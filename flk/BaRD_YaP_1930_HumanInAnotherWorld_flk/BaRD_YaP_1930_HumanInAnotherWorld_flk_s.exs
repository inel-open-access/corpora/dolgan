<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDDB483BFD-BC63-31BD-7D24-750C3AD0AFE7">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>BaRD_YaP_1930_HumanInAnotherWorld_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\BaRD_YaP_1930_HumanInAnotherWorld_flk\BaRD_YaP_1930_HumanInAnotherWorld_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">367</ud-information>
            <ud-information attribute-name="# HIAT:w">275</ud-information>
            <ud-information attribute-name="# e">275</ud-information>
            <ud-information attribute-name="# HIAT:u">50</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="BaRD">
            <abbreviation>BaRD</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
         <tli id="T161" time="84.5" type="appl" />
         <tli id="T162" time="85.0" type="appl" />
         <tli id="T163" time="85.5" type="appl" />
         <tli id="T164" time="86.0" type="appl" />
         <tli id="T165" time="86.5" type="appl" />
         <tli id="T166" time="87.0" type="appl" />
         <tli id="T167" time="87.5" type="appl" />
         <tli id="T168" time="88.0" type="appl" />
         <tli id="T169" time="88.5" type="appl" />
         <tli id="T170" time="89.0" type="appl" />
         <tli id="T171" time="89.5" type="appl" />
         <tli id="T172" time="90.0" type="appl" />
         <tli id="T173" time="90.5" type="appl" />
         <tli id="T174" time="91.0" type="appl" />
         <tli id="T175" time="91.5" type="appl" />
         <tli id="T176" time="92.0" type="appl" />
         <tli id="T177" time="92.5" type="appl" />
         <tli id="T178" time="93.0" type="appl" />
         <tli id="T179" time="93.5" type="appl" />
         <tli id="T180" time="94.0" type="appl" />
         <tli id="T181" time="94.5" type="appl" />
         <tli id="T182" time="95.0" type="appl" />
         <tli id="T183" time="95.5" type="appl" />
         <tli id="T184" time="96.0" type="appl" />
         <tli id="T185" time="96.5" type="appl" />
         <tli id="T186" time="97.0" type="appl" />
         <tli id="T187" time="97.5" type="appl" />
         <tli id="T188" time="98.0" type="appl" />
         <tli id="T189" time="98.5" type="appl" />
         <tli id="T190" time="99.0" type="appl" />
         <tli id="T191" time="99.5" type="appl" />
         <tli id="T192" time="100.0" type="appl" />
         <tli id="T193" time="100.5" type="appl" />
         <tli id="T194" time="101.0" type="appl" />
         <tli id="T195" time="101.5" type="appl" />
         <tli id="T196" time="102.0" type="appl" />
         <tli id="T197" time="102.5" type="appl" />
         <tli id="T198" time="103.0" type="appl" />
         <tli id="T199" time="103.5" type="appl" />
         <tli id="T200" time="104.0" type="appl" />
         <tli id="T201" time="104.5" type="appl" />
         <tli id="T202" time="105.0" type="appl" />
         <tli id="T203" time="105.5" type="appl" />
         <tli id="T204" time="106.0" type="appl" />
         <tli id="T205" time="106.5" type="appl" />
         <tli id="T206" time="107.0" type="appl" />
         <tli id="T207" time="107.5" type="appl" />
         <tli id="T208" time="108.0" type="appl" />
         <tli id="T209" time="108.5" type="appl" />
         <tli id="T210" time="109.0" type="appl" />
         <tli id="T211" time="109.5" type="appl" />
         <tli id="T212" time="110.0" type="appl" />
         <tli id="T213" time="110.5" type="appl" />
         <tli id="T214" time="111.0" type="appl" />
         <tli id="T215" time="111.5" type="appl" />
         <tli id="T216" time="112.0" type="appl" />
         <tli id="T217" time="112.5" type="appl" />
         <tli id="T218" time="113.0" type="appl" />
         <tli id="T219" time="113.5" type="appl" />
         <tli id="T220" time="114.0" type="appl" />
         <tli id="T221" time="114.5" type="appl" />
         <tli id="T222" time="115.0" type="appl" />
         <tli id="T223" time="115.5" type="appl" />
         <tli id="T224" time="116.0" type="appl" />
         <tli id="T225" time="116.5" type="appl" />
         <tli id="T226" time="117.0" type="appl" />
         <tli id="T227" time="117.5" type="appl" />
         <tli id="T228" time="118.0" type="appl" />
         <tli id="T229" time="118.5" type="appl" />
         <tli id="T230" time="119.0" type="appl" />
         <tli id="T231" time="119.5" type="appl" />
         <tli id="T232" time="120.0" type="appl" />
         <tli id="T233" time="120.5" type="appl" />
         <tli id="T234" time="121.0" type="appl" />
         <tli id="T235" time="121.5" type="appl" />
         <tli id="T236" time="122.0" type="appl" />
         <tli id="T237" time="122.5" type="appl" />
         <tli id="T238" time="123.0" type="appl" />
         <tli id="T239" time="123.5" type="appl" />
         <tli id="T240" time="124.0" type="appl" />
         <tli id="T241" time="124.5" type="appl" />
         <tli id="T242" time="125.0" type="appl" />
         <tli id="T243" time="125.5" type="appl" />
         <tli id="T244" time="126.0" type="appl" />
         <tli id="T245" time="126.5" type="appl" />
         <tli id="T246" time="127.0" type="appl" />
         <tli id="T247" time="127.5" type="appl" />
         <tli id="T248" time="128.0" type="appl" />
         <tli id="T249" time="128.5" type="appl" />
         <tli id="T250" time="129.0" type="appl" />
         <tli id="T251" time="129.5" type="appl" />
         <tli id="T252" time="130.0" type="appl" />
         <tli id="T253" time="130.5" type="appl" />
         <tli id="T254" time="131.0" type="appl" />
         <tli id="T255" time="131.5" type="appl" />
         <tli id="T256" time="132.0" type="appl" />
         <tli id="T257" time="132.5" type="appl" />
         <tli id="T258" time="133.0" type="appl" />
         <tli id="T259" time="133.5" type="appl" />
         <tli id="T260" time="134.0" type="appl" />
         <tli id="T261" time="134.5" type="appl" />
         <tli id="T262" time="135.0" type="appl" />
         <tli id="T263" time="135.5" type="appl" />
         <tli id="T264" time="136.0" type="appl" />
         <tli id="T265" time="136.5" type="appl" />
         <tli id="T266" time="137.0" type="appl" />
         <tli id="T267" time="137.5" type="appl" />
         <tli id="T268" time="138.0" type="appl" />
         <tli id="T269" time="138.5" type="appl" />
         <tli id="T270" time="139.0" type="appl" />
         <tli id="T271" time="139.5" type="appl" />
         <tli id="T272" time="140.0" type="appl" />
         <tli id="T273" time="140.5" type="appl" />
         <tli id="T274" time="141.0" type="appl" />
         <tli id="T275" time="141.5" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="BaRD"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T275" id="Seg_0" n="sc" s="T0">
               <ts e="T9" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Bɨlɨr</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">biːr</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">kihi</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">bultana</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">hɨldʼan</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">hir</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">kajagahɨnan</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">tühen</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">kaːlbɨt</ts>
                  <nts id="Seg_29" n="HIAT:ip">.</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_32" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">Tühen</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">baran</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">öjdöːn</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">körbüte</ts>
                  <nts id="Seg_44" n="HIAT:ip">,</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_47" n="HIAT:w" s="T13">hirge</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_50" n="HIAT:w" s="T14">hɨtar</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_53" n="HIAT:w" s="T15">ebit</ts>
                  <nts id="Seg_54" n="HIAT:ip">.</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_57" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">Bu</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_62" n="HIAT:w" s="T17">hɨttagɨna</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_65" n="HIAT:w" s="T18">üs</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_68" n="HIAT:w" s="T19">attaːk</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_71" n="HIAT:w" s="T20">kɨːs</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_74" n="HIAT:w" s="T21">kelbitter</ts>
                  <nts id="Seg_75" n="HIAT:ip">.</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_78" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_80" n="HIAT:w" s="T22">Manɨ</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_82" n="HIAT:ip">"</nts>
                  <ts e="T24" id="Seg_84" n="HIAT:w" s="T23">min</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_87" n="HIAT:w" s="T24">ɨ͡aldʼar</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_90" n="HIAT:w" s="T25">kihibin</ts>
                  <nts id="Seg_91" n="HIAT:ip">,</nts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_94" n="HIAT:w" s="T26">abɨraːŋ</ts>
                  <nts id="Seg_95" n="HIAT:ip">"</nts>
                  <nts id="Seg_96" n="HIAT:ip">,</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_99" n="HIAT:w" s="T27">di͡en</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_102" n="HIAT:w" s="T28">aːttaha</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_105" n="HIAT:w" s="T29">hataːbɨt</ts>
                  <nts id="Seg_106" n="HIAT:ip">,</nts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_109" n="HIAT:w" s="T30">biːrgestere</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_112" n="HIAT:w" s="T31">daː</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_115" n="HIAT:w" s="T32">istibet</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_118" n="HIAT:w" s="T33">kurduk</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_121" n="HIAT:w" s="T34">baran</ts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_124" n="HIAT:w" s="T35">kaːlbɨttar</ts>
                  <nts id="Seg_125" n="HIAT:ip">.</nts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T40" id="Seg_128" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_130" n="HIAT:w" s="T36">Bu</ts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_133" n="HIAT:w" s="T37">kɨrgɨttar</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_136" n="HIAT:w" s="T38">östörün</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_139" n="HIAT:w" s="T39">istibit</ts>
                  <nts id="Seg_140" n="HIAT:ip">:</nts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_143" n="HIAT:u" s="T40">
                  <nts id="Seg_144" n="HIAT:ip">"</nts>
                  <ts e="T41" id="Seg_146" n="HIAT:w" s="T40">Kanna</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_149" n="HIAT:w" s="T41">barbɨta</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_152" n="HIAT:w" s="T42">bu͡olu͡oj</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_155" n="HIAT:w" s="T43">bihigi</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_158" n="HIAT:w" s="T44">tababɨt</ts>
                  <nts id="Seg_159" n="HIAT:ip">"</nts>
                  <nts id="Seg_160" n="HIAT:ip">,</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_163" n="HIAT:w" s="T45">despitter</ts>
                  <nts id="Seg_164" n="HIAT:ip">.</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_167" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_169" n="HIAT:w" s="T46">Balar</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_172" n="HIAT:w" s="T47">dʼi͡eleriger</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_175" n="HIAT:w" s="T48">barbɨttarɨgar</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_178" n="HIAT:w" s="T49">batan</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_181" n="HIAT:w" s="T50">barbɨt</ts>
                  <nts id="Seg_182" n="HIAT:ip">.</nts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_185" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_187" n="HIAT:w" s="T51">Biːr</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_190" n="HIAT:w" s="T52">dʼi͡ege</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_193" n="HIAT:w" s="T53">tijbit</ts>
                  <nts id="Seg_194" n="HIAT:ip">.</nts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_197" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_199" n="HIAT:w" s="T54">Ol</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_202" n="HIAT:w" s="T55">tijen</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_205" n="HIAT:w" s="T56">kiːrbite</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_208" n="HIAT:w" s="T57">ogonnʼordoːk</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_211" n="HIAT:w" s="T58">emeːksin</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_214" n="HIAT:w" s="T59">olorollor</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_217" n="HIAT:w" s="T60">ebit</ts>
                  <nts id="Seg_218" n="HIAT:ip">.</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_221" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_223" n="HIAT:w" s="T61">Üs</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_226" n="HIAT:w" s="T62">kɨːstaːk</ts>
                  <nts id="Seg_227" n="HIAT:ip">.</nts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_230" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_232" n="HIAT:w" s="T63">Aːŋŋa</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_235" n="HIAT:w" s="T64">turan</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_238" n="HIAT:w" s="T65">kördö</ts>
                  <nts id="Seg_239" n="HIAT:ip">.</nts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_242" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_244" n="HIAT:w" s="T66">Biːr</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_247" n="HIAT:w" s="T67">daː</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_250" n="HIAT:w" s="T68">kihi</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_252" n="HIAT:ip">"</nts>
                  <ts e="T70" id="Seg_254" n="HIAT:w" s="T69">kelliŋ</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_257" n="HIAT:w" s="T70">eːt</ts>
                  <nts id="Seg_258" n="HIAT:ip">"</nts>
                  <nts id="Seg_259" n="HIAT:ip">,</nts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_262" n="HIAT:w" s="T71">di͡en</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_265" n="HIAT:w" s="T72">haŋarbata</ts>
                  <nts id="Seg_266" n="HIAT:ip">.</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_269" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_271" n="HIAT:w" s="T73">Manɨ</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_274" n="HIAT:w" s="T74">biːr</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_277" n="HIAT:w" s="T75">kɨːska</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_280" n="HIAT:w" s="T76">hergestehen</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_283" n="HIAT:w" s="T77">olordo</ts>
                  <nts id="Seg_284" n="HIAT:ip">.</nts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_287" n="HIAT:u" s="T78">
                  <ts e="T79" id="Seg_289" n="HIAT:w" s="T78">Manɨ</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_292" n="HIAT:w" s="T79">bu</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_295" n="HIAT:w" s="T80">olorbutun</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_298" n="HIAT:w" s="T81">bilbetter</ts>
                  <nts id="Seg_299" n="HIAT:ip">.</nts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_302" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_304" n="HIAT:w" s="T82">Ahɨː</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_307" n="HIAT:w" s="T83">olordular</ts>
                  <nts id="Seg_308" n="HIAT:ip">.</nts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_311" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_313" n="HIAT:w" s="T84">Onu͡oga</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_315" n="HIAT:ip">"</nts>
                  <ts e="T86" id="Seg_317" n="HIAT:w" s="T85">ahaː</ts>
                  <nts id="Seg_318" n="HIAT:ip">"</nts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_321" n="HIAT:w" s="T86">di͡ebetiler</ts>
                  <nts id="Seg_322" n="HIAT:ip">.</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T94" id="Seg_325" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_327" n="HIAT:w" s="T87">Manɨ</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_330" n="HIAT:w" s="T88">bejete</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_333" n="HIAT:w" s="T89">biːr</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_336" n="HIAT:w" s="T90">eme</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_339" n="HIAT:w" s="T91">lu͡oskukaːnɨ</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_342" n="HIAT:w" s="T92">ɨlan</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_345" n="HIAT:w" s="T93">hiːr</ts>
                  <nts id="Seg_346" n="HIAT:ip">.</nts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_349" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_351" n="HIAT:w" s="T94">Onu</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_354" n="HIAT:w" s="T95">körböttör</ts>
                  <nts id="Seg_355" n="HIAT:ip">.</nts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_358" n="HIAT:u" s="T96">
                  <nts id="Seg_359" n="HIAT:ip">"</nts>
                  <ts e="T97" id="Seg_361" n="HIAT:w" s="T96">Togo</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_364" n="HIAT:w" s="T97">aspɨt</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_367" n="HIAT:w" s="T98">agɨjɨːr</ts>
                  <nts id="Seg_368" n="HIAT:ip">"</nts>
                  <nts id="Seg_369" n="HIAT:ip">,</nts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_372" n="HIAT:w" s="T99">ere</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_375" n="HIAT:w" s="T100">deheller</ts>
                  <nts id="Seg_376" n="HIAT:ip">.</nts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_379" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_381" n="HIAT:w" s="T101">Onton</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_384" n="HIAT:w" s="T102">hanaːta</ts>
                  <nts id="Seg_385" n="HIAT:ip">:</nts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_388" n="HIAT:u" s="T103">
                  <nts id="Seg_389" n="HIAT:ip">"</nts>
                  <ts e="T104" id="Seg_391" n="HIAT:w" s="T103">Miːgin</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_394" n="HIAT:w" s="T104">körböttör</ts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_397" n="HIAT:w" s="T105">bɨhɨːlaːk</ts>
                  <nts id="Seg_398" n="HIAT:ip">.</nts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T109" id="Seg_401" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_403" n="HIAT:w" s="T106">Min</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_406" n="HIAT:w" s="T107">tɨːtan</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_409" n="HIAT:w" s="T108">körüːm</ts>
                  <nts id="Seg_410" n="HIAT:ip">"</nts>
                  <nts id="Seg_411" n="HIAT:ip">.</nts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_414" n="HIAT:u" s="T109">
                  <ts e="T110" id="Seg_416" n="HIAT:w" s="T109">Baran</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_419" n="HIAT:w" s="T110">biːr</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_422" n="HIAT:w" s="T111">kɨːska</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_425" n="HIAT:w" s="T112">herge</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_428" n="HIAT:w" s="T113">oloron</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_431" n="HIAT:w" s="T114">tɨːtar</ts>
                  <nts id="Seg_432" n="HIAT:ip">.</nts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_435" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_437" n="HIAT:w" s="T115">Manɨ͡aga</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_440" n="HIAT:w" s="T116">ölör</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_443" n="HIAT:w" s="T117">haŋata</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_446" n="HIAT:w" s="T118">taksar</ts>
                  <nts id="Seg_447" n="HIAT:ip">.</nts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_450" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_452" n="HIAT:w" s="T119">Bugurduk</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_455" n="HIAT:w" s="T120">üs</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_458" n="HIAT:w" s="T121">konugu</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_461" n="HIAT:w" s="T122">hɨrɨtta</ts>
                  <nts id="Seg_462" n="HIAT:ip">.</nts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_465" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_467" n="HIAT:w" s="T123">Bu</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_470" n="HIAT:w" s="T124">hɨldʼan</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_473" n="HIAT:w" s="T125">duːmajdanar</ts>
                  <nts id="Seg_474" n="HIAT:ip">:</nts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_477" n="HIAT:u" s="T126">
                  <nts id="Seg_478" n="HIAT:ip">"</nts>
                  <ts e="T127" id="Seg_480" n="HIAT:w" s="T126">Min</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_483" n="HIAT:w" s="T127">bu</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_486" n="HIAT:w" s="T128">dojduttan</ts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_489" n="HIAT:w" s="T129">barar</ts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_492" n="HIAT:w" s="T130">keskilbin</ts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_495" n="HIAT:w" s="T131">oŋostu͡oktaːkpɨn</ts>
                  <nts id="Seg_496" n="HIAT:ip">"</nts>
                  <nts id="Seg_497" n="HIAT:ip">,</nts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_500" n="HIAT:w" s="T132">diːr</ts>
                  <nts id="Seg_501" n="HIAT:ip">.</nts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T138" id="Seg_504" n="HIAT:u" s="T133">
                  <ts e="T134" id="Seg_506" n="HIAT:w" s="T133">Biːr</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_509" n="HIAT:w" s="T134">kɨːstan</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_512" n="HIAT:w" s="T135">dʼe</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_515" n="HIAT:w" s="T136">arakpat</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_518" n="HIAT:w" s="T137">bu͡olar</ts>
                  <nts id="Seg_519" n="HIAT:ip">.</nts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_522" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_524" n="HIAT:w" s="T138">Manɨga</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_527" n="HIAT:w" s="T139">ikki</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_530" n="HIAT:w" s="T140">ojunu</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_533" n="HIAT:w" s="T141">kɨːrdardɨlar</ts>
                  <nts id="Seg_534" n="HIAT:ip">,</nts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_537" n="HIAT:w" s="T142">bu</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_540" n="HIAT:w" s="T143">ojuttar</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_543" n="HIAT:w" s="T144">tu͡ok</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_546" n="HIAT:w" s="T145">da</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_549" n="HIAT:w" s="T146">kömönü</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_552" n="HIAT:w" s="T147">bi͡erbetiler</ts>
                  <nts id="Seg_553" n="HIAT:ip">.</nts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_556" n="HIAT:u" s="T148">
                  <ts e="T149" id="Seg_558" n="HIAT:w" s="T148">Manɨ</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_561" n="HIAT:w" s="T149">ettiler</ts>
                  <nts id="Seg_562" n="HIAT:ip">:</nts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_565" n="HIAT:u" s="T150">
                  <nts id="Seg_566" n="HIAT:ip">"</nts>
                  <ts e="T151" id="Seg_568" n="HIAT:w" s="T150">Ulakan</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_571" n="HIAT:w" s="T151">ojunu</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_574" n="HIAT:w" s="T152">egelteri͡ekke</ts>
                  <nts id="Seg_575" n="HIAT:ip">.</nts>
                  <nts id="Seg_576" n="HIAT:ip">"</nts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_579" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_581" n="HIAT:w" s="T153">Ol</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_584" n="HIAT:w" s="T154">ojunnarɨgar</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_587" n="HIAT:w" s="T155">bardɨlar</ts>
                  <nts id="Seg_588" n="HIAT:ip">.</nts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_591" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_593" n="HIAT:w" s="T156">Manɨ</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_596" n="HIAT:w" s="T157">barbɨttarɨn</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_599" n="HIAT:w" s="T158">kenne</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_602" n="HIAT:w" s="T159">u͡ol</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_605" n="HIAT:w" s="T160">horujan</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_608" n="HIAT:w" s="T161">kuːhar</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_611" n="HIAT:w" s="T162">kɨːhɨ</ts>
                  <nts id="Seg_612" n="HIAT:ip">,</nts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_615" n="HIAT:w" s="T163">ontuta</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_618" n="HIAT:w" s="T164">ölör</ts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_621" n="HIAT:w" s="T165">haŋata</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_624" n="HIAT:w" s="T166">taksar</ts>
                  <nts id="Seg_625" n="HIAT:ip">.</nts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T175" id="Seg_628" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_630" n="HIAT:w" s="T167">Bu</ts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_633" n="HIAT:w" s="T168">hɨrɨttagɨna</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_636" n="HIAT:w" s="T169">ikki</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_639" n="HIAT:w" s="T170">karak</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_642" n="HIAT:w" s="T171">u͡ota</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_645" n="HIAT:w" s="T172">huptu</ts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_648" n="HIAT:w" s="T173">kini͡eke</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_651" n="HIAT:w" s="T174">tüste</ts>
                  <nts id="Seg_652" n="HIAT:ip">.</nts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T178" id="Seg_655" n="HIAT:u" s="T175">
                  <nts id="Seg_656" n="HIAT:ip">"</nts>
                  <ts e="T176" id="Seg_658" n="HIAT:w" s="T175">Ojummut</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_661" n="HIAT:w" s="T176">iher</ts>
                  <nts id="Seg_662" n="HIAT:ip">"</nts>
                  <nts id="Seg_663" n="HIAT:ip">,</nts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_666" n="HIAT:w" s="T177">destiler</ts>
                  <nts id="Seg_667" n="HIAT:ip">.</nts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_670" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_672" n="HIAT:w" s="T178">Manɨga</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_675" n="HIAT:w" s="T179">kajtak</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_678" n="HIAT:w" s="T180">gɨnar</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_681" n="HIAT:w" s="T181">ebit</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_684" n="HIAT:w" s="T182">di͡en</ts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_687" n="HIAT:w" s="T183">kɨːhɨn</ts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_690" n="HIAT:w" s="T184">poduːskatɨn</ts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_693" n="HIAT:w" s="T185">annɨgar</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_696" n="HIAT:w" s="T186">hahan</ts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_699" n="HIAT:w" s="T187">kaːlla</ts>
                  <nts id="Seg_700" n="HIAT:ip">.</nts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T197" id="Seg_703" n="HIAT:u" s="T188">
                  <ts e="T189" id="Seg_705" n="HIAT:w" s="T188">Huːlana</ts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_708" n="HIAT:w" s="T189">hataːta</ts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_711" n="HIAT:w" s="T190">da</ts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_714" n="HIAT:w" s="T191">ol</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_717" n="HIAT:w" s="T192">ojun</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_720" n="HIAT:w" s="T193">karagɨn</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_723" n="HIAT:w" s="T194">u͡ota</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_726" n="HIAT:w" s="T195">huptu</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_729" n="HIAT:w" s="T196">körör</ts>
                  <nts id="Seg_730" n="HIAT:ip">.</nts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T204" id="Seg_733" n="HIAT:u" s="T197">
                  <ts e="T198" id="Seg_735" n="HIAT:w" s="T197">Kiːreːt</ts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_738" n="HIAT:w" s="T198">ojuna</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_741" n="HIAT:w" s="T199">ürdüger</ts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_744" n="HIAT:w" s="T200">tüste</ts>
                  <nts id="Seg_745" n="HIAT:ip">,</nts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_748" n="HIAT:w" s="T201">tühen</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_751" n="HIAT:w" s="T202">köksüger</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_754" n="HIAT:w" s="T203">olordo</ts>
                  <nts id="Seg_755" n="HIAT:ip">.</nts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T207" id="Seg_758" n="HIAT:u" s="T204">
                  <ts e="T205" id="Seg_760" n="HIAT:w" s="T204">Manɨ</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_763" n="HIAT:w" s="T205">ulakannɨk</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_766" n="HIAT:w" s="T206">mögüste</ts>
                  <nts id="Seg_767" n="HIAT:ip">.</nts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T215" id="Seg_770" n="HIAT:u" s="T207">
                  <ts e="T208" id="Seg_772" n="HIAT:w" s="T207">Onno</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_775" n="HIAT:w" s="T208">ojuna</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_778" n="HIAT:w" s="T209">ɨːppat</ts>
                  <nts id="Seg_779" n="HIAT:ip">,</nts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_782" n="HIAT:w" s="T210">ol</ts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_785" n="HIAT:w" s="T211">gɨnan</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_788" n="HIAT:w" s="T212">baran</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_791" n="HIAT:w" s="T213">östöːk</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_794" n="HIAT:w" s="T214">bu͡olla</ts>
                  <nts id="Seg_795" n="HIAT:ip">:</nts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T223" id="Seg_798" n="HIAT:u" s="T215">
                  <nts id="Seg_799" n="HIAT:ip">"</nts>
                  <ts e="T216" id="Seg_801" n="HIAT:w" s="T215">En</ts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_804" n="HIAT:w" s="T216">bu</ts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_807" n="HIAT:w" s="T217">tu͡ok</ts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_810" n="HIAT:w" s="T218">bu͡olan</ts>
                  <nts id="Seg_811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_813" n="HIAT:w" s="T219">kihini</ts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_816" n="HIAT:w" s="T220">mu͡okastɨː</ts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_819" n="HIAT:w" s="T221">hɨtagɨn</ts>
                  <nts id="Seg_820" n="HIAT:ip">"</nts>
                  <nts id="Seg_821" n="HIAT:ip">,</nts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_824" n="HIAT:w" s="T222">di͡en</ts>
                  <nts id="Seg_825" n="HIAT:ip">.</nts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T226" id="Seg_828" n="HIAT:u" s="T223">
                  <ts e="T224" id="Seg_830" n="HIAT:w" s="T223">Manɨ</ts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_833" n="HIAT:w" s="T224">kihite</ts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_836" n="HIAT:w" s="T225">kepsi͡ete</ts>
                  <nts id="Seg_837" n="HIAT:ip">:</nts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T233" id="Seg_840" n="HIAT:u" s="T226">
                  <nts id="Seg_841" n="HIAT:ip">"</nts>
                  <ts e="T227" id="Seg_843" n="HIAT:w" s="T226">Min</ts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_846" n="HIAT:w" s="T227">bejem</ts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_849" n="HIAT:w" s="T228">köŋülbünen</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_852" n="HIAT:w" s="T229">kelbetegim</ts>
                  <nts id="Seg_853" n="HIAT:ip">,</nts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_856" n="HIAT:w" s="T230">hir</ts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_859" n="HIAT:w" s="T231">kajagahɨnan</ts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_862" n="HIAT:w" s="T232">tühen</ts>
                  <nts id="Seg_863" n="HIAT:ip">.</nts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T243" id="Seg_866" n="HIAT:u" s="T233">
                  <ts e="T234" id="Seg_868" n="HIAT:w" s="T233">Min</ts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_871" n="HIAT:w" s="T234">ɨ͡aldʼa</ts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_874" n="HIAT:w" s="T235">hɨtammɨn</ts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_877" n="HIAT:w" s="T236">bu</ts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_880" n="HIAT:w" s="T237">üs</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_883" n="HIAT:w" s="T238">kɨːhɨ</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_886" n="HIAT:w" s="T239">kördöspüpper</ts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_889" n="HIAT:w" s="T240">biːrgestere</ts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_892" n="HIAT:w" s="T241">da</ts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_895" n="HIAT:w" s="T242">kömölöspötögö</ts>
                  <nts id="Seg_896" n="HIAT:ip">.</nts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T248" id="Seg_899" n="HIAT:u" s="T243">
                  <ts e="T244" id="Seg_901" n="HIAT:w" s="T243">Ol</ts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_904" n="HIAT:w" s="T244">tuhuttan</ts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_907" n="HIAT:w" s="T245">anaːn</ts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_910" n="HIAT:w" s="T246">tɨːta</ts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_913" n="HIAT:w" s="T247">hɨtabɨn</ts>
                  <nts id="Seg_914" n="HIAT:ip">.</nts>
                  <nts id="Seg_915" n="HIAT:ip">"</nts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T249" id="Seg_918" n="HIAT:u" s="T248">
                  <ts e="T249" id="Seg_920" n="HIAT:w" s="T248">Onu͡oga</ts>
                  <nts id="Seg_921" n="HIAT:ip">:</nts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T252" id="Seg_924" n="HIAT:u" s="T249">
                  <nts id="Seg_925" n="HIAT:ip">"</nts>
                  <ts e="T250" id="Seg_927" n="HIAT:w" s="T249">Kirdik</ts>
                  <nts id="Seg_928" n="HIAT:ip">,</nts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_931" n="HIAT:w" s="T250">en</ts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_934" n="HIAT:w" s="T251">keːs</ts>
                  <nts id="Seg_935" n="HIAT:ip">.</nts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T258" id="Seg_938" n="HIAT:u" s="T252">
                  <ts e="T253" id="Seg_940" n="HIAT:w" s="T252">Min</ts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_943" n="HIAT:w" s="T253">ejigin</ts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_946" n="HIAT:w" s="T254">törüt</ts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_949" n="HIAT:w" s="T255">hirger</ts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_952" n="HIAT:w" s="T256">ataːrɨ͡am</ts>
                  <nts id="Seg_953" n="HIAT:ip">"</nts>
                  <nts id="Seg_954" n="HIAT:ip">,</nts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_957" n="HIAT:w" s="T257">di͡ete</ts>
                  <nts id="Seg_958" n="HIAT:ip">.</nts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T267" id="Seg_961" n="HIAT:u" s="T258">
                  <ts e="T259" id="Seg_963" n="HIAT:w" s="T258">Emek</ts>
                  <nts id="Seg_964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_966" n="HIAT:w" s="T259">kuruska</ts>
                  <nts id="Seg_967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_969" n="HIAT:w" s="T260">ürdüger</ts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_972" n="HIAT:w" s="T261">miːnnerde</ts>
                  <nts id="Seg_973" n="HIAT:ip">,</nts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_976" n="HIAT:w" s="T262">ontuta</ts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_979" n="HIAT:w" s="T263">at</ts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_982" n="HIAT:w" s="T264">bu͡olan</ts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_985" n="HIAT:w" s="T265">kötön</ts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_988" n="HIAT:w" s="T266">barbɨt</ts>
                  <nts id="Seg_989" n="HIAT:ip">.</nts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T275" id="Seg_992" n="HIAT:u" s="T267">
                  <ts e="T268" id="Seg_994" n="HIAT:w" s="T267">Öjdöːn</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_997" n="HIAT:w" s="T268">körbüte</ts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1000" n="HIAT:w" s="T269">emek</ts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1003" n="HIAT:w" s="T270">mahɨ</ts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1006" n="HIAT:w" s="T271">miːnen</ts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1009" n="HIAT:w" s="T272">baran</ts>
                  <nts id="Seg_1010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1012" n="HIAT:w" s="T273">dojdutugar</ts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1015" n="HIAT:w" s="T274">hɨtar</ts>
                  <nts id="Seg_1016" n="HIAT:ip">.</nts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T275" id="Seg_1018" n="sc" s="T0">
               <ts e="T1" id="Seg_1020" n="e" s="T0">Bɨlɨr </ts>
               <ts e="T2" id="Seg_1022" n="e" s="T1">biːr </ts>
               <ts e="T3" id="Seg_1024" n="e" s="T2">kihi </ts>
               <ts e="T4" id="Seg_1026" n="e" s="T3">bultana </ts>
               <ts e="T5" id="Seg_1028" n="e" s="T4">hɨldʼan </ts>
               <ts e="T6" id="Seg_1030" n="e" s="T5">hir </ts>
               <ts e="T7" id="Seg_1032" n="e" s="T6">kajagahɨnan </ts>
               <ts e="T8" id="Seg_1034" n="e" s="T7">tühen </ts>
               <ts e="T9" id="Seg_1036" n="e" s="T8">kaːlbɨt. </ts>
               <ts e="T10" id="Seg_1038" n="e" s="T9">Tühen </ts>
               <ts e="T11" id="Seg_1040" n="e" s="T10">baran </ts>
               <ts e="T12" id="Seg_1042" n="e" s="T11">öjdöːn </ts>
               <ts e="T13" id="Seg_1044" n="e" s="T12">körbüte, </ts>
               <ts e="T14" id="Seg_1046" n="e" s="T13">hirge </ts>
               <ts e="T15" id="Seg_1048" n="e" s="T14">hɨtar </ts>
               <ts e="T16" id="Seg_1050" n="e" s="T15">ebit. </ts>
               <ts e="T17" id="Seg_1052" n="e" s="T16">Bu </ts>
               <ts e="T18" id="Seg_1054" n="e" s="T17">hɨttagɨna </ts>
               <ts e="T19" id="Seg_1056" n="e" s="T18">üs </ts>
               <ts e="T20" id="Seg_1058" n="e" s="T19">attaːk </ts>
               <ts e="T21" id="Seg_1060" n="e" s="T20">kɨːs </ts>
               <ts e="T22" id="Seg_1062" n="e" s="T21">kelbitter. </ts>
               <ts e="T23" id="Seg_1064" n="e" s="T22">Manɨ </ts>
               <ts e="T24" id="Seg_1066" n="e" s="T23">"min </ts>
               <ts e="T25" id="Seg_1068" n="e" s="T24">ɨ͡aldʼar </ts>
               <ts e="T26" id="Seg_1070" n="e" s="T25">kihibin, </ts>
               <ts e="T27" id="Seg_1072" n="e" s="T26">abɨraːŋ", </ts>
               <ts e="T28" id="Seg_1074" n="e" s="T27">di͡en </ts>
               <ts e="T29" id="Seg_1076" n="e" s="T28">aːttaha </ts>
               <ts e="T30" id="Seg_1078" n="e" s="T29">hataːbɨt, </ts>
               <ts e="T31" id="Seg_1080" n="e" s="T30">biːrgestere </ts>
               <ts e="T32" id="Seg_1082" n="e" s="T31">daː </ts>
               <ts e="T33" id="Seg_1084" n="e" s="T32">istibet </ts>
               <ts e="T34" id="Seg_1086" n="e" s="T33">kurduk </ts>
               <ts e="T35" id="Seg_1088" n="e" s="T34">baran </ts>
               <ts e="T36" id="Seg_1090" n="e" s="T35">kaːlbɨttar. </ts>
               <ts e="T37" id="Seg_1092" n="e" s="T36">Bu </ts>
               <ts e="T38" id="Seg_1094" n="e" s="T37">kɨrgɨttar </ts>
               <ts e="T39" id="Seg_1096" n="e" s="T38">östörün </ts>
               <ts e="T40" id="Seg_1098" n="e" s="T39">istibit: </ts>
               <ts e="T41" id="Seg_1100" n="e" s="T40">"Kanna </ts>
               <ts e="T42" id="Seg_1102" n="e" s="T41">barbɨta </ts>
               <ts e="T43" id="Seg_1104" n="e" s="T42">bu͡olu͡oj </ts>
               <ts e="T44" id="Seg_1106" n="e" s="T43">bihigi </ts>
               <ts e="T45" id="Seg_1108" n="e" s="T44">tababɨt", </ts>
               <ts e="T46" id="Seg_1110" n="e" s="T45">despitter. </ts>
               <ts e="T47" id="Seg_1112" n="e" s="T46">Balar </ts>
               <ts e="T48" id="Seg_1114" n="e" s="T47">dʼi͡eleriger </ts>
               <ts e="T49" id="Seg_1116" n="e" s="T48">barbɨttarɨgar </ts>
               <ts e="T50" id="Seg_1118" n="e" s="T49">batan </ts>
               <ts e="T51" id="Seg_1120" n="e" s="T50">barbɨt. </ts>
               <ts e="T52" id="Seg_1122" n="e" s="T51">Biːr </ts>
               <ts e="T53" id="Seg_1124" n="e" s="T52">dʼi͡ege </ts>
               <ts e="T54" id="Seg_1126" n="e" s="T53">tijbit. </ts>
               <ts e="T55" id="Seg_1128" n="e" s="T54">Ol </ts>
               <ts e="T56" id="Seg_1130" n="e" s="T55">tijen </ts>
               <ts e="T57" id="Seg_1132" n="e" s="T56">kiːrbite </ts>
               <ts e="T58" id="Seg_1134" n="e" s="T57">ogonnʼordoːk </ts>
               <ts e="T59" id="Seg_1136" n="e" s="T58">emeːksin </ts>
               <ts e="T60" id="Seg_1138" n="e" s="T59">olorollor </ts>
               <ts e="T61" id="Seg_1140" n="e" s="T60">ebit. </ts>
               <ts e="T62" id="Seg_1142" n="e" s="T61">Üs </ts>
               <ts e="T63" id="Seg_1144" n="e" s="T62">kɨːstaːk. </ts>
               <ts e="T64" id="Seg_1146" n="e" s="T63">Aːŋŋa </ts>
               <ts e="T65" id="Seg_1148" n="e" s="T64">turan </ts>
               <ts e="T66" id="Seg_1150" n="e" s="T65">kördö. </ts>
               <ts e="T67" id="Seg_1152" n="e" s="T66">Biːr </ts>
               <ts e="T68" id="Seg_1154" n="e" s="T67">daː </ts>
               <ts e="T69" id="Seg_1156" n="e" s="T68">kihi </ts>
               <ts e="T70" id="Seg_1158" n="e" s="T69">"kelliŋ </ts>
               <ts e="T71" id="Seg_1160" n="e" s="T70">eːt", </ts>
               <ts e="T72" id="Seg_1162" n="e" s="T71">di͡en </ts>
               <ts e="T73" id="Seg_1164" n="e" s="T72">haŋarbata. </ts>
               <ts e="T74" id="Seg_1166" n="e" s="T73">Manɨ </ts>
               <ts e="T75" id="Seg_1168" n="e" s="T74">biːr </ts>
               <ts e="T76" id="Seg_1170" n="e" s="T75">kɨːska </ts>
               <ts e="T77" id="Seg_1172" n="e" s="T76">hergestehen </ts>
               <ts e="T78" id="Seg_1174" n="e" s="T77">olordo. </ts>
               <ts e="T79" id="Seg_1176" n="e" s="T78">Manɨ </ts>
               <ts e="T80" id="Seg_1178" n="e" s="T79">bu </ts>
               <ts e="T81" id="Seg_1180" n="e" s="T80">olorbutun </ts>
               <ts e="T82" id="Seg_1182" n="e" s="T81">bilbetter. </ts>
               <ts e="T83" id="Seg_1184" n="e" s="T82">Ahɨː </ts>
               <ts e="T84" id="Seg_1186" n="e" s="T83">olordular. </ts>
               <ts e="T85" id="Seg_1188" n="e" s="T84">Onu͡oga </ts>
               <ts e="T86" id="Seg_1190" n="e" s="T85">"ahaː" </ts>
               <ts e="T87" id="Seg_1192" n="e" s="T86">di͡ebetiler. </ts>
               <ts e="T88" id="Seg_1194" n="e" s="T87">Manɨ </ts>
               <ts e="T89" id="Seg_1196" n="e" s="T88">bejete </ts>
               <ts e="T90" id="Seg_1198" n="e" s="T89">biːr </ts>
               <ts e="T91" id="Seg_1200" n="e" s="T90">eme </ts>
               <ts e="T92" id="Seg_1202" n="e" s="T91">lu͡oskukaːnɨ </ts>
               <ts e="T93" id="Seg_1204" n="e" s="T92">ɨlan </ts>
               <ts e="T94" id="Seg_1206" n="e" s="T93">hiːr. </ts>
               <ts e="T95" id="Seg_1208" n="e" s="T94">Onu </ts>
               <ts e="T96" id="Seg_1210" n="e" s="T95">körböttör. </ts>
               <ts e="T97" id="Seg_1212" n="e" s="T96">"Togo </ts>
               <ts e="T98" id="Seg_1214" n="e" s="T97">aspɨt </ts>
               <ts e="T99" id="Seg_1216" n="e" s="T98">agɨjɨːr", </ts>
               <ts e="T100" id="Seg_1218" n="e" s="T99">ere </ts>
               <ts e="T101" id="Seg_1220" n="e" s="T100">deheller. </ts>
               <ts e="T102" id="Seg_1222" n="e" s="T101">Onton </ts>
               <ts e="T103" id="Seg_1224" n="e" s="T102">hanaːta: </ts>
               <ts e="T104" id="Seg_1226" n="e" s="T103">"Miːgin </ts>
               <ts e="T105" id="Seg_1228" n="e" s="T104">körböttör </ts>
               <ts e="T106" id="Seg_1230" n="e" s="T105">bɨhɨːlaːk. </ts>
               <ts e="T107" id="Seg_1232" n="e" s="T106">Min </ts>
               <ts e="T108" id="Seg_1234" n="e" s="T107">tɨːtan </ts>
               <ts e="T109" id="Seg_1236" n="e" s="T108">körüːm". </ts>
               <ts e="T110" id="Seg_1238" n="e" s="T109">Baran </ts>
               <ts e="T111" id="Seg_1240" n="e" s="T110">biːr </ts>
               <ts e="T112" id="Seg_1242" n="e" s="T111">kɨːska </ts>
               <ts e="T113" id="Seg_1244" n="e" s="T112">herge </ts>
               <ts e="T114" id="Seg_1246" n="e" s="T113">oloron </ts>
               <ts e="T115" id="Seg_1248" n="e" s="T114">tɨːtar. </ts>
               <ts e="T116" id="Seg_1250" n="e" s="T115">Manɨ͡aga </ts>
               <ts e="T117" id="Seg_1252" n="e" s="T116">ölör </ts>
               <ts e="T118" id="Seg_1254" n="e" s="T117">haŋata </ts>
               <ts e="T119" id="Seg_1256" n="e" s="T118">taksar. </ts>
               <ts e="T120" id="Seg_1258" n="e" s="T119">Bugurduk </ts>
               <ts e="T121" id="Seg_1260" n="e" s="T120">üs </ts>
               <ts e="T122" id="Seg_1262" n="e" s="T121">konugu </ts>
               <ts e="T123" id="Seg_1264" n="e" s="T122">hɨrɨtta. </ts>
               <ts e="T124" id="Seg_1266" n="e" s="T123">Bu </ts>
               <ts e="T125" id="Seg_1268" n="e" s="T124">hɨldʼan </ts>
               <ts e="T126" id="Seg_1270" n="e" s="T125">duːmajdanar: </ts>
               <ts e="T127" id="Seg_1272" n="e" s="T126">"Min </ts>
               <ts e="T128" id="Seg_1274" n="e" s="T127">bu </ts>
               <ts e="T129" id="Seg_1276" n="e" s="T128">dojduttan </ts>
               <ts e="T130" id="Seg_1278" n="e" s="T129">barar </ts>
               <ts e="T131" id="Seg_1280" n="e" s="T130">keskilbin </ts>
               <ts e="T132" id="Seg_1282" n="e" s="T131">oŋostu͡oktaːkpɨn", </ts>
               <ts e="T133" id="Seg_1284" n="e" s="T132">diːr. </ts>
               <ts e="T134" id="Seg_1286" n="e" s="T133">Biːr </ts>
               <ts e="T135" id="Seg_1288" n="e" s="T134">kɨːstan </ts>
               <ts e="T136" id="Seg_1290" n="e" s="T135">dʼe </ts>
               <ts e="T137" id="Seg_1292" n="e" s="T136">arakpat </ts>
               <ts e="T138" id="Seg_1294" n="e" s="T137">bu͡olar. </ts>
               <ts e="T139" id="Seg_1296" n="e" s="T138">Manɨga </ts>
               <ts e="T140" id="Seg_1298" n="e" s="T139">ikki </ts>
               <ts e="T141" id="Seg_1300" n="e" s="T140">ojunu </ts>
               <ts e="T142" id="Seg_1302" n="e" s="T141">kɨːrdardɨlar, </ts>
               <ts e="T143" id="Seg_1304" n="e" s="T142">bu </ts>
               <ts e="T144" id="Seg_1306" n="e" s="T143">ojuttar </ts>
               <ts e="T145" id="Seg_1308" n="e" s="T144">tu͡ok </ts>
               <ts e="T146" id="Seg_1310" n="e" s="T145">da </ts>
               <ts e="T147" id="Seg_1312" n="e" s="T146">kömönü </ts>
               <ts e="T148" id="Seg_1314" n="e" s="T147">bi͡erbetiler. </ts>
               <ts e="T149" id="Seg_1316" n="e" s="T148">Manɨ </ts>
               <ts e="T150" id="Seg_1318" n="e" s="T149">ettiler: </ts>
               <ts e="T151" id="Seg_1320" n="e" s="T150">"Ulakan </ts>
               <ts e="T152" id="Seg_1322" n="e" s="T151">ojunu </ts>
               <ts e="T153" id="Seg_1324" n="e" s="T152">egelteri͡ekke." </ts>
               <ts e="T154" id="Seg_1326" n="e" s="T153">Ol </ts>
               <ts e="T155" id="Seg_1328" n="e" s="T154">ojunnarɨgar </ts>
               <ts e="T156" id="Seg_1330" n="e" s="T155">bardɨlar. </ts>
               <ts e="T157" id="Seg_1332" n="e" s="T156">Manɨ </ts>
               <ts e="T158" id="Seg_1334" n="e" s="T157">barbɨttarɨn </ts>
               <ts e="T159" id="Seg_1336" n="e" s="T158">kenne </ts>
               <ts e="T160" id="Seg_1338" n="e" s="T159">u͡ol </ts>
               <ts e="T161" id="Seg_1340" n="e" s="T160">horujan </ts>
               <ts e="T162" id="Seg_1342" n="e" s="T161">kuːhar </ts>
               <ts e="T163" id="Seg_1344" n="e" s="T162">kɨːhɨ, </ts>
               <ts e="T164" id="Seg_1346" n="e" s="T163">ontuta </ts>
               <ts e="T165" id="Seg_1348" n="e" s="T164">ölör </ts>
               <ts e="T166" id="Seg_1350" n="e" s="T165">haŋata </ts>
               <ts e="T167" id="Seg_1352" n="e" s="T166">taksar. </ts>
               <ts e="T168" id="Seg_1354" n="e" s="T167">Bu </ts>
               <ts e="T169" id="Seg_1356" n="e" s="T168">hɨrɨttagɨna </ts>
               <ts e="T170" id="Seg_1358" n="e" s="T169">ikki </ts>
               <ts e="T171" id="Seg_1360" n="e" s="T170">karak </ts>
               <ts e="T172" id="Seg_1362" n="e" s="T171">u͡ota </ts>
               <ts e="T173" id="Seg_1364" n="e" s="T172">huptu </ts>
               <ts e="T174" id="Seg_1366" n="e" s="T173">kini͡eke </ts>
               <ts e="T175" id="Seg_1368" n="e" s="T174">tüste. </ts>
               <ts e="T176" id="Seg_1370" n="e" s="T175">"Ojummut </ts>
               <ts e="T177" id="Seg_1372" n="e" s="T176">iher", </ts>
               <ts e="T178" id="Seg_1374" n="e" s="T177">destiler. </ts>
               <ts e="T179" id="Seg_1376" n="e" s="T178">Manɨga </ts>
               <ts e="T180" id="Seg_1378" n="e" s="T179">kajtak </ts>
               <ts e="T181" id="Seg_1380" n="e" s="T180">gɨnar </ts>
               <ts e="T182" id="Seg_1382" n="e" s="T181">ebit </ts>
               <ts e="T183" id="Seg_1384" n="e" s="T182">di͡en </ts>
               <ts e="T184" id="Seg_1386" n="e" s="T183">kɨːhɨn </ts>
               <ts e="T185" id="Seg_1388" n="e" s="T184">poduːskatɨn </ts>
               <ts e="T186" id="Seg_1390" n="e" s="T185">annɨgar </ts>
               <ts e="T187" id="Seg_1392" n="e" s="T186">hahan </ts>
               <ts e="T188" id="Seg_1394" n="e" s="T187">kaːlla. </ts>
               <ts e="T189" id="Seg_1396" n="e" s="T188">Huːlana </ts>
               <ts e="T190" id="Seg_1398" n="e" s="T189">hataːta </ts>
               <ts e="T191" id="Seg_1400" n="e" s="T190">da </ts>
               <ts e="T192" id="Seg_1402" n="e" s="T191">ol </ts>
               <ts e="T193" id="Seg_1404" n="e" s="T192">ojun </ts>
               <ts e="T194" id="Seg_1406" n="e" s="T193">karagɨn </ts>
               <ts e="T195" id="Seg_1408" n="e" s="T194">u͡ota </ts>
               <ts e="T196" id="Seg_1410" n="e" s="T195">huptu </ts>
               <ts e="T197" id="Seg_1412" n="e" s="T196">körör. </ts>
               <ts e="T198" id="Seg_1414" n="e" s="T197">Kiːreːt </ts>
               <ts e="T199" id="Seg_1416" n="e" s="T198">ojuna </ts>
               <ts e="T200" id="Seg_1418" n="e" s="T199">ürdüger </ts>
               <ts e="T201" id="Seg_1420" n="e" s="T200">tüste, </ts>
               <ts e="T202" id="Seg_1422" n="e" s="T201">tühen </ts>
               <ts e="T203" id="Seg_1424" n="e" s="T202">köksüger </ts>
               <ts e="T204" id="Seg_1426" n="e" s="T203">olordo. </ts>
               <ts e="T205" id="Seg_1428" n="e" s="T204">Manɨ </ts>
               <ts e="T206" id="Seg_1430" n="e" s="T205">ulakannɨk </ts>
               <ts e="T207" id="Seg_1432" n="e" s="T206">mögüste. </ts>
               <ts e="T208" id="Seg_1434" n="e" s="T207">Onno </ts>
               <ts e="T209" id="Seg_1436" n="e" s="T208">ojuna </ts>
               <ts e="T210" id="Seg_1438" n="e" s="T209">ɨːppat, </ts>
               <ts e="T211" id="Seg_1440" n="e" s="T210">ol </ts>
               <ts e="T212" id="Seg_1442" n="e" s="T211">gɨnan </ts>
               <ts e="T213" id="Seg_1444" n="e" s="T212">baran </ts>
               <ts e="T214" id="Seg_1446" n="e" s="T213">östöːk </ts>
               <ts e="T215" id="Seg_1448" n="e" s="T214">bu͡olla: </ts>
               <ts e="T216" id="Seg_1450" n="e" s="T215">"En </ts>
               <ts e="T217" id="Seg_1452" n="e" s="T216">bu </ts>
               <ts e="T218" id="Seg_1454" n="e" s="T217">tu͡ok </ts>
               <ts e="T219" id="Seg_1456" n="e" s="T218">bu͡olan </ts>
               <ts e="T220" id="Seg_1458" n="e" s="T219">kihini </ts>
               <ts e="T221" id="Seg_1460" n="e" s="T220">mu͡okastɨː </ts>
               <ts e="T222" id="Seg_1462" n="e" s="T221">hɨtagɨn", </ts>
               <ts e="T223" id="Seg_1464" n="e" s="T222">di͡en. </ts>
               <ts e="T224" id="Seg_1466" n="e" s="T223">Manɨ </ts>
               <ts e="T225" id="Seg_1468" n="e" s="T224">kihite </ts>
               <ts e="T226" id="Seg_1470" n="e" s="T225">kepsi͡ete: </ts>
               <ts e="T227" id="Seg_1472" n="e" s="T226">"Min </ts>
               <ts e="T228" id="Seg_1474" n="e" s="T227">bejem </ts>
               <ts e="T229" id="Seg_1476" n="e" s="T228">köŋülbünen </ts>
               <ts e="T230" id="Seg_1478" n="e" s="T229">kelbetegim, </ts>
               <ts e="T231" id="Seg_1480" n="e" s="T230">hir </ts>
               <ts e="T232" id="Seg_1482" n="e" s="T231">kajagahɨnan </ts>
               <ts e="T233" id="Seg_1484" n="e" s="T232">tühen. </ts>
               <ts e="T234" id="Seg_1486" n="e" s="T233">Min </ts>
               <ts e="T235" id="Seg_1488" n="e" s="T234">ɨ͡aldʼa </ts>
               <ts e="T236" id="Seg_1490" n="e" s="T235">hɨtammɨn </ts>
               <ts e="T237" id="Seg_1492" n="e" s="T236">bu </ts>
               <ts e="T238" id="Seg_1494" n="e" s="T237">üs </ts>
               <ts e="T239" id="Seg_1496" n="e" s="T238">kɨːhɨ </ts>
               <ts e="T240" id="Seg_1498" n="e" s="T239">kördöspüpper </ts>
               <ts e="T241" id="Seg_1500" n="e" s="T240">biːrgestere </ts>
               <ts e="T242" id="Seg_1502" n="e" s="T241">da </ts>
               <ts e="T243" id="Seg_1504" n="e" s="T242">kömölöspötögö. </ts>
               <ts e="T244" id="Seg_1506" n="e" s="T243">Ol </ts>
               <ts e="T245" id="Seg_1508" n="e" s="T244">tuhuttan </ts>
               <ts e="T246" id="Seg_1510" n="e" s="T245">anaːn </ts>
               <ts e="T247" id="Seg_1512" n="e" s="T246">tɨːta </ts>
               <ts e="T248" id="Seg_1514" n="e" s="T247">hɨtabɨn." </ts>
               <ts e="T249" id="Seg_1516" n="e" s="T248">Onu͡oga: </ts>
               <ts e="T250" id="Seg_1518" n="e" s="T249">"Kirdik, </ts>
               <ts e="T251" id="Seg_1520" n="e" s="T250">en </ts>
               <ts e="T252" id="Seg_1522" n="e" s="T251">keːs. </ts>
               <ts e="T253" id="Seg_1524" n="e" s="T252">Min </ts>
               <ts e="T254" id="Seg_1526" n="e" s="T253">ejigin </ts>
               <ts e="T255" id="Seg_1528" n="e" s="T254">törüt </ts>
               <ts e="T256" id="Seg_1530" n="e" s="T255">hirger </ts>
               <ts e="T257" id="Seg_1532" n="e" s="T256">ataːrɨ͡am", </ts>
               <ts e="T258" id="Seg_1534" n="e" s="T257">di͡ete. </ts>
               <ts e="T259" id="Seg_1536" n="e" s="T258">Emek </ts>
               <ts e="T260" id="Seg_1538" n="e" s="T259">kuruska </ts>
               <ts e="T261" id="Seg_1540" n="e" s="T260">ürdüger </ts>
               <ts e="T262" id="Seg_1542" n="e" s="T261">miːnnerde, </ts>
               <ts e="T263" id="Seg_1544" n="e" s="T262">ontuta </ts>
               <ts e="T264" id="Seg_1546" n="e" s="T263">at </ts>
               <ts e="T265" id="Seg_1548" n="e" s="T264">bu͡olan </ts>
               <ts e="T266" id="Seg_1550" n="e" s="T265">kötön </ts>
               <ts e="T267" id="Seg_1552" n="e" s="T266">barbɨt. </ts>
               <ts e="T268" id="Seg_1554" n="e" s="T267">Öjdöːn </ts>
               <ts e="T269" id="Seg_1556" n="e" s="T268">körbüte </ts>
               <ts e="T270" id="Seg_1558" n="e" s="T269">emek </ts>
               <ts e="T271" id="Seg_1560" n="e" s="T270">mahɨ </ts>
               <ts e="T272" id="Seg_1562" n="e" s="T271">miːnen </ts>
               <ts e="T273" id="Seg_1564" n="e" s="T272">baran </ts>
               <ts e="T274" id="Seg_1566" n="e" s="T273">dojdutugar </ts>
               <ts e="T275" id="Seg_1568" n="e" s="T274">hɨtar. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T9" id="Seg_1569" s="T0">BaRD_YaP_1930_HumanInAnotherWorld_flk.001 (001.001)</ta>
            <ta e="T16" id="Seg_1570" s="T9">BaRD_YaP_1930_HumanInAnotherWorld_flk.002 (001.002)</ta>
            <ta e="T22" id="Seg_1571" s="T16">BaRD_YaP_1930_HumanInAnotherWorld_flk.003 (001.003)</ta>
            <ta e="T36" id="Seg_1572" s="T22">BaRD_YaP_1930_HumanInAnotherWorld_flk.004 (001.004)</ta>
            <ta e="T40" id="Seg_1573" s="T36">BaRD_YaP_1930_HumanInAnotherWorld_flk.005 (001.005)</ta>
            <ta e="T46" id="Seg_1574" s="T40">BaRD_YaP_1930_HumanInAnotherWorld_flk.006 (001.005)</ta>
            <ta e="T51" id="Seg_1575" s="T46">BaRD_YaP_1930_HumanInAnotherWorld_flk.007 (001.007)</ta>
            <ta e="T54" id="Seg_1576" s="T51">BaRD_YaP_1930_HumanInAnotherWorld_flk.008 (001.008)</ta>
            <ta e="T61" id="Seg_1577" s="T54">BaRD_YaP_1930_HumanInAnotherWorld_flk.009 (001.009)</ta>
            <ta e="T63" id="Seg_1578" s="T61">BaRD_YaP_1930_HumanInAnotherWorld_flk.010 (001.010)</ta>
            <ta e="T66" id="Seg_1579" s="T63">BaRD_YaP_1930_HumanInAnotherWorld_flk.011 (001.011)</ta>
            <ta e="T73" id="Seg_1580" s="T66">BaRD_YaP_1930_HumanInAnotherWorld_flk.012 (001.012)</ta>
            <ta e="T78" id="Seg_1581" s="T73">BaRD_YaP_1930_HumanInAnotherWorld_flk.013 (001.013)</ta>
            <ta e="T82" id="Seg_1582" s="T78">BaRD_YaP_1930_HumanInAnotherWorld_flk.014 (001.014)</ta>
            <ta e="T84" id="Seg_1583" s="T82">BaRD_YaP_1930_HumanInAnotherWorld_flk.015 (001.015)</ta>
            <ta e="T87" id="Seg_1584" s="T84">BaRD_YaP_1930_HumanInAnotherWorld_flk.016 (001.016)</ta>
            <ta e="T94" id="Seg_1585" s="T87">BaRD_YaP_1930_HumanInAnotherWorld_flk.017 (001.017)</ta>
            <ta e="T96" id="Seg_1586" s="T94">BaRD_YaP_1930_HumanInAnotherWorld_flk.018 (001.018)</ta>
            <ta e="T101" id="Seg_1587" s="T96">BaRD_YaP_1930_HumanInAnotherWorld_flk.019 (001.019)</ta>
            <ta e="T103" id="Seg_1588" s="T101">BaRD_YaP_1930_HumanInAnotherWorld_flk.020 (001.020)</ta>
            <ta e="T106" id="Seg_1589" s="T103">BaRD_YaP_1930_HumanInAnotherWorld_flk.021 (001.020)</ta>
            <ta e="T109" id="Seg_1590" s="T106">BaRD_YaP_1930_HumanInAnotherWorld_flk.022 (001.021)</ta>
            <ta e="T115" id="Seg_1591" s="T109">BaRD_YaP_1930_HumanInAnotherWorld_flk.023 (001.022)</ta>
            <ta e="T119" id="Seg_1592" s="T115">BaRD_YaP_1930_HumanInAnotherWorld_flk.024 (001.023)</ta>
            <ta e="T123" id="Seg_1593" s="T119">BaRD_YaP_1930_HumanInAnotherWorld_flk.025 (001.024)</ta>
            <ta e="T126" id="Seg_1594" s="T123">BaRD_YaP_1930_HumanInAnotherWorld_flk.026 (001.025)</ta>
            <ta e="T133" id="Seg_1595" s="T126">BaRD_YaP_1930_HumanInAnotherWorld_flk.027 (001.025)</ta>
            <ta e="T138" id="Seg_1596" s="T133">BaRD_YaP_1930_HumanInAnotherWorld_flk.028 (001.026)</ta>
            <ta e="T148" id="Seg_1597" s="T138">BaRD_YaP_1930_HumanInAnotherWorld_flk.029 (001.027)</ta>
            <ta e="T150" id="Seg_1598" s="T148">BaRD_YaP_1930_HumanInAnotherWorld_flk.030 (001.028)</ta>
            <ta e="T153" id="Seg_1599" s="T150">BaRD_YaP_1930_HumanInAnotherWorld_flk.031 (001.028)</ta>
            <ta e="T156" id="Seg_1600" s="T153">BaRD_YaP_1930_HumanInAnotherWorld_flk.032 (001.029)</ta>
            <ta e="T167" id="Seg_1601" s="T156">BaRD_YaP_1930_HumanInAnotherWorld_flk.033 (001.030)</ta>
            <ta e="T175" id="Seg_1602" s="T167">BaRD_YaP_1930_HumanInAnotherWorld_flk.034 (001.031)</ta>
            <ta e="T178" id="Seg_1603" s="T175">BaRD_YaP_1930_HumanInAnotherWorld_flk.035 (001.032)</ta>
            <ta e="T188" id="Seg_1604" s="T178">BaRD_YaP_1930_HumanInAnotherWorld_flk.036 (001.033)</ta>
            <ta e="T197" id="Seg_1605" s="T188">BaRD_YaP_1930_HumanInAnotherWorld_flk.037 (001.034)</ta>
            <ta e="T204" id="Seg_1606" s="T197">BaRD_YaP_1930_HumanInAnotherWorld_flk.038 (001.035)</ta>
            <ta e="T207" id="Seg_1607" s="T204">BaRD_YaP_1930_HumanInAnotherWorld_flk.039 (001.036)</ta>
            <ta e="T215" id="Seg_1608" s="T207">BaRD_YaP_1930_HumanInAnotherWorld_flk.040 (001.037)</ta>
            <ta e="T223" id="Seg_1609" s="T215">BaRD_YaP_1930_HumanInAnotherWorld_flk.041 (001.038)</ta>
            <ta e="T226" id="Seg_1610" s="T223">BaRD_YaP_1930_HumanInAnotherWorld_flk.042 (001.040)</ta>
            <ta e="T233" id="Seg_1611" s="T226">BaRD_YaP_1930_HumanInAnotherWorld_flk.043 (001.041)</ta>
            <ta e="T243" id="Seg_1612" s="T233">BaRD_YaP_1930_HumanInAnotherWorld_flk.044 (001.042)</ta>
            <ta e="T248" id="Seg_1613" s="T243">BaRD_YaP_1930_HumanInAnotherWorld_flk.045 (001.043)</ta>
            <ta e="T249" id="Seg_1614" s="T248">BaRD_YaP_1930_HumanInAnotherWorld_flk.046 (001.044)</ta>
            <ta e="T252" id="Seg_1615" s="T249">BaRD_YaP_1930_HumanInAnotherWorld_flk.047 (001.045)</ta>
            <ta e="T258" id="Seg_1616" s="T252">BaRD_YaP_1930_HumanInAnotherWorld_flk.048 (001.046)</ta>
            <ta e="T267" id="Seg_1617" s="T258">BaRD_YaP_1930_HumanInAnotherWorld_flk.049 (001.047)</ta>
            <ta e="T275" id="Seg_1618" s="T267">BaRD_YaP_1930_HumanInAnotherWorld_flk.050 (001.048)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T9" id="Seg_1619" s="T0">Былыр биир киһи бултана һылдьан һир кайагаһынан түһэн каалбыт.</ta>
            <ta e="T16" id="Seg_1620" s="T9">Түһэн баран өйдөөн көрбүтэ, һиргэ һытар эбит.</ta>
            <ta e="T22" id="Seg_1621" s="T16">Бу һыттагына үс аттаак кыыс кэлбиттэр.</ta>
            <ta e="T36" id="Seg_1622" s="T22">Маны: "Мин ыалдьар киһибин, абырааҥ", — диэн ааттаһа һатаабыт, бииргэстэрэ даа истибэт курдук баран каалбыттар.</ta>
            <ta e="T40" id="Seg_1623" s="T36">Бу кыргыттар өстөрүн истибит: </ta>
            <ta e="T46" id="Seg_1624" s="T40">"Канна барбыта буолуой биһиги табабыт?" — дэспиттэр.</ta>
            <ta e="T51" id="Seg_1625" s="T46">Балар дьиэлэригэр барбыттарыгар батан барбыт.</ta>
            <ta e="T54" id="Seg_1626" s="T51">Биир дьиэгэ тийбит.</ta>
            <ta e="T61" id="Seg_1627" s="T54">Ол тийэн киирбитэ огонньордоок эмээксин олороллор эбит.</ta>
            <ta e="T63" id="Seg_1628" s="T61">Үс кыыстаак.</ta>
            <ta e="T66" id="Seg_1629" s="T63">Ааҥҥа туран көрдө.</ta>
            <ta e="T73" id="Seg_1630" s="T66">Биир даа киһи: "Кэллиҥ ээт" — диэн һаҥарбата.</ta>
            <ta e="T78" id="Seg_1631" s="T73">Маны биир кыыска һэргэстэһэн олордо.</ta>
            <ta e="T82" id="Seg_1632" s="T78">Маны бу олорбутун билбэттэр.</ta>
            <ta e="T84" id="Seg_1633" s="T82">Аһыы олордулар.</ta>
            <ta e="T87" id="Seg_1634" s="T84">Онуога "аһаа" диэбэтилэр.</ta>
            <ta e="T94" id="Seg_1635" s="T87">Маны бэйэтэ биир эмэ луоскукааны ылан һиир.</ta>
            <ta e="T96" id="Seg_1636" s="T94">Ону көрбөттөр.</ta>
            <ta e="T101" id="Seg_1637" s="T96">"Того аспыт агыйыыр" эрэ дэһэллэр.</ta>
            <ta e="T103" id="Seg_1638" s="T101">Онтон һанаата: </ta>
            <ta e="T106" id="Seg_1639" s="T103">"Миигин көрбөттөр быһыылаак.</ta>
            <ta e="T109" id="Seg_1640" s="T106">Мин тыытан көрүүм".</ta>
            <ta e="T115" id="Seg_1641" s="T109">Баран биир кыыска һэргэ олорон тыытар.</ta>
            <ta e="T119" id="Seg_1642" s="T115">Маныага өлөр һаҥата таксар.</ta>
            <ta e="T123" id="Seg_1643" s="T119">Бугурдук үс конугу һырытта.</ta>
            <ta e="T126" id="Seg_1644" s="T123">Бу һылдьан дуумайданар: </ta>
            <ta e="T133" id="Seg_1645" s="T126">"Мин бу дойдуттан барар кэскилбин оҥостуоктаакпын", — диир.</ta>
            <ta e="T138" id="Seg_1646" s="T133">Биир кыыстан дьэ аракпат буолар.</ta>
            <ta e="T148" id="Seg_1647" s="T138">Маныга икки ойуну кыырдардылар, бу ойуттар туок да көмөнү биэрбэтилэр.</ta>
            <ta e="T150" id="Seg_1648" s="T148">Маны эттилэр: </ta>
            <ta e="T153" id="Seg_1649" s="T150">"Улакан ойуну эгэлтэриэккэ".</ta>
            <ta e="T156" id="Seg_1650" s="T153">Ол ойуннарыгар бардылар.</ta>
            <ta e="T167" id="Seg_1651" s="T156">Маны барбыттарын кэннэ уол һоруйан кууһар кыыһы, онтута өлөр һаҥата таксар.</ta>
            <ta e="T175" id="Seg_1652" s="T167">Бу һырыттагына икки карак уота һупту киниэкэ түстэ.</ta>
            <ta e="T178" id="Seg_1653" s="T175">"Ойуммут иһэр", — дэстилэр.</ta>
            <ta e="T188" id="Seg_1654" s="T178">Маныга кайтак гынар эбит диэн кыыһын подуускатын анныгар һаһан каалла.</ta>
            <ta e="T197" id="Seg_1655" s="T188">һуулана һатаата да ол ойун карагын уота һупту көрөр.</ta>
            <ta e="T204" id="Seg_1656" s="T197">Киирээт ойуна үрдүгэр түстэ, түһэн көксүгэр олордо.</ta>
            <ta e="T207" id="Seg_1657" s="T204">Маны улаканнык мөгүстэ.</ta>
            <ta e="T215" id="Seg_1658" s="T207">Онно ойуна ыыппат, ол гынан баран өстөөк буолла:</ta>
            <ta e="T223" id="Seg_1659" s="T215">— Эн бу туок буолан киһини муокастыы һытагын? — диэн.</ta>
            <ta e="T226" id="Seg_1660" s="T223">Маны киһитэ кэпсиэтэ:</ta>
            <ta e="T233" id="Seg_1661" s="T226">— Мин бэйэм көҥүлбүнэн кэлбэтэгим, һир кайагаһынан түһэн.</ta>
            <ta e="T243" id="Seg_1662" s="T233">Мин ыалдьа һытаммын бу үс кыыһы көрдөспүппэр бииргэстэрэ да көмөлөспөтөгө.</ta>
            <ta e="T248" id="Seg_1663" s="T243">Ол туһуттан анаан тыыта һытабын.</ta>
            <ta e="T249" id="Seg_1664" s="T248">Онуога:</ta>
            <ta e="T252" id="Seg_1665" s="T249">— Кирдик, эн кээс.</ta>
            <ta e="T258" id="Seg_1666" s="T252">Мин эйигин төрүт һиргэр атаарыам, — диэтэ.</ta>
            <ta e="T267" id="Seg_1667" s="T258">Эмэк куруска үрдүгэр мииннэрдэ, онтута ат буолан көтөн барбыт.</ta>
            <ta e="T275" id="Seg_1668" s="T267">Өйдөөн көрбүтэ эмэк маһы миинэн баран дойдутугар һытар.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T9" id="Seg_1669" s="T0">Bɨlɨr biːr kihi bultana hɨldʼan hir kajagahɨnan tühen kaːlbɨt. </ta>
            <ta e="T16" id="Seg_1670" s="T9">Tühen baran öjdöːn körbüte, hirge hɨtar ebit. </ta>
            <ta e="T22" id="Seg_1671" s="T16">Bu hɨttagɨna üs attaːk kɨːs kelbitter. </ta>
            <ta e="T36" id="Seg_1672" s="T22">Manɨ "min ɨ͡aldʼar kihibin, abɨraːŋ", di͡en aːttaha hataːbɨt, biːrgestere daː istibet kurduk baran kaːlbɨttar. </ta>
            <ta e="T40" id="Seg_1673" s="T36">Bu kɨrgɨttar östörün istibit: </ta>
            <ta e="T46" id="Seg_1674" s="T40">"Kanna barbɨta bu͡olu͡oj bihigi tababɨt", despitter. </ta>
            <ta e="T51" id="Seg_1675" s="T46">Balar dʼi͡eleriger barbɨttarɨgar batan barbɨt. </ta>
            <ta e="T54" id="Seg_1676" s="T51">Biːr dʼi͡ege tijbit. </ta>
            <ta e="T61" id="Seg_1677" s="T54">Ol tijen kiːrbite ogonnʼordoːk emeːksin olorollor ebit. </ta>
            <ta e="T63" id="Seg_1678" s="T61">Üs kɨːstaːk. </ta>
            <ta e="T66" id="Seg_1679" s="T63">Aːŋŋa turan kördö. </ta>
            <ta e="T73" id="Seg_1680" s="T66">Biːr daː kihi "kelliŋ eːt", di͡en haŋarbata. </ta>
            <ta e="T78" id="Seg_1681" s="T73">Manɨ biːr kɨːska hergestehen olordo. </ta>
            <ta e="T82" id="Seg_1682" s="T78">Manɨ bu olorbutun bilbetter. </ta>
            <ta e="T84" id="Seg_1683" s="T82">Ahɨː olordular. </ta>
            <ta e="T87" id="Seg_1684" s="T84">Onu͡oga "ahaː" di͡ebetiler. </ta>
            <ta e="T94" id="Seg_1685" s="T87">Manɨ bejete biːr eme lu͡oskukaːnɨ ɨlan hiːr. </ta>
            <ta e="T96" id="Seg_1686" s="T94">Onu körböttör. </ta>
            <ta e="T101" id="Seg_1687" s="T96">"Togo aspɨt agɨjɨːr", ere deheller. </ta>
            <ta e="T103" id="Seg_1688" s="T101">Onton hanaːta: </ta>
            <ta e="T106" id="Seg_1689" s="T103">"Miːgin körböttör bɨhɨːlaːk. </ta>
            <ta e="T109" id="Seg_1690" s="T106">Min tɨːtan körüːm". </ta>
            <ta e="T115" id="Seg_1691" s="T109">Baran biːr kɨːska herge oloron tɨːtar. </ta>
            <ta e="T119" id="Seg_1692" s="T115">Manɨ͡aga ölör haŋata taksar. </ta>
            <ta e="T123" id="Seg_1693" s="T119">Bugurduk üs konugu hɨrɨtta. </ta>
            <ta e="T126" id="Seg_1694" s="T123">Bu hɨldʼan duːmajdanar: </ta>
            <ta e="T133" id="Seg_1695" s="T126">"Min bu dojduttan barar keskilbin oŋostu͡oktaːkpɨn", diːr. </ta>
            <ta e="T138" id="Seg_1696" s="T133">Biːr kɨːstan dʼe arakpat bu͡olar. </ta>
            <ta e="T148" id="Seg_1697" s="T138">Manɨga ikki ojunu kɨːrdardɨlar, bu ojuttar tu͡ok da kömönü bi͡erbetiler. </ta>
            <ta e="T150" id="Seg_1698" s="T148">Manɨ ettiler: </ta>
            <ta e="T153" id="Seg_1699" s="T150">"Ulakan ojunu egelteri͡ekke." </ta>
            <ta e="T156" id="Seg_1700" s="T153">Ol ojunnarɨgar bardɨlar. </ta>
            <ta e="T167" id="Seg_1701" s="T156">Manɨ barbɨttarɨn kenne u͡ol horujan kuːhar kɨːhɨ, ontuta ölör haŋata taksar. </ta>
            <ta e="T175" id="Seg_1702" s="T167">Bu hɨrɨttagɨna ikki karak u͡ota huptu kini͡eke tüste. </ta>
            <ta e="T178" id="Seg_1703" s="T175">"Ojummut iher", destiler. </ta>
            <ta e="T188" id="Seg_1704" s="T178">Manɨga kajtak gɨnar ebit di͡en kɨːhɨn poduːskatɨn annɨgar hahan kaːlla. </ta>
            <ta e="T197" id="Seg_1705" s="T188">Huːlana hataːta da ol ojun karagɨn u͡ota huptu körör. </ta>
            <ta e="T204" id="Seg_1706" s="T197">Kiːreːt ojuna ürdüger tüste, tühen köksüger olordo. </ta>
            <ta e="T207" id="Seg_1707" s="T204">Manɨ ulakannɨk mögüste. </ta>
            <ta e="T215" id="Seg_1708" s="T207">Onno ojuna ɨːppat, ol gɨnan baran östöːk bu͡olla: </ta>
            <ta e="T223" id="Seg_1709" s="T215">"En bu tu͡ok bu͡olan kihini mu͡okastɨː hɨtagɨn", di͡en. </ta>
            <ta e="T226" id="Seg_1710" s="T223">Manɨ kihite kepsi͡ete:</ta>
            <ta e="T233" id="Seg_1711" s="T226">"Min bejem köŋülbünen kelbetegim, hir kajagahɨnan tühen. </ta>
            <ta e="T243" id="Seg_1712" s="T233">Min ɨ͡aldʼa hɨtammɨn bu üs kɨːhɨ kördöspüpper biːrgestere da kömölöspötögö. </ta>
            <ta e="T248" id="Seg_1713" s="T243">Ol tuhuttan anaːn tɨːta hɨtabɨn." </ta>
            <ta e="T249" id="Seg_1714" s="T248">Onu͡oga:</ta>
            <ta e="T252" id="Seg_1715" s="T249">"Kirdik, en keːs. </ta>
            <ta e="T258" id="Seg_1716" s="T252">Min ejigin törüt hirger ataːrɨ͡am", di͡ete. </ta>
            <ta e="T267" id="Seg_1717" s="T258">Emek kuruska ürdüger miːnnerde, ontuta at bu͡olan kötön barbɨt. </ta>
            <ta e="T275" id="Seg_1718" s="T267">Öjdöːn körbüte emek mahɨ miːnen baran dojdutugar hɨtar. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1719" s="T0">bɨlɨr</ta>
            <ta e="T2" id="Seg_1720" s="T1">biːr</ta>
            <ta e="T3" id="Seg_1721" s="T2">kihi</ta>
            <ta e="T4" id="Seg_1722" s="T3">bultan-a</ta>
            <ta e="T5" id="Seg_1723" s="T4">hɨldʼ-an</ta>
            <ta e="T6" id="Seg_1724" s="T5">hir</ta>
            <ta e="T7" id="Seg_1725" s="T6">kajagah-ɨ-nan</ta>
            <ta e="T8" id="Seg_1726" s="T7">tüh-en</ta>
            <ta e="T9" id="Seg_1727" s="T8">kaːl-bɨt</ta>
            <ta e="T10" id="Seg_1728" s="T9">tüh-en</ta>
            <ta e="T11" id="Seg_1729" s="T10">baran</ta>
            <ta e="T12" id="Seg_1730" s="T11">öjdöː-n</ta>
            <ta e="T13" id="Seg_1731" s="T12">kör-büt-e</ta>
            <ta e="T14" id="Seg_1732" s="T13">hir-ge</ta>
            <ta e="T15" id="Seg_1733" s="T14">hɨt-ar</ta>
            <ta e="T16" id="Seg_1734" s="T15">e-bit</ta>
            <ta e="T17" id="Seg_1735" s="T16">bu</ta>
            <ta e="T18" id="Seg_1736" s="T17">hɨt-tag-ɨna</ta>
            <ta e="T19" id="Seg_1737" s="T18">üs</ta>
            <ta e="T20" id="Seg_1738" s="T19">at-taːk</ta>
            <ta e="T21" id="Seg_1739" s="T20">kɨːs</ta>
            <ta e="T22" id="Seg_1740" s="T21">kel-bit-ter</ta>
            <ta e="T23" id="Seg_1741" s="T22">ma-nɨ</ta>
            <ta e="T24" id="Seg_1742" s="T23">min</ta>
            <ta e="T25" id="Seg_1743" s="T24">ɨ͡aldʼ-ar</ta>
            <ta e="T26" id="Seg_1744" s="T25">kihi-bin</ta>
            <ta e="T27" id="Seg_1745" s="T26">abɨraː-ŋ</ta>
            <ta e="T28" id="Seg_1746" s="T27">di͡e-n</ta>
            <ta e="T29" id="Seg_1747" s="T28">aːttah-a</ta>
            <ta e="T30" id="Seg_1748" s="T29">hataː-bɨt</ta>
            <ta e="T31" id="Seg_1749" s="T30">biːrges-tere</ta>
            <ta e="T32" id="Seg_1750" s="T31">daː</ta>
            <ta e="T33" id="Seg_1751" s="T32">ist-i-bet</ta>
            <ta e="T34" id="Seg_1752" s="T33">kurduk</ta>
            <ta e="T35" id="Seg_1753" s="T34">bar-an</ta>
            <ta e="T36" id="Seg_1754" s="T35">kaːl-bɨt-tar</ta>
            <ta e="T37" id="Seg_1755" s="T36">bu</ta>
            <ta e="T38" id="Seg_1756" s="T37">kɨrgɨt-tar</ta>
            <ta e="T39" id="Seg_1757" s="T38">ös-törü-n</ta>
            <ta e="T40" id="Seg_1758" s="T39">ist-i-bit</ta>
            <ta e="T41" id="Seg_1759" s="T40">kanna</ta>
            <ta e="T42" id="Seg_1760" s="T41">bar-bɨt-a</ta>
            <ta e="T43" id="Seg_1761" s="T42">bu͡olu͡o=j</ta>
            <ta e="T44" id="Seg_1762" s="T43">bihigi</ta>
            <ta e="T45" id="Seg_1763" s="T44">taba-bɨt</ta>
            <ta e="T46" id="Seg_1764" s="T45">d-e-s-pit-ter</ta>
            <ta e="T47" id="Seg_1765" s="T46">ba-lar</ta>
            <ta e="T48" id="Seg_1766" s="T47">dʼi͡e-leri-ger</ta>
            <ta e="T49" id="Seg_1767" s="T48">bar-bɨt-tarɨ-gar</ta>
            <ta e="T50" id="Seg_1768" s="T49">bat-an</ta>
            <ta e="T51" id="Seg_1769" s="T50">bar-bɨt</ta>
            <ta e="T52" id="Seg_1770" s="T51">biːr</ta>
            <ta e="T53" id="Seg_1771" s="T52">dʼi͡e-ge</ta>
            <ta e="T54" id="Seg_1772" s="T53">tij-bit</ta>
            <ta e="T55" id="Seg_1773" s="T54">ol</ta>
            <ta e="T56" id="Seg_1774" s="T55">tij-en</ta>
            <ta e="T57" id="Seg_1775" s="T56">kiːr-bit-e</ta>
            <ta e="T58" id="Seg_1776" s="T57">ogonnʼor-doːk</ta>
            <ta e="T59" id="Seg_1777" s="T58">emeːksin</ta>
            <ta e="T60" id="Seg_1778" s="T59">olor-ol-lor</ta>
            <ta e="T61" id="Seg_1779" s="T60">e-bit</ta>
            <ta e="T62" id="Seg_1780" s="T61">üs</ta>
            <ta e="T63" id="Seg_1781" s="T62">kɨːs-taːk</ta>
            <ta e="T64" id="Seg_1782" s="T63">aːŋ-ŋa</ta>
            <ta e="T65" id="Seg_1783" s="T64">tur-an</ta>
            <ta e="T66" id="Seg_1784" s="T65">kör-d-ö</ta>
            <ta e="T67" id="Seg_1785" s="T66">biːr</ta>
            <ta e="T68" id="Seg_1786" s="T67">daː</ta>
            <ta e="T69" id="Seg_1787" s="T68">kihi</ta>
            <ta e="T70" id="Seg_1788" s="T69">kel-li-ŋ</ta>
            <ta e="T71" id="Seg_1789" s="T70">eːt</ta>
            <ta e="T72" id="Seg_1790" s="T71">di͡e-n</ta>
            <ta e="T73" id="Seg_1791" s="T72">haŋar-ba-t-a</ta>
            <ta e="T74" id="Seg_1792" s="T73">ma-nɨ</ta>
            <ta e="T75" id="Seg_1793" s="T74">biːr</ta>
            <ta e="T76" id="Seg_1794" s="T75">kɨːs-ka</ta>
            <ta e="T77" id="Seg_1795" s="T76">hergesteh-en</ta>
            <ta e="T78" id="Seg_1796" s="T77">olor-d-o</ta>
            <ta e="T79" id="Seg_1797" s="T78">ma-nɨ</ta>
            <ta e="T80" id="Seg_1798" s="T79">bu</ta>
            <ta e="T81" id="Seg_1799" s="T80">olor-but-u-n</ta>
            <ta e="T82" id="Seg_1800" s="T81">bil-bet-ter</ta>
            <ta e="T83" id="Seg_1801" s="T82">ah-ɨː</ta>
            <ta e="T84" id="Seg_1802" s="T83">olor-du-lar</ta>
            <ta e="T85" id="Seg_1803" s="T84">onu͡o-ga</ta>
            <ta e="T86" id="Seg_1804" s="T85">ahaː</ta>
            <ta e="T87" id="Seg_1805" s="T86">di͡e-be-ti-ler</ta>
            <ta e="T88" id="Seg_1806" s="T87">ma-nɨ</ta>
            <ta e="T89" id="Seg_1807" s="T88">beje-te</ta>
            <ta e="T90" id="Seg_1808" s="T89">biːr</ta>
            <ta e="T91" id="Seg_1809" s="T90">eme</ta>
            <ta e="T92" id="Seg_1810" s="T91">lu͡osku-kaːn-ɨ</ta>
            <ta e="T93" id="Seg_1811" s="T92">ɨl-an</ta>
            <ta e="T94" id="Seg_1812" s="T93">hiː-r</ta>
            <ta e="T95" id="Seg_1813" s="T94">o-nu</ta>
            <ta e="T96" id="Seg_1814" s="T95">kör-böt-tör</ta>
            <ta e="T97" id="Seg_1815" s="T96">togo</ta>
            <ta e="T98" id="Seg_1816" s="T97">as-pɨt</ta>
            <ta e="T99" id="Seg_1817" s="T98">agɨjɨː-r</ta>
            <ta e="T100" id="Seg_1818" s="T99">ere</ta>
            <ta e="T101" id="Seg_1819" s="T100">d-e-h-el-ler</ta>
            <ta e="T102" id="Seg_1820" s="T101">onton</ta>
            <ta e="T103" id="Seg_1821" s="T102">hanaː-t-a</ta>
            <ta e="T104" id="Seg_1822" s="T103">miːgi-n</ta>
            <ta e="T105" id="Seg_1823" s="T104">kör-böt-tör</ta>
            <ta e="T106" id="Seg_1824" s="T105">bɨhɨːlaːk</ta>
            <ta e="T107" id="Seg_1825" s="T106">min</ta>
            <ta e="T108" id="Seg_1826" s="T107">tɨːt-an</ta>
            <ta e="T109" id="Seg_1827" s="T108">kör-üːm</ta>
            <ta e="T110" id="Seg_1828" s="T109">bar-an</ta>
            <ta e="T111" id="Seg_1829" s="T110">biːr</ta>
            <ta e="T112" id="Seg_1830" s="T111">kɨːs-ka</ta>
            <ta e="T113" id="Seg_1831" s="T112">herge</ta>
            <ta e="T114" id="Seg_1832" s="T113">olor-on</ta>
            <ta e="T115" id="Seg_1833" s="T114">tɨːt-ar</ta>
            <ta e="T116" id="Seg_1834" s="T115">manɨ͡a-ga</ta>
            <ta e="T117" id="Seg_1835" s="T116">öl-ör</ta>
            <ta e="T118" id="Seg_1836" s="T117">haŋa-ta</ta>
            <ta e="T119" id="Seg_1837" s="T118">taks-ar</ta>
            <ta e="T120" id="Seg_1838" s="T119">bugurduk</ta>
            <ta e="T121" id="Seg_1839" s="T120">üs</ta>
            <ta e="T122" id="Seg_1840" s="T121">konug-u</ta>
            <ta e="T123" id="Seg_1841" s="T122">hɨrɨt-t-a</ta>
            <ta e="T124" id="Seg_1842" s="T123">bu</ta>
            <ta e="T125" id="Seg_1843" s="T124">hɨldʼ-an</ta>
            <ta e="T126" id="Seg_1844" s="T125">duːmaj-dan-ar</ta>
            <ta e="T127" id="Seg_1845" s="T126">min</ta>
            <ta e="T128" id="Seg_1846" s="T127">bu</ta>
            <ta e="T129" id="Seg_1847" s="T128">dojdu-ttan</ta>
            <ta e="T130" id="Seg_1848" s="T129">bar-ar</ta>
            <ta e="T131" id="Seg_1849" s="T130">keskil-bi-n</ta>
            <ta e="T132" id="Seg_1850" s="T131">oŋost-u͡ok-taːk-pɨn</ta>
            <ta e="T133" id="Seg_1851" s="T132">diː-r</ta>
            <ta e="T134" id="Seg_1852" s="T133">biːr</ta>
            <ta e="T135" id="Seg_1853" s="T134">kɨːs-tan</ta>
            <ta e="T136" id="Seg_1854" s="T135">dʼe</ta>
            <ta e="T137" id="Seg_1855" s="T136">arak-pat</ta>
            <ta e="T138" id="Seg_1856" s="T137">bu͡ol-ar</ta>
            <ta e="T139" id="Seg_1857" s="T138">manɨ-ga</ta>
            <ta e="T140" id="Seg_1858" s="T139">ikki</ta>
            <ta e="T141" id="Seg_1859" s="T140">ojun-u</ta>
            <ta e="T142" id="Seg_1860" s="T141">kɨːr-dar-dɨ-lar</ta>
            <ta e="T143" id="Seg_1861" s="T142">bu</ta>
            <ta e="T144" id="Seg_1862" s="T143">ojut-tar</ta>
            <ta e="T145" id="Seg_1863" s="T144">tu͡ok</ta>
            <ta e="T146" id="Seg_1864" s="T145">da</ta>
            <ta e="T147" id="Seg_1865" s="T146">kömö-nü</ta>
            <ta e="T148" id="Seg_1866" s="T147">bi͡er-be-ti-ler</ta>
            <ta e="T149" id="Seg_1867" s="T148">ma-nɨ</ta>
            <ta e="T150" id="Seg_1868" s="T149">et-ti-ler</ta>
            <ta e="T151" id="Seg_1869" s="T150">ulakan</ta>
            <ta e="T152" id="Seg_1870" s="T151">ojun-u</ta>
            <ta e="T153" id="Seg_1871" s="T152">egel-ter-i͡ek-ke</ta>
            <ta e="T154" id="Seg_1872" s="T153">ol</ta>
            <ta e="T155" id="Seg_1873" s="T154">ojun-narɨ-gar</ta>
            <ta e="T156" id="Seg_1874" s="T155">bar-dɨ-lar</ta>
            <ta e="T157" id="Seg_1875" s="T156">ma-nɨ</ta>
            <ta e="T158" id="Seg_1876" s="T157">bar-bɨt-tarɨ-n</ta>
            <ta e="T159" id="Seg_1877" s="T158">kenne</ta>
            <ta e="T160" id="Seg_1878" s="T159">u͡ol</ta>
            <ta e="T161" id="Seg_1879" s="T160">horujan</ta>
            <ta e="T162" id="Seg_1880" s="T161">kuːh-ar</ta>
            <ta e="T163" id="Seg_1881" s="T162">kɨːh-ɨ</ta>
            <ta e="T164" id="Seg_1882" s="T163">on-tu-ta</ta>
            <ta e="T165" id="Seg_1883" s="T164">öl-ör</ta>
            <ta e="T166" id="Seg_1884" s="T165">haŋa-ta</ta>
            <ta e="T167" id="Seg_1885" s="T166">taks-ar</ta>
            <ta e="T168" id="Seg_1886" s="T167">bu</ta>
            <ta e="T169" id="Seg_1887" s="T168">hɨrɨt-tag-ɨna</ta>
            <ta e="T170" id="Seg_1888" s="T169">ikki</ta>
            <ta e="T171" id="Seg_1889" s="T170">karak</ta>
            <ta e="T172" id="Seg_1890" s="T171">u͡ot-a</ta>
            <ta e="T173" id="Seg_1891" s="T172">huptu</ta>
            <ta e="T174" id="Seg_1892" s="T173">kini͡e-ke</ta>
            <ta e="T175" id="Seg_1893" s="T174">tüs-t-e</ta>
            <ta e="T176" id="Seg_1894" s="T175">ojum-mut</ta>
            <ta e="T177" id="Seg_1895" s="T176">ih-er</ta>
            <ta e="T178" id="Seg_1896" s="T177">d-e-s-ti-ler</ta>
            <ta e="T179" id="Seg_1897" s="T178">manɨ-ga</ta>
            <ta e="T180" id="Seg_1898" s="T179">kajtak</ta>
            <ta e="T181" id="Seg_1899" s="T180">gɨn-ar</ta>
            <ta e="T182" id="Seg_1900" s="T181">e-bit</ta>
            <ta e="T183" id="Seg_1901" s="T182">di͡e-n</ta>
            <ta e="T184" id="Seg_1902" s="T183">kɨːh-ɨ-n</ta>
            <ta e="T185" id="Seg_1903" s="T184">poduːska-tɨ-n</ta>
            <ta e="T186" id="Seg_1904" s="T185">ann-ɨ-gar</ta>
            <ta e="T187" id="Seg_1905" s="T186">hah-an</ta>
            <ta e="T188" id="Seg_1906" s="T187">kaːl-l-a</ta>
            <ta e="T189" id="Seg_1907" s="T188">huːlan-a</ta>
            <ta e="T190" id="Seg_1908" s="T189">hataː-t-a</ta>
            <ta e="T191" id="Seg_1909" s="T190">da</ta>
            <ta e="T192" id="Seg_1910" s="T191">ol</ta>
            <ta e="T193" id="Seg_1911" s="T192">ojun</ta>
            <ta e="T194" id="Seg_1912" s="T193">karag-ɨ-n</ta>
            <ta e="T195" id="Seg_1913" s="T194">u͡ot-a</ta>
            <ta e="T196" id="Seg_1914" s="T195">huptu</ta>
            <ta e="T197" id="Seg_1915" s="T196">kör-ör</ta>
            <ta e="T198" id="Seg_1916" s="T197">kiːr-eːt</ta>
            <ta e="T199" id="Seg_1917" s="T198">ojun-a</ta>
            <ta e="T200" id="Seg_1918" s="T199">ürd-ü-ger</ta>
            <ta e="T201" id="Seg_1919" s="T200">tüs-t-e</ta>
            <ta e="T202" id="Seg_1920" s="T201">tüh-en</ta>
            <ta e="T203" id="Seg_1921" s="T202">köks-ü-ger</ta>
            <ta e="T204" id="Seg_1922" s="T203">olor-d-o</ta>
            <ta e="T205" id="Seg_1923" s="T204">ma-nɨ</ta>
            <ta e="T206" id="Seg_1924" s="T205">ulagan-nɨk</ta>
            <ta e="T207" id="Seg_1925" s="T206">mögüs-t-e</ta>
            <ta e="T208" id="Seg_1926" s="T207">onno</ta>
            <ta e="T209" id="Seg_1927" s="T208">ojun-a</ta>
            <ta e="T210" id="Seg_1928" s="T209">ɨːp-pat</ta>
            <ta e="T211" id="Seg_1929" s="T210">ol</ta>
            <ta e="T212" id="Seg_1930" s="T211">gɨn-an</ta>
            <ta e="T213" id="Seg_1931" s="T212">baran</ta>
            <ta e="T214" id="Seg_1932" s="T213">ös-töːk</ta>
            <ta e="T215" id="Seg_1933" s="T214">bu͡ol-l-a</ta>
            <ta e="T216" id="Seg_1934" s="T215">en</ta>
            <ta e="T217" id="Seg_1935" s="T216">bu</ta>
            <ta e="T218" id="Seg_1936" s="T217">tu͡ok</ta>
            <ta e="T219" id="Seg_1937" s="T218">bu͡ol-an</ta>
            <ta e="T220" id="Seg_1938" s="T219">kihi-ni</ta>
            <ta e="T221" id="Seg_1939" s="T220">mu͡okast-ɨː</ta>
            <ta e="T222" id="Seg_1940" s="T221">hɨt-a-gɨn</ta>
            <ta e="T223" id="Seg_1941" s="T222">di͡e-n</ta>
            <ta e="T224" id="Seg_1942" s="T223">ma-nɨ</ta>
            <ta e="T225" id="Seg_1943" s="T224">kihi-te</ta>
            <ta e="T226" id="Seg_1944" s="T225">kepsi͡e-t-e</ta>
            <ta e="T227" id="Seg_1945" s="T226">min</ta>
            <ta e="T228" id="Seg_1946" s="T227">beje-m</ta>
            <ta e="T229" id="Seg_1947" s="T228">köŋül-bü-nen</ta>
            <ta e="T230" id="Seg_1948" s="T229">kel-beteg-i-m</ta>
            <ta e="T231" id="Seg_1949" s="T230">hir</ta>
            <ta e="T232" id="Seg_1950" s="T231">kajagah-ɨ-nan</ta>
            <ta e="T233" id="Seg_1951" s="T232">tüh-en</ta>
            <ta e="T234" id="Seg_1952" s="T233">min</ta>
            <ta e="T235" id="Seg_1953" s="T234">ɨ͡aldʼ-a</ta>
            <ta e="T236" id="Seg_1954" s="T235">hɨt-am-mɨn</ta>
            <ta e="T237" id="Seg_1955" s="T236">bu</ta>
            <ta e="T238" id="Seg_1956" s="T237">üs</ta>
            <ta e="T239" id="Seg_1957" s="T238">kɨːh-ɨ</ta>
            <ta e="T240" id="Seg_1958" s="T239">körd-ö-s-püp-pe-r</ta>
            <ta e="T241" id="Seg_1959" s="T240">biːrges-tere</ta>
            <ta e="T242" id="Seg_1960" s="T241">da</ta>
            <ta e="T243" id="Seg_1961" s="T242">kömölös-pötög-ö</ta>
            <ta e="T244" id="Seg_1962" s="T243">ol</ta>
            <ta e="T245" id="Seg_1963" s="T244">tuh-u-ttan</ta>
            <ta e="T246" id="Seg_1964" s="T245">anaːn</ta>
            <ta e="T247" id="Seg_1965" s="T246">tɨːt-a</ta>
            <ta e="T248" id="Seg_1966" s="T247">hɨt-a-bɨn</ta>
            <ta e="T249" id="Seg_1967" s="T248">onu͡o-ga</ta>
            <ta e="T250" id="Seg_1968" s="T249">kirdik</ta>
            <ta e="T251" id="Seg_1969" s="T250">en</ta>
            <ta e="T252" id="Seg_1970" s="T251">keːs</ta>
            <ta e="T253" id="Seg_1971" s="T252">min</ta>
            <ta e="T254" id="Seg_1972" s="T253">ejigi-n</ta>
            <ta e="T255" id="Seg_1973" s="T254">törüt</ta>
            <ta e="T256" id="Seg_1974" s="T255">hir-ge-r</ta>
            <ta e="T257" id="Seg_1975" s="T256">ataːr-ɨ͡a-m</ta>
            <ta e="T258" id="Seg_1976" s="T257">di͡e-t-e</ta>
            <ta e="T259" id="Seg_1977" s="T258">emek</ta>
            <ta e="T260" id="Seg_1978" s="T259">kur-u-ska</ta>
            <ta e="T261" id="Seg_1979" s="T260">ürd-ü-ger</ta>
            <ta e="T262" id="Seg_1980" s="T261">miːn-ner-d-e</ta>
            <ta e="T263" id="Seg_1981" s="T262">on-tu-ta</ta>
            <ta e="T264" id="Seg_1982" s="T263">at</ta>
            <ta e="T265" id="Seg_1983" s="T264">bu͡ol-an</ta>
            <ta e="T266" id="Seg_1984" s="T265">köt-ön</ta>
            <ta e="T267" id="Seg_1985" s="T266">bar-bɨt</ta>
            <ta e="T268" id="Seg_1986" s="T267">öjdöː-n</ta>
            <ta e="T269" id="Seg_1987" s="T268">kör-büt-e</ta>
            <ta e="T270" id="Seg_1988" s="T269">emek</ta>
            <ta e="T271" id="Seg_1989" s="T270">mah-ɨ</ta>
            <ta e="T272" id="Seg_1990" s="T271">miːn-en</ta>
            <ta e="T273" id="Seg_1991" s="T272">baran</ta>
            <ta e="T274" id="Seg_1992" s="T273">dojdu-tu-gar</ta>
            <ta e="T275" id="Seg_1993" s="T274">hɨt-ar</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1994" s="T0">bɨlɨr</ta>
            <ta e="T2" id="Seg_1995" s="T1">biːr</ta>
            <ta e="T3" id="Seg_1996" s="T2">kihi</ta>
            <ta e="T4" id="Seg_1997" s="T3">bultan-A</ta>
            <ta e="T5" id="Seg_1998" s="T4">hɨrɨt-An</ta>
            <ta e="T6" id="Seg_1999" s="T5">hir</ta>
            <ta e="T7" id="Seg_2000" s="T6">kajagas-tI-nAn</ta>
            <ta e="T8" id="Seg_2001" s="T7">tüs-An</ta>
            <ta e="T9" id="Seg_2002" s="T8">kaːl-BIT</ta>
            <ta e="T10" id="Seg_2003" s="T9">tüs-An</ta>
            <ta e="T11" id="Seg_2004" s="T10">baran</ta>
            <ta e="T12" id="Seg_2005" s="T11">öjdöː-An</ta>
            <ta e="T13" id="Seg_2006" s="T12">kör-BIT-tA</ta>
            <ta e="T14" id="Seg_2007" s="T13">hir-GA</ta>
            <ta e="T15" id="Seg_2008" s="T14">hɨt-Ar</ta>
            <ta e="T16" id="Seg_2009" s="T15">e-BIT</ta>
            <ta e="T17" id="Seg_2010" s="T16">bu</ta>
            <ta e="T18" id="Seg_2011" s="T17">hɨt-TAK-InA</ta>
            <ta e="T19" id="Seg_2012" s="T18">üs</ta>
            <ta e="T20" id="Seg_2013" s="T19">at-LAːK</ta>
            <ta e="T21" id="Seg_2014" s="T20">kɨːs</ta>
            <ta e="T22" id="Seg_2015" s="T21">kel-BIT-LAr</ta>
            <ta e="T23" id="Seg_2016" s="T22">bu-nI</ta>
            <ta e="T24" id="Seg_2017" s="T23">min</ta>
            <ta e="T25" id="Seg_2018" s="T24">ɨ͡arɨj-Ar</ta>
            <ta e="T26" id="Seg_2019" s="T25">kihi-BIn</ta>
            <ta e="T27" id="Seg_2020" s="T26">abɨraː-ŋ</ta>
            <ta e="T28" id="Seg_2021" s="T27">di͡e-An</ta>
            <ta e="T29" id="Seg_2022" s="T28">aːttas-A</ta>
            <ta e="T30" id="Seg_2023" s="T29">hataː-BIT</ta>
            <ta e="T31" id="Seg_2024" s="T30">biːrges-LArA</ta>
            <ta e="T32" id="Seg_2025" s="T31">da</ta>
            <ta e="T33" id="Seg_2026" s="T32">ihit-I-BAT</ta>
            <ta e="T34" id="Seg_2027" s="T33">kurduk</ta>
            <ta e="T35" id="Seg_2028" s="T34">bar-An</ta>
            <ta e="T36" id="Seg_2029" s="T35">kaːl-BIT-LAr</ta>
            <ta e="T37" id="Seg_2030" s="T36">bu</ta>
            <ta e="T38" id="Seg_2031" s="T37">kɨːs-LAr</ta>
            <ta e="T39" id="Seg_2032" s="T38">ös-LArI-n</ta>
            <ta e="T40" id="Seg_2033" s="T39">ihit-I-BIT</ta>
            <ta e="T41" id="Seg_2034" s="T40">kanna</ta>
            <ta e="T42" id="Seg_2035" s="T41">bar-BIT-tA</ta>
            <ta e="T43" id="Seg_2036" s="T42">bu͡olu͡o=Ij</ta>
            <ta e="T44" id="Seg_2037" s="T43">bihigi</ta>
            <ta e="T45" id="Seg_2038" s="T44">taba-BIt</ta>
            <ta e="T46" id="Seg_2039" s="T45">di͡e-A-s-BIT-LAr</ta>
            <ta e="T47" id="Seg_2040" s="T46">bu-LAr</ta>
            <ta e="T48" id="Seg_2041" s="T47">dʼi͡e-LArI-GAr</ta>
            <ta e="T49" id="Seg_2042" s="T48">bar-BIT-LArI-GAr</ta>
            <ta e="T50" id="Seg_2043" s="T49">bat-An</ta>
            <ta e="T51" id="Seg_2044" s="T50">bar-BIT</ta>
            <ta e="T52" id="Seg_2045" s="T51">biːr</ta>
            <ta e="T53" id="Seg_2046" s="T52">dʼi͡e-GA</ta>
            <ta e="T54" id="Seg_2047" s="T53">tij-BIT</ta>
            <ta e="T55" id="Seg_2048" s="T54">ol</ta>
            <ta e="T56" id="Seg_2049" s="T55">tij-An</ta>
            <ta e="T57" id="Seg_2050" s="T56">kiːr-BIT-tA</ta>
            <ta e="T58" id="Seg_2051" s="T57">ogonnʼor-LAːK</ta>
            <ta e="T59" id="Seg_2052" s="T58">emeːksin</ta>
            <ta e="T60" id="Seg_2053" s="T59">olor-Ar-LAr</ta>
            <ta e="T61" id="Seg_2054" s="T60">e-BIT</ta>
            <ta e="T62" id="Seg_2055" s="T61">üs</ta>
            <ta e="T63" id="Seg_2056" s="T62">kɨːs-LAːK</ta>
            <ta e="T64" id="Seg_2057" s="T63">aːn-GA</ta>
            <ta e="T65" id="Seg_2058" s="T64">tur-An</ta>
            <ta e="T66" id="Seg_2059" s="T65">kör-TI-tA</ta>
            <ta e="T67" id="Seg_2060" s="T66">biːr</ta>
            <ta e="T68" id="Seg_2061" s="T67">da</ta>
            <ta e="T69" id="Seg_2062" s="T68">kihi</ta>
            <ta e="T70" id="Seg_2063" s="T69">kel-TI-ŋ</ta>
            <ta e="T71" id="Seg_2064" s="T70">eːt</ta>
            <ta e="T72" id="Seg_2065" s="T71">di͡e-An</ta>
            <ta e="T73" id="Seg_2066" s="T72">haŋar-BA-TI-tA</ta>
            <ta e="T74" id="Seg_2067" s="T73">bu-nI</ta>
            <ta e="T75" id="Seg_2068" s="T74">biːr</ta>
            <ta e="T76" id="Seg_2069" s="T75">kɨːs-GA</ta>
            <ta e="T77" id="Seg_2070" s="T76">hergestes-An</ta>
            <ta e="T78" id="Seg_2071" s="T77">olor-TI-tA</ta>
            <ta e="T79" id="Seg_2072" s="T78">bu-nI</ta>
            <ta e="T80" id="Seg_2073" s="T79">bu</ta>
            <ta e="T81" id="Seg_2074" s="T80">olor-BIT-tI-n</ta>
            <ta e="T82" id="Seg_2075" s="T81">bil-BAT-LAr</ta>
            <ta e="T83" id="Seg_2076" s="T82">ahaː-A</ta>
            <ta e="T84" id="Seg_2077" s="T83">olor-TI-LAr</ta>
            <ta e="T85" id="Seg_2078" s="T84">ol-GA</ta>
            <ta e="T86" id="Seg_2079" s="T85">ahaː</ta>
            <ta e="T87" id="Seg_2080" s="T86">di͡e-BA-TI-LAr</ta>
            <ta e="T88" id="Seg_2081" s="T87">bu-nI</ta>
            <ta e="T89" id="Seg_2082" s="T88">beje-tA</ta>
            <ta e="T90" id="Seg_2083" s="T89">biːr</ta>
            <ta e="T91" id="Seg_2084" s="T90">eme</ta>
            <ta e="T92" id="Seg_2085" s="T91">lu͡osku-kAːN-nI</ta>
            <ta e="T93" id="Seg_2086" s="T92">ɨl-An</ta>
            <ta e="T94" id="Seg_2087" s="T93">hi͡e-Ar</ta>
            <ta e="T95" id="Seg_2088" s="T94">ol-nI</ta>
            <ta e="T96" id="Seg_2089" s="T95">kör-BAT-LAr</ta>
            <ta e="T97" id="Seg_2090" s="T96">togo</ta>
            <ta e="T98" id="Seg_2091" s="T97">as-BIt</ta>
            <ta e="T99" id="Seg_2092" s="T98">agɨjaː-Ar</ta>
            <ta e="T100" id="Seg_2093" s="T99">ere</ta>
            <ta e="T101" id="Seg_2094" s="T100">di͡e-A-s-Ar-LAr</ta>
            <ta e="T102" id="Seg_2095" s="T101">onton</ta>
            <ta e="T103" id="Seg_2096" s="T102">hanaː-TI-tA</ta>
            <ta e="T104" id="Seg_2097" s="T103">min-n</ta>
            <ta e="T105" id="Seg_2098" s="T104">kör-BAT-LAr</ta>
            <ta e="T106" id="Seg_2099" s="T105">bɨhɨːlaːk</ta>
            <ta e="T107" id="Seg_2100" s="T106">min</ta>
            <ta e="T108" id="Seg_2101" s="T107">tɨːt-An</ta>
            <ta e="T109" id="Seg_2102" s="T108">kör-Iːm</ta>
            <ta e="T110" id="Seg_2103" s="T109">bar-An</ta>
            <ta e="T111" id="Seg_2104" s="T110">biːr</ta>
            <ta e="T112" id="Seg_2105" s="T111">kɨːs-GA</ta>
            <ta e="T113" id="Seg_2106" s="T112">herge</ta>
            <ta e="T114" id="Seg_2107" s="T113">olor-An</ta>
            <ta e="T115" id="Seg_2108" s="T114">tɨːt-Ar</ta>
            <ta e="T116" id="Seg_2109" s="T115">bu-GA</ta>
            <ta e="T117" id="Seg_2110" s="T116">öl-Ar</ta>
            <ta e="T118" id="Seg_2111" s="T117">haŋa-tA</ta>
            <ta e="T119" id="Seg_2112" s="T118">tagɨs-Ar</ta>
            <ta e="T120" id="Seg_2113" s="T119">bugurduk</ta>
            <ta e="T121" id="Seg_2114" s="T120">üs</ta>
            <ta e="T122" id="Seg_2115" s="T121">konuk-nI</ta>
            <ta e="T123" id="Seg_2116" s="T122">hɨrɨt-TI-tA</ta>
            <ta e="T124" id="Seg_2117" s="T123">bu</ta>
            <ta e="T125" id="Seg_2118" s="T124">hɨrɨt-An</ta>
            <ta e="T126" id="Seg_2119" s="T125">dumaj-LAN-Ar</ta>
            <ta e="T127" id="Seg_2120" s="T126">min</ta>
            <ta e="T128" id="Seg_2121" s="T127">bu</ta>
            <ta e="T129" id="Seg_2122" s="T128">dojdu-ttAn</ta>
            <ta e="T130" id="Seg_2123" s="T129">bar-Ar</ta>
            <ta e="T131" id="Seg_2124" s="T130">keskil-BI-n</ta>
            <ta e="T132" id="Seg_2125" s="T131">oŋohun-IAK-LAːK-BIn</ta>
            <ta e="T133" id="Seg_2126" s="T132">di͡e-Ar</ta>
            <ta e="T134" id="Seg_2127" s="T133">biːr</ta>
            <ta e="T135" id="Seg_2128" s="T134">kɨːs-ttAn</ta>
            <ta e="T136" id="Seg_2129" s="T135">dʼe</ta>
            <ta e="T137" id="Seg_2130" s="T136">araːk-BAT</ta>
            <ta e="T138" id="Seg_2131" s="T137">bu͡ol-Ar</ta>
            <ta e="T139" id="Seg_2132" s="T138">bu-GA</ta>
            <ta e="T140" id="Seg_2133" s="T139">ikki</ta>
            <ta e="T141" id="Seg_2134" s="T140">ojun-nI</ta>
            <ta e="T142" id="Seg_2135" s="T141">kɨːr-TAr-TI-LAr</ta>
            <ta e="T143" id="Seg_2136" s="T142">bu</ta>
            <ta e="T144" id="Seg_2137" s="T143">ojun-LAr</ta>
            <ta e="T145" id="Seg_2138" s="T144">tu͡ok</ta>
            <ta e="T146" id="Seg_2139" s="T145">da</ta>
            <ta e="T147" id="Seg_2140" s="T146">kömö-nI</ta>
            <ta e="T148" id="Seg_2141" s="T147">bi͡er-BA-TI-LAr</ta>
            <ta e="T149" id="Seg_2142" s="T148">bu-nI</ta>
            <ta e="T150" id="Seg_2143" s="T149">et-TI-LAr</ta>
            <ta e="T151" id="Seg_2144" s="T150">ulakan</ta>
            <ta e="T152" id="Seg_2145" s="T151">ojun-nI</ta>
            <ta e="T153" id="Seg_2146" s="T152">egel-TAr-IAK-GA</ta>
            <ta e="T154" id="Seg_2147" s="T153">ol</ta>
            <ta e="T155" id="Seg_2148" s="T154">ojun-LArI-GAr</ta>
            <ta e="T156" id="Seg_2149" s="T155">bar-TI-LAr</ta>
            <ta e="T157" id="Seg_2150" s="T156">bu-nI</ta>
            <ta e="T158" id="Seg_2151" s="T157">bar-BIT-LArI-n</ta>
            <ta e="T159" id="Seg_2152" s="T158">genne</ta>
            <ta e="T160" id="Seg_2153" s="T159">u͡ol</ta>
            <ta e="T161" id="Seg_2154" s="T160">horujan</ta>
            <ta e="T162" id="Seg_2155" s="T161">kuːs-Ar</ta>
            <ta e="T163" id="Seg_2156" s="T162">kɨːs-nI</ta>
            <ta e="T164" id="Seg_2157" s="T163">ol-tI-tA</ta>
            <ta e="T165" id="Seg_2158" s="T164">öl-Ar</ta>
            <ta e="T166" id="Seg_2159" s="T165">haŋa-tA</ta>
            <ta e="T167" id="Seg_2160" s="T166">tagɨs-Ar</ta>
            <ta e="T168" id="Seg_2161" s="T167">bu</ta>
            <ta e="T169" id="Seg_2162" s="T168">hɨrɨt-TAK-InA</ta>
            <ta e="T170" id="Seg_2163" s="T169">ikki</ta>
            <ta e="T171" id="Seg_2164" s="T170">karak</ta>
            <ta e="T172" id="Seg_2165" s="T171">u͡ot-tA</ta>
            <ta e="T173" id="Seg_2166" s="T172">huptu</ta>
            <ta e="T174" id="Seg_2167" s="T173">gini-GA</ta>
            <ta e="T175" id="Seg_2168" s="T174">tüs-TI-tA</ta>
            <ta e="T176" id="Seg_2169" s="T175">ojun-BIt</ta>
            <ta e="T177" id="Seg_2170" s="T176">is-Ar</ta>
            <ta e="T178" id="Seg_2171" s="T177">di͡e-A-s-TI-LAr</ta>
            <ta e="T179" id="Seg_2172" s="T178">bu-GA</ta>
            <ta e="T180" id="Seg_2173" s="T179">kajdak</ta>
            <ta e="T181" id="Seg_2174" s="T180">gɨn-Ar</ta>
            <ta e="T182" id="Seg_2175" s="T181">e-BIT</ta>
            <ta e="T183" id="Seg_2176" s="T182">di͡e-An</ta>
            <ta e="T184" id="Seg_2177" s="T183">kɨːs-tI-n</ta>
            <ta e="T185" id="Seg_2178" s="T184">poduːska-tI-n</ta>
            <ta e="T186" id="Seg_2179" s="T185">alɨn-tI-GAr</ta>
            <ta e="T187" id="Seg_2180" s="T186">has-An</ta>
            <ta e="T188" id="Seg_2181" s="T187">kaːl-TI-tA</ta>
            <ta e="T189" id="Seg_2182" s="T188">huːlan-A</ta>
            <ta e="T190" id="Seg_2183" s="T189">hataː-TI-tA</ta>
            <ta e="T191" id="Seg_2184" s="T190">da</ta>
            <ta e="T192" id="Seg_2185" s="T191">ol</ta>
            <ta e="T193" id="Seg_2186" s="T192">ojun</ta>
            <ta e="T194" id="Seg_2187" s="T193">karak-tI-n</ta>
            <ta e="T195" id="Seg_2188" s="T194">u͡ot-tA</ta>
            <ta e="T196" id="Seg_2189" s="T195">huptu</ta>
            <ta e="T197" id="Seg_2190" s="T196">kör-Ar</ta>
            <ta e="T198" id="Seg_2191" s="T197">kiːr-AːT</ta>
            <ta e="T199" id="Seg_2192" s="T198">ojun-tA</ta>
            <ta e="T200" id="Seg_2193" s="T199">ürüt-tI-GAr</ta>
            <ta e="T201" id="Seg_2194" s="T200">tüs-TI-tA</ta>
            <ta e="T202" id="Seg_2195" s="T201">tüs-An</ta>
            <ta e="T203" id="Seg_2196" s="T202">kögüs-tI-GAr</ta>
            <ta e="T204" id="Seg_2197" s="T203">olor-TI-tA</ta>
            <ta e="T205" id="Seg_2198" s="T204">bu-nI</ta>
            <ta e="T206" id="Seg_2199" s="T205">ulakan-LIk</ta>
            <ta e="T207" id="Seg_2200" s="T206">mögüs-TI-tA</ta>
            <ta e="T208" id="Seg_2201" s="T207">onno</ta>
            <ta e="T209" id="Seg_2202" s="T208">ojun-tA</ta>
            <ta e="T210" id="Seg_2203" s="T209">ɨːt-BAT</ta>
            <ta e="T211" id="Seg_2204" s="T210">ol</ta>
            <ta e="T212" id="Seg_2205" s="T211">gɨn-An</ta>
            <ta e="T213" id="Seg_2206" s="T212">baran</ta>
            <ta e="T214" id="Seg_2207" s="T213">ös-LAːK</ta>
            <ta e="T215" id="Seg_2208" s="T214">bu͡ol-TI-tA</ta>
            <ta e="T216" id="Seg_2209" s="T215">en</ta>
            <ta e="T217" id="Seg_2210" s="T216">bu</ta>
            <ta e="T218" id="Seg_2211" s="T217">tu͡ok</ta>
            <ta e="T219" id="Seg_2212" s="T218">bu͡ol-An</ta>
            <ta e="T220" id="Seg_2213" s="T219">kihi-nI</ta>
            <ta e="T221" id="Seg_2214" s="T220">mu͡okastaː-A</ta>
            <ta e="T222" id="Seg_2215" s="T221">hɨt-A-GIn</ta>
            <ta e="T223" id="Seg_2216" s="T222">di͡e-An</ta>
            <ta e="T224" id="Seg_2217" s="T223">bu-nI</ta>
            <ta e="T225" id="Seg_2218" s="T224">kihi-tA</ta>
            <ta e="T226" id="Seg_2219" s="T225">kepseː-TI-tA</ta>
            <ta e="T227" id="Seg_2220" s="T226">min</ta>
            <ta e="T228" id="Seg_2221" s="T227">beje-m</ta>
            <ta e="T229" id="Seg_2222" s="T228">köŋül-BI-nAn</ta>
            <ta e="T230" id="Seg_2223" s="T229">kel-BAtAK-I-m</ta>
            <ta e="T231" id="Seg_2224" s="T230">hir</ta>
            <ta e="T232" id="Seg_2225" s="T231">kajagas-tI-nAn</ta>
            <ta e="T233" id="Seg_2226" s="T232">tüs-An</ta>
            <ta e="T234" id="Seg_2227" s="T233">min</ta>
            <ta e="T235" id="Seg_2228" s="T234">ɨ͡arɨj-A</ta>
            <ta e="T236" id="Seg_2229" s="T235">hɨt-An-BIn</ta>
            <ta e="T237" id="Seg_2230" s="T236">bu</ta>
            <ta e="T238" id="Seg_2231" s="T237">üs</ta>
            <ta e="T239" id="Seg_2232" s="T238">kɨːs-nI</ta>
            <ta e="T240" id="Seg_2233" s="T239">kördöː-A-s-BIT-BA-r</ta>
            <ta e="T241" id="Seg_2234" s="T240">biːrges-LArA</ta>
            <ta e="T242" id="Seg_2235" s="T241">da</ta>
            <ta e="T243" id="Seg_2236" s="T242">kömölös-BAtAK-tA</ta>
            <ta e="T244" id="Seg_2237" s="T243">ol</ta>
            <ta e="T245" id="Seg_2238" s="T244">tuh-tI-ttAn</ta>
            <ta e="T246" id="Seg_2239" s="T245">anaːn</ta>
            <ta e="T247" id="Seg_2240" s="T246">tɨːt-A</ta>
            <ta e="T248" id="Seg_2241" s="T247">hɨt-A-BIn</ta>
            <ta e="T249" id="Seg_2242" s="T248">ol-GA</ta>
            <ta e="T250" id="Seg_2243" s="T249">kirdik</ta>
            <ta e="T251" id="Seg_2244" s="T250">en</ta>
            <ta e="T252" id="Seg_2245" s="T251">keːs</ta>
            <ta e="T253" id="Seg_2246" s="T252">min</ta>
            <ta e="T254" id="Seg_2247" s="T253">en-n</ta>
            <ta e="T255" id="Seg_2248" s="T254">törüt</ta>
            <ta e="T256" id="Seg_2249" s="T255">hir-GA-r</ta>
            <ta e="T257" id="Seg_2250" s="T256">ataːr-IAK-m</ta>
            <ta e="T258" id="Seg_2251" s="T257">di͡e-TI-tA</ta>
            <ta e="T259" id="Seg_2252" s="T258">emek</ta>
            <ta e="T260" id="Seg_2253" s="T259">kur-I-skA</ta>
            <ta e="T261" id="Seg_2254" s="T260">ürüt-tI-GAr</ta>
            <ta e="T262" id="Seg_2255" s="T261">miːn-TAr-TI-tA</ta>
            <ta e="T263" id="Seg_2256" s="T262">ol-tI-tA</ta>
            <ta e="T264" id="Seg_2257" s="T263">at</ta>
            <ta e="T265" id="Seg_2258" s="T264">bu͡ol-An</ta>
            <ta e="T266" id="Seg_2259" s="T265">köt-An</ta>
            <ta e="T267" id="Seg_2260" s="T266">bar-BIT</ta>
            <ta e="T268" id="Seg_2261" s="T267">öjdöː-An</ta>
            <ta e="T269" id="Seg_2262" s="T268">kör-BIT-tA</ta>
            <ta e="T270" id="Seg_2263" s="T269">emek</ta>
            <ta e="T271" id="Seg_2264" s="T270">mas-nI</ta>
            <ta e="T272" id="Seg_2265" s="T271">miːn-An</ta>
            <ta e="T273" id="Seg_2266" s="T272">baran</ta>
            <ta e="T274" id="Seg_2267" s="T273">dojdu-tI-GAr</ta>
            <ta e="T275" id="Seg_2268" s="T274">hɨt-Ar</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_2269" s="T0">long.ago</ta>
            <ta e="T2" id="Seg_2270" s="T1">one</ta>
            <ta e="T3" id="Seg_2271" s="T2">human.being.[NOM]</ta>
            <ta e="T4" id="Seg_2272" s="T3">hunt-CVB.SIM</ta>
            <ta e="T5" id="Seg_2273" s="T4">go-CVB.SEQ</ta>
            <ta e="T6" id="Seg_2274" s="T5">earth.[NOM]</ta>
            <ta e="T7" id="Seg_2275" s="T6">hole-3SG-INSTR</ta>
            <ta e="T8" id="Seg_2276" s="T7">fall-CVB.SEQ</ta>
            <ta e="T9" id="Seg_2277" s="T8">stay-PST2.[3SG]</ta>
            <ta e="T10" id="Seg_2278" s="T9">fall-CVB.SEQ</ta>
            <ta e="T11" id="Seg_2279" s="T10">after</ta>
            <ta e="T12" id="Seg_2280" s="T11">remember-CVB.SEQ</ta>
            <ta e="T13" id="Seg_2281" s="T12">see-PST2-3SG</ta>
            <ta e="T14" id="Seg_2282" s="T13">earth-DAT/LOC</ta>
            <ta e="T15" id="Seg_2283" s="T14">lie-PTCP.PRS</ta>
            <ta e="T16" id="Seg_2284" s="T15">be-PST2.[3SG]</ta>
            <ta e="T17" id="Seg_2285" s="T16">this</ta>
            <ta e="T18" id="Seg_2286" s="T17">lie-TEMP-3SG</ta>
            <ta e="T19" id="Seg_2287" s="T18">three</ta>
            <ta e="T20" id="Seg_2288" s="T19">horse-PROPR</ta>
            <ta e="T21" id="Seg_2289" s="T20">girl.[NOM]</ta>
            <ta e="T22" id="Seg_2290" s="T21">come-PST2-3PL</ta>
            <ta e="T23" id="Seg_2291" s="T22">this-ACC</ta>
            <ta e="T24" id="Seg_2292" s="T23">1SG.[NOM]</ta>
            <ta e="T25" id="Seg_2293" s="T24">be.sick-PTCP.PRS</ta>
            <ta e="T26" id="Seg_2294" s="T25">human.being-1SG</ta>
            <ta e="T27" id="Seg_2295" s="T26">save-IMP.2PL</ta>
            <ta e="T28" id="Seg_2296" s="T27">say-CVB.SEQ</ta>
            <ta e="T29" id="Seg_2297" s="T28">beg-CVB.SIM</ta>
            <ta e="T30" id="Seg_2298" s="T29">do.in.vain-PST2.[3SG]</ta>
            <ta e="T31" id="Seg_2299" s="T30">other-3PL.[NOM]</ta>
            <ta e="T32" id="Seg_2300" s="T31">NEG</ta>
            <ta e="T33" id="Seg_2301" s="T32">hear-EP-NEG.PTCP.[NOM]</ta>
            <ta e="T34" id="Seg_2302" s="T33">like</ta>
            <ta e="T35" id="Seg_2303" s="T34">go-CVB.SEQ</ta>
            <ta e="T36" id="Seg_2304" s="T35">stay-PST2-3PL</ta>
            <ta e="T37" id="Seg_2305" s="T36">this</ta>
            <ta e="T38" id="Seg_2306" s="T37">girl-PL.[NOM]</ta>
            <ta e="T39" id="Seg_2307" s="T38">word-3PL-ACC</ta>
            <ta e="T40" id="Seg_2308" s="T39">hear-EP-PST2.[3SG]</ta>
            <ta e="T41" id="Seg_2309" s="T40">whereto</ta>
            <ta e="T42" id="Seg_2310" s="T41">go-PST2-3SG</ta>
            <ta e="T43" id="Seg_2311" s="T42">probably=Q</ta>
            <ta e="T44" id="Seg_2312" s="T43">1PL.[NOM]</ta>
            <ta e="T45" id="Seg_2313" s="T44">reindeer-1PL.[NOM]</ta>
            <ta e="T46" id="Seg_2314" s="T45">say-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T47" id="Seg_2315" s="T46">this-PL.[NOM]</ta>
            <ta e="T48" id="Seg_2316" s="T47">house-3PL-DAT/LOC</ta>
            <ta e="T49" id="Seg_2317" s="T48">go-PTCP.PST-3PL-DAT/LOC</ta>
            <ta e="T50" id="Seg_2318" s="T49">follow-CVB.SEQ</ta>
            <ta e="T51" id="Seg_2319" s="T50">go-PST2.[3SG]</ta>
            <ta e="T52" id="Seg_2320" s="T51">one</ta>
            <ta e="T53" id="Seg_2321" s="T52">house-DAT/LOC</ta>
            <ta e="T54" id="Seg_2322" s="T53">reach-PST2.[3SG]</ta>
            <ta e="T55" id="Seg_2323" s="T54">that</ta>
            <ta e="T56" id="Seg_2324" s="T55">reach-CVB.SEQ</ta>
            <ta e="T57" id="Seg_2325" s="T56">go.in-PST2-3SG</ta>
            <ta e="T58" id="Seg_2326" s="T57">old.man-PROPR</ta>
            <ta e="T59" id="Seg_2327" s="T58">old.woman.[NOM]</ta>
            <ta e="T60" id="Seg_2328" s="T59">live-PRS-3PL</ta>
            <ta e="T61" id="Seg_2329" s="T60">be-PST2.[3SG]</ta>
            <ta e="T62" id="Seg_2330" s="T61">three</ta>
            <ta e="T63" id="Seg_2331" s="T62">daughter-PROPR.[NOM]</ta>
            <ta e="T64" id="Seg_2332" s="T63">door-DAT/LOC</ta>
            <ta e="T65" id="Seg_2333" s="T64">stand-CVB.SEQ</ta>
            <ta e="T66" id="Seg_2334" s="T65">see-PST1-3SG</ta>
            <ta e="T67" id="Seg_2335" s="T66">one</ta>
            <ta e="T68" id="Seg_2336" s="T67">NEG</ta>
            <ta e="T69" id="Seg_2337" s="T68">human.being.[NOM]</ta>
            <ta e="T70" id="Seg_2338" s="T69">come-PST1-2SG</ta>
            <ta e="T71" id="Seg_2339" s="T70">EVID</ta>
            <ta e="T72" id="Seg_2340" s="T71">say-CVB.SEQ</ta>
            <ta e="T73" id="Seg_2341" s="T72">speak-NEG-PST1-3SG</ta>
            <ta e="T74" id="Seg_2342" s="T73">this-ACC</ta>
            <ta e="T75" id="Seg_2343" s="T74">one</ta>
            <ta e="T76" id="Seg_2344" s="T75">girl-DAT/LOC</ta>
            <ta e="T77" id="Seg_2345" s="T76">come.close-CVB.SEQ</ta>
            <ta e="T78" id="Seg_2346" s="T77">sit.down-PST1-3SG</ta>
            <ta e="T79" id="Seg_2347" s="T78">this-ACC</ta>
            <ta e="T80" id="Seg_2348" s="T79">this</ta>
            <ta e="T81" id="Seg_2349" s="T80">sit.down-PTCP.PST-3SG-ACC</ta>
            <ta e="T82" id="Seg_2350" s="T81">notice-NEG-3PL</ta>
            <ta e="T83" id="Seg_2351" s="T82">eat-CVB.SIM</ta>
            <ta e="T84" id="Seg_2352" s="T83">sit-PST1-3PL</ta>
            <ta e="T85" id="Seg_2353" s="T84">that-DAT/LOC</ta>
            <ta e="T86" id="Seg_2354" s="T85">eat.[IMP.2SG]</ta>
            <ta e="T87" id="Seg_2355" s="T86">say-NEG-PST1-3PL</ta>
            <ta e="T88" id="Seg_2356" s="T87">this-ACC</ta>
            <ta e="T89" id="Seg_2357" s="T88">self-3SG.[NOM]</ta>
            <ta e="T90" id="Seg_2358" s="T89">one</ta>
            <ta e="T91" id="Seg_2359" s="T90">INDEF</ta>
            <ta e="T92" id="Seg_2360" s="T91">spoon-DIM-ACC</ta>
            <ta e="T93" id="Seg_2361" s="T92">take-CVB.SEQ</ta>
            <ta e="T94" id="Seg_2362" s="T93">eat-PRS.[3SG]</ta>
            <ta e="T95" id="Seg_2363" s="T94">that-ACC</ta>
            <ta e="T96" id="Seg_2364" s="T95">see-NEG-3PL</ta>
            <ta e="T97" id="Seg_2365" s="T96">why</ta>
            <ta e="T98" id="Seg_2366" s="T97">food-1PL.[NOM]</ta>
            <ta e="T99" id="Seg_2367" s="T98">decrease-PRS.[3SG]</ta>
            <ta e="T100" id="Seg_2368" s="T99">just</ta>
            <ta e="T101" id="Seg_2369" s="T100">say-EP-RECP/COLL-PRS-3PL</ta>
            <ta e="T102" id="Seg_2370" s="T101">then</ta>
            <ta e="T103" id="Seg_2371" s="T102">think-PST1-3SG</ta>
            <ta e="T104" id="Seg_2372" s="T103">1SG-ACC</ta>
            <ta e="T105" id="Seg_2373" s="T104">see-NEG-3PL</ta>
            <ta e="T106" id="Seg_2374" s="T105">apparently</ta>
            <ta e="T107" id="Seg_2375" s="T106">1SG.[NOM]</ta>
            <ta e="T108" id="Seg_2376" s="T107">touch-CVB.SEQ</ta>
            <ta e="T109" id="Seg_2377" s="T108">try-IMP.1SG</ta>
            <ta e="T110" id="Seg_2378" s="T109">go-CVB.SEQ</ta>
            <ta e="T111" id="Seg_2379" s="T110">one</ta>
            <ta e="T112" id="Seg_2380" s="T111">girl-DAT/LOC</ta>
            <ta e="T113" id="Seg_2381" s="T112">beneath</ta>
            <ta e="T114" id="Seg_2382" s="T113">sit-CVB.SEQ</ta>
            <ta e="T115" id="Seg_2383" s="T114">touch-PRS.[3SG]</ta>
            <ta e="T116" id="Seg_2384" s="T115">this-DAT/LOC</ta>
            <ta e="T117" id="Seg_2385" s="T116">die-PTCP.PRS</ta>
            <ta e="T118" id="Seg_2386" s="T117">voice-3SG.[NOM]</ta>
            <ta e="T119" id="Seg_2387" s="T118">go.out-PRS.[3SG]</ta>
            <ta e="T120" id="Seg_2388" s="T119">like.this</ta>
            <ta e="T121" id="Seg_2389" s="T120">three</ta>
            <ta e="T122" id="Seg_2390" s="T121">day.and.night-ACC</ta>
            <ta e="T123" id="Seg_2391" s="T122">go-PST1-3SG</ta>
            <ta e="T124" id="Seg_2392" s="T123">this</ta>
            <ta e="T125" id="Seg_2393" s="T124">go-CVB.SEQ</ta>
            <ta e="T126" id="Seg_2394" s="T125">think-VBZ-PRS.[3SG]</ta>
            <ta e="T127" id="Seg_2395" s="T126">1SG.[NOM]</ta>
            <ta e="T128" id="Seg_2396" s="T127">this</ta>
            <ta e="T129" id="Seg_2397" s="T128">country-ABL</ta>
            <ta e="T130" id="Seg_2398" s="T129">go-PTCP.PRS</ta>
            <ta e="T131" id="Seg_2399" s="T130">perspective-1SG-ACC</ta>
            <ta e="T132" id="Seg_2400" s="T131">make-FUT-NEC-1SG</ta>
            <ta e="T133" id="Seg_2401" s="T132">think-PRS.[3SG]</ta>
            <ta e="T134" id="Seg_2402" s="T133">one</ta>
            <ta e="T135" id="Seg_2403" s="T134">girl-ABL</ta>
            <ta e="T136" id="Seg_2404" s="T135">well</ta>
            <ta e="T137" id="Seg_2405" s="T136">go.away-NEG.PTCP</ta>
            <ta e="T138" id="Seg_2406" s="T137">be-PRS.[3SG]</ta>
            <ta e="T139" id="Seg_2407" s="T138">this-DAT/LOC</ta>
            <ta e="T140" id="Seg_2408" s="T139">two</ta>
            <ta e="T141" id="Seg_2409" s="T140">shaman-ACC</ta>
            <ta e="T142" id="Seg_2410" s="T141">shamanize-CAUS-PST1-3PL</ta>
            <ta e="T143" id="Seg_2411" s="T142">this</ta>
            <ta e="T144" id="Seg_2412" s="T143">shaman-PL.[NOM]</ta>
            <ta e="T145" id="Seg_2413" s="T144">what.[NOM]</ta>
            <ta e="T146" id="Seg_2414" s="T145">NEG</ta>
            <ta e="T147" id="Seg_2415" s="T146">help-ACC</ta>
            <ta e="T148" id="Seg_2416" s="T147">give-NEG-PST1-3PL</ta>
            <ta e="T149" id="Seg_2417" s="T148">this-ACC</ta>
            <ta e="T150" id="Seg_2418" s="T149">speak-PST1-3PL</ta>
            <ta e="T151" id="Seg_2419" s="T150">big</ta>
            <ta e="T152" id="Seg_2420" s="T151">shaman-ACC</ta>
            <ta e="T153" id="Seg_2421" s="T152">bring-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T154" id="Seg_2422" s="T153">that</ta>
            <ta e="T155" id="Seg_2423" s="T154">shaman-3PL-DAT/LOC</ta>
            <ta e="T156" id="Seg_2424" s="T155">go-PST1-3PL</ta>
            <ta e="T157" id="Seg_2425" s="T156">this-ACC</ta>
            <ta e="T158" id="Seg_2426" s="T157">go-PTCP.PST-3PL-ACC</ta>
            <ta e="T159" id="Seg_2427" s="T158">after</ta>
            <ta e="T160" id="Seg_2428" s="T159">boy.[NOM]</ta>
            <ta e="T161" id="Seg_2429" s="T160">on.purpose</ta>
            <ta e="T162" id="Seg_2430" s="T161">embrace-PRS.[3SG]</ta>
            <ta e="T163" id="Seg_2431" s="T162">girl-ACC</ta>
            <ta e="T164" id="Seg_2432" s="T163">that-3SG-3SG.[NOM]</ta>
            <ta e="T165" id="Seg_2433" s="T164">die-PTCP.PRS</ta>
            <ta e="T166" id="Seg_2434" s="T165">voice-3SG.[NOM]</ta>
            <ta e="T167" id="Seg_2435" s="T166">go.out-PRS.[3SG]</ta>
            <ta e="T168" id="Seg_2436" s="T167">this</ta>
            <ta e="T169" id="Seg_2437" s="T168">go-TEMP-3SG</ta>
            <ta e="T170" id="Seg_2438" s="T169">two</ta>
            <ta e="T171" id="Seg_2439" s="T170">eye.[NOM]</ta>
            <ta e="T172" id="Seg_2440" s="T171">beam-3SG.[NOM]</ta>
            <ta e="T173" id="Seg_2441" s="T172">straight</ta>
            <ta e="T174" id="Seg_2442" s="T173">3SG-DAT/LOC</ta>
            <ta e="T175" id="Seg_2443" s="T174">fall-PST1-3SG</ta>
            <ta e="T176" id="Seg_2444" s="T175">shaman-1PL.[NOM]</ta>
            <ta e="T177" id="Seg_2445" s="T176">go-PRS.[3SG]</ta>
            <ta e="T178" id="Seg_2446" s="T177">say-EP-RECP/COLL-PST1-3PL</ta>
            <ta e="T179" id="Seg_2447" s="T178">this-DAT/LOC</ta>
            <ta e="T180" id="Seg_2448" s="T179">how</ta>
            <ta e="T181" id="Seg_2449" s="T180">make-PTCP.PRS</ta>
            <ta e="T182" id="Seg_2450" s="T181">be-PST2.[3SG]</ta>
            <ta e="T183" id="Seg_2451" s="T182">think-CVB.SEQ</ta>
            <ta e="T184" id="Seg_2452" s="T183">girl-3SG-GEN</ta>
            <ta e="T185" id="Seg_2453" s="T184">pillow-3SG-GEN</ta>
            <ta e="T186" id="Seg_2454" s="T185">lower.part-3SG-DAT/LOC</ta>
            <ta e="T187" id="Seg_2455" s="T186">hide-CVB.SEQ</ta>
            <ta e="T188" id="Seg_2456" s="T187">stay-PST1-3SG</ta>
            <ta e="T189" id="Seg_2457" s="T188">tuck.oneself.up-CVB.SIM</ta>
            <ta e="T190" id="Seg_2458" s="T189">not.manage-PST1-3SG</ta>
            <ta e="T191" id="Seg_2459" s="T190">and</ta>
            <ta e="T192" id="Seg_2460" s="T191">that</ta>
            <ta e="T193" id="Seg_2461" s="T192">shaman.[NOM]</ta>
            <ta e="T194" id="Seg_2462" s="T193">eye-3SG-GEN</ta>
            <ta e="T195" id="Seg_2463" s="T194">beam-3SG.[NOM]</ta>
            <ta e="T196" id="Seg_2464" s="T195">straight</ta>
            <ta e="T197" id="Seg_2465" s="T196">see-PRS.[3SG]</ta>
            <ta e="T198" id="Seg_2466" s="T197">go.in-CVB.ANT</ta>
            <ta e="T199" id="Seg_2467" s="T198">shaman-3SG.[NOM]</ta>
            <ta e="T200" id="Seg_2468" s="T199">upper.part-3SG-DAT/LOC</ta>
            <ta e="T201" id="Seg_2469" s="T200">fall-PST1-3SG</ta>
            <ta e="T202" id="Seg_2470" s="T201">fall-CVB.SEQ</ta>
            <ta e="T203" id="Seg_2471" s="T202">back-3SG-DAT/LOC</ta>
            <ta e="T204" id="Seg_2472" s="T203">sit.down-PST1-3SG</ta>
            <ta e="T205" id="Seg_2473" s="T204">this-ACC</ta>
            <ta e="T206" id="Seg_2474" s="T205">big-ADVZ</ta>
            <ta e="T207" id="Seg_2475" s="T206">fight-PST1-3SG</ta>
            <ta e="T208" id="Seg_2476" s="T207">there</ta>
            <ta e="T209" id="Seg_2477" s="T208">shaman-3SG.[NOM]</ta>
            <ta e="T210" id="Seg_2478" s="T209">release-NEG.[3SG]</ta>
            <ta e="T211" id="Seg_2479" s="T210">that</ta>
            <ta e="T212" id="Seg_2480" s="T211">make-CVB.SEQ</ta>
            <ta e="T213" id="Seg_2481" s="T212">after</ta>
            <ta e="T214" id="Seg_2482" s="T213">word-PROPR.[NOM]</ta>
            <ta e="T215" id="Seg_2483" s="T214">be-PST1-3SG</ta>
            <ta e="T216" id="Seg_2484" s="T215">2SG.[NOM]</ta>
            <ta e="T217" id="Seg_2485" s="T216">this</ta>
            <ta e="T218" id="Seg_2486" s="T217">what.[NOM]</ta>
            <ta e="T219" id="Seg_2487" s="T218">be-CVB.SEQ</ta>
            <ta e="T220" id="Seg_2488" s="T219">human.being-ACC</ta>
            <ta e="T221" id="Seg_2489" s="T220">torture-CVB.SIM</ta>
            <ta e="T222" id="Seg_2490" s="T221">lie-PRS-2SG</ta>
            <ta e="T223" id="Seg_2491" s="T222">say-CVB.SEQ</ta>
            <ta e="T224" id="Seg_2492" s="T223">this-ACC</ta>
            <ta e="T225" id="Seg_2493" s="T224">human.being-3SG.[NOM]</ta>
            <ta e="T226" id="Seg_2494" s="T225">tell-PST1-3SG</ta>
            <ta e="T227" id="Seg_2495" s="T226">1SG.[NOM]</ta>
            <ta e="T228" id="Seg_2496" s="T227">self-1SG.[NOM]</ta>
            <ta e="T229" id="Seg_2497" s="T228">will-1SG-INSTR</ta>
            <ta e="T230" id="Seg_2498" s="T229">come-PST2.NEG-EP-1SG</ta>
            <ta e="T231" id="Seg_2499" s="T230">earth.[NOM]</ta>
            <ta e="T232" id="Seg_2500" s="T231">hole-3SG-INSTR</ta>
            <ta e="T233" id="Seg_2501" s="T232">fall-CVB.SEQ</ta>
            <ta e="T234" id="Seg_2502" s="T233">1SG.[NOM]</ta>
            <ta e="T235" id="Seg_2503" s="T234">be.sick-CVB.SIM</ta>
            <ta e="T236" id="Seg_2504" s="T235">lie-CVB.SEQ-1SG</ta>
            <ta e="T237" id="Seg_2505" s="T236">this</ta>
            <ta e="T238" id="Seg_2506" s="T237">three</ta>
            <ta e="T239" id="Seg_2507" s="T238">girl-ACC</ta>
            <ta e="T240" id="Seg_2508" s="T239">beg-EP-MED-PTCP.PST-1SG-DAT/LOC</ta>
            <ta e="T241" id="Seg_2509" s="T240">other-3PL.[NOM]</ta>
            <ta e="T242" id="Seg_2510" s="T241">NEG</ta>
            <ta e="T243" id="Seg_2511" s="T242">help-PST2.NEG-3SG</ta>
            <ta e="T244" id="Seg_2512" s="T243">that.[NOM]</ta>
            <ta e="T245" id="Seg_2513" s="T244">side-3SG-ABL</ta>
            <ta e="T246" id="Seg_2514" s="T245">on.purpose</ta>
            <ta e="T247" id="Seg_2515" s="T246">touch-CVB.SIM</ta>
            <ta e="T248" id="Seg_2516" s="T247">lie-PRS-1SG</ta>
            <ta e="T249" id="Seg_2517" s="T248">that-DAT/LOC</ta>
            <ta e="T250" id="Seg_2518" s="T249">truth.[NOM]</ta>
            <ta e="T251" id="Seg_2519" s="T250">2SG.[NOM]</ta>
            <ta e="T252" id="Seg_2520" s="T251">let.[IMP.2SG]</ta>
            <ta e="T253" id="Seg_2521" s="T252">1SG.[NOM]</ta>
            <ta e="T254" id="Seg_2522" s="T253">2SG-ACC</ta>
            <ta e="T255" id="Seg_2523" s="T254">ancestor.[NOM]</ta>
            <ta e="T256" id="Seg_2524" s="T255">place-2SG-DAT/LOC</ta>
            <ta e="T257" id="Seg_2525" s="T256">accompany-FUT-1SG</ta>
            <ta e="T258" id="Seg_2526" s="T257">say-PST1-3SG</ta>
            <ta e="T259" id="Seg_2527" s="T258">rotten</ta>
            <ta e="T260" id="Seg_2528" s="T259">old-EP-NMNZ.[NOM]</ta>
            <ta e="T261" id="Seg_2529" s="T260">upper.part-3SG-DAT/LOC</ta>
            <ta e="T262" id="Seg_2530" s="T261">mount-CAUS-PST1-3SG</ta>
            <ta e="T263" id="Seg_2531" s="T262">that-3SG-3SG.[NOM]</ta>
            <ta e="T264" id="Seg_2532" s="T263">horse.[NOM]</ta>
            <ta e="T265" id="Seg_2533" s="T264">become-CVB.SEQ</ta>
            <ta e="T266" id="Seg_2534" s="T265">fly-CVB.SEQ</ta>
            <ta e="T267" id="Seg_2535" s="T266">go-PST2.[3SG]</ta>
            <ta e="T268" id="Seg_2536" s="T267">remember-CVB.SEQ</ta>
            <ta e="T269" id="Seg_2537" s="T268">see-PST2-3SG</ta>
            <ta e="T270" id="Seg_2538" s="T269">rotten</ta>
            <ta e="T271" id="Seg_2539" s="T270">tree-ACC</ta>
            <ta e="T272" id="Seg_2540" s="T271">mount-CVB.SEQ</ta>
            <ta e="T273" id="Seg_2541" s="T272">after</ta>
            <ta e="T274" id="Seg_2542" s="T273">country-3SG-DAT/LOC</ta>
            <ta e="T275" id="Seg_2543" s="T274">lie-PRS.[3SG]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_2544" s="T0">vor.langer.Zeit</ta>
            <ta e="T2" id="Seg_2545" s="T1">eins</ta>
            <ta e="T3" id="Seg_2546" s="T2">Mensch.[NOM]</ta>
            <ta e="T4" id="Seg_2547" s="T3">jagen-CVB.SIM</ta>
            <ta e="T5" id="Seg_2548" s="T4">gehen-CVB.SEQ</ta>
            <ta e="T6" id="Seg_2549" s="T5">Erde.[NOM]</ta>
            <ta e="T7" id="Seg_2550" s="T6">Loch-3SG-INSTR</ta>
            <ta e="T8" id="Seg_2551" s="T7">fallen-CVB.SEQ</ta>
            <ta e="T9" id="Seg_2552" s="T8">bleiben-PST2.[3SG]</ta>
            <ta e="T10" id="Seg_2553" s="T9">fallen-CVB.SEQ</ta>
            <ta e="T11" id="Seg_2554" s="T10">nachdem</ta>
            <ta e="T12" id="Seg_2555" s="T11">erinnern-CVB.SEQ</ta>
            <ta e="T13" id="Seg_2556" s="T12">sehen-PST2-3SG</ta>
            <ta e="T14" id="Seg_2557" s="T13">Erde-DAT/LOC</ta>
            <ta e="T15" id="Seg_2558" s="T14">liegen-PTCP.PRS</ta>
            <ta e="T16" id="Seg_2559" s="T15">sein-PST2.[3SG]</ta>
            <ta e="T17" id="Seg_2560" s="T16">dieses</ta>
            <ta e="T18" id="Seg_2561" s="T17">liegen-TEMP-3SG</ta>
            <ta e="T19" id="Seg_2562" s="T18">drei</ta>
            <ta e="T20" id="Seg_2563" s="T19">Pferd-PROPR</ta>
            <ta e="T21" id="Seg_2564" s="T20">Mädchen.[NOM]</ta>
            <ta e="T22" id="Seg_2565" s="T21">kommen-PST2-3PL</ta>
            <ta e="T23" id="Seg_2566" s="T22">dieses-ACC</ta>
            <ta e="T24" id="Seg_2567" s="T23">1SG.[NOM]</ta>
            <ta e="T25" id="Seg_2568" s="T24">krank.sein-PTCP.PRS</ta>
            <ta e="T26" id="Seg_2569" s="T25">Mensch-1SG</ta>
            <ta e="T27" id="Seg_2570" s="T26">retten-IMP.2PL</ta>
            <ta e="T28" id="Seg_2571" s="T27">sagen-CVB.SEQ</ta>
            <ta e="T29" id="Seg_2572" s="T28">bitten-CVB.SIM</ta>
            <ta e="T30" id="Seg_2573" s="T29">vergeblich.tun-PST2.[3SG]</ta>
            <ta e="T31" id="Seg_2574" s="T30">anderer-3PL.[NOM]</ta>
            <ta e="T32" id="Seg_2575" s="T31">NEG</ta>
            <ta e="T33" id="Seg_2576" s="T32">hören-EP-NEG.PTCP.[NOM]</ta>
            <ta e="T34" id="Seg_2577" s="T33">wie</ta>
            <ta e="T35" id="Seg_2578" s="T34">gehen-CVB.SEQ</ta>
            <ta e="T36" id="Seg_2579" s="T35">bleiben-PST2-3PL</ta>
            <ta e="T37" id="Seg_2580" s="T36">dieses</ta>
            <ta e="T38" id="Seg_2581" s="T37">Mädchen-PL.[NOM]</ta>
            <ta e="T39" id="Seg_2582" s="T38">Wort-3PL-ACC</ta>
            <ta e="T40" id="Seg_2583" s="T39">hören-EP-PST2.[3SG]</ta>
            <ta e="T41" id="Seg_2584" s="T40">wohin</ta>
            <ta e="T42" id="Seg_2585" s="T41">gehen-PST2-3SG</ta>
            <ta e="T43" id="Seg_2586" s="T42">wahrscheinlich=Q</ta>
            <ta e="T44" id="Seg_2587" s="T43">1PL.[NOM]</ta>
            <ta e="T45" id="Seg_2588" s="T44">Rentier-1PL.[NOM]</ta>
            <ta e="T46" id="Seg_2589" s="T45">sagen-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T47" id="Seg_2590" s="T46">dieses-PL.[NOM]</ta>
            <ta e="T48" id="Seg_2591" s="T47">Haus-3PL-DAT/LOC</ta>
            <ta e="T49" id="Seg_2592" s="T48">gehen-PTCP.PST-3PL-DAT/LOC</ta>
            <ta e="T50" id="Seg_2593" s="T49">folgen-CVB.SEQ</ta>
            <ta e="T51" id="Seg_2594" s="T50">gehen-PST2.[3SG]</ta>
            <ta e="T52" id="Seg_2595" s="T51">eins</ta>
            <ta e="T53" id="Seg_2596" s="T52">Haus-DAT/LOC</ta>
            <ta e="T54" id="Seg_2597" s="T53">ankommen-PST2.[3SG]</ta>
            <ta e="T55" id="Seg_2598" s="T54">jenes</ta>
            <ta e="T56" id="Seg_2599" s="T55">ankommen-CVB.SEQ</ta>
            <ta e="T57" id="Seg_2600" s="T56">hineingehen-PST2-3SG</ta>
            <ta e="T58" id="Seg_2601" s="T57">alter.Mann-PROPR</ta>
            <ta e="T59" id="Seg_2602" s="T58">Alte.[NOM]</ta>
            <ta e="T60" id="Seg_2603" s="T59">leben-PRS-3PL</ta>
            <ta e="T61" id="Seg_2604" s="T60">sein-PST2.[3SG]</ta>
            <ta e="T62" id="Seg_2605" s="T61">drei</ta>
            <ta e="T63" id="Seg_2606" s="T62">Tochter-PROPR.[NOM]</ta>
            <ta e="T64" id="Seg_2607" s="T63">Tür-DAT/LOC</ta>
            <ta e="T65" id="Seg_2608" s="T64">stehen-CVB.SEQ</ta>
            <ta e="T66" id="Seg_2609" s="T65">sehen-PST1-3SG</ta>
            <ta e="T67" id="Seg_2610" s="T66">eins</ta>
            <ta e="T68" id="Seg_2611" s="T67">NEG</ta>
            <ta e="T69" id="Seg_2612" s="T68">Mensch.[NOM]</ta>
            <ta e="T70" id="Seg_2613" s="T69">kommen-PST1-2SG</ta>
            <ta e="T71" id="Seg_2614" s="T70">EVID</ta>
            <ta e="T72" id="Seg_2615" s="T71">sagen-CVB.SEQ</ta>
            <ta e="T73" id="Seg_2616" s="T72">sprechen-NEG-PST1-3SG</ta>
            <ta e="T74" id="Seg_2617" s="T73">dieses-ACC</ta>
            <ta e="T75" id="Seg_2618" s="T74">eins</ta>
            <ta e="T76" id="Seg_2619" s="T75">Mädchen-DAT/LOC</ta>
            <ta e="T77" id="Seg_2620" s="T76">in.die.Nähe.gehen-CVB.SEQ</ta>
            <ta e="T78" id="Seg_2621" s="T77">sich.setzen-PST1-3SG</ta>
            <ta e="T79" id="Seg_2622" s="T78">dieses-ACC</ta>
            <ta e="T80" id="Seg_2623" s="T79">dieses</ta>
            <ta e="T81" id="Seg_2624" s="T80">sich.setzen-PTCP.PST-3SG-ACC</ta>
            <ta e="T82" id="Seg_2625" s="T81">bemerken-NEG-3PL</ta>
            <ta e="T83" id="Seg_2626" s="T82">essen-CVB.SIM</ta>
            <ta e="T84" id="Seg_2627" s="T83">sitzen-PST1-3PL</ta>
            <ta e="T85" id="Seg_2628" s="T84">jenes-DAT/LOC</ta>
            <ta e="T86" id="Seg_2629" s="T85">essen.[IMP.2SG]</ta>
            <ta e="T87" id="Seg_2630" s="T86">sagen-NEG-PST1-3PL</ta>
            <ta e="T88" id="Seg_2631" s="T87">dieses-ACC</ta>
            <ta e="T89" id="Seg_2632" s="T88">selbst-3SG.[NOM]</ta>
            <ta e="T90" id="Seg_2633" s="T89">eins</ta>
            <ta e="T91" id="Seg_2634" s="T90">INDEF</ta>
            <ta e="T92" id="Seg_2635" s="T91">Löffel-DIM-ACC</ta>
            <ta e="T93" id="Seg_2636" s="T92">nehmen-CVB.SEQ</ta>
            <ta e="T94" id="Seg_2637" s="T93">essen-PRS.[3SG]</ta>
            <ta e="T95" id="Seg_2638" s="T94">jenes-ACC</ta>
            <ta e="T96" id="Seg_2639" s="T95">sehen-NEG-3PL</ta>
            <ta e="T97" id="Seg_2640" s="T96">warum</ta>
            <ta e="T98" id="Seg_2641" s="T97">Nahrung-1PL.[NOM]</ta>
            <ta e="T99" id="Seg_2642" s="T98">abnehmen-PRS.[3SG]</ta>
            <ta e="T100" id="Seg_2643" s="T99">nur</ta>
            <ta e="T101" id="Seg_2644" s="T100">sagen-EP-RECP/COLL-PRS-3PL</ta>
            <ta e="T102" id="Seg_2645" s="T101">dann</ta>
            <ta e="T103" id="Seg_2646" s="T102">denken-PST1-3SG</ta>
            <ta e="T104" id="Seg_2647" s="T103">1SG-ACC</ta>
            <ta e="T105" id="Seg_2648" s="T104">sehen-NEG-3PL</ta>
            <ta e="T106" id="Seg_2649" s="T105">offenbar</ta>
            <ta e="T107" id="Seg_2650" s="T106">1SG.[NOM]</ta>
            <ta e="T108" id="Seg_2651" s="T107">berühren-CVB.SEQ</ta>
            <ta e="T109" id="Seg_2652" s="T108">versuchen-IMP.1SG</ta>
            <ta e="T110" id="Seg_2653" s="T109">gehen-CVB.SEQ</ta>
            <ta e="T111" id="Seg_2654" s="T110">eins</ta>
            <ta e="T112" id="Seg_2655" s="T111">Mädchen-DAT/LOC</ta>
            <ta e="T113" id="Seg_2656" s="T112">neben</ta>
            <ta e="T114" id="Seg_2657" s="T113">sitzen-CVB.SEQ</ta>
            <ta e="T115" id="Seg_2658" s="T114">berühren-PRS.[3SG]</ta>
            <ta e="T116" id="Seg_2659" s="T115">dieses-DAT/LOC</ta>
            <ta e="T117" id="Seg_2660" s="T116">sterben-PTCP.PRS</ta>
            <ta e="T118" id="Seg_2661" s="T117">Stimme-3SG.[NOM]</ta>
            <ta e="T119" id="Seg_2662" s="T118">hinausgehen-PRS.[3SG]</ta>
            <ta e="T120" id="Seg_2663" s="T119">so</ta>
            <ta e="T121" id="Seg_2664" s="T120">drei</ta>
            <ta e="T122" id="Seg_2665" s="T121">Tag.und.Nacht-ACC</ta>
            <ta e="T123" id="Seg_2666" s="T122">gehen-PST1-3SG</ta>
            <ta e="T124" id="Seg_2667" s="T123">dieses</ta>
            <ta e="T125" id="Seg_2668" s="T124">gehen-CVB.SEQ</ta>
            <ta e="T126" id="Seg_2669" s="T125">denken-VBZ-PRS.[3SG]</ta>
            <ta e="T127" id="Seg_2670" s="T126">1SG.[NOM]</ta>
            <ta e="T128" id="Seg_2671" s="T127">dieses</ta>
            <ta e="T129" id="Seg_2672" s="T128">Land-ABL</ta>
            <ta e="T130" id="Seg_2673" s="T129">gehen-PTCP.PRS</ta>
            <ta e="T131" id="Seg_2674" s="T130">Perspektive-1SG-ACC</ta>
            <ta e="T132" id="Seg_2675" s="T131">machen-FUT-NEC-1SG</ta>
            <ta e="T133" id="Seg_2676" s="T132">denken-PRS.[3SG]</ta>
            <ta e="T134" id="Seg_2677" s="T133">eins</ta>
            <ta e="T135" id="Seg_2678" s="T134">Mädchen-ABL</ta>
            <ta e="T136" id="Seg_2679" s="T135">doch</ta>
            <ta e="T137" id="Seg_2680" s="T136">weggehen-NEG.PTCP</ta>
            <ta e="T138" id="Seg_2681" s="T137">sein-PRS.[3SG]</ta>
            <ta e="T139" id="Seg_2682" s="T138">dieses-DAT/LOC</ta>
            <ta e="T140" id="Seg_2683" s="T139">zwei</ta>
            <ta e="T141" id="Seg_2684" s="T140">Schamane-ACC</ta>
            <ta e="T142" id="Seg_2685" s="T141">schamanisieren-CAUS-PST1-3PL</ta>
            <ta e="T143" id="Seg_2686" s="T142">dieses</ta>
            <ta e="T144" id="Seg_2687" s="T143">Schamane-PL.[NOM]</ta>
            <ta e="T145" id="Seg_2688" s="T144">was.[NOM]</ta>
            <ta e="T146" id="Seg_2689" s="T145">NEG</ta>
            <ta e="T147" id="Seg_2690" s="T146">Hilfe-ACC</ta>
            <ta e="T148" id="Seg_2691" s="T147">geben-NEG-PST1-3PL</ta>
            <ta e="T149" id="Seg_2692" s="T148">dieses-ACC</ta>
            <ta e="T150" id="Seg_2693" s="T149">sprechen-PST1-3PL</ta>
            <ta e="T151" id="Seg_2694" s="T150">groß</ta>
            <ta e="T152" id="Seg_2695" s="T151">Schamane-ACC</ta>
            <ta e="T153" id="Seg_2696" s="T152">bringen-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T154" id="Seg_2697" s="T153">jenes</ta>
            <ta e="T155" id="Seg_2698" s="T154">Schamane-3PL-DAT/LOC</ta>
            <ta e="T156" id="Seg_2699" s="T155">gehen-PST1-3PL</ta>
            <ta e="T157" id="Seg_2700" s="T156">dieses-ACC</ta>
            <ta e="T158" id="Seg_2701" s="T157">gehen-PTCP.PST-3PL-ACC</ta>
            <ta e="T159" id="Seg_2702" s="T158">nachdem</ta>
            <ta e="T160" id="Seg_2703" s="T159">Junge.[NOM]</ta>
            <ta e="T161" id="Seg_2704" s="T160">absichtlich</ta>
            <ta e="T162" id="Seg_2705" s="T161">umarmen-PRS.[3SG]</ta>
            <ta e="T163" id="Seg_2706" s="T162">Mädchen-ACC</ta>
            <ta e="T164" id="Seg_2707" s="T163">jenes-3SG-3SG.[NOM]</ta>
            <ta e="T165" id="Seg_2708" s="T164">sterben-PTCP.PRS</ta>
            <ta e="T166" id="Seg_2709" s="T165">Stimme-3SG.[NOM]</ta>
            <ta e="T167" id="Seg_2710" s="T166">hinausgehen-PRS.[3SG]</ta>
            <ta e="T168" id="Seg_2711" s="T167">dieses</ta>
            <ta e="T169" id="Seg_2712" s="T168">gehen-TEMP-3SG</ta>
            <ta e="T170" id="Seg_2713" s="T169">zwei</ta>
            <ta e="T171" id="Seg_2714" s="T170">Auge.[NOM]</ta>
            <ta e="T172" id="Seg_2715" s="T171">Strahl-3SG.[NOM]</ta>
            <ta e="T173" id="Seg_2716" s="T172">direkt</ta>
            <ta e="T174" id="Seg_2717" s="T173">3SG-DAT/LOC</ta>
            <ta e="T175" id="Seg_2718" s="T174">fallen-PST1-3SG</ta>
            <ta e="T176" id="Seg_2719" s="T175">Schamane-1PL.[NOM]</ta>
            <ta e="T177" id="Seg_2720" s="T176">gehen-PRS.[3SG]</ta>
            <ta e="T178" id="Seg_2721" s="T177">sagen-EP-RECP/COLL-PST1-3PL</ta>
            <ta e="T179" id="Seg_2722" s="T178">dieses-DAT/LOC</ta>
            <ta e="T180" id="Seg_2723" s="T179">wie</ta>
            <ta e="T181" id="Seg_2724" s="T180">machen-PTCP.PRS</ta>
            <ta e="T182" id="Seg_2725" s="T181">sein-PST2.[3SG]</ta>
            <ta e="T183" id="Seg_2726" s="T182">denken-CVB.SEQ</ta>
            <ta e="T184" id="Seg_2727" s="T183">Mädchen-3SG-GEN</ta>
            <ta e="T185" id="Seg_2728" s="T184">Kissen-3SG-GEN</ta>
            <ta e="T186" id="Seg_2729" s="T185">Unterteil-3SG-DAT/LOC</ta>
            <ta e="T187" id="Seg_2730" s="T186">sich.verstecken-CVB.SEQ</ta>
            <ta e="T188" id="Seg_2731" s="T187">bleiben-PST1-3SG</ta>
            <ta e="T189" id="Seg_2732" s="T188">sich.zudecken-CVB.SIM</ta>
            <ta e="T190" id="Seg_2733" s="T189">nicht.schaffen-PST1-3SG</ta>
            <ta e="T191" id="Seg_2734" s="T190">und</ta>
            <ta e="T192" id="Seg_2735" s="T191">jenes</ta>
            <ta e="T193" id="Seg_2736" s="T192">Schamane.[NOM]</ta>
            <ta e="T194" id="Seg_2737" s="T193">Auge-3SG-GEN</ta>
            <ta e="T195" id="Seg_2738" s="T194">Strahl-3SG.[NOM]</ta>
            <ta e="T196" id="Seg_2739" s="T195">direkt</ta>
            <ta e="T197" id="Seg_2740" s="T196">sehen-PRS.[3SG]</ta>
            <ta e="T198" id="Seg_2741" s="T197">hineingehen-CVB.ANT</ta>
            <ta e="T199" id="Seg_2742" s="T198">Schamane-3SG.[NOM]</ta>
            <ta e="T200" id="Seg_2743" s="T199">oberer.Teil-3SG-DAT/LOC</ta>
            <ta e="T201" id="Seg_2744" s="T200">fallen-PST1-3SG</ta>
            <ta e="T202" id="Seg_2745" s="T201">fallen-CVB.SEQ</ta>
            <ta e="T203" id="Seg_2746" s="T202">Rücken-3SG-DAT/LOC</ta>
            <ta e="T204" id="Seg_2747" s="T203">sich.setzen-PST1-3SG</ta>
            <ta e="T205" id="Seg_2748" s="T204">dieses-ACC</ta>
            <ta e="T206" id="Seg_2749" s="T205">groß-ADVZ</ta>
            <ta e="T207" id="Seg_2750" s="T206">kämpfen-PST1-3SG</ta>
            <ta e="T208" id="Seg_2751" s="T207">dort</ta>
            <ta e="T209" id="Seg_2752" s="T208">Schamane-3SG.[NOM]</ta>
            <ta e="T210" id="Seg_2753" s="T209">lassen-NEG.[3SG]</ta>
            <ta e="T211" id="Seg_2754" s="T210">jenes</ta>
            <ta e="T212" id="Seg_2755" s="T211">machen-CVB.SEQ</ta>
            <ta e="T213" id="Seg_2756" s="T212">nachdem</ta>
            <ta e="T214" id="Seg_2757" s="T213">Wort-PROPR.[NOM]</ta>
            <ta e="T215" id="Seg_2758" s="T214">sein-PST1-3SG</ta>
            <ta e="T216" id="Seg_2759" s="T215">2SG.[NOM]</ta>
            <ta e="T217" id="Seg_2760" s="T216">dieses</ta>
            <ta e="T218" id="Seg_2761" s="T217">was.[NOM]</ta>
            <ta e="T219" id="Seg_2762" s="T218">sein-CVB.SEQ</ta>
            <ta e="T220" id="Seg_2763" s="T219">Mensch-ACC</ta>
            <ta e="T221" id="Seg_2764" s="T220">quälen-CVB.SIM</ta>
            <ta e="T222" id="Seg_2765" s="T221">liegen-PRS-2SG</ta>
            <ta e="T223" id="Seg_2766" s="T222">sagen-CVB.SEQ</ta>
            <ta e="T224" id="Seg_2767" s="T223">dieses-ACC</ta>
            <ta e="T225" id="Seg_2768" s="T224">Mensch-3SG.[NOM]</ta>
            <ta e="T226" id="Seg_2769" s="T225">erzählen-PST1-3SG</ta>
            <ta e="T227" id="Seg_2770" s="T226">1SG.[NOM]</ta>
            <ta e="T228" id="Seg_2771" s="T227">selbst-1SG.[NOM]</ta>
            <ta e="T229" id="Seg_2772" s="T228">Wille-1SG-INSTR</ta>
            <ta e="T230" id="Seg_2773" s="T229">kommen-PST2.NEG-EP-1SG</ta>
            <ta e="T231" id="Seg_2774" s="T230">Erde.[NOM]</ta>
            <ta e="T232" id="Seg_2775" s="T231">Loch-3SG-INSTR</ta>
            <ta e="T233" id="Seg_2776" s="T232">fallen-CVB.SEQ</ta>
            <ta e="T234" id="Seg_2777" s="T233">1SG.[NOM]</ta>
            <ta e="T235" id="Seg_2778" s="T234">krank.sein-CVB.SIM</ta>
            <ta e="T236" id="Seg_2779" s="T235">liegen-CVB.SEQ-1SG</ta>
            <ta e="T237" id="Seg_2780" s="T236">dieses</ta>
            <ta e="T238" id="Seg_2781" s="T237">drei</ta>
            <ta e="T239" id="Seg_2782" s="T238">Mädchen-ACC</ta>
            <ta e="T240" id="Seg_2783" s="T239">bitten-EP-MED-PTCP.PST-1SG-DAT/LOC</ta>
            <ta e="T241" id="Seg_2784" s="T240">anderer-3PL.[NOM]</ta>
            <ta e="T242" id="Seg_2785" s="T241">NEG</ta>
            <ta e="T243" id="Seg_2786" s="T242">helfen-PST2.NEG-3SG</ta>
            <ta e="T244" id="Seg_2787" s="T243">jenes.[NOM]</ta>
            <ta e="T245" id="Seg_2788" s="T244">Seite-3SG-ABL</ta>
            <ta e="T246" id="Seg_2789" s="T245">absichtlich</ta>
            <ta e="T247" id="Seg_2790" s="T246">berühren-CVB.SIM</ta>
            <ta e="T248" id="Seg_2791" s="T247">liegen-PRS-1SG</ta>
            <ta e="T249" id="Seg_2792" s="T248">jenes-DAT/LOC</ta>
            <ta e="T250" id="Seg_2793" s="T249">Wahrheit.[NOM]</ta>
            <ta e="T251" id="Seg_2794" s="T250">2SG.[NOM]</ta>
            <ta e="T252" id="Seg_2795" s="T251">lassen.[IMP.2SG]</ta>
            <ta e="T253" id="Seg_2796" s="T252">1SG.[NOM]</ta>
            <ta e="T254" id="Seg_2797" s="T253">2SG-ACC</ta>
            <ta e="T255" id="Seg_2798" s="T254">Vorfahr.[NOM]</ta>
            <ta e="T256" id="Seg_2799" s="T255">Ort-2SG-DAT/LOC</ta>
            <ta e="T257" id="Seg_2800" s="T256">begleiten-FUT-1SG</ta>
            <ta e="T258" id="Seg_2801" s="T257">sagen-PST1-3SG</ta>
            <ta e="T259" id="Seg_2802" s="T258">faul</ta>
            <ta e="T260" id="Seg_2803" s="T259">alt-EP-NMNZ.[NOM]</ta>
            <ta e="T261" id="Seg_2804" s="T260">oberer.Teil-3SG-DAT/LOC</ta>
            <ta e="T262" id="Seg_2805" s="T261">besteigen-CAUS-PST1-3SG</ta>
            <ta e="T263" id="Seg_2806" s="T262">jenes-3SG-3SG.[NOM]</ta>
            <ta e="T264" id="Seg_2807" s="T263">Pferd.[NOM]</ta>
            <ta e="T265" id="Seg_2808" s="T264">werden-CVB.SEQ</ta>
            <ta e="T266" id="Seg_2809" s="T265">fliegen-CVB.SEQ</ta>
            <ta e="T267" id="Seg_2810" s="T266">gehen-PST2.[3SG]</ta>
            <ta e="T268" id="Seg_2811" s="T267">erinnern-CVB.SEQ</ta>
            <ta e="T269" id="Seg_2812" s="T268">sehen-PST2-3SG</ta>
            <ta e="T270" id="Seg_2813" s="T269">faul</ta>
            <ta e="T271" id="Seg_2814" s="T270">Baum-ACC</ta>
            <ta e="T272" id="Seg_2815" s="T271">besteigen-CVB.SEQ</ta>
            <ta e="T273" id="Seg_2816" s="T272">nachdem</ta>
            <ta e="T274" id="Seg_2817" s="T273">Land-3SG-DAT/LOC</ta>
            <ta e="T275" id="Seg_2818" s="T274">liegen-PRS.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_2819" s="T0">давно</ta>
            <ta e="T2" id="Seg_2820" s="T1">один</ta>
            <ta e="T3" id="Seg_2821" s="T2">человек.[NOM]</ta>
            <ta e="T4" id="Seg_2822" s="T3">охотиться-CVB.SIM</ta>
            <ta e="T5" id="Seg_2823" s="T4">идти-CVB.SEQ</ta>
            <ta e="T6" id="Seg_2824" s="T5">земля.[NOM]</ta>
            <ta e="T7" id="Seg_2825" s="T6">отверстие-3SG-INSTR</ta>
            <ta e="T8" id="Seg_2826" s="T7">падать-CVB.SEQ</ta>
            <ta e="T9" id="Seg_2827" s="T8">оставаться-PST2.[3SG]</ta>
            <ta e="T10" id="Seg_2828" s="T9">падать-CVB.SEQ</ta>
            <ta e="T11" id="Seg_2829" s="T10">после</ta>
            <ta e="T12" id="Seg_2830" s="T11">помнить-CVB.SEQ</ta>
            <ta e="T13" id="Seg_2831" s="T12">видеть-PST2-3SG</ta>
            <ta e="T14" id="Seg_2832" s="T13">земля-DAT/LOC</ta>
            <ta e="T15" id="Seg_2833" s="T14">лежать-PTCP.PRS</ta>
            <ta e="T16" id="Seg_2834" s="T15">быть-PST2.[3SG]</ta>
            <ta e="T17" id="Seg_2835" s="T16">этот</ta>
            <ta e="T18" id="Seg_2836" s="T17">лежать-TEMP-3SG</ta>
            <ta e="T19" id="Seg_2837" s="T18">три</ta>
            <ta e="T20" id="Seg_2838" s="T19">лошадь-PROPR</ta>
            <ta e="T21" id="Seg_2839" s="T20">девушка.[NOM]</ta>
            <ta e="T22" id="Seg_2840" s="T21">приходить-PST2-3PL</ta>
            <ta e="T23" id="Seg_2841" s="T22">этот-ACC</ta>
            <ta e="T24" id="Seg_2842" s="T23">1SG.[NOM]</ta>
            <ta e="T25" id="Seg_2843" s="T24">быть.больным-PTCP.PRS</ta>
            <ta e="T26" id="Seg_2844" s="T25">человек-1SG</ta>
            <ta e="T27" id="Seg_2845" s="T26">спасать-IMP.2PL</ta>
            <ta e="T28" id="Seg_2846" s="T27">говорить-CVB.SEQ</ta>
            <ta e="T29" id="Seg_2847" s="T28">просить-CVB.SIM</ta>
            <ta e="T30" id="Seg_2848" s="T29">делать.напрасно-PST2.[3SG]</ta>
            <ta e="T31" id="Seg_2849" s="T30">другой-3PL.[NOM]</ta>
            <ta e="T32" id="Seg_2850" s="T31">NEG</ta>
            <ta e="T33" id="Seg_2851" s="T32">слышать-EP-NEG.PTCP.[NOM]</ta>
            <ta e="T34" id="Seg_2852" s="T33">как</ta>
            <ta e="T35" id="Seg_2853" s="T34">идти-CVB.SEQ</ta>
            <ta e="T36" id="Seg_2854" s="T35">оставаться-PST2-3PL</ta>
            <ta e="T37" id="Seg_2855" s="T36">этот</ta>
            <ta e="T38" id="Seg_2856" s="T37">девушка-PL.[NOM]</ta>
            <ta e="T39" id="Seg_2857" s="T38">слово-3PL-ACC</ta>
            <ta e="T40" id="Seg_2858" s="T39">слышать-EP-PST2.[3SG]</ta>
            <ta e="T41" id="Seg_2859" s="T40">куда</ta>
            <ta e="T42" id="Seg_2860" s="T41">идти-PST2-3SG</ta>
            <ta e="T43" id="Seg_2861" s="T42">наверное=Q</ta>
            <ta e="T44" id="Seg_2862" s="T43">1PL.[NOM]</ta>
            <ta e="T45" id="Seg_2863" s="T44">олень-1PL.[NOM]</ta>
            <ta e="T46" id="Seg_2864" s="T45">говорить-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T47" id="Seg_2865" s="T46">этот-PL.[NOM]</ta>
            <ta e="T48" id="Seg_2866" s="T47">дом-3PL-DAT/LOC</ta>
            <ta e="T49" id="Seg_2867" s="T48">идти-PTCP.PST-3PL-DAT/LOC</ta>
            <ta e="T50" id="Seg_2868" s="T49">следовать-CVB.SEQ</ta>
            <ta e="T51" id="Seg_2869" s="T50">идти-PST2.[3SG]</ta>
            <ta e="T52" id="Seg_2870" s="T51">один</ta>
            <ta e="T53" id="Seg_2871" s="T52">дом-DAT/LOC</ta>
            <ta e="T54" id="Seg_2872" s="T53">доезжать-PST2.[3SG]</ta>
            <ta e="T55" id="Seg_2873" s="T54">тот</ta>
            <ta e="T56" id="Seg_2874" s="T55">доезжать-CVB.SEQ</ta>
            <ta e="T57" id="Seg_2875" s="T56">входить-PST2-3SG</ta>
            <ta e="T58" id="Seg_2876" s="T57">старик-PROPR</ta>
            <ta e="T59" id="Seg_2877" s="T58">старуха.[NOM]</ta>
            <ta e="T60" id="Seg_2878" s="T59">жить-PRS-3PL</ta>
            <ta e="T61" id="Seg_2879" s="T60">быть-PST2.[3SG]</ta>
            <ta e="T62" id="Seg_2880" s="T61">три</ta>
            <ta e="T63" id="Seg_2881" s="T62">дочь-PROPR.[NOM]</ta>
            <ta e="T64" id="Seg_2882" s="T63">дверь-DAT/LOC</ta>
            <ta e="T65" id="Seg_2883" s="T64">стоять-CVB.SEQ</ta>
            <ta e="T66" id="Seg_2884" s="T65">видеть-PST1-3SG</ta>
            <ta e="T67" id="Seg_2885" s="T66">один</ta>
            <ta e="T68" id="Seg_2886" s="T67">NEG</ta>
            <ta e="T69" id="Seg_2887" s="T68">человек.[NOM]</ta>
            <ta e="T70" id="Seg_2888" s="T69">приходить-PST1-2SG</ta>
            <ta e="T71" id="Seg_2889" s="T70">EVID</ta>
            <ta e="T72" id="Seg_2890" s="T71">говорить-CVB.SEQ</ta>
            <ta e="T73" id="Seg_2891" s="T72">говорить-NEG-PST1-3SG</ta>
            <ta e="T74" id="Seg_2892" s="T73">этот-ACC</ta>
            <ta e="T75" id="Seg_2893" s="T74">один</ta>
            <ta e="T76" id="Seg_2894" s="T75">девушка-DAT/LOC</ta>
            <ta e="T77" id="Seg_2895" s="T76">идти.рядом-CVB.SEQ</ta>
            <ta e="T78" id="Seg_2896" s="T77">сесть-PST1-3SG</ta>
            <ta e="T79" id="Seg_2897" s="T78">этот-ACC</ta>
            <ta e="T80" id="Seg_2898" s="T79">этот</ta>
            <ta e="T81" id="Seg_2899" s="T80">сесть-PTCP.PST-3SG-ACC</ta>
            <ta e="T82" id="Seg_2900" s="T81">замечать-NEG-3PL</ta>
            <ta e="T83" id="Seg_2901" s="T82">есть-CVB.SIM</ta>
            <ta e="T84" id="Seg_2902" s="T83">сидеть-PST1-3PL</ta>
            <ta e="T85" id="Seg_2903" s="T84">тот-DAT/LOC</ta>
            <ta e="T86" id="Seg_2904" s="T85">есть.[IMP.2SG]</ta>
            <ta e="T87" id="Seg_2905" s="T86">говорить-NEG-PST1-3PL</ta>
            <ta e="T88" id="Seg_2906" s="T87">этот-ACC</ta>
            <ta e="T89" id="Seg_2907" s="T88">сам-3SG.[NOM]</ta>
            <ta e="T90" id="Seg_2908" s="T89">один</ta>
            <ta e="T91" id="Seg_2909" s="T90">INDEF</ta>
            <ta e="T92" id="Seg_2910" s="T91">ложка-DIM-ACC</ta>
            <ta e="T93" id="Seg_2911" s="T92">взять-CVB.SEQ</ta>
            <ta e="T94" id="Seg_2912" s="T93">есть-PRS.[3SG]</ta>
            <ta e="T95" id="Seg_2913" s="T94">тот-ACC</ta>
            <ta e="T96" id="Seg_2914" s="T95">видеть-NEG-3PL</ta>
            <ta e="T97" id="Seg_2915" s="T96">почему</ta>
            <ta e="T98" id="Seg_2916" s="T97">пища-1PL.[NOM]</ta>
            <ta e="T99" id="Seg_2917" s="T98">убывать-PRS.[3SG]</ta>
            <ta e="T100" id="Seg_2918" s="T99">только</ta>
            <ta e="T101" id="Seg_2919" s="T100">говорить-EP-RECP/COLL-PRS-3PL</ta>
            <ta e="T102" id="Seg_2920" s="T101">потом</ta>
            <ta e="T103" id="Seg_2921" s="T102">думать-PST1-3SG</ta>
            <ta e="T104" id="Seg_2922" s="T103">1SG-ACC</ta>
            <ta e="T105" id="Seg_2923" s="T104">видеть-NEG-3PL</ta>
            <ta e="T106" id="Seg_2924" s="T105">наверное</ta>
            <ta e="T107" id="Seg_2925" s="T106">1SG.[NOM]</ta>
            <ta e="T108" id="Seg_2926" s="T107">трогать-CVB.SEQ</ta>
            <ta e="T109" id="Seg_2927" s="T108">попробовать-IMP.1SG</ta>
            <ta e="T110" id="Seg_2928" s="T109">идти-CVB.SEQ</ta>
            <ta e="T111" id="Seg_2929" s="T110">один</ta>
            <ta e="T112" id="Seg_2930" s="T111">девушка-DAT/LOC</ta>
            <ta e="T113" id="Seg_2931" s="T112">рядом</ta>
            <ta e="T114" id="Seg_2932" s="T113">сидеть-CVB.SEQ</ta>
            <ta e="T115" id="Seg_2933" s="T114">трогать-PRS.[3SG]</ta>
            <ta e="T116" id="Seg_2934" s="T115">этот-DAT/LOC</ta>
            <ta e="T117" id="Seg_2935" s="T116">умирать-PTCP.PRS</ta>
            <ta e="T118" id="Seg_2936" s="T117">голос-3SG.[NOM]</ta>
            <ta e="T119" id="Seg_2937" s="T118">выйти-PRS.[3SG]</ta>
            <ta e="T120" id="Seg_2938" s="T119">так</ta>
            <ta e="T121" id="Seg_2939" s="T120">три</ta>
            <ta e="T122" id="Seg_2940" s="T121">сутки-ACC</ta>
            <ta e="T123" id="Seg_2941" s="T122">идти-PST1-3SG</ta>
            <ta e="T124" id="Seg_2942" s="T123">этот</ta>
            <ta e="T125" id="Seg_2943" s="T124">идти-CVB.SEQ</ta>
            <ta e="T126" id="Seg_2944" s="T125">думать-VBZ-PRS.[3SG]</ta>
            <ta e="T127" id="Seg_2945" s="T126">1SG.[NOM]</ta>
            <ta e="T128" id="Seg_2946" s="T127">этот</ta>
            <ta e="T129" id="Seg_2947" s="T128">страна-ABL</ta>
            <ta e="T130" id="Seg_2948" s="T129">идти-PTCP.PRS</ta>
            <ta e="T131" id="Seg_2949" s="T130">перспектива-1SG-ACC</ta>
            <ta e="T132" id="Seg_2950" s="T131">делать-FUT-NEC-1SG</ta>
            <ta e="T133" id="Seg_2951" s="T132">думать-PRS.[3SG]</ta>
            <ta e="T134" id="Seg_2952" s="T133">один</ta>
            <ta e="T135" id="Seg_2953" s="T134">девушка-ABL</ta>
            <ta e="T136" id="Seg_2954" s="T135">вот</ta>
            <ta e="T137" id="Seg_2955" s="T136">уйти-NEG.PTCP</ta>
            <ta e="T138" id="Seg_2956" s="T137">быть-PRS.[3SG]</ta>
            <ta e="T139" id="Seg_2957" s="T138">этот-DAT/LOC</ta>
            <ta e="T140" id="Seg_2958" s="T139">два</ta>
            <ta e="T141" id="Seg_2959" s="T140">шаман-ACC</ta>
            <ta e="T142" id="Seg_2960" s="T141">камлать-CAUS-PST1-3PL</ta>
            <ta e="T143" id="Seg_2961" s="T142">этот</ta>
            <ta e="T144" id="Seg_2962" s="T143">шаман-PL.[NOM]</ta>
            <ta e="T145" id="Seg_2963" s="T144">что.[NOM]</ta>
            <ta e="T146" id="Seg_2964" s="T145">NEG</ta>
            <ta e="T147" id="Seg_2965" s="T146">помощь-ACC</ta>
            <ta e="T148" id="Seg_2966" s="T147">давать-NEG-PST1-3PL</ta>
            <ta e="T149" id="Seg_2967" s="T148">этот-ACC</ta>
            <ta e="T150" id="Seg_2968" s="T149">говорить-PST1-3PL</ta>
            <ta e="T151" id="Seg_2969" s="T150">большой</ta>
            <ta e="T152" id="Seg_2970" s="T151">шаман-ACC</ta>
            <ta e="T153" id="Seg_2971" s="T152">принести-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T154" id="Seg_2972" s="T153">тот</ta>
            <ta e="T155" id="Seg_2973" s="T154">шаман-3PL-DAT/LOC</ta>
            <ta e="T156" id="Seg_2974" s="T155">идти-PST1-3PL</ta>
            <ta e="T157" id="Seg_2975" s="T156">этот-ACC</ta>
            <ta e="T158" id="Seg_2976" s="T157">идти-PTCP.PST-3PL-ACC</ta>
            <ta e="T159" id="Seg_2977" s="T158">после.того</ta>
            <ta e="T160" id="Seg_2978" s="T159">мальчик.[NOM]</ta>
            <ta e="T161" id="Seg_2979" s="T160">нарочно</ta>
            <ta e="T162" id="Seg_2980" s="T161">обнимать-PRS.[3SG]</ta>
            <ta e="T163" id="Seg_2981" s="T162">девушка-ACC</ta>
            <ta e="T164" id="Seg_2982" s="T163">тот-3SG-3SG.[NOM]</ta>
            <ta e="T165" id="Seg_2983" s="T164">умирать-PTCP.PRS</ta>
            <ta e="T166" id="Seg_2984" s="T165">голос-3SG.[NOM]</ta>
            <ta e="T167" id="Seg_2985" s="T166">выйти-PRS.[3SG]</ta>
            <ta e="T168" id="Seg_2986" s="T167">этот</ta>
            <ta e="T169" id="Seg_2987" s="T168">идти-TEMP-3SG</ta>
            <ta e="T170" id="Seg_2988" s="T169">два</ta>
            <ta e="T171" id="Seg_2989" s="T170">глаз.[NOM]</ta>
            <ta e="T172" id="Seg_2990" s="T171">луч-3SG.[NOM]</ta>
            <ta e="T173" id="Seg_2991" s="T172">прямо</ta>
            <ta e="T174" id="Seg_2992" s="T173">3SG-DAT/LOC</ta>
            <ta e="T175" id="Seg_2993" s="T174">падать-PST1-3SG</ta>
            <ta e="T176" id="Seg_2994" s="T175">шаман-1PL.[NOM]</ta>
            <ta e="T177" id="Seg_2995" s="T176">идти-PRS.[3SG]</ta>
            <ta e="T178" id="Seg_2996" s="T177">говорить-EP-RECP/COLL-PST1-3PL</ta>
            <ta e="T179" id="Seg_2997" s="T178">этот-DAT/LOC</ta>
            <ta e="T180" id="Seg_2998" s="T179">как</ta>
            <ta e="T181" id="Seg_2999" s="T180">делать-PTCP.PRS</ta>
            <ta e="T182" id="Seg_3000" s="T181">быть-PST2.[3SG]</ta>
            <ta e="T183" id="Seg_3001" s="T182">думать-CVB.SEQ</ta>
            <ta e="T184" id="Seg_3002" s="T183">девушка-3SG-GEN</ta>
            <ta e="T185" id="Seg_3003" s="T184">подушка-3SG-GEN</ta>
            <ta e="T186" id="Seg_3004" s="T185">нижняя.часть-3SG-DAT/LOC</ta>
            <ta e="T187" id="Seg_3005" s="T186">прятаться-CVB.SEQ</ta>
            <ta e="T188" id="Seg_3006" s="T187">оставаться-PST1-3SG</ta>
            <ta e="T189" id="Seg_3007" s="T188">укрываться-CVB.SIM</ta>
            <ta e="T190" id="Seg_3008" s="T189">не.справиться-PST1-3SG</ta>
            <ta e="T191" id="Seg_3009" s="T190">да</ta>
            <ta e="T192" id="Seg_3010" s="T191">тот</ta>
            <ta e="T193" id="Seg_3011" s="T192">шаман.[NOM]</ta>
            <ta e="T194" id="Seg_3012" s="T193">глаз-3SG-GEN</ta>
            <ta e="T195" id="Seg_3013" s="T194">луч-3SG.[NOM]</ta>
            <ta e="T196" id="Seg_3014" s="T195">прямо</ta>
            <ta e="T197" id="Seg_3015" s="T196">видеть-PRS.[3SG]</ta>
            <ta e="T198" id="Seg_3016" s="T197">входить-CVB.ANT</ta>
            <ta e="T199" id="Seg_3017" s="T198">шаман-3SG.[NOM]</ta>
            <ta e="T200" id="Seg_3018" s="T199">верхняя.часть-3SG-DAT/LOC</ta>
            <ta e="T201" id="Seg_3019" s="T200">падать-PST1-3SG</ta>
            <ta e="T202" id="Seg_3020" s="T201">падать-CVB.SEQ</ta>
            <ta e="T203" id="Seg_3021" s="T202">спина-3SG-DAT/LOC</ta>
            <ta e="T204" id="Seg_3022" s="T203">сесть-PST1-3SG</ta>
            <ta e="T205" id="Seg_3023" s="T204">этот-ACC</ta>
            <ta e="T206" id="Seg_3024" s="T205">большой-ADVZ</ta>
            <ta e="T207" id="Seg_3025" s="T206">биться-PST1-3SG</ta>
            <ta e="T208" id="Seg_3026" s="T207">там</ta>
            <ta e="T209" id="Seg_3027" s="T208">шаман-3SG.[NOM]</ta>
            <ta e="T210" id="Seg_3028" s="T209">пустить-NEG.[3SG]</ta>
            <ta e="T211" id="Seg_3029" s="T210">тот</ta>
            <ta e="T212" id="Seg_3030" s="T211">делать-CVB.SEQ</ta>
            <ta e="T213" id="Seg_3031" s="T212">после</ta>
            <ta e="T214" id="Seg_3032" s="T213">слово-PROPR.[NOM]</ta>
            <ta e="T215" id="Seg_3033" s="T214">быть-PST1-3SG</ta>
            <ta e="T216" id="Seg_3034" s="T215">2SG.[NOM]</ta>
            <ta e="T217" id="Seg_3035" s="T216">этот</ta>
            <ta e="T218" id="Seg_3036" s="T217">что.[NOM]</ta>
            <ta e="T219" id="Seg_3037" s="T218">быть-CVB.SEQ</ta>
            <ta e="T220" id="Seg_3038" s="T219">человек-ACC</ta>
            <ta e="T221" id="Seg_3039" s="T220">мучить-CVB.SIM</ta>
            <ta e="T222" id="Seg_3040" s="T221">лежать-PRS-2SG</ta>
            <ta e="T223" id="Seg_3041" s="T222">говорить-CVB.SEQ</ta>
            <ta e="T224" id="Seg_3042" s="T223">этот-ACC</ta>
            <ta e="T225" id="Seg_3043" s="T224">человек-3SG.[NOM]</ta>
            <ta e="T226" id="Seg_3044" s="T225">рассказывать-PST1-3SG</ta>
            <ta e="T227" id="Seg_3045" s="T226">1SG.[NOM]</ta>
            <ta e="T228" id="Seg_3046" s="T227">сам-1SG.[NOM]</ta>
            <ta e="T229" id="Seg_3047" s="T228">воля-1SG-INSTR</ta>
            <ta e="T230" id="Seg_3048" s="T229">приходить-PST2.NEG-EP-1SG</ta>
            <ta e="T231" id="Seg_3049" s="T230">земля.[NOM]</ta>
            <ta e="T232" id="Seg_3050" s="T231">отверстие-3SG-INSTR</ta>
            <ta e="T233" id="Seg_3051" s="T232">падать-CVB.SEQ</ta>
            <ta e="T234" id="Seg_3052" s="T233">1SG.[NOM]</ta>
            <ta e="T235" id="Seg_3053" s="T234">быть.больным-CVB.SIM</ta>
            <ta e="T236" id="Seg_3054" s="T235">лежать-CVB.SEQ-1SG</ta>
            <ta e="T237" id="Seg_3055" s="T236">этот</ta>
            <ta e="T238" id="Seg_3056" s="T237">три</ta>
            <ta e="T239" id="Seg_3057" s="T238">девушка-ACC</ta>
            <ta e="T240" id="Seg_3058" s="T239">попросить-EP-MED-PTCP.PST-1SG-DAT/LOC</ta>
            <ta e="T241" id="Seg_3059" s="T240">другой-3PL.[NOM]</ta>
            <ta e="T242" id="Seg_3060" s="T241">NEG</ta>
            <ta e="T243" id="Seg_3061" s="T242">помогать-PST2.NEG-3SG</ta>
            <ta e="T244" id="Seg_3062" s="T243">тот.[NOM]</ta>
            <ta e="T245" id="Seg_3063" s="T244">сторона-3SG-ABL</ta>
            <ta e="T246" id="Seg_3064" s="T245">нарочно</ta>
            <ta e="T247" id="Seg_3065" s="T246">трогать-CVB.SIM</ta>
            <ta e="T248" id="Seg_3066" s="T247">лежать-PRS-1SG</ta>
            <ta e="T249" id="Seg_3067" s="T248">тот-DAT/LOC</ta>
            <ta e="T250" id="Seg_3068" s="T249">правда.[NOM]</ta>
            <ta e="T251" id="Seg_3069" s="T250">2SG.[NOM]</ta>
            <ta e="T252" id="Seg_3070" s="T251">оставлять.[IMP.2SG]</ta>
            <ta e="T253" id="Seg_3071" s="T252">1SG.[NOM]</ta>
            <ta e="T254" id="Seg_3072" s="T253">2SG-ACC</ta>
            <ta e="T255" id="Seg_3073" s="T254">предок.[NOM]</ta>
            <ta e="T256" id="Seg_3074" s="T255">место-2SG-DAT/LOC</ta>
            <ta e="T257" id="Seg_3075" s="T256">провожать-FUT-1SG</ta>
            <ta e="T258" id="Seg_3076" s="T257">говорить-PST1-3SG</ta>
            <ta e="T259" id="Seg_3077" s="T258">гнилой</ta>
            <ta e="T260" id="Seg_3078" s="T259">старый-EP-NMNZ.[NOM]</ta>
            <ta e="T261" id="Seg_3079" s="T260">верхняя.часть-3SG-DAT/LOC</ta>
            <ta e="T262" id="Seg_3080" s="T261">садиться-CAUS-PST1-3SG</ta>
            <ta e="T263" id="Seg_3081" s="T262">тот-3SG-3SG.[NOM]</ta>
            <ta e="T264" id="Seg_3082" s="T263">лошадь.[NOM]</ta>
            <ta e="T265" id="Seg_3083" s="T264">становиться-CVB.SEQ</ta>
            <ta e="T266" id="Seg_3084" s="T265">летать-CVB.SEQ</ta>
            <ta e="T267" id="Seg_3085" s="T266">идти-PST2.[3SG]</ta>
            <ta e="T268" id="Seg_3086" s="T267">помнить-CVB.SEQ</ta>
            <ta e="T269" id="Seg_3087" s="T268">видеть-PST2-3SG</ta>
            <ta e="T270" id="Seg_3088" s="T269">гнилой</ta>
            <ta e="T271" id="Seg_3089" s="T270">дерево-ACC</ta>
            <ta e="T272" id="Seg_3090" s="T271">садиться-CVB.SEQ</ta>
            <ta e="T273" id="Seg_3091" s="T272">после</ta>
            <ta e="T274" id="Seg_3092" s="T273">страна-3SG-DAT/LOC</ta>
            <ta e="T275" id="Seg_3093" s="T274">лежать-PRS.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_3094" s="T0">adv</ta>
            <ta e="T2" id="Seg_3095" s="T1">cardnum</ta>
            <ta e="T3" id="Seg_3096" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_3097" s="T3">v-v:cvb</ta>
            <ta e="T5" id="Seg_3098" s="T4">v-v:cvb</ta>
            <ta e="T6" id="Seg_3099" s="T5">n-n:case</ta>
            <ta e="T7" id="Seg_3100" s="T6">n-n:poss-n:case</ta>
            <ta e="T8" id="Seg_3101" s="T7">v-v:cvb</ta>
            <ta e="T9" id="Seg_3102" s="T8">v-v:tense-v:pred.pn</ta>
            <ta e="T10" id="Seg_3103" s="T9">v-v:cvb</ta>
            <ta e="T11" id="Seg_3104" s="T10">post</ta>
            <ta e="T12" id="Seg_3105" s="T11">v-v:cvb</ta>
            <ta e="T13" id="Seg_3106" s="T12">v-v:tense-v:poss.pn</ta>
            <ta e="T14" id="Seg_3107" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_3108" s="T14">v-v:ptcp</ta>
            <ta e="T16" id="Seg_3109" s="T15">v-v:tense-v:pred.pn</ta>
            <ta e="T17" id="Seg_3110" s="T16">dempro</ta>
            <ta e="T18" id="Seg_3111" s="T17">v-v:mood-v:temp.pn</ta>
            <ta e="T19" id="Seg_3112" s="T18">cardnum</ta>
            <ta e="T20" id="Seg_3113" s="T19">n-n&gt;adj</ta>
            <ta e="T21" id="Seg_3114" s="T20">n-n:case</ta>
            <ta e="T22" id="Seg_3115" s="T21">v-v:tense-v:pred.pn</ta>
            <ta e="T23" id="Seg_3116" s="T22">dempro-pro:case</ta>
            <ta e="T24" id="Seg_3117" s="T23">pers-pro:case</ta>
            <ta e="T25" id="Seg_3118" s="T24">v-v:ptcp</ta>
            <ta e="T26" id="Seg_3119" s="T25">n-n:(pred.pn)</ta>
            <ta e="T27" id="Seg_3120" s="T26">v-v:mood.pn</ta>
            <ta e="T28" id="Seg_3121" s="T27">v-v:cvb</ta>
            <ta e="T29" id="Seg_3122" s="T28">v-v:cvb</ta>
            <ta e="T30" id="Seg_3123" s="T29">v-v:tense-v:pred.pn</ta>
            <ta e="T31" id="Seg_3124" s="T30">adj-n:(poss)-n:case</ta>
            <ta e="T32" id="Seg_3125" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_3126" s="T32">v-v:(ins)-v:ptcp-v:(case)</ta>
            <ta e="T34" id="Seg_3127" s="T33">post</ta>
            <ta e="T35" id="Seg_3128" s="T34">v-v:cvb</ta>
            <ta e="T36" id="Seg_3129" s="T35">v-v:tense-v:pred.pn</ta>
            <ta e="T37" id="Seg_3130" s="T36">dempro</ta>
            <ta e="T38" id="Seg_3131" s="T37">n-n:(num)-n:case</ta>
            <ta e="T39" id="Seg_3132" s="T38">n-n:poss-n:case</ta>
            <ta e="T40" id="Seg_3133" s="T39">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T41" id="Seg_3134" s="T40">que</ta>
            <ta e="T42" id="Seg_3135" s="T41">v-v:tense-v:poss.pn</ta>
            <ta e="T43" id="Seg_3136" s="T42">adv-ptcl</ta>
            <ta e="T44" id="Seg_3137" s="T43">pers-pro:case</ta>
            <ta e="T45" id="Seg_3138" s="T44">n-n:(poss)-n:case</ta>
            <ta e="T46" id="Seg_3139" s="T45">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T47" id="Seg_3140" s="T46">dempro-pro:(num)-pro:case</ta>
            <ta e="T48" id="Seg_3141" s="T47">n-n:poss-n:case</ta>
            <ta e="T49" id="Seg_3142" s="T48">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T50" id="Seg_3143" s="T49">v-v:cvb</ta>
            <ta e="T51" id="Seg_3144" s="T50">v-v:tense-v:pred.pn</ta>
            <ta e="T52" id="Seg_3145" s="T51">cardnum</ta>
            <ta e="T53" id="Seg_3146" s="T52">n-n:case</ta>
            <ta e="T54" id="Seg_3147" s="T53">v-v:tense-v:pred.pn</ta>
            <ta e="T55" id="Seg_3148" s="T54">dempro</ta>
            <ta e="T56" id="Seg_3149" s="T55">v-v:cvb</ta>
            <ta e="T57" id="Seg_3150" s="T56">v-v:tense-v:poss.pn</ta>
            <ta e="T58" id="Seg_3151" s="T57">n-n&gt;adj</ta>
            <ta e="T59" id="Seg_3152" s="T58">n-n:case</ta>
            <ta e="T60" id="Seg_3153" s="T59">v-v:tense-v:pred.pn</ta>
            <ta e="T61" id="Seg_3154" s="T60">v-v:tense-v:pred.pn</ta>
            <ta e="T62" id="Seg_3155" s="T61">cardnum</ta>
            <ta e="T63" id="Seg_3156" s="T62">n-n&gt;adj-n:case</ta>
            <ta e="T64" id="Seg_3157" s="T63">n-n:case</ta>
            <ta e="T65" id="Seg_3158" s="T64">v-v:cvb</ta>
            <ta e="T66" id="Seg_3159" s="T65">v-v:tense-v:poss.pn</ta>
            <ta e="T67" id="Seg_3160" s="T66">cardnum</ta>
            <ta e="T68" id="Seg_3161" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_3162" s="T68">n-n:case</ta>
            <ta e="T70" id="Seg_3163" s="T69">v-v:tense-v:poss.pn</ta>
            <ta e="T71" id="Seg_3164" s="T70">ptcl</ta>
            <ta e="T72" id="Seg_3165" s="T71">v-v:cvb</ta>
            <ta e="T73" id="Seg_3166" s="T72">v-v:(neg)-v:tense-v:poss.pn</ta>
            <ta e="T74" id="Seg_3167" s="T73">dempro-pro:case</ta>
            <ta e="T75" id="Seg_3168" s="T74">cardnum</ta>
            <ta e="T76" id="Seg_3169" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_3170" s="T76">v-v:cvb</ta>
            <ta e="T78" id="Seg_3171" s="T77">v-v:tense-v:poss.pn</ta>
            <ta e="T79" id="Seg_3172" s="T78">dempro-pro:case</ta>
            <ta e="T80" id="Seg_3173" s="T79">dempro</ta>
            <ta e="T81" id="Seg_3174" s="T80">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T82" id="Seg_3175" s="T81">v-v:(neg)-v:pred.pn</ta>
            <ta e="T83" id="Seg_3176" s="T82">v-v:cvb</ta>
            <ta e="T84" id="Seg_3177" s="T83">v-v:tense-v:pred.pn</ta>
            <ta e="T85" id="Seg_3178" s="T84">dempro-pro:case</ta>
            <ta e="T86" id="Seg_3179" s="T85">v-v:mood.pn</ta>
            <ta e="T87" id="Seg_3180" s="T86">v-v:(neg)-v:tense-v:pred.pn</ta>
            <ta e="T88" id="Seg_3181" s="T87">dempro-pro:case</ta>
            <ta e="T89" id="Seg_3182" s="T88">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T90" id="Seg_3183" s="T89">cardnum</ta>
            <ta e="T91" id="Seg_3184" s="T90">ptcl</ta>
            <ta e="T92" id="Seg_3185" s="T91">n-n&gt;n-n:case</ta>
            <ta e="T93" id="Seg_3186" s="T92">v-v:cvb</ta>
            <ta e="T94" id="Seg_3187" s="T93">v-v:tense-v:pred.pn</ta>
            <ta e="T95" id="Seg_3188" s="T94">dempro-pro:case</ta>
            <ta e="T96" id="Seg_3189" s="T95">v-v:(neg)-v:pred.pn</ta>
            <ta e="T97" id="Seg_3190" s="T96">que</ta>
            <ta e="T98" id="Seg_3191" s="T97">n-n:(poss)-n:case</ta>
            <ta e="T99" id="Seg_3192" s="T98">v-v:tense-v:pred.pn</ta>
            <ta e="T100" id="Seg_3193" s="T99">ptcl</ta>
            <ta e="T101" id="Seg_3194" s="T100">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T102" id="Seg_3195" s="T101">adv</ta>
            <ta e="T103" id="Seg_3196" s="T102">v-v:tense-v:poss.pn</ta>
            <ta e="T104" id="Seg_3197" s="T103">pers-pro:case</ta>
            <ta e="T105" id="Seg_3198" s="T104">v-v:(neg)-v:pred.pn</ta>
            <ta e="T106" id="Seg_3199" s="T105">adv</ta>
            <ta e="T107" id="Seg_3200" s="T106">pers-pro:case</ta>
            <ta e="T108" id="Seg_3201" s="T107">v-v:cvb</ta>
            <ta e="T109" id="Seg_3202" s="T108">v-v:mood.pn</ta>
            <ta e="T110" id="Seg_3203" s="T109">v-v:cvb</ta>
            <ta e="T111" id="Seg_3204" s="T110">cardnum</ta>
            <ta e="T112" id="Seg_3205" s="T111">n-n:case</ta>
            <ta e="T113" id="Seg_3206" s="T112">post</ta>
            <ta e="T114" id="Seg_3207" s="T113">v-v:cvb</ta>
            <ta e="T115" id="Seg_3208" s="T114">v-v:tense-v:pred.pn</ta>
            <ta e="T116" id="Seg_3209" s="T115">dempro-pro:case</ta>
            <ta e="T117" id="Seg_3210" s="T116">v-v:ptcp</ta>
            <ta e="T118" id="Seg_3211" s="T117">n-n:(poss)-n:case</ta>
            <ta e="T119" id="Seg_3212" s="T118">v-v:tense-v:pred.pn</ta>
            <ta e="T120" id="Seg_3213" s="T119">adv</ta>
            <ta e="T121" id="Seg_3214" s="T120">cardnum</ta>
            <ta e="T122" id="Seg_3215" s="T121">n-n:case</ta>
            <ta e="T123" id="Seg_3216" s="T122">v-v:tense-v:poss.pn</ta>
            <ta e="T124" id="Seg_3217" s="T123">dempro</ta>
            <ta e="T125" id="Seg_3218" s="T124">v-v:cvb</ta>
            <ta e="T126" id="Seg_3219" s="T125">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T127" id="Seg_3220" s="T126">pers-pro:case</ta>
            <ta e="T128" id="Seg_3221" s="T127">dempro</ta>
            <ta e="T129" id="Seg_3222" s="T128">n-n:case</ta>
            <ta e="T130" id="Seg_3223" s="T129">v-v:ptcp</ta>
            <ta e="T131" id="Seg_3224" s="T130">n-n:poss-n:case</ta>
            <ta e="T132" id="Seg_3225" s="T131">v-v:tense-v:mood-v:pred.pn</ta>
            <ta e="T133" id="Seg_3226" s="T132">v-v:tense-v:pred.pn</ta>
            <ta e="T134" id="Seg_3227" s="T133">cardnum</ta>
            <ta e="T135" id="Seg_3228" s="T134">n-n:case</ta>
            <ta e="T136" id="Seg_3229" s="T135">ptcl</ta>
            <ta e="T137" id="Seg_3230" s="T136">v-v:ptcp</ta>
            <ta e="T138" id="Seg_3231" s="T137">v-v:tense-v:pred.pn</ta>
            <ta e="T139" id="Seg_3232" s="T138">dempro-pro:case</ta>
            <ta e="T140" id="Seg_3233" s="T139">cardnum</ta>
            <ta e="T141" id="Seg_3234" s="T140">n-n:case</ta>
            <ta e="T142" id="Seg_3235" s="T141">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T143" id="Seg_3236" s="T142">dempro</ta>
            <ta e="T144" id="Seg_3237" s="T143">n-n:(num)-n:case</ta>
            <ta e="T145" id="Seg_3238" s="T144">que-pro:case</ta>
            <ta e="T146" id="Seg_3239" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_3240" s="T146">n-n:case</ta>
            <ta e="T148" id="Seg_3241" s="T147">v-v:(neg)-v:tense-v:pred.pn</ta>
            <ta e="T149" id="Seg_3242" s="T148">dempro-pro:case</ta>
            <ta e="T150" id="Seg_3243" s="T149">v-v:tense-v:pred.pn</ta>
            <ta e="T151" id="Seg_3244" s="T150">adj</ta>
            <ta e="T152" id="Seg_3245" s="T151">n-n:case</ta>
            <ta e="T153" id="Seg_3246" s="T152">v-v&gt;v-v:ptcp-v:(case)</ta>
            <ta e="T154" id="Seg_3247" s="T153">dempro</ta>
            <ta e="T155" id="Seg_3248" s="T154">n-n:poss-n:case</ta>
            <ta e="T156" id="Seg_3249" s="T155">v-v:tense-v:pred.pn</ta>
            <ta e="T157" id="Seg_3250" s="T156">dempro-pro:case</ta>
            <ta e="T158" id="Seg_3251" s="T157">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T159" id="Seg_3252" s="T158">post</ta>
            <ta e="T160" id="Seg_3253" s="T159">n-n:case</ta>
            <ta e="T161" id="Seg_3254" s="T160">adv</ta>
            <ta e="T162" id="Seg_3255" s="T161">v-v:tense-v:pred.pn</ta>
            <ta e="T163" id="Seg_3256" s="T162">n-n:case</ta>
            <ta e="T164" id="Seg_3257" s="T163">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T165" id="Seg_3258" s="T164">v-v:ptcp</ta>
            <ta e="T166" id="Seg_3259" s="T165">n-n:(poss)-n:case</ta>
            <ta e="T167" id="Seg_3260" s="T166">v-v:tense-v:pred.pn</ta>
            <ta e="T168" id="Seg_3261" s="T167">dempro</ta>
            <ta e="T169" id="Seg_3262" s="T168">v-v:mood-v:temp.pn</ta>
            <ta e="T170" id="Seg_3263" s="T169">cardnum</ta>
            <ta e="T171" id="Seg_3264" s="T170">n-n:case</ta>
            <ta e="T172" id="Seg_3265" s="T171">n-n:(poss)-n:case</ta>
            <ta e="T173" id="Seg_3266" s="T172">adv</ta>
            <ta e="T174" id="Seg_3267" s="T173">pers-pro:case</ta>
            <ta e="T175" id="Seg_3268" s="T174">v-v:tense-v:poss.pn</ta>
            <ta e="T176" id="Seg_3269" s="T175">n-n:(poss)-n:case</ta>
            <ta e="T177" id="Seg_3270" s="T176">v-v:tense-v:pred.pn</ta>
            <ta e="T178" id="Seg_3271" s="T177">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T179" id="Seg_3272" s="T178">dempro-pro:case</ta>
            <ta e="T180" id="Seg_3273" s="T179">que</ta>
            <ta e="T181" id="Seg_3274" s="T180">v-v:ptcp</ta>
            <ta e="T182" id="Seg_3275" s="T181">v-v:tense-v:pred.pn</ta>
            <ta e="T183" id="Seg_3276" s="T182">v-v:cvb</ta>
            <ta e="T184" id="Seg_3277" s="T183">n-n:poss-n:case</ta>
            <ta e="T185" id="Seg_3278" s="T184">n-n:poss-n:case</ta>
            <ta e="T186" id="Seg_3279" s="T185">n-n:poss-n:case</ta>
            <ta e="T187" id="Seg_3280" s="T186">v-v:cvb</ta>
            <ta e="T188" id="Seg_3281" s="T187">v-v:tense-v:poss.pn</ta>
            <ta e="T189" id="Seg_3282" s="T188">v-v:cvb</ta>
            <ta e="T190" id="Seg_3283" s="T189">v-v:tense-v:poss.pn</ta>
            <ta e="T191" id="Seg_3284" s="T190">conj</ta>
            <ta e="T192" id="Seg_3285" s="T191">dempro</ta>
            <ta e="T193" id="Seg_3286" s="T192">n-n:case</ta>
            <ta e="T194" id="Seg_3287" s="T193">n-n:poss-n:case</ta>
            <ta e="T195" id="Seg_3288" s="T194">n-n:(poss)-n:case</ta>
            <ta e="T196" id="Seg_3289" s="T195">adv</ta>
            <ta e="T197" id="Seg_3290" s="T196">v-v:tense-v:pred.pn</ta>
            <ta e="T198" id="Seg_3291" s="T197">v-v:cvb</ta>
            <ta e="T199" id="Seg_3292" s="T198">n-n:(poss)-n:case</ta>
            <ta e="T200" id="Seg_3293" s="T199">n-n:poss-n:case</ta>
            <ta e="T201" id="Seg_3294" s="T200">v-v:tense-v:poss.pn</ta>
            <ta e="T202" id="Seg_3295" s="T201">v-v:cvb</ta>
            <ta e="T203" id="Seg_3296" s="T202">n-n:poss-n:case</ta>
            <ta e="T204" id="Seg_3297" s="T203">v-v:tense-v:poss.pn</ta>
            <ta e="T205" id="Seg_3298" s="T204">dempro-pro:case</ta>
            <ta e="T206" id="Seg_3299" s="T205">adj-adj&gt;adv</ta>
            <ta e="T207" id="Seg_3300" s="T206">v-v:tense-v:poss.pn</ta>
            <ta e="T208" id="Seg_3301" s="T207">adv</ta>
            <ta e="T209" id="Seg_3302" s="T208">n-n:(poss)-n:case</ta>
            <ta e="T210" id="Seg_3303" s="T209">v-v:(neg)-v:pred.pn</ta>
            <ta e="T211" id="Seg_3304" s="T210">dempro</ta>
            <ta e="T212" id="Seg_3305" s="T211">v-v:cvb</ta>
            <ta e="T213" id="Seg_3306" s="T212">post</ta>
            <ta e="T214" id="Seg_3307" s="T213">n-n&gt;adj-n:case</ta>
            <ta e="T215" id="Seg_3308" s="T214">v-v:tense-v:poss.pn</ta>
            <ta e="T216" id="Seg_3309" s="T215">pers-pro:case</ta>
            <ta e="T217" id="Seg_3310" s="T216">dempro</ta>
            <ta e="T218" id="Seg_3311" s="T217">que-pro:case</ta>
            <ta e="T219" id="Seg_3312" s="T218">v-v:cvb</ta>
            <ta e="T220" id="Seg_3313" s="T219">n-n:case</ta>
            <ta e="T221" id="Seg_3314" s="T220">v-v:cvb</ta>
            <ta e="T222" id="Seg_3315" s="T221">v-v:tense-v:pred.pn</ta>
            <ta e="T223" id="Seg_3316" s="T222">v-v:cvb</ta>
            <ta e="T224" id="Seg_3317" s="T223">dempro-pro:case</ta>
            <ta e="T225" id="Seg_3318" s="T224">n-n:(poss)-n:case</ta>
            <ta e="T226" id="Seg_3319" s="T225">v-v:tense-v:poss.pn</ta>
            <ta e="T227" id="Seg_3320" s="T226">pers-pro:case</ta>
            <ta e="T228" id="Seg_3321" s="T227">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T229" id="Seg_3322" s="T228">n-n:poss-n:case</ta>
            <ta e="T230" id="Seg_3323" s="T229">v-v:neg-v:(ins)-v:poss.pn</ta>
            <ta e="T231" id="Seg_3324" s="T230">n-n:case</ta>
            <ta e="T232" id="Seg_3325" s="T231">n-n:poss-n:case</ta>
            <ta e="T233" id="Seg_3326" s="T232">v-v:cvb</ta>
            <ta e="T234" id="Seg_3327" s="T233">pers-pro:case</ta>
            <ta e="T235" id="Seg_3328" s="T234">v-v:cvb</ta>
            <ta e="T236" id="Seg_3329" s="T235">v-v:cvb-v:pred.pn</ta>
            <ta e="T237" id="Seg_3330" s="T236">dempro</ta>
            <ta e="T238" id="Seg_3331" s="T237">cardnum</ta>
            <ta e="T239" id="Seg_3332" s="T238">n-n:case</ta>
            <ta e="T240" id="Seg_3333" s="T239">v-v:(ins)-v&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T241" id="Seg_3334" s="T240">adj-n:(poss)-n:case</ta>
            <ta e="T242" id="Seg_3335" s="T241">ptcl</ta>
            <ta e="T243" id="Seg_3336" s="T242">v-v:neg-v:poss.pn</ta>
            <ta e="T244" id="Seg_3337" s="T243">dempro-pro:case</ta>
            <ta e="T245" id="Seg_3338" s="T244">n-n:poss-n:case</ta>
            <ta e="T246" id="Seg_3339" s="T245">adv</ta>
            <ta e="T247" id="Seg_3340" s="T246">v-v:cvb</ta>
            <ta e="T248" id="Seg_3341" s="T247">v-v:tense-v:pred.pn</ta>
            <ta e="T249" id="Seg_3342" s="T248">dempro-pro:case</ta>
            <ta e="T250" id="Seg_3343" s="T249">n-n:case</ta>
            <ta e="T251" id="Seg_3344" s="T250">pers-pro:case</ta>
            <ta e="T252" id="Seg_3345" s="T251">v-v:mood.pn</ta>
            <ta e="T253" id="Seg_3346" s="T252">pers-pro:case</ta>
            <ta e="T254" id="Seg_3347" s="T253">pers-pro:case</ta>
            <ta e="T255" id="Seg_3348" s="T254">n-n:case</ta>
            <ta e="T256" id="Seg_3349" s="T255">n-n:poss-n:case</ta>
            <ta e="T257" id="Seg_3350" s="T256">v-v:tense-v:poss.pn</ta>
            <ta e="T258" id="Seg_3351" s="T257">v-v:tense-v:poss.pn</ta>
            <ta e="T259" id="Seg_3352" s="T258">adj</ta>
            <ta e="T260" id="Seg_3353" s="T259">adj-n:(ins)-adj&gt;n-n:case</ta>
            <ta e="T261" id="Seg_3354" s="T260">n-n:poss-n:case</ta>
            <ta e="T262" id="Seg_3355" s="T261">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T263" id="Seg_3356" s="T262">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T264" id="Seg_3357" s="T263">n-n:case</ta>
            <ta e="T265" id="Seg_3358" s="T264">v-v:cvb</ta>
            <ta e="T266" id="Seg_3359" s="T265">v-v:cvb</ta>
            <ta e="T267" id="Seg_3360" s="T266">v-v:tense-v:pred.pn</ta>
            <ta e="T268" id="Seg_3361" s="T267">v-v:cvb</ta>
            <ta e="T269" id="Seg_3362" s="T268">v-v:tense-v:poss.pn</ta>
            <ta e="T270" id="Seg_3363" s="T269">adj</ta>
            <ta e="T271" id="Seg_3364" s="T270">n-n:case</ta>
            <ta e="T272" id="Seg_3365" s="T271">v-v:cvb</ta>
            <ta e="T273" id="Seg_3366" s="T272">post</ta>
            <ta e="T274" id="Seg_3367" s="T273">n-n:poss-n:case</ta>
            <ta e="T275" id="Seg_3368" s="T274">v-v:tense-v:pred.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_3369" s="T0">adv</ta>
            <ta e="T2" id="Seg_3370" s="T1">cardnum</ta>
            <ta e="T3" id="Seg_3371" s="T2">n</ta>
            <ta e="T4" id="Seg_3372" s="T3">v</ta>
            <ta e="T5" id="Seg_3373" s="T4">v</ta>
            <ta e="T6" id="Seg_3374" s="T5">n</ta>
            <ta e="T7" id="Seg_3375" s="T6">n</ta>
            <ta e="T8" id="Seg_3376" s="T7">v</ta>
            <ta e="T9" id="Seg_3377" s="T8">aux</ta>
            <ta e="T10" id="Seg_3378" s="T9">v</ta>
            <ta e="T11" id="Seg_3379" s="T10">post</ta>
            <ta e="T12" id="Seg_3380" s="T11">v</ta>
            <ta e="T13" id="Seg_3381" s="T12">v</ta>
            <ta e="T14" id="Seg_3382" s="T13">n</ta>
            <ta e="T15" id="Seg_3383" s="T14">v</ta>
            <ta e="T16" id="Seg_3384" s="T15">aux</ta>
            <ta e="T17" id="Seg_3385" s="T16">dempro</ta>
            <ta e="T18" id="Seg_3386" s="T17">v</ta>
            <ta e="T19" id="Seg_3387" s="T18">cardnum</ta>
            <ta e="T20" id="Seg_3388" s="T19">adj</ta>
            <ta e="T21" id="Seg_3389" s="T20">n</ta>
            <ta e="T22" id="Seg_3390" s="T21">v</ta>
            <ta e="T23" id="Seg_3391" s="T22">dempro</ta>
            <ta e="T24" id="Seg_3392" s="T23">pers</ta>
            <ta e="T25" id="Seg_3393" s="T24">v</ta>
            <ta e="T26" id="Seg_3394" s="T25">n</ta>
            <ta e="T27" id="Seg_3395" s="T26">v</ta>
            <ta e="T28" id="Seg_3396" s="T27">v</ta>
            <ta e="T29" id="Seg_3397" s="T28">v</ta>
            <ta e="T30" id="Seg_3398" s="T29">v</ta>
            <ta e="T31" id="Seg_3399" s="T30">adj</ta>
            <ta e="T32" id="Seg_3400" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_3401" s="T32">v</ta>
            <ta e="T34" id="Seg_3402" s="T33">post</ta>
            <ta e="T35" id="Seg_3403" s="T34">v</ta>
            <ta e="T36" id="Seg_3404" s="T35">aux</ta>
            <ta e="T37" id="Seg_3405" s="T36">dempro</ta>
            <ta e="T38" id="Seg_3406" s="T37">n</ta>
            <ta e="T39" id="Seg_3407" s="T38">n</ta>
            <ta e="T40" id="Seg_3408" s="T39">v</ta>
            <ta e="T41" id="Seg_3409" s="T40">que</ta>
            <ta e="T42" id="Seg_3410" s="T41">v</ta>
            <ta e="T43" id="Seg_3411" s="T42">adv</ta>
            <ta e="T44" id="Seg_3412" s="T43">pers</ta>
            <ta e="T45" id="Seg_3413" s="T44">n</ta>
            <ta e="T46" id="Seg_3414" s="T45">v</ta>
            <ta e="T47" id="Seg_3415" s="T46">dempro</ta>
            <ta e="T48" id="Seg_3416" s="T47">n</ta>
            <ta e="T49" id="Seg_3417" s="T48">v</ta>
            <ta e="T50" id="Seg_3418" s="T49">v</ta>
            <ta e="T51" id="Seg_3419" s="T50">aux</ta>
            <ta e="T52" id="Seg_3420" s="T51">cardnum</ta>
            <ta e="T53" id="Seg_3421" s="T52">n</ta>
            <ta e="T54" id="Seg_3422" s="T53">v</ta>
            <ta e="T55" id="Seg_3423" s="T54">dempro</ta>
            <ta e="T56" id="Seg_3424" s="T55">v</ta>
            <ta e="T57" id="Seg_3425" s="T56">v</ta>
            <ta e="T58" id="Seg_3426" s="T57">adj</ta>
            <ta e="T59" id="Seg_3427" s="T58">n</ta>
            <ta e="T60" id="Seg_3428" s="T59">v</ta>
            <ta e="T61" id="Seg_3429" s="T60">aux</ta>
            <ta e="T62" id="Seg_3430" s="T61">cardnum</ta>
            <ta e="T63" id="Seg_3431" s="T62">adj</ta>
            <ta e="T64" id="Seg_3432" s="T63">n</ta>
            <ta e="T65" id="Seg_3433" s="T64">v</ta>
            <ta e="T66" id="Seg_3434" s="T65">v</ta>
            <ta e="T67" id="Seg_3435" s="T66">cardnum</ta>
            <ta e="T68" id="Seg_3436" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_3437" s="T68">n</ta>
            <ta e="T70" id="Seg_3438" s="T69">v</ta>
            <ta e="T71" id="Seg_3439" s="T70">ptcl</ta>
            <ta e="T72" id="Seg_3440" s="T71">v</ta>
            <ta e="T73" id="Seg_3441" s="T72">v</ta>
            <ta e="T74" id="Seg_3442" s="T73">dempro</ta>
            <ta e="T75" id="Seg_3443" s="T74">cardnum</ta>
            <ta e="T76" id="Seg_3444" s="T75">n</ta>
            <ta e="T77" id="Seg_3445" s="T76">v</ta>
            <ta e="T78" id="Seg_3446" s="T77">v</ta>
            <ta e="T79" id="Seg_3447" s="T78">dempro</ta>
            <ta e="T80" id="Seg_3448" s="T79">dempro</ta>
            <ta e="T81" id="Seg_3449" s="T80">v</ta>
            <ta e="T82" id="Seg_3450" s="T81">v</ta>
            <ta e="T83" id="Seg_3451" s="T82">v</ta>
            <ta e="T84" id="Seg_3452" s="T83">v</ta>
            <ta e="T85" id="Seg_3453" s="T84">dempro</ta>
            <ta e="T86" id="Seg_3454" s="T85">v</ta>
            <ta e="T87" id="Seg_3455" s="T86">v</ta>
            <ta e="T88" id="Seg_3456" s="T87">dempro</ta>
            <ta e="T89" id="Seg_3457" s="T88">emphpro</ta>
            <ta e="T90" id="Seg_3458" s="T89">cardnum</ta>
            <ta e="T91" id="Seg_3459" s="T90">ptcl</ta>
            <ta e="T92" id="Seg_3460" s="T91">n</ta>
            <ta e="T93" id="Seg_3461" s="T92">v</ta>
            <ta e="T94" id="Seg_3462" s="T93">v</ta>
            <ta e="T95" id="Seg_3463" s="T94">dempro</ta>
            <ta e="T96" id="Seg_3464" s="T95">v</ta>
            <ta e="T97" id="Seg_3465" s="T96">que</ta>
            <ta e="T98" id="Seg_3466" s="T97">n</ta>
            <ta e="T99" id="Seg_3467" s="T98">v</ta>
            <ta e="T100" id="Seg_3468" s="T99">ptcl</ta>
            <ta e="T101" id="Seg_3469" s="T100">v</ta>
            <ta e="T102" id="Seg_3470" s="T101">adv</ta>
            <ta e="T103" id="Seg_3471" s="T102">v</ta>
            <ta e="T104" id="Seg_3472" s="T103">pers</ta>
            <ta e="T105" id="Seg_3473" s="T104">v</ta>
            <ta e="T106" id="Seg_3474" s="T105">adv</ta>
            <ta e="T107" id="Seg_3475" s="T106">pers</ta>
            <ta e="T108" id="Seg_3476" s="T107">v</ta>
            <ta e="T109" id="Seg_3477" s="T108">v</ta>
            <ta e="T110" id="Seg_3478" s="T109">v</ta>
            <ta e="T111" id="Seg_3479" s="T110">cardnum</ta>
            <ta e="T112" id="Seg_3480" s="T111">n</ta>
            <ta e="T113" id="Seg_3481" s="T112">post</ta>
            <ta e="T114" id="Seg_3482" s="T113">v</ta>
            <ta e="T115" id="Seg_3483" s="T114">v</ta>
            <ta e="T116" id="Seg_3484" s="T115">dempro</ta>
            <ta e="T117" id="Seg_3485" s="T116">v</ta>
            <ta e="T118" id="Seg_3486" s="T117">n</ta>
            <ta e="T119" id="Seg_3487" s="T118">v</ta>
            <ta e="T120" id="Seg_3488" s="T119">adv</ta>
            <ta e="T121" id="Seg_3489" s="T120">cardnum</ta>
            <ta e="T122" id="Seg_3490" s="T121">n</ta>
            <ta e="T123" id="Seg_3491" s="T122">v</ta>
            <ta e="T124" id="Seg_3492" s="T123">dempro</ta>
            <ta e="T125" id="Seg_3493" s="T124">v</ta>
            <ta e="T126" id="Seg_3494" s="T125">v</ta>
            <ta e="T127" id="Seg_3495" s="T126">pers</ta>
            <ta e="T128" id="Seg_3496" s="T127">dempro</ta>
            <ta e="T129" id="Seg_3497" s="T128">n</ta>
            <ta e="T130" id="Seg_3498" s="T129">v</ta>
            <ta e="T131" id="Seg_3499" s="T130">n</ta>
            <ta e="T132" id="Seg_3500" s="T131">v</ta>
            <ta e="T133" id="Seg_3501" s="T132">v</ta>
            <ta e="T134" id="Seg_3502" s="T133">cardnum</ta>
            <ta e="T135" id="Seg_3503" s="T134">n</ta>
            <ta e="T136" id="Seg_3504" s="T135">ptcl</ta>
            <ta e="T137" id="Seg_3505" s="T136">v</ta>
            <ta e="T138" id="Seg_3506" s="T137">aux</ta>
            <ta e="T139" id="Seg_3507" s="T138">dempro</ta>
            <ta e="T140" id="Seg_3508" s="T139">cardnum</ta>
            <ta e="T141" id="Seg_3509" s="T140">n</ta>
            <ta e="T142" id="Seg_3510" s="T141">v</ta>
            <ta e="T143" id="Seg_3511" s="T142">dempro</ta>
            <ta e="T144" id="Seg_3512" s="T143">n</ta>
            <ta e="T145" id="Seg_3513" s="T144">que</ta>
            <ta e="T146" id="Seg_3514" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_3515" s="T146">n</ta>
            <ta e="T148" id="Seg_3516" s="T147">v</ta>
            <ta e="T149" id="Seg_3517" s="T148">dempro</ta>
            <ta e="T150" id="Seg_3518" s="T149">v</ta>
            <ta e="T151" id="Seg_3519" s="T150">adj</ta>
            <ta e="T152" id="Seg_3520" s="T151">n</ta>
            <ta e="T153" id="Seg_3521" s="T152">v</ta>
            <ta e="T154" id="Seg_3522" s="T153">dempro</ta>
            <ta e="T155" id="Seg_3523" s="T154">n</ta>
            <ta e="T156" id="Seg_3524" s="T155">v</ta>
            <ta e="T157" id="Seg_3525" s="T156">dempro</ta>
            <ta e="T158" id="Seg_3526" s="T157">v</ta>
            <ta e="T159" id="Seg_3527" s="T158">post</ta>
            <ta e="T160" id="Seg_3528" s="T159">n</ta>
            <ta e="T161" id="Seg_3529" s="T160">adv</ta>
            <ta e="T162" id="Seg_3530" s="T161">v</ta>
            <ta e="T163" id="Seg_3531" s="T162">n</ta>
            <ta e="T164" id="Seg_3532" s="T163">dempro</ta>
            <ta e="T165" id="Seg_3533" s="T164">v</ta>
            <ta e="T166" id="Seg_3534" s="T165">n</ta>
            <ta e="T167" id="Seg_3535" s="T166">v</ta>
            <ta e="T168" id="Seg_3536" s="T167">dempro</ta>
            <ta e="T169" id="Seg_3537" s="T168">v</ta>
            <ta e="T170" id="Seg_3538" s="T169">cardnum</ta>
            <ta e="T171" id="Seg_3539" s="T170">n</ta>
            <ta e="T172" id="Seg_3540" s="T171">n</ta>
            <ta e="T173" id="Seg_3541" s="T172">adv</ta>
            <ta e="T174" id="Seg_3542" s="T173">pers</ta>
            <ta e="T175" id="Seg_3543" s="T174">v</ta>
            <ta e="T176" id="Seg_3544" s="T175">n</ta>
            <ta e="T177" id="Seg_3545" s="T176">v</ta>
            <ta e="T178" id="Seg_3546" s="T177">v</ta>
            <ta e="T179" id="Seg_3547" s="T178">dempro</ta>
            <ta e="T180" id="Seg_3548" s="T179">que</ta>
            <ta e="T181" id="Seg_3549" s="T180">v</ta>
            <ta e="T182" id="Seg_3550" s="T181">aux</ta>
            <ta e="T183" id="Seg_3551" s="T182">v</ta>
            <ta e="T184" id="Seg_3552" s="T183">n</ta>
            <ta e="T185" id="Seg_3553" s="T184">n</ta>
            <ta e="T186" id="Seg_3554" s="T185">n</ta>
            <ta e="T187" id="Seg_3555" s="T186">v</ta>
            <ta e="T188" id="Seg_3556" s="T187">aux</ta>
            <ta e="T189" id="Seg_3557" s="T188">v</ta>
            <ta e="T190" id="Seg_3558" s="T189">v</ta>
            <ta e="T191" id="Seg_3559" s="T190">conj</ta>
            <ta e="T192" id="Seg_3560" s="T191">dempro</ta>
            <ta e="T193" id="Seg_3561" s="T192">n</ta>
            <ta e="T194" id="Seg_3562" s="T193">n</ta>
            <ta e="T195" id="Seg_3563" s="T194">n</ta>
            <ta e="T196" id="Seg_3564" s="T195">adv</ta>
            <ta e="T197" id="Seg_3565" s="T196">v</ta>
            <ta e="T198" id="Seg_3566" s="T197">v</ta>
            <ta e="T199" id="Seg_3567" s="T198">n</ta>
            <ta e="T200" id="Seg_3568" s="T199">n</ta>
            <ta e="T201" id="Seg_3569" s="T200">v</ta>
            <ta e="T202" id="Seg_3570" s="T201">v</ta>
            <ta e="T203" id="Seg_3571" s="T202">n</ta>
            <ta e="T204" id="Seg_3572" s="T203">v</ta>
            <ta e="T205" id="Seg_3573" s="T204">dempro</ta>
            <ta e="T206" id="Seg_3574" s="T205">adv</ta>
            <ta e="T207" id="Seg_3575" s="T206">v</ta>
            <ta e="T208" id="Seg_3576" s="T207">adv</ta>
            <ta e="T209" id="Seg_3577" s="T208">n</ta>
            <ta e="T210" id="Seg_3578" s="T209">v</ta>
            <ta e="T211" id="Seg_3579" s="T210">dempro</ta>
            <ta e="T212" id="Seg_3580" s="T211">v</ta>
            <ta e="T213" id="Seg_3581" s="T212">post</ta>
            <ta e="T214" id="Seg_3582" s="T213">adj</ta>
            <ta e="T215" id="Seg_3583" s="T214">cop</ta>
            <ta e="T216" id="Seg_3584" s="T215">pers</ta>
            <ta e="T217" id="Seg_3585" s="T216">dempro</ta>
            <ta e="T218" id="Seg_3586" s="T217">que</ta>
            <ta e="T219" id="Seg_3587" s="T218">cop</ta>
            <ta e="T220" id="Seg_3588" s="T219">n</ta>
            <ta e="T221" id="Seg_3589" s="T220">v</ta>
            <ta e="T222" id="Seg_3590" s="T221">aux</ta>
            <ta e="T223" id="Seg_3591" s="T222">v</ta>
            <ta e="T224" id="Seg_3592" s="T223">dempro</ta>
            <ta e="T225" id="Seg_3593" s="T224">n</ta>
            <ta e="T226" id="Seg_3594" s="T225">v</ta>
            <ta e="T227" id="Seg_3595" s="T226">pers</ta>
            <ta e="T228" id="Seg_3596" s="T227">emphpro</ta>
            <ta e="T229" id="Seg_3597" s="T228">n</ta>
            <ta e="T230" id="Seg_3598" s="T229">v</ta>
            <ta e="T231" id="Seg_3599" s="T230">n</ta>
            <ta e="T232" id="Seg_3600" s="T231">n</ta>
            <ta e="T233" id="Seg_3601" s="T232">v</ta>
            <ta e="T234" id="Seg_3602" s="T233">pers</ta>
            <ta e="T235" id="Seg_3603" s="T234">v</ta>
            <ta e="T236" id="Seg_3604" s="T235">v</ta>
            <ta e="T237" id="Seg_3605" s="T236">dempro</ta>
            <ta e="T238" id="Seg_3606" s="T237">cardnum</ta>
            <ta e="T239" id="Seg_3607" s="T238">n</ta>
            <ta e="T240" id="Seg_3608" s="T239">v</ta>
            <ta e="T241" id="Seg_3609" s="T240">adj</ta>
            <ta e="T242" id="Seg_3610" s="T241">ptcl</ta>
            <ta e="T243" id="Seg_3611" s="T242">v</ta>
            <ta e="T244" id="Seg_3612" s="T243">dempro</ta>
            <ta e="T245" id="Seg_3613" s="T244">n</ta>
            <ta e="T246" id="Seg_3614" s="T245">adv</ta>
            <ta e="T247" id="Seg_3615" s="T246">v</ta>
            <ta e="T248" id="Seg_3616" s="T247">aux</ta>
            <ta e="T249" id="Seg_3617" s="T248">dempro</ta>
            <ta e="T250" id="Seg_3618" s="T249">n</ta>
            <ta e="T251" id="Seg_3619" s="T250">pers</ta>
            <ta e="T252" id="Seg_3620" s="T251">v</ta>
            <ta e="T253" id="Seg_3621" s="T252">pers</ta>
            <ta e="T254" id="Seg_3622" s="T253">pers</ta>
            <ta e="T255" id="Seg_3623" s="T254">n</ta>
            <ta e="T256" id="Seg_3624" s="T255">n</ta>
            <ta e="T257" id="Seg_3625" s="T256">v</ta>
            <ta e="T258" id="Seg_3626" s="T257">v</ta>
            <ta e="T259" id="Seg_3627" s="T258">adj</ta>
            <ta e="T260" id="Seg_3628" s="T259">n</ta>
            <ta e="T261" id="Seg_3629" s="T260">n</ta>
            <ta e="T262" id="Seg_3630" s="T261">v</ta>
            <ta e="T263" id="Seg_3631" s="T262">dempro</ta>
            <ta e="T264" id="Seg_3632" s="T263">n</ta>
            <ta e="T265" id="Seg_3633" s="T264">cop</ta>
            <ta e="T266" id="Seg_3634" s="T265">v</ta>
            <ta e="T267" id="Seg_3635" s="T266">aux</ta>
            <ta e="T268" id="Seg_3636" s="T267">v</ta>
            <ta e="T269" id="Seg_3637" s="T268">v</ta>
            <ta e="T270" id="Seg_3638" s="T269">adj</ta>
            <ta e="T271" id="Seg_3639" s="T270">n</ta>
            <ta e="T272" id="Seg_3640" s="T271">v</ta>
            <ta e="T273" id="Seg_3641" s="T272">post</ta>
            <ta e="T274" id="Seg_3642" s="T273">n</ta>
            <ta e="T275" id="Seg_3643" s="T274">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T9" id="Seg_3644" s="T0">Long ago one human went hunting and fell into a hole in the earth.</ta>
            <ta e="T16" id="Seg_3645" s="T9">After the fall he came to himself and saw that he was lying on the earth.</ta>
            <ta e="T22" id="Seg_3646" s="T16">As he was lying, there came three girls on horses.</ta>
            <ta e="T36" id="Seg_3647" s="T22">He asked them: "I'm a sick human, save me", but those went away as if they hadn't heard anything.</ta>
            <ta e="T40" id="Seg_3648" s="T36">He heard those girls' words: </ta>
            <ta e="T46" id="Seg_3649" s="T40">"Where did our reindeers go?", they said.</ta>
            <ta e="T51" id="Seg_3650" s="T46">When they went home, he followed them.</ta>
            <ta e="T54" id="Seg_3651" s="T51">He came to a house.</ta>
            <ta e="T61" id="Seg_3652" s="T54">He came there and went in, there lived apparently an old man and an old woman.</ta>
            <ta e="T63" id="Seg_3653" s="T61">They have three daughters.</ta>
            <ta e="T66" id="Seg_3654" s="T63">Standing in the door he was watching.</ta>
            <ta e="T73" id="Seg_3655" s="T66">Nobody said: "You have come apparently".</ta>
            <ta e="T78" id="Seg_3656" s="T73">Then he went up to one girl and sat down.</ta>
            <ta e="T82" id="Seg_3657" s="T78">They don't notice that he has sat down.</ta>
            <ta e="T84" id="Seg_3658" s="T82">They were sitting and eating.</ta>
            <ta e="T87" id="Seg_3659" s="T84">Nobody said: "Eat".</ta>
            <ta e="T94" id="Seg_3660" s="T87">So he himself takes a small spoon and eats.</ta>
            <ta e="T96" id="Seg_3661" s="T94">They don't see that.</ta>
            <ta e="T101" id="Seg_3662" s="T96">"Why is our food decreasing?", they just say.</ta>
            <ta e="T103" id="Seg_3663" s="T101">There he thought: </ta>
            <ta e="T106" id="Seg_3664" s="T103">"They don't see me, apparently.</ta>
            <ta e="T109" id="Seg_3665" s="T106">I will try to touch them."</ta>
            <ta e="T115" id="Seg_3666" s="T109">He sat down next to one girl and touched her.</ta>
            <ta e="T119" id="Seg_3667" s="T115">There a death scream rings out.</ta>
            <ta e="T123" id="Seg_3668" s="T119">He spent three days like this.</ta>
            <ta e="T126" id="Seg_3669" s="T123">Then he thinks: </ta>
            <ta e="T133" id="Seg_3670" s="T126">"I have to find out how to leave this country."</ta>
            <ta e="T138" id="Seg_3671" s="T133">So he starts following one girl.</ta>
            <ta e="T148" id="Seg_3672" s="T138">They let two shamans shamanize, those shamans couldn't help at all.</ta>
            <ta e="T150" id="Seg_3673" s="T148">They said on this: </ta>
            <ta e="T153" id="Seg_3674" s="T150">"We have to bring the big shaman."</ta>
            <ta e="T156" id="Seg_3675" s="T153">They went to that shaman.</ta>
            <ta e="T167" id="Seg_3676" s="T156">As they have gone away, the boy embraces the girl on purpose, she runs out with a death scream.</ta>
            <ta e="T175" id="Seg_3677" s="T167">Meanwhile the glance of two eyes fell straight onto him.</ta>
            <ta e="T178" id="Seg_3678" s="T175">"Our shaman is going", they said.</ta>
            <ta e="T188" id="Seg_3679" s="T178">He thought, what he should, and hid himself unter the girl's pillow.</ta>
            <ta e="T197" id="Seg_3680" s="T188">He didn't manage to tuck himself up, the glance of that shaman's eyes fell straight onto him.</ta>
            <ta e="T204" id="Seg_3681" s="T197">The shaman went inside, fell onto him and sat down on his back.</ta>
            <ta e="T207" id="Seg_3682" s="T204">That one was fighting a lot.</ta>
            <ta e="T215" id="Seg_3683" s="T207">But the shaman didn't release him and said:</ta>
            <ta e="T223" id="Seg_3684" s="T215">"Why are you torturing this human being?", he said.</ta>
            <ta e="T226" id="Seg_3685" s="T223">On that the human told:</ta>
            <ta e="T233" id="Seg_3686" s="T226">"I didn't come here on my own free will, I fell down through a hole in the earth.</ta>
            <ta e="T243" id="Seg_3687" s="T233">As I was lying sick there, I asked these three girls for help, but no one of them helped me.</ta>
            <ta e="T248" id="Seg_3688" s="T243">That's why I disquiet them."</ta>
            <ta e="T249" id="Seg_3689" s="T248">On that:</ta>
            <ta e="T252" id="Seg_3690" s="T249">"That's right, now let her.</ta>
            <ta e="T258" id="Seg_3691" s="T252">I will bring you back to the place where you are from", he said.</ta>
            <ta e="T267" id="Seg_3692" s="T258">He sat him on an old rotten tree which became a horse and flew off.</ta>
            <ta e="T275" id="Seg_3693" s="T267">He came to himself and saw that he was lying on that rotten tree in his land.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T9" id="Seg_3694" s="T0">Vor langer Zeit ging einmal ein Mensch jagen und fiel in ein Loch in der Erde.</ta>
            <ta e="T16" id="Seg_3695" s="T9">Nach dem Fall kam er zu Sinnen und sah, dass er wohl auf der Erde lag.</ta>
            <ta e="T22" id="Seg_3696" s="T16">Als er so lag, kamen drei Mädchen auf Pferden.</ta>
            <ta e="T36" id="Seg_3697" s="T22">Darauf bat er sie: "Ich bin ein kranker Mensch, rettet mich", aber jene ritten davon, als hätten sie nichts gehört.</ta>
            <ta e="T40" id="Seg_3698" s="T36">Er hörte die Worte der Mädchen: </ta>
            <ta e="T46" id="Seg_3699" s="T40">"Wohin sind denn unsere Rentiere gegangen?", sagten sie.</ta>
            <ta e="T51" id="Seg_3700" s="T46">Als sie nach Hause gingen, folgte er ihnen.</ta>
            <ta e="T54" id="Seg_3701" s="T51">Er kam zu einem Haus.</ta>
            <ta e="T61" id="Seg_3702" s="T54">Er kam an und ging hinein, dort lebten offenbar ein alter Mann und eine alte Frau.</ta>
            <ta e="T63" id="Seg_3703" s="T61">Sie haben drei Töchter.</ta>
            <ta e="T66" id="Seg_3704" s="T63">Er stand an der Tür und schaute.</ta>
            <ta e="T73" id="Seg_3705" s="T66">Kein Mensch sagte: "Du bist wohl gekommen."</ta>
            <ta e="T78" id="Seg_3706" s="T73">Da ging er zu einem Mädchen hin und setzte sich.</ta>
            <ta e="T82" id="Seg_3707" s="T78">Sie bemerken nicht, dass er sich gesetzt hat.</ta>
            <ta e="T84" id="Seg_3708" s="T82">Sie saßen und aßen.</ta>
            <ta e="T87" id="Seg_3709" s="T84">Keiner sagte: "Iss."</ta>
            <ta e="T94" id="Seg_3710" s="T87">Da nimmt er sich selber irgendein Löffelchen und isst.</ta>
            <ta e="T96" id="Seg_3711" s="T94">Das sehen sie nicht.</ta>
            <ta e="T101" id="Seg_3712" s="T96">"Warum wird unser Essen weniger?", sagen sie nur.</ta>
            <ta e="T103" id="Seg_3713" s="T101">Da dachte er: </ta>
            <ta e="T106" id="Seg_3714" s="T103">"Sie sehen mich offenbar nicht.</ta>
            <ta e="T109" id="Seg_3715" s="T106">Ich versuche, sie zu berühren."</ta>
            <ta e="T115" id="Seg_3716" s="T109">Er setzte sich neben ein Mädchen und berührte sie.</ta>
            <ta e="T119" id="Seg_3717" s="T115">Darauf ertönt er Todesschrei.</ta>
            <ta e="T123" id="Seg_3718" s="T119">So verbrachte er drei Tage.</ta>
            <ta e="T126" id="Seg_3719" s="T123">Danach denkt er sich: </ta>
            <ta e="T133" id="Seg_3720" s="T126">"Ich muss herausfinden, wie ich aus diesem Land wegkomme."</ta>
            <ta e="T138" id="Seg_3721" s="T133">So weicht er einem Mädchen nicht mehr von der Seite.</ta>
            <ta e="T148" id="Seg_3722" s="T138">Sie ließen zwei Schamanen schamanisieren, diese Schamanen konnten überhaupt nicht helfen.</ta>
            <ta e="T150" id="Seg_3723" s="T148">Darauf sagten sie: </ta>
            <ta e="T153" id="Seg_3724" s="T150">"Wir müssen nach dem großen Schamanen schicken."</ta>
            <ta e="T156" id="Seg_3725" s="T153">Sie gingen zu jenem Schamanen.</ta>
            <ta e="T167" id="Seg_3726" s="T156">Als sie weggefahren sind, umarmt der Junge das Mädchen absichtlich, sie läuft mit einem Todesschrei hinaus.</ta>
            <ta e="T175" id="Seg_3727" s="T167">Währenddessen fiel der Blick zweier Augen direkt auf ihn.</ta>
            <ta e="T178" id="Seg_3728" s="T175">"Unser Schamane geht", sagten sie.</ta>
            <ta e="T188" id="Seg_3729" s="T178">Er überlegte, was er machen sollte, und versteckte sich unter dem Kissen des Mädchens.</ta>
            <ta e="T197" id="Seg_3730" s="T188">Er schaffte es nicht, sich zuzudecken und der Blick der Augen dieses Schamanen fiel direkt auf ihn.</ta>
            <ta e="T204" id="Seg_3731" s="T197">Der Schamane ging herein, fiel auf ihn drauf und setzte sich auf seinen Rücken.</ta>
            <ta e="T207" id="Seg_3732" s="T204">Darauf kämpfte jener mit allen Kräften.</ta>
            <ta e="T215" id="Seg_3733" s="T207">Aber der Schamane ließ ihn nicht los und sagte:</ta>
            <ta e="T223" id="Seg_3734" s="T215">"Warum quälst du den Menschen?", sagte er.</ta>
            <ta e="T226" id="Seg_3735" s="T223">Da erzählte der Mensch:</ta>
            <ta e="T233" id="Seg_3736" s="T226">"Ich bin nicht freiwillig hierher gekommen, ich bin durch ein Loch in der Erde gefallen.</ta>
            <ta e="T243" id="Seg_3737" s="T233">Als ich lag und litt, da bat ich diese drei Mädchen um Hilfe, aber keine von ihnen half.</ta>
            <ta e="T248" id="Seg_3738" s="T243">Deshalb belästige ich sie."</ta>
            <ta e="T249" id="Seg_3739" s="T248">Darauf:</ta>
            <ta e="T252" id="Seg_3740" s="T249">"Ja, das stimmt, jetzt lass sie.</ta>
            <ta e="T258" id="Seg_3741" s="T252">Ich bringe dich wieder an den Ort, wo du her bist", sagte er.</ta>
            <ta e="T267" id="Seg_3742" s="T258">Er setzte ihn auf einen verfaulten alten Baum, dieser verwandelte sich in ein Pferd und flog los.</ta>
            <ta e="T275" id="Seg_3743" s="T267">Er erinnerte sich und sah, dass er auf dem verfaulten Baum in seinem Land lag.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T9" id="Seg_3744" s="T0">В старину один человек охотился и вдруг провалился в какую-то дыру в земле.</ta>
            <ta e="T16" id="Seg_3745" s="T9">Очнулся после падения — лежит, оказывается, на земле.</ta>
            <ta e="T22" id="Seg_3746" s="T16">Когда вот так лежал, три девушки на лошадях приехали.</ta>
            <ta e="T36" id="Seg_3747" s="T22">Тогда он попросил их: "Я больной человек, спасите", но те уехали, как будто ни одна из них и не слышала</ta>
            <ta e="T40" id="Seg_3748" s="T36">Он только услыхал слова этих девушек: </ta>
            <ta e="T46" id="Seg_3749" s="T40">"Куда же подевались наши олени?", сказали.</ta>
            <ta e="T51" id="Seg_3750" s="T46">Когда те домой поехали, он пошел по их следам.</ta>
            <ta e="T54" id="Seg_3751" s="T51">К одному дому подошел.</ta>
            <ta e="T61" id="Seg_3752" s="T54">Когда вошел, там старик со старухой живут, оказывается.</ta>
            <ta e="T63" id="Seg_3753" s="T61">У них три дочери.</ta>
            <ta e="T66" id="Seg_3754" s="T63">Стоя у дверей, стал смотреть.</ta>
            <ta e="T73" id="Seg_3755" s="T66">Никто не сказал слов приветствия: "Вот и человек [в гости] пришел."</ta>
            <ta e="T78" id="Seg_3756" s="T73">Тогда он подсел к одной девушке.</ta>
            <ta e="T82" id="Seg_3757" s="T78">Не замечают, что он подсел к ней.</ta>
            <ta e="T84" id="Seg_3758" s="T82">Сидели и ели.</ta>
            <ta e="T87" id="Seg_3759" s="T84">Но никто не пригласил: "Угощайся".</ta>
            <ta e="T94" id="Seg_3760" s="T87">Сам потихоньку по одной ложке берет и ест.</ta>
            <ta e="T96" id="Seg_3761" s="T94">Этого не замечают.</ta>
            <ta e="T101" id="Seg_3762" s="T96">Только говорят: "Почему убывает наша пища?"</ta>
            <ta e="T103" id="Seg_3763" s="T101">Тут подумал: </ta>
            <ta e="T106" id="Seg_3764" s="T103">"Меня, кажется, не видят.</ta>
            <ta e="T109" id="Seg_3765" s="T106">Попробую потрогать их".</ta>
            <ta e="T115" id="Seg_3766" s="T109">Усевшись рядом с одной из девушек, дотрагивается до нее.</ta>
            <ta e="T119" id="Seg_3767" s="T115">Та смертным криком кричит.</ta>
            <ta e="T123" id="Seg_3768" s="T119">Так три дня пробыл.</ta>
            <ta e="T126" id="Seg_3769" s="T123">После этого решил: </ta>
            <ta e="T133" id="Seg_3770" s="T126">"Я должен узнать, как выбраться из этой страны."</ta>
            <ta e="T138" id="Seg_3771" s="T133">Вот стал постоянно приставать к одной девушке.</ta>
            <ta e="T148" id="Seg_3772" s="T138">Призвали двух шаманов камлать, эти шаманы ничем помочь не смогли.</ta>
            <ta e="T150" id="Seg_3773" s="T148">Тогда сказали: </ta>
            <ta e="T153" id="Seg_3774" s="T150">"Надо послать за великим шаманом."</ta>
            <ta e="T156" id="Seg_3775" s="T153">Поехали за тем шаманом.</ta>
            <ta e="T167" id="Seg_3776" s="T156">Когда уехали, парень нарочно стал обнимать девушку, она смертным криком исходит.</ta>
            <ta e="T175" id="Seg_3777" s="T167">В это время лучи двух глаз прямо на него пали.</ta>
            <ta e="T178" id="Seg_3778" s="T175">"Шаман наш едет", сказали.</ta>
            <ta e="T188" id="Seg_3779" s="T178">Тут подумал что должен делать, и спрятался под подушкой той девушки.</ta>
            <ta e="T197" id="Seg_3780" s="T188">Как ни укрывался, лучи от света глаз шамана пронизывают его.</ta>
            <ta e="T204" id="Seg_3781" s="T197">Войдя, шаман сразу напал на него, уселся верхом.</ta>
            <ta e="T207" id="Seg_3782" s="T204">Изо всех сил под ним забился.</ta>
            <ta e="T215" id="Seg_3783" s="T207">Но шаман не отпустил и сказал:</ta>
            <ta e="T223" id="Seg_3784" s="T215">"Зачем так мучаешь человека?", сказал.</ta>
            <ta e="T226" id="Seg_3785" s="T223">На это человек рассказал:</ta>
            <ta e="T233" id="Seg_3786" s="T226">"Я не по своей воле сюда прибыл, провалился через дыру в земле.</ta>
            <ta e="T243" id="Seg_3787" s="T233">Когда я лежал и страдал от боли, попросил помощи у этих трех девушек, но ни одна не помогла.</ta>
            <ta e="T248" id="Seg_3788" s="T243">Из-за этого их беспокою."</ta>
            <ta e="T249" id="Seg_3789" s="T248">На это:</ta>
            <ta e="T252" id="Seg_3790" s="T249">"Да, правда, теперь ты отстань от нее.</ta>
            <ta e="T258" id="Seg_3791" s="T252">Я тебя в родные места доставлю", он сказал.</ta>
            <ta e="T267" id="Seg_3792" s="T258">Усадил верхом на гнилое сухое дерево, оно, превратившись в коня, полетело.</ta>
            <ta e="T275" id="Seg_3793" s="T267">Опомнившись, глядит: верхом на гнилом дереве лежит на своей земле.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T9" id="Seg_3794" s="T0">В старину один человек охотился и вдруг провалился в какую-то дыру в земле.</ta>
            <ta e="T16" id="Seg_3795" s="T9">Очнулся после паления — лежит, оказывается, на земле.</ta>
            <ta e="T22" id="Seg_3796" s="T16">Когда вот так лежал, три девушки на лошадях приехали.</ta>
            <ta e="T36" id="Seg_3797" s="T22">Тогда он попросил их: "Я больной человек, спасите", — но те уехали, как будто ни одна из них и не слышала</ta>
            <ta e="T40" id="Seg_3798" s="T36">Он только услыхал слова этих девушек: </ta>
            <ta e="T46" id="Seg_3799" s="T40">"Куда же подевались наши олени?"</ta>
            <ta e="T51" id="Seg_3800" s="T46">Когда те домой поехали, он пошел по их следам.</ta>
            <ta e="T54" id="Seg_3801" s="T51">К одному дому подошел.</ta>
            <ta e="T61" id="Seg_3802" s="T54">Когда вошел, там старик со старухой живут, оказывается.</ta>
            <ta e="T63" id="Seg_3803" s="T61">У них три дочери.</ta>
            <ta e="T66" id="Seg_3804" s="T63">Стоя у дверей, стал смотреть.</ta>
            <ta e="T73" id="Seg_3805" s="T66">Никто не сказал слов приветствия: "Вот и человек [в гости] пришел".</ta>
            <ta e="T78" id="Seg_3806" s="T73">Тогда он подсел к одной девушке.</ta>
            <ta e="T82" id="Seg_3807" s="T78">Не замечают, что он подсел к ней.</ta>
            <ta e="T84" id="Seg_3808" s="T82">Сидят и едят.</ta>
            <ta e="T87" id="Seg_3809" s="T84">Но никто не пригласил: "Угощайся".</ta>
            <ta e="T94" id="Seg_3810" s="T87">Сам потихоньку по одной ложке берет и ест.</ta>
            <ta e="T96" id="Seg_3811" s="T94">Этого не замечают.</ta>
            <ta e="T101" id="Seg_3812" s="T96">Только говорят: "Почему убывает наша пища?"</ta>
            <ta e="T103" id="Seg_3813" s="T101">Тут подумал: </ta>
            <ta e="T106" id="Seg_3814" s="T103">"Меня, кажется, не видят.</ta>
            <ta e="T109" id="Seg_3815" s="T106">Попробую потрогать их".</ta>
            <ta e="T115" id="Seg_3816" s="T109">Усевшись рядом с одной из девушек, дотрагивается до нее.</ta>
            <ta e="T119" id="Seg_3817" s="T115">Та смертным криком кричит.</ta>
            <ta e="T123" id="Seg_3818" s="T119">Так три дня пробыл.</ta>
            <ta e="T126" id="Seg_3819" s="T123">После этого решил: </ta>
            <ta e="T133" id="Seg_3820" s="T126">"Я должен узнать, как выбраться из этой страны".</ta>
            <ta e="T138" id="Seg_3821" s="T133">Вот стал постоянно приставать к одной девушке.</ta>
            <ta e="T148" id="Seg_3822" s="T138">Призвали двух шаманов камлать, эти шаманы ничем помочь не смогли.</ta>
            <ta e="T150" id="Seg_3823" s="T148">Тогда сказали: </ta>
            <ta e="T153" id="Seg_3824" s="T150">"Надо послать за великим шаманом".</ta>
            <ta e="T156" id="Seg_3825" s="T153">Поехали за тем шаманом.</ta>
            <ta e="T167" id="Seg_3826" s="T156">Когда уехали, парень нарочно стал обнимать девушку, она смертным криком исходит.</ta>
            <ta e="T175" id="Seg_3827" s="T167">В это время лучи двух глаз прямо на него пали.</ta>
            <ta e="T178" id="Seg_3828" s="T175">"Шаман наш едет", — сказали.</ta>
            <ta e="T188" id="Seg_3829" s="T178">Тут подумал: "Что сможет со мной сделать?" — и спрятался под подушкой той девушки.</ta>
            <ta e="T197" id="Seg_3830" s="T188">Как ни укрывался, лучи от света глаз шамана пронизывают его.</ta>
            <ta e="T204" id="Seg_3831" s="T197">Войдя, шаман сразу напал на него, уселся верхом.</ta>
            <ta e="T207" id="Seg_3832" s="T204">Изо всех сил под ним забился.</ta>
            <ta e="T215" id="Seg_3833" s="T207">Но шаман не отпускает и говорит:</ta>
            <ta e="T223" id="Seg_3834" s="T215">— Зачем так мучаешь человека?</ta>
            <ta e="T226" id="Seg_3835" s="T223">[В ответ] на это наш человек рассказал:</ta>
            <ta e="T233" id="Seg_3836" s="T226">— Я не по своей воле сюда прибыл, провалился через дыру в земле.</ta>
            <ta e="T243" id="Seg_3837" s="T233">Когда я лежал и страдал от боли, попросил помощи у этих трех девушек, но ни одна не помогла.</ta>
            <ta e="T248" id="Seg_3838" s="T243">Из-за этого их беспокою.</ta>
            <ta e="T249" id="Seg_3839" s="T248">Тогда [ шаман] сказал:</ta>
            <ta e="T252" id="Seg_3840" s="T249">— Да, правда, теперь ты отстань от нее.</ta>
            <ta e="T258" id="Seg_3841" s="T252">Я тебя в родные места доставлю.</ta>
            <ta e="T267" id="Seg_3842" s="T258">Усадил верхом на гнилое сухое дерево, оно, превратившись в коня, полетело.</ta>
            <ta e="T275" id="Seg_3843" s="T267">Опомнившись, глядит: верхом на гнилом дереве лежит на своей земле.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T63" id="Seg_3844" s="T61">[DCh]: Number marking of the possessor is, apparently, absent here.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
