<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDB42D4EAB-FA9B-6F75-6CAD-D87A990684D5">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>YaP_1930_GroomFromUpperWorld_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\YaP_1930_GroomFromUpperWorld_flk\YaP_1930_GroomFromUpperWorld_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">379</ud-information>
            <ud-information attribute-name="# HIAT:w">301</ud-information>
            <ud-information attribute-name="# e">300</ud-information>
            <ud-information attribute-name="# HIAT:u">42</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="YaP">
            <abbreviation>YaP</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
         <tli id="T161" time="84.5" type="appl" />
         <tli id="T162" time="85.0" type="appl" />
         <tli id="T163" time="85.5" type="appl" />
         <tli id="T164" time="86.0" type="appl" />
         <tli id="T165" time="86.5" type="appl" />
         <tli id="T166" time="87.0" type="appl" />
         <tli id="T167" time="87.5" type="appl" />
         <tli id="T168" time="88.0" type="appl" />
         <tli id="T169" time="88.5" type="appl" />
         <tli id="T170" time="89.0" type="appl" />
         <tli id="T171" time="89.5" type="appl" />
         <tli id="T172" time="90.0" type="appl" />
         <tli id="T173" time="90.5" type="appl" />
         <tli id="T174" time="91.0" type="appl" />
         <tli id="T175" time="91.5" type="appl" />
         <tli id="T176" time="92.0" type="appl" />
         <tli id="T177" time="92.5" type="appl" />
         <tli id="T178" time="93.0" type="appl" />
         <tli id="T179" time="93.5" type="appl" />
         <tli id="T180" time="94.0" type="appl" />
         <tli id="T181" time="94.5" type="appl" />
         <tli id="T182" time="95.0" type="appl" />
         <tli id="T183" time="95.5" type="appl" />
         <tli id="T184" time="96.0" type="appl" />
         <tli id="T185" time="96.5" type="appl" />
         <tli id="T186" time="97.0" type="appl" />
         <tli id="T187" time="97.5" type="appl" />
         <tli id="T188" time="98.0" type="appl" />
         <tli id="T189" time="98.5" type="appl" />
         <tli id="T190" time="99.0" type="appl" />
         <tli id="T191" time="99.5" type="appl" />
         <tli id="T192" time="100.0" type="appl" />
         <tli id="T193" time="100.5" type="appl" />
         <tli id="T194" time="101.0" type="appl" />
         <tli id="T195" time="101.5" type="appl" />
         <tli id="T196" time="102.0" type="appl" />
         <tli id="T197" time="102.5" type="appl" />
         <tli id="T198" time="103.0" type="appl" />
         <tli id="T199" time="103.5" type="appl" />
         <tli id="T200" time="104.0" type="appl" />
         <tli id="T201" time="104.5" type="appl" />
         <tli id="T202" time="105.0" type="appl" />
         <tli id="T203" time="105.5" type="appl" />
         <tli id="T204" time="106.0" type="appl" />
         <tli id="T205" time="106.5" type="appl" />
         <tli id="T206" time="107.0" type="appl" />
         <tli id="T207" time="107.5" type="appl" />
         <tli id="T208" time="108.0" type="appl" />
         <tli id="T209" time="108.5" type="appl" />
         <tli id="T210" time="109.0" type="appl" />
         <tli id="T211" time="109.5" type="appl" />
         <tli id="T212" time="110.0" type="appl" />
         <tli id="T213" time="110.5" type="appl" />
         <tli id="T214" time="111.0" type="appl" />
         <tli id="T215" time="111.5" type="appl" />
         <tli id="T216" time="112.0" type="appl" />
         <tli id="T217" time="112.5" type="appl" />
         <tli id="T218" time="113.0" type="appl" />
         <tli id="T219" time="113.5" type="appl" />
         <tli id="T220" time="114.0" type="appl" />
         <tli id="T221" time="114.5" type="appl" />
         <tli id="T222" time="115.0" type="appl" />
         <tli id="T223" time="115.5" type="appl" />
         <tli id="T224" time="116.0" type="appl" />
         <tli id="T225" time="116.5" type="appl" />
         <tli id="T226" time="117.0" type="appl" />
         <tli id="T227" time="117.5" type="appl" />
         <tli id="T228" time="118.0" type="appl" />
         <tli id="T229" time="118.5" type="appl" />
         <tli id="T230" time="119.0" type="appl" />
         <tli id="T231" time="119.5" type="appl" />
         <tli id="T232" time="120.0" type="appl" />
         <tli id="T233" time="120.5" type="appl" />
         <tli id="T234" time="121.0" type="appl" />
         <tli id="T235" time="121.5" type="appl" />
         <tli id="T236" time="122.0" type="appl" />
         <tli id="T237" time="122.5" type="appl" />
         <tli id="T238" time="123.0" type="appl" />
         <tli id="T239" time="123.5" type="appl" />
         <tli id="T240" time="124.0" type="appl" />
         <tli id="T241" time="124.5" type="appl" />
         <tli id="T242" time="125.0" type="appl" />
         <tli id="T243" time="125.5" type="appl" />
         <tli id="T244" time="126.0" type="appl" />
         <tli id="T245" time="126.5" type="appl" />
         <tli id="T246" time="127.0" type="appl" />
         <tli id="T247" time="127.5" type="appl" />
         <tli id="T248" time="128.0" type="appl" />
         <tli id="T249" time="128.5" type="appl" />
         <tli id="T250" time="129.0" type="appl" />
         <tli id="T251" time="129.5" type="appl" />
         <tli id="T252" time="130.0" type="appl" />
         <tli id="T253" time="130.5" type="appl" />
         <tli id="T254" time="131.0" type="appl" />
         <tli id="T255" time="131.5" type="appl" />
         <tli id="T256" time="132.0" type="appl" />
         <tli id="T257" time="132.5" type="appl" />
         <tli id="T258" time="133.0" type="appl" />
         <tli id="T259" time="133.5" type="appl" />
         <tli id="T260" time="134.0" type="appl" />
         <tli id="T261" time="134.5" type="appl" />
         <tli id="T262" time="135.0" type="appl" />
         <tli id="T263" time="135.5" type="appl" />
         <tli id="T264" time="136.0" type="appl" />
         <tli id="T265" time="136.5" type="appl" />
         <tli id="T266" time="137.0" type="appl" />
         <tli id="T267" time="137.5" type="appl" />
         <tli id="T268" time="138.0" type="appl" />
         <tli id="T269" time="138.5" type="appl" />
         <tli id="T270" time="139.0" type="appl" />
         <tli id="T271" time="139.5" type="appl" />
         <tli id="T272" time="140.0" type="appl" />
         <tli id="T273" time="140.5" type="appl" />
         <tli id="T274" time="141.0" type="appl" />
         <tli id="T275" time="141.5" type="appl" />
         <tli id="T276" time="142.0" type="appl" />
         <tli id="T277" time="142.5" type="appl" />
         <tli id="T278" time="143.0" type="appl" />
         <tli id="T279" time="143.5" type="appl" />
         <tli id="T280" time="144.0" type="appl" />
         <tli id="T281" time="144.5" type="appl" />
         <tli id="T282" time="145.0" type="appl" />
         <tli id="T283" time="145.5" type="appl" />
         <tli id="T284" time="146.0" type="appl" />
         <tli id="T285" time="146.5" type="appl" />
         <tli id="T286" time="147.0" type="appl" />
         <tli id="T287" time="147.5" type="appl" />
         <tli id="T288" time="148.0" type="appl" />
         <tli id="T289" time="148.5" type="appl" />
         <tli id="T290" time="149.0" type="appl" />
         <tli id="T291" time="149.5" type="appl" />
         <tli id="T292" time="150.0" type="appl" />
         <tli id="T293" time="150.5" type="appl" />
         <tli id="T294" time="151.0" type="appl" />
         <tli id="T295" time="151.5" type="appl" />
         <tli id="T296" time="152.0" type="appl" />
         <tli id="T297" time="152.5" type="appl" />
         <tli id="T298" time="153.0" type="appl" />
         <tli id="T299" time="153.5" type="appl" />
         <tli id="T300" time="154.0" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="YaP"
                      type="t">
         <timeline-fork end="T241" start="T240">
            <tli id="T240.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T300" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Bɨlɨr</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">ogonnʼordoːk</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">emeːksin</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">olorbuttar</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">Kiniler</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">biːr</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">kɨːstaːktar</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T15" id="Seg_29" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">Bu</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">kɨːstara</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">ki͡ehe</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">hunuːrdarɨn</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">ututtular</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">daː</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">kepseten</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">barar</ts>
                  <nts id="Seg_53" n="HIAT:ip">.</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_56" n="HIAT:u" s="T15">
                  <nts id="Seg_57" n="HIAT:ip">"</nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">Min</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_62" n="HIAT:w" s="T16">eji͡eke</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">erge</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">barɨ͡am</ts>
                  <nts id="Seg_69" n="HIAT:ip">"</nts>
                  <nts id="Seg_70" n="HIAT:ip">,</nts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_73" n="HIAT:w" s="T19">diːr</ts>
                  <nts id="Seg_74" n="HIAT:ip">.</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_77" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_79" n="HIAT:w" s="T20">Künüs</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_82" n="HIAT:w" s="T21">kɨːstarɨn</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_85" n="HIAT:w" s="T22">ɨjɨta</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_88" n="HIAT:w" s="T23">hatɨːllar</ts>
                  <nts id="Seg_89" n="HIAT:ip">,</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_92" n="HIAT:w" s="T24">tu͡ok</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_95" n="HIAT:w" s="T25">da</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">tɨlɨ</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">kinitten</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">kɨ͡ajan</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_107" n="HIAT:w" s="T29">ɨlbattar</ts>
                  <nts id="Seg_108" n="HIAT:ip">.</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_111" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_113" n="HIAT:w" s="T30">Biːrde</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_116" n="HIAT:w" s="T31">ki͡ehe</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_119" n="HIAT:w" s="T32">hɨtan</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_122" n="HIAT:w" s="T33">barannar</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_125" n="HIAT:w" s="T34">hunuːrdarɨn</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_128" n="HIAT:w" s="T35">utupput</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_131" n="HIAT:w" s="T36">kurduk</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_134" n="HIAT:w" s="T37">haban</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_137" n="HIAT:w" s="T38">keːheller</ts>
                  <nts id="Seg_138" n="HIAT:ip">.</nts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_141" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_143" n="HIAT:w" s="T39">Kɨːstara</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_146" n="HIAT:w" s="T40">emi͡e</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_149" n="HIAT:w" s="T41">haŋaran</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_152" n="HIAT:w" s="T42">barar</ts>
                  <nts id="Seg_153" n="HIAT:ip">.</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_156" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_158" n="HIAT:w" s="T43">Manɨ͡aka</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_161" n="HIAT:w" s="T44">hunuːrdarɨn</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_164" n="HIAT:w" s="T45">habɨːtɨn</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_167" n="HIAT:w" s="T46">ɨlan</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_170" n="HIAT:w" s="T47">keːheller</ts>
                  <nts id="Seg_171" n="HIAT:ip">.</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_174" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_176" n="HIAT:w" s="T48">Körbüttere</ts>
                  <nts id="Seg_177" n="HIAT:ip">,</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_180" n="HIAT:w" s="T49">ogolorun</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_183" n="HIAT:w" s="T50">kɨtta</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_186" n="HIAT:w" s="T51">biːr</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_189" n="HIAT:w" s="T52">kihi</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_192" n="HIAT:w" s="T53">hɨtar</ts>
                  <nts id="Seg_193" n="HIAT:ip">.</nts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_196" n="HIAT:u" s="T54">
                  <nts id="Seg_197" n="HIAT:ip">"</nts>
                  <ts e="T55" id="Seg_199" n="HIAT:w" s="T54">Eː</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_202" n="HIAT:w" s="T55">dʼe</ts>
                  <nts id="Seg_203" n="HIAT:ip">,</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_206" n="HIAT:w" s="T56">kütü͡öppüt</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_209" n="HIAT:w" s="T57">bu͡ollagɨŋ</ts>
                  <nts id="Seg_210" n="HIAT:ip">"</nts>
                  <nts id="Seg_211" n="HIAT:ip">,</nts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_214" n="HIAT:w" s="T58">di͡en</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_217" n="HIAT:w" s="T59">ogonnʼordoːk</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_220" n="HIAT:w" s="T60">emeːksin</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_223" n="HIAT:w" s="T61">eteller</ts>
                  <nts id="Seg_224" n="HIAT:ip">.</nts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_227" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_229" n="HIAT:w" s="T62">Manɨga</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_232" n="HIAT:w" s="T63">kihilere</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_235" n="HIAT:w" s="T64">eter</ts>
                  <nts id="Seg_236" n="HIAT:ip">:</nts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T78" id="Seg_239" n="HIAT:u" s="T65">
                  <nts id="Seg_240" n="HIAT:ip">"</nts>
                  <ts e="T66" id="Seg_242" n="HIAT:w" s="T65">Dʼolgut</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_245" n="HIAT:w" s="T66">bu͡ollaga</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_248" n="HIAT:w" s="T67">ehi͡ene</ts>
                  <nts id="Seg_249" n="HIAT:ip">,</nts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_252" n="HIAT:w" s="T68">iti</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_255" n="HIAT:w" s="T69">miːgin</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_258" n="HIAT:w" s="T70">körbükküt</ts>
                  <nts id="Seg_259" n="HIAT:ip">,</nts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_262" n="HIAT:w" s="T71">kanna</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_265" n="HIAT:w" s="T72">kɨːskɨt</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_268" n="HIAT:w" s="T73">barbɨtɨn</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_271" n="HIAT:w" s="T74">bilimne</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_274" n="HIAT:w" s="T75">kaːlɨ͡ak</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_277" n="HIAT:w" s="T76">etigit</ts>
                  <nts id="Seg_278" n="HIAT:ip">"</nts>
                  <nts id="Seg_279" n="HIAT:ip">,</nts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_282" n="HIAT:w" s="T77">diːr</ts>
                  <nts id="Seg_283" n="HIAT:ip">.</nts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_286" n="HIAT:u" s="T78">
                  <nts id="Seg_287" n="HIAT:ip">"</nts>
                  <ts e="T79" id="Seg_289" n="HIAT:w" s="T78">Hol</ts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_292" n="HIAT:w" s="T79">da</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_295" n="HIAT:w" s="T80">bu͡ollar</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_298" n="HIAT:w" s="T81">kɨːskɨtɨn</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_301" n="HIAT:w" s="T82">ildʼi͡em</ts>
                  <nts id="Seg_302" n="HIAT:ip">.</nts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_305" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_307" n="HIAT:w" s="T83">Min</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_310" n="HIAT:w" s="T84">barɨ͡am</ts>
                  <nts id="Seg_311" n="HIAT:ip">,</nts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_314" n="HIAT:w" s="T85">harsɨn</ts>
                  <nts id="Seg_315" n="HIAT:ip">,</nts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_318" n="HIAT:w" s="T86">ogonnʼor</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_321" n="HIAT:w" s="T87">erde</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_324" n="HIAT:w" s="T88">kün</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_327" n="HIAT:w" s="T89">haːrɨːra</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_330" n="HIAT:w" s="T90">tagɨstɨn</ts>
                  <nts id="Seg_331" n="HIAT:ip">,</nts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_334" n="HIAT:w" s="T91">hette</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_337" n="HIAT:w" s="T92">kɨːl</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_340" n="HIAT:w" s="T93">ebeni</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_343" n="HIAT:w" s="T94">karbaːrar</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_346" n="HIAT:w" s="T95">bu͡olu͡oga</ts>
                  <nts id="Seg_347" n="HIAT:ip">.</nts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_350" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_352" n="HIAT:w" s="T96">Onon</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_355" n="HIAT:w" s="T97">bili͡ekkit</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_358" n="HIAT:w" s="T98">min</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_361" n="HIAT:w" s="T99">dʼolu</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_364" n="HIAT:w" s="T100">agalbɨppɨn</ts>
                  <nts id="Seg_365" n="HIAT:ip">.</nts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_368" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_370" n="HIAT:w" s="T101">Kɨtaːtan</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_373" n="HIAT:w" s="T102">ol</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_376" n="HIAT:w" s="T103">kɨːllarɨ</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_379" n="HIAT:w" s="T104">ogonnʼor</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_382" n="HIAT:w" s="T105">ölördün</ts>
                  <nts id="Seg_383" n="HIAT:ip">.</nts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_386" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_388" n="HIAT:w" s="T106">Onu</ts>
                  <nts id="Seg_389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_391" n="HIAT:w" s="T107">hülen</ts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_394" n="HIAT:w" s="T108">büterdegine</ts>
                  <nts id="Seg_395" n="HIAT:ip">,</nts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_398" n="HIAT:w" s="T109">timir</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_401" n="HIAT:w" s="T110">čibɨnan</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_404" n="HIAT:w" s="T111">kallaːntan</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_407" n="HIAT:w" s="T112">timir</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_410" n="HIAT:w" s="T113">bihikke</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_413" n="HIAT:w" s="T114">ogo</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_416" n="HIAT:w" s="T115">tühü͡öge</ts>
                  <nts id="Seg_417" n="HIAT:ip">.</nts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_420" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_422" n="HIAT:w" s="T116">Ol</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_425" n="HIAT:w" s="T117">habɨːlaːk</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_428" n="HIAT:w" s="T118">bu͡olu͡oga</ts>
                  <nts id="Seg_429" n="HIAT:ip">.</nts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_432" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_434" n="HIAT:w" s="T119">Bukatɨn</ts>
                  <nts id="Seg_435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_437" n="HIAT:w" s="T120">üs</ts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_440" n="HIAT:w" s="T121">konukka</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_443" n="HIAT:w" s="T122">di͡eri</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_446" n="HIAT:w" s="T123">habɨːtɨn</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_449" n="HIAT:w" s="T124">arɨjan</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_452" n="HIAT:w" s="T125">körör</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_455" n="HIAT:w" s="T126">bu͡olaːjagɨt</ts>
                  <nts id="Seg_456" n="HIAT:ip">.</nts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_459" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_461" n="HIAT:w" s="T127">Manɨ</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_464" n="HIAT:w" s="T128">eten</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_467" n="HIAT:w" s="T129">baran</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_470" n="HIAT:w" s="T130">taksɨbɨt</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_473" n="HIAT:w" s="T131">kihilere</ts>
                  <nts id="Seg_474" n="HIAT:ip">.</nts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_477" n="HIAT:u" s="T132">
                  <ts e="T133" id="Seg_479" n="HIAT:w" s="T132">Kɨːstara</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_482" n="HIAT:w" s="T133">onu</ts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_485" n="HIAT:w" s="T134">barɨspɨt</ts>
                  <nts id="Seg_486" n="HIAT:ip">.</nts>
                  <nts id="Seg_487" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_489" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_491" n="HIAT:w" s="T135">Ogonnʼordoːk</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_494" n="HIAT:w" s="T136">emeːksin</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_497" n="HIAT:w" s="T137">onu</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_500" n="HIAT:w" s="T138">ataːra</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_503" n="HIAT:w" s="T139">tahaːra</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_506" n="HIAT:w" s="T140">taksɨbɨttar</ts>
                  <nts id="Seg_507" n="HIAT:ip">.</nts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_510" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_512" n="HIAT:w" s="T141">Körbüttere</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_515" n="HIAT:w" s="T142">er</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_518" n="HIAT:w" s="T143">kihilere-kütü͡öttere</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_521" n="HIAT:w" s="T144">ulakan</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_524" n="HIAT:w" s="T145">muŋ</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_527" n="HIAT:w" s="T146">kɨːl</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_530" n="HIAT:w" s="T147">buːra</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_533" n="HIAT:w" s="T148">bu͡olan</ts>
                  <nts id="Seg_534" n="HIAT:ip">,</nts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_537" n="HIAT:w" s="T149">kɨːstara</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_540" n="HIAT:w" s="T150">kɨːl</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_543" n="HIAT:w" s="T151">ikteːnete</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_546" n="HIAT:w" s="T152">bu͡olan</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_549" n="HIAT:w" s="T153">ebeni</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_552" n="HIAT:w" s="T154">karbɨː</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_555" n="HIAT:w" s="T155">turbuttar</ts>
                  <nts id="Seg_556" n="HIAT:ip">.</nts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_559" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_561" n="HIAT:w" s="T156">Harsɨ͡arda</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_564" n="HIAT:w" s="T157">ogonnʼor</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_567" n="HIAT:w" s="T158">iːktiː</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_570" n="HIAT:w" s="T159">taksar</ts>
                  <nts id="Seg_571" n="HIAT:ip">.</nts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T166" id="Seg_574" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_576" n="HIAT:w" s="T160">Körbüte</ts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_579" n="HIAT:w" s="T161">hette</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_582" n="HIAT:w" s="T162">kɨːl</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_585" n="HIAT:w" s="T163">ebeni</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_588" n="HIAT:w" s="T164">karbaːn</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_591" n="HIAT:w" s="T165">iher</ts>
                  <nts id="Seg_592" n="HIAT:ip">.</nts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T178" id="Seg_595" n="HIAT:u" s="T166">
                  <ts e="T167" id="Seg_597" n="HIAT:w" s="T166">Manɨ</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_600" n="HIAT:w" s="T167">üŋüːtün</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_603" n="HIAT:w" s="T168">ɨlan</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_606" n="HIAT:w" s="T169">tɨːnnan</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_609" n="HIAT:w" s="T170">baran</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_612" n="HIAT:w" s="T171">hette</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_615" n="HIAT:w" s="T172">kɨːlɨ</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_618" n="HIAT:w" s="T173">barɨtɨn</ts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_621" n="HIAT:w" s="T174">bɨha</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_624" n="HIAT:w" s="T175">kejen</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_627" n="HIAT:w" s="T176">ölörtöːn</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_630" n="HIAT:w" s="T177">keːher</ts>
                  <nts id="Seg_631" n="HIAT:ip">.</nts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_634" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_636" n="HIAT:w" s="T178">Manɨ</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_639" n="HIAT:w" s="T179">hüle</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_642" n="HIAT:w" s="T180">kaːlar</ts>
                  <nts id="Seg_643" n="HIAT:ip">.</nts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_646" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_648" n="HIAT:w" s="T181">Emeːksin</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_651" n="HIAT:w" s="T182">utujan</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_654" n="HIAT:w" s="T183">kaːlbɨt</ts>
                  <nts id="Seg_655" n="HIAT:ip">.</nts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_658" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_660" n="HIAT:w" s="T184">Tüːn</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_663" n="HIAT:w" s="T185">kojutaːbɨt</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_666" n="HIAT:w" s="T186">kihi</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_669" n="HIAT:w" s="T187">uhuktubuta</ts>
                  <nts id="Seg_670" n="HIAT:ip">,</nts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_673" n="HIAT:w" s="T188">timir</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_676" n="HIAT:w" s="T189">čɨbɨnan</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_679" n="HIAT:w" s="T190">kallaːntan</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_682" n="HIAT:w" s="T191">tühen</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_685" n="HIAT:w" s="T192">turar</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_688" n="HIAT:w" s="T193">ebit</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_691" n="HIAT:w" s="T194">timir</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_694" n="HIAT:w" s="T195">bihik</ts>
                  <nts id="Seg_695" n="HIAT:ip">.</nts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T201" id="Seg_698" n="HIAT:u" s="T196">
                  <ts e="T197" id="Seg_700" n="HIAT:w" s="T196">Ihiger</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_703" n="HIAT:w" s="T197">ogo</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_706" n="HIAT:w" s="T198">baːr</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_709" n="HIAT:w" s="T199">bɨhɨːlaːk</ts>
                  <nts id="Seg_710" n="HIAT:ip">,</nts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_713" n="HIAT:w" s="T200">ɨtɨːr</ts>
                  <nts id="Seg_714" n="HIAT:ip">.</nts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T210" id="Seg_717" n="HIAT:u" s="T201">
                  <ts e="T202" id="Seg_719" n="HIAT:w" s="T201">Emeːksin</ts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_722" n="HIAT:w" s="T202">ahɨna</ts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_725" n="HIAT:w" s="T203">hanɨːr</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_728" n="HIAT:w" s="T204">da</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_731" n="HIAT:w" s="T205">herenen</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_734" n="HIAT:w" s="T206">ürdünen</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_737" n="HIAT:w" s="T207">habɨːtɨn</ts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_740" n="HIAT:w" s="T208">arɨjan</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_743" n="HIAT:w" s="T209">körör</ts>
                  <nts id="Seg_744" n="HIAT:ip">.</nts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T218" id="Seg_747" n="HIAT:u" s="T210">
                  <ts e="T211" id="Seg_749" n="HIAT:w" s="T210">Körbüte</ts>
                  <nts id="Seg_750" n="HIAT:ip">,</nts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_753" n="HIAT:w" s="T211">kaːr</ts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_756" n="HIAT:w" s="T212">kurduk</ts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_759" n="HIAT:w" s="T213">čeːlkeː</ts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_762" n="HIAT:w" s="T214">bagajɨ</ts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_765" n="HIAT:w" s="T215">ogo</ts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_768" n="HIAT:w" s="T216">hɨtar</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_771" n="HIAT:w" s="T217">ebit</ts>
                  <nts id="Seg_772" n="HIAT:ip">.</nts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T224" id="Seg_775" n="HIAT:u" s="T218">
                  <ts e="T219" id="Seg_777" n="HIAT:w" s="T218">Manɨ͡aka</ts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_780" n="HIAT:w" s="T219">ogoto</ts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_783" n="HIAT:w" s="T220">ölör</ts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_786" n="HIAT:w" s="T221">haŋata</ts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_789" n="HIAT:w" s="T222">haŋara</ts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_792" n="HIAT:w" s="T223">tüspüt</ts>
                  <nts id="Seg_793" n="HIAT:ip">:</nts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T234" id="Seg_796" n="HIAT:u" s="T224">
                  <nts id="Seg_797" n="HIAT:ip">"</nts>
                  <ts e="T225" id="Seg_799" n="HIAT:w" s="T224">Karagɨm</ts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_802" n="HIAT:w" s="T225">tehe</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_805" n="HIAT:w" s="T226">u͡olaːrɨ</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_808" n="HIAT:w" s="T227">gɨnna</ts>
                  <nts id="Seg_809" n="HIAT:ip">"</nts>
                  <nts id="Seg_810" n="HIAT:ip">,</nts>
                  <nts id="Seg_811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_813" n="HIAT:w" s="T228">diː-diː</ts>
                  <nts id="Seg_814" n="HIAT:ip">,</nts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_817" n="HIAT:w" s="T229">čɨba</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_820" n="HIAT:w" s="T230">ü͡öhe</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_823" n="HIAT:w" s="T231">kötögö</ts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_826" n="HIAT:w" s="T232">turbut</ts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_829" n="HIAT:w" s="T233">bihigin</ts>
                  <nts id="Seg_830" n="HIAT:ip">.</nts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T237" id="Seg_833" n="HIAT:u" s="T234">
                  <ts e="T235" id="Seg_835" n="HIAT:w" s="T234">Ogo</ts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_838" n="HIAT:w" s="T235">haŋata</ts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_841" n="HIAT:w" s="T236">ihillibit</ts>
                  <nts id="Seg_842" n="HIAT:ip">:</nts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T257" id="Seg_845" n="HIAT:u" s="T237">
                  <nts id="Seg_846" n="HIAT:ip">"</nts>
                  <ts e="T238" id="Seg_848" n="HIAT:w" s="T237">Bu</ts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_851" n="HIAT:w" s="T238">min</ts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_854" n="HIAT:w" s="T239">ubajɨm</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240.tx.1" id="Seg_857" n="HIAT:w" s="T240">Ürüŋ</ts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_860" n="HIAT:w" s="T240.tx.1">Ajɨː</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_863" n="HIAT:w" s="T241">u͡ola</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_866" n="HIAT:w" s="T242">ete</ts>
                  <nts id="Seg_867" n="HIAT:ip">,</nts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_870" n="HIAT:w" s="T243">miːgin</ts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_873" n="HIAT:w" s="T244">ubajɨm</ts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_876" n="HIAT:w" s="T245">ɨːppɨta</ts>
                  <nts id="Seg_877" n="HIAT:ip">,</nts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_880" n="HIAT:w" s="T246">üs</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_883" n="HIAT:w" s="T247">konukka</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_886" n="HIAT:w" s="T248">di͡eri</ts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_889" n="HIAT:w" s="T249">miːgin</ts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_892" n="HIAT:w" s="T250">bɨrtak</ts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_895" n="HIAT:w" s="T251">karaktarɨnan</ts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_898" n="HIAT:w" s="T252">körön</ts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_901" n="HIAT:w" s="T253">hokkor</ts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_904" n="HIAT:w" s="T254">oŋoru͡oktara</ts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_907" n="HIAT:w" s="T255">hu͡oga</ts>
                  <nts id="Seg_908" n="HIAT:ip">"</nts>
                  <nts id="Seg_909" n="HIAT:ip">,</nts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_912" n="HIAT:w" s="T256">di͡en</ts>
                  <nts id="Seg_913" n="HIAT:ip">.</nts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T265" id="Seg_916" n="HIAT:u" s="T257">
                  <nts id="Seg_917" n="HIAT:ip">"</nts>
                  <ts e="T258" id="Seg_919" n="HIAT:w" s="T257">Biligin</ts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_922" n="HIAT:w" s="T258">tönnü͡ökpün</ts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_925" n="HIAT:w" s="T259">tönnü͡ö</ts>
                  <nts id="Seg_926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_928" n="HIAT:w" s="T260">hu͡okpun</ts>
                  <nts id="Seg_929" n="HIAT:ip">,</nts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_932" n="HIAT:w" s="T261">ol</ts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_935" n="HIAT:w" s="T262">ubajɨm</ts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_938" n="HIAT:w" s="T263">bili͡e</ts>
                  <nts id="Seg_939" n="HIAT:ip">"</nts>
                  <nts id="Seg_940" n="HIAT:ip">,</nts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_943" n="HIAT:w" s="T264">di͡ebit</ts>
                  <nts id="Seg_944" n="HIAT:ip">.</nts>
                  <nts id="Seg_945" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T276" id="Seg_947" n="HIAT:u" s="T265">
                  <ts e="T266" id="Seg_949" n="HIAT:w" s="T265">Ol</ts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_952" n="HIAT:w" s="T266">biri͡emege</ts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_955" n="HIAT:w" s="T267">ogonnʼor</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_958" n="HIAT:w" s="T268">kɨːllarɨn</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_961" n="HIAT:w" s="T269">hülen</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_964" n="HIAT:w" s="T270">ikki</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_967" n="HIAT:w" s="T271">buːtun</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_970" n="HIAT:w" s="T272">hannɨgar</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_973" n="HIAT:w" s="T273">uːran</ts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_976" n="HIAT:w" s="T274">kelen</ts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_979" n="HIAT:w" s="T275">ispit</ts>
                  <nts id="Seg_980" n="HIAT:ip">.</nts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T280" id="Seg_983" n="HIAT:u" s="T276">
                  <ts e="T277" id="Seg_985" n="HIAT:w" s="T276">Manɨ</ts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_988" n="HIAT:w" s="T277">emeːksine</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_991" n="HIAT:w" s="T278">körsön</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_994" n="HIAT:w" s="T279">kepsiːr</ts>
                  <nts id="Seg_995" n="HIAT:ip">.</nts>
                  <nts id="Seg_996" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T284" id="Seg_998" n="HIAT:u" s="T280">
                  <ts e="T281" id="Seg_1000" n="HIAT:w" s="T280">Ogonnʼor</ts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1003" n="HIAT:w" s="T281">itegejimne</ts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1006" n="HIAT:w" s="T282">hüːren</ts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1009" n="HIAT:w" s="T283">keler</ts>
                  <nts id="Seg_1010" n="HIAT:ip">.</nts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T290" id="Seg_1013" n="HIAT:u" s="T284">
                  <ts e="T285" id="Seg_1015" n="HIAT:w" s="T284">Hu͡ogun</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1018" n="HIAT:w" s="T285">körön</ts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1021" n="HIAT:w" s="T286">baran</ts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1024" n="HIAT:w" s="T287">emeːksinin</ts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1027" n="HIAT:w" s="T288">ettiːr</ts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1030" n="HIAT:w" s="T289">kolunan</ts>
                  <nts id="Seg_1031" n="HIAT:ip">.</nts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T300" id="Seg_1034" n="HIAT:u" s="T290">
                  <ts e="T291" id="Seg_1036" n="HIAT:w" s="T290">Emeːksine</ts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1039" n="HIAT:w" s="T291">aŋar</ts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1042" n="HIAT:w" s="T292">kolun</ts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1045" n="HIAT:w" s="T293">ɨlar</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1048" n="HIAT:w" s="T294">da</ts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1051" n="HIAT:w" s="T295">ettehen</ts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1054" n="HIAT:w" s="T296">beje</ts>
                  <nts id="Seg_1055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1057" n="HIAT:w" s="T297">bejelerin</ts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1060" n="HIAT:w" s="T298">ölörön</ts>
                  <nts id="Seg_1061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1063" n="HIAT:w" s="T299">keːheller</ts>
                  <nts id="Seg_1064" n="HIAT:ip">.</nts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T300" id="Seg_1066" n="sc" s="T0">
               <ts e="T1" id="Seg_1068" n="e" s="T0">Bɨlɨr </ts>
               <ts e="T2" id="Seg_1070" n="e" s="T1">ogonnʼordoːk </ts>
               <ts e="T3" id="Seg_1072" n="e" s="T2">emeːksin </ts>
               <ts e="T4" id="Seg_1074" n="e" s="T3">olorbuttar. </ts>
               <ts e="T5" id="Seg_1076" n="e" s="T4">Kiniler </ts>
               <ts e="T6" id="Seg_1078" n="e" s="T5">biːr </ts>
               <ts e="T7" id="Seg_1080" n="e" s="T6">kɨːstaːktar. </ts>
               <ts e="T8" id="Seg_1082" n="e" s="T7">Bu </ts>
               <ts e="T9" id="Seg_1084" n="e" s="T8">kɨːstara </ts>
               <ts e="T10" id="Seg_1086" n="e" s="T9">ki͡ehe </ts>
               <ts e="T11" id="Seg_1088" n="e" s="T10">hunuːrdarɨn </ts>
               <ts e="T12" id="Seg_1090" n="e" s="T11">ututtular </ts>
               <ts e="T13" id="Seg_1092" n="e" s="T12">daː </ts>
               <ts e="T14" id="Seg_1094" n="e" s="T13">kepseten </ts>
               <ts e="T15" id="Seg_1096" n="e" s="T14">barar. </ts>
               <ts e="T16" id="Seg_1098" n="e" s="T15">"Min </ts>
               <ts e="T17" id="Seg_1100" n="e" s="T16">eji͡eke </ts>
               <ts e="T18" id="Seg_1102" n="e" s="T17">erge </ts>
               <ts e="T19" id="Seg_1104" n="e" s="T18">barɨ͡am", </ts>
               <ts e="T20" id="Seg_1106" n="e" s="T19">diːr. </ts>
               <ts e="T21" id="Seg_1108" n="e" s="T20">Künüs </ts>
               <ts e="T22" id="Seg_1110" n="e" s="T21">kɨːstarɨn </ts>
               <ts e="T23" id="Seg_1112" n="e" s="T22">ɨjɨta </ts>
               <ts e="T24" id="Seg_1114" n="e" s="T23">hatɨːllar, </ts>
               <ts e="T25" id="Seg_1116" n="e" s="T24">tu͡ok </ts>
               <ts e="T26" id="Seg_1118" n="e" s="T25">da </ts>
               <ts e="T27" id="Seg_1120" n="e" s="T26">tɨlɨ </ts>
               <ts e="T28" id="Seg_1122" n="e" s="T27">kinitten </ts>
               <ts e="T29" id="Seg_1124" n="e" s="T28">kɨ͡ajan </ts>
               <ts e="T30" id="Seg_1126" n="e" s="T29">ɨlbattar. </ts>
               <ts e="T31" id="Seg_1128" n="e" s="T30">Biːrde </ts>
               <ts e="T32" id="Seg_1130" n="e" s="T31">ki͡ehe </ts>
               <ts e="T33" id="Seg_1132" n="e" s="T32">hɨtan </ts>
               <ts e="T34" id="Seg_1134" n="e" s="T33">barannar </ts>
               <ts e="T35" id="Seg_1136" n="e" s="T34">hunuːrdarɨn </ts>
               <ts e="T36" id="Seg_1138" n="e" s="T35">utupput </ts>
               <ts e="T37" id="Seg_1140" n="e" s="T36">kurduk </ts>
               <ts e="T38" id="Seg_1142" n="e" s="T37">haban </ts>
               <ts e="T39" id="Seg_1144" n="e" s="T38">keːheller. </ts>
               <ts e="T40" id="Seg_1146" n="e" s="T39">Kɨːstara </ts>
               <ts e="T41" id="Seg_1148" n="e" s="T40">emi͡e </ts>
               <ts e="T42" id="Seg_1150" n="e" s="T41">haŋaran </ts>
               <ts e="T43" id="Seg_1152" n="e" s="T42">barar. </ts>
               <ts e="T44" id="Seg_1154" n="e" s="T43">Manɨ͡aka </ts>
               <ts e="T45" id="Seg_1156" n="e" s="T44">hunuːrdarɨn </ts>
               <ts e="T46" id="Seg_1158" n="e" s="T45">habɨːtɨn </ts>
               <ts e="T47" id="Seg_1160" n="e" s="T46">ɨlan </ts>
               <ts e="T48" id="Seg_1162" n="e" s="T47">keːheller. </ts>
               <ts e="T49" id="Seg_1164" n="e" s="T48">Körbüttere, </ts>
               <ts e="T50" id="Seg_1166" n="e" s="T49">ogolorun </ts>
               <ts e="T51" id="Seg_1168" n="e" s="T50">kɨtta </ts>
               <ts e="T52" id="Seg_1170" n="e" s="T51">biːr </ts>
               <ts e="T53" id="Seg_1172" n="e" s="T52">kihi </ts>
               <ts e="T54" id="Seg_1174" n="e" s="T53">hɨtar. </ts>
               <ts e="T55" id="Seg_1176" n="e" s="T54">"Eː </ts>
               <ts e="T56" id="Seg_1178" n="e" s="T55">dʼe, </ts>
               <ts e="T57" id="Seg_1180" n="e" s="T56">kütü͡öppüt </ts>
               <ts e="T58" id="Seg_1182" n="e" s="T57">bu͡ollagɨŋ", </ts>
               <ts e="T59" id="Seg_1184" n="e" s="T58">di͡en </ts>
               <ts e="T60" id="Seg_1186" n="e" s="T59">ogonnʼordoːk </ts>
               <ts e="T61" id="Seg_1188" n="e" s="T60">emeːksin </ts>
               <ts e="T62" id="Seg_1190" n="e" s="T61">eteller. </ts>
               <ts e="T63" id="Seg_1192" n="e" s="T62">Manɨga </ts>
               <ts e="T64" id="Seg_1194" n="e" s="T63">kihilere </ts>
               <ts e="T65" id="Seg_1196" n="e" s="T64">eter: </ts>
               <ts e="T66" id="Seg_1198" n="e" s="T65">"Dʼolgut </ts>
               <ts e="T67" id="Seg_1200" n="e" s="T66">bu͡ollaga </ts>
               <ts e="T68" id="Seg_1202" n="e" s="T67">ehi͡ene, </ts>
               <ts e="T69" id="Seg_1204" n="e" s="T68">iti </ts>
               <ts e="T70" id="Seg_1206" n="e" s="T69">miːgin </ts>
               <ts e="T71" id="Seg_1208" n="e" s="T70">körbükküt, </ts>
               <ts e="T72" id="Seg_1210" n="e" s="T71">kanna </ts>
               <ts e="T73" id="Seg_1212" n="e" s="T72">kɨːskɨt </ts>
               <ts e="T74" id="Seg_1214" n="e" s="T73">barbɨtɨn </ts>
               <ts e="T75" id="Seg_1216" n="e" s="T74">bilimne </ts>
               <ts e="T76" id="Seg_1218" n="e" s="T75">kaːlɨ͡ak </ts>
               <ts e="T77" id="Seg_1220" n="e" s="T76">etigit", </ts>
               <ts e="T78" id="Seg_1222" n="e" s="T77">diːr. </ts>
               <ts e="T79" id="Seg_1224" n="e" s="T78">"Hol </ts>
               <ts e="T80" id="Seg_1226" n="e" s="T79">da </ts>
               <ts e="T81" id="Seg_1228" n="e" s="T80">bu͡ollar </ts>
               <ts e="T82" id="Seg_1230" n="e" s="T81">kɨːskɨtɨn </ts>
               <ts e="T83" id="Seg_1232" n="e" s="T82">ildʼi͡em. </ts>
               <ts e="T84" id="Seg_1234" n="e" s="T83">Min </ts>
               <ts e="T85" id="Seg_1236" n="e" s="T84">barɨ͡am, </ts>
               <ts e="T86" id="Seg_1238" n="e" s="T85">harsɨn, </ts>
               <ts e="T87" id="Seg_1240" n="e" s="T86">ogonnʼor </ts>
               <ts e="T88" id="Seg_1242" n="e" s="T87">erde </ts>
               <ts e="T89" id="Seg_1244" n="e" s="T88">kün </ts>
               <ts e="T90" id="Seg_1246" n="e" s="T89">haːrɨːra </ts>
               <ts e="T91" id="Seg_1248" n="e" s="T90">tagɨstɨn, </ts>
               <ts e="T92" id="Seg_1250" n="e" s="T91">hette </ts>
               <ts e="T93" id="Seg_1252" n="e" s="T92">kɨːl </ts>
               <ts e="T94" id="Seg_1254" n="e" s="T93">ebeni </ts>
               <ts e="T95" id="Seg_1256" n="e" s="T94">karbaːrar </ts>
               <ts e="T96" id="Seg_1258" n="e" s="T95">bu͡olu͡oga. </ts>
               <ts e="T97" id="Seg_1260" n="e" s="T96">Onon </ts>
               <ts e="T98" id="Seg_1262" n="e" s="T97">bili͡ekkit </ts>
               <ts e="T99" id="Seg_1264" n="e" s="T98">min </ts>
               <ts e="T100" id="Seg_1266" n="e" s="T99">dʼolu </ts>
               <ts e="T101" id="Seg_1268" n="e" s="T100">agalbɨppɨn. </ts>
               <ts e="T102" id="Seg_1270" n="e" s="T101">Kɨtaːtan </ts>
               <ts e="T103" id="Seg_1272" n="e" s="T102">ol </ts>
               <ts e="T104" id="Seg_1274" n="e" s="T103">kɨːllarɨ </ts>
               <ts e="T105" id="Seg_1276" n="e" s="T104">ogonnʼor </ts>
               <ts e="T106" id="Seg_1278" n="e" s="T105">ölördün. </ts>
               <ts e="T107" id="Seg_1280" n="e" s="T106">Onu </ts>
               <ts e="T108" id="Seg_1282" n="e" s="T107">hülen </ts>
               <ts e="T109" id="Seg_1284" n="e" s="T108">büterdegine, </ts>
               <ts e="T110" id="Seg_1286" n="e" s="T109">timir </ts>
               <ts e="T111" id="Seg_1288" n="e" s="T110">čibɨnan </ts>
               <ts e="T112" id="Seg_1290" n="e" s="T111">kallaːntan </ts>
               <ts e="T113" id="Seg_1292" n="e" s="T112">timir </ts>
               <ts e="T114" id="Seg_1294" n="e" s="T113">bihikke </ts>
               <ts e="T115" id="Seg_1296" n="e" s="T114">ogo </ts>
               <ts e="T116" id="Seg_1298" n="e" s="T115">tühü͡öge. </ts>
               <ts e="T117" id="Seg_1300" n="e" s="T116">Ol </ts>
               <ts e="T118" id="Seg_1302" n="e" s="T117">habɨːlaːk </ts>
               <ts e="T119" id="Seg_1304" n="e" s="T118">bu͡olu͡oga. </ts>
               <ts e="T120" id="Seg_1306" n="e" s="T119">Bukatɨn </ts>
               <ts e="T121" id="Seg_1308" n="e" s="T120">üs </ts>
               <ts e="T122" id="Seg_1310" n="e" s="T121">konukka </ts>
               <ts e="T123" id="Seg_1312" n="e" s="T122">di͡eri </ts>
               <ts e="T124" id="Seg_1314" n="e" s="T123">habɨːtɨn </ts>
               <ts e="T125" id="Seg_1316" n="e" s="T124">arɨjan </ts>
               <ts e="T126" id="Seg_1318" n="e" s="T125">körör </ts>
               <ts e="T127" id="Seg_1320" n="e" s="T126">bu͡olaːjagɨt. </ts>
               <ts e="T128" id="Seg_1322" n="e" s="T127">Manɨ </ts>
               <ts e="T129" id="Seg_1324" n="e" s="T128">eten </ts>
               <ts e="T130" id="Seg_1326" n="e" s="T129">baran </ts>
               <ts e="T131" id="Seg_1328" n="e" s="T130">taksɨbɨt </ts>
               <ts e="T132" id="Seg_1330" n="e" s="T131">kihilere. </ts>
               <ts e="T133" id="Seg_1332" n="e" s="T132">Kɨːstara </ts>
               <ts e="T134" id="Seg_1334" n="e" s="T133">onu </ts>
               <ts e="T135" id="Seg_1336" n="e" s="T134">barɨspɨt. </ts>
               <ts e="T136" id="Seg_1338" n="e" s="T135">Ogonnʼordoːk </ts>
               <ts e="T137" id="Seg_1340" n="e" s="T136">emeːksin </ts>
               <ts e="T138" id="Seg_1342" n="e" s="T137">onu </ts>
               <ts e="T139" id="Seg_1344" n="e" s="T138">ataːra </ts>
               <ts e="T140" id="Seg_1346" n="e" s="T139">tahaːra </ts>
               <ts e="T141" id="Seg_1348" n="e" s="T140">taksɨbɨttar. </ts>
               <ts e="T142" id="Seg_1350" n="e" s="T141">Körbüttere </ts>
               <ts e="T143" id="Seg_1352" n="e" s="T142">er </ts>
               <ts e="T144" id="Seg_1354" n="e" s="T143">kihilere-kütü͡öttere </ts>
               <ts e="T145" id="Seg_1356" n="e" s="T144">ulakan </ts>
               <ts e="T146" id="Seg_1358" n="e" s="T145">muŋ </ts>
               <ts e="T147" id="Seg_1360" n="e" s="T146">kɨːl </ts>
               <ts e="T148" id="Seg_1362" n="e" s="T147">buːra </ts>
               <ts e="T149" id="Seg_1364" n="e" s="T148">bu͡olan, </ts>
               <ts e="T150" id="Seg_1366" n="e" s="T149">kɨːstara </ts>
               <ts e="T151" id="Seg_1368" n="e" s="T150">kɨːl </ts>
               <ts e="T152" id="Seg_1370" n="e" s="T151">ikteːnete </ts>
               <ts e="T153" id="Seg_1372" n="e" s="T152">bu͡olan </ts>
               <ts e="T154" id="Seg_1374" n="e" s="T153">ebeni </ts>
               <ts e="T155" id="Seg_1376" n="e" s="T154">karbɨː </ts>
               <ts e="T156" id="Seg_1378" n="e" s="T155">turbuttar. </ts>
               <ts e="T157" id="Seg_1380" n="e" s="T156">Harsɨ͡arda </ts>
               <ts e="T158" id="Seg_1382" n="e" s="T157">ogonnʼor </ts>
               <ts e="T159" id="Seg_1384" n="e" s="T158">iːktiː </ts>
               <ts e="T160" id="Seg_1386" n="e" s="T159">taksar. </ts>
               <ts e="T161" id="Seg_1388" n="e" s="T160">Körbüte </ts>
               <ts e="T162" id="Seg_1390" n="e" s="T161">hette </ts>
               <ts e="T163" id="Seg_1392" n="e" s="T162">kɨːl </ts>
               <ts e="T164" id="Seg_1394" n="e" s="T163">ebeni </ts>
               <ts e="T165" id="Seg_1396" n="e" s="T164">karbaːn </ts>
               <ts e="T166" id="Seg_1398" n="e" s="T165">iher. </ts>
               <ts e="T167" id="Seg_1400" n="e" s="T166">Manɨ </ts>
               <ts e="T168" id="Seg_1402" n="e" s="T167">üŋüːtün </ts>
               <ts e="T169" id="Seg_1404" n="e" s="T168">ɨlan </ts>
               <ts e="T170" id="Seg_1406" n="e" s="T169">tɨːnnan </ts>
               <ts e="T171" id="Seg_1408" n="e" s="T170">baran </ts>
               <ts e="T172" id="Seg_1410" n="e" s="T171">hette </ts>
               <ts e="T173" id="Seg_1412" n="e" s="T172">kɨːlɨ </ts>
               <ts e="T174" id="Seg_1414" n="e" s="T173">barɨtɨn </ts>
               <ts e="T175" id="Seg_1416" n="e" s="T174">bɨha </ts>
               <ts e="T176" id="Seg_1418" n="e" s="T175">kejen </ts>
               <ts e="T177" id="Seg_1420" n="e" s="T176">ölörtöːn </ts>
               <ts e="T178" id="Seg_1422" n="e" s="T177">keːher. </ts>
               <ts e="T179" id="Seg_1424" n="e" s="T178">Manɨ </ts>
               <ts e="T180" id="Seg_1426" n="e" s="T179">hüle </ts>
               <ts e="T181" id="Seg_1428" n="e" s="T180">kaːlar. </ts>
               <ts e="T182" id="Seg_1430" n="e" s="T181">Emeːksin </ts>
               <ts e="T183" id="Seg_1432" n="e" s="T182">utujan </ts>
               <ts e="T184" id="Seg_1434" n="e" s="T183">kaːlbɨt. </ts>
               <ts e="T185" id="Seg_1436" n="e" s="T184">Tüːn </ts>
               <ts e="T186" id="Seg_1438" n="e" s="T185">kojutaːbɨt </ts>
               <ts e="T187" id="Seg_1440" n="e" s="T186">kihi </ts>
               <ts e="T188" id="Seg_1442" n="e" s="T187">uhuktubuta, </ts>
               <ts e="T189" id="Seg_1444" n="e" s="T188">timir </ts>
               <ts e="T190" id="Seg_1446" n="e" s="T189">čɨbɨnan </ts>
               <ts e="T191" id="Seg_1448" n="e" s="T190">kallaːntan </ts>
               <ts e="T192" id="Seg_1450" n="e" s="T191">tühen </ts>
               <ts e="T193" id="Seg_1452" n="e" s="T192">turar </ts>
               <ts e="T194" id="Seg_1454" n="e" s="T193">ebit </ts>
               <ts e="T195" id="Seg_1456" n="e" s="T194">timir </ts>
               <ts e="T196" id="Seg_1458" n="e" s="T195">bihik. </ts>
               <ts e="T197" id="Seg_1460" n="e" s="T196">Ihiger </ts>
               <ts e="T198" id="Seg_1462" n="e" s="T197">ogo </ts>
               <ts e="T199" id="Seg_1464" n="e" s="T198">baːr </ts>
               <ts e="T200" id="Seg_1466" n="e" s="T199">bɨhɨːlaːk, </ts>
               <ts e="T201" id="Seg_1468" n="e" s="T200">ɨtɨːr. </ts>
               <ts e="T202" id="Seg_1470" n="e" s="T201">Emeːksin </ts>
               <ts e="T203" id="Seg_1472" n="e" s="T202">ahɨna </ts>
               <ts e="T204" id="Seg_1474" n="e" s="T203">hanɨːr </ts>
               <ts e="T205" id="Seg_1476" n="e" s="T204">da </ts>
               <ts e="T206" id="Seg_1478" n="e" s="T205">herenen </ts>
               <ts e="T207" id="Seg_1480" n="e" s="T206">ürdünen </ts>
               <ts e="T208" id="Seg_1482" n="e" s="T207">habɨːtɨn </ts>
               <ts e="T209" id="Seg_1484" n="e" s="T208">arɨjan </ts>
               <ts e="T210" id="Seg_1486" n="e" s="T209">körör. </ts>
               <ts e="T211" id="Seg_1488" n="e" s="T210">Körbüte, </ts>
               <ts e="T212" id="Seg_1490" n="e" s="T211">kaːr </ts>
               <ts e="T213" id="Seg_1492" n="e" s="T212">kurduk </ts>
               <ts e="T214" id="Seg_1494" n="e" s="T213">čeːlkeː </ts>
               <ts e="T215" id="Seg_1496" n="e" s="T214">bagajɨ </ts>
               <ts e="T216" id="Seg_1498" n="e" s="T215">ogo </ts>
               <ts e="T217" id="Seg_1500" n="e" s="T216">hɨtar </ts>
               <ts e="T218" id="Seg_1502" n="e" s="T217">ebit. </ts>
               <ts e="T219" id="Seg_1504" n="e" s="T218">Manɨ͡aka </ts>
               <ts e="T220" id="Seg_1506" n="e" s="T219">ogoto </ts>
               <ts e="T221" id="Seg_1508" n="e" s="T220">ölör </ts>
               <ts e="T222" id="Seg_1510" n="e" s="T221">haŋata </ts>
               <ts e="T223" id="Seg_1512" n="e" s="T222">haŋara </ts>
               <ts e="T224" id="Seg_1514" n="e" s="T223">tüspüt: </ts>
               <ts e="T225" id="Seg_1516" n="e" s="T224">"Karagɨm </ts>
               <ts e="T226" id="Seg_1518" n="e" s="T225">tehe </ts>
               <ts e="T227" id="Seg_1520" n="e" s="T226">u͡olaːrɨ </ts>
               <ts e="T228" id="Seg_1522" n="e" s="T227">gɨnna", </ts>
               <ts e="T229" id="Seg_1524" n="e" s="T228">diː-diː, </ts>
               <ts e="T230" id="Seg_1526" n="e" s="T229">čɨba </ts>
               <ts e="T231" id="Seg_1528" n="e" s="T230">ü͡öhe </ts>
               <ts e="T232" id="Seg_1530" n="e" s="T231">kötögö </ts>
               <ts e="T233" id="Seg_1532" n="e" s="T232">turbut </ts>
               <ts e="T234" id="Seg_1534" n="e" s="T233">bihigin. </ts>
               <ts e="T235" id="Seg_1536" n="e" s="T234">Ogo </ts>
               <ts e="T236" id="Seg_1538" n="e" s="T235">haŋata </ts>
               <ts e="T237" id="Seg_1540" n="e" s="T236">ihillibit: </ts>
               <ts e="T238" id="Seg_1542" n="e" s="T237">"Bu </ts>
               <ts e="T239" id="Seg_1544" n="e" s="T238">min </ts>
               <ts e="T240" id="Seg_1546" n="e" s="T239">ubajɨm </ts>
               <ts e="T241" id="Seg_1548" n="e" s="T240">Ürüŋ Ajɨː </ts>
               <ts e="T242" id="Seg_1550" n="e" s="T241">u͡ola </ts>
               <ts e="T243" id="Seg_1552" n="e" s="T242">ete, </ts>
               <ts e="T244" id="Seg_1554" n="e" s="T243">miːgin </ts>
               <ts e="T245" id="Seg_1556" n="e" s="T244">ubajɨm </ts>
               <ts e="T246" id="Seg_1558" n="e" s="T245">ɨːppɨta, </ts>
               <ts e="T247" id="Seg_1560" n="e" s="T246">üs </ts>
               <ts e="T248" id="Seg_1562" n="e" s="T247">konukka </ts>
               <ts e="T249" id="Seg_1564" n="e" s="T248">di͡eri </ts>
               <ts e="T250" id="Seg_1566" n="e" s="T249">miːgin </ts>
               <ts e="T251" id="Seg_1568" n="e" s="T250">bɨrtak </ts>
               <ts e="T252" id="Seg_1570" n="e" s="T251">karaktarɨnan </ts>
               <ts e="T253" id="Seg_1572" n="e" s="T252">körön </ts>
               <ts e="T254" id="Seg_1574" n="e" s="T253">hokkor </ts>
               <ts e="T255" id="Seg_1576" n="e" s="T254">oŋoru͡oktara </ts>
               <ts e="T256" id="Seg_1578" n="e" s="T255">hu͡oga", </ts>
               <ts e="T257" id="Seg_1580" n="e" s="T256">di͡en. </ts>
               <ts e="T258" id="Seg_1582" n="e" s="T257">"Biligin </ts>
               <ts e="T259" id="Seg_1584" n="e" s="T258">tönnü͡ökpün </ts>
               <ts e="T260" id="Seg_1586" n="e" s="T259">tönnü͡ö </ts>
               <ts e="T261" id="Seg_1588" n="e" s="T260">hu͡okpun, </ts>
               <ts e="T262" id="Seg_1590" n="e" s="T261">ol </ts>
               <ts e="T263" id="Seg_1592" n="e" s="T262">ubajɨm </ts>
               <ts e="T264" id="Seg_1594" n="e" s="T263">bili͡e", </ts>
               <ts e="T265" id="Seg_1596" n="e" s="T264">di͡ebit. </ts>
               <ts e="T266" id="Seg_1598" n="e" s="T265">Ol </ts>
               <ts e="T267" id="Seg_1600" n="e" s="T266">biri͡emege </ts>
               <ts e="T268" id="Seg_1602" n="e" s="T267">ogonnʼor </ts>
               <ts e="T269" id="Seg_1604" n="e" s="T268">kɨːllarɨn </ts>
               <ts e="T270" id="Seg_1606" n="e" s="T269">hülen </ts>
               <ts e="T271" id="Seg_1608" n="e" s="T270">ikki </ts>
               <ts e="T272" id="Seg_1610" n="e" s="T271">buːtun </ts>
               <ts e="T273" id="Seg_1612" n="e" s="T272">hannɨgar </ts>
               <ts e="T274" id="Seg_1614" n="e" s="T273">uːran </ts>
               <ts e="T275" id="Seg_1616" n="e" s="T274">kelen </ts>
               <ts e="T276" id="Seg_1618" n="e" s="T275">ispit. </ts>
               <ts e="T277" id="Seg_1620" n="e" s="T276">Manɨ </ts>
               <ts e="T278" id="Seg_1622" n="e" s="T277">emeːksine </ts>
               <ts e="T279" id="Seg_1624" n="e" s="T278">körsön </ts>
               <ts e="T280" id="Seg_1626" n="e" s="T279">kepsiːr. </ts>
               <ts e="T281" id="Seg_1628" n="e" s="T280">Ogonnʼor </ts>
               <ts e="T282" id="Seg_1630" n="e" s="T281">itegejimne </ts>
               <ts e="T283" id="Seg_1632" n="e" s="T282">hüːren </ts>
               <ts e="T284" id="Seg_1634" n="e" s="T283">keler. </ts>
               <ts e="T285" id="Seg_1636" n="e" s="T284">Hu͡ogun </ts>
               <ts e="T286" id="Seg_1638" n="e" s="T285">körön </ts>
               <ts e="T287" id="Seg_1640" n="e" s="T286">baran </ts>
               <ts e="T288" id="Seg_1642" n="e" s="T287">emeːksinin </ts>
               <ts e="T289" id="Seg_1644" n="e" s="T288">ettiːr </ts>
               <ts e="T290" id="Seg_1646" n="e" s="T289">kolunan. </ts>
               <ts e="T291" id="Seg_1648" n="e" s="T290">Emeːksine </ts>
               <ts e="T292" id="Seg_1650" n="e" s="T291">aŋar </ts>
               <ts e="T293" id="Seg_1652" n="e" s="T292">kolun </ts>
               <ts e="T294" id="Seg_1654" n="e" s="T293">ɨlar </ts>
               <ts e="T295" id="Seg_1656" n="e" s="T294">da </ts>
               <ts e="T296" id="Seg_1658" n="e" s="T295">ettehen </ts>
               <ts e="T297" id="Seg_1660" n="e" s="T296">beje </ts>
               <ts e="T298" id="Seg_1662" n="e" s="T297">bejelerin </ts>
               <ts e="T299" id="Seg_1664" n="e" s="T298">ölörön </ts>
               <ts e="T300" id="Seg_1666" n="e" s="T299">keːheller. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_1667" s="T0">YaP_1930_GroomFromUpperWorld_flk.001 (001.001)</ta>
            <ta e="T7" id="Seg_1668" s="T4">YaP_1930_GroomFromUpperWorld_flk.002 (001.002)</ta>
            <ta e="T15" id="Seg_1669" s="T7">YaP_1930_GroomFromUpperWorld_flk.003 (001.003)</ta>
            <ta e="T20" id="Seg_1670" s="T15">YaP_1930_GroomFromUpperWorld_flk.004 (001.004)</ta>
            <ta e="T30" id="Seg_1671" s="T20">YaP_1930_GroomFromUpperWorld_flk.005 (001.005)</ta>
            <ta e="T39" id="Seg_1672" s="T30">YaP_1930_GroomFromUpperWorld_flk.006 (001.006)</ta>
            <ta e="T43" id="Seg_1673" s="T39">YaP_1930_GroomFromUpperWorld_flk.007 (001.007)</ta>
            <ta e="T48" id="Seg_1674" s="T43">YaP_1930_GroomFromUpperWorld_flk.008 (001.008)</ta>
            <ta e="T54" id="Seg_1675" s="T48">YaP_1930_GroomFromUpperWorld_flk.009 (001.009)</ta>
            <ta e="T62" id="Seg_1676" s="T54">YaP_1930_GroomFromUpperWorld_flk.010 (001.010)</ta>
            <ta e="T65" id="Seg_1677" s="T62">YaP_1930_GroomFromUpperWorld_flk.011 (001.011)</ta>
            <ta e="T78" id="Seg_1678" s="T65">YaP_1930_GroomFromUpperWorld_flk.012 (001.012)</ta>
            <ta e="T83" id="Seg_1679" s="T78">YaP_1930_GroomFromUpperWorld_flk.013 (001.013)</ta>
            <ta e="T96" id="Seg_1680" s="T83">YaP_1930_GroomFromUpperWorld_flk.014 (001.014)</ta>
            <ta e="T101" id="Seg_1681" s="T96">YaP_1930_GroomFromUpperWorld_flk.015 (001.015)</ta>
            <ta e="T106" id="Seg_1682" s="T101">YaP_1930_GroomFromUpperWorld_flk.016 (001.016)</ta>
            <ta e="T116" id="Seg_1683" s="T106">YaP_1930_GroomFromUpperWorld_flk.017 (001.017)</ta>
            <ta e="T119" id="Seg_1684" s="T116">YaP_1930_GroomFromUpperWorld_flk.018 (001.018)</ta>
            <ta e="T127" id="Seg_1685" s="T119">YaP_1930_GroomFromUpperWorld_flk.019 (001.019)</ta>
            <ta e="T132" id="Seg_1686" s="T127">YaP_1930_GroomFromUpperWorld_flk.020 (001.020)</ta>
            <ta e="T135" id="Seg_1687" s="T132">YaP_1930_GroomFromUpperWorld_flk.021 (001.021)</ta>
            <ta e="T141" id="Seg_1688" s="T135">YaP_1930_GroomFromUpperWorld_flk.022 (001.022)</ta>
            <ta e="T156" id="Seg_1689" s="T141">YaP_1930_GroomFromUpperWorld_flk.023 (001.023)</ta>
            <ta e="T160" id="Seg_1690" s="T156">YaP_1930_GroomFromUpperWorld_flk.024 (001.024)</ta>
            <ta e="T166" id="Seg_1691" s="T160">YaP_1930_GroomFromUpperWorld_flk.025 (001.025)</ta>
            <ta e="T178" id="Seg_1692" s="T166">YaP_1930_GroomFromUpperWorld_flk.026 (001.026)</ta>
            <ta e="T181" id="Seg_1693" s="T178">YaP_1930_GroomFromUpperWorld_flk.027 (001.027)</ta>
            <ta e="T184" id="Seg_1694" s="T181">YaP_1930_GroomFromUpperWorld_flk.028 (001.028)</ta>
            <ta e="T196" id="Seg_1695" s="T184">YaP_1930_GroomFromUpperWorld_flk.029 (001.029)</ta>
            <ta e="T201" id="Seg_1696" s="T196">YaP_1930_GroomFromUpperWorld_flk.030 (001.030)</ta>
            <ta e="T210" id="Seg_1697" s="T201">YaP_1930_GroomFromUpperWorld_flk.031 (001.031)</ta>
            <ta e="T218" id="Seg_1698" s="T210">YaP_1930_GroomFromUpperWorld_flk.032 (001.032)</ta>
            <ta e="T224" id="Seg_1699" s="T218">YaP_1930_GroomFromUpperWorld_flk.033 (001.033)</ta>
            <ta e="T234" id="Seg_1700" s="T224">YaP_1930_GroomFromUpperWorld_flk.034 (001.034)</ta>
            <ta e="T237" id="Seg_1701" s="T234">YaP_1930_GroomFromUpperWorld_flk.035 (001.036)</ta>
            <ta e="T257" id="Seg_1702" s="T237">YaP_1930_GroomFromUpperWorld_flk.036 (001.037)</ta>
            <ta e="T265" id="Seg_1703" s="T257">YaP_1930_GroomFromUpperWorld_flk.037 (001.038)</ta>
            <ta e="T276" id="Seg_1704" s="T265">YaP_1930_GroomFromUpperWorld_flk.038 (001.039)</ta>
            <ta e="T280" id="Seg_1705" s="T276">YaP_1930_GroomFromUpperWorld_flk.039 (001.040)</ta>
            <ta e="T284" id="Seg_1706" s="T280">YaP_1930_GroomFromUpperWorld_flk.040 (001.041)</ta>
            <ta e="T290" id="Seg_1707" s="T284">YaP_1930_GroomFromUpperWorld_flk.041 (001.042)</ta>
            <ta e="T300" id="Seg_1708" s="T290">YaP_1930_GroomFromUpperWorld_flk.042 (001.043)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_1709" s="T0">Былыр огонньордоок эмээксин олорбуттар.</ta>
            <ta e="T7" id="Seg_1710" s="T4">Кинилэр биир кыыстаактар.</ta>
            <ta e="T15" id="Seg_1711" s="T7">Бу кыыстара киэһэ һунуурдарын утуттулар даа кэпсэтэн барар.</ta>
            <ta e="T20" id="Seg_1712" s="T15">— Мин эйиэкэ эргэ барыам, — диир.</ta>
            <ta e="T30" id="Seg_1713" s="T20">Күнүс кыыстарын ыйыта һатыыллар, туок да тылы киниттэн кыайан ылбаттар.</ta>
            <ta e="T39" id="Seg_1714" s="T30">Биирдэ киэһэ һытан бараннар һунуурдарын утуппут курдук һабан кээһэллэр.</ta>
            <ta e="T43" id="Seg_1715" s="T39">Кыыстара эмиэ һаҥаран барар.</ta>
            <ta e="T48" id="Seg_1716" s="T43">Маныака һунуурдарын һабыытын ылан кээһэллэр.</ta>
            <ta e="T54" id="Seg_1717" s="T48">Көрбүттэрэ: оголорун кытта биир киһи һытар.</ta>
            <ta e="T62" id="Seg_1718" s="T54">— Ээ дьэ, күтүөппүт буоллагыҥ, — диэн огонньордоок эмээксин этэллэр.</ta>
            <ta e="T65" id="Seg_1719" s="T62">Маныга киһилэрэ этэр:</ta>
            <ta e="T78" id="Seg_1720" s="T65">— Дьолгут буоллага эһиэнэ, ити миигин көрбүккүт, канна кыыскыт барбытын билимнэ каалыак этигит, — диир.</ta>
            <ta e="T83" id="Seg_1721" s="T78">— һол да буоллар кыыскытын илдьиэм.</ta>
            <ta e="T96" id="Seg_1722" s="T83">Мин барыам, һарсын, огонньор эрдэ күн һаарыыра тагыстын, һэттэ кыыл эбэни карбаарар буолуога.</ta>
            <ta e="T101" id="Seg_1723" s="T96">Онон билиэккит мин дьолу агалбыппын.</ta>
            <ta e="T106" id="Seg_1724" s="T101">Кытаатан ол кыыллары огонньор өлөрдүн.</ta>
            <ta e="T116" id="Seg_1725" s="T106">Ону һүлэн бүтэрдэгинэ, тимир чибынан каллаантан тимир биһиккэ ого түһүөгэ.</ta>
            <ta e="T119" id="Seg_1726" s="T116">Ол һабыылаак буолуога.</ta>
            <ta e="T127" id="Seg_1727" s="T119">Букатын үс конукка диэри һабыытын арыйан көрөр буолаайагыт.</ta>
            <ta e="T132" id="Seg_1728" s="T127">Маны этэн баран таксыбыт киһилэрэ.</ta>
            <ta e="T135" id="Seg_1729" s="T132">Кыыстара ону барыспыт.</ta>
            <ta e="T141" id="Seg_1730" s="T135">Огонньордоок эмээксин ону атаара таһаара таксыбыттар.</ta>
            <ta e="T156" id="Seg_1731" s="T141">Көрбүттэрэ эр киһилэрэ-күтүөттэрэ улакан муҥ кыыл буура буолан, кыыстара кыыл иктээнэтэ буолан эбэни карбыы турбуттар.</ta>
            <ta e="T160" id="Seg_1732" s="T156">һарсыарда огонньор ииктии таксар.</ta>
            <ta e="T166" id="Seg_1733" s="T160">Көрбүтэ һэттэ кыыл эбэни карбаан иһэр.</ta>
            <ta e="T178" id="Seg_1734" s="T166">Маны үҥүүтүн ылан тыыннан баран һэттэ кыылы барытын быһа кэйэн өлөртөөн кээһэр.</ta>
            <ta e="T181" id="Seg_1735" s="T178">Маны һүлэ каалар.</ta>
            <ta e="T184" id="Seg_1736" s="T181">Эмээксин утуйан каалбыт.</ta>
            <ta e="T196" id="Seg_1737" s="T184">Түүн койутаабыт киһи уһуктубута, тимир чыбынан каллаантан түһэн турар эбит тимир биһик.</ta>
            <ta e="T201" id="Seg_1738" s="T196">Иһигэр ого баар быһыылаак, ытыыр.</ta>
            <ta e="T210" id="Seg_1739" s="T201">Эмээксин аһына һаныыр да һэрэнэн үрдүнэн һабыытын арыйан көрөр.</ta>
            <ta e="T218" id="Seg_1740" s="T210">Көрбүтэ: каар курдук чээлкээ багайы ого һытар эбит.</ta>
            <ta e="T224" id="Seg_1741" s="T218">Маныака огото өлөр һаҥата һаҥара түспүт:</ta>
            <ta e="T234" id="Seg_1742" s="T224">— Карагым тэһэ уолаары гынна! — дии-дии, чыба үөһэ көтөгө турбут биһигин.</ta>
            <ta e="T237" id="Seg_1743" s="T234">Ого һаҥата иһиллибит:</ta>
            <ta e="T257" id="Seg_1744" s="T237">— Бу мин убайым Үрүҥ Айыы уола этэ, миигин убайым ыыппыта, үс конукка диэри миигин быртак карактарынан көрөн һоккор оҥоруоктара һуога, — диэн.</ta>
            <ta e="T265" id="Seg_1745" s="T257">— Билигин төннүөкпүн төннүө һуокпун, ол убайым билиэ, — диэбит.</ta>
            <ta e="T276" id="Seg_1746" s="T265">Ол бириэмэгэ огонньор кыылларын һүлэн икки буутун һанныгар ууран кэлэн испит.</ta>
            <ta e="T280" id="Seg_1747" s="T276">Маны эмээксинэ көрсөн кэпсиир.</ta>
            <ta e="T284" id="Seg_1748" s="T280">Огонньор итэгэйимнэ һүүрэн кэлэр. һуогун көрөн баран эмээксинин эттиир колунан.</ta>
            <ta e="T290" id="Seg_1749" s="T284">Һуогун көрөн баран эмээксинин эттиир колунан.</ta>
            <ta e="T300" id="Seg_1750" s="T290">Эмээксинэ аҥар колун ылар да эттэһэн бэйэ бэйэлэрин өл кээһэллэр.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_1751" s="T0">Bɨlɨr ogonnʼordoːk emeːksin olorbuttar. </ta>
            <ta e="T7" id="Seg_1752" s="T4">Kiniler biːr kɨːstaːktar. </ta>
            <ta e="T15" id="Seg_1753" s="T7">Bu kɨːstara ki͡ehe hunuːrdarɨn ututtular daː kepseten barar. </ta>
            <ta e="T20" id="Seg_1754" s="T15">"Min eji͡eke erge barɨ͡am", diːr. </ta>
            <ta e="T30" id="Seg_1755" s="T20">Künüs kɨːstarɨn ɨjɨta hatɨːllar, tu͡ok da tɨlɨ kinitten kɨ͡ajan ɨlbattar. </ta>
            <ta e="T39" id="Seg_1756" s="T30">Biːrde ki͡ehe hɨtan barannar hunuːrdarɨn utupput kurduk haban keːheller. </ta>
            <ta e="T43" id="Seg_1757" s="T39">Kɨːstara emi͡e haŋaran barar. </ta>
            <ta e="T48" id="Seg_1758" s="T43">Manɨ͡aka hunuːrdarɨn habɨːtɨn ɨlan keːheller. </ta>
            <ta e="T54" id="Seg_1759" s="T48">Körbüttere, ogolorun kɨtta biːr kihi hɨtar. </ta>
            <ta e="T62" id="Seg_1760" s="T54">"Eː dʼe, kütü͡öppüt bu͡ollagɨŋ", di͡en ogonnʼordoːk emeːksin eteller. </ta>
            <ta e="T65" id="Seg_1761" s="T62">Manɨga kihilere eter: </ta>
            <ta e="T78" id="Seg_1762" s="T65">"Dʼolgut bu͡ollaga ehi͡ene, iti miːgin körbükküt, kanna kɨːskɨt barbɨtɨn bilimne kaːlɨ͡ak etigit", diːr. </ta>
            <ta e="T83" id="Seg_1763" s="T78">"Hol da bu͡ollar kɨːskɨtɨn ildʼi͡em. </ta>
            <ta e="T96" id="Seg_1764" s="T83">Min barɨ͡am, harsɨn, ogonnʼor erde kün haːrɨːra tagɨstɨn, hette kɨːl ebeni karbaːrar bu͡olu͡oga. </ta>
            <ta e="T101" id="Seg_1765" s="T96">Onon bili͡ekkit min dʼolu agalbɨppɨn. </ta>
            <ta e="T106" id="Seg_1766" s="T101">Kɨtaːtan ol kɨːllarɨ ogonnʼor ölördün. </ta>
            <ta e="T116" id="Seg_1767" s="T106">Onu hülen büterdegine, timir čibɨnan kallaːntan timir bihikke ogo tühü͡öge. </ta>
            <ta e="T119" id="Seg_1768" s="T116">Ol habɨːlaːk bu͡olu͡oga. </ta>
            <ta e="T127" id="Seg_1769" s="T119">Bukatɨn üs konukka di͡eri habɨːtɨn arɨjan körör bu͡olaːjagɨt. </ta>
            <ta e="T132" id="Seg_1770" s="T127">Manɨ eten baran taksɨbɨt kihilere. </ta>
            <ta e="T135" id="Seg_1771" s="T132">Kɨːstara onu barɨspɨt. </ta>
            <ta e="T141" id="Seg_1772" s="T135">Ogonnʼordoːk emeːksin onu ataːra tahaːra taksɨbɨttar. </ta>
            <ta e="T156" id="Seg_1773" s="T141">Körbüttere er kihilere-kütü͡öttere ulakan muŋ kɨːl buːra bu͡olan, kɨːstara kɨːl ikteːnete bu͡olan ebeni karbɨː turbuttar. </ta>
            <ta e="T160" id="Seg_1774" s="T156">Harsɨ͡arda ogonnʼor iːktiː taksar. </ta>
            <ta e="T166" id="Seg_1775" s="T160">Körbüte hette kɨːl ebeni karbaːn iher. </ta>
            <ta e="T178" id="Seg_1776" s="T166">Manɨ üŋüːtün ɨlan tɨːnnan baran hette kɨːlɨ barɨtɨn bɨha kejen ölörtöːn keːher. </ta>
            <ta e="T181" id="Seg_1777" s="T178">Manɨ hüle kaːlar. </ta>
            <ta e="T184" id="Seg_1778" s="T181">Emeːksin utujan kaːlbɨt. </ta>
            <ta e="T196" id="Seg_1779" s="T184">Tüːn kojutaːbɨt kihi uhuktubuta, timir čɨbɨnan kallaːntan tühen turar ebit timir bihik. </ta>
            <ta e="T201" id="Seg_1780" s="T196">Ihiger ogo baːr bɨhɨːlaːk, ɨtɨːr. </ta>
            <ta e="T210" id="Seg_1781" s="T201">Emeːksin ahɨna hanɨːr da herenen ürdünen habɨːtɨn arɨjan körör. </ta>
            <ta e="T218" id="Seg_1782" s="T210">Körbüte, kaːr kurduk čeːlkeː bagajɨ ogo hɨtar ebit. </ta>
            <ta e="T224" id="Seg_1783" s="T218">Manɨ͡aka ogoto ölör haŋata haŋara tüspüt: </ta>
            <ta e="T234" id="Seg_1784" s="T224">"Karagɨm tehe u͡olaːrɨ gɨnna!", diː-diː, čɨba ü͡öhe kötögö turbut bihigin. </ta>
            <ta e="T237" id="Seg_1785" s="T234">Ogo haŋata ihillibit: </ta>
            <ta e="T257" id="Seg_1786" s="T237">"Bu min ubajɨm Ürüŋ Ajɨː u͡ola ete, miːgin ubajɨm ɨːppɨta, üs konukka di͡eri miːgin bɨrtak karaktarɨnan körön hokkor oŋoru͡oktara hu͡oga", di͡en. </ta>
            <ta e="T265" id="Seg_1787" s="T257">"Biligin tönnü͡ökpün tönnü͡ö hu͡okpun, ol ubajɨm bili͡e", di͡ebit. </ta>
            <ta e="T276" id="Seg_1788" s="T265">Ol biri͡emege ogonnʼor kɨːllarɨn hülen ikki buːtun hannɨgar uːran kelen ispit. </ta>
            <ta e="T280" id="Seg_1789" s="T276">Manɨ emeːksine körsön kepsiːr. </ta>
            <ta e="T284" id="Seg_1790" s="T280">Ogonnʼor itegejimne hüːren keler. </ta>
            <ta e="T290" id="Seg_1791" s="T284">Hu͡ogun körön baran emeːksinin ettiːr kolunan. </ta>
            <ta e="T300" id="Seg_1792" s="T290">Emeːksine aŋar kolun ɨlar da ettehen beje bejelerin ölörön keːheller. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1793" s="T0">bɨlɨr</ta>
            <ta e="T2" id="Seg_1794" s="T1">ogonnʼor-doːk</ta>
            <ta e="T3" id="Seg_1795" s="T2">emeːksin</ta>
            <ta e="T4" id="Seg_1796" s="T3">olor-but-tar</ta>
            <ta e="T5" id="Seg_1797" s="T4">kiniler</ta>
            <ta e="T6" id="Seg_1798" s="T5">biːr</ta>
            <ta e="T7" id="Seg_1799" s="T6">kɨːs-taːk-tar</ta>
            <ta e="T8" id="Seg_1800" s="T7">bu</ta>
            <ta e="T9" id="Seg_1801" s="T8">kɨːs-tara</ta>
            <ta e="T10" id="Seg_1802" s="T9">ki͡ehe</ta>
            <ta e="T11" id="Seg_1803" s="T10">hunuːr-darɨ-n</ta>
            <ta e="T12" id="Seg_1804" s="T11">utut-tu-lar</ta>
            <ta e="T13" id="Seg_1805" s="T12">daː</ta>
            <ta e="T14" id="Seg_1806" s="T13">kepset-en</ta>
            <ta e="T15" id="Seg_1807" s="T14">bar-ar</ta>
            <ta e="T16" id="Seg_1808" s="T15">min</ta>
            <ta e="T17" id="Seg_1809" s="T16">eji͡e-ke</ta>
            <ta e="T18" id="Seg_1810" s="T17">er-ge</ta>
            <ta e="T19" id="Seg_1811" s="T18">bar-ɨ͡a-m</ta>
            <ta e="T20" id="Seg_1812" s="T19">diː-r</ta>
            <ta e="T21" id="Seg_1813" s="T20">künüs</ta>
            <ta e="T22" id="Seg_1814" s="T21">kɨːs-tarɨ-n</ta>
            <ta e="T23" id="Seg_1815" s="T22">ɨjɨt-a</ta>
            <ta e="T24" id="Seg_1816" s="T23">hatɨː-l-lar</ta>
            <ta e="T25" id="Seg_1817" s="T24">tu͡ok</ta>
            <ta e="T26" id="Seg_1818" s="T25">da</ta>
            <ta e="T27" id="Seg_1819" s="T26">tɨl-ɨ</ta>
            <ta e="T28" id="Seg_1820" s="T27">kini-tten</ta>
            <ta e="T29" id="Seg_1821" s="T28">kɨ͡aj-an</ta>
            <ta e="T30" id="Seg_1822" s="T29">ɨl-bat-tar</ta>
            <ta e="T31" id="Seg_1823" s="T30">biːrde</ta>
            <ta e="T32" id="Seg_1824" s="T31">ki͡ehe</ta>
            <ta e="T33" id="Seg_1825" s="T32">hɨt-an</ta>
            <ta e="T34" id="Seg_1826" s="T33">bar-an-nar</ta>
            <ta e="T35" id="Seg_1827" s="T34">hunuːr-darɨ-n</ta>
            <ta e="T36" id="Seg_1828" s="T35">utup-put</ta>
            <ta e="T37" id="Seg_1829" s="T36">kurduk</ta>
            <ta e="T38" id="Seg_1830" s="T37">hab-an</ta>
            <ta e="T39" id="Seg_1831" s="T38">keːh-el-ler</ta>
            <ta e="T40" id="Seg_1832" s="T39">kɨːs-tara</ta>
            <ta e="T41" id="Seg_1833" s="T40">emi͡e</ta>
            <ta e="T42" id="Seg_1834" s="T41">haŋar-an</ta>
            <ta e="T43" id="Seg_1835" s="T42">bar-ar</ta>
            <ta e="T44" id="Seg_1836" s="T43">manɨ͡a-ka</ta>
            <ta e="T45" id="Seg_1837" s="T44">hunuːr-darɨ-n</ta>
            <ta e="T46" id="Seg_1838" s="T45">habɨː-tɨ-n</ta>
            <ta e="T47" id="Seg_1839" s="T46">ɨl-an</ta>
            <ta e="T48" id="Seg_1840" s="T47">keːh-el-ler</ta>
            <ta e="T49" id="Seg_1841" s="T48">kör-büt-tere</ta>
            <ta e="T50" id="Seg_1842" s="T49">ogo-loru-n</ta>
            <ta e="T51" id="Seg_1843" s="T50">kɨtta</ta>
            <ta e="T52" id="Seg_1844" s="T51">biːr</ta>
            <ta e="T53" id="Seg_1845" s="T52">kihi</ta>
            <ta e="T54" id="Seg_1846" s="T53">hɨt-ar</ta>
            <ta e="T55" id="Seg_1847" s="T54">eː</ta>
            <ta e="T56" id="Seg_1848" s="T55">dʼe</ta>
            <ta e="T57" id="Seg_1849" s="T56">kütü͡öp-püt</ta>
            <ta e="T58" id="Seg_1850" s="T57">bu͡ol-lag-ɨ-ŋ</ta>
            <ta e="T59" id="Seg_1851" s="T58">di͡e-n</ta>
            <ta e="T60" id="Seg_1852" s="T59">ogonnʼor-doːk</ta>
            <ta e="T61" id="Seg_1853" s="T60">emeːksin</ta>
            <ta e="T62" id="Seg_1854" s="T61">et-el-ler</ta>
            <ta e="T63" id="Seg_1855" s="T62">manɨ-ga</ta>
            <ta e="T64" id="Seg_1856" s="T63">kihi-lere</ta>
            <ta e="T65" id="Seg_1857" s="T64">et-er</ta>
            <ta e="T66" id="Seg_1858" s="T65">dʼol-gut</ta>
            <ta e="T67" id="Seg_1859" s="T66">bu͡ol-lag-a</ta>
            <ta e="T68" id="Seg_1860" s="T67">ehi͡ene</ta>
            <ta e="T69" id="Seg_1861" s="T68">iti</ta>
            <ta e="T70" id="Seg_1862" s="T69">miːgi-n</ta>
            <ta e="T71" id="Seg_1863" s="T70">kör-bük-küt</ta>
            <ta e="T72" id="Seg_1864" s="T71">kanna</ta>
            <ta e="T73" id="Seg_1865" s="T72">kɨːs-kɨt</ta>
            <ta e="T74" id="Seg_1866" s="T73">bar-bɨt-ɨ-n</ta>
            <ta e="T75" id="Seg_1867" s="T74">bil-i-mne</ta>
            <ta e="T76" id="Seg_1868" s="T75">kaːl-ɨ͡ak</ta>
            <ta e="T77" id="Seg_1869" s="T76">e-ti-git</ta>
            <ta e="T78" id="Seg_1870" s="T77">diː-r</ta>
            <ta e="T79" id="Seg_1871" s="T78">hol</ta>
            <ta e="T80" id="Seg_1872" s="T79">da</ta>
            <ta e="T81" id="Seg_1873" s="T80">bu͡ol-lar</ta>
            <ta e="T82" id="Seg_1874" s="T81">kɨːs-kɨtɨ-n</ta>
            <ta e="T83" id="Seg_1875" s="T82">ildʼ-i͡e-m</ta>
            <ta e="T84" id="Seg_1876" s="T83">min</ta>
            <ta e="T85" id="Seg_1877" s="T84">bar-ɨ͡a-m</ta>
            <ta e="T86" id="Seg_1878" s="T85">harsɨn</ta>
            <ta e="T87" id="Seg_1879" s="T86">ogonnʼor</ta>
            <ta e="T88" id="Seg_1880" s="T87">erde</ta>
            <ta e="T89" id="Seg_1881" s="T88">kün</ta>
            <ta e="T90" id="Seg_1882" s="T89">haːrɨː-r-a</ta>
            <ta e="T91" id="Seg_1883" s="T90">tagɨs-tɨn</ta>
            <ta e="T92" id="Seg_1884" s="T91">hette</ta>
            <ta e="T93" id="Seg_1885" s="T92">kɨːl</ta>
            <ta e="T94" id="Seg_1886" s="T93">ebe-ni</ta>
            <ta e="T95" id="Seg_1887" s="T94">karbaː-r-ar</ta>
            <ta e="T96" id="Seg_1888" s="T95">bu͡ol-u͡og-a</ta>
            <ta e="T97" id="Seg_1889" s="T96">o-non</ta>
            <ta e="T98" id="Seg_1890" s="T97">bil-i͡ek-kit</ta>
            <ta e="T99" id="Seg_1891" s="T98">min</ta>
            <ta e="T100" id="Seg_1892" s="T99">dʼol-u</ta>
            <ta e="T101" id="Seg_1893" s="T100">agal-bɨp-pɨ-n</ta>
            <ta e="T102" id="Seg_1894" s="T101">kɨtaːt-an</ta>
            <ta e="T103" id="Seg_1895" s="T102">ol</ta>
            <ta e="T104" id="Seg_1896" s="T103">kɨːl-lar-ɨ</ta>
            <ta e="T105" id="Seg_1897" s="T104">ogonnʼor</ta>
            <ta e="T106" id="Seg_1898" s="T105">ölör-dün</ta>
            <ta e="T107" id="Seg_1899" s="T106">o-nu</ta>
            <ta e="T108" id="Seg_1900" s="T107">hül-en</ta>
            <ta e="T109" id="Seg_1901" s="T108">büt-e-r-deg-ine</ta>
            <ta e="T110" id="Seg_1902" s="T109">timir</ta>
            <ta e="T111" id="Seg_1903" s="T110">čib-ɨ-nan</ta>
            <ta e="T112" id="Seg_1904" s="T111">kallaːn-tan</ta>
            <ta e="T113" id="Seg_1905" s="T112">timir</ta>
            <ta e="T114" id="Seg_1906" s="T113">bihik-ke</ta>
            <ta e="T115" id="Seg_1907" s="T114">ogo</ta>
            <ta e="T116" id="Seg_1908" s="T115">tüh-ü͡ög-e</ta>
            <ta e="T117" id="Seg_1909" s="T116">ol</ta>
            <ta e="T118" id="Seg_1910" s="T117">habɨː-laːk</ta>
            <ta e="T119" id="Seg_1911" s="T118">bu͡ol-u͡og-a</ta>
            <ta e="T120" id="Seg_1912" s="T119">bukatɨn</ta>
            <ta e="T121" id="Seg_1913" s="T120">üs</ta>
            <ta e="T122" id="Seg_1914" s="T121">konuk-ka</ta>
            <ta e="T123" id="Seg_1915" s="T122">di͡eri</ta>
            <ta e="T124" id="Seg_1916" s="T123">habɨː-tɨ-n</ta>
            <ta e="T125" id="Seg_1917" s="T124">arɨj-an</ta>
            <ta e="T126" id="Seg_1918" s="T125">kör-ör</ta>
            <ta e="T127" id="Seg_1919" s="T126">bu͡ol-aːja-gɨt</ta>
            <ta e="T128" id="Seg_1920" s="T127">ma-nɨ</ta>
            <ta e="T129" id="Seg_1921" s="T128">et-en</ta>
            <ta e="T130" id="Seg_1922" s="T129">baran</ta>
            <ta e="T131" id="Seg_1923" s="T130">taks-ɨ-bɨt</ta>
            <ta e="T132" id="Seg_1924" s="T131">kihi-lere</ta>
            <ta e="T133" id="Seg_1925" s="T132">kɨːs-tara</ta>
            <ta e="T134" id="Seg_1926" s="T133">o-nu</ta>
            <ta e="T135" id="Seg_1927" s="T134">barɨs-pɨt</ta>
            <ta e="T136" id="Seg_1928" s="T135">ogonnʼor-doːk</ta>
            <ta e="T137" id="Seg_1929" s="T136">emeːksin</ta>
            <ta e="T138" id="Seg_1930" s="T137">o-nu</ta>
            <ta e="T139" id="Seg_1931" s="T138">ataːr-a</ta>
            <ta e="T140" id="Seg_1932" s="T139">tahaːra</ta>
            <ta e="T141" id="Seg_1933" s="T140">taks-ɨ-bɨt-tar</ta>
            <ta e="T142" id="Seg_1934" s="T141">kör-büt-tere</ta>
            <ta e="T143" id="Seg_1935" s="T142">er</ta>
            <ta e="T144" id="Seg_1936" s="T143">kihi-lere-kütü͡öt-tere</ta>
            <ta e="T145" id="Seg_1937" s="T144">ulakan</ta>
            <ta e="T146" id="Seg_1938" s="T145">muŋ</ta>
            <ta e="T147" id="Seg_1939" s="T146">kɨːl</ta>
            <ta e="T148" id="Seg_1940" s="T147">buːr-a</ta>
            <ta e="T149" id="Seg_1941" s="T148">bu͡ol-an</ta>
            <ta e="T150" id="Seg_1942" s="T149">kɨːs-tara</ta>
            <ta e="T151" id="Seg_1943" s="T150">kɨːl</ta>
            <ta e="T152" id="Seg_1944" s="T151">ikteːne-te</ta>
            <ta e="T153" id="Seg_1945" s="T152">bu͡ol-an</ta>
            <ta e="T154" id="Seg_1946" s="T153">ebe-ni</ta>
            <ta e="T155" id="Seg_1947" s="T154">karb-ɨː</ta>
            <ta e="T156" id="Seg_1948" s="T155">tur-but-tar</ta>
            <ta e="T157" id="Seg_1949" s="T156">harsɨ͡arda</ta>
            <ta e="T158" id="Seg_1950" s="T157">ogonnʼor</ta>
            <ta e="T159" id="Seg_1951" s="T158">iːkt-iː</ta>
            <ta e="T160" id="Seg_1952" s="T159">taks-ar</ta>
            <ta e="T161" id="Seg_1953" s="T160">kör-büt-e</ta>
            <ta e="T162" id="Seg_1954" s="T161">hette</ta>
            <ta e="T163" id="Seg_1955" s="T162">kɨːl</ta>
            <ta e="T164" id="Seg_1956" s="T163">ebe-ni</ta>
            <ta e="T165" id="Seg_1957" s="T164">karbaː-n</ta>
            <ta e="T166" id="Seg_1958" s="T165">ih-er</ta>
            <ta e="T167" id="Seg_1959" s="T166">ma-nɨ</ta>
            <ta e="T168" id="Seg_1960" s="T167">üŋüː-tü-n</ta>
            <ta e="T169" id="Seg_1961" s="T168">ɨl-an</ta>
            <ta e="T170" id="Seg_1962" s="T169">tɨː-nnan</ta>
            <ta e="T171" id="Seg_1963" s="T170">bar-an</ta>
            <ta e="T172" id="Seg_1964" s="T171">hette</ta>
            <ta e="T173" id="Seg_1965" s="T172">kɨːl-ɨ</ta>
            <ta e="T174" id="Seg_1966" s="T173">barɨ-tɨ-n</ta>
            <ta e="T175" id="Seg_1967" s="T174">bɨh-a</ta>
            <ta e="T176" id="Seg_1968" s="T175">kej-en</ta>
            <ta e="T177" id="Seg_1969" s="T176">ölör-töː-n</ta>
            <ta e="T178" id="Seg_1970" s="T177">keːh-er</ta>
            <ta e="T179" id="Seg_1971" s="T178">ma-nɨ</ta>
            <ta e="T180" id="Seg_1972" s="T179">hül-e</ta>
            <ta e="T181" id="Seg_1973" s="T180">kaːl-ar</ta>
            <ta e="T182" id="Seg_1974" s="T181">emeːksin</ta>
            <ta e="T183" id="Seg_1975" s="T182">utuj-an</ta>
            <ta e="T184" id="Seg_1976" s="T183">kaːl-bɨt</ta>
            <ta e="T185" id="Seg_1977" s="T184">tüːn</ta>
            <ta e="T186" id="Seg_1978" s="T185">kojut-aː-bɨt</ta>
            <ta e="T187" id="Seg_1979" s="T186">kihi</ta>
            <ta e="T188" id="Seg_1980" s="T187">uhukt-u-but-a</ta>
            <ta e="T189" id="Seg_1981" s="T188">timir</ta>
            <ta e="T190" id="Seg_1982" s="T189">čɨb-ɨ-nan</ta>
            <ta e="T191" id="Seg_1983" s="T190">kallaːn-tan</ta>
            <ta e="T192" id="Seg_1984" s="T191">tüh-en</ta>
            <ta e="T193" id="Seg_1985" s="T192">tur-ar</ta>
            <ta e="T194" id="Seg_1986" s="T193">e-bit</ta>
            <ta e="T195" id="Seg_1987" s="T194">timir</ta>
            <ta e="T196" id="Seg_1988" s="T195">bihik</ta>
            <ta e="T197" id="Seg_1989" s="T196">ih-i-ger</ta>
            <ta e="T198" id="Seg_1990" s="T197">ogo</ta>
            <ta e="T199" id="Seg_1991" s="T198">baːr</ta>
            <ta e="T200" id="Seg_1992" s="T199">bɨhɨːlaːk</ta>
            <ta e="T201" id="Seg_1993" s="T200">ɨtɨː-r</ta>
            <ta e="T202" id="Seg_1994" s="T201">emeːksin</ta>
            <ta e="T203" id="Seg_1995" s="T202">ahɨn-a</ta>
            <ta e="T204" id="Seg_1996" s="T203">hanɨː-r</ta>
            <ta e="T205" id="Seg_1997" s="T204">da</ta>
            <ta e="T206" id="Seg_1998" s="T205">heren-en</ta>
            <ta e="T207" id="Seg_1999" s="T206">ürd-ü-nen</ta>
            <ta e="T208" id="Seg_2000" s="T207">habɨː-tɨ-n</ta>
            <ta e="T209" id="Seg_2001" s="T208">arɨj-an</ta>
            <ta e="T210" id="Seg_2002" s="T209">kör-ör</ta>
            <ta e="T211" id="Seg_2003" s="T210">kör-büt-e</ta>
            <ta e="T212" id="Seg_2004" s="T211">kaːr</ta>
            <ta e="T213" id="Seg_2005" s="T212">kurduk</ta>
            <ta e="T214" id="Seg_2006" s="T213">čeːlkeː</ta>
            <ta e="T215" id="Seg_2007" s="T214">bagajɨ</ta>
            <ta e="T216" id="Seg_2008" s="T215">ogo</ta>
            <ta e="T217" id="Seg_2009" s="T216">hɨt-ar</ta>
            <ta e="T218" id="Seg_2010" s="T217">e-bit</ta>
            <ta e="T219" id="Seg_2011" s="T218">manɨ͡a-ka</ta>
            <ta e="T220" id="Seg_2012" s="T219">ogo-to</ta>
            <ta e="T221" id="Seg_2013" s="T220">öl-ör</ta>
            <ta e="T222" id="Seg_2014" s="T221">haŋa-ta</ta>
            <ta e="T223" id="Seg_2015" s="T222">haŋar-a</ta>
            <ta e="T224" id="Seg_2016" s="T223">tüs-püt</ta>
            <ta e="T225" id="Seg_2017" s="T224">karag-ɨ-m</ta>
            <ta e="T226" id="Seg_2018" s="T225">teh-e</ta>
            <ta e="T227" id="Seg_2019" s="T226">u͡ol-aːrɨ</ta>
            <ta e="T228" id="Seg_2020" s="T227">gɨn-n-a</ta>
            <ta e="T229" id="Seg_2021" s="T228">d-iː-d-iː</ta>
            <ta e="T230" id="Seg_2022" s="T229">čɨb-a</ta>
            <ta e="T231" id="Seg_2023" s="T230">ü͡öhe</ta>
            <ta e="T232" id="Seg_2024" s="T231">kötög-ö</ta>
            <ta e="T233" id="Seg_2025" s="T232">tur-but</ta>
            <ta e="T234" id="Seg_2026" s="T233">bihig-i-n</ta>
            <ta e="T235" id="Seg_2027" s="T234">ogo</ta>
            <ta e="T236" id="Seg_2028" s="T235">haŋa-ta</ta>
            <ta e="T237" id="Seg_2029" s="T236">ihill-i-bit</ta>
            <ta e="T238" id="Seg_2030" s="T237">bu</ta>
            <ta e="T239" id="Seg_2031" s="T238">min</ta>
            <ta e="T240" id="Seg_2032" s="T239">ubaj-ɨ-m</ta>
            <ta e="T241" id="Seg_2033" s="T240">Ürüŋ Ajɨː</ta>
            <ta e="T242" id="Seg_2034" s="T241">u͡ol-a</ta>
            <ta e="T243" id="Seg_2035" s="T242">e-t-e</ta>
            <ta e="T244" id="Seg_2036" s="T243">miːgi-n</ta>
            <ta e="T245" id="Seg_2037" s="T244">ubaj-ɨ-m</ta>
            <ta e="T246" id="Seg_2038" s="T245">ɨːp-pɨt-a</ta>
            <ta e="T247" id="Seg_2039" s="T246">üs</ta>
            <ta e="T248" id="Seg_2040" s="T247">konuk-ka</ta>
            <ta e="T249" id="Seg_2041" s="T248">di͡eri</ta>
            <ta e="T250" id="Seg_2042" s="T249">miːgi-n</ta>
            <ta e="T251" id="Seg_2043" s="T250">bɨrtak</ta>
            <ta e="T252" id="Seg_2044" s="T251">karak-tar-ɨ-nan</ta>
            <ta e="T253" id="Seg_2045" s="T252">kör-ön</ta>
            <ta e="T254" id="Seg_2046" s="T253">hokkor</ta>
            <ta e="T255" id="Seg_2047" s="T254">oŋor-u͡ok-tara</ta>
            <ta e="T256" id="Seg_2048" s="T255">hu͡og-a</ta>
            <ta e="T257" id="Seg_2049" s="T256">di͡e-n</ta>
            <ta e="T258" id="Seg_2050" s="T257">biligin</ta>
            <ta e="T259" id="Seg_2051" s="T258">tönn-ü͡ök-pün</ta>
            <ta e="T260" id="Seg_2052" s="T259">tönn-ü͡ö</ta>
            <ta e="T261" id="Seg_2053" s="T260">hu͡ok-pun</ta>
            <ta e="T262" id="Seg_2054" s="T261">ol</ta>
            <ta e="T263" id="Seg_2055" s="T262">ubaj-ɨ-m</ta>
            <ta e="T264" id="Seg_2056" s="T263">bil-i͡e</ta>
            <ta e="T265" id="Seg_2057" s="T264">di͡e-bit</ta>
            <ta e="T266" id="Seg_2058" s="T265">ol</ta>
            <ta e="T267" id="Seg_2059" s="T266">biri͡eme-ge</ta>
            <ta e="T268" id="Seg_2060" s="T267">ogonnʼor</ta>
            <ta e="T269" id="Seg_2061" s="T268">kɨːl-lar-ɨ-n</ta>
            <ta e="T270" id="Seg_2062" s="T269">hül-en</ta>
            <ta e="T271" id="Seg_2063" s="T270">ikki</ta>
            <ta e="T272" id="Seg_2064" s="T271">buːt-u-n</ta>
            <ta e="T273" id="Seg_2065" s="T272">hannɨ-gar</ta>
            <ta e="T274" id="Seg_2066" s="T273">uːr-an</ta>
            <ta e="T275" id="Seg_2067" s="T274">kel-en</ta>
            <ta e="T276" id="Seg_2068" s="T275">is-pit</ta>
            <ta e="T277" id="Seg_2069" s="T276">ma-nɨ</ta>
            <ta e="T278" id="Seg_2070" s="T277">emeːksin-e</ta>
            <ta e="T279" id="Seg_2071" s="T278">körs-ön</ta>
            <ta e="T280" id="Seg_2072" s="T279">kepsiː-r</ta>
            <ta e="T281" id="Seg_2073" s="T280">ogonnʼor</ta>
            <ta e="T282" id="Seg_2074" s="T281">itegej-i-mne</ta>
            <ta e="T283" id="Seg_2075" s="T282">hüːr-en</ta>
            <ta e="T284" id="Seg_2076" s="T283">kel-er</ta>
            <ta e="T285" id="Seg_2077" s="T284">hu͡og-u-n</ta>
            <ta e="T286" id="Seg_2078" s="T285">kör-ön</ta>
            <ta e="T287" id="Seg_2079" s="T286">baran</ta>
            <ta e="T288" id="Seg_2080" s="T287">emeːksin-i-n</ta>
            <ta e="T289" id="Seg_2081" s="T288">ettiː-r</ta>
            <ta e="T290" id="Seg_2082" s="T289">kol-u-nan</ta>
            <ta e="T291" id="Seg_2083" s="T290">emeːksin-e</ta>
            <ta e="T292" id="Seg_2084" s="T291">aŋar</ta>
            <ta e="T293" id="Seg_2085" s="T292">kol-u-n</ta>
            <ta e="T294" id="Seg_2086" s="T293">ɨl-ar</ta>
            <ta e="T295" id="Seg_2087" s="T294">da</ta>
            <ta e="T296" id="Seg_2088" s="T295">ett-e-h-en</ta>
            <ta e="T297" id="Seg_2089" s="T296">beje</ta>
            <ta e="T298" id="Seg_2090" s="T297">beje-leri-n</ta>
            <ta e="T299" id="Seg_2091" s="T298">ölör-ön</ta>
            <ta e="T300" id="Seg_2092" s="T299">keːh-el-ler</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_2093" s="T0">bɨlɨr</ta>
            <ta e="T2" id="Seg_2094" s="T1">ogonnʼor-LAːK</ta>
            <ta e="T3" id="Seg_2095" s="T2">emeːksin</ta>
            <ta e="T4" id="Seg_2096" s="T3">olor-BIT-LAr</ta>
            <ta e="T5" id="Seg_2097" s="T4">giniler</ta>
            <ta e="T6" id="Seg_2098" s="T5">biːr</ta>
            <ta e="T7" id="Seg_2099" s="T6">kɨːs-LAːK-LAr</ta>
            <ta e="T8" id="Seg_2100" s="T7">bu</ta>
            <ta e="T9" id="Seg_2101" s="T8">kɨːs-LArA</ta>
            <ta e="T10" id="Seg_2102" s="T9">ki͡ehe</ta>
            <ta e="T11" id="Seg_2103" s="T10">hunuːr-LArI-n</ta>
            <ta e="T12" id="Seg_2104" s="T11">utut-TI-LAr</ta>
            <ta e="T13" id="Seg_2105" s="T12">da</ta>
            <ta e="T14" id="Seg_2106" s="T13">kepset-An</ta>
            <ta e="T15" id="Seg_2107" s="T14">bar-Ar</ta>
            <ta e="T16" id="Seg_2108" s="T15">min</ta>
            <ta e="T17" id="Seg_2109" s="T16">en-GA</ta>
            <ta e="T18" id="Seg_2110" s="T17">er-GA</ta>
            <ta e="T19" id="Seg_2111" s="T18">bar-IAK-m</ta>
            <ta e="T20" id="Seg_2112" s="T19">di͡e-Ar</ta>
            <ta e="T21" id="Seg_2113" s="T20">künüs</ta>
            <ta e="T22" id="Seg_2114" s="T21">kɨːs-LArI-n</ta>
            <ta e="T23" id="Seg_2115" s="T22">ɨjɨt-A</ta>
            <ta e="T24" id="Seg_2116" s="T23">hataː-Ar-LAr</ta>
            <ta e="T25" id="Seg_2117" s="T24">tu͡ok</ta>
            <ta e="T26" id="Seg_2118" s="T25">da</ta>
            <ta e="T27" id="Seg_2119" s="T26">tɨl-nI</ta>
            <ta e="T28" id="Seg_2120" s="T27">gini-ttAn</ta>
            <ta e="T29" id="Seg_2121" s="T28">kɨ͡aj-An</ta>
            <ta e="T30" id="Seg_2122" s="T29">ɨl-BAT-LAr</ta>
            <ta e="T31" id="Seg_2123" s="T30">biːrde</ta>
            <ta e="T32" id="Seg_2124" s="T31">ki͡ehe</ta>
            <ta e="T33" id="Seg_2125" s="T32">hɨt-An</ta>
            <ta e="T34" id="Seg_2126" s="T33">bar-An-LAr</ta>
            <ta e="T35" id="Seg_2127" s="T34">hunuːr-LArI-n</ta>
            <ta e="T36" id="Seg_2128" s="T35">utut-BIT</ta>
            <ta e="T37" id="Seg_2129" s="T36">kurduk</ta>
            <ta e="T38" id="Seg_2130" s="T37">hap-An</ta>
            <ta e="T39" id="Seg_2131" s="T38">keːs-Ar-LAr</ta>
            <ta e="T40" id="Seg_2132" s="T39">kɨːs-LArA</ta>
            <ta e="T41" id="Seg_2133" s="T40">emi͡e</ta>
            <ta e="T42" id="Seg_2134" s="T41">haŋar-An</ta>
            <ta e="T43" id="Seg_2135" s="T42">bar-Ar</ta>
            <ta e="T44" id="Seg_2136" s="T43">bu-GA</ta>
            <ta e="T45" id="Seg_2137" s="T44">hunuːr-LArI-n</ta>
            <ta e="T46" id="Seg_2138" s="T45">habɨː-tI-n</ta>
            <ta e="T47" id="Seg_2139" s="T46">ɨl-An</ta>
            <ta e="T48" id="Seg_2140" s="T47">keːs-Ar-LAr</ta>
            <ta e="T49" id="Seg_2141" s="T48">kör-BIT-LArA</ta>
            <ta e="T50" id="Seg_2142" s="T49">ogo-LArI-n</ta>
            <ta e="T51" id="Seg_2143" s="T50">kɨtta</ta>
            <ta e="T52" id="Seg_2144" s="T51">biːr</ta>
            <ta e="T53" id="Seg_2145" s="T52">kihi</ta>
            <ta e="T54" id="Seg_2146" s="T53">hɨt-Ar</ta>
            <ta e="T55" id="Seg_2147" s="T54">eː</ta>
            <ta e="T56" id="Seg_2148" s="T55">dʼe</ta>
            <ta e="T57" id="Seg_2149" s="T56">kütü͡öt-BIt</ta>
            <ta e="T58" id="Seg_2150" s="T57">bu͡ol-TAK-I-ŋ</ta>
            <ta e="T59" id="Seg_2151" s="T58">di͡e-An</ta>
            <ta e="T60" id="Seg_2152" s="T59">ogonnʼor-LAːK</ta>
            <ta e="T61" id="Seg_2153" s="T60">emeːksin</ta>
            <ta e="T62" id="Seg_2154" s="T61">et-Ar-LAr</ta>
            <ta e="T63" id="Seg_2155" s="T62">bu-GA</ta>
            <ta e="T64" id="Seg_2156" s="T63">kihi-LArA</ta>
            <ta e="T65" id="Seg_2157" s="T64">et-Ar</ta>
            <ta e="T66" id="Seg_2158" s="T65">dʼol-GIt</ta>
            <ta e="T67" id="Seg_2159" s="T66">bu͡ol-TAK-tA</ta>
            <ta e="T68" id="Seg_2160" s="T67">ehi͡ene</ta>
            <ta e="T69" id="Seg_2161" s="T68">iti</ta>
            <ta e="T70" id="Seg_2162" s="T69">min-n</ta>
            <ta e="T71" id="Seg_2163" s="T70">kör-BIT-GIt</ta>
            <ta e="T72" id="Seg_2164" s="T71">kanna</ta>
            <ta e="T73" id="Seg_2165" s="T72">kɨːs-GIt</ta>
            <ta e="T74" id="Seg_2166" s="T73">bar-BIT-tI-n</ta>
            <ta e="T75" id="Seg_2167" s="T74">bil-I-mInA</ta>
            <ta e="T76" id="Seg_2168" s="T75">kaːl-IAK</ta>
            <ta e="T77" id="Seg_2169" s="T76">e-TI-GIt</ta>
            <ta e="T78" id="Seg_2170" s="T77">di͡e-Ar</ta>
            <ta e="T79" id="Seg_2171" s="T78">hol</ta>
            <ta e="T80" id="Seg_2172" s="T79">da</ta>
            <ta e="T81" id="Seg_2173" s="T80">bu͡ol-TAR</ta>
            <ta e="T82" id="Seg_2174" s="T81">kɨːs-GItI-n</ta>
            <ta e="T83" id="Seg_2175" s="T82">ilt-IAK-m</ta>
            <ta e="T84" id="Seg_2176" s="T83">min</ta>
            <ta e="T85" id="Seg_2177" s="T84">bar-IAK-m</ta>
            <ta e="T86" id="Seg_2178" s="T85">harsɨn</ta>
            <ta e="T87" id="Seg_2179" s="T86">ogonnʼor</ta>
            <ta e="T88" id="Seg_2180" s="T87">erde</ta>
            <ta e="T89" id="Seg_2181" s="T88">kün</ta>
            <ta e="T90" id="Seg_2182" s="T89">haraː-Ar-tA</ta>
            <ta e="T91" id="Seg_2183" s="T90">tagɨs-TIn</ta>
            <ta e="T92" id="Seg_2184" s="T91">hette</ta>
            <ta e="T93" id="Seg_2185" s="T92">kɨːl</ta>
            <ta e="T94" id="Seg_2186" s="T93">ebe-nI</ta>
            <ta e="T95" id="Seg_2187" s="T94">karbaː-r-Ar</ta>
            <ta e="T96" id="Seg_2188" s="T95">bu͡ol-IAK-tA</ta>
            <ta e="T97" id="Seg_2189" s="T96">ol-nAn</ta>
            <ta e="T98" id="Seg_2190" s="T97">bil-IAK-GIt</ta>
            <ta e="T99" id="Seg_2191" s="T98">min</ta>
            <ta e="T100" id="Seg_2192" s="T99">dʼol-nI</ta>
            <ta e="T101" id="Seg_2193" s="T100">egel-BIT-BI-n</ta>
            <ta e="T102" id="Seg_2194" s="T101">kɨtaːt-An</ta>
            <ta e="T103" id="Seg_2195" s="T102">ol</ta>
            <ta e="T104" id="Seg_2196" s="T103">kɨːl-LAr-nI</ta>
            <ta e="T105" id="Seg_2197" s="T104">ogonnʼor</ta>
            <ta e="T106" id="Seg_2198" s="T105">ölör-TIn</ta>
            <ta e="T107" id="Seg_2199" s="T106">ol-nI</ta>
            <ta e="T108" id="Seg_2200" s="T107">hül-An</ta>
            <ta e="T109" id="Seg_2201" s="T108">büt-A-r-TAK-InA</ta>
            <ta e="T110" id="Seg_2202" s="T109">timir</ta>
            <ta e="T111" id="Seg_2203" s="T110">čip-I-nAn</ta>
            <ta e="T112" id="Seg_2204" s="T111">kallaːn-ttAn</ta>
            <ta e="T113" id="Seg_2205" s="T112">timir</ta>
            <ta e="T114" id="Seg_2206" s="T113">bihik-GA</ta>
            <ta e="T115" id="Seg_2207" s="T114">ogo</ta>
            <ta e="T116" id="Seg_2208" s="T115">tüs-IAK-tA</ta>
            <ta e="T117" id="Seg_2209" s="T116">ol</ta>
            <ta e="T118" id="Seg_2210" s="T117">habɨː-LAːK</ta>
            <ta e="T119" id="Seg_2211" s="T118">bu͡ol-IAK-tA</ta>
            <ta e="T120" id="Seg_2212" s="T119">bukatɨn</ta>
            <ta e="T121" id="Seg_2213" s="T120">üs</ta>
            <ta e="T122" id="Seg_2214" s="T121">konuk-GA</ta>
            <ta e="T123" id="Seg_2215" s="T122">di͡eri</ta>
            <ta e="T124" id="Seg_2216" s="T123">habɨː-tI-n</ta>
            <ta e="T125" id="Seg_2217" s="T124">arɨj-An</ta>
            <ta e="T126" id="Seg_2218" s="T125">kör-Ar</ta>
            <ta e="T127" id="Seg_2219" s="T126">bu͡ol-AːjA-GIt</ta>
            <ta e="T128" id="Seg_2220" s="T127">bu-nI</ta>
            <ta e="T129" id="Seg_2221" s="T128">et-An</ta>
            <ta e="T130" id="Seg_2222" s="T129">baran</ta>
            <ta e="T131" id="Seg_2223" s="T130">tagɨs-I-BIT</ta>
            <ta e="T132" id="Seg_2224" s="T131">kihi-LArA</ta>
            <ta e="T133" id="Seg_2225" s="T132">kɨːs-LArA</ta>
            <ta e="T134" id="Seg_2226" s="T133">ol-nI</ta>
            <ta e="T135" id="Seg_2227" s="T134">barɨs-BIT</ta>
            <ta e="T136" id="Seg_2228" s="T135">ogonnʼor-LAːK</ta>
            <ta e="T137" id="Seg_2229" s="T136">emeːksin</ta>
            <ta e="T138" id="Seg_2230" s="T137">ol-nI</ta>
            <ta e="T139" id="Seg_2231" s="T138">ataːr-A</ta>
            <ta e="T140" id="Seg_2232" s="T139">tahaːra</ta>
            <ta e="T141" id="Seg_2233" s="T140">tagɨs-I-BIT-LAr</ta>
            <ta e="T142" id="Seg_2234" s="T141">kör-BIT-LArA</ta>
            <ta e="T143" id="Seg_2235" s="T142">er</ta>
            <ta e="T144" id="Seg_2236" s="T143">kihi-LArA-kütü͡öt-LArA</ta>
            <ta e="T145" id="Seg_2237" s="T144">ulakan</ta>
            <ta e="T146" id="Seg_2238" s="T145">muŋ</ta>
            <ta e="T147" id="Seg_2239" s="T146">kɨːl</ta>
            <ta e="T148" id="Seg_2240" s="T147">buːr-tA</ta>
            <ta e="T149" id="Seg_2241" s="T148">bu͡ol-An</ta>
            <ta e="T150" id="Seg_2242" s="T149">kɨːs-LArA</ta>
            <ta e="T151" id="Seg_2243" s="T150">kɨːl</ta>
            <ta e="T152" id="Seg_2244" s="T151">ikteːne-tA</ta>
            <ta e="T153" id="Seg_2245" s="T152">bu͡ol-An</ta>
            <ta e="T154" id="Seg_2246" s="T153">ebe-nI</ta>
            <ta e="T155" id="Seg_2247" s="T154">karbaː-A</ta>
            <ta e="T156" id="Seg_2248" s="T155">tur-BIT-LAr</ta>
            <ta e="T157" id="Seg_2249" s="T156">harsi͡erda</ta>
            <ta e="T158" id="Seg_2250" s="T157">ogonnʼor</ta>
            <ta e="T159" id="Seg_2251" s="T158">iːkteː-A</ta>
            <ta e="T160" id="Seg_2252" s="T159">tagɨs-Ar</ta>
            <ta e="T161" id="Seg_2253" s="T160">kör-BIT-tA</ta>
            <ta e="T162" id="Seg_2254" s="T161">hette</ta>
            <ta e="T163" id="Seg_2255" s="T162">kɨːl</ta>
            <ta e="T164" id="Seg_2256" s="T163">ebe-nI</ta>
            <ta e="T165" id="Seg_2257" s="T164">karbaː-An</ta>
            <ta e="T166" id="Seg_2258" s="T165">is-Ar</ta>
            <ta e="T167" id="Seg_2259" s="T166">bu-nI</ta>
            <ta e="T168" id="Seg_2260" s="T167">üŋüː-tI-n</ta>
            <ta e="T169" id="Seg_2261" s="T168">ɨl-An</ta>
            <ta e="T170" id="Seg_2262" s="T169">tɨː-nAn</ta>
            <ta e="T171" id="Seg_2263" s="T170">bar-An</ta>
            <ta e="T172" id="Seg_2264" s="T171">hette</ta>
            <ta e="T173" id="Seg_2265" s="T172">kɨːl-nI</ta>
            <ta e="T174" id="Seg_2266" s="T173">barɨ-tI-n</ta>
            <ta e="T175" id="Seg_2267" s="T174">bɨs-A</ta>
            <ta e="T176" id="Seg_2268" s="T175">kej-An</ta>
            <ta e="T177" id="Seg_2269" s="T176">ölör-TAː-An</ta>
            <ta e="T178" id="Seg_2270" s="T177">keːs-Ar</ta>
            <ta e="T179" id="Seg_2271" s="T178">bu-nI</ta>
            <ta e="T180" id="Seg_2272" s="T179">hül-A</ta>
            <ta e="T181" id="Seg_2273" s="T180">kaːl-Ar</ta>
            <ta e="T182" id="Seg_2274" s="T181">emeːksin</ta>
            <ta e="T183" id="Seg_2275" s="T182">utuj-An</ta>
            <ta e="T184" id="Seg_2276" s="T183">kaːl-BIT</ta>
            <ta e="T185" id="Seg_2277" s="T184">tüːn</ta>
            <ta e="T186" id="Seg_2278" s="T185">kojut-Aː-BIT</ta>
            <ta e="T187" id="Seg_2279" s="T186">kihi</ta>
            <ta e="T188" id="Seg_2280" s="T187">uhugun-I-BIT-tA</ta>
            <ta e="T189" id="Seg_2281" s="T188">timir</ta>
            <ta e="T190" id="Seg_2282" s="T189">čip-I-nAn</ta>
            <ta e="T191" id="Seg_2283" s="T190">kallaːn-ttAn</ta>
            <ta e="T192" id="Seg_2284" s="T191">tüs-An</ta>
            <ta e="T193" id="Seg_2285" s="T192">tur-Ar</ta>
            <ta e="T194" id="Seg_2286" s="T193">e-BIT</ta>
            <ta e="T195" id="Seg_2287" s="T194">timir</ta>
            <ta e="T196" id="Seg_2288" s="T195">bihik</ta>
            <ta e="T197" id="Seg_2289" s="T196">is-tI-GAr</ta>
            <ta e="T198" id="Seg_2290" s="T197">ogo</ta>
            <ta e="T199" id="Seg_2291" s="T198">baːr</ta>
            <ta e="T200" id="Seg_2292" s="T199">bɨhɨːlaːk</ta>
            <ta e="T201" id="Seg_2293" s="T200">ɨtaː-Ar</ta>
            <ta e="T202" id="Seg_2294" s="T201">emeːksin</ta>
            <ta e="T203" id="Seg_2295" s="T202">ahɨn-A</ta>
            <ta e="T204" id="Seg_2296" s="T203">hanaː-Ar</ta>
            <ta e="T205" id="Seg_2297" s="T204">da</ta>
            <ta e="T206" id="Seg_2298" s="T205">heren-An</ta>
            <ta e="T207" id="Seg_2299" s="T206">ürüt-tI-nAn</ta>
            <ta e="T208" id="Seg_2300" s="T207">habɨː-tI-n</ta>
            <ta e="T209" id="Seg_2301" s="T208">arɨj-An</ta>
            <ta e="T210" id="Seg_2302" s="T209">kör-Ar</ta>
            <ta e="T211" id="Seg_2303" s="T210">kör-BIT-tA</ta>
            <ta e="T212" id="Seg_2304" s="T211">kaːr</ta>
            <ta e="T213" id="Seg_2305" s="T212">kurduk</ta>
            <ta e="T214" id="Seg_2306" s="T213">čeːlkeː</ta>
            <ta e="T215" id="Seg_2307" s="T214">bagajɨ</ta>
            <ta e="T216" id="Seg_2308" s="T215">ogo</ta>
            <ta e="T217" id="Seg_2309" s="T216">hɨt-Ar</ta>
            <ta e="T218" id="Seg_2310" s="T217">e-BIT</ta>
            <ta e="T219" id="Seg_2311" s="T218">bu-GA</ta>
            <ta e="T220" id="Seg_2312" s="T219">ogo-tA</ta>
            <ta e="T221" id="Seg_2313" s="T220">öl-Ar</ta>
            <ta e="T222" id="Seg_2314" s="T221">haŋa-tA</ta>
            <ta e="T223" id="Seg_2315" s="T222">haŋar-A</ta>
            <ta e="T224" id="Seg_2316" s="T223">tüs-BIT</ta>
            <ta e="T225" id="Seg_2317" s="T224">karak-I-m</ta>
            <ta e="T226" id="Seg_2318" s="T225">tes-A</ta>
            <ta e="T227" id="Seg_2319" s="T226">u͡ol-AːrI</ta>
            <ta e="T228" id="Seg_2320" s="T227">gɨn-TI-tA</ta>
            <ta e="T229" id="Seg_2321" s="T228">di͡e-A-di͡e-A</ta>
            <ta e="T230" id="Seg_2322" s="T229">čip-tA</ta>
            <ta e="T231" id="Seg_2323" s="T230">ü͡öhe</ta>
            <ta e="T232" id="Seg_2324" s="T231">kötök-A</ta>
            <ta e="T233" id="Seg_2325" s="T232">tur-BIT</ta>
            <ta e="T234" id="Seg_2326" s="T233">bihik-tI-n</ta>
            <ta e="T235" id="Seg_2327" s="T234">ogo</ta>
            <ta e="T236" id="Seg_2328" s="T235">haŋa-tA</ta>
            <ta e="T237" id="Seg_2329" s="T236">ihilin-I-BIT</ta>
            <ta e="T238" id="Seg_2330" s="T237">bu</ta>
            <ta e="T239" id="Seg_2331" s="T238">min</ta>
            <ta e="T240" id="Seg_2332" s="T239">ubaj-I-m</ta>
            <ta e="T241" id="Seg_2333" s="T240">Ürüŋ Ajɨː</ta>
            <ta e="T242" id="Seg_2334" s="T241">u͡ol-tA</ta>
            <ta e="T243" id="Seg_2335" s="T242">e-TI-tA</ta>
            <ta e="T244" id="Seg_2336" s="T243">min-n</ta>
            <ta e="T245" id="Seg_2337" s="T244">ubaj-I-m</ta>
            <ta e="T246" id="Seg_2338" s="T245">ɨːt-BIT-tA</ta>
            <ta e="T247" id="Seg_2339" s="T246">üs</ta>
            <ta e="T248" id="Seg_2340" s="T247">konuk-GA</ta>
            <ta e="T249" id="Seg_2341" s="T248">di͡eri</ta>
            <ta e="T250" id="Seg_2342" s="T249">min-n</ta>
            <ta e="T251" id="Seg_2343" s="T250">bɨrtak</ta>
            <ta e="T252" id="Seg_2344" s="T251">karak-LAr-I-nAn</ta>
            <ta e="T253" id="Seg_2345" s="T252">kör-An</ta>
            <ta e="T254" id="Seg_2346" s="T253">hokkor</ta>
            <ta e="T255" id="Seg_2347" s="T254">oŋor-IAK-LArA</ta>
            <ta e="T256" id="Seg_2348" s="T255">hu͡ok-tA</ta>
            <ta e="T257" id="Seg_2349" s="T256">di͡e-An</ta>
            <ta e="T258" id="Seg_2350" s="T257">biligin</ta>
            <ta e="T259" id="Seg_2351" s="T258">tönün-IAK-BIn</ta>
            <ta e="T260" id="Seg_2352" s="T259">tönün-IAK.[tA]</ta>
            <ta e="T261" id="Seg_2353" s="T260">hu͡ok-BIn</ta>
            <ta e="T262" id="Seg_2354" s="T261">ol</ta>
            <ta e="T263" id="Seg_2355" s="T262">ubaj-I-m</ta>
            <ta e="T264" id="Seg_2356" s="T263">bil-IAK.[tA]</ta>
            <ta e="T265" id="Seg_2357" s="T264">di͡e-BIT</ta>
            <ta e="T266" id="Seg_2358" s="T265">ol</ta>
            <ta e="T267" id="Seg_2359" s="T266">biri͡eme-GA</ta>
            <ta e="T268" id="Seg_2360" s="T267">ogonnʼor</ta>
            <ta e="T269" id="Seg_2361" s="T268">kɨːl-LAr-tI-n</ta>
            <ta e="T270" id="Seg_2362" s="T269">hül-An</ta>
            <ta e="T271" id="Seg_2363" s="T270">ikki</ta>
            <ta e="T272" id="Seg_2364" s="T271">buːt-tI-n</ta>
            <ta e="T273" id="Seg_2365" s="T272">hannɨ-GAr</ta>
            <ta e="T274" id="Seg_2366" s="T273">uːr-An</ta>
            <ta e="T275" id="Seg_2367" s="T274">kel-An</ta>
            <ta e="T276" id="Seg_2368" s="T275">is-BIT</ta>
            <ta e="T277" id="Seg_2369" s="T276">bu-nI</ta>
            <ta e="T278" id="Seg_2370" s="T277">emeːksin-tA</ta>
            <ta e="T279" id="Seg_2371" s="T278">körüs-An</ta>
            <ta e="T280" id="Seg_2372" s="T279">kepseː-Ar</ta>
            <ta e="T281" id="Seg_2373" s="T280">ogonnʼor</ta>
            <ta e="T282" id="Seg_2374" s="T281">itegej-I-mInA</ta>
            <ta e="T283" id="Seg_2375" s="T282">hüːr-An</ta>
            <ta e="T284" id="Seg_2376" s="T283">kel-Ar</ta>
            <ta e="T285" id="Seg_2377" s="T284">hu͡ok-tI-n</ta>
            <ta e="T286" id="Seg_2378" s="T285">kör-An</ta>
            <ta e="T287" id="Seg_2379" s="T286">baran</ta>
            <ta e="T288" id="Seg_2380" s="T287">emeːksin-tI-n</ta>
            <ta e="T289" id="Seg_2381" s="T288">etteː-Ar</ta>
            <ta e="T290" id="Seg_2382" s="T289">kol-tI-nAn</ta>
            <ta e="T291" id="Seg_2383" s="T290">emeːksin-tA</ta>
            <ta e="T292" id="Seg_2384" s="T291">aŋar</ta>
            <ta e="T293" id="Seg_2385" s="T292">kol-tI-n</ta>
            <ta e="T294" id="Seg_2386" s="T293">ɨl-Ar</ta>
            <ta e="T295" id="Seg_2387" s="T294">da</ta>
            <ta e="T296" id="Seg_2388" s="T295">etteː-A-s-An</ta>
            <ta e="T297" id="Seg_2389" s="T296">beje</ta>
            <ta e="T298" id="Seg_2390" s="T297">beje-LArI-n</ta>
            <ta e="T299" id="Seg_2391" s="T298">ölör-An</ta>
            <ta e="T300" id="Seg_2392" s="T299">keːs-Ar-LAr</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_2393" s="T0">a.long.time</ta>
            <ta e="T2" id="Seg_2394" s="T1">old.man-PROPR</ta>
            <ta e="T3" id="Seg_2395" s="T2">old.woman.[NOM]</ta>
            <ta e="T4" id="Seg_2396" s="T3">live-PST2-3PL</ta>
            <ta e="T5" id="Seg_2397" s="T4">3PL.[NOM]</ta>
            <ta e="T6" id="Seg_2398" s="T5">one</ta>
            <ta e="T7" id="Seg_2399" s="T6">girl-PROPR-3PL</ta>
            <ta e="T8" id="Seg_2400" s="T7">this</ta>
            <ta e="T9" id="Seg_2401" s="T8">girl-3PL.[NOM]</ta>
            <ta e="T10" id="Seg_2402" s="T9">in.the.evening</ta>
            <ta e="T11" id="Seg_2403" s="T10">lamp-3PL-ACC</ta>
            <ta e="T12" id="Seg_2404" s="T11">put.out-PST1-3PL</ta>
            <ta e="T13" id="Seg_2405" s="T12">and</ta>
            <ta e="T14" id="Seg_2406" s="T13">chat-CVB.SEQ</ta>
            <ta e="T15" id="Seg_2407" s="T14">go-PRS.[3SG]</ta>
            <ta e="T16" id="Seg_2408" s="T15">1SG.[NOM]</ta>
            <ta e="T17" id="Seg_2409" s="T16">2SG-DAT/LOC</ta>
            <ta e="T18" id="Seg_2410" s="T17">husband-DAT/LOC</ta>
            <ta e="T19" id="Seg_2411" s="T18">go-FUT-1SG</ta>
            <ta e="T20" id="Seg_2412" s="T19">say-PRS.[3SG]</ta>
            <ta e="T21" id="Seg_2413" s="T20">by.day</ta>
            <ta e="T22" id="Seg_2414" s="T21">daughter-3PL-ACC</ta>
            <ta e="T23" id="Seg_2415" s="T22">ask-CVB.SIM</ta>
            <ta e="T24" id="Seg_2416" s="T23">try-PRS-3PL</ta>
            <ta e="T25" id="Seg_2417" s="T24">what.[NOM]</ta>
            <ta e="T26" id="Seg_2418" s="T25">NEG</ta>
            <ta e="T27" id="Seg_2419" s="T26">word-ACC</ta>
            <ta e="T28" id="Seg_2420" s="T27">3SG-ABL</ta>
            <ta e="T29" id="Seg_2421" s="T28">can-CVB.SEQ</ta>
            <ta e="T30" id="Seg_2422" s="T29">take-NEG-3PL</ta>
            <ta e="T31" id="Seg_2423" s="T30">once</ta>
            <ta e="T32" id="Seg_2424" s="T31">in.the.evening</ta>
            <ta e="T33" id="Seg_2425" s="T32">lie.down-CVB.SEQ</ta>
            <ta e="T34" id="Seg_2426" s="T33">go-CVB.SEQ-3PL</ta>
            <ta e="T35" id="Seg_2427" s="T34">lamp-3PL-ACC</ta>
            <ta e="T36" id="Seg_2428" s="T35">put.out-PTCP.PST.[NOM]</ta>
            <ta e="T37" id="Seg_2429" s="T36">like</ta>
            <ta e="T38" id="Seg_2430" s="T37">cover-CVB.SEQ</ta>
            <ta e="T39" id="Seg_2431" s="T38">throw-PRS-3PL</ta>
            <ta e="T40" id="Seg_2432" s="T39">daughter-3PL.[NOM]</ta>
            <ta e="T41" id="Seg_2433" s="T40">again</ta>
            <ta e="T42" id="Seg_2434" s="T41">speak-CVB.SEQ</ta>
            <ta e="T43" id="Seg_2435" s="T42">go-PRS.[3SG]</ta>
            <ta e="T44" id="Seg_2436" s="T43">this-DAT/LOC</ta>
            <ta e="T45" id="Seg_2437" s="T44">lamp-3PL-GEN</ta>
            <ta e="T46" id="Seg_2438" s="T45">lid-3SG-ACC</ta>
            <ta e="T47" id="Seg_2439" s="T46">take-CVB.SEQ</ta>
            <ta e="T48" id="Seg_2440" s="T47">throw-PRS-3PL</ta>
            <ta e="T49" id="Seg_2441" s="T48">see-PST2-3PL</ta>
            <ta e="T50" id="Seg_2442" s="T49">child-3PL-ACC</ta>
            <ta e="T51" id="Seg_2443" s="T50">with</ta>
            <ta e="T52" id="Seg_2444" s="T51">one</ta>
            <ta e="T53" id="Seg_2445" s="T52">human.being.[NOM]</ta>
            <ta e="T54" id="Seg_2446" s="T53">lie-PRS.[3SG]</ta>
            <ta e="T55" id="Seg_2447" s="T54">hey</ta>
            <ta e="T56" id="Seg_2448" s="T55">well</ta>
            <ta e="T57" id="Seg_2449" s="T56">son_in_law-1PL.[NOM]</ta>
            <ta e="T58" id="Seg_2450" s="T57">be-INFER-EP-2SG</ta>
            <ta e="T59" id="Seg_2451" s="T58">say-CVB.SEQ</ta>
            <ta e="T60" id="Seg_2452" s="T59">old.man-PROPR</ta>
            <ta e="T61" id="Seg_2453" s="T60">old.woman.[NOM]</ta>
            <ta e="T62" id="Seg_2454" s="T61">speak-PRS-3PL</ta>
            <ta e="T63" id="Seg_2455" s="T62">this-DAT/LOC</ta>
            <ta e="T64" id="Seg_2456" s="T63">human.being-3PL.[NOM]</ta>
            <ta e="T65" id="Seg_2457" s="T64">speak-PRS.[3SG]</ta>
            <ta e="T66" id="Seg_2458" s="T65">luck-2PL.[NOM]</ta>
            <ta e="T67" id="Seg_2459" s="T66">be-INFER-3SG</ta>
            <ta e="T68" id="Seg_2460" s="T67">your</ta>
            <ta e="T69" id="Seg_2461" s="T68">that.[NOM]</ta>
            <ta e="T70" id="Seg_2462" s="T69">1SG-ACC</ta>
            <ta e="T71" id="Seg_2463" s="T70">see-PST2-2PL</ta>
            <ta e="T72" id="Seg_2464" s="T71">whereto</ta>
            <ta e="T73" id="Seg_2465" s="T72">daughter-2PL.[NOM]</ta>
            <ta e="T74" id="Seg_2466" s="T73">go-PTCP.PST-3SG-ACC</ta>
            <ta e="T75" id="Seg_2467" s="T74">know-EP-NEG.CVB</ta>
            <ta e="T76" id="Seg_2468" s="T75">stay-PTCP.FUT</ta>
            <ta e="T77" id="Seg_2469" s="T76">be-PST1-2PL</ta>
            <ta e="T78" id="Seg_2470" s="T77">say-PRS.[3SG]</ta>
            <ta e="T79" id="Seg_2471" s="T78">that.EMPH.[NOM]</ta>
            <ta e="T80" id="Seg_2472" s="T79">and</ta>
            <ta e="T81" id="Seg_2473" s="T80">be-COND.[3SG]</ta>
            <ta e="T82" id="Seg_2474" s="T81">girl-2PL-ACC</ta>
            <ta e="T83" id="Seg_2475" s="T82">carry-FUT-1SG</ta>
            <ta e="T84" id="Seg_2476" s="T83">1SG.[NOM]</ta>
            <ta e="T85" id="Seg_2477" s="T84">go-FUT-1SG</ta>
            <ta e="T86" id="Seg_2478" s="T85">tomorrow</ta>
            <ta e="T87" id="Seg_2479" s="T86">old.man.[NOM]</ta>
            <ta e="T88" id="Seg_2480" s="T87">early</ta>
            <ta e="T89" id="Seg_2481" s="T88">day.[NOM]</ta>
            <ta e="T90" id="Seg_2482" s="T89">become.light-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T91" id="Seg_2483" s="T90">go.out-IMP.3SG</ta>
            <ta e="T92" id="Seg_2484" s="T91">seven</ta>
            <ta e="T93" id="Seg_2485" s="T92">wild.reindeer.[NOM]</ta>
            <ta e="T94" id="Seg_2486" s="T93">river-ACC</ta>
            <ta e="T95" id="Seg_2487" s="T94">swim-CAUS-PTCP.PRS</ta>
            <ta e="T96" id="Seg_2488" s="T95">be-FUT-3SG</ta>
            <ta e="T97" id="Seg_2489" s="T96">that-INSTR</ta>
            <ta e="T98" id="Seg_2490" s="T97">know-FUT-2PL</ta>
            <ta e="T99" id="Seg_2491" s="T98">1SG.[NOM]</ta>
            <ta e="T100" id="Seg_2492" s="T99">luck-ACC</ta>
            <ta e="T101" id="Seg_2493" s="T100">bring-PTCP.PST-1SG-ACC</ta>
            <ta e="T102" id="Seg_2494" s="T101">attempt-CVB.SEQ</ta>
            <ta e="T103" id="Seg_2495" s="T102">that</ta>
            <ta e="T104" id="Seg_2496" s="T103">wild.reindeer-PL-ACC</ta>
            <ta e="T105" id="Seg_2497" s="T104">old.man.[NOM]</ta>
            <ta e="T106" id="Seg_2498" s="T105">kill-IMP.3SG</ta>
            <ta e="T107" id="Seg_2499" s="T106">that-ACC</ta>
            <ta e="T108" id="Seg_2500" s="T107">skin-CVB.SEQ</ta>
            <ta e="T109" id="Seg_2501" s="T108">stop-EP-CAUS-TEMP-3SG</ta>
            <ta e="T110" id="Seg_2502" s="T109">iron</ta>
            <ta e="T111" id="Seg_2503" s="T110">chain-EP-INSTR</ta>
            <ta e="T112" id="Seg_2504" s="T111">sky-ABL</ta>
            <ta e="T113" id="Seg_2505" s="T112">iron</ta>
            <ta e="T114" id="Seg_2506" s="T113">cradle-DAT/LOC</ta>
            <ta e="T115" id="Seg_2507" s="T114">child.[NOM]</ta>
            <ta e="T116" id="Seg_2508" s="T115">go.down-FUT-3SG</ta>
            <ta e="T117" id="Seg_2509" s="T116">that.[NOM]</ta>
            <ta e="T118" id="Seg_2510" s="T117">lid-PROPR.[NOM]</ta>
            <ta e="T119" id="Seg_2511" s="T118">be-FUT-3SG</ta>
            <ta e="T120" id="Seg_2512" s="T119">completely</ta>
            <ta e="T121" id="Seg_2513" s="T120">three</ta>
            <ta e="T122" id="Seg_2514" s="T121">day.and.night-DAT/LOC</ta>
            <ta e="T123" id="Seg_2515" s="T122">until</ta>
            <ta e="T124" id="Seg_2516" s="T123">lid-3SG-ACC</ta>
            <ta e="T125" id="Seg_2517" s="T124">open-CVB.SEQ</ta>
            <ta e="T126" id="Seg_2518" s="T125">see-PTCP.PRS</ta>
            <ta e="T127" id="Seg_2519" s="T126">be-APPR-2PL</ta>
            <ta e="T128" id="Seg_2520" s="T127">this-ACC</ta>
            <ta e="T129" id="Seg_2521" s="T128">speak-CVB.SEQ</ta>
            <ta e="T130" id="Seg_2522" s="T129">after</ta>
            <ta e="T131" id="Seg_2523" s="T130">go.out-EP-PST2.[3SG]</ta>
            <ta e="T132" id="Seg_2524" s="T131">human.being-3PL.[NOM]</ta>
            <ta e="T133" id="Seg_2525" s="T132">daughter-3PL.[NOM]</ta>
            <ta e="T134" id="Seg_2526" s="T133">that-ACC</ta>
            <ta e="T135" id="Seg_2527" s="T134">come.along-PST2.[3SG]</ta>
            <ta e="T136" id="Seg_2528" s="T135">old.man-PROPR</ta>
            <ta e="T137" id="Seg_2529" s="T136">old.woman.[NOM]</ta>
            <ta e="T138" id="Seg_2530" s="T137">that-ACC</ta>
            <ta e="T139" id="Seg_2531" s="T138">accompany-CVB.SIM</ta>
            <ta e="T140" id="Seg_2532" s="T139">out</ta>
            <ta e="T141" id="Seg_2533" s="T140">go.out-EP-PST2-3PL</ta>
            <ta e="T142" id="Seg_2534" s="T141">see-PST2-3PL</ta>
            <ta e="T143" id="Seg_2535" s="T142">man.[NOM]</ta>
            <ta e="T144" id="Seg_2536" s="T143">human.being-3PL.[NOM]-son_in_law-3PL.[NOM]</ta>
            <ta e="T145" id="Seg_2537" s="T144">big</ta>
            <ta e="T146" id="Seg_2538" s="T145">most</ta>
            <ta e="T147" id="Seg_2539" s="T146">wild.reindeer.[NOM]</ta>
            <ta e="T148" id="Seg_2540" s="T147">male.reindeer-3SG.[NOM]</ta>
            <ta e="T149" id="Seg_2541" s="T148">be-CVB.SEQ</ta>
            <ta e="T150" id="Seg_2542" s="T149">daughter-3PL.[NOM]</ta>
            <ta e="T151" id="Seg_2543" s="T150">wild.reindeer.[NOM]</ta>
            <ta e="T152" id="Seg_2544" s="T151">two.year.old.reindeer-3SG.[NOM]</ta>
            <ta e="T153" id="Seg_2545" s="T152">become-CVB.SEQ</ta>
            <ta e="T154" id="Seg_2546" s="T153">river-ACC</ta>
            <ta e="T155" id="Seg_2547" s="T154">swim-CVB.SIM</ta>
            <ta e="T156" id="Seg_2548" s="T155">stand-PST2-3PL</ta>
            <ta e="T157" id="Seg_2549" s="T156">in.the.morning</ta>
            <ta e="T158" id="Seg_2550" s="T157">old.man.[NOM]</ta>
            <ta e="T159" id="Seg_2551" s="T158">pee-CVB.SIM</ta>
            <ta e="T160" id="Seg_2552" s="T159">go.out-PRS.[3SG]</ta>
            <ta e="T161" id="Seg_2553" s="T160">see-PST2-3SG</ta>
            <ta e="T162" id="Seg_2554" s="T161">seven</ta>
            <ta e="T163" id="Seg_2555" s="T162">wild.reindeer.[NOM]</ta>
            <ta e="T164" id="Seg_2556" s="T163">river-ACC</ta>
            <ta e="T165" id="Seg_2557" s="T164">swim-CVB.SEQ</ta>
            <ta e="T166" id="Seg_2558" s="T165">go-PRS.[3SG]</ta>
            <ta e="T167" id="Seg_2559" s="T166">this-ACC</ta>
            <ta e="T168" id="Seg_2560" s="T167">spear-3SG-ACC</ta>
            <ta e="T169" id="Seg_2561" s="T168">take-CVB.SEQ</ta>
            <ta e="T170" id="Seg_2562" s="T169">small.boat-INSTR</ta>
            <ta e="T171" id="Seg_2563" s="T170">go-CVB.SEQ</ta>
            <ta e="T172" id="Seg_2564" s="T171">seven</ta>
            <ta e="T173" id="Seg_2565" s="T172">wild.reindeer-ACC</ta>
            <ta e="T174" id="Seg_2566" s="T173">every-3SG-ACC</ta>
            <ta e="T175" id="Seg_2567" s="T174">cut-CVB.SIM</ta>
            <ta e="T176" id="Seg_2568" s="T175">chop-CVB.SEQ</ta>
            <ta e="T177" id="Seg_2569" s="T176">kill-ITER-CVB.SEQ</ta>
            <ta e="T178" id="Seg_2570" s="T177">throw-PRS.[3SG]</ta>
            <ta e="T179" id="Seg_2571" s="T178">this-ACC</ta>
            <ta e="T180" id="Seg_2572" s="T179">skin-CVB.SIM</ta>
            <ta e="T181" id="Seg_2573" s="T180">stay-PRS.[3SG]</ta>
            <ta e="T182" id="Seg_2574" s="T181">old.woman.[NOM]</ta>
            <ta e="T183" id="Seg_2575" s="T182">sleep-CVB.SEQ</ta>
            <ta e="T184" id="Seg_2576" s="T183">stay-PST2.[3SG]</ta>
            <ta e="T185" id="Seg_2577" s="T184">night.[NOM]</ta>
            <ta e="T186" id="Seg_2578" s="T185">late-VBZ-PST2.[3SG]</ta>
            <ta e="T187" id="Seg_2579" s="T186">human.being.[NOM]</ta>
            <ta e="T188" id="Seg_2580" s="T187">wake.up-EP-PST2-3SG</ta>
            <ta e="T189" id="Seg_2581" s="T188">iron</ta>
            <ta e="T190" id="Seg_2582" s="T189">chain-EP-INSTR</ta>
            <ta e="T191" id="Seg_2583" s="T190">sky-ABL</ta>
            <ta e="T192" id="Seg_2584" s="T191">go.down-CVB.SEQ</ta>
            <ta e="T193" id="Seg_2585" s="T192">stand-PTCP.PRS</ta>
            <ta e="T194" id="Seg_2586" s="T193">be-PST2.[3SG]</ta>
            <ta e="T195" id="Seg_2587" s="T194">iron</ta>
            <ta e="T196" id="Seg_2588" s="T195">cradle.[NOM]</ta>
            <ta e="T197" id="Seg_2589" s="T196">inside-3SG-DAT/LOC</ta>
            <ta e="T198" id="Seg_2590" s="T197">child.[NOM]</ta>
            <ta e="T199" id="Seg_2591" s="T198">there.is</ta>
            <ta e="T200" id="Seg_2592" s="T199">apparently</ta>
            <ta e="T201" id="Seg_2593" s="T200">cry-PRS.[3SG]</ta>
            <ta e="T202" id="Seg_2594" s="T201">old.woman.[NOM]</ta>
            <ta e="T203" id="Seg_2595" s="T202">feel.sorry-CVB.SIM</ta>
            <ta e="T204" id="Seg_2596" s="T203">think-PRS.[3SG]</ta>
            <ta e="T205" id="Seg_2597" s="T204">and</ta>
            <ta e="T206" id="Seg_2598" s="T205">be.careful-CVB.SEQ</ta>
            <ta e="T207" id="Seg_2599" s="T206">upper.part-3SG-INSTR</ta>
            <ta e="T208" id="Seg_2600" s="T207">lid-3SG-ACC</ta>
            <ta e="T209" id="Seg_2601" s="T208">open-CVB.SEQ</ta>
            <ta e="T210" id="Seg_2602" s="T209">see-PRS.[3SG]</ta>
            <ta e="T211" id="Seg_2603" s="T210">see-PST2-3SG</ta>
            <ta e="T212" id="Seg_2604" s="T211">snow.[NOM]</ta>
            <ta e="T213" id="Seg_2605" s="T212">like</ta>
            <ta e="T214" id="Seg_2606" s="T213">white</ta>
            <ta e="T215" id="Seg_2607" s="T214">very</ta>
            <ta e="T216" id="Seg_2608" s="T215">child.[NOM]</ta>
            <ta e="T217" id="Seg_2609" s="T216">lie-PTCP.PRS</ta>
            <ta e="T218" id="Seg_2610" s="T217">be-PST2.[3SG]</ta>
            <ta e="T219" id="Seg_2611" s="T218">this-DAT/LOC</ta>
            <ta e="T220" id="Seg_2612" s="T219">child-3SG.[NOM]</ta>
            <ta e="T221" id="Seg_2613" s="T220">die-PTCP.PRS</ta>
            <ta e="T222" id="Seg_2614" s="T221">voice-3SG.[NOM]</ta>
            <ta e="T223" id="Seg_2615" s="T222">speak-CVB.SIM</ta>
            <ta e="T224" id="Seg_2616" s="T223">fall-PST2.[3SG]</ta>
            <ta e="T225" id="Seg_2617" s="T224">eye-EP-1SG.[NOM]</ta>
            <ta e="T226" id="Seg_2618" s="T225">pierce-CVB.SIM</ta>
            <ta e="T227" id="Seg_2619" s="T226">pop-CVB.PURP</ta>
            <ta e="T228" id="Seg_2620" s="T227">make-PST1-3SG</ta>
            <ta e="T229" id="Seg_2621" s="T228">say-CVB.SIM-say-CVB.SIM</ta>
            <ta e="T230" id="Seg_2622" s="T229">chain-3SG.[NOM]</ta>
            <ta e="T231" id="Seg_2623" s="T230">up</ta>
            <ta e="T232" id="Seg_2624" s="T231">raise-CVB.SIM</ta>
            <ta e="T233" id="Seg_2625" s="T232">stand-PST2.[3SG]</ta>
            <ta e="T234" id="Seg_2626" s="T233">cradle-3SG-ACC</ta>
            <ta e="T235" id="Seg_2627" s="T234">child.[NOM]</ta>
            <ta e="T236" id="Seg_2628" s="T235">voice-3SG.[NOM]</ta>
            <ta e="T237" id="Seg_2629" s="T236">be.heard-EP-PST2.[3SG]</ta>
            <ta e="T238" id="Seg_2630" s="T237">this</ta>
            <ta e="T239" id="Seg_2631" s="T238">1SG.[NOM]</ta>
            <ta e="T240" id="Seg_2632" s="T239">elder.brother-EP-1SG.[NOM]</ta>
            <ta e="T241" id="Seg_2633" s="T240">Urung.Ajyy.[NOM]</ta>
            <ta e="T242" id="Seg_2634" s="T241">son-3SG.[NOM]</ta>
            <ta e="T243" id="Seg_2635" s="T242">be-PST1-3SG</ta>
            <ta e="T244" id="Seg_2636" s="T243">1SG-ACC</ta>
            <ta e="T245" id="Seg_2637" s="T244">elder.brother-EP-1SG.[NOM]</ta>
            <ta e="T246" id="Seg_2638" s="T245">send-PST2-3SG</ta>
            <ta e="T247" id="Seg_2639" s="T246">three</ta>
            <ta e="T248" id="Seg_2640" s="T247">day.and.night-DAT/LOC</ta>
            <ta e="T249" id="Seg_2641" s="T248">until</ta>
            <ta e="T250" id="Seg_2642" s="T249">1SG-ACC</ta>
            <ta e="T251" id="Seg_2643" s="T250">dirty</ta>
            <ta e="T252" id="Seg_2644" s="T251">eye-PL-EP-INSTR</ta>
            <ta e="T253" id="Seg_2645" s="T252">see-CVB.SEQ</ta>
            <ta e="T254" id="Seg_2646" s="T253">buckled.[NOM]</ta>
            <ta e="T255" id="Seg_2647" s="T254">make-FUT-3PL</ta>
            <ta e="T256" id="Seg_2648" s="T255">NEG-3SG</ta>
            <ta e="T257" id="Seg_2649" s="T256">say-CVB.SEQ</ta>
            <ta e="T258" id="Seg_2650" s="T257">now</ta>
            <ta e="T259" id="Seg_2651" s="T258">come.back-FUT-1SG</ta>
            <ta e="T260" id="Seg_2652" s="T259">come.back-FUT.[3SG]</ta>
            <ta e="T261" id="Seg_2653" s="T260">NEG-1SG</ta>
            <ta e="T262" id="Seg_2654" s="T261">that.[NOM]</ta>
            <ta e="T263" id="Seg_2655" s="T262">elder.brother-EP-1SG.[NOM]</ta>
            <ta e="T264" id="Seg_2656" s="T263">know-FUT.[3SG]</ta>
            <ta e="T265" id="Seg_2657" s="T264">say-PST2.[3SG]</ta>
            <ta e="T266" id="Seg_2658" s="T265">that</ta>
            <ta e="T267" id="Seg_2659" s="T266">time-DAT/LOC</ta>
            <ta e="T268" id="Seg_2660" s="T267">old.man.[NOM]</ta>
            <ta e="T269" id="Seg_2661" s="T268">wild.reindeer-PL-3SG-ACC</ta>
            <ta e="T270" id="Seg_2662" s="T269">skin-CVB.SEQ</ta>
            <ta e="T271" id="Seg_2663" s="T270">two</ta>
            <ta e="T272" id="Seg_2664" s="T271">hind.leg-3SG-ACC</ta>
            <ta e="T273" id="Seg_2665" s="T272">shoulder-DAT/LOC</ta>
            <ta e="T274" id="Seg_2666" s="T273">lay-CVB.SEQ</ta>
            <ta e="T275" id="Seg_2667" s="T274">come-CVB.SEQ</ta>
            <ta e="T276" id="Seg_2668" s="T275">go-PST2.[3SG]</ta>
            <ta e="T277" id="Seg_2669" s="T276">this-ACC</ta>
            <ta e="T278" id="Seg_2670" s="T277">old.woman-3SG.[NOM]</ta>
            <ta e="T279" id="Seg_2671" s="T278">meet-CVB.SEQ</ta>
            <ta e="T280" id="Seg_2672" s="T279">tell-PRS.[3SG]</ta>
            <ta e="T281" id="Seg_2673" s="T280">old.man.[NOM]</ta>
            <ta e="T282" id="Seg_2674" s="T281">believe-EP-NEG.CVB</ta>
            <ta e="T283" id="Seg_2675" s="T282">run-CVB.SEQ</ta>
            <ta e="T284" id="Seg_2676" s="T283">come-PRS.[3SG]</ta>
            <ta e="T285" id="Seg_2677" s="T284">NEG.EX-3SG-ACC</ta>
            <ta e="T286" id="Seg_2678" s="T285">see-CVB.SEQ</ta>
            <ta e="T287" id="Seg_2679" s="T286">after</ta>
            <ta e="T288" id="Seg_2680" s="T287">old.woman-3SG-ACC</ta>
            <ta e="T289" id="Seg_2681" s="T288">hit-PRS.[3SG]</ta>
            <ta e="T290" id="Seg_2682" s="T289">thigh-3SG-INSTR</ta>
            <ta e="T291" id="Seg_2683" s="T290">old.woman-3SG.[NOM]</ta>
            <ta e="T292" id="Seg_2684" s="T291">other.of.two</ta>
            <ta e="T293" id="Seg_2685" s="T292">thigh-3SG-ACC</ta>
            <ta e="T294" id="Seg_2686" s="T293">take-PRS.[3SG]</ta>
            <ta e="T295" id="Seg_2687" s="T294">and</ta>
            <ta e="T296" id="Seg_2688" s="T295">hit-EP-RECP/COLL-CVB.SEQ</ta>
            <ta e="T297" id="Seg_2689" s="T296">self</ta>
            <ta e="T298" id="Seg_2690" s="T297">self-3PL-ACC</ta>
            <ta e="T299" id="Seg_2691" s="T298">kill-CVB.SEQ</ta>
            <ta e="T300" id="Seg_2692" s="T299">throw-PRS-3PL</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_2693" s="T0">lange</ta>
            <ta e="T2" id="Seg_2694" s="T1">alter.Mann-PROPR</ta>
            <ta e="T3" id="Seg_2695" s="T2">Alte.[NOM]</ta>
            <ta e="T4" id="Seg_2696" s="T3">leben-PST2-3PL</ta>
            <ta e="T5" id="Seg_2697" s="T4">3PL.[NOM]</ta>
            <ta e="T6" id="Seg_2698" s="T5">eins</ta>
            <ta e="T7" id="Seg_2699" s="T6">Mädchen-PROPR-3PL</ta>
            <ta e="T8" id="Seg_2700" s="T7">dieses</ta>
            <ta e="T9" id="Seg_2701" s="T8">Mädchen-3PL.[NOM]</ta>
            <ta e="T10" id="Seg_2702" s="T9">am.Abend</ta>
            <ta e="T11" id="Seg_2703" s="T10">Leuchte-3PL-ACC</ta>
            <ta e="T12" id="Seg_2704" s="T11">ausmachen-PST1-3PL</ta>
            <ta e="T13" id="Seg_2705" s="T12">und</ta>
            <ta e="T14" id="Seg_2706" s="T13">sich.unterhalten-CVB.SEQ</ta>
            <ta e="T15" id="Seg_2707" s="T14">gehen-PRS.[3SG]</ta>
            <ta e="T16" id="Seg_2708" s="T15">1SG.[NOM]</ta>
            <ta e="T17" id="Seg_2709" s="T16">2SG-DAT/LOC</ta>
            <ta e="T18" id="Seg_2710" s="T17">Ehemann-DAT/LOC</ta>
            <ta e="T19" id="Seg_2711" s="T18">gehen-FUT-1SG</ta>
            <ta e="T20" id="Seg_2712" s="T19">sagen-PRS.[3SG]</ta>
            <ta e="T21" id="Seg_2713" s="T20">am.Tag</ta>
            <ta e="T22" id="Seg_2714" s="T21">Tochter-3PL-ACC</ta>
            <ta e="T23" id="Seg_2715" s="T22">fragen-CVB.SIM</ta>
            <ta e="T24" id="Seg_2716" s="T23">versuchen-PRS-3PL</ta>
            <ta e="T25" id="Seg_2717" s="T24">was.[NOM]</ta>
            <ta e="T26" id="Seg_2718" s="T25">NEG</ta>
            <ta e="T27" id="Seg_2719" s="T26">Wort-ACC</ta>
            <ta e="T28" id="Seg_2720" s="T27">3SG-ABL</ta>
            <ta e="T29" id="Seg_2721" s="T28">können-CVB.SEQ</ta>
            <ta e="T30" id="Seg_2722" s="T29">nehmen-NEG-3PL</ta>
            <ta e="T31" id="Seg_2723" s="T30">einmal</ta>
            <ta e="T32" id="Seg_2724" s="T31">am.Abend</ta>
            <ta e="T33" id="Seg_2725" s="T32">sich.hinlegen-CVB.SEQ</ta>
            <ta e="T34" id="Seg_2726" s="T33">gehen-CVB.SEQ-3PL</ta>
            <ta e="T35" id="Seg_2727" s="T34">Leuchte-3PL-ACC</ta>
            <ta e="T36" id="Seg_2728" s="T35">ausmachen-PTCP.PST.[NOM]</ta>
            <ta e="T37" id="Seg_2729" s="T36">wie</ta>
            <ta e="T38" id="Seg_2730" s="T37">bedecken-CVB.SEQ</ta>
            <ta e="T39" id="Seg_2731" s="T38">werfen-PRS-3PL</ta>
            <ta e="T40" id="Seg_2732" s="T39">Tochter-3PL.[NOM]</ta>
            <ta e="T41" id="Seg_2733" s="T40">wieder</ta>
            <ta e="T42" id="Seg_2734" s="T41">sprechen-CVB.SEQ</ta>
            <ta e="T43" id="Seg_2735" s="T42">gehen-PRS.[3SG]</ta>
            <ta e="T44" id="Seg_2736" s="T43">dieses-DAT/LOC</ta>
            <ta e="T45" id="Seg_2737" s="T44">Leuchte-3PL-GEN</ta>
            <ta e="T46" id="Seg_2738" s="T45">Deckel-3SG-ACC</ta>
            <ta e="T47" id="Seg_2739" s="T46">nehmen-CVB.SEQ</ta>
            <ta e="T48" id="Seg_2740" s="T47">werfen-PRS-3PL</ta>
            <ta e="T49" id="Seg_2741" s="T48">sehen-PST2-3PL</ta>
            <ta e="T50" id="Seg_2742" s="T49">Kind-3PL-ACC</ta>
            <ta e="T51" id="Seg_2743" s="T50">mit</ta>
            <ta e="T52" id="Seg_2744" s="T51">eins</ta>
            <ta e="T53" id="Seg_2745" s="T52">Mensch.[NOM]</ta>
            <ta e="T54" id="Seg_2746" s="T53">liegen-PRS.[3SG]</ta>
            <ta e="T55" id="Seg_2747" s="T54">hey</ta>
            <ta e="T56" id="Seg_2748" s="T55">doch</ta>
            <ta e="T57" id="Seg_2749" s="T56">Schwiegersohn-1PL.[NOM]</ta>
            <ta e="T58" id="Seg_2750" s="T57">sein-INFER-EP-2SG</ta>
            <ta e="T59" id="Seg_2751" s="T58">sagen-CVB.SEQ</ta>
            <ta e="T60" id="Seg_2752" s="T59">alter.Mann-PROPR</ta>
            <ta e="T61" id="Seg_2753" s="T60">Alte.[NOM]</ta>
            <ta e="T62" id="Seg_2754" s="T61">sprechen-PRS-3PL</ta>
            <ta e="T63" id="Seg_2755" s="T62">dieses-DAT/LOC</ta>
            <ta e="T64" id="Seg_2756" s="T63">Mensch-3PL.[NOM]</ta>
            <ta e="T65" id="Seg_2757" s="T64">sprechen-PRS.[3SG]</ta>
            <ta e="T66" id="Seg_2758" s="T65">Glück-2PL.[NOM]</ta>
            <ta e="T67" id="Seg_2759" s="T66">sein-INFER-3SG</ta>
            <ta e="T68" id="Seg_2760" s="T67">euer</ta>
            <ta e="T69" id="Seg_2761" s="T68">dieses.[NOM]</ta>
            <ta e="T70" id="Seg_2762" s="T69">1SG-ACC</ta>
            <ta e="T71" id="Seg_2763" s="T70">sehen-PST2-2PL</ta>
            <ta e="T72" id="Seg_2764" s="T71">wohin</ta>
            <ta e="T73" id="Seg_2765" s="T72">Tochter-2PL.[NOM]</ta>
            <ta e="T74" id="Seg_2766" s="T73">gehen-PTCP.PST-3SG-ACC</ta>
            <ta e="T75" id="Seg_2767" s="T74">wissen-EP-NEG.CVB</ta>
            <ta e="T76" id="Seg_2768" s="T75">bleiben-PTCP.FUT</ta>
            <ta e="T77" id="Seg_2769" s="T76">sein-PST1-2PL</ta>
            <ta e="T78" id="Seg_2770" s="T77">sagen-PRS.[3SG]</ta>
            <ta e="T79" id="Seg_2771" s="T78">jenes.EMPH.[NOM]</ta>
            <ta e="T80" id="Seg_2772" s="T79">und</ta>
            <ta e="T81" id="Seg_2773" s="T80">sein-COND.[3SG]</ta>
            <ta e="T82" id="Seg_2774" s="T81">Mädchen-2PL-ACC</ta>
            <ta e="T83" id="Seg_2775" s="T82">tragen-FUT-1SG</ta>
            <ta e="T84" id="Seg_2776" s="T83">1SG.[NOM]</ta>
            <ta e="T85" id="Seg_2777" s="T84">gehen-FUT-1SG</ta>
            <ta e="T86" id="Seg_2778" s="T85">morgen</ta>
            <ta e="T87" id="Seg_2779" s="T86">alter.Mann.[NOM]</ta>
            <ta e="T88" id="Seg_2780" s="T87">früh</ta>
            <ta e="T89" id="Seg_2781" s="T88">Tag.[NOM]</ta>
            <ta e="T90" id="Seg_2782" s="T89">hell.werden-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T91" id="Seg_2783" s="T90">hinausgehen-IMP.3SG</ta>
            <ta e="T92" id="Seg_2784" s="T91">sieben</ta>
            <ta e="T93" id="Seg_2785" s="T92">wildes.Rentier.[NOM]</ta>
            <ta e="T94" id="Seg_2786" s="T93">Fluss-ACC</ta>
            <ta e="T95" id="Seg_2787" s="T94">schwimmen-CAUS-PTCP.PRS</ta>
            <ta e="T96" id="Seg_2788" s="T95">sein-FUT-3SG</ta>
            <ta e="T97" id="Seg_2789" s="T96">jenes-INSTR</ta>
            <ta e="T98" id="Seg_2790" s="T97">wissen-FUT-2PL</ta>
            <ta e="T99" id="Seg_2791" s="T98">1SG.[NOM]</ta>
            <ta e="T100" id="Seg_2792" s="T99">Glück-ACC</ta>
            <ta e="T101" id="Seg_2793" s="T100">bringen-PTCP.PST-1SG-ACC</ta>
            <ta e="T102" id="Seg_2794" s="T101">sich.bemühen-CVB.SEQ</ta>
            <ta e="T103" id="Seg_2795" s="T102">jenes</ta>
            <ta e="T104" id="Seg_2796" s="T103">wildes.Rentier-PL-ACC</ta>
            <ta e="T105" id="Seg_2797" s="T104">alter.Mann.[NOM]</ta>
            <ta e="T106" id="Seg_2798" s="T105">töten-IMP.3SG</ta>
            <ta e="T107" id="Seg_2799" s="T106">jenes-ACC</ta>
            <ta e="T108" id="Seg_2800" s="T107">Haut.abziehen-CVB.SEQ</ta>
            <ta e="T109" id="Seg_2801" s="T108">aufhören-EP-CAUS-TEMP-3SG</ta>
            <ta e="T110" id="Seg_2802" s="T109">eisern</ta>
            <ta e="T111" id="Seg_2803" s="T110">Kette-EP-INSTR</ta>
            <ta e="T112" id="Seg_2804" s="T111">Himmel-ABL</ta>
            <ta e="T113" id="Seg_2805" s="T112">eisern</ta>
            <ta e="T114" id="Seg_2806" s="T113">Wiege-DAT/LOC</ta>
            <ta e="T115" id="Seg_2807" s="T114">Kind.[NOM]</ta>
            <ta e="T116" id="Seg_2808" s="T115">hinuntergehen-FUT-3SG</ta>
            <ta e="T117" id="Seg_2809" s="T116">jenes.[NOM]</ta>
            <ta e="T118" id="Seg_2810" s="T117">Deckel-PROPR.[NOM]</ta>
            <ta e="T119" id="Seg_2811" s="T118">sein-FUT-3SG</ta>
            <ta e="T120" id="Seg_2812" s="T119">ganz</ta>
            <ta e="T121" id="Seg_2813" s="T120">drei</ta>
            <ta e="T122" id="Seg_2814" s="T121">Tag.und.Nacht-DAT/LOC</ta>
            <ta e="T123" id="Seg_2815" s="T122">bis.zu</ta>
            <ta e="T124" id="Seg_2816" s="T123">Deckel-3SG-ACC</ta>
            <ta e="T125" id="Seg_2817" s="T124">öffnen-CVB.SEQ</ta>
            <ta e="T126" id="Seg_2818" s="T125">sehen-PTCP.PRS</ta>
            <ta e="T127" id="Seg_2819" s="T126">sein-APPR-2PL</ta>
            <ta e="T128" id="Seg_2820" s="T127">dieses-ACC</ta>
            <ta e="T129" id="Seg_2821" s="T128">sprechen-CVB.SEQ</ta>
            <ta e="T130" id="Seg_2822" s="T129">nachdem</ta>
            <ta e="T131" id="Seg_2823" s="T130">hinausgehen-EP-PST2.[3SG]</ta>
            <ta e="T132" id="Seg_2824" s="T131">Mensch-3PL.[NOM]</ta>
            <ta e="T133" id="Seg_2825" s="T132">Tochter-3PL.[NOM]</ta>
            <ta e="T134" id="Seg_2826" s="T133">jenes-ACC</ta>
            <ta e="T135" id="Seg_2827" s="T134">mitkommen-PST2.[3SG]</ta>
            <ta e="T136" id="Seg_2828" s="T135">alter.Mann-PROPR</ta>
            <ta e="T137" id="Seg_2829" s="T136">Alte.[NOM]</ta>
            <ta e="T138" id="Seg_2830" s="T137">jenes-ACC</ta>
            <ta e="T139" id="Seg_2831" s="T138">begleiten-CVB.SIM</ta>
            <ta e="T140" id="Seg_2832" s="T139">nach.draußen</ta>
            <ta e="T141" id="Seg_2833" s="T140">hinausgehen-EP-PST2-3PL</ta>
            <ta e="T142" id="Seg_2834" s="T141">sehen-PST2-3PL</ta>
            <ta e="T143" id="Seg_2835" s="T142">Mann.[NOM]</ta>
            <ta e="T144" id="Seg_2836" s="T143">Mensch-3PL.[NOM]-Schwiegersohn-3PL.[NOM]</ta>
            <ta e="T145" id="Seg_2837" s="T144">groß</ta>
            <ta e="T146" id="Seg_2838" s="T145">meist</ta>
            <ta e="T147" id="Seg_2839" s="T146">wildes.Rentier.[NOM]</ta>
            <ta e="T148" id="Seg_2840" s="T147">männliches.Rentier-3SG.[NOM]</ta>
            <ta e="T149" id="Seg_2841" s="T148">sein-CVB.SEQ</ta>
            <ta e="T150" id="Seg_2842" s="T149">Tochter-3PL.[NOM]</ta>
            <ta e="T151" id="Seg_2843" s="T150">wildes.Rentier.[NOM]</ta>
            <ta e="T152" id="Seg_2844" s="T151">zweijähriges.Rentier-3SG.[NOM]</ta>
            <ta e="T153" id="Seg_2845" s="T152">werden-CVB.SEQ</ta>
            <ta e="T154" id="Seg_2846" s="T153">Fluss-ACC</ta>
            <ta e="T155" id="Seg_2847" s="T154">schwimmen-CVB.SIM</ta>
            <ta e="T156" id="Seg_2848" s="T155">stehen-PST2-3PL</ta>
            <ta e="T157" id="Seg_2849" s="T156">am.Morgen</ta>
            <ta e="T158" id="Seg_2850" s="T157">alter.Mann.[NOM]</ta>
            <ta e="T159" id="Seg_2851" s="T158">pinkeln-CVB.SIM</ta>
            <ta e="T160" id="Seg_2852" s="T159">hinausgehen-PRS.[3SG]</ta>
            <ta e="T161" id="Seg_2853" s="T160">sehen-PST2-3SG</ta>
            <ta e="T162" id="Seg_2854" s="T161">sieben</ta>
            <ta e="T163" id="Seg_2855" s="T162">wildes.Rentier.[NOM]</ta>
            <ta e="T164" id="Seg_2856" s="T163">Fluss-ACC</ta>
            <ta e="T165" id="Seg_2857" s="T164">schwimmen-CVB.SEQ</ta>
            <ta e="T166" id="Seg_2858" s="T165">gehen-PRS.[3SG]</ta>
            <ta e="T167" id="Seg_2859" s="T166">dieses-ACC</ta>
            <ta e="T168" id="Seg_2860" s="T167">Speer-3SG-ACC</ta>
            <ta e="T169" id="Seg_2861" s="T168">nehmen-CVB.SEQ</ta>
            <ta e="T170" id="Seg_2862" s="T169">kleines.Boot-INSTR</ta>
            <ta e="T171" id="Seg_2863" s="T170">gehen-CVB.SEQ</ta>
            <ta e="T172" id="Seg_2864" s="T171">sieben</ta>
            <ta e="T173" id="Seg_2865" s="T172">wildes.Rentier-ACC</ta>
            <ta e="T174" id="Seg_2866" s="T173">jeder-3SG-ACC</ta>
            <ta e="T175" id="Seg_2867" s="T174">schneiden-CVB.SIM</ta>
            <ta e="T176" id="Seg_2868" s="T175">hacken-CVB.SEQ</ta>
            <ta e="T177" id="Seg_2869" s="T176">töten-ITER-CVB.SEQ</ta>
            <ta e="T178" id="Seg_2870" s="T177">werfen-PRS.[3SG]</ta>
            <ta e="T179" id="Seg_2871" s="T178">dieses-ACC</ta>
            <ta e="T180" id="Seg_2872" s="T179">Haut.abziehen-CVB.SIM</ta>
            <ta e="T181" id="Seg_2873" s="T180">bleiben-PRS.[3SG]</ta>
            <ta e="T182" id="Seg_2874" s="T181">Alte.[NOM]</ta>
            <ta e="T183" id="Seg_2875" s="T182">schlafen-CVB.SEQ</ta>
            <ta e="T184" id="Seg_2876" s="T183">bleiben-PST2.[3SG]</ta>
            <ta e="T185" id="Seg_2877" s="T184">Nacht.[NOM]</ta>
            <ta e="T186" id="Seg_2878" s="T185">spät-VBZ-PST2.[3SG]</ta>
            <ta e="T187" id="Seg_2879" s="T186">Mensch.[NOM]</ta>
            <ta e="T188" id="Seg_2880" s="T187">aufwachen-EP-PST2-3SG</ta>
            <ta e="T189" id="Seg_2881" s="T188">eisern</ta>
            <ta e="T190" id="Seg_2882" s="T189">Kette-EP-INSTR</ta>
            <ta e="T191" id="Seg_2883" s="T190">Himmel-ABL</ta>
            <ta e="T192" id="Seg_2884" s="T191">hinuntergehen-CVB.SEQ</ta>
            <ta e="T193" id="Seg_2885" s="T192">stehen-PTCP.PRS</ta>
            <ta e="T194" id="Seg_2886" s="T193">sein-PST2.[3SG]</ta>
            <ta e="T195" id="Seg_2887" s="T194">eisern</ta>
            <ta e="T196" id="Seg_2888" s="T195">Wiege.[NOM]</ta>
            <ta e="T197" id="Seg_2889" s="T196">Inneres-3SG-DAT/LOC</ta>
            <ta e="T198" id="Seg_2890" s="T197">Kind.[NOM]</ta>
            <ta e="T199" id="Seg_2891" s="T198">es.gibt</ta>
            <ta e="T200" id="Seg_2892" s="T199">offenbar</ta>
            <ta e="T201" id="Seg_2893" s="T200">weinen-PRS.[3SG]</ta>
            <ta e="T202" id="Seg_2894" s="T201">Alte.[NOM]</ta>
            <ta e="T203" id="Seg_2895" s="T202">bemitleiden-CVB.SIM</ta>
            <ta e="T204" id="Seg_2896" s="T203">denken-PRS.[3SG]</ta>
            <ta e="T205" id="Seg_2897" s="T204">und</ta>
            <ta e="T206" id="Seg_2898" s="T205">vorsichtig.sein-CVB.SEQ</ta>
            <ta e="T207" id="Seg_2899" s="T206">oberer.Teil-3SG-INSTR</ta>
            <ta e="T208" id="Seg_2900" s="T207">Deckel-3SG-ACC</ta>
            <ta e="T209" id="Seg_2901" s="T208">öffnen-CVB.SEQ</ta>
            <ta e="T210" id="Seg_2902" s="T209">sehen-PRS.[3SG]</ta>
            <ta e="T211" id="Seg_2903" s="T210">sehen-PST2-3SG</ta>
            <ta e="T212" id="Seg_2904" s="T211">Schnee.[NOM]</ta>
            <ta e="T213" id="Seg_2905" s="T212">wie</ta>
            <ta e="T214" id="Seg_2906" s="T213">weiß</ta>
            <ta e="T215" id="Seg_2907" s="T214">sehr</ta>
            <ta e="T216" id="Seg_2908" s="T215">Kind.[NOM]</ta>
            <ta e="T217" id="Seg_2909" s="T216">liegen-PTCP.PRS</ta>
            <ta e="T218" id="Seg_2910" s="T217">sein-PST2.[3SG]</ta>
            <ta e="T219" id="Seg_2911" s="T218">dieses-DAT/LOC</ta>
            <ta e="T220" id="Seg_2912" s="T219">Kind-3SG.[NOM]</ta>
            <ta e="T221" id="Seg_2913" s="T220">sterben-PTCP.PRS</ta>
            <ta e="T222" id="Seg_2914" s="T221">Stimme-3SG.[NOM]</ta>
            <ta e="T223" id="Seg_2915" s="T222">sprechen-CVB.SIM</ta>
            <ta e="T224" id="Seg_2916" s="T223">fallen-PST2.[3SG]</ta>
            <ta e="T225" id="Seg_2917" s="T224">Auge-EP-1SG.[NOM]</ta>
            <ta e="T226" id="Seg_2918" s="T225">durchstechen-CVB.SIM</ta>
            <ta e="T227" id="Seg_2919" s="T226">platzen-CVB.PURP</ta>
            <ta e="T228" id="Seg_2920" s="T227">machen-PST1-3SG</ta>
            <ta e="T229" id="Seg_2921" s="T228">sagen-CVB.SIM-sagen-CVB.SIM</ta>
            <ta e="T230" id="Seg_2922" s="T229">Kette-3SG.[NOM]</ta>
            <ta e="T231" id="Seg_2923" s="T230">nach.oben</ta>
            <ta e="T232" id="Seg_2924" s="T231">heben-CVB.SIM</ta>
            <ta e="T233" id="Seg_2925" s="T232">stehen-PST2.[3SG]</ta>
            <ta e="T234" id="Seg_2926" s="T233">Wiege-3SG-ACC</ta>
            <ta e="T235" id="Seg_2927" s="T234">Kind.[NOM]</ta>
            <ta e="T236" id="Seg_2928" s="T235">Stimme-3SG.[NOM]</ta>
            <ta e="T237" id="Seg_2929" s="T236">gehört.werden-EP-PST2.[3SG]</ta>
            <ta e="T238" id="Seg_2930" s="T237">dieses</ta>
            <ta e="T239" id="Seg_2931" s="T238">1SG.[NOM]</ta>
            <ta e="T240" id="Seg_2932" s="T239">älterer.Bruder-EP-1SG.[NOM]</ta>
            <ta e="T241" id="Seg_2933" s="T240">Ürüng.Ajyy.[NOM]</ta>
            <ta e="T242" id="Seg_2934" s="T241">Sohn-3SG.[NOM]</ta>
            <ta e="T243" id="Seg_2935" s="T242">sein-PST1-3SG</ta>
            <ta e="T244" id="Seg_2936" s="T243">1SG-ACC</ta>
            <ta e="T245" id="Seg_2937" s="T244">älterer.Bruder-EP-1SG.[NOM]</ta>
            <ta e="T246" id="Seg_2938" s="T245">schicken-PST2-3SG</ta>
            <ta e="T247" id="Seg_2939" s="T246">drei</ta>
            <ta e="T248" id="Seg_2940" s="T247">Tag.und.Nacht-DAT/LOC</ta>
            <ta e="T249" id="Seg_2941" s="T248">bis.zu</ta>
            <ta e="T250" id="Seg_2942" s="T249">1SG-ACC</ta>
            <ta e="T251" id="Seg_2943" s="T250">schmutzig</ta>
            <ta e="T252" id="Seg_2944" s="T251">Auge-PL-EP-INSTR</ta>
            <ta e="T253" id="Seg_2945" s="T252">sehen-CVB.SEQ</ta>
            <ta e="T254" id="Seg_2946" s="T253">krumm.[NOM]</ta>
            <ta e="T255" id="Seg_2947" s="T254">machen-FUT-3PL</ta>
            <ta e="T256" id="Seg_2948" s="T255">NEG-3SG</ta>
            <ta e="T257" id="Seg_2949" s="T256">sagen-CVB.SEQ</ta>
            <ta e="T258" id="Seg_2950" s="T257">jetzt</ta>
            <ta e="T259" id="Seg_2951" s="T258">zurückkommen-FUT-1SG</ta>
            <ta e="T260" id="Seg_2952" s="T259">zurückkommen-FUT.[3SG]</ta>
            <ta e="T261" id="Seg_2953" s="T260">NEG-1SG</ta>
            <ta e="T262" id="Seg_2954" s="T261">jenes.[NOM]</ta>
            <ta e="T263" id="Seg_2955" s="T262">älterer.Bruder-EP-1SG.[NOM]</ta>
            <ta e="T264" id="Seg_2956" s="T263">wissen-FUT.[3SG]</ta>
            <ta e="T265" id="Seg_2957" s="T264">sagen-PST2.[3SG]</ta>
            <ta e="T266" id="Seg_2958" s="T265">jenes</ta>
            <ta e="T267" id="Seg_2959" s="T266">Zeit-DAT/LOC</ta>
            <ta e="T268" id="Seg_2960" s="T267">alter.Mann.[NOM]</ta>
            <ta e="T269" id="Seg_2961" s="T268">wildes.Rentier-PL-3SG-ACC</ta>
            <ta e="T270" id="Seg_2962" s="T269">Haut.abziehen-CVB.SEQ</ta>
            <ta e="T271" id="Seg_2963" s="T270">zwei</ta>
            <ta e="T272" id="Seg_2964" s="T271">Hinterbein-3SG-ACC</ta>
            <ta e="T273" id="Seg_2965" s="T272">Schulter-DAT/LOC</ta>
            <ta e="T274" id="Seg_2966" s="T273">legen-CVB.SEQ</ta>
            <ta e="T275" id="Seg_2967" s="T274">kommen-CVB.SEQ</ta>
            <ta e="T276" id="Seg_2968" s="T275">gehen-PST2.[3SG]</ta>
            <ta e="T277" id="Seg_2969" s="T276">dieses-ACC</ta>
            <ta e="T278" id="Seg_2970" s="T277">Alte-3SG.[NOM]</ta>
            <ta e="T279" id="Seg_2971" s="T278">treffen-CVB.SEQ</ta>
            <ta e="T280" id="Seg_2972" s="T279">erzählen-PRS.[3SG]</ta>
            <ta e="T281" id="Seg_2973" s="T280">alter.Mann.[NOM]</ta>
            <ta e="T282" id="Seg_2974" s="T281">glauben-EP-NEG.CVB</ta>
            <ta e="T283" id="Seg_2975" s="T282">laufen-CVB.SEQ</ta>
            <ta e="T284" id="Seg_2976" s="T283">kommen-PRS.[3SG]</ta>
            <ta e="T285" id="Seg_2977" s="T284">NEG.EX-3SG-ACC</ta>
            <ta e="T286" id="Seg_2978" s="T285">sehen-CVB.SEQ</ta>
            <ta e="T287" id="Seg_2979" s="T286">nachdem</ta>
            <ta e="T288" id="Seg_2980" s="T287">Alte-3SG-ACC</ta>
            <ta e="T289" id="Seg_2981" s="T288">schlagen-PRS.[3SG]</ta>
            <ta e="T290" id="Seg_2982" s="T289">Oberschenkel-3SG-INSTR</ta>
            <ta e="T291" id="Seg_2983" s="T290">Alte-3SG.[NOM]</ta>
            <ta e="T292" id="Seg_2984" s="T291">anderer.von.zwei</ta>
            <ta e="T293" id="Seg_2985" s="T292">Oberschenkel-3SG-ACC</ta>
            <ta e="T294" id="Seg_2986" s="T293">nehmen-PRS.[3SG]</ta>
            <ta e="T295" id="Seg_2987" s="T294">und</ta>
            <ta e="T296" id="Seg_2988" s="T295">schlagen-EP-RECP/COLL-CVB.SEQ</ta>
            <ta e="T297" id="Seg_2989" s="T296">selbst</ta>
            <ta e="T298" id="Seg_2990" s="T297">selbst-3PL-ACC</ta>
            <ta e="T299" id="Seg_2991" s="T298">töten-CVB.SEQ</ta>
            <ta e="T300" id="Seg_2992" s="T299">werfen-PRS-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_2993" s="T0">давно</ta>
            <ta e="T2" id="Seg_2994" s="T1">старик-PROPR</ta>
            <ta e="T3" id="Seg_2995" s="T2">старуха.[NOM]</ta>
            <ta e="T4" id="Seg_2996" s="T3">жить-PST2-3PL</ta>
            <ta e="T5" id="Seg_2997" s="T4">3PL.[NOM]</ta>
            <ta e="T6" id="Seg_2998" s="T5">один</ta>
            <ta e="T7" id="Seg_2999" s="T6">девушка-PROPR-3PL</ta>
            <ta e="T8" id="Seg_3000" s="T7">этот</ta>
            <ta e="T9" id="Seg_3001" s="T8">девушка-3PL.[NOM]</ta>
            <ta e="T10" id="Seg_3002" s="T9">вечером</ta>
            <ta e="T11" id="Seg_3003" s="T10">светильник-3PL-ACC</ta>
            <ta e="T12" id="Seg_3004" s="T11">гасить-PST1-3PL</ta>
            <ta e="T13" id="Seg_3005" s="T12">да</ta>
            <ta e="T14" id="Seg_3006" s="T13">разговаривать-CVB.SEQ</ta>
            <ta e="T15" id="Seg_3007" s="T14">идти-PRS.[3SG]</ta>
            <ta e="T16" id="Seg_3008" s="T15">1SG.[NOM]</ta>
            <ta e="T17" id="Seg_3009" s="T16">2SG-DAT/LOC</ta>
            <ta e="T18" id="Seg_3010" s="T17">муж-DAT/LOC</ta>
            <ta e="T19" id="Seg_3011" s="T18">идти-FUT-1SG</ta>
            <ta e="T20" id="Seg_3012" s="T19">говорить-PRS.[3SG]</ta>
            <ta e="T21" id="Seg_3013" s="T20">днем</ta>
            <ta e="T22" id="Seg_3014" s="T21">дочь-3PL-ACC</ta>
            <ta e="T23" id="Seg_3015" s="T22">спрашивать-CVB.SIM</ta>
            <ta e="T24" id="Seg_3016" s="T23">пытаться-PRS-3PL</ta>
            <ta e="T25" id="Seg_3017" s="T24">что.[NOM]</ta>
            <ta e="T26" id="Seg_3018" s="T25">NEG</ta>
            <ta e="T27" id="Seg_3019" s="T26">слово-ACC</ta>
            <ta e="T28" id="Seg_3020" s="T27">3SG-ABL</ta>
            <ta e="T29" id="Seg_3021" s="T28">мочь-CVB.SEQ</ta>
            <ta e="T30" id="Seg_3022" s="T29">взять-NEG-3PL</ta>
            <ta e="T31" id="Seg_3023" s="T30">однажды</ta>
            <ta e="T32" id="Seg_3024" s="T31">вечером</ta>
            <ta e="T33" id="Seg_3025" s="T32">ложиться-CVB.SEQ</ta>
            <ta e="T34" id="Seg_3026" s="T33">идти-CVB.SEQ-3PL</ta>
            <ta e="T35" id="Seg_3027" s="T34">светильник-3PL-ACC</ta>
            <ta e="T36" id="Seg_3028" s="T35">гасить-PTCP.PST.[NOM]</ta>
            <ta e="T37" id="Seg_3029" s="T36">как</ta>
            <ta e="T38" id="Seg_3030" s="T37">покрывать-CVB.SEQ</ta>
            <ta e="T39" id="Seg_3031" s="T38">бросать-PRS-3PL</ta>
            <ta e="T40" id="Seg_3032" s="T39">дочь-3PL.[NOM]</ta>
            <ta e="T41" id="Seg_3033" s="T40">опять</ta>
            <ta e="T42" id="Seg_3034" s="T41">говорить-CVB.SEQ</ta>
            <ta e="T43" id="Seg_3035" s="T42">идти-PRS.[3SG]</ta>
            <ta e="T44" id="Seg_3036" s="T43">этот-DAT/LOC</ta>
            <ta e="T45" id="Seg_3037" s="T44">светильник-3PL-GEN</ta>
            <ta e="T46" id="Seg_3038" s="T45">крышка-3SG-ACC</ta>
            <ta e="T47" id="Seg_3039" s="T46">взять-CVB.SEQ</ta>
            <ta e="T48" id="Seg_3040" s="T47">бросать-PRS-3PL</ta>
            <ta e="T49" id="Seg_3041" s="T48">видеть-PST2-3PL</ta>
            <ta e="T50" id="Seg_3042" s="T49">ребенок-3PL-ACC</ta>
            <ta e="T51" id="Seg_3043" s="T50">с</ta>
            <ta e="T52" id="Seg_3044" s="T51">один</ta>
            <ta e="T53" id="Seg_3045" s="T52">человек.[NOM]</ta>
            <ta e="T54" id="Seg_3046" s="T53">лежать-PRS.[3SG]</ta>
            <ta e="T55" id="Seg_3047" s="T54">ээ</ta>
            <ta e="T56" id="Seg_3048" s="T55">вот</ta>
            <ta e="T57" id="Seg_3049" s="T56">зять-1PL.[NOM]</ta>
            <ta e="T58" id="Seg_3050" s="T57">быть-INFER-EP-2SG</ta>
            <ta e="T59" id="Seg_3051" s="T58">говорить-CVB.SEQ</ta>
            <ta e="T60" id="Seg_3052" s="T59">старик-PROPR</ta>
            <ta e="T61" id="Seg_3053" s="T60">старуха.[NOM]</ta>
            <ta e="T62" id="Seg_3054" s="T61">говорить-PRS-3PL</ta>
            <ta e="T63" id="Seg_3055" s="T62">этот-DAT/LOC</ta>
            <ta e="T64" id="Seg_3056" s="T63">человек-3PL.[NOM]</ta>
            <ta e="T65" id="Seg_3057" s="T64">говорить-PRS.[3SG]</ta>
            <ta e="T66" id="Seg_3058" s="T65">счастье-2PL.[NOM]</ta>
            <ta e="T67" id="Seg_3059" s="T66">быть-INFER-3SG</ta>
            <ta e="T68" id="Seg_3060" s="T67">ваш</ta>
            <ta e="T69" id="Seg_3061" s="T68">тот.[NOM]</ta>
            <ta e="T70" id="Seg_3062" s="T69">1SG-ACC</ta>
            <ta e="T71" id="Seg_3063" s="T70">видеть-PST2-2PL</ta>
            <ta e="T72" id="Seg_3064" s="T71">куда</ta>
            <ta e="T73" id="Seg_3065" s="T72">дочь-2PL.[NOM]</ta>
            <ta e="T74" id="Seg_3066" s="T73">идти-PTCP.PST-3SG-ACC</ta>
            <ta e="T75" id="Seg_3067" s="T74">знать-EP-NEG.CVB</ta>
            <ta e="T76" id="Seg_3068" s="T75">оставаться-PTCP.FUT</ta>
            <ta e="T77" id="Seg_3069" s="T76">быть-PST1-2PL</ta>
            <ta e="T78" id="Seg_3070" s="T77">говорить-PRS.[3SG]</ta>
            <ta e="T79" id="Seg_3071" s="T78">тот.EMPH.[NOM]</ta>
            <ta e="T80" id="Seg_3072" s="T79">да</ta>
            <ta e="T81" id="Seg_3073" s="T80">быть-COND.[3SG]</ta>
            <ta e="T82" id="Seg_3074" s="T81">девушка-2PL-ACC</ta>
            <ta e="T83" id="Seg_3075" s="T82">носить-FUT-1SG</ta>
            <ta e="T84" id="Seg_3076" s="T83">1SG.[NOM]</ta>
            <ta e="T85" id="Seg_3077" s="T84">идти-FUT-1SG</ta>
            <ta e="T86" id="Seg_3078" s="T85">завтра</ta>
            <ta e="T87" id="Seg_3079" s="T86">старик.[NOM]</ta>
            <ta e="T88" id="Seg_3080" s="T87">рано</ta>
            <ta e="T89" id="Seg_3081" s="T88">день.[NOM]</ta>
            <ta e="T90" id="Seg_3082" s="T89">светать-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T91" id="Seg_3083" s="T90">выйти-IMP.3SG</ta>
            <ta e="T92" id="Seg_3084" s="T91">семь</ta>
            <ta e="T93" id="Seg_3085" s="T92">дикий.олень.[NOM]</ta>
            <ta e="T94" id="Seg_3086" s="T93">река-ACC</ta>
            <ta e="T95" id="Seg_3087" s="T94">плавать-CAUS-PTCP.PRS</ta>
            <ta e="T96" id="Seg_3088" s="T95">быть-FUT-3SG</ta>
            <ta e="T97" id="Seg_3089" s="T96">тот-INSTR</ta>
            <ta e="T98" id="Seg_3090" s="T97">знать-FUT-2PL</ta>
            <ta e="T99" id="Seg_3091" s="T98">1SG.[NOM]</ta>
            <ta e="T100" id="Seg_3092" s="T99">счастье-ACC</ta>
            <ta e="T101" id="Seg_3093" s="T100">принести-PTCP.PST-1SG-ACC</ta>
            <ta e="T102" id="Seg_3094" s="T101">стараться-CVB.SEQ</ta>
            <ta e="T103" id="Seg_3095" s="T102">тот</ta>
            <ta e="T104" id="Seg_3096" s="T103">дикий.олень-PL-ACC</ta>
            <ta e="T105" id="Seg_3097" s="T104">старик.[NOM]</ta>
            <ta e="T106" id="Seg_3098" s="T105">убить-IMP.3SG</ta>
            <ta e="T107" id="Seg_3099" s="T106">тот-ACC</ta>
            <ta e="T108" id="Seg_3100" s="T107">сдирать.кожу-CVB.SEQ</ta>
            <ta e="T109" id="Seg_3101" s="T108">кончать-EP-CAUS-TEMP-3SG</ta>
            <ta e="T110" id="Seg_3102" s="T109">железный</ta>
            <ta e="T111" id="Seg_3103" s="T110">цепь-EP-INSTR</ta>
            <ta e="T112" id="Seg_3104" s="T111">небо-ABL</ta>
            <ta e="T113" id="Seg_3105" s="T112">железный</ta>
            <ta e="T114" id="Seg_3106" s="T113">колыбель-DAT/LOC</ta>
            <ta e="T115" id="Seg_3107" s="T114">ребенок.[NOM]</ta>
            <ta e="T116" id="Seg_3108" s="T115">сходить-FUT-3SG</ta>
            <ta e="T117" id="Seg_3109" s="T116">тот.[NOM]</ta>
            <ta e="T118" id="Seg_3110" s="T117">крышка-PROPR.[NOM]</ta>
            <ta e="T119" id="Seg_3111" s="T118">быть-FUT-3SG</ta>
            <ta e="T120" id="Seg_3112" s="T119">совсем</ta>
            <ta e="T121" id="Seg_3113" s="T120">три</ta>
            <ta e="T122" id="Seg_3114" s="T121">сутки-DAT/LOC</ta>
            <ta e="T123" id="Seg_3115" s="T122">пока</ta>
            <ta e="T124" id="Seg_3116" s="T123">крышка-3SG-ACC</ta>
            <ta e="T125" id="Seg_3117" s="T124">открывать-CVB.SEQ</ta>
            <ta e="T126" id="Seg_3118" s="T125">видеть-PTCP.PRS</ta>
            <ta e="T127" id="Seg_3119" s="T126">быть-APPR-2PL</ta>
            <ta e="T128" id="Seg_3120" s="T127">этот-ACC</ta>
            <ta e="T129" id="Seg_3121" s="T128">говорить-CVB.SEQ</ta>
            <ta e="T130" id="Seg_3122" s="T129">после</ta>
            <ta e="T131" id="Seg_3123" s="T130">выйти-EP-PST2.[3SG]</ta>
            <ta e="T132" id="Seg_3124" s="T131">человек-3PL.[NOM]</ta>
            <ta e="T133" id="Seg_3125" s="T132">дочь-3PL.[NOM]</ta>
            <ta e="T134" id="Seg_3126" s="T133">тот-ACC</ta>
            <ta e="T135" id="Seg_3127" s="T134">сопровождать-PST2.[3SG]</ta>
            <ta e="T136" id="Seg_3128" s="T135">старик-PROPR</ta>
            <ta e="T137" id="Seg_3129" s="T136">старуха.[NOM]</ta>
            <ta e="T138" id="Seg_3130" s="T137">тот-ACC</ta>
            <ta e="T139" id="Seg_3131" s="T138">провожать-CVB.SIM</ta>
            <ta e="T140" id="Seg_3132" s="T139">на.улицу</ta>
            <ta e="T141" id="Seg_3133" s="T140">выйти-EP-PST2-3PL</ta>
            <ta e="T142" id="Seg_3134" s="T141">видеть-PST2-3PL</ta>
            <ta e="T143" id="Seg_3135" s="T142">мужчина.[NOM]</ta>
            <ta e="T144" id="Seg_3136" s="T143">человек-3PL.[NOM]-зять-3PL.[NOM]</ta>
            <ta e="T145" id="Seg_3137" s="T144">большой</ta>
            <ta e="T146" id="Seg_3138" s="T145">самый</ta>
            <ta e="T147" id="Seg_3139" s="T146">дикий.олень.[NOM]</ta>
            <ta e="T148" id="Seg_3140" s="T147">олений.самец-3SG.[NOM]</ta>
            <ta e="T149" id="Seg_3141" s="T148">быть-CVB.SEQ</ta>
            <ta e="T150" id="Seg_3142" s="T149">дочь-3PL.[NOM]</ta>
            <ta e="T151" id="Seg_3143" s="T150">дикий.олень.[NOM]</ta>
            <ta e="T152" id="Seg_3144" s="T151">двухгодовадый.олень-3SG.[NOM]</ta>
            <ta e="T153" id="Seg_3145" s="T152">становиться-CVB.SEQ</ta>
            <ta e="T154" id="Seg_3146" s="T153">река-ACC</ta>
            <ta e="T155" id="Seg_3147" s="T154">плавать-CVB.SIM</ta>
            <ta e="T156" id="Seg_3148" s="T155">стоять-PST2-3PL</ta>
            <ta e="T157" id="Seg_3149" s="T156">утром</ta>
            <ta e="T158" id="Seg_3150" s="T157">старик.[NOM]</ta>
            <ta e="T159" id="Seg_3151" s="T158">мочиться-CVB.SIM</ta>
            <ta e="T160" id="Seg_3152" s="T159">выйти-PRS.[3SG]</ta>
            <ta e="T161" id="Seg_3153" s="T160">видеть-PST2-3SG</ta>
            <ta e="T162" id="Seg_3154" s="T161">семь</ta>
            <ta e="T163" id="Seg_3155" s="T162">дикий.олень.[NOM]</ta>
            <ta e="T164" id="Seg_3156" s="T163">река-ACC</ta>
            <ta e="T165" id="Seg_3157" s="T164">плавать-CVB.SEQ</ta>
            <ta e="T166" id="Seg_3158" s="T165">идти-PRS.[3SG]</ta>
            <ta e="T167" id="Seg_3159" s="T166">этот-ACC</ta>
            <ta e="T168" id="Seg_3160" s="T167">копье-3SG-ACC</ta>
            <ta e="T169" id="Seg_3161" s="T168">взять-CVB.SEQ</ta>
            <ta e="T170" id="Seg_3162" s="T169">лодочка-INSTR</ta>
            <ta e="T171" id="Seg_3163" s="T170">идти-CVB.SEQ</ta>
            <ta e="T172" id="Seg_3164" s="T171">семь</ta>
            <ta e="T173" id="Seg_3165" s="T172">дикий.олень-ACC</ta>
            <ta e="T174" id="Seg_3166" s="T173">каждый-3SG-ACC</ta>
            <ta e="T175" id="Seg_3167" s="T174">резать-CVB.SIM</ta>
            <ta e="T176" id="Seg_3168" s="T175">колоть-CVB.SEQ</ta>
            <ta e="T177" id="Seg_3169" s="T176">убить-ITER-CVB.SEQ</ta>
            <ta e="T178" id="Seg_3170" s="T177">бросать-PRS.[3SG]</ta>
            <ta e="T179" id="Seg_3171" s="T178">этот-ACC</ta>
            <ta e="T180" id="Seg_3172" s="T179">сдирать.кожу-CVB.SIM</ta>
            <ta e="T181" id="Seg_3173" s="T180">оставаться-PRS.[3SG]</ta>
            <ta e="T182" id="Seg_3174" s="T181">старуха.[NOM]</ta>
            <ta e="T183" id="Seg_3175" s="T182">спать-CVB.SEQ</ta>
            <ta e="T184" id="Seg_3176" s="T183">оставаться-PST2.[3SG]</ta>
            <ta e="T185" id="Seg_3177" s="T184">ночь.[NOM]</ta>
            <ta e="T186" id="Seg_3178" s="T185">поздний-VBZ-PST2.[3SG]</ta>
            <ta e="T187" id="Seg_3179" s="T186">человек.[NOM]</ta>
            <ta e="T188" id="Seg_3180" s="T187">просыпаться-EP-PST2-3SG</ta>
            <ta e="T189" id="Seg_3181" s="T188">железный</ta>
            <ta e="T190" id="Seg_3182" s="T189">цепь-EP-INSTR</ta>
            <ta e="T191" id="Seg_3183" s="T190">небо-ABL</ta>
            <ta e="T192" id="Seg_3184" s="T191">сходить-CVB.SEQ</ta>
            <ta e="T193" id="Seg_3185" s="T192">стоять-PTCP.PRS</ta>
            <ta e="T194" id="Seg_3186" s="T193">быть-PST2.[3SG]</ta>
            <ta e="T195" id="Seg_3187" s="T194">железный</ta>
            <ta e="T196" id="Seg_3188" s="T195">колыбель.[NOM]</ta>
            <ta e="T197" id="Seg_3189" s="T196">нутро-3SG-DAT/LOC</ta>
            <ta e="T198" id="Seg_3190" s="T197">ребенок.[NOM]</ta>
            <ta e="T199" id="Seg_3191" s="T198">есть</ta>
            <ta e="T200" id="Seg_3192" s="T199">наверное</ta>
            <ta e="T201" id="Seg_3193" s="T200">плакать-PRS.[3SG]</ta>
            <ta e="T202" id="Seg_3194" s="T201">старуха.[NOM]</ta>
            <ta e="T203" id="Seg_3195" s="T202">жалеть-CVB.SIM</ta>
            <ta e="T204" id="Seg_3196" s="T203">думать-PRS.[3SG]</ta>
            <ta e="T205" id="Seg_3197" s="T204">да</ta>
            <ta e="T206" id="Seg_3198" s="T205">беречься-CVB.SEQ</ta>
            <ta e="T207" id="Seg_3199" s="T206">верхняя.часть-3SG-INSTR</ta>
            <ta e="T208" id="Seg_3200" s="T207">крышка-3SG-ACC</ta>
            <ta e="T209" id="Seg_3201" s="T208">открывать-CVB.SEQ</ta>
            <ta e="T210" id="Seg_3202" s="T209">видеть-PRS.[3SG]</ta>
            <ta e="T211" id="Seg_3203" s="T210">видеть-PST2-3SG</ta>
            <ta e="T212" id="Seg_3204" s="T211">снег.[NOM]</ta>
            <ta e="T213" id="Seg_3205" s="T212">как</ta>
            <ta e="T214" id="Seg_3206" s="T213">белый</ta>
            <ta e="T215" id="Seg_3207" s="T214">очень</ta>
            <ta e="T216" id="Seg_3208" s="T215">ребенок.[NOM]</ta>
            <ta e="T217" id="Seg_3209" s="T216">лежать-PTCP.PRS</ta>
            <ta e="T218" id="Seg_3210" s="T217">быть-PST2.[3SG]</ta>
            <ta e="T219" id="Seg_3211" s="T218">этот-DAT/LOC</ta>
            <ta e="T220" id="Seg_3212" s="T219">ребенок-3SG.[NOM]</ta>
            <ta e="T221" id="Seg_3213" s="T220">умирать-PTCP.PRS</ta>
            <ta e="T222" id="Seg_3214" s="T221">голос-3SG.[NOM]</ta>
            <ta e="T223" id="Seg_3215" s="T222">говорить-CVB.SIM</ta>
            <ta e="T224" id="Seg_3216" s="T223">падать-PST2.[3SG]</ta>
            <ta e="T225" id="Seg_3217" s="T224">глаз-EP-1SG.[NOM]</ta>
            <ta e="T226" id="Seg_3218" s="T225">протыкать-CVB.SIM</ta>
            <ta e="T227" id="Seg_3219" s="T226">лопнуть-CVB.PURP</ta>
            <ta e="T228" id="Seg_3220" s="T227">делать-PST1-3SG</ta>
            <ta e="T229" id="Seg_3221" s="T228">говорить-CVB.SIM-говорить-CVB.SIM</ta>
            <ta e="T230" id="Seg_3222" s="T229">цепь-3SG.[NOM]</ta>
            <ta e="T231" id="Seg_3223" s="T230">наверх</ta>
            <ta e="T232" id="Seg_3224" s="T231">поднимать-CVB.SIM</ta>
            <ta e="T233" id="Seg_3225" s="T232">стоять-PST2.[3SG]</ta>
            <ta e="T234" id="Seg_3226" s="T233">колыбель-3SG-ACC</ta>
            <ta e="T235" id="Seg_3227" s="T234">ребенок.[NOM]</ta>
            <ta e="T236" id="Seg_3228" s="T235">голос-3SG.[NOM]</ta>
            <ta e="T237" id="Seg_3229" s="T236">слышаться-EP-PST2.[3SG]</ta>
            <ta e="T238" id="Seg_3230" s="T237">этот</ta>
            <ta e="T239" id="Seg_3231" s="T238">1SG.[NOM]</ta>
            <ta e="T240" id="Seg_3232" s="T239">старший.брат-EP-1SG.[NOM]</ta>
            <ta e="T241" id="Seg_3233" s="T240">Юрюнг.Айыы.[NOM]</ta>
            <ta e="T242" id="Seg_3234" s="T241">сын-3SG.[NOM]</ta>
            <ta e="T243" id="Seg_3235" s="T242">быть-PST1-3SG</ta>
            <ta e="T244" id="Seg_3236" s="T243">1SG-ACC</ta>
            <ta e="T245" id="Seg_3237" s="T244">старший.брат-EP-1SG.[NOM]</ta>
            <ta e="T246" id="Seg_3238" s="T245">послать-PST2-3SG</ta>
            <ta e="T247" id="Seg_3239" s="T246">три</ta>
            <ta e="T248" id="Seg_3240" s="T247">сутки-DAT/LOC</ta>
            <ta e="T249" id="Seg_3241" s="T248">пока</ta>
            <ta e="T250" id="Seg_3242" s="T249">1SG-ACC</ta>
            <ta e="T251" id="Seg_3243" s="T250">грязный</ta>
            <ta e="T252" id="Seg_3244" s="T251">глаз-PL-EP-INSTR</ta>
            <ta e="T253" id="Seg_3245" s="T252">видеть-CVB.SEQ</ta>
            <ta e="T254" id="Seg_3246" s="T253">кривой.[NOM]</ta>
            <ta e="T255" id="Seg_3247" s="T254">делать-FUT-3PL</ta>
            <ta e="T256" id="Seg_3248" s="T255">NEG-3SG</ta>
            <ta e="T257" id="Seg_3249" s="T256">говорить-CVB.SEQ</ta>
            <ta e="T258" id="Seg_3250" s="T257">сейчас</ta>
            <ta e="T259" id="Seg_3251" s="T258">возвращаться-FUT-1SG</ta>
            <ta e="T260" id="Seg_3252" s="T259">возвращаться-FUT.[3SG]</ta>
            <ta e="T261" id="Seg_3253" s="T260">NEG-1SG</ta>
            <ta e="T262" id="Seg_3254" s="T261">тот.[NOM]</ta>
            <ta e="T263" id="Seg_3255" s="T262">старший.брат-EP-1SG.[NOM]</ta>
            <ta e="T264" id="Seg_3256" s="T263">знать-FUT.[3SG]</ta>
            <ta e="T265" id="Seg_3257" s="T264">говорить-PST2.[3SG]</ta>
            <ta e="T266" id="Seg_3258" s="T265">тот</ta>
            <ta e="T267" id="Seg_3259" s="T266">время-DAT/LOC</ta>
            <ta e="T268" id="Seg_3260" s="T267">старик.[NOM]</ta>
            <ta e="T269" id="Seg_3261" s="T268">дикий.олень-PL-3SG-ACC</ta>
            <ta e="T270" id="Seg_3262" s="T269">сдирать.кожу-CVB.SEQ</ta>
            <ta e="T271" id="Seg_3263" s="T270">два</ta>
            <ta e="T272" id="Seg_3264" s="T271">задняя.нога-3SG-ACC</ta>
            <ta e="T273" id="Seg_3265" s="T272">плечо-DAT/LOC</ta>
            <ta e="T274" id="Seg_3266" s="T273">класть-CVB.SEQ</ta>
            <ta e="T275" id="Seg_3267" s="T274">приходить-CVB.SEQ</ta>
            <ta e="T276" id="Seg_3268" s="T275">идти-PST2.[3SG]</ta>
            <ta e="T277" id="Seg_3269" s="T276">этот-ACC</ta>
            <ta e="T278" id="Seg_3270" s="T277">старуха-3SG.[NOM]</ta>
            <ta e="T279" id="Seg_3271" s="T278">встречать-CVB.SEQ</ta>
            <ta e="T280" id="Seg_3272" s="T279">рассказывать-PRS.[3SG]</ta>
            <ta e="T281" id="Seg_3273" s="T280">старик.[NOM]</ta>
            <ta e="T282" id="Seg_3274" s="T281">верить-EP-NEG.CVB</ta>
            <ta e="T283" id="Seg_3275" s="T282">бегать-CVB.SEQ</ta>
            <ta e="T284" id="Seg_3276" s="T283">приходить-PRS.[3SG]</ta>
            <ta e="T285" id="Seg_3277" s="T284">NEG.EX-3SG-ACC</ta>
            <ta e="T286" id="Seg_3278" s="T285">видеть-CVB.SEQ</ta>
            <ta e="T287" id="Seg_3279" s="T286">после</ta>
            <ta e="T288" id="Seg_3280" s="T287">старуха-3SG-ACC</ta>
            <ta e="T289" id="Seg_3281" s="T288">бить-PRS.[3SG]</ta>
            <ta e="T290" id="Seg_3282" s="T289">стегно-3SG-INSTR</ta>
            <ta e="T291" id="Seg_3283" s="T290">старуха-3SG.[NOM]</ta>
            <ta e="T292" id="Seg_3284" s="T291">другой.из.двух</ta>
            <ta e="T293" id="Seg_3285" s="T292">стегно-3SG-ACC</ta>
            <ta e="T294" id="Seg_3286" s="T293">взять-PRS.[3SG]</ta>
            <ta e="T295" id="Seg_3287" s="T294">да</ta>
            <ta e="T296" id="Seg_3288" s="T295">бить-EP-RECP/COLL-CVB.SEQ</ta>
            <ta e="T297" id="Seg_3289" s="T296">сам</ta>
            <ta e="T298" id="Seg_3290" s="T297">сам-3PL-ACC</ta>
            <ta e="T299" id="Seg_3291" s="T298">убить-CVB.SEQ</ta>
            <ta e="T300" id="Seg_3292" s="T299">бросать-PRS-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_3293" s="T0">adv</ta>
            <ta e="T2" id="Seg_3294" s="T1">n-n&gt;adj</ta>
            <ta e="T3" id="Seg_3295" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_3296" s="T3">v-v:tense-v:pred.pn</ta>
            <ta e="T5" id="Seg_3297" s="T4">pers-pro:case</ta>
            <ta e="T6" id="Seg_3298" s="T5">cardnum</ta>
            <ta e="T7" id="Seg_3299" s="T6">n-n&gt;adj-n:(num)-n:(pred.pn)</ta>
            <ta e="T8" id="Seg_3300" s="T7">dempro</ta>
            <ta e="T9" id="Seg_3301" s="T8">n-n:(poss)-n:case</ta>
            <ta e="T10" id="Seg_3302" s="T9">adv</ta>
            <ta e="T11" id="Seg_3303" s="T10">n-n:poss-n:case</ta>
            <ta e="T12" id="Seg_3304" s="T11">v-v:tense-v:pred.pn</ta>
            <ta e="T13" id="Seg_3305" s="T12">conj</ta>
            <ta e="T14" id="Seg_3306" s="T13">v-v:cvb</ta>
            <ta e="T15" id="Seg_3307" s="T14">v-v:tense-v:pred.pn</ta>
            <ta e="T16" id="Seg_3308" s="T15">pers-pro:case</ta>
            <ta e="T17" id="Seg_3309" s="T16">pers-pro:case</ta>
            <ta e="T18" id="Seg_3310" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_3311" s="T18">v-v:tense-v:poss.pn</ta>
            <ta e="T20" id="Seg_3312" s="T19">v-v:tense-v:pred.pn</ta>
            <ta e="T21" id="Seg_3313" s="T20">adv</ta>
            <ta e="T22" id="Seg_3314" s="T21">n-n:poss-n:case</ta>
            <ta e="T23" id="Seg_3315" s="T22">v-v:cvb</ta>
            <ta e="T24" id="Seg_3316" s="T23">v-v:tense-v:pred.pn</ta>
            <ta e="T25" id="Seg_3317" s="T24">que-pro:case</ta>
            <ta e="T26" id="Seg_3318" s="T25">ptcl</ta>
            <ta e="T27" id="Seg_3319" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_3320" s="T27">pers-pro:case</ta>
            <ta e="T29" id="Seg_3321" s="T28">v-v:cvb</ta>
            <ta e="T30" id="Seg_3322" s="T29">v-v:(neg)-v:pred.pn</ta>
            <ta e="T31" id="Seg_3323" s="T30">adv</ta>
            <ta e="T32" id="Seg_3324" s="T31">adv</ta>
            <ta e="T33" id="Seg_3325" s="T32">v-v:cvb</ta>
            <ta e="T34" id="Seg_3326" s="T33">v-v:cvb-v:pred.pn</ta>
            <ta e="T35" id="Seg_3327" s="T34">n-n:poss-n:case</ta>
            <ta e="T36" id="Seg_3328" s="T35">v-v:ptcp-v:(case)</ta>
            <ta e="T37" id="Seg_3329" s="T36">post</ta>
            <ta e="T38" id="Seg_3330" s="T37">v-v:cvb</ta>
            <ta e="T39" id="Seg_3331" s="T38">v-v:tense-v:pred.pn</ta>
            <ta e="T40" id="Seg_3332" s="T39">n-n:(poss)-n:case</ta>
            <ta e="T41" id="Seg_3333" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_3334" s="T41">v-v:cvb</ta>
            <ta e="T43" id="Seg_3335" s="T42">v-v:tense-v:pred.pn</ta>
            <ta e="T44" id="Seg_3336" s="T43">dempro-pro:case</ta>
            <ta e="T45" id="Seg_3337" s="T44">n-n:poss-n:case</ta>
            <ta e="T46" id="Seg_3338" s="T45">n-n:poss-n:case</ta>
            <ta e="T47" id="Seg_3339" s="T46">v-v:cvb</ta>
            <ta e="T48" id="Seg_3340" s="T47">v-v:tense-v:pred.pn</ta>
            <ta e="T49" id="Seg_3341" s="T48">v-v:tense-v:poss.pn</ta>
            <ta e="T50" id="Seg_3342" s="T49">n-n:poss-n:case</ta>
            <ta e="T51" id="Seg_3343" s="T50">post</ta>
            <ta e="T52" id="Seg_3344" s="T51">cardnum</ta>
            <ta e="T53" id="Seg_3345" s="T52">n-n:case</ta>
            <ta e="T54" id="Seg_3346" s="T53">v-v:tense-v:pred.pn</ta>
            <ta e="T55" id="Seg_3347" s="T54">interj</ta>
            <ta e="T56" id="Seg_3348" s="T55">ptcl</ta>
            <ta e="T57" id="Seg_3349" s="T56">n-n:(poss)-n:case</ta>
            <ta e="T58" id="Seg_3350" s="T57">v-v:mood-v:(ins)-v:poss.pn</ta>
            <ta e="T59" id="Seg_3351" s="T58">v-v:cvb</ta>
            <ta e="T60" id="Seg_3352" s="T59">n-n&gt;adj</ta>
            <ta e="T61" id="Seg_3353" s="T60">n-n:case</ta>
            <ta e="T62" id="Seg_3354" s="T61">v-v:tense-v:pred.pn</ta>
            <ta e="T63" id="Seg_3355" s="T62">dempro-pro:case</ta>
            <ta e="T64" id="Seg_3356" s="T63">n-n:(poss)-n:case</ta>
            <ta e="T65" id="Seg_3357" s="T64">v-v:tense-v:pred.pn</ta>
            <ta e="T66" id="Seg_3358" s="T65">n-n:(poss)-n:case</ta>
            <ta e="T67" id="Seg_3359" s="T66">v-v:mood-v:poss.pn</ta>
            <ta e="T68" id="Seg_3360" s="T67">posspr</ta>
            <ta e="T69" id="Seg_3361" s="T68">dempro-pro:case</ta>
            <ta e="T70" id="Seg_3362" s="T69">pers-pro:case</ta>
            <ta e="T71" id="Seg_3363" s="T70">v-v:tense-v:pred.pn</ta>
            <ta e="T72" id="Seg_3364" s="T71">que</ta>
            <ta e="T73" id="Seg_3365" s="T72">n-n:(poss)-n:case</ta>
            <ta e="T74" id="Seg_3366" s="T73">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T75" id="Seg_3367" s="T74">v-v:(ins)-v:cvb</ta>
            <ta e="T76" id="Seg_3368" s="T75">v-v:ptcp</ta>
            <ta e="T77" id="Seg_3369" s="T76">v-v:tense-v:poss.pn</ta>
            <ta e="T78" id="Seg_3370" s="T77">v-v:tense-v:pred.pn</ta>
            <ta e="T79" id="Seg_3371" s="T78">dempro-pro:case</ta>
            <ta e="T80" id="Seg_3372" s="T79">conj</ta>
            <ta e="T81" id="Seg_3373" s="T80">v-v:mood-v:pred.pn</ta>
            <ta e="T82" id="Seg_3374" s="T81">n-n:poss-n:case</ta>
            <ta e="T83" id="Seg_3375" s="T82">v-v:tense-v:poss.pn</ta>
            <ta e="T84" id="Seg_3376" s="T83">pers-pro:case</ta>
            <ta e="T85" id="Seg_3377" s="T84">v-v:tense-v:poss.pn</ta>
            <ta e="T86" id="Seg_3378" s="T85">adv</ta>
            <ta e="T87" id="Seg_3379" s="T86">n-n:case</ta>
            <ta e="T88" id="Seg_3380" s="T87">adv</ta>
            <ta e="T89" id="Seg_3381" s="T88">n-n:case</ta>
            <ta e="T90" id="Seg_3382" s="T89">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T91" id="Seg_3383" s="T90">v-v:mood.pn</ta>
            <ta e="T92" id="Seg_3384" s="T91">cardnum</ta>
            <ta e="T93" id="Seg_3385" s="T92">n-n:case</ta>
            <ta e="T94" id="Seg_3386" s="T93">n-n:case</ta>
            <ta e="T95" id="Seg_3387" s="T94">v-v&gt;v-v:ptcp</ta>
            <ta e="T96" id="Seg_3388" s="T95">v-v:tense-v:poss.pn</ta>
            <ta e="T97" id="Seg_3389" s="T96">dempro-pro:case</ta>
            <ta e="T98" id="Seg_3390" s="T97">v-v:tense-v:pred.pn</ta>
            <ta e="T99" id="Seg_3391" s="T98">pers-pro:case</ta>
            <ta e="T100" id="Seg_3392" s="T99">n-n:case</ta>
            <ta e="T101" id="Seg_3393" s="T100">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T102" id="Seg_3394" s="T101">v-v:cvb</ta>
            <ta e="T103" id="Seg_3395" s="T102">dempro</ta>
            <ta e="T104" id="Seg_3396" s="T103">n-n:(num)-n:case</ta>
            <ta e="T105" id="Seg_3397" s="T104">n-n:case</ta>
            <ta e="T106" id="Seg_3398" s="T105">v-v:mood.pn</ta>
            <ta e="T107" id="Seg_3399" s="T106">dempro-pro:case</ta>
            <ta e="T108" id="Seg_3400" s="T107">v-v:cvb</ta>
            <ta e="T109" id="Seg_3401" s="T108">v-v:(ins)-v&gt;v-v:mood-v:temp.pn</ta>
            <ta e="T110" id="Seg_3402" s="T109">adj</ta>
            <ta e="T111" id="Seg_3403" s="T110">n-n:(ins)-n:case</ta>
            <ta e="T112" id="Seg_3404" s="T111">n-n:case</ta>
            <ta e="T113" id="Seg_3405" s="T112">adj</ta>
            <ta e="T114" id="Seg_3406" s="T113">n-n:case</ta>
            <ta e="T115" id="Seg_3407" s="T114">n-n:case</ta>
            <ta e="T116" id="Seg_3408" s="T115">v-v:tense-v:poss.pn</ta>
            <ta e="T117" id="Seg_3409" s="T116">dempro-pro:case</ta>
            <ta e="T118" id="Seg_3410" s="T117">n-n&gt;adj-n:case</ta>
            <ta e="T119" id="Seg_3411" s="T118">v-v:tense-v:poss.pn</ta>
            <ta e="T120" id="Seg_3412" s="T119">adv</ta>
            <ta e="T121" id="Seg_3413" s="T120">cardnum</ta>
            <ta e="T122" id="Seg_3414" s="T121">n-n:case</ta>
            <ta e="T123" id="Seg_3415" s="T122">post</ta>
            <ta e="T124" id="Seg_3416" s="T123">n-n:poss-n:case</ta>
            <ta e="T125" id="Seg_3417" s="T124">v-v:cvb</ta>
            <ta e="T126" id="Seg_3418" s="T125">v-v:ptcp</ta>
            <ta e="T127" id="Seg_3419" s="T126">v-v:mood-v:pred.pn</ta>
            <ta e="T128" id="Seg_3420" s="T127">dempro-pro:case</ta>
            <ta e="T129" id="Seg_3421" s="T128">v-v:cvb</ta>
            <ta e="T130" id="Seg_3422" s="T129">post</ta>
            <ta e="T131" id="Seg_3423" s="T130">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T132" id="Seg_3424" s="T131">n-n:(poss)-n:case</ta>
            <ta e="T133" id="Seg_3425" s="T132">n-n:(poss)-n:case</ta>
            <ta e="T134" id="Seg_3426" s="T133">dempro-pro:case</ta>
            <ta e="T135" id="Seg_3427" s="T134">v-v:tense-v:pred.pn</ta>
            <ta e="T136" id="Seg_3428" s="T135">n-n&gt;adj</ta>
            <ta e="T137" id="Seg_3429" s="T136">n-n:case</ta>
            <ta e="T138" id="Seg_3430" s="T137">dempro-pro:case</ta>
            <ta e="T139" id="Seg_3431" s="T138">v-v:cvb</ta>
            <ta e="T140" id="Seg_3432" s="T139">adv</ta>
            <ta e="T141" id="Seg_3433" s="T140">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T142" id="Seg_3434" s="T141">v-v:tense-v:poss.pn</ta>
            <ta e="T143" id="Seg_3435" s="T142">n-n:case</ta>
            <ta e="T144" id="Seg_3436" s="T143">n-n:(poss)-n:case-n-n:(poss)-n:case</ta>
            <ta e="T145" id="Seg_3437" s="T144">adj</ta>
            <ta e="T146" id="Seg_3438" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_3439" s="T146">n-n:case</ta>
            <ta e="T148" id="Seg_3440" s="T147">n-n:(poss)-n:case</ta>
            <ta e="T149" id="Seg_3441" s="T148">v-v:cvb</ta>
            <ta e="T150" id="Seg_3442" s="T149">n-n:(poss)-n:case</ta>
            <ta e="T151" id="Seg_3443" s="T150">n-n:case</ta>
            <ta e="T152" id="Seg_3444" s="T151">n-n:(poss)-n:case</ta>
            <ta e="T153" id="Seg_3445" s="T152">v-v:cvb</ta>
            <ta e="T154" id="Seg_3446" s="T153">n-n:case</ta>
            <ta e="T155" id="Seg_3447" s="T154">v-v:cvb</ta>
            <ta e="T156" id="Seg_3448" s="T155">v-v:tense-v:pred.pn</ta>
            <ta e="T157" id="Seg_3449" s="T156">adv</ta>
            <ta e="T158" id="Seg_3450" s="T157">n-n:case</ta>
            <ta e="T159" id="Seg_3451" s="T158">v-v:cvb</ta>
            <ta e="T160" id="Seg_3452" s="T159">v-v:tense-v:pred.pn</ta>
            <ta e="T161" id="Seg_3453" s="T160">v-v:tense-v:poss.pn</ta>
            <ta e="T162" id="Seg_3454" s="T161">cardnum</ta>
            <ta e="T163" id="Seg_3455" s="T162">n-n:case</ta>
            <ta e="T164" id="Seg_3456" s="T163">n-n:case</ta>
            <ta e="T165" id="Seg_3457" s="T164">v-v:cvb</ta>
            <ta e="T166" id="Seg_3458" s="T165">v-v:tense-v:pred.pn</ta>
            <ta e="T167" id="Seg_3459" s="T166">dempro-pro:case</ta>
            <ta e="T168" id="Seg_3460" s="T167">n-n:poss-n:case</ta>
            <ta e="T169" id="Seg_3461" s="T168">v-v:cvb</ta>
            <ta e="T170" id="Seg_3462" s="T169">n-n:case</ta>
            <ta e="T171" id="Seg_3463" s="T170">v-v:cvb</ta>
            <ta e="T172" id="Seg_3464" s="T171">cardnum</ta>
            <ta e="T173" id="Seg_3465" s="T172">n-n:case</ta>
            <ta e="T174" id="Seg_3466" s="T173">adj-n:poss-n:case</ta>
            <ta e="T175" id="Seg_3467" s="T174">v-v:cvb</ta>
            <ta e="T176" id="Seg_3468" s="T175">v-v:cvb</ta>
            <ta e="T177" id="Seg_3469" s="T176">v-v&gt;v-v:cvb</ta>
            <ta e="T178" id="Seg_3470" s="T177">v-v:tense-v:pred.pn</ta>
            <ta e="T179" id="Seg_3471" s="T178">dempro-pro:case</ta>
            <ta e="T180" id="Seg_3472" s="T179">v-v:cvb</ta>
            <ta e="T181" id="Seg_3473" s="T180">v-v:tense-v:pred.pn</ta>
            <ta e="T182" id="Seg_3474" s="T181">n-n:case</ta>
            <ta e="T183" id="Seg_3475" s="T182">v-v:cvb</ta>
            <ta e="T184" id="Seg_3476" s="T183">v-v:tense-v:pred.pn</ta>
            <ta e="T185" id="Seg_3477" s="T184">n-n:case</ta>
            <ta e="T186" id="Seg_3478" s="T185">adj-adj&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T187" id="Seg_3479" s="T186">n-n:case</ta>
            <ta e="T188" id="Seg_3480" s="T187">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T189" id="Seg_3481" s="T188">adj</ta>
            <ta e="T190" id="Seg_3482" s="T189">n-n:(ins)-n:case</ta>
            <ta e="T191" id="Seg_3483" s="T190">n-n:case</ta>
            <ta e="T192" id="Seg_3484" s="T191">v-v:cvb</ta>
            <ta e="T193" id="Seg_3485" s="T192">v-v:ptcp</ta>
            <ta e="T194" id="Seg_3486" s="T193">v-v:tense-v:pred.pn</ta>
            <ta e="T195" id="Seg_3487" s="T194">adj</ta>
            <ta e="T196" id="Seg_3488" s="T195">n-n:case</ta>
            <ta e="T197" id="Seg_3489" s="T196">n-n:poss-n:case</ta>
            <ta e="T198" id="Seg_3490" s="T197">n-n:case</ta>
            <ta e="T199" id="Seg_3491" s="T198">ptcl</ta>
            <ta e="T200" id="Seg_3492" s="T199">adv</ta>
            <ta e="T201" id="Seg_3493" s="T200">v-v:tense-v:pred.pn</ta>
            <ta e="T202" id="Seg_3494" s="T201">n-n:case</ta>
            <ta e="T203" id="Seg_3495" s="T202">v-v:cvb</ta>
            <ta e="T204" id="Seg_3496" s="T203">v-v:tense-v:pred.pn</ta>
            <ta e="T205" id="Seg_3497" s="T204">conj</ta>
            <ta e="T206" id="Seg_3498" s="T205">v-v:cvb</ta>
            <ta e="T207" id="Seg_3499" s="T206">n-n:poss-n:case</ta>
            <ta e="T208" id="Seg_3500" s="T207">n-n:poss-n:case</ta>
            <ta e="T209" id="Seg_3501" s="T208">v-v:cvb</ta>
            <ta e="T210" id="Seg_3502" s="T209">v-v:tense-v:pred.pn</ta>
            <ta e="T211" id="Seg_3503" s="T210">v-v:tense-v:poss.pn</ta>
            <ta e="T212" id="Seg_3504" s="T211">n-n:case</ta>
            <ta e="T213" id="Seg_3505" s="T212">post</ta>
            <ta e="T214" id="Seg_3506" s="T213">adj</ta>
            <ta e="T215" id="Seg_3507" s="T214">ptcl</ta>
            <ta e="T216" id="Seg_3508" s="T215">n-n:case</ta>
            <ta e="T217" id="Seg_3509" s="T216">v-v:ptcp</ta>
            <ta e="T218" id="Seg_3510" s="T217">v-v:tense-v:pred.pn</ta>
            <ta e="T219" id="Seg_3511" s="T218">dempro-pro:case</ta>
            <ta e="T220" id="Seg_3512" s="T219">n-n:(poss)-n:case</ta>
            <ta e="T221" id="Seg_3513" s="T220">v-v:ptcp</ta>
            <ta e="T222" id="Seg_3514" s="T221">n-n:(poss)-n:case</ta>
            <ta e="T223" id="Seg_3515" s="T222">v-v:cvb</ta>
            <ta e="T224" id="Seg_3516" s="T223">v-v:tense-v:pred.pn</ta>
            <ta e="T225" id="Seg_3517" s="T224">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T226" id="Seg_3518" s="T225">v-v:cvb</ta>
            <ta e="T227" id="Seg_3519" s="T226">v-v:cvb</ta>
            <ta e="T228" id="Seg_3520" s="T227">v-v:tense-v:poss.pn</ta>
            <ta e="T229" id="Seg_3521" s="T228">v-v:cvb-v-v:cvb</ta>
            <ta e="T230" id="Seg_3522" s="T229">n-n:(poss)-n:case</ta>
            <ta e="T231" id="Seg_3523" s="T230">adv</ta>
            <ta e="T232" id="Seg_3524" s="T231">v-v:cvb</ta>
            <ta e="T233" id="Seg_3525" s="T232">v-v:tense-v:pred.pn</ta>
            <ta e="T234" id="Seg_3526" s="T233">n-n:poss-n:case</ta>
            <ta e="T235" id="Seg_3527" s="T234">n-n:case</ta>
            <ta e="T236" id="Seg_3528" s="T235">n-n:(poss)-n:case</ta>
            <ta e="T237" id="Seg_3529" s="T236">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T238" id="Seg_3530" s="T237">dempro</ta>
            <ta e="T239" id="Seg_3531" s="T238">pers-pro:case</ta>
            <ta e="T240" id="Seg_3532" s="T239">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T241" id="Seg_3533" s="T240">propr-n:case</ta>
            <ta e="T242" id="Seg_3534" s="T241">n-n:(poss)-n:case</ta>
            <ta e="T243" id="Seg_3535" s="T242">v-v:tense-v:poss.pn</ta>
            <ta e="T244" id="Seg_3536" s="T243">pers-pro:case</ta>
            <ta e="T245" id="Seg_3537" s="T244">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T246" id="Seg_3538" s="T245">v-v:tense-v:poss.pn</ta>
            <ta e="T247" id="Seg_3539" s="T246">cardnum</ta>
            <ta e="T248" id="Seg_3540" s="T247">n-n:case</ta>
            <ta e="T249" id="Seg_3541" s="T248">post</ta>
            <ta e="T250" id="Seg_3542" s="T249">pers-pro:case</ta>
            <ta e="T251" id="Seg_3543" s="T250">adj</ta>
            <ta e="T252" id="Seg_3544" s="T251">n-n:(num)-n:(ins)-n:case</ta>
            <ta e="T253" id="Seg_3545" s="T252">v-v:cvb</ta>
            <ta e="T254" id="Seg_3546" s="T253">adj-n:case</ta>
            <ta e="T255" id="Seg_3547" s="T254">v-v:tense-v:poss.pn</ta>
            <ta e="T256" id="Seg_3548" s="T255">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T257" id="Seg_3549" s="T256">v-v:cvb</ta>
            <ta e="T258" id="Seg_3550" s="T257">adv</ta>
            <ta e="T259" id="Seg_3551" s="T258">v-v:tense-v:pred.pn</ta>
            <ta e="T260" id="Seg_3552" s="T259">v-v:tense-v:poss.pn</ta>
            <ta e="T261" id="Seg_3553" s="T260">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T262" id="Seg_3554" s="T261">dempro-pro:case</ta>
            <ta e="T263" id="Seg_3555" s="T262">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T264" id="Seg_3556" s="T263">v-v:tense-v:poss.pn</ta>
            <ta e="T265" id="Seg_3557" s="T264">v-v:tense-v:pred.pn</ta>
            <ta e="T266" id="Seg_3558" s="T265">dempro</ta>
            <ta e="T267" id="Seg_3559" s="T266">n-n:case</ta>
            <ta e="T268" id="Seg_3560" s="T267">n-n:case</ta>
            <ta e="T269" id="Seg_3561" s="T268">n-n:(num)-n:poss-n:case</ta>
            <ta e="T270" id="Seg_3562" s="T269">v-v:cvb</ta>
            <ta e="T271" id="Seg_3563" s="T270">cardnum</ta>
            <ta e="T272" id="Seg_3564" s="T271">n-n:poss-n:case</ta>
            <ta e="T273" id="Seg_3565" s="T272">n-n:case</ta>
            <ta e="T274" id="Seg_3566" s="T273">v-v:cvb</ta>
            <ta e="T275" id="Seg_3567" s="T274">v-v:cvb</ta>
            <ta e="T276" id="Seg_3568" s="T275">v-v:tense-v:pred.pn</ta>
            <ta e="T277" id="Seg_3569" s="T276">dempro-pro:case</ta>
            <ta e="T278" id="Seg_3570" s="T277">n-n:(poss)-n:case</ta>
            <ta e="T279" id="Seg_3571" s="T278">v-v:cvb</ta>
            <ta e="T280" id="Seg_3572" s="T279">v-v:tense-v:pred.pn</ta>
            <ta e="T281" id="Seg_3573" s="T280">n-n:case</ta>
            <ta e="T282" id="Seg_3574" s="T281">v-v:(ins)-v:cvb</ta>
            <ta e="T283" id="Seg_3575" s="T282">v-v:cvb</ta>
            <ta e="T284" id="Seg_3576" s="T283">v-v:tense-v:pred.pn</ta>
            <ta e="T285" id="Seg_3577" s="T284">ptcl-n:poss-n:case</ta>
            <ta e="T286" id="Seg_3578" s="T285">v-v:cvb</ta>
            <ta e="T287" id="Seg_3579" s="T286">post</ta>
            <ta e="T288" id="Seg_3580" s="T287">n-n:poss-n:case</ta>
            <ta e="T289" id="Seg_3581" s="T288">v-v:tense-v:pred.pn</ta>
            <ta e="T290" id="Seg_3582" s="T289">n-n:poss-n:case</ta>
            <ta e="T291" id="Seg_3583" s="T290">n-n:(poss)-n:case</ta>
            <ta e="T292" id="Seg_3584" s="T291">adj</ta>
            <ta e="T293" id="Seg_3585" s="T292">n-n:poss-n:case</ta>
            <ta e="T294" id="Seg_3586" s="T293">v-v:tense-v:pred.pn</ta>
            <ta e="T295" id="Seg_3587" s="T294">conj</ta>
            <ta e="T296" id="Seg_3588" s="T295">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T297" id="Seg_3589" s="T296">emphpro</ta>
            <ta e="T298" id="Seg_3590" s="T297">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T299" id="Seg_3591" s="T298">v-v:cvb</ta>
            <ta e="T300" id="Seg_3592" s="T299">v-v:tense-v:pred.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_3593" s="T0">adv</ta>
            <ta e="T2" id="Seg_3594" s="T1">adj</ta>
            <ta e="T3" id="Seg_3595" s="T2">n</ta>
            <ta e="T4" id="Seg_3596" s="T3">v</ta>
            <ta e="T5" id="Seg_3597" s="T4">pers</ta>
            <ta e="T6" id="Seg_3598" s="T5">cardnum</ta>
            <ta e="T7" id="Seg_3599" s="T6">adj</ta>
            <ta e="T8" id="Seg_3600" s="T7">dempro</ta>
            <ta e="T9" id="Seg_3601" s="T8">n</ta>
            <ta e="T10" id="Seg_3602" s="T9">adv</ta>
            <ta e="T11" id="Seg_3603" s="T10">n</ta>
            <ta e="T12" id="Seg_3604" s="T11">v</ta>
            <ta e="T13" id="Seg_3605" s="T12">conj</ta>
            <ta e="T14" id="Seg_3606" s="T13">v</ta>
            <ta e="T15" id="Seg_3607" s="T14">aux</ta>
            <ta e="T16" id="Seg_3608" s="T15">pers</ta>
            <ta e="T17" id="Seg_3609" s="T16">pers</ta>
            <ta e="T18" id="Seg_3610" s="T17">n</ta>
            <ta e="T19" id="Seg_3611" s="T18">v</ta>
            <ta e="T20" id="Seg_3612" s="T19">v</ta>
            <ta e="T21" id="Seg_3613" s="T20">adv</ta>
            <ta e="T22" id="Seg_3614" s="T21">n</ta>
            <ta e="T23" id="Seg_3615" s="T22">v</ta>
            <ta e="T24" id="Seg_3616" s="T23">v</ta>
            <ta e="T25" id="Seg_3617" s="T24">que</ta>
            <ta e="T26" id="Seg_3618" s="T25">ptcl</ta>
            <ta e="T27" id="Seg_3619" s="T26">n</ta>
            <ta e="T28" id="Seg_3620" s="T27">pers</ta>
            <ta e="T29" id="Seg_3621" s="T28">v</ta>
            <ta e="T30" id="Seg_3622" s="T29">v</ta>
            <ta e="T31" id="Seg_3623" s="T30">adv</ta>
            <ta e="T32" id="Seg_3624" s="T31">adv</ta>
            <ta e="T33" id="Seg_3625" s="T32">v</ta>
            <ta e="T34" id="Seg_3626" s="T33">aux</ta>
            <ta e="T35" id="Seg_3627" s="T34">n</ta>
            <ta e="T36" id="Seg_3628" s="T35">v</ta>
            <ta e="T37" id="Seg_3629" s="T36">post</ta>
            <ta e="T38" id="Seg_3630" s="T37">v</ta>
            <ta e="T39" id="Seg_3631" s="T38">aux</ta>
            <ta e="T40" id="Seg_3632" s="T39">n</ta>
            <ta e="T41" id="Seg_3633" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_3634" s="T41">v</ta>
            <ta e="T43" id="Seg_3635" s="T42">aux</ta>
            <ta e="T44" id="Seg_3636" s="T43">dempro</ta>
            <ta e="T45" id="Seg_3637" s="T44">n</ta>
            <ta e="T46" id="Seg_3638" s="T45">n</ta>
            <ta e="T47" id="Seg_3639" s="T46">v</ta>
            <ta e="T48" id="Seg_3640" s="T47">aux</ta>
            <ta e="T49" id="Seg_3641" s="T48">v</ta>
            <ta e="T50" id="Seg_3642" s="T49">n</ta>
            <ta e="T51" id="Seg_3643" s="T50">post</ta>
            <ta e="T52" id="Seg_3644" s="T51">cardnum</ta>
            <ta e="T53" id="Seg_3645" s="T52">n</ta>
            <ta e="T54" id="Seg_3646" s="T53">v</ta>
            <ta e="T55" id="Seg_3647" s="T54">interj</ta>
            <ta e="T56" id="Seg_3648" s="T55">ptcl</ta>
            <ta e="T57" id="Seg_3649" s="T56">n</ta>
            <ta e="T58" id="Seg_3650" s="T57">cop</ta>
            <ta e="T59" id="Seg_3651" s="T58">v</ta>
            <ta e="T60" id="Seg_3652" s="T59">adj</ta>
            <ta e="T61" id="Seg_3653" s="T60">n</ta>
            <ta e="T62" id="Seg_3654" s="T61">v</ta>
            <ta e="T63" id="Seg_3655" s="T62">dempro</ta>
            <ta e="T64" id="Seg_3656" s="T63">n</ta>
            <ta e="T65" id="Seg_3657" s="T64">v</ta>
            <ta e="T66" id="Seg_3658" s="T65">n</ta>
            <ta e="T67" id="Seg_3659" s="T66">cop</ta>
            <ta e="T68" id="Seg_3660" s="T67">posspr</ta>
            <ta e="T69" id="Seg_3661" s="T68">dempro</ta>
            <ta e="T70" id="Seg_3662" s="T69">pers</ta>
            <ta e="T71" id="Seg_3663" s="T70">v</ta>
            <ta e="T72" id="Seg_3664" s="T71">que</ta>
            <ta e="T73" id="Seg_3665" s="T72">n</ta>
            <ta e="T74" id="Seg_3666" s="T73">v</ta>
            <ta e="T75" id="Seg_3667" s="T74">v</ta>
            <ta e="T76" id="Seg_3668" s="T75">v</ta>
            <ta e="T77" id="Seg_3669" s="T76">aux</ta>
            <ta e="T78" id="Seg_3670" s="T77">v</ta>
            <ta e="T79" id="Seg_3671" s="T78">dempro</ta>
            <ta e="T80" id="Seg_3672" s="T79">conj</ta>
            <ta e="T81" id="Seg_3673" s="T80">cop</ta>
            <ta e="T82" id="Seg_3674" s="T81">n</ta>
            <ta e="T83" id="Seg_3675" s="T82">v</ta>
            <ta e="T84" id="Seg_3676" s="T83">pers</ta>
            <ta e="T85" id="Seg_3677" s="T84">v</ta>
            <ta e="T86" id="Seg_3678" s="T85">adv</ta>
            <ta e="T87" id="Seg_3679" s="T86">n</ta>
            <ta e="T88" id="Seg_3680" s="T87">adv</ta>
            <ta e="T89" id="Seg_3681" s="T88">n</ta>
            <ta e="T90" id="Seg_3682" s="T89">v</ta>
            <ta e="T91" id="Seg_3683" s="T90">v</ta>
            <ta e="T92" id="Seg_3684" s="T91">cardnum</ta>
            <ta e="T93" id="Seg_3685" s="T92">n</ta>
            <ta e="T94" id="Seg_3686" s="T93">n</ta>
            <ta e="T95" id="Seg_3687" s="T94">v</ta>
            <ta e="T96" id="Seg_3688" s="T95">aux</ta>
            <ta e="T97" id="Seg_3689" s="T96">dempro</ta>
            <ta e="T98" id="Seg_3690" s="T97">v</ta>
            <ta e="T99" id="Seg_3691" s="T98">pers</ta>
            <ta e="T100" id="Seg_3692" s="T99">n</ta>
            <ta e="T101" id="Seg_3693" s="T100">v</ta>
            <ta e="T102" id="Seg_3694" s="T101">v</ta>
            <ta e="T103" id="Seg_3695" s="T102">dempro</ta>
            <ta e="T104" id="Seg_3696" s="T103">n</ta>
            <ta e="T105" id="Seg_3697" s="T104">n</ta>
            <ta e="T106" id="Seg_3698" s="T105">v</ta>
            <ta e="T107" id="Seg_3699" s="T106">dempro</ta>
            <ta e="T108" id="Seg_3700" s="T107">v</ta>
            <ta e="T109" id="Seg_3701" s="T108">v</ta>
            <ta e="T110" id="Seg_3702" s="T109">adj</ta>
            <ta e="T111" id="Seg_3703" s="T110">n</ta>
            <ta e="T112" id="Seg_3704" s="T111">n</ta>
            <ta e="T113" id="Seg_3705" s="T112">adj</ta>
            <ta e="T114" id="Seg_3706" s="T113">n</ta>
            <ta e="T115" id="Seg_3707" s="T114">n</ta>
            <ta e="T116" id="Seg_3708" s="T115">v</ta>
            <ta e="T117" id="Seg_3709" s="T116">dempro</ta>
            <ta e="T118" id="Seg_3710" s="T117">adj</ta>
            <ta e="T119" id="Seg_3711" s="T118">cop</ta>
            <ta e="T120" id="Seg_3712" s="T119">adv</ta>
            <ta e="T121" id="Seg_3713" s="T120">cardnum</ta>
            <ta e="T122" id="Seg_3714" s="T121">n</ta>
            <ta e="T123" id="Seg_3715" s="T122">post</ta>
            <ta e="T124" id="Seg_3716" s="T123">n</ta>
            <ta e="T125" id="Seg_3717" s="T124">v</ta>
            <ta e="T126" id="Seg_3718" s="T125">v</ta>
            <ta e="T127" id="Seg_3719" s="T126">aux</ta>
            <ta e="T128" id="Seg_3720" s="T127">dempro</ta>
            <ta e="T129" id="Seg_3721" s="T128">v</ta>
            <ta e="T130" id="Seg_3722" s="T129">post</ta>
            <ta e="T131" id="Seg_3723" s="T130">v</ta>
            <ta e="T132" id="Seg_3724" s="T131">n</ta>
            <ta e="T133" id="Seg_3725" s="T132">n</ta>
            <ta e="T134" id="Seg_3726" s="T133">dempro</ta>
            <ta e="T135" id="Seg_3727" s="T134">v</ta>
            <ta e="T136" id="Seg_3728" s="T135">adj</ta>
            <ta e="T137" id="Seg_3729" s="T136">n</ta>
            <ta e="T138" id="Seg_3730" s="T137">dempro</ta>
            <ta e="T139" id="Seg_3731" s="T138">v</ta>
            <ta e="T140" id="Seg_3732" s="T139">adv</ta>
            <ta e="T141" id="Seg_3733" s="T140">v</ta>
            <ta e="T142" id="Seg_3734" s="T141">v</ta>
            <ta e="T143" id="Seg_3735" s="T142">n</ta>
            <ta e="T144" id="Seg_3736" s="T143">n</ta>
            <ta e="T145" id="Seg_3737" s="T144">adj</ta>
            <ta e="T146" id="Seg_3738" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_3739" s="T146">n</ta>
            <ta e="T148" id="Seg_3740" s="T147">n</ta>
            <ta e="T149" id="Seg_3741" s="T148">cop</ta>
            <ta e="T150" id="Seg_3742" s="T149">n</ta>
            <ta e="T151" id="Seg_3743" s="T150">n</ta>
            <ta e="T152" id="Seg_3744" s="T151">n</ta>
            <ta e="T153" id="Seg_3745" s="T152">cop</ta>
            <ta e="T154" id="Seg_3746" s="T153">n</ta>
            <ta e="T155" id="Seg_3747" s="T154">v</ta>
            <ta e="T156" id="Seg_3748" s="T155">aux</ta>
            <ta e="T157" id="Seg_3749" s="T156">adv</ta>
            <ta e="T158" id="Seg_3750" s="T157">n</ta>
            <ta e="T159" id="Seg_3751" s="T158">v</ta>
            <ta e="T160" id="Seg_3752" s="T159">v</ta>
            <ta e="T161" id="Seg_3753" s="T160">v</ta>
            <ta e="T162" id="Seg_3754" s="T161">cardnum</ta>
            <ta e="T163" id="Seg_3755" s="T162">n</ta>
            <ta e="T164" id="Seg_3756" s="T163">n</ta>
            <ta e="T165" id="Seg_3757" s="T164">v</ta>
            <ta e="T166" id="Seg_3758" s="T165">aux</ta>
            <ta e="T167" id="Seg_3759" s="T166">dempro</ta>
            <ta e="T168" id="Seg_3760" s="T167">n</ta>
            <ta e="T169" id="Seg_3761" s="T168">v</ta>
            <ta e="T170" id="Seg_3762" s="T169">n</ta>
            <ta e="T171" id="Seg_3763" s="T170">v</ta>
            <ta e="T172" id="Seg_3764" s="T171">cardnum</ta>
            <ta e="T173" id="Seg_3765" s="T172">n</ta>
            <ta e="T174" id="Seg_3766" s="T173">adj</ta>
            <ta e="T175" id="Seg_3767" s="T174">v</ta>
            <ta e="T176" id="Seg_3768" s="T175">v</ta>
            <ta e="T177" id="Seg_3769" s="T176">v</ta>
            <ta e="T178" id="Seg_3770" s="T177">aux</ta>
            <ta e="T179" id="Seg_3771" s="T178">dempro</ta>
            <ta e="T180" id="Seg_3772" s="T179">v</ta>
            <ta e="T181" id="Seg_3773" s="T180">aux</ta>
            <ta e="T182" id="Seg_3774" s="T181">n</ta>
            <ta e="T183" id="Seg_3775" s="T182">v</ta>
            <ta e="T184" id="Seg_3776" s="T183">aux</ta>
            <ta e="T185" id="Seg_3777" s="T184">n</ta>
            <ta e="T186" id="Seg_3778" s="T185">v</ta>
            <ta e="T187" id="Seg_3779" s="T186">n</ta>
            <ta e="T188" id="Seg_3780" s="T187">v</ta>
            <ta e="T189" id="Seg_3781" s="T188">adj</ta>
            <ta e="T190" id="Seg_3782" s="T189">n</ta>
            <ta e="T191" id="Seg_3783" s="T190">n</ta>
            <ta e="T192" id="Seg_3784" s="T191">v</ta>
            <ta e="T193" id="Seg_3785" s="T192">aux</ta>
            <ta e="T194" id="Seg_3786" s="T193">aux</ta>
            <ta e="T195" id="Seg_3787" s="T194">adj</ta>
            <ta e="T196" id="Seg_3788" s="T195">n</ta>
            <ta e="T197" id="Seg_3789" s="T196">n</ta>
            <ta e="T198" id="Seg_3790" s="T197">n</ta>
            <ta e="T199" id="Seg_3791" s="T198">ptcl</ta>
            <ta e="T200" id="Seg_3792" s="T199">adv</ta>
            <ta e="T201" id="Seg_3793" s="T200">v</ta>
            <ta e="T202" id="Seg_3794" s="T201">n</ta>
            <ta e="T203" id="Seg_3795" s="T202">v</ta>
            <ta e="T204" id="Seg_3796" s="T203">v</ta>
            <ta e="T205" id="Seg_3797" s="T204">conj</ta>
            <ta e="T206" id="Seg_3798" s="T205">v</ta>
            <ta e="T207" id="Seg_3799" s="T206">n</ta>
            <ta e="T208" id="Seg_3800" s="T207">n</ta>
            <ta e="T209" id="Seg_3801" s="T208">v</ta>
            <ta e="T210" id="Seg_3802" s="T209">v</ta>
            <ta e="T211" id="Seg_3803" s="T210">v</ta>
            <ta e="T212" id="Seg_3804" s="T211">n</ta>
            <ta e="T213" id="Seg_3805" s="T212">post</ta>
            <ta e="T214" id="Seg_3806" s="T213">adj</ta>
            <ta e="T215" id="Seg_3807" s="T214">ptcl</ta>
            <ta e="T216" id="Seg_3808" s="T215">n</ta>
            <ta e="T217" id="Seg_3809" s="T216">v</ta>
            <ta e="T218" id="Seg_3810" s="T217">aux</ta>
            <ta e="T219" id="Seg_3811" s="T218">dempro</ta>
            <ta e="T220" id="Seg_3812" s="T219">n</ta>
            <ta e="T221" id="Seg_3813" s="T220">v</ta>
            <ta e="T222" id="Seg_3814" s="T221">n</ta>
            <ta e="T223" id="Seg_3815" s="T222">v</ta>
            <ta e="T224" id="Seg_3816" s="T223">aux</ta>
            <ta e="T225" id="Seg_3817" s="T224">n</ta>
            <ta e="T226" id="Seg_3818" s="T225">v</ta>
            <ta e="T227" id="Seg_3819" s="T226">v</ta>
            <ta e="T228" id="Seg_3820" s="T227">v</ta>
            <ta e="T229" id="Seg_3821" s="T228">v</ta>
            <ta e="T230" id="Seg_3822" s="T229">n</ta>
            <ta e="T231" id="Seg_3823" s="T230">adv</ta>
            <ta e="T232" id="Seg_3824" s="T231">v</ta>
            <ta e="T233" id="Seg_3825" s="T232">aux</ta>
            <ta e="T234" id="Seg_3826" s="T233">n</ta>
            <ta e="T235" id="Seg_3827" s="T234">n</ta>
            <ta e="T236" id="Seg_3828" s="T235">n</ta>
            <ta e="T237" id="Seg_3829" s="T236">v</ta>
            <ta e="T238" id="Seg_3830" s="T237">dempro</ta>
            <ta e="T239" id="Seg_3831" s="T238">pers</ta>
            <ta e="T240" id="Seg_3832" s="T239">n</ta>
            <ta e="T241" id="Seg_3833" s="T240">propr</ta>
            <ta e="T242" id="Seg_3834" s="T241">n</ta>
            <ta e="T243" id="Seg_3835" s="T242">cop</ta>
            <ta e="T244" id="Seg_3836" s="T243">pers</ta>
            <ta e="T245" id="Seg_3837" s="T244">n</ta>
            <ta e="T246" id="Seg_3838" s="T245">v</ta>
            <ta e="T247" id="Seg_3839" s="T246">cardnum</ta>
            <ta e="T248" id="Seg_3840" s="T247">n</ta>
            <ta e="T249" id="Seg_3841" s="T248">post</ta>
            <ta e="T250" id="Seg_3842" s="T249">pers</ta>
            <ta e="T251" id="Seg_3843" s="T250">adj</ta>
            <ta e="T252" id="Seg_3844" s="T251">n</ta>
            <ta e="T253" id="Seg_3845" s="T252">v</ta>
            <ta e="T254" id="Seg_3846" s="T253">adj</ta>
            <ta e="T255" id="Seg_3847" s="T254">v</ta>
            <ta e="T256" id="Seg_3848" s="T255">ptcl</ta>
            <ta e="T257" id="Seg_3849" s="T256">v</ta>
            <ta e="T258" id="Seg_3850" s="T257">adv</ta>
            <ta e="T259" id="Seg_3851" s="T258">v</ta>
            <ta e="T260" id="Seg_3852" s="T259">v</ta>
            <ta e="T261" id="Seg_3853" s="T260">ptcl</ta>
            <ta e="T262" id="Seg_3854" s="T261">dempro</ta>
            <ta e="T263" id="Seg_3855" s="T262">n</ta>
            <ta e="T264" id="Seg_3856" s="T263">v</ta>
            <ta e="T265" id="Seg_3857" s="T264">v</ta>
            <ta e="T266" id="Seg_3858" s="T265">dempro</ta>
            <ta e="T267" id="Seg_3859" s="T266">n</ta>
            <ta e="T268" id="Seg_3860" s="T267">n</ta>
            <ta e="T269" id="Seg_3861" s="T268">adj</ta>
            <ta e="T270" id="Seg_3862" s="T269">v</ta>
            <ta e="T271" id="Seg_3863" s="T270">cardnum</ta>
            <ta e="T272" id="Seg_3864" s="T271">n</ta>
            <ta e="T273" id="Seg_3865" s="T272">n</ta>
            <ta e="T274" id="Seg_3866" s="T273">v</ta>
            <ta e="T275" id="Seg_3867" s="T274">v</ta>
            <ta e="T276" id="Seg_3868" s="T275">aux</ta>
            <ta e="T277" id="Seg_3869" s="T276">dempro</ta>
            <ta e="T278" id="Seg_3870" s="T277">n</ta>
            <ta e="T279" id="Seg_3871" s="T278">v</ta>
            <ta e="T280" id="Seg_3872" s="T279">v</ta>
            <ta e="T281" id="Seg_3873" s="T280">n</ta>
            <ta e="T282" id="Seg_3874" s="T281">v</ta>
            <ta e="T283" id="Seg_3875" s="T282">v</ta>
            <ta e="T284" id="Seg_3876" s="T283">aux</ta>
            <ta e="T285" id="Seg_3877" s="T284">ptcl</ta>
            <ta e="T286" id="Seg_3878" s="T285">v</ta>
            <ta e="T287" id="Seg_3879" s="T286">post</ta>
            <ta e="T288" id="Seg_3880" s="T287">n</ta>
            <ta e="T289" id="Seg_3881" s="T288">v</ta>
            <ta e="T290" id="Seg_3882" s="T289">n</ta>
            <ta e="T291" id="Seg_3883" s="T290">n</ta>
            <ta e="T292" id="Seg_3884" s="T291">adj</ta>
            <ta e="T293" id="Seg_3885" s="T292">n</ta>
            <ta e="T294" id="Seg_3886" s="T293">v</ta>
            <ta e="T295" id="Seg_3887" s="T294">conj</ta>
            <ta e="T296" id="Seg_3888" s="T295">v</ta>
            <ta e="T297" id="Seg_3889" s="T296">emphpro</ta>
            <ta e="T298" id="Seg_3890" s="T297">emphpro</ta>
            <ta e="T299" id="Seg_3891" s="T298">v</ta>
            <ta e="T300" id="Seg_3892" s="T299">aux</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T111" id="Seg_3893" s="T110">RUS:cult</ta>
            <ta e="T190" id="Seg_3894" s="T189">RUS:cult</ta>
            <ta e="T230" id="Seg_3895" s="T229">RUS:cult</ta>
            <ta e="T267" id="Seg_3896" s="T266">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T111" id="Seg_3897" s="T110">Csub Vsub</ta>
            <ta e="T190" id="Seg_3898" s="T189">Csub Vsub</ta>
            <ta e="T230" id="Seg_3899" s="T229">Csub Vsub</ta>
            <ta e="T267" id="Seg_3900" s="T266">fortition medVins Vsub Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T111" id="Seg_3901" s="T110">dir:infl</ta>
            <ta e="T190" id="Seg_3902" s="T189">dir:infl</ta>
            <ta e="T230" id="Seg_3903" s="T229">dir:infl</ta>
            <ta e="T267" id="Seg_3904" s="T266">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_3905" s="T0">Long ago an old man and an old woman lived.</ta>
            <ta e="T7" id="Seg_3906" s="T4">They had a daughter.</ta>
            <ta e="T15" id="Seg_3907" s="T7">In the evening as soon as they extinguished their lamp, that daughter began to chat:</ta>
            <ta e="T20" id="Seg_3908" s="T15">"I will marry you", she said.</ta>
            <ta e="T30" id="Seg_3909" s="T20">By day they asked their daughter in vain, they couldn't take a word out of her.</ta>
            <ta e="T39" id="Seg_3910" s="T30">Once in the evening as they lay down, they covered their lamp as if they had extinguished it.</ta>
            <ta e="T43" id="Seg_3911" s="T39">The daughter began again to chat.</ta>
            <ta e="T48" id="Seg_3912" s="T43">There they threw the lid off the lamp.</ta>
            <ta e="T54" id="Seg_3913" s="T48">They saw one human lying next to their daughter.</ta>
            <ta e="T62" id="Seg_3914" s="T54">"Well, so you are our son-in-law", the old man and the old woman said.</ta>
            <ta e="T65" id="Seg_3915" s="T62">That human said on that:</ta>
            <ta e="T78" id="Seg_3916" s="T65">"You are lucky to have seen me, otherwise you wouldn't know where your daughter has gone to", he said.</ta>
            <ta e="T83" id="Seg_3917" s="T78">"Despite of this I will carry your daughter away".</ta>
            <ta e="T96" id="Seg_3918" s="T83">After I go, tomorrow in the morning the old man should go out, seven wild reindeers will be swimming across the river.</ta>
            <ta e="T101" id="Seg_3919" s="T96">So you will know that I've brought you luck.</ta>
            <ta e="T106" id="Seg_3920" s="T101">The old man should attempt to kill those wild reindeers.</ta>
            <ta e="T116" id="Seg_3921" s="T106">When he finishes skinning them, a child in an iron cradle on an iron chain will come down from the sky.</ta>
            <ta e="T119" id="Seg_3922" s="T116">It will be covered with a lid.</ta>
            <ta e="T127" id="Seg_3923" s="T119">During three whole days and nights you shouldn't open the lid and do not look into it".</ta>
            <ta e="T132" id="Seg_3924" s="T127">As he said this, that human went out.</ta>
            <ta e="T135" id="Seg_3925" s="T132">Their daughter came along with him.</ta>
            <ta e="T141" id="Seg_3926" s="T135">The old man and the old woman accompanied them outside.</ta>
            <ta e="T156" id="Seg_3927" s="T141">They saw as their son-in-law became a huge wild buck and their daughter became a two-year-old female reindeer, they swam across the river.</ta>
            <ta e="T160" id="Seg_3928" s="T156">In the morning the old man went outside to pee.</ta>
            <ta e="T166" id="Seg_3929" s="T160">He saw seven wild reindeers swimming across the river.</ta>
            <ta e="T178" id="Seg_3930" s="T166">He took a spear, went on a small boat and stabbed to death all seven wild reindeers.</ta>
            <ta e="T181" id="Seg_3931" s="T178">He began to skin them.</ta>
            <ta e="T184" id="Seg_3932" s="T181">The old oman was sleeping.</ta>
            <ta e="T196" id="Seg_3933" s="T184">She woke up as the night came, an iron cradle on an iron chain had come down from the sky.</ta>
            <ta e="T201" id="Seg_3934" s="T196">Apparently there was a child inside, it was crying.</ta>
            <ta e="T210" id="Seg_3935" s="T201">The old woman felt sorry for it, she opened carefully the lid and looked inside.</ta>
            <ta e="T218" id="Seg_3936" s="T210">She saw a child as white as snow lying inside.</ta>
            <ta e="T224" id="Seg_3937" s="T218">There the child suddenly began to speak with a dying voice:</ta>
            <ta e="T234" id="Seg_3938" s="T224">"My eyes will burst now!", after it said this the chain started to raise up the cradle. </ta>
            <ta e="T237" id="Seg_3939" s="T234">The child's voice was heard:</ta>
            <ta e="T257" id="Seg_3940" s="T237">"My elder brother is Ürüng Ajyy's son, my brother has sent me, I was supposed to not be seen for three days for not to become buckled from evil eyes.</ta>
            <ta e="T265" id="Seg_3941" s="T257">Now whether I come back to you or not, my elder brother will know that", it said.</ta>
            <ta e="T276" id="Seg_3942" s="T265">At that time the old man, after having skinned the wild reindeers, was carring two reindeer hind legs on his shoulders.</ta>
            <ta e="T280" id="Seg_3943" s="T276">The old woman met him and told everything.</ta>
            <ta e="T284" id="Seg_3944" s="T280">The old man didn't believe her and ran into the yurt.</ta>
            <ta e="T290" id="Seg_3945" s="T284">As he saw that the child was not there, he hit the old woman with a thigh.</ta>
            <ta e="T300" id="Seg_3946" s="T290">The old woman toook another thigh and so they hit each other to death.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_3947" s="T0">Vor langer Zeit lebten ein alter Mann und eine alte Frau.</ta>
            <ta e="T7" id="Seg_3948" s="T4">Sie hatten eine Tochter.</ta>
            <ta e="T15" id="Seg_3949" s="T7">Diese Tochter fing, als sie abends gerade ihre Lampe ausgemacht hatten, an sich zu unterhalten:</ta>
            <ta e="T20" id="Seg_3950" s="T15">"Ich werde dich heiraten", sagte sie.</ta>
            <ta e="T30" id="Seg_3951" s="T20">Am Tag, als sie ihre Tochter fragten, konnten sie kein Wort aus ihr herausbekommen.</ta>
            <ta e="T39" id="Seg_3952" s="T30">Einmal am Abend, als sie sich hinlegten, bedeckten sie ihre Lampe, als ob sie sie ausgemacht hätten.</ta>
            <ta e="T43" id="Seg_3953" s="T39">Die Tochter fing wieder an, sich zu unterhalten.</ta>
            <ta e="T48" id="Seg_3954" s="T43">Da rissen sie die Abdeckung von der Lampe.</ta>
            <ta e="T54" id="Seg_3955" s="T48">Sie sahen, dass mit ihrer Tochter einer Mensch dort lag.</ta>
            <ta e="T62" id="Seg_3956" s="T54">"Nun, du bist also unser Schwiegersohn", sagten der alte Mann und die alte Frau.</ta>
            <ta e="T65" id="Seg_3957" s="T62">Daraufhin sagte dieser Mensch:</ta>
            <ta e="T78" id="Seg_3958" s="T65">"Es ist wohl euer Glück, dass ihr mich gesehen habt, sonst wüsstet ihr nicht, wo euer Tochter bleibt", sagte er.</ta>
            <ta e="T83" id="Seg_3959" s="T78">"Auf jeden Fall nehme ich eure Tochter mit.</ta>
            <ta e="T96" id="Seg_3960" s="T83">Ich werde gehen, morgen früh soll der alte Mann hinausgehen, sieben wilde Rentier werden durch den Fluss schwimmen.</ta>
            <ta e="T101" id="Seg_3961" s="T96">Daran werdet ihr erkennen, dass ich Glück gebracht habe.</ta>
            <ta e="T106" id="Seg_3962" s="T101">Der alte Mann soll versuchen diese wilden Rentiere zu töten.</ta>
            <ta e="T116" id="Seg_3963" s="T106">Wenn er fertig damit ist, das Fell abzuziehen, dann wird an einer eisernen Kette in einer eisernen Wiege ein Kind vom Himmel hinunterkommen.</ta>
            <ta e="T119" id="Seg_3964" s="T116">Sie wird verschlossen sein.</ta>
            <ta e="T127" id="Seg_3965" s="T119">Drei Tage lang öffnet weder den Deckel noch schaut hinein."</ta>
            <ta e="T132" id="Seg_3966" s="T127">Als er das gesagt hatte, ging dieser Mensch hinaus.</ta>
            <ta e="T135" id="Seg_3967" s="T132">Ihre Tochter ging mit ihm mit.</ta>
            <ta e="T141" id="Seg_3968" s="T135">Der alte Mann und die alte Frau begleiteten sie nach draußen.</ta>
            <ta e="T156" id="Seg_3969" s="T141">Sie sahen, dass der Schwiegersohn sich in ein riesiges Rentiermännchen verwandelte, die Tochter in ein zweijähriges Rentier, dass sie über den Fluss fortschwammen.</ta>
            <ta e="T160" id="Seg_3970" s="T156">Am Morgen ging der alte Mann zum Pinkeln nach draußen.</ta>
            <ta e="T166" id="Seg_3971" s="T160">Er sah, dass sieben wilde Rentiere durch den Fluss schwammen.</ta>
            <ta e="T178" id="Seg_3972" s="T166">Da nahm er ein Speer, fuhr mit einem kleinen Boot und erstach alle sieben wilden Rentiere.</ta>
            <ta e="T181" id="Seg_3973" s="T178">Er fing an ihnen das Fell abzuziehen.</ta>
            <ta e="T184" id="Seg_3974" s="T181">Die alte Frau schlief.</ta>
            <ta e="T196" id="Seg_3975" s="T184">Als es Nacht war und sie aufwachte, hing offenbar an eisernen Kette eine eiserne Wiege vom Himmel herab.</ta>
            <ta e="T201" id="Seg_3976" s="T196">Da drinnen war offenbar ein Kind, das weinte.</ta>
            <ta e="T210" id="Seg_3977" s="T201">Der alten Frau tat es leid und so öffnete sie vorsichtig den Deckel und schaute hinein.</ta>
            <ta e="T218" id="Seg_3978" s="T210">Sie sah, dass dort ein Kind, weiß wie Schnee, lag.</ta>
            <ta e="T224" id="Seg_3979" s="T218">Da sagte das Kind mit einem Todesschrei:</ta>
            <ta e="T234" id="Seg_3980" s="T224">"Jetzt platzen meine Augen!", schrie es und die Kette fing an, die Wiege hochzuheben.</ta>
            <ta e="T237" id="Seg_3981" s="T234">Die Stimme des Kindes war zu hören:</ta>
            <ta e="T257" id="Seg_3982" s="T237">"Mein älterer Bruder ist ein Sohn von Ürüng Ajyy, mein Bruder hat mich geschickt, drei Tage lang würde ich nicht angeschaut werden, um von den hässlichen Blicken nicht krumm zu werden, sagte er.</ta>
            <ta e="T265" id="Seg_3983" s="T257">Ob ich jetzt zurückkehre oder nicht, dass weiß mein Bruder", sagte es.</ta>
            <ta e="T276" id="Seg_3984" s="T265">Zu dieser Zeit hatte der alte Mann allen wilden Rentieren das Fell abgezogen und ging nun mit zwei Hinterbeinen auf den Schultern.</ta>
            <ta e="T280" id="Seg_3985" s="T276">Da traf die alte Frau ihn und erzählte.</ta>
            <ta e="T284" id="Seg_3986" s="T280">Der alte Mann glaubte es nicht und lief [hinein].</ta>
            <ta e="T290" id="Seg_3987" s="T284">Als er sah, dass kein Kind da war, schlug er die alte Frau mit einem Oberschenkel.</ta>
            <ta e="T300" id="Seg_3988" s="T290">Die alte Frau nahm den anderen Oberschenkel und so schlagend erschlugen sie einander.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_3989" s="T0">В старину старик со старухой жили.</ta>
            <ta e="T7" id="Seg_3990" s="T4">У них была одна дочь.</ta>
            <ta e="T15" id="Seg_3991" s="T7">Эта дочь, как только вечером гасили свой светильник, начинала разговаривать:</ta>
            <ta e="T20" id="Seg_3992" s="T15">— Я за тебя замуж пойду, — говорила.</ta>
            <ta e="T30" id="Seg_3993" s="T20">Днем, как ни спрашивали у дочки, ничего от нее не могли добиться.</ta>
            <ta e="T39" id="Seg_3994" s="T30">Однажды вечером, когда улеглись, старики прикрыли свой светильник, как будто погасив его.</ta>
            <ta e="T43" id="Seg_3995" s="T39">Дочь опять начала разговаривать.</ta>
            <ta e="T48" id="Seg_3996" s="T43">Тогда сбросили со светильника крышку.</ta>
            <ta e="T54" id="Seg_3997" s="T48">Увидели: с их дочерью один человек лежит.</ta>
            <ta e="T62" id="Seg_3998" s="T54">— Ну ты, значит, наш зятек, — сказали старик со старухой.</ta>
            <ta e="T65" id="Seg_3999" s="T62">На это человек ответил:</ta>
            <ta e="T78" id="Seg_4000" s="T65">— Счастье, значит, ваше, что меня увидели, иначе не знали бы, куда девалась ваша дочь, — сказал.</ta>
            <ta e="T83" id="Seg_4001" s="T78">— Но все равно уведу вашу дочь.</ta>
            <ta e="T96" id="Seg_4002" s="T83">После того как уйду, пусть завтра рано, с восходом солнца, выйдет старик. ‎‎Семь диких оленей будут переплывать реку.</ta>
            <ta e="T101" id="Seg_4003" s="T96">По этому узнаете, что я принес вам счастье.</ta>
            <ta e="T106" id="Seg_4004" s="T101">Пусть старик постарается убить тех диких оленей.</ta>
            <ta e="T116" id="Seg_4005" s="T106">Когда закончит снимать с них шкуры, на железной цепи с неба спустится железная колыбель с ребенком.</ta>
            <ta e="T119" id="Seg_4006" s="T116">Она будет закрыта.</ta>
            <ta e="T127" id="Seg_4007" s="T119">В течение трех дней ни за что не открывайте и не смотрите.</ta>
            <ta e="T132" id="Seg_4008" s="T127">Сказав это, человек тот вышел.</ta>
            <ta e="T135" id="Seg_4009" s="T132">Дочь — вслед за ним.</ta>
            <ta e="T141" id="Seg_4010" s="T135">Старик со старухой, провожая их, вышли во двор.</ta>
            <ta e="T156" id="Seg_4011" s="T141">Увидели: мужчина-зять, превратившись в огромного самцс дикого оленя, а дочь их, став двухгодовалой важенкой, поплыли через реку.</ta>
            <ta e="T160" id="Seg_4012" s="T156">Утром старик вышел во двор мочиться.</ta>
            <ta e="T166" id="Seg_4013" s="T160">Смотрит: семь диких оленей плывут через реку.</ta>
            <ta e="T178" id="Seg_4014" s="T166">Тут он взял копье, сел в лодку и переколол всех семерых.</ta>
            <ta e="T181" id="Seg_4015" s="T178">Стал свежевать их туши.</ta>
            <ta e="T184" id="Seg_4016" s="T181">Старуха же спала.</ta>
            <ta e="T196" id="Seg_4017" s="T184">Когда она ночью проснулась, оказалось, что на железной цепи с неба спустилась железная колыбель.</ta>
            <ta e="T201" id="Seg_4018" s="T196">Внутри нее, видно, ребенок: слышен плач.</ta>
            <ta e="T210" id="Seg_4019" s="T201">Старухе стало жалко его, она приоткрыла покрывало и посмотрела.</ta>
            <ta e="T218" id="Seg_4020" s="T210">Увидел: лежит, оказывается, белый, как снег, ребенок.</ta>
            <ta e="T224" id="Seg_4021" s="T218">Тут ребенок закричал смертным криком:</ta>
            <ta e="T234" id="Seg_4022" s="T224">— Сейчас лопнет глаз мой! — после этих слов цепь стала поднимать колыбель вверх.</ta>
            <ta e="T237" id="Seg_4023" s="T234">Послышался голос ребенка:</ta>
            <ta e="T257" id="Seg_4024" s="T237">— Мой старший брат был сыном Юрюнг Айыы, меня брат послал, он говорил, не будут, мол, глядеть на меня в течение трех дней, чтоб я не окривел от сглаза.</ta>
            <ta e="T265" id="Seg_4025" s="T257">Теперь возвратиться или не возвратиться мне к вам, решит мой старший брат, — сказал.</ta>
            <ta e="T276" id="Seg_4026" s="T265">В это время старик, кончив свежевать диких оленей, шел, неся на плечах две задние ноги оленя.</ta>
            <ta e="T280" id="Seg_4027" s="T276">Тут старуха, встретив его, обо всем рассказала.</ta>
            <ta e="T284" id="Seg_4028" s="T280">Старик, не поверив, вбежал в юрту.</ta>
            <ta e="T290" id="Seg_4029" s="T284">Увидев, что ребенка нет, стал бить старуху одним стегном.</ta>
            <ta e="T300" id="Seg_4030" s="T290">Старуха схватила другое стегно, и побили они друг друга до смерти.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_4031" s="T0">В старину старик со старухой жили.</ta>
            <ta e="T7" id="Seg_4032" s="T4">У них была одна дочь.</ta>
            <ta e="T15" id="Seg_4033" s="T7">Эта дочь, как только вечером гасили свой светильник, начинала с кем-то разговаривать:</ta>
            <ta e="T20" id="Seg_4034" s="T15">— Я за тебя замуж пойду, — говорила.</ta>
            <ta e="T30" id="Seg_4035" s="T20">Днем, как ни спрашивали у дочки, ничего от нее не могли добиться.</ta>
            <ta e="T39" id="Seg_4036" s="T30">Однажды вечером, когда улеглись, старики прикрыли свой светильник, не погасив его.</ta>
            <ta e="T43" id="Seg_4037" s="T39">Дочь опять начала разговаривать.</ta>
            <ta e="T48" id="Seg_4038" s="T43">Тогда сбросили со светильника крышку.</ta>
            <ta e="T54" id="Seg_4039" s="T48">Увидели: с их дочерью один человек лежит.</ta>
            <ta e="T62" id="Seg_4040" s="T54">— Ну ты, значит, наш зятек, — сказали старик со старухой.</ta>
            <ta e="T65" id="Seg_4041" s="T62">На это человек ответил:</ta>
            <ta e="T78" id="Seg_4042" s="T65">— Счастье, значит, ваше, что меня увидели, иначе не знали бы, куда девалась ваша дочь, — сказал.</ta>
            <ta e="T83" id="Seg_4043" s="T78">— Но все равно уведу вашу дочь.</ta>
            <ta e="T96" id="Seg_4044" s="T83">После того как уйду, пусть завтра рано, с восходом солнца, выйдет старик. ‎‎Семь диких оленей будут переплывать реку.</ta>
            <ta e="T101" id="Seg_4045" s="T96">По этому узнаете, что я принес вам счастье.</ta>
            <ta e="T106" id="Seg_4046" s="T101">Пусть старик постарается убить тех диких оленей.</ta>
            <ta e="T116" id="Seg_4047" s="T106">Когда закончит снимать с них шкуры, на железной цепи с неба спустится железная колыбель с ребенком.</ta>
            <ta e="T119" id="Seg_4048" s="T116">Она будет закрыта.</ta>
            <ta e="T127" id="Seg_4049" s="T119">В течение трех дней ни за что не открывайте и не смотрите.</ta>
            <ta e="T132" id="Seg_4050" s="T127">Сказав это, человек тот вышел.</ta>
            <ta e="T135" id="Seg_4051" s="T132">Дочь — вслед за ним.</ta>
            <ta e="T141" id="Seg_4052" s="T135">Старик со старухой, провожая их, вышли во двор.</ta>
            <ta e="T156" id="Seg_4053" s="T141">Увидели: мужчина-зять, превратившись в огромного самцс дикого оленя, а дочь их, став двухгодовалой важенкой, поплыли через реку.</ta>
            <ta e="T160" id="Seg_4054" s="T156">Утром старик вышел во двор.</ta>
            <ta e="T166" id="Seg_4055" s="T160">Смотрит; семь диких оленей плывут через реку.</ta>
            <ta e="T181" id="Seg_4056" s="T178">Стал свежевать их туши.</ta>
            <ta e="T184" id="Seg_4057" s="T181">Старуха же спала.</ta>
            <ta e="T196" id="Seg_4058" s="T184">Когда она ночью проснулась, оказалось, что на железной цепи с неба спустилась железная колыбель.</ta>
            <ta e="T201" id="Seg_4059" s="T196">Внутри нее, видно, ребенок: слышен плач.</ta>
            <ta e="T210" id="Seg_4060" s="T201">Старухе стало жалко его, она приоткрыла покрывало и посмотрела.</ta>
            <ta e="T218" id="Seg_4061" s="T210">Видит: лежит, оказывается, белый, как снег, ребенок.</ta>
            <ta e="T224" id="Seg_4062" s="T218">Тут ребенок закричал смертным криком:</ta>
            <ta e="T234" id="Seg_4063" s="T224">— Сейчас лопнет глаз мой! — после этих слов цепь стала поднимать колыбель вверх.</ta>
            <ta e="T237" id="Seg_4064" s="T234">Послышался голос ребенка:</ta>
            <ta e="T257" id="Seg_4065" s="T237">— Мой старший брат был сыном Юрюнг Айыы, меня брат послал, он говорил, не будут, мол, глядеть на меня в течение трех дней, чтоб я не окривел от сглаза.</ta>
            <ta e="T265" id="Seg_4066" s="T257">Теперь возвратиться или не возвратиться мне к вам, решит мой старший брат, — сказал.</ta>
            <ta e="T276" id="Seg_4067" s="T265">В это время старик, кончив свежевать диких оленей, шел, неся на плечах две задние ноги оленя.</ta>
            <ta e="T280" id="Seg_4068" s="T276">Тут старуха, встретив его, обо всем рассказала.</ta>
            <ta e="T284" id="Seg_4069" s="T280">Старик, не поверив, вбежал в юрту.</ta>
            <ta e="T290" id="Seg_4070" s="T284">Увидев, что ребенка нет, стал бить старуху одним стегном.</ta>
            <ta e="T300" id="Seg_4071" s="T290">Старуха схватила другое стегно, и побили они друг друга до смерти.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T96" id="Seg_4072" s="T83">[DCh]: "ebe" 'grandmother' is a euphemism for 'river'; according to Dolgan belief rivers and lakes must not be named properly</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
