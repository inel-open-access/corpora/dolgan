<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDD804F9A2-F019-4DD9-385D-2E5F1BF921B2">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>BaA_1930_OldManOldWoman_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\BaA_1930_OldManOldWoman_flk\BaA_1930_OldManOldWoman_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">781</ud-information>
            <ud-information attribute-name="# HIAT:w">605</ud-information>
            <ud-information attribute-name="# e">601</ud-information>
            <ud-information attribute-name="# HIAT:u">82</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="BaA">
            <abbreviation>BaA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
         <tli id="T161" time="84.5" type="appl" />
         <tli id="T162" time="85.0" type="appl" />
         <tli id="T163" time="85.5" type="appl" />
         <tli id="T164" time="86.0" type="appl" />
         <tli id="T165" time="86.5" type="appl" />
         <tli id="T166" time="87.0" type="appl" />
         <tli id="T167" time="87.5" type="appl" />
         <tli id="T168" time="88.0" type="appl" />
         <tli id="T169" time="88.5" type="appl" />
         <tli id="T170" time="89.0" type="appl" />
         <tli id="T171" time="89.5" type="appl" />
         <tli id="T172" time="90.0" type="appl" />
         <tli id="T173" time="90.5" type="appl" />
         <tli id="T174" time="91.0" type="appl" />
         <tli id="T175" time="91.5" type="appl" />
         <tli id="T176" time="92.0" type="appl" />
         <tli id="T177" time="92.5" type="appl" />
         <tli id="T178" time="93.0" type="appl" />
         <tli id="T179" time="93.5" type="appl" />
         <tli id="T180" time="94.0" type="appl" />
         <tli id="T181" time="94.5" type="appl" />
         <tli id="T182" time="95.0" type="appl" />
         <tli id="T183" time="95.5" type="appl" />
         <tli id="T184" time="96.0" type="appl" />
         <tli id="T185" time="96.5" type="appl" />
         <tli id="T186" time="97.0" type="appl" />
         <tli id="T187" time="97.5" type="appl" />
         <tli id="T188" time="98.0" type="appl" />
         <tli id="T189" time="98.5" type="appl" />
         <tli id="T190" time="99.0" type="appl" />
         <tli id="T191" time="99.5" type="appl" />
         <tli id="T192" time="100.0" type="appl" />
         <tli id="T193" time="100.5" type="appl" />
         <tli id="T194" time="101.0" type="appl" />
         <tli id="T195" time="101.5" type="appl" />
         <tli id="T196" time="102.0" type="appl" />
         <tli id="T197" time="102.5" type="appl" />
         <tli id="T198" time="103.0" type="appl" />
         <tli id="T199" time="103.5" type="appl" />
         <tli id="T200" time="104.0" type="appl" />
         <tli id="T201" time="104.5" type="appl" />
         <tli id="T202" time="105.0" type="appl" />
         <tli id="T203" time="105.5" type="appl" />
         <tli id="T204" time="106.0" type="appl" />
         <tli id="T205" time="106.5" type="appl" />
         <tli id="T206" time="107.0" type="appl" />
         <tli id="T207" time="107.5" type="appl" />
         <tli id="T208" time="108.0" type="appl" />
         <tli id="T209" time="108.5" type="appl" />
         <tli id="T210" time="109.0" type="appl" />
         <tli id="T211" time="109.5" type="appl" />
         <tli id="T212" time="110.0" type="appl" />
         <tli id="T213" time="110.5" type="appl" />
         <tli id="T214" time="111.0" type="appl" />
         <tli id="T215" time="111.5" type="appl" />
         <tli id="T216" time="112.0" type="appl" />
         <tli id="T217" time="112.5" type="appl" />
         <tli id="T218" time="113.0" type="appl" />
         <tli id="T219" time="113.5" type="appl" />
         <tli id="T220" time="114.0" type="appl" />
         <tli id="T221" time="114.5" type="appl" />
         <tli id="T222" time="115.0" type="appl" />
         <tli id="T223" time="115.5" type="appl" />
         <tli id="T224" time="116.0" type="appl" />
         <tli id="T225" time="116.5" type="appl" />
         <tli id="T226" time="117.0" type="appl" />
         <tli id="T227" time="117.5" type="appl" />
         <tli id="T228" time="118.0" type="appl" />
         <tli id="T229" time="118.5" type="appl" />
         <tli id="T230" time="119.0" type="appl" />
         <tli id="T231" time="119.5" type="appl" />
         <tli id="T232" time="120.0" type="appl" />
         <tli id="T233" time="120.5" type="appl" />
         <tli id="T234" time="121.0" type="appl" />
         <tli id="T235" time="121.5" type="appl" />
         <tli id="T236" time="122.0" type="appl" />
         <tli id="T237" time="122.5" type="appl" />
         <tli id="T238" time="123.0" type="appl" />
         <tli id="T239" time="123.5" type="appl" />
         <tli id="T240" time="124.0" type="appl" />
         <tli id="T241" time="124.5" type="appl" />
         <tli id="T242" time="125.0" type="appl" />
         <tli id="T243" time="125.5" type="appl" />
         <tli id="T244" time="126.0" type="appl" />
         <tli id="T245" time="126.5" type="appl" />
         <tli id="T246" time="127.0" type="appl" />
         <tli id="T247" time="127.5" type="appl" />
         <tli id="T248" time="128.0" type="appl" />
         <tli id="T249" time="128.5" type="appl" />
         <tli id="T250" time="129.0" type="appl" />
         <tli id="T251" time="129.5" type="appl" />
         <tli id="T252" time="130.0" type="appl" />
         <tli id="T253" time="130.5" type="appl" />
         <tli id="T254" time="131.0" type="appl" />
         <tli id="T255" time="131.5" type="appl" />
         <tli id="T256" time="132.0" type="appl" />
         <tli id="T257" time="132.5" type="appl" />
         <tli id="T258" time="133.0" type="appl" />
         <tli id="T259" time="133.5" type="appl" />
         <tli id="T260" time="134.0" type="appl" />
         <tli id="T261" time="134.5" type="appl" />
         <tli id="T262" time="135.0" type="appl" />
         <tli id="T263" time="135.5" type="appl" />
         <tli id="T264" time="136.0" type="appl" />
         <tli id="T265" time="136.5" type="appl" />
         <tli id="T266" time="137.0" type="appl" />
         <tli id="T267" time="137.5" type="appl" />
         <tli id="T268" time="138.0" type="appl" />
         <tli id="T269" time="138.5" type="appl" />
         <tli id="T270" time="139.0" type="appl" />
         <tli id="T271" time="139.5" type="appl" />
         <tli id="T272" time="140.0" type="appl" />
         <tli id="T273" time="140.5" type="appl" />
         <tli id="T274" time="141.0" type="appl" />
         <tli id="T275" time="141.5" type="appl" />
         <tli id="T276" time="142.0" type="appl" />
         <tli id="T277" time="142.5" type="appl" />
         <tli id="T278" time="143.0" type="appl" />
         <tli id="T279" time="143.5" type="appl" />
         <tli id="T280" time="144.0" type="appl" />
         <tli id="T281" time="144.5" type="appl" />
         <tli id="T282" time="145.0" type="appl" />
         <tli id="T283" time="145.5" type="appl" />
         <tli id="T284" time="146.0" type="appl" />
         <tli id="T285" time="146.5" type="appl" />
         <tli id="T286" time="147.0" type="appl" />
         <tli id="T287" time="147.5" type="appl" />
         <tli id="T288" time="148.0" type="appl" />
         <tli id="T289" time="148.5" type="appl" />
         <tli id="T290" time="149.0" type="appl" />
         <tli id="T291" time="149.5" type="appl" />
         <tli id="T292" time="150.0" type="appl" />
         <tli id="T293" time="150.5" type="appl" />
         <tli id="T294" time="151.0" type="appl" />
         <tli id="T295" time="151.5" type="appl" />
         <tli id="T296" time="152.0" type="appl" />
         <tli id="T297" time="152.5" type="appl" />
         <tli id="T298" time="153.0" type="appl" />
         <tli id="T299" time="153.5" type="appl" />
         <tli id="T300" time="154.0" type="appl" />
         <tli id="T301" time="154.5" type="appl" />
         <tli id="T302" time="155.0" type="appl" />
         <tli id="T303" time="155.5" type="appl" />
         <tli id="T304" time="156.0" type="appl" />
         <tli id="T305" time="156.5" type="appl" />
         <tli id="T306" time="157.0" type="appl" />
         <tli id="T307" time="157.5" type="appl" />
         <tli id="T308" time="158.0" type="appl" />
         <tli id="T309" time="158.5" type="appl" />
         <tli id="T310" time="159.0" type="appl" />
         <tli id="T311" time="159.5" type="appl" />
         <tli id="T312" time="160.0" type="appl" />
         <tli id="T313" time="160.5" type="appl" />
         <tli id="T314" time="161.0" type="appl" />
         <tli id="T315" time="161.5" type="appl" />
         <tli id="T316" time="162.0" type="appl" />
         <tli id="T317" time="162.5" type="appl" />
         <tli id="T318" time="163.0" type="appl" />
         <tli id="T319" time="163.5" type="appl" />
         <tli id="T320" time="164.0" type="appl" />
         <tli id="T321" time="164.5" type="appl" />
         <tli id="T322" time="165.0" type="appl" />
         <tli id="T323" time="165.5" type="appl" />
         <tli id="T324" time="166.0" type="appl" />
         <tli id="T325" time="166.5" type="appl" />
         <tli id="T326" time="167.0" type="appl" />
         <tli id="T327" time="167.5" type="appl" />
         <tli id="T328" time="168.0" type="appl" />
         <tli id="T329" time="168.5" type="appl" />
         <tli id="T330" time="169.0" type="appl" />
         <tli id="T331" time="169.5" type="appl" />
         <tli id="T332" time="170.0" type="appl" />
         <tli id="T333" time="170.5" type="appl" />
         <tli id="T334" time="171.0" type="appl" />
         <tli id="T335" time="171.5" type="appl" />
         <tli id="T336" time="172.0" type="appl" />
         <tli id="T337" time="172.5" type="appl" />
         <tli id="T338" time="173.0" type="appl" />
         <tli id="T339" time="173.5" type="appl" />
         <tli id="T340" time="174.0" type="appl" />
         <tli id="T341" time="174.5" type="appl" />
         <tli id="T342" time="175.0" type="appl" />
         <tli id="T343" time="175.5" type="appl" />
         <tli id="T344" time="176.0" type="appl" />
         <tli id="T345" time="176.5" type="appl" />
         <tli id="T346" time="177.0" type="appl" />
         <tli id="T347" time="177.5" type="appl" />
         <tli id="T348" time="178.0" type="appl" />
         <tli id="T349" time="178.5" type="appl" />
         <tli id="T350" time="179.0" type="appl" />
         <tli id="T351" time="179.5" type="appl" />
         <tli id="T352" time="180.0" type="appl" />
         <tli id="T353" time="180.5" type="appl" />
         <tli id="T354" time="181.0" type="appl" />
         <tli id="T355" time="181.5" type="appl" />
         <tli id="T356" time="182.0" type="appl" />
         <tli id="T357" time="182.5" type="appl" />
         <tli id="T358" time="183.0" type="appl" />
         <tli id="T359" time="183.5" type="appl" />
         <tli id="T360" time="184.0" type="appl" />
         <tli id="T361" time="184.5" type="appl" />
         <tli id="T362" time="185.0" type="appl" />
         <tli id="T363" time="185.5" type="appl" />
         <tli id="T364" time="186.0" type="appl" />
         <tli id="T365" time="186.5" type="appl" />
         <tli id="T366" time="187.0" type="appl" />
         <tli id="T367" time="187.5" type="appl" />
         <tli id="T368" time="188.0" type="appl" />
         <tli id="T369" time="188.5" type="appl" />
         <tli id="T370" time="189.0" type="appl" />
         <tli id="T371" time="189.5" type="appl" />
         <tli id="T372" time="190.0" type="appl" />
         <tli id="T373" time="190.5" type="appl" />
         <tli id="T374" time="191.0" type="appl" />
         <tli id="T375" time="191.5" type="appl" />
         <tli id="T376" time="192.0" type="appl" />
         <tli id="T377" time="192.5" type="appl" />
         <tli id="T378" time="193.0" type="appl" />
         <tli id="T379" time="193.5" type="appl" />
         <tli id="T380" time="194.0" type="appl" />
         <tli id="T381" time="194.5" type="appl" />
         <tli id="T382" time="195.0" type="appl" />
         <tli id="T383" time="195.5" type="appl" />
         <tli id="T384" time="196.0" type="appl" />
         <tli id="T385" time="196.5" type="appl" />
         <tli id="T386" time="197.0" type="appl" />
         <tli id="T387" time="197.5" type="appl" />
         <tli id="T388" time="198.0" type="appl" />
         <tli id="T389" time="198.5" type="appl" />
         <tli id="T390" time="199.0" type="appl" />
         <tli id="T391" time="199.5" type="appl" />
         <tli id="T392" time="200.0" type="appl" />
         <tli id="T393" time="200.5" type="appl" />
         <tli id="T394" time="201.0" type="appl" />
         <tli id="T395" time="201.5" type="appl" />
         <tli id="T396" time="202.0" type="appl" />
         <tli id="T397" time="202.5" type="appl" />
         <tli id="T398" time="203.0" type="appl" />
         <tli id="T399" time="203.5" type="appl" />
         <tli id="T400" time="204.0" type="appl" />
         <tli id="T401" time="204.5" type="appl" />
         <tli id="T402" time="205.0" type="appl" />
         <tli id="T403" time="205.5" type="appl" />
         <tli id="T404" time="206.0" type="appl" />
         <tli id="T405" time="206.5" type="appl" />
         <tli id="T406" time="207.0" type="appl" />
         <tli id="T407" time="207.5" type="appl" />
         <tli id="T408" time="208.0" type="appl" />
         <tli id="T409" time="208.5" type="appl" />
         <tli id="T410" time="209.0" type="appl" />
         <tli id="T411" time="209.5" type="appl" />
         <tli id="T412" time="210.0" type="appl" />
         <tli id="T413" time="210.5" type="appl" />
         <tli id="T414" time="211.0" type="appl" />
         <tli id="T415" time="211.5" type="appl" />
         <tli id="T416" time="212.0" type="appl" />
         <tli id="T417" time="212.5" type="appl" />
         <tli id="T418" time="213.0" type="appl" />
         <tli id="T419" time="213.5" type="appl" />
         <tli id="T420" time="214.0" type="appl" />
         <tli id="T421" time="214.5" type="appl" />
         <tli id="T422" time="215.0" type="appl" />
         <tli id="T423" time="215.5" type="appl" />
         <tli id="T424" time="216.0" type="appl" />
         <tli id="T425" time="216.5" type="appl" />
         <tli id="T426" time="217.0" type="appl" />
         <tli id="T427" time="217.5" type="appl" />
         <tli id="T428" time="218.0" type="appl" />
         <tli id="T429" time="218.5" type="appl" />
         <tli id="T430" time="219.0" type="appl" />
         <tli id="T431" time="219.5" type="appl" />
         <tli id="T432" time="220.0" type="appl" />
         <tli id="T433" time="220.5" type="appl" />
         <tli id="T434" time="221.0" type="appl" />
         <tli id="T435" time="221.5" type="appl" />
         <tli id="T436" time="222.0" type="appl" />
         <tli id="T437" time="222.5" type="appl" />
         <tli id="T438" time="223.0" type="appl" />
         <tli id="T439" time="223.5" type="appl" />
         <tli id="T440" time="224.0" type="appl" />
         <tli id="T441" time="224.5" type="appl" />
         <tli id="T442" time="225.0" type="appl" />
         <tli id="T443" time="225.5" type="appl" />
         <tli id="T444" time="226.0" type="appl" />
         <tli id="T445" time="226.5" type="appl" />
         <tli id="T446" time="227.0" type="appl" />
         <tli id="T447" time="227.5" type="appl" />
         <tli id="T448" time="228.0" type="appl" />
         <tli id="T449" time="228.5" type="appl" />
         <tli id="T450" time="229.0" type="appl" />
         <tli id="T451" time="229.5" type="appl" />
         <tli id="T452" time="230.0" type="appl" />
         <tli id="T453" time="230.5" type="appl" />
         <tli id="T454" time="231.0" type="appl" />
         <tli id="T455" time="231.5" type="appl" />
         <tli id="T456" time="232.0" type="appl" />
         <tli id="T457" time="232.5" type="appl" />
         <tli id="T458" time="233.0" type="appl" />
         <tli id="T459" time="233.5" type="appl" />
         <tli id="T460" time="234.0" type="appl" />
         <tli id="T461" time="234.5" type="appl" />
         <tli id="T462" time="235.0" type="appl" />
         <tli id="T463" time="235.5" type="appl" />
         <tli id="T464" time="236.0" type="appl" />
         <tli id="T465" time="236.5" type="appl" />
         <tli id="T466" time="237.0" type="appl" />
         <tli id="T467" time="237.5" type="appl" />
         <tli id="T468" time="238.0" type="appl" />
         <tli id="T469" time="238.5" type="appl" />
         <tli id="T470" time="239.0" type="appl" />
         <tli id="T471" time="239.5" type="appl" />
         <tli id="T472" time="240.0" type="appl" />
         <tli id="T473" time="240.5" type="appl" />
         <tli id="T474" time="241.0" type="appl" />
         <tli id="T475" time="241.5" type="appl" />
         <tli id="T476" time="242.0" type="appl" />
         <tli id="T477" time="242.5" type="appl" />
         <tli id="T478" time="243.0" type="appl" />
         <tli id="T479" time="243.5" type="appl" />
         <tli id="T480" time="244.0" type="appl" />
         <tli id="T481" time="244.5" type="appl" />
         <tli id="T482" time="245.0" type="appl" />
         <tli id="T483" time="245.5" type="appl" />
         <tli id="T484" time="246.0" type="appl" />
         <tli id="T485" time="246.5" type="appl" />
         <tli id="T486" time="247.0" type="appl" />
         <tli id="T487" time="247.5" type="appl" />
         <tli id="T488" time="248.0" type="appl" />
         <tli id="T489" time="248.5" type="appl" />
         <tli id="T490" time="249.0" type="appl" />
         <tli id="T491" time="249.5" type="appl" />
         <tli id="T492" time="250.0" type="appl" />
         <tli id="T493" time="250.5" type="appl" />
         <tli id="T494" time="251.0" type="appl" />
         <tli id="T495" time="251.5" type="appl" />
         <tli id="T496" time="252.0" type="appl" />
         <tli id="T497" time="252.5" type="appl" />
         <tli id="T498" time="253.0" type="appl" />
         <tli id="T499" time="253.5" type="appl" />
         <tli id="T500" time="254.0" type="appl" />
         <tli id="T501" time="254.5" type="appl" />
         <tli id="T502" time="255.0" type="appl" />
         <tli id="T503" time="255.5" type="appl" />
         <tli id="T504" time="256.0" type="appl" />
         <tli id="T505" time="256.5" type="appl" />
         <tli id="T506" time="257.0" type="appl" />
         <tli id="T507" time="257.5" type="appl" />
         <tli id="T508" time="258.0" type="appl" />
         <tli id="T509" time="258.5" type="appl" />
         <tli id="T510" time="259.0" type="appl" />
         <tli id="T511" time="259.5" type="appl" />
         <tli id="T512" time="260.0" type="appl" />
         <tli id="T513" time="260.5" type="appl" />
         <tli id="T514" time="261.0" type="appl" />
         <tli id="T515" time="261.5" type="appl" />
         <tli id="T516" time="262.0" type="appl" />
         <tli id="T517" time="262.5" type="appl" />
         <tli id="T518" time="263.0" type="appl" />
         <tli id="T519" time="263.5" type="appl" />
         <tli id="T520" time="264.0" type="appl" />
         <tli id="T521" time="264.5" type="appl" />
         <tli id="T522" time="265.0" type="appl" />
         <tli id="T523" time="265.5" type="appl" />
         <tli id="T524" time="266.0" type="appl" />
         <tli id="T525" time="266.5" type="appl" />
         <tli id="T526" time="267.0" type="appl" />
         <tli id="T527" time="267.5" type="appl" />
         <tli id="T528" time="268.0" type="appl" />
         <tli id="T529" time="268.5" type="appl" />
         <tli id="T530" time="269.0" type="appl" />
         <tli id="T531" time="269.5" type="appl" />
         <tli id="T532" time="270.0" type="appl" />
         <tli id="T533" time="270.5" type="appl" />
         <tli id="T534" time="271.0" type="appl" />
         <tli id="T535" time="271.5" type="appl" />
         <tli id="T536" time="272.0" type="appl" />
         <tli id="T537" time="272.5" type="appl" />
         <tli id="T538" time="273.0" type="appl" />
         <tli id="T539" time="273.5" type="appl" />
         <tli id="T540" time="274.0" type="appl" />
         <tli id="T541" time="274.5" type="appl" />
         <tli id="T542" time="275.0" type="appl" />
         <tli id="T543" time="275.5" type="appl" />
         <tli id="T544" time="276.0" type="appl" />
         <tli id="T545" time="276.5" type="appl" />
         <tli id="T546" time="277.0" type="appl" />
         <tli id="T547" time="277.5" type="appl" />
         <tli id="T548" time="278.0" type="appl" />
         <tli id="T549" time="278.5" type="appl" />
         <tli id="T550" time="279.0" type="appl" />
         <tli id="T551" time="279.5" type="appl" />
         <tli id="T552" time="280.0" type="appl" />
         <tli id="T553" time="280.5" type="appl" />
         <tli id="T554" time="281.0" type="appl" />
         <tli id="T555" time="281.5" type="appl" />
         <tli id="T556" time="282.0" type="appl" />
         <tli id="T557" time="282.5" type="appl" />
         <tli id="T558" time="283.0" type="appl" />
         <tli id="T559" time="283.5" type="appl" />
         <tli id="T560" time="284.0" type="appl" />
         <tli id="T561" time="284.5" type="appl" />
         <tli id="T562" time="285.0" type="appl" />
         <tli id="T563" time="285.5" type="appl" />
         <tli id="T564" time="286.0" type="appl" />
         <tli id="T565" time="286.5" type="appl" />
         <tli id="T566" time="287.0" type="appl" />
         <tli id="T567" time="287.5" type="appl" />
         <tli id="T568" time="288.0" type="appl" />
         <tli id="T569" time="288.5" type="appl" />
         <tli id="T570" time="289.0" type="appl" />
         <tli id="T571" time="289.5" type="appl" />
         <tli id="T572" time="290.0" type="appl" />
         <tli id="T573" time="290.5" type="appl" />
         <tli id="T574" time="291.0" type="appl" />
         <tli id="T575" time="291.5" type="appl" />
         <tli id="T576" time="292.0" type="appl" />
         <tli id="T577" time="292.5" type="appl" />
         <tli id="T578" time="293.0" type="appl" />
         <tli id="T579" time="293.5" type="appl" />
         <tli id="T580" time="294.0" type="appl" />
         <tli id="T581" time="294.5" type="appl" />
         <tli id="T582" time="295.0" type="appl" />
         <tli id="T583" time="295.5" type="appl" />
         <tli id="T584" time="296.0" type="appl" />
         <tli id="T585" time="296.5" type="appl" />
         <tli id="T586" time="297.0" type="appl" />
         <tli id="T587" time="297.5" type="appl" />
         <tli id="T588" time="298.0" type="appl" />
         <tli id="T589" time="298.5" type="appl" />
         <tli id="T590" time="299.0" type="appl" />
         <tli id="T591" time="299.5" type="appl" />
         <tli id="T592" time="300.0" type="appl" />
         <tli id="T593" time="300.5" type="appl" />
         <tli id="T594" time="301.0" type="appl" />
         <tli id="T595" time="301.5" type="appl" />
         <tli id="T596" time="302.0" type="appl" />
         <tli id="T597" time="302.5" type="appl" />
         <tli id="T598" time="303.0" type="appl" />
         <tli id="T601" time="303.22727272727275" type="intp" />
         <tli id="T599" time="303.5" type="appl" />
         <tli id="T600" time="304.0" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="BaA"
                      type="t">
         <timeline-fork end="T26" start="T25">
            <tli id="T25.tx.1" />
         </timeline-fork>
         <timeline-fork end="T254" start="T253">
            <tli id="T253.tx.1" />
         </timeline-fork>
         <timeline-fork end="T285" start="T284">
            <tli id="T284.tx.1" />
         </timeline-fork>
         <timeline-fork end="T381" start="T380">
            <tli id="T380.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T600" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Ogonnʼordoːk</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">emeːksin</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">olorbuttar</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">ühü</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T7" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">Törötör</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">ogoto</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">hu͡oktar</ts>
                  <nts id="Seg_26" n="HIAT:ip">.</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_29" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">Dʼi͡ege</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">olordoktoruna</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">u͡ottara</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">ere</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">kɨtarar</ts>
                  <nts id="Seg_44" n="HIAT:ip">,</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">ogurduk</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">dʼadaŋɨlar</ts>
                  <nts id="Seg_51" n="HIAT:ip">.</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_54" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">Tahaːra</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">tagɨstaktarɨna</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_62" n="HIAT:w" s="T16">kaːrdara</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">ere</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">turtajar</ts>
                  <nts id="Seg_69" n="HIAT:ip">.</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_72" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_74" n="HIAT:w" s="T19">Tu͡oga</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_77" n="HIAT:w" s="T20">daː</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_80" n="HIAT:w" s="T21">hu͡oktar</ts>
                  <nts id="Seg_81" n="HIAT:ip">.</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_84" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_86" n="HIAT:w" s="T22">Araj</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_89" n="HIAT:w" s="T23">üptere</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_92" n="HIAT:w" s="T24">di͡ennere</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25.tx.1" id="Seg_95" n="HIAT:w" s="T25">hu͡os</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_98" n="HIAT:w" s="T25.tx.1">hogotok</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_101" n="HIAT:w" s="T26">attaːktar</ts>
                  <nts id="Seg_102" n="HIAT:ip">.</nts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T33" id="Seg_105" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_107" n="HIAT:w" s="T27">Onton</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_110" n="HIAT:w" s="T28">bultanar</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_113" n="HIAT:w" s="T29">tergettere</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_116" n="HIAT:w" s="T30">üs</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_119" n="HIAT:w" s="T31">taːstaːk</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_122" n="HIAT:w" s="T32">ilimkeːnneːkter</ts>
                  <nts id="Seg_123" n="HIAT:ip">.</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_126" n="HIAT:u" s="T33">
                  <ts e="T34" id="Seg_128" n="HIAT:w" s="T33">Ontularɨgar</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_131" n="HIAT:w" s="T34">uː</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_134" n="HIAT:w" s="T35">kurdʼagata</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_137" n="HIAT:w" s="T36">iːlinnegine</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_140" n="HIAT:w" s="T37">onu</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_143" n="HIAT:w" s="T38">hiːller</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_146" n="HIAT:w" s="T39">üčehege</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_149" n="HIAT:w" s="T40">ü͡ölen</ts>
                  <nts id="Seg_150" n="HIAT:ip">.</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_153" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_155" n="HIAT:w" s="T41">Araj</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_158" n="HIAT:w" s="T42">ölöːrü</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_161" n="HIAT:w" s="T43">ü͡ökejbitter</ts>
                  <nts id="Seg_162" n="HIAT:ip">,</nts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_165" n="HIAT:w" s="T44">bɨstaːrɨ</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_168" n="HIAT:w" s="T45">bɨ͡akajbɨttar</ts>
                  <nts id="Seg_169" n="HIAT:ip">.</nts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_172" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_174" n="HIAT:w" s="T46">Onton</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_177" n="HIAT:w" s="T47">ogonnʼoro</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_180" n="HIAT:w" s="T48">araj</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_183" n="HIAT:w" s="T49">kiːrbit</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_186" n="HIAT:w" s="T50">emeːksiniger</ts>
                  <nts id="Seg_187" n="HIAT:ip">.</nts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_190" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_192" n="HIAT:w" s="T51">Dʼe</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_195" n="HIAT:w" s="T52">kiːren</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_198" n="HIAT:w" s="T53">dʼi͡ebit</ts>
                  <nts id="Seg_199" n="HIAT:ip">:</nts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_202" n="HIAT:u" s="T54">
                  <nts id="Seg_203" n="HIAT:ip">"</nts>
                  <ts e="T55" id="Seg_205" n="HIAT:w" s="T54">Kaja</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_208" n="HIAT:w" s="T55">emeːksin</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_211" n="HIAT:w" s="T56">ürdüːbüt</ts>
                  <nts id="Seg_212" n="HIAT:ip">.</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_215" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_217" n="HIAT:w" s="T57">Kaːmtarɨ͡akpɨt</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_220" n="HIAT:w" s="T58">hogotok</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_223" n="HIAT:w" s="T59">attaːkpɨt</ts>
                  <nts id="Seg_224" n="HIAT:ip">,</nts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_227" n="HIAT:w" s="T60">hi͡ekpit</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_230" n="HIAT:w" s="T61">baːra</ts>
                  <nts id="Seg_231" n="HIAT:ip">,</nts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_234" n="HIAT:w" s="T62">onnoːgor</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_237" n="HIAT:w" s="T63">okton</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_240" n="HIAT:w" s="T64">öllökpütüne</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_243" n="HIAT:w" s="T65">kennibitiger</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_246" n="HIAT:w" s="T66">kaːlɨ͡aga</ts>
                  <nts id="Seg_247" n="HIAT:ip">.</nts>
                  <nts id="Seg_248" n="HIAT:ip">"</nts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_251" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_253" n="HIAT:w" s="T67">Onuga</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_256" n="HIAT:w" s="T68">emeːksin</ts>
                  <nts id="Seg_257" n="HIAT:ip">:</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_260" n="HIAT:u" s="T69">
                  <nts id="Seg_261" n="HIAT:ip">"</nts>
                  <ts e="T70" id="Seg_263" n="HIAT:w" s="T69">Bejeŋ</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_266" n="HIAT:w" s="T70">hanaːŋ</ts>
                  <nts id="Seg_267" n="HIAT:ip">"</nts>
                  <nts id="Seg_268" n="HIAT:ip">,</nts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_271" n="HIAT:w" s="T71">di͡ebit</ts>
                  <nts id="Seg_272" n="HIAT:ip">.</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_275" n="HIAT:u" s="T72">
                  <nts id="Seg_276" n="HIAT:ip">"</nts>
                  <ts e="T73" id="Seg_278" n="HIAT:w" s="T72">Ölördörgün</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_281" n="HIAT:w" s="T73">ere</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_284" n="HIAT:w" s="T74">hi͡ekpit</ts>
                  <nts id="Seg_285" n="HIAT:ip">!</nts>
                  <nts id="Seg_286" n="HIAT:ip">"</nts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_289" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_291" n="HIAT:w" s="T75">Ere</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_294" n="HIAT:w" s="T76">tagɨspɨt</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_297" n="HIAT:w" s="T77">daː</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_300" n="HIAT:w" s="T78">ölörbüt</ts>
                  <nts id="Seg_301" n="HIAT:ip">.</nts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_304" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_306" n="HIAT:w" s="T79">Ogonnʼoro</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_309" n="HIAT:w" s="T80">hülbüt</ts>
                  <nts id="Seg_310" n="HIAT:ip">,</nts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_313" n="HIAT:w" s="T81">araj</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_316" n="HIAT:w" s="T82">tɨmnɨː</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_319" n="HIAT:w" s="T83">bagajɨ</ts>
                  <nts id="Seg_320" n="HIAT:ip">,</nts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_323" n="HIAT:w" s="T84">čɨskaːn</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_326" n="HIAT:w" s="T85">bagajɨ</ts>
                  <nts id="Seg_327" n="HIAT:ip">,</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_330" n="HIAT:w" s="T86">ogonnʼoro</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_333" n="HIAT:w" s="T87">kiːrbit</ts>
                  <nts id="Seg_334" n="HIAT:ip">,</nts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_337" n="HIAT:w" s="T88">kiːren</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_340" n="HIAT:w" s="T89">di͡ebit</ts>
                  <nts id="Seg_341" n="HIAT:ip">:</nts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_344" n="HIAT:u" s="T90">
                  <nts id="Seg_345" n="HIAT:ip">"</nts>
                  <ts e="T91" id="Seg_347" n="HIAT:w" s="T90">Appɨt</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_350" n="HIAT:w" s="T91">etin</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_353" n="HIAT:w" s="T92">bütünnüː</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_356" n="HIAT:w" s="T93">golomokoːmmut</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_359" n="HIAT:w" s="T94">uŋu͡or</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_362" n="HIAT:w" s="T95">taskajdaːn</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_365" n="HIAT:w" s="T96">keːs</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_368" n="HIAT:w" s="T97">čɨːstatɨn</ts>
                  <nts id="Seg_369" n="HIAT:ip">.</nts>
                  <nts id="Seg_370" n="HIAT:ip">"</nts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T101" id="Seg_373" n="HIAT:u" s="T98">
                  <ts e="T99" id="Seg_375" n="HIAT:w" s="T98">Emeːksine</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_378" n="HIAT:w" s="T99">barɨtɨn</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_381" n="HIAT:w" s="T100">taskajdaːbɨt</ts>
                  <nts id="Seg_382" n="HIAT:ip">.</nts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_385" n="HIAT:u" s="T101">
                  <ts e="T102" id="Seg_387" n="HIAT:w" s="T101">Emeːksine</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_390" n="HIAT:w" s="T102">iliːtin</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_393" n="HIAT:w" s="T103">itteːri</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_396" n="HIAT:w" s="T104">kɨmmɨtɨn</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_399" n="HIAT:w" s="T105">ogonnʼor</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_402" n="HIAT:w" s="T106">eppit</ts>
                  <nts id="Seg_403" n="HIAT:ip">:</nts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T118" id="Seg_406" n="HIAT:u" s="T107">
                  <nts id="Seg_407" n="HIAT:ip">"</nts>
                  <ts e="T108" id="Seg_409" n="HIAT:w" s="T107">Hi͡ekkin</ts>
                  <nts id="Seg_410" n="HIAT:ip">,</nts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_413" n="HIAT:w" s="T108">barɨ</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_416" n="HIAT:w" s="T109">ihikker</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_419" n="HIAT:w" s="T110">holuːrgar</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_422" n="HIAT:w" s="T111">uːta</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_425" n="HIAT:w" s="T112">bahan</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_428" n="HIAT:w" s="T113">tolor</ts>
                  <nts id="Seg_429" n="HIAT:ip">,</nts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_432" n="HIAT:w" s="T114">belemneːn</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_435" n="HIAT:w" s="T115">baran</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_438" n="HIAT:w" s="T116">dʼe</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_441" n="HIAT:w" s="T117">ahaːr</ts>
                  <nts id="Seg_442" n="HIAT:ip">.</nts>
                  <nts id="Seg_443" n="HIAT:ip">"</nts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_446" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_448" n="HIAT:w" s="T118">Innʼe</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_451" n="HIAT:w" s="T119">gɨnan</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_454" n="HIAT:w" s="T120">emeːksin</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_457" n="HIAT:w" s="T121">taksan</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_460" n="HIAT:w" s="T122">barɨ</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_463" n="HIAT:w" s="T123">oŋkučagar</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_466" n="HIAT:w" s="T124">barɨkakaːnnarɨgar</ts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_469" n="HIAT:w" s="T125">uː</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_472" n="HIAT:w" s="T126">baspɨt</ts>
                  <nts id="Seg_473" n="HIAT:ip">.</nts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_476" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_478" n="HIAT:w" s="T127">Araj</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_481" n="HIAT:w" s="T128">bütehik</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_484" n="HIAT:w" s="T129">holuːrgakakkakaːnɨn</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_487" n="HIAT:w" s="T130">egeleːri</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_490" n="HIAT:w" s="T131">gɨmmɨta</ts>
                  <nts id="Seg_491" n="HIAT:ip">,</nts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_494" n="HIAT:w" s="T132">aːna</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_497" n="HIAT:w" s="T133">katɨːlaːk</ts>
                  <nts id="Seg_498" n="HIAT:ip">.</nts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_501" n="HIAT:u" s="T134">
                  <nts id="Seg_502" n="HIAT:ip">"</nts>
                  <ts e="T135" id="Seg_504" n="HIAT:w" s="T134">Kaja</ts>
                  <nts id="Seg_505" n="HIAT:ip">,</nts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_508" n="HIAT:w" s="T135">ogonnʼor</ts>
                  <nts id="Seg_509" n="HIAT:ip">,</nts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_512" n="HIAT:w" s="T136">aːŋŋɨn</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_515" n="HIAT:w" s="T137">as</ts>
                  <nts id="Seg_516" n="HIAT:ip">,</nts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_519" n="HIAT:w" s="T138">ülüjeːri</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_522" n="HIAT:w" s="T139">gɨnnɨm</ts>
                  <nts id="Seg_523" n="HIAT:ip">"</nts>
                  <nts id="Seg_524" n="HIAT:ip">,</nts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_527" n="HIAT:w" s="T140">di͡ebit</ts>
                  <nts id="Seg_528" n="HIAT:ip">.</nts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_531" n="HIAT:u" s="T141">
                  <nts id="Seg_532" n="HIAT:ip">"</nts>
                  <ts e="T142" id="Seg_534" n="HIAT:w" s="T141">Huːka</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_537" n="HIAT:w" s="T142">kɨːha</ts>
                  <nts id="Seg_538" n="HIAT:ip">,</nts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_541" n="HIAT:w" s="T143">kaːrga</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_544" n="HIAT:w" s="T144">kampɨ</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_547" n="HIAT:w" s="T145">bɨrak</ts>
                  <nts id="Seg_548" n="HIAT:ip">"</nts>
                  <nts id="Seg_549" n="HIAT:ip">,</nts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_552" n="HIAT:w" s="T146">di͡ebit</ts>
                  <nts id="Seg_553" n="HIAT:ip">.</nts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_556" n="HIAT:u" s="T147">
                  <ts e="T148" id="Seg_558" n="HIAT:w" s="T147">Emeːksine</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_561" n="HIAT:w" s="T148">baː</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_564" n="HIAT:w" s="T149">holuːrčagɨn</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_567" n="HIAT:w" s="T150">bɨraːn</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_570" n="HIAT:w" s="T151">baran</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_573" n="HIAT:w" s="T152">dʼe</ts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_576" n="HIAT:w" s="T153">toŋon</ts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_579" n="HIAT:w" s="T154">ölöːrü</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_582" n="HIAT:w" s="T155">gɨnar</ts>
                  <nts id="Seg_583" n="HIAT:ip">.</nts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T165" id="Seg_586" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_588" n="HIAT:w" s="T156">Emeːksine</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_591" n="HIAT:w" s="T157">balaganɨn</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_594" n="HIAT:w" s="T158">ürdüger</ts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_597" n="HIAT:w" s="T159">taksan</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_600" n="HIAT:w" s="T160">kɨːmkaːnnar</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_603" n="HIAT:w" s="T161">taksɨːlarɨgar</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_606" n="HIAT:w" s="T162">onno</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_609" n="HIAT:w" s="T163">iliːtin</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_612" n="HIAT:w" s="T164">itter</ts>
                  <nts id="Seg_613" n="HIAT:ip">.</nts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T176" id="Seg_616" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_618" n="HIAT:w" s="T165">Araj</ts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_621" n="HIAT:w" s="T166">erin</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_624" n="HIAT:w" s="T167">körbüte</ts>
                  <nts id="Seg_625" n="HIAT:ip">,</nts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_628" n="HIAT:w" s="T168">atɨn</ts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_631" n="HIAT:w" s="T169">hamagɨn</ts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_634" n="HIAT:w" s="T170">ɨlan</ts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_637" n="HIAT:w" s="T171">baran</ts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_640" n="HIAT:w" s="T172">alčajan</ts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_643" n="HIAT:w" s="T173">baran</ts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_646" n="HIAT:w" s="T174">hačalana</ts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_649" n="HIAT:w" s="T175">oloror</ts>
                  <nts id="Seg_650" n="HIAT:ip">.</nts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_653" n="HIAT:u" s="T176">
                  <ts e="T177" id="Seg_655" n="HIAT:w" s="T176">Ol</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_658" n="HIAT:w" s="T177">itteːri</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_661" n="HIAT:w" s="T178">gɨnar</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_664" n="HIAT:w" s="T179">emeːksinin</ts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_667" n="HIAT:w" s="T180">iliːtin</ts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_670" n="HIAT:w" s="T181">üčehennen</ts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_673" n="HIAT:w" s="T182">bɨhɨta</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_676" n="HIAT:w" s="T183">kejer</ts>
                  <nts id="Seg_677" n="HIAT:ip">.</nts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T195" id="Seg_680" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_682" n="HIAT:w" s="T184">Ontuta</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_685" n="HIAT:w" s="T185">ɨtɨː-ɨtɨː</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_688" n="HIAT:w" s="T186">hirge</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_691" n="HIAT:w" s="T187">tüheːkteːn</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_694" n="HIAT:w" s="T188">baran</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_697" n="HIAT:w" s="T189">mastɨːr</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_700" n="HIAT:w" s="T190">hirkeːn</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_703" n="HIAT:w" s="T191">di͡eg</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_706" n="HIAT:w" s="T192">ɨtɨː-ɨtɨː</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_709" n="HIAT:w" s="T193">kaːma</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_712" n="HIAT:w" s="T194">turar</ts>
                  <nts id="Seg_713" n="HIAT:ip">.</nts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T208" id="Seg_716" n="HIAT:u" s="T195">
                  <ts e="T196" id="Seg_718" n="HIAT:w" s="T195">Dʼe</ts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_721" n="HIAT:w" s="T196">onno</ts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_724" n="HIAT:w" s="T197">tiːtikun</ts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_727" n="HIAT:w" s="T198">mas</ts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_730" n="HIAT:w" s="T199">tördüger</ts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_733" n="HIAT:w" s="T200">hɨtaːrɨ</ts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_736" n="HIAT:w" s="T201">gɨmmɨt</ts>
                  <nts id="Seg_737" n="HIAT:ip">,</nts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_740" n="HIAT:w" s="T202">hɨtaːrɨ</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_743" n="HIAT:w" s="T203">gɨmmɨt</ts>
                  <nts id="Seg_744" n="HIAT:ip">,</nts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_747" n="HIAT:w" s="T204">tɨmnɨː</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_750" n="HIAT:w" s="T205">titiriːr</ts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_753" n="HIAT:w" s="T206">hüreger</ts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_756" n="HIAT:w" s="T207">kiːrer</ts>
                  <nts id="Seg_757" n="HIAT:ip">.</nts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T219" id="Seg_760" n="HIAT:u" s="T208">
                  <ts e="T209" id="Seg_762" n="HIAT:w" s="T208">Ol</ts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_765" n="HIAT:w" s="T209">hɨttagɨna</ts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_768" n="HIAT:w" s="T210">ajdaːn</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_771" n="HIAT:w" s="T211">bögö</ts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_774" n="HIAT:w" s="T212">ihillibit</ts>
                  <nts id="Seg_775" n="HIAT:ip">,</nts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_778" n="HIAT:w" s="T213">taba</ts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_781" n="HIAT:w" s="T214">üːrerten</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_784" n="HIAT:w" s="T215">taba</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_787" n="HIAT:w" s="T216">üːrer</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_790" n="HIAT:w" s="T217">enin</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_793" n="HIAT:w" s="T218">bagajɨ</ts>
                  <nts id="Seg_794" n="HIAT:ip">.</nts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T225" id="Seg_797" n="HIAT:u" s="T219">
                  <ts e="T220" id="Seg_799" n="HIAT:w" s="T219">Ol</ts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_802" n="HIAT:w" s="T220">kotu</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_805" n="HIAT:w" s="T221">dʼe</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_808" n="HIAT:w" s="T222">barar</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_811" n="HIAT:w" s="T223">bu</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_814" n="HIAT:w" s="T224">emeːksin</ts>
                  <nts id="Seg_815" n="HIAT:ip">.</nts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T240" id="Seg_818" n="HIAT:u" s="T225">
                  <ts e="T226" id="Seg_820" n="HIAT:w" s="T225">Ol</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_823" n="HIAT:w" s="T226">baran</ts>
                  <nts id="Seg_824" n="HIAT:ip">,</nts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_827" n="HIAT:w" s="T227">ajɨː</ts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_830" n="HIAT:w" s="T228">daganɨ</ts>
                  <nts id="Seg_831" n="HIAT:ip">,</nts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_834" n="HIAT:w" s="T229">ɨjɨ</ts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_837" n="HIAT:w" s="T230">bü͡ölüːr</ts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_840" n="HIAT:w" s="T231">ɨːs</ts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_843" n="HIAT:w" s="T232">tuman</ts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_846" n="HIAT:w" s="T233">tabaga</ts>
                  <nts id="Seg_847" n="HIAT:ip">,</nts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_850" n="HIAT:w" s="T234">künü</ts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_853" n="HIAT:w" s="T235">bü͡ölüːr</ts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_856" n="HIAT:w" s="T236">kuden</ts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_859" n="HIAT:w" s="T237">tuman</ts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_862" n="HIAT:w" s="T238">hü͡öhüge</ts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_865" n="HIAT:w" s="T239">tijer</ts>
                  <nts id="Seg_866" n="HIAT:ip">.</nts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_869" n="HIAT:u" s="T240">
                  <ts e="T241" id="Seg_871" n="HIAT:w" s="T240">Bu</ts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_874" n="HIAT:w" s="T241">emeːksiniŋ</ts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_877" n="HIAT:w" s="T242">dʼi͡eni</ts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_880" n="HIAT:w" s="T243">kördöːn</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_883" n="HIAT:w" s="T244">bu</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_886" n="HIAT:w" s="T245">tabanɨ</ts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_889" n="HIAT:w" s="T246">teleriger</ts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_892" n="HIAT:w" s="T247">ikki</ts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_895" n="HIAT:w" s="T248">eneːre</ts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_898" n="HIAT:w" s="T249">elejen</ts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_901" n="HIAT:w" s="T250">kaːlbɨt</ts>
                  <nts id="Seg_902" n="HIAT:ip">.</nts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T258" id="Seg_905" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_907" n="HIAT:w" s="T251">Araj</ts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_910" n="HIAT:w" s="T252">alɨːnɨ</ts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253.tx.1" id="Seg_913" n="HIAT:w" s="T253">tobus</ts>
                  <nts id="Seg_914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_916" n="HIAT:w" s="T253.tx.1">toloru</ts>
                  <nts id="Seg_917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_919" n="HIAT:w" s="T254">uraha</ts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_922" n="HIAT:w" s="T255">dʼi͡e</ts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_925" n="HIAT:w" s="T256">tutullan</ts>
                  <nts id="Seg_926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_928" n="HIAT:w" s="T257">turar</ts>
                  <nts id="Seg_929" n="HIAT:ip">.</nts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T274" id="Seg_932" n="HIAT:u" s="T258">
                  <ts e="T259" id="Seg_934" n="HIAT:w" s="T258">Ulakan</ts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_937" n="HIAT:w" s="T259">mɨjaː</ts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_940" n="HIAT:w" s="T260">dʼi͡e</ts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_943" n="HIAT:w" s="T261">kelle</ts>
                  <nts id="Seg_944" n="HIAT:ip">,</nts>
                  <nts id="Seg_945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_947" n="HIAT:w" s="T262">bu</ts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_950" n="HIAT:w" s="T263">dʼi͡ege</ts>
                  <nts id="Seg_951" n="HIAT:ip">,</nts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_954" n="HIAT:w" s="T264">manna</ts>
                  <nts id="Seg_955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_957" n="HIAT:w" s="T265">dʼe</ts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_960" n="HIAT:w" s="T266">kas</ts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_963" n="HIAT:w" s="T267">u͡on</ts>
                  <nts id="Seg_964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_966" n="HIAT:w" s="T268">kihi</ts>
                  <nts id="Seg_967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_969" n="HIAT:w" s="T269">bɨha</ts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_972" n="HIAT:w" s="T270">ölbügüne</ts>
                  <nts id="Seg_973" n="HIAT:ip">,</nts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_976" n="HIAT:w" s="T271">bu͡olaga</ts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_979" n="HIAT:w" s="T272">huburujan</ts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_982" n="HIAT:w" s="T273">turar</ts>
                  <nts id="Seg_983" n="HIAT:ip">.</nts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T282" id="Seg_986" n="HIAT:u" s="T274">
                  <ts e="T275" id="Seg_988" n="HIAT:w" s="T274">Innʼe</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_991" n="HIAT:w" s="T275">gɨnan</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_994" n="HIAT:w" s="T276">baː</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_997" n="HIAT:w" s="T277">emeːksin</ts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1000" n="HIAT:w" s="T278">baː</ts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1003" n="HIAT:w" s="T279">dʼi͡eni</ts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1006" n="HIAT:w" s="T280">arɨja</ts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1009" n="HIAT:w" s="T281">oksubut</ts>
                  <nts id="Seg_1010" n="HIAT:ip">.</nts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T287" id="Seg_1013" n="HIAT:u" s="T282">
                  <ts e="T283" id="Seg_1015" n="HIAT:w" s="T282">Oduːlaːn</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1018" n="HIAT:w" s="T283">körbüte</ts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1020" n="HIAT:ip">—</nts>
                  <nts id="Seg_1021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284.tx.1" id="Seg_1023" n="HIAT:w" s="T284">ibis</ts>
                  <nts id="Seg_1024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1026" n="HIAT:w" s="T284.tx.1">iččitek</ts>
                  <nts id="Seg_1027" n="HIAT:ip">,</nts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1030" n="HIAT:w" s="T285">kihite</ts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1033" n="HIAT:w" s="T286">hu͡ok</ts>
                  <nts id="Seg_1034" n="HIAT:ip">.</nts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T302" id="Seg_1037" n="HIAT:u" s="T287">
                  <ts e="T288" id="Seg_1039" n="HIAT:w" s="T287">Barɨ</ts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1042" n="HIAT:w" s="T288">kü͡ös</ts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1045" n="HIAT:w" s="T289">kü͡östenen</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1048" n="HIAT:w" s="T290">turar</ts>
                  <nts id="Seg_1049" n="HIAT:ip">,</nts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1052" n="HIAT:w" s="T291">barɨ</ts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1055" n="HIAT:w" s="T292">as</ts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1058" n="HIAT:w" s="T293">astanan</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1061" n="HIAT:w" s="T294">turar</ts>
                  <nts id="Seg_1062" n="HIAT:ip">,</nts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1065" n="HIAT:w" s="T295">taːs</ts>
                  <nts id="Seg_1066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1068" n="HIAT:w" s="T296">kɨrabaːttar</ts>
                  <nts id="Seg_1069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1071" n="HIAT:w" s="T297">bütünnüː</ts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1074" n="HIAT:w" s="T298">onnuk</ts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1077" n="HIAT:w" s="T299">tergetteːk</ts>
                  <nts id="Seg_1078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1080" n="HIAT:w" s="T300">dʼi͡e</ts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1083" n="HIAT:w" s="T301">turar</ts>
                  <nts id="Seg_1084" n="HIAT:ip">.</nts>
                  <nts id="Seg_1085" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T317" id="Seg_1087" n="HIAT:u" s="T302">
                  <ts e="T303" id="Seg_1089" n="HIAT:w" s="T302">Ol</ts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1092" n="HIAT:w" s="T303">emeːksin</ts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1095" n="HIAT:w" s="T304">kiːren</ts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1098" n="HIAT:w" s="T305">ahaːbatagɨn</ts>
                  <nts id="Seg_1099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1101" n="HIAT:w" s="T306">ahɨːr</ts>
                  <nts id="Seg_1102" n="HIAT:ip">,</nts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1105" n="HIAT:w" s="T307">hi͡ebetegin</ts>
                  <nts id="Seg_1106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1108" n="HIAT:w" s="T308">hiːr</ts>
                  <nts id="Seg_1109" n="HIAT:ip">,</nts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1112" n="HIAT:w" s="T309">nuːčča</ts>
                  <nts id="Seg_1113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1115" n="HIAT:w" s="T310">daː</ts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1118" n="HIAT:w" s="T311">aha</ts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1121" n="HIAT:w" s="T312">ügüs</ts>
                  <nts id="Seg_1122" n="HIAT:ip">,</nts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1125" n="HIAT:w" s="T313">tɨ͡a</ts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1128" n="HIAT:w" s="T314">daː</ts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1131" n="HIAT:w" s="T315">aha</ts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1134" n="HIAT:w" s="T316">ügüs</ts>
                  <nts id="Seg_1135" n="HIAT:ip">.</nts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T325" id="Seg_1138" n="HIAT:u" s="T317">
                  <ts e="T318" id="Seg_1140" n="HIAT:w" s="T317">Bu</ts>
                  <nts id="Seg_1141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1143" n="HIAT:w" s="T318">emeːksin</ts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1146" n="HIAT:w" s="T319">huːnan-taraːnan</ts>
                  <nts id="Seg_1147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1149" n="HIAT:w" s="T320">baran</ts>
                  <nts id="Seg_1150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1152" n="HIAT:w" s="T321">üːs-kiːs</ts>
                  <nts id="Seg_1153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1155" n="HIAT:w" s="T322">taŋahɨ</ts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1158" n="HIAT:w" s="T323">taŋnan</ts>
                  <nts id="Seg_1159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1161" n="HIAT:w" s="T324">keːspit</ts>
                  <nts id="Seg_1162" n="HIAT:ip">.</nts>
                  <nts id="Seg_1163" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T331" id="Seg_1165" n="HIAT:u" s="T325">
                  <ts e="T326" id="Seg_1167" n="HIAT:w" s="T325">Bu</ts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1170" n="HIAT:w" s="T326">tulkarɨ</ts>
                  <nts id="Seg_1171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1173" n="HIAT:w" s="T327">tu͡ok</ts>
                  <nts id="Seg_1174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1176" n="HIAT:w" s="T328">daː</ts>
                  <nts id="Seg_1177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1179" n="HIAT:w" s="T329">kihite</ts>
                  <nts id="Seg_1180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1182" n="HIAT:w" s="T330">billibet</ts>
                  <nts id="Seg_1183" n="HIAT:ip">.</nts>
                  <nts id="Seg_1184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T338" id="Seg_1186" n="HIAT:u" s="T331">
                  <ts e="T332" id="Seg_1188" n="HIAT:w" s="T331">Üs</ts>
                  <nts id="Seg_1189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1191" n="HIAT:w" s="T332">künü</ts>
                  <nts id="Seg_1192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1194" n="HIAT:w" s="T333">meldʼi</ts>
                  <nts id="Seg_1195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1197" n="HIAT:w" s="T334">tu͡ok</ts>
                  <nts id="Seg_1198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1200" n="HIAT:w" s="T335">daː</ts>
                  <nts id="Seg_1201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1203" n="HIAT:w" s="T336">kihite</ts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1206" n="HIAT:w" s="T337">billibet</ts>
                  <nts id="Seg_1207" n="HIAT:ip">.</nts>
                  <nts id="Seg_1208" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T347" id="Seg_1210" n="HIAT:u" s="T338">
                  <ts e="T339" id="Seg_1212" n="HIAT:w" s="T338">Araj</ts>
                  <nts id="Seg_1213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1215" n="HIAT:w" s="T339">ki͡ehe</ts>
                  <nts id="Seg_1216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1218" n="HIAT:w" s="T340">bu͡olu͡o</ts>
                  <nts id="Seg_1219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1221" n="HIAT:w" s="T341">daː</ts>
                  <nts id="Seg_1222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1224" n="HIAT:w" s="T342">ü͡ölehinen</ts>
                  <nts id="Seg_1225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1227" n="HIAT:w" s="T343">kur</ts>
                  <nts id="Seg_1228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1230" n="HIAT:w" s="T344">kihi</ts>
                  <nts id="Seg_1231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1233" n="HIAT:w" s="T345">mejiːlere</ts>
                  <nts id="Seg_1234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1236" n="HIAT:w" s="T346">tuheller</ts>
                  <nts id="Seg_1237" n="HIAT:ip">.</nts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T359" id="Seg_1240" n="HIAT:u" s="T347">
                  <ts e="T348" id="Seg_1242" n="HIAT:w" s="T347">Oloru</ts>
                  <nts id="Seg_1243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1244" n="HIAT:ip">"</nts>
                  <ts e="T349" id="Seg_1246" n="HIAT:w" s="T348">tehiŋ-buhuŋ</ts>
                  <nts id="Seg_1247" n="HIAT:ip">"</nts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1250" n="HIAT:w" s="T349">tehite</ts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1253" n="HIAT:w" s="T350">bar</ts>
                  <nts id="Seg_1254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1256" n="HIAT:w" s="T351">diːdiː</ts>
                  <nts id="Seg_1257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1259" n="HIAT:w" s="T352">tehite</ts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1262" n="HIAT:w" s="T353">üktüːr</ts>
                  <nts id="Seg_1263" n="HIAT:ip">,</nts>
                  <nts id="Seg_1264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1266" n="HIAT:w" s="T354">oloro</ts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1269" n="HIAT:w" s="T355">hir</ts>
                  <nts id="Seg_1270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1272" n="HIAT:w" s="T356">di͡eg</ts>
                  <nts id="Seg_1273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1275" n="HIAT:w" s="T357">himitten</ts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1278" n="HIAT:w" s="T358">iheller</ts>
                  <nts id="Seg_1279" n="HIAT:ip">.</nts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T377" id="Seg_1282" n="HIAT:u" s="T359">
                  <ts e="T360" id="Seg_1284" n="HIAT:w" s="T359">Emeːksin</ts>
                  <nts id="Seg_1285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1287" n="HIAT:w" s="T360">kah</ts>
                  <nts id="Seg_1288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1290" n="HIAT:w" s="T361">u͡on</ts>
                  <nts id="Seg_1291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1293" n="HIAT:w" s="T362">tukkarɨ</ts>
                  <nts id="Seg_1294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1296" n="HIAT:w" s="T363">olorbut</ts>
                  <nts id="Seg_1297" n="HIAT:ip">,</nts>
                  <nts id="Seg_1298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1300" n="HIAT:w" s="T364">tabata</ts>
                  <nts id="Seg_1301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1303" n="HIAT:w" s="T365">kanna</ts>
                  <nts id="Seg_1304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1306" n="HIAT:w" s="T366">daː</ts>
                  <nts id="Seg_1307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1309" n="HIAT:w" s="T367">kamnaːbat</ts>
                  <nts id="Seg_1310" n="HIAT:ip">,</nts>
                  <nts id="Seg_1311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1313" n="HIAT:w" s="T368">attara</ts>
                  <nts id="Seg_1314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1316" n="HIAT:w" s="T369">hubu</ts>
                  <nts id="Seg_1317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1319" n="HIAT:w" s="T370">kördük</ts>
                  <nts id="Seg_1320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1322" n="HIAT:w" s="T371">hɨtɨllar</ts>
                  <nts id="Seg_1323" n="HIAT:ip">,</nts>
                  <nts id="Seg_1324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1326" n="HIAT:w" s="T372">balɨktara</ts>
                  <nts id="Seg_1327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1329" n="HIAT:w" s="T373">hubu</ts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1332" n="HIAT:w" s="T374">bultammɨt</ts>
                  <nts id="Seg_1333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1335" n="HIAT:w" s="T375">kurduk</ts>
                  <nts id="Seg_1336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1338" n="HIAT:w" s="T376">hɨllʼar</ts>
                  <nts id="Seg_1339" n="HIAT:ip">.</nts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T382" id="Seg_1342" n="HIAT:u" s="T377">
                  <ts e="T378" id="Seg_1344" n="HIAT:w" s="T377">Emeːksin</ts>
                  <nts id="Seg_1345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1347" n="HIAT:w" s="T378">onon</ts>
                  <nts id="Seg_1348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1350" n="HIAT:w" s="T379">bajan-tajan</ts>
                  <nts id="Seg_1351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380.tx.1" id="Seg_1353" n="HIAT:w" s="T380">hu͡oč</ts>
                  <nts id="Seg_1354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1356" n="HIAT:w" s="T380.tx.1">hogotok</ts>
                  <nts id="Seg_1357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1359" n="HIAT:w" s="T381">oloror</ts>
                  <nts id="Seg_1360" n="HIAT:ip">.</nts>
                  <nts id="Seg_1361" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T387" id="Seg_1363" n="HIAT:u" s="T382">
                  <ts e="T383" id="Seg_1365" n="HIAT:w" s="T382">Innʼe</ts>
                  <nts id="Seg_1366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1368" n="HIAT:w" s="T383">gɨnan</ts>
                  <nts id="Seg_1369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1371" n="HIAT:w" s="T384">emeːksin</ts>
                  <nts id="Seg_1372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1374" n="HIAT:w" s="T385">duːmata</ts>
                  <nts id="Seg_1375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1377" n="HIAT:w" s="T386">kelbit</ts>
                  <nts id="Seg_1378" n="HIAT:ip">.</nts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T395" id="Seg_1381" n="HIAT:u" s="T387">
                  <nts id="Seg_1382" n="HIAT:ip">"</nts>
                  <ts e="T388" id="Seg_1384" n="HIAT:w" s="T387">Ogoloːr</ts>
                  <nts id="Seg_1385" n="HIAT:ip">,</nts>
                  <nts id="Seg_1386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1388" n="HIAT:w" s="T388">min</ts>
                  <nts id="Seg_1389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1391" n="HIAT:w" s="T389">oloru͡oktaːgar</ts>
                  <nts id="Seg_1392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1394" n="HIAT:w" s="T390">ogonnʼorum</ts>
                  <nts id="Seg_1395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1397" n="HIAT:w" s="T391">ɨratɨgar</ts>
                  <nts id="Seg_1398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1400" n="HIAT:w" s="T392">bilse</ts>
                  <nts id="Seg_1401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1403" n="HIAT:w" s="T393">barɨ͡ak</ts>
                  <nts id="Seg_1404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1406" n="HIAT:w" s="T394">baːrɨm</ts>
                  <nts id="Seg_1407" n="HIAT:ip">.</nts>
                  <nts id="Seg_1408" n="HIAT:ip">"</nts>
                  <nts id="Seg_1409" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T400" id="Seg_1411" n="HIAT:u" s="T395">
                  <ts e="T396" id="Seg_1413" n="HIAT:w" s="T395">Ühüs</ts>
                  <nts id="Seg_1414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1416" n="HIAT:w" s="T396">hɨlɨgar</ts>
                  <nts id="Seg_1417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1419" n="HIAT:w" s="T397">ogonnʼorugar</ts>
                  <nts id="Seg_1420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1422" n="HIAT:w" s="T398">baraːrɨ</ts>
                  <nts id="Seg_1423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1425" n="HIAT:w" s="T399">taŋɨnna</ts>
                  <nts id="Seg_1426" n="HIAT:ip">.</nts>
                  <nts id="Seg_1427" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T404" id="Seg_1429" n="HIAT:u" s="T400">
                  <ts e="T401" id="Seg_1431" n="HIAT:w" s="T400">Keli͡ebi</ts>
                  <nts id="Seg_1432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1434" n="HIAT:w" s="T401">bɨhɨtalaːn</ts>
                  <nts id="Seg_1435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1437" n="HIAT:w" s="T402">baran</ts>
                  <nts id="Seg_1438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1440" n="HIAT:w" s="T403">ɨlla</ts>
                  <nts id="Seg_1441" n="HIAT:ip">.</nts>
                  <nts id="Seg_1442" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T410" id="Seg_1444" n="HIAT:u" s="T404">
                  <ts e="T405" id="Seg_1446" n="HIAT:w" s="T404">Ol</ts>
                  <nts id="Seg_1447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1449" n="HIAT:w" s="T405">gɨnan</ts>
                  <nts id="Seg_1450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1452" n="HIAT:w" s="T406">baran</ts>
                  <nts id="Seg_1453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1455" n="HIAT:w" s="T407">kɨːl</ts>
                  <nts id="Seg_1456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1458" n="HIAT:w" s="T408">hɨ͡atɨn</ts>
                  <nts id="Seg_1459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1461" n="HIAT:w" s="T409">ildʼi͡ehi</ts>
                  <nts id="Seg_1462" n="HIAT:ip">.</nts>
                  <nts id="Seg_1463" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T415" id="Seg_1465" n="HIAT:u" s="T410">
                  <ts e="T411" id="Seg_1467" n="HIAT:w" s="T410">Innʼe</ts>
                  <nts id="Seg_1468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1470" n="HIAT:w" s="T411">gɨnan</ts>
                  <nts id="Seg_1471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1473" n="HIAT:w" s="T412">baran</ts>
                  <nts id="Seg_1474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1476" n="HIAT:w" s="T413">ogonnʼorugar</ts>
                  <nts id="Seg_1477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1479" n="HIAT:w" s="T414">barda</ts>
                  <nts id="Seg_1480" n="HIAT:ip">.</nts>
                  <nts id="Seg_1481" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T421" id="Seg_1483" n="HIAT:u" s="T415">
                  <ts e="T416" id="Seg_1485" n="HIAT:w" s="T415">Bu</ts>
                  <nts id="Seg_1486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1488" n="HIAT:w" s="T416">baran</ts>
                  <nts id="Seg_1489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1491" n="HIAT:w" s="T417">baran</ts>
                  <nts id="Seg_1492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1494" n="HIAT:w" s="T418">ogonnʼorun</ts>
                  <nts id="Seg_1495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1497" n="HIAT:w" s="T419">golomotugar</ts>
                  <nts id="Seg_1498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1500" n="HIAT:w" s="T420">tijbit</ts>
                  <nts id="Seg_1501" n="HIAT:ip">.</nts>
                  <nts id="Seg_1502" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T430" id="Seg_1504" n="HIAT:u" s="T421">
                  <ts e="T422" id="Seg_1506" n="HIAT:w" s="T421">Araj</ts>
                  <nts id="Seg_1507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1509" n="HIAT:w" s="T422">tu͡okkaːn</ts>
                  <nts id="Seg_1510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1512" n="HIAT:w" s="T423">du</ts>
                  <nts id="Seg_1513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1515" n="HIAT:w" s="T424">hu͡ok</ts>
                  <nts id="Seg_1516" n="HIAT:ip">,</nts>
                  <nts id="Seg_1517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1519" n="HIAT:w" s="T425">bütünnüː</ts>
                  <nts id="Seg_1520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1522" n="HIAT:w" s="T426">barɨta</ts>
                  <nts id="Seg_1523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1525" n="HIAT:w" s="T427">tibillen</ts>
                  <nts id="Seg_1526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1528" n="HIAT:w" s="T428">huːlan</ts>
                  <nts id="Seg_1529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1531" n="HIAT:w" s="T429">kaːlbɨt</ts>
                  <nts id="Seg_1532" n="HIAT:ip">.</nts>
                  <nts id="Seg_1533" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T435" id="Seg_1535" n="HIAT:u" s="T430">
                  <ts e="T431" id="Seg_1537" n="HIAT:w" s="T430">Ol</ts>
                  <nts id="Seg_1538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1540" n="HIAT:w" s="T431">da</ts>
                  <nts id="Seg_1541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1543" n="HIAT:w" s="T432">bu͡ollar</ts>
                  <nts id="Seg_1544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1546" n="HIAT:w" s="T433">üːlehiger</ts>
                  <nts id="Seg_1547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1549" n="HIAT:w" s="T434">ɨttɨbɨt</ts>
                  <nts id="Seg_1550" n="HIAT:ip">.</nts>
                  <nts id="Seg_1551" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T453" id="Seg_1553" n="HIAT:u" s="T435">
                  <ts e="T436" id="Seg_1555" n="HIAT:w" s="T435">Araj</ts>
                  <nts id="Seg_1556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1558" n="HIAT:w" s="T436">ačaːgɨn</ts>
                  <nts id="Seg_1559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1561" n="HIAT:w" s="T437">tügürüččü</ts>
                  <nts id="Seg_1562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1564" n="HIAT:w" s="T438">üs</ts>
                  <nts id="Seg_1565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1567" n="HIAT:w" s="T439">tüːleːk</ts>
                  <nts id="Seg_1568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1570" n="HIAT:w" s="T440">kürdʼege</ts>
                  <nts id="Seg_1571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1573" n="HIAT:w" s="T441">bu͡olan</ts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1576" n="HIAT:w" s="T442">dʼaːbɨlana</ts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1579" n="HIAT:w" s="T443">hɨtar</ts>
                  <nts id="Seg_1580" n="HIAT:ip">,</nts>
                  <nts id="Seg_1581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1583" n="HIAT:w" s="T444">atɨn</ts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1586" n="HIAT:w" s="T445">kara</ts>
                  <nts id="Seg_1587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1589" n="HIAT:w" s="T446">tujagɨn</ts>
                  <nts id="Seg_1590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1592" n="HIAT:w" s="T447">gɨtta</ts>
                  <nts id="Seg_1593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1595" n="HIAT:w" s="T448">büttehin</ts>
                  <nts id="Seg_1596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1598" n="HIAT:w" s="T449">ere</ts>
                  <nts id="Seg_1599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1601" n="HIAT:w" s="T450">ordorbut</ts>
                  <nts id="Seg_1602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1604" n="HIAT:w" s="T451">atɨn</ts>
                  <nts id="Seg_1605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1607" n="HIAT:w" s="T452">gi͡enin</ts>
                  <nts id="Seg_1608" n="HIAT:ip">.</nts>
                  <nts id="Seg_1609" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T457" id="Seg_1611" n="HIAT:u" s="T453">
                  <ts e="T454" id="Seg_1613" n="HIAT:w" s="T453">Maːgɨŋ</ts>
                  <nts id="Seg_1614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1616" n="HIAT:w" s="T454">kubulujbut</ts>
                  <nts id="Seg_1617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1619" n="HIAT:w" s="T455">obu͡oja</ts>
                  <nts id="Seg_1620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1622" n="HIAT:w" s="T456">ühü</ts>
                  <nts id="Seg_1623" n="HIAT:ip">.</nts>
                  <nts id="Seg_1624" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T462" id="Seg_1626" n="HIAT:u" s="T457">
                  <ts e="T458" id="Seg_1628" n="HIAT:w" s="T457">Ol</ts>
                  <nts id="Seg_1629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1631" n="HIAT:w" s="T458">ihin</ts>
                  <nts id="Seg_1632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1634" n="HIAT:w" s="T459">ölümeːri</ts>
                  <nts id="Seg_1635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1637" n="HIAT:w" s="T460">kubulujbuta</ts>
                  <nts id="Seg_1638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1640" n="HIAT:w" s="T461">ühü</ts>
                  <nts id="Seg_1641" n="HIAT:ip">.</nts>
                  <nts id="Seg_1642" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T467" id="Seg_1644" n="HIAT:u" s="T462">
                  <ts e="T463" id="Seg_1646" n="HIAT:w" s="T462">Dʼe</ts>
                  <nts id="Seg_1647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1649" n="HIAT:w" s="T463">onu</ts>
                  <nts id="Seg_1650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1652" n="HIAT:w" s="T464">baː</ts>
                  <nts id="Seg_1653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1655" n="HIAT:w" s="T465">kili͡ebinen</ts>
                  <nts id="Seg_1656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1658" n="HIAT:w" s="T466">bɨrakpɨt</ts>
                  <nts id="Seg_1659" n="HIAT:ip">.</nts>
                  <nts id="Seg_1660" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T472" id="Seg_1662" n="HIAT:u" s="T467">
                  <ts e="T468" id="Seg_1664" n="HIAT:w" s="T467">Manɨ</ts>
                  <nts id="Seg_1665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1667" n="HIAT:w" s="T468">türdes</ts>
                  <nts id="Seg_1668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1670" n="HIAT:w" s="T469">gɨmmɨt</ts>
                  <nts id="Seg_1671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1673" n="HIAT:w" s="T470">daː</ts>
                  <nts id="Seg_1674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1676" n="HIAT:w" s="T471">eppit</ts>
                  <nts id="Seg_1677" n="HIAT:ip">:</nts>
                  <nts id="Seg_1678" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T478" id="Seg_1680" n="HIAT:u" s="T472">
                  <nts id="Seg_1681" n="HIAT:ip">"</nts>
                  <ts e="T473" id="Seg_1683" n="HIAT:w" s="T472">Baː</ts>
                  <nts id="Seg_1684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1686" n="HIAT:w" s="T473">ohogum</ts>
                  <nts id="Seg_1687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1689" n="HIAT:w" s="T474">bu͡ora</ts>
                  <nts id="Seg_1690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1692" n="HIAT:w" s="T475">hɨtɨ͡arɨmaːrɨ</ts>
                  <nts id="Seg_1693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1695" n="HIAT:w" s="T476">gɨmmɨt</ts>
                  <nts id="Seg_1696" n="HIAT:ip">"</nts>
                  <nts id="Seg_1697" n="HIAT:ip">,</nts>
                  <nts id="Seg_1698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1700" n="HIAT:w" s="T477">diːr</ts>
                  <nts id="Seg_1701" n="HIAT:ip">.</nts>
                  <nts id="Seg_1702" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T480" id="Seg_1704" n="HIAT:u" s="T478">
                  <ts e="T479" id="Seg_1706" n="HIAT:w" s="T478">Hɨ͡annan</ts>
                  <nts id="Seg_1707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1709" n="HIAT:w" s="T479">bɨrakpɨt</ts>
                  <nts id="Seg_1710" n="HIAT:ip">.</nts>
                  <nts id="Seg_1711" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T485" id="Seg_1713" n="HIAT:u" s="T480">
                  <nts id="Seg_1714" n="HIAT:ip">"</nts>
                  <ts e="T481" id="Seg_1716" n="HIAT:w" s="T480">ɨčča</ts>
                  <nts id="Seg_1717" n="HIAT:ip">,</nts>
                  <nts id="Seg_1718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1720" n="HIAT:w" s="T481">koŋuraktar</ts>
                  <nts id="Seg_1721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1723" n="HIAT:w" s="T482">tühenner</ts>
                  <nts id="Seg_1724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1726" n="HIAT:w" s="T483">hɨtɨ͡arɨmaːrɨ</ts>
                  <nts id="Seg_1727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1729" n="HIAT:w" s="T484">gɨnnɨlar</ts>
                  <nts id="Seg_1730" n="HIAT:ip">.</nts>
                  <nts id="Seg_1731" n="HIAT:ip">"</nts>
                  <nts id="Seg_1732" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T487" id="Seg_1734" n="HIAT:u" s="T485">
                  <ts e="T486" id="Seg_1736" n="HIAT:w" s="T485">Emi͡e</ts>
                  <nts id="Seg_1737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1739" n="HIAT:w" s="T486">bɨrakpɨt</ts>
                  <nts id="Seg_1740" n="HIAT:ip">.</nts>
                  <nts id="Seg_1741" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T492" id="Seg_1743" n="HIAT:u" s="T487">
                  <ts e="T488" id="Seg_1745" n="HIAT:w" s="T487">Manɨ</ts>
                  <nts id="Seg_1746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1748" n="HIAT:w" s="T488">kiren</ts>
                  <nts id="Seg_1749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1751" n="HIAT:w" s="T489">körbüt</ts>
                  <nts id="Seg_1752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1753" n="HIAT:ip">—</nts>
                  <nts id="Seg_1754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1756" n="HIAT:w" s="T490">hɨ͡a</ts>
                  <nts id="Seg_1757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1759" n="HIAT:w" s="T491">ebit</ts>
                  <nts id="Seg_1760" n="HIAT:ip">.</nts>
                  <nts id="Seg_1761" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T497" id="Seg_1763" n="HIAT:u" s="T492">
                  <ts e="T493" id="Seg_1765" n="HIAT:w" s="T492">Onu</ts>
                  <nts id="Seg_1766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1767" n="HIAT:ip">"</nts>
                  <ts e="T494" id="Seg_1769" n="HIAT:w" s="T493">taŋara</ts>
                  <nts id="Seg_1770" n="HIAT:ip">,</nts>
                  <nts id="Seg_1771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1773" n="HIAT:w" s="T494">taŋara</ts>
                  <nts id="Seg_1774" n="HIAT:ip">,</nts>
                  <nts id="Seg_1775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1777" n="HIAT:w" s="T495">čočumčakeːn</ts>
                  <nts id="Seg_1778" n="HIAT:ip">"</nts>
                  <nts id="Seg_1779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1781" n="HIAT:w" s="T496">di͡ebit</ts>
                  <nts id="Seg_1782" n="HIAT:ip">.</nts>
                  <nts id="Seg_1783" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T503" id="Seg_1785" n="HIAT:u" s="T497">
                  <nts id="Seg_1786" n="HIAT:ip">"</nts>
                  <ts e="T498" id="Seg_1788" n="HIAT:w" s="T497">Kaja</ts>
                  <nts id="Seg_1789" n="HIAT:ip">,</nts>
                  <nts id="Seg_1790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1792" n="HIAT:w" s="T498">ogonnʼor</ts>
                  <nts id="Seg_1793" n="HIAT:ip">,</nts>
                  <nts id="Seg_1794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1796" n="HIAT:w" s="T499">aːŋŋɨn</ts>
                  <nts id="Seg_1797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1799" n="HIAT:w" s="T500">arɨj</ts>
                  <nts id="Seg_1800" n="HIAT:ip">"</nts>
                  <nts id="Seg_1801" n="HIAT:ip">,</nts>
                  <nts id="Seg_1802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1804" n="HIAT:w" s="T501">di͡ebit</ts>
                  <nts id="Seg_1805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1807" n="HIAT:w" s="T502">emeːksine</ts>
                  <nts id="Seg_1808" n="HIAT:ip">.</nts>
                  <nts id="Seg_1809" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T506" id="Seg_1811" n="HIAT:u" s="T503">
                  <ts e="T504" id="Seg_1813" n="HIAT:w" s="T503">Aːnɨn</ts>
                  <nts id="Seg_1814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1816" n="HIAT:w" s="T504">arɨjan</ts>
                  <nts id="Seg_1817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1819" n="HIAT:w" s="T505">obu͡ojdammɨt</ts>
                  <nts id="Seg_1820" n="HIAT:ip">.</nts>
                  <nts id="Seg_1821" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T511" id="Seg_1823" n="HIAT:u" s="T506">
                  <ts e="T507" id="Seg_1825" n="HIAT:w" s="T506">Emeːksin</ts>
                  <nts id="Seg_1826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1828" n="HIAT:w" s="T507">ahatan</ts>
                  <nts id="Seg_1829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1831" n="HIAT:w" s="T508">baran</ts>
                  <nts id="Seg_1832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1834" n="HIAT:w" s="T509">dʼi͡etiger</ts>
                  <nts id="Seg_1835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1837" n="HIAT:w" s="T510">ilpit</ts>
                  <nts id="Seg_1838" n="HIAT:ip">.</nts>
                  <nts id="Seg_1839" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T524" id="Seg_1841" n="HIAT:u" s="T511">
                  <ts e="T512" id="Seg_1843" n="HIAT:w" s="T511">Bu</ts>
                  <nts id="Seg_1844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1846" n="HIAT:w" s="T512">dʼi͡etiger</ts>
                  <nts id="Seg_1847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1849" n="HIAT:w" s="T513">ildʼen</ts>
                  <nts id="Seg_1850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1852" n="HIAT:w" s="T514">huːjan-taraːn</ts>
                  <nts id="Seg_1853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1855" n="HIAT:w" s="T515">baran</ts>
                  <nts id="Seg_1856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1858" n="HIAT:w" s="T516">tannɨbatak</ts>
                  <nts id="Seg_1859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1861" n="HIAT:w" s="T517">taŋahɨ</ts>
                  <nts id="Seg_1862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1864" n="HIAT:w" s="T518">taŋɨnnaran</ts>
                  <nts id="Seg_1865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1867" n="HIAT:w" s="T519">baran</ts>
                  <nts id="Seg_1868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1870" n="HIAT:w" s="T520">taːs</ts>
                  <nts id="Seg_1871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1873" n="HIAT:w" s="T521">kɨrabaːkka</ts>
                  <nts id="Seg_1874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1876" n="HIAT:w" s="T522">olordon</ts>
                  <nts id="Seg_1877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1879" n="HIAT:w" s="T523">keːspit</ts>
                  <nts id="Seg_1880" n="HIAT:ip">.</nts>
                  <nts id="Seg_1881" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T537" id="Seg_1883" n="HIAT:u" s="T524">
                  <ts e="T525" id="Seg_1885" n="HIAT:w" s="T524">Ol</ts>
                  <nts id="Seg_1886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1888" n="HIAT:w" s="T525">gɨnan</ts>
                  <nts id="Seg_1889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1891" n="HIAT:w" s="T526">baran</ts>
                  <nts id="Seg_1892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1894" n="HIAT:w" s="T527">čaːj</ts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1897" n="HIAT:w" s="T528">iherpit</ts>
                  <nts id="Seg_1898" n="HIAT:ip">,</nts>
                  <nts id="Seg_1899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_1901" n="HIAT:w" s="T529">onton</ts>
                  <nts id="Seg_1902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_1904" n="HIAT:w" s="T530">et</ts>
                  <nts id="Seg_1905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_1907" n="HIAT:w" s="T531">kü͡ös</ts>
                  <nts id="Seg_1908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_1910" n="HIAT:w" s="T532">kotorbut</ts>
                  <nts id="Seg_1911" n="HIAT:ip">,</nts>
                  <nts id="Seg_1912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1914" n="HIAT:w" s="T533">hɨ͡alaːk</ts>
                  <nts id="Seg_1915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_1917" n="HIAT:w" s="T534">mini</ts>
                  <nts id="Seg_1918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1920" n="HIAT:w" s="T535">kɨtɨjaga</ts>
                  <nts id="Seg_1921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_1923" n="HIAT:w" s="T536">kotorbut</ts>
                  <nts id="Seg_1924" n="HIAT:ip">.</nts>
                  <nts id="Seg_1925" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T543" id="Seg_1927" n="HIAT:u" s="T537">
                  <ts e="T538" id="Seg_1929" n="HIAT:w" s="T537">Innʼe</ts>
                  <nts id="Seg_1930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1932" n="HIAT:w" s="T538">gɨnan</ts>
                  <nts id="Seg_1933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1935" n="HIAT:w" s="T539">kömüs</ts>
                  <nts id="Seg_1936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_1938" n="HIAT:w" s="T540">lu͡osku</ts>
                  <nts id="Seg_1939" n="HIAT:ip">,</nts>
                  <nts id="Seg_1940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_1942" n="HIAT:w" s="T541">biːlke</ts>
                  <nts id="Seg_1943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_1945" n="HIAT:w" s="T542">uːrbut</ts>
                  <nts id="Seg_1946" n="HIAT:ip">.</nts>
                  <nts id="Seg_1947" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T547" id="Seg_1949" n="HIAT:u" s="T543">
                  <ts e="T544" id="Seg_1951" n="HIAT:w" s="T543">Araj</ts>
                  <nts id="Seg_1952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_1954" n="HIAT:w" s="T544">lu͡oskutunan</ts>
                  <nts id="Seg_1955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_1957" n="HIAT:w" s="T545">iheːri</ts>
                  <nts id="Seg_1958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_1960" n="HIAT:w" s="T546">gɨmmɨt</ts>
                  <nts id="Seg_1961" n="HIAT:ip">:</nts>
                  <nts id="Seg_1962" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T558" id="Seg_1964" n="HIAT:u" s="T547">
                  <nts id="Seg_1965" n="HIAT:ip">"</nts>
                  <ts e="T548" id="Seg_1967" n="HIAT:w" s="T547">Kaja</ts>
                  <nts id="Seg_1968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_1970" n="HIAT:w" s="T548">ba-ba</ts>
                  <nts id="Seg_1971" n="HIAT:ip">,</nts>
                  <nts id="Seg_1972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_1974" n="HIAT:w" s="T549">lu͡oskum</ts>
                  <nts id="Seg_1975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_1977" n="HIAT:w" s="T550">dʼogus</ts>
                  <nts id="Seg_1978" n="HIAT:ip">,</nts>
                  <nts id="Seg_1979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_1981" n="HIAT:w" s="T551">ulakan</ts>
                  <nts id="Seg_1982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_1984" n="HIAT:w" s="T552">pabaraːŋkɨta</ts>
                  <nts id="Seg_1985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_1987" n="HIAT:w" s="T553">duː</ts>
                  <nts id="Seg_1988" n="HIAT:ip">,</nts>
                  <nts id="Seg_1989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_1991" n="HIAT:w" s="T554">komu͡osta</ts>
                  <nts id="Seg_1992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_1994" n="HIAT:w" s="T555">duː</ts>
                  <nts id="Seg_1995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_1997" n="HIAT:w" s="T556">egel</ts>
                  <nts id="Seg_1998" n="HIAT:ip">"</nts>
                  <nts id="Seg_1999" n="HIAT:ip">,</nts>
                  <nts id="Seg_2000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2002" n="HIAT:w" s="T557">di͡ebit</ts>
                  <nts id="Seg_2003" n="HIAT:ip">.</nts>
                  <nts id="Seg_2004" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T563" id="Seg_2006" n="HIAT:u" s="T558">
                  <ts e="T559" id="Seg_2008" n="HIAT:w" s="T558">Maːgɨta</ts>
                  <nts id="Seg_2009" n="HIAT:ip">,</nts>
                  <nts id="Seg_2010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_2012" n="HIAT:w" s="T559">komu͡os</ts>
                  <nts id="Seg_2013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2015" n="HIAT:w" s="T560">bagajɨta</ts>
                  <nts id="Seg_2016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2018" n="HIAT:w" s="T561">tuttaran</ts>
                  <nts id="Seg_2019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2021" n="HIAT:w" s="T562">keːspit</ts>
                  <nts id="Seg_2022" n="HIAT:ip">.</nts>
                  <nts id="Seg_2023" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T568" id="Seg_2025" n="HIAT:u" s="T563">
                  <ts e="T564" id="Seg_2027" n="HIAT:w" s="T563">Hɨ͡alaːk</ts>
                  <nts id="Seg_2028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2030" n="HIAT:w" s="T564">mini</ts>
                  <nts id="Seg_2031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2033" n="HIAT:w" s="T565">baːtɨnan</ts>
                  <nts id="Seg_2034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2035" n="HIAT:ip">"</nts>
                  <ts e="T567" id="Seg_2037" n="HIAT:w" s="T566">huːp</ts>
                  <nts id="Seg_2038" n="HIAT:ip">"</nts>
                  <nts id="Seg_2039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2041" n="HIAT:w" s="T567">gɨmmɨt</ts>
                  <nts id="Seg_2042" n="HIAT:ip">.</nts>
                  <nts id="Seg_2043" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T572" id="Seg_2045" n="HIAT:u" s="T568">
                  <ts e="T569" id="Seg_2047" n="HIAT:w" s="T568">Manɨ͡aka</ts>
                  <nts id="Seg_2048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2050" n="HIAT:w" s="T569">dʼe</ts>
                  <nts id="Seg_2051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2053" n="HIAT:w" s="T570">čačajar</ts>
                  <nts id="Seg_2054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2056" n="HIAT:w" s="T571">diː</ts>
                  <nts id="Seg_2057" n="HIAT:ip">.</nts>
                  <nts id="Seg_2058" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T591" id="Seg_2060" n="HIAT:u" s="T572">
                  <ts e="T573" id="Seg_2062" n="HIAT:w" s="T572">Dʼe</ts>
                  <nts id="Seg_2063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2065" n="HIAT:w" s="T573">onton</ts>
                  <nts id="Seg_2066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2067" n="HIAT:ip">"</nts>
                  <ts e="T575" id="Seg_2069" n="HIAT:w" s="T574">čačajabɨn</ts>
                  <nts id="Seg_2070" n="HIAT:ip">"</nts>
                  <nts id="Seg_2071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2073" n="HIAT:w" s="T575">di͡en</ts>
                  <nts id="Seg_2074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2076" n="HIAT:w" s="T576">dʼe</ts>
                  <nts id="Seg_2077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2079" n="HIAT:w" s="T577">uturuktuːr</ts>
                  <nts id="Seg_2080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2082" n="HIAT:w" s="T578">diː</ts>
                  <nts id="Seg_2083" n="HIAT:ip">,</nts>
                  <nts id="Seg_2084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2086" n="HIAT:w" s="T579">taːs</ts>
                  <nts id="Seg_2087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2089" n="HIAT:w" s="T580">olbogun</ts>
                  <nts id="Seg_2090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2092" n="HIAT:w" s="T581">bɨha</ts>
                  <nts id="Seg_2093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2095" n="HIAT:w" s="T582">uturuktuːr</ts>
                  <nts id="Seg_2096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2098" n="HIAT:w" s="T583">daː</ts>
                  <nts id="Seg_2099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2101" n="HIAT:w" s="T584">onon</ts>
                  <nts id="Seg_2102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2104" n="HIAT:w" s="T585">üs</ts>
                  <nts id="Seg_2105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2107" n="HIAT:w" s="T586">buluːs</ts>
                  <nts id="Seg_2108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2110" n="HIAT:w" s="T587">hiri</ts>
                  <nts id="Seg_2111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2113" n="HIAT:w" s="T588">tobulu</ts>
                  <nts id="Seg_2114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2116" n="HIAT:w" s="T589">tuhen</ts>
                  <nts id="Seg_2117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2119" n="HIAT:w" s="T590">kaːlar</ts>
                  <nts id="Seg_2120" n="HIAT:ip">.</nts>
                  <nts id="Seg_2121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T600" id="Seg_2123" n="HIAT:u" s="T591">
                  <ts e="T592" id="Seg_2125" n="HIAT:w" s="T591">Emeːksine</ts>
                  <nts id="Seg_2126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2128" n="HIAT:w" s="T592">tu͡ok</ts>
                  <nts id="Seg_2129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2131" n="HIAT:w" s="T593">bu͡olu͡ogaj</ts>
                  <nts id="Seg_2132" n="HIAT:ip">,</nts>
                  <nts id="Seg_2133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2135" n="HIAT:w" s="T594">kajdak</ts>
                  <nts id="Seg_2136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2138" n="HIAT:w" s="T595">olorbuta</ts>
                  <nts id="Seg_2139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2141" n="HIAT:w" s="T596">daː</ts>
                  <nts id="Seg_2142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2144" n="HIAT:w" s="T597">ogurduk</ts>
                  <nts id="Seg_2145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2147" n="HIAT:w" s="T598">bi͡ek</ts>
                  <nts id="Seg_2148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2150" n="HIAT:w" s="T601">bi͡ek</ts>
                  <nts id="Seg_2151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2153" n="HIAT:w" s="T599">oloror</ts>
                  <nts id="Seg_2154" n="HIAT:ip">.</nts>
                  <nts id="Seg_2155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T600" id="Seg_2156" n="sc" s="T0">
               <ts e="T1" id="Seg_2158" n="e" s="T0">Ogonnʼordoːk </ts>
               <ts e="T2" id="Seg_2160" n="e" s="T1">emeːksin </ts>
               <ts e="T3" id="Seg_2162" n="e" s="T2">olorbuttar </ts>
               <ts e="T4" id="Seg_2164" n="e" s="T3">ühü. </ts>
               <ts e="T5" id="Seg_2166" n="e" s="T4">Törötör </ts>
               <ts e="T6" id="Seg_2168" n="e" s="T5">ogoto </ts>
               <ts e="T7" id="Seg_2170" n="e" s="T6">hu͡oktar. </ts>
               <ts e="T8" id="Seg_2172" n="e" s="T7">Dʼi͡ege </ts>
               <ts e="T9" id="Seg_2174" n="e" s="T8">olordoktoruna </ts>
               <ts e="T10" id="Seg_2176" n="e" s="T9">u͡ottara </ts>
               <ts e="T11" id="Seg_2178" n="e" s="T10">ere </ts>
               <ts e="T12" id="Seg_2180" n="e" s="T11">kɨtarar, </ts>
               <ts e="T13" id="Seg_2182" n="e" s="T12">ogurduk </ts>
               <ts e="T14" id="Seg_2184" n="e" s="T13">dʼadaŋɨlar. </ts>
               <ts e="T15" id="Seg_2186" n="e" s="T14">Tahaːra </ts>
               <ts e="T16" id="Seg_2188" n="e" s="T15">tagɨstaktarɨna </ts>
               <ts e="T17" id="Seg_2190" n="e" s="T16">kaːrdara </ts>
               <ts e="T18" id="Seg_2192" n="e" s="T17">ere </ts>
               <ts e="T19" id="Seg_2194" n="e" s="T18">turtajar. </ts>
               <ts e="T20" id="Seg_2196" n="e" s="T19">Tu͡oga </ts>
               <ts e="T21" id="Seg_2198" n="e" s="T20">daː </ts>
               <ts e="T22" id="Seg_2200" n="e" s="T21">hu͡oktar. </ts>
               <ts e="T23" id="Seg_2202" n="e" s="T22">Araj </ts>
               <ts e="T24" id="Seg_2204" n="e" s="T23">üptere </ts>
               <ts e="T25" id="Seg_2206" n="e" s="T24">di͡ennere </ts>
               <ts e="T26" id="Seg_2208" n="e" s="T25">hu͡os hogotok </ts>
               <ts e="T27" id="Seg_2210" n="e" s="T26">attaːktar. </ts>
               <ts e="T28" id="Seg_2212" n="e" s="T27">Onton </ts>
               <ts e="T29" id="Seg_2214" n="e" s="T28">bultanar </ts>
               <ts e="T30" id="Seg_2216" n="e" s="T29">tergettere </ts>
               <ts e="T31" id="Seg_2218" n="e" s="T30">üs </ts>
               <ts e="T32" id="Seg_2220" n="e" s="T31">taːstaːk </ts>
               <ts e="T33" id="Seg_2222" n="e" s="T32">ilimkeːnneːkter. </ts>
               <ts e="T34" id="Seg_2224" n="e" s="T33">Ontularɨgar </ts>
               <ts e="T35" id="Seg_2226" n="e" s="T34">uː </ts>
               <ts e="T36" id="Seg_2228" n="e" s="T35">kurdʼagata </ts>
               <ts e="T37" id="Seg_2230" n="e" s="T36">iːlinnegine </ts>
               <ts e="T38" id="Seg_2232" n="e" s="T37">onu </ts>
               <ts e="T39" id="Seg_2234" n="e" s="T38">hiːller </ts>
               <ts e="T40" id="Seg_2236" n="e" s="T39">üčehege </ts>
               <ts e="T41" id="Seg_2238" n="e" s="T40">ü͡ölen. </ts>
               <ts e="T42" id="Seg_2240" n="e" s="T41">Araj </ts>
               <ts e="T43" id="Seg_2242" n="e" s="T42">ölöːrü </ts>
               <ts e="T44" id="Seg_2244" n="e" s="T43">ü͡ökejbitter, </ts>
               <ts e="T45" id="Seg_2246" n="e" s="T44">bɨstaːrɨ </ts>
               <ts e="T46" id="Seg_2248" n="e" s="T45">bɨ͡akajbɨttar. </ts>
               <ts e="T47" id="Seg_2250" n="e" s="T46">Onton </ts>
               <ts e="T48" id="Seg_2252" n="e" s="T47">ogonnʼoro </ts>
               <ts e="T49" id="Seg_2254" n="e" s="T48">araj </ts>
               <ts e="T50" id="Seg_2256" n="e" s="T49">kiːrbit </ts>
               <ts e="T51" id="Seg_2258" n="e" s="T50">emeːksiniger. </ts>
               <ts e="T52" id="Seg_2260" n="e" s="T51">Dʼe </ts>
               <ts e="T53" id="Seg_2262" n="e" s="T52">kiːren </ts>
               <ts e="T54" id="Seg_2264" n="e" s="T53">dʼi͡ebit: </ts>
               <ts e="T55" id="Seg_2266" n="e" s="T54">"Kaja </ts>
               <ts e="T56" id="Seg_2268" n="e" s="T55">emeːksin </ts>
               <ts e="T57" id="Seg_2270" n="e" s="T56">ürdüːbüt. </ts>
               <ts e="T58" id="Seg_2272" n="e" s="T57">Kaːmtarɨ͡akpɨt </ts>
               <ts e="T59" id="Seg_2274" n="e" s="T58">hogotok </ts>
               <ts e="T60" id="Seg_2276" n="e" s="T59">attaːkpɨt, </ts>
               <ts e="T61" id="Seg_2278" n="e" s="T60">hi͡ekpit </ts>
               <ts e="T62" id="Seg_2280" n="e" s="T61">baːra, </ts>
               <ts e="T63" id="Seg_2282" n="e" s="T62">onnoːgor </ts>
               <ts e="T64" id="Seg_2284" n="e" s="T63">okton </ts>
               <ts e="T65" id="Seg_2286" n="e" s="T64">öllökpütüne </ts>
               <ts e="T66" id="Seg_2288" n="e" s="T65">kennibitiger </ts>
               <ts e="T67" id="Seg_2290" n="e" s="T66">kaːlɨ͡aga." </ts>
               <ts e="T68" id="Seg_2292" n="e" s="T67">Onuga </ts>
               <ts e="T69" id="Seg_2294" n="e" s="T68">emeːksin: </ts>
               <ts e="T70" id="Seg_2296" n="e" s="T69">"Bejeŋ </ts>
               <ts e="T71" id="Seg_2298" n="e" s="T70">hanaːŋ", </ts>
               <ts e="T72" id="Seg_2300" n="e" s="T71">di͡ebit. </ts>
               <ts e="T73" id="Seg_2302" n="e" s="T72">"Ölördörgün </ts>
               <ts e="T74" id="Seg_2304" n="e" s="T73">ere </ts>
               <ts e="T75" id="Seg_2306" n="e" s="T74">hi͡ekpit!" </ts>
               <ts e="T76" id="Seg_2308" n="e" s="T75">Ere </ts>
               <ts e="T77" id="Seg_2310" n="e" s="T76">tagɨspɨt </ts>
               <ts e="T78" id="Seg_2312" n="e" s="T77">daː </ts>
               <ts e="T79" id="Seg_2314" n="e" s="T78">ölörbüt. </ts>
               <ts e="T80" id="Seg_2316" n="e" s="T79">Ogonnʼoro </ts>
               <ts e="T81" id="Seg_2318" n="e" s="T80">hülbüt, </ts>
               <ts e="T82" id="Seg_2320" n="e" s="T81">araj </ts>
               <ts e="T83" id="Seg_2322" n="e" s="T82">tɨmnɨː </ts>
               <ts e="T84" id="Seg_2324" n="e" s="T83">bagajɨ, </ts>
               <ts e="T85" id="Seg_2326" n="e" s="T84">čɨskaːn </ts>
               <ts e="T86" id="Seg_2328" n="e" s="T85">bagajɨ, </ts>
               <ts e="T87" id="Seg_2330" n="e" s="T86">ogonnʼoro </ts>
               <ts e="T88" id="Seg_2332" n="e" s="T87">kiːrbit, </ts>
               <ts e="T89" id="Seg_2334" n="e" s="T88">kiːren </ts>
               <ts e="T90" id="Seg_2336" n="e" s="T89">di͡ebit: </ts>
               <ts e="T91" id="Seg_2338" n="e" s="T90">"Appɨt </ts>
               <ts e="T92" id="Seg_2340" n="e" s="T91">etin </ts>
               <ts e="T93" id="Seg_2342" n="e" s="T92">bütünnüː </ts>
               <ts e="T94" id="Seg_2344" n="e" s="T93">golomokoːmmut </ts>
               <ts e="T95" id="Seg_2346" n="e" s="T94">uŋu͡or </ts>
               <ts e="T96" id="Seg_2348" n="e" s="T95">taskajdaːn </ts>
               <ts e="T97" id="Seg_2350" n="e" s="T96">keːs </ts>
               <ts e="T98" id="Seg_2352" n="e" s="T97">čɨːstatɨn." </ts>
               <ts e="T99" id="Seg_2354" n="e" s="T98">Emeːksine </ts>
               <ts e="T100" id="Seg_2356" n="e" s="T99">barɨtɨn </ts>
               <ts e="T101" id="Seg_2358" n="e" s="T100">taskajdaːbɨt. </ts>
               <ts e="T102" id="Seg_2360" n="e" s="T101">Emeːksine </ts>
               <ts e="T103" id="Seg_2362" n="e" s="T102">iliːtin </ts>
               <ts e="T104" id="Seg_2364" n="e" s="T103">itteːri </ts>
               <ts e="T105" id="Seg_2366" n="e" s="T104">kɨmmɨtɨn </ts>
               <ts e="T106" id="Seg_2368" n="e" s="T105">ogonnʼor </ts>
               <ts e="T107" id="Seg_2370" n="e" s="T106">eppit: </ts>
               <ts e="T108" id="Seg_2372" n="e" s="T107">"Hi͡ekkin, </ts>
               <ts e="T109" id="Seg_2374" n="e" s="T108">barɨ </ts>
               <ts e="T110" id="Seg_2376" n="e" s="T109">ihikker </ts>
               <ts e="T111" id="Seg_2378" n="e" s="T110">holuːrgar </ts>
               <ts e="T112" id="Seg_2380" n="e" s="T111">uːta </ts>
               <ts e="T113" id="Seg_2382" n="e" s="T112">bahan </ts>
               <ts e="T114" id="Seg_2384" n="e" s="T113">tolor, </ts>
               <ts e="T115" id="Seg_2386" n="e" s="T114">belemneːn </ts>
               <ts e="T116" id="Seg_2388" n="e" s="T115">baran </ts>
               <ts e="T117" id="Seg_2390" n="e" s="T116">dʼe </ts>
               <ts e="T118" id="Seg_2392" n="e" s="T117">ahaːr." </ts>
               <ts e="T119" id="Seg_2394" n="e" s="T118">Innʼe </ts>
               <ts e="T120" id="Seg_2396" n="e" s="T119">gɨnan </ts>
               <ts e="T121" id="Seg_2398" n="e" s="T120">emeːksin </ts>
               <ts e="T122" id="Seg_2400" n="e" s="T121">taksan </ts>
               <ts e="T123" id="Seg_2402" n="e" s="T122">barɨ </ts>
               <ts e="T124" id="Seg_2404" n="e" s="T123">oŋkučagar </ts>
               <ts e="T125" id="Seg_2406" n="e" s="T124">barɨkakaːnnarɨgar </ts>
               <ts e="T126" id="Seg_2408" n="e" s="T125">uː </ts>
               <ts e="T127" id="Seg_2410" n="e" s="T126">baspɨt. </ts>
               <ts e="T128" id="Seg_2412" n="e" s="T127">Araj </ts>
               <ts e="T129" id="Seg_2414" n="e" s="T128">bütehik </ts>
               <ts e="T130" id="Seg_2416" n="e" s="T129">holuːrgakakkakaːnɨn </ts>
               <ts e="T131" id="Seg_2418" n="e" s="T130">egeleːri </ts>
               <ts e="T132" id="Seg_2420" n="e" s="T131">gɨmmɨta, </ts>
               <ts e="T133" id="Seg_2422" n="e" s="T132">aːna </ts>
               <ts e="T134" id="Seg_2424" n="e" s="T133">katɨːlaːk. </ts>
               <ts e="T135" id="Seg_2426" n="e" s="T134">"Kaja, </ts>
               <ts e="T136" id="Seg_2428" n="e" s="T135">ogonnʼor, </ts>
               <ts e="T137" id="Seg_2430" n="e" s="T136">aːŋŋɨn </ts>
               <ts e="T138" id="Seg_2432" n="e" s="T137">as, </ts>
               <ts e="T139" id="Seg_2434" n="e" s="T138">ülüjeːri </ts>
               <ts e="T140" id="Seg_2436" n="e" s="T139">gɨnnɨm", </ts>
               <ts e="T141" id="Seg_2438" n="e" s="T140">di͡ebit. </ts>
               <ts e="T142" id="Seg_2440" n="e" s="T141">"Huːka </ts>
               <ts e="T143" id="Seg_2442" n="e" s="T142">kɨːha, </ts>
               <ts e="T144" id="Seg_2444" n="e" s="T143">kaːrga </ts>
               <ts e="T145" id="Seg_2446" n="e" s="T144">kampɨ </ts>
               <ts e="T146" id="Seg_2448" n="e" s="T145">bɨrak", </ts>
               <ts e="T147" id="Seg_2450" n="e" s="T146">di͡ebit. </ts>
               <ts e="T148" id="Seg_2452" n="e" s="T147">Emeːksine </ts>
               <ts e="T149" id="Seg_2454" n="e" s="T148">baː </ts>
               <ts e="T150" id="Seg_2456" n="e" s="T149">holuːrčagɨn </ts>
               <ts e="T151" id="Seg_2458" n="e" s="T150">bɨraːn </ts>
               <ts e="T152" id="Seg_2460" n="e" s="T151">baran </ts>
               <ts e="T153" id="Seg_2462" n="e" s="T152">dʼe </ts>
               <ts e="T154" id="Seg_2464" n="e" s="T153">toŋon </ts>
               <ts e="T155" id="Seg_2466" n="e" s="T154">ölöːrü </ts>
               <ts e="T156" id="Seg_2468" n="e" s="T155">gɨnar. </ts>
               <ts e="T157" id="Seg_2470" n="e" s="T156">Emeːksine </ts>
               <ts e="T158" id="Seg_2472" n="e" s="T157">balaganɨn </ts>
               <ts e="T159" id="Seg_2474" n="e" s="T158">ürdüger </ts>
               <ts e="T160" id="Seg_2476" n="e" s="T159">taksan </ts>
               <ts e="T161" id="Seg_2478" n="e" s="T160">kɨːmkaːnnar </ts>
               <ts e="T162" id="Seg_2480" n="e" s="T161">taksɨːlarɨgar </ts>
               <ts e="T163" id="Seg_2482" n="e" s="T162">onno </ts>
               <ts e="T164" id="Seg_2484" n="e" s="T163">iliːtin </ts>
               <ts e="T165" id="Seg_2486" n="e" s="T164">itter. </ts>
               <ts e="T166" id="Seg_2488" n="e" s="T165">Araj </ts>
               <ts e="T167" id="Seg_2490" n="e" s="T166">erin </ts>
               <ts e="T168" id="Seg_2492" n="e" s="T167">körbüte, </ts>
               <ts e="T169" id="Seg_2494" n="e" s="T168">atɨn </ts>
               <ts e="T170" id="Seg_2496" n="e" s="T169">hamagɨn </ts>
               <ts e="T171" id="Seg_2498" n="e" s="T170">ɨlan </ts>
               <ts e="T172" id="Seg_2500" n="e" s="T171">baran </ts>
               <ts e="T173" id="Seg_2502" n="e" s="T172">alčajan </ts>
               <ts e="T174" id="Seg_2504" n="e" s="T173">baran </ts>
               <ts e="T175" id="Seg_2506" n="e" s="T174">hačalana </ts>
               <ts e="T176" id="Seg_2508" n="e" s="T175">oloror. </ts>
               <ts e="T177" id="Seg_2510" n="e" s="T176">Ol </ts>
               <ts e="T178" id="Seg_2512" n="e" s="T177">itteːri </ts>
               <ts e="T179" id="Seg_2514" n="e" s="T178">gɨnar </ts>
               <ts e="T180" id="Seg_2516" n="e" s="T179">emeːksinin </ts>
               <ts e="T181" id="Seg_2518" n="e" s="T180">iliːtin </ts>
               <ts e="T182" id="Seg_2520" n="e" s="T181">üčehennen </ts>
               <ts e="T183" id="Seg_2522" n="e" s="T182">bɨhɨta </ts>
               <ts e="T184" id="Seg_2524" n="e" s="T183">kejer. </ts>
               <ts e="T185" id="Seg_2526" n="e" s="T184">Ontuta </ts>
               <ts e="T186" id="Seg_2528" n="e" s="T185">ɨtɨː-ɨtɨː </ts>
               <ts e="T187" id="Seg_2530" n="e" s="T186">hirge </ts>
               <ts e="T188" id="Seg_2532" n="e" s="T187">tüheːkteːn </ts>
               <ts e="T189" id="Seg_2534" n="e" s="T188">baran </ts>
               <ts e="T190" id="Seg_2536" n="e" s="T189">mastɨːr </ts>
               <ts e="T191" id="Seg_2538" n="e" s="T190">hirkeːn </ts>
               <ts e="T192" id="Seg_2540" n="e" s="T191">di͡eg </ts>
               <ts e="T193" id="Seg_2542" n="e" s="T192">ɨtɨː-ɨtɨː </ts>
               <ts e="T194" id="Seg_2544" n="e" s="T193">kaːma </ts>
               <ts e="T195" id="Seg_2546" n="e" s="T194">turar. </ts>
               <ts e="T196" id="Seg_2548" n="e" s="T195">Dʼe </ts>
               <ts e="T197" id="Seg_2550" n="e" s="T196">onno </ts>
               <ts e="T198" id="Seg_2552" n="e" s="T197">tiːtikun </ts>
               <ts e="T199" id="Seg_2554" n="e" s="T198">mas </ts>
               <ts e="T200" id="Seg_2556" n="e" s="T199">tördüger </ts>
               <ts e="T201" id="Seg_2558" n="e" s="T200">hɨtaːrɨ </ts>
               <ts e="T202" id="Seg_2560" n="e" s="T201">gɨmmɨt, </ts>
               <ts e="T203" id="Seg_2562" n="e" s="T202">hɨtaːrɨ </ts>
               <ts e="T204" id="Seg_2564" n="e" s="T203">gɨmmɨt, </ts>
               <ts e="T205" id="Seg_2566" n="e" s="T204">tɨmnɨː </ts>
               <ts e="T206" id="Seg_2568" n="e" s="T205">titiriːr </ts>
               <ts e="T207" id="Seg_2570" n="e" s="T206">hüreger </ts>
               <ts e="T208" id="Seg_2572" n="e" s="T207">kiːrer. </ts>
               <ts e="T209" id="Seg_2574" n="e" s="T208">Ol </ts>
               <ts e="T210" id="Seg_2576" n="e" s="T209">hɨttagɨna </ts>
               <ts e="T211" id="Seg_2578" n="e" s="T210">ajdaːn </ts>
               <ts e="T212" id="Seg_2580" n="e" s="T211">bögö </ts>
               <ts e="T213" id="Seg_2582" n="e" s="T212">ihillibit, </ts>
               <ts e="T214" id="Seg_2584" n="e" s="T213">taba </ts>
               <ts e="T215" id="Seg_2586" n="e" s="T214">üːrerten </ts>
               <ts e="T216" id="Seg_2588" n="e" s="T215">taba </ts>
               <ts e="T217" id="Seg_2590" n="e" s="T216">üːrer </ts>
               <ts e="T218" id="Seg_2592" n="e" s="T217">enin </ts>
               <ts e="T219" id="Seg_2594" n="e" s="T218">bagajɨ. </ts>
               <ts e="T220" id="Seg_2596" n="e" s="T219">Ol </ts>
               <ts e="T221" id="Seg_2598" n="e" s="T220">kotu </ts>
               <ts e="T222" id="Seg_2600" n="e" s="T221">dʼe </ts>
               <ts e="T223" id="Seg_2602" n="e" s="T222">barar </ts>
               <ts e="T224" id="Seg_2604" n="e" s="T223">bu </ts>
               <ts e="T225" id="Seg_2606" n="e" s="T224">emeːksin. </ts>
               <ts e="T226" id="Seg_2608" n="e" s="T225">Ol </ts>
               <ts e="T227" id="Seg_2610" n="e" s="T226">baran, </ts>
               <ts e="T228" id="Seg_2612" n="e" s="T227">ajɨː </ts>
               <ts e="T229" id="Seg_2614" n="e" s="T228">daganɨ, </ts>
               <ts e="T230" id="Seg_2616" n="e" s="T229">ɨjɨ </ts>
               <ts e="T231" id="Seg_2618" n="e" s="T230">bü͡ölüːr </ts>
               <ts e="T232" id="Seg_2620" n="e" s="T231">ɨːs </ts>
               <ts e="T233" id="Seg_2622" n="e" s="T232">tuman </ts>
               <ts e="T234" id="Seg_2624" n="e" s="T233">tabaga, </ts>
               <ts e="T235" id="Seg_2626" n="e" s="T234">künü </ts>
               <ts e="T236" id="Seg_2628" n="e" s="T235">bü͡ölüːr </ts>
               <ts e="T237" id="Seg_2630" n="e" s="T236">kuden </ts>
               <ts e="T238" id="Seg_2632" n="e" s="T237">tuman </ts>
               <ts e="T239" id="Seg_2634" n="e" s="T238">hü͡öhüge </ts>
               <ts e="T240" id="Seg_2636" n="e" s="T239">tijer. </ts>
               <ts e="T241" id="Seg_2638" n="e" s="T240">Bu </ts>
               <ts e="T242" id="Seg_2640" n="e" s="T241">emeːksiniŋ </ts>
               <ts e="T243" id="Seg_2642" n="e" s="T242">dʼi͡eni </ts>
               <ts e="T244" id="Seg_2644" n="e" s="T243">kördöːn </ts>
               <ts e="T245" id="Seg_2646" n="e" s="T244">bu </ts>
               <ts e="T246" id="Seg_2648" n="e" s="T245">tabanɨ </ts>
               <ts e="T247" id="Seg_2650" n="e" s="T246">teleriger </ts>
               <ts e="T248" id="Seg_2652" n="e" s="T247">ikki </ts>
               <ts e="T249" id="Seg_2654" n="e" s="T248">eneːre </ts>
               <ts e="T250" id="Seg_2656" n="e" s="T249">elejen </ts>
               <ts e="T251" id="Seg_2658" n="e" s="T250">kaːlbɨt. </ts>
               <ts e="T252" id="Seg_2660" n="e" s="T251">Araj </ts>
               <ts e="T253" id="Seg_2662" n="e" s="T252">alɨːnɨ </ts>
               <ts e="T254" id="Seg_2664" n="e" s="T253">tobus toloru </ts>
               <ts e="T255" id="Seg_2666" n="e" s="T254">uraha </ts>
               <ts e="T256" id="Seg_2668" n="e" s="T255">dʼi͡e </ts>
               <ts e="T257" id="Seg_2670" n="e" s="T256">tutullan </ts>
               <ts e="T258" id="Seg_2672" n="e" s="T257">turar. </ts>
               <ts e="T259" id="Seg_2674" n="e" s="T258">Ulakan </ts>
               <ts e="T260" id="Seg_2676" n="e" s="T259">mɨjaː </ts>
               <ts e="T261" id="Seg_2678" n="e" s="T260">dʼi͡e </ts>
               <ts e="T262" id="Seg_2680" n="e" s="T261">kelle, </ts>
               <ts e="T263" id="Seg_2682" n="e" s="T262">bu </ts>
               <ts e="T264" id="Seg_2684" n="e" s="T263">dʼi͡ege, </ts>
               <ts e="T265" id="Seg_2686" n="e" s="T264">manna </ts>
               <ts e="T266" id="Seg_2688" n="e" s="T265">dʼe </ts>
               <ts e="T267" id="Seg_2690" n="e" s="T266">kas </ts>
               <ts e="T268" id="Seg_2692" n="e" s="T267">u͡on </ts>
               <ts e="T269" id="Seg_2694" n="e" s="T268">kihi </ts>
               <ts e="T270" id="Seg_2696" n="e" s="T269">bɨha </ts>
               <ts e="T271" id="Seg_2698" n="e" s="T270">ölbügüne, </ts>
               <ts e="T272" id="Seg_2700" n="e" s="T271">bu͡olaga </ts>
               <ts e="T273" id="Seg_2702" n="e" s="T272">huburujan </ts>
               <ts e="T274" id="Seg_2704" n="e" s="T273">turar. </ts>
               <ts e="T275" id="Seg_2706" n="e" s="T274">Innʼe </ts>
               <ts e="T276" id="Seg_2708" n="e" s="T275">gɨnan </ts>
               <ts e="T277" id="Seg_2710" n="e" s="T276">baː </ts>
               <ts e="T278" id="Seg_2712" n="e" s="T277">emeːksin </ts>
               <ts e="T279" id="Seg_2714" n="e" s="T278">baː </ts>
               <ts e="T280" id="Seg_2716" n="e" s="T279">dʼi͡eni </ts>
               <ts e="T281" id="Seg_2718" n="e" s="T280">arɨja </ts>
               <ts e="T282" id="Seg_2720" n="e" s="T281">oksubut. </ts>
               <ts e="T283" id="Seg_2722" n="e" s="T282">Oduːlaːn </ts>
               <ts e="T284" id="Seg_2724" n="e" s="T283">körbüte — </ts>
               <ts e="T285" id="Seg_2726" n="e" s="T284">ibis iččitek, </ts>
               <ts e="T286" id="Seg_2728" n="e" s="T285">kihite </ts>
               <ts e="T287" id="Seg_2730" n="e" s="T286">hu͡ok. </ts>
               <ts e="T288" id="Seg_2732" n="e" s="T287">Barɨ </ts>
               <ts e="T289" id="Seg_2734" n="e" s="T288">kü͡ös </ts>
               <ts e="T290" id="Seg_2736" n="e" s="T289">kü͡östenen </ts>
               <ts e="T291" id="Seg_2738" n="e" s="T290">turar, </ts>
               <ts e="T292" id="Seg_2740" n="e" s="T291">barɨ </ts>
               <ts e="T293" id="Seg_2742" n="e" s="T292">as </ts>
               <ts e="T294" id="Seg_2744" n="e" s="T293">astanan </ts>
               <ts e="T295" id="Seg_2746" n="e" s="T294">turar, </ts>
               <ts e="T296" id="Seg_2748" n="e" s="T295">taːs </ts>
               <ts e="T297" id="Seg_2750" n="e" s="T296">kɨrabaːttar </ts>
               <ts e="T298" id="Seg_2752" n="e" s="T297">bütünnüː </ts>
               <ts e="T299" id="Seg_2754" n="e" s="T298">onnuk </ts>
               <ts e="T300" id="Seg_2756" n="e" s="T299">tergetteːk </ts>
               <ts e="T301" id="Seg_2758" n="e" s="T300">dʼi͡e </ts>
               <ts e="T302" id="Seg_2760" n="e" s="T301">turar. </ts>
               <ts e="T303" id="Seg_2762" n="e" s="T302">Ol </ts>
               <ts e="T304" id="Seg_2764" n="e" s="T303">emeːksin </ts>
               <ts e="T305" id="Seg_2766" n="e" s="T304">kiːren </ts>
               <ts e="T306" id="Seg_2768" n="e" s="T305">ahaːbatagɨn </ts>
               <ts e="T307" id="Seg_2770" n="e" s="T306">ahɨːr, </ts>
               <ts e="T308" id="Seg_2772" n="e" s="T307">hi͡ebetegin </ts>
               <ts e="T309" id="Seg_2774" n="e" s="T308">hiːr, </ts>
               <ts e="T310" id="Seg_2776" n="e" s="T309">nuːčča </ts>
               <ts e="T311" id="Seg_2778" n="e" s="T310">daː </ts>
               <ts e="T312" id="Seg_2780" n="e" s="T311">aha </ts>
               <ts e="T313" id="Seg_2782" n="e" s="T312">ügüs, </ts>
               <ts e="T314" id="Seg_2784" n="e" s="T313">tɨ͡a </ts>
               <ts e="T315" id="Seg_2786" n="e" s="T314">daː </ts>
               <ts e="T316" id="Seg_2788" n="e" s="T315">aha </ts>
               <ts e="T317" id="Seg_2790" n="e" s="T316">ügüs. </ts>
               <ts e="T318" id="Seg_2792" n="e" s="T317">Bu </ts>
               <ts e="T319" id="Seg_2794" n="e" s="T318">emeːksin </ts>
               <ts e="T320" id="Seg_2796" n="e" s="T319">huːnan-taraːnan </ts>
               <ts e="T321" id="Seg_2798" n="e" s="T320">baran </ts>
               <ts e="T322" id="Seg_2800" n="e" s="T321">üːs-kiːs </ts>
               <ts e="T323" id="Seg_2802" n="e" s="T322">taŋahɨ </ts>
               <ts e="T324" id="Seg_2804" n="e" s="T323">taŋnan </ts>
               <ts e="T325" id="Seg_2806" n="e" s="T324">keːspit. </ts>
               <ts e="T326" id="Seg_2808" n="e" s="T325">Bu </ts>
               <ts e="T327" id="Seg_2810" n="e" s="T326">tulkarɨ </ts>
               <ts e="T328" id="Seg_2812" n="e" s="T327">tu͡ok </ts>
               <ts e="T329" id="Seg_2814" n="e" s="T328">daː </ts>
               <ts e="T330" id="Seg_2816" n="e" s="T329">kihite </ts>
               <ts e="T331" id="Seg_2818" n="e" s="T330">billibet. </ts>
               <ts e="T332" id="Seg_2820" n="e" s="T331">Üs </ts>
               <ts e="T333" id="Seg_2822" n="e" s="T332">künü </ts>
               <ts e="T334" id="Seg_2824" n="e" s="T333">meldʼi </ts>
               <ts e="T335" id="Seg_2826" n="e" s="T334">tu͡ok </ts>
               <ts e="T336" id="Seg_2828" n="e" s="T335">daː </ts>
               <ts e="T337" id="Seg_2830" n="e" s="T336">kihite </ts>
               <ts e="T338" id="Seg_2832" n="e" s="T337">billibet. </ts>
               <ts e="T339" id="Seg_2834" n="e" s="T338">Araj </ts>
               <ts e="T340" id="Seg_2836" n="e" s="T339">ki͡ehe </ts>
               <ts e="T341" id="Seg_2838" n="e" s="T340">bu͡olu͡o </ts>
               <ts e="T342" id="Seg_2840" n="e" s="T341">daː </ts>
               <ts e="T343" id="Seg_2842" n="e" s="T342">ü͡ölehinen </ts>
               <ts e="T344" id="Seg_2844" n="e" s="T343">kur </ts>
               <ts e="T345" id="Seg_2846" n="e" s="T344">kihi </ts>
               <ts e="T346" id="Seg_2848" n="e" s="T345">mejiːlere </ts>
               <ts e="T347" id="Seg_2850" n="e" s="T346">tuheller. </ts>
               <ts e="T348" id="Seg_2852" n="e" s="T347">Oloru </ts>
               <ts e="T349" id="Seg_2854" n="e" s="T348">"tehiŋ-buhuŋ" </ts>
               <ts e="T350" id="Seg_2856" n="e" s="T349">tehite </ts>
               <ts e="T351" id="Seg_2858" n="e" s="T350">bar </ts>
               <ts e="T352" id="Seg_2860" n="e" s="T351">diːdiː </ts>
               <ts e="T353" id="Seg_2862" n="e" s="T352">tehite </ts>
               <ts e="T354" id="Seg_2864" n="e" s="T353">üktüːr, </ts>
               <ts e="T355" id="Seg_2866" n="e" s="T354">oloro </ts>
               <ts e="T356" id="Seg_2868" n="e" s="T355">hir </ts>
               <ts e="T357" id="Seg_2870" n="e" s="T356">di͡eg </ts>
               <ts e="T358" id="Seg_2872" n="e" s="T357">himitten </ts>
               <ts e="T359" id="Seg_2874" n="e" s="T358">iheller. </ts>
               <ts e="T360" id="Seg_2876" n="e" s="T359">Emeːksin </ts>
               <ts e="T361" id="Seg_2878" n="e" s="T360">kah </ts>
               <ts e="T362" id="Seg_2880" n="e" s="T361">u͡on </ts>
               <ts e="T363" id="Seg_2882" n="e" s="T362">tukkarɨ </ts>
               <ts e="T364" id="Seg_2884" n="e" s="T363">olorbut, </ts>
               <ts e="T365" id="Seg_2886" n="e" s="T364">tabata </ts>
               <ts e="T366" id="Seg_2888" n="e" s="T365">kanna </ts>
               <ts e="T367" id="Seg_2890" n="e" s="T366">daː </ts>
               <ts e="T368" id="Seg_2892" n="e" s="T367">kamnaːbat, </ts>
               <ts e="T369" id="Seg_2894" n="e" s="T368">attara </ts>
               <ts e="T370" id="Seg_2896" n="e" s="T369">hubu </ts>
               <ts e="T371" id="Seg_2898" n="e" s="T370">kördük </ts>
               <ts e="T372" id="Seg_2900" n="e" s="T371">hɨtɨllar, </ts>
               <ts e="T373" id="Seg_2902" n="e" s="T372">balɨktara </ts>
               <ts e="T374" id="Seg_2904" n="e" s="T373">hubu </ts>
               <ts e="T375" id="Seg_2906" n="e" s="T374">bultammɨt </ts>
               <ts e="T376" id="Seg_2908" n="e" s="T375">kurduk </ts>
               <ts e="T377" id="Seg_2910" n="e" s="T376">hɨllʼar. </ts>
               <ts e="T378" id="Seg_2912" n="e" s="T377">Emeːksin </ts>
               <ts e="T379" id="Seg_2914" n="e" s="T378">onon </ts>
               <ts e="T380" id="Seg_2916" n="e" s="T379">bajan-tajan </ts>
               <ts e="T381" id="Seg_2918" n="e" s="T380">hu͡oč hogotok </ts>
               <ts e="T382" id="Seg_2920" n="e" s="T381">oloror. </ts>
               <ts e="T383" id="Seg_2922" n="e" s="T382">Innʼe </ts>
               <ts e="T384" id="Seg_2924" n="e" s="T383">gɨnan </ts>
               <ts e="T385" id="Seg_2926" n="e" s="T384">emeːksin </ts>
               <ts e="T386" id="Seg_2928" n="e" s="T385">duːmata </ts>
               <ts e="T387" id="Seg_2930" n="e" s="T386">kelbit. </ts>
               <ts e="T388" id="Seg_2932" n="e" s="T387">"Ogoloːr, </ts>
               <ts e="T389" id="Seg_2934" n="e" s="T388">min </ts>
               <ts e="T390" id="Seg_2936" n="e" s="T389">oloru͡oktaːgar </ts>
               <ts e="T391" id="Seg_2938" n="e" s="T390">ogonnʼorum </ts>
               <ts e="T392" id="Seg_2940" n="e" s="T391">ɨratɨgar </ts>
               <ts e="T393" id="Seg_2942" n="e" s="T392">bilse </ts>
               <ts e="T394" id="Seg_2944" n="e" s="T393">barɨ͡ak </ts>
               <ts e="T395" id="Seg_2946" n="e" s="T394">baːrɨm." </ts>
               <ts e="T396" id="Seg_2948" n="e" s="T395">Ühüs </ts>
               <ts e="T397" id="Seg_2950" n="e" s="T396">hɨlɨgar </ts>
               <ts e="T398" id="Seg_2952" n="e" s="T397">ogonnʼorugar </ts>
               <ts e="T399" id="Seg_2954" n="e" s="T398">baraːrɨ </ts>
               <ts e="T400" id="Seg_2956" n="e" s="T399">taŋɨnna. </ts>
               <ts e="T401" id="Seg_2958" n="e" s="T400">Keli͡ebi </ts>
               <ts e="T402" id="Seg_2960" n="e" s="T401">bɨhɨtalaːn </ts>
               <ts e="T403" id="Seg_2962" n="e" s="T402">baran </ts>
               <ts e="T404" id="Seg_2964" n="e" s="T403">ɨlla. </ts>
               <ts e="T405" id="Seg_2966" n="e" s="T404">Ol </ts>
               <ts e="T406" id="Seg_2968" n="e" s="T405">gɨnan </ts>
               <ts e="T407" id="Seg_2970" n="e" s="T406">baran </ts>
               <ts e="T408" id="Seg_2972" n="e" s="T407">kɨːl </ts>
               <ts e="T409" id="Seg_2974" n="e" s="T408">hɨ͡atɨn </ts>
               <ts e="T410" id="Seg_2976" n="e" s="T409">ildʼi͡ehi. </ts>
               <ts e="T411" id="Seg_2978" n="e" s="T410">Innʼe </ts>
               <ts e="T412" id="Seg_2980" n="e" s="T411">gɨnan </ts>
               <ts e="T413" id="Seg_2982" n="e" s="T412">baran </ts>
               <ts e="T414" id="Seg_2984" n="e" s="T413">ogonnʼorugar </ts>
               <ts e="T415" id="Seg_2986" n="e" s="T414">barda. </ts>
               <ts e="T416" id="Seg_2988" n="e" s="T415">Bu </ts>
               <ts e="T417" id="Seg_2990" n="e" s="T416">baran </ts>
               <ts e="T418" id="Seg_2992" n="e" s="T417">baran </ts>
               <ts e="T419" id="Seg_2994" n="e" s="T418">ogonnʼorun </ts>
               <ts e="T420" id="Seg_2996" n="e" s="T419">golomotugar </ts>
               <ts e="T421" id="Seg_2998" n="e" s="T420">tijbit. </ts>
               <ts e="T422" id="Seg_3000" n="e" s="T421">Araj </ts>
               <ts e="T423" id="Seg_3002" n="e" s="T422">tu͡okkaːn </ts>
               <ts e="T424" id="Seg_3004" n="e" s="T423">du </ts>
               <ts e="T425" id="Seg_3006" n="e" s="T424">hu͡ok, </ts>
               <ts e="T426" id="Seg_3008" n="e" s="T425">bütünnüː </ts>
               <ts e="T427" id="Seg_3010" n="e" s="T426">barɨta </ts>
               <ts e="T428" id="Seg_3012" n="e" s="T427">tibillen </ts>
               <ts e="T429" id="Seg_3014" n="e" s="T428">huːlan </ts>
               <ts e="T430" id="Seg_3016" n="e" s="T429">kaːlbɨt. </ts>
               <ts e="T431" id="Seg_3018" n="e" s="T430">Ol </ts>
               <ts e="T432" id="Seg_3020" n="e" s="T431">da </ts>
               <ts e="T433" id="Seg_3022" n="e" s="T432">bu͡ollar </ts>
               <ts e="T434" id="Seg_3024" n="e" s="T433">üːlehiger </ts>
               <ts e="T435" id="Seg_3026" n="e" s="T434">ɨttɨbɨt. </ts>
               <ts e="T436" id="Seg_3028" n="e" s="T435">Araj </ts>
               <ts e="T437" id="Seg_3030" n="e" s="T436">ačaːgɨn </ts>
               <ts e="T438" id="Seg_3032" n="e" s="T437">tügürüččü </ts>
               <ts e="T439" id="Seg_3034" n="e" s="T438">üs </ts>
               <ts e="T440" id="Seg_3036" n="e" s="T439">tüːleːk </ts>
               <ts e="T441" id="Seg_3038" n="e" s="T440">kürdʼege </ts>
               <ts e="T442" id="Seg_3040" n="e" s="T441">bu͡olan </ts>
               <ts e="T443" id="Seg_3042" n="e" s="T442">dʼaːbɨlana </ts>
               <ts e="T444" id="Seg_3044" n="e" s="T443">hɨtar, </ts>
               <ts e="T445" id="Seg_3046" n="e" s="T444">atɨn </ts>
               <ts e="T446" id="Seg_3048" n="e" s="T445">kara </ts>
               <ts e="T447" id="Seg_3050" n="e" s="T446">tujagɨn </ts>
               <ts e="T448" id="Seg_3052" n="e" s="T447">gɨtta </ts>
               <ts e="T449" id="Seg_3054" n="e" s="T448">büttehin </ts>
               <ts e="T450" id="Seg_3056" n="e" s="T449">ere </ts>
               <ts e="T451" id="Seg_3058" n="e" s="T450">ordorbut </ts>
               <ts e="T452" id="Seg_3060" n="e" s="T451">atɨn </ts>
               <ts e="T453" id="Seg_3062" n="e" s="T452">gi͡enin. </ts>
               <ts e="T454" id="Seg_3064" n="e" s="T453">Maːgɨŋ </ts>
               <ts e="T455" id="Seg_3066" n="e" s="T454">kubulujbut </ts>
               <ts e="T456" id="Seg_3068" n="e" s="T455">obu͡oja </ts>
               <ts e="T457" id="Seg_3070" n="e" s="T456">ühü. </ts>
               <ts e="T458" id="Seg_3072" n="e" s="T457">Ol </ts>
               <ts e="T459" id="Seg_3074" n="e" s="T458">ihin </ts>
               <ts e="T460" id="Seg_3076" n="e" s="T459">ölümeːri </ts>
               <ts e="T461" id="Seg_3078" n="e" s="T460">kubulujbuta </ts>
               <ts e="T462" id="Seg_3080" n="e" s="T461">ühü. </ts>
               <ts e="T463" id="Seg_3082" n="e" s="T462">Dʼe </ts>
               <ts e="T464" id="Seg_3084" n="e" s="T463">onu </ts>
               <ts e="T465" id="Seg_3086" n="e" s="T464">baː </ts>
               <ts e="T466" id="Seg_3088" n="e" s="T465">kili͡ebinen </ts>
               <ts e="T467" id="Seg_3090" n="e" s="T466">bɨrakpɨt. </ts>
               <ts e="T468" id="Seg_3092" n="e" s="T467">Manɨ </ts>
               <ts e="T469" id="Seg_3094" n="e" s="T468">türdes </ts>
               <ts e="T470" id="Seg_3096" n="e" s="T469">gɨmmɨt </ts>
               <ts e="T471" id="Seg_3098" n="e" s="T470">daː </ts>
               <ts e="T472" id="Seg_3100" n="e" s="T471">eppit: </ts>
               <ts e="T473" id="Seg_3102" n="e" s="T472">"Baː </ts>
               <ts e="T474" id="Seg_3104" n="e" s="T473">ohogum </ts>
               <ts e="T475" id="Seg_3106" n="e" s="T474">bu͡ora </ts>
               <ts e="T476" id="Seg_3108" n="e" s="T475">hɨtɨ͡arɨmaːrɨ </ts>
               <ts e="T477" id="Seg_3110" n="e" s="T476">gɨmmɨt", </ts>
               <ts e="T478" id="Seg_3112" n="e" s="T477">diːr. </ts>
               <ts e="T479" id="Seg_3114" n="e" s="T478">Hɨ͡annan </ts>
               <ts e="T480" id="Seg_3116" n="e" s="T479">bɨrakpɨt. </ts>
               <ts e="T481" id="Seg_3118" n="e" s="T480">"ɨčča, </ts>
               <ts e="T482" id="Seg_3120" n="e" s="T481">koŋuraktar </ts>
               <ts e="T483" id="Seg_3122" n="e" s="T482">tühenner </ts>
               <ts e="T484" id="Seg_3124" n="e" s="T483">hɨtɨ͡arɨmaːrɨ </ts>
               <ts e="T485" id="Seg_3126" n="e" s="T484">gɨnnɨlar." </ts>
               <ts e="T486" id="Seg_3128" n="e" s="T485">Emi͡e </ts>
               <ts e="T487" id="Seg_3130" n="e" s="T486">bɨrakpɨt. </ts>
               <ts e="T488" id="Seg_3132" n="e" s="T487">Manɨ </ts>
               <ts e="T489" id="Seg_3134" n="e" s="T488">kiren </ts>
               <ts e="T490" id="Seg_3136" n="e" s="T489">körbüt — </ts>
               <ts e="T491" id="Seg_3138" n="e" s="T490">hɨ͡a </ts>
               <ts e="T492" id="Seg_3140" n="e" s="T491">ebit. </ts>
               <ts e="T493" id="Seg_3142" n="e" s="T492">Onu </ts>
               <ts e="T494" id="Seg_3144" n="e" s="T493">"taŋara, </ts>
               <ts e="T495" id="Seg_3146" n="e" s="T494">taŋara, </ts>
               <ts e="T496" id="Seg_3148" n="e" s="T495">čočumčakeːn" </ts>
               <ts e="T497" id="Seg_3150" n="e" s="T496">di͡ebit. </ts>
               <ts e="T498" id="Seg_3152" n="e" s="T497">"Kaja, </ts>
               <ts e="T499" id="Seg_3154" n="e" s="T498">ogonnʼor, </ts>
               <ts e="T500" id="Seg_3156" n="e" s="T499">aːŋŋɨn </ts>
               <ts e="T501" id="Seg_3158" n="e" s="T500">arɨj", </ts>
               <ts e="T502" id="Seg_3160" n="e" s="T501">di͡ebit </ts>
               <ts e="T503" id="Seg_3162" n="e" s="T502">emeːksine. </ts>
               <ts e="T504" id="Seg_3164" n="e" s="T503">Aːnɨn </ts>
               <ts e="T505" id="Seg_3166" n="e" s="T504">arɨjan </ts>
               <ts e="T506" id="Seg_3168" n="e" s="T505">obu͡ojdammɨt. </ts>
               <ts e="T507" id="Seg_3170" n="e" s="T506">Emeːksin </ts>
               <ts e="T508" id="Seg_3172" n="e" s="T507">ahatan </ts>
               <ts e="T509" id="Seg_3174" n="e" s="T508">baran </ts>
               <ts e="T510" id="Seg_3176" n="e" s="T509">dʼi͡etiger </ts>
               <ts e="T511" id="Seg_3178" n="e" s="T510">ilpit. </ts>
               <ts e="T512" id="Seg_3180" n="e" s="T511">Bu </ts>
               <ts e="T513" id="Seg_3182" n="e" s="T512">dʼi͡etiger </ts>
               <ts e="T514" id="Seg_3184" n="e" s="T513">ildʼen </ts>
               <ts e="T515" id="Seg_3186" n="e" s="T514">huːjan-taraːn </ts>
               <ts e="T516" id="Seg_3188" n="e" s="T515">baran </ts>
               <ts e="T517" id="Seg_3190" n="e" s="T516">tannɨbatak </ts>
               <ts e="T518" id="Seg_3192" n="e" s="T517">taŋahɨ </ts>
               <ts e="T519" id="Seg_3194" n="e" s="T518">taŋɨnnaran </ts>
               <ts e="T520" id="Seg_3196" n="e" s="T519">baran </ts>
               <ts e="T521" id="Seg_3198" n="e" s="T520">taːs </ts>
               <ts e="T522" id="Seg_3200" n="e" s="T521">kɨrabaːkka </ts>
               <ts e="T523" id="Seg_3202" n="e" s="T522">olordon </ts>
               <ts e="T524" id="Seg_3204" n="e" s="T523">keːspit. </ts>
               <ts e="T525" id="Seg_3206" n="e" s="T524">Ol </ts>
               <ts e="T526" id="Seg_3208" n="e" s="T525">gɨnan </ts>
               <ts e="T527" id="Seg_3210" n="e" s="T526">baran </ts>
               <ts e="T528" id="Seg_3212" n="e" s="T527">čaːj </ts>
               <ts e="T529" id="Seg_3214" n="e" s="T528">iherpit, </ts>
               <ts e="T530" id="Seg_3216" n="e" s="T529">onton </ts>
               <ts e="T531" id="Seg_3218" n="e" s="T530">et </ts>
               <ts e="T532" id="Seg_3220" n="e" s="T531">kü͡ös </ts>
               <ts e="T533" id="Seg_3222" n="e" s="T532">kotorbut, </ts>
               <ts e="T534" id="Seg_3224" n="e" s="T533">hɨ͡alaːk </ts>
               <ts e="T535" id="Seg_3226" n="e" s="T534">mini </ts>
               <ts e="T536" id="Seg_3228" n="e" s="T535">kɨtɨjaga </ts>
               <ts e="T537" id="Seg_3230" n="e" s="T536">kotorbut. </ts>
               <ts e="T538" id="Seg_3232" n="e" s="T537">Innʼe </ts>
               <ts e="T539" id="Seg_3234" n="e" s="T538">gɨnan </ts>
               <ts e="T540" id="Seg_3236" n="e" s="T539">kömüs </ts>
               <ts e="T541" id="Seg_3238" n="e" s="T540">lu͡osku, </ts>
               <ts e="T542" id="Seg_3240" n="e" s="T541">biːlke </ts>
               <ts e="T543" id="Seg_3242" n="e" s="T542">uːrbut. </ts>
               <ts e="T544" id="Seg_3244" n="e" s="T543">Araj </ts>
               <ts e="T545" id="Seg_3246" n="e" s="T544">lu͡oskutunan </ts>
               <ts e="T546" id="Seg_3248" n="e" s="T545">iheːri </ts>
               <ts e="T547" id="Seg_3250" n="e" s="T546">gɨmmɨt: </ts>
               <ts e="T548" id="Seg_3252" n="e" s="T547">"Kaja </ts>
               <ts e="T549" id="Seg_3254" n="e" s="T548">ba-ba, </ts>
               <ts e="T550" id="Seg_3256" n="e" s="T549">lu͡oskum </ts>
               <ts e="T551" id="Seg_3258" n="e" s="T550">dʼogus, </ts>
               <ts e="T552" id="Seg_3260" n="e" s="T551">ulakan </ts>
               <ts e="T553" id="Seg_3262" n="e" s="T552">pabaraːŋkɨta </ts>
               <ts e="T554" id="Seg_3264" n="e" s="T553">duː, </ts>
               <ts e="T555" id="Seg_3266" n="e" s="T554">komu͡osta </ts>
               <ts e="T556" id="Seg_3268" n="e" s="T555">duː </ts>
               <ts e="T557" id="Seg_3270" n="e" s="T556">egel", </ts>
               <ts e="T558" id="Seg_3272" n="e" s="T557">di͡ebit. </ts>
               <ts e="T559" id="Seg_3274" n="e" s="T558">Maːgɨta, </ts>
               <ts e="T560" id="Seg_3276" n="e" s="T559">komu͡os </ts>
               <ts e="T561" id="Seg_3278" n="e" s="T560">bagajɨta </ts>
               <ts e="T562" id="Seg_3280" n="e" s="T561">tuttaran </ts>
               <ts e="T563" id="Seg_3282" n="e" s="T562">keːspit. </ts>
               <ts e="T564" id="Seg_3284" n="e" s="T563">Hɨ͡alaːk </ts>
               <ts e="T565" id="Seg_3286" n="e" s="T564">mini </ts>
               <ts e="T566" id="Seg_3288" n="e" s="T565">baːtɨnan </ts>
               <ts e="T567" id="Seg_3290" n="e" s="T566">"huːp" </ts>
               <ts e="T568" id="Seg_3292" n="e" s="T567">gɨmmɨt. </ts>
               <ts e="T569" id="Seg_3294" n="e" s="T568">Manɨ͡aka </ts>
               <ts e="T570" id="Seg_3296" n="e" s="T569">dʼe </ts>
               <ts e="T571" id="Seg_3298" n="e" s="T570">čačajar </ts>
               <ts e="T572" id="Seg_3300" n="e" s="T571">diː. </ts>
               <ts e="T573" id="Seg_3302" n="e" s="T572">Dʼe </ts>
               <ts e="T574" id="Seg_3304" n="e" s="T573">onton </ts>
               <ts e="T575" id="Seg_3306" n="e" s="T574">"čačajabɨn" </ts>
               <ts e="T576" id="Seg_3308" n="e" s="T575">di͡en </ts>
               <ts e="T577" id="Seg_3310" n="e" s="T576">dʼe </ts>
               <ts e="T578" id="Seg_3312" n="e" s="T577">uturuktuːr </ts>
               <ts e="T579" id="Seg_3314" n="e" s="T578">diː, </ts>
               <ts e="T580" id="Seg_3316" n="e" s="T579">taːs </ts>
               <ts e="T581" id="Seg_3318" n="e" s="T580">olbogun </ts>
               <ts e="T582" id="Seg_3320" n="e" s="T581">bɨha </ts>
               <ts e="T583" id="Seg_3322" n="e" s="T582">uturuktuːr </ts>
               <ts e="T584" id="Seg_3324" n="e" s="T583">daː </ts>
               <ts e="T585" id="Seg_3326" n="e" s="T584">onon </ts>
               <ts e="T586" id="Seg_3328" n="e" s="T585">üs </ts>
               <ts e="T587" id="Seg_3330" n="e" s="T586">buluːs </ts>
               <ts e="T588" id="Seg_3332" n="e" s="T587">hiri </ts>
               <ts e="T589" id="Seg_3334" n="e" s="T588">tobulu </ts>
               <ts e="T590" id="Seg_3336" n="e" s="T589">tuhen </ts>
               <ts e="T591" id="Seg_3338" n="e" s="T590">kaːlar. </ts>
               <ts e="T592" id="Seg_3340" n="e" s="T591">Emeːksine </ts>
               <ts e="T593" id="Seg_3342" n="e" s="T592">tu͡ok </ts>
               <ts e="T594" id="Seg_3344" n="e" s="T593">bu͡olu͡ogaj, </ts>
               <ts e="T595" id="Seg_3346" n="e" s="T594">kajdak </ts>
               <ts e="T596" id="Seg_3348" n="e" s="T595">olorbuta </ts>
               <ts e="T597" id="Seg_3350" n="e" s="T596">daː </ts>
               <ts e="T598" id="Seg_3352" n="e" s="T597">ogurduk </ts>
               <ts e="T601" id="Seg_3354" n="e" s="T598">bi͡ek </ts>
               <ts e="T599" id="Seg_3356" n="e" s="T601">bi͡ek </ts>
               <ts e="T600" id="Seg_3358" n="e" s="T599">oloror. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_3359" s="T0">BaA_1930_OldManOldWoman_flk.001 (001.001)</ta>
            <ta e="T7" id="Seg_3360" s="T4">BaA_1930_OldManOldWoman_flk.002 (001.002)</ta>
            <ta e="T14" id="Seg_3361" s="T7">BaA_1930_OldManOldWoman_flk.003 (001.003)</ta>
            <ta e="T19" id="Seg_3362" s="T14">BaA_1930_OldManOldWoman_flk.004 (001.004)</ta>
            <ta e="T22" id="Seg_3363" s="T19">BaA_1930_OldManOldWoman_flk.005 (001.005)</ta>
            <ta e="T27" id="Seg_3364" s="T22">BaA_1930_OldManOldWoman_flk.006 (001.006)</ta>
            <ta e="T33" id="Seg_3365" s="T27">BaA_1930_OldManOldWoman_flk.007 (001.007)</ta>
            <ta e="T41" id="Seg_3366" s="T33">BaA_1930_OldManOldWoman_flk.008 (001.008)</ta>
            <ta e="T46" id="Seg_3367" s="T41">BaA_1930_OldManOldWoman_flk.009 (001.009)</ta>
            <ta e="T51" id="Seg_3368" s="T46">BaA_1930_OldManOldWoman_flk.010 (001.010)</ta>
            <ta e="T54" id="Seg_3369" s="T51">BaA_1930_OldManOldWoman_flk.011 (001.011)</ta>
            <ta e="T57" id="Seg_3370" s="T54">BaA_1930_OldManOldWoman_flk.012 (001.011)</ta>
            <ta e="T67" id="Seg_3371" s="T57">BaA_1930_OldManOldWoman_flk.013 (001.012)</ta>
            <ta e="T69" id="Seg_3372" s="T67">BaA_1930_OldManOldWoman_flk.014 (001.013)</ta>
            <ta e="T72" id="Seg_3373" s="T69">BaA_1930_OldManOldWoman_flk.015 (001.013)</ta>
            <ta e="T75" id="Seg_3374" s="T72">BaA_1930_OldManOldWoman_flk.016 (001.014)</ta>
            <ta e="T79" id="Seg_3375" s="T75">BaA_1930_OldManOldWoman_flk.017 (001.015)</ta>
            <ta e="T90" id="Seg_3376" s="T79">BaA_1930_OldManOldWoman_flk.018 (001.016)</ta>
            <ta e="T98" id="Seg_3377" s="T90">BaA_1930_OldManOldWoman_flk.019 (001.016)</ta>
            <ta e="T101" id="Seg_3378" s="T98">BaA_1930_OldManOldWoman_flk.020 (001.017)</ta>
            <ta e="T107" id="Seg_3379" s="T101">BaA_1930_OldManOldWoman_flk.021 (001.018)</ta>
            <ta e="T118" id="Seg_3380" s="T107">BaA_1930_OldManOldWoman_flk.022 (001.018)</ta>
            <ta e="T127" id="Seg_3381" s="T118">BaA_1930_OldManOldWoman_flk.023 (001.019)</ta>
            <ta e="T134" id="Seg_3382" s="T127">BaA_1930_OldManOldWoman_flk.024 (001.020)</ta>
            <ta e="T141" id="Seg_3383" s="T134">BaA_1930_OldManOldWoman_flk.025 (001.021)</ta>
            <ta e="T147" id="Seg_3384" s="T141">BaA_1930_OldManOldWoman_flk.026 (001.022)</ta>
            <ta e="T156" id="Seg_3385" s="T147">BaA_1930_OldManOldWoman_flk.027 (001.023)</ta>
            <ta e="T165" id="Seg_3386" s="T156">BaA_1930_OldManOldWoman_flk.028 (001.024)</ta>
            <ta e="T176" id="Seg_3387" s="T165">BaA_1930_OldManOldWoman_flk.029 (001.025)</ta>
            <ta e="T184" id="Seg_3388" s="T176">BaA_1930_OldManOldWoman_flk.030 (001.026)</ta>
            <ta e="T195" id="Seg_3389" s="T184">BaA_1930_OldManOldWoman_flk.031 (001.027)</ta>
            <ta e="T208" id="Seg_3390" s="T195">BaA_1930_OldManOldWoman_flk.032 (001.028)</ta>
            <ta e="T219" id="Seg_3391" s="T208">BaA_1930_OldManOldWoman_flk.033 (001.029)</ta>
            <ta e="T225" id="Seg_3392" s="T219">BaA_1930_OldManOldWoman_flk.034 (001.030)</ta>
            <ta e="T240" id="Seg_3393" s="T225">BaA_1930_OldManOldWoman_flk.035 (001.031)</ta>
            <ta e="T251" id="Seg_3394" s="T240">BaA_1930_OldManOldWoman_flk.036 (001.032)</ta>
            <ta e="T258" id="Seg_3395" s="T251">BaA_1930_OldManOldWoman_flk.037 (001.033)</ta>
            <ta e="T274" id="Seg_3396" s="T258">BaA_1930_OldManOldWoman_flk.038 (001.034)</ta>
            <ta e="T282" id="Seg_3397" s="T274">BaA_1930_OldManOldWoman_flk.039 (001.035)</ta>
            <ta e="T287" id="Seg_3398" s="T282">BaA_1930_OldManOldWoman_flk.040 (001.036)</ta>
            <ta e="T302" id="Seg_3399" s="T287">BaA_1930_OldManOldWoman_flk.041 (001.037)</ta>
            <ta e="T317" id="Seg_3400" s="T302">BaA_1930_OldManOldWoman_flk.042 (001.038)</ta>
            <ta e="T325" id="Seg_3401" s="T317">BaA_1930_OldManOldWoman_flk.043 (001.039)</ta>
            <ta e="T331" id="Seg_3402" s="T325">BaA_1930_OldManOldWoman_flk.044 (001.040)</ta>
            <ta e="T338" id="Seg_3403" s="T331">BaA_1930_OldManOldWoman_flk.045 (001.041)</ta>
            <ta e="T347" id="Seg_3404" s="T338">BaA_1930_OldManOldWoman_flk.046 (001.042)</ta>
            <ta e="T359" id="Seg_3405" s="T347">BaA_1930_OldManOldWoman_flk.047 (001.043)</ta>
            <ta e="T377" id="Seg_3406" s="T359">BaA_1930_OldManOldWoman_flk.048 (001.044)</ta>
            <ta e="T382" id="Seg_3407" s="T377">BaA_1930_OldManOldWoman_flk.049 (001.045)</ta>
            <ta e="T387" id="Seg_3408" s="T382">BaA_1930_OldManOldWoman_flk.050 (001.046)</ta>
            <ta e="T395" id="Seg_3409" s="T387">BaA_1930_OldManOldWoman_flk.051 (001.047)</ta>
            <ta e="T400" id="Seg_3410" s="T395">BaA_1930_OldManOldWoman_flk.052 (001.048)</ta>
            <ta e="T404" id="Seg_3411" s="T400">BaA_1930_OldManOldWoman_flk.053 (001.049)</ta>
            <ta e="T410" id="Seg_3412" s="T404">BaA_1930_OldManOldWoman_flk.054 (001.050)</ta>
            <ta e="T415" id="Seg_3413" s="T410">BaA_1930_OldManOldWoman_flk.055 (001.051)</ta>
            <ta e="T421" id="Seg_3414" s="T415">BaA_1930_OldManOldWoman_flk.056 (001.052)</ta>
            <ta e="T430" id="Seg_3415" s="T421">BaA_1930_OldManOldWoman_flk.057 (001.053)</ta>
            <ta e="T435" id="Seg_3416" s="T430">BaA_1930_OldManOldWoman_flk.058 (001.054)</ta>
            <ta e="T453" id="Seg_3417" s="T435">BaA_1930_OldManOldWoman_flk.059 (001.055)</ta>
            <ta e="T457" id="Seg_3418" s="T453">BaA_1930_OldManOldWoman_flk.060 (001.056)</ta>
            <ta e="T462" id="Seg_3419" s="T457">BaA_1930_OldManOldWoman_flk.061 (001.057)</ta>
            <ta e="T467" id="Seg_3420" s="T462">BaA_1930_OldManOldWoman_flk.062 (001.058)</ta>
            <ta e="T472" id="Seg_3421" s="T467">BaA_1930_OldManOldWoman_flk.063 (001.059)</ta>
            <ta e="T478" id="Seg_3422" s="T472">BaA_1930_OldManOldWoman_flk.064 (001.059)</ta>
            <ta e="T480" id="Seg_3423" s="T478">BaA_1930_OldManOldWoman_flk.065 (001.060)</ta>
            <ta e="T485" id="Seg_3424" s="T480">BaA_1930_OldManOldWoman_flk.066 (001.061)</ta>
            <ta e="T487" id="Seg_3425" s="T485">BaA_1930_OldManOldWoman_flk.067 (001.062)</ta>
            <ta e="T492" id="Seg_3426" s="T487">BaA_1930_OldManOldWoman_flk.068 (001.063)</ta>
            <ta e="T497" id="Seg_3427" s="T492">BaA_1930_OldManOldWoman_flk.069 (001.064)</ta>
            <ta e="T503" id="Seg_3428" s="T497">BaA_1930_OldManOldWoman_flk.070 (001.065)</ta>
            <ta e="T506" id="Seg_3429" s="T503">BaA_1930_OldManOldWoman_flk.071 (001.066)</ta>
            <ta e="T511" id="Seg_3430" s="T506">BaA_1930_OldManOldWoman_flk.072 (001.067)</ta>
            <ta e="T524" id="Seg_3431" s="T511">BaA_1930_OldManOldWoman_flk.073 (001.068)</ta>
            <ta e="T537" id="Seg_3432" s="T524">BaA_1930_OldManOldWoman_flk.074 (001.069)</ta>
            <ta e="T543" id="Seg_3433" s="T537">BaA_1930_OldManOldWoman_flk.075 (001.070)</ta>
            <ta e="T547" id="Seg_3434" s="T543">BaA_1930_OldManOldWoman_flk.076 (001.071)</ta>
            <ta e="T558" id="Seg_3435" s="T547">BaA_1930_OldManOldWoman_flk.077 (001.071)</ta>
            <ta e="T563" id="Seg_3436" s="T558">BaA_1930_OldManOldWoman_flk.078 (001.072)</ta>
            <ta e="T568" id="Seg_3437" s="T563">BaA_1930_OldManOldWoman_flk.079 (001.073)</ta>
            <ta e="T572" id="Seg_3438" s="T568">BaA_1930_OldManOldWoman_flk.080 (001.074)</ta>
            <ta e="T591" id="Seg_3439" s="T572">BaA_1930_OldManOldWoman_flk.081 (001.075)</ta>
            <ta e="T600" id="Seg_3440" s="T591">BaA_1930_OldManOldWoman_flk.082 (001.076)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_3441" s="T0">Огонньордоок эмээксин олорбуттар үһү.</ta>
            <ta e="T7" id="Seg_3442" s="T4">Төрөтөр огото һуоктар.</ta>
            <ta e="T14" id="Seg_3443" s="T7">Дьиэгэ олордокторуна уоттара эрэ кытарар, огурдук дьадаҥылар.</ta>
            <ta e="T19" id="Seg_3444" s="T14">Таһаара тагыстактарына каардара эрэ туртайар.</ta>
            <ta e="T22" id="Seg_3445" s="T19">Туога даа һуоктар.</ta>
            <ta e="T27" id="Seg_3446" s="T22">Арай үптэрэ диэннэрэ һуос һоготок аттаактар.</ta>
            <ta e="T33" id="Seg_3447" s="T27">Онтон бултанар тэргэттэрэ үс таастаак илимкээннээктэр.</ta>
            <ta e="T41" id="Seg_3448" s="T33">Онтуларыгар уу курдьагата иилиннэгинэ ону һииллэр үчэһэгэ үөлэн.</ta>
            <ta e="T46" id="Seg_3449" s="T41">Арай өлөөрү үөкэйбиттэр, быстаары быакайбыттар.</ta>
            <ta e="T51" id="Seg_3450" s="T46">Онтон огонньоро арай киирбит эмээксинигэр.</ta>
            <ta e="T54" id="Seg_3451" s="T51">Дьэ киирэн дьиэбит: </ta>
            <ta e="T57" id="Seg_3452" s="T54">— Кайа эмээксин үрдүүбүт.</ta>
            <ta e="T67" id="Seg_3453" s="T57">Каамтарыакпыт һоготок аттаакпыт, һиэкпит баара, онноогор октон өллөкпүтүнэ кэннибитигэр каалыага.</ta>
            <ta e="T69" id="Seg_3454" s="T67">Онуга эмээксин: </ta>
            <ta e="T72" id="Seg_3455" s="T69">— Бэйэҥ һанааҥ, — диэбит.</ta>
            <ta e="T75" id="Seg_3456" s="T72">— Өлөрдөргүн эрэ һиэкпит!</ta>
            <ta e="T79" id="Seg_3457" s="T75">Эрэ тагыспыт даа өлөрбүт.</ta>
            <ta e="T90" id="Seg_3458" s="T79">Огонньоро һүлбүт, арай тымныы багайы, чыскаан багайы, огонньоро киирбит, киирэн диэбит: </ta>
            <ta e="T98" id="Seg_3459" s="T90">— Аппыт этин бүтүннүү голомокооммут уҥуор таскайдаан кээс чыыстатын.</ta>
            <ta e="T101" id="Seg_3460" s="T98">Эмээксинэ барытын таскайдаабыт.</ta>
            <ta e="T107" id="Seg_3461" s="T101">Эмээксинэ илиитин иттээри кыммытын огонньор эппит: </ta>
            <ta e="T118" id="Seg_3462" s="T107">— Һиэккин, бары иһиккэр һолуургар уута баһан толор, бэлэмнээн баран дьэ аһаар.</ta>
            <ta e="T127" id="Seg_3463" s="T118">Инньэ гынан эмээксин таксан бары оҥкучагар барыкакаан-нарыгар уу баспыт.</ta>
            <ta e="T134" id="Seg_3464" s="T127">Арай бүтэһик һолуургакаккакаанын эгэлээри гыммыта, аана катыылаак.</ta>
            <ta e="T141" id="Seg_3465" s="T134">— Кайа, огонньор, ааҥҥын ас, үлүйээри гынным, — диэбит.</ta>
            <ta e="T147" id="Seg_3466" s="T141">— һуука кыыһа, каарга кампы бырак, — диэбит.</ta>
            <ta e="T156" id="Seg_3467" s="T147">Эмээксинэ баа һолуурчагын быраан баран дьэ тоҥон өлөөрү гынар.</ta>
            <ta e="T165" id="Seg_3468" s="T156">Эмээксинэ балаганын үрдүгэр таксан кыымкааннар таксыыларыгар онно илиитин иттэр.</ta>
            <ta e="T176" id="Seg_3469" s="T165">Арай эрин көрбүтэ, атын һамагын ылан баран алчайан баран һачалана олорор.</ta>
            <ta e="T184" id="Seg_3470" s="T176">Ол иттээри гынар эмээксинин илиитин үчэһэннэн быһыта кэйэр.</ta>
            <ta e="T195" id="Seg_3471" s="T184">Онтута ытыы-ытыы һиргэ түһээктээн баран мастыыр һиркээн диэг ытыы-ытыы каама турар.</ta>
            <ta e="T208" id="Seg_3472" s="T195">Дьэ онно тиитикун мас төрдүгэр һытаары гыммыт, һытаары гыммыт, тымныы титириир һүрэгэр киирэр.</ta>
            <ta e="T219" id="Seg_3473" s="T208">Ол һыттагына айдаан бөгө иһиллибит, таба үүрэртэн таба үүрэр энин багайы.</ta>
            <ta e="T225" id="Seg_3474" s="T219">Ол коту дьэ барар бу эмээксин.</ta>
            <ta e="T240" id="Seg_3475" s="T225">Ол баран, айыы даганы, ыйы бүөлүүр ыыс туман табага, күнү бүөлүүр кудэн туман һүөһүгэ тийэр.</ta>
            <ta e="T251" id="Seg_3476" s="T240">Бу эмээксиниҥ дьиэни көрдөөн бу табаны тэлэригэр икки энээрэ элэйэн каалбыт.</ta>
            <ta e="T258" id="Seg_3477" s="T251">Арай алыыны тобус толору ураһа дьиэ тутуллан турар.</ta>
            <ta e="T274" id="Seg_3478" s="T258">Улакан мыйаа дьиэ кэллэ, бу дьиэгэ, манна дьэ кас уон киһи быһа өлбүгүнэ, буолага һубуруйан турар.</ta>
            <ta e="T282" id="Seg_3479" s="T274">Инньэ гынан баа эмээксин баа дьиэни арыйа оксубут.</ta>
            <ta e="T287" id="Seg_3480" s="T282">Одуулаан көрбүтэ — ибис иччитэк, киһитэ һуок.</ta>
            <ta e="T302" id="Seg_3481" s="T287">Бары күөс күөстэнэн турар, бары ас астанан турар, таас кырабааттар бүтүннүү оннук тэргэттээк дьиэ турар.</ta>
            <ta e="T317" id="Seg_3482" s="T302">Ол эмээксин киирэн аһаабатагын аһыыр, һиэбэтэгин һиир, нуучча даа аһа үгүс, тыа даа аһа үгүс.</ta>
            <ta e="T325" id="Seg_3483" s="T317">Бу эмээксин һуунан-тараанан баран үүс-киис таҥаһы таҥнан кээспит.</ta>
            <ta e="T331" id="Seg_3484" s="T325">Бу тулкары туок даа киһитэ биллибэт.</ta>
            <ta e="T338" id="Seg_3485" s="T331">Үс күнү мэлдьи туок даа киһитэ биллибэт.</ta>
            <ta e="T347" id="Seg_3486" s="T338">Арай киэһэ буолуо даа үөлэһинэн кур киһи мэйиилэрэ туһэллэр.</ta>
            <ta e="T359" id="Seg_3487" s="T347">Олору "тэһиҥ-буһуҥ!" [тэһитэ бар] диидии тэһитэ үктүүр, олоро һир диэг һимиттэн иһэллэр.</ta>
            <ta e="T377" id="Seg_3488" s="T359">Эмээксин хаһ уон туккары олорбут, табата канна даа камнаабат, аттара һубу көрдүк һытыллар, балыктара һубу бултаммыт курдук һылльар.</ta>
            <ta e="T382" id="Seg_3489" s="T377">Эмээксин онон байан-тайан һуоч һоготок олорор.</ta>
            <ta e="T387" id="Seg_3490" s="T382">Инньэ гынан эмээксин дуумата кэлбит.</ta>
            <ta e="T395" id="Seg_3491" s="T387">"Оголоор, мин олоруоктаагар огонньорум ыратыгар билсэ барыак баарым".</ta>
            <ta e="T400" id="Seg_3492" s="T395">Үһүс һылыгар огонньоругар бараары таҥынна.</ta>
            <ta e="T404" id="Seg_3493" s="T400">Кэлиэби быһыталаан баран ылла.</ta>
            <ta e="T410" id="Seg_3494" s="T404">Ол гынан баран кыыл һыатын илдьиэһи.</ta>
            <ta e="T415" id="Seg_3495" s="T410">Инньэ гынан баран огонньоругар барда.</ta>
            <ta e="T421" id="Seg_3496" s="T415">Бу баран баран огонньорун голомотугар тийбит.</ta>
            <ta e="T430" id="Seg_3497" s="T421">Арай туоккаан ду һуок, бүтүннүү барыта тибиллэн һуулан каалбыт.</ta>
            <ta e="T435" id="Seg_3498" s="T430">Ол да буоллар үүлэһигэр ыттыбыт.</ta>
            <ta e="T453" id="Seg_3499" s="T435">Арай ачаагын түгүрүччү үс түүлээк күрдьэгэ буолан дьаабылана һытар, атын кара туйагын гытта бүттэһин эрэ ордорбут атын гиэнин.</ta>
            <ta e="T457" id="Seg_3500" s="T453">Маагыҥ кубулуйбут обуойа үһү.</ta>
            <ta e="T462" id="Seg_3501" s="T457">Ол иһин өлүмээри кубулуйбута үһү.</ta>
            <ta e="T467" id="Seg_3502" s="T462">Дьэ ону баа килиэбинэн быракпыт.</ta>
            <ta e="T472" id="Seg_3503" s="T467">Маны түрдэс гыммыт даа эппит: </ta>
            <ta e="T478" id="Seg_3504" s="T472">— Баа оһогум буора һытыарымаары гыммыт, — диир.</ta>
            <ta e="T480" id="Seg_3505" s="T478">һыаннан быракпыт.</ta>
            <ta e="T485" id="Seg_3506" s="T480">— Ыч-ча, коҥурактар түһэннэр һытыарымаары гыннылар.</ta>
            <ta e="T487" id="Seg_3507" s="T485">Эмиэ быракпыт.</ta>
            <ta e="T492" id="Seg_3508" s="T487">Маны кирэн көрбүт — һыа эбит.</ta>
            <ta e="T497" id="Seg_3509" s="T492">Ону: — Таҥара, таҥара, чочумчакээн, — диэбит.</ta>
            <ta e="T503" id="Seg_3510" s="T497">— Кайа, огонньор, ааҥҥын арый, — диэбит эмээксинэ.</ta>
            <ta e="T506" id="Seg_3511" s="T503">Аанын арыйан обуойдаммыт.</ta>
            <ta e="T511" id="Seg_3512" s="T506">Эмээксин аһатан баран дьиэтигэр илпит.</ta>
            <ta e="T524" id="Seg_3513" s="T511">Бу дьиэтигэр илдьэн һууйан-тараан баран танныбатак таҥаһы таҥыннаран баран таас кырабаакка олордон кээспит.</ta>
            <ta e="T537" id="Seg_3514" s="T524">Ол гынан баран чаай иһэрпит, онтон эт күөс которбут, һыалаак мини кытыйага которбут.</ta>
            <ta e="T543" id="Seg_3515" s="T537">Инньэ гынан көмүс луоску, биилкэ уурбут.</ta>
            <ta e="T547" id="Seg_3516" s="T543">Арай луоскутунан иһээри гыммыт: </ta>
            <ta e="T558" id="Seg_3517" s="T547">— Кайа ба-ба, луоскум дьогус, улакан пабарааҥкыта дуу, комуоста дуу эгэл, — диэбит.</ta>
            <ta e="T563" id="Seg_3518" s="T558">Маагыта, комуос багайыта туттаран кээспит. </ta>
            <ta e="T568" id="Seg_3519" s="T563">һыалаак мини баатынан "һууп" гыммыт.</ta>
            <ta e="T572" id="Seg_3520" s="T568">Маныака дьэ чачайар дии.</ta>
            <ta e="T591" id="Seg_3521" s="T572">Дьэ онтон чачайабын диэн дьэ утуруктуур дии, таас олбогун быһа утуруктуур даа онон үс булуус һири тобулу туһэн каалар.</ta>
            <ta e="T600" id="Seg_3522" s="T591">Эмээксинэ туок буолуогай, кайдак олорбута даа огурдук биэк-биэк олорор.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_3523" s="T0">Ogonnʼordoːk emeːksin olorbuttar ühü. </ta>
            <ta e="T7" id="Seg_3524" s="T4">Törötör ogoto hu͡oktar. </ta>
            <ta e="T14" id="Seg_3525" s="T7">Dʼi͡ege olordoktoruna u͡ottara ere kɨtarar, ogurduk dʼadaŋɨlar. </ta>
            <ta e="T19" id="Seg_3526" s="T14">Tahaːra tagɨstaktarɨna kaːrdara ere turtajar. </ta>
            <ta e="T22" id="Seg_3527" s="T19">Tu͡oga daː hu͡oktar. </ta>
            <ta e="T27" id="Seg_3528" s="T22">Araj üptere di͡ennere hu͡os hogotok attaːktar. </ta>
            <ta e="T33" id="Seg_3529" s="T27">Onton bultanar tergettere üs taːstaːk ilimkeːnneːkter. </ta>
            <ta e="T41" id="Seg_3530" s="T33">Ontularɨgar uː kurdʼagata iːlinnegine onu hiːller üčehege ü͡ölen. </ta>
            <ta e="T46" id="Seg_3531" s="T41">Araj ölöːrü ü͡ökejbitter, bɨstaːrɨ bɨ͡akajbɨttar. </ta>
            <ta e="T51" id="Seg_3532" s="T46">Onton ogonnʼoro araj kiːrbit emeːksiniger. </ta>
            <ta e="T54" id="Seg_3533" s="T51">Dʼe kiːren dʼi͡ebit: </ta>
            <ta e="T57" id="Seg_3534" s="T54">"Kaja emeːksin ürdüːbüt. </ta>
            <ta e="T67" id="Seg_3535" s="T57">Kaːmtarɨ͡akpɨt hogotok attaːkpɨt, hi͡ekpit baːra, onnoːgor okton öllökpütüne kennibitiger kaːlɨ͡aga." </ta>
            <ta e="T69" id="Seg_3536" s="T67">Onuga emeːksin: </ta>
            <ta e="T72" id="Seg_3537" s="T69">"Bejeŋ hanaːŋ", di͡ebit. </ta>
            <ta e="T75" id="Seg_3538" s="T72">"Ölördörgün ere hi͡ekpit!" </ta>
            <ta e="T79" id="Seg_3539" s="T75">Ere tagɨspɨt daː ölörbüt. </ta>
            <ta e="T90" id="Seg_3540" s="T79">Ogonnʼoro hülbüt, araj tɨmnɨː bagajɨ, čɨskaːn bagajɨ, ogonnʼoro kiːrbit, kiːren di͡ebit: </ta>
            <ta e="T98" id="Seg_3541" s="T90">"Appɨt etin bütünnüː golomokoːmmut uŋu͡or taskajdaːn keːs čɨːstatɨn." </ta>
            <ta e="T101" id="Seg_3542" s="T98">Emeːksine barɨtɨn taskajdaːbɨt. </ta>
            <ta e="T107" id="Seg_3543" s="T101">Emeːksine iliːtin itteːri kɨmmɨtɨn ogonnʼor eppit: </ta>
            <ta e="T118" id="Seg_3544" s="T107">"Hi͡ekkin, barɨ ihikker holuːrgar uːta bahan tolor, belemneːn baran dʼe ahaːr." </ta>
            <ta e="T127" id="Seg_3545" s="T118">Innʼe gɨnan emeːksin taksan barɨ oŋkučagar barɨkakaːnnarɨgar uː baspɨt. </ta>
            <ta e="T134" id="Seg_3546" s="T127">Araj bütehik holuːrgakakkakaːnɨn egeleːri gɨmmɨta, aːna katɨːlaːk. </ta>
            <ta e="T141" id="Seg_3547" s="T134">"Kaja, ogonnʼor, aːŋŋɨn as, ülüjeːri gɨnnɨm", di͡ebit. </ta>
            <ta e="T147" id="Seg_3548" s="T141">"Huːka kɨːha, kaːrga kampɨ bɨrak", di͡ebit. </ta>
            <ta e="T156" id="Seg_3549" s="T147">Emeːksine baː holuːrčagɨn bɨraːn baran dʼe toŋon ölöːrü gɨnar. </ta>
            <ta e="T165" id="Seg_3550" s="T156">Emeːksine balaganɨn ürdüger taksan kɨːmkaːnnar taksɨːlarɨgar onno iliːtin itter. </ta>
            <ta e="T176" id="Seg_3551" s="T165">Araj erin körbüte, atɨn hamagɨn ɨlan baran alčajan baran hačalana oloror. </ta>
            <ta e="T184" id="Seg_3552" s="T176">Ol itteːri gɨnar emeːksinin iliːtin üčehennen bɨhɨta kejer. </ta>
            <ta e="T195" id="Seg_3553" s="T184">Ontuta ɨtɨː-ɨtɨː hirge tüheːkteːn baran mastɨːr hirkeːn di͡eg ɨtɨː-ɨtɨː kaːma turar. </ta>
            <ta e="T208" id="Seg_3554" s="T195">Dʼe onno tiːtikun mas tördüger hɨtaːrɨ gɨmmɨt, hɨtaːrɨ gɨmmɨt, tɨmnɨː titiriːr hüreger kiːrer. </ta>
            <ta e="T219" id="Seg_3555" s="T208">Ol hɨttagɨna ajdaːn bögö ihillibit, taba üːrerten taba üːrer enin bagajɨ. </ta>
            <ta e="T225" id="Seg_3556" s="T219">Ol kotu dʼe barar bu emeːksin. </ta>
            <ta e="T240" id="Seg_3557" s="T225">Ol baran, ajɨː daganɨ, ɨjɨ bü͡ölüːr ɨːs tuman tabaga, künü bü͡ölüːr kuden tuman hü͡öhüge tijer. </ta>
            <ta e="T251" id="Seg_3558" s="T240">Bu emeːksiniŋ dʼi͡eni kördöːn bu tabanɨ teleriger ikki eneːre elejen kaːlbɨt. </ta>
            <ta e="T258" id="Seg_3559" s="T251">Araj alɨːnɨ tobus toloru uraha dʼi͡e tutullan turar. </ta>
            <ta e="T274" id="Seg_3560" s="T258">Ulakan mɨjaː dʼi͡e kelle, bu dʼi͡ege, manna dʼe kas u͡on kihi bɨha ölbügüne, bu͡olaga huburujan turar. </ta>
            <ta e="T282" id="Seg_3561" s="T274">Innʼe gɨnan baː emeːksin baː dʼi͡eni arɨja oksubut. </ta>
            <ta e="T287" id="Seg_3562" s="T282">Oduːlaːn körbüte — ibis iččitek, kihite hu͡ok. </ta>
            <ta e="T302" id="Seg_3563" s="T287">Barɨ kü͡ös kü͡östenen turar, barɨ as astanan turar, taːs kɨrabaːttar bütünnüː onnuk tergetteːk dʼi͡e turar. </ta>
            <ta e="T317" id="Seg_3564" s="T302">Ol emeːksin kiːren ahaːbatagɨn ahɨːr, hi͡ebetegin hiːr, nuːčča daː aha ügüs, tɨ͡a daː aha ügüs. </ta>
            <ta e="T325" id="Seg_3565" s="T317">Bu emeːksin huːnan-taraːnan baran üːs-kiːs taŋahɨ taŋnan keːspit. </ta>
            <ta e="T331" id="Seg_3566" s="T325">Bu tulkarɨ tu͡ok daː kihite billibet. </ta>
            <ta e="T338" id="Seg_3567" s="T331">Üs künü meldʼi tu͡ok daː kihite billibet. </ta>
            <ta e="T347" id="Seg_3568" s="T338">Araj ki͡ehe bu͡olu͡o daː ü͡ölehinen kur kihi mejiːlere tuheller. </ta>
            <ta e="T359" id="Seg_3569" s="T347">Oloru "tehiŋ-buhuŋ" [tehite bar] diːdiː tehite üktüːr, oloro hir di͡eg himitten iheller. </ta>
            <ta e="T377" id="Seg_3570" s="T359">Emeːksin kah u͡on tukkarɨ olorbut, tabata kanna daː kamnaːbat, attara hubu kördük hɨtɨllar, balɨktara hubu bultammɨt kurduk hɨllʼar. </ta>
            <ta e="T382" id="Seg_3571" s="T377">Emeːksin onon bajan-tajan hu͡oč hogotok oloror. </ta>
            <ta e="T387" id="Seg_3572" s="T382">Innʼe gɨnan emeːksin duːmata kelbit. </ta>
            <ta e="T395" id="Seg_3573" s="T387">"Ogoloːr, min oloru͡oktaːgar ogonnʼorum ɨratɨgar bilse barɨ͡ak baːrɨm." </ta>
            <ta e="T400" id="Seg_3574" s="T395">Ühüs hɨlɨgar ogonnʼorugar baraːrɨ taŋɨnna. </ta>
            <ta e="T404" id="Seg_3575" s="T400">Keli͡ebi bɨhɨtalaːn baran ɨlla. </ta>
            <ta e="T410" id="Seg_3576" s="T404">Ol gɨnan baran kɨːl hɨ͡atɨn ildʼi͡ehi. </ta>
            <ta e="T415" id="Seg_3577" s="T410">Innʼe gɨnan baran ogonnʼorugar barda. </ta>
            <ta e="T421" id="Seg_3578" s="T415">Bu baran baran ogonnʼorun golomotugar tijbit. </ta>
            <ta e="T430" id="Seg_3579" s="T421">Araj tu͡okkaːn du hu͡ok, bütünnüː barɨta tibillen huːlan kaːlbɨt. </ta>
            <ta e="T435" id="Seg_3580" s="T430">Ol da bu͡ollar üːlehiger ɨttɨbɨt. </ta>
            <ta e="T453" id="Seg_3581" s="T435">Araj ačaːgɨn tügürüččü üs tüːleːk kürdʼege bu͡olan dʼaːbɨlana hɨtar, atɨn kara tujagɨn gɨtta büttehin ere ordorbut atɨn gi͡enin. </ta>
            <ta e="T457" id="Seg_3582" s="T453">Maːgɨŋ kubulujbut obu͡oja ühü. </ta>
            <ta e="T462" id="Seg_3583" s="T457">Ol ihin ölümeːri kubulujbuta ühü. </ta>
            <ta e="T467" id="Seg_3584" s="T462">Dʼe onu baː kili͡ebinen bɨrakpɨt. </ta>
            <ta e="T472" id="Seg_3585" s="T467">Manɨ türdes gɨmmɨt daː eppit: </ta>
            <ta e="T478" id="Seg_3586" s="T472">"Baː ohogum bu͡ora hɨtɨ͡arɨmaːrɨ gɨmmɨt", diːr. </ta>
            <ta e="T480" id="Seg_3587" s="T478">Hɨ͡annan bɨrakpɨt. </ta>
            <ta e="T485" id="Seg_3588" s="T480">"ɨčča, koŋuraktar tühenner hɨtɨ͡arɨmaːrɨ gɨnnɨlar." </ta>
            <ta e="T487" id="Seg_3589" s="T485">Emi͡e bɨrakpɨt. </ta>
            <ta e="T492" id="Seg_3590" s="T487">Manɨ kiren körbüt — hɨ͡a ebit. </ta>
            <ta e="T497" id="Seg_3591" s="T492">Onu "taŋara, taŋara, čočumčakeːn" di͡ebit. </ta>
            <ta e="T503" id="Seg_3592" s="T497">"Kaja, ogonnʼor, aːŋŋɨn arɨj", di͡ebit emeːksine. </ta>
            <ta e="T506" id="Seg_3593" s="T503">Aːnɨn arɨjan obu͡ojdammɨt. </ta>
            <ta e="T511" id="Seg_3594" s="T506">Emeːksin ahatan baran dʼi͡etiger ilpit. </ta>
            <ta e="T524" id="Seg_3595" s="T511">Bu dʼi͡etiger ildʼen huːjan-taraːn baran tannɨbatak taŋahɨ taŋɨnnaran baran taːs kɨrabaːkka olordon keːspit. </ta>
            <ta e="T537" id="Seg_3596" s="T524">Ol gɨnan baran čaːj iherpit, onton et kü͡ös kotorbut, hɨ͡alaːk mini kɨtɨjaga kotorbut. </ta>
            <ta e="T543" id="Seg_3597" s="T537">Innʼe gɨnan kömüs lu͡osku, biːlke uːrbut. </ta>
            <ta e="T547" id="Seg_3598" s="T543">Araj lu͡oskutunan iheːri gɨmmɨt: </ta>
            <ta e="T558" id="Seg_3599" s="T547">"Kaja ba-ba, lu͡oskum dʼogus, ulakan pabaraːŋkɨta duː, komu͡osta duː egel", di͡ebit. </ta>
            <ta e="T563" id="Seg_3600" s="T558">Maːgɨta, komu͡os bagajɨta tuttaran keːspit. </ta>
            <ta e="T568" id="Seg_3601" s="T563">Hɨ͡alaːk mini baːtɨnan "huːp" gɨmmɨt. </ta>
            <ta e="T572" id="Seg_3602" s="T568">Manɨ͡aka dʼe čačajar diː. </ta>
            <ta e="T591" id="Seg_3603" s="T572">Dʼe onton "čačajabɨn" di͡en dʼe uturuktuːr diː, taːs olbogun bɨha uturuktuːr daː onon üs buluːs hiri tobulu tuhen kaːlar. </ta>
            <ta e="T600" id="Seg_3604" s="T591">Emeːksine tu͡ok bu͡olu͡ogaj, kajdak olorbuta daː ogurduk bi͡ek bi͡ek oloror. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_3605" s="T0">ogonnʼor-doːk</ta>
            <ta e="T2" id="Seg_3606" s="T1">emeːksin</ta>
            <ta e="T3" id="Seg_3607" s="T2">olor-but-tar</ta>
            <ta e="T4" id="Seg_3608" s="T3">ühü</ta>
            <ta e="T5" id="Seg_3609" s="T4">tör-ö-t-ör</ta>
            <ta e="T6" id="Seg_3610" s="T5">ogo-to</ta>
            <ta e="T7" id="Seg_3611" s="T6">hu͡ok-tar</ta>
            <ta e="T8" id="Seg_3612" s="T7">dʼi͡e-ge</ta>
            <ta e="T9" id="Seg_3613" s="T8">olor-dok-toruna</ta>
            <ta e="T10" id="Seg_3614" s="T9">u͡ot-tara</ta>
            <ta e="T11" id="Seg_3615" s="T10">ere</ta>
            <ta e="T12" id="Seg_3616" s="T11">kɨtar-ar</ta>
            <ta e="T13" id="Seg_3617" s="T12">ogurduk</ta>
            <ta e="T14" id="Seg_3618" s="T13">dʼadaŋɨ-lar</ta>
            <ta e="T15" id="Seg_3619" s="T14">tahaːra</ta>
            <ta e="T16" id="Seg_3620" s="T15">tagɨs-tak-tarɨna</ta>
            <ta e="T17" id="Seg_3621" s="T16">kaːr-dara</ta>
            <ta e="T18" id="Seg_3622" s="T17">ere</ta>
            <ta e="T19" id="Seg_3623" s="T18">turtaj-ar</ta>
            <ta e="T20" id="Seg_3624" s="T19">tu͡og-a</ta>
            <ta e="T21" id="Seg_3625" s="T20">daː</ta>
            <ta e="T22" id="Seg_3626" s="T21">hu͡ok-tar</ta>
            <ta e="T23" id="Seg_3627" s="T22">araj</ta>
            <ta e="T24" id="Seg_3628" s="T23">üp-tere</ta>
            <ta e="T25" id="Seg_3629" s="T24">di͡e-n-nere</ta>
            <ta e="T26" id="Seg_3630" s="T25">hu͡os-hogotok</ta>
            <ta e="T27" id="Seg_3631" s="T26">at-taːk-tar</ta>
            <ta e="T28" id="Seg_3632" s="T27">onton</ta>
            <ta e="T29" id="Seg_3633" s="T28">bultan-ar</ta>
            <ta e="T30" id="Seg_3634" s="T29">terget-tere</ta>
            <ta e="T31" id="Seg_3635" s="T30">üs</ta>
            <ta e="T32" id="Seg_3636" s="T31">taːs-taːk</ta>
            <ta e="T33" id="Seg_3637" s="T32">ilim-keːn-neːk-ter</ta>
            <ta e="T34" id="Seg_3638" s="T33">on-tu-larɨ-gar</ta>
            <ta e="T35" id="Seg_3639" s="T34">uː</ta>
            <ta e="T36" id="Seg_3640" s="T35">kurdʼaga-ta</ta>
            <ta e="T37" id="Seg_3641" s="T36">iːl-i-n-neg-ine</ta>
            <ta e="T38" id="Seg_3642" s="T37">o-nu</ta>
            <ta e="T39" id="Seg_3643" s="T38">hiː-l-ler</ta>
            <ta e="T40" id="Seg_3644" s="T39">üčehe-ge</ta>
            <ta e="T41" id="Seg_3645" s="T40">ü͡öl-en</ta>
            <ta e="T42" id="Seg_3646" s="T41">araj</ta>
            <ta e="T43" id="Seg_3647" s="T42">öl-öːrü</ta>
            <ta e="T44" id="Seg_3648" s="T43">ü͡ökej-bit-ter</ta>
            <ta e="T45" id="Seg_3649" s="T44">bɨst-aːrɨ</ta>
            <ta e="T46" id="Seg_3650" s="T45">bɨ͡akaj-bɨt-tar</ta>
            <ta e="T47" id="Seg_3651" s="T46">onton</ta>
            <ta e="T48" id="Seg_3652" s="T47">ogonnʼor-o</ta>
            <ta e="T49" id="Seg_3653" s="T48">araj</ta>
            <ta e="T50" id="Seg_3654" s="T49">kiːr-bit</ta>
            <ta e="T51" id="Seg_3655" s="T50">emeːksin-i-ger</ta>
            <ta e="T52" id="Seg_3656" s="T51">dʼe</ta>
            <ta e="T53" id="Seg_3657" s="T52">kiːr-en</ta>
            <ta e="T54" id="Seg_3658" s="T53">dʼi͡e-bit</ta>
            <ta e="T55" id="Seg_3659" s="T54">kaja</ta>
            <ta e="T56" id="Seg_3660" s="T55">emeːksin</ta>
            <ta e="T57" id="Seg_3661" s="T56">ürd-üː-büt</ta>
            <ta e="T58" id="Seg_3662" s="T57">kaːm-tar-ɨ͡ak-pɨt</ta>
            <ta e="T59" id="Seg_3663" s="T58">hogotok</ta>
            <ta e="T60" id="Seg_3664" s="T59">at-taːk-pɨt</ta>
            <ta e="T61" id="Seg_3665" s="T60">h-i͡ek-pit</ta>
            <ta e="T62" id="Seg_3666" s="T61">baːr-a</ta>
            <ta e="T63" id="Seg_3667" s="T62">onnoːgor</ta>
            <ta e="T64" id="Seg_3668" s="T63">okt-on</ta>
            <ta e="T65" id="Seg_3669" s="T64">öl-lök-pütüne</ta>
            <ta e="T66" id="Seg_3670" s="T65">kenni-biti-ger</ta>
            <ta e="T67" id="Seg_3671" s="T66">kaːl-ɨ͡ag-a</ta>
            <ta e="T68" id="Seg_3672" s="T67">onu-ga</ta>
            <ta e="T69" id="Seg_3673" s="T68">emeːksin</ta>
            <ta e="T70" id="Seg_3674" s="T69">beje-ŋ</ta>
            <ta e="T71" id="Seg_3675" s="T70">hanaː-ŋ</ta>
            <ta e="T72" id="Seg_3676" s="T71">di͡e-bit</ta>
            <ta e="T73" id="Seg_3677" s="T72">ölör-dör-gün</ta>
            <ta e="T74" id="Seg_3678" s="T73">ere</ta>
            <ta e="T75" id="Seg_3679" s="T74">h-i͡ek-pit</ta>
            <ta e="T76" id="Seg_3680" s="T75">er-e</ta>
            <ta e="T77" id="Seg_3681" s="T76">tagɨs-pɨt</ta>
            <ta e="T78" id="Seg_3682" s="T77">daː</ta>
            <ta e="T79" id="Seg_3683" s="T78">ölör-büt</ta>
            <ta e="T80" id="Seg_3684" s="T79">ogonnʼor-o</ta>
            <ta e="T81" id="Seg_3685" s="T80">hül-büt</ta>
            <ta e="T82" id="Seg_3686" s="T81">araj</ta>
            <ta e="T83" id="Seg_3687" s="T82">tɨmnɨː</ta>
            <ta e="T84" id="Seg_3688" s="T83">bagajɨ</ta>
            <ta e="T85" id="Seg_3689" s="T84">čɨskaːn</ta>
            <ta e="T86" id="Seg_3690" s="T85">bagajɨ</ta>
            <ta e="T87" id="Seg_3691" s="T86">ogonnʼor-o</ta>
            <ta e="T88" id="Seg_3692" s="T87">kiːr-bit</ta>
            <ta e="T89" id="Seg_3693" s="T88">kiːr-en</ta>
            <ta e="T90" id="Seg_3694" s="T89">di͡e-bit</ta>
            <ta e="T91" id="Seg_3695" s="T90">ap-pɨt</ta>
            <ta e="T92" id="Seg_3696" s="T91">et-i-n</ta>
            <ta e="T93" id="Seg_3697" s="T92">bütün-nüː</ta>
            <ta e="T94" id="Seg_3698" s="T93">golomo-koːm-mut</ta>
            <ta e="T95" id="Seg_3699" s="T94">uŋu͡or</ta>
            <ta e="T96" id="Seg_3700" s="T95">taskaj-daː-n</ta>
            <ta e="T97" id="Seg_3701" s="T96">keːs</ta>
            <ta e="T98" id="Seg_3702" s="T97">čɨːsta-tɨ-n</ta>
            <ta e="T99" id="Seg_3703" s="T98">emeːksin-e</ta>
            <ta e="T100" id="Seg_3704" s="T99">barɨ-tɨ-n</ta>
            <ta e="T101" id="Seg_3705" s="T100">taskaj-daː-bɨt</ta>
            <ta e="T102" id="Seg_3706" s="T101">emeːksin-e</ta>
            <ta e="T103" id="Seg_3707" s="T102">iliː-ti-n</ta>
            <ta e="T104" id="Seg_3708" s="T103">it-t-eːri</ta>
            <ta e="T105" id="Seg_3709" s="T104">kɨm-mɨt-ɨ-n</ta>
            <ta e="T106" id="Seg_3710" s="T105">ogonnʼor</ta>
            <ta e="T107" id="Seg_3711" s="T106">ep-pit</ta>
            <ta e="T108" id="Seg_3712" s="T107">h-i͡ek-ki-n</ta>
            <ta e="T109" id="Seg_3713" s="T108">barɨ</ta>
            <ta e="T110" id="Seg_3714" s="T109">ihik-ke-r</ta>
            <ta e="T111" id="Seg_3715" s="T110">holuːr-ga-r</ta>
            <ta e="T112" id="Seg_3716" s="T111">uː-ta</ta>
            <ta e="T113" id="Seg_3717" s="T112">bah-an</ta>
            <ta e="T114" id="Seg_3718" s="T113">tolor</ta>
            <ta e="T115" id="Seg_3719" s="T114">belem-neː-n</ta>
            <ta e="T116" id="Seg_3720" s="T115">baran</ta>
            <ta e="T117" id="Seg_3721" s="T116">dʼe</ta>
            <ta e="T118" id="Seg_3722" s="T117">ah-aːr</ta>
            <ta e="T119" id="Seg_3723" s="T118">innʼe</ta>
            <ta e="T120" id="Seg_3724" s="T119">gɨn-an</ta>
            <ta e="T121" id="Seg_3725" s="T120">emeːksin</ta>
            <ta e="T122" id="Seg_3726" s="T121">taks-an</ta>
            <ta e="T123" id="Seg_3727" s="T122">barɨ</ta>
            <ta e="T124" id="Seg_3728" s="T123">oŋkučag-a-r</ta>
            <ta e="T125" id="Seg_3729" s="T124">barɨka-kaːn-narɨ-gar</ta>
            <ta e="T126" id="Seg_3730" s="T125">uː</ta>
            <ta e="T127" id="Seg_3731" s="T126">bas-pɨt</ta>
            <ta e="T128" id="Seg_3732" s="T127">araj</ta>
            <ta e="T129" id="Seg_3733" s="T128">bütehik</ta>
            <ta e="T130" id="Seg_3734" s="T129">holuːr-ga-ka-kka-kaːn-ɨ-n</ta>
            <ta e="T131" id="Seg_3735" s="T130">egel-eːri</ta>
            <ta e="T132" id="Seg_3736" s="T131">gɨm-mɨt-a</ta>
            <ta e="T133" id="Seg_3737" s="T132">aːn-a</ta>
            <ta e="T134" id="Seg_3738" s="T133">katɨːlaːk</ta>
            <ta e="T135" id="Seg_3739" s="T134">kaja</ta>
            <ta e="T136" id="Seg_3740" s="T135">ogonnʼor</ta>
            <ta e="T137" id="Seg_3741" s="T136">aːŋ-ŋɨ-n</ta>
            <ta e="T138" id="Seg_3742" s="T137">as</ta>
            <ta e="T139" id="Seg_3743" s="T138">ülüj-eːri</ta>
            <ta e="T140" id="Seg_3744" s="T139">gɨn-nɨ-m</ta>
            <ta e="T141" id="Seg_3745" s="T140">di͡e-bit</ta>
            <ta e="T142" id="Seg_3746" s="T141">huːka</ta>
            <ta e="T143" id="Seg_3747" s="T142">kɨːh-a</ta>
            <ta e="T144" id="Seg_3748" s="T143">kaːr-ga</ta>
            <ta e="T145" id="Seg_3749" s="T144">kampɨ</ta>
            <ta e="T146" id="Seg_3750" s="T145">bɨrak</ta>
            <ta e="T147" id="Seg_3751" s="T146">di͡e-bit</ta>
            <ta e="T148" id="Seg_3752" s="T147">emeːksin-e</ta>
            <ta e="T149" id="Seg_3753" s="T148">baː</ta>
            <ta e="T150" id="Seg_3754" s="T149">holuːr-čag-ɨ-n</ta>
            <ta e="T151" id="Seg_3755" s="T150">bɨraː-n</ta>
            <ta e="T152" id="Seg_3756" s="T151">baran</ta>
            <ta e="T153" id="Seg_3757" s="T152">dʼe</ta>
            <ta e="T154" id="Seg_3758" s="T153">toŋ-on</ta>
            <ta e="T155" id="Seg_3759" s="T154">öl-öːrü</ta>
            <ta e="T156" id="Seg_3760" s="T155">gɨn-ar</ta>
            <ta e="T157" id="Seg_3761" s="T156">emeːksin-e</ta>
            <ta e="T158" id="Seg_3762" s="T157">balagan-ɨ-n</ta>
            <ta e="T159" id="Seg_3763" s="T158">ürd-ü-ger</ta>
            <ta e="T160" id="Seg_3764" s="T159">taks-an</ta>
            <ta e="T161" id="Seg_3765" s="T160">kɨːm-kaːn-nar</ta>
            <ta e="T162" id="Seg_3766" s="T161">taks-ɨː-larɨ-gar</ta>
            <ta e="T163" id="Seg_3767" s="T162">onno</ta>
            <ta e="T164" id="Seg_3768" s="T163">iliː-ti-n</ta>
            <ta e="T165" id="Seg_3769" s="T164">it-t-er</ta>
            <ta e="T166" id="Seg_3770" s="T165">araj</ta>
            <ta e="T167" id="Seg_3771" s="T166">er-i-n</ta>
            <ta e="T168" id="Seg_3772" s="T167">kör-büt-e</ta>
            <ta e="T169" id="Seg_3773" s="T168">at-ɨ-n</ta>
            <ta e="T170" id="Seg_3774" s="T169">hamag-ɨ-n</ta>
            <ta e="T171" id="Seg_3775" s="T170">ɨl-an</ta>
            <ta e="T172" id="Seg_3776" s="T171">baran</ta>
            <ta e="T173" id="Seg_3777" s="T172">alčaj-an</ta>
            <ta e="T174" id="Seg_3778" s="T173">baran</ta>
            <ta e="T175" id="Seg_3779" s="T174">hača-lan-a</ta>
            <ta e="T176" id="Seg_3780" s="T175">olor-or</ta>
            <ta e="T177" id="Seg_3781" s="T176">ol</ta>
            <ta e="T178" id="Seg_3782" s="T177">it-t-eːri</ta>
            <ta e="T179" id="Seg_3783" s="T178">gɨn-ar</ta>
            <ta e="T180" id="Seg_3784" s="T179">emeːksin-i-n</ta>
            <ta e="T181" id="Seg_3785" s="T180">iliː-ti-n</ta>
            <ta e="T182" id="Seg_3786" s="T181">üčehe-nnen</ta>
            <ta e="T183" id="Seg_3787" s="T182">bɨhɨt-a</ta>
            <ta e="T184" id="Seg_3788" s="T183">kej-er</ta>
            <ta e="T185" id="Seg_3789" s="T184">on-tu-ta</ta>
            <ta e="T186" id="Seg_3790" s="T185">ɨt-ɨː-ɨt-ɨː</ta>
            <ta e="T187" id="Seg_3791" s="T186">hir-ge</ta>
            <ta e="T188" id="Seg_3792" s="T187">tüh-eːkteː-n</ta>
            <ta e="T189" id="Seg_3793" s="T188">baran</ta>
            <ta e="T190" id="Seg_3794" s="T189">mas-tɨː-r</ta>
            <ta e="T191" id="Seg_3795" s="T190">hir-keːn</ta>
            <ta e="T192" id="Seg_3796" s="T191">di͡eg</ta>
            <ta e="T193" id="Seg_3797" s="T192">ɨt-ɨː-ɨt-ɨː</ta>
            <ta e="T194" id="Seg_3798" s="T193">kaːm-a</ta>
            <ta e="T195" id="Seg_3799" s="T194">tur-ar</ta>
            <ta e="T196" id="Seg_3800" s="T195">dʼe</ta>
            <ta e="T197" id="Seg_3801" s="T196">onno</ta>
            <ta e="T198" id="Seg_3802" s="T197">tiːt-i-kun</ta>
            <ta e="T199" id="Seg_3803" s="T198">mas</ta>
            <ta e="T200" id="Seg_3804" s="T199">törd-ü-ger</ta>
            <ta e="T201" id="Seg_3805" s="T200">hɨt-aːrɨ</ta>
            <ta e="T202" id="Seg_3806" s="T201">gɨm-mɨt</ta>
            <ta e="T203" id="Seg_3807" s="T202">hɨt-aːrɨ</ta>
            <ta e="T204" id="Seg_3808" s="T203">gɨm-mɨt</ta>
            <ta e="T205" id="Seg_3809" s="T204">tɨmnɨː</ta>
            <ta e="T206" id="Seg_3810" s="T205">titiriː-r</ta>
            <ta e="T207" id="Seg_3811" s="T206">hüreg-e-r</ta>
            <ta e="T208" id="Seg_3812" s="T207">kiːr-er</ta>
            <ta e="T209" id="Seg_3813" s="T208">ol</ta>
            <ta e="T210" id="Seg_3814" s="T209">hɨt-tag-ɨna</ta>
            <ta e="T211" id="Seg_3815" s="T210">ajdaːn</ta>
            <ta e="T212" id="Seg_3816" s="T211">bögö</ta>
            <ta e="T213" id="Seg_3817" s="T212">ihill-i-bit</ta>
            <ta e="T214" id="Seg_3818" s="T213">taba</ta>
            <ta e="T215" id="Seg_3819" s="T214">üːr-er-ten</ta>
            <ta e="T216" id="Seg_3820" s="T215">taba</ta>
            <ta e="T217" id="Seg_3821" s="T216">üːr-er</ta>
            <ta e="T218" id="Seg_3822" s="T217">enin</ta>
            <ta e="T219" id="Seg_3823" s="T218">bagajɨ</ta>
            <ta e="T220" id="Seg_3824" s="T219">ol</ta>
            <ta e="T221" id="Seg_3825" s="T220">kotu</ta>
            <ta e="T222" id="Seg_3826" s="T221">dʼe</ta>
            <ta e="T223" id="Seg_3827" s="T222">bar-ar</ta>
            <ta e="T224" id="Seg_3828" s="T223">bu</ta>
            <ta e="T225" id="Seg_3829" s="T224">emeːksin</ta>
            <ta e="T226" id="Seg_3830" s="T225">ol</ta>
            <ta e="T227" id="Seg_3831" s="T226">bar-an</ta>
            <ta e="T228" id="Seg_3832" s="T227">ajɨː</ta>
            <ta e="T229" id="Seg_3833" s="T228">daganɨ</ta>
            <ta e="T230" id="Seg_3834" s="T229">ɨj-ɨ</ta>
            <ta e="T231" id="Seg_3835" s="T230">bü͡ölüː-r</ta>
            <ta e="T232" id="Seg_3836" s="T231">ɨːs</ta>
            <ta e="T233" id="Seg_3837" s="T232">tuman</ta>
            <ta e="T234" id="Seg_3838" s="T233">taba-ga</ta>
            <ta e="T235" id="Seg_3839" s="T234">kün-ü</ta>
            <ta e="T236" id="Seg_3840" s="T235">bü͡ölüː-r</ta>
            <ta e="T237" id="Seg_3841" s="T236">kuden</ta>
            <ta e="T238" id="Seg_3842" s="T237">tuman</ta>
            <ta e="T239" id="Seg_3843" s="T238">hü͡öhü-ge</ta>
            <ta e="T240" id="Seg_3844" s="T239">tij-er</ta>
            <ta e="T241" id="Seg_3845" s="T240">bu</ta>
            <ta e="T242" id="Seg_3846" s="T241">emeːksin-i-ŋ</ta>
            <ta e="T243" id="Seg_3847" s="T242">dʼi͡e-ni</ta>
            <ta e="T244" id="Seg_3848" s="T243">kördöː-n</ta>
            <ta e="T245" id="Seg_3849" s="T244">bu</ta>
            <ta e="T246" id="Seg_3850" s="T245">taba-nɨ</ta>
            <ta e="T247" id="Seg_3851" s="T246">tel-er-i-ger</ta>
            <ta e="T248" id="Seg_3852" s="T247">ikki</ta>
            <ta e="T249" id="Seg_3853" s="T248">eneːr-e</ta>
            <ta e="T250" id="Seg_3854" s="T249">elej-en</ta>
            <ta e="T251" id="Seg_3855" s="T250">kaːl-bɨt</ta>
            <ta e="T252" id="Seg_3856" s="T251">araj</ta>
            <ta e="T253" id="Seg_3857" s="T252">alɨː-nɨ</ta>
            <ta e="T254" id="Seg_3858" s="T253">tobus-toloru</ta>
            <ta e="T255" id="Seg_3859" s="T254">uraha</ta>
            <ta e="T256" id="Seg_3860" s="T255">dʼi͡e</ta>
            <ta e="T257" id="Seg_3861" s="T256">tut-u-ll-an</ta>
            <ta e="T258" id="Seg_3862" s="T257">tur-ar</ta>
            <ta e="T259" id="Seg_3863" s="T258">ulakan</ta>
            <ta e="T260" id="Seg_3864" s="T259">mɨjaː</ta>
            <ta e="T261" id="Seg_3865" s="T260">dʼi͡e</ta>
            <ta e="T262" id="Seg_3866" s="T261">kel-l-e</ta>
            <ta e="T263" id="Seg_3867" s="T262">bu</ta>
            <ta e="T264" id="Seg_3868" s="T263">dʼi͡e-ge</ta>
            <ta e="T265" id="Seg_3869" s="T264">manna</ta>
            <ta e="T266" id="Seg_3870" s="T265">dʼe</ta>
            <ta e="T267" id="Seg_3871" s="T266">kas</ta>
            <ta e="T268" id="Seg_3872" s="T267">u͡on</ta>
            <ta e="T269" id="Seg_3873" s="T268">kihi</ta>
            <ta e="T270" id="Seg_3874" s="T269">bɨh-a</ta>
            <ta e="T271" id="Seg_3875" s="T270">ölbügün-e</ta>
            <ta e="T272" id="Seg_3876" s="T271">bu͡olag-a</ta>
            <ta e="T273" id="Seg_3877" s="T272">huburuj-an</ta>
            <ta e="T274" id="Seg_3878" s="T273">tur-ar</ta>
            <ta e="T275" id="Seg_3879" s="T274">innʼe</ta>
            <ta e="T276" id="Seg_3880" s="T275">gɨn-an</ta>
            <ta e="T277" id="Seg_3881" s="T276">baː</ta>
            <ta e="T278" id="Seg_3882" s="T277">emeːksin</ta>
            <ta e="T279" id="Seg_3883" s="T278">baː</ta>
            <ta e="T280" id="Seg_3884" s="T279">dʼi͡e-ni</ta>
            <ta e="T281" id="Seg_3885" s="T280">arɨj-a</ta>
            <ta e="T282" id="Seg_3886" s="T281">oks-u-but</ta>
            <ta e="T283" id="Seg_3887" s="T282">oduːlaː-n</ta>
            <ta e="T284" id="Seg_3888" s="T283">kör-büt-e</ta>
            <ta e="T285" id="Seg_3889" s="T284">ibis-iččitek</ta>
            <ta e="T286" id="Seg_3890" s="T285">kihi-te</ta>
            <ta e="T287" id="Seg_3891" s="T286">hu͡ok</ta>
            <ta e="T288" id="Seg_3892" s="T287">barɨ</ta>
            <ta e="T289" id="Seg_3893" s="T288">kü͡ös</ta>
            <ta e="T290" id="Seg_3894" s="T289">kü͡ös-ten-en</ta>
            <ta e="T291" id="Seg_3895" s="T290">tur-ar</ta>
            <ta e="T292" id="Seg_3896" s="T291">barɨ</ta>
            <ta e="T293" id="Seg_3897" s="T292">as</ta>
            <ta e="T294" id="Seg_3898" s="T293">as-tan-an</ta>
            <ta e="T295" id="Seg_3899" s="T294">tur-ar</ta>
            <ta e="T296" id="Seg_3900" s="T295">taːs</ta>
            <ta e="T297" id="Seg_3901" s="T296">kɨrabaːt-tar</ta>
            <ta e="T298" id="Seg_3902" s="T297">bütün-nüː</ta>
            <ta e="T299" id="Seg_3903" s="T298">onnuk</ta>
            <ta e="T300" id="Seg_3904" s="T299">terget-teːk</ta>
            <ta e="T301" id="Seg_3905" s="T300">dʼi͡e</ta>
            <ta e="T302" id="Seg_3906" s="T301">tur-ar</ta>
            <ta e="T303" id="Seg_3907" s="T302">ol</ta>
            <ta e="T304" id="Seg_3908" s="T303">emeːksin</ta>
            <ta e="T305" id="Seg_3909" s="T304">kiːr-en</ta>
            <ta e="T306" id="Seg_3910" s="T305">ahaː-batag-ɨ-n</ta>
            <ta e="T307" id="Seg_3911" s="T306">ahɨː-r</ta>
            <ta e="T308" id="Seg_3912" s="T307">hi͡e-beteg-i-n</ta>
            <ta e="T309" id="Seg_3913" s="T308">hiː-r</ta>
            <ta e="T310" id="Seg_3914" s="T309">nuːčča</ta>
            <ta e="T311" id="Seg_3915" s="T310">daː</ta>
            <ta e="T312" id="Seg_3916" s="T311">ah-a</ta>
            <ta e="T313" id="Seg_3917" s="T312">ügüs</ta>
            <ta e="T314" id="Seg_3918" s="T313">tɨ͡a</ta>
            <ta e="T315" id="Seg_3919" s="T314">daː</ta>
            <ta e="T316" id="Seg_3920" s="T315">ah-a</ta>
            <ta e="T317" id="Seg_3921" s="T316">ügüs</ta>
            <ta e="T318" id="Seg_3922" s="T317">bu</ta>
            <ta e="T319" id="Seg_3923" s="T318">emeːksin</ta>
            <ta e="T320" id="Seg_3924" s="T319">huːn-an-taraːn-an</ta>
            <ta e="T321" id="Seg_3925" s="T320">baran</ta>
            <ta e="T322" id="Seg_3926" s="T321">üːs-kiːs</ta>
            <ta e="T323" id="Seg_3927" s="T322">taŋah-ɨ</ta>
            <ta e="T324" id="Seg_3928" s="T323">taŋn-an</ta>
            <ta e="T325" id="Seg_3929" s="T324">keːs-pit</ta>
            <ta e="T326" id="Seg_3930" s="T325">bu</ta>
            <ta e="T327" id="Seg_3931" s="T326">tulkarɨ</ta>
            <ta e="T328" id="Seg_3932" s="T327">tu͡ok</ta>
            <ta e="T329" id="Seg_3933" s="T328">daː</ta>
            <ta e="T330" id="Seg_3934" s="T329">kihi-te</ta>
            <ta e="T331" id="Seg_3935" s="T330">bil-l-i-bet</ta>
            <ta e="T332" id="Seg_3936" s="T331">üs</ta>
            <ta e="T333" id="Seg_3937" s="T332">kün-ü</ta>
            <ta e="T334" id="Seg_3938" s="T333">meldʼi</ta>
            <ta e="T335" id="Seg_3939" s="T334">tu͡ok</ta>
            <ta e="T336" id="Seg_3940" s="T335">daː</ta>
            <ta e="T337" id="Seg_3941" s="T336">kihi-te</ta>
            <ta e="T338" id="Seg_3942" s="T337">bil-l-i-bet</ta>
            <ta e="T339" id="Seg_3943" s="T338">araj</ta>
            <ta e="T340" id="Seg_3944" s="T339">ki͡ehe</ta>
            <ta e="T341" id="Seg_3945" s="T340">bu͡ol-u͡o</ta>
            <ta e="T342" id="Seg_3946" s="T341">daː</ta>
            <ta e="T343" id="Seg_3947" s="T342">ü͡öleh-i-nen</ta>
            <ta e="T344" id="Seg_3948" s="T343">kur</ta>
            <ta e="T345" id="Seg_3949" s="T344">kihi</ta>
            <ta e="T346" id="Seg_3950" s="T345">mejiː-ler-e</ta>
            <ta e="T347" id="Seg_3951" s="T346">tuh-el-ler</ta>
            <ta e="T348" id="Seg_3952" s="T347">o-lor-u</ta>
            <ta e="T349" id="Seg_3953" s="T348">teh-i-ŋ-buh-u-ŋ</ta>
            <ta e="T350" id="Seg_3954" s="T349">teh-i-t-e</ta>
            <ta e="T351" id="Seg_3955" s="T350">bar</ta>
            <ta e="T352" id="Seg_3956" s="T351">d-iː-d-iː</ta>
            <ta e="T353" id="Seg_3957" s="T352">teh-i-t-e</ta>
            <ta e="T354" id="Seg_3958" s="T353">üktüː-r</ta>
            <ta e="T355" id="Seg_3959" s="T354">o-lor-o</ta>
            <ta e="T356" id="Seg_3960" s="T355">hir</ta>
            <ta e="T357" id="Seg_3961" s="T356">di͡eg</ta>
            <ta e="T358" id="Seg_3962" s="T357">himitt-en</ta>
            <ta e="T359" id="Seg_3963" s="T358">ih-el-ler</ta>
            <ta e="T360" id="Seg_3964" s="T359">emeːksin</ta>
            <ta e="T361" id="Seg_3965" s="T360">kah</ta>
            <ta e="T362" id="Seg_3966" s="T361">u͡on</ta>
            <ta e="T363" id="Seg_3967" s="T362">tukkarɨ</ta>
            <ta e="T364" id="Seg_3968" s="T363">olor-but</ta>
            <ta e="T365" id="Seg_3969" s="T364">taba-ta</ta>
            <ta e="T366" id="Seg_3970" s="T365">kanna</ta>
            <ta e="T367" id="Seg_3971" s="T366">daː</ta>
            <ta e="T368" id="Seg_3972" s="T367">kamnaː-bat</ta>
            <ta e="T369" id="Seg_3973" s="T368">at-tar-a</ta>
            <ta e="T370" id="Seg_3974" s="T369">hubu</ta>
            <ta e="T371" id="Seg_3975" s="T370">kördük</ta>
            <ta e="T372" id="Seg_3976" s="T371">hɨt-ɨl-lar</ta>
            <ta e="T373" id="Seg_3977" s="T372">balɨk-tara</ta>
            <ta e="T374" id="Seg_3978" s="T373">hubu</ta>
            <ta e="T375" id="Seg_3979" s="T374">bult-a-m-mɨt</ta>
            <ta e="T376" id="Seg_3980" s="T375">kurduk</ta>
            <ta e="T377" id="Seg_3981" s="T376">hɨllʼ-ar</ta>
            <ta e="T378" id="Seg_3982" s="T377">emeːksin</ta>
            <ta e="T379" id="Seg_3983" s="T378">onon</ta>
            <ta e="T380" id="Seg_3984" s="T379">baj-an-taj-an</ta>
            <ta e="T381" id="Seg_3985" s="T380">hu͡oč-hogotok</ta>
            <ta e="T382" id="Seg_3986" s="T381">olor-or</ta>
            <ta e="T383" id="Seg_3987" s="T382">innʼe</ta>
            <ta e="T384" id="Seg_3988" s="T383">gɨn-an</ta>
            <ta e="T385" id="Seg_3989" s="T384">emeːksin</ta>
            <ta e="T386" id="Seg_3990" s="T385">duːma-ta</ta>
            <ta e="T387" id="Seg_3991" s="T386">kel-bit</ta>
            <ta e="T388" id="Seg_3992" s="T387">ogo-loːr</ta>
            <ta e="T389" id="Seg_3993" s="T388">min</ta>
            <ta e="T390" id="Seg_3994" s="T389">olor-u͡ok-taːgar</ta>
            <ta e="T391" id="Seg_3995" s="T390">ogonnʼor-u-m</ta>
            <ta e="T392" id="Seg_3996" s="T391">ɨrat-ɨ-gar</ta>
            <ta e="T393" id="Seg_3997" s="T392">bil-s-e</ta>
            <ta e="T394" id="Seg_3998" s="T393">bar-ɨ͡ak</ta>
            <ta e="T395" id="Seg_3999" s="T394">baːr-ɨ-m</ta>
            <ta e="T396" id="Seg_4000" s="T395">üh-üs</ta>
            <ta e="T397" id="Seg_4001" s="T396">hɨl-ɨ-gar</ta>
            <ta e="T398" id="Seg_4002" s="T397">ogonnʼor-u-gar</ta>
            <ta e="T399" id="Seg_4003" s="T398">bar-aːrɨ</ta>
            <ta e="T400" id="Seg_4004" s="T399">taŋɨn-n-a</ta>
            <ta e="T401" id="Seg_4005" s="T400">keli͡eb-i</ta>
            <ta e="T402" id="Seg_4006" s="T401">bɨh-ɨtalaː-n</ta>
            <ta e="T403" id="Seg_4007" s="T402">baran</ta>
            <ta e="T404" id="Seg_4008" s="T403">ɨl-l-a</ta>
            <ta e="T405" id="Seg_4009" s="T404">ol</ta>
            <ta e="T406" id="Seg_4010" s="T405">gɨn-an</ta>
            <ta e="T407" id="Seg_4011" s="T406">baran</ta>
            <ta e="T408" id="Seg_4012" s="T407">kɨːl</ta>
            <ta e="T409" id="Seg_4013" s="T408">hɨ͡a-tɨ-n</ta>
            <ta e="T410" id="Seg_4014" s="T409">ildʼ-i͡ehi</ta>
            <ta e="T411" id="Seg_4015" s="T410">innʼe</ta>
            <ta e="T412" id="Seg_4016" s="T411">gɨn-an</ta>
            <ta e="T413" id="Seg_4017" s="T412">baran</ta>
            <ta e="T414" id="Seg_4018" s="T413">ogonnʼor-u-gar</ta>
            <ta e="T415" id="Seg_4019" s="T414">bar-d-a</ta>
            <ta e="T416" id="Seg_4020" s="T415">bu</ta>
            <ta e="T417" id="Seg_4021" s="T416">bar-an</ta>
            <ta e="T418" id="Seg_4022" s="T417">bar-an</ta>
            <ta e="T419" id="Seg_4023" s="T418">ogonnʼor-u-n</ta>
            <ta e="T420" id="Seg_4024" s="T419">golomo-tu-gar</ta>
            <ta e="T421" id="Seg_4025" s="T420">tij-bit</ta>
            <ta e="T422" id="Seg_4026" s="T421">araj</ta>
            <ta e="T423" id="Seg_4027" s="T422">tu͡ok-kaːn</ta>
            <ta e="T424" id="Seg_4028" s="T423">du</ta>
            <ta e="T425" id="Seg_4029" s="T424">hu͡ok</ta>
            <ta e="T426" id="Seg_4030" s="T425">bütün-nüː</ta>
            <ta e="T427" id="Seg_4031" s="T426">barɨta</ta>
            <ta e="T428" id="Seg_4032" s="T427">tib-i-ll-en</ta>
            <ta e="T429" id="Seg_4033" s="T428">huːl-an</ta>
            <ta e="T430" id="Seg_4034" s="T429">kaːl-bɨt</ta>
            <ta e="T431" id="Seg_4035" s="T430">ol</ta>
            <ta e="T432" id="Seg_4036" s="T431">da</ta>
            <ta e="T433" id="Seg_4037" s="T432">bu͡ol-lar</ta>
            <ta e="T434" id="Seg_4038" s="T433">üːleh-i-ger</ta>
            <ta e="T435" id="Seg_4039" s="T434">ɨtt-ɨ-bɨt</ta>
            <ta e="T436" id="Seg_4040" s="T435">araj</ta>
            <ta e="T437" id="Seg_4041" s="T436">ačaːg-ɨ-n</ta>
            <ta e="T438" id="Seg_4042" s="T437">tügürüččü</ta>
            <ta e="T439" id="Seg_4043" s="T438">üs</ta>
            <ta e="T440" id="Seg_4044" s="T439">tüː-leːk</ta>
            <ta e="T441" id="Seg_4045" s="T440">kürdʼege</ta>
            <ta e="T442" id="Seg_4046" s="T441">bu͡ol-an</ta>
            <ta e="T443" id="Seg_4047" s="T442">dʼaːbɨlan-a</ta>
            <ta e="T444" id="Seg_4048" s="T443">hɨt-ar</ta>
            <ta e="T445" id="Seg_4049" s="T444">at-ɨ-n</ta>
            <ta e="T446" id="Seg_4050" s="T445">kara</ta>
            <ta e="T447" id="Seg_4051" s="T446">tujag-ɨ-n</ta>
            <ta e="T448" id="Seg_4052" s="T447">gɨtta</ta>
            <ta e="T449" id="Seg_4053" s="T448">büt-t-e-h-i-n</ta>
            <ta e="T450" id="Seg_4054" s="T449">ere</ta>
            <ta e="T451" id="Seg_4055" s="T450">or-dor-but</ta>
            <ta e="T452" id="Seg_4056" s="T451">at-ɨ-n</ta>
            <ta e="T453" id="Seg_4057" s="T452">gi͡en-i-n</ta>
            <ta e="T454" id="Seg_4058" s="T453">maːgɨŋ</ta>
            <ta e="T455" id="Seg_4059" s="T454">kubuluj-but</ta>
            <ta e="T456" id="Seg_4060" s="T455">obu͡oj-a</ta>
            <ta e="T457" id="Seg_4061" s="T456">ühü</ta>
            <ta e="T458" id="Seg_4062" s="T457">ol</ta>
            <ta e="T459" id="Seg_4063" s="T458">ihin</ta>
            <ta e="T460" id="Seg_4064" s="T459">öl-ü-m-eːri</ta>
            <ta e="T461" id="Seg_4065" s="T460">kubuluj-but-a</ta>
            <ta e="T462" id="Seg_4066" s="T461">ühü</ta>
            <ta e="T463" id="Seg_4067" s="T462">dʼe</ta>
            <ta e="T464" id="Seg_4068" s="T463">o-nu</ta>
            <ta e="T465" id="Seg_4069" s="T464">baː</ta>
            <ta e="T466" id="Seg_4070" s="T465">kili͡eb-i-nen</ta>
            <ta e="T467" id="Seg_4071" s="T466">bɨrak-pɨt</ta>
            <ta e="T468" id="Seg_4072" s="T467">ma-nɨ</ta>
            <ta e="T469" id="Seg_4073" s="T468">türd-e-s</ta>
            <ta e="T470" id="Seg_4074" s="T469">gɨm-mɨt</ta>
            <ta e="T471" id="Seg_4075" s="T470">daː</ta>
            <ta e="T472" id="Seg_4076" s="T471">ep-pit</ta>
            <ta e="T473" id="Seg_4077" s="T472">baː</ta>
            <ta e="T474" id="Seg_4078" s="T473">ohog-u-m</ta>
            <ta e="T475" id="Seg_4079" s="T474">bu͡or-a</ta>
            <ta e="T476" id="Seg_4080" s="T475">hɨt-ɨ͡ar-ɨ-m-aːrɨ</ta>
            <ta e="T477" id="Seg_4081" s="T476">gɨm-mɨt</ta>
            <ta e="T478" id="Seg_4082" s="T477">diː-r</ta>
            <ta e="T479" id="Seg_4083" s="T478">hɨ͡a-nnan</ta>
            <ta e="T480" id="Seg_4084" s="T479">bɨrak-pɨt</ta>
            <ta e="T481" id="Seg_4085" s="T480">ɨčča</ta>
            <ta e="T482" id="Seg_4086" s="T481">koŋurak-tar</ta>
            <ta e="T483" id="Seg_4087" s="T482">tüh-en-ner</ta>
            <ta e="T484" id="Seg_4088" s="T483">hɨt-ɨ͡ar-ɨ-m-aːrɨ</ta>
            <ta e="T485" id="Seg_4089" s="T484">gɨn-nɨ-lar</ta>
            <ta e="T486" id="Seg_4090" s="T485">emi͡e</ta>
            <ta e="T487" id="Seg_4091" s="T486">bɨrak-pɨt</ta>
            <ta e="T488" id="Seg_4092" s="T487">ma-nɨ</ta>
            <ta e="T489" id="Seg_4093" s="T488">kir-en</ta>
            <ta e="T490" id="Seg_4094" s="T489">kör-büt</ta>
            <ta e="T491" id="Seg_4095" s="T490">hɨ͡a</ta>
            <ta e="T492" id="Seg_4096" s="T491">e-bit</ta>
            <ta e="T493" id="Seg_4097" s="T492">o-nu</ta>
            <ta e="T494" id="Seg_4098" s="T493">taŋara</ta>
            <ta e="T495" id="Seg_4099" s="T494">taŋara</ta>
            <ta e="T496" id="Seg_4100" s="T495">čočum-ča-keːn</ta>
            <ta e="T497" id="Seg_4101" s="T496">di͡e-bit</ta>
            <ta e="T498" id="Seg_4102" s="T497">kaja</ta>
            <ta e="T499" id="Seg_4103" s="T498">ogonnʼor</ta>
            <ta e="T500" id="Seg_4104" s="T499">aːŋ-ŋɨ-n</ta>
            <ta e="T501" id="Seg_4105" s="T500">arɨj</ta>
            <ta e="T502" id="Seg_4106" s="T501">di͡e-bit</ta>
            <ta e="T503" id="Seg_4107" s="T502">emeːksin-e</ta>
            <ta e="T504" id="Seg_4108" s="T503">aːn-ɨ-n</ta>
            <ta e="T505" id="Seg_4109" s="T504">arɨj-an</ta>
            <ta e="T506" id="Seg_4110" s="T505">obu͡oj-dam-mɨt</ta>
            <ta e="T507" id="Seg_4111" s="T506">emeːksin</ta>
            <ta e="T508" id="Seg_4112" s="T507">ah-a-t-an</ta>
            <ta e="T509" id="Seg_4113" s="T508">baran</ta>
            <ta e="T510" id="Seg_4114" s="T509">dʼi͡e-ti-ger</ta>
            <ta e="T511" id="Seg_4115" s="T510">il-pit</ta>
            <ta e="T512" id="Seg_4116" s="T511">bu</ta>
            <ta e="T513" id="Seg_4117" s="T512">dʼi͡e-ti-ger</ta>
            <ta e="T514" id="Seg_4118" s="T513">ildʼ-en</ta>
            <ta e="T515" id="Seg_4119" s="T514">huːj-an-taraː-n</ta>
            <ta e="T516" id="Seg_4120" s="T515">baran</ta>
            <ta e="T517" id="Seg_4121" s="T516">tann-ɨ-batak</ta>
            <ta e="T518" id="Seg_4122" s="T517">taŋah-ɨ</ta>
            <ta e="T519" id="Seg_4123" s="T518">taŋɨn-nar-an</ta>
            <ta e="T520" id="Seg_4124" s="T519">baran</ta>
            <ta e="T521" id="Seg_4125" s="T520">taːs</ta>
            <ta e="T522" id="Seg_4126" s="T521">kɨrabaːk-ka</ta>
            <ta e="T523" id="Seg_4127" s="T522">olord-on</ta>
            <ta e="T524" id="Seg_4128" s="T523">keːs-pit</ta>
            <ta e="T525" id="Seg_4129" s="T524">ol</ta>
            <ta e="T526" id="Seg_4130" s="T525">gɨn-an</ta>
            <ta e="T527" id="Seg_4131" s="T526">baran</ta>
            <ta e="T528" id="Seg_4132" s="T527">čaːj</ta>
            <ta e="T529" id="Seg_4133" s="T528">ih-e-r-pit</ta>
            <ta e="T530" id="Seg_4134" s="T529">onton</ta>
            <ta e="T531" id="Seg_4135" s="T530">et</ta>
            <ta e="T532" id="Seg_4136" s="T531">kü͡ös</ta>
            <ta e="T533" id="Seg_4137" s="T532">kotor-but</ta>
            <ta e="T534" id="Seg_4138" s="T533">hɨ͡a-laːk</ta>
            <ta e="T535" id="Seg_4139" s="T534">min-i</ta>
            <ta e="T536" id="Seg_4140" s="T535">kɨtɨja-ga</ta>
            <ta e="T537" id="Seg_4141" s="T536">kotor-but</ta>
            <ta e="T538" id="Seg_4142" s="T537">innʼe</ta>
            <ta e="T539" id="Seg_4143" s="T538">gɨn-an</ta>
            <ta e="T540" id="Seg_4144" s="T539">kömüs</ta>
            <ta e="T541" id="Seg_4145" s="T540">lu͡osku</ta>
            <ta e="T542" id="Seg_4146" s="T541">biːlke</ta>
            <ta e="T543" id="Seg_4147" s="T542">uːr-but</ta>
            <ta e="T544" id="Seg_4148" s="T543">araj</ta>
            <ta e="T545" id="Seg_4149" s="T544">lu͡osku-tu-nan</ta>
            <ta e="T546" id="Seg_4150" s="T545">ih-eːri</ta>
            <ta e="T547" id="Seg_4151" s="T546">gɨm-mɨt</ta>
            <ta e="T548" id="Seg_4152" s="T547">kaja</ta>
            <ta e="T549" id="Seg_4153" s="T548">ba-ba</ta>
            <ta e="T550" id="Seg_4154" s="T549">lu͡osku-m</ta>
            <ta e="T551" id="Seg_4155" s="T550">dʼogus</ta>
            <ta e="T552" id="Seg_4156" s="T551">ulagan</ta>
            <ta e="T553" id="Seg_4157" s="T552">pabaraːŋkɨ-ta</ta>
            <ta e="T554" id="Seg_4158" s="T553">duː</ta>
            <ta e="T555" id="Seg_4159" s="T554">komu͡os-ta</ta>
            <ta e="T556" id="Seg_4160" s="T555">duː</ta>
            <ta e="T557" id="Seg_4161" s="T556">egel</ta>
            <ta e="T558" id="Seg_4162" s="T557">di͡e-bit</ta>
            <ta e="T559" id="Seg_4163" s="T558">maːgɨ-ta</ta>
            <ta e="T560" id="Seg_4164" s="T559">komu͡os</ta>
            <ta e="T561" id="Seg_4165" s="T560">bagajɨ-ta</ta>
            <ta e="T562" id="Seg_4166" s="T561">tuttar-an</ta>
            <ta e="T563" id="Seg_4167" s="T562">keːs-pit</ta>
            <ta e="T564" id="Seg_4168" s="T563">hɨ͡a-laːk</ta>
            <ta e="T565" id="Seg_4169" s="T564">min-i</ta>
            <ta e="T566" id="Seg_4170" s="T565">baː-tɨ-nan</ta>
            <ta e="T567" id="Seg_4171" s="T566">huːp</ta>
            <ta e="T568" id="Seg_4172" s="T567">gɨm-mɨt</ta>
            <ta e="T569" id="Seg_4173" s="T568">manɨ͡a-ka</ta>
            <ta e="T570" id="Seg_4174" s="T569">dʼe</ta>
            <ta e="T571" id="Seg_4175" s="T570">čačaj-ar</ta>
            <ta e="T572" id="Seg_4176" s="T571">diː</ta>
            <ta e="T573" id="Seg_4177" s="T572">dʼe</ta>
            <ta e="T574" id="Seg_4178" s="T573">onton</ta>
            <ta e="T575" id="Seg_4179" s="T574">čačaj-a-bɨn</ta>
            <ta e="T576" id="Seg_4180" s="T575">di͡e-n</ta>
            <ta e="T577" id="Seg_4181" s="T576">dʼe</ta>
            <ta e="T578" id="Seg_4182" s="T577">uturuk-tuː-r</ta>
            <ta e="T579" id="Seg_4183" s="T578">diː</ta>
            <ta e="T580" id="Seg_4184" s="T579">taːs</ta>
            <ta e="T581" id="Seg_4185" s="T580">olbog-u-n</ta>
            <ta e="T582" id="Seg_4186" s="T581">bɨha</ta>
            <ta e="T583" id="Seg_4187" s="T582">uturuk-tuː-r</ta>
            <ta e="T584" id="Seg_4188" s="T583">daː</ta>
            <ta e="T585" id="Seg_4189" s="T584">onon</ta>
            <ta e="T586" id="Seg_4190" s="T585">üs</ta>
            <ta e="T587" id="Seg_4191" s="T586">buluːs</ta>
            <ta e="T588" id="Seg_4192" s="T587">hir-i</ta>
            <ta e="T589" id="Seg_4193" s="T588">tobulu</ta>
            <ta e="T590" id="Seg_4194" s="T589">tuh-en</ta>
            <ta e="T591" id="Seg_4195" s="T590">kaːl-ar</ta>
            <ta e="T592" id="Seg_4196" s="T591">emeːksin-e</ta>
            <ta e="T593" id="Seg_4197" s="T592">tu͡ok</ta>
            <ta e="T594" id="Seg_4198" s="T593">bu͡ol-u͡og-a=j</ta>
            <ta e="T595" id="Seg_4199" s="T594">kajdak</ta>
            <ta e="T596" id="Seg_4200" s="T595">olor-but-a</ta>
            <ta e="T597" id="Seg_4201" s="T596">daː</ta>
            <ta e="T598" id="Seg_4202" s="T597">ogurduk</ta>
            <ta e="T601" id="Seg_4203" s="T598">bi͡ek</ta>
            <ta e="T599" id="Seg_4204" s="T601">bi͡ek</ta>
            <ta e="T600" id="Seg_4205" s="T599">olor-or</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_4206" s="T0">ogonnʼor-LAːK</ta>
            <ta e="T2" id="Seg_4207" s="T1">emeːksin</ta>
            <ta e="T3" id="Seg_4208" s="T2">olor-BIT-LAr</ta>
            <ta e="T4" id="Seg_4209" s="T3">ühü</ta>
            <ta e="T5" id="Seg_4210" s="T4">töröː-A-t-Ar</ta>
            <ta e="T6" id="Seg_4211" s="T5">ogo-tA</ta>
            <ta e="T7" id="Seg_4212" s="T6">hu͡ok-LAr</ta>
            <ta e="T8" id="Seg_4213" s="T7">dʼi͡e-GA</ta>
            <ta e="T9" id="Seg_4214" s="T8">olor-TAK-TArInA</ta>
            <ta e="T10" id="Seg_4215" s="T9">u͡ot-LArA</ta>
            <ta e="T11" id="Seg_4216" s="T10">ere</ta>
            <ta e="T12" id="Seg_4217" s="T11">kɨtar-Ar</ta>
            <ta e="T13" id="Seg_4218" s="T12">ogorduk</ta>
            <ta e="T14" id="Seg_4219" s="T13">dʼadaŋɨ-LAr</ta>
            <ta e="T15" id="Seg_4220" s="T14">tahaːra</ta>
            <ta e="T16" id="Seg_4221" s="T15">tagɨs-TAK-TArInA</ta>
            <ta e="T17" id="Seg_4222" s="T16">kaːr-LArA</ta>
            <ta e="T18" id="Seg_4223" s="T17">ere</ta>
            <ta e="T19" id="Seg_4224" s="T18">turtaj-Ar</ta>
            <ta e="T20" id="Seg_4225" s="T19">tu͡ok-tA</ta>
            <ta e="T21" id="Seg_4226" s="T20">da</ta>
            <ta e="T22" id="Seg_4227" s="T21">hu͡ok-LAr</ta>
            <ta e="T23" id="Seg_4228" s="T22">agaj</ta>
            <ta e="T24" id="Seg_4229" s="T23">üp-LArA</ta>
            <ta e="T25" id="Seg_4230" s="T24">di͡e-An-LArA</ta>
            <ta e="T26" id="Seg_4231" s="T25">[C^1][V^1][C^2]-čogotok</ta>
            <ta e="T27" id="Seg_4232" s="T26">at-LAːK-LAr</ta>
            <ta e="T28" id="Seg_4233" s="T27">onton</ta>
            <ta e="T29" id="Seg_4234" s="T28">bultan-Ar</ta>
            <ta e="T30" id="Seg_4235" s="T29">terget-LArA</ta>
            <ta e="T31" id="Seg_4236" s="T30">üs</ta>
            <ta e="T32" id="Seg_4237" s="T31">taːs-LAːK</ta>
            <ta e="T33" id="Seg_4238" s="T32">ilim-kAːN-LAːK-LAr</ta>
            <ta e="T34" id="Seg_4239" s="T33">ol-tI-LArI-GAr</ta>
            <ta e="T35" id="Seg_4240" s="T34">uː</ta>
            <ta e="T36" id="Seg_4241" s="T35">kurdʼaga-tA</ta>
            <ta e="T37" id="Seg_4242" s="T36">iːl-I-n-TAK-InA</ta>
            <ta e="T38" id="Seg_4243" s="T37">ol-nI</ta>
            <ta e="T39" id="Seg_4244" s="T38">hi͡e-Ar-LAr</ta>
            <ta e="T40" id="Seg_4245" s="T39">üčehe-GA</ta>
            <ta e="T41" id="Seg_4246" s="T40">ü͡öl-An</ta>
            <ta e="T42" id="Seg_4247" s="T41">agaj</ta>
            <ta e="T43" id="Seg_4248" s="T42">öl-AːrI</ta>
            <ta e="T44" id="Seg_4249" s="T43">ököj-BIT-LAr</ta>
            <ta e="T45" id="Seg_4250" s="T44">bɨhɨn-AːrI</ta>
            <ta e="T46" id="Seg_4251" s="T45">bɨ͡akaj-BIT-LAr</ta>
            <ta e="T47" id="Seg_4252" s="T46">onton</ta>
            <ta e="T48" id="Seg_4253" s="T47">ogonnʼor-tA</ta>
            <ta e="T49" id="Seg_4254" s="T48">agaj</ta>
            <ta e="T50" id="Seg_4255" s="T49">kiːr-BIT</ta>
            <ta e="T51" id="Seg_4256" s="T50">emeːksin-tI-GAr</ta>
            <ta e="T52" id="Seg_4257" s="T51">dʼe</ta>
            <ta e="T53" id="Seg_4258" s="T52">kiːr-An</ta>
            <ta e="T54" id="Seg_4259" s="T53">dʼi͡e-BIT</ta>
            <ta e="T55" id="Seg_4260" s="T54">kaja</ta>
            <ta e="T56" id="Seg_4261" s="T55">emeːksin</ta>
            <ta e="T57" id="Seg_4262" s="T56">ürdeː-A-BIt</ta>
            <ta e="T58" id="Seg_4263" s="T57">kaːm-TAr-IAK-BIt</ta>
            <ta e="T59" id="Seg_4264" s="T58">čogotok</ta>
            <ta e="T60" id="Seg_4265" s="T59">at-LAːK-BIt</ta>
            <ta e="T61" id="Seg_4266" s="T60">hi͡e-IAK-BIt</ta>
            <ta e="T62" id="Seg_4267" s="T61">baːr-tA</ta>
            <ta e="T63" id="Seg_4268" s="T62">onnoːgor</ta>
            <ta e="T64" id="Seg_4269" s="T63">ogut-An</ta>
            <ta e="T65" id="Seg_4270" s="T64">öl-TAK-BItInA</ta>
            <ta e="T66" id="Seg_4271" s="T65">kelin-BItI-GAr</ta>
            <ta e="T67" id="Seg_4272" s="T66">kaːl-IAK-tA</ta>
            <ta e="T68" id="Seg_4273" s="T67">ol-GA</ta>
            <ta e="T69" id="Seg_4274" s="T68">emeːksin</ta>
            <ta e="T70" id="Seg_4275" s="T69">beje-ŋ</ta>
            <ta e="T71" id="Seg_4276" s="T70">hanaː-ŋ</ta>
            <ta e="T72" id="Seg_4277" s="T71">di͡e-BIT</ta>
            <ta e="T73" id="Seg_4278" s="T72">ölör-TAR-GIn</ta>
            <ta e="T74" id="Seg_4279" s="T73">ere</ta>
            <ta e="T75" id="Seg_4280" s="T74">hi͡e-IAK-BIt</ta>
            <ta e="T76" id="Seg_4281" s="T75">er-tA</ta>
            <ta e="T77" id="Seg_4282" s="T76">tagɨs-BIT</ta>
            <ta e="T78" id="Seg_4283" s="T77">da</ta>
            <ta e="T79" id="Seg_4284" s="T78">ölör-BIT</ta>
            <ta e="T80" id="Seg_4285" s="T79">ogonnʼor-tA</ta>
            <ta e="T81" id="Seg_4286" s="T80">hül-BIT</ta>
            <ta e="T82" id="Seg_4287" s="T81">agaj</ta>
            <ta e="T83" id="Seg_4288" s="T82">tɨmnɨː</ta>
            <ta e="T84" id="Seg_4289" s="T83">bagajɨ</ta>
            <ta e="T85" id="Seg_4290" s="T84">čɨskaːn</ta>
            <ta e="T86" id="Seg_4291" s="T85">bagajɨ</ta>
            <ta e="T87" id="Seg_4292" s="T86">ogonnʼor-tA</ta>
            <ta e="T88" id="Seg_4293" s="T87">kiːr-BIT</ta>
            <ta e="T89" id="Seg_4294" s="T88">kiːr-An</ta>
            <ta e="T90" id="Seg_4295" s="T89">di͡e-BIT</ta>
            <ta e="T91" id="Seg_4296" s="T90">at-BIt</ta>
            <ta e="T92" id="Seg_4297" s="T91">et-tI-n</ta>
            <ta e="T93" id="Seg_4298" s="T92">bütün-LIː</ta>
            <ta e="T94" id="Seg_4299" s="T93">golomo-kAːN-BIt</ta>
            <ta e="T95" id="Seg_4300" s="T94">onu͡or</ta>
            <ta e="T96" id="Seg_4301" s="T95">taskaj-LAː-An</ta>
            <ta e="T97" id="Seg_4302" s="T96">keːs</ta>
            <ta e="T98" id="Seg_4303" s="T97">čɨːsta-tI-n</ta>
            <ta e="T99" id="Seg_4304" s="T98">emeːksin-tA</ta>
            <ta e="T100" id="Seg_4305" s="T99">barɨ-tI-n</ta>
            <ta e="T101" id="Seg_4306" s="T100">taskaj-LAː-BIT</ta>
            <ta e="T102" id="Seg_4307" s="T101">emeːksin-tA</ta>
            <ta e="T103" id="Seg_4308" s="T102">iliː-tI-n</ta>
            <ta e="T104" id="Seg_4309" s="T103">itij-t-AːrI</ta>
            <ta e="T105" id="Seg_4310" s="T104">gɨn-BIT-tI-n</ta>
            <ta e="T106" id="Seg_4311" s="T105">ogonnʼor</ta>
            <ta e="T107" id="Seg_4312" s="T106">et-BIT</ta>
            <ta e="T108" id="Seg_4313" s="T107">hi͡e-IAK-GI-n</ta>
            <ta e="T109" id="Seg_4314" s="T108">barɨ</ta>
            <ta e="T110" id="Seg_4315" s="T109">ihit-GA-r</ta>
            <ta e="T111" id="Seg_4316" s="T110">holuːr-GA-r</ta>
            <ta e="T112" id="Seg_4317" s="T111">uː-TA</ta>
            <ta e="T113" id="Seg_4318" s="T112">bas-An</ta>
            <ta e="T114" id="Seg_4319" s="T113">tolor</ta>
            <ta e="T115" id="Seg_4320" s="T114">belem-LAː-An</ta>
            <ta e="T116" id="Seg_4321" s="T115">baran</ta>
            <ta e="T117" id="Seg_4322" s="T116">dʼe</ta>
            <ta e="T118" id="Seg_4323" s="T117">ahaː-Aːr</ta>
            <ta e="T119" id="Seg_4324" s="T118">innʼe</ta>
            <ta e="T120" id="Seg_4325" s="T119">gɨn-An</ta>
            <ta e="T121" id="Seg_4326" s="T120">emeːksin</ta>
            <ta e="T122" id="Seg_4327" s="T121">tagɨs-An</ta>
            <ta e="T123" id="Seg_4328" s="T122">barɨ</ta>
            <ta e="T124" id="Seg_4329" s="T123">oŋkučak-tA-r</ta>
            <ta e="T125" id="Seg_4330" s="T124">barɨta-kAːN-LArI-GAr</ta>
            <ta e="T126" id="Seg_4331" s="T125">uː</ta>
            <ta e="T127" id="Seg_4332" s="T126">bas-BIT</ta>
            <ta e="T128" id="Seg_4333" s="T127">agaj</ta>
            <ta e="T129" id="Seg_4334" s="T128">bütehik</ta>
            <ta e="T130" id="Seg_4335" s="T129">holuːr-GA-kA-kA-kAːN-tI-n</ta>
            <ta e="T131" id="Seg_4336" s="T130">egel-AːrI</ta>
            <ta e="T132" id="Seg_4337" s="T131">gɨn-BIT-tA</ta>
            <ta e="T133" id="Seg_4338" s="T132">aːn-tA</ta>
            <ta e="T134" id="Seg_4339" s="T133">katɨːlaːk</ta>
            <ta e="T135" id="Seg_4340" s="T134">kaja</ta>
            <ta e="T136" id="Seg_4341" s="T135">ogonnʼor</ta>
            <ta e="T137" id="Seg_4342" s="T136">aːn-GI-n</ta>
            <ta e="T138" id="Seg_4343" s="T137">as</ta>
            <ta e="T139" id="Seg_4344" s="T138">ülüj-AːrI</ta>
            <ta e="T140" id="Seg_4345" s="T139">gɨn-TI-m</ta>
            <ta e="T141" id="Seg_4346" s="T140">di͡e-BIT</ta>
            <ta e="T142" id="Seg_4347" s="T141">huːka</ta>
            <ta e="T143" id="Seg_4348" s="T142">kɨːs-tA</ta>
            <ta e="T144" id="Seg_4349" s="T143">kaːr-GA</ta>
            <ta e="T145" id="Seg_4350" s="T144">kampɨ</ta>
            <ta e="T146" id="Seg_4351" s="T145">bɨrak</ta>
            <ta e="T147" id="Seg_4352" s="T146">di͡e-BIT</ta>
            <ta e="T148" id="Seg_4353" s="T147">emeːksin-tA</ta>
            <ta e="T149" id="Seg_4354" s="T148">bu</ta>
            <ta e="T150" id="Seg_4355" s="T149">holuːr-čak-tI-n</ta>
            <ta e="T151" id="Seg_4356" s="T150">bɨraː-An</ta>
            <ta e="T152" id="Seg_4357" s="T151">baran</ta>
            <ta e="T153" id="Seg_4358" s="T152">dʼe</ta>
            <ta e="T154" id="Seg_4359" s="T153">toŋ-An</ta>
            <ta e="T155" id="Seg_4360" s="T154">öl-AːrI</ta>
            <ta e="T156" id="Seg_4361" s="T155">gɨn-Ar</ta>
            <ta e="T157" id="Seg_4362" s="T156">emeːksin-tA</ta>
            <ta e="T158" id="Seg_4363" s="T157">balagan-tI-n</ta>
            <ta e="T159" id="Seg_4364" s="T158">ürüt-tI-GAr</ta>
            <ta e="T160" id="Seg_4365" s="T159">tagɨs-An</ta>
            <ta e="T161" id="Seg_4366" s="T160">kɨːm-kAːN-LAr</ta>
            <ta e="T162" id="Seg_4367" s="T161">tagɨs-Iː-LArI-GAr</ta>
            <ta e="T163" id="Seg_4368" s="T162">onno</ta>
            <ta e="T164" id="Seg_4369" s="T163">iliː-tI-n</ta>
            <ta e="T165" id="Seg_4370" s="T164">itij-t-Ar</ta>
            <ta e="T166" id="Seg_4371" s="T165">agaj</ta>
            <ta e="T167" id="Seg_4372" s="T166">er-tI-n</ta>
            <ta e="T168" id="Seg_4373" s="T167">kör-BIT-tA</ta>
            <ta e="T169" id="Seg_4374" s="T168">at-tI-n</ta>
            <ta e="T170" id="Seg_4375" s="T169">hamak-tI-n</ta>
            <ta e="T171" id="Seg_4376" s="T170">ɨl-An</ta>
            <ta e="T172" id="Seg_4377" s="T171">baran</ta>
            <ta e="T173" id="Seg_4378" s="T172">alčaj-An</ta>
            <ta e="T174" id="Seg_4379" s="T173">baran</ta>
            <ta e="T175" id="Seg_4380" s="T174">hača-LAN-A</ta>
            <ta e="T176" id="Seg_4381" s="T175">olor-Ar</ta>
            <ta e="T177" id="Seg_4382" s="T176">ol</ta>
            <ta e="T178" id="Seg_4383" s="T177">itij-t-AːrI</ta>
            <ta e="T179" id="Seg_4384" s="T178">gɨn-Ar</ta>
            <ta e="T180" id="Seg_4385" s="T179">emeːksin-tI-n</ta>
            <ta e="T181" id="Seg_4386" s="T180">iliː-tI-n</ta>
            <ta e="T182" id="Seg_4387" s="T181">üčehe-nAn</ta>
            <ta e="T183" id="Seg_4388" s="T182">bɨhɨt-A</ta>
            <ta e="T184" id="Seg_4389" s="T183">kej-Ar</ta>
            <ta e="T185" id="Seg_4390" s="T184">ol-tI-tA</ta>
            <ta e="T186" id="Seg_4391" s="T185">ɨtaː-A-ɨtaː-A</ta>
            <ta e="T187" id="Seg_4392" s="T186">hir-GA</ta>
            <ta e="T188" id="Seg_4393" s="T187">tüs-AːktAː-An</ta>
            <ta e="T189" id="Seg_4394" s="T188">baran</ta>
            <ta e="T190" id="Seg_4395" s="T189">mas-LAː-Ar</ta>
            <ta e="T191" id="Seg_4396" s="T190">hir-kAːN</ta>
            <ta e="T192" id="Seg_4397" s="T191">dek</ta>
            <ta e="T193" id="Seg_4398" s="T192">ɨtaː-A-ɨtaː-A</ta>
            <ta e="T194" id="Seg_4399" s="T193">kaːm-A</ta>
            <ta e="T195" id="Seg_4400" s="T194">tur-Ar</ta>
            <ta e="T196" id="Seg_4401" s="T195">dʼe</ta>
            <ta e="T197" id="Seg_4402" s="T196">onno</ta>
            <ta e="T198" id="Seg_4403" s="T197">tiːt-I-kAːN</ta>
            <ta e="T199" id="Seg_4404" s="T198">mas</ta>
            <ta e="T200" id="Seg_4405" s="T199">törüt-tI-GAr</ta>
            <ta e="T201" id="Seg_4406" s="T200">hɨt-AːrI</ta>
            <ta e="T202" id="Seg_4407" s="T201">gɨn-BIT</ta>
            <ta e="T203" id="Seg_4408" s="T202">hɨt-AːrI</ta>
            <ta e="T204" id="Seg_4409" s="T203">gɨn-BIT</ta>
            <ta e="T205" id="Seg_4410" s="T204">tɨmnɨː</ta>
            <ta e="T206" id="Seg_4411" s="T205">titireː-Ar</ta>
            <ta e="T207" id="Seg_4412" s="T206">hürek-tA-r</ta>
            <ta e="T208" id="Seg_4413" s="T207">kiːr-Ar</ta>
            <ta e="T209" id="Seg_4414" s="T208">ol</ta>
            <ta e="T210" id="Seg_4415" s="T209">hɨt-TAK-InA</ta>
            <ta e="T211" id="Seg_4416" s="T210">ajdaːn</ta>
            <ta e="T212" id="Seg_4417" s="T211">bögö</ta>
            <ta e="T213" id="Seg_4418" s="T212">ihilin-I-BIT</ta>
            <ta e="T214" id="Seg_4419" s="T213">taba</ta>
            <ta e="T215" id="Seg_4420" s="T214">üːr-Ar-ttAn</ta>
            <ta e="T216" id="Seg_4421" s="T215">taba</ta>
            <ta e="T217" id="Seg_4422" s="T216">üːr-Ar</ta>
            <ta e="T218" id="Seg_4423" s="T217">eŋin</ta>
            <ta e="T219" id="Seg_4424" s="T218">bagajɨ</ta>
            <ta e="T220" id="Seg_4425" s="T219">ol</ta>
            <ta e="T221" id="Seg_4426" s="T220">kotun</ta>
            <ta e="T222" id="Seg_4427" s="T221">dʼe</ta>
            <ta e="T223" id="Seg_4428" s="T222">bar-Ar</ta>
            <ta e="T224" id="Seg_4429" s="T223">bu</ta>
            <ta e="T225" id="Seg_4430" s="T224">emeːksin</ta>
            <ta e="T226" id="Seg_4431" s="T225">ol</ta>
            <ta e="T227" id="Seg_4432" s="T226">bar-An</ta>
            <ta e="T228" id="Seg_4433" s="T227">ajɨː</ta>
            <ta e="T229" id="Seg_4434" s="T228">daːganɨ</ta>
            <ta e="T230" id="Seg_4435" s="T229">ɨj-nI</ta>
            <ta e="T231" id="Seg_4436" s="T230">bü͡öleː-Ar</ta>
            <ta e="T232" id="Seg_4437" s="T231">ɨːs</ta>
            <ta e="T233" id="Seg_4438" s="T232">tuman</ta>
            <ta e="T234" id="Seg_4439" s="T233">taba-GA</ta>
            <ta e="T235" id="Seg_4440" s="T234">kün-nI</ta>
            <ta e="T236" id="Seg_4441" s="T235">bü͡öleː-Ar</ta>
            <ta e="T237" id="Seg_4442" s="T236">küden</ta>
            <ta e="T238" id="Seg_4443" s="T237">tuman</ta>
            <ta e="T239" id="Seg_4444" s="T238">hü͡öhü-GA</ta>
            <ta e="T240" id="Seg_4445" s="T239">tij-Ar</ta>
            <ta e="T241" id="Seg_4446" s="T240">bu</ta>
            <ta e="T242" id="Seg_4447" s="T241">emeːksin-I-ŋ</ta>
            <ta e="T243" id="Seg_4448" s="T242">dʼi͡e-nI</ta>
            <ta e="T244" id="Seg_4449" s="T243">kördöː-An</ta>
            <ta e="T245" id="Seg_4450" s="T244">bu</ta>
            <ta e="T246" id="Seg_4451" s="T245">taba-nI</ta>
            <ta e="T247" id="Seg_4452" s="T246">tel-Ar-tI-GAr</ta>
            <ta e="T248" id="Seg_4453" s="T247">ikki</ta>
            <ta e="T249" id="Seg_4454" s="T248">aŋar-tA</ta>
            <ta e="T250" id="Seg_4455" s="T249">elej-An</ta>
            <ta e="T251" id="Seg_4456" s="T250">kaːl-BIT</ta>
            <ta e="T252" id="Seg_4457" s="T251">agaj</ta>
            <ta e="T253" id="Seg_4458" s="T252">alɨː-nI</ta>
            <ta e="T254" id="Seg_4459" s="T253">[C^1][V^1][C^2]-toloru</ta>
            <ta e="T255" id="Seg_4460" s="T254">uraha</ta>
            <ta e="T256" id="Seg_4461" s="T255">dʼi͡e</ta>
            <ta e="T257" id="Seg_4462" s="T256">tut-I-LIN-An</ta>
            <ta e="T258" id="Seg_4463" s="T257">tur-Ar</ta>
            <ta e="T259" id="Seg_4464" s="T258">ulakan</ta>
            <ta e="T260" id="Seg_4465" s="T259">mɨjaːn</ta>
            <ta e="T261" id="Seg_4466" s="T260">dʼi͡e</ta>
            <ta e="T262" id="Seg_4467" s="T261">kel-TI-tA</ta>
            <ta e="T263" id="Seg_4468" s="T262">bu</ta>
            <ta e="T264" id="Seg_4469" s="T263">dʼi͡e-GA</ta>
            <ta e="T265" id="Seg_4470" s="T264">manna</ta>
            <ta e="T266" id="Seg_4471" s="T265">dʼe</ta>
            <ta e="T267" id="Seg_4472" s="T266">kas</ta>
            <ta e="T268" id="Seg_4473" s="T267">u͡on</ta>
            <ta e="T269" id="Seg_4474" s="T268">kihi</ta>
            <ta e="T270" id="Seg_4475" s="T269">bɨs-A</ta>
            <ta e="T271" id="Seg_4476" s="T270">ölgöbüːn-tA</ta>
            <ta e="T272" id="Seg_4477" s="T271">balok-tA</ta>
            <ta e="T273" id="Seg_4478" s="T272">huburuj-An</ta>
            <ta e="T274" id="Seg_4479" s="T273">tur-Ar</ta>
            <ta e="T275" id="Seg_4480" s="T274">innʼe</ta>
            <ta e="T276" id="Seg_4481" s="T275">gɨn-An</ta>
            <ta e="T277" id="Seg_4482" s="T276">bu</ta>
            <ta e="T278" id="Seg_4483" s="T277">emeːksin</ta>
            <ta e="T279" id="Seg_4484" s="T278">bu</ta>
            <ta e="T280" id="Seg_4485" s="T279">dʼi͡e-nI</ta>
            <ta e="T281" id="Seg_4486" s="T280">arɨj-A</ta>
            <ta e="T282" id="Seg_4487" s="T281">ogus-I-BIT</ta>
            <ta e="T283" id="Seg_4488" s="T282">oduːlaː-An</ta>
            <ta e="T284" id="Seg_4489" s="T283">kör-BIT-tA</ta>
            <ta e="T285" id="Seg_4490" s="T284">[C^1][V^1][C^2]-iččitek</ta>
            <ta e="T286" id="Seg_4491" s="T285">kihi-tA</ta>
            <ta e="T287" id="Seg_4492" s="T286">hu͡ok</ta>
            <ta e="T288" id="Seg_4493" s="T287">barɨ</ta>
            <ta e="T289" id="Seg_4494" s="T288">kü͡ös</ta>
            <ta e="T290" id="Seg_4495" s="T289">kü͡ös-LAN-An</ta>
            <ta e="T291" id="Seg_4496" s="T290">tur-Ar</ta>
            <ta e="T292" id="Seg_4497" s="T291">barɨ</ta>
            <ta e="T293" id="Seg_4498" s="T292">as</ta>
            <ta e="T294" id="Seg_4499" s="T293">as-LAN-An</ta>
            <ta e="T295" id="Seg_4500" s="T294">tur-Ar</ta>
            <ta e="T296" id="Seg_4501" s="T295">taːs</ta>
            <ta e="T297" id="Seg_4502" s="T296">kɨrabaːt-LAr</ta>
            <ta e="T298" id="Seg_4503" s="T297">bütün-LIː</ta>
            <ta e="T299" id="Seg_4504" s="T298">onnuk</ta>
            <ta e="T300" id="Seg_4505" s="T299">terget-LAːK</ta>
            <ta e="T301" id="Seg_4506" s="T300">dʼi͡e</ta>
            <ta e="T302" id="Seg_4507" s="T301">tur-Ar</ta>
            <ta e="T303" id="Seg_4508" s="T302">ol</ta>
            <ta e="T304" id="Seg_4509" s="T303">emeːksin</ta>
            <ta e="T305" id="Seg_4510" s="T304">kiːr-An</ta>
            <ta e="T306" id="Seg_4511" s="T305">ahaː-BAtAK-tI-n</ta>
            <ta e="T307" id="Seg_4512" s="T306">ahaː-Ar</ta>
            <ta e="T308" id="Seg_4513" s="T307">hi͡e-BAtAK-tI-n</ta>
            <ta e="T309" id="Seg_4514" s="T308">hi͡e-Ar</ta>
            <ta e="T310" id="Seg_4515" s="T309">nuːčča</ta>
            <ta e="T311" id="Seg_4516" s="T310">da</ta>
            <ta e="T312" id="Seg_4517" s="T311">as-tA</ta>
            <ta e="T313" id="Seg_4518" s="T312">ügüs</ta>
            <ta e="T314" id="Seg_4519" s="T313">tɨ͡a</ta>
            <ta e="T315" id="Seg_4520" s="T314">da</ta>
            <ta e="T316" id="Seg_4521" s="T315">as-tA</ta>
            <ta e="T317" id="Seg_4522" s="T316">ügüs</ta>
            <ta e="T318" id="Seg_4523" s="T317">bu</ta>
            <ta e="T319" id="Seg_4524" s="T318">emeːksin</ta>
            <ta e="T320" id="Seg_4525" s="T319">huːn-An-taraːn-An</ta>
            <ta e="T321" id="Seg_4526" s="T320">baran</ta>
            <ta e="T322" id="Seg_4527" s="T321">üːs-kiːs</ta>
            <ta e="T323" id="Seg_4528" s="T322">taŋas-nI</ta>
            <ta e="T324" id="Seg_4529" s="T323">taŋɨn-An</ta>
            <ta e="T325" id="Seg_4530" s="T324">keːs-BIT</ta>
            <ta e="T326" id="Seg_4531" s="T325">bu</ta>
            <ta e="T327" id="Seg_4532" s="T326">tukarɨ</ta>
            <ta e="T328" id="Seg_4533" s="T327">tu͡ok</ta>
            <ta e="T329" id="Seg_4534" s="T328">da</ta>
            <ta e="T330" id="Seg_4535" s="T329">kihi-tA</ta>
            <ta e="T331" id="Seg_4536" s="T330">bil-LIN-I-BAT</ta>
            <ta e="T332" id="Seg_4537" s="T331">üs</ta>
            <ta e="T333" id="Seg_4538" s="T332">kün-nI</ta>
            <ta e="T334" id="Seg_4539" s="T333">meldʼi</ta>
            <ta e="T335" id="Seg_4540" s="T334">tu͡ok</ta>
            <ta e="T336" id="Seg_4541" s="T335">da</ta>
            <ta e="T337" id="Seg_4542" s="T336">kihi-tA</ta>
            <ta e="T338" id="Seg_4543" s="T337">bil-LIN-I-BAT</ta>
            <ta e="T339" id="Seg_4544" s="T338">agaj</ta>
            <ta e="T340" id="Seg_4545" s="T339">ki͡ehe</ta>
            <ta e="T341" id="Seg_4546" s="T340">bu͡ol-IAK.[tA]</ta>
            <ta e="T342" id="Seg_4547" s="T341">da</ta>
            <ta e="T343" id="Seg_4548" s="T342">ü͡öles-tI-nAn</ta>
            <ta e="T344" id="Seg_4549" s="T343">kur</ta>
            <ta e="T345" id="Seg_4550" s="T344">kihi</ta>
            <ta e="T346" id="Seg_4551" s="T345">menʼiː-LAr-tA</ta>
            <ta e="T347" id="Seg_4552" s="T346">tüs-Ar-LAr</ta>
            <ta e="T348" id="Seg_4553" s="T347">ol-LAr-nI</ta>
            <ta e="T349" id="Seg_4554" s="T348">tes-I-ŋ-bɨs-I-ŋ</ta>
            <ta e="T350" id="Seg_4555" s="T349">tes-I-t-A</ta>
            <ta e="T351" id="Seg_4556" s="T350">bar</ta>
            <ta e="T352" id="Seg_4557" s="T351">di͡e-A-di͡e-A</ta>
            <ta e="T353" id="Seg_4558" s="T352">tes-I-t-A</ta>
            <ta e="T354" id="Seg_4559" s="T353">ükteː-Ar</ta>
            <ta e="T355" id="Seg_4560" s="T354">ol-LAr-tA</ta>
            <ta e="T356" id="Seg_4561" s="T355">hir</ta>
            <ta e="T357" id="Seg_4562" s="T356">dek</ta>
            <ta e="T358" id="Seg_4563" s="T357">himitin-An</ta>
            <ta e="T359" id="Seg_4564" s="T358">is-Ar-LAr</ta>
            <ta e="T360" id="Seg_4565" s="T359">emeːksin</ta>
            <ta e="T361" id="Seg_4566" s="T360">kas</ta>
            <ta e="T362" id="Seg_4567" s="T361">u͡on</ta>
            <ta e="T363" id="Seg_4568" s="T362">turkarɨ</ta>
            <ta e="T364" id="Seg_4569" s="T363">olor-BIT</ta>
            <ta e="T365" id="Seg_4570" s="T364">taba-tA</ta>
            <ta e="T366" id="Seg_4571" s="T365">kanna</ta>
            <ta e="T367" id="Seg_4572" s="T366">da</ta>
            <ta e="T368" id="Seg_4573" s="T367">kamnaː-BAT</ta>
            <ta e="T369" id="Seg_4574" s="T368">at-LAr-tA</ta>
            <ta e="T370" id="Seg_4575" s="T369">hubu</ta>
            <ta e="T371" id="Seg_4576" s="T370">kördük</ta>
            <ta e="T372" id="Seg_4577" s="T371">hɨt-Ar-LAr</ta>
            <ta e="T373" id="Seg_4578" s="T372">balɨk-LArA</ta>
            <ta e="T374" id="Seg_4579" s="T373">hubu</ta>
            <ta e="T375" id="Seg_4580" s="T374">bultaː-A-n-BIT</ta>
            <ta e="T376" id="Seg_4581" s="T375">kurduk</ta>
            <ta e="T377" id="Seg_4582" s="T376">hɨrɨt-Ar</ta>
            <ta e="T378" id="Seg_4583" s="T377">emeːksin</ta>
            <ta e="T379" id="Seg_4584" s="T378">onon</ta>
            <ta e="T380" id="Seg_4585" s="T379">baːj-An-baːj-An</ta>
            <ta e="T381" id="Seg_4586" s="T380">[C^1][V^1][C^2]-čogotok</ta>
            <ta e="T382" id="Seg_4587" s="T381">olor-Ar</ta>
            <ta e="T383" id="Seg_4588" s="T382">innʼe</ta>
            <ta e="T384" id="Seg_4589" s="T383">gɨn-An</ta>
            <ta e="T385" id="Seg_4590" s="T384">emeːksin</ta>
            <ta e="T386" id="Seg_4591" s="T385">duːma-tA</ta>
            <ta e="T387" id="Seg_4592" s="T386">kel-BIT</ta>
            <ta e="T388" id="Seg_4593" s="T387">ogo-LAr</ta>
            <ta e="T389" id="Seg_4594" s="T388">min</ta>
            <ta e="T390" id="Seg_4595" s="T389">olor-IAK-TAːgAr</ta>
            <ta e="T391" id="Seg_4596" s="T390">ogonnʼor-I-m</ta>
            <ta e="T392" id="Seg_4597" s="T391">ɨraːk-tI-GAr</ta>
            <ta e="T393" id="Seg_4598" s="T392">bil-s-A</ta>
            <ta e="T394" id="Seg_4599" s="T393">bar-IAK</ta>
            <ta e="T395" id="Seg_4600" s="T394">baːr-I-m</ta>
            <ta e="T396" id="Seg_4601" s="T395">üs-Is</ta>
            <ta e="T397" id="Seg_4602" s="T396">dʼɨl-tI-GAr</ta>
            <ta e="T398" id="Seg_4603" s="T397">ogonnʼor-tI-GAr</ta>
            <ta e="T399" id="Seg_4604" s="T398">bar-AːrI</ta>
            <ta e="T400" id="Seg_4605" s="T399">taŋɨn-TI-tA</ta>
            <ta e="T401" id="Seg_4606" s="T400">keli͡ep-nI</ta>
            <ta e="T402" id="Seg_4607" s="T401">bɨs-ItAlAː-An</ta>
            <ta e="T403" id="Seg_4608" s="T402">baran</ta>
            <ta e="T404" id="Seg_4609" s="T403">ɨl-TI-tA</ta>
            <ta e="T405" id="Seg_4610" s="T404">ol</ta>
            <ta e="T406" id="Seg_4611" s="T405">gɨn-An</ta>
            <ta e="T407" id="Seg_4612" s="T406">baran</ta>
            <ta e="T408" id="Seg_4613" s="T407">kɨːl</ta>
            <ta e="T409" id="Seg_4614" s="T408">hɨ͡a-tI-n</ta>
            <ta e="T410" id="Seg_4615" s="T409">ilt-IːhI</ta>
            <ta e="T411" id="Seg_4616" s="T410">innʼe</ta>
            <ta e="T412" id="Seg_4617" s="T411">gɨn-An</ta>
            <ta e="T413" id="Seg_4618" s="T412">baran</ta>
            <ta e="T414" id="Seg_4619" s="T413">ogonnʼor-tI-GAr</ta>
            <ta e="T415" id="Seg_4620" s="T414">bar-TI-tA</ta>
            <ta e="T416" id="Seg_4621" s="T415">bu</ta>
            <ta e="T417" id="Seg_4622" s="T416">bar-An</ta>
            <ta e="T418" id="Seg_4623" s="T417">bar-An</ta>
            <ta e="T419" id="Seg_4624" s="T418">ogonnʼor-tI-n</ta>
            <ta e="T420" id="Seg_4625" s="T419">golomo-tI-GAr</ta>
            <ta e="T421" id="Seg_4626" s="T420">tij-BIT</ta>
            <ta e="T422" id="Seg_4627" s="T421">agaj</ta>
            <ta e="T423" id="Seg_4628" s="T422">tu͡ok-kAːN</ta>
            <ta e="T424" id="Seg_4629" s="T423">du͡o</ta>
            <ta e="T425" id="Seg_4630" s="T424">hu͡ok</ta>
            <ta e="T426" id="Seg_4631" s="T425">bütün-LIː</ta>
            <ta e="T427" id="Seg_4632" s="T426">barɨta</ta>
            <ta e="T428" id="Seg_4633" s="T427">tip-I-LIN-An</ta>
            <ta e="T429" id="Seg_4634" s="T428">huːl-An</ta>
            <ta e="T430" id="Seg_4635" s="T429">kaːl-BIT</ta>
            <ta e="T431" id="Seg_4636" s="T430">ol</ta>
            <ta e="T432" id="Seg_4637" s="T431">da</ta>
            <ta e="T433" id="Seg_4638" s="T432">bu͡ol-TAR</ta>
            <ta e="T434" id="Seg_4639" s="T433">ü͡öles-tI-GAr</ta>
            <ta e="T435" id="Seg_4640" s="T434">ɨtɨn-I-BIT</ta>
            <ta e="T436" id="Seg_4641" s="T435">agaj</ta>
            <ta e="T437" id="Seg_4642" s="T436">ačaːk-tI-n</ta>
            <ta e="T438" id="Seg_4643" s="T437">tögürüččü</ta>
            <ta e="T439" id="Seg_4644" s="T438">üs</ta>
            <ta e="T440" id="Seg_4645" s="T439">tüː-LAːK</ta>
            <ta e="T441" id="Seg_4646" s="T440">kurdʼaga</ta>
            <ta e="T442" id="Seg_4647" s="T441">bu͡ol-An</ta>
            <ta e="T443" id="Seg_4648" s="T442">dʼaːbɨlan-A</ta>
            <ta e="T444" id="Seg_4649" s="T443">hɨt-Ar</ta>
            <ta e="T445" id="Seg_4650" s="T444">at-tI-n</ta>
            <ta e="T446" id="Seg_4651" s="T445">kara</ta>
            <ta e="T447" id="Seg_4652" s="T446">tunʼak-tI-n</ta>
            <ta e="T448" id="Seg_4653" s="T447">kɨtta</ta>
            <ta e="T449" id="Seg_4654" s="T448">büt-t-A-s-tI-n</ta>
            <ta e="T450" id="Seg_4655" s="T449">ere</ta>
            <ta e="T451" id="Seg_4656" s="T450">ort-TAr-BIT</ta>
            <ta e="T452" id="Seg_4657" s="T451">at-tI-n</ta>
            <ta e="T453" id="Seg_4658" s="T452">gi͡en-tI-n</ta>
            <ta e="T454" id="Seg_4659" s="T453">maːgɨŋ</ta>
            <ta e="T455" id="Seg_4660" s="T454">kubuluj-BIT</ta>
            <ta e="T456" id="Seg_4661" s="T455">obu͡oj-tA</ta>
            <ta e="T457" id="Seg_4662" s="T456">ühü</ta>
            <ta e="T458" id="Seg_4663" s="T457">ol</ta>
            <ta e="T459" id="Seg_4664" s="T458">ihin</ta>
            <ta e="T460" id="Seg_4665" s="T459">öl-I-m-AːrI</ta>
            <ta e="T461" id="Seg_4666" s="T460">kubuluj-BIT-tA</ta>
            <ta e="T462" id="Seg_4667" s="T461">ühü</ta>
            <ta e="T463" id="Seg_4668" s="T462">dʼe</ta>
            <ta e="T464" id="Seg_4669" s="T463">ol-nI</ta>
            <ta e="T465" id="Seg_4670" s="T464">bu</ta>
            <ta e="T466" id="Seg_4671" s="T465">keli͡ep-I-nAn</ta>
            <ta e="T467" id="Seg_4672" s="T466">bɨrak-BIT</ta>
            <ta e="T468" id="Seg_4673" s="T467">bu-nI</ta>
            <ta e="T469" id="Seg_4674" s="T468">türd-A-s</ta>
            <ta e="T470" id="Seg_4675" s="T469">gɨn-BIT</ta>
            <ta e="T471" id="Seg_4676" s="T470">da</ta>
            <ta e="T472" id="Seg_4677" s="T471">et-BIT</ta>
            <ta e="T473" id="Seg_4678" s="T472">bu</ta>
            <ta e="T474" id="Seg_4679" s="T473">ačaːk-I-m</ta>
            <ta e="T475" id="Seg_4680" s="T474">bu͡or-tA</ta>
            <ta e="T476" id="Seg_4681" s="T475">hɨt-IAr-I-m-AːrI</ta>
            <ta e="T477" id="Seg_4682" s="T476">gɨn-BIT</ta>
            <ta e="T478" id="Seg_4683" s="T477">di͡e-Ar</ta>
            <ta e="T479" id="Seg_4684" s="T478">hɨ͡a-nAn</ta>
            <ta e="T480" id="Seg_4685" s="T479">bɨrak-BIT</ta>
            <ta e="T481" id="Seg_4686" s="T480">ɨčča</ta>
            <ta e="T482" id="Seg_4687" s="T481">koŋurak-LAr</ta>
            <ta e="T483" id="Seg_4688" s="T482">tüs-An-LAr</ta>
            <ta e="T484" id="Seg_4689" s="T483">hɨt-IAr-I-m-AːrI</ta>
            <ta e="T485" id="Seg_4690" s="T484">gɨn-TI-LAr</ta>
            <ta e="T486" id="Seg_4691" s="T485">emi͡e</ta>
            <ta e="T487" id="Seg_4692" s="T486">bɨrak-BIT</ta>
            <ta e="T488" id="Seg_4693" s="T487">bu-nI</ta>
            <ta e="T489" id="Seg_4694" s="T488">kir-An</ta>
            <ta e="T490" id="Seg_4695" s="T489">kör-BIT</ta>
            <ta e="T491" id="Seg_4696" s="T490">hɨ͡a</ta>
            <ta e="T492" id="Seg_4697" s="T491">e-BIT</ta>
            <ta e="T493" id="Seg_4698" s="T492">ol-nI</ta>
            <ta e="T494" id="Seg_4699" s="T493">taŋara</ta>
            <ta e="T495" id="Seg_4700" s="T494">taŋara</ta>
            <ta e="T496" id="Seg_4701" s="T495">čočum-čak-kAːN</ta>
            <ta e="T497" id="Seg_4702" s="T496">di͡e-BIT</ta>
            <ta e="T498" id="Seg_4703" s="T497">kaja</ta>
            <ta e="T499" id="Seg_4704" s="T498">ogonnʼor</ta>
            <ta e="T500" id="Seg_4705" s="T499">aːn-GI-n</ta>
            <ta e="T501" id="Seg_4706" s="T500">arɨj</ta>
            <ta e="T502" id="Seg_4707" s="T501">di͡e-BIT</ta>
            <ta e="T503" id="Seg_4708" s="T502">emeːksin-tA</ta>
            <ta e="T504" id="Seg_4709" s="T503">aːn-tI-n</ta>
            <ta e="T505" id="Seg_4710" s="T504">arɨj-An</ta>
            <ta e="T506" id="Seg_4711" s="T505">obu͡oj-LAN-BIT</ta>
            <ta e="T507" id="Seg_4712" s="T506">emeːksin</ta>
            <ta e="T508" id="Seg_4713" s="T507">ahaː-A-t-An</ta>
            <ta e="T509" id="Seg_4714" s="T508">baran</ta>
            <ta e="T510" id="Seg_4715" s="T509">dʼi͡e-tI-GAr</ta>
            <ta e="T511" id="Seg_4716" s="T510">ilt-BIT</ta>
            <ta e="T512" id="Seg_4717" s="T511">bu</ta>
            <ta e="T513" id="Seg_4718" s="T512">dʼi͡e-tI-GAr</ta>
            <ta e="T514" id="Seg_4719" s="T513">ilt-An</ta>
            <ta e="T515" id="Seg_4720" s="T514">huːj-An-taraː-An</ta>
            <ta e="T516" id="Seg_4721" s="T515">baran</ta>
            <ta e="T517" id="Seg_4722" s="T516">taŋɨn-I-BAtAK</ta>
            <ta e="T518" id="Seg_4723" s="T517">taŋas-nI</ta>
            <ta e="T519" id="Seg_4724" s="T518">taŋɨn-TAr-An</ta>
            <ta e="T520" id="Seg_4725" s="T519">baran</ta>
            <ta e="T521" id="Seg_4726" s="T520">taːs</ta>
            <ta e="T522" id="Seg_4727" s="T521">kɨrabaːt-GA</ta>
            <ta e="T523" id="Seg_4728" s="T522">olort-An</ta>
            <ta e="T524" id="Seg_4729" s="T523">keːs-BIT</ta>
            <ta e="T525" id="Seg_4730" s="T524">ol</ta>
            <ta e="T526" id="Seg_4731" s="T525">gɨn-An</ta>
            <ta e="T527" id="Seg_4732" s="T526">baran</ta>
            <ta e="T528" id="Seg_4733" s="T527">čaːj</ta>
            <ta e="T529" id="Seg_4734" s="T528">is-A-r.[t]-BIT</ta>
            <ta e="T530" id="Seg_4735" s="T529">onton</ta>
            <ta e="T531" id="Seg_4736" s="T530">et</ta>
            <ta e="T532" id="Seg_4737" s="T531">kü͡ös</ta>
            <ta e="T533" id="Seg_4738" s="T532">kotor-BIT</ta>
            <ta e="T534" id="Seg_4739" s="T533">hɨ͡a-LAːK</ta>
            <ta e="T535" id="Seg_4740" s="T534">min-nI</ta>
            <ta e="T536" id="Seg_4741" s="T535">kɨtɨja-GA</ta>
            <ta e="T537" id="Seg_4742" s="T536">kotor-BIT</ta>
            <ta e="T538" id="Seg_4743" s="T537">innʼe</ta>
            <ta e="T539" id="Seg_4744" s="T538">gɨn-An</ta>
            <ta e="T540" id="Seg_4745" s="T539">kömüs</ta>
            <ta e="T541" id="Seg_4746" s="T540">lu͡osku</ta>
            <ta e="T542" id="Seg_4747" s="T541">biːlke</ta>
            <ta e="T543" id="Seg_4748" s="T542">uːr-BIT</ta>
            <ta e="T544" id="Seg_4749" s="T543">agaj</ta>
            <ta e="T545" id="Seg_4750" s="T544">lu͡osku-tI-nAn</ta>
            <ta e="T546" id="Seg_4751" s="T545">is-AːrI</ta>
            <ta e="T547" id="Seg_4752" s="T546">gɨn-BIT</ta>
            <ta e="T548" id="Seg_4753" s="T547">kaja</ta>
            <ta e="T549" id="Seg_4754" s="T548">bu-bu</ta>
            <ta e="T550" id="Seg_4755" s="T549">lu͡osku-m</ta>
            <ta e="T551" id="Seg_4756" s="T550">dʼogus</ta>
            <ta e="T552" id="Seg_4757" s="T551">ulakan</ta>
            <ta e="T553" id="Seg_4758" s="T552">pabaraːŋkɨ-TA</ta>
            <ta e="T554" id="Seg_4759" s="T553">du͡o</ta>
            <ta e="T555" id="Seg_4760" s="T554">komu͡os-TA</ta>
            <ta e="T556" id="Seg_4761" s="T555">du͡o</ta>
            <ta e="T557" id="Seg_4762" s="T556">egel</ta>
            <ta e="T558" id="Seg_4763" s="T557">di͡e-BIT</ta>
            <ta e="T559" id="Seg_4764" s="T558">maːgɨ-tA</ta>
            <ta e="T560" id="Seg_4765" s="T559">komu͡os</ta>
            <ta e="T561" id="Seg_4766" s="T560">bagajɨ-tA</ta>
            <ta e="T562" id="Seg_4767" s="T561">tuttar-An</ta>
            <ta e="T563" id="Seg_4768" s="T562">keːs-BIT</ta>
            <ta e="T564" id="Seg_4769" s="T563">hɨ͡a-LAːK</ta>
            <ta e="T565" id="Seg_4770" s="T564">min-nI</ta>
            <ta e="T566" id="Seg_4771" s="T565">bu-tI-nAn</ta>
            <ta e="T567" id="Seg_4772" s="T566">huːp</ta>
            <ta e="T568" id="Seg_4773" s="T567">gɨn-BIT</ta>
            <ta e="T569" id="Seg_4774" s="T568">bu-GA</ta>
            <ta e="T570" id="Seg_4775" s="T569">dʼe</ta>
            <ta e="T571" id="Seg_4776" s="T570">čačaj-Ar</ta>
            <ta e="T572" id="Seg_4777" s="T571">diː</ta>
            <ta e="T573" id="Seg_4778" s="T572">dʼe</ta>
            <ta e="T574" id="Seg_4779" s="T573">onton</ta>
            <ta e="T575" id="Seg_4780" s="T574">čačaj-A-BIn</ta>
            <ta e="T576" id="Seg_4781" s="T575">di͡e-An</ta>
            <ta e="T577" id="Seg_4782" s="T576">dʼe</ta>
            <ta e="T578" id="Seg_4783" s="T577">uturuk-LAː-Ar</ta>
            <ta e="T579" id="Seg_4784" s="T578">diː</ta>
            <ta e="T580" id="Seg_4785" s="T579">taːs</ta>
            <ta e="T581" id="Seg_4786" s="T580">olbok-tI-n</ta>
            <ta e="T582" id="Seg_4787" s="T581">bɨha</ta>
            <ta e="T583" id="Seg_4788" s="T582">uturuk-LAː-Ar</ta>
            <ta e="T584" id="Seg_4789" s="T583">da</ta>
            <ta e="T585" id="Seg_4790" s="T584">onon</ta>
            <ta e="T586" id="Seg_4791" s="T585">üs</ta>
            <ta e="T587" id="Seg_4792" s="T586">buluːs</ta>
            <ta e="T588" id="Seg_4793" s="T587">hir-nI</ta>
            <ta e="T589" id="Seg_4794" s="T588">tobulu</ta>
            <ta e="T590" id="Seg_4795" s="T589">tüs-An</ta>
            <ta e="T591" id="Seg_4796" s="T590">kaːl-Ar</ta>
            <ta e="T592" id="Seg_4797" s="T591">emeːksin-tA</ta>
            <ta e="T593" id="Seg_4798" s="T592">tu͡ok</ta>
            <ta e="T594" id="Seg_4799" s="T593">bu͡ol-IAK-tA=Ij</ta>
            <ta e="T595" id="Seg_4800" s="T594">kajdak</ta>
            <ta e="T596" id="Seg_4801" s="T595">olor-BIT-tA</ta>
            <ta e="T597" id="Seg_4802" s="T596">da</ta>
            <ta e="T598" id="Seg_4803" s="T597">ogorduk</ta>
            <ta e="T601" id="Seg_4804" s="T598">bi͡ek</ta>
            <ta e="T599" id="Seg_4805" s="T601">bi͡ek</ta>
            <ta e="T600" id="Seg_4806" s="T599">olor-Ar</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_4807" s="T0">old.man-PROPR</ta>
            <ta e="T2" id="Seg_4808" s="T1">old.woman.[NOM]</ta>
            <ta e="T3" id="Seg_4809" s="T2">live-PST2-3PL</ta>
            <ta e="T4" id="Seg_4810" s="T3">it.is.said</ta>
            <ta e="T5" id="Seg_4811" s="T4">give.birth-EP-CAUS-PTCP.PRS</ta>
            <ta e="T6" id="Seg_4812" s="T5">child-POSS</ta>
            <ta e="T7" id="Seg_4813" s="T6">NEG-3PL</ta>
            <ta e="T8" id="Seg_4814" s="T7">tent-DAT/LOC</ta>
            <ta e="T9" id="Seg_4815" s="T8">sit-TEMP-3PL</ta>
            <ta e="T10" id="Seg_4816" s="T9">fire-3PL.[NOM]</ta>
            <ta e="T11" id="Seg_4817" s="T10">just</ta>
            <ta e="T12" id="Seg_4818" s="T11">get.red-PRS.[3SG]</ta>
            <ta e="T13" id="Seg_4819" s="T12">like.that</ta>
            <ta e="T14" id="Seg_4820" s="T13">poor-PL.[NOM]</ta>
            <ta e="T15" id="Seg_4821" s="T14">out</ta>
            <ta e="T16" id="Seg_4822" s="T15">go.out-TEMP-3PL</ta>
            <ta e="T17" id="Seg_4823" s="T16">snow-3PL.[NOM]</ta>
            <ta e="T18" id="Seg_4824" s="T17">just</ta>
            <ta e="T19" id="Seg_4825" s="T18">be.white-PRS.[3SG]</ta>
            <ta e="T20" id="Seg_4826" s="T19">what-POSS</ta>
            <ta e="T21" id="Seg_4827" s="T20">NEG</ta>
            <ta e="T22" id="Seg_4828" s="T21">NEG-3PL</ta>
            <ta e="T23" id="Seg_4829" s="T22">only</ta>
            <ta e="T24" id="Seg_4830" s="T23">resources-3PL.[NOM]</ta>
            <ta e="T25" id="Seg_4831" s="T24">say-CVB.SEQ-3PL</ta>
            <ta e="T26" id="Seg_4832" s="T25">EMPH-lonely</ta>
            <ta e="T27" id="Seg_4833" s="T26">horse-PROPR-3PL</ta>
            <ta e="T28" id="Seg_4834" s="T27">then</ta>
            <ta e="T29" id="Seg_4835" s="T28">hunt-PTCP.PRS</ta>
            <ta e="T30" id="Seg_4836" s="T29">equipment-3PL.[NOM]</ta>
            <ta e="T31" id="Seg_4837" s="T30">three</ta>
            <ta e="T32" id="Seg_4838" s="T31">stone-PROPR</ta>
            <ta e="T33" id="Seg_4839" s="T32">net-DIM-PROPR-3PL</ta>
            <ta e="T34" id="Seg_4840" s="T33">that-3SG-3PL-DAT/LOC</ta>
            <ta e="T35" id="Seg_4841" s="T34">water.[NOM]</ta>
            <ta e="T36" id="Seg_4842" s="T35">small.creatures-3SG.[NOM]</ta>
            <ta e="T37" id="Seg_4843" s="T36">fasten-EP-REFL-TEMP-3SG</ta>
            <ta e="T38" id="Seg_4844" s="T37">that-ACC</ta>
            <ta e="T39" id="Seg_4845" s="T38">eat-PRS-3PL</ta>
            <ta e="T40" id="Seg_4846" s="T39">skewer-DAT/LOC</ta>
            <ta e="T41" id="Seg_4847" s="T40">fry-CVB.SEQ</ta>
            <ta e="T42" id="Seg_4848" s="T41">only</ta>
            <ta e="T43" id="Seg_4849" s="T42">die-CVB.PURP</ta>
            <ta e="T44" id="Seg_4850" s="T43">slouch-PST2-3PL</ta>
            <ta e="T45" id="Seg_4851" s="T44">come.to.an.end-CVB.PURP</ta>
            <ta e="T46" id="Seg_4852" s="T45">hold.in.stomach-PST2-3PL</ta>
            <ta e="T47" id="Seg_4853" s="T46">then</ta>
            <ta e="T48" id="Seg_4854" s="T47">old.man-3SG.[NOM]</ta>
            <ta e="T49" id="Seg_4855" s="T48">suddenly</ta>
            <ta e="T50" id="Seg_4856" s="T49">go.in-PST2.[3SG]</ta>
            <ta e="T51" id="Seg_4857" s="T50">old.woman-3SG-DAT/LOC</ta>
            <ta e="T52" id="Seg_4858" s="T51">well</ta>
            <ta e="T53" id="Seg_4859" s="T52">go.in-CVB.SEQ</ta>
            <ta e="T54" id="Seg_4860" s="T53">say-PST2.[3SG]</ta>
            <ta e="T55" id="Seg_4861" s="T54">well</ta>
            <ta e="T56" id="Seg_4862" s="T55">old.woman.[NOM]</ta>
            <ta e="T57" id="Seg_4863" s="T56">get.up-PRS-1PL</ta>
            <ta e="T58" id="Seg_4864" s="T57">walk-CAUS-FUT-1PL</ta>
            <ta e="T59" id="Seg_4865" s="T58">lonely</ta>
            <ta e="T60" id="Seg_4866" s="T59">horse-PROPR-1PL</ta>
            <ta e="T61" id="Seg_4867" s="T60">eat-PTCP.FUT-1PL.[NOM]</ta>
            <ta e="T62" id="Seg_4868" s="T61">there.is-3SG</ta>
            <ta e="T63" id="Seg_4869" s="T62">even</ta>
            <ta e="T64" id="Seg_4870" s="T63">hunger-CVB.SEQ</ta>
            <ta e="T65" id="Seg_4871" s="T64">die-TEMP-1PL</ta>
            <ta e="T66" id="Seg_4872" s="T65">back-1PL-DAT/LOC</ta>
            <ta e="T67" id="Seg_4873" s="T66">stay-FUT-3SG</ta>
            <ta e="T68" id="Seg_4874" s="T67">that-DAT/LOC</ta>
            <ta e="T69" id="Seg_4875" s="T68">old.woman.[NOM]</ta>
            <ta e="T70" id="Seg_4876" s="T69">self-2SG.[NOM]</ta>
            <ta e="T71" id="Seg_4877" s="T70">thought-2SG.[NOM]</ta>
            <ta e="T72" id="Seg_4878" s="T71">say-PST2.[3SG]</ta>
            <ta e="T73" id="Seg_4879" s="T72">kill-COND-2SG</ta>
            <ta e="T74" id="Seg_4880" s="T73">just</ta>
            <ta e="T75" id="Seg_4881" s="T74">eat-FUT-1PL</ta>
            <ta e="T76" id="Seg_4882" s="T75">man-3SG.[NOM]</ta>
            <ta e="T77" id="Seg_4883" s="T76">go.out-PST2.[3SG]</ta>
            <ta e="T78" id="Seg_4884" s="T77">and</ta>
            <ta e="T79" id="Seg_4885" s="T78">kill-PST2.[3SG]</ta>
            <ta e="T80" id="Seg_4886" s="T79">old.man-3SG.[NOM]</ta>
            <ta e="T81" id="Seg_4887" s="T80">skin-PST2.[3SG]</ta>
            <ta e="T82" id="Seg_4888" s="T81">only</ta>
            <ta e="T83" id="Seg_4889" s="T82">cold.[NOM]</ta>
            <ta e="T84" id="Seg_4890" s="T83">very</ta>
            <ta e="T85" id="Seg_4891" s="T84">frosty.[NOM]</ta>
            <ta e="T86" id="Seg_4892" s="T85">very</ta>
            <ta e="T87" id="Seg_4893" s="T86">old.man-3SG.[NOM]</ta>
            <ta e="T88" id="Seg_4894" s="T87">go.in-PST2.[3SG]</ta>
            <ta e="T89" id="Seg_4895" s="T88">go.in-CVB.SEQ</ta>
            <ta e="T90" id="Seg_4896" s="T89">say-PST2.[3SG]</ta>
            <ta e="T91" id="Seg_4897" s="T90">horse-1PL.[NOM]</ta>
            <ta e="T92" id="Seg_4898" s="T91">meat-3SG-ACC</ta>
            <ta e="T93" id="Seg_4899" s="T92">intact-SIM</ta>
            <ta e="T94" id="Seg_4900" s="T93">four_cornered.poletent-DIM-1PL.[NOM]</ta>
            <ta e="T95" id="Seg_4901" s="T94">to.the.other.shore</ta>
            <ta e="T96" id="Seg_4902" s="T95">pull-VBZ-CVB.SEQ</ta>
            <ta e="T97" id="Seg_4903" s="T96">throw.[IMP.2SG]</ta>
            <ta e="T98" id="Seg_4904" s="T97">whole-3SG-ACC</ta>
            <ta e="T99" id="Seg_4905" s="T98">old.woman-3SG.[NOM]</ta>
            <ta e="T100" id="Seg_4906" s="T99">whole-3SG-ACC</ta>
            <ta e="T101" id="Seg_4907" s="T100">pull-VBZ-PST2.[3SG]</ta>
            <ta e="T102" id="Seg_4908" s="T101">old.woman-3SG.[NOM]</ta>
            <ta e="T103" id="Seg_4909" s="T102">hand-3SG-ACC</ta>
            <ta e="T104" id="Seg_4910" s="T103">become.warm-CAUS-CVB.PURP</ta>
            <ta e="T105" id="Seg_4911" s="T104">want-PTCP.PST-3SG-ACC</ta>
            <ta e="T106" id="Seg_4912" s="T105">old.man.[NOM]</ta>
            <ta e="T107" id="Seg_4913" s="T106">speak-PST2.[3SG]</ta>
            <ta e="T108" id="Seg_4914" s="T107">eat-PTCP.FUT-2SG-ACC</ta>
            <ta e="T109" id="Seg_4915" s="T108">every</ta>
            <ta e="T110" id="Seg_4916" s="T109">jar-2SG-DAT/LOC</ta>
            <ta e="T111" id="Seg_4917" s="T110">bucket-2SG-DAT/LOC</ta>
            <ta e="T112" id="Seg_4918" s="T111">water-PART</ta>
            <ta e="T113" id="Seg_4919" s="T112">scoop-CVB.SEQ</ta>
            <ta e="T114" id="Seg_4920" s="T113">fill.[IMP.2SG]</ta>
            <ta e="T115" id="Seg_4921" s="T114">ready-VBZ-CVB.SEQ</ta>
            <ta e="T116" id="Seg_4922" s="T115">after</ta>
            <ta e="T117" id="Seg_4923" s="T116">well</ta>
            <ta e="T118" id="Seg_4924" s="T117">eat-FUT.[IMP.2SG]</ta>
            <ta e="T119" id="Seg_4925" s="T118">so</ta>
            <ta e="T120" id="Seg_4926" s="T119">make-CVB.SEQ</ta>
            <ta e="T121" id="Seg_4927" s="T120">old.woman.[NOM]</ta>
            <ta e="T122" id="Seg_4928" s="T121">go.out-CVB.SEQ</ta>
            <ta e="T123" id="Seg_4929" s="T122">every</ta>
            <ta e="T124" id="Seg_4930" s="T123">jar-3SG-DAT/LOC</ta>
            <ta e="T125" id="Seg_4931" s="T124">whole-DIM-3PL-DAT/LOC</ta>
            <ta e="T126" id="Seg_4932" s="T125">water.[NOM]</ta>
            <ta e="T127" id="Seg_4933" s="T126">scoop-PST2.[3SG]</ta>
            <ta e="T128" id="Seg_4934" s="T127">only</ta>
            <ta e="T129" id="Seg_4935" s="T128">last</ta>
            <ta e="T130" id="Seg_4936" s="T129">kettle-DAT/LOC-DIM-DIM-DIM-3SG-ACC</ta>
            <ta e="T131" id="Seg_4937" s="T130">bring-CVB.PURP</ta>
            <ta e="T132" id="Seg_4938" s="T131">want-PST2-3SG</ta>
            <ta e="T133" id="Seg_4939" s="T132">door-3SG.[NOM]</ta>
            <ta e="T134" id="Seg_4940" s="T133">locked.[NOM]</ta>
            <ta e="T135" id="Seg_4941" s="T134">well</ta>
            <ta e="T136" id="Seg_4942" s="T135">old.man.[NOM]</ta>
            <ta e="T137" id="Seg_4943" s="T136">door-2SG-ACC</ta>
            <ta e="T138" id="Seg_4944" s="T137">push.[IMP.2SG]</ta>
            <ta e="T139" id="Seg_4945" s="T138">freeze-CVB.PURP</ta>
            <ta e="T140" id="Seg_4946" s="T139">make-PST1-1SG</ta>
            <ta e="T141" id="Seg_4947" s="T140">say-PST2.[3SG]</ta>
            <ta e="T142" id="Seg_4948" s="T141">bitch.[NOM]</ta>
            <ta e="T143" id="Seg_4949" s="T142">daughter-3SG.[NOM]</ta>
            <ta e="T144" id="Seg_4950" s="T143">snow-DAT/LOC</ta>
            <ta e="T145" id="Seg_4951" s="T144">apart</ta>
            <ta e="T146" id="Seg_4952" s="T145">throw.[IMP.2SG]</ta>
            <ta e="T147" id="Seg_4953" s="T146">say-PST2.[3SG]</ta>
            <ta e="T148" id="Seg_4954" s="T147">old.woman-3SG.[NOM]</ta>
            <ta e="T149" id="Seg_4955" s="T148">this</ta>
            <ta e="T150" id="Seg_4956" s="T149">kettle-DIM-3SG-ACC</ta>
            <ta e="T151" id="Seg_4957" s="T150">throw-CVB.SEQ</ta>
            <ta e="T152" id="Seg_4958" s="T151">after</ta>
            <ta e="T153" id="Seg_4959" s="T152">well</ta>
            <ta e="T154" id="Seg_4960" s="T153">freeze-CVB.SEQ</ta>
            <ta e="T155" id="Seg_4961" s="T154">die-CVB.PURP</ta>
            <ta e="T156" id="Seg_4962" s="T155">make-PRS.[3SG]</ta>
            <ta e="T157" id="Seg_4963" s="T156">old.woman-3SG.[NOM]</ta>
            <ta e="T158" id="Seg_4964" s="T157">yurt-3SG-GEN</ta>
            <ta e="T159" id="Seg_4965" s="T158">upper.part-3SG-DAT/LOC</ta>
            <ta e="T160" id="Seg_4966" s="T159">go.out-CVB.SEQ</ta>
            <ta e="T161" id="Seg_4967" s="T160">spark-DIM-PL.[NOM]</ta>
            <ta e="T162" id="Seg_4968" s="T161">go.out-NMNZ-3PL-DAT/LOC</ta>
            <ta e="T163" id="Seg_4969" s="T162">there</ta>
            <ta e="T164" id="Seg_4970" s="T163">hand-3SG-ACC</ta>
            <ta e="T165" id="Seg_4971" s="T164">become.warm-CAUS-PRS.[3SG]</ta>
            <ta e="T166" id="Seg_4972" s="T165">only</ta>
            <ta e="T167" id="Seg_4973" s="T166">husband-3SG-ACC</ta>
            <ta e="T168" id="Seg_4974" s="T167">see-PST2-3SG</ta>
            <ta e="T169" id="Seg_4975" s="T168">horse-3SG-GEN</ta>
            <ta e="T170" id="Seg_4976" s="T169">perineum-3SG-ACC</ta>
            <ta e="T171" id="Seg_4977" s="T170">take-CVB.SEQ</ta>
            <ta e="T172" id="Seg_4978" s="T171">after</ta>
            <ta e="T173" id="Seg_4979" s="T172">place-CVB.SEQ</ta>
            <ta e="T174" id="Seg_4980" s="T173">after</ta>
            <ta e="T175" id="Seg_4981" s="T174">gut.fat-VBZ-CVB.SIM</ta>
            <ta e="T176" id="Seg_4982" s="T175">sit-PRS.[3SG]</ta>
            <ta e="T177" id="Seg_4983" s="T176">that</ta>
            <ta e="T178" id="Seg_4984" s="T177">become.warm-CAUS-CVB.PURP</ta>
            <ta e="T179" id="Seg_4985" s="T178">make-PTCP.PRS</ta>
            <ta e="T180" id="Seg_4986" s="T179">old.woman-3SG-GEN</ta>
            <ta e="T181" id="Seg_4987" s="T180">hand-3SG-ACC</ta>
            <ta e="T182" id="Seg_4988" s="T181">peaked.pole-INSTR</ta>
            <ta e="T183" id="Seg_4989" s="T182">cut-CVB.SIM</ta>
            <ta e="T184" id="Seg_4990" s="T183">chop-PRS.[3SG]</ta>
            <ta e="T185" id="Seg_4991" s="T184">that-3SG-3SG.[NOM]</ta>
            <ta e="T186" id="Seg_4992" s="T185">cry-CVB.SIM-cry-CVB.SIM</ta>
            <ta e="T187" id="Seg_4993" s="T186">earth-DAT/LOC</ta>
            <ta e="T188" id="Seg_4994" s="T187">go.down-EMOT-CVB.SEQ</ta>
            <ta e="T189" id="Seg_4995" s="T188">after</ta>
            <ta e="T190" id="Seg_4996" s="T189">wood-VBZ-PTCP.PRS</ta>
            <ta e="T191" id="Seg_4997" s="T190">place-DIM.[NOM]</ta>
            <ta e="T192" id="Seg_4998" s="T191">to</ta>
            <ta e="T193" id="Seg_4999" s="T192">cry-CVB.SIM-cry-CVB.SIM</ta>
            <ta e="T194" id="Seg_5000" s="T193">stride-CVB.SIM</ta>
            <ta e="T195" id="Seg_5001" s="T194">stand-PRS.[3SG]</ta>
            <ta e="T196" id="Seg_5002" s="T195">well</ta>
            <ta e="T197" id="Seg_5003" s="T196">there</ta>
            <ta e="T198" id="Seg_5004" s="T197">larch-EP-DIM.[NOM]</ta>
            <ta e="T199" id="Seg_5005" s="T198">tree.[NOM]</ta>
            <ta e="T200" id="Seg_5006" s="T199">root-3SG-DAT/LOC</ta>
            <ta e="T201" id="Seg_5007" s="T200">lie.down-CVB.PURP</ta>
            <ta e="T202" id="Seg_5008" s="T201">want-PST2.[3SG]</ta>
            <ta e="T203" id="Seg_5009" s="T202">lie.down-CVB.PURP</ta>
            <ta e="T204" id="Seg_5010" s="T203">want-PST2.[3SG]</ta>
            <ta e="T205" id="Seg_5011" s="T204">cold.[NOM]</ta>
            <ta e="T206" id="Seg_5012" s="T205">shiver-PTCP.PRS.[NOM]</ta>
            <ta e="T207" id="Seg_5013" s="T206">heart-3SG-DAT/LOC</ta>
            <ta e="T208" id="Seg_5014" s="T207">go.in-PRS.[3SG]</ta>
            <ta e="T209" id="Seg_5015" s="T208">that</ta>
            <ta e="T210" id="Seg_5016" s="T209">lie-TEMP-3SG</ta>
            <ta e="T211" id="Seg_5017" s="T210">noise.[NOM]</ta>
            <ta e="T212" id="Seg_5018" s="T211">EMPH</ta>
            <ta e="T213" id="Seg_5019" s="T212">be.heard-EP-PST2.[3SG]</ta>
            <ta e="T214" id="Seg_5020" s="T213">reindeer.[NOM]</ta>
            <ta e="T215" id="Seg_5021" s="T214">hunt-PTCP.PRS-ABL</ta>
            <ta e="T216" id="Seg_5022" s="T215">reindeer.[NOM]</ta>
            <ta e="T217" id="Seg_5023" s="T216">hunt-PTCP.PRS.[NOM]</ta>
            <ta e="T218" id="Seg_5024" s="T217">different</ta>
            <ta e="T219" id="Seg_5025" s="T218">very</ta>
            <ta e="T220" id="Seg_5026" s="T219">that.[NOM]</ta>
            <ta e="T221" id="Seg_5027" s="T220">in.the.direction</ta>
            <ta e="T222" id="Seg_5028" s="T221">well</ta>
            <ta e="T223" id="Seg_5029" s="T222">go-PRS.[3SG]</ta>
            <ta e="T224" id="Seg_5030" s="T223">this</ta>
            <ta e="T225" id="Seg_5031" s="T224">old.woman.[NOM]</ta>
            <ta e="T226" id="Seg_5032" s="T225">that</ta>
            <ta e="T227" id="Seg_5033" s="T226">go-CVB.SEQ</ta>
            <ta e="T228" id="Seg_5034" s="T227">good.spirit.[NOM]</ta>
            <ta e="T229" id="Seg_5035" s="T228">EMPH</ta>
            <ta e="T230" id="Seg_5036" s="T229">moon-ACC</ta>
            <ta e="T231" id="Seg_5037" s="T230">cover-PTCP.PRS</ta>
            <ta e="T232" id="Seg_5038" s="T231">dense</ta>
            <ta e="T233" id="Seg_5039" s="T232">fog.[NOM]</ta>
            <ta e="T234" id="Seg_5040" s="T233">reindeer-DAT/LOC</ta>
            <ta e="T235" id="Seg_5041" s="T234">sun-ACC</ta>
            <ta e="T236" id="Seg_5042" s="T235">cover-PTCP.PRS</ta>
            <ta e="T237" id="Seg_5043" s="T236">light.fog.[NOM]</ta>
            <ta e="T238" id="Seg_5044" s="T237">fog.[NOM]</ta>
            <ta e="T239" id="Seg_5045" s="T238">cattle-DAT/LOC</ta>
            <ta e="T240" id="Seg_5046" s="T239">reach-PRS.[3SG]</ta>
            <ta e="T241" id="Seg_5047" s="T240">this</ta>
            <ta e="T242" id="Seg_5048" s="T241">old.woman-EP-2SG.[NOM]</ta>
            <ta e="T243" id="Seg_5049" s="T242">house-ACC</ta>
            <ta e="T244" id="Seg_5050" s="T243">search-CVB.SEQ</ta>
            <ta e="T245" id="Seg_5051" s="T244">this</ta>
            <ta e="T246" id="Seg_5052" s="T245">reindeer-ACC</ta>
            <ta e="T247" id="Seg_5053" s="T246">cut.through-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T248" id="Seg_5054" s="T247">two</ta>
            <ta e="T249" id="Seg_5055" s="T248">half-3SG.[NOM]</ta>
            <ta e="T250" id="Seg_5056" s="T249">grind-CVB.SEQ</ta>
            <ta e="T251" id="Seg_5057" s="T250">stay-PST2.[3SG]</ta>
            <ta e="T252" id="Seg_5058" s="T251">suddenly</ta>
            <ta e="T253" id="Seg_5059" s="T252">valley-ACC</ta>
            <ta e="T254" id="Seg_5060" s="T253">EMPH-full</ta>
            <ta e="T255" id="Seg_5061" s="T254">pole.tent.[NOM]</ta>
            <ta e="T256" id="Seg_5062" s="T255">house.[NOM]</ta>
            <ta e="T257" id="Seg_5063" s="T256">build-EP-PASS/REFL-CVB.SEQ</ta>
            <ta e="T258" id="Seg_5064" s="T257">stand-PRS.[3SG]</ta>
            <ta e="T259" id="Seg_5065" s="T258">big</ta>
            <ta e="T260" id="Seg_5066" s="T259">very</ta>
            <ta e="T261" id="Seg_5067" s="T260">tent.[NOM]</ta>
            <ta e="T262" id="Seg_5068" s="T261">come-PST1-3SG</ta>
            <ta e="T263" id="Seg_5069" s="T262">this</ta>
            <ta e="T264" id="Seg_5070" s="T263">tent-DAT/LOC</ta>
            <ta e="T265" id="Seg_5071" s="T264">here</ta>
            <ta e="T266" id="Seg_5072" s="T265">well</ta>
            <ta e="T267" id="Seg_5073" s="T266">how.much</ta>
            <ta e="T268" id="Seg_5074" s="T267">ten</ta>
            <ta e="T269" id="Seg_5075" s="T268">MOD</ta>
            <ta e="T270" id="Seg_5076" s="T269">cut-CVB.SIM</ta>
            <ta e="T271" id="Seg_5077" s="T270">caravan-3SG.[NOM]</ta>
            <ta e="T272" id="Seg_5078" s="T271">balok-3SG.[NOM]</ta>
            <ta e="T273" id="Seg_5079" s="T272">stretch.out-CVB.SEQ</ta>
            <ta e="T274" id="Seg_5080" s="T273">stand-PRS.[3SG]</ta>
            <ta e="T275" id="Seg_5081" s="T274">so</ta>
            <ta e="T276" id="Seg_5082" s="T275">make-CVB.SEQ</ta>
            <ta e="T277" id="Seg_5083" s="T276">this</ta>
            <ta e="T278" id="Seg_5084" s="T277">old.woman.[NOM]</ta>
            <ta e="T279" id="Seg_5085" s="T278">this</ta>
            <ta e="T280" id="Seg_5086" s="T279">tent-ACC</ta>
            <ta e="T281" id="Seg_5087" s="T280">open-CVB.SIM</ta>
            <ta e="T282" id="Seg_5088" s="T281">beat-EP-PST2.[3SG]</ta>
            <ta e="T283" id="Seg_5089" s="T282">watch-CVB.SEQ</ta>
            <ta e="T284" id="Seg_5090" s="T283">see-PST2-3SG</ta>
            <ta e="T285" id="Seg_5091" s="T284">EMPH-empty.[NOM]</ta>
            <ta e="T286" id="Seg_5092" s="T285">human.being-3SG.[NOM]</ta>
            <ta e="T287" id="Seg_5093" s="T286">NEG.EX</ta>
            <ta e="T288" id="Seg_5094" s="T287">every</ta>
            <ta e="T289" id="Seg_5095" s="T288">kettle.[NOM]</ta>
            <ta e="T290" id="Seg_5096" s="T289">kettle-VBZ-CVB.SEQ</ta>
            <ta e="T291" id="Seg_5097" s="T290">stand-PRS.[3SG]</ta>
            <ta e="T292" id="Seg_5098" s="T291">every</ta>
            <ta e="T293" id="Seg_5099" s="T292">food.[NOM]</ta>
            <ta e="T294" id="Seg_5100" s="T293">food-VBZ-CVB.SEQ</ta>
            <ta e="T295" id="Seg_5101" s="T294">stand-PTCP.PRS.[3SG]</ta>
            <ta e="T296" id="Seg_5102" s="T295">stone.[NOM]</ta>
            <ta e="T297" id="Seg_5103" s="T296">bed-PL.[NOM]</ta>
            <ta e="T298" id="Seg_5104" s="T297">intact-SIM</ta>
            <ta e="T299" id="Seg_5105" s="T298">such</ta>
            <ta e="T300" id="Seg_5106" s="T299">equipment-PROPR</ta>
            <ta e="T301" id="Seg_5107" s="T300">tent.[NOM]</ta>
            <ta e="T302" id="Seg_5108" s="T301">stand-PRS.[3SG]</ta>
            <ta e="T303" id="Seg_5109" s="T302">that</ta>
            <ta e="T304" id="Seg_5110" s="T303">old.woman.[NOM]</ta>
            <ta e="T305" id="Seg_5111" s="T304">go.in-CVB.SEQ</ta>
            <ta e="T306" id="Seg_5112" s="T305">eat-NEG.PTCP.PST-3SG-ACC</ta>
            <ta e="T307" id="Seg_5113" s="T306">eat-PRS.[3SG]</ta>
            <ta e="T308" id="Seg_5114" s="T307">eat-NEG.PTCP.PST-3SG-ACC</ta>
            <ta e="T309" id="Seg_5115" s="T308">eat-PRS.[3SG]</ta>
            <ta e="T310" id="Seg_5116" s="T309">Russian</ta>
            <ta e="T311" id="Seg_5117" s="T310">and</ta>
            <ta e="T312" id="Seg_5118" s="T311">food-3SG.[NOM]</ta>
            <ta e="T313" id="Seg_5119" s="T312">many</ta>
            <ta e="T314" id="Seg_5120" s="T313">Dolgan</ta>
            <ta e="T315" id="Seg_5121" s="T314">and</ta>
            <ta e="T316" id="Seg_5122" s="T315">food-3SG.[NOM]</ta>
            <ta e="T317" id="Seg_5123" s="T316">many</ta>
            <ta e="T318" id="Seg_5124" s="T317">this</ta>
            <ta e="T319" id="Seg_5125" s="T318">old.woman.[NOM]</ta>
            <ta e="T320" id="Seg_5126" s="T319">wash.oneself-CVB.SEQ-comb.oneself-CVB.SEQ</ta>
            <ta e="T321" id="Seg_5127" s="T320">after</ta>
            <ta e="T322" id="Seg_5128" s="T321">lynx.[NOM]-sable.[NOM]</ta>
            <ta e="T323" id="Seg_5129" s="T322">clothes-ACC</ta>
            <ta e="T324" id="Seg_5130" s="T323">dress-CVB.SEQ</ta>
            <ta e="T325" id="Seg_5131" s="T324">throw-PST2.[3SG]</ta>
            <ta e="T326" id="Seg_5132" s="T325">this.[NOM]</ta>
            <ta e="T327" id="Seg_5133" s="T326">while</ta>
            <ta e="T328" id="Seg_5134" s="T327">what.[NOM]</ta>
            <ta e="T329" id="Seg_5135" s="T328">NEG</ta>
            <ta e="T330" id="Seg_5136" s="T329">human.being-3SG.[NOM]</ta>
            <ta e="T331" id="Seg_5137" s="T330">notice-PASS/REFL-EP-NEG.[3SG]</ta>
            <ta e="T332" id="Seg_5138" s="T331">three</ta>
            <ta e="T333" id="Seg_5139" s="T332">day-ACC</ta>
            <ta e="T334" id="Seg_5140" s="T333">during</ta>
            <ta e="T335" id="Seg_5141" s="T334">what.[NOM]</ta>
            <ta e="T336" id="Seg_5142" s="T335">NEG</ta>
            <ta e="T337" id="Seg_5143" s="T336">human.being-3SG.[NOM]</ta>
            <ta e="T338" id="Seg_5144" s="T337">notice-PASS/REFL-EP-NEG.[3SG]</ta>
            <ta e="T339" id="Seg_5145" s="T338">only</ta>
            <ta e="T340" id="Seg_5146" s="T339">evening.[NOM]</ta>
            <ta e="T341" id="Seg_5147" s="T340">be-FUT.[3SG]</ta>
            <ta e="T342" id="Seg_5148" s="T341">and</ta>
            <ta e="T343" id="Seg_5149" s="T342">chimney-3SG-INSTR</ta>
            <ta e="T344" id="Seg_5150" s="T343">old</ta>
            <ta e="T345" id="Seg_5151" s="T344">human.being.[NOM]</ta>
            <ta e="T346" id="Seg_5152" s="T345">head-PL-3SG.[NOM]</ta>
            <ta e="T347" id="Seg_5153" s="T346">fall-PRS-3PL</ta>
            <ta e="T348" id="Seg_5154" s="T347">that-PL-ACC</ta>
            <ta e="T349" id="Seg_5155" s="T348">pierce-EP-IMP.2PL-cut-EP-IMP.2PL</ta>
            <ta e="T350" id="Seg_5156" s="T349">pierce-EP-CAUS-CVB.SIM</ta>
            <ta e="T351" id="Seg_5157" s="T350">go.[IMP.2SG]</ta>
            <ta e="T352" id="Seg_5158" s="T351">say-CVB.SIM-say-CVB.SIM</ta>
            <ta e="T353" id="Seg_5159" s="T352">pierce-EP-CAUS-CVB.SIM</ta>
            <ta e="T354" id="Seg_5160" s="T353">go-PRS.[3SG]</ta>
            <ta e="T355" id="Seg_5161" s="T354">that-PL-3SG.[NOM]</ta>
            <ta e="T356" id="Seg_5162" s="T355">earth.[NOM]</ta>
            <ta e="T357" id="Seg_5163" s="T356">to</ta>
            <ta e="T358" id="Seg_5164" s="T357">be.afraid-CVB.SEQ</ta>
            <ta e="T359" id="Seg_5165" s="T358">go-PRS-3PL</ta>
            <ta e="T360" id="Seg_5166" s="T359">old.woman.[NOM]</ta>
            <ta e="T361" id="Seg_5167" s="T360">how.much</ta>
            <ta e="T362" id="Seg_5168" s="T361">ten</ta>
            <ta e="T363" id="Seg_5169" s="T362">as.long.as</ta>
            <ta e="T364" id="Seg_5170" s="T363">live-PST2.[3SG]</ta>
            <ta e="T365" id="Seg_5171" s="T364">reindeer-3SG.[NOM]</ta>
            <ta e="T366" id="Seg_5172" s="T365">whereto</ta>
            <ta e="T367" id="Seg_5173" s="T366">NEG</ta>
            <ta e="T368" id="Seg_5174" s="T367">move-NEG.[3SG]</ta>
            <ta e="T369" id="Seg_5175" s="T368">horse-PL-3SG.[NOM]</ta>
            <ta e="T370" id="Seg_5176" s="T369">this.EMPH.[NOM]</ta>
            <ta e="T371" id="Seg_5177" s="T370">similar</ta>
            <ta e="T372" id="Seg_5178" s="T371">lie-PRS-3PL</ta>
            <ta e="T373" id="Seg_5179" s="T372">fish-3PL.[NOM]</ta>
            <ta e="T374" id="Seg_5180" s="T373">this.EMPH</ta>
            <ta e="T375" id="Seg_5181" s="T374">hunt-EP-REFL-PTCP.PST.[NOM]</ta>
            <ta e="T376" id="Seg_5182" s="T375">like</ta>
            <ta e="T377" id="Seg_5183" s="T376">go-PRS.[3SG]</ta>
            <ta e="T378" id="Seg_5184" s="T377">old.woman.[NOM]</ta>
            <ta e="T379" id="Seg_5185" s="T378">so</ta>
            <ta e="T380" id="Seg_5186" s="T379">be.rich-CVB.SEQ-be.rich-CVB.SEQ</ta>
            <ta e="T381" id="Seg_5187" s="T380">EMPH-alone</ta>
            <ta e="T382" id="Seg_5188" s="T381">live-PRS.[3SG]</ta>
            <ta e="T383" id="Seg_5189" s="T382">so</ta>
            <ta e="T384" id="Seg_5190" s="T383">make-CVB.SEQ</ta>
            <ta e="T385" id="Seg_5191" s="T384">old.woman.[NOM]</ta>
            <ta e="T386" id="Seg_5192" s="T385">thought-3SG.[NOM]</ta>
            <ta e="T387" id="Seg_5193" s="T386">come-PST2.[3SG]</ta>
            <ta e="T388" id="Seg_5194" s="T387">child-PL.[NOM]</ta>
            <ta e="T389" id="Seg_5195" s="T388">1SG.[NOM]</ta>
            <ta e="T390" id="Seg_5196" s="T389">sit-PTCP.FUT-COMP</ta>
            <ta e="T391" id="Seg_5197" s="T390">old.man-EP-1SG.[NOM]</ta>
            <ta e="T392" id="Seg_5198" s="T391">distant-3SG-DAT/LOC</ta>
            <ta e="T393" id="Seg_5199" s="T392">get.to.know-RECP/COLL-CVB.SIM</ta>
            <ta e="T394" id="Seg_5200" s="T393">go-PTCP.FUT</ta>
            <ta e="T395" id="Seg_5201" s="T394">there.is-EP-1SG</ta>
            <ta e="T396" id="Seg_5202" s="T395">three-ORD</ta>
            <ta e="T397" id="Seg_5203" s="T396">year-3SG-DAT/LOC</ta>
            <ta e="T398" id="Seg_5204" s="T397">old.man-3SG-DAT/LOC</ta>
            <ta e="T399" id="Seg_5205" s="T398">go-CVB.PURP</ta>
            <ta e="T400" id="Seg_5206" s="T399">dress-PST1-3SG</ta>
            <ta e="T401" id="Seg_5207" s="T400">bread-ACC</ta>
            <ta e="T402" id="Seg_5208" s="T401">cut-FREQ-CVB.SEQ</ta>
            <ta e="T403" id="Seg_5209" s="T402">after</ta>
            <ta e="T404" id="Seg_5210" s="T403">take-PST1-3SG</ta>
            <ta e="T405" id="Seg_5211" s="T404">that.[NOM]</ta>
            <ta e="T406" id="Seg_5212" s="T405">make-CVB.SEQ</ta>
            <ta e="T407" id="Seg_5213" s="T406">after</ta>
            <ta e="T408" id="Seg_5214" s="T407">wild.reindeer.[NOM]</ta>
            <ta e="T409" id="Seg_5215" s="T408">fat-3SG-ACC</ta>
            <ta e="T410" id="Seg_5216" s="T409">bring-CAP.[3SG]</ta>
            <ta e="T411" id="Seg_5217" s="T410">so</ta>
            <ta e="T412" id="Seg_5218" s="T411">make-CVB.SEQ</ta>
            <ta e="T413" id="Seg_5219" s="T412">after</ta>
            <ta e="T414" id="Seg_5220" s="T413">old.man-3SG-DAT/LOC</ta>
            <ta e="T415" id="Seg_5221" s="T414">go-PST1-3SG</ta>
            <ta e="T416" id="Seg_5222" s="T415">this</ta>
            <ta e="T417" id="Seg_5223" s="T416">go-CVB.SEQ</ta>
            <ta e="T418" id="Seg_5224" s="T417">go-CVB.SEQ</ta>
            <ta e="T419" id="Seg_5225" s="T418">old.man-3SG-GEN</ta>
            <ta e="T420" id="Seg_5226" s="T419">pit_dwelling-3SG-DAT/LOC</ta>
            <ta e="T421" id="Seg_5227" s="T420">reach-PST2.[3SG]</ta>
            <ta e="T422" id="Seg_5228" s="T421">only</ta>
            <ta e="T423" id="Seg_5229" s="T422">what-INTNS.[NOM]</ta>
            <ta e="T424" id="Seg_5230" s="T423">MOD</ta>
            <ta e="T425" id="Seg_5231" s="T424">NEG.EX</ta>
            <ta e="T426" id="Seg_5232" s="T425">intact-SIM</ta>
            <ta e="T427" id="Seg_5233" s="T426">completely</ta>
            <ta e="T428" id="Seg_5234" s="T427">flurry-EP-PASS/REFL-CVB.SEQ</ta>
            <ta e="T429" id="Seg_5235" s="T428">crash-CVB.SEQ</ta>
            <ta e="T430" id="Seg_5236" s="T429">stay-PST2.[3SG]</ta>
            <ta e="T431" id="Seg_5237" s="T430">that.[NOM]</ta>
            <ta e="T432" id="Seg_5238" s="T431">and</ta>
            <ta e="T433" id="Seg_5239" s="T432">be-COND.[3SG]</ta>
            <ta e="T434" id="Seg_5240" s="T433">chimney-3SG-DAT/LOC</ta>
            <ta e="T435" id="Seg_5241" s="T434">climb-EP-PST2.[3SG]</ta>
            <ta e="T436" id="Seg_5242" s="T435">only</ta>
            <ta e="T437" id="Seg_5243" s="T436">stove-3SG-ACC</ta>
            <ta e="T438" id="Seg_5244" s="T437">around</ta>
            <ta e="T439" id="Seg_5245" s="T438">three</ta>
            <ta e="T440" id="Seg_5246" s="T439">animal.hair-PROPR</ta>
            <ta e="T441" id="Seg_5247" s="T440">worm.[NOM]</ta>
            <ta e="T442" id="Seg_5248" s="T441">become-CVB.SEQ</ta>
            <ta e="T443" id="Seg_5249" s="T442">behave.badly-CVB.SIM</ta>
            <ta e="T444" id="Seg_5250" s="T443">lie-PRS.[3SG]</ta>
            <ta e="T445" id="Seg_5251" s="T444">horse-3SG-GEN</ta>
            <ta e="T446" id="Seg_5252" s="T445">black</ta>
            <ta e="T447" id="Seg_5253" s="T446">hoof-3SG-ACC</ta>
            <ta e="T448" id="Seg_5254" s="T447">with</ta>
            <ta e="T449" id="Seg_5255" s="T448">stop-MED-EP-NMNZ-3SG-ACC</ta>
            <ta e="T450" id="Seg_5256" s="T449">just</ta>
            <ta e="T451" id="Seg_5257" s="T450">remain-CAUS-PST2.[3SG]</ta>
            <ta e="T452" id="Seg_5258" s="T451">horse-3SG-GEN</ta>
            <ta e="T453" id="Seg_5259" s="T452">own-3SG-ACC</ta>
            <ta e="T454" id="Seg_5260" s="T453">recently</ta>
            <ta e="T455" id="Seg_5261" s="T454">turn.into-PTCP.PST</ta>
            <ta e="T456" id="Seg_5262" s="T455">leftover-3SG.[NOM]</ta>
            <ta e="T457" id="Seg_5263" s="T456">it.is.said</ta>
            <ta e="T458" id="Seg_5264" s="T457">that.[NOM]</ta>
            <ta e="T459" id="Seg_5265" s="T458">for</ta>
            <ta e="T460" id="Seg_5266" s="T459">die-EP-NEG-CVB.PURP</ta>
            <ta e="T461" id="Seg_5267" s="T460">turn.into-PST2-3SG</ta>
            <ta e="T462" id="Seg_5268" s="T461">it.is.said</ta>
            <ta e="T463" id="Seg_5269" s="T462">well</ta>
            <ta e="T464" id="Seg_5270" s="T463">that-ACC</ta>
            <ta e="T465" id="Seg_5271" s="T464">this</ta>
            <ta e="T466" id="Seg_5272" s="T465">bread-EP-INSTR</ta>
            <ta e="T467" id="Seg_5273" s="T466">throw-PST2.[3SG]</ta>
            <ta e="T468" id="Seg_5274" s="T467">this-ACC</ta>
            <ta e="T469" id="Seg_5275" s="T468">crook-EP-NMNZ.[NOM]</ta>
            <ta e="T470" id="Seg_5276" s="T469">make-PST2.[3SG]</ta>
            <ta e="T471" id="Seg_5277" s="T470">and</ta>
            <ta e="T472" id="Seg_5278" s="T471">speak-PST2.[3SG]</ta>
            <ta e="T473" id="Seg_5279" s="T472">this.[NOM]</ta>
            <ta e="T474" id="Seg_5280" s="T473">stove-EP-1SG.[NOM]</ta>
            <ta e="T475" id="Seg_5281" s="T474">clay-3SG.[NOM]</ta>
            <ta e="T476" id="Seg_5282" s="T475">lie-CAUS-EP-NEG-CVB.PURP</ta>
            <ta e="T477" id="Seg_5283" s="T476">make-PST2.[3SG]</ta>
            <ta e="T478" id="Seg_5284" s="T477">say-PRS.[3SG]</ta>
            <ta e="T479" id="Seg_5285" s="T478">fat-INSTR</ta>
            <ta e="T480" id="Seg_5286" s="T479">throw-PST2.[3SG]</ta>
            <ta e="T481" id="Seg_5287" s="T480">oh</ta>
            <ta e="T482" id="Seg_5288" s="T481">lump-PL.[NOM]</ta>
            <ta e="T483" id="Seg_5289" s="T482">fall-CVB.SEQ-3PL</ta>
            <ta e="T484" id="Seg_5290" s="T483">lie-CAUS-EP-NEG-CVB.PURP</ta>
            <ta e="T485" id="Seg_5291" s="T484">make-PST1-3PL</ta>
            <ta e="T486" id="Seg_5292" s="T485">again</ta>
            <ta e="T487" id="Seg_5293" s="T486">throw-PST2.[3SG]</ta>
            <ta e="T488" id="Seg_5294" s="T487">this-ACC</ta>
            <ta e="T489" id="Seg_5295" s="T488">bite-CVB.SEQ</ta>
            <ta e="T490" id="Seg_5296" s="T489">try-PST2.[3SG]</ta>
            <ta e="T491" id="Seg_5297" s="T490">fat.[NOM]</ta>
            <ta e="T492" id="Seg_5298" s="T491">be-PST2.[3SG]</ta>
            <ta e="T493" id="Seg_5299" s="T492">that-ACC</ta>
            <ta e="T494" id="Seg_5300" s="T493">sky.[NOM]</ta>
            <ta e="T495" id="Seg_5301" s="T494">sky.[NOM]</ta>
            <ta e="T496" id="Seg_5302" s="T495">a.bit-DIM-INTNS</ta>
            <ta e="T497" id="Seg_5303" s="T496">say-PST2.[3SG]</ta>
            <ta e="T498" id="Seg_5304" s="T497">well</ta>
            <ta e="T499" id="Seg_5305" s="T498">old.man.[NOM]</ta>
            <ta e="T500" id="Seg_5306" s="T499">door-2SG-ACC</ta>
            <ta e="T501" id="Seg_5307" s="T500">open.[IMP.2SG]</ta>
            <ta e="T502" id="Seg_5308" s="T501">say-PST2.[3SG]</ta>
            <ta e="T503" id="Seg_5309" s="T502">old.woman-3SG.[NOM]</ta>
            <ta e="T504" id="Seg_5310" s="T503">door-3SG-ACC</ta>
            <ta e="T505" id="Seg_5311" s="T504">open-CVB.SEQ</ta>
            <ta e="T506" id="Seg_5312" s="T505">leftover-VBZ-PST2.[3SG]</ta>
            <ta e="T507" id="Seg_5313" s="T506">old.woman.[NOM]</ta>
            <ta e="T508" id="Seg_5314" s="T507">eat-EP-CAUS-CVB.SEQ</ta>
            <ta e="T509" id="Seg_5315" s="T508">after</ta>
            <ta e="T510" id="Seg_5316" s="T509">house-3SG-DAT/LOC</ta>
            <ta e="T511" id="Seg_5317" s="T510">bring-PST2.[3SG]</ta>
            <ta e="T512" id="Seg_5318" s="T511">this</ta>
            <ta e="T513" id="Seg_5319" s="T512">house-3SG-DAT/LOC</ta>
            <ta e="T514" id="Seg_5320" s="T513">carry-CVB.SEQ</ta>
            <ta e="T515" id="Seg_5321" s="T514">wash-CVB.SEQ-comb-CVB.SEQ</ta>
            <ta e="T516" id="Seg_5322" s="T515">after</ta>
            <ta e="T517" id="Seg_5323" s="T516">dress-EP-NEG.PTCP.PST</ta>
            <ta e="T518" id="Seg_5324" s="T517">clothes-ACC</ta>
            <ta e="T519" id="Seg_5325" s="T518">dress-CAUS-CVB.SEQ</ta>
            <ta e="T520" id="Seg_5326" s="T519">after</ta>
            <ta e="T521" id="Seg_5327" s="T520">glass.[NOM]</ta>
            <ta e="T522" id="Seg_5328" s="T521">bed-DAT/LOC</ta>
            <ta e="T523" id="Seg_5329" s="T522">seat-CVB.SEQ</ta>
            <ta e="T524" id="Seg_5330" s="T523">throw-PST2.[3SG]</ta>
            <ta e="T525" id="Seg_5331" s="T524">that</ta>
            <ta e="T526" id="Seg_5332" s="T525">make-CVB.SEQ</ta>
            <ta e="T527" id="Seg_5333" s="T526">after</ta>
            <ta e="T528" id="Seg_5334" s="T527">tea.[NOM]</ta>
            <ta e="T529" id="Seg_5335" s="T528">drink-EP-CAUS.[CAUS]-PST2.[3SG]</ta>
            <ta e="T530" id="Seg_5336" s="T529">then</ta>
            <ta e="T531" id="Seg_5337" s="T530">meat.[NOM]</ta>
            <ta e="T532" id="Seg_5338" s="T531">meal.[NOM]</ta>
            <ta e="T533" id="Seg_5339" s="T532">take.out-PST2.[3SG]</ta>
            <ta e="T534" id="Seg_5340" s="T533">fat-PROPR</ta>
            <ta e="T535" id="Seg_5341" s="T534">soup-ACC</ta>
            <ta e="T536" id="Seg_5342" s="T535">bowl-DAT/LOC</ta>
            <ta e="T537" id="Seg_5343" s="T536">take.out-PST2.[3SG]</ta>
            <ta e="T538" id="Seg_5344" s="T537">so</ta>
            <ta e="T539" id="Seg_5345" s="T538">make-CVB.SEQ</ta>
            <ta e="T540" id="Seg_5346" s="T539">gold.[NOM]</ta>
            <ta e="T541" id="Seg_5347" s="T540">spoon.[NOM]</ta>
            <ta e="T542" id="Seg_5348" s="T541">fork.[NOM]</ta>
            <ta e="T543" id="Seg_5349" s="T542">lay-PST2.[3SG]</ta>
            <ta e="T544" id="Seg_5350" s="T543">only</ta>
            <ta e="T545" id="Seg_5351" s="T544">spoon-3SG-INSTR</ta>
            <ta e="T546" id="Seg_5352" s="T545">drink-CVB.PURP</ta>
            <ta e="T547" id="Seg_5353" s="T546">want-PST2.[3SG]</ta>
            <ta e="T548" id="Seg_5354" s="T547">well</ta>
            <ta e="T549" id="Seg_5355" s="T548">this-this</ta>
            <ta e="T550" id="Seg_5356" s="T549">spoon-1SG.[NOM]</ta>
            <ta e="T551" id="Seg_5357" s="T550">small.[NOM]</ta>
            <ta e="T552" id="Seg_5358" s="T551">big</ta>
            <ta e="T553" id="Seg_5359" s="T552">ladle-PART</ta>
            <ta e="T554" id="Seg_5360" s="T553">Q</ta>
            <ta e="T555" id="Seg_5361" s="T554">scoop-PART</ta>
            <ta e="T556" id="Seg_5362" s="T555">Q</ta>
            <ta e="T557" id="Seg_5363" s="T556">bring.[IMP.2SG]</ta>
            <ta e="T558" id="Seg_5364" s="T557">say-PST2.[3SG]</ta>
            <ta e="T559" id="Seg_5365" s="T558">recent-3SG.[NOM]</ta>
            <ta e="T560" id="Seg_5366" s="T559">scoop.[NOM]</ta>
            <ta e="T561" id="Seg_5367" s="T560">very-3SG</ta>
            <ta e="T562" id="Seg_5368" s="T561">hand-CVB.SEQ</ta>
            <ta e="T563" id="Seg_5369" s="T562">throw-PST2.[3SG]</ta>
            <ta e="T564" id="Seg_5370" s="T563">fat-PROPR</ta>
            <ta e="T565" id="Seg_5371" s="T564">soup-ACC</ta>
            <ta e="T566" id="Seg_5372" s="T565">this-3SG-INSTR</ta>
            <ta e="T567" id="Seg_5373" s="T566">hup</ta>
            <ta e="T568" id="Seg_5374" s="T567">make-PST2.[3SG]</ta>
            <ta e="T569" id="Seg_5375" s="T568">this-DAT/LOC</ta>
            <ta e="T570" id="Seg_5376" s="T569">well</ta>
            <ta e="T571" id="Seg_5377" s="T570">choke-PRS.[3SG]</ta>
            <ta e="T572" id="Seg_5378" s="T571">EMPH</ta>
            <ta e="T573" id="Seg_5379" s="T572">well</ta>
            <ta e="T574" id="Seg_5380" s="T573">then</ta>
            <ta e="T575" id="Seg_5381" s="T574">choke-PRS-1SG</ta>
            <ta e="T576" id="Seg_5382" s="T575">say-CVB.SEQ</ta>
            <ta e="T577" id="Seg_5383" s="T576">well</ta>
            <ta e="T578" id="Seg_5384" s="T577">loud.noise-VBZ-PRS.[3SG]</ta>
            <ta e="T579" id="Seg_5385" s="T578">EMPH</ta>
            <ta e="T580" id="Seg_5386" s="T579">glass.[NOM]</ta>
            <ta e="T581" id="Seg_5387" s="T580">padding-3SG-ACC</ta>
            <ta e="T582" id="Seg_5388" s="T581">through</ta>
            <ta e="T583" id="Seg_5389" s="T582">loud.noise-VBZ-PRS.[3SG]</ta>
            <ta e="T584" id="Seg_5390" s="T583">and</ta>
            <ta e="T585" id="Seg_5391" s="T584">so</ta>
            <ta e="T586" id="Seg_5392" s="T585">three</ta>
            <ta e="T587" id="Seg_5393" s="T586">icesheet.[NOM]</ta>
            <ta e="T588" id="Seg_5394" s="T587">earth-ACC</ta>
            <ta e="T589" id="Seg_5395" s="T588">completely</ta>
            <ta e="T590" id="Seg_5396" s="T589">fall-CVB.SEQ</ta>
            <ta e="T591" id="Seg_5397" s="T590">stay-PRS.[3SG]</ta>
            <ta e="T592" id="Seg_5398" s="T591">old.woman-3SG.[NOM]</ta>
            <ta e="T593" id="Seg_5399" s="T592">what.[NOM]</ta>
            <ta e="T594" id="Seg_5400" s="T593">be-FUT-3SG=Q</ta>
            <ta e="T595" id="Seg_5401" s="T594">how</ta>
            <ta e="T596" id="Seg_5402" s="T595">live-PST2-3SG</ta>
            <ta e="T597" id="Seg_5403" s="T596">and</ta>
            <ta e="T598" id="Seg_5404" s="T597">like.that</ta>
            <ta e="T601" id="Seg_5405" s="T598">always</ta>
            <ta e="T599" id="Seg_5406" s="T601">always</ta>
            <ta e="T600" id="Seg_5407" s="T599">live-PRS.[3SG]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_5408" s="T0">alter.Mann-PROPR</ta>
            <ta e="T2" id="Seg_5409" s="T1">Alte.[NOM]</ta>
            <ta e="T3" id="Seg_5410" s="T2">leben-PST2-3PL</ta>
            <ta e="T4" id="Seg_5411" s="T3">man.sagt</ta>
            <ta e="T5" id="Seg_5412" s="T4">gebären-EP-CAUS-PTCP.PRS</ta>
            <ta e="T6" id="Seg_5413" s="T5">Kind-POSS</ta>
            <ta e="T7" id="Seg_5414" s="T6">NEG-3PL</ta>
            <ta e="T8" id="Seg_5415" s="T7">Zelt-DAT/LOC</ta>
            <ta e="T9" id="Seg_5416" s="T8">sitzen-TEMP-3PL</ta>
            <ta e="T10" id="Seg_5417" s="T9">Feuer-3PL.[NOM]</ta>
            <ta e="T11" id="Seg_5418" s="T10">nur</ta>
            <ta e="T12" id="Seg_5419" s="T11">rot.werden-PRS.[3SG]</ta>
            <ta e="T13" id="Seg_5420" s="T12">so</ta>
            <ta e="T14" id="Seg_5421" s="T13">arm-PL.[NOM]</ta>
            <ta e="T15" id="Seg_5422" s="T14">nach.draußen</ta>
            <ta e="T16" id="Seg_5423" s="T15">hinausgehen-TEMP-3PL</ta>
            <ta e="T17" id="Seg_5424" s="T16">Schnee-3PL.[NOM]</ta>
            <ta e="T18" id="Seg_5425" s="T17">nur</ta>
            <ta e="T19" id="Seg_5426" s="T18">weiß.sein-PRS.[3SG]</ta>
            <ta e="T20" id="Seg_5427" s="T19">was-POSS</ta>
            <ta e="T21" id="Seg_5428" s="T20">NEG</ta>
            <ta e="T22" id="Seg_5429" s="T21">NEG-3PL</ta>
            <ta e="T23" id="Seg_5430" s="T22">nur</ta>
            <ta e="T24" id="Seg_5431" s="T23">Mittel-3PL.[NOM]</ta>
            <ta e="T25" id="Seg_5432" s="T24">sagen-CVB.SEQ-3PL</ta>
            <ta e="T26" id="Seg_5433" s="T25">EMPH-einsam</ta>
            <ta e="T27" id="Seg_5434" s="T26">Pferd-PROPR-3PL</ta>
            <ta e="T28" id="Seg_5435" s="T27">dann</ta>
            <ta e="T29" id="Seg_5436" s="T28">jagen-PTCP.PRS</ta>
            <ta e="T30" id="Seg_5437" s="T29">Ausrüstung-3PL.[NOM]</ta>
            <ta e="T31" id="Seg_5438" s="T30">drei</ta>
            <ta e="T32" id="Seg_5439" s="T31">Stein-PROPR</ta>
            <ta e="T33" id="Seg_5440" s="T32">Netz-DIM-PROPR-3PL</ta>
            <ta e="T34" id="Seg_5441" s="T33">jenes-3SG-3PL-DAT/LOC</ta>
            <ta e="T35" id="Seg_5442" s="T34">Wasser.[NOM]</ta>
            <ta e="T36" id="Seg_5443" s="T35">kleines.Getier-3SG.[NOM]</ta>
            <ta e="T37" id="Seg_5444" s="T36">festmachen-EP-REFL-TEMP-3SG</ta>
            <ta e="T38" id="Seg_5445" s="T37">jenes-ACC</ta>
            <ta e="T39" id="Seg_5446" s="T38">essen-PRS-3PL</ta>
            <ta e="T40" id="Seg_5447" s="T39">Bratspieß-DAT/LOC</ta>
            <ta e="T41" id="Seg_5448" s="T40">braten-CVB.SEQ</ta>
            <ta e="T42" id="Seg_5449" s="T41">nur</ta>
            <ta e="T43" id="Seg_5450" s="T42">sterben-CVB.PURP</ta>
            <ta e="T44" id="Seg_5451" s="T43">latschen-PST2-3PL</ta>
            <ta e="T45" id="Seg_5452" s="T44">zu.Ende.gehen-CVB.PURP</ta>
            <ta e="T46" id="Seg_5453" s="T45">den.Bauch.einziehen-PST2-3PL</ta>
            <ta e="T47" id="Seg_5454" s="T46">dann</ta>
            <ta e="T48" id="Seg_5455" s="T47">alter.Mann-3SG.[NOM]</ta>
            <ta e="T49" id="Seg_5456" s="T48">plötzlich</ta>
            <ta e="T50" id="Seg_5457" s="T49">hineingehen-PST2.[3SG]</ta>
            <ta e="T51" id="Seg_5458" s="T50">Alte-3SG-DAT/LOC</ta>
            <ta e="T52" id="Seg_5459" s="T51">doch</ta>
            <ta e="T53" id="Seg_5460" s="T52">hineingehen-CVB.SEQ</ta>
            <ta e="T54" id="Seg_5461" s="T53">sagen-PST2.[3SG]</ta>
            <ta e="T55" id="Seg_5462" s="T54">na</ta>
            <ta e="T56" id="Seg_5463" s="T55">Alte.[NOM]</ta>
            <ta e="T57" id="Seg_5464" s="T56">aufstehen-PRS-1PL</ta>
            <ta e="T58" id="Seg_5465" s="T57">go-CAUS-FUT-1PL</ta>
            <ta e="T59" id="Seg_5466" s="T58">einsam</ta>
            <ta e="T60" id="Seg_5467" s="T59">Pferd-PROPR-1PL</ta>
            <ta e="T61" id="Seg_5468" s="T60">essen-PTCP.FUT-1PL.[NOM]</ta>
            <ta e="T62" id="Seg_5469" s="T61">es.gibt-3SG</ta>
            <ta e="T63" id="Seg_5470" s="T62">sogar</ta>
            <ta e="T64" id="Seg_5471" s="T63">hungern-CVB.SEQ</ta>
            <ta e="T65" id="Seg_5472" s="T64">sterben-TEMP-1PL</ta>
            <ta e="T66" id="Seg_5473" s="T65">Hinterteil-1PL-DAT/LOC</ta>
            <ta e="T67" id="Seg_5474" s="T66">bleiben-FUT-3SG</ta>
            <ta e="T68" id="Seg_5475" s="T67">jenes-DAT/LOC</ta>
            <ta e="T69" id="Seg_5476" s="T68">Alte.[NOM]</ta>
            <ta e="T70" id="Seg_5477" s="T69">selbst-2SG.[NOM]</ta>
            <ta e="T71" id="Seg_5478" s="T70">Gedanke-2SG.[NOM]</ta>
            <ta e="T72" id="Seg_5479" s="T71">sagen-PST2.[3SG]</ta>
            <ta e="T73" id="Seg_5480" s="T72">töten-COND-2SG</ta>
            <ta e="T74" id="Seg_5481" s="T73">nur</ta>
            <ta e="T75" id="Seg_5482" s="T74">essen-FUT-1PL</ta>
            <ta e="T76" id="Seg_5483" s="T75">Mann-3SG.[NOM]</ta>
            <ta e="T77" id="Seg_5484" s="T76">hinausgehen-PST2.[3SG]</ta>
            <ta e="T78" id="Seg_5485" s="T77">und</ta>
            <ta e="T79" id="Seg_5486" s="T78">töten-PST2.[3SG]</ta>
            <ta e="T80" id="Seg_5487" s="T79">alter.Mann-3SG.[NOM]</ta>
            <ta e="T81" id="Seg_5488" s="T80">Haut.abziehen-PST2.[3SG]</ta>
            <ta e="T82" id="Seg_5489" s="T81">nur</ta>
            <ta e="T83" id="Seg_5490" s="T82">kalt.[NOM]</ta>
            <ta e="T84" id="Seg_5491" s="T83">sehr</ta>
            <ta e="T85" id="Seg_5492" s="T84">frostig.[NOM]</ta>
            <ta e="T86" id="Seg_5493" s="T85">sehr</ta>
            <ta e="T87" id="Seg_5494" s="T86">alter.Mann-3SG.[NOM]</ta>
            <ta e="T88" id="Seg_5495" s="T87">hineingehen-PST2.[3SG]</ta>
            <ta e="T89" id="Seg_5496" s="T88">hineingehen-CVB.SEQ</ta>
            <ta e="T90" id="Seg_5497" s="T89">sagen-PST2.[3SG]</ta>
            <ta e="T91" id="Seg_5498" s="T90">Pferd-1PL.[NOM]</ta>
            <ta e="T92" id="Seg_5499" s="T91">Fleisch-3SG-ACC</ta>
            <ta e="T93" id="Seg_5500" s="T92">ganz-SIM</ta>
            <ta e="T94" id="Seg_5501" s="T93">viereckiges.Stangenzelt-DIM-1PL.[NOM]</ta>
            <ta e="T95" id="Seg_5502" s="T94">ans.andere.Ufer</ta>
            <ta e="T96" id="Seg_5503" s="T95">ziehen-VBZ-CVB.SEQ</ta>
            <ta e="T97" id="Seg_5504" s="T96">werfen.[IMP.2SG]</ta>
            <ta e="T98" id="Seg_5505" s="T97">ganz-3SG-ACC</ta>
            <ta e="T99" id="Seg_5506" s="T98">Alte-3SG.[NOM]</ta>
            <ta e="T100" id="Seg_5507" s="T99">ganz-3SG-ACC</ta>
            <ta e="T101" id="Seg_5508" s="T100">ziehen-VBZ-PST2.[3SG]</ta>
            <ta e="T102" id="Seg_5509" s="T101">Alte-3SG.[NOM]</ta>
            <ta e="T103" id="Seg_5510" s="T102">Hand-3SG-ACC</ta>
            <ta e="T104" id="Seg_5511" s="T103">warm.werden-CAUS-CVB.PURP</ta>
            <ta e="T105" id="Seg_5512" s="T104">wollen-PTCP.PST-3SG-ACC</ta>
            <ta e="T106" id="Seg_5513" s="T105">alter.Mann.[NOM]</ta>
            <ta e="T107" id="Seg_5514" s="T106">sprechen-PST2.[3SG]</ta>
            <ta e="T108" id="Seg_5515" s="T107">essen-PTCP.FUT-2SG-ACC</ta>
            <ta e="T109" id="Seg_5516" s="T108">jeder</ta>
            <ta e="T110" id="Seg_5517" s="T109">Gefäß-2SG-DAT/LOC</ta>
            <ta e="T111" id="Seg_5518" s="T110">Eimer-2SG-DAT/LOC</ta>
            <ta e="T112" id="Seg_5519" s="T111">Wasser-PART</ta>
            <ta e="T113" id="Seg_5520" s="T112">schöpfen-CVB.SEQ</ta>
            <ta e="T114" id="Seg_5521" s="T113">füllen.[IMP.2SG]</ta>
            <ta e="T115" id="Seg_5522" s="T114">fertig-VBZ-CVB.SEQ</ta>
            <ta e="T116" id="Seg_5523" s="T115">nachdem</ta>
            <ta e="T117" id="Seg_5524" s="T116">doch</ta>
            <ta e="T118" id="Seg_5525" s="T117">essen-FUT.[IMP.2SG]</ta>
            <ta e="T119" id="Seg_5526" s="T118">so</ta>
            <ta e="T120" id="Seg_5527" s="T119">machen-CVB.SEQ</ta>
            <ta e="T121" id="Seg_5528" s="T120">Alte.[NOM]</ta>
            <ta e="T122" id="Seg_5529" s="T121">hinausgehen-CVB.SEQ</ta>
            <ta e="T123" id="Seg_5530" s="T122">jeder</ta>
            <ta e="T124" id="Seg_5531" s="T123">Gefäß-3SG-DAT/LOC</ta>
            <ta e="T125" id="Seg_5532" s="T124">ganz-DIM-3PL-DAT/LOC</ta>
            <ta e="T126" id="Seg_5533" s="T125">Wasser.[NOM]</ta>
            <ta e="T127" id="Seg_5534" s="T126">schöpfen-PST2.[3SG]</ta>
            <ta e="T128" id="Seg_5535" s="T127">nur</ta>
            <ta e="T129" id="Seg_5536" s="T128">letzter</ta>
            <ta e="T130" id="Seg_5537" s="T129">Kessel-DAT/LOC-DIM-DIM-DIM-3SG-ACC</ta>
            <ta e="T131" id="Seg_5538" s="T130">bringen-CVB.PURP</ta>
            <ta e="T132" id="Seg_5539" s="T131">wollen-PST2-3SG</ta>
            <ta e="T133" id="Seg_5540" s="T132">Tür-3SG.[NOM]</ta>
            <ta e="T134" id="Seg_5541" s="T133">verschlossen.[NOM]</ta>
            <ta e="T135" id="Seg_5542" s="T134">na</ta>
            <ta e="T136" id="Seg_5543" s="T135">alter.Mann.[NOM]</ta>
            <ta e="T137" id="Seg_5544" s="T136">Tür-2SG-ACC</ta>
            <ta e="T138" id="Seg_5545" s="T137">stoßen.[IMP.2SG]</ta>
            <ta e="T139" id="Seg_5546" s="T138">frieren-CVB.PURP</ta>
            <ta e="T140" id="Seg_5547" s="T139">machen-PST1-1SG</ta>
            <ta e="T141" id="Seg_5548" s="T140">sagen-PST2.[3SG]</ta>
            <ta e="T142" id="Seg_5549" s="T141">Schlampe.[NOM]</ta>
            <ta e="T143" id="Seg_5550" s="T142">Tochter-3SG.[NOM]</ta>
            <ta e="T144" id="Seg_5551" s="T143">Schnee-DAT/LOC</ta>
            <ta e="T145" id="Seg_5552" s="T144">entzwei</ta>
            <ta e="T146" id="Seg_5553" s="T145">werfen.[IMP.2SG]</ta>
            <ta e="T147" id="Seg_5554" s="T146">sagen-PST2.[3SG]</ta>
            <ta e="T148" id="Seg_5555" s="T147">Alte-3SG.[NOM]</ta>
            <ta e="T149" id="Seg_5556" s="T148">dieses</ta>
            <ta e="T150" id="Seg_5557" s="T149">Kessel-DIM-3SG-ACC</ta>
            <ta e="T151" id="Seg_5558" s="T150">werfen-CVB.SEQ</ta>
            <ta e="T152" id="Seg_5559" s="T151">nachdem</ta>
            <ta e="T153" id="Seg_5560" s="T152">doch</ta>
            <ta e="T154" id="Seg_5561" s="T153">frieren-CVB.SEQ</ta>
            <ta e="T155" id="Seg_5562" s="T154">sterben-CVB.PURP</ta>
            <ta e="T156" id="Seg_5563" s="T155">machen-PRS.[3SG]</ta>
            <ta e="T157" id="Seg_5564" s="T156">Alte-3SG.[NOM]</ta>
            <ta e="T158" id="Seg_5565" s="T157">Jurte-3SG-GEN</ta>
            <ta e="T159" id="Seg_5566" s="T158">oberer.Teil-3SG-DAT/LOC</ta>
            <ta e="T160" id="Seg_5567" s="T159">hinausgehen-CVB.SEQ</ta>
            <ta e="T161" id="Seg_5568" s="T160">Funke-DIM-PL.[NOM]</ta>
            <ta e="T162" id="Seg_5569" s="T161">hinausgehen-NMNZ-3PL-DAT/LOC</ta>
            <ta e="T163" id="Seg_5570" s="T162">dort</ta>
            <ta e="T164" id="Seg_5571" s="T163">Hand-3SG-ACC</ta>
            <ta e="T165" id="Seg_5572" s="T164">warm.werden-CAUS-PRS.[3SG]</ta>
            <ta e="T166" id="Seg_5573" s="T165">nur</ta>
            <ta e="T167" id="Seg_5574" s="T166">Ehemann-3SG-ACC</ta>
            <ta e="T168" id="Seg_5575" s="T167">sehen-PST2-3SG</ta>
            <ta e="T169" id="Seg_5576" s="T168">Pferd-3SG-GEN</ta>
            <ta e="T170" id="Seg_5577" s="T169">Perineum-3SG-ACC</ta>
            <ta e="T171" id="Seg_5578" s="T170">nehmen-CVB.SEQ</ta>
            <ta e="T172" id="Seg_5579" s="T171">nachdem</ta>
            <ta e="T173" id="Seg_5580" s="T172">platzieren-CVB.SEQ</ta>
            <ta e="T174" id="Seg_5581" s="T173">nachdem</ta>
            <ta e="T175" id="Seg_5582" s="T174">Darmfett-VBZ-CVB.SIM</ta>
            <ta e="T176" id="Seg_5583" s="T175">sitzen-PRS.[3SG]</ta>
            <ta e="T177" id="Seg_5584" s="T176">jenes</ta>
            <ta e="T178" id="Seg_5585" s="T177">warm.werden-CAUS-CVB.PURP</ta>
            <ta e="T179" id="Seg_5586" s="T178">machen-PTCP.PRS</ta>
            <ta e="T180" id="Seg_5587" s="T179">Alte-3SG-GEN</ta>
            <ta e="T181" id="Seg_5588" s="T180">Hand-3SG-ACC</ta>
            <ta e="T182" id="Seg_5589" s="T181">spitze.Stange-INSTR</ta>
            <ta e="T183" id="Seg_5590" s="T182">schneiden-CVB.SIM</ta>
            <ta e="T184" id="Seg_5591" s="T183">hacken-PRS.[3SG]</ta>
            <ta e="T185" id="Seg_5592" s="T184">jenes-3SG-3SG.[NOM]</ta>
            <ta e="T186" id="Seg_5593" s="T185">weinen-CVB.SIM-weinen-CVB.SIM</ta>
            <ta e="T187" id="Seg_5594" s="T186">Erde-DAT/LOC</ta>
            <ta e="T188" id="Seg_5595" s="T187">hinuntergehen-EMOT-CVB.SEQ</ta>
            <ta e="T189" id="Seg_5596" s="T188">nachdem</ta>
            <ta e="T190" id="Seg_5597" s="T189">Holz-VBZ-PTCP.PRS</ta>
            <ta e="T191" id="Seg_5598" s="T190">Ort-DIM.[NOM]</ta>
            <ta e="T192" id="Seg_5599" s="T191">zu</ta>
            <ta e="T193" id="Seg_5600" s="T192">weinen-CVB.SIM-weinen-CVB.SIM</ta>
            <ta e="T194" id="Seg_5601" s="T193">schreiten-CVB.SIM</ta>
            <ta e="T195" id="Seg_5602" s="T194">stehen-PRS.[3SG]</ta>
            <ta e="T196" id="Seg_5603" s="T195">doch</ta>
            <ta e="T197" id="Seg_5604" s="T196">dort</ta>
            <ta e="T198" id="Seg_5605" s="T197">Lärche-EP-DIM.[NOM]</ta>
            <ta e="T199" id="Seg_5606" s="T198">Baum.[NOM]</ta>
            <ta e="T200" id="Seg_5607" s="T199">Wurzel-3SG-DAT/LOC</ta>
            <ta e="T201" id="Seg_5608" s="T200">sich.hinlegen-CVB.PURP</ta>
            <ta e="T202" id="Seg_5609" s="T201">wollen-PST2.[3SG]</ta>
            <ta e="T203" id="Seg_5610" s="T202">sich.hinlegen-CVB.PURP</ta>
            <ta e="T204" id="Seg_5611" s="T203">wollen-PST2.[3SG]</ta>
            <ta e="T205" id="Seg_5612" s="T204">Kälte.[NOM]</ta>
            <ta e="T206" id="Seg_5613" s="T205">zittern-PTCP.PRS.[NOM]</ta>
            <ta e="T207" id="Seg_5614" s="T206">Herz-3SG-DAT/LOC</ta>
            <ta e="T208" id="Seg_5615" s="T207">hineingehen-PRS.[3SG]</ta>
            <ta e="T209" id="Seg_5616" s="T208">jenes</ta>
            <ta e="T210" id="Seg_5617" s="T209">liegen-TEMP-3SG</ta>
            <ta e="T211" id="Seg_5618" s="T210">Lärm.[NOM]</ta>
            <ta e="T212" id="Seg_5619" s="T211">EMPH</ta>
            <ta e="T213" id="Seg_5620" s="T212">gehört.werden-EP-PST2.[3SG]</ta>
            <ta e="T214" id="Seg_5621" s="T213">Rentier.[NOM]</ta>
            <ta e="T215" id="Seg_5622" s="T214">jagen-PTCP.PRS-ABL</ta>
            <ta e="T216" id="Seg_5623" s="T215">Rentier.[NOM]</ta>
            <ta e="T217" id="Seg_5624" s="T216">jagen-PTCP.PRS.[NOM]</ta>
            <ta e="T218" id="Seg_5625" s="T217">unterschiedlich</ta>
            <ta e="T219" id="Seg_5626" s="T218">sehr</ta>
            <ta e="T220" id="Seg_5627" s="T219">jenes.[NOM]</ta>
            <ta e="T221" id="Seg_5628" s="T220">in.die.Richtung</ta>
            <ta e="T222" id="Seg_5629" s="T221">doch</ta>
            <ta e="T223" id="Seg_5630" s="T222">gehen-PRS.[3SG]</ta>
            <ta e="T224" id="Seg_5631" s="T223">dieses</ta>
            <ta e="T225" id="Seg_5632" s="T224">Alte.[NOM]</ta>
            <ta e="T226" id="Seg_5633" s="T225">jenes</ta>
            <ta e="T227" id="Seg_5634" s="T226">gehen-CVB.SEQ</ta>
            <ta e="T228" id="Seg_5635" s="T227">guter.Geist.[NOM]</ta>
            <ta e="T229" id="Seg_5636" s="T228">EMPH</ta>
            <ta e="T230" id="Seg_5637" s="T229">Mond-ACC</ta>
            <ta e="T231" id="Seg_5638" s="T230">bedecken-PTCP.PRS</ta>
            <ta e="T232" id="Seg_5639" s="T231">dicht</ta>
            <ta e="T233" id="Seg_5640" s="T232">Nebel.[NOM]</ta>
            <ta e="T234" id="Seg_5641" s="T233">Rentier-DAT/LOC</ta>
            <ta e="T235" id="Seg_5642" s="T234">Sonne-ACC</ta>
            <ta e="T236" id="Seg_5643" s="T235">bedecken-PTCP.PRS</ta>
            <ta e="T237" id="Seg_5644" s="T236">leichter.Nebel.[NOM]</ta>
            <ta e="T238" id="Seg_5645" s="T237">Nebel.[NOM]</ta>
            <ta e="T239" id="Seg_5646" s="T238">Vieh-DAT/LOC</ta>
            <ta e="T240" id="Seg_5647" s="T239">ankommen-PRS.[3SG]</ta>
            <ta e="T241" id="Seg_5648" s="T240">dieses</ta>
            <ta e="T242" id="Seg_5649" s="T241">Alte-EP-2SG.[NOM]</ta>
            <ta e="T243" id="Seg_5650" s="T242">Haus-ACC</ta>
            <ta e="T244" id="Seg_5651" s="T243">suchen-CVB.SEQ</ta>
            <ta e="T245" id="Seg_5652" s="T244">dieses</ta>
            <ta e="T246" id="Seg_5653" s="T245">Rentier-ACC</ta>
            <ta e="T247" id="Seg_5654" s="T246">zerschneiden-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T248" id="Seg_5655" s="T247">zwei</ta>
            <ta e="T249" id="Seg_5656" s="T248">Hälfte-3SG.[NOM]</ta>
            <ta e="T250" id="Seg_5657" s="T249">zerreiben-CVB.SEQ</ta>
            <ta e="T251" id="Seg_5658" s="T250">bleiben-PST2.[3SG]</ta>
            <ta e="T252" id="Seg_5659" s="T251">plötzlich</ta>
            <ta e="T253" id="Seg_5660" s="T252">Tal-ACC</ta>
            <ta e="T254" id="Seg_5661" s="T253">EMPH-voll</ta>
            <ta e="T255" id="Seg_5662" s="T254">Stangenzelt.[NOM]</ta>
            <ta e="T256" id="Seg_5663" s="T255">Haus.[NOM]</ta>
            <ta e="T257" id="Seg_5664" s="T256">bauen-EP-PASS/REFL-CVB.SEQ</ta>
            <ta e="T258" id="Seg_5665" s="T257">stehen-PRS.[3SG]</ta>
            <ta e="T259" id="Seg_5666" s="T258">groß</ta>
            <ta e="T260" id="Seg_5667" s="T259">sehr</ta>
            <ta e="T261" id="Seg_5668" s="T260">Zelt.[NOM]</ta>
            <ta e="T262" id="Seg_5669" s="T261">kommen-PST1-3SG</ta>
            <ta e="T263" id="Seg_5670" s="T262">dieses</ta>
            <ta e="T264" id="Seg_5671" s="T263">Zelt-DAT/LOC</ta>
            <ta e="T265" id="Seg_5672" s="T264">hier</ta>
            <ta e="T266" id="Seg_5673" s="T265">doch</ta>
            <ta e="T267" id="Seg_5674" s="T266">wie.viel</ta>
            <ta e="T268" id="Seg_5675" s="T267">zehn</ta>
            <ta e="T269" id="Seg_5676" s="T268">MOD</ta>
            <ta e="T270" id="Seg_5677" s="T269">schneiden-CVB.SIM</ta>
            <ta e="T271" id="Seg_5678" s="T270">Karawane-3SG.[NOM]</ta>
            <ta e="T272" id="Seg_5679" s="T271">Balok-3SG.[NOM]</ta>
            <ta e="T273" id="Seg_5680" s="T272">sich.ausstrecken-CVB.SEQ</ta>
            <ta e="T274" id="Seg_5681" s="T273">stehen-PRS.[3SG]</ta>
            <ta e="T275" id="Seg_5682" s="T274">so</ta>
            <ta e="T276" id="Seg_5683" s="T275">machen-CVB.SEQ</ta>
            <ta e="T277" id="Seg_5684" s="T276">dieses</ta>
            <ta e="T278" id="Seg_5685" s="T277">Alte.[NOM]</ta>
            <ta e="T279" id="Seg_5686" s="T278">dieses</ta>
            <ta e="T280" id="Seg_5687" s="T279">Zelt-ACC</ta>
            <ta e="T281" id="Seg_5688" s="T280">öffnen-CVB.SIM</ta>
            <ta e="T282" id="Seg_5689" s="T281">schlagen-EP-PST2.[3SG]</ta>
            <ta e="T283" id="Seg_5690" s="T282">betrachten-CVB.SEQ</ta>
            <ta e="T284" id="Seg_5691" s="T283">sehen-PST2-3SG</ta>
            <ta e="T285" id="Seg_5692" s="T284">EMPH-leer.[NOM]</ta>
            <ta e="T286" id="Seg_5693" s="T285">Mensch-3SG.[NOM]</ta>
            <ta e="T287" id="Seg_5694" s="T286">NEG.EX</ta>
            <ta e="T288" id="Seg_5695" s="T287">jeder</ta>
            <ta e="T289" id="Seg_5696" s="T288">Kessel.[NOM]</ta>
            <ta e="T290" id="Seg_5697" s="T289">Kessel-VBZ-CVB.SEQ</ta>
            <ta e="T291" id="Seg_5698" s="T290">stehen-PRS.[3SG]</ta>
            <ta e="T292" id="Seg_5699" s="T291">jeder</ta>
            <ta e="T293" id="Seg_5700" s="T292">Nahrung.[NOM]</ta>
            <ta e="T294" id="Seg_5701" s="T293">Nahrung-VBZ-CVB.SEQ</ta>
            <ta e="T295" id="Seg_5702" s="T294">stehen-PTCP.PRS.[3SG]</ta>
            <ta e="T296" id="Seg_5703" s="T295">Stein.[NOM]</ta>
            <ta e="T297" id="Seg_5704" s="T296">Bett-PL.[NOM]</ta>
            <ta e="T298" id="Seg_5705" s="T297">ganz-SIM</ta>
            <ta e="T299" id="Seg_5706" s="T298">solch</ta>
            <ta e="T300" id="Seg_5707" s="T299">Ausrüstung-PROPR</ta>
            <ta e="T301" id="Seg_5708" s="T300">Zelt.[NOM]</ta>
            <ta e="T302" id="Seg_5709" s="T301">stehen-PRS.[3SG]</ta>
            <ta e="T303" id="Seg_5710" s="T302">jenes</ta>
            <ta e="T304" id="Seg_5711" s="T303">Alte.[NOM]</ta>
            <ta e="T305" id="Seg_5712" s="T304">hineingehen-CVB.SEQ</ta>
            <ta e="T306" id="Seg_5713" s="T305">essen-NEG.PTCP.PST-3SG-ACC</ta>
            <ta e="T307" id="Seg_5714" s="T306">essen-PRS.[3SG]</ta>
            <ta e="T308" id="Seg_5715" s="T307">essen-NEG.PTCP.PST-3SG-ACC</ta>
            <ta e="T309" id="Seg_5716" s="T308">essen-PRS.[3SG]</ta>
            <ta e="T310" id="Seg_5717" s="T309">russisch</ta>
            <ta e="T311" id="Seg_5718" s="T310">und</ta>
            <ta e="T312" id="Seg_5719" s="T311">Nahrung-3SG.[NOM]</ta>
            <ta e="T313" id="Seg_5720" s="T312">viel</ta>
            <ta e="T314" id="Seg_5721" s="T313">dolganisch</ta>
            <ta e="T315" id="Seg_5722" s="T314">und</ta>
            <ta e="T316" id="Seg_5723" s="T315">Nahrung-3SG.[NOM]</ta>
            <ta e="T317" id="Seg_5724" s="T316">viel</ta>
            <ta e="T318" id="Seg_5725" s="T317">dieses</ta>
            <ta e="T319" id="Seg_5726" s="T318">Alte.[NOM]</ta>
            <ta e="T320" id="Seg_5727" s="T319">sich.waschen-CVB.SEQ-sich.kämmen-CVB.SEQ</ta>
            <ta e="T321" id="Seg_5728" s="T320">nachdem</ta>
            <ta e="T322" id="Seg_5729" s="T321">Luchs.[NOM]-Zobel.[NOM]</ta>
            <ta e="T323" id="Seg_5730" s="T322">Kleidung-ACC</ta>
            <ta e="T324" id="Seg_5731" s="T323">sich.anziehen-CVB.SEQ</ta>
            <ta e="T325" id="Seg_5732" s="T324">werfen-PST2.[3SG]</ta>
            <ta e="T326" id="Seg_5733" s="T325">dieses.[NOM]</ta>
            <ta e="T327" id="Seg_5734" s="T326">solange</ta>
            <ta e="T328" id="Seg_5735" s="T327">was.[NOM]</ta>
            <ta e="T329" id="Seg_5736" s="T328">NEG</ta>
            <ta e="T330" id="Seg_5737" s="T329">Mensch-3SG.[NOM]</ta>
            <ta e="T331" id="Seg_5738" s="T330">bemerken-PASS/REFL-EP-NEG.[3SG]</ta>
            <ta e="T332" id="Seg_5739" s="T331">drei</ta>
            <ta e="T333" id="Seg_5740" s="T332">Tag-ACC</ta>
            <ta e="T334" id="Seg_5741" s="T333">während</ta>
            <ta e="T335" id="Seg_5742" s="T334">was.[NOM]</ta>
            <ta e="T336" id="Seg_5743" s="T335">NEG</ta>
            <ta e="T337" id="Seg_5744" s="T336">Mensch-3SG.[NOM]</ta>
            <ta e="T338" id="Seg_5745" s="T337">bemerken-PASS/REFL-EP-NEG.[3SG]</ta>
            <ta e="T339" id="Seg_5746" s="T338">nur</ta>
            <ta e="T340" id="Seg_5747" s="T339">Abend.[NOM]</ta>
            <ta e="T341" id="Seg_5748" s="T340">sein-FUT.[3SG]</ta>
            <ta e="T342" id="Seg_5749" s="T341">und</ta>
            <ta e="T343" id="Seg_5750" s="T342">Rauchloch-3SG-INSTR</ta>
            <ta e="T344" id="Seg_5751" s="T343">alt</ta>
            <ta e="T345" id="Seg_5752" s="T344">Mensch.[NOM]</ta>
            <ta e="T346" id="Seg_5753" s="T345">Kopf-PL-3SG.[NOM]</ta>
            <ta e="T347" id="Seg_5754" s="T346">fallen-PRS-3PL</ta>
            <ta e="T348" id="Seg_5755" s="T347">jenes-PL-ACC</ta>
            <ta e="T349" id="Seg_5756" s="T348">durchstechen-EP-IMP.2PL-schneiden-EP-IMP.2PL</ta>
            <ta e="T350" id="Seg_5757" s="T349">durchstechen-EP-CAUS-CVB.SIM</ta>
            <ta e="T351" id="Seg_5758" s="T350">gehen.[IMP.2SG]</ta>
            <ta e="T352" id="Seg_5759" s="T351">sagen-CVB.SIM-sagen-CVB.SIM</ta>
            <ta e="T353" id="Seg_5760" s="T352">durchstechen-EP-CAUS-CVB.SIM</ta>
            <ta e="T354" id="Seg_5761" s="T353">gehen-PRS.[3SG]</ta>
            <ta e="T355" id="Seg_5762" s="T354">jenes-PL-3SG.[NOM]</ta>
            <ta e="T356" id="Seg_5763" s="T355">Erde.[NOM]</ta>
            <ta e="T357" id="Seg_5764" s="T356">zu</ta>
            <ta e="T358" id="Seg_5765" s="T357">Angst.haben-CVB.SEQ</ta>
            <ta e="T359" id="Seg_5766" s="T358">gehen-PRS-3PL</ta>
            <ta e="T360" id="Seg_5767" s="T359">Alte.[NOM]</ta>
            <ta e="T361" id="Seg_5768" s="T360">wie.viel</ta>
            <ta e="T362" id="Seg_5769" s="T361">zehn</ta>
            <ta e="T363" id="Seg_5770" s="T362">solange</ta>
            <ta e="T364" id="Seg_5771" s="T363">leben-PST2.[3SG]</ta>
            <ta e="T365" id="Seg_5772" s="T364">Rentier-3SG.[NOM]</ta>
            <ta e="T366" id="Seg_5773" s="T365">wohin</ta>
            <ta e="T367" id="Seg_5774" s="T366">NEG</ta>
            <ta e="T368" id="Seg_5775" s="T367">sich.bewegen-NEG.[3SG]</ta>
            <ta e="T369" id="Seg_5776" s="T368">Pferd-PL-3SG.[NOM]</ta>
            <ta e="T370" id="Seg_5777" s="T369">dieses.EMPH.[NOM]</ta>
            <ta e="T371" id="Seg_5778" s="T370">ähnlich</ta>
            <ta e="T372" id="Seg_5779" s="T371">liegen-PRS-3PL</ta>
            <ta e="T373" id="Seg_5780" s="T372">Fisch-3PL.[NOM]</ta>
            <ta e="T374" id="Seg_5781" s="T373">dieses.EMPH</ta>
            <ta e="T375" id="Seg_5782" s="T374">jagen-EP-REFL-PTCP.PST.[NOM]</ta>
            <ta e="T376" id="Seg_5783" s="T375">wie</ta>
            <ta e="T377" id="Seg_5784" s="T376">gehen-PRS.[3SG]</ta>
            <ta e="T378" id="Seg_5785" s="T377">Alte.[NOM]</ta>
            <ta e="T379" id="Seg_5786" s="T378">so</ta>
            <ta e="T380" id="Seg_5787" s="T379">reich.sein-CVB.SEQ-reich.sein-CVB.SEQ</ta>
            <ta e="T381" id="Seg_5788" s="T380">EMPH-alleine</ta>
            <ta e="T382" id="Seg_5789" s="T381">leben-PRS.[3SG]</ta>
            <ta e="T383" id="Seg_5790" s="T382">so</ta>
            <ta e="T384" id="Seg_5791" s="T383">machen-CVB.SEQ</ta>
            <ta e="T385" id="Seg_5792" s="T384">Alte.[NOM]</ta>
            <ta e="T386" id="Seg_5793" s="T385">Gedanke-3SG.[NOM]</ta>
            <ta e="T387" id="Seg_5794" s="T386">kommen-PST2.[3SG]</ta>
            <ta e="T388" id="Seg_5795" s="T387">Kind-PL.[NOM]</ta>
            <ta e="T389" id="Seg_5796" s="T388">1SG.[NOM]</ta>
            <ta e="T390" id="Seg_5797" s="T389">sitzen-PTCP.FUT-COMP</ta>
            <ta e="T391" id="Seg_5798" s="T390">alter.Mann-EP-1SG.[NOM]</ta>
            <ta e="T392" id="Seg_5799" s="T391">fern-3SG-DAT/LOC</ta>
            <ta e="T393" id="Seg_5800" s="T392">erfahren-RECP/COLL-CVB.SIM</ta>
            <ta e="T394" id="Seg_5801" s="T393">gehen-PTCP.FUT</ta>
            <ta e="T395" id="Seg_5802" s="T394">es.gibt-EP-1SG</ta>
            <ta e="T396" id="Seg_5803" s="T395">drei-ORD</ta>
            <ta e="T397" id="Seg_5804" s="T396">Jahr-3SG-DAT/LOC</ta>
            <ta e="T398" id="Seg_5805" s="T397">alter.Mann-3SG-DAT/LOC</ta>
            <ta e="T399" id="Seg_5806" s="T398">gehen-CVB.PURP</ta>
            <ta e="T400" id="Seg_5807" s="T399">sich.anziehen-PST1-3SG</ta>
            <ta e="T401" id="Seg_5808" s="T400">Brot-ACC</ta>
            <ta e="T402" id="Seg_5809" s="T401">schneiden-FREQ-CVB.SEQ</ta>
            <ta e="T403" id="Seg_5810" s="T402">nachdem</ta>
            <ta e="T404" id="Seg_5811" s="T403">nehmen-PST1-3SG</ta>
            <ta e="T405" id="Seg_5812" s="T404">jenes.[NOM]</ta>
            <ta e="T406" id="Seg_5813" s="T405">machen-CVB.SEQ</ta>
            <ta e="T407" id="Seg_5814" s="T406">nachdem</ta>
            <ta e="T408" id="Seg_5815" s="T407">wildes.Rentier.[NOM]</ta>
            <ta e="T409" id="Seg_5816" s="T408">Fett-3SG-ACC</ta>
            <ta e="T410" id="Seg_5817" s="T409">bringen-CAP.[3SG]</ta>
            <ta e="T411" id="Seg_5818" s="T410">so</ta>
            <ta e="T412" id="Seg_5819" s="T411">machen-CVB.SEQ</ta>
            <ta e="T413" id="Seg_5820" s="T412">nachdem</ta>
            <ta e="T414" id="Seg_5821" s="T413">alter.Mann-3SG-DAT/LOC</ta>
            <ta e="T415" id="Seg_5822" s="T414">gehen-PST1-3SG</ta>
            <ta e="T416" id="Seg_5823" s="T415">dieses</ta>
            <ta e="T417" id="Seg_5824" s="T416">gehen-CVB.SEQ</ta>
            <ta e="T418" id="Seg_5825" s="T417">gehen-CVB.SEQ</ta>
            <ta e="T419" id="Seg_5826" s="T418">alter.Mann-3SG-GEN</ta>
            <ta e="T420" id="Seg_5827" s="T419">Erdhütte-3SG-DAT/LOC</ta>
            <ta e="T421" id="Seg_5828" s="T420">ankommen-PST2.[3SG]</ta>
            <ta e="T422" id="Seg_5829" s="T421">nur</ta>
            <ta e="T423" id="Seg_5830" s="T422">was-INTNS.[NOM]</ta>
            <ta e="T424" id="Seg_5831" s="T423">MOD</ta>
            <ta e="T425" id="Seg_5832" s="T424">NEG.EX</ta>
            <ta e="T426" id="Seg_5833" s="T425">ganz-SIM</ta>
            <ta e="T427" id="Seg_5834" s="T426">ganz</ta>
            <ta e="T428" id="Seg_5835" s="T427">stöbern-EP-PASS/REFL-CVB.SEQ</ta>
            <ta e="T429" id="Seg_5836" s="T428">krachen-CVB.SEQ</ta>
            <ta e="T430" id="Seg_5837" s="T429">bleiben-PST2.[3SG]</ta>
            <ta e="T431" id="Seg_5838" s="T430">jenes.[NOM]</ta>
            <ta e="T432" id="Seg_5839" s="T431">und</ta>
            <ta e="T433" id="Seg_5840" s="T432">sein-COND.[3SG]</ta>
            <ta e="T434" id="Seg_5841" s="T433">Rauchloch-3SG-DAT/LOC</ta>
            <ta e="T435" id="Seg_5842" s="T434">klettern-EP-PST2.[3SG]</ta>
            <ta e="T436" id="Seg_5843" s="T435">nur</ta>
            <ta e="T437" id="Seg_5844" s="T436">Herd-3SG-ACC</ta>
            <ta e="T438" id="Seg_5845" s="T437">um.herum</ta>
            <ta e="T439" id="Seg_5846" s="T438">drei</ta>
            <ta e="T440" id="Seg_5847" s="T439">Tierhaar-PROPR</ta>
            <ta e="T441" id="Seg_5848" s="T440">Wurm.[NOM]</ta>
            <ta e="T442" id="Seg_5849" s="T441">werden-CVB.SEQ</ta>
            <ta e="T443" id="Seg_5850" s="T442">sich.schlecht.benehmen-CVB.SIM</ta>
            <ta e="T444" id="Seg_5851" s="T443">liegen-PRS.[3SG]</ta>
            <ta e="T445" id="Seg_5852" s="T444">Pferd-3SG-GEN</ta>
            <ta e="T446" id="Seg_5853" s="T445">schwarz</ta>
            <ta e="T447" id="Seg_5854" s="T446">Huf-3SG-ACC</ta>
            <ta e="T448" id="Seg_5855" s="T447">mit</ta>
            <ta e="T449" id="Seg_5856" s="T448">aufhören-MED-EP-NMNZ-3SG-ACC</ta>
            <ta e="T450" id="Seg_5857" s="T449">nur</ta>
            <ta e="T451" id="Seg_5858" s="T450">übrig.bleiben-CAUS-PST2.[3SG]</ta>
            <ta e="T452" id="Seg_5859" s="T451">Pferd-3SG-GEN</ta>
            <ta e="T453" id="Seg_5860" s="T452">eigen-3SG-ACC</ta>
            <ta e="T454" id="Seg_5861" s="T453">vor.kurzem</ta>
            <ta e="T455" id="Seg_5862" s="T454">sich.verwandeln-PTCP.PST</ta>
            <ta e="T456" id="Seg_5863" s="T455">Überreste-3SG.[NOM]</ta>
            <ta e="T457" id="Seg_5864" s="T456">man.sagt</ta>
            <ta e="T458" id="Seg_5865" s="T457">jenes.[NOM]</ta>
            <ta e="T459" id="Seg_5866" s="T458">für</ta>
            <ta e="T460" id="Seg_5867" s="T459">sterben-EP-NEG-CVB.PURP</ta>
            <ta e="T461" id="Seg_5868" s="T460">sich.verwandeln-PST2-3SG</ta>
            <ta e="T462" id="Seg_5869" s="T461">man.sagt</ta>
            <ta e="T463" id="Seg_5870" s="T462">doch</ta>
            <ta e="T464" id="Seg_5871" s="T463">jenes-ACC</ta>
            <ta e="T465" id="Seg_5872" s="T464">dieses</ta>
            <ta e="T466" id="Seg_5873" s="T465">Brot-EP-INSTR</ta>
            <ta e="T467" id="Seg_5874" s="T466">werfen-PST2.[3SG]</ta>
            <ta e="T468" id="Seg_5875" s="T467">dieses-ACC</ta>
            <ta e="T469" id="Seg_5876" s="T468">krümmen-EP-NMNZ.[NOM]</ta>
            <ta e="T470" id="Seg_5877" s="T469">machen-PST2.[3SG]</ta>
            <ta e="T471" id="Seg_5878" s="T470">und</ta>
            <ta e="T472" id="Seg_5879" s="T471">sprechen-PST2.[3SG]</ta>
            <ta e="T473" id="Seg_5880" s="T472">dieses.[NOM]</ta>
            <ta e="T474" id="Seg_5881" s="T473">Herd-EP-1SG.[NOM]</ta>
            <ta e="T475" id="Seg_5882" s="T474">Lehm-3SG.[NOM]</ta>
            <ta e="T476" id="Seg_5883" s="T475">liegen-CAUS-EP-NEG-CVB.PURP</ta>
            <ta e="T477" id="Seg_5884" s="T476">machen-PST2.[3SG]</ta>
            <ta e="T478" id="Seg_5885" s="T477">sagen-PRS.[3SG]</ta>
            <ta e="T479" id="Seg_5886" s="T478">Fett-INSTR</ta>
            <ta e="T480" id="Seg_5887" s="T479">werfen-PST2.[3SG]</ta>
            <ta e="T481" id="Seg_5888" s="T480">oh</ta>
            <ta e="T482" id="Seg_5889" s="T481">Klumpen-PL.[NOM]</ta>
            <ta e="T483" id="Seg_5890" s="T482">fallen-CVB.SEQ-3PL</ta>
            <ta e="T484" id="Seg_5891" s="T483">liegen-CAUS-EP-NEG-CVB.PURP</ta>
            <ta e="T485" id="Seg_5892" s="T484">machen-PST1-3PL</ta>
            <ta e="T486" id="Seg_5893" s="T485">wieder</ta>
            <ta e="T487" id="Seg_5894" s="T486">werfen-PST2.[3SG]</ta>
            <ta e="T488" id="Seg_5895" s="T487">dieses-ACC</ta>
            <ta e="T489" id="Seg_5896" s="T488">beißen-CVB.SEQ</ta>
            <ta e="T490" id="Seg_5897" s="T489">versuchen-PST2.[3SG]</ta>
            <ta e="T491" id="Seg_5898" s="T490">Fett.[NOM]</ta>
            <ta e="T492" id="Seg_5899" s="T491">sein-PST2.[3SG]</ta>
            <ta e="T493" id="Seg_5900" s="T492">jenes-ACC</ta>
            <ta e="T494" id="Seg_5901" s="T493">Himmel.[NOM]</ta>
            <ta e="T495" id="Seg_5902" s="T494">Himmel.[NOM]</ta>
            <ta e="T496" id="Seg_5903" s="T495">ein.Bisschen-DIM-INTNS</ta>
            <ta e="T497" id="Seg_5904" s="T496">sagen-PST2.[3SG]</ta>
            <ta e="T498" id="Seg_5905" s="T497">na</ta>
            <ta e="T499" id="Seg_5906" s="T498">alter.Mann.[NOM]</ta>
            <ta e="T500" id="Seg_5907" s="T499">Tür-2SG-ACC</ta>
            <ta e="T501" id="Seg_5908" s="T500">öffnen.[IMP.2SG]</ta>
            <ta e="T502" id="Seg_5909" s="T501">sagen-PST2.[3SG]</ta>
            <ta e="T503" id="Seg_5910" s="T502">Alte-3SG.[NOM]</ta>
            <ta e="T504" id="Seg_5911" s="T503">Tür-3SG-ACC</ta>
            <ta e="T505" id="Seg_5912" s="T504">öffnen-CVB.SEQ</ta>
            <ta e="T506" id="Seg_5913" s="T505">Überreste-VBZ-PST2.[3SG]</ta>
            <ta e="T507" id="Seg_5914" s="T506">Alte.[NOM]</ta>
            <ta e="T508" id="Seg_5915" s="T507">essen-EP-CAUS-CVB.SEQ#</ta>
            <ta e="T509" id="Seg_5916" s="T508">nachdem</ta>
            <ta e="T510" id="Seg_5917" s="T509">Haus-3SG-DAT/LOC</ta>
            <ta e="T511" id="Seg_5918" s="T510">bringen-PST2.[3SG]</ta>
            <ta e="T512" id="Seg_5919" s="T511">dieses</ta>
            <ta e="T513" id="Seg_5920" s="T512">Haus-3SG-DAT/LOC</ta>
            <ta e="T514" id="Seg_5921" s="T513">tragen-CVB.SEQ</ta>
            <ta e="T515" id="Seg_5922" s="T514">abwaschen-CVB.SEQ-kämmen-CVB.SEQ</ta>
            <ta e="T516" id="Seg_5923" s="T515">nachdem</ta>
            <ta e="T517" id="Seg_5924" s="T516">sich.anziehen-EP-NEG.PTCP.PST</ta>
            <ta e="T518" id="Seg_5925" s="T517">Kleidung-ACC</ta>
            <ta e="T519" id="Seg_5926" s="T518">sich.anziehen-CAUS-CVB.SEQ</ta>
            <ta e="T520" id="Seg_5927" s="T519">nachdem</ta>
            <ta e="T521" id="Seg_5928" s="T520">Glas.[NOM]</ta>
            <ta e="T522" id="Seg_5929" s="T521">Bett-DAT/LOC</ta>
            <ta e="T523" id="Seg_5930" s="T522">setzen-CVB.SEQ</ta>
            <ta e="T524" id="Seg_5931" s="T523">werfen-PST2.[3SG]</ta>
            <ta e="T525" id="Seg_5932" s="T524">jenes</ta>
            <ta e="T526" id="Seg_5933" s="T525">machen-CVB.SEQ</ta>
            <ta e="T527" id="Seg_5934" s="T526">nachdem</ta>
            <ta e="T528" id="Seg_5935" s="T527">Tee.[NOM]</ta>
            <ta e="T529" id="Seg_5936" s="T528">trinken-EP-CAUS.[CAUS]-PST2.[3SG]</ta>
            <ta e="T530" id="Seg_5937" s="T529">dann</ta>
            <ta e="T531" id="Seg_5938" s="T530">Fleisch.[NOM]</ta>
            <ta e="T532" id="Seg_5939" s="T531">Speise.[NOM]</ta>
            <ta e="T533" id="Seg_5940" s="T532">herausnehmen-PST2.[3SG]</ta>
            <ta e="T534" id="Seg_5941" s="T533">Fett-PROPR</ta>
            <ta e="T535" id="Seg_5942" s="T534">Suppe-ACC</ta>
            <ta e="T536" id="Seg_5943" s="T535">Schüssel-DAT/LOC</ta>
            <ta e="T537" id="Seg_5944" s="T536">herausnehmen-PST2.[3SG]</ta>
            <ta e="T538" id="Seg_5945" s="T537">so</ta>
            <ta e="T539" id="Seg_5946" s="T538">machen-CVB.SEQ</ta>
            <ta e="T540" id="Seg_5947" s="T539">Gold.[NOM]</ta>
            <ta e="T541" id="Seg_5948" s="T540">Löffel.[NOM]</ta>
            <ta e="T542" id="Seg_5949" s="T541">Gabel.[NOM]</ta>
            <ta e="T543" id="Seg_5950" s="T542">legen-PST2.[3SG]</ta>
            <ta e="T544" id="Seg_5951" s="T543">nur</ta>
            <ta e="T545" id="Seg_5952" s="T544">Löffel-3SG-INSTR</ta>
            <ta e="T546" id="Seg_5953" s="T545">trinken-CVB.PURP</ta>
            <ta e="T547" id="Seg_5954" s="T546">wollen-PST2.[3SG]</ta>
            <ta e="T548" id="Seg_5955" s="T547">na</ta>
            <ta e="T549" id="Seg_5956" s="T548">dieses-dieses</ta>
            <ta e="T550" id="Seg_5957" s="T549">Löffel-1SG.[NOM]</ta>
            <ta e="T551" id="Seg_5958" s="T550">klein.[NOM]</ta>
            <ta e="T552" id="Seg_5959" s="T551">groß</ta>
            <ta e="T553" id="Seg_5960" s="T552">Suppenkelle-PART</ta>
            <ta e="T554" id="Seg_5961" s="T553">Q</ta>
            <ta e="T555" id="Seg_5962" s="T554">Schöpfkelle-PART</ta>
            <ta e="T556" id="Seg_5963" s="T555">Q</ta>
            <ta e="T557" id="Seg_5964" s="T556">bringen.[IMP.2SG]</ta>
            <ta e="T558" id="Seg_5965" s="T557">sagen-PST2.[3SG]</ta>
            <ta e="T559" id="Seg_5966" s="T558">von.eben-3SG.[NOM]</ta>
            <ta e="T560" id="Seg_5967" s="T559">Schöpfkelle.[NOM]</ta>
            <ta e="T561" id="Seg_5968" s="T560">sehr-3SG</ta>
            <ta e="T562" id="Seg_5969" s="T561">überreichen-CVB.SEQ</ta>
            <ta e="T563" id="Seg_5970" s="T562">werfen-PST2.[3SG]</ta>
            <ta e="T564" id="Seg_5971" s="T563">Fett-PROPR</ta>
            <ta e="T565" id="Seg_5972" s="T564">Suppe-ACC</ta>
            <ta e="T566" id="Seg_5973" s="T565">dieses-3SG-INSTR</ta>
            <ta e="T567" id="Seg_5974" s="T566">hup</ta>
            <ta e="T568" id="Seg_5975" s="T567">machen-PST2.[3SG]</ta>
            <ta e="T569" id="Seg_5976" s="T568">dieses-DAT/LOC</ta>
            <ta e="T570" id="Seg_5977" s="T569">doch</ta>
            <ta e="T571" id="Seg_5978" s="T570">ersticken-PRS.[3SG]</ta>
            <ta e="T572" id="Seg_5979" s="T571">EMPH</ta>
            <ta e="T573" id="Seg_5980" s="T572">doch</ta>
            <ta e="T574" id="Seg_5981" s="T573">dann</ta>
            <ta e="T575" id="Seg_5982" s="T574">ersticken-PRS-1SG</ta>
            <ta e="T576" id="Seg_5983" s="T575">sagen-CVB.SEQ</ta>
            <ta e="T577" id="Seg_5984" s="T576">doch</ta>
            <ta e="T578" id="Seg_5985" s="T577">lautes.Geräusch-VBZ-PRS.[3SG]</ta>
            <ta e="T579" id="Seg_5986" s="T578">EMPH</ta>
            <ta e="T580" id="Seg_5987" s="T579">Glas.[NOM]</ta>
            <ta e="T581" id="Seg_5988" s="T580">Unterlage-3SG-ACC</ta>
            <ta e="T582" id="Seg_5989" s="T581">durch</ta>
            <ta e="T583" id="Seg_5990" s="T582">lautes.Geräusch-VBZ-PRS.[3SG]</ta>
            <ta e="T584" id="Seg_5991" s="T583">und</ta>
            <ta e="T585" id="Seg_5992" s="T584">so</ta>
            <ta e="T586" id="Seg_5993" s="T585">drei</ta>
            <ta e="T587" id="Seg_5994" s="T586">Eisschicht.[NOM]</ta>
            <ta e="T588" id="Seg_5995" s="T587">Erde-ACC</ta>
            <ta e="T589" id="Seg_5996" s="T588">völlig</ta>
            <ta e="T590" id="Seg_5997" s="T589">fallen-CVB.SEQ</ta>
            <ta e="T591" id="Seg_5998" s="T590">bleiben-PRS.[3SG]</ta>
            <ta e="T592" id="Seg_5999" s="T591">Alte-3SG.[NOM]</ta>
            <ta e="T593" id="Seg_6000" s="T592">was.[NOM]</ta>
            <ta e="T594" id="Seg_6001" s="T593">sein-FUT-3SG=Q</ta>
            <ta e="T595" id="Seg_6002" s="T594">wie</ta>
            <ta e="T596" id="Seg_6003" s="T595">leben-PST2-3SG</ta>
            <ta e="T597" id="Seg_6004" s="T596">und</ta>
            <ta e="T598" id="Seg_6005" s="T597">so</ta>
            <ta e="T601" id="Seg_6006" s="T598">immer</ta>
            <ta e="T599" id="Seg_6007" s="T601">immer</ta>
            <ta e="T600" id="Seg_6008" s="T599">leben-PRS.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_6009" s="T0">старик-PROPR</ta>
            <ta e="T2" id="Seg_6010" s="T1">старуха.[NOM]</ta>
            <ta e="T3" id="Seg_6011" s="T2">жить-PST2-3PL</ta>
            <ta e="T4" id="Seg_6012" s="T3">говорят</ta>
            <ta e="T5" id="Seg_6013" s="T4">родить-EP-CAUS-PTCP.PRS</ta>
            <ta e="T6" id="Seg_6014" s="T5">ребенок-POSS</ta>
            <ta e="T7" id="Seg_6015" s="T6">NEG-3PL</ta>
            <ta e="T8" id="Seg_6016" s="T7">чум-DAT/LOC</ta>
            <ta e="T9" id="Seg_6017" s="T8">сидеть-TEMP-3PL</ta>
            <ta e="T10" id="Seg_6018" s="T9">огонь-3PL.[NOM]</ta>
            <ta e="T11" id="Seg_6019" s="T10">только</ta>
            <ta e="T12" id="Seg_6020" s="T11">краснеть-PRS.[3SG]</ta>
            <ta e="T13" id="Seg_6021" s="T12">так</ta>
            <ta e="T14" id="Seg_6022" s="T13">бедный-PL.[NOM]</ta>
            <ta e="T15" id="Seg_6023" s="T14">на.улицу</ta>
            <ta e="T16" id="Seg_6024" s="T15">выйти-TEMP-3PL</ta>
            <ta e="T17" id="Seg_6025" s="T16">снег-3PL.[NOM]</ta>
            <ta e="T18" id="Seg_6026" s="T17">только</ta>
            <ta e="T19" id="Seg_6027" s="T18">белеть-PRS.[3SG]</ta>
            <ta e="T20" id="Seg_6028" s="T19">что-POSS</ta>
            <ta e="T21" id="Seg_6029" s="T20">NEG</ta>
            <ta e="T22" id="Seg_6030" s="T21">NEG-3PL</ta>
            <ta e="T23" id="Seg_6031" s="T22">только</ta>
            <ta e="T24" id="Seg_6032" s="T23">средства-3PL.[NOM]</ta>
            <ta e="T25" id="Seg_6033" s="T24">говорить-CVB.SEQ-3PL</ta>
            <ta e="T26" id="Seg_6034" s="T25">EMPH-одинокий</ta>
            <ta e="T27" id="Seg_6035" s="T26">лошадь-PROPR-3PL</ta>
            <ta e="T28" id="Seg_6036" s="T27">потом</ta>
            <ta e="T29" id="Seg_6037" s="T28">охотиться-PTCP.PRS</ta>
            <ta e="T30" id="Seg_6038" s="T29">снаряжение-3PL.[NOM]</ta>
            <ta e="T31" id="Seg_6039" s="T30">три</ta>
            <ta e="T32" id="Seg_6040" s="T31">камень-PROPR</ta>
            <ta e="T33" id="Seg_6041" s="T32">сеть-DIM-PROPR-3PL</ta>
            <ta e="T34" id="Seg_6042" s="T33">тот-3SG-3PL-DAT/LOC</ta>
            <ta e="T35" id="Seg_6043" s="T34">вода.[NOM]</ta>
            <ta e="T36" id="Seg_6044" s="T35">мелкие.гады-3SG.[NOM]</ta>
            <ta e="T37" id="Seg_6045" s="T36">нацеплять-EP-REFL-TEMP-3SG</ta>
            <ta e="T38" id="Seg_6046" s="T37">тот-ACC</ta>
            <ta e="T39" id="Seg_6047" s="T38">есть-PRS-3PL</ta>
            <ta e="T40" id="Seg_6048" s="T39">вертел-DAT/LOC</ta>
            <ta e="T41" id="Seg_6049" s="T40">жарить-CVB.SEQ</ta>
            <ta e="T42" id="Seg_6050" s="T41">только</ta>
            <ta e="T43" id="Seg_6051" s="T42">умирать-CVB.PURP</ta>
            <ta e="T44" id="Seg_6052" s="T43">сутулиться-PST2-3PL</ta>
            <ta e="T45" id="Seg_6053" s="T44">кончаться-CVB.PURP</ta>
            <ta e="T46" id="Seg_6054" s="T45">подтянуть.брюхо-PST2-3PL</ta>
            <ta e="T47" id="Seg_6055" s="T46">потом</ta>
            <ta e="T48" id="Seg_6056" s="T47">старик-3SG.[NOM]</ta>
            <ta e="T49" id="Seg_6057" s="T48">вдруг</ta>
            <ta e="T50" id="Seg_6058" s="T49">входить-PST2.[3SG]</ta>
            <ta e="T51" id="Seg_6059" s="T50">старуха-3SG-DAT/LOC</ta>
            <ta e="T52" id="Seg_6060" s="T51">вот</ta>
            <ta e="T53" id="Seg_6061" s="T52">входить-CVB.SEQ</ta>
            <ta e="T54" id="Seg_6062" s="T53">говорить-PST2.[3SG]</ta>
            <ta e="T55" id="Seg_6063" s="T54">эй</ta>
            <ta e="T56" id="Seg_6064" s="T55">старуха.[NOM]</ta>
            <ta e="T57" id="Seg_6065" s="T56">подниматься-PRS-1PL</ta>
            <ta e="T58" id="Seg_6066" s="T57">идти-CAUS-FUT-1PL</ta>
            <ta e="T59" id="Seg_6067" s="T58">одинокий</ta>
            <ta e="T60" id="Seg_6068" s="T59">лошадь-PROPR-1PL</ta>
            <ta e="T61" id="Seg_6069" s="T60">есть-PTCP.FUT-1PL.[NOM]</ta>
            <ta e="T62" id="Seg_6070" s="T61">есть-3SG</ta>
            <ta e="T63" id="Seg_6071" s="T62">даже</ta>
            <ta e="T64" id="Seg_6072" s="T63">голодать-CVB.SEQ</ta>
            <ta e="T65" id="Seg_6073" s="T64">умирать-TEMP-1PL</ta>
            <ta e="T66" id="Seg_6074" s="T65">задняя.часть-1PL-DAT/LOC</ta>
            <ta e="T67" id="Seg_6075" s="T66">оставаться-FUT-3SG</ta>
            <ta e="T68" id="Seg_6076" s="T67">тот-DAT/LOC</ta>
            <ta e="T69" id="Seg_6077" s="T68">старуха.[NOM]</ta>
            <ta e="T70" id="Seg_6078" s="T69">сам-2SG.[NOM]</ta>
            <ta e="T71" id="Seg_6079" s="T70">мысль-2SG.[NOM]</ta>
            <ta e="T72" id="Seg_6080" s="T71">говорить-PST2.[3SG]</ta>
            <ta e="T73" id="Seg_6081" s="T72">убить-COND-2SG</ta>
            <ta e="T74" id="Seg_6082" s="T73">только</ta>
            <ta e="T75" id="Seg_6083" s="T74">есть-FUT-1PL</ta>
            <ta e="T76" id="Seg_6084" s="T75">мужчина-3SG.[NOM]</ta>
            <ta e="T77" id="Seg_6085" s="T76">выйти-PST2.[3SG]</ta>
            <ta e="T78" id="Seg_6086" s="T77">да</ta>
            <ta e="T79" id="Seg_6087" s="T78">убить-PST2.[3SG]</ta>
            <ta e="T80" id="Seg_6088" s="T79">старик-3SG.[NOM]</ta>
            <ta e="T81" id="Seg_6089" s="T80">сдирать.кожу-PST2.[3SG]</ta>
            <ta e="T82" id="Seg_6090" s="T81">только</ta>
            <ta e="T83" id="Seg_6091" s="T82">холодний.[NOM]</ta>
            <ta e="T84" id="Seg_6092" s="T83">очень</ta>
            <ta e="T85" id="Seg_6093" s="T84">морозный.[NOM]</ta>
            <ta e="T86" id="Seg_6094" s="T85">очень</ta>
            <ta e="T87" id="Seg_6095" s="T86">старик-3SG.[NOM]</ta>
            <ta e="T88" id="Seg_6096" s="T87">входить-PST2.[3SG]</ta>
            <ta e="T89" id="Seg_6097" s="T88">входить-CVB.SEQ</ta>
            <ta e="T90" id="Seg_6098" s="T89">говорить-PST2.[3SG]</ta>
            <ta e="T91" id="Seg_6099" s="T90">лошадь-1PL.[NOM]</ta>
            <ta e="T92" id="Seg_6100" s="T91">мясо-3SG-ACC</ta>
            <ta e="T93" id="Seg_6101" s="T92">целый-SIM</ta>
            <ta e="T94" id="Seg_6102" s="T93">четырехугольный.чум-DIM-1PL.[NOM]</ta>
            <ta e="T95" id="Seg_6103" s="T94">на.тот.берег</ta>
            <ta e="T96" id="Seg_6104" s="T95">таскать-VBZ-CVB.SEQ</ta>
            <ta e="T97" id="Seg_6105" s="T96">бросать.[IMP.2SG]</ta>
            <ta e="T98" id="Seg_6106" s="T97">целый-3SG-ACC</ta>
            <ta e="T99" id="Seg_6107" s="T98">старуха-3SG.[NOM]</ta>
            <ta e="T100" id="Seg_6108" s="T99">целый-3SG-ACC</ta>
            <ta e="T101" id="Seg_6109" s="T100">таскать-VBZ-PST2.[3SG]</ta>
            <ta e="T102" id="Seg_6110" s="T101">старуха-3SG.[NOM]</ta>
            <ta e="T103" id="Seg_6111" s="T102">рука-3SG-ACC</ta>
            <ta e="T104" id="Seg_6112" s="T103">согреться-CAUS-CVB.PURP</ta>
            <ta e="T105" id="Seg_6113" s="T104">хотеть-PTCP.PST-3SG-ACC</ta>
            <ta e="T106" id="Seg_6114" s="T105">старик.[NOM]</ta>
            <ta e="T107" id="Seg_6115" s="T106">говорить-PST2.[3SG]</ta>
            <ta e="T108" id="Seg_6116" s="T107">есть-PTCP.FUT-2SG-ACC</ta>
            <ta e="T109" id="Seg_6117" s="T108">каждый</ta>
            <ta e="T110" id="Seg_6118" s="T109">сосуд-2SG-DAT/LOC</ta>
            <ta e="T111" id="Seg_6119" s="T110">ведро-2SG-DAT/LOC</ta>
            <ta e="T112" id="Seg_6120" s="T111">вода-PART</ta>
            <ta e="T113" id="Seg_6121" s="T112">черпать-CVB.SEQ</ta>
            <ta e="T114" id="Seg_6122" s="T113">наполнять.[IMP.2SG]</ta>
            <ta e="T115" id="Seg_6123" s="T114">готовый-VBZ-CVB.SEQ</ta>
            <ta e="T116" id="Seg_6124" s="T115">после</ta>
            <ta e="T117" id="Seg_6125" s="T116">вот</ta>
            <ta e="T118" id="Seg_6126" s="T117">есть-FUT.[IMP.2SG]</ta>
            <ta e="T119" id="Seg_6127" s="T118">так</ta>
            <ta e="T120" id="Seg_6128" s="T119">делать-CVB.SEQ</ta>
            <ta e="T121" id="Seg_6129" s="T120">старуха.[NOM]</ta>
            <ta e="T122" id="Seg_6130" s="T121">выйти-CVB.SEQ</ta>
            <ta e="T123" id="Seg_6131" s="T122">каждый</ta>
            <ta e="T124" id="Seg_6132" s="T123">сосуд-3SG-DAT/LOC</ta>
            <ta e="T125" id="Seg_6133" s="T124">всё-DIM-3PL-DAT/LOC</ta>
            <ta e="T126" id="Seg_6134" s="T125">вода.[NOM]</ta>
            <ta e="T127" id="Seg_6135" s="T126">черпать-PST2.[3SG]</ta>
            <ta e="T128" id="Seg_6136" s="T127">только</ta>
            <ta e="T129" id="Seg_6137" s="T128">последний</ta>
            <ta e="T130" id="Seg_6138" s="T129">котел-DAT/LOC-DIM-DIM-DIM-3SG-ACC</ta>
            <ta e="T131" id="Seg_6139" s="T130">принести-CVB.PURP</ta>
            <ta e="T132" id="Seg_6140" s="T131">хотеть-PST2-3SG</ta>
            <ta e="T133" id="Seg_6141" s="T132">дверь-3SG.[NOM]</ta>
            <ta e="T134" id="Seg_6142" s="T133">запертый.[NOM]</ta>
            <ta e="T135" id="Seg_6143" s="T134">эй</ta>
            <ta e="T136" id="Seg_6144" s="T135">старик.[NOM]</ta>
            <ta e="T137" id="Seg_6145" s="T136">дверь-2SG-ACC</ta>
            <ta e="T138" id="Seg_6146" s="T137">толкать.[IMP.2SG]</ta>
            <ta e="T139" id="Seg_6147" s="T138">замерзать-CVB.PURP</ta>
            <ta e="T140" id="Seg_6148" s="T139">делать-PST1-1SG</ta>
            <ta e="T141" id="Seg_6149" s="T140">говорить-PST2.[3SG]</ta>
            <ta e="T142" id="Seg_6150" s="T141">сука.[NOM]</ta>
            <ta e="T143" id="Seg_6151" s="T142">дочь-3SG.[NOM]</ta>
            <ta e="T144" id="Seg_6152" s="T143">снег-DAT/LOC</ta>
            <ta e="T145" id="Seg_6153" s="T144">врозь</ta>
            <ta e="T146" id="Seg_6154" s="T145">бросать.[IMP.2SG]</ta>
            <ta e="T147" id="Seg_6155" s="T146">говорить-PST2.[3SG]</ta>
            <ta e="T148" id="Seg_6156" s="T147">старуха-3SG.[NOM]</ta>
            <ta e="T149" id="Seg_6157" s="T148">этот</ta>
            <ta e="T150" id="Seg_6158" s="T149">котел-DIM-3SG-ACC</ta>
            <ta e="T151" id="Seg_6159" s="T150">бросить-CVB.SEQ</ta>
            <ta e="T152" id="Seg_6160" s="T151">после</ta>
            <ta e="T153" id="Seg_6161" s="T152">вот</ta>
            <ta e="T154" id="Seg_6162" s="T153">мерзнуть-CVB.SEQ</ta>
            <ta e="T155" id="Seg_6163" s="T154">умирать-CVB.PURP</ta>
            <ta e="T156" id="Seg_6164" s="T155">делать-PRS.[3SG]</ta>
            <ta e="T157" id="Seg_6165" s="T156">старуха-3SG.[NOM]</ta>
            <ta e="T158" id="Seg_6166" s="T157">юрта-3SG-GEN</ta>
            <ta e="T159" id="Seg_6167" s="T158">верхняя.часть-3SG-DAT/LOC</ta>
            <ta e="T160" id="Seg_6168" s="T159">выйти-CVB.SEQ</ta>
            <ta e="T161" id="Seg_6169" s="T160">искра-DIM-PL.[NOM]</ta>
            <ta e="T162" id="Seg_6170" s="T161">выйти-NMNZ-3PL-DAT/LOC</ta>
            <ta e="T163" id="Seg_6171" s="T162">там</ta>
            <ta e="T164" id="Seg_6172" s="T163">рука-3SG-ACC</ta>
            <ta e="T165" id="Seg_6173" s="T164">согреться-CAUS-PRS.[3SG]</ta>
            <ta e="T166" id="Seg_6174" s="T165">только</ta>
            <ta e="T167" id="Seg_6175" s="T166">муж-3SG-ACC</ta>
            <ta e="T168" id="Seg_6176" s="T167">видеть-PST2-3SG</ta>
            <ta e="T169" id="Seg_6177" s="T168">лошадь-3SG-GEN</ta>
            <ta e="T170" id="Seg_6178" s="T169">промежность-3SG-ACC</ta>
            <ta e="T171" id="Seg_6179" s="T170">взять-CVB.SEQ</ta>
            <ta e="T172" id="Seg_6180" s="T171">после</ta>
            <ta e="T173" id="Seg_6181" s="T172">расставлять-CVB.SEQ</ta>
            <ta e="T174" id="Seg_6182" s="T173">после</ta>
            <ta e="T175" id="Seg_6183" s="T174">жир.от.кишок-VBZ-CVB.SIM</ta>
            <ta e="T176" id="Seg_6184" s="T175">сидеть-PRS.[3SG]</ta>
            <ta e="T177" id="Seg_6185" s="T176">тот</ta>
            <ta e="T178" id="Seg_6186" s="T177">согреться-CAUS-CVB.PURP</ta>
            <ta e="T179" id="Seg_6187" s="T178">делать-PTCP.PRS</ta>
            <ta e="T180" id="Seg_6188" s="T179">старуха-3SG-GEN</ta>
            <ta e="T181" id="Seg_6189" s="T180">рука-3SG-ACC</ta>
            <ta e="T182" id="Seg_6190" s="T181">рожон-INSTR</ta>
            <ta e="T183" id="Seg_6191" s="T182">резать-CVB.SIM</ta>
            <ta e="T184" id="Seg_6192" s="T183">колоть-PRS.[3SG]</ta>
            <ta e="T185" id="Seg_6193" s="T184">тот-3SG-3SG.[NOM]</ta>
            <ta e="T186" id="Seg_6194" s="T185">плакать-CVB.SIM-плакать-CVB.SIM</ta>
            <ta e="T187" id="Seg_6195" s="T186">земля-DAT/LOC</ta>
            <ta e="T188" id="Seg_6196" s="T187">сходить-EMOT-CVB.SEQ</ta>
            <ta e="T189" id="Seg_6197" s="T188">после</ta>
            <ta e="T190" id="Seg_6198" s="T189">дерево-VBZ-PTCP.PRS</ta>
            <ta e="T191" id="Seg_6199" s="T190">место-DIM.[NOM]</ta>
            <ta e="T192" id="Seg_6200" s="T191">к</ta>
            <ta e="T193" id="Seg_6201" s="T192">плакать-CVB.SIM-плакать-CVB.SIM</ta>
            <ta e="T194" id="Seg_6202" s="T193">шагать-CVB.SIM</ta>
            <ta e="T195" id="Seg_6203" s="T194">стоять-PRS.[3SG]</ta>
            <ta e="T196" id="Seg_6204" s="T195">вот</ta>
            <ta e="T197" id="Seg_6205" s="T196">там</ta>
            <ta e="T198" id="Seg_6206" s="T197">лиственница-EP-DIM.[NOM]</ta>
            <ta e="T199" id="Seg_6207" s="T198">дерево.[NOM]</ta>
            <ta e="T200" id="Seg_6208" s="T199">корень-3SG-DAT/LOC</ta>
            <ta e="T201" id="Seg_6209" s="T200">ложиться-CVB.PURP</ta>
            <ta e="T202" id="Seg_6210" s="T201">хотеть-PST2.[3SG]</ta>
            <ta e="T203" id="Seg_6211" s="T202">ложиться-CVB.PURP</ta>
            <ta e="T204" id="Seg_6212" s="T203">хотеть-PST2.[3SG]</ta>
            <ta e="T205" id="Seg_6213" s="T204">холод.[NOM]</ta>
            <ta e="T206" id="Seg_6214" s="T205">дрожать-PTCP.PRS.[NOM]</ta>
            <ta e="T207" id="Seg_6215" s="T206">сердце-3SG-DAT/LOC</ta>
            <ta e="T208" id="Seg_6216" s="T207">входить-PRS.[3SG]</ta>
            <ta e="T209" id="Seg_6217" s="T208">тот</ta>
            <ta e="T210" id="Seg_6218" s="T209">лежать-TEMP-3SG</ta>
            <ta e="T211" id="Seg_6219" s="T210">шум.[NOM]</ta>
            <ta e="T212" id="Seg_6220" s="T211">EMPH</ta>
            <ta e="T213" id="Seg_6221" s="T212">слышаться-EP-PST2.[3SG]</ta>
            <ta e="T214" id="Seg_6222" s="T213">олень.[NOM]</ta>
            <ta e="T215" id="Seg_6223" s="T214">гнать-PTCP.PRS-ABL</ta>
            <ta e="T216" id="Seg_6224" s="T215">олень.[NOM]</ta>
            <ta e="T217" id="Seg_6225" s="T216">гнать-PTCP.PRS.[NOM]</ta>
            <ta e="T218" id="Seg_6226" s="T217">разный</ta>
            <ta e="T219" id="Seg_6227" s="T218">очень</ta>
            <ta e="T220" id="Seg_6228" s="T219">тот.[NOM]</ta>
            <ta e="T221" id="Seg_6229" s="T220">в.сторону</ta>
            <ta e="T222" id="Seg_6230" s="T221">вот</ta>
            <ta e="T223" id="Seg_6231" s="T222">идти-PRS.[3SG]</ta>
            <ta e="T224" id="Seg_6232" s="T223">этот</ta>
            <ta e="T225" id="Seg_6233" s="T224">старуха.[NOM]</ta>
            <ta e="T226" id="Seg_6234" s="T225">тот</ta>
            <ta e="T227" id="Seg_6235" s="T226">идти-CVB.SEQ</ta>
            <ta e="T228" id="Seg_6236" s="T227">хороший.дух.[NOM]</ta>
            <ta e="T229" id="Seg_6237" s="T228">EMPH</ta>
            <ta e="T230" id="Seg_6238" s="T229">луна-ACC</ta>
            <ta e="T231" id="Seg_6239" s="T230">закрывать-PTCP.PRS</ta>
            <ta e="T232" id="Seg_6240" s="T231">густой</ta>
            <ta e="T233" id="Seg_6241" s="T232">туман.[NOM]</ta>
            <ta e="T234" id="Seg_6242" s="T233">олень-DAT/LOC</ta>
            <ta e="T235" id="Seg_6243" s="T234">солнце-ACC</ta>
            <ta e="T236" id="Seg_6244" s="T235">закрывать-PTCP.PRS</ta>
            <ta e="T237" id="Seg_6245" s="T236">легкий.туман.[NOM]</ta>
            <ta e="T238" id="Seg_6246" s="T237">туман.[NOM]</ta>
            <ta e="T239" id="Seg_6247" s="T238">скот-DAT/LOC</ta>
            <ta e="T240" id="Seg_6248" s="T239">доезжать-PRS.[3SG]</ta>
            <ta e="T241" id="Seg_6249" s="T240">этот</ta>
            <ta e="T242" id="Seg_6250" s="T241">старуха-EP-2SG.[NOM]</ta>
            <ta e="T243" id="Seg_6251" s="T242">дом-ACC</ta>
            <ta e="T244" id="Seg_6252" s="T243">искать-CVB.SEQ</ta>
            <ta e="T245" id="Seg_6253" s="T244">этот</ta>
            <ta e="T246" id="Seg_6254" s="T245">олень-ACC</ta>
            <ta e="T247" id="Seg_6255" s="T246">разрезать-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T248" id="Seg_6256" s="T247">два</ta>
            <ta e="T249" id="Seg_6257" s="T248">половина-3SG.[NOM]</ta>
            <ta e="T250" id="Seg_6258" s="T249">истереть-CVB.SEQ</ta>
            <ta e="T251" id="Seg_6259" s="T250">оставаться-PST2.[3SG]</ta>
            <ta e="T252" id="Seg_6260" s="T251">вдруг</ta>
            <ta e="T253" id="Seg_6261" s="T252">долина-ACC</ta>
            <ta e="T254" id="Seg_6262" s="T253">EMPH-полный</ta>
            <ta e="T255" id="Seg_6263" s="T254">чум.[NOM]</ta>
            <ta e="T256" id="Seg_6264" s="T255">дом.[NOM]</ta>
            <ta e="T257" id="Seg_6265" s="T256">строить-EP-PASS/REFL-CVB.SEQ</ta>
            <ta e="T258" id="Seg_6266" s="T257">стоять-PRS.[3SG]</ta>
            <ta e="T259" id="Seg_6267" s="T258">большой</ta>
            <ta e="T260" id="Seg_6268" s="T259">очень</ta>
            <ta e="T261" id="Seg_6269" s="T260">чум.[NOM]</ta>
            <ta e="T262" id="Seg_6270" s="T261">приходить-PST1-3SG</ta>
            <ta e="T263" id="Seg_6271" s="T262">этот</ta>
            <ta e="T264" id="Seg_6272" s="T263">чум-DAT/LOC</ta>
            <ta e="T265" id="Seg_6273" s="T264">здесь</ta>
            <ta e="T266" id="Seg_6274" s="T265">вот</ta>
            <ta e="T267" id="Seg_6275" s="T266">сколько</ta>
            <ta e="T268" id="Seg_6276" s="T267">десять</ta>
            <ta e="T269" id="Seg_6277" s="T268">MOD</ta>
            <ta e="T270" id="Seg_6278" s="T269">резать-CVB.SIM</ta>
            <ta e="T271" id="Seg_6279" s="T270">караван-3SG.[NOM]</ta>
            <ta e="T272" id="Seg_6280" s="T271">балок-3SG.[NOM]</ta>
            <ta e="T273" id="Seg_6281" s="T272">растянуться-CVB.SEQ</ta>
            <ta e="T274" id="Seg_6282" s="T273">стоять-PRS.[3SG]</ta>
            <ta e="T275" id="Seg_6283" s="T274">так</ta>
            <ta e="T276" id="Seg_6284" s="T275">делать-CVB.SEQ</ta>
            <ta e="T277" id="Seg_6285" s="T276">этот</ta>
            <ta e="T278" id="Seg_6286" s="T277">старуха.[NOM]</ta>
            <ta e="T279" id="Seg_6287" s="T278">этот</ta>
            <ta e="T280" id="Seg_6288" s="T279">чум-ACC</ta>
            <ta e="T281" id="Seg_6289" s="T280">открывать-CVB.SIM</ta>
            <ta e="T282" id="Seg_6290" s="T281">бить-EP-PST2.[3SG]</ta>
            <ta e="T283" id="Seg_6291" s="T282">смотреть-CVB.SEQ</ta>
            <ta e="T284" id="Seg_6292" s="T283">видеть-PST2-3SG</ta>
            <ta e="T285" id="Seg_6293" s="T284">EMPH-пустой.[NOM]</ta>
            <ta e="T286" id="Seg_6294" s="T285">человек-3SG.[NOM]</ta>
            <ta e="T287" id="Seg_6295" s="T286">NEG.EX</ta>
            <ta e="T288" id="Seg_6296" s="T287">каждый</ta>
            <ta e="T289" id="Seg_6297" s="T288">котел.[NOM]</ta>
            <ta e="T290" id="Seg_6298" s="T289">котел-VBZ-CVB.SEQ</ta>
            <ta e="T291" id="Seg_6299" s="T290">стоять-PRS.[3SG]</ta>
            <ta e="T292" id="Seg_6300" s="T291">каждый</ta>
            <ta e="T293" id="Seg_6301" s="T292">пища.[NOM]</ta>
            <ta e="T294" id="Seg_6302" s="T293">пища-VBZ-CVB.SEQ</ta>
            <ta e="T295" id="Seg_6303" s="T294">стоять-PTCP.PRS.[3SG]</ta>
            <ta e="T296" id="Seg_6304" s="T295">камень.[NOM]</ta>
            <ta e="T297" id="Seg_6305" s="T296">кровать-PL.[NOM]</ta>
            <ta e="T298" id="Seg_6306" s="T297">целый-SIM</ta>
            <ta e="T299" id="Seg_6307" s="T298">такой</ta>
            <ta e="T300" id="Seg_6308" s="T299">снаряжение-PROPR</ta>
            <ta e="T301" id="Seg_6309" s="T300">чум.[NOM]</ta>
            <ta e="T302" id="Seg_6310" s="T301">стоять-PRS.[3SG]</ta>
            <ta e="T303" id="Seg_6311" s="T302">тот</ta>
            <ta e="T304" id="Seg_6312" s="T303">старуха.[NOM]</ta>
            <ta e="T305" id="Seg_6313" s="T304">входить-CVB.SEQ</ta>
            <ta e="T306" id="Seg_6314" s="T305">есть-NEG.PTCP.PST-3SG-ACC</ta>
            <ta e="T307" id="Seg_6315" s="T306">есть-PRS.[3SG]</ta>
            <ta e="T308" id="Seg_6316" s="T307">есть-NEG.PTCP.PST-3SG-ACC</ta>
            <ta e="T309" id="Seg_6317" s="T308">есть-PRS.[3SG]</ta>
            <ta e="T310" id="Seg_6318" s="T309">русский</ta>
            <ta e="T311" id="Seg_6319" s="T310">да</ta>
            <ta e="T312" id="Seg_6320" s="T311">пища-3SG.[NOM]</ta>
            <ta e="T313" id="Seg_6321" s="T312">много</ta>
            <ta e="T314" id="Seg_6322" s="T313">долганский</ta>
            <ta e="T315" id="Seg_6323" s="T314">да</ta>
            <ta e="T316" id="Seg_6324" s="T315">пища-3SG.[NOM]</ta>
            <ta e="T317" id="Seg_6325" s="T316">много</ta>
            <ta e="T318" id="Seg_6326" s="T317">этот</ta>
            <ta e="T319" id="Seg_6327" s="T318">старуха.[NOM]</ta>
            <ta e="T320" id="Seg_6328" s="T319">мыться-CVB.SEQ-причесаться-CVB.SEQ</ta>
            <ta e="T321" id="Seg_6329" s="T320">после</ta>
            <ta e="T322" id="Seg_6330" s="T321">рысь.[NOM]-соболь.[NOM]</ta>
            <ta e="T323" id="Seg_6331" s="T322">одежда-ACC</ta>
            <ta e="T324" id="Seg_6332" s="T323">одеваться-CVB.SEQ</ta>
            <ta e="T325" id="Seg_6333" s="T324">бросать-PST2.[3SG]</ta>
            <ta e="T326" id="Seg_6334" s="T325">этот.[NOM]</ta>
            <ta e="T327" id="Seg_6335" s="T326">пока</ta>
            <ta e="T328" id="Seg_6336" s="T327">что.[NOM]</ta>
            <ta e="T329" id="Seg_6337" s="T328">NEG</ta>
            <ta e="T330" id="Seg_6338" s="T329">человек-3SG.[NOM]</ta>
            <ta e="T331" id="Seg_6339" s="T330">замечать-PASS/REFL-EP-NEG.[3SG]</ta>
            <ta e="T332" id="Seg_6340" s="T331">три</ta>
            <ta e="T333" id="Seg_6341" s="T332">день-ACC</ta>
            <ta e="T334" id="Seg_6342" s="T333">во.время</ta>
            <ta e="T335" id="Seg_6343" s="T334">что.[NOM]</ta>
            <ta e="T336" id="Seg_6344" s="T335">NEG</ta>
            <ta e="T337" id="Seg_6345" s="T336">человек-3SG.[NOM]</ta>
            <ta e="T338" id="Seg_6346" s="T337">замечать-PASS/REFL-EP-NEG.[3SG]</ta>
            <ta e="T339" id="Seg_6347" s="T338">только</ta>
            <ta e="T340" id="Seg_6348" s="T339">вечер.[NOM]</ta>
            <ta e="T341" id="Seg_6349" s="T340">быть-FUT.[3SG]</ta>
            <ta e="T342" id="Seg_6350" s="T341">да</ta>
            <ta e="T343" id="Seg_6351" s="T342">дымоход-3SG-INSTR</ta>
            <ta e="T344" id="Seg_6352" s="T343">старый</ta>
            <ta e="T345" id="Seg_6353" s="T344">человек.[NOM]</ta>
            <ta e="T346" id="Seg_6354" s="T345">голова-PL-3SG.[NOM]</ta>
            <ta e="T347" id="Seg_6355" s="T346">падать-PRS-3PL</ta>
            <ta e="T348" id="Seg_6356" s="T347">тот-PL-ACC</ta>
            <ta e="T349" id="Seg_6357" s="T348">протыкать-EP-IMP.2PL-резать-EP-IMP.2PL</ta>
            <ta e="T350" id="Seg_6358" s="T349">протыкать-EP-CAUS-CVB.SIM</ta>
            <ta e="T351" id="Seg_6359" s="T350">идти.[IMP.2SG]</ta>
            <ta e="T352" id="Seg_6360" s="T351">говорить-CVB.SIM-говорить-CVB.SIM</ta>
            <ta e="T353" id="Seg_6361" s="T352">протыкать-EP-CAUS-CVB.SIM</ta>
            <ta e="T354" id="Seg_6362" s="T353">идти-PRS.[3SG]</ta>
            <ta e="T355" id="Seg_6363" s="T354">тот-PL-3SG.[NOM]</ta>
            <ta e="T356" id="Seg_6364" s="T355">земля.[NOM]</ta>
            <ta e="T357" id="Seg_6365" s="T356">к</ta>
            <ta e="T358" id="Seg_6366" s="T357">стесняться-CVB.SEQ</ta>
            <ta e="T359" id="Seg_6367" s="T358">идти-PRS-3PL</ta>
            <ta e="T360" id="Seg_6368" s="T359">старуха.[NOM]</ta>
            <ta e="T361" id="Seg_6369" s="T360">сколько</ta>
            <ta e="T362" id="Seg_6370" s="T361">десять</ta>
            <ta e="T363" id="Seg_6371" s="T362">так.долго</ta>
            <ta e="T364" id="Seg_6372" s="T363">жить-PST2.[3SG]</ta>
            <ta e="T365" id="Seg_6373" s="T364">олень-3SG.[NOM]</ta>
            <ta e="T366" id="Seg_6374" s="T365">куда</ta>
            <ta e="T367" id="Seg_6375" s="T366">NEG</ta>
            <ta e="T368" id="Seg_6376" s="T367">двигаться-NEG.[3SG]</ta>
            <ta e="T369" id="Seg_6377" s="T368">лошадь-PL-3SG.[NOM]</ta>
            <ta e="T370" id="Seg_6378" s="T369">этот.EMPH.[NOM]</ta>
            <ta e="T371" id="Seg_6379" s="T370">подобно</ta>
            <ta e="T372" id="Seg_6380" s="T371">лежать-PRS-3PL</ta>
            <ta e="T373" id="Seg_6381" s="T372">рыба-3PL.[NOM]</ta>
            <ta e="T374" id="Seg_6382" s="T373">этот.EMPH</ta>
            <ta e="T375" id="Seg_6383" s="T374">охотить-EP-REFL-PTCP.PST.[NOM]</ta>
            <ta e="T376" id="Seg_6384" s="T375">как</ta>
            <ta e="T377" id="Seg_6385" s="T376">идти-PRS.[3SG]</ta>
            <ta e="T378" id="Seg_6386" s="T377">старуха.[NOM]</ta>
            <ta e="T379" id="Seg_6387" s="T378">так</ta>
            <ta e="T380" id="Seg_6388" s="T379">быть.богатым-CVB.SEQ-быть.богатым-CVB.SEQ</ta>
            <ta e="T381" id="Seg_6389" s="T380">EMPH-одиноко</ta>
            <ta e="T382" id="Seg_6390" s="T381">жить-PRS.[3SG]</ta>
            <ta e="T383" id="Seg_6391" s="T382">так</ta>
            <ta e="T384" id="Seg_6392" s="T383">делать-CVB.SEQ</ta>
            <ta e="T385" id="Seg_6393" s="T384">старуха.[NOM]</ta>
            <ta e="T386" id="Seg_6394" s="T385">мысль-3SG.[NOM]</ta>
            <ta e="T387" id="Seg_6395" s="T386">приходить-PST2.[3SG]</ta>
            <ta e="T388" id="Seg_6396" s="T387">ребенок-PL.[NOM]</ta>
            <ta e="T389" id="Seg_6397" s="T388">1SG.[NOM]</ta>
            <ta e="T390" id="Seg_6398" s="T389">сидеть-PTCP.FUT-COMP</ta>
            <ta e="T391" id="Seg_6399" s="T390">старик-EP-1SG.[NOM]</ta>
            <ta e="T392" id="Seg_6400" s="T391">далекий-3SG-DAT/LOC</ta>
            <ta e="T393" id="Seg_6401" s="T392">узнавать-RECP/COLL-CVB.SIM</ta>
            <ta e="T394" id="Seg_6402" s="T393">идти-PTCP.FUT</ta>
            <ta e="T395" id="Seg_6403" s="T394">есть-EP-1SG</ta>
            <ta e="T396" id="Seg_6404" s="T395">три-ORD</ta>
            <ta e="T397" id="Seg_6405" s="T396">год-3SG-DAT/LOC</ta>
            <ta e="T398" id="Seg_6406" s="T397">старик-3SG-DAT/LOC</ta>
            <ta e="T399" id="Seg_6407" s="T398">идти-CVB.PURP</ta>
            <ta e="T400" id="Seg_6408" s="T399">одеваться-PST1-3SG</ta>
            <ta e="T401" id="Seg_6409" s="T400">хлеб-ACC</ta>
            <ta e="T402" id="Seg_6410" s="T401">резать-FREQ-CVB.SEQ</ta>
            <ta e="T403" id="Seg_6411" s="T402">после</ta>
            <ta e="T404" id="Seg_6412" s="T403">взять-PST1-3SG</ta>
            <ta e="T405" id="Seg_6413" s="T404">тот.[NOM]</ta>
            <ta e="T406" id="Seg_6414" s="T405">делать-CVB.SEQ</ta>
            <ta e="T407" id="Seg_6415" s="T406">после</ta>
            <ta e="T408" id="Seg_6416" s="T407">дикий.олень.[NOM]</ta>
            <ta e="T409" id="Seg_6417" s="T408">сало-3SG-ACC</ta>
            <ta e="T410" id="Seg_6418" s="T409">приносить-CAP.[3SG]</ta>
            <ta e="T411" id="Seg_6419" s="T410">так</ta>
            <ta e="T412" id="Seg_6420" s="T411">делать-CVB.SEQ</ta>
            <ta e="T413" id="Seg_6421" s="T412">после</ta>
            <ta e="T414" id="Seg_6422" s="T413">старик-3SG-DAT/LOC</ta>
            <ta e="T415" id="Seg_6423" s="T414">идти-PST1-3SG</ta>
            <ta e="T416" id="Seg_6424" s="T415">этот</ta>
            <ta e="T417" id="Seg_6425" s="T416">идти-CVB.SEQ</ta>
            <ta e="T418" id="Seg_6426" s="T417">идти-CVB.SEQ</ta>
            <ta e="T419" id="Seg_6427" s="T418">старик-3SG-GEN</ta>
            <ta e="T420" id="Seg_6428" s="T419">землянка-3SG-DAT/LOC</ta>
            <ta e="T421" id="Seg_6429" s="T420">доезжать-PST2.[3SG]</ta>
            <ta e="T422" id="Seg_6430" s="T421">только</ta>
            <ta e="T423" id="Seg_6431" s="T422">что-INTNS.[NOM]</ta>
            <ta e="T424" id="Seg_6432" s="T423">MOD</ta>
            <ta e="T425" id="Seg_6433" s="T424">NEG.EX</ta>
            <ta e="T426" id="Seg_6434" s="T425">целый-SIM</ta>
            <ta e="T427" id="Seg_6435" s="T426">полностью</ta>
            <ta e="T428" id="Seg_6436" s="T427">замести-EP-PASS/REFL-CVB.SEQ</ta>
            <ta e="T429" id="Seg_6437" s="T428">рухнуть-CVB.SEQ</ta>
            <ta e="T430" id="Seg_6438" s="T429">оставаться-PST2.[3SG]</ta>
            <ta e="T431" id="Seg_6439" s="T430">тот.[NOM]</ta>
            <ta e="T432" id="Seg_6440" s="T431">да</ta>
            <ta e="T433" id="Seg_6441" s="T432">быть-COND.[3SG]</ta>
            <ta e="T434" id="Seg_6442" s="T433">дымоход-3SG-DAT/LOC</ta>
            <ta e="T435" id="Seg_6443" s="T434">лезть-EP-PST2.[3SG]</ta>
            <ta e="T436" id="Seg_6444" s="T435">только</ta>
            <ta e="T437" id="Seg_6445" s="T436">очаг-3SG-ACC</ta>
            <ta e="T438" id="Seg_6446" s="T437">вокруг</ta>
            <ta e="T439" id="Seg_6447" s="T438">три</ta>
            <ta e="T440" id="Seg_6448" s="T439">шерсть-PROPR</ta>
            <ta e="T441" id="Seg_6449" s="T440">червь.[NOM]</ta>
            <ta e="T442" id="Seg_6450" s="T441">становиться-CVB.SEQ</ta>
            <ta e="T443" id="Seg_6451" s="T442">безобразничать-CVB.SIM</ta>
            <ta e="T444" id="Seg_6452" s="T443">лежать-PRS.[3SG]</ta>
            <ta e="T445" id="Seg_6453" s="T444">лошадь-3SG-GEN</ta>
            <ta e="T446" id="Seg_6454" s="T445">черный</ta>
            <ta e="T447" id="Seg_6455" s="T446">копыто-3SG-ACC</ta>
            <ta e="T448" id="Seg_6456" s="T447">с</ta>
            <ta e="T449" id="Seg_6457" s="T448">кончать-MED-EP-NMNZ-3SG-ACC</ta>
            <ta e="T450" id="Seg_6458" s="T449">только</ta>
            <ta e="T451" id="Seg_6459" s="T450">быть.лишным-CAUS-PST2.[3SG]</ta>
            <ta e="T452" id="Seg_6460" s="T451">лошадь-3SG-GEN</ta>
            <ta e="T453" id="Seg_6461" s="T452">собственный-3SG-ACC</ta>
            <ta e="T454" id="Seg_6462" s="T453">недавно</ta>
            <ta e="T455" id="Seg_6463" s="T454">превращаться-PTCP.PST</ta>
            <ta e="T456" id="Seg_6464" s="T455">остатки-3SG.[NOM]</ta>
            <ta e="T457" id="Seg_6465" s="T456">говорят</ta>
            <ta e="T458" id="Seg_6466" s="T457">тот.[NOM]</ta>
            <ta e="T459" id="Seg_6467" s="T458">для</ta>
            <ta e="T460" id="Seg_6468" s="T459">умирать-EP-NEG-CVB.PURP</ta>
            <ta e="T461" id="Seg_6469" s="T460">превращаться-PST2-3SG</ta>
            <ta e="T462" id="Seg_6470" s="T461">говорят</ta>
            <ta e="T463" id="Seg_6471" s="T462">вот</ta>
            <ta e="T464" id="Seg_6472" s="T463">тот-ACC</ta>
            <ta e="T465" id="Seg_6473" s="T464">этот</ta>
            <ta e="T466" id="Seg_6474" s="T465">хлеб-EP-INSTR</ta>
            <ta e="T467" id="Seg_6475" s="T466">бросать-PST2.[3SG]</ta>
            <ta e="T468" id="Seg_6476" s="T467">этот-ACC</ta>
            <ta e="T469" id="Seg_6477" s="T468">гнуть-EP-NMNZ.[NOM]</ta>
            <ta e="T470" id="Seg_6478" s="T469">делать-PST2.[3SG]</ta>
            <ta e="T471" id="Seg_6479" s="T470">да</ta>
            <ta e="T472" id="Seg_6480" s="T471">говорить-PST2.[3SG]</ta>
            <ta e="T473" id="Seg_6481" s="T472">этот.[NOM]</ta>
            <ta e="T474" id="Seg_6482" s="T473">очаг-EP-1SG.[NOM]</ta>
            <ta e="T475" id="Seg_6483" s="T474">глина-3SG.[NOM]</ta>
            <ta e="T476" id="Seg_6484" s="T475">лежать-CAUS-EP-NEG-CVB.PURP</ta>
            <ta e="T477" id="Seg_6485" s="T476">делать-PST2.[3SG]</ta>
            <ta e="T478" id="Seg_6486" s="T477">говорить-PRS.[3SG]</ta>
            <ta e="T479" id="Seg_6487" s="T478">сало-INSTR</ta>
            <ta e="T480" id="Seg_6488" s="T479">бросать-PST2.[3SG]</ta>
            <ta e="T481" id="Seg_6489" s="T480">ой</ta>
            <ta e="T482" id="Seg_6490" s="T481">ком-PL.[NOM]</ta>
            <ta e="T483" id="Seg_6491" s="T482">падать-CVB.SEQ-3PL</ta>
            <ta e="T484" id="Seg_6492" s="T483">лежать-CAUS-EP-NEG-CVB.PURP</ta>
            <ta e="T485" id="Seg_6493" s="T484">делать-PST1-3PL</ta>
            <ta e="T486" id="Seg_6494" s="T485">опять</ta>
            <ta e="T487" id="Seg_6495" s="T486">бросать-PST2.[3SG]</ta>
            <ta e="T488" id="Seg_6496" s="T487">этот-ACC</ta>
            <ta e="T489" id="Seg_6497" s="T488">кусать-CVB.SEQ</ta>
            <ta e="T490" id="Seg_6498" s="T489">попробовать-PST2.[3SG]</ta>
            <ta e="T491" id="Seg_6499" s="T490">сало.[NOM]</ta>
            <ta e="T492" id="Seg_6500" s="T491">быть-PST2.[3SG]</ta>
            <ta e="T493" id="Seg_6501" s="T492">тот-ACC</ta>
            <ta e="T494" id="Seg_6502" s="T493">небо.[NOM]</ta>
            <ta e="T495" id="Seg_6503" s="T494">небо.[NOM]</ta>
            <ta e="T496" id="Seg_6504" s="T495">немного-DIM-INTNS</ta>
            <ta e="T497" id="Seg_6505" s="T496">говорить-PST2.[3SG]</ta>
            <ta e="T498" id="Seg_6506" s="T497">эй</ta>
            <ta e="T499" id="Seg_6507" s="T498">старик.[NOM]</ta>
            <ta e="T500" id="Seg_6508" s="T499">дверь-2SG-ACC</ta>
            <ta e="T501" id="Seg_6509" s="T500">открывать.[IMP.2SG]</ta>
            <ta e="T502" id="Seg_6510" s="T501">говорить-PST2.[3SG]</ta>
            <ta e="T503" id="Seg_6511" s="T502">старуха-3SG.[NOM]</ta>
            <ta e="T504" id="Seg_6512" s="T503">дверь-3SG-ACC</ta>
            <ta e="T505" id="Seg_6513" s="T504">открывать-CVB.SEQ</ta>
            <ta e="T506" id="Seg_6514" s="T505">остатки-VBZ-PST2.[3SG]</ta>
            <ta e="T507" id="Seg_6515" s="T506">старуха.[NOM]</ta>
            <ta e="T508" id="Seg_6516" s="T507">есть-EP-CAUS-CVB.SEQ</ta>
            <ta e="T509" id="Seg_6517" s="T508">после</ta>
            <ta e="T510" id="Seg_6518" s="T509">дом-3SG-DAT/LOC</ta>
            <ta e="T511" id="Seg_6519" s="T510">приносить-PST2.[3SG]</ta>
            <ta e="T512" id="Seg_6520" s="T511">этот</ta>
            <ta e="T513" id="Seg_6521" s="T512">дом-3SG-DAT/LOC</ta>
            <ta e="T514" id="Seg_6522" s="T513">носить-CVB.SEQ</ta>
            <ta e="T515" id="Seg_6523" s="T514">обмыть-CVB.SEQ-причесать-CVB.SEQ</ta>
            <ta e="T516" id="Seg_6524" s="T515">после</ta>
            <ta e="T517" id="Seg_6525" s="T516">одеваться-EP-NEG.PTCP.PST</ta>
            <ta e="T518" id="Seg_6526" s="T517">одежда-ACC</ta>
            <ta e="T519" id="Seg_6527" s="T518">одеваться-CAUS-CVB.SEQ</ta>
            <ta e="T520" id="Seg_6528" s="T519">после</ta>
            <ta e="T521" id="Seg_6529" s="T520">стекло.[NOM]</ta>
            <ta e="T522" id="Seg_6530" s="T521">кровать-DAT/LOC</ta>
            <ta e="T523" id="Seg_6531" s="T522">посадить-CVB.SEQ</ta>
            <ta e="T524" id="Seg_6532" s="T523">бросать-PST2.[3SG]</ta>
            <ta e="T525" id="Seg_6533" s="T524">тот</ta>
            <ta e="T526" id="Seg_6534" s="T525">делать-CVB.SEQ</ta>
            <ta e="T527" id="Seg_6535" s="T526">после</ta>
            <ta e="T528" id="Seg_6536" s="T527">чай.[NOM]</ta>
            <ta e="T529" id="Seg_6537" s="T528">пить-EP-CAUS.[CAUS]-PST2.[3SG]</ta>
            <ta e="T530" id="Seg_6538" s="T529">потом</ta>
            <ta e="T531" id="Seg_6539" s="T530">мясо.[NOM]</ta>
            <ta e="T532" id="Seg_6540" s="T531">пища.[NOM]</ta>
            <ta e="T533" id="Seg_6541" s="T532">выкладывать-PST2.[3SG]</ta>
            <ta e="T534" id="Seg_6542" s="T533">сало-PROPR</ta>
            <ta e="T535" id="Seg_6543" s="T534">суп-ACC</ta>
            <ta e="T536" id="Seg_6544" s="T535">миска-DAT/LOC</ta>
            <ta e="T537" id="Seg_6545" s="T536">выкладывать-PST2.[3SG]</ta>
            <ta e="T538" id="Seg_6546" s="T537">так</ta>
            <ta e="T539" id="Seg_6547" s="T538">делать-CVB.SEQ</ta>
            <ta e="T540" id="Seg_6548" s="T539">золото.[NOM]</ta>
            <ta e="T541" id="Seg_6549" s="T540">ложка.[NOM]</ta>
            <ta e="T542" id="Seg_6550" s="T541">вилка.[NOM]</ta>
            <ta e="T543" id="Seg_6551" s="T542">класть-PST2.[3SG]</ta>
            <ta e="T544" id="Seg_6552" s="T543">только</ta>
            <ta e="T545" id="Seg_6553" s="T544">ложка-3SG-INSTR</ta>
            <ta e="T546" id="Seg_6554" s="T545">пить-CVB.PURP</ta>
            <ta e="T547" id="Seg_6555" s="T546">хотеть-PST2.[3SG]</ta>
            <ta e="T548" id="Seg_6556" s="T547">эй</ta>
            <ta e="T549" id="Seg_6557" s="T548">этот-этот</ta>
            <ta e="T550" id="Seg_6558" s="T549">ложка-1SG.[NOM]</ta>
            <ta e="T551" id="Seg_6559" s="T550">небольшой.[NOM]</ta>
            <ta e="T552" id="Seg_6560" s="T551">большой</ta>
            <ta e="T553" id="Seg_6561" s="T552">поварешка-PART</ta>
            <ta e="T554" id="Seg_6562" s="T553">Q</ta>
            <ta e="T555" id="Seg_6563" s="T554">ковш-PART</ta>
            <ta e="T556" id="Seg_6564" s="T555">Q</ta>
            <ta e="T557" id="Seg_6565" s="T556">принести.[IMP.2SG]</ta>
            <ta e="T558" id="Seg_6566" s="T557">говорить-PST2.[3SG]</ta>
            <ta e="T559" id="Seg_6567" s="T558">давнишний-3SG.[NOM]</ta>
            <ta e="T560" id="Seg_6568" s="T559">ковш.[NOM]</ta>
            <ta e="T561" id="Seg_6569" s="T560">очень-3SG</ta>
            <ta e="T562" id="Seg_6570" s="T561">подать-CVB.SEQ</ta>
            <ta e="T563" id="Seg_6571" s="T562">бросать-PST2.[3SG]</ta>
            <ta e="T564" id="Seg_6572" s="T563">сало-PROPR</ta>
            <ta e="T565" id="Seg_6573" s="T564">суп-ACC</ta>
            <ta e="T566" id="Seg_6574" s="T565">этот-3SG-INSTR</ta>
            <ta e="T567" id="Seg_6575" s="T566">уп</ta>
            <ta e="T568" id="Seg_6576" s="T567">делать-PST2.[3SG]</ta>
            <ta e="T569" id="Seg_6577" s="T568">этот-DAT/LOC</ta>
            <ta e="T570" id="Seg_6578" s="T569">вот</ta>
            <ta e="T571" id="Seg_6579" s="T570">поперхнуться-PRS.[3SG]</ta>
            <ta e="T572" id="Seg_6580" s="T571">EMPH</ta>
            <ta e="T573" id="Seg_6581" s="T572">вот</ta>
            <ta e="T574" id="Seg_6582" s="T573">потом</ta>
            <ta e="T575" id="Seg_6583" s="T574">поперхнуться-PRS-1SG</ta>
            <ta e="T576" id="Seg_6584" s="T575">говорить-CVB.SEQ</ta>
            <ta e="T577" id="Seg_6585" s="T576">вот</ta>
            <ta e="T578" id="Seg_6586" s="T577">громкий.звук-VBZ-PRS.[3SG]</ta>
            <ta e="T579" id="Seg_6587" s="T578">EMPH</ta>
            <ta e="T580" id="Seg_6588" s="T579">стекло.[NOM]</ta>
            <ta e="T581" id="Seg_6589" s="T580">подстилка-3SG-ACC</ta>
            <ta e="T582" id="Seg_6590" s="T581">сквозь</ta>
            <ta e="T583" id="Seg_6591" s="T582">громкий.звук-VBZ-PRS.[3SG]</ta>
            <ta e="T584" id="Seg_6592" s="T583">да</ta>
            <ta e="T585" id="Seg_6593" s="T584">так</ta>
            <ta e="T586" id="Seg_6594" s="T585">три</ta>
            <ta e="T587" id="Seg_6595" s="T586">слой.льда.[NOM]</ta>
            <ta e="T588" id="Seg_6596" s="T587">земля-ACC</ta>
            <ta e="T589" id="Seg_6597" s="T588">насквозь</ta>
            <ta e="T590" id="Seg_6598" s="T589">падать-CVB.SEQ</ta>
            <ta e="T591" id="Seg_6599" s="T590">оставаться-PRS.[3SG]</ta>
            <ta e="T592" id="Seg_6600" s="T591">старуха-3SG.[NOM]</ta>
            <ta e="T593" id="Seg_6601" s="T592">что.[NOM]</ta>
            <ta e="T594" id="Seg_6602" s="T593">быть-FUT-3SG=Q</ta>
            <ta e="T595" id="Seg_6603" s="T594">как</ta>
            <ta e="T596" id="Seg_6604" s="T595">жить-PST2-3SG</ta>
            <ta e="T597" id="Seg_6605" s="T596">да</ta>
            <ta e="T598" id="Seg_6606" s="T597">так</ta>
            <ta e="T601" id="Seg_6607" s="T598">всегда</ta>
            <ta e="T599" id="Seg_6608" s="T601">всегда</ta>
            <ta e="T600" id="Seg_6609" s="T599">жить-PRS.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_6610" s="T0">n-n&gt;adj</ta>
            <ta e="T2" id="Seg_6611" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_6612" s="T2">v-v:tense-v:pred.pn</ta>
            <ta e="T4" id="Seg_6613" s="T3">ptcl</ta>
            <ta e="T5" id="Seg_6614" s="T4">v-v:(ins)-v&gt;v-v:ptcp</ta>
            <ta e="T6" id="Seg_6615" s="T5">n-n:(poss)</ta>
            <ta e="T7" id="Seg_6616" s="T6">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T8" id="Seg_6617" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_6618" s="T8">v-v:mood-v:temp.pn</ta>
            <ta e="T10" id="Seg_6619" s="T9">n-n:(poss)-n:case</ta>
            <ta e="T11" id="Seg_6620" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_6621" s="T11">v-v:tense-v:pred.pn</ta>
            <ta e="T13" id="Seg_6622" s="T12">adv</ta>
            <ta e="T14" id="Seg_6623" s="T13">adj-n:(num)-n:case</ta>
            <ta e="T15" id="Seg_6624" s="T14">adv</ta>
            <ta e="T16" id="Seg_6625" s="T15">v-v:mood-v:temp.pn</ta>
            <ta e="T17" id="Seg_6626" s="T16">n-n:(poss)-n:case</ta>
            <ta e="T18" id="Seg_6627" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_6628" s="T18">v-v:tense-v:pred.pn</ta>
            <ta e="T20" id="Seg_6629" s="T19">que-pro:(poss)</ta>
            <ta e="T21" id="Seg_6630" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_6631" s="T21">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T23" id="Seg_6632" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_6633" s="T23">n-n:(poss)-n:case</ta>
            <ta e="T25" id="Seg_6634" s="T24">v-v:cvb-v:poss.pn</ta>
            <ta e="T26" id="Seg_6635" s="T25">adj&gt;adj-adj</ta>
            <ta e="T27" id="Seg_6636" s="T26">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T28" id="Seg_6637" s="T27">adv</ta>
            <ta e="T29" id="Seg_6638" s="T28">v-v:ptcp</ta>
            <ta e="T30" id="Seg_6639" s="T29">n-n:(poss)-n:case</ta>
            <ta e="T31" id="Seg_6640" s="T30">cardnum</ta>
            <ta e="T32" id="Seg_6641" s="T31">n-n&gt;adj</ta>
            <ta e="T33" id="Seg_6642" s="T32">n-n&gt;n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T34" id="Seg_6643" s="T33">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T35" id="Seg_6644" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_6645" s="T35">n-n:(poss)-n:case</ta>
            <ta e="T37" id="Seg_6646" s="T36">v-v:(ins)-v&gt;v-v:mood-v:temp.pn</ta>
            <ta e="T38" id="Seg_6647" s="T37">dempro-pro:case</ta>
            <ta e="T39" id="Seg_6648" s="T38">v-v:tense-v:pred.pn</ta>
            <ta e="T40" id="Seg_6649" s="T39">n-n:case</ta>
            <ta e="T41" id="Seg_6650" s="T40">v-v:cvb</ta>
            <ta e="T42" id="Seg_6651" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_6652" s="T42">v-v:cvb</ta>
            <ta e="T44" id="Seg_6653" s="T43">v-v:tense-v:pred.pn</ta>
            <ta e="T45" id="Seg_6654" s="T44">v-v:cvb</ta>
            <ta e="T46" id="Seg_6655" s="T45">v-v:tense-v:pred.pn</ta>
            <ta e="T47" id="Seg_6656" s="T46">adv</ta>
            <ta e="T48" id="Seg_6657" s="T47">n-n:(poss)-n:case</ta>
            <ta e="T49" id="Seg_6658" s="T48">adv</ta>
            <ta e="T50" id="Seg_6659" s="T49">v-v:tense-v:pred.pn</ta>
            <ta e="T51" id="Seg_6660" s="T50">n-n:poss-n:case</ta>
            <ta e="T52" id="Seg_6661" s="T51">ptcl</ta>
            <ta e="T53" id="Seg_6662" s="T52">v-v:cvb</ta>
            <ta e="T54" id="Seg_6663" s="T53">v-v:tense-v:pred.pn</ta>
            <ta e="T55" id="Seg_6664" s="T54">interj</ta>
            <ta e="T56" id="Seg_6665" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_6666" s="T56">v-v:tense-v:pred.pn</ta>
            <ta e="T58" id="Seg_6667" s="T57">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T59" id="Seg_6668" s="T58">adj</ta>
            <ta e="T60" id="Seg_6669" s="T59">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T61" id="Seg_6670" s="T60">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T62" id="Seg_6671" s="T61">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T63" id="Seg_6672" s="T62">ptcl</ta>
            <ta e="T64" id="Seg_6673" s="T63">v-v:cvb</ta>
            <ta e="T65" id="Seg_6674" s="T64">v-v:mood-v:temp.pn</ta>
            <ta e="T66" id="Seg_6675" s="T65">n-n:poss-n:case</ta>
            <ta e="T67" id="Seg_6676" s="T66">v-v:tense-v:poss.pn</ta>
            <ta e="T68" id="Seg_6677" s="T67">dempro-pro:case</ta>
            <ta e="T69" id="Seg_6678" s="T68">n-n:case</ta>
            <ta e="T70" id="Seg_6679" s="T69">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T71" id="Seg_6680" s="T70">n-n:(poss)-n:case</ta>
            <ta e="T72" id="Seg_6681" s="T71">v-v:tense-v:pred.pn</ta>
            <ta e="T73" id="Seg_6682" s="T72">v-v:mood-v:pred.pn</ta>
            <ta e="T74" id="Seg_6683" s="T73">ptcl</ta>
            <ta e="T75" id="Seg_6684" s="T74">v-v:tense-v:poss.pn</ta>
            <ta e="T76" id="Seg_6685" s="T75">n-n:(poss)-n:case</ta>
            <ta e="T77" id="Seg_6686" s="T76">v-v:tense-v:pred.pn</ta>
            <ta e="T78" id="Seg_6687" s="T77">conj</ta>
            <ta e="T79" id="Seg_6688" s="T78">v-v:tense-v:pred.pn</ta>
            <ta e="T80" id="Seg_6689" s="T79">n-n:(poss)-n:case</ta>
            <ta e="T81" id="Seg_6690" s="T80">v-v:tense-v:pred.pn</ta>
            <ta e="T82" id="Seg_6691" s="T81">ptcl</ta>
            <ta e="T83" id="Seg_6692" s="T82">adj-n:case</ta>
            <ta e="T84" id="Seg_6693" s="T83">ptcl</ta>
            <ta e="T85" id="Seg_6694" s="T84">adj-n:case</ta>
            <ta e="T86" id="Seg_6695" s="T85">ptcl</ta>
            <ta e="T87" id="Seg_6696" s="T86">n-n:(poss)-n:case</ta>
            <ta e="T88" id="Seg_6697" s="T87">v-v:tense-v:pred.pn</ta>
            <ta e="T89" id="Seg_6698" s="T88">v-v:cvb</ta>
            <ta e="T90" id="Seg_6699" s="T89">v-v:tense-v:pred.pn</ta>
            <ta e="T91" id="Seg_6700" s="T90">n-n:(poss)-n:case</ta>
            <ta e="T92" id="Seg_6701" s="T91">n-n:poss-n:case</ta>
            <ta e="T93" id="Seg_6702" s="T92">adj-adj&gt;adv</ta>
            <ta e="T94" id="Seg_6703" s="T93">n-n&gt;n-n:(poss)-n:case</ta>
            <ta e="T95" id="Seg_6704" s="T94">adv</ta>
            <ta e="T96" id="Seg_6705" s="T95">v-v&gt;v-v:cvb</ta>
            <ta e="T97" id="Seg_6706" s="T96">v-v:mood.pn</ta>
            <ta e="T98" id="Seg_6707" s="T97">adj-n:poss-n:case</ta>
            <ta e="T99" id="Seg_6708" s="T98">n-n:(poss)-n:case</ta>
            <ta e="T100" id="Seg_6709" s="T99">adj-n:poss-n:case</ta>
            <ta e="T101" id="Seg_6710" s="T100">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T102" id="Seg_6711" s="T101">n-n:(poss)-n:case</ta>
            <ta e="T103" id="Seg_6712" s="T102">n-n:poss-n:case</ta>
            <ta e="T104" id="Seg_6713" s="T103">v-v&gt;v-v:cvb</ta>
            <ta e="T105" id="Seg_6714" s="T104">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T106" id="Seg_6715" s="T105">n-n:case</ta>
            <ta e="T107" id="Seg_6716" s="T106">v-v:tense-v:pred.pn</ta>
            <ta e="T108" id="Seg_6717" s="T107">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T109" id="Seg_6718" s="T108">adj</ta>
            <ta e="T110" id="Seg_6719" s="T109">n-n:poss-n:case</ta>
            <ta e="T111" id="Seg_6720" s="T110">n-n:poss-n:case</ta>
            <ta e="T112" id="Seg_6721" s="T111">n-n:case</ta>
            <ta e="T113" id="Seg_6722" s="T112">v-v:cvb</ta>
            <ta e="T114" id="Seg_6723" s="T113">v-v:mood.pn</ta>
            <ta e="T115" id="Seg_6724" s="T114">adj-adj&gt;v-v:cvb</ta>
            <ta e="T116" id="Seg_6725" s="T115">post</ta>
            <ta e="T117" id="Seg_6726" s="T116">ptcl</ta>
            <ta e="T118" id="Seg_6727" s="T117">v-v:(tense)-v:mood.pn</ta>
            <ta e="T119" id="Seg_6728" s="T118">adv</ta>
            <ta e="T120" id="Seg_6729" s="T119">v-v:cvb</ta>
            <ta e="T121" id="Seg_6730" s="T120">n-n:case</ta>
            <ta e="T122" id="Seg_6731" s="T121">v-v:cvb</ta>
            <ta e="T123" id="Seg_6732" s="T122">adj</ta>
            <ta e="T124" id="Seg_6733" s="T123">n-n:(poss)-n:case</ta>
            <ta e="T125" id="Seg_6734" s="T124">adj-n&gt;n-n:poss-n:case</ta>
            <ta e="T126" id="Seg_6735" s="T125">n-n:case</ta>
            <ta e="T127" id="Seg_6736" s="T126">v-v:tense-v:pred.pn</ta>
            <ta e="T128" id="Seg_6737" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_6738" s="T128">adj</ta>
            <ta e="T130" id="Seg_6739" s="T129">n-n:case-n&gt;n-n&gt;n-n&gt;n-n:poss-n:case</ta>
            <ta e="T131" id="Seg_6740" s="T130">v-v:cvb</ta>
            <ta e="T132" id="Seg_6741" s="T131">v-v:tense-v:poss.pn</ta>
            <ta e="T133" id="Seg_6742" s="T132">n-n:(poss)-n:case</ta>
            <ta e="T134" id="Seg_6743" s="T133">adj-n:case</ta>
            <ta e="T135" id="Seg_6744" s="T134">interj</ta>
            <ta e="T136" id="Seg_6745" s="T135">n-n:case</ta>
            <ta e="T137" id="Seg_6746" s="T136">n-n:poss-n:case</ta>
            <ta e="T138" id="Seg_6747" s="T137">v-v:mood.pn</ta>
            <ta e="T139" id="Seg_6748" s="T138">v-v:cvb</ta>
            <ta e="T140" id="Seg_6749" s="T139">v-v:tense-v:poss.pn</ta>
            <ta e="T141" id="Seg_6750" s="T140">v-v:tense-v:pred.pn</ta>
            <ta e="T142" id="Seg_6751" s="T141">n-n:case</ta>
            <ta e="T143" id="Seg_6752" s="T142">n-n:(poss)-n:case</ta>
            <ta e="T144" id="Seg_6753" s="T143">n-n:case</ta>
            <ta e="T145" id="Seg_6754" s="T144">adv</ta>
            <ta e="T146" id="Seg_6755" s="T145">v-v:mood.pn</ta>
            <ta e="T147" id="Seg_6756" s="T146">v-v:tense-v:pred.pn</ta>
            <ta e="T148" id="Seg_6757" s="T147">n-n:(poss)-n:case</ta>
            <ta e="T149" id="Seg_6758" s="T148">dempro</ta>
            <ta e="T150" id="Seg_6759" s="T149">n-n&gt;n-n:poss-n:case</ta>
            <ta e="T151" id="Seg_6760" s="T150">v-v:cvb</ta>
            <ta e="T152" id="Seg_6761" s="T151">post</ta>
            <ta e="T153" id="Seg_6762" s="T152">ptcl</ta>
            <ta e="T154" id="Seg_6763" s="T153">v-v:cvb</ta>
            <ta e="T155" id="Seg_6764" s="T154">v-v:cvb</ta>
            <ta e="T156" id="Seg_6765" s="T155">v-v:tense-v:pred.pn</ta>
            <ta e="T157" id="Seg_6766" s="T156">n-n:(poss)-n:case</ta>
            <ta e="T158" id="Seg_6767" s="T157">n-n:poss-n:case</ta>
            <ta e="T159" id="Seg_6768" s="T158">n-n:poss-n:case</ta>
            <ta e="T160" id="Seg_6769" s="T159">v-v:cvb</ta>
            <ta e="T161" id="Seg_6770" s="T160">n-n&gt;n-n:(num)-n:case</ta>
            <ta e="T162" id="Seg_6771" s="T161">v-v&gt;n-n:poss-n:case</ta>
            <ta e="T163" id="Seg_6772" s="T162">adv</ta>
            <ta e="T164" id="Seg_6773" s="T163">n-n:poss-n:case</ta>
            <ta e="T165" id="Seg_6774" s="T164">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T166" id="Seg_6775" s="T165">ptcl</ta>
            <ta e="T167" id="Seg_6776" s="T166">n-n:poss-n:case</ta>
            <ta e="T168" id="Seg_6777" s="T167">v-v:tense-v:poss.pn</ta>
            <ta e="T169" id="Seg_6778" s="T168">n-n:poss-n:case</ta>
            <ta e="T170" id="Seg_6779" s="T169">n-n:poss-n:case</ta>
            <ta e="T171" id="Seg_6780" s="T170">v-v:cvb</ta>
            <ta e="T172" id="Seg_6781" s="T171">post</ta>
            <ta e="T173" id="Seg_6782" s="T172">v-v:cvb</ta>
            <ta e="T174" id="Seg_6783" s="T173">post</ta>
            <ta e="T175" id="Seg_6784" s="T174">n-n&gt;v-v:cvb</ta>
            <ta e="T176" id="Seg_6785" s="T175">v-v:tense-v:pred.pn</ta>
            <ta e="T177" id="Seg_6786" s="T176">dempro</ta>
            <ta e="T178" id="Seg_6787" s="T177">v-v&gt;v-v:cvb</ta>
            <ta e="T179" id="Seg_6788" s="T178">v-v:ptcp</ta>
            <ta e="T180" id="Seg_6789" s="T179">n-n:poss-n:case</ta>
            <ta e="T181" id="Seg_6790" s="T180">n-n:poss-n:case</ta>
            <ta e="T182" id="Seg_6791" s="T181">n-n:case</ta>
            <ta e="T183" id="Seg_6792" s="T182">v-v:cvb</ta>
            <ta e="T184" id="Seg_6793" s="T183">v-v:tense-v:pred.pn</ta>
            <ta e="T185" id="Seg_6794" s="T184">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T186" id="Seg_6795" s="T185">v-v:cvb-v-v:cvb</ta>
            <ta e="T187" id="Seg_6796" s="T186">n-n:case</ta>
            <ta e="T188" id="Seg_6797" s="T187">v-v&gt;v-v:cvb</ta>
            <ta e="T189" id="Seg_6798" s="T188">post</ta>
            <ta e="T190" id="Seg_6799" s="T189">n-n&gt;v-v:ptcp</ta>
            <ta e="T191" id="Seg_6800" s="T190">n-n&gt;n-n:case</ta>
            <ta e="T192" id="Seg_6801" s="T191">post</ta>
            <ta e="T193" id="Seg_6802" s="T192">v-v:cvb-v-v:cvb</ta>
            <ta e="T194" id="Seg_6803" s="T193">v-v:cvb</ta>
            <ta e="T195" id="Seg_6804" s="T194">v-v:tense-v:pred.pn</ta>
            <ta e="T196" id="Seg_6805" s="T195">ptcl</ta>
            <ta e="T197" id="Seg_6806" s="T196">adv</ta>
            <ta e="T198" id="Seg_6807" s="T197">n-n:(ins)-n&gt;n-n:case</ta>
            <ta e="T199" id="Seg_6808" s="T198">n-n:case</ta>
            <ta e="T200" id="Seg_6809" s="T199">n-n:poss-n:case</ta>
            <ta e="T201" id="Seg_6810" s="T200">v-v:cvb</ta>
            <ta e="T202" id="Seg_6811" s="T201">v-v:tense-v:pred.pn</ta>
            <ta e="T203" id="Seg_6812" s="T202">v-v:cvb</ta>
            <ta e="T204" id="Seg_6813" s="T203">v-v:tense-v:pred.pn</ta>
            <ta e="T205" id="Seg_6814" s="T204">n-n:case</ta>
            <ta e="T206" id="Seg_6815" s="T205">v-v:ptcp-v:(case)</ta>
            <ta e="T207" id="Seg_6816" s="T206">n-n:(poss)-n:case</ta>
            <ta e="T208" id="Seg_6817" s="T207">v-v:tense-v:pred.pn</ta>
            <ta e="T209" id="Seg_6818" s="T208">dempro</ta>
            <ta e="T210" id="Seg_6819" s="T209">v-v:mood-v:temp.pn</ta>
            <ta e="T211" id="Seg_6820" s="T210">n-n:case</ta>
            <ta e="T212" id="Seg_6821" s="T211">ptcl</ta>
            <ta e="T213" id="Seg_6822" s="T212">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T214" id="Seg_6823" s="T213">n-n:case</ta>
            <ta e="T215" id="Seg_6824" s="T214">v-v:ptcp-v:(case)</ta>
            <ta e="T216" id="Seg_6825" s="T215">n-n:case</ta>
            <ta e="T217" id="Seg_6826" s="T216">v-v:ptcp-v:(case)</ta>
            <ta e="T218" id="Seg_6827" s="T217">adj</ta>
            <ta e="T219" id="Seg_6828" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_6829" s="T219">dempro-pro:case</ta>
            <ta e="T221" id="Seg_6830" s="T220">post</ta>
            <ta e="T222" id="Seg_6831" s="T221">ptcl</ta>
            <ta e="T223" id="Seg_6832" s="T222">v-v:tense-v:pred.pn</ta>
            <ta e="T224" id="Seg_6833" s="T223">dempro</ta>
            <ta e="T225" id="Seg_6834" s="T224">n-n:case</ta>
            <ta e="T226" id="Seg_6835" s="T225">dempro</ta>
            <ta e="T227" id="Seg_6836" s="T226">v-v:cvb</ta>
            <ta e="T228" id="Seg_6837" s="T227">n-n:case</ta>
            <ta e="T229" id="Seg_6838" s="T228">ptcl</ta>
            <ta e="T230" id="Seg_6839" s="T229">n-n:case</ta>
            <ta e="T231" id="Seg_6840" s="T230">v-v:ptcp</ta>
            <ta e="T232" id="Seg_6841" s="T231">adj</ta>
            <ta e="T233" id="Seg_6842" s="T232">n-n:case</ta>
            <ta e="T234" id="Seg_6843" s="T233">n-n:case</ta>
            <ta e="T235" id="Seg_6844" s="T234">n-n:case</ta>
            <ta e="T236" id="Seg_6845" s="T235">v-v:ptcp</ta>
            <ta e="T237" id="Seg_6846" s="T236">n-n:case</ta>
            <ta e="T238" id="Seg_6847" s="T237">n-n:case</ta>
            <ta e="T239" id="Seg_6848" s="T238">n-n:case</ta>
            <ta e="T240" id="Seg_6849" s="T239">v-v:tense-v:pred.pn</ta>
            <ta e="T241" id="Seg_6850" s="T240">dempro</ta>
            <ta e="T242" id="Seg_6851" s="T241">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T243" id="Seg_6852" s="T242">n-n:case</ta>
            <ta e="T244" id="Seg_6853" s="T243">v-v:cvb</ta>
            <ta e="T245" id="Seg_6854" s="T244">dempro</ta>
            <ta e="T246" id="Seg_6855" s="T245">n-n:case</ta>
            <ta e="T247" id="Seg_6856" s="T246">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T248" id="Seg_6857" s="T247">cardnum</ta>
            <ta e="T249" id="Seg_6858" s="T248">n-n:(poss)-n:case</ta>
            <ta e="T250" id="Seg_6859" s="T249">v-v:cvb</ta>
            <ta e="T251" id="Seg_6860" s="T250">v-v:tense-v:pred.pn</ta>
            <ta e="T252" id="Seg_6861" s="T251">adv</ta>
            <ta e="T253" id="Seg_6862" s="T252">n-n:case</ta>
            <ta e="T254" id="Seg_6863" s="T253">adj&gt;adj-adj</ta>
            <ta e="T255" id="Seg_6864" s="T254">n-n:case</ta>
            <ta e="T256" id="Seg_6865" s="T255">n-n:case</ta>
            <ta e="T257" id="Seg_6866" s="T256">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T258" id="Seg_6867" s="T257">v-v:tense-v:pred.pn</ta>
            <ta e="T259" id="Seg_6868" s="T258">adj</ta>
            <ta e="T260" id="Seg_6869" s="T259">post</ta>
            <ta e="T261" id="Seg_6870" s="T260">n-n:case</ta>
            <ta e="T262" id="Seg_6871" s="T261">v-v:tense-v:poss.pn</ta>
            <ta e="T263" id="Seg_6872" s="T262">dempro</ta>
            <ta e="T264" id="Seg_6873" s="T263">n-n:case</ta>
            <ta e="T265" id="Seg_6874" s="T264">adv</ta>
            <ta e="T266" id="Seg_6875" s="T265">ptcl</ta>
            <ta e="T267" id="Seg_6876" s="T266">que</ta>
            <ta e="T268" id="Seg_6877" s="T267">cardnum</ta>
            <ta e="T269" id="Seg_6878" s="T268">ptcl</ta>
            <ta e="T270" id="Seg_6879" s="T269">v-v:cvb</ta>
            <ta e="T271" id="Seg_6880" s="T270">n-n:(poss)-n:case</ta>
            <ta e="T272" id="Seg_6881" s="T271">n-n:(poss)-n:case</ta>
            <ta e="T273" id="Seg_6882" s="T272">v-v:cvb</ta>
            <ta e="T274" id="Seg_6883" s="T273">v-v:tense-v:pred.pn</ta>
            <ta e="T275" id="Seg_6884" s="T274">adv</ta>
            <ta e="T276" id="Seg_6885" s="T275">v-v:cvb</ta>
            <ta e="T277" id="Seg_6886" s="T276">dempro</ta>
            <ta e="T278" id="Seg_6887" s="T277">n-n:case</ta>
            <ta e="T279" id="Seg_6888" s="T278">dempro</ta>
            <ta e="T280" id="Seg_6889" s="T279">n-n:case</ta>
            <ta e="T281" id="Seg_6890" s="T280">v-v:cvb</ta>
            <ta e="T282" id="Seg_6891" s="T281">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T283" id="Seg_6892" s="T282">v-v:cvb</ta>
            <ta e="T284" id="Seg_6893" s="T283">v-v:tense-v:poss.pn</ta>
            <ta e="T285" id="Seg_6894" s="T284">adj&gt;adj-adj-n:case</ta>
            <ta e="T286" id="Seg_6895" s="T285">n-n:(poss)-n:case</ta>
            <ta e="T287" id="Seg_6896" s="T286">ptcl</ta>
            <ta e="T288" id="Seg_6897" s="T287">adj</ta>
            <ta e="T289" id="Seg_6898" s="T288">n-n:case</ta>
            <ta e="T290" id="Seg_6899" s="T289">n-n&gt;v-v:cvb</ta>
            <ta e="T291" id="Seg_6900" s="T290">v-v:tense-v:pred.pn</ta>
            <ta e="T292" id="Seg_6901" s="T291">adj</ta>
            <ta e="T293" id="Seg_6902" s="T292">n-n:case</ta>
            <ta e="T294" id="Seg_6903" s="T293">n-n&gt;v-v:cvb</ta>
            <ta e="T295" id="Seg_6904" s="T294">v-v:ptcp-v:pred.pn</ta>
            <ta e="T296" id="Seg_6905" s="T295">n-n:case</ta>
            <ta e="T297" id="Seg_6906" s="T296">n-n:(num)-n:case</ta>
            <ta e="T298" id="Seg_6907" s="T297">adj-adj&gt;adv</ta>
            <ta e="T299" id="Seg_6908" s="T298">dempro</ta>
            <ta e="T300" id="Seg_6909" s="T299">n-n&gt;adj</ta>
            <ta e="T301" id="Seg_6910" s="T300">n-n:case</ta>
            <ta e="T302" id="Seg_6911" s="T301">v-v:tense-v:pred.pn</ta>
            <ta e="T303" id="Seg_6912" s="T302">dempro</ta>
            <ta e="T304" id="Seg_6913" s="T303">n-n:case</ta>
            <ta e="T305" id="Seg_6914" s="T304">v-v:cvb</ta>
            <ta e="T306" id="Seg_6915" s="T305">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T307" id="Seg_6916" s="T306">v-v:tense-v:pred.pn</ta>
            <ta e="T308" id="Seg_6917" s="T307">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T309" id="Seg_6918" s="T308">v-v:tense-v:pred.pn</ta>
            <ta e="T310" id="Seg_6919" s="T309">adj</ta>
            <ta e="T311" id="Seg_6920" s="T310">conj</ta>
            <ta e="T312" id="Seg_6921" s="T311">n-n:(poss)-n:case</ta>
            <ta e="T313" id="Seg_6922" s="T312">quant</ta>
            <ta e="T314" id="Seg_6923" s="T313">adj</ta>
            <ta e="T315" id="Seg_6924" s="T314">conj</ta>
            <ta e="T316" id="Seg_6925" s="T315">n-n:(poss)-n:case</ta>
            <ta e="T317" id="Seg_6926" s="T316">quant</ta>
            <ta e="T318" id="Seg_6927" s="T317">dempro</ta>
            <ta e="T319" id="Seg_6928" s="T318">n-n:case</ta>
            <ta e="T320" id="Seg_6929" s="T319">v-v:cvb-v-v:cvb</ta>
            <ta e="T321" id="Seg_6930" s="T320">post</ta>
            <ta e="T322" id="Seg_6931" s="T321">n-n:case-n-n:case</ta>
            <ta e="T323" id="Seg_6932" s="T322">n-n:case</ta>
            <ta e="T324" id="Seg_6933" s="T323">v-v:cvb</ta>
            <ta e="T325" id="Seg_6934" s="T324">v-v:tense-v:pred.pn</ta>
            <ta e="T326" id="Seg_6935" s="T325">dempro-pro:case</ta>
            <ta e="T327" id="Seg_6936" s="T326">post</ta>
            <ta e="T328" id="Seg_6937" s="T327">que-pro:case</ta>
            <ta e="T329" id="Seg_6938" s="T328">ptcl</ta>
            <ta e="T330" id="Seg_6939" s="T329">n-n:(poss)-n:case</ta>
            <ta e="T331" id="Seg_6940" s="T330">v-v&gt;v-v:(ins)-v:(neg).[v:pred.pn]</ta>
            <ta e="T332" id="Seg_6941" s="T331">cardnum</ta>
            <ta e="T333" id="Seg_6942" s="T332">n-n:case</ta>
            <ta e="T334" id="Seg_6943" s="T333">post</ta>
            <ta e="T335" id="Seg_6944" s="T334">que-pro:case</ta>
            <ta e="T336" id="Seg_6945" s="T335">ptcl</ta>
            <ta e="T337" id="Seg_6946" s="T336">n-n:(poss)-n:case</ta>
            <ta e="T338" id="Seg_6947" s="T337">v-v&gt;v-v:(ins)-v:(neg).[v:pred.pn]</ta>
            <ta e="T339" id="Seg_6948" s="T338">ptcl</ta>
            <ta e="T340" id="Seg_6949" s="T339">n-n:case</ta>
            <ta e="T341" id="Seg_6950" s="T340">v-v:tense-v:poss.pn</ta>
            <ta e="T342" id="Seg_6951" s="T341">conj</ta>
            <ta e="T343" id="Seg_6952" s="T342">n-n:poss-n:case</ta>
            <ta e="T344" id="Seg_6953" s="T343">adj</ta>
            <ta e="T345" id="Seg_6954" s="T344">n-n:case</ta>
            <ta e="T346" id="Seg_6955" s="T345">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T347" id="Seg_6956" s="T346">v-v:tense-v:pred.pn</ta>
            <ta e="T348" id="Seg_6957" s="T347">dempro-pro:(num)-pro:case</ta>
            <ta e="T349" id="Seg_6958" s="T348">v-v:(ins)-v:mood.pn-v-v:(ins)-v:mood.pn</ta>
            <ta e="T350" id="Seg_6959" s="T349">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T351" id="Seg_6960" s="T350">v-v:mood.pn</ta>
            <ta e="T352" id="Seg_6961" s="T351">v-v:cvb-v-v:cvb</ta>
            <ta e="T353" id="Seg_6962" s="T352">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T354" id="Seg_6963" s="T353">v-v:tense-v:pred.pn</ta>
            <ta e="T355" id="Seg_6964" s="T354">dempro-pro:(num)-pro:(poss)-pro:case</ta>
            <ta e="T356" id="Seg_6965" s="T355">n-n:case</ta>
            <ta e="T357" id="Seg_6966" s="T356">post</ta>
            <ta e="T358" id="Seg_6967" s="T357">v-v:cvb</ta>
            <ta e="T359" id="Seg_6968" s="T358">v-v:tense-v:pred.pn</ta>
            <ta e="T360" id="Seg_6969" s="T359">n-n:case</ta>
            <ta e="T361" id="Seg_6970" s="T360">que</ta>
            <ta e="T362" id="Seg_6971" s="T361">cardnum</ta>
            <ta e="T363" id="Seg_6972" s="T362">post</ta>
            <ta e="T364" id="Seg_6973" s="T363">v-v:tense-v:pred.pn</ta>
            <ta e="T365" id="Seg_6974" s="T364">n-n:(poss)-n:case</ta>
            <ta e="T366" id="Seg_6975" s="T365">que</ta>
            <ta e="T367" id="Seg_6976" s="T366">ptcl</ta>
            <ta e="T368" id="Seg_6977" s="T367">v-v:(neg)-v:pred.pn</ta>
            <ta e="T369" id="Seg_6978" s="T368">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T370" id="Seg_6979" s="T369">dempro-pro:case</ta>
            <ta e="T371" id="Seg_6980" s="T370">post</ta>
            <ta e="T372" id="Seg_6981" s="T371">v-v:tense-v:pred.pn</ta>
            <ta e="T373" id="Seg_6982" s="T372">n-n:(poss)-n:case</ta>
            <ta e="T374" id="Seg_6983" s="T373">dempro</ta>
            <ta e="T375" id="Seg_6984" s="T374">v-v:(ins)-v&gt;v-v:ptcp-v:(case)</ta>
            <ta e="T376" id="Seg_6985" s="T375">post</ta>
            <ta e="T377" id="Seg_6986" s="T376">v-v:tense-v:pred.pn</ta>
            <ta e="T378" id="Seg_6987" s="T377">n-n:case</ta>
            <ta e="T379" id="Seg_6988" s="T378">adv</ta>
            <ta e="T380" id="Seg_6989" s="T379">v-v:cvb-v-v:cvb</ta>
            <ta e="T381" id="Seg_6990" s="T380">adv&gt;adv-adv</ta>
            <ta e="T382" id="Seg_6991" s="T381">v-v:tense-v:pred.pn</ta>
            <ta e="T383" id="Seg_6992" s="T382">adv</ta>
            <ta e="T384" id="Seg_6993" s="T383">v-v:cvb</ta>
            <ta e="T385" id="Seg_6994" s="T384">n-n:case</ta>
            <ta e="T386" id="Seg_6995" s="T385">n-n:(poss)-n:case</ta>
            <ta e="T387" id="Seg_6996" s="T386">v-v:tense-v:pred.pn</ta>
            <ta e="T388" id="Seg_6997" s="T387">n-n:(num)-n:case</ta>
            <ta e="T389" id="Seg_6998" s="T388">pers-pro:case</ta>
            <ta e="T390" id="Seg_6999" s="T389">v-v:ptcp-v:(case)</ta>
            <ta e="T391" id="Seg_7000" s="T390">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T392" id="Seg_7001" s="T391">adj-n:poss-n:case</ta>
            <ta e="T393" id="Seg_7002" s="T392">v-v&gt;v-v:cvb</ta>
            <ta e="T394" id="Seg_7003" s="T393">v-v:ptcp</ta>
            <ta e="T395" id="Seg_7004" s="T394">ptcl-ptcl:(ins)-ptcl:(poss.pn)</ta>
            <ta e="T396" id="Seg_7005" s="T395">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T397" id="Seg_7006" s="T396">n-n:poss-n:case</ta>
            <ta e="T398" id="Seg_7007" s="T397">n-n:poss-n:case</ta>
            <ta e="T399" id="Seg_7008" s="T398">v-v:cvb</ta>
            <ta e="T400" id="Seg_7009" s="T399">v-v:tense-v:poss.pn</ta>
            <ta e="T401" id="Seg_7010" s="T400">n-n:case</ta>
            <ta e="T402" id="Seg_7011" s="T401">v-v&gt;v-v:cvb</ta>
            <ta e="T403" id="Seg_7012" s="T402">post</ta>
            <ta e="T404" id="Seg_7013" s="T403">v-v:tense-v:poss.pn</ta>
            <ta e="T405" id="Seg_7014" s="T404">dempro-pro:case</ta>
            <ta e="T406" id="Seg_7015" s="T405">v-v:cvb</ta>
            <ta e="T407" id="Seg_7016" s="T406">post</ta>
            <ta e="T408" id="Seg_7017" s="T407">n-n:case</ta>
            <ta e="T409" id="Seg_7018" s="T408">n-n:poss-n:case</ta>
            <ta e="T410" id="Seg_7019" s="T409">v-v:mood-v:pred.pn</ta>
            <ta e="T411" id="Seg_7020" s="T410">adv</ta>
            <ta e="T412" id="Seg_7021" s="T411">v-v:cvb</ta>
            <ta e="T413" id="Seg_7022" s="T412">post</ta>
            <ta e="T414" id="Seg_7023" s="T413">n-n:poss-n:case</ta>
            <ta e="T415" id="Seg_7024" s="T414">v-v:tense-v:poss.pn</ta>
            <ta e="T416" id="Seg_7025" s="T415">dempro</ta>
            <ta e="T417" id="Seg_7026" s="T416">v-v:cvb</ta>
            <ta e="T418" id="Seg_7027" s="T417">v-v:cvb</ta>
            <ta e="T419" id="Seg_7028" s="T418">n-n:poss-n:case</ta>
            <ta e="T420" id="Seg_7029" s="T419">n-n:poss-n:case</ta>
            <ta e="T421" id="Seg_7030" s="T420">v-v:tense-v:pred.pn</ta>
            <ta e="T422" id="Seg_7031" s="T421">ptcl</ta>
            <ta e="T423" id="Seg_7032" s="T422">que-pro&gt;pro-pro:case</ta>
            <ta e="T424" id="Seg_7033" s="T423">ptcl</ta>
            <ta e="T425" id="Seg_7034" s="T424">ptcl</ta>
            <ta e="T426" id="Seg_7035" s="T425">adj-adj&gt;adv</ta>
            <ta e="T427" id="Seg_7036" s="T426">adv</ta>
            <ta e="T428" id="Seg_7037" s="T427">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T429" id="Seg_7038" s="T428">v-v:cvb</ta>
            <ta e="T430" id="Seg_7039" s="T429">v-v:tense-v:pred.pn</ta>
            <ta e="T431" id="Seg_7040" s="T430">dempro-pro:case</ta>
            <ta e="T432" id="Seg_7041" s="T431">conj</ta>
            <ta e="T433" id="Seg_7042" s="T432">v-v:mood-v:pred.pn</ta>
            <ta e="T434" id="Seg_7043" s="T433">n-n:poss-n:case</ta>
            <ta e="T435" id="Seg_7044" s="T434">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T436" id="Seg_7045" s="T435">ptcl</ta>
            <ta e="T437" id="Seg_7046" s="T436">n-n:poss-n:case</ta>
            <ta e="T438" id="Seg_7047" s="T437">post</ta>
            <ta e="T439" id="Seg_7048" s="T438">cardnum</ta>
            <ta e="T440" id="Seg_7049" s="T439">n-n&gt;adj</ta>
            <ta e="T441" id="Seg_7050" s="T440">n-n:case</ta>
            <ta e="T442" id="Seg_7051" s="T441">v-v:cvb</ta>
            <ta e="T443" id="Seg_7052" s="T442">v-v:cvb</ta>
            <ta e="T444" id="Seg_7053" s="T443">v-v:tense-v:pred.pn</ta>
            <ta e="T445" id="Seg_7054" s="T444">n-n:poss-n:case</ta>
            <ta e="T446" id="Seg_7055" s="T445">adj</ta>
            <ta e="T447" id="Seg_7056" s="T446">n-n:poss-n:case</ta>
            <ta e="T448" id="Seg_7057" s="T447">post</ta>
            <ta e="T449" id="Seg_7058" s="T448">v-v&gt;v-v:(ins)-v&gt;n-n:poss-n:case</ta>
            <ta e="T450" id="Seg_7059" s="T449">ptcl</ta>
            <ta e="T451" id="Seg_7060" s="T450">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T452" id="Seg_7061" s="T451">n-n:poss-n:case</ta>
            <ta e="T453" id="Seg_7062" s="T452">adj-pro:(poss)-pro:case</ta>
            <ta e="T454" id="Seg_7063" s="T453">adv</ta>
            <ta e="T455" id="Seg_7064" s="T454">v-v:ptcp</ta>
            <ta e="T456" id="Seg_7065" s="T455">n-n:(poss)-n:case</ta>
            <ta e="T457" id="Seg_7066" s="T456">ptcl</ta>
            <ta e="T458" id="Seg_7067" s="T457">dempro-pro:case</ta>
            <ta e="T459" id="Seg_7068" s="T458">post</ta>
            <ta e="T460" id="Seg_7069" s="T459">v-v:(ins)-v:(neg)-v:cvb</ta>
            <ta e="T461" id="Seg_7070" s="T460">v-v:tense-v:poss.pn</ta>
            <ta e="T462" id="Seg_7071" s="T461">ptcl</ta>
            <ta e="T463" id="Seg_7072" s="T462">ptcl</ta>
            <ta e="T464" id="Seg_7073" s="T463">dempro-pro:case</ta>
            <ta e="T465" id="Seg_7074" s="T464">dempro</ta>
            <ta e="T466" id="Seg_7075" s="T465">n-n:(ins)-n:case</ta>
            <ta e="T467" id="Seg_7076" s="T466">v-v:tense-v:pred.pn</ta>
            <ta e="T468" id="Seg_7077" s="T467">dempro-pro:case</ta>
            <ta e="T469" id="Seg_7078" s="T468">v-v:(ins)-v&gt;n-n:case</ta>
            <ta e="T470" id="Seg_7079" s="T469">v-v:tense-v:pred.pn</ta>
            <ta e="T471" id="Seg_7080" s="T470">conj</ta>
            <ta e="T472" id="Seg_7081" s="T471">v-v:tense-v:pred.pn</ta>
            <ta e="T473" id="Seg_7082" s="T472">dempro-pro:case</ta>
            <ta e="T474" id="Seg_7083" s="T473">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T475" id="Seg_7084" s="T474">n-n:(poss)-n:case</ta>
            <ta e="T476" id="Seg_7085" s="T475">v-v&gt;v-v:(ins)-v:(neg)-v:cvb</ta>
            <ta e="T477" id="Seg_7086" s="T476">v-v:tense-v:pred.pn</ta>
            <ta e="T478" id="Seg_7087" s="T477">v-v:tense-v:pred.pn</ta>
            <ta e="T479" id="Seg_7088" s="T478">n-n:case</ta>
            <ta e="T480" id="Seg_7089" s="T479">v-v:tense-v:pred.pn</ta>
            <ta e="T481" id="Seg_7090" s="T480">interj</ta>
            <ta e="T482" id="Seg_7091" s="T481">n-n:(num)-n:case</ta>
            <ta e="T483" id="Seg_7092" s="T482">v-v:cvb-v:pred.pn</ta>
            <ta e="T484" id="Seg_7093" s="T483">v-v&gt;v-v:(ins)-v:(neg)-v:cvb</ta>
            <ta e="T485" id="Seg_7094" s="T484">v-v:tense-v:pred.pn</ta>
            <ta e="T486" id="Seg_7095" s="T485">ptcl</ta>
            <ta e="T487" id="Seg_7096" s="T486">v-v:tense-v:pred.pn</ta>
            <ta e="T488" id="Seg_7097" s="T487">dempro-pro:case</ta>
            <ta e="T489" id="Seg_7098" s="T488">v-v:cvb</ta>
            <ta e="T490" id="Seg_7099" s="T489">v-v:tense-v:pred.pn</ta>
            <ta e="T491" id="Seg_7100" s="T490">n-n:case</ta>
            <ta e="T492" id="Seg_7101" s="T491">v-v:tense-v:pred.pn</ta>
            <ta e="T493" id="Seg_7102" s="T492">dempro-pro:case</ta>
            <ta e="T494" id="Seg_7103" s="T493">n-n:case</ta>
            <ta e="T495" id="Seg_7104" s="T494">n-n:case</ta>
            <ta e="T496" id="Seg_7105" s="T495">adv-adv&gt;adv-adv&gt;adv</ta>
            <ta e="T497" id="Seg_7106" s="T496">v-v:tense-v:pred.pn</ta>
            <ta e="T498" id="Seg_7107" s="T497">interj</ta>
            <ta e="T499" id="Seg_7108" s="T498">n-n:case</ta>
            <ta e="T500" id="Seg_7109" s="T499">n-n:poss-n:case</ta>
            <ta e="T501" id="Seg_7110" s="T500">v-v:mood.pn</ta>
            <ta e="T502" id="Seg_7111" s="T501">v-v:tense-v:pred.pn</ta>
            <ta e="T503" id="Seg_7112" s="T502">n-n:(poss)-n:case</ta>
            <ta e="T504" id="Seg_7113" s="T503">n-n:poss-n:case</ta>
            <ta e="T505" id="Seg_7114" s="T504">v-v:cvb</ta>
            <ta e="T506" id="Seg_7115" s="T505">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T507" id="Seg_7116" s="T506">n-n:case</ta>
            <ta e="T508" id="Seg_7117" s="T507">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T509" id="Seg_7118" s="T508">post</ta>
            <ta e="T510" id="Seg_7119" s="T509">n-n:poss-n:case</ta>
            <ta e="T511" id="Seg_7120" s="T510">v-v:tense-v:pred.pn</ta>
            <ta e="T512" id="Seg_7121" s="T511">dempro</ta>
            <ta e="T513" id="Seg_7122" s="T512">n-n:poss-n:case</ta>
            <ta e="T514" id="Seg_7123" s="T513">v-v:cvb</ta>
            <ta e="T515" id="Seg_7124" s="T514">v-v:cvb-v-v:cvb</ta>
            <ta e="T516" id="Seg_7125" s="T515">post</ta>
            <ta e="T517" id="Seg_7126" s="T516">v-v:(ins)-v:ptcp</ta>
            <ta e="T518" id="Seg_7127" s="T517">n-n:case</ta>
            <ta e="T519" id="Seg_7128" s="T518">v-v&gt;v-v:cvb</ta>
            <ta e="T520" id="Seg_7129" s="T519">post</ta>
            <ta e="T521" id="Seg_7130" s="T520">n-n:case</ta>
            <ta e="T522" id="Seg_7131" s="T521">n-n:case</ta>
            <ta e="T523" id="Seg_7132" s="T522">v-v:cvb</ta>
            <ta e="T524" id="Seg_7133" s="T523">v-v:tense-v:pred.pn</ta>
            <ta e="T525" id="Seg_7134" s="T524">dempro</ta>
            <ta e="T526" id="Seg_7135" s="T525">v-v:cvb</ta>
            <ta e="T527" id="Seg_7136" s="T526">post</ta>
            <ta e="T528" id="Seg_7137" s="T527">n-n:case</ta>
            <ta e="T529" id="Seg_7138" s="T528">v-v:(ins)-v&gt;v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T530" id="Seg_7139" s="T529">adv</ta>
            <ta e="T531" id="Seg_7140" s="T530">n-n:case</ta>
            <ta e="T532" id="Seg_7141" s="T531">n-n:case</ta>
            <ta e="T533" id="Seg_7142" s="T532">v-v:tense-v:pred.pn</ta>
            <ta e="T534" id="Seg_7143" s="T533">n-n&gt;adj</ta>
            <ta e="T535" id="Seg_7144" s="T534">n-n:case</ta>
            <ta e="T536" id="Seg_7145" s="T535">n-n:case</ta>
            <ta e="T537" id="Seg_7146" s="T536">v-v:tense-v:pred.pn</ta>
            <ta e="T538" id="Seg_7147" s="T537">adv</ta>
            <ta e="T539" id="Seg_7148" s="T538">v-v:cvb</ta>
            <ta e="T540" id="Seg_7149" s="T539">n-n:case</ta>
            <ta e="T541" id="Seg_7150" s="T540">n-n:case</ta>
            <ta e="T542" id="Seg_7151" s="T541">n-n:case</ta>
            <ta e="T543" id="Seg_7152" s="T542">v-v:tense-v:pred.pn</ta>
            <ta e="T544" id="Seg_7153" s="T543">ptcl</ta>
            <ta e="T545" id="Seg_7154" s="T544">n-n:poss-n:case</ta>
            <ta e="T546" id="Seg_7155" s="T545">v-v:cvb</ta>
            <ta e="T547" id="Seg_7156" s="T546">v-v:tense-v:pred.pn</ta>
            <ta e="T548" id="Seg_7157" s="T547">interj</ta>
            <ta e="T549" id="Seg_7158" s="T548">dempro-dempro</ta>
            <ta e="T550" id="Seg_7159" s="T549">n-n:(poss)-n:case</ta>
            <ta e="T551" id="Seg_7160" s="T550">adj-n:case</ta>
            <ta e="T552" id="Seg_7161" s="T551">adj</ta>
            <ta e="T553" id="Seg_7162" s="T552">n-n:case</ta>
            <ta e="T554" id="Seg_7163" s="T553">ptcl</ta>
            <ta e="T555" id="Seg_7164" s="T554">n-n:case</ta>
            <ta e="T556" id="Seg_7165" s="T555">ptcl</ta>
            <ta e="T557" id="Seg_7166" s="T556">v-v:mood.pn</ta>
            <ta e="T558" id="Seg_7167" s="T557">v-v:tense-v:pred.pn</ta>
            <ta e="T559" id="Seg_7168" s="T558">adj-n:(poss)-n:case</ta>
            <ta e="T560" id="Seg_7169" s="T559">n-n:case</ta>
            <ta e="T561" id="Seg_7170" s="T560">ptcl-ptcl:(poss)</ta>
            <ta e="T562" id="Seg_7171" s="T561">v-v:cvb</ta>
            <ta e="T563" id="Seg_7172" s="T562">v-v:tense-v:pred.pn</ta>
            <ta e="T564" id="Seg_7173" s="T563">n-n&gt;adj</ta>
            <ta e="T565" id="Seg_7174" s="T564">n-n:case</ta>
            <ta e="T566" id="Seg_7175" s="T565">dempro-pro:(poss)-pro:case</ta>
            <ta e="T567" id="Seg_7176" s="T566">interj</ta>
            <ta e="T568" id="Seg_7177" s="T567">v-v:tense-v:pred.pn</ta>
            <ta e="T569" id="Seg_7178" s="T568">dempro-pro:case</ta>
            <ta e="T570" id="Seg_7179" s="T569">ptcl</ta>
            <ta e="T571" id="Seg_7180" s="T570">v-v:tense-v:pred.pn</ta>
            <ta e="T572" id="Seg_7181" s="T571">ptcl</ta>
            <ta e="T573" id="Seg_7182" s="T572">ptcl</ta>
            <ta e="T574" id="Seg_7183" s="T573">adv</ta>
            <ta e="T575" id="Seg_7184" s="T574">v-v:tense-v:pred.pn</ta>
            <ta e="T576" id="Seg_7185" s="T575">v-v:cvb</ta>
            <ta e="T577" id="Seg_7186" s="T576">ptcl</ta>
            <ta e="T578" id="Seg_7187" s="T577">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T579" id="Seg_7188" s="T578">ptcl</ta>
            <ta e="T580" id="Seg_7189" s="T579">n-n:case</ta>
            <ta e="T581" id="Seg_7190" s="T580">n-n:poss-n:case</ta>
            <ta e="T582" id="Seg_7191" s="T581">post</ta>
            <ta e="T583" id="Seg_7192" s="T582">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T584" id="Seg_7193" s="T583">conj</ta>
            <ta e="T585" id="Seg_7194" s="T584">adv</ta>
            <ta e="T586" id="Seg_7195" s="T585">cardnum</ta>
            <ta e="T587" id="Seg_7196" s="T586">n-n:case</ta>
            <ta e="T588" id="Seg_7197" s="T587">n-n:case</ta>
            <ta e="T589" id="Seg_7198" s="T588">adv</ta>
            <ta e="T590" id="Seg_7199" s="T589">v-v:cvb</ta>
            <ta e="T591" id="Seg_7200" s="T590">v-v:tense-v:pred.pn</ta>
            <ta e="T592" id="Seg_7201" s="T591">n-n:(poss)-n:case</ta>
            <ta e="T593" id="Seg_7202" s="T592">que-pro:case</ta>
            <ta e="T594" id="Seg_7203" s="T593">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T595" id="Seg_7204" s="T594">que</ta>
            <ta e="T596" id="Seg_7205" s="T595">v-v:tense-v:poss.pn</ta>
            <ta e="T597" id="Seg_7206" s="T596">conj</ta>
            <ta e="T598" id="Seg_7207" s="T597">adv</ta>
            <ta e="T601" id="Seg_7208" s="T598">adv</ta>
            <ta e="T599" id="Seg_7209" s="T601">adv</ta>
            <ta e="T600" id="Seg_7210" s="T599">v-v:tense-v:pred.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_7211" s="T0">adj</ta>
            <ta e="T2" id="Seg_7212" s="T1">n</ta>
            <ta e="T3" id="Seg_7213" s="T2">v</ta>
            <ta e="T4" id="Seg_7214" s="T3">ptcl</ta>
            <ta e="T5" id="Seg_7215" s="T4">v</ta>
            <ta e="T6" id="Seg_7216" s="T5">n</ta>
            <ta e="T7" id="Seg_7217" s="T6">ptcl</ta>
            <ta e="T8" id="Seg_7218" s="T7">n</ta>
            <ta e="T9" id="Seg_7219" s="T8">v</ta>
            <ta e="T10" id="Seg_7220" s="T9">n</ta>
            <ta e="T11" id="Seg_7221" s="T10">ptcl</ta>
            <ta e="T12" id="Seg_7222" s="T11">v</ta>
            <ta e="T13" id="Seg_7223" s="T12">adv</ta>
            <ta e="T14" id="Seg_7224" s="T13">adj</ta>
            <ta e="T15" id="Seg_7225" s="T14">adv</ta>
            <ta e="T16" id="Seg_7226" s="T15">v</ta>
            <ta e="T17" id="Seg_7227" s="T16">n</ta>
            <ta e="T18" id="Seg_7228" s="T17">ptcl</ta>
            <ta e="T19" id="Seg_7229" s="T18">v</ta>
            <ta e="T20" id="Seg_7230" s="T19">que</ta>
            <ta e="T21" id="Seg_7231" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_7232" s="T21">ptcl</ta>
            <ta e="T23" id="Seg_7233" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_7234" s="T23">n</ta>
            <ta e="T25" id="Seg_7235" s="T24">v</ta>
            <ta e="T26" id="Seg_7236" s="T25">adj</ta>
            <ta e="T27" id="Seg_7237" s="T26">adj</ta>
            <ta e="T28" id="Seg_7238" s="T27">adv</ta>
            <ta e="T29" id="Seg_7239" s="T28">v</ta>
            <ta e="T30" id="Seg_7240" s="T29">n</ta>
            <ta e="T31" id="Seg_7241" s="T30">cardnum</ta>
            <ta e="T32" id="Seg_7242" s="T31">adj</ta>
            <ta e="T33" id="Seg_7243" s="T32">adj</ta>
            <ta e="T34" id="Seg_7244" s="T33">dempro</ta>
            <ta e="T35" id="Seg_7245" s="T34">n</ta>
            <ta e="T36" id="Seg_7246" s="T35">n</ta>
            <ta e="T37" id="Seg_7247" s="T36">v</ta>
            <ta e="T38" id="Seg_7248" s="T37">dempro</ta>
            <ta e="T39" id="Seg_7249" s="T38">v</ta>
            <ta e="T40" id="Seg_7250" s="T39">n</ta>
            <ta e="T41" id="Seg_7251" s="T40">v</ta>
            <ta e="T42" id="Seg_7252" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_7253" s="T42">v</ta>
            <ta e="T44" id="Seg_7254" s="T43">v</ta>
            <ta e="T45" id="Seg_7255" s="T44">v</ta>
            <ta e="T46" id="Seg_7256" s="T45">v</ta>
            <ta e="T47" id="Seg_7257" s="T46">adv</ta>
            <ta e="T48" id="Seg_7258" s="T47">n</ta>
            <ta e="T49" id="Seg_7259" s="T48">adv</ta>
            <ta e="T50" id="Seg_7260" s="T49">v</ta>
            <ta e="T51" id="Seg_7261" s="T50">n</ta>
            <ta e="T52" id="Seg_7262" s="T51">ptcl</ta>
            <ta e="T53" id="Seg_7263" s="T52">v</ta>
            <ta e="T54" id="Seg_7264" s="T53">v</ta>
            <ta e="T55" id="Seg_7265" s="T54">interj</ta>
            <ta e="T56" id="Seg_7266" s="T55">n</ta>
            <ta e="T57" id="Seg_7267" s="T56">v</ta>
            <ta e="T58" id="Seg_7268" s="T57">v</ta>
            <ta e="T59" id="Seg_7269" s="T58">adj</ta>
            <ta e="T60" id="Seg_7270" s="T59">adj</ta>
            <ta e="T61" id="Seg_7271" s="T60">v</ta>
            <ta e="T62" id="Seg_7272" s="T61">ptcl</ta>
            <ta e="T63" id="Seg_7273" s="T62">ptcl</ta>
            <ta e="T64" id="Seg_7274" s="T63">v</ta>
            <ta e="T65" id="Seg_7275" s="T64">v</ta>
            <ta e="T66" id="Seg_7276" s="T65">n</ta>
            <ta e="T67" id="Seg_7277" s="T66">v</ta>
            <ta e="T68" id="Seg_7278" s="T67">dempro</ta>
            <ta e="T69" id="Seg_7279" s="T68">n</ta>
            <ta e="T70" id="Seg_7280" s="T69">emphpro</ta>
            <ta e="T71" id="Seg_7281" s="T70">n</ta>
            <ta e="T72" id="Seg_7282" s="T71">v</ta>
            <ta e="T73" id="Seg_7283" s="T72">v</ta>
            <ta e="T74" id="Seg_7284" s="T73">ptcl</ta>
            <ta e="T75" id="Seg_7285" s="T74">v</ta>
            <ta e="T76" id="Seg_7286" s="T75">n</ta>
            <ta e="T77" id="Seg_7287" s="T76">v</ta>
            <ta e="T78" id="Seg_7288" s="T77">conj</ta>
            <ta e="T79" id="Seg_7289" s="T78">v</ta>
            <ta e="T80" id="Seg_7290" s="T79">n</ta>
            <ta e="T81" id="Seg_7291" s="T80">v</ta>
            <ta e="T82" id="Seg_7292" s="T81">ptcl</ta>
            <ta e="T83" id="Seg_7293" s="T82">adj</ta>
            <ta e="T84" id="Seg_7294" s="T83">ptcl</ta>
            <ta e="T85" id="Seg_7295" s="T84">adj</ta>
            <ta e="T86" id="Seg_7296" s="T85">ptcl</ta>
            <ta e="T87" id="Seg_7297" s="T86">n</ta>
            <ta e="T88" id="Seg_7298" s="T87">v</ta>
            <ta e="T89" id="Seg_7299" s="T88">v</ta>
            <ta e="T90" id="Seg_7300" s="T89">v</ta>
            <ta e="T91" id="Seg_7301" s="T90">n</ta>
            <ta e="T92" id="Seg_7302" s="T91">n</ta>
            <ta e="T93" id="Seg_7303" s="T92">adv</ta>
            <ta e="T94" id="Seg_7304" s="T93">n</ta>
            <ta e="T95" id="Seg_7305" s="T94">adv</ta>
            <ta e="T96" id="Seg_7306" s="T95">v</ta>
            <ta e="T97" id="Seg_7307" s="T96">aux</ta>
            <ta e="T98" id="Seg_7308" s="T97">adj</ta>
            <ta e="T99" id="Seg_7309" s="T98">n</ta>
            <ta e="T100" id="Seg_7310" s="T99">adj</ta>
            <ta e="T101" id="Seg_7311" s="T100">v</ta>
            <ta e="T102" id="Seg_7312" s="T101">n</ta>
            <ta e="T103" id="Seg_7313" s="T102">n</ta>
            <ta e="T104" id="Seg_7314" s="T103">v</ta>
            <ta e="T105" id="Seg_7315" s="T104">v</ta>
            <ta e="T106" id="Seg_7316" s="T105">n</ta>
            <ta e="T107" id="Seg_7317" s="T106">v</ta>
            <ta e="T108" id="Seg_7318" s="T107">v</ta>
            <ta e="T109" id="Seg_7319" s="T108">adj</ta>
            <ta e="T110" id="Seg_7320" s="T109">n</ta>
            <ta e="T111" id="Seg_7321" s="T110">n</ta>
            <ta e="T112" id="Seg_7322" s="T111">n</ta>
            <ta e="T113" id="Seg_7323" s="T112">v</ta>
            <ta e="T114" id="Seg_7324" s="T113">v</ta>
            <ta e="T115" id="Seg_7325" s="T114">v</ta>
            <ta e="T116" id="Seg_7326" s="T115">post</ta>
            <ta e="T117" id="Seg_7327" s="T116">ptcl</ta>
            <ta e="T118" id="Seg_7328" s="T117">v</ta>
            <ta e="T119" id="Seg_7329" s="T118">adv</ta>
            <ta e="T120" id="Seg_7330" s="T119">v</ta>
            <ta e="T121" id="Seg_7331" s="T120">n</ta>
            <ta e="T122" id="Seg_7332" s="T121">v</ta>
            <ta e="T123" id="Seg_7333" s="T122">adj</ta>
            <ta e="T124" id="Seg_7334" s="T123">n</ta>
            <ta e="T125" id="Seg_7335" s="T124">adj</ta>
            <ta e="T126" id="Seg_7336" s="T125">n</ta>
            <ta e="T127" id="Seg_7337" s="T126">v</ta>
            <ta e="T128" id="Seg_7338" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_7339" s="T128">adj</ta>
            <ta e="T130" id="Seg_7340" s="T129">n</ta>
            <ta e="T131" id="Seg_7341" s="T130">v</ta>
            <ta e="T132" id="Seg_7342" s="T131">v</ta>
            <ta e="T133" id="Seg_7343" s="T132">n</ta>
            <ta e="T134" id="Seg_7344" s="T133">adj</ta>
            <ta e="T135" id="Seg_7345" s="T134">interj</ta>
            <ta e="T136" id="Seg_7346" s="T135">n</ta>
            <ta e="T137" id="Seg_7347" s="T136">n</ta>
            <ta e="T138" id="Seg_7348" s="T137">v</ta>
            <ta e="T139" id="Seg_7349" s="T138">v</ta>
            <ta e="T140" id="Seg_7350" s="T139">aux</ta>
            <ta e="T141" id="Seg_7351" s="T140">v</ta>
            <ta e="T142" id="Seg_7352" s="T141">n</ta>
            <ta e="T143" id="Seg_7353" s="T142">n</ta>
            <ta e="T144" id="Seg_7354" s="T143">n</ta>
            <ta e="T145" id="Seg_7355" s="T144">adv</ta>
            <ta e="T146" id="Seg_7356" s="T145">v</ta>
            <ta e="T147" id="Seg_7357" s="T146">v</ta>
            <ta e="T148" id="Seg_7358" s="T147">n</ta>
            <ta e="T149" id="Seg_7359" s="T148">dempro</ta>
            <ta e="T150" id="Seg_7360" s="T149">n</ta>
            <ta e="T151" id="Seg_7361" s="T150">v</ta>
            <ta e="T152" id="Seg_7362" s="T151">post</ta>
            <ta e="T153" id="Seg_7363" s="T152">ptcl</ta>
            <ta e="T154" id="Seg_7364" s="T153">v</ta>
            <ta e="T155" id="Seg_7365" s="T154">v</ta>
            <ta e="T156" id="Seg_7366" s="T155">aux</ta>
            <ta e="T157" id="Seg_7367" s="T156">n</ta>
            <ta e="T158" id="Seg_7368" s="T157">n</ta>
            <ta e="T159" id="Seg_7369" s="T158">n</ta>
            <ta e="T160" id="Seg_7370" s="T159">v</ta>
            <ta e="T161" id="Seg_7371" s="T160">n</ta>
            <ta e="T162" id="Seg_7372" s="T161">n</ta>
            <ta e="T163" id="Seg_7373" s="T162">adv</ta>
            <ta e="T164" id="Seg_7374" s="T163">n</ta>
            <ta e="T165" id="Seg_7375" s="T164">v</ta>
            <ta e="T166" id="Seg_7376" s="T165">ptcl</ta>
            <ta e="T167" id="Seg_7377" s="T166">n</ta>
            <ta e="T168" id="Seg_7378" s="T167">v</ta>
            <ta e="T169" id="Seg_7379" s="T168">n</ta>
            <ta e="T170" id="Seg_7380" s="T169">n</ta>
            <ta e="T171" id="Seg_7381" s="T170">v</ta>
            <ta e="T172" id="Seg_7382" s="T171">post</ta>
            <ta e="T173" id="Seg_7383" s="T172">v</ta>
            <ta e="T174" id="Seg_7384" s="T173">post</ta>
            <ta e="T175" id="Seg_7385" s="T174">v</ta>
            <ta e="T176" id="Seg_7386" s="T175">aux</ta>
            <ta e="T177" id="Seg_7387" s="T176">dempro</ta>
            <ta e="T178" id="Seg_7388" s="T177">v</ta>
            <ta e="T179" id="Seg_7389" s="T178">v</ta>
            <ta e="T180" id="Seg_7390" s="T179">n</ta>
            <ta e="T181" id="Seg_7391" s="T180">n</ta>
            <ta e="T182" id="Seg_7392" s="T181">n</ta>
            <ta e="T183" id="Seg_7393" s="T182">v</ta>
            <ta e="T184" id="Seg_7394" s="T183">v</ta>
            <ta e="T185" id="Seg_7395" s="T184">dempro</ta>
            <ta e="T186" id="Seg_7396" s="T185">v</ta>
            <ta e="T187" id="Seg_7397" s="T186">n</ta>
            <ta e="T188" id="Seg_7398" s="T187">v</ta>
            <ta e="T189" id="Seg_7399" s="T188">post</ta>
            <ta e="T190" id="Seg_7400" s="T189">adj</ta>
            <ta e="T191" id="Seg_7401" s="T190">n</ta>
            <ta e="T192" id="Seg_7402" s="T191">post</ta>
            <ta e="T193" id="Seg_7403" s="T192">v</ta>
            <ta e="T194" id="Seg_7404" s="T193">v</ta>
            <ta e="T195" id="Seg_7405" s="T194">aux</ta>
            <ta e="T196" id="Seg_7406" s="T195">ptcl</ta>
            <ta e="T197" id="Seg_7407" s="T196">adv</ta>
            <ta e="T198" id="Seg_7408" s="T197">n</ta>
            <ta e="T199" id="Seg_7409" s="T198">n</ta>
            <ta e="T200" id="Seg_7410" s="T199">n</ta>
            <ta e="T201" id="Seg_7411" s="T200">v</ta>
            <ta e="T202" id="Seg_7412" s="T201">v</ta>
            <ta e="T203" id="Seg_7413" s="T202">v</ta>
            <ta e="T204" id="Seg_7414" s="T203">v</ta>
            <ta e="T205" id="Seg_7415" s="T204">n</ta>
            <ta e="T206" id="Seg_7416" s="T205">v</ta>
            <ta e="T207" id="Seg_7417" s="T206">n</ta>
            <ta e="T208" id="Seg_7418" s="T207">v</ta>
            <ta e="T209" id="Seg_7419" s="T208">dempro</ta>
            <ta e="T210" id="Seg_7420" s="T209">v</ta>
            <ta e="T211" id="Seg_7421" s="T210">n</ta>
            <ta e="T212" id="Seg_7422" s="T211">ptcl</ta>
            <ta e="T213" id="Seg_7423" s="T212">v</ta>
            <ta e="T214" id="Seg_7424" s="T213">n</ta>
            <ta e="T215" id="Seg_7425" s="T214">v</ta>
            <ta e="T216" id="Seg_7426" s="T215">n</ta>
            <ta e="T217" id="Seg_7427" s="T216">v</ta>
            <ta e="T218" id="Seg_7428" s="T217">adj</ta>
            <ta e="T219" id="Seg_7429" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_7430" s="T219">dempro</ta>
            <ta e="T221" id="Seg_7431" s="T220">post</ta>
            <ta e="T222" id="Seg_7432" s="T221">ptcl</ta>
            <ta e="T223" id="Seg_7433" s="T222">v</ta>
            <ta e="T224" id="Seg_7434" s="T223">dempro</ta>
            <ta e="T225" id="Seg_7435" s="T224">n</ta>
            <ta e="T226" id="Seg_7436" s="T225">dempro</ta>
            <ta e="T227" id="Seg_7437" s="T226">v</ta>
            <ta e="T228" id="Seg_7438" s="T227">n</ta>
            <ta e="T229" id="Seg_7439" s="T228">ptcl</ta>
            <ta e="T230" id="Seg_7440" s="T229">n</ta>
            <ta e="T231" id="Seg_7441" s="T230">v</ta>
            <ta e="T232" id="Seg_7442" s="T231">adj</ta>
            <ta e="T233" id="Seg_7443" s="T232">n</ta>
            <ta e="T234" id="Seg_7444" s="T233">n</ta>
            <ta e="T235" id="Seg_7445" s="T234">n</ta>
            <ta e="T236" id="Seg_7446" s="T235">v</ta>
            <ta e="T237" id="Seg_7447" s="T236">n</ta>
            <ta e="T238" id="Seg_7448" s="T237">n</ta>
            <ta e="T239" id="Seg_7449" s="T238">n</ta>
            <ta e="T240" id="Seg_7450" s="T239">v</ta>
            <ta e="T241" id="Seg_7451" s="T240">dempro</ta>
            <ta e="T242" id="Seg_7452" s="T241">n</ta>
            <ta e="T243" id="Seg_7453" s="T242">n</ta>
            <ta e="T244" id="Seg_7454" s="T243">v</ta>
            <ta e="T245" id="Seg_7455" s="T244">dempro</ta>
            <ta e="T246" id="Seg_7456" s="T245">n</ta>
            <ta e="T247" id="Seg_7457" s="T246">v</ta>
            <ta e="T248" id="Seg_7458" s="T247">cardnum</ta>
            <ta e="T249" id="Seg_7459" s="T248">n</ta>
            <ta e="T250" id="Seg_7460" s="T249">v</ta>
            <ta e="T251" id="Seg_7461" s="T250">aux</ta>
            <ta e="T252" id="Seg_7462" s="T251">adv</ta>
            <ta e="T253" id="Seg_7463" s="T252">n</ta>
            <ta e="T254" id="Seg_7464" s="T253">adj</ta>
            <ta e="T255" id="Seg_7465" s="T254">n</ta>
            <ta e="T256" id="Seg_7466" s="T255">n</ta>
            <ta e="T257" id="Seg_7467" s="T256">v</ta>
            <ta e="T258" id="Seg_7468" s="T257">aux</ta>
            <ta e="T259" id="Seg_7469" s="T258">adj</ta>
            <ta e="T260" id="Seg_7470" s="T259">post</ta>
            <ta e="T261" id="Seg_7471" s="T260">n</ta>
            <ta e="T262" id="Seg_7472" s="T261">v</ta>
            <ta e="T263" id="Seg_7473" s="T262">dempro</ta>
            <ta e="T264" id="Seg_7474" s="T263">n</ta>
            <ta e="T265" id="Seg_7475" s="T264">adv</ta>
            <ta e="T266" id="Seg_7476" s="T265">ptcl</ta>
            <ta e="T267" id="Seg_7477" s="T266">que</ta>
            <ta e="T268" id="Seg_7478" s="T267">cardnum</ta>
            <ta e="T269" id="Seg_7479" s="T268">ptcl</ta>
            <ta e="T270" id="Seg_7480" s="T269">v</ta>
            <ta e="T271" id="Seg_7481" s="T270">n</ta>
            <ta e="T272" id="Seg_7482" s="T271">n</ta>
            <ta e="T273" id="Seg_7483" s="T272">v</ta>
            <ta e="T274" id="Seg_7484" s="T273">aux</ta>
            <ta e="T275" id="Seg_7485" s="T274">adv</ta>
            <ta e="T276" id="Seg_7486" s="T275">v</ta>
            <ta e="T277" id="Seg_7487" s="T276">dempro</ta>
            <ta e="T278" id="Seg_7488" s="T277">n</ta>
            <ta e="T279" id="Seg_7489" s="T278">dempro</ta>
            <ta e="T280" id="Seg_7490" s="T279">n</ta>
            <ta e="T281" id="Seg_7491" s="T280">v</ta>
            <ta e="T282" id="Seg_7492" s="T281">aux</ta>
            <ta e="T283" id="Seg_7493" s="T282">v</ta>
            <ta e="T284" id="Seg_7494" s="T283">v</ta>
            <ta e="T285" id="Seg_7495" s="T284">adj</ta>
            <ta e="T286" id="Seg_7496" s="T285">n</ta>
            <ta e="T287" id="Seg_7497" s="T286">ptcl</ta>
            <ta e="T288" id="Seg_7498" s="T287">adj</ta>
            <ta e="T289" id="Seg_7499" s="T288">n</ta>
            <ta e="T290" id="Seg_7500" s="T289">v</ta>
            <ta e="T291" id="Seg_7501" s="T290">aux</ta>
            <ta e="T292" id="Seg_7502" s="T291">adj</ta>
            <ta e="T293" id="Seg_7503" s="T292">n</ta>
            <ta e="T294" id="Seg_7504" s="T293">v</ta>
            <ta e="T295" id="Seg_7505" s="T294">aux</ta>
            <ta e="T296" id="Seg_7506" s="T295">n</ta>
            <ta e="T297" id="Seg_7507" s="T296">n</ta>
            <ta e="T298" id="Seg_7508" s="T297">adv</ta>
            <ta e="T299" id="Seg_7509" s="T298">dempro</ta>
            <ta e="T300" id="Seg_7510" s="T299">adj</ta>
            <ta e="T301" id="Seg_7511" s="T300">n</ta>
            <ta e="T302" id="Seg_7512" s="T301">v</ta>
            <ta e="T303" id="Seg_7513" s="T302">dempro</ta>
            <ta e="T304" id="Seg_7514" s="T303">n</ta>
            <ta e="T305" id="Seg_7515" s="T304">v</ta>
            <ta e="T306" id="Seg_7516" s="T305">v</ta>
            <ta e="T307" id="Seg_7517" s="T306">v</ta>
            <ta e="T308" id="Seg_7518" s="T307">v</ta>
            <ta e="T309" id="Seg_7519" s="T308">v</ta>
            <ta e="T310" id="Seg_7520" s="T309">adj</ta>
            <ta e="T311" id="Seg_7521" s="T310">conj</ta>
            <ta e="T312" id="Seg_7522" s="T311">n</ta>
            <ta e="T313" id="Seg_7523" s="T312">quant</ta>
            <ta e="T314" id="Seg_7524" s="T313">adj</ta>
            <ta e="T315" id="Seg_7525" s="T314">conj</ta>
            <ta e="T316" id="Seg_7526" s="T315">n</ta>
            <ta e="T317" id="Seg_7527" s="T316">quant</ta>
            <ta e="T318" id="Seg_7528" s="T317">dempro</ta>
            <ta e="T319" id="Seg_7529" s="T318">n</ta>
            <ta e="T320" id="Seg_7530" s="T319">v</ta>
            <ta e="T321" id="Seg_7531" s="T320">post</ta>
            <ta e="T322" id="Seg_7532" s="T321">n</ta>
            <ta e="T323" id="Seg_7533" s="T322">n</ta>
            <ta e="T324" id="Seg_7534" s="T323">v</ta>
            <ta e="T325" id="Seg_7535" s="T324">aux</ta>
            <ta e="T326" id="Seg_7536" s="T325">dempro</ta>
            <ta e="T327" id="Seg_7537" s="T326">post</ta>
            <ta e="T328" id="Seg_7538" s="T327">que</ta>
            <ta e="T329" id="Seg_7539" s="T328">ptcl</ta>
            <ta e="T330" id="Seg_7540" s="T329">n</ta>
            <ta e="T331" id="Seg_7541" s="T330">v</ta>
            <ta e="T332" id="Seg_7542" s="T331">cardnum</ta>
            <ta e="T333" id="Seg_7543" s="T332">n</ta>
            <ta e="T334" id="Seg_7544" s="T333">post</ta>
            <ta e="T335" id="Seg_7545" s="T334">que</ta>
            <ta e="T336" id="Seg_7546" s="T335">ptcl</ta>
            <ta e="T337" id="Seg_7547" s="T336">n</ta>
            <ta e="T338" id="Seg_7548" s="T337">v</ta>
            <ta e="T339" id="Seg_7549" s="T338">ptcl</ta>
            <ta e="T340" id="Seg_7550" s="T339">n</ta>
            <ta e="T341" id="Seg_7551" s="T340">cop</ta>
            <ta e="T342" id="Seg_7552" s="T341">conj</ta>
            <ta e="T343" id="Seg_7553" s="T342">n</ta>
            <ta e="T344" id="Seg_7554" s="T343">adj</ta>
            <ta e="T345" id="Seg_7555" s="T344">n</ta>
            <ta e="T346" id="Seg_7556" s="T345">n</ta>
            <ta e="T347" id="Seg_7557" s="T346">v</ta>
            <ta e="T348" id="Seg_7558" s="T347">dempro</ta>
            <ta e="T349" id="Seg_7559" s="T348">v</ta>
            <ta e="T350" id="Seg_7560" s="T349">v</ta>
            <ta e="T351" id="Seg_7561" s="T350">v</ta>
            <ta e="T352" id="Seg_7562" s="T351">v</ta>
            <ta e="T353" id="Seg_7563" s="T352">v</ta>
            <ta e="T354" id="Seg_7564" s="T353">aux</ta>
            <ta e="T355" id="Seg_7565" s="T354">dempro</ta>
            <ta e="T356" id="Seg_7566" s="T355">n</ta>
            <ta e="T357" id="Seg_7567" s="T356">post</ta>
            <ta e="T358" id="Seg_7568" s="T357">v</ta>
            <ta e="T359" id="Seg_7569" s="T358">v</ta>
            <ta e="T360" id="Seg_7570" s="T359">n</ta>
            <ta e="T361" id="Seg_7571" s="T360">que</ta>
            <ta e="T362" id="Seg_7572" s="T361">cardnum</ta>
            <ta e="T363" id="Seg_7573" s="T362">post</ta>
            <ta e="T364" id="Seg_7574" s="T363">v</ta>
            <ta e="T365" id="Seg_7575" s="T364">n</ta>
            <ta e="T366" id="Seg_7576" s="T365">que</ta>
            <ta e="T367" id="Seg_7577" s="T366">post</ta>
            <ta e="T368" id="Seg_7578" s="T367">v</ta>
            <ta e="T369" id="Seg_7579" s="T368">n</ta>
            <ta e="T370" id="Seg_7580" s="T369">dempro</ta>
            <ta e="T371" id="Seg_7581" s="T370">post</ta>
            <ta e="T372" id="Seg_7582" s="T371">v</ta>
            <ta e="T373" id="Seg_7583" s="T372">n</ta>
            <ta e="T374" id="Seg_7584" s="T373">dempro</ta>
            <ta e="T375" id="Seg_7585" s="T374">v</ta>
            <ta e="T376" id="Seg_7586" s="T375">post</ta>
            <ta e="T377" id="Seg_7587" s="T376">v</ta>
            <ta e="T378" id="Seg_7588" s="T377">n</ta>
            <ta e="T379" id="Seg_7589" s="T378">adv</ta>
            <ta e="T380" id="Seg_7590" s="T379">v</ta>
            <ta e="T381" id="Seg_7591" s="T380">adv</ta>
            <ta e="T382" id="Seg_7592" s="T381">v</ta>
            <ta e="T383" id="Seg_7593" s="T382">adv</ta>
            <ta e="T384" id="Seg_7594" s="T383">v</ta>
            <ta e="T385" id="Seg_7595" s="T384">n</ta>
            <ta e="T386" id="Seg_7596" s="T385">n</ta>
            <ta e="T387" id="Seg_7597" s="T386">v</ta>
            <ta e="T388" id="Seg_7598" s="T387">n</ta>
            <ta e="T389" id="Seg_7599" s="T388">pers</ta>
            <ta e="T390" id="Seg_7600" s="T389">v</ta>
            <ta e="T391" id="Seg_7601" s="T390">n</ta>
            <ta e="T392" id="Seg_7602" s="T391">adj</ta>
            <ta e="T393" id="Seg_7603" s="T392">v</ta>
            <ta e="T394" id="Seg_7604" s="T393">v</ta>
            <ta e="T395" id="Seg_7605" s="T394">ptcl</ta>
            <ta e="T396" id="Seg_7606" s="T395">ordnum</ta>
            <ta e="T397" id="Seg_7607" s="T396">n</ta>
            <ta e="T398" id="Seg_7608" s="T397">n</ta>
            <ta e="T399" id="Seg_7609" s="T398">v</ta>
            <ta e="T400" id="Seg_7610" s="T399">v</ta>
            <ta e="T401" id="Seg_7611" s="T400">n</ta>
            <ta e="T402" id="Seg_7612" s="T401">v</ta>
            <ta e="T403" id="Seg_7613" s="T402">post</ta>
            <ta e="T404" id="Seg_7614" s="T403">v</ta>
            <ta e="T405" id="Seg_7615" s="T404">dempro</ta>
            <ta e="T406" id="Seg_7616" s="T405">v</ta>
            <ta e="T407" id="Seg_7617" s="T406">post</ta>
            <ta e="T408" id="Seg_7618" s="T407">n</ta>
            <ta e="T409" id="Seg_7619" s="T408">n</ta>
            <ta e="T410" id="Seg_7620" s="T409">v</ta>
            <ta e="T411" id="Seg_7621" s="T410">adv</ta>
            <ta e="T412" id="Seg_7622" s="T411">v</ta>
            <ta e="T413" id="Seg_7623" s="T412">post</ta>
            <ta e="T414" id="Seg_7624" s="T413">n</ta>
            <ta e="T415" id="Seg_7625" s="T414">v</ta>
            <ta e="T416" id="Seg_7626" s="T415">dempro</ta>
            <ta e="T417" id="Seg_7627" s="T416">v</ta>
            <ta e="T418" id="Seg_7628" s="T417">v</ta>
            <ta e="T419" id="Seg_7629" s="T418">n</ta>
            <ta e="T420" id="Seg_7630" s="T419">n</ta>
            <ta e="T421" id="Seg_7631" s="T420">v</ta>
            <ta e="T422" id="Seg_7632" s="T421">ptcl</ta>
            <ta e="T423" id="Seg_7633" s="T422">que</ta>
            <ta e="T424" id="Seg_7634" s="T423">ptcl</ta>
            <ta e="T425" id="Seg_7635" s="T424">ptcl</ta>
            <ta e="T426" id="Seg_7636" s="T425">adv</ta>
            <ta e="T427" id="Seg_7637" s="T426">adv</ta>
            <ta e="T428" id="Seg_7638" s="T427">v</ta>
            <ta e="T429" id="Seg_7639" s="T428">v</ta>
            <ta e="T430" id="Seg_7640" s="T429">aux</ta>
            <ta e="T431" id="Seg_7641" s="T430">dempro</ta>
            <ta e="T432" id="Seg_7642" s="T431">conj</ta>
            <ta e="T433" id="Seg_7643" s="T432">cop</ta>
            <ta e="T434" id="Seg_7644" s="T433">n</ta>
            <ta e="T435" id="Seg_7645" s="T434">v</ta>
            <ta e="T436" id="Seg_7646" s="T435">ptcl</ta>
            <ta e="T437" id="Seg_7647" s="T436">n</ta>
            <ta e="T438" id="Seg_7648" s="T437">post</ta>
            <ta e="T439" id="Seg_7649" s="T438">cardnum</ta>
            <ta e="T440" id="Seg_7650" s="T439">adj</ta>
            <ta e="T441" id="Seg_7651" s="T440">n</ta>
            <ta e="T442" id="Seg_7652" s="T441">cop</ta>
            <ta e="T443" id="Seg_7653" s="T442">v</ta>
            <ta e="T444" id="Seg_7654" s="T443">v</ta>
            <ta e="T445" id="Seg_7655" s="T444">n</ta>
            <ta e="T446" id="Seg_7656" s="T445">adj</ta>
            <ta e="T447" id="Seg_7657" s="T446">n</ta>
            <ta e="T448" id="Seg_7658" s="T447">post</ta>
            <ta e="T449" id="Seg_7659" s="T448">n</ta>
            <ta e="T450" id="Seg_7660" s="T449">ptcl</ta>
            <ta e="T451" id="Seg_7661" s="T450">v</ta>
            <ta e="T452" id="Seg_7662" s="T451">n</ta>
            <ta e="T453" id="Seg_7663" s="T452">adj</ta>
            <ta e="T454" id="Seg_7664" s="T453">adv</ta>
            <ta e="T455" id="Seg_7665" s="T454">v</ta>
            <ta e="T456" id="Seg_7666" s="T455">n</ta>
            <ta e="T457" id="Seg_7667" s="T456">ptcl</ta>
            <ta e="T458" id="Seg_7668" s="T457">dempro</ta>
            <ta e="T459" id="Seg_7669" s="T458">post</ta>
            <ta e="T460" id="Seg_7670" s="T459">v</ta>
            <ta e="T461" id="Seg_7671" s="T460">v</ta>
            <ta e="T462" id="Seg_7672" s="T461">ptcl</ta>
            <ta e="T463" id="Seg_7673" s="T462">ptcl</ta>
            <ta e="T464" id="Seg_7674" s="T463">dempro</ta>
            <ta e="T465" id="Seg_7675" s="T464">dempro</ta>
            <ta e="T466" id="Seg_7676" s="T465">n</ta>
            <ta e="T467" id="Seg_7677" s="T466">v</ta>
            <ta e="T468" id="Seg_7678" s="T467">dempro</ta>
            <ta e="T469" id="Seg_7679" s="T468">n</ta>
            <ta e="T470" id="Seg_7680" s="T469">v</ta>
            <ta e="T471" id="Seg_7681" s="T470">conj</ta>
            <ta e="T472" id="Seg_7682" s="T471">v</ta>
            <ta e="T473" id="Seg_7683" s="T472">dempro</ta>
            <ta e="T474" id="Seg_7684" s="T473">n</ta>
            <ta e="T475" id="Seg_7685" s="T474">n</ta>
            <ta e="T476" id="Seg_7686" s="T475">v</ta>
            <ta e="T477" id="Seg_7687" s="T476">v</ta>
            <ta e="T478" id="Seg_7688" s="T477">v</ta>
            <ta e="T479" id="Seg_7689" s="T478">n</ta>
            <ta e="T480" id="Seg_7690" s="T479">v</ta>
            <ta e="T481" id="Seg_7691" s="T480">interj</ta>
            <ta e="T482" id="Seg_7692" s="T481">n</ta>
            <ta e="T483" id="Seg_7693" s="T482">v</ta>
            <ta e="T484" id="Seg_7694" s="T483">v</ta>
            <ta e="T485" id="Seg_7695" s="T484">v</ta>
            <ta e="T486" id="Seg_7696" s="T485">ptcl</ta>
            <ta e="T487" id="Seg_7697" s="T486">v</ta>
            <ta e="T488" id="Seg_7698" s="T487">dempro</ta>
            <ta e="T489" id="Seg_7699" s="T488">v</ta>
            <ta e="T490" id="Seg_7700" s="T489">v</ta>
            <ta e="T491" id="Seg_7701" s="T490">n</ta>
            <ta e="T492" id="Seg_7702" s="T491">cop</ta>
            <ta e="T493" id="Seg_7703" s="T492">dempro</ta>
            <ta e="T494" id="Seg_7704" s="T493">n</ta>
            <ta e="T495" id="Seg_7705" s="T494">n</ta>
            <ta e="T496" id="Seg_7706" s="T495">adv</ta>
            <ta e="T497" id="Seg_7707" s="T496">v</ta>
            <ta e="T498" id="Seg_7708" s="T497">interj</ta>
            <ta e="T499" id="Seg_7709" s="T498">n</ta>
            <ta e="T500" id="Seg_7710" s="T499">n</ta>
            <ta e="T501" id="Seg_7711" s="T500">v</ta>
            <ta e="T502" id="Seg_7712" s="T501">v</ta>
            <ta e="T503" id="Seg_7713" s="T502">n</ta>
            <ta e="T504" id="Seg_7714" s="T503">n</ta>
            <ta e="T505" id="Seg_7715" s="T504">v</ta>
            <ta e="T506" id="Seg_7716" s="T505">v</ta>
            <ta e="T507" id="Seg_7717" s="T506">n</ta>
            <ta e="T508" id="Seg_7718" s="T507">v</ta>
            <ta e="T509" id="Seg_7719" s="T508">post</ta>
            <ta e="T510" id="Seg_7720" s="T509">n</ta>
            <ta e="T511" id="Seg_7721" s="T510">v</ta>
            <ta e="T512" id="Seg_7722" s="T511">dempro</ta>
            <ta e="T513" id="Seg_7723" s="T512">n</ta>
            <ta e="T514" id="Seg_7724" s="T513">v</ta>
            <ta e="T515" id="Seg_7725" s="T514">v</ta>
            <ta e="T516" id="Seg_7726" s="T515">post</ta>
            <ta e="T517" id="Seg_7727" s="T516">v</ta>
            <ta e="T518" id="Seg_7728" s="T517">n</ta>
            <ta e="T519" id="Seg_7729" s="T518">v</ta>
            <ta e="T520" id="Seg_7730" s="T519">post</ta>
            <ta e="T521" id="Seg_7731" s="T520">n</ta>
            <ta e="T522" id="Seg_7732" s="T521">n</ta>
            <ta e="T523" id="Seg_7733" s="T522">v</ta>
            <ta e="T524" id="Seg_7734" s="T523">aux</ta>
            <ta e="T525" id="Seg_7735" s="T524">dempro</ta>
            <ta e="T526" id="Seg_7736" s="T525">v</ta>
            <ta e="T527" id="Seg_7737" s="T526">post</ta>
            <ta e="T528" id="Seg_7738" s="T527">n</ta>
            <ta e="T529" id="Seg_7739" s="T528">v</ta>
            <ta e="T530" id="Seg_7740" s="T529">adv</ta>
            <ta e="T531" id="Seg_7741" s="T530">n</ta>
            <ta e="T532" id="Seg_7742" s="T531">n</ta>
            <ta e="T533" id="Seg_7743" s="T532">v</ta>
            <ta e="T534" id="Seg_7744" s="T533">adj</ta>
            <ta e="T535" id="Seg_7745" s="T534">n</ta>
            <ta e="T536" id="Seg_7746" s="T535">n</ta>
            <ta e="T537" id="Seg_7747" s="T536">v</ta>
            <ta e="T538" id="Seg_7748" s="T537">adv</ta>
            <ta e="T539" id="Seg_7749" s="T538">v</ta>
            <ta e="T540" id="Seg_7750" s="T539">n</ta>
            <ta e="T541" id="Seg_7751" s="T540">n</ta>
            <ta e="T542" id="Seg_7752" s="T541">n</ta>
            <ta e="T543" id="Seg_7753" s="T542">v</ta>
            <ta e="T544" id="Seg_7754" s="T543">ptcl</ta>
            <ta e="T545" id="Seg_7755" s="T544">n</ta>
            <ta e="T546" id="Seg_7756" s="T545">v</ta>
            <ta e="T547" id="Seg_7757" s="T546">v</ta>
            <ta e="T548" id="Seg_7758" s="T547">interj</ta>
            <ta e="T549" id="Seg_7759" s="T548">dempro</ta>
            <ta e="T550" id="Seg_7760" s="T549">n</ta>
            <ta e="T551" id="Seg_7761" s="T550">adj</ta>
            <ta e="T552" id="Seg_7762" s="T551">adj</ta>
            <ta e="T553" id="Seg_7763" s="T552">n</ta>
            <ta e="T554" id="Seg_7764" s="T553">ptcl</ta>
            <ta e="T555" id="Seg_7765" s="T554">n</ta>
            <ta e="T556" id="Seg_7766" s="T555">ptcl</ta>
            <ta e="T557" id="Seg_7767" s="T556">v</ta>
            <ta e="T558" id="Seg_7768" s="T557">v</ta>
            <ta e="T559" id="Seg_7769" s="T558">adj</ta>
            <ta e="T560" id="Seg_7770" s="T559">n</ta>
            <ta e="T561" id="Seg_7771" s="T560">ptcl</ta>
            <ta e="T562" id="Seg_7772" s="T561">v</ta>
            <ta e="T563" id="Seg_7773" s="T562">aux</ta>
            <ta e="T564" id="Seg_7774" s="T563">adj</ta>
            <ta e="T565" id="Seg_7775" s="T564">n</ta>
            <ta e="T566" id="Seg_7776" s="T565">dempro</ta>
            <ta e="T567" id="Seg_7777" s="T566">interj</ta>
            <ta e="T568" id="Seg_7778" s="T567">v</ta>
            <ta e="T569" id="Seg_7779" s="T568">dempro</ta>
            <ta e="T570" id="Seg_7780" s="T569">ptcl</ta>
            <ta e="T571" id="Seg_7781" s="T570">v</ta>
            <ta e="T572" id="Seg_7782" s="T571">ptcl</ta>
            <ta e="T573" id="Seg_7783" s="T572">ptcl</ta>
            <ta e="T574" id="Seg_7784" s="T573">adv</ta>
            <ta e="T575" id="Seg_7785" s="T574">v</ta>
            <ta e="T576" id="Seg_7786" s="T575">v</ta>
            <ta e="T577" id="Seg_7787" s="T576">ptcl</ta>
            <ta e="T578" id="Seg_7788" s="T577">v</ta>
            <ta e="T579" id="Seg_7789" s="T578">ptcl</ta>
            <ta e="T580" id="Seg_7790" s="T579">n</ta>
            <ta e="T581" id="Seg_7791" s="T580">n</ta>
            <ta e="T582" id="Seg_7792" s="T581">post</ta>
            <ta e="T583" id="Seg_7793" s="T582">v</ta>
            <ta e="T584" id="Seg_7794" s="T583">conj</ta>
            <ta e="T585" id="Seg_7795" s="T584">adv</ta>
            <ta e="T586" id="Seg_7796" s="T585">cardnum</ta>
            <ta e="T587" id="Seg_7797" s="T586">n</ta>
            <ta e="T588" id="Seg_7798" s="T587">n</ta>
            <ta e="T589" id="Seg_7799" s="T588">adv</ta>
            <ta e="T590" id="Seg_7800" s="T589">v</ta>
            <ta e="T591" id="Seg_7801" s="T590">aux</ta>
            <ta e="T592" id="Seg_7802" s="T591">n</ta>
            <ta e="T593" id="Seg_7803" s="T592">que</ta>
            <ta e="T594" id="Seg_7804" s="T593">cop</ta>
            <ta e="T595" id="Seg_7805" s="T594">que</ta>
            <ta e="T596" id="Seg_7806" s="T595">v</ta>
            <ta e="T597" id="Seg_7807" s="T596">conj</ta>
            <ta e="T598" id="Seg_7808" s="T597">adv</ta>
            <ta e="T601" id="Seg_7809" s="T598">adv</ta>
            <ta e="T599" id="Seg_7810" s="T601">adv</ta>
            <ta e="T600" id="Seg_7811" s="T599">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_7812" s="T0">np.h:Th</ta>
            <ta e="T2" id="Seg_7813" s="T1">np.h:Th</ta>
            <ta e="T6" id="Seg_7814" s="T5">np.h:Th</ta>
            <ta e="T7" id="Seg_7815" s="T6">0.3.h:Poss</ta>
            <ta e="T8" id="Seg_7816" s="T7">np:L</ta>
            <ta e="T9" id="Seg_7817" s="T8">0.3.h:Th</ta>
            <ta e="T10" id="Seg_7818" s="T9">0.3.h:Poss np:Th</ta>
            <ta e="T14" id="Seg_7819" s="T13">0.3.h:Th</ta>
            <ta e="T15" id="Seg_7820" s="T14">adv:G</ta>
            <ta e="T16" id="Seg_7821" s="T15">0.3.h:A</ta>
            <ta e="T17" id="Seg_7822" s="T16">np:Th</ta>
            <ta e="T20" id="Seg_7823" s="T19">pro:Th</ta>
            <ta e="T22" id="Seg_7824" s="T21">0.3.h:Poss</ta>
            <ta e="T24" id="Seg_7825" s="T23">0.3.h:Poss</ta>
            <ta e="T27" id="Seg_7826" s="T26">0.3.h:Poss np:Th</ta>
            <ta e="T30" id="Seg_7827" s="T29">0.3.h:Poss</ta>
            <ta e="T33" id="Seg_7828" s="T32">0.3.h:Poss np:Th</ta>
            <ta e="T34" id="Seg_7829" s="T33">pro:G</ta>
            <ta e="T36" id="Seg_7830" s="T35">np:Th</ta>
            <ta e="T38" id="Seg_7831" s="T37">pro:P</ta>
            <ta e="T39" id="Seg_7832" s="T38">0.3.h:A</ta>
            <ta e="T40" id="Seg_7833" s="T39">np:L</ta>
            <ta e="T41" id="Seg_7834" s="T40">0.3.h:A</ta>
            <ta e="T48" id="Seg_7835" s="T47">np.h:A</ta>
            <ta e="T51" id="Seg_7836" s="T50">np:G</ta>
            <ta e="T53" id="Seg_7837" s="T52">0.3.h:A</ta>
            <ta e="T54" id="Seg_7838" s="T53">0.3.h:A</ta>
            <ta e="T57" id="Seg_7839" s="T56">0.1.h:A</ta>
            <ta e="T58" id="Seg_7840" s="T57">0.1.h:A</ta>
            <ta e="T60" id="Seg_7841" s="T59">0.1.h:Poss np:Th</ta>
            <ta e="T61" id="Seg_7842" s="T60">0.1.h:A</ta>
            <ta e="T65" id="Seg_7843" s="T64">0.1.h:P</ta>
            <ta e="T67" id="Seg_7844" s="T66">0.3:Th</ta>
            <ta e="T70" id="Seg_7845" s="T69">pro.h:Poss</ta>
            <ta e="T72" id="Seg_7846" s="T71">0.3.h:A</ta>
            <ta e="T73" id="Seg_7847" s="T72">0.2.h:A</ta>
            <ta e="T75" id="Seg_7848" s="T74">0.1.h:A</ta>
            <ta e="T76" id="Seg_7849" s="T75">np.h:A</ta>
            <ta e="T79" id="Seg_7850" s="T78">0.3.h:A</ta>
            <ta e="T80" id="Seg_7851" s="T79">np.h:A</ta>
            <ta e="T87" id="Seg_7852" s="T86">np.h:A</ta>
            <ta e="T89" id="Seg_7853" s="T88">0.3.h:A</ta>
            <ta e="T90" id="Seg_7854" s="T89">0.3.h:A</ta>
            <ta e="T91" id="Seg_7855" s="T90">0.1.h:Poss np:Poss</ta>
            <ta e="T92" id="Seg_7856" s="T91">np:Th</ta>
            <ta e="T94" id="Seg_7857" s="T93">0.1.h:Poss</ta>
            <ta e="T95" id="Seg_7858" s="T94">adv:G</ta>
            <ta e="T97" id="Seg_7859" s="T96">0.2.h:A</ta>
            <ta e="T99" id="Seg_7860" s="T98">np.h:A</ta>
            <ta e="T100" id="Seg_7861" s="T99">np:Th</ta>
            <ta e="T102" id="Seg_7862" s="T101">np.h:E</ta>
            <ta e="T103" id="Seg_7863" s="T102">0.3.h:Poss np:Th</ta>
            <ta e="T106" id="Seg_7864" s="T105">np.h:A</ta>
            <ta e="T108" id="Seg_7865" s="T107">0.2.h:A</ta>
            <ta e="T110" id="Seg_7866" s="T109">np:G</ta>
            <ta e="T111" id="Seg_7867" s="T110">np:G</ta>
            <ta e="T112" id="Seg_7868" s="T111">np:Th</ta>
            <ta e="T113" id="Seg_7869" s="T112">0.2.h:A</ta>
            <ta e="T114" id="Seg_7870" s="T113">0.2.h:A</ta>
            <ta e="T115" id="Seg_7871" s="T114">0.2.h:A</ta>
            <ta e="T118" id="Seg_7872" s="T117">0.2.h:A</ta>
            <ta e="T121" id="Seg_7873" s="T120">np.h:A</ta>
            <ta e="T122" id="Seg_7874" s="T121">0.3.h:A</ta>
            <ta e="T124" id="Seg_7875" s="T123">np:G</ta>
            <ta e="T126" id="Seg_7876" s="T125">np:Th</ta>
            <ta e="T130" id="Seg_7877" s="T129">np:Th</ta>
            <ta e="T131" id="Seg_7878" s="T130">0.3.h:A</ta>
            <ta e="T132" id="Seg_7879" s="T131">0.3.h:E</ta>
            <ta e="T133" id="Seg_7880" s="T132">np:Th</ta>
            <ta e="T137" id="Seg_7881" s="T136">np:Th</ta>
            <ta e="T138" id="Seg_7882" s="T137">0.2.h:A</ta>
            <ta e="T140" id="Seg_7883" s="T139">0.1.h:Th</ta>
            <ta e="T141" id="Seg_7884" s="T140">0.3.h:A</ta>
            <ta e="T144" id="Seg_7885" s="T143">np:G</ta>
            <ta e="T146" id="Seg_7886" s="T145">0.2.h:A</ta>
            <ta e="T147" id="Seg_7887" s="T146">0.3.h:A</ta>
            <ta e="T148" id="Seg_7888" s="T147">np.h:A</ta>
            <ta e="T150" id="Seg_7889" s="T149">np:Th</ta>
            <ta e="T156" id="Seg_7890" s="T155">0.3.h:P</ta>
            <ta e="T157" id="Seg_7891" s="T156">np.h:A</ta>
            <ta e="T159" id="Seg_7892" s="T158">np:G</ta>
            <ta e="T160" id="Seg_7893" s="T159">0.3.h:A</ta>
            <ta e="T161" id="Seg_7894" s="T160">np:Th</ta>
            <ta e="T162" id="Seg_7895" s="T161">np:G</ta>
            <ta e="T163" id="Seg_7896" s="T162">adv:L</ta>
            <ta e="T164" id="Seg_7897" s="T163">0.3.h:Poss np:Th</ta>
            <ta e="T167" id="Seg_7898" s="T166">0.3.h:Poss np.h:St</ta>
            <ta e="T168" id="Seg_7899" s="T167">0.3.h:E</ta>
            <ta e="T169" id="Seg_7900" s="T168">np:Poss</ta>
            <ta e="T170" id="Seg_7901" s="T169">np:Th</ta>
            <ta e="T171" id="Seg_7902" s="T170">0.3.h:A</ta>
            <ta e="T173" id="Seg_7903" s="T172">0.3.h:A</ta>
            <ta e="T176" id="Seg_7904" s="T175">0.3.h:A</ta>
            <ta e="T180" id="Seg_7905" s="T179">np.h:Poss</ta>
            <ta e="T181" id="Seg_7906" s="T180">np:P</ta>
            <ta e="T182" id="Seg_7907" s="T181">np:Ins</ta>
            <ta e="T184" id="Seg_7908" s="T183">0.3.h:A</ta>
            <ta e="T185" id="Seg_7909" s="T184">pro.h:A</ta>
            <ta e="T186" id="Seg_7910" s="T185">0.3.h:A</ta>
            <ta e="T187" id="Seg_7911" s="T186">np:G</ta>
            <ta e="T188" id="Seg_7912" s="T187">0.3.h:A</ta>
            <ta e="T192" id="Seg_7913" s="T190">pp:G</ta>
            <ta e="T193" id="Seg_7914" s="T192">0.3.h:A</ta>
            <ta e="T197" id="Seg_7915" s="T196">adv:L</ta>
            <ta e="T199" id="Seg_7916" s="T198">np:Poss</ta>
            <ta e="T200" id="Seg_7917" s="T199">np:L</ta>
            <ta e="T201" id="Seg_7918" s="T200">0.3.h:A</ta>
            <ta e="T202" id="Seg_7919" s="T201">0.3.h:E</ta>
            <ta e="T203" id="Seg_7920" s="T202">0.3.h:A</ta>
            <ta e="T204" id="Seg_7921" s="T203">0.3.h:E</ta>
            <ta e="T206" id="Seg_7922" s="T205">np:Th</ta>
            <ta e="T207" id="Seg_7923" s="T206">np:G</ta>
            <ta e="T210" id="Seg_7924" s="T209">0.3.h:Th</ta>
            <ta e="T211" id="Seg_7925" s="T210">np:St</ta>
            <ta e="T221" id="Seg_7926" s="T219">pp:G</ta>
            <ta e="T225" id="Seg_7927" s="T224">np.h:A</ta>
            <ta e="T227" id="Seg_7928" s="T226">0.3.h:A</ta>
            <ta e="T230" id="Seg_7929" s="T229">np:Th</ta>
            <ta e="T234" id="Seg_7930" s="T233">np:G</ta>
            <ta e="T235" id="Seg_7931" s="T234">np:Th</ta>
            <ta e="T239" id="Seg_7932" s="T238">np:G</ta>
            <ta e="T240" id="Seg_7933" s="T239">0.3.h:Th</ta>
            <ta e="T242" id="Seg_7934" s="T241">np.h:A</ta>
            <ta e="T243" id="Seg_7935" s="T242">np:Th</ta>
            <ta e="T246" id="Seg_7936" s="T245">np:Th</ta>
            <ta e="T249" id="Seg_7937" s="T248">np:P</ta>
            <ta e="T256" id="Seg_7938" s="T255">np:Th</ta>
            <ta e="T261" id="Seg_7939" s="T260">np:Th</ta>
            <ta e="T264" id="Seg_7940" s="T263">np:L</ta>
            <ta e="T265" id="Seg_7941" s="T264">adv:L</ta>
            <ta e="T271" id="Seg_7942" s="T270">np:Th</ta>
            <ta e="T272" id="Seg_7943" s="T271">np:Th</ta>
            <ta e="T278" id="Seg_7944" s="T277">np.h:A</ta>
            <ta e="T280" id="Seg_7945" s="T279">np:Th</ta>
            <ta e="T284" id="Seg_7946" s="T283">0.3.h:A</ta>
            <ta e="T285" id="Seg_7947" s="T284">0.3:Th</ta>
            <ta e="T286" id="Seg_7948" s="T285">np.h:Th</ta>
            <ta e="T289" id="Seg_7949" s="T288">np:Th</ta>
            <ta e="T293" id="Seg_7950" s="T292">np:Th</ta>
            <ta e="T301" id="Seg_7951" s="T300">np:Th</ta>
            <ta e="T304" id="Seg_7952" s="T303">np.h:A</ta>
            <ta e="T305" id="Seg_7953" s="T304">0.3.h:A</ta>
            <ta e="T306" id="Seg_7954" s="T305">np:P</ta>
            <ta e="T308" id="Seg_7955" s="T307">np:P</ta>
            <ta e="T309" id="Seg_7956" s="T308">0.3.h:A</ta>
            <ta e="T312" id="Seg_7957" s="T311">np:Th</ta>
            <ta e="T316" id="Seg_7958" s="T315">np:Th</ta>
            <ta e="T319" id="Seg_7959" s="T318">np.h:A</ta>
            <ta e="T320" id="Seg_7960" s="T319">0.3.h:A</ta>
            <ta e="T323" id="Seg_7961" s="T322">np:Th</ta>
            <ta e="T327" id="Seg_7962" s="T325">pp:Time</ta>
            <ta e="T330" id="Seg_7963" s="T329">np.h:Th</ta>
            <ta e="T334" id="Seg_7964" s="T332">pp:Time</ta>
            <ta e="T337" id="Seg_7965" s="T336">np.h:Th</ta>
            <ta e="T340" id="Seg_7966" s="T339">np:Th</ta>
            <ta e="T343" id="Seg_7967" s="T342">np:Path</ta>
            <ta e="T345" id="Seg_7968" s="T344">np.h:Poss</ta>
            <ta e="T346" id="Seg_7969" s="T345">np:P</ta>
            <ta e="T348" id="Seg_7970" s="T347">pro:P</ta>
            <ta e="T352" id="Seg_7971" s="T351">0.3.h:A</ta>
            <ta e="T354" id="Seg_7972" s="T353">0.3.h:A</ta>
            <ta e="T355" id="Seg_7973" s="T354">pro:A</ta>
            <ta e="T357" id="Seg_7974" s="T355">pp:G</ta>
            <ta e="T358" id="Seg_7975" s="T357">0.3:E</ta>
            <ta e="T360" id="Seg_7976" s="T359">np.h:Th</ta>
            <ta e="T363" id="Seg_7977" s="T361">pp:Time</ta>
            <ta e="T365" id="Seg_7978" s="T364">np:A</ta>
            <ta e="T366" id="Seg_7979" s="T365">pro:G</ta>
            <ta e="T369" id="Seg_7980" s="T368">np:Th</ta>
            <ta e="T373" id="Seg_7981" s="T372">np:Th</ta>
            <ta e="T378" id="Seg_7982" s="T377">np.h:Th</ta>
            <ta e="T380" id="Seg_7983" s="T379">0.3.h:Th</ta>
            <ta e="T385" id="Seg_7984" s="T384">np.h:Poss</ta>
            <ta e="T386" id="Seg_7985" s="T385">np:Th</ta>
            <ta e="T389" id="Seg_7986" s="T388">pro.h:A</ta>
            <ta e="T391" id="Seg_7987" s="T390">pro.h:Th</ta>
            <ta e="T392" id="Seg_7988" s="T391">np:L</ta>
            <ta e="T394" id="Seg_7989" s="T393">np:Th</ta>
            <ta e="T397" id="Seg_7990" s="T396">n:Time</ta>
            <ta e="T398" id="Seg_7991" s="T397">0.3.h:Poss np:G</ta>
            <ta e="T399" id="Seg_7992" s="T398">0.3.h:A</ta>
            <ta e="T400" id="Seg_7993" s="T399">0.3.h:A</ta>
            <ta e="T401" id="Seg_7994" s="T400">np:Th</ta>
            <ta e="T402" id="Seg_7995" s="T401">0.3.h:A</ta>
            <ta e="T404" id="Seg_7996" s="T403">0.3.h:A</ta>
            <ta e="T409" id="Seg_7997" s="T408">np:Th</ta>
            <ta e="T410" id="Seg_7998" s="T409">0.3.h:A</ta>
            <ta e="T414" id="Seg_7999" s="T413">0.3.h:Poss np:G</ta>
            <ta e="T415" id="Seg_8000" s="T414">0.3.h:A</ta>
            <ta e="T417" id="Seg_8001" s="T416">0.3.h:A</ta>
            <ta e="T419" id="Seg_8002" s="T418">0.3.h:Poss np.h:Poss</ta>
            <ta e="T420" id="Seg_8003" s="T419">np:G</ta>
            <ta e="T421" id="Seg_8004" s="T420">0.3.h:Th</ta>
            <ta e="T423" id="Seg_8005" s="T422">pro:Th</ta>
            <ta e="T430" id="Seg_8006" s="T429">0.3:Th</ta>
            <ta e="T434" id="Seg_8007" s="T433">np:G</ta>
            <ta e="T435" id="Seg_8008" s="T434">0.3.h:A</ta>
            <ta e="T438" id="Seg_8009" s="T436">pp:L</ta>
            <ta e="T442" id="Seg_8010" s="T441">0.3.h:Th</ta>
            <ta e="T443" id="Seg_8011" s="T442">0.3.h:Th</ta>
            <ta e="T444" id="Seg_8012" s="T443">0.3.h:Th</ta>
            <ta e="T459" id="Seg_8013" s="T457">pp:Cau</ta>
            <ta e="T460" id="Seg_8014" s="T459">0.3.h:P</ta>
            <ta e="T461" id="Seg_8015" s="T460">0.3.h:A</ta>
            <ta e="T464" id="Seg_8016" s="T463">pro.h:R</ta>
            <ta e="T466" id="Seg_8017" s="T465">np:Th</ta>
            <ta e="T467" id="Seg_8018" s="T466">0.3.h:A</ta>
            <ta e="T470" id="Seg_8019" s="T469">0.3.h:A</ta>
            <ta e="T472" id="Seg_8020" s="T471">0.3.h:A</ta>
            <ta e="T475" id="Seg_8021" s="T474">np:A</ta>
            <ta e="T478" id="Seg_8022" s="T477">0.3.h:A</ta>
            <ta e="T480" id="Seg_8023" s="T479">0.3.h:A</ta>
            <ta e="T482" id="Seg_8024" s="T481">np:A</ta>
            <ta e="T483" id="Seg_8025" s="T482">0.3:P</ta>
            <ta e="T487" id="Seg_8026" s="T486">0.3.h:A</ta>
            <ta e="T488" id="Seg_8027" s="T487">pro:P</ta>
            <ta e="T490" id="Seg_8028" s="T489">0.3.h:A</ta>
            <ta e="T492" id="Seg_8029" s="T491">0.3:Th</ta>
            <ta e="T497" id="Seg_8030" s="T496">0.3.h:A</ta>
            <ta e="T500" id="Seg_8031" s="T499">np:Th</ta>
            <ta e="T501" id="Seg_8032" s="T500">0.2.h:A</ta>
            <ta e="T503" id="Seg_8033" s="T502">np.h:A</ta>
            <ta e="T504" id="Seg_8034" s="T503">np:Th</ta>
            <ta e="T506" id="Seg_8035" s="T505">0.3.h:A</ta>
            <ta e="T507" id="Seg_8036" s="T506">np.h:A</ta>
            <ta e="T508" id="Seg_8037" s="T507">0.3.h:A</ta>
            <ta e="T510" id="Seg_8038" s="T509">np:G</ta>
            <ta e="T513" id="Seg_8039" s="T512">np:G</ta>
            <ta e="T514" id="Seg_8040" s="T513">0.3.h:A</ta>
            <ta e="T515" id="Seg_8041" s="T514">0.3.h:A</ta>
            <ta e="T518" id="Seg_8042" s="T517">np:Th</ta>
            <ta e="T519" id="Seg_8043" s="T518">0.3.h:A</ta>
            <ta e="T522" id="Seg_8044" s="T521">np:G</ta>
            <ta e="T524" id="Seg_8045" s="T523">0.3.h:A</ta>
            <ta e="T528" id="Seg_8046" s="T527">np:Th</ta>
            <ta e="T529" id="Seg_8047" s="T528">0.3.h:A</ta>
            <ta e="T531" id="Seg_8048" s="T530">np:Th</ta>
            <ta e="T532" id="Seg_8049" s="T531">np:Th</ta>
            <ta e="T533" id="Seg_8050" s="T532">0.3.h:A</ta>
            <ta e="T535" id="Seg_8051" s="T534">np:Th</ta>
            <ta e="T536" id="Seg_8052" s="T535">np:G</ta>
            <ta e="T537" id="Seg_8053" s="T536">0.3.h:A</ta>
            <ta e="T541" id="Seg_8054" s="T540">np:Th</ta>
            <ta e="T542" id="Seg_8055" s="T541">np:Th</ta>
            <ta e="T543" id="Seg_8056" s="T542">0.3.h:A</ta>
            <ta e="T545" id="Seg_8057" s="T544">np:Ins</ta>
            <ta e="T546" id="Seg_8058" s="T545">0.3.h:A</ta>
            <ta e="T547" id="Seg_8059" s="T546">0.3.h:E</ta>
            <ta e="T550" id="Seg_8060" s="T549">0.1.h:Poss np:Th</ta>
            <ta e="T553" id="Seg_8061" s="T552">np:Th</ta>
            <ta e="T555" id="Seg_8062" s="T554">np:Th</ta>
            <ta e="T557" id="Seg_8063" s="T556">0.2.h:A</ta>
            <ta e="T558" id="Seg_8064" s="T557">0.3.h:A</ta>
            <ta e="T559" id="Seg_8065" s="T558">np.h:A</ta>
            <ta e="T560" id="Seg_8066" s="T559">np:Th</ta>
            <ta e="T565" id="Seg_8067" s="T564">np:P</ta>
            <ta e="T566" id="Seg_8068" s="T565">pro:Ins</ta>
            <ta e="T568" id="Seg_8069" s="T567">0.3.h:A</ta>
            <ta e="T571" id="Seg_8070" s="T570">0.3.h:P</ta>
            <ta e="T575" id="Seg_8071" s="T574">0.1.h:P</ta>
            <ta e="T576" id="Seg_8072" s="T575">0.3.h:A</ta>
            <ta e="T578" id="Seg_8073" s="T577">0.3.h:A</ta>
            <ta e="T581" id="Seg_8074" s="T580">np:P</ta>
            <ta e="T583" id="Seg_8075" s="T582">0.3.h:A</ta>
            <ta e="T591" id="Seg_8076" s="T590">0.3.h:A</ta>
            <ta e="T592" id="Seg_8077" s="T591">np.h:Th</ta>
            <ta e="T601" id="Seg_8078" s="T598">adv:Time</ta>
            <ta e="T599" id="Seg_8079" s="T601">adv:Time</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_8080" s="T1">np.h:S</ta>
            <ta e="T3" id="Seg_8081" s="T2">v:pred</ta>
            <ta e="T7" id="Seg_8082" s="T6">0.3.h:S ptcl:pred</ta>
            <ta e="T9" id="Seg_8083" s="T7">s:temp</ta>
            <ta e="T10" id="Seg_8084" s="T9">np:S</ta>
            <ta e="T12" id="Seg_8085" s="T11">v:pred</ta>
            <ta e="T14" id="Seg_8086" s="T13">0.3.h:S adj:pred</ta>
            <ta e="T16" id="Seg_8087" s="T14">s:temp</ta>
            <ta e="T17" id="Seg_8088" s="T16">np:S</ta>
            <ta e="T19" id="Seg_8089" s="T18">v:pred</ta>
            <ta e="T22" id="Seg_8090" s="T21">0.3.h:S ptcl:pred</ta>
            <ta e="T27" id="Seg_8091" s="T26">0.3.h:S adj:pred</ta>
            <ta e="T33" id="Seg_8092" s="T32">0.3.h:S adj:pred</ta>
            <ta e="T37" id="Seg_8093" s="T33">s:temp</ta>
            <ta e="T38" id="Seg_8094" s="T37">pro:O</ta>
            <ta e="T39" id="Seg_8095" s="T38">0.3.h:S v:pred</ta>
            <ta e="T41" id="Seg_8096" s="T39">s:adv</ta>
            <ta e="T44" id="Seg_8097" s="T43">0.3.h:S v:pred</ta>
            <ta e="T46" id="Seg_8098" s="T45">0.3.h:S v:pred</ta>
            <ta e="T48" id="Seg_8099" s="T47">np.h:S</ta>
            <ta e="T50" id="Seg_8100" s="T49">v:pred</ta>
            <ta e="T53" id="Seg_8101" s="T52">s:adv</ta>
            <ta e="T54" id="Seg_8102" s="T53">0.3.h:S v:pred</ta>
            <ta e="T57" id="Seg_8103" s="T56">0.1.h:S v:pred</ta>
            <ta e="T58" id="Seg_8104" s="T57">0.1.h:S v:pred</ta>
            <ta e="T60" id="Seg_8105" s="T59">0.1.h:S adj:pred</ta>
            <ta e="T61" id="Seg_8106" s="T60">np:S</ta>
            <ta e="T62" id="Seg_8107" s="T61">ptcl:pred</ta>
            <ta e="T65" id="Seg_8108" s="T63">s:temp</ta>
            <ta e="T67" id="Seg_8109" s="T66">0.3:S v:pred</ta>
            <ta e="T71" id="Seg_8110" s="T70">n:pred</ta>
            <ta e="T72" id="Seg_8111" s="T71">0.3.h:S v:pred</ta>
            <ta e="T73" id="Seg_8112" s="T72">s:cond</ta>
            <ta e="T75" id="Seg_8113" s="T74">0.1.h:S v:pred</ta>
            <ta e="T76" id="Seg_8114" s="T75">np.h:S</ta>
            <ta e="T77" id="Seg_8115" s="T76">v:pred</ta>
            <ta e="T79" id="Seg_8116" s="T78">0.3.h:S v:pred</ta>
            <ta e="T80" id="Seg_8117" s="T79">np.h:S</ta>
            <ta e="T81" id="Seg_8118" s="T80">v:pred</ta>
            <ta e="T83" id="Seg_8119" s="T82">adj:pred</ta>
            <ta e="T85" id="Seg_8120" s="T84">adj:pred</ta>
            <ta e="T87" id="Seg_8121" s="T86">np.h:S</ta>
            <ta e="T88" id="Seg_8122" s="T87">v:pred</ta>
            <ta e="T89" id="Seg_8123" s="T88">s:adv</ta>
            <ta e="T90" id="Seg_8124" s="T89">0.3.h:S v:pred</ta>
            <ta e="T92" id="Seg_8125" s="T91">np:O</ta>
            <ta e="T97" id="Seg_8126" s="T96">0.2.h:S v:pred</ta>
            <ta e="T99" id="Seg_8127" s="T98">np.h:S</ta>
            <ta e="T100" id="Seg_8128" s="T99">np:O</ta>
            <ta e="T101" id="Seg_8129" s="T100">v:pred</ta>
            <ta e="T105" id="Seg_8130" s="T101">s:temp</ta>
            <ta e="T106" id="Seg_8131" s="T105">np.h:S</ta>
            <ta e="T107" id="Seg_8132" s="T106">v:pred</ta>
            <ta e="T108" id="Seg_8133" s="T107">s:purp</ta>
            <ta e="T113" id="Seg_8134" s="T108">s:adv</ta>
            <ta e="T114" id="Seg_8135" s="T113">0.2.h:S v:pred</ta>
            <ta e="T116" id="Seg_8136" s="T114">s:temp</ta>
            <ta e="T118" id="Seg_8137" s="T117">0.2.h:S v:pred</ta>
            <ta e="T121" id="Seg_8138" s="T120">np.h:S</ta>
            <ta e="T122" id="Seg_8139" s="T121">s:adv</ta>
            <ta e="T126" id="Seg_8140" s="T125">np:O</ta>
            <ta e="T127" id="Seg_8141" s="T126">v:pred</ta>
            <ta e="T131" id="Seg_8142" s="T128">s:comp</ta>
            <ta e="T132" id="Seg_8143" s="T131">0.3.h:S v:pred</ta>
            <ta e="T133" id="Seg_8144" s="T132">np:S</ta>
            <ta e="T134" id="Seg_8145" s="T133">v:pred</ta>
            <ta e="T137" id="Seg_8146" s="T136">np:O</ta>
            <ta e="T138" id="Seg_8147" s="T137">0.2.h:S v:pred</ta>
            <ta e="T140" id="Seg_8148" s="T139">0.1.h:S v:pred</ta>
            <ta e="T141" id="Seg_8149" s="T140">0.3.h:S v:pred</ta>
            <ta e="T146" id="Seg_8150" s="T145">0.2.h:S v:pred</ta>
            <ta e="T147" id="Seg_8151" s="T146">0.3.h:S v:pred</ta>
            <ta e="T152" id="Seg_8152" s="T147">s:temp</ta>
            <ta e="T156" id="Seg_8153" s="T155">v:pred</ta>
            <ta e="T157" id="Seg_8154" s="T156">np.h:S</ta>
            <ta e="T162" id="Seg_8155" s="T157">s:adv</ta>
            <ta e="T164" id="Seg_8156" s="T163">np:O</ta>
            <ta e="T165" id="Seg_8157" s="T164">v:pred</ta>
            <ta e="T167" id="Seg_8158" s="T166">np.h:O</ta>
            <ta e="T168" id="Seg_8159" s="T167">0.3.h:S v:pred</ta>
            <ta e="T172" id="Seg_8160" s="T168">s:temp</ta>
            <ta e="T174" id="Seg_8161" s="T172">s:temp</ta>
            <ta e="T176" id="Seg_8162" s="T175">0.3.h:S v:pred</ta>
            <ta e="T179" id="Seg_8163" s="T177">s:rel</ta>
            <ta e="T181" id="Seg_8164" s="T180">np:O</ta>
            <ta e="T184" id="Seg_8165" s="T183">0.3.h:S v:pred</ta>
            <ta e="T185" id="Seg_8166" s="T184">pro.h:S</ta>
            <ta e="T189" id="Seg_8167" s="T185">s:temp</ta>
            <ta e="T193" id="Seg_8168" s="T192">s:adv</ta>
            <ta e="T195" id="Seg_8169" s="T194">v:pred</ta>
            <ta e="T201" id="Seg_8170" s="T197">s:comp</ta>
            <ta e="T202" id="Seg_8171" s="T201">0.3.h:S v:pred</ta>
            <ta e="T203" id="Seg_8172" s="T202">s:comp</ta>
            <ta e="T204" id="Seg_8173" s="T203">0.3.h:S v:pred</ta>
            <ta e="T206" id="Seg_8174" s="T205">np:S</ta>
            <ta e="T208" id="Seg_8175" s="T207">v:pred</ta>
            <ta e="T210" id="Seg_8176" s="T208">s:temp</ta>
            <ta e="T211" id="Seg_8177" s="T210">np:S</ta>
            <ta e="T213" id="Seg_8178" s="T212">v:pred</ta>
            <ta e="T223" id="Seg_8179" s="T222">v:pred</ta>
            <ta e="T225" id="Seg_8180" s="T224">np.h:S</ta>
            <ta e="T227" id="Seg_8181" s="T225">s:adv</ta>
            <ta e="T231" id="Seg_8182" s="T229">s:rel</ta>
            <ta e="T236" id="Seg_8183" s="T234">s:rel</ta>
            <ta e="T240" id="Seg_8184" s="T239">0.3.h:S v:pred</ta>
            <ta e="T247" id="Seg_8185" s="T240">s:temp</ta>
            <ta e="T249" id="Seg_8186" s="T248">np:S</ta>
            <ta e="T251" id="Seg_8187" s="T250">v:pred</ta>
            <ta e="T256" id="Seg_8188" s="T255">np:S</ta>
            <ta e="T258" id="Seg_8189" s="T257">v:pred</ta>
            <ta e="T261" id="Seg_8190" s="T260">np:S</ta>
            <ta e="T262" id="Seg_8191" s="T261">v:pred</ta>
            <ta e="T271" id="Seg_8192" s="T270">np:S</ta>
            <ta e="T272" id="Seg_8193" s="T271">np:S</ta>
            <ta e="T274" id="Seg_8194" s="T273">v:pred</ta>
            <ta e="T278" id="Seg_8195" s="T277">np.h:S</ta>
            <ta e="T280" id="Seg_8196" s="T279">np:O</ta>
            <ta e="T282" id="Seg_8197" s="T281">v:pred</ta>
            <ta e="T284" id="Seg_8198" s="T283">0.3.h:S v:pred</ta>
            <ta e="T285" id="Seg_8199" s="T284">0.3:S adj:pred</ta>
            <ta e="T286" id="Seg_8200" s="T285">np.h:S</ta>
            <ta e="T287" id="Seg_8201" s="T286">ptcl:pred</ta>
            <ta e="T289" id="Seg_8202" s="T288">np:S</ta>
            <ta e="T291" id="Seg_8203" s="T290">v:pred</ta>
            <ta e="T293" id="Seg_8204" s="T292">np:S</ta>
            <ta e="T295" id="Seg_8205" s="T294">v:pred</ta>
            <ta e="T301" id="Seg_8206" s="T300">np:S</ta>
            <ta e="T302" id="Seg_8207" s="T301">v:pred</ta>
            <ta e="T304" id="Seg_8208" s="T303">np.h:S</ta>
            <ta e="T305" id="Seg_8209" s="T304">s:adv</ta>
            <ta e="T306" id="Seg_8210" s="T305">np:O</ta>
            <ta e="T307" id="Seg_8211" s="T306">v:pred</ta>
            <ta e="T308" id="Seg_8212" s="T307">np:O</ta>
            <ta e="T309" id="Seg_8213" s="T308">0.3.h:S v:pred</ta>
            <ta e="T312" id="Seg_8214" s="T311">np:S</ta>
            <ta e="T313" id="Seg_8215" s="T312">quant:pred</ta>
            <ta e="T316" id="Seg_8216" s="T315">np:S</ta>
            <ta e="T317" id="Seg_8217" s="T316">quant:pred</ta>
            <ta e="T319" id="Seg_8218" s="T318">np.h:S</ta>
            <ta e="T321" id="Seg_8219" s="T319">s:temp</ta>
            <ta e="T323" id="Seg_8220" s="T322">np:O</ta>
            <ta e="T325" id="Seg_8221" s="T324">v:pred</ta>
            <ta e="T330" id="Seg_8222" s="T329">np.h:S</ta>
            <ta e="T331" id="Seg_8223" s="T330">v:pred</ta>
            <ta e="T337" id="Seg_8224" s="T336">np.h:S</ta>
            <ta e="T338" id="Seg_8225" s="T337">v:pred</ta>
            <ta e="T340" id="Seg_8226" s="T339">np:S</ta>
            <ta e="T341" id="Seg_8227" s="T340">cop</ta>
            <ta e="T346" id="Seg_8228" s="T345">np:S</ta>
            <ta e="T347" id="Seg_8229" s="T346">v:pred</ta>
            <ta e="T348" id="Seg_8230" s="T347">pro:O</ta>
            <ta e="T352" id="Seg_8231" s="T348">s:adv</ta>
            <ta e="T354" id="Seg_8232" s="T353">0.3.h:S v:pred</ta>
            <ta e="T355" id="Seg_8233" s="T354">pro:S</ta>
            <ta e="T358" id="Seg_8234" s="T357">s:adv</ta>
            <ta e="T359" id="Seg_8235" s="T358">v:pred</ta>
            <ta e="T360" id="Seg_8236" s="T359">np.h:S</ta>
            <ta e="T364" id="Seg_8237" s="T363">v:pred</ta>
            <ta e="T365" id="Seg_8238" s="T364">np:S</ta>
            <ta e="T368" id="Seg_8239" s="T367">v:pred</ta>
            <ta e="T369" id="Seg_8240" s="T368">np:S</ta>
            <ta e="T372" id="Seg_8241" s="T371">v:pred</ta>
            <ta e="T373" id="Seg_8242" s="T372">np:S</ta>
            <ta e="T377" id="Seg_8243" s="T376">v:pred</ta>
            <ta e="T378" id="Seg_8244" s="T377">np.h:S</ta>
            <ta e="T380" id="Seg_8245" s="T379">s:adv</ta>
            <ta e="T382" id="Seg_8246" s="T381">v:pred</ta>
            <ta e="T386" id="Seg_8247" s="T385">np:S</ta>
            <ta e="T387" id="Seg_8248" s="T386">v:pred</ta>
            <ta e="T394" id="Seg_8249" s="T393">np:S</ta>
            <ta e="T395" id="Seg_8250" s="T394">ptcl:pred</ta>
            <ta e="T399" id="Seg_8251" s="T397">s:purp</ta>
            <ta e="T400" id="Seg_8252" s="T399">0.3.h:S v:pred</ta>
            <ta e="T401" id="Seg_8253" s="T400">np:O</ta>
            <ta e="T403" id="Seg_8254" s="T401">s:temp</ta>
            <ta e="T404" id="Seg_8255" s="T403">0.3.h:S v:pred</ta>
            <ta e="T407" id="Seg_8256" s="T404">s:temp</ta>
            <ta e="T409" id="Seg_8257" s="T408">np:O</ta>
            <ta e="T410" id="Seg_8258" s="T409">0.3.h:S v:pred</ta>
            <ta e="T413" id="Seg_8259" s="T410">s:temp</ta>
            <ta e="T415" id="Seg_8260" s="T414">0.3.h:S v:pred</ta>
            <ta e="T418" id="Seg_8261" s="T415">s:temp</ta>
            <ta e="T421" id="Seg_8262" s="T420">0.3.h:S v:pred</ta>
            <ta e="T423" id="Seg_8263" s="T422">pro:S</ta>
            <ta e="T425" id="Seg_8264" s="T424">ptcl:pred</ta>
            <ta e="T428" id="Seg_8265" s="T427">s:adv</ta>
            <ta e="T430" id="Seg_8266" s="T429">0.3:S v:pred</ta>
            <ta e="T435" id="Seg_8267" s="T434">0.3.h:S v:pred</ta>
            <ta e="T442" id="Seg_8268" s="T438">s:adv</ta>
            <ta e="T443" id="Seg_8269" s="T442">s:adv</ta>
            <ta e="T444" id="Seg_8270" s="T443">0.3.h:S v:pred</ta>
            <ta e="T460" id="Seg_8271" s="T459">s:purp</ta>
            <ta e="T461" id="Seg_8272" s="T460">0.3.h:S v:pred</ta>
            <ta e="T464" id="Seg_8273" s="T463">pro.h:O</ta>
            <ta e="T467" id="Seg_8274" s="T466">0.3.h:S v:pred</ta>
            <ta e="T469" id="Seg_8275" s="T468">np:O</ta>
            <ta e="T470" id="Seg_8276" s="T469">0.3.h:S v:pred</ta>
            <ta e="T472" id="Seg_8277" s="T471">0.3.h:S v:pred</ta>
            <ta e="T475" id="Seg_8278" s="T474">np:S</ta>
            <ta e="T477" id="Seg_8279" s="T476">v:pred</ta>
            <ta e="T478" id="Seg_8280" s="T477">0.3.h:S v:pred</ta>
            <ta e="T480" id="Seg_8281" s="T479">0.3.h:S v:pred</ta>
            <ta e="T482" id="Seg_8282" s="T481">np:S</ta>
            <ta e="T483" id="Seg_8283" s="T482">s:adv</ta>
            <ta e="T485" id="Seg_8284" s="T484">v:pred</ta>
            <ta e="T487" id="Seg_8285" s="T486">0.3.h:S v:pred</ta>
            <ta e="T488" id="Seg_8286" s="T487">pro:O</ta>
            <ta e="T490" id="Seg_8287" s="T489">0.3.h:S v:pred</ta>
            <ta e="T491" id="Seg_8288" s="T490">n:pred</ta>
            <ta e="T492" id="Seg_8289" s="T491">0.3:S cop</ta>
            <ta e="T497" id="Seg_8290" s="T496">0.3.h:S v:pred</ta>
            <ta e="T500" id="Seg_8291" s="T499">np:O</ta>
            <ta e="T501" id="Seg_8292" s="T500">0.2.h:S v:pred</ta>
            <ta e="T502" id="Seg_8293" s="T501">v:pred</ta>
            <ta e="T503" id="Seg_8294" s="T502">np.h:S</ta>
            <ta e="T504" id="Seg_8295" s="T503">np:O</ta>
            <ta e="T506" id="Seg_8296" s="T505">0.3.h:S v:pred</ta>
            <ta e="T507" id="Seg_8297" s="T506">np.h:S</ta>
            <ta e="T509" id="Seg_8298" s="T507">s:temp</ta>
            <ta e="T511" id="Seg_8299" s="T510">v:pred</ta>
            <ta e="T514" id="Seg_8300" s="T511">s:adv</ta>
            <ta e="T516" id="Seg_8301" s="T514">s:temp</ta>
            <ta e="T520" id="Seg_8302" s="T516">s:temp</ta>
            <ta e="T524" id="Seg_8303" s="T523">0.3.h:S v:pred</ta>
            <ta e="T527" id="Seg_8304" s="T524">s:temp</ta>
            <ta e="T528" id="Seg_8305" s="T527">np:O</ta>
            <ta e="T529" id="Seg_8306" s="T528">0.3.h:S v:pred</ta>
            <ta e="T531" id="Seg_8307" s="T530">np:O</ta>
            <ta e="T532" id="Seg_8308" s="T531">np:O</ta>
            <ta e="T533" id="Seg_8309" s="T532">0.3.h:S v:pred</ta>
            <ta e="T535" id="Seg_8310" s="T534">np:O</ta>
            <ta e="T537" id="Seg_8311" s="T536">0.3.h:S v:pred</ta>
            <ta e="T539" id="Seg_8312" s="T537">s:adv</ta>
            <ta e="T541" id="Seg_8313" s="T540">np:O</ta>
            <ta e="T542" id="Seg_8314" s="T541">np:O</ta>
            <ta e="T543" id="Seg_8315" s="T542">0.3.h:S v:pred</ta>
            <ta e="T546" id="Seg_8316" s="T544">s:comp</ta>
            <ta e="T547" id="Seg_8317" s="T546">0.3.h:S v:pred</ta>
            <ta e="T550" id="Seg_8318" s="T549">np:S</ta>
            <ta e="T551" id="Seg_8319" s="T550">adj:pred</ta>
            <ta e="T553" id="Seg_8320" s="T552">np:O</ta>
            <ta e="T555" id="Seg_8321" s="T554">np:O</ta>
            <ta e="T557" id="Seg_8322" s="T556">0.2.h:S v:pred</ta>
            <ta e="T558" id="Seg_8323" s="T557">0.3.h:S v:pred</ta>
            <ta e="T559" id="Seg_8324" s="T558">np.h:S</ta>
            <ta e="T560" id="Seg_8325" s="T559">np:O</ta>
            <ta e="T563" id="Seg_8326" s="T562">v:pred</ta>
            <ta e="T565" id="Seg_8327" s="T564">np:O</ta>
            <ta e="T568" id="Seg_8328" s="T567">0.3.h:S v:pred</ta>
            <ta e="T571" id="Seg_8329" s="T570">0.3.h:S v:pred</ta>
            <ta e="T576" id="Seg_8330" s="T574">s:adv</ta>
            <ta e="T578" id="Seg_8331" s="T577">0.3.h:S v:pred</ta>
            <ta e="T581" id="Seg_8332" s="T580">np:O</ta>
            <ta e="T583" id="Seg_8333" s="T582">0.3.h:S v:pred</ta>
            <ta e="T591" id="Seg_8334" s="T590">0.3.h:S v:pred</ta>
            <ta e="T592" id="Seg_8335" s="T591">np.h:S</ta>
            <ta e="T600" id="Seg_8336" s="T599">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T1" id="Seg_8337" s="T0">new</ta>
            <ta e="T2" id="Seg_8338" s="T1">new</ta>
            <ta e="T7" id="Seg_8339" s="T6">0.accs-aggr</ta>
            <ta e="T8" id="Seg_8340" s="T7">accs-inf</ta>
            <ta e="T9" id="Seg_8341" s="T8">0.giv-active</ta>
            <ta e="T10" id="Seg_8342" s="T9">accs-inf</ta>
            <ta e="T14" id="Seg_8343" s="T13">0.giv-active</ta>
            <ta e="T16" id="Seg_8344" s="T15">0.giv-active</ta>
            <ta e="T17" id="Seg_8345" s="T16">accs-inf</ta>
            <ta e="T22" id="Seg_8346" s="T21">0.giv-active</ta>
            <ta e="T24" id="Seg_8347" s="T23">new</ta>
            <ta e="T27" id="Seg_8348" s="T26">0.giv-active new</ta>
            <ta e="T30" id="Seg_8349" s="T29">new</ta>
            <ta e="T33" id="Seg_8350" s="T32">0.giv-active new</ta>
            <ta e="T34" id="Seg_8351" s="T33">giv-active</ta>
            <ta e="T36" id="Seg_8352" s="T35">new</ta>
            <ta e="T38" id="Seg_8353" s="T37">giv-active</ta>
            <ta e="T39" id="Seg_8354" s="T38">0.giv-active</ta>
            <ta e="T40" id="Seg_8355" s="T39">new</ta>
            <ta e="T41" id="Seg_8356" s="T40">0.giv-active</ta>
            <ta e="T44" id="Seg_8357" s="T43">0.giv-active</ta>
            <ta e="T46" id="Seg_8358" s="T45">0.giv-active</ta>
            <ta e="T48" id="Seg_8359" s="T47">giv-active</ta>
            <ta e="T51" id="Seg_8360" s="T50">giv-active</ta>
            <ta e="T53" id="Seg_8361" s="T52">0.giv-active</ta>
            <ta e="T54" id="Seg_8362" s="T53">0.giv-active 0.quot-sp</ta>
            <ta e="T56" id="Seg_8363" s="T55">giv-active-Q</ta>
            <ta e="T57" id="Seg_8364" s="T56">0.giv-active-Q</ta>
            <ta e="T58" id="Seg_8365" s="T57">0.giv-active-Q</ta>
            <ta e="T60" id="Seg_8366" s="T59">giv-inactive-Q</ta>
            <ta e="T61" id="Seg_8367" s="T60">0.giv-active-Q</ta>
            <ta e="T65" id="Seg_8368" s="T64">0.giv-active-Q</ta>
            <ta e="T67" id="Seg_8369" s="T66">0.giv-active-Q</ta>
            <ta e="T69" id="Seg_8370" s="T68">giv-active</ta>
            <ta e="T70" id="Seg_8371" s="T69">giv-active-Q</ta>
            <ta e="T72" id="Seg_8372" s="T71">0.giv-active 0.quot-sp</ta>
            <ta e="T73" id="Seg_8373" s="T72">0.giv-active-Q</ta>
            <ta e="T75" id="Seg_8374" s="T74">0.giv-active-Q</ta>
            <ta e="T76" id="Seg_8375" s="T75">giv-active</ta>
            <ta e="T79" id="Seg_8376" s="T78">0.giv-active</ta>
            <ta e="T80" id="Seg_8377" s="T79">giv-active</ta>
            <ta e="T87" id="Seg_8378" s="T86">giv-active</ta>
            <ta e="T89" id="Seg_8379" s="T88">0.giv-active</ta>
            <ta e="T90" id="Seg_8380" s="T89">0.giv-active 0.quot-sp</ta>
            <ta e="T91" id="Seg_8381" s="T90">giv-inactive-Q</ta>
            <ta e="T92" id="Seg_8382" s="T91">accs-inf-Q</ta>
            <ta e="T94" id="Seg_8383" s="T93">giv-inactive-Q</ta>
            <ta e="T97" id="Seg_8384" s="T96">0.giv-inactive-Q</ta>
            <ta e="T99" id="Seg_8385" s="T98">giv-active</ta>
            <ta e="T100" id="Seg_8386" s="T99">giv-active</ta>
            <ta e="T102" id="Seg_8387" s="T101">giv-active</ta>
            <ta e="T103" id="Seg_8388" s="T102">accs-inf</ta>
            <ta e="T106" id="Seg_8389" s="T105">giv-inactive</ta>
            <ta e="T107" id="Seg_8390" s="T106">quot-sp</ta>
            <ta e="T108" id="Seg_8391" s="T107">0.giv-active-Q</ta>
            <ta e="T110" id="Seg_8392" s="T109">new-Q</ta>
            <ta e="T111" id="Seg_8393" s="T110">new-Q</ta>
            <ta e="T113" id="Seg_8394" s="T112">0.giv-active-Q</ta>
            <ta e="T114" id="Seg_8395" s="T113">0.giv-active-Q</ta>
            <ta e="T115" id="Seg_8396" s="T114">0.giv-active-Q</ta>
            <ta e="T118" id="Seg_8397" s="T117">0.giv-active-Q</ta>
            <ta e="T121" id="Seg_8398" s="T120">giv-active</ta>
            <ta e="T122" id="Seg_8399" s="T121">0.giv-active</ta>
            <ta e="T124" id="Seg_8400" s="T123">giv-active</ta>
            <ta e="T130" id="Seg_8401" s="T129">accs-inf</ta>
            <ta e="T131" id="Seg_8402" s="T130">0.giv-active</ta>
            <ta e="T132" id="Seg_8403" s="T131">0.giv-active</ta>
            <ta e="T133" id="Seg_8404" s="T132">accs-inf</ta>
            <ta e="T136" id="Seg_8405" s="T135">giv-inactive-Q</ta>
            <ta e="T137" id="Seg_8406" s="T136">giv-active-Q</ta>
            <ta e="T138" id="Seg_8407" s="T137">0.giv-active-Q</ta>
            <ta e="T140" id="Seg_8408" s="T139">0.giv-active-Q</ta>
            <ta e="T141" id="Seg_8409" s="T140">0.giv-active 0.quot-sp</ta>
            <ta e="T144" id="Seg_8410" s="T143">accs-sit</ta>
            <ta e="T146" id="Seg_8411" s="T145">0.giv-active-Q</ta>
            <ta e="T147" id="Seg_8412" s="T146">0.giv-active 0.quot-sp</ta>
            <ta e="T148" id="Seg_8413" s="T147">giv-active</ta>
            <ta e="T150" id="Seg_8414" s="T149">giv-inactive</ta>
            <ta e="T151" id="Seg_8415" s="T150">0.giv-active</ta>
            <ta e="T157" id="Seg_8416" s="T156">giv-active</ta>
            <ta e="T158" id="Seg_8417" s="T157">accs-sit</ta>
            <ta e="T159" id="Seg_8418" s="T158">accs-inf</ta>
            <ta e="T160" id="Seg_8419" s="T159">0.giv-active</ta>
            <ta e="T162" id="Seg_8420" s="T161">accs-inf</ta>
            <ta e="T164" id="Seg_8421" s="T163">giv-inactive</ta>
            <ta e="T167" id="Seg_8422" s="T166">giv-inactive</ta>
            <ta e="T168" id="Seg_8423" s="T167">0.giv-active</ta>
            <ta e="T169" id="Seg_8424" s="T168">giv-inactive</ta>
            <ta e="T170" id="Seg_8425" s="T169">accs-inf</ta>
            <ta e="T171" id="Seg_8426" s="T170">0.giv-active</ta>
            <ta e="T173" id="Seg_8427" s="T172">0.giv-active</ta>
            <ta e="T176" id="Seg_8428" s="T175">0.giv-active</ta>
            <ta e="T180" id="Seg_8429" s="T179">giv-active</ta>
            <ta e="T181" id="Seg_8430" s="T180">giv-inactive</ta>
            <ta e="T182" id="Seg_8431" s="T181">new</ta>
            <ta e="T184" id="Seg_8432" s="T183">0.giv-active</ta>
            <ta e="T185" id="Seg_8433" s="T184">giv-active</ta>
            <ta e="T186" id="Seg_8434" s="T185">0.giv-active</ta>
            <ta e="T187" id="Seg_8435" s="T186">accs-sit</ta>
            <ta e="T188" id="Seg_8436" s="T187">0.giv-active</ta>
            <ta e="T191" id="Seg_8437" s="T190">accs-inf</ta>
            <ta e="T193" id="Seg_8438" s="T192">0.giv-active</ta>
            <ta e="T199" id="Seg_8439" s="T198">new</ta>
            <ta e="T200" id="Seg_8440" s="T199">accs-inf</ta>
            <ta e="T201" id="Seg_8441" s="T200">0.giv-active</ta>
            <ta e="T202" id="Seg_8442" s="T201">0.giv-active</ta>
            <ta e="T203" id="Seg_8443" s="T202">0.giv-active</ta>
            <ta e="T204" id="Seg_8444" s="T203">0.giv-active</ta>
            <ta e="T206" id="Seg_8445" s="T205">accs-sit</ta>
            <ta e="T207" id="Seg_8446" s="T206">accs-inf</ta>
            <ta e="T210" id="Seg_8447" s="T209">0.giv-active</ta>
            <ta e="T211" id="Seg_8448" s="T210">new</ta>
            <ta e="T225" id="Seg_8449" s="T224">giv-active</ta>
            <ta e="T227" id="Seg_8450" s="T226">0.giv-active</ta>
            <ta e="T230" id="Seg_8451" s="T229">accs-gen</ta>
            <ta e="T234" id="Seg_8452" s="T233">new</ta>
            <ta e="T235" id="Seg_8453" s="T234">accs-gen</ta>
            <ta e="T239" id="Seg_8454" s="T238">new</ta>
            <ta e="T240" id="Seg_8455" s="T239">0.giv-active</ta>
            <ta e="T242" id="Seg_8456" s="T241">giv-active</ta>
            <ta e="T246" id="Seg_8457" s="T245">giv-active</ta>
            <ta e="T249" id="Seg_8458" s="T248">accs-inf</ta>
            <ta e="T256" id="Seg_8459" s="T255">new</ta>
            <ta e="T261" id="Seg_8460" s="T260">new</ta>
            <ta e="T264" id="Seg_8461" s="T263">giv-active</ta>
            <ta e="T271" id="Seg_8462" s="T270">new</ta>
            <ta e="T272" id="Seg_8463" s="T271">new</ta>
            <ta e="T278" id="Seg_8464" s="T277">giv-inactive</ta>
            <ta e="T280" id="Seg_8465" s="T279">giv-active</ta>
            <ta e="T284" id="Seg_8466" s="T283">0.giv-active</ta>
            <ta e="T285" id="Seg_8467" s="T284">0.giv-active</ta>
            <ta e="T289" id="Seg_8468" s="T288">new</ta>
            <ta e="T293" id="Seg_8469" s="T292">new</ta>
            <ta e="T297" id="Seg_8470" s="T296">new</ta>
            <ta e="T301" id="Seg_8471" s="T300">giv-active</ta>
            <ta e="T304" id="Seg_8472" s="T303">giv-inactive</ta>
            <ta e="T305" id="Seg_8473" s="T304">0.giv-active</ta>
            <ta e="T306" id="Seg_8474" s="T305">new</ta>
            <ta e="T308" id="Seg_8475" s="T307">new</ta>
            <ta e="T309" id="Seg_8476" s="T308">0.giv-active</ta>
            <ta e="T312" id="Seg_8477" s="T311">new</ta>
            <ta e="T316" id="Seg_8478" s="T315">new</ta>
            <ta e="T319" id="Seg_8479" s="T318">giv-active</ta>
            <ta e="T320" id="Seg_8480" s="T319">0.giv-active</ta>
            <ta e="T323" id="Seg_8481" s="T322">new</ta>
            <ta e="T343" id="Seg_8482" s="T342">accs-inf</ta>
            <ta e="T346" id="Seg_8483" s="T345">new</ta>
            <ta e="T348" id="Seg_8484" s="T347">giv-active</ta>
            <ta e="T352" id="Seg_8485" s="T351">0.giv-inactive</ta>
            <ta e="T354" id="Seg_8486" s="T353">0.giv-active</ta>
            <ta e="T355" id="Seg_8487" s="T354">giv-active</ta>
            <ta e="T356" id="Seg_8488" s="T355">accs-sit</ta>
            <ta e="T358" id="Seg_8489" s="T357">0.giv-active</ta>
            <ta e="T360" id="Seg_8490" s="T359">giv-active</ta>
            <ta e="T365" id="Seg_8491" s="T364">giv-inactive</ta>
            <ta e="T369" id="Seg_8492" s="T368">new</ta>
            <ta e="T373" id="Seg_8493" s="T372">new</ta>
            <ta e="T378" id="Seg_8494" s="T377">giv-active</ta>
            <ta e="T380" id="Seg_8495" s="T379">0.giv-active</ta>
            <ta e="T385" id="Seg_8496" s="T384">giv-active</ta>
            <ta e="T386" id="Seg_8497" s="T385">accs-inf</ta>
            <ta e="T389" id="Seg_8498" s="T388">giv-active-Q</ta>
            <ta e="T391" id="Seg_8499" s="T390">giv-inactive-Q</ta>
            <ta e="T398" id="Seg_8500" s="T397">giv-active</ta>
            <ta e="T399" id="Seg_8501" s="T398">0.giv-active</ta>
            <ta e="T400" id="Seg_8502" s="T399">0.giv-active</ta>
            <ta e="T401" id="Seg_8503" s="T400">new</ta>
            <ta e="T402" id="Seg_8504" s="T401">0.giv-active</ta>
            <ta e="T404" id="Seg_8505" s="T403">0.giv-active</ta>
            <ta e="T409" id="Seg_8506" s="T408">new</ta>
            <ta e="T410" id="Seg_8507" s="T409">0.giv-active</ta>
            <ta e="T414" id="Seg_8508" s="T413">giv-inactive</ta>
            <ta e="T415" id="Seg_8509" s="T414">0.giv-active</ta>
            <ta e="T417" id="Seg_8510" s="T416">0.giv-active</ta>
            <ta e="T419" id="Seg_8511" s="T418">giv-active</ta>
            <ta e="T420" id="Seg_8512" s="T419">accs-inf</ta>
            <ta e="T421" id="Seg_8513" s="T420">0.giv-active</ta>
            <ta e="T434" id="Seg_8514" s="T433">accs-inf</ta>
            <ta e="T435" id="Seg_8515" s="T434">0.giv-inactive</ta>
            <ta e="T437" id="Seg_8516" s="T436">accs-inf</ta>
            <ta e="T442" id="Seg_8517" s="T441">0.giv-inactive</ta>
            <ta e="T443" id="Seg_8518" s="T442">0.giv-active</ta>
            <ta e="T444" id="Seg_8519" s="T443">0.giv-active</ta>
            <ta e="T460" id="Seg_8520" s="T459">0.giv-inactive</ta>
            <ta e="T461" id="Seg_8521" s="T460">0.giv-active</ta>
            <ta e="T464" id="Seg_8522" s="T463">giv-active</ta>
            <ta e="T466" id="Seg_8523" s="T465">giv-inactive</ta>
            <ta e="T467" id="Seg_8524" s="T466">0.giv-inactive</ta>
            <ta e="T470" id="Seg_8525" s="T469">0.giv-active</ta>
            <ta e="T472" id="Seg_8526" s="T471">0.giv-active 0.quot-sp</ta>
            <ta e="T474" id="Seg_8527" s="T473">giv-inactive-Q</ta>
            <ta e="T475" id="Seg_8528" s="T474">accs-inf-Q</ta>
            <ta e="T478" id="Seg_8529" s="T477">0.giv-active 0.quot-sp</ta>
            <ta e="T479" id="Seg_8530" s="T478">giv-inactive</ta>
            <ta e="T480" id="Seg_8531" s="T479">0.giv-inactive</ta>
            <ta e="T482" id="Seg_8532" s="T481">new-Q</ta>
            <ta e="T483" id="Seg_8533" s="T482">0.giv-active-Q</ta>
            <ta e="T487" id="Seg_8534" s="T486">0.giv-inactive</ta>
            <ta e="T488" id="Seg_8535" s="T487">giv-inactive</ta>
            <ta e="T490" id="Seg_8536" s="T489">0.giv-inactive</ta>
            <ta e="T492" id="Seg_8537" s="T491">0.giv-active</ta>
            <ta e="T494" id="Seg_8538" s="T493">accs-gen</ta>
            <ta e="T495" id="Seg_8539" s="T494">giv-active</ta>
            <ta e="T497" id="Seg_8540" s="T496">0.giv-active 0.quot-sp</ta>
            <ta e="T499" id="Seg_8541" s="T498">giv-active-Q</ta>
            <ta e="T500" id="Seg_8542" s="T499">accs-inf-Q</ta>
            <ta e="T501" id="Seg_8543" s="T500">0.giv-active-Q</ta>
            <ta e="T502" id="Seg_8544" s="T501">quot-sp</ta>
            <ta e="T503" id="Seg_8545" s="T502">giv-inactive</ta>
            <ta e="T504" id="Seg_8546" s="T503">giv-active</ta>
            <ta e="T506" id="Seg_8547" s="T505">0.giv-active</ta>
            <ta e="T507" id="Seg_8548" s="T506">giv-inactive</ta>
            <ta e="T508" id="Seg_8549" s="T507">0.giv-active</ta>
            <ta e="T514" id="Seg_8550" s="T513">0.giv-active</ta>
            <ta e="T515" id="Seg_8551" s="T514">0.giv-active</ta>
            <ta e="T518" id="Seg_8552" s="T517">new</ta>
            <ta e="T519" id="Seg_8553" s="T518">0.giv-active</ta>
            <ta e="T522" id="Seg_8554" s="T521">accs-inf</ta>
            <ta e="T524" id="Seg_8555" s="T523">0.giv-active</ta>
            <ta e="T528" id="Seg_8556" s="T527">new</ta>
            <ta e="T529" id="Seg_8557" s="T528">0.giv-active</ta>
            <ta e="T531" id="Seg_8558" s="T530">new</ta>
            <ta e="T532" id="Seg_8559" s="T531">new</ta>
            <ta e="T533" id="Seg_8560" s="T532">0.giv-active</ta>
            <ta e="T535" id="Seg_8561" s="T534">new</ta>
            <ta e="T536" id="Seg_8562" s="T535">new</ta>
            <ta e="T537" id="Seg_8563" s="T536">0.giv-active</ta>
            <ta e="T541" id="Seg_8564" s="T540">new</ta>
            <ta e="T542" id="Seg_8565" s="T541">new</ta>
            <ta e="T543" id="Seg_8566" s="T542">0.giv-active</ta>
            <ta e="T545" id="Seg_8567" s="T544">giv-active</ta>
            <ta e="T546" id="Seg_8568" s="T545">0.giv-inactive</ta>
            <ta e="T547" id="Seg_8569" s="T546">0.giv-active</ta>
            <ta e="T550" id="Seg_8570" s="T549">giv-active-Q</ta>
            <ta e="T553" id="Seg_8571" s="T552">new-Q</ta>
            <ta e="T555" id="Seg_8572" s="T554">new-Q</ta>
            <ta e="T557" id="Seg_8573" s="T556">0.giv-active-Q</ta>
            <ta e="T558" id="Seg_8574" s="T557">0.giv-active 0.quot-sp</ta>
            <ta e="T559" id="Seg_8575" s="T558">giv-active</ta>
            <ta e="T560" id="Seg_8576" s="T559">giv-active</ta>
            <ta e="T565" id="Seg_8577" s="T564">giv-inactive</ta>
            <ta e="T566" id="Seg_8578" s="T565">giv-active</ta>
            <ta e="T568" id="Seg_8579" s="T567">0.giv-inactive</ta>
            <ta e="T571" id="Seg_8580" s="T570">0.giv-active</ta>
            <ta e="T575" id="Seg_8581" s="T574">0.giv-active-Q</ta>
            <ta e="T576" id="Seg_8582" s="T575">0.giv-active 0.quot-sp</ta>
            <ta e="T578" id="Seg_8583" s="T577">0.giv-active</ta>
            <ta e="T581" id="Seg_8584" s="T580">giv-inactive</ta>
            <ta e="T583" id="Seg_8585" s="T582">0.giv-active</ta>
            <ta e="T588" id="Seg_8586" s="T587">accs-sit</ta>
            <ta e="T591" id="Seg_8587" s="T590">0.giv-active</ta>
            <ta e="T592" id="Seg_8588" s="T591">giv-inactive</ta>
         </annotation>
         <annotation name="Top" tierref="Top">
            <ta e="T3" id="Seg_8589" s="T2">0.top.int.abstr</ta>
            <ta e="T7" id="Seg_8590" s="T6">0.top.int.concr</ta>
            <ta e="T9" id="Seg_8591" s="T7">top.int.concr</ta>
            <ta e="T14" id="Seg_8592" s="T13">0.top.int.concr</ta>
            <ta e="T16" id="Seg_8593" s="T14">top.int.concr</ta>
            <ta e="T22" id="Seg_8594" s="T21">0.top.int.concr</ta>
            <ta e="T25" id="Seg_8595" s="T22">top.ext</ta>
            <ta e="T27" id="Seg_8596" s="T26">0.top.int.concr</ta>
            <ta e="T30" id="Seg_8597" s="T27">top.ext</ta>
            <ta e="T33" id="Seg_8598" s="T32">0.top.int.concr</ta>
            <ta e="T37" id="Seg_8599" s="T33">top.int.concr</ta>
            <ta e="T51" id="Seg_8600" s="T50">0.top.int.abstr.</ta>
            <ta e="T54" id="Seg_8601" s="T53">0.top.int.concr</ta>
            <ta e="T58" id="Seg_8602" s="T57">0.top.int.concr</ta>
            <ta e="T60" id="Seg_8603" s="T59">0.top.int.concr</ta>
            <ta e="T65" id="Seg_8604" s="T63">top.int.concr</ta>
            <ta e="T73" id="Seg_8605" s="T72">top.int.concr</ta>
            <ta e="T76" id="Seg_8606" s="T75">top.int.concr</ta>
            <ta e="T79" id="Seg_8607" s="T78">0.top.int.concr</ta>
            <ta e="T80" id="Seg_8608" s="T79">top.int.concr</ta>
            <ta e="T87" id="Seg_8609" s="T86">top.int.concr</ta>
            <ta e="T90" id="Seg_8610" s="T89">0.top.int.concr</ta>
            <ta e="T99" id="Seg_8611" s="T98">top.int.concr</ta>
            <ta e="T105" id="Seg_8612" s="T101">top.int.concr</ta>
            <ta e="T108" id="Seg_8613" s="T107">top.int.concr</ta>
            <ta e="T121" id="Seg_8614" s="T120">top.int.concr</ta>
            <ta e="T132" id="Seg_8615" s="T127">top.int.concr</ta>
            <ta e="T140" id="Seg_8616" s="T139">0.top.int.concr</ta>
            <ta e="T146" id="Seg_8617" s="T145">0.top.int.concr</ta>
            <ta e="T152" id="Seg_8618" s="T147">top.int.concr</ta>
            <ta e="T157" id="Seg_8619" s="T156">top.int.concr</ta>
            <ta e="T168" id="Seg_8620" s="T167">0.top.int.concr</ta>
            <ta e="T176" id="Seg_8621" s="T175">0.top.int.concr</ta>
            <ta e="T184" id="Seg_8622" s="T183">0.top.int.concr</ta>
            <ta e="T185" id="Seg_8623" s="T184">top.int.concr</ta>
            <ta e="T200" id="Seg_8624" s="T196">top.int.concr</ta>
            <ta e="T204" id="Seg_8625" s="T203">0.top.int.concr</ta>
            <ta e="T206" id="Seg_8626" s="T204">top.int.concr</ta>
            <ta e="T210" id="Seg_8627" s="T208">top.int.concr</ta>
            <ta e="T225" id="Seg_8628" s="T223">top.int.concr</ta>
            <ta e="T240" id="Seg_8629" s="T239">0.top.int.concr</ta>
            <ta e="T247" id="Seg_8630" s="T240">top.int.concr</ta>
            <ta e="T258" id="Seg_8631" s="T257">0.top.int.abstr.</ta>
            <ta e="T264" id="Seg_8632" s="T262">top.int.concr</ta>
            <ta e="T265" id="Seg_8633" s="T264">top.int.concr</ta>
            <ta e="T278" id="Seg_8634" s="T276">top.int.concr</ta>
            <ta e="T284" id="Seg_8635" s="T283">0.top.int.concr</ta>
            <ta e="T285" id="Seg_8636" s="T284">0.top.int.concr</ta>
            <ta e="T287" id="Seg_8637" s="T286">0.top.int.abstr</ta>
            <ta e="T289" id="Seg_8638" s="T287">top.int.concr</ta>
            <ta e="T293" id="Seg_8639" s="T291">top.int.concr</ta>
            <ta e="T301" id="Seg_8640" s="T300">top.int.concr</ta>
            <ta e="T304" id="Seg_8641" s="T302">top.int.concr</ta>
            <ta e="T309" id="Seg_8642" s="T308">0.top.int.concr</ta>
            <ta e="T312" id="Seg_8643" s="T309">top.int.concr</ta>
            <ta e="T316" id="Seg_8644" s="T313">top.int.concr</ta>
            <ta e="T319" id="Seg_8645" s="T317">top.int.concr</ta>
            <ta e="T327" id="Seg_8646" s="T325">top.int.concr</ta>
            <ta e="T340" id="Seg_8647" s="T338">top.int.concr</ta>
            <ta e="T348" id="Seg_8648" s="T347">top.int.concr</ta>
            <ta e="T355" id="Seg_8649" s="T354">top.int.concr</ta>
            <ta e="T360" id="Seg_8650" s="T359">top.int.concr</ta>
            <ta e="T365" id="Seg_8651" s="T364">top.int.concr</ta>
            <ta e="T369" id="Seg_8652" s="T368">top.int.concr</ta>
            <ta e="T373" id="Seg_8653" s="T372">top.int.concr</ta>
            <ta e="T378" id="Seg_8654" s="T377">top.int.concr</ta>
            <ta e="T384" id="Seg_8655" s="T382">top.int.concr</ta>
            <ta e="T397" id="Seg_8656" s="T395">top.int.concr</ta>
            <ta e="T404" id="Seg_8657" s="T403">0.top.int.concr</ta>
            <ta e="T407" id="Seg_8658" s="T404">top.int.concr</ta>
            <ta e="T413" id="Seg_8659" s="T410">top.int.concr</ta>
            <ta e="T421" id="Seg_8660" s="T420">0.top.int.concr</ta>
            <ta e="T425" id="Seg_8661" s="T424">0.top.int.abstr</ta>
            <ta e="T430" id="Seg_8662" s="T429">0.top.int.concr</ta>
            <ta e="T435" id="Seg_8663" s="T434">0.top.int.concr</ta>
            <ta e="T438" id="Seg_8664" s="T435">top.int.concr</ta>
            <ta e="T461" id="Seg_8665" s="T460">0.top.int.concr</ta>
            <ta e="T467" id="Seg_8666" s="T466">0.top.int.concr</ta>
            <ta e="T470" id="Seg_8667" s="T469">0.top.int.concr</ta>
            <ta e="T472" id="Seg_8668" s="T471">0.top.int.concr</ta>
            <ta e="T475" id="Seg_8669" s="T472">top.int.concr</ta>
            <ta e="T480" id="Seg_8670" s="T479">0.top.int.concr</ta>
            <ta e="T482" id="Seg_8671" s="T481">top.int.concr</ta>
            <ta e="T487" id="Seg_8672" s="T486">0.top.int.concr</ta>
            <ta e="T490" id="Seg_8673" s="T489">0.top.int.concr</ta>
            <ta e="T492" id="Seg_8674" s="T491">0.top.int.concr</ta>
            <ta e="T506" id="Seg_8675" s="T505">0.top.int.concr</ta>
            <ta e="T507" id="Seg_8676" s="T506">top.int.concr</ta>
            <ta e="T520" id="Seg_8677" s="T511">top.int.concr</ta>
            <ta e="T527" id="Seg_8678" s="T524">top.int.concr</ta>
            <ta e="T533" id="Seg_8679" s="T532">0.top.int.concr</ta>
            <ta e="T537" id="Seg_8680" s="T536">0.top.int.concr</ta>
            <ta e="T543" id="Seg_8681" s="T542">0.top.int.concr</ta>
            <ta e="T547" id="Seg_8682" s="T546">0.top.int.concr</ta>
            <ta e="T550" id="Seg_8683" s="T549">top.int.concr</ta>
            <ta e="T559" id="Seg_8684" s="T558">top.int.concr</ta>
            <ta e="T568" id="Seg_8685" s="T567">0.top.int.concr</ta>
            <ta e="T569" id="Seg_8686" s="T568">top.int.concr</ta>
            <ta e="T575" id="Seg_8687" s="T574">0.top.int.concr</ta>
            <ta e="T578" id="Seg_8688" s="T577">0.top.int.concr</ta>
            <ta e="T583" id="Seg_8689" s="T582">0.top.int.concr</ta>
            <ta e="T591" id="Seg_8690" s="T590">0.top.int.concr</ta>
            <ta e="T592" id="Seg_8691" s="T591">top.int.concr</ta>
         </annotation>
         <annotation name="Foc" tierref="Foc">
            <ta e="T4" id="Seg_8692" s="T0">foc.wid</ta>
            <ta e="T7" id="Seg_8693" s="T4">foc.int</ta>
            <ta e="T11" id="Seg_8694" s="T9">foc.nar</ta>
            <ta e="T14" id="Seg_8695" s="T12">foc.nar</ta>
            <ta e="T18" id="Seg_8696" s="T16">foc.nar</ta>
            <ta e="T21" id="Seg_8697" s="T19">foc.nar</ta>
            <ta e="T27" id="Seg_8698" s="T25">foc.nar</ta>
            <ta e="T33" id="Seg_8699" s="T30">foc.nar</ta>
            <ta e="T41" id="Seg_8700" s="T37">foc.int</ta>
            <ta e="T51" id="Seg_8701" s="T46">foc.wid</ta>
            <ta e="T54" id="Seg_8702" s="T52">foc.int</ta>
            <ta e="T57" id="Seg_8703" s="T56">foc.int</ta>
            <ta e="T58" id="Seg_8704" s="T57">foc.int</ta>
            <ta e="T60" id="Seg_8705" s="T58">foc.nar</ta>
            <ta e="T67" id="Seg_8706" s="T65">foc.int</ta>
            <ta e="T71" id="Seg_8707" s="T69">foc.int</ta>
            <ta e="T75" id="Seg_8708" s="T74">foc.int</ta>
            <ta e="T77" id="Seg_8709" s="T76">foc.int</ta>
            <ta e="T79" id="Seg_8710" s="T78">foc.int</ta>
            <ta e="T81" id="Seg_8711" s="T80">foc.int</ta>
            <ta e="T84" id="Seg_8712" s="T82">foc.nar</ta>
            <ta e="T86" id="Seg_8713" s="T84">foc.nar</ta>
            <ta e="T88" id="Seg_8714" s="T87">foc.int</ta>
            <ta e="T90" id="Seg_8715" s="T88">foc.int</ta>
            <ta e="T95" id="Seg_8716" s="T93">foc.nar</ta>
            <ta e="T101" id="Seg_8717" s="T99">foc.int</ta>
            <ta e="T107" id="Seg_8718" s="T105">foc.wid</ta>
            <ta e="T118" id="Seg_8719" s="T108">foc.int</ta>
            <ta e="T127" id="Seg_8720" s="T121">foc.int</ta>
            <ta e="T134" id="Seg_8721" s="T132">foc.wid</ta>
            <ta e="T138" id="Seg_8722" s="T136">foc.int</ta>
            <ta e="T140" id="Seg_8723" s="T138">foc.int</ta>
            <ta e="T146" id="Seg_8724" s="T143">foc.int</ta>
            <ta e="T156" id="Seg_8725" s="T153">foc.int</ta>
            <ta e="T165" id="Seg_8726" s="T157">foc.int</ta>
            <ta e="T168" id="Seg_8727" s="T166">foc.int</ta>
            <ta e="T176" id="Seg_8728" s="T168">foc.int</ta>
            <ta e="T184" id="Seg_8729" s="T176">foc.int</ta>
            <ta e="T195" id="Seg_8730" s="T185">foc.int</ta>
            <ta e="T202" id="Seg_8731" s="T200">foc.int</ta>
            <ta e="T204" id="Seg_8732" s="T202">foc.int</ta>
            <ta e="T207" id="Seg_8733" s="T206">foc.nar</ta>
            <ta e="T213" id="Seg_8734" s="T210">foc.wid</ta>
            <ta e="T222" id="Seg_8735" s="T219">foc.nar</ta>
            <ta e="T234" id="Seg_8736" s="T233">foc.nar</ta>
            <ta e="T239" id="Seg_8737" s="T238">foc.nar</ta>
            <ta e="T251" id="Seg_8738" s="T247">foc.wid</ta>
            <ta e="T258" id="Seg_8739" s="T251">foc.wid</ta>
            <ta e="T262" id="Seg_8740" s="T258">foc.wid</ta>
            <ta e="T274" id="Seg_8741" s="T266">foc.wid</ta>
            <ta e="T282" id="Seg_8742" s="T280">foc.nar</ta>
            <ta e="T284" id="Seg_8743" s="T282">foc.int</ta>
            <ta e="T285" id="Seg_8744" s="T284">foc.nar</ta>
            <ta e="T287" id="Seg_8745" s="T285">foc.wid</ta>
            <ta e="T291" id="Seg_8746" s="T289">foc.int</ta>
            <ta e="T295" id="Seg_8747" s="T293">foc.int</ta>
            <ta e="T300" id="Seg_8748" s="T298">foc.nar</ta>
            <ta e="T306" id="Seg_8749" s="T305">foc.nar</ta>
            <ta e="T308" id="Seg_8750" s="T307">foc.nar</ta>
            <ta e="T313" id="Seg_8751" s="T312">foc.nar</ta>
            <ta e="T317" id="Seg_8752" s="T316">foc.nar</ta>
            <ta e="T325" id="Seg_8753" s="T319">foc.int</ta>
            <ta e="T330" id="Seg_8754" s="T327">foc.nar</ta>
            <ta e="T334" id="Seg_8755" s="T331">foc.nar</ta>
            <ta e="T347" id="Seg_8756" s="T342">foc.wid</ta>
            <ta e="T354" id="Seg_8757" s="T348">foc.int</ta>
            <ta e="T359" id="Seg_8758" s="T355">foc.int</ta>
            <ta e="T364" id="Seg_8759" s="T360">foc.int</ta>
            <ta e="T368" id="Seg_8760" s="T365">foc.int</ta>
            <ta e="T372" id="Seg_8761" s="T369">foc.int</ta>
            <ta e="T377" id="Seg_8762" s="T373">foc.int</ta>
            <ta e="T382" id="Seg_8763" s="T378">foc.int</ta>
            <ta e="T386" id="Seg_8764" s="T385">foc.nar</ta>
            <ta e="T395" id="Seg_8765" s="T388">foc.wid</ta>
            <ta e="T400" id="Seg_8766" s="T397">foc.int</ta>
            <ta e="T404" id="Seg_8767" s="T400">foc.int</ta>
            <ta e="T409" id="Seg_8768" s="T407">foc.nar</ta>
            <ta e="T415" id="Seg_8769" s="T413">foc.int</ta>
            <ta e="T421" id="Seg_8770" s="T415">foc.int</ta>
            <ta e="T425" id="Seg_8771" s="T421">foc.wid</ta>
            <ta e="T430" id="Seg_8772" s="T425">foc.int</ta>
            <ta e="T435" id="Seg_8773" s="T433">foc.int</ta>
            <ta e="T444" id="Seg_8774" s="T443">foc.nar</ta>
            <ta e="T460" id="Seg_8775" s="T459">foc.nar</ta>
            <ta e="T467" id="Seg_8776" s="T463">foc.int</ta>
            <ta e="T470" id="Seg_8777" s="T467">foc.int</ta>
            <ta e="T472" id="Seg_8778" s="T471">foc.int</ta>
            <ta e="T477" id="Seg_8779" s="T475">foc.int</ta>
            <ta e="T479" id="Seg_8780" s="T478">foc.nar</ta>
            <ta e="T485" id="Seg_8781" s="T482">foc.int</ta>
            <ta e="T487" id="Seg_8782" s="T485">foc.nar</ta>
            <ta e="T490" id="Seg_8783" s="T487">foc.int</ta>
            <ta e="T491" id="Seg_8784" s="T490">foc.nar</ta>
            <ta e="T501" id="Seg_8785" s="T499">foc.int</ta>
            <ta e="T506" id="Seg_8786" s="T503">foc.int</ta>
            <ta e="T511" id="Seg_8787" s="T507">foc.int</ta>
            <ta e="T524" id="Seg_8788" s="T520">foc.int</ta>
            <ta e="T528" id="Seg_8789" s="T527">foc.nar</ta>
            <ta e="T531" id="Seg_8790" s="T530">foc.nar</ta>
            <ta e="T532" id="Seg_8791" s="T531">foc.nar</ta>
            <ta e="T537" id="Seg_8792" s="T533">foc.int</ta>
            <ta e="T543" id="Seg_8793" s="T539">foc.int</ta>
            <ta e="T547" id="Seg_8794" s="T543">foc.int</ta>
            <ta e="T551" id="Seg_8795" s="T550">foc.nar</ta>
            <ta e="T553" id="Seg_8796" s="T551">foc.nar</ta>
            <ta e="T555" id="Seg_8797" s="T554">foc.nar</ta>
            <ta e="T561" id="Seg_8798" s="T559">foc.nar</ta>
            <ta e="T568" id="Seg_8799" s="T563">foc.int</ta>
            <ta e="T571" id="Seg_8800" s="T570">foc.nar</ta>
            <ta e="T575" id="Seg_8801" s="T574">foc.nar</ta>
            <ta e="T578" id="Seg_8802" s="T577">foc.int</ta>
            <ta e="T583" id="Seg_8803" s="T579">foc.int</ta>
            <ta e="T591" id="Seg_8804" s="T585">foc.int</ta>
            <ta e="T600" id="Seg_8805" s="T592">foc.int</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T94" id="Seg_8806" s="T93">EV:cult EV:gram (DIM)</ta>
            <ta e="T96" id="Seg_8807" s="T95">RUS:core</ta>
            <ta e="T98" id="Seg_8808" s="T97">RUS:core</ta>
            <ta e="T101" id="Seg_8809" s="T100">RUS:core</ta>
            <ta e="T125" id="Seg_8810" s="T124">EV:gram (DIM)</ta>
            <ta e="T130" id="Seg_8811" s="T129">EV:gram (DIM)</ta>
            <ta e="T142" id="Seg_8812" s="T141">RUS:cult</ta>
            <ta e="T161" id="Seg_8813" s="T160">EV:gram (DIM)</ta>
            <ta e="T175" id="Seg_8814" s="T174">EV:core</ta>
            <ta e="T191" id="Seg_8815" s="T190">EV:gram (DIM)</ta>
            <ta e="T198" id="Seg_8816" s="T197">EV:gram (DIM)</ta>
            <ta e="T233" id="Seg_8817" s="T232">RUS:core</ta>
            <ta e="T238" id="Seg_8818" s="T237">RUS:core</ta>
            <ta e="T271" id="Seg_8819" s="T270">EV:cult</ta>
            <ta e="T272" id="Seg_8820" s="T271">RUS:cult</ta>
            <ta e="T297" id="Seg_8821" s="T296">RUS:cult</ta>
            <ta e="T311" id="Seg_8822" s="T310">RUS:gram</ta>
            <ta e="T315" id="Seg_8823" s="T314">RUS:gram</ta>
            <ta e="T342" id="Seg_8824" s="T341">RUS:gram</ta>
            <ta e="T386" id="Seg_8825" s="T385">RUS:core</ta>
            <ta e="T401" id="Seg_8826" s="T400">RUS:cult</ta>
            <ta e="T420" id="Seg_8827" s="T419">EV:cult</ta>
            <ta e="T423" id="Seg_8828" s="T422">EV:gram (DIM)</ta>
            <ta e="T432" id="Seg_8829" s="T431">RUS:gram</ta>
            <ta e="T437" id="Seg_8830" s="T436">RUS:cult</ta>
            <ta e="T466" id="Seg_8831" s="T465">RUS:cult</ta>
            <ta e="T471" id="Seg_8832" s="T470">RUS:gram</ta>
            <ta e="T496" id="Seg_8833" s="T495">EV:gram (DIM)</ta>
            <ta e="T522" id="Seg_8834" s="T521">RUS:cult</ta>
            <ta e="T528" id="Seg_8835" s="T527">RUS:cult</ta>
            <ta e="T541" id="Seg_8836" s="T540">RUS:cult</ta>
            <ta e="T542" id="Seg_8837" s="T541">RUS:cult</ta>
            <ta e="T545" id="Seg_8838" s="T544">RUS:cult</ta>
            <ta e="T550" id="Seg_8839" s="T549">RUS:cult</ta>
            <ta e="T553" id="Seg_8840" s="T552">RUS:cult</ta>
            <ta e="T584" id="Seg_8841" s="T583">RUS:gram</ta>
            <ta e="T597" id="Seg_8842" s="T596">RUS:gram</ta>
            <ta e="T601" id="Seg_8843" s="T598">RUS:core </ta>
            <ta e="T599" id="Seg_8844" s="T601">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T98" id="Seg_8845" s="T97">Vsub</ta>
            <ta e="T142" id="Seg_8846" s="T141">Csub Vsub</ta>
            <ta e="T175" id="Seg_8847" s="T174">Csub</ta>
            <ta e="T271" id="Seg_8848" s="T270">Vsub Csub Vsub Csub Vsub</ta>
            <ta e="T272" id="Seg_8849" s="T271">Vsub Vsub</ta>
            <ta e="T297" id="Seg_8850" s="T296">medVins fortition Vsub</ta>
            <ta e="T311" id="Seg_8851" s="T310">Vsub</ta>
            <ta e="T315" id="Seg_8852" s="T314">Vsub</ta>
            <ta e="T342" id="Seg_8853" s="T341">Vsub</ta>
            <ta e="T386" id="Seg_8854" s="T385">Vsub</ta>
            <ta e="T401" id="Seg_8855" s="T400">fortition medVins Vsub</ta>
            <ta e="T437" id="Seg_8856" s="T436">Vsub</ta>
            <ta e="T466" id="Seg_8857" s="T465">fortition medVins Vsub</ta>
            <ta e="T471" id="Seg_8858" s="T470">Vsub</ta>
            <ta e="T522" id="Seg_8859" s="T521">medVins fortition Vsub</ta>
            <ta e="T528" id="Seg_8860" s="T527">Vsub</ta>
            <ta e="T541" id="Seg_8861" s="T540">Vsub Csub Vsub</ta>
            <ta e="T542" id="Seg_8862" s="T541">fortition Vsub Vsub</ta>
            <ta e="T545" id="Seg_8863" s="T544">Vsub Csub Vsub</ta>
            <ta e="T550" id="Seg_8864" s="T549">Vsub Csub Vsub</ta>
            <ta e="T553" id="Seg_8865" s="T552">fortition</ta>
            <ta e="T584" id="Seg_8866" s="T583">Vsub</ta>
            <ta e="T597" id="Seg_8867" s="T596">Vsub</ta>
            <ta e="T601" id="Seg_8868" s="T598">fortition Vsub</ta>
            <ta e="T599" id="Seg_8869" s="T601">fortition Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T94" id="Seg_8870" s="T93">dir:infl</ta>
            <ta e="T96" id="Seg_8871" s="T95">indir:infl</ta>
            <ta e="T98" id="Seg_8872" s="T97">dir:infl</ta>
            <ta e="T101" id="Seg_8873" s="T100">indir:infl</ta>
            <ta e="T142" id="Seg_8874" s="T141">dir:bare</ta>
            <ta e="T175" id="Seg_8875" s="T174">dir:infl</ta>
            <ta e="T233" id="Seg_8876" s="T232">dir:bare</ta>
            <ta e="T238" id="Seg_8877" s="T237">dir:bare</ta>
            <ta e="T271" id="Seg_8878" s="T270">dir:infl</ta>
            <ta e="T272" id="Seg_8879" s="T271">dir:infl</ta>
            <ta e="T297" id="Seg_8880" s="T296">dir:infl</ta>
            <ta e="T311" id="Seg_8881" s="T310">dir:bare</ta>
            <ta e="T315" id="Seg_8882" s="T314">dir:bare</ta>
            <ta e="T342" id="Seg_8883" s="T341">dir:bare</ta>
            <ta e="T386" id="Seg_8884" s="T385">dir:infl</ta>
            <ta e="T401" id="Seg_8885" s="T400">dir:infl</ta>
            <ta e="T420" id="Seg_8886" s="T419">dir:infl</ta>
            <ta e="T432" id="Seg_8887" s="T431">dir:bare</ta>
            <ta e="T437" id="Seg_8888" s="T436">dir:infl</ta>
            <ta e="T466" id="Seg_8889" s="T465">dir:infl</ta>
            <ta e="T471" id="Seg_8890" s="T470">dir:bare</ta>
            <ta e="T522" id="Seg_8891" s="T521">dir:infl</ta>
            <ta e="T528" id="Seg_8892" s="T527">dir:bare</ta>
            <ta e="T541" id="Seg_8893" s="T540">dir:bare</ta>
            <ta e="T542" id="Seg_8894" s="T541">dir:bare</ta>
            <ta e="T545" id="Seg_8895" s="T544">dir:infl</ta>
            <ta e="T550" id="Seg_8896" s="T549">dir:infl</ta>
            <ta e="T553" id="Seg_8897" s="T552">dir:infl</ta>
            <ta e="T584" id="Seg_8898" s="T583">dir:bare</ta>
            <ta e="T597" id="Seg_8899" s="T596">dir:bare</ta>
            <ta e="T601" id="Seg_8900" s="T598">dir:bare</ta>
            <ta e="T599" id="Seg_8901" s="T601">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_8902" s="T0">There lived an old man and an old woman, they say.</ta>
            <ta e="T7" id="Seg_8903" s="T4">They didn't have their own children.</ta>
            <ta e="T14" id="Seg_8904" s="T7">When they are sitting in their tent, only the fire is glowing, so poor they are.</ta>
            <ta e="T19" id="Seg_8905" s="T14">When they go outside, only the snow is white.</ta>
            <ta e="T22" id="Seg_8906" s="T19">They don't have anything.</ta>
            <ta e="T27" id="Seg_8907" s="T22">Their only property, they have a very lonely horse.</ta>
            <ta e="T33" id="Seg_8908" s="T27">Then their hunting equipment, they have a small net with three lead weights.</ta>
            <ta e="T41" id="Seg_8909" s="T33">When any small water creatures get into it, then they fry them on the skewer and eat them.</ta>
            <ta e="T46" id="Seg_8910" s="T41">The only thing left was to die.</ta>
            <ta e="T51" id="Seg_8911" s="T46">So the old man went to the old woman.</ta>
            <ta e="T54" id="Seg_8912" s="T51">He went inside and said: </ta>
            <ta e="T57" id="Seg_8913" s="T54">"Well, old woman, let's get up.</ta>
            <ta e="T67" id="Seg_8914" s="T57">Only our lonely horse can save us, we should eat it, if we die of hunger, it will live after us for nothing."</ta>
            <ta e="T69" id="Seg_8915" s="T67">The old woman on that: </ta>
            <ta e="T72" id="Seg_8916" s="T69">"It's your decision", she said.</ta>
            <ta e="T75" id="Seg_8917" s="T72">"If you kill it, we will eat it!"</ta>
            <ta e="T79" id="Seg_8918" s="T75">The the old man went out and killed it.</ta>
            <ta e="T90" id="Seg_8919" s="T79">The old man skinned it, it was very cold and frosty, the old man went inside and said: </ta>
            <ta e="T98" id="Seg_8920" s="T90">"Carry the meat of our horse completely into our tent."</ta>
            <ta e="T101" id="Seg_8921" s="T98">The old woman carried everything inside.</ta>
            <ta e="T107" id="Seg_8922" s="T101">As the old woman wanted to warm up her hands, the old man told her: </ta>
            <ta e="T118" id="Seg_8923" s="T107">"If you want to eat, fill all jars and buckets with water, after that you eat."</ta>
            <ta e="T127" id="Seg_8924" s="T118">Then the old woman went outside and scooped water in all jars.</ta>
            <ta e="T134" id="Seg_8925" s="T127">As she wanted to bring the last small kettle, the door was closed.</ta>
            <ta e="T141" id="Seg_8926" s="T134">"Hey, old man, open the door, I'm freezing", she said.</ta>
            <ta e="T147" id="Seg_8927" s="T141">"Son of a bitch, throw it in the snow", he said.</ta>
            <ta e="T156" id="Seg_8928" s="T147">Having thrown away the small kettle, the old woman is about to die. </ta>
            <ta e="T165" id="Seg_8929" s="T156">She climbs onto the yurt, there, where sparks go out, she warms up her hands.</ta>
            <ta e="T176" id="Seg_8930" s="T165">She saw her husband, that he takes the horse's perineum, places it and takes out the gut fat.</ta>
            <ta e="T184" id="Seg_8931" s="T176">The old woman's hands, which are warmed up, he chops with a peaked pole.</ta>
            <ta e="T195" id="Seg_8932" s="T184">Crying and crying she goes down and goes to the place where wood is chopped.</ta>
            <ta e="T208" id="Seg_8933" s="T195">There she wanted to lie down on the root of a small larch tree, she wanted to lie down, the shiver from the cold going into her heart.</ta>
            <ta e="T219" id="Seg_8934" s="T208">As she was lying there, a loud noise was heard: either it was a hunting on a reindeer or on something else.</ta>
            <ta e="T225" id="Seg_8935" s="T219">There the old woman goes in that direction.</ta>
            <ta e="T240" id="Seg_8936" s="T225">Going so, what a joy, she comes to reindeers, where a dense fog covers the moon, she came to cattle, where a light fog covers the sun.</ta>
            <ta e="T251" id="Seg_8937" s="T240">As the old woman, searching for a house, made her way through the reindeers, both sides [of her clothes] abraded.</ta>
            <ta e="T258" id="Seg_8938" s="T251">Suddenly there is a valley full of built-up pole tents.</ta>
            <ta e="T274" id="Seg_8939" s="T258">There appeared a very big tent, at this tent, a caravan with a couple of dozen baloks stands there.</ta>
            <ta e="T282" id="Seg_8940" s="T274">Then the old woman opened this tent.</ta>
            <ta e="T287" id="Seg_8941" s="T282">She looked, it was completely empty, there were no people.</ta>
            <ta e="T302" id="Seg_8942" s="T287">All kettles are standing ready there, the whole food is prepared, everywhere are stone beds, with such equipment is the tent.</ta>
            <ta e="T317" id="Seg_8943" s="T302">The old woman goes inside and eats what she had never eaten before, there is a lot of Russian food and a lot of Dolgan food.</ta>
            <ta e="T325" id="Seg_8944" s="T317">The old woman washed and combed herself, then she put on sable-lynx fur clothes.</ta>
            <ta e="T331" id="Seg_8945" s="T325">During this time nobody appeared.</ta>
            <ta e="T338" id="Seg_8946" s="T331">For three days nobody appeared.</ta>
            <ta e="T347" id="Seg_8947" s="T338">Only in the evening old human skulls are falling down through the chimney.</ta>
            <ta e="T359" id="Seg_8948" s="T347">Saying "Burst, break!", she shatters them and those, being afraid, disappeared into the earth.</ta>
            <ta e="T377" id="Seg_8949" s="T359">The old woman lived so for several decades, the reindeers don't run away, the horses are lying there, fish comes as if it was just caught.</ta>
            <ta e="T382" id="Seg_8950" s="T377">So the old woman lives rich and completely alone.</ta>
            <ta e="T387" id="Seg_8951" s="T382">Then the old woman thought so:</ta>
            <ta e="T395" id="Seg_8952" s="T387">"Oh, children, inspite of sitting here I better go and see my old man."</ta>
            <ta e="T400" id="Seg_8953" s="T395">In the third year she dressed herself to go to her old man. </ta>
            <ta e="T404" id="Seg_8954" s="T400">She cut bread and took it. </ta>
            <ta e="T410" id="Seg_8955" s="T404">After that she decided to take wild reindeer fat.</ta>
            <ta e="T415" id="Seg_8956" s="T410">After that she went to her old man.</ta>
            <ta e="T421" id="Seg_8957" s="T415">She went on and on and came to the pit-dwelling of her old man.</ta>
            <ta e="T430" id="Seg_8958" s="T421">But nothing was there, everything was drifted with snow.</ta>
            <ta e="T435" id="Seg_8959" s="T430">Still she climbed onto the chimney.</ta>
            <ta e="T453" id="Seg_8960" s="T435">Around the stove [the old man] is lying, having turned into a three-haired creeping worm, only the black hooves and remains are left from the horse. </ta>
            <ta e="T457" id="Seg_8961" s="T453">Recently he turned into leftover, it is said.</ta>
            <ta e="T462" id="Seg_8962" s="T457">He turned into it not to die, they say.</ta>
            <ta e="T467" id="Seg_8963" s="T462">Then she threw some bread to him.</ta>
            <ta e="T472" id="Seg_8964" s="T467">On that he crooked and said: </ta>
            <ta e="T478" id="Seg_8965" s="T472">"The clay of the stove doesn't let me lie."</ta>
            <ta e="T480" id="Seg_8966" s="T478">She threw some fat.</ta>
            <ta e="T485" id="Seg_8967" s="T480">"Oh, falling snow lumps don't let me lie."</ta>
            <ta e="T487" id="Seg_8968" s="T485">She threw again.</ta>
            <ta e="T492" id="Seg_8969" s="T487">He tried to bite, apparently it was the fat.</ta>
            <ta e="T497" id="Seg_8970" s="T492">On that: "Sky, sky, a little bit", he said.</ta>
            <ta e="T503" id="Seg_8971" s="T497">"Hey, old man, open the door", said the old woman.</ta>
            <ta e="T506" id="Seg_8972" s="T503">He opened the door.</ta>
            <ta e="T511" id="Seg_8973" s="T506">The old woman gave him to eat and brought him home.</ta>
            <ta e="T524" id="Seg_8974" s="T511">After bringing him home, she washed and combed him, she dressed him in clothes, in which he never dressed, and seated him on a glass bed.</ta>
            <ta e="T537" id="Seg_8975" s="T524">After that she gave him some tea to drink, she took out meat and poured some fat bouillon into a bowl.</ta>
            <ta e="T543" id="Seg_8976" s="T537">After that she put a golden spoon and a golden fork.</ta>
            <ta e="T547" id="Seg_8977" s="T543">And he was about to sup with the spoon: </ta>
            <ta e="T558" id="Seg_8978" s="T547">"What is that, my spoon is small, bring me a big ladle or a scoop", he said.</ta>
            <ta e="T563" id="Seg_8979" s="T558">Then she gave him a very big scoop.</ta>
            <ta e="T568" id="Seg_8980" s="T563">He slerped the fat bouillon out of it.</ta>
            <ta e="T572" id="Seg_8981" s="T568">Then he chokes.</ta>
            <ta e="T591" id="Seg_8982" s="T572">He says "I choke", and with a loud noise, having shattered the glass bed, he beats it through and falls through three icesheets down into the earth. </ta>
            <ta e="T600" id="Seg_8983" s="T591">The old woman lives like she had lived there.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_8984" s="T0">Es lebten ein alter Mann und eine alte Frau, sagt man.</ta>
            <ta e="T7" id="Seg_8985" s="T4">Sie hatten keine eigenen Kinder.</ta>
            <ta e="T14" id="Seg_8986" s="T7">Wenn sie im Zelt sitzen, scheint nur ihr Feuer rot, so arm sind sie.</ta>
            <ta e="T19" id="Seg_8987" s="T14">Wenn sie nach draußen gehen, ist nur der Schnee weiß.</ta>
            <ta e="T22" id="Seg_8988" s="T19">Sie haben nichts.</ta>
            <ta e="T27" id="Seg_8989" s="T22">Ihr einziger Besitz, sie haben ein sehr einsames Pferd.</ta>
            <ta e="T33" id="Seg_8990" s="T27">Dann ihre Jagdausrüstung, sie haben ein Netz mit drei Senksteinen.</ta>
            <ta e="T41" id="Seg_8991" s="T33">Wenn in diese irgendein kleines Wassergetier gerät, dann braten sie es auf dem Bratspieß und essen es.</ta>
            <ta e="T46" id="Seg_8992" s="T41">So war es ihnen beschieden zu sterben.</ta>
            <ta e="T51" id="Seg_8993" s="T46">Da ging der alte Mann zur alten Frau.</ta>
            <ta e="T54" id="Seg_8994" s="T51">Er ging herein und sagte: </ta>
            <ta e="T57" id="Seg_8995" s="T54">"Nun, alte Frau, stehen wir auf.</ta>
            <ta e="T67" id="Seg_8996" s="T57">Wir gehen, wir haben ein einsames Pferd, wir müssen es essen, wenn wir vor Hunger sterben, es wird uns sonst überleben."</ta>
            <ta e="T69" id="Seg_8997" s="T67">Darauf die alte Frau: </ta>
            <ta e="T72" id="Seg_8998" s="T69">"Es ist deine Entscheidung", sagte sie.</ta>
            <ta e="T75" id="Seg_8999" s="T72">"Wenn du es tötest, essen wir es!"</ta>
            <ta e="T79" id="Seg_9000" s="T75">Der Mann ging hinaus und tötete es.</ta>
            <ta e="T90" id="Seg_9001" s="T79">Der alte Mann nahm das Fell ab, es war sehr kalt, sehr frostig, der alte Mann ging hinein und sagte: </ta>
            <ta e="T98" id="Seg_9002" s="T90">"Bring das Fleisch unseres Pferdes ganz in unser Zelt hinein, mach es bis zum Rand voll."</ta>
            <ta e="T101" id="Seg_9003" s="T98">Die alte Frau schleppte alles herein.</ta>
            <ta e="T107" id="Seg_9004" s="T101">Als die alte Frau ihre Hände wärmen wollte, sagte der alte Mann: </ta>
            <ta e="T118" id="Seg_9005" s="T107">"Wenn du essen willst, dann füll alle Gefäße und Eimer mit Wasser, danach iss."</ta>
            <ta e="T127" id="Seg_9006" s="T118">Da ging die alte Frau hinaus und schöpfte in alle ihre Gefäße Wasser.</ta>
            <ta e="T134" id="Seg_9007" s="T127">Als sie das letzte Kesselchen hineinbringen wollte, war die Tür verschlossen.</ta>
            <ta e="T141" id="Seg_9008" s="T134">"Nun, Alter, mach die Tür auf, ich friere", sagte sie.</ta>
            <ta e="T147" id="Seg_9009" s="T141">"Du Hurentochter, wirf ihn in den Schnee", sagte er.</ta>
            <ta e="T156" id="Seg_9010" s="T147">Nachdem die alte Frau das Kesselchen weggeworfen hat, ist sie dabei zu erfrieren.</ta>
            <ta e="T165" id="Seg_9011" s="T156">Sie steigt auf die Jurte, dort, wo Fünkchen herauskommen, wärmt sie ihre Hände.</ta>
            <ta e="T176" id="Seg_9012" s="T165">Sie sah ihren Mann, dass er den Damm des Pferdes nimmt, ihn platziert und das Darmfett herausholt.</ta>
            <ta e="T184" id="Seg_9013" s="T176">Die Hände der Alten, die gewärmt wurden, hackte er mit einer spitzen Stange ab.</ta>
            <ta e="T195" id="Seg_9014" s="T184">Jene lässt sich weinend auf die Erde herab und schreitet weinend zu dem Platz, wo Holz gehackt wird.</ta>
            <ta e="T208" id="Seg_9015" s="T195">Dort wollte sie sich an den Fuß einer kleinen Lärche legen, sie wollte sich hinlegen, das Zittern von der Kälte drang bis ins Herz.</ta>
            <ta e="T219" id="Seg_9016" s="T208">Als sie lag, war ein starker Lärm zu hören, Rentiere oder etwas anderes werden gejagt.</ta>
            <ta e="T225" id="Seg_9017" s="T219">In diese Richtung geht da die Alte.</ta>
            <ta e="T240" id="Seg_9018" s="T225">So gehend, welch Freude, kommt sie zu Rentieren, wo dichter Nebel den Mond bedeckt, kommt sie zu Vieh, wo leichter Nebel die Sonne bedeckt.</ta>
            <ta e="T251" id="Seg_9019" s="T240">Als die alte Frau auf der Suche nach einem Haus durch die Rentiere hindurch ging, wurden beide Seiten [ihrer Kleidung] zerrieben.</ta>
            <ta e="T258" id="Seg_9020" s="T251">Plötzlich stand ein Tal voller aufgebauter Stangenzelte da.</ta>
            <ta e="T274" id="Seg_9021" s="T258">Dort kam ein sehr großes Zelt, bei diesem Zelt, steht eine Karawane mit einigen Dutzend Baloks da.</ta>
            <ta e="T282" id="Seg_9022" s="T274">Danach öffnete die Alte dieses Zelt.</ta>
            <ta e="T287" id="Seg_9023" s="T282">Sie schaute, es war ganz leer, keine Menschen.</ta>
            <ta e="T302" id="Seg_9024" s="T287">Alle Kessel stehen fertig da, alles Essen ist vorbereitet da, es sind überall steinerne Betten, mit so einer Ausrüstung ist das Zelt.</ta>
            <ta e="T317" id="Seg_9025" s="T302">Die alte Frau geht hinein und istt, was sie noch nie gegessen hat, russisches Essen gibt es viel, dolganisches Essen gibt es viel.</ta>
            <ta e="T325" id="Seg_9026" s="T317">Die Alte wusch sich und kämmte sich, dann zog sie Zobel-Luchs-Kleidung an.</ta>
            <ta e="T331" id="Seg_9027" s="T325">Währenddessen erschien niemand.</ta>
            <ta e="T338" id="Seg_9028" s="T331">Drei Tage lang war kein Mensch da.</ta>
            <ta e="T347" id="Seg_9029" s="T338">Erst als es Abend wurde, fallen durch das Rauchloch alte Menschenköpfe.</ta>
            <ta e="T359" id="Seg_9030" s="T347">Sie zerschlägt sie mit den Worten "Zerberstet, zerbrecht!" und diese hatten Angst und verschwanden in der Erde.</ta>
            <ta e="T377" id="Seg_9031" s="T359">Die alte Frau lebte so einige Jahrzehnte, die Rentiere laufen nicht weg, die Pferde liegen so da, die Fische fangen sich fast von selbst.</ta>
            <ta e="T382" id="Seg_9032" s="T377">So lebt die Alte Frau in Reichtum und Wohlstand und ganz allein.</ta>
            <ta e="T387" id="Seg_9033" s="T382">Dann kam der alten Frau ein Gedanke:</ta>
            <ta e="T395" id="Seg_9034" s="T387">"Ach, Kinder, besser als hier zu sitzen könnte ich meinen alten Mann besuchen gehen."</ta>
            <ta e="T400" id="Seg_9035" s="T395">Im dritten Jahr zog sie sich an, um zu ihrem alten Mann zu gehen.</ta>
            <ta e="T404" id="Seg_9036" s="T400">Sie schnitt Brot und nahm es.</ta>
            <ta e="T410" id="Seg_9037" s="T404">Danach entschied sie Rentierfett mitzunehmen.</ta>
            <ta e="T415" id="Seg_9038" s="T410">Danach ging sie zu ihrem alten Mann.</ta>
            <ta e="T421" id="Seg_9039" s="T415">Sie ging und ging und kam zu der Erdhütte ihres alten Mannes.</ta>
            <ta e="T430" id="Seg_9040" s="T421">Es war nichts da, alles war vom Schnee zugeweht.</ta>
            <ta e="T435" id="Seg_9041" s="T430">Dennoch kletterte sie zum Rauchloch.</ta>
            <ta e="T453" id="Seg_9042" s="T435">Um den Herd herum liegt [der alte Mann], in einen ungehobelten Wurm mit drei Haaren verwandelt, vom Pferd sind nur noch die Überreste mit den schwarzen Hufen geblieben.</ta>
            <ta e="T457" id="Seg_9043" s="T453">Vor kurzem erst hat der sich wohl verwandelt.</ta>
            <ta e="T462" id="Seg_9044" s="T457">Er hatte sich verwandelt, um nicht zu sterben.</ta>
            <ta e="T467" id="Seg_9045" s="T462">Da warf sie ihm Brot zu.</ta>
            <ta e="T472" id="Seg_9046" s="T467">Darauf krümmte sich dieser und sagte: </ta>
            <ta e="T478" id="Seg_9047" s="T472">"Der Lehm des Ofens lässt mich nicht liegen."</ta>
            <ta e="T480" id="Seg_9048" s="T478">Sie warf mit Fett.</ta>
            <ta e="T485" id="Seg_9049" s="T480">"Oh, die Schneeklumpen, die herunterfallen, lassen mich nicht liegen."</ta>
            <ta e="T487" id="Seg_9050" s="T485">Sie warf wieder.</ta>
            <ta e="T492" id="Seg_9051" s="T487">Er versuchte reinzubeißen, es war offenbar Fett.</ta>
            <ta e="T497" id="Seg_9052" s="T492">Darauf: "Himmel, Himmel, noch ein Bisschen", sagte er.</ta>
            <ta e="T503" id="Seg_9053" s="T497">"Hey, alter Mann, öffne die Tür", sagte die alte Frau.</ta>
            <ta e="T506" id="Seg_9054" s="T503">Er öffnete die Tür.</ta>
            <ta e="T511" id="Seg_9055" s="T506">Die alte Frau gab ihm zu essen und brachte ihn nach Hause.</ta>
            <ta e="T524" id="Seg_9056" s="T511">Nachdem sie ihn nach Hause gebracht hatte, wusch und kämmte sie ihn, zog ihm noch nie angezogene Kleidung an und setzte ihn auf ein gläsernes Bett.</ta>
            <ta e="T537" id="Seg_9057" s="T524">Danach gab sie ihm Tee zu trinken, nahm Fleisch heraus und tat fettige Brühe in eine Schüssel.</ta>
            <ta e="T543" id="Seg_9058" s="T537">Dann legte sie einen goldenen Löffel und Gabel hin.</ta>
            <ta e="T547" id="Seg_9059" s="T543">Und er wollte mit dem Löffel trinken: </ta>
            <ta e="T558" id="Seg_9060" s="T547">"Was ist das, mein Löffel ist klein, bring eine Suppenkelle oder eine Schöpfkelle", sagte er.</ta>
            <ta e="T563" id="Seg_9061" s="T558">Da gab diese ihm eine sehr große Schöpfkelle.</ta>
            <ta e="T568" id="Seg_9062" s="T563">Er schlürfte daraus die fettige Brühe.</ta>
            <ta e="T572" id="Seg_9063" s="T568">Daraufhin erstickt er.</ta>
            <ta e="T591" id="Seg_9064" s="T572">Er sagt "ich ersticke", macht ein lautes Geräusch, zerschlägt das gläserne Bett mit einem lauten Geräusch und verschwindet in den drei gefrorenen Schichten der Erde.</ta>
            <ta e="T600" id="Seg_9065" s="T591">Die alte Frau lebt immer so, wie sie gelebt hatte.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_9066" s="T0">Жили старик со старухой, говорят.</ta>
            <ta e="T7" id="Seg_9067" s="T4">Рожденных детей у них не было.</ta>
            <ta e="T14" id="Seg_9068" s="T7">В юрте сидят — только огонь их краснеет, такие бедные.</ta>
            <ta e="T19" id="Seg_9069" s="T14">Во двор выйдут — только снег их белеет.</ta>
            <ta e="T22" id="Seg_9070" s="T19">Ничего у них нет.</ta>
            <ta e="T27" id="Seg_9071" s="T22">Только и богатства — одна-единственная лошадь.</ta>
            <ta e="T33" id="Seg_9072" s="T27">А промыслового снаряжения — с тремя грузилами сеть.</ta>
            <ta e="T41" id="Seg_9073" s="T33">Когда в нее попадет какая-нибудь живность, ею питаются, на рожон насаживая.</ta>
            <ta e="T46" id="Seg_9074" s="T41">Вот приходится умирать-пропадать.</ta>
            <ta e="T51" id="Seg_9075" s="T46">Пошел вот старик к старухе.</ta>
            <ta e="T54" id="Seg_9076" s="T51">Входит и говорит: </ta>
            <ta e="T57" id="Seg_9077" s="T54">"Ну старуха, поднимемся.</ta>
            <ta e="T67" id="Seg_9078" s="T57">Пойдем, у нас есть одинокая наша лошадь, надо бы съесть ее, если умрем от голода — после нас [зря] останется."</ta>
            <ta e="T69" id="Seg_9079" s="T67">На это старуха: </ta>
            <ta e="T72" id="Seg_9080" s="T69">"Сам решай", сказала.</ta>
            <ta e="T75" id="Seg_9081" s="T72">"Если забьешь, съедим."</ta>
            <ta e="T79" id="Seg_9082" s="T75">Муж вышел и убил [лошадь].</ta>
            <ta e="T90" id="Seg_9083" s="T79">Старик снял шкуру, очень холодно было, метельно, старик входит и говорит: </ta>
            <ta e="T98" id="Seg_9084" s="T90">"Мясо лошади перетаскай все без остатка, до крошки в наше жилище-голомо."</ta>
            <ta e="T101" id="Seg_9085" s="T98">Старуха все перетаскала.</ta>
            <ta e="T107" id="Seg_9086" s="T101">Когда старуха захотела погреть руки, старик сказал: </ta>
            <ta e="T118" id="Seg_9087" s="T107">"Если хочешь есть, во всю посуду, в ведра натаскай воды, после этого поешь."</ta>
            <ta e="T127" id="Seg_9088" s="T118">Тогда старуха, выйдя, во всю посуду натаскала воды.</ta>
            <ta e="T134" id="Seg_9089" s="T127">Когда последний котелочек принесла, дверь заперта, оказывается.</ta>
            <ta e="T141" id="Seg_9090" s="T134">"Ну, старик, открой дверь, замерзаю", сказала.</ta>
            <ta e="T147" id="Seg_9091" s="T141">"Сукина дочь, брось [ты его] на снег", сказал.</ta>
            <ta e="T156" id="Seg_9092" s="T147">Старуха, бросив тот котелок, совсем замерзает.</ta>
            <ta e="T165" id="Seg_9093" s="T156">Взобравшись на балаган, старуха стала греть руки о теплый дым.</ta>
            <ta e="T176" id="Seg_9094" s="T165">Вот смотрит на мужа. ‎‎А тот, ухватив круп лошади, сидит, расставив ноги, и отделяет жир от кишок.</ta>
            <ta e="T184" id="Seg_9095" s="T176">И рожном начинает колоть руки старухи, протянутые к теплу.</ta>
            <ta e="T195" id="Seg_9096" s="T184">Та с плачем спускается на землю, и, плачарыдая, идет к тому месту, где рубят дрова.</ta>
            <ta e="T208" id="Seg_9097" s="T195">Вот там под маленьким деревцем хочет улечься, дрожь от мороза пробирает до сердца.</ta>
            <ta e="T219" id="Seg_9098" s="T208">Когда так лежала, сильный шум послышался: то ли гонят оленей, то ли что-то другое.</ta>
            <ta e="T225" id="Seg_9099" s="T219">Старуха идет в ту сторону.</ta>
            <ta e="T240" id="Seg_9100" s="T225">Так идя, о диво! к оленям, густым туманом [дыхания] месяц закрывающим, к скоту, легким туманом [дыхания] солнце застилающему, приходит.</ta>
            <ta e="T251" id="Seg_9101" s="T240">Когда эта старуха в поисках жилья пробиралась через стадо, обе полы ее одежды вконец истерлись.</ta>
            <ta e="T258" id="Seg_9102" s="T251">Вдруг видит: в низине поставлено полным-полно урас.</ta>
            <ta e="T274" id="Seg_9103" s="T258">Огромное жилище стоит, около него, каравана из несколько десятков нарт стоит.</ta>
            <ta e="T282" id="Seg_9104" s="T274">Старуха открыла этот чум.</ta>
            <ta e="T287" id="Seg_9105" s="T282">Оглядела: совсем пусто, людей нет.</ta>
            <ta e="T302" id="Seg_9106" s="T287">Все котлы с варевом на огне кипят, вся еда приготовленная стоит, везде каменные кровати — с таким убранством чум стоит.</ta>
            <ta e="T317" id="Seg_9107" s="T302">Та старуха, войдя, то, что никогда не едала, ест, то, что никогда не пробовала, пробует, и русской еды много, и долганской еды много.</ta>
            <ta e="T325" id="Seg_9108" s="T317">Эта старуха, умывшись-причесавшись, в соболиные меховые одежды оделась.</ta>
            <ta e="T331" id="Seg_9109" s="T325">За все это время ни один человек не появился.</ta>
            <ta e="T338" id="Seg_9110" s="T331">Три дня с тех пор никого не было.</ta>
            <ta e="T347" id="Seg_9111" s="T338">Только как вечер настанет, через дымоход старые человеческие черепа сыплются.</ta>
            <ta e="T359" id="Seg_9112" s="T347">Их со словами: "Тресните-провалитесь!" топчет, и те в землю уходят-пропадают.</ta>
            <ta e="T377" id="Seg_9113" s="T359">Старуха, может, несколько десятков лет так прожила; олени ее никуда не разбегаются, лошади тут же лежат, рыба — будто только что наловлена.</ta>
            <ta e="T382" id="Seg_9114" s="T377">Так старуха в богатстве и достатке живет одна-одинешенька.</ta>
            <ta e="T387" id="Seg_9115" s="T382">Наконец, старуха вот что придумала: </ta>
            <ta e="T395" id="Seg_9116" s="T387">"Э, чем вот так сидеть, пойти проведать бедного своего старикашку надо бы."</ta>
            <ta e="T400" id="Seg_9117" s="T395">На третий год оделась, чтоб пойти к старику своему.</ta>
            <ta e="T404" id="Seg_9118" s="T400">Нарезала и взяла хлеба.</ta>
            <ta e="T410" id="Seg_9119" s="T404">После этого еще жир дикого [оленя] взять решила.</ta>
            <ta e="T415" id="Seg_9120" s="T410">После этого отправилась к своему старику.</ta>
            <ta e="T421" id="Seg_9121" s="T415">Шла-шла и пришла к голомо своего старика.</ta>
            <ta e="T430" id="Seg_9122" s="T421">Оказалось, ничего нет, все снегом заметено, развалилось.</ta>
            <ta e="T435" id="Seg_9123" s="T430">Но все-таки подобралась к дымоходу.</ta>
            <ta e="T453" id="Seg_9124" s="T435">Около очага лежит [старик], превратившись в безобразного червя с тремя волосами, от лошади только черные копыта и голенные кости остались. </ta>
            <ta e="T457" id="Seg_9125" s="T453">Вот кем оборотился, говорят.</ta>
            <ta e="T462" id="Seg_9126" s="T457">Чтобы не умереть, оборотился, говорят.</ta>
            <ta e="T467" id="Seg_9127" s="T462">Вот в него хлебом бросила. </ta>
            <ta e="T472" id="Seg_9128" s="T467">На это, покорежившись, сказал: </ta>
            <ta e="T478" id="Seg_9129" s="T472">"Глина, [отколовшаяся] от печи, не дает лежать."</ta>
            <ta e="T480" id="Seg_9130" s="T478">Жиром бросила. </ta>
            <ta e="T485" id="Seg_9131" s="T480">"Ой, падающие комья снега не дают лежать."</ta>
            <ta e="T487" id="Seg_9132" s="T485">Опять бросила.</ta>
            <ta e="T492" id="Seg_9133" s="T487">Попробовал это погрызть - оказывается, сало.</ta>
            <ta e="T497" id="Seg_9134" s="T492">На это: "О, тангара, тангара, еще немного", сказал. </ta>
            <ta e="T503" id="Seg_9135" s="T497">"Ну, старик, открывай же дверь", сказала старуха. </ta>
            <ta e="T506" id="Seg_9136" s="T503">Двери все же открыл. </ta>
            <ta e="T511" id="Seg_9137" s="T506">Старуха, его накормив, к себе домой увела.</ta>
            <ta e="T524" id="Seg_9138" s="T511">Вот, приведя домой, умыла-причесала его, одела в одежды, которых никогда не носил, и посадила на хрустальную кровать.</ta>
            <ta e="T537" id="Seg_9139" s="T524">После этого напоила чаем, а потом мясо выложила, жирный навар в миску налила. </ta>
            <ta e="T543" id="Seg_9140" s="T537">Сделав так, золотые ложку, вилку положила. </ta>
            <ta e="T547" id="Seg_9141" s="T543">И вот, собираясь зачерпнуть ложкой, сказал: </ta>
            <ta e="T558" id="Seg_9142" s="T547">"Что такое, ложка-то моя мала, большую поварешку или ковш подай", сказал. </ta>
            <ta e="T563" id="Seg_9143" s="T558">Та ему большой ковш подала.</ta>
            <ta e="T568" id="Seg_9144" s="T563">Жирный навар из него втянул с шумом в рот.</ta>
            <ta e="T572" id="Seg_9145" s="T568">Тут поперхнулся. </ta>
            <ta e="T591" id="Seg_9146" s="T572">Поперхнувшись, издает громкий звук и, пробив насквозь хрустальную кровать, в ту дыру через в три слоя промерзшую землю проваляется.</ta>
            <ta e="T600" id="Seg_9147" s="T591">Как жила, так и весь век живет.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_9148" s="T0">Жили старик со старухой, говорят.</ta>
            <ta e="T7" id="Seg_9149" s="T4">Рожденных детей у них не было.</ta>
            <ta e="T14" id="Seg_9150" s="T7">В юрте сидят — только огонь их краснеет, такие бедные.</ta>
            <ta e="T19" id="Seg_9151" s="T14">Во двор выйдут — только снег их белеет.</ta>
            <ta e="T22" id="Seg_9152" s="T19">Ничего у них нет.</ta>
            <ta e="T27" id="Seg_9153" s="T22">Только и богатства — одна-единственная лошадь.</ta>
            <ta e="T33" id="Seg_9154" s="T27">А промыслового снаряжения — с тремя грузилами сеть.</ta>
            <ta e="T41" id="Seg_9155" s="T33">Когда в нее попадет какая-нибудь живность, ею питаются, на рожон насаживая.</ta>
            <ta e="T46" id="Seg_9156" s="T41">Вот приходится умирать-пропадать.</ta>
            <ta e="T51" id="Seg_9157" s="T46">Пошел вот старик к старухе.</ta>
            <ta e="T54" id="Seg_9158" s="T51">Входит и говорит: </ta>
            <ta e="T57" id="Seg_9159" s="T54">— Ну старуха,</ta>
            <ta e="T67" id="Seg_9160" s="T57">только единственная наша лошадь может нас спасти и жизнь продлить, надо бы съесть ее, если умрем от голода — после нас [зря] останется.</ta>
            <ta e="T69" id="Seg_9161" s="T67">На это старуха: </ta>
            <ta e="T72" id="Seg_9162" s="T69">— Сам решай, — сказала.</ta>
            <ta e="T75" id="Seg_9163" s="T72">— Если забьешь, съедим.</ta>
            <ta e="T79" id="Seg_9164" s="T75">Муж вышел и убил [лошадь].</ta>
            <ta e="T90" id="Seg_9165" s="T79">Старик снял шкуру, очень холодно было, метельно, старик входит и говорит. </ta>
            <ta e="T98" id="Seg_9166" s="T90">— Мясо лошади перетаскай все без остатка, до крошки в наше жилище-голомо.</ta>
            <ta e="T101" id="Seg_9167" s="T98">Старуха все перетаскала.</ta>
            <ta e="T107" id="Seg_9168" s="T101">Когда старуха захотела погреть руки, старик сказал: </ta>
            <ta e="T118" id="Seg_9169" s="T107">— Если хочешь есть, во всю посуду, в ведра натаскай воды, после этого поешь.</ta>
            <ta e="T127" id="Seg_9170" s="T118">Тогда старуха, выйдя, во всю посуду натаскала воды.</ta>
            <ta e="T134" id="Seg_9171" s="T127">Когда последний котелочек принесла, дверь заперта, оказывается.</ta>
            <ta e="T141" id="Seg_9172" s="T134">— Ну, старик, открой дверь, замерзаю, — сказала.</ta>
            <ta e="T147" id="Seg_9173" s="T141">— Сукина дочь, брось [ты его] на снег, — сказал.</ta>
            <ta e="T156" id="Seg_9174" s="T147">Старуха, бросив тот котелок, совсем замерзает.</ta>
            <ta e="T165" id="Seg_9175" s="T156">Взобравшись на балаган, старуха стала греть руки о теплый дым.</ta>
            <ta e="T176" id="Seg_9176" s="T165">Вот смотрит на мужа. ‎‎А тот, ухватив круп лошади, сидит, расставив ноги, и отделяет жир от кишок.</ta>
            <ta e="T184" id="Seg_9177" s="T176">И рожном начинает колоть руки старухи, протянутые к теплу.</ta>
            <ta e="T195" id="Seg_9178" s="T184">Та с плачем спускается на землю, и, плачарыдая, идет к тому месту, где рубят дрова.</ta>
            <ta e="T208" id="Seg_9179" s="T195">Вот там под маленьким деревцем хочет улечься, дрожь от мороза пробирает до сердца.</ta>
            <ta e="T219" id="Seg_9180" s="T208">Когда так лежала, сильный шум послышался: то ли гонят оленей, то ли что-то другое.</ta>
            <ta e="T225" id="Seg_9181" s="T219">Старуха идет в ту сторону.</ta>
            <ta e="T240" id="Seg_9182" s="T225">Так идя, о диво! к оленям, густым туманом [дыхания] месяц закрывающим, к скоту, легким туманом [дыхания] солнце застилающему, приходит.</ta>
            <ta e="T251" id="Seg_9183" s="T240">Когда эта старуха в поисках жилья пробиралась через стадо, обе полы ее одежды вконец истерлись.</ta>
            <ta e="T258" id="Seg_9184" s="T251">Вдруг видит: в низине поставлено полным-полно урас.</ta>
            <ta e="T274" id="Seg_9185" s="T258">Огромное жилище стоит, около него, на несколько десятков перелесков растянувшись, нагруженные караваны нарт стоят.</ta>
            <ta e="T282" id="Seg_9186" s="T274">Старуха открыла дверь в этот чум.</ta>
            <ta e="T287" id="Seg_9187" s="T282">Оглядела: совсем пусто, людей нет.</ta>
            <ta e="T302" id="Seg_9188" s="T287">Все котлы с варевом на огне кипят, вся еда приготовленная стоит, везде каменные кровати — с таким убранством чум стоит.</ta>
            <ta e="T317" id="Seg_9189" s="T302">Та старуха, войдя, то, что никогда не едала, ест, то, что никогда не пробовала, пробует, и русской еды много, и долганской еды много.</ta>
            <ta e="T325" id="Seg_9190" s="T317">Эта старуха, умывшись-причесавшись, в соболиные меховые одежды оделась.</ta>
            <ta e="T331" id="Seg_9191" s="T325">За все это время ни один человек не появился.</ta>
            <ta e="T338" id="Seg_9192" s="T331">Три дня с тех пор никого не было.</ta>
            <ta e="T347" id="Seg_9193" s="T338">Только как вечер настанет, через дымоход старые человеческие черепа сыплются.</ta>
            <ta e="T359" id="Seg_9194" s="T347">Их со словами: "Тресните-провалитесь!" — топчет, и те в землю уходят-пропадают.</ta>
            <ta e="T377" id="Seg_9195" s="T359">Старуха, может, несколько десятков лет так прожила; олени ее никуда не разбегаются, лошади тут же лежат, рыба — будто только что наловлена.</ta>
            <ta e="T382" id="Seg_9196" s="T377">Так старуха в богатстве и достатке живет одна-одинешенька.</ta>
            <ta e="T387" id="Seg_9197" s="T382">Наконец, старуха вот что придумала: </ta>
            <ta e="T395" id="Seg_9198" s="T387">"Э, чем вот так сидеть, пойти проведать бедного своего старикашку надо бы".</ta>
            <ta e="T400" id="Seg_9199" s="T395">На третий год оделась, чтоб пойти к старику своему.</ta>
            <ta e="T404" id="Seg_9200" s="T400">Нарезала и взяла хлеба.</ta>
            <ta e="T410" id="Seg_9201" s="T404">После этого еще жир дикого [оленя] взять решила.</ta>
            <ta e="T415" id="Seg_9202" s="T410">Все это проделав, отправилась к своему старику.</ta>
            <ta e="T421" id="Seg_9203" s="T415">Шла-шла и пришла к голомо своего старика.</ta>
            <ta e="T430" id="Seg_9204" s="T421">Оказалось, ничего нет, все снегом заметено, развалилось.</ta>
            <ta e="T435" id="Seg_9205" s="T430">Но все-таки подобралась к дымоходу.</ta>
            <ta e="T453" id="Seg_9206" s="T435">А там [в голомо], превратившись в трех шерстистых безобразных червей-гадов, лежит [старик], около очага от лошади только черные копыта и голенные кости остались. </ta>
            <ta e="T457" id="Seg_9207" s="T453">Вот кем оборотился, говорят. </ta>
            <ta e="T462" id="Seg_9208" s="T457">Чтобы не умереть, оборотился, говорят. </ta>
            <ta e="T467" id="Seg_9209" s="T462">Вот в него хлебом бросила. </ta>
            <ta e="T472" id="Seg_9210" s="T467">На это, покорежившись, сказал: </ta>
            <ta e="T478" id="Seg_9211" s="T472">— Глина, [отколовшаяся] от печи, не дает лежать.</ta>
            <ta e="T480" id="Seg_9212" s="T478">Жиром бросила. </ta>
            <ta e="T485" id="Seg_9213" s="T480">— Ой, холодно, падающие комья снега не дают лежать.</ta>
            <ta e="T487" id="Seg_9214" s="T485">Опять бросила.</ta>
            <ta e="T492" id="Seg_9215" s="T487">Попробовал это погрызть - оказывается, сало.</ta>
            <ta e="T497" id="Seg_9216" s="T492">На это: — О, тангара, тангара, постой, — сказал. </ta>
            <ta e="T503" id="Seg_9217" s="T497">— Ну, старик, открывай же дверь, — сказала старуха. </ta>
            <ta e="T506" id="Seg_9218" s="T503">Двери все же открыл. </ta>
            <ta e="T511" id="Seg_9219" s="T506">Старуха, его накормив, к себе домой увела.</ta>
            <ta e="T524" id="Seg_9220" s="T511">Вот, приведя домой, умыла-причесала его, одела в одежды, которых никогда не носил, и посадила на хрустальную кровать.</ta>
            <ta e="T537" id="Seg_9221" s="T524">После этого напоила чаем, а потом мясо выложила, жирный навар в миску налила. </ta>
            <ta e="T543" id="Seg_9222" s="T537">Сделав так, золотые ложку, вилку положила. </ta>
            <ta e="T547" id="Seg_9223" s="T543">И вот, собираясь зачерпнуть ложкой, сказал: </ta>
            <ta e="T558" id="Seg_9224" s="T547">— Что такое, ложка-то моя мала, большую поварешку или ковш подай, — сказал. </ta>
            <ta e="T563" id="Seg_9225" s="T558">Та ему большой ковш подала.</ta>
            <ta e="T568" id="Seg_9226" s="T563">Жирный навар из него втянул с шумом в рот.</ta>
            <ta e="T572" id="Seg_9227" s="T568">Тут поперхнулся. </ta>
            <ta e="T591" id="Seg_9228" s="T572">Поперхнувшись, издал громкий звук и, пробив насквозь хрустальную кровать, в ту дыру через в три слоя промерзшую землю провалился.</ta>
            <ta e="T600" id="Seg_9229" s="T591">Как жила, так и весь век живет.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T601" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
