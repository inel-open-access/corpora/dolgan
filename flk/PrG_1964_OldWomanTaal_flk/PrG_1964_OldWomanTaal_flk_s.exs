<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDD87AC4B6-06EB-A70D-046A-9B323EEEF474">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>PrG_1964_OldWomanTaal_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\PrG_1964_OldWomanTaal_flk\PrG_1964_OldWomanTaal_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">273</ud-information>
            <ud-information attribute-name="# HIAT:w">168</ud-information>
            <ud-information attribute-name="# e">168</ud-information>
            <ud-information attribute-name="# HIAT:u">35</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PrG">
            <abbreviation>PrG</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
         <tli id="T161" time="84.5" type="appl" />
         <tli id="T162" time="85.0" type="appl" />
         <tli id="T163" time="85.5" type="appl" />
         <tli id="T164" time="86.0" type="appl" />
         <tli id="T165" time="86.5" type="appl" />
         <tli id="T166" time="87.0" type="appl" />
         <tli id="T167" time="87.5" type="appl" />
         <tli id="T168" time="88.0" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PrG"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T168" id="Seg_0" n="sc" s="T0">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Taːl</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">emeːksin</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">olorbut</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">ebit</ts>
                  <nts id="Seg_14" n="HIAT:ip">.</nts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_17" n="HIAT:u" s="T4">
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">Biːrde</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">du͡o</ts>
                  <nts id="Seg_23" n="HIAT:ip">—</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_26" n="HIAT:w" s="T6">bu</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">Taːl</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">emeːksin</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">tellegiger</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">iːkteːbit</ts>
                  <nts id="Seg_39" n="HIAT:ip">.</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_42" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">Ol</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">kün</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">du͡o</ts>
                  <nts id="Seg_51" n="HIAT:ip">—</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_54" n="HIAT:w" s="T14">künneːk</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_57" n="HIAT:w" s="T15">kün</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_60" n="HIAT:w" s="T16">bu͡olbut</ts>
                  <nts id="Seg_61" n="HIAT:ip">.</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_64" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_66" n="HIAT:w" s="T17">Taːl</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_69" n="HIAT:w" s="T18">emeːksin</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_72" n="HIAT:w" s="T19">periːnetin</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_75" n="HIAT:w" s="T20">kuːrdaːrɨ</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_78" n="HIAT:w" s="T21">araŋaska</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_81" n="HIAT:w" s="T22">uːrbut</ts>
                  <nts id="Seg_82" n="HIAT:ip">.</nts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_85" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_87" n="HIAT:w" s="T23">Onno</ts>
                  <nts id="Seg_88" n="HIAT:ip">,</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">ol</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_94" n="HIAT:w" s="T25">kiːrbitin</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_97" n="HIAT:w" s="T26">kenne</ts>
                  <nts id="Seg_98" n="HIAT:ip">,</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">tɨ͡al</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">kötögüllübüt</ts>
                  <nts id="Seg_105" n="HIAT:ip">.</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T36" id="Seg_108" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_110" n="HIAT:w" s="T29">Ol</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_113" n="HIAT:w" s="T30">kötögüllen</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_116" n="HIAT:w" s="T31">Taːl</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_119" n="HIAT:w" s="T32">emeːksin</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_122" n="HIAT:w" s="T33">periːnetin</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_125" n="HIAT:w" s="T34">kötüten</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_128" n="HIAT:w" s="T35">ilpit</ts>
                  <nts id="Seg_129" n="HIAT:ip">.</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_132" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_134" n="HIAT:w" s="T36">Taːl</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_137" n="HIAT:w" s="T37">emeːksin</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_140" n="HIAT:w" s="T38">taksa</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_143" n="HIAT:w" s="T39">köppüt</ts>
                  <nts id="Seg_144" n="HIAT:ip">,</nts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_147" n="HIAT:w" s="T40">periːnete</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_150" n="HIAT:w" s="T41">kötö</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_153" n="HIAT:w" s="T42">turar</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_156" n="HIAT:w" s="T43">ebit</ts>
                  <nts id="Seg_157" n="HIAT:ip">.</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_160" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_162" n="HIAT:w" s="T44">Taːl</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_165" n="HIAT:w" s="T45">emeːksin</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_168" n="HIAT:w" s="T46">periːnetin</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_171" n="HIAT:w" s="T47">batɨhan</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_174" n="HIAT:w" s="T48">barbɨt</ts>
                  <nts id="Seg_175" n="HIAT:ip">.</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_178" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_180" n="HIAT:w" s="T49">Baran</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_183" n="HIAT:w" s="T50">ihen</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_186" n="HIAT:w" s="T51">buːska</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_189" n="HIAT:w" s="T52">kelbit</ts>
                  <nts id="Seg_190" n="HIAT:ip">,</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_193" n="HIAT:w" s="T53">kaltɨrɨjan</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_196" n="HIAT:w" s="T54">tüspüt</ts>
                  <nts id="Seg_197" n="HIAT:ip">.</nts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_200" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_202" n="HIAT:w" s="T55">Ol</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_205" n="HIAT:w" s="T56">kaltɨrɨjan</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_208" n="HIAT:w" s="T57">atagɨn</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_211" n="HIAT:w" s="T58">ölörümmüt</ts>
                  <nts id="Seg_212" n="HIAT:ip">.</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_215" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_217" n="HIAT:w" s="T59">Ol</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_220" n="HIAT:w" s="T60">hɨtan</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_223" n="HIAT:w" s="T61">haŋarbɨt</ts>
                  <nts id="Seg_224" n="HIAT:ip">:</nts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_227" n="HIAT:u" s="T62">
                  <nts id="Seg_228" n="HIAT:ip">"</nts>
                  <ts e="T63" id="Seg_230" n="HIAT:w" s="T62">Buːs</ts>
                  <nts id="Seg_231" n="HIAT:ip">,</nts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_234" n="HIAT:w" s="T63">buːs</ts>
                  <nts id="Seg_235" n="HIAT:ip">,</nts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_238" n="HIAT:w" s="T64">berkin</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_241" n="HIAT:w" s="T65">du͡o</ts>
                  <nts id="Seg_242" n="HIAT:ip">?</nts>
                  <nts id="Seg_243" n="HIAT:ip">"</nts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_246" n="HIAT:u" s="T66">
                  <nts id="Seg_247" n="HIAT:ip">"</nts>
                  <ts e="T67" id="Seg_249" n="HIAT:w" s="T66">Berpin</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_252" n="HIAT:w" s="T67">da</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_255" n="HIAT:w" s="T68">berpi͡en</ts>
                  <nts id="Seg_256" n="HIAT:ip">"</nts>
                  <nts id="Seg_257" n="HIAT:ip">,</nts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_260" n="HIAT:w" s="T69">di͡ebit</ts>
                  <nts id="Seg_261" n="HIAT:ip">.</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_264" n="HIAT:u" s="T70">
                  <nts id="Seg_265" n="HIAT:ip">"</nts>
                  <ts e="T71" id="Seg_267" n="HIAT:w" s="T70">Togo-keː</ts>
                  <nts id="Seg_268" n="HIAT:ip">,</nts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_271" n="HIAT:w" s="T71">kün</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_274" n="HIAT:w" s="T72">u͡otugar</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_277" n="HIAT:w" s="T73">uːlagɨn</ts>
                  <nts id="Seg_278" n="HIAT:ip">?</nts>
                  <nts id="Seg_279" n="HIAT:ip">"</nts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_282" n="HIAT:u" s="T74">
                  <nts id="Seg_283" n="HIAT:ip">"</nts>
                  <ts e="T75" id="Seg_285" n="HIAT:w" s="T74">Kün</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_288" n="HIAT:w" s="T75">u͡ota</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_291" n="HIAT:w" s="T76">bert</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_294" n="HIAT:w" s="T77">da</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_297" n="HIAT:w" s="T78">bert</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_300" n="HIAT:w" s="T79">bu͡ollaga</ts>
                  <nts id="Seg_301" n="HIAT:ip">"</nts>
                  <nts id="Seg_302" n="HIAT:ip">,</nts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_305" n="HIAT:w" s="T80">di͡ebit</ts>
                  <nts id="Seg_306" n="HIAT:ip">.</nts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_309" n="HIAT:u" s="T81">
                  <nts id="Seg_310" n="HIAT:ip">"</nts>
                  <ts e="T82" id="Seg_312" n="HIAT:w" s="T81">Kün</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_315" n="HIAT:w" s="T82">u͡ota</ts>
                  <nts id="Seg_316" n="HIAT:ip">,</nts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_319" n="HIAT:w" s="T83">berkin</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_322" n="HIAT:w" s="T84">du͡o</ts>
                  <nts id="Seg_323" n="HIAT:ip">?</nts>
                  <nts id="Seg_324" n="HIAT:ip">"</nts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_327" n="HIAT:u" s="T85">
                  <nts id="Seg_328" n="HIAT:ip">"</nts>
                  <ts e="T86" id="Seg_330" n="HIAT:w" s="T85">Berpin</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_333" n="HIAT:w" s="T86">da</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_336" n="HIAT:w" s="T87">berpi͡en</ts>
                  <nts id="Seg_337" n="HIAT:ip">!</nts>
                  <nts id="Seg_338" n="HIAT:ip">"</nts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_341" n="HIAT:u" s="T88">
                  <nts id="Seg_342" n="HIAT:ip">"</nts>
                  <ts e="T89" id="Seg_344" n="HIAT:w" s="T88">Togo-keː</ts>
                  <nts id="Seg_345" n="HIAT:ip">,</nts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_348" n="HIAT:w" s="T89">taːs</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_351" n="HIAT:w" s="T90">kajaga</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_354" n="HIAT:w" s="T91">kakkalanaːččɨgɨnɨj</ts>
                  <nts id="Seg_355" n="HIAT:ip">?</nts>
                  <nts id="Seg_356" n="HIAT:ip">"</nts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T99" id="Seg_359" n="HIAT:u" s="T92">
                  <nts id="Seg_360" n="HIAT:ip">"</nts>
                  <ts e="T93" id="Seg_362" n="HIAT:w" s="T92">Taːs</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_365" n="HIAT:w" s="T93">kaja</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_368" n="HIAT:w" s="T94">bert</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_371" n="HIAT:w" s="T95">da</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_374" n="HIAT:w" s="T96">bert</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_377" n="HIAT:w" s="T97">bu͡ollaga</ts>
                  <nts id="Seg_378" n="HIAT:ip">"</nts>
                  <nts id="Seg_379" n="HIAT:ip">,</nts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_382" n="HIAT:w" s="T98">di͡ebit</ts>
                  <nts id="Seg_383" n="HIAT:ip">.</nts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_386" n="HIAT:u" s="T99">
                  <nts id="Seg_387" n="HIAT:ip">"</nts>
                  <ts e="T100" id="Seg_389" n="HIAT:w" s="T99">Taːs</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_392" n="HIAT:w" s="T100">kaja</ts>
                  <nts id="Seg_393" n="HIAT:ip">,</nts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_396" n="HIAT:w" s="T101">berkin</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_399" n="HIAT:w" s="T102">du͡o</ts>
                  <nts id="Seg_400" n="HIAT:ip">?</nts>
                  <nts id="Seg_401" n="HIAT:ip">"</nts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_404" n="HIAT:u" s="T103">
                  <nts id="Seg_405" n="HIAT:ip">"</nts>
                  <ts e="T104" id="Seg_407" n="HIAT:w" s="T103">Berpin</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_410" n="HIAT:w" s="T104">da</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_413" n="HIAT:w" s="T105">berpi͡en</ts>
                  <nts id="Seg_414" n="HIAT:ip">"</nts>
                  <nts id="Seg_415" n="HIAT:ip">,</nts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_418" n="HIAT:w" s="T106">di͡ebit</ts>
                  <nts id="Seg_419" n="HIAT:ip">.</nts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T112" id="Seg_422" n="HIAT:u" s="T107">
                  <nts id="Seg_423" n="HIAT:ip">"</nts>
                  <ts e="T108" id="Seg_425" n="HIAT:w" s="T107">Togo-keː</ts>
                  <nts id="Seg_426" n="HIAT:ip">,</nts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_429" n="HIAT:w" s="T108">üːteːn</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_432" n="HIAT:w" s="T109">kutujak</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_435" n="HIAT:w" s="T110">dʼölö</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_438" n="HIAT:w" s="T111">hüːrerij</ts>
                  <nts id="Seg_439" n="HIAT:ip">?</nts>
                  <nts id="Seg_440" n="HIAT:ip">"</nts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_443" n="HIAT:u" s="T112">
                  <nts id="Seg_444" n="HIAT:ip">"</nts>
                  <ts e="T113" id="Seg_446" n="HIAT:w" s="T112">Üːteːn</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_449" n="HIAT:w" s="T113">kutujak</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_452" n="HIAT:w" s="T114">bert</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_455" n="HIAT:w" s="T115">da</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_458" n="HIAT:w" s="T116">bert</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_461" n="HIAT:w" s="T117">bu͡ollaga</ts>
                  <nts id="Seg_462" n="HIAT:ip">"</nts>
                  <nts id="Seg_463" n="HIAT:ip">,</nts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_466" n="HIAT:w" s="T118">di͡ebit</ts>
                  <nts id="Seg_467" n="HIAT:ip">.</nts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_470" n="HIAT:u" s="T119">
                  <nts id="Seg_471" n="HIAT:ip">"</nts>
                  <ts e="T120" id="Seg_473" n="HIAT:w" s="T119">Üːteːn</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_476" n="HIAT:w" s="T120">kutujak</ts>
                  <nts id="Seg_477" n="HIAT:ip">,</nts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_480" n="HIAT:w" s="T121">berkin</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_483" n="HIAT:w" s="T122">du͡o</ts>
                  <nts id="Seg_484" n="HIAT:ip">?</nts>
                  <nts id="Seg_485" n="HIAT:ip">"</nts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_488" n="HIAT:u" s="T123">
                  <nts id="Seg_489" n="HIAT:ip">"</nts>
                  <ts e="T124" id="Seg_491" n="HIAT:w" s="T123">Berpin</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_494" n="HIAT:w" s="T124">da</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_497" n="HIAT:w" s="T125">berpi͡en</ts>
                  <nts id="Seg_498" n="HIAT:ip">!</nts>
                  <nts id="Seg_499" n="HIAT:ip">"</nts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_502" n="HIAT:u" s="T126">
                  <nts id="Seg_503" n="HIAT:ip">"</nts>
                  <ts e="T127" id="Seg_505" n="HIAT:w" s="T126">Togo-keː</ts>
                  <nts id="Seg_506" n="HIAT:ip">,</nts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_509" n="HIAT:w" s="T127">haːmaj</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_512" n="HIAT:w" s="T128">ogolorugar</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_515" n="HIAT:w" s="T129">ölörtöröːččügünüj</ts>
                  <nts id="Seg_516" n="HIAT:ip">?</nts>
                  <nts id="Seg_517" n="HIAT:ip">"</nts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T136" id="Seg_520" n="HIAT:u" s="T130">
                  <nts id="Seg_521" n="HIAT:ip">"</nts>
                  <ts e="T131" id="Seg_523" n="HIAT:w" s="T130">Haːmaj</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_526" n="HIAT:w" s="T131">ogoloro</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_529" n="HIAT:w" s="T132">bert</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_532" n="HIAT:w" s="T133">da</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_535" n="HIAT:w" s="T134">bert</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_538" n="HIAT:w" s="T135">bu͡ollaktara</ts>
                  <nts id="Seg_539" n="HIAT:ip">.</nts>
                  <nts id="Seg_540" n="HIAT:ip">"</nts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_543" n="HIAT:u" s="T136">
                  <nts id="Seg_544" n="HIAT:ip">"</nts>
                  <ts e="T137" id="Seg_546" n="HIAT:w" s="T136">Haːmaj</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_549" n="HIAT:w" s="T137">ogoloro</ts>
                  <nts id="Seg_550" n="HIAT:ip">,</nts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_553" n="HIAT:w" s="T138">berkit</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_556" n="HIAT:w" s="T139">du͡o</ts>
                  <nts id="Seg_557" n="HIAT:ip">?</nts>
                  <nts id="Seg_558" n="HIAT:ip">"</nts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_561" n="HIAT:u" s="T140">
                  <nts id="Seg_562" n="HIAT:ip">"</nts>
                  <ts e="T141" id="Seg_564" n="HIAT:w" s="T140">Berpit</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_567" n="HIAT:w" s="T141">da</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_570" n="HIAT:w" s="T142">berpi͡et</ts>
                  <nts id="Seg_571" n="HIAT:ip">"</nts>
                  <nts id="Seg_572" n="HIAT:ip">,</nts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_575" n="HIAT:w" s="T143">di͡ebitter</ts>
                  <nts id="Seg_576" n="HIAT:ip">.</nts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_579" n="HIAT:u" s="T144">
                  <nts id="Seg_580" n="HIAT:ip">"</nts>
                  <ts e="T145" id="Seg_582" n="HIAT:w" s="T144">Togo-keː</ts>
                  <nts id="Seg_583" n="HIAT:ip">,</nts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_586" n="HIAT:w" s="T145">himi͡erkitiger</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_589" n="HIAT:w" s="T146">kottoroːččugutuj</ts>
                  <nts id="Seg_590" n="HIAT:ip">?</nts>
                  <nts id="Seg_591" n="HIAT:ip">"</nts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_594" n="HIAT:u" s="T147">
                  <nts id="Seg_595" n="HIAT:ip">"</nts>
                  <ts e="T148" id="Seg_597" n="HIAT:w" s="T147">Himi͡ert</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_600" n="HIAT:w" s="T148">bert</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_603" n="HIAT:w" s="T149">da</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_606" n="HIAT:w" s="T150">bert</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_609" n="HIAT:w" s="T151">bu͡ollaga</ts>
                  <nts id="Seg_610" n="HIAT:ip">"</nts>
                  <nts id="Seg_611" n="HIAT:ip">,</nts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_614" n="HIAT:w" s="T152">di͡ebitter</ts>
                  <nts id="Seg_615" n="HIAT:ip">.</nts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_618" n="HIAT:u" s="T153">
                  <nts id="Seg_619" n="HIAT:ip">"</nts>
                  <ts e="T154" id="Seg_621" n="HIAT:w" s="T153">Himi͡ert</ts>
                  <nts id="Seg_622" n="HIAT:ip">,</nts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_625" n="HIAT:w" s="T154">berkin</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_628" n="HIAT:w" s="T155">du͡o</ts>
                  <nts id="Seg_629" n="HIAT:ip">?</nts>
                  <nts id="Seg_630" n="HIAT:ip">"</nts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_633" n="HIAT:u" s="T156">
                  <nts id="Seg_634" n="HIAT:ip">"</nts>
                  <ts e="T157" id="Seg_636" n="HIAT:w" s="T156">Berpin</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_639" n="HIAT:w" s="T157">da</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_642" n="HIAT:w" s="T158">berpi͡en</ts>
                  <nts id="Seg_643" n="HIAT:ip">"</nts>
                  <nts id="Seg_644" n="HIAT:ip">,</nts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_647" n="HIAT:w" s="T159">di͡ebit</ts>
                  <nts id="Seg_648" n="HIAT:ip">.</nts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_651" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_653" n="HIAT:w" s="T160">Innʼe</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_656" n="HIAT:w" s="T161">di͡en</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_659" n="HIAT:w" s="T162">baran</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_662" n="HIAT:w" s="T163">Taːl</ts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_665" n="HIAT:w" s="T164">emeːksin</ts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_668" n="HIAT:w" s="T165">ölön</ts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_671" n="HIAT:w" s="T166">kaːlbɨt</ts>
                  <nts id="Seg_672" n="HIAT:ip">.</nts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_675" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_677" n="HIAT:w" s="T167">Elete</ts>
                  <nts id="Seg_678" n="HIAT:ip">.</nts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T168" id="Seg_680" n="sc" s="T0">
               <ts e="T1" id="Seg_682" n="e" s="T0">Taːl </ts>
               <ts e="T2" id="Seg_684" n="e" s="T1">emeːksin </ts>
               <ts e="T3" id="Seg_686" n="e" s="T2">olorbut </ts>
               <ts e="T4" id="Seg_688" n="e" s="T3">ebit. </ts>
               <ts e="T5" id="Seg_690" n="e" s="T4">Biːrde </ts>
               <ts e="T6" id="Seg_692" n="e" s="T5">du͡o— </ts>
               <ts e="T7" id="Seg_694" n="e" s="T6">bu </ts>
               <ts e="T8" id="Seg_696" n="e" s="T7">Taːl </ts>
               <ts e="T9" id="Seg_698" n="e" s="T8">emeːksin </ts>
               <ts e="T10" id="Seg_700" n="e" s="T9">tellegiger </ts>
               <ts e="T11" id="Seg_702" n="e" s="T10">iːkteːbit. </ts>
               <ts e="T12" id="Seg_704" n="e" s="T11">Ol </ts>
               <ts e="T13" id="Seg_706" n="e" s="T12">kün </ts>
               <ts e="T14" id="Seg_708" n="e" s="T13">du͡o— </ts>
               <ts e="T15" id="Seg_710" n="e" s="T14">künneːk </ts>
               <ts e="T16" id="Seg_712" n="e" s="T15">kün </ts>
               <ts e="T17" id="Seg_714" n="e" s="T16">bu͡olbut. </ts>
               <ts e="T18" id="Seg_716" n="e" s="T17">Taːl </ts>
               <ts e="T19" id="Seg_718" n="e" s="T18">emeːksin </ts>
               <ts e="T20" id="Seg_720" n="e" s="T19">periːnetin </ts>
               <ts e="T21" id="Seg_722" n="e" s="T20">kuːrdaːrɨ </ts>
               <ts e="T22" id="Seg_724" n="e" s="T21">araŋaska </ts>
               <ts e="T23" id="Seg_726" n="e" s="T22">uːrbut. </ts>
               <ts e="T24" id="Seg_728" n="e" s="T23">Onno, </ts>
               <ts e="T25" id="Seg_730" n="e" s="T24">ol </ts>
               <ts e="T26" id="Seg_732" n="e" s="T25">kiːrbitin </ts>
               <ts e="T27" id="Seg_734" n="e" s="T26">kenne, </ts>
               <ts e="T28" id="Seg_736" n="e" s="T27">tɨ͡al </ts>
               <ts e="T29" id="Seg_738" n="e" s="T28">kötögüllübüt. </ts>
               <ts e="T30" id="Seg_740" n="e" s="T29">Ol </ts>
               <ts e="T31" id="Seg_742" n="e" s="T30">kötögüllen </ts>
               <ts e="T32" id="Seg_744" n="e" s="T31">Taːl </ts>
               <ts e="T33" id="Seg_746" n="e" s="T32">emeːksin </ts>
               <ts e="T34" id="Seg_748" n="e" s="T33">periːnetin </ts>
               <ts e="T35" id="Seg_750" n="e" s="T34">kötüten </ts>
               <ts e="T36" id="Seg_752" n="e" s="T35">ilpit. </ts>
               <ts e="T37" id="Seg_754" n="e" s="T36">Taːl </ts>
               <ts e="T38" id="Seg_756" n="e" s="T37">emeːksin </ts>
               <ts e="T39" id="Seg_758" n="e" s="T38">taksa </ts>
               <ts e="T40" id="Seg_760" n="e" s="T39">köppüt, </ts>
               <ts e="T41" id="Seg_762" n="e" s="T40">periːnete </ts>
               <ts e="T42" id="Seg_764" n="e" s="T41">kötö </ts>
               <ts e="T43" id="Seg_766" n="e" s="T42">turar </ts>
               <ts e="T44" id="Seg_768" n="e" s="T43">ebit. </ts>
               <ts e="T45" id="Seg_770" n="e" s="T44">Taːl </ts>
               <ts e="T46" id="Seg_772" n="e" s="T45">emeːksin </ts>
               <ts e="T47" id="Seg_774" n="e" s="T46">periːnetin </ts>
               <ts e="T48" id="Seg_776" n="e" s="T47">batɨhan </ts>
               <ts e="T49" id="Seg_778" n="e" s="T48">barbɨt. </ts>
               <ts e="T50" id="Seg_780" n="e" s="T49">Baran </ts>
               <ts e="T51" id="Seg_782" n="e" s="T50">ihen </ts>
               <ts e="T52" id="Seg_784" n="e" s="T51">buːska </ts>
               <ts e="T53" id="Seg_786" n="e" s="T52">kelbit, </ts>
               <ts e="T54" id="Seg_788" n="e" s="T53">kaltɨrɨjan </ts>
               <ts e="T55" id="Seg_790" n="e" s="T54">tüspüt. </ts>
               <ts e="T56" id="Seg_792" n="e" s="T55">Ol </ts>
               <ts e="T57" id="Seg_794" n="e" s="T56">kaltɨrɨjan </ts>
               <ts e="T58" id="Seg_796" n="e" s="T57">atagɨn </ts>
               <ts e="T59" id="Seg_798" n="e" s="T58">ölörümmüt. </ts>
               <ts e="T60" id="Seg_800" n="e" s="T59">Ol </ts>
               <ts e="T61" id="Seg_802" n="e" s="T60">hɨtan </ts>
               <ts e="T62" id="Seg_804" n="e" s="T61">haŋarbɨt: </ts>
               <ts e="T63" id="Seg_806" n="e" s="T62">"Buːs, </ts>
               <ts e="T64" id="Seg_808" n="e" s="T63">buːs, </ts>
               <ts e="T65" id="Seg_810" n="e" s="T64">berkin </ts>
               <ts e="T66" id="Seg_812" n="e" s="T65">du͡o?" </ts>
               <ts e="T67" id="Seg_814" n="e" s="T66">"Berpin </ts>
               <ts e="T68" id="Seg_816" n="e" s="T67">da </ts>
               <ts e="T69" id="Seg_818" n="e" s="T68">berpi͡en", </ts>
               <ts e="T70" id="Seg_820" n="e" s="T69">di͡ebit. </ts>
               <ts e="T71" id="Seg_822" n="e" s="T70">"Togo-keː, </ts>
               <ts e="T72" id="Seg_824" n="e" s="T71">kün </ts>
               <ts e="T73" id="Seg_826" n="e" s="T72">u͡otugar </ts>
               <ts e="T74" id="Seg_828" n="e" s="T73">uːlagɨn?" </ts>
               <ts e="T75" id="Seg_830" n="e" s="T74">"Kün </ts>
               <ts e="T76" id="Seg_832" n="e" s="T75">u͡ota </ts>
               <ts e="T77" id="Seg_834" n="e" s="T76">bert </ts>
               <ts e="T78" id="Seg_836" n="e" s="T77">da </ts>
               <ts e="T79" id="Seg_838" n="e" s="T78">bert </ts>
               <ts e="T80" id="Seg_840" n="e" s="T79">bu͡ollaga", </ts>
               <ts e="T81" id="Seg_842" n="e" s="T80">di͡ebit. </ts>
               <ts e="T82" id="Seg_844" n="e" s="T81">"Kün </ts>
               <ts e="T83" id="Seg_846" n="e" s="T82">u͡ota, </ts>
               <ts e="T84" id="Seg_848" n="e" s="T83">berkin </ts>
               <ts e="T85" id="Seg_850" n="e" s="T84">du͡o?" </ts>
               <ts e="T86" id="Seg_852" n="e" s="T85">"Berpin </ts>
               <ts e="T87" id="Seg_854" n="e" s="T86">da </ts>
               <ts e="T88" id="Seg_856" n="e" s="T87">berpi͡en!" </ts>
               <ts e="T89" id="Seg_858" n="e" s="T88">"Togo-keː, </ts>
               <ts e="T90" id="Seg_860" n="e" s="T89">taːs </ts>
               <ts e="T91" id="Seg_862" n="e" s="T90">kajaga </ts>
               <ts e="T92" id="Seg_864" n="e" s="T91">kakkalanaːččɨgɨnɨj?" </ts>
               <ts e="T93" id="Seg_866" n="e" s="T92">"Taːs </ts>
               <ts e="T94" id="Seg_868" n="e" s="T93">kaja </ts>
               <ts e="T95" id="Seg_870" n="e" s="T94">bert </ts>
               <ts e="T96" id="Seg_872" n="e" s="T95">da </ts>
               <ts e="T97" id="Seg_874" n="e" s="T96">bert </ts>
               <ts e="T98" id="Seg_876" n="e" s="T97">bu͡ollaga", </ts>
               <ts e="T99" id="Seg_878" n="e" s="T98">di͡ebit. </ts>
               <ts e="T100" id="Seg_880" n="e" s="T99">"Taːs </ts>
               <ts e="T101" id="Seg_882" n="e" s="T100">kaja, </ts>
               <ts e="T102" id="Seg_884" n="e" s="T101">berkin </ts>
               <ts e="T103" id="Seg_886" n="e" s="T102">du͡o?" </ts>
               <ts e="T104" id="Seg_888" n="e" s="T103">"Berpin </ts>
               <ts e="T105" id="Seg_890" n="e" s="T104">da </ts>
               <ts e="T106" id="Seg_892" n="e" s="T105">berpi͡en", </ts>
               <ts e="T107" id="Seg_894" n="e" s="T106">di͡ebit. </ts>
               <ts e="T108" id="Seg_896" n="e" s="T107">"Togo-keː, </ts>
               <ts e="T109" id="Seg_898" n="e" s="T108">üːteːn </ts>
               <ts e="T110" id="Seg_900" n="e" s="T109">kutujak </ts>
               <ts e="T111" id="Seg_902" n="e" s="T110">dʼölö </ts>
               <ts e="T112" id="Seg_904" n="e" s="T111">hüːrerij?" </ts>
               <ts e="T113" id="Seg_906" n="e" s="T112">"Üːteːn </ts>
               <ts e="T114" id="Seg_908" n="e" s="T113">kutujak </ts>
               <ts e="T115" id="Seg_910" n="e" s="T114">bert </ts>
               <ts e="T116" id="Seg_912" n="e" s="T115">da </ts>
               <ts e="T117" id="Seg_914" n="e" s="T116">bert </ts>
               <ts e="T118" id="Seg_916" n="e" s="T117">bu͡ollaga", </ts>
               <ts e="T119" id="Seg_918" n="e" s="T118">di͡ebit. </ts>
               <ts e="T120" id="Seg_920" n="e" s="T119">"Üːteːn </ts>
               <ts e="T121" id="Seg_922" n="e" s="T120">kutujak, </ts>
               <ts e="T122" id="Seg_924" n="e" s="T121">berkin </ts>
               <ts e="T123" id="Seg_926" n="e" s="T122">du͡o?" </ts>
               <ts e="T124" id="Seg_928" n="e" s="T123">"Berpin </ts>
               <ts e="T125" id="Seg_930" n="e" s="T124">da </ts>
               <ts e="T126" id="Seg_932" n="e" s="T125">berpi͡en!" </ts>
               <ts e="T127" id="Seg_934" n="e" s="T126">"Togo-keː, </ts>
               <ts e="T128" id="Seg_936" n="e" s="T127">haːmaj </ts>
               <ts e="T129" id="Seg_938" n="e" s="T128">ogolorugar </ts>
               <ts e="T130" id="Seg_940" n="e" s="T129">ölörtöröːččügünüj?" </ts>
               <ts e="T131" id="Seg_942" n="e" s="T130">"Haːmaj </ts>
               <ts e="T132" id="Seg_944" n="e" s="T131">ogoloro </ts>
               <ts e="T133" id="Seg_946" n="e" s="T132">bert </ts>
               <ts e="T134" id="Seg_948" n="e" s="T133">da </ts>
               <ts e="T135" id="Seg_950" n="e" s="T134">bert </ts>
               <ts e="T136" id="Seg_952" n="e" s="T135">bu͡ollaktara." </ts>
               <ts e="T137" id="Seg_954" n="e" s="T136">"Haːmaj </ts>
               <ts e="T138" id="Seg_956" n="e" s="T137">ogoloro, </ts>
               <ts e="T139" id="Seg_958" n="e" s="T138">berkit </ts>
               <ts e="T140" id="Seg_960" n="e" s="T139">du͡o?" </ts>
               <ts e="T141" id="Seg_962" n="e" s="T140">"Berpit </ts>
               <ts e="T142" id="Seg_964" n="e" s="T141">da </ts>
               <ts e="T143" id="Seg_966" n="e" s="T142">berpi͡et", </ts>
               <ts e="T144" id="Seg_968" n="e" s="T143">di͡ebitter. </ts>
               <ts e="T145" id="Seg_970" n="e" s="T144">"Togo-keː, </ts>
               <ts e="T146" id="Seg_972" n="e" s="T145">himi͡erkitiger </ts>
               <ts e="T147" id="Seg_974" n="e" s="T146">kottoroːččugutuj?" </ts>
               <ts e="T148" id="Seg_976" n="e" s="T147">"Himi͡ert </ts>
               <ts e="T149" id="Seg_978" n="e" s="T148">bert </ts>
               <ts e="T150" id="Seg_980" n="e" s="T149">da </ts>
               <ts e="T151" id="Seg_982" n="e" s="T150">bert </ts>
               <ts e="T152" id="Seg_984" n="e" s="T151">bu͡ollaga", </ts>
               <ts e="T153" id="Seg_986" n="e" s="T152">di͡ebitter. </ts>
               <ts e="T154" id="Seg_988" n="e" s="T153">"Himi͡ert, </ts>
               <ts e="T155" id="Seg_990" n="e" s="T154">berkin </ts>
               <ts e="T156" id="Seg_992" n="e" s="T155">du͡o?" </ts>
               <ts e="T157" id="Seg_994" n="e" s="T156">"Berpin </ts>
               <ts e="T158" id="Seg_996" n="e" s="T157">da </ts>
               <ts e="T159" id="Seg_998" n="e" s="T158">berpi͡en", </ts>
               <ts e="T160" id="Seg_1000" n="e" s="T159">di͡ebit. </ts>
               <ts e="T161" id="Seg_1002" n="e" s="T160">Innʼe </ts>
               <ts e="T162" id="Seg_1004" n="e" s="T161">di͡en </ts>
               <ts e="T163" id="Seg_1006" n="e" s="T162">baran </ts>
               <ts e="T164" id="Seg_1008" n="e" s="T163">Taːl </ts>
               <ts e="T165" id="Seg_1010" n="e" s="T164">emeːksin </ts>
               <ts e="T166" id="Seg_1012" n="e" s="T165">ölön </ts>
               <ts e="T167" id="Seg_1014" n="e" s="T166">kaːlbɨt. </ts>
               <ts e="T168" id="Seg_1016" n="e" s="T167">Elete. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T4" id="Seg_1017" s="T0">PrG_1964_OldWomanTaal_flk.001 (001.001)</ta>
            <ta e="T11" id="Seg_1018" s="T4">PrG_1964_OldWomanTaal_flk.002 (001.002)</ta>
            <ta e="T17" id="Seg_1019" s="T11">PrG_1964_OldWomanTaal_flk.003 (001.003)</ta>
            <ta e="T23" id="Seg_1020" s="T17">PrG_1964_OldWomanTaal_flk.004 (001.004)</ta>
            <ta e="T29" id="Seg_1021" s="T23">PrG_1964_OldWomanTaal_flk.005 (001.005)</ta>
            <ta e="T36" id="Seg_1022" s="T29">PrG_1964_OldWomanTaal_flk.006 (001.006)</ta>
            <ta e="T44" id="Seg_1023" s="T36">PrG_1964_OldWomanTaal_flk.007 (001.007)</ta>
            <ta e="T49" id="Seg_1024" s="T44">PrG_1964_OldWomanTaal_flk.008 (001.008)</ta>
            <ta e="T55" id="Seg_1025" s="T49">PrG_1964_OldWomanTaal_flk.009 (001.009)</ta>
            <ta e="T59" id="Seg_1026" s="T55">PrG_1964_OldWomanTaal_flk.010 (001.010)</ta>
            <ta e="T62" id="Seg_1027" s="T59">PrG_1964_OldWomanTaal_flk.011 (001.011)</ta>
            <ta e="T66" id="Seg_1028" s="T62">PrG_1964_OldWomanTaal_flk.012 (001.011)</ta>
            <ta e="T70" id="Seg_1029" s="T66">PrG_1964_OldWomanTaal_flk.013 (001.012)</ta>
            <ta e="T74" id="Seg_1030" s="T70">PrG_1964_OldWomanTaal_flk.014 (001.014)</ta>
            <ta e="T81" id="Seg_1031" s="T74">PrG_1964_OldWomanTaal_flk.015 (001.015)</ta>
            <ta e="T85" id="Seg_1032" s="T81">PrG_1964_OldWomanTaal_flk.016 (001.017)</ta>
            <ta e="T88" id="Seg_1033" s="T85">PrG_1964_OldWomanTaal_flk.017 (001.018)</ta>
            <ta e="T92" id="Seg_1034" s="T88">PrG_1964_OldWomanTaal_flk.018 (001.019)</ta>
            <ta e="T99" id="Seg_1035" s="T92">PrG_1964_OldWomanTaal_flk.019 (001.020)</ta>
            <ta e="T103" id="Seg_1036" s="T99">PrG_1964_OldWomanTaal_flk.020 (001.021)</ta>
            <ta e="T107" id="Seg_1037" s="T103">PrG_1964_OldWomanTaal_flk.021 (001.022)</ta>
            <ta e="T112" id="Seg_1038" s="T107">PrG_1964_OldWomanTaal_flk.022 (001.024)</ta>
            <ta e="T119" id="Seg_1039" s="T112">PrG_1964_OldWomanTaal_flk.023 (001.025)</ta>
            <ta e="T123" id="Seg_1040" s="T119">PrG_1964_OldWomanTaal_flk.024 (001.026)</ta>
            <ta e="T126" id="Seg_1041" s="T123">PrG_1964_OldWomanTaal_flk.025 (001.027)</ta>
            <ta e="T130" id="Seg_1042" s="T126">PrG_1964_OldWomanTaal_flk.026 (001.028)</ta>
            <ta e="T136" id="Seg_1043" s="T130">PrG_1964_OldWomanTaal_flk.027 (001.029)</ta>
            <ta e="T140" id="Seg_1044" s="T136">PrG_1964_OldWomanTaal_flk.028 (001.030)</ta>
            <ta e="T144" id="Seg_1045" s="T140">PrG_1964_OldWomanTaal_flk.029 (001.031)</ta>
            <ta e="T147" id="Seg_1046" s="T144">PrG_1964_OldWomanTaal_flk.030 (001.033)</ta>
            <ta e="T153" id="Seg_1047" s="T147">PrG_1964_OldWomanTaal_flk.031 (001.034)</ta>
            <ta e="T156" id="Seg_1048" s="T153">PrG_1964_OldWomanTaal_flk.032 (001.035)</ta>
            <ta e="T160" id="Seg_1049" s="T156">PrG_1964_OldWomanTaal_flk.033 (001.036)</ta>
            <ta e="T167" id="Seg_1050" s="T160">PrG_1964_OldWomanTaal_flk.034 (001.038)</ta>
            <ta e="T168" id="Seg_1051" s="T167">PrG_1964_OldWomanTaal_flk.035 (001.039)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T4" id="Seg_1052" s="T0">Таал эмээксин олорбут эбит.</ta>
            <ta e="T11" id="Seg_1053" s="T4">Биирдэ дуо — бу Таал эмээксин тэллэгигэр ииктээбит.</ta>
            <ta e="T17" id="Seg_1054" s="T11">Ол күн дуо — күннээк күн буолбут.</ta>
            <ta e="T23" id="Seg_1055" s="T17">Таал эмээксин пэриинэтин куурдаары араҥаска уурбут.</ta>
            <ta e="T29" id="Seg_1056" s="T23">Онно, ол киирбитин кэннэ, тыал көтөгүллүбүт.</ta>
            <ta e="T36" id="Seg_1057" s="T29">Ол көтөгүллэн Таал эмээксин пэриинэтин көтүтэн илпит.</ta>
            <ta e="T44" id="Seg_1058" s="T36">Таал эмээксин такса көппүт, пэриинэтэ көтө турар эбит.</ta>
            <ta e="T49" id="Seg_1059" s="T44">Таал эмээксин пэриинэтин батыһан барбыт.</ta>
            <ta e="T55" id="Seg_1060" s="T49">Баран иһэн бууска кэлбит, калтырыйан түспүт.</ta>
            <ta e="T59" id="Seg_1061" s="T55">Ол калтырыйан атагын өлөрүммүт.</ta>
            <ta e="T62" id="Seg_1062" s="T59">Ол һытан һаҥарбыт: </ta>
            <ta e="T66" id="Seg_1063" s="T62">— Буус, буус, бэркин дуо?</ta>
            <ta e="T70" id="Seg_1064" s="T66">— Бэрпин да бэрпиэн! — диэбит.</ta>
            <ta e="T74" id="Seg_1065" s="T70">— Того-кээ, күн уотугар уулагын?</ta>
            <ta e="T81" id="Seg_1066" s="T74">— Күн уота бэрт да бэрт буоллага! — диэбит.</ta>
            <ta e="T85" id="Seg_1067" s="T81">— Күн уота, бэркин дуо?</ta>
            <ta e="T88" id="Seg_1068" s="T85">— Бэрпин да бэрпиэн!</ta>
            <ta e="T92" id="Seg_1069" s="T88">— Того-кээ, таас кайага каккаланааччыгыный?</ta>
            <ta e="T99" id="Seg_1070" s="T92">— Таас кайа бэрт да бэрт буоллага, — диэбит.</ta>
            <ta e="T103" id="Seg_1071" s="T99">— Таас кайа, бэркин дуо?</ta>
            <ta e="T107" id="Seg_1072" s="T103">— Бэрпин да бэрпиэн! — диэбит.</ta>
            <ta e="T112" id="Seg_1073" s="T107">— Того-кээ, үүтээн кутуйак дьөлө һүүрэрий?</ta>
            <ta e="T119" id="Seg_1074" s="T112">— Үүтээн кутуйак бэрт да бэрт буоллага, — диэбит.</ta>
            <ta e="T123" id="Seg_1075" s="T119">— Үүтээн кутуйак, бэркин дуо?</ta>
            <ta e="T126" id="Seg_1076" s="T123">— Бэрпин да бэрпиэн!</ta>
            <ta e="T130" id="Seg_1077" s="T126">— Того-кээ, һаамай оголоругар өлөртөрөөччүгүнүй?</ta>
            <ta e="T136" id="Seg_1078" s="T130">— һаамай оголоро бэрт да бэрт буоллактара.</ta>
            <ta e="T140" id="Seg_1079" s="T136">— һаамай оголоро, бэркит дуо?</ta>
            <ta e="T144" id="Seg_1080" s="T140">— Бэрпит да бэрпиэт! — диэбиттэр.</ta>
            <ta e="T147" id="Seg_1081" s="T144">— Того-кээ, һимиэркитигэр котторооччугутуй?</ta>
            <ta e="T153" id="Seg_1082" s="T147">— Һимиэрт бэрт да бэрт буоллага, — диэбиттэр.</ta>
            <ta e="T156" id="Seg_1083" s="T153">— һимиэрт, бэркин дуо?</ta>
            <ta e="T160" id="Seg_1084" s="T156">— Бэрпин да бэрпиэн!— диэбит.</ta>
            <ta e="T167" id="Seg_1085" s="T160">Инньэ диэн баран Таал эмээксин өлөн каалбыт.</ta>
            <ta e="T168" id="Seg_1086" s="T167">Элэтэ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T4" id="Seg_1087" s="T0">Taːl emeːksin olorbut ebit. </ta>
            <ta e="T11" id="Seg_1088" s="T4">Biːrde du͡o— bu Taːl emeːksin tellegiger iːkteːbit. </ta>
            <ta e="T17" id="Seg_1089" s="T11">Ol kün du͡o— künneːk kün bu͡olbut. </ta>
            <ta e="T23" id="Seg_1090" s="T17">Taːl emeːksin periːnetin kuːrdaːrɨ araŋaska uːrbut. </ta>
            <ta e="T29" id="Seg_1091" s="T23">Onno, ol kiːrbitin kenne, tɨ͡al kötögüllübüt. </ta>
            <ta e="T36" id="Seg_1092" s="T29">Ol kötögüllen Taːl emeːksin periːnetin kötüten ilpit. </ta>
            <ta e="T44" id="Seg_1093" s="T36">Taːl emeːksin taksa köppüt, periːnete kötö turar ebit. </ta>
            <ta e="T49" id="Seg_1094" s="T44">Taːl emeːksin periːnetin batɨhan barbɨt. </ta>
            <ta e="T55" id="Seg_1095" s="T49">Baran ihen buːska kelbit, kaltɨrɨjan tüspüt. </ta>
            <ta e="T59" id="Seg_1096" s="T55">Ol kaltɨrɨjan atagɨn ölörümmüt. </ta>
            <ta e="T62" id="Seg_1097" s="T59">Ol hɨtan haŋarbɨt: </ta>
            <ta e="T66" id="Seg_1098" s="T62">"Buːs, buːs, berkin du͡o?" </ta>
            <ta e="T70" id="Seg_1099" s="T66">"Berpin da berpi͡en", di͡ebit. </ta>
            <ta e="T74" id="Seg_1100" s="T70">"Togo-keː, kün u͡otugar uːlagɨn?" </ta>
            <ta e="T81" id="Seg_1101" s="T74">"Kün u͡ota bert da bert bu͡ollaga", di͡ebit. </ta>
            <ta e="T85" id="Seg_1102" s="T81">"Kün u͡ota, berkin du͡o?" </ta>
            <ta e="T88" id="Seg_1103" s="T85">"Berpin da berpi͡en!" </ta>
            <ta e="T92" id="Seg_1104" s="T88">"Togo-keː, taːs kajaga kakkalanaːččɨgɨnɨj?" </ta>
            <ta e="T99" id="Seg_1105" s="T92">"Taːs kaja bert da bert bu͡ollaga", di͡ebit. </ta>
            <ta e="T103" id="Seg_1106" s="T99">"Taːs kaja, berkin du͡o?" </ta>
            <ta e="T107" id="Seg_1107" s="T103">"Berpin da berpi͡en", di͡ebit. </ta>
            <ta e="T112" id="Seg_1108" s="T107">"Togo-keː, üːteːn kutujak dʼölö hüːrerij?" </ta>
            <ta e="T119" id="Seg_1109" s="T112">"Üːteːn kutujak bert da bert bu͡ollaga", di͡ebit. </ta>
            <ta e="T123" id="Seg_1110" s="T119">"Üːteːn kutujak, berkin du͡o?" </ta>
            <ta e="T126" id="Seg_1111" s="T123">"Berpin da berpi͡en!" </ta>
            <ta e="T130" id="Seg_1112" s="T126">"Togo-keː, haːmaj ogolorugar ölörtöröːččügünüj?" </ta>
            <ta e="T136" id="Seg_1113" s="T130">"Haːmaj ogoloro bert da bert bu͡ollaktara." </ta>
            <ta e="T140" id="Seg_1114" s="T136">"Haːmaj ogoloro, berkit du͡o?" </ta>
            <ta e="T144" id="Seg_1115" s="T140">"Berpit da berpi͡et", di͡ebitter. </ta>
            <ta e="T147" id="Seg_1116" s="T144">"Togo-keː, himi͡erkitiger kottoroːččugutuj?" </ta>
            <ta e="T153" id="Seg_1117" s="T147">"Himi͡ert bert da bert bu͡ollaga", di͡ebitter. </ta>
            <ta e="T156" id="Seg_1118" s="T153">"Himi͡ert, berkin du͡o?" </ta>
            <ta e="T160" id="Seg_1119" s="T156">"Berpin da berpi͡en", di͡ebit. </ta>
            <ta e="T167" id="Seg_1120" s="T160">Innʼe di͡en baran Taːl emeːksin ölön kaːlbɨt. </ta>
            <ta e="T168" id="Seg_1121" s="T167">Elete. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1122" s="T0">Taːl</ta>
            <ta e="T2" id="Seg_1123" s="T1">emeːksin</ta>
            <ta e="T3" id="Seg_1124" s="T2">olor-but</ta>
            <ta e="T4" id="Seg_1125" s="T3">e-bit</ta>
            <ta e="T5" id="Seg_1126" s="T4">biːrde</ta>
            <ta e="T6" id="Seg_1127" s="T5">du͡o</ta>
            <ta e="T7" id="Seg_1128" s="T6">bu</ta>
            <ta e="T8" id="Seg_1129" s="T7">Taːl</ta>
            <ta e="T9" id="Seg_1130" s="T8">emeːksin</ta>
            <ta e="T10" id="Seg_1131" s="T9">telleg-i-ger</ta>
            <ta e="T11" id="Seg_1132" s="T10">iːkteː-bit</ta>
            <ta e="T12" id="Seg_1133" s="T11">ol</ta>
            <ta e="T13" id="Seg_1134" s="T12">kün</ta>
            <ta e="T14" id="Seg_1135" s="T13">du͡o</ta>
            <ta e="T15" id="Seg_1136" s="T14">kün-neːk</ta>
            <ta e="T16" id="Seg_1137" s="T15">kün</ta>
            <ta e="T17" id="Seg_1138" s="T16">bu͡ol-but</ta>
            <ta e="T18" id="Seg_1139" s="T17">Taːl</ta>
            <ta e="T19" id="Seg_1140" s="T18">emeːksin</ta>
            <ta e="T20" id="Seg_1141" s="T19">periːne-ti-n</ta>
            <ta e="T21" id="Seg_1142" s="T20">kuːr-d-aːrɨ</ta>
            <ta e="T22" id="Seg_1143" s="T21">araŋas-ka</ta>
            <ta e="T23" id="Seg_1144" s="T22">uːr-but</ta>
            <ta e="T24" id="Seg_1145" s="T23">onno</ta>
            <ta e="T25" id="Seg_1146" s="T24">ol</ta>
            <ta e="T26" id="Seg_1147" s="T25">kiːr-bit-i-n</ta>
            <ta e="T27" id="Seg_1148" s="T26">kenne</ta>
            <ta e="T28" id="Seg_1149" s="T27">tɨ͡al</ta>
            <ta e="T29" id="Seg_1150" s="T28">kötög-ü-ll-ü-büt</ta>
            <ta e="T30" id="Seg_1151" s="T29">ol</ta>
            <ta e="T31" id="Seg_1152" s="T30">kötög-ü-ll-en</ta>
            <ta e="T32" id="Seg_1153" s="T31">Taːl</ta>
            <ta e="T33" id="Seg_1154" s="T32">emeːksin</ta>
            <ta e="T34" id="Seg_1155" s="T33">periːne-ti-n</ta>
            <ta e="T35" id="Seg_1156" s="T34">köt-ü-t-en</ta>
            <ta e="T36" id="Seg_1157" s="T35">il-pit</ta>
            <ta e="T37" id="Seg_1158" s="T36">Taːl</ta>
            <ta e="T38" id="Seg_1159" s="T37">emeːksin</ta>
            <ta e="T39" id="Seg_1160" s="T38">taks-a</ta>
            <ta e="T40" id="Seg_1161" s="T39">köp-püt</ta>
            <ta e="T41" id="Seg_1162" s="T40">periːne-te</ta>
            <ta e="T42" id="Seg_1163" s="T41">köt-ö</ta>
            <ta e="T43" id="Seg_1164" s="T42">tur-ar</ta>
            <ta e="T44" id="Seg_1165" s="T43">e-bit</ta>
            <ta e="T45" id="Seg_1166" s="T44">Taːl</ta>
            <ta e="T46" id="Seg_1167" s="T45">emeːksin</ta>
            <ta e="T47" id="Seg_1168" s="T46">periːne-ti-n</ta>
            <ta e="T48" id="Seg_1169" s="T47">batɨh-an</ta>
            <ta e="T49" id="Seg_1170" s="T48">bar-bɨt</ta>
            <ta e="T50" id="Seg_1171" s="T49">bar-an</ta>
            <ta e="T51" id="Seg_1172" s="T50">ih-en</ta>
            <ta e="T52" id="Seg_1173" s="T51">buːs-ka</ta>
            <ta e="T53" id="Seg_1174" s="T52">kel-bit</ta>
            <ta e="T54" id="Seg_1175" s="T53">kaltɨrɨj-an</ta>
            <ta e="T55" id="Seg_1176" s="T54">tüs-püt</ta>
            <ta e="T56" id="Seg_1177" s="T55">ol</ta>
            <ta e="T57" id="Seg_1178" s="T56">kaltɨrɨj-an</ta>
            <ta e="T58" id="Seg_1179" s="T57">atag-ɨ-n</ta>
            <ta e="T59" id="Seg_1180" s="T58">ölör-ü-m-müt</ta>
            <ta e="T60" id="Seg_1181" s="T59">ol</ta>
            <ta e="T61" id="Seg_1182" s="T60">hɨt-an</ta>
            <ta e="T62" id="Seg_1183" s="T61">haŋar-bɨt</ta>
            <ta e="T63" id="Seg_1184" s="T62">buːs</ta>
            <ta e="T64" id="Seg_1185" s="T63">buːs</ta>
            <ta e="T65" id="Seg_1186" s="T64">ber-kin</ta>
            <ta e="T66" id="Seg_1187" s="T65">du͡o</ta>
            <ta e="T67" id="Seg_1188" s="T66">ber-pin</ta>
            <ta e="T68" id="Seg_1189" s="T67">da</ta>
            <ta e="T69" id="Seg_1190" s="T68">ber-pi͡en</ta>
            <ta e="T70" id="Seg_1191" s="T69">di͡e-bit</ta>
            <ta e="T71" id="Seg_1192" s="T70">togo-keː</ta>
            <ta e="T72" id="Seg_1193" s="T71">kün</ta>
            <ta e="T73" id="Seg_1194" s="T72">u͡ot-u-gar</ta>
            <ta e="T74" id="Seg_1195" s="T73">uːl-a-gɨn</ta>
            <ta e="T75" id="Seg_1196" s="T74">kün</ta>
            <ta e="T76" id="Seg_1197" s="T75">u͡ot-a</ta>
            <ta e="T77" id="Seg_1198" s="T76">bert</ta>
            <ta e="T78" id="Seg_1199" s="T77">da</ta>
            <ta e="T79" id="Seg_1200" s="T78">bert</ta>
            <ta e="T80" id="Seg_1201" s="T79">bu͡ol-lag-a</ta>
            <ta e="T81" id="Seg_1202" s="T80">di͡e-bit</ta>
            <ta e="T82" id="Seg_1203" s="T81">kün</ta>
            <ta e="T83" id="Seg_1204" s="T82">u͡ot-a</ta>
            <ta e="T84" id="Seg_1205" s="T83">ber-kin</ta>
            <ta e="T85" id="Seg_1206" s="T84">du͡o</ta>
            <ta e="T86" id="Seg_1207" s="T85">ber-pin</ta>
            <ta e="T87" id="Seg_1208" s="T86">da</ta>
            <ta e="T88" id="Seg_1209" s="T87">ber-pi͡en</ta>
            <ta e="T89" id="Seg_1210" s="T88">togo-keː</ta>
            <ta e="T90" id="Seg_1211" s="T89">taːs</ta>
            <ta e="T91" id="Seg_1212" s="T90">kaja-ga</ta>
            <ta e="T92" id="Seg_1213" s="T91">kakka-lan-aːččɨ-gɨn=ɨj</ta>
            <ta e="T93" id="Seg_1214" s="T92">taːs</ta>
            <ta e="T94" id="Seg_1215" s="T93">kaja</ta>
            <ta e="T95" id="Seg_1216" s="T94">bert</ta>
            <ta e="T96" id="Seg_1217" s="T95">da</ta>
            <ta e="T97" id="Seg_1218" s="T96">bert</ta>
            <ta e="T98" id="Seg_1219" s="T97">bu͡ol-lag-a</ta>
            <ta e="T99" id="Seg_1220" s="T98">di͡e-bit</ta>
            <ta e="T100" id="Seg_1221" s="T99">taːs</ta>
            <ta e="T101" id="Seg_1222" s="T100">kaja</ta>
            <ta e="T102" id="Seg_1223" s="T101">ber-kin</ta>
            <ta e="T103" id="Seg_1224" s="T102">du͡o</ta>
            <ta e="T104" id="Seg_1225" s="T103">ber-pin</ta>
            <ta e="T105" id="Seg_1226" s="T104">da</ta>
            <ta e="T106" id="Seg_1227" s="T105">ber-pi͡en</ta>
            <ta e="T107" id="Seg_1228" s="T106">di͡e-bit</ta>
            <ta e="T108" id="Seg_1229" s="T107">togo-keː</ta>
            <ta e="T109" id="Seg_1230" s="T108">üːteːn</ta>
            <ta e="T110" id="Seg_1231" s="T109">kutujak</ta>
            <ta e="T111" id="Seg_1232" s="T110">dʼöl-ö</ta>
            <ta e="T112" id="Seg_1233" s="T111">hüːr-er=ij</ta>
            <ta e="T113" id="Seg_1234" s="T112">üːteːn</ta>
            <ta e="T114" id="Seg_1235" s="T113">kutujak</ta>
            <ta e="T115" id="Seg_1236" s="T114">bert</ta>
            <ta e="T116" id="Seg_1237" s="T115">da</ta>
            <ta e="T117" id="Seg_1238" s="T116">bert</ta>
            <ta e="T118" id="Seg_1239" s="T117">bu͡ol-lag-a</ta>
            <ta e="T119" id="Seg_1240" s="T118">di͡e-bit</ta>
            <ta e="T120" id="Seg_1241" s="T119">üːteːn</ta>
            <ta e="T121" id="Seg_1242" s="T120">kutujak</ta>
            <ta e="T122" id="Seg_1243" s="T121">ber-kin</ta>
            <ta e="T123" id="Seg_1244" s="T122">du͡o</ta>
            <ta e="T124" id="Seg_1245" s="T123">ber-pin</ta>
            <ta e="T125" id="Seg_1246" s="T124">da</ta>
            <ta e="T126" id="Seg_1247" s="T125">ber-pi͡en</ta>
            <ta e="T127" id="Seg_1248" s="T126">togo-keː</ta>
            <ta e="T128" id="Seg_1249" s="T127">haːmaj</ta>
            <ta e="T129" id="Seg_1250" s="T128">ogo-loru-gar</ta>
            <ta e="T130" id="Seg_1251" s="T129">ölör-tör-öːččü-gün=üj</ta>
            <ta e="T131" id="Seg_1252" s="T130">haːmaj</ta>
            <ta e="T132" id="Seg_1253" s="T131">ogo-loro</ta>
            <ta e="T133" id="Seg_1254" s="T132">bert</ta>
            <ta e="T134" id="Seg_1255" s="T133">da</ta>
            <ta e="T135" id="Seg_1256" s="T134">bert</ta>
            <ta e="T136" id="Seg_1257" s="T135">bu͡ol-lak-tara</ta>
            <ta e="T137" id="Seg_1258" s="T136">haːmaj</ta>
            <ta e="T138" id="Seg_1259" s="T137">ogo-loro</ta>
            <ta e="T139" id="Seg_1260" s="T138">ber-kit</ta>
            <ta e="T140" id="Seg_1261" s="T139">du͡o</ta>
            <ta e="T141" id="Seg_1262" s="T140">ber-pit</ta>
            <ta e="T142" id="Seg_1263" s="T141">da</ta>
            <ta e="T143" id="Seg_1264" s="T142">ber-pi͡et</ta>
            <ta e="T144" id="Seg_1265" s="T143">di͡e-bit-ter</ta>
            <ta e="T145" id="Seg_1266" s="T144">togo-keː</ta>
            <ta e="T146" id="Seg_1267" s="T145">himi͡er-kiti-ger</ta>
            <ta e="T147" id="Seg_1268" s="T146">kot-tor-oːčču-gut=uj</ta>
            <ta e="T148" id="Seg_1269" s="T147">himi͡ert</ta>
            <ta e="T149" id="Seg_1270" s="T148">bert</ta>
            <ta e="T150" id="Seg_1271" s="T149">da</ta>
            <ta e="T151" id="Seg_1272" s="T150">bert</ta>
            <ta e="T152" id="Seg_1273" s="T151">bu͡ol-lag-a</ta>
            <ta e="T153" id="Seg_1274" s="T152">di͡e-bit-ter</ta>
            <ta e="T154" id="Seg_1275" s="T153">himi͡ert</ta>
            <ta e="T155" id="Seg_1276" s="T154">ber-kin</ta>
            <ta e="T156" id="Seg_1277" s="T155">du͡o</ta>
            <ta e="T157" id="Seg_1278" s="T156">ber-pin</ta>
            <ta e="T158" id="Seg_1279" s="T157">da</ta>
            <ta e="T159" id="Seg_1280" s="T158">ber-pi͡en</ta>
            <ta e="T160" id="Seg_1281" s="T159">di͡e-bit</ta>
            <ta e="T161" id="Seg_1282" s="T160">innʼe</ta>
            <ta e="T162" id="Seg_1283" s="T161">di͡e-n</ta>
            <ta e="T163" id="Seg_1284" s="T162">baran</ta>
            <ta e="T164" id="Seg_1285" s="T163">Taːl</ta>
            <ta e="T165" id="Seg_1286" s="T164">emeːksin</ta>
            <ta e="T166" id="Seg_1287" s="T165">öl-ön</ta>
            <ta e="T167" id="Seg_1288" s="T166">kaːl-bɨt</ta>
            <ta e="T168" id="Seg_1289" s="T167">ele-te</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1290" s="T0">Taːl</ta>
            <ta e="T2" id="Seg_1291" s="T1">emeːksin</ta>
            <ta e="T3" id="Seg_1292" s="T2">olor-BIT</ta>
            <ta e="T4" id="Seg_1293" s="T3">e-BIT</ta>
            <ta e="T5" id="Seg_1294" s="T4">biːrde</ta>
            <ta e="T6" id="Seg_1295" s="T5">du͡o</ta>
            <ta e="T7" id="Seg_1296" s="T6">bu</ta>
            <ta e="T8" id="Seg_1297" s="T7">Taːl</ta>
            <ta e="T9" id="Seg_1298" s="T8">emeːksin</ta>
            <ta e="T10" id="Seg_1299" s="T9">tellek-tI-GAr</ta>
            <ta e="T11" id="Seg_1300" s="T10">iːkteː-BIT</ta>
            <ta e="T12" id="Seg_1301" s="T11">ol</ta>
            <ta e="T13" id="Seg_1302" s="T12">kün</ta>
            <ta e="T14" id="Seg_1303" s="T13">du͡o</ta>
            <ta e="T15" id="Seg_1304" s="T14">kün-LAːK</ta>
            <ta e="T16" id="Seg_1305" s="T15">kün</ta>
            <ta e="T17" id="Seg_1306" s="T16">bu͡ol-BIT</ta>
            <ta e="T18" id="Seg_1307" s="T17">Taːl</ta>
            <ta e="T19" id="Seg_1308" s="T18">emeːksin</ta>
            <ta e="T20" id="Seg_1309" s="T19">periːne-tI-n</ta>
            <ta e="T21" id="Seg_1310" s="T20">kuːr-t-AːrI</ta>
            <ta e="T22" id="Seg_1311" s="T21">araŋas-GA</ta>
            <ta e="T23" id="Seg_1312" s="T22">uːr-BIT</ta>
            <ta e="T24" id="Seg_1313" s="T23">onno</ta>
            <ta e="T25" id="Seg_1314" s="T24">ol</ta>
            <ta e="T26" id="Seg_1315" s="T25">kiːr-BIT-tI-n</ta>
            <ta e="T27" id="Seg_1316" s="T26">genne</ta>
            <ta e="T28" id="Seg_1317" s="T27">tɨ͡al</ta>
            <ta e="T29" id="Seg_1318" s="T28">kötök-I-LIN-I-BIT</ta>
            <ta e="T30" id="Seg_1319" s="T29">ol</ta>
            <ta e="T31" id="Seg_1320" s="T30">kötök-I-LIN-An</ta>
            <ta e="T32" id="Seg_1321" s="T31">Taːl</ta>
            <ta e="T33" id="Seg_1322" s="T32">emeːksin</ta>
            <ta e="T34" id="Seg_1323" s="T33">periːne-tI-n</ta>
            <ta e="T35" id="Seg_1324" s="T34">köt-I-t-An</ta>
            <ta e="T36" id="Seg_1325" s="T35">ilt-BIT</ta>
            <ta e="T37" id="Seg_1326" s="T36">Taːl</ta>
            <ta e="T38" id="Seg_1327" s="T37">emeːksin</ta>
            <ta e="T39" id="Seg_1328" s="T38">tagɨs-A</ta>
            <ta e="T40" id="Seg_1329" s="T39">köt-BIT</ta>
            <ta e="T41" id="Seg_1330" s="T40">periːne-tA</ta>
            <ta e="T42" id="Seg_1331" s="T41">köt-A</ta>
            <ta e="T43" id="Seg_1332" s="T42">tur-Ar</ta>
            <ta e="T44" id="Seg_1333" s="T43">e-BIT</ta>
            <ta e="T45" id="Seg_1334" s="T44">Taːl</ta>
            <ta e="T46" id="Seg_1335" s="T45">emeːksin</ta>
            <ta e="T47" id="Seg_1336" s="T46">periːne-tI-n</ta>
            <ta e="T48" id="Seg_1337" s="T47">batɨs-An</ta>
            <ta e="T49" id="Seg_1338" s="T48">bar-BIT</ta>
            <ta e="T50" id="Seg_1339" s="T49">bar-An</ta>
            <ta e="T51" id="Seg_1340" s="T50">is-An</ta>
            <ta e="T52" id="Seg_1341" s="T51">buːs-GA</ta>
            <ta e="T53" id="Seg_1342" s="T52">kel-BIT</ta>
            <ta e="T54" id="Seg_1343" s="T53">kaltɨrɨj-An</ta>
            <ta e="T55" id="Seg_1344" s="T54">tüs-BIT</ta>
            <ta e="T56" id="Seg_1345" s="T55">ol</ta>
            <ta e="T57" id="Seg_1346" s="T56">kaltɨrɨj-An</ta>
            <ta e="T58" id="Seg_1347" s="T57">atak-tI-n</ta>
            <ta e="T59" id="Seg_1348" s="T58">ölör-I-n-BIT</ta>
            <ta e="T60" id="Seg_1349" s="T59">ol</ta>
            <ta e="T61" id="Seg_1350" s="T60">hɨt-An</ta>
            <ta e="T62" id="Seg_1351" s="T61">haŋar-BIT</ta>
            <ta e="T63" id="Seg_1352" s="T62">buːs</ta>
            <ta e="T64" id="Seg_1353" s="T63">buːs</ta>
            <ta e="T65" id="Seg_1354" s="T64">bert-GIn</ta>
            <ta e="T66" id="Seg_1355" s="T65">du͡o</ta>
            <ta e="T67" id="Seg_1356" s="T66">bert-BIn</ta>
            <ta e="T68" id="Seg_1357" s="T67">da</ta>
            <ta e="T69" id="Seg_1358" s="T68">bert-BIn</ta>
            <ta e="T70" id="Seg_1359" s="T69">di͡e-BIT</ta>
            <ta e="T71" id="Seg_1360" s="T70">togo-keː</ta>
            <ta e="T72" id="Seg_1361" s="T71">kün</ta>
            <ta e="T73" id="Seg_1362" s="T72">u͡ot-tI-GAr</ta>
            <ta e="T74" id="Seg_1363" s="T73">uːl-A-GIn</ta>
            <ta e="T75" id="Seg_1364" s="T74">kün</ta>
            <ta e="T76" id="Seg_1365" s="T75">u͡ot-tA</ta>
            <ta e="T77" id="Seg_1366" s="T76">bert</ta>
            <ta e="T78" id="Seg_1367" s="T77">da</ta>
            <ta e="T79" id="Seg_1368" s="T78">bert</ta>
            <ta e="T80" id="Seg_1369" s="T79">bu͡ol-TAK-tA</ta>
            <ta e="T81" id="Seg_1370" s="T80">di͡e-BIT</ta>
            <ta e="T82" id="Seg_1371" s="T81">kün</ta>
            <ta e="T83" id="Seg_1372" s="T82">u͡ot-tA</ta>
            <ta e="T84" id="Seg_1373" s="T83">bert-GIn</ta>
            <ta e="T85" id="Seg_1374" s="T84">du͡o</ta>
            <ta e="T86" id="Seg_1375" s="T85">bert-BIn</ta>
            <ta e="T87" id="Seg_1376" s="T86">da</ta>
            <ta e="T88" id="Seg_1377" s="T87">bert-BIn</ta>
            <ta e="T89" id="Seg_1378" s="T88">togo-keː</ta>
            <ta e="T90" id="Seg_1379" s="T89">taːs</ta>
            <ta e="T91" id="Seg_1380" s="T90">kaja-GA</ta>
            <ta e="T92" id="Seg_1381" s="T91">kakka-LAN-AːččI-GIn=Ij</ta>
            <ta e="T93" id="Seg_1382" s="T92">taːs</ta>
            <ta e="T94" id="Seg_1383" s="T93">kaja</ta>
            <ta e="T95" id="Seg_1384" s="T94">bert</ta>
            <ta e="T96" id="Seg_1385" s="T95">da</ta>
            <ta e="T97" id="Seg_1386" s="T96">bert</ta>
            <ta e="T98" id="Seg_1387" s="T97">bu͡ol-TAK-tA</ta>
            <ta e="T99" id="Seg_1388" s="T98">di͡e-BIT</ta>
            <ta e="T100" id="Seg_1389" s="T99">taːs</ta>
            <ta e="T101" id="Seg_1390" s="T100">kaja</ta>
            <ta e="T102" id="Seg_1391" s="T101">bert-GIn</ta>
            <ta e="T103" id="Seg_1392" s="T102">du͡o</ta>
            <ta e="T104" id="Seg_1393" s="T103">bert-BIn</ta>
            <ta e="T105" id="Seg_1394" s="T104">da</ta>
            <ta e="T106" id="Seg_1395" s="T105">bert-BIn</ta>
            <ta e="T107" id="Seg_1396" s="T106">di͡e-BIT</ta>
            <ta e="T108" id="Seg_1397" s="T107">togo-keː</ta>
            <ta e="T109" id="Seg_1398" s="T108">üːteːn</ta>
            <ta e="T110" id="Seg_1399" s="T109">kutujak</ta>
            <ta e="T111" id="Seg_1400" s="T110">dʼöl-A</ta>
            <ta e="T112" id="Seg_1401" s="T111">hüːr-Ar=Ij</ta>
            <ta e="T113" id="Seg_1402" s="T112">üːteːn</ta>
            <ta e="T114" id="Seg_1403" s="T113">kutujak</ta>
            <ta e="T115" id="Seg_1404" s="T114">bert</ta>
            <ta e="T116" id="Seg_1405" s="T115">da</ta>
            <ta e="T117" id="Seg_1406" s="T116">bert</ta>
            <ta e="T118" id="Seg_1407" s="T117">bu͡ol-TAK-tA</ta>
            <ta e="T119" id="Seg_1408" s="T118">di͡e-BIT</ta>
            <ta e="T120" id="Seg_1409" s="T119">üːteːn</ta>
            <ta e="T121" id="Seg_1410" s="T120">kutujak</ta>
            <ta e="T122" id="Seg_1411" s="T121">bert-GIn</ta>
            <ta e="T123" id="Seg_1412" s="T122">du͡o</ta>
            <ta e="T124" id="Seg_1413" s="T123">bert-BIn</ta>
            <ta e="T125" id="Seg_1414" s="T124">da</ta>
            <ta e="T126" id="Seg_1415" s="T125">bert-BIn</ta>
            <ta e="T127" id="Seg_1416" s="T126">togo-keː</ta>
            <ta e="T128" id="Seg_1417" s="T127">haːmaj</ta>
            <ta e="T129" id="Seg_1418" s="T128">ogo-LArI-GAr</ta>
            <ta e="T130" id="Seg_1419" s="T129">ölör-TAr-AːččI-GIn=Ij</ta>
            <ta e="T131" id="Seg_1420" s="T130">haːmaj</ta>
            <ta e="T132" id="Seg_1421" s="T131">ogo-LArA</ta>
            <ta e="T133" id="Seg_1422" s="T132">bert</ta>
            <ta e="T134" id="Seg_1423" s="T133">da</ta>
            <ta e="T135" id="Seg_1424" s="T134">bert</ta>
            <ta e="T136" id="Seg_1425" s="T135">bu͡ol-TAK-LArA</ta>
            <ta e="T137" id="Seg_1426" s="T136">haːmaj</ta>
            <ta e="T138" id="Seg_1427" s="T137">ogo-LArA</ta>
            <ta e="T139" id="Seg_1428" s="T138">bert-GIt</ta>
            <ta e="T140" id="Seg_1429" s="T139">du͡o</ta>
            <ta e="T141" id="Seg_1430" s="T140">bert-BIt</ta>
            <ta e="T142" id="Seg_1431" s="T141">da</ta>
            <ta e="T143" id="Seg_1432" s="T142">bert-BIt</ta>
            <ta e="T144" id="Seg_1433" s="T143">di͡e-BIT-LAr</ta>
            <ta e="T145" id="Seg_1434" s="T144">togo-keː</ta>
            <ta e="T146" id="Seg_1435" s="T145">himi͡ert-GItI-GAr</ta>
            <ta e="T147" id="Seg_1436" s="T146">kot-TAr-AːččI-GIt=Ij</ta>
            <ta e="T148" id="Seg_1437" s="T147">himi͡ert</ta>
            <ta e="T149" id="Seg_1438" s="T148">bert</ta>
            <ta e="T150" id="Seg_1439" s="T149">da</ta>
            <ta e="T151" id="Seg_1440" s="T150">bert</ta>
            <ta e="T152" id="Seg_1441" s="T151">bu͡ol-TAK-tA</ta>
            <ta e="T153" id="Seg_1442" s="T152">di͡e-BIT-LAr</ta>
            <ta e="T154" id="Seg_1443" s="T153">himi͡ert</ta>
            <ta e="T155" id="Seg_1444" s="T154">bert-GIn</ta>
            <ta e="T156" id="Seg_1445" s="T155">du͡o</ta>
            <ta e="T157" id="Seg_1446" s="T156">bert-BIn</ta>
            <ta e="T158" id="Seg_1447" s="T157">da</ta>
            <ta e="T159" id="Seg_1448" s="T158">bert-BIn</ta>
            <ta e="T160" id="Seg_1449" s="T159">di͡e-BIT</ta>
            <ta e="T161" id="Seg_1450" s="T160">innʼe</ta>
            <ta e="T162" id="Seg_1451" s="T161">di͡e-An</ta>
            <ta e="T163" id="Seg_1452" s="T162">baran</ta>
            <ta e="T164" id="Seg_1453" s="T163">Taːl</ta>
            <ta e="T165" id="Seg_1454" s="T164">emeːksin</ta>
            <ta e="T166" id="Seg_1455" s="T165">öl-An</ta>
            <ta e="T167" id="Seg_1456" s="T166">kaːl-BIT</ta>
            <ta e="T168" id="Seg_1457" s="T167">ele-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1458" s="T0">Taal.[NOM]</ta>
            <ta e="T2" id="Seg_1459" s="T1">old.woman.[NOM]</ta>
            <ta e="T3" id="Seg_1460" s="T2">live-PTCP.PST</ta>
            <ta e="T4" id="Seg_1461" s="T3">be-PST2.[3SG]</ta>
            <ta e="T5" id="Seg_1462" s="T4">once</ta>
            <ta e="T6" id="Seg_1463" s="T5">MOD</ta>
            <ta e="T7" id="Seg_1464" s="T6">this</ta>
            <ta e="T8" id="Seg_1465" s="T7">Taal.[NOM]</ta>
            <ta e="T9" id="Seg_1466" s="T8">old.woman.[NOM]</ta>
            <ta e="T10" id="Seg_1467" s="T9">bedding-3SG-DAT/LOC</ta>
            <ta e="T11" id="Seg_1468" s="T10">pee-PST2.[3SG]</ta>
            <ta e="T12" id="Seg_1469" s="T11">that</ta>
            <ta e="T13" id="Seg_1470" s="T12">day.[NOM]</ta>
            <ta e="T14" id="Seg_1471" s="T13">MOD</ta>
            <ta e="T15" id="Seg_1472" s="T14">sun-PROPR</ta>
            <ta e="T16" id="Seg_1473" s="T15">day.[NOM]</ta>
            <ta e="T17" id="Seg_1474" s="T16">be-PST2.[3SG]</ta>
            <ta e="T18" id="Seg_1475" s="T17">Taal.[NOM]</ta>
            <ta e="T19" id="Seg_1476" s="T18">old.woman.[NOM]</ta>
            <ta e="T20" id="Seg_1477" s="T19">feather_bed-3SG-ACC</ta>
            <ta e="T21" id="Seg_1478" s="T20">become.dry-CAUS-CVB.PURP</ta>
            <ta e="T22" id="Seg_1479" s="T21">storage-DAT/LOC</ta>
            <ta e="T23" id="Seg_1480" s="T22">lay-PST2.[3SG]</ta>
            <ta e="T24" id="Seg_1481" s="T23">there</ta>
            <ta e="T25" id="Seg_1482" s="T24">that</ta>
            <ta e="T26" id="Seg_1483" s="T25">go.in-PTCP.PST-3SG-ACC</ta>
            <ta e="T27" id="Seg_1484" s="T26">after</ta>
            <ta e="T28" id="Seg_1485" s="T27">wind.[NOM]</ta>
            <ta e="T29" id="Seg_1486" s="T28">raise-EP-PASS/REFL-EP-PST2.[3SG]</ta>
            <ta e="T30" id="Seg_1487" s="T29">that.[NOM]</ta>
            <ta e="T31" id="Seg_1488" s="T30">raise-EP-PASS/REFL-CVB.SEQ</ta>
            <ta e="T32" id="Seg_1489" s="T31">Taal.[NOM]</ta>
            <ta e="T33" id="Seg_1490" s="T32">old.woman.[NOM]</ta>
            <ta e="T34" id="Seg_1491" s="T33">feather_bed-3SG-ACC</ta>
            <ta e="T35" id="Seg_1492" s="T34">fly-EP-CAUS-CVB.SEQ</ta>
            <ta e="T36" id="Seg_1493" s="T35">carry-PST2.[3SG]</ta>
            <ta e="T37" id="Seg_1494" s="T36">Taal.[NOM]</ta>
            <ta e="T38" id="Seg_1495" s="T37">old.woman.[NOM]</ta>
            <ta e="T39" id="Seg_1496" s="T38">go.out-CVB.SIM</ta>
            <ta e="T40" id="Seg_1497" s="T39">run-PST2.[3SG]</ta>
            <ta e="T41" id="Seg_1498" s="T40">feather_bed-3SG.[NOM]</ta>
            <ta e="T42" id="Seg_1499" s="T41">fly-CVB.SIM</ta>
            <ta e="T43" id="Seg_1500" s="T42">stand-PTCP.PRS</ta>
            <ta e="T44" id="Seg_1501" s="T43">be-PST2.[3SG]</ta>
            <ta e="T45" id="Seg_1502" s="T44">Taal.[NOM]</ta>
            <ta e="T46" id="Seg_1503" s="T45">old.woman.[NOM]</ta>
            <ta e="T47" id="Seg_1504" s="T46">feather_bed-3SG-ACC</ta>
            <ta e="T48" id="Seg_1505" s="T47">follow-CVB.SEQ</ta>
            <ta e="T49" id="Seg_1506" s="T48">go-PST2.[3SG]</ta>
            <ta e="T50" id="Seg_1507" s="T49">go-CVB.SEQ</ta>
            <ta e="T51" id="Seg_1508" s="T50">go-CVB.SEQ</ta>
            <ta e="T52" id="Seg_1509" s="T51">ice-DAT/LOC</ta>
            <ta e="T53" id="Seg_1510" s="T52">come-PST2.[3SG]</ta>
            <ta e="T54" id="Seg_1511" s="T53">slip-CVB.SEQ</ta>
            <ta e="T55" id="Seg_1512" s="T54">fall-PST2.[3SG]</ta>
            <ta e="T56" id="Seg_1513" s="T55">that</ta>
            <ta e="T57" id="Seg_1514" s="T56">slip-CVB.SEQ</ta>
            <ta e="T58" id="Seg_1515" s="T57">leg-3SG-ACC</ta>
            <ta e="T59" id="Seg_1516" s="T58">injure-EP-REFL-PST2.[3SG]</ta>
            <ta e="T60" id="Seg_1517" s="T59">that</ta>
            <ta e="T61" id="Seg_1518" s="T60">lie-CVB.SEQ</ta>
            <ta e="T62" id="Seg_1519" s="T61">speak-PST2.[3SG]</ta>
            <ta e="T63" id="Seg_1520" s="T62">ice.[NOM]</ta>
            <ta e="T64" id="Seg_1521" s="T63">ice.[NOM]</ta>
            <ta e="T65" id="Seg_1522" s="T64">powerful-2SG</ta>
            <ta e="T66" id="Seg_1523" s="T65">Q</ta>
            <ta e="T67" id="Seg_1524" s="T66">powerful-1SG</ta>
            <ta e="T68" id="Seg_1525" s="T67">and</ta>
            <ta e="T69" id="Seg_1526" s="T68">power-1SG</ta>
            <ta e="T70" id="Seg_1527" s="T69">say-PST2.[3SG]</ta>
            <ta e="T71" id="Seg_1528" s="T70">why-EMPH</ta>
            <ta e="T72" id="Seg_1529" s="T71">sun.[NOM]</ta>
            <ta e="T73" id="Seg_1530" s="T72">light-3SG-DAT/LOC</ta>
            <ta e="T74" id="Seg_1531" s="T73">thaw-PRS-2SG</ta>
            <ta e="T75" id="Seg_1532" s="T74">sun.[NOM]</ta>
            <ta e="T76" id="Seg_1533" s="T75">light-3SG.[NOM]</ta>
            <ta e="T77" id="Seg_1534" s="T76">powerful.[NOM]</ta>
            <ta e="T78" id="Seg_1535" s="T77">and</ta>
            <ta e="T79" id="Seg_1536" s="T78">powerful.[NOM]</ta>
            <ta e="T80" id="Seg_1537" s="T79">be-INFER-3SG</ta>
            <ta e="T81" id="Seg_1538" s="T80">say-PST2.[3SG]</ta>
            <ta e="T82" id="Seg_1539" s="T81">sun.[NOM]</ta>
            <ta e="T83" id="Seg_1540" s="T82">light-3SG.[NOM]</ta>
            <ta e="T84" id="Seg_1541" s="T83">powerful-2SG</ta>
            <ta e="T85" id="Seg_1542" s="T84">Q</ta>
            <ta e="T86" id="Seg_1543" s="T85">powerful-1SG</ta>
            <ta e="T87" id="Seg_1544" s="T86">and</ta>
            <ta e="T88" id="Seg_1545" s="T87">power-1SG</ta>
            <ta e="T89" id="Seg_1546" s="T88">why-EMPH</ta>
            <ta e="T90" id="Seg_1547" s="T89">stone.[NOM]</ta>
            <ta e="T91" id="Seg_1548" s="T90">rock-DAT/LOC</ta>
            <ta e="T92" id="Seg_1549" s="T91">covering-VBZ-HAB-2SG=Q</ta>
            <ta e="T93" id="Seg_1550" s="T92">stone.[NOM]</ta>
            <ta e="T94" id="Seg_1551" s="T93">mountain.[NOM]</ta>
            <ta e="T95" id="Seg_1552" s="T94">powerful.[NOM]</ta>
            <ta e="T96" id="Seg_1553" s="T95">and</ta>
            <ta e="T97" id="Seg_1554" s="T96">powerful.[NOM]</ta>
            <ta e="T98" id="Seg_1555" s="T97">be-INFER-3SG</ta>
            <ta e="T99" id="Seg_1556" s="T98">say-PST2.[3SG]</ta>
            <ta e="T100" id="Seg_1557" s="T99">stone.[NOM]</ta>
            <ta e="T101" id="Seg_1558" s="T100">mountain.[NOM]</ta>
            <ta e="T102" id="Seg_1559" s="T101">powerful-2SG</ta>
            <ta e="T103" id="Seg_1560" s="T102">Q</ta>
            <ta e="T104" id="Seg_1561" s="T103">powerful-1SG</ta>
            <ta e="T105" id="Seg_1562" s="T104">and</ta>
            <ta e="T106" id="Seg_1563" s="T105">powerful-1SG</ta>
            <ta e="T107" id="Seg_1564" s="T106">say-PST2.[3SG]</ta>
            <ta e="T108" id="Seg_1565" s="T107">why-EMPH</ta>
            <ta e="T109" id="Seg_1566" s="T108">narrowheaded</ta>
            <ta e="T110" id="Seg_1567" s="T109">mouse.[NOM]</ta>
            <ta e="T111" id="Seg_1568" s="T110">bore.through-CVB.SIM</ta>
            <ta e="T112" id="Seg_1569" s="T111">run-PRS.[3SG]=Q</ta>
            <ta e="T113" id="Seg_1570" s="T112">narrowheaded</ta>
            <ta e="T114" id="Seg_1571" s="T113">mouse.[NOM]</ta>
            <ta e="T115" id="Seg_1572" s="T114">powerful.[NOM]</ta>
            <ta e="T116" id="Seg_1573" s="T115">and</ta>
            <ta e="T117" id="Seg_1574" s="T116">powerful.[NOM]</ta>
            <ta e="T118" id="Seg_1575" s="T117">be-INFER-3SG</ta>
            <ta e="T119" id="Seg_1576" s="T118">say-PST2.[3SG]</ta>
            <ta e="T120" id="Seg_1577" s="T119">narrowheaded</ta>
            <ta e="T121" id="Seg_1578" s="T120">mouse.[NOM]</ta>
            <ta e="T122" id="Seg_1579" s="T121">powerful-2SG</ta>
            <ta e="T123" id="Seg_1580" s="T122">Q</ta>
            <ta e="T124" id="Seg_1581" s="T123">powerful-1SG</ta>
            <ta e="T125" id="Seg_1582" s="T124">and</ta>
            <ta e="T126" id="Seg_1583" s="T125">power-1SG</ta>
            <ta e="T127" id="Seg_1584" s="T126">why-EMPH</ta>
            <ta e="T128" id="Seg_1585" s="T127">Nganasan</ta>
            <ta e="T129" id="Seg_1586" s="T128">child-3PL-DAT/LOC</ta>
            <ta e="T130" id="Seg_1587" s="T129">kill-PASS-HAB-2SG=Q</ta>
            <ta e="T131" id="Seg_1588" s="T130">Nganasan</ta>
            <ta e="T132" id="Seg_1589" s="T131">child-3PL.[NOM]</ta>
            <ta e="T133" id="Seg_1590" s="T132">powerful.[NOM]</ta>
            <ta e="T134" id="Seg_1591" s="T133">and</ta>
            <ta e="T135" id="Seg_1592" s="T134">powerful.[NOM]</ta>
            <ta e="T136" id="Seg_1593" s="T135">be-INFER-3PL</ta>
            <ta e="T137" id="Seg_1594" s="T136">Nganasan</ta>
            <ta e="T138" id="Seg_1595" s="T137">child-3PL.[NOM]</ta>
            <ta e="T139" id="Seg_1596" s="T138">powerful-2PL</ta>
            <ta e="T140" id="Seg_1597" s="T139">Q</ta>
            <ta e="T141" id="Seg_1598" s="T140">powerful-1PL</ta>
            <ta e="T142" id="Seg_1599" s="T141">and</ta>
            <ta e="T143" id="Seg_1600" s="T142">powerful-1PL</ta>
            <ta e="T144" id="Seg_1601" s="T143">say-PST2-3PL</ta>
            <ta e="T145" id="Seg_1602" s="T144">why-EMPH</ta>
            <ta e="T146" id="Seg_1603" s="T145">death-2PL-DAT/LOC</ta>
            <ta e="T147" id="Seg_1604" s="T146">make.it-PASS-HAB-2PL=Q</ta>
            <ta e="T148" id="Seg_1605" s="T147">death.[NOM]</ta>
            <ta e="T149" id="Seg_1606" s="T148">powerful.[NOM]</ta>
            <ta e="T150" id="Seg_1607" s="T149">and</ta>
            <ta e="T151" id="Seg_1608" s="T150">powerful.[NOM]</ta>
            <ta e="T152" id="Seg_1609" s="T151">be-INFER-3SG</ta>
            <ta e="T153" id="Seg_1610" s="T152">say-PST2-3PL</ta>
            <ta e="T154" id="Seg_1611" s="T153">death.[NOM]</ta>
            <ta e="T155" id="Seg_1612" s="T154">powerful-2SG</ta>
            <ta e="T156" id="Seg_1613" s="T155">Q</ta>
            <ta e="T157" id="Seg_1614" s="T156">powerful-1SG</ta>
            <ta e="T158" id="Seg_1615" s="T157">and</ta>
            <ta e="T159" id="Seg_1616" s="T158">powerful-1SG</ta>
            <ta e="T160" id="Seg_1617" s="T159">say-PST2.[3SG]</ta>
            <ta e="T161" id="Seg_1618" s="T160">so</ta>
            <ta e="T162" id="Seg_1619" s="T161">say-CVB.SEQ</ta>
            <ta e="T163" id="Seg_1620" s="T162">after</ta>
            <ta e="T164" id="Seg_1621" s="T163">Taal.[NOM]</ta>
            <ta e="T165" id="Seg_1622" s="T164">old.woman.[NOM]</ta>
            <ta e="T166" id="Seg_1623" s="T165">die-CVB.SEQ</ta>
            <ta e="T167" id="Seg_1624" s="T166">stay-PST2.[3SG]</ta>
            <ta e="T168" id="Seg_1625" s="T167">last-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_1626" s="T0">Taal.[NOM]</ta>
            <ta e="T2" id="Seg_1627" s="T1">Alte.[NOM]</ta>
            <ta e="T3" id="Seg_1628" s="T2">leben-PTCP.PST</ta>
            <ta e="T4" id="Seg_1629" s="T3">sein-PST2.[3SG]</ta>
            <ta e="T5" id="Seg_1630" s="T4">einmal</ta>
            <ta e="T6" id="Seg_1631" s="T5">MOD</ta>
            <ta e="T7" id="Seg_1632" s="T6">dieses</ta>
            <ta e="T8" id="Seg_1633" s="T7">Taal.[NOM]</ta>
            <ta e="T9" id="Seg_1634" s="T8">Alte.[NOM]</ta>
            <ta e="T10" id="Seg_1635" s="T9">Bettwäsche-3SG-DAT/LOC</ta>
            <ta e="T11" id="Seg_1636" s="T10">pinkeln-PST2.[3SG]</ta>
            <ta e="T12" id="Seg_1637" s="T11">jenes</ta>
            <ta e="T13" id="Seg_1638" s="T12">Tag.[NOM]</ta>
            <ta e="T14" id="Seg_1639" s="T13">MOD</ta>
            <ta e="T15" id="Seg_1640" s="T14">Sonne-PROPR</ta>
            <ta e="T16" id="Seg_1641" s="T15">Tag.[NOM]</ta>
            <ta e="T17" id="Seg_1642" s="T16">sein-PST2.[3SG]</ta>
            <ta e="T18" id="Seg_1643" s="T17">Taal.[NOM]</ta>
            <ta e="T19" id="Seg_1644" s="T18">Alte.[NOM]</ta>
            <ta e="T20" id="Seg_1645" s="T19">Federbett-3SG-ACC</ta>
            <ta e="T21" id="Seg_1646" s="T20">trocken.werden-CAUS-CVB.PURP</ta>
            <ta e="T22" id="Seg_1647" s="T21">Speicher-DAT/LOC</ta>
            <ta e="T23" id="Seg_1648" s="T22">legen-PST2.[3SG]</ta>
            <ta e="T24" id="Seg_1649" s="T23">dort</ta>
            <ta e="T25" id="Seg_1650" s="T24">jenes</ta>
            <ta e="T26" id="Seg_1651" s="T25">hineingehen-PTCP.PST-3SG-ACC</ta>
            <ta e="T27" id="Seg_1652" s="T26">nachdem</ta>
            <ta e="T28" id="Seg_1653" s="T27">Wind.[NOM]</ta>
            <ta e="T29" id="Seg_1654" s="T28">heben-EP-PASS/REFL-EP-PST2.[3SG]</ta>
            <ta e="T30" id="Seg_1655" s="T29">jenes.[NOM]</ta>
            <ta e="T31" id="Seg_1656" s="T30">heben-EP-PASS/REFL-CVB.SEQ</ta>
            <ta e="T32" id="Seg_1657" s="T31">Taal.[NOM]</ta>
            <ta e="T33" id="Seg_1658" s="T32">Alte.[NOM]</ta>
            <ta e="T34" id="Seg_1659" s="T33">Federbett-3SG-ACC</ta>
            <ta e="T35" id="Seg_1660" s="T34">fliegen-EP-CAUS-CVB.SEQ</ta>
            <ta e="T36" id="Seg_1661" s="T35">tragen-PST2.[3SG]</ta>
            <ta e="T37" id="Seg_1662" s="T36">Taal.[NOM]</ta>
            <ta e="T38" id="Seg_1663" s="T37">Alte.[NOM]</ta>
            <ta e="T39" id="Seg_1664" s="T38">hinausgehen-CVB.SIM</ta>
            <ta e="T40" id="Seg_1665" s="T39">rennen-PST2.[3SG]</ta>
            <ta e="T41" id="Seg_1666" s="T40">Federbett-3SG.[NOM]</ta>
            <ta e="T42" id="Seg_1667" s="T41">fliegen-CVB.SIM</ta>
            <ta e="T43" id="Seg_1668" s="T42">stehen-PTCP.PRS</ta>
            <ta e="T44" id="Seg_1669" s="T43">sein-PST2.[3SG]</ta>
            <ta e="T45" id="Seg_1670" s="T44">Taal.[NOM]</ta>
            <ta e="T46" id="Seg_1671" s="T45">Alte.[NOM]</ta>
            <ta e="T47" id="Seg_1672" s="T46">Federbett-3SG-ACC</ta>
            <ta e="T48" id="Seg_1673" s="T47">folgen-CVB.SEQ</ta>
            <ta e="T49" id="Seg_1674" s="T48">gehen-PST2.[3SG]</ta>
            <ta e="T50" id="Seg_1675" s="T49">gehen-CVB.SEQ</ta>
            <ta e="T51" id="Seg_1676" s="T50">gehen-CVB.SEQ</ta>
            <ta e="T52" id="Seg_1677" s="T51">Eis-DAT/LOC</ta>
            <ta e="T53" id="Seg_1678" s="T52">kommen-PST2.[3SG]</ta>
            <ta e="T54" id="Seg_1679" s="T53">ausrutschen-CVB.SEQ</ta>
            <ta e="T55" id="Seg_1680" s="T54">fallen-PST2.[3SG]</ta>
            <ta e="T56" id="Seg_1681" s="T55">jenes</ta>
            <ta e="T57" id="Seg_1682" s="T56">ausrutschen-CVB.SEQ</ta>
            <ta e="T58" id="Seg_1683" s="T57">Bein-3SG-ACC</ta>
            <ta e="T59" id="Seg_1684" s="T58">verletzen-EP-REFL-PST2.[3SG]</ta>
            <ta e="T60" id="Seg_1685" s="T59">jenes</ta>
            <ta e="T61" id="Seg_1686" s="T60">liegen-CVB.SEQ</ta>
            <ta e="T62" id="Seg_1687" s="T61">sprechen-PST2.[3SG]</ta>
            <ta e="T63" id="Seg_1688" s="T62">Eis.[NOM]</ta>
            <ta e="T64" id="Seg_1689" s="T63">Eis.[NOM]</ta>
            <ta e="T65" id="Seg_1690" s="T64">kräftig-2SG</ta>
            <ta e="T66" id="Seg_1691" s="T65">Q</ta>
            <ta e="T67" id="Seg_1692" s="T66">kräftig-1SG</ta>
            <ta e="T68" id="Seg_1693" s="T67">und</ta>
            <ta e="T69" id="Seg_1694" s="T68">Kraft-1SG</ta>
            <ta e="T70" id="Seg_1695" s="T69">sagen-PST2.[3SG]</ta>
            <ta e="T71" id="Seg_1696" s="T70">warum-EMPH</ta>
            <ta e="T72" id="Seg_1697" s="T71">Sonne.[NOM]</ta>
            <ta e="T73" id="Seg_1698" s="T72">Licht-3SG-DAT/LOC</ta>
            <ta e="T74" id="Seg_1699" s="T73">tauen-PRS-2SG</ta>
            <ta e="T75" id="Seg_1700" s="T74">Sonne.[NOM]</ta>
            <ta e="T76" id="Seg_1701" s="T75">Licht-3SG.[NOM]</ta>
            <ta e="T77" id="Seg_1702" s="T76">kräftig.[NOM]</ta>
            <ta e="T78" id="Seg_1703" s="T77">und</ta>
            <ta e="T79" id="Seg_1704" s="T78">kräftig.[NOM]</ta>
            <ta e="T80" id="Seg_1705" s="T79">sein-INFER-3SG</ta>
            <ta e="T81" id="Seg_1706" s="T80">sagen-PST2.[3SG]</ta>
            <ta e="T82" id="Seg_1707" s="T81">Sonne.[NOM]</ta>
            <ta e="T83" id="Seg_1708" s="T82">Licht-3SG.[NOM]</ta>
            <ta e="T84" id="Seg_1709" s="T83">kräftig-2SG</ta>
            <ta e="T85" id="Seg_1710" s="T84">Q</ta>
            <ta e="T86" id="Seg_1711" s="T85">kräftig-1SG</ta>
            <ta e="T87" id="Seg_1712" s="T86">und</ta>
            <ta e="T88" id="Seg_1713" s="T87">Kraft-1SG</ta>
            <ta e="T89" id="Seg_1714" s="T88">warum-EMPH</ta>
            <ta e="T90" id="Seg_1715" s="T89">Stein.[NOM]</ta>
            <ta e="T91" id="Seg_1716" s="T90">Felsen-DAT/LOC</ta>
            <ta e="T92" id="Seg_1717" s="T91">Bedeckung-VBZ-HAB-2SG=Q</ta>
            <ta e="T93" id="Seg_1718" s="T92">Stein.[NOM]</ta>
            <ta e="T94" id="Seg_1719" s="T93">Berg.[NOM]</ta>
            <ta e="T95" id="Seg_1720" s="T94">kräftig.[NOM]</ta>
            <ta e="T96" id="Seg_1721" s="T95">und</ta>
            <ta e="T97" id="Seg_1722" s="T96">kräftig.[NOM]</ta>
            <ta e="T98" id="Seg_1723" s="T97">sein-INFER-3SG</ta>
            <ta e="T99" id="Seg_1724" s="T98">sagen-PST2.[3SG]</ta>
            <ta e="T100" id="Seg_1725" s="T99">Stein.[NOM]</ta>
            <ta e="T101" id="Seg_1726" s="T100">Berg.[NOM]</ta>
            <ta e="T102" id="Seg_1727" s="T101">kräftig-2SG</ta>
            <ta e="T103" id="Seg_1728" s="T102">Q</ta>
            <ta e="T104" id="Seg_1729" s="T103">kräftig-1SG</ta>
            <ta e="T105" id="Seg_1730" s="T104">und</ta>
            <ta e="T106" id="Seg_1731" s="T105">kräftig-1SG</ta>
            <ta e="T107" id="Seg_1732" s="T106">sagen-PST2.[3SG]</ta>
            <ta e="T108" id="Seg_1733" s="T107">warum-EMPH</ta>
            <ta e="T109" id="Seg_1734" s="T108">spitzmäulig</ta>
            <ta e="T110" id="Seg_1735" s="T109">Maus.[NOM]</ta>
            <ta e="T111" id="Seg_1736" s="T110">durchbohren-CVB.SIM</ta>
            <ta e="T112" id="Seg_1737" s="T111">laufen-PRS.[3SG]=Q</ta>
            <ta e="T113" id="Seg_1738" s="T112">spitzmäulig</ta>
            <ta e="T114" id="Seg_1739" s="T113">Maus.[NOM]</ta>
            <ta e="T115" id="Seg_1740" s="T114">kräftig.[NOM]</ta>
            <ta e="T116" id="Seg_1741" s="T115">und</ta>
            <ta e="T117" id="Seg_1742" s="T116">kräftig.[NOM]</ta>
            <ta e="T118" id="Seg_1743" s="T117">sein-INFER-3SG</ta>
            <ta e="T119" id="Seg_1744" s="T118">sagen-PST2.[3SG]</ta>
            <ta e="T120" id="Seg_1745" s="T119">spitzmäulig</ta>
            <ta e="T121" id="Seg_1746" s="T120">Maus.[NOM]</ta>
            <ta e="T122" id="Seg_1747" s="T121">kräftig-2SG</ta>
            <ta e="T123" id="Seg_1748" s="T122">Q</ta>
            <ta e="T124" id="Seg_1749" s="T123">kräftig-1SG</ta>
            <ta e="T125" id="Seg_1750" s="T124">und</ta>
            <ta e="T126" id="Seg_1751" s="T125">Kraft-1SG</ta>
            <ta e="T127" id="Seg_1752" s="T126">warum-EMPH</ta>
            <ta e="T128" id="Seg_1753" s="T127">nganasanisch</ta>
            <ta e="T129" id="Seg_1754" s="T128">Kind-3PL-DAT/LOC</ta>
            <ta e="T130" id="Seg_1755" s="T129">töten-PASS-HAB-2SG=Q</ta>
            <ta e="T131" id="Seg_1756" s="T130">nganasanisch</ta>
            <ta e="T132" id="Seg_1757" s="T131">Kind-3PL.[NOM]</ta>
            <ta e="T133" id="Seg_1758" s="T132">kräftig.[NOM]</ta>
            <ta e="T134" id="Seg_1759" s="T133">und</ta>
            <ta e="T135" id="Seg_1760" s="T134">kräftig.[NOM]</ta>
            <ta e="T136" id="Seg_1761" s="T135">sein-INFER-3PL</ta>
            <ta e="T137" id="Seg_1762" s="T136">nganasanisch</ta>
            <ta e="T138" id="Seg_1763" s="T137">Kind-3PL.[NOM]</ta>
            <ta e="T139" id="Seg_1764" s="T138">kräftig-2PL</ta>
            <ta e="T140" id="Seg_1765" s="T139">Q</ta>
            <ta e="T141" id="Seg_1766" s="T140">kräftig-1PL</ta>
            <ta e="T142" id="Seg_1767" s="T141">und</ta>
            <ta e="T143" id="Seg_1768" s="T142">kräftig-1PL</ta>
            <ta e="T144" id="Seg_1769" s="T143">sagen-PST2-3PL</ta>
            <ta e="T145" id="Seg_1770" s="T144">warum-EMPH</ta>
            <ta e="T146" id="Seg_1771" s="T145">Tod-2PL-DAT/LOC</ta>
            <ta e="T147" id="Seg_1772" s="T146">schaffen-PASS-HAB-2PL=Q</ta>
            <ta e="T148" id="Seg_1773" s="T147">Tod.[NOM]</ta>
            <ta e="T149" id="Seg_1774" s="T148">kräftig.[NOM]</ta>
            <ta e="T150" id="Seg_1775" s="T149">und</ta>
            <ta e="T151" id="Seg_1776" s="T150">kräftig.[NOM]</ta>
            <ta e="T152" id="Seg_1777" s="T151">sein-INFER-3SG</ta>
            <ta e="T153" id="Seg_1778" s="T152">sagen-PST2-3PL</ta>
            <ta e="T154" id="Seg_1779" s="T153">Tod.[NOM]</ta>
            <ta e="T155" id="Seg_1780" s="T154">kräftig-2SG</ta>
            <ta e="T156" id="Seg_1781" s="T155">Q</ta>
            <ta e="T157" id="Seg_1782" s="T156">kräftig-1SG</ta>
            <ta e="T158" id="Seg_1783" s="T157">und</ta>
            <ta e="T159" id="Seg_1784" s="T158">kräftig-1SG</ta>
            <ta e="T160" id="Seg_1785" s="T159">sagen-PST2.[3SG]</ta>
            <ta e="T161" id="Seg_1786" s="T160">so</ta>
            <ta e="T162" id="Seg_1787" s="T161">sagen-CVB.SEQ</ta>
            <ta e="T163" id="Seg_1788" s="T162">nachdem</ta>
            <ta e="T164" id="Seg_1789" s="T163">Taal.[NOM]</ta>
            <ta e="T165" id="Seg_1790" s="T164">Alte.[NOM]</ta>
            <ta e="T166" id="Seg_1791" s="T165">sterben-CVB.SEQ</ta>
            <ta e="T167" id="Seg_1792" s="T166">bleiben-PST2.[3SG]</ta>
            <ta e="T168" id="Seg_1793" s="T167">letzter-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1794" s="T0">Таал.[NOM]</ta>
            <ta e="T2" id="Seg_1795" s="T1">старуха.[NOM]</ta>
            <ta e="T3" id="Seg_1796" s="T2">жить-PTCP.PST</ta>
            <ta e="T4" id="Seg_1797" s="T3">быть-PST2.[3SG]</ta>
            <ta e="T5" id="Seg_1798" s="T4">однажды</ta>
            <ta e="T6" id="Seg_1799" s="T5">MOD</ta>
            <ta e="T7" id="Seg_1800" s="T6">этот</ta>
            <ta e="T8" id="Seg_1801" s="T7">Таал.[NOM]</ta>
            <ta e="T9" id="Seg_1802" s="T8">старуха.[NOM]</ta>
            <ta e="T10" id="Seg_1803" s="T9">постельное.белье-3SG-DAT/LOC</ta>
            <ta e="T11" id="Seg_1804" s="T10">мочиться-PST2.[3SG]</ta>
            <ta e="T12" id="Seg_1805" s="T11">тот</ta>
            <ta e="T13" id="Seg_1806" s="T12">день.[NOM]</ta>
            <ta e="T14" id="Seg_1807" s="T13">MOD</ta>
            <ta e="T15" id="Seg_1808" s="T14">солнце-PROPR</ta>
            <ta e="T16" id="Seg_1809" s="T15">день.[NOM]</ta>
            <ta e="T17" id="Seg_1810" s="T16">быть-PST2.[3SG]</ta>
            <ta e="T18" id="Seg_1811" s="T17">Таал.[NOM]</ta>
            <ta e="T19" id="Seg_1812" s="T18">старуха.[NOM]</ta>
            <ta e="T20" id="Seg_1813" s="T19">перина-3SG-ACC</ta>
            <ta e="T21" id="Seg_1814" s="T20">высыхать-CAUS-CVB.PURP</ta>
            <ta e="T22" id="Seg_1815" s="T21">лабаз-DAT/LOC</ta>
            <ta e="T23" id="Seg_1816" s="T22">класть-PST2.[3SG]</ta>
            <ta e="T24" id="Seg_1817" s="T23">там</ta>
            <ta e="T25" id="Seg_1818" s="T24">тот</ta>
            <ta e="T26" id="Seg_1819" s="T25">входить-PTCP.PST-3SG-ACC</ta>
            <ta e="T27" id="Seg_1820" s="T26">после.того</ta>
            <ta e="T28" id="Seg_1821" s="T27">ветер.[NOM]</ta>
            <ta e="T29" id="Seg_1822" s="T28">поднимать-EP-PASS/REFL-EP-PST2.[3SG]</ta>
            <ta e="T30" id="Seg_1823" s="T29">тот.[NOM]</ta>
            <ta e="T31" id="Seg_1824" s="T30">поднимать-EP-PASS/REFL-CVB.SEQ</ta>
            <ta e="T32" id="Seg_1825" s="T31">Таал.[NOM]</ta>
            <ta e="T33" id="Seg_1826" s="T32">старуха.[NOM]</ta>
            <ta e="T34" id="Seg_1827" s="T33">перина-3SG-ACC</ta>
            <ta e="T35" id="Seg_1828" s="T34">летать-EP-CAUS-CVB.SEQ</ta>
            <ta e="T36" id="Seg_1829" s="T35">носить-PST2.[3SG]</ta>
            <ta e="T37" id="Seg_1830" s="T36">Таал.[NOM]</ta>
            <ta e="T38" id="Seg_1831" s="T37">старуха.[NOM]</ta>
            <ta e="T39" id="Seg_1832" s="T38">выйти-CVB.SIM</ta>
            <ta e="T40" id="Seg_1833" s="T39">бежать-PST2.[3SG]</ta>
            <ta e="T41" id="Seg_1834" s="T40">перина-3SG.[NOM]</ta>
            <ta e="T42" id="Seg_1835" s="T41">летать-CVB.SIM</ta>
            <ta e="T43" id="Seg_1836" s="T42">стоять-PTCP.PRS</ta>
            <ta e="T44" id="Seg_1837" s="T43">быть-PST2.[3SG]</ta>
            <ta e="T45" id="Seg_1838" s="T44">Таал.[NOM]</ta>
            <ta e="T46" id="Seg_1839" s="T45">старуха.[NOM]</ta>
            <ta e="T47" id="Seg_1840" s="T46">перина-3SG-ACC</ta>
            <ta e="T48" id="Seg_1841" s="T47">следовать-CVB.SEQ</ta>
            <ta e="T49" id="Seg_1842" s="T48">идти-PST2.[3SG]</ta>
            <ta e="T50" id="Seg_1843" s="T49">идти-CVB.SEQ</ta>
            <ta e="T51" id="Seg_1844" s="T50">идти-CVB.SEQ</ta>
            <ta e="T52" id="Seg_1845" s="T51">лед-DAT/LOC</ta>
            <ta e="T53" id="Seg_1846" s="T52">приходить-PST2.[3SG]</ta>
            <ta e="T54" id="Seg_1847" s="T53">поскользнуться-CVB.SEQ</ta>
            <ta e="T55" id="Seg_1848" s="T54">падать-PST2.[3SG]</ta>
            <ta e="T56" id="Seg_1849" s="T55">тот</ta>
            <ta e="T57" id="Seg_1850" s="T56">поскользнуться-CVB.SEQ</ta>
            <ta e="T58" id="Seg_1851" s="T57">нога-3SG-ACC</ta>
            <ta e="T59" id="Seg_1852" s="T58">повредить-EP-REFL-PST2.[3SG]</ta>
            <ta e="T60" id="Seg_1853" s="T59">тот</ta>
            <ta e="T61" id="Seg_1854" s="T60">лежать-CVB.SEQ</ta>
            <ta e="T62" id="Seg_1855" s="T61">говорить-PST2.[3SG]</ta>
            <ta e="T63" id="Seg_1856" s="T62">лед.[NOM]</ta>
            <ta e="T64" id="Seg_1857" s="T63">лед.[NOM]</ta>
            <ta e="T65" id="Seg_1858" s="T64">сильный-2SG</ta>
            <ta e="T66" id="Seg_1859" s="T65">Q</ta>
            <ta e="T67" id="Seg_1860" s="T66">сильный-1SG</ta>
            <ta e="T68" id="Seg_1861" s="T67">да</ta>
            <ta e="T69" id="Seg_1862" s="T68">сила-1SG</ta>
            <ta e="T70" id="Seg_1863" s="T69">говорить-PST2.[3SG]</ta>
            <ta e="T71" id="Seg_1864" s="T70">почему-EMPH</ta>
            <ta e="T72" id="Seg_1865" s="T71">солнце.[NOM]</ta>
            <ta e="T73" id="Seg_1866" s="T72">свет-3SG-DAT/LOC</ta>
            <ta e="T74" id="Seg_1867" s="T73">таять-PRS-2SG</ta>
            <ta e="T75" id="Seg_1868" s="T74">солнце.[NOM]</ta>
            <ta e="T76" id="Seg_1869" s="T75">свет-3SG.[NOM]</ta>
            <ta e="T77" id="Seg_1870" s="T76">сильный.[NOM]</ta>
            <ta e="T78" id="Seg_1871" s="T77">да</ta>
            <ta e="T79" id="Seg_1872" s="T78">сильный.[NOM]</ta>
            <ta e="T80" id="Seg_1873" s="T79">быть-INFER-3SG</ta>
            <ta e="T81" id="Seg_1874" s="T80">говорить-PST2.[3SG]</ta>
            <ta e="T82" id="Seg_1875" s="T81">солнце.[NOM]</ta>
            <ta e="T83" id="Seg_1876" s="T82">свет-3SG.[NOM]</ta>
            <ta e="T84" id="Seg_1877" s="T83">сильный-2SG</ta>
            <ta e="T85" id="Seg_1878" s="T84">Q</ta>
            <ta e="T86" id="Seg_1879" s="T85">сильный-1SG</ta>
            <ta e="T87" id="Seg_1880" s="T86">да</ta>
            <ta e="T88" id="Seg_1881" s="T87">сила-1SG</ta>
            <ta e="T89" id="Seg_1882" s="T88">почему-EMPH</ta>
            <ta e="T90" id="Seg_1883" s="T89">камень.[NOM]</ta>
            <ta e="T91" id="Seg_1884" s="T90">скала-DAT/LOC</ta>
            <ta e="T92" id="Seg_1885" s="T91">укрытие-VBZ-HAB-2SG=Q</ta>
            <ta e="T93" id="Seg_1886" s="T92">камень.[NOM]</ta>
            <ta e="T94" id="Seg_1887" s="T93">гора.[NOM]</ta>
            <ta e="T95" id="Seg_1888" s="T94">сильный.[NOM]</ta>
            <ta e="T96" id="Seg_1889" s="T95">да</ta>
            <ta e="T97" id="Seg_1890" s="T96">сильный.[NOM]</ta>
            <ta e="T98" id="Seg_1891" s="T97">быть-INFER-3SG</ta>
            <ta e="T99" id="Seg_1892" s="T98">говорить-PST2.[3SG]</ta>
            <ta e="T100" id="Seg_1893" s="T99">камень.[NOM]</ta>
            <ta e="T101" id="Seg_1894" s="T100">гора.[NOM]</ta>
            <ta e="T102" id="Seg_1895" s="T101">сильный-2SG</ta>
            <ta e="T103" id="Seg_1896" s="T102">Q</ta>
            <ta e="T104" id="Seg_1897" s="T103">сильный-1SG</ta>
            <ta e="T105" id="Seg_1898" s="T104">да</ta>
            <ta e="T106" id="Seg_1899" s="T105">сильный-1SG</ta>
            <ta e="T107" id="Seg_1900" s="T106">говорить-PST2.[3SG]</ta>
            <ta e="T108" id="Seg_1901" s="T107">почему-EMPH</ta>
            <ta e="T109" id="Seg_1902" s="T108">остромордый</ta>
            <ta e="T110" id="Seg_1903" s="T109">мышь.[NOM]</ta>
            <ta e="T111" id="Seg_1904" s="T110">пробурить-CVB.SIM</ta>
            <ta e="T112" id="Seg_1905" s="T111">бегать-PRS.[3SG]=Q</ta>
            <ta e="T113" id="Seg_1906" s="T112">остромордый</ta>
            <ta e="T114" id="Seg_1907" s="T113">мышь.[NOM]</ta>
            <ta e="T115" id="Seg_1908" s="T114">сильный.[NOM]</ta>
            <ta e="T116" id="Seg_1909" s="T115">да</ta>
            <ta e="T117" id="Seg_1910" s="T116">сильный.[NOM]</ta>
            <ta e="T118" id="Seg_1911" s="T117">быть-INFER-3SG</ta>
            <ta e="T119" id="Seg_1912" s="T118">говорить-PST2.[3SG]</ta>
            <ta e="T120" id="Seg_1913" s="T119">остромордый</ta>
            <ta e="T121" id="Seg_1914" s="T120">мышь.[NOM]</ta>
            <ta e="T122" id="Seg_1915" s="T121">сильный-2SG</ta>
            <ta e="T123" id="Seg_1916" s="T122">Q</ta>
            <ta e="T124" id="Seg_1917" s="T123">сильный-1SG</ta>
            <ta e="T125" id="Seg_1918" s="T124">да</ta>
            <ta e="T126" id="Seg_1919" s="T125">сила-1SG</ta>
            <ta e="T127" id="Seg_1920" s="T126">почему-EMPH</ta>
            <ta e="T128" id="Seg_1921" s="T127">нганасанский</ta>
            <ta e="T129" id="Seg_1922" s="T128">ребенок-3PL-DAT/LOC</ta>
            <ta e="T130" id="Seg_1923" s="T129">убить-PASS-HAB-2SG=Q</ta>
            <ta e="T131" id="Seg_1924" s="T130">нганасанский</ta>
            <ta e="T132" id="Seg_1925" s="T131">ребенок-3PL.[NOM]</ta>
            <ta e="T133" id="Seg_1926" s="T132">сильный.[NOM]</ta>
            <ta e="T134" id="Seg_1927" s="T133">да</ta>
            <ta e="T135" id="Seg_1928" s="T134">сильный.[NOM]</ta>
            <ta e="T136" id="Seg_1929" s="T135">быть-INFER-3PL</ta>
            <ta e="T137" id="Seg_1930" s="T136">нганасанский</ta>
            <ta e="T138" id="Seg_1931" s="T137">ребенок-3PL.[NOM]</ta>
            <ta e="T139" id="Seg_1932" s="T138">сильный-2PL</ta>
            <ta e="T140" id="Seg_1933" s="T139">Q</ta>
            <ta e="T141" id="Seg_1934" s="T140">сильный-1PL</ta>
            <ta e="T142" id="Seg_1935" s="T141">да</ta>
            <ta e="T143" id="Seg_1936" s="T142">сильный-1PL</ta>
            <ta e="T144" id="Seg_1937" s="T143">говорить-PST2-3PL</ta>
            <ta e="T145" id="Seg_1938" s="T144">почему-EMPH</ta>
            <ta e="T146" id="Seg_1939" s="T145">смерть-2PL-DAT/LOC</ta>
            <ta e="T147" id="Seg_1940" s="T146">мочь-PASS-HAB-2PL=Q</ta>
            <ta e="T148" id="Seg_1941" s="T147">смерть.[NOM]</ta>
            <ta e="T149" id="Seg_1942" s="T148">сильный.[NOM]</ta>
            <ta e="T150" id="Seg_1943" s="T149">да</ta>
            <ta e="T151" id="Seg_1944" s="T150">сильный.[NOM]</ta>
            <ta e="T152" id="Seg_1945" s="T151">быть-INFER-3SG</ta>
            <ta e="T153" id="Seg_1946" s="T152">говорить-PST2-3PL</ta>
            <ta e="T154" id="Seg_1947" s="T153">смерть.[NOM]</ta>
            <ta e="T155" id="Seg_1948" s="T154">сильный-2SG</ta>
            <ta e="T156" id="Seg_1949" s="T155">Q</ta>
            <ta e="T157" id="Seg_1950" s="T156">сильный-1SG</ta>
            <ta e="T158" id="Seg_1951" s="T157">да</ta>
            <ta e="T159" id="Seg_1952" s="T158">сильный-1SG</ta>
            <ta e="T160" id="Seg_1953" s="T159">говорить-PST2.[3SG]</ta>
            <ta e="T161" id="Seg_1954" s="T160">так</ta>
            <ta e="T162" id="Seg_1955" s="T161">говорить-CVB.SEQ</ta>
            <ta e="T163" id="Seg_1956" s="T162">после</ta>
            <ta e="T164" id="Seg_1957" s="T163">Таал.[NOM]</ta>
            <ta e="T165" id="Seg_1958" s="T164">старуха.[NOM]</ta>
            <ta e="T166" id="Seg_1959" s="T165">умирать-CVB.SEQ</ta>
            <ta e="T167" id="Seg_1960" s="T166">оставаться-PST2.[3SG]</ta>
            <ta e="T168" id="Seg_1961" s="T167">последний-3SG.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_1962" s="T0">propr-n:case</ta>
            <ta e="T2" id="Seg_1963" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_1964" s="T2">v-v:ptcp</ta>
            <ta e="T4" id="Seg_1965" s="T3">v-v:tense-v:pred.pn</ta>
            <ta e="T5" id="Seg_1966" s="T4">adv</ta>
            <ta e="T6" id="Seg_1967" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_1968" s="T6">dempro</ta>
            <ta e="T8" id="Seg_1969" s="T7">propr-n:case</ta>
            <ta e="T9" id="Seg_1970" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_1971" s="T9">n-n:poss-n:case</ta>
            <ta e="T11" id="Seg_1972" s="T10">v-v:tense-v:pred.pn</ta>
            <ta e="T12" id="Seg_1973" s="T11">dempro</ta>
            <ta e="T13" id="Seg_1974" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_1975" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_1976" s="T14">n-n&gt;adj</ta>
            <ta e="T16" id="Seg_1977" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_1978" s="T16">v-v:tense-v:pred.pn</ta>
            <ta e="T18" id="Seg_1979" s="T17">propr-n:case</ta>
            <ta e="T19" id="Seg_1980" s="T18">n-n:case</ta>
            <ta e="T20" id="Seg_1981" s="T19">n-n:poss-n:case</ta>
            <ta e="T21" id="Seg_1982" s="T20">v-v&gt;v-v:cvb</ta>
            <ta e="T22" id="Seg_1983" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_1984" s="T22">v-v:tense-v:pred.pn</ta>
            <ta e="T24" id="Seg_1985" s="T23">adv</ta>
            <ta e="T25" id="Seg_1986" s="T24">dempro</ta>
            <ta e="T26" id="Seg_1987" s="T25">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T27" id="Seg_1988" s="T26">post</ta>
            <ta e="T28" id="Seg_1989" s="T27">n-n:case</ta>
            <ta e="T29" id="Seg_1990" s="T28">v-v:(ins)-v&gt;v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T30" id="Seg_1991" s="T29">dempro-pro:case</ta>
            <ta e="T31" id="Seg_1992" s="T30">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T32" id="Seg_1993" s="T31">propr-n:case</ta>
            <ta e="T33" id="Seg_1994" s="T32">n-n:case</ta>
            <ta e="T34" id="Seg_1995" s="T33">n-n:poss-n:case</ta>
            <ta e="T35" id="Seg_1996" s="T34">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T36" id="Seg_1997" s="T35">v-v:tense-v:pred.pn</ta>
            <ta e="T37" id="Seg_1998" s="T36">propr-n:case</ta>
            <ta e="T38" id="Seg_1999" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_2000" s="T38">v-v:cvb</ta>
            <ta e="T40" id="Seg_2001" s="T39">v-v:tense-v:pred.pn</ta>
            <ta e="T41" id="Seg_2002" s="T40">n-n:(poss)-n:case</ta>
            <ta e="T42" id="Seg_2003" s="T41">v-v:cvb</ta>
            <ta e="T43" id="Seg_2004" s="T42">v-v:ptcp</ta>
            <ta e="T44" id="Seg_2005" s="T43">v-v:tense-v:pred.pn</ta>
            <ta e="T45" id="Seg_2006" s="T44">propr-n:case</ta>
            <ta e="T46" id="Seg_2007" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_2008" s="T46">n-n:poss-n:case</ta>
            <ta e="T48" id="Seg_2009" s="T47">v-v:cvb</ta>
            <ta e="T49" id="Seg_2010" s="T48">v-v:tense-v:pred.pn</ta>
            <ta e="T50" id="Seg_2011" s="T49">v-v:cvb</ta>
            <ta e="T51" id="Seg_2012" s="T50">v-v:cvb</ta>
            <ta e="T52" id="Seg_2013" s="T51">n-n:case</ta>
            <ta e="T53" id="Seg_2014" s="T52">v-v:tense-v:pred.pn</ta>
            <ta e="T54" id="Seg_2015" s="T53">v-v:cvb</ta>
            <ta e="T55" id="Seg_2016" s="T54">v-v:tense-v:pred.pn</ta>
            <ta e="T56" id="Seg_2017" s="T55">dempro</ta>
            <ta e="T57" id="Seg_2018" s="T56">v-v:cvb</ta>
            <ta e="T58" id="Seg_2019" s="T57">n-n:poss-n:case</ta>
            <ta e="T59" id="Seg_2020" s="T58">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T60" id="Seg_2021" s="T59">dempro</ta>
            <ta e="T61" id="Seg_2022" s="T60">v-v:cvb</ta>
            <ta e="T62" id="Seg_2023" s="T61">v-v:tense-v:pred.pn</ta>
            <ta e="T63" id="Seg_2024" s="T62">n-n:case</ta>
            <ta e="T64" id="Seg_2025" s="T63">n-n:case</ta>
            <ta e="T65" id="Seg_2026" s="T64">adj-n:(pred.pn)</ta>
            <ta e="T66" id="Seg_2027" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_2028" s="T66">adj-n:(pred.pn)</ta>
            <ta e="T68" id="Seg_2029" s="T67">conj</ta>
            <ta e="T69" id="Seg_2030" s="T68">n-n:(pred.pn)</ta>
            <ta e="T70" id="Seg_2031" s="T69">v-v:tense-v:pred.pn</ta>
            <ta e="T71" id="Seg_2032" s="T70">que-ptcl</ta>
            <ta e="T72" id="Seg_2033" s="T71">n-n:case</ta>
            <ta e="T73" id="Seg_2034" s="T72">n-n:poss-n:case</ta>
            <ta e="T74" id="Seg_2035" s="T73">v-v:tense-v:pred.pn</ta>
            <ta e="T75" id="Seg_2036" s="T74">n-n:case</ta>
            <ta e="T76" id="Seg_2037" s="T75">n-n:(poss)-n:case</ta>
            <ta e="T77" id="Seg_2038" s="T76">adj-n:case</ta>
            <ta e="T78" id="Seg_2039" s="T77">conj</ta>
            <ta e="T79" id="Seg_2040" s="T78">adj-n:case</ta>
            <ta e="T80" id="Seg_2041" s="T79">v-v:mood-v:poss.pn</ta>
            <ta e="T81" id="Seg_2042" s="T80">v-v:tense-v:pred.pn</ta>
            <ta e="T82" id="Seg_2043" s="T81">n-n:case</ta>
            <ta e="T83" id="Seg_2044" s="T82">n-n:(poss)-n:case</ta>
            <ta e="T84" id="Seg_2045" s="T83">adj-n:(pred.pn)</ta>
            <ta e="T85" id="Seg_2046" s="T84">ptcl</ta>
            <ta e="T86" id="Seg_2047" s="T85">adj-n:(pred.pn)</ta>
            <ta e="T87" id="Seg_2048" s="T86">conj</ta>
            <ta e="T88" id="Seg_2049" s="T87">n-n:(pred.pn)</ta>
            <ta e="T89" id="Seg_2050" s="T88">que-ptcl</ta>
            <ta e="T90" id="Seg_2051" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_2052" s="T90">n-n:case</ta>
            <ta e="T92" id="Seg_2053" s="T91">n-n&gt;v-v:mood-v:pred.pn-ptcl</ta>
            <ta e="T93" id="Seg_2054" s="T92">n-n:case</ta>
            <ta e="T94" id="Seg_2055" s="T93">n-n:case</ta>
            <ta e="T95" id="Seg_2056" s="T94">adj-n:case</ta>
            <ta e="T96" id="Seg_2057" s="T95">conj</ta>
            <ta e="T97" id="Seg_2058" s="T96">adj-n:case</ta>
            <ta e="T98" id="Seg_2059" s="T97">v-v:mood-v:poss.pn</ta>
            <ta e="T99" id="Seg_2060" s="T98">v-v:tense-v:pred.pn</ta>
            <ta e="T100" id="Seg_2061" s="T99">n-n:case</ta>
            <ta e="T101" id="Seg_2062" s="T100">n-n:case</ta>
            <ta e="T102" id="Seg_2063" s="T101">adj-n:(pred.pn)</ta>
            <ta e="T103" id="Seg_2064" s="T102">ptcl</ta>
            <ta e="T104" id="Seg_2065" s="T103">adj-n:(pred.pn)</ta>
            <ta e="T105" id="Seg_2066" s="T104">conj</ta>
            <ta e="T106" id="Seg_2067" s="T105">adj-n:(pred.pn)</ta>
            <ta e="T107" id="Seg_2068" s="T106">v-v:tense-v:pred.pn</ta>
            <ta e="T108" id="Seg_2069" s="T107">que-ptcl</ta>
            <ta e="T109" id="Seg_2070" s="T108">adj</ta>
            <ta e="T110" id="Seg_2071" s="T109">n-n:case</ta>
            <ta e="T111" id="Seg_2072" s="T110">v-v:cvb</ta>
            <ta e="T112" id="Seg_2073" s="T111">v-v:tense-v:pred.pn-ptcl</ta>
            <ta e="T113" id="Seg_2074" s="T112">adj</ta>
            <ta e="T114" id="Seg_2075" s="T113">n-n:case</ta>
            <ta e="T115" id="Seg_2076" s="T114">adj-n:case</ta>
            <ta e="T116" id="Seg_2077" s="T115">conj</ta>
            <ta e="T117" id="Seg_2078" s="T116">adj-n:case</ta>
            <ta e="T118" id="Seg_2079" s="T117">v-v:mood-v:poss.pn</ta>
            <ta e="T119" id="Seg_2080" s="T118">v-v:tense-v:pred.pn</ta>
            <ta e="T120" id="Seg_2081" s="T119">adj</ta>
            <ta e="T121" id="Seg_2082" s="T120">n-n:case</ta>
            <ta e="T122" id="Seg_2083" s="T121">adj-n:(pred.pn)</ta>
            <ta e="T123" id="Seg_2084" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_2085" s="T123">adj-n:(pred.pn)</ta>
            <ta e="T125" id="Seg_2086" s="T124">conj</ta>
            <ta e="T126" id="Seg_2087" s="T125">n-n:(pred.pn)</ta>
            <ta e="T127" id="Seg_2088" s="T126">que-ptcl</ta>
            <ta e="T128" id="Seg_2089" s="T127">adj</ta>
            <ta e="T129" id="Seg_2090" s="T128">n-n:poss-n:case</ta>
            <ta e="T130" id="Seg_2091" s="T129">v-v&gt;v-v:mood-v:pred.pn-ptcl</ta>
            <ta e="T131" id="Seg_2092" s="T130">adj</ta>
            <ta e="T132" id="Seg_2093" s="T131">n-n:(poss)-n:case</ta>
            <ta e="T133" id="Seg_2094" s="T132">adj-n:case</ta>
            <ta e="T134" id="Seg_2095" s="T133">conj</ta>
            <ta e="T135" id="Seg_2096" s="T134">adj-n:case</ta>
            <ta e="T136" id="Seg_2097" s="T135">v-v:mood-v:poss.pn</ta>
            <ta e="T137" id="Seg_2098" s="T136">adj</ta>
            <ta e="T138" id="Seg_2099" s="T137">n-n:(poss)-n:case</ta>
            <ta e="T139" id="Seg_2100" s="T138">adj-n:(pred.pn)</ta>
            <ta e="T140" id="Seg_2101" s="T139">ptcl</ta>
            <ta e="T141" id="Seg_2102" s="T140">adj-n:(pred.pn)</ta>
            <ta e="T142" id="Seg_2103" s="T141">conj</ta>
            <ta e="T143" id="Seg_2104" s="T142">adj-n:(pred.pn)</ta>
            <ta e="T144" id="Seg_2105" s="T143">v-v:tense-v:pred.pn</ta>
            <ta e="T145" id="Seg_2106" s="T144">que-ptcl</ta>
            <ta e="T146" id="Seg_2107" s="T145">n-n:poss-n:case</ta>
            <ta e="T147" id="Seg_2108" s="T146">v-v&gt;v-v:mood-v:pred.pn-ptcl</ta>
            <ta e="T148" id="Seg_2109" s="T147">n-n:case</ta>
            <ta e="T149" id="Seg_2110" s="T148">adj-n:case</ta>
            <ta e="T150" id="Seg_2111" s="T149">conj</ta>
            <ta e="T151" id="Seg_2112" s="T150">adj-n:case</ta>
            <ta e="T152" id="Seg_2113" s="T151">v-v:mood-v:poss.pn</ta>
            <ta e="T153" id="Seg_2114" s="T152">v-v:tense-v:pred.pn</ta>
            <ta e="T154" id="Seg_2115" s="T153">n-n:case</ta>
            <ta e="T155" id="Seg_2116" s="T154">adj-n:(pred.pn)</ta>
            <ta e="T156" id="Seg_2117" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_2118" s="T156">adj-n:(pred.pn)</ta>
            <ta e="T158" id="Seg_2119" s="T157">conj</ta>
            <ta e="T159" id="Seg_2120" s="T158">adj-n:(pred.pn)</ta>
            <ta e="T160" id="Seg_2121" s="T159">v-v:tense-v:pred.pn</ta>
            <ta e="T161" id="Seg_2122" s="T160">adv</ta>
            <ta e="T162" id="Seg_2123" s="T161">v-v:cvb</ta>
            <ta e="T163" id="Seg_2124" s="T162">post</ta>
            <ta e="T164" id="Seg_2125" s="T163">propr-n:case</ta>
            <ta e="T165" id="Seg_2126" s="T164">n-n:case</ta>
            <ta e="T166" id="Seg_2127" s="T165">v-v:cvb</ta>
            <ta e="T167" id="Seg_2128" s="T166">v-v:tense-v:pred.pn</ta>
            <ta e="T168" id="Seg_2129" s="T167">adj-n:(poss)-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_2130" s="T0">propr</ta>
            <ta e="T2" id="Seg_2131" s="T1">n</ta>
            <ta e="T3" id="Seg_2132" s="T2">v</ta>
            <ta e="T4" id="Seg_2133" s="T3">aux</ta>
            <ta e="T5" id="Seg_2134" s="T4">adv</ta>
            <ta e="T6" id="Seg_2135" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_2136" s="T6">dempro</ta>
            <ta e="T8" id="Seg_2137" s="T7">propr</ta>
            <ta e="T9" id="Seg_2138" s="T8">n</ta>
            <ta e="T10" id="Seg_2139" s="T9">n</ta>
            <ta e="T11" id="Seg_2140" s="T10">v</ta>
            <ta e="T12" id="Seg_2141" s="T11">dempro</ta>
            <ta e="T13" id="Seg_2142" s="T12">n</ta>
            <ta e="T14" id="Seg_2143" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_2144" s="T14">adj</ta>
            <ta e="T16" id="Seg_2145" s="T15">n</ta>
            <ta e="T17" id="Seg_2146" s="T16">cop</ta>
            <ta e="T18" id="Seg_2147" s="T17">propr</ta>
            <ta e="T19" id="Seg_2148" s="T18">n</ta>
            <ta e="T20" id="Seg_2149" s="T19">n</ta>
            <ta e="T21" id="Seg_2150" s="T20">v</ta>
            <ta e="T22" id="Seg_2151" s="T21">n</ta>
            <ta e="T23" id="Seg_2152" s="T22">v</ta>
            <ta e="T24" id="Seg_2153" s="T23">adv</ta>
            <ta e="T25" id="Seg_2154" s="T24">dempro</ta>
            <ta e="T26" id="Seg_2155" s="T25">v</ta>
            <ta e="T27" id="Seg_2156" s="T26">post</ta>
            <ta e="T28" id="Seg_2157" s="T27">n</ta>
            <ta e="T29" id="Seg_2158" s="T28">v</ta>
            <ta e="T30" id="Seg_2159" s="T29">dempro</ta>
            <ta e="T31" id="Seg_2160" s="T30">v</ta>
            <ta e="T32" id="Seg_2161" s="T31">propr</ta>
            <ta e="T33" id="Seg_2162" s="T32">n</ta>
            <ta e="T34" id="Seg_2163" s="T33">n</ta>
            <ta e="T35" id="Seg_2164" s="T34">v</ta>
            <ta e="T36" id="Seg_2165" s="T35">v</ta>
            <ta e="T37" id="Seg_2166" s="T36">propr</ta>
            <ta e="T38" id="Seg_2167" s="T37">n</ta>
            <ta e="T39" id="Seg_2168" s="T38">v</ta>
            <ta e="T40" id="Seg_2169" s="T39">v</ta>
            <ta e="T41" id="Seg_2170" s="T40">n</ta>
            <ta e="T42" id="Seg_2171" s="T41">v</ta>
            <ta e="T43" id="Seg_2172" s="T42">aux</ta>
            <ta e="T44" id="Seg_2173" s="T43">aux</ta>
            <ta e="T45" id="Seg_2174" s="T44">propr</ta>
            <ta e="T46" id="Seg_2175" s="T45">n</ta>
            <ta e="T47" id="Seg_2176" s="T46">n</ta>
            <ta e="T48" id="Seg_2177" s="T47">v</ta>
            <ta e="T49" id="Seg_2178" s="T48">v</ta>
            <ta e="T50" id="Seg_2179" s="T49">v</ta>
            <ta e="T51" id="Seg_2180" s="T50">aux</ta>
            <ta e="T52" id="Seg_2181" s="T51">n</ta>
            <ta e="T53" id="Seg_2182" s="T52">v</ta>
            <ta e="T54" id="Seg_2183" s="T53">v</ta>
            <ta e="T55" id="Seg_2184" s="T54">aux</ta>
            <ta e="T56" id="Seg_2185" s="T55">dempro</ta>
            <ta e="T57" id="Seg_2186" s="T56">v</ta>
            <ta e="T58" id="Seg_2187" s="T57">n</ta>
            <ta e="T59" id="Seg_2188" s="T58">v</ta>
            <ta e="T60" id="Seg_2189" s="T59">dempro</ta>
            <ta e="T61" id="Seg_2190" s="T60">v</ta>
            <ta e="T62" id="Seg_2191" s="T61">v</ta>
            <ta e="T63" id="Seg_2192" s="T62">n</ta>
            <ta e="T64" id="Seg_2193" s="T63">n</ta>
            <ta e="T65" id="Seg_2194" s="T64">adj</ta>
            <ta e="T66" id="Seg_2195" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_2196" s="T66">adj</ta>
            <ta e="T68" id="Seg_2197" s="T67">conj</ta>
            <ta e="T69" id="Seg_2198" s="T68">n</ta>
            <ta e="T70" id="Seg_2199" s="T69">v</ta>
            <ta e="T71" id="Seg_2200" s="T70">que</ta>
            <ta e="T72" id="Seg_2201" s="T71">n</ta>
            <ta e="T73" id="Seg_2202" s="T72">n</ta>
            <ta e="T74" id="Seg_2203" s="T73">v</ta>
            <ta e="T75" id="Seg_2204" s="T74">n</ta>
            <ta e="T76" id="Seg_2205" s="T75">n</ta>
            <ta e="T77" id="Seg_2206" s="T76">adj</ta>
            <ta e="T78" id="Seg_2207" s="T77">ptcl</ta>
            <ta e="T79" id="Seg_2208" s="T78">adj</ta>
            <ta e="T80" id="Seg_2209" s="T79">cop</ta>
            <ta e="T81" id="Seg_2210" s="T80">v</ta>
            <ta e="T82" id="Seg_2211" s="T81">n</ta>
            <ta e="T83" id="Seg_2212" s="T82">n</ta>
            <ta e="T84" id="Seg_2213" s="T83">adj</ta>
            <ta e="T85" id="Seg_2214" s="T84">ptcl</ta>
            <ta e="T86" id="Seg_2215" s="T85">adj</ta>
            <ta e="T87" id="Seg_2216" s="T86">conj</ta>
            <ta e="T88" id="Seg_2217" s="T87">n</ta>
            <ta e="T89" id="Seg_2218" s="T88">que</ta>
            <ta e="T90" id="Seg_2219" s="T89">n</ta>
            <ta e="T91" id="Seg_2220" s="T90">n</ta>
            <ta e="T92" id="Seg_2221" s="T91">v</ta>
            <ta e="T93" id="Seg_2222" s="T92">n</ta>
            <ta e="T94" id="Seg_2223" s="T93">n</ta>
            <ta e="T95" id="Seg_2224" s="T94">adj</ta>
            <ta e="T96" id="Seg_2225" s="T95">conj</ta>
            <ta e="T97" id="Seg_2226" s="T96">adj</ta>
            <ta e="T98" id="Seg_2227" s="T97">cop</ta>
            <ta e="T99" id="Seg_2228" s="T98">v</ta>
            <ta e="T100" id="Seg_2229" s="T99">n</ta>
            <ta e="T101" id="Seg_2230" s="T100">n</ta>
            <ta e="T102" id="Seg_2231" s="T101">adj</ta>
            <ta e="T103" id="Seg_2232" s="T102">ptcl</ta>
            <ta e="T104" id="Seg_2233" s="T103">adj</ta>
            <ta e="T105" id="Seg_2234" s="T104">conj</ta>
            <ta e="T106" id="Seg_2235" s="T105">adj</ta>
            <ta e="T107" id="Seg_2236" s="T106">v</ta>
            <ta e="T108" id="Seg_2237" s="T107">que</ta>
            <ta e="T109" id="Seg_2238" s="T108">adj</ta>
            <ta e="T110" id="Seg_2239" s="T109">n</ta>
            <ta e="T111" id="Seg_2240" s="T110">v</ta>
            <ta e="T112" id="Seg_2241" s="T111">v</ta>
            <ta e="T113" id="Seg_2242" s="T112">adj</ta>
            <ta e="T114" id="Seg_2243" s="T113">n</ta>
            <ta e="T115" id="Seg_2244" s="T114">adj</ta>
            <ta e="T116" id="Seg_2245" s="T115">conj</ta>
            <ta e="T117" id="Seg_2246" s="T116">adj</ta>
            <ta e="T118" id="Seg_2247" s="T117">cop</ta>
            <ta e="T119" id="Seg_2248" s="T118">v</ta>
            <ta e="T120" id="Seg_2249" s="T119">adj</ta>
            <ta e="T121" id="Seg_2250" s="T120">n</ta>
            <ta e="T122" id="Seg_2251" s="T121">adj</ta>
            <ta e="T123" id="Seg_2252" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_2253" s="T123">adj</ta>
            <ta e="T125" id="Seg_2254" s="T124">conj</ta>
            <ta e="T126" id="Seg_2255" s="T125">n</ta>
            <ta e="T127" id="Seg_2256" s="T126">que</ta>
            <ta e="T128" id="Seg_2257" s="T127">adj</ta>
            <ta e="T129" id="Seg_2258" s="T128">n</ta>
            <ta e="T130" id="Seg_2259" s="T129">v</ta>
            <ta e="T131" id="Seg_2260" s="T130">adj</ta>
            <ta e="T132" id="Seg_2261" s="T131">n</ta>
            <ta e="T133" id="Seg_2262" s="T132">adj</ta>
            <ta e="T134" id="Seg_2263" s="T133">conj</ta>
            <ta e="T135" id="Seg_2264" s="T134">adj</ta>
            <ta e="T136" id="Seg_2265" s="T135">cop</ta>
            <ta e="T137" id="Seg_2266" s="T136">adj</ta>
            <ta e="T138" id="Seg_2267" s="T137">n</ta>
            <ta e="T139" id="Seg_2268" s="T138">adj</ta>
            <ta e="T140" id="Seg_2269" s="T139">ptcl</ta>
            <ta e="T141" id="Seg_2270" s="T140">adj</ta>
            <ta e="T142" id="Seg_2271" s="T141">conj</ta>
            <ta e="T143" id="Seg_2272" s="T142">adj</ta>
            <ta e="T144" id="Seg_2273" s="T143">v</ta>
            <ta e="T145" id="Seg_2274" s="T144">que</ta>
            <ta e="T146" id="Seg_2275" s="T145">n</ta>
            <ta e="T147" id="Seg_2276" s="T146">v</ta>
            <ta e="T148" id="Seg_2277" s="T147">n</ta>
            <ta e="T149" id="Seg_2278" s="T148">adj</ta>
            <ta e="T150" id="Seg_2279" s="T149">conj</ta>
            <ta e="T151" id="Seg_2280" s="T150">adj</ta>
            <ta e="T152" id="Seg_2281" s="T151">cop</ta>
            <ta e="T153" id="Seg_2282" s="T152">v</ta>
            <ta e="T154" id="Seg_2283" s="T153">n</ta>
            <ta e="T155" id="Seg_2284" s="T154">adj</ta>
            <ta e="T156" id="Seg_2285" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_2286" s="T156">adj</ta>
            <ta e="T158" id="Seg_2287" s="T157">conj</ta>
            <ta e="T159" id="Seg_2288" s="T158">adj</ta>
            <ta e="T160" id="Seg_2289" s="T159">v</ta>
            <ta e="T161" id="Seg_2290" s="T160">adv</ta>
            <ta e="T162" id="Seg_2291" s="T161">v</ta>
            <ta e="T163" id="Seg_2292" s="T162">post</ta>
            <ta e="T164" id="Seg_2293" s="T163">propr</ta>
            <ta e="T165" id="Seg_2294" s="T164">n</ta>
            <ta e="T166" id="Seg_2295" s="T165">v</ta>
            <ta e="T167" id="Seg_2296" s="T166">aux</ta>
            <ta e="T168" id="Seg_2297" s="T167">adj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T4" id="Seg_2298" s="T0">There was an old woman [named] Taal</ta>
            <ta e="T11" id="Seg_2299" s="T4">Once this old woman Taal peed in her bedding.</ta>
            <ta e="T17" id="Seg_2300" s="T11">It was a sunny day.</ta>
            <ta e="T23" id="Seg_2301" s="T17">The old woman Taal hung her feather bed on the storage, for it to get dry.</ta>
            <ta e="T29" id="Seg_2302" s="T23">She just went back into her house, as the wind came.</ta>
            <ta e="T36" id="Seg_2303" s="T29">The wind raised and carried the old woman's geather bed away.</ta>
            <ta e="T44" id="Seg_2304" s="T36">The old woman Taal ran out but her feather bed was flying already.</ta>
            <ta e="T49" id="Seg_2305" s="T44">The old Taal followed her feather bed.</ta>
            <ta e="T55" id="Seg_2306" s="T49">She ran until the ice, slipped and fell.</ta>
            <ta e="T59" id="Seg_2307" s="T55">While slipping she injured her leg.</ta>
            <ta e="T62" id="Seg_2308" s="T59">Lying there she spoke to the ice: </ta>
            <ta e="T66" id="Seg_2309" s="T62">"Ice, Ice, are you powerful?"</ta>
            <ta e="T70" id="Seg_2310" s="T66">"I am very powerful!", it said.</ta>
            <ta e="T74" id="Seg_2311" s="T70">"So why do you thaw under the sun beam?"</ta>
            <ta e="T81" id="Seg_2312" s="T74">"The sun beam is even more powerful then!", it said.</ta>
            <ta e="T85" id="Seg_2313" s="T81">"Sun beam, are you powerful?"</ta>
            <ta e="T88" id="Seg_2314" s="T85">"I am very powerful!"</ta>
            <ta e="T92" id="Seg_2315" s="T88">"So why does a stone mountain cover you?"</ta>
            <ta e="T99" id="Seg_2316" s="T92">"The stone mountain is even more powerful then!", it said.</ta>
            <ta e="T103" id="Seg_2317" s="T99">"Stone mountain, are you powerful?"</ta>
            <ta e="T107" id="Seg_2318" s="T103">"I am very powerful!", it said.</ta>
            <ta e="T112" id="Seg_2319" s="T107">"So why does a narrowheaded mouse bore you through?"</ta>
            <ta e="T119" id="Seg_2320" s="T112">"The narrowheaded mouse is even more powerful then!", it said.</ta>
            <ta e="T123" id="Seg_2321" s="T119">"Narrowheaded mouse, are you powerful?"</ta>
            <ta e="T126" id="Seg_2322" s="T123">"I am very powerful!"</ta>
            <ta e="T130" id="Seg_2323" s="T126">"So why do Nganasan children kill you?"</ta>
            <ta e="T136" id="Seg_2324" s="T130">"The Nganasan children are even more powerful then."</ta>
            <ta e="T140" id="Seg_2325" s="T136">"Nganasan children, are you powerful?"</ta>
            <ta e="T144" id="Seg_2326" s="T140">"We are powerful!", they said.</ta>
            <ta e="T147" id="Seg_2327" s="T144">"So why do you let the death counquer you?"</ta>
            <ta e="T153" id="Seg_2328" s="T147">"The death is even more powerful then", they said.</ta>
            <ta e="T156" id="Seg_2329" s="T153">"Death, are you powerful?"</ta>
            <ta e="T160" id="Seg_2330" s="T156">"I am very powerful!", it said.</ta>
            <ta e="T167" id="Seg_2331" s="T160">After it said that, the old woman died.</ta>
            <ta e="T168" id="Seg_2332" s="T167">The end.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T4" id="Seg_2333" s="T0">Es lebte einmal die alte Frau Taal.</ta>
            <ta e="T11" id="Seg_2334" s="T4">Einmal aber nässte die Alte Taal ihr Bett ein.</ta>
            <ta e="T17" id="Seg_2335" s="T11">Es war ein sonniger Tag.</ta>
            <ta e="T23" id="Seg_2336" s="T17">Die Alte Taal hängte ihr Federbett auf den Speicher, damit es trocknete.</ta>
            <ta e="T29" id="Seg_2337" s="T23">Gerade nachdem sie wieder hineingegangen war, kam Wind auf.</ta>
            <ta e="T36" id="Seg_2338" s="T29">Der Wind, als er aufgekommen war, trug das Fedetbett der Alten Taal davon.</ta>
            <ta e="T44" id="Seg_2339" s="T36">Die Alte Taal lief hinaus, ihr Federbett aber flog schon.</ta>
            <ta e="T49" id="Seg_2340" s="T44">Die Alte Taal verfolgte das Federbett.</ta>
            <ta e="T55" id="Seg_2341" s="T49">Sie lief und kam bis zum Eis, sie rutschte aus und fiel hin.</ta>
            <ta e="T59" id="Seg_2342" s="T55">Als sie ausrutschte, verletzte sie sich ihr Bein.</ta>
            <ta e="T62" id="Seg_2343" s="T59">Sie lag und sagte: </ta>
            <ta e="T66" id="Seg_2344" s="T62">"Eis, Eis, bist du kräftig?"</ta>
            <ta e="T70" id="Seg_2345" s="T66">"Ich bin kräftig, und zwar wie!", sagte es.</ta>
            <ta e="T74" id="Seg_2346" s="T70">"Und warum taust du dann im Sonnenlicht?"</ta>
            <ta e="T81" id="Seg_2347" s="T74">"Das Sonnenlicht ist wohl stärker!", sagte es.</ta>
            <ta e="T85" id="Seg_2348" s="T81">"Sonnenlicht, bist du stark?"</ta>
            <ta e="T88" id="Seg_2349" s="T85">"Ich bin stark, und zwar wie!"</ta>
            <ta e="T92" id="Seg_2350" s="T88">"Und warum wirst du von einem Steinberg verdeckt?"</ta>
            <ta e="T99" id="Seg_2351" s="T92">"Der Steinberg ist wohl stärker", sagte es.</ta>
            <ta e="T103" id="Seg_2352" s="T99">"Steinberg, bist du stark?"</ta>
            <ta e="T107" id="Seg_2353" s="T103">"Ich bin stark, und wie!", sagte er.</ta>
            <ta e="T112" id="Seg_2354" s="T107">"Und warum gibt sich die Spitzmaus durch [dich] durch?"</ta>
            <ta e="T119" id="Seg_2355" s="T112">"Die Spitzmaus ist wohl stärker", sagte er.</ta>
            <ta e="T123" id="Seg_2356" s="T119">"Spitzmaus, bist du stark?"</ta>
            <ta e="T126" id="Seg_2357" s="T123">"Ich bin stark, und wie!"</ta>
            <ta e="T130" id="Seg_2358" s="T126">"Aber warum lässt du dich von nganasanischen Kindern töten?"</ta>
            <ta e="T136" id="Seg_2359" s="T130">"Die nganasanischen Kinder sind wohl stärker."</ta>
            <ta e="T140" id="Seg_2360" s="T136">"Nganasanische Kinder, seid ihr stark?"</ta>
            <ta e="T144" id="Seg_2361" s="T140">"Wir sind stark, und wie!", sagten sie.</ta>
            <ta e="T147" id="Seg_2362" s="T144">"Und warum unterwerft ihr euch dem Tod?"</ta>
            <ta e="T153" id="Seg_2363" s="T147">"Der Tod ist wohl stärker", sagten sie.</ta>
            <ta e="T156" id="Seg_2364" s="T153">"Tod, bist du stark?"</ta>
            <ta e="T160" id="Seg_2365" s="T156">"Ich bin kräftig, und wie!", sagte er.</ta>
            <ta e="T167" id="Seg_2366" s="T160">Nachdem er das gesagt hatte, starb die alte Frau Taal.</ta>
            <ta e="T168" id="Seg_2367" s="T167">Ende.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T4" id="Seg_2368" s="T0">Жила-была старушка Таал.</ta>
            <ta e="T11" id="Seg_2369" s="T4">Как-то раз старушка Таал подмочила постель.</ta>
            <ta e="T17" id="Seg_2370" s="T11">А день-то солнечный был.</ta>
            <ta e="T23" id="Seg_2371" s="T17">Старушка Таал перину бросила на лабаз, чтоб просушить.</ta>
            <ta e="T29" id="Seg_2372" s="T23">Только вошла в дом, — поднялся ветер.</ta>
            <ta e="T36" id="Seg_2373" s="T29">И, задув, унес перину старушки Таал.</ta>
            <ta e="T44" id="Seg_2374" s="T36">Старушка Таал выбежала, а перина уже летит.</ta>
            <ta e="T49" id="Seg_2375" s="T44">Старушка Таал бросилась за периной.</ta>
            <ta e="T55" id="Seg_2376" s="T49">Добежала до льда и поскользнулось.</ta>
            <ta e="T59" id="Seg_2377" s="T55">Поскользнувшись, повредила ногу.</ta>
            <ta e="T62" id="Seg_2378" s="T59">Так лежа, сказала: </ta>
            <ta e="T66" id="Seg_2379" s="T62">— Лед, лед, силен ли ты?</ta>
            <ta e="T70" id="Seg_2380" s="T66">— Силен, да еще как! — сказал.</ta>
            <ta e="T74" id="Seg_2381" s="T70">— А почему таешь от солнечных лучей?</ta>
            <ta e="T81" id="Seg_2382" s="T74">— Солнечный луч, значит, сильнее, — сказал.</ta>
            <ta e="T85" id="Seg_2383" s="T81">— Солнечный луч, силен ли ты?</ta>
            <ta e="T88" id="Seg_2384" s="T85">— Силен, да еще как!</ta>
            <ta e="T92" id="Seg_2385" s="T88">— А почему заслоняет тебя каменная гора?</ta>
            <ta e="T99" id="Seg_2386" s="T92">— Каменная гора, значит, сильнее, — сказал.</ta>
            <ta e="T103" id="Seg_2387" s="T99">— Каменная гора, сильна ли ты?</ta>
            <ta e="T107" id="Seg_2388" s="T103">— Сильна, да еще как! — сказал.</ta>
            <ta e="T112" id="Seg_2389" s="T107">— А почему роет тебя насквозь остромордая мышь?</ta>
            <ta e="T119" id="Seg_2390" s="T112">— Остромордая мышь, значит, сильнее, — сказала.</ta>
            <ta e="T123" id="Seg_2391" s="T119">— Остромордая мышь, сильна ли ты?</ta>
            <ta e="T126" id="Seg_2392" s="T123">— Сильна, да еще как!</ta>
            <ta e="T130" id="Seg_2393" s="T126">— А почему даешь себя убивать нганасанским детям?</ta>
            <ta e="T136" id="Seg_2394" s="T130">Нганасанские дети, значит, сильнее.</ta>
            <ta e="T140" id="Seg_2395" s="T136">— Нганасанские дети, сильны ли вы?</ta>
            <ta e="T144" id="Seg_2396" s="T140">— Сильны, да еще как! — сказали.</ta>
            <ta e="T147" id="Seg_2397" s="T144">— А почему поддаетесь смерти?</ta>
            <ta e="T153" id="Seg_2398" s="T147">Смерть, значит, сильнее, — сказали.</ta>
            <ta e="T156" id="Seg_2399" s="T153">— Смерть, сильна ли ты?</ta>
            <ta e="T160" id="Seg_2400" s="T156">— Сильна, да еще как! — сказала.</ta>
            <ta e="T167" id="Seg_2401" s="T160">Услышав это, старушка Таал померла.</ta>
            <ta e="T168" id="Seg_2402" s="T167">Конец.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T4" id="Seg_2403" s="T0">Жила-была старушка Таал.</ta>
            <ta e="T11" id="Seg_2404" s="T4">Как-то раз старушка Таал подмочила постель.</ta>
            <ta e="T17" id="Seg_2405" s="T11">А день-то солнечный был.</ta>
            <ta e="T23" id="Seg_2406" s="T17">Старушка Таал перину бросила на лабаз, чтоб просушить.</ta>
            <ta e="T29" id="Seg_2407" s="T23">Только вошла в дом, — поднялся ветер.</ta>
            <ta e="T36" id="Seg_2408" s="T29">И, задув, унес перину старушки Таал.</ta>
            <ta e="T44" id="Seg_2409" s="T36">Старушка Таал выбежала, а перина уже летит.</ta>
            <ta e="T49" id="Seg_2410" s="T44">Старушка Таал бросилась за периной.</ta>
            <ta e="T55" id="Seg_2411" s="T49">Добежала до льда и поскользнулось.</ta>
            <ta e="T59" id="Seg_2412" s="T55">Поскользнувшись, повредила ногу.</ta>
            <ta e="T62" id="Seg_2413" s="T59">Так лежа, сказала: </ta>
            <ta e="T66" id="Seg_2414" s="T62">— Лед, лед, силен ли ты?</ta>
            <ta e="T70" id="Seg_2415" s="T66">— Силен, да еще как! — сказал.</ta>
            <ta e="T74" id="Seg_2416" s="T70">— А почему таешь от солнечных лучей?</ta>
            <ta e="T81" id="Seg_2417" s="T74">— Солнечный луч, значит, сильнее, — сказал.</ta>
            <ta e="T85" id="Seg_2418" s="T81">— Солнечный луч, силен ли ты?</ta>
            <ta e="T88" id="Seg_2419" s="T85">— Силен, да еще как!</ta>
            <ta e="T92" id="Seg_2420" s="T88">— А почему заслоняет тебя каменная гора?</ta>
            <ta e="T99" id="Seg_2421" s="T92">— Каменная гора, значит, сильнее, — сказал.</ta>
            <ta e="T103" id="Seg_2422" s="T99">— Каменная гора, сильна ли ты?</ta>
            <ta e="T107" id="Seg_2423" s="T103">— Сильна, да еще как! — сказал.</ta>
            <ta e="T112" id="Seg_2424" s="T107">— А почему роет тебя насквозь остромордая мышь?</ta>
            <ta e="T119" id="Seg_2425" s="T112">— Остромордая мышь, значит, сильнее, — сказала.</ta>
            <ta e="T123" id="Seg_2426" s="T119">— Остромордая мышь, сильна ли ты?</ta>
            <ta e="T126" id="Seg_2427" s="T123">— Сильна, да еще как!</ta>
            <ta e="T130" id="Seg_2428" s="T126">— А почему даешь себя убивать нганасанским детям?</ta>
            <ta e="T136" id="Seg_2429" s="T130">— Нганасанские дети, значит, сильнее.</ta>
            <ta e="T140" id="Seg_2430" s="T136">— Нганасанские дети, сильны ли вы?</ta>
            <ta e="T144" id="Seg_2431" s="T140">— Сильны, да еще как! — сказали.</ta>
            <ta e="T147" id="Seg_2432" s="T144">— А почему поддаетесь смерти?</ta>
            <ta e="T153" id="Seg_2433" s="T147">Смерть, значит, сильнее, — сказали.</ta>
            <ta e="T156" id="Seg_2434" s="T153">— Смерть, сильна ли ты?</ta>
            <ta e="T160" id="Seg_2435" s="T156">— Сильна, да еще как! — сказала.</ta>
            <ta e="T167" id="Seg_2436" s="T160">Услышав это, старушка Таал померла.</ta>
            <ta e="T168" id="Seg_2437" s="T167">Конец.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
