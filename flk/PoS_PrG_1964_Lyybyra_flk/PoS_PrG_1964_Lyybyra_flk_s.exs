<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDEEC27B64-BBA2-7923-5B04-24B28AC1524F">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>PoS_PrG_1964_Lyybyra_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="creation-date">2017-02-16T17:30:47.427+01:00</ud-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\PoS_PrG_1964_Lyybyra_flk\PoS_PrG_1964_Lyybyra_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">665</ud-information>
            <ud-information attribute-name="# HIAT:w">462</ud-information>
            <ud-information attribute-name="# e">450</ud-information>
            <ud-information attribute-name="# HIAT:u">103</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PoS">
            <abbreviation>PoS</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T451" time="34.94444444444444" type="intp" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
         <tli id="T161" time="84.5" type="appl" />
         <tli id="T162" time="85.0" type="appl" />
         <tli id="T163" time="85.5" type="appl" />
         <tli id="T164" time="86.0" type="appl" />
         <tli id="T165" time="86.5" type="appl" />
         <tli id="T166" time="87.0" type="appl" />
         <tli id="T167" time="87.5" type="appl" />
         <tli id="T168" time="88.0" type="appl" />
         <tli id="T169" time="88.5" type="appl" />
         <tli id="T170" time="89.0" type="appl" />
         <tli id="T171" time="89.5" type="appl" />
         <tli id="T172" time="90.0" type="appl" />
         <tli id="T173" time="90.5" type="appl" />
         <tli id="T174" time="91.0" type="appl" />
         <tli id="T175" time="91.5" type="appl" />
         <tli id="T176" time="92.0" type="appl" />
         <tli id="T177" time="92.5" type="appl" />
         <tli id="T178" time="93.0" type="appl" />
         <tli id="T179" time="93.5" type="appl" />
         <tli id="T180" time="94.0" type="appl" />
         <tli id="T181" time="94.5" type="appl" />
         <tli id="T182" time="95.0" type="appl" />
         <tli id="T183" time="95.5" type="appl" />
         <tli id="T184" time="96.0" type="appl" />
         <tli id="T185" time="96.5" type="appl" />
         <tli id="T186" time="97.0" type="appl" />
         <tli id="T187" time="97.5" type="appl" />
         <tli id="T188" time="98.0" type="appl" />
         <tli id="T189" time="98.5" type="appl" />
         <tli id="T190" time="99.0" type="appl" />
         <tli id="T191" time="99.5" type="appl" />
         <tli id="T192" time="100.0" type="appl" />
         <tli id="T193" time="100.5" type="appl" />
         <tli id="T194" time="101.0" type="appl" />
         <tli id="T195" time="101.5" type="appl" />
         <tli id="T196" time="102.0" type="appl" />
         <tli id="T197" time="102.5" type="appl" />
         <tli id="T198" time="103.0" type="appl" />
         <tli id="T199" time="103.5" type="appl" />
         <tli id="T200" time="104.0" type="appl" />
         <tli id="T201" time="104.5" type="appl" />
         <tli id="T202" time="105.0" type="appl" />
         <tli id="T203" time="105.5" type="appl" />
         <tli id="T204" time="106.0" type="appl" />
         <tli id="T205" time="106.5" type="appl" />
         <tli id="T206" time="107.0" type="appl" />
         <tli id="T207" time="107.5" type="appl" />
         <tli id="T208" time="108.0" type="appl" />
         <tli id="T209" time="108.5" type="appl" />
         <tli id="T210" time="109.0" type="appl" />
         <tli id="T211" time="109.5" type="appl" />
         <tli id="T212" time="110.0" type="appl" />
         <tli id="T213" time="110.5" type="appl" />
         <tli id="T214" time="111.0" type="appl" />
         <tli id="T215" time="111.5" type="appl" />
         <tli id="T216" time="112.0" type="appl" />
         <tli id="T217" time="112.5" type="appl" />
         <tli id="T218" time="113.0" type="appl" />
         <tli id="T219" time="113.5" type="appl" />
         <tli id="T220" time="114.0" type="appl" />
         <tli id="T221" time="114.5" type="appl" />
         <tli id="T222" time="115.0" type="appl" />
         <tli id="T223" time="115.5" type="appl" />
         <tli id="T224" time="116.0" type="appl" />
         <tli id="T225" time="116.5" type="appl" />
         <tli id="T226" time="117.0" type="appl" />
         <tli id="T227" time="117.5" type="appl" />
         <tli id="T228" time="118.0" type="appl" />
         <tli id="T229" time="118.5" type="appl" />
         <tli id="T230" time="119.0" type="appl" />
         <tli id="T231" time="119.5" type="appl" />
         <tli id="T232" time="120.0" type="appl" />
         <tli id="T233" time="120.5" type="appl" />
         <tli id="T234" time="121.0" type="appl" />
         <tli id="T235" time="121.5" type="appl" />
         <tli id="T236" time="122.0" type="appl" />
         <tli id="T237" time="122.5" type="appl" />
         <tli id="T238" time="123.0" type="appl" />
         <tli id="T239" time="123.5" type="appl" />
         <tli id="T240" time="124.0" type="appl" />
         <tli id="T241" time="124.5" type="appl" />
         <tli id="T242" time="125.0" type="appl" />
         <tli id="T243" time="125.5" type="appl" />
         <tli id="T244" time="126.0" type="appl" />
         <tli id="T245" time="126.5" type="appl" />
         <tli id="T246" time="127.0" type="appl" />
         <tli id="T247" time="127.5" type="appl" />
         <tli id="T248" time="128.0" type="appl" />
         <tli id="T249" time="128.5" type="appl" />
         <tli id="T250" time="129.0" type="appl" />
         <tli id="T251" time="129.5" type="appl" />
         <tli id="T252" time="130.0" type="appl" />
         <tli id="T253" time="130.5" type="appl" />
         <tli id="T254" time="131.0" type="appl" />
         <tli id="T255" time="131.5" type="appl" />
         <tli id="T256" time="132.0" type="appl" />
         <tli id="T257" time="132.5" type="appl" />
         <tli id="T258" time="133.0" type="appl" />
         <tli id="T259" time="133.5" type="appl" />
         <tli id="T260" time="134.0" type="appl" />
         <tli id="T261" time="134.5" type="appl" />
         <tli id="T262" time="135.0" type="appl" />
         <tli id="T263" time="135.5" type="appl" />
         <tli id="T264" time="136.0" type="appl" />
         <tli id="T265" time="136.5" type="appl" />
         <tli id="T266" time="137.0" type="appl" />
         <tli id="T267" time="137.5" type="appl" />
         <tli id="T268" time="138.0" type="appl" />
         <tli id="T269" time="138.5" type="appl" />
         <tli id="T270" time="139.0" type="appl" />
         <tli id="T271" time="139.5" type="appl" />
         <tli id="T272" time="140.0" type="appl" />
         <tli id="T273" time="140.5" type="appl" />
         <tli id="T274" time="141.0" type="appl" />
         <tli id="T275" time="141.5" type="appl" />
         <tli id="T276" time="142.0" type="appl" />
         <tli id="T277" time="142.5" type="appl" />
         <tli id="T278" time="143.0" type="appl" />
         <tli id="T279" time="143.5" type="appl" />
         <tli id="T280" time="144.0" type="appl" />
         <tli id="T281" time="144.5" type="appl" />
         <tli id="T282" time="145.0" type="appl" />
         <tli id="T283" time="145.5" type="appl" />
         <tli id="T284" time="146.0" type="appl" />
         <tli id="T285" time="146.5" type="appl" />
         <tli id="T286" time="147.0" type="appl" />
         <tli id="T287" time="147.5" type="appl" />
         <tli id="T288" time="148.0" type="appl" />
         <tli id="T289" time="148.5" type="appl" />
         <tli id="T290" time="149.0" type="appl" />
         <tli id="T291" time="149.5" type="appl" />
         <tli id="T292" time="150.0" type="appl" />
         <tli id="T293" time="150.5" type="appl" />
         <tli id="T294" time="151.0" type="appl" />
         <tli id="T295" time="151.5" type="appl" />
         <tli id="T296" time="152.0" type="appl" />
         <tli id="T297" time="152.5" type="appl" />
         <tli id="T298" time="153.0" type="appl" />
         <tli id="T299" time="153.5" type="appl" />
         <tli id="T300" time="154.0" type="appl" />
         <tli id="T301" time="154.5" type="appl" />
         <tli id="T302" time="155.0" type="appl" />
         <tli id="T303" time="155.5" type="appl" />
         <tli id="T304" time="156.0" type="appl" />
         <tli id="T305" time="156.5" type="appl" />
         <tli id="T306" time="157.0" type="appl" />
         <tli id="T307" time="157.5" type="appl" />
         <tli id="T308" time="158.0" type="appl" />
         <tli id="T309" time="158.5" type="appl" />
         <tli id="T310" time="159.0" type="appl" />
         <tli id="T311" time="159.5" type="appl" />
         <tli id="T312" time="160.0" type="appl" />
         <tli id="T313" time="160.5" type="appl" />
         <tli id="T314" time="161.0" type="appl" />
         <tli id="T315" time="161.5" type="appl" />
         <tli id="T316" time="162.0" type="appl" />
         <tli id="T317" time="162.5" type="appl" />
         <tli id="T318" time="163.0" type="appl" />
         <tli id="T319" time="163.5" type="appl" />
         <tli id="T320" time="164.0" type="appl" />
         <tli id="T321" time="164.5" type="appl" />
         <tli id="T322" time="165.0" type="appl" />
         <tli id="T323" time="165.5" type="appl" />
         <tli id="T324" time="166.0" type="appl" />
         <tli id="T325" time="166.5" type="appl" />
         <tli id="T326" time="167.0" type="appl" />
         <tli id="T327" time="167.5" type="appl" />
         <tli id="T328" time="168.0" type="appl" />
         <tli id="T329" time="168.5" type="appl" />
         <tli id="T330" time="169.0" type="appl" />
         <tli id="T331" time="169.5" type="appl" />
         <tli id="T332" time="170.0" type="appl" />
         <tli id="T333" time="170.5" type="appl" />
         <tli id="T334" time="171.0" type="appl" />
         <tli id="T335" time="171.5" type="appl" />
         <tli id="T336" time="172.0" type="appl" />
         <tli id="T337" time="172.5" type="appl" />
         <tli id="T338" time="173.0" type="appl" />
         <tli id="T339" time="173.5" type="appl" />
         <tli id="T340" time="174.0" type="appl" />
         <tli id="T341" time="174.5" type="appl" />
         <tli id="T342" time="175.0" type="appl" />
         <tli id="T343" time="175.5" type="appl" />
         <tli id="T344" time="176.0" type="appl" />
         <tli id="T345" time="176.5" type="appl" />
         <tli id="T346" time="177.0" type="appl" />
         <tli id="T347" time="177.5" type="appl" />
         <tli id="T348" time="178.0" type="appl" />
         <tli id="T349" time="178.5" type="appl" />
         <tli id="T350" time="179.0" type="appl" />
         <tli id="T351" time="179.5" type="appl" />
         <tli id="T352" time="180.0" type="appl" />
         <tli id="T353" time="180.5" type="appl" />
         <tli id="T354" time="181.0" type="appl" />
         <tli id="T355" time="181.5" type="appl" />
         <tli id="T356" time="182.0" type="appl" />
         <tli id="T357" time="182.5" type="appl" />
         <tli id="T358" time="183.0" type="appl" />
         <tli id="T359" time="183.5" type="appl" />
         <tli id="T360" time="184.0" type="appl" />
         <tli id="T361" time="184.5" type="appl" />
         <tli id="T362" time="185.0" type="appl" />
         <tli id="T363" time="185.5" type="appl" />
         <tli id="T364" time="186.0" type="appl" />
         <tli id="T365" time="186.5" type="appl" />
         <tli id="T366" time="187.0" type="appl" />
         <tli id="T367" time="187.5" type="appl" />
         <tli id="T368" time="188.0" type="appl" />
         <tli id="T369" time="188.5" type="appl" />
         <tli id="T370" time="189.0" type="appl" />
         <tli id="T371" time="189.5" type="appl" />
         <tli id="T372" time="190.0" type="appl" />
         <tli id="T373" time="190.5" type="appl" />
         <tli id="T374" time="191.0" type="appl" />
         <tli id="T375" time="191.5" type="appl" />
         <tli id="T376" time="192.0" type="appl" />
         <tli id="T377" time="192.5" type="appl" />
         <tli id="T378" time="193.0" type="appl" />
         <tli id="T379" time="193.5" type="appl" />
         <tli id="T380" time="194.0" type="appl" />
         <tli id="T381" time="194.5" type="appl" />
         <tli id="T382" time="195.0" type="appl" />
         <tli id="T383" time="195.5" type="appl" />
         <tli id="T384" time="196.0" type="appl" />
         <tli id="T385" time="196.5" type="appl" />
         <tli id="T386" time="197.0" type="appl" />
         <tli id="T387" time="197.5" type="appl" />
         <tli id="T388" time="198.0" type="appl" />
         <tli id="T389" time="198.5" type="appl" />
         <tli id="T390" time="199.0" type="appl" />
         <tli id="T391" time="199.5" type="appl" />
         <tli id="T392" time="200.0" type="appl" />
         <tli id="T393" time="200.5" type="appl" />
         <tli id="T394" time="201.0" type="appl" />
         <tli id="T395" time="201.5" type="appl" />
         <tli id="T396" time="202.0" type="appl" />
         <tli id="T397" time="202.5" type="appl" />
         <tli id="T398" time="203.0" type="appl" />
         <tli id="T399" time="203.5" type="appl" />
         <tli id="T400" time="204.0" type="appl" />
         <tli id="T401" time="204.5" type="appl" />
         <tli id="T402" time="205.0" type="appl" />
         <tli id="T403" time="205.5" type="appl" />
         <tli id="T404" time="206.0" type="appl" />
         <tli id="T405" time="206.5" type="appl" />
         <tli id="T406" time="207.0" type="appl" />
         <tli id="T407" time="207.5" type="appl" />
         <tli id="T408" time="208.0" type="appl" />
         <tli id="T409" time="208.5" type="appl" />
         <tli id="T410" time="209.0" type="appl" />
         <tli id="T411" time="209.5" type="appl" />
         <tli id="T412" time="210.0" type="appl" />
         <tli id="T413" time="210.5" type="appl" />
         <tli id="T414" time="211.0" type="appl" />
         <tli id="T415" time="211.5" type="appl" />
         <tli id="T416" time="212.0" type="appl" />
         <tli id="T417" time="212.5" type="appl" />
         <tli id="T418" time="213.0" type="appl" />
         <tli id="T419" time="213.5" type="appl" />
         <tli id="T420" time="214.0" type="appl" />
         <tli id="T421" time="214.5" type="appl" />
         <tli id="T422" time="215.0" type="appl" />
         <tli id="T423" time="215.5" type="appl" />
         <tli id="T424" time="216.0" type="appl" />
         <tli id="T425" time="216.5" type="appl" />
         <tli id="T426" time="217.0" type="appl" />
         <tli id="T427" time="217.5" type="appl" />
         <tli id="T428" time="218.0" type="appl" />
         <tli id="T429" time="218.5" type="appl" />
         <tli id="T430" time="219.0" type="appl" />
         <tli id="T431" time="219.5" type="appl" />
         <tli id="T432" time="220.0" type="appl" />
         <tli id="T433" time="220.5" type="appl" />
         <tli id="T434" time="221.0" type="appl" />
         <tli id="T435" time="221.5" type="appl" />
         <tli id="T436" time="222.0" type="appl" />
         <tli id="T437" time="222.5" type="appl" />
         <tli id="T438" time="223.0" type="appl" />
         <tli id="T439" time="223.5" type="appl" />
         <tli id="T440" time="224.0" type="appl" />
         <tli id="T441" time="224.5" type="appl" />
         <tli id="T442" time="225.0" type="appl" />
         <tli id="T443" time="225.5" type="appl" />
         <tli id="T444" time="226.0" type="appl" />
         <tli id="T445" time="226.5" type="appl" />
         <tli id="T446" time="227.0" type="appl" />
         <tli id="T447" time="227.5" type="appl" />
         <tli id="T448" time="228.0" type="appl" />
         <tli id="T449" time="228.5" type="appl" />
         <tli id="T450" time="229.0" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PoS"
                      type="t">
         <timeline-fork end="T57" start="T56">
            <tli id="T56.tx.1" />
         </timeline-fork>
         <timeline-fork end="T73" start="T72">
            <tli id="T72.tx.1" />
         </timeline-fork>
         <timeline-fork end="T85" start="T84">
            <tli id="T84.tx.1" />
         </timeline-fork>
         <timeline-fork end="T132" start="T131">
            <tli id="T131.tx.1" />
         </timeline-fork>
         <timeline-fork end="T153" start="T152">
            <tli id="T152.tx.1" />
         </timeline-fork>
         <timeline-fork end="T174" start="T173">
            <tli id="T173.tx.1" />
         </timeline-fork>
         <timeline-fork end="T282" start="T281">
            <tli id="T281.tx.1" />
         </timeline-fork>
         <timeline-fork end="T284" start="T283">
            <tli id="T283.tx.1" />
         </timeline-fork>
         <timeline-fork end="T356" start="T355">
            <tli id="T355.tx.1" />
         </timeline-fork>
         <timeline-fork end="T408" start="T407">
            <tli id="T407.tx.1" />
         </timeline-fork>
         <timeline-fork end="T424" start="T423">
            <tli id="T423.tx.1" />
         </timeline-fork>
         <timeline-fork end="T437" start="T436">
            <tli id="T436.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T450" id="Seg_0" n="sc" s="T0">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Lɨːbɨra</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">erden</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">kü͡ögeldʼeten</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">iheːktiːr</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">daː</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_20" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">Ihen-ihen</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">taːska</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">keppit</ts>
                  <nts id="Seg_29" n="HIAT:ip">,</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">tɨːta</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">hɨstan</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">kaːlbɨt</ts>
                  <nts id="Seg_39" n="HIAT:ip">.</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_42" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">Erdiːtinen</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">oksubut</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">erdiːte</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_53" n="HIAT:w" s="T14">hɨstan</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_56" n="HIAT:w" s="T15">kaːlbɨt</ts>
                  <nts id="Seg_57" n="HIAT:ip">.</nts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_60" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_62" n="HIAT:w" s="T16">Iliːtinen</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">oksubut</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">iliːte</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_71" n="HIAT:w" s="T19">hɨstan</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_74" n="HIAT:w" s="T20">kaːlbɨt</ts>
                  <nts id="Seg_75" n="HIAT:ip">.</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_78" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_80" n="HIAT:w" s="T21">Biːrges</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_83" n="HIAT:w" s="T22">iliːtinen</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_86" n="HIAT:w" s="T23">oksor</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_89" n="HIAT:w" s="T24">biːrgehe</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_92" n="HIAT:w" s="T25">emi͡e</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_95" n="HIAT:w" s="T26">hɨstan</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_98" n="HIAT:w" s="T27">kaːlbɨt</ts>
                  <nts id="Seg_99" n="HIAT:ip">.</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_102" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">Atagɨnan</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_107" n="HIAT:w" s="T29">teppit</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_110" n="HIAT:w" s="T30">ataga</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_113" n="HIAT:w" s="T31">hɨstan</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_116" n="HIAT:w" s="T32">kaːlbɨt</ts>
                  <nts id="Seg_117" n="HIAT:ip">,</nts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_120" n="HIAT:w" s="T33">nöŋü͡ö</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_123" n="HIAT:w" s="T34">atagɨnan</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_126" n="HIAT:w" s="T35">teppite</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_129" n="HIAT:w" s="T36">emi͡e</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_132" n="HIAT:w" s="T37">hɨstan</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_135" n="HIAT:w" s="T38">kaːlbɨt</ts>
                  <nts id="Seg_136" n="HIAT:ip">.</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_139" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_141" n="HIAT:w" s="T39">Bɨ͡arɨnan</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_144" n="HIAT:w" s="T40">üppüte</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_147" n="HIAT:w" s="T41">bɨ͡ara</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_150" n="HIAT:w" s="T42">emi͡e</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_153" n="HIAT:w" s="T43">hɨstan</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_156" n="HIAT:w" s="T44">kaːlbɨt</ts>
                  <nts id="Seg_157" n="HIAT:ip">.</nts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_160" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_162" n="HIAT:w" s="T45">Onton</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_165" n="HIAT:w" s="T46">menʼiːtinen</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_168" n="HIAT:w" s="T47">oksubut</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_171" n="HIAT:w" s="T48">menʼiːte</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_174" n="HIAT:w" s="T49">emi͡e</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_177" n="HIAT:w" s="T50">hɨstan</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_180" n="HIAT:w" s="T51">kaːlbɨt</ts>
                  <nts id="Seg_181" n="HIAT:ip">.</nts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_184" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_186" n="HIAT:w" s="T52">Hɨstan</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_189" n="HIAT:w" s="T53">turdaga</ts>
                  <nts id="Seg_190" n="HIAT:ip">.</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_193" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_195" n="HIAT:w" s="T54">Ol</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_198" n="HIAT:w" s="T55">turdagɨn</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56.tx.1" id="Seg_201" n="HIAT:w" s="T56">Aŋaː</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_204" n="HIAT:w" s="T56.tx.1">Moŋus</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_207" n="HIAT:w" s="T57">kelbit</ts>
                  <nts id="Seg_208" n="HIAT:ip">.</nts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_211" n="HIAT:u" s="T58">
                  <nts id="Seg_212" n="HIAT:ip">"</nts>
                  <ts e="T59" id="Seg_214" n="HIAT:w" s="T58">Anʼɨkataːn</ts>
                  <nts id="Seg_215" n="HIAT:ip">!</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_218" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_220" n="HIAT:w" s="T59">Ki͡eheːŋŋi</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_223" n="HIAT:w" s="T60">hokuːskalaːk</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_226" n="HIAT:w" s="T61">bu͡ollum</ts>
                  <nts id="Seg_227" n="HIAT:ip">"</nts>
                  <nts id="Seg_228" n="HIAT:ip">,</nts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_231" n="HIAT:w" s="T62">di͡ebitinen</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_234" n="HIAT:w" s="T63">Lɨːbɨranɨ</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_237" n="HIAT:w" s="T64">ɨlan</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_240" n="HIAT:w" s="T65">karmaːnɨgar</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_243" n="HIAT:w" s="T66">uktan</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_246" n="HIAT:w" s="T67">keːspit</ts>
                  <nts id="Seg_247" n="HIAT:ip">.</nts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_250" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_252" n="HIAT:w" s="T68">Dʼi͡etiger</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_255" n="HIAT:w" s="T69">barbɨt</ts>
                  <nts id="Seg_256" n="HIAT:ip">.</nts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_259" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_261" n="HIAT:w" s="T70">Orogugar</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_264" n="HIAT:w" s="T71">tijen</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72.tx.1" id="Seg_267" n="HIAT:w" s="T72">Aŋaː</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_270" n="HIAT:w" s="T72.tx.1">Moŋus</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_273" n="HIAT:w" s="T73">haːktɨ͡an</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_276" n="HIAT:w" s="T74">bagarbɨt</ts>
                  <nts id="Seg_277" n="HIAT:ip">.</nts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_280" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_282" n="HIAT:w" s="T75">Ol</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_285" n="HIAT:w" s="T76">haːktɨː</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_288" n="HIAT:w" s="T77">olordoguna</ts>
                  <nts id="Seg_289" n="HIAT:ip">,</nts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_292" n="HIAT:w" s="T78">Lɨːbɨra</ts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_295" n="HIAT:w" s="T79">karmaːnɨn</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_298" n="HIAT:w" s="T80">kajagahɨn</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_301" n="HIAT:w" s="T81">üstün</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_304" n="HIAT:w" s="T82">tühen</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_307" n="HIAT:w" s="T83">kaːlbɨt</ts>
                  <nts id="Seg_308" n="HIAT:ip">.</nts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_311" n="HIAT:u" s="T84">
                  <ts e="T84.tx.1" id="Seg_313" n="HIAT:w" s="T84">Aŋaː</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_316" n="HIAT:w" s="T84.tx.1">Moŋus</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_319" n="HIAT:w" s="T85">tugu</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_322" n="HIAT:w" s="T86">da</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_325" n="HIAT:w" s="T87">bilbekke</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_328" n="HIAT:w" s="T88">hɨ͡alɨjatɨn</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_331" n="HIAT:w" s="T89">kötögünne</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_334" n="HIAT:w" s="T90">da</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_337" n="HIAT:w" s="T91">dʼi͡etiger</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_340" n="HIAT:w" s="T92">barbɨt</ts>
                  <nts id="Seg_341" n="HIAT:ip">.</nts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_344" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_346" n="HIAT:w" s="T93">Dʼi͡etiger</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_349" n="HIAT:w" s="T94">tahɨgar</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_352" n="HIAT:w" s="T95">honun</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_355" n="HIAT:w" s="T96">araŋaska</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_358" n="HIAT:w" s="T97">ɨjaːn</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_361" n="HIAT:w" s="T98">baran</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_364" n="HIAT:w" s="T99">kiːrbit</ts>
                  <nts id="Seg_365" n="HIAT:ip">.</nts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T104" id="Seg_368" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_370" n="HIAT:w" s="T100">Emeːksine</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_373" n="HIAT:w" s="T101">iːstene</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_376" n="HIAT:w" s="T102">oloror</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_379" n="HIAT:w" s="T103">ebit</ts>
                  <nts id="Seg_380" n="HIAT:ip">.</nts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_383" n="HIAT:u" s="T104">
                  <nts id="Seg_384" n="HIAT:ip">"</nts>
                  <ts e="T105" id="Seg_386" n="HIAT:w" s="T104">Emeːksi͡en</ts>
                  <nts id="Seg_387" n="HIAT:ip">,</nts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_390" n="HIAT:w" s="T105">kehiːbin</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_393" n="HIAT:w" s="T106">egellim</ts>
                  <nts id="Seg_394" n="HIAT:ip">.</nts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_397" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_399" n="HIAT:w" s="T107">Honum</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_402" n="HIAT:w" s="T108">karmaːnɨgar</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_405" n="HIAT:w" s="T109">baːr</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_408" n="HIAT:w" s="T110">tahaːra</ts>
                  <nts id="Seg_409" n="HIAT:ip">,</nts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_412" n="HIAT:w" s="T111">onu</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_415" n="HIAT:w" s="T112">killeren</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_418" n="HIAT:w" s="T113">kör</ts>
                  <nts id="Seg_419" n="HIAT:ip">"</nts>
                  <nts id="Seg_420" n="HIAT:ip">,</nts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_423" n="HIAT:w" s="T114">di͡ebit</ts>
                  <nts id="Seg_424" n="HIAT:ip">.</nts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_427" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_429" n="HIAT:w" s="T115">Emeːksine</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_432" n="HIAT:w" s="T116">ü͡örüːtüger</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_435" n="HIAT:w" s="T117">iŋneleːk</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_438" n="HIAT:w" s="T118">hüːtügün</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_441" n="HIAT:w" s="T119">ɨjɨran</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_444" n="HIAT:w" s="T120">keːspit</ts>
                  <nts id="Seg_445" n="HIAT:ip">,</nts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_448" n="HIAT:w" s="T121">taksa</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_451" n="HIAT:w" s="T122">köppüt</ts>
                  <nts id="Seg_452" n="HIAT:ip">.</nts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_455" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_457" n="HIAT:w" s="T123">Bulbakka</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_460" n="HIAT:w" s="T124">tönnön</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_463" n="HIAT:w" s="T125">kiːrbit</ts>
                  <nts id="Seg_464" n="HIAT:ip">:</nts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_467" n="HIAT:u" s="T126">
                  <nts id="Seg_468" n="HIAT:ip">"</nts>
                  <ts e="T127" id="Seg_470" n="HIAT:w" s="T126">Karmaːŋŋar</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_473" n="HIAT:w" s="T127">tu͡ok</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_476" n="HIAT:w" s="T128">da</ts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_479" n="HIAT:w" s="T129">hu͡ok</ts>
                  <nts id="Seg_480" n="HIAT:ip">"</nts>
                  <nts id="Seg_481" n="HIAT:ip">,</nts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_484" n="HIAT:w" s="T130">di͡ebit</ts>
                  <nts id="Seg_485" n="HIAT:ip">.</nts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T132" id="Seg_488" n="HIAT:u" s="T131">
                  <ts e="T131.tx.1" id="Seg_490" n="HIAT:w" s="T131">Aŋaː</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_493" n="HIAT:w" s="T131.tx.1">Moŋus</ts>
                  <nts id="Seg_494" n="HIAT:ip">:</nts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_497" n="HIAT:u" s="T132">
                  <nts id="Seg_498" n="HIAT:ip">"</nts>
                  <ts e="T133" id="Seg_500" n="HIAT:w" s="T132">Bejeŋ</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_503" n="HIAT:w" s="T133">hi͡etegiŋ</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_506" n="HIAT:w" s="T134">diːn</ts>
                  <nts id="Seg_507" n="HIAT:ip">"</nts>
                  <nts id="Seg_508" n="HIAT:ip">,</nts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_511" n="HIAT:w" s="T135">di͡ebit</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_514" n="HIAT:w" s="T136">da</ts>
                  <nts id="Seg_515" n="HIAT:ip">,</nts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_518" n="HIAT:w" s="T137">emeːksin</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_521" n="HIAT:w" s="T138">bɨ͡arɨn</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_524" n="HIAT:w" s="T139">kaja</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_527" n="HIAT:w" s="T140">tardar</ts>
                  <nts id="Seg_528" n="HIAT:ip">.</nts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T148" id="Seg_531" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_533" n="HIAT:w" s="T141">Emeːksin</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_536" n="HIAT:w" s="T142">bɨ͡arɨttan</ts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_539" n="HIAT:w" s="T143">iŋneleːk</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_542" n="HIAT:w" s="T144">hüːtük</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_545" n="HIAT:w" s="T145">kɨlɨr</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_548" n="HIAT:w" s="T146">gɨna</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_551" n="HIAT:w" s="T147">tüspüt</ts>
                  <nts id="Seg_552" n="HIAT:ip">.</nts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_555" n="HIAT:u" s="T148">
                  <nts id="Seg_556" n="HIAT:ip">"</nts>
                  <ts e="T149" id="Seg_558" n="HIAT:w" s="T148">Oː</ts>
                  <nts id="Seg_559" n="HIAT:ip">,</nts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_562" n="HIAT:w" s="T149">kaːrɨ͡an</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_565" n="HIAT:w" s="T150">dʼaktarbɨn</ts>
                  <nts id="Seg_566" n="HIAT:ip">"</nts>
                  <nts id="Seg_567" n="HIAT:ip">,</nts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_570" n="HIAT:w" s="T151">üːgüleːbit</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152.tx.1" id="Seg_573" n="HIAT:w" s="T152">Aŋaː</ts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_576" n="HIAT:w" s="T152.tx.1">Moŋus</ts>
                  <nts id="Seg_577" n="HIAT:ip">.</nts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T155" id="Seg_580" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_582" n="HIAT:w" s="T153">Töttörü</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_585" n="HIAT:w" s="T154">hüːrbüt</ts>
                  <nts id="Seg_586" n="HIAT:ip">.</nts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T160" id="Seg_589" n="HIAT:u" s="T155">
                  <ts e="T156" id="Seg_591" n="HIAT:w" s="T155">Lɨːbɨra</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_594" n="HIAT:w" s="T156">orokko</ts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_597" n="HIAT:w" s="T157">bu</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_600" n="HIAT:w" s="T158">čaŋkaja</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_603" n="HIAT:w" s="T159">hɨtar</ts>
                  <nts id="Seg_604" n="HIAT:ip">.</nts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T164" id="Seg_607" n="HIAT:u" s="T160">
                  <ts e="T161" id="Seg_609" n="HIAT:w" s="T160">ɨlar</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_612" n="HIAT:w" s="T161">da</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_615" n="HIAT:w" s="T162">karmaːnɨgar</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_618" n="HIAT:w" s="T163">uktar</ts>
                  <nts id="Seg_619" n="HIAT:ip">.</nts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_622" n="HIAT:u" s="T164">
                  <ts e="T165" id="Seg_624" n="HIAT:w" s="T164">Töttörü</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_627" n="HIAT:w" s="T165">hüːren</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_630" n="HIAT:w" s="T166">keler</ts>
                  <nts id="Seg_631" n="HIAT:ip">.</nts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T173" id="Seg_634" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_636" n="HIAT:w" s="T167">Dʼi͡etiger</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_639" n="HIAT:w" s="T168">kiːren</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_642" n="HIAT:w" s="T169">Lɨːbɨranɨ</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_645" n="HIAT:w" s="T170">iːkeːptiːŋŋe</ts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_648" n="HIAT:w" s="T171">ɨjaːn</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_651" n="HIAT:w" s="T172">keːher</ts>
                  <nts id="Seg_652" n="HIAT:ip">.</nts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T178" id="Seg_655" n="HIAT:u" s="T173">
                  <ts e="T173.tx.1" id="Seg_657" n="HIAT:w" s="T173">Aŋaː</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_660" n="HIAT:w" s="T173.tx.1">Moŋus</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_663" n="HIAT:w" s="T174">hette</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_666" n="HIAT:w" s="T175">tarakaːj</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_669" n="HIAT:w" s="T176">ogoloːk</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_672" n="HIAT:w" s="T177">ebit</ts>
                  <nts id="Seg_673" n="HIAT:ip">.</nts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_676" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_678" n="HIAT:w" s="T178">Ol</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_681" n="HIAT:w" s="T179">ogolorukar</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_684" n="HIAT:w" s="T180">di͡ebit</ts>
                  <nts id="Seg_685" n="HIAT:ip">:</nts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T187" id="Seg_688" n="HIAT:u" s="T181">
                  <nts id="Seg_689" n="HIAT:ip">"</nts>
                  <ts e="T182" id="Seg_691" n="HIAT:w" s="T181">Ogoloːr</ts>
                  <nts id="Seg_692" n="HIAT:ip">,</nts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_695" n="HIAT:w" s="T182">Lɨːbɨra</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_698" n="HIAT:w" s="T183">hɨ͡ata</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_701" n="HIAT:w" s="T184">testi͡ege</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_703" n="HIAT:ip">—</nts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_706" n="HIAT:w" s="T185">körör</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_709" n="HIAT:w" s="T186">bu͡oluŋ</ts>
                  <nts id="Seg_710" n="HIAT:ip">.</nts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T189" id="Seg_713" n="HIAT:u" s="T187">
                  <ts e="T188" id="Seg_715" n="HIAT:w" s="T187">Kelerber</ts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_718" n="HIAT:w" s="T188">kü͡östeːriŋ</ts>
                  <nts id="Seg_719" n="HIAT:ip">!</nts>
                  <nts id="Seg_720" n="HIAT:ip">"</nts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T193" id="Seg_723" n="HIAT:u" s="T189">
                  <ts e="T190" id="Seg_725" n="HIAT:w" s="T189">Di͡ete</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_728" n="HIAT:w" s="T190">da</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_731" n="HIAT:w" s="T191">taksan</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_734" n="HIAT:w" s="T192">kaːlbɨt</ts>
                  <nts id="Seg_735" n="HIAT:ip">.</nts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T199" id="Seg_738" n="HIAT:u" s="T193">
                  <ts e="T194" id="Seg_740" n="HIAT:w" s="T193">Töhö</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_743" n="HIAT:w" s="T194">da</ts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_746" n="HIAT:w" s="T195">bu͡olbakka</ts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_749" n="HIAT:w" s="T196">Lɨːbɨra</ts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_752" n="HIAT:w" s="T197">dʼe</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_755" n="HIAT:w" s="T198">iːkteːbit</ts>
                  <nts id="Seg_756" n="HIAT:ip">.</nts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T202" id="Seg_759" n="HIAT:u" s="T199">
                  <ts e="T200" id="Seg_761" n="HIAT:w" s="T199">Ogolor</ts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_764" n="HIAT:w" s="T200">onno</ts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_767" n="HIAT:w" s="T201">üːgülespitter</ts>
                  <nts id="Seg_768" n="HIAT:ip">:</nts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T203" id="Seg_771" n="HIAT:u" s="T202">
                  <nts id="Seg_772" n="HIAT:ip">"</nts>
                  <ts e="T203" id="Seg_774" n="HIAT:w" s="T202">Anʼikataːn</ts>
                  <nts id="Seg_775" n="HIAT:ip">!</nts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T207" id="Seg_778" n="HIAT:u" s="T203">
                  <ts e="T204" id="Seg_780" n="HIAT:w" s="T203">Lɨːbɨra</ts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_783" n="HIAT:w" s="T204">ɨbagas</ts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_786" n="HIAT:w" s="T205">hɨ͡ata</ts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_789" n="HIAT:w" s="T206">togunna</ts>
                  <nts id="Seg_790" n="HIAT:ip">!</nts>
                  <nts id="Seg_791" n="HIAT:ip">"</nts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T211" id="Seg_794" n="HIAT:u" s="T207">
                  <ts e="T208" id="Seg_796" n="HIAT:w" s="T207">Onton</ts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_799" n="HIAT:w" s="T208">Lɨːbɨra</ts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_802" n="HIAT:w" s="T209">dʼe</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_805" n="HIAT:w" s="T210">haːktɨːr</ts>
                  <nts id="Seg_806" n="HIAT:ip">.</nts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T213" id="Seg_809" n="HIAT:u" s="T211">
                  <ts e="T212" id="Seg_811" n="HIAT:w" s="T211">Ogolor</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_814" n="HIAT:w" s="T212">üːgülespitter</ts>
                  <nts id="Seg_815" n="HIAT:ip">:</nts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T214" id="Seg_818" n="HIAT:u" s="T213">
                  <nts id="Seg_819" n="HIAT:ip">"</nts>
                  <ts e="T214" id="Seg_821" n="HIAT:w" s="T213">Anʼikataːn</ts>
                  <nts id="Seg_822" n="HIAT:ip">!</nts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T218" id="Seg_825" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_827" n="HIAT:w" s="T214">Lɨːbɨra</ts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_830" n="HIAT:w" s="T215">kojuː</ts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_833" n="HIAT:w" s="T216">hɨ͡ata</ts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_836" n="HIAT:w" s="T217">togunna</ts>
                  <nts id="Seg_837" n="HIAT:ip">!</nts>
                  <nts id="Seg_838" n="HIAT:ip">"</nts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T221" id="Seg_841" n="HIAT:u" s="T218">
                  <ts e="T219" id="Seg_843" n="HIAT:w" s="T218">Lɨːbɨra</ts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_846" n="HIAT:w" s="T219">ogolorgo</ts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_849" n="HIAT:w" s="T220">di͡ebit</ts>
                  <nts id="Seg_850" n="HIAT:ip">:</nts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T222" id="Seg_853" n="HIAT:u" s="T221">
                  <nts id="Seg_854" n="HIAT:ip">"</nts>
                  <ts e="T222" id="Seg_856" n="HIAT:w" s="T221">Ogoloːr</ts>
                  <nts id="Seg_857" n="HIAT:ip">!</nts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T224" id="Seg_860" n="HIAT:u" s="T222">
                  <ts e="T223" id="Seg_862" n="HIAT:w" s="T222">Tüheriŋ</ts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_865" n="HIAT:w" s="T223">miːgin</ts>
                  <nts id="Seg_866" n="HIAT:ip">.</nts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T234" id="Seg_869" n="HIAT:u" s="T224">
                  <ts e="T225" id="Seg_871" n="HIAT:w" s="T224">Min</ts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_874" n="HIAT:w" s="T225">ehi͡eke</ts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_877" n="HIAT:w" s="T226">lu͡osku</ts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_880" n="HIAT:w" s="T227">oŋoron</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_883" n="HIAT:w" s="T228">bi͡eri͡em</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_885" n="HIAT:ip">—</nts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_888" n="HIAT:w" s="T229">min</ts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_891" n="HIAT:w" s="T230">hɨ͡abɨn</ts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_894" n="HIAT:w" s="T231">hiːrgitiger</ts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_897" n="HIAT:w" s="T232">üčügej</ts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_900" n="HIAT:w" s="T233">bu͡olu͡o</ts>
                  <nts id="Seg_901" n="HIAT:ip">!</nts>
                  <nts id="Seg_902" n="HIAT:ip">"</nts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T237" id="Seg_905" n="HIAT:u" s="T234">
                  <nts id="Seg_906" n="HIAT:ip">"</nts>
                  <ts e="T235" id="Seg_908" n="HIAT:w" s="T234">Kaja</ts>
                  <nts id="Seg_909" n="HIAT:ip">,</nts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_912" n="HIAT:w" s="T235">erej</ts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_915" n="HIAT:w" s="T236">keje</ts>
                  <nts id="Seg_916" n="HIAT:ip">!</nts>
                  <nts id="Seg_917" n="HIAT:ip">"</nts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T239" id="Seg_920" n="HIAT:u" s="T237">
                  <ts e="T238" id="Seg_922" n="HIAT:w" s="T237">Lɨːbɨranɨ</ts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_925" n="HIAT:w" s="T238">tüherbitter</ts>
                  <nts id="Seg_926" n="HIAT:ip">.</nts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T241" id="Seg_929" n="HIAT:u" s="T239">
                  <ts e="T240" id="Seg_931" n="HIAT:w" s="T239">Lɨːbɨra</ts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_934" n="HIAT:w" s="T240">di͡ebit</ts>
                  <nts id="Seg_935" n="HIAT:ip">:</nts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T245" id="Seg_938" n="HIAT:u" s="T241">
                  <nts id="Seg_939" n="HIAT:ip">"</nts>
                  <ts e="T242" id="Seg_941" n="HIAT:w" s="T241">Egeliŋ</ts>
                  <nts id="Seg_942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_944" n="HIAT:w" s="T242">teːtegit</ts>
                  <nts id="Seg_945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_947" n="HIAT:w" s="T243">hɨtɨː</ts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_950" n="HIAT:w" s="T244">hügetin</ts>
                  <nts id="Seg_951" n="HIAT:ip">!</nts>
                  <nts id="Seg_952" n="HIAT:ip">"</nts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T247" id="Seg_955" n="HIAT:u" s="T245">
                  <ts e="T246" id="Seg_957" n="HIAT:w" s="T245">Egelen</ts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_960" n="HIAT:w" s="T246">bi͡erbitter</ts>
                  <nts id="Seg_961" n="HIAT:ip">.</nts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T253" id="Seg_964" n="HIAT:u" s="T247">
                  <nts id="Seg_965" n="HIAT:ip">"</nts>
                  <ts e="T248" id="Seg_967" n="HIAT:w" s="T247">Oroŋŋutugar</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_970" n="HIAT:w" s="T248">körö</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_973" n="HIAT:w" s="T249">hɨtɨŋ</ts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_975" n="HIAT:ip">—</nts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_978" n="HIAT:w" s="T250">kajdak</ts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_981" n="HIAT:w" s="T251">min</ts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_984" n="HIAT:w" s="T252">oŋorobun</ts>
                  <nts id="Seg_985" n="HIAT:ip">.</nts>
                  <nts id="Seg_986" n="HIAT:ip">"</nts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T256" id="Seg_989" n="HIAT:u" s="T253">
                  <ts e="T254" id="Seg_991" n="HIAT:w" s="T253">Ogolor</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_994" n="HIAT:w" s="T254">hɨtan</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_997" n="HIAT:w" s="T255">kaːlbɨttar</ts>
                  <nts id="Seg_998" n="HIAT:ip">.</nts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T262" id="Seg_1001" n="HIAT:u" s="T256">
                  <ts e="T257" id="Seg_1003" n="HIAT:w" s="T256">Ol</ts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_1006" n="HIAT:w" s="T257">hɨppɨttarɨgar</ts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_1009" n="HIAT:w" s="T258">Lɨːbɨra</ts>
                  <nts id="Seg_1010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_1012" n="HIAT:w" s="T259">menʼiːlerin</ts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_1015" n="HIAT:w" s="T260">bɨha</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1018" n="HIAT:w" s="T261">kerditeleːbit</ts>
                  <nts id="Seg_1019" n="HIAT:ip">.</nts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T265" id="Seg_1022" n="HIAT:u" s="T262">
                  <ts e="T263" id="Seg_1024" n="HIAT:w" s="T262">Etterin</ts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_1027" n="HIAT:w" s="T263">kü͡östeːn</ts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_1030" n="HIAT:w" s="T264">keːspit</ts>
                  <nts id="Seg_1031" n="HIAT:ip">.</nts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T272" id="Seg_1034" n="HIAT:u" s="T265">
                  <ts e="T266" id="Seg_1036" n="HIAT:w" s="T265">Menʼiːleri</ts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1039" n="HIAT:w" s="T266">oroŋŋo</ts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1042" n="HIAT:w" s="T267">kečigireččikeːn</ts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1045" n="HIAT:w" s="T268">uːran</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1048" n="HIAT:w" s="T269">baran</ts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1051" n="HIAT:w" s="T270">hu͡organɨnan</ts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1054" n="HIAT:w" s="T271">habɨtalaːbɨt</ts>
                  <nts id="Seg_1055" n="HIAT:ip">.</nts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T279" id="Seg_1058" n="HIAT:u" s="T272">
                  <ts e="T273" id="Seg_1060" n="HIAT:w" s="T272">Korgoldʼiːnɨ</ts>
                  <nts id="Seg_1061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1063" n="HIAT:w" s="T273">ɨlan</ts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1066" n="HIAT:w" s="T274">baran</ts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1069" n="HIAT:w" s="T275">ohok</ts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_1072" n="HIAT:w" s="T276">annɨgar</ts>
                  <nts id="Seg_1073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1075" n="HIAT:w" s="T277">kiːren</ts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1078" n="HIAT:w" s="T278">kaːlbɨt</ts>
                  <nts id="Seg_1079" n="HIAT:ip">.</nts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T283" id="Seg_1082" n="HIAT:u" s="T279">
                  <ts e="T280" id="Seg_1084" n="HIAT:w" s="T279">Bu</ts>
                  <nts id="Seg_1085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1087" n="HIAT:w" s="T280">hɨttagɨna</ts>
                  <nts id="Seg_1088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281.tx.1" id="Seg_1090" n="HIAT:w" s="T281">Aŋaː</ts>
                  <nts id="Seg_1091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1093" n="HIAT:w" s="T281.tx.1">Moŋus</ts>
                  <nts id="Seg_1094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1096" n="HIAT:w" s="T282">kelbit</ts>
                  <nts id="Seg_1097" n="HIAT:ip">.</nts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T286" id="Seg_1100" n="HIAT:u" s="T283">
                  <ts e="T283.tx.1" id="Seg_1102" n="HIAT:w" s="T283">Aŋaː</ts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1105" n="HIAT:w" s="T283.tx.1">Moŋus</ts>
                  <nts id="Seg_1106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1108" n="HIAT:w" s="T284">kelen</ts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1111" n="HIAT:w" s="T285">üːgüleːbit</ts>
                  <nts id="Seg_1112" n="HIAT:ip">:</nts>
                  <nts id="Seg_1113" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T287" id="Seg_1115" n="HIAT:u" s="T286">
                  <nts id="Seg_1116" n="HIAT:ip">"</nts>
                  <ts e="T287" id="Seg_1118" n="HIAT:w" s="T286">Ogoloːr</ts>
                  <nts id="Seg_1119" n="HIAT:ip">!</nts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T289" id="Seg_1122" n="HIAT:u" s="T287">
                  <ts e="T288" id="Seg_1124" n="HIAT:w" s="T287">Aːŋŋɨtɨn</ts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1127" n="HIAT:w" s="T288">arɨjɨŋ</ts>
                  <nts id="Seg_1128" n="HIAT:ip">!</nts>
                  <nts id="Seg_1129" n="HIAT:ip">"</nts>
                  <nts id="Seg_1130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T292" id="Seg_1132" n="HIAT:u" s="T289">
                  <ts e="T290" id="Seg_1134" n="HIAT:w" s="T289">Kim</ts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1137" n="HIAT:w" s="T290">da</ts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1140" n="HIAT:w" s="T291">arɨjbat</ts>
                  <nts id="Seg_1141" n="HIAT:ip">.</nts>
                  <nts id="Seg_1142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T295" id="Seg_1144" n="HIAT:u" s="T292">
                  <ts e="T293" id="Seg_1146" n="HIAT:w" s="T292">Kasta</ts>
                  <nts id="Seg_1147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1149" n="HIAT:w" s="T293">da</ts>
                  <nts id="Seg_1150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1152" n="HIAT:w" s="T294">üːgüleːbit</ts>
                  <nts id="Seg_1153" n="HIAT:ip">.</nts>
                  <nts id="Seg_1154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T298" id="Seg_1156" n="HIAT:u" s="T295">
                  <ts e="T296" id="Seg_1158" n="HIAT:w" s="T295">Ogolor</ts>
                  <nts id="Seg_1159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1161" n="HIAT:w" s="T296">törüt</ts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1164" n="HIAT:w" s="T297">arɨjbataktar</ts>
                  <nts id="Seg_1165" n="HIAT:ip">.</nts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T302" id="Seg_1168" n="HIAT:u" s="T298">
                  <ts e="T299" id="Seg_1170" n="HIAT:w" s="T298">Aːnɨn</ts>
                  <nts id="Seg_1171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1173" n="HIAT:w" s="T299">kostuː</ts>
                  <nts id="Seg_1174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1176" n="HIAT:w" s="T300">tardan</ts>
                  <nts id="Seg_1177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1179" n="HIAT:w" s="T301">kiːrbit</ts>
                  <nts id="Seg_1180" n="HIAT:ip">.</nts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T309" id="Seg_1183" n="HIAT:u" s="T302">
                  <ts e="T303" id="Seg_1185" n="HIAT:w" s="T302">Körbüte</ts>
                  <nts id="Seg_1186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1187" n="HIAT:ip">—</nts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1190" n="HIAT:w" s="T303">et</ts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1193" n="HIAT:w" s="T304">buha</ts>
                  <nts id="Seg_1194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1196" n="HIAT:w" s="T305">turar</ts>
                  <nts id="Seg_1197" n="HIAT:ip">,</nts>
                  <nts id="Seg_1198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1200" n="HIAT:w" s="T306">ogolor</ts>
                  <nts id="Seg_1201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1203" n="HIAT:w" s="T307">utuja</ts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1206" n="HIAT:w" s="T308">hɨtallar</ts>
                  <nts id="Seg_1207" n="HIAT:ip">.</nts>
                  <nts id="Seg_1208" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T320" id="Seg_1210" n="HIAT:u" s="T309">
                  <nts id="Seg_1211" n="HIAT:ip">"</nts>
                  <ts e="T310" id="Seg_1213" n="HIAT:w" s="T309">Oː</ts>
                  <nts id="Seg_1214" n="HIAT:ip">,</nts>
                  <nts id="Seg_1215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1217" n="HIAT:w" s="T310">ogolorum</ts>
                  <nts id="Seg_1218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1220" n="HIAT:w" s="T311">Lɨːbɨra</ts>
                  <nts id="Seg_1221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1223" n="HIAT:w" s="T312">hɨ͡atɨttan</ts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1226" n="HIAT:w" s="T313">toton</ts>
                  <nts id="Seg_1227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1229" n="HIAT:w" s="T314">baran</ts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1232" n="HIAT:w" s="T315">utuja</ts>
                  <nts id="Seg_1233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1235" n="HIAT:w" s="T316">hɨtallar</ts>
                  <nts id="Seg_1236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1238" n="HIAT:w" s="T317">ebit</ts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1241" n="HIAT:w" s="T318">bu͡o</ts>
                  <nts id="Seg_1242" n="HIAT:ip">"</nts>
                  <nts id="Seg_1243" n="HIAT:ip">,</nts>
                  <nts id="Seg_1244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1246" n="HIAT:w" s="T319">diːr</ts>
                  <nts id="Seg_1247" n="HIAT:ip">.</nts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T322" id="Seg_1250" n="HIAT:u" s="T320">
                  <ts e="T321" id="Seg_1252" n="HIAT:w" s="T320">Kü͡öhü</ts>
                  <nts id="Seg_1253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1255" n="HIAT:w" s="T321">tahaːrar</ts>
                  <nts id="Seg_1256" n="HIAT:ip">.</nts>
                  <nts id="Seg_1257" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T326" id="Seg_1259" n="HIAT:u" s="T322">
                  <ts e="T323" id="Seg_1261" n="HIAT:w" s="T322">Hüregitten</ts>
                  <nts id="Seg_1262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1264" n="HIAT:w" s="T323">amsajar</ts>
                  <nts id="Seg_1265" n="HIAT:ip">—</nts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1268" n="HIAT:w" s="T324">hürege</ts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1271" n="HIAT:w" s="T325">tarpɨt</ts>
                  <nts id="Seg_1272" n="HIAT:ip">.</nts>
                  <nts id="Seg_1273" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T333" id="Seg_1275" n="HIAT:u" s="T326">
                  <nts id="Seg_1276" n="HIAT:ip">"</nts>
                  <ts e="T327" id="Seg_1278" n="HIAT:w" s="T326">Oː</ts>
                  <nts id="Seg_1279" n="HIAT:ip">,</nts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1282" n="HIAT:w" s="T327">Lɨːbɨra</ts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1285" n="HIAT:w" s="T328">min</ts>
                  <nts id="Seg_1286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1288" n="HIAT:w" s="T329">uruːm</ts>
                  <nts id="Seg_1289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1291" n="HIAT:w" s="T330">ebit</ts>
                  <nts id="Seg_1292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1294" n="HIAT:w" s="T331">duː</ts>
                  <nts id="Seg_1295" n="HIAT:ip">"</nts>
                  <nts id="Seg_1296" n="HIAT:ip">,</nts>
                  <nts id="Seg_1297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1299" n="HIAT:w" s="T332">di͡ebit</ts>
                  <nts id="Seg_1300" n="HIAT:ip">.</nts>
                  <nts id="Seg_1301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T337" id="Seg_1303" n="HIAT:u" s="T333">
                  <ts e="T334" id="Seg_1305" n="HIAT:w" s="T333">Taːl</ts>
                  <nts id="Seg_1306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1308" n="HIAT:w" s="T334">hi͡ebit</ts>
                  <nts id="Seg_1309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1310" n="HIAT:ip">—</nts>
                  <nts id="Seg_1311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1313" n="HIAT:w" s="T335">taːla</ts>
                  <nts id="Seg_1314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1316" n="HIAT:w" s="T336">tarpɨt</ts>
                  <nts id="Seg_1317" n="HIAT:ip">.</nts>
                  <nts id="Seg_1318" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T344" id="Seg_1320" n="HIAT:u" s="T337">
                  <nts id="Seg_1321" n="HIAT:ip">"</nts>
                  <ts e="T338" id="Seg_1323" n="HIAT:w" s="T337">Oː</ts>
                  <nts id="Seg_1324" n="HIAT:ip">,</nts>
                  <nts id="Seg_1325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1327" n="HIAT:w" s="T338">Lɨːbɨra</ts>
                  <nts id="Seg_1328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1330" n="HIAT:w" s="T339">ile</ts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1333" n="HIAT:w" s="T340">uruːm</ts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1336" n="HIAT:w" s="T341">ebit</ts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1339" n="HIAT:w" s="T342">duː</ts>
                  <nts id="Seg_1340" n="HIAT:ip">"</nts>
                  <nts id="Seg_1341" n="HIAT:ip">,</nts>
                  <nts id="Seg_1342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1344" n="HIAT:w" s="T343">di͡ebit</ts>
                  <nts id="Seg_1345" n="HIAT:ip">.</nts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T346" id="Seg_1348" n="HIAT:u" s="T344">
                  <ts e="T345" id="Seg_1350" n="HIAT:w" s="T344">Onton</ts>
                  <nts id="Seg_1351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1353" n="HIAT:w" s="T345">turbut</ts>
                  <nts id="Seg_1354" n="HIAT:ip">.</nts>
                  <nts id="Seg_1355" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T353" id="Seg_1357" n="HIAT:u" s="T346">
                  <ts e="T347" id="Seg_1359" n="HIAT:w" s="T346">Ogolorun</ts>
                  <nts id="Seg_1360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1362" n="HIAT:w" s="T347">arɨja</ts>
                  <nts id="Seg_1363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1365" n="HIAT:w" s="T348">tarpɨta</ts>
                  <nts id="Seg_1366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1367" n="HIAT:ip">—</nts>
                  <nts id="Seg_1368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1370" n="HIAT:w" s="T349">hette</ts>
                  <nts id="Seg_1371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1373" n="HIAT:w" s="T350">menʼiː</ts>
                  <nts id="Seg_1374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1376" n="HIAT:w" s="T351">čekenis</ts>
                  <nts id="Seg_1377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1379" n="HIAT:w" s="T352">gɨmmɨttar</ts>
                  <nts id="Seg_1380" n="HIAT:ip">.</nts>
                  <nts id="Seg_1381" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T357" id="Seg_1383" n="HIAT:u" s="T353">
                  <ts e="T354" id="Seg_1385" n="HIAT:w" s="T353">Hohuja</ts>
                  <nts id="Seg_1386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1388" n="HIAT:w" s="T354">kɨtta</ts>
                  <nts id="Seg_1389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355.tx.1" id="Seg_1391" n="HIAT:w" s="T355">Aŋaː</ts>
                  <nts id="Seg_1392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1394" n="HIAT:w" s="T355.tx.1">Moŋus</ts>
                  <nts id="Seg_1395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1397" n="HIAT:w" s="T356">üːgüleːbit</ts>
                  <nts id="Seg_1398" n="HIAT:ip">:</nts>
                  <nts id="Seg_1399" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T358" id="Seg_1401" n="HIAT:u" s="T357">
                  <nts id="Seg_1402" n="HIAT:ip">"</nts>
                  <ts e="T358" id="Seg_1404" n="HIAT:w" s="T357">Lɨːbɨra</ts>
                  <nts id="Seg_1405" n="HIAT:ip">!</nts>
                  <nts id="Seg_1406" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T360" id="Seg_1408" n="HIAT:u" s="T358">
                  <ts e="T359" id="Seg_1410" n="HIAT:w" s="T358">Kanna</ts>
                  <nts id="Seg_1411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1413" n="HIAT:w" s="T359">baːrgɨnɨj</ts>
                  <nts id="Seg_1414" n="HIAT:ip">?</nts>
                  <nts id="Seg_1415" n="HIAT:ip">"</nts>
                  <nts id="Seg_1416" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T361" id="Seg_1418" n="HIAT:u" s="T360">
                  <ts e="T361" id="Seg_1420" n="HIAT:w" s="T360">Lɨːbɨra</ts>
                  <nts id="Seg_1421" n="HIAT:ip">:</nts>
                  <nts id="Seg_1422" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T364" id="Seg_1424" n="HIAT:u" s="T361">
                  <nts id="Seg_1425" n="HIAT:ip">"</nts>
                  <ts e="T362" id="Seg_1427" n="HIAT:w" s="T361">Tahaːra</ts>
                  <nts id="Seg_1428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1430" n="HIAT:w" s="T362">baːrbɨn</ts>
                  <nts id="Seg_1431" n="HIAT:ip">"</nts>
                  <nts id="Seg_1432" n="HIAT:ip">,</nts>
                  <nts id="Seg_1433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1435" n="HIAT:w" s="T363">di͡ebit</ts>
                  <nts id="Seg_1436" n="HIAT:ip">.</nts>
                  <nts id="Seg_1437" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T371" id="Seg_1439" n="HIAT:u" s="T364">
                  <ts e="T365" id="Seg_1441" n="HIAT:w" s="T364">Onu</ts>
                  <nts id="Seg_1442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1444" n="HIAT:w" s="T365">ihitte</ts>
                  <nts id="Seg_1445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1447" n="HIAT:w" s="T366">da</ts>
                  <nts id="Seg_1448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1450" n="HIAT:w" s="T367">dʼi͡etin</ts>
                  <nts id="Seg_1451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1453" n="HIAT:w" s="T368">kaja</ts>
                  <nts id="Seg_1454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1456" n="HIAT:w" s="T369">kötön</ts>
                  <nts id="Seg_1457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1459" n="HIAT:w" s="T370">taksar</ts>
                  <nts id="Seg_1460" n="HIAT:ip">.</nts>
                  <nts id="Seg_1461" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T375" id="Seg_1463" n="HIAT:u" s="T371">
                  <ts e="T372" id="Seg_1465" n="HIAT:w" s="T371">Lɨːbɨra</ts>
                  <nts id="Seg_1466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1468" n="HIAT:w" s="T372">kanna</ts>
                  <nts id="Seg_1469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1471" n="HIAT:w" s="T373">da</ts>
                  <nts id="Seg_1472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1474" n="HIAT:w" s="T374">hu͡ok</ts>
                  <nts id="Seg_1475" n="HIAT:ip">.</nts>
                  <nts id="Seg_1476" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T377" id="Seg_1478" n="HIAT:u" s="T375">
                  <nts id="Seg_1479" n="HIAT:ip">"</nts>
                  <ts e="T376" id="Seg_1481" n="HIAT:w" s="T375">Lɨːbɨraː</ts>
                  <nts id="Seg_1482" n="HIAT:ip">,</nts>
                  <nts id="Seg_1483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1485" n="HIAT:w" s="T376">kannagɨnɨj</ts>
                  <nts id="Seg_1486" n="HIAT:ip">?</nts>
                  <nts id="Seg_1487" n="HIAT:ip">"</nts>
                  <nts id="Seg_1488" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T382" id="Seg_1490" n="HIAT:u" s="T377">
                  <nts id="Seg_1491" n="HIAT:ip">"</nts>
                  <ts e="T378" id="Seg_1493" n="HIAT:w" s="T377">Dʼi͡e</ts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1496" n="HIAT:w" s="T378">ihiger</ts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1499" n="HIAT:w" s="T379">baːrbɨn</ts>
                  <nts id="Seg_1500" n="HIAT:ip">"</nts>
                  <nts id="Seg_1501" n="HIAT:ip">,</nts>
                  <nts id="Seg_1502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1504" n="HIAT:w" s="T380">di͡ebit</ts>
                  <nts id="Seg_1505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1507" n="HIAT:w" s="T381">Lɨːbɨra</ts>
                  <nts id="Seg_1508" n="HIAT:ip">.</nts>
                  <nts id="Seg_1509" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T387" id="Seg_1511" n="HIAT:u" s="T382">
                  <ts e="T383" id="Seg_1513" n="HIAT:w" s="T382">Dʼi͡ege</ts>
                  <nts id="Seg_1514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1516" n="HIAT:w" s="T383">kötön</ts>
                  <nts id="Seg_1517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1519" n="HIAT:w" s="T384">tüspüt</ts>
                  <nts id="Seg_1520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1521" n="HIAT:ip">—</nts>
                  <nts id="Seg_1522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1524" n="HIAT:w" s="T385">Lɨːbɨra</ts>
                  <nts id="Seg_1525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1527" n="HIAT:w" s="T386">hu͡ok</ts>
                  <nts id="Seg_1528" n="HIAT:ip">.</nts>
                  <nts id="Seg_1529" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T389" id="Seg_1531" n="HIAT:u" s="T387">
                  <nts id="Seg_1532" n="HIAT:ip">"</nts>
                  <ts e="T388" id="Seg_1534" n="HIAT:w" s="T387">Lɨːbɨraː</ts>
                  <nts id="Seg_1535" n="HIAT:ip">,</nts>
                  <nts id="Seg_1536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1538" n="HIAT:w" s="T388">kannagɨnɨj</ts>
                  <nts id="Seg_1539" n="HIAT:ip">?</nts>
                  <nts id="Seg_1540" n="HIAT:ip">"</nts>
                  <nts id="Seg_1541" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T391" id="Seg_1543" n="HIAT:u" s="T389">
                  <nts id="Seg_1544" n="HIAT:ip">"</nts>
                  <ts e="T390" id="Seg_1546" n="HIAT:w" s="T389">Ü͡öleske</ts>
                  <nts id="Seg_1547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1549" n="HIAT:w" s="T390">baːrbɨn</ts>
                  <nts id="Seg_1550" n="HIAT:ip">!</nts>
                  <nts id="Seg_1551" n="HIAT:ip">"</nts>
                  <nts id="Seg_1552" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T397" id="Seg_1554" n="HIAT:u" s="T391">
                  <ts e="T392" id="Seg_1556" n="HIAT:w" s="T391">Ü͡öleske</ts>
                  <nts id="Seg_1557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1559" n="HIAT:w" s="T392">körüleːbite</ts>
                  <nts id="Seg_1560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1561" n="HIAT:ip">—</nts>
                  <nts id="Seg_1562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1564" n="HIAT:w" s="T393">Lɨːbɨra</ts>
                  <nts id="Seg_1565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1567" n="HIAT:w" s="T394">kanna</ts>
                  <nts id="Seg_1568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1570" n="HIAT:w" s="T395">da</ts>
                  <nts id="Seg_1571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1573" n="HIAT:w" s="T396">hu͡ok</ts>
                  <nts id="Seg_1574" n="HIAT:ip">.</nts>
                  <nts id="Seg_1575" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T401" id="Seg_1577" n="HIAT:u" s="T397">
                  <nts id="Seg_1578" n="HIAT:ip">"</nts>
                  <ts e="T398" id="Seg_1580" n="HIAT:w" s="T397">Lɨːbɨraː</ts>
                  <nts id="Seg_1581" n="HIAT:ip">,</nts>
                  <nts id="Seg_1582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1584" n="HIAT:w" s="T398">kanna</ts>
                  <nts id="Seg_1585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1587" n="HIAT:w" s="T399">baːrgɨnɨj</ts>
                  <nts id="Seg_1588" n="HIAT:ip">"</nts>
                  <nts id="Seg_1589" n="HIAT:ip">,</nts>
                  <nts id="Seg_1590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1592" n="HIAT:w" s="T400">di͡ebit</ts>
                  <nts id="Seg_1593" n="HIAT:ip">.</nts>
                  <nts id="Seg_1594" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T402" id="Seg_1596" n="HIAT:u" s="T401">
                  <ts e="T402" id="Seg_1598" n="HIAT:w" s="T401">Lɨːbɨra</ts>
                  <nts id="Seg_1599" n="HIAT:ip">:</nts>
                  <nts id="Seg_1600" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T406" id="Seg_1602" n="HIAT:u" s="T402">
                  <nts id="Seg_1603" n="HIAT:ip">"</nts>
                  <ts e="T403" id="Seg_1605" n="HIAT:w" s="T402">Ohok</ts>
                  <nts id="Seg_1606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1608" n="HIAT:w" s="T403">annɨgar</ts>
                  <nts id="Seg_1609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1611" n="HIAT:w" s="T404">baːrbɨn</ts>
                  <nts id="Seg_1612" n="HIAT:ip">"</nts>
                  <nts id="Seg_1613" n="HIAT:ip">,</nts>
                  <nts id="Seg_1614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1616" n="HIAT:w" s="T405">di͡ebit</ts>
                  <nts id="Seg_1617" n="HIAT:ip">.</nts>
                  <nts id="Seg_1618" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T413" id="Seg_1620" n="HIAT:u" s="T406">
                  <ts e="T407" id="Seg_1622" n="HIAT:w" s="T406">Onu</ts>
                  <nts id="Seg_1623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407.tx.1" id="Seg_1625" n="HIAT:w" s="T407">Aŋaː</ts>
                  <nts id="Seg_1626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1628" n="HIAT:w" s="T407.tx.1">Moŋus</ts>
                  <nts id="Seg_1629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1631" n="HIAT:w" s="T408">ihitte</ts>
                  <nts id="Seg_1632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1634" n="HIAT:w" s="T409">da</ts>
                  <nts id="Seg_1635" n="HIAT:ip">,</nts>
                  <nts id="Seg_1636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1638" n="HIAT:w" s="T410">menʼiːtinen</ts>
                  <nts id="Seg_1639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1641" n="HIAT:w" s="T411">kiːreːri</ts>
                  <nts id="Seg_1642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1644" n="HIAT:w" s="T412">gɨmmɨt</ts>
                  <nts id="Seg_1645" n="HIAT:ip">.</nts>
                  <nts id="Seg_1646" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T416" id="Seg_1648" n="HIAT:u" s="T413">
                  <ts e="T414" id="Seg_1650" n="HIAT:w" s="T413">Onuga</ts>
                  <nts id="Seg_1651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1653" n="HIAT:w" s="T414">Lɨːbɨra</ts>
                  <nts id="Seg_1654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1656" n="HIAT:w" s="T415">hanarar</ts>
                  <nts id="Seg_1657" n="HIAT:ip">:</nts>
                  <nts id="Seg_1658" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T421" id="Seg_1660" n="HIAT:u" s="T416">
                  <nts id="Seg_1661" n="HIAT:ip">"</nts>
                  <ts e="T417" id="Seg_1663" n="HIAT:w" s="T416">Eheː</ts>
                  <nts id="Seg_1664" n="HIAT:ip">,</nts>
                  <nts id="Seg_1665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1667" n="HIAT:w" s="T417">menʼiːginen</ts>
                  <nts id="Seg_1668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1670" n="HIAT:w" s="T418">kiːrime</ts>
                  <nts id="Seg_1671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1672" n="HIAT:ip">—</nts>
                  <nts id="Seg_1673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1675" n="HIAT:w" s="T419">batɨ͡aŋ</ts>
                  <nts id="Seg_1676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1678" n="HIAT:w" s="T420">hu͡oga</ts>
                  <nts id="Seg_1679" n="HIAT:ip">.</nts>
                  <nts id="Seg_1680" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T423" id="Seg_1682" n="HIAT:u" s="T421">
                  <ts e="T422" id="Seg_1684" n="HIAT:w" s="T421">Emeheginen</ts>
                  <nts id="Seg_1685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1687" n="HIAT:w" s="T422">kiːr</ts>
                  <nts id="Seg_1688" n="HIAT:ip">.</nts>
                  <nts id="Seg_1689" n="HIAT:ip">"</nts>
                  <nts id="Seg_1690" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T426" id="Seg_1692" n="HIAT:u" s="T423">
                  <ts e="T423.tx.1" id="Seg_1694" n="HIAT:w" s="T423">Aŋaː</ts>
                  <nts id="Seg_1695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1697" n="HIAT:w" s="T423.tx.1">Moŋus</ts>
                  <nts id="Seg_1698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1700" n="HIAT:w" s="T424">emehetinen</ts>
                  <nts id="Seg_1701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1703" n="HIAT:w" s="T425">kiːrbit</ts>
                  <nts id="Seg_1704" n="HIAT:ip">.</nts>
                  <nts id="Seg_1705" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T436" id="Seg_1707" n="HIAT:u" s="T426">
                  <ts e="T427" id="Seg_1709" n="HIAT:w" s="T426">Ol</ts>
                  <nts id="Seg_1710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1712" n="HIAT:w" s="T427">kiːren</ts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1715" n="HIAT:w" s="T428">erdegine</ts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1718" n="HIAT:w" s="T429">Lɨːbɨra</ts>
                  <nts id="Seg_1719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1721" n="HIAT:w" s="T430">kɨtarčɨ</ts>
                  <nts id="Seg_1722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1724" n="HIAT:w" s="T431">barbɨt</ts>
                  <nts id="Seg_1725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1727" n="HIAT:w" s="T432">korgoldʼiːnɨnan</ts>
                  <nts id="Seg_1728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1730" n="HIAT:w" s="T433">emehetin</ts>
                  <nts id="Seg_1731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1733" n="HIAT:w" s="T434">ihinen</ts>
                  <nts id="Seg_1734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1736" n="HIAT:w" s="T435">aspɨt</ts>
                  <nts id="Seg_1737" n="HIAT:ip">.</nts>
                  <nts id="Seg_1738" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T439" id="Seg_1740" n="HIAT:u" s="T436">
                  <ts e="T436.tx.1" id="Seg_1742" n="HIAT:w" s="T436">Aŋaː</ts>
                  <nts id="Seg_1743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1745" n="HIAT:w" s="T436.tx.1">Moŋus</ts>
                  <nts id="Seg_1746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1748" n="HIAT:w" s="T437">ölön</ts>
                  <nts id="Seg_1749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1751" n="HIAT:w" s="T438">kaːlbɨt</ts>
                  <nts id="Seg_1752" n="HIAT:ip">.</nts>
                  <nts id="Seg_1753" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T445" id="Seg_1755" n="HIAT:u" s="T439">
                  <ts e="T440" id="Seg_1757" n="HIAT:w" s="T439">Lɨːbɨra</ts>
                  <nts id="Seg_1758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1760" n="HIAT:w" s="T440">tagɨsta</ts>
                  <nts id="Seg_1761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1763" n="HIAT:w" s="T441">da</ts>
                  <nts id="Seg_1764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1766" n="HIAT:w" s="T442">dʼi͡etiger</ts>
                  <nts id="Seg_1767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1769" n="HIAT:w" s="T443">baran</ts>
                  <nts id="Seg_1770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1772" n="HIAT:w" s="T444">kaːlbɨt</ts>
                  <nts id="Seg_1773" n="HIAT:ip">.</nts>
                  <nts id="Seg_1774" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T448" id="Seg_1776" n="HIAT:u" s="T445">
                  <ts e="T446" id="Seg_1778" n="HIAT:w" s="T445">Onon</ts>
                  <nts id="Seg_1779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1781" n="HIAT:w" s="T446">bajan-tajan</ts>
                  <nts id="Seg_1782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1784" n="HIAT:w" s="T447">olorbut</ts>
                  <nts id="Seg_1785" n="HIAT:ip">.</nts>
                  <nts id="Seg_1786" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T450" id="Seg_1788" n="HIAT:u" s="T448">
                  <ts e="T449" id="Seg_1790" n="HIAT:w" s="T448">Dʼe</ts>
                  <nts id="Seg_1791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1793" n="HIAT:w" s="T449">bütte</ts>
                  <nts id="Seg_1794" n="HIAT:ip">.</nts>
                  <nts id="Seg_1795" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T450" id="Seg_1796" n="sc" s="T0">
               <ts e="T1" id="Seg_1798" n="e" s="T0">Lɨːbɨra </ts>
               <ts e="T2" id="Seg_1800" n="e" s="T1">erden </ts>
               <ts e="T3" id="Seg_1802" n="e" s="T2">kü͡ögeldʼeten </ts>
               <ts e="T4" id="Seg_1804" n="e" s="T3">iheːktiːr </ts>
               <ts e="T5" id="Seg_1806" n="e" s="T4">daː. </ts>
               <ts e="T6" id="Seg_1808" n="e" s="T5">Ihen-ihen </ts>
               <ts e="T7" id="Seg_1810" n="e" s="T6">taːska </ts>
               <ts e="T8" id="Seg_1812" n="e" s="T7">keppit, </ts>
               <ts e="T9" id="Seg_1814" n="e" s="T8">tɨːta </ts>
               <ts e="T10" id="Seg_1816" n="e" s="T9">hɨstan </ts>
               <ts e="T11" id="Seg_1818" n="e" s="T10">kaːlbɨt. </ts>
               <ts e="T12" id="Seg_1820" n="e" s="T11">Erdiːtinen </ts>
               <ts e="T13" id="Seg_1822" n="e" s="T12">oksubut </ts>
               <ts e="T14" id="Seg_1824" n="e" s="T13">erdiːte </ts>
               <ts e="T15" id="Seg_1826" n="e" s="T14">hɨstan </ts>
               <ts e="T16" id="Seg_1828" n="e" s="T15">kaːlbɨt. </ts>
               <ts e="T17" id="Seg_1830" n="e" s="T16">Iliːtinen </ts>
               <ts e="T18" id="Seg_1832" n="e" s="T17">oksubut </ts>
               <ts e="T19" id="Seg_1834" n="e" s="T18">iliːte </ts>
               <ts e="T20" id="Seg_1836" n="e" s="T19">hɨstan </ts>
               <ts e="T21" id="Seg_1838" n="e" s="T20">kaːlbɨt. </ts>
               <ts e="T22" id="Seg_1840" n="e" s="T21">Biːrges </ts>
               <ts e="T23" id="Seg_1842" n="e" s="T22">iliːtinen </ts>
               <ts e="T24" id="Seg_1844" n="e" s="T23">oksor </ts>
               <ts e="T25" id="Seg_1846" n="e" s="T24">biːrgehe </ts>
               <ts e="T26" id="Seg_1848" n="e" s="T25">emi͡e </ts>
               <ts e="T27" id="Seg_1850" n="e" s="T26">hɨstan </ts>
               <ts e="T28" id="Seg_1852" n="e" s="T27">kaːlbɨt. </ts>
               <ts e="T29" id="Seg_1854" n="e" s="T28">Atagɨnan </ts>
               <ts e="T30" id="Seg_1856" n="e" s="T29">teppit </ts>
               <ts e="T31" id="Seg_1858" n="e" s="T30">ataga </ts>
               <ts e="T32" id="Seg_1860" n="e" s="T31">hɨstan </ts>
               <ts e="T33" id="Seg_1862" n="e" s="T32">kaːlbɨt, </ts>
               <ts e="T34" id="Seg_1864" n="e" s="T33">nöŋü͡ö </ts>
               <ts e="T35" id="Seg_1866" n="e" s="T34">atagɨnan </ts>
               <ts e="T36" id="Seg_1868" n="e" s="T35">teppite </ts>
               <ts e="T37" id="Seg_1870" n="e" s="T36">emi͡e </ts>
               <ts e="T38" id="Seg_1872" n="e" s="T37">hɨstan </ts>
               <ts e="T39" id="Seg_1874" n="e" s="T38">kaːlbɨt. </ts>
               <ts e="T40" id="Seg_1876" n="e" s="T39">Bɨ͡arɨnan </ts>
               <ts e="T41" id="Seg_1878" n="e" s="T40">üppüte </ts>
               <ts e="T42" id="Seg_1880" n="e" s="T41">bɨ͡ara </ts>
               <ts e="T43" id="Seg_1882" n="e" s="T42">emi͡e </ts>
               <ts e="T44" id="Seg_1884" n="e" s="T43">hɨstan </ts>
               <ts e="T45" id="Seg_1886" n="e" s="T44">kaːlbɨt. </ts>
               <ts e="T46" id="Seg_1888" n="e" s="T45">Onton </ts>
               <ts e="T47" id="Seg_1890" n="e" s="T46">menʼiːtinen </ts>
               <ts e="T48" id="Seg_1892" n="e" s="T47">oksubut </ts>
               <ts e="T49" id="Seg_1894" n="e" s="T48">menʼiːte </ts>
               <ts e="T50" id="Seg_1896" n="e" s="T49">emi͡e </ts>
               <ts e="T51" id="Seg_1898" n="e" s="T50">hɨstan </ts>
               <ts e="T52" id="Seg_1900" n="e" s="T51">kaːlbɨt. </ts>
               <ts e="T53" id="Seg_1902" n="e" s="T52">Hɨstan </ts>
               <ts e="T54" id="Seg_1904" n="e" s="T53">turdaga. </ts>
               <ts e="T55" id="Seg_1906" n="e" s="T54">Ol </ts>
               <ts e="T56" id="Seg_1908" n="e" s="T55">turdagɨn </ts>
               <ts e="T57" id="Seg_1910" n="e" s="T56">Aŋaː Moŋus </ts>
               <ts e="T58" id="Seg_1912" n="e" s="T57">kelbit. </ts>
               <ts e="T59" id="Seg_1914" n="e" s="T58">"Anʼɨkataːn! </ts>
               <ts e="T60" id="Seg_1916" n="e" s="T59">Ki͡eheːŋŋi </ts>
               <ts e="T61" id="Seg_1918" n="e" s="T60">hokuːskalaːk </ts>
               <ts e="T62" id="Seg_1920" n="e" s="T61">bu͡ollum", </ts>
               <ts e="T63" id="Seg_1922" n="e" s="T62">di͡ebitinen </ts>
               <ts e="T64" id="Seg_1924" n="e" s="T63">Lɨːbɨranɨ </ts>
               <ts e="T65" id="Seg_1926" n="e" s="T64">ɨlan </ts>
               <ts e="T66" id="Seg_1928" n="e" s="T65">karmaːnɨgar </ts>
               <ts e="T67" id="Seg_1930" n="e" s="T66">uktan </ts>
               <ts e="T68" id="Seg_1932" n="e" s="T67">keːspit. </ts>
               <ts e="T69" id="Seg_1934" n="e" s="T68">Dʼi͡etiger </ts>
               <ts e="T70" id="Seg_1936" n="e" s="T69">barbɨt. </ts>
               <ts e="T71" id="Seg_1938" n="e" s="T70">Orogugar </ts>
               <ts e="T72" id="Seg_1940" n="e" s="T71">tijen </ts>
               <ts e="T73" id="Seg_1942" n="e" s="T72">Aŋaː Moŋus </ts>
               <ts e="T74" id="Seg_1944" n="e" s="T73">haːktɨ͡an </ts>
               <ts e="T75" id="Seg_1946" n="e" s="T74">bagarbɨt. </ts>
               <ts e="T76" id="Seg_1948" n="e" s="T75">Ol </ts>
               <ts e="T77" id="Seg_1950" n="e" s="T76">haːktɨː </ts>
               <ts e="T78" id="Seg_1952" n="e" s="T77">olordoguna, </ts>
               <ts e="T79" id="Seg_1954" n="e" s="T78">Lɨːbɨra </ts>
               <ts e="T80" id="Seg_1956" n="e" s="T79">karmaːnɨn </ts>
               <ts e="T81" id="Seg_1958" n="e" s="T80">kajagahɨn </ts>
               <ts e="T82" id="Seg_1960" n="e" s="T81">üstün </ts>
               <ts e="T83" id="Seg_1962" n="e" s="T82">tühen </ts>
               <ts e="T84" id="Seg_1964" n="e" s="T83">kaːlbɨt. </ts>
               <ts e="T85" id="Seg_1966" n="e" s="T84">Aŋaː Moŋus </ts>
               <ts e="T86" id="Seg_1968" n="e" s="T85">tugu </ts>
               <ts e="T87" id="Seg_1970" n="e" s="T86">da </ts>
               <ts e="T88" id="Seg_1972" n="e" s="T87">bilbekke </ts>
               <ts e="T89" id="Seg_1974" n="e" s="T88">hɨ͡alɨjatɨn </ts>
               <ts e="T90" id="Seg_1976" n="e" s="T89">kötögünne </ts>
               <ts e="T91" id="Seg_1978" n="e" s="T90">da </ts>
               <ts e="T92" id="Seg_1980" n="e" s="T91">dʼi͡etiger </ts>
               <ts e="T93" id="Seg_1982" n="e" s="T92">barbɨt. </ts>
               <ts e="T94" id="Seg_1984" n="e" s="T93">Dʼi͡etiger </ts>
               <ts e="T95" id="Seg_1986" n="e" s="T94">tahɨgar </ts>
               <ts e="T96" id="Seg_1988" n="e" s="T95">honun </ts>
               <ts e="T97" id="Seg_1990" n="e" s="T96">araŋaska </ts>
               <ts e="T98" id="Seg_1992" n="e" s="T97">ɨjaːn </ts>
               <ts e="T99" id="Seg_1994" n="e" s="T98">baran </ts>
               <ts e="T100" id="Seg_1996" n="e" s="T99">kiːrbit. </ts>
               <ts e="T101" id="Seg_1998" n="e" s="T100">Emeːksine </ts>
               <ts e="T102" id="Seg_2000" n="e" s="T101">iːstene </ts>
               <ts e="T103" id="Seg_2002" n="e" s="T102">oloror </ts>
               <ts e="T104" id="Seg_2004" n="e" s="T103">ebit. </ts>
               <ts e="T105" id="Seg_2006" n="e" s="T104">"Emeːksi͡en, </ts>
               <ts e="T106" id="Seg_2008" n="e" s="T105">kehiːbin </ts>
               <ts e="T107" id="Seg_2010" n="e" s="T106">egellim. </ts>
               <ts e="T108" id="Seg_2012" n="e" s="T107">Honum </ts>
               <ts e="T109" id="Seg_2014" n="e" s="T108">karmaːnɨgar </ts>
               <ts e="T110" id="Seg_2016" n="e" s="T109">baːr </ts>
               <ts e="T111" id="Seg_2018" n="e" s="T110">tahaːra, </ts>
               <ts e="T112" id="Seg_2020" n="e" s="T111">onu </ts>
               <ts e="T113" id="Seg_2022" n="e" s="T112">killeren </ts>
               <ts e="T114" id="Seg_2024" n="e" s="T113">kör", </ts>
               <ts e="T115" id="Seg_2026" n="e" s="T114">di͡ebit. </ts>
               <ts e="T116" id="Seg_2028" n="e" s="T115">Emeːksine </ts>
               <ts e="T117" id="Seg_2030" n="e" s="T116">ü͡örüːtüger </ts>
               <ts e="T118" id="Seg_2032" n="e" s="T117">iŋneleːk </ts>
               <ts e="T119" id="Seg_2034" n="e" s="T118">hüːtügün </ts>
               <ts e="T120" id="Seg_2036" n="e" s="T119">ɨjɨran </ts>
               <ts e="T121" id="Seg_2038" n="e" s="T120">keːspit, </ts>
               <ts e="T122" id="Seg_2040" n="e" s="T121">taksa </ts>
               <ts e="T123" id="Seg_2042" n="e" s="T122">köppüt. </ts>
               <ts e="T124" id="Seg_2044" n="e" s="T123">Bulbakka </ts>
               <ts e="T125" id="Seg_2046" n="e" s="T124">tönnön </ts>
               <ts e="T126" id="Seg_2048" n="e" s="T125">kiːrbit: </ts>
               <ts e="T127" id="Seg_2050" n="e" s="T126">"Karmaːŋŋar </ts>
               <ts e="T128" id="Seg_2052" n="e" s="T127">tu͡ok </ts>
               <ts e="T129" id="Seg_2054" n="e" s="T128">da </ts>
               <ts e="T130" id="Seg_2056" n="e" s="T129">hu͡ok", </ts>
               <ts e="T131" id="Seg_2058" n="e" s="T130">di͡ebit. </ts>
               <ts e="T132" id="Seg_2060" n="e" s="T131">Aŋaː Moŋus: </ts>
               <ts e="T133" id="Seg_2062" n="e" s="T132">"Bejeŋ </ts>
               <ts e="T134" id="Seg_2064" n="e" s="T133">hi͡etegiŋ </ts>
               <ts e="T135" id="Seg_2066" n="e" s="T134">diːn", </ts>
               <ts e="T136" id="Seg_2068" n="e" s="T135">di͡ebit </ts>
               <ts e="T137" id="Seg_2070" n="e" s="T136">da, </ts>
               <ts e="T138" id="Seg_2072" n="e" s="T137">emeːksin </ts>
               <ts e="T139" id="Seg_2074" n="e" s="T138">bɨ͡arɨn </ts>
               <ts e="T140" id="Seg_2076" n="e" s="T139">kaja </ts>
               <ts e="T141" id="Seg_2078" n="e" s="T140">tardar. </ts>
               <ts e="T142" id="Seg_2080" n="e" s="T141">Emeːksin </ts>
               <ts e="T143" id="Seg_2082" n="e" s="T142">bɨ͡arɨttan </ts>
               <ts e="T144" id="Seg_2084" n="e" s="T143">iŋneleːk </ts>
               <ts e="T145" id="Seg_2086" n="e" s="T144">hüːtük </ts>
               <ts e="T146" id="Seg_2088" n="e" s="T145">kɨlɨr </ts>
               <ts e="T147" id="Seg_2090" n="e" s="T146">gɨna </ts>
               <ts e="T148" id="Seg_2092" n="e" s="T147">tüspüt. </ts>
               <ts e="T149" id="Seg_2094" n="e" s="T148">"Oː, </ts>
               <ts e="T150" id="Seg_2096" n="e" s="T149">kaːrɨ͡an </ts>
               <ts e="T151" id="Seg_2098" n="e" s="T150">dʼaktarbɨn", </ts>
               <ts e="T152" id="Seg_2100" n="e" s="T151">üːgüleːbit </ts>
               <ts e="T153" id="Seg_2102" n="e" s="T152">Aŋaː Moŋus. </ts>
               <ts e="T154" id="Seg_2104" n="e" s="T153">Töttörü </ts>
               <ts e="T155" id="Seg_2106" n="e" s="T154">hüːrbüt. </ts>
               <ts e="T156" id="Seg_2108" n="e" s="T155">Lɨːbɨra </ts>
               <ts e="T157" id="Seg_2110" n="e" s="T156">orokko </ts>
               <ts e="T158" id="Seg_2112" n="e" s="T157">bu </ts>
               <ts e="T159" id="Seg_2114" n="e" s="T158">čaŋkaja </ts>
               <ts e="T160" id="Seg_2116" n="e" s="T159">hɨtar. </ts>
               <ts e="T161" id="Seg_2118" n="e" s="T160">ɨlar </ts>
               <ts e="T162" id="Seg_2120" n="e" s="T161">da </ts>
               <ts e="T163" id="Seg_2122" n="e" s="T162">karmaːnɨgar </ts>
               <ts e="T164" id="Seg_2124" n="e" s="T163">uktar. </ts>
               <ts e="T165" id="Seg_2126" n="e" s="T164">Töttörü </ts>
               <ts e="T166" id="Seg_2128" n="e" s="T165">hüːren </ts>
               <ts e="T167" id="Seg_2130" n="e" s="T166">keler. </ts>
               <ts e="T168" id="Seg_2132" n="e" s="T167">Dʼi͡etiger </ts>
               <ts e="T169" id="Seg_2134" n="e" s="T168">kiːren </ts>
               <ts e="T170" id="Seg_2136" n="e" s="T169">Lɨːbɨranɨ </ts>
               <ts e="T171" id="Seg_2138" n="e" s="T170">iːkeːptiːŋŋe </ts>
               <ts e="T172" id="Seg_2140" n="e" s="T171">ɨjaːn </ts>
               <ts e="T173" id="Seg_2142" n="e" s="T172">keːher. </ts>
               <ts e="T174" id="Seg_2144" n="e" s="T173">Aŋaː Moŋus </ts>
               <ts e="T175" id="Seg_2146" n="e" s="T174">hette </ts>
               <ts e="T176" id="Seg_2148" n="e" s="T175">tarakaːj </ts>
               <ts e="T177" id="Seg_2150" n="e" s="T176">ogoloːk </ts>
               <ts e="T178" id="Seg_2152" n="e" s="T177">ebit. </ts>
               <ts e="T179" id="Seg_2154" n="e" s="T178">Ol </ts>
               <ts e="T180" id="Seg_2156" n="e" s="T179">ogolorukar </ts>
               <ts e="T181" id="Seg_2158" n="e" s="T180">di͡ebit: </ts>
               <ts e="T182" id="Seg_2160" n="e" s="T181">"Ogoloːr, </ts>
               <ts e="T183" id="Seg_2162" n="e" s="T182">Lɨːbɨra </ts>
               <ts e="T184" id="Seg_2164" n="e" s="T183">hɨ͡ata </ts>
               <ts e="T185" id="Seg_2166" n="e" s="T184">testi͡ege — </ts>
               <ts e="T186" id="Seg_2168" n="e" s="T185">körör </ts>
               <ts e="T187" id="Seg_2170" n="e" s="T186">bu͡oluŋ. </ts>
               <ts e="T188" id="Seg_2172" n="e" s="T187">Kelerber </ts>
               <ts e="T189" id="Seg_2174" n="e" s="T188">kü͡östeːriŋ!" </ts>
               <ts e="T190" id="Seg_2176" n="e" s="T189">Di͡ete </ts>
               <ts e="T191" id="Seg_2178" n="e" s="T190">da </ts>
               <ts e="T192" id="Seg_2180" n="e" s="T191">taksan </ts>
               <ts e="T193" id="Seg_2182" n="e" s="T192">kaːlbɨt. </ts>
               <ts e="T194" id="Seg_2184" n="e" s="T193">Töhö </ts>
               <ts e="T195" id="Seg_2186" n="e" s="T194">da </ts>
               <ts e="T196" id="Seg_2188" n="e" s="T195">bu͡olbakka </ts>
               <ts e="T197" id="Seg_2190" n="e" s="T196">Lɨːbɨra </ts>
               <ts e="T198" id="Seg_2192" n="e" s="T197">dʼe </ts>
               <ts e="T199" id="Seg_2194" n="e" s="T198">iːkteːbit. </ts>
               <ts e="T200" id="Seg_2196" n="e" s="T199">Ogolor </ts>
               <ts e="T201" id="Seg_2198" n="e" s="T200">onno </ts>
               <ts e="T202" id="Seg_2200" n="e" s="T201">üːgülespitter: </ts>
               <ts e="T203" id="Seg_2202" n="e" s="T202">"Anʼikataːn! </ts>
               <ts e="T204" id="Seg_2204" n="e" s="T203">Lɨːbɨra </ts>
               <ts e="T205" id="Seg_2206" n="e" s="T204">ɨbagas </ts>
               <ts e="T206" id="Seg_2208" n="e" s="T205">hɨ͡ata </ts>
               <ts e="T207" id="Seg_2210" n="e" s="T206">togunna!" </ts>
               <ts e="T208" id="Seg_2212" n="e" s="T207">Onton </ts>
               <ts e="T209" id="Seg_2214" n="e" s="T208">Lɨːbɨra </ts>
               <ts e="T210" id="Seg_2216" n="e" s="T209">dʼe </ts>
               <ts e="T211" id="Seg_2218" n="e" s="T210">haːktɨːr. </ts>
               <ts e="T212" id="Seg_2220" n="e" s="T211">Ogolor </ts>
               <ts e="T213" id="Seg_2222" n="e" s="T212">üːgülespitter: </ts>
               <ts e="T214" id="Seg_2224" n="e" s="T213">"Anʼikataːn! </ts>
               <ts e="T215" id="Seg_2226" n="e" s="T214">Lɨːbɨra </ts>
               <ts e="T216" id="Seg_2228" n="e" s="T215">kojuː </ts>
               <ts e="T217" id="Seg_2230" n="e" s="T216">hɨ͡ata </ts>
               <ts e="T218" id="Seg_2232" n="e" s="T217">togunna!" </ts>
               <ts e="T219" id="Seg_2234" n="e" s="T218">Lɨːbɨra </ts>
               <ts e="T220" id="Seg_2236" n="e" s="T219">ogolorgo </ts>
               <ts e="T221" id="Seg_2238" n="e" s="T220">di͡ebit: </ts>
               <ts e="T222" id="Seg_2240" n="e" s="T221">"Ogoloːr! </ts>
               <ts e="T223" id="Seg_2242" n="e" s="T222">Tüheriŋ </ts>
               <ts e="T224" id="Seg_2244" n="e" s="T223">miːgin. </ts>
               <ts e="T225" id="Seg_2246" n="e" s="T224">Min </ts>
               <ts e="T226" id="Seg_2248" n="e" s="T225">ehi͡eke </ts>
               <ts e="T227" id="Seg_2250" n="e" s="T226">lu͡osku </ts>
               <ts e="T228" id="Seg_2252" n="e" s="T227">oŋoron </ts>
               <ts e="T229" id="Seg_2254" n="e" s="T228">bi͡eri͡em — </ts>
               <ts e="T230" id="Seg_2256" n="e" s="T229">min </ts>
               <ts e="T231" id="Seg_2258" n="e" s="T230">hɨ͡abɨn </ts>
               <ts e="T232" id="Seg_2260" n="e" s="T231">hiːrgitiger </ts>
               <ts e="T233" id="Seg_2262" n="e" s="T232">üčügej </ts>
               <ts e="T234" id="Seg_2264" n="e" s="T233">bu͡olu͡o!" </ts>
               <ts e="T235" id="Seg_2266" n="e" s="T234">"Kaja, </ts>
               <ts e="T236" id="Seg_2268" n="e" s="T235">erej </ts>
               <ts e="T237" id="Seg_2270" n="e" s="T236">keje!" </ts>
               <ts e="T238" id="Seg_2272" n="e" s="T237">Lɨːbɨranɨ </ts>
               <ts e="T239" id="Seg_2274" n="e" s="T238">tüherbitter. </ts>
               <ts e="T240" id="Seg_2276" n="e" s="T239">Lɨːbɨra </ts>
               <ts e="T241" id="Seg_2278" n="e" s="T240">di͡ebit: </ts>
               <ts e="T242" id="Seg_2280" n="e" s="T241">"Egeliŋ </ts>
               <ts e="T243" id="Seg_2282" n="e" s="T242">teːtegit </ts>
               <ts e="T244" id="Seg_2284" n="e" s="T243">hɨtɨː </ts>
               <ts e="T245" id="Seg_2286" n="e" s="T244">hügetin!" </ts>
               <ts e="T246" id="Seg_2288" n="e" s="T245">Egelen </ts>
               <ts e="T247" id="Seg_2290" n="e" s="T246">bi͡erbitter. </ts>
               <ts e="T248" id="Seg_2292" n="e" s="T247">"Oroŋŋutugar </ts>
               <ts e="T249" id="Seg_2294" n="e" s="T248">körö </ts>
               <ts e="T250" id="Seg_2296" n="e" s="T249">hɨtɨŋ — </ts>
               <ts e="T251" id="Seg_2298" n="e" s="T250">kajdak </ts>
               <ts e="T252" id="Seg_2300" n="e" s="T251">min </ts>
               <ts e="T253" id="Seg_2302" n="e" s="T252">oŋorobun." </ts>
               <ts e="T254" id="Seg_2304" n="e" s="T253">Ogolor </ts>
               <ts e="T255" id="Seg_2306" n="e" s="T254">hɨtan </ts>
               <ts e="T256" id="Seg_2308" n="e" s="T255">kaːlbɨttar. </ts>
               <ts e="T257" id="Seg_2310" n="e" s="T256">Ol </ts>
               <ts e="T258" id="Seg_2312" n="e" s="T257">hɨppɨttarɨgar </ts>
               <ts e="T259" id="Seg_2314" n="e" s="T258">Lɨːbɨra </ts>
               <ts e="T260" id="Seg_2316" n="e" s="T259">menʼiːlerin </ts>
               <ts e="T261" id="Seg_2318" n="e" s="T260">bɨha </ts>
               <ts e="T262" id="Seg_2320" n="e" s="T261">kerditeleːbit. </ts>
               <ts e="T263" id="Seg_2322" n="e" s="T262">Etterin </ts>
               <ts e="T264" id="Seg_2324" n="e" s="T263">kü͡östeːn </ts>
               <ts e="T265" id="Seg_2326" n="e" s="T264">keːspit. </ts>
               <ts e="T266" id="Seg_2328" n="e" s="T265">Menʼiːleri </ts>
               <ts e="T267" id="Seg_2330" n="e" s="T266">oroŋŋo </ts>
               <ts e="T268" id="Seg_2332" n="e" s="T267">kečigireččikeːn </ts>
               <ts e="T269" id="Seg_2334" n="e" s="T268">uːran </ts>
               <ts e="T270" id="Seg_2336" n="e" s="T269">baran </ts>
               <ts e="T271" id="Seg_2338" n="e" s="T270">hu͡organɨnan </ts>
               <ts e="T272" id="Seg_2340" n="e" s="T271">habɨtalaːbɨt. </ts>
               <ts e="T273" id="Seg_2342" n="e" s="T272">Korgoldʼiːnɨ </ts>
               <ts e="T274" id="Seg_2344" n="e" s="T273">ɨlan </ts>
               <ts e="T275" id="Seg_2346" n="e" s="T274">baran </ts>
               <ts e="T276" id="Seg_2348" n="e" s="T275">ohok </ts>
               <ts e="T277" id="Seg_2350" n="e" s="T276">annɨgar </ts>
               <ts e="T278" id="Seg_2352" n="e" s="T277">kiːren </ts>
               <ts e="T279" id="Seg_2354" n="e" s="T278">kaːlbɨt. </ts>
               <ts e="T280" id="Seg_2356" n="e" s="T279">Bu </ts>
               <ts e="T281" id="Seg_2358" n="e" s="T280">hɨttagɨna </ts>
               <ts e="T282" id="Seg_2360" n="e" s="T281">Aŋaː Moŋus </ts>
               <ts e="T283" id="Seg_2362" n="e" s="T282">kelbit. </ts>
               <ts e="T284" id="Seg_2364" n="e" s="T283">Aŋaː Moŋus </ts>
               <ts e="T285" id="Seg_2366" n="e" s="T284">kelen </ts>
               <ts e="T286" id="Seg_2368" n="e" s="T285">üːgüleːbit: </ts>
               <ts e="T287" id="Seg_2370" n="e" s="T286">"Ogoloːr! </ts>
               <ts e="T288" id="Seg_2372" n="e" s="T287">Aːŋŋɨtɨn </ts>
               <ts e="T289" id="Seg_2374" n="e" s="T288">arɨjɨŋ!" </ts>
               <ts e="T290" id="Seg_2376" n="e" s="T289">Kim </ts>
               <ts e="T291" id="Seg_2378" n="e" s="T290">da </ts>
               <ts e="T292" id="Seg_2380" n="e" s="T291">arɨjbat. </ts>
               <ts e="T293" id="Seg_2382" n="e" s="T292">Kasta </ts>
               <ts e="T294" id="Seg_2384" n="e" s="T293">da </ts>
               <ts e="T295" id="Seg_2386" n="e" s="T294">üːgüleːbit. </ts>
               <ts e="T296" id="Seg_2388" n="e" s="T295">Ogolor </ts>
               <ts e="T297" id="Seg_2390" n="e" s="T296">törüt </ts>
               <ts e="T298" id="Seg_2392" n="e" s="T297">arɨjbataktar. </ts>
               <ts e="T299" id="Seg_2394" n="e" s="T298">Aːnɨn </ts>
               <ts e="T300" id="Seg_2396" n="e" s="T299">kostuː </ts>
               <ts e="T301" id="Seg_2398" n="e" s="T300">tardan </ts>
               <ts e="T302" id="Seg_2400" n="e" s="T301">kiːrbit. </ts>
               <ts e="T303" id="Seg_2402" n="e" s="T302">Körbüte — </ts>
               <ts e="T304" id="Seg_2404" n="e" s="T303">et </ts>
               <ts e="T305" id="Seg_2406" n="e" s="T304">buha </ts>
               <ts e="T306" id="Seg_2408" n="e" s="T305">turar, </ts>
               <ts e="T307" id="Seg_2410" n="e" s="T306">ogolor </ts>
               <ts e="T308" id="Seg_2412" n="e" s="T307">utuja </ts>
               <ts e="T309" id="Seg_2414" n="e" s="T308">hɨtallar. </ts>
               <ts e="T310" id="Seg_2416" n="e" s="T309">"Oː, </ts>
               <ts e="T311" id="Seg_2418" n="e" s="T310">ogolorum </ts>
               <ts e="T312" id="Seg_2420" n="e" s="T311">Lɨːbɨra </ts>
               <ts e="T313" id="Seg_2422" n="e" s="T312">hɨ͡atɨttan </ts>
               <ts e="T314" id="Seg_2424" n="e" s="T313">toton </ts>
               <ts e="T315" id="Seg_2426" n="e" s="T314">baran </ts>
               <ts e="T316" id="Seg_2428" n="e" s="T315">utuja </ts>
               <ts e="T317" id="Seg_2430" n="e" s="T316">hɨtallar </ts>
               <ts e="T318" id="Seg_2432" n="e" s="T317">ebit </ts>
               <ts e="T319" id="Seg_2434" n="e" s="T318">bu͡o", </ts>
               <ts e="T320" id="Seg_2436" n="e" s="T319">diːr. </ts>
               <ts e="T321" id="Seg_2438" n="e" s="T320">Kü͡öhü </ts>
               <ts e="T322" id="Seg_2440" n="e" s="T321">tahaːrar. </ts>
               <ts e="T323" id="Seg_2442" n="e" s="T322">Hüregitten </ts>
               <ts e="T324" id="Seg_2444" n="e" s="T323">amsajar— </ts>
               <ts e="T325" id="Seg_2446" n="e" s="T324">hürege </ts>
               <ts e="T326" id="Seg_2448" n="e" s="T325">tarpɨt. </ts>
               <ts e="T327" id="Seg_2450" n="e" s="T326">"Oː, </ts>
               <ts e="T328" id="Seg_2452" n="e" s="T327">Lɨːbɨra </ts>
               <ts e="T329" id="Seg_2454" n="e" s="T328">min </ts>
               <ts e="T330" id="Seg_2456" n="e" s="T329">uruːm </ts>
               <ts e="T331" id="Seg_2458" n="e" s="T330">ebit </ts>
               <ts e="T332" id="Seg_2460" n="e" s="T331">duː", </ts>
               <ts e="T333" id="Seg_2462" n="e" s="T332">di͡ebit. </ts>
               <ts e="T334" id="Seg_2464" n="e" s="T333">Taːl </ts>
               <ts e="T335" id="Seg_2466" n="e" s="T334">hi͡ebit — </ts>
               <ts e="T336" id="Seg_2468" n="e" s="T335">taːla </ts>
               <ts e="T337" id="Seg_2470" n="e" s="T336">tarpɨt. </ts>
               <ts e="T338" id="Seg_2472" n="e" s="T337">"Oː, </ts>
               <ts e="T339" id="Seg_2474" n="e" s="T338">Lɨːbɨra </ts>
               <ts e="T340" id="Seg_2476" n="e" s="T339">ile </ts>
               <ts e="T341" id="Seg_2478" n="e" s="T340">uruːm </ts>
               <ts e="T342" id="Seg_2480" n="e" s="T341">ebit </ts>
               <ts e="T343" id="Seg_2482" n="e" s="T342">duː", </ts>
               <ts e="T344" id="Seg_2484" n="e" s="T343">di͡ebit. </ts>
               <ts e="T345" id="Seg_2486" n="e" s="T344">Onton </ts>
               <ts e="T346" id="Seg_2488" n="e" s="T345">turbut. </ts>
               <ts e="T347" id="Seg_2490" n="e" s="T346">Ogolorun </ts>
               <ts e="T348" id="Seg_2492" n="e" s="T347">arɨja </ts>
               <ts e="T349" id="Seg_2494" n="e" s="T348">tarpɨta — </ts>
               <ts e="T350" id="Seg_2496" n="e" s="T349">hette </ts>
               <ts e="T351" id="Seg_2498" n="e" s="T350">menʼiː </ts>
               <ts e="T352" id="Seg_2500" n="e" s="T351">čekenis </ts>
               <ts e="T353" id="Seg_2502" n="e" s="T352">gɨmmɨttar. </ts>
               <ts e="T354" id="Seg_2504" n="e" s="T353">Hohuja </ts>
               <ts e="T355" id="Seg_2506" n="e" s="T354">kɨtta </ts>
               <ts e="T356" id="Seg_2508" n="e" s="T355">Aŋaː Moŋus </ts>
               <ts e="T357" id="Seg_2510" n="e" s="T356">üːgüleːbit: </ts>
               <ts e="T358" id="Seg_2512" n="e" s="T357">"Lɨːbɨra! </ts>
               <ts e="T359" id="Seg_2514" n="e" s="T358">Kanna </ts>
               <ts e="T360" id="Seg_2516" n="e" s="T359">baːrgɨnɨj?" </ts>
               <ts e="T361" id="Seg_2518" n="e" s="T360">Lɨːbɨra: </ts>
               <ts e="T362" id="Seg_2520" n="e" s="T361">"Tahaːra </ts>
               <ts e="T363" id="Seg_2522" n="e" s="T362">baːrbɨn", </ts>
               <ts e="T364" id="Seg_2524" n="e" s="T363">di͡ebit. </ts>
               <ts e="T365" id="Seg_2526" n="e" s="T364">Onu </ts>
               <ts e="T366" id="Seg_2528" n="e" s="T365">ihitte </ts>
               <ts e="T367" id="Seg_2530" n="e" s="T366">da </ts>
               <ts e="T368" id="Seg_2532" n="e" s="T367">dʼi͡etin </ts>
               <ts e="T369" id="Seg_2534" n="e" s="T368">kaja </ts>
               <ts e="T370" id="Seg_2536" n="e" s="T369">kötön </ts>
               <ts e="T371" id="Seg_2538" n="e" s="T370">taksar. </ts>
               <ts e="T372" id="Seg_2540" n="e" s="T371">Lɨːbɨra </ts>
               <ts e="T373" id="Seg_2542" n="e" s="T372">kanna </ts>
               <ts e="T374" id="Seg_2544" n="e" s="T373">da </ts>
               <ts e="T375" id="Seg_2546" n="e" s="T374">hu͡ok. </ts>
               <ts e="T376" id="Seg_2548" n="e" s="T375">"Lɨːbɨraː, </ts>
               <ts e="T377" id="Seg_2550" n="e" s="T376">kannagɨnɨj?" </ts>
               <ts e="T378" id="Seg_2552" n="e" s="T377">"Dʼi͡e </ts>
               <ts e="T379" id="Seg_2554" n="e" s="T378">ihiger </ts>
               <ts e="T380" id="Seg_2556" n="e" s="T379">baːrbɨn", </ts>
               <ts e="T381" id="Seg_2558" n="e" s="T380">di͡ebit </ts>
               <ts e="T382" id="Seg_2560" n="e" s="T381">Lɨːbɨra. </ts>
               <ts e="T383" id="Seg_2562" n="e" s="T382">Dʼi͡ege </ts>
               <ts e="T384" id="Seg_2564" n="e" s="T383">kötön </ts>
               <ts e="T385" id="Seg_2566" n="e" s="T384">tüspüt — </ts>
               <ts e="T386" id="Seg_2568" n="e" s="T385">Lɨːbɨra </ts>
               <ts e="T387" id="Seg_2570" n="e" s="T386">hu͡ok. </ts>
               <ts e="T388" id="Seg_2572" n="e" s="T387">"Lɨːbɨraː, </ts>
               <ts e="T389" id="Seg_2574" n="e" s="T388">kannagɨnɨj?" </ts>
               <ts e="T390" id="Seg_2576" n="e" s="T389">"Ü͡öleske </ts>
               <ts e="T391" id="Seg_2578" n="e" s="T390">baːrbɨn!" </ts>
               <ts e="T392" id="Seg_2580" n="e" s="T391">Ü͡öleske </ts>
               <ts e="T393" id="Seg_2582" n="e" s="T392">körüleːbite — </ts>
               <ts e="T394" id="Seg_2584" n="e" s="T393">Lɨːbɨra </ts>
               <ts e="T395" id="Seg_2586" n="e" s="T394">kanna </ts>
               <ts e="T396" id="Seg_2588" n="e" s="T395">da </ts>
               <ts e="T397" id="Seg_2590" n="e" s="T396">hu͡ok. </ts>
               <ts e="T398" id="Seg_2592" n="e" s="T397">"Lɨːbɨraː, </ts>
               <ts e="T399" id="Seg_2594" n="e" s="T398">kanna </ts>
               <ts e="T400" id="Seg_2596" n="e" s="T399">baːrgɨnɨj", </ts>
               <ts e="T401" id="Seg_2598" n="e" s="T400">di͡ebit. </ts>
               <ts e="T402" id="Seg_2600" n="e" s="T401">Lɨːbɨra: </ts>
               <ts e="T403" id="Seg_2602" n="e" s="T402">"Ohok </ts>
               <ts e="T404" id="Seg_2604" n="e" s="T403">annɨgar </ts>
               <ts e="T405" id="Seg_2606" n="e" s="T404">baːrbɨn", </ts>
               <ts e="T406" id="Seg_2608" n="e" s="T405">di͡ebit. </ts>
               <ts e="T407" id="Seg_2610" n="e" s="T406">Onu </ts>
               <ts e="T408" id="Seg_2612" n="e" s="T407">Aŋaː Moŋus </ts>
               <ts e="T409" id="Seg_2614" n="e" s="T408">ihitte </ts>
               <ts e="T410" id="Seg_2616" n="e" s="T409">da, </ts>
               <ts e="T411" id="Seg_2618" n="e" s="T410">menʼiːtinen </ts>
               <ts e="T412" id="Seg_2620" n="e" s="T411">kiːreːri </ts>
               <ts e="T413" id="Seg_2622" n="e" s="T412">gɨmmɨt. </ts>
               <ts e="T414" id="Seg_2624" n="e" s="T413">Onuga </ts>
               <ts e="T415" id="Seg_2626" n="e" s="T414">Lɨːbɨra </ts>
               <ts e="T416" id="Seg_2628" n="e" s="T415">hanarar: </ts>
               <ts e="T417" id="Seg_2630" n="e" s="T416">"Eheː, </ts>
               <ts e="T418" id="Seg_2632" n="e" s="T417">menʼiːginen </ts>
               <ts e="T419" id="Seg_2634" n="e" s="T418">kiːrime — </ts>
               <ts e="T420" id="Seg_2636" n="e" s="T419">batɨ͡aŋ </ts>
               <ts e="T421" id="Seg_2638" n="e" s="T420">hu͡oga. </ts>
               <ts e="T422" id="Seg_2640" n="e" s="T421">Emeheginen </ts>
               <ts e="T423" id="Seg_2642" n="e" s="T422">kiːr." </ts>
               <ts e="T424" id="Seg_2644" n="e" s="T423">Aŋaː Moŋus </ts>
               <ts e="T425" id="Seg_2646" n="e" s="T424">emehetinen </ts>
               <ts e="T426" id="Seg_2648" n="e" s="T425">kiːrbit. </ts>
               <ts e="T427" id="Seg_2650" n="e" s="T426">Ol </ts>
               <ts e="T428" id="Seg_2652" n="e" s="T427">kiːren </ts>
               <ts e="T429" id="Seg_2654" n="e" s="T428">erdegine </ts>
               <ts e="T430" id="Seg_2656" n="e" s="T429">Lɨːbɨra </ts>
               <ts e="T431" id="Seg_2658" n="e" s="T430">kɨtarčɨ </ts>
               <ts e="T432" id="Seg_2660" n="e" s="T431">barbɨt </ts>
               <ts e="T433" id="Seg_2662" n="e" s="T432">korgoldʼiːnɨnan </ts>
               <ts e="T434" id="Seg_2664" n="e" s="T433">emehetin </ts>
               <ts e="T435" id="Seg_2666" n="e" s="T434">ihinen </ts>
               <ts e="T436" id="Seg_2668" n="e" s="T435">aspɨt. </ts>
               <ts e="T437" id="Seg_2670" n="e" s="T436">Aŋaː Moŋus </ts>
               <ts e="T438" id="Seg_2672" n="e" s="T437">ölön </ts>
               <ts e="T439" id="Seg_2674" n="e" s="T438">kaːlbɨt. </ts>
               <ts e="T440" id="Seg_2676" n="e" s="T439">Lɨːbɨra </ts>
               <ts e="T441" id="Seg_2678" n="e" s="T440">tagɨsta </ts>
               <ts e="T442" id="Seg_2680" n="e" s="T441">da </ts>
               <ts e="T443" id="Seg_2682" n="e" s="T442">dʼi͡etiger </ts>
               <ts e="T444" id="Seg_2684" n="e" s="T443">baran </ts>
               <ts e="T445" id="Seg_2686" n="e" s="T444">kaːlbɨt. </ts>
               <ts e="T446" id="Seg_2688" n="e" s="T445">Onon </ts>
               <ts e="T447" id="Seg_2690" n="e" s="T446">bajan-tajan </ts>
               <ts e="T448" id="Seg_2692" n="e" s="T447">olorbut. </ts>
               <ts e="T449" id="Seg_2694" n="e" s="T448">Dʼe </ts>
               <ts e="T450" id="Seg_2696" n="e" s="T449">bütte. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_2697" s="T0">PoS_PrG_1964_Lyybyra_flk.001</ta>
            <ta e="T11" id="Seg_2698" s="T5">PoS_PrG_1964_Lyybyra_flk.002</ta>
            <ta e="T16" id="Seg_2699" s="T11">PoS_PrG_1964_Lyybyra_flk.003</ta>
            <ta e="T21" id="Seg_2700" s="T16">PoS_PrG_1964_Lyybyra_flk.004</ta>
            <ta e="T28" id="Seg_2701" s="T21">PoS_PrG_1964_Lyybyra_flk.005</ta>
            <ta e="T39" id="Seg_2702" s="T28">PoS_PrG_1964_Lyybyra_flk.006</ta>
            <ta e="T45" id="Seg_2703" s="T39">PoS_PrG_1964_Lyybyra_flk.007</ta>
            <ta e="T52" id="Seg_2704" s="T45">PoS_PrG_1964_Lyybyra_flk.008</ta>
            <ta e="T54" id="Seg_2705" s="T52">PoS_PrG_1964_Lyybyra_flk.009</ta>
            <ta e="T58" id="Seg_2706" s="T54">PoS_PrG_1964_Lyybyra_flk.010</ta>
            <ta e="T59" id="Seg_2707" s="T58">PoS_PrG_1964_Lyybyra_flk.011</ta>
            <ta e="T68" id="Seg_2708" s="T59">PoS_PrG_1964_Lyybyra_flk.012</ta>
            <ta e="T70" id="Seg_2709" s="T68">PoS_PrG_1964_Lyybyra_flk.013</ta>
            <ta e="T75" id="Seg_2710" s="T70">PoS_PrG_1964_Lyybyra_flk.014</ta>
            <ta e="T84" id="Seg_2711" s="T75">PoS_PrG_1964_Lyybyra_flk.015</ta>
            <ta e="T93" id="Seg_2712" s="T84">PoS_PrG_1964_Lyybyra_flk.016</ta>
            <ta e="T100" id="Seg_2713" s="T93">PoS_PrG_1964_Lyybyra_flk.017</ta>
            <ta e="T104" id="Seg_2714" s="T100">PoS_PrG_1964_Lyybyra_flk.018</ta>
            <ta e="T107" id="Seg_2715" s="T104">PoS_PrG_1964_Lyybyra_flk.019</ta>
            <ta e="T115" id="Seg_2716" s="T107">PoS_PrG_1964_Lyybyra_flk.020</ta>
            <ta e="T123" id="Seg_2717" s="T115">PoS_PrG_1964_Lyybyra_flk.021</ta>
            <ta e="T126" id="Seg_2718" s="T123">PoS_PrG_1964_Lyybyra_flk.022</ta>
            <ta e="T131" id="Seg_2719" s="T126">PoS_PrG_1964_Lyybyra_flk.023</ta>
            <ta e="T132" id="Seg_2720" s="T131">PoS_PrG_1964_Lyybyra_flk.024</ta>
            <ta e="T141" id="Seg_2721" s="T132">PoS_PrG_1964_Lyybyra_flk.025</ta>
            <ta e="T148" id="Seg_2722" s="T141">PoS_PrG_1964_Lyybyra_flk.026</ta>
            <ta e="T153" id="Seg_2723" s="T148">PoS_PrG_1964_Lyybyra_flk.027</ta>
            <ta e="T155" id="Seg_2724" s="T153">PoS_PrG_1964_Lyybyra_flk.028</ta>
            <ta e="T160" id="Seg_2725" s="T155">PoS_PrG_1964_Lyybyra_flk.029</ta>
            <ta e="T164" id="Seg_2726" s="T160">PoS_PrG_1964_Lyybyra_flk.030</ta>
            <ta e="T167" id="Seg_2727" s="T164">PoS_PrG_1964_Lyybyra_flk.031</ta>
            <ta e="T173" id="Seg_2728" s="T167">PoS_PrG_1964_Lyybyra_flk.032</ta>
            <ta e="T178" id="Seg_2729" s="T173">PoS_PrG_1964_Lyybyra_flk.033</ta>
            <ta e="T181" id="Seg_2730" s="T178">PoS_PrG_1964_Lyybyra_flk.034</ta>
            <ta e="T187" id="Seg_2731" s="T181">PoS_PrG_1964_Lyybyra_flk.035</ta>
            <ta e="T189" id="Seg_2732" s="T187">PoS_PrG_1964_Lyybyra_flk.036</ta>
            <ta e="T193" id="Seg_2733" s="T189">PoS_PrG_1964_Lyybyra_flk.037</ta>
            <ta e="T199" id="Seg_2734" s="T193">PoS_PrG_1964_Lyybyra_flk.038</ta>
            <ta e="T202" id="Seg_2735" s="T199">PoS_PrG_1964_Lyybyra_flk.039</ta>
            <ta e="T203" id="Seg_2736" s="T202">PoS_PrG_1964_Lyybyra_flk.040</ta>
            <ta e="T207" id="Seg_2737" s="T203">PoS_PrG_1964_Lyybyra_flk.041</ta>
            <ta e="T211" id="Seg_2738" s="T207">PoS_PrG_1964_Lyybyra_flk.042</ta>
            <ta e="T213" id="Seg_2739" s="T211">PoS_PrG_1964_Lyybyra_flk.043</ta>
            <ta e="T214" id="Seg_2740" s="T213">PoS_PrG_1964_Lyybyra_flk.044</ta>
            <ta e="T218" id="Seg_2741" s="T214">PoS_PrG_1964_Lyybyra_flk.045</ta>
            <ta e="T221" id="Seg_2742" s="T218">PoS_PrG_1964_Lyybyra_flk.046</ta>
            <ta e="T222" id="Seg_2743" s="T221">PoS_PrG_1964_Lyybyra_flk.047</ta>
            <ta e="T224" id="Seg_2744" s="T222">PoS_PrG_1964_Lyybyra_flk.048</ta>
            <ta e="T234" id="Seg_2745" s="T224">PoS_PrG_1964_Lyybyra_flk.049</ta>
            <ta e="T237" id="Seg_2746" s="T234">PoS_PrG_1964_Lyybyra_flk.050</ta>
            <ta e="T239" id="Seg_2747" s="T237">PoS_PrG_1964_Lyybyra_flk.051</ta>
            <ta e="T241" id="Seg_2748" s="T239">PoS_PrG_1964_Lyybyra_flk.052</ta>
            <ta e="T245" id="Seg_2749" s="T241">PoS_PrG_1964_Lyybyra_flk.053</ta>
            <ta e="T247" id="Seg_2750" s="T245">PoS_PrG_1964_Lyybyra_flk.054</ta>
            <ta e="T253" id="Seg_2751" s="T247">PoS_PrG_1964_Lyybyra_flk.055</ta>
            <ta e="T256" id="Seg_2752" s="T253">PoS_PrG_1964_Lyybyra_flk.056</ta>
            <ta e="T262" id="Seg_2753" s="T256">PoS_PrG_1964_Lyybyra_flk.057</ta>
            <ta e="T265" id="Seg_2754" s="T262">PoS_PrG_1964_Lyybyra_flk.058</ta>
            <ta e="T272" id="Seg_2755" s="T265">PoS_PrG_1964_Lyybyra_flk.059</ta>
            <ta e="T279" id="Seg_2756" s="T272">PoS_PrG_1964_Lyybyra_flk.060</ta>
            <ta e="T283" id="Seg_2757" s="T279">PoS_PrG_1964_Lyybyra_flk.061</ta>
            <ta e="T286" id="Seg_2758" s="T283">PoS_PrG_1964_Lyybyra_flk.062</ta>
            <ta e="T287" id="Seg_2759" s="T286">PoS_PrG_1964_Lyybyra_flk.063</ta>
            <ta e="T289" id="Seg_2760" s="T287">PoS_PrG_1964_Lyybyra_flk.064</ta>
            <ta e="T292" id="Seg_2761" s="T289">PoS_PrG_1964_Lyybyra_flk.065</ta>
            <ta e="T295" id="Seg_2762" s="T292">PoS_PrG_1964_Lyybyra_flk.066</ta>
            <ta e="T298" id="Seg_2763" s="T295">PoS_PrG_1964_Lyybyra_flk.067</ta>
            <ta e="T302" id="Seg_2764" s="T298">PoS_PrG_1964_Lyybyra_flk.068</ta>
            <ta e="T309" id="Seg_2765" s="T302">PoS_PrG_1964_Lyybyra_flk.069</ta>
            <ta e="T320" id="Seg_2766" s="T309">PoS_PrG_1964_Lyybyra_flk.070</ta>
            <ta e="T322" id="Seg_2767" s="T320">PoS_PrG_1964_Lyybyra_flk.071</ta>
            <ta e="T326" id="Seg_2768" s="T322">PoS_PrG_1964_Lyybyra_flk.072</ta>
            <ta e="T333" id="Seg_2769" s="T326">PoS_PrG_1964_Lyybyra_flk.073</ta>
            <ta e="T337" id="Seg_2770" s="T333">PoS_PrG_1964_Lyybyra_flk.074</ta>
            <ta e="T344" id="Seg_2771" s="T337">PoS_PrG_1964_Lyybyra_flk.075</ta>
            <ta e="T346" id="Seg_2772" s="T344">PoS_PrG_1964_Lyybyra_flk.076</ta>
            <ta e="T353" id="Seg_2773" s="T346">PoS_PrG_1964_Lyybyra_flk.077</ta>
            <ta e="T357" id="Seg_2774" s="T353">PoS_PrG_1964_Lyybyra_flk.078</ta>
            <ta e="T358" id="Seg_2775" s="T357">PoS_PrG_1964_Lyybyra_flk.079</ta>
            <ta e="T360" id="Seg_2776" s="T358">PoS_PrG_1964_Lyybyra_flk.080</ta>
            <ta e="T361" id="Seg_2777" s="T360">PoS_PrG_1964_Lyybyra_flk.081</ta>
            <ta e="T364" id="Seg_2778" s="T361">PoS_PrG_1964_Lyybyra_flk.082</ta>
            <ta e="T371" id="Seg_2779" s="T364">PoS_PrG_1964_Lyybyra_flk.083</ta>
            <ta e="T375" id="Seg_2780" s="T371">PoS_PrG_1964_Lyybyra_flk.084</ta>
            <ta e="T377" id="Seg_2781" s="T375">PoS_PrG_1964_Lyybyra_flk.085</ta>
            <ta e="T382" id="Seg_2782" s="T377">PoS_PrG_1964_Lyybyra_flk.086</ta>
            <ta e="T387" id="Seg_2783" s="T382">PoS_PrG_1964_Lyybyra_flk.087</ta>
            <ta e="T389" id="Seg_2784" s="T387">PoS_PrG_1964_Lyybyra_flk.088</ta>
            <ta e="T391" id="Seg_2785" s="T389">PoS_PrG_1964_Lyybyra_flk.089</ta>
            <ta e="T397" id="Seg_2786" s="T391">PoS_PrG_1964_Lyybyra_flk.090</ta>
            <ta e="T401" id="Seg_2787" s="T397">PoS_PrG_1964_Lyybyra_flk.091</ta>
            <ta e="T402" id="Seg_2788" s="T401">PoS_PrG_1964_Lyybyra_flk.092</ta>
            <ta e="T406" id="Seg_2789" s="T402">PoS_PrG_1964_Lyybyra_flk.093</ta>
            <ta e="T413" id="Seg_2790" s="T406">PoS_PrG_1964_Lyybyra_flk.094</ta>
            <ta e="T416" id="Seg_2791" s="T413">PoS_PrG_1964_Lyybyra_flk.095</ta>
            <ta e="T421" id="Seg_2792" s="T416">PoS_PrG_1964_Lyybyra_flk.096</ta>
            <ta e="T423" id="Seg_2793" s="T421">PoS_PrG_1964_Lyybyra_flk.097</ta>
            <ta e="T426" id="Seg_2794" s="T423">PoS_PrG_1964_Lyybyra_flk.098</ta>
            <ta e="T436" id="Seg_2795" s="T426">PoS_PrG_1964_Lyybyra_flk.099</ta>
            <ta e="T439" id="Seg_2796" s="T436">PoS_PrG_1964_Lyybyra_flk.100</ta>
            <ta e="T445" id="Seg_2797" s="T439">PoS_PrG_1964_Lyybyra_flk.101</ta>
            <ta e="T448" id="Seg_2798" s="T445">PoS_PrG_1964_Lyybyra_flk.102</ta>
            <ta e="T450" id="Seg_2799" s="T448">PoS_PrG_1964_Lyybyra_flk.103</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_2800" s="T0">Лыыбыра эрдэн күөгэлдьэтэн иһээктиир даа.</ta>
            <ta e="T11" id="Seg_2801" s="T5">Иһэн-иһэн тааска кэппит, тыыта һыстан каалбыт.</ta>
            <ta e="T16" id="Seg_2802" s="T11">Эрдиитинэн оксубут — эрдиитэ һыстан каалбыт.</ta>
            <ta e="T21" id="Seg_2803" s="T16">Илиитинэн оксубут — илиитэ һыстан каалбыт.</ta>
            <ta e="T28" id="Seg_2804" s="T21">Бииргэс илиитинэн оксор — бииргэһэ эмиэ һыстан каалбыт.</ta>
            <ta e="T39" id="Seg_2805" s="T28">Атагынан тэппит — атага һыстан каалбыт, нөҥүө атагынан тэппитэ — эмиэ һыстан каалбыт.</ta>
            <ta e="T45" id="Seg_2806" s="T39">Быарынан үппүтэ — быара эмиэ һыстан каалбыт.</ta>
            <ta e="T52" id="Seg_2807" s="T45">Онтон мэньиитинэн оксубут — мэньиитэ эмиэ һыстан каалбыт.</ta>
            <ta e="T54" id="Seg_2808" s="T52">һыстан турдага.</ta>
            <ta e="T58" id="Seg_2809" s="T54">Ол турдагын Аҥаа Моҥус кэлбит.</ta>
            <ta e="T59" id="Seg_2810" s="T58">— Аньыкатаан!</ta>
            <ta e="T68" id="Seg_2811" s="T59">Киэһээҥҥи һокуускалаак буоллум! — диэбитинэн Лыыбыраны ылан кармааныгар уктан кээспит.</ta>
            <ta e="T70" id="Seg_2812" s="T68">Дьиэтигэр барбыт.</ta>
            <ta e="T75" id="Seg_2813" s="T70">Орогугар тийэн Аҥаа Моҥус һаактыан багарбыт.</ta>
            <ta e="T84" id="Seg_2814" s="T75">Ол һаактыы олордогуна, Лыыбыра кармаанын кайагаһын үстүн түһэн каалбыт.</ta>
            <ta e="T93" id="Seg_2815" s="T84">Аҥаа Моҥус тугу да билбэккэ һыалыйатын көтөгүннэ да дьиэтигэр барбыт.</ta>
            <ta e="T100" id="Seg_2816" s="T93">Дьиэтигэр таһыгар һонун араҥаска ыйаан баран киирбит.</ta>
            <ta e="T104" id="Seg_2817" s="T100">Эмээксинэ иистэнэ олорор эбит.</ta>
            <ta e="T107" id="Seg_2818" s="T104">— Эмээксиэн, кэһиибин эгэллим.</ta>
            <ta e="T115" id="Seg_2819" s="T107">һонум кармааныгар баар таһаара, ону киллэрэн көр! — диэбит.</ta>
            <ta e="T123" id="Seg_2820" s="T115">Эмээксинэ үөрүүтүгэр иҥнэлээк-һүүтүгүн ыйыран кээспит, такса көппүт.</ta>
            <ta e="T126" id="Seg_2821" s="T123">Булбакка төннөн киирбит:</ta>
            <ta e="T131" id="Seg_2822" s="T126">— Кармааҥҥар туок да һуок! — диэбит.</ta>
            <ta e="T132" id="Seg_2823" s="T131">Аҥаа Моҥус:</ta>
            <ta e="T141" id="Seg_2824" s="T132">— Бэйэҥ һиэтэгиҥ диин, — диэбит да, эмээксин быарын кайа тардар.</ta>
            <ta e="T148" id="Seg_2825" s="T141">Эмээксин быарыттан иҥнэлээк һүүтүк кылыр гына түспүт.</ta>
            <ta e="T153" id="Seg_2826" s="T148">— Оо, каарыан дьактарбын! — үүгүлээбит Аҥаа Моҥус.</ta>
            <ta e="T155" id="Seg_2827" s="T153">Төттөрү һүүрбүт.</ta>
            <ta e="T160" id="Seg_2828" s="T155">Лыыбыра орокко бу чаҥкайа һытар.</ta>
            <ta e="T164" id="Seg_2829" s="T160">Ылар да кармааныгар уктар.</ta>
            <ta e="T167" id="Seg_2830" s="T164">Төттөрү һүүрэн кэлэр.</ta>
            <ta e="T173" id="Seg_2831" s="T167">Дьиэтигэр киирэн Лыыбыраны иикээптииҥҥэ ыйаан кээһэр.</ta>
            <ta e="T178" id="Seg_2832" s="T173">Аҥаа Моҥус һэттэ таракаай оголоок эбит.</ta>
            <ta e="T181" id="Seg_2833" s="T178">Ол оголорукар диэбит:</ta>
            <ta e="T187" id="Seg_2834" s="T181">— Оголоор, Лыыбыра һыата тэстиэгэ — көрөр буолуҥ.</ta>
            <ta e="T189" id="Seg_2835" s="T187">Кэлэрбэр күөстээриҥ!</ta>
            <ta e="T193" id="Seg_2836" s="T189">Диэтэ да таксан каалбыт.</ta>
            <ta e="T199" id="Seg_2837" s="T193">Төһө да буолбакка Лыыбыра дьэ ииктээбит.</ta>
            <ta e="T202" id="Seg_2838" s="T199">Оголор онно үүгүлэспиттэр:</ta>
            <ta e="T203" id="Seg_2839" s="T202">— Аньикатаан!</ta>
            <ta e="T207" id="Seg_2840" s="T203">Лыыбыра ыбагас һыата тогунна!</ta>
            <ta e="T211" id="Seg_2841" s="T207">Онтон Лыыбыра дьэ һаактыыр.</ta>
            <ta e="T213" id="Seg_2842" s="T211">Оголор үүгүлэспиттэр:</ta>
            <ta e="T214" id="Seg_2843" s="T213">— Аньикатаан!</ta>
            <ta e="T218" id="Seg_2844" s="T214">Лыыбыра койуу һыата тогунна!</ta>
            <ta e="T221" id="Seg_2845" s="T218">Лыыбыра оголорго диэбит:</ta>
            <ta e="T222" id="Seg_2846" s="T221">— Оголоор!</ta>
            <ta e="T224" id="Seg_2847" s="T222">Түһэриҥ миигин.</ta>
            <ta e="T234" id="Seg_2848" s="T224">Мин эһиэкэ луоску оҥорон биэриэм — мин һыабын һииргитигэр үчүгэй буолуо!</ta>
            <ta e="T237" id="Seg_2849" s="T234">— Кайа, эрэй кэйэ!</ta>
            <ta e="T239" id="Seg_2850" s="T237">— Лыыбыраны түһэрбиттэр.</ta>
            <ta e="T241" id="Seg_2851" s="T239">Лыыбыра диэбит:</ta>
            <ta e="T245" id="Seg_2852" s="T241">— Эгэлиҥ тээтэгит һытыы һүгэтин!</ta>
            <ta e="T247" id="Seg_2853" s="T245">Эгэлэн биэрбиттэр.</ta>
            <ta e="T253" id="Seg_2854" s="T247">— Ороҥҥутугар көрө һытыҥ — кайдак мин оҥоробун.</ta>
            <ta e="T256" id="Seg_2855" s="T253">Оголор һытан каалбыттар.</ta>
            <ta e="T262" id="Seg_2856" s="T256">Ол һыппыттарыгар Лыыбыра мэньиилэрин быһа кэрдитэлээбит.</ta>
            <ta e="T265" id="Seg_2857" s="T262">Эттэрин күөстээн кээспит.</ta>
            <ta e="T272" id="Seg_2858" s="T265">Мэньиилэри ороҥҥо кэчигирэччикээн ууран баран һуорганынан һабыталаабыт.</ta>
            <ta e="T279" id="Seg_2859" s="T272">Корголдьиины ылан баран оһок анныгар киирэн каалбыт.</ta>
            <ta e="T283" id="Seg_2860" s="T279">Бу һыттагына Аҥаа Моҥус кэлбит.</ta>
            <ta e="T286" id="Seg_2861" s="T283">Аҥаа Моҥус кэлэн үүгүлээбит:</ta>
            <ta e="T287" id="Seg_2862" s="T286">— Оголоор!</ta>
            <ta e="T289" id="Seg_2863" s="T287">Ааҥҥытын арыйыҥ!</ta>
            <ta e="T292" id="Seg_2864" s="T289">Ким да арыйбат.</ta>
            <ta e="T295" id="Seg_2865" s="T292">Каста да үүгүлээбит.</ta>
            <ta e="T298" id="Seg_2866" s="T295">Оголор төрүт арыйбатактар.</ta>
            <ta e="T302" id="Seg_2867" s="T298">Аанын костуу тардан киирбит.</ta>
            <ta e="T309" id="Seg_2868" s="T302">Көрбүтэ — эт буһа турар, оголор утуйа һыталлар.</ta>
            <ta e="T320" id="Seg_2869" s="T309">— Оо, оголорум Лыыбыра һыатыттан тотон баран утуйа һыталлар эбит буо! — диир.</ta>
            <ta e="T322" id="Seg_2870" s="T320">Күөһү таһаарар,</ta>
            <ta e="T326" id="Seg_2871" s="T322">һүрэгиттэн амсайар — һүрэгэ тарпыт.</ta>
            <ta e="T333" id="Seg_2872" s="T326">— Оо, Лыыбыра мин уруум эбит дуу?! — диэбит.</ta>
            <ta e="T337" id="Seg_2873" s="T333">Таал һиэбит — таала тарпыт.</ta>
            <ta e="T344" id="Seg_2874" s="T337">— Оо, Лыыбыра илэ уруум эбит дуу?! диэбит.</ta>
            <ta e="T346" id="Seg_2875" s="T344">Онтон турбут.</ta>
            <ta e="T353" id="Seg_2876" s="T346">Оголорун арыйа тарпыта — һэттэ мэньии чэкэнис гыммыттар.</ta>
            <ta e="T357" id="Seg_2877" s="T353">Һоһуйа кытта Аҥаа Моҥус үүгүлээбит:</ta>
            <ta e="T358" id="Seg_2878" s="T357">— Лыыбыра!</ta>
            <ta e="T360" id="Seg_2879" s="T358">Канна бааргыный?</ta>
            <ta e="T361" id="Seg_2880" s="T360">Лыыбыра:</ta>
            <ta e="T364" id="Seg_2881" s="T361">— Таһаара баарбын! диэбит.</ta>
            <ta e="T371" id="Seg_2882" s="T364">Ону иһиттэ да дьиэтин кайа көтөн таксар.</ta>
            <ta e="T375" id="Seg_2883" s="T371">Лыыбыра канна да һуок.</ta>
            <ta e="T377" id="Seg_2884" s="T375">— Лыыбыраа, каннагыный?</ta>
            <ta e="T382" id="Seg_2885" s="T377">— Дьиэ иһигэр баарбын! диэбит Лыыбыра.</ta>
            <ta e="T387" id="Seg_2886" s="T382">Дьиэгэ көтөн түспүт — Лыыбыра һуок.</ta>
            <ta e="T389" id="Seg_2887" s="T387">— Лыыбыраа, каннагыный?</ta>
            <ta e="T391" id="Seg_2888" s="T389">— Үөлэскэ баарбын!</ta>
            <ta e="T397" id="Seg_2889" s="T391">Үөлэскэ көрүлээбитэ — Лыыбыра канна да һуок.</ta>
            <ta e="T401" id="Seg_2890" s="T397">— Лыыбыраа, канна бааргыный? диэбит.</ta>
            <ta e="T402" id="Seg_2891" s="T401">Лыыбыра:</ta>
            <ta e="T406" id="Seg_2892" s="T402">— Оһок анныгар баарбын! диэбит.</ta>
            <ta e="T413" id="Seg_2893" s="T406">Ону Аҥаа Моҥус иһиттэ да, мэньиитинэн киирээри гыммыт.</ta>
            <ta e="T416" id="Seg_2894" s="T413">Онуга Лыыбыра һанарар:</ta>
            <ta e="T421" id="Seg_2895" s="T416">— Эһээ, мэньиигинэн кииримэ — батыаҥ һуога.</ta>
            <ta e="T423" id="Seg_2896" s="T421">Эмэһэгинэн киир.</ta>
            <ta e="T426" id="Seg_2897" s="T423">Аҥаа Моҥус эмэһэтинэн киирбит.</ta>
            <ta e="T436" id="Seg_2898" s="T426">Ол киирэн эрдэгинэ Лыыбыра кытарчы барбыт корголдьиинынан эмэһэтин иһинэн аспыт.</ta>
            <ta e="T439" id="Seg_2899" s="T436">Аҥаа Моҥус өлөн каалбыт.</ta>
            <ta e="T445" id="Seg_2900" s="T439">Лыыбыра тагыста да дьиэтигэр баран каалбыт.</ta>
            <ta e="T448" id="Seg_2901" s="T445">Онон байан-тайан олорбут.</ta>
            <ta e="T450" id="Seg_2902" s="T448">Дьэ бүттэ.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_2903" s="T0">Lɨːbɨra erden kü͡ögeldʼeten iheːktiːr daː. </ta>
            <ta e="T11" id="Seg_2904" s="T5">Ihen-ihen taːska keppit, tɨːta hɨstan kaːlbɨt. </ta>
            <ta e="T16" id="Seg_2905" s="T11">Erdiːtinen oksubut — erdiːte hɨstan kaːlbɨt. </ta>
            <ta e="T21" id="Seg_2906" s="T16">Iliːtinen oksubut — iliːte hɨstan kaːlbɨt. </ta>
            <ta e="T28" id="Seg_2907" s="T21">Biːrges iliːtinen oksor — biːrgehe emi͡e hɨstan kaːlbɨt. </ta>
            <ta e="T39" id="Seg_2908" s="T28">Atagɨnan teppit — ataga hɨstan kaːlbɨt, nöŋü͡ö atagɨnan teppite — emi͡e hɨstan kaːlbɨt. </ta>
            <ta e="T45" id="Seg_2909" s="T39">Bɨ͡arɨnan üppüte — bɨ͡ara emi͡e hɨstan kaːlbɨt. </ta>
            <ta e="T52" id="Seg_2910" s="T45">Onton menʼiːtinen oksubut — menʼiːte emi͡e hɨstan kaːlbɨt. </ta>
            <ta e="T54" id="Seg_2911" s="T52">Hɨstan turdaga. </ta>
            <ta e="T58" id="Seg_2912" s="T54">Ol turdagɨn Aŋaː Moŋus kelbit. </ta>
            <ta e="T59" id="Seg_2913" s="T58">"Anʼɨkataːn!</ta>
            <ta e="T68" id="Seg_2914" s="T59">Ki͡eheːŋŋi hokuːskalaːk bu͡ollum", di͡ebitinen Lɨːbɨranɨ ɨlan karmaːnɨgar uktan keːspit. </ta>
            <ta e="T70" id="Seg_2915" s="T68">Dʼi͡etiger barbɨt. </ta>
            <ta e="T75" id="Seg_2916" s="T70">Orogugar tijen Aŋaː Moŋus haːktɨ͡an bagarbɨt. </ta>
            <ta e="T84" id="Seg_2917" s="T75">Ol haːktɨː olordoguna, Lɨːbɨra karmaːnɨn kajagahɨn üstün tühen kaːlbɨt. </ta>
            <ta e="T93" id="Seg_2918" s="T84">Aŋaː Moŋus tugu da bilbekke hɨ͡alɨjatɨn kötögünne da dʼi͡etiger barbɨt. </ta>
            <ta e="T100" id="Seg_2919" s="T93">Dʼi͡etiger tahɨgar honun araŋaska ɨjaːn baran kiːrbit. </ta>
            <ta e="T104" id="Seg_2920" s="T100">Emeːksine iːstene oloror ebit. </ta>
            <ta e="T107" id="Seg_2921" s="T104">"Emeːksi͡en, kehiːbin egellim. </ta>
            <ta e="T115" id="Seg_2922" s="T107">Honum karmaːnɨgar baːr tahaːra, onu killeren kör", di͡ebit. </ta>
            <ta e="T123" id="Seg_2923" s="T115">Emeːksine ü͡örüːtüger iŋneleːk hüːtügün ɨjɨran keːspit, taksa köppüt. </ta>
            <ta e="T126" id="Seg_2924" s="T123">Bulbakka tönnön kiːrbit:</ta>
            <ta e="T131" id="Seg_2925" s="T126">"Karmaːŋŋar tu͡ok da hu͡ok", di͡ebit. </ta>
            <ta e="T132" id="Seg_2926" s="T131">Aŋaː Moŋus:</ta>
            <ta e="T141" id="Seg_2927" s="T132">"Bejeŋ hi͡etegiŋ diːn", di͡ebit da, emeːksin bɨ͡arɨn kaja tardar. </ta>
            <ta e="T148" id="Seg_2928" s="T141">Emeːksin bɨ͡arɨttan iŋneleːk hüːtük kɨlɨr gɨna tüspüt. </ta>
            <ta e="T153" id="Seg_2929" s="T148">"Oː, kaːrɨ͡an dʼaktarbɨn", üːgüleːbit Aŋaː Moŋus. </ta>
            <ta e="T155" id="Seg_2930" s="T153">Töttörü hüːrbüt. </ta>
            <ta e="T160" id="Seg_2931" s="T155">Lɨːbɨra orokko bu čaŋkaja hɨtar. </ta>
            <ta e="T164" id="Seg_2932" s="T160">ɨlar da karmaːnɨgar uktar. </ta>
            <ta e="T167" id="Seg_2933" s="T164">Töttörü hüːren keler. </ta>
            <ta e="T173" id="Seg_2934" s="T167">Dʼi͡etiger kiːren Lɨːbɨranɨ iːkeːptiːŋŋe ɨjaːn keːher. </ta>
            <ta e="T178" id="Seg_2935" s="T173">Aŋaː Moŋus hette tarakaːj ogoloːk ebit. </ta>
            <ta e="T181" id="Seg_2936" s="T178">Ol ogolorukar di͡ebit: </ta>
            <ta e="T187" id="Seg_2937" s="T181">"Ogoloːr, Lɨːbɨra hɨ͡ata testi͡ege — körör bu͡oluŋ. </ta>
            <ta e="T189" id="Seg_2938" s="T187">Kelerber kü͡östeːriŋ!" </ta>
            <ta e="T193" id="Seg_2939" s="T189">Di͡ete da taksan kaːlbɨt. </ta>
            <ta e="T199" id="Seg_2940" s="T193">Töhö da bu͡olbakka Lɨːbɨra dʼe iːkteːbit. </ta>
            <ta e="T202" id="Seg_2941" s="T199">Ogolor onno üːgülespitter: </ta>
            <ta e="T203" id="Seg_2942" s="T202">"Anʼikataːn! </ta>
            <ta e="T207" id="Seg_2943" s="T203">Lɨːbɨra ɨbagas hɨ͡ata togunna!" </ta>
            <ta e="T211" id="Seg_2944" s="T207">Onton Lɨːbɨra dʼe haːktɨːr. </ta>
            <ta e="T213" id="Seg_2945" s="T211">Ogolor üːgülespitter: </ta>
            <ta e="T214" id="Seg_2946" s="T213">"Anʼikataːn! </ta>
            <ta e="T218" id="Seg_2947" s="T214">Lɨːbɨra kojuː hɨ͡ata togunna!" </ta>
            <ta e="T221" id="Seg_2948" s="T218">Lɨːbɨra ogolorgo di͡ebit: </ta>
            <ta e="T222" id="Seg_2949" s="T221">"Ogoloːr! </ta>
            <ta e="T224" id="Seg_2950" s="T222">Tüheriŋ miːgin. </ta>
            <ta e="T234" id="Seg_2951" s="T224">Min ehi͡eke lu͡osku oŋoron bi͡eri͡em — min hɨ͡abɨn hiːrgitiger üčügej bu͡olu͡o!" </ta>
            <ta e="T237" id="Seg_2952" s="T234">"Kaja, erej keje!" </ta>
            <ta e="T239" id="Seg_2953" s="T237">Lɨːbɨranɨ tüherbitter. </ta>
            <ta e="T241" id="Seg_2954" s="T239">Lɨːbɨra di͡ebit: </ta>
            <ta e="T245" id="Seg_2955" s="T241">"Egeliŋ teːtegit hɨtɨː hügetin!" </ta>
            <ta e="T247" id="Seg_2956" s="T245">Egelen bi͡erbitter. </ta>
            <ta e="T253" id="Seg_2957" s="T247">"Oroŋŋutugar körö hɨtɨŋ — kajdak min oŋorobun." </ta>
            <ta e="T256" id="Seg_2958" s="T253">Ogolor hɨtan kaːlbɨttar. </ta>
            <ta e="T262" id="Seg_2959" s="T256">Ol hɨppɨttarɨgar Lɨːbɨra menʼiːlerin bɨha kerditeleːbit. </ta>
            <ta e="T265" id="Seg_2960" s="T262">Etterin kü͡östeːn keːspit. </ta>
            <ta e="T272" id="Seg_2961" s="T265">Menʼiːleri oroŋŋo kečigireččikeːn uːran baran hu͡organɨnan habɨtalaːbɨt. </ta>
            <ta e="T279" id="Seg_2962" s="T272">Korgoldʼiːnɨ ɨlan baran ohok annɨgar kiːren kaːlbɨt. </ta>
            <ta e="T283" id="Seg_2963" s="T279">Bu hɨttagɨna Aŋaː Moŋus kelbit. </ta>
            <ta e="T286" id="Seg_2964" s="T283">Aŋaː Moŋus kelen üːgüleːbit: </ta>
            <ta e="T287" id="Seg_2965" s="T286">"Ogoloːr! </ta>
            <ta e="T289" id="Seg_2966" s="T287">Aːŋŋɨtɨn arɨjɨŋ!" </ta>
            <ta e="T292" id="Seg_2967" s="T289">Kim da arɨjbat. </ta>
            <ta e="T295" id="Seg_2968" s="T292">Kasta da üːgüleːbit. </ta>
            <ta e="T298" id="Seg_2969" s="T295">Ogolor törüt arɨjbataktar. </ta>
            <ta e="T302" id="Seg_2970" s="T298">Aːnɨn kostuː tardan kiːrbit. </ta>
            <ta e="T309" id="Seg_2971" s="T302">Körbüte — et buha turar, ogolor utuja hɨtallar. </ta>
            <ta e="T320" id="Seg_2972" s="T309">"Oː, ogolorum Lɨːbɨra hɨ͡atɨttan toton baran utuja hɨtallar ebit bu͡o", diːr. </ta>
            <ta e="T322" id="Seg_2973" s="T320">Kü͡öhü tahaːrar. </ta>
            <ta e="T326" id="Seg_2974" s="T322">Hüregitten amsajar — hürege tarpɨt. </ta>
            <ta e="T333" id="Seg_2975" s="T326">"Oː, Lɨːbɨra min uruːm ebit duː", di͡ebit. </ta>
            <ta e="T337" id="Seg_2976" s="T333">Taːl hi͡ebit — taːla tarpɨt. </ta>
            <ta e="T344" id="Seg_2977" s="T337">"Oː, Lɨːbɨra ile uruːm ebit duː?!", di͡ebit. </ta>
            <ta e="T346" id="Seg_2978" s="T344">Onton turbut. </ta>
            <ta e="T353" id="Seg_2979" s="T346">Ogolorun arɨja tarpɨta — hette menʼiː čekenis gɨmmɨttar. </ta>
            <ta e="T357" id="Seg_2980" s="T353">Hohuja kɨtta Aŋaː Moŋus üːgüleːbit: </ta>
            <ta e="T358" id="Seg_2981" s="T357">"Lɨːbɨra! </ta>
            <ta e="T360" id="Seg_2982" s="T358">Kanna baːrgɨnɨj?" </ta>
            <ta e="T361" id="Seg_2983" s="T360">Lɨːbɨra: </ta>
            <ta e="T364" id="Seg_2984" s="T361">"Tahaːra baːrbɨn", di͡ebit. </ta>
            <ta e="T371" id="Seg_2985" s="T364">Onu ihitte da dʼi͡etin kaja kötön taksar. </ta>
            <ta e="T375" id="Seg_2986" s="T371">Lɨːbɨra kanna da hu͡ok. </ta>
            <ta e="T377" id="Seg_2987" s="T375">"Lɨːbɨraː, kannagɨnɨj?" </ta>
            <ta e="T382" id="Seg_2988" s="T377">"Dʼi͡e ihiger baːrbɨn", di͡ebit Lɨːbɨra. </ta>
            <ta e="T387" id="Seg_2989" s="T382">Dʼi͡ege kötön tüspüt — Lɨːbɨra hu͡ok. </ta>
            <ta e="T389" id="Seg_2990" s="T387">"Lɨːbɨraː, kannagɨnɨj?" </ta>
            <ta e="T391" id="Seg_2991" s="T389">"Ü͡öleske baːrbɨn!" </ta>
            <ta e="T397" id="Seg_2992" s="T391">Ü͡öleske körüleːbite — Lɨːbɨra kanna da hu͡ok. </ta>
            <ta e="T401" id="Seg_2993" s="T397">"Lɨːbɨraː, kanna baːrgɨnɨj", di͡ebit. </ta>
            <ta e="T402" id="Seg_2994" s="T401">Lɨːbɨra: </ta>
            <ta e="T406" id="Seg_2995" s="T402">"Ohok annɨgar baːrbɨn!", di͡ebit. </ta>
            <ta e="T413" id="Seg_2996" s="T406">Onu Aŋaː Moŋus ihitte da, menʼiːtinen kiːreːri gɨmmɨt. </ta>
            <ta e="T416" id="Seg_2997" s="T413">Onuga Lɨːbɨra hanarar: </ta>
            <ta e="T421" id="Seg_2998" s="T416">"Eheː, menʼiːginen kiːrime — batɨ͡aŋ hu͡oga. </ta>
            <ta e="T423" id="Seg_2999" s="T421">Emeheginen kiːr." </ta>
            <ta e="T426" id="Seg_3000" s="T423">Aŋaː Moŋus emehetinen kiːrbit. </ta>
            <ta e="T436" id="Seg_3001" s="T426">Ol kiːren erdegine Lɨːbɨra kɨtarčɨ barbɨt korgoldʼiːnɨnan emehetin ihinen aspɨt. </ta>
            <ta e="T439" id="Seg_3002" s="T436">Aŋaː Moŋus ölön kaːlbɨt. </ta>
            <ta e="T445" id="Seg_3003" s="T439">Lɨːbɨra tagɨsta da dʼi͡etiger baran kaːlbɨt. </ta>
            <ta e="T448" id="Seg_3004" s="T445">Onon bajan-tajan olorbut. </ta>
            <ta e="T450" id="Seg_3005" s="T448">Dʼe bütte. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_3006" s="T0">Lɨːbɨra</ta>
            <ta e="T2" id="Seg_3007" s="T1">erd-en</ta>
            <ta e="T3" id="Seg_3008" s="T2">kü͡ögeldʼet-en</ta>
            <ta e="T4" id="Seg_3009" s="T3">ih-eːktiː-r</ta>
            <ta e="T5" id="Seg_3010" s="T4">daː</ta>
            <ta e="T6" id="Seg_3011" s="T5">ih-en-ih-en</ta>
            <ta e="T7" id="Seg_3012" s="T6">taːs-ka</ta>
            <ta e="T8" id="Seg_3013" s="T7">kep-pit</ta>
            <ta e="T9" id="Seg_3014" s="T8">tɨː-ta</ta>
            <ta e="T10" id="Seg_3015" s="T9">hɨst-an</ta>
            <ta e="T11" id="Seg_3016" s="T10">kaːl-bɨt</ta>
            <ta e="T12" id="Seg_3017" s="T11">erdiː-ti-nen</ta>
            <ta e="T13" id="Seg_3018" s="T12">oks-u-but</ta>
            <ta e="T14" id="Seg_3019" s="T13">erdiː-te</ta>
            <ta e="T15" id="Seg_3020" s="T14">hɨst-an</ta>
            <ta e="T16" id="Seg_3021" s="T15">kaːl-bɨt</ta>
            <ta e="T17" id="Seg_3022" s="T16">iliː-ti-nen</ta>
            <ta e="T18" id="Seg_3023" s="T17">oks-u-but</ta>
            <ta e="T19" id="Seg_3024" s="T18">iliː-te</ta>
            <ta e="T20" id="Seg_3025" s="T19">hɨst-an</ta>
            <ta e="T21" id="Seg_3026" s="T20">kaːl-bɨt</ta>
            <ta e="T22" id="Seg_3027" s="T21">biːrges</ta>
            <ta e="T23" id="Seg_3028" s="T22">iliː-ti-nen</ta>
            <ta e="T24" id="Seg_3029" s="T23">oks-or</ta>
            <ta e="T25" id="Seg_3030" s="T24">biːrgeh-e</ta>
            <ta e="T26" id="Seg_3031" s="T25">emi͡e</ta>
            <ta e="T27" id="Seg_3032" s="T26">hɨst-an</ta>
            <ta e="T28" id="Seg_3033" s="T27">kaːl-bɨt</ta>
            <ta e="T29" id="Seg_3034" s="T28">atag-ɨ-nan</ta>
            <ta e="T30" id="Seg_3035" s="T29">tep-pit</ta>
            <ta e="T31" id="Seg_3036" s="T30">atag-a</ta>
            <ta e="T32" id="Seg_3037" s="T31">hɨst-an</ta>
            <ta e="T33" id="Seg_3038" s="T32">kaːl-bɨt</ta>
            <ta e="T34" id="Seg_3039" s="T33">nöŋü͡ö</ta>
            <ta e="T35" id="Seg_3040" s="T34">atag-ɨ-nan</ta>
            <ta e="T36" id="Seg_3041" s="T35">tep-pit-e</ta>
            <ta e="T37" id="Seg_3042" s="T36">emi͡e</ta>
            <ta e="T38" id="Seg_3043" s="T37">hɨst-an</ta>
            <ta e="T39" id="Seg_3044" s="T38">kaːl-bɨt</ta>
            <ta e="T40" id="Seg_3045" s="T39">bɨ͡ar-ɨ-nan</ta>
            <ta e="T41" id="Seg_3046" s="T40">üp-püt-e</ta>
            <ta e="T42" id="Seg_3047" s="T41">bɨ͡ar-a</ta>
            <ta e="T43" id="Seg_3048" s="T42">emi͡e</ta>
            <ta e="T44" id="Seg_3049" s="T43">hɨst-an</ta>
            <ta e="T45" id="Seg_3050" s="T44">kaːl-bɨt</ta>
            <ta e="T46" id="Seg_3051" s="T45">onton</ta>
            <ta e="T47" id="Seg_3052" s="T46">menʼiː-ti-nen</ta>
            <ta e="T48" id="Seg_3053" s="T47">oks-u-but</ta>
            <ta e="T49" id="Seg_3054" s="T48">menʼiː-te</ta>
            <ta e="T50" id="Seg_3055" s="T49">emi͡e</ta>
            <ta e="T51" id="Seg_3056" s="T50">hɨst-an</ta>
            <ta e="T52" id="Seg_3057" s="T51">kaːl-bɨt</ta>
            <ta e="T53" id="Seg_3058" s="T52">hɨst-an</ta>
            <ta e="T54" id="Seg_3059" s="T53">tur-dag-a</ta>
            <ta e="T55" id="Seg_3060" s="T54">ol</ta>
            <ta e="T56" id="Seg_3061" s="T55">tur-dag-ɨn</ta>
            <ta e="T57" id="Seg_3062" s="T56">Aŋaː Moŋus</ta>
            <ta e="T58" id="Seg_3063" s="T57">kel-bit</ta>
            <ta e="T59" id="Seg_3064" s="T58">anʼɨkataːn</ta>
            <ta e="T60" id="Seg_3065" s="T59">ki͡eheːŋŋi</ta>
            <ta e="T61" id="Seg_3066" s="T60">hokuːska-laːk</ta>
            <ta e="T62" id="Seg_3067" s="T61">bu͡ol-lu-m</ta>
            <ta e="T63" id="Seg_3068" s="T62">di͡e-bit-i-nen</ta>
            <ta e="T64" id="Seg_3069" s="T63">Lɨːbɨra-nɨ</ta>
            <ta e="T65" id="Seg_3070" s="T64">ɨl-an</ta>
            <ta e="T66" id="Seg_3071" s="T65">karmaːn-ɨ-gar</ta>
            <ta e="T67" id="Seg_3072" s="T66">ukt-an</ta>
            <ta e="T68" id="Seg_3073" s="T67">keːs-pit</ta>
            <ta e="T69" id="Seg_3074" s="T68">dʼi͡e-ti-ger</ta>
            <ta e="T70" id="Seg_3075" s="T69">bar-bɨt</ta>
            <ta e="T71" id="Seg_3076" s="T70">orog-u-gar</ta>
            <ta e="T72" id="Seg_3077" s="T71">tij-en</ta>
            <ta e="T73" id="Seg_3078" s="T72">Aŋaː Moŋus</ta>
            <ta e="T74" id="Seg_3079" s="T73">haːkt-ɨ͡a-n</ta>
            <ta e="T75" id="Seg_3080" s="T74">bagar-bɨt</ta>
            <ta e="T76" id="Seg_3081" s="T75">ol</ta>
            <ta e="T77" id="Seg_3082" s="T76">haːkt-ɨː</ta>
            <ta e="T78" id="Seg_3083" s="T77">olor-dog-una</ta>
            <ta e="T79" id="Seg_3084" s="T78">Lɨːbɨra</ta>
            <ta e="T80" id="Seg_3085" s="T79">karmaːn-ɨ-n</ta>
            <ta e="T81" id="Seg_3086" s="T80">kajagah-ɨ-n</ta>
            <ta e="T82" id="Seg_3087" s="T81">üstün</ta>
            <ta e="T83" id="Seg_3088" s="T82">tüh-en</ta>
            <ta e="T84" id="Seg_3089" s="T83">kaːl-bɨt</ta>
            <ta e="T85" id="Seg_3090" s="T84">Aŋaː Moŋus</ta>
            <ta e="T86" id="Seg_3091" s="T85">tug-u</ta>
            <ta e="T87" id="Seg_3092" s="T86">da</ta>
            <ta e="T88" id="Seg_3093" s="T87">bil-bekke</ta>
            <ta e="T89" id="Seg_3094" s="T88">hɨ͡alɨja-tɨ-n</ta>
            <ta e="T90" id="Seg_3095" s="T89">kötög-ü-n-n-e</ta>
            <ta e="T91" id="Seg_3096" s="T90">da</ta>
            <ta e="T92" id="Seg_3097" s="T91">dʼi͡e-ti-ger</ta>
            <ta e="T93" id="Seg_3098" s="T92">bar-bɨt</ta>
            <ta e="T94" id="Seg_3099" s="T93">dʼi͡e-ti-ger</ta>
            <ta e="T95" id="Seg_3100" s="T94">tah-ɨ-gar</ta>
            <ta e="T96" id="Seg_3101" s="T95">hon-u-n</ta>
            <ta e="T97" id="Seg_3102" s="T96">araŋas-ka</ta>
            <ta e="T98" id="Seg_3103" s="T97">ɨjaː-n</ta>
            <ta e="T99" id="Seg_3104" s="T98">baran</ta>
            <ta e="T100" id="Seg_3105" s="T99">kiːr-bit</ta>
            <ta e="T101" id="Seg_3106" s="T100">emeːksin-e</ta>
            <ta e="T102" id="Seg_3107" s="T101">iːsten-e</ta>
            <ta e="T103" id="Seg_3108" s="T102">olor-or</ta>
            <ta e="T104" id="Seg_3109" s="T103">e-bit</ta>
            <ta e="T105" id="Seg_3110" s="T104">emeːksi͡en</ta>
            <ta e="T106" id="Seg_3111" s="T105">kehiː-bi-n</ta>
            <ta e="T107" id="Seg_3112" s="T106">egel-li-m</ta>
            <ta e="T108" id="Seg_3113" s="T107">hon-u-m</ta>
            <ta e="T109" id="Seg_3114" s="T108">karmaːn-ɨ-gar</ta>
            <ta e="T110" id="Seg_3115" s="T109">baːr</ta>
            <ta e="T111" id="Seg_3116" s="T110">tahaːra</ta>
            <ta e="T112" id="Seg_3117" s="T111">o-nu</ta>
            <ta e="T113" id="Seg_3118" s="T112">killer-en</ta>
            <ta e="T114" id="Seg_3119" s="T113">kör</ta>
            <ta e="T115" id="Seg_3120" s="T114">di͡e-bit</ta>
            <ta e="T116" id="Seg_3121" s="T115">emeːksin-e</ta>
            <ta e="T117" id="Seg_3122" s="T116">ü͡ör-üː-tü-ger</ta>
            <ta e="T118" id="Seg_3123" s="T117">iŋne-leːk</ta>
            <ta e="T119" id="Seg_3124" s="T118">hüːtüg-ü-n</ta>
            <ta e="T120" id="Seg_3125" s="T119">ɨjɨr-an</ta>
            <ta e="T121" id="Seg_3126" s="T120">keːs-pit</ta>
            <ta e="T122" id="Seg_3127" s="T121">taks-a</ta>
            <ta e="T123" id="Seg_3128" s="T122">köp-püt</ta>
            <ta e="T124" id="Seg_3129" s="T123">bul-bakka</ta>
            <ta e="T125" id="Seg_3130" s="T124">tönn-ön</ta>
            <ta e="T126" id="Seg_3131" s="T125">kiːr-bit</ta>
            <ta e="T127" id="Seg_3132" s="T126">karmaːŋ-ŋa-r</ta>
            <ta e="T128" id="Seg_3133" s="T127">tu͡ok</ta>
            <ta e="T129" id="Seg_3134" s="T128">da</ta>
            <ta e="T130" id="Seg_3135" s="T129">hu͡ok</ta>
            <ta e="T131" id="Seg_3136" s="T130">di͡e-bit</ta>
            <ta e="T132" id="Seg_3137" s="T131">Aŋaː Moŋus</ta>
            <ta e="T133" id="Seg_3138" s="T132">beje-ŋ</ta>
            <ta e="T134" id="Seg_3139" s="T133">hi͡e-teg-i-ŋ</ta>
            <ta e="T135" id="Seg_3140" s="T134">diːn</ta>
            <ta e="T136" id="Seg_3141" s="T135">di͡e-bit</ta>
            <ta e="T137" id="Seg_3142" s="T136">da</ta>
            <ta e="T138" id="Seg_3143" s="T137">emeːksin</ta>
            <ta e="T139" id="Seg_3144" s="T138">bɨ͡ar-ɨ-n</ta>
            <ta e="T140" id="Seg_3145" s="T139">kaj-a</ta>
            <ta e="T141" id="Seg_3146" s="T140">tard-ar</ta>
            <ta e="T142" id="Seg_3147" s="T141">emeːksin</ta>
            <ta e="T143" id="Seg_3148" s="T142">bɨ͡ar-ɨ-ttan</ta>
            <ta e="T144" id="Seg_3149" s="T143">iŋne-leːk</ta>
            <ta e="T145" id="Seg_3150" s="T144">hüːtük</ta>
            <ta e="T146" id="Seg_3151" s="T145">kɨlɨr</ta>
            <ta e="T147" id="Seg_3152" s="T146">gɨn-a</ta>
            <ta e="T148" id="Seg_3153" s="T147">tüs-püt</ta>
            <ta e="T149" id="Seg_3154" s="T148">oː</ta>
            <ta e="T150" id="Seg_3155" s="T149">kaːrɨ͡an</ta>
            <ta e="T151" id="Seg_3156" s="T150">dʼaktar-bɨ-n</ta>
            <ta e="T152" id="Seg_3157" s="T151">üːgüleː-bit</ta>
            <ta e="T153" id="Seg_3158" s="T152">Aŋaː Moŋus</ta>
            <ta e="T154" id="Seg_3159" s="T153">töttörü</ta>
            <ta e="T155" id="Seg_3160" s="T154">hüːr-büt</ta>
            <ta e="T156" id="Seg_3161" s="T155">Lɨːbɨra</ta>
            <ta e="T157" id="Seg_3162" s="T156">orok-ko</ta>
            <ta e="T158" id="Seg_3163" s="T157">bu</ta>
            <ta e="T159" id="Seg_3164" s="T158">čaŋkaj-a</ta>
            <ta e="T160" id="Seg_3165" s="T159">hɨt-ar</ta>
            <ta e="T161" id="Seg_3166" s="T160">ɨl-ar</ta>
            <ta e="T162" id="Seg_3167" s="T161">da</ta>
            <ta e="T163" id="Seg_3168" s="T162">karmaːn-ɨ-gar</ta>
            <ta e="T164" id="Seg_3169" s="T163">ukt-ar</ta>
            <ta e="T165" id="Seg_3170" s="T164">töttörü</ta>
            <ta e="T166" id="Seg_3171" s="T165">hüːr-en</ta>
            <ta e="T167" id="Seg_3172" s="T166">kel-er</ta>
            <ta e="T168" id="Seg_3173" s="T167">dʼi͡e-ti-ger</ta>
            <ta e="T169" id="Seg_3174" s="T168">kiːr-en</ta>
            <ta e="T170" id="Seg_3175" s="T169">Lɨːbɨra-nɨ</ta>
            <ta e="T171" id="Seg_3176" s="T170">iːkeːptiːŋ-ŋe</ta>
            <ta e="T172" id="Seg_3177" s="T171">ɨjaː-n</ta>
            <ta e="T173" id="Seg_3178" s="T172">keːh-er</ta>
            <ta e="T174" id="Seg_3179" s="T173">Aŋaː Moŋus</ta>
            <ta e="T175" id="Seg_3180" s="T174">hette</ta>
            <ta e="T176" id="Seg_3181" s="T175">tarakaːj</ta>
            <ta e="T177" id="Seg_3182" s="T176">ogo-loːk</ta>
            <ta e="T178" id="Seg_3183" s="T177">e-bit</ta>
            <ta e="T179" id="Seg_3184" s="T178">ol</ta>
            <ta e="T180" id="Seg_3185" s="T179">ogo-lor-u-kar</ta>
            <ta e="T181" id="Seg_3186" s="T180">di͡e-bit</ta>
            <ta e="T182" id="Seg_3187" s="T181">ogo-loːr</ta>
            <ta e="T183" id="Seg_3188" s="T182">Lɨːbɨra</ta>
            <ta e="T184" id="Seg_3189" s="T183">hɨ͡a-ta</ta>
            <ta e="T185" id="Seg_3190" s="T184">test-i͡eg-e</ta>
            <ta e="T186" id="Seg_3191" s="T185">kör-ör</ta>
            <ta e="T187" id="Seg_3192" s="T186">bu͡ol-u-ŋ</ta>
            <ta e="T188" id="Seg_3193" s="T187">kel-er-be-r</ta>
            <ta e="T189" id="Seg_3194" s="T188">kü͡öst-eːr-i-ŋ</ta>
            <ta e="T190" id="Seg_3195" s="T189">di͡e-t-e</ta>
            <ta e="T191" id="Seg_3196" s="T190">da</ta>
            <ta e="T192" id="Seg_3197" s="T191">taks-an</ta>
            <ta e="T193" id="Seg_3198" s="T192">kaːl-bɨt</ta>
            <ta e="T194" id="Seg_3199" s="T193">töhö</ta>
            <ta e="T195" id="Seg_3200" s="T194">da</ta>
            <ta e="T196" id="Seg_3201" s="T195">bu͡ol-bakka</ta>
            <ta e="T197" id="Seg_3202" s="T196">Lɨːbɨra</ta>
            <ta e="T198" id="Seg_3203" s="T197">dʼe</ta>
            <ta e="T199" id="Seg_3204" s="T198">iːkteː-bit</ta>
            <ta e="T200" id="Seg_3205" s="T199">ogo-lor</ta>
            <ta e="T201" id="Seg_3206" s="T200">onno</ta>
            <ta e="T202" id="Seg_3207" s="T201">üːgül-e-s-pit-ter</ta>
            <ta e="T203" id="Seg_3208" s="T202">anʼikataːn</ta>
            <ta e="T204" id="Seg_3209" s="T203">Lɨːbɨra</ta>
            <ta e="T205" id="Seg_3210" s="T204">ɨbagas</ta>
            <ta e="T206" id="Seg_3211" s="T205">hɨ͡a-ta</ta>
            <ta e="T207" id="Seg_3212" s="T206">tog-u-n-n-a</ta>
            <ta e="T208" id="Seg_3213" s="T207">onton</ta>
            <ta e="T209" id="Seg_3214" s="T208">Lɨːbɨra</ta>
            <ta e="T210" id="Seg_3215" s="T209">dʼe</ta>
            <ta e="T211" id="Seg_3216" s="T210">haːktɨː-r</ta>
            <ta e="T212" id="Seg_3217" s="T211">ogo-lor</ta>
            <ta e="T213" id="Seg_3218" s="T212">üːgül-e-s-pit-ter</ta>
            <ta e="T214" id="Seg_3219" s="T213">anʼikataːn</ta>
            <ta e="T215" id="Seg_3220" s="T214">Lɨːbɨra</ta>
            <ta e="T216" id="Seg_3221" s="T215">kojuː</ta>
            <ta e="T217" id="Seg_3222" s="T216">hɨ͡a-ta</ta>
            <ta e="T218" id="Seg_3223" s="T217">tog-u-n-n-a</ta>
            <ta e="T219" id="Seg_3224" s="T218">Lɨːbɨra</ta>
            <ta e="T220" id="Seg_3225" s="T219">ogo-lor-go</ta>
            <ta e="T221" id="Seg_3226" s="T220">di͡e-bit</ta>
            <ta e="T222" id="Seg_3227" s="T221">ogo-loːr</ta>
            <ta e="T223" id="Seg_3228" s="T222">tüher-i-ŋ</ta>
            <ta e="T224" id="Seg_3229" s="T223">miːgi-n</ta>
            <ta e="T225" id="Seg_3230" s="T224">min</ta>
            <ta e="T226" id="Seg_3231" s="T225">ehi͡e-ke</ta>
            <ta e="T227" id="Seg_3232" s="T226">lu͡osku</ta>
            <ta e="T228" id="Seg_3233" s="T227">oŋor-on</ta>
            <ta e="T229" id="Seg_3234" s="T228">bi͡er-i͡e-m</ta>
            <ta e="T230" id="Seg_3235" s="T229">min</ta>
            <ta e="T231" id="Seg_3236" s="T230">hɨ͡a-bɨ-n</ta>
            <ta e="T232" id="Seg_3237" s="T231">hiː-r-giti-ger</ta>
            <ta e="T233" id="Seg_3238" s="T232">üčügej</ta>
            <ta e="T234" id="Seg_3239" s="T233">bu͡ol-u͡o</ta>
            <ta e="T235" id="Seg_3240" s="T234">kaja</ta>
            <ta e="T236" id="Seg_3241" s="T235">erej</ta>
            <ta e="T237" id="Seg_3242" s="T236">keje</ta>
            <ta e="T238" id="Seg_3243" s="T237">Lɨːbɨra-nɨ</ta>
            <ta e="T239" id="Seg_3244" s="T238">tüher-bit-ter</ta>
            <ta e="T240" id="Seg_3245" s="T239">Lɨːbɨra</ta>
            <ta e="T241" id="Seg_3246" s="T240">di͡e-bit</ta>
            <ta e="T242" id="Seg_3247" s="T241">egel-i-ŋ</ta>
            <ta e="T243" id="Seg_3248" s="T242">teːte-git</ta>
            <ta e="T244" id="Seg_3249" s="T243">hɨtɨː</ta>
            <ta e="T245" id="Seg_3250" s="T244">hüge-ti-n</ta>
            <ta e="T246" id="Seg_3251" s="T245">egel-en</ta>
            <ta e="T247" id="Seg_3252" s="T246">bi͡er-bit-ter</ta>
            <ta e="T248" id="Seg_3253" s="T247">oroŋ-ŋutu-gar</ta>
            <ta e="T249" id="Seg_3254" s="T248">kör-ö</ta>
            <ta e="T250" id="Seg_3255" s="T249">hɨt-ɨ-ŋ</ta>
            <ta e="T251" id="Seg_3256" s="T250">kajdak</ta>
            <ta e="T252" id="Seg_3257" s="T251">min</ta>
            <ta e="T253" id="Seg_3258" s="T252">oŋor-o-bun</ta>
            <ta e="T254" id="Seg_3259" s="T253">ogo-lor</ta>
            <ta e="T255" id="Seg_3260" s="T254">hɨt-an</ta>
            <ta e="T256" id="Seg_3261" s="T255">kaːl-bɨt-tar</ta>
            <ta e="T257" id="Seg_3262" s="T256">ol</ta>
            <ta e="T258" id="Seg_3263" s="T257">hɨp-pɨt-tarɨ-gar</ta>
            <ta e="T259" id="Seg_3264" s="T258">Lɨːbɨra</ta>
            <ta e="T260" id="Seg_3265" s="T259">menʼiː-leri-n</ta>
            <ta e="T261" id="Seg_3266" s="T260">bɨh-a</ta>
            <ta e="T262" id="Seg_3267" s="T261">kerd-iteleː-bit</ta>
            <ta e="T263" id="Seg_3268" s="T262">et-teri-n</ta>
            <ta e="T264" id="Seg_3269" s="T263">kü͡östeː-n</ta>
            <ta e="T265" id="Seg_3270" s="T264">keːs-pit</ta>
            <ta e="T266" id="Seg_3271" s="T265">menʼiː-ler-i</ta>
            <ta e="T267" id="Seg_3272" s="T266">oroŋ-ŋo</ta>
            <ta e="T268" id="Seg_3273" s="T267">kečigirečči-keːn</ta>
            <ta e="T269" id="Seg_3274" s="T268">uːr-an</ta>
            <ta e="T270" id="Seg_3275" s="T269">baran</ta>
            <ta e="T271" id="Seg_3276" s="T270">hu͡organ-ɨ-nan</ta>
            <ta e="T272" id="Seg_3277" s="T271">hab-ɨtalaː-bɨt</ta>
            <ta e="T273" id="Seg_3278" s="T272">korgoldʼiːn-ɨ</ta>
            <ta e="T274" id="Seg_3279" s="T273">ɨl-an</ta>
            <ta e="T275" id="Seg_3280" s="T274">baran</ta>
            <ta e="T276" id="Seg_3281" s="T275">ohok</ta>
            <ta e="T277" id="Seg_3282" s="T276">ann-ɨ-gar</ta>
            <ta e="T278" id="Seg_3283" s="T277">kiːr-en</ta>
            <ta e="T279" id="Seg_3284" s="T278">kaːl-bɨt</ta>
            <ta e="T280" id="Seg_3285" s="T279">bu</ta>
            <ta e="T281" id="Seg_3286" s="T280">hɨt-tag-ɨna</ta>
            <ta e="T282" id="Seg_3287" s="T281">Aŋaː Moŋus</ta>
            <ta e="T283" id="Seg_3288" s="T282">kel-bit</ta>
            <ta e="T284" id="Seg_3289" s="T283">Aŋaː Moŋus</ta>
            <ta e="T285" id="Seg_3290" s="T284">kel-en</ta>
            <ta e="T286" id="Seg_3291" s="T285">üːgüleː-bit</ta>
            <ta e="T287" id="Seg_3292" s="T286">ogo-loːr</ta>
            <ta e="T288" id="Seg_3293" s="T287">aːŋ-ŋɨtɨ-n</ta>
            <ta e="T289" id="Seg_3294" s="T288">arɨj-ɨ-ŋ</ta>
            <ta e="T290" id="Seg_3295" s="T289">kim</ta>
            <ta e="T291" id="Seg_3296" s="T290">da</ta>
            <ta e="T292" id="Seg_3297" s="T291">arɨj-bat</ta>
            <ta e="T293" id="Seg_3298" s="T292">kas-ta</ta>
            <ta e="T294" id="Seg_3299" s="T293">da</ta>
            <ta e="T295" id="Seg_3300" s="T294">üːgüleː-bit</ta>
            <ta e="T296" id="Seg_3301" s="T295">ogo-lor</ta>
            <ta e="T297" id="Seg_3302" s="T296">törüt</ta>
            <ta e="T298" id="Seg_3303" s="T297">arɨj-batak-tar</ta>
            <ta e="T299" id="Seg_3304" s="T298">aːn-ɨ-n</ta>
            <ta e="T300" id="Seg_3305" s="T299">kost-uː</ta>
            <ta e="T301" id="Seg_3306" s="T300">tard-an</ta>
            <ta e="T302" id="Seg_3307" s="T301">kiːr-bit</ta>
            <ta e="T303" id="Seg_3308" s="T302">kör-büt-e</ta>
            <ta e="T304" id="Seg_3309" s="T303">et</ta>
            <ta e="T305" id="Seg_3310" s="T304">buh-a</ta>
            <ta e="T306" id="Seg_3311" s="T305">tur-ar</ta>
            <ta e="T307" id="Seg_3312" s="T306">ogo-lor</ta>
            <ta e="T308" id="Seg_3313" s="T307">utuj-a</ta>
            <ta e="T309" id="Seg_3314" s="T308">hɨt-al-lar</ta>
            <ta e="T310" id="Seg_3315" s="T309">oː</ta>
            <ta e="T311" id="Seg_3316" s="T310">ogo-lor-u-m</ta>
            <ta e="T312" id="Seg_3317" s="T311">Lɨːbɨra</ta>
            <ta e="T313" id="Seg_3318" s="T312">hɨ͡a-tɨ-ttan</ta>
            <ta e="T314" id="Seg_3319" s="T313">tot-on</ta>
            <ta e="T315" id="Seg_3320" s="T314">baran</ta>
            <ta e="T316" id="Seg_3321" s="T315">utuj-a</ta>
            <ta e="T317" id="Seg_3322" s="T316">hɨt-al-lar</ta>
            <ta e="T318" id="Seg_3323" s="T317">e-bit</ta>
            <ta e="T319" id="Seg_3324" s="T318">bu͡o</ta>
            <ta e="T320" id="Seg_3325" s="T319">diː-r</ta>
            <ta e="T321" id="Seg_3326" s="T320">kü͡öh-ü</ta>
            <ta e="T322" id="Seg_3327" s="T321">tahaːr-ar</ta>
            <ta e="T323" id="Seg_3328" s="T322">hüreg-i-tten</ta>
            <ta e="T324" id="Seg_3329" s="T323">amsaj-ar</ta>
            <ta e="T325" id="Seg_3330" s="T324">hüreg-e</ta>
            <ta e="T326" id="Seg_3331" s="T325">tar-pɨt</ta>
            <ta e="T327" id="Seg_3332" s="T326">oː</ta>
            <ta e="T328" id="Seg_3333" s="T327">Lɨːbɨra</ta>
            <ta e="T329" id="Seg_3334" s="T328">min</ta>
            <ta e="T330" id="Seg_3335" s="T329">uruː-m</ta>
            <ta e="T331" id="Seg_3336" s="T330">e-bit</ta>
            <ta e="T332" id="Seg_3337" s="T331">duː</ta>
            <ta e="T333" id="Seg_3338" s="T332">di͡e-bit</ta>
            <ta e="T334" id="Seg_3339" s="T333">taːl</ta>
            <ta e="T335" id="Seg_3340" s="T334">hi͡e-bit</ta>
            <ta e="T336" id="Seg_3341" s="T335">taːl-a</ta>
            <ta e="T337" id="Seg_3342" s="T336">tar-pɨt</ta>
            <ta e="T338" id="Seg_3343" s="T337">oː</ta>
            <ta e="T339" id="Seg_3344" s="T338">Lɨːbɨra</ta>
            <ta e="T340" id="Seg_3345" s="T339">ile</ta>
            <ta e="T341" id="Seg_3346" s="T340">uruː-m</ta>
            <ta e="T342" id="Seg_3347" s="T341">e-bit</ta>
            <ta e="T343" id="Seg_3348" s="T342">duː</ta>
            <ta e="T344" id="Seg_3349" s="T343">di͡e-bit</ta>
            <ta e="T345" id="Seg_3350" s="T344">onton</ta>
            <ta e="T346" id="Seg_3351" s="T345">tur-but</ta>
            <ta e="T347" id="Seg_3352" s="T346">ogo-lor-u-n</ta>
            <ta e="T348" id="Seg_3353" s="T347">arɨj-a</ta>
            <ta e="T349" id="Seg_3354" s="T348">tar-pɨt-a</ta>
            <ta e="T350" id="Seg_3355" s="T349">hette</ta>
            <ta e="T351" id="Seg_3356" s="T350">menʼiː</ta>
            <ta e="T352" id="Seg_3357" s="T351">čekeni-s</ta>
            <ta e="T353" id="Seg_3358" s="T352">gɨm-mɨt-tar</ta>
            <ta e="T354" id="Seg_3359" s="T353">hohuj-a</ta>
            <ta e="T355" id="Seg_3360" s="T354">kɨtta</ta>
            <ta e="T356" id="Seg_3361" s="T355">Aŋaː Moŋus</ta>
            <ta e="T357" id="Seg_3362" s="T356">üːgüleː-bit</ta>
            <ta e="T358" id="Seg_3363" s="T357">Lɨːbɨra</ta>
            <ta e="T359" id="Seg_3364" s="T358">kanna</ta>
            <ta e="T360" id="Seg_3365" s="T359">baːr-gɨn=ɨj</ta>
            <ta e="T361" id="Seg_3366" s="T360">Lɨːbɨra</ta>
            <ta e="T362" id="Seg_3367" s="T361">tahaːra</ta>
            <ta e="T363" id="Seg_3368" s="T362">baːr-bɨn</ta>
            <ta e="T364" id="Seg_3369" s="T363">di͡e-bit</ta>
            <ta e="T365" id="Seg_3370" s="T364">o-nu</ta>
            <ta e="T366" id="Seg_3371" s="T365">ihit-t-e</ta>
            <ta e="T367" id="Seg_3372" s="T366">da</ta>
            <ta e="T368" id="Seg_3373" s="T367">dʼi͡e-ti-n</ta>
            <ta e="T369" id="Seg_3374" s="T368">kaj-a</ta>
            <ta e="T370" id="Seg_3375" s="T369">köt-ön</ta>
            <ta e="T371" id="Seg_3376" s="T370">taks-ar</ta>
            <ta e="T372" id="Seg_3377" s="T371">Lɨːbɨra</ta>
            <ta e="T373" id="Seg_3378" s="T372">kanna</ta>
            <ta e="T374" id="Seg_3379" s="T373">da</ta>
            <ta e="T375" id="Seg_3380" s="T374">hu͡ok</ta>
            <ta e="T376" id="Seg_3381" s="T375">Lɨːbɨraː</ta>
            <ta e="T377" id="Seg_3382" s="T376">kanna-gɨn=ɨj</ta>
            <ta e="T378" id="Seg_3383" s="T377">dʼi͡e</ta>
            <ta e="T379" id="Seg_3384" s="T378">ih-i-ger</ta>
            <ta e="T380" id="Seg_3385" s="T379">baːr-bɨn</ta>
            <ta e="T381" id="Seg_3386" s="T380">di͡e-bit</ta>
            <ta e="T382" id="Seg_3387" s="T381">Lɨːbɨra</ta>
            <ta e="T383" id="Seg_3388" s="T382">dʼi͡e-ge</ta>
            <ta e="T384" id="Seg_3389" s="T383">köt-ön</ta>
            <ta e="T385" id="Seg_3390" s="T384">tüs-püt</ta>
            <ta e="T386" id="Seg_3391" s="T385">Lɨːbɨra</ta>
            <ta e="T387" id="Seg_3392" s="T386">hu͡ok</ta>
            <ta e="T388" id="Seg_3393" s="T387">Lɨːbɨraː</ta>
            <ta e="T389" id="Seg_3394" s="T388">kanna-gɨn=ɨj</ta>
            <ta e="T390" id="Seg_3395" s="T389">ü͡öles-ke</ta>
            <ta e="T391" id="Seg_3396" s="T390">baːr-bɨn</ta>
            <ta e="T392" id="Seg_3397" s="T391">ü͡öles-ke</ta>
            <ta e="T393" id="Seg_3398" s="T392">körüleː-bit-e</ta>
            <ta e="T394" id="Seg_3399" s="T393">Lɨːbɨra</ta>
            <ta e="T395" id="Seg_3400" s="T394">kanna</ta>
            <ta e="T396" id="Seg_3401" s="T395">da</ta>
            <ta e="T397" id="Seg_3402" s="T396">hu͡ok</ta>
            <ta e="T398" id="Seg_3403" s="T397">Lɨːbɨraː</ta>
            <ta e="T399" id="Seg_3404" s="T398">kanna</ta>
            <ta e="T400" id="Seg_3405" s="T399">baːr-gɨn=ɨj</ta>
            <ta e="T401" id="Seg_3406" s="T400">di͡e-bit</ta>
            <ta e="T402" id="Seg_3407" s="T401">Lɨːbɨra</ta>
            <ta e="T403" id="Seg_3408" s="T402">ohok</ta>
            <ta e="T404" id="Seg_3409" s="T403">ann-ɨ-gar</ta>
            <ta e="T405" id="Seg_3410" s="T404">baːr-bɨn</ta>
            <ta e="T406" id="Seg_3411" s="T405">di͡e-bit</ta>
            <ta e="T407" id="Seg_3412" s="T406">o-nu</ta>
            <ta e="T408" id="Seg_3413" s="T407">Aŋaː Moŋus</ta>
            <ta e="T409" id="Seg_3414" s="T408">ihit-t-e</ta>
            <ta e="T410" id="Seg_3415" s="T409">da</ta>
            <ta e="T411" id="Seg_3416" s="T410">menʼiː-ti-nen</ta>
            <ta e="T412" id="Seg_3417" s="T411">kiːr-eːri</ta>
            <ta e="T413" id="Seg_3418" s="T412">gɨm-mɨt</ta>
            <ta e="T414" id="Seg_3419" s="T413">onu-ga</ta>
            <ta e="T415" id="Seg_3420" s="T414">Lɨːbɨra</ta>
            <ta e="T416" id="Seg_3421" s="T415">hanar-ar</ta>
            <ta e="T417" id="Seg_3422" s="T416">eheː</ta>
            <ta e="T418" id="Seg_3423" s="T417">menʼiː-gi-nen</ta>
            <ta e="T419" id="Seg_3424" s="T418">kiːr-i-me</ta>
            <ta e="T420" id="Seg_3425" s="T419">bat-ɨ͡a-ŋ</ta>
            <ta e="T421" id="Seg_3426" s="T420">hu͡og-a</ta>
            <ta e="T422" id="Seg_3427" s="T421">emehe-gi-nen</ta>
            <ta e="T423" id="Seg_3428" s="T422">kiːr</ta>
            <ta e="T424" id="Seg_3429" s="T423">Aŋaː Moŋus</ta>
            <ta e="T425" id="Seg_3430" s="T424">emehe-ti-nen</ta>
            <ta e="T426" id="Seg_3431" s="T425">kiːr-bit</ta>
            <ta e="T427" id="Seg_3432" s="T426">ol</ta>
            <ta e="T428" id="Seg_3433" s="T427">kiːr-en</ta>
            <ta e="T429" id="Seg_3434" s="T428">er-deg-ine</ta>
            <ta e="T430" id="Seg_3435" s="T429">Lɨːbɨra</ta>
            <ta e="T431" id="Seg_3436" s="T430">kɨtar-čɨ</ta>
            <ta e="T432" id="Seg_3437" s="T431">bar-bɨt</ta>
            <ta e="T433" id="Seg_3438" s="T432">korgoldʼiːn-ɨ-nan</ta>
            <ta e="T434" id="Seg_3439" s="T433">emehe-ti-n</ta>
            <ta e="T435" id="Seg_3440" s="T434">ih-i-nen</ta>
            <ta e="T436" id="Seg_3441" s="T435">as-pɨt</ta>
            <ta e="T437" id="Seg_3442" s="T436">Aŋaː Moŋus</ta>
            <ta e="T438" id="Seg_3443" s="T437">öl-ön</ta>
            <ta e="T439" id="Seg_3444" s="T438">kaːl-bɨt</ta>
            <ta e="T440" id="Seg_3445" s="T439">Lɨːbɨra</ta>
            <ta e="T441" id="Seg_3446" s="T440">tagɨs-t-a</ta>
            <ta e="T442" id="Seg_3447" s="T441">da</ta>
            <ta e="T443" id="Seg_3448" s="T442">dʼi͡e-ti-ger</ta>
            <ta e="T444" id="Seg_3449" s="T443">bar-an</ta>
            <ta e="T445" id="Seg_3450" s="T444">kaːl-bɨt</ta>
            <ta e="T446" id="Seg_3451" s="T445">onon</ta>
            <ta e="T447" id="Seg_3452" s="T446">baj-an-taj-an</ta>
            <ta e="T448" id="Seg_3453" s="T447">olor-but</ta>
            <ta e="T449" id="Seg_3454" s="T448">dʼe</ta>
            <ta e="T450" id="Seg_3455" s="T449">büt-t-e</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_3456" s="T0">Lɨːbɨra</ta>
            <ta e="T2" id="Seg_3457" s="T1">ert-An</ta>
            <ta e="T3" id="Seg_3458" s="T2">kü͡ögeldʼet-An</ta>
            <ta e="T4" id="Seg_3459" s="T3">is-AːktAː-Ar</ta>
            <ta e="T5" id="Seg_3460" s="T4">da</ta>
            <ta e="T6" id="Seg_3461" s="T5">is-An-is-An</ta>
            <ta e="T7" id="Seg_3462" s="T6">taːs-GA</ta>
            <ta e="T8" id="Seg_3463" s="T7">kep-BIT</ta>
            <ta e="T9" id="Seg_3464" s="T8">tɨː-tA</ta>
            <ta e="T10" id="Seg_3465" s="T9">hɨhɨn-An</ta>
            <ta e="T11" id="Seg_3466" s="T10">kaːl-BIT</ta>
            <ta e="T12" id="Seg_3467" s="T11">erdiː-tI-nAn</ta>
            <ta e="T13" id="Seg_3468" s="T12">ogus-I-BIT</ta>
            <ta e="T14" id="Seg_3469" s="T13">erdiː-tA</ta>
            <ta e="T15" id="Seg_3470" s="T14">hɨhɨn-An</ta>
            <ta e="T16" id="Seg_3471" s="T15">kaːl-BIT</ta>
            <ta e="T17" id="Seg_3472" s="T16">iliː-tI-nAn</ta>
            <ta e="T18" id="Seg_3473" s="T17">ogus-I-BIT</ta>
            <ta e="T19" id="Seg_3474" s="T18">iliː-tA</ta>
            <ta e="T20" id="Seg_3475" s="T19">hɨhɨn-An</ta>
            <ta e="T21" id="Seg_3476" s="T20">kaːl-BIT</ta>
            <ta e="T22" id="Seg_3477" s="T21">biːrges</ta>
            <ta e="T23" id="Seg_3478" s="T22">iliː-tI-nAn</ta>
            <ta e="T24" id="Seg_3479" s="T23">ogus-Ar</ta>
            <ta e="T25" id="Seg_3480" s="T24">biːrges-tA</ta>
            <ta e="T26" id="Seg_3481" s="T25">emi͡e</ta>
            <ta e="T27" id="Seg_3482" s="T26">hɨhɨn-An</ta>
            <ta e="T28" id="Seg_3483" s="T27">kaːl-BIT</ta>
            <ta e="T29" id="Seg_3484" s="T28">atak-tI-nAn</ta>
            <ta e="T30" id="Seg_3485" s="T29">tep-BIT</ta>
            <ta e="T31" id="Seg_3486" s="T30">atak-tA</ta>
            <ta e="T32" id="Seg_3487" s="T31">hɨhɨn-An</ta>
            <ta e="T33" id="Seg_3488" s="T32">kaːl-BIT</ta>
            <ta e="T34" id="Seg_3489" s="T33">nöŋü͡ö</ta>
            <ta e="T35" id="Seg_3490" s="T34">atak-tI-nAn</ta>
            <ta e="T36" id="Seg_3491" s="T35">tep-BIT-tA</ta>
            <ta e="T37" id="Seg_3492" s="T36">emi͡e</ta>
            <ta e="T38" id="Seg_3493" s="T37">hɨhɨn-An</ta>
            <ta e="T39" id="Seg_3494" s="T38">kaːl-BIT</ta>
            <ta e="T40" id="Seg_3495" s="T39">bɨ͡ar-tI-nAn</ta>
            <ta e="T41" id="Seg_3496" s="T40">üt-BIT-tA</ta>
            <ta e="T42" id="Seg_3497" s="T41">bɨ͡ar-tA</ta>
            <ta e="T43" id="Seg_3498" s="T42">emi͡e</ta>
            <ta e="T44" id="Seg_3499" s="T43">hɨhɨn-An</ta>
            <ta e="T45" id="Seg_3500" s="T44">kaːl-BIT</ta>
            <ta e="T46" id="Seg_3501" s="T45">onton</ta>
            <ta e="T47" id="Seg_3502" s="T46">menʼiː-tI-nAn</ta>
            <ta e="T48" id="Seg_3503" s="T47">ogus-I-BIT</ta>
            <ta e="T49" id="Seg_3504" s="T48">menʼiː-tA</ta>
            <ta e="T50" id="Seg_3505" s="T49">emi͡e</ta>
            <ta e="T51" id="Seg_3506" s="T50">hɨhɨn-An</ta>
            <ta e="T52" id="Seg_3507" s="T51">kaːl-BIT</ta>
            <ta e="T53" id="Seg_3508" s="T52">hɨhɨn-An</ta>
            <ta e="T54" id="Seg_3509" s="T53">tur-TAK-tA</ta>
            <ta e="T55" id="Seg_3510" s="T54">ol</ta>
            <ta e="T56" id="Seg_3511" s="T55">tur-TAK-In</ta>
            <ta e="T57" id="Seg_3512" s="T56">Aŋaː Moŋus</ta>
            <ta e="T58" id="Seg_3513" s="T57">kel-BIT</ta>
            <ta e="T59" id="Seg_3514" s="T58">anʼɨkataːn</ta>
            <ta e="T60" id="Seg_3515" s="T59">ki͡eheːŋŋi</ta>
            <ta e="T61" id="Seg_3516" s="T60">hokuːska-LAːK</ta>
            <ta e="T62" id="Seg_3517" s="T61">bu͡ol-TI-m</ta>
            <ta e="T63" id="Seg_3518" s="T62">di͡e-BIT-tI-nAn</ta>
            <ta e="T64" id="Seg_3519" s="T63">Lɨːbɨra-nI</ta>
            <ta e="T65" id="Seg_3520" s="T64">ɨl-An</ta>
            <ta e="T66" id="Seg_3521" s="T65">karmaːn-tI-GAr</ta>
            <ta e="T67" id="Seg_3522" s="T66">ugun-An</ta>
            <ta e="T68" id="Seg_3523" s="T67">keːs-BIT</ta>
            <ta e="T69" id="Seg_3524" s="T68">dʼi͡e-tI-GAr</ta>
            <ta e="T70" id="Seg_3525" s="T69">bar-BIT</ta>
            <ta e="T71" id="Seg_3526" s="T70">orok-tI-GAr</ta>
            <ta e="T72" id="Seg_3527" s="T71">tij-An</ta>
            <ta e="T73" id="Seg_3528" s="T72">Aŋaː Moŋus</ta>
            <ta e="T74" id="Seg_3529" s="T73">haːktaː-IAK.[tI]-n</ta>
            <ta e="T75" id="Seg_3530" s="T74">bagar-BIT</ta>
            <ta e="T76" id="Seg_3531" s="T75">ol</ta>
            <ta e="T77" id="Seg_3532" s="T76">haːktaː-A</ta>
            <ta e="T78" id="Seg_3533" s="T77">olor-TAK-InA</ta>
            <ta e="T79" id="Seg_3534" s="T78">Lɨːbɨra</ta>
            <ta e="T80" id="Seg_3535" s="T79">karmaːn-tI-n</ta>
            <ta e="T81" id="Seg_3536" s="T80">kajagas-tI-n</ta>
            <ta e="T82" id="Seg_3537" s="T81">üstün</ta>
            <ta e="T83" id="Seg_3538" s="T82">tüs-An</ta>
            <ta e="T84" id="Seg_3539" s="T83">kaːl-BIT</ta>
            <ta e="T85" id="Seg_3540" s="T84">Aŋaː Moŋus</ta>
            <ta e="T86" id="Seg_3541" s="T85">tu͡ok-nI</ta>
            <ta e="T87" id="Seg_3542" s="T86">da</ta>
            <ta e="T88" id="Seg_3543" s="T87">bil-BAkkA</ta>
            <ta e="T89" id="Seg_3544" s="T88">hɨ͡aldʼa-tI-n</ta>
            <ta e="T90" id="Seg_3545" s="T89">kötök-I-n-TI-tA</ta>
            <ta e="T91" id="Seg_3546" s="T90">da</ta>
            <ta e="T92" id="Seg_3547" s="T91">dʼi͡e-tI-GAr</ta>
            <ta e="T93" id="Seg_3548" s="T92">bar-BIT</ta>
            <ta e="T94" id="Seg_3549" s="T93">dʼi͡e-tI-GAr</ta>
            <ta e="T95" id="Seg_3550" s="T94">tas-tI-GAr</ta>
            <ta e="T96" id="Seg_3551" s="T95">hon-tI-n</ta>
            <ta e="T97" id="Seg_3552" s="T96">araŋas-GA</ta>
            <ta e="T98" id="Seg_3553" s="T97">ɨjaː-An</ta>
            <ta e="T99" id="Seg_3554" s="T98">baran</ta>
            <ta e="T100" id="Seg_3555" s="T99">kiːr-BIT</ta>
            <ta e="T101" id="Seg_3556" s="T100">emeːksin-tA</ta>
            <ta e="T102" id="Seg_3557" s="T101">iːsten-A</ta>
            <ta e="T103" id="Seg_3558" s="T102">olor-Ar</ta>
            <ta e="T104" id="Seg_3559" s="T103">e-BIT</ta>
            <ta e="T105" id="Seg_3560" s="T104">emeːksin</ta>
            <ta e="T106" id="Seg_3561" s="T105">kehiː-BI-n</ta>
            <ta e="T107" id="Seg_3562" s="T106">egel-TI-m</ta>
            <ta e="T108" id="Seg_3563" s="T107">hon-I-m</ta>
            <ta e="T109" id="Seg_3564" s="T108">karmaːn-tI-GAr</ta>
            <ta e="T110" id="Seg_3565" s="T109">baːr</ta>
            <ta e="T111" id="Seg_3566" s="T110">tahaːra</ta>
            <ta e="T112" id="Seg_3567" s="T111">ol-nI</ta>
            <ta e="T113" id="Seg_3568" s="T112">killer-An</ta>
            <ta e="T114" id="Seg_3569" s="T113">kör</ta>
            <ta e="T115" id="Seg_3570" s="T114">di͡e-BIT</ta>
            <ta e="T116" id="Seg_3571" s="T115">emeːksin-tA</ta>
            <ta e="T117" id="Seg_3572" s="T116">ü͡ör-Iː-tI-GAr</ta>
            <ta e="T118" id="Seg_3573" s="T117">iŋne-LAːK</ta>
            <ta e="T119" id="Seg_3574" s="T118">hüːtük-tI-n</ta>
            <ta e="T120" id="Seg_3575" s="T119">ɨnʼɨr-An</ta>
            <ta e="T121" id="Seg_3576" s="T120">keːs-BIT</ta>
            <ta e="T122" id="Seg_3577" s="T121">tagɨs-A</ta>
            <ta e="T123" id="Seg_3578" s="T122">köt-BIT</ta>
            <ta e="T124" id="Seg_3579" s="T123">bul-BAkkA</ta>
            <ta e="T125" id="Seg_3580" s="T124">tönün-An</ta>
            <ta e="T126" id="Seg_3581" s="T125">kiːr-BIT</ta>
            <ta e="T127" id="Seg_3582" s="T126">karmaːn-GA-r</ta>
            <ta e="T128" id="Seg_3583" s="T127">tu͡ok</ta>
            <ta e="T129" id="Seg_3584" s="T128">da</ta>
            <ta e="T130" id="Seg_3585" s="T129">hu͡ok</ta>
            <ta e="T131" id="Seg_3586" s="T130">di͡e-BIT</ta>
            <ta e="T132" id="Seg_3587" s="T131">Aŋaː Moŋus</ta>
            <ta e="T133" id="Seg_3588" s="T132">beje-ŋ</ta>
            <ta e="T134" id="Seg_3589" s="T133">hi͡e-TAK-I-ŋ</ta>
            <ta e="T135" id="Seg_3590" s="T134">diː</ta>
            <ta e="T136" id="Seg_3591" s="T135">di͡e-BIT</ta>
            <ta e="T137" id="Seg_3592" s="T136">da</ta>
            <ta e="T138" id="Seg_3593" s="T137">emeːksin</ta>
            <ta e="T139" id="Seg_3594" s="T138">bɨ͡ar-tI-n</ta>
            <ta e="T140" id="Seg_3595" s="T139">kaj-A</ta>
            <ta e="T141" id="Seg_3596" s="T140">tart-Ar</ta>
            <ta e="T142" id="Seg_3597" s="T141">emeːksin</ta>
            <ta e="T143" id="Seg_3598" s="T142">bɨ͡ar-tI-ttAn</ta>
            <ta e="T144" id="Seg_3599" s="T143">iŋne-LAːK</ta>
            <ta e="T145" id="Seg_3600" s="T144">hüːtük</ta>
            <ta e="T146" id="Seg_3601" s="T145">kɨlɨr</ta>
            <ta e="T147" id="Seg_3602" s="T146">gɨn-A</ta>
            <ta e="T148" id="Seg_3603" s="T147">tüs-BIT</ta>
            <ta e="T149" id="Seg_3604" s="T148">oː</ta>
            <ta e="T150" id="Seg_3605" s="T149">kaːrɨ͡an</ta>
            <ta e="T151" id="Seg_3606" s="T150">dʼaktar-BI-n</ta>
            <ta e="T152" id="Seg_3607" s="T151">ü͡ögüleː-BIT</ta>
            <ta e="T153" id="Seg_3608" s="T152">Aŋaː Moŋus</ta>
            <ta e="T154" id="Seg_3609" s="T153">töttörü</ta>
            <ta e="T155" id="Seg_3610" s="T154">hüːr-BIT</ta>
            <ta e="T156" id="Seg_3611" s="T155">Lɨːbɨra</ta>
            <ta e="T157" id="Seg_3612" s="T156">orok-GA</ta>
            <ta e="T158" id="Seg_3613" s="T157">bu</ta>
            <ta e="T159" id="Seg_3614" s="T158">čaŋkaj-A</ta>
            <ta e="T160" id="Seg_3615" s="T159">hɨt-Ar</ta>
            <ta e="T161" id="Seg_3616" s="T160">ɨl-Ar</ta>
            <ta e="T162" id="Seg_3617" s="T161">da</ta>
            <ta e="T163" id="Seg_3618" s="T162">karmaːn-tI-GAr</ta>
            <ta e="T164" id="Seg_3619" s="T163">ugun-Ar</ta>
            <ta e="T165" id="Seg_3620" s="T164">töttörü</ta>
            <ta e="T166" id="Seg_3621" s="T165">hüːr-An</ta>
            <ta e="T167" id="Seg_3622" s="T166">kel-Ar</ta>
            <ta e="T168" id="Seg_3623" s="T167">dʼi͡e-tI-GAr</ta>
            <ta e="T169" id="Seg_3624" s="T168">kiːr-An</ta>
            <ta e="T170" id="Seg_3625" s="T169">Lɨːbɨra-nI</ta>
            <ta e="T171" id="Seg_3626" s="T170">iːkeːptiːŋ-GA</ta>
            <ta e="T172" id="Seg_3627" s="T171">ɨjaː-An</ta>
            <ta e="T173" id="Seg_3628" s="T172">keːs-Ar</ta>
            <ta e="T174" id="Seg_3629" s="T173">Aŋaː Moŋus</ta>
            <ta e="T175" id="Seg_3630" s="T174">hette</ta>
            <ta e="T176" id="Seg_3631" s="T175">tarakaːj</ta>
            <ta e="T177" id="Seg_3632" s="T176">ogo-LAːK</ta>
            <ta e="T178" id="Seg_3633" s="T177">e-BIT</ta>
            <ta e="T179" id="Seg_3634" s="T178">ol</ta>
            <ta e="T180" id="Seg_3635" s="T179">ogo-LAr-tI-GAr</ta>
            <ta e="T181" id="Seg_3636" s="T180">di͡e-BIT</ta>
            <ta e="T182" id="Seg_3637" s="T181">ogo-LAr</ta>
            <ta e="T183" id="Seg_3638" s="T182">Lɨːbɨra</ta>
            <ta e="T184" id="Seg_3639" s="T183">hɨ͡a-tA</ta>
            <ta e="T185" id="Seg_3640" s="T184">tehin-IAK-tA</ta>
            <ta e="T186" id="Seg_3641" s="T185">kör-Ar</ta>
            <ta e="T187" id="Seg_3642" s="T186">bu͡ol-I-ŋ</ta>
            <ta e="T188" id="Seg_3643" s="T187">kel-Ar-BA-r</ta>
            <ta e="T189" id="Seg_3644" s="T188">kü͡östeː-Aːr-I-ŋ</ta>
            <ta e="T190" id="Seg_3645" s="T189">di͡e-TI-tA</ta>
            <ta e="T191" id="Seg_3646" s="T190">da</ta>
            <ta e="T192" id="Seg_3647" s="T191">tagɨs-An</ta>
            <ta e="T193" id="Seg_3648" s="T192">kaːl-BIT</ta>
            <ta e="T194" id="Seg_3649" s="T193">töhö</ta>
            <ta e="T195" id="Seg_3650" s="T194">da</ta>
            <ta e="T196" id="Seg_3651" s="T195">bu͡ol-BAkkA</ta>
            <ta e="T197" id="Seg_3652" s="T196">Lɨːbɨra</ta>
            <ta e="T198" id="Seg_3653" s="T197">dʼe</ta>
            <ta e="T199" id="Seg_3654" s="T198">iːkteː-BIT</ta>
            <ta e="T200" id="Seg_3655" s="T199">ogo-LAr</ta>
            <ta e="T201" id="Seg_3656" s="T200">onno</ta>
            <ta e="T202" id="Seg_3657" s="T201">ü͡ögüleː-A-s-BIT-LAr</ta>
            <ta e="T203" id="Seg_3658" s="T202">anʼɨkataːn</ta>
            <ta e="T204" id="Seg_3659" s="T203">Lɨːbɨra</ta>
            <ta e="T205" id="Seg_3660" s="T204">ɨbagas</ta>
            <ta e="T206" id="Seg_3661" s="T205">hɨ͡a-tA</ta>
            <ta e="T207" id="Seg_3662" s="T206">tok-I-n-TI-tA</ta>
            <ta e="T208" id="Seg_3663" s="T207">onton</ta>
            <ta e="T209" id="Seg_3664" s="T208">Lɨːbɨra</ta>
            <ta e="T210" id="Seg_3665" s="T209">dʼe</ta>
            <ta e="T211" id="Seg_3666" s="T210">haːktaː-Ar</ta>
            <ta e="T212" id="Seg_3667" s="T211">ogo-LAr</ta>
            <ta e="T213" id="Seg_3668" s="T212">ü͡ögüleː-A-s-BIT-LAr</ta>
            <ta e="T214" id="Seg_3669" s="T213">anʼɨkataːn</ta>
            <ta e="T215" id="Seg_3670" s="T214">Lɨːbɨra</ta>
            <ta e="T216" id="Seg_3671" s="T215">kojuː</ta>
            <ta e="T217" id="Seg_3672" s="T216">hɨ͡a-tA</ta>
            <ta e="T218" id="Seg_3673" s="T217">tok-I-n-TI-tA</ta>
            <ta e="T219" id="Seg_3674" s="T218">Lɨːbɨra</ta>
            <ta e="T220" id="Seg_3675" s="T219">ogo-LAr-GA</ta>
            <ta e="T221" id="Seg_3676" s="T220">di͡e-BIT</ta>
            <ta e="T222" id="Seg_3677" s="T221">ogo-LAr</ta>
            <ta e="T223" id="Seg_3678" s="T222">tüher-I-ŋ</ta>
            <ta e="T224" id="Seg_3679" s="T223">min-n</ta>
            <ta e="T225" id="Seg_3680" s="T224">min</ta>
            <ta e="T226" id="Seg_3681" s="T225">ehigi-GA</ta>
            <ta e="T227" id="Seg_3682" s="T226">lu͡osku</ta>
            <ta e="T228" id="Seg_3683" s="T227">oŋor-An</ta>
            <ta e="T229" id="Seg_3684" s="T228">bi͡er-IAK-m</ta>
            <ta e="T230" id="Seg_3685" s="T229">min</ta>
            <ta e="T231" id="Seg_3686" s="T230">hɨ͡a-BI-n</ta>
            <ta e="T232" id="Seg_3687" s="T231">hi͡e-Ar-GItI-GAr</ta>
            <ta e="T233" id="Seg_3688" s="T232">üčügej</ta>
            <ta e="T234" id="Seg_3689" s="T233">bu͡ol-IAK.[tA]</ta>
            <ta e="T235" id="Seg_3690" s="T234">kaja</ta>
            <ta e="T236" id="Seg_3691" s="T235">ere</ta>
            <ta e="T237" id="Seg_3692" s="T236">kaja</ta>
            <ta e="T238" id="Seg_3693" s="T237">Lɨːbɨra-nI</ta>
            <ta e="T239" id="Seg_3694" s="T238">tüher-BIT-LAr</ta>
            <ta e="T240" id="Seg_3695" s="T239">Lɨːbɨra</ta>
            <ta e="T241" id="Seg_3696" s="T240">di͡e-BIT</ta>
            <ta e="T242" id="Seg_3697" s="T241">egel-I-ŋ</ta>
            <ta e="T243" id="Seg_3698" s="T242">teːte-GIt</ta>
            <ta e="T244" id="Seg_3699" s="T243">hɨtɨː</ta>
            <ta e="T245" id="Seg_3700" s="T244">hüge-tI-n</ta>
            <ta e="T246" id="Seg_3701" s="T245">egel-An</ta>
            <ta e="T247" id="Seg_3702" s="T246">bi͡er-BIT-LAr</ta>
            <ta e="T248" id="Seg_3703" s="T247">oron-GItI-GAr</ta>
            <ta e="T249" id="Seg_3704" s="T248">kör-A</ta>
            <ta e="T250" id="Seg_3705" s="T249">hɨt-I-ŋ</ta>
            <ta e="T251" id="Seg_3706" s="T250">kajdak</ta>
            <ta e="T252" id="Seg_3707" s="T251">min</ta>
            <ta e="T253" id="Seg_3708" s="T252">oŋor-A-BIn</ta>
            <ta e="T254" id="Seg_3709" s="T253">ogo-LAr</ta>
            <ta e="T255" id="Seg_3710" s="T254">hɨt-An</ta>
            <ta e="T256" id="Seg_3711" s="T255">kaːl-BIT-LAr</ta>
            <ta e="T257" id="Seg_3712" s="T256">ol</ta>
            <ta e="T258" id="Seg_3713" s="T257">hɨt-BIT-LArI-GAr</ta>
            <ta e="T259" id="Seg_3714" s="T258">Lɨːbɨra</ta>
            <ta e="T260" id="Seg_3715" s="T259">menʼiː-LArI-n</ta>
            <ta e="T261" id="Seg_3716" s="T260">bɨs-A</ta>
            <ta e="T262" id="Seg_3717" s="T261">kert-ItAlAː-BIT</ta>
            <ta e="T263" id="Seg_3718" s="T262">et-LArI-n</ta>
            <ta e="T264" id="Seg_3719" s="T263">kü͡östeː-An</ta>
            <ta e="T265" id="Seg_3720" s="T264">keːs-BIT</ta>
            <ta e="T266" id="Seg_3721" s="T265">menʼiː-LAr-nI</ta>
            <ta e="T267" id="Seg_3722" s="T266">oron-GA</ta>
            <ta e="T268" id="Seg_3723" s="T267">kečigirečči-kAːN</ta>
            <ta e="T269" id="Seg_3724" s="T268">uːr-An</ta>
            <ta e="T270" id="Seg_3725" s="T269">baran</ta>
            <ta e="T271" id="Seg_3726" s="T270">hu͡organ-tI-nAn</ta>
            <ta e="T272" id="Seg_3727" s="T271">hap-ItAlAː-BIT</ta>
            <ta e="T273" id="Seg_3728" s="T272">korgoldʼuːn-nI</ta>
            <ta e="T274" id="Seg_3729" s="T273">ɨl-An</ta>
            <ta e="T275" id="Seg_3730" s="T274">baran</ta>
            <ta e="T276" id="Seg_3731" s="T275">ačaːk</ta>
            <ta e="T277" id="Seg_3732" s="T276">alɨn-tI-GAr</ta>
            <ta e="T278" id="Seg_3733" s="T277">kiːr-An</ta>
            <ta e="T279" id="Seg_3734" s="T278">kaːl-BIT</ta>
            <ta e="T280" id="Seg_3735" s="T279">bu</ta>
            <ta e="T281" id="Seg_3736" s="T280">hɨt-TAK-InA</ta>
            <ta e="T282" id="Seg_3737" s="T281">Aŋaː Moŋus</ta>
            <ta e="T283" id="Seg_3738" s="T282">kel-BIT</ta>
            <ta e="T284" id="Seg_3739" s="T283">Aŋaː Moŋus</ta>
            <ta e="T285" id="Seg_3740" s="T284">kel-An</ta>
            <ta e="T286" id="Seg_3741" s="T285">ü͡ögüleː-BIT</ta>
            <ta e="T287" id="Seg_3742" s="T286">ogo-LAr</ta>
            <ta e="T288" id="Seg_3743" s="T287">aːn-GItI-n</ta>
            <ta e="T289" id="Seg_3744" s="T288">arɨj-I-ŋ</ta>
            <ta e="T290" id="Seg_3745" s="T289">kim</ta>
            <ta e="T291" id="Seg_3746" s="T290">da</ta>
            <ta e="T292" id="Seg_3747" s="T291">arɨj-BAT</ta>
            <ta e="T293" id="Seg_3748" s="T292">kas-TA</ta>
            <ta e="T294" id="Seg_3749" s="T293">da</ta>
            <ta e="T295" id="Seg_3750" s="T294">ü͡ögüleː-BIT</ta>
            <ta e="T296" id="Seg_3751" s="T295">ogo-LAr</ta>
            <ta e="T297" id="Seg_3752" s="T296">törüt</ta>
            <ta e="T298" id="Seg_3753" s="T297">arɨj-BAtAK-LAr</ta>
            <ta e="T299" id="Seg_3754" s="T298">aːn-tI-n</ta>
            <ta e="T300" id="Seg_3755" s="T299">kostoː-A</ta>
            <ta e="T301" id="Seg_3756" s="T300">tart-An</ta>
            <ta e="T302" id="Seg_3757" s="T301">kiːr-BIT</ta>
            <ta e="T303" id="Seg_3758" s="T302">kör-BIT-tA</ta>
            <ta e="T304" id="Seg_3759" s="T303">et</ta>
            <ta e="T305" id="Seg_3760" s="T304">bus-A</ta>
            <ta e="T306" id="Seg_3761" s="T305">tur-Ar</ta>
            <ta e="T307" id="Seg_3762" s="T306">ogo-LAr</ta>
            <ta e="T308" id="Seg_3763" s="T307">utuj-A</ta>
            <ta e="T309" id="Seg_3764" s="T308">hɨt-Ar-LAr</ta>
            <ta e="T310" id="Seg_3765" s="T309">oː</ta>
            <ta e="T311" id="Seg_3766" s="T310">ogo-LAr-I-m</ta>
            <ta e="T312" id="Seg_3767" s="T311">Lɨːbɨra</ta>
            <ta e="T313" id="Seg_3768" s="T312">hɨ͡a-tI-ttAn</ta>
            <ta e="T314" id="Seg_3769" s="T313">tot-An</ta>
            <ta e="T315" id="Seg_3770" s="T314">baran</ta>
            <ta e="T316" id="Seg_3771" s="T315">utuj-A</ta>
            <ta e="T317" id="Seg_3772" s="T316">hɨt-Ar-LAr</ta>
            <ta e="T318" id="Seg_3773" s="T317">e-BIT</ta>
            <ta e="T319" id="Seg_3774" s="T318">bu͡o</ta>
            <ta e="T320" id="Seg_3775" s="T319">di͡e-Ar</ta>
            <ta e="T321" id="Seg_3776" s="T320">kü͡ös-nI</ta>
            <ta e="T322" id="Seg_3777" s="T321">tahaːr-Ar</ta>
            <ta e="T323" id="Seg_3778" s="T322">hürek-tI-ttAn</ta>
            <ta e="T324" id="Seg_3779" s="T323">amsaj-Ar</ta>
            <ta e="T325" id="Seg_3780" s="T324">hürek-tA</ta>
            <ta e="T326" id="Seg_3781" s="T325">tart-BIT</ta>
            <ta e="T327" id="Seg_3782" s="T326">oː</ta>
            <ta e="T328" id="Seg_3783" s="T327">Lɨːbɨra</ta>
            <ta e="T329" id="Seg_3784" s="T328">min</ta>
            <ta e="T330" id="Seg_3785" s="T329">uruː-m</ta>
            <ta e="T331" id="Seg_3786" s="T330">e-BIT</ta>
            <ta e="T332" id="Seg_3787" s="T331">du͡o</ta>
            <ta e="T333" id="Seg_3788" s="T332">di͡e-BIT</ta>
            <ta e="T334" id="Seg_3789" s="T333">taːl</ta>
            <ta e="T335" id="Seg_3790" s="T334">hi͡e-BIT</ta>
            <ta e="T336" id="Seg_3791" s="T335">taːl-tA</ta>
            <ta e="T337" id="Seg_3792" s="T336">tart-BIT</ta>
            <ta e="T338" id="Seg_3793" s="T337">oː</ta>
            <ta e="T339" id="Seg_3794" s="T338">Lɨːbɨra</ta>
            <ta e="T340" id="Seg_3795" s="T339">ile</ta>
            <ta e="T341" id="Seg_3796" s="T340">uruː-m</ta>
            <ta e="T342" id="Seg_3797" s="T341">e-BIT</ta>
            <ta e="T343" id="Seg_3798" s="T342">du͡o</ta>
            <ta e="T344" id="Seg_3799" s="T343">di͡e-BIT</ta>
            <ta e="T345" id="Seg_3800" s="T344">onton</ta>
            <ta e="T346" id="Seg_3801" s="T345">tur-BIT</ta>
            <ta e="T347" id="Seg_3802" s="T346">ogo-LAr-tI-n</ta>
            <ta e="T348" id="Seg_3803" s="T347">arɨj-A</ta>
            <ta e="T349" id="Seg_3804" s="T348">tart-BIT-tA</ta>
            <ta e="T350" id="Seg_3805" s="T349">hette</ta>
            <ta e="T351" id="Seg_3806" s="T350">menʼiː</ta>
            <ta e="T352" id="Seg_3807" s="T351">čekenij-s</ta>
            <ta e="T353" id="Seg_3808" s="T352">gɨn-BIT-LAr</ta>
            <ta e="T354" id="Seg_3809" s="T353">hohuj-A</ta>
            <ta e="T355" id="Seg_3810" s="T354">kɨtta</ta>
            <ta e="T356" id="Seg_3811" s="T355">Aŋaː Moŋus</ta>
            <ta e="T357" id="Seg_3812" s="T356">ü͡ögüleː-BIT</ta>
            <ta e="T358" id="Seg_3813" s="T357">Lɨːbɨra</ta>
            <ta e="T359" id="Seg_3814" s="T358">kanna</ta>
            <ta e="T360" id="Seg_3815" s="T359">baːr-GIn=Ij</ta>
            <ta e="T361" id="Seg_3816" s="T360">Lɨːbɨra</ta>
            <ta e="T362" id="Seg_3817" s="T361">tahaːra</ta>
            <ta e="T363" id="Seg_3818" s="T362">baːr-BIn</ta>
            <ta e="T364" id="Seg_3819" s="T363">di͡e-BIT</ta>
            <ta e="T365" id="Seg_3820" s="T364">ol-nI</ta>
            <ta e="T366" id="Seg_3821" s="T365">ihit-TI-tA</ta>
            <ta e="T367" id="Seg_3822" s="T366">da</ta>
            <ta e="T368" id="Seg_3823" s="T367">dʼi͡e-tI-n</ta>
            <ta e="T369" id="Seg_3824" s="T368">kaj-A</ta>
            <ta e="T370" id="Seg_3825" s="T369">köt-An</ta>
            <ta e="T371" id="Seg_3826" s="T370">tagɨs-Ar</ta>
            <ta e="T372" id="Seg_3827" s="T371">Lɨːbɨra</ta>
            <ta e="T373" id="Seg_3828" s="T372">kanna</ta>
            <ta e="T374" id="Seg_3829" s="T373">da</ta>
            <ta e="T375" id="Seg_3830" s="T374">hu͡ok</ta>
            <ta e="T376" id="Seg_3831" s="T375">Lɨːbɨra</ta>
            <ta e="T377" id="Seg_3832" s="T376">kanna-GIn=Ij</ta>
            <ta e="T378" id="Seg_3833" s="T377">dʼi͡e</ta>
            <ta e="T379" id="Seg_3834" s="T378">is-tI-GAr</ta>
            <ta e="T380" id="Seg_3835" s="T379">baːr-BIn</ta>
            <ta e="T381" id="Seg_3836" s="T380">di͡e-BIT</ta>
            <ta e="T382" id="Seg_3837" s="T381">Lɨːbɨra</ta>
            <ta e="T383" id="Seg_3838" s="T382">dʼi͡e-GA</ta>
            <ta e="T384" id="Seg_3839" s="T383">köt-An</ta>
            <ta e="T385" id="Seg_3840" s="T384">tüs-BIT</ta>
            <ta e="T386" id="Seg_3841" s="T385">Lɨːbɨra</ta>
            <ta e="T387" id="Seg_3842" s="T386">hu͡ok</ta>
            <ta e="T388" id="Seg_3843" s="T387">Lɨːbɨra</ta>
            <ta e="T389" id="Seg_3844" s="T388">kanna-GIn=Ij</ta>
            <ta e="T390" id="Seg_3845" s="T389">ü͡öles-GA</ta>
            <ta e="T391" id="Seg_3846" s="T390">baːr-BIn</ta>
            <ta e="T392" id="Seg_3847" s="T391">ü͡öles-GA</ta>
            <ta e="T393" id="Seg_3848" s="T392">körüleː-BIT-tA</ta>
            <ta e="T394" id="Seg_3849" s="T393">Lɨːbɨra</ta>
            <ta e="T395" id="Seg_3850" s="T394">kanna</ta>
            <ta e="T396" id="Seg_3851" s="T395">da</ta>
            <ta e="T397" id="Seg_3852" s="T396">hu͡ok</ta>
            <ta e="T398" id="Seg_3853" s="T397">Lɨːbɨra</ta>
            <ta e="T399" id="Seg_3854" s="T398">kanna</ta>
            <ta e="T400" id="Seg_3855" s="T399">baːr-GIn=Ij</ta>
            <ta e="T401" id="Seg_3856" s="T400">di͡e-BIT</ta>
            <ta e="T402" id="Seg_3857" s="T401">Lɨːbɨra</ta>
            <ta e="T403" id="Seg_3858" s="T402">ačaːk</ta>
            <ta e="T404" id="Seg_3859" s="T403">alɨn-tI-GAr</ta>
            <ta e="T405" id="Seg_3860" s="T404">baːr-BIn</ta>
            <ta e="T406" id="Seg_3861" s="T405">di͡e-BIT</ta>
            <ta e="T407" id="Seg_3862" s="T406">ol-nI</ta>
            <ta e="T408" id="Seg_3863" s="T407">Aŋaː Moŋus</ta>
            <ta e="T409" id="Seg_3864" s="T408">ihit-TI-tA</ta>
            <ta e="T410" id="Seg_3865" s="T409">da</ta>
            <ta e="T411" id="Seg_3866" s="T410">menʼiː-tI-nAn</ta>
            <ta e="T412" id="Seg_3867" s="T411">kiːr-AːrI</ta>
            <ta e="T413" id="Seg_3868" s="T412">gɨn-BIT</ta>
            <ta e="T414" id="Seg_3869" s="T413">ol-GA</ta>
            <ta e="T415" id="Seg_3870" s="T414">Lɨːbɨra</ta>
            <ta e="T416" id="Seg_3871" s="T415">haŋar-Ar</ta>
            <ta e="T417" id="Seg_3872" s="T416">ehe</ta>
            <ta e="T418" id="Seg_3873" s="T417">menʼiː-GI-nAn</ta>
            <ta e="T419" id="Seg_3874" s="T418">kiːr-I-m</ta>
            <ta e="T420" id="Seg_3875" s="T419">bat-IAK-ŋ</ta>
            <ta e="T421" id="Seg_3876" s="T420">hu͡ok-tA</ta>
            <ta e="T422" id="Seg_3877" s="T421">emehe-GI-nAn</ta>
            <ta e="T423" id="Seg_3878" s="T422">kiːr</ta>
            <ta e="T424" id="Seg_3879" s="T423">Aŋaː Moŋus</ta>
            <ta e="T425" id="Seg_3880" s="T424">emehe-tI-nAn</ta>
            <ta e="T426" id="Seg_3881" s="T425">kiːr-BIT</ta>
            <ta e="T427" id="Seg_3882" s="T426">ol</ta>
            <ta e="T428" id="Seg_3883" s="T427">kiːr-An</ta>
            <ta e="T429" id="Seg_3884" s="T428">er-TAK-InA</ta>
            <ta e="T430" id="Seg_3885" s="T429">Lɨːbɨra</ta>
            <ta e="T431" id="Seg_3886" s="T430">kɨtar-čI</ta>
            <ta e="T432" id="Seg_3887" s="T431">bar-BIT</ta>
            <ta e="T433" id="Seg_3888" s="T432">korgoldʼuːn-tI-nAn</ta>
            <ta e="T434" id="Seg_3889" s="T433">emehe-tI-n</ta>
            <ta e="T435" id="Seg_3890" s="T434">is-tI-nAn</ta>
            <ta e="T436" id="Seg_3891" s="T435">as-BIT</ta>
            <ta e="T437" id="Seg_3892" s="T436">Aŋaː Moŋus</ta>
            <ta e="T438" id="Seg_3893" s="T437">öl-An</ta>
            <ta e="T439" id="Seg_3894" s="T438">kaːl-BIT</ta>
            <ta e="T440" id="Seg_3895" s="T439">Lɨːbɨra</ta>
            <ta e="T441" id="Seg_3896" s="T440">tagɨs-TI-tA</ta>
            <ta e="T442" id="Seg_3897" s="T441">da</ta>
            <ta e="T443" id="Seg_3898" s="T442">dʼi͡e-tI-GAr</ta>
            <ta e="T444" id="Seg_3899" s="T443">bar-An</ta>
            <ta e="T445" id="Seg_3900" s="T444">kaːl-BIT</ta>
            <ta e="T446" id="Seg_3901" s="T445">onon</ta>
            <ta e="T447" id="Seg_3902" s="T446">baːj-An-baːj-An</ta>
            <ta e="T448" id="Seg_3903" s="T447">olor-BIT</ta>
            <ta e="T449" id="Seg_3904" s="T448">dʼe</ta>
            <ta e="T450" id="Seg_3905" s="T449">büt-TI-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_3906" s="T0">Lyybyra.[NOM]</ta>
            <ta e="T2" id="Seg_3907" s="T1">row-CVB.SEQ</ta>
            <ta e="T3" id="Seg_3908" s="T2">sway.smoothly-CVB.SEQ</ta>
            <ta e="T4" id="Seg_3909" s="T3">go-FREQ-PRS.[3SG]</ta>
            <ta e="T5" id="Seg_3910" s="T4">and</ta>
            <ta e="T6" id="Seg_3911" s="T5">drive-CVB.SEQ-drive-CVB.SEQ</ta>
            <ta e="T7" id="Seg_3912" s="T6">stone-DAT/LOC</ta>
            <ta e="T8" id="Seg_3913" s="T7">push-PST2.[3SG]</ta>
            <ta e="T9" id="Seg_3914" s="T8">small.boat-3SG.[NOM]</ta>
            <ta e="T10" id="Seg_3915" s="T9">stick-CVB.SEQ</ta>
            <ta e="T11" id="Seg_3916" s="T10">stay-PST2.[3SG]</ta>
            <ta e="T12" id="Seg_3917" s="T11">oar-3SG-INSTR</ta>
            <ta e="T13" id="Seg_3918" s="T12">beat-EP-PST2.[3SG]</ta>
            <ta e="T14" id="Seg_3919" s="T13">oar-3SG.[NOM]</ta>
            <ta e="T15" id="Seg_3920" s="T14">stick-CVB.SEQ</ta>
            <ta e="T16" id="Seg_3921" s="T15">stay-PST2.[3SG]</ta>
            <ta e="T17" id="Seg_3922" s="T16">hand-3SG-INSTR</ta>
            <ta e="T18" id="Seg_3923" s="T17">beat-EP-PST2.[3SG]</ta>
            <ta e="T19" id="Seg_3924" s="T18">hand-3SG.[NOM]</ta>
            <ta e="T20" id="Seg_3925" s="T19">stick-CVB.SEQ</ta>
            <ta e="T21" id="Seg_3926" s="T20">stay-PST2.[3SG]</ta>
            <ta e="T22" id="Seg_3927" s="T21">other</ta>
            <ta e="T23" id="Seg_3928" s="T22">hand-3SG-INSTR</ta>
            <ta e="T24" id="Seg_3929" s="T23">beat-PRS.[3SG]</ta>
            <ta e="T25" id="Seg_3930" s="T24">other-3SG.[NOM]</ta>
            <ta e="T26" id="Seg_3931" s="T25">also</ta>
            <ta e="T27" id="Seg_3932" s="T26">stick-CVB.SEQ</ta>
            <ta e="T28" id="Seg_3933" s="T27">stay-PST2.[3SG]</ta>
            <ta e="T29" id="Seg_3934" s="T28">foot-3SG-INSTR</ta>
            <ta e="T30" id="Seg_3935" s="T29">kick-PST2.[3SG]</ta>
            <ta e="T31" id="Seg_3936" s="T30">foot-3SG.[NOM]</ta>
            <ta e="T32" id="Seg_3937" s="T31">stick-CVB.SEQ</ta>
            <ta e="T33" id="Seg_3938" s="T32">stay-PST2.[3SG]</ta>
            <ta e="T34" id="Seg_3939" s="T33">next</ta>
            <ta e="T35" id="Seg_3940" s="T34">foot-3SG-INSTR</ta>
            <ta e="T36" id="Seg_3941" s="T35">kick-PST2-3SG</ta>
            <ta e="T37" id="Seg_3942" s="T36">also</ta>
            <ta e="T38" id="Seg_3943" s="T37">stick-CVB.SEQ</ta>
            <ta e="T39" id="Seg_3944" s="T38">stay-PST2.[3SG]</ta>
            <ta e="T40" id="Seg_3945" s="T39">belly-3SG-INSTR</ta>
            <ta e="T41" id="Seg_3946" s="T40">push-PST2-3SG</ta>
            <ta e="T42" id="Seg_3947" s="T41">belly-3SG.[NOM]</ta>
            <ta e="T43" id="Seg_3948" s="T42">also</ta>
            <ta e="T44" id="Seg_3949" s="T43">stick-CVB.SEQ</ta>
            <ta e="T45" id="Seg_3950" s="T44">stay-PST2.[3SG]</ta>
            <ta e="T46" id="Seg_3951" s="T45">then</ta>
            <ta e="T47" id="Seg_3952" s="T46">head-3SG-INSTR</ta>
            <ta e="T48" id="Seg_3953" s="T47">beat-EP-PST2.[3SG]</ta>
            <ta e="T49" id="Seg_3954" s="T48">head-3SG.[NOM]</ta>
            <ta e="T50" id="Seg_3955" s="T49">also</ta>
            <ta e="T51" id="Seg_3956" s="T50">stick-CVB.SEQ</ta>
            <ta e="T52" id="Seg_3957" s="T51">stay-PST2.[3SG]</ta>
            <ta e="T53" id="Seg_3958" s="T52">stick-CVB.SEQ</ta>
            <ta e="T54" id="Seg_3959" s="T53">stand-INFER-3SG</ta>
            <ta e="T55" id="Seg_3960" s="T54">that</ta>
            <ta e="T56" id="Seg_3961" s="T55">stand-TEMP-3SG</ta>
            <ta e="T57" id="Seg_3962" s="T56">Angaa.Mongus.[NOM]</ta>
            <ta e="T58" id="Seg_3963" s="T57">come-PST2.[3SG]</ta>
            <ta e="T59" id="Seg_3964" s="T58">what.a.joy</ta>
            <ta e="T60" id="Seg_3965" s="T59">evening</ta>
            <ta e="T61" id="Seg_3966" s="T60">snack-PROPR.[NOM]</ta>
            <ta e="T62" id="Seg_3967" s="T61">be-PST1-1SG</ta>
            <ta e="T63" id="Seg_3968" s="T62">say-PTCP.PST-3SG-INSTR</ta>
            <ta e="T64" id="Seg_3969" s="T63">Lyybyra-ACC</ta>
            <ta e="T65" id="Seg_3970" s="T64">take-CVB.SEQ</ta>
            <ta e="T66" id="Seg_3971" s="T65">pocket-3SG-DAT/LOC</ta>
            <ta e="T67" id="Seg_3972" s="T66">put.in-CVB.SEQ</ta>
            <ta e="T68" id="Seg_3973" s="T67">throw-PST2.[3SG]</ta>
            <ta e="T69" id="Seg_3974" s="T68">house-3SG-DAT/LOC</ta>
            <ta e="T70" id="Seg_3975" s="T69">go-PST2.[3SG]</ta>
            <ta e="T71" id="Seg_3976" s="T70">path-3SG-DAT/LOC</ta>
            <ta e="T72" id="Seg_3977" s="T71">reach-CVB.SEQ</ta>
            <ta e="T73" id="Seg_3978" s="T72">Angaa.Mongus.[NOM]</ta>
            <ta e="T74" id="Seg_3979" s="T73">empty.oneself-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T75" id="Seg_3980" s="T74">want-PST2.[3SG]</ta>
            <ta e="T76" id="Seg_3981" s="T75">that</ta>
            <ta e="T77" id="Seg_3982" s="T76">empty.oneself-CVB.SIM</ta>
            <ta e="T78" id="Seg_3983" s="T77">sit-TEMP-3SG</ta>
            <ta e="T79" id="Seg_3984" s="T78">Lyybyra.[NOM]</ta>
            <ta e="T80" id="Seg_3985" s="T79">pocket-3SG-GEN</ta>
            <ta e="T81" id="Seg_3986" s="T80">hole-3SG-ACC</ta>
            <ta e="T82" id="Seg_3987" s="T81">through</ta>
            <ta e="T83" id="Seg_3988" s="T82">fall-CVB.SEQ</ta>
            <ta e="T84" id="Seg_3989" s="T83">stay-PST2.[3SG]</ta>
            <ta e="T85" id="Seg_3990" s="T84">Angaa.Mongus.[NOM]</ta>
            <ta e="T86" id="Seg_3991" s="T85">what-ACC</ta>
            <ta e="T87" id="Seg_3992" s="T86">NEG</ta>
            <ta e="T88" id="Seg_3993" s="T87">notice-NEG.CVB.SIM</ta>
            <ta e="T89" id="Seg_3994" s="T88">trousers-3SG-ACC</ta>
            <ta e="T90" id="Seg_3995" s="T89">raise-EP-REFL-PST1-3SG</ta>
            <ta e="T91" id="Seg_3996" s="T90">and</ta>
            <ta e="T92" id="Seg_3997" s="T91">house-3SG-DAT/LOC</ta>
            <ta e="T93" id="Seg_3998" s="T92">go-PST2.[3SG]</ta>
            <ta e="T94" id="Seg_3999" s="T93">house-3SG-DAT/LOC</ta>
            <ta e="T95" id="Seg_4000" s="T94">edge-3SG-DAT/LOC</ta>
            <ta e="T96" id="Seg_4001" s="T95">coat-3SG-ACC</ta>
            <ta e="T97" id="Seg_4002" s="T96">storage-DAT/LOC</ta>
            <ta e="T98" id="Seg_4003" s="T97">hang.up-CVB.SEQ</ta>
            <ta e="T99" id="Seg_4004" s="T98">after</ta>
            <ta e="T100" id="Seg_4005" s="T99">go.in-PST2.[3SG]</ta>
            <ta e="T101" id="Seg_4006" s="T100">old.woman-3SG.[NOM]</ta>
            <ta e="T102" id="Seg_4007" s="T101">sew-CVB.SIM</ta>
            <ta e="T103" id="Seg_4008" s="T102">sit-PTCP.PRS</ta>
            <ta e="T104" id="Seg_4009" s="T103">be-PST2.[3SG]</ta>
            <ta e="T105" id="Seg_4010" s="T104">old.woman.[NOM]</ta>
            <ta e="T106" id="Seg_4011" s="T105">gift-1SG-ACC</ta>
            <ta e="T107" id="Seg_4012" s="T106">bring-PST1-1SG</ta>
            <ta e="T108" id="Seg_4013" s="T107">coat-EP-1SG.[NOM]</ta>
            <ta e="T109" id="Seg_4014" s="T108">pocket-3SG-DAT/LOC</ta>
            <ta e="T110" id="Seg_4015" s="T109">there.is</ta>
            <ta e="T111" id="Seg_4016" s="T110">outside</ta>
            <ta e="T112" id="Seg_4017" s="T111">that-ACC</ta>
            <ta e="T113" id="Seg_4018" s="T112">bring.in-CVB.SEQ</ta>
            <ta e="T114" id="Seg_4019" s="T113">see.[IMP.2SG]</ta>
            <ta e="T115" id="Seg_4020" s="T114">say-PST2.[3SG]</ta>
            <ta e="T116" id="Seg_4021" s="T115">old.woman-3SG.[NOM]</ta>
            <ta e="T117" id="Seg_4022" s="T116">be.happy-NMNZ-3SG-DAT/LOC</ta>
            <ta e="T118" id="Seg_4023" s="T117">needle-PROPR</ta>
            <ta e="T119" id="Seg_4024" s="T118">thimble-3SG-ACC</ta>
            <ta e="T120" id="Seg_4025" s="T119">devour-CVB.SEQ</ta>
            <ta e="T121" id="Seg_4026" s="T120">throw-PST2.[3SG]</ta>
            <ta e="T122" id="Seg_4027" s="T121">go.out-CVB.SIM</ta>
            <ta e="T123" id="Seg_4028" s="T122">run-PST2.[3SG]</ta>
            <ta e="T124" id="Seg_4029" s="T123">find-NEG.CVB.SIM</ta>
            <ta e="T125" id="Seg_4030" s="T124">come.back-CVB.SEQ</ta>
            <ta e="T126" id="Seg_4031" s="T125">go.in-PST2.[3SG]</ta>
            <ta e="T127" id="Seg_4032" s="T126">pocket-2SG-DAT/LOC</ta>
            <ta e="T128" id="Seg_4033" s="T127">what.[NOM]</ta>
            <ta e="T129" id="Seg_4034" s="T128">NEG</ta>
            <ta e="T130" id="Seg_4035" s="T129">NEG.EX</ta>
            <ta e="T131" id="Seg_4036" s="T130">say-PST2.[3SG]</ta>
            <ta e="T132" id="Seg_4037" s="T131">Angaa.Mongus.[NOM]</ta>
            <ta e="T133" id="Seg_4038" s="T132">self-2SG.[NOM]</ta>
            <ta e="T134" id="Seg_4039" s="T133">eat-INFER-EP-2SG</ta>
            <ta e="T135" id="Seg_4040" s="T134">EMPH</ta>
            <ta e="T136" id="Seg_4041" s="T135">say-PST2.[3SG]</ta>
            <ta e="T137" id="Seg_4042" s="T136">and</ta>
            <ta e="T138" id="Seg_4043" s="T137">old.woman.[NOM]</ta>
            <ta e="T139" id="Seg_4044" s="T138">belly-3SG-ACC</ta>
            <ta e="T140" id="Seg_4045" s="T139">split-CVB.SIM</ta>
            <ta e="T141" id="Seg_4046" s="T140">pull-PRS.[3SG]</ta>
            <ta e="T142" id="Seg_4047" s="T141">old.woman.[NOM]</ta>
            <ta e="T143" id="Seg_4048" s="T142">belly-3SG-ABL</ta>
            <ta e="T144" id="Seg_4049" s="T143">needle-PROPR</ta>
            <ta e="T145" id="Seg_4050" s="T144">thimble.[NOM]</ta>
            <ta e="T146" id="Seg_4051" s="T145">klirr</ta>
            <ta e="T147" id="Seg_4052" s="T146">make-CVB.SIM</ta>
            <ta e="T148" id="Seg_4053" s="T147">fall-PST2.[3SG]</ta>
            <ta e="T149" id="Seg_4054" s="T148">oh</ta>
            <ta e="T150" id="Seg_4055" s="T149">dear</ta>
            <ta e="T151" id="Seg_4056" s="T150">woman-1SG-ACC</ta>
            <ta e="T152" id="Seg_4057" s="T151">shout-PST2.[3SG]</ta>
            <ta e="T153" id="Seg_4058" s="T152">Angaa.Mongus.[NOM]</ta>
            <ta e="T154" id="Seg_4059" s="T153">back</ta>
            <ta e="T155" id="Seg_4060" s="T154">run-PST2.[3SG]</ta>
            <ta e="T156" id="Seg_4061" s="T155">Lyybyra.[NOM]</ta>
            <ta e="T157" id="Seg_4062" s="T156">path-DAT/LOC</ta>
            <ta e="T158" id="Seg_4063" s="T157">this</ta>
            <ta e="T159" id="Seg_4064" s="T158">lie.stiff-CVB.SIM</ta>
            <ta e="T160" id="Seg_4065" s="T159">lie-PRS.[3SG]</ta>
            <ta e="T161" id="Seg_4066" s="T160">take-PRS.[3SG]</ta>
            <ta e="T162" id="Seg_4067" s="T161">and</ta>
            <ta e="T163" id="Seg_4068" s="T162">pocket-3SG-DAT/LOC</ta>
            <ta e="T164" id="Seg_4069" s="T163">put.in-PRS.[3SG]</ta>
            <ta e="T165" id="Seg_4070" s="T164">back</ta>
            <ta e="T166" id="Seg_4071" s="T165">run-CVB.SEQ</ta>
            <ta e="T167" id="Seg_4072" s="T166">come-PRS.[3SG]</ta>
            <ta e="T168" id="Seg_4073" s="T167">house-3SG-DAT/LOC</ta>
            <ta e="T169" id="Seg_4074" s="T168">go.in-CVB.SEQ</ta>
            <ta e="T170" id="Seg_4075" s="T169">Lyybyra-ACC</ta>
            <ta e="T171" id="Seg_4076" s="T170">hook.for.kettle-DAT/LOC</ta>
            <ta e="T172" id="Seg_4077" s="T171">hang.up-CVB.SEQ</ta>
            <ta e="T173" id="Seg_4078" s="T172">throw-PRS.[3SG]</ta>
            <ta e="T174" id="Seg_4079" s="T173">Angaa.Mongus.[NOM]</ta>
            <ta e="T175" id="Seg_4080" s="T174">seven</ta>
            <ta e="T176" id="Seg_4081" s="T175">baldheaded</ta>
            <ta e="T177" id="Seg_4082" s="T176">child-PROPR.[NOM]</ta>
            <ta e="T178" id="Seg_4083" s="T177">be-PST2.[3SG]</ta>
            <ta e="T179" id="Seg_4084" s="T178">that</ta>
            <ta e="T180" id="Seg_4085" s="T179">child-PL-3SG-DAT/LOC</ta>
            <ta e="T181" id="Seg_4086" s="T180">say-PST2.[3SG]</ta>
            <ta e="T182" id="Seg_4087" s="T181">child-PL.[NOM]</ta>
            <ta e="T183" id="Seg_4088" s="T182">Lyybyra.[NOM]</ta>
            <ta e="T184" id="Seg_4089" s="T183">fat-3SG.[NOM]</ta>
            <ta e="T185" id="Seg_4090" s="T184">drip-FUT-3SG</ta>
            <ta e="T186" id="Seg_4091" s="T185">see-PTCP.PRS</ta>
            <ta e="T187" id="Seg_4092" s="T186">be-EP-IMP.2PL</ta>
            <ta e="T188" id="Seg_4093" s="T187">come-PTCP.PRS-1SG-DAT/LOC</ta>
            <ta e="T189" id="Seg_4094" s="T188">cook-FUT-EP-IMP.2PL</ta>
            <ta e="T190" id="Seg_4095" s="T189">say-PST1-3SG</ta>
            <ta e="T191" id="Seg_4096" s="T190">and</ta>
            <ta e="T192" id="Seg_4097" s="T191">go.out-CVB.SEQ</ta>
            <ta e="T193" id="Seg_4098" s="T192">stay-PST2.[3SG]</ta>
            <ta e="T194" id="Seg_4099" s="T193">how.much</ta>
            <ta e="T195" id="Seg_4100" s="T194">NEG</ta>
            <ta e="T196" id="Seg_4101" s="T195">be-NEG.CVB.SIM</ta>
            <ta e="T197" id="Seg_4102" s="T196">Lyybyra.[NOM]</ta>
            <ta e="T198" id="Seg_4103" s="T197">well</ta>
            <ta e="T199" id="Seg_4104" s="T198">pee-PST2.[3SG]</ta>
            <ta e="T200" id="Seg_4105" s="T199">child-PL.[NOM]</ta>
            <ta e="T201" id="Seg_4106" s="T200">there</ta>
            <ta e="T202" id="Seg_4107" s="T201">shout-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T203" id="Seg_4108" s="T202">what.a.joy</ta>
            <ta e="T204" id="Seg_4109" s="T203">Lyybyra.[NOM]</ta>
            <ta e="T205" id="Seg_4110" s="T204">liquid</ta>
            <ta e="T206" id="Seg_4111" s="T205">fat-3SG.[NOM]</ta>
            <ta e="T207" id="Seg_4112" s="T206">pour.out-EP-REFL-PST1-3SG</ta>
            <ta e="T208" id="Seg_4113" s="T207">then</ta>
            <ta e="T209" id="Seg_4114" s="T208">Lyybyra.[NOM]</ta>
            <ta e="T210" id="Seg_4115" s="T209">well</ta>
            <ta e="T211" id="Seg_4116" s="T210">empty.oneself-PRS.[3SG]</ta>
            <ta e="T212" id="Seg_4117" s="T211">child-PL.[NOM]</ta>
            <ta e="T213" id="Seg_4118" s="T212">shout-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T214" id="Seg_4119" s="T213">what.a.joy</ta>
            <ta e="T215" id="Seg_4120" s="T214">Lyybyra.[NOM]</ta>
            <ta e="T216" id="Seg_4121" s="T215">viscous</ta>
            <ta e="T217" id="Seg_4122" s="T216">fat-3SG.[NOM]</ta>
            <ta e="T218" id="Seg_4123" s="T217">pour.out-EP-REFL-PST1-3SG</ta>
            <ta e="T219" id="Seg_4124" s="T218">Lyybyra.[NOM]</ta>
            <ta e="T220" id="Seg_4125" s="T219">child-PL-DAT/LOC</ta>
            <ta e="T221" id="Seg_4126" s="T220">say-PST2.[3SG]</ta>
            <ta e="T222" id="Seg_4127" s="T221">child-PL.[NOM]</ta>
            <ta e="T223" id="Seg_4128" s="T222">drop-EP-IMP.2PL</ta>
            <ta e="T224" id="Seg_4129" s="T223">1SG-ACC</ta>
            <ta e="T225" id="Seg_4130" s="T224">1SG.[NOM]</ta>
            <ta e="T226" id="Seg_4131" s="T225">2PL-DAT/LOC</ta>
            <ta e="T227" id="Seg_4132" s="T226">spoon.[NOM]</ta>
            <ta e="T228" id="Seg_4133" s="T227">make-CVB.SEQ</ta>
            <ta e="T229" id="Seg_4134" s="T228">give-FUT-1SG</ta>
            <ta e="T230" id="Seg_4135" s="T229">1SG.[NOM]</ta>
            <ta e="T231" id="Seg_4136" s="T230">fat-1SG-ACC</ta>
            <ta e="T232" id="Seg_4137" s="T231">eat-PTCP.PRS-2PL-DAT/LOC</ta>
            <ta e="T233" id="Seg_4138" s="T232">good.[NOM]</ta>
            <ta e="T234" id="Seg_4139" s="T233">be-FUT.[3SG]</ta>
            <ta e="T235" id="Seg_4140" s="T234">well</ta>
            <ta e="T236" id="Seg_4141" s="T235">EMPH</ta>
            <ta e="T237" id="Seg_4142" s="T236">well</ta>
            <ta e="T238" id="Seg_4143" s="T237">Lyybyra-ACC</ta>
            <ta e="T239" id="Seg_4144" s="T238">drop-PST2-3PL</ta>
            <ta e="T240" id="Seg_4145" s="T239">Lyybyra.[NOM]</ta>
            <ta e="T241" id="Seg_4146" s="T240">say-PST2.[3SG]</ta>
            <ta e="T242" id="Seg_4147" s="T241">bring-EP-IMP.2PL</ta>
            <ta e="T243" id="Seg_4148" s="T242">father-2PL.[NOM]</ta>
            <ta e="T244" id="Seg_4149" s="T243">sharp</ta>
            <ta e="T245" id="Seg_4150" s="T244">axe-3SG-ACC</ta>
            <ta e="T246" id="Seg_4151" s="T245">bring-CVB.SEQ</ta>
            <ta e="T247" id="Seg_4152" s="T246">give-PST2-3PL</ta>
            <ta e="T248" id="Seg_4153" s="T247">bed-2PL-DAT/LOC</ta>
            <ta e="T249" id="Seg_4154" s="T248">see-CVB.SIM</ta>
            <ta e="T250" id="Seg_4155" s="T249">lie.down-EP-IMP.2PL</ta>
            <ta e="T251" id="Seg_4156" s="T250">how</ta>
            <ta e="T252" id="Seg_4157" s="T251">1SG.[NOM]</ta>
            <ta e="T253" id="Seg_4158" s="T252">make-PRS-1SG</ta>
            <ta e="T254" id="Seg_4159" s="T253">child-PL.[NOM]</ta>
            <ta e="T255" id="Seg_4160" s="T254">lie.down-CVB.SEQ</ta>
            <ta e="T256" id="Seg_4161" s="T255">stay-PST2-3PL</ta>
            <ta e="T257" id="Seg_4162" s="T256">that</ta>
            <ta e="T258" id="Seg_4163" s="T257">lie.down-PTCP.PST-3PL-DAT/LOC</ta>
            <ta e="T259" id="Seg_4164" s="T258">Lyybyra.[NOM]</ta>
            <ta e="T260" id="Seg_4165" s="T259">head-3PL-ACC</ta>
            <ta e="T261" id="Seg_4166" s="T260">cut-CVB.SIM</ta>
            <ta e="T262" id="Seg_4167" s="T261">cut.up-FREQ-PST2.[3SG]</ta>
            <ta e="T263" id="Seg_4168" s="T262">meat-3PL-ACC</ta>
            <ta e="T264" id="Seg_4169" s="T263">cook-CVB.SEQ</ta>
            <ta e="T265" id="Seg_4170" s="T264">throw-PST2.[3SG]</ta>
            <ta e="T266" id="Seg_4171" s="T265">head-PL-ACC</ta>
            <ta e="T267" id="Seg_4172" s="T266">bed-DAT/LOC</ta>
            <ta e="T268" id="Seg_4173" s="T267">in.a.row-INTNS</ta>
            <ta e="T269" id="Seg_4174" s="T268">lay-CVB.SEQ</ta>
            <ta e="T270" id="Seg_4175" s="T269">after</ta>
            <ta e="T271" id="Seg_4176" s="T270">blanket-3SG-INSTR</ta>
            <ta e="T272" id="Seg_4177" s="T271">cover-FREQ-PST2.[3SG]</ta>
            <ta e="T273" id="Seg_4178" s="T272">poker-ACC</ta>
            <ta e="T274" id="Seg_4179" s="T273">take-CVB.SEQ</ta>
            <ta e="T275" id="Seg_4180" s="T274">after</ta>
            <ta e="T276" id="Seg_4181" s="T275">stove.[NOM]</ta>
            <ta e="T277" id="Seg_4182" s="T276">lower.part-3SG-DAT/LOC</ta>
            <ta e="T278" id="Seg_4183" s="T277">go.in-CVB.SEQ</ta>
            <ta e="T279" id="Seg_4184" s="T278">stay-PST2.[3SG]</ta>
            <ta e="T280" id="Seg_4185" s="T279">this</ta>
            <ta e="T281" id="Seg_4186" s="T280">lie-TEMP-3SG</ta>
            <ta e="T282" id="Seg_4187" s="T281">Angaa.Mongus.[NOM]</ta>
            <ta e="T283" id="Seg_4188" s="T282">come-PST2.[3SG]</ta>
            <ta e="T284" id="Seg_4189" s="T283">Angaa.Mongus.[NOM]</ta>
            <ta e="T285" id="Seg_4190" s="T284">come-CVB.SEQ</ta>
            <ta e="T286" id="Seg_4191" s="T285">shout-PST2.[3SG]</ta>
            <ta e="T287" id="Seg_4192" s="T286">child-PL.[NOM]</ta>
            <ta e="T288" id="Seg_4193" s="T287">door-2PL-ACC</ta>
            <ta e="T289" id="Seg_4194" s="T288">open-EP-IMP.2PL</ta>
            <ta e="T290" id="Seg_4195" s="T289">who.[NOM]</ta>
            <ta e="T291" id="Seg_4196" s="T290">NEG</ta>
            <ta e="T292" id="Seg_4197" s="T291">open-NEG.[3SG]</ta>
            <ta e="T293" id="Seg_4198" s="T292">how.much-MLTP</ta>
            <ta e="T294" id="Seg_4199" s="T293">INDEF</ta>
            <ta e="T295" id="Seg_4200" s="T294">shout-PST2.[3SG]</ta>
            <ta e="T296" id="Seg_4201" s="T295">child-PL.[NOM]</ta>
            <ta e="T297" id="Seg_4202" s="T296">at.all</ta>
            <ta e="T298" id="Seg_4203" s="T297">open-PST2.NEG-3PL</ta>
            <ta e="T299" id="Seg_4204" s="T298">door-3SG-ACC</ta>
            <ta e="T300" id="Seg_4205" s="T299">take.out-CVB.SIM</ta>
            <ta e="T301" id="Seg_4206" s="T300">pull-CVB.SEQ</ta>
            <ta e="T302" id="Seg_4207" s="T301">go.in-PST2.[3SG]</ta>
            <ta e="T303" id="Seg_4208" s="T302">see-PST2-3SG</ta>
            <ta e="T304" id="Seg_4209" s="T303">meat.[NOM]</ta>
            <ta e="T305" id="Seg_4210" s="T304">boil-CVB.SIM</ta>
            <ta e="T306" id="Seg_4211" s="T305">stand-PRS.[3SG]</ta>
            <ta e="T307" id="Seg_4212" s="T306">child-PL.[NOM]</ta>
            <ta e="T308" id="Seg_4213" s="T307">sleep-CVB.SIM</ta>
            <ta e="T309" id="Seg_4214" s="T308">lie-PRS-3PL</ta>
            <ta e="T310" id="Seg_4215" s="T309">oh</ta>
            <ta e="T311" id="Seg_4216" s="T310">child-PL-EP-1SG.[NOM]</ta>
            <ta e="T312" id="Seg_4217" s="T311">Lyybyra.[NOM]</ta>
            <ta e="T313" id="Seg_4218" s="T312">fat-3SG-ABL</ta>
            <ta e="T314" id="Seg_4219" s="T313">eat.ones.fill-CVB.SEQ</ta>
            <ta e="T315" id="Seg_4220" s="T314">after</ta>
            <ta e="T316" id="Seg_4221" s="T315">sleep-CVB.SIM</ta>
            <ta e="T317" id="Seg_4222" s="T316">lie-PRS-3PL</ta>
            <ta e="T318" id="Seg_4223" s="T317">be-PST2.[3SG]</ta>
            <ta e="T319" id="Seg_4224" s="T318">EMPH</ta>
            <ta e="T320" id="Seg_4225" s="T319">say-PRS.[3SG]</ta>
            <ta e="T321" id="Seg_4226" s="T320">kettle-ACC</ta>
            <ta e="T322" id="Seg_4227" s="T321">take.out-PRS.[3SG]</ta>
            <ta e="T323" id="Seg_4228" s="T322">heart-3SG-ABL</ta>
            <ta e="T324" id="Seg_4229" s="T323">test-PRS.[3SG]</ta>
            <ta e="T325" id="Seg_4230" s="T324">heart-3SG.[NOM]</ta>
            <ta e="T326" id="Seg_4231" s="T325">pull-PST2.[3SG]</ta>
            <ta e="T327" id="Seg_4232" s="T326">oh</ta>
            <ta e="T328" id="Seg_4233" s="T327">Lyybyra.[NOM]</ta>
            <ta e="T329" id="Seg_4234" s="T328">1SG.[NOM]</ta>
            <ta e="T330" id="Seg_4235" s="T329">sibling-1SG.[NOM]</ta>
            <ta e="T331" id="Seg_4236" s="T330">be-PST2.[3SG]</ta>
            <ta e="T332" id="Seg_4237" s="T331">Q</ta>
            <ta e="T333" id="Seg_4238" s="T332">say-PST2.[3SG]</ta>
            <ta e="T334" id="Seg_4239" s="T333">spleen.[NOM]</ta>
            <ta e="T335" id="Seg_4240" s="T334">eat-PST2.[3SG]</ta>
            <ta e="T336" id="Seg_4241" s="T335">spleen-3SG.[NOM]</ta>
            <ta e="T337" id="Seg_4242" s="T336">pull-PST2.[3SG]</ta>
            <ta e="T338" id="Seg_4243" s="T337">oh</ta>
            <ta e="T339" id="Seg_4244" s="T338">Lyybyra.[NOM]</ta>
            <ta e="T340" id="Seg_4245" s="T339">real</ta>
            <ta e="T341" id="Seg_4246" s="T340">sibling-1SG.[NOM]</ta>
            <ta e="T342" id="Seg_4247" s="T341">be-PST2.[3SG]</ta>
            <ta e="T343" id="Seg_4248" s="T342">Q</ta>
            <ta e="T344" id="Seg_4249" s="T343">say-PST2.[3SG]</ta>
            <ta e="T345" id="Seg_4250" s="T344">then</ta>
            <ta e="T346" id="Seg_4251" s="T345">stand.up-PST2.[3SG]</ta>
            <ta e="T347" id="Seg_4252" s="T346">child-PL-3SG-ACC</ta>
            <ta e="T348" id="Seg_4253" s="T347">open-CVB.SIM</ta>
            <ta e="T349" id="Seg_4254" s="T348">pull-PST2-3SG</ta>
            <ta e="T350" id="Seg_4255" s="T349">seven</ta>
            <ta e="T351" id="Seg_4256" s="T350">head.[NOM]</ta>
            <ta e="T352" id="Seg_4257" s="T351">roll-NMNZ.[NOM]</ta>
            <ta e="T353" id="Seg_4258" s="T352">make-PST2-3PL</ta>
            <ta e="T354" id="Seg_4259" s="T353">startle-CVB.SIM</ta>
            <ta e="T355" id="Seg_4260" s="T354">with</ta>
            <ta e="T356" id="Seg_4261" s="T355">Angaa.Mongus.[NOM]</ta>
            <ta e="T357" id="Seg_4262" s="T356">shout-PST2.[3SG]</ta>
            <ta e="T358" id="Seg_4263" s="T357">Lyybyra.[NOM]</ta>
            <ta e="T359" id="Seg_4264" s="T358">where</ta>
            <ta e="T360" id="Seg_4265" s="T359">there.is-2SG=Q</ta>
            <ta e="T361" id="Seg_4266" s="T360">Lyybyra.[NOM]</ta>
            <ta e="T362" id="Seg_4267" s="T361">outside</ta>
            <ta e="T363" id="Seg_4268" s="T362">there.is-1SG</ta>
            <ta e="T364" id="Seg_4269" s="T363">say-PST2.[3SG]</ta>
            <ta e="T365" id="Seg_4270" s="T364">that-ACC</ta>
            <ta e="T366" id="Seg_4271" s="T365">hear-PST1-3SG</ta>
            <ta e="T367" id="Seg_4272" s="T366">and</ta>
            <ta e="T368" id="Seg_4273" s="T367">house-3SG-ACC</ta>
            <ta e="T369" id="Seg_4274" s="T368">split-CVB.SIM</ta>
            <ta e="T370" id="Seg_4275" s="T369">fly-CVB.SEQ</ta>
            <ta e="T371" id="Seg_4276" s="T370">go.out-PRS.[3SG]</ta>
            <ta e="T372" id="Seg_4277" s="T371">Lyybyra.[NOM]</ta>
            <ta e="T373" id="Seg_4278" s="T372">where</ta>
            <ta e="T374" id="Seg_4279" s="T373">NEG</ta>
            <ta e="T375" id="Seg_4280" s="T374">NEG.EX</ta>
            <ta e="T376" id="Seg_4281" s="T375">Lyybyra</ta>
            <ta e="T377" id="Seg_4282" s="T376">where-2SG=Q</ta>
            <ta e="T378" id="Seg_4283" s="T377">house.[NOM]</ta>
            <ta e="T379" id="Seg_4284" s="T378">inside-3SG-DAT/LOC</ta>
            <ta e="T380" id="Seg_4285" s="T379">there.is-1SG</ta>
            <ta e="T381" id="Seg_4286" s="T380">say-PST2.[3SG]</ta>
            <ta e="T382" id="Seg_4287" s="T381">Lyybyra.[NOM]</ta>
            <ta e="T383" id="Seg_4288" s="T382">house-DAT/LOC</ta>
            <ta e="T384" id="Seg_4289" s="T383">fly-CVB.SEQ</ta>
            <ta e="T385" id="Seg_4290" s="T384">fall-PST2.[3SG]</ta>
            <ta e="T386" id="Seg_4291" s="T385">Lyybyra.[NOM]</ta>
            <ta e="T387" id="Seg_4292" s="T386">NEG.EX</ta>
            <ta e="T388" id="Seg_4293" s="T387">Lyybyra</ta>
            <ta e="T389" id="Seg_4294" s="T388">where-2SG=Q</ta>
            <ta e="T390" id="Seg_4295" s="T389">chimney-DAT/LOC</ta>
            <ta e="T391" id="Seg_4296" s="T390">there.is-1SG</ta>
            <ta e="T392" id="Seg_4297" s="T391">chimney-DAT/LOC</ta>
            <ta e="T393" id="Seg_4298" s="T392">look.at-PST2-3SG</ta>
            <ta e="T394" id="Seg_4299" s="T393">Lyybyra.[NOM]</ta>
            <ta e="T395" id="Seg_4300" s="T394">where</ta>
            <ta e="T396" id="Seg_4301" s="T395">NEG</ta>
            <ta e="T397" id="Seg_4302" s="T396">NEG.EX</ta>
            <ta e="T398" id="Seg_4303" s="T397">Lyybyra.[NOM]</ta>
            <ta e="T399" id="Seg_4304" s="T398">where</ta>
            <ta e="T400" id="Seg_4305" s="T399">there.is-2SG=Q</ta>
            <ta e="T401" id="Seg_4306" s="T400">say-PST2.[3SG]</ta>
            <ta e="T402" id="Seg_4307" s="T401">Lyybyra.[NOM]</ta>
            <ta e="T403" id="Seg_4308" s="T402">stove.[NOM]</ta>
            <ta e="T404" id="Seg_4309" s="T403">lower.part-3SG-DAT/LOC</ta>
            <ta e="T405" id="Seg_4310" s="T404">there.is-1SG</ta>
            <ta e="T406" id="Seg_4311" s="T405">say-PST2.[3SG]</ta>
            <ta e="T407" id="Seg_4312" s="T406">that-ACC</ta>
            <ta e="T408" id="Seg_4313" s="T407">Angaa.Mongus.[NOM]</ta>
            <ta e="T409" id="Seg_4314" s="T408">hear-PST1-3SG</ta>
            <ta e="T410" id="Seg_4315" s="T409">and</ta>
            <ta e="T411" id="Seg_4316" s="T410">head-3SG-INSTR</ta>
            <ta e="T412" id="Seg_4317" s="T411">go.in-CVB.PURP</ta>
            <ta e="T413" id="Seg_4318" s="T412">make-PST2.[3SG]</ta>
            <ta e="T414" id="Seg_4319" s="T413">that-DAT/LOC</ta>
            <ta e="T415" id="Seg_4320" s="T414">Lyybyra.[NOM]</ta>
            <ta e="T416" id="Seg_4321" s="T415">speak-PRS.[3SG]</ta>
            <ta e="T417" id="Seg_4322" s="T416">grandfather.[NOM]</ta>
            <ta e="T418" id="Seg_4323" s="T417">head-2SG-INSTR</ta>
            <ta e="T419" id="Seg_4324" s="T418">go.in-EP-NEG.[IMP.2SG]</ta>
            <ta e="T420" id="Seg_4325" s="T419">fit-FUT-2SG</ta>
            <ta e="T421" id="Seg_4326" s="T420">NEG-3SG</ta>
            <ta e="T422" id="Seg_4327" s="T421">bottom-2SG-INSTR</ta>
            <ta e="T423" id="Seg_4328" s="T422">go.in.[IMP.2SG]</ta>
            <ta e="T424" id="Seg_4329" s="T423">Angaa.Mongus.[NOM]</ta>
            <ta e="T425" id="Seg_4330" s="T424">bottom-3SG-INSTR</ta>
            <ta e="T426" id="Seg_4331" s="T425">go.in-PST2.[3SG]</ta>
            <ta e="T427" id="Seg_4332" s="T426">that</ta>
            <ta e="T428" id="Seg_4333" s="T427">go.in-CVB.SEQ</ta>
            <ta e="T429" id="Seg_4334" s="T428">be-TEMP-3SG</ta>
            <ta e="T430" id="Seg_4335" s="T429">Lyybyra.[NOM]</ta>
            <ta e="T431" id="Seg_4336" s="T430">get.red-INCH</ta>
            <ta e="T432" id="Seg_4337" s="T431">go-PTCP.PST</ta>
            <ta e="T433" id="Seg_4338" s="T432">poker-3SG-INSTR</ta>
            <ta e="T434" id="Seg_4339" s="T433">bottom-3SG-GEN</ta>
            <ta e="T435" id="Seg_4340" s="T434">inside-3SG-INSTR</ta>
            <ta e="T436" id="Seg_4341" s="T435">push-PST2.[3SG]</ta>
            <ta e="T437" id="Seg_4342" s="T436">Angaa.Mongus.[NOM]</ta>
            <ta e="T438" id="Seg_4343" s="T437">die-CVB.SEQ</ta>
            <ta e="T439" id="Seg_4344" s="T438">stay-PST2.[3SG]</ta>
            <ta e="T440" id="Seg_4345" s="T439">Lyybyra.[NOM]</ta>
            <ta e="T441" id="Seg_4346" s="T440">go.out-PST1-3SG</ta>
            <ta e="T442" id="Seg_4347" s="T441">and</ta>
            <ta e="T443" id="Seg_4348" s="T442">house-3SG-DAT/LOC</ta>
            <ta e="T444" id="Seg_4349" s="T443">go-CVB.SEQ</ta>
            <ta e="T445" id="Seg_4350" s="T444">stay-PST2.[3SG]</ta>
            <ta e="T446" id="Seg_4351" s="T445">so</ta>
            <ta e="T447" id="Seg_4352" s="T446">be.rich-CVB.SEQ-be.rich-CVB.SEQ</ta>
            <ta e="T448" id="Seg_4353" s="T447">live-PST2.[3SG]</ta>
            <ta e="T449" id="Seg_4354" s="T448">well</ta>
            <ta e="T450" id="Seg_4355" s="T449">stop-PST1-3SG</ta>
         </annotation>
         <annotation name="gg" tierref="gd">
            <ta e="T1" id="Seg_4356" s="T0">Lyybyra.[NOM]</ta>
            <ta e="T2" id="Seg_4357" s="T1">rudern-CVB.SEQ</ta>
            <ta e="T3" id="Seg_4358" s="T2">sanft.schaukeln-CVB.SEQ</ta>
            <ta e="T4" id="Seg_4359" s="T3">gehen-FREQ-PRS.[3SG]</ta>
            <ta e="T5" id="Seg_4360" s="T4">und</ta>
            <ta e="T6" id="Seg_4361" s="T5">gehen-CVB.SEQ-gehen-CVB.SEQ</ta>
            <ta e="T7" id="Seg_4362" s="T6">Stein-DAT/LOC</ta>
            <ta e="T8" id="Seg_4363" s="T7">stoßen-PST2.[3SG]</ta>
            <ta e="T9" id="Seg_4364" s="T8">kleines.Boot-3SG.[NOM]</ta>
            <ta e="T10" id="Seg_4365" s="T9">haften-CVB.SEQ</ta>
            <ta e="T11" id="Seg_4366" s="T10">bleiben-PST2.[3SG]</ta>
            <ta e="T12" id="Seg_4367" s="T11">Ruder-3SG-INSTR</ta>
            <ta e="T13" id="Seg_4368" s="T12">schlagen-EP-PST2.[3SG]</ta>
            <ta e="T14" id="Seg_4369" s="T13">Ruder-3SG.[NOM]</ta>
            <ta e="T15" id="Seg_4370" s="T14">haften-CVB.SEQ</ta>
            <ta e="T16" id="Seg_4371" s="T15">bleiben-PST2.[3SG]</ta>
            <ta e="T17" id="Seg_4372" s="T16">Hand-3SG-INSTR</ta>
            <ta e="T18" id="Seg_4373" s="T17">schlagen-EP-PST2.[3SG]</ta>
            <ta e="T19" id="Seg_4374" s="T18">Hand-3SG.[NOM]</ta>
            <ta e="T20" id="Seg_4375" s="T19">haften-CVB.SEQ</ta>
            <ta e="T21" id="Seg_4376" s="T20">bleiben-PST2.[3SG]</ta>
            <ta e="T22" id="Seg_4377" s="T21">anderer</ta>
            <ta e="T23" id="Seg_4378" s="T22">Hand-3SG-INSTR</ta>
            <ta e="T24" id="Seg_4379" s="T23">schlagen-PRS.[3SG]</ta>
            <ta e="T25" id="Seg_4380" s="T24">anderer-3SG.[NOM]</ta>
            <ta e="T26" id="Seg_4381" s="T25">auch</ta>
            <ta e="T27" id="Seg_4382" s="T26">haften-CVB.SEQ</ta>
            <ta e="T28" id="Seg_4383" s="T27">bleiben-PST2.[3SG]</ta>
            <ta e="T29" id="Seg_4384" s="T28">Fuß-3SG-INSTR</ta>
            <ta e="T30" id="Seg_4385" s="T29">treten-PST2.[3SG]</ta>
            <ta e="T31" id="Seg_4386" s="T30">Fuß-3SG.[NOM]</ta>
            <ta e="T32" id="Seg_4387" s="T31">haften-CVB.SEQ</ta>
            <ta e="T33" id="Seg_4388" s="T32">bleiben-PST2.[3SG]</ta>
            <ta e="T34" id="Seg_4389" s="T33">nächster</ta>
            <ta e="T35" id="Seg_4390" s="T34">Fuß-3SG-INSTR</ta>
            <ta e="T36" id="Seg_4391" s="T35">treten-PST2-3SG</ta>
            <ta e="T37" id="Seg_4392" s="T36">auch</ta>
            <ta e="T38" id="Seg_4393" s="T37">haften-CVB.SEQ</ta>
            <ta e="T39" id="Seg_4394" s="T38">bleiben-PST2.[3SG]</ta>
            <ta e="T40" id="Seg_4395" s="T39">Bauch-3SG-INSTR</ta>
            <ta e="T41" id="Seg_4396" s="T40">stoßen-PST2-3SG</ta>
            <ta e="T42" id="Seg_4397" s="T41">Bauch-3SG.[NOM]</ta>
            <ta e="T43" id="Seg_4398" s="T42">auch</ta>
            <ta e="T44" id="Seg_4399" s="T43">haften-CVB.SEQ</ta>
            <ta e="T45" id="Seg_4400" s="T44">bleiben-PST2.[3SG]</ta>
            <ta e="T46" id="Seg_4401" s="T45">dann</ta>
            <ta e="T47" id="Seg_4402" s="T46">Kopf-3SG-INSTR</ta>
            <ta e="T48" id="Seg_4403" s="T47">schlagen-EP-PST2.[3SG]</ta>
            <ta e="T49" id="Seg_4404" s="T48">Kopf-3SG.[NOM]</ta>
            <ta e="T50" id="Seg_4405" s="T49">auch</ta>
            <ta e="T51" id="Seg_4406" s="T50">haften-CVB.SEQ</ta>
            <ta e="T52" id="Seg_4407" s="T51">bleiben-PST2.[3SG]</ta>
            <ta e="T53" id="Seg_4408" s="T52">haften-CVB.SEQ</ta>
            <ta e="T54" id="Seg_4409" s="T53">stehen-INFER-3SG</ta>
            <ta e="T55" id="Seg_4410" s="T54">jenes</ta>
            <ta e="T56" id="Seg_4411" s="T55">stehen-TEMP-3SG</ta>
            <ta e="T57" id="Seg_4412" s="T56">Angaa.Mongus.[NOM]</ta>
            <ta e="T58" id="Seg_4413" s="T57">kommen-PST2.[3SG]</ta>
            <ta e="T59" id="Seg_4414" s="T58">welch.Freude</ta>
            <ta e="T60" id="Seg_4415" s="T59">abendlich</ta>
            <ta e="T61" id="Seg_4416" s="T60">Imbiss-PROPR.[NOM]</ta>
            <ta e="T62" id="Seg_4417" s="T61">sein-PST1-1SG</ta>
            <ta e="T63" id="Seg_4418" s="T62">sagen-PTCP.PST-3SG-INSTR</ta>
            <ta e="T64" id="Seg_4419" s="T63">Lyybyra-ACC</ta>
            <ta e="T65" id="Seg_4420" s="T64">nehmen-CVB.SEQ</ta>
            <ta e="T66" id="Seg_4421" s="T65">Tasche-3SG-DAT/LOC</ta>
            <ta e="T67" id="Seg_4422" s="T66">hineintun-CVB.SEQ</ta>
            <ta e="T68" id="Seg_4423" s="T67">werfen-PST2.[3SG]</ta>
            <ta e="T69" id="Seg_4424" s="T68">Haus-3SG-DAT/LOC</ta>
            <ta e="T70" id="Seg_4425" s="T69">gehen-PST2.[3SG]</ta>
            <ta e="T71" id="Seg_4426" s="T70">Pfad-3SG-DAT/LOC</ta>
            <ta e="T72" id="Seg_4427" s="T71">ankommen-CVB.SEQ</ta>
            <ta e="T73" id="Seg_4428" s="T72">Angaa.Mongus.[NOM]</ta>
            <ta e="T74" id="Seg_4429" s="T73">sich.entleeren-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T75" id="Seg_4430" s="T74">wollen-PST2.[3SG]</ta>
            <ta e="T76" id="Seg_4431" s="T75">jenes</ta>
            <ta e="T77" id="Seg_4432" s="T76">sich.entleeren-CVB.SIM</ta>
            <ta e="T78" id="Seg_4433" s="T77">sitzen-TEMP-3SG</ta>
            <ta e="T79" id="Seg_4434" s="T78">Lyybyra.[NOM]</ta>
            <ta e="T80" id="Seg_4435" s="T79">Tasche-3SG-GEN</ta>
            <ta e="T81" id="Seg_4436" s="T80">Loch-3SG-ACC</ta>
            <ta e="T82" id="Seg_4437" s="T81">durch</ta>
            <ta e="T83" id="Seg_4438" s="T82">fallen-CVB.SEQ</ta>
            <ta e="T84" id="Seg_4439" s="T83">bleiben-PST2.[3SG]</ta>
            <ta e="T85" id="Seg_4440" s="T84">Angaa.Mongus.[NOM]</ta>
            <ta e="T86" id="Seg_4441" s="T85">was-ACC</ta>
            <ta e="T87" id="Seg_4442" s="T86">NEG</ta>
            <ta e="T88" id="Seg_4443" s="T87">bemerken-NEG.CVB.SIM</ta>
            <ta e="T89" id="Seg_4444" s="T88">Hose-3SG-ACC</ta>
            <ta e="T90" id="Seg_4445" s="T89">heben-EP-REFL-PST1-3SG</ta>
            <ta e="T91" id="Seg_4446" s="T90">und</ta>
            <ta e="T92" id="Seg_4447" s="T91">Haus-3SG-DAT/LOC</ta>
            <ta e="T93" id="Seg_4448" s="T92">gehen-PST2.[3SG]</ta>
            <ta e="T94" id="Seg_4449" s="T93">Haus-3SG-DAT/LOC</ta>
            <ta e="T95" id="Seg_4450" s="T94">Rand-3SG-DAT/LOC</ta>
            <ta e="T96" id="Seg_4451" s="T95">Mantel-3SG-ACC</ta>
            <ta e="T97" id="Seg_4452" s="T96">Speicher-DAT/LOC</ta>
            <ta e="T98" id="Seg_4453" s="T97">aufhängen-CVB.SEQ</ta>
            <ta e="T99" id="Seg_4454" s="T98">nachdem</ta>
            <ta e="T100" id="Seg_4455" s="T99">hineingehen-PST2.[3SG]</ta>
            <ta e="T101" id="Seg_4456" s="T100">Alte-3SG.[NOM]</ta>
            <ta e="T102" id="Seg_4457" s="T101">nähen-CVB.SIM</ta>
            <ta e="T103" id="Seg_4458" s="T102">sitzen-PTCP.PRS</ta>
            <ta e="T104" id="Seg_4459" s="T103">sein-PST2.[3SG]</ta>
            <ta e="T105" id="Seg_4460" s="T104">Alte.[NOM]</ta>
            <ta e="T106" id="Seg_4461" s="T105">Geschenk-1SG-ACC</ta>
            <ta e="T107" id="Seg_4462" s="T106">bringen-PST1-1SG</ta>
            <ta e="T108" id="Seg_4463" s="T107">Mantel-EP-1SG.[NOM]</ta>
            <ta e="T109" id="Seg_4464" s="T108">Tasche-3SG-DAT/LOC</ta>
            <ta e="T110" id="Seg_4465" s="T109">es.gibt</ta>
            <ta e="T111" id="Seg_4466" s="T110">draußen</ta>
            <ta e="T112" id="Seg_4467" s="T111">jenes-ACC</ta>
            <ta e="T113" id="Seg_4468" s="T112">hereinbringen-CVB.SEQ</ta>
            <ta e="T114" id="Seg_4469" s="T113">sehen.[IMP.2SG]</ta>
            <ta e="T115" id="Seg_4470" s="T114">sagen-PST2.[3SG]</ta>
            <ta e="T116" id="Seg_4471" s="T115">Alte-3SG.[NOM]</ta>
            <ta e="T117" id="Seg_4472" s="T116">sich.freuen-NMNZ-3SG-DAT/LOC</ta>
            <ta e="T118" id="Seg_4473" s="T117">Nadel-PROPR</ta>
            <ta e="T119" id="Seg_4474" s="T118">Fingerhut-3SG-ACC</ta>
            <ta e="T120" id="Seg_4475" s="T119">verschlingen-CVB.SEQ</ta>
            <ta e="T121" id="Seg_4476" s="T120">werfen-PST2.[3SG]</ta>
            <ta e="T122" id="Seg_4477" s="T121">hinausgehen-CVB.SIM</ta>
            <ta e="T123" id="Seg_4478" s="T122">rennen-PST2.[3SG]</ta>
            <ta e="T124" id="Seg_4479" s="T123">finden-NEG.CVB.SIM</ta>
            <ta e="T125" id="Seg_4480" s="T124">zurückkommen-CVB.SEQ</ta>
            <ta e="T126" id="Seg_4481" s="T125">hineingehen-PST2.[3SG]</ta>
            <ta e="T127" id="Seg_4482" s="T126">Tasche-2SG-DAT/LOC</ta>
            <ta e="T128" id="Seg_4483" s="T127">was.[NOM]</ta>
            <ta e="T129" id="Seg_4484" s="T128">NEG</ta>
            <ta e="T130" id="Seg_4485" s="T129">NEG.EX</ta>
            <ta e="T131" id="Seg_4486" s="T130">sagen-PST2.[3SG]</ta>
            <ta e="T132" id="Seg_4487" s="T131">Angaa.Mongus.[NOM]</ta>
            <ta e="T133" id="Seg_4488" s="T132">selbst-2SG.[NOM]</ta>
            <ta e="T134" id="Seg_4489" s="T133">essen-INFER-EP-2SG</ta>
            <ta e="T135" id="Seg_4490" s="T134">EMPH</ta>
            <ta e="T136" id="Seg_4491" s="T135">sagen-PST2.[3SG]</ta>
            <ta e="T137" id="Seg_4492" s="T136">und</ta>
            <ta e="T138" id="Seg_4493" s="T137">Alte.[NOM]</ta>
            <ta e="T139" id="Seg_4494" s="T138">Bauch-3SG-ACC</ta>
            <ta e="T140" id="Seg_4495" s="T139">spalten-CVB.SIM</ta>
            <ta e="T141" id="Seg_4496" s="T140">ziehen-PRS.[3SG]</ta>
            <ta e="T142" id="Seg_4497" s="T141">Alte.[NOM]</ta>
            <ta e="T143" id="Seg_4498" s="T142">Bauch-3SG-ABL</ta>
            <ta e="T144" id="Seg_4499" s="T143">Nadel-PROPR</ta>
            <ta e="T145" id="Seg_4500" s="T144">Fingerhut.[NOM]</ta>
            <ta e="T146" id="Seg_4501" s="T145">klirr</ta>
            <ta e="T147" id="Seg_4502" s="T146">machen-CVB.SIM</ta>
            <ta e="T148" id="Seg_4503" s="T147">fallen-PST2.[3SG]</ta>
            <ta e="T149" id="Seg_4504" s="T148">oh</ta>
            <ta e="T150" id="Seg_4505" s="T149">lieb</ta>
            <ta e="T151" id="Seg_4506" s="T150">Frau-1SG-ACC</ta>
            <ta e="T152" id="Seg_4507" s="T151">schreien-PST2.[3SG]</ta>
            <ta e="T153" id="Seg_4508" s="T152">Angaa.Mongus.[NOM]</ta>
            <ta e="T154" id="Seg_4509" s="T153">zurück</ta>
            <ta e="T155" id="Seg_4510" s="T154">laufen-PST2.[3SG]</ta>
            <ta e="T156" id="Seg_4511" s="T155">Lyybyra.[NOM]</ta>
            <ta e="T157" id="Seg_4512" s="T156">Pfad-DAT/LOC</ta>
            <ta e="T158" id="Seg_4513" s="T157">dieses</ta>
            <ta e="T159" id="Seg_4514" s="T158">starr.liegen-CVB.SIM</ta>
            <ta e="T160" id="Seg_4515" s="T159">liegen-PRS.[3SG]</ta>
            <ta e="T161" id="Seg_4516" s="T160">nehmen-PRS.[3SG]</ta>
            <ta e="T162" id="Seg_4517" s="T161">und</ta>
            <ta e="T163" id="Seg_4518" s="T162">Tasche-3SG-DAT/LOC</ta>
            <ta e="T164" id="Seg_4519" s="T163">hineintun-PRS.[3SG]</ta>
            <ta e="T165" id="Seg_4520" s="T164">zurück</ta>
            <ta e="T166" id="Seg_4521" s="T165">laufen-CVB.SEQ</ta>
            <ta e="T167" id="Seg_4522" s="T166">kommen-PRS.[3SG]</ta>
            <ta e="T168" id="Seg_4523" s="T167">Haus-3SG-DAT/LOC</ta>
            <ta e="T169" id="Seg_4524" s="T168">hineingehen-CVB.SEQ</ta>
            <ta e="T170" id="Seg_4525" s="T169">Lyybyra-ACC</ta>
            <ta e="T171" id="Seg_4526" s="T170">Kesselhaken-DAT/LOC</ta>
            <ta e="T172" id="Seg_4527" s="T171">aufhängen-CVB.SEQ</ta>
            <ta e="T173" id="Seg_4528" s="T172">werfen-PRS.[3SG]</ta>
            <ta e="T174" id="Seg_4529" s="T173">Angaa.Mongus.[NOM]</ta>
            <ta e="T175" id="Seg_4530" s="T174">sieben</ta>
            <ta e="T176" id="Seg_4531" s="T175">glatzköpfig</ta>
            <ta e="T177" id="Seg_4532" s="T176">Kind-PROPR.[NOM]</ta>
            <ta e="T178" id="Seg_4533" s="T177">sein-PST2.[3SG]</ta>
            <ta e="T179" id="Seg_4534" s="T178">jenes</ta>
            <ta e="T180" id="Seg_4535" s="T179">Kind-PL-3SG-DAT/LOC</ta>
            <ta e="T181" id="Seg_4536" s="T180">sagen-PST2.[3SG]</ta>
            <ta e="T182" id="Seg_4537" s="T181">Kind-PL.[NOM]</ta>
            <ta e="T183" id="Seg_4538" s="T182">Lyybyra.[NOM]</ta>
            <ta e="T184" id="Seg_4539" s="T183">Fett-3SG.[NOM]</ta>
            <ta e="T185" id="Seg_4540" s="T184">tropfen-FUT-3SG</ta>
            <ta e="T186" id="Seg_4541" s="T185">sehen-PTCP.PRS</ta>
            <ta e="T187" id="Seg_4542" s="T186">sein-EP-IMP.2PL</ta>
            <ta e="T188" id="Seg_4543" s="T187">kommen-PTCP.PRS-1SG-DAT/LOC</ta>
            <ta e="T189" id="Seg_4544" s="T188">kochen-FUT-EP-IMP.2PL</ta>
            <ta e="T190" id="Seg_4545" s="T189">sagen-PST1-3SG</ta>
            <ta e="T191" id="Seg_4546" s="T190">und</ta>
            <ta e="T192" id="Seg_4547" s="T191">hinausgehen-CVB.SEQ</ta>
            <ta e="T193" id="Seg_4548" s="T192">bleiben-PST2.[3SG]</ta>
            <ta e="T194" id="Seg_4549" s="T193">wie.viel</ta>
            <ta e="T195" id="Seg_4550" s="T194">NEG</ta>
            <ta e="T196" id="Seg_4551" s="T195">sein-NEG.CVB.SIM</ta>
            <ta e="T197" id="Seg_4552" s="T196">Lyybyra.[NOM]</ta>
            <ta e="T198" id="Seg_4553" s="T197">doch</ta>
            <ta e="T199" id="Seg_4554" s="T198">pinkeln-PST2.[3SG]</ta>
            <ta e="T200" id="Seg_4555" s="T199">Kind-PL.[NOM]</ta>
            <ta e="T201" id="Seg_4556" s="T200">dort</ta>
            <ta e="T202" id="Seg_4557" s="T201">schreien-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T203" id="Seg_4558" s="T202">welch.Freude</ta>
            <ta e="T204" id="Seg_4559" s="T203">Lyybyra.[NOM]</ta>
            <ta e="T205" id="Seg_4560" s="T204">flüssig</ta>
            <ta e="T206" id="Seg_4561" s="T205">Fett-3SG.[NOM]</ta>
            <ta e="T207" id="Seg_4562" s="T206">ausgießen-EP-REFL-PST1-3SG</ta>
            <ta e="T208" id="Seg_4563" s="T207">dann</ta>
            <ta e="T209" id="Seg_4564" s="T208">Lyybyra.[NOM]</ta>
            <ta e="T210" id="Seg_4565" s="T209">doch</ta>
            <ta e="T211" id="Seg_4566" s="T210">sich.entleeren-PRS.[3SG]</ta>
            <ta e="T212" id="Seg_4567" s="T211">Kind-PL.[NOM]</ta>
            <ta e="T213" id="Seg_4568" s="T212">schreien-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T214" id="Seg_4569" s="T213">welch.Freude</ta>
            <ta e="T215" id="Seg_4570" s="T214">Lyybyra.[NOM]</ta>
            <ta e="T216" id="Seg_4571" s="T215">dickflüssig</ta>
            <ta e="T217" id="Seg_4572" s="T216">Fett-3SG.[NOM]</ta>
            <ta e="T218" id="Seg_4573" s="T217">ausgießen-EP-REFL-PST1-3SG</ta>
            <ta e="T219" id="Seg_4574" s="T218">Lyybyra.[NOM]</ta>
            <ta e="T220" id="Seg_4575" s="T219">Kind-PL-DAT/LOC</ta>
            <ta e="T221" id="Seg_4576" s="T220">sagen-PST2.[3SG]</ta>
            <ta e="T222" id="Seg_4577" s="T221">Kind-PL.[NOM]</ta>
            <ta e="T223" id="Seg_4578" s="T222">fallen.lassen-EP-IMP.2PL</ta>
            <ta e="T224" id="Seg_4579" s="T223">1SG-ACC</ta>
            <ta e="T225" id="Seg_4580" s="T224">1SG.[NOM]</ta>
            <ta e="T226" id="Seg_4581" s="T225">2PL-DAT/LOC</ta>
            <ta e="T227" id="Seg_4582" s="T226">Löffel.[NOM]</ta>
            <ta e="T228" id="Seg_4583" s="T227">machen-CVB.SEQ</ta>
            <ta e="T229" id="Seg_4584" s="T228">geben-FUT-1SG</ta>
            <ta e="T230" id="Seg_4585" s="T229">1SG.[NOM]</ta>
            <ta e="T231" id="Seg_4586" s="T230">Fett-1SG-ACC</ta>
            <ta e="T232" id="Seg_4587" s="T231">essen-PTCP.PRS-2PL-DAT/LOC</ta>
            <ta e="T233" id="Seg_4588" s="T232">gut.[NOM]</ta>
            <ta e="T234" id="Seg_4589" s="T233">sein-FUT.[3SG]</ta>
            <ta e="T235" id="Seg_4590" s="T234">na</ta>
            <ta e="T236" id="Seg_4591" s="T235">EMPH</ta>
            <ta e="T237" id="Seg_4592" s="T236">na</ta>
            <ta e="T238" id="Seg_4593" s="T237">Lyybyra-ACC</ta>
            <ta e="T239" id="Seg_4594" s="T238">fallen.lassen-PST2-3PL</ta>
            <ta e="T240" id="Seg_4595" s="T239">Lyybyra.[NOM]</ta>
            <ta e="T241" id="Seg_4596" s="T240">sagen-PST2.[3SG]</ta>
            <ta e="T242" id="Seg_4597" s="T241">bringen-EP-IMP.2PL</ta>
            <ta e="T243" id="Seg_4598" s="T242">Vater-2PL.[NOM]</ta>
            <ta e="T244" id="Seg_4599" s="T243">scharf</ta>
            <ta e="T245" id="Seg_4600" s="T244">Beil-3SG-ACC</ta>
            <ta e="T246" id="Seg_4601" s="T245">holen-CVB.SEQ</ta>
            <ta e="T247" id="Seg_4602" s="T246">geben-PST2-3PL</ta>
            <ta e="T248" id="Seg_4603" s="T247">Bett-2PL-DAT/LOC</ta>
            <ta e="T249" id="Seg_4604" s="T248">sehen-CVB.SIM</ta>
            <ta e="T250" id="Seg_4605" s="T249">sich.hinlegen-EP-IMP.2PL</ta>
            <ta e="T251" id="Seg_4606" s="T250">wie</ta>
            <ta e="T252" id="Seg_4607" s="T251">1SG.[NOM]</ta>
            <ta e="T253" id="Seg_4608" s="T252">machen-PRS-1SG</ta>
            <ta e="T254" id="Seg_4609" s="T253">Kind-PL.[NOM]</ta>
            <ta e="T255" id="Seg_4610" s="T254">sich.hinlegen-CVB.SEQ</ta>
            <ta e="T256" id="Seg_4611" s="T255">bleiben-PST2-3PL</ta>
            <ta e="T257" id="Seg_4612" s="T256">jenes</ta>
            <ta e="T258" id="Seg_4613" s="T257">sich.hinlegen-PTCP.PST-3PL-DAT/LOC</ta>
            <ta e="T259" id="Seg_4614" s="T258">Lyybyra.[NOM]</ta>
            <ta e="T260" id="Seg_4615" s="T259">Kopf-3PL-ACC</ta>
            <ta e="T261" id="Seg_4616" s="T260">schneiden-CVB.SIM</ta>
            <ta e="T262" id="Seg_4617" s="T261">zerschneiden-FREQ-PST2.[3SG]</ta>
            <ta e="T263" id="Seg_4618" s="T262">Fleisch-3PL-ACC</ta>
            <ta e="T264" id="Seg_4619" s="T263">kochen-CVB.SEQ</ta>
            <ta e="T265" id="Seg_4620" s="T264">werfen-PST2.[3SG]</ta>
            <ta e="T266" id="Seg_4621" s="T265">Kopf-PL-ACC</ta>
            <ta e="T267" id="Seg_4622" s="T266">Bett-DAT/LOC</ta>
            <ta e="T268" id="Seg_4623" s="T267">in.einer.Reihe-INTNS</ta>
            <ta e="T269" id="Seg_4624" s="T268">legen-CVB.SEQ</ta>
            <ta e="T270" id="Seg_4625" s="T269">nachdem</ta>
            <ta e="T271" id="Seg_4626" s="T270">Bettdecke-3SG-INSTR</ta>
            <ta e="T272" id="Seg_4627" s="T271">bedecken-FREQ-PST2.[3SG]</ta>
            <ta e="T273" id="Seg_4628" s="T272">Schürhaken-ACC</ta>
            <ta e="T274" id="Seg_4629" s="T273">nehmen-CVB.SEQ</ta>
            <ta e="T275" id="Seg_4630" s="T274">nachdem</ta>
            <ta e="T276" id="Seg_4631" s="T275">Herd.[NOM]</ta>
            <ta e="T277" id="Seg_4632" s="T276">Unterteil-3SG-DAT/LOC</ta>
            <ta e="T278" id="Seg_4633" s="T277">hineingehen-CVB.SEQ</ta>
            <ta e="T279" id="Seg_4634" s="T278">bleiben-PST2.[3SG]</ta>
            <ta e="T280" id="Seg_4635" s="T279">dieses</ta>
            <ta e="T281" id="Seg_4636" s="T280">liegen-TEMP-3SG</ta>
            <ta e="T282" id="Seg_4637" s="T281">Angaa.Mongus.[NOM]</ta>
            <ta e="T283" id="Seg_4638" s="T282">kommen-PST2.[3SG]</ta>
            <ta e="T284" id="Seg_4639" s="T283">Angaa.Mongus.[NOM]</ta>
            <ta e="T285" id="Seg_4640" s="T284">kommen-CVB.SEQ</ta>
            <ta e="T286" id="Seg_4641" s="T285">schreien-PST2.[3SG]</ta>
            <ta e="T287" id="Seg_4642" s="T286">Kind-PL.[NOM]</ta>
            <ta e="T288" id="Seg_4643" s="T287">Tür-2PL-ACC</ta>
            <ta e="T289" id="Seg_4644" s="T288">öffnen-EP-IMP.2PL</ta>
            <ta e="T290" id="Seg_4645" s="T289">wer.[NOM]</ta>
            <ta e="T291" id="Seg_4646" s="T290">NEG</ta>
            <ta e="T292" id="Seg_4647" s="T291">öffnen-NEG.[3SG]</ta>
            <ta e="T293" id="Seg_4648" s="T292">wie.viel-MLTP</ta>
            <ta e="T294" id="Seg_4649" s="T293">INDEF</ta>
            <ta e="T295" id="Seg_4650" s="T294">schreien-PST2.[3SG]</ta>
            <ta e="T296" id="Seg_4651" s="T295">Kind-PL.[NOM]</ta>
            <ta e="T297" id="Seg_4652" s="T296">ganz</ta>
            <ta e="T298" id="Seg_4653" s="T297">öffnen-PST2.NEG-3PL</ta>
            <ta e="T299" id="Seg_4654" s="T298">Tür-3SG-ACC</ta>
            <ta e="T300" id="Seg_4655" s="T299">herausnehmen-CVB.SIM</ta>
            <ta e="T301" id="Seg_4656" s="T300">ziehen-CVB.SEQ</ta>
            <ta e="T302" id="Seg_4657" s="T301">hineingehen-PST2.[3SG]</ta>
            <ta e="T303" id="Seg_4658" s="T302">sehen-PST2-3SG</ta>
            <ta e="T304" id="Seg_4659" s="T303">Fleisch.[NOM]</ta>
            <ta e="T305" id="Seg_4660" s="T304">kochen-CVB.SIM</ta>
            <ta e="T306" id="Seg_4661" s="T305">stehen-PRS.[3SG]</ta>
            <ta e="T307" id="Seg_4662" s="T306">Kind-PL.[NOM]</ta>
            <ta e="T308" id="Seg_4663" s="T307">schlafen-CVB.SIM</ta>
            <ta e="T309" id="Seg_4664" s="T308">liegen-PRS-3PL</ta>
            <ta e="T310" id="Seg_4665" s="T309">oh</ta>
            <ta e="T311" id="Seg_4666" s="T310">Kind-PL-EP-1SG.[NOM]</ta>
            <ta e="T312" id="Seg_4667" s="T311">Lyybyra.[NOM]</ta>
            <ta e="T313" id="Seg_4668" s="T312">Fett-3SG-ABL</ta>
            <ta e="T314" id="Seg_4669" s="T313">sich.satt.essen-CVB.SEQ</ta>
            <ta e="T315" id="Seg_4670" s="T314">nachdem</ta>
            <ta e="T316" id="Seg_4671" s="T315">schlafen-CVB.SIM</ta>
            <ta e="T317" id="Seg_4672" s="T316">anfangen-PRS-3PL</ta>
            <ta e="T318" id="Seg_4673" s="T317">sein-PST2.[3SG]</ta>
            <ta e="T319" id="Seg_4674" s="T318">EMPH</ta>
            <ta e="T320" id="Seg_4675" s="T319">sagen-PRS.[3SG]</ta>
            <ta e="T321" id="Seg_4676" s="T320">Kessel-ACC</ta>
            <ta e="T322" id="Seg_4677" s="T321">herausnehmen-PRS.[3SG]</ta>
            <ta e="T323" id="Seg_4678" s="T322">Herz-3SG-ABL</ta>
            <ta e="T324" id="Seg_4679" s="T323">probieren-PRS.[3SG]</ta>
            <ta e="T325" id="Seg_4680" s="T324">Herz-3SG.[NOM]</ta>
            <ta e="T326" id="Seg_4681" s="T325">ziehen-PST2.[3SG]</ta>
            <ta e="T327" id="Seg_4682" s="T326">oh</ta>
            <ta e="T328" id="Seg_4683" s="T327">Lyybyra.[NOM]</ta>
            <ta e="T329" id="Seg_4684" s="T328">1SG.[NOM]</ta>
            <ta e="T330" id="Seg_4685" s="T329">Verwandter-1SG.[NOM]</ta>
            <ta e="T331" id="Seg_4686" s="T330">sein-PST2.[3SG]</ta>
            <ta e="T332" id="Seg_4687" s="T331">Q</ta>
            <ta e="T333" id="Seg_4688" s="T332">sagen-PST2.[3SG]</ta>
            <ta e="T334" id="Seg_4689" s="T333">Milz.[NOM]</ta>
            <ta e="T335" id="Seg_4690" s="T334">essen-PST2.[3SG]</ta>
            <ta e="T336" id="Seg_4691" s="T335">Milz-3SG.[NOM]</ta>
            <ta e="T337" id="Seg_4692" s="T336">ziehen-PST2.[3SG]</ta>
            <ta e="T338" id="Seg_4693" s="T337">oh</ta>
            <ta e="T339" id="Seg_4694" s="T338">Lyybyra.[NOM]</ta>
            <ta e="T340" id="Seg_4695" s="T339">echt</ta>
            <ta e="T341" id="Seg_4696" s="T340">Verwandter-1SG.[NOM]</ta>
            <ta e="T342" id="Seg_4697" s="T341">sein-PST2.[3SG]</ta>
            <ta e="T343" id="Seg_4698" s="T342">Q</ta>
            <ta e="T344" id="Seg_4699" s="T343">sagen-PST2.[3SG]</ta>
            <ta e="T345" id="Seg_4700" s="T344">dann</ta>
            <ta e="T346" id="Seg_4701" s="T345">aufstehen-PST2.[3SG]</ta>
            <ta e="T347" id="Seg_4702" s="T346">Kind-PL-3SG-ACC</ta>
            <ta e="T348" id="Seg_4703" s="T347">öffnen-CVB.SIM</ta>
            <ta e="T349" id="Seg_4704" s="T348">ziehen-PST2-3SG</ta>
            <ta e="T350" id="Seg_4705" s="T349">sieben</ta>
            <ta e="T351" id="Seg_4706" s="T350">Kopf.[NOM]</ta>
            <ta e="T352" id="Seg_4707" s="T351">sich.rollen-NMNZ.[NOM]</ta>
            <ta e="T353" id="Seg_4708" s="T352">machen-PST2-3PL</ta>
            <ta e="T354" id="Seg_4709" s="T353">erschrecken-CVB.SIM</ta>
            <ta e="T355" id="Seg_4710" s="T354">mit</ta>
            <ta e="T356" id="Seg_4711" s="T355">Angaa.Mongus.[NOM]</ta>
            <ta e="T357" id="Seg_4712" s="T356">schreien-PST2.[3SG]</ta>
            <ta e="T358" id="Seg_4713" s="T357">Lyybyra.[NOM]</ta>
            <ta e="T359" id="Seg_4714" s="T358">wo</ta>
            <ta e="T360" id="Seg_4715" s="T359">es.gibt-2SG=Q</ta>
            <ta e="T361" id="Seg_4716" s="T360">Lyybyra.[NOM]</ta>
            <ta e="T362" id="Seg_4717" s="T361">draußen</ta>
            <ta e="T363" id="Seg_4718" s="T362">es.gibt-1SG</ta>
            <ta e="T364" id="Seg_4719" s="T363">sagen-PST2.[3SG]</ta>
            <ta e="T365" id="Seg_4720" s="T364">jenes-ACC</ta>
            <ta e="T366" id="Seg_4721" s="T365">hören-PST1-3SG</ta>
            <ta e="T367" id="Seg_4722" s="T366">und</ta>
            <ta e="T368" id="Seg_4723" s="T367">Haus-3SG-ACC</ta>
            <ta e="T369" id="Seg_4724" s="T368">spalten-CVB.SIM</ta>
            <ta e="T370" id="Seg_4725" s="T369">fliegen-CVB.SEQ</ta>
            <ta e="T371" id="Seg_4726" s="T370">hinausgehen-PRS.[3SG]</ta>
            <ta e="T372" id="Seg_4727" s="T371">Lyybyra.[NOM]</ta>
            <ta e="T373" id="Seg_4728" s="T372">wo</ta>
            <ta e="T374" id="Seg_4729" s="T373">NEG</ta>
            <ta e="T375" id="Seg_4730" s="T374">NEG.EX</ta>
            <ta e="T376" id="Seg_4731" s="T375">Lyybyra</ta>
            <ta e="T377" id="Seg_4732" s="T376">wo-2SG=Q</ta>
            <ta e="T378" id="Seg_4733" s="T377">Haus.[NOM]</ta>
            <ta e="T379" id="Seg_4734" s="T378">Inneres-3SG-DAT/LOC</ta>
            <ta e="T380" id="Seg_4735" s="T379">es.gibt-1SG</ta>
            <ta e="T381" id="Seg_4736" s="T380">sagen-PST2.[3SG]</ta>
            <ta e="T382" id="Seg_4737" s="T381">Lyybyra.[NOM]</ta>
            <ta e="T383" id="Seg_4738" s="T382">Haus-DAT/LOC</ta>
            <ta e="T384" id="Seg_4739" s="T383">fliegen-CVB.SEQ</ta>
            <ta e="T385" id="Seg_4740" s="T384">fallen-PST2.[3SG]</ta>
            <ta e="T386" id="Seg_4741" s="T385">Lyybyra.[NOM]</ta>
            <ta e="T387" id="Seg_4742" s="T386">NEG.EX</ta>
            <ta e="T388" id="Seg_4743" s="T387">Lyybyra</ta>
            <ta e="T389" id="Seg_4744" s="T388">wo-2SG=Q</ta>
            <ta e="T390" id="Seg_4745" s="T389">Rauchloch-DAT/LOC</ta>
            <ta e="T391" id="Seg_4746" s="T390">es.gibt-1SG</ta>
            <ta e="T392" id="Seg_4747" s="T391">Rauchloch-DAT/LOC</ta>
            <ta e="T393" id="Seg_4748" s="T392">schauen.auf-PST2-3SG</ta>
            <ta e="T394" id="Seg_4749" s="T393">Lyybyra.[NOM]</ta>
            <ta e="T395" id="Seg_4750" s="T394">wo</ta>
            <ta e="T396" id="Seg_4751" s="T395">NEG</ta>
            <ta e="T397" id="Seg_4752" s="T396">NEG.EX</ta>
            <ta e="T398" id="Seg_4753" s="T397">Lyybyra.[NOM]</ta>
            <ta e="T399" id="Seg_4754" s="T398">wo</ta>
            <ta e="T400" id="Seg_4755" s="T399">es.gibt-2SG=Q</ta>
            <ta e="T401" id="Seg_4756" s="T400">sagen-PST2.[3SG]</ta>
            <ta e="T402" id="Seg_4757" s="T401">Lyybyra.[NOM]</ta>
            <ta e="T403" id="Seg_4758" s="T402">Herd.[NOM]</ta>
            <ta e="T404" id="Seg_4759" s="T403">Unterteil-3SG-DAT/LOC</ta>
            <ta e="T405" id="Seg_4760" s="T404">es.gibt-1SG</ta>
            <ta e="T406" id="Seg_4761" s="T405">sagen-PST2.[3SG]</ta>
            <ta e="T407" id="Seg_4762" s="T406">jenes-ACC</ta>
            <ta e="T408" id="Seg_4763" s="T407">Angaa.Mongus.[NOM]</ta>
            <ta e="T409" id="Seg_4764" s="T408">hören-PST1-3SG</ta>
            <ta e="T410" id="Seg_4765" s="T409">und</ta>
            <ta e="T411" id="Seg_4766" s="T410">Kopf-3SG-INSTR</ta>
            <ta e="T412" id="Seg_4767" s="T411">hineingehen-CVB.PURP</ta>
            <ta e="T413" id="Seg_4768" s="T412">machen-PST2.[3SG]</ta>
            <ta e="T414" id="Seg_4769" s="T413">jenes-DAT/LOC</ta>
            <ta e="T415" id="Seg_4770" s="T414">Lyybyra.[NOM]</ta>
            <ta e="T416" id="Seg_4771" s="T415">sprechen-PRS.[3SG]</ta>
            <ta e="T417" id="Seg_4772" s="T416">Großvater.[NOM]</ta>
            <ta e="T418" id="Seg_4773" s="T417">Kopf-2SG-INSTR</ta>
            <ta e="T419" id="Seg_4774" s="T418">hineingehen-EP-NEG.[IMP.2SG]</ta>
            <ta e="T420" id="Seg_4775" s="T419">passen-FUT-2SG</ta>
            <ta e="T421" id="Seg_4776" s="T420">NEG-3SG</ta>
            <ta e="T422" id="Seg_4777" s="T421">Hintern-2SG-INSTR</ta>
            <ta e="T423" id="Seg_4778" s="T422">hineingehen.[IMP.2SG]</ta>
            <ta e="T424" id="Seg_4779" s="T423">Angaa.Mongus.[NOM]</ta>
            <ta e="T425" id="Seg_4780" s="T424">Hintern-3SG-INSTR</ta>
            <ta e="T426" id="Seg_4781" s="T425">hineingehen-PST2.[3SG]</ta>
            <ta e="T427" id="Seg_4782" s="T426">jenes</ta>
            <ta e="T428" id="Seg_4783" s="T427">hineingehen-CVB.SEQ</ta>
            <ta e="T429" id="Seg_4784" s="T428">sein-TEMP-3SG</ta>
            <ta e="T430" id="Seg_4785" s="T429">Lyybyra.[NOM]</ta>
            <ta e="T431" id="Seg_4786" s="T430">rot.werden-INCH</ta>
            <ta e="T432" id="Seg_4787" s="T431">gehen-PTCP.PST</ta>
            <ta e="T433" id="Seg_4788" s="T432">Schürhaken-3SG-INSTR</ta>
            <ta e="T434" id="Seg_4789" s="T433">Hintern-3SG-GEN</ta>
            <ta e="T435" id="Seg_4790" s="T434">Inneres-3SG-INSTR</ta>
            <ta e="T436" id="Seg_4791" s="T435">stoßen-PST2.[3SG]</ta>
            <ta e="T437" id="Seg_4792" s="T436">Angaa.Mongus.[NOM]</ta>
            <ta e="T438" id="Seg_4793" s="T437">sterben-CVB.SEQ</ta>
            <ta e="T439" id="Seg_4794" s="T438">bleiben-PST2.[3SG]</ta>
            <ta e="T440" id="Seg_4795" s="T439">Lyybyra.[NOM]</ta>
            <ta e="T441" id="Seg_4796" s="T440">hinausgehen-PST1-3SG</ta>
            <ta e="T442" id="Seg_4797" s="T441">und</ta>
            <ta e="T443" id="Seg_4798" s="T442">Haus-3SG-DAT/LOC</ta>
            <ta e="T444" id="Seg_4799" s="T443">gehen-CVB.SEQ</ta>
            <ta e="T445" id="Seg_4800" s="T444">bleiben-PST2.[3SG]</ta>
            <ta e="T446" id="Seg_4801" s="T445">so</ta>
            <ta e="T447" id="Seg_4802" s="T446">reich.sein-CVB.SEQ-reich.sein-CVB.SEQ</ta>
            <ta e="T448" id="Seg_4803" s="T447">leben-PST2.[3SG]</ta>
            <ta e="T449" id="Seg_4804" s="T448">doch</ta>
            <ta e="T450" id="Seg_4805" s="T449">aufhören-PST1-3SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_4806" s="T0">Лыыбыра.[NOM]</ta>
            <ta e="T2" id="Seg_4807" s="T1">грести-CVB.SEQ</ta>
            <ta e="T3" id="Seg_4808" s="T2">плавно.покачиваться-CVB.SEQ</ta>
            <ta e="T4" id="Seg_4809" s="T3">идти-FREQ-PRS.[3SG]</ta>
            <ta e="T5" id="Seg_4810" s="T4">да</ta>
            <ta e="T6" id="Seg_4811" s="T5">ехать-CVB.SEQ-ехать-CVB.SEQ</ta>
            <ta e="T7" id="Seg_4812" s="T6">камень-DAT/LOC</ta>
            <ta e="T8" id="Seg_4813" s="T7">толкать-PST2.[3SG]</ta>
            <ta e="T9" id="Seg_4814" s="T8">лодочка-3SG.[NOM]</ta>
            <ta e="T10" id="Seg_4815" s="T9">прилипать-CVB.SEQ</ta>
            <ta e="T11" id="Seg_4816" s="T10">оставаться-PST2.[3SG]</ta>
            <ta e="T12" id="Seg_4817" s="T11">весло-3SG-INSTR</ta>
            <ta e="T13" id="Seg_4818" s="T12">бить-EP-PST2.[3SG]</ta>
            <ta e="T14" id="Seg_4819" s="T13">весло-3SG.[NOM]</ta>
            <ta e="T15" id="Seg_4820" s="T14">прилипать-CVB.SEQ</ta>
            <ta e="T16" id="Seg_4821" s="T15">оставаться-PST2.[3SG]</ta>
            <ta e="T17" id="Seg_4822" s="T16">рука-3SG-INSTR</ta>
            <ta e="T18" id="Seg_4823" s="T17">бить-EP-PST2.[3SG]</ta>
            <ta e="T19" id="Seg_4824" s="T18">рука-3SG.[NOM]</ta>
            <ta e="T20" id="Seg_4825" s="T19">прилипать-CVB.SEQ</ta>
            <ta e="T21" id="Seg_4826" s="T20">оставаться-PST2.[3SG]</ta>
            <ta e="T22" id="Seg_4827" s="T21">другой</ta>
            <ta e="T23" id="Seg_4828" s="T22">рука-3SG-INSTR</ta>
            <ta e="T24" id="Seg_4829" s="T23">бить-PRS.[3SG]</ta>
            <ta e="T25" id="Seg_4830" s="T24">другой-3SG.[NOM]</ta>
            <ta e="T26" id="Seg_4831" s="T25">тоже</ta>
            <ta e="T27" id="Seg_4832" s="T26">прилипать-CVB.SEQ</ta>
            <ta e="T28" id="Seg_4833" s="T27">оставаться-PST2.[3SG]</ta>
            <ta e="T29" id="Seg_4834" s="T28">нога-3SG-INSTR</ta>
            <ta e="T30" id="Seg_4835" s="T29">пнуть-PST2.[3SG]</ta>
            <ta e="T31" id="Seg_4836" s="T30">нога-3SG.[NOM]</ta>
            <ta e="T32" id="Seg_4837" s="T31">прилипать-CVB.SEQ</ta>
            <ta e="T33" id="Seg_4838" s="T32">оставаться-PST2.[3SG]</ta>
            <ta e="T34" id="Seg_4839" s="T33">следующий</ta>
            <ta e="T35" id="Seg_4840" s="T34">нога-3SG-INSTR</ta>
            <ta e="T36" id="Seg_4841" s="T35">пнуть-PST2-3SG</ta>
            <ta e="T37" id="Seg_4842" s="T36">тоже</ta>
            <ta e="T38" id="Seg_4843" s="T37">прилипать-CVB.SEQ</ta>
            <ta e="T39" id="Seg_4844" s="T38">оставаться-PST2.[3SG]</ta>
            <ta e="T40" id="Seg_4845" s="T39">живот-3SG-INSTR</ta>
            <ta e="T41" id="Seg_4846" s="T40">толкать-PST2-3SG</ta>
            <ta e="T42" id="Seg_4847" s="T41">живот-3SG.[NOM]</ta>
            <ta e="T43" id="Seg_4848" s="T42">тоже</ta>
            <ta e="T44" id="Seg_4849" s="T43">прилипать-CVB.SEQ</ta>
            <ta e="T45" id="Seg_4850" s="T44">оставаться-PST2.[3SG]</ta>
            <ta e="T46" id="Seg_4851" s="T45">потом</ta>
            <ta e="T47" id="Seg_4852" s="T46">голова-3SG-INSTR</ta>
            <ta e="T48" id="Seg_4853" s="T47">бить-EP-PST2.[3SG]</ta>
            <ta e="T49" id="Seg_4854" s="T48">голова-3SG.[NOM]</ta>
            <ta e="T50" id="Seg_4855" s="T49">тоже</ta>
            <ta e="T51" id="Seg_4856" s="T50">прилипать-CVB.SEQ</ta>
            <ta e="T52" id="Seg_4857" s="T51">оставаться-PST2.[3SG]</ta>
            <ta e="T53" id="Seg_4858" s="T52">прилипать-CVB.SEQ</ta>
            <ta e="T54" id="Seg_4859" s="T53">стоять-INFER-3SG</ta>
            <ta e="T55" id="Seg_4860" s="T54">тот</ta>
            <ta e="T56" id="Seg_4861" s="T55">стоять-TEMP-3SG</ta>
            <ta e="T57" id="Seg_4862" s="T56">Ангаа.Монгус.[NOM]</ta>
            <ta e="T58" id="Seg_4863" s="T57">приходить-PST2.[3SG]</ta>
            <ta e="T59" id="Seg_4864" s="T58">вот.радость.то</ta>
            <ta e="T60" id="Seg_4865" s="T59">вечерний</ta>
            <ta e="T61" id="Seg_4866" s="T60">закуска-PROPR.[NOM]</ta>
            <ta e="T62" id="Seg_4867" s="T61">быть-PST1-1SG</ta>
            <ta e="T63" id="Seg_4868" s="T62">говорить-PTCP.PST-3SG-INSTR</ta>
            <ta e="T64" id="Seg_4869" s="T63">Лыыбыра-ACC</ta>
            <ta e="T65" id="Seg_4870" s="T64">взять-CVB.SEQ</ta>
            <ta e="T66" id="Seg_4871" s="T65">карман-3SG-DAT/LOC</ta>
            <ta e="T67" id="Seg_4872" s="T66">вкладывать-CVB.SEQ</ta>
            <ta e="T68" id="Seg_4873" s="T67">бросать-PST2.[3SG]</ta>
            <ta e="T69" id="Seg_4874" s="T68">дом-3SG-DAT/LOC</ta>
            <ta e="T70" id="Seg_4875" s="T69">идти-PST2.[3SG]</ta>
            <ta e="T71" id="Seg_4876" s="T70">тропка-3SG-DAT/LOC</ta>
            <ta e="T72" id="Seg_4877" s="T71">доезжать-CVB.SEQ</ta>
            <ta e="T73" id="Seg_4878" s="T72">Ангаа.Монгус.[NOM]</ta>
            <ta e="T74" id="Seg_4879" s="T73">испражняться-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T75" id="Seg_4880" s="T74">хотеть-PST2.[3SG]</ta>
            <ta e="T76" id="Seg_4881" s="T75">тот</ta>
            <ta e="T77" id="Seg_4882" s="T76">испражняться-CVB.SIM</ta>
            <ta e="T78" id="Seg_4883" s="T77">сидеть-TEMP-3SG</ta>
            <ta e="T79" id="Seg_4884" s="T78">Лыыбыра.[NOM]</ta>
            <ta e="T80" id="Seg_4885" s="T79">карман-3SG-GEN</ta>
            <ta e="T81" id="Seg_4886" s="T80">отверстие-3SG-ACC</ta>
            <ta e="T82" id="Seg_4887" s="T81">через</ta>
            <ta e="T83" id="Seg_4888" s="T82">падать-CVB.SEQ</ta>
            <ta e="T84" id="Seg_4889" s="T83">оставаться-PST2.[3SG]</ta>
            <ta e="T85" id="Seg_4890" s="T84">Ангаа.Монгус.[NOM]</ta>
            <ta e="T86" id="Seg_4891" s="T85">что-ACC</ta>
            <ta e="T87" id="Seg_4892" s="T86">NEG</ta>
            <ta e="T88" id="Seg_4893" s="T87">замечать-NEG.CVB.SIM</ta>
            <ta e="T89" id="Seg_4894" s="T88">штаны-3SG-ACC</ta>
            <ta e="T90" id="Seg_4895" s="T89">поднимать-EP-REFL-PST1-3SG</ta>
            <ta e="T91" id="Seg_4896" s="T90">и</ta>
            <ta e="T92" id="Seg_4897" s="T91">дом-3SG-DAT/LOC</ta>
            <ta e="T93" id="Seg_4898" s="T92">идти-PST2.[3SG]</ta>
            <ta e="T94" id="Seg_4899" s="T93">дом-3SG-DAT/LOC</ta>
            <ta e="T95" id="Seg_4900" s="T94">край-3SG-DAT/LOC</ta>
            <ta e="T96" id="Seg_4901" s="T95">шуба-3SG-ACC</ta>
            <ta e="T97" id="Seg_4902" s="T96">лабаз-DAT/LOC</ta>
            <ta e="T98" id="Seg_4903" s="T97">повесить-CVB.SEQ</ta>
            <ta e="T99" id="Seg_4904" s="T98">после</ta>
            <ta e="T100" id="Seg_4905" s="T99">входить-PST2.[3SG]</ta>
            <ta e="T101" id="Seg_4906" s="T100">старуха-3SG.[NOM]</ta>
            <ta e="T102" id="Seg_4907" s="T101">шить-CVB.SIM</ta>
            <ta e="T103" id="Seg_4908" s="T102">сидеть-PTCP.PRS</ta>
            <ta e="T104" id="Seg_4909" s="T103">быть-PST2.[3SG]</ta>
            <ta e="T105" id="Seg_4910" s="T104">старуха.[NOM]</ta>
            <ta e="T106" id="Seg_4911" s="T105">подарок-1SG-ACC</ta>
            <ta e="T107" id="Seg_4912" s="T106">принести-PST1-1SG</ta>
            <ta e="T108" id="Seg_4913" s="T107">шуба-EP-1SG.[NOM]</ta>
            <ta e="T109" id="Seg_4914" s="T108">карман-3SG-DAT/LOC</ta>
            <ta e="T110" id="Seg_4915" s="T109">есть</ta>
            <ta e="T111" id="Seg_4916" s="T110">на.улице</ta>
            <ta e="T112" id="Seg_4917" s="T111">тот-ACC</ta>
            <ta e="T113" id="Seg_4918" s="T112">внести-CVB.SEQ</ta>
            <ta e="T114" id="Seg_4919" s="T113">видеть.[IMP.2SG]</ta>
            <ta e="T115" id="Seg_4920" s="T114">говорить-PST2.[3SG]</ta>
            <ta e="T116" id="Seg_4921" s="T115">старуха-3SG.[NOM]</ta>
            <ta e="T117" id="Seg_4922" s="T116">радоваться-NMNZ-3SG-DAT/LOC</ta>
            <ta e="T118" id="Seg_4923" s="T117">пгла-PROPR</ta>
            <ta e="T119" id="Seg_4924" s="T118">наперсток-3SG-ACC</ta>
            <ta e="T120" id="Seg_4925" s="T119">проглотить-CVB.SEQ</ta>
            <ta e="T121" id="Seg_4926" s="T120">бросать-PST2.[3SG]</ta>
            <ta e="T122" id="Seg_4927" s="T121">выйти-CVB.SIM</ta>
            <ta e="T123" id="Seg_4928" s="T122">бежать-PST2.[3SG]</ta>
            <ta e="T124" id="Seg_4929" s="T123">найти-NEG.CVB.SIM</ta>
            <ta e="T125" id="Seg_4930" s="T124">возвращаться-CVB.SEQ</ta>
            <ta e="T126" id="Seg_4931" s="T125">входить-PST2.[3SG]</ta>
            <ta e="T127" id="Seg_4932" s="T126">карман-2SG-DAT/LOC</ta>
            <ta e="T128" id="Seg_4933" s="T127">что.[NOM]</ta>
            <ta e="T129" id="Seg_4934" s="T128">NEG</ta>
            <ta e="T130" id="Seg_4935" s="T129">NEG.EX</ta>
            <ta e="T131" id="Seg_4936" s="T130">говорить-PST2.[3SG]</ta>
            <ta e="T132" id="Seg_4937" s="T131">Ангаа.Монгус.[NOM]</ta>
            <ta e="T133" id="Seg_4938" s="T132">сам-2SG.[NOM]</ta>
            <ta e="T134" id="Seg_4939" s="T133">есть-INFER-EP-2SG</ta>
            <ta e="T135" id="Seg_4940" s="T134">EMPH</ta>
            <ta e="T136" id="Seg_4941" s="T135">говорить-PST2.[3SG]</ta>
            <ta e="T137" id="Seg_4942" s="T136">да</ta>
            <ta e="T138" id="Seg_4943" s="T137">старуха.[NOM]</ta>
            <ta e="T139" id="Seg_4944" s="T138">живот-3SG-ACC</ta>
            <ta e="T140" id="Seg_4945" s="T139">колоть-CVB.SIM</ta>
            <ta e="T141" id="Seg_4946" s="T140">тянуть-PRS.[3SG]</ta>
            <ta e="T142" id="Seg_4947" s="T141">старуха.[NOM]</ta>
            <ta e="T143" id="Seg_4948" s="T142">живот-3SG-ABL</ta>
            <ta e="T144" id="Seg_4949" s="T143">пгла-PROPR</ta>
            <ta e="T145" id="Seg_4950" s="T144">наперсток.[NOM]</ta>
            <ta e="T146" id="Seg_4951" s="T145">звяк</ta>
            <ta e="T147" id="Seg_4952" s="T146">делать-CVB.SIM</ta>
            <ta e="T148" id="Seg_4953" s="T147">падать-PST2.[3SG]</ta>
            <ta e="T149" id="Seg_4954" s="T148">о</ta>
            <ta e="T150" id="Seg_4955" s="T149">дорогой</ta>
            <ta e="T151" id="Seg_4956" s="T150">жена-1SG-ACC</ta>
            <ta e="T152" id="Seg_4957" s="T151">кричать-PST2.[3SG]</ta>
            <ta e="T153" id="Seg_4958" s="T152">Ангаа.Монгус.[NOM]</ta>
            <ta e="T154" id="Seg_4959" s="T153">назад</ta>
            <ta e="T155" id="Seg_4960" s="T154">бегать-PST2.[3SG]</ta>
            <ta e="T156" id="Seg_4961" s="T155">Лыыбыра.[NOM]</ta>
            <ta e="T157" id="Seg_4962" s="T156">тропка-DAT/LOC</ta>
            <ta e="T158" id="Seg_4963" s="T157">этот</ta>
            <ta e="T159" id="Seg_4964" s="T158">лежать.затверденно-CVB.SIM</ta>
            <ta e="T160" id="Seg_4965" s="T159">лежать-PRS.[3SG]</ta>
            <ta e="T161" id="Seg_4966" s="T160">взять-PRS.[3SG]</ta>
            <ta e="T162" id="Seg_4967" s="T161">да</ta>
            <ta e="T163" id="Seg_4968" s="T162">карман-3SG-DAT/LOC</ta>
            <ta e="T164" id="Seg_4969" s="T163">вкладывать-PRS.[3SG]</ta>
            <ta e="T165" id="Seg_4970" s="T164">назад</ta>
            <ta e="T166" id="Seg_4971" s="T165">бегать-CVB.SEQ</ta>
            <ta e="T167" id="Seg_4972" s="T166">приходить-PRS.[3SG]</ta>
            <ta e="T168" id="Seg_4973" s="T167">дом-3SG-DAT/LOC</ta>
            <ta e="T169" id="Seg_4974" s="T168">входить-CVB.SEQ</ta>
            <ta e="T170" id="Seg_4975" s="T169">Лыыбыра-ACC</ta>
            <ta e="T171" id="Seg_4976" s="T170">крюк.для.котла-DAT/LOC</ta>
            <ta e="T172" id="Seg_4977" s="T171">повесить-CVB.SEQ</ta>
            <ta e="T173" id="Seg_4978" s="T172">бросать-PRS.[3SG]</ta>
            <ta e="T174" id="Seg_4979" s="T173">Ангаа.Монгус.[NOM]</ta>
            <ta e="T175" id="Seg_4980" s="T174">семь</ta>
            <ta e="T176" id="Seg_4981" s="T175">плешивый</ta>
            <ta e="T177" id="Seg_4982" s="T176">ребенок-PROPR.[NOM]</ta>
            <ta e="T178" id="Seg_4983" s="T177">быть-PST2.[3SG]</ta>
            <ta e="T179" id="Seg_4984" s="T178">тот</ta>
            <ta e="T180" id="Seg_4985" s="T179">ребенок-PL-3SG-DAT/LOC</ta>
            <ta e="T181" id="Seg_4986" s="T180">говорить-PST2.[3SG]</ta>
            <ta e="T182" id="Seg_4987" s="T181">ребенок-PL.[NOM]</ta>
            <ta e="T183" id="Seg_4988" s="T182">Лыыбыра.[NOM]</ta>
            <ta e="T184" id="Seg_4989" s="T183">сало-3SG.[NOM]</ta>
            <ta e="T185" id="Seg_4990" s="T184">капать-FUT-3SG</ta>
            <ta e="T186" id="Seg_4991" s="T185">видеть-PTCP.PRS</ta>
            <ta e="T187" id="Seg_4992" s="T186">быть-EP-IMP.2PL</ta>
            <ta e="T188" id="Seg_4993" s="T187">приходить-PTCP.PRS-1SG-DAT/LOC</ta>
            <ta e="T189" id="Seg_4994" s="T188">варить-FUT-EP-IMP.2PL</ta>
            <ta e="T190" id="Seg_4995" s="T189">говорить-PST1-3SG</ta>
            <ta e="T191" id="Seg_4996" s="T190">да</ta>
            <ta e="T192" id="Seg_4997" s="T191">выйти-CVB.SEQ</ta>
            <ta e="T193" id="Seg_4998" s="T192">оставаться-PST2.[3SG]</ta>
            <ta e="T194" id="Seg_4999" s="T193">сколько</ta>
            <ta e="T195" id="Seg_5000" s="T194">NEG</ta>
            <ta e="T196" id="Seg_5001" s="T195">быть-NEG.CVB.SIM</ta>
            <ta e="T197" id="Seg_5002" s="T196">Лыыбыра.[NOM]</ta>
            <ta e="T198" id="Seg_5003" s="T197">вот</ta>
            <ta e="T199" id="Seg_5004" s="T198">мочиться-PST2.[3SG]</ta>
            <ta e="T200" id="Seg_5005" s="T199">ребенок-PL.[NOM]</ta>
            <ta e="T201" id="Seg_5006" s="T200">там</ta>
            <ta e="T202" id="Seg_5007" s="T201">кричать-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T203" id="Seg_5008" s="T202">вот.радость.то</ta>
            <ta e="T204" id="Seg_5009" s="T203">Лыыбыра.[NOM]</ta>
            <ta e="T205" id="Seg_5010" s="T204">жидкий</ta>
            <ta e="T206" id="Seg_5011" s="T205">сало-3SG.[NOM]</ta>
            <ta e="T207" id="Seg_5012" s="T206">выливать-EP-REFL-PST1-3SG</ta>
            <ta e="T208" id="Seg_5013" s="T207">потом</ta>
            <ta e="T209" id="Seg_5014" s="T208">Лыыбыра.[NOM]</ta>
            <ta e="T210" id="Seg_5015" s="T209">вот</ta>
            <ta e="T211" id="Seg_5016" s="T210">испражняться-PRS.[3SG]</ta>
            <ta e="T212" id="Seg_5017" s="T211">ребенок-PL.[NOM]</ta>
            <ta e="T213" id="Seg_5018" s="T212">кричать-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T214" id="Seg_5019" s="T213">вот.радость.то</ta>
            <ta e="T215" id="Seg_5020" s="T214">Лыыбыра.[NOM]</ta>
            <ta e="T216" id="Seg_5021" s="T215">густой</ta>
            <ta e="T217" id="Seg_5022" s="T216">сало-3SG.[NOM]</ta>
            <ta e="T218" id="Seg_5023" s="T217">выливать-EP-REFL-PST1-3SG</ta>
            <ta e="T219" id="Seg_5024" s="T218">Лыыбыра.[NOM]</ta>
            <ta e="T220" id="Seg_5025" s="T219">ребенок-PL-DAT/LOC</ta>
            <ta e="T221" id="Seg_5026" s="T220">говорить-PST2.[3SG]</ta>
            <ta e="T222" id="Seg_5027" s="T221">ребенок-PL.[NOM]</ta>
            <ta e="T223" id="Seg_5028" s="T222">спустить-EP-IMP.2PL</ta>
            <ta e="T224" id="Seg_5029" s="T223">1SG-ACC</ta>
            <ta e="T225" id="Seg_5030" s="T224">1SG.[NOM]</ta>
            <ta e="T226" id="Seg_5031" s="T225">2PL-DAT/LOC</ta>
            <ta e="T227" id="Seg_5032" s="T226">ложка.[NOM]</ta>
            <ta e="T228" id="Seg_5033" s="T227">делать-CVB.SEQ</ta>
            <ta e="T229" id="Seg_5034" s="T228">давать-FUT-1SG</ta>
            <ta e="T230" id="Seg_5035" s="T229">1SG.[NOM]</ta>
            <ta e="T231" id="Seg_5036" s="T230">сало-1SG-ACC</ta>
            <ta e="T232" id="Seg_5037" s="T231">есть-PTCP.PRS-2PL-DAT/LOC</ta>
            <ta e="T233" id="Seg_5038" s="T232">хороший.[NOM]</ta>
            <ta e="T234" id="Seg_5039" s="T233">быть-FUT.[3SG]</ta>
            <ta e="T235" id="Seg_5040" s="T234">эй</ta>
            <ta e="T236" id="Seg_5041" s="T235">EMPH</ta>
            <ta e="T237" id="Seg_5042" s="T236">эй</ta>
            <ta e="T238" id="Seg_5043" s="T237">Лыыбыра-ACC</ta>
            <ta e="T239" id="Seg_5044" s="T238">спустить-PST2-3PL</ta>
            <ta e="T240" id="Seg_5045" s="T239">Лыыбыра.[NOM]</ta>
            <ta e="T241" id="Seg_5046" s="T240">говорить-PST2.[3SG]</ta>
            <ta e="T242" id="Seg_5047" s="T241">принести-EP-IMP.2PL</ta>
            <ta e="T243" id="Seg_5048" s="T242">отец-2PL.[NOM]</ta>
            <ta e="T244" id="Seg_5049" s="T243">острый</ta>
            <ta e="T245" id="Seg_5050" s="T244">топор-3SG-ACC</ta>
            <ta e="T246" id="Seg_5051" s="T245">принести-CVB.SEQ</ta>
            <ta e="T247" id="Seg_5052" s="T246">давать-PST2-3PL</ta>
            <ta e="T248" id="Seg_5053" s="T247">постель-2PL-DAT/LOC</ta>
            <ta e="T249" id="Seg_5054" s="T248">видеть-CVB.SIM</ta>
            <ta e="T250" id="Seg_5055" s="T249">ложиться-EP-IMP.2PL</ta>
            <ta e="T251" id="Seg_5056" s="T250">как</ta>
            <ta e="T252" id="Seg_5057" s="T251">1SG.[NOM]</ta>
            <ta e="T253" id="Seg_5058" s="T252">делать-PRS-1SG</ta>
            <ta e="T254" id="Seg_5059" s="T253">ребенок-PL.[NOM]</ta>
            <ta e="T255" id="Seg_5060" s="T254">ложиться-CVB.SEQ</ta>
            <ta e="T256" id="Seg_5061" s="T255">оставаться-PST2-3PL</ta>
            <ta e="T257" id="Seg_5062" s="T256">тот</ta>
            <ta e="T258" id="Seg_5063" s="T257">ложиться-PTCP.PST-3PL-DAT/LOC</ta>
            <ta e="T259" id="Seg_5064" s="T258">Лыыбыра.[NOM]</ta>
            <ta e="T260" id="Seg_5065" s="T259">голова-3PL-ACC</ta>
            <ta e="T261" id="Seg_5066" s="T260">резать-CVB.SIM</ta>
            <ta e="T262" id="Seg_5067" s="T261">разрезать-FREQ-PST2.[3SG]</ta>
            <ta e="T263" id="Seg_5068" s="T262">мясо-3PL-ACC</ta>
            <ta e="T264" id="Seg_5069" s="T263">варить-CVB.SEQ</ta>
            <ta e="T265" id="Seg_5070" s="T264">бросать-PST2.[3SG]</ta>
            <ta e="T266" id="Seg_5071" s="T265">голова-PL-ACC</ta>
            <ta e="T267" id="Seg_5072" s="T266">постель-DAT/LOC</ta>
            <ta e="T268" id="Seg_5073" s="T267">в.ряду-INTNS</ta>
            <ta e="T269" id="Seg_5074" s="T268">класть-CVB.SEQ</ta>
            <ta e="T270" id="Seg_5075" s="T269">после</ta>
            <ta e="T271" id="Seg_5076" s="T270">одеяло-3SG-INSTR</ta>
            <ta e="T272" id="Seg_5077" s="T271">покрывать-FREQ-PST2.[3SG]</ta>
            <ta e="T273" id="Seg_5078" s="T272">кочерга-ACC</ta>
            <ta e="T274" id="Seg_5079" s="T273">взять-CVB.SEQ</ta>
            <ta e="T275" id="Seg_5080" s="T274">после</ta>
            <ta e="T276" id="Seg_5081" s="T275">очаг.[NOM]</ta>
            <ta e="T277" id="Seg_5082" s="T276">нижняя.часть-3SG-DAT/LOC</ta>
            <ta e="T278" id="Seg_5083" s="T277">входить-CVB.SEQ</ta>
            <ta e="T279" id="Seg_5084" s="T278">оставаться-PST2.[3SG]</ta>
            <ta e="T280" id="Seg_5085" s="T279">этот</ta>
            <ta e="T281" id="Seg_5086" s="T280">лежать-TEMP-3SG</ta>
            <ta e="T282" id="Seg_5087" s="T281">Ангаа.Монгус.[NOM]</ta>
            <ta e="T283" id="Seg_5088" s="T282">приходить-PST2.[3SG]</ta>
            <ta e="T284" id="Seg_5089" s="T283">Ангаа.Монгус.[NOM]</ta>
            <ta e="T285" id="Seg_5090" s="T284">приходить-CVB.SEQ</ta>
            <ta e="T286" id="Seg_5091" s="T285">кричать-PST2.[3SG]</ta>
            <ta e="T287" id="Seg_5092" s="T286">ребенок-PL.[NOM]</ta>
            <ta e="T288" id="Seg_5093" s="T287">дверь-2PL-ACC</ta>
            <ta e="T289" id="Seg_5094" s="T288">открывать-EP-IMP.2PL</ta>
            <ta e="T290" id="Seg_5095" s="T289">кто.[NOM]</ta>
            <ta e="T291" id="Seg_5096" s="T290">NEG</ta>
            <ta e="T292" id="Seg_5097" s="T291">открывать-NEG.[3SG]</ta>
            <ta e="T293" id="Seg_5098" s="T292">сколько-MLTP</ta>
            <ta e="T294" id="Seg_5099" s="T293">INDEF</ta>
            <ta e="T295" id="Seg_5100" s="T294">кричать-PST2.[3SG]</ta>
            <ta e="T296" id="Seg_5101" s="T295">ребенок-PL.[NOM]</ta>
            <ta e="T297" id="Seg_5102" s="T296">совсем</ta>
            <ta e="T298" id="Seg_5103" s="T297">открывать-PST2.NEG-3PL</ta>
            <ta e="T299" id="Seg_5104" s="T298">дверь-3SG-ACC</ta>
            <ta e="T300" id="Seg_5105" s="T299">вынимать-CVB.SIM</ta>
            <ta e="T301" id="Seg_5106" s="T300">тянуть-CVB.SEQ</ta>
            <ta e="T302" id="Seg_5107" s="T301">входить-PST2.[3SG]</ta>
            <ta e="T303" id="Seg_5108" s="T302">видеть-PST2-3SG</ta>
            <ta e="T304" id="Seg_5109" s="T303">мясо.[NOM]</ta>
            <ta e="T305" id="Seg_5110" s="T304">вариться-CVB.SIM</ta>
            <ta e="T306" id="Seg_5111" s="T305">стоять-PRS.[3SG]</ta>
            <ta e="T307" id="Seg_5112" s="T306">ребенок-PL.[NOM]</ta>
            <ta e="T308" id="Seg_5113" s="T307">спать-CVB.SIM</ta>
            <ta e="T309" id="Seg_5114" s="T308">лежать-PRS-3PL</ta>
            <ta e="T310" id="Seg_5115" s="T309">о</ta>
            <ta e="T311" id="Seg_5116" s="T310">ребенок-PL-EP-1SG.[NOM]</ta>
            <ta e="T312" id="Seg_5117" s="T311">Лыыбыра.[NOM]</ta>
            <ta e="T313" id="Seg_5118" s="T312">сало-3SG-ABL</ta>
            <ta e="T314" id="Seg_5119" s="T313">наесться-CVB.SEQ</ta>
            <ta e="T315" id="Seg_5120" s="T314">после</ta>
            <ta e="T316" id="Seg_5121" s="T315">спать-CVB.SIM</ta>
            <ta e="T317" id="Seg_5122" s="T316">лежать-PRS-3PL</ta>
            <ta e="T318" id="Seg_5123" s="T317">быть-PST2.[3SG]</ta>
            <ta e="T319" id="Seg_5124" s="T318">EMPH</ta>
            <ta e="T320" id="Seg_5125" s="T319">говорить-PRS.[3SG]</ta>
            <ta e="T321" id="Seg_5126" s="T320">котел-ACC</ta>
            <ta e="T322" id="Seg_5127" s="T321">вынимать-PRS.[3SG]</ta>
            <ta e="T323" id="Seg_5128" s="T322">сердце-3SG-ABL</ta>
            <ta e="T324" id="Seg_5129" s="T323">пробовать-PRS.[3SG]</ta>
            <ta e="T325" id="Seg_5130" s="T324">сердце-3SG.[NOM]</ta>
            <ta e="T326" id="Seg_5131" s="T325">тянуть-PST2.[3SG]</ta>
            <ta e="T327" id="Seg_5132" s="T326">о</ta>
            <ta e="T328" id="Seg_5133" s="T327">Лыыбыра.[NOM]</ta>
            <ta e="T329" id="Seg_5134" s="T328">1SG.[NOM]</ta>
            <ta e="T330" id="Seg_5135" s="T329">родственник-1SG.[NOM]</ta>
            <ta e="T331" id="Seg_5136" s="T330">быть-PST2.[3SG]</ta>
            <ta e="T332" id="Seg_5137" s="T331">Q</ta>
            <ta e="T333" id="Seg_5138" s="T332">говорить-PST2.[3SG]</ta>
            <ta e="T334" id="Seg_5139" s="T333">селезенка.[NOM]</ta>
            <ta e="T335" id="Seg_5140" s="T334">есть-PST2.[3SG]</ta>
            <ta e="T336" id="Seg_5141" s="T335">селезенка-3SG.[NOM]</ta>
            <ta e="T337" id="Seg_5142" s="T336">тянуть-PST2.[3SG]</ta>
            <ta e="T338" id="Seg_5143" s="T337">о</ta>
            <ta e="T339" id="Seg_5144" s="T338">Лыыбыра.[NOM]</ta>
            <ta e="T340" id="Seg_5145" s="T339">настоящий</ta>
            <ta e="T341" id="Seg_5146" s="T340">родственник-1SG.[NOM]</ta>
            <ta e="T342" id="Seg_5147" s="T341">быть-PST2.[3SG]</ta>
            <ta e="T343" id="Seg_5148" s="T342">Q</ta>
            <ta e="T344" id="Seg_5149" s="T343">говорить-PST2.[3SG]</ta>
            <ta e="T345" id="Seg_5150" s="T344">потом</ta>
            <ta e="T346" id="Seg_5151" s="T345">вставать-PST2.[3SG]</ta>
            <ta e="T347" id="Seg_5152" s="T346">ребенок-PL-3SG-ACC</ta>
            <ta e="T348" id="Seg_5153" s="T347">открывать-CVB.SIM</ta>
            <ta e="T349" id="Seg_5154" s="T348">тянуть-PST2-3SG</ta>
            <ta e="T350" id="Seg_5155" s="T349">семь</ta>
            <ta e="T351" id="Seg_5156" s="T350">голова.[NOM]</ta>
            <ta e="T352" id="Seg_5157" s="T351">кататься-NMNZ.[NOM]</ta>
            <ta e="T353" id="Seg_5158" s="T352">делать-PST2-3PL</ta>
            <ta e="T354" id="Seg_5159" s="T353">испугаться-CVB.SIM</ta>
            <ta e="T355" id="Seg_5160" s="T354">с</ta>
            <ta e="T356" id="Seg_5161" s="T355">Ангаа.Монгус.[NOM]</ta>
            <ta e="T357" id="Seg_5162" s="T356">кричать-PST2.[3SG]</ta>
            <ta e="T358" id="Seg_5163" s="T357">Лыыбыра.[NOM]</ta>
            <ta e="T359" id="Seg_5164" s="T358">где</ta>
            <ta e="T360" id="Seg_5165" s="T359">есть-2SG=Q</ta>
            <ta e="T361" id="Seg_5166" s="T360">Лыыбыра.[NOM]</ta>
            <ta e="T362" id="Seg_5167" s="T361">на.улице</ta>
            <ta e="T363" id="Seg_5168" s="T362">есть-1SG</ta>
            <ta e="T364" id="Seg_5169" s="T363">говорить-PST2.[3SG]</ta>
            <ta e="T365" id="Seg_5170" s="T364">тот-ACC</ta>
            <ta e="T366" id="Seg_5171" s="T365">слышать-PST1-3SG</ta>
            <ta e="T367" id="Seg_5172" s="T366">да</ta>
            <ta e="T368" id="Seg_5173" s="T367">дом-3SG-ACC</ta>
            <ta e="T369" id="Seg_5174" s="T368">колоть-CVB.SIM</ta>
            <ta e="T370" id="Seg_5175" s="T369">летать-CVB.SEQ</ta>
            <ta e="T371" id="Seg_5176" s="T370">выйти-PRS.[3SG]</ta>
            <ta e="T372" id="Seg_5177" s="T371">Лыыбыра.[NOM]</ta>
            <ta e="T373" id="Seg_5178" s="T372">где</ta>
            <ta e="T374" id="Seg_5179" s="T373">NEG</ta>
            <ta e="T375" id="Seg_5180" s="T374">NEG.EX</ta>
            <ta e="T376" id="Seg_5181" s="T375">Лыыбыра</ta>
            <ta e="T377" id="Seg_5182" s="T376">где-2SG=Q</ta>
            <ta e="T378" id="Seg_5183" s="T377">дом.[NOM]</ta>
            <ta e="T379" id="Seg_5184" s="T378">нутро-3SG-DAT/LOC</ta>
            <ta e="T380" id="Seg_5185" s="T379">есть-1SG</ta>
            <ta e="T381" id="Seg_5186" s="T380">говорить-PST2.[3SG]</ta>
            <ta e="T382" id="Seg_5187" s="T381">Лыыбыра.[NOM]</ta>
            <ta e="T383" id="Seg_5188" s="T382">дом-DAT/LOC</ta>
            <ta e="T384" id="Seg_5189" s="T383">летать-CVB.SEQ</ta>
            <ta e="T385" id="Seg_5190" s="T384">падать-PST2.[3SG]</ta>
            <ta e="T386" id="Seg_5191" s="T385">Лыыбыра.[NOM]</ta>
            <ta e="T387" id="Seg_5192" s="T386">NEG.EX</ta>
            <ta e="T388" id="Seg_5193" s="T387">Лыыбыра</ta>
            <ta e="T389" id="Seg_5194" s="T388">где-2SG=Q</ta>
            <ta e="T390" id="Seg_5195" s="T389">дымоход-DAT/LOC</ta>
            <ta e="T391" id="Seg_5196" s="T390">есть-1SG</ta>
            <ta e="T392" id="Seg_5197" s="T391">дымоход-DAT/LOC</ta>
            <ta e="T393" id="Seg_5198" s="T392">заглянуть-PST2-3SG</ta>
            <ta e="T394" id="Seg_5199" s="T393">Лыыбыра.[NOM]</ta>
            <ta e="T395" id="Seg_5200" s="T394">где</ta>
            <ta e="T396" id="Seg_5201" s="T395">NEG</ta>
            <ta e="T397" id="Seg_5202" s="T396">NEG.EX</ta>
            <ta e="T398" id="Seg_5203" s="T397">Лыыбыра.[NOM]</ta>
            <ta e="T399" id="Seg_5204" s="T398">где</ta>
            <ta e="T400" id="Seg_5205" s="T399">есть-2SG=Q</ta>
            <ta e="T401" id="Seg_5206" s="T400">говорить-PST2.[3SG]</ta>
            <ta e="T402" id="Seg_5207" s="T401">Лыыбыра.[NOM]</ta>
            <ta e="T403" id="Seg_5208" s="T402">очаг.[NOM]</ta>
            <ta e="T404" id="Seg_5209" s="T403">нижняя.часть-3SG-DAT/LOC</ta>
            <ta e="T405" id="Seg_5210" s="T404">есть-1SG</ta>
            <ta e="T406" id="Seg_5211" s="T405">говорить-PST2.[3SG]</ta>
            <ta e="T407" id="Seg_5212" s="T406">тот-ACC</ta>
            <ta e="T408" id="Seg_5213" s="T407">Ангаа.Монгус.[NOM]</ta>
            <ta e="T409" id="Seg_5214" s="T408">слышать-PST1-3SG</ta>
            <ta e="T410" id="Seg_5215" s="T409">да</ta>
            <ta e="T411" id="Seg_5216" s="T410">голова-3SG-INSTR</ta>
            <ta e="T412" id="Seg_5217" s="T411">входить-CVB.PURP</ta>
            <ta e="T413" id="Seg_5218" s="T412">делать-PST2.[3SG]</ta>
            <ta e="T414" id="Seg_5219" s="T413">тот-DAT/LOC</ta>
            <ta e="T415" id="Seg_5220" s="T414">Лыыбыра.[NOM]</ta>
            <ta e="T416" id="Seg_5221" s="T415">говорить-PRS.[3SG]</ta>
            <ta e="T417" id="Seg_5222" s="T416">дедушка.[NOM]</ta>
            <ta e="T418" id="Seg_5223" s="T417">голова-2SG-INSTR</ta>
            <ta e="T419" id="Seg_5224" s="T418">входить-EP-NEG.[IMP.2SG]</ta>
            <ta e="T420" id="Seg_5225" s="T419">подходить-FUT-2SG</ta>
            <ta e="T421" id="Seg_5226" s="T420">NEG-3SG</ta>
            <ta e="T422" id="Seg_5227" s="T421">задница-2SG-INSTR</ta>
            <ta e="T423" id="Seg_5228" s="T422">входить.[IMP.2SG]</ta>
            <ta e="T424" id="Seg_5229" s="T423">Ангаа.Монгус.[NOM]</ta>
            <ta e="T425" id="Seg_5230" s="T424">задница-3SG-INSTR</ta>
            <ta e="T426" id="Seg_5231" s="T425">входить-PST2.[3SG]</ta>
            <ta e="T427" id="Seg_5232" s="T426">тот</ta>
            <ta e="T428" id="Seg_5233" s="T427">входить-CVB.SEQ</ta>
            <ta e="T429" id="Seg_5234" s="T428">быть-TEMP-3SG</ta>
            <ta e="T430" id="Seg_5235" s="T429">Лыыбыра.[NOM]</ta>
            <ta e="T431" id="Seg_5236" s="T430">краснеть-INCH</ta>
            <ta e="T432" id="Seg_5237" s="T431">идти-PTCP.PST</ta>
            <ta e="T433" id="Seg_5238" s="T432">кочерга-3SG-INSTR</ta>
            <ta e="T434" id="Seg_5239" s="T433">задница-3SG-GEN</ta>
            <ta e="T435" id="Seg_5240" s="T434">нутро-3SG-INSTR</ta>
            <ta e="T436" id="Seg_5241" s="T435">толкать-PST2.[3SG]</ta>
            <ta e="T437" id="Seg_5242" s="T436">Ангаа.Монгус.[NOM]</ta>
            <ta e="T438" id="Seg_5243" s="T437">умирать-CVB.SEQ</ta>
            <ta e="T439" id="Seg_5244" s="T438">оставаться-PST2.[3SG]</ta>
            <ta e="T440" id="Seg_5245" s="T439">Лыыбыра.[NOM]</ta>
            <ta e="T441" id="Seg_5246" s="T440">выйти-PST1-3SG</ta>
            <ta e="T442" id="Seg_5247" s="T441">да</ta>
            <ta e="T443" id="Seg_5248" s="T442">дом-3SG-DAT/LOC</ta>
            <ta e="T444" id="Seg_5249" s="T443">идти-CVB.SEQ</ta>
            <ta e="T445" id="Seg_5250" s="T444">оставаться-PST2.[3SG]</ta>
            <ta e="T446" id="Seg_5251" s="T445">так</ta>
            <ta e="T447" id="Seg_5252" s="T446">быть.богатым-CVB.SEQ-быть.богатым-CVB.SEQ</ta>
            <ta e="T448" id="Seg_5253" s="T447">жить-PST2.[3SG]</ta>
            <ta e="T449" id="Seg_5254" s="T448">вот</ta>
            <ta e="T450" id="Seg_5255" s="T449">кончать-PST1-3SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_5256" s="T0">propr-n:case</ta>
            <ta e="T2" id="Seg_5257" s="T1">v-v:cvb</ta>
            <ta e="T3" id="Seg_5258" s="T2">v-v:cvb</ta>
            <ta e="T4" id="Seg_5259" s="T3">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T5" id="Seg_5260" s="T4">conj</ta>
            <ta e="T6" id="Seg_5261" s="T5">v-v:cvb-v-v:cvb</ta>
            <ta e="T7" id="Seg_5262" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_5263" s="T7">v-v:tense-v:pred.pn</ta>
            <ta e="T9" id="Seg_5264" s="T8">n-n:(poss)-n:case</ta>
            <ta e="T10" id="Seg_5265" s="T9">v-v:cvb</ta>
            <ta e="T11" id="Seg_5266" s="T10">v-v:tense-v:pred.pn</ta>
            <ta e="T12" id="Seg_5267" s="T11">n-n:poss-n:case</ta>
            <ta e="T13" id="Seg_5268" s="T12">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T14" id="Seg_5269" s="T13">n-n:(poss)-n:case</ta>
            <ta e="T15" id="Seg_5270" s="T14">v-v:cvb</ta>
            <ta e="T16" id="Seg_5271" s="T15">v-v:tense-v:pred.pn</ta>
            <ta e="T17" id="Seg_5272" s="T16">n-n:poss-n:case</ta>
            <ta e="T18" id="Seg_5273" s="T17">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T19" id="Seg_5274" s="T18">n-n:(poss)-n:case</ta>
            <ta e="T20" id="Seg_5275" s="T19">v-v:cvb</ta>
            <ta e="T21" id="Seg_5276" s="T20">v-v:tense-v:pred.pn</ta>
            <ta e="T22" id="Seg_5277" s="T21">adj</ta>
            <ta e="T23" id="Seg_5278" s="T22">n-n:poss-n:case</ta>
            <ta e="T24" id="Seg_5279" s="T23">v-v:tense-v:pred.pn</ta>
            <ta e="T25" id="Seg_5280" s="T24">adj-n:(poss)-n:case</ta>
            <ta e="T26" id="Seg_5281" s="T25">ptcl</ta>
            <ta e="T27" id="Seg_5282" s="T26">v-v:cvb</ta>
            <ta e="T28" id="Seg_5283" s="T27">v-v:tense-v:pred.pn</ta>
            <ta e="T29" id="Seg_5284" s="T28">n-n:poss-n:case</ta>
            <ta e="T30" id="Seg_5285" s="T29">v-v:tense-v:pred.pn</ta>
            <ta e="T31" id="Seg_5286" s="T30">n-n:(poss)-n:case</ta>
            <ta e="T32" id="Seg_5287" s="T31">v-v:cvb</ta>
            <ta e="T33" id="Seg_5288" s="T32">v-v:tense-v:pred.pn</ta>
            <ta e="T34" id="Seg_5289" s="T33">adj</ta>
            <ta e="T35" id="Seg_5290" s="T34">n-n:poss-n:case</ta>
            <ta e="T36" id="Seg_5291" s="T35">v-v:tense-v:poss.pn</ta>
            <ta e="T37" id="Seg_5292" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_5293" s="T37">v-v:cvb</ta>
            <ta e="T39" id="Seg_5294" s="T38">v-v:tense-v:pred.pn</ta>
            <ta e="T40" id="Seg_5295" s="T39">n-n:poss-n:case</ta>
            <ta e="T41" id="Seg_5296" s="T40">v-v:tense-v:poss.pn</ta>
            <ta e="T42" id="Seg_5297" s="T41">n-n:(poss)-n:case</ta>
            <ta e="T43" id="Seg_5298" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_5299" s="T43">v-v:cvb</ta>
            <ta e="T45" id="Seg_5300" s="T44">v-v:tense-v:pred.pn</ta>
            <ta e="T46" id="Seg_5301" s="T45">adv</ta>
            <ta e="T47" id="Seg_5302" s="T46">n-n:poss-n:case</ta>
            <ta e="T48" id="Seg_5303" s="T47">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T49" id="Seg_5304" s="T48">n-n:(poss)-n:case</ta>
            <ta e="T50" id="Seg_5305" s="T49">ptcl</ta>
            <ta e="T51" id="Seg_5306" s="T50">v-v:cvb</ta>
            <ta e="T52" id="Seg_5307" s="T51">v-v:tense-v:pred.pn</ta>
            <ta e="T53" id="Seg_5308" s="T52">v-v:cvb</ta>
            <ta e="T54" id="Seg_5309" s="T53">v-v:mood-v:poss.pn</ta>
            <ta e="T55" id="Seg_5310" s="T54">dempro</ta>
            <ta e="T56" id="Seg_5311" s="T55">v-v:mood-v:pred.pn</ta>
            <ta e="T57" id="Seg_5312" s="T56">propr-n:case</ta>
            <ta e="T58" id="Seg_5313" s="T57">v-v:tense-v:pred.pn</ta>
            <ta e="T59" id="Seg_5314" s="T58">interj</ta>
            <ta e="T60" id="Seg_5315" s="T59">adj</ta>
            <ta e="T61" id="Seg_5316" s="T60">n-n&gt;adj-n:case</ta>
            <ta e="T62" id="Seg_5317" s="T61">v-v:tense-v:poss.pn</ta>
            <ta e="T63" id="Seg_5318" s="T62">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T64" id="Seg_5319" s="T63">propr-n:case</ta>
            <ta e="T65" id="Seg_5320" s="T64">v-v:cvb</ta>
            <ta e="T66" id="Seg_5321" s="T65">n-n:poss-n:case</ta>
            <ta e="T67" id="Seg_5322" s="T66">v-v:cvb</ta>
            <ta e="T68" id="Seg_5323" s="T67">v-v:tense-v:pred.pn</ta>
            <ta e="T69" id="Seg_5324" s="T68">n-n:poss-n:case</ta>
            <ta e="T70" id="Seg_5325" s="T69">v-v:tense-v:pred.pn</ta>
            <ta e="T71" id="Seg_5326" s="T70">n-n:poss-n:case</ta>
            <ta e="T72" id="Seg_5327" s="T71">v-v:cvb</ta>
            <ta e="T73" id="Seg_5328" s="T72">propr-n:case</ta>
            <ta e="T74" id="Seg_5329" s="T73">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T75" id="Seg_5330" s="T74">v-v:tense-v:pred.pn</ta>
            <ta e="T76" id="Seg_5331" s="T75">dempro</ta>
            <ta e="T77" id="Seg_5332" s="T76">v-v:cvb</ta>
            <ta e="T78" id="Seg_5333" s="T77">v-v:mood-v:temp.pn</ta>
            <ta e="T79" id="Seg_5334" s="T78">propr-n:case</ta>
            <ta e="T80" id="Seg_5335" s="T79">n-n:poss-n:case</ta>
            <ta e="T81" id="Seg_5336" s="T80">n-n:poss-n:case</ta>
            <ta e="T82" id="Seg_5337" s="T81">post</ta>
            <ta e="T83" id="Seg_5338" s="T82">v-v:cvb</ta>
            <ta e="T84" id="Seg_5339" s="T83">v-v:tense-v:pred.pn</ta>
            <ta e="T85" id="Seg_5340" s="T84">propr-n:case</ta>
            <ta e="T86" id="Seg_5341" s="T85">que-pro:case</ta>
            <ta e="T87" id="Seg_5342" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_5343" s="T87">v-v:cvb</ta>
            <ta e="T89" id="Seg_5344" s="T88">n-n:poss-n:case</ta>
            <ta e="T90" id="Seg_5345" s="T89">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T91" id="Seg_5346" s="T90">conj</ta>
            <ta e="T92" id="Seg_5347" s="T91">n-n:poss-n:case</ta>
            <ta e="T93" id="Seg_5348" s="T92">v-v:tense-v:pred.pn</ta>
            <ta e="T94" id="Seg_5349" s="T93">n-n:poss-n:case</ta>
            <ta e="T95" id="Seg_5350" s="T94">n-n:poss-n:case</ta>
            <ta e="T96" id="Seg_5351" s="T95">n-n:poss-n:case</ta>
            <ta e="T97" id="Seg_5352" s="T96">n-n:case</ta>
            <ta e="T98" id="Seg_5353" s="T97">v-v:cvb</ta>
            <ta e="T99" id="Seg_5354" s="T98">post</ta>
            <ta e="T100" id="Seg_5355" s="T99">v-v:tense-v:pred.pn</ta>
            <ta e="T101" id="Seg_5356" s="T100">n-n:(poss)-n:case</ta>
            <ta e="T102" id="Seg_5357" s="T101">v-v:cvb</ta>
            <ta e="T103" id="Seg_5358" s="T102">v-v:ptcp</ta>
            <ta e="T104" id="Seg_5359" s="T103">v-v:tense-v:pred.pn</ta>
            <ta e="T105" id="Seg_5360" s="T104">n-n:case</ta>
            <ta e="T106" id="Seg_5361" s="T105">n-n:poss-n:case</ta>
            <ta e="T107" id="Seg_5362" s="T106">v-v:tense-v:poss.pn</ta>
            <ta e="T108" id="Seg_5363" s="T107">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T109" id="Seg_5364" s="T108">n-n:poss-n:case</ta>
            <ta e="T110" id="Seg_5365" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_5366" s="T110">adv</ta>
            <ta e="T112" id="Seg_5367" s="T111">dempro-pro:case</ta>
            <ta e="T113" id="Seg_5368" s="T112">v-v:cvb</ta>
            <ta e="T114" id="Seg_5369" s="T113">v-v:mood.pn</ta>
            <ta e="T115" id="Seg_5370" s="T114">v-v:tense-v:pred.pn</ta>
            <ta e="T116" id="Seg_5371" s="T115">n-n:(poss)-n:case</ta>
            <ta e="T117" id="Seg_5372" s="T116">v-v&gt;n-n:poss-n:case</ta>
            <ta e="T118" id="Seg_5373" s="T117">n-n&gt;adj</ta>
            <ta e="T119" id="Seg_5374" s="T118">n-n:poss-n:case</ta>
            <ta e="T120" id="Seg_5375" s="T119">v-v:cvb</ta>
            <ta e="T121" id="Seg_5376" s="T120">v-v:tense-v:pred.pn</ta>
            <ta e="T122" id="Seg_5377" s="T121">v-v:cvb</ta>
            <ta e="T123" id="Seg_5378" s="T122">v-v:tense-v:pred.pn</ta>
            <ta e="T124" id="Seg_5379" s="T123">v-v:cvb</ta>
            <ta e="T125" id="Seg_5380" s="T124">v-v:cvb</ta>
            <ta e="T126" id="Seg_5381" s="T125">v-v:tense-v:pred.pn</ta>
            <ta e="T127" id="Seg_5382" s="T126">n-n:poss-n:case</ta>
            <ta e="T128" id="Seg_5383" s="T127">que-pro:case</ta>
            <ta e="T129" id="Seg_5384" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_5385" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_5386" s="T130">v-v:tense-v:pred.pn</ta>
            <ta e="T132" id="Seg_5387" s="T131">propr-n:case</ta>
            <ta e="T133" id="Seg_5388" s="T132">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T134" id="Seg_5389" s="T133">v-v:mood-v:(ins)-v:poss.pn</ta>
            <ta e="T135" id="Seg_5390" s="T134">ptcl</ta>
            <ta e="T136" id="Seg_5391" s="T135">v-v:tense-v:pred.pn</ta>
            <ta e="T137" id="Seg_5392" s="T136">conj</ta>
            <ta e="T138" id="Seg_5393" s="T137">n-n:case</ta>
            <ta e="T139" id="Seg_5394" s="T138">n-n:poss-n:case</ta>
            <ta e="T140" id="Seg_5395" s="T139">v-v:cvb</ta>
            <ta e="T141" id="Seg_5396" s="T140">v-v:tense-v:pred.pn</ta>
            <ta e="T142" id="Seg_5397" s="T141">n-n:case</ta>
            <ta e="T143" id="Seg_5398" s="T142">n-n:poss-n:case</ta>
            <ta e="T144" id="Seg_5399" s="T143">n-n&gt;adj</ta>
            <ta e="T145" id="Seg_5400" s="T144">n-n:case</ta>
            <ta e="T146" id="Seg_5401" s="T145">interj</ta>
            <ta e="T147" id="Seg_5402" s="T146">v-v:cvb</ta>
            <ta e="T148" id="Seg_5403" s="T147">v-v:tense-v:pred.pn</ta>
            <ta e="T149" id="Seg_5404" s="T148">interj</ta>
            <ta e="T150" id="Seg_5405" s="T149">adj</ta>
            <ta e="T151" id="Seg_5406" s="T150">n-n:poss-n:case</ta>
            <ta e="T152" id="Seg_5407" s="T151">v-v:tense-v:pred.pn</ta>
            <ta e="T153" id="Seg_5408" s="T152">propr-n:case</ta>
            <ta e="T154" id="Seg_5409" s="T153">adv</ta>
            <ta e="T155" id="Seg_5410" s="T154">v-v:tense-v:pred.pn</ta>
            <ta e="T156" id="Seg_5411" s="T155">propr-n:case</ta>
            <ta e="T157" id="Seg_5412" s="T156">n-n:case</ta>
            <ta e="T158" id="Seg_5413" s="T157">dempro</ta>
            <ta e="T159" id="Seg_5414" s="T158">v-v:cvb</ta>
            <ta e="T160" id="Seg_5415" s="T159">v-v:tense-v:pred.pn</ta>
            <ta e="T161" id="Seg_5416" s="T160">v-v:tense-v:pred.pn</ta>
            <ta e="T162" id="Seg_5417" s="T161">conj</ta>
            <ta e="T163" id="Seg_5418" s="T162">n-n:poss-n:case</ta>
            <ta e="T164" id="Seg_5419" s="T163">v-v:tense-v:pred.pn</ta>
            <ta e="T165" id="Seg_5420" s="T164">adv</ta>
            <ta e="T166" id="Seg_5421" s="T165">v-v:cvb</ta>
            <ta e="T167" id="Seg_5422" s="T166">v-v:tense-v:pred.pn</ta>
            <ta e="T168" id="Seg_5423" s="T167">n-n:poss-n:case</ta>
            <ta e="T169" id="Seg_5424" s="T168">v-v:cvb</ta>
            <ta e="T170" id="Seg_5425" s="T169">propr-n:case</ta>
            <ta e="T171" id="Seg_5426" s="T170">n-n:case</ta>
            <ta e="T172" id="Seg_5427" s="T171">v-v:cvb</ta>
            <ta e="T173" id="Seg_5428" s="T172">v-v:tense-v:pred.pn</ta>
            <ta e="T174" id="Seg_5429" s="T173">propr-n:case</ta>
            <ta e="T175" id="Seg_5430" s="T174">cardnum</ta>
            <ta e="T176" id="Seg_5431" s="T175">adj</ta>
            <ta e="T177" id="Seg_5432" s="T176">n-n&gt;adj-n:case</ta>
            <ta e="T178" id="Seg_5433" s="T177">v-v:tense-v:pred.pn</ta>
            <ta e="T179" id="Seg_5434" s="T178">dempro</ta>
            <ta e="T180" id="Seg_5435" s="T179">n-n:(num)-n:poss-n:case</ta>
            <ta e="T181" id="Seg_5436" s="T180">v-v:tense-v:pred.pn</ta>
            <ta e="T182" id="Seg_5437" s="T181">n-n:(num)-n:case</ta>
            <ta e="T183" id="Seg_5438" s="T182">propr-n:case</ta>
            <ta e="T184" id="Seg_5439" s="T183">n-n:(poss)-n:case</ta>
            <ta e="T185" id="Seg_5440" s="T184">v-v:tense-v:poss.pn</ta>
            <ta e="T186" id="Seg_5441" s="T185">v-v:ptcp</ta>
            <ta e="T187" id="Seg_5442" s="T186">v-v:(ins)-v:mood.pn</ta>
            <ta e="T188" id="Seg_5443" s="T187">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T189" id="Seg_5444" s="T188">v-v:(tense)-v:(ins)-v:mood.pn</ta>
            <ta e="T190" id="Seg_5445" s="T189">v-v:tense-v:poss.pn</ta>
            <ta e="T191" id="Seg_5446" s="T190">conj</ta>
            <ta e="T192" id="Seg_5447" s="T191">v-v:cvb</ta>
            <ta e="T193" id="Seg_5448" s="T192">v-v:tense-v:pred.pn</ta>
            <ta e="T194" id="Seg_5449" s="T193">que</ta>
            <ta e="T195" id="Seg_5450" s="T194">ptcl</ta>
            <ta e="T196" id="Seg_5451" s="T195">v-v:cvb</ta>
            <ta e="T197" id="Seg_5452" s="T196">propr-n:case</ta>
            <ta e="T198" id="Seg_5453" s="T197">ptcl</ta>
            <ta e="T199" id="Seg_5454" s="T198">v-v:tense-v:pred.pn</ta>
            <ta e="T200" id="Seg_5455" s="T199">n-n:(num)-n:case</ta>
            <ta e="T201" id="Seg_5456" s="T200">adv</ta>
            <ta e="T202" id="Seg_5457" s="T201">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T203" id="Seg_5458" s="T202">interj</ta>
            <ta e="T204" id="Seg_5459" s="T203">propr-n:case</ta>
            <ta e="T205" id="Seg_5460" s="T204">adj</ta>
            <ta e="T206" id="Seg_5461" s="T205">n-n:(poss)-n:case</ta>
            <ta e="T207" id="Seg_5462" s="T206">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T208" id="Seg_5463" s="T207">adv</ta>
            <ta e="T209" id="Seg_5464" s="T208">propr-n:case</ta>
            <ta e="T210" id="Seg_5465" s="T209">ptcl</ta>
            <ta e="T211" id="Seg_5466" s="T210">v-v:tense-v:pred.pn</ta>
            <ta e="T212" id="Seg_5467" s="T211">n-n:(num)-n:case</ta>
            <ta e="T213" id="Seg_5468" s="T212">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T214" id="Seg_5469" s="T213">interj</ta>
            <ta e="T215" id="Seg_5470" s="T214">propr-n:case</ta>
            <ta e="T216" id="Seg_5471" s="T215">adj</ta>
            <ta e="T217" id="Seg_5472" s="T216">n-n:(poss)-n:case</ta>
            <ta e="T218" id="Seg_5473" s="T217">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T219" id="Seg_5474" s="T218">propr-n:case</ta>
            <ta e="T220" id="Seg_5475" s="T219">n-n:(num)-n:case</ta>
            <ta e="T221" id="Seg_5476" s="T220">v-v:tense-v:pred.pn</ta>
            <ta e="T222" id="Seg_5477" s="T221">n-n:(num)-n:case</ta>
            <ta e="T223" id="Seg_5478" s="T222">v-v:(ins)-v:mood.pn</ta>
            <ta e="T224" id="Seg_5479" s="T223">pers-n:case</ta>
            <ta e="T225" id="Seg_5480" s="T224">pers-pro:case</ta>
            <ta e="T226" id="Seg_5481" s="T225">pers-pro:case</ta>
            <ta e="T227" id="Seg_5482" s="T226">n-n:case</ta>
            <ta e="T228" id="Seg_5483" s="T227">v-v:cvb</ta>
            <ta e="T229" id="Seg_5484" s="T228">v-v:tense-v:poss.pn</ta>
            <ta e="T230" id="Seg_5485" s="T229">pers-pro:case</ta>
            <ta e="T231" id="Seg_5486" s="T230">n-n:poss-n:case</ta>
            <ta e="T232" id="Seg_5487" s="T231">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T233" id="Seg_5488" s="T232">adj-n:case</ta>
            <ta e="T234" id="Seg_5489" s="T233">v-v:tense-v:poss.pn</ta>
            <ta e="T235" id="Seg_5490" s="T234">interj</ta>
            <ta e="T236" id="Seg_5491" s="T235">ptcl</ta>
            <ta e="T237" id="Seg_5492" s="T236">interj</ta>
            <ta e="T238" id="Seg_5493" s="T237">propr-n:case</ta>
            <ta e="T239" id="Seg_5494" s="T238">v-v:tense-v:pred.pn</ta>
            <ta e="T240" id="Seg_5495" s="T239">propr-n:case</ta>
            <ta e="T241" id="Seg_5496" s="T240">v-v:tense-v:pred.pn</ta>
            <ta e="T242" id="Seg_5497" s="T241">v-v:(ins)-v:mood.pn</ta>
            <ta e="T243" id="Seg_5498" s="T242">n-n:(poss)-n:case</ta>
            <ta e="T244" id="Seg_5499" s="T243">adj</ta>
            <ta e="T245" id="Seg_5500" s="T244">n-n:poss-n:case</ta>
            <ta e="T246" id="Seg_5501" s="T245">v-v:cvb</ta>
            <ta e="T247" id="Seg_5502" s="T246">v-v:tense-v:pred.pn</ta>
            <ta e="T248" id="Seg_5503" s="T247">n-n:poss-n:case</ta>
            <ta e="T249" id="Seg_5504" s="T248">v-v:cvb</ta>
            <ta e="T250" id="Seg_5505" s="T249">v-v:(ins)-v:mood.pn</ta>
            <ta e="T251" id="Seg_5506" s="T250">que</ta>
            <ta e="T252" id="Seg_5507" s="T251">pers-pro:case</ta>
            <ta e="T253" id="Seg_5508" s="T252">v-v:tense-v:pred.pn</ta>
            <ta e="T254" id="Seg_5509" s="T253">n-n:(num)-n:case</ta>
            <ta e="T255" id="Seg_5510" s="T254">v-v:cvb</ta>
            <ta e="T256" id="Seg_5511" s="T255">v-v:tense-v:pred.pn</ta>
            <ta e="T257" id="Seg_5512" s="T256">dempro</ta>
            <ta e="T258" id="Seg_5513" s="T257">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T259" id="Seg_5514" s="T258">propr-n:case</ta>
            <ta e="T260" id="Seg_5515" s="T259">n-n:poss-n:case</ta>
            <ta e="T261" id="Seg_5516" s="T260">v-v:cvb</ta>
            <ta e="T262" id="Seg_5517" s="T261">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T263" id="Seg_5518" s="T262">n-n:poss-n:case</ta>
            <ta e="T264" id="Seg_5519" s="T263">v-v:cvb</ta>
            <ta e="T265" id="Seg_5520" s="T264">v-v:tense-v:pred.pn</ta>
            <ta e="T266" id="Seg_5521" s="T265">n-n:(num)-n:case</ta>
            <ta e="T267" id="Seg_5522" s="T266">n-n:case</ta>
            <ta e="T268" id="Seg_5523" s="T267">adv-adv&gt;adv</ta>
            <ta e="T269" id="Seg_5524" s="T268">v-v:cvb</ta>
            <ta e="T270" id="Seg_5525" s="T269">post</ta>
            <ta e="T271" id="Seg_5526" s="T270">n-n:poss-n:case</ta>
            <ta e="T272" id="Seg_5527" s="T271">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T273" id="Seg_5528" s="T272">n-n:case</ta>
            <ta e="T274" id="Seg_5529" s="T273">v-v:cvb</ta>
            <ta e="T275" id="Seg_5530" s="T274">post</ta>
            <ta e="T276" id="Seg_5531" s="T275">n-n:case</ta>
            <ta e="T277" id="Seg_5532" s="T276">n-n:poss-n:case</ta>
            <ta e="T278" id="Seg_5533" s="T277">v-v:cvb</ta>
            <ta e="T279" id="Seg_5534" s="T278">v-v:tense-v:pred.pn</ta>
            <ta e="T280" id="Seg_5535" s="T279">dempro</ta>
            <ta e="T281" id="Seg_5536" s="T280">v-v:mood-v:temp.pn</ta>
            <ta e="T282" id="Seg_5537" s="T281">propr-n:case</ta>
            <ta e="T283" id="Seg_5538" s="T282">v-v:tense-v:pred.pn</ta>
            <ta e="T284" id="Seg_5539" s="T283">propr-n:case</ta>
            <ta e="T285" id="Seg_5540" s="T284">v-v:cvb</ta>
            <ta e="T286" id="Seg_5541" s="T285">v-v:tense-v:pred.pn</ta>
            <ta e="T287" id="Seg_5542" s="T286">n-n:(num)-n:case</ta>
            <ta e="T288" id="Seg_5543" s="T287">n-n:poss-n:case</ta>
            <ta e="T289" id="Seg_5544" s="T288">v-v:(ins)-v:mood.pn</ta>
            <ta e="T290" id="Seg_5545" s="T289">que-pro:case</ta>
            <ta e="T291" id="Seg_5546" s="T290">ptcl</ta>
            <ta e="T292" id="Seg_5547" s="T291">v-v:(neg)-v:pred.pn</ta>
            <ta e="T293" id="Seg_5548" s="T292">que-que&gt;adv</ta>
            <ta e="T294" id="Seg_5549" s="T293">ptcl</ta>
            <ta e="T295" id="Seg_5550" s="T294">v-v:tense-v:pred.pn</ta>
            <ta e="T296" id="Seg_5551" s="T295">n-n:(num)-n:case</ta>
            <ta e="T297" id="Seg_5552" s="T296">adv</ta>
            <ta e="T298" id="Seg_5553" s="T297">v-v:neg-v:pred.pn</ta>
            <ta e="T299" id="Seg_5554" s="T298">n-n:poss-n:case</ta>
            <ta e="T300" id="Seg_5555" s="T299">v-v:cvb</ta>
            <ta e="T301" id="Seg_5556" s="T300">v-v:cvb</ta>
            <ta e="T302" id="Seg_5557" s="T301">v-v:tense-v:pred.pn</ta>
            <ta e="T303" id="Seg_5558" s="T302">v-v:tense-v:poss.pn</ta>
            <ta e="T304" id="Seg_5559" s="T303">n-n:case</ta>
            <ta e="T305" id="Seg_5560" s="T304">v-v:cvb</ta>
            <ta e="T306" id="Seg_5561" s="T305">v-v:tense-v:pred.pn</ta>
            <ta e="T307" id="Seg_5562" s="T306">n-n:(num)-n:case</ta>
            <ta e="T308" id="Seg_5563" s="T307">v-v:cvb</ta>
            <ta e="T309" id="Seg_5564" s="T308">v-v:tense-v:pred.pn</ta>
            <ta e="T310" id="Seg_5565" s="T309">interj</ta>
            <ta e="T311" id="Seg_5566" s="T310">n-n:(num)-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T312" id="Seg_5567" s="T311">propr-n:case</ta>
            <ta e="T313" id="Seg_5568" s="T312">n-n:poss-n:case</ta>
            <ta e="T314" id="Seg_5569" s="T313">v-v:cvb</ta>
            <ta e="T315" id="Seg_5570" s="T314">post</ta>
            <ta e="T316" id="Seg_5571" s="T315">v-v:cvb</ta>
            <ta e="T317" id="Seg_5572" s="T316">v-v:tense-v:pred.pn</ta>
            <ta e="T318" id="Seg_5573" s="T317">v-v:tense-v:pred.pn</ta>
            <ta e="T319" id="Seg_5574" s="T318">ptcl</ta>
            <ta e="T320" id="Seg_5575" s="T319">v-v:tense-v:pred.pn</ta>
            <ta e="T321" id="Seg_5576" s="T320">n-n:case</ta>
            <ta e="T322" id="Seg_5577" s="T321">v-v:tense-v:pred.pn</ta>
            <ta e="T323" id="Seg_5578" s="T322">n-n:poss-n:case</ta>
            <ta e="T324" id="Seg_5579" s="T323">v-v:tense-v:pred.pn</ta>
            <ta e="T325" id="Seg_5580" s="T324">n-n:(poss)-n:case</ta>
            <ta e="T326" id="Seg_5581" s="T325">v-v:tense-v:pred.pn</ta>
            <ta e="T327" id="Seg_5582" s="T326">interj</ta>
            <ta e="T328" id="Seg_5583" s="T327">propr-n:case</ta>
            <ta e="T329" id="Seg_5584" s="T328">pers-pro:case</ta>
            <ta e="T330" id="Seg_5585" s="T329">n-n:(poss)-n:case</ta>
            <ta e="T331" id="Seg_5586" s="T330">v-v:tense-v:pred.pn</ta>
            <ta e="T332" id="Seg_5587" s="T331">ptcl</ta>
            <ta e="T333" id="Seg_5588" s="T332">v-v:tense-v:pred.pn</ta>
            <ta e="T334" id="Seg_5589" s="T333">n-n:case</ta>
            <ta e="T335" id="Seg_5590" s="T334">v-v:tense-v:pred.pn</ta>
            <ta e="T336" id="Seg_5591" s="T335">n-n:(poss)-n:case</ta>
            <ta e="T337" id="Seg_5592" s="T336">v-v:tense-v:pred.pn</ta>
            <ta e="T338" id="Seg_5593" s="T337">interj</ta>
            <ta e="T339" id="Seg_5594" s="T338">propr-n:case</ta>
            <ta e="T340" id="Seg_5595" s="T339">adj</ta>
            <ta e="T341" id="Seg_5596" s="T340">n-n:(poss)-n:case</ta>
            <ta e="T342" id="Seg_5597" s="T341">v-v:tense-v:pred.pn</ta>
            <ta e="T343" id="Seg_5598" s="T342">ptcl</ta>
            <ta e="T344" id="Seg_5599" s="T343">v-v:tense-v:pred.pn</ta>
            <ta e="T345" id="Seg_5600" s="T344">adv</ta>
            <ta e="T346" id="Seg_5601" s="T345">v-v:tense-v:pred.pn</ta>
            <ta e="T347" id="Seg_5602" s="T346">n-n:(num)-n:poss-n:case</ta>
            <ta e="T348" id="Seg_5603" s="T347">v-v:cvb</ta>
            <ta e="T349" id="Seg_5604" s="T348">v-v:tense-v:poss.pn</ta>
            <ta e="T350" id="Seg_5605" s="T349">cardnum</ta>
            <ta e="T351" id="Seg_5606" s="T350">n-n:case</ta>
            <ta e="T352" id="Seg_5607" s="T351">v-v&gt;n-n:case</ta>
            <ta e="T353" id="Seg_5608" s="T352">v-v:tense-v:pred.pn</ta>
            <ta e="T354" id="Seg_5609" s="T353">v-v:cvb</ta>
            <ta e="T355" id="Seg_5610" s="T354">post</ta>
            <ta e="T356" id="Seg_5611" s="T355">propr-n:case</ta>
            <ta e="T357" id="Seg_5612" s="T356">v-v:tense-v:pred.pn</ta>
            <ta e="T358" id="Seg_5613" s="T357">propr-n:case</ta>
            <ta e="T359" id="Seg_5614" s="T358">que</ta>
            <ta e="T360" id="Seg_5615" s="T359">ptcl-ptcl:(pred.pn)-ptcl</ta>
            <ta e="T361" id="Seg_5616" s="T360">propr-n:case</ta>
            <ta e="T362" id="Seg_5617" s="T361">adv</ta>
            <ta e="T363" id="Seg_5618" s="T362">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T364" id="Seg_5619" s="T363">v-v:tense-v:pred.pn</ta>
            <ta e="T365" id="Seg_5620" s="T364">dempro-pro:case</ta>
            <ta e="T366" id="Seg_5621" s="T365">v-v:tense-v:poss.pn</ta>
            <ta e="T367" id="Seg_5622" s="T366">conj</ta>
            <ta e="T368" id="Seg_5623" s="T367">n-n:poss-n:case</ta>
            <ta e="T369" id="Seg_5624" s="T368">v-v:cvb</ta>
            <ta e="T370" id="Seg_5625" s="T369">v-v:cvb</ta>
            <ta e="T371" id="Seg_5626" s="T370">v-v:tense-v:pred.pn</ta>
            <ta e="T372" id="Seg_5627" s="T371">propr-n:case</ta>
            <ta e="T373" id="Seg_5628" s="T372">que</ta>
            <ta e="T374" id="Seg_5629" s="T373">ptcl</ta>
            <ta e="T375" id="Seg_5630" s="T374">ptcl</ta>
            <ta e="T376" id="Seg_5631" s="T375">propr</ta>
            <ta e="T377" id="Seg_5632" s="T376">que-que:(pred.pn)-ptcl</ta>
            <ta e="T378" id="Seg_5633" s="T377">n-n:case</ta>
            <ta e="T379" id="Seg_5634" s="T378">n-n:poss-n:case</ta>
            <ta e="T380" id="Seg_5635" s="T379">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T381" id="Seg_5636" s="T380">v-v:tense-v:pred.pn</ta>
            <ta e="T382" id="Seg_5637" s="T381">propr-n:case</ta>
            <ta e="T383" id="Seg_5638" s="T382">n-n:case</ta>
            <ta e="T384" id="Seg_5639" s="T383">v-v:cvb</ta>
            <ta e="T385" id="Seg_5640" s="T384">v-v:tense-v:pred.pn</ta>
            <ta e="T386" id="Seg_5641" s="T385">propr-n:case</ta>
            <ta e="T387" id="Seg_5642" s="T386">ptcl</ta>
            <ta e="T388" id="Seg_5643" s="T387">propr</ta>
            <ta e="T389" id="Seg_5644" s="T388">que-que:(pred.pn)-ptcl</ta>
            <ta e="T390" id="Seg_5645" s="T389">n-n:case</ta>
            <ta e="T391" id="Seg_5646" s="T390">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T392" id="Seg_5647" s="T391">n-n:case</ta>
            <ta e="T393" id="Seg_5648" s="T392">v-v:tense-v:poss.pn</ta>
            <ta e="T394" id="Seg_5649" s="T393">propr-n:case</ta>
            <ta e="T395" id="Seg_5650" s="T394">que</ta>
            <ta e="T396" id="Seg_5651" s="T395">ptcl</ta>
            <ta e="T397" id="Seg_5652" s="T396">ptcl</ta>
            <ta e="T398" id="Seg_5653" s="T397">propr</ta>
            <ta e="T399" id="Seg_5654" s="T398">que</ta>
            <ta e="T400" id="Seg_5655" s="T399">ptcl-ptcl:(pred.pn)-ptcl</ta>
            <ta e="T401" id="Seg_5656" s="T400">v-v:tense-v:pred.pn</ta>
            <ta e="T402" id="Seg_5657" s="T401">propr-n:case</ta>
            <ta e="T403" id="Seg_5658" s="T402">n-n:case</ta>
            <ta e="T404" id="Seg_5659" s="T403">n-n:poss-n:case</ta>
            <ta e="T405" id="Seg_5660" s="T404">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T406" id="Seg_5661" s="T405">v-v:tense-v:pred.pn</ta>
            <ta e="T407" id="Seg_5662" s="T406">dempro-pro:case</ta>
            <ta e="T408" id="Seg_5663" s="T407">propr-n:case</ta>
            <ta e="T409" id="Seg_5664" s="T408">v-v:tense-v:poss.pn</ta>
            <ta e="T410" id="Seg_5665" s="T409">conj</ta>
            <ta e="T411" id="Seg_5666" s="T410">n-n:poss-n:case</ta>
            <ta e="T412" id="Seg_5667" s="T411">v-v:cvb</ta>
            <ta e="T413" id="Seg_5668" s="T412">v-v:tense-v:pred.pn</ta>
            <ta e="T414" id="Seg_5669" s="T413">dempro-pro:case</ta>
            <ta e="T415" id="Seg_5670" s="T414">propr-n:case</ta>
            <ta e="T416" id="Seg_5671" s="T415">v-v:tense-v:pred.pn</ta>
            <ta e="T417" id="Seg_5672" s="T416">n-n:case</ta>
            <ta e="T418" id="Seg_5673" s="T417">n-n:poss-n:case</ta>
            <ta e="T419" id="Seg_5674" s="T418">v-v:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T420" id="Seg_5675" s="T419">v-v:tense-v:poss.pn</ta>
            <ta e="T421" id="Seg_5676" s="T420">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T422" id="Seg_5677" s="T421">n-n:poss-n:case</ta>
            <ta e="T423" id="Seg_5678" s="T422">v-v:mood.pn</ta>
            <ta e="T424" id="Seg_5679" s="T423">propr-n:case</ta>
            <ta e="T425" id="Seg_5680" s="T424">n-n:poss-n:case</ta>
            <ta e="T426" id="Seg_5681" s="T425">v-v:tense-v:pred.pn</ta>
            <ta e="T427" id="Seg_5682" s="T426">dempro</ta>
            <ta e="T428" id="Seg_5683" s="T427">v-v:cvb</ta>
            <ta e="T429" id="Seg_5684" s="T428">v-v:mood-v:temp.pn</ta>
            <ta e="T430" id="Seg_5685" s="T429">propr-n:case</ta>
            <ta e="T431" id="Seg_5686" s="T430">v-v&gt;adv</ta>
            <ta e="T432" id="Seg_5687" s="T431">v-v:ptcp</ta>
            <ta e="T433" id="Seg_5688" s="T432">n-n:poss-n:case</ta>
            <ta e="T434" id="Seg_5689" s="T433">n-n:poss-n:case</ta>
            <ta e="T435" id="Seg_5690" s="T434">n-n:poss-n:case</ta>
            <ta e="T436" id="Seg_5691" s="T435">v-v:tense-v:pred.pn</ta>
            <ta e="T437" id="Seg_5692" s="T436">propr-n:case</ta>
            <ta e="T438" id="Seg_5693" s="T437">v-v:cvb</ta>
            <ta e="T439" id="Seg_5694" s="T438">v-v:tense-v:pred.pn</ta>
            <ta e="T440" id="Seg_5695" s="T439">propr-n:case</ta>
            <ta e="T441" id="Seg_5696" s="T440">v-v:tense-v:poss.pn</ta>
            <ta e="T442" id="Seg_5697" s="T441">conj</ta>
            <ta e="T443" id="Seg_5698" s="T442">n-n:poss-n:case</ta>
            <ta e="T444" id="Seg_5699" s="T443">v-v:cvb</ta>
            <ta e="T445" id="Seg_5700" s="T444">v-v:tense-v:pred.pn</ta>
            <ta e="T446" id="Seg_5701" s="T445">adv</ta>
            <ta e="T447" id="Seg_5702" s="T446">v-v:cvb-v-v:cvb</ta>
            <ta e="T448" id="Seg_5703" s="T447">v-v:tense-v:pred.pn</ta>
            <ta e="T449" id="Seg_5704" s="T448">ptcl</ta>
            <ta e="T450" id="Seg_5705" s="T449">v-v:tense-v:poss.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_5706" s="T0">propr</ta>
            <ta e="T2" id="Seg_5707" s="T1">v</ta>
            <ta e="T3" id="Seg_5708" s="T2">v</ta>
            <ta e="T4" id="Seg_5709" s="T3">aux</ta>
            <ta e="T5" id="Seg_5710" s="T4">conj</ta>
            <ta e="T6" id="Seg_5711" s="T5">v</ta>
            <ta e="T7" id="Seg_5712" s="T6">n</ta>
            <ta e="T8" id="Seg_5713" s="T7">v</ta>
            <ta e="T9" id="Seg_5714" s="T8">n</ta>
            <ta e="T10" id="Seg_5715" s="T9">v</ta>
            <ta e="T11" id="Seg_5716" s="T10">aux</ta>
            <ta e="T12" id="Seg_5717" s="T11">n</ta>
            <ta e="T13" id="Seg_5718" s="T12">v</ta>
            <ta e="T14" id="Seg_5719" s="T13">n</ta>
            <ta e="T15" id="Seg_5720" s="T14">v</ta>
            <ta e="T16" id="Seg_5721" s="T15">aux</ta>
            <ta e="T17" id="Seg_5722" s="T16">n</ta>
            <ta e="T18" id="Seg_5723" s="T17">v</ta>
            <ta e="T19" id="Seg_5724" s="T18">n</ta>
            <ta e="T20" id="Seg_5725" s="T19">v</ta>
            <ta e="T21" id="Seg_5726" s="T20">aux</ta>
            <ta e="T22" id="Seg_5727" s="T21">adj</ta>
            <ta e="T23" id="Seg_5728" s="T22">n</ta>
            <ta e="T24" id="Seg_5729" s="T23">v</ta>
            <ta e="T25" id="Seg_5730" s="T24">n</ta>
            <ta e="T26" id="Seg_5731" s="T25">ptcl</ta>
            <ta e="T27" id="Seg_5732" s="T26">v</ta>
            <ta e="T28" id="Seg_5733" s="T27">aux</ta>
            <ta e="T29" id="Seg_5734" s="T28">n</ta>
            <ta e="T30" id="Seg_5735" s="T29">v</ta>
            <ta e="T31" id="Seg_5736" s="T30">n</ta>
            <ta e="T32" id="Seg_5737" s="T31">v</ta>
            <ta e="T33" id="Seg_5738" s="T32">aux</ta>
            <ta e="T34" id="Seg_5739" s="T33">adj</ta>
            <ta e="T35" id="Seg_5740" s="T34">n</ta>
            <ta e="T36" id="Seg_5741" s="T35">v</ta>
            <ta e="T37" id="Seg_5742" s="T36">ptcl</ta>
            <ta e="T38" id="Seg_5743" s="T37">v</ta>
            <ta e="T39" id="Seg_5744" s="T38">aux</ta>
            <ta e="T40" id="Seg_5745" s="T39">n</ta>
            <ta e="T41" id="Seg_5746" s="T40">v</ta>
            <ta e="T42" id="Seg_5747" s="T41">n</ta>
            <ta e="T43" id="Seg_5748" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_5749" s="T43">v</ta>
            <ta e="T45" id="Seg_5750" s="T44">aux</ta>
            <ta e="T46" id="Seg_5751" s="T45">adv</ta>
            <ta e="T47" id="Seg_5752" s="T46">n</ta>
            <ta e="T48" id="Seg_5753" s="T47">v</ta>
            <ta e="T49" id="Seg_5754" s="T48">n</ta>
            <ta e="T50" id="Seg_5755" s="T49">ptcl</ta>
            <ta e="T51" id="Seg_5756" s="T50">v</ta>
            <ta e="T52" id="Seg_5757" s="T51">aux</ta>
            <ta e="T53" id="Seg_5758" s="T52">v</ta>
            <ta e="T54" id="Seg_5759" s="T53">v</ta>
            <ta e="T55" id="Seg_5760" s="T54">dempro</ta>
            <ta e="T56" id="Seg_5761" s="T55">v</ta>
            <ta e="T57" id="Seg_5762" s="T56">propr</ta>
            <ta e="T58" id="Seg_5763" s="T57">v</ta>
            <ta e="T59" id="Seg_5764" s="T58">interj</ta>
            <ta e="T60" id="Seg_5765" s="T59">adj</ta>
            <ta e="T61" id="Seg_5766" s="T60">adj</ta>
            <ta e="T62" id="Seg_5767" s="T61">cop</ta>
            <ta e="T63" id="Seg_5768" s="T62">v</ta>
            <ta e="T64" id="Seg_5769" s="T63">propr</ta>
            <ta e="T65" id="Seg_5770" s="T64">v</ta>
            <ta e="T66" id="Seg_5771" s="T65">n</ta>
            <ta e="T67" id="Seg_5772" s="T66">v</ta>
            <ta e="T68" id="Seg_5773" s="T67">aux</ta>
            <ta e="T69" id="Seg_5774" s="T68">n</ta>
            <ta e="T70" id="Seg_5775" s="T69">v</ta>
            <ta e="T71" id="Seg_5776" s="T70">n</ta>
            <ta e="T72" id="Seg_5777" s="T71">v</ta>
            <ta e="T73" id="Seg_5778" s="T72">propr</ta>
            <ta e="T74" id="Seg_5779" s="T73">v</ta>
            <ta e="T75" id="Seg_5780" s="T74">v</ta>
            <ta e="T76" id="Seg_5781" s="T75">dempro</ta>
            <ta e="T77" id="Seg_5782" s="T76">v</ta>
            <ta e="T78" id="Seg_5783" s="T77">v</ta>
            <ta e="T79" id="Seg_5784" s="T78">propr</ta>
            <ta e="T80" id="Seg_5785" s="T79">n</ta>
            <ta e="T81" id="Seg_5786" s="T80">n</ta>
            <ta e="T82" id="Seg_5787" s="T81">post</ta>
            <ta e="T83" id="Seg_5788" s="T82">v</ta>
            <ta e="T84" id="Seg_5789" s="T83">aux</ta>
            <ta e="T85" id="Seg_5790" s="T84">propr</ta>
            <ta e="T86" id="Seg_5791" s="T85">que</ta>
            <ta e="T87" id="Seg_5792" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_5793" s="T87">v</ta>
            <ta e="T89" id="Seg_5794" s="T88">n</ta>
            <ta e="T90" id="Seg_5795" s="T89">v</ta>
            <ta e="T91" id="Seg_5796" s="T90">conj</ta>
            <ta e="T92" id="Seg_5797" s="T91">n</ta>
            <ta e="T93" id="Seg_5798" s="T92">v</ta>
            <ta e="T94" id="Seg_5799" s="T93">n</ta>
            <ta e="T95" id="Seg_5800" s="T94">n</ta>
            <ta e="T96" id="Seg_5801" s="T95">n</ta>
            <ta e="T97" id="Seg_5802" s="T96">n</ta>
            <ta e="T98" id="Seg_5803" s="T97">v</ta>
            <ta e="T99" id="Seg_5804" s="T98">post</ta>
            <ta e="T100" id="Seg_5805" s="T99">v</ta>
            <ta e="T101" id="Seg_5806" s="T100">n</ta>
            <ta e="T102" id="Seg_5807" s="T101">v</ta>
            <ta e="T103" id="Seg_5808" s="T102">v</ta>
            <ta e="T104" id="Seg_5809" s="T103">aux</ta>
            <ta e="T105" id="Seg_5810" s="T104">n</ta>
            <ta e="T106" id="Seg_5811" s="T105">n</ta>
            <ta e="T107" id="Seg_5812" s="T106">v</ta>
            <ta e="T108" id="Seg_5813" s="T107">n</ta>
            <ta e="T109" id="Seg_5814" s="T108">n</ta>
            <ta e="T110" id="Seg_5815" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_5816" s="T110">adv</ta>
            <ta e="T112" id="Seg_5817" s="T111">dempro</ta>
            <ta e="T113" id="Seg_5818" s="T112">v</ta>
            <ta e="T114" id="Seg_5819" s="T113">aux</ta>
            <ta e="T115" id="Seg_5820" s="T114">v</ta>
            <ta e="T116" id="Seg_5821" s="T115">n</ta>
            <ta e="T117" id="Seg_5822" s="T116">n</ta>
            <ta e="T118" id="Seg_5823" s="T117">adj</ta>
            <ta e="T119" id="Seg_5824" s="T118">n</ta>
            <ta e="T120" id="Seg_5825" s="T119">v</ta>
            <ta e="T121" id="Seg_5826" s="T120">aux</ta>
            <ta e="T122" id="Seg_5827" s="T121">v</ta>
            <ta e="T123" id="Seg_5828" s="T122">aux</ta>
            <ta e="T124" id="Seg_5829" s="T123">v</ta>
            <ta e="T125" id="Seg_5830" s="T124">v</ta>
            <ta e="T126" id="Seg_5831" s="T125">v</ta>
            <ta e="T127" id="Seg_5832" s="T126">n</ta>
            <ta e="T128" id="Seg_5833" s="T127">que</ta>
            <ta e="T129" id="Seg_5834" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_5835" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_5836" s="T130">v</ta>
            <ta e="T132" id="Seg_5837" s="T131">propr</ta>
            <ta e="T133" id="Seg_5838" s="T132">emphpro</ta>
            <ta e="T134" id="Seg_5839" s="T133">v</ta>
            <ta e="T135" id="Seg_5840" s="T134">ptcl</ta>
            <ta e="T136" id="Seg_5841" s="T135">v</ta>
            <ta e="T137" id="Seg_5842" s="T136">conj</ta>
            <ta e="T138" id="Seg_5843" s="T137">n</ta>
            <ta e="T139" id="Seg_5844" s="T138">n</ta>
            <ta e="T140" id="Seg_5845" s="T139">v</ta>
            <ta e="T141" id="Seg_5846" s="T140">aux</ta>
            <ta e="T142" id="Seg_5847" s="T141">n</ta>
            <ta e="T143" id="Seg_5848" s="T142">n</ta>
            <ta e="T144" id="Seg_5849" s="T143">adj</ta>
            <ta e="T145" id="Seg_5850" s="T144">n</ta>
            <ta e="T146" id="Seg_5851" s="T145">interj</ta>
            <ta e="T147" id="Seg_5852" s="T146">v</ta>
            <ta e="T148" id="Seg_5853" s="T147">v</ta>
            <ta e="T149" id="Seg_5854" s="T148">interj</ta>
            <ta e="T150" id="Seg_5855" s="T149">adj</ta>
            <ta e="T151" id="Seg_5856" s="T150">n</ta>
            <ta e="T152" id="Seg_5857" s="T151">v</ta>
            <ta e="T153" id="Seg_5858" s="T152">propr</ta>
            <ta e="T154" id="Seg_5859" s="T153">adv</ta>
            <ta e="T155" id="Seg_5860" s="T154">v</ta>
            <ta e="T156" id="Seg_5861" s="T155">propr</ta>
            <ta e="T157" id="Seg_5862" s="T156">n</ta>
            <ta e="T158" id="Seg_5863" s="T157">dempro</ta>
            <ta e="T159" id="Seg_5864" s="T158">v</ta>
            <ta e="T160" id="Seg_5865" s="T159">v</ta>
            <ta e="T161" id="Seg_5866" s="T160">v</ta>
            <ta e="T162" id="Seg_5867" s="T161">conj</ta>
            <ta e="T163" id="Seg_5868" s="T162">n</ta>
            <ta e="T164" id="Seg_5869" s="T163">v</ta>
            <ta e="T165" id="Seg_5870" s="T164">adv</ta>
            <ta e="T166" id="Seg_5871" s="T165">v</ta>
            <ta e="T167" id="Seg_5872" s="T166">aux</ta>
            <ta e="T168" id="Seg_5873" s="T167">n</ta>
            <ta e="T169" id="Seg_5874" s="T168">v</ta>
            <ta e="T170" id="Seg_5875" s="T169">propr</ta>
            <ta e="T171" id="Seg_5876" s="T170">n</ta>
            <ta e="T172" id="Seg_5877" s="T171">v</ta>
            <ta e="T173" id="Seg_5878" s="T172">aux</ta>
            <ta e="T174" id="Seg_5879" s="T173">propr</ta>
            <ta e="T175" id="Seg_5880" s="T174">cardnum</ta>
            <ta e="T176" id="Seg_5881" s="T175">adj</ta>
            <ta e="T177" id="Seg_5882" s="T176">adj</ta>
            <ta e="T178" id="Seg_5883" s="T177">cop</ta>
            <ta e="T179" id="Seg_5884" s="T178">dempro</ta>
            <ta e="T180" id="Seg_5885" s="T179">n</ta>
            <ta e="T181" id="Seg_5886" s="T180">v</ta>
            <ta e="T182" id="Seg_5887" s="T181">n</ta>
            <ta e="T183" id="Seg_5888" s="T182">propr</ta>
            <ta e="T184" id="Seg_5889" s="T183">n</ta>
            <ta e="T185" id="Seg_5890" s="T184">v</ta>
            <ta e="T186" id="Seg_5891" s="T185">v</ta>
            <ta e="T187" id="Seg_5892" s="T186">aux</ta>
            <ta e="T188" id="Seg_5893" s="T187">v</ta>
            <ta e="T189" id="Seg_5894" s="T188">v</ta>
            <ta e="T190" id="Seg_5895" s="T189">v</ta>
            <ta e="T191" id="Seg_5896" s="T190">conj</ta>
            <ta e="T192" id="Seg_5897" s="T191">v</ta>
            <ta e="T193" id="Seg_5898" s="T192">aux</ta>
            <ta e="T194" id="Seg_5899" s="T193">que</ta>
            <ta e="T195" id="Seg_5900" s="T194">ptcl</ta>
            <ta e="T196" id="Seg_5901" s="T195">cop</ta>
            <ta e="T197" id="Seg_5902" s="T196">propr</ta>
            <ta e="T198" id="Seg_5903" s="T197">ptcl</ta>
            <ta e="T199" id="Seg_5904" s="T198">v</ta>
            <ta e="T200" id="Seg_5905" s="T199">n</ta>
            <ta e="T201" id="Seg_5906" s="T200">adv</ta>
            <ta e="T202" id="Seg_5907" s="T201">v</ta>
            <ta e="T203" id="Seg_5908" s="T202">interj</ta>
            <ta e="T204" id="Seg_5909" s="T203">propr</ta>
            <ta e="T205" id="Seg_5910" s="T204">adj</ta>
            <ta e="T206" id="Seg_5911" s="T205">n</ta>
            <ta e="T207" id="Seg_5912" s="T206">v</ta>
            <ta e="T208" id="Seg_5913" s="T207">adv</ta>
            <ta e="T209" id="Seg_5914" s="T208">propr</ta>
            <ta e="T210" id="Seg_5915" s="T209">ptcl</ta>
            <ta e="T211" id="Seg_5916" s="T210">v</ta>
            <ta e="T212" id="Seg_5917" s="T211">n</ta>
            <ta e="T213" id="Seg_5918" s="T212">v</ta>
            <ta e="T214" id="Seg_5919" s="T213">interj</ta>
            <ta e="T215" id="Seg_5920" s="T214">propr</ta>
            <ta e="T216" id="Seg_5921" s="T215">adj</ta>
            <ta e="T217" id="Seg_5922" s="T216">n</ta>
            <ta e="T218" id="Seg_5923" s="T217">v</ta>
            <ta e="T219" id="Seg_5924" s="T218">propr</ta>
            <ta e="T220" id="Seg_5925" s="T219">n</ta>
            <ta e="T221" id="Seg_5926" s="T220">v</ta>
            <ta e="T222" id="Seg_5927" s="T221">n</ta>
            <ta e="T223" id="Seg_5928" s="T222">v</ta>
            <ta e="T224" id="Seg_5929" s="T223">pers</ta>
            <ta e="T225" id="Seg_5930" s="T224">pers</ta>
            <ta e="T226" id="Seg_5931" s="T225">pers</ta>
            <ta e="T227" id="Seg_5932" s="T226">n</ta>
            <ta e="T228" id="Seg_5933" s="T227">v</ta>
            <ta e="T229" id="Seg_5934" s="T228">aux</ta>
            <ta e="T230" id="Seg_5935" s="T229">pers</ta>
            <ta e="T231" id="Seg_5936" s="T230">n</ta>
            <ta e="T232" id="Seg_5937" s="T231">v</ta>
            <ta e="T233" id="Seg_5938" s="T232">adj</ta>
            <ta e="T234" id="Seg_5939" s="T233">cop</ta>
            <ta e="T235" id="Seg_5940" s="T234">interj</ta>
            <ta e="T236" id="Seg_5941" s="T235">ptcl</ta>
            <ta e="T237" id="Seg_5942" s="T236">interj</ta>
            <ta e="T238" id="Seg_5943" s="T237">propr</ta>
            <ta e="T239" id="Seg_5944" s="T238">v</ta>
            <ta e="T240" id="Seg_5945" s="T239">propr</ta>
            <ta e="T241" id="Seg_5946" s="T240">v</ta>
            <ta e="T242" id="Seg_5947" s="T241">v</ta>
            <ta e="T243" id="Seg_5948" s="T242">n</ta>
            <ta e="T244" id="Seg_5949" s="T243">adj</ta>
            <ta e="T245" id="Seg_5950" s="T244">n</ta>
            <ta e="T246" id="Seg_5951" s="T245">v</ta>
            <ta e="T247" id="Seg_5952" s="T246">v</ta>
            <ta e="T248" id="Seg_5953" s="T247">n</ta>
            <ta e="T249" id="Seg_5954" s="T248">v</ta>
            <ta e="T250" id="Seg_5955" s="T249">v</ta>
            <ta e="T251" id="Seg_5956" s="T250">que</ta>
            <ta e="T252" id="Seg_5957" s="T251">pers</ta>
            <ta e="T253" id="Seg_5958" s="T252">v</ta>
            <ta e="T254" id="Seg_5959" s="T253">n</ta>
            <ta e="T255" id="Seg_5960" s="T254">v</ta>
            <ta e="T256" id="Seg_5961" s="T255">aux</ta>
            <ta e="T257" id="Seg_5962" s="T256">dempro</ta>
            <ta e="T258" id="Seg_5963" s="T257">v</ta>
            <ta e="T259" id="Seg_5964" s="T258">propr</ta>
            <ta e="T260" id="Seg_5965" s="T259">n</ta>
            <ta e="T261" id="Seg_5966" s="T260">v</ta>
            <ta e="T262" id="Seg_5967" s="T261">v</ta>
            <ta e="T263" id="Seg_5968" s="T262">n</ta>
            <ta e="T264" id="Seg_5969" s="T263">v</ta>
            <ta e="T265" id="Seg_5970" s="T264">aux</ta>
            <ta e="T266" id="Seg_5971" s="T265">n</ta>
            <ta e="T267" id="Seg_5972" s="T266">n</ta>
            <ta e="T268" id="Seg_5973" s="T267">adv</ta>
            <ta e="T269" id="Seg_5974" s="T268">v</ta>
            <ta e="T270" id="Seg_5975" s="T269">post</ta>
            <ta e="T271" id="Seg_5976" s="T270">n</ta>
            <ta e="T272" id="Seg_5977" s="T271">v</ta>
            <ta e="T273" id="Seg_5978" s="T272">n</ta>
            <ta e="T274" id="Seg_5979" s="T273">v</ta>
            <ta e="T275" id="Seg_5980" s="T274">post</ta>
            <ta e="T276" id="Seg_5981" s="T275">n</ta>
            <ta e="T277" id="Seg_5982" s="T276">n</ta>
            <ta e="T278" id="Seg_5983" s="T277">v</ta>
            <ta e="T279" id="Seg_5984" s="T278">aux</ta>
            <ta e="T280" id="Seg_5985" s="T279">dempro</ta>
            <ta e="T281" id="Seg_5986" s="T280">v</ta>
            <ta e="T282" id="Seg_5987" s="T281">propr</ta>
            <ta e="T283" id="Seg_5988" s="T282">v</ta>
            <ta e="T284" id="Seg_5989" s="T283">propr</ta>
            <ta e="T285" id="Seg_5990" s="T284">v</ta>
            <ta e="T286" id="Seg_5991" s="T285">v</ta>
            <ta e="T287" id="Seg_5992" s="T286">n</ta>
            <ta e="T288" id="Seg_5993" s="T287">n</ta>
            <ta e="T289" id="Seg_5994" s="T288">v</ta>
            <ta e="T290" id="Seg_5995" s="T289">que</ta>
            <ta e="T291" id="Seg_5996" s="T290">ptcl</ta>
            <ta e="T292" id="Seg_5997" s="T291">v</ta>
            <ta e="T293" id="Seg_5998" s="T292">adv</ta>
            <ta e="T294" id="Seg_5999" s="T293">ptcl</ta>
            <ta e="T295" id="Seg_6000" s="T294">v</ta>
            <ta e="T296" id="Seg_6001" s="T295">n</ta>
            <ta e="T297" id="Seg_6002" s="T296">adv</ta>
            <ta e="T298" id="Seg_6003" s="T297">v</ta>
            <ta e="T299" id="Seg_6004" s="T298">n</ta>
            <ta e="T300" id="Seg_6005" s="T299">v</ta>
            <ta e="T301" id="Seg_6006" s="T300">v</ta>
            <ta e="T302" id="Seg_6007" s="T301">v</ta>
            <ta e="T303" id="Seg_6008" s="T302">v</ta>
            <ta e="T304" id="Seg_6009" s="T303">n</ta>
            <ta e="T305" id="Seg_6010" s="T304">v</ta>
            <ta e="T306" id="Seg_6011" s="T305">aux</ta>
            <ta e="T307" id="Seg_6012" s="T306">n</ta>
            <ta e="T308" id="Seg_6013" s="T307">v</ta>
            <ta e="T309" id="Seg_6014" s="T308">aux</ta>
            <ta e="T310" id="Seg_6015" s="T309">interj</ta>
            <ta e="T311" id="Seg_6016" s="T310">n</ta>
            <ta e="T312" id="Seg_6017" s="T311">propr</ta>
            <ta e="T313" id="Seg_6018" s="T312">n</ta>
            <ta e="T314" id="Seg_6019" s="T313">v</ta>
            <ta e="T315" id="Seg_6020" s="T314">post</ta>
            <ta e="T316" id="Seg_6021" s="T315">v</ta>
            <ta e="T317" id="Seg_6022" s="T316">aux</ta>
            <ta e="T318" id="Seg_6023" s="T317">aux</ta>
            <ta e="T319" id="Seg_6024" s="T318">ptcl</ta>
            <ta e="T320" id="Seg_6025" s="T319">v</ta>
            <ta e="T321" id="Seg_6026" s="T320">n</ta>
            <ta e="T322" id="Seg_6027" s="T321">v</ta>
            <ta e="T323" id="Seg_6028" s="T322">n</ta>
            <ta e="T324" id="Seg_6029" s="T323">v</ta>
            <ta e="T325" id="Seg_6030" s="T324">n</ta>
            <ta e="T326" id="Seg_6031" s="T325">v</ta>
            <ta e="T327" id="Seg_6032" s="T326">interj</ta>
            <ta e="T328" id="Seg_6033" s="T327">propr</ta>
            <ta e="T329" id="Seg_6034" s="T328">pers</ta>
            <ta e="T330" id="Seg_6035" s="T329">n</ta>
            <ta e="T331" id="Seg_6036" s="T330">cop</ta>
            <ta e="T332" id="Seg_6037" s="T331">ptcl</ta>
            <ta e="T333" id="Seg_6038" s="T332">v</ta>
            <ta e="T334" id="Seg_6039" s="T333">n</ta>
            <ta e="T335" id="Seg_6040" s="T334">v</ta>
            <ta e="T336" id="Seg_6041" s="T335">n</ta>
            <ta e="T337" id="Seg_6042" s="T336">v</ta>
            <ta e="T338" id="Seg_6043" s="T337">interj</ta>
            <ta e="T339" id="Seg_6044" s="T338">propr</ta>
            <ta e="T340" id="Seg_6045" s="T339">adj</ta>
            <ta e="T341" id="Seg_6046" s="T340">n</ta>
            <ta e="T342" id="Seg_6047" s="T341">cop</ta>
            <ta e="T343" id="Seg_6048" s="T342">ptcl</ta>
            <ta e="T344" id="Seg_6049" s="T343">v</ta>
            <ta e="T345" id="Seg_6050" s="T344">adv</ta>
            <ta e="T346" id="Seg_6051" s="T345">v</ta>
            <ta e="T347" id="Seg_6052" s="T346">n</ta>
            <ta e="T348" id="Seg_6053" s="T347">v</ta>
            <ta e="T349" id="Seg_6054" s="T348">aux</ta>
            <ta e="T350" id="Seg_6055" s="T349">cardnum</ta>
            <ta e="T351" id="Seg_6056" s="T350">n</ta>
            <ta e="T352" id="Seg_6057" s="T351">n</ta>
            <ta e="T353" id="Seg_6058" s="T352">v</ta>
            <ta e="T354" id="Seg_6059" s="T353">v</ta>
            <ta e="T355" id="Seg_6060" s="T354">post</ta>
            <ta e="T356" id="Seg_6061" s="T355">propr</ta>
            <ta e="T357" id="Seg_6062" s="T356">v</ta>
            <ta e="T358" id="Seg_6063" s="T357">propr</ta>
            <ta e="T359" id="Seg_6064" s="T358">que</ta>
            <ta e="T360" id="Seg_6065" s="T359">ptcl</ta>
            <ta e="T361" id="Seg_6066" s="T360">propr</ta>
            <ta e="T362" id="Seg_6067" s="T361">adv</ta>
            <ta e="T363" id="Seg_6068" s="T362">ptcl</ta>
            <ta e="T364" id="Seg_6069" s="T363">v</ta>
            <ta e="T365" id="Seg_6070" s="T364">dempro</ta>
            <ta e="T366" id="Seg_6071" s="T365">v</ta>
            <ta e="T367" id="Seg_6072" s="T366">conj</ta>
            <ta e="T368" id="Seg_6073" s="T367">n</ta>
            <ta e="T369" id="Seg_6074" s="T368">v</ta>
            <ta e="T370" id="Seg_6075" s="T369">v</ta>
            <ta e="T371" id="Seg_6076" s="T370">v</ta>
            <ta e="T372" id="Seg_6077" s="T371">propr</ta>
            <ta e="T373" id="Seg_6078" s="T372">que</ta>
            <ta e="T374" id="Seg_6079" s="T373">ptcl</ta>
            <ta e="T375" id="Seg_6080" s="T374">ptcl</ta>
            <ta e="T376" id="Seg_6081" s="T375">propr</ta>
            <ta e="T377" id="Seg_6082" s="T376">que</ta>
            <ta e="T378" id="Seg_6083" s="T377">n</ta>
            <ta e="T379" id="Seg_6084" s="T378">n</ta>
            <ta e="T380" id="Seg_6085" s="T379">ptcl</ta>
            <ta e="T381" id="Seg_6086" s="T380">v</ta>
            <ta e="T382" id="Seg_6087" s="T381">propr</ta>
            <ta e="T383" id="Seg_6088" s="T382">n</ta>
            <ta e="T384" id="Seg_6089" s="T383">v</ta>
            <ta e="T385" id="Seg_6090" s="T384">aux</ta>
            <ta e="T386" id="Seg_6091" s="T385">propr</ta>
            <ta e="T387" id="Seg_6092" s="T386">ptcl</ta>
            <ta e="T388" id="Seg_6093" s="T387">propr</ta>
            <ta e="T389" id="Seg_6094" s="T388">que</ta>
            <ta e="T390" id="Seg_6095" s="T389">n</ta>
            <ta e="T391" id="Seg_6096" s="T390">ptcl</ta>
            <ta e="T392" id="Seg_6097" s="T391">n</ta>
            <ta e="T393" id="Seg_6098" s="T392">v</ta>
            <ta e="T394" id="Seg_6099" s="T393">propr</ta>
            <ta e="T395" id="Seg_6100" s="T394">que</ta>
            <ta e="T396" id="Seg_6101" s="T395">ptcl</ta>
            <ta e="T397" id="Seg_6102" s="T396">ptcl</ta>
            <ta e="T398" id="Seg_6103" s="T397">propr</ta>
            <ta e="T399" id="Seg_6104" s="T398">que</ta>
            <ta e="T400" id="Seg_6105" s="T399">ptcl</ta>
            <ta e="T401" id="Seg_6106" s="T400">v</ta>
            <ta e="T402" id="Seg_6107" s="T401">propr</ta>
            <ta e="T403" id="Seg_6108" s="T402">n</ta>
            <ta e="T404" id="Seg_6109" s="T403">n</ta>
            <ta e="T405" id="Seg_6110" s="T404">ptcl</ta>
            <ta e="T406" id="Seg_6111" s="T405">v</ta>
            <ta e="T407" id="Seg_6112" s="T406">dempro</ta>
            <ta e="T408" id="Seg_6113" s="T407">propr</ta>
            <ta e="T409" id="Seg_6114" s="T408">v</ta>
            <ta e="T410" id="Seg_6115" s="T409">conj</ta>
            <ta e="T411" id="Seg_6116" s="T410">n</ta>
            <ta e="T412" id="Seg_6117" s="T411">v</ta>
            <ta e="T413" id="Seg_6118" s="T412">aux</ta>
            <ta e="T414" id="Seg_6119" s="T413">dempro</ta>
            <ta e="T415" id="Seg_6120" s="T414">propr</ta>
            <ta e="T416" id="Seg_6121" s="T415">v</ta>
            <ta e="T417" id="Seg_6122" s="T416">n</ta>
            <ta e="T418" id="Seg_6123" s="T417">n</ta>
            <ta e="T419" id="Seg_6124" s="T418">v</ta>
            <ta e="T420" id="Seg_6125" s="T419">v</ta>
            <ta e="T421" id="Seg_6126" s="T420">ptcl</ta>
            <ta e="T422" id="Seg_6127" s="T421">n</ta>
            <ta e="T423" id="Seg_6128" s="T422">v</ta>
            <ta e="T424" id="Seg_6129" s="T423">propr</ta>
            <ta e="T425" id="Seg_6130" s="T424">n</ta>
            <ta e="T426" id="Seg_6131" s="T425">v</ta>
            <ta e="T427" id="Seg_6132" s="T426">dempro</ta>
            <ta e="T428" id="Seg_6133" s="T427">v</ta>
            <ta e="T429" id="Seg_6134" s="T428">aux</ta>
            <ta e="T430" id="Seg_6135" s="T429">propr</ta>
            <ta e="T431" id="Seg_6136" s="T430">adv</ta>
            <ta e="T432" id="Seg_6137" s="T431">v</ta>
            <ta e="T433" id="Seg_6138" s="T432">n</ta>
            <ta e="T434" id="Seg_6139" s="T433">n</ta>
            <ta e="T435" id="Seg_6140" s="T434">n</ta>
            <ta e="T436" id="Seg_6141" s="T435">v</ta>
            <ta e="T437" id="Seg_6142" s="T436">propr</ta>
            <ta e="T438" id="Seg_6143" s="T437">v</ta>
            <ta e="T439" id="Seg_6144" s="T438">aux</ta>
            <ta e="T440" id="Seg_6145" s="T439">propr</ta>
            <ta e="T441" id="Seg_6146" s="T440">v</ta>
            <ta e="T442" id="Seg_6147" s="T441">conj</ta>
            <ta e="T443" id="Seg_6148" s="T442">n</ta>
            <ta e="T444" id="Seg_6149" s="T443">v</ta>
            <ta e="T445" id="Seg_6150" s="T444">aux</ta>
            <ta e="T446" id="Seg_6151" s="T445">adv</ta>
            <ta e="T447" id="Seg_6152" s="T446">v</ta>
            <ta e="T448" id="Seg_6153" s="T447">v</ta>
            <ta e="T449" id="Seg_6154" s="T448">ptcl</ta>
            <ta e="T450" id="Seg_6155" s="T449">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_6156" s="T0">np.h:A</ta>
            <ta e="T7" id="Seg_6157" s="T6">np:G</ta>
            <ta e="T8" id="Seg_6158" s="T7">0.3.h:P</ta>
            <ta e="T9" id="Seg_6159" s="T8">0.3.h:Poss np:P</ta>
            <ta e="T12" id="Seg_6160" s="T11">np:Ins</ta>
            <ta e="T13" id="Seg_6161" s="T12">0.3.h:A</ta>
            <ta e="T14" id="Seg_6162" s="T13">np:P</ta>
            <ta e="T17" id="Seg_6163" s="T16">0.3.h:Poss np:Ins</ta>
            <ta e="T18" id="Seg_6164" s="T17">0.3.h:A</ta>
            <ta e="T19" id="Seg_6165" s="T18">np:P</ta>
            <ta e="T23" id="Seg_6166" s="T22">0.3.h:Poss np:Ins</ta>
            <ta e="T24" id="Seg_6167" s="T23">0.3.h:A</ta>
            <ta e="T25" id="Seg_6168" s="T24">np:P</ta>
            <ta e="T29" id="Seg_6169" s="T28">0.3.h:Poss np:Ins</ta>
            <ta e="T30" id="Seg_6170" s="T29">0.3.h:A</ta>
            <ta e="T31" id="Seg_6171" s="T30">np:P</ta>
            <ta e="T35" id="Seg_6172" s="T34">0.3.h:Poss np:Ins</ta>
            <ta e="T36" id="Seg_6173" s="T35">0.3.h:A</ta>
            <ta e="T39" id="Seg_6174" s="T37">0.3:P</ta>
            <ta e="T40" id="Seg_6175" s="T39">0.3.h:Poss np:Ins</ta>
            <ta e="T41" id="Seg_6176" s="T40">0.3.h:A</ta>
            <ta e="T42" id="Seg_6177" s="T41">np:P</ta>
            <ta e="T47" id="Seg_6178" s="T46">0.3.h:Poss np:Ins</ta>
            <ta e="T48" id="Seg_6179" s="T47">0.3.h:A</ta>
            <ta e="T49" id="Seg_6180" s="T48">np:P</ta>
            <ta e="T54" id="Seg_6181" s="T53">0.3.h:Th</ta>
            <ta e="T56" id="Seg_6182" s="T55">0.3.h:Th</ta>
            <ta e="T57" id="Seg_6183" s="T56">np.h:A</ta>
            <ta e="T61" id="Seg_6184" s="T60">np:Th</ta>
            <ta e="T62" id="Seg_6185" s="T61">0.1.h:Poss</ta>
            <ta e="T63" id="Seg_6186" s="T62">0.3.h:A</ta>
            <ta e="T64" id="Seg_6187" s="T63">np.h:Th</ta>
            <ta e="T66" id="Seg_6188" s="T65">0.3.h:Poss np:G</ta>
            <ta e="T68" id="Seg_6189" s="T66">0.3.h:A</ta>
            <ta e="T69" id="Seg_6190" s="T68">np:G</ta>
            <ta e="T70" id="Seg_6191" s="T69">0.3.h:A</ta>
            <ta e="T71" id="Seg_6192" s="T70">np:G</ta>
            <ta e="T73" id="Seg_6193" s="T72">np.h:E</ta>
            <ta e="T78" id="Seg_6194" s="T77">0.3.h:Th</ta>
            <ta e="T79" id="Seg_6195" s="T78">np.h:P</ta>
            <ta e="T80" id="Seg_6196" s="T79">np:Poss</ta>
            <ta e="T82" id="Seg_6197" s="T80">pp:Path</ta>
            <ta e="T85" id="Seg_6198" s="T84">np.h:A</ta>
            <ta e="T89" id="Seg_6199" s="T88">0.3.h:Poss np:Th</ta>
            <ta e="T92" id="Seg_6200" s="T91">np:G</ta>
            <ta e="T93" id="Seg_6201" s="T92">0.3.h:A</ta>
            <ta e="T95" id="Seg_6202" s="T94">np:L</ta>
            <ta e="T96" id="Seg_6203" s="T95">0.3.h:Poss np:Th</ta>
            <ta e="T97" id="Seg_6204" s="T96">np:G</ta>
            <ta e="T100" id="Seg_6205" s="T99">0.3.h:A</ta>
            <ta e="T101" id="Seg_6206" s="T100">0.3.h:Poss np.h:Th</ta>
            <ta e="T106" id="Seg_6207" s="T105">np:Th</ta>
            <ta e="T107" id="Seg_6208" s="T106">0.1.h:A</ta>
            <ta e="T108" id="Seg_6209" s="T107">0.1.h:Poss np:Poss</ta>
            <ta e="T109" id="Seg_6210" s="T108">np:L</ta>
            <ta e="T110" id="Seg_6211" s="T109">0.3:Th</ta>
            <ta e="T111" id="Seg_6212" s="T110">adv:L</ta>
            <ta e="T112" id="Seg_6213" s="T111">pro:Th</ta>
            <ta e="T114" id="Seg_6214" s="T112">0.2.h:A</ta>
            <ta e="T115" id="Seg_6215" s="T114">0.3.h:A</ta>
            <ta e="T116" id="Seg_6216" s="T115">np.h:A</ta>
            <ta e="T117" id="Seg_6217" s="T116">np:Cau</ta>
            <ta e="T118" id="Seg_6218" s="T117">np:Com</ta>
            <ta e="T119" id="Seg_6219" s="T118">0.3.h:Poss np:P</ta>
            <ta e="T123" id="Seg_6220" s="T121">0.3.h:A</ta>
            <ta e="T126" id="Seg_6221" s="T125">0.3.h:A</ta>
            <ta e="T127" id="Seg_6222" s="T126">0.2.h:Poss np:L</ta>
            <ta e="T128" id="Seg_6223" s="T127">pro:Th</ta>
            <ta e="T131" id="Seg_6224" s="T130">0.3.h:A</ta>
            <ta e="T133" id="Seg_6225" s="T132">pro.h:A</ta>
            <ta e="T136" id="Seg_6226" s="T135">0.3.h:A</ta>
            <ta e="T138" id="Seg_6227" s="T137">np.h:Poss</ta>
            <ta e="T139" id="Seg_6228" s="T138">np:P</ta>
            <ta e="T141" id="Seg_6229" s="T139">0.3.h:A</ta>
            <ta e="T142" id="Seg_6230" s="T141">np.h:Poss</ta>
            <ta e="T143" id="Seg_6231" s="T142">np:So</ta>
            <ta e="T144" id="Seg_6232" s="T143">np:Com</ta>
            <ta e="T145" id="Seg_6233" s="T144">np:P</ta>
            <ta e="T153" id="Seg_6234" s="T152">np.h:A</ta>
            <ta e="T155" id="Seg_6235" s="T154">0.3.h:A</ta>
            <ta e="T156" id="Seg_6236" s="T155">np.h:Th</ta>
            <ta e="T157" id="Seg_6237" s="T156">np:L</ta>
            <ta e="T161" id="Seg_6238" s="T160">0.3.h:A</ta>
            <ta e="T163" id="Seg_6239" s="T162">0.3.h:Poss np:G</ta>
            <ta e="T164" id="Seg_6240" s="T163">0.3.h:A</ta>
            <ta e="T167" id="Seg_6241" s="T165">0.3.h:A</ta>
            <ta e="T168" id="Seg_6242" s="T167">np:G</ta>
            <ta e="T170" id="Seg_6243" s="T169">np.h:P</ta>
            <ta e="T171" id="Seg_6244" s="T170">np:G</ta>
            <ta e="T173" id="Seg_6245" s="T171">0.3.h:A</ta>
            <ta e="T174" id="Seg_6246" s="T173">np.h:Poss</ta>
            <ta e="T177" id="Seg_6247" s="T176">np.h:Th</ta>
            <ta e="T180" id="Seg_6248" s="T179">0.3.h:Poss np.h:R</ta>
            <ta e="T181" id="Seg_6249" s="T180">0.3.h:A</ta>
            <ta e="T183" id="Seg_6250" s="T182">np.h:Poss</ta>
            <ta e="T184" id="Seg_6251" s="T183">np:P</ta>
            <ta e="T187" id="Seg_6252" s="T185">0.2.h:A</ta>
            <ta e="T188" id="Seg_6253" s="T187">0.1.h:A</ta>
            <ta e="T189" id="Seg_6254" s="T188">0.2.h:A</ta>
            <ta e="T190" id="Seg_6255" s="T189">0.3.h:A</ta>
            <ta e="T193" id="Seg_6256" s="T191">0.3.h:A</ta>
            <ta e="T197" id="Seg_6257" s="T196">np.h:A</ta>
            <ta e="T200" id="Seg_6258" s="T199">np.h:A</ta>
            <ta e="T204" id="Seg_6259" s="T203">np.h:Poss</ta>
            <ta e="T206" id="Seg_6260" s="T205">np:P</ta>
            <ta e="T209" id="Seg_6261" s="T208">np.h:A</ta>
            <ta e="T212" id="Seg_6262" s="T211">np.h:A</ta>
            <ta e="T215" id="Seg_6263" s="T214">np.h:Poss</ta>
            <ta e="T217" id="Seg_6264" s="T216">np:P</ta>
            <ta e="T219" id="Seg_6265" s="T218">np.h:A</ta>
            <ta e="T220" id="Seg_6266" s="T219">np.h:R</ta>
            <ta e="T223" id="Seg_6267" s="T222">0.2.h:A</ta>
            <ta e="T224" id="Seg_6268" s="T223">pro.h:Th</ta>
            <ta e="T225" id="Seg_6269" s="T224">pro.h:A</ta>
            <ta e="T226" id="Seg_6270" s="T225">pro.h:B</ta>
            <ta e="T227" id="Seg_6271" s="T226">np:P</ta>
            <ta e="T238" id="Seg_6272" s="T237">np.h:Th</ta>
            <ta e="T239" id="Seg_6273" s="T238">0.3.h:A</ta>
            <ta e="T240" id="Seg_6274" s="T239">np.h:A</ta>
            <ta e="T242" id="Seg_6275" s="T241">0.2.h:A</ta>
            <ta e="T243" id="Seg_6276" s="T242">0.2.h:Poss np.h:Poss</ta>
            <ta e="T245" id="Seg_6277" s="T244">np:Th</ta>
            <ta e="T247" id="Seg_6278" s="T246">0.3.h:A</ta>
            <ta e="T248" id="Seg_6279" s="T247">np:L</ta>
            <ta e="T250" id="Seg_6280" s="T249">0.2.h:A</ta>
            <ta e="T252" id="Seg_6281" s="T251">pro.h:A</ta>
            <ta e="T254" id="Seg_6282" s="T253">np.h:A</ta>
            <ta e="T258" id="Seg_6283" s="T257">0.3.h:A</ta>
            <ta e="T259" id="Seg_6284" s="T258">np.h:A</ta>
            <ta e="T260" id="Seg_6285" s="T259">0.3.h:Poss np:P</ta>
            <ta e="T263" id="Seg_6286" s="T262">np:P</ta>
            <ta e="T265" id="Seg_6287" s="T263">0.3.h:A</ta>
            <ta e="T266" id="Seg_6288" s="T265">np:Th</ta>
            <ta e="T267" id="Seg_6289" s="T266">np:L</ta>
            <ta e="T271" id="Seg_6290" s="T270">np:Ins</ta>
            <ta e="T272" id="Seg_6291" s="T271">0.3.h:A</ta>
            <ta e="T273" id="Seg_6292" s="T272">np:Th</ta>
            <ta e="T277" id="Seg_6293" s="T276">np:G</ta>
            <ta e="T279" id="Seg_6294" s="T277">0.3.h:A</ta>
            <ta e="T281" id="Seg_6295" s="T280">0.3.h:Th</ta>
            <ta e="T282" id="Seg_6296" s="T281">np.h:A</ta>
            <ta e="T284" id="Seg_6297" s="T283">np.h:A</ta>
            <ta e="T288" id="Seg_6298" s="T287">np:Th</ta>
            <ta e="T289" id="Seg_6299" s="T288">0.2.h:A</ta>
            <ta e="T290" id="Seg_6300" s="T289">pro.h:A</ta>
            <ta e="T295" id="Seg_6301" s="T294">0.3.h:A</ta>
            <ta e="T296" id="Seg_6302" s="T295">np.h:A</ta>
            <ta e="T299" id="Seg_6303" s="T298">np:P</ta>
            <ta e="T302" id="Seg_6304" s="T301">0.3.h:A</ta>
            <ta e="T303" id="Seg_6305" s="T302">0.3.h:E</ta>
            <ta e="T304" id="Seg_6306" s="T303">np:P</ta>
            <ta e="T307" id="Seg_6307" s="T306">np.h:Th</ta>
            <ta e="T311" id="Seg_6308" s="T310">0.1.h:Poss 0.1.h:Th</ta>
            <ta e="T312" id="Seg_6309" s="T311">np.h:Poss</ta>
            <ta e="T320" id="Seg_6310" s="T319">0.3.h:A</ta>
            <ta e="T321" id="Seg_6311" s="T320">np:Th</ta>
            <ta e="T322" id="Seg_6312" s="T321">0.3.h:A</ta>
            <ta e="T324" id="Seg_6313" s="T323">0.3.h:A</ta>
            <ta e="T325" id="Seg_6314" s="T324">0.3.h:Poss np:Th</ta>
            <ta e="T328" id="Seg_6315" s="T327">np.h:Th</ta>
            <ta e="T329" id="Seg_6316" s="T328">pro.h:Poss</ta>
            <ta e="T333" id="Seg_6317" s="T332">0.3.h:A</ta>
            <ta e="T334" id="Seg_6318" s="T333">np:P</ta>
            <ta e="T335" id="Seg_6319" s="T334">0.3.h:E</ta>
            <ta e="T336" id="Seg_6320" s="T335">0.3.h:Poss np:Cau</ta>
            <ta e="T339" id="Seg_6321" s="T338">np.h:Th</ta>
            <ta e="T341" id="Seg_6322" s="T340">0.1.h:Poss </ta>
            <ta e="T344" id="Seg_6323" s="T343">0.3.h:A</ta>
            <ta e="T346" id="Seg_6324" s="T345">0.3.h:A</ta>
            <ta e="T347" id="Seg_6325" s="T346">np.h:Th</ta>
            <ta e="T349" id="Seg_6326" s="T347">0.3.h:A</ta>
            <ta e="T351" id="Seg_6327" s="T350">np:P</ta>
            <ta e="T356" id="Seg_6328" s="T355">np.h:A</ta>
            <ta e="T359" id="Seg_6329" s="T358">pro:L</ta>
            <ta e="T360" id="Seg_6330" s="T359">0.2.h:Th</ta>
            <ta e="T362" id="Seg_6331" s="T361">adv:L</ta>
            <ta e="T363" id="Seg_6332" s="T362">0.1.h:Th</ta>
            <ta e="T364" id="Seg_6333" s="T363">0.3.h:A</ta>
            <ta e="T365" id="Seg_6334" s="T364">pro:St</ta>
            <ta e="T366" id="Seg_6335" s="T365">0.3.h:E</ta>
            <ta e="T368" id="Seg_6336" s="T367">np:P</ta>
            <ta e="T371" id="Seg_6337" s="T370">0.3.h:A</ta>
            <ta e="T372" id="Seg_6338" s="T371">np.h:Th</ta>
            <ta e="T377" id="Seg_6339" s="T376">0.2.h:Th</ta>
            <ta e="T379" id="Seg_6340" s="T378">np:L</ta>
            <ta e="T380" id="Seg_6341" s="T379">0.1.h:Th</ta>
            <ta e="T382" id="Seg_6342" s="T381">np.h:A</ta>
            <ta e="T383" id="Seg_6343" s="T382">np:G</ta>
            <ta e="T385" id="Seg_6344" s="T383">0.3.h:A</ta>
            <ta e="T386" id="Seg_6345" s="T385">np.h:Th</ta>
            <ta e="T389" id="Seg_6346" s="T388">0.2.h:Th</ta>
            <ta e="T390" id="Seg_6347" s="T389">np:L</ta>
            <ta e="T391" id="Seg_6348" s="T390">0.1.h:Th</ta>
            <ta e="T393" id="Seg_6349" s="T392">0.3.h:A</ta>
            <ta e="T394" id="Seg_6350" s="T393">np.h:Th</ta>
            <ta e="T399" id="Seg_6351" s="T398">pro:L</ta>
            <ta e="T400" id="Seg_6352" s="T399">0.2.h:Th</ta>
            <ta e="T401" id="Seg_6353" s="T400">0.3.h:A</ta>
            <ta e="T404" id="Seg_6354" s="T403">np:L</ta>
            <ta e="T405" id="Seg_6355" s="T404">0.1.h:Th</ta>
            <ta e="T406" id="Seg_6356" s="T405">0.3.h:A</ta>
            <ta e="T407" id="Seg_6357" s="T406">pro:St</ta>
            <ta e="T408" id="Seg_6358" s="T407">np.h:E</ta>
            <ta e="T413" id="Seg_6359" s="T412">0.3.h:A</ta>
            <ta e="T415" id="Seg_6360" s="T414">np.h:A</ta>
            <ta e="T419" id="Seg_6361" s="T418">0.2.h:A</ta>
            <ta e="T421" id="Seg_6362" s="T419">0.2.h:Th</ta>
            <ta e="T423" id="Seg_6363" s="T422">0.2.h:A</ta>
            <ta e="T424" id="Seg_6364" s="T423">np.h:A</ta>
            <ta e="T429" id="Seg_6365" s="T428">0.3.h:A</ta>
            <ta e="T430" id="Seg_6366" s="T429">np.h:A</ta>
            <ta e="T433" id="Seg_6367" s="T432">np:Ins</ta>
            <ta e="T435" id="Seg_6368" s="T434">np:G</ta>
            <ta e="T437" id="Seg_6369" s="T436">np.h:P</ta>
            <ta e="T440" id="Seg_6370" s="T439">np.h:A</ta>
            <ta e="T443" id="Seg_6371" s="T442">np:G</ta>
            <ta e="T445" id="Seg_6372" s="T443">0.3.h:A</ta>
            <ta e="T448" id="Seg_6373" s="T447">0.3.h:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_6374" s="T0">np.h:S</ta>
            <ta e="T4" id="Seg_6375" s="T2">v:pred</ta>
            <ta e="T6" id="Seg_6376" s="T5">s:adv</ta>
            <ta e="T8" id="Seg_6377" s="T7">0.3.h:S v:pred</ta>
            <ta e="T9" id="Seg_6378" s="T8">np:S</ta>
            <ta e="T11" id="Seg_6379" s="T9">v:pred</ta>
            <ta e="T13" id="Seg_6380" s="T12">0.3.h:S v:pred</ta>
            <ta e="T14" id="Seg_6381" s="T13">np:S</ta>
            <ta e="T16" id="Seg_6382" s="T14">v:pred</ta>
            <ta e="T18" id="Seg_6383" s="T17">0.3.h:S v:pred</ta>
            <ta e="T19" id="Seg_6384" s="T18">np:S</ta>
            <ta e="T21" id="Seg_6385" s="T19">v:pred</ta>
            <ta e="T24" id="Seg_6386" s="T23">0.3.h:S v:pred</ta>
            <ta e="T25" id="Seg_6387" s="T24">np:S</ta>
            <ta e="T28" id="Seg_6388" s="T26">v:pred</ta>
            <ta e="T30" id="Seg_6389" s="T29">0.3.h:S v:pred</ta>
            <ta e="T31" id="Seg_6390" s="T30">np:S</ta>
            <ta e="T33" id="Seg_6391" s="T31">v:pred</ta>
            <ta e="T36" id="Seg_6392" s="T35">0.3.h:S v:pred</ta>
            <ta e="T39" id="Seg_6393" s="T37">0.3:S v:pred</ta>
            <ta e="T41" id="Seg_6394" s="T40">0.3.h:S v:pred</ta>
            <ta e="T42" id="Seg_6395" s="T41">np:S</ta>
            <ta e="T45" id="Seg_6396" s="T43">v:pred</ta>
            <ta e="T48" id="Seg_6397" s="T47">0.3.h:S v:pred</ta>
            <ta e="T49" id="Seg_6398" s="T48">np:S</ta>
            <ta e="T52" id="Seg_6399" s="T50">v:pred</ta>
            <ta e="T53" id="Seg_6400" s="T52">s:adv</ta>
            <ta e="T54" id="Seg_6401" s="T53">0.3.h:S v:pred</ta>
            <ta e="T56" id="Seg_6402" s="T54">s:temp</ta>
            <ta e="T57" id="Seg_6403" s="T56">np.h:S</ta>
            <ta e="T58" id="Seg_6404" s="T57">v:pred</ta>
            <ta e="T61" id="Seg_6405" s="T60">adj:pred</ta>
            <ta e="T62" id="Seg_6406" s="T61">0.1.h:S cop</ta>
            <ta e="T63" id="Seg_6407" s="T62">s:adv</ta>
            <ta e="T65" id="Seg_6408" s="T63">s:temp</ta>
            <ta e="T68" id="Seg_6409" s="T66">0.3.h:S v:pred</ta>
            <ta e="T70" id="Seg_6410" s="T69">0.3.h:S v:pred</ta>
            <ta e="T72" id="Seg_6411" s="T70">s:temp</ta>
            <ta e="T73" id="Seg_6412" s="T72">np.h:S</ta>
            <ta e="T74" id="Seg_6413" s="T73">s:comp</ta>
            <ta e="T75" id="Seg_6414" s="T74">v:pred</ta>
            <ta e="T78" id="Seg_6415" s="T75">s:temp</ta>
            <ta e="T79" id="Seg_6416" s="T78">np.h:S</ta>
            <ta e="T84" id="Seg_6417" s="T82">v:pred</ta>
            <ta e="T85" id="Seg_6418" s="T84">np.h:S</ta>
            <ta e="T88" id="Seg_6419" s="T85">s:adv</ta>
            <ta e="T89" id="Seg_6420" s="T88">np:O</ta>
            <ta e="T90" id="Seg_6421" s="T89">v:pred</ta>
            <ta e="T93" id="Seg_6422" s="T92">0.3.h:S v:pred</ta>
            <ta e="T99" id="Seg_6423" s="T93">s:temp</ta>
            <ta e="T100" id="Seg_6424" s="T99">0.3.h:S v:pred</ta>
            <ta e="T101" id="Seg_6425" s="T100">np.h:S</ta>
            <ta e="T102" id="Seg_6426" s="T101">s:adv</ta>
            <ta e="T104" id="Seg_6427" s="T102">v:pred</ta>
            <ta e="T106" id="Seg_6428" s="T105">np:O</ta>
            <ta e="T107" id="Seg_6429" s="T106">0.1.h:S v:pred</ta>
            <ta e="T109" id="Seg_6430" s="T108">n:pred</ta>
            <ta e="T110" id="Seg_6431" s="T109">0.3:S ptcl:pred</ta>
            <ta e="T112" id="Seg_6432" s="T111">pro:O</ta>
            <ta e="T114" id="Seg_6433" s="T112">0.2.h:S v:pred</ta>
            <ta e="T115" id="Seg_6434" s="T114">0.3.h:S v:pred</ta>
            <ta e="T116" id="Seg_6435" s="T115">np.h:S</ta>
            <ta e="T119" id="Seg_6436" s="T118">np:O</ta>
            <ta e="T121" id="Seg_6437" s="T119">v:pred</ta>
            <ta e="T123" id="Seg_6438" s="T121">0.3.h:S v:pred</ta>
            <ta e="T124" id="Seg_6439" s="T123">s:adv</ta>
            <ta e="T126" id="Seg_6440" s="T125">0.3.h:S v:pred</ta>
            <ta e="T128" id="Seg_6441" s="T127">pro:S</ta>
            <ta e="T130" id="Seg_6442" s="T129">ptcl:pred</ta>
            <ta e="T131" id="Seg_6443" s="T130">0.3.h:S v:pred</ta>
            <ta e="T133" id="Seg_6444" s="T132">pro.h:S</ta>
            <ta e="T134" id="Seg_6445" s="T133">v:pred</ta>
            <ta e="T136" id="Seg_6446" s="T135">0.3.h:S v:pred</ta>
            <ta e="T139" id="Seg_6447" s="T138">np:O</ta>
            <ta e="T141" id="Seg_6448" s="T139">0.3.h:S v:pred</ta>
            <ta e="T145" id="Seg_6449" s="T144">np:S</ta>
            <ta e="T147" id="Seg_6450" s="T145">s:adv</ta>
            <ta e="T148" id="Seg_6451" s="T147">v:pred</ta>
            <ta e="T152" id="Seg_6452" s="T151">v:pred</ta>
            <ta e="T153" id="Seg_6453" s="T152">np.h:S</ta>
            <ta e="T155" id="Seg_6454" s="T154">0.3.h:S v:pred</ta>
            <ta e="T156" id="Seg_6455" s="T155">np.h:S</ta>
            <ta e="T160" id="Seg_6456" s="T159">v:pred</ta>
            <ta e="T161" id="Seg_6457" s="T160">0.3.h:S v:pred</ta>
            <ta e="T164" id="Seg_6458" s="T163">0.3.h:S v:pred</ta>
            <ta e="T167" id="Seg_6459" s="T165">0.3.h:S v:pred</ta>
            <ta e="T169" id="Seg_6460" s="T167">s:temp</ta>
            <ta e="T170" id="Seg_6461" s="T169">np.h:O</ta>
            <ta e="T173" id="Seg_6462" s="T171">0.3.h:S v:pred</ta>
            <ta e="T174" id="Seg_6463" s="T173">np.h:S</ta>
            <ta e="T177" id="Seg_6464" s="T176">adj:pred</ta>
            <ta e="T178" id="Seg_6465" s="T177">cop</ta>
            <ta e="T181" id="Seg_6466" s="T180">0.3.h:S v:pred</ta>
            <ta e="T184" id="Seg_6467" s="T183">np:S</ta>
            <ta e="T185" id="Seg_6468" s="T184">v:pred</ta>
            <ta e="T187" id="Seg_6469" s="T185">0.2.h:S v:pred</ta>
            <ta e="T188" id="Seg_6470" s="T187">s:temp</ta>
            <ta e="T189" id="Seg_6471" s="T188">0.2.h:S v:pred</ta>
            <ta e="T190" id="Seg_6472" s="T189">0.3.h:S v:pred</ta>
            <ta e="T193" id="Seg_6473" s="T191">0.3.h:S v:pred</ta>
            <ta e="T197" id="Seg_6474" s="T196">np.h:S</ta>
            <ta e="T199" id="Seg_6475" s="T198">v:pred</ta>
            <ta e="T200" id="Seg_6476" s="T199">np.h:S</ta>
            <ta e="T202" id="Seg_6477" s="T201">v:pred</ta>
            <ta e="T206" id="Seg_6478" s="T205">np:S</ta>
            <ta e="T207" id="Seg_6479" s="T206">v:pred</ta>
            <ta e="T209" id="Seg_6480" s="T208">np.h:S</ta>
            <ta e="T211" id="Seg_6481" s="T210">v:pred</ta>
            <ta e="T212" id="Seg_6482" s="T211">np.h:S</ta>
            <ta e="T213" id="Seg_6483" s="T212">v:pred</ta>
            <ta e="T217" id="Seg_6484" s="T216">np:S</ta>
            <ta e="T218" id="Seg_6485" s="T217">v:pred</ta>
            <ta e="T219" id="Seg_6486" s="T218">np.h:S</ta>
            <ta e="T221" id="Seg_6487" s="T220">v:pred</ta>
            <ta e="T223" id="Seg_6488" s="T222">0.2.h:S v:pred</ta>
            <ta e="T224" id="Seg_6489" s="T223">pro.h:O</ta>
            <ta e="T225" id="Seg_6490" s="T224">pro.h:S</ta>
            <ta e="T227" id="Seg_6491" s="T226">np:O</ta>
            <ta e="T229" id="Seg_6492" s="T227">v:pred</ta>
            <ta e="T234" id="Seg_6493" s="T229">s:purp</ta>
            <ta e="T238" id="Seg_6494" s="T237">np.h:O</ta>
            <ta e="T239" id="Seg_6495" s="T238">0.3.h:S v:pred</ta>
            <ta e="T240" id="Seg_6496" s="T239">np.h:S</ta>
            <ta e="T241" id="Seg_6497" s="T240">v:pred</ta>
            <ta e="T242" id="Seg_6498" s="T241">0.2.h:S v:pred</ta>
            <ta e="T245" id="Seg_6499" s="T244">np:O</ta>
            <ta e="T247" id="Seg_6500" s="T246">0.3.h:S v:pred</ta>
            <ta e="T250" id="Seg_6501" s="T249">0.2.h:S v:pred</ta>
            <ta e="T253" id="Seg_6502" s="T250">s:adv</ta>
            <ta e="T254" id="Seg_6503" s="T253">np.h:S</ta>
            <ta e="T256" id="Seg_6504" s="T254">v:pred</ta>
            <ta e="T258" id="Seg_6505" s="T256">s:temp</ta>
            <ta e="T259" id="Seg_6506" s="T258">np.h:S</ta>
            <ta e="T260" id="Seg_6507" s="T259">np:O</ta>
            <ta e="T262" id="Seg_6508" s="T261">v:pred</ta>
            <ta e="T263" id="Seg_6509" s="T262">np:O</ta>
            <ta e="T265" id="Seg_6510" s="T263">0.3.h:S v:pred</ta>
            <ta e="T266" id="Seg_6511" s="T265">np:O</ta>
            <ta e="T270" id="Seg_6512" s="T266">s:temp</ta>
            <ta e="T272" id="Seg_6513" s="T271">0.3.h:S v:pred</ta>
            <ta e="T275" id="Seg_6514" s="T272">s:temp</ta>
            <ta e="T279" id="Seg_6515" s="T277">0.3.h:S v:pred</ta>
            <ta e="T281" id="Seg_6516" s="T279">s:temp</ta>
            <ta e="T282" id="Seg_6517" s="T281">np.h:S</ta>
            <ta e="T283" id="Seg_6518" s="T282">v:pred</ta>
            <ta e="T284" id="Seg_6519" s="T283">np.h:S</ta>
            <ta e="T285" id="Seg_6520" s="T284">s:temp</ta>
            <ta e="T286" id="Seg_6521" s="T285">v:pred</ta>
            <ta e="T288" id="Seg_6522" s="T287">np:O</ta>
            <ta e="T289" id="Seg_6523" s="T288">0.2.h:S v:pred</ta>
            <ta e="T290" id="Seg_6524" s="T289">pro.h:S</ta>
            <ta e="T292" id="Seg_6525" s="T291">v:pred</ta>
            <ta e="T295" id="Seg_6526" s="T294">0.3.h:S v:pred</ta>
            <ta e="T296" id="Seg_6527" s="T295">np.h:S</ta>
            <ta e="T298" id="Seg_6528" s="T297">v:pred</ta>
            <ta e="T301" id="Seg_6529" s="T298">s:temp</ta>
            <ta e="T302" id="Seg_6530" s="T301">0.3.h:S v:pred</ta>
            <ta e="T303" id="Seg_6531" s="T302">0.3.h:S v:pred</ta>
            <ta e="T304" id="Seg_6532" s="T303">np:S</ta>
            <ta e="T306" id="Seg_6533" s="T304">v:pred</ta>
            <ta e="T307" id="Seg_6534" s="T306">np.h:S</ta>
            <ta e="T309" id="Seg_6535" s="T307">v:pred</ta>
            <ta e="T311" id="Seg_6536" s="T310">np.h:S</ta>
            <ta e="T315" id="Seg_6537" s="T311">s:temp</ta>
            <ta e="T317" id="Seg_6538" s="T315">v:pred</ta>
            <ta e="T320" id="Seg_6539" s="T319">0.3.h:S v:pred</ta>
            <ta e="T321" id="Seg_6540" s="T320">np:O</ta>
            <ta e="T322" id="Seg_6541" s="T321">0.3.h:S v:pred</ta>
            <ta e="T324" id="Seg_6542" s="T323">0.3.h:S v:pred</ta>
            <ta e="T325" id="Seg_6543" s="T324">np:S</ta>
            <ta e="T326" id="Seg_6544" s="T325">v:pred</ta>
            <ta e="T328" id="Seg_6545" s="T327">np.h:S</ta>
            <ta e="T330" id="Seg_6546" s="T329">n:pred</ta>
            <ta e="T331" id="Seg_6547" s="T330">cop</ta>
            <ta e="T333" id="Seg_6548" s="T332">0.3.h:S v:pred</ta>
            <ta e="T334" id="Seg_6549" s="T333">np:O</ta>
            <ta e="T335" id="Seg_6550" s="T334">0.3.h:S v:pred</ta>
            <ta e="T336" id="Seg_6551" s="T335">np:S</ta>
            <ta e="T337" id="Seg_6552" s="T336">v:pred</ta>
            <ta e="T339" id="Seg_6553" s="T338">np.h:S</ta>
            <ta e="T341" id="Seg_6554" s="T340">n:pred</ta>
            <ta e="T342" id="Seg_6555" s="T341">cop</ta>
            <ta e="T344" id="Seg_6556" s="T343">0.3.h:S v:pred</ta>
            <ta e="T346" id="Seg_6557" s="T345">0.3.h:S v:pred</ta>
            <ta e="T347" id="Seg_6558" s="T346">np.h:O</ta>
            <ta e="T349" id="Seg_6559" s="T347">0.3.h:S v:pred</ta>
            <ta e="T351" id="Seg_6560" s="T350">np:S</ta>
            <ta e="T352" id="Seg_6561" s="T351">np:O</ta>
            <ta e="T353" id="Seg_6562" s="T352">v:pred</ta>
            <ta e="T354" id="Seg_6563" s="T353">s:adv</ta>
            <ta e="T356" id="Seg_6564" s="T355">np.h:S</ta>
            <ta e="T357" id="Seg_6565" s="T356">v:pred</ta>
            <ta e="T360" id="Seg_6566" s="T359">0.2.h:S ptcl:pred</ta>
            <ta e="T363" id="Seg_6567" s="T362">0.1.h:S ptcl:pred</ta>
            <ta e="T364" id="Seg_6568" s="T363">0.3.h:S v:pred</ta>
            <ta e="T365" id="Seg_6569" s="T364">pro:O</ta>
            <ta e="T366" id="Seg_6570" s="T365">0.3.h:S v:pred</ta>
            <ta e="T368" id="Seg_6571" s="T367">np:O</ta>
            <ta e="T371" id="Seg_6572" s="T370">0.3.h:S v:pred</ta>
            <ta e="T372" id="Seg_6573" s="T371">np.h:S</ta>
            <ta e="T375" id="Seg_6574" s="T374">ptcl:pred</ta>
            <ta e="T377" id="Seg_6575" s="T376">0.2.h:S pro:pred</ta>
            <ta e="T380" id="Seg_6576" s="T379">0.1.h:S ptcl:pred</ta>
            <ta e="T381" id="Seg_6577" s="T380">v:pred</ta>
            <ta e="T382" id="Seg_6578" s="T381">np.h:S</ta>
            <ta e="T385" id="Seg_6579" s="T383">0.3.h:S v:pred</ta>
            <ta e="T386" id="Seg_6580" s="T385">np.h:S</ta>
            <ta e="T387" id="Seg_6581" s="T386">ptcl:pred</ta>
            <ta e="T389" id="Seg_6582" s="T388">0.2.h:S pro:pred</ta>
            <ta e="T391" id="Seg_6583" s="T390">0.1.h:S ptcl:pred</ta>
            <ta e="T393" id="Seg_6584" s="T392">0.3.h:S v:pred</ta>
            <ta e="T394" id="Seg_6585" s="T393">np.h:S</ta>
            <ta e="T397" id="Seg_6586" s="T396">ptcl:pred</ta>
            <ta e="T400" id="Seg_6587" s="T399">0.2.h:S ptcl:pred</ta>
            <ta e="T401" id="Seg_6588" s="T400">0.3.h:S v:pred</ta>
            <ta e="T405" id="Seg_6589" s="T404">0.1.h:S ptcl:pred</ta>
            <ta e="T406" id="Seg_6590" s="T405">0.3.h:S v:pred</ta>
            <ta e="T407" id="Seg_6591" s="T406">pro:O</ta>
            <ta e="T408" id="Seg_6592" s="T407">np.h:S</ta>
            <ta e="T409" id="Seg_6593" s="T408">v:pred</ta>
            <ta e="T413" id="Seg_6594" s="T412">0.3.h:S v:pred</ta>
            <ta e="T415" id="Seg_6595" s="T414">np.h:S</ta>
            <ta e="T416" id="Seg_6596" s="T415">v:pred</ta>
            <ta e="T419" id="Seg_6597" s="T418">0.2.h:S v:pred</ta>
            <ta e="T421" id="Seg_6598" s="T419">0.2.h:S v:pred</ta>
            <ta e="T423" id="Seg_6599" s="T422">0.2.h:S v:pred</ta>
            <ta e="T424" id="Seg_6600" s="T423">np.h:S</ta>
            <ta e="T426" id="Seg_6601" s="T425">v:pred</ta>
            <ta e="T429" id="Seg_6602" s="T426">s:temp</ta>
            <ta e="T430" id="Seg_6603" s="T429">np.h:S</ta>
            <ta e="T436" id="Seg_6604" s="T435">v:pred</ta>
            <ta e="T437" id="Seg_6605" s="T436">np.h:S</ta>
            <ta e="T439" id="Seg_6606" s="T437">v:pred</ta>
            <ta e="T440" id="Seg_6607" s="T439">np.h:S</ta>
            <ta e="T441" id="Seg_6608" s="T440">v:pred</ta>
            <ta e="T445" id="Seg_6609" s="T443">0.3.h:S v:pred</ta>
            <ta e="T447" id="Seg_6610" s="T446">s:adv</ta>
            <ta e="T448" id="Seg_6611" s="T447">0.3.h:S v:pred</ta>
            <ta e="T450" id="Seg_6612" s="T449">0.3:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="TIE0">
            <ta e="T61" id="Seg_6613" s="T60">RUS:cult</ta>
            <ta e="T66" id="Seg_6614" s="T65">RUS:cult</ta>
            <ta e="T80" id="Seg_6615" s="T79">RUS:cult</ta>
            <ta e="T109" id="Seg_6616" s="T108">RUS:cult</ta>
            <ta e="T127" id="Seg_6617" s="T126">RUS:cult</ta>
            <ta e="T163" id="Seg_6618" s="T162">RUS:cult</ta>
            <ta e="T171" id="Seg_6619" s="T170">EV:cult</ta>
            <ta e="T227" id="Seg_6620" s="T226">RUS:cult</ta>
            <ta e="T245" id="Seg_6621" s="T244">EV:cult</ta>
            <ta e="T268" id="Seg_6622" s="T267">EV:gram (INTNS)</ta>
            <ta e="T276" id="Seg_6623" s="T275">RUS:cult</ta>
            <ta e="T403" id="Seg_6624" s="T402">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="TIE1">
            <ta e="T61" id="Seg_6625" s="T60">Vsub</ta>
            <ta e="T171" id="Seg_6626" s="T170">Vsub Vsub</ta>
            <ta e="T227" id="Seg_6627" s="T226">Vsub Vsub</ta>
            <ta e="T245" id="Seg_6628" s="T244">Csub lenition</ta>
            <ta e="T276" id="Seg_6629" s="T275">Vsub Csub Vsub</ta>
            <ta e="T403" id="Seg_6630" s="T402">Vsub Csub Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="TIE2">
            <ta e="T61" id="Seg_6631" s="T60">dir:infl</ta>
            <ta e="T66" id="Seg_6632" s="T65">dir:infl</ta>
            <ta e="T80" id="Seg_6633" s="T79">dir:infl</ta>
            <ta e="T109" id="Seg_6634" s="T108">dir:infl</ta>
            <ta e="T127" id="Seg_6635" s="T126">dir:infl</ta>
            <ta e="T163" id="Seg_6636" s="T162">dir:infl</ta>
            <ta e="T171" id="Seg_6637" s="T170">dir:infl</ta>
            <ta e="T227" id="Seg_6638" s="T226">dir:bare</ta>
            <ta e="T245" id="Seg_6639" s="T244">dir:infl</ta>
            <ta e="T276" id="Seg_6640" s="T275">dir:bare</ta>
            <ta e="T403" id="Seg_6641" s="T402">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="TIE4" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_6642" s="T0">Lyybyra rowed swaying smoothly.</ta>
            <ta e="T11" id="Seg_6643" s="T5">While going so he stumbled against a stone, his small boat got stuck.</ta>
            <ta e="T16" id="Seg_6644" s="T11">He beat with an oar, the oar got stuck.</ta>
            <ta e="T21" id="Seg_6645" s="T16">He beat with his hand, his hand got stuck.</ta>
            <ta e="T28" id="Seg_6646" s="T21">He beat with his other hand, his other hand got also stuck.</ta>
            <ta e="T39" id="Seg_6647" s="T28">He kicked with his foot, his foot got stuck; he kicked with his other foot, it got also stuck.</ta>
            <ta e="T45" id="Seg_6648" s="T39">He pushed with his belly, his belly also got stuck. </ta>
            <ta e="T52" id="Seg_6649" s="T45">Then he beat with his head, his head also got stuck.</ta>
            <ta e="T54" id="Seg_6650" s="T52">Having stuck [to it], he was standing there.</ta>
            <ta e="T58" id="Seg_6651" s="T54">As he was standing there, Angaa Mongus came.</ta>
            <ta e="T59" id="Seg_6652" s="T58">"What a joy!</ta>
            <ta e="T68" id="Seg_6653" s="T59">I got an evening snack!", he said, took Lyybyra and put him into his pocket.</ta>
            <ta e="T70" id="Seg_6654" s="T68">He went home.</ta>
            <ta e="T75" id="Seg_6655" s="T70">As he reached the path, he decided to ease himself.</ta>
            <ta e="T84" id="Seg_6656" s="T75">As he was sitting and was easing himself, Lyybyra fell through a hole out of the pocket.</ta>
            <ta e="T93" id="Seg_6657" s="T84">Angaa Mongus didn't notice anything, he pulled his trousers up and went home.</ta>
            <ta e="T100" id="Seg_6658" s="T93">In the yard he hung up his coat on the storage and went into the house.</ta>
            <ta e="T104" id="Seg_6659" s="T100">His old wife was sitting and sewing.</ta>
            <ta e="T107" id="Seg_6660" s="T104">"[My] old wife, I've brought you a gift.</ta>
            <ta e="T115" id="Seg_6661" s="T107">It is outside in the pocket of my coat, bring it in!", he said.</ta>
            <ta e="T123" id="Seg_6662" s="T115">Out of happiness the old woman devoured her needle and her thimble, she ran out. </ta>
            <ta e="T126" id="Seg_6663" s="T123">She didn't find anything and came in again:</ta>
            <ta e="T131" id="Seg_6664" s="T126">"There is nothing in your pocket!", she said.</ta>
            <ta e="T132" id="Seg_6665" s="T131">Angaa Mongus:</ta>
            <ta e="T141" id="Seg_6666" s="T132">"You ate [it] apparently yourself!", he said and split the old woman's belly.</ta>
            <ta e="T148" id="Seg_6667" s="T141">Tinkling the needle and the thimble fell out of the old woman's belly.</ta>
            <ta e="T153" id="Seg_6668" s="T148">"Oh, my dear woman!", Angaa Mongus shouted.</ta>
            <ta e="T155" id="Seg_6669" s="T153">He ran back.</ta>
            <ta e="T160" id="Seg_6670" s="T155">Lyybyra was lying on the path.</ta>
            <ta e="T164" id="Seg_6671" s="T160">He took him and put him into his pocket.</ta>
            <ta e="T167" id="Seg_6672" s="T164">He ran back.</ta>
            <ta e="T173" id="Seg_6673" s="T167">He went into the house and hung him up on the hook for the kettle.</ta>
            <ta e="T178" id="Seg_6674" s="T173">Angaa Mongus apparently had seven baldheaded children.</ta>
            <ta e="T181" id="Seg_6675" s="T178">He told his children:</ta>
            <ta e="T187" id="Seg_6676" s="T181">"Children, watch [when] Lyybyra's fat will start to drip.</ta>
            <ta e="T189" id="Seg_6677" s="T187">Get it cooked until I come back!"</ta>
            <ta e="T193" id="Seg_6678" s="T189">He said it and went out.</ta>
            <ta e="T199" id="Seg_6679" s="T193">After a while Lyybyra peed.</ta>
            <ta e="T202" id="Seg_6680" s="T199">The children shouted:</ta>
            <ta e="T203" id="Seg_6681" s="T202">"What a joy!</ta>
            <ta e="T207" id="Seg_6682" s="T203">Lyybyra's liquid fat is leaking out!"</ta>
            <ta e="T211" id="Seg_6683" s="T207">Then Lyybyra defecated.</ta>
            <ta e="T213" id="Seg_6684" s="T211">The children shouted:</ta>
            <ta e="T214" id="Seg_6685" s="T213">"What a joy!</ta>
            <ta e="T218" id="Seg_6686" s="T214">Lyybyra's viscous fat is leaking out!"</ta>
            <ta e="T221" id="Seg_6687" s="T218">Lyybyra said to the children:</ta>
            <ta e="T222" id="Seg_6688" s="T221">"Children!</ta>
            <ta e="T224" id="Seg_6689" s="T222">Let me down.</ta>
            <ta e="T234" id="Seg_6690" s="T224">I will make you spoons so that you can eat my fat well!"</ta>
            <ta e="T237" id="Seg_6691" s="T234">"Very good!"</ta>
            <ta e="T239" id="Seg_6692" s="T237">They let Lyybyra down.</ta>
            <ta e="T241" id="Seg_6693" s="T239">Lyybyra said:</ta>
            <ta e="T245" id="Seg_6694" s="T241">"Bring your father's sharp axe!"</ta>
            <ta e="T247" id="Seg_6695" s="T245">They got [it] and gave [it to him].</ta>
            <ta e="T253" id="Seg_6696" s="T247">"Lie down into your bed and watch how I will make it."</ta>
            <ta e="T256" id="Seg_6697" s="T253">The children laid down.</ta>
            <ta e="T262" id="Seg_6698" s="T256">As they had laid down, Lyybyra cut off their heads. </ta>
            <ta e="T265" id="Seg_6699" s="T262">He cooked their meat.</ta>
            <ta e="T272" id="Seg_6700" s="T265">He laid their heads in a row onto the bed and covered them with the blanket.</ta>
            <ta e="T279" id="Seg_6701" s="T272">He took a poker and crawled under the stove.</ta>
            <ta e="T283" id="Seg_6702" s="T279">As he was lying there, Angaa Mongus came.</ta>
            <ta e="T286" id="Seg_6703" s="T283">Angaa Mongus came and shouted:</ta>
            <ta e="T287" id="Seg_6704" s="T286">"Children!</ta>
            <ta e="T289" id="Seg_6705" s="T287">Open the door!"</ta>
            <ta e="T292" id="Seg_6706" s="T289">Nobody opened.</ta>
            <ta e="T295" id="Seg_6707" s="T292">He shouted several times.</ta>
            <ta e="T298" id="Seg_6708" s="T295">The children didn't open at all.</ta>
            <ta e="T302" id="Seg_6709" s="T298">He tore the door open and went in.</ta>
            <ta e="T309" id="Seg_6710" s="T302">He saw that the meat was boiling and that the children were sleeping.</ta>
            <ta e="T320" id="Seg_6711" s="T309">"Oh, my children ate their fill of Lyybyra's fat and fell asleep!", he said.</ta>
            <ta e="T322" id="Seg_6712" s="T320">He took the kettle down.</ta>
            <ta e="T326" id="Seg_6713" s="T322">He tasted the heart, his [own] heart skipped a beat.</ta>
            <ta e="T333" id="Seg_6714" s="T326">"Oh, Lyybyra is apparently my sibling?!", he said.</ta>
            <ta e="T337" id="Seg_6715" s="T333">He tasted the spleen, his [own] spleen tweaked.</ta>
            <ta e="T344" id="Seg_6716" s="T337">"Oh, Lyybyra is my real sibling?!", he said.</ta>
            <ta e="T346" id="Seg_6717" s="T344">Then he stood up.</ta>
            <ta e="T353" id="Seg_6718" s="T346">He pulled [the blanket] down from the children — the seven heads rolled.</ta>
            <ta e="T357" id="Seg_6719" s="T353">Out of fright Angaa Mongus shouted:</ta>
            <ta e="T358" id="Seg_6720" s="T357">"Lyybyra!</ta>
            <ta e="T360" id="Seg_6721" s="T358">Where are you?"</ta>
            <ta e="T361" id="Seg_6722" s="T360">Lyybyra:</ta>
            <ta e="T364" id="Seg_6723" s="T361">"I am outside!", he said.</ta>
            <ta e="T371" id="Seg_6724" s="T364">As he heard that, he flew outside through the wall of the house. </ta>
            <ta e="T375" id="Seg_6725" s="T371">Lyybyra was nowhere.</ta>
            <ta e="T377" id="Seg_6726" s="T375">"Lyybyra, where are you?"</ta>
            <ta e="T382" id="Seg_6727" s="T377">"I am in the house", Lyybyra said.</ta>
            <ta e="T387" id="Seg_6728" s="T382">He flew into the house, Lyybyra was not there.</ta>
            <ta e="T389" id="Seg_6729" s="T387">"Lyybyra, where are you?"</ta>
            <ta e="T391" id="Seg_6730" s="T389">"I am in the chimney!"</ta>
            <ta e="T397" id="Seg_6731" s="T391">He looked into the chimney, Lyybyra was nowhere.</ta>
            <ta e="T401" id="Seg_6732" s="T397">"Lyybyra, where are you?", he said.</ta>
            <ta e="T402" id="Seg_6733" s="T401">Lyybyra:</ta>
            <ta e="T406" id="Seg_6734" s="T402">"I am under the stove!", he said.</ta>
            <ta e="T413" id="Seg_6735" s="T406">Angaa Mongus heard this and put his head there.</ta>
            <ta e="T416" id="Seg_6736" s="T413">On that Lyybyra said:</ta>
            <ta e="T421" id="Seg_6737" s="T416">"Grandfather, don't put your head here in, you won't fit in.</ta>
            <ta e="T423" id="Seg_6738" s="T421">Go backwards!"</ta>
            <ta e="T426" id="Seg_6739" s="T423">Angaa Mongus went backwards inside.</ta>
            <ta e="T436" id="Seg_6740" s="T426">As he was going so, Lyybyra pushed the red hot poker into his bottom.</ta>
            <ta e="T439" id="Seg_6741" s="T436">Angyy Mongus died.</ta>
            <ta e="T445" id="Seg_6742" s="T439">Lyybyra went out and went home.</ta>
            <ta e="T448" id="Seg_6743" s="T445">And he lived happily and satisfied.</ta>
            <ta e="T450" id="Seg_6744" s="T448">This is the end.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_6745" s="T0">Lyybyra ruderte und schaukelte sanft.</ta>
            <ta e="T11" id="Seg_6746" s="T5">So fahrend stieß er an einen Stein, sein kleines Boot blieb kleben.</ta>
            <ta e="T16" id="Seg_6747" s="T11">Er schlug mit einem Ruder, das Ruder blieb kleben.</ta>
            <ta e="T21" id="Seg_6748" s="T16">Er schlug mit der Hand, die Hand blieb kleben.</ta>
            <ta e="T28" id="Seg_6749" s="T21">Er schlug mit der anderen Hand, die andere Hand blieb auch kleben.</ta>
            <ta e="T39" id="Seg_6750" s="T28">Er trat mit dem Fuß, der Fuß blieb kleben; er trat mit dem nächsten Fuß, der blieb auch kleben.</ta>
            <ta e="T45" id="Seg_6751" s="T39">Er stieß mit dem Bauch, der Bauch blieb auch kleben.</ta>
            <ta e="T52" id="Seg_6752" s="T45">Dann schlug er mit dem Kopf, der Kopf blieb auch kleben.</ta>
            <ta e="T54" id="Seg_6753" s="T52">So stand er festgeklebt da.</ta>
            <ta e="T58" id="Seg_6754" s="T54">Als er da stand, kam Angaa Mongus.</ta>
            <ta e="T59" id="Seg_6755" s="T58">"Welch Freude!</ta>
            <ta e="T68" id="Seg_6756" s="T59">Ich habe einen Abendimbiss!", sagte er, nahm Lyybyra und steckte ihn in seine Tasche.</ta>
            <ta e="T70" id="Seg_6757" s="T68">Er ging nach Hause.</ta>
            <ta e="T75" id="Seg_6758" s="T70">Als er an den Pfad kam, wollte Angaa Mongus sich erleichtern.</ta>
            <ta e="T84" id="Seg_6759" s="T75">Als er saß und sich erleichterte, fiel Lyybyra durch ein Loch in der Tasche hinaus.</ta>
            <ta e="T93" id="Seg_6760" s="T84">Angaa Mongus bemerkte nichts, zog sich die Hose hoch und ging nach Hause.</ta>
            <ta e="T100" id="Seg_6761" s="T93">Auf dem Hof hängte er seinen Mantel auf den Speicher und ging ins Haus.</ta>
            <ta e="T104" id="Seg_6762" s="T100">Seine Alte saß da und nähte.</ta>
            <ta e="T107" id="Seg_6763" s="T104">"Alte, ich habe ein Geschenk mitgebracht.</ta>
            <ta e="T115" id="Seg_6764" s="T107">Es ist draußen in der Tasche meines Mantels, bring es rein!", sagte er.</ta>
            <ta e="T123" id="Seg_6765" s="T115">Die Alte verschluckte vor Freude ihre Nadel und ihren Fingerhut, sie rannte hinaus.</ta>
            <ta e="T126" id="Seg_6766" s="T123">Sie fand nichts und kam wieder herein:</ta>
            <ta e="T131" id="Seg_6767" s="T126">"In deiner Tasche ist nichts!", sagte sie.</ta>
            <ta e="T132" id="Seg_6768" s="T131">Angaa Mongus:</ta>
            <ta e="T141" id="Seg_6769" s="T132">"Du hast [es] wohl selbst gegessen!", sagte er und schlitzte den Bauch der Alten auf.</ta>
            <ta e="T148" id="Seg_6770" s="T141">Aus dem Bauch der Alten fielen mit einem Klirren Nadel und Fingerhut heraus.</ta>
            <ta e="T153" id="Seg_6771" s="T148">"Oh, meine liebe Frau!", schrie Angaa Mongus.</ta>
            <ta e="T155" id="Seg_6772" s="T153">Er lief zurück.</ta>
            <ta e="T160" id="Seg_6773" s="T155">Lyybyra lag auf dem Pfad.</ta>
            <ta e="T164" id="Seg_6774" s="T160">Er nahm ihn und steckte ihn in die Tasche.</ta>
            <ta e="T167" id="Seg_6775" s="T164">Er lief zurück.</ta>
            <ta e="T173" id="Seg_6776" s="T167">Er ging ins Haus hinein und hängte Lyybyra an den Kesselhaken.</ta>
            <ta e="T178" id="Seg_6777" s="T173">Angaa Mongus hatte offenbar sieben glatzköpfige Kinder. </ta>
            <ta e="T181" id="Seg_6778" s="T178">Zu diesen seinen Kindern sagte er:</ta>
            <ta e="T187" id="Seg_6779" s="T181">"Kinder, das Fett von Lyybyra wird tropfen, achtet darauf.</ta>
            <ta e="T189" id="Seg_6780" s="T187">Habt es gekocht, bis ich wieder komme!".</ta>
            <ta e="T193" id="Seg_6781" s="T189">Er sagte es und ging hinaus.</ta>
            <ta e="T199" id="Seg_6782" s="T193">Es verging eine Weile, da pinkelte Lyybyra.</ta>
            <ta e="T202" id="Seg_6783" s="T199">Da schrieen die Kinder:</ta>
            <ta e="T203" id="Seg_6784" s="T202">"Welch Freude!</ta>
            <ta e="T207" id="Seg_6785" s="T203">Das flüssige Fett von Lyybyra läuft aus!"</ta>
            <ta e="T211" id="Seg_6786" s="T207">Dann entleerte sich Lyybyra.</ta>
            <ta e="T213" id="Seg_6787" s="T211">Die Kinder schrieen:</ta>
            <ta e="T214" id="Seg_6788" s="T213">"Welch Freude!</ta>
            <ta e="T218" id="Seg_6789" s="T214">Das dickflüssige Fett von Lyybyra läuft aus!"</ta>
            <ta e="T221" id="Seg_6790" s="T218">Lyybyra sagte zu den Kindern:</ta>
            <ta e="T222" id="Seg_6791" s="T221">"Kinder!</ta>
            <ta e="T224" id="Seg_6792" s="T222">Lasst mich hinunter.</ta>
            <ta e="T234" id="Seg_6793" s="T224">Ich mache euch Löffel, damit ihr mein Fett gut essen könnt!"</ta>
            <ta e="T237" id="Seg_6794" s="T234">"Sehr gut!"</ta>
            <ta e="T239" id="Seg_6795" s="T237">Sie ließen Lyybyra herunter.</ta>
            <ta e="T241" id="Seg_6796" s="T239">Lyybyra sagte:</ta>
            <ta e="T245" id="Seg_6797" s="T241">"Bringt das scharfe Beil eures Vaters!"</ta>
            <ta e="T247" id="Seg_6798" s="T245">Sie holten und gaben [es ihm].</ta>
            <ta e="T253" id="Seg_6799" s="T247">"Legt euch in euer Bett und schaut, wie ich es mache."</ta>
            <ta e="T256" id="Seg_6800" s="T253">Die Kinder legten sich hin.</ta>
            <ta e="T262" id="Seg_6801" s="T256">Als sie sich hingelegt hatten, schlug Lyybyra ihnen den Kopf ab.</ta>
            <ta e="T265" id="Seg_6802" s="T262">Ihr Fleisch kochte er.</ta>
            <ta e="T272" id="Seg_6803" s="T265">Die Köpfe legte er in einer Reihe aufs Bett und bedeckte sie mit der Bettdecke.</ta>
            <ta e="T279" id="Seg_6804" s="T272">Er nahm den Schürhaken und kroch unter den Herd.</ta>
            <ta e="T283" id="Seg_6805" s="T279">Als er dort lag, kam Angaa Mongus.</ta>
            <ta e="T286" id="Seg_6806" s="T283">Angaa Mongus kam und schrie:</ta>
            <ta e="T287" id="Seg_6807" s="T286">"Kinder!</ta>
            <ta e="T289" id="Seg_6808" s="T287">Öffnet die Tür!"</ta>
            <ta e="T292" id="Seg_6809" s="T289">Niemand öffnete.</ta>
            <ta e="T295" id="Seg_6810" s="T292">Er schrie einige Male.</ta>
            <ta e="T298" id="Seg_6811" s="T295">Die Kinder machten überhaupt nicht auf.</ta>
            <ta e="T302" id="Seg_6812" s="T298">Er riss die Tür auf und ging hinein.</ta>
            <ta e="T309" id="Seg_6813" s="T302">Er sah, dass das Fleisch kochte und dass die Kinder schliefen.</ta>
            <ta e="T320" id="Seg_6814" s="T309">"Oh, meine Kinder haben sich an Lyybyras Fett satt gegessen, sie schlafen offenbar!", sagte er.</ta>
            <ta e="T322" id="Seg_6815" s="T320">Er nahm den Kessel herunter.</ta>
            <ta e="T326" id="Seg_6816" s="T322">Er probierte vom Herz, sein Herz machte einen Sprung.</ta>
            <ta e="T333" id="Seg_6817" s="T326">"Oh, Lyybyra ist wohl mein Verwandter?!", sagte er.</ta>
            <ta e="T337" id="Seg_6818" s="T333">Er aß von der Milz, seine Milz zwickte.</ta>
            <ta e="T344" id="Seg_6819" s="T337">"Oh, Lyybyra ist wohl ein echter Verwandter von mir?!", sagte er.</ta>
            <ta e="T346" id="Seg_6820" s="T344">Dann stand er auf.</ta>
            <ta e="T353" id="Seg_6821" s="T346">Er zog [die Decke] von den Kindern; die sieben Köpfe rollten.</ta>
            <ta e="T357" id="Seg_6822" s="T353">Vor Schreck schrie Angaa Mongus:</ta>
            <ta e="T358" id="Seg_6823" s="T357">"Lyybyra!</ta>
            <ta e="T360" id="Seg_6824" s="T358">Wo bist du?"</ta>
            <ta e="T361" id="Seg_6825" s="T360">Lyybyra:</ta>
            <ta e="T364" id="Seg_6826" s="T361">"Ich bin draußen!", sagte er.</ta>
            <ta e="T371" id="Seg_6827" s="T364">Als er das hörte, flog er durch die Wand des Hauses hinaus.</ta>
            <ta e="T375" id="Seg_6828" s="T371">Lyybyra war nirgendwo.</ta>
            <ta e="T377" id="Seg_6829" s="T375">"Lyybyra, wo bist du?"</ta>
            <ta e="T382" id="Seg_6830" s="T377">"Ich bin im Haus!", sagte Lyybyra.</ta>
            <ta e="T387" id="Seg_6831" s="T382">Er flog ins Haus hinein, Lyybyra war nicht da.</ta>
            <ta e="T389" id="Seg_6832" s="T387">"Lyybyra, wo bist du?"</ta>
            <ta e="T391" id="Seg_6833" s="T389">"Ich bin im Rauchloch!"</ta>
            <ta e="T397" id="Seg_6834" s="T391">Er schaute in das Rauchloch, Lyybyra war nirgendwo.</ta>
            <ta e="T401" id="Seg_6835" s="T397">"Lyybyra, wo bist du?", sagte er.</ta>
            <ta e="T402" id="Seg_6836" s="T401">Lyybyra:</ta>
            <ta e="T406" id="Seg_6837" s="T402">"Ich bin unter dem Herd!", sagte er.</ta>
            <ta e="T413" id="Seg_6838" s="T406">Das hörte Angaa Mongus und steckte seinen Kopf dorthin.</ta>
            <ta e="T416" id="Seg_6839" s="T413">Darauf sagte Lyybyra:</ta>
            <ta e="T421" id="Seg_6840" s="T416">"Opa, steck den Kopf hier nicht hin, du passt nicht rein.</ta>
            <ta e="T423" id="Seg_6841" s="T421">Geh mit dem Hintern voran."</ta>
            <ta e="T426" id="Seg_6842" s="T423">Angaa Mongus ging mit dem Hintern voran.</ta>
            <ta e="T436" id="Seg_6843" s="T426">Als er so hineinkroch, stieß Lyybyra ihm den rot gewordenen Schürhaken in den Hintern hinein.</ta>
            <ta e="T439" id="Seg_6844" s="T436">Angaa Mongus starb.</ta>
            <ta e="T445" id="Seg_6845" s="T439">Lyybyra ging hinaus und ging nach Hause.</ta>
            <ta e="T448" id="Seg_6846" s="T445">Und er lebte reich und zufrieden.</ta>
            <ta e="T450" id="Seg_6847" s="T448">Da ist es zuende.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T5" id="Seg_6848" s="T0">Вот Лыыбыра греб, плавно покачиваясь.</ta>
            <ta e="T11" id="Seg_6849" s="T5">Так плывя, наскочил на камень, лодочка прилипла.</ta>
            <ta e="T16" id="Seg_6850" s="T11">Ударил веслом — весло прилипло.</ta>
            <ta e="T21" id="Seg_6851" s="T16">Ударил рукой — рука прилипла.</ta>
            <ta e="T28" id="Seg_6852" s="T21">Ударил другой рукой — другая тоже прилипла.</ta>
            <ta e="T39" id="Seg_6853" s="T28">Пнул ногой — нога прилипла, пнул другой ногой — тоже прилипла.</ta>
            <ta e="T45" id="Seg_6854" s="T39">Толкнул животом — живот тоже прилип.</ta>
            <ta e="T52" id="Seg_6855" s="T45">Потом головой ударил — голова тоже прилипла.</ta>
            <ta e="T54" id="Seg_6856" s="T52">Вот так прилипнув, стоял.</ta>
            <ta e="T58" id="Seg_6857" s="T54">Когда стоял, подошел Ангаа Монгус.</ta>
            <ta e="T59" id="Seg_6858" s="T58">— Вот радость-то!</ta>
            <ta e="T68" id="Seg_6859" s="T59">Вечерняя закуска готова! — сказав, взял Лыыбыру и положил в карман.</ta>
            <ta e="T70" id="Seg_6860" s="T68">Пошел домой.</ta>
            <ta e="T75" id="Seg_6861" s="T70">Выйдя на тропку, Ангаа Монгус захотел оправиться.</ta>
            <ta e="T84" id="Seg_6862" s="T75">Когда сидел и оправлялся, Лыыбыра через дырку в кармане выпал.</ta>
            <ta e="T93" id="Seg_6863" s="T84">Ангаа Монгус ничего не заметил, подтянул штаны и пошел домой.</ta>
            <ta e="T100" id="Seg_6864" s="T93">Во дворе бросил шубу на лабаз и вошел в дом.</ta>
            <ta e="T104" id="Seg_6865" s="T100">Старуха его, оказывается, сидела и шила.</ta>
            <ta e="T107" id="Seg_6866" s="T104">— Старуха, гостинец принес.</ta>
            <ta e="T115" id="Seg_6867" s="T107">Оставил во дворе в кармане шубы, внеси! — сказал.</ta>
            <ta e="T123" id="Seg_6868" s="T115">Старуха от радости проглотила иголку с наперстком и выскочила.</ta>
            <ta e="T126" id="Seg_6869" s="T123">Не найдя ничего, обратно вошла:</ta>
            <ta e="T131" id="Seg_6870" s="T126">— В кармане твоем ничего же нет! — сказала.</ta>
            <ta e="T132" id="Seg_6871" s="T131">Ангаа Монгус:</ta>
            <ta e="T141" id="Seg_6872" s="T132">— Сама небось съела! — и вспорол живот старухи.</ta>
            <ta e="T148" id="Seg_6873" s="T141">Из живота старухи со звоном выпали только наперсток с иголкой.</ta>
            <ta e="T153" id="Seg_6874" s="T148">— О-о, жена моя дорогая! — закричал Ангаа Монгус.</ta>
            <ta e="T155" id="Seg_6875" s="T153">Побежал обратно.</ta>
            <ta e="T160" id="Seg_6876" s="T155">Лыыбыра на тропке лежит.</ta>
            <ta e="T164" id="Seg_6877" s="T160">Взял и в карман положил.</ta>
            <ta e="T167" id="Seg_6878" s="T164">Обратно побежал.</ta>
            <ta e="T173" id="Seg_6879" s="T167">Вошел в дом, Лыыбыру повесил на крюк [для котлов].</ta>
            <ta e="T178" id="Seg_6880" s="T173">Ангаа Монгус имел семерых плешивых детей, оказывается.</ta>
            <ta e="T181" id="Seg_6881" s="T178">Тем своим детям сказал:</ta>
            <ta e="T187" id="Seg_6882" s="T181">— Дети, следите, [когда] закапает сало Лыыбыры.</ta>
            <ta e="T189" id="Seg_6883" s="T187">К моему приходу сварите [его]!</ta>
            <ta e="T193" id="Seg_6884" s="T189">Сказал и сразу вышел.</ta>
            <ta e="T199" id="Seg_6885" s="T193">Немного погодя, Лыыбыра помочился.</ta>
            <ta e="T202" id="Seg_6886" s="T199">Тут дети закричали:</ta>
            <ta e="T203" id="Seg_6887" s="T202">— Радость-то какая!</ta>
            <ta e="T207" id="Seg_6888" s="T203">Жидкое сало Лыыбыры закапало!</ta>
            <ta e="T211" id="Seg_6889" s="T207">Вот потом Лыыбыра оправляется.</ta>
            <ta e="T213" id="Seg_6890" s="T211">Дети закричали:</ta>
            <ta e="T214" id="Seg_6891" s="T213">— Радость-то какая!</ta>
            <ta e="T218" id="Seg_6892" s="T214">Густое сало Лыыбыры вываливается!</ta>
            <ta e="T221" id="Seg_6893" s="T218">Лыыбыра сказал детям:</ta>
            <ta e="T222" id="Seg_6894" s="T221">— Дети!</ta>
            <ta e="T224" id="Seg_6895" s="T222">Спустите меня.</ta>
            <ta e="T234" id="Seg_6896" s="T224">Я сделаю вам ложки, чтоб удобно было вам черпать мое сало!</ta>
            <ta e="T237" id="Seg_6897" s="T234">— Очень хорошо!</ta>
            <ta e="T239" id="Seg_6898" s="T237">— и спустили Лыыбыру.</ta>
            <ta e="T241" id="Seg_6899" s="T239">Лыыбыра сказал:</ta>
            <ta e="T245" id="Seg_6900" s="T241">— Подайте тятин острый топор!</ta>
            <ta e="T247" id="Seg_6901" s="T245">Принесли [его] и дали [ему].</ta>
            <ta e="T253" id="Seg_6902" s="T247">— Ложитесь в свою постель и смотрите, как буду делать.</ta>
            <ta e="T256" id="Seg_6903" s="T253">Дети легли.</ta>
            <ta e="T262" id="Seg_6904" s="T256">Как только улеглись, Лыыбыра разом отрубил им головы.</ta>
            <ta e="T265" id="Seg_6905" s="T262">Мясо их сварил.</ta>
            <ta e="T272" id="Seg_6906" s="T265">Головы в ряд уложил на постели и накрыл одеялом.</ta>
            <ta e="T279" id="Seg_6907" s="T272">Взял кочергу-ожиг и залез под очаг.</ta>
            <ta e="T283" id="Seg_6908" s="T279">Когда там лежал, пришел Ангаа Монгус.</ta>
            <ta e="T286" id="Seg_6909" s="T283">Ангаа Монгус пришел и крикнул:</ta>
            <ta e="T287" id="Seg_6910" s="T286">— Дети!</ta>
            <ta e="T289" id="Seg_6911" s="T287">Откройте дверь!</ta>
            <ta e="T292" id="Seg_6912" s="T289">Никто не открыл.</ta>
            <ta e="T295" id="Seg_6913" s="T292">Несколько раз крикнул.</ta>
            <ta e="T298" id="Seg_6914" s="T295">Дети все не открывали.</ta>
            <ta e="T302" id="Seg_6915" s="T298">Взломав дверь, вошел.</ta>
            <ta e="T309" id="Seg_6916" s="T302">Увидел, что мясо варится, дети спят.</ta>
            <ta e="T320" id="Seg_6917" s="T309">— О-о, дети мои, сала Лыыбыры наевшись, спят, оказывается! — сказал.</ta>
            <ta e="T322" id="Seg_6918" s="T320">Снимает котел.</ta>
            <ta e="T326" id="Seg_6919" s="T322">Отведал сердца — екнуло сердце.</ta>
            <ta e="T333" id="Seg_6920" s="T326">— О-о Лыыбыра, оказывается, мой родственник! — сказал.</ta>
            <ta e="T337" id="Seg_6921" s="T333">Отведал селезенки — передернуло селезенку.</ta>
            <ta e="T344" id="Seg_6922" s="T337">— О, Лыыбыра, оказывается, мой настоящий родич! — сказал.</ta>
            <ta e="T346" id="Seg_6923" s="T344">Потом встал.</ta>
            <ta e="T353" id="Seg_6924" s="T346">Одеяло с детей сдернул — семь голов покатились.</ta>
            <ta e="T357" id="Seg_6925" s="T353">От неожиданности Ангаа Монгус вскрикнул.</ta>
            <ta e="T358" id="Seg_6926" s="T357"> — Лыыбыра!</ta>
            <ta e="T360" id="Seg_6927" s="T358">Где ты?</ta>
            <ta e="T361" id="Seg_6928" s="T360">Лыыбыра:</ta>
            <ta e="T364" id="Seg_6929" s="T361">— Я во дворе! — сказал.</ta>
            <ta e="T371" id="Seg_6930" s="T364">Как услышал это, пошел напролом через стену.</ta>
            <ta e="T375" id="Seg_6931" s="T371">Лыыбыры нигде нет.</ta>
            <ta e="T377" id="Seg_6932" s="T375">— Лыыбыра, где ты?</ta>
            <ta e="T382" id="Seg_6933" s="T377">— Я в доме! — говорит Лыыбыра.</ta>
            <ta e="T387" id="Seg_6934" s="T382">Вломился в дом — Лыыбыры нет.</ta>
            <ta e="T389" id="Seg_6935" s="T387">— Лыыбыры, где ты?</ta>
            <ta e="T391" id="Seg_6936" s="T389">— Я в дымоходе!</ta>
            <ta e="T397" id="Seg_6937" s="T391">Заглянул в дымоход — Лыыбыры нигде нет.</ta>
            <ta e="T401" id="Seg_6938" s="T397">— Лыыбыра, где ты? — закричал.</ta>
            <ta e="T402" id="Seg_6939" s="T401">Лыыбыра:</ta>
            <ta e="T406" id="Seg_6940" s="T402">— Я под очагом! — сказал.</ta>
            <ta e="T413" id="Seg_6941" s="T406">Как услышал Ангаа Монгус, стал туда просовывать голову.</ta>
            <ta e="T416" id="Seg_6942" s="T413">На это сказал Лыыбыра:</ta>
            <ta e="T421" id="Seg_6943" s="T416">— Дедушка, голову не суй — не влезешь.</ta>
            <ta e="T423" id="Seg_6944" s="T421">Задом иди.</ta>
            <ta e="T426" id="Seg_6945" s="T423">Ангаа Монгус задом полез.</ta>
            <ta e="T436" id="Seg_6946" s="T426">Когда так стал влезать, Лыыбыра раскаленную докрасна кочергу-ожиг в задний проход [ему] всадил.</ta>
            <ta e="T439" id="Seg_6947" s="T436">Ангаа Монгус испустил дух.</ta>
            <ta e="T445" id="Seg_6948" s="T439">Лыыбыра вышел и отправился домой.</ta>
            <ta e="T448" id="Seg_6949" s="T445">Так и жил себе богато и сыто.</ta>
            <ta e="T450" id="Seg_6950" s="T448">Вот и все.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_6951" s="T0">Вот Лыыбыра гребет, плавно покачиваясь.</ta>
            <ta e="T11" id="Seg_6952" s="T5">Так плывя, наскочил на камень, лодочка прилипла.</ta>
            <ta e="T16" id="Seg_6953" s="T11">Ударил веслом — весло прилипло.</ta>
            <ta e="T21" id="Seg_6954" s="T16">Ударил рукой — рука прилипла.</ta>
            <ta e="T28" id="Seg_6955" s="T21">Ударил другой рукой — другая тоже прилипла.</ta>
            <ta e="T39" id="Seg_6956" s="T28">Пнул ногой — нога прилипла, пнул другой ногой — тоже прилипла.</ta>
            <ta e="T45" id="Seg_6957" s="T39">Толкнул животом — живот тоже прилип.</ta>
            <ta e="T52" id="Seg_6958" s="T45">Потом головой ударил — голова тоже прилипла.</ta>
            <ta e="T54" id="Seg_6959" s="T52">Вот так прилипнув, стоит.</ta>
            <ta e="T58" id="Seg_6960" s="T54">Когда так стоял, подошел Ангаа Монгус.</ta>
            <ta e="T59" id="Seg_6961" s="T58">— Вот радость-то!</ta>
            <ta e="T68" id="Seg_6962" s="T59">Вечерняя закуска готова! — сказав, взял Лыыбыру и положил в карман.</ta>
            <ta e="T70" id="Seg_6963" s="T68">Пошел домой.</ta>
            <ta e="T75" id="Seg_6964" s="T70">Выйдя на тропку, Ангаа Монгус захотел оправиться.</ta>
            <ta e="T84" id="Seg_6965" s="T75">Когда присел, Лыыбыра через дырку в кармане выпал.</ta>
            <ta e="T93" id="Seg_6966" s="T84">Ангаа Монгус ничего не заметил, подтянул штаны и пошел домой.</ta>
            <ta e="T100" id="Seg_6967" s="T93">Во дворе бросил шубу на лабаз и вошел в дом.</ta>
            <ta e="T104" id="Seg_6968" s="T100">Старуха его, оказывается, сидела и шила.</ta>
            <ta e="T107" id="Seg_6969" s="T104">— Старуха, гостинец принес.</ta>
            <ta e="T115" id="Seg_6970" s="T107">Оставил во дворе в кармане шубы, поди возьми! — сказал.</ta>
            <ta e="T123" id="Seg_6971" s="T115">Старуха от радости проглотила иголку с наперстком и выскочила.</ta>
            <ta e="T126" id="Seg_6972" s="T123">Не найдя ничего, обратно вошла:</ta>
            <ta e="T131" id="Seg_6973" s="T126">— В кармане твоем ничего же нет! — сказала.</ta>
            <ta e="T132" id="Seg_6974" s="T131">Ангаа Монгус воскликнул:</ta>
            <ta e="T141" id="Seg_6975" s="T132">— Сама небось съела! — и вспорол живот старухи.</ta>
            <ta e="T148" id="Seg_6976" s="T141">Из живота старухи со звоном выпали только наперсток с иголкой.</ta>
            <ta e="T153" id="Seg_6977" s="T148">— О-о, жена моя дорогая! — закричал Ангаа Монгус.</ta>
            <ta e="T155" id="Seg_6978" s="T153">Побежал обратно.</ta>
            <ta e="T160" id="Seg_6979" s="T155">Лыыбыра на тропке лежит.</ta>
            <ta e="T164" id="Seg_6980" s="T160">Взял и в карман положил.</ta>
            <ta e="T167" id="Seg_6981" s="T164">Обратно побежал.</ta>
            <ta e="T173" id="Seg_6982" s="T167">Вошел в дом, Лыыбыру повесил на крюк [для котлов].</ta>
            <ta e="T178" id="Seg_6983" s="T173">Ангаа Монгус имел семерых плешивых детей, оказывается.</ta>
            <ta e="T181" id="Seg_6984" s="T178">Тем своим детям сказал:</ta>
            <ta e="T187" id="Seg_6985" s="T181">— Дети, следите, [когда] закапает сало Лыыбыры.</ta>
            <ta e="T189" id="Seg_6986" s="T187">К моему приходу сварите [его]!</ta>
            <ta e="T193" id="Seg_6987" s="T189">Сказал и сразу вышел.</ta>
            <ta e="T199" id="Seg_6988" s="T193">Немного погодя, Лыыбыра справил малую нужду.</ta>
            <ta e="T202" id="Seg_6989" s="T199">Тут дети закричали:</ta>
            <ta e="T203" id="Seg_6990" s="T202">— Радость-то какая!</ta>
            <ta e="T207" id="Seg_6991" s="T203">Жидкое сало Лыыбыры закапало!</ta>
            <ta e="T211" id="Seg_6992" s="T207">Вот потом Лыыбыра оправляется.</ta>
            <ta e="T213" id="Seg_6993" s="T211">Дети закричали:</ta>
            <ta e="T214" id="Seg_6994" s="T213">— Радость-то какая!</ta>
            <ta e="T218" id="Seg_6995" s="T214">Густое сало Лыыбыры вываливается!</ta>
            <ta e="T221" id="Seg_6996" s="T218">Лыыбыра сказал детям:</ta>
            <ta e="T222" id="Seg_6997" s="T221">— Дети!</ta>
            <ta e="T224" id="Seg_6998" s="T222">Спустите меня.</ta>
            <ta e="T234" id="Seg_6999" s="T224">Я сделаю вам ложки, чтоб удобно было вам черпать мое сало!</ta>
            <ta e="T237" id="Seg_7000" s="T234">— Очень хорошо!</ta>
            <ta e="T239" id="Seg_7001" s="T237">— и спустили Лыыбыру.</ta>
            <ta e="T241" id="Seg_7002" s="T239">Лыыбыра сказал:</ta>
            <ta e="T245" id="Seg_7003" s="T241">— Подайте тятин острый топор!</ta>
            <ta e="T247" id="Seg_7004" s="T245">Подали.</ta>
            <ta e="T253" id="Seg_7005" s="T247">— Ложитесь в свою постель и смотрите, как буду делать.</ta>
            <ta e="T256" id="Seg_7006" s="T253">Дети легли.</ta>
            <ta e="T262" id="Seg_7007" s="T256">Как только улеглись, Лыыбыра разом отрубил им головы.</ta>
            <ta e="T265" id="Seg_7008" s="T262">Мясо их сварил.</ta>
            <ta e="T272" id="Seg_7009" s="T265">Головы в ряд уложил на постели и накрыл одеялом.</ta>
            <ta e="T279" id="Seg_7010" s="T272">Взял кочергу-ожиг и залез под очаг.</ta>
            <ta e="T283" id="Seg_7011" s="T279">Когда там лежал, пришел Ангаа Монгус.</ta>
            <ta e="T286" id="Seg_7012" s="T283">Ангаа Монгус крикнул:</ta>
            <ta e="T287" id="Seg_7013" s="T286">— Дети!</ta>
            <ta e="T289" id="Seg_7014" s="T287">Откройте дверь!</ta>
            <ta e="T292" id="Seg_7015" s="T289">Никто не открывает.</ta>
            <ta e="T295" id="Seg_7016" s="T292">Несколько раз крикнул.</ta>
            <ta e="T298" id="Seg_7017" s="T295">Дети все не открывают.</ta>
            <ta e="T302" id="Seg_7018" s="T298">Взломав дверь, вошел.</ta>
            <ta e="T309" id="Seg_7019" s="T302">Смотрит, мясо варится, дети спят.</ta>
            <ta e="T320" id="Seg_7020" s="T309">— О-о, дети мои, сала Лыыбыры наевшись, спят, оказывается! — сказал.</ta>
            <ta e="T322" id="Seg_7021" s="T320">Снимает котел.</ta>
            <ta e="T326" id="Seg_7022" s="T322">Отведал сердца — екнуло сердце.</ta>
            <ta e="T333" id="Seg_7023" s="T326">— О-о Лыыбыра, оказывается, мой родственник! — сказал.</ta>
            <ta e="T337" id="Seg_7024" s="T333">Отведал селезенки — передернуло селезенку.</ta>
            <ta e="T344" id="Seg_7025" s="T337">— О, Лыыбыра, оказывается, мой настоящий родич! — сказал.</ta>
            <ta e="T346" id="Seg_7026" s="T344">Потом встал.</ta>
            <ta e="T353" id="Seg_7027" s="T346">Одеяло с детей сдернул — семь голов покатились.</ta>
            <ta e="T357" id="Seg_7028" s="T353">От неожиданности Ангаа Монгус вскрикнул.</ta>
            <ta e="T358" id="Seg_7029" s="T357">— Лыыбыра!</ta>
            <ta e="T360" id="Seg_7030" s="T358">Где ты?</ta>
            <ta e="T361" id="Seg_7031" s="T360">Лыыбыра кричит:</ta>
            <ta e="T364" id="Seg_7032" s="T361">— Я во дворе! — сказал.</ta>
            <ta e="T371" id="Seg_7033" s="T364">Как услышал это, пошел напролом через стену.</ta>
            <ta e="T375" id="Seg_7034" s="T371">Лыыбыры нигде нет.</ta>
            <ta e="T377" id="Seg_7035" s="T375">— Лыыбыра, где ты?</ta>
            <ta e="T382" id="Seg_7036" s="T377">— Я тут — в доме! — говорит Лыыбыра.</ta>
            <ta e="T387" id="Seg_7037" s="T382">Вломился в дом — Лыыбыры нет.</ta>
            <ta e="T389" id="Seg_7038" s="T387">— Лыыбыры, где ты?</ta>
            <ta e="T391" id="Seg_7039" s="T389">— Я в дымоходе!</ta>
            <ta e="T397" id="Seg_7040" s="T391">Заглянул в дымоход — Лыыбыры нигде нет.</ta>
            <ta e="T401" id="Seg_7041" s="T397">— Лыыбыра, где ты? — закричал.</ta>
            <ta e="T402" id="Seg_7042" s="T401">Лыыбыра говорит:</ta>
            <ta e="T406" id="Seg_7043" s="T402">— Я под очагом! — сказал.</ta>
            <ta e="T413" id="Seg_7044" s="T406">Как услышал Ангаа Монгус, стал туда просовывать голову.</ta>
            <ta e="T416" id="Seg_7045" s="T413">Лыыбыра говорит:</ta>
            <ta e="T421" id="Seg_7046" s="T416">— Дедушка, голову не суй — не влезешь.</ta>
            <ta e="T423" id="Seg_7047" s="T421">Задом иди.</ta>
            <ta e="T426" id="Seg_7048" s="T423">Ангаа Монгус задом полез.</ta>
            <ta e="T436" id="Seg_7049" s="T426">Когда так стал влезать, Лыыбыра раскаленную докрасна кочергу-ожиг в задний проход [ему] всадил.</ta>
            <ta e="T439" id="Seg_7050" s="T436">Ангаа Монгус испустил дух.</ta>
            <ta e="T445" id="Seg_7051" s="T439">Лыыбыра вышел и отправился домой.</ta>
            <ta e="T448" id="Seg_7052" s="T445">Так и жил себе богато и сыто.</ta>
            <ta e="T450" id="Seg_7053" s="T448">Вот и все.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T153" id="Seg_7054" s="T148">[DCh]: Not clear, why accusative case here.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T451" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
