<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID682F8C55-877F-386E-4508-654D968C7946">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>BaA_1930_One-EyedGirl_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\BaA_1930_OneEyedGirl_flk\BaA_1930_OneEyedGirl_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">471</ud-information>
            <ud-information attribute-name="# HIAT:w">368</ud-information>
            <ud-information attribute-name="# e">368</ud-information>
            <ud-information attribute-name="# HIAT:u">51</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="BaA">
            <abbreviation>BaA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
         <tli id="T161" time="84.5" type="appl" />
         <tli id="T162" time="85.0" type="appl" />
         <tli id="T163" time="85.5" type="appl" />
         <tli id="T164" time="86.0" type="appl" />
         <tli id="T165" time="86.5" type="appl" />
         <tli id="T166" time="87.0" type="appl" />
         <tli id="T167" time="87.5" type="appl" />
         <tli id="T168" time="88.0" type="appl" />
         <tli id="T169" time="88.5" type="appl" />
         <tli id="T170" time="89.0" type="appl" />
         <tli id="T171" time="89.5" type="appl" />
         <tli id="T172" time="90.0" type="appl" />
         <tli id="T173" time="90.5" type="appl" />
         <tli id="T174" time="91.0" type="appl" />
         <tli id="T175" time="91.5" type="appl" />
         <tli id="T176" time="92.0" type="appl" />
         <tli id="T177" time="92.5" type="appl" />
         <tli id="T178" time="93.0" type="appl" />
         <tli id="T179" time="93.5" type="appl" />
         <tli id="T180" time="94.0" type="appl" />
         <tli id="T181" time="94.5" type="appl" />
         <tli id="T182" time="95.0" type="appl" />
         <tli id="T183" time="95.5" type="appl" />
         <tli id="T184" time="96.0" type="appl" />
         <tli id="T185" time="96.5" type="appl" />
         <tli id="T186" time="97.0" type="appl" />
         <tli id="T187" time="97.5" type="appl" />
         <tli id="T188" time="98.0" type="appl" />
         <tli id="T189" time="98.5" type="appl" />
         <tli id="T190" time="99.0" type="appl" />
         <tli id="T191" time="99.5" type="appl" />
         <tli id="T192" time="100.0" type="appl" />
         <tli id="T193" time="100.5" type="appl" />
         <tli id="T194" time="101.0" type="appl" />
         <tli id="T195" time="101.5" type="appl" />
         <tli id="T196" time="102.0" type="appl" />
         <tli id="T197" time="102.5" type="appl" />
         <tli id="T198" time="103.0" type="appl" />
         <tli id="T199" time="103.5" type="appl" />
         <tli id="T200" time="104.0" type="appl" />
         <tli id="T201" time="104.5" type="appl" />
         <tli id="T202" time="105.0" type="appl" />
         <tli id="T203" time="105.5" type="appl" />
         <tli id="T204" time="106.0" type="appl" />
         <tli id="T205" time="106.5" type="appl" />
         <tli id="T206" time="107.0" type="appl" />
         <tli id="T207" time="107.5" type="appl" />
         <tli id="T208" time="108.0" type="appl" />
         <tli id="T209" time="108.5" type="appl" />
         <tli id="T210" time="109.0" type="appl" />
         <tli id="T211" time="109.5" type="appl" />
         <tli id="T212" time="110.0" type="appl" />
         <tli id="T213" time="110.5" type="appl" />
         <tli id="T214" time="111.0" type="appl" />
         <tli id="T215" time="111.5" type="appl" />
         <tli id="T216" time="112.0" type="appl" />
         <tli id="T217" time="112.5" type="appl" />
         <tli id="T218" time="113.0" type="appl" />
         <tli id="T219" time="113.5" type="appl" />
         <tli id="T220" time="114.0" type="appl" />
         <tli id="T221" time="114.5" type="appl" />
         <tli id="T222" time="115.0" type="appl" />
         <tli id="T223" time="115.5" type="appl" />
         <tli id="T224" time="116.0" type="appl" />
         <tli id="T225" time="116.5" type="appl" />
         <tli id="T226" time="117.0" type="appl" />
         <tli id="T227" time="117.5" type="appl" />
         <tli id="T228" time="118.0" type="appl" />
         <tli id="T229" time="118.5" type="appl" />
         <tli id="T230" time="119.0" type="appl" />
         <tli id="T231" time="119.5" type="appl" />
         <tli id="T232" time="120.0" type="appl" />
         <tli id="T233" time="120.5" type="appl" />
         <tli id="T234" time="121.0" type="appl" />
         <tli id="T235" time="121.5" type="appl" />
         <tli id="T236" time="122.0" type="appl" />
         <tli id="T237" time="122.5" type="appl" />
         <tli id="T238" time="123.0" type="appl" />
         <tli id="T239" time="123.5" type="appl" />
         <tli id="T240" time="124.0" type="appl" />
         <tli id="T241" time="124.5" type="appl" />
         <tli id="T242" time="125.0" type="appl" />
         <tli id="T243" time="125.5" type="appl" />
         <tli id="T244" time="126.0" type="appl" />
         <tli id="T245" time="126.5" type="appl" />
         <tli id="T246" time="127.0" type="appl" />
         <tli id="T247" time="127.5" type="appl" />
         <tli id="T248" time="128.0" type="appl" />
         <tli id="T249" time="128.5" type="appl" />
         <tli id="T250" time="129.0" type="appl" />
         <tli id="T251" time="129.5" type="appl" />
         <tli id="T252" time="130.0" type="appl" />
         <tli id="T253" time="130.5" type="appl" />
         <tli id="T254" time="131.0" type="appl" />
         <tli id="T255" time="131.5" type="appl" />
         <tli id="T256" time="132.0" type="appl" />
         <tli id="T257" time="132.5" type="appl" />
         <tli id="T258" time="133.0" type="appl" />
         <tli id="T259" time="133.5" type="appl" />
         <tli id="T260" time="134.0" type="appl" />
         <tli id="T261" time="134.5" type="appl" />
         <tli id="T262" time="135.0" type="appl" />
         <tli id="T263" time="135.5" type="appl" />
         <tli id="T264" time="136.0" type="appl" />
         <tli id="T265" time="136.5" type="appl" />
         <tli id="T266" time="137.0" type="appl" />
         <tli id="T267" time="137.5" type="appl" />
         <tli id="T268" time="138.0" type="appl" />
         <tli id="T269" time="138.5" type="appl" />
         <tli id="T270" time="139.0" type="appl" />
         <tli id="T271" time="139.5" type="appl" />
         <tli id="T272" time="140.0" type="appl" />
         <tli id="T273" time="140.5" type="appl" />
         <tli id="T274" time="141.0" type="appl" />
         <tli id="T275" time="141.5" type="appl" />
         <tli id="T276" time="142.0" type="appl" />
         <tli id="T277" time="142.5" type="appl" />
         <tli id="T278" time="143.0" type="appl" />
         <tli id="T279" time="143.5" type="appl" />
         <tli id="T280" time="144.0" type="appl" />
         <tli id="T281" time="144.5" type="appl" />
         <tli id="T282" time="145.0" type="appl" />
         <tli id="T283" time="145.5" type="appl" />
         <tli id="T284" time="146.0" type="appl" />
         <tli id="T285" time="146.5" type="appl" />
         <tli id="T286" time="147.0" type="appl" />
         <tli id="T287" time="147.5" type="appl" />
         <tli id="T288" time="148.0" type="appl" />
         <tli id="T289" time="148.5" type="appl" />
         <tli id="T290" time="149.0" type="appl" />
         <tli id="T291" time="149.5" type="appl" />
         <tli id="T292" time="150.0" type="appl" />
         <tli id="T293" time="150.5" type="appl" />
         <tli id="T294" time="151.0" type="appl" />
         <tli id="T295" time="151.5" type="appl" />
         <tli id="T296" time="152.0" type="appl" />
         <tli id="T297" time="152.5" type="appl" />
         <tli id="T298" time="153.0" type="appl" />
         <tli id="T299" time="153.5" type="appl" />
         <tli id="T300" time="154.0" type="appl" />
         <tli id="T301" time="154.5" type="appl" />
         <tli id="T302" time="155.0" type="appl" />
         <tli id="T303" time="155.5" type="appl" />
         <tli id="T304" time="156.0" type="appl" />
         <tli id="T305" time="156.5" type="appl" />
         <tli id="T306" time="157.0" type="appl" />
         <tli id="T307" time="157.5" type="appl" />
         <tli id="T308" time="158.0" type="appl" />
         <tli id="T309" time="158.5" type="appl" />
         <tli id="T310" time="159.0" type="appl" />
         <tli id="T311" time="159.5" type="appl" />
         <tli id="T312" time="160.0" type="appl" />
         <tli id="T313" time="160.5" type="appl" />
         <tli id="T314" time="161.0" type="appl" />
         <tli id="T315" time="161.5" type="appl" />
         <tli id="T316" time="162.0" type="appl" />
         <tli id="T317" time="162.5" type="appl" />
         <tli id="T318" time="163.0" type="appl" />
         <tli id="T319" time="163.5" type="appl" />
         <tli id="T320" time="164.0" type="appl" />
         <tli id="T321" time="164.5" type="appl" />
         <tli id="T322" time="165.0" type="appl" />
         <tli id="T323" time="165.5" type="appl" />
         <tli id="T324" time="166.0" type="appl" />
         <tli id="T325" time="166.5" type="appl" />
         <tli id="T326" time="167.0" type="appl" />
         <tli id="T327" time="167.5" type="appl" />
         <tli id="T328" time="168.0" type="appl" />
         <tli id="T329" time="168.5" type="appl" />
         <tli id="T330" time="169.0" type="appl" />
         <tli id="T331" time="169.5" type="appl" />
         <tli id="T332" time="170.0" type="appl" />
         <tli id="T333" time="170.5" type="appl" />
         <tli id="T334" time="171.0" type="appl" />
         <tli id="T335" time="171.5" type="appl" />
         <tli id="T336" time="172.0" type="appl" />
         <tli id="T337" time="172.5" type="appl" />
         <tli id="T338" time="173.0" type="appl" />
         <tli id="T339" time="173.5" type="appl" />
         <tli id="T340" time="174.0" type="appl" />
         <tli id="T341" time="174.5" type="appl" />
         <tli id="T342" time="175.0" type="appl" />
         <tli id="T343" time="175.5" type="appl" />
         <tli id="T344" time="176.0" type="appl" />
         <tli id="T345" time="176.5" type="appl" />
         <tli id="T346" time="177.0" type="appl" />
         <tli id="T347" time="177.5" type="appl" />
         <tli id="T348" time="178.0" type="appl" />
         <tli id="T349" time="178.5" type="appl" />
         <tli id="T350" time="179.0" type="appl" />
         <tli id="T351" time="179.5" type="appl" />
         <tli id="T352" time="180.0" type="appl" />
         <tli id="T353" time="180.5" type="appl" />
         <tli id="T354" time="181.0" type="appl" />
         <tli id="T355" time="181.5" type="appl" />
         <tli id="T356" time="182.0" type="appl" />
         <tli id="T357" time="182.5" type="appl" />
         <tli id="T358" time="183.0" type="appl" />
         <tli id="T359" time="183.5" type="appl" />
         <tli id="T360" time="184.0" type="appl" />
         <tli id="T361" time="184.5" type="appl" />
         <tli id="T362" time="185.0" type="appl" />
         <tli id="T363" time="185.5" type="appl" />
         <tli id="T364" time="186.0" type="appl" />
         <tli id="T365" time="186.5" type="appl" />
         <tli id="T366" time="187.0" type="appl" />
         <tli id="T367" time="187.5" type="appl" />
         <tli id="T368" time="188.0" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="BaA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T368" id="Seg_0" n="sc" s="T0">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Bɨlɨr</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">biːr</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">u͡ollaːk</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">dʼuraːk</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">kineːhe</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">olorbut</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_23" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">Kɨhɨn</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">bajgalga</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">dʼuraːktar</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">bölüːgeleːččiler</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_38" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">Maː</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">kineːs</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">bajgal</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">buːhunan</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">bölüːge</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">ojbono</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">allara</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_61" n="HIAT:w" s="T17">barar</ts>
                  <nts id="Seg_62" n="HIAT:ip">.</nts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_65" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_67" n="HIAT:w" s="T18">Bu</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">gɨnan</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_73" n="HIAT:w" s="T20">baran</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_76" n="HIAT:w" s="T21">tönnöːrü</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_79" n="HIAT:w" s="T22">gɨmmɨta</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_82" n="HIAT:w" s="T23">buːhun</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_85" n="HIAT:w" s="T24">bajgal</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_88" n="HIAT:w" s="T25">aldʼatan</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_91" n="HIAT:w" s="T26">keːspit</ts>
                  <nts id="Seg_92" n="HIAT:ip">.</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_95" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_97" n="HIAT:w" s="T27">Üs</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_100" n="HIAT:w" s="T28">kün</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_103" n="HIAT:w" s="T29">kördüːr</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_106" n="HIAT:w" s="T30">taksar</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_109" n="HIAT:w" s="T31">hirin</ts>
                  <nts id="Seg_110" n="HIAT:ip">,</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_113" n="HIAT:w" s="T32">muŋa</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_116" n="HIAT:w" s="T33">bu͡olan</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_119" n="HIAT:w" s="T34">üs</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_122" n="HIAT:w" s="T35">tabatɨttan</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_125" n="HIAT:w" s="T36">biːrin</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_128" n="HIAT:w" s="T37">hogudaːjdaːn</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_131" n="HIAT:w" s="T38">hiːr</ts>
                  <nts id="Seg_132" n="HIAT:ip">.</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_135" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_137" n="HIAT:w" s="T39">Onton</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_140" n="HIAT:w" s="T40">emi͡e</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_143" n="HIAT:w" s="T41">kaːma</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_146" n="HIAT:w" s="T42">hataːn</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_149" n="HIAT:w" s="T43">ikki</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_152" n="HIAT:w" s="T44">tabatɨn</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_155" n="HIAT:w" s="T45">barɨːr</ts>
                  <nts id="Seg_156" n="HIAT:ip">,</nts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_159" n="HIAT:w" s="T46">ölörör</ts>
                  <nts id="Seg_160" n="HIAT:ip">.</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_163" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_165" n="HIAT:w" s="T47">Buːha</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_168" n="HIAT:w" s="T48">ojun</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_171" n="HIAT:w" s="T49">düŋürün</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_174" n="HIAT:w" s="T50">haga</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_177" n="HIAT:w" s="T51">bu͡olar</ts>
                  <nts id="Seg_178" n="HIAT:ip">.</nts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_181" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_183" n="HIAT:w" s="T52">Onu</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_186" n="HIAT:w" s="T53">balkɨːr</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_189" n="HIAT:w" s="T54">kajdak</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_192" n="HIAT:w" s="T55">ere</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_195" n="HIAT:w" s="T56">arɨː</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_198" n="HIAT:w" s="T57">kɨtɨːtɨgar</ts>
                  <nts id="Seg_199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_201" n="HIAT:w" s="T58">ustan</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_204" n="HIAT:w" s="T59">kelbit</ts>
                  <nts id="Seg_205" n="HIAT:ip">,</nts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_208" n="HIAT:w" s="T60">maska</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_211" n="HIAT:w" s="T61">tu͡on</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_214" n="HIAT:w" s="T62">keːher</ts>
                  <nts id="Seg_215" n="HIAT:ip">.</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T68" id="Seg_218" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_220" n="HIAT:w" s="T63">Bu</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_223" n="HIAT:w" s="T64">arɨːga</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_226" n="HIAT:w" s="T65">taksɨbɨta</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_229" n="HIAT:w" s="T66">mastaːk</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_232" n="HIAT:w" s="T67">bagajɨ</ts>
                  <nts id="Seg_233" n="HIAT:ip">.</nts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_236" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_238" n="HIAT:w" s="T68">Kanna</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_241" n="HIAT:w" s="T69">ere</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_244" n="HIAT:w" s="T70">hüge</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_247" n="HIAT:w" s="T71">tɨ͡aha</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_250" n="HIAT:w" s="T72">ihiller</ts>
                  <nts id="Seg_251" n="HIAT:ip">.</nts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_254" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_256" n="HIAT:w" s="T73">Manna</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_259" n="HIAT:w" s="T74">dʼe</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_262" n="HIAT:w" s="T75">barɨ</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_265" n="HIAT:w" s="T76">küːhün</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_268" n="HIAT:w" s="T77">baran</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_271" n="HIAT:w" s="T78">tijer</ts>
                  <nts id="Seg_272" n="HIAT:ip">.</nts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T90" id="Seg_275" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_277" n="HIAT:w" s="T79">Tijen</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_280" n="HIAT:w" s="T80">körbüte</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_283" n="HIAT:w" s="T81">biːr</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_286" n="HIAT:w" s="T82">oččugukkaːn</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_289" n="HIAT:w" s="T83">kɨːs</ts>
                  <nts id="Seg_290" n="HIAT:ip">,</nts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_293" n="HIAT:w" s="T84">kini͡eke</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_296" n="HIAT:w" s="T85">köksütünen</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_299" n="HIAT:w" s="T86">turan</ts>
                  <nts id="Seg_300" n="HIAT:ip">,</nts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_303" n="HIAT:w" s="T87">mas</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_306" n="HIAT:w" s="T88">kerde</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_309" n="HIAT:w" s="T89">turar</ts>
                  <nts id="Seg_310" n="HIAT:ip">.</nts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_313" n="HIAT:u" s="T90">
                  <ts e="T91" id="Seg_315" n="HIAT:w" s="T90">Bu</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_318" n="HIAT:w" s="T91">kɨːs</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_321" n="HIAT:w" s="T92">kajɨhar</ts>
                  <nts id="Seg_322" n="HIAT:ip">,</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_325" n="HIAT:w" s="T93">onto</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_328" n="HIAT:w" s="T94">aŋaːr</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_331" n="HIAT:w" s="T95">karaktaːk</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_334" n="HIAT:w" s="T96">ebit</ts>
                  <nts id="Seg_335" n="HIAT:ip">.</nts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_338" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_340" n="HIAT:w" s="T97">Kɨːha</ts>
                  <nts id="Seg_341" n="HIAT:ip">,</nts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_344" n="HIAT:w" s="T98">kihini</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_347" n="HIAT:w" s="T99">körön</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_350" n="HIAT:w" s="T100">baran</ts>
                  <nts id="Seg_351" n="HIAT:ip">,</nts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_354" n="HIAT:w" s="T101">kuttanan</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_357" n="HIAT:w" s="T102">hügetin</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_360" n="HIAT:w" s="T103">bɨragan</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_363" n="HIAT:w" s="T104">hüːren</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_366" n="HIAT:w" s="T105">barar</ts>
                  <nts id="Seg_367" n="HIAT:ip">.</nts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_370" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_372" n="HIAT:w" s="T106">Hüːren</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_375" n="HIAT:w" s="T107">barar</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_378" n="HIAT:w" s="T108">da</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_380" n="HIAT:ip">"</nts>
                  <ts e="T110" id="Seg_382" n="HIAT:w" s="T109">onnugu</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_385" n="HIAT:w" s="T110">kördüm</ts>
                  <nts id="Seg_386" n="HIAT:ip">"</nts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_389" n="HIAT:w" s="T111">di͡en</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_392" n="HIAT:w" s="T112">dʼonugar</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_395" n="HIAT:w" s="T113">kepsiːr</ts>
                  <nts id="Seg_396" n="HIAT:ip">.</nts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_399" n="HIAT:u" s="T114">
                  <ts e="T115" id="Seg_401" n="HIAT:w" s="T114">Mantɨlara</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_404" n="HIAT:w" s="T115">kas</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_407" n="HIAT:w" s="T116">da</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_410" n="HIAT:w" s="T117">kihi</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_413" n="HIAT:w" s="T118">bu͡olan</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_416" n="HIAT:w" s="T119">barannar</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_419" n="HIAT:w" s="T120">hɨlajan</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_422" n="HIAT:w" s="T121">oktubut</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_425" n="HIAT:w" s="T122">kihilerin</ts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_428" n="HIAT:w" s="T123">tiriːge</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_431" n="HIAT:w" s="T124">kötögön</ts>
                  <nts id="Seg_432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_434" n="HIAT:w" s="T125">egeleller</ts>
                  <nts id="Seg_435" n="HIAT:ip">.</nts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_438" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_440" n="HIAT:w" s="T126">Ol</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_443" n="HIAT:w" s="T127">egelbitteriger</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_446" n="HIAT:w" s="T128">biːr</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_449" n="HIAT:w" s="T129">kaːr</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_452" n="HIAT:w" s="T130">kurduk</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_455" n="HIAT:w" s="T131">astaːk</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_458" n="HIAT:w" s="T132">ogonnʼor</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_461" n="HIAT:w" s="T133">eter</ts>
                  <nts id="Seg_462" n="HIAT:ip">:</nts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T167" id="Seg_465" n="HIAT:u" s="T134">
                  <ts e="T135" id="Seg_467" n="HIAT:w" s="T134">Eː</ts>
                  <nts id="Seg_468" n="HIAT:ip">,</nts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_471" n="HIAT:w" s="T135">tugu</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_474" n="HIAT:w" s="T136">högögüt</ts>
                  <nts id="Seg_475" n="HIAT:ip">,</nts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_478" n="HIAT:w" s="T137">oloŋkogo</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_481" n="HIAT:w" s="T138">da</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_484" n="HIAT:w" s="T139">baːr</ts>
                  <nts id="Seg_485" n="HIAT:ip">,</nts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_488" n="HIAT:w" s="T140">bɨlɨrgɨ</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_491" n="HIAT:w" s="T141">kepselge</ts>
                  <nts id="Seg_492" n="HIAT:ip">,</nts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_495" n="HIAT:w" s="T142">hurukka</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_498" n="HIAT:w" s="T143">da</ts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_501" n="HIAT:w" s="T144">baːr</ts>
                  <nts id="Seg_502" n="HIAT:ip">,</nts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_505" n="HIAT:w" s="T145">mannɨk</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_508" n="HIAT:w" s="T146">ikki</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_511" n="HIAT:w" s="T147">karaktaːk</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_514" n="HIAT:w" s="T148">noru͡ot</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_517" n="HIAT:w" s="T149">baːr</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_520" n="HIAT:w" s="T150">di͡en</ts>
                  <nts id="Seg_521" n="HIAT:ip">,</nts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_524" n="HIAT:w" s="T151">čejin</ts>
                  <nts id="Seg_525" n="HIAT:ip">,</nts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_528" n="HIAT:w" s="T152">korgujbut</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_531" n="HIAT:w" s="T153">kihi</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_534" n="HIAT:w" s="T154">bɨhɨːtɨnan</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_537" n="HIAT:w" s="T155">alɨs</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_540" n="HIAT:w" s="T156">ahɨtɨmna</ts>
                  <nts id="Seg_541" n="HIAT:ip">,</nts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_544" n="HIAT:w" s="T157">ahatan</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_547" n="HIAT:w" s="T158">ihiŋ</ts>
                  <nts id="Seg_548" n="HIAT:ip">,</nts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_551" n="HIAT:w" s="T159">tutajan</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_554" n="HIAT:w" s="T160">bejetin</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_557" n="HIAT:w" s="T161">hiritten</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_560" n="HIAT:w" s="T162">munan</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_563" n="HIAT:w" s="T163">kelbit</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_566" n="HIAT:w" s="T164">baraksan</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_569" n="HIAT:w" s="T165">bu͡olu͡o</ts>
                  <nts id="Seg_570" n="HIAT:ip">"</nts>
                  <nts id="Seg_571" n="HIAT:ip">,</nts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_574" n="HIAT:w" s="T166">diːr</ts>
                  <nts id="Seg_575" n="HIAT:ip">.</nts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T179" id="Seg_578" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_580" n="HIAT:w" s="T167">Bu</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_583" n="HIAT:w" s="T168">baːj</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_586" n="HIAT:w" s="T169">bagajɨ</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_589" n="HIAT:w" s="T170">noru͡ot</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_592" n="HIAT:w" s="T171">ebit</ts>
                  <nts id="Seg_593" n="HIAT:ip">,</nts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_596" n="HIAT:w" s="T172">hahɨllara</ts>
                  <nts id="Seg_597" n="HIAT:ip">,</nts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_600" n="HIAT:w" s="T173">kɨrsalara</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_603" n="HIAT:w" s="T174">dʼi͡elerin</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_606" n="HIAT:w" s="T175">aːnɨgar</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_609" n="HIAT:w" s="T176">bejete</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_612" n="HIAT:w" s="T177">keler</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_615" n="HIAT:w" s="T178">ebit</ts>
                  <nts id="Seg_616" n="HIAT:ip">.</nts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T183" id="Seg_619" n="HIAT:u" s="T179">
                  <ts e="T180" id="Seg_621" n="HIAT:w" s="T179">Baː</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_624" n="HIAT:w" s="T180">kihi</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_627" n="HIAT:w" s="T181">manna</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_630" n="HIAT:w" s="T182">oloktonor</ts>
                  <nts id="Seg_631" n="HIAT:ip">.</nts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T194" id="Seg_634" n="HIAT:u" s="T183">
                  <ts e="T184" id="Seg_636" n="HIAT:w" s="T183">Oloktommut</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_639" n="HIAT:w" s="T184">ɨ͡ala</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_642" n="HIAT:w" s="T185">ikki</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_645" n="HIAT:w" s="T186">kɨːstaːk</ts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_648" n="HIAT:w" s="T187">ebitter</ts>
                  <nts id="Seg_649" n="HIAT:ip">,</nts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_652" n="HIAT:w" s="T188">mantan</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_655" n="HIAT:w" s="T189">kɨrdʼagahɨn</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_658" n="HIAT:w" s="T190">agata</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_661" n="HIAT:w" s="T191">ojok</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_664" n="HIAT:w" s="T192">bi͡erer</ts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_667" n="HIAT:w" s="T193">u͡olga</ts>
                  <nts id="Seg_668" n="HIAT:ip">.</nts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T200" id="Seg_671" n="HIAT:u" s="T194">
                  <ts e="T195" id="Seg_673" n="HIAT:w" s="T194">Manɨ͡aka</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_676" n="HIAT:w" s="T195">baː</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_679" n="HIAT:w" s="T196">köröːččü</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_682" n="HIAT:w" s="T197">kɨːs</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_685" n="HIAT:w" s="T198">elʼijiger</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_688" n="HIAT:w" s="T199">künüːleːn</ts>
                  <nts id="Seg_689" n="HIAT:ip">:</nts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T206" id="Seg_692" n="HIAT:u" s="T200">
                  <nts id="Seg_693" n="HIAT:ip">"</nts>
                  <ts e="T201" id="Seg_695" n="HIAT:w" s="T200">Min</ts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_698" n="HIAT:w" s="T201">barɨ͡aktaːk</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_701" n="HIAT:w" s="T202">bejem</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_704" n="HIAT:w" s="T203">buluːbar</ts>
                  <nts id="Seg_705" n="HIAT:ip">"</nts>
                  <nts id="Seg_706" n="HIAT:ip">,</nts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_709" n="HIAT:w" s="T204">di͡en</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_712" n="HIAT:w" s="T205">ɨtɨːr</ts>
                  <nts id="Seg_713" n="HIAT:ip">.</nts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T213" id="Seg_716" n="HIAT:u" s="T206">
                  <ts e="T207" id="Seg_718" n="HIAT:w" s="T206">Onu</ts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_721" n="HIAT:w" s="T207">istimne</ts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_724" n="HIAT:w" s="T208">hin</ts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_727" n="HIAT:w" s="T209">kɨrdʼas</ts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_730" n="HIAT:w" s="T210">kɨːhɨ</ts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_733" n="HIAT:w" s="T211">bi͡ereller</ts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_736" n="HIAT:w" s="T212">u͡olga</ts>
                  <nts id="Seg_737" n="HIAT:ip">.</nts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T220" id="Seg_740" n="HIAT:u" s="T213">
                  <ts e="T214" id="Seg_742" n="HIAT:w" s="T213">Biːr</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_745" n="HIAT:w" s="T214">hɨlɨnan</ts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_748" n="HIAT:w" s="T215">ikki</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_751" n="HIAT:w" s="T216">karaktaːk</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_754" n="HIAT:w" s="T217">u͡ol</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_757" n="HIAT:w" s="T218">ogo</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_760" n="HIAT:w" s="T219">törüːr</ts>
                  <nts id="Seg_761" n="HIAT:ip">.</nts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T225" id="Seg_764" n="HIAT:u" s="T220">
                  <ts e="T221" id="Seg_766" n="HIAT:w" s="T220">Dʼe</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_769" n="HIAT:w" s="T221">ör</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_772" n="HIAT:w" s="T222">bagajɨ</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_775" n="HIAT:w" s="T223">olorollor</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_778" n="HIAT:w" s="T224">ogurduk</ts>
                  <nts id="Seg_779" n="HIAT:ip">.</nts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T232" id="Seg_782" n="HIAT:u" s="T225">
                  <ts e="T226" id="Seg_784" n="HIAT:w" s="T225">Bu</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_787" n="HIAT:w" s="T226">aŋaːr</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_790" n="HIAT:w" s="T227">karaktaːktar</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_793" n="HIAT:w" s="T228">elbek</ts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_796" n="HIAT:w" s="T229">bagajɨ</ts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_799" n="HIAT:w" s="T230">noru͡ot</ts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_802" n="HIAT:w" s="T231">ebitter</ts>
                  <nts id="Seg_803" n="HIAT:ip">.</nts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T238" id="Seg_806" n="HIAT:u" s="T232">
                  <ts e="T233" id="Seg_808" n="HIAT:w" s="T232">Biːrde</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_811" n="HIAT:w" s="T233">karaːbɨnan</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_814" n="HIAT:w" s="T234">ɨraːktaːgɨlara</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_817" n="HIAT:w" s="T235">ölbüge</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_820" n="HIAT:w" s="T236">komuja</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_823" n="HIAT:w" s="T237">keler</ts>
                  <nts id="Seg_824" n="HIAT:ip">.</nts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T244" id="Seg_827" n="HIAT:u" s="T238">
                  <ts e="T239" id="Seg_829" n="HIAT:w" s="T238">Kinitten</ts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_832" n="HIAT:w" s="T239">kihi</ts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_835" n="HIAT:w" s="T240">keler</ts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_838" n="HIAT:w" s="T241">baː</ts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_841" n="HIAT:w" s="T242">kineːs</ts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_844" n="HIAT:w" s="T243">kɨnnɨgar</ts>
                  <nts id="Seg_845" n="HIAT:ip">:</nts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T255" id="Seg_848" n="HIAT:u" s="T244">
                  <nts id="Seg_849" n="HIAT:ip">"</nts>
                  <ts e="T245" id="Seg_851" n="HIAT:w" s="T244">En</ts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_854" n="HIAT:w" s="T245">ikki</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_857" n="HIAT:w" s="T246">karaktaːk</ts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_860" n="HIAT:w" s="T247">kütü͡ötteːk</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_863" n="HIAT:w" s="T248">ühügün</ts>
                  <nts id="Seg_864" n="HIAT:ip">,</nts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_867" n="HIAT:w" s="T249">en</ts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_870" n="HIAT:w" s="T250">ontugun</ts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_873" n="HIAT:w" s="T251">ɨraːktaːgɨ</ts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_876" n="HIAT:w" s="T252">köröːrü</ts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_879" n="HIAT:w" s="T253">gɨnar</ts>
                  <nts id="Seg_880" n="HIAT:ip">"</nts>
                  <nts id="Seg_881" n="HIAT:ip">,</nts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_884" n="HIAT:w" s="T254">diːr</ts>
                  <nts id="Seg_885" n="HIAT:ip">.</nts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T257" id="Seg_888" n="HIAT:u" s="T255">
                  <ts e="T256" id="Seg_890" n="HIAT:w" s="T255">Kɨnna</ts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_893" n="HIAT:w" s="T256">eter</ts>
                  <nts id="Seg_894" n="HIAT:ip">:</nts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T268" id="Seg_897" n="HIAT:u" s="T257">
                  <ts e="T258" id="Seg_899" n="HIAT:w" s="T257">Kuːbam</ts>
                  <nts id="Seg_900" n="HIAT:ip">,</nts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_903" n="HIAT:w" s="T258">bar</ts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_906" n="HIAT:w" s="T259">ɨraːktaːgɨ</ts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_909" n="HIAT:w" s="T260">körü͡ö</ts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_912" n="HIAT:w" s="T261">ere</ts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_915" n="HIAT:w" s="T262">diː</ts>
                  <nts id="Seg_916" n="HIAT:ip">,</nts>
                  <nts id="Seg_917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_919" n="HIAT:w" s="T263">en</ts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_922" n="HIAT:w" s="T264">bar</ts>
                  <nts id="Seg_923" n="HIAT:ip">,</nts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_926" n="HIAT:w" s="T265">barbatakkɨna</ts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_929" n="HIAT:w" s="T266">baspɨtɨn</ts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_932" n="HIAT:w" s="T267">bɨhɨ͡a</ts>
                  <nts id="Seg_933" n="HIAT:ip">.</nts>
                  <nts id="Seg_934" n="HIAT:ip">"</nts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T271" id="Seg_937" n="HIAT:u" s="T268">
                  <ts e="T269" id="Seg_939" n="HIAT:w" s="T268">Kineːs</ts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_942" n="HIAT:w" s="T269">ɨraːktaːgɨga</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_945" n="HIAT:w" s="T270">barar</ts>
                  <nts id="Seg_946" n="HIAT:ip">.</nts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T273" id="Seg_949" n="HIAT:u" s="T271">
                  <ts e="T272" id="Seg_951" n="HIAT:w" s="T271">Ontuta</ts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_954" n="HIAT:w" s="T272">eter</ts>
                  <nts id="Seg_955" n="HIAT:ip">:</nts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T288" id="Seg_958" n="HIAT:u" s="T273">
                  <nts id="Seg_959" n="HIAT:ip">"</nts>
                  <ts e="T274" id="Seg_961" n="HIAT:w" s="T273">Min</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_964" n="HIAT:w" s="T274">ejigin</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_967" n="HIAT:w" s="T275">manna</ts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_970" n="HIAT:w" s="T276">baːj</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_973" n="HIAT:w" s="T277">kihi</ts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_976" n="HIAT:w" s="T278">gɨnan</ts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_979" n="HIAT:w" s="T279">teriji͡em</ts>
                  <nts id="Seg_980" n="HIAT:ip">,</nts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_983" n="HIAT:w" s="T280">en</ts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_986" n="HIAT:w" s="T281">manna</ts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_989" n="HIAT:w" s="T282">kaːlɨ͡aŋ</ts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_992" n="HIAT:w" s="T283">duː</ts>
                  <nts id="Seg_993" n="HIAT:ip">,</nts>
                  <nts id="Seg_994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_996" n="HIAT:w" s="T284">dojduŋ</ts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_999" n="HIAT:w" s="T285">di͡eg</ts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1002" n="HIAT:w" s="T286">tönnü͡öŋ</ts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1005" n="HIAT:w" s="T287">duː</ts>
                  <nts id="Seg_1006" n="HIAT:ip">?</nts>
                  <nts id="Seg_1007" n="HIAT:ip">"</nts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T289" id="Seg_1010" n="HIAT:u" s="T288">
                  <ts e="T289" id="Seg_1012" n="HIAT:w" s="T288">Onu</ts>
                  <nts id="Seg_1013" n="HIAT:ip">:</nts>
                  <nts id="Seg_1014" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T291" id="Seg_1016" n="HIAT:u" s="T289">
                  <nts id="Seg_1017" n="HIAT:ip">"</nts>
                  <ts e="T290" id="Seg_1019" n="HIAT:w" s="T289">Tönnü͡öm</ts>
                  <nts id="Seg_1020" n="HIAT:ip">"</nts>
                  <nts id="Seg_1021" n="HIAT:ip">,</nts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1024" n="HIAT:w" s="T290">di͡ete</ts>
                  <nts id="Seg_1025" n="HIAT:ip">.</nts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T294" id="Seg_1028" n="HIAT:u" s="T291">
                  <ts e="T292" id="Seg_1030" n="HIAT:w" s="T291">ɨraːktaːgɨ</ts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1033" n="HIAT:w" s="T292">onuga</ts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1036" n="HIAT:w" s="T293">di͡ete</ts>
                  <nts id="Seg_1037" n="HIAT:ip">:</nts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T301" id="Seg_1040" n="HIAT:u" s="T294">
                  <nts id="Seg_1041" n="HIAT:ip">"</nts>
                  <ts e="T295" id="Seg_1043" n="HIAT:w" s="T294">ɨraːk</ts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1046" n="HIAT:w" s="T295">hir</ts>
                  <nts id="Seg_1047" n="HIAT:ip">,</nts>
                  <nts id="Seg_1048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1050" n="HIAT:w" s="T296">uː</ts>
                  <nts id="Seg_1051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1053" n="HIAT:w" s="T297">egelbitin</ts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1056" n="HIAT:w" s="T298">kurduk</ts>
                  <nts id="Seg_1057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1059" n="HIAT:w" s="T299">barɨ͡aŋ</ts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1062" n="HIAT:w" s="T300">hu͡oga</ts>
                  <nts id="Seg_1063" n="HIAT:ip">.</nts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T313" id="Seg_1066" n="HIAT:u" s="T301">
                  <ts e="T302" id="Seg_1068" n="HIAT:w" s="T301">Min</ts>
                  <nts id="Seg_1069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1071" n="HIAT:w" s="T302">huruk</ts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1074" n="HIAT:w" s="T303">bi͡eri͡em</ts>
                  <nts id="Seg_1075" n="HIAT:ip">,</nts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1078" n="HIAT:w" s="T304">kim</ts>
                  <nts id="Seg_1079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1081" n="HIAT:w" s="T305">da</ts>
                  <nts id="Seg_1082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1084" n="HIAT:w" s="T306">ejigin</ts>
                  <nts id="Seg_1085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1087" n="HIAT:w" s="T307">bolbu͡ota</ts>
                  <nts id="Seg_1088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1090" n="HIAT:w" s="T308">hu͡ok</ts>
                  <nts id="Seg_1091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1093" n="HIAT:w" s="T309">ildʼi͡e</ts>
                  <nts id="Seg_1094" n="HIAT:ip">"</nts>
                  <nts id="Seg_1095" n="HIAT:ip">,</nts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1098" n="HIAT:w" s="T310">di͡en</ts>
                  <nts id="Seg_1099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1101" n="HIAT:w" s="T311">huruk</ts>
                  <nts id="Seg_1102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1104" n="HIAT:w" s="T312">bi͡erer</ts>
                  <nts id="Seg_1105" n="HIAT:ip">.</nts>
                  <nts id="Seg_1106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T321" id="Seg_1108" n="HIAT:u" s="T313">
                  <ts e="T314" id="Seg_1110" n="HIAT:w" s="T313">Onu</ts>
                  <nts id="Seg_1111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1113" n="HIAT:w" s="T314">ɨlan</ts>
                  <nts id="Seg_1114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1116" n="HIAT:w" s="T315">baran</ts>
                  <nts id="Seg_1117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1119" n="HIAT:w" s="T316">hin</ts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1122" n="HIAT:w" s="T317">kas</ts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1125" n="HIAT:w" s="T318">da</ts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1128" n="HIAT:w" s="T319">hɨl</ts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1131" n="HIAT:w" s="T320">oloror</ts>
                  <nts id="Seg_1132" n="HIAT:ip">.</nts>
                  <nts id="Seg_1133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T328" id="Seg_1135" n="HIAT:u" s="T321">
                  <ts e="T322" id="Seg_1137" n="HIAT:w" s="T321">Bu</ts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1140" n="HIAT:w" s="T322">oloron</ts>
                  <nts id="Seg_1141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1143" n="HIAT:w" s="T323">baran</ts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1146" n="HIAT:w" s="T324">baraːrɨ</ts>
                  <nts id="Seg_1147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1149" n="HIAT:w" s="T325">teriner</ts>
                  <nts id="Seg_1150" n="HIAT:ip">,</nts>
                  <nts id="Seg_1151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1153" n="HIAT:w" s="T326">dʼaktarɨn</ts>
                  <nts id="Seg_1154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1156" n="HIAT:w" s="T327">ɨlan</ts>
                  <nts id="Seg_1157" n="HIAT:ip">.</nts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T333" id="Seg_1160" n="HIAT:u" s="T328">
                  <ts e="T329" id="Seg_1162" n="HIAT:w" s="T328">Manɨ͡aga</ts>
                  <nts id="Seg_1163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1165" n="HIAT:w" s="T329">ɨlgɨn</ts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1168" n="HIAT:w" s="T330">kɨːs</ts>
                  <nts id="Seg_1169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1171" n="HIAT:w" s="T331">ɨtaːn</ts>
                  <nts id="Seg_1172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1174" n="HIAT:w" s="T332">barar</ts>
                  <nts id="Seg_1175" n="HIAT:ip">:</nts>
                  <nts id="Seg_1176" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T339" id="Seg_1178" n="HIAT:u" s="T333">
                  <nts id="Seg_1179" n="HIAT:ip">"</nts>
                  <ts e="T334" id="Seg_1181" n="HIAT:w" s="T333">Min</ts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1184" n="HIAT:w" s="T334">bulbut</ts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1187" n="HIAT:w" s="T335">kihibitten</ts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1190" n="HIAT:w" s="T336">kaːlbappɨn</ts>
                  <nts id="Seg_1191" n="HIAT:ip">,</nts>
                  <nts id="Seg_1192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1194" n="HIAT:w" s="T337">biːrge</ts>
                  <nts id="Seg_1195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1197" n="HIAT:w" s="T338">barsɨ͡am</ts>
                  <nts id="Seg_1198" n="HIAT:ip">.</nts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T341" id="Seg_1201" n="HIAT:u" s="T339">
                  <ts e="T340" id="Seg_1203" n="HIAT:w" s="T339">Agahɨgar</ts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1206" n="HIAT:w" s="T340">eter</ts>
                  <nts id="Seg_1207" n="HIAT:ip">:</nts>
                  <nts id="Seg_1208" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T347" id="Seg_1210" n="HIAT:u" s="T341">
                  <nts id="Seg_1211" n="HIAT:ip">"</nts>
                  <ts e="T342" id="Seg_1213" n="HIAT:w" s="T341">En</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1216" n="HIAT:w" s="T342">körör</ts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1219" n="HIAT:w" s="T343">gɨnɨ͡akkɨn</ts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1222" n="HIAT:w" s="T344">ikki</ts>
                  <nts id="Seg_1223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1225" n="HIAT:w" s="T345">karaktaːk</ts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1228" n="HIAT:w" s="T346">ogoloːkkun</ts>
                  <nts id="Seg_1229" n="HIAT:ip">.</nts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T351" id="Seg_1232" n="HIAT:u" s="T347">
                  <ts e="T348" id="Seg_1234" n="HIAT:w" s="T347">Min</ts>
                  <nts id="Seg_1235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1237" n="HIAT:w" s="T348">barsɨ͡am</ts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1240" n="HIAT:w" s="T349">kinini</ts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1243" n="HIAT:w" s="T350">gɨtta</ts>
                  <nts id="Seg_1244" n="HIAT:ip">.</nts>
                  <nts id="Seg_1245" n="HIAT:ip">"</nts>
                  <nts id="Seg_1246" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T354" id="Seg_1248" n="HIAT:u" s="T351">
                  <ts e="T352" id="Seg_1250" n="HIAT:w" s="T351">Agata</ts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1253" n="HIAT:w" s="T352">hübeleːn</ts>
                  <nts id="Seg_1254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1256" n="HIAT:w" s="T353">köŋüllüːr</ts>
                  <nts id="Seg_1257" n="HIAT:ip">.</nts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T359" id="Seg_1260" n="HIAT:u" s="T354">
                  <ts e="T355" id="Seg_1262" n="HIAT:w" s="T354">Onon</ts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1265" n="HIAT:w" s="T355">ɨlgɨn</ts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1268" n="HIAT:w" s="T356">kɨːs</ts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1271" n="HIAT:w" s="T357">biːrge</ts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1274" n="HIAT:w" s="T358">barsar</ts>
                  <nts id="Seg_1275" n="HIAT:ip">.</nts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T365" id="Seg_1278" n="HIAT:u" s="T359">
                  <ts e="T360" id="Seg_1280" n="HIAT:w" s="T359">Dʼe</ts>
                  <nts id="Seg_1281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1283" n="HIAT:w" s="T360">ör</ts>
                  <nts id="Seg_1284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1286" n="HIAT:w" s="T361">bagajɨ</ts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1289" n="HIAT:w" s="T362">ajannaːn</ts>
                  <nts id="Seg_1290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1292" n="HIAT:w" s="T363">tijeller</ts>
                  <nts id="Seg_1293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1295" n="HIAT:w" s="T364">dojdularɨgar</ts>
                  <nts id="Seg_1296" n="HIAT:ip">.</nts>
                  <nts id="Seg_1297" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T368" id="Seg_1299" n="HIAT:u" s="T365">
                  <ts e="T366" id="Seg_1301" n="HIAT:w" s="T365">Ogurduk</ts>
                  <nts id="Seg_1302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1304" n="HIAT:w" s="T366">ös</ts>
                  <nts id="Seg_1305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1307" n="HIAT:w" s="T367">baːr</ts>
                  <nts id="Seg_1308" n="HIAT:ip">.</nts>
                  <nts id="Seg_1309" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T368" id="Seg_1310" n="sc" s="T0">
               <ts e="T1" id="Seg_1312" n="e" s="T0">Bɨlɨr </ts>
               <ts e="T2" id="Seg_1314" n="e" s="T1">biːr </ts>
               <ts e="T3" id="Seg_1316" n="e" s="T2">u͡ollaːk </ts>
               <ts e="T4" id="Seg_1318" n="e" s="T3">dʼuraːk </ts>
               <ts e="T5" id="Seg_1320" n="e" s="T4">kineːhe </ts>
               <ts e="T6" id="Seg_1322" n="e" s="T5">olorbut. </ts>
               <ts e="T7" id="Seg_1324" n="e" s="T6">Kɨhɨn </ts>
               <ts e="T8" id="Seg_1326" n="e" s="T7">bajgalga </ts>
               <ts e="T9" id="Seg_1328" n="e" s="T8">dʼuraːktar </ts>
               <ts e="T10" id="Seg_1330" n="e" s="T9">bölüːgeleːččiler. </ts>
               <ts e="T11" id="Seg_1332" n="e" s="T10">Maː </ts>
               <ts e="T12" id="Seg_1334" n="e" s="T11">kineːs </ts>
               <ts e="T13" id="Seg_1336" n="e" s="T12">bajgal </ts>
               <ts e="T14" id="Seg_1338" n="e" s="T13">buːhunan </ts>
               <ts e="T15" id="Seg_1340" n="e" s="T14">bölüːge </ts>
               <ts e="T16" id="Seg_1342" n="e" s="T15">ojbono </ts>
               <ts e="T17" id="Seg_1344" n="e" s="T16">allara </ts>
               <ts e="T18" id="Seg_1346" n="e" s="T17">barar. </ts>
               <ts e="T19" id="Seg_1348" n="e" s="T18">Bu </ts>
               <ts e="T20" id="Seg_1350" n="e" s="T19">gɨnan </ts>
               <ts e="T21" id="Seg_1352" n="e" s="T20">baran </ts>
               <ts e="T22" id="Seg_1354" n="e" s="T21">tönnöːrü </ts>
               <ts e="T23" id="Seg_1356" n="e" s="T22">gɨmmɨta </ts>
               <ts e="T24" id="Seg_1358" n="e" s="T23">buːhun </ts>
               <ts e="T25" id="Seg_1360" n="e" s="T24">bajgal </ts>
               <ts e="T26" id="Seg_1362" n="e" s="T25">aldʼatan </ts>
               <ts e="T27" id="Seg_1364" n="e" s="T26">keːspit. </ts>
               <ts e="T28" id="Seg_1366" n="e" s="T27">Üs </ts>
               <ts e="T29" id="Seg_1368" n="e" s="T28">kün </ts>
               <ts e="T30" id="Seg_1370" n="e" s="T29">kördüːr </ts>
               <ts e="T31" id="Seg_1372" n="e" s="T30">taksar </ts>
               <ts e="T32" id="Seg_1374" n="e" s="T31">hirin, </ts>
               <ts e="T33" id="Seg_1376" n="e" s="T32">muŋa </ts>
               <ts e="T34" id="Seg_1378" n="e" s="T33">bu͡olan </ts>
               <ts e="T35" id="Seg_1380" n="e" s="T34">üs </ts>
               <ts e="T36" id="Seg_1382" n="e" s="T35">tabatɨttan </ts>
               <ts e="T37" id="Seg_1384" n="e" s="T36">biːrin </ts>
               <ts e="T38" id="Seg_1386" n="e" s="T37">hogudaːjdaːn </ts>
               <ts e="T39" id="Seg_1388" n="e" s="T38">hiːr. </ts>
               <ts e="T40" id="Seg_1390" n="e" s="T39">Onton </ts>
               <ts e="T41" id="Seg_1392" n="e" s="T40">emi͡e </ts>
               <ts e="T42" id="Seg_1394" n="e" s="T41">kaːma </ts>
               <ts e="T43" id="Seg_1396" n="e" s="T42">hataːn </ts>
               <ts e="T44" id="Seg_1398" n="e" s="T43">ikki </ts>
               <ts e="T45" id="Seg_1400" n="e" s="T44">tabatɨn </ts>
               <ts e="T46" id="Seg_1402" n="e" s="T45">barɨːr, </ts>
               <ts e="T47" id="Seg_1404" n="e" s="T46">ölörör. </ts>
               <ts e="T48" id="Seg_1406" n="e" s="T47">Buːha </ts>
               <ts e="T49" id="Seg_1408" n="e" s="T48">ojun </ts>
               <ts e="T50" id="Seg_1410" n="e" s="T49">düŋürün </ts>
               <ts e="T51" id="Seg_1412" n="e" s="T50">haga </ts>
               <ts e="T52" id="Seg_1414" n="e" s="T51">bu͡olar. </ts>
               <ts e="T53" id="Seg_1416" n="e" s="T52">Onu </ts>
               <ts e="T54" id="Seg_1418" n="e" s="T53">balkɨːr </ts>
               <ts e="T55" id="Seg_1420" n="e" s="T54">kajdak </ts>
               <ts e="T56" id="Seg_1422" n="e" s="T55">ere </ts>
               <ts e="T57" id="Seg_1424" n="e" s="T56">arɨː </ts>
               <ts e="T58" id="Seg_1426" n="e" s="T57">kɨtɨːtɨgar </ts>
               <ts e="T59" id="Seg_1428" n="e" s="T58">ustan </ts>
               <ts e="T60" id="Seg_1430" n="e" s="T59">kelbit, </ts>
               <ts e="T61" id="Seg_1432" n="e" s="T60">maska </ts>
               <ts e="T62" id="Seg_1434" n="e" s="T61">tu͡on </ts>
               <ts e="T63" id="Seg_1436" n="e" s="T62">keːher. </ts>
               <ts e="T64" id="Seg_1438" n="e" s="T63">Bu </ts>
               <ts e="T65" id="Seg_1440" n="e" s="T64">arɨːga </ts>
               <ts e="T66" id="Seg_1442" n="e" s="T65">taksɨbɨta </ts>
               <ts e="T67" id="Seg_1444" n="e" s="T66">mastaːk </ts>
               <ts e="T68" id="Seg_1446" n="e" s="T67">bagajɨ. </ts>
               <ts e="T69" id="Seg_1448" n="e" s="T68">Kanna </ts>
               <ts e="T70" id="Seg_1450" n="e" s="T69">ere </ts>
               <ts e="T71" id="Seg_1452" n="e" s="T70">hüge </ts>
               <ts e="T72" id="Seg_1454" n="e" s="T71">tɨ͡aha </ts>
               <ts e="T73" id="Seg_1456" n="e" s="T72">ihiller. </ts>
               <ts e="T74" id="Seg_1458" n="e" s="T73">Manna </ts>
               <ts e="T75" id="Seg_1460" n="e" s="T74">dʼe </ts>
               <ts e="T76" id="Seg_1462" n="e" s="T75">barɨ </ts>
               <ts e="T77" id="Seg_1464" n="e" s="T76">küːhün </ts>
               <ts e="T78" id="Seg_1466" n="e" s="T77">baran </ts>
               <ts e="T79" id="Seg_1468" n="e" s="T78">tijer. </ts>
               <ts e="T80" id="Seg_1470" n="e" s="T79">Tijen </ts>
               <ts e="T81" id="Seg_1472" n="e" s="T80">körbüte </ts>
               <ts e="T82" id="Seg_1474" n="e" s="T81">biːr </ts>
               <ts e="T83" id="Seg_1476" n="e" s="T82">oččugukkaːn </ts>
               <ts e="T84" id="Seg_1478" n="e" s="T83">kɨːs, </ts>
               <ts e="T85" id="Seg_1480" n="e" s="T84">kini͡eke </ts>
               <ts e="T86" id="Seg_1482" n="e" s="T85">köksütünen </ts>
               <ts e="T87" id="Seg_1484" n="e" s="T86">turan, </ts>
               <ts e="T88" id="Seg_1486" n="e" s="T87">mas </ts>
               <ts e="T89" id="Seg_1488" n="e" s="T88">kerde </ts>
               <ts e="T90" id="Seg_1490" n="e" s="T89">turar. </ts>
               <ts e="T91" id="Seg_1492" n="e" s="T90">Bu </ts>
               <ts e="T92" id="Seg_1494" n="e" s="T91">kɨːs </ts>
               <ts e="T93" id="Seg_1496" n="e" s="T92">kajɨhar, </ts>
               <ts e="T94" id="Seg_1498" n="e" s="T93">onto </ts>
               <ts e="T95" id="Seg_1500" n="e" s="T94">aŋaːr </ts>
               <ts e="T96" id="Seg_1502" n="e" s="T95">karaktaːk </ts>
               <ts e="T97" id="Seg_1504" n="e" s="T96">ebit. </ts>
               <ts e="T98" id="Seg_1506" n="e" s="T97">Kɨːha, </ts>
               <ts e="T99" id="Seg_1508" n="e" s="T98">kihini </ts>
               <ts e="T100" id="Seg_1510" n="e" s="T99">körön </ts>
               <ts e="T101" id="Seg_1512" n="e" s="T100">baran, </ts>
               <ts e="T102" id="Seg_1514" n="e" s="T101">kuttanan </ts>
               <ts e="T103" id="Seg_1516" n="e" s="T102">hügetin </ts>
               <ts e="T104" id="Seg_1518" n="e" s="T103">bɨragan </ts>
               <ts e="T105" id="Seg_1520" n="e" s="T104">hüːren </ts>
               <ts e="T106" id="Seg_1522" n="e" s="T105">barar. </ts>
               <ts e="T107" id="Seg_1524" n="e" s="T106">Hüːren </ts>
               <ts e="T108" id="Seg_1526" n="e" s="T107">barar </ts>
               <ts e="T109" id="Seg_1528" n="e" s="T108">da </ts>
               <ts e="T110" id="Seg_1530" n="e" s="T109">"onnugu </ts>
               <ts e="T111" id="Seg_1532" n="e" s="T110">kördüm" </ts>
               <ts e="T112" id="Seg_1534" n="e" s="T111">di͡en </ts>
               <ts e="T113" id="Seg_1536" n="e" s="T112">dʼonugar </ts>
               <ts e="T114" id="Seg_1538" n="e" s="T113">kepsiːr. </ts>
               <ts e="T115" id="Seg_1540" n="e" s="T114">Mantɨlara </ts>
               <ts e="T116" id="Seg_1542" n="e" s="T115">kas </ts>
               <ts e="T117" id="Seg_1544" n="e" s="T116">da </ts>
               <ts e="T118" id="Seg_1546" n="e" s="T117">kihi </ts>
               <ts e="T119" id="Seg_1548" n="e" s="T118">bu͡olan </ts>
               <ts e="T120" id="Seg_1550" n="e" s="T119">barannar </ts>
               <ts e="T121" id="Seg_1552" n="e" s="T120">hɨlajan </ts>
               <ts e="T122" id="Seg_1554" n="e" s="T121">oktubut </ts>
               <ts e="T123" id="Seg_1556" n="e" s="T122">kihilerin </ts>
               <ts e="T124" id="Seg_1558" n="e" s="T123">tiriːge </ts>
               <ts e="T125" id="Seg_1560" n="e" s="T124">kötögön </ts>
               <ts e="T126" id="Seg_1562" n="e" s="T125">egeleller. </ts>
               <ts e="T127" id="Seg_1564" n="e" s="T126">Ol </ts>
               <ts e="T128" id="Seg_1566" n="e" s="T127">egelbitteriger </ts>
               <ts e="T129" id="Seg_1568" n="e" s="T128">biːr </ts>
               <ts e="T130" id="Seg_1570" n="e" s="T129">kaːr </ts>
               <ts e="T131" id="Seg_1572" n="e" s="T130">kurduk </ts>
               <ts e="T132" id="Seg_1574" n="e" s="T131">astaːk </ts>
               <ts e="T133" id="Seg_1576" n="e" s="T132">ogonnʼor </ts>
               <ts e="T134" id="Seg_1578" n="e" s="T133">eter: </ts>
               <ts e="T135" id="Seg_1580" n="e" s="T134">Eː, </ts>
               <ts e="T136" id="Seg_1582" n="e" s="T135">tugu </ts>
               <ts e="T137" id="Seg_1584" n="e" s="T136">högögüt, </ts>
               <ts e="T138" id="Seg_1586" n="e" s="T137">oloŋkogo </ts>
               <ts e="T139" id="Seg_1588" n="e" s="T138">da </ts>
               <ts e="T140" id="Seg_1590" n="e" s="T139">baːr, </ts>
               <ts e="T141" id="Seg_1592" n="e" s="T140">bɨlɨrgɨ </ts>
               <ts e="T142" id="Seg_1594" n="e" s="T141">kepselge, </ts>
               <ts e="T143" id="Seg_1596" n="e" s="T142">hurukka </ts>
               <ts e="T144" id="Seg_1598" n="e" s="T143">da </ts>
               <ts e="T145" id="Seg_1600" n="e" s="T144">baːr, </ts>
               <ts e="T146" id="Seg_1602" n="e" s="T145">mannɨk </ts>
               <ts e="T147" id="Seg_1604" n="e" s="T146">ikki </ts>
               <ts e="T148" id="Seg_1606" n="e" s="T147">karaktaːk </ts>
               <ts e="T149" id="Seg_1608" n="e" s="T148">noru͡ot </ts>
               <ts e="T150" id="Seg_1610" n="e" s="T149">baːr </ts>
               <ts e="T151" id="Seg_1612" n="e" s="T150">di͡en, </ts>
               <ts e="T152" id="Seg_1614" n="e" s="T151">čejin, </ts>
               <ts e="T153" id="Seg_1616" n="e" s="T152">korgujbut </ts>
               <ts e="T154" id="Seg_1618" n="e" s="T153">kihi </ts>
               <ts e="T155" id="Seg_1620" n="e" s="T154">bɨhɨːtɨnan </ts>
               <ts e="T156" id="Seg_1622" n="e" s="T155">alɨs </ts>
               <ts e="T157" id="Seg_1624" n="e" s="T156">ahɨtɨmna, </ts>
               <ts e="T158" id="Seg_1626" n="e" s="T157">ahatan </ts>
               <ts e="T159" id="Seg_1628" n="e" s="T158">ihiŋ, </ts>
               <ts e="T160" id="Seg_1630" n="e" s="T159">tutajan </ts>
               <ts e="T161" id="Seg_1632" n="e" s="T160">bejetin </ts>
               <ts e="T162" id="Seg_1634" n="e" s="T161">hiritten </ts>
               <ts e="T163" id="Seg_1636" n="e" s="T162">munan </ts>
               <ts e="T164" id="Seg_1638" n="e" s="T163">kelbit </ts>
               <ts e="T165" id="Seg_1640" n="e" s="T164">baraksan </ts>
               <ts e="T166" id="Seg_1642" n="e" s="T165">bu͡olu͡o", </ts>
               <ts e="T167" id="Seg_1644" n="e" s="T166">diːr. </ts>
               <ts e="T168" id="Seg_1646" n="e" s="T167">Bu </ts>
               <ts e="T169" id="Seg_1648" n="e" s="T168">baːj </ts>
               <ts e="T170" id="Seg_1650" n="e" s="T169">bagajɨ </ts>
               <ts e="T171" id="Seg_1652" n="e" s="T170">noru͡ot </ts>
               <ts e="T172" id="Seg_1654" n="e" s="T171">ebit, </ts>
               <ts e="T173" id="Seg_1656" n="e" s="T172">hahɨllara, </ts>
               <ts e="T174" id="Seg_1658" n="e" s="T173">kɨrsalara </ts>
               <ts e="T175" id="Seg_1660" n="e" s="T174">dʼi͡elerin </ts>
               <ts e="T176" id="Seg_1662" n="e" s="T175">aːnɨgar </ts>
               <ts e="T177" id="Seg_1664" n="e" s="T176">bejete </ts>
               <ts e="T178" id="Seg_1666" n="e" s="T177">keler </ts>
               <ts e="T179" id="Seg_1668" n="e" s="T178">ebit. </ts>
               <ts e="T180" id="Seg_1670" n="e" s="T179">Baː </ts>
               <ts e="T181" id="Seg_1672" n="e" s="T180">kihi </ts>
               <ts e="T182" id="Seg_1674" n="e" s="T181">manna </ts>
               <ts e="T183" id="Seg_1676" n="e" s="T182">oloktonor. </ts>
               <ts e="T184" id="Seg_1678" n="e" s="T183">Oloktommut </ts>
               <ts e="T185" id="Seg_1680" n="e" s="T184">ɨ͡ala </ts>
               <ts e="T186" id="Seg_1682" n="e" s="T185">ikki </ts>
               <ts e="T187" id="Seg_1684" n="e" s="T186">kɨːstaːk </ts>
               <ts e="T188" id="Seg_1686" n="e" s="T187">ebitter, </ts>
               <ts e="T189" id="Seg_1688" n="e" s="T188">mantan </ts>
               <ts e="T190" id="Seg_1690" n="e" s="T189">kɨrdʼagahɨn </ts>
               <ts e="T191" id="Seg_1692" n="e" s="T190">agata </ts>
               <ts e="T192" id="Seg_1694" n="e" s="T191">ojok </ts>
               <ts e="T193" id="Seg_1696" n="e" s="T192">bi͡erer </ts>
               <ts e="T194" id="Seg_1698" n="e" s="T193">u͡olga. </ts>
               <ts e="T195" id="Seg_1700" n="e" s="T194">Manɨ͡aka </ts>
               <ts e="T196" id="Seg_1702" n="e" s="T195">baː </ts>
               <ts e="T197" id="Seg_1704" n="e" s="T196">köröːččü </ts>
               <ts e="T198" id="Seg_1706" n="e" s="T197">kɨːs </ts>
               <ts e="T199" id="Seg_1708" n="e" s="T198">elʼijiger </ts>
               <ts e="T200" id="Seg_1710" n="e" s="T199">künüːleːn: </ts>
               <ts e="T201" id="Seg_1712" n="e" s="T200">"Min </ts>
               <ts e="T202" id="Seg_1714" n="e" s="T201">barɨ͡aktaːk </ts>
               <ts e="T203" id="Seg_1716" n="e" s="T202">bejem </ts>
               <ts e="T204" id="Seg_1718" n="e" s="T203">buluːbar", </ts>
               <ts e="T205" id="Seg_1720" n="e" s="T204">di͡en </ts>
               <ts e="T206" id="Seg_1722" n="e" s="T205">ɨtɨːr. </ts>
               <ts e="T207" id="Seg_1724" n="e" s="T206">Onu </ts>
               <ts e="T208" id="Seg_1726" n="e" s="T207">istimne </ts>
               <ts e="T209" id="Seg_1728" n="e" s="T208">hin </ts>
               <ts e="T210" id="Seg_1730" n="e" s="T209">kɨrdʼas </ts>
               <ts e="T211" id="Seg_1732" n="e" s="T210">kɨːhɨ </ts>
               <ts e="T212" id="Seg_1734" n="e" s="T211">bi͡ereller </ts>
               <ts e="T213" id="Seg_1736" n="e" s="T212">u͡olga. </ts>
               <ts e="T214" id="Seg_1738" n="e" s="T213">Biːr </ts>
               <ts e="T215" id="Seg_1740" n="e" s="T214">hɨlɨnan </ts>
               <ts e="T216" id="Seg_1742" n="e" s="T215">ikki </ts>
               <ts e="T217" id="Seg_1744" n="e" s="T216">karaktaːk </ts>
               <ts e="T218" id="Seg_1746" n="e" s="T217">u͡ol </ts>
               <ts e="T219" id="Seg_1748" n="e" s="T218">ogo </ts>
               <ts e="T220" id="Seg_1750" n="e" s="T219">törüːr. </ts>
               <ts e="T221" id="Seg_1752" n="e" s="T220">Dʼe </ts>
               <ts e="T222" id="Seg_1754" n="e" s="T221">ör </ts>
               <ts e="T223" id="Seg_1756" n="e" s="T222">bagajɨ </ts>
               <ts e="T224" id="Seg_1758" n="e" s="T223">olorollor </ts>
               <ts e="T225" id="Seg_1760" n="e" s="T224">ogurduk. </ts>
               <ts e="T226" id="Seg_1762" n="e" s="T225">Bu </ts>
               <ts e="T227" id="Seg_1764" n="e" s="T226">aŋaːr </ts>
               <ts e="T228" id="Seg_1766" n="e" s="T227">karaktaːktar </ts>
               <ts e="T229" id="Seg_1768" n="e" s="T228">elbek </ts>
               <ts e="T230" id="Seg_1770" n="e" s="T229">bagajɨ </ts>
               <ts e="T231" id="Seg_1772" n="e" s="T230">noru͡ot </ts>
               <ts e="T232" id="Seg_1774" n="e" s="T231">ebitter. </ts>
               <ts e="T233" id="Seg_1776" n="e" s="T232">Biːrde </ts>
               <ts e="T234" id="Seg_1778" n="e" s="T233">karaːbɨnan </ts>
               <ts e="T235" id="Seg_1780" n="e" s="T234">ɨraːktaːgɨlara </ts>
               <ts e="T236" id="Seg_1782" n="e" s="T235">ölbüge </ts>
               <ts e="T237" id="Seg_1784" n="e" s="T236">komuja </ts>
               <ts e="T238" id="Seg_1786" n="e" s="T237">keler. </ts>
               <ts e="T239" id="Seg_1788" n="e" s="T238">Kinitten </ts>
               <ts e="T240" id="Seg_1790" n="e" s="T239">kihi </ts>
               <ts e="T241" id="Seg_1792" n="e" s="T240">keler </ts>
               <ts e="T242" id="Seg_1794" n="e" s="T241">baː </ts>
               <ts e="T243" id="Seg_1796" n="e" s="T242">kineːs </ts>
               <ts e="T244" id="Seg_1798" n="e" s="T243">kɨnnɨgar: </ts>
               <ts e="T245" id="Seg_1800" n="e" s="T244">"En </ts>
               <ts e="T246" id="Seg_1802" n="e" s="T245">ikki </ts>
               <ts e="T247" id="Seg_1804" n="e" s="T246">karaktaːk </ts>
               <ts e="T248" id="Seg_1806" n="e" s="T247">kütü͡ötteːk </ts>
               <ts e="T249" id="Seg_1808" n="e" s="T248">ühügün, </ts>
               <ts e="T250" id="Seg_1810" n="e" s="T249">en </ts>
               <ts e="T251" id="Seg_1812" n="e" s="T250">ontugun </ts>
               <ts e="T252" id="Seg_1814" n="e" s="T251">ɨraːktaːgɨ </ts>
               <ts e="T253" id="Seg_1816" n="e" s="T252">köröːrü </ts>
               <ts e="T254" id="Seg_1818" n="e" s="T253">gɨnar", </ts>
               <ts e="T255" id="Seg_1820" n="e" s="T254">diːr. </ts>
               <ts e="T256" id="Seg_1822" n="e" s="T255">Kɨnna </ts>
               <ts e="T257" id="Seg_1824" n="e" s="T256">eter: </ts>
               <ts e="T258" id="Seg_1826" n="e" s="T257">Kuːbam, </ts>
               <ts e="T259" id="Seg_1828" n="e" s="T258">bar </ts>
               <ts e="T260" id="Seg_1830" n="e" s="T259">ɨraːktaːgɨ </ts>
               <ts e="T261" id="Seg_1832" n="e" s="T260">körü͡ö </ts>
               <ts e="T262" id="Seg_1834" n="e" s="T261">ere </ts>
               <ts e="T263" id="Seg_1836" n="e" s="T262">diː, </ts>
               <ts e="T264" id="Seg_1838" n="e" s="T263">en </ts>
               <ts e="T265" id="Seg_1840" n="e" s="T264">bar, </ts>
               <ts e="T266" id="Seg_1842" n="e" s="T265">barbatakkɨna </ts>
               <ts e="T267" id="Seg_1844" n="e" s="T266">baspɨtɨn </ts>
               <ts e="T268" id="Seg_1846" n="e" s="T267">bɨhɨ͡a." </ts>
               <ts e="T269" id="Seg_1848" n="e" s="T268">Kineːs </ts>
               <ts e="T270" id="Seg_1850" n="e" s="T269">ɨraːktaːgɨga </ts>
               <ts e="T271" id="Seg_1852" n="e" s="T270">barar. </ts>
               <ts e="T272" id="Seg_1854" n="e" s="T271">Ontuta </ts>
               <ts e="T273" id="Seg_1856" n="e" s="T272">eter: </ts>
               <ts e="T274" id="Seg_1858" n="e" s="T273">"Min </ts>
               <ts e="T275" id="Seg_1860" n="e" s="T274">ejigin </ts>
               <ts e="T276" id="Seg_1862" n="e" s="T275">manna </ts>
               <ts e="T277" id="Seg_1864" n="e" s="T276">baːj </ts>
               <ts e="T278" id="Seg_1866" n="e" s="T277">kihi </ts>
               <ts e="T279" id="Seg_1868" n="e" s="T278">gɨnan </ts>
               <ts e="T280" id="Seg_1870" n="e" s="T279">teriji͡em, </ts>
               <ts e="T281" id="Seg_1872" n="e" s="T280">en </ts>
               <ts e="T282" id="Seg_1874" n="e" s="T281">manna </ts>
               <ts e="T283" id="Seg_1876" n="e" s="T282">kaːlɨ͡aŋ </ts>
               <ts e="T284" id="Seg_1878" n="e" s="T283">duː, </ts>
               <ts e="T285" id="Seg_1880" n="e" s="T284">dojduŋ </ts>
               <ts e="T286" id="Seg_1882" n="e" s="T285">di͡eg </ts>
               <ts e="T287" id="Seg_1884" n="e" s="T286">tönnü͡öŋ </ts>
               <ts e="T288" id="Seg_1886" n="e" s="T287">duː?" </ts>
               <ts e="T289" id="Seg_1888" n="e" s="T288">Onu: </ts>
               <ts e="T290" id="Seg_1890" n="e" s="T289">"Tönnü͡öm", </ts>
               <ts e="T291" id="Seg_1892" n="e" s="T290">di͡ete. </ts>
               <ts e="T292" id="Seg_1894" n="e" s="T291">ɨraːktaːgɨ </ts>
               <ts e="T293" id="Seg_1896" n="e" s="T292">onuga </ts>
               <ts e="T294" id="Seg_1898" n="e" s="T293">di͡ete: </ts>
               <ts e="T295" id="Seg_1900" n="e" s="T294">"ɨraːk </ts>
               <ts e="T296" id="Seg_1902" n="e" s="T295">hir, </ts>
               <ts e="T297" id="Seg_1904" n="e" s="T296">uː </ts>
               <ts e="T298" id="Seg_1906" n="e" s="T297">egelbitin </ts>
               <ts e="T299" id="Seg_1908" n="e" s="T298">kurduk </ts>
               <ts e="T300" id="Seg_1910" n="e" s="T299">barɨ͡aŋ </ts>
               <ts e="T301" id="Seg_1912" n="e" s="T300">hu͡oga. </ts>
               <ts e="T302" id="Seg_1914" n="e" s="T301">Min </ts>
               <ts e="T303" id="Seg_1916" n="e" s="T302">huruk </ts>
               <ts e="T304" id="Seg_1918" n="e" s="T303">bi͡eri͡em, </ts>
               <ts e="T305" id="Seg_1920" n="e" s="T304">kim </ts>
               <ts e="T306" id="Seg_1922" n="e" s="T305">da </ts>
               <ts e="T307" id="Seg_1924" n="e" s="T306">ejigin </ts>
               <ts e="T308" id="Seg_1926" n="e" s="T307">bolbu͡ota </ts>
               <ts e="T309" id="Seg_1928" n="e" s="T308">hu͡ok </ts>
               <ts e="T310" id="Seg_1930" n="e" s="T309">ildʼi͡e", </ts>
               <ts e="T311" id="Seg_1932" n="e" s="T310">di͡en </ts>
               <ts e="T312" id="Seg_1934" n="e" s="T311">huruk </ts>
               <ts e="T313" id="Seg_1936" n="e" s="T312">bi͡erer. </ts>
               <ts e="T314" id="Seg_1938" n="e" s="T313">Onu </ts>
               <ts e="T315" id="Seg_1940" n="e" s="T314">ɨlan </ts>
               <ts e="T316" id="Seg_1942" n="e" s="T315">baran </ts>
               <ts e="T317" id="Seg_1944" n="e" s="T316">hin </ts>
               <ts e="T318" id="Seg_1946" n="e" s="T317">kas </ts>
               <ts e="T319" id="Seg_1948" n="e" s="T318">da </ts>
               <ts e="T320" id="Seg_1950" n="e" s="T319">hɨl </ts>
               <ts e="T321" id="Seg_1952" n="e" s="T320">oloror. </ts>
               <ts e="T322" id="Seg_1954" n="e" s="T321">Bu </ts>
               <ts e="T323" id="Seg_1956" n="e" s="T322">oloron </ts>
               <ts e="T324" id="Seg_1958" n="e" s="T323">baran </ts>
               <ts e="T325" id="Seg_1960" n="e" s="T324">baraːrɨ </ts>
               <ts e="T326" id="Seg_1962" n="e" s="T325">teriner, </ts>
               <ts e="T327" id="Seg_1964" n="e" s="T326">dʼaktarɨn </ts>
               <ts e="T328" id="Seg_1966" n="e" s="T327">ɨlan. </ts>
               <ts e="T329" id="Seg_1968" n="e" s="T328">Manɨ͡aga </ts>
               <ts e="T330" id="Seg_1970" n="e" s="T329">ɨlgɨn </ts>
               <ts e="T331" id="Seg_1972" n="e" s="T330">kɨːs </ts>
               <ts e="T332" id="Seg_1974" n="e" s="T331">ɨtaːn </ts>
               <ts e="T333" id="Seg_1976" n="e" s="T332">barar: </ts>
               <ts e="T334" id="Seg_1978" n="e" s="T333">"Min </ts>
               <ts e="T335" id="Seg_1980" n="e" s="T334">bulbut </ts>
               <ts e="T336" id="Seg_1982" n="e" s="T335">kihibitten </ts>
               <ts e="T337" id="Seg_1984" n="e" s="T336">kaːlbappɨn, </ts>
               <ts e="T338" id="Seg_1986" n="e" s="T337">biːrge </ts>
               <ts e="T339" id="Seg_1988" n="e" s="T338">barsɨ͡am. </ts>
               <ts e="T340" id="Seg_1990" n="e" s="T339">Agahɨgar </ts>
               <ts e="T341" id="Seg_1992" n="e" s="T340">eter: </ts>
               <ts e="T342" id="Seg_1994" n="e" s="T341">"En </ts>
               <ts e="T343" id="Seg_1996" n="e" s="T342">körör </ts>
               <ts e="T344" id="Seg_1998" n="e" s="T343">gɨnɨ͡akkɨn </ts>
               <ts e="T345" id="Seg_2000" n="e" s="T344">ikki </ts>
               <ts e="T346" id="Seg_2002" n="e" s="T345">karaktaːk </ts>
               <ts e="T347" id="Seg_2004" n="e" s="T346">ogoloːkkun. </ts>
               <ts e="T348" id="Seg_2006" n="e" s="T347">Min </ts>
               <ts e="T349" id="Seg_2008" n="e" s="T348">barsɨ͡am </ts>
               <ts e="T350" id="Seg_2010" n="e" s="T349">kinini </ts>
               <ts e="T351" id="Seg_2012" n="e" s="T350">gɨtta." </ts>
               <ts e="T352" id="Seg_2014" n="e" s="T351">Agata </ts>
               <ts e="T353" id="Seg_2016" n="e" s="T352">hübeleːn </ts>
               <ts e="T354" id="Seg_2018" n="e" s="T353">köŋüllüːr. </ts>
               <ts e="T355" id="Seg_2020" n="e" s="T354">Onon </ts>
               <ts e="T356" id="Seg_2022" n="e" s="T355">ɨlgɨn </ts>
               <ts e="T357" id="Seg_2024" n="e" s="T356">kɨːs </ts>
               <ts e="T358" id="Seg_2026" n="e" s="T357">biːrge </ts>
               <ts e="T359" id="Seg_2028" n="e" s="T358">barsar. </ts>
               <ts e="T360" id="Seg_2030" n="e" s="T359">Dʼe </ts>
               <ts e="T361" id="Seg_2032" n="e" s="T360">ör </ts>
               <ts e="T362" id="Seg_2034" n="e" s="T361">bagajɨ </ts>
               <ts e="T363" id="Seg_2036" n="e" s="T362">ajannaːn </ts>
               <ts e="T364" id="Seg_2038" n="e" s="T363">tijeller </ts>
               <ts e="T365" id="Seg_2040" n="e" s="T364">dojdularɨgar. </ts>
               <ts e="T366" id="Seg_2042" n="e" s="T365">Ogurduk </ts>
               <ts e="T367" id="Seg_2044" n="e" s="T366">ös </ts>
               <ts e="T368" id="Seg_2046" n="e" s="T367">baːr. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_2047" s="T0">BaA_1930_OneEyedGirl_flk.001 (001.001)</ta>
            <ta e="T10" id="Seg_2048" s="T6">BaA_1930_OneEyedGirl_flk.002 (001.002)</ta>
            <ta e="T18" id="Seg_2049" s="T10">BaA_1930_OneEyedGirl_flk.003 (001.003)</ta>
            <ta e="T27" id="Seg_2050" s="T18">BaA_1930_OneEyedGirl_flk.004 (001.004)</ta>
            <ta e="T39" id="Seg_2051" s="T27">BaA_1930_OneEyedGirl_flk.005 (001.005)</ta>
            <ta e="T47" id="Seg_2052" s="T39">BaA_1930_OneEyedGirl_flk.006 (001.006)</ta>
            <ta e="T52" id="Seg_2053" s="T47">BaA_1930_OneEyedGirl_flk.007 (001.007)</ta>
            <ta e="T63" id="Seg_2054" s="T52">BaA_1930_OneEyedGirl_flk.008 (001.008)</ta>
            <ta e="T68" id="Seg_2055" s="T63">BaA_1930_OneEyedGirl_flk.009 (001.009)</ta>
            <ta e="T73" id="Seg_2056" s="T68">BaA_1930_OneEyedGirl_flk.010 (001.010)</ta>
            <ta e="T79" id="Seg_2057" s="T73">BaA_1930_OneEyedGirl_flk.011 (001.011)</ta>
            <ta e="T90" id="Seg_2058" s="T79">BaA_1930_OneEyedGirl_flk.012 (001.012)</ta>
            <ta e="T97" id="Seg_2059" s="T90">BaA_1930_OneEyedGirl_flk.013 (001.013)</ta>
            <ta e="T106" id="Seg_2060" s="T97">BaA_1930_OneEyedGirl_flk.014 (001.014)</ta>
            <ta e="T114" id="Seg_2061" s="T106">BaA_1930_OneEyedGirl_flk.015 (001.015)</ta>
            <ta e="T126" id="Seg_2062" s="T114">BaA_1930_OneEyedGirl_flk.016 (001.016)</ta>
            <ta e="T134" id="Seg_2063" s="T126">BaA_1930_OneEyedGirl_flk.017 (001.017)</ta>
            <ta e="T167" id="Seg_2064" s="T134">BaA_1930_OneEyedGirl_flk.018 (001.018)</ta>
            <ta e="T179" id="Seg_2065" s="T167">BaA_1930_OneEyedGirl_flk.019 (001.019)</ta>
            <ta e="T183" id="Seg_2066" s="T179">BaA_1930_OneEyedGirl_flk.020 (001.020)</ta>
            <ta e="T194" id="Seg_2067" s="T183">BaA_1930_OneEyedGirl_flk.021 (001.021)</ta>
            <ta e="T200" id="Seg_2068" s="T194">BaA_1930_OneEyedGirl_flk.022 (001.022)</ta>
            <ta e="T206" id="Seg_2069" s="T200">BaA_1930_OneEyedGirl_flk.023 (001.023)</ta>
            <ta e="T213" id="Seg_2070" s="T206">BaA_1930_OneEyedGirl_flk.024 (001.024)</ta>
            <ta e="T220" id="Seg_2071" s="T213">BaA_1930_OneEyedGirl_flk.025 (001.025)</ta>
            <ta e="T225" id="Seg_2072" s="T220">BaA_1930_OneEyedGirl_flk.026 (001.026)</ta>
            <ta e="T232" id="Seg_2073" s="T225">BaA_1930_OneEyedGirl_flk.027 (001.027)</ta>
            <ta e="T238" id="Seg_2074" s="T232">BaA_1930_OneEyedGirl_flk.028 (001.028)</ta>
            <ta e="T244" id="Seg_2075" s="T238">BaA_1930_OneEyedGirl_flk.029 (001.029)</ta>
            <ta e="T255" id="Seg_2076" s="T244">BaA_1930_OneEyedGirl_flk.030 (001.030)</ta>
            <ta e="T257" id="Seg_2077" s="T255">BaA_1930_OneEyedGirl_flk.031 (001.031)</ta>
            <ta e="T268" id="Seg_2078" s="T257">BaA_1930_OneEyedGirl_flk.032 (001.032)</ta>
            <ta e="T271" id="Seg_2079" s="T268">BaA_1930_OneEyedGirl_flk.033 (001.033)</ta>
            <ta e="T273" id="Seg_2080" s="T271">BaA_1930_OneEyedGirl_flk.034 (001.034)</ta>
            <ta e="T288" id="Seg_2081" s="T273">BaA_1930_OneEyedGirl_flk.035 (001.035)</ta>
            <ta e="T289" id="Seg_2082" s="T288">BaA_1930_OneEyedGirl_flk.036 (001.036)</ta>
            <ta e="T291" id="Seg_2083" s="T289">BaA_1930_OneEyedGirl_flk.037 (001.037)</ta>
            <ta e="T294" id="Seg_2084" s="T291">BaA_1930_OneEyedGirl_flk.038 (001.038)</ta>
            <ta e="T301" id="Seg_2085" s="T294">BaA_1930_OneEyedGirl_flk.039 (001.039)</ta>
            <ta e="T313" id="Seg_2086" s="T301">BaA_1930_OneEyedGirl_flk.040 (001.040)</ta>
            <ta e="T321" id="Seg_2087" s="T313">BaA_1930_OneEyedGirl_flk.041 (001.041)</ta>
            <ta e="T328" id="Seg_2088" s="T321">BaA_1930_OneEyedGirl_flk.042 (001.042)</ta>
            <ta e="T333" id="Seg_2089" s="T328">BaA_1930_OneEyedGirl_flk.043 (001.043)</ta>
            <ta e="T339" id="Seg_2090" s="T333">BaA_1930_OneEyedGirl_flk.044 (001.044)</ta>
            <ta e="T341" id="Seg_2091" s="T339">BaA_1930_OneEyedGirl_flk.045 (001.045)</ta>
            <ta e="T347" id="Seg_2092" s="T341">BaA_1930_OneEyedGirl_flk.046 (001.046)</ta>
            <ta e="T351" id="Seg_2093" s="T347">BaA_1930_OneEyedGirl_flk.047 (001.047)</ta>
            <ta e="T354" id="Seg_2094" s="T351">BaA_1930_OneEyedGirl_flk.048 (001.048)</ta>
            <ta e="T359" id="Seg_2095" s="T354">BaA_1930_OneEyedGirl_flk.049 (001.049)</ta>
            <ta e="T365" id="Seg_2096" s="T359">BaA_1930_OneEyedGirl_flk.050 (001.050)</ta>
            <ta e="T368" id="Seg_2097" s="T365">BaA_1930_OneEyedGirl_flk.051 (001.051)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_2098" s="T0">Былыр биир уоллаак дьураак кинээһэ олорбут.</ta>
            <ta e="T10" id="Seg_2099" s="T6">Кыһын байгалга дьураактар бөлүүгэлээччилэр. </ta>
            <ta e="T18" id="Seg_2100" s="T10">Маа кинээс байгал бууһунан бөлүүгэ ойбоно аллара барар.</ta>
            <ta e="T27" id="Seg_2101" s="T18">Бу гынан баран төннөөрү гыммыта бууһун байгал алдьатан кээспит.</ta>
            <ta e="T39" id="Seg_2102" s="T27">Үс күн көрдүүр таксар һирин, муҥа буолан үс табатыттан биирин һогудаайдаан һиир.</ta>
            <ta e="T47" id="Seg_2103" s="T39">Онтон эмиэ каама һатаан икки табатын барыыр, өлөрөр.</ta>
            <ta e="T52" id="Seg_2104" s="T47">Бууһа ойун дүҥүрүн һага буолар.</ta>
            <ta e="T63" id="Seg_2105" s="T52">Ону балкыыр кайдак эрэ арыы кытыытыгар устан кэлбит, маска туон кээһэр.</ta>
            <ta e="T68" id="Seg_2106" s="T63">Бу арыыга таксыбыта мастаак багайы.</ta>
            <ta e="T73" id="Seg_2107" s="T68">Канна эрэ һүгэ тыаһа иһиллэр.</ta>
            <ta e="T79" id="Seg_2108" s="T73">Манна дьэ бары күүһүн[эн] баран тийэр.</ta>
            <ta e="T90" id="Seg_2109" s="T79">Тийэн көрбүтэ биир оччугуккаан кыыс, киниэкэ көксүтүнэн туран, мас кэрдэ турар.</ta>
            <ta e="T97" id="Seg_2110" s="T90">Бу кыыс кайыһар, онто аҥаар карактаак эбит.</ta>
            <ta e="T106" id="Seg_2111" s="T97">Кыыһа, киһини көрөн баран, куттанан һүгэтин быраган һүүрэн барар.</ta>
            <ta e="T114" id="Seg_2112" s="T106">Һүүрэн барар да "оннугу көрдүм" диэн дьонугар кэпсиир.</ta>
            <ta e="T126" id="Seg_2113" s="T114">Мантылара кас да киһи буолан бараннар һылайан октубут киһилэрин тириигэ көтөгөн эгэлэллэр.</ta>
            <ta e="T134" id="Seg_2114" s="T126">Ол эгэлбиттэригэр биир каар курдук астаак огонньор этэр:</ta>
            <ta e="T167" id="Seg_2115" s="T134">— Ээ, тугу һөгөгүт, олоҥкого да баар, былыргы кэпсэлгэ, һурукка да баар: маннык икки карактаак норуот баар диэн, чэйин, коргуйбут киһи быһыытынан алыс аһытымна, аһатан иһиҥ, тутайан бэйэтин һириттэн мунан кэлбит бараксан буолуо, — диир.</ta>
            <ta e="T179" id="Seg_2116" s="T167">Бу баай багайы норуот эбит, һаһыллара, кырсалара дьиэлэрин ааныгар бэйэтэ кэлэр эбит.</ta>
            <ta e="T183" id="Seg_2117" s="T179">Баа киһи манна олоктонор.</ta>
            <ta e="T194" id="Seg_2118" s="T183">Олоктоммут ыала икки кыыстаак эбиттэр, мантан кырдьагаһын агата ойок биэрэр уолга.</ta>
            <ta e="T200" id="Seg_2119" s="T194">Маныака баа көрөөччү кыыс эльийигэр күнүүлээн:</ta>
            <ta e="T206" id="Seg_2120" s="T200">— Мин барыактаак[пын] бэйэм булуубар, — диэн ытыыр.</ta>
            <ta e="T213" id="Seg_2121" s="T206">Ону истимнэ һин кырдьа[га]с кыыһы биэрэллэр уолга.</ta>
            <ta e="T220" id="Seg_2122" s="T213">Биир һылынан икки карактаак уол ого төрүүр.</ta>
            <ta e="T225" id="Seg_2123" s="T220">Дьэ өр багайы олороллор огурдук.</ta>
            <ta e="T232" id="Seg_2124" s="T225">Бу аҥаар карактаактар элбэк багайы норуот эбиттэр.</ta>
            <ta e="T238" id="Seg_2125" s="T232">Биирдэ караабынан ыраактаагылара өлбүгэ комуйа кэлэр.</ta>
            <ta e="T244" id="Seg_2126" s="T238">Киниттэн киһи кэлэр баа кинээс кынныгар:</ta>
            <ta e="T255" id="Seg_2127" s="T244">— Эн икки карактаак күтүөттээк үһүгүн, эн онтугун ыраактаагы көрөөрү гынар, — диир.</ta>
            <ta e="T257" id="Seg_2128" s="T255">Кынна этэр:</ta>
            <ta e="T268" id="Seg_2129" s="T257">— Куубам, бар ыраактаагы көрүө эрэ дии, эн бар, барбатаккына баспытын быһыа.</ta>
            <ta e="T271" id="Seg_2130" s="T268">Кинээс ыраактаагыга барар.</ta>
            <ta e="T273" id="Seg_2131" s="T271">Онтута этэр:</ta>
            <ta e="T288" id="Seg_2132" s="T273">— Мин эйигин манна баай киһи гынан тэрийиэм, эн манна каалыаҥ дуу, дойдуҥ диэг төннүөҥ дуу?</ta>
            <ta e="T289" id="Seg_2133" s="T288">Ону:</ta>
            <ta e="T291" id="Seg_2134" s="T289">— Төннүөм, — диэтэ.</ta>
            <ta e="T294" id="Seg_2135" s="T291">Ыраактаагы онуга диэтэ:</ta>
            <ta e="T301" id="Seg_2136" s="T294">— Ыраак һир, уу эгэлбитин курдук барыаҥ һуога.</ta>
            <ta e="T313" id="Seg_2137" s="T301">Мин һурук биэриэм, ким да эйигин болбуота һуок илдьиэ, — диэн һурук биэрэр.</ta>
            <ta e="T321" id="Seg_2138" s="T313">Ону ылан баран һин кас да һыл олорор.</ta>
            <ta e="T328" id="Seg_2139" s="T321">Бу олорон баран бараары тэринэр, дьактарын ылан.</ta>
            <ta e="T333" id="Seg_2140" s="T328">Маныага ылгын кыыс ытаан барар:</ta>
            <ta e="T339" id="Seg_2141" s="T333">— Мин булбут киһибиттэн каалбаппын, бииргэ барсыам.</ta>
            <ta e="T341" id="Seg_2142" s="T339">Агаһыгар этэр:</ta>
            <ta e="T347" id="Seg_2143" s="T341">— Эн көрөр гыныаккын икки карактаак оголооккун.</ta>
            <ta e="T351" id="Seg_2144" s="T347">Мин барсыам кинини гытта.</ta>
            <ta e="T354" id="Seg_2145" s="T351">Агата һүбэлээн көҥүллүүр.</ta>
            <ta e="T359" id="Seg_2146" s="T354">Онон ылгын кыыс бииргэ барсар.</ta>
            <ta e="T365" id="Seg_2147" s="T359">Дьэ өр багайы айаннаан тийэллэр дойдуларыгар.</ta>
            <ta e="T368" id="Seg_2148" s="T365">Огурдук өс баар.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_2149" s="T0">Bɨlɨr biːr u͡ollaːk dʼuraːk kineːhe olorbut. </ta>
            <ta e="T10" id="Seg_2150" s="T6">Kɨhɨn bajgalga dʼuraːktar bölüːgeleːččiler. </ta>
            <ta e="T18" id="Seg_2151" s="T10">Maː kineːs bajgal buːhunan bölüːge ojbono allara barar. </ta>
            <ta e="T27" id="Seg_2152" s="T18">Bu gɨnan baran tönnöːrü gɨmmɨta buːhun bajgal aldʼatan keːspit. </ta>
            <ta e="T39" id="Seg_2153" s="T27">Üs kün kördüːr taksar hirin, muŋa bu͡olan üs tabatɨttan biːrin hogudaːjdaːn hiːr. </ta>
            <ta e="T47" id="Seg_2154" s="T39">Onton emi͡e kaːma hataːn ikki tabatɨn barɨːr, ölörör. </ta>
            <ta e="T52" id="Seg_2155" s="T47">Buːha ojun düŋürün haga bu͡olar. </ta>
            <ta e="T63" id="Seg_2156" s="T52">Onu balkɨːr kajdak ere arɨː kɨtɨːtɨgar ustan kelbit, maska tu͡on keːher. </ta>
            <ta e="T68" id="Seg_2157" s="T63">Bu arɨːga taksɨbɨta mastaːk bagajɨ. </ta>
            <ta e="T73" id="Seg_2158" s="T68">Kanna ere hüge tɨ͡aha ihiller. </ta>
            <ta e="T79" id="Seg_2159" s="T73">Manna dʼe barɨ küːhün baran tijer. </ta>
            <ta e="T90" id="Seg_2160" s="T79">Tijen körbüte biːr oččugukkaːn kɨːs, kini͡eke köksütünen turan, mas kerde turar. </ta>
            <ta e="T97" id="Seg_2161" s="T90">Bu kɨːs kajɨhar, onto aŋaːr karaktaːk ebit. </ta>
            <ta e="T106" id="Seg_2162" s="T97">Kɨːha, kihini körön baran, kuttanan hügetin bɨragan hüːren barar. </ta>
            <ta e="T114" id="Seg_2163" s="T106">Hüːren barar da "onnugu kördüm" di͡en dʼonugar kepsiːr. </ta>
            <ta e="T126" id="Seg_2164" s="T114">Mantɨlara kas da kihi bu͡olan barannar hɨlajan oktubut kihilerin tiriːge kötögön egeleller. </ta>
            <ta e="T134" id="Seg_2165" s="T126">Ol egelbitteriger biːr kaːr kurduk astaːk ogonnʼor eter:</ta>
            <ta e="T167" id="Seg_2166" s="T134">"Eː, tugu högögüt, oloŋkogo da baːr, bɨlɨrgɨ kepselge, hurukka da baːr, mannɨk ikki karaktaːk noru͡ot baːr di͡en, čejin, korgujbut kihi bɨhɨːtɨnan alɨs ahɨtɨmna, ahatan ihiŋ, tutajan bejetin hiritten munan kelbit baraksan bu͡olu͡o", diːr. </ta>
            <ta e="T179" id="Seg_2167" s="T167">Bu baːj bagajɨ noru͡ot ebit, hahɨllara, kɨrsalara dʼi͡elerin aːnɨgar bejete keler ebit. </ta>
            <ta e="T183" id="Seg_2168" s="T179">Baː kihi manna oloktonor. </ta>
            <ta e="T194" id="Seg_2169" s="T183">Oloktommut ɨ͡ala ikki kɨːstaːk ebitter, mantan kɨrdʼagahɨn agata ojok bi͡erer u͡olga. </ta>
            <ta e="T200" id="Seg_2170" s="T194">Manɨ͡aka baː köröːččü kɨːs elʼijiger künüːleːn:</ta>
            <ta e="T206" id="Seg_2171" s="T200">"Min barɨ͡aktaːk bejem buluːbar", di͡en ɨtɨːr. </ta>
            <ta e="T213" id="Seg_2172" s="T206">Onu istimne hin kɨrdʼas kɨːhɨ bi͡ereller u͡olga. </ta>
            <ta e="T220" id="Seg_2173" s="T213">Biːr hɨlɨnan ikki karaktaːk u͡ol ogo törüːr. </ta>
            <ta e="T225" id="Seg_2174" s="T220">Dʼe ör bagajɨ olorollor ogurduk. </ta>
            <ta e="T232" id="Seg_2175" s="T225">Bu aŋaːr karaktaːktar elbek bagajɨ noru͡ot ebitter. </ta>
            <ta e="T238" id="Seg_2176" s="T232">Biːrde karaːbɨnan ɨraːktaːgɨlara ölbüge komuja keler. </ta>
            <ta e="T244" id="Seg_2177" s="T238">Kinitten kihi keler baː kineːs kɨnnɨgar:</ta>
            <ta e="T255" id="Seg_2178" s="T244">"En ikki karaktaːk kütü͡ötteːk ühügün, en ontugun ɨraːktaːgɨ köröːrü gɨnar", diːr. </ta>
            <ta e="T257" id="Seg_2179" s="T255">Kɨnna eter:</ta>
            <ta e="T268" id="Seg_2180" s="T257">"Kuːbam, bar ɨraːktaːgɨ körü͡ö ere diː, en bar, barbatakkɨna baspɨtɨn bɨhɨ͡a." </ta>
            <ta e="T271" id="Seg_2181" s="T268">Kineːs ɨraːktaːgɨga barar. </ta>
            <ta e="T273" id="Seg_2182" s="T271">Ontuta eter:</ta>
            <ta e="T288" id="Seg_2183" s="T273">"Min ejigin manna baːj kihi gɨnan teriji͡em, en manna kaːlɨ͡aŋ duː, dojduŋ di͡eg tönnü͡öŋ duː?" </ta>
            <ta e="T289" id="Seg_2184" s="T288">Onu:</ta>
            <ta e="T291" id="Seg_2185" s="T289">"Tönnü͡öm", di͡ete. </ta>
            <ta e="T294" id="Seg_2186" s="T291">ɨraːktaːgɨ onuga di͡ete:</ta>
            <ta e="T301" id="Seg_2187" s="T294">"ɨraːk hir, uː egelbitin kurduk barɨ͡aŋ hu͡oga. </ta>
            <ta e="T313" id="Seg_2188" s="T301">Min huruk bi͡eri͡em, kim da ejigin bolbu͡ota hu͡ok ildʼi͡e", di͡en huruk bi͡erer. </ta>
            <ta e="T321" id="Seg_2189" s="T313">Onu ɨlan baran hin kas da hɨl oloror. </ta>
            <ta e="T328" id="Seg_2190" s="T321">Bu oloron baran baraːrɨ teriner, dʼaktarɨn ɨlan. </ta>
            <ta e="T333" id="Seg_2191" s="T328">Manɨ͡aga ɨlgɨn kɨːs ɨtaːn barar:</ta>
            <ta e="T339" id="Seg_2192" s="T333">"Min bulbut kihibitten kaːlbappɨn, biːrge barsɨ͡am." </ta>
            <ta e="T341" id="Seg_2193" s="T339">Agahɨgar eter:</ta>
            <ta e="T347" id="Seg_2194" s="T341">"En körör gɨnɨ͡akkɨn ikki karaktaːk ogoloːkkun. </ta>
            <ta e="T351" id="Seg_2195" s="T347">Min barsɨ͡am kinini gɨtta." </ta>
            <ta e="T354" id="Seg_2196" s="T351">Agata hübeleːn köŋüllüːr. </ta>
            <ta e="T359" id="Seg_2197" s="T354">Onon ɨlgɨn kɨːs biːrge barsar. </ta>
            <ta e="T365" id="Seg_2198" s="T359">Dʼe ör bagajɨ ajannaːn tijeller dojdularɨgar. </ta>
            <ta e="T368" id="Seg_2199" s="T365">Ogurduk ös baːr. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_2200" s="T0">bɨlɨr</ta>
            <ta e="T2" id="Seg_2201" s="T1">biːr</ta>
            <ta e="T3" id="Seg_2202" s="T2">u͡ol-laːk</ta>
            <ta e="T4" id="Seg_2203" s="T3">dʼuraːk</ta>
            <ta e="T5" id="Seg_2204" s="T4">kineːh-e</ta>
            <ta e="T6" id="Seg_2205" s="T5">olor-but</ta>
            <ta e="T7" id="Seg_2206" s="T6">kɨhɨn</ta>
            <ta e="T8" id="Seg_2207" s="T7">bajgal-ga</ta>
            <ta e="T9" id="Seg_2208" s="T8">dʼuraːk-tar</ta>
            <ta e="T10" id="Seg_2209" s="T9">bölüːge-leː-čči-ler</ta>
            <ta e="T11" id="Seg_2210" s="T10">maː</ta>
            <ta e="T12" id="Seg_2211" s="T11">kineːs</ta>
            <ta e="T13" id="Seg_2212" s="T12">bajgal</ta>
            <ta e="T14" id="Seg_2213" s="T13">buːh-u-nan</ta>
            <ta e="T15" id="Seg_2214" s="T14">bölüːge</ta>
            <ta e="T16" id="Seg_2215" s="T15">ojbon-o</ta>
            <ta e="T17" id="Seg_2216" s="T16">allar-a</ta>
            <ta e="T18" id="Seg_2217" s="T17">bar-ar</ta>
            <ta e="T19" id="Seg_2218" s="T18">bu</ta>
            <ta e="T20" id="Seg_2219" s="T19">gɨn-an</ta>
            <ta e="T21" id="Seg_2220" s="T20">baran</ta>
            <ta e="T22" id="Seg_2221" s="T21">tönn-öːrü</ta>
            <ta e="T23" id="Seg_2222" s="T22">gɨm-mɨt-a</ta>
            <ta e="T24" id="Seg_2223" s="T23">buːh-u-n</ta>
            <ta e="T25" id="Seg_2224" s="T24">bajgal</ta>
            <ta e="T26" id="Seg_2225" s="T25">aldʼat-an</ta>
            <ta e="T27" id="Seg_2226" s="T26">keːs-pit</ta>
            <ta e="T28" id="Seg_2227" s="T27">üs</ta>
            <ta e="T29" id="Seg_2228" s="T28">kün</ta>
            <ta e="T30" id="Seg_2229" s="T29">kördüː-r</ta>
            <ta e="T31" id="Seg_2230" s="T30">taks-ar</ta>
            <ta e="T32" id="Seg_2231" s="T31">hir-i-n</ta>
            <ta e="T33" id="Seg_2232" s="T32">muŋ-a</ta>
            <ta e="T34" id="Seg_2233" s="T33">bu͡ol-an</ta>
            <ta e="T35" id="Seg_2234" s="T34">üs</ta>
            <ta e="T36" id="Seg_2235" s="T35">taba-tɨ-ttan</ta>
            <ta e="T37" id="Seg_2236" s="T36">biːr-i-n</ta>
            <ta e="T38" id="Seg_2237" s="T37">hogudaːj-daː-n</ta>
            <ta e="T39" id="Seg_2238" s="T38">hiː-r</ta>
            <ta e="T40" id="Seg_2239" s="T39">onton</ta>
            <ta e="T41" id="Seg_2240" s="T40">emi͡e</ta>
            <ta e="T42" id="Seg_2241" s="T41">kaːm-a</ta>
            <ta e="T43" id="Seg_2242" s="T42">hataː-n</ta>
            <ta e="T44" id="Seg_2243" s="T43">ikki</ta>
            <ta e="T45" id="Seg_2244" s="T44">taba-tɨ-n</ta>
            <ta e="T46" id="Seg_2245" s="T45">barɨː-r</ta>
            <ta e="T47" id="Seg_2246" s="T46">ölör-ör</ta>
            <ta e="T48" id="Seg_2247" s="T47">buːh-a</ta>
            <ta e="T49" id="Seg_2248" s="T48">ojun</ta>
            <ta e="T50" id="Seg_2249" s="T49">düŋür-ü-n</ta>
            <ta e="T51" id="Seg_2250" s="T50">haga</ta>
            <ta e="T52" id="Seg_2251" s="T51">bu͡ol-ar</ta>
            <ta e="T53" id="Seg_2252" s="T52">o-nu</ta>
            <ta e="T54" id="Seg_2253" s="T53">balkɨːr</ta>
            <ta e="T55" id="Seg_2254" s="T54">kajdak</ta>
            <ta e="T56" id="Seg_2255" s="T55">ere</ta>
            <ta e="T57" id="Seg_2256" s="T56">arɨː</ta>
            <ta e="T58" id="Seg_2257" s="T57">kɨtɨː-tɨ-gar</ta>
            <ta e="T59" id="Seg_2258" s="T58">ust-an</ta>
            <ta e="T60" id="Seg_2259" s="T59">kel-bit</ta>
            <ta e="T61" id="Seg_2260" s="T60">mas-ka</ta>
            <ta e="T62" id="Seg_2261" s="T61">tu͡on</ta>
            <ta e="T63" id="Seg_2262" s="T62">keːh-er</ta>
            <ta e="T64" id="Seg_2263" s="T63">bu</ta>
            <ta e="T65" id="Seg_2264" s="T64">arɨː-ga</ta>
            <ta e="T66" id="Seg_2265" s="T65">taks-ɨ-bɨt-a</ta>
            <ta e="T67" id="Seg_2266" s="T66">mas-taːk</ta>
            <ta e="T68" id="Seg_2267" s="T67">bagajɨ</ta>
            <ta e="T69" id="Seg_2268" s="T68">kanna</ta>
            <ta e="T70" id="Seg_2269" s="T69">ere</ta>
            <ta e="T71" id="Seg_2270" s="T70">hüge</ta>
            <ta e="T72" id="Seg_2271" s="T71">tɨ͡ah-a</ta>
            <ta e="T73" id="Seg_2272" s="T72">ihill-er</ta>
            <ta e="T74" id="Seg_2273" s="T73">manna</ta>
            <ta e="T75" id="Seg_2274" s="T74">dʼe</ta>
            <ta e="T76" id="Seg_2275" s="T75">barɨ</ta>
            <ta e="T77" id="Seg_2276" s="T76">küːh-ü-n</ta>
            <ta e="T78" id="Seg_2277" s="T77">bar-an</ta>
            <ta e="T79" id="Seg_2278" s="T78">tij-er</ta>
            <ta e="T80" id="Seg_2279" s="T79">tij-en</ta>
            <ta e="T81" id="Seg_2280" s="T80">kör-büt-e</ta>
            <ta e="T82" id="Seg_2281" s="T81">biːr</ta>
            <ta e="T83" id="Seg_2282" s="T82">oččuguk-kaːn</ta>
            <ta e="T84" id="Seg_2283" s="T83">kɨːs</ta>
            <ta e="T85" id="Seg_2284" s="T84">kini͡e-ke</ta>
            <ta e="T86" id="Seg_2285" s="T85">köksü-tü-nen</ta>
            <ta e="T87" id="Seg_2286" s="T86">tur-an</ta>
            <ta e="T88" id="Seg_2287" s="T87">mas</ta>
            <ta e="T89" id="Seg_2288" s="T88">kerd-e</ta>
            <ta e="T90" id="Seg_2289" s="T89">tur-ar</ta>
            <ta e="T91" id="Seg_2290" s="T90">bu</ta>
            <ta e="T92" id="Seg_2291" s="T91">kɨːs</ta>
            <ta e="T93" id="Seg_2292" s="T92">kaj-ɨ-h-ar</ta>
            <ta e="T94" id="Seg_2293" s="T93">onto</ta>
            <ta e="T95" id="Seg_2294" s="T94">aŋaːr</ta>
            <ta e="T96" id="Seg_2295" s="T95">karak-taːk</ta>
            <ta e="T97" id="Seg_2296" s="T96">e-bit</ta>
            <ta e="T98" id="Seg_2297" s="T97">kɨːh-a</ta>
            <ta e="T99" id="Seg_2298" s="T98">kihi-ni</ta>
            <ta e="T100" id="Seg_2299" s="T99">kör-ön</ta>
            <ta e="T101" id="Seg_2300" s="T100">baran</ta>
            <ta e="T102" id="Seg_2301" s="T101">kuttan-an</ta>
            <ta e="T103" id="Seg_2302" s="T102">hüge-ti-n</ta>
            <ta e="T104" id="Seg_2303" s="T103">bɨrag-an</ta>
            <ta e="T105" id="Seg_2304" s="T104">hüːr-en</ta>
            <ta e="T106" id="Seg_2305" s="T105">bar-ar</ta>
            <ta e="T107" id="Seg_2306" s="T106">hüːr-en</ta>
            <ta e="T108" id="Seg_2307" s="T107">bar-ar</ta>
            <ta e="T109" id="Seg_2308" s="T108">da</ta>
            <ta e="T110" id="Seg_2309" s="T109">onnug-u</ta>
            <ta e="T111" id="Seg_2310" s="T110">kör-dü-m</ta>
            <ta e="T112" id="Seg_2311" s="T111">di͡e-n</ta>
            <ta e="T113" id="Seg_2312" s="T112">dʼon-u-gar</ta>
            <ta e="T114" id="Seg_2313" s="T113">kepsiː-r</ta>
            <ta e="T115" id="Seg_2314" s="T114">man-tɨ-lara</ta>
            <ta e="T116" id="Seg_2315" s="T115">kas</ta>
            <ta e="T117" id="Seg_2316" s="T116">da</ta>
            <ta e="T118" id="Seg_2317" s="T117">kihi</ta>
            <ta e="T119" id="Seg_2318" s="T118">bu͡ol-an</ta>
            <ta e="T120" id="Seg_2319" s="T119">bar-an-nar</ta>
            <ta e="T121" id="Seg_2320" s="T120">hɨlaj-an</ta>
            <ta e="T122" id="Seg_2321" s="T121">okt-u-but</ta>
            <ta e="T123" id="Seg_2322" s="T122">kihi-leri-n</ta>
            <ta e="T124" id="Seg_2323" s="T123">tiriː-ge</ta>
            <ta e="T125" id="Seg_2324" s="T124">kötög-ön</ta>
            <ta e="T126" id="Seg_2325" s="T125">egel-el-ler</ta>
            <ta e="T127" id="Seg_2326" s="T126">ol</ta>
            <ta e="T128" id="Seg_2327" s="T127">egel-bit-teri-ger</ta>
            <ta e="T129" id="Seg_2328" s="T128">biːr</ta>
            <ta e="T130" id="Seg_2329" s="T129">kaːr</ta>
            <ta e="T131" id="Seg_2330" s="T130">kurduk</ta>
            <ta e="T132" id="Seg_2331" s="T131">as-taːk</ta>
            <ta e="T133" id="Seg_2332" s="T132">ogonnʼor</ta>
            <ta e="T134" id="Seg_2333" s="T133">et-er</ta>
            <ta e="T135" id="Seg_2334" s="T134">eː</ta>
            <ta e="T136" id="Seg_2335" s="T135">tug-u</ta>
            <ta e="T137" id="Seg_2336" s="T136">hög-ö-güt</ta>
            <ta e="T138" id="Seg_2337" s="T137">oloŋko-go</ta>
            <ta e="T139" id="Seg_2338" s="T138">da</ta>
            <ta e="T140" id="Seg_2339" s="T139">baːr</ta>
            <ta e="T141" id="Seg_2340" s="T140">bɨlɨrgɨ</ta>
            <ta e="T142" id="Seg_2341" s="T141">kepsel-ge</ta>
            <ta e="T143" id="Seg_2342" s="T142">huruk-ka</ta>
            <ta e="T144" id="Seg_2343" s="T143">da</ta>
            <ta e="T145" id="Seg_2344" s="T144">baːr</ta>
            <ta e="T146" id="Seg_2345" s="T145">mannɨk</ta>
            <ta e="T147" id="Seg_2346" s="T146">ikki</ta>
            <ta e="T148" id="Seg_2347" s="T147">karak-taːk</ta>
            <ta e="T149" id="Seg_2348" s="T148">noru͡ot</ta>
            <ta e="T150" id="Seg_2349" s="T149">baːr</ta>
            <ta e="T151" id="Seg_2350" s="T150">di͡e-n</ta>
            <ta e="T152" id="Seg_2351" s="T151">čejin</ta>
            <ta e="T153" id="Seg_2352" s="T152">korguj-but</ta>
            <ta e="T154" id="Seg_2353" s="T153">kihi</ta>
            <ta e="T155" id="Seg_2354" s="T154">bɨhɨː-tɨ-nan</ta>
            <ta e="T156" id="Seg_2355" s="T155">alɨs</ta>
            <ta e="T157" id="Seg_2356" s="T156">ah-ɨ-t-ɨ-mna</ta>
            <ta e="T158" id="Seg_2357" s="T157">ah-a-t-an</ta>
            <ta e="T159" id="Seg_2358" s="T158">ih-i-ŋ</ta>
            <ta e="T160" id="Seg_2359" s="T159">tutaj-an</ta>
            <ta e="T161" id="Seg_2360" s="T160">beje-ti-n</ta>
            <ta e="T162" id="Seg_2361" s="T161">hir-i-tten</ta>
            <ta e="T163" id="Seg_2362" s="T162">mu-nan</ta>
            <ta e="T164" id="Seg_2363" s="T163">kel-bit</ta>
            <ta e="T165" id="Seg_2364" s="T164">baraksan</ta>
            <ta e="T166" id="Seg_2365" s="T165">bu͡olu͡o</ta>
            <ta e="T167" id="Seg_2366" s="T166">diː-r</ta>
            <ta e="T168" id="Seg_2367" s="T167">bu</ta>
            <ta e="T169" id="Seg_2368" s="T168">baːj</ta>
            <ta e="T170" id="Seg_2369" s="T169">bagajɨ</ta>
            <ta e="T171" id="Seg_2370" s="T170">noru͡ot</ta>
            <ta e="T172" id="Seg_2371" s="T171">e-bit</ta>
            <ta e="T173" id="Seg_2372" s="T172">hahɨl-lara</ta>
            <ta e="T174" id="Seg_2373" s="T173">kɨrsa-lara</ta>
            <ta e="T175" id="Seg_2374" s="T174">dʼi͡e-leri-n</ta>
            <ta e="T176" id="Seg_2375" s="T175">aːn-ɨ-gar</ta>
            <ta e="T177" id="Seg_2376" s="T176">beje-te</ta>
            <ta e="T178" id="Seg_2377" s="T177">kel-er</ta>
            <ta e="T179" id="Seg_2378" s="T178">e-bit</ta>
            <ta e="T180" id="Seg_2379" s="T179">baː</ta>
            <ta e="T181" id="Seg_2380" s="T180">kihi</ta>
            <ta e="T182" id="Seg_2381" s="T181">manna</ta>
            <ta e="T183" id="Seg_2382" s="T182">olok-ton-or</ta>
            <ta e="T184" id="Seg_2383" s="T183">olok-tom-mut</ta>
            <ta e="T185" id="Seg_2384" s="T184">ɨ͡al-a</ta>
            <ta e="T186" id="Seg_2385" s="T185">ikki</ta>
            <ta e="T187" id="Seg_2386" s="T186">kɨːs-taːk</ta>
            <ta e="T188" id="Seg_2387" s="T187">e-bit-ter</ta>
            <ta e="T189" id="Seg_2388" s="T188">man-tan</ta>
            <ta e="T190" id="Seg_2389" s="T189">kɨrdʼagah-ɨ-n</ta>
            <ta e="T191" id="Seg_2390" s="T190">aga-ta</ta>
            <ta e="T192" id="Seg_2391" s="T191">ojok</ta>
            <ta e="T193" id="Seg_2392" s="T192">bi͡er-er</ta>
            <ta e="T194" id="Seg_2393" s="T193">u͡ol-ga</ta>
            <ta e="T195" id="Seg_2394" s="T194">manɨ͡a-ka</ta>
            <ta e="T196" id="Seg_2395" s="T195">baː</ta>
            <ta e="T197" id="Seg_2396" s="T196">kör-öːččü</ta>
            <ta e="T198" id="Seg_2397" s="T197">kɨːs</ta>
            <ta e="T199" id="Seg_2398" s="T198">elʼij-i-ger</ta>
            <ta e="T200" id="Seg_2399" s="T199">künüːleː-n</ta>
            <ta e="T201" id="Seg_2400" s="T200">min</ta>
            <ta e="T202" id="Seg_2401" s="T201">bar-ɨ͡ak-taːk</ta>
            <ta e="T203" id="Seg_2402" s="T202">beje-m</ta>
            <ta e="T204" id="Seg_2403" s="T203">bul-uː-ba-r</ta>
            <ta e="T205" id="Seg_2404" s="T204">di͡e-n</ta>
            <ta e="T206" id="Seg_2405" s="T205">ɨtɨː-r</ta>
            <ta e="T207" id="Seg_2406" s="T206">o-nu</ta>
            <ta e="T208" id="Seg_2407" s="T207">ist-i-mne</ta>
            <ta e="T209" id="Seg_2408" s="T208">hin</ta>
            <ta e="T210" id="Seg_2409" s="T209">kɨrdʼas</ta>
            <ta e="T211" id="Seg_2410" s="T210">kɨːh-ɨ</ta>
            <ta e="T212" id="Seg_2411" s="T211">bi͡er-el-ler</ta>
            <ta e="T213" id="Seg_2412" s="T212">u͡ol-ga</ta>
            <ta e="T214" id="Seg_2413" s="T213">biːr</ta>
            <ta e="T215" id="Seg_2414" s="T214">hɨl-ɨ-nan</ta>
            <ta e="T216" id="Seg_2415" s="T215">ikki</ta>
            <ta e="T217" id="Seg_2416" s="T216">karak-taːk</ta>
            <ta e="T218" id="Seg_2417" s="T217">u͡ol</ta>
            <ta e="T219" id="Seg_2418" s="T218">ogo</ta>
            <ta e="T220" id="Seg_2419" s="T219">törüː-r</ta>
            <ta e="T221" id="Seg_2420" s="T220">dʼe</ta>
            <ta e="T222" id="Seg_2421" s="T221">ör</ta>
            <ta e="T223" id="Seg_2422" s="T222">bagajɨ</ta>
            <ta e="T224" id="Seg_2423" s="T223">olor-ol-lor</ta>
            <ta e="T225" id="Seg_2424" s="T224">ogurduk</ta>
            <ta e="T226" id="Seg_2425" s="T225">bu</ta>
            <ta e="T227" id="Seg_2426" s="T226">aŋaːr</ta>
            <ta e="T228" id="Seg_2427" s="T227">karak-taːk-tar</ta>
            <ta e="T229" id="Seg_2428" s="T228">elbek</ta>
            <ta e="T230" id="Seg_2429" s="T229">bagajɨ</ta>
            <ta e="T231" id="Seg_2430" s="T230">noru͡ot</ta>
            <ta e="T232" id="Seg_2431" s="T231">e-bit-ter</ta>
            <ta e="T233" id="Seg_2432" s="T232">biːrde</ta>
            <ta e="T234" id="Seg_2433" s="T233">karaːb-ɨ-nan</ta>
            <ta e="T235" id="Seg_2434" s="T234">ɨraːktaːgɨ-lara</ta>
            <ta e="T236" id="Seg_2435" s="T235">ölbüge</ta>
            <ta e="T237" id="Seg_2436" s="T236">komuj-a</ta>
            <ta e="T238" id="Seg_2437" s="T237">kel-er</ta>
            <ta e="T239" id="Seg_2438" s="T238">kini-tten</ta>
            <ta e="T240" id="Seg_2439" s="T239">kihi</ta>
            <ta e="T241" id="Seg_2440" s="T240">kel-er</ta>
            <ta e="T242" id="Seg_2441" s="T241">baː</ta>
            <ta e="T243" id="Seg_2442" s="T242">kineːs</ta>
            <ta e="T244" id="Seg_2443" s="T243">kɨnn-ɨ-gar</ta>
            <ta e="T245" id="Seg_2444" s="T244">en</ta>
            <ta e="T246" id="Seg_2445" s="T245">ikki</ta>
            <ta e="T247" id="Seg_2446" s="T246">karak-taːk</ta>
            <ta e="T248" id="Seg_2447" s="T247">kütü͡öt-teːk</ta>
            <ta e="T249" id="Seg_2448" s="T248">ühü-gün</ta>
            <ta e="T250" id="Seg_2449" s="T249">en</ta>
            <ta e="T251" id="Seg_2450" s="T250">on-tu-gu-n</ta>
            <ta e="T252" id="Seg_2451" s="T251">ɨraːktaːgɨ</ta>
            <ta e="T253" id="Seg_2452" s="T252">kör-öːrü</ta>
            <ta e="T254" id="Seg_2453" s="T253">gɨn-ar</ta>
            <ta e="T255" id="Seg_2454" s="T254">diː-r</ta>
            <ta e="T256" id="Seg_2455" s="T255">kɨnn-a</ta>
            <ta e="T257" id="Seg_2456" s="T256">et-er</ta>
            <ta e="T258" id="Seg_2457" s="T257">kuːba-m</ta>
            <ta e="T259" id="Seg_2458" s="T258">bar</ta>
            <ta e="T260" id="Seg_2459" s="T259">ɨraːktaːgɨ</ta>
            <ta e="T261" id="Seg_2460" s="T260">kör-ü͡ö</ta>
            <ta e="T262" id="Seg_2461" s="T261">ere</ta>
            <ta e="T263" id="Seg_2462" s="T262">diː</ta>
            <ta e="T264" id="Seg_2463" s="T263">en</ta>
            <ta e="T265" id="Seg_2464" s="T264">bar</ta>
            <ta e="T266" id="Seg_2465" s="T265">bar-ba-tak-kɨna</ta>
            <ta e="T267" id="Seg_2466" s="T266">bas-pɨtɨ-n</ta>
            <ta e="T268" id="Seg_2467" s="T267">bɨh-ɨ͡a</ta>
            <ta e="T269" id="Seg_2468" s="T268">kineːs</ta>
            <ta e="T270" id="Seg_2469" s="T269">ɨraːktaːgɨ-ga</ta>
            <ta e="T271" id="Seg_2470" s="T270">bar-ar</ta>
            <ta e="T272" id="Seg_2471" s="T271">on-tu-ta</ta>
            <ta e="T273" id="Seg_2472" s="T272">et-er</ta>
            <ta e="T274" id="Seg_2473" s="T273">min</ta>
            <ta e="T275" id="Seg_2474" s="T274">ejigi-n</ta>
            <ta e="T276" id="Seg_2475" s="T275">manna</ta>
            <ta e="T277" id="Seg_2476" s="T276">baːj</ta>
            <ta e="T278" id="Seg_2477" s="T277">kihi</ta>
            <ta e="T279" id="Seg_2478" s="T278">gɨn-an</ta>
            <ta e="T280" id="Seg_2479" s="T279">terij-i͡e-m</ta>
            <ta e="T281" id="Seg_2480" s="T280">en</ta>
            <ta e="T282" id="Seg_2481" s="T281">manna</ta>
            <ta e="T283" id="Seg_2482" s="T282">kaːl-ɨ͡a-ŋ</ta>
            <ta e="T284" id="Seg_2483" s="T283">duː</ta>
            <ta e="T285" id="Seg_2484" s="T284">dojdu-ŋ</ta>
            <ta e="T286" id="Seg_2485" s="T285">di͡eg</ta>
            <ta e="T287" id="Seg_2486" s="T286">tönn-ü͡ö-ŋ</ta>
            <ta e="T288" id="Seg_2487" s="T287">duː</ta>
            <ta e="T289" id="Seg_2488" s="T288">o-nu</ta>
            <ta e="T290" id="Seg_2489" s="T289">tönn-ü͡ö-m</ta>
            <ta e="T291" id="Seg_2490" s="T290">di͡e-t-e</ta>
            <ta e="T292" id="Seg_2491" s="T291">ɨraːktaːgɨ</ta>
            <ta e="T293" id="Seg_2492" s="T292">onu-ga</ta>
            <ta e="T294" id="Seg_2493" s="T293">di͡e-t-e</ta>
            <ta e="T295" id="Seg_2494" s="T294">ɨraːk</ta>
            <ta e="T296" id="Seg_2495" s="T295">hir</ta>
            <ta e="T297" id="Seg_2496" s="T296">uː</ta>
            <ta e="T298" id="Seg_2497" s="T297">egel-bit-i-n</ta>
            <ta e="T299" id="Seg_2498" s="T298">kurduk</ta>
            <ta e="T300" id="Seg_2499" s="T299">bar-ɨ͡a-ŋ</ta>
            <ta e="T301" id="Seg_2500" s="T300">hu͡og-a</ta>
            <ta e="T302" id="Seg_2501" s="T301">min</ta>
            <ta e="T303" id="Seg_2502" s="T302">huruk</ta>
            <ta e="T304" id="Seg_2503" s="T303">bi͡er-i͡e-m</ta>
            <ta e="T305" id="Seg_2504" s="T304">kim</ta>
            <ta e="T306" id="Seg_2505" s="T305">da</ta>
            <ta e="T307" id="Seg_2506" s="T306">ejigi-n</ta>
            <ta e="T308" id="Seg_2507" s="T307">bolbu͡ot-a</ta>
            <ta e="T309" id="Seg_2508" s="T308">hu͡ok</ta>
            <ta e="T310" id="Seg_2509" s="T309">ildʼ-i͡e</ta>
            <ta e="T311" id="Seg_2510" s="T310">di͡e-n</ta>
            <ta e="T312" id="Seg_2511" s="T311">huruk</ta>
            <ta e="T313" id="Seg_2512" s="T312">bi͡er-er</ta>
            <ta e="T314" id="Seg_2513" s="T313">o-nu</ta>
            <ta e="T315" id="Seg_2514" s="T314">ɨl-an</ta>
            <ta e="T316" id="Seg_2515" s="T315">baran</ta>
            <ta e="T317" id="Seg_2516" s="T316">hin</ta>
            <ta e="T318" id="Seg_2517" s="T317">kas</ta>
            <ta e="T319" id="Seg_2518" s="T318">da</ta>
            <ta e="T320" id="Seg_2519" s="T319">hɨl</ta>
            <ta e="T321" id="Seg_2520" s="T320">olor-or</ta>
            <ta e="T322" id="Seg_2521" s="T321">bu</ta>
            <ta e="T323" id="Seg_2522" s="T322">olor-on</ta>
            <ta e="T324" id="Seg_2523" s="T323">baran</ta>
            <ta e="T325" id="Seg_2524" s="T324">bar-aːrɨ</ta>
            <ta e="T326" id="Seg_2525" s="T325">terin-er</ta>
            <ta e="T327" id="Seg_2526" s="T326">dʼaktar-ɨ-n</ta>
            <ta e="T328" id="Seg_2527" s="T327">ɨl-an</ta>
            <ta e="T329" id="Seg_2528" s="T328">manɨ͡a-ga</ta>
            <ta e="T330" id="Seg_2529" s="T329">ɨlgɨn</ta>
            <ta e="T331" id="Seg_2530" s="T330">kɨːs</ta>
            <ta e="T332" id="Seg_2531" s="T331">ɨtaː-n</ta>
            <ta e="T333" id="Seg_2532" s="T332">bar-ar</ta>
            <ta e="T334" id="Seg_2533" s="T333">min</ta>
            <ta e="T335" id="Seg_2534" s="T334">bul-but</ta>
            <ta e="T336" id="Seg_2535" s="T335">kihi-bi-tten</ta>
            <ta e="T337" id="Seg_2536" s="T336">kaːl-bap-pɨn</ta>
            <ta e="T338" id="Seg_2537" s="T337">biːrge</ta>
            <ta e="T339" id="Seg_2538" s="T338">bars-ɨ͡a-m</ta>
            <ta e="T340" id="Seg_2539" s="T339">agah-ɨ-gar</ta>
            <ta e="T341" id="Seg_2540" s="T340">et-er</ta>
            <ta e="T342" id="Seg_2541" s="T341">en</ta>
            <ta e="T343" id="Seg_2542" s="T342">kör-ör</ta>
            <ta e="T344" id="Seg_2543" s="T343">gɨn-ɨ͡ak-kɨ-n</ta>
            <ta e="T345" id="Seg_2544" s="T344">ikki</ta>
            <ta e="T346" id="Seg_2545" s="T345">karak-taːk</ta>
            <ta e="T347" id="Seg_2546" s="T346">ogo-loːk-kun</ta>
            <ta e="T348" id="Seg_2547" s="T347">min</ta>
            <ta e="T349" id="Seg_2548" s="T348">bars-ɨ͡a-m</ta>
            <ta e="T350" id="Seg_2549" s="T349">kini-ni</ta>
            <ta e="T351" id="Seg_2550" s="T350">gɨtta</ta>
            <ta e="T352" id="Seg_2551" s="T351">aga-ta</ta>
            <ta e="T353" id="Seg_2552" s="T352">hübeleː-n</ta>
            <ta e="T354" id="Seg_2553" s="T353">köŋül-lüː-r</ta>
            <ta e="T355" id="Seg_2554" s="T354">onon</ta>
            <ta e="T356" id="Seg_2555" s="T355">ɨlgɨn</ta>
            <ta e="T357" id="Seg_2556" s="T356">kɨːs</ta>
            <ta e="T358" id="Seg_2557" s="T357">biːrge</ta>
            <ta e="T359" id="Seg_2558" s="T358">bars-ar</ta>
            <ta e="T360" id="Seg_2559" s="T359">dʼe</ta>
            <ta e="T361" id="Seg_2560" s="T360">ör</ta>
            <ta e="T362" id="Seg_2561" s="T361">bagajɨ</ta>
            <ta e="T363" id="Seg_2562" s="T362">ajannaː-n</ta>
            <ta e="T364" id="Seg_2563" s="T363">tij-el-ler</ta>
            <ta e="T365" id="Seg_2564" s="T364">dojdu-larɨ-gar</ta>
            <ta e="T366" id="Seg_2565" s="T365">ogurduk</ta>
            <ta e="T367" id="Seg_2566" s="T366">ös</ta>
            <ta e="T368" id="Seg_2567" s="T367">baːr</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_2568" s="T0">bɨlɨr</ta>
            <ta e="T2" id="Seg_2569" s="T1">biːr</ta>
            <ta e="T3" id="Seg_2570" s="T2">u͡ol-LAːK</ta>
            <ta e="T4" id="Seg_2571" s="T3">dʼuraːk</ta>
            <ta e="T5" id="Seg_2572" s="T4">kineːs-tA</ta>
            <ta e="T6" id="Seg_2573" s="T5">olor-BIT</ta>
            <ta e="T7" id="Seg_2574" s="T6">kɨhɨn</ta>
            <ta e="T8" id="Seg_2575" s="T7">bajgal-GA</ta>
            <ta e="T9" id="Seg_2576" s="T8">dʼuraːk-LAr</ta>
            <ta e="T10" id="Seg_2577" s="T9">bölüːge-LAː-AːččI-LAr</ta>
            <ta e="T11" id="Seg_2578" s="T10">bu</ta>
            <ta e="T12" id="Seg_2579" s="T11">kineːs</ta>
            <ta e="T13" id="Seg_2580" s="T12">bajgal</ta>
            <ta e="T14" id="Seg_2581" s="T13">buːs-tI-nAn</ta>
            <ta e="T15" id="Seg_2582" s="T14">bölüːge</ta>
            <ta e="T16" id="Seg_2583" s="T15">ojbon-tA</ta>
            <ta e="T17" id="Seg_2584" s="T16">allar-A</ta>
            <ta e="T18" id="Seg_2585" s="T17">bar-Ar</ta>
            <ta e="T19" id="Seg_2586" s="T18">bu</ta>
            <ta e="T20" id="Seg_2587" s="T19">gɨn-An</ta>
            <ta e="T21" id="Seg_2588" s="T20">baran</ta>
            <ta e="T22" id="Seg_2589" s="T21">tönün-AːrI</ta>
            <ta e="T23" id="Seg_2590" s="T22">gɨn-BIT-tA</ta>
            <ta e="T24" id="Seg_2591" s="T23">buːs-tI-n</ta>
            <ta e="T25" id="Seg_2592" s="T24">bajgal</ta>
            <ta e="T26" id="Seg_2593" s="T25">aldʼat-An</ta>
            <ta e="T27" id="Seg_2594" s="T26">keːs-BIT</ta>
            <ta e="T28" id="Seg_2595" s="T27">üs</ta>
            <ta e="T29" id="Seg_2596" s="T28">kün</ta>
            <ta e="T30" id="Seg_2597" s="T29">kördöː-Ar</ta>
            <ta e="T31" id="Seg_2598" s="T30">tagɨs-Ar</ta>
            <ta e="T32" id="Seg_2599" s="T31">hir-tI-n</ta>
            <ta e="T33" id="Seg_2600" s="T32">muŋ-tA</ta>
            <ta e="T34" id="Seg_2601" s="T33">bu͡ol-An</ta>
            <ta e="T35" id="Seg_2602" s="T34">üs</ta>
            <ta e="T36" id="Seg_2603" s="T35">taba-tI-ttAn</ta>
            <ta e="T37" id="Seg_2604" s="T36">biːr-tI-n</ta>
            <ta e="T38" id="Seg_2605" s="T37">hogudaːj-LAː-An</ta>
            <ta e="T39" id="Seg_2606" s="T38">hi͡e-Ar</ta>
            <ta e="T40" id="Seg_2607" s="T39">onton</ta>
            <ta e="T41" id="Seg_2608" s="T40">emi͡e</ta>
            <ta e="T42" id="Seg_2609" s="T41">kaːm-A</ta>
            <ta e="T43" id="Seg_2610" s="T42">hataː-An</ta>
            <ta e="T44" id="Seg_2611" s="T43">ikki</ta>
            <ta e="T45" id="Seg_2612" s="T44">taba-tI-n</ta>
            <ta e="T46" id="Seg_2613" s="T45">baraː-Ar</ta>
            <ta e="T47" id="Seg_2614" s="T46">ölör-Ar</ta>
            <ta e="T48" id="Seg_2615" s="T47">buːs-tA</ta>
            <ta e="T49" id="Seg_2616" s="T48">ojun</ta>
            <ta e="T50" id="Seg_2617" s="T49">düŋür-tI-n</ta>
            <ta e="T51" id="Seg_2618" s="T50">haga</ta>
            <ta e="T52" id="Seg_2619" s="T51">bu͡ol-Ar</ta>
            <ta e="T53" id="Seg_2620" s="T52">ol-nI</ta>
            <ta e="T54" id="Seg_2621" s="T53">balkɨːr</ta>
            <ta e="T55" id="Seg_2622" s="T54">kajdak</ta>
            <ta e="T56" id="Seg_2623" s="T55">ere</ta>
            <ta e="T57" id="Seg_2624" s="T56">arɨː</ta>
            <ta e="T58" id="Seg_2625" s="T57">kɨtɨl-tI-GAr</ta>
            <ta e="T59" id="Seg_2626" s="T58">uhun-An</ta>
            <ta e="T60" id="Seg_2627" s="T59">kel-BIT</ta>
            <ta e="T61" id="Seg_2628" s="T60">mas-GA</ta>
            <ta e="T62" id="Seg_2629" s="T61">tu͡on</ta>
            <ta e="T63" id="Seg_2630" s="T62">keːs-Ar</ta>
            <ta e="T64" id="Seg_2631" s="T63">bu</ta>
            <ta e="T65" id="Seg_2632" s="T64">arɨː-GA</ta>
            <ta e="T66" id="Seg_2633" s="T65">tagɨs-I-BIT-tA</ta>
            <ta e="T67" id="Seg_2634" s="T66">mas-LAːK</ta>
            <ta e="T68" id="Seg_2635" s="T67">bagajɨ</ta>
            <ta e="T69" id="Seg_2636" s="T68">kanna</ta>
            <ta e="T70" id="Seg_2637" s="T69">ere</ta>
            <ta e="T71" id="Seg_2638" s="T70">hüge</ta>
            <ta e="T72" id="Seg_2639" s="T71">tɨ͡as-tA</ta>
            <ta e="T73" id="Seg_2640" s="T72">ihilin-Ar</ta>
            <ta e="T74" id="Seg_2641" s="T73">manna</ta>
            <ta e="T75" id="Seg_2642" s="T74">dʼe</ta>
            <ta e="T76" id="Seg_2643" s="T75">barɨ</ta>
            <ta e="T77" id="Seg_2644" s="T76">küːs-tI-n</ta>
            <ta e="T78" id="Seg_2645" s="T77">bar-An</ta>
            <ta e="T79" id="Seg_2646" s="T78">tij-Ar</ta>
            <ta e="T80" id="Seg_2647" s="T79">tij-An</ta>
            <ta e="T81" id="Seg_2648" s="T80">kör-BIT-tA</ta>
            <ta e="T82" id="Seg_2649" s="T81">biːr</ta>
            <ta e="T83" id="Seg_2650" s="T82">oččuguk-kAːN</ta>
            <ta e="T84" id="Seg_2651" s="T83">kɨːs</ta>
            <ta e="T85" id="Seg_2652" s="T84">gini-GA</ta>
            <ta e="T86" id="Seg_2653" s="T85">köksü-tI-nAn</ta>
            <ta e="T87" id="Seg_2654" s="T86">tur-An</ta>
            <ta e="T88" id="Seg_2655" s="T87">mas</ta>
            <ta e="T89" id="Seg_2656" s="T88">kert-A</ta>
            <ta e="T90" id="Seg_2657" s="T89">tur-Ar</ta>
            <ta e="T91" id="Seg_2658" s="T90">bu</ta>
            <ta e="T92" id="Seg_2659" s="T91">kɨːs</ta>
            <ta e="T93" id="Seg_2660" s="T92">kaj-I-s-Ar</ta>
            <ta e="T94" id="Seg_2661" s="T93">onton</ta>
            <ta e="T95" id="Seg_2662" s="T94">aŋar</ta>
            <ta e="T96" id="Seg_2663" s="T95">karak-LAːK</ta>
            <ta e="T97" id="Seg_2664" s="T96">e-BIT</ta>
            <ta e="T98" id="Seg_2665" s="T97">kɨːs-tA</ta>
            <ta e="T99" id="Seg_2666" s="T98">kihi-nI</ta>
            <ta e="T100" id="Seg_2667" s="T99">kör-An</ta>
            <ta e="T101" id="Seg_2668" s="T100">baran</ta>
            <ta e="T102" id="Seg_2669" s="T101">kuttan-An</ta>
            <ta e="T103" id="Seg_2670" s="T102">hüge-tI-n</ta>
            <ta e="T104" id="Seg_2671" s="T103">bɨrak-An</ta>
            <ta e="T105" id="Seg_2672" s="T104">hüːr-An</ta>
            <ta e="T106" id="Seg_2673" s="T105">bar-Ar</ta>
            <ta e="T107" id="Seg_2674" s="T106">hüːr-An</ta>
            <ta e="T108" id="Seg_2675" s="T107">bar-Ar</ta>
            <ta e="T109" id="Seg_2676" s="T108">da</ta>
            <ta e="T110" id="Seg_2677" s="T109">onnuk-nI</ta>
            <ta e="T111" id="Seg_2678" s="T110">kör-TI-m</ta>
            <ta e="T112" id="Seg_2679" s="T111">di͡e-An</ta>
            <ta e="T113" id="Seg_2680" s="T112">dʼon-tI-GAr</ta>
            <ta e="T114" id="Seg_2681" s="T113">kepseː-Ar</ta>
            <ta e="T115" id="Seg_2682" s="T114">bu-tI-LArA</ta>
            <ta e="T116" id="Seg_2683" s="T115">kas</ta>
            <ta e="T117" id="Seg_2684" s="T116">da</ta>
            <ta e="T118" id="Seg_2685" s="T117">kihi</ta>
            <ta e="T119" id="Seg_2686" s="T118">bu͡ol-An</ta>
            <ta e="T120" id="Seg_2687" s="T119">bar-An-LAr</ta>
            <ta e="T121" id="Seg_2688" s="T120">hɨlaj-An</ta>
            <ta e="T122" id="Seg_2689" s="T121">ogut-I-BIT</ta>
            <ta e="T123" id="Seg_2690" s="T122">kihi-LArI-n</ta>
            <ta e="T124" id="Seg_2691" s="T123">tiriː-GA</ta>
            <ta e="T125" id="Seg_2692" s="T124">kötök-An</ta>
            <ta e="T126" id="Seg_2693" s="T125">egel-Ar-LAr</ta>
            <ta e="T127" id="Seg_2694" s="T126">ol</ta>
            <ta e="T128" id="Seg_2695" s="T127">egel-BIT-LArI-GAr</ta>
            <ta e="T129" id="Seg_2696" s="T128">biːr</ta>
            <ta e="T130" id="Seg_2697" s="T129">kaːr</ta>
            <ta e="T131" id="Seg_2698" s="T130">kurduk</ta>
            <ta e="T132" id="Seg_2699" s="T131">as-LAːK</ta>
            <ta e="T133" id="Seg_2700" s="T132">ogonnʼor</ta>
            <ta e="T134" id="Seg_2701" s="T133">et-Ar</ta>
            <ta e="T135" id="Seg_2702" s="T134">eː</ta>
            <ta e="T136" id="Seg_2703" s="T135">tu͡ok-nI</ta>
            <ta e="T137" id="Seg_2704" s="T136">hök-A-GIt</ta>
            <ta e="T138" id="Seg_2705" s="T137">oloŋko-GA</ta>
            <ta e="T139" id="Seg_2706" s="T138">da</ta>
            <ta e="T140" id="Seg_2707" s="T139">baːr</ta>
            <ta e="T141" id="Seg_2708" s="T140">bɨlɨrgɨ</ta>
            <ta e="T142" id="Seg_2709" s="T141">kepsel-GA</ta>
            <ta e="T143" id="Seg_2710" s="T142">huruk-GA</ta>
            <ta e="T144" id="Seg_2711" s="T143">da</ta>
            <ta e="T145" id="Seg_2712" s="T144">baːr</ta>
            <ta e="T146" id="Seg_2713" s="T145">mannɨk</ta>
            <ta e="T147" id="Seg_2714" s="T146">ikki</ta>
            <ta e="T148" id="Seg_2715" s="T147">karak-LAːK</ta>
            <ta e="T149" id="Seg_2716" s="T148">noru͡ot</ta>
            <ta e="T150" id="Seg_2717" s="T149">baːr</ta>
            <ta e="T151" id="Seg_2718" s="T150">di͡e-An</ta>
            <ta e="T152" id="Seg_2719" s="T151">dʼe</ta>
            <ta e="T153" id="Seg_2720" s="T152">korguj-BIT</ta>
            <ta e="T154" id="Seg_2721" s="T153">kihi</ta>
            <ta e="T155" id="Seg_2722" s="T154">bɨhɨː-tI-nAn</ta>
            <ta e="T156" id="Seg_2723" s="T155">alɨs</ta>
            <ta e="T157" id="Seg_2724" s="T156">ahaː-I-t-I-mInA</ta>
            <ta e="T158" id="Seg_2725" s="T157">ahaː-A-t-An</ta>
            <ta e="T159" id="Seg_2726" s="T158">is-I-ŋ</ta>
            <ta e="T160" id="Seg_2727" s="T159">tutaj-An</ta>
            <ta e="T161" id="Seg_2728" s="T160">beje-tI-n</ta>
            <ta e="T162" id="Seg_2729" s="T161">hir-tI-ttAn</ta>
            <ta e="T163" id="Seg_2730" s="T162">bu-nAn</ta>
            <ta e="T164" id="Seg_2731" s="T163">kel-BIT</ta>
            <ta e="T165" id="Seg_2732" s="T164">baraksan</ta>
            <ta e="T166" id="Seg_2733" s="T165">bu͡olu͡o</ta>
            <ta e="T167" id="Seg_2734" s="T166">di͡e-Ar</ta>
            <ta e="T168" id="Seg_2735" s="T167">bu</ta>
            <ta e="T169" id="Seg_2736" s="T168">baːj</ta>
            <ta e="T170" id="Seg_2737" s="T169">bagajɨ</ta>
            <ta e="T171" id="Seg_2738" s="T170">noru͡ot</ta>
            <ta e="T172" id="Seg_2739" s="T171">e-BIT</ta>
            <ta e="T173" id="Seg_2740" s="T172">hahɨl-LArA</ta>
            <ta e="T174" id="Seg_2741" s="T173">kɨrsa-LArA</ta>
            <ta e="T175" id="Seg_2742" s="T174">dʼi͡e-LArI-n</ta>
            <ta e="T176" id="Seg_2743" s="T175">aːn-tI-GAr</ta>
            <ta e="T177" id="Seg_2744" s="T176">beje-tA</ta>
            <ta e="T178" id="Seg_2745" s="T177">kel-Ar</ta>
            <ta e="T179" id="Seg_2746" s="T178">e-BIT</ta>
            <ta e="T180" id="Seg_2747" s="T179">bu</ta>
            <ta e="T181" id="Seg_2748" s="T180">kihi</ta>
            <ta e="T182" id="Seg_2749" s="T181">manna</ta>
            <ta e="T183" id="Seg_2750" s="T182">olok-LAN-Ar</ta>
            <ta e="T184" id="Seg_2751" s="T183">olok-LAN-BIT</ta>
            <ta e="T185" id="Seg_2752" s="T184">ɨ͡al-tA</ta>
            <ta e="T186" id="Seg_2753" s="T185">ikki</ta>
            <ta e="T187" id="Seg_2754" s="T186">kɨːs-LAːK</ta>
            <ta e="T188" id="Seg_2755" s="T187">e-BIT-LAr</ta>
            <ta e="T189" id="Seg_2756" s="T188">bu-ttAn</ta>
            <ta e="T190" id="Seg_2757" s="T189">kɨrdʼagas-tI-n</ta>
            <ta e="T191" id="Seg_2758" s="T190">aga-tA</ta>
            <ta e="T192" id="Seg_2759" s="T191">ojok</ta>
            <ta e="T193" id="Seg_2760" s="T192">bi͡er-Ar</ta>
            <ta e="T194" id="Seg_2761" s="T193">u͡ol-GA</ta>
            <ta e="T195" id="Seg_2762" s="T194">bu-GA</ta>
            <ta e="T196" id="Seg_2763" s="T195">bu</ta>
            <ta e="T197" id="Seg_2764" s="T196">kör-AːččI</ta>
            <ta e="T198" id="Seg_2765" s="T197">kɨːs</ta>
            <ta e="T199" id="Seg_2766" s="T198">edʼij-tI-GAr</ta>
            <ta e="T200" id="Seg_2767" s="T199">künüːleː-An</ta>
            <ta e="T201" id="Seg_2768" s="T200">min</ta>
            <ta e="T202" id="Seg_2769" s="T201">bar-IAK-LAːK</ta>
            <ta e="T203" id="Seg_2770" s="T202">beje-m</ta>
            <ta e="T204" id="Seg_2771" s="T203">bul-Iː-BA-r</ta>
            <ta e="T205" id="Seg_2772" s="T204">di͡e-An</ta>
            <ta e="T206" id="Seg_2773" s="T205">ɨtaː-Ar</ta>
            <ta e="T207" id="Seg_2774" s="T206">ol-nI</ta>
            <ta e="T208" id="Seg_2775" s="T207">ihit-I-mInA</ta>
            <ta e="T209" id="Seg_2776" s="T208">hin</ta>
            <ta e="T210" id="Seg_2777" s="T209">kɨrdʼagas</ta>
            <ta e="T211" id="Seg_2778" s="T210">kɨːs-nI</ta>
            <ta e="T212" id="Seg_2779" s="T211">bi͡er-Ar-LAr</ta>
            <ta e="T213" id="Seg_2780" s="T212">u͡ol-GA</ta>
            <ta e="T214" id="Seg_2781" s="T213">biːr</ta>
            <ta e="T215" id="Seg_2782" s="T214">dʼɨl-I-nAn</ta>
            <ta e="T216" id="Seg_2783" s="T215">ikki</ta>
            <ta e="T217" id="Seg_2784" s="T216">karak-LAːK</ta>
            <ta e="T218" id="Seg_2785" s="T217">u͡ol</ta>
            <ta e="T219" id="Seg_2786" s="T218">ogo</ta>
            <ta e="T220" id="Seg_2787" s="T219">töröː-Ar</ta>
            <ta e="T221" id="Seg_2788" s="T220">dʼe</ta>
            <ta e="T222" id="Seg_2789" s="T221">ör</ta>
            <ta e="T223" id="Seg_2790" s="T222">bagajɨ</ta>
            <ta e="T224" id="Seg_2791" s="T223">olor-Ar-LAr</ta>
            <ta e="T225" id="Seg_2792" s="T224">ogorduk</ta>
            <ta e="T226" id="Seg_2793" s="T225">bu</ta>
            <ta e="T227" id="Seg_2794" s="T226">aŋar</ta>
            <ta e="T228" id="Seg_2795" s="T227">karak-LAːK-LAr</ta>
            <ta e="T229" id="Seg_2796" s="T228">elbek</ta>
            <ta e="T230" id="Seg_2797" s="T229">bagajɨ</ta>
            <ta e="T231" id="Seg_2798" s="T230">noru͡ot</ta>
            <ta e="T232" id="Seg_2799" s="T231">e-BIT-LAr</ta>
            <ta e="T233" id="Seg_2800" s="T232">biːrde</ta>
            <ta e="T234" id="Seg_2801" s="T233">karaːp-tI-nAn</ta>
            <ta e="T235" id="Seg_2802" s="T234">ɨraːktaːgɨ-LArA</ta>
            <ta e="T236" id="Seg_2803" s="T235">ölbüge</ta>
            <ta e="T237" id="Seg_2804" s="T236">komuj-A</ta>
            <ta e="T238" id="Seg_2805" s="T237">kel-Ar</ta>
            <ta e="T239" id="Seg_2806" s="T238">gini-ttAn</ta>
            <ta e="T240" id="Seg_2807" s="T239">kihi</ta>
            <ta e="T241" id="Seg_2808" s="T240">kel-Ar</ta>
            <ta e="T242" id="Seg_2809" s="T241">bu</ta>
            <ta e="T243" id="Seg_2810" s="T242">kineːs</ta>
            <ta e="T244" id="Seg_2811" s="T243">kɨlɨn-tI-GAr</ta>
            <ta e="T245" id="Seg_2812" s="T244">en</ta>
            <ta e="T246" id="Seg_2813" s="T245">ikki</ta>
            <ta e="T247" id="Seg_2814" s="T246">karak-LAːK</ta>
            <ta e="T248" id="Seg_2815" s="T247">kütü͡öt-LAːK</ta>
            <ta e="T249" id="Seg_2816" s="T248">ühü-GIn</ta>
            <ta e="T250" id="Seg_2817" s="T249">en</ta>
            <ta e="T251" id="Seg_2818" s="T250">ol-tI-GI-n</ta>
            <ta e="T252" id="Seg_2819" s="T251">ɨraːktaːgɨ</ta>
            <ta e="T253" id="Seg_2820" s="T252">kör-AːrI</ta>
            <ta e="T254" id="Seg_2821" s="T253">gɨn-Ar</ta>
            <ta e="T255" id="Seg_2822" s="T254">di͡e-Ar</ta>
            <ta e="T256" id="Seg_2823" s="T255">kɨlɨn-tA</ta>
            <ta e="T257" id="Seg_2824" s="T256">et-Ar</ta>
            <ta e="T258" id="Seg_2825" s="T257">kuba-m</ta>
            <ta e="T259" id="Seg_2826" s="T258">bar</ta>
            <ta e="T260" id="Seg_2827" s="T259">ɨraːktaːgɨ</ta>
            <ta e="T261" id="Seg_2828" s="T260">kör-IAK.[tA]</ta>
            <ta e="T262" id="Seg_2829" s="T261">ere</ta>
            <ta e="T263" id="Seg_2830" s="T262">diː</ta>
            <ta e="T264" id="Seg_2831" s="T263">en</ta>
            <ta e="T265" id="Seg_2832" s="T264">bar</ta>
            <ta e="T266" id="Seg_2833" s="T265">bar-BA-TAK-GInA</ta>
            <ta e="T267" id="Seg_2834" s="T266">bas-BItI-n</ta>
            <ta e="T268" id="Seg_2835" s="T267">bɨs-IAK.[tA]</ta>
            <ta e="T269" id="Seg_2836" s="T268">kineːs</ta>
            <ta e="T270" id="Seg_2837" s="T269">ɨraːktaːgɨ-GA</ta>
            <ta e="T271" id="Seg_2838" s="T270">bar-Ar</ta>
            <ta e="T272" id="Seg_2839" s="T271">ol-tI-tA</ta>
            <ta e="T273" id="Seg_2840" s="T272">et-Ar</ta>
            <ta e="T274" id="Seg_2841" s="T273">min</ta>
            <ta e="T275" id="Seg_2842" s="T274">en-n</ta>
            <ta e="T276" id="Seg_2843" s="T275">manna</ta>
            <ta e="T277" id="Seg_2844" s="T276">baːj</ta>
            <ta e="T278" id="Seg_2845" s="T277">kihi</ta>
            <ta e="T279" id="Seg_2846" s="T278">gɨn-An</ta>
            <ta e="T280" id="Seg_2847" s="T279">terij-IAK-m</ta>
            <ta e="T281" id="Seg_2848" s="T280">en</ta>
            <ta e="T282" id="Seg_2849" s="T281">manna</ta>
            <ta e="T283" id="Seg_2850" s="T282">kaːl-IAK-ŋ</ta>
            <ta e="T284" id="Seg_2851" s="T283">du͡o</ta>
            <ta e="T285" id="Seg_2852" s="T284">dojdu-ŋ</ta>
            <ta e="T286" id="Seg_2853" s="T285">dek</ta>
            <ta e="T287" id="Seg_2854" s="T286">tönün-IAK-ŋ</ta>
            <ta e="T288" id="Seg_2855" s="T287">du͡o</ta>
            <ta e="T289" id="Seg_2856" s="T288">ol-nI</ta>
            <ta e="T290" id="Seg_2857" s="T289">tönün-IAK-m</ta>
            <ta e="T291" id="Seg_2858" s="T290">di͡e-TI-tA</ta>
            <ta e="T292" id="Seg_2859" s="T291">ɨraːktaːgɨ</ta>
            <ta e="T293" id="Seg_2860" s="T292">ol-GA</ta>
            <ta e="T294" id="Seg_2861" s="T293">di͡e-TI-tA</ta>
            <ta e="T295" id="Seg_2862" s="T294">ɨraːk</ta>
            <ta e="T296" id="Seg_2863" s="T295">hir</ta>
            <ta e="T297" id="Seg_2864" s="T296">uː</ta>
            <ta e="T298" id="Seg_2865" s="T297">egel-BIT-tI-n</ta>
            <ta e="T299" id="Seg_2866" s="T298">kurduk</ta>
            <ta e="T300" id="Seg_2867" s="T299">bar-IAK-ŋ</ta>
            <ta e="T301" id="Seg_2868" s="T300">hu͡ok-tA</ta>
            <ta e="T302" id="Seg_2869" s="T301">min</ta>
            <ta e="T303" id="Seg_2870" s="T302">huruk</ta>
            <ta e="T304" id="Seg_2871" s="T303">bi͡er-IAK-m</ta>
            <ta e="T305" id="Seg_2872" s="T304">kim</ta>
            <ta e="T306" id="Seg_2873" s="T305">da</ta>
            <ta e="T307" id="Seg_2874" s="T306">en-n</ta>
            <ta e="T308" id="Seg_2875" s="T307">bolbu͡ot-tA</ta>
            <ta e="T309" id="Seg_2876" s="T308">hu͡ok</ta>
            <ta e="T310" id="Seg_2877" s="T309">ilt-IAK.[tA]</ta>
            <ta e="T311" id="Seg_2878" s="T310">di͡e-An</ta>
            <ta e="T312" id="Seg_2879" s="T311">huruk</ta>
            <ta e="T313" id="Seg_2880" s="T312">bi͡er-Ar</ta>
            <ta e="T314" id="Seg_2881" s="T313">ol-nI</ta>
            <ta e="T315" id="Seg_2882" s="T314">ɨl-An</ta>
            <ta e="T316" id="Seg_2883" s="T315">baran</ta>
            <ta e="T317" id="Seg_2884" s="T316">hin</ta>
            <ta e="T318" id="Seg_2885" s="T317">kas</ta>
            <ta e="T319" id="Seg_2886" s="T318">da</ta>
            <ta e="T320" id="Seg_2887" s="T319">dʼɨl</ta>
            <ta e="T321" id="Seg_2888" s="T320">olor-Ar</ta>
            <ta e="T322" id="Seg_2889" s="T321">bu</ta>
            <ta e="T323" id="Seg_2890" s="T322">olor-An</ta>
            <ta e="T324" id="Seg_2891" s="T323">baran</ta>
            <ta e="T325" id="Seg_2892" s="T324">bar-AːrI</ta>
            <ta e="T326" id="Seg_2893" s="T325">terin-Ar</ta>
            <ta e="T327" id="Seg_2894" s="T326">dʼaktar-tI-n</ta>
            <ta e="T328" id="Seg_2895" s="T327">ɨl-An</ta>
            <ta e="T329" id="Seg_2896" s="T328">bu-GA</ta>
            <ta e="T330" id="Seg_2897" s="T329">ɨlgɨn</ta>
            <ta e="T331" id="Seg_2898" s="T330">kɨːs</ta>
            <ta e="T332" id="Seg_2899" s="T331">ɨtaː-An</ta>
            <ta e="T333" id="Seg_2900" s="T332">bar-Ar</ta>
            <ta e="T334" id="Seg_2901" s="T333">min</ta>
            <ta e="T335" id="Seg_2902" s="T334">bul-BIT</ta>
            <ta e="T336" id="Seg_2903" s="T335">kihi-BI-ttAn</ta>
            <ta e="T337" id="Seg_2904" s="T336">kaːl-BAT-BIn</ta>
            <ta e="T338" id="Seg_2905" s="T337">biːrge</ta>
            <ta e="T339" id="Seg_2906" s="T338">barɨs-IAK-m</ta>
            <ta e="T340" id="Seg_2907" s="T339">agas-tI-GAr</ta>
            <ta e="T341" id="Seg_2908" s="T340">et-Ar</ta>
            <ta e="T342" id="Seg_2909" s="T341">en</ta>
            <ta e="T343" id="Seg_2910" s="T342">kör-Ar</ta>
            <ta e="T344" id="Seg_2911" s="T343">gɨn-IAK-GI-n</ta>
            <ta e="T345" id="Seg_2912" s="T344">ikki</ta>
            <ta e="T346" id="Seg_2913" s="T345">karak-LAːK</ta>
            <ta e="T347" id="Seg_2914" s="T346">ogo-LAːK-GIn</ta>
            <ta e="T348" id="Seg_2915" s="T347">min</ta>
            <ta e="T349" id="Seg_2916" s="T348">barɨs-IAK-m</ta>
            <ta e="T350" id="Seg_2917" s="T349">gini-nI</ta>
            <ta e="T351" id="Seg_2918" s="T350">kɨtta</ta>
            <ta e="T352" id="Seg_2919" s="T351">aga-tA</ta>
            <ta e="T353" id="Seg_2920" s="T352">hübeleː-An</ta>
            <ta e="T354" id="Seg_2921" s="T353">köŋül-LIː-Ar</ta>
            <ta e="T355" id="Seg_2922" s="T354">onon</ta>
            <ta e="T356" id="Seg_2923" s="T355">ɨlgɨn</ta>
            <ta e="T357" id="Seg_2924" s="T356">kɨːs</ta>
            <ta e="T358" id="Seg_2925" s="T357">biːrge</ta>
            <ta e="T359" id="Seg_2926" s="T358">barɨs-Ar</ta>
            <ta e="T360" id="Seg_2927" s="T359">dʼe</ta>
            <ta e="T361" id="Seg_2928" s="T360">ör</ta>
            <ta e="T362" id="Seg_2929" s="T361">bagajɨ</ta>
            <ta e="T363" id="Seg_2930" s="T362">ajannaː-An</ta>
            <ta e="T364" id="Seg_2931" s="T363">tij-Ar-LAr</ta>
            <ta e="T365" id="Seg_2932" s="T364">dojdu-LArI-GAr</ta>
            <ta e="T366" id="Seg_2933" s="T365">ogorduk</ta>
            <ta e="T367" id="Seg_2934" s="T366">ös</ta>
            <ta e="T368" id="Seg_2935" s="T367">baːr</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_2936" s="T0">long.ago</ta>
            <ta e="T2" id="Seg_2937" s="T1">one</ta>
            <ta e="T3" id="Seg_2938" s="T2">boy-PROPR</ta>
            <ta e="T4" id="Seg_2939" s="T3">Nenets</ta>
            <ta e="T5" id="Seg_2940" s="T4">prince-3SG.[NOM]</ta>
            <ta e="T6" id="Seg_2941" s="T5">live-PST2.[3SG]</ta>
            <ta e="T7" id="Seg_2942" s="T6">in.winter</ta>
            <ta e="T8" id="Seg_2943" s="T7">sea-DAT/LOC</ta>
            <ta e="T9" id="Seg_2944" s="T8">Nenets-PL.[NOM]</ta>
            <ta e="T10" id="Seg_2945" s="T9">beluga-VBZ-HAB-3PL</ta>
            <ta e="T11" id="Seg_2946" s="T10">this</ta>
            <ta e="T12" id="Seg_2947" s="T11">prince.[NOM]</ta>
            <ta e="T13" id="Seg_2948" s="T12">sea.[NOM]</ta>
            <ta e="T14" id="Seg_2949" s="T13">ice-3SG-INSTR</ta>
            <ta e="T15" id="Seg_2950" s="T14">beluga.[NOM]</ta>
            <ta e="T16" id="Seg_2951" s="T15">icehole-3SG.[NOM]</ta>
            <ta e="T17" id="Seg_2952" s="T16">cut.through-CVB.SIM</ta>
            <ta e="T18" id="Seg_2953" s="T17">go-PRS.[3SG]</ta>
            <ta e="T19" id="Seg_2954" s="T18">this</ta>
            <ta e="T20" id="Seg_2955" s="T19">make-CVB.SEQ</ta>
            <ta e="T21" id="Seg_2956" s="T20">after</ta>
            <ta e="T22" id="Seg_2957" s="T21">come.back-CVB.PURP</ta>
            <ta e="T23" id="Seg_2958" s="T22">want-PST2-3SG</ta>
            <ta e="T24" id="Seg_2959" s="T23">ice.floe-3SG-ACC</ta>
            <ta e="T25" id="Seg_2960" s="T24">sea.[NOM]</ta>
            <ta e="T26" id="Seg_2961" s="T25">break-CVB.SEQ</ta>
            <ta e="T27" id="Seg_2962" s="T26">throw-PST2.[3SG]</ta>
            <ta e="T28" id="Seg_2963" s="T27">three</ta>
            <ta e="T29" id="Seg_2964" s="T28">day.[NOM]</ta>
            <ta e="T30" id="Seg_2965" s="T29">search-PRS.[3SG]</ta>
            <ta e="T31" id="Seg_2966" s="T30">go.out-PTCP.PRS</ta>
            <ta e="T32" id="Seg_2967" s="T31">place-3SG-ACC</ta>
            <ta e="T33" id="Seg_2968" s="T32">misery-3SG.[NOM]</ta>
            <ta e="T34" id="Seg_2969" s="T33">be-CVB.SEQ</ta>
            <ta e="T35" id="Seg_2970" s="T34">three</ta>
            <ta e="T36" id="Seg_2971" s="T35">reindeer-3SG-ABL</ta>
            <ta e="T37" id="Seg_2972" s="T36">one-3SG-ACC</ta>
            <ta e="T38" id="Seg_2973" s="T37">raw.meat-VBZ-CVB.SEQ</ta>
            <ta e="T39" id="Seg_2974" s="T38">eat-PRS.[3SG]</ta>
            <ta e="T40" id="Seg_2975" s="T39">then</ta>
            <ta e="T41" id="Seg_2976" s="T40">again</ta>
            <ta e="T42" id="Seg_2977" s="T41">walk-CVB.SIM</ta>
            <ta e="T43" id="Seg_2978" s="T42">not.manage-CVB.SEQ</ta>
            <ta e="T44" id="Seg_2979" s="T43">two</ta>
            <ta e="T45" id="Seg_2980" s="T44">reindeer-3SG-ACC</ta>
            <ta e="T46" id="Seg_2981" s="T45">defeat-PRS.[3SG]</ta>
            <ta e="T47" id="Seg_2982" s="T46">kill-PRS.[3SG]</ta>
            <ta e="T48" id="Seg_2983" s="T47">ice.floe-3SG.[NOM]</ta>
            <ta e="T49" id="Seg_2984" s="T48">shaman.[NOM]</ta>
            <ta e="T50" id="Seg_2985" s="T49">drum.of.the.shaman-3SG-ACC</ta>
            <ta e="T51" id="Seg_2986" s="T50">big.as</ta>
            <ta e="T52" id="Seg_2987" s="T51">become-PRS.[3SG]</ta>
            <ta e="T53" id="Seg_2988" s="T52">that-ACC</ta>
            <ta e="T54" id="Seg_2989" s="T53">storm.[NOM]</ta>
            <ta e="T55" id="Seg_2990" s="T54">how</ta>
            <ta e="T56" id="Seg_2991" s="T55">INDEF</ta>
            <ta e="T57" id="Seg_2992" s="T56">island.[NOM]</ta>
            <ta e="T58" id="Seg_2993" s="T57">shore-3SG-DAT/LOC</ta>
            <ta e="T59" id="Seg_2994" s="T58">flow-CVB.SEQ</ta>
            <ta e="T60" id="Seg_2995" s="T59">come-PST2.[3SG]</ta>
            <ta e="T61" id="Seg_2996" s="T60">tree-DAT/LOC</ta>
            <ta e="T62" id="Seg_2997" s="T61">to</ta>
            <ta e="T63" id="Seg_2998" s="T62">throw-PRS.[3SG]</ta>
            <ta e="T64" id="Seg_2999" s="T63">this</ta>
            <ta e="T65" id="Seg_3000" s="T64">island-DAT/LOC</ta>
            <ta e="T66" id="Seg_3001" s="T65">go.out-EP-PST2-3SG</ta>
            <ta e="T67" id="Seg_3002" s="T66">tree-PROPR.[NOM]</ta>
            <ta e="T68" id="Seg_3003" s="T67">very</ta>
            <ta e="T69" id="Seg_3004" s="T68">where</ta>
            <ta e="T70" id="Seg_3005" s="T69">INDEF</ta>
            <ta e="T71" id="Seg_3006" s="T70">axe.[NOM]</ta>
            <ta e="T72" id="Seg_3007" s="T71">sound-3SG.[NOM]</ta>
            <ta e="T73" id="Seg_3008" s="T72">be.heard-PRS.[3SG]</ta>
            <ta e="T74" id="Seg_3009" s="T73">hither</ta>
            <ta e="T75" id="Seg_3010" s="T74">well</ta>
            <ta e="T76" id="Seg_3011" s="T75">every</ta>
            <ta e="T77" id="Seg_3012" s="T76">power-3SG-ACC</ta>
            <ta e="T78" id="Seg_3013" s="T77">go-CVB.SEQ</ta>
            <ta e="T79" id="Seg_3014" s="T78">reach-PRS.[3SG]</ta>
            <ta e="T80" id="Seg_3015" s="T79">reach-CVB.SEQ</ta>
            <ta e="T81" id="Seg_3016" s="T80">see-PST2-3SG</ta>
            <ta e="T82" id="Seg_3017" s="T81">one</ta>
            <ta e="T83" id="Seg_3018" s="T82">so.small-INTNS</ta>
            <ta e="T84" id="Seg_3019" s="T83">girl.[NOM]</ta>
            <ta e="T85" id="Seg_3020" s="T84">3SG-DAT/LOC</ta>
            <ta e="T86" id="Seg_3021" s="T85">back-3SG-INSTR</ta>
            <ta e="T87" id="Seg_3022" s="T86">stand-CVB.SEQ</ta>
            <ta e="T88" id="Seg_3023" s="T87">wood.[NOM]</ta>
            <ta e="T89" id="Seg_3024" s="T88">cut.up-CVB.SIM</ta>
            <ta e="T90" id="Seg_3025" s="T89">stand-PRS.[3SG]</ta>
            <ta e="T91" id="Seg_3026" s="T90">this</ta>
            <ta e="T92" id="Seg_3027" s="T91">girl.[NOM]</ta>
            <ta e="T93" id="Seg_3028" s="T92">follow-EP-RECP/COLL-PRS.[3SG]</ta>
            <ta e="T94" id="Seg_3029" s="T93">then</ta>
            <ta e="T95" id="Seg_3030" s="T94">half.[NOM]</ta>
            <ta e="T96" id="Seg_3031" s="T95">eye-PROPR.[NOM]</ta>
            <ta e="T97" id="Seg_3032" s="T96">be-PST2.[3SG]</ta>
            <ta e="T98" id="Seg_3033" s="T97">girl-3SG.[NOM]</ta>
            <ta e="T99" id="Seg_3034" s="T98">human.being-ACC</ta>
            <ta e="T100" id="Seg_3035" s="T99">see-CVB.SEQ</ta>
            <ta e="T101" id="Seg_3036" s="T100">after</ta>
            <ta e="T102" id="Seg_3037" s="T101">startle-CVB.SEQ</ta>
            <ta e="T103" id="Seg_3038" s="T102">axe-3SG-ACC</ta>
            <ta e="T104" id="Seg_3039" s="T103">throw-CVB.SEQ</ta>
            <ta e="T105" id="Seg_3040" s="T104">run-CVB.SEQ</ta>
            <ta e="T106" id="Seg_3041" s="T105">go-PRS.[3SG]</ta>
            <ta e="T107" id="Seg_3042" s="T106">run-CVB.SEQ</ta>
            <ta e="T108" id="Seg_3043" s="T107">go-PRS.[3SG]</ta>
            <ta e="T109" id="Seg_3044" s="T108">and</ta>
            <ta e="T110" id="Seg_3045" s="T109">such-ACC</ta>
            <ta e="T111" id="Seg_3046" s="T110">see-PST1-1SG</ta>
            <ta e="T112" id="Seg_3047" s="T111">say-CVB.SEQ</ta>
            <ta e="T113" id="Seg_3048" s="T112">people-3SG-DAT/LOC</ta>
            <ta e="T114" id="Seg_3049" s="T113">tell-PRS.[3SG]</ta>
            <ta e="T115" id="Seg_3050" s="T114">this-3SG-3PL.[NOM]</ta>
            <ta e="T116" id="Seg_3051" s="T115">how.much</ta>
            <ta e="T117" id="Seg_3052" s="T116">NEG</ta>
            <ta e="T118" id="Seg_3053" s="T117">human.being.[NOM]</ta>
            <ta e="T119" id="Seg_3054" s="T118">be-CVB.SEQ</ta>
            <ta e="T120" id="Seg_3055" s="T119">go-CVB.SEQ-3PL</ta>
            <ta e="T121" id="Seg_3056" s="T120">get.tired-CVB.SEQ</ta>
            <ta e="T122" id="Seg_3057" s="T121">hunger-EP-PTCP.PST</ta>
            <ta e="T123" id="Seg_3058" s="T122">human.being-3PL-ACC</ta>
            <ta e="T124" id="Seg_3059" s="T123">fur-DAT/LOC</ta>
            <ta e="T125" id="Seg_3060" s="T124">raise-CVB.SEQ</ta>
            <ta e="T126" id="Seg_3061" s="T125">bring-PRS-3PL</ta>
            <ta e="T127" id="Seg_3062" s="T126">that</ta>
            <ta e="T128" id="Seg_3063" s="T127">bring-PTCP.PST-3PL-DAT/LOC</ta>
            <ta e="T129" id="Seg_3064" s="T128">one</ta>
            <ta e="T130" id="Seg_3065" s="T129">snow.[NOM]</ta>
            <ta e="T131" id="Seg_3066" s="T130">like</ta>
            <ta e="T132" id="Seg_3067" s="T131">hair-PROPR</ta>
            <ta e="T133" id="Seg_3068" s="T132">old.man.[NOM]</ta>
            <ta e="T134" id="Seg_3069" s="T133">speak-PRS.[3SG]</ta>
            <ta e="T135" id="Seg_3070" s="T134">hey</ta>
            <ta e="T136" id="Seg_3071" s="T135">what-ACC</ta>
            <ta e="T137" id="Seg_3072" s="T136">wonder-PRS-2PL</ta>
            <ta e="T138" id="Seg_3073" s="T137">tale-DAT/LOC</ta>
            <ta e="T139" id="Seg_3074" s="T138">and</ta>
            <ta e="T140" id="Seg_3075" s="T139">there.is</ta>
            <ta e="T141" id="Seg_3076" s="T140">ancestor.[NOM]</ta>
            <ta e="T142" id="Seg_3077" s="T141">story-DAT/LOC</ta>
            <ta e="T143" id="Seg_3078" s="T142">writing-DAT/LOC</ta>
            <ta e="T144" id="Seg_3079" s="T143">and</ta>
            <ta e="T145" id="Seg_3080" s="T144">there.is</ta>
            <ta e="T146" id="Seg_3081" s="T145">such</ta>
            <ta e="T147" id="Seg_3082" s="T146">two</ta>
            <ta e="T148" id="Seg_3083" s="T147">eye-PROPR</ta>
            <ta e="T149" id="Seg_3084" s="T148">people.[NOM]</ta>
            <ta e="T150" id="Seg_3085" s="T149">there.is</ta>
            <ta e="T151" id="Seg_3086" s="T150">say-CVB.SEQ</ta>
            <ta e="T152" id="Seg_3087" s="T151">well</ta>
            <ta e="T153" id="Seg_3088" s="T152">hunger-PTCP.PST</ta>
            <ta e="T154" id="Seg_3089" s="T153">human.being.[NOM]</ta>
            <ta e="T155" id="Seg_3090" s="T154">custom-3SG-INSTR</ta>
            <ta e="T156" id="Seg_3091" s="T155">too</ta>
            <ta e="T157" id="Seg_3092" s="T156">eat-EP-CAUS-EP-NEG.CVB</ta>
            <ta e="T158" id="Seg_3093" s="T157">eat-EP-CAUS-CVB.SEQ</ta>
            <ta e="T159" id="Seg_3094" s="T158">go-EP-IMP.2PL</ta>
            <ta e="T160" id="Seg_3095" s="T159">need-CVB.SEQ</ta>
            <ta e="T161" id="Seg_3096" s="T160">self-3SG-GEN</ta>
            <ta e="T162" id="Seg_3097" s="T161">earth-3SG-ABL</ta>
            <ta e="T163" id="Seg_3098" s="T162">this-INSTR</ta>
            <ta e="T164" id="Seg_3099" s="T163">come-PST2.[3SG]</ta>
            <ta e="T165" id="Seg_3100" s="T164">MOD</ta>
            <ta e="T166" id="Seg_3101" s="T165">probably</ta>
            <ta e="T167" id="Seg_3102" s="T166">say-PRS.[3SG]</ta>
            <ta e="T168" id="Seg_3103" s="T167">this.[NOM]</ta>
            <ta e="T169" id="Seg_3104" s="T168">rich</ta>
            <ta e="T170" id="Seg_3105" s="T169">very</ta>
            <ta e="T171" id="Seg_3106" s="T170">people.[NOM]</ta>
            <ta e="T172" id="Seg_3107" s="T171">be-PST2.[3SG]</ta>
            <ta e="T173" id="Seg_3108" s="T172">fox-3PL.[NOM]</ta>
            <ta e="T174" id="Seg_3109" s="T173">polar.fox-3PL.[NOM]</ta>
            <ta e="T175" id="Seg_3110" s="T174">house-3PL-GEN</ta>
            <ta e="T176" id="Seg_3111" s="T175">door-3SG-DAT/LOC</ta>
            <ta e="T177" id="Seg_3112" s="T176">self-3SG.[NOM]</ta>
            <ta e="T178" id="Seg_3113" s="T177">come-PTCP.PRS</ta>
            <ta e="T179" id="Seg_3114" s="T178">be-PST2.[3SG]</ta>
            <ta e="T180" id="Seg_3115" s="T179">this</ta>
            <ta e="T181" id="Seg_3116" s="T180">human.being.[NOM]</ta>
            <ta e="T182" id="Seg_3117" s="T181">here</ta>
            <ta e="T183" id="Seg_3118" s="T182">life-VBZ-PRS.[3SG]</ta>
            <ta e="T184" id="Seg_3119" s="T183">life-VBZ-PTCP.PST</ta>
            <ta e="T185" id="Seg_3120" s="T184">family-3SG.[NOM]</ta>
            <ta e="T186" id="Seg_3121" s="T185">two</ta>
            <ta e="T187" id="Seg_3122" s="T186">daughter-PROPR.[NOM]</ta>
            <ta e="T188" id="Seg_3123" s="T187">be-PST2-3PL</ta>
            <ta e="T189" id="Seg_3124" s="T188">this-ABL</ta>
            <ta e="T190" id="Seg_3125" s="T189">old-3SG-ACC</ta>
            <ta e="T191" id="Seg_3126" s="T190">father-3SG.[NOM]</ta>
            <ta e="T192" id="Seg_3127" s="T191">wife.[NOM]</ta>
            <ta e="T193" id="Seg_3128" s="T192">give-PRS.[3SG]</ta>
            <ta e="T194" id="Seg_3129" s="T193">boy-DAT/LOC</ta>
            <ta e="T195" id="Seg_3130" s="T194">this-DAT/LOC</ta>
            <ta e="T196" id="Seg_3131" s="T195">this</ta>
            <ta e="T197" id="Seg_3132" s="T196">see-PTCP.HAB</ta>
            <ta e="T198" id="Seg_3133" s="T197">girl.[NOM]</ta>
            <ta e="T199" id="Seg_3134" s="T198">older.sister-3SG-DAT/LOC</ta>
            <ta e="T200" id="Seg_3135" s="T199">envy-CVB.SEQ</ta>
            <ta e="T201" id="Seg_3136" s="T200">1SG.[NOM]</ta>
            <ta e="T202" id="Seg_3137" s="T201">go-FUT-NEC.[3SG]</ta>
            <ta e="T203" id="Seg_3138" s="T202">self-1SG.[NOM]</ta>
            <ta e="T204" id="Seg_3139" s="T203">find-NMNZ-1SG-DAT/LOC</ta>
            <ta e="T205" id="Seg_3140" s="T204">say-CVB.SEQ</ta>
            <ta e="T206" id="Seg_3141" s="T205">cry-PRS.[3SG]</ta>
            <ta e="T207" id="Seg_3142" s="T206">that-ACC</ta>
            <ta e="T208" id="Seg_3143" s="T207">hear-EP-NEG.CVB</ta>
            <ta e="T209" id="Seg_3144" s="T208">however</ta>
            <ta e="T210" id="Seg_3145" s="T209">old</ta>
            <ta e="T211" id="Seg_3146" s="T210">daughter-ACC</ta>
            <ta e="T212" id="Seg_3147" s="T211">give-PRS-3PL</ta>
            <ta e="T213" id="Seg_3148" s="T212">boy-DAT/LOC</ta>
            <ta e="T214" id="Seg_3149" s="T213">one</ta>
            <ta e="T215" id="Seg_3150" s="T214">year-EP-INSTR</ta>
            <ta e="T216" id="Seg_3151" s="T215">two</ta>
            <ta e="T217" id="Seg_3152" s="T216">eye-PROPR</ta>
            <ta e="T218" id="Seg_3153" s="T217">boy.[NOM]</ta>
            <ta e="T219" id="Seg_3154" s="T218">child.[NOM]</ta>
            <ta e="T220" id="Seg_3155" s="T219">be.born-PRS.[3SG]</ta>
            <ta e="T221" id="Seg_3156" s="T220">well</ta>
            <ta e="T222" id="Seg_3157" s="T221">long</ta>
            <ta e="T223" id="Seg_3158" s="T222">very</ta>
            <ta e="T224" id="Seg_3159" s="T223">live-PRS-3PL</ta>
            <ta e="T225" id="Seg_3160" s="T224">like.that</ta>
            <ta e="T226" id="Seg_3161" s="T225">this</ta>
            <ta e="T227" id="Seg_3162" s="T226">half.[NOM]</ta>
            <ta e="T228" id="Seg_3163" s="T227">eye-PROPR-PL.[NOM]</ta>
            <ta e="T229" id="Seg_3164" s="T228">many</ta>
            <ta e="T230" id="Seg_3165" s="T229">very</ta>
            <ta e="T231" id="Seg_3166" s="T230">people.[NOM]</ta>
            <ta e="T232" id="Seg_3167" s="T231">be-PST2-3PL</ta>
            <ta e="T233" id="Seg_3168" s="T232">once</ta>
            <ta e="T234" id="Seg_3169" s="T233">ship-3SG-INSTR</ta>
            <ta e="T235" id="Seg_3170" s="T234">czar-3PL.[NOM]</ta>
            <ta e="T236" id="Seg_3171" s="T235">tax.[NOM]</ta>
            <ta e="T237" id="Seg_3172" s="T236">gather-CVB.SIM</ta>
            <ta e="T238" id="Seg_3173" s="T237">come-PRS.[3SG]</ta>
            <ta e="T239" id="Seg_3174" s="T238">3SG-ABL</ta>
            <ta e="T240" id="Seg_3175" s="T239">human.being.[NOM]</ta>
            <ta e="T241" id="Seg_3176" s="T240">come-PRS.[3SG]</ta>
            <ta e="T242" id="Seg_3177" s="T241">this</ta>
            <ta e="T243" id="Seg_3178" s="T242">prince.[NOM]</ta>
            <ta e="T244" id="Seg_3179" s="T243">father_in_law-3SG-DAT/LOC</ta>
            <ta e="T245" id="Seg_3180" s="T244">2SG.[NOM]</ta>
            <ta e="T246" id="Seg_3181" s="T245">two</ta>
            <ta e="T247" id="Seg_3182" s="T246">eye-PROPR</ta>
            <ta e="T248" id="Seg_3183" s="T247">son_in_law-PROPR.[NOM]</ta>
            <ta e="T249" id="Seg_3184" s="T248">it.is.said-2SG</ta>
            <ta e="T250" id="Seg_3185" s="T249">2SG.[NOM]</ta>
            <ta e="T251" id="Seg_3186" s="T250">that-3SG-2SG-ACC</ta>
            <ta e="T252" id="Seg_3187" s="T251">czar.[NOM]</ta>
            <ta e="T253" id="Seg_3188" s="T252">see-CVB.PURP</ta>
            <ta e="T254" id="Seg_3189" s="T253">want-PRS.[3SG]</ta>
            <ta e="T255" id="Seg_3190" s="T254">say-PRS.[3SG]</ta>
            <ta e="T256" id="Seg_3191" s="T255">father_in_law-3SG.[NOM]</ta>
            <ta e="T257" id="Seg_3192" s="T256">speak-PRS.[3SG]</ta>
            <ta e="T258" id="Seg_3193" s="T257">swan-1SG.[NOM]</ta>
            <ta e="T259" id="Seg_3194" s="T258">go.[IMP.2SG]</ta>
            <ta e="T260" id="Seg_3195" s="T259">czar.[NOM]</ta>
            <ta e="T261" id="Seg_3196" s="T260">see-FUT.[3SG]</ta>
            <ta e="T262" id="Seg_3197" s="T261">just</ta>
            <ta e="T263" id="Seg_3198" s="T262">EMPH</ta>
            <ta e="T264" id="Seg_3199" s="T263">2SG.[NOM]</ta>
            <ta e="T265" id="Seg_3200" s="T264">go.[IMP.2SG]</ta>
            <ta e="T266" id="Seg_3201" s="T265">go-NEG-TEMP-2SG</ta>
            <ta e="T267" id="Seg_3202" s="T266">head-1PL-ACC</ta>
            <ta e="T268" id="Seg_3203" s="T267">cut-FUT.[3SG]</ta>
            <ta e="T269" id="Seg_3204" s="T268">prince.[NOM]</ta>
            <ta e="T270" id="Seg_3205" s="T269">czar-DAT/LOC</ta>
            <ta e="T271" id="Seg_3206" s="T270">go-PRS.[3SG]</ta>
            <ta e="T272" id="Seg_3207" s="T271">that-3SG-3SG.[NOM]</ta>
            <ta e="T273" id="Seg_3208" s="T272">speak-PRS.[3SG]</ta>
            <ta e="T274" id="Seg_3209" s="T273">1SG.[NOM]</ta>
            <ta e="T275" id="Seg_3210" s="T274">2SG-ACC</ta>
            <ta e="T276" id="Seg_3211" s="T275">here</ta>
            <ta e="T277" id="Seg_3212" s="T276">rich</ta>
            <ta e="T278" id="Seg_3213" s="T277">human.being.[NOM]</ta>
            <ta e="T279" id="Seg_3214" s="T278">make-CVB.SEQ</ta>
            <ta e="T280" id="Seg_3215" s="T279">prepare-FUT-1SG</ta>
            <ta e="T281" id="Seg_3216" s="T280">2SG.[NOM]</ta>
            <ta e="T282" id="Seg_3217" s="T281">here</ta>
            <ta e="T283" id="Seg_3218" s="T282">stay-FUT-2SG</ta>
            <ta e="T284" id="Seg_3219" s="T283">Q</ta>
            <ta e="T285" id="Seg_3220" s="T284">country-2SG.[NOM]</ta>
            <ta e="T286" id="Seg_3221" s="T285">to</ta>
            <ta e="T287" id="Seg_3222" s="T286">come.back-FUT-2SG</ta>
            <ta e="T288" id="Seg_3223" s="T287">Q</ta>
            <ta e="T289" id="Seg_3224" s="T288">that-ACC</ta>
            <ta e="T290" id="Seg_3225" s="T289">come.back-FUT-1SG</ta>
            <ta e="T291" id="Seg_3226" s="T290">say-PST1-3SG</ta>
            <ta e="T292" id="Seg_3227" s="T291">czar.[NOM]</ta>
            <ta e="T293" id="Seg_3228" s="T292">that-DAT/LOC</ta>
            <ta e="T294" id="Seg_3229" s="T293">say-PST1-3SG</ta>
            <ta e="T295" id="Seg_3230" s="T294">distant.[NOM]</ta>
            <ta e="T296" id="Seg_3231" s="T295">earth.[NOM]</ta>
            <ta e="T297" id="Seg_3232" s="T296">water.[NOM]</ta>
            <ta e="T298" id="Seg_3233" s="T297">bring-PTCP.PST-3SG-ACC</ta>
            <ta e="T299" id="Seg_3234" s="T298">like</ta>
            <ta e="T300" id="Seg_3235" s="T299">go-FUT-2SG</ta>
            <ta e="T301" id="Seg_3236" s="T300">NEG-3SG</ta>
            <ta e="T302" id="Seg_3237" s="T301">1SG.[NOM]</ta>
            <ta e="T303" id="Seg_3238" s="T302">letter.[NOM]</ta>
            <ta e="T304" id="Seg_3239" s="T303">give-FUT-1SG</ta>
            <ta e="T305" id="Seg_3240" s="T304">who.[NOM]</ta>
            <ta e="T306" id="Seg_3241" s="T305">INDEF</ta>
            <ta e="T307" id="Seg_3242" s="T306">2SG-ACC</ta>
            <ta e="T308" id="Seg_3243" s="T307">delay-POSS</ta>
            <ta e="T309" id="Seg_3244" s="T308">NEG</ta>
            <ta e="T310" id="Seg_3245" s="T309">bring-FUT.[3SG]</ta>
            <ta e="T311" id="Seg_3246" s="T310">say-CVB.SEQ</ta>
            <ta e="T312" id="Seg_3247" s="T311">letter.[NOM]</ta>
            <ta e="T313" id="Seg_3248" s="T312">give-PRS.[3SG]</ta>
            <ta e="T314" id="Seg_3249" s="T313">that-ACC</ta>
            <ta e="T315" id="Seg_3250" s="T314">take-CVB.SEQ</ta>
            <ta e="T316" id="Seg_3251" s="T315">after</ta>
            <ta e="T317" id="Seg_3252" s="T316">however</ta>
            <ta e="T318" id="Seg_3253" s="T317">how.much</ta>
            <ta e="T319" id="Seg_3254" s="T318">NEG</ta>
            <ta e="T320" id="Seg_3255" s="T319">year.[NOM]</ta>
            <ta e="T321" id="Seg_3256" s="T320">live-PRS.[3SG]</ta>
            <ta e="T322" id="Seg_3257" s="T321">this</ta>
            <ta e="T323" id="Seg_3258" s="T322">live-CVB.SEQ</ta>
            <ta e="T324" id="Seg_3259" s="T323">after</ta>
            <ta e="T325" id="Seg_3260" s="T324">go-CVB.PURP</ta>
            <ta e="T326" id="Seg_3261" s="T325">prepare-PRS.[3SG]</ta>
            <ta e="T327" id="Seg_3262" s="T326">woman-3SG-ACC</ta>
            <ta e="T328" id="Seg_3263" s="T327">take-CVB.SEQ</ta>
            <ta e="T329" id="Seg_3264" s="T328">this-DAT/LOC</ta>
            <ta e="T330" id="Seg_3265" s="T329">little</ta>
            <ta e="T331" id="Seg_3266" s="T330">daughter.[NOM]</ta>
            <ta e="T332" id="Seg_3267" s="T331">cry-CVB.SEQ</ta>
            <ta e="T333" id="Seg_3268" s="T332">go-PRS.[3SG]</ta>
            <ta e="T334" id="Seg_3269" s="T333">1SG.[NOM]</ta>
            <ta e="T335" id="Seg_3270" s="T334">find-PTCP.PST</ta>
            <ta e="T336" id="Seg_3271" s="T335">human.being-1SG-ABL</ta>
            <ta e="T337" id="Seg_3272" s="T336">stay-NEG-1SG</ta>
            <ta e="T338" id="Seg_3273" s="T337">together</ta>
            <ta e="T339" id="Seg_3274" s="T338">come.along-FUT-1SG</ta>
            <ta e="T340" id="Seg_3275" s="T339">elder.sister-3SG-DAT/LOC</ta>
            <ta e="T341" id="Seg_3276" s="T340">speak-PRS.[3SG]</ta>
            <ta e="T342" id="Seg_3277" s="T341">2SG.[NOM]</ta>
            <ta e="T343" id="Seg_3278" s="T342">see-PTCP.PRS</ta>
            <ta e="T344" id="Seg_3279" s="T343">make-PTCP.FUT-2SG-ACC</ta>
            <ta e="T345" id="Seg_3280" s="T344">two</ta>
            <ta e="T346" id="Seg_3281" s="T345">eye-PROPR</ta>
            <ta e="T347" id="Seg_3282" s="T346">child-PROPR-2SG</ta>
            <ta e="T348" id="Seg_3283" s="T347">1SG.[NOM]</ta>
            <ta e="T349" id="Seg_3284" s="T348">come.along-FUT-1SG</ta>
            <ta e="T350" id="Seg_3285" s="T349">3SG-ACC</ta>
            <ta e="T351" id="Seg_3286" s="T350">with</ta>
            <ta e="T352" id="Seg_3287" s="T351">father-3SG.[NOM]</ta>
            <ta e="T353" id="Seg_3288" s="T352">advise-CVB.SEQ</ta>
            <ta e="T354" id="Seg_3289" s="T353">will-VBZ-PRS.[3SG]</ta>
            <ta e="T355" id="Seg_3290" s="T354">so</ta>
            <ta e="T356" id="Seg_3291" s="T355">little</ta>
            <ta e="T357" id="Seg_3292" s="T356">daughter.[NOM]</ta>
            <ta e="T358" id="Seg_3293" s="T357">together</ta>
            <ta e="T359" id="Seg_3294" s="T358">come.along-PRS.[3SG]</ta>
            <ta e="T360" id="Seg_3295" s="T359">well</ta>
            <ta e="T361" id="Seg_3296" s="T360">long</ta>
            <ta e="T362" id="Seg_3297" s="T361">very</ta>
            <ta e="T363" id="Seg_3298" s="T362">travel-CVB.SEQ</ta>
            <ta e="T364" id="Seg_3299" s="T363">reach-PRS-3PL</ta>
            <ta e="T365" id="Seg_3300" s="T364">country-3PL-DAT/LOC</ta>
            <ta e="T366" id="Seg_3301" s="T365">like.that</ta>
            <ta e="T367" id="Seg_3302" s="T366">story.[NOM]</ta>
            <ta e="T368" id="Seg_3303" s="T367">there.is</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_3304" s="T0">vor.langer.Zeit</ta>
            <ta e="T2" id="Seg_3305" s="T1">eins</ta>
            <ta e="T3" id="Seg_3306" s="T2">Junge-PROPR</ta>
            <ta e="T4" id="Seg_3307" s="T3">nenzisch</ta>
            <ta e="T5" id="Seg_3308" s="T4">Fürst-3SG.[NOM]</ta>
            <ta e="T6" id="Seg_3309" s="T5">leben-PST2.[3SG]</ta>
            <ta e="T7" id="Seg_3310" s="T6">im.Winter</ta>
            <ta e="T8" id="Seg_3311" s="T7">Meer-DAT/LOC</ta>
            <ta e="T9" id="Seg_3312" s="T8">Nenze-PL.[NOM]</ta>
            <ta e="T10" id="Seg_3313" s="T9">Beluga-VBZ-HAB-3PL</ta>
            <ta e="T11" id="Seg_3314" s="T10">dieses</ta>
            <ta e="T12" id="Seg_3315" s="T11">Fürst.[NOM]</ta>
            <ta e="T13" id="Seg_3316" s="T12">Meer.[NOM]</ta>
            <ta e="T14" id="Seg_3317" s="T13">Eis-3SG-INSTR</ta>
            <ta e="T15" id="Seg_3318" s="T14">Beluga.[NOM]</ta>
            <ta e="T16" id="Seg_3319" s="T15">Eisloch-3SG.[NOM]</ta>
            <ta e="T17" id="Seg_3320" s="T16">zerschlagen-CVB.SIM</ta>
            <ta e="T18" id="Seg_3321" s="T17">gehen-PRS.[3SG]</ta>
            <ta e="T19" id="Seg_3322" s="T18">dieses</ta>
            <ta e="T20" id="Seg_3323" s="T19">machen-CVB.SEQ</ta>
            <ta e="T21" id="Seg_3324" s="T20">nachdem</ta>
            <ta e="T22" id="Seg_3325" s="T21">zurückkommen-CVB.PURP</ta>
            <ta e="T23" id="Seg_3326" s="T22">wollen-PST2-3SG</ta>
            <ta e="T24" id="Seg_3327" s="T23">Eisscholle-3SG-ACC</ta>
            <ta e="T25" id="Seg_3328" s="T24">Meer.[NOM]</ta>
            <ta e="T26" id="Seg_3329" s="T25">zerbrechen-CVB.SEQ</ta>
            <ta e="T27" id="Seg_3330" s="T26">werfen-PST2.[3SG]</ta>
            <ta e="T28" id="Seg_3331" s="T27">drei</ta>
            <ta e="T29" id="Seg_3332" s="T28">Tag.[NOM]</ta>
            <ta e="T30" id="Seg_3333" s="T29">suchen-PRS.[3SG]</ta>
            <ta e="T31" id="Seg_3334" s="T30">hinausgehen-PTCP.PRS</ta>
            <ta e="T32" id="Seg_3335" s="T31">Ort-3SG-ACC</ta>
            <ta e="T33" id="Seg_3336" s="T32">Unglück-3SG.[NOM]</ta>
            <ta e="T34" id="Seg_3337" s="T33">sein-CVB.SEQ</ta>
            <ta e="T35" id="Seg_3338" s="T34">drei</ta>
            <ta e="T36" id="Seg_3339" s="T35">Rentier-3SG-ABL</ta>
            <ta e="T37" id="Seg_3340" s="T36">eins-3SG-ACC</ta>
            <ta e="T38" id="Seg_3341" s="T37">rohes.Fleisch-VBZ-CVB.SEQ</ta>
            <ta e="T39" id="Seg_3342" s="T38">essen-PRS.[3SG]</ta>
            <ta e="T40" id="Seg_3343" s="T39">dann</ta>
            <ta e="T41" id="Seg_3344" s="T40">wieder</ta>
            <ta e="T42" id="Seg_3345" s="T41">go-CVB.SIM</ta>
            <ta e="T43" id="Seg_3346" s="T42">nicht.schaffen-CVB.SEQ</ta>
            <ta e="T44" id="Seg_3347" s="T43">zwei</ta>
            <ta e="T45" id="Seg_3348" s="T44">Rentier-3SG-ACC</ta>
            <ta e="T46" id="Seg_3349" s="T45">vernichten-PRS.[3SG]</ta>
            <ta e="T47" id="Seg_3350" s="T46">töten-PRS.[3SG]</ta>
            <ta e="T48" id="Seg_3351" s="T47">Eisscholle-3SG.[NOM]</ta>
            <ta e="T49" id="Seg_3352" s="T48">Schamane.[NOM]</ta>
            <ta e="T50" id="Seg_3353" s="T49">Schamanentrommel-3SG-ACC</ta>
            <ta e="T51" id="Seg_3354" s="T50">von.der.Größe.von</ta>
            <ta e="T52" id="Seg_3355" s="T51">werden-PRS.[3SG]</ta>
            <ta e="T53" id="Seg_3356" s="T52">jenes-ACC</ta>
            <ta e="T54" id="Seg_3357" s="T53">Sturm.[NOM]</ta>
            <ta e="T55" id="Seg_3358" s="T54">wie</ta>
            <ta e="T56" id="Seg_3359" s="T55">INDEF</ta>
            <ta e="T57" id="Seg_3360" s="T56">Insel.[NOM]</ta>
            <ta e="T58" id="Seg_3361" s="T57">Ufer-3SG-DAT/LOC</ta>
            <ta e="T59" id="Seg_3362" s="T58">fließen-CVB.SEQ</ta>
            <ta e="T60" id="Seg_3363" s="T59">kommen-PST2.[3SG]</ta>
            <ta e="T61" id="Seg_3364" s="T60">Baum-DAT/LOC</ta>
            <ta e="T62" id="Seg_3365" s="T61">zu</ta>
            <ta e="T63" id="Seg_3366" s="T62">werfen-PRS.[3SG]</ta>
            <ta e="T64" id="Seg_3367" s="T63">dieses</ta>
            <ta e="T65" id="Seg_3368" s="T64">Insel-DAT/LOC</ta>
            <ta e="T66" id="Seg_3369" s="T65">hinausgehen-EP-PST2-3SG</ta>
            <ta e="T67" id="Seg_3370" s="T66">Baum-PROPR.[NOM]</ta>
            <ta e="T68" id="Seg_3371" s="T67">sehr</ta>
            <ta e="T69" id="Seg_3372" s="T68">wo</ta>
            <ta e="T70" id="Seg_3373" s="T69">INDEF</ta>
            <ta e="T71" id="Seg_3374" s="T70">Beil.[NOM]</ta>
            <ta e="T72" id="Seg_3375" s="T71">Geräusch-3SG.[NOM]</ta>
            <ta e="T73" id="Seg_3376" s="T72">gehört.werden-PRS.[3SG]</ta>
            <ta e="T74" id="Seg_3377" s="T73">hierher</ta>
            <ta e="T75" id="Seg_3378" s="T74">doch</ta>
            <ta e="T76" id="Seg_3379" s="T75">jeder</ta>
            <ta e="T77" id="Seg_3380" s="T76">Kraft-3SG-ACC</ta>
            <ta e="T78" id="Seg_3381" s="T77">gehen-CVB.SEQ</ta>
            <ta e="T79" id="Seg_3382" s="T78">ankommen-PRS.[3SG]</ta>
            <ta e="T80" id="Seg_3383" s="T79">ankommen-CVB.SEQ</ta>
            <ta e="T81" id="Seg_3384" s="T80">sehen-PST2-3SG</ta>
            <ta e="T82" id="Seg_3385" s="T81">eins</ta>
            <ta e="T83" id="Seg_3386" s="T82">so.klein-INTNS</ta>
            <ta e="T84" id="Seg_3387" s="T83">Mädchen.[NOM]</ta>
            <ta e="T85" id="Seg_3388" s="T84">3SG-DAT/LOC</ta>
            <ta e="T86" id="Seg_3389" s="T85">Rücken-3SG-INSTR</ta>
            <ta e="T87" id="Seg_3390" s="T86">stehen-CVB.SEQ</ta>
            <ta e="T88" id="Seg_3391" s="T87">Holz.[NOM]</ta>
            <ta e="T89" id="Seg_3392" s="T88">zerschneiden-CVB.SIM</ta>
            <ta e="T90" id="Seg_3393" s="T89">stehen-PRS.[3SG]</ta>
            <ta e="T91" id="Seg_3394" s="T90">dieses</ta>
            <ta e="T92" id="Seg_3395" s="T91">Mädchen.[NOM]</ta>
            <ta e="T93" id="Seg_3396" s="T92">verfolgen-EP-RECP/COLL-PRS.[3SG]</ta>
            <ta e="T94" id="Seg_3397" s="T93">dann</ta>
            <ta e="T95" id="Seg_3398" s="T94">Hälfte.[NOM]</ta>
            <ta e="T96" id="Seg_3399" s="T95">Auge-PROPR.[NOM]</ta>
            <ta e="T97" id="Seg_3400" s="T96">sein-PST2.[3SG]</ta>
            <ta e="T98" id="Seg_3401" s="T97">Mädchen-3SG.[NOM]</ta>
            <ta e="T99" id="Seg_3402" s="T98">Mensch-ACC</ta>
            <ta e="T100" id="Seg_3403" s="T99">sehen-CVB.SEQ</ta>
            <ta e="T101" id="Seg_3404" s="T100">nachdem</ta>
            <ta e="T102" id="Seg_3405" s="T101">erschrecken-CVB.SEQ</ta>
            <ta e="T103" id="Seg_3406" s="T102">Beil-3SG-ACC</ta>
            <ta e="T104" id="Seg_3407" s="T103">werfen-CVB.SEQ</ta>
            <ta e="T105" id="Seg_3408" s="T104">laufen-CVB.SEQ</ta>
            <ta e="T106" id="Seg_3409" s="T105">gehen-PRS.[3SG]</ta>
            <ta e="T107" id="Seg_3410" s="T106">laufen-CVB.SEQ</ta>
            <ta e="T108" id="Seg_3411" s="T107">gehen-PRS.[3SG]</ta>
            <ta e="T109" id="Seg_3412" s="T108">und</ta>
            <ta e="T110" id="Seg_3413" s="T109">solch-ACC</ta>
            <ta e="T111" id="Seg_3414" s="T110">sehen-PST1-1SG</ta>
            <ta e="T112" id="Seg_3415" s="T111">sagen-CVB.SEQ</ta>
            <ta e="T113" id="Seg_3416" s="T112">Leute-3SG-DAT/LOC</ta>
            <ta e="T114" id="Seg_3417" s="T113">erzählen-PRS.[3SG]</ta>
            <ta e="T115" id="Seg_3418" s="T114">dieses-3SG-3PL.[NOM]</ta>
            <ta e="T116" id="Seg_3419" s="T115">wie.viel</ta>
            <ta e="T117" id="Seg_3420" s="T116">NEG</ta>
            <ta e="T118" id="Seg_3421" s="T117">Mensch.[NOM]</ta>
            <ta e="T119" id="Seg_3422" s="T118">sein-CVB.SEQ</ta>
            <ta e="T120" id="Seg_3423" s="T119">gehen-CVB.SEQ-3PL</ta>
            <ta e="T121" id="Seg_3424" s="T120">müde.werden-CVB.SEQ</ta>
            <ta e="T122" id="Seg_3425" s="T121">hungern-EP-PTCP.PST</ta>
            <ta e="T123" id="Seg_3426" s="T122">Mensch-3PL-ACC</ta>
            <ta e="T124" id="Seg_3427" s="T123">Fell-DAT/LOC</ta>
            <ta e="T125" id="Seg_3428" s="T124">heben-CVB.SEQ</ta>
            <ta e="T126" id="Seg_3429" s="T125">bringen-PRS-3PL</ta>
            <ta e="T127" id="Seg_3430" s="T126">jenes</ta>
            <ta e="T128" id="Seg_3431" s="T127">bringen-PTCP.PST-3PL-DAT/LOC</ta>
            <ta e="T129" id="Seg_3432" s="T128">eins</ta>
            <ta e="T130" id="Seg_3433" s="T129">Schnee.[NOM]</ta>
            <ta e="T131" id="Seg_3434" s="T130">wie</ta>
            <ta e="T132" id="Seg_3435" s="T131">Haar-PROPR</ta>
            <ta e="T133" id="Seg_3436" s="T132">alter.Mann.[NOM]</ta>
            <ta e="T134" id="Seg_3437" s="T133">sprechen-PRS.[3SG]</ta>
            <ta e="T135" id="Seg_3438" s="T134">hey</ta>
            <ta e="T136" id="Seg_3439" s="T135">was-ACC</ta>
            <ta e="T137" id="Seg_3440" s="T136">sich.wundern-PRS-2PL</ta>
            <ta e="T138" id="Seg_3441" s="T137">Märchen-DAT/LOC</ta>
            <ta e="T139" id="Seg_3442" s="T138">und</ta>
            <ta e="T140" id="Seg_3443" s="T139">es.gibt</ta>
            <ta e="T141" id="Seg_3444" s="T140">Vorfahre.[NOM]</ta>
            <ta e="T142" id="Seg_3445" s="T141">Erzählung-DAT/LOC</ta>
            <ta e="T143" id="Seg_3446" s="T142">Schrift-DAT/LOC</ta>
            <ta e="T144" id="Seg_3447" s="T143">und</ta>
            <ta e="T145" id="Seg_3448" s="T144">es.gibt</ta>
            <ta e="T146" id="Seg_3449" s="T145">solch</ta>
            <ta e="T147" id="Seg_3450" s="T146">zwei</ta>
            <ta e="T148" id="Seg_3451" s="T147">Auge-PROPR</ta>
            <ta e="T149" id="Seg_3452" s="T148">Volk.[NOM]</ta>
            <ta e="T150" id="Seg_3453" s="T149">es.gibt</ta>
            <ta e="T151" id="Seg_3454" s="T150">sagen-CVB.SEQ</ta>
            <ta e="T152" id="Seg_3455" s="T151">doch</ta>
            <ta e="T153" id="Seg_3456" s="T152">hungern-PTCP.PST</ta>
            <ta e="T154" id="Seg_3457" s="T153">Mensch.[NOM]</ta>
            <ta e="T155" id="Seg_3458" s="T154">Sitte-3SG-INSTR</ta>
            <ta e="T156" id="Seg_3459" s="T155">zu</ta>
            <ta e="T157" id="Seg_3460" s="T156">essen-EP-CAUS-EP-NEG.CVB</ta>
            <ta e="T158" id="Seg_3461" s="T157">essen-EP-CAUS-CVB.SEQ</ta>
            <ta e="T159" id="Seg_3462" s="T158">gehen-EP-IMP.2PL</ta>
            <ta e="T160" id="Seg_3463" s="T159">brauchen-CVB.SEQ</ta>
            <ta e="T161" id="Seg_3464" s="T160">selbst-3SG-GEN</ta>
            <ta e="T162" id="Seg_3465" s="T161">Erde-3SG-ABL</ta>
            <ta e="T163" id="Seg_3466" s="T162">dieses-INSTR</ta>
            <ta e="T164" id="Seg_3467" s="T163">kommen-PST2.[3SG]</ta>
            <ta e="T165" id="Seg_3468" s="T164">MOD</ta>
            <ta e="T166" id="Seg_3469" s="T165">wahrscheinlich</ta>
            <ta e="T167" id="Seg_3470" s="T166">sagen-PRS.[3SG]</ta>
            <ta e="T168" id="Seg_3471" s="T167">dieses.[NOM]</ta>
            <ta e="T169" id="Seg_3472" s="T168">reich</ta>
            <ta e="T170" id="Seg_3473" s="T169">sehr</ta>
            <ta e="T171" id="Seg_3474" s="T170">Volk.[NOM]</ta>
            <ta e="T172" id="Seg_3475" s="T171">sein-PST2.[3SG]</ta>
            <ta e="T173" id="Seg_3476" s="T172">Fuchs-3PL.[NOM]</ta>
            <ta e="T174" id="Seg_3477" s="T173">Polarfuchs-3PL.[NOM]</ta>
            <ta e="T175" id="Seg_3478" s="T174">Haus-3PL-GEN</ta>
            <ta e="T176" id="Seg_3479" s="T175">Tür-3SG-DAT/LOC</ta>
            <ta e="T177" id="Seg_3480" s="T176">selbst-3SG.[NOM]</ta>
            <ta e="T178" id="Seg_3481" s="T177">kommen-PTCP.PRS</ta>
            <ta e="T179" id="Seg_3482" s="T178">sein-PST2.[3SG]</ta>
            <ta e="T180" id="Seg_3483" s="T179">dieses</ta>
            <ta e="T181" id="Seg_3484" s="T180">Mensch.[NOM]</ta>
            <ta e="T182" id="Seg_3485" s="T181">hier</ta>
            <ta e="T183" id="Seg_3486" s="T182">Leben-VBZ-PRS.[3SG]</ta>
            <ta e="T184" id="Seg_3487" s="T183">Leben-VBZ-PTCP.PST</ta>
            <ta e="T185" id="Seg_3488" s="T184">Familie-3SG.[NOM]</ta>
            <ta e="T186" id="Seg_3489" s="T185">zwei</ta>
            <ta e="T187" id="Seg_3490" s="T186">Tochter-PROPR.[NOM]</ta>
            <ta e="T188" id="Seg_3491" s="T187">sein-PST2-3PL</ta>
            <ta e="T189" id="Seg_3492" s="T188">dieses-ABL</ta>
            <ta e="T190" id="Seg_3493" s="T189">alt-3SG-ACC</ta>
            <ta e="T191" id="Seg_3494" s="T190">Vater-3SG.[NOM]</ta>
            <ta e="T192" id="Seg_3495" s="T191">Frau.[NOM]</ta>
            <ta e="T193" id="Seg_3496" s="T192">geben-PRS.[3SG]</ta>
            <ta e="T194" id="Seg_3497" s="T193">Junge-DAT/LOC</ta>
            <ta e="T195" id="Seg_3498" s="T194">dieses-DAT/LOC</ta>
            <ta e="T196" id="Seg_3499" s="T195">dieses</ta>
            <ta e="T197" id="Seg_3500" s="T196">sehen-PTCP.HAB</ta>
            <ta e="T198" id="Seg_3501" s="T197">Mädchen.[NOM]</ta>
            <ta e="T199" id="Seg_3502" s="T198">ältere.Schwester-3SG-DAT/LOC</ta>
            <ta e="T200" id="Seg_3503" s="T199">beneiden-CVB.SEQ</ta>
            <ta e="T201" id="Seg_3504" s="T200">1SG.[NOM]</ta>
            <ta e="T202" id="Seg_3505" s="T201">gehen-FUT-NEC.[3SG]</ta>
            <ta e="T203" id="Seg_3506" s="T202">selbst-1SG.[NOM]</ta>
            <ta e="T204" id="Seg_3507" s="T203">finden-NMNZ-1SG-DAT/LOC</ta>
            <ta e="T205" id="Seg_3508" s="T204">sagen-CVB.SEQ</ta>
            <ta e="T206" id="Seg_3509" s="T205">weinen-PRS.[3SG]</ta>
            <ta e="T207" id="Seg_3510" s="T206">jenes-ACC</ta>
            <ta e="T208" id="Seg_3511" s="T207">hören-EP-NEG.CVB</ta>
            <ta e="T209" id="Seg_3512" s="T208">doch</ta>
            <ta e="T210" id="Seg_3513" s="T209">alt</ta>
            <ta e="T211" id="Seg_3514" s="T210">Tochter-ACC</ta>
            <ta e="T212" id="Seg_3515" s="T211">geben-PRS-3PL</ta>
            <ta e="T213" id="Seg_3516" s="T212">Junge-DAT/LOC</ta>
            <ta e="T214" id="Seg_3517" s="T213">eins</ta>
            <ta e="T215" id="Seg_3518" s="T214">Jahr-EP-INSTR</ta>
            <ta e="T216" id="Seg_3519" s="T215">zwei</ta>
            <ta e="T217" id="Seg_3520" s="T216">Auge-PROPR</ta>
            <ta e="T218" id="Seg_3521" s="T217">Junge.[NOM]</ta>
            <ta e="T219" id="Seg_3522" s="T218">Kind.[NOM]</ta>
            <ta e="T220" id="Seg_3523" s="T219">geboren.werden-PRS.[3SG]</ta>
            <ta e="T221" id="Seg_3524" s="T220">doch</ta>
            <ta e="T222" id="Seg_3525" s="T221">lange</ta>
            <ta e="T223" id="Seg_3526" s="T222">sehr</ta>
            <ta e="T224" id="Seg_3527" s="T223">leben-PRS-3PL</ta>
            <ta e="T225" id="Seg_3528" s="T224">so</ta>
            <ta e="T226" id="Seg_3529" s="T225">dieses</ta>
            <ta e="T227" id="Seg_3530" s="T226">Hälfte.[NOM]</ta>
            <ta e="T228" id="Seg_3531" s="T227">Auge-PROPR-PL.[NOM]</ta>
            <ta e="T229" id="Seg_3532" s="T228">viel</ta>
            <ta e="T230" id="Seg_3533" s="T229">sehr</ta>
            <ta e="T231" id="Seg_3534" s="T230">Volk.[NOM]</ta>
            <ta e="T232" id="Seg_3535" s="T231">sein-PST2-3PL</ta>
            <ta e="T233" id="Seg_3536" s="T232">einmal</ta>
            <ta e="T234" id="Seg_3537" s="T233">Schiff-3SG-INSTR</ta>
            <ta e="T235" id="Seg_3538" s="T234">Zar-3PL.[NOM]</ta>
            <ta e="T236" id="Seg_3539" s="T235">Steuer.[NOM]</ta>
            <ta e="T237" id="Seg_3540" s="T236">sammeln-CVB.SIM</ta>
            <ta e="T238" id="Seg_3541" s="T237">kommen-PRS.[3SG]</ta>
            <ta e="T239" id="Seg_3542" s="T238">3SG-ABL</ta>
            <ta e="T240" id="Seg_3543" s="T239">Mensch.[NOM]</ta>
            <ta e="T241" id="Seg_3544" s="T240">kommen-PRS.[3SG]</ta>
            <ta e="T242" id="Seg_3545" s="T241">dieses</ta>
            <ta e="T243" id="Seg_3546" s="T242">Fürst.[NOM]</ta>
            <ta e="T244" id="Seg_3547" s="T243">Schwiegervater-3SG-DAT/LOC</ta>
            <ta e="T245" id="Seg_3548" s="T244">2SG.[NOM]</ta>
            <ta e="T246" id="Seg_3549" s="T245">zwei</ta>
            <ta e="T247" id="Seg_3550" s="T246">Auge-PROPR</ta>
            <ta e="T248" id="Seg_3551" s="T247">Schwiegersohn-PROPR.[NOM]</ta>
            <ta e="T249" id="Seg_3552" s="T248">man.sagt-2SG</ta>
            <ta e="T250" id="Seg_3553" s="T249">2SG.[NOM]</ta>
            <ta e="T251" id="Seg_3554" s="T250">jenes-3SG-2SG-ACC</ta>
            <ta e="T252" id="Seg_3555" s="T251">Zar.[NOM]</ta>
            <ta e="T253" id="Seg_3556" s="T252">sehen-CVB.PURP</ta>
            <ta e="T254" id="Seg_3557" s="T253">wollen-PRS.[3SG]</ta>
            <ta e="T255" id="Seg_3558" s="T254">sagen-PRS.[3SG]</ta>
            <ta e="T256" id="Seg_3559" s="T255">Schwiegervater-3SG.[NOM]</ta>
            <ta e="T257" id="Seg_3560" s="T256">sprechen-PRS.[3SG]</ta>
            <ta e="T258" id="Seg_3561" s="T257">Schwan-1SG.[NOM]</ta>
            <ta e="T259" id="Seg_3562" s="T258">gehen.[IMP.2SG]</ta>
            <ta e="T260" id="Seg_3563" s="T259">Zar.[NOM]</ta>
            <ta e="T261" id="Seg_3564" s="T260">sehen-FUT.[3SG]</ta>
            <ta e="T262" id="Seg_3565" s="T261">nur</ta>
            <ta e="T263" id="Seg_3566" s="T262">EMPH</ta>
            <ta e="T264" id="Seg_3567" s="T263">2SG.[NOM]</ta>
            <ta e="T265" id="Seg_3568" s="T264">gehen.[IMP.2SG]</ta>
            <ta e="T266" id="Seg_3569" s="T265">gehen-NEG-TEMP-2SG</ta>
            <ta e="T267" id="Seg_3570" s="T266">Kopf-1PL-ACC</ta>
            <ta e="T268" id="Seg_3571" s="T267">schneiden-FUT.[3SG]</ta>
            <ta e="T269" id="Seg_3572" s="T268">Fürst.[NOM]</ta>
            <ta e="T270" id="Seg_3573" s="T269">Zar-DAT/LOC</ta>
            <ta e="T271" id="Seg_3574" s="T270">gehen-PRS.[3SG]</ta>
            <ta e="T272" id="Seg_3575" s="T271">jenes-3SG-3SG.[NOM]</ta>
            <ta e="T273" id="Seg_3576" s="T272">sprechen-PRS.[3SG]</ta>
            <ta e="T274" id="Seg_3577" s="T273">1SG.[NOM]</ta>
            <ta e="T275" id="Seg_3578" s="T274">2SG-ACC</ta>
            <ta e="T276" id="Seg_3579" s="T275">hier</ta>
            <ta e="T277" id="Seg_3580" s="T276">reich</ta>
            <ta e="T278" id="Seg_3581" s="T277">Mensch.[NOM]</ta>
            <ta e="T279" id="Seg_3582" s="T278">machen-CVB.SEQ</ta>
            <ta e="T280" id="Seg_3583" s="T279">vorbereiten-FUT-1SG</ta>
            <ta e="T281" id="Seg_3584" s="T280">2SG.[NOM]</ta>
            <ta e="T282" id="Seg_3585" s="T281">hier</ta>
            <ta e="T283" id="Seg_3586" s="T282">bleiben-FUT-2SG</ta>
            <ta e="T284" id="Seg_3587" s="T283">Q</ta>
            <ta e="T285" id="Seg_3588" s="T284">Land-2SG.[NOM]</ta>
            <ta e="T286" id="Seg_3589" s="T285">zu</ta>
            <ta e="T287" id="Seg_3590" s="T286">zurückkommen-FUT-2SG</ta>
            <ta e="T288" id="Seg_3591" s="T287">Q</ta>
            <ta e="T289" id="Seg_3592" s="T288">jenes-ACC</ta>
            <ta e="T290" id="Seg_3593" s="T289">zurückkommen-FUT-1SG</ta>
            <ta e="T291" id="Seg_3594" s="T290">sagen-PST1-3SG</ta>
            <ta e="T292" id="Seg_3595" s="T291">Zar.[NOM]</ta>
            <ta e="T293" id="Seg_3596" s="T292">jenes-DAT/LOC</ta>
            <ta e="T294" id="Seg_3597" s="T293">sagen-PST1-3SG</ta>
            <ta e="T295" id="Seg_3598" s="T294">fern.[NOM]</ta>
            <ta e="T296" id="Seg_3599" s="T295">Erde.[NOM]</ta>
            <ta e="T297" id="Seg_3600" s="T296">Wasser.[NOM]</ta>
            <ta e="T298" id="Seg_3601" s="T297">bringen-PTCP.PST-3SG-ACC</ta>
            <ta e="T299" id="Seg_3602" s="T298">wie</ta>
            <ta e="T300" id="Seg_3603" s="T299">gehen-FUT-2SG</ta>
            <ta e="T301" id="Seg_3604" s="T300">NEG-3SG</ta>
            <ta e="T302" id="Seg_3605" s="T301">1SG.[NOM]</ta>
            <ta e="T303" id="Seg_3606" s="T302">Brief.[NOM]</ta>
            <ta e="T304" id="Seg_3607" s="T303">geben-FUT-1SG</ta>
            <ta e="T305" id="Seg_3608" s="T304">wer.[NOM]</ta>
            <ta e="T306" id="Seg_3609" s="T305">INDEF</ta>
            <ta e="T307" id="Seg_3610" s="T306">2SG-ACC</ta>
            <ta e="T308" id="Seg_3611" s="T307">Verzögerung-POSS</ta>
            <ta e="T309" id="Seg_3612" s="T308">NEG</ta>
            <ta e="T310" id="Seg_3613" s="T309">bringen-FUT.[3SG]</ta>
            <ta e="T311" id="Seg_3614" s="T310">sagen-CVB.SEQ</ta>
            <ta e="T312" id="Seg_3615" s="T311">Brief.[NOM]</ta>
            <ta e="T313" id="Seg_3616" s="T312">geben-PRS.[3SG]</ta>
            <ta e="T314" id="Seg_3617" s="T313">jenes-ACC</ta>
            <ta e="T315" id="Seg_3618" s="T314">nehmen-CVB.SEQ</ta>
            <ta e="T316" id="Seg_3619" s="T315">nachdem</ta>
            <ta e="T317" id="Seg_3620" s="T316">doch</ta>
            <ta e="T318" id="Seg_3621" s="T317">wie.viel</ta>
            <ta e="T319" id="Seg_3622" s="T318">NEG</ta>
            <ta e="T320" id="Seg_3623" s="T319">Jahr.[NOM]</ta>
            <ta e="T321" id="Seg_3624" s="T320">leben-PRS.[3SG]</ta>
            <ta e="T322" id="Seg_3625" s="T321">dieses</ta>
            <ta e="T323" id="Seg_3626" s="T322">leben-CVB.SEQ</ta>
            <ta e="T324" id="Seg_3627" s="T323">nachdem</ta>
            <ta e="T325" id="Seg_3628" s="T324">gehen-CVB.PURP</ta>
            <ta e="T326" id="Seg_3629" s="T325">vorbereiten-PRS.[3SG]</ta>
            <ta e="T327" id="Seg_3630" s="T326">Frau-3SG-ACC</ta>
            <ta e="T328" id="Seg_3631" s="T327">nehmen-CVB.SEQ</ta>
            <ta e="T329" id="Seg_3632" s="T328">dieses-DAT/LOC</ta>
            <ta e="T330" id="Seg_3633" s="T329">klein</ta>
            <ta e="T331" id="Seg_3634" s="T330">Tochter.[NOM]</ta>
            <ta e="T332" id="Seg_3635" s="T331">weinen-CVB.SEQ</ta>
            <ta e="T333" id="Seg_3636" s="T332">gehen-PRS.[3SG]</ta>
            <ta e="T334" id="Seg_3637" s="T333">1SG.[NOM]</ta>
            <ta e="T335" id="Seg_3638" s="T334">finden-PTCP.PST</ta>
            <ta e="T336" id="Seg_3639" s="T335">Mensch-1SG-ABL</ta>
            <ta e="T337" id="Seg_3640" s="T336">bleiben-NEG-1SG</ta>
            <ta e="T338" id="Seg_3641" s="T337">zusammen</ta>
            <ta e="T339" id="Seg_3642" s="T338">mitkommen-FUT-1SG</ta>
            <ta e="T340" id="Seg_3643" s="T339">ältere.Schwester-3SG-DAT/LOC</ta>
            <ta e="T341" id="Seg_3644" s="T340">sprechen-PRS.[3SG]</ta>
            <ta e="T342" id="Seg_3645" s="T341">2SG.[NOM]</ta>
            <ta e="T343" id="Seg_3646" s="T342">sehen-PTCP.PRS</ta>
            <ta e="T344" id="Seg_3647" s="T343">machen-PTCP.FUT-2SG-ACC</ta>
            <ta e="T345" id="Seg_3648" s="T344">zwei</ta>
            <ta e="T346" id="Seg_3649" s="T345">Auge-PROPR</ta>
            <ta e="T347" id="Seg_3650" s="T346">Kind-PROPR-2SG</ta>
            <ta e="T348" id="Seg_3651" s="T347">1SG.[NOM]</ta>
            <ta e="T349" id="Seg_3652" s="T348">mitkommen-FUT-1SG</ta>
            <ta e="T350" id="Seg_3653" s="T349">3SG-ACC</ta>
            <ta e="T351" id="Seg_3654" s="T350">mit</ta>
            <ta e="T352" id="Seg_3655" s="T351">Vater-3SG.[NOM]</ta>
            <ta e="T353" id="Seg_3656" s="T352">raten-CVB.SEQ</ta>
            <ta e="T354" id="Seg_3657" s="T353">Wille-VBZ-PRS.[3SG]</ta>
            <ta e="T355" id="Seg_3658" s="T354">so</ta>
            <ta e="T356" id="Seg_3659" s="T355">klein</ta>
            <ta e="T357" id="Seg_3660" s="T356">Tochter.[NOM]</ta>
            <ta e="T358" id="Seg_3661" s="T357">zusammen</ta>
            <ta e="T359" id="Seg_3662" s="T358">mitkommen-PRS.[3SG]</ta>
            <ta e="T360" id="Seg_3663" s="T359">doch</ta>
            <ta e="T361" id="Seg_3664" s="T360">lange</ta>
            <ta e="T362" id="Seg_3665" s="T361">sehr</ta>
            <ta e="T363" id="Seg_3666" s="T362">reisen-CVB.SEQ</ta>
            <ta e="T364" id="Seg_3667" s="T363">ankommen-PRS-3PL</ta>
            <ta e="T365" id="Seg_3668" s="T364">Land-3PL-DAT/LOC</ta>
            <ta e="T366" id="Seg_3669" s="T365">so</ta>
            <ta e="T367" id="Seg_3670" s="T366">Erzählung.[NOM]</ta>
            <ta e="T368" id="Seg_3671" s="T367">es.gibt</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_3672" s="T0">давно</ta>
            <ta e="T2" id="Seg_3673" s="T1">один</ta>
            <ta e="T3" id="Seg_3674" s="T2">мальчик-PROPR</ta>
            <ta e="T4" id="Seg_3675" s="T3">ненецкий</ta>
            <ta e="T5" id="Seg_3676" s="T4">князь-3SG.[NOM]</ta>
            <ta e="T6" id="Seg_3677" s="T5">жить-PST2.[3SG]</ta>
            <ta e="T7" id="Seg_3678" s="T6">зимой</ta>
            <ta e="T8" id="Seg_3679" s="T7">море-DAT/LOC</ta>
            <ta e="T9" id="Seg_3680" s="T8">ненец-PL.[NOM]</ta>
            <ta e="T10" id="Seg_3681" s="T9">белуха-VBZ-HAB-3PL</ta>
            <ta e="T11" id="Seg_3682" s="T10">этот</ta>
            <ta e="T12" id="Seg_3683" s="T11">князь.[NOM]</ta>
            <ta e="T13" id="Seg_3684" s="T12">море.[NOM]</ta>
            <ta e="T14" id="Seg_3685" s="T13">лед-3SG-INSTR</ta>
            <ta e="T15" id="Seg_3686" s="T14">белуха.[NOM]</ta>
            <ta e="T16" id="Seg_3687" s="T15">прорубь-3SG.[NOM]</ta>
            <ta e="T17" id="Seg_3688" s="T16">прорубить-CVB.SIM</ta>
            <ta e="T18" id="Seg_3689" s="T17">идти-PRS.[3SG]</ta>
            <ta e="T19" id="Seg_3690" s="T18">этот</ta>
            <ta e="T20" id="Seg_3691" s="T19">делать-CVB.SEQ</ta>
            <ta e="T21" id="Seg_3692" s="T20">после</ta>
            <ta e="T22" id="Seg_3693" s="T21">возвращаться-CVB.PURP</ta>
            <ta e="T23" id="Seg_3694" s="T22">хотеть-PST2-3SG</ta>
            <ta e="T24" id="Seg_3695" s="T23">льдина-3SG-ACC</ta>
            <ta e="T25" id="Seg_3696" s="T24">море.[NOM]</ta>
            <ta e="T26" id="Seg_3697" s="T25">разбивать-CVB.SEQ</ta>
            <ta e="T27" id="Seg_3698" s="T26">бросать-PST2.[3SG]</ta>
            <ta e="T28" id="Seg_3699" s="T27">три</ta>
            <ta e="T29" id="Seg_3700" s="T28">день.[NOM]</ta>
            <ta e="T30" id="Seg_3701" s="T29">искать-PRS.[3SG]</ta>
            <ta e="T31" id="Seg_3702" s="T30">выйти-PTCP.PRS</ta>
            <ta e="T32" id="Seg_3703" s="T31">место-3SG-ACC</ta>
            <ta e="T33" id="Seg_3704" s="T32">беда-3SG.[NOM]</ta>
            <ta e="T34" id="Seg_3705" s="T33">быть-CVB.SEQ</ta>
            <ta e="T35" id="Seg_3706" s="T34">три</ta>
            <ta e="T36" id="Seg_3707" s="T35">олень-3SG-ABL</ta>
            <ta e="T37" id="Seg_3708" s="T36">один-3SG-ACC</ta>
            <ta e="T38" id="Seg_3709" s="T37">сырое.мясо-VBZ-CVB.SEQ</ta>
            <ta e="T39" id="Seg_3710" s="T38">есть-PRS.[3SG]</ta>
            <ta e="T40" id="Seg_3711" s="T39">потом</ta>
            <ta e="T41" id="Seg_3712" s="T40">опять</ta>
            <ta e="T42" id="Seg_3713" s="T41">идти-CVB.SIM</ta>
            <ta e="T43" id="Seg_3714" s="T42">не.справиться-CVB.SEQ</ta>
            <ta e="T44" id="Seg_3715" s="T43">два</ta>
            <ta e="T45" id="Seg_3716" s="T44">олень-3SG-ACC</ta>
            <ta e="T46" id="Seg_3717" s="T45">истребить-PRS.[3SG]</ta>
            <ta e="T47" id="Seg_3718" s="T46">убить-PRS.[3SG]</ta>
            <ta e="T48" id="Seg_3719" s="T47">льдина-3SG.[NOM]</ta>
            <ta e="T49" id="Seg_3720" s="T48">шаман.[NOM]</ta>
            <ta e="T50" id="Seg_3721" s="T49">шаманский.бубен-3SG-ACC</ta>
            <ta e="T51" id="Seg_3722" s="T50">ростом.с</ta>
            <ta e="T52" id="Seg_3723" s="T51">становиться-PRS.[3SG]</ta>
            <ta e="T53" id="Seg_3724" s="T52">тот-ACC</ta>
            <ta e="T54" id="Seg_3725" s="T53">шторм.[NOM]</ta>
            <ta e="T55" id="Seg_3726" s="T54">как</ta>
            <ta e="T56" id="Seg_3727" s="T55">INDEF</ta>
            <ta e="T57" id="Seg_3728" s="T56">остров.[NOM]</ta>
            <ta e="T58" id="Seg_3729" s="T57">берег-3SG-DAT/LOC</ta>
            <ta e="T59" id="Seg_3730" s="T58">течь-CVB.SEQ</ta>
            <ta e="T60" id="Seg_3731" s="T59">приходить-PST2.[3SG]</ta>
            <ta e="T61" id="Seg_3732" s="T60">дерево-DAT/LOC</ta>
            <ta e="T62" id="Seg_3733" s="T61">к</ta>
            <ta e="T63" id="Seg_3734" s="T62">бросать-PRS.[3SG]</ta>
            <ta e="T64" id="Seg_3735" s="T63">этот</ta>
            <ta e="T65" id="Seg_3736" s="T64">остров-DAT/LOC</ta>
            <ta e="T66" id="Seg_3737" s="T65">выйти-EP-PST2-3SG</ta>
            <ta e="T67" id="Seg_3738" s="T66">дерево-PROPR.[NOM]</ta>
            <ta e="T68" id="Seg_3739" s="T67">очень</ta>
            <ta e="T69" id="Seg_3740" s="T68">где</ta>
            <ta e="T70" id="Seg_3741" s="T69">INDEF</ta>
            <ta e="T71" id="Seg_3742" s="T70">топор.[NOM]</ta>
            <ta e="T72" id="Seg_3743" s="T71">звук-3SG.[NOM]</ta>
            <ta e="T73" id="Seg_3744" s="T72">слышаться-PRS.[3SG]</ta>
            <ta e="T74" id="Seg_3745" s="T73">сюда</ta>
            <ta e="T75" id="Seg_3746" s="T74">вот</ta>
            <ta e="T76" id="Seg_3747" s="T75">каждый</ta>
            <ta e="T77" id="Seg_3748" s="T76">сила-3SG-ACC</ta>
            <ta e="T78" id="Seg_3749" s="T77">идти-CVB.SEQ</ta>
            <ta e="T79" id="Seg_3750" s="T78">доезжать-PRS.[3SG]</ta>
            <ta e="T80" id="Seg_3751" s="T79">доезжать-CVB.SEQ</ta>
            <ta e="T81" id="Seg_3752" s="T80">видеть-PST2-3SG</ta>
            <ta e="T82" id="Seg_3753" s="T81">один</ta>
            <ta e="T83" id="Seg_3754" s="T82">так.маленький-INTNS</ta>
            <ta e="T84" id="Seg_3755" s="T83">девушка.[NOM]</ta>
            <ta e="T85" id="Seg_3756" s="T84">3SG-DAT/LOC</ta>
            <ta e="T86" id="Seg_3757" s="T85">спина-3SG-INSTR</ta>
            <ta e="T87" id="Seg_3758" s="T86">стоять-CVB.SEQ</ta>
            <ta e="T88" id="Seg_3759" s="T87">дерево.[NOM]</ta>
            <ta e="T89" id="Seg_3760" s="T88">разрезать-CVB.SIM</ta>
            <ta e="T90" id="Seg_3761" s="T89">стоять-PRS.[3SG]</ta>
            <ta e="T91" id="Seg_3762" s="T90">этот</ta>
            <ta e="T92" id="Seg_3763" s="T91">девушка.[NOM]</ta>
            <ta e="T93" id="Seg_3764" s="T92">идти.по.следам-EP-RECP/COLL-PRS.[3SG]</ta>
            <ta e="T94" id="Seg_3765" s="T93">потом</ta>
            <ta e="T95" id="Seg_3766" s="T94">половина.[NOM]</ta>
            <ta e="T96" id="Seg_3767" s="T95">глаз-PROPR.[NOM]</ta>
            <ta e="T97" id="Seg_3768" s="T96">быть-PST2.[3SG]</ta>
            <ta e="T98" id="Seg_3769" s="T97">девушка-3SG.[NOM]</ta>
            <ta e="T99" id="Seg_3770" s="T98">человек-ACC</ta>
            <ta e="T100" id="Seg_3771" s="T99">видеть-CVB.SEQ</ta>
            <ta e="T101" id="Seg_3772" s="T100">после</ta>
            <ta e="T102" id="Seg_3773" s="T101">испугаться-CVB.SEQ</ta>
            <ta e="T103" id="Seg_3774" s="T102">топор-3SG-ACC</ta>
            <ta e="T104" id="Seg_3775" s="T103">бросать-CVB.SEQ</ta>
            <ta e="T105" id="Seg_3776" s="T104">бегать-CVB.SEQ</ta>
            <ta e="T106" id="Seg_3777" s="T105">идти-PRS.[3SG]</ta>
            <ta e="T107" id="Seg_3778" s="T106">бегать-CVB.SEQ</ta>
            <ta e="T108" id="Seg_3779" s="T107">идти-PRS.[3SG]</ta>
            <ta e="T109" id="Seg_3780" s="T108">да</ta>
            <ta e="T110" id="Seg_3781" s="T109">такой-ACC</ta>
            <ta e="T111" id="Seg_3782" s="T110">видеть-PST1-1SG</ta>
            <ta e="T112" id="Seg_3783" s="T111">говорить-CVB.SEQ</ta>
            <ta e="T113" id="Seg_3784" s="T112">люди-3SG-DAT/LOC</ta>
            <ta e="T114" id="Seg_3785" s="T113">рассказывать-PRS.[3SG]</ta>
            <ta e="T115" id="Seg_3786" s="T114">этот-3SG-3PL.[NOM]</ta>
            <ta e="T116" id="Seg_3787" s="T115">сколько</ta>
            <ta e="T117" id="Seg_3788" s="T116">NEG</ta>
            <ta e="T118" id="Seg_3789" s="T117">человек.[NOM]</ta>
            <ta e="T119" id="Seg_3790" s="T118">быть-CVB.SEQ</ta>
            <ta e="T120" id="Seg_3791" s="T119">идти-CVB.SEQ-3PL</ta>
            <ta e="T121" id="Seg_3792" s="T120">устать-CVB.SEQ</ta>
            <ta e="T122" id="Seg_3793" s="T121">голодать-EP-PTCP.PST</ta>
            <ta e="T123" id="Seg_3794" s="T122">человек-3PL-ACC</ta>
            <ta e="T124" id="Seg_3795" s="T123">шкура-DAT/LOC</ta>
            <ta e="T125" id="Seg_3796" s="T124">поднимать-CVB.SEQ</ta>
            <ta e="T126" id="Seg_3797" s="T125">принести-PRS-3PL</ta>
            <ta e="T127" id="Seg_3798" s="T126">тот</ta>
            <ta e="T128" id="Seg_3799" s="T127">принести-PTCP.PST-3PL-DAT/LOC</ta>
            <ta e="T129" id="Seg_3800" s="T128">один</ta>
            <ta e="T130" id="Seg_3801" s="T129">снег.[NOM]</ta>
            <ta e="T131" id="Seg_3802" s="T130">как</ta>
            <ta e="T132" id="Seg_3803" s="T131">волос-PROPR</ta>
            <ta e="T133" id="Seg_3804" s="T132">старик.[NOM]</ta>
            <ta e="T134" id="Seg_3805" s="T133">говорить-PRS.[3SG]</ta>
            <ta e="T135" id="Seg_3806" s="T134">ээ</ta>
            <ta e="T136" id="Seg_3807" s="T135">что-ACC</ta>
            <ta e="T137" id="Seg_3808" s="T136">удивляться-PRS-2PL</ta>
            <ta e="T138" id="Seg_3809" s="T137">сказка-DAT/LOC</ta>
            <ta e="T139" id="Seg_3810" s="T138">да</ta>
            <ta e="T140" id="Seg_3811" s="T139">есть</ta>
            <ta e="T141" id="Seg_3812" s="T140">предок.[NOM]</ta>
            <ta e="T142" id="Seg_3813" s="T141">рассказ-DAT/LOC</ta>
            <ta e="T143" id="Seg_3814" s="T142">письмо-DAT/LOC</ta>
            <ta e="T144" id="Seg_3815" s="T143">да</ta>
            <ta e="T145" id="Seg_3816" s="T144">есть</ta>
            <ta e="T146" id="Seg_3817" s="T145">такой</ta>
            <ta e="T147" id="Seg_3818" s="T146">два</ta>
            <ta e="T148" id="Seg_3819" s="T147">глаз-PROPR</ta>
            <ta e="T149" id="Seg_3820" s="T148">народ.[NOM]</ta>
            <ta e="T150" id="Seg_3821" s="T149">есть</ta>
            <ta e="T151" id="Seg_3822" s="T150">говорить-CVB.SEQ</ta>
            <ta e="T152" id="Seg_3823" s="T151">вот</ta>
            <ta e="T153" id="Seg_3824" s="T152">голодать-PTCP.PST</ta>
            <ta e="T154" id="Seg_3825" s="T153">человек.[NOM]</ta>
            <ta e="T155" id="Seg_3826" s="T154">обычай-3SG-INSTR</ta>
            <ta e="T156" id="Seg_3827" s="T155">слишком</ta>
            <ta e="T157" id="Seg_3828" s="T156">есть-EP-CAUS-EP-NEG.CVB</ta>
            <ta e="T158" id="Seg_3829" s="T157">есть-EP-CAUS-CVB.SEQ</ta>
            <ta e="T159" id="Seg_3830" s="T158">идти-EP-IMP.2PL</ta>
            <ta e="T160" id="Seg_3831" s="T159">нуждаться-CVB.SEQ</ta>
            <ta e="T161" id="Seg_3832" s="T160">сам-3SG-GEN</ta>
            <ta e="T162" id="Seg_3833" s="T161">земля-3SG-ABL</ta>
            <ta e="T163" id="Seg_3834" s="T162">этот-INSTR</ta>
            <ta e="T164" id="Seg_3835" s="T163">приходить-PST2.[3SG]</ta>
            <ta e="T165" id="Seg_3836" s="T164">MOD</ta>
            <ta e="T166" id="Seg_3837" s="T165">наверное</ta>
            <ta e="T167" id="Seg_3838" s="T166">говорить-PRS.[3SG]</ta>
            <ta e="T168" id="Seg_3839" s="T167">этот.[NOM]</ta>
            <ta e="T169" id="Seg_3840" s="T168">богатый</ta>
            <ta e="T170" id="Seg_3841" s="T169">очень</ta>
            <ta e="T171" id="Seg_3842" s="T170">народ.[NOM]</ta>
            <ta e="T172" id="Seg_3843" s="T171">быть-PST2.[3SG]</ta>
            <ta e="T173" id="Seg_3844" s="T172">лиса-3PL.[NOM]</ta>
            <ta e="T174" id="Seg_3845" s="T173">песец-3PL.[NOM]</ta>
            <ta e="T175" id="Seg_3846" s="T174">дом-3PL-GEN</ta>
            <ta e="T176" id="Seg_3847" s="T175">дверь-3SG-DAT/LOC</ta>
            <ta e="T177" id="Seg_3848" s="T176">сам-3SG.[NOM]</ta>
            <ta e="T178" id="Seg_3849" s="T177">приходить-PTCP.PRS</ta>
            <ta e="T179" id="Seg_3850" s="T178">быть-PST2.[3SG]</ta>
            <ta e="T180" id="Seg_3851" s="T179">этот</ta>
            <ta e="T181" id="Seg_3852" s="T180">человек.[NOM]</ta>
            <ta e="T182" id="Seg_3853" s="T181">здесь</ta>
            <ta e="T183" id="Seg_3854" s="T182">жизнь-VBZ-PRS.[3SG]</ta>
            <ta e="T184" id="Seg_3855" s="T183">жизнь-VBZ-PTCP.PST</ta>
            <ta e="T185" id="Seg_3856" s="T184">семья-3SG.[NOM]</ta>
            <ta e="T186" id="Seg_3857" s="T185">два</ta>
            <ta e="T187" id="Seg_3858" s="T186">дочь-PROPR.[NOM]</ta>
            <ta e="T188" id="Seg_3859" s="T187">быть-PST2-3PL</ta>
            <ta e="T189" id="Seg_3860" s="T188">этот-ABL</ta>
            <ta e="T190" id="Seg_3861" s="T189">старый-3SG-ACC</ta>
            <ta e="T191" id="Seg_3862" s="T190">отец-3SG.[NOM]</ta>
            <ta e="T192" id="Seg_3863" s="T191">жена.[NOM]</ta>
            <ta e="T193" id="Seg_3864" s="T192">давать-PRS.[3SG]</ta>
            <ta e="T194" id="Seg_3865" s="T193">мальчик-DAT/LOC</ta>
            <ta e="T195" id="Seg_3866" s="T194">этот-DAT/LOC</ta>
            <ta e="T196" id="Seg_3867" s="T195">этот</ta>
            <ta e="T197" id="Seg_3868" s="T196">видеть-PTCP.HAB</ta>
            <ta e="T198" id="Seg_3869" s="T197">девушка.[NOM]</ta>
            <ta e="T199" id="Seg_3870" s="T198">старшая.сестра-3SG-DAT/LOC</ta>
            <ta e="T200" id="Seg_3871" s="T199">завидовать-CVB.SEQ</ta>
            <ta e="T201" id="Seg_3872" s="T200">1SG.[NOM]</ta>
            <ta e="T202" id="Seg_3873" s="T201">идти-FUT-NEC.[3SG]</ta>
            <ta e="T203" id="Seg_3874" s="T202">сам-1SG.[NOM]</ta>
            <ta e="T204" id="Seg_3875" s="T203">найти-NMNZ-1SG-DAT/LOC</ta>
            <ta e="T205" id="Seg_3876" s="T204">говорить-CVB.SEQ</ta>
            <ta e="T206" id="Seg_3877" s="T205">плакать-PRS.[3SG]</ta>
            <ta e="T207" id="Seg_3878" s="T206">тот-ACC</ta>
            <ta e="T208" id="Seg_3879" s="T207">слышать-EP-NEG.CVB</ta>
            <ta e="T209" id="Seg_3880" s="T208">ведь</ta>
            <ta e="T210" id="Seg_3881" s="T209">старый</ta>
            <ta e="T211" id="Seg_3882" s="T210">дочь-ACC</ta>
            <ta e="T212" id="Seg_3883" s="T211">давать-PRS-3PL</ta>
            <ta e="T213" id="Seg_3884" s="T212">мальчик-DAT/LOC</ta>
            <ta e="T214" id="Seg_3885" s="T213">один</ta>
            <ta e="T215" id="Seg_3886" s="T214">год-EP-INSTR</ta>
            <ta e="T216" id="Seg_3887" s="T215">два</ta>
            <ta e="T217" id="Seg_3888" s="T216">глаз-PROPR</ta>
            <ta e="T218" id="Seg_3889" s="T217">мальчик.[NOM]</ta>
            <ta e="T219" id="Seg_3890" s="T218">ребенок.[NOM]</ta>
            <ta e="T220" id="Seg_3891" s="T219">родиться-PRS.[3SG]</ta>
            <ta e="T221" id="Seg_3892" s="T220">вот</ta>
            <ta e="T222" id="Seg_3893" s="T221">долго</ta>
            <ta e="T223" id="Seg_3894" s="T222">очень</ta>
            <ta e="T224" id="Seg_3895" s="T223">жить-PRS-3PL</ta>
            <ta e="T225" id="Seg_3896" s="T224">так</ta>
            <ta e="T226" id="Seg_3897" s="T225">этот</ta>
            <ta e="T227" id="Seg_3898" s="T226">половина.[NOM]</ta>
            <ta e="T228" id="Seg_3899" s="T227">глаз-PROPR-PL.[NOM]</ta>
            <ta e="T229" id="Seg_3900" s="T228">много</ta>
            <ta e="T230" id="Seg_3901" s="T229">очень</ta>
            <ta e="T231" id="Seg_3902" s="T230">народ.[NOM]</ta>
            <ta e="T232" id="Seg_3903" s="T231">быть-PST2-3PL</ta>
            <ta e="T233" id="Seg_3904" s="T232">однажды</ta>
            <ta e="T234" id="Seg_3905" s="T233">корабль-3SG-INSTR</ta>
            <ta e="T235" id="Seg_3906" s="T234">царь-3PL.[NOM]</ta>
            <ta e="T236" id="Seg_3907" s="T235">подать.[NOM]</ta>
            <ta e="T237" id="Seg_3908" s="T236">собирать-CVB.SIM</ta>
            <ta e="T238" id="Seg_3909" s="T237">приходить-PRS.[3SG]</ta>
            <ta e="T239" id="Seg_3910" s="T238">3SG-ABL</ta>
            <ta e="T240" id="Seg_3911" s="T239">человек.[NOM]</ta>
            <ta e="T241" id="Seg_3912" s="T240">приходить-PRS.[3SG]</ta>
            <ta e="T242" id="Seg_3913" s="T241">этот</ta>
            <ta e="T243" id="Seg_3914" s="T242">князь.[NOM]</ta>
            <ta e="T244" id="Seg_3915" s="T243">тесть-3SG-DAT/LOC</ta>
            <ta e="T245" id="Seg_3916" s="T244">2SG.[NOM]</ta>
            <ta e="T246" id="Seg_3917" s="T245">два</ta>
            <ta e="T247" id="Seg_3918" s="T246">глаз-PROPR</ta>
            <ta e="T248" id="Seg_3919" s="T247">зять-PROPR.[NOM]</ta>
            <ta e="T249" id="Seg_3920" s="T248">говорят-2SG</ta>
            <ta e="T250" id="Seg_3921" s="T249">2SG.[NOM]</ta>
            <ta e="T251" id="Seg_3922" s="T250">тот-3SG-2SG-ACC</ta>
            <ta e="T252" id="Seg_3923" s="T251">царь.[NOM]</ta>
            <ta e="T253" id="Seg_3924" s="T252">видеть-CVB.PURP</ta>
            <ta e="T254" id="Seg_3925" s="T253">хотеть-PRS.[3SG]</ta>
            <ta e="T255" id="Seg_3926" s="T254">говорить-PRS.[3SG]</ta>
            <ta e="T256" id="Seg_3927" s="T255">тесть-3SG.[NOM]</ta>
            <ta e="T257" id="Seg_3928" s="T256">говорить-PRS.[3SG]</ta>
            <ta e="T258" id="Seg_3929" s="T257">лебедь-1SG.[NOM]</ta>
            <ta e="T259" id="Seg_3930" s="T258">идти.[IMP.2SG]</ta>
            <ta e="T260" id="Seg_3931" s="T259">царь.[NOM]</ta>
            <ta e="T261" id="Seg_3932" s="T260">видеть-FUT.[3SG]</ta>
            <ta e="T262" id="Seg_3933" s="T261">только</ta>
            <ta e="T263" id="Seg_3934" s="T262">EMPH</ta>
            <ta e="T264" id="Seg_3935" s="T263">2SG.[NOM]</ta>
            <ta e="T265" id="Seg_3936" s="T264">идти.[IMP.2SG]</ta>
            <ta e="T266" id="Seg_3937" s="T265">идти-NEG-TEMP-2SG</ta>
            <ta e="T267" id="Seg_3938" s="T266">голова-1PL-ACC</ta>
            <ta e="T268" id="Seg_3939" s="T267">резать-FUT.[3SG]</ta>
            <ta e="T269" id="Seg_3940" s="T268">князь.[NOM]</ta>
            <ta e="T270" id="Seg_3941" s="T269">царь-DAT/LOC</ta>
            <ta e="T271" id="Seg_3942" s="T270">идти-PRS.[3SG]</ta>
            <ta e="T272" id="Seg_3943" s="T271">тот-3SG-3SG.[NOM]</ta>
            <ta e="T273" id="Seg_3944" s="T272">говорить-PRS.[3SG]</ta>
            <ta e="T274" id="Seg_3945" s="T273">1SG.[NOM]</ta>
            <ta e="T275" id="Seg_3946" s="T274">2SG-ACC</ta>
            <ta e="T276" id="Seg_3947" s="T275">здесь</ta>
            <ta e="T277" id="Seg_3948" s="T276">богатый</ta>
            <ta e="T278" id="Seg_3949" s="T277">человек.[NOM]</ta>
            <ta e="T279" id="Seg_3950" s="T278">делать-CVB.SEQ</ta>
            <ta e="T280" id="Seg_3951" s="T279">готовиться-FUT-1SG</ta>
            <ta e="T281" id="Seg_3952" s="T280">2SG.[NOM]</ta>
            <ta e="T282" id="Seg_3953" s="T281">здесь</ta>
            <ta e="T283" id="Seg_3954" s="T282">оставаться-FUT-2SG</ta>
            <ta e="T284" id="Seg_3955" s="T283">Q</ta>
            <ta e="T285" id="Seg_3956" s="T284">страна-2SG.[NOM]</ta>
            <ta e="T286" id="Seg_3957" s="T285">к</ta>
            <ta e="T287" id="Seg_3958" s="T286">возвращаться-FUT-2SG</ta>
            <ta e="T288" id="Seg_3959" s="T287">Q</ta>
            <ta e="T289" id="Seg_3960" s="T288">тот-ACC</ta>
            <ta e="T290" id="Seg_3961" s="T289">возвращаться-FUT-1SG</ta>
            <ta e="T291" id="Seg_3962" s="T290">говорить-PST1-3SG</ta>
            <ta e="T292" id="Seg_3963" s="T291">царь.[NOM]</ta>
            <ta e="T293" id="Seg_3964" s="T292">тот-DAT/LOC</ta>
            <ta e="T294" id="Seg_3965" s="T293">говорить-PST1-3SG</ta>
            <ta e="T295" id="Seg_3966" s="T294">далекий.[NOM]</ta>
            <ta e="T296" id="Seg_3967" s="T295">земля.[NOM]</ta>
            <ta e="T297" id="Seg_3968" s="T296">вода.[NOM]</ta>
            <ta e="T298" id="Seg_3969" s="T297">принести-PTCP.PST-3SG-ACC</ta>
            <ta e="T299" id="Seg_3970" s="T298">как</ta>
            <ta e="T300" id="Seg_3971" s="T299">идти-FUT-2SG</ta>
            <ta e="T301" id="Seg_3972" s="T300">NEG-3SG</ta>
            <ta e="T302" id="Seg_3973" s="T301">1SG.[NOM]</ta>
            <ta e="T303" id="Seg_3974" s="T302">письмо.[NOM]</ta>
            <ta e="T304" id="Seg_3975" s="T303">давать-FUT-1SG</ta>
            <ta e="T305" id="Seg_3976" s="T304">кто.[NOM]</ta>
            <ta e="T306" id="Seg_3977" s="T305">INDEF</ta>
            <ta e="T307" id="Seg_3978" s="T306">2SG-ACC</ta>
            <ta e="T308" id="Seg_3979" s="T307">задержка-POSS</ta>
            <ta e="T309" id="Seg_3980" s="T308">NEG</ta>
            <ta e="T310" id="Seg_3981" s="T309">приносить-FUT.[3SG]</ta>
            <ta e="T311" id="Seg_3982" s="T310">говорить-CVB.SEQ</ta>
            <ta e="T312" id="Seg_3983" s="T311">письмо.[NOM]</ta>
            <ta e="T313" id="Seg_3984" s="T312">давать-PRS.[3SG]</ta>
            <ta e="T314" id="Seg_3985" s="T313">тот-ACC</ta>
            <ta e="T315" id="Seg_3986" s="T314">взять-CVB.SEQ</ta>
            <ta e="T316" id="Seg_3987" s="T315">после</ta>
            <ta e="T317" id="Seg_3988" s="T316">ведь</ta>
            <ta e="T318" id="Seg_3989" s="T317">сколько</ta>
            <ta e="T319" id="Seg_3990" s="T318">NEG</ta>
            <ta e="T320" id="Seg_3991" s="T319">год.[NOM]</ta>
            <ta e="T321" id="Seg_3992" s="T320">жить-PRS.[3SG]</ta>
            <ta e="T322" id="Seg_3993" s="T321">этот</ta>
            <ta e="T323" id="Seg_3994" s="T322">жить-CVB.SEQ</ta>
            <ta e="T324" id="Seg_3995" s="T323">после</ta>
            <ta e="T325" id="Seg_3996" s="T324">идти-CVB.PURP</ta>
            <ta e="T326" id="Seg_3997" s="T325">готовить-PRS.[3SG]</ta>
            <ta e="T327" id="Seg_3998" s="T326">жена-3SG-ACC</ta>
            <ta e="T328" id="Seg_3999" s="T327">взять-CVB.SEQ</ta>
            <ta e="T329" id="Seg_4000" s="T328">этот-DAT/LOC</ta>
            <ta e="T330" id="Seg_4001" s="T329">маленький</ta>
            <ta e="T331" id="Seg_4002" s="T330">дочь.[NOM]</ta>
            <ta e="T332" id="Seg_4003" s="T331">плакать-CVB.SEQ</ta>
            <ta e="T333" id="Seg_4004" s="T332">идти-PRS.[3SG]</ta>
            <ta e="T334" id="Seg_4005" s="T333">1SG.[NOM]</ta>
            <ta e="T335" id="Seg_4006" s="T334">найти-PTCP.PST</ta>
            <ta e="T336" id="Seg_4007" s="T335">человек-1SG-ABL</ta>
            <ta e="T337" id="Seg_4008" s="T336">оставаться-NEG-1SG</ta>
            <ta e="T338" id="Seg_4009" s="T337">вместе</ta>
            <ta e="T339" id="Seg_4010" s="T338">сопровождать-FUT-1SG</ta>
            <ta e="T340" id="Seg_4011" s="T339">старшая.сестра-3SG-DAT/LOC</ta>
            <ta e="T341" id="Seg_4012" s="T340">говорить-PRS.[3SG]</ta>
            <ta e="T342" id="Seg_4013" s="T341">2SG.[NOM]</ta>
            <ta e="T343" id="Seg_4014" s="T342">видеть-PTCP.PRS</ta>
            <ta e="T344" id="Seg_4015" s="T343">делать-PTCP.FUT-2SG-ACC</ta>
            <ta e="T345" id="Seg_4016" s="T344">два</ta>
            <ta e="T346" id="Seg_4017" s="T345">глаз-PROPR</ta>
            <ta e="T347" id="Seg_4018" s="T346">ребенок-PROPR-2SG</ta>
            <ta e="T348" id="Seg_4019" s="T347">1SG.[NOM]</ta>
            <ta e="T349" id="Seg_4020" s="T348">сопровождать-FUT-1SG</ta>
            <ta e="T350" id="Seg_4021" s="T349">3SG-ACC</ta>
            <ta e="T351" id="Seg_4022" s="T350">с</ta>
            <ta e="T352" id="Seg_4023" s="T351">отец-3SG.[NOM]</ta>
            <ta e="T353" id="Seg_4024" s="T352">советовать-CVB.SEQ</ta>
            <ta e="T354" id="Seg_4025" s="T353">воля-VBZ-PRS.[3SG]</ta>
            <ta e="T355" id="Seg_4026" s="T354">так</ta>
            <ta e="T356" id="Seg_4027" s="T355">маленький</ta>
            <ta e="T357" id="Seg_4028" s="T356">дочь.[NOM]</ta>
            <ta e="T358" id="Seg_4029" s="T357">вместе</ta>
            <ta e="T359" id="Seg_4030" s="T358">сопровождать-PRS.[3SG]</ta>
            <ta e="T360" id="Seg_4031" s="T359">вот</ta>
            <ta e="T361" id="Seg_4032" s="T360">долго</ta>
            <ta e="T362" id="Seg_4033" s="T361">очень</ta>
            <ta e="T363" id="Seg_4034" s="T362">путешествовать-CVB.SEQ</ta>
            <ta e="T364" id="Seg_4035" s="T363">доезжать-PRS-3PL</ta>
            <ta e="T365" id="Seg_4036" s="T364">страна-3PL-DAT/LOC</ta>
            <ta e="T366" id="Seg_4037" s="T365">так</ta>
            <ta e="T367" id="Seg_4038" s="T366">рассказ.[NOM]</ta>
            <ta e="T368" id="Seg_4039" s="T367">есть</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_4040" s="T0">adv</ta>
            <ta e="T2" id="Seg_4041" s="T1">cardnum</ta>
            <ta e="T3" id="Seg_4042" s="T2">n-n&gt;adj</ta>
            <ta e="T4" id="Seg_4043" s="T3">adj</ta>
            <ta e="T5" id="Seg_4044" s="T4">n-n:(poss)-n:case</ta>
            <ta e="T6" id="Seg_4045" s="T5">v-v:tense-v:pred.pn</ta>
            <ta e="T7" id="Seg_4046" s="T6">adv</ta>
            <ta e="T8" id="Seg_4047" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_4048" s="T8">n-n:(num)-n:case</ta>
            <ta e="T10" id="Seg_4049" s="T9">n-n&gt;v-v:mood-v:pred.pn</ta>
            <ta e="T11" id="Seg_4050" s="T10">dempro</ta>
            <ta e="T12" id="Seg_4051" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_4052" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_4053" s="T13">n-n:poss-n:case</ta>
            <ta e="T15" id="Seg_4054" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_4055" s="T15">n-n:(poss)-n:case</ta>
            <ta e="T17" id="Seg_4056" s="T16">v-v:cvb</ta>
            <ta e="T18" id="Seg_4057" s="T17">v-v:tense-v:pred.pn</ta>
            <ta e="T19" id="Seg_4058" s="T18">dempro</ta>
            <ta e="T20" id="Seg_4059" s="T19">v-v:cvb</ta>
            <ta e="T21" id="Seg_4060" s="T20">post</ta>
            <ta e="T22" id="Seg_4061" s="T21">v-v:cvb</ta>
            <ta e="T23" id="Seg_4062" s="T22">v-v:tense-v:poss.pn</ta>
            <ta e="T24" id="Seg_4063" s="T23">n-n:poss-n:case</ta>
            <ta e="T25" id="Seg_4064" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_4065" s="T25">v-v:cvb</ta>
            <ta e="T27" id="Seg_4066" s="T26">v-v:tense-v:pred.pn</ta>
            <ta e="T28" id="Seg_4067" s="T27">cardnum</ta>
            <ta e="T29" id="Seg_4068" s="T28">n-n:case</ta>
            <ta e="T30" id="Seg_4069" s="T29">v-v:tense-v:pred.pn</ta>
            <ta e="T31" id="Seg_4070" s="T30">v-v:ptcp</ta>
            <ta e="T32" id="Seg_4071" s="T31">n-n:poss-n:case</ta>
            <ta e="T33" id="Seg_4072" s="T32">n-n:(poss)-n:case</ta>
            <ta e="T34" id="Seg_4073" s="T33">v-v:cvb</ta>
            <ta e="T35" id="Seg_4074" s="T34">cardnum</ta>
            <ta e="T36" id="Seg_4075" s="T35">n-n:poss-n:case</ta>
            <ta e="T37" id="Seg_4076" s="T36">cardnum-n:poss-n:case</ta>
            <ta e="T38" id="Seg_4077" s="T37">n-n&gt;v-v:cvb</ta>
            <ta e="T39" id="Seg_4078" s="T38">v-v:tense-v:pred.pn</ta>
            <ta e="T40" id="Seg_4079" s="T39">adv</ta>
            <ta e="T41" id="Seg_4080" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_4081" s="T41">v-v:cvb</ta>
            <ta e="T43" id="Seg_4082" s="T42">v-v:cvb</ta>
            <ta e="T44" id="Seg_4083" s="T43">cardnum</ta>
            <ta e="T45" id="Seg_4084" s="T44">n-n:poss-n:case</ta>
            <ta e="T46" id="Seg_4085" s="T45">v-v:tense-v:pred.pn</ta>
            <ta e="T47" id="Seg_4086" s="T46">v-v:tense-v:pred.pn</ta>
            <ta e="T48" id="Seg_4087" s="T47">n-n:(poss)-n:case</ta>
            <ta e="T49" id="Seg_4088" s="T48">n-n:case</ta>
            <ta e="T50" id="Seg_4089" s="T49">n-n:poss-n:case</ta>
            <ta e="T51" id="Seg_4090" s="T50">post</ta>
            <ta e="T52" id="Seg_4091" s="T51">v-v:tense-v:pred.pn</ta>
            <ta e="T53" id="Seg_4092" s="T52">dempro-pro:case</ta>
            <ta e="T54" id="Seg_4093" s="T53">n-n:case</ta>
            <ta e="T55" id="Seg_4094" s="T54">que</ta>
            <ta e="T56" id="Seg_4095" s="T55">ptcl</ta>
            <ta e="T57" id="Seg_4096" s="T56">n-n:case</ta>
            <ta e="T58" id="Seg_4097" s="T57">n-n:poss-n:case</ta>
            <ta e="T59" id="Seg_4098" s="T58">v-v:cvb</ta>
            <ta e="T60" id="Seg_4099" s="T59">v-v:tense-v:pred.pn</ta>
            <ta e="T61" id="Seg_4100" s="T60">n-n:case</ta>
            <ta e="T62" id="Seg_4101" s="T61">post</ta>
            <ta e="T63" id="Seg_4102" s="T62">v-v:tense-v:pred.pn</ta>
            <ta e="T64" id="Seg_4103" s="T63">dempro</ta>
            <ta e="T65" id="Seg_4104" s="T64">n-n:case</ta>
            <ta e="T66" id="Seg_4105" s="T65">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T67" id="Seg_4106" s="T66">n-n&gt;adj-n:case</ta>
            <ta e="T68" id="Seg_4107" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_4108" s="T68">que</ta>
            <ta e="T70" id="Seg_4109" s="T69">ptcl</ta>
            <ta e="T71" id="Seg_4110" s="T70">n-n:case</ta>
            <ta e="T72" id="Seg_4111" s="T71">n-n:(poss)-n:case</ta>
            <ta e="T73" id="Seg_4112" s="T72">v-v:tense-v:pred.pn</ta>
            <ta e="T74" id="Seg_4113" s="T73">adv</ta>
            <ta e="T75" id="Seg_4114" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_4115" s="T75">adj</ta>
            <ta e="T77" id="Seg_4116" s="T76">n-n:poss-n:case</ta>
            <ta e="T78" id="Seg_4117" s="T77">v-v:cvb</ta>
            <ta e="T79" id="Seg_4118" s="T78">v-v:tense-v:pred.pn</ta>
            <ta e="T80" id="Seg_4119" s="T79">v-v:cvb</ta>
            <ta e="T81" id="Seg_4120" s="T80">v-v:tense-v:poss.pn</ta>
            <ta e="T82" id="Seg_4121" s="T81">cardnum</ta>
            <ta e="T83" id="Seg_4122" s="T82">adj-adj&gt;adj</ta>
            <ta e="T84" id="Seg_4123" s="T83">n-n:case</ta>
            <ta e="T85" id="Seg_4124" s="T84">pers-pro:case</ta>
            <ta e="T86" id="Seg_4125" s="T85">n-n:poss-n:case</ta>
            <ta e="T87" id="Seg_4126" s="T86">v-v:cvb</ta>
            <ta e="T88" id="Seg_4127" s="T87">n-n:case</ta>
            <ta e="T89" id="Seg_4128" s="T88">v-v:cvb</ta>
            <ta e="T90" id="Seg_4129" s="T89">v-v:tense-v:pred.pn</ta>
            <ta e="T91" id="Seg_4130" s="T90">dempro</ta>
            <ta e="T92" id="Seg_4131" s="T91">n-n:case</ta>
            <ta e="T93" id="Seg_4132" s="T92">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T94" id="Seg_4133" s="T93">adv</ta>
            <ta e="T95" id="Seg_4134" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_4135" s="T95">n-n&gt;adj-n:case</ta>
            <ta e="T97" id="Seg_4136" s="T96">v-v:tense-v:pred.pn</ta>
            <ta e="T98" id="Seg_4137" s="T97">n-n:(poss)-n:case</ta>
            <ta e="T99" id="Seg_4138" s="T98">n-n:case</ta>
            <ta e="T100" id="Seg_4139" s="T99">v-v:cvb</ta>
            <ta e="T101" id="Seg_4140" s="T100">post</ta>
            <ta e="T102" id="Seg_4141" s="T101">v-v:cvb</ta>
            <ta e="T103" id="Seg_4142" s="T102">n-n:poss-n:case</ta>
            <ta e="T104" id="Seg_4143" s="T103">v-v:cvb</ta>
            <ta e="T105" id="Seg_4144" s="T104">v-v:cvb</ta>
            <ta e="T106" id="Seg_4145" s="T105">v-v:tense-v:pred.pn</ta>
            <ta e="T107" id="Seg_4146" s="T106">v-v:cvb</ta>
            <ta e="T108" id="Seg_4147" s="T107">v-v:tense-v:pred.pn</ta>
            <ta e="T109" id="Seg_4148" s="T108">conj</ta>
            <ta e="T110" id="Seg_4149" s="T109">dempro-pro:case</ta>
            <ta e="T111" id="Seg_4150" s="T110">v-v:tense-v:poss.pn</ta>
            <ta e="T112" id="Seg_4151" s="T111">v-v:cvb</ta>
            <ta e="T113" id="Seg_4152" s="T112">n-n:poss-n:case</ta>
            <ta e="T114" id="Seg_4153" s="T113">v-v:tense-v:pred.pn</ta>
            <ta e="T115" id="Seg_4154" s="T114">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T116" id="Seg_4155" s="T115">que</ta>
            <ta e="T117" id="Seg_4156" s="T116">ptcl</ta>
            <ta e="T118" id="Seg_4157" s="T117">n-n:case</ta>
            <ta e="T119" id="Seg_4158" s="T118">v-v:cvb</ta>
            <ta e="T120" id="Seg_4159" s="T119">v-v:cvb-v:pred.pn</ta>
            <ta e="T121" id="Seg_4160" s="T120">v-v:cvb</ta>
            <ta e="T122" id="Seg_4161" s="T121">v-v:(ins)-v:ptcp</ta>
            <ta e="T123" id="Seg_4162" s="T122">n-n:poss-n:case</ta>
            <ta e="T124" id="Seg_4163" s="T123">n-n:case</ta>
            <ta e="T125" id="Seg_4164" s="T124">v-v:cvb</ta>
            <ta e="T126" id="Seg_4165" s="T125">v-v:tense-v:pred.pn</ta>
            <ta e="T127" id="Seg_4166" s="T126">dempro</ta>
            <ta e="T128" id="Seg_4167" s="T127">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T129" id="Seg_4168" s="T128">cardnum</ta>
            <ta e="T130" id="Seg_4169" s="T129">n-n:case</ta>
            <ta e="T131" id="Seg_4170" s="T130">post</ta>
            <ta e="T132" id="Seg_4171" s="T131">n-n&gt;adj</ta>
            <ta e="T133" id="Seg_4172" s="T132">n-n:case</ta>
            <ta e="T134" id="Seg_4173" s="T133">v-v:tense-v:pred.pn</ta>
            <ta e="T135" id="Seg_4174" s="T134">interj</ta>
            <ta e="T136" id="Seg_4175" s="T135">que-pro:case</ta>
            <ta e="T137" id="Seg_4176" s="T136">v-v:tense-v:pred.pn</ta>
            <ta e="T138" id="Seg_4177" s="T137">n-n:case</ta>
            <ta e="T139" id="Seg_4178" s="T138">conj</ta>
            <ta e="T140" id="Seg_4179" s="T139">ptcl</ta>
            <ta e="T141" id="Seg_4180" s="T140">n-n:case</ta>
            <ta e="T142" id="Seg_4181" s="T141">n-n:case</ta>
            <ta e="T143" id="Seg_4182" s="T142">n-n:case</ta>
            <ta e="T144" id="Seg_4183" s="T143">conj</ta>
            <ta e="T145" id="Seg_4184" s="T144">ptcl</ta>
            <ta e="T146" id="Seg_4185" s="T145">dempro</ta>
            <ta e="T147" id="Seg_4186" s="T146">cardnum</ta>
            <ta e="T148" id="Seg_4187" s="T147">n-n&gt;adj</ta>
            <ta e="T149" id="Seg_4188" s="T148">n-n:case</ta>
            <ta e="T150" id="Seg_4189" s="T149">ptcl</ta>
            <ta e="T151" id="Seg_4190" s="T150">v-v:cvb</ta>
            <ta e="T152" id="Seg_4191" s="T151">ptcl</ta>
            <ta e="T153" id="Seg_4192" s="T152">v-v:ptcp</ta>
            <ta e="T154" id="Seg_4193" s="T153">n-n:case</ta>
            <ta e="T155" id="Seg_4194" s="T154">n-n:poss-n:case</ta>
            <ta e="T156" id="Seg_4195" s="T155">adv</ta>
            <ta e="T157" id="Seg_4196" s="T156">v-v:(ins)-v&gt;v-v:(ins)-v:cvb</ta>
            <ta e="T158" id="Seg_4197" s="T157">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T159" id="Seg_4198" s="T158">v-v:(ins)-v:mood.pn</ta>
            <ta e="T160" id="Seg_4199" s="T159">v-v:cvb</ta>
            <ta e="T161" id="Seg_4200" s="T160">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T162" id="Seg_4201" s="T161">n-n:poss-n:case</ta>
            <ta e="T163" id="Seg_4202" s="T162">dempro-pro:case</ta>
            <ta e="T164" id="Seg_4203" s="T163">v-v:tense-v:pred.pn</ta>
            <ta e="T165" id="Seg_4204" s="T164">ptcl</ta>
            <ta e="T166" id="Seg_4205" s="T165">adv</ta>
            <ta e="T167" id="Seg_4206" s="T166">v-v:tense-v:pred.pn</ta>
            <ta e="T168" id="Seg_4207" s="T167">dempro-pro:case</ta>
            <ta e="T169" id="Seg_4208" s="T168">adj</ta>
            <ta e="T170" id="Seg_4209" s="T169">ptcl</ta>
            <ta e="T171" id="Seg_4210" s="T170">n-n:case</ta>
            <ta e="T172" id="Seg_4211" s="T171">v-v:tense-v:pred.pn</ta>
            <ta e="T173" id="Seg_4212" s="T172">n-n:(poss)-n:case</ta>
            <ta e="T174" id="Seg_4213" s="T173">n-n:(poss)-n:case</ta>
            <ta e="T175" id="Seg_4214" s="T174">n-n:poss-n:case</ta>
            <ta e="T176" id="Seg_4215" s="T175">n-n:poss-n:case</ta>
            <ta e="T177" id="Seg_4216" s="T176">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T178" id="Seg_4217" s="T177">v-v:ptcp</ta>
            <ta e="T179" id="Seg_4218" s="T178">v-v:tense-v:pred.pn</ta>
            <ta e="T180" id="Seg_4219" s="T179">dempro</ta>
            <ta e="T181" id="Seg_4220" s="T180">n-n:case</ta>
            <ta e="T182" id="Seg_4221" s="T181">adv</ta>
            <ta e="T183" id="Seg_4222" s="T182">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T184" id="Seg_4223" s="T183">n-n&gt;v-v:ptcp</ta>
            <ta e="T185" id="Seg_4224" s="T184">n-n:(poss)-n:case</ta>
            <ta e="T186" id="Seg_4225" s="T185">cardnum</ta>
            <ta e="T187" id="Seg_4226" s="T186">n-n&gt;adj-n:case</ta>
            <ta e="T188" id="Seg_4227" s="T187">v-v:tense-v:pred.pn</ta>
            <ta e="T189" id="Seg_4228" s="T188">dempro-pro:case</ta>
            <ta e="T190" id="Seg_4229" s="T189">adj-n:poss-n:case</ta>
            <ta e="T191" id="Seg_4230" s="T190">n-n:(poss)-n:case</ta>
            <ta e="T192" id="Seg_4231" s="T191">n-n:case</ta>
            <ta e="T193" id="Seg_4232" s="T192">v-v:tense-v:pred.pn</ta>
            <ta e="T194" id="Seg_4233" s="T193">n-n:case</ta>
            <ta e="T195" id="Seg_4234" s="T194">dempro-pro:case</ta>
            <ta e="T196" id="Seg_4235" s="T195">dempro</ta>
            <ta e="T197" id="Seg_4236" s="T196">v-v:ptcp</ta>
            <ta e="T198" id="Seg_4237" s="T197">n-n:case</ta>
            <ta e="T199" id="Seg_4238" s="T198">n-n:poss-n:case</ta>
            <ta e="T200" id="Seg_4239" s="T199">v-v:cvb</ta>
            <ta e="T201" id="Seg_4240" s="T200">pers-pro:case</ta>
            <ta e="T202" id="Seg_4241" s="T201">v-v:tense-v:mood-v:pred.pn</ta>
            <ta e="T203" id="Seg_4242" s="T202">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T204" id="Seg_4243" s="T203">v-v&gt;n-n:poss-n:case</ta>
            <ta e="T205" id="Seg_4244" s="T204">v-v:cvb</ta>
            <ta e="T206" id="Seg_4245" s="T205">v-v:tense-v:pred.pn</ta>
            <ta e="T207" id="Seg_4246" s="T206">dempro-pro:case</ta>
            <ta e="T208" id="Seg_4247" s="T207">v-v:(ins)-v:cvb</ta>
            <ta e="T209" id="Seg_4248" s="T208">ptcl</ta>
            <ta e="T210" id="Seg_4249" s="T209">adj</ta>
            <ta e="T211" id="Seg_4250" s="T210">n-n:case</ta>
            <ta e="T212" id="Seg_4251" s="T211">v-v:tense-v:pred.pn</ta>
            <ta e="T213" id="Seg_4252" s="T212">n-n:case</ta>
            <ta e="T214" id="Seg_4253" s="T213">cardnum</ta>
            <ta e="T215" id="Seg_4254" s="T214">n-n:(ins)-n:case</ta>
            <ta e="T216" id="Seg_4255" s="T215">cardnum</ta>
            <ta e="T217" id="Seg_4256" s="T216">n-n&gt;adj</ta>
            <ta e="T218" id="Seg_4257" s="T217">n-n:case</ta>
            <ta e="T219" id="Seg_4258" s="T218">n-n:case</ta>
            <ta e="T220" id="Seg_4259" s="T219">v-v:tense-v:pred.pn</ta>
            <ta e="T221" id="Seg_4260" s="T220">ptcl</ta>
            <ta e="T222" id="Seg_4261" s="T221">adv</ta>
            <ta e="T223" id="Seg_4262" s="T222">ptcl</ta>
            <ta e="T224" id="Seg_4263" s="T223">v-v:tense-v:pred.pn</ta>
            <ta e="T225" id="Seg_4264" s="T224">adv</ta>
            <ta e="T226" id="Seg_4265" s="T225">dempro</ta>
            <ta e="T227" id="Seg_4266" s="T226">n-n:case</ta>
            <ta e="T228" id="Seg_4267" s="T227">n-n&gt;adj-n:(num)-n:case</ta>
            <ta e="T229" id="Seg_4268" s="T228">quant</ta>
            <ta e="T230" id="Seg_4269" s="T229">ptcl</ta>
            <ta e="T231" id="Seg_4270" s="T230">n-n:case</ta>
            <ta e="T232" id="Seg_4271" s="T231">v-v:tense-v:pred.pn</ta>
            <ta e="T233" id="Seg_4272" s="T232">adv</ta>
            <ta e="T234" id="Seg_4273" s="T233">n-n:poss-n:case</ta>
            <ta e="T235" id="Seg_4274" s="T234">n-n:(poss)-n:case</ta>
            <ta e="T236" id="Seg_4275" s="T235">n-n:case</ta>
            <ta e="T237" id="Seg_4276" s="T236">v-v:cvb</ta>
            <ta e="T238" id="Seg_4277" s="T237">v-v:tense-v:pred.pn</ta>
            <ta e="T239" id="Seg_4278" s="T238">pers-pro:case</ta>
            <ta e="T240" id="Seg_4279" s="T239">n-n:case</ta>
            <ta e="T241" id="Seg_4280" s="T240">v-v:tense-v:pred.pn</ta>
            <ta e="T242" id="Seg_4281" s="T241">dempro</ta>
            <ta e="T243" id="Seg_4282" s="T242">n-n:case</ta>
            <ta e="T244" id="Seg_4283" s="T243">n-n:poss-n:case</ta>
            <ta e="T245" id="Seg_4284" s="T244">pers-pro:case</ta>
            <ta e="T246" id="Seg_4285" s="T245">cardnum</ta>
            <ta e="T247" id="Seg_4286" s="T246">n-n&gt;adj</ta>
            <ta e="T248" id="Seg_4287" s="T247">n-n&gt;adj-n:case</ta>
            <ta e="T249" id="Seg_4288" s="T248">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T250" id="Seg_4289" s="T249">pers-pro:case</ta>
            <ta e="T251" id="Seg_4290" s="T250">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T252" id="Seg_4291" s="T251">n-n:case</ta>
            <ta e="T253" id="Seg_4292" s="T252">v-v:cvb</ta>
            <ta e="T254" id="Seg_4293" s="T253">v-v:tense-v:pred.pn</ta>
            <ta e="T255" id="Seg_4294" s="T254">v-v:tense-v:pred.pn</ta>
            <ta e="T256" id="Seg_4295" s="T255">n-n:(poss)-n:case</ta>
            <ta e="T257" id="Seg_4296" s="T256">v-v:tense-v:pred.pn</ta>
            <ta e="T258" id="Seg_4297" s="T257">n-n:(poss)-n:case</ta>
            <ta e="T259" id="Seg_4298" s="T258">v-v:mood.pn</ta>
            <ta e="T260" id="Seg_4299" s="T259">n-n:case</ta>
            <ta e="T261" id="Seg_4300" s="T260">v-v:tense-v:poss.pn</ta>
            <ta e="T262" id="Seg_4301" s="T261">ptcl</ta>
            <ta e="T263" id="Seg_4302" s="T262">ptcl</ta>
            <ta e="T264" id="Seg_4303" s="T263">pers-pro:case</ta>
            <ta e="T265" id="Seg_4304" s="T264">v-v:mood.pn</ta>
            <ta e="T266" id="Seg_4305" s="T265">v-v:(neg)-v:mood-v:temp.pn</ta>
            <ta e="T267" id="Seg_4306" s="T266">n-n:poss-n:case</ta>
            <ta e="T268" id="Seg_4307" s="T267">v-v:tense-v:poss.pn</ta>
            <ta e="T269" id="Seg_4308" s="T268">n-n:case</ta>
            <ta e="T270" id="Seg_4309" s="T269">n-n:case</ta>
            <ta e="T271" id="Seg_4310" s="T270">v-v:tense-v:pred.pn</ta>
            <ta e="T272" id="Seg_4311" s="T271">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T273" id="Seg_4312" s="T272">v-v:tense-v:pred.pn</ta>
            <ta e="T274" id="Seg_4313" s="T273">pers-pro:case</ta>
            <ta e="T275" id="Seg_4314" s="T274">pers-pro:case</ta>
            <ta e="T276" id="Seg_4315" s="T275">adv</ta>
            <ta e="T277" id="Seg_4316" s="T276">adj</ta>
            <ta e="T278" id="Seg_4317" s="T277">n-n:case</ta>
            <ta e="T279" id="Seg_4318" s="T278">v-v:cvb</ta>
            <ta e="T280" id="Seg_4319" s="T279">v-v:tense-v:poss.pn</ta>
            <ta e="T281" id="Seg_4320" s="T280">pers-pro:case</ta>
            <ta e="T282" id="Seg_4321" s="T281">adv</ta>
            <ta e="T283" id="Seg_4322" s="T282">v-v:tense-v:poss.pn</ta>
            <ta e="T284" id="Seg_4323" s="T283">ptcl</ta>
            <ta e="T285" id="Seg_4324" s="T284">n-n:(poss)-n:case</ta>
            <ta e="T286" id="Seg_4325" s="T285">post</ta>
            <ta e="T287" id="Seg_4326" s="T286">v-v:tense-v:poss.pn</ta>
            <ta e="T288" id="Seg_4327" s="T287">ptcl</ta>
            <ta e="T289" id="Seg_4328" s="T288">dempro-pro:case</ta>
            <ta e="T290" id="Seg_4329" s="T289">v-v:tense-v:poss.pn</ta>
            <ta e="T291" id="Seg_4330" s="T290">v-v:tense-v:poss.pn</ta>
            <ta e="T292" id="Seg_4331" s="T291">n-n:case</ta>
            <ta e="T293" id="Seg_4332" s="T292">dempro-pro:case</ta>
            <ta e="T294" id="Seg_4333" s="T293">v-v:tense-v:poss.pn</ta>
            <ta e="T295" id="Seg_4334" s="T294">adj-n:case</ta>
            <ta e="T296" id="Seg_4335" s="T295">n-n:case</ta>
            <ta e="T297" id="Seg_4336" s="T296">n-n:case</ta>
            <ta e="T298" id="Seg_4337" s="T297">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T299" id="Seg_4338" s="T298">post</ta>
            <ta e="T300" id="Seg_4339" s="T299">v-v:tense-v:poss.pn</ta>
            <ta e="T301" id="Seg_4340" s="T300">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T302" id="Seg_4341" s="T301">pers-pro:case</ta>
            <ta e="T303" id="Seg_4342" s="T302">n-n:case</ta>
            <ta e="T304" id="Seg_4343" s="T303">v-v:tense-v:poss.pn</ta>
            <ta e="T305" id="Seg_4344" s="T304">que-pro:case</ta>
            <ta e="T306" id="Seg_4345" s="T305">ptcl</ta>
            <ta e="T307" id="Seg_4346" s="T306">pers-pro:case</ta>
            <ta e="T308" id="Seg_4347" s="T307">n-n:(poss)</ta>
            <ta e="T309" id="Seg_4348" s="T308">ptcl</ta>
            <ta e="T310" id="Seg_4349" s="T309">v-v:tense-v:poss.pn</ta>
            <ta e="T311" id="Seg_4350" s="T310">v-v:cvb</ta>
            <ta e="T312" id="Seg_4351" s="T311">n-n:case</ta>
            <ta e="T313" id="Seg_4352" s="T312">v-v:tense-v:pred.pn</ta>
            <ta e="T314" id="Seg_4353" s="T313">dempro-pro:case</ta>
            <ta e="T315" id="Seg_4354" s="T314">v-v:cvb</ta>
            <ta e="T316" id="Seg_4355" s="T315">post</ta>
            <ta e="T317" id="Seg_4356" s="T316">ptcl</ta>
            <ta e="T318" id="Seg_4357" s="T317">que</ta>
            <ta e="T319" id="Seg_4358" s="T318">ptcl</ta>
            <ta e="T320" id="Seg_4359" s="T319">n-n:case</ta>
            <ta e="T321" id="Seg_4360" s="T320">v-v:tense-v:pred.pn</ta>
            <ta e="T322" id="Seg_4361" s="T321">dempro</ta>
            <ta e="T323" id="Seg_4362" s="T322">v-v:cvb</ta>
            <ta e="T324" id="Seg_4363" s="T323">post</ta>
            <ta e="T325" id="Seg_4364" s="T324">v-v:cvb</ta>
            <ta e="T326" id="Seg_4365" s="T325">v-v:tense-v:pred.pn</ta>
            <ta e="T327" id="Seg_4366" s="T326">n-n:poss-n:case</ta>
            <ta e="T328" id="Seg_4367" s="T327">v-v:cvb</ta>
            <ta e="T329" id="Seg_4368" s="T328">dempro-pro:case</ta>
            <ta e="T330" id="Seg_4369" s="T329">adj</ta>
            <ta e="T331" id="Seg_4370" s="T330">n-n:case</ta>
            <ta e="T332" id="Seg_4371" s="T331">v-v:cvb</ta>
            <ta e="T333" id="Seg_4372" s="T332">v-v:tense-v:pred.pn</ta>
            <ta e="T334" id="Seg_4373" s="T333">pers-pro:case</ta>
            <ta e="T335" id="Seg_4374" s="T334">v-v:ptcp</ta>
            <ta e="T336" id="Seg_4375" s="T335">n-n:poss-n:case</ta>
            <ta e="T337" id="Seg_4376" s="T336">v-v:(neg)-v:pred.pn</ta>
            <ta e="T338" id="Seg_4377" s="T337">adv</ta>
            <ta e="T339" id="Seg_4378" s="T338">v-v:tense-v:poss.pn</ta>
            <ta e="T340" id="Seg_4379" s="T339">n-n:poss-n:case</ta>
            <ta e="T341" id="Seg_4380" s="T340">v-v:tense-v:pred.pn</ta>
            <ta e="T342" id="Seg_4381" s="T341">pers-pro:case</ta>
            <ta e="T343" id="Seg_4382" s="T342">v-v:ptcp</ta>
            <ta e="T344" id="Seg_4383" s="T343">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T345" id="Seg_4384" s="T344">cardnum</ta>
            <ta e="T346" id="Seg_4385" s="T345">n-n&gt;adj</ta>
            <ta e="T347" id="Seg_4386" s="T346">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T348" id="Seg_4387" s="T347">pers-pro:case</ta>
            <ta e="T349" id="Seg_4388" s="T348">v-v:tense-v:poss.pn</ta>
            <ta e="T350" id="Seg_4389" s="T349">pers-pro:case</ta>
            <ta e="T351" id="Seg_4390" s="T350">post</ta>
            <ta e="T352" id="Seg_4391" s="T351">n-n:(poss)-n:case</ta>
            <ta e="T353" id="Seg_4392" s="T352">v-v:cvb</ta>
            <ta e="T354" id="Seg_4393" s="T353">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T355" id="Seg_4394" s="T354">adv</ta>
            <ta e="T356" id="Seg_4395" s="T355">adj</ta>
            <ta e="T357" id="Seg_4396" s="T356">n-n:case</ta>
            <ta e="T358" id="Seg_4397" s="T357">adv</ta>
            <ta e="T359" id="Seg_4398" s="T358">v-v:tense-v:pred.pn</ta>
            <ta e="T360" id="Seg_4399" s="T359">ptcl</ta>
            <ta e="T361" id="Seg_4400" s="T360">adv</ta>
            <ta e="T362" id="Seg_4401" s="T361">ptcl</ta>
            <ta e="T363" id="Seg_4402" s="T362">v-v:cvb</ta>
            <ta e="T364" id="Seg_4403" s="T363">v-v:tense-v:pred.pn</ta>
            <ta e="T365" id="Seg_4404" s="T364">n-n:poss-n:case</ta>
            <ta e="T366" id="Seg_4405" s="T365">adv</ta>
            <ta e="T367" id="Seg_4406" s="T366">n-n:case</ta>
            <ta e="T368" id="Seg_4407" s="T367">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_4408" s="T0">adv</ta>
            <ta e="T2" id="Seg_4409" s="T1">cardnum</ta>
            <ta e="T3" id="Seg_4410" s="T2">adj</ta>
            <ta e="T4" id="Seg_4411" s="T3">adj</ta>
            <ta e="T5" id="Seg_4412" s="T4">n</ta>
            <ta e="T6" id="Seg_4413" s="T5">v</ta>
            <ta e="T7" id="Seg_4414" s="T6">adv</ta>
            <ta e="T8" id="Seg_4415" s="T7">n</ta>
            <ta e="T9" id="Seg_4416" s="T8">n</ta>
            <ta e="T10" id="Seg_4417" s="T9">v</ta>
            <ta e="T11" id="Seg_4418" s="T10">dempro</ta>
            <ta e="T12" id="Seg_4419" s="T11">n</ta>
            <ta e="T13" id="Seg_4420" s="T12">n</ta>
            <ta e="T14" id="Seg_4421" s="T13">n</ta>
            <ta e="T15" id="Seg_4422" s="T14">n</ta>
            <ta e="T16" id="Seg_4423" s="T15">n</ta>
            <ta e="T17" id="Seg_4424" s="T16">v</ta>
            <ta e="T18" id="Seg_4425" s="T17">v</ta>
            <ta e="T19" id="Seg_4426" s="T18">dempro</ta>
            <ta e="T20" id="Seg_4427" s="T19">v</ta>
            <ta e="T21" id="Seg_4428" s="T20">post</ta>
            <ta e="T22" id="Seg_4429" s="T21">v</ta>
            <ta e="T23" id="Seg_4430" s="T22">v</ta>
            <ta e="T24" id="Seg_4431" s="T23">n</ta>
            <ta e="T25" id="Seg_4432" s="T24">n</ta>
            <ta e="T26" id="Seg_4433" s="T25">v</ta>
            <ta e="T27" id="Seg_4434" s="T26">aux</ta>
            <ta e="T28" id="Seg_4435" s="T27">cardnum</ta>
            <ta e="T29" id="Seg_4436" s="T28">n</ta>
            <ta e="T30" id="Seg_4437" s="T29">v</ta>
            <ta e="T31" id="Seg_4438" s="T30">v</ta>
            <ta e="T32" id="Seg_4439" s="T31">n</ta>
            <ta e="T33" id="Seg_4440" s="T32">n</ta>
            <ta e="T34" id="Seg_4441" s="T33">cop</ta>
            <ta e="T35" id="Seg_4442" s="T34">cardnum</ta>
            <ta e="T36" id="Seg_4443" s="T35">n</ta>
            <ta e="T37" id="Seg_4444" s="T36">cardnum</ta>
            <ta e="T38" id="Seg_4445" s="T37">v</ta>
            <ta e="T39" id="Seg_4446" s="T38">v</ta>
            <ta e="T40" id="Seg_4447" s="T39">adv</ta>
            <ta e="T41" id="Seg_4448" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_4449" s="T41">v</ta>
            <ta e="T43" id="Seg_4450" s="T42">v</ta>
            <ta e="T44" id="Seg_4451" s="T43">cardnum</ta>
            <ta e="T45" id="Seg_4452" s="T44">n</ta>
            <ta e="T46" id="Seg_4453" s="T45">v</ta>
            <ta e="T47" id="Seg_4454" s="T46">v</ta>
            <ta e="T48" id="Seg_4455" s="T47">n</ta>
            <ta e="T49" id="Seg_4456" s="T48">n</ta>
            <ta e="T50" id="Seg_4457" s="T49">n</ta>
            <ta e="T51" id="Seg_4458" s="T50">post</ta>
            <ta e="T52" id="Seg_4459" s="T51">cop</ta>
            <ta e="T53" id="Seg_4460" s="T52">dempro</ta>
            <ta e="T54" id="Seg_4461" s="T53">n</ta>
            <ta e="T55" id="Seg_4462" s="T54">que</ta>
            <ta e="T56" id="Seg_4463" s="T55">ptcl</ta>
            <ta e="T57" id="Seg_4464" s="T56">n</ta>
            <ta e="T58" id="Seg_4465" s="T57">n</ta>
            <ta e="T59" id="Seg_4466" s="T58">v</ta>
            <ta e="T60" id="Seg_4467" s="T59">v</ta>
            <ta e="T61" id="Seg_4468" s="T60">n</ta>
            <ta e="T62" id="Seg_4469" s="T61">post</ta>
            <ta e="T63" id="Seg_4470" s="T62">v</ta>
            <ta e="T64" id="Seg_4471" s="T63">dempro</ta>
            <ta e="T65" id="Seg_4472" s="T64">n</ta>
            <ta e="T66" id="Seg_4473" s="T65">v</ta>
            <ta e="T67" id="Seg_4474" s="T66">n</ta>
            <ta e="T68" id="Seg_4475" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_4476" s="T68">que</ta>
            <ta e="T70" id="Seg_4477" s="T69">ptcl</ta>
            <ta e="T71" id="Seg_4478" s="T70">n</ta>
            <ta e="T72" id="Seg_4479" s="T71">n</ta>
            <ta e="T73" id="Seg_4480" s="T72">v</ta>
            <ta e="T74" id="Seg_4481" s="T73">adv</ta>
            <ta e="T75" id="Seg_4482" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_4483" s="T75">adj</ta>
            <ta e="T77" id="Seg_4484" s="T76">n</ta>
            <ta e="T78" id="Seg_4485" s="T77">v</ta>
            <ta e="T79" id="Seg_4486" s="T78">v</ta>
            <ta e="T80" id="Seg_4487" s="T79">v</ta>
            <ta e="T81" id="Seg_4488" s="T80">v</ta>
            <ta e="T82" id="Seg_4489" s="T81">cardnum</ta>
            <ta e="T83" id="Seg_4490" s="T82">adj</ta>
            <ta e="T84" id="Seg_4491" s="T83">n</ta>
            <ta e="T85" id="Seg_4492" s="T84">pers</ta>
            <ta e="T86" id="Seg_4493" s="T85">n</ta>
            <ta e="T87" id="Seg_4494" s="T86">v</ta>
            <ta e="T88" id="Seg_4495" s="T87">n</ta>
            <ta e="T89" id="Seg_4496" s="T88">v</ta>
            <ta e="T90" id="Seg_4497" s="T89">aux</ta>
            <ta e="T91" id="Seg_4498" s="T90">dempro</ta>
            <ta e="T92" id="Seg_4499" s="T91">n</ta>
            <ta e="T93" id="Seg_4500" s="T92">v</ta>
            <ta e="T94" id="Seg_4501" s="T93">adv</ta>
            <ta e="T95" id="Seg_4502" s="T94">n</ta>
            <ta e="T96" id="Seg_4503" s="T95">adj</ta>
            <ta e="T97" id="Seg_4504" s="T96">cop</ta>
            <ta e="T98" id="Seg_4505" s="T97">n</ta>
            <ta e="T99" id="Seg_4506" s="T98">n</ta>
            <ta e="T100" id="Seg_4507" s="T99">v</ta>
            <ta e="T101" id="Seg_4508" s="T100">post</ta>
            <ta e="T102" id="Seg_4509" s="T101">v</ta>
            <ta e="T103" id="Seg_4510" s="T102">n</ta>
            <ta e="T104" id="Seg_4511" s="T103">v</ta>
            <ta e="T105" id="Seg_4512" s="T104">v</ta>
            <ta e="T106" id="Seg_4513" s="T105">aux</ta>
            <ta e="T107" id="Seg_4514" s="T106">v</ta>
            <ta e="T108" id="Seg_4515" s="T107">aux</ta>
            <ta e="T109" id="Seg_4516" s="T108">conj</ta>
            <ta e="T110" id="Seg_4517" s="T109">dempro</ta>
            <ta e="T111" id="Seg_4518" s="T110">v</ta>
            <ta e="T112" id="Seg_4519" s="T111">v</ta>
            <ta e="T113" id="Seg_4520" s="T112">n</ta>
            <ta e="T114" id="Seg_4521" s="T113">v</ta>
            <ta e="T115" id="Seg_4522" s="T114">dempro</ta>
            <ta e="T116" id="Seg_4523" s="T115">que</ta>
            <ta e="T117" id="Seg_4524" s="T116">ptcl</ta>
            <ta e="T118" id="Seg_4525" s="T117">n</ta>
            <ta e="T119" id="Seg_4526" s="T118">cop</ta>
            <ta e="T120" id="Seg_4527" s="T119">v</ta>
            <ta e="T121" id="Seg_4528" s="T120">v</ta>
            <ta e="T122" id="Seg_4529" s="T121">v</ta>
            <ta e="T123" id="Seg_4530" s="T122">n</ta>
            <ta e="T124" id="Seg_4531" s="T123">n</ta>
            <ta e="T125" id="Seg_4532" s="T124">v</ta>
            <ta e="T126" id="Seg_4533" s="T125">v</ta>
            <ta e="T127" id="Seg_4534" s="T126">dempro</ta>
            <ta e="T128" id="Seg_4535" s="T127">v</ta>
            <ta e="T129" id="Seg_4536" s="T128">cardnum</ta>
            <ta e="T130" id="Seg_4537" s="T129">n</ta>
            <ta e="T131" id="Seg_4538" s="T130">post</ta>
            <ta e="T132" id="Seg_4539" s="T131">adj</ta>
            <ta e="T133" id="Seg_4540" s="T132">n</ta>
            <ta e="T134" id="Seg_4541" s="T133">v</ta>
            <ta e="T135" id="Seg_4542" s="T134">interj</ta>
            <ta e="T136" id="Seg_4543" s="T135">que</ta>
            <ta e="T137" id="Seg_4544" s="T136">v</ta>
            <ta e="T138" id="Seg_4545" s="T137">n</ta>
            <ta e="T139" id="Seg_4546" s="T138">conj</ta>
            <ta e="T140" id="Seg_4547" s="T139">ptcl</ta>
            <ta e="T141" id="Seg_4548" s="T140">n</ta>
            <ta e="T142" id="Seg_4549" s="T141">n</ta>
            <ta e="T143" id="Seg_4550" s="T142">n</ta>
            <ta e="T144" id="Seg_4551" s="T143">conj</ta>
            <ta e="T145" id="Seg_4552" s="T144">ptcl</ta>
            <ta e="T146" id="Seg_4553" s="T145">dempro</ta>
            <ta e="T147" id="Seg_4554" s="T146">cardnum</ta>
            <ta e="T148" id="Seg_4555" s="T147">adj</ta>
            <ta e="T149" id="Seg_4556" s="T148">n</ta>
            <ta e="T150" id="Seg_4557" s="T149">ptcl</ta>
            <ta e="T151" id="Seg_4558" s="T150">v</ta>
            <ta e="T152" id="Seg_4559" s="T151">ptcl</ta>
            <ta e="T153" id="Seg_4560" s="T152">v</ta>
            <ta e="T154" id="Seg_4561" s="T153">n</ta>
            <ta e="T155" id="Seg_4562" s="T154">n</ta>
            <ta e="T156" id="Seg_4563" s="T155">adv</ta>
            <ta e="T157" id="Seg_4564" s="T156">v</ta>
            <ta e="T158" id="Seg_4565" s="T157">v</ta>
            <ta e="T159" id="Seg_4566" s="T158">aux</ta>
            <ta e="T160" id="Seg_4567" s="T159">v</ta>
            <ta e="T161" id="Seg_4568" s="T160">emphpro</ta>
            <ta e="T162" id="Seg_4569" s="T161">n</ta>
            <ta e="T163" id="Seg_4570" s="T162">dempro</ta>
            <ta e="T164" id="Seg_4571" s="T163">v</ta>
            <ta e="T165" id="Seg_4572" s="T164">ptcl</ta>
            <ta e="T166" id="Seg_4573" s="T165">adv</ta>
            <ta e="T167" id="Seg_4574" s="T166">v</ta>
            <ta e="T168" id="Seg_4575" s="T167">dempro</ta>
            <ta e="T169" id="Seg_4576" s="T168">adj</ta>
            <ta e="T170" id="Seg_4577" s="T169">ptcl</ta>
            <ta e="T171" id="Seg_4578" s="T170">n</ta>
            <ta e="T172" id="Seg_4579" s="T171">cop</ta>
            <ta e="T173" id="Seg_4580" s="T172">n</ta>
            <ta e="T174" id="Seg_4581" s="T173">n</ta>
            <ta e="T175" id="Seg_4582" s="T174">n</ta>
            <ta e="T176" id="Seg_4583" s="T175">n</ta>
            <ta e="T177" id="Seg_4584" s="T176">emphpro</ta>
            <ta e="T178" id="Seg_4585" s="T177">v</ta>
            <ta e="T179" id="Seg_4586" s="T178">aux</ta>
            <ta e="T180" id="Seg_4587" s="T179">dempro</ta>
            <ta e="T181" id="Seg_4588" s="T180">n</ta>
            <ta e="T182" id="Seg_4589" s="T181">adv</ta>
            <ta e="T183" id="Seg_4590" s="T182">v</ta>
            <ta e="T184" id="Seg_4591" s="T183">v</ta>
            <ta e="T185" id="Seg_4592" s="T184">n</ta>
            <ta e="T186" id="Seg_4593" s="T185">cardnum</ta>
            <ta e="T187" id="Seg_4594" s="T186">adj</ta>
            <ta e="T188" id="Seg_4595" s="T187">cop</ta>
            <ta e="T189" id="Seg_4596" s="T188">dempro</ta>
            <ta e="T190" id="Seg_4597" s="T189">n</ta>
            <ta e="T191" id="Seg_4598" s="T190">n</ta>
            <ta e="T192" id="Seg_4599" s="T191">n</ta>
            <ta e="T193" id="Seg_4600" s="T192">v</ta>
            <ta e="T194" id="Seg_4601" s="T193">n</ta>
            <ta e="T195" id="Seg_4602" s="T194">dempro</ta>
            <ta e="T196" id="Seg_4603" s="T195">dempro</ta>
            <ta e="T197" id="Seg_4604" s="T196">v</ta>
            <ta e="T198" id="Seg_4605" s="T197">n</ta>
            <ta e="T199" id="Seg_4606" s="T198">n</ta>
            <ta e="T200" id="Seg_4607" s="T199">v</ta>
            <ta e="T201" id="Seg_4608" s="T200">pers</ta>
            <ta e="T202" id="Seg_4609" s="T201">v</ta>
            <ta e="T203" id="Seg_4610" s="T202">emphpro</ta>
            <ta e="T204" id="Seg_4611" s="T203">n</ta>
            <ta e="T205" id="Seg_4612" s="T204">v</ta>
            <ta e="T206" id="Seg_4613" s="T205">v</ta>
            <ta e="T207" id="Seg_4614" s="T206">dempro</ta>
            <ta e="T208" id="Seg_4615" s="T207">v</ta>
            <ta e="T209" id="Seg_4616" s="T208">ptcl</ta>
            <ta e="T210" id="Seg_4617" s="T209">adj</ta>
            <ta e="T211" id="Seg_4618" s="T210">n</ta>
            <ta e="T212" id="Seg_4619" s="T211">v</ta>
            <ta e="T213" id="Seg_4620" s="T212">n</ta>
            <ta e="T214" id="Seg_4621" s="T213">cardnum</ta>
            <ta e="T215" id="Seg_4622" s="T214">n</ta>
            <ta e="T216" id="Seg_4623" s="T215">cardnum</ta>
            <ta e="T217" id="Seg_4624" s="T216">adj</ta>
            <ta e="T218" id="Seg_4625" s="T217">n</ta>
            <ta e="T219" id="Seg_4626" s="T218">n</ta>
            <ta e="T220" id="Seg_4627" s="T219">v</ta>
            <ta e="T221" id="Seg_4628" s="T220">ptcl</ta>
            <ta e="T222" id="Seg_4629" s="T221">adv</ta>
            <ta e="T223" id="Seg_4630" s="T222">ptcl</ta>
            <ta e="T224" id="Seg_4631" s="T223">v</ta>
            <ta e="T225" id="Seg_4632" s="T224">adv</ta>
            <ta e="T226" id="Seg_4633" s="T225">dempro</ta>
            <ta e="T227" id="Seg_4634" s="T226">n</ta>
            <ta e="T228" id="Seg_4635" s="T227">n</ta>
            <ta e="T229" id="Seg_4636" s="T228">quant</ta>
            <ta e="T230" id="Seg_4637" s="T229">ptcl</ta>
            <ta e="T231" id="Seg_4638" s="T230">n</ta>
            <ta e="T232" id="Seg_4639" s="T231">cop</ta>
            <ta e="T233" id="Seg_4640" s="T232">adv</ta>
            <ta e="T234" id="Seg_4641" s="T233">n</ta>
            <ta e="T235" id="Seg_4642" s="T234">n</ta>
            <ta e="T236" id="Seg_4643" s="T235">n</ta>
            <ta e="T237" id="Seg_4644" s="T236">v</ta>
            <ta e="T238" id="Seg_4645" s="T237">v</ta>
            <ta e="T239" id="Seg_4646" s="T238">pers</ta>
            <ta e="T240" id="Seg_4647" s="T239">n</ta>
            <ta e="T241" id="Seg_4648" s="T240">v</ta>
            <ta e="T242" id="Seg_4649" s="T241">dempro</ta>
            <ta e="T243" id="Seg_4650" s="T242">n</ta>
            <ta e="T244" id="Seg_4651" s="T243">n</ta>
            <ta e="T245" id="Seg_4652" s="T244">pers</ta>
            <ta e="T246" id="Seg_4653" s="T245">cardnum</ta>
            <ta e="T247" id="Seg_4654" s="T246">adj</ta>
            <ta e="T248" id="Seg_4655" s="T247">adj</ta>
            <ta e="T249" id="Seg_4656" s="T248">ptcl</ta>
            <ta e="T250" id="Seg_4657" s="T249">pers</ta>
            <ta e="T251" id="Seg_4658" s="T250">dempro</ta>
            <ta e="T252" id="Seg_4659" s="T251">n</ta>
            <ta e="T253" id="Seg_4660" s="T252">v</ta>
            <ta e="T254" id="Seg_4661" s="T253">v</ta>
            <ta e="T255" id="Seg_4662" s="T254">v</ta>
            <ta e="T256" id="Seg_4663" s="T255">n</ta>
            <ta e="T257" id="Seg_4664" s="T256">v</ta>
            <ta e="T258" id="Seg_4665" s="T257">n</ta>
            <ta e="T259" id="Seg_4666" s="T258">v</ta>
            <ta e="T260" id="Seg_4667" s="T259">n</ta>
            <ta e="T261" id="Seg_4668" s="T260">v</ta>
            <ta e="T262" id="Seg_4669" s="T261">ptcl</ta>
            <ta e="T263" id="Seg_4670" s="T262">ptcl</ta>
            <ta e="T264" id="Seg_4671" s="T263">pers</ta>
            <ta e="T265" id="Seg_4672" s="T264">v</ta>
            <ta e="T266" id="Seg_4673" s="T265">v</ta>
            <ta e="T267" id="Seg_4674" s="T266">n</ta>
            <ta e="T268" id="Seg_4675" s="T267">v</ta>
            <ta e="T269" id="Seg_4676" s="T268">n</ta>
            <ta e="T270" id="Seg_4677" s="T269">n</ta>
            <ta e="T271" id="Seg_4678" s="T270">v</ta>
            <ta e="T272" id="Seg_4679" s="T271">dempro</ta>
            <ta e="T273" id="Seg_4680" s="T272">v</ta>
            <ta e="T274" id="Seg_4681" s="T273">pers</ta>
            <ta e="T275" id="Seg_4682" s="T274">pers</ta>
            <ta e="T276" id="Seg_4683" s="T275">adv</ta>
            <ta e="T277" id="Seg_4684" s="T276">adj</ta>
            <ta e="T278" id="Seg_4685" s="T277">n</ta>
            <ta e="T279" id="Seg_4686" s="T278">v</ta>
            <ta e="T280" id="Seg_4687" s="T279">v</ta>
            <ta e="T281" id="Seg_4688" s="T280">pers</ta>
            <ta e="T282" id="Seg_4689" s="T281">adv</ta>
            <ta e="T283" id="Seg_4690" s="T282">v</ta>
            <ta e="T284" id="Seg_4691" s="T283">ptcl</ta>
            <ta e="T285" id="Seg_4692" s="T284">n</ta>
            <ta e="T286" id="Seg_4693" s="T285">post</ta>
            <ta e="T287" id="Seg_4694" s="T286">v</ta>
            <ta e="T288" id="Seg_4695" s="T287">ptcl</ta>
            <ta e="T289" id="Seg_4696" s="T288">dempro</ta>
            <ta e="T290" id="Seg_4697" s="T289">v</ta>
            <ta e="T291" id="Seg_4698" s="T290">v</ta>
            <ta e="T292" id="Seg_4699" s="T291">n</ta>
            <ta e="T293" id="Seg_4700" s="T292">dempro</ta>
            <ta e="T294" id="Seg_4701" s="T293">v</ta>
            <ta e="T295" id="Seg_4702" s="T294">adj</ta>
            <ta e="T296" id="Seg_4703" s="T295">n</ta>
            <ta e="T297" id="Seg_4704" s="T296">n</ta>
            <ta e="T298" id="Seg_4705" s="T297">v</ta>
            <ta e="T299" id="Seg_4706" s="T298">post</ta>
            <ta e="T300" id="Seg_4707" s="T299">v</ta>
            <ta e="T301" id="Seg_4708" s="T300">ptcl</ta>
            <ta e="T302" id="Seg_4709" s="T301">pers</ta>
            <ta e="T303" id="Seg_4710" s="T302">n</ta>
            <ta e="T304" id="Seg_4711" s="T303">v</ta>
            <ta e="T305" id="Seg_4712" s="T304">que</ta>
            <ta e="T306" id="Seg_4713" s="T305">ptcl</ta>
            <ta e="T307" id="Seg_4714" s="T306">pers</ta>
            <ta e="T308" id="Seg_4715" s="T307">n</ta>
            <ta e="T309" id="Seg_4716" s="T308">ptcl</ta>
            <ta e="T310" id="Seg_4717" s="T309">v</ta>
            <ta e="T311" id="Seg_4718" s="T310">v</ta>
            <ta e="T312" id="Seg_4719" s="T311">n</ta>
            <ta e="T313" id="Seg_4720" s="T312">v</ta>
            <ta e="T314" id="Seg_4721" s="T313">dempro</ta>
            <ta e="T315" id="Seg_4722" s="T314">v</ta>
            <ta e="T316" id="Seg_4723" s="T315">post</ta>
            <ta e="T317" id="Seg_4724" s="T316">ptcl</ta>
            <ta e="T318" id="Seg_4725" s="T317">que</ta>
            <ta e="T319" id="Seg_4726" s="T318">ptcl</ta>
            <ta e="T320" id="Seg_4727" s="T319">n</ta>
            <ta e="T321" id="Seg_4728" s="T320">v</ta>
            <ta e="T322" id="Seg_4729" s="T321">dempro</ta>
            <ta e="T323" id="Seg_4730" s="T322">v</ta>
            <ta e="T324" id="Seg_4731" s="T323">post</ta>
            <ta e="T325" id="Seg_4732" s="T324">v</ta>
            <ta e="T326" id="Seg_4733" s="T325">v</ta>
            <ta e="T327" id="Seg_4734" s="T326">n</ta>
            <ta e="T328" id="Seg_4735" s="T327">v</ta>
            <ta e="T329" id="Seg_4736" s="T328">dempro</ta>
            <ta e="T330" id="Seg_4737" s="T329">adj</ta>
            <ta e="T331" id="Seg_4738" s="T330">n</ta>
            <ta e="T332" id="Seg_4739" s="T331">v</ta>
            <ta e="T333" id="Seg_4740" s="T332">aux</ta>
            <ta e="T334" id="Seg_4741" s="T333">pers</ta>
            <ta e="T335" id="Seg_4742" s="T334">v</ta>
            <ta e="T336" id="Seg_4743" s="T335">n</ta>
            <ta e="T337" id="Seg_4744" s="T336">v</ta>
            <ta e="T338" id="Seg_4745" s="T337">adv</ta>
            <ta e="T339" id="Seg_4746" s="T338">v</ta>
            <ta e="T340" id="Seg_4747" s="T339">n</ta>
            <ta e="T341" id="Seg_4748" s="T340">v</ta>
            <ta e="T342" id="Seg_4749" s="T341">pers</ta>
            <ta e="T343" id="Seg_4750" s="T342">v</ta>
            <ta e="T344" id="Seg_4751" s="T343">v</ta>
            <ta e="T345" id="Seg_4752" s="T344">cardnum</ta>
            <ta e="T346" id="Seg_4753" s="T345">adj</ta>
            <ta e="T347" id="Seg_4754" s="T346">adj</ta>
            <ta e="T348" id="Seg_4755" s="T347">pers</ta>
            <ta e="T349" id="Seg_4756" s="T348">v</ta>
            <ta e="T350" id="Seg_4757" s="T349">pers</ta>
            <ta e="T351" id="Seg_4758" s="T350">post</ta>
            <ta e="T352" id="Seg_4759" s="T351">n</ta>
            <ta e="T353" id="Seg_4760" s="T352">v</ta>
            <ta e="T354" id="Seg_4761" s="T353">v</ta>
            <ta e="T355" id="Seg_4762" s="T354">adv</ta>
            <ta e="T356" id="Seg_4763" s="T355">adj</ta>
            <ta e="T357" id="Seg_4764" s="T356">n</ta>
            <ta e="T358" id="Seg_4765" s="T357">adv</ta>
            <ta e="T359" id="Seg_4766" s="T358">v</ta>
            <ta e="T360" id="Seg_4767" s="T359">ptcl</ta>
            <ta e="T361" id="Seg_4768" s="T360">adv</ta>
            <ta e="T362" id="Seg_4769" s="T361">ptcl</ta>
            <ta e="T363" id="Seg_4770" s="T362">v</ta>
            <ta e="T364" id="Seg_4771" s="T363">v</ta>
            <ta e="T365" id="Seg_4772" s="T364">n</ta>
            <ta e="T366" id="Seg_4773" s="T365">adv</ta>
            <ta e="T367" id="Seg_4774" s="T366">n</ta>
            <ta e="T368" id="Seg_4775" s="T367">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_4776" s="T0">adv:Time</ta>
            <ta e="T5" id="Seg_4777" s="T4">np.h:Th</ta>
            <ta e="T7" id="Seg_4778" s="T6">adv:Time</ta>
            <ta e="T8" id="Seg_4779" s="T7">np:L</ta>
            <ta e="T9" id="Seg_4780" s="T8">np.h:A</ta>
            <ta e="T12" id="Seg_4781" s="T11">np.h:A</ta>
            <ta e="T14" id="Seg_4782" s="T13">np:Path</ta>
            <ta e="T16" id="Seg_4783" s="T15">np:P</ta>
            <ta e="T17" id="Seg_4784" s="T16">0.3.h:A</ta>
            <ta e="T22" id="Seg_4785" s="T21">0.3.h:A</ta>
            <ta e="T23" id="Seg_4786" s="T22">0.3.h:E</ta>
            <ta e="T24" id="Seg_4787" s="T23">np:P</ta>
            <ta e="T25" id="Seg_4788" s="T24">np:Cau</ta>
            <ta e="T29" id="Seg_4789" s="T28">n:Time</ta>
            <ta e="T30" id="Seg_4790" s="T29">0.3.h:A</ta>
            <ta e="T32" id="Seg_4791" s="T31">np:Th</ta>
            <ta e="T36" id="Seg_4792" s="T35">0.3.h:Poss np:So</ta>
            <ta e="T37" id="Seg_4793" s="T36">np:P</ta>
            <ta e="T39" id="Seg_4794" s="T38">0.3.h:A</ta>
            <ta e="T43" id="Seg_4795" s="T42">0.3.h:A</ta>
            <ta e="T45" id="Seg_4796" s="T44">np:P</ta>
            <ta e="T46" id="Seg_4797" s="T45">0.3.h:A</ta>
            <ta e="T47" id="Seg_4798" s="T46">0.3.h:A</ta>
            <ta e="T48" id="Seg_4799" s="T47">np:Th</ta>
            <ta e="T53" id="Seg_4800" s="T52">pro.h:Th</ta>
            <ta e="T54" id="Seg_4801" s="T53">np:Cau</ta>
            <ta e="T57" id="Seg_4802" s="T56">np:Poss</ta>
            <ta e="T58" id="Seg_4803" s="T57">np:G</ta>
            <ta e="T62" id="Seg_4804" s="T60">pp:G</ta>
            <ta e="T63" id="Seg_4805" s="T62">0.3:Cau</ta>
            <ta e="T65" id="Seg_4806" s="T64">np:G</ta>
            <ta e="T66" id="Seg_4807" s="T65">0.3.h:A</ta>
            <ta e="T67" id="Seg_4808" s="T66">0.3:Poss np:Th</ta>
            <ta e="T69" id="Seg_4809" s="T68">pro:L</ta>
            <ta e="T72" id="Seg_4810" s="T71">np:St</ta>
            <ta e="T74" id="Seg_4811" s="T73">adv:G</ta>
            <ta e="T79" id="Seg_4812" s="T78">0.3.h:Th</ta>
            <ta e="T80" id="Seg_4813" s="T79">0.3.h:Th</ta>
            <ta e="T81" id="Seg_4814" s="T80">0.3.h:E</ta>
            <ta e="T84" id="Seg_4815" s="T83">np.h:A</ta>
            <ta e="T87" id="Seg_4816" s="T86">0.3.h:Th</ta>
            <ta e="T88" id="Seg_4817" s="T87">np:P</ta>
            <ta e="T92" id="Seg_4818" s="T91">np.h:A</ta>
            <ta e="T96" id="Seg_4819" s="T95">np:Th</ta>
            <ta e="T97" id="Seg_4820" s="T96">0.3.h:Poss</ta>
            <ta e="T98" id="Seg_4821" s="T97">np.h:A</ta>
            <ta e="T99" id="Seg_4822" s="T98">np.h:St</ta>
            <ta e="T100" id="Seg_4823" s="T99">0.3.h:E</ta>
            <ta e="T102" id="Seg_4824" s="T101">0.3.h:E</ta>
            <ta e="T103" id="Seg_4825" s="T102">np:Th</ta>
            <ta e="T104" id="Seg_4826" s="T103">0.3.h:A</ta>
            <ta e="T108" id="Seg_4827" s="T107">0.3.h:A</ta>
            <ta e="T110" id="Seg_4828" s="T109">pro:St</ta>
            <ta e="T111" id="Seg_4829" s="T110">0.1.h:E</ta>
            <ta e="T112" id="Seg_4830" s="T111">0.3.h:A</ta>
            <ta e="T113" id="Seg_4831" s="T112">0.3.h:Poss np.h:R</ta>
            <ta e="T114" id="Seg_4832" s="T113">0.3.h:A</ta>
            <ta e="T115" id="Seg_4833" s="T114">pro.h:A</ta>
            <ta e="T119" id="Seg_4834" s="T118">0.3.h:Th</ta>
            <ta e="T120" id="Seg_4835" s="T119">0.3.h:A</ta>
            <ta e="T123" id="Seg_4836" s="T122">np.h:Th</ta>
            <ta e="T124" id="Seg_4837" s="T123">np:G</ta>
            <ta e="T125" id="Seg_4838" s="T124">0.3.h:A</ta>
            <ta e="T128" id="Seg_4839" s="T127">0.3.h:A</ta>
            <ta e="T133" id="Seg_4840" s="T132">np.h:A</ta>
            <ta e="T137" id="Seg_4841" s="T136">0.2.h:E</ta>
            <ta e="T138" id="Seg_4842" s="T137">np:L</ta>
            <ta e="T142" id="Seg_4843" s="T141">np:L</ta>
            <ta e="T143" id="Seg_4844" s="T142">np:L</ta>
            <ta e="T149" id="Seg_4845" s="T148">np.h:Th</ta>
            <ta e="T154" id="Seg_4846" s="T153">np.h:Th</ta>
            <ta e="T159" id="Seg_4847" s="T158">0.2.h:A</ta>
            <ta e="T160" id="Seg_4848" s="T159">0.3.h:E</ta>
            <ta e="T161" id="Seg_4849" s="T160">pro.h:Poss</ta>
            <ta e="T162" id="Seg_4850" s="T161">np:So</ta>
            <ta e="T164" id="Seg_4851" s="T163">0.3.h:A</ta>
            <ta e="T167" id="Seg_4852" s="T166">0.3.h:A</ta>
            <ta e="T168" id="Seg_4853" s="T167">pro.h:Th</ta>
            <ta e="T173" id="Seg_4854" s="T172">np:A</ta>
            <ta e="T174" id="Seg_4855" s="T173">np:A</ta>
            <ta e="T175" id="Seg_4856" s="T174">0.3.h:Poss np:Poss</ta>
            <ta e="T176" id="Seg_4857" s="T175">np:G</ta>
            <ta e="T181" id="Seg_4858" s="T180">np.h:A</ta>
            <ta e="T182" id="Seg_4859" s="T181">adv:L</ta>
            <ta e="T185" id="Seg_4860" s="T184">np.h:Poss</ta>
            <ta e="T187" id="Seg_4861" s="T186">np.h:Th</ta>
            <ta e="T189" id="Seg_4862" s="T188">pro:So</ta>
            <ta e="T190" id="Seg_4863" s="T189">np.h:Th</ta>
            <ta e="T191" id="Seg_4864" s="T190">np.h:A</ta>
            <ta e="T194" id="Seg_4865" s="T193">np.h:R</ta>
            <ta e="T198" id="Seg_4866" s="T197">np.h:E</ta>
            <ta e="T201" id="Seg_4867" s="T200">pro.h:A</ta>
            <ta e="T203" id="Seg_4868" s="T202">pro.h:Poss</ta>
            <ta e="T204" id="Seg_4869" s="T203">np.h:Th</ta>
            <ta e="T206" id="Seg_4870" s="T205">0.3.h:A</ta>
            <ta e="T207" id="Seg_4871" s="T206">pro:Th</ta>
            <ta e="T208" id="Seg_4872" s="T207">0.3.h:A</ta>
            <ta e="T211" id="Seg_4873" s="T210">np.h:Th</ta>
            <ta e="T212" id="Seg_4874" s="T211">0.3.h:A</ta>
            <ta e="T213" id="Seg_4875" s="T212">np.h:R</ta>
            <ta e="T215" id="Seg_4876" s="T214">n:Time</ta>
            <ta e="T219" id="Seg_4877" s="T218">np.h:P</ta>
            <ta e="T224" id="Seg_4878" s="T223">0.3.h:Th</ta>
            <ta e="T228" id="Seg_4879" s="T227">np.h:Th</ta>
            <ta e="T234" id="Seg_4880" s="T233">0.3.h:Poss np:Ins</ta>
            <ta e="T235" id="Seg_4881" s="T234">0.3.h:Poss np.h:A</ta>
            <ta e="T236" id="Seg_4882" s="T235">np:Th</ta>
            <ta e="T237" id="Seg_4883" s="T236">0.3.h:A</ta>
            <ta e="T240" id="Seg_4884" s="T239">np.h:A</ta>
            <ta e="T243" id="Seg_4885" s="T242">np.h:Poss</ta>
            <ta e="T244" id="Seg_4886" s="T243">np:G</ta>
            <ta e="T245" id="Seg_4887" s="T244">pro.h:Poss</ta>
            <ta e="T248" id="Seg_4888" s="T247">np.h:Th</ta>
            <ta e="T250" id="Seg_4889" s="T249">pro.h:Poss</ta>
            <ta e="T251" id="Seg_4890" s="T250">pro.h:St</ta>
            <ta e="T252" id="Seg_4891" s="T251">np.h:E</ta>
            <ta e="T253" id="Seg_4892" s="T252">0.3.h:E</ta>
            <ta e="T255" id="Seg_4893" s="T254">0.3.h:A</ta>
            <ta e="T256" id="Seg_4894" s="T255">np.h:A</ta>
            <ta e="T259" id="Seg_4895" s="T258">0.2.h:A</ta>
            <ta e="T260" id="Seg_4896" s="T259">np.h:A</ta>
            <ta e="T264" id="Seg_4897" s="T263">pro.h:A</ta>
            <ta e="T266" id="Seg_4898" s="T265">0.2.h:A</ta>
            <ta e="T267" id="Seg_4899" s="T266">0.1.h:Poss np:P</ta>
            <ta e="T268" id="Seg_4900" s="T267">0.3.h:A</ta>
            <ta e="T269" id="Seg_4901" s="T268">np.h:A</ta>
            <ta e="T270" id="Seg_4902" s="T269">np:G</ta>
            <ta e="T272" id="Seg_4903" s="T271">pro.h:A</ta>
            <ta e="T274" id="Seg_4904" s="T273">pro.h:A</ta>
            <ta e="T275" id="Seg_4905" s="T274">np.h:P</ta>
            <ta e="T276" id="Seg_4906" s="T275">adv:L</ta>
            <ta e="T281" id="Seg_4907" s="T280">pro.h:A</ta>
            <ta e="T282" id="Seg_4908" s="T281">adv:L</ta>
            <ta e="T286" id="Seg_4909" s="T284">0.2.h:Poss pp:G</ta>
            <ta e="T287" id="Seg_4910" s="T286">0.2.h:A</ta>
            <ta e="T290" id="Seg_4911" s="T289">0.1.h:A</ta>
            <ta e="T291" id="Seg_4912" s="T290">0.3.h:A</ta>
            <ta e="T292" id="Seg_4913" s="T291">np.h:A</ta>
            <ta e="T300" id="Seg_4914" s="T299">0.2.h:A</ta>
            <ta e="T302" id="Seg_4915" s="T301">pro.h:A</ta>
            <ta e="T303" id="Seg_4916" s="T302">np:Th</ta>
            <ta e="T305" id="Seg_4917" s="T304">pro.h:A</ta>
            <ta e="T307" id="Seg_4918" s="T306">pro.h:Th</ta>
            <ta e="T311" id="Seg_4919" s="T310">0.3.h:A</ta>
            <ta e="T312" id="Seg_4920" s="T311">np:Th</ta>
            <ta e="T313" id="Seg_4921" s="T312">0.3.h:A</ta>
            <ta e="T314" id="Seg_4922" s="T313">pro:Th</ta>
            <ta e="T315" id="Seg_4923" s="T314">0.3.h:A</ta>
            <ta e="T320" id="Seg_4924" s="T319">n:Time</ta>
            <ta e="T321" id="Seg_4925" s="T320">0.3.h:Th</ta>
            <ta e="T323" id="Seg_4926" s="T322">0.3.h:Th</ta>
            <ta e="T325" id="Seg_4927" s="T324">0.3.h:A</ta>
            <ta e="T326" id="Seg_4928" s="T325">0.3.h:A</ta>
            <ta e="T327" id="Seg_4929" s="T326">0.3.h:Poss np.h:Th</ta>
            <ta e="T328" id="Seg_4930" s="T327">0.3.h:A</ta>
            <ta e="T331" id="Seg_4931" s="T330">np.h:A</ta>
            <ta e="T334" id="Seg_4932" s="T333">pro.h:A</ta>
            <ta e="T336" id="Seg_4933" s="T335">np:So</ta>
            <ta e="T337" id="Seg_4934" s="T336">0.1.h:A</ta>
            <ta e="T339" id="Seg_4935" s="T338">0.1.h:A</ta>
            <ta e="T340" id="Seg_4936" s="T339">0.3.h:Poss np.h:R</ta>
            <ta e="T341" id="Seg_4937" s="T340">0.3.h:A</ta>
            <ta e="T342" id="Seg_4938" s="T341">pro.h:Poss</ta>
            <ta e="T344" id="Seg_4939" s="T343">0.2.h:E</ta>
            <ta e="T347" id="Seg_4940" s="T346">np.h:Th</ta>
            <ta e="T348" id="Seg_4941" s="T347">pro.h:A</ta>
            <ta e="T351" id="Seg_4942" s="T349">pp:Com</ta>
            <ta e="T352" id="Seg_4943" s="T351">0.3.h:Poss np.h:A</ta>
            <ta e="T357" id="Seg_4944" s="T356">np.h:A</ta>
            <ta e="T363" id="Seg_4945" s="T362">0.3.h:A</ta>
            <ta e="T364" id="Seg_4946" s="T363">0.3.h:Th</ta>
            <ta e="T365" id="Seg_4947" s="T364">0.3.h:Poss np:G</ta>
            <ta e="T367" id="Seg_4948" s="T366">np:Th</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T5" id="Seg_4949" s="T4">np.h:S</ta>
            <ta e="T6" id="Seg_4950" s="T5">v:pred</ta>
            <ta e="T9" id="Seg_4951" s="T8">np.h:S</ta>
            <ta e="T10" id="Seg_4952" s="T9">v:pred</ta>
            <ta e="T12" id="Seg_4953" s="T11">np.h:S</ta>
            <ta e="T17" id="Seg_4954" s="T14">s:purp</ta>
            <ta e="T18" id="Seg_4955" s="T17">v:pred</ta>
            <ta e="T21" id="Seg_4956" s="T18">s:adv</ta>
            <ta e="T22" id="Seg_4957" s="T21">s:comp</ta>
            <ta e="T23" id="Seg_4958" s="T22">0.3.h:S v:pred</ta>
            <ta e="T24" id="Seg_4959" s="T23">np:O</ta>
            <ta e="T25" id="Seg_4960" s="T24">np:S</ta>
            <ta e="T27" id="Seg_4961" s="T26">v:pred</ta>
            <ta e="T30" id="Seg_4962" s="T29">0.3.h:S v:pred</ta>
            <ta e="T31" id="Seg_4963" s="T30">s:rel</ta>
            <ta e="T32" id="Seg_4964" s="T31">np:O</ta>
            <ta e="T34" id="Seg_4965" s="T32">s:adv</ta>
            <ta e="T37" id="Seg_4966" s="T36">np:O</ta>
            <ta e="T38" id="Seg_4967" s="T37">s:adv</ta>
            <ta e="T39" id="Seg_4968" s="T38">0.3.h:S v:pred</ta>
            <ta e="T43" id="Seg_4969" s="T39">s:adv</ta>
            <ta e="T45" id="Seg_4970" s="T44">np:O</ta>
            <ta e="T46" id="Seg_4971" s="T45">0.3.h:S v:pred</ta>
            <ta e="T47" id="Seg_4972" s="T46">0.3.h:S v:pred</ta>
            <ta e="T48" id="Seg_4973" s="T47">np:S</ta>
            <ta e="T50" id="Seg_4974" s="T49">n:pred</ta>
            <ta e="T52" id="Seg_4975" s="T51">cop</ta>
            <ta e="T53" id="Seg_4976" s="T52">pro.h:O</ta>
            <ta e="T54" id="Seg_4977" s="T53">np:S</ta>
            <ta e="T60" id="Seg_4978" s="T59">v:pred</ta>
            <ta e="T63" id="Seg_4979" s="T62">0.3:S v:pred</ta>
            <ta e="T66" id="Seg_4980" s="T65">0.3.h:S v:pred</ta>
            <ta e="T67" id="Seg_4981" s="T66">0.3:S adj:pred</ta>
            <ta e="T72" id="Seg_4982" s="T71">np:S</ta>
            <ta e="T73" id="Seg_4983" s="T72">v:pred</ta>
            <ta e="T79" id="Seg_4984" s="T78">0.3.h:S v:pred</ta>
            <ta e="T80" id="Seg_4985" s="T79">s:adv</ta>
            <ta e="T81" id="Seg_4986" s="T80">0.3.h:S v:pred</ta>
            <ta e="T84" id="Seg_4987" s="T83">np.h:S</ta>
            <ta e="T87" id="Seg_4988" s="T84">s:adv</ta>
            <ta e="T88" id="Seg_4989" s="T87">np:O</ta>
            <ta e="T90" id="Seg_4990" s="T89">v:pred</ta>
            <ta e="T92" id="Seg_4991" s="T91">np.h:S</ta>
            <ta e="T93" id="Seg_4992" s="T92">v:pred</ta>
            <ta e="T96" id="Seg_4993" s="T95">adj:pred</ta>
            <ta e="T97" id="Seg_4994" s="T96">0.3.h:S cop</ta>
            <ta e="T98" id="Seg_4995" s="T97">np.h:S</ta>
            <ta e="T101" id="Seg_4996" s="T98">s:temp</ta>
            <ta e="T102" id="Seg_4997" s="T101">s:adv</ta>
            <ta e="T104" id="Seg_4998" s="T102">s:adv</ta>
            <ta e="T106" id="Seg_4999" s="T105">v:pred</ta>
            <ta e="T108" id="Seg_5000" s="T107">0.3.h:S v:pred</ta>
            <ta e="T112" id="Seg_5001" s="T109">s:adv</ta>
            <ta e="T114" id="Seg_5002" s="T113">0.3.h:S v:pred</ta>
            <ta e="T115" id="Seg_5003" s="T114">pro.h:S</ta>
            <ta e="T119" id="Seg_5004" s="T115">s:adv</ta>
            <ta e="T120" id="Seg_5005" s="T119">s:adv</ta>
            <ta e="T125" id="Seg_5006" s="T120">s:adv</ta>
            <ta e="T126" id="Seg_5007" s="T125">v:pred</ta>
            <ta e="T128" id="Seg_5008" s="T126">s:temp</ta>
            <ta e="T133" id="Seg_5009" s="T132">np.h:S</ta>
            <ta e="T134" id="Seg_5010" s="T133">v:pred</ta>
            <ta e="T137" id="Seg_5011" s="T136">0.2.h:S v:pred</ta>
            <ta e="T140" id="Seg_5012" s="T139">ptcl:pred</ta>
            <ta e="T145" id="Seg_5013" s="T144">ptcl:pred</ta>
            <ta e="T149" id="Seg_5014" s="T148">np.h:S</ta>
            <ta e="T150" id="Seg_5015" s="T149">ptcl:pred</ta>
            <ta e="T154" id="Seg_5016" s="T153">np.h:O</ta>
            <ta e="T157" id="Seg_5017" s="T155">s:adv</ta>
            <ta e="T159" id="Seg_5018" s="T158">0.2.h:S v:pred</ta>
            <ta e="T160" id="Seg_5019" s="T159">s:adv</ta>
            <ta e="T164" id="Seg_5020" s="T163">0.3.h:S v:pred</ta>
            <ta e="T167" id="Seg_5021" s="T166">0.3.h:S v:pred</ta>
            <ta e="T168" id="Seg_5022" s="T167">pro.h:S</ta>
            <ta e="T171" id="Seg_5023" s="T170">n:pred</ta>
            <ta e="T172" id="Seg_5024" s="T171">cop</ta>
            <ta e="T173" id="Seg_5025" s="T172">np:S</ta>
            <ta e="T174" id="Seg_5026" s="T173">np:S</ta>
            <ta e="T179" id="Seg_5027" s="T178">v:pred</ta>
            <ta e="T181" id="Seg_5028" s="T180">np.h:S</ta>
            <ta e="T183" id="Seg_5029" s="T182">v:pred</ta>
            <ta e="T185" id="Seg_5030" s="T184">np.h:S</ta>
            <ta e="T187" id="Seg_5031" s="T186">adj:pred</ta>
            <ta e="T188" id="Seg_5032" s="T187">cop</ta>
            <ta e="T190" id="Seg_5033" s="T189">np.h:O</ta>
            <ta e="T191" id="Seg_5034" s="T190">np.h:S</ta>
            <ta e="T193" id="Seg_5035" s="T192">v:pred</ta>
            <ta e="T200" id="Seg_5036" s="T194">s:adv</ta>
            <ta e="T201" id="Seg_5037" s="T200">pro.h:S</ta>
            <ta e="T202" id="Seg_5038" s="T201">v:pred</ta>
            <ta e="T206" id="Seg_5039" s="T205">0.3.h:S v:pred</ta>
            <ta e="T208" id="Seg_5040" s="T206">s:adv</ta>
            <ta e="T211" id="Seg_5041" s="T210">np.h:O</ta>
            <ta e="T212" id="Seg_5042" s="T211">0.3.h:S v:pred</ta>
            <ta e="T219" id="Seg_5043" s="T218">np.h:S</ta>
            <ta e="T220" id="Seg_5044" s="T219">v:pred</ta>
            <ta e="T224" id="Seg_5045" s="T223">0.3.h:S v:pred</ta>
            <ta e="T228" id="Seg_5046" s="T227">np.h:S</ta>
            <ta e="T231" id="Seg_5047" s="T230">n:pred</ta>
            <ta e="T232" id="Seg_5048" s="T231">cop</ta>
            <ta e="T235" id="Seg_5049" s="T234">np.h:S</ta>
            <ta e="T237" id="Seg_5050" s="T235">s:purp</ta>
            <ta e="T238" id="Seg_5051" s="T237">v:pred</ta>
            <ta e="T240" id="Seg_5052" s="T239">np.h:S</ta>
            <ta e="T241" id="Seg_5053" s="T240">v:pred</ta>
            <ta e="T245" id="Seg_5054" s="T244">pro.h:S</ta>
            <ta e="T248" id="Seg_5055" s="T247">adj:pred </ta>
            <ta e="T252" id="Seg_5056" s="T251">np.h:S</ta>
            <ta e="T253" id="Seg_5057" s="T252">s:comp</ta>
            <ta e="T254" id="Seg_5058" s="T253">v:pred</ta>
            <ta e="T255" id="Seg_5059" s="T254">0.3.h:S v:pred</ta>
            <ta e="T256" id="Seg_5060" s="T255">np.h:S</ta>
            <ta e="T257" id="Seg_5061" s="T256">v:pred</ta>
            <ta e="T259" id="Seg_5062" s="T258">0.2.h:S v:pred</ta>
            <ta e="T260" id="Seg_5063" s="T259">np.h:S</ta>
            <ta e="T261" id="Seg_5064" s="T260">v:pred</ta>
            <ta e="T264" id="Seg_5065" s="T263">pro.h:S</ta>
            <ta e="T265" id="Seg_5066" s="T264">v:pred</ta>
            <ta e="T266" id="Seg_5067" s="T265">s:temp</ta>
            <ta e="T267" id="Seg_5068" s="T266">np:O</ta>
            <ta e="T268" id="Seg_5069" s="T267">0.3.h:S v:pred</ta>
            <ta e="T269" id="Seg_5070" s="T268">np.h:S</ta>
            <ta e="T271" id="Seg_5071" s="T270">v:pred</ta>
            <ta e="T272" id="Seg_5072" s="T271">pro.h:S</ta>
            <ta e="T273" id="Seg_5073" s="T272">v:pred</ta>
            <ta e="T274" id="Seg_5074" s="T273">pro.h:S</ta>
            <ta e="T275" id="Seg_5075" s="T274">pro.h:O</ta>
            <ta e="T280" id="Seg_5076" s="T279">v:pred</ta>
            <ta e="T281" id="Seg_5077" s="T280">pro.h:S</ta>
            <ta e="T283" id="Seg_5078" s="T282">v:pred</ta>
            <ta e="T287" id="Seg_5079" s="T286">0.2.h:S v:pred</ta>
            <ta e="T290" id="Seg_5080" s="T289">0.1.h:S v:pred</ta>
            <ta e="T291" id="Seg_5081" s="T290">0.3.h:S v:pred</ta>
            <ta e="T292" id="Seg_5082" s="T291">np.h:S</ta>
            <ta e="T294" id="Seg_5083" s="T293">v:pred</ta>
            <ta e="T300" id="Seg_5084" s="T299">0.2.h:S v:pred</ta>
            <ta e="T302" id="Seg_5085" s="T301">pro.h:S</ta>
            <ta e="T303" id="Seg_5086" s="T302">np:O</ta>
            <ta e="T304" id="Seg_5087" s="T303">v:pred</ta>
            <ta e="T305" id="Seg_5088" s="T304">pro.h:S</ta>
            <ta e="T307" id="Seg_5089" s="T306">pro.h:O</ta>
            <ta e="T310" id="Seg_5090" s="T309">v:pred</ta>
            <ta e="T311" id="Seg_5091" s="T310">s:adv</ta>
            <ta e="T312" id="Seg_5092" s="T311">np:O</ta>
            <ta e="T313" id="Seg_5093" s="T312">0.3.h:S v:pred</ta>
            <ta e="T316" id="Seg_5094" s="T313">s:temp</ta>
            <ta e="T321" id="Seg_5095" s="T320">0.3.h:S v:pred</ta>
            <ta e="T324" id="Seg_5096" s="T321">s:adv</ta>
            <ta e="T325" id="Seg_5097" s="T324">s:purp</ta>
            <ta e="T326" id="Seg_5098" s="T325">0.3.h:S v:pred</ta>
            <ta e="T328" id="Seg_5099" s="T326">s:adv</ta>
            <ta e="T331" id="Seg_5100" s="T330">np.h:S</ta>
            <ta e="T333" id="Seg_5101" s="T332">v:pred</ta>
            <ta e="T335" id="Seg_5102" s="T333">s:rel</ta>
            <ta e="T337" id="Seg_5103" s="T336">0.1.h:S v:pred</ta>
            <ta e="T339" id="Seg_5104" s="T338">0.1.h:S v:pred</ta>
            <ta e="T341" id="Seg_5105" s="T340">0.3.h:S v:pred</ta>
            <ta e="T342" id="Seg_5106" s="T341">pro.h:S</ta>
            <ta e="T344" id="Seg_5107" s="T342">s:purp</ta>
            <ta e="T347" id="Seg_5108" s="T346">adj:pred</ta>
            <ta e="T348" id="Seg_5109" s="T347">pro.h:S</ta>
            <ta e="T349" id="Seg_5110" s="T348">v:pred</ta>
            <ta e="T352" id="Seg_5111" s="T351">np.h:S</ta>
            <ta e="T354" id="Seg_5112" s="T353">v:pred</ta>
            <ta e="T357" id="Seg_5113" s="T356">np.h:S</ta>
            <ta e="T359" id="Seg_5114" s="T358">v:pred</ta>
            <ta e="T363" id="Seg_5115" s="T360">s:adv</ta>
            <ta e="T364" id="Seg_5116" s="T363">0.3.h:S v:pred</ta>
            <ta e="T367" id="Seg_5117" s="T366">np:S</ta>
            <ta e="T368" id="Seg_5118" s="T367">ptcl:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T3" id="Seg_5119" s="T2">new</ta>
            <ta e="T5" id="Seg_5120" s="T4">new</ta>
            <ta e="T8" id="Seg_5121" s="T7">accs-gen</ta>
            <ta e="T9" id="Seg_5122" s="T8">accs-gen</ta>
            <ta e="T10" id="Seg_5123" s="T9">accs-gen</ta>
            <ta e="T12" id="Seg_5124" s="T11">giv-inactive</ta>
            <ta e="T13" id="Seg_5125" s="T12">giv-active</ta>
            <ta e="T14" id="Seg_5126" s="T13">accs-inf</ta>
            <ta e="T15" id="Seg_5127" s="T14">giv-active</ta>
            <ta e="T16" id="Seg_5128" s="T15">new</ta>
            <ta e="T17" id="Seg_5129" s="T16">0.giv-active</ta>
            <ta e="T22" id="Seg_5130" s="T21">0.giv-active</ta>
            <ta e="T23" id="Seg_5131" s="T22">0.giv-active</ta>
            <ta e="T24" id="Seg_5132" s="T23">accs-inf</ta>
            <ta e="T25" id="Seg_5133" s="T24">giv-active</ta>
            <ta e="T30" id="Seg_5134" s="T29">0.giv-active</ta>
            <ta e="T32" id="Seg_5135" s="T31">new</ta>
            <ta e="T36" id="Seg_5136" s="T35">new</ta>
            <ta e="T37" id="Seg_5137" s="T36">accs-inf</ta>
            <ta e="T39" id="Seg_5138" s="T38">0.giv-active</ta>
            <ta e="T43" id="Seg_5139" s="T42">0.giv-active</ta>
            <ta e="T45" id="Seg_5140" s="T44">accs-inf</ta>
            <ta e="T46" id="Seg_5141" s="T45">0.giv-active</ta>
            <ta e="T47" id="Seg_5142" s="T46">0.giv-active</ta>
            <ta e="T48" id="Seg_5143" s="T47">giv-inactive</ta>
            <ta e="T53" id="Seg_5144" s="T52">giv-inactive</ta>
            <ta e="T54" id="Seg_5145" s="T53">new</ta>
            <ta e="T57" id="Seg_5146" s="T56">new</ta>
            <ta e="T58" id="Seg_5147" s="T57">accs-inf</ta>
            <ta e="T61" id="Seg_5148" s="T60">new</ta>
            <ta e="T63" id="Seg_5149" s="T62">0.giv-active</ta>
            <ta e="T65" id="Seg_5150" s="T64">giv-active</ta>
            <ta e="T66" id="Seg_5151" s="T65">0.giv-active</ta>
            <ta e="T71" id="Seg_5152" s="T70">new</ta>
            <ta e="T72" id="Seg_5153" s="T71">new</ta>
            <ta e="T77" id="Seg_5154" s="T76">accs-inf</ta>
            <ta e="T79" id="Seg_5155" s="T78">0.giv-inactive</ta>
            <ta e="T80" id="Seg_5156" s="T79">0.giv-active</ta>
            <ta e="T81" id="Seg_5157" s="T80">0.giv-active</ta>
            <ta e="T84" id="Seg_5158" s="T83">new</ta>
            <ta e="T85" id="Seg_5159" s="T84">giv-active</ta>
            <ta e="T86" id="Seg_5160" s="T85">accs-inf</ta>
            <ta e="T87" id="Seg_5161" s="T86">0.giv-active</ta>
            <ta e="T90" id="Seg_5162" s="T89">0.giv-active</ta>
            <ta e="T92" id="Seg_5163" s="T91">giv-active</ta>
            <ta e="T97" id="Seg_5164" s="T96">0.giv-active</ta>
            <ta e="T98" id="Seg_5165" s="T97">giv-active</ta>
            <ta e="T99" id="Seg_5166" s="T98">giv-inactive</ta>
            <ta e="T100" id="Seg_5167" s="T99">0.giv-active</ta>
            <ta e="T102" id="Seg_5168" s="T101">0.giv-active</ta>
            <ta e="T103" id="Seg_5169" s="T102">giv-inactive</ta>
            <ta e="T104" id="Seg_5170" s="T103">0.giv-active</ta>
            <ta e="T108" id="Seg_5171" s="T107">0.giv-active</ta>
            <ta e="T111" id="Seg_5172" s="T110">0.giv-active-Q</ta>
            <ta e="T112" id="Seg_5173" s="T111">0.giv-active 0.quot-sp</ta>
            <ta e="T113" id="Seg_5174" s="T112">accs-inf</ta>
            <ta e="T114" id="Seg_5175" s="T113">0.giv-active</ta>
            <ta e="T115" id="Seg_5176" s="T114">giv-active</ta>
            <ta e="T120" id="Seg_5177" s="T119">0.giv-active</ta>
            <ta e="T123" id="Seg_5178" s="T122">giv-inactive</ta>
            <ta e="T124" id="Seg_5179" s="T123">new</ta>
            <ta e="T125" id="Seg_5180" s="T124">0.giv-active</ta>
            <ta e="T128" id="Seg_5181" s="T127">0.giv-active</ta>
            <ta e="T133" id="Seg_5182" s="T132">new</ta>
            <ta e="T134" id="Seg_5183" s="T133">quot-sp</ta>
            <ta e="T137" id="Seg_5184" s="T136">0.giv-active-Q</ta>
            <ta e="T138" id="Seg_5185" s="T137">accs-gen-Q</ta>
            <ta e="T142" id="Seg_5186" s="T141">accs-gen-Q</ta>
            <ta e="T143" id="Seg_5187" s="T142">accs-gen-Q</ta>
            <ta e="T149" id="Seg_5188" s="T148">accs-gen-Q</ta>
            <ta e="T154" id="Seg_5189" s="T153">giv-active-Q</ta>
            <ta e="T159" id="Seg_5190" s="T158">0.giv-active-Q</ta>
            <ta e="T160" id="Seg_5191" s="T159">0.giv-active-Q</ta>
            <ta e="T161" id="Seg_5192" s="T160">giv-active-Q</ta>
            <ta e="T162" id="Seg_5193" s="T161">accs-inf-Q</ta>
            <ta e="T164" id="Seg_5194" s="T163">0.giv-active-Q</ta>
            <ta e="T167" id="Seg_5195" s="T166">0.giv-active 0.quot-sp</ta>
            <ta e="T168" id="Seg_5196" s="T167">giv-active</ta>
            <ta e="T173" id="Seg_5197" s="T172">new</ta>
            <ta e="T174" id="Seg_5198" s="T173">new</ta>
            <ta e="T175" id="Seg_5199" s="T174">accs-inf</ta>
            <ta e="T176" id="Seg_5200" s="T175">accs-inf</ta>
            <ta e="T181" id="Seg_5201" s="T180">giv-inactive</ta>
            <ta e="T185" id="Seg_5202" s="T184">new</ta>
            <ta e="T187" id="Seg_5203" s="T186">new</ta>
            <ta e="T189" id="Seg_5204" s="T188">giv-active</ta>
            <ta e="T190" id="Seg_5205" s="T189">accs-inf</ta>
            <ta e="T191" id="Seg_5206" s="T190">accs-inf</ta>
            <ta e="T194" id="Seg_5207" s="T193">giv-active</ta>
            <ta e="T198" id="Seg_5208" s="T197">giv-inactive</ta>
            <ta e="T199" id="Seg_5209" s="T198">giv-active</ta>
            <ta e="T201" id="Seg_5210" s="T200">giv-active-Q</ta>
            <ta e="T203" id="Seg_5211" s="T202">giv-active-Q</ta>
            <ta e="T204" id="Seg_5212" s="T203">giv-inactive-Q</ta>
            <ta e="T206" id="Seg_5213" s="T205">0.giv-active 0.quot-sp</ta>
            <ta e="T208" id="Seg_5214" s="T207">0.giv-inactive</ta>
            <ta e="T211" id="Seg_5215" s="T210">giv-inactive</ta>
            <ta e="T212" id="Seg_5216" s="T211">0.giv-active</ta>
            <ta e="T213" id="Seg_5217" s="T212">giv-active</ta>
            <ta e="T219" id="Seg_5218" s="T218">new</ta>
            <ta e="T224" id="Seg_5219" s="T223">0.giv-inactive</ta>
            <ta e="T228" id="Seg_5220" s="T227">giv-inactive</ta>
            <ta e="T234" id="Seg_5221" s="T233">new</ta>
            <ta e="T235" id="Seg_5222" s="T234">accs-inf</ta>
            <ta e="T236" id="Seg_5223" s="T235">new</ta>
            <ta e="T237" id="Seg_5224" s="T236">0.giv-active</ta>
            <ta e="T239" id="Seg_5225" s="T238">giv-active</ta>
            <ta e="T240" id="Seg_5226" s="T239">new</ta>
            <ta e="T243" id="Seg_5227" s="T242">giv-inactive</ta>
            <ta e="T244" id="Seg_5228" s="T243">giv-inactive</ta>
            <ta e="T245" id="Seg_5229" s="T244">giv-active-Q</ta>
            <ta e="T248" id="Seg_5230" s="T247">giv-active-Q</ta>
            <ta e="T250" id="Seg_5231" s="T249">giv-active-Q</ta>
            <ta e="T251" id="Seg_5232" s="T250">giv-active-Q</ta>
            <ta e="T252" id="Seg_5233" s="T251">giv-active-Q</ta>
            <ta e="T253" id="Seg_5234" s="T252">0.giv-active-Q</ta>
            <ta e="T255" id="Seg_5235" s="T254">0.giv-active 0.quot-sp</ta>
            <ta e="T256" id="Seg_5236" s="T255">giv-active</ta>
            <ta e="T257" id="Seg_5237" s="T256">quot-sp</ta>
            <ta e="T258" id="Seg_5238" s="T257">giv-inactive-Q</ta>
            <ta e="T259" id="Seg_5239" s="T258">0.giv-active-Q</ta>
            <ta e="T260" id="Seg_5240" s="T259">giv-active-Q</ta>
            <ta e="T264" id="Seg_5241" s="T263">giv-active-Q</ta>
            <ta e="T266" id="Seg_5242" s="T265">0.giv-active-Q</ta>
            <ta e="T267" id="Seg_5243" s="T266">accs-inf-Q</ta>
            <ta e="T268" id="Seg_5244" s="T267">0.giv-active-Q</ta>
            <ta e="T269" id="Seg_5245" s="T268">giv-active</ta>
            <ta e="T270" id="Seg_5246" s="T269">giv-active</ta>
            <ta e="T272" id="Seg_5247" s="T271">giv-active</ta>
            <ta e="T273" id="Seg_5248" s="T272">quot-sp</ta>
            <ta e="T274" id="Seg_5249" s="T273">giv-active-Q</ta>
            <ta e="T275" id="Seg_5250" s="T274">giv-active-Q</ta>
            <ta e="T281" id="Seg_5251" s="T280">giv-active-Q</ta>
            <ta e="T285" id="Seg_5252" s="T284">giv-inactive-Q</ta>
            <ta e="T287" id="Seg_5253" s="T286">0.giv-active-Q</ta>
            <ta e="T290" id="Seg_5254" s="T289">0.giv-active-Q</ta>
            <ta e="T291" id="Seg_5255" s="T290">0.giv-active 0.quot-sp</ta>
            <ta e="T292" id="Seg_5256" s="T291">giv-inactive</ta>
            <ta e="T294" id="Seg_5257" s="T293">quot-sp</ta>
            <ta e="T300" id="Seg_5258" s="T299">0.giv-inactive-Q</ta>
            <ta e="T302" id="Seg_5259" s="T301">giv-inactive-Q</ta>
            <ta e="T303" id="Seg_5260" s="T302">new-Q</ta>
            <ta e="T305" id="Seg_5261" s="T304">new-Q</ta>
            <ta e="T307" id="Seg_5262" s="T306">giv-active-Q</ta>
            <ta e="T311" id="Seg_5263" s="T310">0.giv-active 0.quot-sp</ta>
            <ta e="T312" id="Seg_5264" s="T311">giv-active</ta>
            <ta e="T313" id="Seg_5265" s="T312">0.giv-active</ta>
            <ta e="T314" id="Seg_5266" s="T313">giv-active</ta>
            <ta e="T315" id="Seg_5267" s="T314">0.giv-active</ta>
            <ta e="T321" id="Seg_5268" s="T320">0.giv-active</ta>
            <ta e="T323" id="Seg_5269" s="T322">0.giv-active</ta>
            <ta e="T325" id="Seg_5270" s="T324">0.giv-active</ta>
            <ta e="T326" id="Seg_5271" s="T325">0.giv-active</ta>
            <ta e="T327" id="Seg_5272" s="T326">giv-inactive</ta>
            <ta e="T328" id="Seg_5273" s="T327">0.giv-active</ta>
            <ta e="T331" id="Seg_5274" s="T330">giv-inactive</ta>
            <ta e="T334" id="Seg_5275" s="T333">giv-active-Q</ta>
            <ta e="T336" id="Seg_5276" s="T335">giv-inactive-Q</ta>
            <ta e="T337" id="Seg_5277" s="T336">0.giv-active-Q</ta>
            <ta e="T339" id="Seg_5278" s="T338">0.accs-aggr-Q</ta>
            <ta e="T340" id="Seg_5279" s="T339">giv-inactive</ta>
            <ta e="T341" id="Seg_5280" s="T340">0.giv-active 0.quot-sp</ta>
            <ta e="T342" id="Seg_5281" s="T341">giv-active-Q</ta>
            <ta e="T344" id="Seg_5282" s="T343">0.giv-active-Q</ta>
            <ta e="T347" id="Seg_5283" s="T346">giv-inactive-Q</ta>
            <ta e="T348" id="Seg_5284" s="T347">giv-inactive-Q</ta>
            <ta e="T350" id="Seg_5285" s="T349">giv-inactive-Q</ta>
            <ta e="T352" id="Seg_5286" s="T351">giv-inactive</ta>
            <ta e="T357" id="Seg_5287" s="T356">giv-inactive</ta>
            <ta e="T363" id="Seg_5288" s="T362">0.accs-aggr</ta>
            <ta e="T364" id="Seg_5289" s="T363">0.giv-active</ta>
            <ta e="T365" id="Seg_5290" s="T364">giv-inactive</ta>
            <ta e="T367" id="Seg_5291" s="T366">accs-sit</ta>
         </annotation>
         <annotation name="Top" tierref="Top">
            <ta e="T1" id="Seg_5292" s="T0">top.int.concr</ta>
            <ta e="T7" id="Seg_5293" s="T6">top.int.concr</ta>
            <ta e="T12" id="Seg_5294" s="T10">top.int.concr</ta>
            <ta e="T21" id="Seg_5295" s="T18">top.int.concr</ta>
            <ta e="T24" id="Seg_5296" s="T23">top.int.concr</ta>
            <ta e="T29" id="Seg_5297" s="T27">top.int.concr</ta>
            <ta e="T39" id="Seg_5298" s="T38">0.top.int.concr</ta>
            <ta e="T43" id="Seg_5299" s="T39">top.int.concr</ta>
            <ta e="T48" id="Seg_5300" s="T47">top.int.concr</ta>
            <ta e="T53" id="Seg_5301" s="T52">top.int.concr</ta>
            <ta e="T66" id="Seg_5302" s="T65">0.top.int.concr</ta>
            <ta e="T67" id="Seg_5303" s="T66">0.top.int.concr</ta>
            <ta e="T70" id="Seg_5304" s="T68">top.int.concr</ta>
            <ta e="T79" id="Seg_5305" s="T78">0.top.int.concr</ta>
            <ta e="T81" id="Seg_5306" s="T80">0.top.int.concr</ta>
            <ta e="T90" id="Seg_5307" s="T89">0.top.int.abstr</ta>
            <ta e="T92" id="Seg_5308" s="T90">top.int.concr</ta>
            <ta e="T97" id="Seg_5309" s="T96">0.top.int.concr</ta>
            <ta e="T98" id="Seg_5310" s="T97">top.int.concr.contr</ta>
            <ta e="T108" id="Seg_5311" s="T107">0.top.int.concr</ta>
            <ta e="T111" id="Seg_5312" s="T110">0.top.int.concr</ta>
            <ta e="T114" id="Seg_5313" s="T113">0.top.int.concr</ta>
            <ta e="T115" id="Seg_5314" s="T114">top.int.concr</ta>
            <ta e="T128" id="Seg_5315" s="T126">top.int.concr</ta>
            <ta e="T145" id="Seg_5316" s="T144">0.top.int.abstr</ta>
            <ta e="T150" id="Seg_5317" s="T149">0.top.int.abstr</ta>
            <ta e="T164" id="Seg_5318" s="T163">0.top.int.concr</ta>
            <ta e="T168" id="Seg_5319" s="T167">top.int.concr</ta>
            <ta e="T173" id="Seg_5320" s="T172">top.int.concr</ta>
            <ta e="T174" id="Seg_5321" s="T173">top.int.concr</ta>
            <ta e="T181" id="Seg_5322" s="T179">top.int.concr</ta>
            <ta e="T185" id="Seg_5323" s="T183">top.int.concr</ta>
            <ta e="T190" id="Seg_5324" s="T188">top.int.concr</ta>
            <ta e="T200" id="Seg_5325" s="T199">0.top.int.abstr.</ta>
            <ta e="T213" id="Seg_5326" s="T212">0.top.int.concr</ta>
            <ta e="T215" id="Seg_5327" s="T213">top.int.concr</ta>
            <ta e="T224" id="Seg_5328" s="T223">0.top.int.concr</ta>
            <ta e="T228" id="Seg_5329" s="T225">top.int.concr.contr</ta>
            <ta e="T238" id="Seg_5330" s="T237">0.top.int.abstr.</ta>
            <ta e="T239" id="Seg_5331" s="T238">top.int.concr</ta>
            <ta e="T245" id="Seg_5332" s="T244">top.int.concr</ta>
            <ta e="T251" id="Seg_5333" s="T249">top.int.concr</ta>
            <ta e="T256" id="Seg_5334" s="T255">top.int.concr</ta>
            <ta e="T260" id="Seg_5335" s="T259">top.int.concr</ta>
            <ta e="T266" id="Seg_5336" s="T265">top.int.concr</ta>
            <ta e="T269" id="Seg_5337" s="T268">top.int.concr</ta>
            <ta e="T272" id="Seg_5338" s="T271">top.int.concr</ta>
            <ta e="T274" id="Seg_5339" s="T273">top.int.concr</ta>
            <ta e="T290" id="Seg_5340" s="T289">0.top.int.concr</ta>
            <ta e="T292" id="Seg_5341" s="T291">top.int.concr</ta>
            <ta e="T300" id="Seg_5342" s="T299">0.top.int.concr</ta>
            <ta e="T302" id="Seg_5343" s="T301">top.int.concr</ta>
            <ta e="T313" id="Seg_5344" s="T312">0.top.int.concr</ta>
            <ta e="T316" id="Seg_5345" s="T313">top.int.concr</ta>
            <ta e="T324" id="Seg_5346" s="T321">top.int.concr</ta>
            <ta e="T333" id="Seg_5347" s="T332">0.top.int.abstr.</ta>
            <ta e="T337" id="Seg_5348" s="T336">0.top.int.concr</ta>
            <ta e="T339" id="Seg_5349" s="T338">0.top.int.concr</ta>
            <ta e="T341" id="Seg_5350" s="T340">0.top.int.concr</ta>
            <ta e="T342" id="Seg_5351" s="T341">top.int.concr</ta>
            <ta e="T352" id="Seg_5352" s="T351">top.int.concr</ta>
            <ta e="T364" id="Seg_5353" s="T363">0.top.int.concr</ta>
         </annotation>
         <annotation name="Foc" tierref="Foc">
            <ta e="T6" id="Seg_5354" s="T0">foc.wid</ta>
            <ta e="T10" id="Seg_5355" s="T6">foc.wid</ta>
            <ta e="T16" id="Seg_5356" s="T14">foc.nar</ta>
            <ta e="T23" id="Seg_5357" s="T21">foc.int</ta>
            <ta e="T27" id="Seg_5358" s="T25">foc.nar</ta>
            <ta e="T32" id="Seg_5359" s="T30">foc.nar</ta>
            <ta e="T39" id="Seg_5360" s="T34">foc.int</ta>
            <ta e="T47" id="Seg_5361" s="T43">foc.int</ta>
            <ta e="T51" id="Seg_5362" s="T48">foc.nar</ta>
            <ta e="T58" id="Seg_5363" s="T56">foc.nar</ta>
            <ta e="T62" id="Seg_5364" s="T60">foc.nar</ta>
            <ta e="T66" id="Seg_5365" s="T63">foc.int</ta>
            <ta e="T68" id="Seg_5366" s="T66">foc.nar</ta>
            <ta e="T73" id="Seg_5367" s="T70">foc.wid</ta>
            <ta e="T79" id="Seg_5368" s="T73">foc.int</ta>
            <ta e="T81" id="Seg_5369" s="T79">foc.int</ta>
            <ta e="T90" id="Seg_5370" s="T81">foc.wid</ta>
            <ta e="T93" id="Seg_5371" s="T92">foc.int</ta>
            <ta e="T96" id="Seg_5372" s="T94">foc.nar</ta>
            <ta e="T106" id="Seg_5373" s="T101">foc.int</ta>
            <ta e="T108" id="Seg_5374" s="T106">foc.int</ta>
            <ta e="T110" id="Seg_5375" s="T109">foc.nar</ta>
            <ta e="T114" id="Seg_5376" s="T111">foc.int</ta>
            <ta e="T126" id="Seg_5377" s="T119">foc.int</ta>
            <ta e="T134" id="Seg_5378" s="T128">foc.wid</ta>
            <ta e="T136" id="Seg_5379" s="T135">foc.nar</ta>
            <ta e="T139" id="Seg_5380" s="T137">foc.nar</ta>
            <ta e="T142" id="Seg_5381" s="T140">foc.nar</ta>
            <ta e="T143" id="Seg_5382" s="T142">foc.nar</ta>
            <ta e="T149" id="Seg_5383" s="T145">foc.nar</ta>
            <ta e="T159" id="Seg_5384" s="T152">foc.int</ta>
            <ta e="T164" id="Seg_5385" s="T159">foc.int</ta>
            <ta e="T170" id="Seg_5386" s="T168">foc.nar</ta>
            <ta e="T177" id="Seg_5387" s="T176">foc.nar</ta>
            <ta e="T183" id="Seg_5388" s="T181">foc.int</ta>
            <ta e="T188" id="Seg_5389" s="T185">foc.int</ta>
            <ta e="T194" id="Seg_5390" s="T191">foc.int</ta>
            <ta e="T200" id="Seg_5391" s="T198">foc.int</ta>
            <ta e="T201" id="Seg_5392" s="T200">foc.nar</ta>
            <ta e="T211" id="Seg_5393" s="T209">foc.nar</ta>
            <ta e="T220" id="Seg_5394" s="T215">foc.wid</ta>
            <ta e="T225" id="Seg_5395" s="T221">foc.int</ta>
            <ta e="T230" id="Seg_5396" s="T228">foc.nar</ta>
            <ta e="T238" id="Seg_5397" s="T232">foc.wid</ta>
            <ta e="T244" id="Seg_5398" s="T239">foc.wid</ta>
            <ta e="T249" id="Seg_5399" s="T245">foc.int</ta>
            <ta e="T254" id="Seg_5400" s="T252">foc.nar</ta>
            <ta e="T257" id="Seg_5401" s="T256">foc.int</ta>
            <ta e="T259" id="Seg_5402" s="T258">foc.int</ta>
            <ta e="T262" id="Seg_5403" s="T260">foc.nar</ta>
            <ta e="T265" id="Seg_5404" s="T264">foc.int</ta>
            <ta e="T268" id="Seg_5405" s="T266">foc.int</ta>
            <ta e="T271" id="Seg_5406" s="T269">foc.int</ta>
            <ta e="T273" id="Seg_5407" s="T272">foc.int</ta>
            <ta e="T278" id="Seg_5408" s="T276">foc.nar</ta>
            <ta e="T283" id="Seg_5409" s="T281">foc.int</ta>
            <ta e="T287" id="Seg_5410" s="T284">foc.contr</ta>
            <ta e="T290" id="Seg_5411" s="T289">foc.nar</ta>
            <ta e="T294" id="Seg_5412" s="T292">foc.int</ta>
            <ta e="T301" id="Seg_5413" s="T296">foc.int</ta>
            <ta e="T303" id="Seg_5414" s="T302">foc.nar</ta>
            <ta e="T309" id="Seg_5415" s="T307">foc.nar</ta>
            <ta e="T313" id="Seg_5416" s="T311">foc.int</ta>
            <ta e="T320" id="Seg_5417" s="T317">foc.nar</ta>
            <ta e="T326" id="Seg_5418" s="T324">foc.int</ta>
            <ta e="T333" id="Seg_5419" s="T331">foc.int</ta>
            <ta e="T337" id="Seg_5420" s="T336">foc.nar</ta>
            <ta e="T338" id="Seg_5421" s="T337">foc.nar</ta>
            <ta e="T341" id="Seg_5422" s="T339">foc.int</ta>
            <ta e="T347" id="Seg_5423" s="T344">foc.nar</ta>
            <ta e="T348" id="Seg_5424" s="T347">foc.nar</ta>
            <ta e="T354" id="Seg_5425" s="T352">foc.int</ta>
            <ta e="T359" id="Seg_5426" s="T358">foc.ver</ta>
            <ta e="T365" id="Seg_5427" s="T360">foc.int</ta>
            <ta e="T368" id="Seg_5428" s="T365">foc.wid</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_5429" s="T3">RUS:cult</ta>
            <ta e="T5" id="Seg_5430" s="T4">RUS:cult</ta>
            <ta e="T9" id="Seg_5431" s="T8">RUS:cult</ta>
            <ta e="T10" id="Seg_5432" s="T9">RUS:cult</ta>
            <ta e="T12" id="Seg_5433" s="T11">RUS:cult</ta>
            <ta e="T15" id="Seg_5434" s="T14">RUS:cult</ta>
            <ta e="T71" id="Seg_5435" s="T70">EV:cult</ta>
            <ta e="T83" id="Seg_5436" s="T82">EV:gram (INTNS)</ta>
            <ta e="T103" id="Seg_5437" s="T102">EV:cult</ta>
            <ta e="T109" id="Seg_5438" s="T108">RUS:gram</ta>
            <ta e="T139" id="Seg_5439" s="T138">RUS:gram</ta>
            <ta e="T144" id="Seg_5440" s="T143">RUS:gram</ta>
            <ta e="T149" id="Seg_5441" s="T148">RUS:core</ta>
            <ta e="T171" id="Seg_5442" s="T170">RUS:core</ta>
            <ta e="T231" id="Seg_5443" s="T230">RUS:core</ta>
            <ta e="T234" id="Seg_5444" s="T233">RUS:cult</ta>
            <ta e="T243" id="Seg_5445" s="T242">RUS:cult</ta>
            <ta e="T269" id="Seg_5446" s="T268">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T4" id="Seg_5447" s="T3">fortition Vsub</ta>
            <ta e="T5" id="Seg_5448" s="T4">medVins Vsub</ta>
            <ta e="T9" id="Seg_5449" s="T8">fortition Vsub</ta>
            <ta e="T10" id="Seg_5450" s="T9">Vsub Vsub fortition Vsub</ta>
            <ta e="T12" id="Seg_5451" s="T11">medVins Vsub</ta>
            <ta e="T15" id="Seg_5452" s="T14">Vsub Vsub fortition Vsub</ta>
            <ta e="T71" id="Seg_5453" s="T70">Csub lenition</ta>
            <ta e="T103" id="Seg_5454" s="T102">Csub lenition</ta>
            <ta e="T149" id="Seg_5455" s="T148">Vsub Vsub fortition</ta>
            <ta e="T171" id="Seg_5456" s="T170">Vsub Vsub fortition</ta>
            <ta e="T231" id="Seg_5457" s="T230">Vsub Vsub fortition</ta>
            <ta e="T234" id="Seg_5458" s="T233">Vsub finCdel</ta>
            <ta e="T243" id="Seg_5459" s="T242">medVins Vsub</ta>
            <ta e="T269" id="Seg_5460" s="T268">medVins Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T4" id="Seg_5461" s="T3">dir:bare</ta>
            <ta e="T5" id="Seg_5462" s="T4">dir:infl</ta>
            <ta e="T9" id="Seg_5463" s="T8">dir:infl</ta>
            <ta e="T10" id="Seg_5464" s="T9">dir:infl</ta>
            <ta e="T12" id="Seg_5465" s="T11">dir:bare</ta>
            <ta e="T15" id="Seg_5466" s="T14">dir:bare</ta>
            <ta e="T71" id="Seg_5467" s="T70">dir:bare</ta>
            <ta e="T103" id="Seg_5468" s="T102">dir:infl</ta>
            <ta e="T109" id="Seg_5469" s="T108">dir:bare</ta>
            <ta e="T139" id="Seg_5470" s="T138">dir:bare</ta>
            <ta e="T144" id="Seg_5471" s="T143">dir:bare</ta>
            <ta e="T149" id="Seg_5472" s="T148">dir:bare</ta>
            <ta e="T171" id="Seg_5473" s="T170">dir:bare</ta>
            <ta e="T231" id="Seg_5474" s="T230">dir:bare</ta>
            <ta e="T234" id="Seg_5475" s="T233">dir:infl</ta>
            <ta e="T243" id="Seg_5476" s="T242">dir:bare</ta>
            <ta e="T269" id="Seg_5477" s="T268">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_5478" s="T0">Long ago there lived a Nenets prince, who had a son.</ta>
            <ta e="T10" id="Seg_5479" s="T6">In winter the Nenets people hunt belugas in the sea.</ta>
            <ta e="T18" id="Seg_5480" s="T10">That prince goes over the ice of the sea to cut an ice hole for [catching] belugas.</ta>
            <ta e="T27" id="Seg_5481" s="T18">Later, as he wanted to come back, he saw that his ice floe tore away [from the shore].</ta>
            <ta e="T39" id="Seg_5482" s="T27">For three days he searches for a place to leave the ice floe, in his distress he kills one of his three reindeers and eats its raw meat.</ta>
            <ta e="T47" id="Seg_5483" s="T39">After unsuccessfully trying to leave the ice floe again, he kills the other two reindeers.</ta>
            <ta e="T52" id="Seg_5484" s="T47">His ice floe turns as small as a shaman's drum.</ta>
            <ta e="T63" id="Seg_5485" s="T52">Somehow he drifts to the shore of an island, he gets stuck at some trees.</ta>
            <ta e="T68" id="Seg_5486" s="T63">He went out to the island, there were a lot of trees.</ta>
            <ta e="T73" id="Seg_5487" s="T68">Somewhere the sound of an axe is heard.</ta>
            <ta e="T79" id="Seg_5488" s="T73">With the last of his strength he went there.</ta>
            <ta e="T90" id="Seg_5489" s="T79">He came there and saw a very small girl standing with her back towards him and chopping wood.</ta>
            <ta e="T97" id="Seg_5490" s="T90">That girl looks around, apparently it had only one eye.</ta>
            <ta e="T106" id="Seg_5491" s="T97">Having seen the human, she startles, throws down the axe and runs away.</ta>
            <ta e="T114" id="Seg_5492" s="T106">She runs away and tells her people "such a thing I have seen".</ta>
            <ta e="T126" id="Seg_5493" s="T114">Some of those people come, put that tired and starving human on a fur, raise him and bring him there.</ta>
            <ta e="T134" id="Seg_5494" s="T126">As they bring him there, an old man with white hair like snow says:</ta>
            <ta e="T167" id="Seg_5495" s="T134">"Well, why are you wondering, in tales, in old stories, in writings it is also said that there is such a two-eyed people, now, feed the starving human without giving him too much, he must be needy since he came here from his land", he says.</ta>
            <ta e="T179" id="Seg_5496" s="T167">These were very rich people, apparently, foxes and polar foxes came themselves to their doors.</ta>
            <ta e="T183" id="Seg_5497" s="T179">That human begins to live there.</ta>
            <ta e="T194" id="Seg_5498" s="T183">There were, apparently, two daughters in the family, where he started to live, the father bestowed his elder daughter in marriage to the boy.</ta>
            <ta e="T200" id="Seg_5499" s="T194">The girl, who saw that human first, envies her elder sister: </ta>
            <ta e="T206" id="Seg_5500" s="T200">"It's me who should marry the foundling", she says crying.</ta>
            <ta e="T213" id="Seg_5501" s="T206">Without listening to her they give the elder daughter to the boy eitherway.</ta>
            <ta e="T220" id="Seg_5502" s="T213">One year later a two-eyed boy is born.</ta>
            <ta e="T225" id="Seg_5503" s="T220">They live like that for a long time.</ta>
            <ta e="T232" id="Seg_5504" s="T225">This one-eyed people was apparently quite numerous.</ta>
            <ta e="T238" id="Seg_5505" s="T232">Once their czar comes on his ship to gather taxes.</ta>
            <ta e="T244" id="Seg_5506" s="T238">One human of his comes to that prince's father-in-law:</ta>
            <ta e="T255" id="Seg_5507" s="T244">"They say, you have a two-eyed son-in-law, the czar wants to see him", he says.</ta>
            <ta e="T257" id="Seg_5508" s="T255">The father-in-law says:</ta>
            <ta e="T268" id="Seg_5509" s="T257">"Ducky, go, the czar will just look at you, go, if you don't go, he will cut off our heads."</ta>
            <ta e="T271" id="Seg_5510" s="T268">The prince goes to the czar.</ta>
            <ta e="T273" id="Seg_5511" s="T271">That one says:</ta>
            <ta e="T288" id="Seg_5512" s="T273">"I will make a rich man out of you, will you stay here or go back to your country?"</ta>
            <ta e="T289" id="Seg_5513" s="T288">On that:</ta>
            <ta e="T291" id="Seg_5514" s="T289">"I will go back", he said.</ta>
            <ta e="T294" id="Seg_5515" s="T291">The czar said on that:</ta>
            <ta e="T301" id="Seg_5516" s="T294">"It is far away, you won't get there as fast as the water had brought you.</ta>
            <ta e="T313" id="Seg_5517" s="T301">I will give you a letter, anyone, no matter who, will bring you without any delay", he says and gives him the letter.</ta>
            <ta e="T321" id="Seg_5518" s="T313">Having received that letter he still lives several years there.</ta>
            <ta e="T328" id="Seg_5519" s="T321">Having lived so for a while, he prepares to go together with his wife.</ta>
            <ta e="T333" id="Seg_5520" s="T328">The younger daughter starts crying:</ta>
            <ta e="T339" id="Seg_5521" s="T333">"I won't let my foundling go alone, I will come along with him."</ta>
            <ta e="T341" id="Seg_5522" s="T339">She says to her elder sister:</ta>
            <ta e="T347" id="Seg_5523" s="T341">"You already have your two-eyed child to remember hin.</ta>
            <ta e="T351" id="Seg_5524" s="T347">I will go with him."</ta>
            <ta e="T354" id="Seg_5525" s="T351">Her father agrees and allows it.</ta>
            <ta e="T359" id="Seg_5526" s="T354">So the younger comes along.</ta>
            <ta e="T365" id="Seg_5527" s="T359">After travelling for a very long time, they come to their country.</ta>
            <ta e="T368" id="Seg_5528" s="T365">There is such a story.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_5529" s="T0">Vor langer Zeit lebte ein nenzischer Fürst, der hatte einen Sohn.</ta>
            <ta e="T10" id="Seg_5530" s="T6">Im Winter jagen die Nenzen im Meer Belugas.</ta>
            <ta e="T18" id="Seg_5531" s="T10">Dieser Fürst geht über das Eis des Meeres, um ein Eisloch für [die Jagd auf] Belugas zu schlagen.</ta>
            <ta e="T27" id="Seg_5532" s="T18">Danach, als er zurückkehren wollte, wurde seine Eisscholle [vom Ufer] abgerissen.</ta>
            <ta e="T39" id="Seg_5533" s="T27">Drei Tage land sucht er eine Stelle, wo er die Eisscholle verlassen könnte, in seiner Not isst er das rohe Fleisch eines seiner drei Rentiere.</ta>
            <ta e="T47" id="Seg_5534" s="T39">Er versucht wieder erfolglos wegzugehen und tötet die anderen beiden Rentiere.</ta>
            <ta e="T52" id="Seg_5535" s="T47">Seine Eisscholle wird so klein wie eine Schamanentrommel.</ta>
            <ta e="T63" id="Seg_5536" s="T52">Er kommt irgendwie durch einen Sturm ans Ufer einer Insel, er bleibt an Bäumen hängen.</ta>
            <ta e="T68" id="Seg_5537" s="T63">Er ging auf die Insel, dort waren sehr viele Bäume.</ta>
            <ta e="T73" id="Seg_5538" s="T68">Irgendwo ist das Geräusch eines Beils zu hören.</ta>
            <ta e="T79" id="Seg_5539" s="T73">Mit letzter Kraft ging er dorthin.</ta>
            <ta e="T90" id="Seg_5540" s="T79">Er kam dorthin und sah, dass dort ein ganz kleines Mädchen mit dem Rücken zu ihm steht, und Holz hackt.</ta>
            <ta e="T97" id="Seg_5541" s="T90">Dieses Mädchen schaut sich um, es hatte offenbar nur ein Auge.</ta>
            <ta e="T106" id="Seg_5542" s="T97">Als es den Menschen sieht, erschrickt das Mädchen, wirft das Beil weg und läuft davon.</ta>
            <ta e="T114" id="Seg_5543" s="T106">Es läuft davon und erzählt seinen Leuten "so etwas habe ich gesehen".</ta>
            <ta e="T126" id="Seg_5544" s="T114">Da kommen einige dieser Leute, legen den ermüdeten und ausgehungerten Menschen auf einen Fell und bringen ihn her.</ta>
            <ta e="T134" id="Seg_5545" s="T126">Während sie ihn wegbringen, sagt ein alter Mann mit Haaren weiß wie Schnee:</ta>
            <ta e="T167" id="Seg_5546" s="T134">"Nun, was wundert ihr euch, auch in den Märchen, in den alten Erzählungen, in den Aufzeichnungen gibt es so ein zweiäugiges Volk, nun aber, bewirtet den ausgehungerten Menschen, ohne ihm zu viel zu geben, er ist wohl bedürftig, wenn er sich hierher verirrt hat", sagt er.</ta>
            <ta e="T179" id="Seg_5547" s="T167">Das war wohl ein sehr reiches Volk, Füchse und Polarfüchse kamen offenbar von selbst an die Türen ihrer Häuser.</ta>
            <ta e="T183" id="Seg_5548" s="T179">Der Mensch fängt an dort zu leben.</ta>
            <ta e="T194" id="Seg_5549" s="T183">In der Familie, wo er anfing zu leben, gab es offenbar zwei Töchter, die ältere davon gibt der Vater dem Jungen zur Frau.</ta>
            <ta e="T200" id="Seg_5550" s="T194">Da beneidet das Mädchen, das ihn zuerst gesehen hatte, ihre ältere Schwester:</ta>
            <ta e="T206" id="Seg_5551" s="T200">"Ich sollte mein Findelkind heiraten", sagt sie und weint.</ta>
            <ta e="T213" id="Seg_5552" s="T206">Darauf nicht hörend geben sie die ältere Tochter dennoch dem Jungen.</ta>
            <ta e="T220" id="Seg_5553" s="T213">Nach einem Jahr wird ein zweiäugiger Junge geboren.</ta>
            <ta e="T225" id="Seg_5554" s="T220">Und so leben sie sehr lange.</ta>
            <ta e="T232" id="Seg_5555" s="T225">Diese Einäugigen waren offenbar ein sehr zahlreiches Volk.</ta>
            <ta e="T238" id="Seg_5556" s="T232">Einmal kommt ihr Zar mit seinem Schiff, um Steuern einzutreiben.</ta>
            <ta e="T244" id="Seg_5557" s="T238">Es kommt von ihm ein Mensch zum Schwiegervater dieses Fürsten:</ta>
            <ta e="T255" id="Seg_5558" s="T244">"Du hast einen zweiäugigen Schwiegersohn, sagt man, der Zar möchte diesen deinen sehen", sagt er.</ta>
            <ta e="T257" id="Seg_5559" s="T255">Der Schwiegervater sagt:</ta>
            <ta e="T268" id="Seg_5560" s="T257">"Mein Liebling, geh nur, der Zar möchte dich nur anschauen, geh, wenn du nicht gehst, dann schlägt er unsere Köpfe ab."</ta>
            <ta e="T271" id="Seg_5561" s="T268">Der Fürst geht zum Zaren.</ta>
            <ta e="T273" id="Seg_5562" s="T271">Jener sagt:</ta>
            <ta e="T288" id="Seg_5563" s="T273">"Ich mache dich hier zu einem reichen Menschen, bleibst du hier oder kehrst du in dein Land zurück?"</ta>
            <ta e="T289" id="Seg_5564" s="T288">Darauf:</ta>
            <ta e="T291" id="Seg_5565" s="T289">"Ich gehe zurück", sagte er.</ta>
            <ta e="T294" id="Seg_5566" s="T291">Der Zar sagte darauf:</ta>
            <ta e="T301" id="Seg_5567" s="T294">"Das Land ist fern, du gelangst nicht so schnell dorthin, wie das Wasser dich gebracht hat.</ta>
            <ta e="T313" id="Seg_5568" s="T301">Ich gebe dir einen Brief; wer auch immer es ist, er wird dich ohne Verzögerung bringen", sagt er und gibt ihm den Brief.</ta>
            <ta e="T321" id="Seg_5569" s="T313">Nachdem er den Brief bekommen hatte, lebt er dennoch einige Jahre dort.</ta>
            <ta e="T328" id="Seg_5570" s="T321">Nachdem er so lebte, bereitet er sich vor zu gehen und seine Frau mitzunehmen.</ta>
            <ta e="T333" id="Seg_5571" s="T328">Darauf weint die jüngere Tochter:</ta>
            <ta e="T339" id="Seg_5572" s="T333">"Ich weiche dem Menschen, den ich gefunden habe, nicht von der Seite, ich gehe mit."</ta>
            <ta e="T341" id="Seg_5573" s="T339">Sie sagt zu ihrer älteren Schwester:</ta>
            <ta e="T347" id="Seg_5574" s="T341">"Du hast ja schon das zweiäugige Kind, um dich an ihn zu erinnern.</ta>
            <ta e="T351" id="Seg_5575" s="T347">Ich gehe mit ihm mit."</ta>
            <ta e="T354" id="Seg_5576" s="T351">Ihr Vater willigt ein und erlaubt es.</ta>
            <ta e="T359" id="Seg_5577" s="T354">So geht die kleine Tochter mit.</ta>
            <ta e="T365" id="Seg_5578" s="T359">Sie reisen sehr lange und kommen in ihr Land.</ta>
            <ta e="T368" id="Seg_5579" s="T365">So eine Erzählung gibt es.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T6" id="Seg_5580" s="T0">В старину жил один ненецкий князь с сыном.</ta>
            <ta e="T10" id="Seg_5581" s="T6">Зимой ненцы на море ловили белуху. </ta>
            <ta e="T18" id="Seg_5582" s="T10">Тот князь по льду моря пошел прорубь долбить для [ловли] белухи.</ta>
            <ta e="T27" id="Seg_5583" s="T18">Затем, когда захотел вернуться, льдина его оторвалась [от берега].</ta>
            <ta e="T39" id="Seg_5584" s="T27">Три дня ищет, где бы сойти на берег, до крайности дойдя, одного из трех оленей забивает и сырым съедает.</ta>
            <ta e="T47" id="Seg_5585" s="T39">Затем снова пытается сойти со льдины, но и двух оставшихся оленей забивает.</ta>
            <ta e="T52" id="Seg_5586" s="T47">Льдина его, [ломаясь], с бубен шамана становится.</ta>
            <ta e="T63" id="Seg_5587" s="T52">Прибивает ее как-то волной к берегу острова и притыкает к деревьям.</ta>
            <ta e="T68" id="Seg_5588" s="T63">Выбрался на остров, а на нем много деревьев растет.</ta>
            <ta e="T73" id="Seg_5589" s="T68">Где-то слышится стух топора.</ta>
            <ta e="T79" id="Seg_5590" s="T73">С последней силой идет туда.</ta>
            <ta e="T90" id="Seg_5591" s="T79">Подойдя, увидел: одна небольшая девушка, стоя к нему спиной, рубит дерево.</ta>
            <ta e="T97" id="Seg_5592" s="T90">Эта девушка олядывается — оказывается, у ней единственный глаз.</ta>
            <ta e="T106" id="Seg_5593" s="T97">Девушка, увидев человека, испугется и, бросив топор, убегает.</ta>
            <ta e="T114" id="Seg_5594" s="T106">Прибегает и людям своим рассказывает: "вот такое видела."</ta>
            <ta e="T126" id="Seg_5595" s="T114">Тогда несколько человек [туда] пойдут и приносят, положив на оленью шкуру, человека, упавшего от истощения.</ta>
            <ta e="T134" id="Seg_5596" s="T126">Когда приносят, один старик с волосами, белыми как снег, говорит:</ta>
            <ta e="T167" id="Seg_5597" s="T134">"Да ну, что же вы удивляетесь, и в олонгко сказано, и в старых сказаниях, и в записях есть, что где-то такой двухглазый народ живет, ну-ка, изголодавшегося человека начинайте кормить, слишком много не давая. Потеряв свою страну, заблудившись, истощенный прибыл, наверное, бедняжка", говорит.</ta>
            <ta e="T179" id="Seg_5598" s="T167">Это был очень богатый народ, оказывается, песцы и лисицы сами приходили к их дверям, оказывается.</ta>
            <ta e="T183" id="Seg_5599" s="T179">Тот человек здесь стал обживаться.</ta>
            <ta e="T194" id="Seg_5600" s="T183">В семье, где он поселился, оказывается, было две дочери, старшую из них отец отдает парню в жены.</ta>
            <ta e="T200" id="Seg_5601" s="T194">Тут девушка, первой увидевшая его, стала ревновать к своей старшей сестре:</ta>
            <ta e="T206" id="Seg_5602" s="T200">"Мне полагается выйти за своего найденыша", говорит, плача.</ta>
            <ta e="T213" id="Seg_5603" s="T206">Не слушая ее, все равно старшую дочь выдают за парня.</ta>
            <ta e="T220" id="Seg_5604" s="T213">Через год двухглазый мальчик рождаетса.</ta>
            <ta e="T225" id="Seg_5605" s="T220">Вот так очень долго живут.</ta>
            <ta e="T232" id="Seg_5606" s="T225">Эти-то одноглазые многочисленным народом были, оказывается.</ta>
            <ta e="T238" id="Seg_5607" s="T232">Однажды их царь прибывает на корабле собирать подати.</ta>
            <ta e="T244" id="Seg_5608" s="T238">Приходит от него человек к тестю того князя:</ta>
            <ta e="T255" id="Seg_5609" s="T244">"У тебя, говорят, есть двухглазый зять, царь его хочет видеть", говорит [посланный].</ta>
            <ta e="T257" id="Seg_5610" s="T255">Тесть говорит:</ta>
            <ta e="T268" id="Seg_5611" s="T257">"Голубчик мой, иди, царь ведь только посмотрит, если не пойдешь, отрубит нам головы."</ta>
            <ta e="T271" id="Seg_5612" s="T268">Князь пойдет к царю.</ta>
            <ta e="T273" id="Seg_5613" s="T271">Тот говорит:</ta>
            <ta e="T288" id="Seg_5614" s="T273">"Я тебя здесь богатым человеком сделаю, ты здесь ли останешься или вернешься домой?"</ta>
            <ta e="T289" id="Seg_5615" s="T288">На это:</ta>
            <ta e="T291" id="Seg_5616" s="T289">"Вернусь", сказал.</ta>
            <ta e="T294" id="Seg_5617" s="T291">Царь на это:</ta>
            <ta e="T301" id="Seg_5618" s="T294">"Далеко твоя страна, не доедешь [так быстро], как принесла вода.</ta>
            <ta e="T313" id="Seg_5619" s="T301">Я дам письмо: кто бы ни был, будет везти тебя без задержки", так сказав, дает письмо.</ta>
            <ta e="T321" id="Seg_5620" s="T313">Получив это [письмо], все же несколько лет еще живет [там].</ta>
            <ta e="T328" id="Seg_5621" s="T321">Прожив так, засобирается в путь вместе с женой.</ta>
            <ta e="T333" id="Seg_5622" s="T328">Тут младшая дочь начинает плакать:</ta>
            <ta e="T339" id="Seg_5623" s="T333">"Я от найденного мной человека не отстану, с ним вместе поеду."</ta>
            <ta e="T341" id="Seg_5624" s="T339">Старшей сестре говорит:</ta>
            <ta e="T347" id="Seg_5625" s="T341">"У тебя, чтоб поглядеть-вспомнить его, есть двухглазый ребенок.</ta>
            <ta e="T351" id="Seg_5626" s="T347">Я с ним поеду."</ta>
            <ta e="T354" id="Seg_5627" s="T351">Отец ее, согласившись, разрешает.</ta>
            <ta e="T359" id="Seg_5628" s="T354">Так младшая дочь едет с ним вместе.</ta>
            <ta e="T365" id="Seg_5629" s="T359">Вот они, очень долго пробыв в пути, приезжают в свою страну.</ta>
            <ta e="T368" id="Seg_5630" s="T365">Так рассказывают.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_5631" s="T0">В старину жил один ненецкий князь с сыном.</ta>
            <ta e="T10" id="Seg_5632" s="T6">Зимой ненцы на море ловили белуху. </ta>
            <ta e="T18" id="Seg_5633" s="T10">Тот молодой князь по льду моря пошел прорубь долбить для [ловли] белухи.</ta>
            <ta e="T27" id="Seg_5634" s="T18">Затем, когда захотел вернуться, [видит:] льдина его оторвалась [от берега].</ta>
            <ta e="T39" id="Seg_5635" s="T27">Три дня искал, где бы сойти на берег, до крайности дойдя, одного из трех оленей забил и сырым съел.</ta>
            <ta e="T47" id="Seg_5636" s="T39">Затем снова пытался сойти со льдины, но и двух оставшихся оленей забил и съел.</ta>
            <ta e="T52" id="Seg_5637" s="T47">Льдина его, [ломаясь], с бубен шамана стала.</ta>
            <ta e="T63" id="Seg_5638" s="T52">Прибило ее как-то волной к берегу острова и приткнуло к деревьям.</ta>
            <ta e="T68" id="Seg_5639" s="T63">Выбрался на остров, а на нем много деревьев растет.</ta>
            <ta e="T73" id="Seg_5640" s="T68">Где-то слышался стух топора.</ta>
            <ta e="T79" id="Seg_5641" s="T73">Все свои силы напряг и туда пошел.</ta>
            <ta e="T90" id="Seg_5642" s="T79">Подойдя, видит: одна небольшая девушка, стоя к нему спиной, рубит дерево.</ta>
            <ta e="T97" id="Seg_5643" s="T90">Эта девушка оглянулась — оказывается, у ней единственный глаз [на лбу].</ta>
            <ta e="T106" id="Seg_5644" s="T97">Девушка, увидев человека, испугалась и, бросив топор, убежала.</ta>
            <ta e="T114" id="Seg_5645" s="T106">Прибегает и людям своим рассказывает: вот такое видела.</ta>
            <ta e="T126" id="Seg_5646" s="T114">Тогда несколько человек [туда] пошли и принесли, положив на оленью шкуру, человека, упавшего от истощения.</ta>
            <ta e="T134" id="Seg_5647" s="T126">Когда принесли, один старик с волосами, белыми как снег, сказал:</ta>
            <ta e="T167" id="Seg_5648" s="T134">— Да ну, что же вы удивляетесь, и в олонгко сказано, и в старых сказаниях, и в записях есть, что где-то такой двухглазый народ живет, ну-ка, изголодавшегося человека начинайте кормить, слишком много не давая. Потеряв свою страну, заблудившись, истощенный прибыл, наверное, бедняжка.</ta>
            <ta e="T179" id="Seg_5649" s="T167">Это был очень богатый народ, оказывается, песцы и лисицы сами приходили к их дверям, оказывается.</ta>
            <ta e="T183" id="Seg_5650" s="T179">Тот человек здесь стал обживаться.</ta>
            <ta e="T194" id="Seg_5651" s="T183">В семье, где он поселился, оказывается, было две дочери, старшую из них отец отдает парню в жены.</ta>
            <ta e="T200" id="Seg_5652" s="T194">Тут девушка, первой увидевшая его, стала ревновать к своей старшей сестре:</ta>
            <ta e="T206" id="Seg_5653" s="T200">— Мне полагается выйти за своего найденыша, — говорит, плача.</ta>
            <ta e="T213" id="Seg_5654" s="T206">Не согласились с ней, все равно старшую дочь выдали за парня.</ta>
            <ta e="T220" id="Seg_5655" s="T213">Через год двухглазый мальчик родился.</ta>
            <ta e="T225" id="Seg_5656" s="T220">Вот так очень долго живут.</ta>
            <ta e="T232" id="Seg_5657" s="T225">Эти-то одноглазые многочисленным народом были, оказывается.</ta>
            <ta e="T238" id="Seg_5658" s="T232">Однажды их царь прибыл на корабле собирать подати.</ta>
            <ta e="T244" id="Seg_5659" s="T238">Приходит от него человек к тестю того князя:</ta>
            <ta e="T255" id="Seg_5660" s="T244">— У тебя, говорят, есть двухглазый зять, царь его хочет видеть, — говорит [посланный].</ta>
            <ta e="T257" id="Seg_5661" s="T255">Тесть говорит:</ta>
            <ta e="T268" id="Seg_5662" s="T257">— Голубчик мой, иди, царь ведь только посмотрит, если ж пойдешь, отрубит нам головы.</ta>
            <ta e="T271" id="Seg_5663" s="T268">Князь пошел к царю.</ta>
            <ta e="T273" id="Seg_5664" s="T271">Тот говорит:</ta>
            <ta e="T288" id="Seg_5665" s="T273">— Я тебя здесь богатым человеком сделаю, ты здесь ли останешься или вернешься домой?</ta>
            <ta e="T289" id="Seg_5666" s="T288">На это:</ta>
            <ta e="T291" id="Seg_5667" s="T289">— Вернусь, — сказал.</ta>
            <ta e="T294" id="Seg_5668" s="T291">Царь на это:</ta>
            <ta e="T301" id="Seg_5669" s="T294">— Далеко твоя страна, не доедешь [так быстро], как принесла вода.</ta>
            <ta e="T313" id="Seg_5670" s="T301">Я дам письмо: кто бы ни был, будет везти тебя без задержки, — так сказав, дал письмо.</ta>
            <ta e="T321" id="Seg_5671" s="T313">Получив это [письмо], все же несколько лет еще жил [там].</ta>
            <ta e="T328" id="Seg_5672" s="T321">Прожив так, засобирался в путь вместе с женой.</ta>
            <ta e="T333" id="Seg_5673" s="T328">Тут младшая дочь начинает плакать:</ta>
            <ta e="T339" id="Seg_5674" s="T333">— Я от найденного мной человека не отстану, с ним вместе поеду.</ta>
            <ta e="T341" id="Seg_5675" s="T339">Старшей сестре говорит:</ta>
            <ta e="T347" id="Seg_5676" s="T341">— У тебя, чтоб поглядеть-вспомнить его, есть двухглазый ребенок.</ta>
            <ta e="T351" id="Seg_5677" s="T347">Я с ним поеду.</ta>
            <ta e="T354" id="Seg_5678" s="T351">Отец ее, согласившись, разрешил.</ta>
            <ta e="T359" id="Seg_5679" s="T354">Так младшая дочь поехала с ним вместе.</ta>
            <ta e="T365" id="Seg_5680" s="T359">Вот они, очень долго пробыв в пути, приехали в свою страну.</ta>
            <ta e="T368" id="Seg_5681" s="T365">Так рассказывают.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T79" id="Seg_5682" s="T73">[DCh]: The form aus "küːs" 'power' should actually be instrumental, as indicated in brackets in the original transcription.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
