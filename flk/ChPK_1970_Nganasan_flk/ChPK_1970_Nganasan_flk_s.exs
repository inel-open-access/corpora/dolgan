<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDE5D69C76-B78B-596F-411A-1F91B74827AD">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>ChPK_1970_Nganasan_flk</transcription-name>
         <referenced-file url="" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\ChPK_1970_Nganasan_flk\ChPK_1970_Nganasan_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">517</ud-information>
            <ud-information attribute-name="# HIAT:w">377</ud-information>
            <ud-information attribute-name="# e">377</ud-information>
            <ud-information attribute-name="# HIAT:u">59</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="ChPK">
            <abbreviation>ChPK</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="4.0" type="appl" />
         <tli id="T1" time="4.5" type="appl" />
         <tli id="T2" time="5.0" type="appl" />
         <tli id="T3" time="5.5" type="appl" />
         <tli id="T4" time="6.0" type="appl" />
         <tli id="T5" time="6.5" type="appl" />
         <tli id="T6" time="7.0" type="appl" />
         <tli id="T7" time="7.5" type="appl" />
         <tli id="T8" time="8.0" type="appl" />
         <tli id="T9" time="8.5" type="appl" />
         <tli id="T10" time="9.0" type="appl" />
         <tli id="T11" time="9.5" type="appl" />
         <tli id="T12" time="10.0" type="appl" />
         <tli id="T13" time="10.5" type="appl" />
         <tli id="T14" time="11.0" type="appl" />
         <tli id="T15" time="11.5" type="appl" />
         <tli id="T16" time="12.0" type="appl" />
         <tli id="T17" time="12.5" type="appl" />
         <tli id="T18" time="13.0" type="appl" />
         <tli id="T19" time="13.5" type="appl" />
         <tli id="T20" time="14.0" type="appl" />
         <tli id="T21" time="14.5" type="appl" />
         <tli id="T22" time="15.0" type="appl" />
         <tli id="T23" time="15.5" type="appl" />
         <tli id="T24" time="16.0" type="appl" />
         <tli id="T25" time="16.5" type="appl" />
         <tli id="T26" time="17.0" type="appl" />
         <tli id="T27" time="17.5" type="appl" />
         <tli id="T28" time="18.0" type="appl" />
         <tli id="T29" time="18.5" type="appl" />
         <tli id="T30" time="19.0" type="appl" />
         <tli id="T31" time="19.5" type="appl" />
         <tli id="T32" time="20.0" type="appl" />
         <tli id="T33" time="20.5" type="appl" />
         <tli id="T34" time="21.0" type="appl" />
         <tli id="T35" time="21.5" type="appl" />
         <tli id="T36" time="22.0" type="appl" />
         <tli id="T37" time="22.5" type="appl" />
         <tli id="T38" time="23.0" type="appl" />
         <tli id="T39" time="23.5" type="appl" />
         <tli id="T40" time="24.0" type="appl" />
         <tli id="T41" time="24.5" type="appl" />
         <tli id="T42" time="25.0" type="appl" />
         <tli id="T43" time="25.5" type="appl" />
         <tli id="T44" time="26.0" type="appl" />
         <tli id="T45" time="26.5" type="appl" />
         <tli id="T46" time="27.0" type="appl" />
         <tli id="T47" time="27.5" type="appl" />
         <tli id="T48" time="28.0" type="appl" />
         <tli id="T49" time="28.5" type="appl" />
         <tli id="T50" time="29.0" type="appl" />
         <tli id="T51" time="29.5" type="appl" />
         <tli id="T52" time="30.0" type="appl" />
         <tli id="T53" time="30.5" type="appl" />
         <tli id="T54" time="31.0" type="appl" />
         <tli id="T55" time="31.5" type="appl" />
         <tli id="T56" time="32.0" type="appl" />
         <tli id="T57" time="32.5" type="appl" />
         <tli id="T58" time="33.0" type="appl" />
         <tli id="T59" time="33.5" type="appl" />
         <tli id="T60" time="34.0" type="appl" />
         <tli id="T61" time="34.5" type="appl" />
         <tli id="T62" time="35.0" type="appl" />
         <tli id="T63" time="35.5" type="appl" />
         <tli id="T64" time="36.0" type="appl" />
         <tli id="T65" time="36.5" type="appl" />
         <tli id="T66" time="37.0" type="appl" />
         <tli id="T67" time="37.5" type="appl" />
         <tli id="T68" time="38.0" type="appl" />
         <tli id="T69" time="38.5" type="appl" />
         <tli id="T70" time="39.0" type="appl" />
         <tli id="T71" time="39.5" type="appl" />
         <tli id="T72" time="40.0" type="appl" />
         <tli id="T73" time="40.5" type="appl" />
         <tli id="T74" time="41.0" type="appl" />
         <tli id="T75" time="41.5" type="appl" />
         <tli id="T76" time="42.0" type="appl" />
         <tli id="T77" time="42.5" type="appl" />
         <tli id="T78" time="43.0" type="appl" />
         <tli id="T79" time="43.5" type="appl" />
         <tli id="T80" time="44.0" type="appl" />
         <tli id="T81" time="44.5" type="appl" />
         <tli id="T82" time="45.0" type="appl" />
         <tli id="T83" time="45.5" type="appl" />
         <tli id="T84" time="46.0" type="appl" />
         <tli id="T85" time="46.5" type="appl" />
         <tli id="T86" time="47.0" type="appl" />
         <tli id="T87" time="47.5" type="appl" />
         <tli id="T88" time="48.0" type="appl" />
         <tli id="T89" time="48.5" type="appl" />
         <tli id="T90" time="49.0" type="appl" />
         <tli id="T91" time="49.5" type="appl" />
         <tli id="T92" time="50.0" type="appl" />
         <tli id="T93" time="50.5" type="appl" />
         <tli id="T94" time="51.0" type="appl" />
         <tli id="T95" time="51.5" type="appl" />
         <tli id="T96" time="52.0" type="appl" />
         <tli id="T97" time="52.5" type="appl" />
         <tli id="T98" time="53.0" type="appl" />
         <tli id="T99" time="53.5" type="appl" />
         <tli id="T100" time="54.0" type="appl" />
         <tli id="T101" time="54.5" type="appl" />
         <tli id="T102" time="55.0" type="appl" />
         <tli id="T103" time="55.5" type="appl" />
         <tli id="T104" time="56.0" type="appl" />
         <tli id="T105" time="56.5" type="appl" />
         <tli id="T106" time="57.0" type="appl" />
         <tli id="T107" time="57.5" type="appl" />
         <tli id="T108" time="58.0" type="appl" />
         <tli id="T109" time="58.5" type="appl" />
         <tli id="T110" time="59.0" type="appl" />
         <tli id="T111" time="59.5" type="appl" />
         <tli id="T112" time="60.0" type="appl" />
         <tli id="T113" time="60.5" type="appl" />
         <tli id="T114" time="61.0" type="appl" />
         <tli id="T115" time="61.5" type="appl" />
         <tli id="T116" time="62.0" type="appl" />
         <tli id="T117" time="62.5" type="appl" />
         <tli id="T118" time="63.0" type="appl" />
         <tli id="T119" time="63.5" type="appl" />
         <tli id="T120" time="64.0" type="appl" />
         <tli id="T121" time="64.5" type="appl" />
         <tli id="T122" time="65.0" type="appl" />
         <tli id="T123" time="65.5" type="appl" />
         <tli id="T124" time="66.0" type="appl" />
         <tli id="T125" time="66.5" type="appl" />
         <tli id="T126" time="67.0" type="appl" />
         <tli id="T127" time="67.5" type="appl" />
         <tli id="T128" time="68.0" type="appl" />
         <tli id="T129" time="68.5" type="appl" />
         <tli id="T130" time="69.0" type="appl" />
         <tli id="T131" time="69.5" type="appl" />
         <tli id="T132" time="70.0" type="appl" />
         <tli id="T133" time="70.5" type="appl" />
         <tli id="T134" time="71.0" type="appl" />
         <tli id="T135" time="71.5" type="appl" />
         <tli id="T136" time="72.0" type="appl" />
         <tli id="T137" time="72.5" type="appl" />
         <tli id="T138" time="73.0" type="appl" />
         <tli id="T139" time="73.5" type="appl" />
         <tli id="T140" time="74.0" type="appl" />
         <tli id="T141" time="74.5" type="appl" />
         <tli id="T142" time="75.0" type="appl" />
         <tli id="T143" time="75.5" type="appl" />
         <tli id="T144" time="76.0" type="appl" />
         <tli id="T145" time="76.5" type="appl" />
         <tli id="T146" time="77.0" type="appl" />
         <tli id="T147" time="77.5" type="appl" />
         <tli id="T148" time="78.0" type="appl" />
         <tli id="T149" time="78.5" type="appl" />
         <tli id="T150" time="79.0" type="appl" />
         <tli id="T151" time="79.5" type="appl" />
         <tli id="T152" time="80.0" type="appl" />
         <tli id="T153" time="80.5" type="appl" />
         <tli id="T154" time="81.0" type="appl" />
         <tli id="T155" time="81.5" type="appl" />
         <tli id="T156" time="82.0" type="appl" />
         <tli id="T157" time="82.5" type="appl" />
         <tli id="T158" time="83.0" type="appl" />
         <tli id="T159" time="83.5" type="appl" />
         <tli id="T160" time="84.0" type="appl" />
         <tli id="T161" time="84.5" type="appl" />
         <tli id="T162" time="85.0" type="appl" />
         <tli id="T163" time="85.5" type="appl" />
         <tli id="T164" time="86.0" type="appl" />
         <tli id="T165" time="86.5" type="appl" />
         <tli id="T166" time="87.0" type="appl" />
         <tli id="T167" time="87.5" type="appl" />
         <tli id="T168" time="88.0" type="appl" />
         <tli id="T169" time="88.5" type="appl" />
         <tli id="T170" time="89.0" type="appl" />
         <tli id="T171" time="89.5" type="appl" />
         <tli id="T172" time="90.0" type="appl" />
         <tli id="T173" time="90.5" type="appl" />
         <tli id="T174" time="91.0" type="appl" />
         <tli id="T175" time="91.5" type="appl" />
         <tli id="T176" time="92.0" type="appl" />
         <tli id="T177" time="92.5" type="appl" />
         <tli id="T178" time="93.0" type="appl" />
         <tli id="T179" time="93.5" type="appl" />
         <tli id="T180" time="94.0" type="appl" />
         <tli id="T181" time="94.5" type="appl" />
         <tli id="T182" time="95.0" type="appl" />
         <tli id="T183" time="95.5" type="appl" />
         <tli id="T184" time="96.0" type="appl" />
         <tli id="T185" time="96.5" type="appl" />
         <tli id="T186" time="97.0" type="appl" />
         <tli id="T187" time="97.5" type="appl" />
         <tli id="T188" time="98.0" type="appl" />
         <tli id="T189" time="98.5" type="appl" />
         <tli id="T190" time="99.0" type="appl" />
         <tli id="T191" time="99.5" type="appl" />
         <tli id="T192" time="100.0" type="appl" />
         <tli id="T193" time="100.5" type="appl" />
         <tli id="T194" time="101.0" type="appl" />
         <tli id="T195" time="101.5" type="appl" />
         <tli id="T196" time="102.0" type="appl" />
         <tli id="T197" time="102.5" type="appl" />
         <tli id="T198" time="103.0" type="appl" />
         <tli id="T199" time="103.5" type="appl" />
         <tli id="T200" time="104.0" type="appl" />
         <tli id="T201" time="104.5" type="appl" />
         <tli id="T202" time="105.0" type="appl" />
         <tli id="T203" time="105.5" type="appl" />
         <tli id="T204" time="106.0" type="appl" />
         <tli id="T205" time="106.5" type="appl" />
         <tli id="T206" time="107.0" type="appl" />
         <tli id="T207" time="107.5" type="appl" />
         <tli id="T208" time="108.0" type="appl" />
         <tli id="T209" time="108.5" type="appl" />
         <tli id="T210" time="109.0" type="appl" />
         <tli id="T211" time="109.5" type="appl" />
         <tli id="T212" time="110.0" type="appl" />
         <tli id="T213" time="110.5" type="appl" />
         <tli id="T214" time="111.0" type="appl" />
         <tli id="T215" time="111.5" type="appl" />
         <tli id="T216" time="112.0" type="appl" />
         <tli id="T217" time="112.5" type="appl" />
         <tli id="T218" time="113.0" type="appl" />
         <tli id="T219" time="113.5" type="appl" />
         <tli id="T220" time="114.0" type="appl" />
         <tli id="T221" time="114.5" type="appl" />
         <tli id="T222" time="115.0" type="appl" />
         <tli id="T223" time="115.5" type="appl" />
         <tli id="T224" time="116.0" type="appl" />
         <tli id="T225" time="116.5" type="appl" />
         <tli id="T226" time="117.0" type="appl" />
         <tli id="T227" time="117.5" type="appl" />
         <tli id="T228" time="118.0" type="appl" />
         <tli id="T229" time="118.5" type="appl" />
         <tli id="T230" time="119.0" type="appl" />
         <tli id="T231" time="119.5" type="appl" />
         <tli id="T232" time="120.0" type="appl" />
         <tli id="T233" time="120.5" type="appl" />
         <tli id="T234" time="121.0" type="appl" />
         <tli id="T235" time="121.5" type="appl" />
         <tli id="T236" time="122.0" type="appl" />
         <tli id="T237" time="122.5" type="appl" />
         <tli id="T238" time="123.0" type="appl" />
         <tli id="T239" time="123.5" type="appl" />
         <tli id="T240" time="124.0" type="appl" />
         <tli id="T241" time="124.5" type="appl" />
         <tli id="T242" time="125.0" type="appl" />
         <tli id="T243" time="125.5" type="appl" />
         <tli id="T244" time="126.0" type="appl" />
         <tli id="T245" time="126.5" type="appl" />
         <tli id="T246" time="127.0" type="appl" />
         <tli id="T247" time="127.5" type="appl" />
         <tli id="T248" time="128.0" type="appl" />
         <tli id="T249" time="128.5" type="appl" />
         <tli id="T250" time="129.0" type="appl" />
         <tli id="T251" time="129.5" type="appl" />
         <tli id="T252" time="130.0" type="appl" />
         <tli id="T253" time="130.5" type="appl" />
         <tli id="T254" time="131.0" type="appl" />
         <tli id="T255" time="131.5" type="appl" />
         <tli id="T256" time="132.0" type="appl" />
         <tli id="T257" time="132.5" type="appl" />
         <tli id="T258" time="133.0" type="appl" />
         <tli id="T259" time="133.5" type="appl" />
         <tli id="T260" time="134.0" type="appl" />
         <tli id="T261" time="134.5" type="appl" />
         <tli id="T262" time="135.0" type="appl" />
         <tli id="T263" time="135.5" type="appl" />
         <tli id="T264" time="136.0" type="appl" />
         <tli id="T265" time="136.5" type="appl" />
         <tli id="T266" time="137.0" type="appl" />
         <tli id="T267" time="137.5" type="appl" />
         <tli id="T268" time="138.0" type="appl" />
         <tli id="T269" time="138.5" type="appl" />
         <tli id="T270" time="139.0" type="appl" />
         <tli id="T271" time="139.5" type="appl" />
         <tli id="T272" time="140.0" type="appl" />
         <tli id="T273" time="140.5" type="appl" />
         <tli id="T274" time="141.0" type="appl" />
         <tli id="T275" time="141.5" type="appl" />
         <tli id="T276" time="142.0" type="appl" />
         <tli id="T277" time="142.5" type="appl" />
         <tli id="T278" time="143.0" type="appl" />
         <tli id="T279" time="143.5" type="appl" />
         <tli id="T280" time="144.0" type="appl" />
         <tli id="T281" time="144.5" type="appl" />
         <tli id="T282" time="145.0" type="appl" />
         <tli id="T283" time="145.5" type="appl" />
         <tli id="T284" time="146.0" type="appl" />
         <tli id="T285" time="146.5" type="appl" />
         <tli id="T286" time="147.0" type="appl" />
         <tli id="T287" time="147.5" type="appl" />
         <tli id="T288" time="148.0" type="appl" />
         <tli id="T289" time="148.5" type="appl" />
         <tli id="T290" time="149.0" type="appl" />
         <tli id="T291" time="149.5" type="appl" />
         <tli id="T292" time="150.0" type="appl" />
         <tli id="T293" time="150.5" type="appl" />
         <tli id="T294" time="151.0" type="appl" />
         <tli id="T295" time="151.5" type="appl" />
         <tli id="T296" time="152.0" type="appl" />
         <tli id="T297" time="152.5" type="appl" />
         <tli id="T298" time="153.0" type="appl" />
         <tli id="T299" time="153.5" type="appl" />
         <tli id="T300" time="154.0" type="appl" />
         <tli id="T301" time="154.5" type="appl" />
         <tli id="T302" time="155.0" type="appl" />
         <tli id="T303" time="155.5" type="appl" />
         <tli id="T304" time="156.0" type="appl" />
         <tli id="T305" time="156.5" type="appl" />
         <tli id="T306" time="157.0" type="appl" />
         <tli id="T307" time="157.5" type="appl" />
         <tli id="T308" time="158.0" type="appl" />
         <tli id="T309" time="158.5" type="appl" />
         <tli id="T310" time="159.0" type="appl" />
         <tli id="T311" time="159.5" type="appl" />
         <tli id="T312" time="160.0" type="appl" />
         <tli id="T313" time="160.5" type="appl" />
         <tli id="T314" time="161.0" type="appl" />
         <tli id="T315" time="161.5" type="appl" />
         <tli id="T316" time="162.0" type="appl" />
         <tli id="T317" time="162.5" type="appl" />
         <tli id="T318" time="163.0" type="appl" />
         <tli id="T319" time="163.5" type="appl" />
         <tli id="T320" time="164.0" type="appl" />
         <tli id="T321" time="164.5" type="appl" />
         <tli id="T322" time="165.0" type="appl" />
         <tli id="T323" time="165.5" type="appl" />
         <tli id="T324" time="166.0" type="appl" />
         <tli id="T325" time="166.5" type="appl" />
         <tli id="T326" time="167.0" type="appl" />
         <tli id="T327" time="167.5" type="appl" />
         <tli id="T328" time="168.0" type="appl" />
         <tli id="T329" time="168.5" type="appl" />
         <tli id="T330" time="169.0" type="appl" />
         <tli id="T331" time="169.5" type="appl" />
         <tli id="T332" time="170.0" type="appl" />
         <tli id="T333" time="170.5" type="appl" />
         <tli id="T334" time="171.0" type="appl" />
         <tli id="T335" time="171.5" type="appl" />
         <tli id="T336" time="172.0" type="appl" />
         <tli id="T337" time="172.5" type="appl" />
         <tli id="T338" time="173.0" type="appl" />
         <tli id="T339" time="173.5" type="appl" />
         <tli id="T340" time="174.0" type="appl" />
         <tli id="T341" time="174.5" type="appl" />
         <tli id="T342" time="175.0" type="appl" />
         <tli id="T343" time="175.5" type="appl" />
         <tli id="T344" time="176.0" type="appl" />
         <tli id="T345" time="176.5" type="appl" />
         <tli id="T346" time="177.0" type="appl" />
         <tli id="T347" time="177.5" type="appl" />
         <tli id="T348" time="178.0" type="appl" />
         <tli id="T349" time="178.5" type="appl" />
         <tli id="T350" time="179.0" type="appl" />
         <tli id="T351" time="179.5" type="appl" />
         <tli id="T352" time="180.0" type="appl" />
         <tli id="T353" time="180.5" type="appl" />
         <tli id="T354" time="181.0" type="appl" />
         <tli id="T355" time="181.5" type="appl" />
         <tli id="T356" time="182.0" type="appl" />
         <tli id="T357" time="182.5" type="appl" />
         <tli id="T358" time="183.0" type="appl" />
         <tli id="T359" time="183.5" type="appl" />
         <tli id="T360" time="184.0" type="appl" />
         <tli id="T361" time="184.5" type="appl" />
         <tli id="T362" time="185.0" type="appl" />
         <tli id="T363" time="185.5" type="appl" />
         <tli id="T364" time="186.0" type="appl" />
         <tli id="T365" time="186.5" type="appl" />
         <tli id="T366" time="187.0" type="appl" />
         <tli id="T367" time="187.5" type="appl" />
         <tli id="T368" time="188.0" type="appl" />
         <tli id="T369" time="188.5" type="appl" />
         <tli id="T370" time="189.0" type="appl" />
         <tli id="T371" time="189.5" type="appl" />
         <tli id="T372" time="190.0" type="appl" />
         <tli id="T373" time="190.5" type="appl" />
         <tli id="T374" time="191.0" type="appl" />
         <tli id="T375" time="191.5" type="appl" />
         <tli id="T376" time="192.0" type="appl" />
         <tli id="T377" time="192.5" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="ChPK"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T377" id="Seg_0" n="sc" s="T0">
               <ts e="T9" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Haːmajdar</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">bɨlɨr-bɨlɨr</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">Kerikege</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">oloror</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">ebittere</ts>
                  <nts id="Seg_17" n="HIAT:ip">,</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">Dʼehejge</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_22" n="HIAT:ip">—</nts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">čaːŋiːttar</ts>
                  <nts id="Seg_26" n="HIAT:ip">,</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">Taːska</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_31" n="HIAT:ip">—</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">toŋustar</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_38" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_40" n="HIAT:w" s="T9">Araj</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_43" n="HIAT:w" s="T10">biːrde</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_46" n="HIAT:w" s="T11">biːr</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">haːmaj</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">dʼaktarɨn</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">čaːŋiːttar</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_58" n="HIAT:w" s="T15">u͡orbuttar</ts>
                  <nts id="Seg_59" n="HIAT:ip">.</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_62" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">Bu</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">dʼaktar</ts>
                  <nts id="Seg_68" n="HIAT:ip">,</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_71" n="HIAT:w" s="T18">ere</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_74" n="HIAT:w" s="T19">kɨːllana</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_77" n="HIAT:w" s="T20">barbɨtɨn</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_80" n="HIAT:w" s="T21">kenne</ts>
                  <nts id="Seg_81" n="HIAT:ip">,</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_84" n="HIAT:w" s="T22">iːstene</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_87" n="HIAT:w" s="T23">olorbut</ts>
                  <nts id="Seg_88" n="HIAT:ip">.</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_91" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_93" n="HIAT:w" s="T24">Ol</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_96" n="HIAT:w" s="T25">olordoguna</ts>
                  <nts id="Seg_97" n="HIAT:ip">,</nts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_100" n="HIAT:w" s="T26">ɨttar</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_103" n="HIAT:w" s="T27">ürbütter</ts>
                  <nts id="Seg_104" n="HIAT:ip">.</nts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_107" n="HIAT:u" s="T28">
                  <nts id="Seg_108" n="HIAT:ip">"</nts>
                  <ts e="T29" id="Seg_110" n="HIAT:w" s="T28">Iččilerin</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_113" n="HIAT:w" s="T29">ɨttar</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_116" n="HIAT:w" s="T30">ürü͡ö</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_119" n="HIAT:w" s="T31">hu͡ok</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_122" n="HIAT:w" s="T32">etilere</ts>
                  <nts id="Seg_123" n="HIAT:ip">"</nts>
                  <nts id="Seg_124" n="HIAT:ip">,</nts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_127" n="HIAT:w" s="T33">dʼiː</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_130" n="HIAT:w" s="T34">hanaːbɨt</ts>
                  <nts id="Seg_131" n="HIAT:ip">.</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_134" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_136" n="HIAT:w" s="T35">U͡otun</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_139" n="HIAT:w" s="T36">ottoːru</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_142" n="HIAT:w" s="T37">gɨnarɨn</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_145" n="HIAT:w" s="T38">gɨtta</ts>
                  <nts id="Seg_146" n="HIAT:ip">,</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_149" n="HIAT:w" s="T39">üs</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_152" n="HIAT:w" s="T40">kihi</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_155" n="HIAT:w" s="T41">kiːrbit</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_158" n="HIAT:w" s="T42">da</ts>
                  <nts id="Seg_159" n="HIAT:ip">,</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_162" n="HIAT:w" s="T43">dʼaktarɨ</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_165" n="HIAT:w" s="T44">torgogo</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_168" n="HIAT:w" s="T45">huːlaːn</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_171" n="HIAT:w" s="T46">baran</ts>
                  <nts id="Seg_172" n="HIAT:ip">,</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_175" n="HIAT:w" s="T47">ildʼe</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_178" n="HIAT:w" s="T48">barbɨttar</ts>
                  <nts id="Seg_179" n="HIAT:ip">.</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_182" n="HIAT:u" s="T49">
                  <ts e="T50" id="Seg_184" n="HIAT:w" s="T49">Ki͡ehe</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_187" n="HIAT:w" s="T50">haːmaj</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_190" n="HIAT:w" s="T51">kelbite</ts>
                  <nts id="Seg_191" n="HIAT:ip">,</nts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_194" n="HIAT:w" s="T52">ɨttara</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_197" n="HIAT:w" s="T53">baːjɨlla</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_200" n="HIAT:w" s="T54">hɨtallar</ts>
                  <nts id="Seg_201" n="HIAT:ip">,</nts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_204" n="HIAT:w" s="T55">dʼi͡ete</ts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_207" n="HIAT:w" s="T56">kam</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_210" n="HIAT:w" s="T57">toŋmut</ts>
                  <nts id="Seg_211" n="HIAT:ip">,</nts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_214" n="HIAT:w" s="T58">kim</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_217" n="HIAT:w" s="T59">da</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_220" n="HIAT:w" s="T60">hu͡ok</ts>
                  <nts id="Seg_221" n="HIAT:ip">,</nts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_224" n="HIAT:w" s="T61">kap-karaŋa</ts>
                  <nts id="Seg_225" n="HIAT:ip">.</nts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_228" n="HIAT:u" s="T62">
                  <nts id="Seg_229" n="HIAT:ip">"</nts>
                  <ts e="T63" id="Seg_231" n="HIAT:w" s="T62">Ajɨː</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_234" n="HIAT:w" s="T63">dʼiː</ts>
                  <nts id="Seg_235" n="HIAT:ip">,</nts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_238" n="HIAT:w" s="T64">dʼaktarɨm</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_241" n="HIAT:w" s="T65">kajdi͡ek</ts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_244" n="HIAT:w" s="T66">barbɨta</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_247" n="HIAT:w" s="T67">bu͡olu͡oj</ts>
                  <nts id="Seg_248" n="HIAT:ip">"</nts>
                  <nts id="Seg_249" n="HIAT:ip">,</nts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_252" n="HIAT:w" s="T68">hanaːrgaːbɨt</ts>
                  <nts id="Seg_253" n="HIAT:ip">.</nts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_256" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_258" n="HIAT:w" s="T69">Harsi͡erde</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_261" n="HIAT:w" s="T70">hɨrdɨːrɨn</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_264" n="HIAT:w" s="T71">gɨtta</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_267" n="HIAT:w" s="T72">ü͡örün</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_270" n="HIAT:w" s="T73">egele</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_273" n="HIAT:w" s="T74">barbɨt</ts>
                  <nts id="Seg_274" n="HIAT:ip">.</nts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_277" n="HIAT:u" s="T75">
                  <ts e="T76" id="Seg_279" n="HIAT:w" s="T75">Tabata</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_282" n="HIAT:w" s="T76">barɨta</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_285" n="HIAT:w" s="T77">baːr</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_288" n="HIAT:w" s="T78">ebit</ts>
                  <nts id="Seg_289" n="HIAT:ip">.</nts>
                  <nts id="Seg_290" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_292" n="HIAT:u" s="T79">
                  <nts id="Seg_293" n="HIAT:ip">"</nts>
                  <ts e="T80" id="Seg_295" n="HIAT:w" s="T79">Min</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_298" n="HIAT:w" s="T80">toŋuspar</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_301" n="HIAT:w" s="T81">köhü͡öm</ts>
                  <nts id="Seg_302" n="HIAT:ip">,</nts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_305" n="HIAT:w" s="T82">tugu</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_308" n="HIAT:w" s="T83">gɨnɨ͡amɨj</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_311" n="HIAT:w" s="T84">hogotogun</ts>
                  <nts id="Seg_312" n="HIAT:ip">?</nts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_315" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_317" n="HIAT:w" s="T85">Hübelehi͡em</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_320" n="HIAT:w" s="T86">itte</ts>
                  <nts id="Seg_321" n="HIAT:ip">"</nts>
                  <nts id="Seg_322" n="HIAT:ip">,</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_325" n="HIAT:w" s="T87">dʼi͡en</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_328" n="HIAT:w" s="T88">hanaːbɨt</ts>
                  <nts id="Seg_329" n="HIAT:ip">.</nts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T104" id="Seg_332" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_334" n="HIAT:w" s="T89">Čor</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_337" n="HIAT:w" s="T90">hogotok</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_340" n="HIAT:w" s="T91">kihi</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_343" n="HIAT:w" s="T92">ör</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_346" n="HIAT:w" s="T93">komunu͡oj</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_348" n="HIAT:ip">—</nts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_351" n="HIAT:w" s="T94">toŋuska</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_354" n="HIAT:w" s="T95">köhön</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_357" n="HIAT:w" s="T96">kelbite</ts>
                  <nts id="Seg_358" n="HIAT:ip">,</nts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_361" n="HIAT:w" s="T97">kihite</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_364" n="HIAT:w" s="T98">ilimnene</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_367" n="HIAT:w" s="T99">barbɨt</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_370" n="HIAT:w" s="T100">ebit</ts>
                  <nts id="Seg_371" n="HIAT:ip">,</nts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_374" n="HIAT:w" s="T101">inʼete</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_377" n="HIAT:w" s="T102">ire</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_380" n="HIAT:w" s="T103">baːr</ts>
                  <nts id="Seg_381" n="HIAT:ip">.</nts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_384" n="HIAT:u" s="T104">
                  <ts e="T105" id="Seg_386" n="HIAT:w" s="T104">Toŋus</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_389" n="HIAT:w" s="T105">hoč-hogotogun</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_392" n="HIAT:w" s="T106">oloror</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_395" n="HIAT:w" s="T107">ebit</ts>
                  <nts id="Seg_396" n="HIAT:ip">.</nts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_399" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_401" n="HIAT:w" s="T108">Ki͡ehe</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_404" n="HIAT:w" s="T109">kelbit</ts>
                  <nts id="Seg_405" n="HIAT:ip">.</nts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_408" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_410" n="HIAT:w" s="T110">Haːmaj</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_413" n="HIAT:w" s="T111">erejdeːk</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_416" n="HIAT:w" s="T112">kepseːbit</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_419" n="HIAT:w" s="T113">dʼaktara</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_422" n="HIAT:w" s="T114">hüppütün</ts>
                  <nts id="Seg_423" n="HIAT:ip">.</nts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_426" n="HIAT:u" s="T115">
                  <nts id="Seg_427" n="HIAT:ip">"</nts>
                  <ts e="T116" id="Seg_429" n="HIAT:w" s="T115">Če</ts>
                  <nts id="Seg_430" n="HIAT:ip">,</nts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_433" n="HIAT:w" s="T116">kim</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_436" n="HIAT:w" s="T117">gɨnɨ͡aj</ts>
                  <nts id="Seg_437" n="HIAT:ip">,</nts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_440" n="HIAT:w" s="T118">begeheː</ts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_443" n="HIAT:w" s="T119">minʼi͡eke</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_446" n="HIAT:w" s="T120">üs</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_449" n="HIAT:w" s="T121">kihi</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_452" n="HIAT:w" s="T122">baːr</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_455" n="HIAT:w" s="T123">etilere</ts>
                  <nts id="Seg_456" n="HIAT:ip">.</nts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_459" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_461" n="HIAT:w" s="T124">Körüŋŋere</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_464" n="HIAT:w" s="T125">hoččoto</ts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_467" n="HIAT:w" s="T126">hu͡ok</ts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_470" n="HIAT:w" s="T127">ete</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_472" n="HIAT:ip">—</nts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_475" n="HIAT:w" s="T128">araːha</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_478" n="HIAT:w" s="T129">čaːŋiːttar</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_481" n="HIAT:w" s="T130">bɨhɨːlaːk</ts>
                  <nts id="Seg_482" n="HIAT:ip">.</nts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_485" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_487" n="HIAT:w" s="T131">Minʼigitten</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_490" n="HIAT:w" s="T132">tu͡okpun</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_493" n="HIAT:w" s="T133">ɨlɨ͡aktaraj</ts>
                  <nts id="Seg_494" n="HIAT:ip">?</nts>
                  <nts id="Seg_495" n="HIAT:ip">"</nts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T146" id="Seg_498" n="HIAT:u" s="T134">
                  <nts id="Seg_499" n="HIAT:ip">"</nts>
                  <ts e="T135" id="Seg_501" n="HIAT:w" s="T134">Čugas</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_504" n="HIAT:w" s="T135">baːr</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_507" n="HIAT:w" s="T136">ɨ͡allar</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_510" n="HIAT:w" s="T137">emi͡e</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_513" n="HIAT:w" s="T138">kepsiːller</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_516" n="HIAT:w" s="T139">kajtak</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_519" n="HIAT:w" s="T140">giniler</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_522" n="HIAT:w" s="T141">dʼaktattarɨ</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_525" n="HIAT:w" s="T142">u͡ora</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_528" n="HIAT:w" s="T143">hɨldʼallarɨn</ts>
                  <nts id="Seg_529" n="HIAT:ip">"</nts>
                  <nts id="Seg_530" n="HIAT:ip">,</nts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_533" n="HIAT:w" s="T144">toŋus</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_536" n="HIAT:w" s="T145">dʼi͡ebit</ts>
                  <nts id="Seg_537" n="HIAT:ip">.</nts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_540" n="HIAT:u" s="T146">
                  <nts id="Seg_541" n="HIAT:ip">"</nts>
                  <ts e="T147" id="Seg_543" n="HIAT:w" s="T146">En</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_546" n="HIAT:w" s="T147">töhö</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_549" n="HIAT:w" s="T148">kohuːŋŋunuj</ts>
                  <nts id="Seg_550" n="HIAT:ip">"</nts>
                  <nts id="Seg_551" n="HIAT:ip">,</nts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_554" n="HIAT:w" s="T149">toŋus</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_557" n="HIAT:w" s="T150">dogorun</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_560" n="HIAT:w" s="T151">ɨjɨppɨt</ts>
                  <nts id="Seg_561" n="HIAT:ip">.</nts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T158" id="Seg_564" n="HIAT:u" s="T152">
                  <nts id="Seg_565" n="HIAT:ip">"</nts>
                  <ts e="T153" id="Seg_567" n="HIAT:w" s="T152">Kohuːn</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_570" n="HIAT:w" s="T153">bu͡olammɨn</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_573" n="HIAT:w" s="T154">hɨldʼar</ts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_576" n="HIAT:w" s="T155">enʼibin</ts>
                  <nts id="Seg_577" n="HIAT:ip">"</nts>
                  <nts id="Seg_578" n="HIAT:ip">,</nts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_581" n="HIAT:w" s="T156">dʼi͡ebit</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_584" n="HIAT:w" s="T157">haːmaja</ts>
                  <nts id="Seg_585" n="HIAT:ip">.</nts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T164" id="Seg_588" n="HIAT:u" s="T158">
                  <nts id="Seg_589" n="HIAT:ip">"</nts>
                  <ts e="T159" id="Seg_591" n="HIAT:w" s="T158">Oččogo</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_594" n="HIAT:w" s="T159">min</ts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_597" n="HIAT:w" s="T160">enʼigin</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_600" n="HIAT:w" s="T161">bili͡em</ts>
                  <nts id="Seg_601" n="HIAT:ip">,</nts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_604" n="HIAT:w" s="T162">töhö</ts>
                  <nts id="Seg_605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_607" n="HIAT:w" s="T163">berkinij</ts>
                  <nts id="Seg_608" n="HIAT:ip">.</nts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T171" id="Seg_611" n="HIAT:u" s="T164">
                  <ts e="T165" id="Seg_613" n="HIAT:w" s="T164">Min</ts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_616" n="HIAT:w" s="T165">enʼigin</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_619" n="HIAT:w" s="T166">üs</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_622" n="HIAT:w" s="T167">ɨt</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_625" n="HIAT:w" s="T168">hɨrgata</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_628" n="HIAT:w" s="T169">kuraŋɨttan</ts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_631" n="HIAT:w" s="T170">ɨtɨ͡am</ts>
                  <nts id="Seg_632" n="HIAT:ip">.</nts>
                  <nts id="Seg_633" n="HIAT:ip">"</nts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T179" id="Seg_636" n="HIAT:u" s="T171">
                  <ts e="T172" id="Seg_638" n="HIAT:w" s="T171">Toŋus</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_641" n="HIAT:w" s="T172">üste</ts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_644" n="HIAT:w" s="T173">ɨppɨt</ts>
                  <nts id="Seg_645" n="HIAT:ip">,</nts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_648" n="HIAT:w" s="T174">eŋerin</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_651" n="HIAT:w" s="T175">ire</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_654" n="HIAT:w" s="T176">kɨrɨːtɨn</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_657" n="HIAT:w" s="T177">hiːre</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_660" n="HIAT:w" s="T178">kötüppüt</ts>
                  <nts id="Seg_661" n="HIAT:ip">.</nts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T183" id="Seg_664" n="HIAT:u" s="T179">
                  <nts id="Seg_665" n="HIAT:ip">"</nts>
                  <ts e="T180" id="Seg_667" n="HIAT:w" s="T179">Anɨ</ts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_670" n="HIAT:w" s="T180">en</ts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_673" n="HIAT:w" s="T181">ɨt</ts>
                  <nts id="Seg_674" n="HIAT:ip">"</nts>
                  <nts id="Seg_675" n="HIAT:ip">,</nts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_678" n="HIAT:w" s="T182">dʼi͡ebit</ts>
                  <nts id="Seg_679" n="HIAT:ip">.</nts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_682" n="HIAT:u" s="T183">
                  <ts e="T184" id="Seg_684" n="HIAT:w" s="T183">Haːmaj</ts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_687" n="HIAT:w" s="T184">ɨppɨt</ts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_689" n="HIAT:ip">—</nts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_692" n="HIAT:w" s="T185">tappatak</ts>
                  <nts id="Seg_693" n="HIAT:ip">.</nts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_696" n="HIAT:u" s="T186">
                  <nts id="Seg_697" n="HIAT:ip">"</nts>
                  <ts e="T187" id="Seg_699" n="HIAT:w" s="T186">Dʼe</ts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_702" n="HIAT:w" s="T187">bert</ts>
                  <nts id="Seg_703" n="HIAT:ip">.</nts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T192" id="Seg_706" n="HIAT:u" s="T188">
                  <ts e="T189" id="Seg_708" n="HIAT:w" s="T188">Kohuːŋŋun</ts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_711" n="HIAT:w" s="T189">ebit</ts>
                  <nts id="Seg_712" n="HIAT:ip">"</nts>
                  <nts id="Seg_713" n="HIAT:ip">,</nts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_716" n="HIAT:w" s="T190">toŋuha</ts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_719" n="HIAT:w" s="T191">kajgaːbɨt</ts>
                  <nts id="Seg_720" n="HIAT:ip">.</nts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T194" id="Seg_723" n="HIAT:u" s="T192">
                  <ts e="T193" id="Seg_725" n="HIAT:w" s="T192">Harsɨŋŋɨtɨn</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_728" n="HIAT:w" s="T193">köspütter</ts>
                  <nts id="Seg_729" n="HIAT:ip">.</nts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T197" id="Seg_732" n="HIAT:u" s="T194">
                  <ts e="T195" id="Seg_734" n="HIAT:w" s="T194">Üs</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_737" n="HIAT:w" s="T195">taːhɨ</ts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_740" n="HIAT:w" s="T196">bɨspɨttar</ts>
                  <nts id="Seg_741" n="HIAT:ip">.</nts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T204" id="Seg_744" n="HIAT:u" s="T197">
                  <nts id="Seg_745" n="HIAT:ip">"</nts>
                  <ts e="T198" id="Seg_747" n="HIAT:w" s="T197">Dʼe</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_750" n="HIAT:w" s="T198">min</ts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_753" n="HIAT:w" s="T199">inʼebin</ts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_756" n="HIAT:w" s="T200">haːmajdɨː</ts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_759" n="HIAT:w" s="T201">taŋɨnnaran</ts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_762" n="HIAT:w" s="T202">baran</ts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_765" n="HIAT:w" s="T203">barɨ͡am</ts>
                  <nts id="Seg_766" n="HIAT:ip">.</nts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T207" id="Seg_769" n="HIAT:u" s="T204">
                  <ts e="T205" id="Seg_771" n="HIAT:w" s="T204">Min</ts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_774" n="HIAT:w" s="T205">ginileri</ts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_777" n="HIAT:w" s="T206">albɨnnɨ͡am</ts>
                  <nts id="Seg_778" n="HIAT:ip">.</nts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T218" id="Seg_781" n="HIAT:u" s="T207">
                  <ts e="T208" id="Seg_783" n="HIAT:w" s="T207">Dʼi͡em</ts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_786" n="HIAT:w" s="T208">'bu</ts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_789" n="HIAT:w" s="T209">dʼaktar</ts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_791" n="HIAT:ip">—</nts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_794" n="HIAT:w" s="T210">ehigi</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_797" n="HIAT:w" s="T211">ɨlaːččɨ</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_800" n="HIAT:w" s="T212">dʼaktargɨt</ts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_803" n="HIAT:w" s="T213">edʼije</ts>
                  <nts id="Seg_804" n="HIAT:ip">,</nts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_807" n="HIAT:w" s="T214">bu</ts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_810" n="HIAT:w" s="T215">dʼaktar</ts>
                  <nts id="Seg_811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_813" n="HIAT:w" s="T216">min</ts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_816" n="HIAT:w" s="T217">dʼaktarɨm'</ts>
                  <nts id="Seg_817" n="HIAT:ip">.</nts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T224" id="Seg_820" n="HIAT:u" s="T218">
                  <ts e="T219" id="Seg_822" n="HIAT:w" s="T218">En</ts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_825" n="HIAT:w" s="T219">bu͡ollagɨna</ts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_828" n="HIAT:w" s="T220">min</ts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_831" n="HIAT:w" s="T221">dʼaktarɨm</ts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_834" n="HIAT:w" s="T222">baltɨta</ts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_837" n="HIAT:w" s="T223">bu͡olu͡oŋ</ts>
                  <nts id="Seg_838" n="HIAT:ip">.</nts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T229" id="Seg_841" n="HIAT:u" s="T224">
                  <ts e="T225" id="Seg_843" n="HIAT:w" s="T224">Toŋus</ts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_846" n="HIAT:w" s="T225">inʼetin</ts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_849" n="HIAT:w" s="T226">kɨtta</ts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_852" n="HIAT:w" s="T227">baran</ts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_855" n="HIAT:w" s="T228">kaːlbɨttar</ts>
                  <nts id="Seg_856" n="HIAT:ip">.</nts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T234" id="Seg_859" n="HIAT:u" s="T229">
                  <ts e="T230" id="Seg_861" n="HIAT:w" s="T229">Tijbittere</ts>
                  <nts id="Seg_862" n="HIAT:ip">,</nts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_865" n="HIAT:w" s="T230">hette</ts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_868" n="HIAT:w" s="T231">uraha</ts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_871" n="HIAT:w" s="T232">dʼi͡e</ts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_874" n="HIAT:w" s="T233">turallar</ts>
                  <nts id="Seg_875" n="HIAT:ip">.</nts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T239" id="Seg_878" n="HIAT:u" s="T234">
                  <ts e="T235" id="Seg_880" n="HIAT:w" s="T234">Onu</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_883" n="HIAT:w" s="T235">tögürüččü</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_886" n="HIAT:w" s="T236">kihiler</ts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_889" n="HIAT:w" s="T237">üŋküːlüː</ts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_892" n="HIAT:w" s="T238">hɨldʼallar</ts>
                  <nts id="Seg_893" n="HIAT:ip">.</nts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T244" id="Seg_896" n="HIAT:u" s="T239">
                  <nts id="Seg_897" n="HIAT:ip">"</nts>
                  <ts e="T240" id="Seg_899" n="HIAT:w" s="T239">Araːha</ts>
                  <nts id="Seg_900" n="HIAT:ip">,</nts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_903" n="HIAT:w" s="T240">kurum</ts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_906" n="HIAT:w" s="T241">bɨhɨlaːk</ts>
                  <nts id="Seg_907" n="HIAT:ip">"</nts>
                  <nts id="Seg_908" n="HIAT:ip">,</nts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_911" n="HIAT:w" s="T242">dʼiː</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_914" n="HIAT:w" s="T243">hanaːbɨttar</ts>
                  <nts id="Seg_915" n="HIAT:ip">.</nts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_918" n="HIAT:u" s="T244">
                  <ts e="T245" id="Seg_920" n="HIAT:w" s="T244">Toŋus</ts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_923" n="HIAT:w" s="T245">biːr</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_926" n="HIAT:w" s="T246">kihini</ts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_929" n="HIAT:w" s="T247">kuːsput</ts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_932" n="HIAT:w" s="T248">da</ts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_935" n="HIAT:w" s="T249">üŋküːlüː</ts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_938" n="HIAT:w" s="T250">hɨldʼɨbɨttar</ts>
                  <nts id="Seg_939" n="HIAT:ip">.</nts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T254" id="Seg_942" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_944" n="HIAT:w" s="T251">Ol</ts>
                  <nts id="Seg_945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_947" n="HIAT:w" s="T252">hɨldʼan</ts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_950" n="HIAT:w" s="T253">ɨjɨppɨt</ts>
                  <nts id="Seg_951" n="HIAT:ip">:</nts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T262" id="Seg_954" n="HIAT:u" s="T254">
                  <nts id="Seg_955" n="HIAT:ip">"</nts>
                  <ts e="T255" id="Seg_957" n="HIAT:w" s="T254">Kanna</ts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_960" n="HIAT:w" s="T255">baːrɨj</ts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_963" n="HIAT:w" s="T256">ulakannarɨn</ts>
                  <nts id="Seg_964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_966" n="HIAT:w" s="T257">dʼi͡ete</ts>
                  <nts id="Seg_967" n="HIAT:ip">,</nts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_970" n="HIAT:w" s="T258">onno</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_973" n="HIAT:w" s="T259">haːmaj</ts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_976" n="HIAT:w" s="T260">dʼaktara</ts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_979" n="HIAT:w" s="T261">baːr</ts>
                  <nts id="Seg_980" n="HIAT:ip">.</nts>
                  <nts id="Seg_981" n="HIAT:ip">"</nts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T268" id="Seg_984" n="HIAT:u" s="T262">
                  <ts e="T263" id="Seg_986" n="HIAT:w" s="T262">Bu</ts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_989" n="HIAT:w" s="T263">kihi</ts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_992" n="HIAT:w" s="T264">kɨːhɨra-kɨːhɨra</ts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_995" n="HIAT:w" s="T265">haŋarbɨt</ts>
                  <nts id="Seg_996" n="HIAT:ip">,</nts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_999" n="HIAT:w" s="T266">ɨjbɨt</ts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1002" n="HIAT:w" s="T267">dʼi͡etin</ts>
                  <nts id="Seg_1003" n="HIAT:ip">.</nts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T276" id="Seg_1006" n="HIAT:u" s="T268">
                  <ts e="T269" id="Seg_1008" n="HIAT:w" s="T268">Toŋus</ts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1011" n="HIAT:w" s="T269">kiːrbite</ts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1013" n="HIAT:ip">—</nts>
                  <nts id="Seg_1014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1016" n="HIAT:w" s="T270">dʼaktar</ts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_1019" n="HIAT:w" s="T271">karaga</ts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1022" n="HIAT:w" s="T272">ɨtaːn</ts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1025" n="HIAT:w" s="T273">komu͡os</ts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1028" n="HIAT:w" s="T274">haga</ts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1031" n="HIAT:w" s="T275">bu͡olbut</ts>
                  <nts id="Seg_1032" n="HIAT:ip">.</nts>
                  <nts id="Seg_1033" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T282" id="Seg_1035" n="HIAT:u" s="T276">
                  <ts e="T277" id="Seg_1037" n="HIAT:w" s="T276">Toŋus</ts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1040" n="HIAT:w" s="T277">kiːrbit</ts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1043" n="HIAT:w" s="T278">da</ts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1046" n="HIAT:w" s="T279">ulakannarɨn</ts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1049" n="HIAT:w" s="T280">dʼi͡ek</ts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1052" n="HIAT:w" s="T281">kaːmpɨt</ts>
                  <nts id="Seg_1053" n="HIAT:ip">:</nts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T288" id="Seg_1056" n="HIAT:u" s="T282">
                  <nts id="Seg_1057" n="HIAT:ip">"</nts>
                  <ts e="T283" id="Seg_1059" n="HIAT:w" s="T282">Min</ts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1062" n="HIAT:w" s="T283">dʼaktarɨm</ts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1065" n="HIAT:w" s="T284">baltɨtɨn</ts>
                  <nts id="Seg_1066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1068" n="HIAT:w" s="T285">dʼaktar</ts>
                  <nts id="Seg_1069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1071" n="HIAT:w" s="T286">gɨnan</ts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1074" n="HIAT:w" s="T287">olorogun</ts>
                  <nts id="Seg_1075" n="HIAT:ip">.</nts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T293" id="Seg_1078" n="HIAT:u" s="T288">
                  <ts e="T289" id="Seg_1080" n="HIAT:w" s="T288">Min</ts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1083" n="HIAT:w" s="T289">dʼaktarɨm</ts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1086" n="HIAT:w" s="T290">baltɨtɨn</ts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1089" n="HIAT:w" s="T291">körsü͡ön</ts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1092" n="HIAT:w" s="T292">bagarar</ts>
                  <nts id="Seg_1093" n="HIAT:ip">.</nts>
                  <nts id="Seg_1094" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T300" id="Seg_1096" n="HIAT:u" s="T293">
                  <nts id="Seg_1097" n="HIAT:ip">"</nts>
                  <ts e="T294" id="Seg_1099" n="HIAT:w" s="T293">Če</ts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1102" n="HIAT:w" s="T294">bert</ts>
                  <nts id="Seg_1103" n="HIAT:ip">,</nts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1106" n="HIAT:w" s="T295">harsɨn</ts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1109" n="HIAT:w" s="T296">barɨ͡akpɨt</ts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1112" n="HIAT:w" s="T297">körö</ts>
                  <nts id="Seg_1113" n="HIAT:ip">"</nts>
                  <nts id="Seg_1114" n="HIAT:ip">,</nts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1117" n="HIAT:w" s="T298">dʼi͡ebit</ts>
                  <nts id="Seg_1118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1120" n="HIAT:w" s="T299">čaːŋiːt</ts>
                  <nts id="Seg_1121" n="HIAT:ip">.</nts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T306" id="Seg_1124" n="HIAT:u" s="T300">
                  <ts e="T301" id="Seg_1126" n="HIAT:w" s="T300">Harsɨŋŋɨtɨn</ts>
                  <nts id="Seg_1127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1129" n="HIAT:w" s="T301">bu</ts>
                  <nts id="Seg_1130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1132" n="HIAT:w" s="T302">dʼaktarɨ</ts>
                  <nts id="Seg_1133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1135" n="HIAT:w" s="T303">čaːŋiːt</ts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1138" n="HIAT:w" s="T304">egelen</ts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1141" n="HIAT:w" s="T305">kelbit</ts>
                  <nts id="Seg_1142" n="HIAT:ip">.</nts>
                  <nts id="Seg_1143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T315" id="Seg_1145" n="HIAT:u" s="T306">
                  <ts e="T307" id="Seg_1147" n="HIAT:w" s="T306">Dʼi͡ege</ts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1150" n="HIAT:w" s="T307">kiːrbitterin</ts>
                  <nts id="Seg_1151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1153" n="HIAT:w" s="T308">kenne</ts>
                  <nts id="Seg_1154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1156" n="HIAT:w" s="T309">haːmaj</ts>
                  <nts id="Seg_1157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1159" n="HIAT:w" s="T310">čaːŋiːt</ts>
                  <nts id="Seg_1160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1162" n="HIAT:w" s="T311">tabalarɨn</ts>
                  <nts id="Seg_1163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1165" n="HIAT:w" s="T312">bɨ͡atɨn</ts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1168" n="HIAT:w" s="T313">bɨha</ts>
                  <nts id="Seg_1169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1171" n="HIAT:w" s="T314">battaːbɨt</ts>
                  <nts id="Seg_1172" n="HIAT:ip">.</nts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T319" id="Seg_1175" n="HIAT:u" s="T315">
                  <ts e="T316" id="Seg_1177" n="HIAT:w" s="T315">Dʼaktarɨn</ts>
                  <nts id="Seg_1178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1180" n="HIAT:w" s="T316">meŋesten</ts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1183" n="HIAT:w" s="T317">baran</ts>
                  <nts id="Seg_1184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1186" n="HIAT:w" s="T318">küreːbit</ts>
                  <nts id="Seg_1187" n="HIAT:ip">.</nts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T323" id="Seg_1190" n="HIAT:u" s="T319">
                  <ts e="T320" id="Seg_1192" n="HIAT:w" s="T319">Čaːŋiːt</ts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1195" n="HIAT:w" s="T320">kennitten</ts>
                  <nts id="Seg_1196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1198" n="HIAT:w" s="T321">hatɨː</ts>
                  <nts id="Seg_1199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1201" n="HIAT:w" s="T322">bappɨt</ts>
                  <nts id="Seg_1202" n="HIAT:ip">.</nts>
                  <nts id="Seg_1203" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T327" id="Seg_1205" n="HIAT:u" s="T323">
                  <ts e="T324" id="Seg_1207" n="HIAT:w" s="T323">Haːmaj</ts>
                  <nts id="Seg_1208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1210" n="HIAT:w" s="T324">bɨlčɨŋɨn</ts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1213" n="HIAT:w" s="T325">hiːre</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1216" n="HIAT:w" s="T326">ɨttarbɨt</ts>
                  <nts id="Seg_1217" n="HIAT:ip">.</nts>
                  <nts id="Seg_1218" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T338" id="Seg_1220" n="HIAT:u" s="T327">
                  <ts e="T328" id="Seg_1222" n="HIAT:w" s="T327">Toktuː</ts>
                  <nts id="Seg_1223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1225" n="HIAT:w" s="T328">tühen</ts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1228" n="HIAT:w" s="T329">baran</ts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1231" n="HIAT:w" s="T330">haːmaj</ts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1234" n="HIAT:w" s="T331">utarɨ</ts>
                  <nts id="Seg_1235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1237" n="HIAT:w" s="T332">kaːmpɨt</ts>
                  <nts id="Seg_1238" n="HIAT:ip">,</nts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1241" n="HIAT:w" s="T333">čaːŋiːt</ts>
                  <nts id="Seg_1242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1244" n="HIAT:w" s="T334">kulgaːgɨn</ts>
                  <nts id="Seg_1245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1247" n="HIAT:w" s="T335">tahɨnan</ts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1250" n="HIAT:w" s="T336">kaja</ts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1253" n="HIAT:w" s="T337">keːspit</ts>
                  <nts id="Seg_1254" n="HIAT:ip">.</nts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T344" id="Seg_1257" n="HIAT:u" s="T338">
                  <ts e="T339" id="Seg_1259" n="HIAT:w" s="T338">Čaːŋiːt</ts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1262" n="HIAT:w" s="T339">kustugun</ts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1265" n="HIAT:w" s="T340">barɨtɨn</ts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1268" n="HIAT:w" s="T341">ɨppɨt</ts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1271" n="HIAT:w" s="T342">da</ts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1274" n="HIAT:w" s="T343">küreːbit</ts>
                  <nts id="Seg_1275" n="HIAT:ip">.</nts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T354" id="Seg_1278" n="HIAT:u" s="T344">
                  <ts e="T345" id="Seg_1280" n="HIAT:w" s="T344">Toŋühü</ts>
                  <nts id="Seg_1281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1283" n="HIAT:w" s="T345">gɨtta</ts>
                  <nts id="Seg_1284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1286" n="HIAT:w" s="T346">haːmaj</ts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1289" n="HIAT:w" s="T347">bappɨttar</ts>
                  <nts id="Seg_1290" n="HIAT:ip">,</nts>
                  <nts id="Seg_1291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1293" n="HIAT:w" s="T348">hite</ts>
                  <nts id="Seg_1294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1296" n="HIAT:w" s="T349">battaːn</ts>
                  <nts id="Seg_1297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1299" n="HIAT:w" s="T350">baran</ts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1302" n="HIAT:w" s="T351">menʼiːtin</ts>
                  <nts id="Seg_1303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1305" n="HIAT:w" s="T352">kaja</ts>
                  <nts id="Seg_1306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1308" n="HIAT:w" s="T353">oksubuttar</ts>
                  <nts id="Seg_1309" n="HIAT:ip">.</nts>
                  <nts id="Seg_1310" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T362" id="Seg_1312" n="HIAT:u" s="T354">
                  <ts e="T355" id="Seg_1314" n="HIAT:w" s="T354">Ol</ts>
                  <nts id="Seg_1315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1317" n="HIAT:w" s="T355">kennitten</ts>
                  <nts id="Seg_1318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1320" n="HIAT:w" s="T356">toŋus</ts>
                  <nts id="Seg_1321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1323" n="HIAT:w" s="T357">dʼaktardammɨt</ts>
                  <nts id="Seg_1324" n="HIAT:ip">,</nts>
                  <nts id="Seg_1325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1327" n="HIAT:w" s="T358">čaːŋiːt</ts>
                  <nts id="Seg_1328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1330" n="HIAT:w" s="T359">tabalarɨnan</ts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1333" n="HIAT:w" s="T360">bajan-toton</ts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1336" n="HIAT:w" s="T361">olorbuttar</ts>
                  <nts id="Seg_1337" n="HIAT:ip">.</nts>
                  <nts id="Seg_1338" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T369" id="Seg_1340" n="HIAT:u" s="T362">
                  <ts e="T363" id="Seg_1342" n="HIAT:w" s="T362">Bu</ts>
                  <nts id="Seg_1343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1345" n="HIAT:w" s="T363">kördük</ts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1348" n="HIAT:w" s="T364">haːmajdarɨ</ts>
                  <nts id="Seg_1349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1351" n="HIAT:w" s="T365">gɨtta</ts>
                  <nts id="Seg_1352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1354" n="HIAT:w" s="T366">toŋustar</ts>
                  <nts id="Seg_1355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1357" n="HIAT:w" s="T367">kolbohon</ts>
                  <nts id="Seg_1358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1360" n="HIAT:w" s="T368">olorbuttar</ts>
                  <nts id="Seg_1361" n="HIAT:ip">.</nts>
                  <nts id="Seg_1362" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T377" id="Seg_1364" n="HIAT:u" s="T369">
                  <ts e="T370" id="Seg_1366" n="HIAT:w" s="T369">Haːmajdar</ts>
                  <nts id="Seg_1367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1369" n="HIAT:w" s="T370">ol</ts>
                  <nts id="Seg_1370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1372" n="HIAT:w" s="T371">kennitten</ts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1375" n="HIAT:w" s="T372">kuttanan</ts>
                  <nts id="Seg_1376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1378" n="HIAT:w" s="T373">Taːs</ts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1381" n="HIAT:w" s="T374">dʼi͡ek</ts>
                  <nts id="Seg_1382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1384" n="HIAT:w" s="T375">köspöt</ts>
                  <nts id="Seg_1385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1387" n="HIAT:w" s="T376">bu͡olbuttar</ts>
                  <nts id="Seg_1388" n="HIAT:ip">.</nts>
                  <nts id="Seg_1389" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T377" id="Seg_1390" n="sc" s="T0">
               <ts e="T1" id="Seg_1392" n="e" s="T0">Haːmajdar </ts>
               <ts e="T2" id="Seg_1394" n="e" s="T1">bɨlɨr-bɨlɨr </ts>
               <ts e="T3" id="Seg_1396" n="e" s="T2">Kerikege </ts>
               <ts e="T4" id="Seg_1398" n="e" s="T3">oloror </ts>
               <ts e="T5" id="Seg_1400" n="e" s="T4">ebittere, </ts>
               <ts e="T6" id="Seg_1402" n="e" s="T5">Dʼehejge — </ts>
               <ts e="T7" id="Seg_1404" n="e" s="T6">čaːŋiːttar, </ts>
               <ts e="T8" id="Seg_1406" n="e" s="T7">Taːska — </ts>
               <ts e="T9" id="Seg_1408" n="e" s="T8">toŋustar. </ts>
               <ts e="T10" id="Seg_1410" n="e" s="T9">Araj </ts>
               <ts e="T11" id="Seg_1412" n="e" s="T10">biːrde </ts>
               <ts e="T12" id="Seg_1414" n="e" s="T11">biːr </ts>
               <ts e="T13" id="Seg_1416" n="e" s="T12">haːmaj </ts>
               <ts e="T14" id="Seg_1418" n="e" s="T13">dʼaktarɨn </ts>
               <ts e="T15" id="Seg_1420" n="e" s="T14">čaːŋiːttar </ts>
               <ts e="T16" id="Seg_1422" n="e" s="T15">u͡orbuttar. </ts>
               <ts e="T17" id="Seg_1424" n="e" s="T16">Bu </ts>
               <ts e="T18" id="Seg_1426" n="e" s="T17">dʼaktar, </ts>
               <ts e="T19" id="Seg_1428" n="e" s="T18">ere </ts>
               <ts e="T20" id="Seg_1430" n="e" s="T19">kɨːllana </ts>
               <ts e="T21" id="Seg_1432" n="e" s="T20">barbɨtɨn </ts>
               <ts e="T22" id="Seg_1434" n="e" s="T21">kenne, </ts>
               <ts e="T23" id="Seg_1436" n="e" s="T22">iːstene </ts>
               <ts e="T24" id="Seg_1438" n="e" s="T23">olorbut. </ts>
               <ts e="T25" id="Seg_1440" n="e" s="T24">Ol </ts>
               <ts e="T26" id="Seg_1442" n="e" s="T25">olordoguna, </ts>
               <ts e="T27" id="Seg_1444" n="e" s="T26">ɨttar </ts>
               <ts e="T28" id="Seg_1446" n="e" s="T27">ürbütter. </ts>
               <ts e="T29" id="Seg_1448" n="e" s="T28">"Iččilerin </ts>
               <ts e="T30" id="Seg_1450" n="e" s="T29">ɨttar </ts>
               <ts e="T31" id="Seg_1452" n="e" s="T30">ürü͡ö </ts>
               <ts e="T32" id="Seg_1454" n="e" s="T31">hu͡ok </ts>
               <ts e="T33" id="Seg_1456" n="e" s="T32">etilere", </ts>
               <ts e="T34" id="Seg_1458" n="e" s="T33">dʼiː </ts>
               <ts e="T35" id="Seg_1460" n="e" s="T34">hanaːbɨt. </ts>
               <ts e="T36" id="Seg_1462" n="e" s="T35">U͡otun </ts>
               <ts e="T37" id="Seg_1464" n="e" s="T36">ottoːru </ts>
               <ts e="T38" id="Seg_1466" n="e" s="T37">gɨnarɨn </ts>
               <ts e="T39" id="Seg_1468" n="e" s="T38">gɨtta, </ts>
               <ts e="T40" id="Seg_1470" n="e" s="T39">üs </ts>
               <ts e="T41" id="Seg_1472" n="e" s="T40">kihi </ts>
               <ts e="T42" id="Seg_1474" n="e" s="T41">kiːrbit </ts>
               <ts e="T43" id="Seg_1476" n="e" s="T42">da, </ts>
               <ts e="T44" id="Seg_1478" n="e" s="T43">dʼaktarɨ </ts>
               <ts e="T45" id="Seg_1480" n="e" s="T44">torgogo </ts>
               <ts e="T46" id="Seg_1482" n="e" s="T45">huːlaːn </ts>
               <ts e="T47" id="Seg_1484" n="e" s="T46">baran, </ts>
               <ts e="T48" id="Seg_1486" n="e" s="T47">ildʼe </ts>
               <ts e="T49" id="Seg_1488" n="e" s="T48">barbɨttar. </ts>
               <ts e="T50" id="Seg_1490" n="e" s="T49">Ki͡ehe </ts>
               <ts e="T51" id="Seg_1492" n="e" s="T50">haːmaj </ts>
               <ts e="T52" id="Seg_1494" n="e" s="T51">kelbite, </ts>
               <ts e="T53" id="Seg_1496" n="e" s="T52">ɨttara </ts>
               <ts e="T54" id="Seg_1498" n="e" s="T53">baːjɨlla </ts>
               <ts e="T55" id="Seg_1500" n="e" s="T54">hɨtallar, </ts>
               <ts e="T56" id="Seg_1502" n="e" s="T55">dʼi͡ete </ts>
               <ts e="T57" id="Seg_1504" n="e" s="T56">kam </ts>
               <ts e="T58" id="Seg_1506" n="e" s="T57">toŋmut, </ts>
               <ts e="T59" id="Seg_1508" n="e" s="T58">kim </ts>
               <ts e="T60" id="Seg_1510" n="e" s="T59">da </ts>
               <ts e="T61" id="Seg_1512" n="e" s="T60">hu͡ok, </ts>
               <ts e="T62" id="Seg_1514" n="e" s="T61">kap-karaŋa. </ts>
               <ts e="T63" id="Seg_1516" n="e" s="T62">"Ajɨː </ts>
               <ts e="T64" id="Seg_1518" n="e" s="T63">dʼiː, </ts>
               <ts e="T65" id="Seg_1520" n="e" s="T64">dʼaktarɨm </ts>
               <ts e="T66" id="Seg_1522" n="e" s="T65">kajdi͡ek </ts>
               <ts e="T67" id="Seg_1524" n="e" s="T66">barbɨta </ts>
               <ts e="T68" id="Seg_1526" n="e" s="T67">bu͡olu͡oj", </ts>
               <ts e="T69" id="Seg_1528" n="e" s="T68">hanaːrgaːbɨt. </ts>
               <ts e="T70" id="Seg_1530" n="e" s="T69">Harsi͡erde </ts>
               <ts e="T71" id="Seg_1532" n="e" s="T70">hɨrdɨːrɨn </ts>
               <ts e="T72" id="Seg_1534" n="e" s="T71">gɨtta </ts>
               <ts e="T73" id="Seg_1536" n="e" s="T72">ü͡örün </ts>
               <ts e="T74" id="Seg_1538" n="e" s="T73">egele </ts>
               <ts e="T75" id="Seg_1540" n="e" s="T74">barbɨt. </ts>
               <ts e="T76" id="Seg_1542" n="e" s="T75">Tabata </ts>
               <ts e="T77" id="Seg_1544" n="e" s="T76">barɨta </ts>
               <ts e="T78" id="Seg_1546" n="e" s="T77">baːr </ts>
               <ts e="T79" id="Seg_1548" n="e" s="T78">ebit. </ts>
               <ts e="T80" id="Seg_1550" n="e" s="T79">"Min </ts>
               <ts e="T81" id="Seg_1552" n="e" s="T80">toŋuspar </ts>
               <ts e="T82" id="Seg_1554" n="e" s="T81">köhü͡öm, </ts>
               <ts e="T83" id="Seg_1556" n="e" s="T82">tugu </ts>
               <ts e="T84" id="Seg_1558" n="e" s="T83">gɨnɨ͡amɨj </ts>
               <ts e="T85" id="Seg_1560" n="e" s="T84">hogotogun? </ts>
               <ts e="T86" id="Seg_1562" n="e" s="T85">Hübelehi͡em </ts>
               <ts e="T87" id="Seg_1564" n="e" s="T86">itte", </ts>
               <ts e="T88" id="Seg_1566" n="e" s="T87">dʼi͡en </ts>
               <ts e="T89" id="Seg_1568" n="e" s="T88">hanaːbɨt. </ts>
               <ts e="T90" id="Seg_1570" n="e" s="T89">Čor </ts>
               <ts e="T91" id="Seg_1572" n="e" s="T90">hogotok </ts>
               <ts e="T92" id="Seg_1574" n="e" s="T91">kihi </ts>
               <ts e="T93" id="Seg_1576" n="e" s="T92">ör </ts>
               <ts e="T94" id="Seg_1578" n="e" s="T93">komunu͡oj — </ts>
               <ts e="T95" id="Seg_1580" n="e" s="T94">toŋuska </ts>
               <ts e="T96" id="Seg_1582" n="e" s="T95">köhön </ts>
               <ts e="T97" id="Seg_1584" n="e" s="T96">kelbite, </ts>
               <ts e="T98" id="Seg_1586" n="e" s="T97">kihite </ts>
               <ts e="T99" id="Seg_1588" n="e" s="T98">ilimnene </ts>
               <ts e="T100" id="Seg_1590" n="e" s="T99">barbɨt </ts>
               <ts e="T101" id="Seg_1592" n="e" s="T100">ebit, </ts>
               <ts e="T102" id="Seg_1594" n="e" s="T101">inʼete </ts>
               <ts e="T103" id="Seg_1596" n="e" s="T102">ire </ts>
               <ts e="T104" id="Seg_1598" n="e" s="T103">baːr. </ts>
               <ts e="T105" id="Seg_1600" n="e" s="T104">Toŋus </ts>
               <ts e="T106" id="Seg_1602" n="e" s="T105">hoč-hogotogun </ts>
               <ts e="T107" id="Seg_1604" n="e" s="T106">oloror </ts>
               <ts e="T108" id="Seg_1606" n="e" s="T107">ebit. </ts>
               <ts e="T109" id="Seg_1608" n="e" s="T108">Ki͡ehe </ts>
               <ts e="T110" id="Seg_1610" n="e" s="T109">kelbit. </ts>
               <ts e="T111" id="Seg_1612" n="e" s="T110">Haːmaj </ts>
               <ts e="T112" id="Seg_1614" n="e" s="T111">erejdeːk </ts>
               <ts e="T113" id="Seg_1616" n="e" s="T112">kepseːbit </ts>
               <ts e="T114" id="Seg_1618" n="e" s="T113">dʼaktara </ts>
               <ts e="T115" id="Seg_1620" n="e" s="T114">hüppütün. </ts>
               <ts e="T116" id="Seg_1622" n="e" s="T115">"Če, </ts>
               <ts e="T117" id="Seg_1624" n="e" s="T116">kim </ts>
               <ts e="T118" id="Seg_1626" n="e" s="T117">gɨnɨ͡aj, </ts>
               <ts e="T119" id="Seg_1628" n="e" s="T118">begeheː </ts>
               <ts e="T120" id="Seg_1630" n="e" s="T119">minʼi͡eke </ts>
               <ts e="T121" id="Seg_1632" n="e" s="T120">üs </ts>
               <ts e="T122" id="Seg_1634" n="e" s="T121">kihi </ts>
               <ts e="T123" id="Seg_1636" n="e" s="T122">baːr </ts>
               <ts e="T124" id="Seg_1638" n="e" s="T123">etilere. </ts>
               <ts e="T125" id="Seg_1640" n="e" s="T124">Körüŋŋere </ts>
               <ts e="T126" id="Seg_1642" n="e" s="T125">hoččoto </ts>
               <ts e="T127" id="Seg_1644" n="e" s="T126">hu͡ok </ts>
               <ts e="T128" id="Seg_1646" n="e" s="T127">ete — </ts>
               <ts e="T129" id="Seg_1648" n="e" s="T128">araːha </ts>
               <ts e="T130" id="Seg_1650" n="e" s="T129">čaːŋiːttar </ts>
               <ts e="T131" id="Seg_1652" n="e" s="T130">bɨhɨːlaːk. </ts>
               <ts e="T132" id="Seg_1654" n="e" s="T131">Minʼigitten </ts>
               <ts e="T133" id="Seg_1656" n="e" s="T132">tu͡okpun </ts>
               <ts e="T134" id="Seg_1658" n="e" s="T133">ɨlɨ͡aktaraj?" </ts>
               <ts e="T135" id="Seg_1660" n="e" s="T134">"Čugas </ts>
               <ts e="T136" id="Seg_1662" n="e" s="T135">baːr </ts>
               <ts e="T137" id="Seg_1664" n="e" s="T136">ɨ͡allar </ts>
               <ts e="T138" id="Seg_1666" n="e" s="T137">emi͡e </ts>
               <ts e="T139" id="Seg_1668" n="e" s="T138">kepsiːller </ts>
               <ts e="T140" id="Seg_1670" n="e" s="T139">kajtak </ts>
               <ts e="T141" id="Seg_1672" n="e" s="T140">giniler </ts>
               <ts e="T142" id="Seg_1674" n="e" s="T141">dʼaktattarɨ </ts>
               <ts e="T143" id="Seg_1676" n="e" s="T142">u͡ora </ts>
               <ts e="T144" id="Seg_1678" n="e" s="T143">hɨldʼallarɨn", </ts>
               <ts e="T145" id="Seg_1680" n="e" s="T144">toŋus </ts>
               <ts e="T146" id="Seg_1682" n="e" s="T145">dʼi͡ebit. </ts>
               <ts e="T147" id="Seg_1684" n="e" s="T146">"En </ts>
               <ts e="T148" id="Seg_1686" n="e" s="T147">töhö </ts>
               <ts e="T149" id="Seg_1688" n="e" s="T148">kohuːŋŋunuj", </ts>
               <ts e="T150" id="Seg_1690" n="e" s="T149">toŋus </ts>
               <ts e="T151" id="Seg_1692" n="e" s="T150">dogorun </ts>
               <ts e="T152" id="Seg_1694" n="e" s="T151">ɨjɨppɨt. </ts>
               <ts e="T153" id="Seg_1696" n="e" s="T152">"Kohuːn </ts>
               <ts e="T154" id="Seg_1698" n="e" s="T153">bu͡olammɨn </ts>
               <ts e="T155" id="Seg_1700" n="e" s="T154">hɨldʼar </ts>
               <ts e="T156" id="Seg_1702" n="e" s="T155">enʼibin", </ts>
               <ts e="T157" id="Seg_1704" n="e" s="T156">dʼi͡ebit </ts>
               <ts e="T158" id="Seg_1706" n="e" s="T157">haːmaja. </ts>
               <ts e="T159" id="Seg_1708" n="e" s="T158">"Oččogo </ts>
               <ts e="T160" id="Seg_1710" n="e" s="T159">min </ts>
               <ts e="T161" id="Seg_1712" n="e" s="T160">enʼigin </ts>
               <ts e="T162" id="Seg_1714" n="e" s="T161">bili͡em, </ts>
               <ts e="T163" id="Seg_1716" n="e" s="T162">töhö </ts>
               <ts e="T164" id="Seg_1718" n="e" s="T163">berkinij. </ts>
               <ts e="T165" id="Seg_1720" n="e" s="T164">Min </ts>
               <ts e="T166" id="Seg_1722" n="e" s="T165">enʼigin </ts>
               <ts e="T167" id="Seg_1724" n="e" s="T166">üs </ts>
               <ts e="T168" id="Seg_1726" n="e" s="T167">ɨt </ts>
               <ts e="T169" id="Seg_1728" n="e" s="T168">hɨrgata </ts>
               <ts e="T170" id="Seg_1730" n="e" s="T169">kuraŋɨttan </ts>
               <ts e="T171" id="Seg_1732" n="e" s="T170">ɨtɨ͡am." </ts>
               <ts e="T172" id="Seg_1734" n="e" s="T171">Toŋus </ts>
               <ts e="T173" id="Seg_1736" n="e" s="T172">üste </ts>
               <ts e="T174" id="Seg_1738" n="e" s="T173">ɨppɨt, </ts>
               <ts e="T175" id="Seg_1740" n="e" s="T174">eŋerin </ts>
               <ts e="T176" id="Seg_1742" n="e" s="T175">ire </ts>
               <ts e="T177" id="Seg_1744" n="e" s="T176">kɨrɨːtɨn </ts>
               <ts e="T178" id="Seg_1746" n="e" s="T177">hiːre </ts>
               <ts e="T179" id="Seg_1748" n="e" s="T178">kötüppüt. </ts>
               <ts e="T180" id="Seg_1750" n="e" s="T179">"Anɨ </ts>
               <ts e="T181" id="Seg_1752" n="e" s="T180">en </ts>
               <ts e="T182" id="Seg_1754" n="e" s="T181">ɨt", </ts>
               <ts e="T183" id="Seg_1756" n="e" s="T182">dʼi͡ebit. </ts>
               <ts e="T184" id="Seg_1758" n="e" s="T183">Haːmaj </ts>
               <ts e="T185" id="Seg_1760" n="e" s="T184">ɨppɨt — </ts>
               <ts e="T186" id="Seg_1762" n="e" s="T185">tappatak. </ts>
               <ts e="T187" id="Seg_1764" n="e" s="T186">"Dʼe </ts>
               <ts e="T188" id="Seg_1766" n="e" s="T187">bert. </ts>
               <ts e="T189" id="Seg_1768" n="e" s="T188">Kohuːŋŋun </ts>
               <ts e="T190" id="Seg_1770" n="e" s="T189">ebit", </ts>
               <ts e="T191" id="Seg_1772" n="e" s="T190">toŋuha </ts>
               <ts e="T192" id="Seg_1774" n="e" s="T191">kajgaːbɨt. </ts>
               <ts e="T193" id="Seg_1776" n="e" s="T192">Harsɨŋŋɨtɨn </ts>
               <ts e="T194" id="Seg_1778" n="e" s="T193">köspütter. </ts>
               <ts e="T195" id="Seg_1780" n="e" s="T194">Üs </ts>
               <ts e="T196" id="Seg_1782" n="e" s="T195">taːhɨ </ts>
               <ts e="T197" id="Seg_1784" n="e" s="T196">bɨspɨttar. </ts>
               <ts e="T198" id="Seg_1786" n="e" s="T197">"Dʼe </ts>
               <ts e="T199" id="Seg_1788" n="e" s="T198">min </ts>
               <ts e="T200" id="Seg_1790" n="e" s="T199">inʼebin </ts>
               <ts e="T201" id="Seg_1792" n="e" s="T200">haːmajdɨː </ts>
               <ts e="T202" id="Seg_1794" n="e" s="T201">taŋɨnnaran </ts>
               <ts e="T203" id="Seg_1796" n="e" s="T202">baran </ts>
               <ts e="T204" id="Seg_1798" n="e" s="T203">barɨ͡am. </ts>
               <ts e="T205" id="Seg_1800" n="e" s="T204">Min </ts>
               <ts e="T206" id="Seg_1802" n="e" s="T205">ginileri </ts>
               <ts e="T207" id="Seg_1804" n="e" s="T206">albɨnnɨ͡am. </ts>
               <ts e="T208" id="Seg_1806" n="e" s="T207">Dʼi͡em </ts>
               <ts e="T209" id="Seg_1808" n="e" s="T208">'bu </ts>
               <ts e="T210" id="Seg_1810" n="e" s="T209">dʼaktar — </ts>
               <ts e="T211" id="Seg_1812" n="e" s="T210">ehigi </ts>
               <ts e="T212" id="Seg_1814" n="e" s="T211">ɨlaːččɨ </ts>
               <ts e="T213" id="Seg_1816" n="e" s="T212">dʼaktargɨt </ts>
               <ts e="T214" id="Seg_1818" n="e" s="T213">edʼije, </ts>
               <ts e="T215" id="Seg_1820" n="e" s="T214">bu </ts>
               <ts e="T216" id="Seg_1822" n="e" s="T215">dʼaktar </ts>
               <ts e="T217" id="Seg_1824" n="e" s="T216">min </ts>
               <ts e="T218" id="Seg_1826" n="e" s="T217">dʼaktarɨm'. </ts>
               <ts e="T219" id="Seg_1828" n="e" s="T218">En </ts>
               <ts e="T220" id="Seg_1830" n="e" s="T219">bu͡ollagɨna </ts>
               <ts e="T221" id="Seg_1832" n="e" s="T220">min </ts>
               <ts e="T222" id="Seg_1834" n="e" s="T221">dʼaktarɨm </ts>
               <ts e="T223" id="Seg_1836" n="e" s="T222">baltɨta </ts>
               <ts e="T224" id="Seg_1838" n="e" s="T223">bu͡olu͡oŋ. </ts>
               <ts e="T225" id="Seg_1840" n="e" s="T224">Toŋus </ts>
               <ts e="T226" id="Seg_1842" n="e" s="T225">inʼetin </ts>
               <ts e="T227" id="Seg_1844" n="e" s="T226">kɨtta </ts>
               <ts e="T228" id="Seg_1846" n="e" s="T227">baran </ts>
               <ts e="T229" id="Seg_1848" n="e" s="T228">kaːlbɨttar. </ts>
               <ts e="T230" id="Seg_1850" n="e" s="T229">Tijbittere, </ts>
               <ts e="T231" id="Seg_1852" n="e" s="T230">hette </ts>
               <ts e="T232" id="Seg_1854" n="e" s="T231">uraha </ts>
               <ts e="T233" id="Seg_1856" n="e" s="T232">dʼi͡e </ts>
               <ts e="T234" id="Seg_1858" n="e" s="T233">turallar. </ts>
               <ts e="T235" id="Seg_1860" n="e" s="T234">Onu </ts>
               <ts e="T236" id="Seg_1862" n="e" s="T235">tögürüččü </ts>
               <ts e="T237" id="Seg_1864" n="e" s="T236">kihiler </ts>
               <ts e="T238" id="Seg_1866" n="e" s="T237">üŋküːlüː </ts>
               <ts e="T239" id="Seg_1868" n="e" s="T238">hɨldʼallar. </ts>
               <ts e="T240" id="Seg_1870" n="e" s="T239">"Araːha, </ts>
               <ts e="T241" id="Seg_1872" n="e" s="T240">kurum </ts>
               <ts e="T242" id="Seg_1874" n="e" s="T241">bɨhɨlaːk", </ts>
               <ts e="T243" id="Seg_1876" n="e" s="T242">dʼiː </ts>
               <ts e="T244" id="Seg_1878" n="e" s="T243">hanaːbɨttar. </ts>
               <ts e="T245" id="Seg_1880" n="e" s="T244">Toŋus </ts>
               <ts e="T246" id="Seg_1882" n="e" s="T245">biːr </ts>
               <ts e="T247" id="Seg_1884" n="e" s="T246">kihini </ts>
               <ts e="T248" id="Seg_1886" n="e" s="T247">kuːsput </ts>
               <ts e="T249" id="Seg_1888" n="e" s="T248">da </ts>
               <ts e="T250" id="Seg_1890" n="e" s="T249">üŋküːlüː </ts>
               <ts e="T251" id="Seg_1892" n="e" s="T250">hɨldʼɨbɨttar. </ts>
               <ts e="T252" id="Seg_1894" n="e" s="T251">Ol </ts>
               <ts e="T253" id="Seg_1896" n="e" s="T252">hɨldʼan </ts>
               <ts e="T254" id="Seg_1898" n="e" s="T253">ɨjɨppɨt: </ts>
               <ts e="T255" id="Seg_1900" n="e" s="T254">"Kanna </ts>
               <ts e="T256" id="Seg_1902" n="e" s="T255">baːrɨj </ts>
               <ts e="T257" id="Seg_1904" n="e" s="T256">ulakannarɨn </ts>
               <ts e="T258" id="Seg_1906" n="e" s="T257">dʼi͡ete, </ts>
               <ts e="T259" id="Seg_1908" n="e" s="T258">onno </ts>
               <ts e="T260" id="Seg_1910" n="e" s="T259">haːmaj </ts>
               <ts e="T261" id="Seg_1912" n="e" s="T260">dʼaktara </ts>
               <ts e="T262" id="Seg_1914" n="e" s="T261">baːr." </ts>
               <ts e="T263" id="Seg_1916" n="e" s="T262">Bu </ts>
               <ts e="T264" id="Seg_1918" n="e" s="T263">kihi </ts>
               <ts e="T265" id="Seg_1920" n="e" s="T264">kɨːhɨra-kɨːhɨra </ts>
               <ts e="T266" id="Seg_1922" n="e" s="T265">haŋarbɨt, </ts>
               <ts e="T267" id="Seg_1924" n="e" s="T266">ɨjbɨt </ts>
               <ts e="T268" id="Seg_1926" n="e" s="T267">dʼi͡etin. </ts>
               <ts e="T269" id="Seg_1928" n="e" s="T268">Toŋus </ts>
               <ts e="T270" id="Seg_1930" n="e" s="T269">kiːrbite — </ts>
               <ts e="T271" id="Seg_1932" n="e" s="T270">dʼaktar </ts>
               <ts e="T272" id="Seg_1934" n="e" s="T271">karaga </ts>
               <ts e="T273" id="Seg_1936" n="e" s="T272">ɨtaːn </ts>
               <ts e="T274" id="Seg_1938" n="e" s="T273">komu͡os </ts>
               <ts e="T275" id="Seg_1940" n="e" s="T274">haga </ts>
               <ts e="T276" id="Seg_1942" n="e" s="T275">bu͡olbut. </ts>
               <ts e="T277" id="Seg_1944" n="e" s="T276">Toŋus </ts>
               <ts e="T278" id="Seg_1946" n="e" s="T277">kiːrbit </ts>
               <ts e="T279" id="Seg_1948" n="e" s="T278">da </ts>
               <ts e="T280" id="Seg_1950" n="e" s="T279">ulakannarɨn </ts>
               <ts e="T281" id="Seg_1952" n="e" s="T280">dʼi͡ek </ts>
               <ts e="T282" id="Seg_1954" n="e" s="T281">kaːmpɨt: </ts>
               <ts e="T283" id="Seg_1956" n="e" s="T282">"Min </ts>
               <ts e="T284" id="Seg_1958" n="e" s="T283">dʼaktarɨm </ts>
               <ts e="T285" id="Seg_1960" n="e" s="T284">baltɨtɨn </ts>
               <ts e="T286" id="Seg_1962" n="e" s="T285">dʼaktar </ts>
               <ts e="T287" id="Seg_1964" n="e" s="T286">gɨnan </ts>
               <ts e="T288" id="Seg_1966" n="e" s="T287">olorogun. </ts>
               <ts e="T289" id="Seg_1968" n="e" s="T288">Min </ts>
               <ts e="T290" id="Seg_1970" n="e" s="T289">dʼaktarɨm </ts>
               <ts e="T291" id="Seg_1972" n="e" s="T290">baltɨtɨn </ts>
               <ts e="T292" id="Seg_1974" n="e" s="T291">körsü͡ön </ts>
               <ts e="T293" id="Seg_1976" n="e" s="T292">bagarar. </ts>
               <ts e="T294" id="Seg_1978" n="e" s="T293">"Če </ts>
               <ts e="T295" id="Seg_1980" n="e" s="T294">bert, </ts>
               <ts e="T296" id="Seg_1982" n="e" s="T295">harsɨn </ts>
               <ts e="T297" id="Seg_1984" n="e" s="T296">barɨ͡akpɨt </ts>
               <ts e="T298" id="Seg_1986" n="e" s="T297">körö", </ts>
               <ts e="T299" id="Seg_1988" n="e" s="T298">dʼi͡ebit </ts>
               <ts e="T300" id="Seg_1990" n="e" s="T299">čaːŋiːt. </ts>
               <ts e="T301" id="Seg_1992" n="e" s="T300">Harsɨŋŋɨtɨn </ts>
               <ts e="T302" id="Seg_1994" n="e" s="T301">bu </ts>
               <ts e="T303" id="Seg_1996" n="e" s="T302">dʼaktarɨ </ts>
               <ts e="T304" id="Seg_1998" n="e" s="T303">čaːŋiːt </ts>
               <ts e="T305" id="Seg_2000" n="e" s="T304">egelen </ts>
               <ts e="T306" id="Seg_2002" n="e" s="T305">kelbit. </ts>
               <ts e="T307" id="Seg_2004" n="e" s="T306">Dʼi͡ege </ts>
               <ts e="T308" id="Seg_2006" n="e" s="T307">kiːrbitterin </ts>
               <ts e="T309" id="Seg_2008" n="e" s="T308">kenne </ts>
               <ts e="T310" id="Seg_2010" n="e" s="T309">haːmaj </ts>
               <ts e="T311" id="Seg_2012" n="e" s="T310">čaːŋiːt </ts>
               <ts e="T312" id="Seg_2014" n="e" s="T311">tabalarɨn </ts>
               <ts e="T313" id="Seg_2016" n="e" s="T312">bɨ͡atɨn </ts>
               <ts e="T314" id="Seg_2018" n="e" s="T313">bɨha </ts>
               <ts e="T315" id="Seg_2020" n="e" s="T314">battaːbɨt. </ts>
               <ts e="T316" id="Seg_2022" n="e" s="T315">Dʼaktarɨn </ts>
               <ts e="T317" id="Seg_2024" n="e" s="T316">meŋesten </ts>
               <ts e="T318" id="Seg_2026" n="e" s="T317">baran </ts>
               <ts e="T319" id="Seg_2028" n="e" s="T318">küreːbit. </ts>
               <ts e="T320" id="Seg_2030" n="e" s="T319">Čaːŋiːt </ts>
               <ts e="T321" id="Seg_2032" n="e" s="T320">kennitten </ts>
               <ts e="T322" id="Seg_2034" n="e" s="T321">hatɨː </ts>
               <ts e="T323" id="Seg_2036" n="e" s="T322">bappɨt. </ts>
               <ts e="T324" id="Seg_2038" n="e" s="T323">Haːmaj </ts>
               <ts e="T325" id="Seg_2040" n="e" s="T324">bɨlčɨŋɨn </ts>
               <ts e="T326" id="Seg_2042" n="e" s="T325">hiːre </ts>
               <ts e="T327" id="Seg_2044" n="e" s="T326">ɨttarbɨt. </ts>
               <ts e="T328" id="Seg_2046" n="e" s="T327">Toktuː </ts>
               <ts e="T329" id="Seg_2048" n="e" s="T328">tühen </ts>
               <ts e="T330" id="Seg_2050" n="e" s="T329">baran </ts>
               <ts e="T331" id="Seg_2052" n="e" s="T330">haːmaj </ts>
               <ts e="T332" id="Seg_2054" n="e" s="T331">utarɨ </ts>
               <ts e="T333" id="Seg_2056" n="e" s="T332">kaːmpɨt, </ts>
               <ts e="T334" id="Seg_2058" n="e" s="T333">čaːŋiːt </ts>
               <ts e="T335" id="Seg_2060" n="e" s="T334">kulgaːgɨn </ts>
               <ts e="T336" id="Seg_2062" n="e" s="T335">tahɨnan </ts>
               <ts e="T337" id="Seg_2064" n="e" s="T336">kaja </ts>
               <ts e="T338" id="Seg_2066" n="e" s="T337">keːspit. </ts>
               <ts e="T339" id="Seg_2068" n="e" s="T338">Čaːŋiːt </ts>
               <ts e="T340" id="Seg_2070" n="e" s="T339">kustugun </ts>
               <ts e="T341" id="Seg_2072" n="e" s="T340">barɨtɨn </ts>
               <ts e="T342" id="Seg_2074" n="e" s="T341">ɨppɨt </ts>
               <ts e="T343" id="Seg_2076" n="e" s="T342">da </ts>
               <ts e="T344" id="Seg_2078" n="e" s="T343">küreːbit. </ts>
               <ts e="T345" id="Seg_2080" n="e" s="T344">Toŋühü </ts>
               <ts e="T346" id="Seg_2082" n="e" s="T345">gɨtta </ts>
               <ts e="T347" id="Seg_2084" n="e" s="T346">haːmaj </ts>
               <ts e="T348" id="Seg_2086" n="e" s="T347">bappɨttar, </ts>
               <ts e="T349" id="Seg_2088" n="e" s="T348">hite </ts>
               <ts e="T350" id="Seg_2090" n="e" s="T349">battaːn </ts>
               <ts e="T351" id="Seg_2092" n="e" s="T350">baran </ts>
               <ts e="T352" id="Seg_2094" n="e" s="T351">menʼiːtin </ts>
               <ts e="T353" id="Seg_2096" n="e" s="T352">kaja </ts>
               <ts e="T354" id="Seg_2098" n="e" s="T353">oksubuttar. </ts>
               <ts e="T355" id="Seg_2100" n="e" s="T354">Ol </ts>
               <ts e="T356" id="Seg_2102" n="e" s="T355">kennitten </ts>
               <ts e="T357" id="Seg_2104" n="e" s="T356">toŋus </ts>
               <ts e="T358" id="Seg_2106" n="e" s="T357">dʼaktardammɨt, </ts>
               <ts e="T359" id="Seg_2108" n="e" s="T358">čaːŋiːt </ts>
               <ts e="T360" id="Seg_2110" n="e" s="T359">tabalarɨnan </ts>
               <ts e="T361" id="Seg_2112" n="e" s="T360">bajan-toton </ts>
               <ts e="T362" id="Seg_2114" n="e" s="T361">olorbuttar. </ts>
               <ts e="T363" id="Seg_2116" n="e" s="T362">Bu </ts>
               <ts e="T364" id="Seg_2118" n="e" s="T363">kördük </ts>
               <ts e="T365" id="Seg_2120" n="e" s="T364">haːmajdarɨ </ts>
               <ts e="T366" id="Seg_2122" n="e" s="T365">gɨtta </ts>
               <ts e="T367" id="Seg_2124" n="e" s="T366">toŋustar </ts>
               <ts e="T368" id="Seg_2126" n="e" s="T367">kolbohon </ts>
               <ts e="T369" id="Seg_2128" n="e" s="T368">olorbuttar. </ts>
               <ts e="T370" id="Seg_2130" n="e" s="T369">Haːmajdar </ts>
               <ts e="T371" id="Seg_2132" n="e" s="T370">ol </ts>
               <ts e="T372" id="Seg_2134" n="e" s="T371">kennitten </ts>
               <ts e="T373" id="Seg_2136" n="e" s="T372">kuttanan </ts>
               <ts e="T374" id="Seg_2138" n="e" s="T373">Taːs </ts>
               <ts e="T375" id="Seg_2140" n="e" s="T374">dʼi͡ek </ts>
               <ts e="T376" id="Seg_2142" n="e" s="T375">köspöt </ts>
               <ts e="T377" id="Seg_2144" n="e" s="T376">bu͡olbuttar. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T9" id="Seg_2145" s="T0">ChPK_1970_Nganasan_flk.001 (001.001)</ta>
            <ta e="T16" id="Seg_2146" s="T9">ChPK_1970_Nganasan_flk.002 (001.002)</ta>
            <ta e="T24" id="Seg_2147" s="T16">ChPK_1970_Nganasan_flk.003 (001.003)</ta>
            <ta e="T28" id="Seg_2148" s="T24">ChPK_1970_Nganasan_flk.004 (001.004)</ta>
            <ta e="T35" id="Seg_2149" s="T28">ChPK_1970_Nganasan_flk.005 (001.005)</ta>
            <ta e="T49" id="Seg_2150" s="T35">ChPK_1970_Nganasan_flk.006 (001.006)</ta>
            <ta e="T62" id="Seg_2151" s="T49">ChPK_1970_Nganasan_flk.007 (001.007)</ta>
            <ta e="T69" id="Seg_2152" s="T62">ChPK_1970_Nganasan_flk.008 (001.008)</ta>
            <ta e="T75" id="Seg_2153" s="T69">ChPK_1970_Nganasan_flk.009 (001.010)</ta>
            <ta e="T79" id="Seg_2154" s="T75">ChPK_1970_Nganasan_flk.010 (001.011)</ta>
            <ta e="T85" id="Seg_2155" s="T79">ChPK_1970_Nganasan_flk.011 (001.012)</ta>
            <ta e="T89" id="Seg_2156" s="T85">ChPK_1970_Nganasan_flk.012 (001.013)</ta>
            <ta e="T104" id="Seg_2157" s="T89">ChPK_1970_Nganasan_flk.013 (001.014)</ta>
            <ta e="T108" id="Seg_2158" s="T104">ChPK_1970_Nganasan_flk.014 (001.015)</ta>
            <ta e="T110" id="Seg_2159" s="T108">ChPK_1970_Nganasan_flk.015 (001.016)</ta>
            <ta e="T115" id="Seg_2160" s="T110">ChPK_1970_Nganasan_flk.016 (001.017)</ta>
            <ta e="T124" id="Seg_2161" s="T115">ChPK_1970_Nganasan_flk.017 (001.018)</ta>
            <ta e="T131" id="Seg_2162" s="T124">ChPK_1970_Nganasan_flk.018 (001.019)</ta>
            <ta e="T134" id="Seg_2163" s="T131">ChPK_1970_Nganasan_flk.019 (001.020)</ta>
            <ta e="T146" id="Seg_2164" s="T134">ChPK_1970_Nganasan_flk.020 (001.021)</ta>
            <ta e="T152" id="Seg_2165" s="T146">ChPK_1970_Nganasan_flk.021 (001.022)</ta>
            <ta e="T158" id="Seg_2166" s="T152">ChPK_1970_Nganasan_flk.022 (001.024)</ta>
            <ta e="T164" id="Seg_2167" s="T158">ChPK_1970_Nganasan_flk.023 (001.025)</ta>
            <ta e="T171" id="Seg_2168" s="T164">ChPK_1970_Nganasan_flk.024 (001.026)</ta>
            <ta e="T179" id="Seg_2169" s="T171">ChPK_1970_Nganasan_flk.025 (001.027)</ta>
            <ta e="T183" id="Seg_2170" s="T179">ChPK_1970_Nganasan_flk.026 (001.028)</ta>
            <ta e="T186" id="Seg_2171" s="T183">ChPK_1970_Nganasan_flk.027 (001.029)</ta>
            <ta e="T188" id="Seg_2172" s="T186">ChPK_1970_Nganasan_flk.028 (001.030)</ta>
            <ta e="T192" id="Seg_2173" s="T188">ChPK_1970_Nganasan_flk.029 (001.031)</ta>
            <ta e="T194" id="Seg_2174" s="T192">ChPK_1970_Nganasan_flk.030 (001.032)</ta>
            <ta e="T197" id="Seg_2175" s="T194">ChPK_1970_Nganasan_flk.031 (001.033)</ta>
            <ta e="T204" id="Seg_2176" s="T197">ChPK_1970_Nganasan_flk.032 (001.034)</ta>
            <ta e="T207" id="Seg_2177" s="T204">ChPK_1970_Nganasan_flk.033 (001.035)</ta>
            <ta e="T218" id="Seg_2178" s="T207">ChPK_1970_Nganasan_flk.034 (001.036)</ta>
            <ta e="T224" id="Seg_2179" s="T218">ChPK_1970_Nganasan_flk.035 (001.037)</ta>
            <ta e="T229" id="Seg_2180" s="T224">ChPK_1970_Nganasan_flk.036 (001.038)</ta>
            <ta e="T234" id="Seg_2181" s="T229">ChPK_1970_Nganasan_flk.037 (001.039)</ta>
            <ta e="T239" id="Seg_2182" s="T234">ChPK_1970_Nganasan_flk.038 (001.040)</ta>
            <ta e="T244" id="Seg_2183" s="T239">ChPK_1970_Nganasan_flk.039 (001.041)</ta>
            <ta e="T251" id="Seg_2184" s="T244">ChPK_1970_Nganasan_flk.040 (001.042)</ta>
            <ta e="T254" id="Seg_2185" s="T251">ChPK_1970_Nganasan_flk.041 (001.043)</ta>
            <ta e="T262" id="Seg_2186" s="T254">ChPK_1970_Nganasan_flk.042 (001.043)</ta>
            <ta e="T268" id="Seg_2187" s="T262">ChPK_1970_Nganasan_flk.043 (001.044)</ta>
            <ta e="T276" id="Seg_2188" s="T268">ChPK_1970_Nganasan_flk.044 (001.045)</ta>
            <ta e="T282" id="Seg_2189" s="T276">ChPK_1970_Nganasan_flk.045 (001.046)</ta>
            <ta e="T288" id="Seg_2190" s="T282">ChPK_1970_Nganasan_flk.046 (001.046)</ta>
            <ta e="T293" id="Seg_2191" s="T288">ChPK_1970_Nganasan_flk.047 (001.047)</ta>
            <ta e="T300" id="Seg_2192" s="T293">ChPK_1970_Nganasan_flk.048 (001.048)</ta>
            <ta e="T306" id="Seg_2193" s="T300">ChPK_1970_Nganasan_flk.049 (001.049)</ta>
            <ta e="T315" id="Seg_2194" s="T306">ChPK_1970_Nganasan_flk.050 (001.050)</ta>
            <ta e="T319" id="Seg_2195" s="T315">ChPK_1970_Nganasan_flk.051 (001.051)</ta>
            <ta e="T323" id="Seg_2196" s="T319">ChPK_1970_Nganasan_flk.052 (001.052)</ta>
            <ta e="T327" id="Seg_2197" s="T323">ChPK_1970_Nganasan_flk.053 (001.053)</ta>
            <ta e="T338" id="Seg_2198" s="T327">ChPK_1970_Nganasan_flk.054 (001.054)</ta>
            <ta e="T344" id="Seg_2199" s="T338">ChPK_1970_Nganasan_flk.055 (001.055)</ta>
            <ta e="T354" id="Seg_2200" s="T344">ChPK_1970_Nganasan_flk.056 (001.056)</ta>
            <ta e="T362" id="Seg_2201" s="T354">ChPK_1970_Nganasan_flk.057 (001.057)</ta>
            <ta e="T369" id="Seg_2202" s="T362">ChPK_1970_Nganasan_flk.058 (001.058)</ta>
            <ta e="T377" id="Seg_2203" s="T369">ChPK_1970_Nganasan_flk.059 (001.059)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T9" id="Seg_2204" s="T0">һаамайдар былыр-былыр Кэрикэҕэ олорор эбиттэрэ, Дьэһэйгэ — чааҥииттар, Тааска — тоҥустар.</ta>
            <ta e="T16" id="Seg_2205" s="T9">Арай биирдэ биир һаамай дьактарын чааҥииттар уорбуттар.</ta>
            <ta e="T24" id="Seg_2206" s="T16">Бу дьактар, эрэ кыыллана барбытын кэннэ, иистэнэ олорбут.</ta>
            <ta e="T28" id="Seg_2207" s="T24">Ол олодогуна, ыттар үрбүттэр.</ta>
            <ta e="T35" id="Seg_2208" s="T28">"Иччилэрин ыттар үрүө һуок этилэрэ", — дьии һанаабыт.</ta>
            <ta e="T49" id="Seg_2209" s="T35">Уотун оттоору гынарын гытта, үс киһи киирбит да, дьактары торгого һуулаан баран, илдьэ барбыттар.</ta>
            <ta e="T62" id="Seg_2210" s="T49">Киэһэ һаамай кэлбитэ: ыттара баайылла һыталлар, дьиэтэ кам тоҥмут, ким да һуок, кап-караҥа.</ta>
            <ta e="T69" id="Seg_2211" s="T62">Айыы дьии, дьактарым кайдиэк барбыта буолуой? — һанааргаабыт.</ta>
            <ta e="T75" id="Seg_2212" s="T69">һарсиэрдэ һырдыырын гытта үөрүн эгэлэ барбыт.</ta>
            <ta e="T79" id="Seg_2213" s="T75">Табата барыта баар эбит.</ta>
            <ta e="T85" id="Seg_2214" s="T79">"Мин тоҥуспар көһүөм, тугу гыныамый һоготогун?</ta>
            <ta e="T89" id="Seg_2215" s="T85">Һүбэлэһиэм иттэ", — дьиэн һанаабыт.</ta>
            <ta e="T104" id="Seg_2216" s="T89">Чор һоготок киһи өр комунуой — тоҥуска көһөн кэлбитэ, киһитэ илимнэнэ барбыт эбит, иньэтэ ирэ баар.</ta>
            <ta e="T108" id="Seg_2217" s="T104">Тоҥус һоч-һоготогун олорор эбит.</ta>
            <ta e="T110" id="Seg_2218" s="T108">Киэһэ кэлбит.</ta>
            <ta e="T115" id="Seg_2219" s="T110">һаамай эрэйдээк кэпсээбит дьактара һүппүтүн.</ta>
            <ta e="T124" id="Seg_2220" s="T115">— Чэ, ким гыныай, бэгэһээ миньиэкэ үс киһи баар этилэрэ.</ta>
            <ta e="T131" id="Seg_2221" s="T124">Көрүҥҥэрэ һоччото һуок этэ — арааһа чааҥииттар быһыылаак.</ta>
            <ta e="T134" id="Seg_2222" s="T131">Миньигиттэн туокпун ылыактарай?!</ta>
            <ta e="T146" id="Seg_2223" s="T134">Чугас баар ыаллар эмиэ кэпсииллэр кайтак гинилэр дьактаттары уора һылдьалларын, — тоҥус дьиэбит.</ta>
            <ta e="T152" id="Seg_2224" s="T146">— Эн төһө коһууҥҥунуй? — тоҥус догорун ыйыппыт.</ta>
            <ta e="T158" id="Seg_2225" s="T152">— Коһуун буоламмын һылдьар эньибин, — дьиэбит һаамайа.</ta>
            <ta e="T164" id="Seg_2226" s="T158">— Оччого мин эньигин билиэм: төһө бэркиний.</ta>
            <ta e="T171" id="Seg_2227" s="T164">Мин эньигин үс ыт һыргата кураҥыттан ытыам.</ta>
            <ta e="T179" id="Seg_2228" s="T171">Тоҥус үстэ ыппыт, эҥэрин ирэ кырыытын һиирэ көтүппүт.</ta>
            <ta e="T183" id="Seg_2229" s="T179">— Аны эн ыт, — дьиэбит. </ta>
            <ta e="T186" id="Seg_2230" s="T183">һаамай ыппыт — таппатак.</ta>
            <ta e="T188" id="Seg_2231" s="T186">— Дьэ бэрт.</ta>
            <ta e="T192" id="Seg_2232" s="T188">Коһууҥҥун эбит, — тоҥуһа кайгаабыт.</ta>
            <ta e="T194" id="Seg_2233" s="T192">Һарсыҥҥытын көспүттэр.</ta>
            <ta e="T197" id="Seg_2234" s="T194">Үс тааһы быспыттар.</ta>
            <ta e="T204" id="Seg_2235" s="T197">— Дьэ мин иньэбин һаамайдыы таҥыннаран баран барыам.</ta>
            <ta e="T207" id="Seg_2236" s="T204">Мин гинилэри албынныам.</ta>
            <ta e="T218" id="Seg_2237" s="T207">Дьиэм: бу дьактар — эһиги ылааччы дьактаргыт эдьийэ, бу дьактар мин дьактарым.</ta>
            <ta e="T224" id="Seg_2238" s="T218">Эн буоллагына мин дьактарым балтыта буолуоҥ.</ta>
            <ta e="T229" id="Seg_2239" s="T224">Тоҥус иньэтин кытта баран каалбыттар.</ta>
            <ta e="T234" id="Seg_2240" s="T229">Тийбиттэрэ: һэттэ ураһа дьиэ тураллар.</ta>
            <ta e="T239" id="Seg_2241" s="T234">Ону төгүрүччү киһилэр үҥкүүлүү һылдьаллар.</ta>
            <ta e="T244" id="Seg_2242" s="T239">"Арааһа, курум быһылаак", — дьии һанаабыттар.</ta>
            <ta e="T251" id="Seg_2243" s="T244">Тоҥус биир киһини кууспут да үҥкүүлүү һылдьыбыттар.</ta>
            <ta e="T254" id="Seg_2244" s="T251">Ол һылдьан ыйыппыт: </ta>
            <ta e="T262" id="Seg_2245" s="T254">— Канна баарый улаканнарын дьиэтэ, онно һаамай дьактара баар.</ta>
            <ta e="T268" id="Seg_2246" s="T262">Бу киһи кыыһыра-кыыһыра һаҥарбыт, ыйбыт дьиэтин.</ta>
            <ta e="T276" id="Seg_2247" s="T268">Тоҥус киирбитэ — дьактар карага ытаан комуос һага буолбут.</ta>
            <ta e="T282" id="Seg_2248" s="T276">Тоҥус киирбит да улаканнарын дьиэк каампыт: </ta>
            <ta e="T288" id="Seg_2249" s="T282">— Мин дьактарым балтытын дьактар гынан олорогун.</ta>
            <ta e="T293" id="Seg_2250" s="T288">Мин дьактарым балтытын көрсүөн багарар.</ta>
            <ta e="T300" id="Seg_2251" s="T293">— Чэ бэрт, һарсын барыакпыт көрө, — дьиэбит чааҥиит.</ta>
            <ta e="T306" id="Seg_2252" s="T300">һарсыҥҥытын бу дьактары чааҥиит эгэлэн кэлбит.</ta>
            <ta e="T315" id="Seg_2253" s="T306">Дьиэгэ киирбиттэрин кэннэ һаамай чааҥиит табаларын быатын быһа баттаабыт.</ta>
            <ta e="T319" id="Seg_2254" s="T315">Дьактарын мэҥэстэн баран күрээбит.</ta>
            <ta e="T323" id="Seg_2255" s="T319">Чааҥиит кэнниттэн һатыы баппыт.</ta>
            <ta e="T327" id="Seg_2256" s="T323">һаамай былчыҥын һиирэ ыттарбыт.</ta>
            <ta e="T338" id="Seg_2257" s="T327">Токтуу түһэн баран һаамай утары каампыт, чааҥиит кулгаагын таһынан кайа кээспит.</ta>
            <ta e="T344" id="Seg_2258" s="T338">Чааҥиит кустугун барытын ыппыт да күрээбит.</ta>
            <ta e="T354" id="Seg_2259" s="T344">Тоҥүһү гытта һаамай баппыттар, һитэ баттаан баран мэньиитин кайа оксубуттар.</ta>
            <ta e="T362" id="Seg_2260" s="T354">Ол кэнниттэн тоҥус дьактардаммыт, чааҥиит табаларынан байан-тотон олорбуттар.</ta>
            <ta e="T369" id="Seg_2261" s="T362">Бу көрдүк һаамайдары гытта тоҥустар колбоһон олорбуттар.</ta>
            <ta e="T377" id="Seg_2262" s="T369">һаамайдар ол кэнниттэн куттанан Таас дьиэк көспөт буолбуттар.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T9" id="Seg_2263" s="T0">Haːmajdar bɨlɨr-bɨlɨr Kerikege oloror ebittere, Dʼehejge — čaːŋiːttar, Taːska — toŋustar. </ta>
            <ta e="T16" id="Seg_2264" s="T9">Araj biːrde biːr haːmaj dʼaktarɨn čaːŋiːttar u͡orbuttar. </ta>
            <ta e="T24" id="Seg_2265" s="T16">Bu dʼaktar, ere kɨːllana barbɨtɨn kenne, iːstene olorbut. </ta>
            <ta e="T28" id="Seg_2266" s="T24">Ol olordoguna, ɨttar ürbütter. </ta>
            <ta e="T35" id="Seg_2267" s="T28">"Iččilerin ɨttar ürü͡ö hu͡ok etilere", dʼiː hanaːbɨt. </ta>
            <ta e="T49" id="Seg_2268" s="T35">U͡otun ottoːru gɨnarɨn gɨtta, üs kihi kiːrbit da, dʼaktarɨ torgogo huːlaːn baran, ildʼe barbɨttar. </ta>
            <ta e="T62" id="Seg_2269" s="T49">Ki͡ehe haːmaj kelbite, ɨttara baːjɨlla hɨtallar, dʼi͡ete kam toŋmut, kim da hu͡ok, kap-karaŋa. </ta>
            <ta e="T69" id="Seg_2270" s="T62">"Ajɨː dʼiː, dʼaktarɨm kajdi͡ek barbɨta bu͡olu͡oj", hanaːrgaːbɨt. </ta>
            <ta e="T75" id="Seg_2271" s="T69">Harsi͡erde hɨrdɨːrɨn gɨtta ü͡örün egele barbɨt. </ta>
            <ta e="T79" id="Seg_2272" s="T75">Tabata barɨta baːr ebit. </ta>
            <ta e="T85" id="Seg_2273" s="T79">"Min toŋuspar köhü͡öm, tugu gɨnɨ͡amɨj hogotogun? </ta>
            <ta e="T89" id="Seg_2274" s="T85">Hübelehi͡em itte", dʼi͡en hanaːbɨt. </ta>
            <ta e="T104" id="Seg_2275" s="T89">Čor hogotok kihi ör komunu͡oj — toŋuska köhön kelbite, kihite ilimnene barbɨt ebit, inʼete ire baːr. </ta>
            <ta e="T108" id="Seg_2276" s="T104">Toŋus hoč-hogotogun oloror ebit. </ta>
            <ta e="T110" id="Seg_2277" s="T108">Ki͡ehe kelbit. </ta>
            <ta e="T115" id="Seg_2278" s="T110">Haːmaj erejdeːk kepseːbit dʼaktara hüppütün. </ta>
            <ta e="T124" id="Seg_2279" s="T115">"Če, kim gɨnɨ͡aj, begeheː minʼi͡eke üs kihi baːr etilere. </ta>
            <ta e="T131" id="Seg_2280" s="T124">Körüŋŋere hoččoto hu͡ok ete — araːha čaːŋiːttar bɨhɨːlaːk. </ta>
            <ta e="T134" id="Seg_2281" s="T131">Minʼigitten tu͡okpun ɨlɨ͡aktaraj?" </ta>
            <ta e="T146" id="Seg_2282" s="T134">"Čugas baːr ɨ͡allar emi͡e kepsiːller kajtak giniler dʼaktattarɨ u͡ora hɨldʼallarɨn", toŋus dʼi͡ebit. </ta>
            <ta e="T152" id="Seg_2283" s="T146">"En töhö kohuːŋŋunuj", toŋus dogorun ɨjɨppɨt. </ta>
            <ta e="T158" id="Seg_2284" s="T152">"Kohuːn bu͡olammɨn hɨldʼar enʼibin", dʼi͡ebit haːmaja. </ta>
            <ta e="T164" id="Seg_2285" s="T158">"Oččogo min enʼigin bili͡em, töhö berkinij. </ta>
            <ta e="T171" id="Seg_2286" s="T164">Min enʼigin üs ɨt hɨrgata kuraŋɨttan ɨtɨ͡am." </ta>
            <ta e="T179" id="Seg_2287" s="T171">Toŋus üste ɨppɨt, eŋerin ire kɨrɨːtɨn hiːre kötüppüt. </ta>
            <ta e="T183" id="Seg_2288" s="T179">"Anɨ en ɨt", dʼi͡ebit. </ta>
            <ta e="T186" id="Seg_2289" s="T183">Haːmaj ɨppɨt — tappatak. </ta>
            <ta e="T188" id="Seg_2290" s="T186">"Dʼe bert. </ta>
            <ta e="T192" id="Seg_2291" s="T188">Kohuːŋŋun ebit", toŋuha kajgaːbɨt. </ta>
            <ta e="T194" id="Seg_2292" s="T192">Harsɨŋŋɨtɨn köspütter. </ta>
            <ta e="T197" id="Seg_2293" s="T194">Üs taːhɨ bɨspɨttar. </ta>
            <ta e="T204" id="Seg_2294" s="T197">"Dʼe min inʼebin haːmajdɨː taŋɨnnaran baran barɨ͡am. </ta>
            <ta e="T207" id="Seg_2295" s="T204">Min ginileri albɨnnɨ͡am. </ta>
            <ta e="T218" id="Seg_2296" s="T207">Dʼi͡em 'bu dʼaktar — ehigi ɨlaːččɨ dʼaktargɨt edʼije, bu dʼaktar min dʼaktarɨm.' </ta>
            <ta e="T224" id="Seg_2297" s="T218">En bu͡ollagɨna min dʼaktarɨm baltɨta bu͡olu͡oŋ. </ta>
            <ta e="T229" id="Seg_2298" s="T224">Toŋus inʼetin kɨtta baran kaːlbɨttar. </ta>
            <ta e="T234" id="Seg_2299" s="T229">Tijbittere, hette uraha dʼi͡e turallar. </ta>
            <ta e="T239" id="Seg_2300" s="T234">Onu tögürüččü kihiler üŋküːlüː hɨldʼallar. </ta>
            <ta e="T244" id="Seg_2301" s="T239">"Araːha, kurum bɨhɨlaːk", dʼiː hanaːbɨttar. </ta>
            <ta e="T251" id="Seg_2302" s="T244">Toŋus biːr kihini kuːsput da üŋküːlüː hɨldʼɨbɨttar. </ta>
            <ta e="T254" id="Seg_2303" s="T251">Ol hɨldʼan ɨjɨppɨt: </ta>
            <ta e="T262" id="Seg_2304" s="T254">"Kanna baːrɨj ulakannarɨn dʼi͡ete, onno haːmaj dʼaktara baːr." </ta>
            <ta e="T268" id="Seg_2305" s="T262">Bu kihi kɨːhɨra-kɨːhɨra haŋarbɨt, ɨjbɨt dʼi͡etin. </ta>
            <ta e="T276" id="Seg_2306" s="T268">Toŋus kiːrbite — dʼaktar karaga ɨtaːn komu͡os haga bu͡olbut. </ta>
            <ta e="T282" id="Seg_2307" s="T276">Toŋus kiːrbit da ulakannarɨn dʼi͡ek kaːmpɨt: </ta>
            <ta e="T288" id="Seg_2308" s="T282">"Min dʼaktarɨm baltɨtɨn dʼaktar gɨnan olorogun. </ta>
            <ta e="T293" id="Seg_2309" s="T288">Min dʼaktarɨm baltɨtɨn körsü͡ön bagarar." </ta>
            <ta e="T300" id="Seg_2310" s="T293">"Če bert, harsɨn barɨ͡akpɨt körö", dʼi͡ebit čaːŋiːt. </ta>
            <ta e="T306" id="Seg_2311" s="T300">Harsɨŋŋɨtɨn bu dʼaktarɨ čaːŋiːt egelen kelbit. </ta>
            <ta e="T315" id="Seg_2312" s="T306">Dʼi͡ege kiːrbitterin kenne haːmaj čaːŋiːt tabalarɨn bɨ͡atɨn bɨha battaːbɨt. </ta>
            <ta e="T319" id="Seg_2313" s="T315">Dʼaktarɨn meŋesten baran küreːbit. </ta>
            <ta e="T323" id="Seg_2314" s="T319">Čaːŋiːt kennitten hatɨː bappɨt. </ta>
            <ta e="T327" id="Seg_2315" s="T323">Haːmaj bɨlčɨŋɨn hiːre ɨttarbɨt. </ta>
            <ta e="T338" id="Seg_2316" s="T327">Toktuː tühen baran haːmaj utarɨ kaːmpɨt, čaːŋiːt kulgaːgɨn tahɨnan kaja keːspit. </ta>
            <ta e="T344" id="Seg_2317" s="T338">Čaːŋiːt kustugun barɨtɨn ɨppɨt da küreːbit. </ta>
            <ta e="T354" id="Seg_2318" s="T344">Toŋühü gɨtta haːmaj bappɨttar, hite battaːn baran menʼiːtin kaja oksubuttar. </ta>
            <ta e="T362" id="Seg_2319" s="T354">Ol kennitten toŋus dʼaktardammɨt, čaːŋiːt tabalarɨnan bajan-toton olorbuttar. </ta>
            <ta e="T369" id="Seg_2320" s="T362">Bu kördük haːmajdarɨ gɨtta toŋustar kolbohon olorbuttar. </ta>
            <ta e="T377" id="Seg_2321" s="T369">Haːmajdar ol kennitten kuttanan Taːs dʼi͡ek köspöt bu͡olbuttar. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_2322" s="T0">haːmaj-dar</ta>
            <ta e="T2" id="Seg_2323" s="T1">bɨlɨr-bɨlɨr</ta>
            <ta e="T3" id="Seg_2324" s="T2">Kerike-ge</ta>
            <ta e="T4" id="Seg_2325" s="T3">olor-or</ta>
            <ta e="T5" id="Seg_2326" s="T4">e-bit-tere</ta>
            <ta e="T6" id="Seg_2327" s="T5">Dʼehej-ge</ta>
            <ta e="T7" id="Seg_2328" s="T6">čaːŋiːt-tar</ta>
            <ta e="T8" id="Seg_2329" s="T7">Taːs-ka</ta>
            <ta e="T9" id="Seg_2330" s="T8">toŋus-tar</ta>
            <ta e="T10" id="Seg_2331" s="T9">araj</ta>
            <ta e="T11" id="Seg_2332" s="T10">biːrde</ta>
            <ta e="T12" id="Seg_2333" s="T11">biːr</ta>
            <ta e="T13" id="Seg_2334" s="T12">haːmaj</ta>
            <ta e="T14" id="Seg_2335" s="T13">dʼaktar-ɨ-n</ta>
            <ta e="T15" id="Seg_2336" s="T14">čaːŋiːt-tar</ta>
            <ta e="T16" id="Seg_2337" s="T15">u͡or-but-tar</ta>
            <ta e="T17" id="Seg_2338" s="T16">bu</ta>
            <ta e="T18" id="Seg_2339" s="T17">dʼaktar</ta>
            <ta e="T19" id="Seg_2340" s="T18">er-e</ta>
            <ta e="T20" id="Seg_2341" s="T19">kɨːl-lan-a</ta>
            <ta e="T21" id="Seg_2342" s="T20">bar-bɨt-ɨ-n</ta>
            <ta e="T22" id="Seg_2343" s="T21">kenne</ta>
            <ta e="T23" id="Seg_2344" s="T22">iːsten-e</ta>
            <ta e="T24" id="Seg_2345" s="T23">olor-but</ta>
            <ta e="T25" id="Seg_2346" s="T24">ol</ta>
            <ta e="T26" id="Seg_2347" s="T25">olor-dog-una</ta>
            <ta e="T27" id="Seg_2348" s="T26">ɨt-tar</ta>
            <ta e="T28" id="Seg_2349" s="T27">ür-büt-ter</ta>
            <ta e="T29" id="Seg_2350" s="T28">ičči-leri-n</ta>
            <ta e="T30" id="Seg_2351" s="T29">ɨt-tar</ta>
            <ta e="T31" id="Seg_2352" s="T30">ür-ü͡ö</ta>
            <ta e="T32" id="Seg_2353" s="T31">hu͡ok</ta>
            <ta e="T33" id="Seg_2354" s="T32">e-ti-lere</ta>
            <ta e="T34" id="Seg_2355" s="T33">dʼ-iː</ta>
            <ta e="T35" id="Seg_2356" s="T34">hanaː-bɨt</ta>
            <ta e="T36" id="Seg_2357" s="T35">u͡ot-u-n</ta>
            <ta e="T37" id="Seg_2358" s="T36">ott-oːru</ta>
            <ta e="T38" id="Seg_2359" s="T37">gɨn-ar-ɨ-n</ta>
            <ta e="T39" id="Seg_2360" s="T38">gɨtta</ta>
            <ta e="T40" id="Seg_2361" s="T39">üs</ta>
            <ta e="T41" id="Seg_2362" s="T40">kihi</ta>
            <ta e="T42" id="Seg_2363" s="T41">kiːr-bit</ta>
            <ta e="T43" id="Seg_2364" s="T42">da</ta>
            <ta e="T44" id="Seg_2365" s="T43">dʼaktar-ɨ</ta>
            <ta e="T45" id="Seg_2366" s="T44">torgo-go</ta>
            <ta e="T46" id="Seg_2367" s="T45">huːlaː-n</ta>
            <ta e="T47" id="Seg_2368" s="T46">baran</ta>
            <ta e="T48" id="Seg_2369" s="T47">ildʼ-e</ta>
            <ta e="T49" id="Seg_2370" s="T48">bar-bɨt-tar</ta>
            <ta e="T50" id="Seg_2371" s="T49">ki͡ehe</ta>
            <ta e="T51" id="Seg_2372" s="T50">haːmaj</ta>
            <ta e="T52" id="Seg_2373" s="T51">kel-bit-e</ta>
            <ta e="T53" id="Seg_2374" s="T52">ɨt-tara</ta>
            <ta e="T54" id="Seg_2375" s="T53">baːj-ɨ-ll-a</ta>
            <ta e="T55" id="Seg_2376" s="T54">hɨt-al-lar</ta>
            <ta e="T56" id="Seg_2377" s="T55">dʼi͡e-te</ta>
            <ta e="T57" id="Seg_2378" s="T56">kam</ta>
            <ta e="T58" id="Seg_2379" s="T57">toŋ-mut</ta>
            <ta e="T59" id="Seg_2380" s="T58">kim</ta>
            <ta e="T60" id="Seg_2381" s="T59">da</ta>
            <ta e="T61" id="Seg_2382" s="T60">hu͡ok</ta>
            <ta e="T62" id="Seg_2383" s="T61">kap-karaŋa</ta>
            <ta e="T63" id="Seg_2384" s="T62">ajɨː</ta>
            <ta e="T64" id="Seg_2385" s="T63">dʼiː</ta>
            <ta e="T65" id="Seg_2386" s="T64">dʼaktar-ɨ-m</ta>
            <ta e="T66" id="Seg_2387" s="T65">kajdi͡ek</ta>
            <ta e="T67" id="Seg_2388" s="T66">bar-bɨt-a</ta>
            <ta e="T68" id="Seg_2389" s="T67">bu͡ol-u͡o=j</ta>
            <ta e="T69" id="Seg_2390" s="T68">hanaːrgaː-bɨt</ta>
            <ta e="T70" id="Seg_2391" s="T69">harsi͡erde</ta>
            <ta e="T71" id="Seg_2392" s="T70">hɨrdɨː-r-ɨ-n</ta>
            <ta e="T72" id="Seg_2393" s="T71">gɨtta</ta>
            <ta e="T73" id="Seg_2394" s="T72">ü͡ör-ü-n</ta>
            <ta e="T74" id="Seg_2395" s="T73">egel-e</ta>
            <ta e="T75" id="Seg_2396" s="T74">bar-bɨt</ta>
            <ta e="T76" id="Seg_2397" s="T75">taba-ta</ta>
            <ta e="T77" id="Seg_2398" s="T76">barɨ-ta</ta>
            <ta e="T78" id="Seg_2399" s="T77">baːr</ta>
            <ta e="T79" id="Seg_2400" s="T78">e-bit</ta>
            <ta e="T80" id="Seg_2401" s="T79">min</ta>
            <ta e="T81" id="Seg_2402" s="T80">toŋus-pa-r</ta>
            <ta e="T82" id="Seg_2403" s="T81">köh-ü͡ö-m</ta>
            <ta e="T83" id="Seg_2404" s="T82">tug-u</ta>
            <ta e="T84" id="Seg_2405" s="T83">gɨn-ɨ͡a-m=ɨj</ta>
            <ta e="T85" id="Seg_2406" s="T84">hogotogun</ta>
            <ta e="T86" id="Seg_2407" s="T85">hübel-e-h-i͡e-m</ta>
            <ta e="T87" id="Seg_2408" s="T86">itte</ta>
            <ta e="T88" id="Seg_2409" s="T87">dʼi͡e-n</ta>
            <ta e="T89" id="Seg_2410" s="T88">hanaː-bɨt</ta>
            <ta e="T90" id="Seg_2411" s="T89">čor</ta>
            <ta e="T91" id="Seg_2412" s="T90">hogotok</ta>
            <ta e="T92" id="Seg_2413" s="T91">kihi</ta>
            <ta e="T93" id="Seg_2414" s="T92">ör</ta>
            <ta e="T94" id="Seg_2415" s="T93">komu-n-u͡o=j</ta>
            <ta e="T95" id="Seg_2416" s="T94">toŋus-ka</ta>
            <ta e="T96" id="Seg_2417" s="T95">köh-ön</ta>
            <ta e="T97" id="Seg_2418" s="T96">kel-bit-e</ta>
            <ta e="T98" id="Seg_2419" s="T97">kihi-te</ta>
            <ta e="T99" id="Seg_2420" s="T98">ilim-nen-e</ta>
            <ta e="T100" id="Seg_2421" s="T99">bar-bɨt</ta>
            <ta e="T101" id="Seg_2422" s="T100">e-bit</ta>
            <ta e="T102" id="Seg_2423" s="T101">inʼe-te</ta>
            <ta e="T103" id="Seg_2424" s="T102">ire</ta>
            <ta e="T104" id="Seg_2425" s="T103">baːr</ta>
            <ta e="T105" id="Seg_2426" s="T104">toŋus</ta>
            <ta e="T106" id="Seg_2427" s="T105">hoč-hogotogun</ta>
            <ta e="T107" id="Seg_2428" s="T106">olor-or</ta>
            <ta e="T108" id="Seg_2429" s="T107">e-bit</ta>
            <ta e="T109" id="Seg_2430" s="T108">ki͡ehe</ta>
            <ta e="T110" id="Seg_2431" s="T109">kel-bit</ta>
            <ta e="T111" id="Seg_2432" s="T110">haːmaj</ta>
            <ta e="T112" id="Seg_2433" s="T111">erej-deːk</ta>
            <ta e="T113" id="Seg_2434" s="T112">kepseː-bit</ta>
            <ta e="T114" id="Seg_2435" s="T113">dʼaktar-a</ta>
            <ta e="T115" id="Seg_2436" s="T114">hüp-püt-ü-n</ta>
            <ta e="T116" id="Seg_2437" s="T115">če</ta>
            <ta e="T117" id="Seg_2438" s="T116">kim</ta>
            <ta e="T118" id="Seg_2439" s="T117">gɨn-ɨ͡a=j</ta>
            <ta e="T119" id="Seg_2440" s="T118">begeheː</ta>
            <ta e="T120" id="Seg_2441" s="T119">minʼi͡e-ke</ta>
            <ta e="T121" id="Seg_2442" s="T120">üs</ta>
            <ta e="T122" id="Seg_2443" s="T121">kihi</ta>
            <ta e="T123" id="Seg_2444" s="T122">baːr</ta>
            <ta e="T124" id="Seg_2445" s="T123">e-ti-lere</ta>
            <ta e="T125" id="Seg_2446" s="T124">körüŋ-ŋere</ta>
            <ta e="T126" id="Seg_2447" s="T125">h-oččo-to</ta>
            <ta e="T127" id="Seg_2448" s="T126">hu͡ok</ta>
            <ta e="T128" id="Seg_2449" s="T127">e-t-e</ta>
            <ta e="T129" id="Seg_2450" s="T128">araːha</ta>
            <ta e="T130" id="Seg_2451" s="T129">čaːŋiːt-tar</ta>
            <ta e="T131" id="Seg_2452" s="T130">bɨhɨːlaːk</ta>
            <ta e="T132" id="Seg_2453" s="T131">minʼigi-tten</ta>
            <ta e="T133" id="Seg_2454" s="T132">tu͡ok-pu-n</ta>
            <ta e="T134" id="Seg_2455" s="T133">ɨl-ɨ͡ak-tara=j</ta>
            <ta e="T135" id="Seg_2456" s="T134">čugas</ta>
            <ta e="T136" id="Seg_2457" s="T135">baːr</ta>
            <ta e="T137" id="Seg_2458" s="T136">ɨ͡al-lar</ta>
            <ta e="T138" id="Seg_2459" s="T137">emi͡e</ta>
            <ta e="T139" id="Seg_2460" s="T138">kepsiː-l-ler</ta>
            <ta e="T140" id="Seg_2461" s="T139">kajtak</ta>
            <ta e="T141" id="Seg_2462" s="T140">giniler</ta>
            <ta e="T142" id="Seg_2463" s="T141">dʼaktat-tar-ɨ</ta>
            <ta e="T143" id="Seg_2464" s="T142">u͡or-a</ta>
            <ta e="T144" id="Seg_2465" s="T143">hɨldʼ-al-larɨ-n</ta>
            <ta e="T145" id="Seg_2466" s="T144">toŋus</ta>
            <ta e="T146" id="Seg_2467" s="T145">dʼi͡e-bit</ta>
            <ta e="T147" id="Seg_2468" s="T146">en</ta>
            <ta e="T148" id="Seg_2469" s="T147">töhö</ta>
            <ta e="T149" id="Seg_2470" s="T148">kohuːŋ-ŋun=uj</ta>
            <ta e="T150" id="Seg_2471" s="T149">toŋus</ta>
            <ta e="T151" id="Seg_2472" s="T150">dogor-u-n</ta>
            <ta e="T152" id="Seg_2473" s="T151">ɨjɨp-pɨt</ta>
            <ta e="T153" id="Seg_2474" s="T152">kohuːn</ta>
            <ta e="T154" id="Seg_2475" s="T153">bu͡ol-am-mɨn</ta>
            <ta e="T155" id="Seg_2476" s="T154">hɨldʼ-ar</ta>
            <ta e="T156" id="Seg_2477" s="T155">enʼi-bin</ta>
            <ta e="T157" id="Seg_2478" s="T156">dʼi͡e-bit</ta>
            <ta e="T158" id="Seg_2479" s="T157">haːmaj-a</ta>
            <ta e="T159" id="Seg_2480" s="T158">oččogo</ta>
            <ta e="T160" id="Seg_2481" s="T159">min</ta>
            <ta e="T161" id="Seg_2482" s="T160">enʼigi-n</ta>
            <ta e="T162" id="Seg_2483" s="T161">bil-i͡e-m</ta>
            <ta e="T163" id="Seg_2484" s="T162">töhö</ta>
            <ta e="T164" id="Seg_2485" s="T163">ber-kin=ij</ta>
            <ta e="T165" id="Seg_2486" s="T164">min</ta>
            <ta e="T166" id="Seg_2487" s="T165">enʼigi-n</ta>
            <ta e="T167" id="Seg_2488" s="T166">üs</ta>
            <ta e="T168" id="Seg_2489" s="T167">ɨt</ta>
            <ta e="T169" id="Seg_2490" s="T168">hɨrga-ta</ta>
            <ta e="T170" id="Seg_2491" s="T169">kuraŋ-ɨ-ttan</ta>
            <ta e="T171" id="Seg_2492" s="T170">ɨt-ɨ͡a-m</ta>
            <ta e="T172" id="Seg_2493" s="T171">toŋus</ta>
            <ta e="T173" id="Seg_2494" s="T172">üs-te</ta>
            <ta e="T174" id="Seg_2495" s="T173">ɨp-pɨt</ta>
            <ta e="T175" id="Seg_2496" s="T174">eŋer-i-n</ta>
            <ta e="T176" id="Seg_2497" s="T175">ire</ta>
            <ta e="T177" id="Seg_2498" s="T176">kɨrɨː-tɨ-n</ta>
            <ta e="T178" id="Seg_2499" s="T177">hiːr-e</ta>
            <ta e="T179" id="Seg_2500" s="T178">köt-ü-p-püt</ta>
            <ta e="T180" id="Seg_2501" s="T179">anɨ</ta>
            <ta e="T181" id="Seg_2502" s="T180">en</ta>
            <ta e="T182" id="Seg_2503" s="T181">ɨt</ta>
            <ta e="T183" id="Seg_2504" s="T182">dʼi͡e-bit</ta>
            <ta e="T184" id="Seg_2505" s="T183">haːmaj</ta>
            <ta e="T185" id="Seg_2506" s="T184">ɨp-pɨt</ta>
            <ta e="T186" id="Seg_2507" s="T185">tap-patak</ta>
            <ta e="T187" id="Seg_2508" s="T186">dʼe</ta>
            <ta e="T188" id="Seg_2509" s="T187">bert</ta>
            <ta e="T189" id="Seg_2510" s="T188">kohuːŋ-ŋun</ta>
            <ta e="T190" id="Seg_2511" s="T189">e-bit</ta>
            <ta e="T191" id="Seg_2512" s="T190">toŋuh-a</ta>
            <ta e="T192" id="Seg_2513" s="T191">kajgaː-bɨt</ta>
            <ta e="T193" id="Seg_2514" s="T192">harsɨŋŋɨ-tɨ-n</ta>
            <ta e="T194" id="Seg_2515" s="T193">kös-püt-ter</ta>
            <ta e="T195" id="Seg_2516" s="T194">üs</ta>
            <ta e="T196" id="Seg_2517" s="T195">taːh-ɨ</ta>
            <ta e="T197" id="Seg_2518" s="T196">bɨs-pɨt-tar</ta>
            <ta e="T198" id="Seg_2519" s="T197">dʼe</ta>
            <ta e="T199" id="Seg_2520" s="T198">min</ta>
            <ta e="T200" id="Seg_2521" s="T199">inʼe-bi-n</ta>
            <ta e="T201" id="Seg_2522" s="T200">haːmaj-dɨː</ta>
            <ta e="T202" id="Seg_2523" s="T201">taŋɨn-nar-an</ta>
            <ta e="T203" id="Seg_2524" s="T202">baran</ta>
            <ta e="T204" id="Seg_2525" s="T203">bar-ɨ͡a-m</ta>
            <ta e="T205" id="Seg_2526" s="T204">min</ta>
            <ta e="T206" id="Seg_2527" s="T205">giniler-i</ta>
            <ta e="T207" id="Seg_2528" s="T206">albɨn-n-ɨ͡a-m</ta>
            <ta e="T208" id="Seg_2529" s="T207">dʼ-i͡e-m</ta>
            <ta e="T209" id="Seg_2530" s="T208">bu</ta>
            <ta e="T210" id="Seg_2531" s="T209">dʼaktar</ta>
            <ta e="T211" id="Seg_2532" s="T210">ehigi</ta>
            <ta e="T212" id="Seg_2533" s="T211">ɨl-aːččɨ</ta>
            <ta e="T213" id="Seg_2534" s="T212">dʼaktar-gɨt</ta>
            <ta e="T214" id="Seg_2535" s="T213">edʼij-e</ta>
            <ta e="T215" id="Seg_2536" s="T214">bu</ta>
            <ta e="T216" id="Seg_2537" s="T215">dʼaktar</ta>
            <ta e="T217" id="Seg_2538" s="T216">min</ta>
            <ta e="T218" id="Seg_2539" s="T217">dʼaktar-ɨ-m</ta>
            <ta e="T219" id="Seg_2540" s="T218">en</ta>
            <ta e="T220" id="Seg_2541" s="T219">bu͡ollagɨna</ta>
            <ta e="T221" id="Seg_2542" s="T220">min</ta>
            <ta e="T222" id="Seg_2543" s="T221">dʼaktar-ɨ-m</ta>
            <ta e="T223" id="Seg_2544" s="T222">balt-ɨ-ta</ta>
            <ta e="T224" id="Seg_2545" s="T223">bu͡ol-u͡o-ŋ</ta>
            <ta e="T225" id="Seg_2546" s="T224">toŋus</ta>
            <ta e="T226" id="Seg_2547" s="T225">inʼe-ti-n</ta>
            <ta e="T227" id="Seg_2548" s="T226">kɨtta</ta>
            <ta e="T228" id="Seg_2549" s="T227">bar-an</ta>
            <ta e="T229" id="Seg_2550" s="T228">kaːl-bɨt-tar</ta>
            <ta e="T230" id="Seg_2551" s="T229">tij-bit-tere</ta>
            <ta e="T231" id="Seg_2552" s="T230">hette</ta>
            <ta e="T232" id="Seg_2553" s="T231">uraha</ta>
            <ta e="T233" id="Seg_2554" s="T232">dʼi͡e</ta>
            <ta e="T234" id="Seg_2555" s="T233">tur-al-lar</ta>
            <ta e="T235" id="Seg_2556" s="T234">o-nu</ta>
            <ta e="T236" id="Seg_2557" s="T235">tögürüččü</ta>
            <ta e="T237" id="Seg_2558" s="T236">kihi-ler</ta>
            <ta e="T238" id="Seg_2559" s="T237">üŋküːl-üː</ta>
            <ta e="T239" id="Seg_2560" s="T238">hɨldʼ-al-lar</ta>
            <ta e="T240" id="Seg_2561" s="T239">araːha</ta>
            <ta e="T241" id="Seg_2562" s="T240">kurum</ta>
            <ta e="T242" id="Seg_2563" s="T241">bɨhɨlaːk</ta>
            <ta e="T243" id="Seg_2564" s="T242">dʼ-iː</ta>
            <ta e="T244" id="Seg_2565" s="T243">hanaː-bɨt-tar</ta>
            <ta e="T245" id="Seg_2566" s="T244">toŋus</ta>
            <ta e="T246" id="Seg_2567" s="T245">biːr</ta>
            <ta e="T247" id="Seg_2568" s="T246">kihi-ni</ta>
            <ta e="T248" id="Seg_2569" s="T247">kuːs-put</ta>
            <ta e="T249" id="Seg_2570" s="T248">da</ta>
            <ta e="T250" id="Seg_2571" s="T249">üŋküːl-üː</ta>
            <ta e="T251" id="Seg_2572" s="T250">hɨldʼ-ɨ-bɨt-tar</ta>
            <ta e="T252" id="Seg_2573" s="T251">ol</ta>
            <ta e="T253" id="Seg_2574" s="T252">hɨldʼ-an</ta>
            <ta e="T254" id="Seg_2575" s="T253">ɨjɨp-pɨt</ta>
            <ta e="T255" id="Seg_2576" s="T254">kanna</ta>
            <ta e="T256" id="Seg_2577" s="T255">baːr=ɨj</ta>
            <ta e="T257" id="Seg_2578" s="T256">ulagan-narɨ-n</ta>
            <ta e="T258" id="Seg_2579" s="T257">dʼi͡e-te</ta>
            <ta e="T259" id="Seg_2580" s="T258">onno</ta>
            <ta e="T260" id="Seg_2581" s="T259">haːmaj</ta>
            <ta e="T261" id="Seg_2582" s="T260">dʼaktar-a</ta>
            <ta e="T262" id="Seg_2583" s="T261">baːr</ta>
            <ta e="T263" id="Seg_2584" s="T262">bu</ta>
            <ta e="T264" id="Seg_2585" s="T263">kihi</ta>
            <ta e="T265" id="Seg_2586" s="T264">kɨːhɨr-a-kɨːhɨr-A</ta>
            <ta e="T266" id="Seg_2587" s="T265">haŋar-bɨt</ta>
            <ta e="T267" id="Seg_2588" s="T266">ɨj-bɨt</ta>
            <ta e="T268" id="Seg_2589" s="T267">dʼi͡e-ti-n</ta>
            <ta e="T269" id="Seg_2590" s="T268">toŋus</ta>
            <ta e="T270" id="Seg_2591" s="T269">kiːr-bit-e</ta>
            <ta e="T271" id="Seg_2592" s="T270">dʼaktar</ta>
            <ta e="T272" id="Seg_2593" s="T271">karag-a</ta>
            <ta e="T273" id="Seg_2594" s="T272">ɨtaː-n</ta>
            <ta e="T274" id="Seg_2595" s="T273">komu͡os</ta>
            <ta e="T275" id="Seg_2596" s="T274">haga</ta>
            <ta e="T276" id="Seg_2597" s="T275">bu͡ol-but</ta>
            <ta e="T277" id="Seg_2598" s="T276">toŋus</ta>
            <ta e="T278" id="Seg_2599" s="T277">kiːr-bit</ta>
            <ta e="T279" id="Seg_2600" s="T278">da</ta>
            <ta e="T280" id="Seg_2601" s="T279">ulakan-narɨ-n</ta>
            <ta e="T281" id="Seg_2602" s="T280">dʼi͡ek</ta>
            <ta e="T282" id="Seg_2603" s="T281">kaːm-pɨt</ta>
            <ta e="T283" id="Seg_2604" s="T282">min</ta>
            <ta e="T284" id="Seg_2605" s="T283">dʼaktar-ɨ-m</ta>
            <ta e="T285" id="Seg_2606" s="T284">balt-ɨ-tɨ-n</ta>
            <ta e="T286" id="Seg_2607" s="T285">dʼaktar</ta>
            <ta e="T287" id="Seg_2608" s="T286">gɨn-an</ta>
            <ta e="T288" id="Seg_2609" s="T287">olor-o-gun</ta>
            <ta e="T289" id="Seg_2610" s="T288">min</ta>
            <ta e="T290" id="Seg_2611" s="T289">dʼaktar-ɨ-m</ta>
            <ta e="T291" id="Seg_2612" s="T290">balt-ɨ-tɨ-n</ta>
            <ta e="T292" id="Seg_2613" s="T291">körs-ü͡ö-n</ta>
            <ta e="T293" id="Seg_2614" s="T292">bagar-ar</ta>
            <ta e="T294" id="Seg_2615" s="T293">če</ta>
            <ta e="T295" id="Seg_2616" s="T294">bert</ta>
            <ta e="T296" id="Seg_2617" s="T295">harsɨn</ta>
            <ta e="T297" id="Seg_2618" s="T296">bar-ɨ͡ak-pɨt</ta>
            <ta e="T298" id="Seg_2619" s="T297">kör-ö</ta>
            <ta e="T299" id="Seg_2620" s="T298">dʼi͡e-bit</ta>
            <ta e="T300" id="Seg_2621" s="T299">čaːŋiːt</ta>
            <ta e="T301" id="Seg_2622" s="T300">harsɨŋŋɨ-tɨ-n</ta>
            <ta e="T302" id="Seg_2623" s="T301">bu</ta>
            <ta e="T303" id="Seg_2624" s="T302">dʼaktar-ɨ</ta>
            <ta e="T304" id="Seg_2625" s="T303">čaːŋiːt</ta>
            <ta e="T305" id="Seg_2626" s="T304">egel-en</ta>
            <ta e="T306" id="Seg_2627" s="T305">kel-bit</ta>
            <ta e="T307" id="Seg_2628" s="T306">dʼi͡e-ge</ta>
            <ta e="T308" id="Seg_2629" s="T307">kiːr-bit-teri-n</ta>
            <ta e="T309" id="Seg_2630" s="T308">kenne</ta>
            <ta e="T310" id="Seg_2631" s="T309">haːmaj</ta>
            <ta e="T311" id="Seg_2632" s="T310">čaːŋiːt</ta>
            <ta e="T312" id="Seg_2633" s="T311">taba-lar-ɨ-n</ta>
            <ta e="T313" id="Seg_2634" s="T312">bɨ͡a-tɨ-n</ta>
            <ta e="T314" id="Seg_2635" s="T313">bɨh-a</ta>
            <ta e="T315" id="Seg_2636" s="T314">battaː-bɨt</ta>
            <ta e="T316" id="Seg_2637" s="T315">dʼaktar-ɨ-n</ta>
            <ta e="T317" id="Seg_2638" s="T316">meŋest-en</ta>
            <ta e="T318" id="Seg_2639" s="T317">baran</ta>
            <ta e="T319" id="Seg_2640" s="T318">küreː-bit</ta>
            <ta e="T320" id="Seg_2641" s="T319">čaːŋiːt</ta>
            <ta e="T321" id="Seg_2642" s="T320">kenn-i-tten</ta>
            <ta e="T322" id="Seg_2643" s="T321">hatɨː</ta>
            <ta e="T323" id="Seg_2644" s="T322">bap-pɨt</ta>
            <ta e="T324" id="Seg_2645" s="T323">haːmaj</ta>
            <ta e="T325" id="Seg_2646" s="T324">bɨlčɨŋ-ɨ-n</ta>
            <ta e="T326" id="Seg_2647" s="T325">hiːr-e</ta>
            <ta e="T327" id="Seg_2648" s="T326">ɨt-tar-bɨt</ta>
            <ta e="T328" id="Seg_2649" s="T327">tokt-uː</ta>
            <ta e="T329" id="Seg_2650" s="T328">tüh-en</ta>
            <ta e="T330" id="Seg_2651" s="T329">baran</ta>
            <ta e="T331" id="Seg_2652" s="T330">haːmaj</ta>
            <ta e="T332" id="Seg_2653" s="T331">utarɨ</ta>
            <ta e="T333" id="Seg_2654" s="T332">kaːm-pɨt</ta>
            <ta e="T334" id="Seg_2655" s="T333">čaːŋiːt</ta>
            <ta e="T335" id="Seg_2656" s="T334">kulgaːg-ɨ-n</ta>
            <ta e="T336" id="Seg_2657" s="T335">tah-ɨ-nan</ta>
            <ta e="T337" id="Seg_2658" s="T336">kaj-a</ta>
            <ta e="T338" id="Seg_2659" s="T337">keːs-pit</ta>
            <ta e="T339" id="Seg_2660" s="T338">čaːŋiːt</ta>
            <ta e="T340" id="Seg_2661" s="T339">kustug-u-n</ta>
            <ta e="T341" id="Seg_2662" s="T340">barɨ-tɨ-n</ta>
            <ta e="T342" id="Seg_2663" s="T341">ɨp-pɨt</ta>
            <ta e="T343" id="Seg_2664" s="T342">da</ta>
            <ta e="T344" id="Seg_2665" s="T343">küreː-bit</ta>
            <ta e="T345" id="Seg_2666" s="T344">toŋüh-ü</ta>
            <ta e="T346" id="Seg_2667" s="T345">gɨtta</ta>
            <ta e="T347" id="Seg_2668" s="T346">haːmaj</ta>
            <ta e="T348" id="Seg_2669" s="T347">bap-pɨt-tar</ta>
            <ta e="T349" id="Seg_2670" s="T348">hit-e</ta>
            <ta e="T350" id="Seg_2671" s="T349">bat-taː-n</ta>
            <ta e="T351" id="Seg_2672" s="T350">baran</ta>
            <ta e="T352" id="Seg_2673" s="T351">menʼiː-ti-n</ta>
            <ta e="T353" id="Seg_2674" s="T352">kaj-a</ta>
            <ta e="T354" id="Seg_2675" s="T353">oks-u-but-tar</ta>
            <ta e="T355" id="Seg_2676" s="T354">ol</ta>
            <ta e="T356" id="Seg_2677" s="T355">kenn-i-tten</ta>
            <ta e="T357" id="Seg_2678" s="T356">toŋus</ta>
            <ta e="T358" id="Seg_2679" s="T357">dʼaktar-dam-mɨt</ta>
            <ta e="T359" id="Seg_2680" s="T358">čaːŋiːt</ta>
            <ta e="T360" id="Seg_2681" s="T359">taba-lar-ɨ-nan</ta>
            <ta e="T361" id="Seg_2682" s="T360">baj-an-tot-on</ta>
            <ta e="T362" id="Seg_2683" s="T361">olor-but-tar</ta>
            <ta e="T363" id="Seg_2684" s="T362">bu</ta>
            <ta e="T364" id="Seg_2685" s="T363">kördük</ta>
            <ta e="T365" id="Seg_2686" s="T364">haːmaj-dar-ɨ</ta>
            <ta e="T366" id="Seg_2687" s="T365">gɨtta</ta>
            <ta e="T367" id="Seg_2688" s="T366">toŋus-tar</ta>
            <ta e="T368" id="Seg_2689" s="T367">kolb-o-h-on</ta>
            <ta e="T369" id="Seg_2690" s="T368">olor-but-tar</ta>
            <ta e="T370" id="Seg_2691" s="T369">haːmaj-dar</ta>
            <ta e="T371" id="Seg_2692" s="T370">ol</ta>
            <ta e="T372" id="Seg_2693" s="T371">kenn-i-tten</ta>
            <ta e="T373" id="Seg_2694" s="T372">kuttan-an</ta>
            <ta e="T374" id="Seg_2695" s="T373">Taːs</ta>
            <ta e="T375" id="Seg_2696" s="T374">dʼi͡ek</ta>
            <ta e="T376" id="Seg_2697" s="T375">kös-pöt</ta>
            <ta e="T377" id="Seg_2698" s="T376">bu͡ol-but-tar</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_2699" s="T0">haːmaj-LAr</ta>
            <ta e="T2" id="Seg_2700" s="T1">bɨlɨr-bɨlɨr</ta>
            <ta e="T3" id="Seg_2701" s="T2">Kerike-GA</ta>
            <ta e="T4" id="Seg_2702" s="T3">olor-Ar</ta>
            <ta e="T5" id="Seg_2703" s="T4">e-BIT-LArA</ta>
            <ta e="T6" id="Seg_2704" s="T5">Dʼehej-GA</ta>
            <ta e="T7" id="Seg_2705" s="T6">čaŋɨt-LAr</ta>
            <ta e="T8" id="Seg_2706" s="T7">Taːs-GA</ta>
            <ta e="T9" id="Seg_2707" s="T8">toŋus-LAr</ta>
            <ta e="T10" id="Seg_2708" s="T9">agaj</ta>
            <ta e="T11" id="Seg_2709" s="T10">biːrde</ta>
            <ta e="T12" id="Seg_2710" s="T11">biːr</ta>
            <ta e="T13" id="Seg_2711" s="T12">haːmaj</ta>
            <ta e="T14" id="Seg_2712" s="T13">dʼaktar-tI-n</ta>
            <ta e="T15" id="Seg_2713" s="T14">čaŋɨt-LAr</ta>
            <ta e="T16" id="Seg_2714" s="T15">u͡or-BIT-LAr</ta>
            <ta e="T17" id="Seg_2715" s="T16">bu</ta>
            <ta e="T18" id="Seg_2716" s="T17">dʼaktar</ta>
            <ta e="T19" id="Seg_2717" s="T18">er-tA</ta>
            <ta e="T20" id="Seg_2718" s="T19">kɨːl-LAN-A</ta>
            <ta e="T21" id="Seg_2719" s="T20">bar-BIT-tI-n</ta>
            <ta e="T22" id="Seg_2720" s="T21">genne</ta>
            <ta e="T23" id="Seg_2721" s="T22">iːsten-A</ta>
            <ta e="T24" id="Seg_2722" s="T23">olor-BIT</ta>
            <ta e="T25" id="Seg_2723" s="T24">ol</ta>
            <ta e="T26" id="Seg_2724" s="T25">olor-TAK-InA</ta>
            <ta e="T27" id="Seg_2725" s="T26">ɨt-LAr</ta>
            <ta e="T28" id="Seg_2726" s="T27">ür-BIT-LAr</ta>
            <ta e="T29" id="Seg_2727" s="T28">ičči-LArI-n</ta>
            <ta e="T30" id="Seg_2728" s="T29">ɨt-LAr</ta>
            <ta e="T31" id="Seg_2729" s="T30">ür-IAK</ta>
            <ta e="T32" id="Seg_2730" s="T31">hu͡ok</ta>
            <ta e="T33" id="Seg_2731" s="T32">e-TI-LArA</ta>
            <ta e="T34" id="Seg_2732" s="T33">di͡e-A</ta>
            <ta e="T35" id="Seg_2733" s="T34">hanaː-BIT</ta>
            <ta e="T36" id="Seg_2734" s="T35">u͡ot-tI-n</ta>
            <ta e="T37" id="Seg_2735" s="T36">otut-AːrI</ta>
            <ta e="T38" id="Seg_2736" s="T37">gɨn-Ar-tI-n</ta>
            <ta e="T39" id="Seg_2737" s="T38">kɨtta</ta>
            <ta e="T40" id="Seg_2738" s="T39">üs</ta>
            <ta e="T41" id="Seg_2739" s="T40">kihi</ta>
            <ta e="T42" id="Seg_2740" s="T41">kiːr-BIT</ta>
            <ta e="T43" id="Seg_2741" s="T42">da</ta>
            <ta e="T44" id="Seg_2742" s="T43">dʼaktar-nI</ta>
            <ta e="T45" id="Seg_2743" s="T44">torgo-GA</ta>
            <ta e="T46" id="Seg_2744" s="T45">huːlaː-An</ta>
            <ta e="T47" id="Seg_2745" s="T46">baran</ta>
            <ta e="T48" id="Seg_2746" s="T47">ilt-A</ta>
            <ta e="T49" id="Seg_2747" s="T48">bar-BIT-LAr</ta>
            <ta e="T50" id="Seg_2748" s="T49">ki͡ehe</ta>
            <ta e="T51" id="Seg_2749" s="T50">haːmaj</ta>
            <ta e="T52" id="Seg_2750" s="T51">kel-BIT-tA</ta>
            <ta e="T53" id="Seg_2751" s="T52">ɨt-LArA</ta>
            <ta e="T54" id="Seg_2752" s="T53">baːj-I-LIN-A</ta>
            <ta e="T55" id="Seg_2753" s="T54">hɨt-Ar-LAr</ta>
            <ta e="T56" id="Seg_2754" s="T55">dʼi͡e-tA</ta>
            <ta e="T57" id="Seg_2755" s="T56">kam</ta>
            <ta e="T58" id="Seg_2756" s="T57">toŋ-BIT</ta>
            <ta e="T59" id="Seg_2757" s="T58">kim</ta>
            <ta e="T60" id="Seg_2758" s="T59">da</ta>
            <ta e="T61" id="Seg_2759" s="T60">hu͡ok</ta>
            <ta e="T62" id="Seg_2760" s="T61">[C^1][V^1][C^2]-karaŋa</ta>
            <ta e="T63" id="Seg_2761" s="T62">anʼɨː</ta>
            <ta e="T64" id="Seg_2762" s="T63">diː</ta>
            <ta e="T65" id="Seg_2763" s="T64">dʼaktar-I-m</ta>
            <ta e="T66" id="Seg_2764" s="T65">kajdi͡ek</ta>
            <ta e="T67" id="Seg_2765" s="T66">bar-BIT-tA</ta>
            <ta e="T68" id="Seg_2766" s="T67">bu͡ol-IAK.[tA]=Ij</ta>
            <ta e="T69" id="Seg_2767" s="T68">hanaːrgaː-BIT</ta>
            <ta e="T70" id="Seg_2768" s="T69">harsi͡erda</ta>
            <ta e="T71" id="Seg_2769" s="T70">hɨrdaː-Ar-tI-n</ta>
            <ta e="T72" id="Seg_2770" s="T71">kɨtta</ta>
            <ta e="T73" id="Seg_2771" s="T72">ü͡ör-tI-n</ta>
            <ta e="T74" id="Seg_2772" s="T73">egel-A</ta>
            <ta e="T75" id="Seg_2773" s="T74">bar-BIT</ta>
            <ta e="T76" id="Seg_2774" s="T75">taba-tA</ta>
            <ta e="T77" id="Seg_2775" s="T76">barɨ-tA</ta>
            <ta e="T78" id="Seg_2776" s="T77">baːr</ta>
            <ta e="T79" id="Seg_2777" s="T78">e-BIT</ta>
            <ta e="T80" id="Seg_2778" s="T79">min</ta>
            <ta e="T81" id="Seg_2779" s="T80">toŋus-BA-r</ta>
            <ta e="T82" id="Seg_2780" s="T81">kös-IAK-m</ta>
            <ta e="T83" id="Seg_2781" s="T82">tu͡ok-nI</ta>
            <ta e="T84" id="Seg_2782" s="T83">gɨn-IAK-m=Ij</ta>
            <ta e="T85" id="Seg_2783" s="T84">hogotogun</ta>
            <ta e="T86" id="Seg_2784" s="T85">hübeleː-A-s-IAK-m</ta>
            <ta e="T87" id="Seg_2785" s="T86">itte</ta>
            <ta e="T88" id="Seg_2786" s="T87">di͡e-An</ta>
            <ta e="T89" id="Seg_2787" s="T88">hanaː-BIT</ta>
            <ta e="T90" id="Seg_2788" s="T89">čor</ta>
            <ta e="T91" id="Seg_2789" s="T90">čogotok</ta>
            <ta e="T92" id="Seg_2790" s="T91">kihi</ta>
            <ta e="T93" id="Seg_2791" s="T92">ör</ta>
            <ta e="T94" id="Seg_2792" s="T93">komuj-n-IAK.[tA]=Ij</ta>
            <ta e="T95" id="Seg_2793" s="T94">toŋus-GA</ta>
            <ta e="T96" id="Seg_2794" s="T95">kös-An</ta>
            <ta e="T97" id="Seg_2795" s="T96">kel-BIT-tA</ta>
            <ta e="T98" id="Seg_2796" s="T97">kihi-tA</ta>
            <ta e="T99" id="Seg_2797" s="T98">ilim-LAN-A</ta>
            <ta e="T100" id="Seg_2798" s="T99">bar-BIT</ta>
            <ta e="T101" id="Seg_2799" s="T100">e-BIT</ta>
            <ta e="T102" id="Seg_2800" s="T101">inʼe-tA</ta>
            <ta e="T103" id="Seg_2801" s="T102">ere</ta>
            <ta e="T104" id="Seg_2802" s="T103">baːr</ta>
            <ta e="T105" id="Seg_2803" s="T104">toŋus</ta>
            <ta e="T106" id="Seg_2804" s="T105">[C^1][V^1][C^2]-hogotogun</ta>
            <ta e="T107" id="Seg_2805" s="T106">olor-Ar</ta>
            <ta e="T108" id="Seg_2806" s="T107">e-BIT</ta>
            <ta e="T109" id="Seg_2807" s="T108">ki͡ehe</ta>
            <ta e="T110" id="Seg_2808" s="T109">kel-BIT</ta>
            <ta e="T111" id="Seg_2809" s="T110">haːmaj</ta>
            <ta e="T112" id="Seg_2810" s="T111">erej-LAːK</ta>
            <ta e="T113" id="Seg_2811" s="T112">kepseː-BIT</ta>
            <ta e="T114" id="Seg_2812" s="T113">dʼaktar-tA</ta>
            <ta e="T115" id="Seg_2813" s="T114">hüt-BIT-tI-n</ta>
            <ta e="T116" id="Seg_2814" s="T115">dʼe</ta>
            <ta e="T117" id="Seg_2815" s="T116">kim</ta>
            <ta e="T118" id="Seg_2816" s="T117">gɨn-IAK.[tA]=Ij</ta>
            <ta e="T119" id="Seg_2817" s="T118">begeheː</ta>
            <ta e="T120" id="Seg_2818" s="T119">min-GA</ta>
            <ta e="T121" id="Seg_2819" s="T120">üs</ta>
            <ta e="T122" id="Seg_2820" s="T121">kihi</ta>
            <ta e="T123" id="Seg_2821" s="T122">baːr</ta>
            <ta e="T124" id="Seg_2822" s="T123">e-TI-LArA</ta>
            <ta e="T125" id="Seg_2823" s="T124">körüŋ-LArA</ta>
            <ta e="T126" id="Seg_2824" s="T125">h-oččo-tA</ta>
            <ta e="T127" id="Seg_2825" s="T126">hu͡ok</ta>
            <ta e="T128" id="Seg_2826" s="T127">e-TI-tA</ta>
            <ta e="T129" id="Seg_2827" s="T128">araːha</ta>
            <ta e="T130" id="Seg_2828" s="T129">čaŋɨt-LAr</ta>
            <ta e="T131" id="Seg_2829" s="T130">bɨhɨːlaːk</ta>
            <ta e="T132" id="Seg_2830" s="T131">min-ttAn</ta>
            <ta e="T133" id="Seg_2831" s="T132">tu͡ok-BI-n</ta>
            <ta e="T134" id="Seg_2832" s="T133">ɨl-IAK-LArA=Ij</ta>
            <ta e="T135" id="Seg_2833" s="T134">hugas</ta>
            <ta e="T136" id="Seg_2834" s="T135">baːr</ta>
            <ta e="T137" id="Seg_2835" s="T136">ɨ͡al-LAr</ta>
            <ta e="T138" id="Seg_2836" s="T137">emi͡e</ta>
            <ta e="T139" id="Seg_2837" s="T138">kepseː-Ar-LAr</ta>
            <ta e="T140" id="Seg_2838" s="T139">kajdak</ta>
            <ta e="T141" id="Seg_2839" s="T140">giniler</ta>
            <ta e="T142" id="Seg_2840" s="T141">dʼaktar-LAr-tI</ta>
            <ta e="T143" id="Seg_2841" s="T142">u͡or-A</ta>
            <ta e="T144" id="Seg_2842" s="T143">hɨrɨt-Ar-LArI-n</ta>
            <ta e="T145" id="Seg_2843" s="T144">toŋus</ta>
            <ta e="T146" id="Seg_2844" s="T145">dʼi͡e-BIT</ta>
            <ta e="T147" id="Seg_2845" s="T146">en</ta>
            <ta e="T148" id="Seg_2846" s="T147">töhö</ta>
            <ta e="T149" id="Seg_2847" s="T148">kosuːn-GIn=Ij</ta>
            <ta e="T150" id="Seg_2848" s="T149">toŋus</ta>
            <ta e="T151" id="Seg_2849" s="T150">dogor-tI-n</ta>
            <ta e="T152" id="Seg_2850" s="T151">ɨjɨt-BIT</ta>
            <ta e="T153" id="Seg_2851" s="T152">kosuːn</ta>
            <ta e="T154" id="Seg_2852" s="T153">bu͡ol-An-BIn</ta>
            <ta e="T155" id="Seg_2853" s="T154">hɨrɨt-Ar</ta>
            <ta e="T156" id="Seg_2854" s="T155">eni-BIn</ta>
            <ta e="T157" id="Seg_2855" s="T156">dʼi͡e-BIT</ta>
            <ta e="T158" id="Seg_2856" s="T157">haːmaj-tA</ta>
            <ta e="T159" id="Seg_2857" s="T158">oččogo</ta>
            <ta e="T160" id="Seg_2858" s="T159">min</ta>
            <ta e="T161" id="Seg_2859" s="T160">en-n</ta>
            <ta e="T162" id="Seg_2860" s="T161">bil-IAK-m</ta>
            <ta e="T163" id="Seg_2861" s="T162">töhö</ta>
            <ta e="T164" id="Seg_2862" s="T163">bert-GIn=Ij</ta>
            <ta e="T165" id="Seg_2863" s="T164">min</ta>
            <ta e="T166" id="Seg_2864" s="T165">en-n</ta>
            <ta e="T167" id="Seg_2865" s="T166">üs</ta>
            <ta e="T168" id="Seg_2866" s="T167">ɨt</ta>
            <ta e="T169" id="Seg_2867" s="T168">hɨrga-tA</ta>
            <ta e="T170" id="Seg_2868" s="T169">kuraŋ-tI-ttAn</ta>
            <ta e="T171" id="Seg_2869" s="T170">ɨt-IAK-m</ta>
            <ta e="T172" id="Seg_2870" s="T171">toŋus</ta>
            <ta e="T173" id="Seg_2871" s="T172">üs-TA</ta>
            <ta e="T174" id="Seg_2872" s="T173">ɨt-BIT</ta>
            <ta e="T175" id="Seg_2873" s="T174">eŋer-tI-n</ta>
            <ta e="T176" id="Seg_2874" s="T175">ere</ta>
            <ta e="T177" id="Seg_2875" s="T176">kɨrɨː-tI-n</ta>
            <ta e="T178" id="Seg_2876" s="T177">hiːr-A</ta>
            <ta e="T179" id="Seg_2877" s="T178">köt-I-t-BIT</ta>
            <ta e="T180" id="Seg_2878" s="T179">anɨ</ta>
            <ta e="T181" id="Seg_2879" s="T180">en</ta>
            <ta e="T182" id="Seg_2880" s="T181">ɨt</ta>
            <ta e="T183" id="Seg_2881" s="T182">dʼi͡e-BIT</ta>
            <ta e="T184" id="Seg_2882" s="T183">haːmaj</ta>
            <ta e="T185" id="Seg_2883" s="T184">ɨt-BIT</ta>
            <ta e="T186" id="Seg_2884" s="T185">tap-BAtAK</ta>
            <ta e="T187" id="Seg_2885" s="T186">dʼe</ta>
            <ta e="T188" id="Seg_2886" s="T187">bert</ta>
            <ta e="T189" id="Seg_2887" s="T188">kosuːn-GIn</ta>
            <ta e="T190" id="Seg_2888" s="T189">e-BIT</ta>
            <ta e="T191" id="Seg_2889" s="T190">toŋus-tA</ta>
            <ta e="T192" id="Seg_2890" s="T191">kajgaː-BIT</ta>
            <ta e="T193" id="Seg_2891" s="T192">harsɨŋŋɨ-tI-n</ta>
            <ta e="T194" id="Seg_2892" s="T193">kös-BIT-LAr</ta>
            <ta e="T195" id="Seg_2893" s="T194">üs</ta>
            <ta e="T196" id="Seg_2894" s="T195">taːs-nI</ta>
            <ta e="T197" id="Seg_2895" s="T196">bɨs-BIT-LAr</ta>
            <ta e="T198" id="Seg_2896" s="T197">dʼe</ta>
            <ta e="T199" id="Seg_2897" s="T198">min</ta>
            <ta e="T200" id="Seg_2898" s="T199">inʼe-BI-n</ta>
            <ta e="T201" id="Seg_2899" s="T200">haːmaj-LIː</ta>
            <ta e="T202" id="Seg_2900" s="T201">taŋɨn-TAr-An</ta>
            <ta e="T203" id="Seg_2901" s="T202">baran</ta>
            <ta e="T204" id="Seg_2902" s="T203">bar-IAK-m</ta>
            <ta e="T205" id="Seg_2903" s="T204">min</ta>
            <ta e="T206" id="Seg_2904" s="T205">giniler-nI</ta>
            <ta e="T207" id="Seg_2905" s="T206">albun-LAː-IAK-m</ta>
            <ta e="T208" id="Seg_2906" s="T207">di͡e-IAK-m</ta>
            <ta e="T209" id="Seg_2907" s="T208">bu</ta>
            <ta e="T210" id="Seg_2908" s="T209">dʼaktar</ta>
            <ta e="T211" id="Seg_2909" s="T210">ehigi</ta>
            <ta e="T212" id="Seg_2910" s="T211">ɨl-AːččI</ta>
            <ta e="T213" id="Seg_2911" s="T212">dʼaktar-GIt</ta>
            <ta e="T214" id="Seg_2912" s="T213">edʼij-tA</ta>
            <ta e="T215" id="Seg_2913" s="T214">bu</ta>
            <ta e="T216" id="Seg_2914" s="T215">dʼaktar</ta>
            <ta e="T217" id="Seg_2915" s="T216">min</ta>
            <ta e="T218" id="Seg_2916" s="T217">dʼaktar-I-m</ta>
            <ta e="T219" id="Seg_2917" s="T218">en</ta>
            <ta e="T220" id="Seg_2918" s="T219">bu͡ollagɨna</ta>
            <ta e="T221" id="Seg_2919" s="T220">min</ta>
            <ta e="T222" id="Seg_2920" s="T221">dʼaktar-I-m</ta>
            <ta e="T223" id="Seg_2921" s="T222">balɨs-I-tA</ta>
            <ta e="T224" id="Seg_2922" s="T223">bu͡ol-IAK-ŋ</ta>
            <ta e="T225" id="Seg_2923" s="T224">toŋus</ta>
            <ta e="T226" id="Seg_2924" s="T225">inʼe-tI-n</ta>
            <ta e="T227" id="Seg_2925" s="T226">kɨtta</ta>
            <ta e="T228" id="Seg_2926" s="T227">bar-An</ta>
            <ta e="T229" id="Seg_2927" s="T228">kaːl-BIT-LAr</ta>
            <ta e="T230" id="Seg_2928" s="T229">tij-BIT-LArA</ta>
            <ta e="T231" id="Seg_2929" s="T230">hette</ta>
            <ta e="T232" id="Seg_2930" s="T231">uraha</ta>
            <ta e="T233" id="Seg_2931" s="T232">dʼi͡e</ta>
            <ta e="T234" id="Seg_2932" s="T233">tur-Ar-LAr</ta>
            <ta e="T235" id="Seg_2933" s="T234">ol-nI</ta>
            <ta e="T236" id="Seg_2934" s="T235">tögürüččü</ta>
            <ta e="T237" id="Seg_2935" s="T236">kihi-LAr</ta>
            <ta e="T238" id="Seg_2936" s="T237">üŋküːleː-A</ta>
            <ta e="T239" id="Seg_2937" s="T238">hɨrɨt-Ar-LAr</ta>
            <ta e="T240" id="Seg_2938" s="T239">araːha</ta>
            <ta e="T241" id="Seg_2939" s="T240">kurum</ta>
            <ta e="T242" id="Seg_2940" s="T241">bɨhɨːlaːk</ta>
            <ta e="T243" id="Seg_2941" s="T242">di͡e-A</ta>
            <ta e="T244" id="Seg_2942" s="T243">hanaː-BIT-LAr</ta>
            <ta e="T245" id="Seg_2943" s="T244">toŋus</ta>
            <ta e="T246" id="Seg_2944" s="T245">biːr</ta>
            <ta e="T247" id="Seg_2945" s="T246">kihi-nI</ta>
            <ta e="T248" id="Seg_2946" s="T247">kuːs-BIT</ta>
            <ta e="T249" id="Seg_2947" s="T248">da</ta>
            <ta e="T250" id="Seg_2948" s="T249">üŋküːleː-A</ta>
            <ta e="T251" id="Seg_2949" s="T250">hɨrɨt-I-BIT-LAr</ta>
            <ta e="T252" id="Seg_2950" s="T251">ol</ta>
            <ta e="T253" id="Seg_2951" s="T252">hɨrɨt-An</ta>
            <ta e="T254" id="Seg_2952" s="T253">ɨjɨt-BIT</ta>
            <ta e="T255" id="Seg_2953" s="T254">kanna</ta>
            <ta e="T256" id="Seg_2954" s="T255">baːr=Ij</ta>
            <ta e="T257" id="Seg_2955" s="T256">ulakan-LArI-n</ta>
            <ta e="T258" id="Seg_2956" s="T257">dʼi͡e-tA</ta>
            <ta e="T259" id="Seg_2957" s="T258">onno</ta>
            <ta e="T260" id="Seg_2958" s="T259">haːmaj</ta>
            <ta e="T261" id="Seg_2959" s="T260">dʼaktar-tA</ta>
            <ta e="T262" id="Seg_2960" s="T261">baːr</ta>
            <ta e="T263" id="Seg_2961" s="T262">bu</ta>
            <ta e="T264" id="Seg_2962" s="T263">kihi</ta>
            <ta e="T265" id="Seg_2963" s="T264">kɨːhɨr-A-kɨːhɨr-A</ta>
            <ta e="T266" id="Seg_2964" s="T265">haŋar-BIT</ta>
            <ta e="T267" id="Seg_2965" s="T266">ɨj-BIT</ta>
            <ta e="T268" id="Seg_2966" s="T267">dʼi͡e-tI-n</ta>
            <ta e="T269" id="Seg_2967" s="T268">toŋus</ta>
            <ta e="T270" id="Seg_2968" s="T269">kiːr-BIT-tA</ta>
            <ta e="T271" id="Seg_2969" s="T270">dʼaktar</ta>
            <ta e="T272" id="Seg_2970" s="T271">karak-tA</ta>
            <ta e="T273" id="Seg_2971" s="T272">ɨtaː-An</ta>
            <ta e="T274" id="Seg_2972" s="T273">komu͡os</ta>
            <ta e="T275" id="Seg_2973" s="T274">haga</ta>
            <ta e="T276" id="Seg_2974" s="T275">bu͡ol-BIT</ta>
            <ta e="T277" id="Seg_2975" s="T276">toŋus</ta>
            <ta e="T278" id="Seg_2976" s="T277">kiːr-BIT</ta>
            <ta e="T279" id="Seg_2977" s="T278">da</ta>
            <ta e="T280" id="Seg_2978" s="T279">ulakan-LArI-n</ta>
            <ta e="T281" id="Seg_2979" s="T280">dek</ta>
            <ta e="T282" id="Seg_2980" s="T281">kaːm-BIT</ta>
            <ta e="T283" id="Seg_2981" s="T282">min</ta>
            <ta e="T284" id="Seg_2982" s="T283">dʼaktar-I-m</ta>
            <ta e="T285" id="Seg_2983" s="T284">balɨs-I-tI-n</ta>
            <ta e="T286" id="Seg_2984" s="T285">dʼaktar</ta>
            <ta e="T287" id="Seg_2985" s="T286">gɨn-An</ta>
            <ta e="T288" id="Seg_2986" s="T287">olor-A-GIn</ta>
            <ta e="T289" id="Seg_2987" s="T288">min</ta>
            <ta e="T290" id="Seg_2988" s="T289">dʼaktar-I-m</ta>
            <ta e="T291" id="Seg_2989" s="T290">balɨs-I-tI-n</ta>
            <ta e="T292" id="Seg_2990" s="T291">körüs-IAK.[tI]-n</ta>
            <ta e="T293" id="Seg_2991" s="T292">bagar-Ar</ta>
            <ta e="T294" id="Seg_2992" s="T293">dʼe</ta>
            <ta e="T295" id="Seg_2993" s="T294">bert</ta>
            <ta e="T296" id="Seg_2994" s="T295">harsɨn</ta>
            <ta e="T297" id="Seg_2995" s="T296">bar-IAK-BIt</ta>
            <ta e="T298" id="Seg_2996" s="T297">kör-A</ta>
            <ta e="T299" id="Seg_2997" s="T298">dʼi͡e-BIT</ta>
            <ta e="T300" id="Seg_2998" s="T299">čaŋɨt</ta>
            <ta e="T301" id="Seg_2999" s="T300">harsɨŋŋɨ-tI-n</ta>
            <ta e="T302" id="Seg_3000" s="T301">bu</ta>
            <ta e="T303" id="Seg_3001" s="T302">dʼaktar-nI</ta>
            <ta e="T304" id="Seg_3002" s="T303">čaŋɨt</ta>
            <ta e="T305" id="Seg_3003" s="T304">egel-An</ta>
            <ta e="T306" id="Seg_3004" s="T305">kel-BIT</ta>
            <ta e="T307" id="Seg_3005" s="T306">dʼi͡e-GA</ta>
            <ta e="T308" id="Seg_3006" s="T307">kiːr-BIT-LArI-n</ta>
            <ta e="T309" id="Seg_3007" s="T308">genne</ta>
            <ta e="T310" id="Seg_3008" s="T309">haːmaj</ta>
            <ta e="T311" id="Seg_3009" s="T310">čaŋɨt</ta>
            <ta e="T312" id="Seg_3010" s="T311">taba-LAr-tI-n</ta>
            <ta e="T313" id="Seg_3011" s="T312">bɨ͡a-tI-n</ta>
            <ta e="T314" id="Seg_3012" s="T313">bɨs-A</ta>
            <ta e="T315" id="Seg_3013" s="T314">battaː-BIT</ta>
            <ta e="T316" id="Seg_3014" s="T315">dʼaktar-tI-n</ta>
            <ta e="T317" id="Seg_3015" s="T316">meŋehin-An</ta>
            <ta e="T318" id="Seg_3016" s="T317">baran</ta>
            <ta e="T319" id="Seg_3017" s="T318">küreː-BIT</ta>
            <ta e="T320" id="Seg_3018" s="T319">čaŋɨt</ta>
            <ta e="T321" id="Seg_3019" s="T320">kelin-tI-ttAn</ta>
            <ta e="T322" id="Seg_3020" s="T321">hatɨː</ta>
            <ta e="T323" id="Seg_3021" s="T322">bat-BIT</ta>
            <ta e="T324" id="Seg_3022" s="T323">haːmaj</ta>
            <ta e="T325" id="Seg_3023" s="T324">bɨlčɨŋ-tI-n</ta>
            <ta e="T326" id="Seg_3024" s="T325">hiːr-A</ta>
            <ta e="T327" id="Seg_3025" s="T326">ɨt-TAr-BIT</ta>
            <ta e="T328" id="Seg_3026" s="T327">toktoː-A</ta>
            <ta e="T329" id="Seg_3027" s="T328">tüs-An</ta>
            <ta e="T330" id="Seg_3028" s="T329">baran</ta>
            <ta e="T331" id="Seg_3029" s="T330">haːmaj</ta>
            <ta e="T332" id="Seg_3030" s="T331">utarɨ</ta>
            <ta e="T333" id="Seg_3031" s="T332">kaːm-BIT</ta>
            <ta e="T334" id="Seg_3032" s="T333">čaŋɨt</ta>
            <ta e="T335" id="Seg_3033" s="T334">kulgaːk-tI-n</ta>
            <ta e="T336" id="Seg_3034" s="T335">tas-tI-nAn</ta>
            <ta e="T337" id="Seg_3035" s="T336">kaj-A</ta>
            <ta e="T338" id="Seg_3036" s="T337">keːs-BIT</ta>
            <ta e="T339" id="Seg_3037" s="T338">čaŋɨt</ta>
            <ta e="T340" id="Seg_3038" s="T339">kustuk-tI-n</ta>
            <ta e="T341" id="Seg_3039" s="T340">barɨ-tI-n</ta>
            <ta e="T342" id="Seg_3040" s="T341">ɨt-BIT</ta>
            <ta e="T343" id="Seg_3041" s="T342">da</ta>
            <ta e="T344" id="Seg_3042" s="T343">küreː-BIT</ta>
            <ta e="T345" id="Seg_3043" s="T344">toŋus-nI</ta>
            <ta e="T346" id="Seg_3044" s="T345">kɨtta</ta>
            <ta e="T347" id="Seg_3045" s="T346">haːmaj</ta>
            <ta e="T348" id="Seg_3046" s="T347">bat-BIT-LAr</ta>
            <ta e="T349" id="Seg_3047" s="T348">hit-A</ta>
            <ta e="T350" id="Seg_3048" s="T349">bat-TAː-An</ta>
            <ta e="T351" id="Seg_3049" s="T350">baran</ta>
            <ta e="T352" id="Seg_3050" s="T351">menʼiː-tI-n</ta>
            <ta e="T353" id="Seg_3051" s="T352">kaj-A</ta>
            <ta e="T354" id="Seg_3052" s="T353">ogus-I-BIT-LAr</ta>
            <ta e="T355" id="Seg_3053" s="T354">ol</ta>
            <ta e="T356" id="Seg_3054" s="T355">kelin-tI-ttAn</ta>
            <ta e="T357" id="Seg_3055" s="T356">toŋus</ta>
            <ta e="T358" id="Seg_3056" s="T357">dʼaktar-LAN-BIT</ta>
            <ta e="T359" id="Seg_3057" s="T358">čaŋɨt</ta>
            <ta e="T360" id="Seg_3058" s="T359">taba-LAr-tI-nAn</ta>
            <ta e="T361" id="Seg_3059" s="T360">baːj-An-tot-An</ta>
            <ta e="T362" id="Seg_3060" s="T361">olor-BIT-LAr</ta>
            <ta e="T363" id="Seg_3061" s="T362">bu</ta>
            <ta e="T364" id="Seg_3062" s="T363">kördük</ta>
            <ta e="T365" id="Seg_3063" s="T364">haːmaj-LAr-nI</ta>
            <ta e="T366" id="Seg_3064" s="T365">kɨtta</ta>
            <ta e="T367" id="Seg_3065" s="T366">toŋus-LAr</ta>
            <ta e="T368" id="Seg_3066" s="T367">kolboː-A-s-An</ta>
            <ta e="T369" id="Seg_3067" s="T368">olor-BIT-LAr</ta>
            <ta e="T370" id="Seg_3068" s="T369">haːmaj-LAr</ta>
            <ta e="T371" id="Seg_3069" s="T370">ol</ta>
            <ta e="T372" id="Seg_3070" s="T371">kelin-tI-ttAn</ta>
            <ta e="T373" id="Seg_3071" s="T372">kuttan-An</ta>
            <ta e="T374" id="Seg_3072" s="T373">Taːs</ta>
            <ta e="T375" id="Seg_3073" s="T374">dek</ta>
            <ta e="T376" id="Seg_3074" s="T375">kös-BAT</ta>
            <ta e="T377" id="Seg_3075" s="T376">bu͡ol-BIT-LAr</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_3076" s="T0">Nganasan-PL.[NOM]</ta>
            <ta e="T2" id="Seg_3077" s="T1">long.ago-long.ago</ta>
            <ta e="T3" id="Seg_3078" s="T2">Kerike-DAT/LOC</ta>
            <ta e="T4" id="Seg_3079" s="T3">live-PTCP.PRS</ta>
            <ta e="T5" id="Seg_3080" s="T4">be-PST2-3PL</ta>
            <ta e="T6" id="Seg_3081" s="T5">Yesej-DAT/LOC</ta>
            <ta e="T7" id="Seg_3082" s="T6">Changit-PL.[NOM]</ta>
            <ta e="T8" id="Seg_3083" s="T7">Taas-DAT/LOC</ta>
            <ta e="T9" id="Seg_3084" s="T8">Evenki-PL.[NOM]</ta>
            <ta e="T10" id="Seg_3085" s="T9">only</ta>
            <ta e="T11" id="Seg_3086" s="T10">once</ta>
            <ta e="T12" id="Seg_3087" s="T11">one</ta>
            <ta e="T13" id="Seg_3088" s="T12">Nganasan.[NOM]</ta>
            <ta e="T14" id="Seg_3089" s="T13">woman-3SG-ACC</ta>
            <ta e="T15" id="Seg_3090" s="T14">Changit-PL.[NOM]</ta>
            <ta e="T16" id="Seg_3091" s="T15">steal-PST2-3PL</ta>
            <ta e="T17" id="Seg_3092" s="T16">this</ta>
            <ta e="T18" id="Seg_3093" s="T17">woman.[NOM]</ta>
            <ta e="T19" id="Seg_3094" s="T18">husband-3SG.[NOM]</ta>
            <ta e="T20" id="Seg_3095" s="T19">wild.reindeer-VBZ-CVB.SIM</ta>
            <ta e="T21" id="Seg_3096" s="T20">go-PTCP.PST-3SG-ACC</ta>
            <ta e="T22" id="Seg_3097" s="T21">after</ta>
            <ta e="T23" id="Seg_3098" s="T22">sew-CVB.SIM</ta>
            <ta e="T24" id="Seg_3099" s="T23">sit-PST2.[3SG]</ta>
            <ta e="T25" id="Seg_3100" s="T24">that</ta>
            <ta e="T26" id="Seg_3101" s="T25">sit-TEMP-3SG</ta>
            <ta e="T27" id="Seg_3102" s="T26">dog-PL.[NOM]</ta>
            <ta e="T28" id="Seg_3103" s="T27">bark-PST2-3PL</ta>
            <ta e="T29" id="Seg_3104" s="T28">master-3PL-ACC</ta>
            <ta e="T30" id="Seg_3105" s="T29">dog-PL.[NOM]</ta>
            <ta e="T31" id="Seg_3106" s="T30">bark-PTCP.FUT</ta>
            <ta e="T32" id="Seg_3107" s="T31">NEG</ta>
            <ta e="T33" id="Seg_3108" s="T32">be-PST1-3PL</ta>
            <ta e="T34" id="Seg_3109" s="T33">think-CVB.SIM</ta>
            <ta e="T35" id="Seg_3110" s="T34">think-PST2.[3SG]</ta>
            <ta e="T36" id="Seg_3111" s="T35">fire-3SG-ACC</ta>
            <ta e="T37" id="Seg_3112" s="T36">light-CVB.PURP</ta>
            <ta e="T38" id="Seg_3113" s="T37">want-PTCP.PRS-3SG-ACC</ta>
            <ta e="T39" id="Seg_3114" s="T38">with</ta>
            <ta e="T40" id="Seg_3115" s="T39">three</ta>
            <ta e="T41" id="Seg_3116" s="T40">human.being.[NOM]</ta>
            <ta e="T42" id="Seg_3117" s="T41">go.in-PST2.[3SG]</ta>
            <ta e="T43" id="Seg_3118" s="T42">and</ta>
            <ta e="T44" id="Seg_3119" s="T43">woman-ACC</ta>
            <ta e="T45" id="Seg_3120" s="T44">fabric-DAT/LOC</ta>
            <ta e="T46" id="Seg_3121" s="T45">wrap.up-CVB.SEQ</ta>
            <ta e="T47" id="Seg_3122" s="T46">after</ta>
            <ta e="T48" id="Seg_3123" s="T47">carry-CVB.SIM</ta>
            <ta e="T49" id="Seg_3124" s="T48">go-PST2-3PL</ta>
            <ta e="T50" id="Seg_3125" s="T49">in.the.evening</ta>
            <ta e="T51" id="Seg_3126" s="T50">Nganasan.[NOM]</ta>
            <ta e="T52" id="Seg_3127" s="T51">come-PST2-3SG</ta>
            <ta e="T53" id="Seg_3128" s="T52">dog-3PL.[NOM]</ta>
            <ta e="T54" id="Seg_3129" s="T53">tie-EP-PASS/REFL-CVB.SIM</ta>
            <ta e="T55" id="Seg_3130" s="T54">lie-PRS-3PL</ta>
            <ta e="T56" id="Seg_3131" s="T55">tent-3SG.[NOM]</ta>
            <ta e="T57" id="Seg_3132" s="T56">strongly</ta>
            <ta e="T58" id="Seg_3133" s="T57">freeze-PST2.[3SG]</ta>
            <ta e="T59" id="Seg_3134" s="T58">who.[NOM]</ta>
            <ta e="T60" id="Seg_3135" s="T59">NEG</ta>
            <ta e="T61" id="Seg_3136" s="T60">NEG.EX</ta>
            <ta e="T62" id="Seg_3137" s="T61">EMPH-dark.[NOM]</ta>
            <ta e="T63" id="Seg_3138" s="T62">sin.[NOM]</ta>
            <ta e="T64" id="Seg_3139" s="T63">EMPH</ta>
            <ta e="T65" id="Seg_3140" s="T64">woman-EP-1SG.[NOM]</ta>
            <ta e="T66" id="Seg_3141" s="T65">whereto</ta>
            <ta e="T67" id="Seg_3142" s="T66">go-PST2-3SG</ta>
            <ta e="T68" id="Seg_3143" s="T67">be-FUT.[3SG]=Q</ta>
            <ta e="T69" id="Seg_3144" s="T68">think.about-PST2.[3SG]</ta>
            <ta e="T70" id="Seg_3145" s="T69">in.the.morning</ta>
            <ta e="T71" id="Seg_3146" s="T70">get.light-PTCP.PRS-3SG-ACC</ta>
            <ta e="T72" id="Seg_3147" s="T71">with</ta>
            <ta e="T73" id="Seg_3148" s="T72">herd-3SG-ACC</ta>
            <ta e="T74" id="Seg_3149" s="T73">bring-CVB.SIM</ta>
            <ta e="T75" id="Seg_3150" s="T74">go-PST2.[3SG]</ta>
            <ta e="T76" id="Seg_3151" s="T75">reindeer-3SG.[NOM]</ta>
            <ta e="T77" id="Seg_3152" s="T76">every-3SG.[NOM]</ta>
            <ta e="T78" id="Seg_3153" s="T77">there.is</ta>
            <ta e="T79" id="Seg_3154" s="T78">be-PST2.[3SG]</ta>
            <ta e="T80" id="Seg_3155" s="T79">1SG.[NOM]</ta>
            <ta e="T81" id="Seg_3156" s="T80">Evenki-1SG-DAT/LOC</ta>
            <ta e="T82" id="Seg_3157" s="T81">start-FUT-1SG</ta>
            <ta e="T83" id="Seg_3158" s="T82">what-ACC</ta>
            <ta e="T84" id="Seg_3159" s="T83">make-FUT-1SG=Q</ta>
            <ta e="T85" id="Seg_3160" s="T84">lonely</ta>
            <ta e="T86" id="Seg_3161" s="T85">advise-EP-RECP/COLL-FUT-1SG</ta>
            <ta e="T87" id="Seg_3162" s="T86">EMPH</ta>
            <ta e="T88" id="Seg_3163" s="T87">think-CVB.SEQ</ta>
            <ta e="T89" id="Seg_3164" s="T88">think-PST2.[3SG]</ta>
            <ta e="T90" id="Seg_3165" s="T89">completely</ta>
            <ta e="T91" id="Seg_3166" s="T90">lonely</ta>
            <ta e="T92" id="Seg_3167" s="T91">human.being.[NOM]</ta>
            <ta e="T93" id="Seg_3168" s="T92">long</ta>
            <ta e="T94" id="Seg_3169" s="T93">gather-REFL-FUT.[3SG]=Q</ta>
            <ta e="T95" id="Seg_3170" s="T94">Evenki-DAT/LOC</ta>
            <ta e="T96" id="Seg_3171" s="T95">nomadize-CVB.SEQ</ta>
            <ta e="T97" id="Seg_3172" s="T96">come-PST2-3SG</ta>
            <ta e="T98" id="Seg_3173" s="T97">human.being-3SG.[NOM]</ta>
            <ta e="T99" id="Seg_3174" s="T98">net-VBZ-CVB.SIM</ta>
            <ta e="T100" id="Seg_3175" s="T99">go-PTCP.PST</ta>
            <ta e="T101" id="Seg_3176" s="T100">be-PST2.[3SG]</ta>
            <ta e="T102" id="Seg_3177" s="T101">mother-3SG.[NOM]</ta>
            <ta e="T103" id="Seg_3178" s="T102">just</ta>
            <ta e="T104" id="Seg_3179" s="T103">there.is</ta>
            <ta e="T105" id="Seg_3180" s="T104">Evenki.[NOM]</ta>
            <ta e="T106" id="Seg_3181" s="T105">EMPH-lonely</ta>
            <ta e="T107" id="Seg_3182" s="T106">live-PTCP.PRS</ta>
            <ta e="T108" id="Seg_3183" s="T107">be-PST2.[3SG]</ta>
            <ta e="T109" id="Seg_3184" s="T108">in.the.evening</ta>
            <ta e="T110" id="Seg_3185" s="T109">come-PST2.[3SG]</ta>
            <ta e="T111" id="Seg_3186" s="T110">Nganasan.[NOM]</ta>
            <ta e="T112" id="Seg_3187" s="T111">pain-PROPR</ta>
            <ta e="T113" id="Seg_3188" s="T112">tell-PST2.[3SG]</ta>
            <ta e="T114" id="Seg_3189" s="T113">woman-3SG.[NOM]</ta>
            <ta e="T115" id="Seg_3190" s="T114">get.lost-PTCP.PST-3SG-ACC</ta>
            <ta e="T116" id="Seg_3191" s="T115">well</ta>
            <ta e="T117" id="Seg_3192" s="T116">who.[NOM]</ta>
            <ta e="T118" id="Seg_3193" s="T117">make-FUT.[3SG]=Q</ta>
            <ta e="T119" id="Seg_3194" s="T118">yesterday</ta>
            <ta e="T120" id="Seg_3195" s="T119">1SG-DAT/LOC</ta>
            <ta e="T121" id="Seg_3196" s="T120">three</ta>
            <ta e="T122" id="Seg_3197" s="T121">human.being.[NOM]</ta>
            <ta e="T123" id="Seg_3198" s="T122">there.is</ta>
            <ta e="T124" id="Seg_3199" s="T123">be-PST1-3PL</ta>
            <ta e="T125" id="Seg_3200" s="T124">look-3PL.[NOM]</ta>
            <ta e="T126" id="Seg_3201" s="T125">EMPH-so.much-POSS</ta>
            <ta e="T127" id="Seg_3202" s="T126">NEG</ta>
            <ta e="T128" id="Seg_3203" s="T127">be-PST1-3SG</ta>
            <ta e="T129" id="Seg_3204" s="T128">probably</ta>
            <ta e="T130" id="Seg_3205" s="T129">Changit-PL.[NOM]</ta>
            <ta e="T131" id="Seg_3206" s="T130">apparently</ta>
            <ta e="T132" id="Seg_3207" s="T131">1SG-ABL</ta>
            <ta e="T133" id="Seg_3208" s="T132">what-1SG-ACC</ta>
            <ta e="T134" id="Seg_3209" s="T133">take-FUT-3PL=Q</ta>
            <ta e="T135" id="Seg_3210" s="T134">close</ta>
            <ta e="T136" id="Seg_3211" s="T135">there.is</ta>
            <ta e="T137" id="Seg_3212" s="T136">neighbour-PL.[NOM]</ta>
            <ta e="T138" id="Seg_3213" s="T137">also</ta>
            <ta e="T139" id="Seg_3214" s="T138">tell-PRS-3PL</ta>
            <ta e="T140" id="Seg_3215" s="T139">how</ta>
            <ta e="T141" id="Seg_3216" s="T140">3PL.[NOM]</ta>
            <ta e="T142" id="Seg_3217" s="T141">woman-PL-ACC</ta>
            <ta e="T143" id="Seg_3218" s="T142">steal-CVB.SIM</ta>
            <ta e="T144" id="Seg_3219" s="T143">go-PTCP.PRS-3PL-ACC</ta>
            <ta e="T145" id="Seg_3220" s="T144">Evenki.[NOM]</ta>
            <ta e="T146" id="Seg_3221" s="T145">say-PST2.[3SG]</ta>
            <ta e="T147" id="Seg_3222" s="T146">2SG.[NOM]</ta>
            <ta e="T148" id="Seg_3223" s="T147">how.much</ta>
            <ta e="T149" id="Seg_3224" s="T148">brave-2SG=Q</ta>
            <ta e="T150" id="Seg_3225" s="T149">Evenki.[NOM]</ta>
            <ta e="T151" id="Seg_3226" s="T150">friend-3SG-ACC</ta>
            <ta e="T152" id="Seg_3227" s="T151">ask-PST2.[3SG]</ta>
            <ta e="T153" id="Seg_3228" s="T152">brave.[NOM]</ta>
            <ta e="T154" id="Seg_3229" s="T153">be-CVB.SEQ-1SG</ta>
            <ta e="T155" id="Seg_3230" s="T154">go-PTCP.PRS</ta>
            <ta e="T156" id="Seg_3231" s="T155">apparently-1SG</ta>
            <ta e="T157" id="Seg_3232" s="T156">say-PST2.[3SG]</ta>
            <ta e="T158" id="Seg_3233" s="T157">Nganasan-3SG.[NOM]</ta>
            <ta e="T159" id="Seg_3234" s="T158">then</ta>
            <ta e="T160" id="Seg_3235" s="T159">1SG.[NOM]</ta>
            <ta e="T161" id="Seg_3236" s="T160">2SG-ACC</ta>
            <ta e="T162" id="Seg_3237" s="T161">know-FUT-1SG</ta>
            <ta e="T163" id="Seg_3238" s="T162">how.much</ta>
            <ta e="T164" id="Seg_3239" s="T163">powerful-2SG=Q</ta>
            <ta e="T165" id="Seg_3240" s="T164">1SG.[NOM]</ta>
            <ta e="T166" id="Seg_3241" s="T165">2SG-ACC</ta>
            <ta e="T167" id="Seg_3242" s="T166">three</ta>
            <ta e="T168" id="Seg_3243" s="T167">dog.[NOM]</ta>
            <ta e="T169" id="Seg_3244" s="T168">sledge-3SG.[NOM]</ta>
            <ta e="T170" id="Seg_3245" s="T169">approximately-3SG-ABL</ta>
            <ta e="T171" id="Seg_3246" s="T170">shoot-FUT-1SG</ta>
            <ta e="T172" id="Seg_3247" s="T171">Evenki.[NOM]</ta>
            <ta e="T173" id="Seg_3248" s="T172">three-MLTP</ta>
            <ta e="T174" id="Seg_3249" s="T173">shoot-PST2.[3SG]</ta>
            <ta e="T175" id="Seg_3250" s="T174">seam-3SG-GEN</ta>
            <ta e="T176" id="Seg_3251" s="T175">just</ta>
            <ta e="T177" id="Seg_3252" s="T176">edge-3SG-ACC</ta>
            <ta e="T178" id="Seg_3253" s="T177">rend-CVB.SIM</ta>
            <ta e="T179" id="Seg_3254" s="T178">run-EP-CAUS-PST2.[3SG]</ta>
            <ta e="T180" id="Seg_3255" s="T179">now</ta>
            <ta e="T181" id="Seg_3256" s="T180">2SG.[NOM]</ta>
            <ta e="T182" id="Seg_3257" s="T181">shoot.[IMP.2SG]</ta>
            <ta e="T183" id="Seg_3258" s="T182">say-PST2.[3SG]</ta>
            <ta e="T184" id="Seg_3259" s="T183">Nganasan.[NOM]</ta>
            <ta e="T185" id="Seg_3260" s="T184">shoot-PST2.[3SG]</ta>
            <ta e="T186" id="Seg_3261" s="T185">strike-PST2.NEG.[3SG]</ta>
            <ta e="T187" id="Seg_3262" s="T186">well</ta>
            <ta e="T188" id="Seg_3263" s="T187">powerful.[NOM]</ta>
            <ta e="T189" id="Seg_3264" s="T188">brave-2SG</ta>
            <ta e="T190" id="Seg_3265" s="T189">be-PST2.[3SG]</ta>
            <ta e="T191" id="Seg_3266" s="T190">Evenki-3SG.[NOM]</ta>
            <ta e="T192" id="Seg_3267" s="T191">praise-PST2.[3SG]</ta>
            <ta e="T193" id="Seg_3268" s="T192">next.morning-3SG-ACC</ta>
            <ta e="T194" id="Seg_3269" s="T193">start-PST2-3PL</ta>
            <ta e="T195" id="Seg_3270" s="T194">three</ta>
            <ta e="T196" id="Seg_3271" s="T195">crest-ACC</ta>
            <ta e="T197" id="Seg_3272" s="T196">cross-PST2-3PL</ta>
            <ta e="T198" id="Seg_3273" s="T197">well</ta>
            <ta e="T199" id="Seg_3274" s="T198">1SG.[NOM]</ta>
            <ta e="T200" id="Seg_3275" s="T199">mother-1SG-ACC</ta>
            <ta e="T201" id="Seg_3276" s="T200">Nganasan-SIM</ta>
            <ta e="T202" id="Seg_3277" s="T201">dress-CAUS-CVB.SEQ</ta>
            <ta e="T203" id="Seg_3278" s="T202">after</ta>
            <ta e="T204" id="Seg_3279" s="T203">go-FUT-1SG</ta>
            <ta e="T205" id="Seg_3280" s="T204">1SG.[NOM]</ta>
            <ta e="T206" id="Seg_3281" s="T205">3PL-ACC</ta>
            <ta e="T207" id="Seg_3282" s="T206">deception-VBZ-FUT-1SG</ta>
            <ta e="T208" id="Seg_3283" s="T207">say-FUT-1SG</ta>
            <ta e="T209" id="Seg_3284" s="T208">this</ta>
            <ta e="T210" id="Seg_3285" s="T209">woman.[NOM]</ta>
            <ta e="T211" id="Seg_3286" s="T210">2PL.[NOM]</ta>
            <ta e="T212" id="Seg_3287" s="T211">take-PTCP.HAB</ta>
            <ta e="T213" id="Seg_3288" s="T212">woman-2PL.[NOM]</ta>
            <ta e="T214" id="Seg_3289" s="T213">older.sister-3SG.[NOM]</ta>
            <ta e="T215" id="Seg_3290" s="T214">this</ta>
            <ta e="T216" id="Seg_3291" s="T215">woman.[NOM]</ta>
            <ta e="T217" id="Seg_3292" s="T216">1SG.[NOM]</ta>
            <ta e="T218" id="Seg_3293" s="T217">woman-EP-1SG.[NOM]</ta>
            <ta e="T219" id="Seg_3294" s="T218">2SG.[NOM]</ta>
            <ta e="T220" id="Seg_3295" s="T219">though</ta>
            <ta e="T221" id="Seg_3296" s="T220">1SG.[NOM]</ta>
            <ta e="T222" id="Seg_3297" s="T221">woman-EP-1SG.[NOM]</ta>
            <ta e="T223" id="Seg_3298" s="T222">younger.brother-EP-3SG.[NOM]</ta>
            <ta e="T224" id="Seg_3299" s="T223">be-FUT-2SG</ta>
            <ta e="T225" id="Seg_3300" s="T224">Evenki.[NOM]</ta>
            <ta e="T226" id="Seg_3301" s="T225">mother-3SG-ACC</ta>
            <ta e="T227" id="Seg_3302" s="T226">with</ta>
            <ta e="T228" id="Seg_3303" s="T227">go-CVB.SEQ</ta>
            <ta e="T229" id="Seg_3304" s="T228">stay-PST2-3PL</ta>
            <ta e="T230" id="Seg_3305" s="T229">reach-PST2-3PL</ta>
            <ta e="T231" id="Seg_3306" s="T230">seven</ta>
            <ta e="T232" id="Seg_3307" s="T231">pole.[NOM]</ta>
            <ta e="T233" id="Seg_3308" s="T232">tent.[NOM]</ta>
            <ta e="T234" id="Seg_3309" s="T233">stand-PRS-3PL</ta>
            <ta e="T235" id="Seg_3310" s="T234">that-ACC</ta>
            <ta e="T236" id="Seg_3311" s="T235">around</ta>
            <ta e="T237" id="Seg_3312" s="T236">human.being-PL.[NOM]</ta>
            <ta e="T238" id="Seg_3313" s="T237">dance-CVB.SIM</ta>
            <ta e="T239" id="Seg_3314" s="T238">go-PRS-3PL</ta>
            <ta e="T240" id="Seg_3315" s="T239">probably</ta>
            <ta e="T241" id="Seg_3316" s="T240">wedding.[NOM]</ta>
            <ta e="T242" id="Seg_3317" s="T241">apparently</ta>
            <ta e="T243" id="Seg_3318" s="T242">think-CVB.SIM</ta>
            <ta e="T244" id="Seg_3319" s="T243">think-PST2-3PL</ta>
            <ta e="T245" id="Seg_3320" s="T244">Evenki.[NOM]</ta>
            <ta e="T246" id="Seg_3321" s="T245">one</ta>
            <ta e="T247" id="Seg_3322" s="T246">human.being-ACC</ta>
            <ta e="T248" id="Seg_3323" s="T247">embrace-PST2.[3SG]</ta>
            <ta e="T249" id="Seg_3324" s="T248">and</ta>
            <ta e="T250" id="Seg_3325" s="T249">dance-CVB.SIM</ta>
            <ta e="T251" id="Seg_3326" s="T250">go-EP-PST2-3PL</ta>
            <ta e="T252" id="Seg_3327" s="T251">that</ta>
            <ta e="T253" id="Seg_3328" s="T252">go-CVB.SEQ</ta>
            <ta e="T254" id="Seg_3329" s="T253">ask-PST2.[3SG]</ta>
            <ta e="T255" id="Seg_3330" s="T254">where</ta>
            <ta e="T256" id="Seg_3331" s="T255">there.is=Q</ta>
            <ta e="T257" id="Seg_3332" s="T256">big-3PL-GEN</ta>
            <ta e="T258" id="Seg_3333" s="T257">tent-3SG.[NOM]</ta>
            <ta e="T259" id="Seg_3334" s="T258">there</ta>
            <ta e="T260" id="Seg_3335" s="T259">Nganasan.[NOM]</ta>
            <ta e="T261" id="Seg_3336" s="T260">woman-3SG.[NOM]</ta>
            <ta e="T262" id="Seg_3337" s="T261">there.is</ta>
            <ta e="T263" id="Seg_3338" s="T262">this</ta>
            <ta e="T264" id="Seg_3339" s="T263">human.being.[NOM]</ta>
            <ta e="T265" id="Seg_3340" s="T264">be.angry-CVB.SIM-be.angry-CVB.SIM</ta>
            <ta e="T266" id="Seg_3341" s="T265">speak-PST2.[3SG]</ta>
            <ta e="T267" id="Seg_3342" s="T266">show-PST2.[3SG]</ta>
            <ta e="T268" id="Seg_3343" s="T267">tent-3SG-ACC</ta>
            <ta e="T269" id="Seg_3344" s="T268">Evenki.[NOM]</ta>
            <ta e="T270" id="Seg_3345" s="T269">go.in-PST2-3SG</ta>
            <ta e="T271" id="Seg_3346" s="T270">woman.[NOM]</ta>
            <ta e="T272" id="Seg_3347" s="T271">eye-3SG.[NOM]</ta>
            <ta e="T273" id="Seg_3348" s="T272">cry-CVB.SEQ</ta>
            <ta e="T274" id="Seg_3349" s="T273">scoop.[NOM]</ta>
            <ta e="T275" id="Seg_3350" s="T274">big.as</ta>
            <ta e="T276" id="Seg_3351" s="T275">become-PST2.[3SG]</ta>
            <ta e="T277" id="Seg_3352" s="T276">Evenki.[NOM]</ta>
            <ta e="T278" id="Seg_3353" s="T277">go.in-PST2.[3SG]</ta>
            <ta e="T279" id="Seg_3354" s="T278">and</ta>
            <ta e="T280" id="Seg_3355" s="T279">big-3PL-ACC</ta>
            <ta e="T281" id="Seg_3356" s="T280">to</ta>
            <ta e="T282" id="Seg_3357" s="T281">stride-PST2.[3SG]</ta>
            <ta e="T283" id="Seg_3358" s="T282">1SG.[NOM]</ta>
            <ta e="T284" id="Seg_3359" s="T283">woman-EP-1SG.[NOM]</ta>
            <ta e="T285" id="Seg_3360" s="T284">younger.sister-EP-3SG-ACC</ta>
            <ta e="T286" id="Seg_3361" s="T285">woman.[NOM]</ta>
            <ta e="T287" id="Seg_3362" s="T286">make-CVB.SEQ</ta>
            <ta e="T288" id="Seg_3363" s="T287">sit-PRS-2SG</ta>
            <ta e="T289" id="Seg_3364" s="T288">1SG.[NOM]</ta>
            <ta e="T290" id="Seg_3365" s="T289">woman-EP-1SG.[NOM]</ta>
            <ta e="T291" id="Seg_3366" s="T290">younger.sister-EP-3SG-ACC</ta>
            <ta e="T292" id="Seg_3367" s="T291">meet-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T293" id="Seg_3368" s="T292">want-PRS.[3SG]</ta>
            <ta e="T294" id="Seg_3369" s="T293">well</ta>
            <ta e="T295" id="Seg_3370" s="T294">well</ta>
            <ta e="T296" id="Seg_3371" s="T295">tomorrow</ta>
            <ta e="T297" id="Seg_3372" s="T296">go-FUT-1PL</ta>
            <ta e="T298" id="Seg_3373" s="T297">see-CVB.SIM</ta>
            <ta e="T299" id="Seg_3374" s="T298">say-PST2.[3SG]</ta>
            <ta e="T300" id="Seg_3375" s="T299">Changit.[NOM]</ta>
            <ta e="T301" id="Seg_3376" s="T300">next.morning-3SG-ACC</ta>
            <ta e="T302" id="Seg_3377" s="T301">this</ta>
            <ta e="T303" id="Seg_3378" s="T302">woman-ACC</ta>
            <ta e="T304" id="Seg_3379" s="T303">Changit.[NOM]</ta>
            <ta e="T305" id="Seg_3380" s="T304">get-CVB.SEQ</ta>
            <ta e="T306" id="Seg_3381" s="T305">come-PST2.[3SG]</ta>
            <ta e="T307" id="Seg_3382" s="T306">tent-DAT/LOC</ta>
            <ta e="T308" id="Seg_3383" s="T307">go.in-PTCP.PST-3PL-ACC</ta>
            <ta e="T309" id="Seg_3384" s="T308">after</ta>
            <ta e="T310" id="Seg_3385" s="T309">Nganasan.[NOM]</ta>
            <ta e="T311" id="Seg_3386" s="T310">Changit.[NOM]</ta>
            <ta e="T312" id="Seg_3387" s="T311">reindeer-PL-3SG-GEN</ta>
            <ta e="T313" id="Seg_3388" s="T312">string-3SG-ACC</ta>
            <ta e="T314" id="Seg_3389" s="T313">cut-CVB.SIM</ta>
            <ta e="T315" id="Seg_3390" s="T314">pull.down-PST2.[3SG]</ta>
            <ta e="T316" id="Seg_3391" s="T315">woman-3SG-ACC</ta>
            <ta e="T317" id="Seg_3392" s="T316">ride-CVB.SEQ</ta>
            <ta e="T318" id="Seg_3393" s="T317">after</ta>
            <ta e="T319" id="Seg_3394" s="T318">escape-PST2.[3SG]</ta>
            <ta e="T320" id="Seg_3395" s="T319">Changit.[NOM]</ta>
            <ta e="T321" id="Seg_3396" s="T320">back-3SG-ABL</ta>
            <ta e="T322" id="Seg_3397" s="T321">by.foot</ta>
            <ta e="T323" id="Seg_3398" s="T322">follow-PST2.[3SG]</ta>
            <ta e="T324" id="Seg_3399" s="T323">Nganasan.[NOM]</ta>
            <ta e="T325" id="Seg_3400" s="T324">muscle-3SG-ACC</ta>
            <ta e="T326" id="Seg_3401" s="T325">rend-CVB.SIM</ta>
            <ta e="T327" id="Seg_3402" s="T326">shoot-CAUS-PST2.[3SG]</ta>
            <ta e="T328" id="Seg_3403" s="T327">stop-CVB.SIM</ta>
            <ta e="T329" id="Seg_3404" s="T328">fall-CVB.SEQ</ta>
            <ta e="T330" id="Seg_3405" s="T329">after</ta>
            <ta e="T331" id="Seg_3406" s="T330">Nganasan.[NOM]</ta>
            <ta e="T332" id="Seg_3407" s="T331">towards</ta>
            <ta e="T333" id="Seg_3408" s="T332">walk-PST2.[3SG]</ta>
            <ta e="T334" id="Seg_3409" s="T333">Changit.[NOM]</ta>
            <ta e="T335" id="Seg_3410" s="T334">ear-3SG-GEN</ta>
            <ta e="T336" id="Seg_3411" s="T335">edge-3SG-INSTR</ta>
            <ta e="T337" id="Seg_3412" s="T336">split-CVB.SIM</ta>
            <ta e="T338" id="Seg_3413" s="T337">throw-PST2.[3SG]</ta>
            <ta e="T339" id="Seg_3414" s="T338">Changit.[NOM]</ta>
            <ta e="T340" id="Seg_3415" s="T339">arrow-3SG-ACC</ta>
            <ta e="T341" id="Seg_3416" s="T340">every-3SG-ACC</ta>
            <ta e="T342" id="Seg_3417" s="T341">shoot-PST2.[3SG]</ta>
            <ta e="T343" id="Seg_3418" s="T342">and</ta>
            <ta e="T344" id="Seg_3419" s="T343">escape-PST2.[3SG]</ta>
            <ta e="T345" id="Seg_3420" s="T344">Evenki-ACC</ta>
            <ta e="T346" id="Seg_3421" s="T345">with</ta>
            <ta e="T347" id="Seg_3422" s="T346">Nganasan.[NOM]</ta>
            <ta e="T348" id="Seg_3423" s="T347">follow-PST2-3PL</ta>
            <ta e="T349" id="Seg_3424" s="T348">chase-CVB.SIM</ta>
            <ta e="T350" id="Seg_3425" s="T349">follow-ITER-CVB.SEQ</ta>
            <ta e="T351" id="Seg_3426" s="T350">after</ta>
            <ta e="T352" id="Seg_3427" s="T351">head-3SG-ACC</ta>
            <ta e="T353" id="Seg_3428" s="T352">split-CVB.SIM</ta>
            <ta e="T354" id="Seg_3429" s="T353">beat-EP-PST2-3PL</ta>
            <ta e="T355" id="Seg_3430" s="T354">that.[NOM]</ta>
            <ta e="T356" id="Seg_3431" s="T355">back-3SG-ABL</ta>
            <ta e="T357" id="Seg_3432" s="T356">Evenki.[NOM]</ta>
            <ta e="T358" id="Seg_3433" s="T357">woman-VBZ-PST2.[3SG]</ta>
            <ta e="T359" id="Seg_3434" s="T358">Changit.[NOM]</ta>
            <ta e="T360" id="Seg_3435" s="T359">reindeer-PL-3SG-INSTR</ta>
            <ta e="T361" id="Seg_3436" s="T360">be.rich-CVB.SEQ-eat.ones.fill-CVB.SEQ</ta>
            <ta e="T362" id="Seg_3437" s="T361">live-PST2-3PL</ta>
            <ta e="T363" id="Seg_3438" s="T362">this.[NOM]</ta>
            <ta e="T364" id="Seg_3439" s="T363">similar</ta>
            <ta e="T365" id="Seg_3440" s="T364">Nganasan-PL-ACC</ta>
            <ta e="T366" id="Seg_3441" s="T365">with</ta>
            <ta e="T367" id="Seg_3442" s="T366">Evenki-PL.[NOM]</ta>
            <ta e="T368" id="Seg_3443" s="T367">connect-EP-RECP/COLL-CVB.SEQ</ta>
            <ta e="T369" id="Seg_3444" s="T368">live-PST2-3PL</ta>
            <ta e="T370" id="Seg_3445" s="T369">Nganasan-PL.[NOM]</ta>
            <ta e="T371" id="Seg_3446" s="T370">that.[NOM]</ta>
            <ta e="T372" id="Seg_3447" s="T371">back-3SG-ABL</ta>
            <ta e="T373" id="Seg_3448" s="T372">be.afraid-CVB.SEQ</ta>
            <ta e="T374" id="Seg_3449" s="T373">Taas.[NOM]</ta>
            <ta e="T375" id="Seg_3450" s="T374">to</ta>
            <ta e="T376" id="Seg_3451" s="T375">nomadize-NEG.PTCP</ta>
            <ta e="T377" id="Seg_3452" s="T376">become-PST-3PL</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_3453" s="T0">Nganasane-PL.[NOM]</ta>
            <ta e="T2" id="Seg_3454" s="T1">vor.langer.Zeit-vor.langer.Zeit</ta>
            <ta e="T3" id="Seg_3455" s="T2">Kerike-DAT/LOC</ta>
            <ta e="T4" id="Seg_3456" s="T3">leben-PTCP.PRS</ta>
            <ta e="T5" id="Seg_3457" s="T4">sein-PST2-3PL</ta>
            <ta e="T6" id="Seg_3458" s="T5">Jesej-DAT/LOC</ta>
            <ta e="T7" id="Seg_3459" s="T6">Tschangit-PL.[NOM]</ta>
            <ta e="T8" id="Seg_3460" s="T7">Taas-DAT/LOC</ta>
            <ta e="T9" id="Seg_3461" s="T8">Ewenke-PL.[NOM]</ta>
            <ta e="T10" id="Seg_3462" s="T9">nur</ta>
            <ta e="T11" id="Seg_3463" s="T10">einmal</ta>
            <ta e="T12" id="Seg_3464" s="T11">eins</ta>
            <ta e="T13" id="Seg_3465" s="T12">Nganasane.[NOM]</ta>
            <ta e="T14" id="Seg_3466" s="T13">Frau-3SG-ACC</ta>
            <ta e="T15" id="Seg_3467" s="T14">Tschangit-PL.[NOM]</ta>
            <ta e="T16" id="Seg_3468" s="T15">stehlen-PST2-3PL</ta>
            <ta e="T17" id="Seg_3469" s="T16">dieses</ta>
            <ta e="T18" id="Seg_3470" s="T17">Frau.[NOM]</ta>
            <ta e="T19" id="Seg_3471" s="T18">Ehemann-3SG.[NOM]</ta>
            <ta e="T20" id="Seg_3472" s="T19">wildes.Rentier-VBZ-CVB.SIM</ta>
            <ta e="T21" id="Seg_3473" s="T20">gehen-PTCP.PST-3SG-ACC</ta>
            <ta e="T22" id="Seg_3474" s="T21">nachdem</ta>
            <ta e="T23" id="Seg_3475" s="T22">nähen-CVB.SIM</ta>
            <ta e="T24" id="Seg_3476" s="T23">sitzen-PST2.[3SG]</ta>
            <ta e="T25" id="Seg_3477" s="T24">jenes</ta>
            <ta e="T26" id="Seg_3478" s="T25">sitzen-TEMP-3SG</ta>
            <ta e="T27" id="Seg_3479" s="T26">Hund-PL.[NOM]</ta>
            <ta e="T28" id="Seg_3480" s="T27">bellen-PST2-3PL</ta>
            <ta e="T29" id="Seg_3481" s="T28">Herr-3PL-ACC</ta>
            <ta e="T30" id="Seg_3482" s="T29">Hund-PL.[NOM]</ta>
            <ta e="T31" id="Seg_3483" s="T30">bellen-PTCP.FUT</ta>
            <ta e="T32" id="Seg_3484" s="T31">NEG</ta>
            <ta e="T33" id="Seg_3485" s="T32">sein-PST1-3PL</ta>
            <ta e="T34" id="Seg_3486" s="T33">denken-CVB.SIM</ta>
            <ta e="T35" id="Seg_3487" s="T34">denken-PST2.[3SG]</ta>
            <ta e="T36" id="Seg_3488" s="T35">Feuer-3SG-ACC</ta>
            <ta e="T37" id="Seg_3489" s="T36">anzünden-CVB.PURP</ta>
            <ta e="T38" id="Seg_3490" s="T37">wollen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T39" id="Seg_3491" s="T38">mit</ta>
            <ta e="T40" id="Seg_3492" s="T39">drei</ta>
            <ta e="T41" id="Seg_3493" s="T40">Mensch.[NOM]</ta>
            <ta e="T42" id="Seg_3494" s="T41">hineingehen-PST2.[3SG]</ta>
            <ta e="T43" id="Seg_3495" s="T42">und</ta>
            <ta e="T44" id="Seg_3496" s="T43">Frau-ACC</ta>
            <ta e="T45" id="Seg_3497" s="T44">Stoff-DAT/LOC</ta>
            <ta e="T46" id="Seg_3498" s="T45">einwickeln-CVB.SEQ</ta>
            <ta e="T47" id="Seg_3499" s="T46">nachdem</ta>
            <ta e="T48" id="Seg_3500" s="T47">tragen-CVB.SIM</ta>
            <ta e="T49" id="Seg_3501" s="T48">gehen-PST2-3PL</ta>
            <ta e="T50" id="Seg_3502" s="T49">am.Abend</ta>
            <ta e="T51" id="Seg_3503" s="T50">Nganasane.[NOM]</ta>
            <ta e="T52" id="Seg_3504" s="T51">kommen-PST2-3SG</ta>
            <ta e="T53" id="Seg_3505" s="T52">Hund-3PL.[NOM]</ta>
            <ta e="T54" id="Seg_3506" s="T53">binden-EP-PASS/REFL-CVB.SIM</ta>
            <ta e="T55" id="Seg_3507" s="T54">liegen-PRS-3PL</ta>
            <ta e="T56" id="Seg_3508" s="T55">Zelt-3SG.[NOM]</ta>
            <ta e="T57" id="Seg_3509" s="T56">heftig</ta>
            <ta e="T58" id="Seg_3510" s="T57">frieren-PST2.[3SG]</ta>
            <ta e="T59" id="Seg_3511" s="T58">wer.[NOM]</ta>
            <ta e="T60" id="Seg_3512" s="T59">NEG</ta>
            <ta e="T61" id="Seg_3513" s="T60">NEG.EX</ta>
            <ta e="T62" id="Seg_3514" s="T61">EMPH-dunkel.[NOM]</ta>
            <ta e="T63" id="Seg_3515" s="T62">Sünde.[NOM]</ta>
            <ta e="T64" id="Seg_3516" s="T63">EMPH</ta>
            <ta e="T65" id="Seg_3517" s="T64">Frau-EP-1SG.[NOM]</ta>
            <ta e="T66" id="Seg_3518" s="T65">wohin</ta>
            <ta e="T67" id="Seg_3519" s="T66">gehen-PST2-3SG</ta>
            <ta e="T68" id="Seg_3520" s="T67">sein-FUT.[3SG]=Q</ta>
            <ta e="T69" id="Seg_3521" s="T68">nachdenken-PST2.[3SG]</ta>
            <ta e="T70" id="Seg_3522" s="T69">am.Morgen</ta>
            <ta e="T71" id="Seg_3523" s="T70">hell.werden-PTCP.PRS-3SG-ACC</ta>
            <ta e="T72" id="Seg_3524" s="T71">mit</ta>
            <ta e="T73" id="Seg_3525" s="T72">Herde-3SG-ACC</ta>
            <ta e="T74" id="Seg_3526" s="T73">bringen-CVB.SIM</ta>
            <ta e="T75" id="Seg_3527" s="T74">gehen-PST2.[3SG]</ta>
            <ta e="T76" id="Seg_3528" s="T75">Rentier-3SG.[NOM]</ta>
            <ta e="T77" id="Seg_3529" s="T76">jeder-3SG.[NOM]</ta>
            <ta e="T78" id="Seg_3530" s="T77">es.gibt</ta>
            <ta e="T79" id="Seg_3531" s="T78">sein-PST2.[3SG]</ta>
            <ta e="T80" id="Seg_3532" s="T79">1SG.[NOM]</ta>
            <ta e="T81" id="Seg_3533" s="T80">Ewenke-1SG-DAT/LOC</ta>
            <ta e="T82" id="Seg_3534" s="T81">wegfahren-FUT-1SG</ta>
            <ta e="T83" id="Seg_3535" s="T82">was-ACC</ta>
            <ta e="T84" id="Seg_3536" s="T83">machen-FUT-1SG=Q</ta>
            <ta e="T85" id="Seg_3537" s="T84">einsam</ta>
            <ta e="T86" id="Seg_3538" s="T85">raten-EP-RECP/COLL-FUT-1SG</ta>
            <ta e="T87" id="Seg_3539" s="T86">EMPH</ta>
            <ta e="T88" id="Seg_3540" s="T87">denken-CVB.SEQ</ta>
            <ta e="T89" id="Seg_3541" s="T88">denken-PST2.[3SG]</ta>
            <ta e="T90" id="Seg_3542" s="T89">ganz.und.gar</ta>
            <ta e="T91" id="Seg_3543" s="T90">einsam</ta>
            <ta e="T92" id="Seg_3544" s="T91">Mensch.[NOM]</ta>
            <ta e="T93" id="Seg_3545" s="T92">lange</ta>
            <ta e="T94" id="Seg_3546" s="T93">sammeln-REFL-FUT.[3SG]=Q</ta>
            <ta e="T95" id="Seg_3547" s="T94">Ewenke-DAT/LOC</ta>
            <ta e="T96" id="Seg_3548" s="T95">nomadisieren-CVB.SEQ</ta>
            <ta e="T97" id="Seg_3549" s="T96">kommen-PST2-3SG</ta>
            <ta e="T98" id="Seg_3550" s="T97">Mensch-3SG.[NOM]</ta>
            <ta e="T99" id="Seg_3551" s="T98">Netz-VBZ-CVB.SIM</ta>
            <ta e="T100" id="Seg_3552" s="T99">gehen-PTCP.PST</ta>
            <ta e="T101" id="Seg_3553" s="T100">sein-PST2.[3SG]</ta>
            <ta e="T102" id="Seg_3554" s="T101">Mutter-3SG.[NOM]</ta>
            <ta e="T103" id="Seg_3555" s="T102">nur</ta>
            <ta e="T104" id="Seg_3556" s="T103">es.gibt</ta>
            <ta e="T105" id="Seg_3557" s="T104">Ewenke.[NOM]</ta>
            <ta e="T106" id="Seg_3558" s="T105">EMPH-einsam</ta>
            <ta e="T107" id="Seg_3559" s="T106">leben-PTCP.PRS</ta>
            <ta e="T108" id="Seg_3560" s="T107">sein-PST2.[3SG]</ta>
            <ta e="T109" id="Seg_3561" s="T108">am.Abend</ta>
            <ta e="T110" id="Seg_3562" s="T109">kommen-PST2.[3SG]</ta>
            <ta e="T111" id="Seg_3563" s="T110">Nganasane.[NOM]</ta>
            <ta e="T112" id="Seg_3564" s="T111">Qual-PROPR</ta>
            <ta e="T113" id="Seg_3565" s="T112">erzählen-PST2.[3SG]</ta>
            <ta e="T114" id="Seg_3566" s="T113">Frau-3SG.[NOM]</ta>
            <ta e="T115" id="Seg_3567" s="T114">verlorengehen-PTCP.PST-3SG-ACC</ta>
            <ta e="T116" id="Seg_3568" s="T115">doch</ta>
            <ta e="T117" id="Seg_3569" s="T116">wer.[NOM]</ta>
            <ta e="T118" id="Seg_3570" s="T117">machen-FUT.[3SG]=Q</ta>
            <ta e="T119" id="Seg_3571" s="T118">gesteren</ta>
            <ta e="T120" id="Seg_3572" s="T119">1SG-DAT/LOC</ta>
            <ta e="T121" id="Seg_3573" s="T120">drei</ta>
            <ta e="T122" id="Seg_3574" s="T121">Mensch.[NOM]</ta>
            <ta e="T123" id="Seg_3575" s="T122">es.gibt</ta>
            <ta e="T124" id="Seg_3576" s="T123">sein-PST1-3PL</ta>
            <ta e="T125" id="Seg_3577" s="T124">Aussehen-3PL.[NOM]</ta>
            <ta e="T126" id="Seg_3578" s="T125">EMPH-so.viel-POSS</ta>
            <ta e="T127" id="Seg_3579" s="T126">NEG</ta>
            <ta e="T128" id="Seg_3580" s="T127">sein-PST1-3SG</ta>
            <ta e="T129" id="Seg_3581" s="T128">wahrscheinlich</ta>
            <ta e="T130" id="Seg_3582" s="T129">Tschangit-PL.[NOM]</ta>
            <ta e="T131" id="Seg_3583" s="T130">offenbar</ta>
            <ta e="T132" id="Seg_3584" s="T131">1SG-ABL</ta>
            <ta e="T133" id="Seg_3585" s="T132">was-1SG-ACC</ta>
            <ta e="T134" id="Seg_3586" s="T133">nehmen-FUT-3PL=Q</ta>
            <ta e="T135" id="Seg_3587" s="T134">nah</ta>
            <ta e="T136" id="Seg_3588" s="T135">es.gibt</ta>
            <ta e="T137" id="Seg_3589" s="T136">Nachbar-PL.[NOM]</ta>
            <ta e="T138" id="Seg_3590" s="T137">auch</ta>
            <ta e="T139" id="Seg_3591" s="T138">erzählen-PRS-3PL</ta>
            <ta e="T140" id="Seg_3592" s="T139">wie</ta>
            <ta e="T141" id="Seg_3593" s="T140">3PL.[NOM]</ta>
            <ta e="T142" id="Seg_3594" s="T141">Frau-PL-ACC</ta>
            <ta e="T143" id="Seg_3595" s="T142">stehlen-CVB.SIM</ta>
            <ta e="T144" id="Seg_3596" s="T143">gehen-PTCP.PRS-3PL-ACC</ta>
            <ta e="T145" id="Seg_3597" s="T144">Ewenke.[NOM]</ta>
            <ta e="T146" id="Seg_3598" s="T145">sagen-PST2.[3SG]</ta>
            <ta e="T147" id="Seg_3599" s="T146">2SG.[NOM]</ta>
            <ta e="T148" id="Seg_3600" s="T147">wie.viel</ta>
            <ta e="T149" id="Seg_3601" s="T148">mutig-2SG=Q</ta>
            <ta e="T150" id="Seg_3602" s="T149">Ewenke.[NOM]</ta>
            <ta e="T151" id="Seg_3603" s="T150">Freund-3SG-ACC</ta>
            <ta e="T152" id="Seg_3604" s="T151">fragen-PST2.[3SG]</ta>
            <ta e="T153" id="Seg_3605" s="T152">mutig.[NOM]</ta>
            <ta e="T154" id="Seg_3606" s="T153">sein-CVB.SEQ-1SG</ta>
            <ta e="T155" id="Seg_3607" s="T154">gehen-PTCP.PRS</ta>
            <ta e="T156" id="Seg_3608" s="T155">offenbar-1SG</ta>
            <ta e="T157" id="Seg_3609" s="T156">sagen-PST2.[3SG]</ta>
            <ta e="T158" id="Seg_3610" s="T157">Nganasane-3SG.[NOM]</ta>
            <ta e="T159" id="Seg_3611" s="T158">dann</ta>
            <ta e="T160" id="Seg_3612" s="T159">1SG.[NOM]</ta>
            <ta e="T161" id="Seg_3613" s="T160">2SG-ACC</ta>
            <ta e="T162" id="Seg_3614" s="T161">wissen-FUT-1SG</ta>
            <ta e="T163" id="Seg_3615" s="T162">wie.viel</ta>
            <ta e="T164" id="Seg_3616" s="T163">kräftig-2SG=Q</ta>
            <ta e="T165" id="Seg_3617" s="T164">1SG.[NOM]</ta>
            <ta e="T166" id="Seg_3618" s="T165">2SG-ACC</ta>
            <ta e="T167" id="Seg_3619" s="T166">drei</ta>
            <ta e="T168" id="Seg_3620" s="T167">Hund.[NOM]</ta>
            <ta e="T169" id="Seg_3621" s="T168">Schlitten-3SG.[NOM]</ta>
            <ta e="T170" id="Seg_3622" s="T169">ungefähr-3SG-ABL</ta>
            <ta e="T171" id="Seg_3623" s="T170">schießen-FUT-1SG</ta>
            <ta e="T172" id="Seg_3624" s="T171">Ewenke.[NOM]</ta>
            <ta e="T173" id="Seg_3625" s="T172">drei-MLTP</ta>
            <ta e="T174" id="Seg_3626" s="T173">schießen-PST2.[3SG]</ta>
            <ta e="T175" id="Seg_3627" s="T174">Saum-3SG-GEN</ta>
            <ta e="T176" id="Seg_3628" s="T175">nur</ta>
            <ta e="T177" id="Seg_3629" s="T176">Rand-3SG-ACC</ta>
            <ta e="T178" id="Seg_3630" s="T177">zerreißen-CVB.SIM</ta>
            <ta e="T179" id="Seg_3631" s="T178">rennen-EP-CAUS-PST2.[3SG]</ta>
            <ta e="T180" id="Seg_3632" s="T179">jetzt</ta>
            <ta e="T181" id="Seg_3633" s="T180">2SG.[NOM]</ta>
            <ta e="T182" id="Seg_3634" s="T181">schießen.[IMP.2SG]</ta>
            <ta e="T183" id="Seg_3635" s="T182">sagen-PST2.[3SG]</ta>
            <ta e="T184" id="Seg_3636" s="T183">Nganasane.[NOM]</ta>
            <ta e="T185" id="Seg_3637" s="T184">schießen-PST2.[3SG]</ta>
            <ta e="T186" id="Seg_3638" s="T185">treffen-PST2.NEG.[3SG]</ta>
            <ta e="T187" id="Seg_3639" s="T186">doch</ta>
            <ta e="T188" id="Seg_3640" s="T187">kräftig.[NOM]</ta>
            <ta e="T189" id="Seg_3641" s="T188">mutig-2SG</ta>
            <ta e="T190" id="Seg_3642" s="T189">sein-PST2.[3SG]</ta>
            <ta e="T191" id="Seg_3643" s="T190">Ewenke-3SG.[NOM]</ta>
            <ta e="T192" id="Seg_3644" s="T191">loben-PST2.[3SG]</ta>
            <ta e="T193" id="Seg_3645" s="T192">nächster.Morgen-3SG-ACC</ta>
            <ta e="T194" id="Seg_3646" s="T193">wegfahren-PST2-3PL</ta>
            <ta e="T195" id="Seg_3647" s="T194">drei</ta>
            <ta e="T196" id="Seg_3648" s="T195">Bergrücken-ACC</ta>
            <ta e="T197" id="Seg_3649" s="T196">überqueren-PST2-3PL</ta>
            <ta e="T198" id="Seg_3650" s="T197">doch</ta>
            <ta e="T199" id="Seg_3651" s="T198">1SG.[NOM]</ta>
            <ta e="T200" id="Seg_3652" s="T199">Mutter-1SG-ACC</ta>
            <ta e="T201" id="Seg_3653" s="T200">nganasanisch-SIM</ta>
            <ta e="T202" id="Seg_3654" s="T201">sich.anziehen-CAUS-CVB.SEQ</ta>
            <ta e="T203" id="Seg_3655" s="T202">nachdem</ta>
            <ta e="T204" id="Seg_3656" s="T203">gehen-FUT-1SG</ta>
            <ta e="T205" id="Seg_3657" s="T204">1SG.[NOM]</ta>
            <ta e="T206" id="Seg_3658" s="T205">3PL-ACC</ta>
            <ta e="T207" id="Seg_3659" s="T206">Betrug-VBZ-FUT-1SG</ta>
            <ta e="T208" id="Seg_3660" s="T207">sagen-FUT-1SG</ta>
            <ta e="T209" id="Seg_3661" s="T208">dieses</ta>
            <ta e="T210" id="Seg_3662" s="T209">Frau.[NOM]</ta>
            <ta e="T211" id="Seg_3663" s="T210">2PL.[NOM]</ta>
            <ta e="T212" id="Seg_3664" s="T211">nehmen-PTCP.HAB</ta>
            <ta e="T213" id="Seg_3665" s="T212">Frau-2PL.[NOM]</ta>
            <ta e="T214" id="Seg_3666" s="T213">ältere.Schwester-3SG.[NOM]</ta>
            <ta e="T215" id="Seg_3667" s="T214">dieses</ta>
            <ta e="T216" id="Seg_3668" s="T215">Frau.[NOM]</ta>
            <ta e="T217" id="Seg_3669" s="T216">1SG.[NOM]</ta>
            <ta e="T218" id="Seg_3670" s="T217">Frau-EP-1SG.[NOM]</ta>
            <ta e="T219" id="Seg_3671" s="T218">2SG.[NOM]</ta>
            <ta e="T220" id="Seg_3672" s="T219">aber</ta>
            <ta e="T221" id="Seg_3673" s="T220">1SG.[NOM]</ta>
            <ta e="T222" id="Seg_3674" s="T221">Frau-EP-1SG.[NOM]</ta>
            <ta e="T223" id="Seg_3675" s="T222">jüngerer.Bruder-EP-3SG.[NOM]</ta>
            <ta e="T224" id="Seg_3676" s="T223">sein-FUT-2SG</ta>
            <ta e="T225" id="Seg_3677" s="T224">Ewenke.[NOM]</ta>
            <ta e="T226" id="Seg_3678" s="T225">Mutter-3SG-ACC</ta>
            <ta e="T227" id="Seg_3679" s="T226">mit</ta>
            <ta e="T228" id="Seg_3680" s="T227">gehen-CVB.SEQ</ta>
            <ta e="T229" id="Seg_3681" s="T228">bleiben-PST2-3PL</ta>
            <ta e="T230" id="Seg_3682" s="T229">ankommen-PST2-3PL</ta>
            <ta e="T231" id="Seg_3683" s="T230">sieben</ta>
            <ta e="T232" id="Seg_3684" s="T231">Stange.[NOM]</ta>
            <ta e="T233" id="Seg_3685" s="T232">Zelt.[NOM]</ta>
            <ta e="T234" id="Seg_3686" s="T233">stehen-PRS-3PL</ta>
            <ta e="T235" id="Seg_3687" s="T234">jenes-ACC</ta>
            <ta e="T236" id="Seg_3688" s="T235">um.herum</ta>
            <ta e="T237" id="Seg_3689" s="T236">Mensch-PL.[NOM]</ta>
            <ta e="T238" id="Seg_3690" s="T237">tanzen-CVB.SIM</ta>
            <ta e="T239" id="Seg_3691" s="T238">gehen-PRS-3PL</ta>
            <ta e="T240" id="Seg_3692" s="T239">wahrscheinlich</ta>
            <ta e="T241" id="Seg_3693" s="T240">Hochzeit.[NOM]</ta>
            <ta e="T242" id="Seg_3694" s="T241">offenbar</ta>
            <ta e="T243" id="Seg_3695" s="T242">denken-CVB.SIM</ta>
            <ta e="T244" id="Seg_3696" s="T243">denken-PST2-3PL</ta>
            <ta e="T245" id="Seg_3697" s="T244">Ewenke.[NOM]</ta>
            <ta e="T246" id="Seg_3698" s="T245">eins</ta>
            <ta e="T247" id="Seg_3699" s="T246">Mensch-ACC</ta>
            <ta e="T248" id="Seg_3700" s="T247">umarmen-PST2.[3SG]</ta>
            <ta e="T249" id="Seg_3701" s="T248">und</ta>
            <ta e="T250" id="Seg_3702" s="T249">tanzen-CVB.SIM</ta>
            <ta e="T251" id="Seg_3703" s="T250">gehen-EP-PST2-3PL</ta>
            <ta e="T252" id="Seg_3704" s="T251">jenes</ta>
            <ta e="T253" id="Seg_3705" s="T252">gehen-CVB.SEQ</ta>
            <ta e="T254" id="Seg_3706" s="T253">fragen-PST2.[3SG]</ta>
            <ta e="T255" id="Seg_3707" s="T254">wo</ta>
            <ta e="T256" id="Seg_3708" s="T255">es.gibt=Q</ta>
            <ta e="T257" id="Seg_3709" s="T256">groß-3PL-GEN</ta>
            <ta e="T258" id="Seg_3710" s="T257">Zelt-3SG.[NOM]</ta>
            <ta e="T259" id="Seg_3711" s="T258">dort</ta>
            <ta e="T260" id="Seg_3712" s="T259">Nganasane.[NOM]</ta>
            <ta e="T261" id="Seg_3713" s="T260">Frau-3SG.[NOM]</ta>
            <ta e="T262" id="Seg_3714" s="T261">es.gibt</ta>
            <ta e="T263" id="Seg_3715" s="T262">dieses</ta>
            <ta e="T264" id="Seg_3716" s="T263">Mensch.[NOM]</ta>
            <ta e="T265" id="Seg_3717" s="T264">sich.ärgern-CVB.SIM-sich.ärgern-CVB.SIM</ta>
            <ta e="T266" id="Seg_3718" s="T265">sprechen-PST2.[3SG]</ta>
            <ta e="T267" id="Seg_3719" s="T266">zeigen-PST2.[3SG]</ta>
            <ta e="T268" id="Seg_3720" s="T267">Zelt-3SG-ACC</ta>
            <ta e="T269" id="Seg_3721" s="T268">Ewenke.[NOM]</ta>
            <ta e="T270" id="Seg_3722" s="T269">hineingehen-PST2-3SG</ta>
            <ta e="T271" id="Seg_3723" s="T270">Frau.[NOM]</ta>
            <ta e="T272" id="Seg_3724" s="T271">Auge-3SG.[NOM]</ta>
            <ta e="T273" id="Seg_3725" s="T272">weinen-CVB.SEQ</ta>
            <ta e="T274" id="Seg_3726" s="T273">Schöpfkelle.[NOM]</ta>
            <ta e="T275" id="Seg_3727" s="T274">von.der.Größe.von</ta>
            <ta e="T276" id="Seg_3728" s="T275">werden-PST2.[3SG]</ta>
            <ta e="T277" id="Seg_3729" s="T276">Ewenke.[NOM]</ta>
            <ta e="T278" id="Seg_3730" s="T277">hineingehen-PST2.[3SG]</ta>
            <ta e="T279" id="Seg_3731" s="T278">und</ta>
            <ta e="T280" id="Seg_3732" s="T279">groß-3PL-ACC</ta>
            <ta e="T281" id="Seg_3733" s="T280">zu</ta>
            <ta e="T282" id="Seg_3734" s="T281">schreiten-PST2.[3SG]</ta>
            <ta e="T283" id="Seg_3735" s="T282">1SG.[NOM]</ta>
            <ta e="T284" id="Seg_3736" s="T283">Frau-EP-1SG.[NOM]</ta>
            <ta e="T285" id="Seg_3737" s="T284">jüngere.Schwester-EP-3SG-ACC</ta>
            <ta e="T286" id="Seg_3738" s="T285">Frau.[NOM]</ta>
            <ta e="T287" id="Seg_3739" s="T286">machen-CVB.SEQ</ta>
            <ta e="T288" id="Seg_3740" s="T287">sitzen-PRS-2SG</ta>
            <ta e="T289" id="Seg_3741" s="T288">1SG.[NOM]</ta>
            <ta e="T290" id="Seg_3742" s="T289">Frau-EP-1SG.[NOM]</ta>
            <ta e="T291" id="Seg_3743" s="T290">jüngere.Schwester-EP-3SG-ACC</ta>
            <ta e="T292" id="Seg_3744" s="T291">treffen-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T293" id="Seg_3745" s="T292">wollen-PRS.[3SG]</ta>
            <ta e="T294" id="Seg_3746" s="T293">doch</ta>
            <ta e="T295" id="Seg_3747" s="T294">gut</ta>
            <ta e="T296" id="Seg_3748" s="T295">morgen</ta>
            <ta e="T297" id="Seg_3749" s="T296">gehen-FUT-1PL</ta>
            <ta e="T298" id="Seg_3750" s="T297">sehen-CVB.SIM</ta>
            <ta e="T299" id="Seg_3751" s="T298">sagen-PST2.[3SG]</ta>
            <ta e="T300" id="Seg_3752" s="T299">Tschangit.[NOM]</ta>
            <ta e="T301" id="Seg_3753" s="T300">nächster.Morgen-3SG-ACC</ta>
            <ta e="T302" id="Seg_3754" s="T301">dieses</ta>
            <ta e="T303" id="Seg_3755" s="T302">Frau-ACC</ta>
            <ta e="T304" id="Seg_3756" s="T303">Tschangit.[NOM]</ta>
            <ta e="T305" id="Seg_3757" s="T304">holen-CVB.SEQ</ta>
            <ta e="T306" id="Seg_3758" s="T305">kommen-PST2.[3SG]</ta>
            <ta e="T307" id="Seg_3759" s="T306">Zelt-DAT/LOC</ta>
            <ta e="T308" id="Seg_3760" s="T307">hineingehen-PTCP.PST-3PL-ACC</ta>
            <ta e="T309" id="Seg_3761" s="T308">nachdem</ta>
            <ta e="T310" id="Seg_3762" s="T309">Nganasane.[NOM]</ta>
            <ta e="T311" id="Seg_3763" s="T310">Tschangit.[NOM]</ta>
            <ta e="T312" id="Seg_3764" s="T311">Rentier-PL-3SG-GEN</ta>
            <ta e="T313" id="Seg_3765" s="T312">Schnur-3SG-ACC</ta>
            <ta e="T314" id="Seg_3766" s="T313">schneiden-CVB.SIM</ta>
            <ta e="T315" id="Seg_3767" s="T314">herunterziehen-PST2.[3SG]</ta>
            <ta e="T316" id="Seg_3768" s="T315">Frau-3SG-ACC</ta>
            <ta e="T317" id="Seg_3769" s="T316">reiten-CVB.SEQ</ta>
            <ta e="T318" id="Seg_3770" s="T317">nachdem</ta>
            <ta e="T319" id="Seg_3771" s="T318">entfliehen-PST2.[3SG]</ta>
            <ta e="T320" id="Seg_3772" s="T319">Tschangit.[NOM]</ta>
            <ta e="T321" id="Seg_3773" s="T320">Hinterteil-3SG-ABL</ta>
            <ta e="T322" id="Seg_3774" s="T321">zu.Fuß</ta>
            <ta e="T323" id="Seg_3775" s="T322">folgen-PST2.[3SG]</ta>
            <ta e="T324" id="Seg_3776" s="T323">Nganasane.[NOM]</ta>
            <ta e="T325" id="Seg_3777" s="T324">Muskel-3SG-ACC</ta>
            <ta e="T326" id="Seg_3778" s="T325">zerreißen-CVB.SIM</ta>
            <ta e="T327" id="Seg_3779" s="T326">schießen-CAUS-PST2.[3SG]</ta>
            <ta e="T328" id="Seg_3780" s="T327">halten-CVB.SIM</ta>
            <ta e="T329" id="Seg_3781" s="T328">fallen-CVB.SEQ</ta>
            <ta e="T330" id="Seg_3782" s="T329">nachdem</ta>
            <ta e="T331" id="Seg_3783" s="T330">Nganasane.[NOM]</ta>
            <ta e="T332" id="Seg_3784" s="T331">entgegen</ta>
            <ta e="T333" id="Seg_3785" s="T332">go-PST2.[3SG]</ta>
            <ta e="T334" id="Seg_3786" s="T333">Tschangit.[NOM]</ta>
            <ta e="T335" id="Seg_3787" s="T334">Ohr-3SG-GEN</ta>
            <ta e="T336" id="Seg_3788" s="T335">Rand-3SG-INSTR</ta>
            <ta e="T337" id="Seg_3789" s="T336">spalten-CVB.SIM</ta>
            <ta e="T338" id="Seg_3790" s="T337">werfen-PST2.[3SG]</ta>
            <ta e="T339" id="Seg_3791" s="T338">Tschangit.[NOM]</ta>
            <ta e="T340" id="Seg_3792" s="T339">Pfeil-3SG-ACC</ta>
            <ta e="T341" id="Seg_3793" s="T340">jeder-3SG-ACC</ta>
            <ta e="T342" id="Seg_3794" s="T341">schießen-PST2.[3SG]</ta>
            <ta e="T343" id="Seg_3795" s="T342">und</ta>
            <ta e="T344" id="Seg_3796" s="T343">entfliehen-PST2.[3SG]</ta>
            <ta e="T345" id="Seg_3797" s="T344">Ewenke-ACC</ta>
            <ta e="T346" id="Seg_3798" s="T345">mit</ta>
            <ta e="T347" id="Seg_3799" s="T346">Nganasane.[NOM]</ta>
            <ta e="T348" id="Seg_3800" s="T347">folgen-PST2-3PL</ta>
            <ta e="T349" id="Seg_3801" s="T348">verfolgen-CVB.SIM</ta>
            <ta e="T350" id="Seg_3802" s="T349">folgen-ITER-CVB.SEQ</ta>
            <ta e="T351" id="Seg_3803" s="T350">nachdem</ta>
            <ta e="T352" id="Seg_3804" s="T351">Kopf-3SG-ACC</ta>
            <ta e="T353" id="Seg_3805" s="T352">spalten-CVB.SIM</ta>
            <ta e="T354" id="Seg_3806" s="T353">schlagen-EP-PST2-3PL</ta>
            <ta e="T355" id="Seg_3807" s="T354">jenes.[NOM]</ta>
            <ta e="T356" id="Seg_3808" s="T355">Hinterteil-3SG-ABL</ta>
            <ta e="T357" id="Seg_3809" s="T356">Ewenke.[NOM]</ta>
            <ta e="T358" id="Seg_3810" s="T357">Frau-VBZ-PST2.[3SG]</ta>
            <ta e="T359" id="Seg_3811" s="T358">Tschangit.[NOM]</ta>
            <ta e="T360" id="Seg_3812" s="T359">Rentier-PL-3SG-INSTR</ta>
            <ta e="T361" id="Seg_3813" s="T360">reich.sein-CVB.SEQ-sich.satt.essen-CVB.SEQ</ta>
            <ta e="T362" id="Seg_3814" s="T361">leben-PST2-3PL</ta>
            <ta e="T363" id="Seg_3815" s="T362">dieses.[NOM]</ta>
            <ta e="T364" id="Seg_3816" s="T363">ähnlich</ta>
            <ta e="T365" id="Seg_3817" s="T364">Nganasane-PL-ACC</ta>
            <ta e="T366" id="Seg_3818" s="T365">mit</ta>
            <ta e="T367" id="Seg_3819" s="T366">Ewenke-PL.[NOM]</ta>
            <ta e="T368" id="Seg_3820" s="T367">verbinden-EP-RECP/COLL-CVB.SEQ</ta>
            <ta e="T369" id="Seg_3821" s="T368">leben-PST2-3PL</ta>
            <ta e="T370" id="Seg_3822" s="T369">Nganasane-PL.[NOM]</ta>
            <ta e="T371" id="Seg_3823" s="T370">jenes.[NOM]</ta>
            <ta e="T372" id="Seg_3824" s="T371">Hinterteil-3SG-ABL</ta>
            <ta e="T373" id="Seg_3825" s="T372">Angst.haben-CVB.SEQ</ta>
            <ta e="T374" id="Seg_3826" s="T373">Taas.[NOM]</ta>
            <ta e="T375" id="Seg_3827" s="T374">zu</ta>
            <ta e="T376" id="Seg_3828" s="T375">nomadisieren-NEG.PTCP</ta>
            <ta e="T377" id="Seg_3829" s="T376">werden-PST-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_3830" s="T0">Нганасан-PL.[NOM]</ta>
            <ta e="T2" id="Seg_3831" s="T1">давно-давно</ta>
            <ta e="T3" id="Seg_3832" s="T2">Кэрикэ-DAT/LOC</ta>
            <ta e="T4" id="Seg_3833" s="T3">жить-PTCP.PRS</ta>
            <ta e="T5" id="Seg_3834" s="T4">быть-PST2-3PL</ta>
            <ta e="T6" id="Seg_3835" s="T5">Есей-DAT/LOC</ta>
            <ta e="T7" id="Seg_3836" s="T6">Чангит-PL.[NOM]</ta>
            <ta e="T8" id="Seg_3837" s="T7">Камень-DAT/LOC</ta>
            <ta e="T9" id="Seg_3838" s="T8">эвенк-PL.[NOM]</ta>
            <ta e="T10" id="Seg_3839" s="T9">только</ta>
            <ta e="T11" id="Seg_3840" s="T10">однажды</ta>
            <ta e="T12" id="Seg_3841" s="T11">один</ta>
            <ta e="T13" id="Seg_3842" s="T12">Нганасан.[NOM]</ta>
            <ta e="T14" id="Seg_3843" s="T13">жена-3SG-ACC</ta>
            <ta e="T15" id="Seg_3844" s="T14">Чангит-PL.[NOM]</ta>
            <ta e="T16" id="Seg_3845" s="T15">красть-PST2-3PL</ta>
            <ta e="T17" id="Seg_3846" s="T16">этот</ta>
            <ta e="T18" id="Seg_3847" s="T17">жена.[NOM]</ta>
            <ta e="T19" id="Seg_3848" s="T18">муж-3SG.[NOM]</ta>
            <ta e="T20" id="Seg_3849" s="T19">дикий.олень-VBZ-CVB.SIM</ta>
            <ta e="T21" id="Seg_3850" s="T20">идти-PTCP.PST-3SG-ACC</ta>
            <ta e="T22" id="Seg_3851" s="T21">после.того</ta>
            <ta e="T23" id="Seg_3852" s="T22">шить-CVB.SIM</ta>
            <ta e="T24" id="Seg_3853" s="T23">сидеть-PST2.[3SG]</ta>
            <ta e="T25" id="Seg_3854" s="T24">тот</ta>
            <ta e="T26" id="Seg_3855" s="T25">сидеть-TEMP-3SG</ta>
            <ta e="T27" id="Seg_3856" s="T26">собака-PL.[NOM]</ta>
            <ta e="T28" id="Seg_3857" s="T27">лаять-PST2-3PL</ta>
            <ta e="T29" id="Seg_3858" s="T28">господин-3PL-ACC</ta>
            <ta e="T30" id="Seg_3859" s="T29">собака-PL.[NOM]</ta>
            <ta e="T31" id="Seg_3860" s="T30">лаять-PTCP.FUT</ta>
            <ta e="T32" id="Seg_3861" s="T31">NEG</ta>
            <ta e="T33" id="Seg_3862" s="T32">быть-PST1-3PL</ta>
            <ta e="T34" id="Seg_3863" s="T33">думать-CVB.SIM</ta>
            <ta e="T35" id="Seg_3864" s="T34">думать-PST2.[3SG]</ta>
            <ta e="T36" id="Seg_3865" s="T35">огонь-3SG-ACC</ta>
            <ta e="T37" id="Seg_3866" s="T36">зажигать-CVB.PURP</ta>
            <ta e="T38" id="Seg_3867" s="T37">хотеть-PTCP.PRS-3SG-ACC</ta>
            <ta e="T39" id="Seg_3868" s="T38">с</ta>
            <ta e="T40" id="Seg_3869" s="T39">три</ta>
            <ta e="T41" id="Seg_3870" s="T40">человек.[NOM]</ta>
            <ta e="T42" id="Seg_3871" s="T41">входить-PST2.[3SG]</ta>
            <ta e="T43" id="Seg_3872" s="T42">да</ta>
            <ta e="T44" id="Seg_3873" s="T43">жена-ACC</ta>
            <ta e="T45" id="Seg_3874" s="T44">ткань-DAT/LOC</ta>
            <ta e="T46" id="Seg_3875" s="T45">укутать-CVB.SEQ</ta>
            <ta e="T47" id="Seg_3876" s="T46">после</ta>
            <ta e="T48" id="Seg_3877" s="T47">носить-CVB.SIM</ta>
            <ta e="T49" id="Seg_3878" s="T48">идти-PST2-3PL</ta>
            <ta e="T50" id="Seg_3879" s="T49">вечером</ta>
            <ta e="T51" id="Seg_3880" s="T50">Нганасан.[NOM]</ta>
            <ta e="T52" id="Seg_3881" s="T51">приходить-PST2-3SG</ta>
            <ta e="T53" id="Seg_3882" s="T52">собака-3PL.[NOM]</ta>
            <ta e="T54" id="Seg_3883" s="T53">связывать-EP-PASS/REFL-CVB.SIM</ta>
            <ta e="T55" id="Seg_3884" s="T54">лежать-PRS-3PL</ta>
            <ta e="T56" id="Seg_3885" s="T55">чум-3SG.[NOM]</ta>
            <ta e="T57" id="Seg_3886" s="T56">крепко</ta>
            <ta e="T58" id="Seg_3887" s="T57">мерзнуть-PST2.[3SG]</ta>
            <ta e="T59" id="Seg_3888" s="T58">кто.[NOM]</ta>
            <ta e="T60" id="Seg_3889" s="T59">NEG</ta>
            <ta e="T61" id="Seg_3890" s="T60">NEG.EX</ta>
            <ta e="T62" id="Seg_3891" s="T61">EMPH-темный.[NOM]</ta>
            <ta e="T63" id="Seg_3892" s="T62">грех.[NOM]</ta>
            <ta e="T64" id="Seg_3893" s="T63">EMPH</ta>
            <ta e="T65" id="Seg_3894" s="T64">жена-EP-1SG.[NOM]</ta>
            <ta e="T66" id="Seg_3895" s="T65">куда</ta>
            <ta e="T67" id="Seg_3896" s="T66">идти-PST2-3SG</ta>
            <ta e="T68" id="Seg_3897" s="T67">быть-FUT.[3SG]=Q</ta>
            <ta e="T69" id="Seg_3898" s="T68">задуматься-PST2.[3SG]</ta>
            <ta e="T70" id="Seg_3899" s="T69">утром</ta>
            <ta e="T71" id="Seg_3900" s="T70">посветлеть-PTCP.PRS-3SG-ACC</ta>
            <ta e="T72" id="Seg_3901" s="T71">с</ta>
            <ta e="T73" id="Seg_3902" s="T72">стадо-3SG-ACC</ta>
            <ta e="T74" id="Seg_3903" s="T73">принести-CVB.SIM</ta>
            <ta e="T75" id="Seg_3904" s="T74">идти-PST2.[3SG]</ta>
            <ta e="T76" id="Seg_3905" s="T75">олень-3SG.[NOM]</ta>
            <ta e="T77" id="Seg_3906" s="T76">каждый-3SG.[NOM]</ta>
            <ta e="T78" id="Seg_3907" s="T77">есть</ta>
            <ta e="T79" id="Seg_3908" s="T78">быть-PST2.[3SG]</ta>
            <ta e="T80" id="Seg_3909" s="T79">1SG.[NOM]</ta>
            <ta e="T81" id="Seg_3910" s="T80">эвенк-1SG-DAT/LOC</ta>
            <ta e="T82" id="Seg_3911" s="T81">уезжать-FUT-1SG</ta>
            <ta e="T83" id="Seg_3912" s="T82">что-ACC</ta>
            <ta e="T84" id="Seg_3913" s="T83">делать-FUT-1SG=Q</ta>
            <ta e="T85" id="Seg_3914" s="T84">одиноко</ta>
            <ta e="T86" id="Seg_3915" s="T85">советовать-EP-RECP/COLL-FUT-1SG</ta>
            <ta e="T87" id="Seg_3916" s="T86">EMPH</ta>
            <ta e="T88" id="Seg_3917" s="T87">думать-CVB.SEQ</ta>
            <ta e="T89" id="Seg_3918" s="T88">думать-PST2.[3SG]</ta>
            <ta e="T90" id="Seg_3919" s="T89">совсем</ta>
            <ta e="T91" id="Seg_3920" s="T90">одинаковый</ta>
            <ta e="T92" id="Seg_3921" s="T91">человек.[NOM]</ta>
            <ta e="T93" id="Seg_3922" s="T92">долго</ta>
            <ta e="T94" id="Seg_3923" s="T93">собирать-REFL-FUT.[3SG]=Q</ta>
            <ta e="T95" id="Seg_3924" s="T94">эвенк-DAT/LOC</ta>
            <ta e="T96" id="Seg_3925" s="T95">кочевать-CVB.SEQ</ta>
            <ta e="T97" id="Seg_3926" s="T96">приходить-PST2-3SG</ta>
            <ta e="T98" id="Seg_3927" s="T97">человек-3SG.[NOM]</ta>
            <ta e="T99" id="Seg_3928" s="T98">сеть-VBZ-CVB.SIM</ta>
            <ta e="T100" id="Seg_3929" s="T99">идти-PTCP.PST</ta>
            <ta e="T101" id="Seg_3930" s="T100">быть-PST2.[3SG]</ta>
            <ta e="T102" id="Seg_3931" s="T101">мать-3SG.[NOM]</ta>
            <ta e="T103" id="Seg_3932" s="T102">только</ta>
            <ta e="T104" id="Seg_3933" s="T103">есть</ta>
            <ta e="T105" id="Seg_3934" s="T104">эвенк.[NOM]</ta>
            <ta e="T106" id="Seg_3935" s="T105">EMPH-одиноко</ta>
            <ta e="T107" id="Seg_3936" s="T106">жить-PTCP.PRS</ta>
            <ta e="T108" id="Seg_3937" s="T107">быть-PST2.[3SG]</ta>
            <ta e="T109" id="Seg_3938" s="T108">вечером</ta>
            <ta e="T110" id="Seg_3939" s="T109">приходить-PST2.[3SG]</ta>
            <ta e="T111" id="Seg_3940" s="T110">Нганасан.[NOM]</ta>
            <ta e="T112" id="Seg_3941" s="T111">мука-PROPR</ta>
            <ta e="T113" id="Seg_3942" s="T112">рассказывать-PST2.[3SG]</ta>
            <ta e="T114" id="Seg_3943" s="T113">жена-3SG.[NOM]</ta>
            <ta e="T115" id="Seg_3944" s="T114">пропадать-PTCP.PST-3SG-ACC</ta>
            <ta e="T116" id="Seg_3945" s="T115">вот</ta>
            <ta e="T117" id="Seg_3946" s="T116">кто.[NOM]</ta>
            <ta e="T118" id="Seg_3947" s="T117">делать-FUT.[3SG]=Q</ta>
            <ta e="T119" id="Seg_3948" s="T118">вчера</ta>
            <ta e="T120" id="Seg_3949" s="T119">1SG-DAT/LOC</ta>
            <ta e="T121" id="Seg_3950" s="T120">три</ta>
            <ta e="T122" id="Seg_3951" s="T121">человек.[NOM]</ta>
            <ta e="T123" id="Seg_3952" s="T122">есть</ta>
            <ta e="T124" id="Seg_3953" s="T123">быть-PST1-3PL</ta>
            <ta e="T125" id="Seg_3954" s="T124">внешний.вид-3PL.[NOM]</ta>
            <ta e="T126" id="Seg_3955" s="T125">EMPH-столько-POSS</ta>
            <ta e="T127" id="Seg_3956" s="T126">NEG</ta>
            <ta e="T128" id="Seg_3957" s="T127">быть-PST1-3SG</ta>
            <ta e="T129" id="Seg_3958" s="T128">наверно</ta>
            <ta e="T130" id="Seg_3959" s="T129">Чангит-PL.[NOM]</ta>
            <ta e="T131" id="Seg_3960" s="T130">наверное</ta>
            <ta e="T132" id="Seg_3961" s="T131">1SG-ABL</ta>
            <ta e="T133" id="Seg_3962" s="T132">что-1SG-ACC</ta>
            <ta e="T134" id="Seg_3963" s="T133">взять-FUT-3PL=Q</ta>
            <ta e="T135" id="Seg_3964" s="T134">близкий</ta>
            <ta e="T136" id="Seg_3965" s="T135">есть</ta>
            <ta e="T137" id="Seg_3966" s="T136">сосед-PL.[NOM]</ta>
            <ta e="T138" id="Seg_3967" s="T137">тоже</ta>
            <ta e="T139" id="Seg_3968" s="T138">рассказывать-PRS-3PL</ta>
            <ta e="T140" id="Seg_3969" s="T139">как</ta>
            <ta e="T141" id="Seg_3970" s="T140">3PL.[NOM]</ta>
            <ta e="T142" id="Seg_3971" s="T141">жена-PL-ACC</ta>
            <ta e="T143" id="Seg_3972" s="T142">красть-CVB.SIM</ta>
            <ta e="T144" id="Seg_3973" s="T143">идти-PTCP.PRS-3PL-ACC</ta>
            <ta e="T145" id="Seg_3974" s="T144">эвенк.[NOM]</ta>
            <ta e="T146" id="Seg_3975" s="T145">говорить-PST2.[3SG]</ta>
            <ta e="T147" id="Seg_3976" s="T146">2SG.[NOM]</ta>
            <ta e="T148" id="Seg_3977" s="T147">сколько</ta>
            <ta e="T149" id="Seg_3978" s="T148">смелый-2SG=Q</ta>
            <ta e="T150" id="Seg_3979" s="T149">эвенк.[NOM]</ta>
            <ta e="T151" id="Seg_3980" s="T150">друг-3SG-ACC</ta>
            <ta e="T152" id="Seg_3981" s="T151">спрашивать-PST2.[3SG]</ta>
            <ta e="T153" id="Seg_3982" s="T152">смелый.[NOM]</ta>
            <ta e="T154" id="Seg_3983" s="T153">быть-CVB.SEQ-1SG</ta>
            <ta e="T155" id="Seg_3984" s="T154">идти-PTCP.PRS</ta>
            <ta e="T156" id="Seg_3985" s="T155">очевидно-1SG</ta>
            <ta e="T157" id="Seg_3986" s="T156">говорить-PST2.[3SG]</ta>
            <ta e="T158" id="Seg_3987" s="T157">Нганасан-3SG.[NOM]</ta>
            <ta e="T159" id="Seg_3988" s="T158">тогда</ta>
            <ta e="T160" id="Seg_3989" s="T159">1SG.[NOM]</ta>
            <ta e="T161" id="Seg_3990" s="T160">2SG-ACC</ta>
            <ta e="T162" id="Seg_3991" s="T161">знать-FUT-1SG</ta>
            <ta e="T163" id="Seg_3992" s="T162">сколько</ta>
            <ta e="T164" id="Seg_3993" s="T163">сильный-2SG=Q</ta>
            <ta e="T165" id="Seg_3994" s="T164">1SG.[NOM]</ta>
            <ta e="T166" id="Seg_3995" s="T165">2SG-ACC</ta>
            <ta e="T167" id="Seg_3996" s="T166">три</ta>
            <ta e="T168" id="Seg_3997" s="T167">собака.[NOM]</ta>
            <ta e="T169" id="Seg_3998" s="T168">сани-3SG.[NOM]</ta>
            <ta e="T170" id="Seg_3999" s="T169">около-3SG-ABL</ta>
            <ta e="T171" id="Seg_4000" s="T170">стрелять-FUT-1SG</ta>
            <ta e="T172" id="Seg_4001" s="T171">эвенк.[NOM]</ta>
            <ta e="T173" id="Seg_4002" s="T172">три-MLTP</ta>
            <ta e="T174" id="Seg_4003" s="T173">стрелять-PST2.[3SG]</ta>
            <ta e="T175" id="Seg_4004" s="T174">подол-3SG-GEN</ta>
            <ta e="T176" id="Seg_4005" s="T175">только</ta>
            <ta e="T177" id="Seg_4006" s="T176">край-3SG-ACC</ta>
            <ta e="T178" id="Seg_4007" s="T177">разрывать-CVB.SIM</ta>
            <ta e="T179" id="Seg_4008" s="T178">бежать-EP-CAUS-PST2.[3SG]</ta>
            <ta e="T180" id="Seg_4009" s="T179">теперь</ta>
            <ta e="T181" id="Seg_4010" s="T180">2SG.[NOM]</ta>
            <ta e="T182" id="Seg_4011" s="T181">стрелять.[IMP.2SG]</ta>
            <ta e="T183" id="Seg_4012" s="T182">говорить-PST2.[3SG]</ta>
            <ta e="T184" id="Seg_4013" s="T183">Нганасан.[NOM]</ta>
            <ta e="T185" id="Seg_4014" s="T184">стрелять-PST2.[3SG]</ta>
            <ta e="T186" id="Seg_4015" s="T185">попадать.в-PST2.NEG.[3SG]</ta>
            <ta e="T187" id="Seg_4016" s="T186">вот</ta>
            <ta e="T188" id="Seg_4017" s="T187">сильный.[NOM]</ta>
            <ta e="T189" id="Seg_4018" s="T188">смелый-2SG</ta>
            <ta e="T190" id="Seg_4019" s="T189">быть-PST2.[3SG]</ta>
            <ta e="T191" id="Seg_4020" s="T190">эвенк-3SG.[NOM]</ta>
            <ta e="T192" id="Seg_4021" s="T191">хвалить-PST2.[3SG]</ta>
            <ta e="T193" id="Seg_4022" s="T192">следующее.утро-3SG-ACC</ta>
            <ta e="T194" id="Seg_4023" s="T193">уезжать-PST2-3PL</ta>
            <ta e="T195" id="Seg_4024" s="T194">три</ta>
            <ta e="T196" id="Seg_4025" s="T195">горный.хребет-ACC</ta>
            <ta e="T197" id="Seg_4026" s="T196">переходить-PST2-3PL</ta>
            <ta e="T198" id="Seg_4027" s="T197">вот</ta>
            <ta e="T199" id="Seg_4028" s="T198">1SG.[NOM]</ta>
            <ta e="T200" id="Seg_4029" s="T199">мать-1SG-ACC</ta>
            <ta e="T201" id="Seg_4030" s="T200">нганасанский-SIM</ta>
            <ta e="T202" id="Seg_4031" s="T201">одеваться-CAUS-CVB.SEQ</ta>
            <ta e="T203" id="Seg_4032" s="T202">после</ta>
            <ta e="T204" id="Seg_4033" s="T203">идти-FUT-1SG</ta>
            <ta e="T205" id="Seg_4034" s="T204">1SG.[NOM]</ta>
            <ta e="T206" id="Seg_4035" s="T205">3PL-ACC</ta>
            <ta e="T207" id="Seg_4036" s="T206">обман-VBZ-FUT-1SG</ta>
            <ta e="T208" id="Seg_4037" s="T207">говорить-FUT-1SG</ta>
            <ta e="T209" id="Seg_4038" s="T208">этот</ta>
            <ta e="T210" id="Seg_4039" s="T209">жена.[NOM]</ta>
            <ta e="T211" id="Seg_4040" s="T210">2PL.[NOM]</ta>
            <ta e="T212" id="Seg_4041" s="T211">взять-PTCP.HAB</ta>
            <ta e="T213" id="Seg_4042" s="T212">жена-2PL.[NOM]</ta>
            <ta e="T214" id="Seg_4043" s="T213">старшая.сестра-3SG.[NOM]</ta>
            <ta e="T215" id="Seg_4044" s="T214">этот</ta>
            <ta e="T216" id="Seg_4045" s="T215">жена.[NOM]</ta>
            <ta e="T217" id="Seg_4046" s="T216">1SG.[NOM]</ta>
            <ta e="T218" id="Seg_4047" s="T217">жена-EP-1SG.[NOM]</ta>
            <ta e="T219" id="Seg_4048" s="T218">2SG.[NOM]</ta>
            <ta e="T220" id="Seg_4049" s="T219">однако</ta>
            <ta e="T221" id="Seg_4050" s="T220">1SG.[NOM]</ta>
            <ta e="T222" id="Seg_4051" s="T221">жена-EP-1SG.[NOM]</ta>
            <ta e="T223" id="Seg_4052" s="T222">младший.брат-EP-3SG.[NOM]</ta>
            <ta e="T224" id="Seg_4053" s="T223">быть-FUT-2SG</ta>
            <ta e="T225" id="Seg_4054" s="T224">эвенк.[NOM]</ta>
            <ta e="T226" id="Seg_4055" s="T225">мать-3SG-ACC</ta>
            <ta e="T227" id="Seg_4056" s="T226">с</ta>
            <ta e="T228" id="Seg_4057" s="T227">идти-CVB.SEQ</ta>
            <ta e="T229" id="Seg_4058" s="T228">оставаться-PST2-3PL</ta>
            <ta e="T230" id="Seg_4059" s="T229">доезжать-PST2-3PL</ta>
            <ta e="T231" id="Seg_4060" s="T230">семь</ta>
            <ta e="T232" id="Seg_4061" s="T231">шест.[NOM]</ta>
            <ta e="T233" id="Seg_4062" s="T232">чум.[NOM]</ta>
            <ta e="T234" id="Seg_4063" s="T233">стоять-PRS-3PL</ta>
            <ta e="T235" id="Seg_4064" s="T234">тот-ACC</ta>
            <ta e="T236" id="Seg_4065" s="T235">вокруг</ta>
            <ta e="T237" id="Seg_4066" s="T236">человек-PL.[NOM]</ta>
            <ta e="T238" id="Seg_4067" s="T237">плясать-CVB.SIM</ta>
            <ta e="T239" id="Seg_4068" s="T238">идти-PRS-3PL</ta>
            <ta e="T240" id="Seg_4069" s="T239">наверно</ta>
            <ta e="T241" id="Seg_4070" s="T240">свадьба.[NOM]</ta>
            <ta e="T242" id="Seg_4071" s="T241">наверное</ta>
            <ta e="T243" id="Seg_4072" s="T242">думать-CVB.SIM</ta>
            <ta e="T244" id="Seg_4073" s="T243">думать-PST2-3PL</ta>
            <ta e="T245" id="Seg_4074" s="T244">эвенк.[NOM]</ta>
            <ta e="T246" id="Seg_4075" s="T245">один</ta>
            <ta e="T247" id="Seg_4076" s="T246">человек-ACC</ta>
            <ta e="T248" id="Seg_4077" s="T247">обнимать-PST2.[3SG]</ta>
            <ta e="T249" id="Seg_4078" s="T248">да</ta>
            <ta e="T250" id="Seg_4079" s="T249">плясать-CVB.SIM</ta>
            <ta e="T251" id="Seg_4080" s="T250">идти-EP-PST2-3PL</ta>
            <ta e="T252" id="Seg_4081" s="T251">тот</ta>
            <ta e="T253" id="Seg_4082" s="T252">идти-CVB.SEQ</ta>
            <ta e="T254" id="Seg_4083" s="T253">спрашивать-PST2.[3SG]</ta>
            <ta e="T255" id="Seg_4084" s="T254">где</ta>
            <ta e="T256" id="Seg_4085" s="T255">есть=Q</ta>
            <ta e="T257" id="Seg_4086" s="T256">большой-3PL-GEN</ta>
            <ta e="T258" id="Seg_4087" s="T257">чум-3SG.[NOM]</ta>
            <ta e="T259" id="Seg_4088" s="T258">там</ta>
            <ta e="T260" id="Seg_4089" s="T259">Нганасан.[NOM]</ta>
            <ta e="T261" id="Seg_4090" s="T260">жена-3SG.[NOM]</ta>
            <ta e="T262" id="Seg_4091" s="T261">есть</ta>
            <ta e="T263" id="Seg_4092" s="T262">этот</ta>
            <ta e="T264" id="Seg_4093" s="T263">человек.[NOM]</ta>
            <ta e="T265" id="Seg_4094" s="T264">сердиться-CVB.SIM-сердиться-CVB.SIM</ta>
            <ta e="T266" id="Seg_4095" s="T265">говорить-PST2.[3SG]</ta>
            <ta e="T267" id="Seg_4096" s="T266">показывать-PST2.[3SG]</ta>
            <ta e="T268" id="Seg_4097" s="T267">чум-3SG-ACC</ta>
            <ta e="T269" id="Seg_4098" s="T268">эвенк.[NOM]</ta>
            <ta e="T270" id="Seg_4099" s="T269">входить-PST2-3SG</ta>
            <ta e="T271" id="Seg_4100" s="T270">жена.[NOM]</ta>
            <ta e="T272" id="Seg_4101" s="T271">глаз-3SG.[NOM]</ta>
            <ta e="T273" id="Seg_4102" s="T272">плакать-CVB.SEQ</ta>
            <ta e="T274" id="Seg_4103" s="T273">ковш.[NOM]</ta>
            <ta e="T275" id="Seg_4104" s="T274">ростом.с</ta>
            <ta e="T276" id="Seg_4105" s="T275">становиться-PST2.[3SG]</ta>
            <ta e="T277" id="Seg_4106" s="T276">эвенк.[NOM]</ta>
            <ta e="T278" id="Seg_4107" s="T277">входить-PST2.[3SG]</ta>
            <ta e="T279" id="Seg_4108" s="T278">да</ta>
            <ta e="T280" id="Seg_4109" s="T279">большой-3PL-ACC</ta>
            <ta e="T281" id="Seg_4110" s="T280">к</ta>
            <ta e="T282" id="Seg_4111" s="T281">шагать-PST2.[3SG]</ta>
            <ta e="T283" id="Seg_4112" s="T282">1SG.[NOM]</ta>
            <ta e="T284" id="Seg_4113" s="T283">жена-EP-1SG.[NOM]</ta>
            <ta e="T285" id="Seg_4114" s="T284">младшая.сестра-EP-3SG-ACC</ta>
            <ta e="T286" id="Seg_4115" s="T285">жена.[NOM]</ta>
            <ta e="T287" id="Seg_4116" s="T286">делать-CVB.SEQ</ta>
            <ta e="T288" id="Seg_4117" s="T287">сидеть-PRS-2SG</ta>
            <ta e="T289" id="Seg_4118" s="T288">1SG.[NOM]</ta>
            <ta e="T290" id="Seg_4119" s="T289">жена-EP-1SG.[NOM]</ta>
            <ta e="T291" id="Seg_4120" s="T290">младшая.сестра-EP-3SG-ACC</ta>
            <ta e="T292" id="Seg_4121" s="T291">встречать-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T293" id="Seg_4122" s="T292">хотеть-PRS.[3SG]</ta>
            <ta e="T294" id="Seg_4123" s="T293">вот</ta>
            <ta e="T295" id="Seg_4124" s="T294">ладно</ta>
            <ta e="T296" id="Seg_4125" s="T295">завтра</ta>
            <ta e="T297" id="Seg_4126" s="T296">идти-FUT-1PL</ta>
            <ta e="T298" id="Seg_4127" s="T297">видеть-CVB.SIM</ta>
            <ta e="T299" id="Seg_4128" s="T298">говорить-PST2.[3SG]</ta>
            <ta e="T300" id="Seg_4129" s="T299">Чангит.[NOM]</ta>
            <ta e="T301" id="Seg_4130" s="T300">следующее.утро-3SG-ACC</ta>
            <ta e="T302" id="Seg_4131" s="T301">этот</ta>
            <ta e="T303" id="Seg_4132" s="T302">жена-ACC</ta>
            <ta e="T304" id="Seg_4133" s="T303">Чангит.[NOM]</ta>
            <ta e="T305" id="Seg_4134" s="T304">приносить-CVB.SEQ</ta>
            <ta e="T306" id="Seg_4135" s="T305">приходить-PST2.[3SG]</ta>
            <ta e="T307" id="Seg_4136" s="T306">чум-DAT/LOC</ta>
            <ta e="T308" id="Seg_4137" s="T307">входить-PTCP.PST-3PL-ACC</ta>
            <ta e="T309" id="Seg_4138" s="T308">после.того</ta>
            <ta e="T310" id="Seg_4139" s="T309">Нганасан.[NOM]</ta>
            <ta e="T311" id="Seg_4140" s="T310">Чангит.[NOM]</ta>
            <ta e="T312" id="Seg_4141" s="T311">олень-PL-3SG-GEN</ta>
            <ta e="T313" id="Seg_4142" s="T312">веревка-3SG-ACC</ta>
            <ta e="T314" id="Seg_4143" s="T313">резать-CVB.SIM</ta>
            <ta e="T315" id="Seg_4144" s="T314">перевешивать-PST2.[3SG]</ta>
            <ta e="T316" id="Seg_4145" s="T315">жена-3SG-ACC</ta>
            <ta e="T317" id="Seg_4146" s="T316">скакать-CVB.SEQ</ta>
            <ta e="T318" id="Seg_4147" s="T317">после</ta>
            <ta e="T319" id="Seg_4148" s="T318">спасаться-PST2.[3SG]</ta>
            <ta e="T320" id="Seg_4149" s="T319">Чангит.[NOM]</ta>
            <ta e="T321" id="Seg_4150" s="T320">задняя.часть-3SG-ABL</ta>
            <ta e="T322" id="Seg_4151" s="T321">пешком</ta>
            <ta e="T323" id="Seg_4152" s="T322">следовать-PST2.[3SG]</ta>
            <ta e="T324" id="Seg_4153" s="T323">Нганасан.[NOM]</ta>
            <ta e="T325" id="Seg_4154" s="T324">мышца-3SG-ACC</ta>
            <ta e="T326" id="Seg_4155" s="T325">разрывать-CVB.SIM</ta>
            <ta e="T327" id="Seg_4156" s="T326">стрелять-CAUS-PST2.[3SG]</ta>
            <ta e="T328" id="Seg_4157" s="T327">останавливаться-CVB.SIM</ta>
            <ta e="T329" id="Seg_4158" s="T328">падать-CVB.SEQ</ta>
            <ta e="T330" id="Seg_4159" s="T329">после</ta>
            <ta e="T331" id="Seg_4160" s="T330">Нганасан.[NOM]</ta>
            <ta e="T332" id="Seg_4161" s="T331">навстречу</ta>
            <ta e="T333" id="Seg_4162" s="T332">идти-PST2.[3SG]</ta>
            <ta e="T334" id="Seg_4163" s="T333">Чангит.[NOM]</ta>
            <ta e="T335" id="Seg_4164" s="T334">ухо-3SG-GEN</ta>
            <ta e="T336" id="Seg_4165" s="T335">край-3SG-INSTR</ta>
            <ta e="T337" id="Seg_4166" s="T336">колоть-CVB.SIM</ta>
            <ta e="T338" id="Seg_4167" s="T337">бросать-PST2.[3SG]</ta>
            <ta e="T339" id="Seg_4168" s="T338">Чангит.[NOM]</ta>
            <ta e="T340" id="Seg_4169" s="T339">стрела-3SG-ACC</ta>
            <ta e="T341" id="Seg_4170" s="T340">каждый-3SG-ACC</ta>
            <ta e="T342" id="Seg_4171" s="T341">стрелять-PST2.[3SG]</ta>
            <ta e="T343" id="Seg_4172" s="T342">да</ta>
            <ta e="T344" id="Seg_4173" s="T343">спасаться-PST2.[3SG]</ta>
            <ta e="T345" id="Seg_4174" s="T344">эвенк-ACC</ta>
            <ta e="T346" id="Seg_4175" s="T345">с</ta>
            <ta e="T347" id="Seg_4176" s="T346">Нганасан.[NOM]</ta>
            <ta e="T348" id="Seg_4177" s="T347">следовать-PST2-3PL</ta>
            <ta e="T349" id="Seg_4178" s="T348">догонять-CVB.SIM</ta>
            <ta e="T350" id="Seg_4179" s="T349">следовать-ITER-CVB.SEQ</ta>
            <ta e="T351" id="Seg_4180" s="T350">после</ta>
            <ta e="T352" id="Seg_4181" s="T351">голова-3SG-ACC</ta>
            <ta e="T353" id="Seg_4182" s="T352">колоть-CVB.SIM</ta>
            <ta e="T354" id="Seg_4183" s="T353">бить-EP-PST2-3PL</ta>
            <ta e="T355" id="Seg_4184" s="T354">тот.[NOM]</ta>
            <ta e="T356" id="Seg_4185" s="T355">задняя.часть-3SG-ABL</ta>
            <ta e="T357" id="Seg_4186" s="T356">эвенк.[NOM]</ta>
            <ta e="T358" id="Seg_4187" s="T357">жена-VBZ-PST2.[3SG]</ta>
            <ta e="T359" id="Seg_4188" s="T358">Чангит.[NOM]</ta>
            <ta e="T360" id="Seg_4189" s="T359">олень-PL-3SG-INSTR</ta>
            <ta e="T361" id="Seg_4190" s="T360">быть.богатым-CVB.SEQ-наесться-CVB.SEQ</ta>
            <ta e="T362" id="Seg_4191" s="T361">жить-PST2-3PL</ta>
            <ta e="T363" id="Seg_4192" s="T362">этот.[NOM]</ta>
            <ta e="T364" id="Seg_4193" s="T363">подобно</ta>
            <ta e="T365" id="Seg_4194" s="T364">Нганасан-PL-ACC</ta>
            <ta e="T366" id="Seg_4195" s="T365">с</ta>
            <ta e="T367" id="Seg_4196" s="T366">эвенк-PL.[NOM]</ta>
            <ta e="T368" id="Seg_4197" s="T367">связывать-EP-RECP/COLL-CVB.SEQ</ta>
            <ta e="T369" id="Seg_4198" s="T368">жить-PST2-3PL</ta>
            <ta e="T370" id="Seg_4199" s="T369">Нганасан-PL.[NOM]</ta>
            <ta e="T371" id="Seg_4200" s="T370">тот.[NOM]</ta>
            <ta e="T372" id="Seg_4201" s="T371">задняя.часть-3SG-ABL</ta>
            <ta e="T373" id="Seg_4202" s="T372">бояться-CVB.SEQ</ta>
            <ta e="T374" id="Seg_4203" s="T373">Камень.[NOM]</ta>
            <ta e="T375" id="Seg_4204" s="T374">к</ta>
            <ta e="T376" id="Seg_4205" s="T375">кочевать-NEG.PTCP</ta>
            <ta e="T377" id="Seg_4206" s="T376">становиться-PST-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_4207" s="T0">n-n:(num)-n:case</ta>
            <ta e="T2" id="Seg_4208" s="T1">adv-adv</ta>
            <ta e="T3" id="Seg_4209" s="T2">propr-n:case</ta>
            <ta e="T4" id="Seg_4210" s="T3">v-v:ptcp</ta>
            <ta e="T5" id="Seg_4211" s="T4">v-v:tense-v:poss.pn</ta>
            <ta e="T6" id="Seg_4212" s="T5">propr-n:case</ta>
            <ta e="T7" id="Seg_4213" s="T6">n-n:(num)-n:case</ta>
            <ta e="T8" id="Seg_4214" s="T7">propr-n:case</ta>
            <ta e="T9" id="Seg_4215" s="T8">n-n:(num)-n:case</ta>
            <ta e="T10" id="Seg_4216" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_4217" s="T10">adv</ta>
            <ta e="T12" id="Seg_4218" s="T11">cardnum</ta>
            <ta e="T13" id="Seg_4219" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_4220" s="T13">n-n:poss-n:case</ta>
            <ta e="T15" id="Seg_4221" s="T14">n-n:(num)-n:case</ta>
            <ta e="T16" id="Seg_4222" s="T15">v-v:tense-v:pred.pn</ta>
            <ta e="T17" id="Seg_4223" s="T16">dempro</ta>
            <ta e="T18" id="Seg_4224" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_4225" s="T18">n-n:(poss)-n:case</ta>
            <ta e="T20" id="Seg_4226" s="T19">n-n&gt;v-v:cvb</ta>
            <ta e="T21" id="Seg_4227" s="T20">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T22" id="Seg_4228" s="T21">post</ta>
            <ta e="T23" id="Seg_4229" s="T22">v-v:cvb</ta>
            <ta e="T24" id="Seg_4230" s="T23">v-v:tense-v:pred.pn</ta>
            <ta e="T25" id="Seg_4231" s="T24">dempro</ta>
            <ta e="T26" id="Seg_4232" s="T25">v-v:mood-v:temp.pn</ta>
            <ta e="T27" id="Seg_4233" s="T26">n-n:(num)-n:case</ta>
            <ta e="T28" id="Seg_4234" s="T27">v-v:tense-v:pred.pn</ta>
            <ta e="T29" id="Seg_4235" s="T28">n-n:poss-n:case</ta>
            <ta e="T30" id="Seg_4236" s="T29">n-n:(num)-n:case</ta>
            <ta e="T31" id="Seg_4237" s="T30">v-v:ptcp</ta>
            <ta e="T32" id="Seg_4238" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_4239" s="T32">v-v:tense-v:poss.pn</ta>
            <ta e="T34" id="Seg_4240" s="T33">v-v:cvb</ta>
            <ta e="T35" id="Seg_4241" s="T34">v-v:tense-v:pred.pn</ta>
            <ta e="T36" id="Seg_4242" s="T35">n-n:poss-n:case</ta>
            <ta e="T37" id="Seg_4243" s="T36">v-v:cvb</ta>
            <ta e="T38" id="Seg_4244" s="T37">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T39" id="Seg_4245" s="T38">post</ta>
            <ta e="T40" id="Seg_4246" s="T39">cardnum</ta>
            <ta e="T41" id="Seg_4247" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_4248" s="T41">v-v:tense-v:pred.pn</ta>
            <ta e="T43" id="Seg_4249" s="T42">conj</ta>
            <ta e="T44" id="Seg_4250" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_4251" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_4252" s="T45">v-v:cvb</ta>
            <ta e="T47" id="Seg_4253" s="T46">post</ta>
            <ta e="T48" id="Seg_4254" s="T47">v-v:cvb</ta>
            <ta e="T49" id="Seg_4255" s="T48">v-v:tense-v:pred.pn</ta>
            <ta e="T50" id="Seg_4256" s="T49">adv</ta>
            <ta e="T51" id="Seg_4257" s="T50">n-n:case</ta>
            <ta e="T52" id="Seg_4258" s="T51">v-v:tense-v:poss.pn</ta>
            <ta e="T53" id="Seg_4259" s="T52">n-n:(poss)-n:case</ta>
            <ta e="T54" id="Seg_4260" s="T53">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T55" id="Seg_4261" s="T54">v-v:tense-v:pred.pn</ta>
            <ta e="T56" id="Seg_4262" s="T55">n-n:(poss)-n:case</ta>
            <ta e="T57" id="Seg_4263" s="T56">adv</ta>
            <ta e="T58" id="Seg_4264" s="T57">v-v:tense-v:pred.pn</ta>
            <ta e="T59" id="Seg_4265" s="T58">que-pro:case</ta>
            <ta e="T60" id="Seg_4266" s="T59">ptcl</ta>
            <ta e="T61" id="Seg_4267" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_4268" s="T61">adj&gt;adj-adj-n:case</ta>
            <ta e="T63" id="Seg_4269" s="T62">n-n:case</ta>
            <ta e="T64" id="Seg_4270" s="T63">ptcl</ta>
            <ta e="T65" id="Seg_4271" s="T64">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T66" id="Seg_4272" s="T65">que</ta>
            <ta e="T67" id="Seg_4273" s="T66">v-v:tense-v:poss.pn</ta>
            <ta e="T68" id="Seg_4274" s="T67">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T69" id="Seg_4275" s="T68">v-v:tense-v:pred.pn</ta>
            <ta e="T70" id="Seg_4276" s="T69">adv</ta>
            <ta e="T71" id="Seg_4277" s="T70">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T72" id="Seg_4278" s="T71">post</ta>
            <ta e="T73" id="Seg_4279" s="T72">n-n:poss-n:case</ta>
            <ta e="T74" id="Seg_4280" s="T73">v-v:cvb</ta>
            <ta e="T75" id="Seg_4281" s="T74">v-v:tense-v:pred.pn</ta>
            <ta e="T76" id="Seg_4282" s="T75">n-n:(poss)-n:case</ta>
            <ta e="T77" id="Seg_4283" s="T76">adj-n:(poss)-n:case</ta>
            <ta e="T78" id="Seg_4284" s="T77">ptcl</ta>
            <ta e="T79" id="Seg_4285" s="T78">v-v:tense-v:pred.pn</ta>
            <ta e="T80" id="Seg_4286" s="T79">pers-pro:case</ta>
            <ta e="T81" id="Seg_4287" s="T80">n-n:poss-n:case</ta>
            <ta e="T82" id="Seg_4288" s="T81">v-v:tense-v:poss.pn</ta>
            <ta e="T83" id="Seg_4289" s="T82">que-pro:case</ta>
            <ta e="T84" id="Seg_4290" s="T83">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T85" id="Seg_4291" s="T84">adv</ta>
            <ta e="T86" id="Seg_4292" s="T85">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T87" id="Seg_4293" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_4294" s="T87">v-v:cvb</ta>
            <ta e="T89" id="Seg_4295" s="T88">v-v:tense-v:pred.pn</ta>
            <ta e="T90" id="Seg_4296" s="T89">ptcl</ta>
            <ta e="T91" id="Seg_4297" s="T90">adj</ta>
            <ta e="T92" id="Seg_4298" s="T91">n-n:case</ta>
            <ta e="T93" id="Seg_4299" s="T92">adv</ta>
            <ta e="T94" id="Seg_4300" s="T93">v-v&gt;v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T95" id="Seg_4301" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_4302" s="T95">v-v:cvb</ta>
            <ta e="T97" id="Seg_4303" s="T96">v-v:tense-v:poss.pn</ta>
            <ta e="T98" id="Seg_4304" s="T97">n-n:(poss)-n:case</ta>
            <ta e="T99" id="Seg_4305" s="T98">n-n&gt;v-v:cvb</ta>
            <ta e="T100" id="Seg_4306" s="T99">v-v:ptcp</ta>
            <ta e="T101" id="Seg_4307" s="T100">v-v:tense-v:pred.pn</ta>
            <ta e="T102" id="Seg_4308" s="T101">n-n:(poss)-n:case</ta>
            <ta e="T103" id="Seg_4309" s="T102">ptcl</ta>
            <ta e="T104" id="Seg_4310" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_4311" s="T104">n-n:case</ta>
            <ta e="T106" id="Seg_4312" s="T105">adv&gt;adv-adv</ta>
            <ta e="T107" id="Seg_4313" s="T106">v-v:ptcp</ta>
            <ta e="T108" id="Seg_4314" s="T107">v-v:tense-v:pred.pn</ta>
            <ta e="T109" id="Seg_4315" s="T108">adv</ta>
            <ta e="T110" id="Seg_4316" s="T109">v-v:tense-v:pred.pn</ta>
            <ta e="T111" id="Seg_4317" s="T110">n-n:case</ta>
            <ta e="T112" id="Seg_4318" s="T111">n-n&gt;adj</ta>
            <ta e="T113" id="Seg_4319" s="T112">v-v:tense-v:pred.pn</ta>
            <ta e="T114" id="Seg_4320" s="T113">n-n:(poss)-n:case</ta>
            <ta e="T115" id="Seg_4321" s="T114">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T116" id="Seg_4322" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_4323" s="T116">que-pro:case</ta>
            <ta e="T118" id="Seg_4324" s="T117">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T119" id="Seg_4325" s="T118">adv</ta>
            <ta e="T120" id="Seg_4326" s="T119">pers-pro:case</ta>
            <ta e="T121" id="Seg_4327" s="T120">cardnum</ta>
            <ta e="T122" id="Seg_4328" s="T121">n-n:case</ta>
            <ta e="T123" id="Seg_4329" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_4330" s="T123">v-v:tense-v:poss.pn</ta>
            <ta e="T125" id="Seg_4331" s="T124">n-n:(poss)-n:case</ta>
            <ta e="T126" id="Seg_4332" s="T125">adv&gt;adv-adv-n:(poss)</ta>
            <ta e="T127" id="Seg_4333" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_4334" s="T127">v-v:tense-v:poss.pn</ta>
            <ta e="T129" id="Seg_4335" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_4336" s="T129">n-n:(num)-n:case</ta>
            <ta e="T131" id="Seg_4337" s="T130">adv</ta>
            <ta e="T132" id="Seg_4338" s="T131">pers-pro:case</ta>
            <ta e="T133" id="Seg_4339" s="T132">que-pro:(poss)-pro:case</ta>
            <ta e="T134" id="Seg_4340" s="T133">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T135" id="Seg_4341" s="T134">adj</ta>
            <ta e="T136" id="Seg_4342" s="T135">ptcl</ta>
            <ta e="T137" id="Seg_4343" s="T136">n-n:(num)-n:case</ta>
            <ta e="T138" id="Seg_4344" s="T137">ptcl</ta>
            <ta e="T139" id="Seg_4345" s="T138">v-v:tense-v:pred.pn</ta>
            <ta e="T140" id="Seg_4346" s="T139">que</ta>
            <ta e="T141" id="Seg_4347" s="T140">pers-pro:case</ta>
            <ta e="T142" id="Seg_4348" s="T141">n-n:(num)-n:case</ta>
            <ta e="T143" id="Seg_4349" s="T142">v-v:cvb</ta>
            <ta e="T144" id="Seg_4350" s="T143">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T145" id="Seg_4351" s="T144">n-n:case</ta>
            <ta e="T146" id="Seg_4352" s="T145">v-v:tense-v:pred.pn</ta>
            <ta e="T147" id="Seg_4353" s="T146">pers-pro:case</ta>
            <ta e="T148" id="Seg_4354" s="T147">que</ta>
            <ta e="T149" id="Seg_4355" s="T148">adj-n:(pred.pn)-ptcl</ta>
            <ta e="T150" id="Seg_4356" s="T149">n-n:case</ta>
            <ta e="T151" id="Seg_4357" s="T150">n-n:poss-n:case</ta>
            <ta e="T152" id="Seg_4358" s="T151">v-v:tense-v:pred.pn</ta>
            <ta e="T153" id="Seg_4359" s="T152">adj-n:case</ta>
            <ta e="T154" id="Seg_4360" s="T153">v-v:cvb-v:pred.pn</ta>
            <ta e="T155" id="Seg_4361" s="T154">v-v:ptcp</ta>
            <ta e="T156" id="Seg_4362" s="T155">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T157" id="Seg_4363" s="T156">v-v:tense-v:pred.pn</ta>
            <ta e="T158" id="Seg_4364" s="T157">n-n:(poss)-n:case</ta>
            <ta e="T159" id="Seg_4365" s="T158">adv</ta>
            <ta e="T160" id="Seg_4366" s="T159">pers-pro:case</ta>
            <ta e="T161" id="Seg_4367" s="T160">pers-pro:case</ta>
            <ta e="T162" id="Seg_4368" s="T161">v-v:tense-v:poss.pn</ta>
            <ta e="T163" id="Seg_4369" s="T162">que</ta>
            <ta e="T164" id="Seg_4370" s="T163">adj-n:(pred.pn)-ptcl</ta>
            <ta e="T165" id="Seg_4371" s="T164">pers-pro:case</ta>
            <ta e="T166" id="Seg_4372" s="T165">pers-pro:case</ta>
            <ta e="T167" id="Seg_4373" s="T166">cardnum</ta>
            <ta e="T168" id="Seg_4374" s="T167">n-n:case</ta>
            <ta e="T169" id="Seg_4375" s="T168">n-n:(poss)-n:case</ta>
            <ta e="T170" id="Seg_4376" s="T169">post-n:poss-n:case</ta>
            <ta e="T171" id="Seg_4377" s="T170">v-v:tense-v:poss.pn</ta>
            <ta e="T172" id="Seg_4378" s="T171">n-n:case</ta>
            <ta e="T173" id="Seg_4379" s="T172">cardnum-cardnum&gt;adv</ta>
            <ta e="T174" id="Seg_4380" s="T173">v-v:tense-v:pred.pn</ta>
            <ta e="T175" id="Seg_4381" s="T174">n-n:poss-n:case</ta>
            <ta e="T176" id="Seg_4382" s="T175">ptcl</ta>
            <ta e="T177" id="Seg_4383" s="T176">n-n:poss-n:case</ta>
            <ta e="T178" id="Seg_4384" s="T177">v-v:cvb</ta>
            <ta e="T179" id="Seg_4385" s="T178">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T180" id="Seg_4386" s="T179">adv</ta>
            <ta e="T181" id="Seg_4387" s="T180">pers-pro:case</ta>
            <ta e="T182" id="Seg_4388" s="T181">v-v:mood.pn</ta>
            <ta e="T183" id="Seg_4389" s="T182">v-v:tense-v:pred.pn</ta>
            <ta e="T184" id="Seg_4390" s="T183">n-n:case</ta>
            <ta e="T185" id="Seg_4391" s="T184">v-v:tense-v:pred.pn</ta>
            <ta e="T186" id="Seg_4392" s="T185">v-v:neg-v:pred.pn</ta>
            <ta e="T187" id="Seg_4393" s="T186">ptcl</ta>
            <ta e="T188" id="Seg_4394" s="T187">adj-n:case</ta>
            <ta e="T189" id="Seg_4395" s="T188">adj-n:(pred.pn)</ta>
            <ta e="T190" id="Seg_4396" s="T189">v-v:tense-v:pred.pn</ta>
            <ta e="T191" id="Seg_4397" s="T190">n-n:(poss)-n:case</ta>
            <ta e="T192" id="Seg_4398" s="T191">v-v:tense-v:pred.pn</ta>
            <ta e="T193" id="Seg_4399" s="T192">n-n:poss-n:case</ta>
            <ta e="T194" id="Seg_4400" s="T193">v-v:tense-v:pred.pn</ta>
            <ta e="T195" id="Seg_4401" s="T194">cardnum</ta>
            <ta e="T196" id="Seg_4402" s="T195">n-n:case</ta>
            <ta e="T197" id="Seg_4403" s="T196">v-v:tense-v:pred.pn</ta>
            <ta e="T198" id="Seg_4404" s="T197">ptcl</ta>
            <ta e="T199" id="Seg_4405" s="T198">pers-pro:case</ta>
            <ta e="T200" id="Seg_4406" s="T199">n-n:poss-n:case</ta>
            <ta e="T201" id="Seg_4407" s="T200">adj-adj&gt;adv</ta>
            <ta e="T202" id="Seg_4408" s="T201">v-v&gt;v-v:cvb</ta>
            <ta e="T203" id="Seg_4409" s="T202">post</ta>
            <ta e="T204" id="Seg_4410" s="T203">v-v:tense-v:poss.pn</ta>
            <ta e="T205" id="Seg_4411" s="T204">pers-pro:case</ta>
            <ta e="T206" id="Seg_4412" s="T205">pers-pro:case</ta>
            <ta e="T207" id="Seg_4413" s="T206">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T208" id="Seg_4414" s="T207">v-v:tense-v:poss.pn</ta>
            <ta e="T209" id="Seg_4415" s="T208">dempro</ta>
            <ta e="T210" id="Seg_4416" s="T209">n-n:case</ta>
            <ta e="T211" id="Seg_4417" s="T210">pers-pro:case</ta>
            <ta e="T212" id="Seg_4418" s="T211">v-v:ptcp</ta>
            <ta e="T213" id="Seg_4419" s="T212">n-n:(poss)-n:case</ta>
            <ta e="T214" id="Seg_4420" s="T213">n-n:(poss)-n:case</ta>
            <ta e="T215" id="Seg_4421" s="T214">dempro</ta>
            <ta e="T216" id="Seg_4422" s="T215">n-n:case</ta>
            <ta e="T217" id="Seg_4423" s="T216">pers-pro:case</ta>
            <ta e="T218" id="Seg_4424" s="T217">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T219" id="Seg_4425" s="T218">pers-pro:case</ta>
            <ta e="T220" id="Seg_4426" s="T219">ptcl</ta>
            <ta e="T221" id="Seg_4427" s="T220">pers-pro:case</ta>
            <ta e="T222" id="Seg_4428" s="T221">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T223" id="Seg_4429" s="T222">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T224" id="Seg_4430" s="T223">v-v:tense-v:poss.pn</ta>
            <ta e="T225" id="Seg_4431" s="T224">n-n:case</ta>
            <ta e="T226" id="Seg_4432" s="T225">n-n:poss-n:case</ta>
            <ta e="T227" id="Seg_4433" s="T226">post</ta>
            <ta e="T228" id="Seg_4434" s="T227">v-v:cvb</ta>
            <ta e="T229" id="Seg_4435" s="T228">v-v:tense-v:pred.pn</ta>
            <ta e="T230" id="Seg_4436" s="T229">v-v:tense-v:poss.pn</ta>
            <ta e="T231" id="Seg_4437" s="T230">cardnum</ta>
            <ta e="T232" id="Seg_4438" s="T231">n-n:case</ta>
            <ta e="T233" id="Seg_4439" s="T232">n-n:case</ta>
            <ta e="T234" id="Seg_4440" s="T233">v-v:tense-v:pred.pn</ta>
            <ta e="T235" id="Seg_4441" s="T234">dempro-pro:case</ta>
            <ta e="T236" id="Seg_4442" s="T235">post</ta>
            <ta e="T237" id="Seg_4443" s="T236">n-n:(num)-n:case</ta>
            <ta e="T238" id="Seg_4444" s="T237">v-v:cvb</ta>
            <ta e="T239" id="Seg_4445" s="T238">v-v:tense-v:pred.pn</ta>
            <ta e="T240" id="Seg_4446" s="T239">ptcl</ta>
            <ta e="T241" id="Seg_4447" s="T240">n-n:case</ta>
            <ta e="T242" id="Seg_4448" s="T241">adv</ta>
            <ta e="T243" id="Seg_4449" s="T242">v-v:cvb</ta>
            <ta e="T244" id="Seg_4450" s="T243">v-v:tense-v:pred.pn</ta>
            <ta e="T245" id="Seg_4451" s="T244">n-n:case</ta>
            <ta e="T246" id="Seg_4452" s="T245">cardnum</ta>
            <ta e="T247" id="Seg_4453" s="T246">n-n:case</ta>
            <ta e="T248" id="Seg_4454" s="T247">v-v:tense-v:pred.pn</ta>
            <ta e="T249" id="Seg_4455" s="T248">conj</ta>
            <ta e="T250" id="Seg_4456" s="T249">v-v:cvb</ta>
            <ta e="T251" id="Seg_4457" s="T250">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T252" id="Seg_4458" s="T251">dempro</ta>
            <ta e="T253" id="Seg_4459" s="T252">v-v:cvb</ta>
            <ta e="T254" id="Seg_4460" s="T253">v-v:tense-v:pred.pn</ta>
            <ta e="T255" id="Seg_4461" s="T254">que</ta>
            <ta e="T256" id="Seg_4462" s="T255">ptcl-ptcl</ta>
            <ta e="T257" id="Seg_4463" s="T256">adj-n:poss-n:case</ta>
            <ta e="T258" id="Seg_4464" s="T257">n-n:(poss)-n:case</ta>
            <ta e="T259" id="Seg_4465" s="T258">adv</ta>
            <ta e="T260" id="Seg_4466" s="T259">n-n:case</ta>
            <ta e="T261" id="Seg_4467" s="T260">n-n:(poss)-n:case</ta>
            <ta e="T262" id="Seg_4468" s="T261">ptcl</ta>
            <ta e="T263" id="Seg_4469" s="T262">dempro</ta>
            <ta e="T264" id="Seg_4470" s="T263">n-n:case</ta>
            <ta e="T265" id="Seg_4471" s="T264">v-v:cvb-v-v:cvb</ta>
            <ta e="T266" id="Seg_4472" s="T265">v-v:tense-v:pred.pn</ta>
            <ta e="T267" id="Seg_4473" s="T266">v-v:tense-v:pred.pn</ta>
            <ta e="T268" id="Seg_4474" s="T267">n-n:poss-n:case</ta>
            <ta e="T269" id="Seg_4475" s="T268">n-n:case</ta>
            <ta e="T270" id="Seg_4476" s="T269">v-v:tense-v:poss.pn</ta>
            <ta e="T271" id="Seg_4477" s="T270">n-n:case</ta>
            <ta e="T272" id="Seg_4478" s="T271">n-n:(poss)-n:case</ta>
            <ta e="T273" id="Seg_4479" s="T272">v-v:cvb</ta>
            <ta e="T274" id="Seg_4480" s="T273">n-n:case</ta>
            <ta e="T275" id="Seg_4481" s="T274">post</ta>
            <ta e="T276" id="Seg_4482" s="T275">v-v:tense-v:pred.pn</ta>
            <ta e="T277" id="Seg_4483" s="T276">n-n:case</ta>
            <ta e="T278" id="Seg_4484" s="T277">v-v:tense-v:pred.pn</ta>
            <ta e="T279" id="Seg_4485" s="T278">conj</ta>
            <ta e="T280" id="Seg_4486" s="T279">adj-n:poss-n:case</ta>
            <ta e="T281" id="Seg_4487" s="T280">post</ta>
            <ta e="T282" id="Seg_4488" s="T281">v-v:tense-v:pred.pn</ta>
            <ta e="T283" id="Seg_4489" s="T282">pers-pro:case</ta>
            <ta e="T284" id="Seg_4490" s="T283">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T285" id="Seg_4491" s="T284">n-n:(ins)-n:poss-n:case</ta>
            <ta e="T286" id="Seg_4492" s="T285">n-n:case</ta>
            <ta e="T287" id="Seg_4493" s="T286">v-v:cvb</ta>
            <ta e="T288" id="Seg_4494" s="T287">v-v:tense-v:pred.pn</ta>
            <ta e="T289" id="Seg_4495" s="T288">pers-pro:case</ta>
            <ta e="T290" id="Seg_4496" s="T289">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T291" id="Seg_4497" s="T290">n-n:(ins)-n:poss-n:case</ta>
            <ta e="T292" id="Seg_4498" s="T291">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T293" id="Seg_4499" s="T292">v-v:tense-v:pred.pn</ta>
            <ta e="T294" id="Seg_4500" s="T293">ptcl</ta>
            <ta e="T295" id="Seg_4501" s="T294">ptcl</ta>
            <ta e="T296" id="Seg_4502" s="T295">adv</ta>
            <ta e="T297" id="Seg_4503" s="T296">v-v:tense-v:poss.pn</ta>
            <ta e="T298" id="Seg_4504" s="T297">v-v:cvb</ta>
            <ta e="T299" id="Seg_4505" s="T298">v-v:tense-v:pred.pn</ta>
            <ta e="T300" id="Seg_4506" s="T299">n-n:case</ta>
            <ta e="T301" id="Seg_4507" s="T300">n-n:poss-n:case</ta>
            <ta e="T302" id="Seg_4508" s="T301">dempro</ta>
            <ta e="T303" id="Seg_4509" s="T302">n-n:case</ta>
            <ta e="T304" id="Seg_4510" s="T303">n-n:case</ta>
            <ta e="T305" id="Seg_4511" s="T304">v-v:cvb</ta>
            <ta e="T306" id="Seg_4512" s="T305">v-v:tense-v:pred.pn</ta>
            <ta e="T307" id="Seg_4513" s="T306">n-n:case</ta>
            <ta e="T308" id="Seg_4514" s="T307">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T309" id="Seg_4515" s="T308">post</ta>
            <ta e="T310" id="Seg_4516" s="T309">n-n:case</ta>
            <ta e="T311" id="Seg_4517" s="T310">n-n:case</ta>
            <ta e="T312" id="Seg_4518" s="T311">n-n:(num)-n:poss-n:case</ta>
            <ta e="T313" id="Seg_4519" s="T312">n-n:poss-n:case</ta>
            <ta e="T314" id="Seg_4520" s="T313">v-v:cvb</ta>
            <ta e="T315" id="Seg_4521" s="T314">v-v:tense-v:pred.pn</ta>
            <ta e="T316" id="Seg_4522" s="T315">n-n:poss-n:case</ta>
            <ta e="T317" id="Seg_4523" s="T316">v-v:cvb</ta>
            <ta e="T318" id="Seg_4524" s="T317">post</ta>
            <ta e="T319" id="Seg_4525" s="T318">v-v:tense-v:pred.pn</ta>
            <ta e="T320" id="Seg_4526" s="T319">n-n:case</ta>
            <ta e="T321" id="Seg_4527" s="T320">n-n:poss-n:case</ta>
            <ta e="T322" id="Seg_4528" s="T321">adv</ta>
            <ta e="T323" id="Seg_4529" s="T322">v-v:tense-v:pred.pn</ta>
            <ta e="T324" id="Seg_4530" s="T323">n-n:case</ta>
            <ta e="T325" id="Seg_4531" s="T324">n-n:poss-n:case</ta>
            <ta e="T326" id="Seg_4532" s="T325">v-v:cvb</ta>
            <ta e="T327" id="Seg_4533" s="T326">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T328" id="Seg_4534" s="T327">v-v:cvb</ta>
            <ta e="T329" id="Seg_4535" s="T328">v-v:cvb</ta>
            <ta e="T330" id="Seg_4536" s="T329">post</ta>
            <ta e="T331" id="Seg_4537" s="T330">n-n:case</ta>
            <ta e="T332" id="Seg_4538" s="T331">post</ta>
            <ta e="T333" id="Seg_4539" s="T332">v-v:tense-v:pred.pn</ta>
            <ta e="T334" id="Seg_4540" s="T333">n-n:case</ta>
            <ta e="T335" id="Seg_4541" s="T334">n-n:poss-n:case</ta>
            <ta e="T336" id="Seg_4542" s="T335">n-n:poss-n:case</ta>
            <ta e="T337" id="Seg_4543" s="T336">v-v:cvb</ta>
            <ta e="T338" id="Seg_4544" s="T337">v-v:tense-v:pred.pn</ta>
            <ta e="T339" id="Seg_4545" s="T338">n-n:case</ta>
            <ta e="T340" id="Seg_4546" s="T339">n-n:poss-n:case</ta>
            <ta e="T341" id="Seg_4547" s="T340">adj-n:poss-n:case</ta>
            <ta e="T342" id="Seg_4548" s="T341">v-v:tense-v:pred.pn</ta>
            <ta e="T343" id="Seg_4549" s="T342">conj</ta>
            <ta e="T344" id="Seg_4550" s="T343">v-v:tense-v:pred.pn</ta>
            <ta e="T345" id="Seg_4551" s="T344">n-n:case</ta>
            <ta e="T346" id="Seg_4552" s="T345">post</ta>
            <ta e="T347" id="Seg_4553" s="T346">n-n:case</ta>
            <ta e="T348" id="Seg_4554" s="T347">v-v:tense-v:pred.pn</ta>
            <ta e="T349" id="Seg_4555" s="T348">v-v:cvb</ta>
            <ta e="T350" id="Seg_4556" s="T349">v-v&gt;v-v:cvb</ta>
            <ta e="T351" id="Seg_4557" s="T350">post</ta>
            <ta e="T352" id="Seg_4558" s="T351">n-n:poss-n:case</ta>
            <ta e="T353" id="Seg_4559" s="T352">v-v:cvb</ta>
            <ta e="T354" id="Seg_4560" s="T353">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T355" id="Seg_4561" s="T354">dempro-pro:case</ta>
            <ta e="T356" id="Seg_4562" s="T355">n-n:poss-n:case</ta>
            <ta e="T357" id="Seg_4563" s="T356">n-n:case</ta>
            <ta e="T358" id="Seg_4564" s="T357">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T359" id="Seg_4565" s="T358">n-n:case</ta>
            <ta e="T360" id="Seg_4566" s="T359">n-n:(num)-n:poss-n:case</ta>
            <ta e="T361" id="Seg_4567" s="T360">v-v:cvb-v-v:cvb</ta>
            <ta e="T362" id="Seg_4568" s="T361">v-v:tense-v:pred.pn</ta>
            <ta e="T363" id="Seg_4569" s="T362">dempro-pro:case</ta>
            <ta e="T364" id="Seg_4570" s="T363">post</ta>
            <ta e="T365" id="Seg_4571" s="T364">n-n:(num)-n:case</ta>
            <ta e="T366" id="Seg_4572" s="T365">post</ta>
            <ta e="T367" id="Seg_4573" s="T366">n-n:(num)-n:case</ta>
            <ta e="T368" id="Seg_4574" s="T367">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T369" id="Seg_4575" s="T368">v-v:tense-v:pred.pn</ta>
            <ta e="T370" id="Seg_4576" s="T369">n-n:(num)-n:case</ta>
            <ta e="T371" id="Seg_4577" s="T370">dempro-pro:case</ta>
            <ta e="T372" id="Seg_4578" s="T371">n-n:poss-n:case</ta>
            <ta e="T373" id="Seg_4579" s="T372">v-v:cvb</ta>
            <ta e="T374" id="Seg_4580" s="T373">propr-n:case</ta>
            <ta e="T375" id="Seg_4581" s="T374">post</ta>
            <ta e="T376" id="Seg_4582" s="T375">v-v:ptcp</ta>
            <ta e="T377" id="Seg_4583" s="T376">v-v:tense-v:pred.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_4584" s="T0">n</ta>
            <ta e="T2" id="Seg_4585" s="T1">adv</ta>
            <ta e="T3" id="Seg_4586" s="T2">propr</ta>
            <ta e="T4" id="Seg_4587" s="T3">v</ta>
            <ta e="T5" id="Seg_4588" s="T4">aux</ta>
            <ta e="T6" id="Seg_4589" s="T5">propr</ta>
            <ta e="T7" id="Seg_4590" s="T6">n</ta>
            <ta e="T8" id="Seg_4591" s="T7">propr</ta>
            <ta e="T9" id="Seg_4592" s="T8">n</ta>
            <ta e="T10" id="Seg_4593" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_4594" s="T10">adv</ta>
            <ta e="T12" id="Seg_4595" s="T11">cardnum</ta>
            <ta e="T13" id="Seg_4596" s="T12">n</ta>
            <ta e="T14" id="Seg_4597" s="T13">n</ta>
            <ta e="T15" id="Seg_4598" s="T14">n</ta>
            <ta e="T16" id="Seg_4599" s="T15">v</ta>
            <ta e="T17" id="Seg_4600" s="T16">dempro</ta>
            <ta e="T18" id="Seg_4601" s="T17">n</ta>
            <ta e="T19" id="Seg_4602" s="T18">n</ta>
            <ta e="T20" id="Seg_4603" s="T19">v</ta>
            <ta e="T21" id="Seg_4604" s="T20">v</ta>
            <ta e="T22" id="Seg_4605" s="T21">post</ta>
            <ta e="T23" id="Seg_4606" s="T22">v</ta>
            <ta e="T24" id="Seg_4607" s="T23">v</ta>
            <ta e="T25" id="Seg_4608" s="T24">dempro</ta>
            <ta e="T26" id="Seg_4609" s="T25">v</ta>
            <ta e="T27" id="Seg_4610" s="T26">n</ta>
            <ta e="T28" id="Seg_4611" s="T27">v</ta>
            <ta e="T29" id="Seg_4612" s="T28">n</ta>
            <ta e="T30" id="Seg_4613" s="T29">n</ta>
            <ta e="T31" id="Seg_4614" s="T30">v</ta>
            <ta e="T32" id="Seg_4615" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_4616" s="T32">aux</ta>
            <ta e="T34" id="Seg_4617" s="T33">v</ta>
            <ta e="T35" id="Seg_4618" s="T34">v</ta>
            <ta e="T36" id="Seg_4619" s="T35">n</ta>
            <ta e="T37" id="Seg_4620" s="T36">v</ta>
            <ta e="T38" id="Seg_4621" s="T37">v</ta>
            <ta e="T39" id="Seg_4622" s="T38">post</ta>
            <ta e="T40" id="Seg_4623" s="T39">cardnum</ta>
            <ta e="T41" id="Seg_4624" s="T40">n</ta>
            <ta e="T42" id="Seg_4625" s="T41">v</ta>
            <ta e="T43" id="Seg_4626" s="T42">conj</ta>
            <ta e="T44" id="Seg_4627" s="T43">n</ta>
            <ta e="T45" id="Seg_4628" s="T44">n</ta>
            <ta e="T46" id="Seg_4629" s="T45">v</ta>
            <ta e="T47" id="Seg_4630" s="T46">post</ta>
            <ta e="T48" id="Seg_4631" s="T47">v</ta>
            <ta e="T49" id="Seg_4632" s="T48">aux</ta>
            <ta e="T50" id="Seg_4633" s="T49">adv</ta>
            <ta e="T51" id="Seg_4634" s="T50">n</ta>
            <ta e="T52" id="Seg_4635" s="T51">v</ta>
            <ta e="T53" id="Seg_4636" s="T52">n</ta>
            <ta e="T54" id="Seg_4637" s="T53">v</ta>
            <ta e="T55" id="Seg_4638" s="T54">v</ta>
            <ta e="T56" id="Seg_4639" s="T55">n</ta>
            <ta e="T57" id="Seg_4640" s="T56">adv</ta>
            <ta e="T58" id="Seg_4641" s="T57">v</ta>
            <ta e="T59" id="Seg_4642" s="T58">que</ta>
            <ta e="T60" id="Seg_4643" s="T59">ptcl</ta>
            <ta e="T61" id="Seg_4644" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_4645" s="T61">adj</ta>
            <ta e="T63" id="Seg_4646" s="T62">n</ta>
            <ta e="T64" id="Seg_4647" s="T63">ptcl</ta>
            <ta e="T65" id="Seg_4648" s="T64">n</ta>
            <ta e="T66" id="Seg_4649" s="T65">que</ta>
            <ta e="T67" id="Seg_4650" s="T66">v</ta>
            <ta e="T68" id="Seg_4651" s="T67">aux</ta>
            <ta e="T69" id="Seg_4652" s="T68">v</ta>
            <ta e="T70" id="Seg_4653" s="T69">adv</ta>
            <ta e="T71" id="Seg_4654" s="T70">v</ta>
            <ta e="T72" id="Seg_4655" s="T71">post</ta>
            <ta e="T73" id="Seg_4656" s="T72">n</ta>
            <ta e="T74" id="Seg_4657" s="T73">v</ta>
            <ta e="T75" id="Seg_4658" s="T74">v</ta>
            <ta e="T76" id="Seg_4659" s="T75">n</ta>
            <ta e="T77" id="Seg_4660" s="T76">adj</ta>
            <ta e="T78" id="Seg_4661" s="T77">ptcl</ta>
            <ta e="T79" id="Seg_4662" s="T78">cop</ta>
            <ta e="T80" id="Seg_4663" s="T79">pers</ta>
            <ta e="T81" id="Seg_4664" s="T80">n</ta>
            <ta e="T82" id="Seg_4665" s="T81">v</ta>
            <ta e="T83" id="Seg_4666" s="T82">que</ta>
            <ta e="T84" id="Seg_4667" s="T83">v</ta>
            <ta e="T85" id="Seg_4668" s="T84">adv</ta>
            <ta e="T86" id="Seg_4669" s="T85">v</ta>
            <ta e="T87" id="Seg_4670" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_4671" s="T87">v</ta>
            <ta e="T89" id="Seg_4672" s="T88">v</ta>
            <ta e="T90" id="Seg_4673" s="T89">ptcl</ta>
            <ta e="T91" id="Seg_4674" s="T90">adj</ta>
            <ta e="T92" id="Seg_4675" s="T91">n</ta>
            <ta e="T93" id="Seg_4676" s="T92">adv</ta>
            <ta e="T94" id="Seg_4677" s="T93">v</ta>
            <ta e="T95" id="Seg_4678" s="T94">n</ta>
            <ta e="T96" id="Seg_4679" s="T95">v</ta>
            <ta e="T97" id="Seg_4680" s="T96">v</ta>
            <ta e="T98" id="Seg_4681" s="T97">n</ta>
            <ta e="T99" id="Seg_4682" s="T98">v</ta>
            <ta e="T100" id="Seg_4683" s="T99">v</ta>
            <ta e="T101" id="Seg_4684" s="T100">aux</ta>
            <ta e="T102" id="Seg_4685" s="T101">n</ta>
            <ta e="T103" id="Seg_4686" s="T102">ptcl</ta>
            <ta e="T104" id="Seg_4687" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_4688" s="T104">n</ta>
            <ta e="T106" id="Seg_4689" s="T105">adv</ta>
            <ta e="T107" id="Seg_4690" s="T106">v</ta>
            <ta e="T108" id="Seg_4691" s="T107">aux</ta>
            <ta e="T109" id="Seg_4692" s="T108">adv</ta>
            <ta e="T110" id="Seg_4693" s="T109">v</ta>
            <ta e="T111" id="Seg_4694" s="T110">n</ta>
            <ta e="T112" id="Seg_4695" s="T111">adj</ta>
            <ta e="T113" id="Seg_4696" s="T112">v</ta>
            <ta e="T114" id="Seg_4697" s="T113">n</ta>
            <ta e="T115" id="Seg_4698" s="T114">v</ta>
            <ta e="T116" id="Seg_4699" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_4700" s="T116">que</ta>
            <ta e="T118" id="Seg_4701" s="T117">v</ta>
            <ta e="T119" id="Seg_4702" s="T118">adv</ta>
            <ta e="T120" id="Seg_4703" s="T119">pers</ta>
            <ta e="T121" id="Seg_4704" s="T120">cardnum</ta>
            <ta e="T122" id="Seg_4705" s="T121">n</ta>
            <ta e="T123" id="Seg_4706" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_4707" s="T123">cop</ta>
            <ta e="T125" id="Seg_4708" s="T124">n</ta>
            <ta e="T126" id="Seg_4709" s="T125">adv</ta>
            <ta e="T127" id="Seg_4710" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_4711" s="T127">cop</ta>
            <ta e="T129" id="Seg_4712" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_4713" s="T129">n</ta>
            <ta e="T131" id="Seg_4714" s="T130">adv</ta>
            <ta e="T132" id="Seg_4715" s="T131">pers</ta>
            <ta e="T133" id="Seg_4716" s="T132">que</ta>
            <ta e="T134" id="Seg_4717" s="T133">v</ta>
            <ta e="T135" id="Seg_4718" s="T134">adj</ta>
            <ta e="T136" id="Seg_4719" s="T135">ptcl</ta>
            <ta e="T137" id="Seg_4720" s="T136">n</ta>
            <ta e="T138" id="Seg_4721" s="T137">ptcl</ta>
            <ta e="T139" id="Seg_4722" s="T138">v</ta>
            <ta e="T140" id="Seg_4723" s="T139">que</ta>
            <ta e="T141" id="Seg_4724" s="T140">pers</ta>
            <ta e="T142" id="Seg_4725" s="T141">n</ta>
            <ta e="T143" id="Seg_4726" s="T142">v</ta>
            <ta e="T144" id="Seg_4727" s="T143">v</ta>
            <ta e="T145" id="Seg_4728" s="T144">n</ta>
            <ta e="T146" id="Seg_4729" s="T145">v</ta>
            <ta e="T147" id="Seg_4730" s="T146">pers</ta>
            <ta e="T148" id="Seg_4731" s="T147">que</ta>
            <ta e="T149" id="Seg_4732" s="T148">adj</ta>
            <ta e="T150" id="Seg_4733" s="T149">n</ta>
            <ta e="T151" id="Seg_4734" s="T150">n</ta>
            <ta e="T152" id="Seg_4735" s="T151">v</ta>
            <ta e="T153" id="Seg_4736" s="T152">adj</ta>
            <ta e="T154" id="Seg_4737" s="T153">cop</ta>
            <ta e="T155" id="Seg_4738" s="T154">aux</ta>
            <ta e="T156" id="Seg_4739" s="T155">ptcl</ta>
            <ta e="T157" id="Seg_4740" s="T156">v</ta>
            <ta e="T158" id="Seg_4741" s="T157">n</ta>
            <ta e="T159" id="Seg_4742" s="T158">adv</ta>
            <ta e="T160" id="Seg_4743" s="T159">pers</ta>
            <ta e="T161" id="Seg_4744" s="T160">pers</ta>
            <ta e="T162" id="Seg_4745" s="T161">v</ta>
            <ta e="T163" id="Seg_4746" s="T162">que</ta>
            <ta e="T164" id="Seg_4747" s="T163">adj</ta>
            <ta e="T165" id="Seg_4748" s="T164">pers</ta>
            <ta e="T166" id="Seg_4749" s="T165">pers</ta>
            <ta e="T167" id="Seg_4750" s="T166">cardnum</ta>
            <ta e="T168" id="Seg_4751" s="T167">n</ta>
            <ta e="T169" id="Seg_4752" s="T168">n</ta>
            <ta e="T170" id="Seg_4753" s="T169">post</ta>
            <ta e="T171" id="Seg_4754" s="T170">v</ta>
            <ta e="T172" id="Seg_4755" s="T171">n</ta>
            <ta e="T173" id="Seg_4756" s="T172">adv</ta>
            <ta e="T174" id="Seg_4757" s="T173">v</ta>
            <ta e="T175" id="Seg_4758" s="T174">n</ta>
            <ta e="T176" id="Seg_4759" s="T175">ptcl</ta>
            <ta e="T177" id="Seg_4760" s="T176">n</ta>
            <ta e="T178" id="Seg_4761" s="T177">v</ta>
            <ta e="T179" id="Seg_4762" s="T178">aux</ta>
            <ta e="T180" id="Seg_4763" s="T179">adv</ta>
            <ta e="T181" id="Seg_4764" s="T180">pers</ta>
            <ta e="T182" id="Seg_4765" s="T181">v</ta>
            <ta e="T183" id="Seg_4766" s="T182">v</ta>
            <ta e="T184" id="Seg_4767" s="T183">n</ta>
            <ta e="T185" id="Seg_4768" s="T184">v</ta>
            <ta e="T186" id="Seg_4769" s="T185">v</ta>
            <ta e="T187" id="Seg_4770" s="T186">ptcl</ta>
            <ta e="T188" id="Seg_4771" s="T187">adj</ta>
            <ta e="T189" id="Seg_4772" s="T188">adj</ta>
            <ta e="T190" id="Seg_4773" s="T189">aux</ta>
            <ta e="T191" id="Seg_4774" s="T190">n</ta>
            <ta e="T192" id="Seg_4775" s="T191">v</ta>
            <ta e="T193" id="Seg_4776" s="T192">n</ta>
            <ta e="T194" id="Seg_4777" s="T193">v</ta>
            <ta e="T195" id="Seg_4778" s="T194">cardnum</ta>
            <ta e="T196" id="Seg_4779" s="T195">n</ta>
            <ta e="T197" id="Seg_4780" s="T196">v</ta>
            <ta e="T198" id="Seg_4781" s="T197">ptcl</ta>
            <ta e="T199" id="Seg_4782" s="T198">pers</ta>
            <ta e="T200" id="Seg_4783" s="T199">n</ta>
            <ta e="T201" id="Seg_4784" s="T200">adv</ta>
            <ta e="T202" id="Seg_4785" s="T201">v</ta>
            <ta e="T203" id="Seg_4786" s="T202">post</ta>
            <ta e="T204" id="Seg_4787" s="T203">v</ta>
            <ta e="T205" id="Seg_4788" s="T204">pers</ta>
            <ta e="T206" id="Seg_4789" s="T205">pers</ta>
            <ta e="T207" id="Seg_4790" s="T206">v</ta>
            <ta e="T208" id="Seg_4791" s="T207">v</ta>
            <ta e="T209" id="Seg_4792" s="T208">dempro</ta>
            <ta e="T210" id="Seg_4793" s="T209">n</ta>
            <ta e="T211" id="Seg_4794" s="T210">pers</ta>
            <ta e="T212" id="Seg_4795" s="T211">v</ta>
            <ta e="T213" id="Seg_4796" s="T212">n</ta>
            <ta e="T214" id="Seg_4797" s="T213">n</ta>
            <ta e="T215" id="Seg_4798" s="T214">dempro</ta>
            <ta e="T216" id="Seg_4799" s="T215">n</ta>
            <ta e="T217" id="Seg_4800" s="T216">pers</ta>
            <ta e="T218" id="Seg_4801" s="T217">n</ta>
            <ta e="T219" id="Seg_4802" s="T218">pers</ta>
            <ta e="T220" id="Seg_4803" s="T219">ptcl</ta>
            <ta e="T221" id="Seg_4804" s="T220">pers</ta>
            <ta e="T222" id="Seg_4805" s="T221">n</ta>
            <ta e="T223" id="Seg_4806" s="T222">n</ta>
            <ta e="T224" id="Seg_4807" s="T223">cop</ta>
            <ta e="T225" id="Seg_4808" s="T224">n</ta>
            <ta e="T226" id="Seg_4809" s="T225">n</ta>
            <ta e="T227" id="Seg_4810" s="T226">post</ta>
            <ta e="T228" id="Seg_4811" s="T227">v</ta>
            <ta e="T229" id="Seg_4812" s="T228">aux</ta>
            <ta e="T230" id="Seg_4813" s="T229">v</ta>
            <ta e="T231" id="Seg_4814" s="T230">cardnum</ta>
            <ta e="T232" id="Seg_4815" s="T231">n</ta>
            <ta e="T233" id="Seg_4816" s="T232">n</ta>
            <ta e="T234" id="Seg_4817" s="T233">v</ta>
            <ta e="T235" id="Seg_4818" s="T234">dempro</ta>
            <ta e="T236" id="Seg_4819" s="T235">post</ta>
            <ta e="T237" id="Seg_4820" s="T236">n</ta>
            <ta e="T238" id="Seg_4821" s="T237">v</ta>
            <ta e="T239" id="Seg_4822" s="T238">aux</ta>
            <ta e="T240" id="Seg_4823" s="T239">ptcl</ta>
            <ta e="T241" id="Seg_4824" s="T240">n</ta>
            <ta e="T242" id="Seg_4825" s="T241">adv</ta>
            <ta e="T243" id="Seg_4826" s="T242">v</ta>
            <ta e="T244" id="Seg_4827" s="T243">v</ta>
            <ta e="T245" id="Seg_4828" s="T244">n</ta>
            <ta e="T246" id="Seg_4829" s="T245">cardnum</ta>
            <ta e="T247" id="Seg_4830" s="T246">n</ta>
            <ta e="T248" id="Seg_4831" s="T247">v</ta>
            <ta e="T249" id="Seg_4832" s="T248">conj</ta>
            <ta e="T250" id="Seg_4833" s="T249">v</ta>
            <ta e="T251" id="Seg_4834" s="T250">aux</ta>
            <ta e="T252" id="Seg_4835" s="T251">dempro</ta>
            <ta e="T253" id="Seg_4836" s="T252">v</ta>
            <ta e="T254" id="Seg_4837" s="T253">v</ta>
            <ta e="T255" id="Seg_4838" s="T254">que</ta>
            <ta e="T256" id="Seg_4839" s="T255">ptcl</ta>
            <ta e="T257" id="Seg_4840" s="T256">n</ta>
            <ta e="T258" id="Seg_4841" s="T257">n</ta>
            <ta e="T259" id="Seg_4842" s="T258">adv</ta>
            <ta e="T260" id="Seg_4843" s="T259">n</ta>
            <ta e="T261" id="Seg_4844" s="T260">n</ta>
            <ta e="T262" id="Seg_4845" s="T261">ptcl</ta>
            <ta e="T263" id="Seg_4846" s="T262">dempro</ta>
            <ta e="T264" id="Seg_4847" s="T263">n</ta>
            <ta e="T265" id="Seg_4848" s="T264">v</ta>
            <ta e="T266" id="Seg_4849" s="T265">v</ta>
            <ta e="T267" id="Seg_4850" s="T266">v</ta>
            <ta e="T268" id="Seg_4851" s="T267">n</ta>
            <ta e="T269" id="Seg_4852" s="T268">n</ta>
            <ta e="T270" id="Seg_4853" s="T269">v</ta>
            <ta e="T271" id="Seg_4854" s="T270">n</ta>
            <ta e="T272" id="Seg_4855" s="T271">n</ta>
            <ta e="T273" id="Seg_4856" s="T272">v</ta>
            <ta e="T274" id="Seg_4857" s="T273">n</ta>
            <ta e="T275" id="Seg_4858" s="T274">post</ta>
            <ta e="T276" id="Seg_4859" s="T275">cop</ta>
            <ta e="T277" id="Seg_4860" s="T276">n</ta>
            <ta e="T278" id="Seg_4861" s="T277">v</ta>
            <ta e="T279" id="Seg_4862" s="T278">conj</ta>
            <ta e="T280" id="Seg_4863" s="T279">n</ta>
            <ta e="T281" id="Seg_4864" s="T280">post</ta>
            <ta e="T282" id="Seg_4865" s="T281">v</ta>
            <ta e="T283" id="Seg_4866" s="T282">pers</ta>
            <ta e="T284" id="Seg_4867" s="T283">n</ta>
            <ta e="T285" id="Seg_4868" s="T284">n</ta>
            <ta e="T286" id="Seg_4869" s="T285">n</ta>
            <ta e="T287" id="Seg_4870" s="T286">v</ta>
            <ta e="T288" id="Seg_4871" s="T287">aux</ta>
            <ta e="T289" id="Seg_4872" s="T288">pers</ta>
            <ta e="T290" id="Seg_4873" s="T289">n</ta>
            <ta e="T291" id="Seg_4874" s="T290">n</ta>
            <ta e="T292" id="Seg_4875" s="T291">v</ta>
            <ta e="T293" id="Seg_4876" s="T292">v</ta>
            <ta e="T294" id="Seg_4877" s="T293">ptcl</ta>
            <ta e="T295" id="Seg_4878" s="T294">ptcl</ta>
            <ta e="T296" id="Seg_4879" s="T295">adv</ta>
            <ta e="T297" id="Seg_4880" s="T296">v</ta>
            <ta e="T298" id="Seg_4881" s="T297">v</ta>
            <ta e="T299" id="Seg_4882" s="T298">v</ta>
            <ta e="T300" id="Seg_4883" s="T299">n</ta>
            <ta e="T301" id="Seg_4884" s="T300">n</ta>
            <ta e="T302" id="Seg_4885" s="T301">dempro</ta>
            <ta e="T303" id="Seg_4886" s="T302">n</ta>
            <ta e="T304" id="Seg_4887" s="T303">n</ta>
            <ta e="T305" id="Seg_4888" s="T304">v</ta>
            <ta e="T306" id="Seg_4889" s="T305">aux</ta>
            <ta e="T307" id="Seg_4890" s="T306">n</ta>
            <ta e="T308" id="Seg_4891" s="T307">v</ta>
            <ta e="T309" id="Seg_4892" s="T308">post</ta>
            <ta e="T310" id="Seg_4893" s="T309">n</ta>
            <ta e="T311" id="Seg_4894" s="T310">n</ta>
            <ta e="T312" id="Seg_4895" s="T311">n</ta>
            <ta e="T313" id="Seg_4896" s="T312">n</ta>
            <ta e="T314" id="Seg_4897" s="T313">v</ta>
            <ta e="T315" id="Seg_4898" s="T314">aux</ta>
            <ta e="T316" id="Seg_4899" s="T315">n</ta>
            <ta e="T317" id="Seg_4900" s="T316">v</ta>
            <ta e="T318" id="Seg_4901" s="T317">post</ta>
            <ta e="T319" id="Seg_4902" s="T318">v</ta>
            <ta e="T320" id="Seg_4903" s="T319">n</ta>
            <ta e="T321" id="Seg_4904" s="T320">n</ta>
            <ta e="T322" id="Seg_4905" s="T321">adv</ta>
            <ta e="T323" id="Seg_4906" s="T322">v</ta>
            <ta e="T324" id="Seg_4907" s="T323">n</ta>
            <ta e="T325" id="Seg_4908" s="T324">n</ta>
            <ta e="T326" id="Seg_4909" s="T325">v</ta>
            <ta e="T327" id="Seg_4910" s="T326">v</ta>
            <ta e="T328" id="Seg_4911" s="T327">v</ta>
            <ta e="T329" id="Seg_4912" s="T328">aux</ta>
            <ta e="T330" id="Seg_4913" s="T329">post</ta>
            <ta e="T331" id="Seg_4914" s="T330">n</ta>
            <ta e="T332" id="Seg_4915" s="T331">post</ta>
            <ta e="T333" id="Seg_4916" s="T332">v</ta>
            <ta e="T334" id="Seg_4917" s="T333">n</ta>
            <ta e="T335" id="Seg_4918" s="T334">n</ta>
            <ta e="T336" id="Seg_4919" s="T335">n</ta>
            <ta e="T337" id="Seg_4920" s="T336">v</ta>
            <ta e="T338" id="Seg_4921" s="T337">aux</ta>
            <ta e="T339" id="Seg_4922" s="T338">n</ta>
            <ta e="T340" id="Seg_4923" s="T339">n</ta>
            <ta e="T341" id="Seg_4924" s="T340">adj</ta>
            <ta e="T342" id="Seg_4925" s="T341">v</ta>
            <ta e="T343" id="Seg_4926" s="T342">conj</ta>
            <ta e="T344" id="Seg_4927" s="T343">v</ta>
            <ta e="T345" id="Seg_4928" s="T344">n</ta>
            <ta e="T346" id="Seg_4929" s="T345">post</ta>
            <ta e="T347" id="Seg_4930" s="T346">n</ta>
            <ta e="T348" id="Seg_4931" s="T347">v</ta>
            <ta e="T349" id="Seg_4932" s="T348">v</ta>
            <ta e="T350" id="Seg_4933" s="T349">v</ta>
            <ta e="T351" id="Seg_4934" s="T350">post</ta>
            <ta e="T352" id="Seg_4935" s="T351">n</ta>
            <ta e="T353" id="Seg_4936" s="T352">v</ta>
            <ta e="T354" id="Seg_4937" s="T353">v</ta>
            <ta e="T355" id="Seg_4938" s="T354">dempro</ta>
            <ta e="T356" id="Seg_4939" s="T355">n</ta>
            <ta e="T357" id="Seg_4940" s="T356">n</ta>
            <ta e="T358" id="Seg_4941" s="T357">v</ta>
            <ta e="T359" id="Seg_4942" s="T358">n</ta>
            <ta e="T360" id="Seg_4943" s="T359">n</ta>
            <ta e="T361" id="Seg_4944" s="T360">v</ta>
            <ta e="T362" id="Seg_4945" s="T361">v</ta>
            <ta e="T363" id="Seg_4946" s="T362">dempro</ta>
            <ta e="T364" id="Seg_4947" s="T363">post</ta>
            <ta e="T365" id="Seg_4948" s="T364">n</ta>
            <ta e="T366" id="Seg_4949" s="T365">post</ta>
            <ta e="T367" id="Seg_4950" s="T366">n</ta>
            <ta e="T368" id="Seg_4951" s="T367">v</ta>
            <ta e="T369" id="Seg_4952" s="T368">v</ta>
            <ta e="T370" id="Seg_4953" s="T369">n</ta>
            <ta e="T371" id="Seg_4954" s="T370">dempro</ta>
            <ta e="T372" id="Seg_4955" s="T371">n</ta>
            <ta e="T373" id="Seg_4956" s="T372">v</ta>
            <ta e="T374" id="Seg_4957" s="T373">propr</ta>
            <ta e="T375" id="Seg_4958" s="T374">post</ta>
            <ta e="T376" id="Seg_4959" s="T375">v</ta>
            <ta e="T377" id="Seg_4960" s="T376">aux</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR" />
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T9" id="Seg_4961" s="T0">Long long ago the Nganasans lived at the [mountain range] Kerike, at [lake] Yesej the Changits and at the [mountain range] Taas the Evenki.</ta>
            <ta e="T16" id="Seg_4962" s="T9">Once the Changits stole a wife of one Nganasan.</ta>
            <ta e="T24" id="Seg_4963" s="T16">After her husband went hunting on wild reindeers, the woman was sitting and sewing.</ta>
            <ta e="T28" id="Seg_4964" s="T24">As she was sitting so, the dogs started to bark.</ta>
            <ta e="T35" id="Seg_4965" s="T28">"They would not bark sat their master", she thought.</ta>
            <ta e="T49" id="Seg_4966" s="T35">As she wanted to light up the fire, three humans came in, wrapped the woman up into fabric and took her away.</ta>
            <ta e="T62" id="Seg_4967" s="T49">In the evening the Nganasan came: the dogs are lying there tied up, the tent is frozen through, nobody is in there, it is completely dark.</ta>
            <ta e="T69" id="Seg_4968" s="T62">"What a sin, where could my wife have gone?", he brooded.</ta>
            <ta e="T75" id="Seg_4969" s="T69">In the morning, as it got light, he went to bring his herd.</ta>
            <ta e="T79" id="Seg_4970" s="T75">All his reindeers were there, apparently.</ta>
            <ta e="T85" id="Seg_4971" s="T79">"I will go to my Evenki, what shall I do alone?</ta>
            <ta e="T89" id="Seg_4972" s="T85">I will consult with him", he thought.</ta>
            <ta e="T104" id="Seg_4973" s="T89">Whether the lonely human needed a long time to gather or not, he came to the Evenki, but that one had gone fishing with a net, apparently, only his mother was at home.</ta>
            <ta e="T108" id="Seg_4974" s="T104">The Evenki was apparently living along.</ta>
            <ta e="T110" id="Seg_4975" s="T108">He came in the evening.</ta>
            <ta e="T115" id="Seg_4976" s="T110">The poor Nganasan told him that his wife had gone lost.</ta>
            <ta e="T124" id="Seg_4977" s="T115">"Who could have done this? Yesterday three men came to me.</ta>
            <ta e="T131" id="Seg_4978" s="T124">They didn't look very nice, apparently, they were the Changits.</ta>
            <ta e="T134" id="Seg_4979" s="T131">But what should they take away from me?!"</ta>
            <ta e="T146" id="Seg_4980" s="T134">The closest neighbours also tell that their wives are stolen", the Evenki said.</ta>
            <ta e="T152" id="Seg_4981" s="T146">"How brave are you?", the Evenki asked his friend.</ta>
            <ta e="T158" id="Seg_4982" s="T152">"I am pretty brave", said the Nganasan.</ta>
            <ta e="T164" id="Seg_4983" s="T158">"Then, for that I know how strong you are.</ta>
            <ta e="T171" id="Seg_4984" s="T164">I will shoot at you from the distance of approximately three dog sledges."</ta>
            <ta e="T179" id="Seg_4985" s="T171">The Evenki shot three times, but shot just through the edge of his seam.</ta>
            <ta e="T183" id="Seg_4986" s="T179">"Now you shoot", he said.</ta>
            <ta e="T186" id="Seg_4987" s="T183">The Nganasan shot, he didn't strike.</ta>
            <ta e="T188" id="Seg_4988" s="T186">"Well then.</ta>
            <ta e="T192" id="Seg_4989" s="T188">You are obviously brave", the Evenki praised.</ta>
            <ta e="T194" id="Seg_4990" s="T192">The next morning they went off.</ta>
            <ta e="T197" id="Seg_4991" s="T194">They crossed three mountain ranges.</ta>
            <ta e="T204" id="Seg_4992" s="T197">"Well, I will dress up my mother into Nganasan clothes and go.</ta>
            <ta e="T207" id="Seg_4993" s="T204">I will deceive them.</ta>
            <ta e="T218" id="Seg_4994" s="T207">I will say: 'This woman is my wife, she is the elder sister of the woman that you've taken.'</ta>
            <ta e="T224" id="Seg_4995" s="T218">You, though, will be the younger brother of my wife."</ta>
            <ta e="T229" id="Seg_4996" s="T224">The Evenki went off with his mother.</ta>
            <ta e="T234" id="Seg_4997" s="T229">They came there, seven pole tents are standing there.</ta>
            <ta e="T239" id="Seg_4998" s="T234">People are dancing around them.</ta>
            <ta e="T244" id="Seg_4999" s="T239">"This must be a wedding", they thought.</ta>
            <ta e="T251" id="Seg_5000" s="T244">The Evenki embraced one human and they were dancing.</ta>
            <ta e="T254" id="Seg_5001" s="T251">Dancing he asked: </ta>
            <ta e="T262" id="Seg_5002" s="T254">"Where is the tent of the eldest one? There should be the Nganasan's wife." </ta>
            <ta e="T268" id="Seg_5003" s="T262">That human answered very angrily and pointed to a tent.</ta>
            <ta e="T276" id="Seg_5004" s="T268">The Evenki went inside, a woman was crying tears as big as a scoop.</ta>
            <ta e="T282" id="Seg_5005" s="T276">The Evenki went inside and strode to the eldest one: </ta>
            <ta e="T288" id="Seg_5006" s="T282">"You have married the younger sister of my wife.</ta>
            <ta e="T293" id="Seg_5007" s="T288">My wife wants to meet her sister."</ta>
            <ta e="T300" id="Seg_5008" s="T293">"Well, all right, tomorrow we will go to meet her", said the Changit.</ta>
            <ta e="T306" id="Seg_5009" s="T300">The next morning the Changit brought the woman</ta>
            <ta e="T315" id="Seg_5010" s="T306">As they just had gone into the tent, the Nganasan cut the strings of the Changit's reindeer sledge.</ta>
            <ta e="T319" id="Seg_5011" s="T315">He put his woman onto the sledge and escaped.</ta>
            <ta e="T323" id="Seg_5012" s="T319">The Changit followed them by foot.</ta>
            <ta e="T327" id="Seg_5013" s="T323">He shot through the Nganasan's [leg] muscles.</ta>
            <ta e="T338" id="Seg_5014" s="T327">After he had stopped, the Changit went towards the Nganasan and shot [an arrow] past his ear.</ta>
            <ta e="T344" id="Seg_5015" s="T338">The Changit shot all his arrows and escaped.</ta>
            <ta e="T354" id="Seg_5016" s="T344">The Nganasan and the Evenki followed him, reached him and split his head.</ta>
            <ta e="T362" id="Seg_5017" s="T354">After that the Evenki married, because of the Changit's reindeers they lived rich and wealthy.</ta>
            <ta e="T369" id="Seg_5018" s="T362">And so the Nganasans and the Evenki lived peacefully together.</ta>
            <ta e="T377" id="Seg_5019" s="T369">Being afraid, the Nganasans stopped nomadizing to the Taas crest after that.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T9" id="Seg_5020" s="T0">Vor langer langer Zeit lebten die Nganasanen am [Bergrücken] Kerike, am [See] Jesej die Tschangiten und am [Bergrücken] Taas die Ewenken.</ta>
            <ta e="T16" id="Seg_5021" s="T9">Einmal stahlen die Tschangiten einem Nganasanen seine Frau.</ta>
            <ta e="T24" id="Seg_5022" s="T16">Nachdem ihr Mann zur Jagd auf wilde Rentiere gegangen war, saß die Frau und nähte.</ta>
            <ta e="T28" id="Seg_5023" s="T24">Als sie so saß, bellten die Hunde.</ta>
            <ta e="T35" id="Seg_5024" s="T28">"Ihren Herren würden sie wohl nicht anbellen", dachte sie.</ta>
            <ta e="T49" id="Seg_5025" s="T35">Als sie gerade das Feuer anzünden wollte, kamen drei Menschen, wickelten die Frau in Stoff ein und nahmen sie mit.</ta>
            <ta e="T62" id="Seg_5026" s="T49">Am Abend kam der Nganasane: Die Hunde liegen angebunden da, das Zelt ist ganz ausgekühlt, niemand ist da, es ist ganz dunkel.</ta>
            <ta e="T69" id="Seg_5027" s="T62">"Was für eine Sünde, wohin kann meine Frau gegangen sein?", grübelte er.</ta>
            <ta e="T75" id="Seg_5028" s="T69">Am Morgen, als es gerade hell wurde, ging er seine Herde holen.</ta>
            <ta e="T79" id="Seg_5029" s="T75">Alle Rentiere waren offenbar da.</ta>
            <ta e="T85" id="Seg_5030" s="T79">"Ich fahre zu meinem Ewenken, was soll ich alleine tun?</ta>
            <ta e="T89" id="Seg_5031" s="T85">Ich berate mich dann mit ihm", dachte er.</ta>
            <ta e="T104" id="Seg_5032" s="T89">Ob der einsame Mensch lange brauchte, sich zu sammeln, oder nicht, er kam zum Ewenken, der war offenbar mit dem Netz fischen gegangen, nur seine Mutter war da.</ta>
            <ta e="T108" id="Seg_5033" s="T104">Der Ewenke lebte offenbar ganz alleine.</ta>
            <ta e="T110" id="Seg_5034" s="T108">Er kam am Abend.</ta>
            <ta e="T115" id="Seg_5035" s="T110">Der arme Nganasane erzählte, dass seine Frau verschwunden war.</ta>
            <ta e="T124" id="Seg_5036" s="T115">"Wer kann das getan haben? Gestern waren drei Männer bei mir.</ta>
            <ta e="T131" id="Seg_5037" s="T124">Sie sahen nicht besonders [gut] aus, offenbar waren es Tschangiten.</ta>
            <ta e="T134" id="Seg_5038" s="T131">Aber was sollen sie mir schon wegnehmen?!"</ta>
            <ta e="T146" id="Seg_5039" s="T134">Die nächsten Nachbarn erzählen auch, dass ihre Frauen gestohlen werden", sagte der Ewenke.</ta>
            <ta e="T152" id="Seg_5040" s="T146">"Wie mutig bist du?", fragte der Ewenke seinen Freund.</ta>
            <ta e="T158" id="Seg_5041" s="T152">"Ich bin wohl schon recht mutig", sagte der Nganasane.</ta>
            <ta e="T164" id="Seg_5042" s="T158">"Dann, damit ich weiß, wie stark du bist.</ta>
            <ta e="T171" id="Seg_5043" s="T164">Ich schieße auf dich ungefähr aus der Entfernung von drei Hundeschlitten."</ta>
            <ta e="T179" id="Seg_5044" s="T171">Der Ewenke schoss drei mal, schoss aber nur den Rand seines Saumes ab.</ta>
            <ta e="T183" id="Seg_5045" s="T179">"Nun schieß du", sagte er.</ta>
            <ta e="T186" id="Seg_5046" s="T183">Der Nganasane schoss, er traf nicht.</ta>
            <ta e="T188" id="Seg_5047" s="T186">"Nun gut.</ta>
            <ta e="T192" id="Seg_5048" s="T188">Du bist wohl wirklich mutig", lobte ihn der Ewenke.</ta>
            <ta e="T194" id="Seg_5049" s="T192">Am nächsten Morgen fuhren sie los.</ta>
            <ta e="T197" id="Seg_5050" s="T194">Sie überquerten drei Bergrücken.</ta>
            <ta e="T204" id="Seg_5051" s="T197">"Nun, ich ziehe meiner Mutter nganasanische Sachen an und gehe.</ta>
            <ta e="T207" id="Seg_5052" s="T204">Ich betrüge sie.</ta>
            <ta e="T218" id="Seg_5053" s="T207">Ich werde sagen: 'Diese Frau, das ist meine Frau, sie ist die ältere Schwester der Frau, die ihr genommen habt.'</ta>
            <ta e="T224" id="Seg_5054" s="T218">Du aber wirst der jüngere Bruder meiner Frau sein."</ta>
            <ta e="T229" id="Seg_5055" s="T224">Der Ewenke ging mit seiner Mutter los.</ta>
            <ta e="T234" id="Seg_5056" s="T229">Sie kamen an, dort stehen sieben Stangenzelte.</ta>
            <ta e="T239" id="Seg_5057" s="T234">Um sie herum tanzten Menschen.</ta>
            <ta e="T244" id="Seg_5058" s="T239">"Das ist wohl eine Hochzeit", dachten sie.</ta>
            <ta e="T251" id="Seg_5059" s="T244">Der Ewenke umarmte einen Menschen und sie tanzten.</ta>
            <ta e="T254" id="Seg_5060" s="T251">So tanzend fragte er: </ta>
            <ta e="T262" id="Seg_5061" s="T254">"Wo ist das Zelt eures Ältesten, dort ist die Frau des Nganasanen."</ta>
            <ta e="T268" id="Seg_5062" s="T262">Dieser Mensch antwortete sehr verärgert und zeigte auf ein Zelt.</ta>
            <ta e="T276" id="Seg_5063" s="T268">Der Ewenke ging hinein, eine Frau weinte Tränen so groß wie Schöpfkellen.</ta>
            <ta e="T282" id="Seg_5064" s="T276">Der Ewenke ging hinein und schritt zum Ältesten: </ta>
            <ta e="T288" id="Seg_5065" s="T282">"Du hast die jüngere Schwester meiner Frau zur Frau genommen.</ta>
            <ta e="T293" id="Seg_5066" s="T288">Meine Frau möchte ihre Schwester treffen."</ta>
            <ta e="T300" id="Seg_5067" s="T293">"Nun gut, morgen gehen wir zum Treffen", sagte der Tschangit.</ta>
            <ta e="T306" id="Seg_5068" s="T300">Am anderen Morgen brachte der Tschangit die Frau mit.</ta>
            <ta e="T315" id="Seg_5069" s="T306">Als sie gerade ins Zelt hereingekommen waren, zerschnitt der Nganasane die Rentierschnur des Tschangiten.</ta>
            <ta e="T319" id="Seg_5070" s="T315">Er setzte seine Frau auf den Schlitten und entfloh.</ta>
            <ta e="T323" id="Seg_5071" s="T319">Der Tschangit verfolgte sie zu Fuß.</ta>
            <ta e="T327" id="Seg_5072" s="T323">Er zerschoss die [Bein]Muskeln des Nganasanen.</ta>
            <ta e="T338" id="Seg_5073" s="T327">Nachdem jener angehalten hatte, ging der Tschangit auf den Nganasanen zu und schoss [einen Pfeil] an seinem Ohr vorbei.</ta>
            <ta e="T344" id="Seg_5074" s="T338">Der Tschangit schoss alle seine Pfeile und floh.</ta>
            <ta e="T354" id="Seg_5075" s="T344">Der Nganasane und der Ewenke verfolgten ihn, erreichten ihn und zerschlugen ihm den Kopf.</ta>
            <ta e="T362" id="Seg_5076" s="T354">Danach heiratete der Ewenke, durch die Rentiere des Tschangiten lebten sie in Reichtum und Wohlstand.</ta>
            <ta e="T369" id="Seg_5077" s="T362">Und so lebten Nganasanen und Ewenken friedlich zusammen.</ta>
            <ta e="T377" id="Seg_5078" s="T369">Nach alledem fürchteten sich die Nganasanen und hörten auf, in Richtung Taas zu ziehen.</ta>
         </annotation>
         <annotation name="fr" tierref="fr">
            <ta e="T9" id="Seg_5079" s="T0">Давным-давно нганасаны жили у хребта Кэрикэ, на Есее — чангиты, на Камне — эвенки.</ta>
            <ta e="T16" id="Seg_5080" s="T9">Как-то у одного нганасанина чангиты украли жену.</ta>
            <ta e="T24" id="Seg_5081" s="T16">Когда муж ушел на охоту за дикими оленями, эта женщина сидела за шитьем.</ta>
            <ta e="T28" id="Seg_5082" s="T24">Вот когда так сидела, залаяли собаки.</ta>
            <ta e="T35" id="Seg_5083" s="T28">"На хозяина собаки не лаяли бы", подумала она.</ta>
            <ta e="T49" id="Seg_5084" s="T35">Как только приготовилась развести очаг, зашли трое, завернули женщину в материю и взяли с собой.</ta>
            <ta e="T62" id="Seg_5085" s="T49">Вечером приходит нганасанин: собаки лежат на привязи, чум насквозь промерз, никого нет, темным-темно.</ta>
            <ta e="T69" id="Seg_5086" s="T62">"Грех-то какой, жена куда могла уйти?", опечалился он.</ta>
            <ta e="T75" id="Seg_5087" s="T69">Утром, как только рассвело, он пошел пригнать свое стадо.</ta>
            <ta e="T79" id="Seg_5088" s="T75">Все олени, оказывается, были на месте.</ta>
            <ta e="T85" id="Seg_5089" s="T79">"Откочую я к моему эвенку, что мне одному делать?</ta>
            <ta e="T89" id="Seg_5090" s="T85">Хоть посоветуюсь", так подумал.</ta>
            <ta e="T104" id="Seg_5091" s="T89">Одинокому человеку долго ли собраться — прикочевал к эвенку, оказывается, тот ушел проверять сети, дома была только его мать.</ta>
            <ta e="T108" id="Seg_5092" s="T104">Эвенк, оказывается, совсем одиноко жил.</ta>
            <ta e="T110" id="Seg_5093" s="T108">Он пришел вечером.</ta>
            <ta e="T115" id="Seg_5094" s="T110">Бедный нганасанин рассказал, что исчезла его жена.</ta>
            <ta e="T124" id="Seg_5095" s="T115">"Ну, кто же мог сделать? Вчера ко мне заходили трое мужчин.</ta>
            <ta e="T131" id="Seg_5096" s="T124">На вид не очень хорошие люди, видимо, чангиты.</ta>
            <ta e="T134" id="Seg_5097" s="T131">Что им с меня взять?!"</ta>
            <ta e="T146" id="Seg_5098" s="T134">Ближние соседи тоже рассказывали, что они крадут женщин", сказал эвенк.</ta>
            <ta e="T152" id="Seg_5099" s="T146">"Насколько ты ловок как косун?", спросил эвенк у друга.</ta>
            <ta e="T158" id="Seg_5100" s="T152">"Раз жив, наверно, не такой уж слабый косун", сказал нганасанин.</ta>
            <ta e="T164" id="Seg_5101" s="T158">"Тогда, чтобы я знал что ты умеешь.</ta>
            <ta e="T171" id="Seg_5102" s="T164">Я буду стрелять в тебя с расстояния трех собачьих нарт."</ta>
            <ta e="T179" id="Seg_5103" s="T171">Эвенк выстрелил три раза, пострелил только край его подола.</ta>
            <ta e="T183" id="Seg_5104" s="T179">"Теперь ты стреляй", сказал.</ta>
            <ta e="T186" id="Seg_5105" s="T183">Нганасанин выстрелил — не попал.</ta>
            <ta e="T188" id="Seg_5106" s="T186">"Ну, хорошо.</ta>
            <ta e="T192" id="Seg_5107" s="T188">Ты, оказывается, [настоящий] косун", похвалил эвенк.</ta>
            <ta e="T194" id="Seg_5108" s="T192">Назавтра отправились в путь.</ta>
            <ta e="T197" id="Seg_5109" s="T194">Перевалили через три горных хребта.</ta>
            <ta e="T204" id="Seg_5110" s="T197">"Вот я переодену мать в нганасанскую одежду и поеду к ним.</ta>
            <ta e="T207" id="Seg_5111" s="T204">Я их обману.</ta>
            <ta e="T218" id="Seg_5112" s="T207">Скажу: 'эта женщина — моя жена, она старшая сестра той женщины, которую вы взяли.'</ta>
            <ta e="T224" id="Seg_5113" s="T218">А ты назовешься братом моей жены."</ta>
            <ta e="T229" id="Seg_5114" s="T224">Эвенк с матерью уехали.</ta>
            <ta e="T234" id="Seg_5115" s="T229">Подъехали и видят, стоят семь чумов.</ta>
            <ta e="T239" id="Seg_5116" s="T234">Вокруг них танцуют люди.</ta>
            <ta e="T244" id="Seg_5117" s="T239">"Кажется, у них свадьба", подумали.</ta>
            <ta e="T251" id="Seg_5118" s="T244">Эвенк подхватил одного человека и [все] танцевали.</ta>
            <ta e="T254" id="Seg_5119" s="T251">Танцуя, спросил: </ta>
            <ta e="T262" id="Seg_5120" s="T254">"Где здесь чум старшего, там должна быть жена нганасанина."</ta>
            <ta e="T268" id="Seg_5121" s="T262">Тот человек ответил сердито-пресердито и указал на чум.</ta>
            <ta e="T276" id="Seg_5122" s="T268">Эвенк зашел — плакает женщина, глаза от слез стали размером с ковш.</ta>
            <ta e="T282" id="Seg_5123" s="T276">Эвенк сразу шагнул в сторону самого старшего: </ta>
            <ta e="T288" id="Seg_5124" s="T282">"Ты взял в жену сестру моей жены.</ta>
            <ta e="T293" id="Seg_5125" s="T288">Моя жена хочет встретиться с сестрой."</ta>
            <ta e="T300" id="Seg_5126" s="T293">"Ну ладно, завтра поедем на встречу", сказал чангит.</ta>
            <ta e="T306" id="Seg_5127" s="T300">Назавтра чангит приехал, привез с собой ту женщину.</ta>
            <ta e="T315" id="Seg_5128" s="T306">Как только они зашли в чум, нганасанин перерезал лямки на упряжке чангита.</ta>
            <ta e="T319" id="Seg_5129" s="T315">Сам умчался, посадив свою жену на сани.</ta>
            <ta e="T323" id="Seg_5130" s="T319">Чангит пешим погнался за ними.</ta>
            <ta e="T327" id="Seg_5131" s="T323">Он прострелил нганасанину мышцы [ног].</ta>
            <ta e="T338" id="Seg_5132" s="T327">Немного погодя нганасанин двинулся навстречу чангиту, тот выпустил [стрелу] мимо его уха.</ta>
            <ta e="T344" id="Seg_5133" s="T338">Чангит выпустил все свои стрелы и стал убегать.</ta>
            <ta e="T354" id="Seg_5134" s="T344">Нганасанин с эвенком за ним, настигли и разбили ему голову.</ta>
            <ta e="T362" id="Seg_5135" s="T354">После этого эвенк женился, из-за оленей чангитов, они зажили в богатстве и довольстве.</ta>
            <ta e="T369" id="Seg_5136" s="T362">Вот так эвенки жили в мире с нганасанами.</ta>
            <ta e="T377" id="Seg_5137" s="T369">Нганасаны после этого, опасаясь, перестали кочевать в сторону Камня.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr">
            <ta e="T9" id="Seg_5138" s="T0">Давным-давно нганасаны жили у хребта Кэрикэ, на Есее — чангиты, на Камне — эвенки.</ta>
            <ta e="T16" id="Seg_5139" s="T9">Как-то у одного нганасанина чангиты украли жену.</ta>
            <ta e="T24" id="Seg_5140" s="T16">Когда муж ушел на охоту за дикими оленями, эта женщина сидела за шитьем.</ta>
            <ta e="T28" id="Seg_5141" s="T24">Вот когда так сидела, залаяли собаки.</ta>
            <ta e="T35" id="Seg_5142" s="T28">"На хозяина собаки не лаяли бы", — подумала она.</ta>
            <ta e="T49" id="Seg_5143" s="T35">Как только приготовилась развести очаг, зашли трое, завернули женщину в материю и взяли с собой.</ta>
            <ta e="T62" id="Seg_5144" s="T49">Вечером приходит нганасанин: собаки лежат на привязи, чум насквозь промерз, никого нет, темным-темно.</ta>
            <ta e="T69" id="Seg_5145" s="T62">"Грех-то какой, жена куда могла уйти?" — опечалился он.</ta>
            <ta e="T75" id="Seg_5146" s="T69">Утром, как только рассвело, он пошел пригнать свое стадо.</ta>
            <ta e="T79" id="Seg_5147" s="T75">Все олени, оказывается, были на месте.</ta>
            <ta e="T85" id="Seg_5148" s="T79">"Откочую я к [знакомому] эвенку, что мне одному делать?</ta>
            <ta e="T89" id="Seg_5149" s="T85">Хоть посоветуюсь" — так подумал.</ta>
            <ta e="T104" id="Seg_5150" s="T89">Одинокому человеку долго ли собраться — прикочевал к эвенку, оказывается, тот ушел проверять сети, дома была только его мать.</ta>
            <ta e="T108" id="Seg_5151" s="T104">Эвенк, оказывается, не имел жены.</ta>
            <ta e="T110" id="Seg_5152" s="T108">Он пришел вечером.</ta>
            <ta e="T115" id="Seg_5153" s="T110">Бедный нганасанин рассказал, что исчезла его жена.</ta>
            <ta e="T124" id="Seg_5154" s="T115">— Ну, кто же мог сделать? Вчера ко мне заходили трое мужчин, </ta>
            <ta e="T131" id="Seg_5155" s="T124">на вид не очень хорошие люди, видимо, чангиты.</ta>
            <ta e="T134" id="Seg_5156" s="T131">Что им с меня взять?!</ta>
            <ta e="T146" id="Seg_5157" s="T134">Ближние соседи тоже рассказывали, что они крадут женщин, — сказал эвенк.</ta>
            <ta e="T152" id="Seg_5158" s="T146">— Насколько ты ловок как косун? — спрашивает эвенк у друга.</ta>
            <ta e="T158" id="Seg_5159" s="T152">— Раз жив, наверно, не такой уж слабый косун, — сказал нганасанин.</ta>
            <ta e="T164" id="Seg_5160" s="T158">— Тогда я испытаю твое умение.</ta>
            <ta e="T171" id="Seg_5161" s="T164">Я буду стрелять в тебя с расстояния трех собачьих нарт.</ta>
            <ta e="T179" id="Seg_5162" s="T171">Эвенк выстрелил три раза, пострелил только край его подола.</ta>
            <ta e="T183" id="Seg_5163" s="T179">— Теперь ты стреляй, — сказал [эвенк].</ta>
            <ta e="T186" id="Seg_5164" s="T183">Нганасанин выстрелил — не попал.</ta>
            <ta e="T188" id="Seg_5165" s="T186">— Ну, хорошо.</ta>
            <ta e="T192" id="Seg_5166" s="T188">Ты, оказывается, [настоящий] косун, — похвалил эвенк.</ta>
            <ta e="T194" id="Seg_5167" s="T192">Назавтра отправились в путь.</ta>
            <ta e="T197" id="Seg_5168" s="T194">Перевалили через три горных хребта.</ta>
            <ta e="T204" id="Seg_5169" s="T197">— Вот я переодену мать в нганасанскую одежду и поеду к ним.</ta>
            <ta e="T207" id="Seg_5170" s="T204">Я их обману.</ta>
            <ta e="T218" id="Seg_5171" s="T207">Скажу: эта женщина — моя жена, она старшая сестра той женщины, которую вы взяли.</ta>
            <ta e="T224" id="Seg_5172" s="T218">А ты назовешься братом моей жены.</ta>
            <ta e="T229" id="Seg_5173" s="T224">Эвенк с матерью уехали.</ta>
            <ta e="T234" id="Seg_5174" s="T229">Подъехали и видят, стоят семь чумов.</ta>
            <ta e="T239" id="Seg_5175" s="T234">Вокруг них танцуют люди.</ta>
            <ta e="T244" id="Seg_5176" s="T239">"Кажется, у них свадьба", — подумали.</ta>
            <ta e="T251" id="Seg_5177" s="T244">Эвенк подхватил одного человека и стал танцевать со всеми.</ta>
            <ta e="T254" id="Seg_5178" s="T251">Танцуя, спросил: </ta>
            <ta e="T262" id="Seg_5179" s="T254">— Где здесь чум старшего, там должна быть жена нганасанина.</ta>
            <ta e="T268" id="Seg_5180" s="T262">Тот человек ответил сердито-пресердито и указал на чум.</ta>
            <ta e="T276" id="Seg_5181" s="T268">Эвенк зашел — сидит женщина, глаза от слез стали размером с ковш.</ta>
            <ta e="T282" id="Seg_5182" s="T276">Эвенк сразу шагнул в сторону самого старшего [из сидящих]: </ta>
            <ta e="T288" id="Seg_5183" s="T282">— Ты взял в жену сестру моей жены.</ta>
            <ta e="T293" id="Seg_5184" s="T288">Моя жена хочет встретиться с сестрой.</ta>
            <ta e="T300" id="Seg_5185" s="T293">— Ну ладно, завтра поедем на встречу, — сказал чангит.</ta>
            <ta e="T306" id="Seg_5186" s="T300">Назавтра чангит приехал, привез с собой ту женщину.</ta>
            <ta e="T315" id="Seg_5187" s="T306">Как только они зашли в чум, нганасанин перерезал лямки на упряжке чангита.</ta>
            <ta e="T319" id="Seg_5188" s="T315">Сам умчался, посадив свою жену на сани.</ta>
            <ta e="T323" id="Seg_5189" s="T319">Чангит пешим погнался за ними, </ta>
            <ta e="T327" id="Seg_5190" s="T323">прострелил нганасанину мышцы ног.</ta>
            <ta e="T338" id="Seg_5191" s="T327">Немного погодя нганасанин двинулся навстречу чангиту, тот бросил копье — [пролетело] мимо уха.</ta>
            <ta e="T344" id="Seg_5192" s="T338">Чангит выпустил все свои стрелы и стал убегать.</ta>
            <ta e="T354" id="Seg_5193" s="T344">Нганасанин с эвенком за ним, настигли и разбили ему голову.</ta>
            <ta e="T362" id="Seg_5194" s="T354">После этого эвенк [тоже] женился. Ваяв оленей чангитов, они зажили в богатстве и довольстве.</ta>
            <ta e="T369" id="Seg_5195" s="T362">Вот так эвенки жили в мире с нганасанами.</ta>
            <ta e="T377" id="Seg_5196" s="T369">Нганасаны после этого, опасаясь, перестали кочевать в сторону Камня.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
