<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID643B1164-EE2F-C79E-C388-97F4A8AE2D03">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>MiPP_199X_OldManButterfly_flk</transcription-name>
         <referenced-file url="MiPP_1996_OldManButterfly_flk.wav" />
         <referenced-file url="MiPP_1996_OldManButterfly_flk.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\flk\MiPP_1996_OldManButterfly_flk\MiPP_1996_OldManButterfly_flk.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1186</ud-information>
            <ud-information attribute-name="# HIAT:w">757</ud-information>
            <ud-information attribute-name="# e">771</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">14</ud-information>
            <ud-information attribute-name="# HIAT:u">104</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="MiPP">
            <abbreviation>MiPP</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.503" type="appl" />
         <tli id="T1" time="1.1340000000000003" type="appl" />
         <tli id="T2" time="1.7639999999999958" type="appl" />
         <tli id="T3" time="2.394999999999996" type="appl" />
         <tli id="T4" time="3.0259999999999962" type="appl" />
         <tli id="T5" time="3.656000000000006" type="appl" />
         <tli id="T6" time="4.287000000000006" type="appl" />
         <tli id="T7" time="4.9140000000000015" type="appl" />
         <tli id="T8" time="5.540000000000006" type="appl" />
         <tli id="T9" time="6.167000000000002" type="appl" />
         <tli id="T10" time="6.793000000000006" type="appl" />
         <tli id="T11" time="7.37333042055927" />
         <tli id="T12" time="7.7450000000000045" type="appl" />
         <tli id="T13" time="8.069999999999993" type="appl" />
         <tli id="T14" time="8.394999999999996" type="appl" />
         <tli id="T15" time="8.719999999999999" type="appl" />
         <tli id="T16" time="9.045000000000002" type="appl" />
         <tli id="T17" time="9.370000000000005" type="appl" />
         <tli id="T18" time="9.694999999999993" type="appl" />
         <tli id="T19" time="10.019999999999996" type="appl" />
         <tli id="T20" time="10.873000000000005" type="appl" />
         <tli id="T21" time="11.725999999999999" type="appl" />
         <tli id="T22" time="12.579999999999998" type="appl" />
         <tli id="T23" time="13.433000000000007" type="appl" />
         <tli id="T24" time="14.286000000000001" type="appl" />
         <tli id="T25" time="14.802999999999997" type="appl" />
         <tli id="T26" time="15.320999999999998" type="appl" />
         <tli id="T27" time="15.837999999999994" type="appl" />
         <tli id="T28" time="16.355999999999995" type="appl" />
         <tli id="T29" time="16.873000000000005" type="appl" />
         <tli id="T30" time="17.39" type="appl" />
         <tli id="T31" time="17.908" type="appl" />
         <tli id="T32" time="18.424999999999997" type="appl" />
         <tli id="T33" time="18.941999999999993" type="appl" />
         <tli id="T35" time="19.977000000000004" type="appl" />
         <tli id="T36" time="20.495000000000005" type="appl" />
         <tli id="T37" time="21.012" type="appl" />
         <tli id="T38" time="21.528999999999996" type="appl" />
         <tli id="T39" time="22.046999999999997" type="appl" />
         <tli id="T40" time="22.563999999999993" type="appl" />
         <tli id="T41" time="23.081999999999994" type="appl" />
         <tli id="T42" time="23.965661414280405" />
         <tli id="T43" time="24.149" type="appl" />
         <tli id="T44" time="24.698999999999998" type="appl" />
         <tli id="T46" time="25.799000000000007" type="appl" />
         <tli id="T47" time="26.349999999999994" type="appl" />
         <tli id="T48" time="26.900000000000006" type="appl" />
         <tli id="T49" time="27.450000000000003" type="appl" />
         <tli id="T50" time="28.0" type="appl" />
         <tli id="T51" time="28.549999999999997" type="appl" />
         <tli id="T52" time="29.099999999999994" type="appl" />
         <tli id="T53" time="29.739000000000004" type="appl" />
         <tli id="T54" time="30.378" type="appl" />
         <tli id="T55" time="31.018" type="appl" />
         <tli id="T56" time="31.997000991626393" />
         <tli id="T57" time="32.403999999999996" type="appl" />
         <tli id="T58" time="33.51276683414726" />
         <tli id="T59" time="33.803" type="appl" />
         <tli id="T60" time="34.455" type="appl" />
         <tli id="T61" time="35.108000000000004" type="appl" />
         <tli id="T62" time="35.760000000000005" type="appl" />
         <tli id="T63" time="36.413" type="appl" />
         <tli id="T783" time="36.739000000000004" type="intp" />
         <tli id="T64" time="37.065" type="appl" />
         <tli id="T66" time="38.370000000000005" type="appl" />
         <tli id="T67" time="38.790000000000006" type="appl" />
         <tli id="T68" time="39.209999999999994" type="appl" />
         <tli id="T69" time="39.629999999999995" type="appl" />
         <tli id="T70" time="40.05" type="appl" />
         <tli id="T71" time="40.47" type="appl" />
         <tli id="T72" time="40.89" type="appl" />
         <tli id="T73" time="41.31" type="appl" />
         <tli id="T74" time="41.730000000000004" type="appl" />
         <tli id="T75" time="42.71271283339594" />
         <tli id="T76" time="42.790000000000006" type="appl" />
         <tli id="T77" time="43.43000000000001" type="appl" />
         <tli id="T78" time="44.06999999999999" type="appl" />
         <tli id="T79" time="44.7966654450082" />
         <tli id="T80" time="45.474000000000004" type="appl" />
         <tli id="T81" time="46.238" type="appl" />
         <tli id="T82" time="47.001999999999995" type="appl" />
         <tli id="T83" time="47.766000000000005" type="appl" />
         <tli id="T84" time="48.629999999999995" type="appl" />
         <tli id="T85" time="49.494" type="appl" />
         <tli id="T86" time="50.358000000000004" type="appl" />
         <tli id="T87" time="51.004000000000005" type="appl" />
         <tli id="T88" time="51.650000000000006" type="appl" />
         <tli id="T89" time="52.297" type="appl" />
         <tli id="T90" time="52.943" type="appl" />
         <tli id="T91" time="54.052646271600295" />
         <tli id="T92" time="54.268" type="appl" />
         <tli id="T93" time="54.947" type="appl" />
         <tli id="T94" time="55.626000000000005" type="appl" />
         <tli id="T95" time="56.306" type="appl" />
         <tli id="T96" time="56.985" type="appl" />
         <tli id="T97" time="57.664" type="appl" />
         <tli id="T98" time="58.343" type="appl" />
         <tli id="T99" time="58.956999999999994" type="appl" />
         <tli id="T100" time="59.572" type="appl" />
         <tli id="T101" time="60.18600000000001" type="appl" />
         <tli id="T102" time="61.23260412753568" />
         <tli id="T103" time="61.42" type="appl" />
         <tli id="T104" time="62.040000000000006" type="appl" />
         <tli id="T105" time="62.66" type="appl" />
         <tli id="T106" time="63.28" type="appl" />
         <tli id="T107" time="64.65925068087904" />
         <tli id="T108" time="64.82" type="appl" />
         <tli id="T109" time="65.74000000000001" type="appl" />
         <tli id="T110" time="66.66" type="appl" />
         <tli id="T111" time="67.58000000000001" type="appl" />
         <tli id="T112" time="68.5" type="appl" />
         <tli id="T113" time="69.41999999999999" type="appl" />
         <tli id="T114" time="70.99921346731782" />
         <tli id="T115" time="72.17500000000001" type="appl" />
         <tli id="T116" time="74.45252653080391" />
         <tli id="T117" time="74.63900000000001" type="appl" />
         <tli id="T118" time="75.268" type="appl" />
         <tli id="T119" time="75.89699999999999" type="appl" />
         <tli id="T120" time="76.52699999999999" type="appl" />
         <tli id="T121" time="77.156" type="appl" />
         <tli id="T122" time="77.785" type="appl" />
         <tli id="T123" time="78.41399999999999" type="appl" />
         <tli id="T784" time="78.7285" type="intp" />
         <tli id="T124" time="79.043" type="appl" />
         <tli id="T125" time="79.672" type="appl" />
         <tli id="T126" time="80.30199999999999" type="appl" />
         <tli id="T128" time="81.56" type="appl" />
         <tli id="T129" time="82.137" type="appl" />
         <tli id="T130" time="82.714" type="appl" />
         <tli id="T131" time="83.291" type="appl" />
         <tli id="T132" time="83.869" type="appl" />
         <tli id="T133" time="84.446" type="appl" />
         <tli id="T134" time="85.023" type="appl" />
         <tli id="T135" time="85.95912565740045" />
         <tli id="T136" time="86.268" type="appl" />
         <tli id="T137" time="86.936" type="appl" />
         <tli id="T138" time="87.60300000000001" type="appl" />
         <tli id="T139" time="88.27099999999999" type="appl" />
         <tli id="T140" time="88.939" type="appl" />
         <tli id="T141" time="89.934" type="appl" />
         <tli id="T142" time="90.928" type="appl" />
         <tli id="T143" time="91.923" type="appl" />
         <tli id="T144" time="92.918" type="appl" />
         <tli id="T145" time="93.912" type="appl" />
         <tli id="T146" time="94.90700000000001" type="appl" />
         <tli id="T147" time="95.41900000000001" type="appl" />
         <tli id="T148" time="95.93100000000001" type="appl" />
         <tli id="T149" time="96.44300000000001" type="appl" />
         <tli id="T150" time="96.95500000000001" type="appl" />
         <tli id="T151" time="97.46700000000001" type="appl" />
         <tli id="T152" time="97.97800000000001" type="appl" />
         <tli id="T153" time="98.49000000000001" type="appl" />
         <tli id="T154" time="99.00200000000001" type="appl" />
         <tli id="T155" time="99.51400000000001" type="appl" />
         <tli id="T156" time="100.02600000000001" type="appl" />
         <tli id="T157" time="100.53800000000001" type="appl" />
         <tli id="T158" time="101.77236617205108" />
         <tli id="T159" time="101.999" type="appl" />
         <tli id="T160" time="102.94900000000001" type="appl" />
         <tli id="T161" time="103.898" type="appl" />
         <tli id="T162" time="104.55199999999999" type="appl" />
         <tli id="T163" time="105.20599999999999" type="appl" />
         <tli id="T164" time="105.86000000000001" type="appl" />
         <tli id="T165" time="106.51400000000001" type="appl" />
         <tli id="T166" time="107.168" type="appl" />
         <tli id="T167" time="107.822" type="appl" />
         <tli id="T168" time="108.476" type="appl" />
         <tli id="T169" time="109.13" type="appl" />
         <tli id="T170" time="109.656" type="appl" />
         <tli id="T171" time="110.18199999999999" type="appl" />
         <tli id="T172" time="110.708" type="appl" />
         <tli id="T173" time="111.23400000000001" type="appl" />
         <tli id="T174" time="111.75899999999999" type="appl" />
         <tli id="T175" time="112.285" type="appl" />
         <tli id="T176" time="112.811" type="appl" />
         <tli id="T177" time="113.33699999999999" type="appl" />
         <tli id="T178" time="114.09896048553719" />
         <tli id="T179" time="114.38999999999999" type="appl" />
         <tli id="T180" time="114.916" type="appl" />
         <tli id="T181" time="115.44300000000001" type="appl" />
         <tli id="T182" time="115.88333803264294" />
         <tli id="T183" time="116.358" type="appl" />
         <tli id="T184" time="116.74600000000001" type="appl" />
         <tli id="T185" time="117.13399999999999" type="appl" />
         <tli id="T186" time="117.52199999999999" type="appl" />
         <tli id="T187" time="117.91" type="appl" />
         <tli id="T188" time="118.298" type="appl" />
         <tli id="T189" time="118.686" type="appl" />
         <tli id="T190" time="119.07400000000001" type="appl" />
         <tli id="T191" time="119.46199999999999" type="appl" />
         <tli id="T192" time="119.85" type="appl" />
         <tli id="T193" time="120.238" type="appl" />
         <tli id="T194" time="120.912" type="appl" />
         <tli id="T195" time="121.58600000000001" type="appl" />
         <tli id="T196" time="122.97224173553721" />
         <tli id="T197" time="123.083" type="appl" />
         <tli id="T198" time="123.905" type="appl" />
         <tli id="T199" time="124.72800000000001" type="appl" />
         <tli id="T200" time="125.55000000000001" type="appl" />
         <tli id="T201" time="126.43300267197333" />
         <tli id="T202" time="127.10900000000001" type="appl" />
         <tli id="T203" time="127.844" type="appl" />
         <tli id="T204" time="129.13220557851236" />
         <tli id="T205" time="129.13399999999996" type="appl" />
         <tli id="T206" time="129.688" type="appl" />
         <tli id="T207" time="130.24199999999996" type="appl" />
         <tli id="T208" time="130.79699999999997" type="appl" />
         <tli id="T209" time="131.351" type="appl" />
         <tli id="T210" time="131.90499999999997" type="appl" />
         <tli id="T211" time="132.459" type="appl" />
         <tli id="T785" time="132.736" type="intp" />
         <tli id="T212" time="133.01299999999998" type="appl" />
         <tli id="T213" time="133.56799999999998" type="appl" />
         <tli id="T214" time="134.122" type="appl" />
         <tli id="T216" time="135.74550009391436" />
         <tli id="T217" time="135.80699999999996" type="appl" />
         <tli id="T218" time="136.38299999999998" type="appl" />
         <tli id="T219" time="136.95999999999998" type="appl" />
         <tli id="T220" time="137.49899999999997" type="appl" />
         <tli id="T222" time="138.577" type="appl" />
         <tli id="T223" time="139.11599999999999" type="appl" />
         <tli id="T224" time="139.7921430080766" />
         <tli id="T225" time="140.94599999999997" type="appl" />
         <tli id="T226" time="142.238" type="appl" />
         <tli id="T227" time="143.529" type="appl" />
         <tli id="T228" time="144.82" type="appl" />
         <tli id="T229" time="145.808" type="appl" />
         <tli id="T230" time="146.796" type="appl" />
         <tli id="T231" time="147.784" type="appl" />
         <tli id="T786" time="148.27799999999996" type="intp" />
         <tli id="T232" time="148.772" type="appl" />
         <tli id="T233" time="149.76" type="appl" />
         <tli id="T234" time="150.748" type="appl" />
         <tli id="T235" time="151.736" type="appl" />
         <tli id="T237" time="153.712" type="appl" />
         <tli id="T238" time="154.84199999999998" type="appl" />
         <tli id="T239" time="155.97199999999998" type="appl" />
         <tli id="T240" time="157.101" type="appl" />
         <tli id="T241" time="158.231" type="appl" />
         <tli id="T242" time="159.361" type="appl" />
         <tli id="T243" time="160.23699999999997" type="appl" />
         <tli id="T244" time="161.113" type="appl" />
         <tli id="T245" time="161.988" type="appl" />
         <tli id="T246" time="162.86399999999998" type="appl" />
         <tli id="T247" time="163.74" type="appl" />
         <tli id="T248" time="164.61599999999999" type="appl" />
         <tli id="T249" time="165.49199999999996" type="appl" />
         <tli id="T250" time="166.36699999999996" type="appl" />
         <tli id="T251" time="167.243" type="appl" />
         <tli id="T252" time="168.11899999999997" type="appl" />
         <tli id="T253" time="168.594" type="appl" />
         <tli id="T254" time="168.95665932165957" />
         <tli id="T255" time="169.57599999999996" type="appl" />
         <tli id="T256" time="170.08299999999997" type="appl" />
         <tli id="T257" time="170.589" type="appl" />
         <tli id="T258" time="171.09499999999997" type="appl" />
         <tli id="T259" time="171.60199999999998" type="appl" />
         <tli id="T260" time="172.108" type="appl" />
         <tli id="T261" time="172.615" type="appl" />
         <tli id="T262" time="173.12099999999998" type="appl" />
         <tli id="T263" time="173.627" type="appl" />
         <tli id="T264" time="174.13399999999996" type="appl" />
         <tli id="T265" time="174.53334533828973" />
         <tli id="T266" time="175.007" type="appl" />
         <tli id="T267" time="175.37399999999997" type="appl" />
         <tli id="T268" time="175.74099999999999" type="appl" />
         <tli id="T269" time="176.10899999999998" type="appl" />
         <tli id="T270" time="176.476" type="appl" />
         <tli id="T271" time="176.84299999999996" type="appl" />
         <tli id="T272" time="177.349" type="appl" />
         <tli id="T273" time="177.856" type="appl" />
         <tli id="T274" time="178.36199999999997" type="appl" />
         <tli id="T275" time="178.868" type="appl" />
         <tli id="T276" time="179.37399999999997" type="appl" />
         <tli id="T277" time="179.88" type="appl" />
         <tli id="T278" time="180.387" type="appl" />
         <tli id="T279" time="180.89299999999997" type="appl" />
         <tli id="T280" time="181.36" type="appl" />
         <tli id="T281" time="181.82599999999996" type="appl" />
         <tli id="T282" time="182.293" type="appl" />
         <tli id="T283" time="182.76" type="appl" />
         <tli id="T284" time="183.18" type="appl" />
         <tli id="T285" time="183.59999999999997" type="appl" />
         <tli id="T286" time="184.01999999999998" type="appl" />
         <tli id="T287" time="184.44" type="appl" />
         <tli id="T288" time="184.86" type="appl" />
         <tli id="T289" time="185.27999999999997" type="appl" />
         <tli id="T290" time="185.7" type="appl" />
         <tli id="T291" time="186.12" type="appl" />
         <tli id="T292" time="186.539" type="appl" />
         <tli id="T293" time="186.959" type="appl" />
         <tli id="T294" time="187.37899999999996" type="appl" />
         <tli id="T295" time="187.79899999999998" type="appl" />
         <tli id="T297" time="188.639" type="appl" />
         <tli id="T298" time="189.05899999999997" type="appl" />
         <tli id="T299" time="189.47899999999998" type="appl" />
         <tli id="T300" time="189.899" type="appl" />
         <tli id="T301" time="190.334" type="appl" />
         <tli id="T302" time="190.769" type="appl" />
         <tli id="T303" time="191.204" type="appl" />
         <tli id="T304" time="191.64" type="appl" />
         <tli id="T305" time="192.075" type="appl" />
         <tli id="T306" time="192.70516575882795" />
         <tli id="T307" time="192.995" type="appl" />
         <tli id="T308" time="193.481" type="appl" />
         <tli id="T309" time="193.966" type="appl" />
         <tli id="T310" time="194.452" type="appl" />
         <tli id="T311" time="194.937" type="appl" />
         <tli id="T312" time="195.423" type="appl" />
         <tli id="T313" time="195.90800000000002" type="appl" />
         <tli id="T314" time="196.39299999999997" type="appl" />
         <tli id="T315" time="196.87900000000002" type="appl" />
         <tli id="T316" time="197.36399999999998" type="appl" />
         <tli id="T317" time="197.85000000000002" type="appl" />
         <tli id="T318" time="198.33499999999998" type="appl" />
         <tli id="T319" time="198.82100000000003" type="appl" />
         <tli id="T320" time="199.30599999999998" type="appl" />
         <tli id="T321" time="200.04399999999998" type="appl" />
         <tli id="T322" time="200.78199999999998" type="appl" />
         <tli id="T323" time="201.4200104388001" />
         <tli id="T324" time="202.204" type="appl" />
         <tli id="T325" time="202.889" type="appl" />
         <tli id="T326" time="203.57299999999998" type="appl" />
         <tli id="T327" time="204.25799999999998" type="appl" />
         <tli id="T328" time="205.43599999999998" type="appl" />
         <tli id="T329" time="206.613" type="appl" />
         <tli id="T330" time="207.79000000000002" type="appl" />
         <tli id="T331" time="208.96800000000002" type="appl" />
         <tli id="T787" time="209.43900000000002" type="intp" />
         <tli id="T332" time="209.91000000000003" type="appl" />
         <tli id="T333" time="210.853" type="appl" />
         <tli id="T334" time="211.79500000000002" type="appl" />
         <tli id="T336" time="213.68" type="appl" />
         <tli id="T337" time="214.719" type="appl" />
         <tli id="T338" time="215.75900000000001" type="appl" />
         <tli id="T339" time="216.798" type="appl" />
         <tli id="T340" time="217.83800000000002" type="appl" />
         <tli id="T341" time="218.877" type="appl" />
         <tli id="T342" time="219.916" type="appl" />
         <tli id="T343" time="220.95600000000002" type="appl" />
         <tli id="T344" time="221.995" type="appl" />
         <tli id="T345" time="223.034" type="appl" />
         <tli id="T346" time="224.074" type="appl" />
         <tli id="T347" time="225.113" type="appl" />
         <tli id="T348" time="226.15300000000002" type="appl" />
         <tli id="T349" time="227.31199387777485" />
         <tli id="T350" time="228.067" type="appl" />
         <tli id="T351" time="228.942" type="appl" />
         <tli id="T788" time="229.3795" type="intp" />
         <tli id="T352" time="229.817" type="appl" />
         <tli id="T353" time="230.692" type="appl" />
         <tli id="T354" time="231.567" type="appl" />
         <tli id="T355" time="232.44299999999998" type="appl" />
         <tli id="T356" time="233.31799999999998" type="appl" />
         <tli id="T357" time="234.19299999999998" type="appl" />
         <tli id="T358" time="235.06799999999998" type="appl" />
         <tli id="T360" time="236.81799999999998" type="appl" />
         <tli id="T361" time="237.226" type="appl" />
         <tli id="T362" time="237.635" type="appl" />
         <tli id="T363" time="238.04399999999998" type="appl" />
         <tli id="T364" time="238.452" type="appl" />
         <tli id="T365" time="238.86" type="appl" />
         <tli id="T366" time="239.269" type="appl" />
         <tli id="T367" time="239.678" type="appl" />
         <tli id="T368" time="240.086" type="appl" />
         <tli id="T369" time="240.49400000000003" type="appl" />
         <tli id="T370" time="240.90300000000002" type="appl" />
         <tli id="T371" time="241.31099999999998" type="appl" />
         <tli id="T372" time="241.72000000000003" type="appl" />
         <tli id="T373" time="242.128" type="appl" />
         <tli id="T374" time="242.53699999999998" type="appl" />
         <tli id="T375" time="242.985" type="appl" />
         <tli id="T376" time="243.43400000000003" type="appl" />
         <tli id="T377" time="243.882" type="appl" />
         <tli id="T378" time="244.33100000000002" type="appl" />
         <tli id="T379" time="244.779" type="appl" />
         <tli id="T380" time="245.228" type="appl" />
         <tli id="T381" time="245.676" type="appl" />
         <tli id="T382" time="246.125" type="appl" />
         <tli id="T383" time="246.57299999999998" type="appl" />
         <tli id="T384" time="247.022" type="appl" />
         <tli id="T385" time="247.47000000000003" type="appl" />
         <tli id="T386" time="247.90499999999997" type="appl" />
         <tli id="T387" time="248.33999999999997" type="appl" />
         <tli id="T388" time="248.77499999999998" type="appl" />
         <tli id="T389" time="249.20999999999998" type="appl" />
         <tli id="T390" time="249.755" type="appl" />
         <tli id="T391" time="250.3" type="appl" />
         <tli id="T392" time="250.846" type="appl" />
         <tli id="T393" time="251.39100000000002" type="appl" />
         <tli id="T394" time="251.93599999999998" type="appl" />
         <tli id="T396" time="253.026" type="appl" />
         <tli id="T397" time="253.572" type="appl" />
         <tli id="T398" time="254.11700000000002" type="appl" />
         <tli id="T399" time="254.66199999999998" type="appl" />
         <tli id="T400" time="255.207" type="appl" />
         <tli id="T401" time="255.752" type="appl" />
         <tli id="T402" time="256.298" type="appl" />
         <tli id="T403" time="256.843" type="appl" />
         <tli id="T404" time="257.81811689988734" />
         <tli id="T405" time="257.878" type="appl" />
         <tli id="T406" time="258.367" type="appl" />
         <tli id="T407" time="258.856" type="appl" />
         <tli id="T408" time="259.346" type="appl" />
         <tli id="T409" time="259.835" type="appl" />
         <tli id="T410" time="260.325" type="appl" />
         <tli id="T411" time="260.814" type="appl" />
         <tli id="T789" time="261.05899999999997" type="intp" />
         <tli id="T412" time="261.304" type="appl" />
         <tli id="T413" time="261.794" type="appl" />
         <tli id="T414" time="262.283" type="appl" />
         <tli id="T416" time="263.262" type="appl" />
         <tli id="T417" time="263.814" type="appl" />
         <tli id="T418" time="264.365" type="appl" />
         <tli id="T419" time="264.917" type="appl" />
         <tli id="T420" time="265.469" type="appl" />
         <tli id="T422" time="266.572" type="appl" />
         <tli id="T423" time="267.124" type="appl" />
         <tli id="T424" time="267.676" type="appl" />
         <tli id="T425" time="268.228" type="appl" />
         <tli id="T426" time="268.779" type="appl" />
         <tli id="T427" time="269.331" type="appl" />
         <tli id="T428" time="269.883" type="appl" />
         <tli id="T429" time="270.435" type="appl" />
         <tli id="T430" time="270.986" type="appl" />
         <tli id="T431" time="271.538" type="appl" />
         <tli id="T432" time="271.985" type="appl" />
         <tli id="T433" time="272.433" type="appl" />
         <tli id="T434" time="272.88" type="appl" />
         <tli id="T435" time="273.327" type="appl" />
         <tli id="T436" time="273.774" type="appl" />
         <tli id="T437" time="274.222" type="appl" />
         <tli id="T438" time="274.669" type="appl" />
         <tli id="T439" time="275.116" type="appl" />
         <tli id="T440" time="275.564" type="appl" />
         <tli id="T441" time="276.011" type="appl" />
         <tli id="T442" time="276.458" type="appl" />
         <tli id="T443" time="276.905" type="appl" />
         <tli id="T444" time="277.353" type="appl" />
         <tli id="T445" time="277.8" type="appl" />
         <tli id="T446" time="278.83" type="appl" />
         <tli id="T447" time="279.859" type="appl" />
         <tli id="T448" time="280.321" type="appl" />
         <tli id="T449" time="280.782" type="appl" />
         <tli id="T450" time="281.244" type="appl" />
         <tli id="T451" time="281.705" type="appl" />
         <tli id="T453" time="282.629" type="appl" />
         <tli id="T454" time="283.09" type="appl" />
         <tli id="T455" time="283.552" type="appl" />
         <tli id="T456" time="284.014" type="appl" />
         <tli id="T457" time="284.475" type="appl" />
         <tli id="T458" time="284.937" type="appl" />
         <tli id="T459" time="285.398" type="appl" />
         <tli id="T460" time="285.70461988166795" />
         <tli id="T461" time="286.249" type="appl" />
         <tli id="T462" time="286.638" type="appl" />
         <tli id="T463" time="287.026" type="appl" />
         <tli id="T464" time="287.415" type="appl" />
         <tli id="T465" time="287.86" type="appl" />
         <tli id="T466" time="288.305" type="appl" />
         <tli id="T467" time="288.75" type="appl" />
         <tli id="T468" time="289.195" type="appl" />
         <tli id="T469" time="289.64" type="appl" />
         <tli id="T470" time="290.085" type="appl" />
         <tli id="T471" time="290.53" type="appl" />
         <tli id="T472" time="290.975" type="appl" />
         <tli id="T473" time="291.42" type="appl" />
         <tli id="T474" time="291.865" type="appl" />
         <tli id="T475" time="292.31" type="appl" />
         <tli id="T476" time="292.926" type="appl" />
         <tli id="T477" time="293.542" type="appl" />
         <tli id="T478" time="294.158" type="appl" />
         <tli id="T479" time="294.774" type="appl" />
         <tli id="T480" time="295.39" type="appl" />
         <tli id="T481" time="297.38455132419233" />
         <tli id="T482" time="297.449" type="appl" />
         <tli id="T483" time="298.309" type="appl" />
         <tli id="T484" time="299.17" type="appl" />
         <tli id="T485" time="300.03" type="appl" />
         <tli id="T486" time="300.87666624325726" />
         <tli id="T487" time="301.416" type="appl" />
         <tli id="T488" time="301.942" type="appl" />
         <tli id="T489" time="302.468" type="appl" />
         <tli id="T490" time="302.994" type="appl" />
         <tli id="T491" time="303.44667719977855" />
         <tli id="T492" time="304.162" type="appl" />
         <tli id="T493" time="304.804" type="appl" />
         <tli id="T494" time="305.446" type="appl" />
         <tli id="T495" time="306.45783140026293" />
         <tli id="T496" time="306.74" type="appl" />
         <tli id="T497" time="307.393" type="appl" />
         <tli id="T498" time="308.045" type="appl" />
         <tli id="T499" time="308.697" type="appl" />
         <tli id="T500" time="309.35" type="appl" />
         <tli id="T501" time="310.002" type="appl" />
         <tli id="T502" time="310.588" type="appl" />
         <tli id="T503" time="311.174" type="appl" />
         <tli id="T504" time="311.76" type="appl" />
         <tli id="T505" time="312.129" type="appl" />
         <tli id="T506" time="312.498" type="appl" />
         <tli id="T507" time="312.867" type="appl" />
         <tli id="T508" time="313.236" type="appl" />
         <tli id="T509" time="313.605" type="appl" />
         <tli id="T510" time="313.975" type="appl" />
         <tli id="T511" time="314.344" type="appl" />
         <tli id="T512" time="314.713" type="appl" />
         <tli id="T513" time="315.082" type="appl" />
         <tli id="T514" time="315.451" type="appl" />
         <tli id="T515" time="315.82" type="appl" />
         <tli id="T516" time="316.287" type="appl" />
         <tli id="T517" time="316.753" type="appl" />
         <tli id="T518" time="317.22" type="appl" />
         <tli id="T519" time="317.687" type="appl" />
         <tli id="T520" time="318.153" type="appl" />
         <tli id="T521" time="318.4933336723666" />
         <tli id="T522" time="319.08" type="appl" />
         <tli id="T523" time="319.541" type="appl" />
         <tli id="T524" time="320.41774945999254" />
         <tli id="T525" time="320.721" type="appl" />
         <tli id="T526" time="321.44" type="appl" />
         <tli id="T527" time="322.086671955616" />
         <tli id="T528" time="322.77" type="appl" />
         <tli id="T529" time="323.379" type="appl" />
         <tli id="T530" time="323.989" type="appl" />
         <tli id="T531" time="324.599" type="appl" />
         <tli id="T532" time="325.208" type="appl" />
         <tli id="T533" time="325.818" type="appl" />
         <tli id="T534" time="326.322" type="appl" />
         <tli id="T535" time="326.827" type="appl" />
         <tli id="T536" time="327.331" type="appl" />
         <tli id="T537" time="327.835" type="appl" />
         <tli id="T538" time="328.34" type="appl" />
         <tli id="T539" time="328.844" type="appl" />
         <tli id="T540" time="329.348" type="appl" />
         <tli id="T541" time="329.853" type="appl" />
         <tli id="T542" time="330.357" type="appl" />
         <tli id="T543" time="331.149" type="appl" />
         <tli id="T544" time="331.941" type="appl" />
         <tli id="T545" time="332.732" type="appl" />
         <tli id="T546" time="333.59066693083133" />
         <tli id="T547" time="334.159" type="appl" />
         <tli id="T790" time="334.476" type="intp" />
         <tli id="T548" time="334.793" type="appl" />
         <tli id="T549" time="335.428" type="appl" />
         <tli id="T550" time="336.063" type="appl" />
         <tli id="T551" time="336.698" type="appl" />
         <tli id="T552" time="337.332" type="appl" />
         <tli id="T553" time="337.967" type="appl" />
         <tli id="T554" time="338.602" type="appl" />
         <tli id="T555" time="339.236" type="appl" />
         <tli id="T556" time="339.871" type="appl" />
         <tli id="T558" time="341.141" type="appl" />
         <tli id="T560" time="342.4433233018928" />
         <tli id="T561" time="343.088" type="appl" />
         <tli id="T562" time="343.765" type="appl" />
         <tli id="T563" time="344.443" type="appl" />
         <tli id="T564" time="345.121" type="appl" />
         <tli id="T565" time="345.798" type="appl" />
         <tli id="T566" time="346.476" type="appl" />
         <tli id="T567" time="347.153" type="appl" />
         <tli id="T568" time="347.831" type="appl" />
         <tli id="T569" time="348.509" type="appl" />
         <tli id="T570" time="349.186" type="appl" />
         <tli id="T571" time="349.864" type="appl" />
         <tli id="T572" time="350.594" type="appl" />
         <tli id="T573" time="351.323" type="appl" />
         <tli id="T574" time="352.053" type="appl" />
         <tli id="T575" time="352.523" type="appl" />
         <tli id="T576" time="352.994" type="appl" />
         <tli id="T577" time="353.464" type="appl" />
         <tli id="T578" time="353.934" type="appl" />
         <tli id="T579" time="354.405" type="appl" />
         <tli id="T580" time="354.875" type="appl" />
         <tli id="T581" time="355.345" type="appl" />
         <tli id="T582" time="355.816" type="appl" />
         <tli id="T583" time="356.286" type="appl" />
         <tli id="T584" time="356.756" type="appl" />
         <tli id="T585" time="357.226" type="appl" />
         <tli id="T586" time="357.697" type="appl" />
         <tli id="T587" time="358.167" type="appl" />
         <tli id="T588" time="358.637" type="appl" />
         <tli id="T589" time="359.108" type="appl" />
         <tli id="T590" time="359.578" type="appl" />
         <tli id="T592" time="360.519" type="appl" />
         <tli id="T593" time="360.989" type="appl" />
         <tli id="T594" time="361.459" type="appl" />
         <tli id="T595" time="361.93" type="appl" />
         <tli id="T596" time="362.320003507114" />
         <tli id="T597" time="362.772" type="appl" />
         <tli id="T598" time="363.144" type="appl" />
         <tli id="T599" time="363.516" type="appl" />
         <tli id="T600" time="363.888" type="appl" />
         <tli id="T601" time="364.26" type="appl" />
         <tli id="T602" time="364.632" type="appl" />
         <tli id="T603" time="365.004" type="appl" />
         <tli id="T604" time="365.364" type="appl" />
         <tli id="T605" time="365.725" type="appl" />
         <tli id="T606" time="366.086" type="appl" />
         <tli id="T607" time="366.446" type="appl" />
         <tli id="T608" time="366.806" type="appl" />
         <tli id="T610" time="367.528" type="appl" />
         <tli id="T611" time="367.888" type="appl" />
         <tli id="T612" time="368.248" type="appl" />
         <tli id="T613" time="368.609" type="appl" />
         <tli id="T614" time="368.97" type="appl" />
         <tli id="T615" time="369.4699875806526" />
         <tli id="T616" time="369.864" type="appl" />
         <tli id="T617" time="370.398" type="appl" />
         <tli id="T618" time="370.932" type="appl" />
         <tli id="T619" time="371.466" type="appl" />
         <tli id="T620" time="372.16666446035197" />
         <tli id="T621" time="372.745" type="appl" />
         <tli id="T622" time="373.49" type="appl" />
         <tli id="T623" time="374.236" type="appl" />
         <tli id="T624" time="375.81075765401954" />
         <tli id="T625" time="375.83237882700973" type="intp" />
         <tli id="T626" time="375.854" type="appl" />
         <tli id="T627" time="376.291" type="appl" />
         <tli id="T628" time="376.728" type="appl" />
         <tli id="T629" time="377.165" type="appl" />
         <tli id="T631" time="378.038" type="appl" />
         <tli id="T632" time="378.475" type="appl" />
         <tli id="T633" time="378.912" type="appl" />
         <tli id="T634" time="379.348" type="appl" />
         <tli id="T635" time="379.785" type="appl" />
         <tli id="T636" time="380.222" type="appl" />
         <tli id="T637" time="380.659" type="appl" />
         <tli id="T638" time="381.095" type="appl" />
         <tli id="T639" time="381.532" type="appl" />
         <tli id="T640" time="382.07" type="appl" />
         <tli id="T641" time="382.609" type="appl" />
         <tli id="T642" time="383.148" type="appl" />
         <tli id="T643" time="383.686" type="appl" />
         <tli id="T644" time="384.224" type="appl" />
         <tli id="T645" time="384.763" type="appl" />
         <tli id="T646" time="385.302" type="appl" />
         <tli id="T647" time="385.54669009053197" />
         <tli id="T648" time="386.422" type="appl" />
         <tli id="T649" time="387.003" type="appl" />
         <tli id="T650" time="387.585" type="appl" />
         <tli id="T651" time="388.166" type="appl" />
         <tli id="T652" time="389.2573453935011" />
         <tli id="T653" time="389.26" type="appl" />
         <tli id="T654" time="389.773" type="appl" />
         <tli id="T655" time="390.285" type="appl" />
         <tli id="T656" time="390.798" type="appl" />
         <tli id="T657" time="391.31" type="appl" />
         <tli id="T658" time="391.823" type="appl" />
         <tli id="T659" time="392.335" type="appl" />
         <tli id="T660" time="392.847" type="appl" />
         <tli id="T661" time="393.36" type="appl" />
         <tli id="T662" time="393.872" type="appl" />
         <tli id="T663" time="394.385" type="appl" />
         <tli id="T664" time="394.897" type="appl" />
         <tli id="T665" time="395.41" type="appl" />
         <tli id="T666" time="395.9886600494958" />
         <tli id="T667" time="396.311" type="appl" />
         <tli id="T668" time="396.7" type="appl" />
         <tli id="T669" time="397.089" type="appl" />
         <tli id="T670" time="397.479" type="appl" />
         <tli id="T671" time="397.868" type="appl" />
         <tli id="T672" time="398.257" type="appl" />
         <tli id="T673" time="398.646" type="appl" />
         <tli id="T791" time="398.8405" type="intp" />
         <tli id="T674" time="399.035" type="appl" />
         <tli id="T675" time="399.424" type="appl" />
         <tli id="T676" time="399.813" type="appl" />
         <tli id="T677" time="400.203" type="appl" />
         <tli id="T678" time="400.592" type="appl" />
         <tli id="T680" time="401.37" type="appl" />
         <tli id="T792" time="401.679" type="intp" />
         <tli id="T681" time="401.988" type="appl" />
         <tli id="T682" time="402.605" type="appl" />
         <tli id="T683" time="403.223" type="appl" />
         <tli id="T684" time="403.84" type="appl" />
         <tli id="T685" time="404.458" type="appl" />
         <tli id="T686" time="405.076" type="appl" />
         <tli id="T688" time="406.311" type="appl" />
         <tli id="T689" time="406.966" type="appl" />
         <tli id="T690" time="407.621" type="appl" />
         <tli id="T691" time="408.276" type="appl" />
         <tli id="T692" time="408.931" type="appl" />
         <tli id="T693" time="409.342" type="appl" />
         <tli id="T694" time="409.754" type="appl" />
         <tli id="T695" time="410.165" type="appl" />
         <tli id="T696" time="410.576" type="appl" />
         <tli id="T697" time="410.987" type="appl" />
         <tli id="T698" time="411.399" type="appl" />
         <tli id="T699" time="411.77669237877706" />
         <tli id="T700" time="412.174" type="appl" />
         <tli id="T701" time="412.538" type="appl" />
         <tli id="T702" time="412.901" type="appl" />
         <tli id="T703" time="413.265" type="appl" />
         <tli id="T704" time="413.629" type="appl" />
         <tli id="T705" time="413.992" type="appl" />
         <tli id="T706" time="414.356" type="appl" />
         <tli id="T707" time="414.72" type="appl" />
         <tli id="T708" time="415.17" type="appl" />
         <tli id="T709" time="415.62" type="appl" />
         <tli id="T710" time="416.07" type="appl" />
         <tli id="T711" time="416.52" type="appl" />
         <tli id="T712" time="416.91" type="appl" />
         <tli id="T713" time="417.3" type="appl" />
         <tli id="T714" time="417.689" type="appl" />
         <tli id="T715" time="418.079" type="appl" />
         <tli id="T716" time="418.542329749975" />
         <tli id="T717" time="418.991" type="appl" />
         <tli id="T718" time="419.513" type="appl" />
         <tli id="T719" time="420.035" type="appl" />
         <tli id="T720" time="420.557" type="appl" />
         <tli id="T721" time="421.079" type="appl" />
         <tli id="T722" time="421.601" type="appl" />
         <tli id="T723" time="422.123" type="appl" />
         <tli id="T724" time="422.604" type="appl" />
         <tli id="T725" time="423.085" type="appl" />
         <tli id="T726" time="423.566" type="appl" />
         <tli id="T727" time="424.047" type="appl" />
         <tli id="T728" time="424.528" type="appl" />
         <tli id="T729" time="425.009" type="appl" />
         <tli id="T730" time="425.49" type="appl" />
         <tli id="T731" time="426.076" type="appl" />
         <tli id="T732" time="426.661" type="appl" />
         <tli id="T733" time="427.247" type="appl" />
         <tli id="T734" time="427.832" type="appl" />
         <tli id="T735" time="428.418" type="appl" />
         <tli id="T736" time="429.003" type="appl" />
         <tli id="T737" time="429.589" type="appl" />
         <tli id="T738" time="430.174" type="appl" />
         <tli id="T739" time="430.6933521773455" />
         <tli id="T740" time="431.162" type="appl" />
         <tli id="T741" time="431.564" type="appl" />
         <tli id="T742" time="431.967" type="appl" />
         <tli id="T743" time="432.369" type="appl" />
         <tli id="T744" time="432.771" type="appl" />
         <tli id="T745" time="433.173" type="appl" />
         <tli id="T746" time="433.575" type="appl" />
         <tli id="T747" time="433.977" type="appl" />
         <tli id="T748" time="434.38" type="appl" />
         <tli id="T749" time="434.782" type="appl" />
         <tli id="T750" time="435.184" type="appl" />
         <tli id="T751" time="435.586" type="appl" />
         <tli id="T752" time="435.988" type="appl" />
         <tli id="T753" time="436.391" type="appl" />
         <tli id="T754" time="436.793" type="appl" />
         <tli id="T755" time="437.195" type="appl" />
         <tli id="T756" time="437.655" type="appl" />
         <tli id="T757" time="438.115" type="appl" />
         <tli id="T758" time="438.575" type="appl" />
         <tli id="T759" time="439.035" type="appl" />
         <tli id="T760" time="439.495" type="appl" />
         <tli id="T761" time="439.955" type="appl" />
         <tli id="T762" time="440.415" type="appl" />
         <tli id="T763" time="440.875" type="appl" />
         <tli id="T764" time="441.335" type="appl" />
         <tli id="T765" time="441.795" type="appl" />
         <tli id="T766" time="442.32" type="appl" />
         <tli id="T767" time="442.845" type="appl" />
         <tli id="T768" time="443.37" type="appl" />
         <tli id="T769" time="443.895" type="appl" />
         <tli id="T770" time="444.42" type="appl" />
         <tli id="T771" time="445.024" type="appl" />
         <tli id="T772" time="445.627" type="appl" />
         <tli id="T773" time="446.231" type="appl" />
         <tli id="T774" time="446.835" type="appl" />
         <tli id="T775" time="447.438" type="appl" />
         <tli id="T776" time="448.042" type="appl" />
         <tli id="T777" time="448.646" type="appl" />
         <tli id="T778" time="449.249" type="appl" />
         <tli id="T779" time="449.85299999999995" type="appl" />
         <tli id="T780" time="450.457" type="appl" />
         <tli id="T781" time="451.05999999999995" type="appl" />
         <tli id="T782" time="451.664" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="MiPP"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T782" id="Seg_0" n="sc" s="T0">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Lörü͡önü</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">gɨtta</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">Tanʼaktaːk</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">ogonnʼor</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">olorbuttar</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">ühü</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_23" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">ɨraːktɨː</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">olorollor</ts>
                  <nts id="Seg_29" n="HIAT:ip">,</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">beje</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">bejelerin</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">bilsiheller</ts>
                  <nts id="Seg_39" n="HIAT:ip">.</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_42" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">Biːrgehe</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_46" n="HIAT:ip">–</nts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">ol</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">di͡ek</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_55" n="HIAT:w" s="T14">dʼi͡eleːk</ts>
                  <nts id="Seg_56" n="HIAT:ip">,</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">biːrgehe</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_61" n="HIAT:ip">–</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">ba</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">di͡ek</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_70" n="HIAT:w" s="T18">dʼi͡eleːk</ts>
                  <nts id="Seg_71" n="HIAT:ip">.</nts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_74" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_76" n="HIAT:w" s="T19">Oloronnor</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_79" n="HIAT:w" s="T20">ikki͡ennere</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_82" n="HIAT:w" s="T21">körsübütter</ts>
                  <nts id="Seg_83" n="HIAT:ip">,</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_86" n="HIAT:w" s="T22">biːrge</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_89" n="HIAT:w" s="T23">kepsespitter</ts>
                  <nts id="Seg_90" n="HIAT:ip">.</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_93" n="HIAT:u" s="T24">
                  <nts id="Seg_94" n="HIAT:ip">"</nts>
                  <ts e="T25" id="Seg_96" n="HIAT:w" s="T24">Kaː</ts>
                  <nts id="Seg_97" n="HIAT:ip">"</nts>
                  <nts id="Seg_98" n="HIAT:ip">,</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_100" n="HIAT:ip">(</nts>
                  <ts e="T26" id="Seg_102" n="HIAT:w" s="T25">diːr</ts>
                  <nts id="Seg_103" n="HIAT:ip">)</nts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_106" n="HIAT:w" s="T26">deheller</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_109" n="HIAT:w" s="T27">Lörü͡ötün</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_112" n="HIAT:w" s="T28">gɨtta</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_115" n="HIAT:w" s="T29">Tanʼaktaːk</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_118" n="HIAT:w" s="T30">ogonnʼor</ts>
                  <nts id="Seg_119" n="HIAT:ip">,</nts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_121" n="HIAT:ip">"</nts>
                  <ts e="T32" id="Seg_123" n="HIAT:w" s="T31">ka</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_126" n="HIAT:w" s="T32">bihigi</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_128" n="HIAT:ip">(</nts>
                  <ts e="T35" id="Seg_130" n="HIAT:w" s="T33">bi-</ts>
                  <nts id="Seg_131" n="HIAT:ip">)</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_134" n="HIAT:w" s="T35">biːr</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_137" n="HIAT:w" s="T36">dʼɨlɨ</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_140" n="HIAT:w" s="T37">bɨhar</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_143" n="HIAT:w" s="T38">aspɨtɨn</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_146" n="HIAT:w" s="T39">biːrge</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_149" n="HIAT:w" s="T40">kolboːmmut</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_152" n="HIAT:w" s="T41">ahɨ͡ak</ts>
                  <nts id="Seg_153" n="HIAT:ip">.</nts>
                  <nts id="Seg_154" n="HIAT:ip">"</nts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_157" n="HIAT:u" s="T42">
                  <nts id="Seg_158" n="HIAT:ip">"</nts>
                  <ts e="T43" id="Seg_160" n="HIAT:w" s="T42">Baːdakpɨtɨna</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_163" n="HIAT:w" s="T43">badaga</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_165" n="HIAT:ip">(</nts>
                  <ts e="T46" id="Seg_167" n="HIAT:w" s="T44">bi-</ts>
                  <nts id="Seg_168" n="HIAT:ip">)</nts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_171" n="HIAT:w" s="T46">uhunnuk</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_174" n="HIAT:w" s="T47">oloru͡okput</ts>
                  <nts id="Seg_175" n="HIAT:ip">,</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_178" n="HIAT:w" s="T48">badaga</ts>
                  <nts id="Seg_179" n="HIAT:ip">,</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_182" n="HIAT:w" s="T49">dʼɨlbɨtɨn</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_185" n="HIAT:w" s="T50">bɨharbɨtɨgar</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_188" n="HIAT:w" s="T51">daːgɨnɨ</ts>
                  <nts id="Seg_189" n="HIAT:ip">.</nts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T56" id="Seg_192" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_194" n="HIAT:w" s="T52">Bu</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_197" n="HIAT:w" s="T53">kuhagan</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_200" n="HIAT:w" s="T54">dʼɨllar</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_203" n="HIAT:w" s="T55">baːllar</ts>
                  <nts id="Seg_204" n="HIAT:ip">.</nts>
                  <nts id="Seg_205" n="HIAT:ip">"</nts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_208" n="HIAT:u" s="T56">
                  <nts id="Seg_209" n="HIAT:ip">"</nts>
                  <ts e="T57" id="Seg_211" n="HIAT:w" s="T56">Dʼe</ts>
                  <nts id="Seg_212" n="HIAT:ip">,</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_215" n="HIAT:w" s="T57">höp</ts>
                  <nts id="Seg_216" n="HIAT:ip">.</nts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_219" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_221" n="HIAT:w" s="T58">Anɨ</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_224" n="HIAT:w" s="T59">oččogo</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_227" n="HIAT:w" s="T60">bihigi</ts>
                  <nts id="Seg_228" n="HIAT:ip">,</nts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_231" n="HIAT:w" s="T61">Lörü͡ö</ts>
                  <nts id="Seg_232" n="HIAT:ip">,</nts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_235" n="HIAT:w" s="T62">eni͡ene</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_237" n="HIAT:ip">(</nts>
                  <nts id="Seg_238" n="HIAT:ip">(</nts>
                  <ats e="T783" id="Seg_239" n="HIAT:non-pho" s="T63">…</ats>
                  <nts id="Seg_240" n="HIAT:ip">)</nts>
                  <nts id="Seg_241" n="HIAT:ip">)</nts>
                  <nts id="Seg_242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_244" n="HIAT:w" s="T783">askɨn</ts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_247" n="HIAT:w" s="T64">ahɨ͡ak</ts>
                  <nts id="Seg_248" n="HIAT:ip">.</nts>
                  <nts id="Seg_249" n="HIAT:ip">"</nts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T75" id="Seg_252" n="HIAT:u" s="T66">
                  <nts id="Seg_253" n="HIAT:ip">"</nts>
                  <ts e="T67" id="Seg_255" n="HIAT:w" s="T66">Eː</ts>
                  <nts id="Seg_256" n="HIAT:ip">,</nts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_259" n="HIAT:w" s="T67">Tanʼaktaːk</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_262" n="HIAT:w" s="T68">ogonnʼor</ts>
                  <nts id="Seg_263" n="HIAT:ip">,</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_266" n="HIAT:w" s="T69">gini</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_269" n="HIAT:w" s="T70">ol</ts>
                  <nts id="Seg_270" n="HIAT:ip">,</nts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_273" n="HIAT:w" s="T71">gini͡ene</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_276" n="HIAT:w" s="T72">dʼi͡etiger</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_279" n="HIAT:w" s="T73">barammɨt</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_282" n="HIAT:w" s="T74">ahɨ͡ak</ts>
                  <nts id="Seg_283" n="HIAT:ip">.</nts>
                  <nts id="Seg_284" n="HIAT:ip">"</nts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_287" n="HIAT:u" s="T75">
                  <nts id="Seg_288" n="HIAT:ip">"</nts>
                  <ts e="T76" id="Seg_290" n="HIAT:w" s="T75">Dʼe</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_293" n="HIAT:w" s="T76">höp</ts>
                  <nts id="Seg_294" n="HIAT:ip">,</nts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_297" n="HIAT:w" s="T77">innʼe</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_300" n="HIAT:w" s="T78">ahɨ͡ak</ts>
                  <nts id="Seg_301" n="HIAT:ip">.</nts>
                  <nts id="Seg_302" n="HIAT:ip">"</nts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_305" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_307" n="HIAT:w" s="T79">Oː</ts>
                  <nts id="Seg_308" n="HIAT:ip">,</nts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_311" n="HIAT:w" s="T80">dʼe</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_314" n="HIAT:w" s="T81">ahɨːllar</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_317" n="HIAT:w" s="T82">araj</ts>
                  <nts id="Seg_318" n="HIAT:ip">.</nts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T86" id="Seg_321" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_323" n="HIAT:w" s="T83">Lörü͡ö</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_326" n="HIAT:w" s="T84">dʼi͡etiger</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_329" n="HIAT:w" s="T85">ahɨːllar</ts>
                  <nts id="Seg_330" n="HIAT:ip">.</nts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_333" n="HIAT:u" s="T86">
                  <ts e="T87" id="Seg_335" n="HIAT:w" s="T86">Biːr</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_338" n="HIAT:w" s="T87">aŋarɨ</ts>
                  <nts id="Seg_339" n="HIAT:ip">,</nts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_342" n="HIAT:w" s="T88">dʼe</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_345" n="HIAT:w" s="T89">baranɨ͡agar</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_348" n="HIAT:w" s="T90">di͡eri</ts>
                  <nts id="Seg_349" n="HIAT:ip">.</nts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T98" id="Seg_352" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_354" n="HIAT:w" s="T91">Dʼe</ts>
                  <nts id="Seg_355" n="HIAT:ip">,</nts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_358" n="HIAT:w" s="T92">ahaːnnar</ts>
                  <nts id="Seg_359" n="HIAT:ip">,</nts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_362" n="HIAT:w" s="T93">ontularɨn</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_365" n="HIAT:w" s="T94">astara</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_368" n="HIAT:w" s="T95">baranna</ts>
                  <nts id="Seg_369" n="HIAT:ip">,</nts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_372" n="HIAT:w" s="T96">Lörü͡ö</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_375" n="HIAT:w" s="T97">gi͡ene</ts>
                  <nts id="Seg_376" n="HIAT:ip">.</nts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_379" n="HIAT:u" s="T98">
                  <nts id="Seg_380" n="HIAT:ip">"</nts>
                  <ts e="T99" id="Seg_382" n="HIAT:w" s="T98">Dʼe</ts>
                  <nts id="Seg_383" n="HIAT:ip">,</nts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_386" n="HIAT:w" s="T99">Tanʼaktaːk</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_389" n="HIAT:w" s="T100">ogonnʼorgo</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_392" n="HIAT:w" s="T101">barɨ͡agɨŋ</ts>
                  <nts id="Seg_393" n="HIAT:ip">.</nts>
                  <nts id="Seg_394" n="HIAT:ip">"</nts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T107" id="Seg_397" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_399" n="HIAT:w" s="T102">Barannar</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_402" n="HIAT:w" s="T103">dʼe</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_405" n="HIAT:w" s="T104">ol</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_408" n="HIAT:w" s="T105">ahɨn</ts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_411" n="HIAT:w" s="T106">ahɨːhɨlar</ts>
                  <nts id="Seg_412" n="HIAT:ip">.</nts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_415" n="HIAT:u" s="T107">
                  <ts e="T108" id="Seg_417" n="HIAT:w" s="T107">Oː</ts>
                  <nts id="Seg_418" n="HIAT:ip">,</nts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_421" n="HIAT:w" s="T108">Tanʼaktaːk</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_424" n="HIAT:w" s="T109">ogonnʼorgo</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_427" n="HIAT:w" s="T110">bardɨlar</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_429" n="HIAT:ip">(</nts>
                  <nts id="Seg_430" n="HIAT:ip">(</nts>
                  <ats e="T112" id="Seg_431" n="HIAT:non-pho" s="T111">PAUSE</ats>
                  <nts id="Seg_432" n="HIAT:ip">)</nts>
                  <nts id="Seg_433" n="HIAT:ip">)</nts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_436" n="HIAT:w" s="T112">dʼe</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_439" n="HIAT:w" s="T113">ahɨːllar</ts>
                  <nts id="Seg_440" n="HIAT:ip">.</nts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_443" n="HIAT:u" s="T114">
                  <ts e="T115" id="Seg_445" n="HIAT:w" s="T114">Ahɨːllar-ahɨːllar</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_448" n="HIAT:w" s="T115">da</ts>
                  <nts id="Seg_449" n="HIAT:ip">.</nts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_452" n="HIAT:u" s="T116">
                  <nts id="Seg_453" n="HIAT:ip">"</nts>
                  <ts e="T117" id="Seg_455" n="HIAT:w" s="T116">Kajaː</ts>
                  <nts id="Seg_456" n="HIAT:ip">"</nts>
                  <nts id="Seg_457" n="HIAT:ip">,</nts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_460" n="HIAT:w" s="T117">biːr</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_463" n="HIAT:w" s="T118">dogoro</ts>
                  <nts id="Seg_464" n="HIAT:ip">,</nts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_466" n="HIAT:ip">"</nts>
                  <ts e="T120" id="Seg_468" n="HIAT:w" s="T119">mini͡ene</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_471" n="HIAT:w" s="T120">ahɨm</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_474" n="HIAT:w" s="T121">baranar</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_477" n="HIAT:w" s="T122">bu͡oltak</ts>
                  <nts id="Seg_478" n="HIAT:ip">"</nts>
                  <nts id="Seg_479" n="HIAT:ip">,</nts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_481" n="HIAT:ip">(</nts>
                  <nts id="Seg_482" n="HIAT:ip">(</nts>
                  <ats e="T784" id="Seg_483" n="HIAT:non-pho" s="T123">…</ats>
                  <nts id="Seg_484" n="HIAT:ip">)</nts>
                  <nts id="Seg_485" n="HIAT:ip">)</nts>
                  <nts id="Seg_486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_488" n="HIAT:w" s="T784">diːr</ts>
                  <nts id="Seg_489" n="HIAT:ip">,</nts>
                  <nts id="Seg_490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_491" n="HIAT:ip">"</nts>
                  <ts e="T125" id="Seg_493" n="HIAT:w" s="T124">en</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_496" n="HIAT:w" s="T125">dʼi͡eger</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_499" n="HIAT:w" s="T126">bar</ts>
                  <nts id="Seg_500" n="HIAT:ip">.</nts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_503" n="HIAT:u" s="T128">
                  <ts e="T129" id="Seg_505" n="HIAT:w" s="T128">Bejeŋ</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_508" n="HIAT:w" s="T129">bultanan</ts>
                  <nts id="Seg_509" n="HIAT:ip">,</nts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_512" n="HIAT:w" s="T130">min</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_515" n="HIAT:w" s="T131">aspɨn</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_518" n="HIAT:w" s="T132">barɨtɨn</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_521" n="HIAT:w" s="T133">hi͡eppin</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_524" n="HIAT:w" s="T134">bagarbappɨn</ts>
                  <nts id="Seg_525" n="HIAT:ip">.</nts>
                  <nts id="Seg_526" n="HIAT:ip">"</nts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_529" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_531" n="HIAT:w" s="T135">Eː</ts>
                  <nts id="Seg_532" n="HIAT:ip">,</nts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_535" n="HIAT:w" s="T136">dʼe</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_538" n="HIAT:w" s="T137">baran</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_541" n="HIAT:w" s="T138">kaːlla</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_544" n="HIAT:w" s="T139">dogoro</ts>
                  <nts id="Seg_545" n="HIAT:ip">.</nts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T146" id="Seg_548" n="HIAT:u" s="T140">
                  <ts e="T141" id="Seg_550" n="HIAT:w" s="T140">Kajaː</ts>
                  <nts id="Seg_551" n="HIAT:ip">,</nts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_554" n="HIAT:w" s="T141">baran</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_557" n="HIAT:w" s="T142">tu͡ogu</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_560" n="HIAT:w" s="T143">hi͡egej</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_563" n="HIAT:w" s="T144">dʼi͡etiger</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_566" n="HIAT:w" s="T145">Löːrü͡ö</ts>
                  <nts id="Seg_567" n="HIAT:ip">?</nts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T158" id="Seg_570" n="HIAT:u" s="T146">
                  <ts e="T147" id="Seg_572" n="HIAT:w" s="T146">Barbɨta</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_575" n="HIAT:w" s="T147">dʼi͡etiger</ts>
                  <nts id="Seg_576" n="HIAT:ip">,</nts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_579" n="HIAT:w" s="T148">tu͡oga</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_582" n="HIAT:w" s="T149">da</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_585" n="HIAT:w" s="T150">hu͡ok</ts>
                  <nts id="Seg_586" n="HIAT:ip">,</nts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_589" n="HIAT:w" s="T151">tu͡ok</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_592" n="HIAT:w" s="T152">ere</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_595" n="HIAT:w" s="T153">kačča</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_598" n="HIAT:w" s="T154">bɨlčɨŋkaːnɨ</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_601" n="HIAT:w" s="T155">bulunan</ts>
                  <nts id="Seg_602" n="HIAT:ip">,</nts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_605" n="HIAT:w" s="T156">ontutun</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_608" n="HIAT:w" s="T157">kü͡östenne</ts>
                  <nts id="Seg_609" n="HIAT:ip">.</nts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_612" n="HIAT:u" s="T158">
                  <ts e="T159" id="Seg_614" n="HIAT:w" s="T158">Kü͡östenen</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_617" n="HIAT:w" s="T159">oloror</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_620" n="HIAT:w" s="T160">araj</ts>
                  <nts id="Seg_621" n="HIAT:ip">.</nts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T169" id="Seg_624" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_626" n="HIAT:w" s="T161">Kaː</ts>
                  <nts id="Seg_627" n="HIAT:ip">,</nts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_630" n="HIAT:w" s="T162">onton</ts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_633" n="HIAT:w" s="T163">körbüte</ts>
                  <nts id="Seg_634" n="HIAT:ip">,</nts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_637" n="HIAT:w" s="T164">tugu</ts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_640" n="HIAT:w" s="T165">ere</ts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_643" n="HIAT:w" s="T166">bultammɨt</ts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_646" n="HIAT:w" s="T167">emi͡e</ts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_649" n="HIAT:w" s="T168">Löːrü͡ö</ts>
                  <nts id="Seg_650" n="HIAT:ip">.</nts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T178" id="Seg_653" n="HIAT:u" s="T169">
                  <ts e="T170" id="Seg_655" n="HIAT:w" s="T169">Onton</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_658" n="HIAT:w" s="T170">maːjdʼɨn</ts>
                  <nts id="Seg_659" n="HIAT:ip">,</nts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_662" n="HIAT:w" s="T171">Tanʼaktaːk</ts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_665" n="HIAT:w" s="T172">ogonnʼor</ts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_668" n="HIAT:w" s="T173">da</ts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_671" n="HIAT:w" s="T174">aha</ts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_674" n="HIAT:w" s="T175">barammɨt</ts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_677" n="HIAT:w" s="T176">ete</ts>
                  <nts id="Seg_678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_680" n="HIAT:w" s="T177">hopsi͡em</ts>
                  <nts id="Seg_681" n="HIAT:ip">.</nts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T182" id="Seg_684" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_686" n="HIAT:w" s="T178">Dʼe</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_689" n="HIAT:w" s="T179">oloror</ts>
                  <nts id="Seg_690" n="HIAT:ip">,</nts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_693" n="HIAT:w" s="T180">bilsibet</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_696" n="HIAT:w" s="T181">gini͡eke</ts>
                  <nts id="Seg_697" n="HIAT:ip">.</nts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T193" id="Seg_700" n="HIAT:u" s="T182">
                  <nts id="Seg_701" n="HIAT:ip">"</nts>
                  <ts e="T183" id="Seg_703" n="HIAT:w" s="T182">Dʼe</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_706" n="HIAT:w" s="T183">kelbetin</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_709" n="HIAT:w" s="T184">gini</ts>
                  <nts id="Seg_710" n="HIAT:ip">,</nts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_713" n="HIAT:w" s="T185">bejem</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_716" n="HIAT:w" s="T186">da</ts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_719" n="HIAT:w" s="T187">bultanan</ts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_722" n="HIAT:w" s="T188">oloroːn</ts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_725" n="HIAT:w" s="T189">biːr</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_728" n="HIAT:w" s="T190">tu͡okkaːn</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_731" n="HIAT:w" s="T191">eme</ts>
                  <nts id="Seg_732" n="HIAT:ip">"</nts>
                  <nts id="Seg_733" n="HIAT:ip">,</nts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_736" n="HIAT:w" s="T192">diːr</ts>
                  <nts id="Seg_737" n="HIAT:ip">.</nts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_740" n="HIAT:u" s="T193">
                  <ts e="T194" id="Seg_742" n="HIAT:w" s="T193">Lörü͡ö</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_745" n="HIAT:w" s="T194">araj</ts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_748" n="HIAT:w" s="T195">oloror</ts>
                  <nts id="Seg_749" n="HIAT:ip">.</nts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T201" id="Seg_752" n="HIAT:u" s="T196">
                  <ts e="T197" id="Seg_754" n="HIAT:w" s="T196">Bu</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_757" n="HIAT:w" s="T197">olordoguna</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_760" n="HIAT:w" s="T198">Tanʼaktaːk</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_763" n="HIAT:w" s="T199">ogonnʼor</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_766" n="HIAT:w" s="T200">kelbit</ts>
                  <nts id="Seg_767" n="HIAT:ip">.</nts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T204" id="Seg_770" n="HIAT:u" s="T201">
                  <ts e="T202" id="Seg_772" n="HIAT:w" s="T201">Iherin</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_775" n="HIAT:w" s="T202">kördö</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_778" n="HIAT:w" s="T203">dogorun</ts>
                  <nts id="Seg_779" n="HIAT:ip">.</nts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T216" id="Seg_782" n="HIAT:u" s="T204">
                  <nts id="Seg_783" n="HIAT:ip">"</nts>
                  <ts e="T205" id="Seg_785" n="HIAT:w" s="T204">Bɨː</ts>
                  <nts id="Seg_786" n="HIAT:ip">,</nts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_789" n="HIAT:w" s="T205">Tanʼaktaːk</ts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_792" n="HIAT:w" s="T206">ogonnʼor</ts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_795" n="HIAT:w" s="T207">iher</ts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_798" n="HIAT:w" s="T208">ebit</ts>
                  <nts id="Seg_799" n="HIAT:ip">"</nts>
                  <nts id="Seg_800" n="HIAT:ip">,</nts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_803" n="HIAT:w" s="T209">Lörü͡ö</ts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_806" n="HIAT:w" s="T210">diːr</ts>
                  <nts id="Seg_807" n="HIAT:ip">,</nts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_809" n="HIAT:ip">(</nts>
                  <nts id="Seg_810" n="HIAT:ip">(</nts>
                  <ats e="T785" id="Seg_811" n="HIAT:non-pho" s="T211">…</ats>
                  <nts id="Seg_812" n="HIAT:ip">)</nts>
                  <nts id="Seg_813" n="HIAT:ip">)</nts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_816" n="HIAT:w" s="T785">dʼi͡etin</ts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_819" n="HIAT:w" s="T212">katanan</ts>
                  <nts id="Seg_820" n="HIAT:ip">,</nts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_823" n="HIAT:w" s="T213">haban</ts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_826" n="HIAT:w" s="T214">keːspit</ts>
                  <nts id="Seg_827" n="HIAT:ip">.</nts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T219" id="Seg_830" n="HIAT:u" s="T216">
                  <ts e="T217" id="Seg_832" n="HIAT:w" s="T216">Dʼe</ts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_835" n="HIAT:w" s="T217">keler</ts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_838" n="HIAT:w" s="T218">araj</ts>
                  <nts id="Seg_839" n="HIAT:ip">.</nts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T224" id="Seg_842" n="HIAT:u" s="T219">
                  <ts e="T220" id="Seg_844" n="HIAT:w" s="T219">Aː</ts>
                  <nts id="Seg_845" n="HIAT:ip">,</nts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_847" n="HIAT:ip">(</nts>
                  <ts e="T222" id="Seg_849" n="HIAT:w" s="T220">Lö-</ts>
                  <nts id="Seg_850" n="HIAT:ip">)</nts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_853" n="HIAT:w" s="T222">Lörü͡ötüger</ts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_856" n="HIAT:w" s="T223">keler</ts>
                  <nts id="Seg_857" n="HIAT:ip">.</nts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T228" id="Seg_860" n="HIAT:u" s="T224">
                  <nts id="Seg_861" n="HIAT:ip">"</nts>
                  <ts e="T225" id="Seg_863" n="HIAT:w" s="T224">Lörü͡ö</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_866" n="HIAT:w" s="T225">dogordoː</ts>
                  <nts id="Seg_867" n="HIAT:ip">,</nts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_870" n="HIAT:w" s="T226">Lörü͡ö</ts>
                  <nts id="Seg_871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_873" n="HIAT:w" s="T227">dogordoː</ts>
                  <nts id="Seg_874" n="HIAT:ip">.</nts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T237" id="Seg_877" n="HIAT:u" s="T228">
                  <ts e="T229" id="Seg_879" n="HIAT:w" s="T228">En</ts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_882" n="HIAT:w" s="T229">bi͡er</ts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_885" n="HIAT:w" s="T230">eme</ts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_887" n="HIAT:ip">(</nts>
                  <nts id="Seg_888" n="HIAT:ip">(</nts>
                  <ats e="T786" id="Seg_889" n="HIAT:non-pho" s="T231">…</ats>
                  <nts id="Seg_890" n="HIAT:ip">)</nts>
                  <nts id="Seg_891" n="HIAT:ip">)</nts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_894" n="HIAT:w" s="T786">bɨldʼɨŋkaːnna</ts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_897" n="HIAT:w" s="T232">bɨragan</ts>
                  <nts id="Seg_898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_900" n="HIAT:w" s="T233">abɨraː</ts>
                  <nts id="Seg_901" n="HIAT:ip">,</nts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_904" n="HIAT:w" s="T234">Löːrü͡ö</ts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_907" n="HIAT:w" s="T235">dogordoː</ts>
                  <nts id="Seg_908" n="HIAT:ip">.</nts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T242" id="Seg_911" n="HIAT:u" s="T237">
                  <ts e="T238" id="Seg_913" n="HIAT:w" s="T237">Ardaːn</ts>
                  <nts id="Seg_914" n="HIAT:ip">,</nts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_917" n="HIAT:w" s="T238">kümmer</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_920" n="HIAT:w" s="T239">turda</ts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_923" n="HIAT:w" s="T240">dʼɨlbɨn</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_926" n="HIAT:w" s="T241">solgoː</ts>
                  <nts id="Seg_927" n="HIAT:ip">.</nts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T252" id="Seg_930" n="HIAT:u" s="T242">
                  <ts e="T243" id="Seg_932" n="HIAT:w" s="T242">Löːrü͡ö</ts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_935" n="HIAT:w" s="T243">dogordoː</ts>
                  <nts id="Seg_936" n="HIAT:ip">,</nts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_939" n="HIAT:w" s="T244">Löːrü͡ö</ts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_942" n="HIAT:w" s="T245">dogordoː</ts>
                  <nts id="Seg_943" n="HIAT:ip">,</nts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_946" n="HIAT:w" s="T246">öllüm</ts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_949" n="HIAT:w" s="T247">kaːlɨ͡am</ts>
                  <nts id="Seg_950" n="HIAT:ip">"</nts>
                  <nts id="Seg_951" n="HIAT:ip">,</nts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_954" n="HIAT:w" s="T248">biːri</ts>
                  <nts id="Seg_955" n="HIAT:ip">,</nts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_958" n="HIAT:w" s="T249">ɨllɨː</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_961" n="HIAT:w" s="T250">olorbut</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_964" n="HIAT:w" s="T251">törüt</ts>
                  <nts id="Seg_965" n="HIAT:ip">.</nts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T254" id="Seg_968" n="HIAT:u" s="T252">
                  <nts id="Seg_969" n="HIAT:ip">"</nts>
                  <ts e="T253" id="Seg_971" n="HIAT:w" s="T252">Kaː</ts>
                  <nts id="Seg_972" n="HIAT:ip">,</nts>
                  <nts id="Seg_973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_975" n="HIAT:w" s="T253">bar-bar</ts>
                  <nts id="Seg_976" n="HIAT:ip">!</nts>
                  <nts id="Seg_977" n="HIAT:ip">"</nts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T265" id="Seg_980" n="HIAT:u" s="T254">
                  <ts e="T255" id="Seg_982" n="HIAT:w" s="T254">Biːr</ts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_985" n="HIAT:w" s="T255">da</ts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_988" n="HIAT:w" s="T256">biːr</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_990" n="HIAT:ip">(</nts>
                  <ts e="T258" id="Seg_992" n="HIAT:w" s="T257">tugu</ts>
                  <nts id="Seg_993" n="HIAT:ip">)</nts>
                  <nts id="Seg_994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_996" n="HIAT:w" s="T258">bütteske</ts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_999" n="HIAT:w" s="T259">hɨstɨbɨt</ts>
                  <nts id="Seg_1000" n="HIAT:ip">,</nts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1002" n="HIAT:ip">(</nts>
                  <ts e="T261" id="Seg_1004" n="HIAT:w" s="T260">kammɨt</ts>
                  <nts id="Seg_1005" n="HIAT:ip">)</nts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_1008" n="HIAT:w" s="T261">kappɨt</ts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_1011" n="HIAT:w" s="T262">bɨlčɨŋkaːnɨ</ts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_1014" n="HIAT:w" s="T263">bɨragan</ts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_1017" n="HIAT:w" s="T264">keːste</ts>
                  <nts id="Seg_1018" n="HIAT:ip">.</nts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T271" id="Seg_1021" n="HIAT:u" s="T265">
                  <nts id="Seg_1022" n="HIAT:ip">"</nts>
                  <ts e="T266" id="Seg_1024" n="HIAT:w" s="T265">Bar</ts>
                  <nts id="Seg_1025" n="HIAT:ip">"</nts>
                  <nts id="Seg_1026" n="HIAT:ip">,</nts>
                  <nts id="Seg_1027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_1029" n="HIAT:w" s="T266">diːr</ts>
                  <nts id="Seg_1030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_1032" n="HIAT:w" s="T267">ontuta</ts>
                  <nts id="Seg_1033" n="HIAT:ip">,</nts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_1036" n="HIAT:w" s="T268">Tanʼaktaːk</ts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_1039" n="HIAT:w" s="T269">baran</ts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_1042" n="HIAT:w" s="T270">kaːlla</ts>
                  <nts id="Seg_1043" n="HIAT:ip">.</nts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T279" id="Seg_1046" n="HIAT:u" s="T271">
                  <nts id="Seg_1047" n="HIAT:ip">"</nts>
                  <nts id="Seg_1048" n="HIAT:ip">(</nts>
                  <ts e="T272" id="Seg_1050" n="HIAT:w" s="T271">Bar</ts>
                  <nts id="Seg_1051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_1053" n="HIAT:w" s="T272">dʼi͡etiger</ts>
                  <nts id="Seg_1054" n="HIAT:ip">)</nts>
                  <nts id="Seg_1055" n="HIAT:ip">,</nts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_1058" n="HIAT:w" s="T273">bar</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_1061" n="HIAT:w" s="T274">dʼi͡eger</ts>
                  <nts id="Seg_1062" n="HIAT:ip">"</nts>
                  <nts id="Seg_1063" n="HIAT:ip">,</nts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_1066" n="HIAT:w" s="T275">diːr</ts>
                  <nts id="Seg_1067" n="HIAT:ip">,</nts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1069" n="HIAT:ip">"</nts>
                  <ts e="T277" id="Seg_1071" n="HIAT:w" s="T276">bolse</ts>
                  <nts id="Seg_1072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1074" n="HIAT:w" s="T277">bi͡erbeppin</ts>
                  <nts id="Seg_1075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1077" n="HIAT:w" s="T278">eni͡eke</ts>
                  <nts id="Seg_1078" n="HIAT:ip">.</nts>
                  <nts id="Seg_1079" n="HIAT:ip">"</nts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T283" id="Seg_1082" n="HIAT:u" s="T279">
                  <ts e="T280" id="Seg_1084" n="HIAT:w" s="T279">Koː</ts>
                  <nts id="Seg_1085" n="HIAT:ip">,</nts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1088" n="HIAT:w" s="T280">baran</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1091" n="HIAT:w" s="T281">kaːlla</ts>
                  <nts id="Seg_1092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1094" n="HIAT:w" s="T282">kihi</ts>
                  <nts id="Seg_1095" n="HIAT:ip">.</nts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T300" id="Seg_1098" n="HIAT:u" s="T283">
                  <ts e="T284" id="Seg_1100" n="HIAT:w" s="T283">Baran</ts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1103" n="HIAT:w" s="T284">toholaːk</ts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1106" n="HIAT:w" s="T285">či͡egej</ts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1109" n="HIAT:w" s="T286">gini</ts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1112" n="HIAT:w" s="T287">onton</ts>
                  <nts id="Seg_1113" n="HIAT:ip">,</nts>
                  <nts id="Seg_1114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1116" n="HIAT:w" s="T288">ol-bu</ts>
                  <nts id="Seg_1117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1119" n="HIAT:w" s="T289">dʼe</ts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1122" n="HIAT:w" s="T290">kaːnɨn</ts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1124" n="HIAT:ip">(</nts>
                  <ts e="T292" id="Seg_1126" n="HIAT:w" s="T291">postu͡oj</ts>
                  <nts id="Seg_1127" n="HIAT:ip">)</nts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1130" n="HIAT:w" s="T292">postu͡oj</ts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1133" n="HIAT:w" s="T293">minin</ts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1136" n="HIAT:w" s="T294">agaj</ts>
                  <nts id="Seg_1137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1138" n="HIAT:ip">(</nts>
                  <ts e="T297" id="Seg_1140" n="HIAT:w" s="T295">kü-</ts>
                  <nts id="Seg_1141" n="HIAT:ip">)</nts>
                  <nts id="Seg_1142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1144" n="HIAT:w" s="T297">kü͡östenen</ts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1147" n="HIAT:w" s="T298">ši͡ebit</ts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1150" n="HIAT:w" s="T299">da</ts>
                  <nts id="Seg_1151" n="HIAT:ip">.</nts>
                  <nts id="Seg_1152" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T306" id="Seg_1154" n="HIAT:u" s="T300">
                  <ts e="T301" id="Seg_1156" n="HIAT:w" s="T300">Tu͡ok</ts>
                  <nts id="Seg_1157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1159" n="HIAT:w" s="T301">da</ts>
                  <nts id="Seg_1160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1162" n="HIAT:w" s="T302">bulbat</ts>
                  <nts id="Seg_1163" n="HIAT:ip">,</nts>
                  <nts id="Seg_1164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1166" n="HIAT:w" s="T303">da</ts>
                  <nts id="Seg_1167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1169" n="HIAT:w" s="T304">kanʼaːbat</ts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1172" n="HIAT:w" s="T305">daːganɨ</ts>
                  <nts id="Seg_1173" n="HIAT:ip">.</nts>
                  <nts id="Seg_1174" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T320" id="Seg_1176" n="HIAT:u" s="T306">
                  <ts e="T307" id="Seg_1178" n="HIAT:w" s="T306">Kas</ts>
                  <nts id="Seg_1179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1181" n="HIAT:w" s="T307">da</ts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1184" n="HIAT:w" s="T308">künü</ts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1187" n="HIAT:w" s="T309">hɨppɨta</ts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1190" n="HIAT:w" s="T310">da</ts>
                  <nts id="Seg_1191" n="HIAT:ip">,</nts>
                  <nts id="Seg_1192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1194" n="HIAT:w" s="T311">araj</ts>
                  <nts id="Seg_1195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1197" n="HIAT:w" s="T312">da</ts>
                  <nts id="Seg_1198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1200" n="HIAT:w" s="T313">kaːman</ts>
                  <nts id="Seg_1201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1203" n="HIAT:w" s="T314">taksar</ts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1206" n="HIAT:w" s="T315">bu͡olbut</ts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1209" n="HIAT:w" s="T316">uhuguttan</ts>
                  <nts id="Seg_1210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1212" n="HIAT:w" s="T317">okton</ts>
                  <nts id="Seg_1213" n="HIAT:ip">,</nts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1216" n="HIAT:w" s="T318">Tanʼaktaːk</ts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1219" n="HIAT:w" s="T319">ogonnʼor</ts>
                  <nts id="Seg_1220" n="HIAT:ip">.</nts>
                  <nts id="Seg_1221" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T323" id="Seg_1223" n="HIAT:u" s="T320">
                  <ts e="T321" id="Seg_1225" n="HIAT:w" s="T320">Epeːt</ts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1228" n="HIAT:w" s="T321">Löːrü͡ötüger</ts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1231" n="HIAT:w" s="T322">barɨːhɨ</ts>
                  <nts id="Seg_1232" n="HIAT:ip">.</nts>
                  <nts id="Seg_1233" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T327" id="Seg_1235" n="HIAT:u" s="T323">
                  <ts e="T324" id="Seg_1237" n="HIAT:w" s="T323">Kelen</ts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1240" n="HIAT:w" s="T324">Löːrü͡ötüger</ts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1243" n="HIAT:w" s="T325">emi͡e</ts>
                  <nts id="Seg_1244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1246" n="HIAT:w" s="T326">ɨllaːbɨt</ts>
                  <nts id="Seg_1247" n="HIAT:ip">:</nts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T331" id="Seg_1250" n="HIAT:u" s="T327">
                  <nts id="Seg_1251" n="HIAT:ip">"</nts>
                  <ts e="T328" id="Seg_1253" n="HIAT:w" s="T327">Löːrü͡ö</ts>
                  <nts id="Seg_1254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1256" n="HIAT:w" s="T328">dogordoː</ts>
                  <nts id="Seg_1257" n="HIAT:ip">,</nts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1260" n="HIAT:w" s="T329">Löːrü͡ö</ts>
                  <nts id="Seg_1261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1263" n="HIAT:w" s="T330">dogordoː</ts>
                  <nts id="Seg_1264" n="HIAT:ip">.</nts>
                  <nts id="Seg_1265" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T336" id="Seg_1267" n="HIAT:u" s="T331">
                  <nts id="Seg_1268" n="HIAT:ip">(</nts>
                  <nts id="Seg_1269" n="HIAT:ip">(</nts>
                  <ats e="T787" id="Seg_1270" n="HIAT:non-pho" s="T331">…</ats>
                  <nts id="Seg_1271" n="HIAT:ip">)</nts>
                  <nts id="Seg_1272" n="HIAT:ip">)</nts>
                  <nts id="Seg_1273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1275" n="HIAT:w" s="T787">öllüm</ts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1278" n="HIAT:w" s="T332">kaːlɨ͡am</ts>
                  <nts id="Seg_1279" n="HIAT:ip">,</nts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1282" n="HIAT:w" s="T333">öllüm</ts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1285" n="HIAT:w" s="T334">kaːlɨ͡am</ts>
                  <nts id="Seg_1286" n="HIAT:ip">.</nts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T349" id="Seg_1289" n="HIAT:u" s="T336">
                  <ts e="T337" id="Seg_1291" n="HIAT:w" s="T336">Löːrü͡ö</ts>
                  <nts id="Seg_1292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1294" n="HIAT:w" s="T337">dogordoː</ts>
                  <nts id="Seg_1295" n="HIAT:ip">,</nts>
                  <nts id="Seg_1296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1298" n="HIAT:w" s="T338">biːr</ts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1301" n="HIAT:w" s="T339">emeː</ts>
                  <nts id="Seg_1302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1303" n="HIAT:ip">(</nts>
                  <nts id="Seg_1304" n="HIAT:ip">(</nts>
                  <ats e="T341" id="Seg_1305" n="HIAT:non-pho" s="T340">BREATH</ats>
                  <nts id="Seg_1306" n="HIAT:ip">)</nts>
                  <nts id="Seg_1307" n="HIAT:ip">)</nts>
                  <nts id="Seg_1308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1310" n="HIAT:w" s="T341">eme</ts>
                  <nts id="Seg_1311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1313" n="HIAT:w" s="T342">bɨlčɨŋkaːnna</ts>
                  <nts id="Seg_1314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1316" n="HIAT:w" s="T343">bɨragan</ts>
                  <nts id="Seg_1317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1319" n="HIAT:w" s="T344">abɨraː</ts>
                  <nts id="Seg_1320" n="HIAT:ip">,</nts>
                  <nts id="Seg_1321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1323" n="HIAT:w" s="T345">Löːrü͡ö</ts>
                  <nts id="Seg_1324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1326" n="HIAT:w" s="T346">dogordoː</ts>
                  <nts id="Seg_1327" n="HIAT:ip">,</nts>
                  <nts id="Seg_1328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1330" n="HIAT:w" s="T347">Löːrü͡ö</ts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1333" n="HIAT:w" s="T348">dogordo</ts>
                  <nts id="Seg_1334" n="HIAT:ip">.</nts>
                  <nts id="Seg_1335" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T360" id="Seg_1337" n="HIAT:u" s="T349">
                  <nts id="Seg_1338" n="HIAT:ip">(</nts>
                  <ts e="T350" id="Seg_1340" n="HIAT:w" s="T349">Ardaːn</ts>
                  <nts id="Seg_1341" n="HIAT:ip">)</nts>
                  <nts id="Seg_1342" n="HIAT:ip">,</nts>
                  <nts id="Seg_1343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1345" n="HIAT:w" s="T350">umnan</ts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1347" n="HIAT:ip">(</nts>
                  <nts id="Seg_1348" n="HIAT:ip">(</nts>
                  <ats e="T788" id="Seg_1349" n="HIAT:non-pho" s="T351">…</ats>
                  <nts id="Seg_1350" n="HIAT:ip">)</nts>
                  <nts id="Seg_1351" n="HIAT:ip">)</nts>
                  <nts id="Seg_1352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1354" n="HIAT:w" s="T788">bu͡olla</ts>
                  <nts id="Seg_1355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1356" n="HIAT:ip">(</nts>
                  <ts e="T353" id="Seg_1358" n="HIAT:w" s="T352">bulu͡o</ts>
                  <nts id="Seg_1359" n="HIAT:ip">)</nts>
                  <nts id="Seg_1360" n="HIAT:ip">,</nts>
                  <nts id="Seg_1361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1363" n="HIAT:w" s="T353">Löːrü͡ö</ts>
                  <nts id="Seg_1364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1366" n="HIAT:w" s="T354">dogordoː</ts>
                  <nts id="Seg_1367" n="HIAT:ip">,</nts>
                  <nts id="Seg_1368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1369" n="HIAT:ip">(</nts>
                  <ts e="T356" id="Seg_1371" n="HIAT:w" s="T355">ardaːn</ts>
                  <nts id="Seg_1372" n="HIAT:ip">)</nts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1375" n="HIAT:w" s="T356">bɨlčɨŋna</ts>
                  <nts id="Seg_1376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1378" n="HIAT:w" s="T357">egel</ts>
                  <nts id="Seg_1379" n="HIAT:ip">,</nts>
                  <nts id="Seg_1380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1382" n="HIAT:w" s="T358">ɨllanabɨn</ts>
                  <nts id="Seg_1383" n="HIAT:ip">.</nts>
                  <nts id="Seg_1384" n="HIAT:ip">"</nts>
                  <nts id="Seg_1385" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T374" id="Seg_1387" n="HIAT:u" s="T360">
                  <nts id="Seg_1388" n="HIAT:ip">"</nts>
                  <ts e="T361" id="Seg_1390" n="HIAT:w" s="T360">E</ts>
                  <nts id="Seg_1391" n="HIAT:ip">,</nts>
                  <nts id="Seg_1392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1394" n="HIAT:w" s="T361">biːr</ts>
                  <nts id="Seg_1395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1397" n="HIAT:w" s="T362">da</ts>
                  <nts id="Seg_1398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1400" n="HIAT:w" s="T363">bɨlčɨŋa</ts>
                  <nts id="Seg_1401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1403" n="HIAT:w" s="T364">hu͡okpun</ts>
                  <nts id="Seg_1404" n="HIAT:ip">,</nts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1407" n="HIAT:w" s="T365">bar</ts>
                  <nts id="Seg_1408" n="HIAT:ip">,</nts>
                  <nts id="Seg_1409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1411" n="HIAT:w" s="T366">bar</ts>
                  <nts id="Seg_1412" n="HIAT:ip">"</nts>
                  <nts id="Seg_1413" n="HIAT:ip">,</nts>
                  <nts id="Seg_1414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1416" n="HIAT:w" s="T367">di͡ebit</ts>
                  <nts id="Seg_1417" n="HIAT:ip">,</nts>
                  <nts id="Seg_1418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1420" n="HIAT:w" s="T368">töttörü</ts>
                  <nts id="Seg_1421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1423" n="HIAT:w" s="T369">batan</ts>
                  <nts id="Seg_1424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1426" n="HIAT:w" s="T370">ɨːppɨt</ts>
                  <nts id="Seg_1427" n="HIAT:ip">,</nts>
                  <nts id="Seg_1428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1430" n="HIAT:w" s="T371">tu͡okkaːnɨ</ts>
                  <nts id="Seg_1431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1433" n="HIAT:w" s="T372">da</ts>
                  <nts id="Seg_1434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1436" n="HIAT:w" s="T373">bi͡erbetek</ts>
                  <nts id="Seg_1437" n="HIAT:ip">.</nts>
                  <nts id="Seg_1438" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T385" id="Seg_1440" n="HIAT:u" s="T374">
                  <ts e="T375" id="Seg_1442" n="HIAT:w" s="T374">Kajdi͡ek</ts>
                  <nts id="Seg_1443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1445" n="HIAT:w" s="T375">barɨ͡aj</ts>
                  <nts id="Seg_1446" n="HIAT:ip">,</nts>
                  <nts id="Seg_1447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1449" n="HIAT:w" s="T376">tahaːra</ts>
                  <nts id="Seg_1450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1452" n="HIAT:w" s="T377">otto</ts>
                  <nts id="Seg_1453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1455" n="HIAT:w" s="T378">toŋor</ts>
                  <nts id="Seg_1456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1458" n="HIAT:w" s="T379">bu͡olla</ts>
                  <nts id="Seg_1459" n="HIAT:ip">,</nts>
                  <nts id="Seg_1460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1462" n="HIAT:w" s="T380">kajdi͡et</ts>
                  <nts id="Seg_1463" n="HIAT:ip">,</nts>
                  <nts id="Seg_1464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1466" n="HIAT:w" s="T381">dʼi͡ege</ts>
                  <nts id="Seg_1467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1469" n="HIAT:w" s="T382">da</ts>
                  <nts id="Seg_1470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1472" n="HIAT:w" s="T383">kiːllerbet</ts>
                  <nts id="Seg_1473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1475" n="HIAT:w" s="T384">saːt</ts>
                  <nts id="Seg_1476" n="HIAT:ip">.</nts>
                  <nts id="Seg_1477" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T389" id="Seg_1479" n="HIAT:u" s="T385">
                  <ts e="T386" id="Seg_1481" n="HIAT:w" s="T385">Dʼe</ts>
                  <nts id="Seg_1482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1484" n="HIAT:w" s="T386">barar</ts>
                  <nts id="Seg_1485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1487" n="HIAT:w" s="T387">bu</ts>
                  <nts id="Seg_1488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1490" n="HIAT:w" s="T388">kihi</ts>
                  <nts id="Seg_1491" n="HIAT:ip">.</nts>
                  <nts id="Seg_1492" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T404" id="Seg_1494" n="HIAT:u" s="T389">
                  <ts e="T390" id="Seg_1496" n="HIAT:w" s="T389">Barbɨta</ts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1499" n="HIAT:w" s="T390">araččɨ</ts>
                  <nts id="Seg_1500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1502" n="HIAT:w" s="T391">onton</ts>
                  <nts id="Seg_1503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1505" n="HIAT:w" s="T392">tijen</ts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1508" n="HIAT:w" s="T393">istegine</ts>
                  <nts id="Seg_1509" n="HIAT:ip">,</nts>
                  <nts id="Seg_1510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1511" n="HIAT:ip">(</nts>
                  <ts e="T396" id="Seg_1513" n="HIAT:w" s="T394">dʼi͡en-</ts>
                  <nts id="Seg_1514" n="HIAT:ip">)</nts>
                  <nts id="Seg_1515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1517" n="HIAT:w" s="T396">dʼi͡etin</ts>
                  <nts id="Seg_1518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1520" n="HIAT:w" s="T397">attɨgar</ts>
                  <nts id="Seg_1521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1523" n="HIAT:w" s="T398">tijer</ts>
                  <nts id="Seg_1524" n="HIAT:ip">,</nts>
                  <nts id="Seg_1525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1527" n="HIAT:w" s="T399">körbüte</ts>
                  <nts id="Seg_1528" n="HIAT:ip">,</nts>
                  <nts id="Seg_1529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1531" n="HIAT:w" s="T400">tuguttaːk</ts>
                  <nts id="Seg_1532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1534" n="HIAT:w" s="T401">tɨhɨ</ts>
                  <nts id="Seg_1535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1537" n="HIAT:w" s="T402">hɨldʼar</ts>
                  <nts id="Seg_1538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1540" n="HIAT:w" s="T403">dʼi͡etiger</ts>
                  <nts id="Seg_1541" n="HIAT:ip">.</nts>
                  <nts id="Seg_1542" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T416" id="Seg_1544" n="HIAT:u" s="T404">
                  <ts e="T405" id="Seg_1546" n="HIAT:w" s="T404">Kaja</ts>
                  <nts id="Seg_1547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1549" n="HIAT:w" s="T405">keliːtten</ts>
                  <nts id="Seg_1550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1552" n="HIAT:w" s="T406">kelbite</ts>
                  <nts id="Seg_1553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1555" n="HIAT:w" s="T407">dʼürü</ts>
                  <nts id="Seg_1556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1558" n="HIAT:w" s="T408">bilbet</ts>
                  <nts id="Seg_1559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1561" n="HIAT:w" s="T409">tabanɨ</ts>
                  <nts id="Seg_1562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1564" n="HIAT:w" s="T410">gini</ts>
                  <nts id="Seg_1565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1566" n="HIAT:ip">(</nts>
                  <nts id="Seg_1567" n="HIAT:ip">(</nts>
                  <ats e="T789" id="Seg_1568" n="HIAT:non-pho" s="T411">…</ats>
                  <nts id="Seg_1569" n="HIAT:ip">)</nts>
                  <nts id="Seg_1570" n="HIAT:ip">)</nts>
                  <nts id="Seg_1571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1573" n="HIAT:w" s="T789">taba</ts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1576" n="HIAT:w" s="T412">hɨldʼar</ts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1579" n="HIAT:w" s="T413">araj</ts>
                  <nts id="Seg_1580" n="HIAT:ip">,</nts>
                  <nts id="Seg_1581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1583" n="HIAT:w" s="T414">kelbit</ts>
                  <nts id="Seg_1584" n="HIAT:ip">.</nts>
                  <nts id="Seg_1585" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T431" id="Seg_1587" n="HIAT:u" s="T416">
                  <ts e="T417" id="Seg_1589" n="HIAT:w" s="T416">Örö</ts>
                  <nts id="Seg_1590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1592" n="HIAT:w" s="T417">gini</ts>
                  <nts id="Seg_1593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1595" n="HIAT:w" s="T418">ölü͡ök</ts>
                  <nts id="Seg_1596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1598" n="HIAT:w" s="T419">ɨjaːgɨgar</ts>
                  <nts id="Seg_1599" n="HIAT:ip">,</nts>
                  <nts id="Seg_1600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1601" n="HIAT:ip">(</nts>
                  <ts e="T422" id="Seg_1603" n="HIAT:w" s="T420">öt-</ts>
                  <nts id="Seg_1604" n="HIAT:ip">)</nts>
                  <nts id="Seg_1605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1607" n="HIAT:w" s="T422">tilli͡ek</ts>
                  <nts id="Seg_1608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1610" n="HIAT:w" s="T423">ɨjaːgɨgar</ts>
                  <nts id="Seg_1611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1613" n="HIAT:w" s="T424">tuguttaːk</ts>
                  <nts id="Seg_1614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1616" n="HIAT:w" s="T425">tɨhɨta</ts>
                  <nts id="Seg_1617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1619" n="HIAT:w" s="T426">bejetiger</ts>
                  <nts id="Seg_1620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1622" n="HIAT:w" s="T427">kelen</ts>
                  <nts id="Seg_1623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1625" n="HIAT:w" s="T428">halɨː</ts>
                  <nts id="Seg_1626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1628" n="HIAT:w" s="T429">hɨldʼɨbɨttar</ts>
                  <nts id="Seg_1629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1631" n="HIAT:w" s="T430">ginini</ts>
                  <nts id="Seg_1632" n="HIAT:ip">.</nts>
                  <nts id="Seg_1633" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T445" id="Seg_1635" n="HIAT:u" s="T431">
                  <ts e="T432" id="Seg_1637" n="HIAT:w" s="T431">Iːkteːbitiger</ts>
                  <nts id="Seg_1638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1640" n="HIAT:w" s="T432">ihiger</ts>
                  <nts id="Seg_1641" n="HIAT:ip">,</nts>
                  <nts id="Seg_1642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1644" n="HIAT:w" s="T433">kelbite</ts>
                  <nts id="Seg_1645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1647" n="HIAT:w" s="T434">da</ts>
                  <nts id="Seg_1648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1650" n="HIAT:w" s="T435">tugukkaːnɨn</ts>
                  <nts id="Seg_1651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1653" n="HIAT:w" s="T436">kaban</ts>
                  <nts id="Seg_1654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1656" n="HIAT:w" s="T437">ɨlan</ts>
                  <nts id="Seg_1657" n="HIAT:ip">,</nts>
                  <nts id="Seg_1658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1660" n="HIAT:w" s="T438">bahakkaːnɨ</ts>
                  <nts id="Seg_1661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1663" n="HIAT:w" s="T439">ɨlla</ts>
                  <nts id="Seg_1664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1666" n="HIAT:w" s="T440">da</ts>
                  <nts id="Seg_1667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1668" n="HIAT:ip">"</nts>
                  <ts e="T442" id="Seg_1670" n="HIAT:w" s="T441">kort</ts>
                  <nts id="Seg_1671" n="HIAT:ip">"</nts>
                  <nts id="Seg_1672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1674" n="HIAT:w" s="T442">gɨnnaran</ts>
                  <nts id="Seg_1675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1677" n="HIAT:w" s="T443">ölörön</ts>
                  <nts id="Seg_1678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1680" n="HIAT:w" s="T444">keːspit</ts>
                  <nts id="Seg_1681" n="HIAT:ip">.</nts>
                  <nts id="Seg_1682" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T447" id="Seg_1684" n="HIAT:u" s="T445">
                  <ts e="T446" id="Seg_1686" n="HIAT:w" s="T445">Inʼete</ts>
                  <nts id="Seg_1687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1689" n="HIAT:w" s="T446">hɨldʼar</ts>
                  <nts id="Seg_1690" n="HIAT:ip">.</nts>
                  <nts id="Seg_1691" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T460" id="Seg_1693" n="HIAT:u" s="T447">
                  <ts e="T448" id="Seg_1695" n="HIAT:w" s="T447">Ko</ts>
                  <nts id="Seg_1696" n="HIAT:ip">,</nts>
                  <nts id="Seg_1697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1699" n="HIAT:w" s="T448">mantɨtɨn</ts>
                  <nts id="Seg_1700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1701" n="HIAT:ip">(</nts>
                  <ts e="T450" id="Seg_1703" n="HIAT:w" s="T449">tu͡ok</ts>
                  <nts id="Seg_1704" n="HIAT:ip">)</nts>
                  <nts id="Seg_1705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1707" n="HIAT:w" s="T450">tu͡ok</ts>
                  <nts id="Seg_1708" n="HIAT:ip">,</nts>
                  <nts id="Seg_1709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1710" n="HIAT:ip">(</nts>
                  <ts e="T453" id="Seg_1712" n="HIAT:w" s="T451">hülü-</ts>
                  <nts id="Seg_1713" n="HIAT:ip">)</nts>
                  <nts id="Seg_1714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1716" n="HIAT:w" s="T453">hülümmüt</ts>
                  <nts id="Seg_1717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1719" n="HIAT:w" s="T454">kihi</ts>
                  <nts id="Seg_1720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1722" n="HIAT:w" s="T455">hiːkejdiː</ts>
                  <nts id="Seg_1723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1725" n="HIAT:w" s="T456">buharan</ts>
                  <nts id="Seg_1726" n="HIAT:ip">,</nts>
                  <nts id="Seg_1727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1729" n="HIAT:w" s="T457">kü͡östenen</ts>
                  <nts id="Seg_1730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1732" n="HIAT:w" s="T458">hi͡en</ts>
                  <nts id="Seg_1733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1735" n="HIAT:w" s="T459">keːspit</ts>
                  <nts id="Seg_1736" n="HIAT:ip">.</nts>
                  <nts id="Seg_1737" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T464" id="Seg_1739" n="HIAT:u" s="T460">
                  <ts e="T461" id="Seg_1741" n="HIAT:w" s="T460">Dʼe</ts>
                  <nts id="Seg_1742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1744" n="HIAT:w" s="T461">karaga</ts>
                  <nts id="Seg_1745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1747" n="HIAT:w" s="T462">hɨrdaːbɨt</ts>
                  <nts id="Seg_1748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1750" n="HIAT:w" s="T463">onno</ts>
                  <nts id="Seg_1751" n="HIAT:ip">.</nts>
                  <nts id="Seg_1752" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T475" id="Seg_1754" n="HIAT:u" s="T464">
                  <nts id="Seg_1755" n="HIAT:ip">"</nts>
                  <ts e="T465" id="Seg_1757" n="HIAT:w" s="T464">Oː</ts>
                  <nts id="Seg_1758" n="HIAT:ip">,</nts>
                  <nts id="Seg_1759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1761" n="HIAT:w" s="T465">dʼe</ts>
                  <nts id="Seg_1762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1764" n="HIAT:w" s="T466">anɨ</ts>
                  <nts id="Seg_1765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1767" n="HIAT:w" s="T467">bagas</ts>
                  <nts id="Seg_1768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1770" n="HIAT:w" s="T468">abɨrannɨm</ts>
                  <nts id="Seg_1771" n="HIAT:ip">,</nts>
                  <nts id="Seg_1772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1774" n="HIAT:w" s="T469">Löːrü͡öge</ts>
                  <nts id="Seg_1775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1777" n="HIAT:w" s="T470">barbappɨn</ts>
                  <nts id="Seg_1778" n="HIAT:ip">,</nts>
                  <nts id="Seg_1779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1781" n="HIAT:w" s="T471">kanʼaːbappɨn</ts>
                  <nts id="Seg_1782" n="HIAT:ip">,</nts>
                  <nts id="Seg_1783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1785" n="HIAT:w" s="T472">astannɨm</ts>
                  <nts id="Seg_1786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1788" n="HIAT:w" s="T473">bu͡o</ts>
                  <nts id="Seg_1789" n="HIAT:ip">"</nts>
                  <nts id="Seg_1790" n="HIAT:ip">,</nts>
                  <nts id="Seg_1791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1793" n="HIAT:w" s="T474">diːr</ts>
                  <nts id="Seg_1794" n="HIAT:ip">.</nts>
                  <nts id="Seg_1795" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T480" id="Seg_1797" n="HIAT:u" s="T475">
                  <ts e="T476" id="Seg_1799" n="HIAT:w" s="T475">Astammɨt</ts>
                  <nts id="Seg_1800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1802" n="HIAT:w" s="T476">kihi</ts>
                  <nts id="Seg_1803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1805" n="HIAT:w" s="T477">kü͡östenen</ts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1808" n="HIAT:w" s="T478">baraːn</ts>
                  <nts id="Seg_1809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1811" n="HIAT:w" s="T479">ahɨːr</ts>
                  <nts id="Seg_1812" n="HIAT:ip">.</nts>
                  <nts id="Seg_1813" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T481" id="Seg_1815" n="HIAT:u" s="T480">
                  <ts e="T481" id="Seg_1817" n="HIAT:w" s="T480">Oːk</ts>
                  <nts id="Seg_1818" n="HIAT:ip">.</nts>
                  <nts id="Seg_1819" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T486" id="Seg_1821" n="HIAT:u" s="T481">
                  <ts e="T482" id="Seg_1823" n="HIAT:w" s="T481">Dʼeː</ts>
                  <nts id="Seg_1824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1826" n="HIAT:w" s="T482">kas</ts>
                  <nts id="Seg_1827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1829" n="HIAT:w" s="T483">künü</ts>
                  <nts id="Seg_1830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1832" n="HIAT:w" s="T484">olorbuta</ts>
                  <nts id="Seg_1833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1835" n="HIAT:w" s="T485">dʼürü</ts>
                  <nts id="Seg_1836" n="HIAT:ip">.</nts>
                  <nts id="Seg_1837" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T491" id="Seg_1839" n="HIAT:u" s="T486">
                  <ts e="T487" id="Seg_1841" n="HIAT:w" s="T486">Tabalaːk</ts>
                  <nts id="Seg_1842" n="HIAT:ip">,</nts>
                  <nts id="Seg_1843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1845" n="HIAT:w" s="T487">tabatɨn</ts>
                  <nts id="Seg_1846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1848" n="HIAT:w" s="T488">itte</ts>
                  <nts id="Seg_1849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1851" n="HIAT:w" s="T489">tuttubut</ts>
                  <nts id="Seg_1852" n="HIAT:ip">,</nts>
                  <nts id="Seg_1853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1855" n="HIAT:w" s="T490">tɨhɨtɨn</ts>
                  <nts id="Seg_1856" n="HIAT:ip">.</nts>
                  <nts id="Seg_1857" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T495" id="Seg_1859" n="HIAT:u" s="T491">
                  <ts e="T492" id="Seg_1861" n="HIAT:w" s="T491">Tɨhɨlaːk</ts>
                  <nts id="Seg_1862" n="HIAT:ip">,</nts>
                  <nts id="Seg_1863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1865" n="HIAT:w" s="T492">tu͡ok</ts>
                  <nts id="Seg_1866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1868" n="HIAT:w" s="T493">bu͡olu͡oj</ts>
                  <nts id="Seg_1869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1871" n="HIAT:w" s="T494">anɨ</ts>
                  <nts id="Seg_1872" n="HIAT:ip">.</nts>
                  <nts id="Seg_1873" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T501" id="Seg_1875" n="HIAT:u" s="T495">
                  <ts e="T496" id="Seg_1877" n="HIAT:w" s="T495">Oː</ts>
                  <nts id="Seg_1878" n="HIAT:ip">,</nts>
                  <nts id="Seg_1879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1881" n="HIAT:w" s="T496">bu</ts>
                  <nts id="Seg_1882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1884" n="HIAT:w" s="T497">hɨttagɨna</ts>
                  <nts id="Seg_1885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1887" n="HIAT:w" s="T498">Löːrü͡ötün</ts>
                  <nts id="Seg_1888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1890" n="HIAT:w" s="T499">haŋata</ts>
                  <nts id="Seg_1891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1893" n="HIAT:w" s="T500">kelle</ts>
                  <nts id="Seg_1894" n="HIAT:ip">.</nts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T504" id="Seg_1897" n="HIAT:u" s="T501">
                  <ts e="T502" id="Seg_1899" n="HIAT:w" s="T501">Dʼe</ts>
                  <nts id="Seg_1900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1902" n="HIAT:w" s="T502">araj</ts>
                  <nts id="Seg_1903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1905" n="HIAT:w" s="T503">körbüte</ts>
                  <nts id="Seg_1906" n="HIAT:ip">.</nts>
                  <nts id="Seg_1907" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T515" id="Seg_1909" n="HIAT:u" s="T504">
                  <nts id="Seg_1910" n="HIAT:ip">"</nts>
                  <ts e="T505" id="Seg_1912" n="HIAT:w" s="T504">Dʼe</ts>
                  <nts id="Seg_1913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1915" n="HIAT:w" s="T505">min</ts>
                  <nts id="Seg_1916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1918" n="HIAT:w" s="T506">anɨ</ts>
                  <nts id="Seg_1919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1921" n="HIAT:w" s="T507">itigirdik</ts>
                  <nts id="Seg_1922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1924" n="HIAT:w" s="T508">gɨnɨ͡am</ts>
                  <nts id="Seg_1925" n="HIAT:ip">,</nts>
                  <nts id="Seg_1926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1928" n="HIAT:w" s="T509">beːbe</ts>
                  <nts id="Seg_1929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1931" n="HIAT:w" s="T510">kellin</ts>
                  <nts id="Seg_1932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1934" n="HIAT:w" s="T511">agaj</ts>
                  <nts id="Seg_1935" n="HIAT:ip">"</nts>
                  <nts id="Seg_1936" n="HIAT:ip">,</nts>
                  <nts id="Seg_1937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1939" n="HIAT:w" s="T512">di͡ebit</ts>
                  <nts id="Seg_1940" n="HIAT:ip">,</nts>
                  <nts id="Seg_1941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1942" n="HIAT:ip">"</nts>
                  <ts e="T514" id="Seg_1944" n="HIAT:w" s="T513">mini͡eke</ts>
                  <nts id="Seg_1945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1947" n="HIAT:w" s="T514">as</ts>
                  <nts id="Seg_1948" n="HIAT:ip">"</nts>
                  <nts id="Seg_1949" n="HIAT:ip">.</nts>
                  <nts id="Seg_1950" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T521" id="Seg_1952" n="HIAT:u" s="T515">
                  <ts e="T516" id="Seg_1954" n="HIAT:w" s="T515">Löːrü͡ö</ts>
                  <nts id="Seg_1955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1957" n="HIAT:w" s="T516">tu͡oktaːn</ts>
                  <nts id="Seg_1958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1960" n="HIAT:w" s="T517">arɨččɨ</ts>
                  <nts id="Seg_1961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1963" n="HIAT:w" s="T518">iher</ts>
                  <nts id="Seg_1964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1966" n="HIAT:w" s="T519">bu͡opsa</ts>
                  <nts id="Seg_1967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1969" n="HIAT:w" s="T520">korgujan</ts>
                  <nts id="Seg_1970" n="HIAT:ip">.</nts>
                  <nts id="Seg_1971" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T524" id="Seg_1973" n="HIAT:u" s="T521">
                  <ts e="T522" id="Seg_1975" n="HIAT:w" s="T521">Araččɨ</ts>
                  <nts id="Seg_1976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1978" n="HIAT:w" s="T522">kaːman</ts>
                  <nts id="Seg_1979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1981" n="HIAT:w" s="T523">iher</ts>
                  <nts id="Seg_1982" n="HIAT:ip">.</nts>
                  <nts id="Seg_1983" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T527" id="Seg_1985" n="HIAT:u" s="T524">
                  <ts e="T525" id="Seg_1987" n="HIAT:w" s="T524">Kelbit</ts>
                  <nts id="Seg_1988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1990" n="HIAT:w" s="T525">da</ts>
                  <nts id="Seg_1991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1993" n="HIAT:w" s="T526">kelbit</ts>
                  <nts id="Seg_1994" n="HIAT:ip">.</nts>
                  <nts id="Seg_1995" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T533" id="Seg_1997" n="HIAT:u" s="T527">
                  <ts e="T528" id="Seg_1999" n="HIAT:w" s="T527">Kelerin</ts>
                  <nts id="Seg_2000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_2002" n="HIAT:w" s="T528">gɨtta</ts>
                  <nts id="Seg_2003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_2005" n="HIAT:w" s="T529">dʼi͡ete</ts>
                  <nts id="Seg_2006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_2008" n="HIAT:w" s="T530">kataːn</ts>
                  <nts id="Seg_2009" n="HIAT:ip">,</nts>
                  <nts id="Seg_2010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_2012" n="HIAT:w" s="T531">katanan</ts>
                  <nts id="Seg_2013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_2015" n="HIAT:w" s="T532">keːspit</ts>
                  <nts id="Seg_2016" n="HIAT:ip">.</nts>
                  <nts id="Seg_2017" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T542" id="Seg_2019" n="HIAT:u" s="T533">
                  <nts id="Seg_2020" n="HIAT:ip">"</nts>
                  <ts e="T534" id="Seg_2022" n="HIAT:w" s="T533">Dʼe</ts>
                  <nts id="Seg_2023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_2025" n="HIAT:w" s="T534">ginini</ts>
                  <nts id="Seg_2026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_2028" n="HIAT:w" s="T535">nakaːstɨ͡am</ts>
                  <nts id="Seg_2029" n="HIAT:ip">,</nts>
                  <nts id="Seg_2030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_2032" n="HIAT:w" s="T536">maːjdʼɨn</ts>
                  <nts id="Seg_2033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_2035" n="HIAT:w" s="T537">minigin</ts>
                  <nts id="Seg_2036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_2038" n="HIAT:w" s="T538">emi͡e</ts>
                  <nts id="Seg_2039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_2041" n="HIAT:w" s="T539">gini</ts>
                  <nts id="Seg_2042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_2044" n="HIAT:w" s="T540">muŋnaːbɨta</ts>
                  <nts id="Seg_2045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_2047" n="HIAT:w" s="T541">burduk</ts>
                  <nts id="Seg_2048" n="HIAT:ip">.</nts>
                  <nts id="Seg_2049" n="HIAT:ip">"</nts>
                  <nts id="Seg_2050" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T546" id="Seg_2052" n="HIAT:u" s="T542">
                  <nts id="Seg_2053" n="HIAT:ip">"</nts>
                  <ts e="T543" id="Seg_2055" n="HIAT:w" s="T542">Tanʼaktaːk</ts>
                  <nts id="Seg_2056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_2058" n="HIAT:w" s="T543">ogonnʼor</ts>
                  <nts id="Seg_2059" n="HIAT:ip">,</nts>
                  <nts id="Seg_2060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_2062" n="HIAT:w" s="T544">Tanʼaktaːk</ts>
                  <nts id="Seg_2063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_2065" n="HIAT:w" s="T545">ogonnʼor</ts>
                  <nts id="Seg_2066" n="HIAT:ip">.</nts>
                  <nts id="Seg_2067" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T560" id="Seg_2069" n="HIAT:u" s="T546">
                  <ts e="T547" id="Seg_2071" n="HIAT:w" s="T546">Biːr</ts>
                  <nts id="Seg_2072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2073" n="HIAT:ip">(</nts>
                  <nts id="Seg_2074" n="HIAT:ip">(</nts>
                  <ats e="T790" id="Seg_2075" n="HIAT:non-pho" s="T547">…</ats>
                  <nts id="Seg_2076" n="HIAT:ip">)</nts>
                  <nts id="Seg_2077" n="HIAT:ip">)</nts>
                  <nts id="Seg_2078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_2080" n="HIAT:w" s="T790">emete</ts>
                  <nts id="Seg_2081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_2083" n="HIAT:w" s="T548">bɨlčɨŋkaːnna</ts>
                  <nts id="Seg_2084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_2086" n="HIAT:w" s="T549">bi͡eren</ts>
                  <nts id="Seg_2087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2089" n="HIAT:w" s="T550">abɨraː</ts>
                  <nts id="Seg_2090" n="HIAT:ip">,</nts>
                  <nts id="Seg_2091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2093" n="HIAT:w" s="T551">Tanʼaktaːk</ts>
                  <nts id="Seg_2094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2096" n="HIAT:w" s="T552">ogonnʼor</ts>
                  <nts id="Seg_2097" n="HIAT:ip">,</nts>
                  <nts id="Seg_2098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2100" n="HIAT:w" s="T553">öllüm</ts>
                  <nts id="Seg_2101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_2103" n="HIAT:w" s="T554">kaːllɨm</ts>
                  <nts id="Seg_2104" n="HIAT:ip">,</nts>
                  <nts id="Seg_2105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2107" n="HIAT:w" s="T555">öllüm</ts>
                  <nts id="Seg_2108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2109" n="HIAT:ip">(</nts>
                  <ts e="T558" id="Seg_2111" n="HIAT:w" s="T556">kaː-</ts>
                  <nts id="Seg_2112" n="HIAT:ip">)</nts>
                  <nts id="Seg_2113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_2115" n="HIAT:w" s="T558">kaːllɨm</ts>
                  <nts id="Seg_2116" n="HIAT:ip">.</nts>
                  <nts id="Seg_2117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T571" id="Seg_2119" n="HIAT:u" s="T560">
                  <ts e="T561" id="Seg_2121" n="HIAT:w" s="T560">Kümmer</ts>
                  <nts id="Seg_2122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2124" n="HIAT:w" s="T561">turda</ts>
                  <nts id="Seg_2125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_2127" n="HIAT:w" s="T562">kuŋ</ts>
                  <nts id="Seg_2128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2130" n="HIAT:w" s="T563">kihite</ts>
                  <nts id="Seg_2131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2133" n="HIAT:w" s="T564">bu͡ollakkɨna</ts>
                  <nts id="Seg_2134" n="HIAT:ip">,</nts>
                  <nts id="Seg_2135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_2137" n="HIAT:w" s="T565">Tanʼaktaːk</ts>
                  <nts id="Seg_2138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2140" n="HIAT:w" s="T566">ogonnʼor</ts>
                  <nts id="Seg_2141" n="HIAT:ip">,</nts>
                  <nts id="Seg_2142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_2144" n="HIAT:w" s="T567">Tanʼaktaːk</ts>
                  <nts id="Seg_2145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2147" n="HIAT:w" s="T568">ogonnʼor</ts>
                  <nts id="Seg_2148" n="HIAT:ip">"</nts>
                  <nts id="Seg_2149" n="HIAT:ip">,</nts>
                  <nts id="Seg_2150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2152" n="HIAT:w" s="T569">ɨllana</ts>
                  <nts id="Seg_2153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2155" n="HIAT:w" s="T570">hɨppɨt</ts>
                  <nts id="Seg_2156" n="HIAT:ip">.</nts>
                  <nts id="Seg_2157" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T574" id="Seg_2159" n="HIAT:u" s="T571">
                  <ts e="T572" id="Seg_2161" n="HIAT:w" s="T571">Tugu</ts>
                  <nts id="Seg_2162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2164" n="HIAT:w" s="T572">da</ts>
                  <nts id="Seg_2165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_2167" n="HIAT:w" s="T573">bi͡erbetek</ts>
                  <nts id="Seg_2168" n="HIAT:ip">.</nts>
                  <nts id="Seg_2169" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T596" id="Seg_2171" n="HIAT:u" s="T574">
                  <ts e="T575" id="Seg_2173" n="HIAT:w" s="T574">Postu͡oj</ts>
                  <nts id="Seg_2174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2176" n="HIAT:w" s="T575">tu͡ok</ts>
                  <nts id="Seg_2177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2179" n="HIAT:w" s="T576">ere</ts>
                  <nts id="Seg_2180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2182" n="HIAT:w" s="T577">bu</ts>
                  <nts id="Seg_2183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2185" n="HIAT:w" s="T578">tugutun</ts>
                  <nts id="Seg_2186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2188" n="HIAT:w" s="T579">gi͡enin</ts>
                  <nts id="Seg_2189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2190" n="HIAT:ip">(</nts>
                  <ts e="T581" id="Seg_2192" n="HIAT:w" s="T580">tunʼaga</ts>
                  <nts id="Seg_2193" n="HIAT:ip">)</nts>
                  <nts id="Seg_2194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2196" n="HIAT:w" s="T581">tunʼaktarɨn</ts>
                  <nts id="Seg_2197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2199" n="HIAT:w" s="T582">tu͡oktammɨt</ts>
                  <nts id="Seg_2200" n="HIAT:ip">,</nts>
                  <nts id="Seg_2201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2203" n="HIAT:w" s="T583">atatɨn</ts>
                  <nts id="Seg_2204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2206" n="HIAT:w" s="T584">gɨtta</ts>
                  <nts id="Seg_2207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2209" n="HIAT:w" s="T585">tunʼaga</ts>
                  <nts id="Seg_2210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2212" n="HIAT:w" s="T586">duː</ts>
                  <nts id="Seg_2213" n="HIAT:ip">,</nts>
                  <nts id="Seg_2214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2216" n="HIAT:w" s="T587">tu͡ok</ts>
                  <nts id="Seg_2217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2219" n="HIAT:w" s="T588">duː</ts>
                  <nts id="Seg_2220" n="HIAT:ip">,</nts>
                  <nts id="Seg_2221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2223" n="HIAT:w" s="T589">onu</ts>
                  <nts id="Seg_2224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2225" n="HIAT:ip">(</nts>
                  <ts e="T592" id="Seg_2227" n="HIAT:w" s="T590">het-</ts>
                  <nts id="Seg_2228" n="HIAT:ip">)</nts>
                  <nts id="Seg_2229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2231" n="HIAT:w" s="T592">atanaːk</ts>
                  <nts id="Seg_2232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2234" n="HIAT:w" s="T593">hɨldʼan</ts>
                  <nts id="Seg_2235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2237" n="HIAT:w" s="T594">onu</ts>
                  <nts id="Seg_2238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2240" n="HIAT:w" s="T595">bɨragan</ts>
                  <nts id="Seg_2241" n="HIAT:ip">.</nts>
                  <nts id="Seg_2242" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T603" id="Seg_2244" n="HIAT:u" s="T596">
                  <nts id="Seg_2245" n="HIAT:ip">"</nts>
                  <ts e="T597" id="Seg_2247" n="HIAT:w" s="T596">Bar</ts>
                  <nts id="Seg_2248" n="HIAT:ip">,</nts>
                  <nts id="Seg_2249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2251" n="HIAT:w" s="T597">ši͡e</ts>
                  <nts id="Seg_2252" n="HIAT:ip">"</nts>
                  <nts id="Seg_2253" n="HIAT:ip">,</nts>
                  <nts id="Seg_2254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2256" n="HIAT:w" s="T598">diː</ts>
                  <nts id="Seg_2257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2259" n="HIAT:w" s="T599">ikki</ts>
                  <nts id="Seg_2260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2262" n="HIAT:w" s="T600">tunʼagɨ</ts>
                  <nts id="Seg_2263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2265" n="HIAT:w" s="T601">bɨragan</ts>
                  <nts id="Seg_2266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2268" n="HIAT:w" s="T602">keːspit</ts>
                  <nts id="Seg_2269" n="HIAT:ip">.</nts>
                  <nts id="Seg_2270" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T615" id="Seg_2272" n="HIAT:u" s="T603">
                  <nts id="Seg_2273" n="HIAT:ip">"</nts>
                  <ts e="T604" id="Seg_2275" n="HIAT:w" s="T603">Bar</ts>
                  <nts id="Seg_2276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2278" n="HIAT:w" s="T604">dʼi͡eger</ts>
                  <nts id="Seg_2279" n="HIAT:ip">"</nts>
                  <nts id="Seg_2280" n="HIAT:ip">,</nts>
                  <nts id="Seg_2281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2283" n="HIAT:w" s="T605">diːr</ts>
                  <nts id="Seg_2284" n="HIAT:ip">,</nts>
                  <nts id="Seg_2285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2286" n="HIAT:ip">"</nts>
                  <ts e="T607" id="Seg_2288" n="HIAT:w" s="T606">bolʼše</ts>
                  <nts id="Seg_2289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2291" n="HIAT:w" s="T607">kelime</ts>
                  <nts id="Seg_2292" n="HIAT:ip">,</nts>
                  <nts id="Seg_2293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2294" n="HIAT:ip">(</nts>
                  <ts e="T610" id="Seg_2296" n="HIAT:w" s="T608">mi-</ts>
                  <nts id="Seg_2297" n="HIAT:ip">)</nts>
                  <nts id="Seg_2298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2300" n="HIAT:w" s="T610">maːjdɨn</ts>
                  <nts id="Seg_2301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2303" n="HIAT:w" s="T611">minigin</ts>
                  <nts id="Seg_2304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2306" n="HIAT:w" s="T612">emi͡e</ts>
                  <nts id="Seg_2307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2309" n="HIAT:w" s="T613">dʼe</ts>
                  <nts id="Seg_2310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2312" n="HIAT:w" s="T614">muŋnaːbɨtɨŋ</ts>
                  <nts id="Seg_2313" n="HIAT:ip">.</nts>
                  <nts id="Seg_2314" n="HIAT:ip">"</nts>
                  <nts id="Seg_2315" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T620" id="Seg_2317" n="HIAT:u" s="T615">
                  <ts e="T616" id="Seg_2319" n="HIAT:w" s="T615">Ontuta</ts>
                  <nts id="Seg_2320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2322" n="HIAT:w" s="T616">bɨstɨ͡a</ts>
                  <nts id="Seg_2323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2325" n="HIAT:w" s="T617">du͡o</ts>
                  <nts id="Seg_2326" n="HIAT:ip">,</nts>
                  <nts id="Seg_2327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2329" n="HIAT:w" s="T618">baran</ts>
                  <nts id="Seg_2330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2332" n="HIAT:w" s="T619">kaːlbɨt</ts>
                  <nts id="Seg_2333" n="HIAT:ip">.</nts>
                  <nts id="Seg_2334" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T624" id="Seg_2336" n="HIAT:u" s="T620">
                  <ts e="T621" id="Seg_2338" n="HIAT:w" s="T620">Dʼe</ts>
                  <nts id="Seg_2339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2341" n="HIAT:w" s="T621">baran</ts>
                  <nts id="Seg_2342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2344" n="HIAT:w" s="T622">kaːlbɨt</ts>
                  <nts id="Seg_2345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2347" n="HIAT:w" s="T623">dʼi͡etiger</ts>
                  <nts id="Seg_2348" n="HIAT:ip">.</nts>
                  <nts id="Seg_2349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T639" id="Seg_2351" n="HIAT:u" s="T624">
                  <ts e="T625" id="Seg_2353" n="HIAT:w" s="T624">Emi͡e</ts>
                  <nts id="Seg_2354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2355" n="HIAT:ip">(</nts>
                  <nts id="Seg_2356" n="HIAT:ip">(</nts>
                  <ats e="T626" id="Seg_2357" n="HIAT:non-pho" s="T625">PAUSE</ats>
                  <nts id="Seg_2358" n="HIAT:ip">)</nts>
                  <nts id="Seg_2359" n="HIAT:ip">)</nts>
                  <nts id="Seg_2360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2362" n="HIAT:w" s="T626">barbɨt</ts>
                  <nts id="Seg_2363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2365" n="HIAT:w" s="T627">da</ts>
                  <nts id="Seg_2366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2368" n="HIAT:w" s="T628">hartɨn</ts>
                  <nts id="Seg_2369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2370" n="HIAT:ip">(</nts>
                  <ts e="T631" id="Seg_2372" n="HIAT:w" s="T629">ka-</ts>
                  <nts id="Seg_2373" n="HIAT:ip">)</nts>
                  <nts id="Seg_2374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2376" n="HIAT:w" s="T631">kas</ts>
                  <nts id="Seg_2377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2379" n="HIAT:w" s="T632">da</ts>
                  <nts id="Seg_2380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2382" n="HIAT:w" s="T633">örö</ts>
                  <nts id="Seg_2383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2384" n="HIAT:ip">(</nts>
                  <ts e="T635" id="Seg_2386" n="HIAT:w" s="T634">günü</ts>
                  <nts id="Seg_2387" n="HIAT:ip">)</nts>
                  <nts id="Seg_2388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2390" n="HIAT:w" s="T635">bu͡olan</ts>
                  <nts id="Seg_2391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2393" n="HIAT:w" s="T636">baraːn</ts>
                  <nts id="Seg_2394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2396" n="HIAT:w" s="T637">emi͡e</ts>
                  <nts id="Seg_2397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2399" n="HIAT:w" s="T638">keler</ts>
                  <nts id="Seg_2400" n="HIAT:ip">.</nts>
                  <nts id="Seg_2401" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T647" id="Seg_2403" n="HIAT:u" s="T639">
                  <ts e="T640" id="Seg_2405" n="HIAT:w" s="T639">Dʼe</ts>
                  <nts id="Seg_2406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2408" n="HIAT:w" s="T640">vavsʼe</ts>
                  <nts id="Seg_2409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2411" n="HIAT:w" s="T641">ölörüger</ts>
                  <nts id="Seg_2412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2414" n="HIAT:w" s="T642">barbɨt</ts>
                  <nts id="Seg_2415" n="HIAT:ip">,</nts>
                  <nts id="Seg_2416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2418" n="HIAT:w" s="T643">bɨhɨlaːk</ts>
                  <nts id="Seg_2419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2421" n="HIAT:w" s="T644">dogoro</ts>
                  <nts id="Seg_2422" n="HIAT:ip">,</nts>
                  <nts id="Seg_2423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2425" n="HIAT:w" s="T645">Löːrü͡öte</ts>
                  <nts id="Seg_2426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2428" n="HIAT:w" s="T646">ɨksaːbɨt</ts>
                  <nts id="Seg_2429" n="HIAT:ip">:</nts>
                  <nts id="Seg_2430" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T652" id="Seg_2432" n="HIAT:u" s="T647">
                  <ts e="T648" id="Seg_2434" n="HIAT:w" s="T647">Battagɨna</ts>
                  <nts id="Seg_2435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2437" n="HIAT:w" s="T648">da</ts>
                  <nts id="Seg_2438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2440" n="HIAT:w" s="T649">barbat</ts>
                  <nts id="Seg_2441" n="HIAT:ip">,</nts>
                  <nts id="Seg_2442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2444" n="HIAT:w" s="T650">kiːrdegine</ts>
                  <nts id="Seg_2445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2447" n="HIAT:w" s="T651">kelbet</ts>
                  <nts id="Seg_2448" n="HIAT:ip">.</nts>
                  <nts id="Seg_2449" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T666" id="Seg_2451" n="HIAT:u" s="T652">
                  <ts e="T653" id="Seg_2453" n="HIAT:w" s="T652">Oː</ts>
                  <nts id="Seg_2454" n="HIAT:ip">,</nts>
                  <nts id="Seg_2455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2457" n="HIAT:w" s="T653">dʼe</ts>
                  <nts id="Seg_2458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2460" n="HIAT:w" s="T654">kajdak</ts>
                  <nts id="Seg_2461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2463" n="HIAT:w" s="T655">da</ts>
                  <nts id="Seg_2464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2466" n="HIAT:w" s="T656">gɨnɨ͡agɨn</ts>
                  <nts id="Seg_2467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2469" n="HIAT:w" s="T657">bert</ts>
                  <nts id="Seg_2470" n="HIAT:ip">,</nts>
                  <nts id="Seg_2471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2473" n="HIAT:w" s="T658">dʼe</ts>
                  <nts id="Seg_2474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2476" n="HIAT:w" s="T659">hürege</ts>
                  <nts id="Seg_2477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2479" n="HIAT:w" s="T660">ɨ͡arɨː</ts>
                  <nts id="Seg_2480" n="HIAT:ip">,</nts>
                  <nts id="Seg_2481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2483" n="HIAT:w" s="T661">ɨ͡aldʼar</ts>
                  <nts id="Seg_2484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2486" n="HIAT:w" s="T662">kihi</ts>
                  <nts id="Seg_2487" n="HIAT:ip">,</nts>
                  <nts id="Seg_2488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2490" n="HIAT:w" s="T663">dogorun</ts>
                  <nts id="Seg_2491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2493" n="HIAT:w" s="T664">ahɨnna</ts>
                  <nts id="Seg_2494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2496" n="HIAT:w" s="T665">diː</ts>
                  <nts id="Seg_2497" n="HIAT:ip">.</nts>
                  <nts id="Seg_2498" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T680" id="Seg_2500" n="HIAT:u" s="T666">
                  <ts e="T667" id="Seg_2502" n="HIAT:w" s="T666">Ahɨnan</ts>
                  <nts id="Seg_2503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2505" n="HIAT:w" s="T667">kajdak</ts>
                  <nts id="Seg_2506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2508" n="HIAT:w" s="T668">da</ts>
                  <nts id="Seg_2509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2511" n="HIAT:w" s="T669">gɨnɨ͡agɨn</ts>
                  <nts id="Seg_2512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2514" n="HIAT:w" s="T670">bert</ts>
                  <nts id="Seg_2515" n="HIAT:ip">,</nts>
                  <nts id="Seg_2516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2518" n="HIAT:w" s="T671">battɨ͡agɨn</ts>
                  <nts id="Seg_2519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2521" n="HIAT:w" s="T672">daː</ts>
                  <nts id="Seg_2522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2523" n="HIAT:ip">(</nts>
                  <nts id="Seg_2524" n="HIAT:ip">(</nts>
                  <ats e="T791" id="Seg_2525" n="HIAT:non-pho" s="T673">…</ats>
                  <nts id="Seg_2526" n="HIAT:ip">)</nts>
                  <nts id="Seg_2527" n="HIAT:ip">)</nts>
                  <nts id="Seg_2528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2530" n="HIAT:w" s="T791">baran</ts>
                  <nts id="Seg_2531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2533" n="HIAT:w" s="T674">da</ts>
                  <nts id="Seg_2534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2536" n="HIAT:w" s="T675">hin</ts>
                  <nts id="Seg_2537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2539" n="HIAT:w" s="T676">dʼi͡etiger</ts>
                  <nts id="Seg_2540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2542" n="HIAT:w" s="T677">tiji͡e</ts>
                  <nts id="Seg_2543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2545" n="HIAT:w" s="T678">hu͡ok</ts>
                  <nts id="Seg_2546" n="HIAT:ip">.</nts>
                  <nts id="Seg_2547" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T688" id="Seg_2549" n="HIAT:u" s="T680">
                  <nts id="Seg_2550" n="HIAT:ip">(</nts>
                  <nts id="Seg_2551" n="HIAT:ip">(</nts>
                  <ats e="T792" id="Seg_2552" n="HIAT:non-pho" s="T680">…</ats>
                  <nts id="Seg_2553" n="HIAT:ip">)</nts>
                  <nts id="Seg_2554" n="HIAT:ip">)</nts>
                  <nts id="Seg_2555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2557" n="HIAT:w" s="T792">ajɨːlaːgɨn</ts>
                  <nts id="Seg_2558" n="HIAT:ip">,</nts>
                  <nts id="Seg_2559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2560" n="HIAT:ip">"</nts>
                  <ts e="T682" id="Seg_2562" n="HIAT:w" s="T681">kiːr</ts>
                  <nts id="Seg_2563" n="HIAT:ip">"</nts>
                  <nts id="Seg_2564" n="HIAT:ip">,</nts>
                  <nts id="Seg_2565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2567" n="HIAT:w" s="T682">di͡ebit</ts>
                  <nts id="Seg_2568" n="HIAT:ip">,</nts>
                  <nts id="Seg_2569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2570" n="HIAT:ip">"</nts>
                  <ts e="T684" id="Seg_2572" n="HIAT:w" s="T683">ahaː</ts>
                  <nts id="Seg_2573" n="HIAT:ip">,</nts>
                  <nts id="Seg_2574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2576" n="HIAT:w" s="T684">kepseti͡ek</ts>
                  <nts id="Seg_2577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2579" n="HIAT:w" s="T685">oččogo</ts>
                  <nts id="Seg_2580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2582" n="HIAT:w" s="T686">üčügejdik</ts>
                  <nts id="Seg_2583" n="HIAT:ip">.</nts>
                  <nts id="Seg_2584" n="HIAT:ip">"</nts>
                  <nts id="Seg_2585" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T692" id="Seg_2587" n="HIAT:u" s="T688">
                  <ts e="T689" id="Seg_2589" n="HIAT:w" s="T688">Dʼe</ts>
                  <nts id="Seg_2590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2592" n="HIAT:w" s="T689">ilbi</ts>
                  <nts id="Seg_2593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2595" n="HIAT:w" s="T690">ile</ts>
                  <nts id="Seg_2596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2598" n="HIAT:w" s="T691">kepseppitter</ts>
                  <nts id="Seg_2599" n="HIAT:ip">.</nts>
                  <nts id="Seg_2600" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T699" id="Seg_2602" n="HIAT:u" s="T692">
                  <nts id="Seg_2603" n="HIAT:ip">"</nts>
                  <ts e="T693" id="Seg_2605" n="HIAT:w" s="T692">Kaja</ts>
                  <nts id="Seg_2606" n="HIAT:ip">,</nts>
                  <nts id="Seg_2607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2609" n="HIAT:w" s="T693">otto</ts>
                  <nts id="Seg_2610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2612" n="HIAT:w" s="T694">bihigi</ts>
                  <nts id="Seg_2613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2615" n="HIAT:w" s="T695">üčügejdik</ts>
                  <nts id="Seg_2616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2618" n="HIAT:w" s="T696">kepseti͡ek</ts>
                  <nts id="Seg_2619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2621" n="HIAT:w" s="T697">biːrge</ts>
                  <nts id="Seg_2622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2624" n="HIAT:w" s="T698">anɨ</ts>
                  <nts id="Seg_2625" n="HIAT:ip">.</nts>
                  <nts id="Seg_2626" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T707" id="Seg_2628" n="HIAT:u" s="T699">
                  <ts e="T700" id="Seg_2630" n="HIAT:w" s="T699">Biːrge</ts>
                  <nts id="Seg_2631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2633" n="HIAT:w" s="T700">oloru͡okput</ts>
                  <nts id="Seg_2634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2636" n="HIAT:w" s="T701">dʼe</ts>
                  <nts id="Seg_2637" n="HIAT:ip">,</nts>
                  <nts id="Seg_2638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2640" n="HIAT:w" s="T702">togo</ts>
                  <nts id="Seg_2641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_2643" n="HIAT:w" s="T703">ikki</ts>
                  <nts id="Seg_2644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2646" n="HIAT:w" s="T704">di͡ek</ts>
                  <nts id="Seg_2647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2649" n="HIAT:w" s="T705">olorobut</ts>
                  <nts id="Seg_2650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2652" n="HIAT:w" s="T706">bihigi</ts>
                  <nts id="Seg_2653" n="HIAT:ip">.</nts>
                  <nts id="Seg_2654" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T711" id="Seg_2656" n="HIAT:u" s="T707">
                  <ts e="T708" id="Seg_2658" n="HIAT:w" s="T707">Körögün</ts>
                  <nts id="Seg_2659" n="HIAT:ip">,</nts>
                  <nts id="Seg_2660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2662" n="HIAT:w" s="T708">min</ts>
                  <nts id="Seg_2663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2665" n="HIAT:w" s="T709">tabalannɨm</ts>
                  <nts id="Seg_2666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2668" n="HIAT:w" s="T710">össü͡ö</ts>
                  <nts id="Seg_2669" n="HIAT:ip">.</nts>
                  <nts id="Seg_2670" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T716" id="Seg_2672" n="HIAT:u" s="T711">
                  <ts e="T712" id="Seg_2674" n="HIAT:w" s="T711">En</ts>
                  <nts id="Seg_2675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2677" n="HIAT:w" s="T712">maːjdʼɨ</ts>
                  <nts id="Seg_2678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2680" n="HIAT:w" s="T713">minigin</ts>
                  <nts id="Seg_2681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2683" n="HIAT:w" s="T714">atagastɨː</ts>
                  <nts id="Seg_2684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2685" n="HIAT:ip">(</nts>
                  <nts id="Seg_2686" n="HIAT:ip">(</nts>
                  <ats e="T716" id="Seg_2687" n="HIAT:non-pho" s="T715">…</ats>
                  <nts id="Seg_2688" n="HIAT:ip">)</nts>
                  <nts id="Seg_2689" n="HIAT:ip">)</nts>
                  <nts id="Seg_2690" n="HIAT:ip">.</nts>
                  <nts id="Seg_2691" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T723" id="Seg_2693" n="HIAT:u" s="T716">
                  <ts e="T717" id="Seg_2695" n="HIAT:w" s="T716">Tu͡ok</ts>
                  <nts id="Seg_2696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2698" n="HIAT:w" s="T717">tabata</ts>
                  <nts id="Seg_2699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2701" n="HIAT:w" s="T718">taŋara</ts>
                  <nts id="Seg_2702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2704" n="HIAT:w" s="T719">egelbite</ts>
                  <nts id="Seg_2705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2707" n="HIAT:w" s="T720">dʼürü</ts>
                  <nts id="Seg_2708" n="HIAT:ip">,</nts>
                  <nts id="Seg_2709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_2711" n="HIAT:w" s="T721">tabanɨ</ts>
                  <nts id="Seg_2712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_2714" n="HIAT:w" s="T722">ahattɨm</ts>
                  <nts id="Seg_2715" n="HIAT:ip">.</nts>
                  <nts id="Seg_2716" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T730" id="Seg_2718" n="HIAT:u" s="T723">
                  <ts e="T724" id="Seg_2720" n="HIAT:w" s="T723">Dʼe</ts>
                  <nts id="Seg_2721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2723" n="HIAT:w" s="T724">anɨ</ts>
                  <nts id="Seg_2724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_2726" n="HIAT:w" s="T725">tabalanɨː</ts>
                  <nts id="Seg_2727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2729" n="HIAT:w" s="T726">biːrge</ts>
                  <nts id="Seg_2730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2732" n="HIAT:w" s="T727">oloru͡ok</ts>
                  <nts id="Seg_2733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2735" n="HIAT:w" s="T728">bihigi</ts>
                  <nts id="Seg_2736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2738" n="HIAT:w" s="T729">anɨ</ts>
                  <nts id="Seg_2739" n="HIAT:ip">.</nts>
                  <nts id="Seg_2740" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T739" id="Seg_2742" n="HIAT:u" s="T730">
                  <ts e="T731" id="Seg_2744" n="HIAT:w" s="T730">Biːrge</ts>
                  <nts id="Seg_2745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_2747" n="HIAT:w" s="T731">olorommut</ts>
                  <nts id="Seg_2748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2750" n="HIAT:w" s="T732">biːr</ts>
                  <nts id="Seg_2751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2753" n="HIAT:w" s="T733">hübennen</ts>
                  <nts id="Seg_2754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_2756" n="HIAT:w" s="T734">bultanɨ͡ak</ts>
                  <nts id="Seg_2757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_2759" n="HIAT:w" s="T735">i</ts>
                  <nts id="Seg_2760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_2762" n="HIAT:w" s="T736">biːr</ts>
                  <nts id="Seg_2763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_2765" n="HIAT:w" s="T737">dʼi͡eleːk</ts>
                  <nts id="Seg_2766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_2768" n="HIAT:w" s="T738">bu͡olu͡ok</ts>
                  <nts id="Seg_2769" n="HIAT:ip">.</nts>
                  <nts id="Seg_2770" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T755" id="Seg_2772" n="HIAT:u" s="T739">
                  <ts e="T740" id="Seg_2774" n="HIAT:w" s="T739">En</ts>
                  <nts id="Seg_2775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2777" n="HIAT:w" s="T740">dʼi͡egin</ts>
                  <nts id="Seg_2778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_2780" n="HIAT:w" s="T741">köhördö</ts>
                  <nts id="Seg_2781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_2783" n="HIAT:w" s="T742">egeli͡ek</ts>
                  <nts id="Seg_2784" n="HIAT:ip">,</nts>
                  <nts id="Seg_2785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_2787" n="HIAT:w" s="T743">bu</ts>
                  <nts id="Seg_2788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_2790" n="HIAT:w" s="T744">tabalaːkpɨn</ts>
                  <nts id="Seg_2791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_2793" n="HIAT:w" s="T745">diː</ts>
                  <nts id="Seg_2794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_2796" n="HIAT:w" s="T746">bu</ts>
                  <nts id="Seg_2797" n="HIAT:ip">,</nts>
                  <nts id="Seg_2798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_2800" n="HIAT:w" s="T747">biːr</ts>
                  <nts id="Seg_2801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_2803" n="HIAT:w" s="T748">tababɨt</ts>
                  <nts id="Seg_2804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_2806" n="HIAT:w" s="T749">tartaran</ts>
                  <nts id="Seg_2807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_2809" n="HIAT:w" s="T750">egeler</ts>
                  <nts id="Seg_2810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_2812" n="HIAT:w" s="T751">tu͡oktaːk</ts>
                  <nts id="Seg_2813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_2815" n="HIAT:w" s="T752">bu͡olu͡oj</ts>
                  <nts id="Seg_2816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_2818" n="HIAT:w" s="T753">i͡e</ts>
                  <nts id="Seg_2819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_2821" n="HIAT:w" s="T754">iti</ts>
                  <nts id="Seg_2822" n="HIAT:ip">.</nts>
                  <nts id="Seg_2823" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T765" id="Seg_2825" n="HIAT:u" s="T755">
                  <ts e="T756" id="Seg_2827" n="HIAT:w" s="T755">Egeli͡ek</ts>
                  <nts id="Seg_2828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_2830" n="HIAT:w" s="T756">manna</ts>
                  <nts id="Seg_2831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_2833" n="HIAT:w" s="T757">tabagɨn</ts>
                  <nts id="Seg_2834" n="HIAT:ip">,</nts>
                  <nts id="Seg_2835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_2837" n="HIAT:w" s="T758">eː</ts>
                  <nts id="Seg_2838" n="HIAT:ip">,</nts>
                  <nts id="Seg_2839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_2841" n="HIAT:w" s="T759">kimŋin</ts>
                  <nts id="Seg_2842" n="HIAT:ip">,</nts>
                  <nts id="Seg_2843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_2845" n="HIAT:w" s="T760">manna</ts>
                  <nts id="Seg_2846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2847" n="HIAT:ip">(</nts>
                  <ts e="T762" id="Seg_2849" n="HIAT:w" s="T761">biːr</ts>
                  <nts id="Seg_2850" n="HIAT:ip">)</nts>
                  <nts id="Seg_2851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_2853" n="HIAT:w" s="T762">biːrge</ts>
                  <nts id="Seg_2854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_2856" n="HIAT:w" s="T763">oloru͡ok</ts>
                  <nts id="Seg_2857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_2859" n="HIAT:w" s="T764">bihigi</ts>
                  <nts id="Seg_2860" n="HIAT:ip">.</nts>
                  <nts id="Seg_2861" n="HIAT:ip">"</nts>
                  <nts id="Seg_2862" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T770" id="Seg_2864" n="HIAT:u" s="T765">
                  <ts e="T766" id="Seg_2866" n="HIAT:w" s="T765">Hübelehenner</ts>
                  <nts id="Seg_2867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_2869" n="HIAT:w" s="T766">dʼe</ts>
                  <nts id="Seg_2870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T768" id="Seg_2872" n="HIAT:w" s="T767">biːrge</ts>
                  <nts id="Seg_2873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_2875" n="HIAT:w" s="T768">oloron</ts>
                  <nts id="Seg_2876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_2878" n="HIAT:w" s="T769">kaːlbɨttar</ts>
                  <nts id="Seg_2879" n="HIAT:ip">.</nts>
                  <nts id="Seg_2880" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T782" id="Seg_2882" n="HIAT:u" s="T770">
                  <ts e="T771" id="Seg_2884" n="HIAT:w" s="T770">Ikki͡ennere</ts>
                  <nts id="Seg_2885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_2887" n="HIAT:w" s="T771">biːr</ts>
                  <nts id="Seg_2888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_2890" n="HIAT:w" s="T772">hübe</ts>
                  <nts id="Seg_2891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_2893" n="HIAT:w" s="T773">bu͡olannar</ts>
                  <nts id="Seg_2894" n="HIAT:ip">,</nts>
                  <nts id="Seg_2895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_2897" n="HIAT:w" s="T774">nʼima</ts>
                  <nts id="Seg_2898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_2900" n="HIAT:w" s="T775">bu͡olannar</ts>
                  <nts id="Seg_2901" n="HIAT:ip">,</nts>
                  <nts id="Seg_2902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_2904" n="HIAT:w" s="T776">ikki͡ennere</ts>
                  <nts id="Seg_2905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_2907" n="HIAT:w" s="T777">dʼe</ts>
                  <nts id="Seg_2908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_2910" n="HIAT:w" s="T778">bajan-toton</ts>
                  <nts id="Seg_2911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_2913" n="HIAT:w" s="T779">olorbuttar</ts>
                  <nts id="Seg_2914" n="HIAT:ip">,</nts>
                  <nts id="Seg_2915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_2917" n="HIAT:w" s="T780">elete</ts>
                  <nts id="Seg_2918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_2920" n="HIAT:w" s="T781">iti</ts>
                  <nts id="Seg_2921" n="HIAT:ip">.</nts>
                  <nts id="Seg_2922" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T782" id="Seg_2923" n="sc" s="T0">
               <ts e="T1" id="Seg_2925" n="e" s="T0">Lörü͡önü </ts>
               <ts e="T2" id="Seg_2927" n="e" s="T1">gɨtta </ts>
               <ts e="T3" id="Seg_2929" n="e" s="T2">Tanʼaktaːk </ts>
               <ts e="T4" id="Seg_2931" n="e" s="T3">ogonnʼor </ts>
               <ts e="T5" id="Seg_2933" n="e" s="T4">olorbuttar </ts>
               <ts e="T6" id="Seg_2935" n="e" s="T5">ühü. </ts>
               <ts e="T7" id="Seg_2937" n="e" s="T6">ɨraːktɨː </ts>
               <ts e="T8" id="Seg_2939" n="e" s="T7">olorollor, </ts>
               <ts e="T9" id="Seg_2941" n="e" s="T8">beje </ts>
               <ts e="T10" id="Seg_2943" n="e" s="T9">bejelerin </ts>
               <ts e="T11" id="Seg_2945" n="e" s="T10">bilsiheller. </ts>
               <ts e="T12" id="Seg_2947" n="e" s="T11">Biːrgehe – </ts>
               <ts e="T13" id="Seg_2949" n="e" s="T12">ol </ts>
               <ts e="T14" id="Seg_2951" n="e" s="T13">di͡ek </ts>
               <ts e="T15" id="Seg_2953" n="e" s="T14">dʼi͡eleːk, </ts>
               <ts e="T16" id="Seg_2955" n="e" s="T15">biːrgehe – </ts>
               <ts e="T17" id="Seg_2957" n="e" s="T16">ba </ts>
               <ts e="T18" id="Seg_2959" n="e" s="T17">di͡ek </ts>
               <ts e="T19" id="Seg_2961" n="e" s="T18">dʼi͡eleːk. </ts>
               <ts e="T20" id="Seg_2963" n="e" s="T19">Oloronnor </ts>
               <ts e="T21" id="Seg_2965" n="e" s="T20">ikki͡ennere </ts>
               <ts e="T22" id="Seg_2967" n="e" s="T21">körsübütter, </ts>
               <ts e="T23" id="Seg_2969" n="e" s="T22">biːrge </ts>
               <ts e="T24" id="Seg_2971" n="e" s="T23">kepsespitter. </ts>
               <ts e="T25" id="Seg_2973" n="e" s="T24">"Kaː", </ts>
               <ts e="T26" id="Seg_2975" n="e" s="T25">(diːr) </ts>
               <ts e="T27" id="Seg_2977" n="e" s="T26">deheller </ts>
               <ts e="T28" id="Seg_2979" n="e" s="T27">Lörü͡ötün </ts>
               <ts e="T29" id="Seg_2981" n="e" s="T28">gɨtta </ts>
               <ts e="T30" id="Seg_2983" n="e" s="T29">Tanʼaktaːk </ts>
               <ts e="T31" id="Seg_2985" n="e" s="T30">ogonnʼor, </ts>
               <ts e="T32" id="Seg_2987" n="e" s="T31">"ka </ts>
               <ts e="T33" id="Seg_2989" n="e" s="T32">bihigi </ts>
               <ts e="T35" id="Seg_2991" n="e" s="T33">(bi-) </ts>
               <ts e="T36" id="Seg_2993" n="e" s="T35">biːr </ts>
               <ts e="T37" id="Seg_2995" n="e" s="T36">dʼɨlɨ </ts>
               <ts e="T38" id="Seg_2997" n="e" s="T37">bɨhar </ts>
               <ts e="T39" id="Seg_2999" n="e" s="T38">aspɨtɨn </ts>
               <ts e="T40" id="Seg_3001" n="e" s="T39">biːrge </ts>
               <ts e="T41" id="Seg_3003" n="e" s="T40">kolboːmmut </ts>
               <ts e="T42" id="Seg_3005" n="e" s="T41">ahɨ͡ak." </ts>
               <ts e="T43" id="Seg_3007" n="e" s="T42">"Baːdakpɨtɨna </ts>
               <ts e="T44" id="Seg_3009" n="e" s="T43">badaga </ts>
               <ts e="T46" id="Seg_3011" n="e" s="T44">(bi-) </ts>
               <ts e="T47" id="Seg_3013" n="e" s="T46">uhunnuk </ts>
               <ts e="T48" id="Seg_3015" n="e" s="T47">oloru͡okput, </ts>
               <ts e="T49" id="Seg_3017" n="e" s="T48">badaga, </ts>
               <ts e="T50" id="Seg_3019" n="e" s="T49">dʼɨlbɨtɨn </ts>
               <ts e="T51" id="Seg_3021" n="e" s="T50">bɨharbɨtɨgar </ts>
               <ts e="T52" id="Seg_3023" n="e" s="T51">daːgɨnɨ. </ts>
               <ts e="T53" id="Seg_3025" n="e" s="T52">Bu </ts>
               <ts e="T54" id="Seg_3027" n="e" s="T53">kuhagan </ts>
               <ts e="T55" id="Seg_3029" n="e" s="T54">dʼɨllar </ts>
               <ts e="T56" id="Seg_3031" n="e" s="T55">baːllar." </ts>
               <ts e="T57" id="Seg_3033" n="e" s="T56">"Dʼe, </ts>
               <ts e="T58" id="Seg_3035" n="e" s="T57">höp. </ts>
               <ts e="T59" id="Seg_3037" n="e" s="T58">Anɨ </ts>
               <ts e="T60" id="Seg_3039" n="e" s="T59">oččogo </ts>
               <ts e="T61" id="Seg_3041" n="e" s="T60">bihigi, </ts>
               <ts e="T62" id="Seg_3043" n="e" s="T61">Lörü͡ö, </ts>
               <ts e="T63" id="Seg_3045" n="e" s="T62">eni͡ene </ts>
               <ts e="T783" id="Seg_3047" n="e" s="T63">((…)) </ts>
               <ts e="T64" id="Seg_3049" n="e" s="T783">askɨn </ts>
               <ts e="T66" id="Seg_3051" n="e" s="T64">ahɨ͡ak." </ts>
               <ts e="T67" id="Seg_3053" n="e" s="T66">"Eː, </ts>
               <ts e="T68" id="Seg_3055" n="e" s="T67">Tanʼaktaːk </ts>
               <ts e="T69" id="Seg_3057" n="e" s="T68">ogonnʼor, </ts>
               <ts e="T70" id="Seg_3059" n="e" s="T69">gini </ts>
               <ts e="T71" id="Seg_3061" n="e" s="T70">ol, </ts>
               <ts e="T72" id="Seg_3063" n="e" s="T71">gini͡ene </ts>
               <ts e="T73" id="Seg_3065" n="e" s="T72">dʼi͡etiger </ts>
               <ts e="T74" id="Seg_3067" n="e" s="T73">barammɨt </ts>
               <ts e="T75" id="Seg_3069" n="e" s="T74">ahɨ͡ak." </ts>
               <ts e="T76" id="Seg_3071" n="e" s="T75">"Dʼe </ts>
               <ts e="T77" id="Seg_3073" n="e" s="T76">höp, </ts>
               <ts e="T78" id="Seg_3075" n="e" s="T77">innʼe </ts>
               <ts e="T79" id="Seg_3077" n="e" s="T78">ahɨ͡ak." </ts>
               <ts e="T80" id="Seg_3079" n="e" s="T79">Oː, </ts>
               <ts e="T81" id="Seg_3081" n="e" s="T80">dʼe </ts>
               <ts e="T82" id="Seg_3083" n="e" s="T81">ahɨːllar </ts>
               <ts e="T83" id="Seg_3085" n="e" s="T82">araj. </ts>
               <ts e="T84" id="Seg_3087" n="e" s="T83">Lörü͡ö </ts>
               <ts e="T85" id="Seg_3089" n="e" s="T84">dʼi͡etiger </ts>
               <ts e="T86" id="Seg_3091" n="e" s="T85">ahɨːllar. </ts>
               <ts e="T87" id="Seg_3093" n="e" s="T86">Biːr </ts>
               <ts e="T88" id="Seg_3095" n="e" s="T87">aŋarɨ, </ts>
               <ts e="T89" id="Seg_3097" n="e" s="T88">dʼe </ts>
               <ts e="T90" id="Seg_3099" n="e" s="T89">baranɨ͡agar </ts>
               <ts e="T91" id="Seg_3101" n="e" s="T90">di͡eri. </ts>
               <ts e="T92" id="Seg_3103" n="e" s="T91">Dʼe, </ts>
               <ts e="T93" id="Seg_3105" n="e" s="T92">ahaːnnar, </ts>
               <ts e="T94" id="Seg_3107" n="e" s="T93">ontularɨn </ts>
               <ts e="T95" id="Seg_3109" n="e" s="T94">astara </ts>
               <ts e="T96" id="Seg_3111" n="e" s="T95">baranna, </ts>
               <ts e="T97" id="Seg_3113" n="e" s="T96">Lörü͡ö </ts>
               <ts e="T98" id="Seg_3115" n="e" s="T97">gi͡ene. </ts>
               <ts e="T99" id="Seg_3117" n="e" s="T98">"Dʼe, </ts>
               <ts e="T100" id="Seg_3119" n="e" s="T99">Tanʼaktaːk </ts>
               <ts e="T101" id="Seg_3121" n="e" s="T100">ogonnʼorgo </ts>
               <ts e="T102" id="Seg_3123" n="e" s="T101">barɨ͡agɨŋ." </ts>
               <ts e="T103" id="Seg_3125" n="e" s="T102">Barannar </ts>
               <ts e="T104" id="Seg_3127" n="e" s="T103">dʼe </ts>
               <ts e="T105" id="Seg_3129" n="e" s="T104">ol </ts>
               <ts e="T106" id="Seg_3131" n="e" s="T105">ahɨn </ts>
               <ts e="T107" id="Seg_3133" n="e" s="T106">ahɨːhɨlar. </ts>
               <ts e="T108" id="Seg_3135" n="e" s="T107">Oː, </ts>
               <ts e="T109" id="Seg_3137" n="e" s="T108">Tanʼaktaːk </ts>
               <ts e="T110" id="Seg_3139" n="e" s="T109">ogonnʼorgo </ts>
               <ts e="T111" id="Seg_3141" n="e" s="T110">bardɨlar </ts>
               <ts e="T112" id="Seg_3143" n="e" s="T111">((PAUSE)) </ts>
               <ts e="T113" id="Seg_3145" n="e" s="T112">dʼe </ts>
               <ts e="T114" id="Seg_3147" n="e" s="T113">ahɨːllar. </ts>
               <ts e="T115" id="Seg_3149" n="e" s="T114">Ahɨːllar-ahɨːllar </ts>
               <ts e="T116" id="Seg_3151" n="e" s="T115">da. </ts>
               <ts e="T117" id="Seg_3153" n="e" s="T116">"Kajaː", </ts>
               <ts e="T118" id="Seg_3155" n="e" s="T117">biːr </ts>
               <ts e="T119" id="Seg_3157" n="e" s="T118">dogoro, </ts>
               <ts e="T120" id="Seg_3159" n="e" s="T119">"mini͡ene </ts>
               <ts e="T121" id="Seg_3161" n="e" s="T120">ahɨm </ts>
               <ts e="T122" id="Seg_3163" n="e" s="T121">baranar </ts>
               <ts e="T123" id="Seg_3165" n="e" s="T122">bu͡oltak", </ts>
               <ts e="T784" id="Seg_3167" n="e" s="T123">((…)) </ts>
               <ts e="T124" id="Seg_3169" n="e" s="T784">diːr, </ts>
               <ts e="T125" id="Seg_3171" n="e" s="T124">"en </ts>
               <ts e="T126" id="Seg_3173" n="e" s="T125">dʼi͡eger </ts>
               <ts e="T128" id="Seg_3175" n="e" s="T126">bar. </ts>
               <ts e="T129" id="Seg_3177" n="e" s="T128">Bejeŋ </ts>
               <ts e="T130" id="Seg_3179" n="e" s="T129">bultanan, </ts>
               <ts e="T131" id="Seg_3181" n="e" s="T130">min </ts>
               <ts e="T132" id="Seg_3183" n="e" s="T131">aspɨn </ts>
               <ts e="T133" id="Seg_3185" n="e" s="T132">barɨtɨn </ts>
               <ts e="T134" id="Seg_3187" n="e" s="T133">hi͡eppin </ts>
               <ts e="T135" id="Seg_3189" n="e" s="T134">bagarbappɨn." </ts>
               <ts e="T136" id="Seg_3191" n="e" s="T135">Eː, </ts>
               <ts e="T137" id="Seg_3193" n="e" s="T136">dʼe </ts>
               <ts e="T138" id="Seg_3195" n="e" s="T137">baran </ts>
               <ts e="T139" id="Seg_3197" n="e" s="T138">kaːlla </ts>
               <ts e="T140" id="Seg_3199" n="e" s="T139">dogoro. </ts>
               <ts e="T141" id="Seg_3201" n="e" s="T140">Kajaː, </ts>
               <ts e="T142" id="Seg_3203" n="e" s="T141">baran </ts>
               <ts e="T143" id="Seg_3205" n="e" s="T142">tu͡ogu </ts>
               <ts e="T144" id="Seg_3207" n="e" s="T143">hi͡egej </ts>
               <ts e="T145" id="Seg_3209" n="e" s="T144">dʼi͡etiger </ts>
               <ts e="T146" id="Seg_3211" n="e" s="T145">Löːrü͡ö? </ts>
               <ts e="T147" id="Seg_3213" n="e" s="T146">Barbɨta </ts>
               <ts e="T148" id="Seg_3215" n="e" s="T147">dʼi͡etiger, </ts>
               <ts e="T149" id="Seg_3217" n="e" s="T148">tu͡oga </ts>
               <ts e="T150" id="Seg_3219" n="e" s="T149">da </ts>
               <ts e="T151" id="Seg_3221" n="e" s="T150">hu͡ok, </ts>
               <ts e="T152" id="Seg_3223" n="e" s="T151">tu͡ok </ts>
               <ts e="T153" id="Seg_3225" n="e" s="T152">ere </ts>
               <ts e="T154" id="Seg_3227" n="e" s="T153">kačča </ts>
               <ts e="T155" id="Seg_3229" n="e" s="T154">bɨlčɨŋkaːnɨ </ts>
               <ts e="T156" id="Seg_3231" n="e" s="T155">bulunan, </ts>
               <ts e="T157" id="Seg_3233" n="e" s="T156">ontutun </ts>
               <ts e="T158" id="Seg_3235" n="e" s="T157">kü͡östenne. </ts>
               <ts e="T159" id="Seg_3237" n="e" s="T158">Kü͡östenen </ts>
               <ts e="T160" id="Seg_3239" n="e" s="T159">oloror </ts>
               <ts e="T161" id="Seg_3241" n="e" s="T160">araj. </ts>
               <ts e="T162" id="Seg_3243" n="e" s="T161">Kaː, </ts>
               <ts e="T163" id="Seg_3245" n="e" s="T162">onton </ts>
               <ts e="T164" id="Seg_3247" n="e" s="T163">körbüte, </ts>
               <ts e="T165" id="Seg_3249" n="e" s="T164">tugu </ts>
               <ts e="T166" id="Seg_3251" n="e" s="T165">ere </ts>
               <ts e="T167" id="Seg_3253" n="e" s="T166">bultammɨt </ts>
               <ts e="T168" id="Seg_3255" n="e" s="T167">emi͡e </ts>
               <ts e="T169" id="Seg_3257" n="e" s="T168">Löːrü͡ö. </ts>
               <ts e="T170" id="Seg_3259" n="e" s="T169">Onton </ts>
               <ts e="T171" id="Seg_3261" n="e" s="T170">maːjdʼɨn, </ts>
               <ts e="T172" id="Seg_3263" n="e" s="T171">Tanʼaktaːk </ts>
               <ts e="T173" id="Seg_3265" n="e" s="T172">ogonnʼor </ts>
               <ts e="T174" id="Seg_3267" n="e" s="T173">da </ts>
               <ts e="T175" id="Seg_3269" n="e" s="T174">aha </ts>
               <ts e="T176" id="Seg_3271" n="e" s="T175">barammɨt </ts>
               <ts e="T177" id="Seg_3273" n="e" s="T176">ete </ts>
               <ts e="T178" id="Seg_3275" n="e" s="T177">hopsi͡em. </ts>
               <ts e="T179" id="Seg_3277" n="e" s="T178">Dʼe </ts>
               <ts e="T180" id="Seg_3279" n="e" s="T179">oloror, </ts>
               <ts e="T181" id="Seg_3281" n="e" s="T180">bilsibet </ts>
               <ts e="T182" id="Seg_3283" n="e" s="T181">gini͡eke. </ts>
               <ts e="T183" id="Seg_3285" n="e" s="T182">"Dʼe </ts>
               <ts e="T184" id="Seg_3287" n="e" s="T183">kelbetin </ts>
               <ts e="T185" id="Seg_3289" n="e" s="T184">gini, </ts>
               <ts e="T186" id="Seg_3291" n="e" s="T185">bejem </ts>
               <ts e="T187" id="Seg_3293" n="e" s="T186">da </ts>
               <ts e="T188" id="Seg_3295" n="e" s="T187">bultanan </ts>
               <ts e="T189" id="Seg_3297" n="e" s="T188">oloroːn </ts>
               <ts e="T190" id="Seg_3299" n="e" s="T189">biːr </ts>
               <ts e="T191" id="Seg_3301" n="e" s="T190">tu͡okkaːn </ts>
               <ts e="T192" id="Seg_3303" n="e" s="T191">eme", </ts>
               <ts e="T193" id="Seg_3305" n="e" s="T192">diːr. </ts>
               <ts e="T194" id="Seg_3307" n="e" s="T193">Lörü͡ö </ts>
               <ts e="T195" id="Seg_3309" n="e" s="T194">araj </ts>
               <ts e="T196" id="Seg_3311" n="e" s="T195">oloror. </ts>
               <ts e="T197" id="Seg_3313" n="e" s="T196">Bu </ts>
               <ts e="T198" id="Seg_3315" n="e" s="T197">olordoguna </ts>
               <ts e="T199" id="Seg_3317" n="e" s="T198">Tanʼaktaːk </ts>
               <ts e="T200" id="Seg_3319" n="e" s="T199">ogonnʼor </ts>
               <ts e="T201" id="Seg_3321" n="e" s="T200">kelbit. </ts>
               <ts e="T202" id="Seg_3323" n="e" s="T201">Iherin </ts>
               <ts e="T203" id="Seg_3325" n="e" s="T202">kördö </ts>
               <ts e="T204" id="Seg_3327" n="e" s="T203">dogorun. </ts>
               <ts e="T205" id="Seg_3329" n="e" s="T204">"Bɨː, </ts>
               <ts e="T206" id="Seg_3331" n="e" s="T205">Tanʼaktaːk </ts>
               <ts e="T207" id="Seg_3333" n="e" s="T206">ogonnʼor </ts>
               <ts e="T208" id="Seg_3335" n="e" s="T207">iher </ts>
               <ts e="T209" id="Seg_3337" n="e" s="T208">ebit", </ts>
               <ts e="T210" id="Seg_3339" n="e" s="T209">Lörü͡ö </ts>
               <ts e="T211" id="Seg_3341" n="e" s="T210">diːr, </ts>
               <ts e="T785" id="Seg_3343" n="e" s="T211">((…)) </ts>
               <ts e="T212" id="Seg_3345" n="e" s="T785">dʼi͡etin </ts>
               <ts e="T213" id="Seg_3347" n="e" s="T212">katanan, </ts>
               <ts e="T214" id="Seg_3349" n="e" s="T213">haban </ts>
               <ts e="T216" id="Seg_3351" n="e" s="T214">keːspit. </ts>
               <ts e="T217" id="Seg_3353" n="e" s="T216">Dʼe </ts>
               <ts e="T218" id="Seg_3355" n="e" s="T217">keler </ts>
               <ts e="T219" id="Seg_3357" n="e" s="T218">araj. </ts>
               <ts e="T220" id="Seg_3359" n="e" s="T219">Aː, </ts>
               <ts e="T222" id="Seg_3361" n="e" s="T220">(Lö-) </ts>
               <ts e="T223" id="Seg_3363" n="e" s="T222">Lörü͡ötüger </ts>
               <ts e="T224" id="Seg_3365" n="e" s="T223">keler. </ts>
               <ts e="T225" id="Seg_3367" n="e" s="T224">"Lörü͡ö </ts>
               <ts e="T226" id="Seg_3369" n="e" s="T225">dogordoː, </ts>
               <ts e="T227" id="Seg_3371" n="e" s="T226">Lörü͡ö </ts>
               <ts e="T228" id="Seg_3373" n="e" s="T227">dogordoː. </ts>
               <ts e="T229" id="Seg_3375" n="e" s="T228">En </ts>
               <ts e="T230" id="Seg_3377" n="e" s="T229">bi͡er </ts>
               <ts e="T231" id="Seg_3379" n="e" s="T230">eme </ts>
               <ts e="T786" id="Seg_3381" n="e" s="T231">((…)) </ts>
               <ts e="T232" id="Seg_3383" n="e" s="T786">bɨldʼɨŋkaːnna </ts>
               <ts e="T233" id="Seg_3385" n="e" s="T232">bɨragan </ts>
               <ts e="T234" id="Seg_3387" n="e" s="T233">abɨraː, </ts>
               <ts e="T235" id="Seg_3389" n="e" s="T234">Löːrü͡ö </ts>
               <ts e="T237" id="Seg_3391" n="e" s="T235">dogordoː. </ts>
               <ts e="T238" id="Seg_3393" n="e" s="T237">Ardaːn, </ts>
               <ts e="T239" id="Seg_3395" n="e" s="T238">kümmer </ts>
               <ts e="T240" id="Seg_3397" n="e" s="T239">turda </ts>
               <ts e="T241" id="Seg_3399" n="e" s="T240">dʼɨlbɨn </ts>
               <ts e="T242" id="Seg_3401" n="e" s="T241">solgoː. </ts>
               <ts e="T243" id="Seg_3403" n="e" s="T242">Löːrü͡ö </ts>
               <ts e="T244" id="Seg_3405" n="e" s="T243">dogordoː, </ts>
               <ts e="T245" id="Seg_3407" n="e" s="T244">Löːrü͡ö </ts>
               <ts e="T246" id="Seg_3409" n="e" s="T245">dogordoː, </ts>
               <ts e="T247" id="Seg_3411" n="e" s="T246">öllüm </ts>
               <ts e="T248" id="Seg_3413" n="e" s="T247">kaːlɨ͡am", </ts>
               <ts e="T249" id="Seg_3415" n="e" s="T248">biːri, </ts>
               <ts e="T250" id="Seg_3417" n="e" s="T249">ɨllɨː </ts>
               <ts e="T251" id="Seg_3419" n="e" s="T250">olorbut </ts>
               <ts e="T252" id="Seg_3421" n="e" s="T251">törüt. </ts>
               <ts e="T253" id="Seg_3423" n="e" s="T252">"Kaː, </ts>
               <ts e="T254" id="Seg_3425" n="e" s="T253">bar-bar!" </ts>
               <ts e="T255" id="Seg_3427" n="e" s="T254">Biːr </ts>
               <ts e="T256" id="Seg_3429" n="e" s="T255">da </ts>
               <ts e="T257" id="Seg_3431" n="e" s="T256">biːr </ts>
               <ts e="T258" id="Seg_3433" n="e" s="T257">(tugu) </ts>
               <ts e="T259" id="Seg_3435" n="e" s="T258">bütteske </ts>
               <ts e="T260" id="Seg_3437" n="e" s="T259">hɨstɨbɨt, </ts>
               <ts e="T261" id="Seg_3439" n="e" s="T260">(kammɨt) </ts>
               <ts e="T262" id="Seg_3441" n="e" s="T261">kappɨt </ts>
               <ts e="T263" id="Seg_3443" n="e" s="T262">bɨlčɨŋkaːnɨ </ts>
               <ts e="T264" id="Seg_3445" n="e" s="T263">bɨragan </ts>
               <ts e="T265" id="Seg_3447" n="e" s="T264">keːste. </ts>
               <ts e="T266" id="Seg_3449" n="e" s="T265">"Bar", </ts>
               <ts e="T267" id="Seg_3451" n="e" s="T266">diːr </ts>
               <ts e="T268" id="Seg_3453" n="e" s="T267">ontuta, </ts>
               <ts e="T269" id="Seg_3455" n="e" s="T268">Tanʼaktaːk </ts>
               <ts e="T270" id="Seg_3457" n="e" s="T269">baran </ts>
               <ts e="T271" id="Seg_3459" n="e" s="T270">kaːlla. </ts>
               <ts e="T272" id="Seg_3461" n="e" s="T271">"(Bar </ts>
               <ts e="T273" id="Seg_3463" n="e" s="T272">dʼi͡etiger), </ts>
               <ts e="T274" id="Seg_3465" n="e" s="T273">bar </ts>
               <ts e="T275" id="Seg_3467" n="e" s="T274">dʼi͡eger", </ts>
               <ts e="T276" id="Seg_3469" n="e" s="T275">diːr, </ts>
               <ts e="T277" id="Seg_3471" n="e" s="T276">"bolse </ts>
               <ts e="T278" id="Seg_3473" n="e" s="T277">bi͡erbeppin </ts>
               <ts e="T279" id="Seg_3475" n="e" s="T278">eni͡eke." </ts>
               <ts e="T280" id="Seg_3477" n="e" s="T279">Koː, </ts>
               <ts e="T281" id="Seg_3479" n="e" s="T280">baran </ts>
               <ts e="T282" id="Seg_3481" n="e" s="T281">kaːlla </ts>
               <ts e="T283" id="Seg_3483" n="e" s="T282">kihi. </ts>
               <ts e="T284" id="Seg_3485" n="e" s="T283">Baran </ts>
               <ts e="T285" id="Seg_3487" n="e" s="T284">toholaːk </ts>
               <ts e="T286" id="Seg_3489" n="e" s="T285">či͡egej </ts>
               <ts e="T287" id="Seg_3491" n="e" s="T286">gini </ts>
               <ts e="T288" id="Seg_3493" n="e" s="T287">onton, </ts>
               <ts e="T289" id="Seg_3495" n="e" s="T288">ol-bu </ts>
               <ts e="T290" id="Seg_3497" n="e" s="T289">dʼe </ts>
               <ts e="T291" id="Seg_3499" n="e" s="T290">kaːnɨn </ts>
               <ts e="T292" id="Seg_3501" n="e" s="T291">(postu͡oj) </ts>
               <ts e="T293" id="Seg_3503" n="e" s="T292">postu͡oj </ts>
               <ts e="T294" id="Seg_3505" n="e" s="T293">minin </ts>
               <ts e="T295" id="Seg_3507" n="e" s="T294">agaj </ts>
               <ts e="T297" id="Seg_3509" n="e" s="T295">(kü-) </ts>
               <ts e="T298" id="Seg_3511" n="e" s="T297">kü͡östenen </ts>
               <ts e="T299" id="Seg_3513" n="e" s="T298">ši͡ebit </ts>
               <ts e="T300" id="Seg_3515" n="e" s="T299">da. </ts>
               <ts e="T301" id="Seg_3517" n="e" s="T300">Tu͡ok </ts>
               <ts e="T302" id="Seg_3519" n="e" s="T301">da </ts>
               <ts e="T303" id="Seg_3521" n="e" s="T302">bulbat, </ts>
               <ts e="T304" id="Seg_3523" n="e" s="T303">da </ts>
               <ts e="T305" id="Seg_3525" n="e" s="T304">kanʼaːbat </ts>
               <ts e="T306" id="Seg_3527" n="e" s="T305">daːganɨ. </ts>
               <ts e="T307" id="Seg_3529" n="e" s="T306">Kas </ts>
               <ts e="T308" id="Seg_3531" n="e" s="T307">da </ts>
               <ts e="T309" id="Seg_3533" n="e" s="T308">künü </ts>
               <ts e="T310" id="Seg_3535" n="e" s="T309">hɨppɨta </ts>
               <ts e="T311" id="Seg_3537" n="e" s="T310">da, </ts>
               <ts e="T312" id="Seg_3539" n="e" s="T311">araj </ts>
               <ts e="T313" id="Seg_3541" n="e" s="T312">da </ts>
               <ts e="T314" id="Seg_3543" n="e" s="T313">kaːman </ts>
               <ts e="T315" id="Seg_3545" n="e" s="T314">taksar </ts>
               <ts e="T316" id="Seg_3547" n="e" s="T315">bu͡olbut </ts>
               <ts e="T317" id="Seg_3549" n="e" s="T316">uhuguttan </ts>
               <ts e="T318" id="Seg_3551" n="e" s="T317">okton, </ts>
               <ts e="T319" id="Seg_3553" n="e" s="T318">Tanʼaktaːk </ts>
               <ts e="T320" id="Seg_3555" n="e" s="T319">ogonnʼor. </ts>
               <ts e="T321" id="Seg_3557" n="e" s="T320">Epeːt </ts>
               <ts e="T322" id="Seg_3559" n="e" s="T321">Löːrü͡ötüger </ts>
               <ts e="T323" id="Seg_3561" n="e" s="T322">barɨːhɨ. </ts>
               <ts e="T324" id="Seg_3563" n="e" s="T323">Kelen </ts>
               <ts e="T325" id="Seg_3565" n="e" s="T324">Löːrü͡ötüger </ts>
               <ts e="T326" id="Seg_3567" n="e" s="T325">emi͡e </ts>
               <ts e="T327" id="Seg_3569" n="e" s="T326">ɨllaːbɨt: </ts>
               <ts e="T328" id="Seg_3571" n="e" s="T327">"Löːrü͡ö </ts>
               <ts e="T329" id="Seg_3573" n="e" s="T328">dogordoː, </ts>
               <ts e="T330" id="Seg_3575" n="e" s="T329">Löːrü͡ö </ts>
               <ts e="T331" id="Seg_3577" n="e" s="T330">dogordoː. </ts>
               <ts e="T787" id="Seg_3579" n="e" s="T331">((…)) </ts>
               <ts e="T332" id="Seg_3581" n="e" s="T787">öllüm </ts>
               <ts e="T333" id="Seg_3583" n="e" s="T332">kaːlɨ͡am, </ts>
               <ts e="T334" id="Seg_3585" n="e" s="T333">öllüm </ts>
               <ts e="T336" id="Seg_3587" n="e" s="T334">kaːlɨ͡am. </ts>
               <ts e="T337" id="Seg_3589" n="e" s="T336">Löːrü͡ö </ts>
               <ts e="T338" id="Seg_3591" n="e" s="T337">dogordoː, </ts>
               <ts e="T339" id="Seg_3593" n="e" s="T338">biːr </ts>
               <ts e="T340" id="Seg_3595" n="e" s="T339">emeː </ts>
               <ts e="T341" id="Seg_3597" n="e" s="T340">((BREATH)) </ts>
               <ts e="T342" id="Seg_3599" n="e" s="T341">eme </ts>
               <ts e="T343" id="Seg_3601" n="e" s="T342">bɨlčɨŋkaːnna </ts>
               <ts e="T344" id="Seg_3603" n="e" s="T343">bɨragan </ts>
               <ts e="T345" id="Seg_3605" n="e" s="T344">abɨraː, </ts>
               <ts e="T346" id="Seg_3607" n="e" s="T345">Löːrü͡ö </ts>
               <ts e="T347" id="Seg_3609" n="e" s="T346">dogordoː, </ts>
               <ts e="T348" id="Seg_3611" n="e" s="T347">Löːrü͡ö </ts>
               <ts e="T349" id="Seg_3613" n="e" s="T348">dogordo. </ts>
               <ts e="T350" id="Seg_3615" n="e" s="T349">(Ardaːn), </ts>
               <ts e="T351" id="Seg_3617" n="e" s="T350">umnan </ts>
               <ts e="T788" id="Seg_3619" n="e" s="T351">((…)) </ts>
               <ts e="T352" id="Seg_3621" n="e" s="T788">bu͡olla </ts>
               <ts e="T353" id="Seg_3623" n="e" s="T352">(bulu͡o), </ts>
               <ts e="T354" id="Seg_3625" n="e" s="T353">Löːrü͡ö </ts>
               <ts e="T355" id="Seg_3627" n="e" s="T354">dogordoː, </ts>
               <ts e="T356" id="Seg_3629" n="e" s="T355">(ardaːn) </ts>
               <ts e="T357" id="Seg_3631" n="e" s="T356">bɨlčɨŋna </ts>
               <ts e="T358" id="Seg_3633" n="e" s="T357">egel, </ts>
               <ts e="T360" id="Seg_3635" n="e" s="T358">ɨllanabɨn." </ts>
               <ts e="T361" id="Seg_3637" n="e" s="T360">"E, </ts>
               <ts e="T362" id="Seg_3639" n="e" s="T361">biːr </ts>
               <ts e="T363" id="Seg_3641" n="e" s="T362">da </ts>
               <ts e="T364" id="Seg_3643" n="e" s="T363">bɨlčɨŋa </ts>
               <ts e="T365" id="Seg_3645" n="e" s="T364">hu͡okpun, </ts>
               <ts e="T366" id="Seg_3647" n="e" s="T365">bar, </ts>
               <ts e="T367" id="Seg_3649" n="e" s="T366">bar", </ts>
               <ts e="T368" id="Seg_3651" n="e" s="T367">di͡ebit, </ts>
               <ts e="T369" id="Seg_3653" n="e" s="T368">töttörü </ts>
               <ts e="T370" id="Seg_3655" n="e" s="T369">batan </ts>
               <ts e="T371" id="Seg_3657" n="e" s="T370">ɨːppɨt, </ts>
               <ts e="T372" id="Seg_3659" n="e" s="T371">tu͡okkaːnɨ </ts>
               <ts e="T373" id="Seg_3661" n="e" s="T372">da </ts>
               <ts e="T374" id="Seg_3663" n="e" s="T373">bi͡erbetek. </ts>
               <ts e="T375" id="Seg_3665" n="e" s="T374">Kajdi͡ek </ts>
               <ts e="T376" id="Seg_3667" n="e" s="T375">barɨ͡aj, </ts>
               <ts e="T377" id="Seg_3669" n="e" s="T376">tahaːra </ts>
               <ts e="T378" id="Seg_3671" n="e" s="T377">otto </ts>
               <ts e="T379" id="Seg_3673" n="e" s="T378">toŋor </ts>
               <ts e="T380" id="Seg_3675" n="e" s="T379">bu͡olla, </ts>
               <ts e="T381" id="Seg_3677" n="e" s="T380">kajdi͡et, </ts>
               <ts e="T382" id="Seg_3679" n="e" s="T381">dʼi͡ege </ts>
               <ts e="T383" id="Seg_3681" n="e" s="T382">da </ts>
               <ts e="T384" id="Seg_3683" n="e" s="T383">kiːllerbet </ts>
               <ts e="T385" id="Seg_3685" n="e" s="T384">saːt. </ts>
               <ts e="T386" id="Seg_3687" n="e" s="T385">Dʼe </ts>
               <ts e="T387" id="Seg_3689" n="e" s="T386">barar </ts>
               <ts e="T388" id="Seg_3691" n="e" s="T387">bu </ts>
               <ts e="T389" id="Seg_3693" n="e" s="T388">kihi. </ts>
               <ts e="T390" id="Seg_3695" n="e" s="T389">Barbɨta </ts>
               <ts e="T391" id="Seg_3697" n="e" s="T390">araččɨ </ts>
               <ts e="T392" id="Seg_3699" n="e" s="T391">onton </ts>
               <ts e="T393" id="Seg_3701" n="e" s="T392">tijen </ts>
               <ts e="T394" id="Seg_3703" n="e" s="T393">istegine, </ts>
               <ts e="T396" id="Seg_3705" n="e" s="T394">(dʼi͡en-) </ts>
               <ts e="T397" id="Seg_3707" n="e" s="T396">dʼi͡etin </ts>
               <ts e="T398" id="Seg_3709" n="e" s="T397">attɨgar </ts>
               <ts e="T399" id="Seg_3711" n="e" s="T398">tijer, </ts>
               <ts e="T400" id="Seg_3713" n="e" s="T399">körbüte, </ts>
               <ts e="T401" id="Seg_3715" n="e" s="T400">tuguttaːk </ts>
               <ts e="T402" id="Seg_3717" n="e" s="T401">tɨhɨ </ts>
               <ts e="T403" id="Seg_3719" n="e" s="T402">hɨldʼar </ts>
               <ts e="T404" id="Seg_3721" n="e" s="T403">dʼi͡etiger. </ts>
               <ts e="T405" id="Seg_3723" n="e" s="T404">Kaja </ts>
               <ts e="T406" id="Seg_3725" n="e" s="T405">keliːtten </ts>
               <ts e="T407" id="Seg_3727" n="e" s="T406">kelbite </ts>
               <ts e="T408" id="Seg_3729" n="e" s="T407">dʼürü </ts>
               <ts e="T409" id="Seg_3731" n="e" s="T408">bilbet </ts>
               <ts e="T410" id="Seg_3733" n="e" s="T409">tabanɨ </ts>
               <ts e="T411" id="Seg_3735" n="e" s="T410">gini </ts>
               <ts e="T789" id="Seg_3737" n="e" s="T411">((…)) </ts>
               <ts e="T412" id="Seg_3739" n="e" s="T789">taba </ts>
               <ts e="T413" id="Seg_3741" n="e" s="T412">hɨldʼar </ts>
               <ts e="T414" id="Seg_3743" n="e" s="T413">araj, </ts>
               <ts e="T416" id="Seg_3745" n="e" s="T414">kelbit. </ts>
               <ts e="T417" id="Seg_3747" n="e" s="T416">Örö </ts>
               <ts e="T418" id="Seg_3749" n="e" s="T417">gini </ts>
               <ts e="T419" id="Seg_3751" n="e" s="T418">ölü͡ök </ts>
               <ts e="T420" id="Seg_3753" n="e" s="T419">ɨjaːgɨgar, </ts>
               <ts e="T422" id="Seg_3755" n="e" s="T420">(öt-) </ts>
               <ts e="T423" id="Seg_3757" n="e" s="T422">tilli͡ek </ts>
               <ts e="T424" id="Seg_3759" n="e" s="T423">ɨjaːgɨgar </ts>
               <ts e="T425" id="Seg_3761" n="e" s="T424">tuguttaːk </ts>
               <ts e="T426" id="Seg_3763" n="e" s="T425">tɨhɨta </ts>
               <ts e="T427" id="Seg_3765" n="e" s="T426">bejetiger </ts>
               <ts e="T428" id="Seg_3767" n="e" s="T427">kelen </ts>
               <ts e="T429" id="Seg_3769" n="e" s="T428">halɨː </ts>
               <ts e="T430" id="Seg_3771" n="e" s="T429">hɨldʼɨbɨttar </ts>
               <ts e="T431" id="Seg_3773" n="e" s="T430">ginini. </ts>
               <ts e="T432" id="Seg_3775" n="e" s="T431">Iːkteːbitiger </ts>
               <ts e="T433" id="Seg_3777" n="e" s="T432">ihiger, </ts>
               <ts e="T434" id="Seg_3779" n="e" s="T433">kelbite </ts>
               <ts e="T435" id="Seg_3781" n="e" s="T434">da </ts>
               <ts e="T436" id="Seg_3783" n="e" s="T435">tugukkaːnɨn </ts>
               <ts e="T437" id="Seg_3785" n="e" s="T436">kaban </ts>
               <ts e="T438" id="Seg_3787" n="e" s="T437">ɨlan, </ts>
               <ts e="T439" id="Seg_3789" n="e" s="T438">bahakkaːnɨ </ts>
               <ts e="T440" id="Seg_3791" n="e" s="T439">ɨlla </ts>
               <ts e="T441" id="Seg_3793" n="e" s="T440">da </ts>
               <ts e="T442" id="Seg_3795" n="e" s="T441">"kort" </ts>
               <ts e="T443" id="Seg_3797" n="e" s="T442">gɨnnaran </ts>
               <ts e="T444" id="Seg_3799" n="e" s="T443">ölörön </ts>
               <ts e="T445" id="Seg_3801" n="e" s="T444">keːspit. </ts>
               <ts e="T446" id="Seg_3803" n="e" s="T445">Inʼete </ts>
               <ts e="T447" id="Seg_3805" n="e" s="T446">hɨldʼar. </ts>
               <ts e="T448" id="Seg_3807" n="e" s="T447">Ko, </ts>
               <ts e="T449" id="Seg_3809" n="e" s="T448">mantɨtɨn </ts>
               <ts e="T450" id="Seg_3811" n="e" s="T449">(tu͡ok) </ts>
               <ts e="T451" id="Seg_3813" n="e" s="T450">tu͡ok, </ts>
               <ts e="T453" id="Seg_3815" n="e" s="T451">(hülü-) </ts>
               <ts e="T454" id="Seg_3817" n="e" s="T453">hülümmüt </ts>
               <ts e="T455" id="Seg_3819" n="e" s="T454">kihi </ts>
               <ts e="T456" id="Seg_3821" n="e" s="T455">hiːkejdiː </ts>
               <ts e="T457" id="Seg_3823" n="e" s="T456">buharan, </ts>
               <ts e="T458" id="Seg_3825" n="e" s="T457">kü͡östenen </ts>
               <ts e="T459" id="Seg_3827" n="e" s="T458">hi͡en </ts>
               <ts e="T460" id="Seg_3829" n="e" s="T459">keːspit. </ts>
               <ts e="T461" id="Seg_3831" n="e" s="T460">Dʼe </ts>
               <ts e="T462" id="Seg_3833" n="e" s="T461">karaga </ts>
               <ts e="T463" id="Seg_3835" n="e" s="T462">hɨrdaːbɨt </ts>
               <ts e="T464" id="Seg_3837" n="e" s="T463">onno. </ts>
               <ts e="T465" id="Seg_3839" n="e" s="T464">"Oː, </ts>
               <ts e="T466" id="Seg_3841" n="e" s="T465">dʼe </ts>
               <ts e="T467" id="Seg_3843" n="e" s="T466">anɨ </ts>
               <ts e="T468" id="Seg_3845" n="e" s="T467">bagas </ts>
               <ts e="T469" id="Seg_3847" n="e" s="T468">abɨrannɨm, </ts>
               <ts e="T470" id="Seg_3849" n="e" s="T469">Löːrü͡öge </ts>
               <ts e="T471" id="Seg_3851" n="e" s="T470">barbappɨn, </ts>
               <ts e="T472" id="Seg_3853" n="e" s="T471">kanʼaːbappɨn, </ts>
               <ts e="T473" id="Seg_3855" n="e" s="T472">astannɨm </ts>
               <ts e="T474" id="Seg_3857" n="e" s="T473">bu͡o", </ts>
               <ts e="T475" id="Seg_3859" n="e" s="T474">diːr. </ts>
               <ts e="T476" id="Seg_3861" n="e" s="T475">Astammɨt </ts>
               <ts e="T477" id="Seg_3863" n="e" s="T476">kihi </ts>
               <ts e="T478" id="Seg_3865" n="e" s="T477">kü͡östenen </ts>
               <ts e="T479" id="Seg_3867" n="e" s="T478">baraːn </ts>
               <ts e="T480" id="Seg_3869" n="e" s="T479">ahɨːr. </ts>
               <ts e="T481" id="Seg_3871" n="e" s="T480">Oːk. </ts>
               <ts e="T482" id="Seg_3873" n="e" s="T481">Dʼeː </ts>
               <ts e="T483" id="Seg_3875" n="e" s="T482">kas </ts>
               <ts e="T484" id="Seg_3877" n="e" s="T483">künü </ts>
               <ts e="T485" id="Seg_3879" n="e" s="T484">olorbuta </ts>
               <ts e="T486" id="Seg_3881" n="e" s="T485">dʼürü. </ts>
               <ts e="T487" id="Seg_3883" n="e" s="T486">Tabalaːk, </ts>
               <ts e="T488" id="Seg_3885" n="e" s="T487">tabatɨn </ts>
               <ts e="T489" id="Seg_3887" n="e" s="T488">itte </ts>
               <ts e="T490" id="Seg_3889" n="e" s="T489">tuttubut, </ts>
               <ts e="T491" id="Seg_3891" n="e" s="T490">tɨhɨtɨn. </ts>
               <ts e="T492" id="Seg_3893" n="e" s="T491">Tɨhɨlaːk, </ts>
               <ts e="T493" id="Seg_3895" n="e" s="T492">tu͡ok </ts>
               <ts e="T494" id="Seg_3897" n="e" s="T493">bu͡olu͡oj </ts>
               <ts e="T495" id="Seg_3899" n="e" s="T494">anɨ. </ts>
               <ts e="T496" id="Seg_3901" n="e" s="T495">Oː, </ts>
               <ts e="T497" id="Seg_3903" n="e" s="T496">bu </ts>
               <ts e="T498" id="Seg_3905" n="e" s="T497">hɨttagɨna </ts>
               <ts e="T499" id="Seg_3907" n="e" s="T498">Löːrü͡ötün </ts>
               <ts e="T500" id="Seg_3909" n="e" s="T499">haŋata </ts>
               <ts e="T501" id="Seg_3911" n="e" s="T500">kelle. </ts>
               <ts e="T502" id="Seg_3913" n="e" s="T501">Dʼe </ts>
               <ts e="T503" id="Seg_3915" n="e" s="T502">araj </ts>
               <ts e="T504" id="Seg_3917" n="e" s="T503">körbüte. </ts>
               <ts e="T505" id="Seg_3919" n="e" s="T504">"Dʼe </ts>
               <ts e="T506" id="Seg_3921" n="e" s="T505">min </ts>
               <ts e="T507" id="Seg_3923" n="e" s="T506">anɨ </ts>
               <ts e="T508" id="Seg_3925" n="e" s="T507">itigirdik </ts>
               <ts e="T509" id="Seg_3927" n="e" s="T508">gɨnɨ͡am, </ts>
               <ts e="T510" id="Seg_3929" n="e" s="T509">beːbe </ts>
               <ts e="T511" id="Seg_3931" n="e" s="T510">kellin </ts>
               <ts e="T512" id="Seg_3933" n="e" s="T511">agaj", </ts>
               <ts e="T513" id="Seg_3935" n="e" s="T512">di͡ebit, </ts>
               <ts e="T514" id="Seg_3937" n="e" s="T513">"mini͡eke </ts>
               <ts e="T515" id="Seg_3939" n="e" s="T514">as". </ts>
               <ts e="T516" id="Seg_3941" n="e" s="T515">Löːrü͡ö </ts>
               <ts e="T517" id="Seg_3943" n="e" s="T516">tu͡oktaːn </ts>
               <ts e="T518" id="Seg_3945" n="e" s="T517">arɨččɨ </ts>
               <ts e="T519" id="Seg_3947" n="e" s="T518">iher </ts>
               <ts e="T520" id="Seg_3949" n="e" s="T519">bu͡opsa </ts>
               <ts e="T521" id="Seg_3951" n="e" s="T520">korgujan. </ts>
               <ts e="T522" id="Seg_3953" n="e" s="T521">Araččɨ </ts>
               <ts e="T523" id="Seg_3955" n="e" s="T522">kaːman </ts>
               <ts e="T524" id="Seg_3957" n="e" s="T523">iher. </ts>
               <ts e="T525" id="Seg_3959" n="e" s="T524">Kelbit </ts>
               <ts e="T526" id="Seg_3961" n="e" s="T525">da </ts>
               <ts e="T527" id="Seg_3963" n="e" s="T526">kelbit. </ts>
               <ts e="T528" id="Seg_3965" n="e" s="T527">Kelerin </ts>
               <ts e="T529" id="Seg_3967" n="e" s="T528">gɨtta </ts>
               <ts e="T530" id="Seg_3969" n="e" s="T529">dʼi͡ete </ts>
               <ts e="T531" id="Seg_3971" n="e" s="T530">kataːn, </ts>
               <ts e="T532" id="Seg_3973" n="e" s="T531">katanan </ts>
               <ts e="T533" id="Seg_3975" n="e" s="T532">keːspit. </ts>
               <ts e="T534" id="Seg_3977" n="e" s="T533">"Dʼe </ts>
               <ts e="T535" id="Seg_3979" n="e" s="T534">ginini </ts>
               <ts e="T536" id="Seg_3981" n="e" s="T535">nakaːstɨ͡am, </ts>
               <ts e="T537" id="Seg_3983" n="e" s="T536">maːjdʼɨn </ts>
               <ts e="T538" id="Seg_3985" n="e" s="T537">minigin </ts>
               <ts e="T539" id="Seg_3987" n="e" s="T538">emi͡e </ts>
               <ts e="T540" id="Seg_3989" n="e" s="T539">gini </ts>
               <ts e="T541" id="Seg_3991" n="e" s="T540">muŋnaːbɨta </ts>
               <ts e="T542" id="Seg_3993" n="e" s="T541">burduk." </ts>
               <ts e="T543" id="Seg_3995" n="e" s="T542">"Tanʼaktaːk </ts>
               <ts e="T544" id="Seg_3997" n="e" s="T543">ogonnʼor, </ts>
               <ts e="T545" id="Seg_3999" n="e" s="T544">Tanʼaktaːk </ts>
               <ts e="T546" id="Seg_4001" n="e" s="T545">ogonnʼor. </ts>
               <ts e="T547" id="Seg_4003" n="e" s="T546">Biːr </ts>
               <ts e="T790" id="Seg_4005" n="e" s="T547">((…)) </ts>
               <ts e="T548" id="Seg_4007" n="e" s="T790">emete </ts>
               <ts e="T549" id="Seg_4009" n="e" s="T548">bɨlčɨŋkaːnna </ts>
               <ts e="T550" id="Seg_4011" n="e" s="T549">bi͡eren </ts>
               <ts e="T551" id="Seg_4013" n="e" s="T550">abɨraː, </ts>
               <ts e="T552" id="Seg_4015" n="e" s="T551">Tanʼaktaːk </ts>
               <ts e="T553" id="Seg_4017" n="e" s="T552">ogonnʼor, </ts>
               <ts e="T554" id="Seg_4019" n="e" s="T553">öllüm </ts>
               <ts e="T555" id="Seg_4021" n="e" s="T554">kaːllɨm, </ts>
               <ts e="T556" id="Seg_4023" n="e" s="T555">öllüm </ts>
               <ts e="T558" id="Seg_4025" n="e" s="T556">(kaː-) </ts>
               <ts e="T560" id="Seg_4027" n="e" s="T558">kaːllɨm. </ts>
               <ts e="T561" id="Seg_4029" n="e" s="T560">Kümmer </ts>
               <ts e="T562" id="Seg_4031" n="e" s="T561">turda </ts>
               <ts e="T563" id="Seg_4033" n="e" s="T562">kuŋ </ts>
               <ts e="T564" id="Seg_4035" n="e" s="T563">kihite </ts>
               <ts e="T565" id="Seg_4037" n="e" s="T564">bu͡ollakkɨna, </ts>
               <ts e="T566" id="Seg_4039" n="e" s="T565">Tanʼaktaːk </ts>
               <ts e="T567" id="Seg_4041" n="e" s="T566">ogonnʼor, </ts>
               <ts e="T568" id="Seg_4043" n="e" s="T567">Tanʼaktaːk </ts>
               <ts e="T569" id="Seg_4045" n="e" s="T568">ogonnʼor", </ts>
               <ts e="T570" id="Seg_4047" n="e" s="T569">ɨllana </ts>
               <ts e="T571" id="Seg_4049" n="e" s="T570">hɨppɨt. </ts>
               <ts e="T572" id="Seg_4051" n="e" s="T571">Tugu </ts>
               <ts e="T573" id="Seg_4053" n="e" s="T572">da </ts>
               <ts e="T574" id="Seg_4055" n="e" s="T573">bi͡erbetek. </ts>
               <ts e="T575" id="Seg_4057" n="e" s="T574">Postu͡oj </ts>
               <ts e="T576" id="Seg_4059" n="e" s="T575">tu͡ok </ts>
               <ts e="T577" id="Seg_4061" n="e" s="T576">ere </ts>
               <ts e="T578" id="Seg_4063" n="e" s="T577">bu </ts>
               <ts e="T579" id="Seg_4065" n="e" s="T578">tugutun </ts>
               <ts e="T580" id="Seg_4067" n="e" s="T579">gi͡enin </ts>
               <ts e="T581" id="Seg_4069" n="e" s="T580">(tunʼaga) </ts>
               <ts e="T582" id="Seg_4071" n="e" s="T581">tunʼaktarɨn </ts>
               <ts e="T583" id="Seg_4073" n="e" s="T582">tu͡oktammɨt, </ts>
               <ts e="T584" id="Seg_4075" n="e" s="T583">atatɨn </ts>
               <ts e="T585" id="Seg_4077" n="e" s="T584">gɨtta </ts>
               <ts e="T586" id="Seg_4079" n="e" s="T585">tunʼaga </ts>
               <ts e="T587" id="Seg_4081" n="e" s="T586">duː, </ts>
               <ts e="T588" id="Seg_4083" n="e" s="T587">tu͡ok </ts>
               <ts e="T589" id="Seg_4085" n="e" s="T588">duː, </ts>
               <ts e="T590" id="Seg_4087" n="e" s="T589">onu </ts>
               <ts e="T592" id="Seg_4089" n="e" s="T590">(het-) </ts>
               <ts e="T593" id="Seg_4091" n="e" s="T592">atanaːk </ts>
               <ts e="T594" id="Seg_4093" n="e" s="T593">hɨldʼan </ts>
               <ts e="T595" id="Seg_4095" n="e" s="T594">onu </ts>
               <ts e="T596" id="Seg_4097" n="e" s="T595">bɨragan. </ts>
               <ts e="T597" id="Seg_4099" n="e" s="T596">"Bar, </ts>
               <ts e="T598" id="Seg_4101" n="e" s="T597">ši͡e", </ts>
               <ts e="T599" id="Seg_4103" n="e" s="T598">diː </ts>
               <ts e="T600" id="Seg_4105" n="e" s="T599">ikki </ts>
               <ts e="T601" id="Seg_4107" n="e" s="T600">tunʼagɨ </ts>
               <ts e="T602" id="Seg_4109" n="e" s="T601">bɨragan </ts>
               <ts e="T603" id="Seg_4111" n="e" s="T602">keːspit. </ts>
               <ts e="T604" id="Seg_4113" n="e" s="T603">"Bar </ts>
               <ts e="T605" id="Seg_4115" n="e" s="T604">dʼi͡eger", </ts>
               <ts e="T606" id="Seg_4117" n="e" s="T605">diːr, </ts>
               <ts e="T607" id="Seg_4119" n="e" s="T606">"bolʼše </ts>
               <ts e="T608" id="Seg_4121" n="e" s="T607">kelime, </ts>
               <ts e="T610" id="Seg_4123" n="e" s="T608">(mi-) </ts>
               <ts e="T611" id="Seg_4125" n="e" s="T610">maːjdɨn </ts>
               <ts e="T612" id="Seg_4127" n="e" s="T611">minigin </ts>
               <ts e="T613" id="Seg_4129" n="e" s="T612">emi͡e </ts>
               <ts e="T614" id="Seg_4131" n="e" s="T613">dʼe </ts>
               <ts e="T615" id="Seg_4133" n="e" s="T614">muŋnaːbɨtɨŋ." </ts>
               <ts e="T616" id="Seg_4135" n="e" s="T615">Ontuta </ts>
               <ts e="T617" id="Seg_4137" n="e" s="T616">bɨstɨ͡a </ts>
               <ts e="T618" id="Seg_4139" n="e" s="T617">du͡o, </ts>
               <ts e="T619" id="Seg_4141" n="e" s="T618">baran </ts>
               <ts e="T620" id="Seg_4143" n="e" s="T619">kaːlbɨt. </ts>
               <ts e="T621" id="Seg_4145" n="e" s="T620">Dʼe </ts>
               <ts e="T622" id="Seg_4147" n="e" s="T621">baran </ts>
               <ts e="T623" id="Seg_4149" n="e" s="T622">kaːlbɨt </ts>
               <ts e="T624" id="Seg_4151" n="e" s="T623">dʼi͡etiger. </ts>
               <ts e="T625" id="Seg_4153" n="e" s="T624">Emi͡e </ts>
               <ts e="T626" id="Seg_4155" n="e" s="T625">((PAUSE)) </ts>
               <ts e="T627" id="Seg_4157" n="e" s="T626">barbɨt </ts>
               <ts e="T628" id="Seg_4159" n="e" s="T627">da </ts>
               <ts e="T629" id="Seg_4161" n="e" s="T628">hartɨn </ts>
               <ts e="T631" id="Seg_4163" n="e" s="T629">(ka-) </ts>
               <ts e="T632" id="Seg_4165" n="e" s="T631">kas </ts>
               <ts e="T633" id="Seg_4167" n="e" s="T632">da </ts>
               <ts e="T634" id="Seg_4169" n="e" s="T633">örö </ts>
               <ts e="T635" id="Seg_4171" n="e" s="T634">(günü) </ts>
               <ts e="T636" id="Seg_4173" n="e" s="T635">bu͡olan </ts>
               <ts e="T637" id="Seg_4175" n="e" s="T636">baraːn </ts>
               <ts e="T638" id="Seg_4177" n="e" s="T637">emi͡e </ts>
               <ts e="T639" id="Seg_4179" n="e" s="T638">keler. </ts>
               <ts e="T640" id="Seg_4181" n="e" s="T639">Dʼe </ts>
               <ts e="T641" id="Seg_4183" n="e" s="T640">vavsʼe </ts>
               <ts e="T642" id="Seg_4185" n="e" s="T641">ölörüger </ts>
               <ts e="T643" id="Seg_4187" n="e" s="T642">barbɨt, </ts>
               <ts e="T644" id="Seg_4189" n="e" s="T643">bɨhɨlaːk </ts>
               <ts e="T645" id="Seg_4191" n="e" s="T644">dogoro, </ts>
               <ts e="T646" id="Seg_4193" n="e" s="T645">Löːrü͡öte </ts>
               <ts e="T647" id="Seg_4195" n="e" s="T646">ɨksaːbɨt: </ts>
               <ts e="T648" id="Seg_4197" n="e" s="T647">Battagɨna </ts>
               <ts e="T649" id="Seg_4199" n="e" s="T648">da </ts>
               <ts e="T650" id="Seg_4201" n="e" s="T649">barbat, </ts>
               <ts e="T651" id="Seg_4203" n="e" s="T650">kiːrdegine </ts>
               <ts e="T652" id="Seg_4205" n="e" s="T651">kelbet. </ts>
               <ts e="T653" id="Seg_4207" n="e" s="T652">Oː, </ts>
               <ts e="T654" id="Seg_4209" n="e" s="T653">dʼe </ts>
               <ts e="T655" id="Seg_4211" n="e" s="T654">kajdak </ts>
               <ts e="T656" id="Seg_4213" n="e" s="T655">da </ts>
               <ts e="T657" id="Seg_4215" n="e" s="T656">gɨnɨ͡agɨn </ts>
               <ts e="T658" id="Seg_4217" n="e" s="T657">bert, </ts>
               <ts e="T659" id="Seg_4219" n="e" s="T658">dʼe </ts>
               <ts e="T660" id="Seg_4221" n="e" s="T659">hürege </ts>
               <ts e="T661" id="Seg_4223" n="e" s="T660">ɨ͡arɨː, </ts>
               <ts e="T662" id="Seg_4225" n="e" s="T661">ɨ͡aldʼar </ts>
               <ts e="T663" id="Seg_4227" n="e" s="T662">kihi, </ts>
               <ts e="T664" id="Seg_4229" n="e" s="T663">dogorun </ts>
               <ts e="T665" id="Seg_4231" n="e" s="T664">ahɨnna </ts>
               <ts e="T666" id="Seg_4233" n="e" s="T665">diː. </ts>
               <ts e="T667" id="Seg_4235" n="e" s="T666">Ahɨnan </ts>
               <ts e="T668" id="Seg_4237" n="e" s="T667">kajdak </ts>
               <ts e="T669" id="Seg_4239" n="e" s="T668">da </ts>
               <ts e="T670" id="Seg_4241" n="e" s="T669">gɨnɨ͡agɨn </ts>
               <ts e="T671" id="Seg_4243" n="e" s="T670">bert, </ts>
               <ts e="T672" id="Seg_4245" n="e" s="T671">battɨ͡agɨn </ts>
               <ts e="T673" id="Seg_4247" n="e" s="T672">daː </ts>
               <ts e="T791" id="Seg_4249" n="e" s="T673">((…)) </ts>
               <ts e="T674" id="Seg_4251" n="e" s="T791">baran </ts>
               <ts e="T675" id="Seg_4253" n="e" s="T674">da </ts>
               <ts e="T676" id="Seg_4255" n="e" s="T675">hin </ts>
               <ts e="T677" id="Seg_4257" n="e" s="T676">dʼi͡etiger </ts>
               <ts e="T678" id="Seg_4259" n="e" s="T677">tiji͡e </ts>
               <ts e="T680" id="Seg_4261" n="e" s="T678">hu͡ok. </ts>
               <ts e="T792" id="Seg_4263" n="e" s="T680">((…)) </ts>
               <ts e="T681" id="Seg_4265" n="e" s="T792">ajɨːlaːgɨn, </ts>
               <ts e="T682" id="Seg_4267" n="e" s="T681">"kiːr", </ts>
               <ts e="T683" id="Seg_4269" n="e" s="T682">di͡ebit, </ts>
               <ts e="T684" id="Seg_4271" n="e" s="T683">"ahaː, </ts>
               <ts e="T685" id="Seg_4273" n="e" s="T684">kepseti͡ek </ts>
               <ts e="T686" id="Seg_4275" n="e" s="T685">oččogo </ts>
               <ts e="T688" id="Seg_4277" n="e" s="T686">üčügejdik." </ts>
               <ts e="T689" id="Seg_4279" n="e" s="T688">Dʼe </ts>
               <ts e="T690" id="Seg_4281" n="e" s="T689">ilbi </ts>
               <ts e="T691" id="Seg_4283" n="e" s="T690">ile </ts>
               <ts e="T692" id="Seg_4285" n="e" s="T691">kepseppitter. </ts>
               <ts e="T693" id="Seg_4287" n="e" s="T692">"Kaja, </ts>
               <ts e="T694" id="Seg_4289" n="e" s="T693">otto </ts>
               <ts e="T695" id="Seg_4291" n="e" s="T694">bihigi </ts>
               <ts e="T696" id="Seg_4293" n="e" s="T695">üčügejdik </ts>
               <ts e="T697" id="Seg_4295" n="e" s="T696">kepseti͡ek </ts>
               <ts e="T698" id="Seg_4297" n="e" s="T697">biːrge </ts>
               <ts e="T699" id="Seg_4299" n="e" s="T698">anɨ. </ts>
               <ts e="T700" id="Seg_4301" n="e" s="T699">Biːrge </ts>
               <ts e="T701" id="Seg_4303" n="e" s="T700">oloru͡okput </ts>
               <ts e="T702" id="Seg_4305" n="e" s="T701">dʼe, </ts>
               <ts e="T703" id="Seg_4307" n="e" s="T702">togo </ts>
               <ts e="T704" id="Seg_4309" n="e" s="T703">ikki </ts>
               <ts e="T705" id="Seg_4311" n="e" s="T704">di͡ek </ts>
               <ts e="T706" id="Seg_4313" n="e" s="T705">olorobut </ts>
               <ts e="T707" id="Seg_4315" n="e" s="T706">bihigi. </ts>
               <ts e="T708" id="Seg_4317" n="e" s="T707">Körögün, </ts>
               <ts e="T709" id="Seg_4319" n="e" s="T708">min </ts>
               <ts e="T710" id="Seg_4321" n="e" s="T709">tabalannɨm </ts>
               <ts e="T711" id="Seg_4323" n="e" s="T710">össü͡ö. </ts>
               <ts e="T712" id="Seg_4325" n="e" s="T711">En </ts>
               <ts e="T713" id="Seg_4327" n="e" s="T712">maːjdʼɨ </ts>
               <ts e="T714" id="Seg_4329" n="e" s="T713">minigin </ts>
               <ts e="T715" id="Seg_4331" n="e" s="T714">atagastɨː </ts>
               <ts e="T716" id="Seg_4333" n="e" s="T715">((…)). </ts>
               <ts e="T717" id="Seg_4335" n="e" s="T716">Tu͡ok </ts>
               <ts e="T718" id="Seg_4337" n="e" s="T717">tabata </ts>
               <ts e="T719" id="Seg_4339" n="e" s="T718">taŋara </ts>
               <ts e="T720" id="Seg_4341" n="e" s="T719">egelbite </ts>
               <ts e="T721" id="Seg_4343" n="e" s="T720">dʼürü, </ts>
               <ts e="T722" id="Seg_4345" n="e" s="T721">tabanɨ </ts>
               <ts e="T723" id="Seg_4347" n="e" s="T722">ahattɨm. </ts>
               <ts e="T724" id="Seg_4349" n="e" s="T723">Dʼe </ts>
               <ts e="T725" id="Seg_4351" n="e" s="T724">anɨ </ts>
               <ts e="T726" id="Seg_4353" n="e" s="T725">tabalanɨː </ts>
               <ts e="T727" id="Seg_4355" n="e" s="T726">biːrge </ts>
               <ts e="T728" id="Seg_4357" n="e" s="T727">oloru͡ok </ts>
               <ts e="T729" id="Seg_4359" n="e" s="T728">bihigi </ts>
               <ts e="T730" id="Seg_4361" n="e" s="T729">anɨ. </ts>
               <ts e="T731" id="Seg_4363" n="e" s="T730">Biːrge </ts>
               <ts e="T732" id="Seg_4365" n="e" s="T731">olorommut </ts>
               <ts e="T733" id="Seg_4367" n="e" s="T732">biːr </ts>
               <ts e="T734" id="Seg_4369" n="e" s="T733">hübennen </ts>
               <ts e="T735" id="Seg_4371" n="e" s="T734">bultanɨ͡ak </ts>
               <ts e="T736" id="Seg_4373" n="e" s="T735">i </ts>
               <ts e="T737" id="Seg_4375" n="e" s="T736">biːr </ts>
               <ts e="T738" id="Seg_4377" n="e" s="T737">dʼi͡eleːk </ts>
               <ts e="T739" id="Seg_4379" n="e" s="T738">bu͡olu͡ok. </ts>
               <ts e="T740" id="Seg_4381" n="e" s="T739">En </ts>
               <ts e="T741" id="Seg_4383" n="e" s="T740">dʼi͡egin </ts>
               <ts e="T742" id="Seg_4385" n="e" s="T741">köhördö </ts>
               <ts e="T743" id="Seg_4387" n="e" s="T742">egeli͡ek, </ts>
               <ts e="T744" id="Seg_4389" n="e" s="T743">bu </ts>
               <ts e="T745" id="Seg_4391" n="e" s="T744">tabalaːkpɨn </ts>
               <ts e="T746" id="Seg_4393" n="e" s="T745">diː </ts>
               <ts e="T747" id="Seg_4395" n="e" s="T746">bu, </ts>
               <ts e="T748" id="Seg_4397" n="e" s="T747">biːr </ts>
               <ts e="T749" id="Seg_4399" n="e" s="T748">tababɨt </ts>
               <ts e="T750" id="Seg_4401" n="e" s="T749">tartaran </ts>
               <ts e="T751" id="Seg_4403" n="e" s="T750">egeler </ts>
               <ts e="T752" id="Seg_4405" n="e" s="T751">tu͡oktaːk </ts>
               <ts e="T753" id="Seg_4407" n="e" s="T752">bu͡olu͡oj </ts>
               <ts e="T754" id="Seg_4409" n="e" s="T753">i͡e </ts>
               <ts e="T755" id="Seg_4411" n="e" s="T754">iti. </ts>
               <ts e="T756" id="Seg_4413" n="e" s="T755">Egeli͡ek </ts>
               <ts e="T757" id="Seg_4415" n="e" s="T756">manna </ts>
               <ts e="T758" id="Seg_4417" n="e" s="T757">tabagɨn, </ts>
               <ts e="T759" id="Seg_4419" n="e" s="T758">eː, </ts>
               <ts e="T760" id="Seg_4421" n="e" s="T759">kimŋin, </ts>
               <ts e="T761" id="Seg_4423" n="e" s="T760">manna </ts>
               <ts e="T762" id="Seg_4425" n="e" s="T761">(biːr) </ts>
               <ts e="T763" id="Seg_4427" n="e" s="T762">biːrge </ts>
               <ts e="T764" id="Seg_4429" n="e" s="T763">oloru͡ok </ts>
               <ts e="T765" id="Seg_4431" n="e" s="T764">bihigi." </ts>
               <ts e="T766" id="Seg_4433" n="e" s="T765">Hübelehenner </ts>
               <ts e="T767" id="Seg_4435" n="e" s="T766">dʼe </ts>
               <ts e="T768" id="Seg_4437" n="e" s="T767">biːrge </ts>
               <ts e="T769" id="Seg_4439" n="e" s="T768">oloron </ts>
               <ts e="T770" id="Seg_4441" n="e" s="T769">kaːlbɨttar. </ts>
               <ts e="T771" id="Seg_4443" n="e" s="T770">Ikki͡ennere </ts>
               <ts e="T772" id="Seg_4445" n="e" s="T771">biːr </ts>
               <ts e="T773" id="Seg_4447" n="e" s="T772">hübe </ts>
               <ts e="T774" id="Seg_4449" n="e" s="T773">bu͡olannar, </ts>
               <ts e="T775" id="Seg_4451" n="e" s="T774">nʼima </ts>
               <ts e="T776" id="Seg_4453" n="e" s="T775">bu͡olannar, </ts>
               <ts e="T777" id="Seg_4455" n="e" s="T776">ikki͡ennere </ts>
               <ts e="T778" id="Seg_4457" n="e" s="T777">dʼe </ts>
               <ts e="T779" id="Seg_4459" n="e" s="T778">bajan-toton </ts>
               <ts e="T780" id="Seg_4461" n="e" s="T779">olorbuttar, </ts>
               <ts e="T781" id="Seg_4463" n="e" s="T780">elete </ts>
               <ts e="T782" id="Seg_4465" n="e" s="T781">iti. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_4466" s="T0">MiPP_1996_OldManButterfly_flk.001 (001.001)</ta>
            <ta e="T11" id="Seg_4467" s="T6">MiPP_1996_OldManButterfly_flk.002 (001.002)</ta>
            <ta e="T19" id="Seg_4468" s="T11">MiPP_1996_OldManButterfly_flk.003 (001.003)</ta>
            <ta e="T24" id="Seg_4469" s="T19">MiPP_1996_OldManButterfly_flk.004 (001.004)</ta>
            <ta e="T42" id="Seg_4470" s="T24">MiPP_1996_OldManButterfly_flk.005 (001.005)</ta>
            <ta e="T52" id="Seg_4471" s="T42">MiPP_1996_OldManButterfly_flk.006 (001.006)</ta>
            <ta e="T56" id="Seg_4472" s="T52">MiPP_1996_OldManButterfly_flk.007 (001.007)</ta>
            <ta e="T58" id="Seg_4473" s="T56">MiPP_1996_OldManButterfly_flk.008 (001.008)</ta>
            <ta e="T66" id="Seg_4474" s="T58">MiPP_1996_OldManButterfly_flk.009 (001.009)</ta>
            <ta e="T75" id="Seg_4475" s="T66">MiPP_1996_OldManButterfly_flk.010 (001.010)</ta>
            <ta e="T79" id="Seg_4476" s="T75">MiPP_1996_OldManButterfly_flk.011 (001.011)</ta>
            <ta e="T83" id="Seg_4477" s="T79">MiPP_1996_OldManButterfly_flk.012 (001.012)</ta>
            <ta e="T86" id="Seg_4478" s="T83">MiPP_1996_OldManButterfly_flk.013 (001.013)</ta>
            <ta e="T91" id="Seg_4479" s="T86">MiPP_1996_OldManButterfly_flk.014 (001.014)</ta>
            <ta e="T98" id="Seg_4480" s="T91">MiPP_1996_OldManButterfly_flk.015 (001.015)</ta>
            <ta e="T102" id="Seg_4481" s="T98">MiPP_1996_OldManButterfly_flk.016 (001.016)</ta>
            <ta e="T107" id="Seg_4482" s="T102">MiPP_1996_OldManButterfly_flk.017 (001.017)</ta>
            <ta e="T114" id="Seg_4483" s="T107">MiPP_1996_OldManButterfly_flk.018 (001.018)</ta>
            <ta e="T116" id="Seg_4484" s="T114">MiPP_1996_OldManButterfly_flk.019 (001.019)</ta>
            <ta e="T128" id="Seg_4485" s="T116">MiPP_1996_OldManButterfly_flk.020 (001.020)</ta>
            <ta e="T135" id="Seg_4486" s="T128">MiPP_1996_OldManButterfly_flk.021 (001.021)</ta>
            <ta e="T140" id="Seg_4487" s="T135">MiPP_1996_OldManButterfly_flk.022 (001.022)</ta>
            <ta e="T146" id="Seg_4488" s="T140">MiPP_1996_OldManButterfly_flk.023 (001.023)</ta>
            <ta e="T158" id="Seg_4489" s="T146">MiPP_1996_OldManButterfly_flk.024 (001.024)</ta>
            <ta e="T161" id="Seg_4490" s="T158">MiPP_1996_OldManButterfly_flk.025 (001.025)</ta>
            <ta e="T169" id="Seg_4491" s="T161">MiPP_1996_OldManButterfly_flk.026 (001.026)</ta>
            <ta e="T178" id="Seg_4492" s="T169">MiPP_1996_OldManButterfly_flk.027 (001.027)</ta>
            <ta e="T182" id="Seg_4493" s="T178">MiPP_1996_OldManButterfly_flk.028 (001.028)</ta>
            <ta e="T193" id="Seg_4494" s="T182">MiPP_1996_OldManButterfly_flk.029 (001.029)</ta>
            <ta e="T196" id="Seg_4495" s="T193">MiPP_1996_OldManButterfly_flk.030 (001.030)</ta>
            <ta e="T201" id="Seg_4496" s="T196">MiPP_1996_OldManButterfly_flk.031 (001.031)</ta>
            <ta e="T204" id="Seg_4497" s="T201">MiPP_1996_OldManButterfly_flk.032 (001.032)</ta>
            <ta e="T216" id="Seg_4498" s="T204">MiPP_1996_OldManButterfly_flk.033 (001.033)</ta>
            <ta e="T219" id="Seg_4499" s="T216">MiPP_1996_OldManButterfly_flk.034 (001.034)</ta>
            <ta e="T224" id="Seg_4500" s="T219">MiPP_1996_OldManButterfly_flk.035 (001.035)</ta>
            <ta e="T228" id="Seg_4501" s="T224">MiPP_1996_OldManButterfly_flk.036 (001.036)</ta>
            <ta e="T237" id="Seg_4502" s="T228">MiPP_1996_OldManButterfly_flk.037 (001.037)</ta>
            <ta e="T242" id="Seg_4503" s="T237">MiPP_1996_OldManButterfly_flk.038 (001.038)</ta>
            <ta e="T252" id="Seg_4504" s="T242">MiPP_1996_OldManButterfly_flk.039 (001.039)</ta>
            <ta e="T254" id="Seg_4505" s="T252">MiPP_1996_OldManButterfly_flk.040 (001.040)</ta>
            <ta e="T265" id="Seg_4506" s="T254">MiPP_1996_OldManButterfly_flk.041 (001.041)</ta>
            <ta e="T271" id="Seg_4507" s="T265">MiPP_1996_OldManButterfly_flk.042 (001.042)</ta>
            <ta e="T279" id="Seg_4508" s="T271">MiPP_1996_OldManButterfly_flk.043 (001.043)</ta>
            <ta e="T283" id="Seg_4509" s="T279">MiPP_1996_OldManButterfly_flk.044 (001.044)</ta>
            <ta e="T300" id="Seg_4510" s="T283">MiPP_1996_OldManButterfly_flk.045 (001.045)</ta>
            <ta e="T306" id="Seg_4511" s="T300">MiPP_1996_OldManButterfly_flk.046 (001.046)</ta>
            <ta e="T320" id="Seg_4512" s="T306">MiPP_1996_OldManButterfly_flk.047 (001.047)</ta>
            <ta e="T323" id="Seg_4513" s="T320">MiPP_1996_OldManButterfly_flk.048 (001.048)</ta>
            <ta e="T327" id="Seg_4514" s="T323">MiPP_1996_OldManButterfly_flk.049 (001.049)</ta>
            <ta e="T331" id="Seg_4515" s="T327">MiPP_1996_OldManButterfly_flk.050 (001.050)</ta>
            <ta e="T336" id="Seg_4516" s="T331">MiPP_1996_OldManButterfly_flk.051 (001.051)</ta>
            <ta e="T349" id="Seg_4517" s="T336">MiPP_1996_OldManButterfly_flk.052 (001.052)</ta>
            <ta e="T360" id="Seg_4518" s="T349">MiPP_1996_OldManButterfly_flk.053 (001.053)</ta>
            <ta e="T374" id="Seg_4519" s="T360">MiPP_1996_OldManButterfly_flk.054 (001.054)</ta>
            <ta e="T385" id="Seg_4520" s="T374">MiPP_1996_OldManButterfly_flk.055 (001.055)</ta>
            <ta e="T389" id="Seg_4521" s="T385">MiPP_1996_OldManButterfly_flk.056 (001.056)</ta>
            <ta e="T404" id="Seg_4522" s="T389">MiPP_1996_OldManButterfly_flk.057 (001.057)</ta>
            <ta e="T416" id="Seg_4523" s="T404">MiPP_1996_OldManButterfly_flk.058 (001.058)</ta>
            <ta e="T431" id="Seg_4524" s="T416">MiPP_1996_OldManButterfly_flk.059 (001.059)</ta>
            <ta e="T445" id="Seg_4525" s="T431">MiPP_1996_OldManButterfly_flk.060 (001.060)</ta>
            <ta e="T447" id="Seg_4526" s="T445">MiPP_1996_OldManButterfly_flk.061 (001.061)</ta>
            <ta e="T460" id="Seg_4527" s="T447">MiPP_1996_OldManButterfly_flk.062 (001.062)</ta>
            <ta e="T464" id="Seg_4528" s="T460">MiPP_1996_OldManButterfly_flk.063 (001.063)</ta>
            <ta e="T475" id="Seg_4529" s="T464">MiPP_1996_OldManButterfly_flk.064 (001.064)</ta>
            <ta e="T480" id="Seg_4530" s="T475">MiPP_1996_OldManButterfly_flk.065 (001.065)</ta>
            <ta e="T481" id="Seg_4531" s="T480">MiPP_1996_OldManButterfly_flk.066 (001.066)</ta>
            <ta e="T486" id="Seg_4532" s="T481">MiPP_1996_OldManButterfly_flk.067 (001.067)</ta>
            <ta e="T491" id="Seg_4533" s="T486">MiPP_1996_OldManButterfly_flk.068 (001.068)</ta>
            <ta e="T495" id="Seg_4534" s="T491">MiPP_1996_OldManButterfly_flk.069 (001.069)</ta>
            <ta e="T501" id="Seg_4535" s="T495">MiPP_1996_OldManButterfly_flk.070 (001.070)</ta>
            <ta e="T504" id="Seg_4536" s="T501">MiPP_1996_OldManButterfly_flk.071 (001.071)</ta>
            <ta e="T515" id="Seg_4537" s="T504">MiPP_1996_OldManButterfly_flk.072 (001.072)</ta>
            <ta e="T521" id="Seg_4538" s="T515">MiPP_1996_OldManButterfly_flk.073 (001.073)</ta>
            <ta e="T524" id="Seg_4539" s="T521">MiPP_1996_OldManButterfly_flk.074 (001.074)</ta>
            <ta e="T527" id="Seg_4540" s="T524">MiPP_1996_OldManButterfly_flk.075 (001.075)</ta>
            <ta e="T533" id="Seg_4541" s="T527">MiPP_1996_OldManButterfly_flk.076 (001.076)</ta>
            <ta e="T542" id="Seg_4542" s="T533">MiPP_1996_OldManButterfly_flk.077 (001.077)</ta>
            <ta e="T546" id="Seg_4543" s="T542">MiPP_1996_OldManButterfly_flk.078 (001.078)</ta>
            <ta e="T560" id="Seg_4544" s="T546">MiPP_1996_OldManButterfly_flk.079 (001.079)</ta>
            <ta e="T571" id="Seg_4545" s="T560">MiPP_1996_OldManButterfly_flk.080 (001.080)</ta>
            <ta e="T574" id="Seg_4546" s="T571">MiPP_1996_OldManButterfly_flk.081 (001.081)</ta>
            <ta e="T596" id="Seg_4547" s="T574">MiPP_1996_OldManButterfly_flk.082 (001.082)</ta>
            <ta e="T603" id="Seg_4548" s="T596">MiPP_1996_OldManButterfly_flk.083 (001.083)</ta>
            <ta e="T615" id="Seg_4549" s="T603">MiPP_1996_OldManButterfly_flk.084 (001.084)</ta>
            <ta e="T620" id="Seg_4550" s="T615">MiPP_1996_OldManButterfly_flk.085 (001.085)</ta>
            <ta e="T624" id="Seg_4551" s="T620">MiPP_1996_OldManButterfly_flk.086 (001.086)</ta>
            <ta e="T639" id="Seg_4552" s="T624">MiPP_1996_OldManButterfly_flk.087 (001.087)</ta>
            <ta e="T647" id="Seg_4553" s="T639">MiPP_1996_OldManButterfly_flk.088 (001.088)</ta>
            <ta e="T652" id="Seg_4554" s="T647">MiPP_1996_OldManButterfly_flk.089 (001.089)</ta>
            <ta e="T666" id="Seg_4555" s="T652">MiPP_1996_OldManButterfly_flk.090 (001.090)</ta>
            <ta e="T680" id="Seg_4556" s="T666">MiPP_1996_OldManButterfly_flk.091 (001.091)</ta>
            <ta e="T688" id="Seg_4557" s="T680">MiPP_1996_OldManButterfly_flk.092 (001.092)</ta>
            <ta e="T692" id="Seg_4558" s="T688">MiPP_1996_OldManButterfly_flk.093 (001.093)</ta>
            <ta e="T699" id="Seg_4559" s="T692">MiPP_1996_OldManButterfly_flk.094 (001.094)</ta>
            <ta e="T707" id="Seg_4560" s="T699">MiPP_1996_OldManButterfly_flk.095 (001.095)</ta>
            <ta e="T711" id="Seg_4561" s="T707">MiPP_1996_OldManButterfly_flk.096 (001.096)</ta>
            <ta e="T716" id="Seg_4562" s="T711">MiPP_1996_OldManButterfly_flk.097 (001.097)</ta>
            <ta e="T723" id="Seg_4563" s="T716">MiPP_1996_OldManButterfly_flk.098 (001.098)</ta>
            <ta e="T730" id="Seg_4564" s="T723">MiPP_1996_OldManButterfly_flk.099 (001.099)</ta>
            <ta e="T739" id="Seg_4565" s="T730">MiPP_1996_OldManButterfly_flk.100 (001.100)</ta>
            <ta e="T755" id="Seg_4566" s="T739">MiPP_1996_OldManButterfly_flk.101 (001.101)</ta>
            <ta e="T765" id="Seg_4567" s="T755">MiPP_1996_OldManButterfly_flk.102 (001.102)</ta>
            <ta e="T770" id="Seg_4568" s="T765">MiPP_1996_OldManButterfly_flk.103 (001.103)</ta>
            <ta e="T782" id="Seg_4569" s="T770">MiPP_1996_OldManButterfly_flk.104 (001.104)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_4570" s="T0">Лөрүөнү гытта Таньактаак огонньор олорбуттар үһү.</ta>
            <ta e="T11" id="Seg_4571" s="T6">Ыраактыы олороллор, бэйэ бэйэлэрин эрэ билсиһэллэр.</ta>
            <ta e="T19" id="Seg_4572" s="T11">Бииргэһэ – ол диэк диэлэк, бииргэһэ – ба диэк диэлэк.</ta>
            <ta e="T24" id="Seg_4573" s="T19">Олороннор иккиэннэрэ көрсүбүттэр, бииргэ кэпсэспитэр (кэпсэппиттэр).</ta>
            <ta e="T42" id="Seg_4574" s="T24">"Ка-а, – (диир) дэһэллэр Лөрүөтүн гытта Таньактаак огонньор, – ка биһиги би (биир) дьылы быһыыр аспытын бииргэ колбооммут аһыак".</ta>
            <ta e="T52" id="Seg_4575" s="T42">"Баадакпытына (багардакпытына?) бадага би.. уһуннук олоруокпут, бадага, дьылбытын быһарбытыгар даагыны.</ta>
            <ta e="T56" id="Seg_4576" s="T52">Бу куһаган дьыллар бааллар". </ta>
            <ta e="T58" id="Seg_4577" s="T56">"Дьэ, һөп.</ta>
            <ta e="T66" id="Seg_4578" s="T58">Аны оччого би (биһиги), Лөрүө, эниэнэ ан.. аскын аһыак". </ta>
            <ta e="T75" id="Seg_4579" s="T66">Э, Таньактаак огонньор, гини ол – гиниэ диэтигэр бараммыт аһыак.</ta>
            <ta e="T79" id="Seg_4580" s="T75">"Дьэ һөп, инньэ аһыак".</ta>
            <ta e="T83" id="Seg_4581" s="T79">О-о, дьэ аһыыллар, арай. </ta>
            <ta e="T86" id="Seg_4582" s="T83">Лөрүө дьиэтигэр аһыыллар.</ta>
            <ta e="T91" id="Seg_4583" s="T86">Биир аӈаны, дьэ банныагар диэри.</ta>
            <ta e="T98" id="Seg_4584" s="T91">Дьэ, аһааннар, дьэ онтуларын астара баранна – Лөрүө гиэнэ.</ta>
            <ta e="T102" id="Seg_4585" s="T98">"Дьэ, Таньактаак огонньорго барыаһыӈ".</ta>
            <ta e="T107" id="Seg_4586" s="T102">Бараннар дьэ ол аһын аһыыһылар.</ta>
            <ta e="T114" id="Seg_4587" s="T107">О-о, Таньактаак огонньорго бардылар, дьэ аһыыллар.</ta>
            <ta e="T116" id="Seg_4588" s="T114">Аһыыллар-аһыыллар да.. </ta>
            <ta e="T128" id="Seg_4589" s="T116">"Кайа, – биир догоро, – миниэнэ аһым баранар буолтак, – диир, – эн дьиэгэр бар, </ta>
            <ta e="T135" id="Seg_4590" s="T128">бэйэӈ бултанан, мин аспын барытын һиэппин багарбаппын".</ta>
            <ta e="T140" id="Seg_4591" s="T135">Э, дьэ баран каалла догоро. </ta>
            <ta e="T146" id="Seg_4592" s="T140">Кайа баран туогу һиэгэй дьиэтигэр Лөрүө? </ta>
            <ta e="T158" id="Seg_4593" s="T146">Барбыта дьиэтигэр, туога да һуок, туок эрэ качча.. былчыӈкааны булунан, онтутун күөстэннэ.</ta>
            <ta e="T161" id="Seg_4594" s="T158">Күөстэнэн олорор арай.</ta>
            <ta e="T169" id="Seg_4595" s="T161">Ка, онтон көрбүтэ, тугу эрэ бултаммыт эмиэ Лөрүө. </ta>
            <ta e="T178" id="Seg_4596" s="T169">Онтон маадьын, Таньактаак огонньор аһа бараммыт этэ һопсиэм.</ta>
            <ta e="T182" id="Seg_4597" s="T178">Дьэ олорор, билсибэт гиниэкэ.</ta>
            <ta e="T193" id="Seg_4598" s="T182">"Дьэ кэлбэтин гини, бэйэм да бултанан олороонубун биир туоккаан эмэ", – диир.</ta>
            <ta e="T196" id="Seg_4599" s="T193">Лөрүө арай олорор.</ta>
            <ta e="T201" id="Seg_4600" s="T196">Бу олордогуна Таньактаак огонньор кэлбит.</ta>
            <ta e="T204" id="Seg_4601" s="T201">Иһэрин көрдө догорун.</ta>
            <ta e="T216" id="Seg_4602" s="T204">"Кыак, Таньактаак огонньор иһэр эбит", – Лөрүө дьиэтин катанан, һабан кээспит.</ta>
            <ta e="T219" id="Seg_4603" s="T216">Дьэ кэлэр арай. </ta>
            <ta e="T224" id="Seg_4604" s="T219">Ка-а, Лөрүөтүгэр кэлэр.</ta>
            <ta e="T228" id="Seg_4605" s="T224">"Лөрүө догордоо, Лөрүө догордоо, </ta>
            <ta e="T237" id="Seg_4606" s="T228">эн биэрэ эм.. былдьыӈкаанна быраган абыраа, Лөрүө догордо..</ta>
            <ta e="T242" id="Seg_4607" s="T237">ардаан,.. күммэр турда дьылбын солгоо</ta>
            <ta e="T252" id="Seg_4608" s="T242">Лөрүө догордоо, Лөрүө догордоо, өллүм каалыам", – биири, ыллыы олорбут төрүт. </ta>
            <ta e="T254" id="Seg_4609" s="T252">"Ка-а, бар-бар!" </ta>
            <ta e="T265" id="Seg_4610" s="T254">Биир туок эрэ бүттэскэ һыстыбыт, каммыт… каппыт былчыӈкааны быраган кээстэ.</ta>
            <ta e="T271" id="Seg_4611" s="T265">"Бар", – диир. Онту.. Таньактаак баран каалла. </ta>
            <ta e="T279" id="Seg_4612" s="T271">"Бар (дьиэтигэр) дьиэгэр, – диир, – больсэ (больше) биэрбэппин эниэкэ. </ta>
            <ta e="T283" id="Seg_4613" s="T279">Ко, баран каалла киһи.</ta>
            <ta e="T300" id="Seg_4614" s="T283">Баран тоһолаак (төһөлөөк) щиэгэй (һиэгэй) гини онтон, кааный постуой минин агай күөстэнэн щиэбит да.</ta>
            <ta e="T306" id="Seg_4615" s="T300">Туок да булбат, каньаабат дааганы. </ta>
            <ta e="T320" id="Seg_4616" s="T306">Кас да күнү һыппыта да, арыччы кааман таксар буолбут уһугуттан октон, Таньактаак огонньор. </ta>
            <ta e="T323" id="Seg_4617" s="T320">Эпээт Лөрүөтүгэр барыыһы.</ta>
            <ta e="T327" id="Seg_4618" s="T323">Кэлэн Лөрүөтүгэр эмиэ ыллаабыт: </ta>
            <ta e="T331" id="Seg_4619" s="T327">"Лөрүө догордоон, Лөрүө догордоон, </ta>
            <ta e="T336" id="Seg_4620" s="T331">өлөн каалыам, олөн каалыам, </ta>
            <ta e="T349" id="Seg_4621" s="T336">Лөрүө догордоон, биир эмэ былчыӈкаанна быраган абыраа, Лөрүө догордоо, Лөрүө догордо</ta>
            <ta e="T360" id="Seg_4622" s="T349">ордон, умнан ба.. буолла-буллуо, Лөрүө догордоон, былчыӈна эгэл, ылланабын".</ta>
            <ta e="T374" id="Seg_4623" s="T360">"Э, биир да былчыӈа һуокпун, бар, бар", – диэбит, төттөрү батан ыыппыт, туоккааны да биэрбэтэк. </ta>
            <ta e="T385" id="Seg_4624" s="T374">Кайдиэк барыай, таһаара отто тоӈор буолла, кайдиэт, дьиэгэ да кииллэрбэт саат.</ta>
            <ta e="T389" id="Seg_4625" s="T385">Дьэ барар бу киһи. </ta>
            <ta e="T404" id="Seg_4626" s="T389">Барбыта араччы (арыччы) онтон тийэн истэгинэ, дьиэтин аттытыгар тийэн истэгинэ, көрбүтэ, тугуттаак тыһы һылдьар дьиэтигэр.</ta>
            <ta e="T416" id="Seg_4627" s="T404">Кайа кэлииттэн кэлбитэ дьүрү билбэт табаны гини … таба һылдьар арай, кэлбит. </ta>
            <ta e="T431" id="Seg_4628" s="T416">Өрө гини өлүөк ыйаагыгар, өт.. тиллиэк ыйаагыгар эрэ тугуттаак тыһыта бэйэтигэр кэлэн һалыы һылдьыбыттар гинини.</ta>
            <ta e="T445" id="Seg_4629" s="T431">Ииктээбитигэр иһигэр кэлбитэ да тугуккаанын кабан ылан, баһаккааны ылла да "корт" гыннарар өлөрөн кээспит.</ta>
            <ta e="T447" id="Seg_4630" s="T445">Иньэтэ һылдьар. </ta>
            <ta e="T460" id="Seg_4631" s="T447">Ко, мантытын туок, һүлү.. һүлүммүт киһи һиикэйдии буһаран, күөстэнэн һиэн кээспит. </ta>
            <ta e="T464" id="Seg_4632" s="T460">Дьэ карага һырдаабыт онно.</ta>
            <ta e="T475" id="Seg_4633" s="T464">"О, аны багас абаранным, Лөрүөгэ барбаппын, каньаабаппын, астанным буо", – диир. </ta>
            <ta e="T480" id="Seg_4634" s="T475">Астаммыт киһи күөстэнэн бараан аһыыр. </ta>
            <ta e="T481" id="Seg_4635" s="T480">Ок.</ta>
            <ta e="T486" id="Seg_4636" s="T481">Дьэ кас күнү олорбута дьүрү.</ta>
            <ta e="T491" id="Seg_4637" s="T486">Табалаак, табатын иттэ туттубут, тыһытын. </ta>
            <ta e="T495" id="Seg_4638" s="T491">Тыһылаак, туок буолуой аны. </ta>
            <ta e="T501" id="Seg_4639" s="T495">Ко, бу һыттагына Лөрүөтүн һаӈата кэллэ.</ta>
            <ta e="T504" id="Seg_4640" s="T501">Дьэ арай көрбүтэ. </ta>
            <ta e="T515" id="Seg_4641" s="T504">"Дьэ мин аны итигирдик гыныам, бээбэ кэллин агай, – диэбит, – миниэкэ ас".</ta>
            <ta e="T521" id="Seg_4642" s="T515">Лөрүө туоктаан арыччы иһэр буопса коргуйан. </ta>
            <ta e="T524" id="Seg_4643" s="T521">Арыччы кааман иһэр. </ta>
            <ta e="T527" id="Seg_4644" s="T524">Кэлбит да кэлбит. </ta>
            <ta e="T533" id="Seg_4645" s="T527">Кэлэрин гытта катаан, катанан кээспит киһи. </ta>
            <ta e="T542" id="Seg_4646" s="T533">"Дьэ гинини накаастыам, маадьын минигин эмиэ гини муӈнаабыта бурдук (бу көрдүк). </ta>
            <ta e="T546" id="Seg_4647" s="T542">"Таньактак огонньор, Таньактаак огонньор,</ta>
            <ta e="T560" id="Seg_4648" s="T546">биирийэӈ эмэтэ былчыӈкаанна биэрэн абыраа, Таньактаак огонньор, өлөн кааллым, өлөн кааллым кааллым</ta>
            <ta e="T571" id="Seg_4649" s="T560">күммэр тур да куӈ киһитэ буоллаккына, Таньактаак огонньор, Таньактаак огонньор ", – ыллана һыппыт. </ta>
            <ta e="T574" id="Seg_4650" s="T571">Тугу да биэрбэтэк.</ta>
            <ta e="T596" id="Seg_4651" s="T574">Постуой туок эрэ бу тугутун гиэнин туньагар, туньактарын туоктаммыт, ататын гытта туньага дуу, туок дуу – ону һэт… атанаак һылдьан ону быраган. </ta>
            <ta e="T603" id="Seg_4652" s="T596">"Бар, щиэ (һиэ)", – дии, икки туньагы быраган кээспит. </ta>
            <ta e="T615" id="Seg_4653" s="T603">"Бар дьиэгэр, больсе кэлимэ. Бу маайдын минигин эмиэ ильчи муӈнаабытыӈ". </ta>
            <ta e="T620" id="Seg_4654" s="T615">Онтута быстыа дуо, баран каалбыт. </ta>
            <ta e="T624" id="Seg_4655" s="T620">Дьэ баран каалбыт дьиэтигэр. </ta>
            <ta e="T639" id="Seg_4656" s="T624">Эмиэ… барбыт да һарсыӈӈытын как кас да өрө (өрүү) буолан бараан эмиэ кэлэр.</ta>
            <ta e="T647" id="Seg_4657" s="T639">Дьэ вовсе өлөрүгэр барбыт, быһылаак догоро, Лөрүөтэ ыксаабыт: </ta>
            <ta e="T652" id="Seg_4658" s="T647">баттагына да барбат, киирдэгинэ кэлбэт.</ta>
            <ta e="T666" id="Seg_4659" s="T652">О, дьэ кайдак да гыныагын бэрт . Дьэ һүрэгэ ыарыы…ыалдьар киһи, догорун аһынна дии.</ta>
            <ta e="T680" id="Seg_4660" s="T666">Аһынан кайдак да гыныагын бэрт, батыагын даа .. баран да һин дьиэтигэр тийиэ һуок.</ta>
            <ta e="T688" id="Seg_4661" s="T680">Айыылаагын – "Киир, – диэбит, – аһаа, – кэпсэтиэк оччо (оччого) үчүгэйдик".</ta>
            <ta e="T692" id="Seg_4662" s="T688">Дьэ илби илэ кэпсэппиттэр.</ta>
            <ta e="T699" id="Seg_4663" s="T692">"Кайа, отто биһиги үчүгэйдик кэпсэтиэк биһиги аны. </ta>
            <ta e="T707" id="Seg_4664" s="T699">Бииргэ олоруокпут дьэ, того икки диэк олоробут биһиги.</ta>
            <ta e="T711" id="Seg_4665" s="T707">Көрөгүн, мин табаланным өссүө. </ta>
            <ta e="T716" id="Seg_4666" s="T711">Эн маайдьы минигин атагастыы һысп.. (һыспытыӈ).</ta>
            <ta e="T723" id="Seg_4667" s="T716">Туок табата таӈара эгэлбитэ дьүрү, табаны аһаттым. </ta>
            <ta e="T730" id="Seg_4668" s="T723">Дьэ аны табаланныа.. бииргэ олоруок биһиги аны. </ta>
            <ta e="T739" id="Seg_4669" s="T730">Бииргэ олороммут биир һүбэннэн бултаныак и биир буолуок.</ta>
            <ta e="T755" id="Seg_4670" s="T739">Эн дьиэгин көһөрдө эгэлиэк, бу табалаакпыт дии бу – биир табабыт тартаран эгэлэр туоктаак буолуой иэ ити.</ta>
            <ta e="T765" id="Seg_4671" s="T755">Эгэлик манна табагын, э, кимӈин, манна биир, бииргэ олоруок биһиги". </ta>
            <ta e="T770" id="Seg_4672" s="T765">Һүбэлэһэннэр дьэ бииргэ олорон каалбыттар. </ta>
            <ta e="T782" id="Seg_4673" s="T770">Иккиэннэрэ биир һүбэ буоланнар, ньима буоланнар – иккиэннэрэ дьэ байан-тотон олорбуттар. Элэтэ ити.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_4674" s="T0">Lörü͡önü gɨtta Tanʼaktaːk ogonnʼor olorbuttar ühü. </ta>
            <ta e="T11" id="Seg_4675" s="T6">ɨraːktɨː olorollor, beje bejelerin bilsiheller. </ta>
            <ta e="T19" id="Seg_4676" s="T11">Biːrgehe – ol di͡ek dʼi͡eleːk, biːrgehe – ba di͡ek dʼi͡eleːk. </ta>
            <ta e="T24" id="Seg_4677" s="T19">Oloronnor ikki͡ennere körsübütter, biːrge kepsespitter. </ta>
            <ta e="T42" id="Seg_4678" s="T24">"Kaː", (diːr) deheller Lörü͡ötün gɨtta Tanʼaktaːk ogonnʼor, "ka bihigi (bi-) biːr dʼɨlɨ bɨhar aspɨtɨn biːrge kolboːmmut ahɨ͡ak." </ta>
            <ta e="T52" id="Seg_4679" s="T42">"Baːdakpɨtɨna badaga (bi-) uhunnuk oloru͡okput, badaga, dʼɨlbɨtɨn bɨharbɨtɨgar daːgɨnɨ. </ta>
            <ta e="T56" id="Seg_4680" s="T52">Bu kuhagan dʼɨllar baːllar." </ta>
            <ta e="T58" id="Seg_4681" s="T56">"Dʼe, höp. </ta>
            <ta e="T66" id="Seg_4682" s="T58">Anɨ oččogo bihigi, Lörü͡ö, eni͡ene ((…)) askɨn ahɨ͡ak." </ta>
            <ta e="T75" id="Seg_4683" s="T66">"Eː, Tanʼaktaːk ogonnʼor, gini ol, gini͡ene dʼi͡etiger barammɨt ahɨ͡ak." </ta>
            <ta e="T79" id="Seg_4684" s="T75">"Dʼe höp, innʼe ahɨ͡ak." </ta>
            <ta e="T83" id="Seg_4685" s="T79">Oː, dʼe ahɨːllar araj. </ta>
            <ta e="T86" id="Seg_4686" s="T83">Lörü͡ö dʼi͡etiger ahɨːllar. </ta>
            <ta e="T91" id="Seg_4687" s="T86">Biːr aŋarɨ, dʼe baranɨ͡agar di͡eri. </ta>
            <ta e="T98" id="Seg_4688" s="T91">Dʼe, ahaːnnar, ontularɨn astara baranna, Lörü͡ö gi͡ene. </ta>
            <ta e="T102" id="Seg_4689" s="T98">"Dʼe, Tanʼaktaːk ogonnʼorgo barɨ͡agɨŋ." </ta>
            <ta e="T107" id="Seg_4690" s="T102">Barannar dʼe ol ahɨn ahɨːhɨlar. </ta>
            <ta e="T114" id="Seg_4691" s="T107">Oː, Tanʼaktaːk ogonnʼorgo bardɨlar ((PAUSE)) dʼe ahɨːllar. </ta>
            <ta e="T116" id="Seg_4692" s="T114">Ahɨːllar-ahɨːllar da. </ta>
            <ta e="T128" id="Seg_4693" s="T116">"Kajaː", biːr dogoro, "mini͡ene ahɨm baranar bu͡oltak", ((…)) diːr, "en dʼi͡eger bar. </ta>
            <ta e="T135" id="Seg_4694" s="T128">Bejeŋ bultanan, min aspɨn barɨtɨn hi͡eppin bagarbappɨn." </ta>
            <ta e="T140" id="Seg_4695" s="T135">Eː, dʼe baran kaːlla dogoro. </ta>
            <ta e="T146" id="Seg_4696" s="T140">Kajaː, baran tu͡ogu hi͡egej dʼi͡etiger Löːrü͡ö? </ta>
            <ta e="T158" id="Seg_4697" s="T146">Barbɨta dʼi͡etiger, tu͡oga da hu͡ok, tu͡ok ere kačča bɨlčɨŋkaːnɨ bulunan, ontutun kü͡östenne. </ta>
            <ta e="T161" id="Seg_4698" s="T158">Kü͡östenen oloror araj. </ta>
            <ta e="T169" id="Seg_4699" s="T161">Kaː, onton körbüte, tugu ere bultammɨt emi͡e Löːrü͡ö. </ta>
            <ta e="T178" id="Seg_4700" s="T169">Onton maːjdʼɨn, Tanʼaktaːk ogonnʼor da aha barammɨt ete hopsi͡em. </ta>
            <ta e="T182" id="Seg_4701" s="T178">Dʼe oloror, bilsibet gini͡eke. </ta>
            <ta e="T193" id="Seg_4702" s="T182">"Dʼe kelbetin gini, bejem da bultanan oloroːn biːr tu͡okkaːn eme", diːr. </ta>
            <ta e="T196" id="Seg_4703" s="T193">Lörü͡ö araj oloror. </ta>
            <ta e="T201" id="Seg_4704" s="T196">Bu olordoguna Tanʼaktaːk ogonnʼor kelbit. </ta>
            <ta e="T204" id="Seg_4705" s="T201">Iherin kördö dogorun. </ta>
            <ta e="T216" id="Seg_4706" s="T204">"Bɨː, Tanʼaktaːk ogonnʼor iher ebit", Lörü͡ö diːr, ((…)) dʼi͡etin katanan, haban keːspit. </ta>
            <ta e="T219" id="Seg_4707" s="T216">Dʼe keler araj. </ta>
            <ta e="T224" id="Seg_4708" s="T219">Aː, (Lö-) Lörü͡ötüger keler. </ta>
            <ta e="T228" id="Seg_4709" s="T224">"Lörü͡ö dogordoː, Lörü͡ö dogordoː. </ta>
            <ta e="T237" id="Seg_4710" s="T228">En bi͡er eme ((…)) bɨldʼɨŋkaːnna bɨragan abɨraː, Löːrü͡ö dogordoː. </ta>
            <ta e="T242" id="Seg_4711" s="T237">Ardaːn, kümmer turda dʼɨlbɨn solgoː. </ta>
            <ta e="T252" id="Seg_4712" s="T242">Löːrü͡ö dogordoː, Löːrü͡ö dogordoː, öllüm kaːlɨ͡am", biːri, ɨllɨː olorbut törüt. </ta>
            <ta e="T254" id="Seg_4713" s="T252">"Kaː, bar-bar!" </ta>
            <ta e="T265" id="Seg_4714" s="T254">Biːr da biːr (tugu) bütteske hɨstɨbɨt, (kammɨt) kappɨt bɨlčɨŋkaːnɨ bɨragan keːste. </ta>
            <ta e="T271" id="Seg_4715" s="T265">"Bar", diːr ontuta, Tanʼaktaːk baran kaːlla. </ta>
            <ta e="T279" id="Seg_4716" s="T271">"(Bar dʼi͡etiger), bar dʼi͡eger", diːr, "bolse bi͡erbeppin eni͡eke." </ta>
            <ta e="T283" id="Seg_4717" s="T279">Koː, baran kaːlla kihi. </ta>
            <ta e="T300" id="Seg_4718" s="T283">Baran toholaːk či͡egej gini onton, ol-bu dʼe kaːnɨn (postu͡oj) postu͡oj minin agaj (kü-) kü͡östenen ši͡ebit da. </ta>
            <ta e="T306" id="Seg_4719" s="T300">Tu͡ok da bulbat, da kanʼaːbat daːganɨ. </ta>
            <ta e="T320" id="Seg_4720" s="T306">Kas da künü hɨppɨta da, araj da kaːman taksar bu͡olbut uhuguttan okton, Tanʼaktaːk ogonnʼor. </ta>
            <ta e="T323" id="Seg_4721" s="T320">Epeːt Löːrü͡ötüger barɨːhɨ. </ta>
            <ta e="T327" id="Seg_4722" s="T323">Kelen Löːrü͡ötüger emi͡e ɨllaːbɨt: </ta>
            <ta e="T331" id="Seg_4723" s="T327">"Löːrü͡ö dogordoː, Löːrü͡ö dogordoː. </ta>
            <ta e="T336" id="Seg_4724" s="T331">((…)) öllüm kaːlɨ͡am, öllüm kaːlɨ͡am. </ta>
            <ta e="T349" id="Seg_4725" s="T336">Löːrü͡ö dogordoː, biːr emeː ((BREATH)) eme bɨlčɨŋkaːnna bɨragan abɨraː, Löːrü͡ö dogordoː, Löːrü͡ö dogordo. </ta>
            <ta e="T360" id="Seg_4726" s="T349">(Ardaːn), umnan ((…)) bu͡olla (bulu͡o), Löːrü͡ö dogordoː, (ardaːn) bɨlčɨŋna egel, ɨllanabɨn." </ta>
            <ta e="T374" id="Seg_4727" s="T360">"E, biːr da bɨlčɨŋa hu͡okpun, bar, bar", di͡ebit, töttörü batan ɨːppɨt, tu͡okkaːnɨ da bi͡erbetek. </ta>
            <ta e="T385" id="Seg_4728" s="T374">Kajdi͡ek barɨ͡aj, tahaːra otto toŋor bu͡olla, kajdi͡et, dʼi͡ege da kiːllerbet saːt. </ta>
            <ta e="T389" id="Seg_4729" s="T385">Dʼe barar bu kihi. </ta>
            <ta e="T404" id="Seg_4730" s="T389">Barbɨta araččɨ onton tijen istegine, (dʼi͡en-) dʼi͡etin attɨgar tijer, körbüte, tuguttaːk tɨhɨ hɨldʼar dʼi͡etiger. </ta>
            <ta e="T416" id="Seg_4731" s="T404">Kaja keliːtten kelbite dʼürü bilbet tabanɨ gini ((…)) taba hɨldʼar araj, kelbit. </ta>
            <ta e="T431" id="Seg_4732" s="T416">Örö gini ölü͡ök ɨjaːgɨgar, (öt-) tilli͡ek ɨjaːgɨgar tuguttaːk tɨhɨta bejetiger kelen halɨː hɨldʼɨbɨttar ginini. </ta>
            <ta e="T445" id="Seg_4733" s="T431">Iːkteːbitiger ihiger, kelbite da tugukkaːnɨn kaban ɨlan, bahakkaːnɨ ɨlla da "kort" gɨnnaran ölörön keːspit. </ta>
            <ta e="T447" id="Seg_4734" s="T445">Inʼete hɨldʼar. </ta>
            <ta e="T460" id="Seg_4735" s="T447">Ko, mantɨtɨn (tu͡ok) tu͡ok, (hülü-) hülümmüt kihi hiːkejdiː buharan, kü͡östenen hi͡en keːspit. </ta>
            <ta e="T464" id="Seg_4736" s="T460">Dʼe karaga hɨrdaːbɨt onno. </ta>
            <ta e="T475" id="Seg_4737" s="T464">"Oː, dʼe anɨ bagas abɨrannɨm, Löːrü͡öge barbappɨn, kanʼaːbappɨn, astannɨm bu͡o", diːr. </ta>
            <ta e="T480" id="Seg_4738" s="T475">Astammɨt kihi kü͡östenen baraːn ahɨːr. </ta>
            <ta e="T481" id="Seg_4739" s="T480">Oːk. </ta>
            <ta e="T486" id="Seg_4740" s="T481">Dʼeː kas künü olorbuta dʼürü. </ta>
            <ta e="T491" id="Seg_4741" s="T486">Tabalaːk, tabatɨn itte tuttubut, tɨhɨtɨn. </ta>
            <ta e="T495" id="Seg_4742" s="T491">Tɨhɨlaːk, tu͡ok bu͡olu͡oj anɨ. </ta>
            <ta e="T501" id="Seg_4743" s="T495">Oː, bu hɨttagɨna Löːrü͡ötün haŋata kelle. </ta>
            <ta e="T504" id="Seg_4744" s="T501">Dʼe araj körbüte. </ta>
            <ta e="T515" id="Seg_4745" s="T504">"Dʼe min anɨ itigirdik gɨnɨ͡am, beːbe kellin agaj", di͡ebit, "mini͡eke as". </ta>
            <ta e="T521" id="Seg_4746" s="T515">Löːrü͡ö tu͡oktaːn arɨččɨ iher bu͡opsa korgujan. </ta>
            <ta e="T524" id="Seg_4747" s="T521">Araččɨ kaːman iher. </ta>
            <ta e="T527" id="Seg_4748" s="T524">Kelbit da kelbit. </ta>
            <ta e="T533" id="Seg_4749" s="T527">Kelerin gɨtta dʼi͡ete kataːn, katanan keːspit. </ta>
            <ta e="T542" id="Seg_4750" s="T533">"Dʼe ginini nakaːstɨ͡am, maːjdʼɨn minigin emi͡e gini muŋnaːbɨta burduk." </ta>
            <ta e="T546" id="Seg_4751" s="T542">"Tanʼaktaːk ogonnʼor, Tanʼaktaːk ogonnʼor. </ta>
            <ta e="T560" id="Seg_4752" s="T546">Biːr ((…)) emete bɨlčɨŋkaːnna bi͡eren abɨraː, Tanʼaktaːk ogonnʼor, öllüm kaːllɨm, öllüm (kaː-) kaːllɨm. </ta>
            <ta e="T571" id="Seg_4753" s="T560">Kümmer turda kuŋ kihite bu͡ollakkɨna, Tanʼaktaːk ogonnʼor, Tanʼaktaːk ogonnʼor", ɨllana hɨppɨt. </ta>
            <ta e="T574" id="Seg_4754" s="T571">Tugu da bi͡erbetek. </ta>
            <ta e="T596" id="Seg_4755" s="T574">Postu͡oj tu͡ok ere bu tugutun gi͡enin (tunʼaga) tunʼaktarɨn tu͡oktammɨt, atatɨn gɨtta tunʼaga duː, tu͡ok duː, onu (het-) atanaːk hɨldʼan onu bɨragan. </ta>
            <ta e="T603" id="Seg_4756" s="T596">"Bar, ši͡e", diː ikki tunʼagɨ bɨragan keːspit. </ta>
            <ta e="T615" id="Seg_4757" s="T603">"Bar dʼi͡eger", diːr, "bolʼše kelime, (mi-) maːjdɨn minigin emi͡e dʼe muŋnaːbɨtɨŋ." </ta>
            <ta e="T620" id="Seg_4758" s="T615">Ontuta bɨstɨ͡a du͡o, baran kaːlbɨt. </ta>
            <ta e="T624" id="Seg_4759" s="T620">Dʼe baran kaːlbɨt dʼi͡etiger. </ta>
            <ta e="T639" id="Seg_4760" s="T624">Emi͡e ((PAUSE)) barbɨt da hartɨn (ka-) kas da örö (günü) bu͡olan baraːn emi͡e keler. </ta>
            <ta e="T647" id="Seg_4761" s="T639">Dʼe vavsʼe ölörüger barbɨt, bɨhɨlaːk dogoro, Löːrü͡öte ɨksaːbɨt: </ta>
            <ta e="T652" id="Seg_4762" s="T647">Battagɨna da barbat, kiːrdegine kelbet. </ta>
            <ta e="T666" id="Seg_4763" s="T652">Oː, dʼe kajdak da gɨnɨ͡agɨn bert, dʼe hürege ɨ͡arɨː, ɨ͡aldʼar kihi, dogorun ahɨnna diː. </ta>
            <ta e="T680" id="Seg_4764" s="T666">Ahɨnan kajdak da gɨnɨ͡agɨn bert, battɨ͡agɨn daː ((…)) baran da hin dʼi͡etiger tiji͡e hu͡ok. </ta>
            <ta e="T688" id="Seg_4765" s="T680">((…)) ajɨːlaːgɨn, "kiːr", di͡ebit, "ahaː, kepseti͡ek oččogo üčügejdik." </ta>
            <ta e="T692" id="Seg_4766" s="T688">Dʼe ilbi ile kepseppitter. </ta>
            <ta e="T699" id="Seg_4767" s="T692">"Kaja, otto bihigi üčügejdik kepseti͡ek biːrge anɨ. </ta>
            <ta e="T707" id="Seg_4768" s="T699">Biːrge oloru͡okput dʼe, togo ikki di͡ek olorobut bihigi. </ta>
            <ta e="T711" id="Seg_4769" s="T707">Körögün, min tabalannɨm össü͡ö. </ta>
            <ta e="T716" id="Seg_4770" s="T711">En maːjdʼɨ minigin atagastɨː ((…)). </ta>
            <ta e="T723" id="Seg_4771" s="T716">Tu͡ok tabata taŋara egelbite dʼürü, tabanɨ ahattɨm. </ta>
            <ta e="T730" id="Seg_4772" s="T723">Dʼe anɨ tabalanɨː biːrge oloru͡ok bihigi anɨ. </ta>
            <ta e="T739" id="Seg_4773" s="T730">Biːrge olorommut biːr hübennen bultanɨ͡ak i biːr dʼi͡eleːk bu͡olu͡ok. </ta>
            <ta e="T755" id="Seg_4774" s="T739">En dʼi͡egin köhördö egeli͡ek, bu tabalaːkpɨn diː bu, biːr tababɨt tartaran egeler tu͡oktaːk bu͡olu͡oj i͡e iti. </ta>
            <ta e="T765" id="Seg_4775" s="T755">Egeli͡ek manna tabagɨn, eː, kimŋin, manna (biːr) biːrge oloru͡ok bihigi." </ta>
            <ta e="T770" id="Seg_4776" s="T765">Hübelehenner dʼe biːrge oloron kaːlbɨttar. </ta>
            <ta e="T782" id="Seg_4777" s="T770">Ikki͡ennere biːr hübe bu͡olannar, nʼima bu͡olannar, ikki͡ennere dʼe bajan-toton olorbuttar, elete iti. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_4778" s="T0">Lörü͡ö-nü</ta>
            <ta e="T2" id="Seg_4779" s="T1">gɨtta</ta>
            <ta e="T3" id="Seg_4780" s="T2">Tanʼaktaːk</ta>
            <ta e="T4" id="Seg_4781" s="T3">ogonnʼor</ta>
            <ta e="T5" id="Seg_4782" s="T4">olor-but-tar</ta>
            <ta e="T6" id="Seg_4783" s="T5">ühü</ta>
            <ta e="T7" id="Seg_4784" s="T6">ɨraːk-tɨː</ta>
            <ta e="T8" id="Seg_4785" s="T7">olor-ol-lor</ta>
            <ta e="T9" id="Seg_4786" s="T8">beje</ta>
            <ta e="T10" id="Seg_4787" s="T9">beje-leri-n</ta>
            <ta e="T11" id="Seg_4788" s="T10">bil-s-i-h-el-ler</ta>
            <ta e="T12" id="Seg_4789" s="T11">biːrgeh-e</ta>
            <ta e="T13" id="Seg_4790" s="T12">ol</ta>
            <ta e="T14" id="Seg_4791" s="T13">di͡ek</ta>
            <ta e="T15" id="Seg_4792" s="T14">dʼi͡e-leːk</ta>
            <ta e="T16" id="Seg_4793" s="T15">biːrgeh-e</ta>
            <ta e="T17" id="Seg_4794" s="T16">ba</ta>
            <ta e="T18" id="Seg_4795" s="T17">di͡ek</ta>
            <ta e="T19" id="Seg_4796" s="T18">dʼi͡e-leːk</ta>
            <ta e="T20" id="Seg_4797" s="T19">olor-on-nor</ta>
            <ta e="T21" id="Seg_4798" s="T20">ikk-i͡en-nere</ta>
            <ta e="T22" id="Seg_4799" s="T21">körs-ü-büt-ter</ta>
            <ta e="T23" id="Seg_4800" s="T22">biːrge</ta>
            <ta e="T24" id="Seg_4801" s="T23">keps-e-s-pit-ter</ta>
            <ta e="T25" id="Seg_4802" s="T24">kaː</ta>
            <ta e="T26" id="Seg_4803" s="T25">diː-r</ta>
            <ta e="T27" id="Seg_4804" s="T26">d-e-h-el-ler</ta>
            <ta e="T28" id="Seg_4805" s="T27">Lörü͡ö-tü-n</ta>
            <ta e="T29" id="Seg_4806" s="T28">gɨtta</ta>
            <ta e="T30" id="Seg_4807" s="T29">Tanʼaktaːk</ta>
            <ta e="T31" id="Seg_4808" s="T30">ogonnʼor</ta>
            <ta e="T32" id="Seg_4809" s="T31">ka</ta>
            <ta e="T33" id="Seg_4810" s="T32">bihigi</ta>
            <ta e="T36" id="Seg_4811" s="T35">biːr</ta>
            <ta e="T37" id="Seg_4812" s="T36">dʼɨl-ɨ</ta>
            <ta e="T38" id="Seg_4813" s="T37">bɨh-ar</ta>
            <ta e="T39" id="Seg_4814" s="T38">as-pɨtɨ-n</ta>
            <ta e="T40" id="Seg_4815" s="T39">biːrge</ta>
            <ta e="T41" id="Seg_4816" s="T40">kolboː-m-mut</ta>
            <ta e="T42" id="Seg_4817" s="T41">ah-ɨ͡ak</ta>
            <ta e="T43" id="Seg_4818" s="T42">baː-dak-pɨtɨna</ta>
            <ta e="T44" id="Seg_4819" s="T43">badaga</ta>
            <ta e="T47" id="Seg_4820" s="T46">uhun-nuk</ta>
            <ta e="T48" id="Seg_4821" s="T47">olor-u͡ok-put</ta>
            <ta e="T49" id="Seg_4822" s="T48">badaga</ta>
            <ta e="T50" id="Seg_4823" s="T49">dʼɨl-bɨtɨ-n</ta>
            <ta e="T51" id="Seg_4824" s="T50">bɨh-ar-bɨtɨ-gar</ta>
            <ta e="T52" id="Seg_4825" s="T51">daːgɨnɨ</ta>
            <ta e="T53" id="Seg_4826" s="T52">bu</ta>
            <ta e="T54" id="Seg_4827" s="T53">kuhagan</ta>
            <ta e="T55" id="Seg_4828" s="T54">dʼɨl-lar</ta>
            <ta e="T56" id="Seg_4829" s="T55">baːl-lar</ta>
            <ta e="T57" id="Seg_4830" s="T56">dʼe</ta>
            <ta e="T58" id="Seg_4831" s="T57">höp</ta>
            <ta e="T59" id="Seg_4832" s="T58">anɨ</ta>
            <ta e="T60" id="Seg_4833" s="T59">oččogo</ta>
            <ta e="T61" id="Seg_4834" s="T60">bihigi</ta>
            <ta e="T62" id="Seg_4835" s="T61">Lörü͡ö</ta>
            <ta e="T63" id="Seg_4836" s="T62">eni͡ene</ta>
            <ta e="T64" id="Seg_4837" s="T783">as-kɨ-n</ta>
            <ta e="T66" id="Seg_4838" s="T64">ah-ɨ͡ak</ta>
            <ta e="T67" id="Seg_4839" s="T66">eː</ta>
            <ta e="T68" id="Seg_4840" s="T67">Tanʼaktaːk</ta>
            <ta e="T69" id="Seg_4841" s="T68">ogonnʼor</ta>
            <ta e="T70" id="Seg_4842" s="T69">gini</ta>
            <ta e="T71" id="Seg_4843" s="T70">ol</ta>
            <ta e="T72" id="Seg_4844" s="T71">gini͡ene</ta>
            <ta e="T73" id="Seg_4845" s="T72">dʼi͡e-ti-ger</ta>
            <ta e="T74" id="Seg_4846" s="T73">bar-am-mɨt</ta>
            <ta e="T75" id="Seg_4847" s="T74">ah-ɨ͡ak</ta>
            <ta e="T76" id="Seg_4848" s="T75">dʼe</ta>
            <ta e="T77" id="Seg_4849" s="T76">höp</ta>
            <ta e="T78" id="Seg_4850" s="T77">innʼe</ta>
            <ta e="T79" id="Seg_4851" s="T78">ah-ɨ͡ak</ta>
            <ta e="T80" id="Seg_4852" s="T79">oː</ta>
            <ta e="T81" id="Seg_4853" s="T80">dʼe</ta>
            <ta e="T82" id="Seg_4854" s="T81">ahɨː-l-lar</ta>
            <ta e="T83" id="Seg_4855" s="T82">araj</ta>
            <ta e="T84" id="Seg_4856" s="T83">Lörü͡ö</ta>
            <ta e="T85" id="Seg_4857" s="T84">dʼi͡e-ti-ger</ta>
            <ta e="T86" id="Seg_4858" s="T85">ahɨː-l-lar</ta>
            <ta e="T87" id="Seg_4859" s="T86">biːr</ta>
            <ta e="T88" id="Seg_4860" s="T87">aŋar-ɨ</ta>
            <ta e="T89" id="Seg_4861" s="T88">dʼe</ta>
            <ta e="T90" id="Seg_4862" s="T89">baran-ɨ͡ag-a-r</ta>
            <ta e="T91" id="Seg_4863" s="T90">di͡eri</ta>
            <ta e="T92" id="Seg_4864" s="T91">dʼe</ta>
            <ta e="T93" id="Seg_4865" s="T92">ahaː-n-nar</ta>
            <ta e="T94" id="Seg_4866" s="T93">on-tu-larɨ-n</ta>
            <ta e="T95" id="Seg_4867" s="T94">as-tara</ta>
            <ta e="T96" id="Seg_4868" s="T95">baran-n-a</ta>
            <ta e="T97" id="Seg_4869" s="T96">Lörü͡ö</ta>
            <ta e="T98" id="Seg_4870" s="T97">gi͡en-e</ta>
            <ta e="T99" id="Seg_4871" s="T98">dʼe</ta>
            <ta e="T100" id="Seg_4872" s="T99">Tanʼaktaːk</ta>
            <ta e="T101" id="Seg_4873" s="T100">ogonnʼor-go</ta>
            <ta e="T102" id="Seg_4874" s="T101">bar-ɨ͡agɨŋ</ta>
            <ta e="T103" id="Seg_4875" s="T102">bar-an-nar</ta>
            <ta e="T104" id="Seg_4876" s="T103">dʼe</ta>
            <ta e="T105" id="Seg_4877" s="T104">ol</ta>
            <ta e="T106" id="Seg_4878" s="T105">ah-ɨ-n</ta>
            <ta e="T107" id="Seg_4879" s="T106">ah-ɨːhɨ-lar</ta>
            <ta e="T108" id="Seg_4880" s="T107">oː</ta>
            <ta e="T109" id="Seg_4881" s="T108">Tanʼaktaːk</ta>
            <ta e="T110" id="Seg_4882" s="T109">ogonnʼor-go</ta>
            <ta e="T111" id="Seg_4883" s="T110">bar-dɨ-lar</ta>
            <ta e="T113" id="Seg_4884" s="T112">dʼe</ta>
            <ta e="T114" id="Seg_4885" s="T113">ahɨː-l-lar</ta>
            <ta e="T115" id="Seg_4886" s="T114">ahɨː-l-lar-ahɨː-l-lar</ta>
            <ta e="T116" id="Seg_4887" s="T115">da</ta>
            <ta e="T117" id="Seg_4888" s="T116">kajaː</ta>
            <ta e="T118" id="Seg_4889" s="T117">biːr</ta>
            <ta e="T119" id="Seg_4890" s="T118">dogor-o</ta>
            <ta e="T120" id="Seg_4891" s="T119">mini͡ene</ta>
            <ta e="T121" id="Seg_4892" s="T120">ah-ɨ-m</ta>
            <ta e="T122" id="Seg_4893" s="T121">baran-ar</ta>
            <ta e="T123" id="Seg_4894" s="T122">bu͡ol-tak</ta>
            <ta e="T124" id="Seg_4895" s="T784">diː-r</ta>
            <ta e="T125" id="Seg_4896" s="T124">en</ta>
            <ta e="T126" id="Seg_4897" s="T125">dʼi͡e-ge-r</ta>
            <ta e="T128" id="Seg_4898" s="T126">bar</ta>
            <ta e="T129" id="Seg_4899" s="T128">beje-ŋ</ta>
            <ta e="T130" id="Seg_4900" s="T129">bult-a-n-an</ta>
            <ta e="T131" id="Seg_4901" s="T130">min</ta>
            <ta e="T132" id="Seg_4902" s="T131">as-pɨ-n</ta>
            <ta e="T133" id="Seg_4903" s="T132">barɨ-tɨ-n</ta>
            <ta e="T134" id="Seg_4904" s="T133">h-i͡ep-pi-n</ta>
            <ta e="T135" id="Seg_4905" s="T134">bagar-bap-pɨn</ta>
            <ta e="T136" id="Seg_4906" s="T135">eː</ta>
            <ta e="T137" id="Seg_4907" s="T136">dʼe</ta>
            <ta e="T138" id="Seg_4908" s="T137">bar-an</ta>
            <ta e="T139" id="Seg_4909" s="T138">kaːl-l-a</ta>
            <ta e="T140" id="Seg_4910" s="T139">dogor-o</ta>
            <ta e="T141" id="Seg_4911" s="T140">kajaː</ta>
            <ta e="T142" id="Seg_4912" s="T141">bar-an</ta>
            <ta e="T143" id="Seg_4913" s="T142">tu͡og-u</ta>
            <ta e="T144" id="Seg_4914" s="T143">h-i͡eg-e=j</ta>
            <ta e="T145" id="Seg_4915" s="T144">dʼi͡e-ti-ger</ta>
            <ta e="T146" id="Seg_4916" s="T145">Löːrü͡ö</ta>
            <ta e="T147" id="Seg_4917" s="T146">bar-bɨt-a</ta>
            <ta e="T148" id="Seg_4918" s="T147">dʼi͡e-ti-ger</ta>
            <ta e="T149" id="Seg_4919" s="T148">tu͡og-a</ta>
            <ta e="T150" id="Seg_4920" s="T149">da</ta>
            <ta e="T151" id="Seg_4921" s="T150">hu͡ok</ta>
            <ta e="T152" id="Seg_4922" s="T151">tu͡ok</ta>
            <ta e="T153" id="Seg_4923" s="T152">ere</ta>
            <ta e="T154" id="Seg_4924" s="T153">kačča</ta>
            <ta e="T155" id="Seg_4925" s="T154">bɨlčɨŋ-kaːn-ɨ</ta>
            <ta e="T156" id="Seg_4926" s="T155">bul-u-n-an</ta>
            <ta e="T157" id="Seg_4927" s="T156">on-tu-tu-n</ta>
            <ta e="T158" id="Seg_4928" s="T157">kü͡öst-e-n-n-e</ta>
            <ta e="T159" id="Seg_4929" s="T158">kü͡öst-e-n-en</ta>
            <ta e="T160" id="Seg_4930" s="T159">olor-or</ta>
            <ta e="T161" id="Seg_4931" s="T160">araj</ta>
            <ta e="T162" id="Seg_4932" s="T161">kaː</ta>
            <ta e="T163" id="Seg_4933" s="T162">onton</ta>
            <ta e="T164" id="Seg_4934" s="T163">kör-büt-e</ta>
            <ta e="T165" id="Seg_4935" s="T164">tug-u</ta>
            <ta e="T166" id="Seg_4936" s="T165">ere</ta>
            <ta e="T167" id="Seg_4937" s="T166">bult-a-m-mɨt</ta>
            <ta e="T168" id="Seg_4938" s="T167">emi͡e</ta>
            <ta e="T169" id="Seg_4939" s="T168">Löːrü͡ö</ta>
            <ta e="T170" id="Seg_4940" s="T169">onton</ta>
            <ta e="T171" id="Seg_4941" s="T170">maːjdʼɨn</ta>
            <ta e="T172" id="Seg_4942" s="T171">Tanʼaktaːk</ta>
            <ta e="T173" id="Seg_4943" s="T172">ogonnʼor</ta>
            <ta e="T174" id="Seg_4944" s="T173">da</ta>
            <ta e="T175" id="Seg_4945" s="T174">ah-a</ta>
            <ta e="T176" id="Seg_4946" s="T175">baram-mɨt</ta>
            <ta e="T177" id="Seg_4947" s="T176">e-t-e</ta>
            <ta e="T178" id="Seg_4948" s="T177">hopsi͡em</ta>
            <ta e="T179" id="Seg_4949" s="T178">dʼe</ta>
            <ta e="T180" id="Seg_4950" s="T179">olor-or</ta>
            <ta e="T181" id="Seg_4951" s="T180">bil-s-i-bet</ta>
            <ta e="T182" id="Seg_4952" s="T181">gini͡e-ke</ta>
            <ta e="T183" id="Seg_4953" s="T182">dʼe</ta>
            <ta e="T184" id="Seg_4954" s="T183">kel-be-tin</ta>
            <ta e="T185" id="Seg_4955" s="T184">gini</ta>
            <ta e="T186" id="Seg_4956" s="T185">beje-m</ta>
            <ta e="T187" id="Seg_4957" s="T186">da</ta>
            <ta e="T188" id="Seg_4958" s="T187">bul-tan-an</ta>
            <ta e="T189" id="Seg_4959" s="T188">olor-oːn</ta>
            <ta e="T190" id="Seg_4960" s="T189">biːr</ta>
            <ta e="T191" id="Seg_4961" s="T190">tu͡ok-kaːn</ta>
            <ta e="T192" id="Seg_4962" s="T191">eme</ta>
            <ta e="T193" id="Seg_4963" s="T192">diː-r</ta>
            <ta e="T194" id="Seg_4964" s="T193">Lörü͡ö</ta>
            <ta e="T195" id="Seg_4965" s="T194">araj</ta>
            <ta e="T196" id="Seg_4966" s="T195">olor-or</ta>
            <ta e="T197" id="Seg_4967" s="T196">bu</ta>
            <ta e="T198" id="Seg_4968" s="T197">olor-dog-una</ta>
            <ta e="T199" id="Seg_4969" s="T198">Tanʼaktaːk</ta>
            <ta e="T200" id="Seg_4970" s="T199">ogonnʼor</ta>
            <ta e="T201" id="Seg_4971" s="T200">kel-bit</ta>
            <ta e="T202" id="Seg_4972" s="T201">ih-er-i-n</ta>
            <ta e="T203" id="Seg_4973" s="T202">kör-d-ö</ta>
            <ta e="T204" id="Seg_4974" s="T203">dogor-u-n</ta>
            <ta e="T205" id="Seg_4975" s="T204">bɨː</ta>
            <ta e="T206" id="Seg_4976" s="T205">Tanʼaktaːk</ta>
            <ta e="T207" id="Seg_4977" s="T206">ogonnʼor</ta>
            <ta e="T208" id="Seg_4978" s="T207">ih-er</ta>
            <ta e="T209" id="Seg_4979" s="T208">e-bit</ta>
            <ta e="T210" id="Seg_4980" s="T209">Lörü͡ö</ta>
            <ta e="T211" id="Seg_4981" s="T210">diː-r</ta>
            <ta e="T212" id="Seg_4982" s="T785">dʼi͡e-ti-n</ta>
            <ta e="T213" id="Seg_4983" s="T212">kat-a-n-an</ta>
            <ta e="T214" id="Seg_4984" s="T213">hab-an</ta>
            <ta e="T216" id="Seg_4985" s="T214">keːs-pit</ta>
            <ta e="T217" id="Seg_4986" s="T216">dʼe</ta>
            <ta e="T218" id="Seg_4987" s="T217">kel-er</ta>
            <ta e="T219" id="Seg_4988" s="T218">araj</ta>
            <ta e="T220" id="Seg_4989" s="T219">aː</ta>
            <ta e="T223" id="Seg_4990" s="T222">Lörü͡ö-tü-ger</ta>
            <ta e="T224" id="Seg_4991" s="T223">kel-er</ta>
            <ta e="T225" id="Seg_4992" s="T224">Lörü͡ö</ta>
            <ta e="T226" id="Seg_4993" s="T225">dogor-doː</ta>
            <ta e="T227" id="Seg_4994" s="T226">Lörü͡ö</ta>
            <ta e="T228" id="Seg_4995" s="T227">dogor-doː</ta>
            <ta e="T229" id="Seg_4996" s="T228">en</ta>
            <ta e="T230" id="Seg_4997" s="T229">bi͡er</ta>
            <ta e="T231" id="Seg_4998" s="T230">eme</ta>
            <ta e="T232" id="Seg_4999" s="T786">bɨldʼɨŋ-kaːn-na</ta>
            <ta e="T233" id="Seg_5000" s="T232">bɨrag-an</ta>
            <ta e="T234" id="Seg_5001" s="T233">abɨraː</ta>
            <ta e="T235" id="Seg_5002" s="T234">Löːrü͡ö</ta>
            <ta e="T237" id="Seg_5003" s="T235">dogor-doː</ta>
            <ta e="T238" id="Seg_5004" s="T237">ardaː-n</ta>
            <ta e="T239" id="Seg_5005" s="T238">küm-me-r</ta>
            <ta e="T240" id="Seg_5006" s="T239">tur-d-a</ta>
            <ta e="T241" id="Seg_5007" s="T240">dʼɨl-bɨ-n</ta>
            <ta e="T242" id="Seg_5008" s="T241">solgoː</ta>
            <ta e="T243" id="Seg_5009" s="T242">Löːrü͡ö</ta>
            <ta e="T244" id="Seg_5010" s="T243">dogor-doː</ta>
            <ta e="T245" id="Seg_5011" s="T244">Löːrü͡ö</ta>
            <ta e="T246" id="Seg_5012" s="T245">dogor-doː</ta>
            <ta e="T247" id="Seg_5013" s="T246">öl-lü-m</ta>
            <ta e="T248" id="Seg_5014" s="T247">kaːl-ɨ͡a-m</ta>
            <ta e="T249" id="Seg_5015" s="T248">biːr-i</ta>
            <ta e="T250" id="Seg_5016" s="T249">ɨll-ɨː</ta>
            <ta e="T251" id="Seg_5017" s="T250">olor-but</ta>
            <ta e="T252" id="Seg_5018" s="T251">törüt</ta>
            <ta e="T253" id="Seg_5019" s="T252">kaː</ta>
            <ta e="T254" id="Seg_5020" s="T253">bar-bar</ta>
            <ta e="T255" id="Seg_5021" s="T254">biːr</ta>
            <ta e="T256" id="Seg_5022" s="T255">da</ta>
            <ta e="T257" id="Seg_5023" s="T256">biːr</ta>
            <ta e="T258" id="Seg_5024" s="T257">tug-u</ta>
            <ta e="T259" id="Seg_5025" s="T258">büttes-ke</ta>
            <ta e="T260" id="Seg_5026" s="T259">hɨst-ɨ-bɨt</ta>
            <ta e="T262" id="Seg_5027" s="T261">kap-pɨt</ta>
            <ta e="T263" id="Seg_5028" s="T262">bɨlčɨŋ-kaːn-ɨ</ta>
            <ta e="T264" id="Seg_5029" s="T263">bɨrag-an</ta>
            <ta e="T265" id="Seg_5030" s="T264">keːs-t-e</ta>
            <ta e="T266" id="Seg_5031" s="T265">bar</ta>
            <ta e="T267" id="Seg_5032" s="T266">diː-r</ta>
            <ta e="T268" id="Seg_5033" s="T267">on-tu-ta</ta>
            <ta e="T269" id="Seg_5034" s="T268">Tanʼaktaːk</ta>
            <ta e="T270" id="Seg_5035" s="T269">bar-an</ta>
            <ta e="T271" id="Seg_5036" s="T270">kaːl-l-a</ta>
            <ta e="T272" id="Seg_5037" s="T271">bar</ta>
            <ta e="T273" id="Seg_5038" s="T272">dʼi͡e-ti-ger</ta>
            <ta e="T274" id="Seg_5039" s="T273">bar</ta>
            <ta e="T275" id="Seg_5040" s="T274">dʼi͡e-ge-r</ta>
            <ta e="T276" id="Seg_5041" s="T275">diː-r</ta>
            <ta e="T277" id="Seg_5042" s="T276">bolse</ta>
            <ta e="T278" id="Seg_5043" s="T277">bi͡er-bep-pin</ta>
            <ta e="T279" id="Seg_5044" s="T278">eni͡e-ke</ta>
            <ta e="T280" id="Seg_5045" s="T279">koː</ta>
            <ta e="T281" id="Seg_5046" s="T280">bar-an</ta>
            <ta e="T282" id="Seg_5047" s="T281">kaːl-l-a</ta>
            <ta e="T283" id="Seg_5048" s="T282">kihi</ta>
            <ta e="T284" id="Seg_5049" s="T283">bar-an</ta>
            <ta e="T285" id="Seg_5050" s="T284">toholaːk</ta>
            <ta e="T286" id="Seg_5051" s="T285">č-i͡eg-e=j</ta>
            <ta e="T287" id="Seg_5052" s="T286">gini</ta>
            <ta e="T288" id="Seg_5053" s="T287">onton</ta>
            <ta e="T289" id="Seg_5054" s="T288">ol-bu</ta>
            <ta e="T290" id="Seg_5055" s="T289">dʼe</ta>
            <ta e="T291" id="Seg_5056" s="T290">kaːn-ɨ-n</ta>
            <ta e="T292" id="Seg_5057" s="T291">postu͡oj</ta>
            <ta e="T293" id="Seg_5058" s="T292">postu͡oj</ta>
            <ta e="T294" id="Seg_5059" s="T293">min-i-n</ta>
            <ta e="T295" id="Seg_5060" s="T294">agaj</ta>
            <ta e="T298" id="Seg_5061" s="T297">kü͡öst-e-n-en</ta>
            <ta e="T299" id="Seg_5062" s="T298">ši͡e-bit</ta>
            <ta e="T300" id="Seg_5063" s="T299">da</ta>
            <ta e="T301" id="Seg_5064" s="T300">tu͡ok</ta>
            <ta e="T302" id="Seg_5065" s="T301">da</ta>
            <ta e="T303" id="Seg_5066" s="T302">bul-bat</ta>
            <ta e="T304" id="Seg_5067" s="T303">da</ta>
            <ta e="T305" id="Seg_5068" s="T304">kanʼaː-bat</ta>
            <ta e="T306" id="Seg_5069" s="T305">daːganɨ</ta>
            <ta e="T307" id="Seg_5070" s="T306">kas</ta>
            <ta e="T308" id="Seg_5071" s="T307">da</ta>
            <ta e="T309" id="Seg_5072" s="T308">kün-ü</ta>
            <ta e="T310" id="Seg_5073" s="T309">hɨp-pɨt-a</ta>
            <ta e="T311" id="Seg_5074" s="T310">da</ta>
            <ta e="T312" id="Seg_5075" s="T311">araj</ta>
            <ta e="T313" id="Seg_5076" s="T312">da</ta>
            <ta e="T314" id="Seg_5077" s="T313">kaːm-an</ta>
            <ta e="T315" id="Seg_5078" s="T314">taks-ar</ta>
            <ta e="T316" id="Seg_5079" s="T315">bu͡ol-but</ta>
            <ta e="T317" id="Seg_5080" s="T316">uhug-u-ttan</ta>
            <ta e="T318" id="Seg_5081" s="T317">okt-on</ta>
            <ta e="T319" id="Seg_5082" s="T318">Tanʼaktaːk</ta>
            <ta e="T320" id="Seg_5083" s="T319">ogonnʼor</ta>
            <ta e="T321" id="Seg_5084" s="T320">epeːt</ta>
            <ta e="T322" id="Seg_5085" s="T321">Löːrü͡ö-tü-ger</ta>
            <ta e="T323" id="Seg_5086" s="T322">bar-ɨːhɨ</ta>
            <ta e="T324" id="Seg_5087" s="T323">kel-en</ta>
            <ta e="T325" id="Seg_5088" s="T324">Löːrü͡ö-tü-ger</ta>
            <ta e="T326" id="Seg_5089" s="T325">emi͡e</ta>
            <ta e="T327" id="Seg_5090" s="T326">ɨllaː-bɨt</ta>
            <ta e="T328" id="Seg_5091" s="T327">Löːrü͡ö</ta>
            <ta e="T329" id="Seg_5092" s="T328">dogor-doː</ta>
            <ta e="T330" id="Seg_5093" s="T329">Löːrü͡ö</ta>
            <ta e="T331" id="Seg_5094" s="T330">dogor-doː</ta>
            <ta e="T332" id="Seg_5095" s="T787">öl-lü-m</ta>
            <ta e="T333" id="Seg_5096" s="T332">kaːl-ɨ͡a-m</ta>
            <ta e="T334" id="Seg_5097" s="T333">öl-lü-m</ta>
            <ta e="T336" id="Seg_5098" s="T334">kaːl-ɨ͡a-m</ta>
            <ta e="T337" id="Seg_5099" s="T336">Löːrü͡ö</ta>
            <ta e="T338" id="Seg_5100" s="T337">dogor-doː</ta>
            <ta e="T339" id="Seg_5101" s="T338">biːr</ta>
            <ta e="T340" id="Seg_5102" s="T339">emeː</ta>
            <ta e="T342" id="Seg_5103" s="T341">eme</ta>
            <ta e="T343" id="Seg_5104" s="T342">bɨlčɨŋ-kaːn-na</ta>
            <ta e="T344" id="Seg_5105" s="T343">bɨrag-an</ta>
            <ta e="T345" id="Seg_5106" s="T344">abɨraː</ta>
            <ta e="T346" id="Seg_5107" s="T345">Löːrü͡ö</ta>
            <ta e="T347" id="Seg_5108" s="T346">dogor-doː</ta>
            <ta e="T348" id="Seg_5109" s="T347">Löːrü͡ö</ta>
            <ta e="T349" id="Seg_5110" s="T348">dogor-do</ta>
            <ta e="T350" id="Seg_5111" s="T349">ardaː-n</ta>
            <ta e="T351" id="Seg_5112" s="T350">umn-an</ta>
            <ta e="T352" id="Seg_5113" s="T788">bu͡ol-l-a</ta>
            <ta e="T353" id="Seg_5114" s="T352">bul-u͡o</ta>
            <ta e="T354" id="Seg_5115" s="T353">Löːrü͡ö</ta>
            <ta e="T355" id="Seg_5116" s="T354">dogor-doː</ta>
            <ta e="T356" id="Seg_5117" s="T355">ardaː-n</ta>
            <ta e="T357" id="Seg_5118" s="T356">bɨlčɨŋ-na</ta>
            <ta e="T358" id="Seg_5119" s="T357">egel</ta>
            <ta e="T360" id="Seg_5120" s="T358">ɨll-a-n-a-bɨn</ta>
            <ta e="T361" id="Seg_5121" s="T360">e</ta>
            <ta e="T362" id="Seg_5122" s="T361">biːr</ta>
            <ta e="T363" id="Seg_5123" s="T362">da</ta>
            <ta e="T364" id="Seg_5124" s="T363">bɨlčɨŋ-a</ta>
            <ta e="T365" id="Seg_5125" s="T364">hu͡ok-pun</ta>
            <ta e="T366" id="Seg_5126" s="T365">bar</ta>
            <ta e="T367" id="Seg_5127" s="T366">bar</ta>
            <ta e="T368" id="Seg_5128" s="T367">di͡e-bit</ta>
            <ta e="T369" id="Seg_5129" s="T368">töttörü</ta>
            <ta e="T370" id="Seg_5130" s="T369">bat-an</ta>
            <ta e="T371" id="Seg_5131" s="T370">ɨːp-pɨt</ta>
            <ta e="T372" id="Seg_5132" s="T371">tu͡ok-kaːn-ɨ</ta>
            <ta e="T373" id="Seg_5133" s="T372">da</ta>
            <ta e="T374" id="Seg_5134" s="T373">bi͡er-betek</ta>
            <ta e="T375" id="Seg_5135" s="T374">kajdi͡ek</ta>
            <ta e="T376" id="Seg_5136" s="T375">bar-ɨ͡a=j</ta>
            <ta e="T377" id="Seg_5137" s="T376">tahaːra</ta>
            <ta e="T378" id="Seg_5138" s="T377">otto</ta>
            <ta e="T379" id="Seg_5139" s="T378">toŋ-or</ta>
            <ta e="T380" id="Seg_5140" s="T379">bu͡ol-l-a</ta>
            <ta e="T381" id="Seg_5141" s="T380">kajdi͡et</ta>
            <ta e="T382" id="Seg_5142" s="T381">dʼi͡e-ge</ta>
            <ta e="T383" id="Seg_5143" s="T382">da</ta>
            <ta e="T384" id="Seg_5144" s="T383">kiːl-ler-bet</ta>
            <ta e="T385" id="Seg_5145" s="T384">saːt</ta>
            <ta e="T386" id="Seg_5146" s="T385">dʼe</ta>
            <ta e="T387" id="Seg_5147" s="T386">bar-ar</ta>
            <ta e="T388" id="Seg_5148" s="T387">bu</ta>
            <ta e="T389" id="Seg_5149" s="T388">kihi</ta>
            <ta e="T390" id="Seg_5150" s="T389">bar-bɨt-a</ta>
            <ta e="T391" id="Seg_5151" s="T390">araččɨ</ta>
            <ta e="T392" id="Seg_5152" s="T391">onton</ta>
            <ta e="T393" id="Seg_5153" s="T392">tij-en</ta>
            <ta e="T394" id="Seg_5154" s="T393">is-teg-ine</ta>
            <ta e="T397" id="Seg_5155" s="T396">dʼi͡e-ti-n</ta>
            <ta e="T398" id="Seg_5156" s="T397">attɨ-gar</ta>
            <ta e="T399" id="Seg_5157" s="T398">tij-er</ta>
            <ta e="T400" id="Seg_5158" s="T399">kör-büt-e</ta>
            <ta e="T401" id="Seg_5159" s="T400">tugut-taːk</ta>
            <ta e="T402" id="Seg_5160" s="T401">tɨhɨ</ta>
            <ta e="T403" id="Seg_5161" s="T402">hɨldʼ-ar</ta>
            <ta e="T404" id="Seg_5162" s="T403">dʼi͡e-ti-ger</ta>
            <ta e="T405" id="Seg_5163" s="T404">kaja</ta>
            <ta e="T406" id="Seg_5164" s="T405">kel-iː-tten</ta>
            <ta e="T407" id="Seg_5165" s="T406">kel-bit-e</ta>
            <ta e="T408" id="Seg_5166" s="T407">dʼürü</ta>
            <ta e="T409" id="Seg_5167" s="T408">bil-bet</ta>
            <ta e="T410" id="Seg_5168" s="T409">taba-nɨ</ta>
            <ta e="T411" id="Seg_5169" s="T410">gini</ta>
            <ta e="T412" id="Seg_5170" s="T789">taba</ta>
            <ta e="T413" id="Seg_5171" s="T412">hɨldʼ-ar</ta>
            <ta e="T414" id="Seg_5172" s="T413">araj</ta>
            <ta e="T416" id="Seg_5173" s="T414">kel-bit</ta>
            <ta e="T417" id="Seg_5174" s="T416">örö</ta>
            <ta e="T418" id="Seg_5175" s="T417">gini</ta>
            <ta e="T419" id="Seg_5176" s="T418">öl-ü͡ök</ta>
            <ta e="T420" id="Seg_5177" s="T419">ɨjaːg-ɨ-gar</ta>
            <ta e="T423" id="Seg_5178" s="T422">till-i͡ek</ta>
            <ta e="T424" id="Seg_5179" s="T423">ɨjaːg-ɨ-gar</ta>
            <ta e="T425" id="Seg_5180" s="T424">tugut-taːk</ta>
            <ta e="T426" id="Seg_5181" s="T425">tɨhɨ-ta</ta>
            <ta e="T427" id="Seg_5182" s="T426">beje-ti-ger</ta>
            <ta e="T428" id="Seg_5183" s="T427">kel-en</ta>
            <ta e="T429" id="Seg_5184" s="T428">hal-ɨː</ta>
            <ta e="T430" id="Seg_5185" s="T429">hɨldʼ-ɨ-bɨt-tar</ta>
            <ta e="T431" id="Seg_5186" s="T430">gini-ni</ta>
            <ta e="T432" id="Seg_5187" s="T431">iːkteː-bit-i-ger</ta>
            <ta e="T433" id="Seg_5188" s="T432">ih-i-ger</ta>
            <ta e="T434" id="Seg_5189" s="T433">kel-bit-e</ta>
            <ta e="T435" id="Seg_5190" s="T434">da</ta>
            <ta e="T436" id="Seg_5191" s="T435">tuguk-kaːn-ɨ-n</ta>
            <ta e="T437" id="Seg_5192" s="T436">kab-an</ta>
            <ta e="T438" id="Seg_5193" s="T437">ɨl-an</ta>
            <ta e="T439" id="Seg_5194" s="T438">bahak-kaːn-ɨ</ta>
            <ta e="T440" id="Seg_5195" s="T439">ɨl-l-a</ta>
            <ta e="T441" id="Seg_5196" s="T440">da</ta>
            <ta e="T442" id="Seg_5197" s="T441">kort</ta>
            <ta e="T443" id="Seg_5198" s="T442">gɨn-nar-an</ta>
            <ta e="T444" id="Seg_5199" s="T443">ölör-ön</ta>
            <ta e="T445" id="Seg_5200" s="T444">keːs-pit</ta>
            <ta e="T446" id="Seg_5201" s="T445">inʼe-te</ta>
            <ta e="T447" id="Seg_5202" s="T446">hɨldʼ-ar</ta>
            <ta e="T448" id="Seg_5203" s="T447">ko</ta>
            <ta e="T449" id="Seg_5204" s="T448">man-tɨ-tɨ-n</ta>
            <ta e="T450" id="Seg_5205" s="T449">tu͡ok</ta>
            <ta e="T451" id="Seg_5206" s="T450">tu͡ok</ta>
            <ta e="T454" id="Seg_5207" s="T453">hül-ü-m-müt</ta>
            <ta e="T455" id="Seg_5208" s="T454">kihi</ta>
            <ta e="T456" id="Seg_5209" s="T455">hiːkej-d-iː</ta>
            <ta e="T457" id="Seg_5210" s="T456">buh-a-r-an</ta>
            <ta e="T458" id="Seg_5211" s="T457">kü͡öst-e-n-en</ta>
            <ta e="T459" id="Seg_5212" s="T458">hi͡e-n</ta>
            <ta e="T460" id="Seg_5213" s="T459">keːs-pit</ta>
            <ta e="T461" id="Seg_5214" s="T460">dʼe</ta>
            <ta e="T462" id="Seg_5215" s="T461">karag-a</ta>
            <ta e="T463" id="Seg_5216" s="T462">hɨrdaː-bɨt</ta>
            <ta e="T464" id="Seg_5217" s="T463">onno</ta>
            <ta e="T465" id="Seg_5218" s="T464">oː</ta>
            <ta e="T466" id="Seg_5219" s="T465">dʼe</ta>
            <ta e="T467" id="Seg_5220" s="T466">anɨ</ta>
            <ta e="T468" id="Seg_5221" s="T467">bagas</ta>
            <ta e="T469" id="Seg_5222" s="T468">abɨr-a-n-nɨ-m</ta>
            <ta e="T470" id="Seg_5223" s="T469">Löːrü͡ö-ge</ta>
            <ta e="T471" id="Seg_5224" s="T470">bar-bap-pɨn</ta>
            <ta e="T472" id="Seg_5225" s="T471">kanʼaː-bap-pɨn</ta>
            <ta e="T473" id="Seg_5226" s="T472">as-tan-nɨ-m</ta>
            <ta e="T474" id="Seg_5227" s="T473">bu͡o</ta>
            <ta e="T475" id="Seg_5228" s="T474">diː-r</ta>
            <ta e="T476" id="Seg_5229" s="T475">as-tam-mɨt</ta>
            <ta e="T477" id="Seg_5230" s="T476">kihi</ta>
            <ta e="T478" id="Seg_5231" s="T477">kü͡öst-e-n-en</ta>
            <ta e="T479" id="Seg_5232" s="T478">baraːn</ta>
            <ta e="T480" id="Seg_5233" s="T479">ahɨː-r</ta>
            <ta e="T481" id="Seg_5234" s="T480">oːk</ta>
            <ta e="T482" id="Seg_5235" s="T481">dʼeː</ta>
            <ta e="T483" id="Seg_5236" s="T482">kas</ta>
            <ta e="T484" id="Seg_5237" s="T483">kün-ü</ta>
            <ta e="T485" id="Seg_5238" s="T484">olor-but-a</ta>
            <ta e="T486" id="Seg_5239" s="T485">dʼürü</ta>
            <ta e="T487" id="Seg_5240" s="T486">taba-laːk</ta>
            <ta e="T488" id="Seg_5241" s="T487">taba-tɨ-n</ta>
            <ta e="T489" id="Seg_5242" s="T488">itte</ta>
            <ta e="T490" id="Seg_5243" s="T489">tutt-u-but</ta>
            <ta e="T491" id="Seg_5244" s="T490">tɨhɨ-tɨ-n</ta>
            <ta e="T492" id="Seg_5245" s="T491">tɨhɨ-laːk</ta>
            <ta e="T493" id="Seg_5246" s="T492">tu͡ok</ta>
            <ta e="T494" id="Seg_5247" s="T493">bu͡ol-u͡o=j</ta>
            <ta e="T495" id="Seg_5248" s="T494">anɨ</ta>
            <ta e="T496" id="Seg_5249" s="T495">oː</ta>
            <ta e="T497" id="Seg_5250" s="T496">bu</ta>
            <ta e="T498" id="Seg_5251" s="T497">hɨt-tag-ɨna</ta>
            <ta e="T499" id="Seg_5252" s="T498">Löːrü͡ö-tü-n</ta>
            <ta e="T500" id="Seg_5253" s="T499">haŋa-ta</ta>
            <ta e="T501" id="Seg_5254" s="T500">kel-l-e</ta>
            <ta e="T502" id="Seg_5255" s="T501">dʼe</ta>
            <ta e="T503" id="Seg_5256" s="T502">araj</ta>
            <ta e="T504" id="Seg_5257" s="T503">kör-büt-e</ta>
            <ta e="T505" id="Seg_5258" s="T504">dʼe</ta>
            <ta e="T506" id="Seg_5259" s="T505">min</ta>
            <ta e="T507" id="Seg_5260" s="T506">anɨ</ta>
            <ta e="T508" id="Seg_5261" s="T507">itigirdik</ta>
            <ta e="T509" id="Seg_5262" s="T508">gɨn-ɨ͡a-m</ta>
            <ta e="T510" id="Seg_5263" s="T509">beːbe</ta>
            <ta e="T511" id="Seg_5264" s="T510">kel-lin</ta>
            <ta e="T512" id="Seg_5265" s="T511">agaj</ta>
            <ta e="T513" id="Seg_5266" s="T512">di͡e-bit</ta>
            <ta e="T514" id="Seg_5267" s="T513">mini͡e-ke</ta>
            <ta e="T515" id="Seg_5268" s="T514">as</ta>
            <ta e="T516" id="Seg_5269" s="T515">Löːrü͡ö</ta>
            <ta e="T517" id="Seg_5270" s="T516">tu͡ok-taː-n</ta>
            <ta e="T518" id="Seg_5271" s="T517">arɨččɨ</ta>
            <ta e="T519" id="Seg_5272" s="T518">ih-er</ta>
            <ta e="T520" id="Seg_5273" s="T519">bu͡opsa</ta>
            <ta e="T521" id="Seg_5274" s="T520">korguj-an</ta>
            <ta e="T522" id="Seg_5275" s="T521">araččɨ</ta>
            <ta e="T523" id="Seg_5276" s="T522">kaːm-an</ta>
            <ta e="T524" id="Seg_5277" s="T523">ih-er</ta>
            <ta e="T525" id="Seg_5278" s="T524">kel-bit</ta>
            <ta e="T526" id="Seg_5279" s="T525">da</ta>
            <ta e="T527" id="Seg_5280" s="T526">kel-bit</ta>
            <ta e="T528" id="Seg_5281" s="T527">kel-er-i-n</ta>
            <ta e="T529" id="Seg_5282" s="T528">gɨtta</ta>
            <ta e="T530" id="Seg_5283" s="T529">dʼi͡e-te</ta>
            <ta e="T531" id="Seg_5284" s="T530">kataː-n</ta>
            <ta e="T532" id="Seg_5285" s="T531">kat-a-n-an</ta>
            <ta e="T533" id="Seg_5286" s="T532">keːs-pit</ta>
            <ta e="T534" id="Seg_5287" s="T533">dʼe</ta>
            <ta e="T535" id="Seg_5288" s="T534">gini-ni</ta>
            <ta e="T536" id="Seg_5289" s="T535">nakaːs-t-ɨ͡a-m</ta>
            <ta e="T537" id="Seg_5290" s="T536">maːjdʼɨn</ta>
            <ta e="T538" id="Seg_5291" s="T537">minigi-n</ta>
            <ta e="T539" id="Seg_5292" s="T538">emi͡e</ta>
            <ta e="T540" id="Seg_5293" s="T539">gini</ta>
            <ta e="T541" id="Seg_5294" s="T540">muŋnaː-bɨt-a</ta>
            <ta e="T542" id="Seg_5295" s="T541">burduk</ta>
            <ta e="T543" id="Seg_5296" s="T542">Tanʼaktaːk</ta>
            <ta e="T544" id="Seg_5297" s="T543">ogonnʼor</ta>
            <ta e="T545" id="Seg_5298" s="T544">Tanʼaktaːk</ta>
            <ta e="T546" id="Seg_5299" s="T545">ogonnʼor</ta>
            <ta e="T547" id="Seg_5300" s="T546">biːr</ta>
            <ta e="T548" id="Seg_5301" s="T790">emete</ta>
            <ta e="T549" id="Seg_5302" s="T548">bɨlčɨŋ-kaːn-na</ta>
            <ta e="T550" id="Seg_5303" s="T549">bi͡er-en</ta>
            <ta e="T551" id="Seg_5304" s="T550">abɨraː</ta>
            <ta e="T552" id="Seg_5305" s="T551">Tanʼaktaːk</ta>
            <ta e="T553" id="Seg_5306" s="T552">ogonnʼor</ta>
            <ta e="T554" id="Seg_5307" s="T553">öl-lü-m</ta>
            <ta e="T555" id="Seg_5308" s="T554">kaːl-lɨ-m</ta>
            <ta e="T556" id="Seg_5309" s="T555">öl-lü-m</ta>
            <ta e="T560" id="Seg_5310" s="T558">kaːl-lɨ-m</ta>
            <ta e="T561" id="Seg_5311" s="T560">küm-me-r</ta>
            <ta e="T562" id="Seg_5312" s="T561">tur-d-a</ta>
            <ta e="T563" id="Seg_5313" s="T562">kuŋ</ta>
            <ta e="T564" id="Seg_5314" s="T563">kihi-te</ta>
            <ta e="T565" id="Seg_5315" s="T564">bu͡ol-lak-kɨna</ta>
            <ta e="T566" id="Seg_5316" s="T565">Tanʼaktaːk</ta>
            <ta e="T567" id="Seg_5317" s="T566">ogonnʼor</ta>
            <ta e="T568" id="Seg_5318" s="T567">Tanʼaktaːk</ta>
            <ta e="T569" id="Seg_5319" s="T568">ogonnʼor</ta>
            <ta e="T570" id="Seg_5320" s="T569">ɨll-a-n-a</ta>
            <ta e="T571" id="Seg_5321" s="T570">hɨp-pɨt</ta>
            <ta e="T572" id="Seg_5322" s="T571">tug-u</ta>
            <ta e="T573" id="Seg_5323" s="T572">da</ta>
            <ta e="T574" id="Seg_5324" s="T573">bi͡er-betek</ta>
            <ta e="T575" id="Seg_5325" s="T574">postu͡oj</ta>
            <ta e="T576" id="Seg_5326" s="T575">tu͡ok</ta>
            <ta e="T577" id="Seg_5327" s="T576">ere</ta>
            <ta e="T578" id="Seg_5328" s="T577">bu</ta>
            <ta e="T579" id="Seg_5329" s="T578">tugut-u-n</ta>
            <ta e="T580" id="Seg_5330" s="T579">gi͡en-i-n</ta>
            <ta e="T581" id="Seg_5331" s="T580">tunʼag-a</ta>
            <ta e="T582" id="Seg_5332" s="T581">tunʼak-tarɨ-n</ta>
            <ta e="T583" id="Seg_5333" s="T582">tu͡ok-t-a-m-mɨt</ta>
            <ta e="T584" id="Seg_5334" s="T583">ata-tɨ-n</ta>
            <ta e="T585" id="Seg_5335" s="T584">gɨtta</ta>
            <ta e="T586" id="Seg_5336" s="T585">tunʼag-a</ta>
            <ta e="T587" id="Seg_5337" s="T586">duː</ta>
            <ta e="T588" id="Seg_5338" s="T587">tu͡ok</ta>
            <ta e="T589" id="Seg_5339" s="T588">duː</ta>
            <ta e="T590" id="Seg_5340" s="T589">o-nu</ta>
            <ta e="T593" id="Seg_5341" s="T592">ata-naːk</ta>
            <ta e="T594" id="Seg_5342" s="T593">hɨldʼ-an</ta>
            <ta e="T595" id="Seg_5343" s="T594">o-nu</ta>
            <ta e="T596" id="Seg_5344" s="T595">bɨrag-an</ta>
            <ta e="T597" id="Seg_5345" s="T596">bar</ta>
            <ta e="T598" id="Seg_5346" s="T597">ši͡e</ta>
            <ta e="T599" id="Seg_5347" s="T598">d-iː</ta>
            <ta e="T600" id="Seg_5348" s="T599">ikki</ta>
            <ta e="T601" id="Seg_5349" s="T600">tunʼag-ɨ</ta>
            <ta e="T602" id="Seg_5350" s="T601">bɨrag-an</ta>
            <ta e="T603" id="Seg_5351" s="T602">keːs-pit</ta>
            <ta e="T604" id="Seg_5352" s="T603">bar</ta>
            <ta e="T605" id="Seg_5353" s="T604">dʼi͡e-ge-r</ta>
            <ta e="T606" id="Seg_5354" s="T605">diː-r</ta>
            <ta e="T607" id="Seg_5355" s="T606">bolʼše</ta>
            <ta e="T608" id="Seg_5356" s="T607">kel-i-me</ta>
            <ta e="T611" id="Seg_5357" s="T610">maːjdɨn</ta>
            <ta e="T612" id="Seg_5358" s="T611">minigi-n</ta>
            <ta e="T613" id="Seg_5359" s="T612">emi͡e</ta>
            <ta e="T614" id="Seg_5360" s="T613">dʼe</ta>
            <ta e="T615" id="Seg_5361" s="T614">muŋnaː-bɨt-ɨ-ŋ</ta>
            <ta e="T616" id="Seg_5362" s="T615">on-tu-ta</ta>
            <ta e="T617" id="Seg_5363" s="T616">bɨst-ɨ͡a</ta>
            <ta e="T618" id="Seg_5364" s="T617">du͡o</ta>
            <ta e="T619" id="Seg_5365" s="T618">bar-an</ta>
            <ta e="T620" id="Seg_5366" s="T619">kaːl-bɨt</ta>
            <ta e="T621" id="Seg_5367" s="T620">dʼe</ta>
            <ta e="T622" id="Seg_5368" s="T621">bar-an</ta>
            <ta e="T623" id="Seg_5369" s="T622">kaːl-bɨt</ta>
            <ta e="T624" id="Seg_5370" s="T623">dʼi͡e-ti-ger</ta>
            <ta e="T625" id="Seg_5371" s="T624">emi͡e</ta>
            <ta e="T627" id="Seg_5372" s="T626">bar-bɨt</ta>
            <ta e="T628" id="Seg_5373" s="T627">da</ta>
            <ta e="T629" id="Seg_5374" s="T628">har-tɨ-n</ta>
            <ta e="T632" id="Seg_5375" s="T631">kas</ta>
            <ta e="T633" id="Seg_5376" s="T632">da</ta>
            <ta e="T634" id="Seg_5377" s="T633">örö</ta>
            <ta e="T635" id="Seg_5378" s="T634">gün-ü</ta>
            <ta e="T636" id="Seg_5379" s="T635">bu͡ol-an</ta>
            <ta e="T637" id="Seg_5380" s="T636">baraːn</ta>
            <ta e="T638" id="Seg_5381" s="T637">emi͡e</ta>
            <ta e="T639" id="Seg_5382" s="T638">kel-er</ta>
            <ta e="T640" id="Seg_5383" s="T639">dʼe</ta>
            <ta e="T641" id="Seg_5384" s="T640">vavsʼe</ta>
            <ta e="T642" id="Seg_5385" s="T641">öl-ör-ü-ger</ta>
            <ta e="T643" id="Seg_5386" s="T642">bar-bɨt</ta>
            <ta e="T644" id="Seg_5387" s="T643">bɨhɨlaːk</ta>
            <ta e="T645" id="Seg_5388" s="T644">dogor-o</ta>
            <ta e="T646" id="Seg_5389" s="T645">Löːrü͡ö-te</ta>
            <ta e="T647" id="Seg_5390" s="T646">ɨksaː-bɨt</ta>
            <ta e="T648" id="Seg_5391" s="T647">bat-tag-ɨna</ta>
            <ta e="T649" id="Seg_5392" s="T648">da</ta>
            <ta e="T650" id="Seg_5393" s="T649">bar-bat</ta>
            <ta e="T651" id="Seg_5394" s="T650">kiːr-deg-ine</ta>
            <ta e="T652" id="Seg_5395" s="T651">kel-bet</ta>
            <ta e="T653" id="Seg_5396" s="T652">oː</ta>
            <ta e="T654" id="Seg_5397" s="T653">dʼe</ta>
            <ta e="T655" id="Seg_5398" s="T654">kajdak</ta>
            <ta e="T656" id="Seg_5399" s="T655">da</ta>
            <ta e="T657" id="Seg_5400" s="T656">gɨn-ɨ͡ag-ɨ-n</ta>
            <ta e="T658" id="Seg_5401" s="T657">bert</ta>
            <ta e="T659" id="Seg_5402" s="T658">dʼe</ta>
            <ta e="T660" id="Seg_5403" s="T659">hüreg-e</ta>
            <ta e="T661" id="Seg_5404" s="T660">ɨ͡arɨː</ta>
            <ta e="T662" id="Seg_5405" s="T661">ɨ͡aldʼ-ar</ta>
            <ta e="T663" id="Seg_5406" s="T662">kihi</ta>
            <ta e="T664" id="Seg_5407" s="T663">dogor-u-n</ta>
            <ta e="T665" id="Seg_5408" s="T664">ahɨn-n-a</ta>
            <ta e="T666" id="Seg_5409" s="T665">diː</ta>
            <ta e="T667" id="Seg_5410" s="T666">ahɨn-an</ta>
            <ta e="T668" id="Seg_5411" s="T667">kajdak</ta>
            <ta e="T669" id="Seg_5412" s="T668">da</ta>
            <ta e="T670" id="Seg_5413" s="T669">gɨn-ɨ͡ag-ɨ-n</ta>
            <ta e="T671" id="Seg_5414" s="T670">bert</ta>
            <ta e="T672" id="Seg_5415" s="T671">bat-t-ɨ͡ag-ɨ-n</ta>
            <ta e="T673" id="Seg_5416" s="T672">daː</ta>
            <ta e="T674" id="Seg_5417" s="T791">bar-an</ta>
            <ta e="T675" id="Seg_5418" s="T674">da</ta>
            <ta e="T676" id="Seg_5419" s="T675">hin</ta>
            <ta e="T677" id="Seg_5420" s="T676">dʼi͡e-ti-ger</ta>
            <ta e="T678" id="Seg_5421" s="T677">tij-i͡e</ta>
            <ta e="T680" id="Seg_5422" s="T678">hu͡ok</ta>
            <ta e="T681" id="Seg_5423" s="T792">ajɨːlaːgɨn</ta>
            <ta e="T682" id="Seg_5424" s="T681">kiːr</ta>
            <ta e="T683" id="Seg_5425" s="T682">di͡e-bit</ta>
            <ta e="T684" id="Seg_5426" s="T683">ahaː</ta>
            <ta e="T685" id="Seg_5427" s="T684">kepset-i͡ek</ta>
            <ta e="T686" id="Seg_5428" s="T685">oččogo</ta>
            <ta e="T688" id="Seg_5429" s="T686">üčügej-dik</ta>
            <ta e="T689" id="Seg_5430" s="T688">dʼe</ta>
            <ta e="T690" id="Seg_5431" s="T689">ilbi</ta>
            <ta e="T691" id="Seg_5432" s="T690">ile</ta>
            <ta e="T692" id="Seg_5433" s="T691">kepsep-pit-ter</ta>
            <ta e="T693" id="Seg_5434" s="T692">kaja</ta>
            <ta e="T694" id="Seg_5435" s="T693">otto</ta>
            <ta e="T695" id="Seg_5436" s="T694">bihigi</ta>
            <ta e="T696" id="Seg_5437" s="T695">üčügej-dik</ta>
            <ta e="T697" id="Seg_5438" s="T696">kepset-i͡ek</ta>
            <ta e="T698" id="Seg_5439" s="T697">biːrge</ta>
            <ta e="T699" id="Seg_5440" s="T698">anɨ</ta>
            <ta e="T700" id="Seg_5441" s="T699">biːrge</ta>
            <ta e="T701" id="Seg_5442" s="T700">olor-u͡ok-put</ta>
            <ta e="T702" id="Seg_5443" s="T701">dʼe</ta>
            <ta e="T703" id="Seg_5444" s="T702">togo</ta>
            <ta e="T704" id="Seg_5445" s="T703">ikki</ta>
            <ta e="T705" id="Seg_5446" s="T704">di͡ek</ta>
            <ta e="T706" id="Seg_5447" s="T705">olor-o-but</ta>
            <ta e="T707" id="Seg_5448" s="T706">bihigi</ta>
            <ta e="T708" id="Seg_5449" s="T707">kör-ö-gün</ta>
            <ta e="T709" id="Seg_5450" s="T708">min</ta>
            <ta e="T710" id="Seg_5451" s="T709">taba-lan-nɨ-m</ta>
            <ta e="T711" id="Seg_5452" s="T710">össü͡ö</ta>
            <ta e="T712" id="Seg_5453" s="T711">en</ta>
            <ta e="T713" id="Seg_5454" s="T712">maːjdʼɨ</ta>
            <ta e="T714" id="Seg_5455" s="T713">minigi-n</ta>
            <ta e="T715" id="Seg_5456" s="T714">atagast-ɨː</ta>
            <ta e="T717" id="Seg_5457" s="T716">tu͡ok</ta>
            <ta e="T718" id="Seg_5458" s="T717">taba-ta</ta>
            <ta e="T719" id="Seg_5459" s="T718">taŋara</ta>
            <ta e="T720" id="Seg_5460" s="T719">egel-bit-e</ta>
            <ta e="T721" id="Seg_5461" s="T720">dʼürü</ta>
            <ta e="T722" id="Seg_5462" s="T721">taba-nɨ</ta>
            <ta e="T723" id="Seg_5463" s="T722">ah-a-t-tɨ-m</ta>
            <ta e="T724" id="Seg_5464" s="T723">dʼe</ta>
            <ta e="T725" id="Seg_5465" s="T724">anɨ</ta>
            <ta e="T726" id="Seg_5466" s="T725">taba-lan-ɨː</ta>
            <ta e="T727" id="Seg_5467" s="T726">biːrge</ta>
            <ta e="T728" id="Seg_5468" s="T727">olor-u͡ok</ta>
            <ta e="T729" id="Seg_5469" s="T728">bihigi</ta>
            <ta e="T730" id="Seg_5470" s="T729">anɨ</ta>
            <ta e="T731" id="Seg_5471" s="T730">biːrge</ta>
            <ta e="T732" id="Seg_5472" s="T731">olor-om-mut</ta>
            <ta e="T733" id="Seg_5473" s="T732">biːr</ta>
            <ta e="T734" id="Seg_5474" s="T733">hübe-nnen</ta>
            <ta e="T735" id="Seg_5475" s="T734">bultan-ɨ͡ak</ta>
            <ta e="T736" id="Seg_5476" s="T735">i</ta>
            <ta e="T737" id="Seg_5477" s="T736">biːr</ta>
            <ta e="T738" id="Seg_5478" s="T737">dʼi͡e-leːk</ta>
            <ta e="T739" id="Seg_5479" s="T738">bu͡ol-u͡ok</ta>
            <ta e="T740" id="Seg_5480" s="T739">en</ta>
            <ta e="T741" id="Seg_5481" s="T740">dʼi͡e-gi-n</ta>
            <ta e="T742" id="Seg_5482" s="T741">köhörd-ö</ta>
            <ta e="T743" id="Seg_5483" s="T742">egel-i͡ek</ta>
            <ta e="T744" id="Seg_5484" s="T743">bu</ta>
            <ta e="T745" id="Seg_5485" s="T744">taba-laːk-pɨn</ta>
            <ta e="T746" id="Seg_5486" s="T745">diː</ta>
            <ta e="T747" id="Seg_5487" s="T746">bu</ta>
            <ta e="T748" id="Seg_5488" s="T747">biːr</ta>
            <ta e="T749" id="Seg_5489" s="T748">taba-bɨt</ta>
            <ta e="T750" id="Seg_5490" s="T749">tart-a-r-an</ta>
            <ta e="T751" id="Seg_5491" s="T750">egel-er</ta>
            <ta e="T752" id="Seg_5492" s="T751">tu͡ok-taːk</ta>
            <ta e="T753" id="Seg_5493" s="T752">bu͡ol-u͡o=j</ta>
            <ta e="T754" id="Seg_5494" s="T753">i͡e</ta>
            <ta e="T755" id="Seg_5495" s="T754">iti</ta>
            <ta e="T756" id="Seg_5496" s="T755">egel-i͡ek</ta>
            <ta e="T757" id="Seg_5497" s="T756">manna</ta>
            <ta e="T758" id="Seg_5498" s="T757">taba-gɨ-n</ta>
            <ta e="T759" id="Seg_5499" s="T758">eː</ta>
            <ta e="T760" id="Seg_5500" s="T759">kim-ŋi-n</ta>
            <ta e="T761" id="Seg_5501" s="T760">manna</ta>
            <ta e="T762" id="Seg_5502" s="T761">biːr</ta>
            <ta e="T763" id="Seg_5503" s="T762">biːrge</ta>
            <ta e="T764" id="Seg_5504" s="T763">olor-u͡ok</ta>
            <ta e="T765" id="Seg_5505" s="T764">bihigi</ta>
            <ta e="T766" id="Seg_5506" s="T765">hübel-e-h-en-ner</ta>
            <ta e="T767" id="Seg_5507" s="T766">dʼe</ta>
            <ta e="T768" id="Seg_5508" s="T767">biːrge</ta>
            <ta e="T769" id="Seg_5509" s="T768">olor-on</ta>
            <ta e="T770" id="Seg_5510" s="T769">kaːl-bɨt-tar</ta>
            <ta e="T771" id="Seg_5511" s="T770">ikk-i͡en-nere</ta>
            <ta e="T772" id="Seg_5512" s="T771">biːr</ta>
            <ta e="T773" id="Seg_5513" s="T772">hübe</ta>
            <ta e="T774" id="Seg_5514" s="T773">bu͡ol-an-nar</ta>
            <ta e="T775" id="Seg_5515" s="T774">nʼima</ta>
            <ta e="T776" id="Seg_5516" s="T775">bu͡ol-an-nar</ta>
            <ta e="T777" id="Seg_5517" s="T776">ikk-i͡en-nere</ta>
            <ta e="T778" id="Seg_5518" s="T777">dʼe</ta>
            <ta e="T779" id="Seg_5519" s="T778">baj-an-tot-on</ta>
            <ta e="T780" id="Seg_5520" s="T779">olor-but-tar</ta>
            <ta e="T781" id="Seg_5521" s="T780">ele-te</ta>
            <ta e="T782" id="Seg_5522" s="T781">iti</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_5523" s="T0">lörü͡ö-nI</ta>
            <ta e="T2" id="Seg_5524" s="T1">kɨtta</ta>
            <ta e="T3" id="Seg_5525" s="T2">Tanʼaktaːk</ta>
            <ta e="T4" id="Seg_5526" s="T3">ogonnʼor</ta>
            <ta e="T5" id="Seg_5527" s="T4">olor-BIT-LAr</ta>
            <ta e="T6" id="Seg_5528" s="T5">ühü</ta>
            <ta e="T7" id="Seg_5529" s="T6">ɨraːk-LIː</ta>
            <ta e="T8" id="Seg_5530" s="T7">olor-Ar-LAr</ta>
            <ta e="T9" id="Seg_5531" s="T8">beje</ta>
            <ta e="T10" id="Seg_5532" s="T9">beje-LArI-n</ta>
            <ta e="T11" id="Seg_5533" s="T10">bil-s-I-s-Ar-LAr</ta>
            <ta e="T12" id="Seg_5534" s="T11">biːrges-tA</ta>
            <ta e="T13" id="Seg_5535" s="T12">ol</ta>
            <ta e="T14" id="Seg_5536" s="T13">dek</ta>
            <ta e="T15" id="Seg_5537" s="T14">dʼi͡e-LAːK</ta>
            <ta e="T16" id="Seg_5538" s="T15">biːrges-tA</ta>
            <ta e="T17" id="Seg_5539" s="T16">bu</ta>
            <ta e="T18" id="Seg_5540" s="T17">dek</ta>
            <ta e="T19" id="Seg_5541" s="T18">dʼi͡e-LAːK</ta>
            <ta e="T20" id="Seg_5542" s="T19">olor-An-LAr</ta>
            <ta e="T21" id="Seg_5543" s="T20">ikki-IAn-LArA</ta>
            <ta e="T22" id="Seg_5544" s="T21">körüs-I-BIT-LAr</ta>
            <ta e="T23" id="Seg_5545" s="T22">biːrge</ta>
            <ta e="T24" id="Seg_5546" s="T23">kepseː-A-s-BIT-LAr</ta>
            <ta e="T25" id="Seg_5547" s="T24">ka</ta>
            <ta e="T26" id="Seg_5548" s="T25">di͡e-Ar</ta>
            <ta e="T27" id="Seg_5549" s="T26">di͡e-A-s-Ar-LAr</ta>
            <ta e="T28" id="Seg_5550" s="T27">lörü͡ö-tI-n</ta>
            <ta e="T29" id="Seg_5551" s="T28">kɨtta</ta>
            <ta e="T30" id="Seg_5552" s="T29">Tanʼaktaːk</ta>
            <ta e="T31" id="Seg_5553" s="T30">ogonnʼor</ta>
            <ta e="T32" id="Seg_5554" s="T31">ka</ta>
            <ta e="T33" id="Seg_5555" s="T32">bihigi</ta>
            <ta e="T36" id="Seg_5556" s="T35">biːr</ta>
            <ta e="T37" id="Seg_5557" s="T36">dʼɨl-nI</ta>
            <ta e="T38" id="Seg_5558" s="T37">bɨs-Ar</ta>
            <ta e="T39" id="Seg_5559" s="T38">as-BItI-n</ta>
            <ta e="T40" id="Seg_5560" s="T39">biːrge</ta>
            <ta e="T41" id="Seg_5561" s="T40">kolboː-An-BIt</ta>
            <ta e="T42" id="Seg_5562" s="T41">ahaː-IAk</ta>
            <ta e="T43" id="Seg_5563" s="T42">bagar-TAK-BItInA</ta>
            <ta e="T44" id="Seg_5564" s="T43">badaga</ta>
            <ta e="T47" id="Seg_5565" s="T46">uhun-LIk</ta>
            <ta e="T48" id="Seg_5566" s="T47">olor-IAK-BIt</ta>
            <ta e="T49" id="Seg_5567" s="T48">badaga</ta>
            <ta e="T50" id="Seg_5568" s="T49">dʼɨl-BItI-n</ta>
            <ta e="T51" id="Seg_5569" s="T50">bɨs-Ar-BItI-GAr</ta>
            <ta e="T52" id="Seg_5570" s="T51">daːganɨ</ta>
            <ta e="T53" id="Seg_5571" s="T52">bu</ta>
            <ta e="T54" id="Seg_5572" s="T53">kuhagan</ta>
            <ta e="T55" id="Seg_5573" s="T54">dʼɨl-LAr</ta>
            <ta e="T56" id="Seg_5574" s="T55">baːr-LAr</ta>
            <ta e="T57" id="Seg_5575" s="T56">dʼe</ta>
            <ta e="T58" id="Seg_5576" s="T57">höp</ta>
            <ta e="T59" id="Seg_5577" s="T58">anɨ</ta>
            <ta e="T60" id="Seg_5578" s="T59">oččogo</ta>
            <ta e="T61" id="Seg_5579" s="T60">bihigi</ta>
            <ta e="T62" id="Seg_5580" s="T61">lörü͡ö</ta>
            <ta e="T63" id="Seg_5581" s="T62">eni͡ene</ta>
            <ta e="T64" id="Seg_5582" s="T783">as-GI-n</ta>
            <ta e="T66" id="Seg_5583" s="T64">ahaː-IAk</ta>
            <ta e="T67" id="Seg_5584" s="T66">eː</ta>
            <ta e="T68" id="Seg_5585" s="T67">Tanʼaktaːk</ta>
            <ta e="T69" id="Seg_5586" s="T68">ogonnʼor</ta>
            <ta e="T70" id="Seg_5587" s="T69">gini</ta>
            <ta e="T71" id="Seg_5588" s="T70">ol</ta>
            <ta e="T72" id="Seg_5589" s="T71">gini͡ene</ta>
            <ta e="T73" id="Seg_5590" s="T72">dʼi͡e-tI-GAr</ta>
            <ta e="T74" id="Seg_5591" s="T73">bar-An-BIt</ta>
            <ta e="T75" id="Seg_5592" s="T74">ahaː-IAk</ta>
            <ta e="T76" id="Seg_5593" s="T75">dʼe</ta>
            <ta e="T77" id="Seg_5594" s="T76">höp</ta>
            <ta e="T78" id="Seg_5595" s="T77">innʼe</ta>
            <ta e="T79" id="Seg_5596" s="T78">ahaː-IAk</ta>
            <ta e="T80" id="Seg_5597" s="T79">oː</ta>
            <ta e="T81" id="Seg_5598" s="T80">dʼe</ta>
            <ta e="T82" id="Seg_5599" s="T81">ahaː-Ar-LAr</ta>
            <ta e="T83" id="Seg_5600" s="T82">agaj</ta>
            <ta e="T84" id="Seg_5601" s="T83">lörü͡ö</ta>
            <ta e="T85" id="Seg_5602" s="T84">dʼi͡e-tI-GAr</ta>
            <ta e="T86" id="Seg_5603" s="T85">ahaː-Ar-LAr</ta>
            <ta e="T87" id="Seg_5604" s="T86">biːr</ta>
            <ta e="T88" id="Seg_5605" s="T87">aŋar-nI</ta>
            <ta e="T89" id="Seg_5606" s="T88">dʼe</ta>
            <ta e="T90" id="Seg_5607" s="T89">baran-IAK-tA-r</ta>
            <ta e="T91" id="Seg_5608" s="T90">di͡eri</ta>
            <ta e="T92" id="Seg_5609" s="T91">dʼe</ta>
            <ta e="T93" id="Seg_5610" s="T92">ahaː-An-LAr</ta>
            <ta e="T94" id="Seg_5611" s="T93">ol-tI-LArI-n</ta>
            <ta e="T95" id="Seg_5612" s="T94">as-LArA</ta>
            <ta e="T96" id="Seg_5613" s="T95">baran-TI-tA</ta>
            <ta e="T97" id="Seg_5614" s="T96">lörü͡ö</ta>
            <ta e="T98" id="Seg_5615" s="T97">gi͡en-tA</ta>
            <ta e="T99" id="Seg_5616" s="T98">dʼe</ta>
            <ta e="T100" id="Seg_5617" s="T99">Tanʼaktaːk</ta>
            <ta e="T101" id="Seg_5618" s="T100">ogonnʼor-GA</ta>
            <ta e="T102" id="Seg_5619" s="T101">bar-IAgIŋ</ta>
            <ta e="T103" id="Seg_5620" s="T102">bar-An-LAr</ta>
            <ta e="T104" id="Seg_5621" s="T103">dʼe</ta>
            <ta e="T105" id="Seg_5622" s="T104">ol</ta>
            <ta e="T106" id="Seg_5623" s="T105">as-tI-n</ta>
            <ta e="T107" id="Seg_5624" s="T106">ahaː-IːhI-LAr</ta>
            <ta e="T108" id="Seg_5625" s="T107">oː</ta>
            <ta e="T109" id="Seg_5626" s="T108">Tanʼaktaːk</ta>
            <ta e="T110" id="Seg_5627" s="T109">ogonnʼor-GA</ta>
            <ta e="T111" id="Seg_5628" s="T110">bar-TI-LAr</ta>
            <ta e="T113" id="Seg_5629" s="T112">dʼe</ta>
            <ta e="T114" id="Seg_5630" s="T113">ahaː-Ar-LAr</ta>
            <ta e="T115" id="Seg_5631" s="T114">ahaː-Ar-LAr-ahaː-Ar-LAr</ta>
            <ta e="T116" id="Seg_5632" s="T115">da</ta>
            <ta e="T117" id="Seg_5633" s="T116">kajaː</ta>
            <ta e="T118" id="Seg_5634" s="T117">biːr</ta>
            <ta e="T119" id="Seg_5635" s="T118">dogor-tA</ta>
            <ta e="T120" id="Seg_5636" s="T119">mini͡ene</ta>
            <ta e="T121" id="Seg_5637" s="T120">as-I-m</ta>
            <ta e="T122" id="Seg_5638" s="T121">baran-Ar</ta>
            <ta e="T123" id="Seg_5639" s="T122">bu͡ol-BAtAK</ta>
            <ta e="T124" id="Seg_5640" s="T784">di͡e-Ar</ta>
            <ta e="T125" id="Seg_5641" s="T124">en</ta>
            <ta e="T126" id="Seg_5642" s="T125">dʼi͡e-GA-r</ta>
            <ta e="T128" id="Seg_5643" s="T126">bar</ta>
            <ta e="T129" id="Seg_5644" s="T128">beje-ŋ</ta>
            <ta e="T130" id="Seg_5645" s="T129">bultaː-A-n-An</ta>
            <ta e="T131" id="Seg_5646" s="T130">min</ta>
            <ta e="T132" id="Seg_5647" s="T131">as-BI-n</ta>
            <ta e="T133" id="Seg_5648" s="T132">barɨ-tI-n</ta>
            <ta e="T134" id="Seg_5649" s="T133">hi͡e-IAK-BI-n</ta>
            <ta e="T135" id="Seg_5650" s="T134">bagar-BAT-BIn</ta>
            <ta e="T136" id="Seg_5651" s="T135">eː</ta>
            <ta e="T137" id="Seg_5652" s="T136">dʼe</ta>
            <ta e="T138" id="Seg_5653" s="T137">bar-An</ta>
            <ta e="T139" id="Seg_5654" s="T138">kaːl-TI-tA</ta>
            <ta e="T140" id="Seg_5655" s="T139">dogor-tA</ta>
            <ta e="T141" id="Seg_5656" s="T140">kajaː</ta>
            <ta e="T142" id="Seg_5657" s="T141">bar-An</ta>
            <ta e="T143" id="Seg_5658" s="T142">tu͡ok-nI</ta>
            <ta e="T144" id="Seg_5659" s="T143">hi͡e-IAK-tA=Ij</ta>
            <ta e="T145" id="Seg_5660" s="T144">dʼi͡e-tI-GAr</ta>
            <ta e="T146" id="Seg_5661" s="T145">lörü͡ö</ta>
            <ta e="T147" id="Seg_5662" s="T146">bar-BIT-tA</ta>
            <ta e="T148" id="Seg_5663" s="T147">dʼi͡e-tI-GAr</ta>
            <ta e="T149" id="Seg_5664" s="T148">tu͡ok-tA</ta>
            <ta e="T150" id="Seg_5665" s="T149">da</ta>
            <ta e="T151" id="Seg_5666" s="T150">hu͡ok</ta>
            <ta e="T152" id="Seg_5667" s="T151">tu͡ok</ta>
            <ta e="T153" id="Seg_5668" s="T152">ere</ta>
            <ta e="T154" id="Seg_5669" s="T153">kaččaga</ta>
            <ta e="T155" id="Seg_5670" s="T154">bɨlčɨŋ-kAːN-nI</ta>
            <ta e="T156" id="Seg_5671" s="T155">bul-I-n-An</ta>
            <ta e="T157" id="Seg_5672" s="T156">ol-tI-tI-n</ta>
            <ta e="T158" id="Seg_5673" s="T157">kü͡östeː-A-n-TI-tA</ta>
            <ta e="T159" id="Seg_5674" s="T158">kü͡östeː-A-n-An</ta>
            <ta e="T160" id="Seg_5675" s="T159">olor-Ar</ta>
            <ta e="T161" id="Seg_5676" s="T160">agaj</ta>
            <ta e="T162" id="Seg_5677" s="T161">ka</ta>
            <ta e="T163" id="Seg_5678" s="T162">onton</ta>
            <ta e="T164" id="Seg_5679" s="T163">kör-BIT-tA</ta>
            <ta e="T165" id="Seg_5680" s="T164">tu͡ok-nI</ta>
            <ta e="T166" id="Seg_5681" s="T165">ere</ta>
            <ta e="T167" id="Seg_5682" s="T166">bultaː-A-n-BIT</ta>
            <ta e="T168" id="Seg_5683" s="T167">emi͡e</ta>
            <ta e="T169" id="Seg_5684" s="T168">lörü͡ö</ta>
            <ta e="T170" id="Seg_5685" s="T169">onton</ta>
            <ta e="T171" id="Seg_5686" s="T170">maːjdiːn</ta>
            <ta e="T172" id="Seg_5687" s="T171">Tanʼaktaːk</ta>
            <ta e="T173" id="Seg_5688" s="T172">ogonnʼor</ta>
            <ta e="T174" id="Seg_5689" s="T173">da</ta>
            <ta e="T175" id="Seg_5690" s="T174">as-tA</ta>
            <ta e="T176" id="Seg_5691" s="T175">baran-BIT</ta>
            <ta e="T177" id="Seg_5692" s="T176">e-TI-tA</ta>
            <ta e="T178" id="Seg_5693" s="T177">hapsi͡em</ta>
            <ta e="T179" id="Seg_5694" s="T178">dʼe</ta>
            <ta e="T180" id="Seg_5695" s="T179">olor-Ar</ta>
            <ta e="T181" id="Seg_5696" s="T180">bil-s-I-BAT</ta>
            <ta e="T182" id="Seg_5697" s="T181">gini-GA</ta>
            <ta e="T183" id="Seg_5698" s="T182">dʼe</ta>
            <ta e="T184" id="Seg_5699" s="T183">kel-BA-TIn</ta>
            <ta e="T185" id="Seg_5700" s="T184">gini</ta>
            <ta e="T186" id="Seg_5701" s="T185">beje-m</ta>
            <ta e="T187" id="Seg_5702" s="T186">da</ta>
            <ta e="T188" id="Seg_5703" s="T187">bult-LAN-An</ta>
            <ta e="T189" id="Seg_5704" s="T188">olor-An</ta>
            <ta e="T190" id="Seg_5705" s="T189">biːr</ta>
            <ta e="T191" id="Seg_5706" s="T190">tu͡ok-kAːN</ta>
            <ta e="T192" id="Seg_5707" s="T191">eme</ta>
            <ta e="T193" id="Seg_5708" s="T192">di͡e-Ar</ta>
            <ta e="T194" id="Seg_5709" s="T193">lörü͡ö</ta>
            <ta e="T195" id="Seg_5710" s="T194">agaj</ta>
            <ta e="T196" id="Seg_5711" s="T195">olor-Ar</ta>
            <ta e="T197" id="Seg_5712" s="T196">bu</ta>
            <ta e="T198" id="Seg_5713" s="T197">olor-TAK-InA</ta>
            <ta e="T199" id="Seg_5714" s="T198">Tanʼaktaːk</ta>
            <ta e="T200" id="Seg_5715" s="T199">ogonnʼor</ta>
            <ta e="T201" id="Seg_5716" s="T200">kel-BIT</ta>
            <ta e="T202" id="Seg_5717" s="T201">is-Ar-tI-n</ta>
            <ta e="T203" id="Seg_5718" s="T202">kör-TI-tA</ta>
            <ta e="T204" id="Seg_5719" s="T203">dogor-tI-n</ta>
            <ta e="T205" id="Seg_5720" s="T204">ɨbɨj</ta>
            <ta e="T206" id="Seg_5721" s="T205">Tanʼaktaːk</ta>
            <ta e="T207" id="Seg_5722" s="T206">ogonnʼor</ta>
            <ta e="T208" id="Seg_5723" s="T207">is-Ar</ta>
            <ta e="T209" id="Seg_5724" s="T208">e-BIT</ta>
            <ta e="T210" id="Seg_5725" s="T209">lörü͡ö</ta>
            <ta e="T211" id="Seg_5726" s="T210">di͡e-Ar</ta>
            <ta e="T212" id="Seg_5727" s="T785">dʼi͡e-tI-n</ta>
            <ta e="T213" id="Seg_5728" s="T212">kataː-A-n-An</ta>
            <ta e="T214" id="Seg_5729" s="T213">hap-An</ta>
            <ta e="T216" id="Seg_5730" s="T214">keːs-BIT</ta>
            <ta e="T217" id="Seg_5731" s="T216">dʼe</ta>
            <ta e="T218" id="Seg_5732" s="T217">kel-Ar</ta>
            <ta e="T219" id="Seg_5733" s="T218">agaj</ta>
            <ta e="T220" id="Seg_5734" s="T219">aː</ta>
            <ta e="T223" id="Seg_5735" s="T222">lörü͡ö-tI-GAr</ta>
            <ta e="T224" id="Seg_5736" s="T223">kel-Ar</ta>
            <ta e="T225" id="Seg_5737" s="T224">lörü͡ö</ta>
            <ta e="T226" id="Seg_5738" s="T225">dogor-LAː</ta>
            <ta e="T227" id="Seg_5739" s="T226">lörü͡ö</ta>
            <ta e="T228" id="Seg_5740" s="T227">dogor-LAː</ta>
            <ta e="T229" id="Seg_5741" s="T228">en</ta>
            <ta e="T230" id="Seg_5742" s="T229">bi͡er</ta>
            <ta e="T231" id="Seg_5743" s="T230">eme</ta>
            <ta e="T232" id="Seg_5744" s="T786">bɨlčɨŋ-kAːN-nA</ta>
            <ta e="T233" id="Seg_5745" s="T232">bɨrak-An</ta>
            <ta e="T234" id="Seg_5746" s="T233">abɨraː</ta>
            <ta e="T235" id="Seg_5747" s="T234">lörü͡ö</ta>
            <ta e="T237" id="Seg_5748" s="T235">dogor-LAː</ta>
            <ta e="T238" id="Seg_5749" s="T237">ardaː-An</ta>
            <ta e="T239" id="Seg_5750" s="T238">kün-BA-r</ta>
            <ta e="T240" id="Seg_5751" s="T239">tur-TI-tA</ta>
            <ta e="T241" id="Seg_5752" s="T240">dʼɨl-BI-n</ta>
            <ta e="T242" id="Seg_5753" s="T241">solgoː</ta>
            <ta e="T243" id="Seg_5754" s="T242">lörü͡ö</ta>
            <ta e="T244" id="Seg_5755" s="T243">dogor-LAː</ta>
            <ta e="T245" id="Seg_5756" s="T244">lörü͡ö</ta>
            <ta e="T246" id="Seg_5757" s="T245">dogor-LAː</ta>
            <ta e="T247" id="Seg_5758" s="T246">öl-TI-m</ta>
            <ta e="T248" id="Seg_5759" s="T247">kaːl-IAK-m</ta>
            <ta e="T249" id="Seg_5760" s="T248">biːr-nI</ta>
            <ta e="T250" id="Seg_5761" s="T249">ɨllaː-A</ta>
            <ta e="T251" id="Seg_5762" s="T250">olor-BIT</ta>
            <ta e="T252" id="Seg_5763" s="T251">törüt</ta>
            <ta e="T253" id="Seg_5764" s="T252">ka</ta>
            <ta e="T254" id="Seg_5765" s="T253">bar-bar</ta>
            <ta e="T255" id="Seg_5766" s="T254">biːr</ta>
            <ta e="T256" id="Seg_5767" s="T255">da</ta>
            <ta e="T257" id="Seg_5768" s="T256">biːr</ta>
            <ta e="T258" id="Seg_5769" s="T257">tu͡ok-nI</ta>
            <ta e="T259" id="Seg_5770" s="T258">büttes-GA</ta>
            <ta e="T260" id="Seg_5771" s="T259">hɨhɨn-I-BIT</ta>
            <ta e="T262" id="Seg_5772" s="T261">kat-BIT</ta>
            <ta e="T263" id="Seg_5773" s="T262">bɨlčɨŋ-kAːN-nI</ta>
            <ta e="T264" id="Seg_5774" s="T263">bɨrak-An</ta>
            <ta e="T265" id="Seg_5775" s="T264">keːs-TI-tA</ta>
            <ta e="T266" id="Seg_5776" s="T265">bar</ta>
            <ta e="T267" id="Seg_5777" s="T266">di͡e-Ar</ta>
            <ta e="T268" id="Seg_5778" s="T267">ol-tI-tA</ta>
            <ta e="T269" id="Seg_5779" s="T268">Tanʼaktaːk</ta>
            <ta e="T270" id="Seg_5780" s="T269">bar-An</ta>
            <ta e="T271" id="Seg_5781" s="T270">kaːl-TI-tA</ta>
            <ta e="T272" id="Seg_5782" s="T271">bar</ta>
            <ta e="T273" id="Seg_5783" s="T272">dʼi͡e-tI-GAr</ta>
            <ta e="T274" id="Seg_5784" s="T273">bar</ta>
            <ta e="T275" id="Seg_5785" s="T274">dʼi͡e-GA-r</ta>
            <ta e="T276" id="Seg_5786" s="T275">di͡e-Ar</ta>
            <ta e="T277" id="Seg_5787" s="T276">bolʼše</ta>
            <ta e="T278" id="Seg_5788" s="T277">bi͡er-BAT-BIn</ta>
            <ta e="T279" id="Seg_5789" s="T278">en-GA</ta>
            <ta e="T280" id="Seg_5790" s="T279">koː</ta>
            <ta e="T281" id="Seg_5791" s="T280">bar-An</ta>
            <ta e="T282" id="Seg_5792" s="T281">kaːl-TI-tA</ta>
            <ta e="T283" id="Seg_5793" s="T282">kihi</ta>
            <ta e="T284" id="Seg_5794" s="T283">bar-An</ta>
            <ta e="T285" id="Seg_5795" s="T284">töhölöːk</ta>
            <ta e="T286" id="Seg_5796" s="T285">hi͡e-IAK-tA=Ij</ta>
            <ta e="T287" id="Seg_5797" s="T286">gini</ta>
            <ta e="T288" id="Seg_5798" s="T287">onton</ta>
            <ta e="T289" id="Seg_5799" s="T288">ol-bu</ta>
            <ta e="T290" id="Seg_5800" s="T289">dʼe</ta>
            <ta e="T291" id="Seg_5801" s="T290">kaːn-tI-n</ta>
            <ta e="T292" id="Seg_5802" s="T291">postu͡oj</ta>
            <ta e="T293" id="Seg_5803" s="T292">postu͡oj</ta>
            <ta e="T294" id="Seg_5804" s="T293">min-tI-n</ta>
            <ta e="T295" id="Seg_5805" s="T294">agaj</ta>
            <ta e="T298" id="Seg_5806" s="T297">kü͡östeː-A-n-An</ta>
            <ta e="T299" id="Seg_5807" s="T298">hi͡e-BIT</ta>
            <ta e="T300" id="Seg_5808" s="T299">da</ta>
            <ta e="T301" id="Seg_5809" s="T300">tu͡ok</ta>
            <ta e="T302" id="Seg_5810" s="T301">da</ta>
            <ta e="T303" id="Seg_5811" s="T302">bul-BAT</ta>
            <ta e="T304" id="Seg_5812" s="T303">da</ta>
            <ta e="T305" id="Seg_5813" s="T304">kanʼaː-BAT</ta>
            <ta e="T306" id="Seg_5814" s="T305">daːganɨ</ta>
            <ta e="T307" id="Seg_5815" s="T306">kas</ta>
            <ta e="T308" id="Seg_5816" s="T307">da</ta>
            <ta e="T309" id="Seg_5817" s="T308">kün-nI</ta>
            <ta e="T310" id="Seg_5818" s="T309">hɨt-BIT-tA</ta>
            <ta e="T311" id="Seg_5819" s="T310">da</ta>
            <ta e="T312" id="Seg_5820" s="T311">agaj</ta>
            <ta e="T313" id="Seg_5821" s="T312">da</ta>
            <ta e="T314" id="Seg_5822" s="T313">kaːm-An</ta>
            <ta e="T315" id="Seg_5823" s="T314">tagɨs-Ar</ta>
            <ta e="T316" id="Seg_5824" s="T315">bu͡ol-BIT</ta>
            <ta e="T317" id="Seg_5825" s="T316">uhuk-I-ttAn</ta>
            <ta e="T318" id="Seg_5826" s="T317">ogut-An</ta>
            <ta e="T319" id="Seg_5827" s="T318">Tanʼaktaːk</ta>
            <ta e="T320" id="Seg_5828" s="T319">ogonnʼor</ta>
            <ta e="T321" id="Seg_5829" s="T320">epeːt</ta>
            <ta e="T322" id="Seg_5830" s="T321">lörü͡ö-tI-GAr</ta>
            <ta e="T323" id="Seg_5831" s="T322">bar-IːhI</ta>
            <ta e="T324" id="Seg_5832" s="T323">kel-An</ta>
            <ta e="T325" id="Seg_5833" s="T324">lörü͡ö-tI-GAr</ta>
            <ta e="T326" id="Seg_5834" s="T325">emi͡e</ta>
            <ta e="T327" id="Seg_5835" s="T326">ɨllaː-BIT</ta>
            <ta e="T328" id="Seg_5836" s="T327">lörü͡ö</ta>
            <ta e="T329" id="Seg_5837" s="T328">dogor-LAː</ta>
            <ta e="T330" id="Seg_5838" s="T329">lörü͡ö</ta>
            <ta e="T331" id="Seg_5839" s="T330">dogor-LAː</ta>
            <ta e="T332" id="Seg_5840" s="T787">öl-TI-m</ta>
            <ta e="T333" id="Seg_5841" s="T332">kaːl-IAK-m</ta>
            <ta e="T334" id="Seg_5842" s="T333">öl-TI-m</ta>
            <ta e="T336" id="Seg_5843" s="T334">kaːl-IAK-m</ta>
            <ta e="T337" id="Seg_5844" s="T336">lörü͡ö</ta>
            <ta e="T338" id="Seg_5845" s="T337">dogor-LAː</ta>
            <ta e="T339" id="Seg_5846" s="T338">biːr</ta>
            <ta e="T340" id="Seg_5847" s="T339">eme</ta>
            <ta e="T342" id="Seg_5848" s="T341">eme</ta>
            <ta e="T343" id="Seg_5849" s="T342">bɨlčɨŋ-kAːN-nA</ta>
            <ta e="T344" id="Seg_5850" s="T343">bɨrak-An</ta>
            <ta e="T345" id="Seg_5851" s="T344">abɨraː</ta>
            <ta e="T346" id="Seg_5852" s="T345">lörü͡ö</ta>
            <ta e="T347" id="Seg_5853" s="T346">dogor-LAː</ta>
            <ta e="T348" id="Seg_5854" s="T347">lörü͡ö</ta>
            <ta e="T349" id="Seg_5855" s="T348">dogor-LAː</ta>
            <ta e="T350" id="Seg_5856" s="T349">ardaː-An</ta>
            <ta e="T351" id="Seg_5857" s="T350">umun-An</ta>
            <ta e="T352" id="Seg_5858" s="T788">bu͡ol-TI-tA</ta>
            <ta e="T353" id="Seg_5859" s="T352">bul-IAK.[tA]</ta>
            <ta e="T354" id="Seg_5860" s="T353">lörü͡ö</ta>
            <ta e="T355" id="Seg_5861" s="T354">dogor-LAː</ta>
            <ta e="T356" id="Seg_5862" s="T355">ardaː-An</ta>
            <ta e="T357" id="Seg_5863" s="T356">bɨlčɨŋ-nA</ta>
            <ta e="T358" id="Seg_5864" s="T357">egel</ta>
            <ta e="T360" id="Seg_5865" s="T358">ɨllaː-A-n-A-BIn</ta>
            <ta e="T361" id="Seg_5866" s="T360">eː</ta>
            <ta e="T362" id="Seg_5867" s="T361">biːr</ta>
            <ta e="T363" id="Seg_5868" s="T362">da</ta>
            <ta e="T364" id="Seg_5869" s="T363">bɨlčɨŋ-tA</ta>
            <ta e="T365" id="Seg_5870" s="T364">hu͡ok-BIn</ta>
            <ta e="T366" id="Seg_5871" s="T365">bar</ta>
            <ta e="T367" id="Seg_5872" s="T366">bar</ta>
            <ta e="T368" id="Seg_5873" s="T367">di͡e-BIT</ta>
            <ta e="T369" id="Seg_5874" s="T368">töttörü</ta>
            <ta e="T370" id="Seg_5875" s="T369">bat-An</ta>
            <ta e="T371" id="Seg_5876" s="T370">ɨːt-BIT</ta>
            <ta e="T372" id="Seg_5877" s="T371">tu͡ok-kAːN-nI</ta>
            <ta e="T373" id="Seg_5878" s="T372">da</ta>
            <ta e="T374" id="Seg_5879" s="T373">bi͡er-BAtAK</ta>
            <ta e="T375" id="Seg_5880" s="T374">kajdi͡ek</ta>
            <ta e="T376" id="Seg_5881" s="T375">bar-IAK.[tA]=Ij</ta>
            <ta e="T377" id="Seg_5882" s="T376">tahaːra</ta>
            <ta e="T378" id="Seg_5883" s="T377">onton</ta>
            <ta e="T379" id="Seg_5884" s="T378">toŋ-Ar</ta>
            <ta e="T380" id="Seg_5885" s="T379">bu͡ol-TI-tA</ta>
            <ta e="T381" id="Seg_5886" s="T380">kajdi͡ek</ta>
            <ta e="T382" id="Seg_5887" s="T381">dʼi͡e-GA</ta>
            <ta e="T383" id="Seg_5888" s="T382">da</ta>
            <ta e="T384" id="Seg_5889" s="T383">kiːr-TAr-BAT</ta>
            <ta e="T385" id="Seg_5890" s="T384">saːt</ta>
            <ta e="T386" id="Seg_5891" s="T385">dʼe</ta>
            <ta e="T387" id="Seg_5892" s="T386">bar-Ar</ta>
            <ta e="T388" id="Seg_5893" s="T387">bu</ta>
            <ta e="T389" id="Seg_5894" s="T388">kihi</ta>
            <ta e="T390" id="Seg_5895" s="T389">bar-BIT-tA</ta>
            <ta e="T391" id="Seg_5896" s="T390">arɨːččɨ</ta>
            <ta e="T392" id="Seg_5897" s="T391">onton</ta>
            <ta e="T393" id="Seg_5898" s="T392">tij-An</ta>
            <ta e="T394" id="Seg_5899" s="T393">is-TAK-InA</ta>
            <ta e="T397" id="Seg_5900" s="T396">dʼi͡e-tI-n</ta>
            <ta e="T398" id="Seg_5901" s="T397">attɨ-GAr</ta>
            <ta e="T399" id="Seg_5902" s="T398">tij-Ar</ta>
            <ta e="T400" id="Seg_5903" s="T399">kör-BIT-tA</ta>
            <ta e="T401" id="Seg_5904" s="T400">tugut-LAːK</ta>
            <ta e="T402" id="Seg_5905" s="T401">tɨhɨ</ta>
            <ta e="T403" id="Seg_5906" s="T402">hɨrɨt-Ar</ta>
            <ta e="T404" id="Seg_5907" s="T403">dʼi͡e-tI-GAr</ta>
            <ta e="T405" id="Seg_5908" s="T404">kaja</ta>
            <ta e="T406" id="Seg_5909" s="T405">kel-Iː-ttAn</ta>
            <ta e="T407" id="Seg_5910" s="T406">kel-BIT-tA</ta>
            <ta e="T408" id="Seg_5911" s="T407">dʼürü</ta>
            <ta e="T409" id="Seg_5912" s="T408">bil-BAT</ta>
            <ta e="T410" id="Seg_5913" s="T409">taba-nI</ta>
            <ta e="T411" id="Seg_5914" s="T410">gini</ta>
            <ta e="T412" id="Seg_5915" s="T789">taba</ta>
            <ta e="T413" id="Seg_5916" s="T412">hɨrɨt-Ar</ta>
            <ta e="T414" id="Seg_5917" s="T413">agaj</ta>
            <ta e="T416" id="Seg_5918" s="T414">kel-BIT</ta>
            <ta e="T417" id="Seg_5919" s="T416">örö</ta>
            <ta e="T418" id="Seg_5920" s="T417">gini</ta>
            <ta e="T419" id="Seg_5921" s="T418">öl-IAK</ta>
            <ta e="T420" id="Seg_5922" s="T419">ɨjaːk-tI-GAr</ta>
            <ta e="T423" id="Seg_5923" s="T422">tilin-IAK</ta>
            <ta e="T424" id="Seg_5924" s="T423">ɨjaːk-tI-GAr</ta>
            <ta e="T425" id="Seg_5925" s="T424">tugut-LAːK</ta>
            <ta e="T426" id="Seg_5926" s="T425">tɨhɨ-tA</ta>
            <ta e="T427" id="Seg_5927" s="T426">beje-tI-GAr</ta>
            <ta e="T428" id="Seg_5928" s="T427">kel-An</ta>
            <ta e="T429" id="Seg_5929" s="T428">halaː-A</ta>
            <ta e="T430" id="Seg_5930" s="T429">hɨrɨt-I-BIT-LAr</ta>
            <ta e="T431" id="Seg_5931" s="T430">gini-nI</ta>
            <ta e="T432" id="Seg_5932" s="T431">iːkteː-BIT-tI-GAr</ta>
            <ta e="T433" id="Seg_5933" s="T432">is-tI-GAr</ta>
            <ta e="T434" id="Seg_5934" s="T433">kel-BIT-tA</ta>
            <ta e="T435" id="Seg_5935" s="T434">da</ta>
            <ta e="T436" id="Seg_5936" s="T435">tugut-kAːN-tI-n</ta>
            <ta e="T437" id="Seg_5937" s="T436">kap-An</ta>
            <ta e="T438" id="Seg_5938" s="T437">ɨl-An</ta>
            <ta e="T439" id="Seg_5939" s="T438">bahak-kAːN-nI</ta>
            <ta e="T440" id="Seg_5940" s="T439">ɨl-TI-tA</ta>
            <ta e="T441" id="Seg_5941" s="T440">da</ta>
            <ta e="T442" id="Seg_5942" s="T441">kort</ta>
            <ta e="T443" id="Seg_5943" s="T442">gɨn-TAr-An</ta>
            <ta e="T444" id="Seg_5944" s="T443">ölör-An</ta>
            <ta e="T445" id="Seg_5945" s="T444">keːs-BIT</ta>
            <ta e="T446" id="Seg_5946" s="T445">inʼe-tA</ta>
            <ta e="T447" id="Seg_5947" s="T446">hɨrɨt-Ar</ta>
            <ta e="T448" id="Seg_5948" s="T447">ka</ta>
            <ta e="T449" id="Seg_5949" s="T448">bu-tI-tI-n</ta>
            <ta e="T450" id="Seg_5950" s="T449">tu͡ok</ta>
            <ta e="T451" id="Seg_5951" s="T450">tu͡ok</ta>
            <ta e="T454" id="Seg_5952" s="T453">hül-I-n-BIT</ta>
            <ta e="T455" id="Seg_5953" s="T454">kihi</ta>
            <ta e="T456" id="Seg_5954" s="T455">hiːkej-LAː-A</ta>
            <ta e="T457" id="Seg_5955" s="T456">bus-A-r-An</ta>
            <ta e="T458" id="Seg_5956" s="T457">kü͡östeː-A-n-An</ta>
            <ta e="T459" id="Seg_5957" s="T458">hi͡e-An</ta>
            <ta e="T460" id="Seg_5958" s="T459">keːs-BIT</ta>
            <ta e="T461" id="Seg_5959" s="T460">dʼe</ta>
            <ta e="T462" id="Seg_5960" s="T461">karak-tA</ta>
            <ta e="T463" id="Seg_5961" s="T462">hɨrdaː-BIT</ta>
            <ta e="T464" id="Seg_5962" s="T463">onno</ta>
            <ta e="T465" id="Seg_5963" s="T464">oː</ta>
            <ta e="T466" id="Seg_5964" s="T465">dʼe</ta>
            <ta e="T467" id="Seg_5965" s="T466">anɨ</ta>
            <ta e="T468" id="Seg_5966" s="T467">bagas</ta>
            <ta e="T469" id="Seg_5967" s="T468">abɨraː-A-n-TI-m</ta>
            <ta e="T470" id="Seg_5968" s="T469">lörü͡ö-GA</ta>
            <ta e="T471" id="Seg_5969" s="T470">bar-BAT-BIn</ta>
            <ta e="T472" id="Seg_5970" s="T471">kanʼaː-BAT-BIn</ta>
            <ta e="T473" id="Seg_5971" s="T472">as-LAN-TI-m</ta>
            <ta e="T474" id="Seg_5972" s="T473">bu͡o</ta>
            <ta e="T475" id="Seg_5973" s="T474">di͡e-Ar</ta>
            <ta e="T476" id="Seg_5974" s="T475">as-LAN-BIT</ta>
            <ta e="T477" id="Seg_5975" s="T476">kihi</ta>
            <ta e="T478" id="Seg_5976" s="T477">kü͡östeː-A-n-An</ta>
            <ta e="T479" id="Seg_5977" s="T478">baran</ta>
            <ta e="T480" id="Seg_5978" s="T479">ahaː-Ar</ta>
            <ta e="T481" id="Seg_5979" s="T480">oːk</ta>
            <ta e="T482" id="Seg_5980" s="T481">dʼe</ta>
            <ta e="T483" id="Seg_5981" s="T482">kas</ta>
            <ta e="T484" id="Seg_5982" s="T483">kün-nI</ta>
            <ta e="T485" id="Seg_5983" s="T484">olor-BIT-tA</ta>
            <ta e="T486" id="Seg_5984" s="T485">dʼürü</ta>
            <ta e="T487" id="Seg_5985" s="T486">taba-LAːK</ta>
            <ta e="T488" id="Seg_5986" s="T487">taba-tI-n</ta>
            <ta e="T489" id="Seg_5987" s="T488">itte</ta>
            <ta e="T490" id="Seg_5988" s="T489">tutun-I-BIT</ta>
            <ta e="T491" id="Seg_5989" s="T490">tɨhɨ-tI-n</ta>
            <ta e="T492" id="Seg_5990" s="T491">tɨhɨ-LAːK</ta>
            <ta e="T493" id="Seg_5991" s="T492">tu͡ok</ta>
            <ta e="T494" id="Seg_5992" s="T493">bu͡ol-IAK.[tA]=Ij</ta>
            <ta e="T495" id="Seg_5993" s="T494">anɨ</ta>
            <ta e="T496" id="Seg_5994" s="T495">oː</ta>
            <ta e="T497" id="Seg_5995" s="T496">bu</ta>
            <ta e="T498" id="Seg_5996" s="T497">hɨt-TAK-InA</ta>
            <ta e="T499" id="Seg_5997" s="T498">lörü͡ö-tI-n</ta>
            <ta e="T500" id="Seg_5998" s="T499">haŋa-tA</ta>
            <ta e="T501" id="Seg_5999" s="T500">kel-TI-tA</ta>
            <ta e="T502" id="Seg_6000" s="T501">dʼe</ta>
            <ta e="T503" id="Seg_6001" s="T502">agaj</ta>
            <ta e="T504" id="Seg_6002" s="T503">kör-BIT-tA</ta>
            <ta e="T505" id="Seg_6003" s="T504">dʼe</ta>
            <ta e="T506" id="Seg_6004" s="T505">min</ta>
            <ta e="T507" id="Seg_6005" s="T506">anɨ</ta>
            <ta e="T508" id="Seg_6006" s="T507">itigirdik</ta>
            <ta e="T509" id="Seg_6007" s="T508">gɨn-IAK-m</ta>
            <ta e="T510" id="Seg_6008" s="T509">beːbe</ta>
            <ta e="T511" id="Seg_6009" s="T510">kel-TIn</ta>
            <ta e="T512" id="Seg_6010" s="T511">agaj</ta>
            <ta e="T513" id="Seg_6011" s="T512">di͡e-BIT</ta>
            <ta e="T514" id="Seg_6012" s="T513">min-GA</ta>
            <ta e="T515" id="Seg_6013" s="T514">as</ta>
            <ta e="T516" id="Seg_6014" s="T515">lörü͡ö</ta>
            <ta e="T517" id="Seg_6015" s="T516">tu͡ok-LAː-An</ta>
            <ta e="T518" id="Seg_6016" s="T517">arɨːččɨ</ta>
            <ta e="T519" id="Seg_6017" s="T518">is-Ar</ta>
            <ta e="T520" id="Seg_6018" s="T519">bu͡opsa</ta>
            <ta e="T521" id="Seg_6019" s="T520">korguj-An</ta>
            <ta e="T522" id="Seg_6020" s="T521">arɨːččɨ</ta>
            <ta e="T523" id="Seg_6021" s="T522">kaːm-An</ta>
            <ta e="T524" id="Seg_6022" s="T523">is-Ar</ta>
            <ta e="T525" id="Seg_6023" s="T524">kel-BIT</ta>
            <ta e="T526" id="Seg_6024" s="T525">da</ta>
            <ta e="T527" id="Seg_6025" s="T526">kel-BIT</ta>
            <ta e="T528" id="Seg_6026" s="T527">kel-Ar-tI-n</ta>
            <ta e="T529" id="Seg_6027" s="T528">kɨtta</ta>
            <ta e="T530" id="Seg_6028" s="T529">dʼi͡e-tA</ta>
            <ta e="T531" id="Seg_6029" s="T530">kataː-An</ta>
            <ta e="T532" id="Seg_6030" s="T531">kataː-A-n-An</ta>
            <ta e="T533" id="Seg_6031" s="T532">keːs-BIT</ta>
            <ta e="T534" id="Seg_6032" s="T533">dʼe</ta>
            <ta e="T535" id="Seg_6033" s="T534">gini-nI</ta>
            <ta e="T536" id="Seg_6034" s="T535">nakaːs-LAː-IAK-m</ta>
            <ta e="T537" id="Seg_6035" s="T536">maːjdiːn</ta>
            <ta e="T538" id="Seg_6036" s="T537">min-n</ta>
            <ta e="T539" id="Seg_6037" s="T538">emi͡e</ta>
            <ta e="T540" id="Seg_6038" s="T539">gini</ta>
            <ta e="T541" id="Seg_6039" s="T540">muŋnaː-BIT-tA</ta>
            <ta e="T542" id="Seg_6040" s="T541">bugurduk</ta>
            <ta e="T543" id="Seg_6041" s="T542">Tanʼaktaːk</ta>
            <ta e="T544" id="Seg_6042" s="T543">ogonnʼor</ta>
            <ta e="T545" id="Seg_6043" s="T544">Tanʼaktaːk</ta>
            <ta e="T546" id="Seg_6044" s="T545">ogonnʼor</ta>
            <ta e="T547" id="Seg_6045" s="T546">biːr</ta>
            <ta e="T548" id="Seg_6046" s="T790">eme</ta>
            <ta e="T549" id="Seg_6047" s="T548">bɨlčɨŋ-kAːN-nA</ta>
            <ta e="T550" id="Seg_6048" s="T549">bi͡er-An</ta>
            <ta e="T551" id="Seg_6049" s="T550">abɨraː</ta>
            <ta e="T552" id="Seg_6050" s="T551">Tanʼaktaːk</ta>
            <ta e="T553" id="Seg_6051" s="T552">ogonnʼor</ta>
            <ta e="T554" id="Seg_6052" s="T553">öl-TI-m</ta>
            <ta e="T555" id="Seg_6053" s="T554">kaːl-TI-m</ta>
            <ta e="T556" id="Seg_6054" s="T555">öl-TI-m</ta>
            <ta e="T560" id="Seg_6055" s="T558">kaːl-TI-m</ta>
            <ta e="T561" id="Seg_6056" s="T560">kün-BA-r</ta>
            <ta e="T562" id="Seg_6057" s="T561">tur-TI-tA</ta>
            <ta e="T563" id="Seg_6058" s="T562">kuŋ</ta>
            <ta e="T564" id="Seg_6059" s="T563">kihi-tA</ta>
            <ta e="T565" id="Seg_6060" s="T564">bu͡ol-TAK-GInA</ta>
            <ta e="T566" id="Seg_6061" s="T565">Tanʼaktaːk</ta>
            <ta e="T567" id="Seg_6062" s="T566">ogonnʼor</ta>
            <ta e="T568" id="Seg_6063" s="T567">Tanʼaktaːk</ta>
            <ta e="T569" id="Seg_6064" s="T568">ogonnʼor</ta>
            <ta e="T570" id="Seg_6065" s="T569">ɨllaː-A-n-A</ta>
            <ta e="T571" id="Seg_6066" s="T570">hɨt-BIT</ta>
            <ta e="T572" id="Seg_6067" s="T571">tu͡ok-nI</ta>
            <ta e="T573" id="Seg_6068" s="T572">da</ta>
            <ta e="T574" id="Seg_6069" s="T573">bi͡er-BAtAK</ta>
            <ta e="T575" id="Seg_6070" s="T574">postu͡oj</ta>
            <ta e="T576" id="Seg_6071" s="T575">tu͡ok</ta>
            <ta e="T577" id="Seg_6072" s="T576">ere</ta>
            <ta e="T578" id="Seg_6073" s="T577">bu</ta>
            <ta e="T579" id="Seg_6074" s="T578">tugut-tI-n</ta>
            <ta e="T580" id="Seg_6075" s="T579">gi͡en-tI-n</ta>
            <ta e="T581" id="Seg_6076" s="T580">tunʼak-tA</ta>
            <ta e="T582" id="Seg_6077" s="T581">tunʼak-LArI-n</ta>
            <ta e="T583" id="Seg_6078" s="T582">tu͡ok-LAː-A-n-BIT</ta>
            <ta e="T584" id="Seg_6079" s="T583">ata-tI-n</ta>
            <ta e="T585" id="Seg_6080" s="T584">kɨtta</ta>
            <ta e="T586" id="Seg_6081" s="T585">tunʼak-tA</ta>
            <ta e="T587" id="Seg_6082" s="T586">du͡o</ta>
            <ta e="T588" id="Seg_6083" s="T587">tu͡ok</ta>
            <ta e="T589" id="Seg_6084" s="T588">du͡o</ta>
            <ta e="T590" id="Seg_6085" s="T589">ol-nI</ta>
            <ta e="T593" id="Seg_6086" s="T592">ata-LAːK</ta>
            <ta e="T594" id="Seg_6087" s="T593">hɨrɨt-An</ta>
            <ta e="T595" id="Seg_6088" s="T594">ol-nI</ta>
            <ta e="T596" id="Seg_6089" s="T595">bɨrak-An</ta>
            <ta e="T597" id="Seg_6090" s="T596">bar</ta>
            <ta e="T598" id="Seg_6091" s="T597">hi͡e</ta>
            <ta e="T599" id="Seg_6092" s="T598">di͡e-A</ta>
            <ta e="T600" id="Seg_6093" s="T599">ikki</ta>
            <ta e="T601" id="Seg_6094" s="T600">tunʼak-nI</ta>
            <ta e="T602" id="Seg_6095" s="T601">bɨrak-An</ta>
            <ta e="T603" id="Seg_6096" s="T602">keːs-BIT</ta>
            <ta e="T604" id="Seg_6097" s="T603">bar</ta>
            <ta e="T605" id="Seg_6098" s="T604">dʼi͡e-GA-r</ta>
            <ta e="T606" id="Seg_6099" s="T605">di͡e-Ar</ta>
            <ta e="T607" id="Seg_6100" s="T606">bolʼše</ta>
            <ta e="T608" id="Seg_6101" s="T607">kel-I-m</ta>
            <ta e="T611" id="Seg_6102" s="T610">maːjdiːn</ta>
            <ta e="T612" id="Seg_6103" s="T611">min-n</ta>
            <ta e="T613" id="Seg_6104" s="T612">emi͡e</ta>
            <ta e="T614" id="Seg_6105" s="T613">dʼe</ta>
            <ta e="T615" id="Seg_6106" s="T614">muŋnaː-BIT-I-ŋ</ta>
            <ta e="T616" id="Seg_6107" s="T615">ol-tI-tA</ta>
            <ta e="T617" id="Seg_6108" s="T616">bɨhɨn-IAK.[tA]</ta>
            <ta e="T618" id="Seg_6109" s="T617">du͡o</ta>
            <ta e="T619" id="Seg_6110" s="T618">bar-An</ta>
            <ta e="T620" id="Seg_6111" s="T619">kaːl-BIT</ta>
            <ta e="T621" id="Seg_6112" s="T620">dʼe</ta>
            <ta e="T622" id="Seg_6113" s="T621">bar-An</ta>
            <ta e="T623" id="Seg_6114" s="T622">kaːl-BIT</ta>
            <ta e="T624" id="Seg_6115" s="T623">dʼi͡e-tI-GAr</ta>
            <ta e="T625" id="Seg_6116" s="T624">emi͡e</ta>
            <ta e="T627" id="Seg_6117" s="T626">bar-BIT</ta>
            <ta e="T628" id="Seg_6118" s="T627">da</ta>
            <ta e="T629" id="Seg_6119" s="T628">harsɨŋŋɨ-tI-n</ta>
            <ta e="T632" id="Seg_6120" s="T631">kas</ta>
            <ta e="T633" id="Seg_6121" s="T632">da</ta>
            <ta e="T634" id="Seg_6122" s="T633">ör</ta>
            <ta e="T635" id="Seg_6123" s="T634">kün-nI</ta>
            <ta e="T636" id="Seg_6124" s="T635">bu͡ol-An</ta>
            <ta e="T637" id="Seg_6125" s="T636">baran</ta>
            <ta e="T638" id="Seg_6126" s="T637">emi͡e</ta>
            <ta e="T639" id="Seg_6127" s="T638">kel-Ar</ta>
            <ta e="T640" id="Seg_6128" s="T639">dʼe</ta>
            <ta e="T641" id="Seg_6129" s="T640">bu͡opsa</ta>
            <ta e="T642" id="Seg_6130" s="T641">öl-Ar-tI-GAr</ta>
            <ta e="T643" id="Seg_6131" s="T642">bar-BIT</ta>
            <ta e="T644" id="Seg_6132" s="T643">bɨhɨːlaːk</ta>
            <ta e="T645" id="Seg_6133" s="T644">dogor-tA</ta>
            <ta e="T646" id="Seg_6134" s="T645">lörü͡ö-tA</ta>
            <ta e="T647" id="Seg_6135" s="T646">ɨksaː-BIT</ta>
            <ta e="T648" id="Seg_6136" s="T647">bat-TAK-InA</ta>
            <ta e="T649" id="Seg_6137" s="T648">da</ta>
            <ta e="T650" id="Seg_6138" s="T649">bar-BAT</ta>
            <ta e="T651" id="Seg_6139" s="T650">kiːr-TAK-InA</ta>
            <ta e="T652" id="Seg_6140" s="T651">kel-BAT</ta>
            <ta e="T653" id="Seg_6141" s="T652">oː</ta>
            <ta e="T654" id="Seg_6142" s="T653">dʼe</ta>
            <ta e="T655" id="Seg_6143" s="T654">kajdak</ta>
            <ta e="T656" id="Seg_6144" s="T655">da</ta>
            <ta e="T657" id="Seg_6145" s="T656">gɨn-IAK-tI-n</ta>
            <ta e="T658" id="Seg_6146" s="T657">bert</ta>
            <ta e="T659" id="Seg_6147" s="T658">dʼe</ta>
            <ta e="T660" id="Seg_6148" s="T659">hürek-tA</ta>
            <ta e="T661" id="Seg_6149" s="T660">ɨ͡arɨː</ta>
            <ta e="T662" id="Seg_6150" s="T661">ɨ͡arɨj-Ar</ta>
            <ta e="T663" id="Seg_6151" s="T662">kihi</ta>
            <ta e="T664" id="Seg_6152" s="T663">dogor-tI-n</ta>
            <ta e="T665" id="Seg_6153" s="T664">ahɨn-TI-tA</ta>
            <ta e="T666" id="Seg_6154" s="T665">diː</ta>
            <ta e="T667" id="Seg_6155" s="T666">ahɨn-An</ta>
            <ta e="T668" id="Seg_6156" s="T667">kajdak</ta>
            <ta e="T669" id="Seg_6157" s="T668">da</ta>
            <ta e="T670" id="Seg_6158" s="T669">gɨn-IAK-tI-n</ta>
            <ta e="T671" id="Seg_6159" s="T670">bert</ta>
            <ta e="T672" id="Seg_6160" s="T671">bat-t-IAK-tI-n</ta>
            <ta e="T673" id="Seg_6161" s="T672">da</ta>
            <ta e="T674" id="Seg_6162" s="T791">bar-An</ta>
            <ta e="T675" id="Seg_6163" s="T674">da</ta>
            <ta e="T676" id="Seg_6164" s="T675">hin</ta>
            <ta e="T677" id="Seg_6165" s="T676">dʼi͡e-tI-GAr</ta>
            <ta e="T678" id="Seg_6166" s="T677">tij-IAK.[tA]</ta>
            <ta e="T680" id="Seg_6167" s="T678">hu͡ok</ta>
            <ta e="T681" id="Seg_6168" s="T792">ajɨlaːgɨn</ta>
            <ta e="T682" id="Seg_6169" s="T681">kiːr</ta>
            <ta e="T683" id="Seg_6170" s="T682">di͡e-BIT</ta>
            <ta e="T684" id="Seg_6171" s="T683">ahaː</ta>
            <ta e="T685" id="Seg_6172" s="T684">kepset-IAk</ta>
            <ta e="T686" id="Seg_6173" s="T685">oččogo</ta>
            <ta e="T688" id="Seg_6174" s="T686">üčügej-LIk</ta>
            <ta e="T689" id="Seg_6175" s="T688">dʼe</ta>
            <ta e="T690" id="Seg_6176" s="T689">ilbi</ta>
            <ta e="T691" id="Seg_6177" s="T690">ile</ta>
            <ta e="T692" id="Seg_6178" s="T691">kepset-BIT-LAr</ta>
            <ta e="T693" id="Seg_6179" s="T692">kaja</ta>
            <ta e="T694" id="Seg_6180" s="T693">onton</ta>
            <ta e="T695" id="Seg_6181" s="T694">bihigi</ta>
            <ta e="T696" id="Seg_6182" s="T695">üčügej-LIk</ta>
            <ta e="T697" id="Seg_6183" s="T696">kepset-IAk</ta>
            <ta e="T698" id="Seg_6184" s="T697">biːrge</ta>
            <ta e="T699" id="Seg_6185" s="T698">anɨ</ta>
            <ta e="T700" id="Seg_6186" s="T699">biːrge</ta>
            <ta e="T701" id="Seg_6187" s="T700">olor-IAK-BIt</ta>
            <ta e="T702" id="Seg_6188" s="T701">dʼe</ta>
            <ta e="T703" id="Seg_6189" s="T702">togo</ta>
            <ta e="T704" id="Seg_6190" s="T703">ikki</ta>
            <ta e="T705" id="Seg_6191" s="T704">dek</ta>
            <ta e="T706" id="Seg_6192" s="T705">olor-A-BIt</ta>
            <ta e="T707" id="Seg_6193" s="T706">bihigi</ta>
            <ta e="T708" id="Seg_6194" s="T707">kör-A-GIn</ta>
            <ta e="T709" id="Seg_6195" s="T708">min</ta>
            <ta e="T710" id="Seg_6196" s="T709">taba-LAN-TI-m</ta>
            <ta e="T711" id="Seg_6197" s="T710">össü͡ö</ta>
            <ta e="T712" id="Seg_6198" s="T711">en</ta>
            <ta e="T713" id="Seg_6199" s="T712">maːjdiːn</ta>
            <ta e="T714" id="Seg_6200" s="T713">min-n</ta>
            <ta e="T715" id="Seg_6201" s="T714">atagastaː-A</ta>
            <ta e="T717" id="Seg_6202" s="T716">tu͡ok</ta>
            <ta e="T718" id="Seg_6203" s="T717">taba-tA</ta>
            <ta e="T719" id="Seg_6204" s="T718">taŋara</ta>
            <ta e="T720" id="Seg_6205" s="T719">egel-BIT-tA</ta>
            <ta e="T721" id="Seg_6206" s="T720">dʼürü</ta>
            <ta e="T722" id="Seg_6207" s="T721">taba-nI</ta>
            <ta e="T723" id="Seg_6208" s="T722">ahaː-A-t-TI-m</ta>
            <ta e="T724" id="Seg_6209" s="T723">dʼe</ta>
            <ta e="T725" id="Seg_6210" s="T724">anɨ</ta>
            <ta e="T726" id="Seg_6211" s="T725">taba-LAN-A</ta>
            <ta e="T727" id="Seg_6212" s="T726">biːrge</ta>
            <ta e="T728" id="Seg_6213" s="T727">olor-IAk</ta>
            <ta e="T729" id="Seg_6214" s="T728">bihigi</ta>
            <ta e="T730" id="Seg_6215" s="T729">anɨ</ta>
            <ta e="T731" id="Seg_6216" s="T730">biːrge</ta>
            <ta e="T732" id="Seg_6217" s="T731">olor-An-BIt</ta>
            <ta e="T733" id="Seg_6218" s="T732">biːr</ta>
            <ta e="T734" id="Seg_6219" s="T733">hübe-nAn</ta>
            <ta e="T735" id="Seg_6220" s="T734">bultan-IAk</ta>
            <ta e="T736" id="Seg_6221" s="T735">i</ta>
            <ta e="T737" id="Seg_6222" s="T736">biːr</ta>
            <ta e="T738" id="Seg_6223" s="T737">dʼi͡e-LAːK</ta>
            <ta e="T739" id="Seg_6224" s="T738">bu͡ol-IAk</ta>
            <ta e="T740" id="Seg_6225" s="T739">en</ta>
            <ta e="T741" id="Seg_6226" s="T740">dʼi͡e-GI-n</ta>
            <ta e="T742" id="Seg_6227" s="T741">köhört-A</ta>
            <ta e="T743" id="Seg_6228" s="T742">egel-IAk</ta>
            <ta e="T744" id="Seg_6229" s="T743">bu</ta>
            <ta e="T745" id="Seg_6230" s="T744">taba-LAːK-BIn</ta>
            <ta e="T746" id="Seg_6231" s="T745">diː</ta>
            <ta e="T747" id="Seg_6232" s="T746">bu</ta>
            <ta e="T748" id="Seg_6233" s="T747">biːr</ta>
            <ta e="T749" id="Seg_6234" s="T748">taba-BIt</ta>
            <ta e="T750" id="Seg_6235" s="T749">tart-A-r-An</ta>
            <ta e="T751" id="Seg_6236" s="T750">egel-Ar</ta>
            <ta e="T752" id="Seg_6237" s="T751">tu͡ok-LAːK</ta>
            <ta e="T753" id="Seg_6238" s="T752">bu͡ol-IAK.[tA]=Ij</ta>
            <ta e="T754" id="Seg_6239" s="T753">eː</ta>
            <ta e="T755" id="Seg_6240" s="T754">iti</ta>
            <ta e="T756" id="Seg_6241" s="T755">egel-IAk</ta>
            <ta e="T757" id="Seg_6242" s="T756">manna</ta>
            <ta e="T758" id="Seg_6243" s="T757">taba-GI-n</ta>
            <ta e="T759" id="Seg_6244" s="T758">eː</ta>
            <ta e="T760" id="Seg_6245" s="T759">kim-GI-n</ta>
            <ta e="T761" id="Seg_6246" s="T760">manna</ta>
            <ta e="T762" id="Seg_6247" s="T761">biːr</ta>
            <ta e="T763" id="Seg_6248" s="T762">biːrge</ta>
            <ta e="T764" id="Seg_6249" s="T763">olor-IAk</ta>
            <ta e="T765" id="Seg_6250" s="T764">bihigi</ta>
            <ta e="T766" id="Seg_6251" s="T765">hübeleː-A-s-An-LAr</ta>
            <ta e="T767" id="Seg_6252" s="T766">dʼe</ta>
            <ta e="T768" id="Seg_6253" s="T767">biːrge</ta>
            <ta e="T769" id="Seg_6254" s="T768">olor-An</ta>
            <ta e="T770" id="Seg_6255" s="T769">kaːl-BIT-LAr</ta>
            <ta e="T771" id="Seg_6256" s="T770">ikki-IAn-LArA</ta>
            <ta e="T772" id="Seg_6257" s="T771">biːr</ta>
            <ta e="T773" id="Seg_6258" s="T772">hübe</ta>
            <ta e="T774" id="Seg_6259" s="T773">bu͡ol-An-LAr</ta>
            <ta e="T775" id="Seg_6260" s="T774">nʼɨma</ta>
            <ta e="T776" id="Seg_6261" s="T775">bu͡ol-An-LAr</ta>
            <ta e="T777" id="Seg_6262" s="T776">ikki-IAn-LArA</ta>
            <ta e="T778" id="Seg_6263" s="T777">dʼe</ta>
            <ta e="T779" id="Seg_6264" s="T778">baːj-An-tot-An</ta>
            <ta e="T780" id="Seg_6265" s="T779">olor-BIT-LAr</ta>
            <ta e="T781" id="Seg_6266" s="T780">ele-tA</ta>
            <ta e="T782" id="Seg_6267" s="T781">iti</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_6268" s="T0">butterfly-ACC</ta>
            <ta e="T2" id="Seg_6269" s="T1">with</ta>
            <ta e="T3" id="Seg_6270" s="T2">with.crutch</ta>
            <ta e="T4" id="Seg_6271" s="T3">old.man.[NOM]</ta>
            <ta e="T5" id="Seg_6272" s="T4">live-PST2-3PL</ta>
            <ta e="T6" id="Seg_6273" s="T5">it.is.said</ta>
            <ta e="T7" id="Seg_6274" s="T6">distant-SIM</ta>
            <ta e="T8" id="Seg_6275" s="T7">live-PRS-3PL</ta>
            <ta e="T9" id="Seg_6276" s="T8">self</ta>
            <ta e="T10" id="Seg_6277" s="T9">self-3PL-ACC</ta>
            <ta e="T11" id="Seg_6278" s="T10">know-RECP/COLL-EP-RECP/COLL-PRS-3PL</ta>
            <ta e="T12" id="Seg_6279" s="T11">one.out.of.two-3SG.[NOM]</ta>
            <ta e="T13" id="Seg_6280" s="T12">that.[NOM]</ta>
            <ta e="T14" id="Seg_6281" s="T13">to</ta>
            <ta e="T15" id="Seg_6282" s="T14">house-PROPR.[NOM]</ta>
            <ta e="T16" id="Seg_6283" s="T15">one.out.of.two-3SG.[NOM]</ta>
            <ta e="T17" id="Seg_6284" s="T16">this.[NOM]</ta>
            <ta e="T18" id="Seg_6285" s="T17">to</ta>
            <ta e="T19" id="Seg_6286" s="T18">house-PROPR.[NOM]</ta>
            <ta e="T20" id="Seg_6287" s="T19">live-CVB.SEQ-3PL</ta>
            <ta e="T21" id="Seg_6288" s="T20">two-COLL-3PL.[NOM]</ta>
            <ta e="T22" id="Seg_6289" s="T21">meet-EP-PST2-3PL</ta>
            <ta e="T23" id="Seg_6290" s="T22">together</ta>
            <ta e="T24" id="Seg_6291" s="T23">tell-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T25" id="Seg_6292" s="T24">well</ta>
            <ta e="T26" id="Seg_6293" s="T25">say-PRS.[3SG]</ta>
            <ta e="T27" id="Seg_6294" s="T26">say-EP-RECP/COLL-PRS-3PL</ta>
            <ta e="T28" id="Seg_6295" s="T27">butterfly-3SG-ACC</ta>
            <ta e="T29" id="Seg_6296" s="T28">with</ta>
            <ta e="T30" id="Seg_6297" s="T29">with.crutch</ta>
            <ta e="T31" id="Seg_6298" s="T30">old.man.[NOM]</ta>
            <ta e="T32" id="Seg_6299" s="T31">well</ta>
            <ta e="T33" id="Seg_6300" s="T32">1PL.[NOM]</ta>
            <ta e="T36" id="Seg_6301" s="T35">one</ta>
            <ta e="T37" id="Seg_6302" s="T36">year-ACC</ta>
            <ta e="T38" id="Seg_6303" s="T37">come.through-PTCP.PRS</ta>
            <ta e="T39" id="Seg_6304" s="T38">food-1PL-ACC</ta>
            <ta e="T40" id="Seg_6305" s="T39">together</ta>
            <ta e="T41" id="Seg_6306" s="T40">connect-CVB.SEQ-1PL</ta>
            <ta e="T42" id="Seg_6307" s="T41">eat-IMP.1DU</ta>
            <ta e="T43" id="Seg_6308" s="T42">want-TEMP-1PL</ta>
            <ta e="T44" id="Seg_6309" s="T43">probably</ta>
            <ta e="T47" id="Seg_6310" s="T46">long-ADVZ</ta>
            <ta e="T48" id="Seg_6311" s="T47">live-FUT-1PL</ta>
            <ta e="T49" id="Seg_6312" s="T48">probably</ta>
            <ta e="T50" id="Seg_6313" s="T49">year-1PL-ACC</ta>
            <ta e="T51" id="Seg_6314" s="T50">cut-PTCP.PRS-1PL-DAT/LOC</ta>
            <ta e="T52" id="Seg_6315" s="T51">EMPH</ta>
            <ta e="T53" id="Seg_6316" s="T52">this</ta>
            <ta e="T54" id="Seg_6317" s="T53">bad</ta>
            <ta e="T55" id="Seg_6318" s="T54">year-PL.[NOM]</ta>
            <ta e="T56" id="Seg_6319" s="T55">there.is-3PL</ta>
            <ta e="T57" id="Seg_6320" s="T56">well</ta>
            <ta e="T58" id="Seg_6321" s="T57">alright</ta>
            <ta e="T59" id="Seg_6322" s="T58">now</ta>
            <ta e="T60" id="Seg_6323" s="T59">then</ta>
            <ta e="T61" id="Seg_6324" s="T60">1PL.[NOM]</ta>
            <ta e="T62" id="Seg_6325" s="T61">butterfly.[NOM]</ta>
            <ta e="T63" id="Seg_6326" s="T62">your</ta>
            <ta e="T64" id="Seg_6327" s="T783">food-2SG-ACC</ta>
            <ta e="T66" id="Seg_6328" s="T64">eat-IMP.1DU</ta>
            <ta e="T67" id="Seg_6329" s="T66">AFFIRM</ta>
            <ta e="T68" id="Seg_6330" s="T67">with.crutch</ta>
            <ta e="T69" id="Seg_6331" s="T68">old.man.[NOM]</ta>
            <ta e="T70" id="Seg_6332" s="T69">3SG.[NOM]</ta>
            <ta e="T71" id="Seg_6333" s="T70">that</ta>
            <ta e="T72" id="Seg_6334" s="T71">his</ta>
            <ta e="T73" id="Seg_6335" s="T72">house-3SG-DAT/LOC</ta>
            <ta e="T74" id="Seg_6336" s="T73">go-CVB.SEQ-1PL</ta>
            <ta e="T75" id="Seg_6337" s="T74">eat-IMP.1DU</ta>
            <ta e="T76" id="Seg_6338" s="T75">well</ta>
            <ta e="T77" id="Seg_6339" s="T76">alright</ta>
            <ta e="T78" id="Seg_6340" s="T77">so</ta>
            <ta e="T79" id="Seg_6341" s="T78">eat-IMP.1DU</ta>
            <ta e="T80" id="Seg_6342" s="T79">oh</ta>
            <ta e="T81" id="Seg_6343" s="T80">well</ta>
            <ta e="T82" id="Seg_6344" s="T81">eat-PRS-3PL</ta>
            <ta e="T83" id="Seg_6345" s="T82">only</ta>
            <ta e="T84" id="Seg_6346" s="T83">butterfly.[NOM]</ta>
            <ta e="T85" id="Seg_6347" s="T84">house-3SG-DAT/LOC</ta>
            <ta e="T86" id="Seg_6348" s="T85">eat-PRS-3PL</ta>
            <ta e="T87" id="Seg_6349" s="T86">one</ta>
            <ta e="T88" id="Seg_6350" s="T87">half-ACC</ta>
            <ta e="T89" id="Seg_6351" s="T88">well</ta>
            <ta e="T90" id="Seg_6352" s="T89">run.out-PTCP.FUT-3SG-DAT/LOC</ta>
            <ta e="T91" id="Seg_6353" s="T90">until</ta>
            <ta e="T92" id="Seg_6354" s="T91">well</ta>
            <ta e="T93" id="Seg_6355" s="T92">eat-CVB.SEQ-3PL</ta>
            <ta e="T94" id="Seg_6356" s="T93">that-3SG-3PL-GEN</ta>
            <ta e="T95" id="Seg_6357" s="T94">food-3PL.[NOM]</ta>
            <ta e="T96" id="Seg_6358" s="T95">run.out-PST1-3SG</ta>
            <ta e="T97" id="Seg_6359" s="T96">butterfly.[NOM]</ta>
            <ta e="T98" id="Seg_6360" s="T97">own-3SG.[NOM]</ta>
            <ta e="T99" id="Seg_6361" s="T98">well</ta>
            <ta e="T100" id="Seg_6362" s="T99">with.crutch</ta>
            <ta e="T101" id="Seg_6363" s="T100">old.man-DAT/LOC</ta>
            <ta e="T102" id="Seg_6364" s="T101">go-IMP.1PL</ta>
            <ta e="T103" id="Seg_6365" s="T102">go-CVB.SEQ-3PL</ta>
            <ta e="T104" id="Seg_6366" s="T103">well</ta>
            <ta e="T105" id="Seg_6367" s="T104">that</ta>
            <ta e="T106" id="Seg_6368" s="T105">food-3SG-ACC</ta>
            <ta e="T107" id="Seg_6369" s="T106">eat-CAP-3PL</ta>
            <ta e="T108" id="Seg_6370" s="T107">oh</ta>
            <ta e="T109" id="Seg_6371" s="T108">with.crutch</ta>
            <ta e="T110" id="Seg_6372" s="T109">old.man-DAT/LOC</ta>
            <ta e="T111" id="Seg_6373" s="T110">go-PST1-3PL</ta>
            <ta e="T113" id="Seg_6374" s="T112">well</ta>
            <ta e="T114" id="Seg_6375" s="T113">eat-PRS-3PL</ta>
            <ta e="T115" id="Seg_6376" s="T114">eat-PRS-3PL-eat-PRS-3PL</ta>
            <ta e="T116" id="Seg_6377" s="T115">EMPH</ta>
            <ta e="T117" id="Seg_6378" s="T116">hey</ta>
            <ta e="T118" id="Seg_6379" s="T117">one</ta>
            <ta e="T119" id="Seg_6380" s="T118">friend-3SG.[NOM]</ta>
            <ta e="T120" id="Seg_6381" s="T119">my</ta>
            <ta e="T121" id="Seg_6382" s="T120">food-EP-1SG.[NOM]</ta>
            <ta e="T122" id="Seg_6383" s="T121">run.out-PTCP.PRS</ta>
            <ta e="T123" id="Seg_6384" s="T122">be-PST2.NEG.[3SG]</ta>
            <ta e="T124" id="Seg_6385" s="T784">say-PRS.[3SG]</ta>
            <ta e="T125" id="Seg_6386" s="T124">2SG.[NOM]</ta>
            <ta e="T126" id="Seg_6387" s="T125">house-2SG-DAT/LOC</ta>
            <ta e="T128" id="Seg_6388" s="T126">go.[IMP.2SG]</ta>
            <ta e="T129" id="Seg_6389" s="T128">self-2SG.[NOM]</ta>
            <ta e="T130" id="Seg_6390" s="T129">hunt-EP-MED-CVB.SEQ</ta>
            <ta e="T131" id="Seg_6391" s="T130">1SG.[NOM]</ta>
            <ta e="T132" id="Seg_6392" s="T131">food-1SG-ACC</ta>
            <ta e="T133" id="Seg_6393" s="T132">whole-3SG-ACC</ta>
            <ta e="T134" id="Seg_6394" s="T133">eat-PTCP.FUT-1SG-ACC</ta>
            <ta e="T135" id="Seg_6395" s="T134">want-NEG-1SG</ta>
            <ta e="T136" id="Seg_6396" s="T135">EMPH</ta>
            <ta e="T137" id="Seg_6397" s="T136">well</ta>
            <ta e="T138" id="Seg_6398" s="T137">go-CVB.SEQ</ta>
            <ta e="T139" id="Seg_6399" s="T138">stay-PST1-3SG</ta>
            <ta e="T140" id="Seg_6400" s="T139">friend-3SG.[NOM]</ta>
            <ta e="T141" id="Seg_6401" s="T140">hey</ta>
            <ta e="T142" id="Seg_6402" s="T141">go-CVB.SEQ</ta>
            <ta e="T143" id="Seg_6403" s="T142">what-ACC</ta>
            <ta e="T144" id="Seg_6404" s="T143">eat-FUT-3SG-Q</ta>
            <ta e="T145" id="Seg_6405" s="T144">house-3SG-DAT/LOC</ta>
            <ta e="T146" id="Seg_6406" s="T145">butterfly.[NOM]</ta>
            <ta e="T147" id="Seg_6407" s="T146">go-PST2-3SG</ta>
            <ta e="T148" id="Seg_6408" s="T147">house-3SG-DAT/LOC</ta>
            <ta e="T149" id="Seg_6409" s="T148">what-3SG.[NOM]</ta>
            <ta e="T150" id="Seg_6410" s="T149">NEG</ta>
            <ta e="T151" id="Seg_6411" s="T150">NEG.EX</ta>
            <ta e="T152" id="Seg_6412" s="T151">what.[NOM]</ta>
            <ta e="T153" id="Seg_6413" s="T152">INDEF</ta>
            <ta e="T154" id="Seg_6414" s="T153">when</ta>
            <ta e="T155" id="Seg_6415" s="T154">sinewy.meat-DIM-ACC</ta>
            <ta e="T156" id="Seg_6416" s="T155">find-EP-MED-CVB.SEQ</ta>
            <ta e="T157" id="Seg_6417" s="T156">that-3SG-3SG-ACC</ta>
            <ta e="T158" id="Seg_6418" s="T157">cook-EP-MED-PST1-3SG</ta>
            <ta e="T159" id="Seg_6419" s="T158">cook-EP-MED-CVB.SEQ</ta>
            <ta e="T160" id="Seg_6420" s="T159">sit-PRS.[3SG]</ta>
            <ta e="T161" id="Seg_6421" s="T160">only</ta>
            <ta e="T162" id="Seg_6422" s="T161">well</ta>
            <ta e="T163" id="Seg_6423" s="T162">then</ta>
            <ta e="T164" id="Seg_6424" s="T163">see-PST2-3SG</ta>
            <ta e="T165" id="Seg_6425" s="T164">what-ACC</ta>
            <ta e="T166" id="Seg_6426" s="T165">INDEF</ta>
            <ta e="T167" id="Seg_6427" s="T166">hunt-EP-MED-PST2.[3SG]</ta>
            <ta e="T168" id="Seg_6428" s="T167">again</ta>
            <ta e="T169" id="Seg_6429" s="T168">butterfly.[NOM]</ta>
            <ta e="T170" id="Seg_6430" s="T169">then</ta>
            <ta e="T171" id="Seg_6431" s="T170">recent.[NOM]</ta>
            <ta e="T172" id="Seg_6432" s="T171">with.crutch</ta>
            <ta e="T173" id="Seg_6433" s="T172">old.man.[NOM]</ta>
            <ta e="T174" id="Seg_6434" s="T173">and</ta>
            <ta e="T175" id="Seg_6435" s="T174">food-3SG.[NOM]</ta>
            <ta e="T176" id="Seg_6436" s="T175">run.out-PTCP.PST</ta>
            <ta e="T177" id="Seg_6437" s="T176">be-PST1-3SG</ta>
            <ta e="T178" id="Seg_6438" s="T177">at.all</ta>
            <ta e="T179" id="Seg_6439" s="T178">well</ta>
            <ta e="T180" id="Seg_6440" s="T179">sit-PRS.[3SG]</ta>
            <ta e="T181" id="Seg_6441" s="T180">know-RECP/COLL-EP-NEG.[3SG]</ta>
            <ta e="T182" id="Seg_6442" s="T181">3SG-DAT/LOC</ta>
            <ta e="T183" id="Seg_6443" s="T182">well</ta>
            <ta e="T184" id="Seg_6444" s="T183">come-NEG-IMP.3SG</ta>
            <ta e="T185" id="Seg_6445" s="T184">3SG.[NOM]</ta>
            <ta e="T186" id="Seg_6446" s="T185">self-1SG.[NOM]</ta>
            <ta e="T187" id="Seg_6447" s="T186">EMPH</ta>
            <ta e="T188" id="Seg_6448" s="T187">hunt-VBZ-CVB.SEQ</ta>
            <ta e="T189" id="Seg_6449" s="T188">live-CVB.SEQ</ta>
            <ta e="T190" id="Seg_6450" s="T189">one</ta>
            <ta e="T191" id="Seg_6451" s="T190">what-INTNS.[NOM]</ta>
            <ta e="T192" id="Seg_6452" s="T191">INDEF</ta>
            <ta e="T193" id="Seg_6453" s="T192">think-PRS.[3SG]</ta>
            <ta e="T194" id="Seg_6454" s="T193">butterfly.[NOM]</ta>
            <ta e="T195" id="Seg_6455" s="T194">only</ta>
            <ta e="T196" id="Seg_6456" s="T195">live-PRS.[3SG]</ta>
            <ta e="T197" id="Seg_6457" s="T196">this</ta>
            <ta e="T198" id="Seg_6458" s="T197">live-TEMP-3SG</ta>
            <ta e="T199" id="Seg_6459" s="T198">with.crutch</ta>
            <ta e="T200" id="Seg_6460" s="T199">old.man.[NOM]</ta>
            <ta e="T201" id="Seg_6461" s="T200">come-PST2.[3SG]</ta>
            <ta e="T202" id="Seg_6462" s="T201">go-PTCP.PRS-3SG-ACC</ta>
            <ta e="T203" id="Seg_6463" s="T202">see-PST1-3SG</ta>
            <ta e="T204" id="Seg_6464" s="T203">friend-3SG-ACC</ta>
            <ta e="T205" id="Seg_6465" s="T204">oh.dear</ta>
            <ta e="T206" id="Seg_6466" s="T205">with.crutch</ta>
            <ta e="T207" id="Seg_6467" s="T206">old.man.[NOM]</ta>
            <ta e="T208" id="Seg_6468" s="T207">go-PTCP.PRS</ta>
            <ta e="T209" id="Seg_6469" s="T208">be-PST2.[3SG]</ta>
            <ta e="T210" id="Seg_6470" s="T209">butterfly.[NOM]</ta>
            <ta e="T211" id="Seg_6471" s="T210">think-PRS.[3SG]</ta>
            <ta e="T212" id="Seg_6472" s="T785">house-3SG-ACC</ta>
            <ta e="T213" id="Seg_6473" s="T212">lock-EP-MED-CVB.SEQ</ta>
            <ta e="T214" id="Seg_6474" s="T213">close-CVB.SEQ</ta>
            <ta e="T216" id="Seg_6475" s="T214">throw-PST2.[3SG]</ta>
            <ta e="T217" id="Seg_6476" s="T216">well</ta>
            <ta e="T218" id="Seg_6477" s="T217">come-PRS.[3SG]</ta>
            <ta e="T219" id="Seg_6478" s="T218">only</ta>
            <ta e="T220" id="Seg_6479" s="T219">ah</ta>
            <ta e="T223" id="Seg_6480" s="T222">butterfly-3SG-DAT/LOC</ta>
            <ta e="T224" id="Seg_6481" s="T223">come-PRS.[3SG]</ta>
            <ta e="T225" id="Seg_6482" s="T224">butterfly.[NOM]</ta>
            <ta e="T226" id="Seg_6483" s="T225">friend-VBZ.[IMP.2SG]</ta>
            <ta e="T227" id="Seg_6484" s="T226">butterfly.[NOM]</ta>
            <ta e="T228" id="Seg_6485" s="T227">friend-VBZ.[IMP.2SG]</ta>
            <ta e="T229" id="Seg_6486" s="T228">2SG.[NOM]</ta>
            <ta e="T230" id="Seg_6487" s="T229">give.[IMP.2SG]</ta>
            <ta e="T231" id="Seg_6488" s="T230">INDEF</ta>
            <ta e="T232" id="Seg_6489" s="T786">sinewy.meat-DIM-PART</ta>
            <ta e="T233" id="Seg_6490" s="T232">throw-CVB.SEQ</ta>
            <ta e="T234" id="Seg_6491" s="T233">come.to.aid.[IMP.2SG]</ta>
            <ta e="T235" id="Seg_6492" s="T234">butterfly.[NOM]</ta>
            <ta e="T237" id="Seg_6493" s="T235">friend-VBZ.[IMP.2SG]</ta>
            <ta e="T238" id="Seg_6494" s="T237">%%-CVB.SEQ</ta>
            <ta e="T239" id="Seg_6495" s="T238">day-1SG-DAT/LOC</ta>
            <ta e="T240" id="Seg_6496" s="T239">stand-PST1-3SG</ta>
            <ta e="T241" id="Seg_6497" s="T240">year-1SG-ACC</ta>
            <ta e="T242" id="Seg_6498" s="T241">%%</ta>
            <ta e="T243" id="Seg_6499" s="T242">butterfly.[NOM]</ta>
            <ta e="T244" id="Seg_6500" s="T243">friend-VBZ.[IMP.2SG]</ta>
            <ta e="T245" id="Seg_6501" s="T244">butterfly.[NOM]</ta>
            <ta e="T246" id="Seg_6502" s="T245">friend-VBZ.[IMP.2SG]</ta>
            <ta e="T247" id="Seg_6503" s="T246">die-PST1-1SG</ta>
            <ta e="T248" id="Seg_6504" s="T247">stay-FUT-1SG</ta>
            <ta e="T249" id="Seg_6505" s="T248">one-ACC</ta>
            <ta e="T250" id="Seg_6506" s="T249">sing-CVB.SIM</ta>
            <ta e="T251" id="Seg_6507" s="T250">sit-PST2.[3SG]</ta>
            <ta e="T252" id="Seg_6508" s="T251">at.all</ta>
            <ta e="T253" id="Seg_6509" s="T252">well</ta>
            <ta e="T254" id="Seg_6510" s="T253">go.[IMP.2SG]-go.[IMP.2SG]</ta>
            <ta e="T255" id="Seg_6511" s="T254">one</ta>
            <ta e="T256" id="Seg_6512" s="T255">INDEF</ta>
            <ta e="T257" id="Seg_6513" s="T256">one</ta>
            <ta e="T258" id="Seg_6514" s="T257">what-ACC</ta>
            <ta e="T259" id="Seg_6515" s="T258">%%-DAT/LOC</ta>
            <ta e="T260" id="Seg_6516" s="T259">stick-EP-PTCP.PST</ta>
            <ta e="T262" id="Seg_6517" s="T261">get.dry-PTCP.PST</ta>
            <ta e="T263" id="Seg_6518" s="T262">sinewy.meat-DIM-ACC</ta>
            <ta e="T264" id="Seg_6519" s="T263">throw-CVB.SEQ</ta>
            <ta e="T265" id="Seg_6520" s="T264">throw-PST1-3SG</ta>
            <ta e="T266" id="Seg_6521" s="T265">go.[IMP.2SG]</ta>
            <ta e="T267" id="Seg_6522" s="T266">say-PRS.[3SG]</ta>
            <ta e="T268" id="Seg_6523" s="T267">that-3SG-3SG.[NOM]</ta>
            <ta e="T269" id="Seg_6524" s="T268">with.crutch</ta>
            <ta e="T270" id="Seg_6525" s="T269">go-CVB.SEQ</ta>
            <ta e="T271" id="Seg_6526" s="T270">stay-PST1-3SG</ta>
            <ta e="T272" id="Seg_6527" s="T271">go.[IMP.2SG]</ta>
            <ta e="T273" id="Seg_6528" s="T272">house-3SG-DAT/LOC</ta>
            <ta e="T274" id="Seg_6529" s="T273">go.[IMP.2SG]</ta>
            <ta e="T275" id="Seg_6530" s="T274">house-2SG-DAT/LOC</ta>
            <ta e="T276" id="Seg_6531" s="T275">say-PRS.[3SG]</ta>
            <ta e="T277" id="Seg_6532" s="T276">more</ta>
            <ta e="T278" id="Seg_6533" s="T277">give-NEG-1SG</ta>
            <ta e="T279" id="Seg_6534" s="T278">2SG-DAT/LOC</ta>
            <ta e="T280" id="Seg_6535" s="T279">EMPH</ta>
            <ta e="T281" id="Seg_6536" s="T280">go-CVB.SEQ</ta>
            <ta e="T282" id="Seg_6537" s="T281">stay-PST1-3SG</ta>
            <ta e="T283" id="Seg_6538" s="T282">human.being.[NOM]</ta>
            <ta e="T284" id="Seg_6539" s="T283">go-CVB.SEQ</ta>
            <ta e="T285" id="Seg_6540" s="T284">so.much.[NOM]</ta>
            <ta e="T286" id="Seg_6541" s="T285">eat-FUT-3SG-Q</ta>
            <ta e="T287" id="Seg_6542" s="T286">3SG.[NOM]</ta>
            <ta e="T288" id="Seg_6543" s="T287">then</ta>
            <ta e="T289" id="Seg_6544" s="T288">that-this</ta>
            <ta e="T290" id="Seg_6545" s="T289">well</ta>
            <ta e="T291" id="Seg_6546" s="T290">blood-3SG-ACC</ta>
            <ta e="T292" id="Seg_6547" s="T291">empty.[NOM]</ta>
            <ta e="T293" id="Seg_6548" s="T292">empty</ta>
            <ta e="T294" id="Seg_6549" s="T293">soup-3SG-ACC</ta>
            <ta e="T295" id="Seg_6550" s="T294">only</ta>
            <ta e="T298" id="Seg_6551" s="T297">cook-EP-MED-CVB.SEQ</ta>
            <ta e="T299" id="Seg_6552" s="T298">eat-PST2.[3SG]</ta>
            <ta e="T300" id="Seg_6553" s="T299">EMPH</ta>
            <ta e="T301" id="Seg_6554" s="T300">what.[NOM]</ta>
            <ta e="T302" id="Seg_6555" s="T301">NEG</ta>
            <ta e="T303" id="Seg_6556" s="T302">find-NEG.[3SG]</ta>
            <ta e="T304" id="Seg_6557" s="T303">and</ta>
            <ta e="T305" id="Seg_6558" s="T304">and.so.on-NEG.[3SG]</ta>
            <ta e="T306" id="Seg_6559" s="T305">EMPH</ta>
            <ta e="T307" id="Seg_6560" s="T306">how.much</ta>
            <ta e="T308" id="Seg_6561" s="T307">INDEF</ta>
            <ta e="T309" id="Seg_6562" s="T308">day-ACC</ta>
            <ta e="T310" id="Seg_6563" s="T309">lie-PST2-3SG</ta>
            <ta e="T311" id="Seg_6564" s="T310">EMPH</ta>
            <ta e="T312" id="Seg_6565" s="T311">only</ta>
            <ta e="T313" id="Seg_6566" s="T312">EMPH</ta>
            <ta e="T314" id="Seg_6567" s="T313">walk-CVB.SEQ</ta>
            <ta e="T315" id="Seg_6568" s="T314">go.out-PTCP.PRS</ta>
            <ta e="T316" id="Seg_6569" s="T315">become-PST2.[3SG]</ta>
            <ta e="T317" id="Seg_6570" s="T316">end-EP-ABL</ta>
            <ta e="T318" id="Seg_6571" s="T317">hunger-CVB.SEQ</ta>
            <ta e="T319" id="Seg_6572" s="T318">with.crutch</ta>
            <ta e="T320" id="Seg_6573" s="T319">old.man.[NOM]</ta>
            <ta e="T321" id="Seg_6574" s="T320">again</ta>
            <ta e="T322" id="Seg_6575" s="T321">butterfly-3SG-DAT/LOC</ta>
            <ta e="T323" id="Seg_6576" s="T322">go-CAP.[3SG]</ta>
            <ta e="T324" id="Seg_6577" s="T323">come-CVB.SEQ</ta>
            <ta e="T325" id="Seg_6578" s="T324">butterfly-3SG-DAT/LOC</ta>
            <ta e="T326" id="Seg_6579" s="T325">again</ta>
            <ta e="T327" id="Seg_6580" s="T326">sing-PST2.[3SG]</ta>
            <ta e="T328" id="Seg_6581" s="T327">butterfly.[NOM]</ta>
            <ta e="T329" id="Seg_6582" s="T328">friend-VBZ.[IMP.2SG]</ta>
            <ta e="T330" id="Seg_6583" s="T329">butterfly.[NOM]</ta>
            <ta e="T331" id="Seg_6584" s="T330">friend-VBZ.[IMP.2SG]</ta>
            <ta e="T332" id="Seg_6585" s="T787">die-PST1-1SG</ta>
            <ta e="T333" id="Seg_6586" s="T332">stay-FUT-1SG</ta>
            <ta e="T334" id="Seg_6587" s="T333">die-PST1-1SG</ta>
            <ta e="T336" id="Seg_6588" s="T334">stay-FUT-1SG</ta>
            <ta e="T337" id="Seg_6589" s="T336">butterfly.[NOM]</ta>
            <ta e="T338" id="Seg_6590" s="T337">friend-VBZ.[IMP.2SG]</ta>
            <ta e="T339" id="Seg_6591" s="T338">one</ta>
            <ta e="T340" id="Seg_6592" s="T339">INDEF</ta>
            <ta e="T342" id="Seg_6593" s="T341">INDEF</ta>
            <ta e="T343" id="Seg_6594" s="T342">sinewy.meat-DIM-PART</ta>
            <ta e="T344" id="Seg_6595" s="T343">throw-CVB.SEQ</ta>
            <ta e="T345" id="Seg_6596" s="T344">come.to.aid.[IMP.2SG]</ta>
            <ta e="T346" id="Seg_6597" s="T345">butterfly.[NOM]</ta>
            <ta e="T347" id="Seg_6598" s="T346">friend-VBZ.[IMP.2SG]</ta>
            <ta e="T348" id="Seg_6599" s="T347">butterfly.[NOM]</ta>
            <ta e="T349" id="Seg_6600" s="T348">friend-VBZ.[IMP.2SG]</ta>
            <ta e="T350" id="Seg_6601" s="T349">%%-CVB.SEQ</ta>
            <ta e="T351" id="Seg_6602" s="T350">forget-CVB.SEQ</ta>
            <ta e="T352" id="Seg_6603" s="T788">be-PST1-3SG</ta>
            <ta e="T353" id="Seg_6604" s="T352">find-FUT.[3SG]</ta>
            <ta e="T354" id="Seg_6605" s="T353">butterfly.[NOM]</ta>
            <ta e="T355" id="Seg_6606" s="T354">friend-VBZ.[IMP.2SG]</ta>
            <ta e="T356" id="Seg_6607" s="T355">%%-CVB.SEQ</ta>
            <ta e="T357" id="Seg_6608" s="T356">sinewy.meat-PART</ta>
            <ta e="T358" id="Seg_6609" s="T357">get.[IMP.2SG]</ta>
            <ta e="T360" id="Seg_6610" s="T358">sing-EP-MED-PRS-1SG</ta>
            <ta e="T361" id="Seg_6611" s="T360">EMPH</ta>
            <ta e="T362" id="Seg_6612" s="T361">one</ta>
            <ta e="T363" id="Seg_6613" s="T362">NEG</ta>
            <ta e="T364" id="Seg_6614" s="T363">sinewy.meat-POSS</ta>
            <ta e="T365" id="Seg_6615" s="T364">NEG-1SG</ta>
            <ta e="T366" id="Seg_6616" s="T365">go.[IMP.2SG]</ta>
            <ta e="T367" id="Seg_6617" s="T366">go.[IMP.2SG]</ta>
            <ta e="T368" id="Seg_6618" s="T367">say-PST2.[3SG]</ta>
            <ta e="T369" id="Seg_6619" s="T368">back</ta>
            <ta e="T370" id="Seg_6620" s="T369">chase-CVB.SEQ</ta>
            <ta e="T371" id="Seg_6621" s="T370">send-PST2.[3SG]</ta>
            <ta e="T372" id="Seg_6622" s="T371">what-INTNS-ACC</ta>
            <ta e="T373" id="Seg_6623" s="T372">NEG</ta>
            <ta e="T374" id="Seg_6624" s="T373">give-PST2.NEG.[3SG]</ta>
            <ta e="T375" id="Seg_6625" s="T374">whereto</ta>
            <ta e="T376" id="Seg_6626" s="T375">go-FUT.[3SG]-Q</ta>
            <ta e="T377" id="Seg_6627" s="T376">outside</ta>
            <ta e="T378" id="Seg_6628" s="T377">then</ta>
            <ta e="T379" id="Seg_6629" s="T378">freeze-PTCP.PRS</ta>
            <ta e="T380" id="Seg_6630" s="T379">be-PST1-3SG</ta>
            <ta e="T381" id="Seg_6631" s="T380">whereto</ta>
            <ta e="T382" id="Seg_6632" s="T381">house-DAT/LOC</ta>
            <ta e="T383" id="Seg_6633" s="T382">NEG</ta>
            <ta e="T384" id="Seg_6634" s="T383">go.in-CAUS-NEG.[3SG]</ta>
            <ta e="T385" id="Seg_6635" s="T384">shame.[NOM]</ta>
            <ta e="T386" id="Seg_6636" s="T385">well</ta>
            <ta e="T387" id="Seg_6637" s="T386">go-PRS.[3SG]</ta>
            <ta e="T388" id="Seg_6638" s="T387">this</ta>
            <ta e="T389" id="Seg_6639" s="T388">human.being.[NOM]</ta>
            <ta e="T390" id="Seg_6640" s="T389">go-PST2-3SG</ta>
            <ta e="T391" id="Seg_6641" s="T390">hardly</ta>
            <ta e="T392" id="Seg_6642" s="T391">then</ta>
            <ta e="T393" id="Seg_6643" s="T392">reach-CVB.SEQ</ta>
            <ta e="T394" id="Seg_6644" s="T393">go-TEMP-3SG</ta>
            <ta e="T397" id="Seg_6645" s="T396">house-3SG-GEN</ta>
            <ta e="T398" id="Seg_6646" s="T397">place.beneath-DAT/LOC</ta>
            <ta e="T399" id="Seg_6647" s="T398">reach-PRS.[3SG]</ta>
            <ta e="T400" id="Seg_6648" s="T399">see-PST2-3SG</ta>
            <ta e="T401" id="Seg_6649" s="T400">calf-PROPR</ta>
            <ta e="T402" id="Seg_6650" s="T401">female.reindeer.[NOM]</ta>
            <ta e="T403" id="Seg_6651" s="T402">go-PRS.[3SG]</ta>
            <ta e="T404" id="Seg_6652" s="T403">house-3SG-DAT/LOC</ta>
            <ta e="T405" id="Seg_6653" s="T404">what.kind.of</ta>
            <ta e="T406" id="Seg_6654" s="T405">come-NMNZ-ABL</ta>
            <ta e="T407" id="Seg_6655" s="T406">come-PST2-3SG</ta>
            <ta e="T408" id="Seg_6656" s="T407">Q</ta>
            <ta e="T409" id="Seg_6657" s="T408">know-NEG.[3SG]</ta>
            <ta e="T410" id="Seg_6658" s="T409">reindeer-ACC</ta>
            <ta e="T411" id="Seg_6659" s="T410">3SG.[NOM]</ta>
            <ta e="T412" id="Seg_6660" s="T789">reindeer.[NOM]</ta>
            <ta e="T413" id="Seg_6661" s="T412">go-PRS.[3SG]</ta>
            <ta e="T414" id="Seg_6662" s="T413">only</ta>
            <ta e="T416" id="Seg_6663" s="T414">come-PST2.[3SG]</ta>
            <ta e="T417" id="Seg_6664" s="T416">up</ta>
            <ta e="T418" id="Seg_6665" s="T417">3SG.[NOM]</ta>
            <ta e="T419" id="Seg_6666" s="T418">die-PTCP.FUT</ta>
            <ta e="T420" id="Seg_6667" s="T419">destiny-3SG-DAT/LOC</ta>
            <ta e="T423" id="Seg_6668" s="T422">revive-PTCP.FUT</ta>
            <ta e="T424" id="Seg_6669" s="T423">destiny-3SG-DAT/LOC</ta>
            <ta e="T425" id="Seg_6670" s="T424">calf-PROPR</ta>
            <ta e="T426" id="Seg_6671" s="T425">female.reindeer-3SG.[NOM]</ta>
            <ta e="T427" id="Seg_6672" s="T426">self-3SG-DAT/LOC</ta>
            <ta e="T428" id="Seg_6673" s="T427">come-CVB.SEQ</ta>
            <ta e="T429" id="Seg_6674" s="T428">lick-CVB.SIM</ta>
            <ta e="T430" id="Seg_6675" s="T429">go-EP-PST2-3PL</ta>
            <ta e="T431" id="Seg_6676" s="T430">3SG-ACC</ta>
            <ta e="T432" id="Seg_6677" s="T431">pee-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T433" id="Seg_6678" s="T432">inside-3SG-DAT/LOC</ta>
            <ta e="T434" id="Seg_6679" s="T433">come-PST2-3SG</ta>
            <ta e="T435" id="Seg_6680" s="T434">and</ta>
            <ta e="T436" id="Seg_6681" s="T435">calf-DIM-3SG-ACC</ta>
            <ta e="T437" id="Seg_6682" s="T436">catch-CVB.SEQ</ta>
            <ta e="T438" id="Seg_6683" s="T437">take-CVB.SEQ</ta>
            <ta e="T439" id="Seg_6684" s="T438">knife-DIM-ACC</ta>
            <ta e="T440" id="Seg_6685" s="T439">take-PST1-3SG</ta>
            <ta e="T441" id="Seg_6686" s="T440">and</ta>
            <ta e="T442" id="Seg_6687" s="T441">kort</ta>
            <ta e="T443" id="Seg_6688" s="T442">make-CAUS-CVB.SEQ</ta>
            <ta e="T444" id="Seg_6689" s="T443">kill-CVB.SEQ</ta>
            <ta e="T445" id="Seg_6690" s="T444">throw-PST2.[3SG]</ta>
            <ta e="T446" id="Seg_6691" s="T445">mother-3SG.[NOM]</ta>
            <ta e="T447" id="Seg_6692" s="T446">go-PRS.[3SG]</ta>
            <ta e="T448" id="Seg_6693" s="T447">well</ta>
            <ta e="T449" id="Seg_6694" s="T448">this-3SG-3SG-ACC</ta>
            <ta e="T450" id="Seg_6695" s="T449">what.[NOM]</ta>
            <ta e="T451" id="Seg_6696" s="T450">what.[NOM]</ta>
            <ta e="T454" id="Seg_6697" s="T453">skin-EP-MED-PST2.[3SG]</ta>
            <ta e="T455" id="Seg_6698" s="T454">human.being.[NOM]</ta>
            <ta e="T456" id="Seg_6699" s="T455">raw-VBZ-CVB.SIM</ta>
            <ta e="T457" id="Seg_6700" s="T456">boil-EP-CAUS-CVB.SEQ</ta>
            <ta e="T458" id="Seg_6701" s="T457">cook-EP-MED-CVB.SEQ</ta>
            <ta e="T459" id="Seg_6702" s="T458">eat-CVB.SEQ</ta>
            <ta e="T460" id="Seg_6703" s="T459">throw-PST2.[3SG]</ta>
            <ta e="T461" id="Seg_6704" s="T460">well</ta>
            <ta e="T462" id="Seg_6705" s="T461">eye-3SG.[NOM]</ta>
            <ta e="T463" id="Seg_6706" s="T462">get.light-PST2.[3SG]</ta>
            <ta e="T464" id="Seg_6707" s="T463">there</ta>
            <ta e="T465" id="Seg_6708" s="T464">oh</ta>
            <ta e="T466" id="Seg_6709" s="T465">well</ta>
            <ta e="T467" id="Seg_6710" s="T466">now</ta>
            <ta e="T468" id="Seg_6711" s="T467">EMPH</ta>
            <ta e="T469" id="Seg_6712" s="T468">save-EP-REFL-PST1-1SG</ta>
            <ta e="T470" id="Seg_6713" s="T469">butterfly-DAT/LOC</ta>
            <ta e="T471" id="Seg_6714" s="T470">go-NEG-1SG</ta>
            <ta e="T472" id="Seg_6715" s="T471">and.so.on-NEG-1SG</ta>
            <ta e="T473" id="Seg_6716" s="T472">food-VBZ-PST1-1SG</ta>
            <ta e="T474" id="Seg_6717" s="T473">EMPH</ta>
            <ta e="T475" id="Seg_6718" s="T474">think-PRS.[3SG]</ta>
            <ta e="T476" id="Seg_6719" s="T475">food-VBZ-PTCP.PST</ta>
            <ta e="T477" id="Seg_6720" s="T476">human.being.[NOM]</ta>
            <ta e="T478" id="Seg_6721" s="T477">cook-EP-MED-CVB.SEQ</ta>
            <ta e="T479" id="Seg_6722" s="T478">after</ta>
            <ta e="T480" id="Seg_6723" s="T479">eat-PRS.[3SG]</ta>
            <ta e="T481" id="Seg_6724" s="T480">ok</ta>
            <ta e="T482" id="Seg_6725" s="T481">well</ta>
            <ta e="T483" id="Seg_6726" s="T482">how.much</ta>
            <ta e="T484" id="Seg_6727" s="T483">day-ACC</ta>
            <ta e="T485" id="Seg_6728" s="T484">live-PST2-3SG</ta>
            <ta e="T486" id="Seg_6729" s="T485">Q</ta>
            <ta e="T487" id="Seg_6730" s="T486">reindeer-PROPR</ta>
            <ta e="T488" id="Seg_6731" s="T487">reindeer-3SG-ACC</ta>
            <ta e="T489" id="Seg_6732" s="T488">EMPH</ta>
            <ta e="T490" id="Seg_6733" s="T489">catch-EP-PST2.[3SG]</ta>
            <ta e="T491" id="Seg_6734" s="T490">female.reindeer-3SG-ACC</ta>
            <ta e="T492" id="Seg_6735" s="T491">female.reindeer-PROPR.[NOM]</ta>
            <ta e="T493" id="Seg_6736" s="T492">what.[NOM]</ta>
            <ta e="T494" id="Seg_6737" s="T493">be-FUT.[3SG]-Q</ta>
            <ta e="T495" id="Seg_6738" s="T494">now</ta>
            <ta e="T496" id="Seg_6739" s="T495">oh</ta>
            <ta e="T497" id="Seg_6740" s="T496">this</ta>
            <ta e="T498" id="Seg_6741" s="T497">lie-TEMP-3SG</ta>
            <ta e="T499" id="Seg_6742" s="T498">butterfly-3SG-GEN</ta>
            <ta e="T500" id="Seg_6743" s="T499">voice-3SG.[NOM]</ta>
            <ta e="T501" id="Seg_6744" s="T500">come-PST1-3SG</ta>
            <ta e="T502" id="Seg_6745" s="T501">well</ta>
            <ta e="T503" id="Seg_6746" s="T502">only</ta>
            <ta e="T504" id="Seg_6747" s="T503">see-PST2-3SG</ta>
            <ta e="T505" id="Seg_6748" s="T504">well</ta>
            <ta e="T506" id="Seg_6749" s="T505">1SG.[NOM]</ta>
            <ta e="T507" id="Seg_6750" s="T506">now</ta>
            <ta e="T508" id="Seg_6751" s="T507">like.that</ta>
            <ta e="T509" id="Seg_6752" s="T508">make-FUT-1SG</ta>
            <ta e="T510" id="Seg_6753" s="T509">wait</ta>
            <ta e="T511" id="Seg_6754" s="T510">come-IMP.3SG</ta>
            <ta e="T512" id="Seg_6755" s="T511">only</ta>
            <ta e="T513" id="Seg_6756" s="T512">think-PST2.[3SG]</ta>
            <ta e="T514" id="Seg_6757" s="T513">1SG-DAT/LOC</ta>
            <ta e="T515" id="Seg_6758" s="T514">food.[NOM]</ta>
            <ta e="T516" id="Seg_6759" s="T515">butterfly.[NOM]</ta>
            <ta e="T517" id="Seg_6760" s="T516">what-VBZ-CVB.SEQ</ta>
            <ta e="T518" id="Seg_6761" s="T517">hardly</ta>
            <ta e="T519" id="Seg_6762" s="T518">go-PRS.[3SG]</ta>
            <ta e="T520" id="Seg_6763" s="T519">completely</ta>
            <ta e="T521" id="Seg_6764" s="T520">hunger-CVB.SEQ</ta>
            <ta e="T522" id="Seg_6765" s="T521">hardly</ta>
            <ta e="T523" id="Seg_6766" s="T522">walk-CVB.SEQ</ta>
            <ta e="T524" id="Seg_6767" s="T523">go-PRS.[3SG]</ta>
            <ta e="T525" id="Seg_6768" s="T524">come-PST2.[3SG]</ta>
            <ta e="T526" id="Seg_6769" s="T525">and</ta>
            <ta e="T527" id="Seg_6770" s="T526">come-PST2.[3SG]</ta>
            <ta e="T528" id="Seg_6771" s="T527">come-PTCP.PRS-3SG-ACC</ta>
            <ta e="T529" id="Seg_6772" s="T528">with</ta>
            <ta e="T530" id="Seg_6773" s="T529">house-3SG.[NOM]</ta>
            <ta e="T531" id="Seg_6774" s="T530">lock-CVB.SEQ</ta>
            <ta e="T532" id="Seg_6775" s="T531">lock-EP-MED-CVB.SEQ</ta>
            <ta e="T533" id="Seg_6776" s="T532">throw-PST2.[3SG]</ta>
            <ta e="T534" id="Seg_6777" s="T533">well</ta>
            <ta e="T535" id="Seg_6778" s="T534">3SG-ACC</ta>
            <ta e="T536" id="Seg_6779" s="T535">punish-VBZ-FUT-1SG</ta>
            <ta e="T537" id="Seg_6780" s="T536">not.long.ago</ta>
            <ta e="T538" id="Seg_6781" s="T537">1SG-ACC</ta>
            <ta e="T539" id="Seg_6782" s="T538">again</ta>
            <ta e="T540" id="Seg_6783" s="T539">3SG.[NOM]</ta>
            <ta e="T541" id="Seg_6784" s="T540">torture-PST2-3SG</ta>
            <ta e="T542" id="Seg_6785" s="T541">like.this</ta>
            <ta e="T543" id="Seg_6786" s="T542">with.crutch</ta>
            <ta e="T544" id="Seg_6787" s="T543">old.man.[NOM]</ta>
            <ta e="T545" id="Seg_6788" s="T544">with.crutch</ta>
            <ta e="T546" id="Seg_6789" s="T545">old.man.[NOM]</ta>
            <ta e="T547" id="Seg_6790" s="T546">one</ta>
            <ta e="T548" id="Seg_6791" s="T790">INDEF</ta>
            <ta e="T549" id="Seg_6792" s="T548">sinewy.meat-DIM-PART</ta>
            <ta e="T550" id="Seg_6793" s="T549">give-CVB.SEQ</ta>
            <ta e="T551" id="Seg_6794" s="T550">come.to.aid.[IMP.2SG]</ta>
            <ta e="T552" id="Seg_6795" s="T551">with.crutch</ta>
            <ta e="T553" id="Seg_6796" s="T552">old.man.[NOM]</ta>
            <ta e="T554" id="Seg_6797" s="T553">die-PST1-1SG</ta>
            <ta e="T555" id="Seg_6798" s="T554">stay-PST1-1SG</ta>
            <ta e="T556" id="Seg_6799" s="T555">die-PST1-1SG</ta>
            <ta e="T560" id="Seg_6800" s="T558">stay-PST1-1SG</ta>
            <ta e="T561" id="Seg_6801" s="T560">day-1SG-DAT/LOC</ta>
            <ta e="T562" id="Seg_6802" s="T561">stand-PST1-3SG</ta>
            <ta e="T563" id="Seg_6803" s="T562">inanimate</ta>
            <ta e="T564" id="Seg_6804" s="T563">human.being-3SG.[NOM]</ta>
            <ta e="T565" id="Seg_6805" s="T564">be-TEMP-2SG</ta>
            <ta e="T566" id="Seg_6806" s="T565">with.crutch</ta>
            <ta e="T567" id="Seg_6807" s="T566">old.man.[NOM]</ta>
            <ta e="T568" id="Seg_6808" s="T567">with.crutch</ta>
            <ta e="T569" id="Seg_6809" s="T568">old.man.[NOM]</ta>
            <ta e="T570" id="Seg_6810" s="T569">sing-EP-MED-CVB.SIM</ta>
            <ta e="T571" id="Seg_6811" s="T570">lie-PST2.[3SG]</ta>
            <ta e="T572" id="Seg_6812" s="T571">what-ACC</ta>
            <ta e="T573" id="Seg_6813" s="T572">NEG</ta>
            <ta e="T574" id="Seg_6814" s="T573">give-PST2.NEG.[3SG]</ta>
            <ta e="T575" id="Seg_6815" s="T574">empty</ta>
            <ta e="T576" id="Seg_6816" s="T575">what.[NOM]</ta>
            <ta e="T577" id="Seg_6817" s="T576">INDEF</ta>
            <ta e="T578" id="Seg_6818" s="T577">this</ta>
            <ta e="T579" id="Seg_6819" s="T578">reindeer.calf-3SG-GEN</ta>
            <ta e="T580" id="Seg_6820" s="T579">own-3SG-GEN</ta>
            <ta e="T581" id="Seg_6821" s="T580">hoof-3SG.[NOM]</ta>
            <ta e="T582" id="Seg_6822" s="T581">hoof-3PL-ACC</ta>
            <ta e="T583" id="Seg_6823" s="T582">what-VBZ-EP-MED-PST2.[3SG]</ta>
            <ta e="T584" id="Seg_6824" s="T583">%%-3SG-ACC</ta>
            <ta e="T585" id="Seg_6825" s="T584">with</ta>
            <ta e="T586" id="Seg_6826" s="T585">hoof-3SG.[NOM]</ta>
            <ta e="T587" id="Seg_6827" s="T586">Q</ta>
            <ta e="T588" id="Seg_6828" s="T587">what.[NOM]</ta>
            <ta e="T589" id="Seg_6829" s="T588">Q</ta>
            <ta e="T590" id="Seg_6830" s="T589">that-ACC</ta>
            <ta e="T593" id="Seg_6831" s="T592">%%-PROPR</ta>
            <ta e="T594" id="Seg_6832" s="T593">go-CVB.SEQ</ta>
            <ta e="T595" id="Seg_6833" s="T594">that-ACC</ta>
            <ta e="T596" id="Seg_6834" s="T595">throw-CVB.SEQ</ta>
            <ta e="T597" id="Seg_6835" s="T596">go.[IMP.2SG]</ta>
            <ta e="T598" id="Seg_6836" s="T597">eat.[IMP.2SG]</ta>
            <ta e="T599" id="Seg_6837" s="T598">say-CVB.SIM</ta>
            <ta e="T600" id="Seg_6838" s="T599">two</ta>
            <ta e="T601" id="Seg_6839" s="T600">hoof-ACC</ta>
            <ta e="T602" id="Seg_6840" s="T601">throw-CVB.SEQ</ta>
            <ta e="T603" id="Seg_6841" s="T602">throw-PST2.[3SG]</ta>
            <ta e="T604" id="Seg_6842" s="T603">go.[IMP.2SG]</ta>
            <ta e="T605" id="Seg_6843" s="T604">house-2SG-DAT/LOC</ta>
            <ta e="T606" id="Seg_6844" s="T605">say-PRS.[3SG]</ta>
            <ta e="T607" id="Seg_6845" s="T606">more</ta>
            <ta e="T608" id="Seg_6846" s="T607">come-EP-NEG.[IMP.2SG]</ta>
            <ta e="T611" id="Seg_6847" s="T610">not.long.ago</ta>
            <ta e="T612" id="Seg_6848" s="T611">1SG-ACC</ta>
            <ta e="T613" id="Seg_6849" s="T612">also</ta>
            <ta e="T614" id="Seg_6850" s="T613">well</ta>
            <ta e="T615" id="Seg_6851" s="T614">torture-PST2-EP-2SG</ta>
            <ta e="T616" id="Seg_6852" s="T615">that-3SG-3SG.[NOM]</ta>
            <ta e="T617" id="Seg_6853" s="T616">come.to.an.end-FUT.[3SG]</ta>
            <ta e="T618" id="Seg_6854" s="T617">MOD</ta>
            <ta e="T619" id="Seg_6855" s="T618">go-CVB.SEQ</ta>
            <ta e="T620" id="Seg_6856" s="T619">stay-PST2.[3SG]</ta>
            <ta e="T621" id="Seg_6857" s="T620">well</ta>
            <ta e="T622" id="Seg_6858" s="T621">go-CVB.SEQ</ta>
            <ta e="T623" id="Seg_6859" s="T622">stay-PST2.[3SG]</ta>
            <ta e="T624" id="Seg_6860" s="T623">house-3SG-DAT/LOC</ta>
            <ta e="T625" id="Seg_6861" s="T624">again</ta>
            <ta e="T627" id="Seg_6862" s="T626">go-PST2.[3SG]</ta>
            <ta e="T628" id="Seg_6863" s="T627">and</ta>
            <ta e="T629" id="Seg_6864" s="T628">next.morning-3SG-ACC</ta>
            <ta e="T632" id="Seg_6865" s="T631">how.much</ta>
            <ta e="T633" id="Seg_6866" s="T632">INDEF</ta>
            <ta e="T634" id="Seg_6867" s="T633">long</ta>
            <ta e="T635" id="Seg_6868" s="T634">day-ACC</ta>
            <ta e="T636" id="Seg_6869" s="T635">be-CVB.SEQ</ta>
            <ta e="T637" id="Seg_6870" s="T636">after</ta>
            <ta e="T638" id="Seg_6871" s="T637">again</ta>
            <ta e="T639" id="Seg_6872" s="T638">come-PRS.[3SG]</ta>
            <ta e="T640" id="Seg_6873" s="T639">well</ta>
            <ta e="T641" id="Seg_6874" s="T640">completely</ta>
            <ta e="T642" id="Seg_6875" s="T641">die-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T643" id="Seg_6876" s="T642">go-PST2.[3SG]</ta>
            <ta e="T644" id="Seg_6877" s="T643">apparently</ta>
            <ta e="T645" id="Seg_6878" s="T644">friend-3SG.[NOM]</ta>
            <ta e="T646" id="Seg_6879" s="T645">butterfly-3SG.[NOM]</ta>
            <ta e="T647" id="Seg_6880" s="T646">reach.a.limit-PST2.[3SG]</ta>
            <ta e="T648" id="Seg_6881" s="T647">chase-TEMP-3SG</ta>
            <ta e="T649" id="Seg_6882" s="T648">and</ta>
            <ta e="T650" id="Seg_6883" s="T649">go-NEG.[3SG]</ta>
            <ta e="T651" id="Seg_6884" s="T650">go.in-TEMP-3SG</ta>
            <ta e="T652" id="Seg_6885" s="T651">come-NEG.[3SG]</ta>
            <ta e="T653" id="Seg_6886" s="T652">oh</ta>
            <ta e="T654" id="Seg_6887" s="T653">well</ta>
            <ta e="T655" id="Seg_6888" s="T654">how</ta>
            <ta e="T656" id="Seg_6889" s="T655">NEG</ta>
            <ta e="T657" id="Seg_6890" s="T656">make-PTCP.FUT-3SG-ACC</ta>
            <ta e="T658" id="Seg_6891" s="T657">very</ta>
            <ta e="T659" id="Seg_6892" s="T658">well</ta>
            <ta e="T660" id="Seg_6893" s="T659">heart-3SG.[NOM]</ta>
            <ta e="T661" id="Seg_6894" s="T660">pain.[NOM]</ta>
            <ta e="T662" id="Seg_6895" s="T661">be.sick-PRS.[3SG]</ta>
            <ta e="T663" id="Seg_6896" s="T662">human.being.[NOM]</ta>
            <ta e="T664" id="Seg_6897" s="T663">friend-3SG-ACC</ta>
            <ta e="T665" id="Seg_6898" s="T664">feel.sorry-PST1-3SG</ta>
            <ta e="T666" id="Seg_6899" s="T665">EMPH</ta>
            <ta e="T667" id="Seg_6900" s="T666">feel.sorry-CVB.SEQ</ta>
            <ta e="T668" id="Seg_6901" s="T667">how</ta>
            <ta e="T669" id="Seg_6902" s="T668">NEG</ta>
            <ta e="T670" id="Seg_6903" s="T669">make-PTCP.FUT-3SG-ACC</ta>
            <ta e="T671" id="Seg_6904" s="T670">very</ta>
            <ta e="T672" id="Seg_6905" s="T671">chase-CAUS-PTCP.FUT-3SG-ACC</ta>
            <ta e="T673" id="Seg_6906" s="T672">and</ta>
            <ta e="T674" id="Seg_6907" s="T791">go-CVB.SEQ</ta>
            <ta e="T675" id="Seg_6908" s="T674">and</ta>
            <ta e="T676" id="Seg_6909" s="T675">however</ta>
            <ta e="T677" id="Seg_6910" s="T676">house-3SG-DAT/LOC</ta>
            <ta e="T678" id="Seg_6911" s="T677">reach-FUT.[3SG]</ta>
            <ta e="T680" id="Seg_6912" s="T678">NEG</ta>
            <ta e="T681" id="Seg_6913" s="T792">if.so</ta>
            <ta e="T682" id="Seg_6914" s="T681">go.in.[IMP.2SG]</ta>
            <ta e="T683" id="Seg_6915" s="T682">say-PST2.[3SG]</ta>
            <ta e="T684" id="Seg_6916" s="T683">eat.[IMP.2SG]</ta>
            <ta e="T685" id="Seg_6917" s="T684">chat-IMP.1DU</ta>
            <ta e="T686" id="Seg_6918" s="T685">then</ta>
            <ta e="T688" id="Seg_6919" s="T686">good-ADVZ</ta>
            <ta e="T689" id="Seg_6920" s="T688">well</ta>
            <ta e="T690" id="Seg_6921" s="T689">INTNS</ta>
            <ta e="T691" id="Seg_6922" s="T690">indeed</ta>
            <ta e="T692" id="Seg_6923" s="T691">chat-PST2-3PL</ta>
            <ta e="T693" id="Seg_6924" s="T692">well</ta>
            <ta e="T694" id="Seg_6925" s="T693">then</ta>
            <ta e="T695" id="Seg_6926" s="T694">1PL.[NOM]</ta>
            <ta e="T696" id="Seg_6927" s="T695">good-ADVZ</ta>
            <ta e="T697" id="Seg_6928" s="T696">chat-IMP.1DU</ta>
            <ta e="T698" id="Seg_6929" s="T697">together</ta>
            <ta e="T699" id="Seg_6930" s="T698">now</ta>
            <ta e="T700" id="Seg_6931" s="T699">together</ta>
            <ta e="T701" id="Seg_6932" s="T700">live-FUT-1PL</ta>
            <ta e="T702" id="Seg_6933" s="T701">well</ta>
            <ta e="T703" id="Seg_6934" s="T702">why</ta>
            <ta e="T704" id="Seg_6935" s="T703">two</ta>
            <ta e="T705" id="Seg_6936" s="T704">side.[NOM]</ta>
            <ta e="T706" id="Seg_6937" s="T705">live-PRS-1PL</ta>
            <ta e="T707" id="Seg_6938" s="T706">1PL.[NOM]</ta>
            <ta e="T708" id="Seg_6939" s="T707">see-PRS-2SG</ta>
            <ta e="T709" id="Seg_6940" s="T708">1SG.[NOM]</ta>
            <ta e="T710" id="Seg_6941" s="T709">reindeer-REFL/MED-PST1-1SG</ta>
            <ta e="T711" id="Seg_6942" s="T710">still</ta>
            <ta e="T712" id="Seg_6943" s="T711">2SG.[NOM]</ta>
            <ta e="T713" id="Seg_6944" s="T712">not.long.ago</ta>
            <ta e="T714" id="Seg_6945" s="T713">1SG-ACC</ta>
            <ta e="T715" id="Seg_6946" s="T714">offend-CVB.SIM</ta>
            <ta e="T717" id="Seg_6947" s="T716">what.[NOM]</ta>
            <ta e="T718" id="Seg_6948" s="T717">reindeer-3SG.[NOM]</ta>
            <ta e="T719" id="Seg_6949" s="T718">god.[NOM]</ta>
            <ta e="T720" id="Seg_6950" s="T719">bring-PST2-3SG</ta>
            <ta e="T721" id="Seg_6951" s="T720">MOD</ta>
            <ta e="T722" id="Seg_6952" s="T721">reindeer-ACC</ta>
            <ta e="T723" id="Seg_6953" s="T722">eat-EP-CAUS-PST1-1SG</ta>
            <ta e="T724" id="Seg_6954" s="T723">well</ta>
            <ta e="T725" id="Seg_6955" s="T724">now</ta>
            <ta e="T726" id="Seg_6956" s="T725">reindeer-REFL/MED-CVB.SIM</ta>
            <ta e="T727" id="Seg_6957" s="T726">together</ta>
            <ta e="T728" id="Seg_6958" s="T727">live-IMP.1DU</ta>
            <ta e="T729" id="Seg_6959" s="T728">1PL.[NOM]</ta>
            <ta e="T730" id="Seg_6960" s="T729">now</ta>
            <ta e="T731" id="Seg_6961" s="T730">together</ta>
            <ta e="T732" id="Seg_6962" s="T731">live-CVB.SEQ-1PL</ta>
            <ta e="T733" id="Seg_6963" s="T732">one</ta>
            <ta e="T734" id="Seg_6964" s="T733">advise-INSTR</ta>
            <ta e="T735" id="Seg_6965" s="T734">hunt-IMP.1DU</ta>
            <ta e="T736" id="Seg_6966" s="T735">and</ta>
            <ta e="T737" id="Seg_6967" s="T736">one</ta>
            <ta e="T738" id="Seg_6968" s="T737">house-PROPR.[NOM]</ta>
            <ta e="T739" id="Seg_6969" s="T738">be-IMP.1DU</ta>
            <ta e="T740" id="Seg_6970" s="T739">2SG.[NOM]</ta>
            <ta e="T741" id="Seg_6971" s="T740">house-2SG-ACC</ta>
            <ta e="T742" id="Seg_6972" s="T741">convey-CVB.SIM</ta>
            <ta e="T743" id="Seg_6973" s="T742">bring-IMP.1DU</ta>
            <ta e="T744" id="Seg_6974" s="T743">this</ta>
            <ta e="T745" id="Seg_6975" s="T744">reindeer-PROPR-1SG</ta>
            <ta e="T746" id="Seg_6976" s="T745">EMPH</ta>
            <ta e="T747" id="Seg_6977" s="T746">this</ta>
            <ta e="T748" id="Seg_6978" s="T747">one</ta>
            <ta e="T749" id="Seg_6979" s="T748">reindeer-1PL.[NOM]</ta>
            <ta e="T750" id="Seg_6980" s="T749">pull-EP-CAUS-CVB.SEQ</ta>
            <ta e="T751" id="Seg_6981" s="T750">bring-PRS.[3SG]</ta>
            <ta e="T752" id="Seg_6982" s="T751">what-PROPR</ta>
            <ta e="T753" id="Seg_6983" s="T752">be-FUT.[3SG]-Q</ta>
            <ta e="T754" id="Seg_6984" s="T753">EMPH</ta>
            <ta e="T755" id="Seg_6985" s="T754">that.[NOM]</ta>
            <ta e="T756" id="Seg_6986" s="T755">bring-IMP.1DU</ta>
            <ta e="T757" id="Seg_6987" s="T756">hither</ta>
            <ta e="T758" id="Seg_6988" s="T757">reindeer-2SG-ACC</ta>
            <ta e="T759" id="Seg_6989" s="T758">eh</ta>
            <ta e="T760" id="Seg_6990" s="T759">who-2SG-ACC</ta>
            <ta e="T761" id="Seg_6991" s="T760">here</ta>
            <ta e="T762" id="Seg_6992" s="T761">one</ta>
            <ta e="T763" id="Seg_6993" s="T762">together</ta>
            <ta e="T764" id="Seg_6994" s="T763">live-IMP.1DU</ta>
            <ta e="T765" id="Seg_6995" s="T764">1PL.[NOM]</ta>
            <ta e="T766" id="Seg_6996" s="T765">advise-EP-RECP/COLL-CVB.SEQ-3PL</ta>
            <ta e="T767" id="Seg_6997" s="T766">well</ta>
            <ta e="T768" id="Seg_6998" s="T767">together</ta>
            <ta e="T769" id="Seg_6999" s="T768">live-CVB.SEQ</ta>
            <ta e="T770" id="Seg_7000" s="T769">stay-PST2-3PL</ta>
            <ta e="T771" id="Seg_7001" s="T770">two-COLL-3PL.[NOM]</ta>
            <ta e="T772" id="Seg_7002" s="T771">one</ta>
            <ta e="T773" id="Seg_7003" s="T772">advise.[NOM]</ta>
            <ta e="T774" id="Seg_7004" s="T773">be-CVB.SEQ-3PL</ta>
            <ta e="T775" id="Seg_7005" s="T774">method.[NOM]</ta>
            <ta e="T776" id="Seg_7006" s="T775">be-CVB.SEQ-3PL</ta>
            <ta e="T777" id="Seg_7007" s="T776">two-COLL-3PL.[NOM]</ta>
            <ta e="T778" id="Seg_7008" s="T777">well</ta>
            <ta e="T779" id="Seg_7009" s="T778">be.rich-CVB.SEQ-eat.ones.fill-CVB.SEQ</ta>
            <ta e="T780" id="Seg_7010" s="T779">live-PST2-3PL</ta>
            <ta e="T781" id="Seg_7011" s="T780">last-3SG.[NOM]</ta>
            <ta e="T782" id="Seg_7012" s="T781">that.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_7013" s="T0">Schmetterling-ACC</ta>
            <ta e="T2" id="Seg_7014" s="T1">mit</ta>
            <ta e="T3" id="Seg_7015" s="T2">mit.Krücke</ta>
            <ta e="T4" id="Seg_7016" s="T3">alter.Mann.[NOM]</ta>
            <ta e="T5" id="Seg_7017" s="T4">leben-PST2-3PL</ta>
            <ta e="T6" id="Seg_7018" s="T5">man.sagt</ta>
            <ta e="T7" id="Seg_7019" s="T6">fern-SIM</ta>
            <ta e="T8" id="Seg_7020" s="T7">leben-PRS-3PL</ta>
            <ta e="T9" id="Seg_7021" s="T8">selbst</ta>
            <ta e="T10" id="Seg_7022" s="T9">selbst-3PL-ACC</ta>
            <ta e="T11" id="Seg_7023" s="T10">wissen-RECP/COLL-EP-RECP/COLL-PRS-3PL</ta>
            <ta e="T12" id="Seg_7024" s="T11">einer.von.zwei-3SG.[NOM]</ta>
            <ta e="T13" id="Seg_7025" s="T12">jenes.[NOM]</ta>
            <ta e="T14" id="Seg_7026" s="T13">zu</ta>
            <ta e="T15" id="Seg_7027" s="T14">Haus-PROPR.[NOM]</ta>
            <ta e="T16" id="Seg_7028" s="T15">einer.von.zwei-3SG.[NOM]</ta>
            <ta e="T17" id="Seg_7029" s="T16">dieses.[NOM]</ta>
            <ta e="T18" id="Seg_7030" s="T17">zu</ta>
            <ta e="T19" id="Seg_7031" s="T18">Haus-PROPR.[NOM]</ta>
            <ta e="T20" id="Seg_7032" s="T19">leben-CVB.SEQ-3PL</ta>
            <ta e="T21" id="Seg_7033" s="T20">zwei-COLL-3PL.[NOM]</ta>
            <ta e="T22" id="Seg_7034" s="T21">treffen-EP-PST2-3PL</ta>
            <ta e="T23" id="Seg_7035" s="T22">zusammen</ta>
            <ta e="T24" id="Seg_7036" s="T23">erzählen-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T25" id="Seg_7037" s="T24">nun</ta>
            <ta e="T26" id="Seg_7038" s="T25">sagen-PRS.[3SG]</ta>
            <ta e="T27" id="Seg_7039" s="T26">sagen-EP-RECP/COLL-PRS-3PL</ta>
            <ta e="T28" id="Seg_7040" s="T27">Schmetterling-3SG-ACC</ta>
            <ta e="T29" id="Seg_7041" s="T28">mit</ta>
            <ta e="T30" id="Seg_7042" s="T29">mit.Krücke</ta>
            <ta e="T31" id="Seg_7043" s="T30">alter.Mann.[NOM]</ta>
            <ta e="T32" id="Seg_7044" s="T31">nun</ta>
            <ta e="T33" id="Seg_7045" s="T32">1PL.[NOM]</ta>
            <ta e="T36" id="Seg_7046" s="T35">eins</ta>
            <ta e="T37" id="Seg_7047" s="T36">Jahr-ACC</ta>
            <ta e="T38" id="Seg_7048" s="T37">überstehen-PTCP.PRS</ta>
            <ta e="T39" id="Seg_7049" s="T38">Nahrung-1PL-ACC</ta>
            <ta e="T40" id="Seg_7050" s="T39">zusammen</ta>
            <ta e="T41" id="Seg_7051" s="T40">verbinden-CVB.SEQ-1PL</ta>
            <ta e="T42" id="Seg_7052" s="T41">essen-IMP.1DU</ta>
            <ta e="T43" id="Seg_7053" s="T42">wollen-TEMP-1PL</ta>
            <ta e="T44" id="Seg_7054" s="T43">wohl</ta>
            <ta e="T47" id="Seg_7055" s="T46">lang-ADVZ</ta>
            <ta e="T48" id="Seg_7056" s="T47">leben-FUT-1PL</ta>
            <ta e="T49" id="Seg_7057" s="T48">wohl</ta>
            <ta e="T50" id="Seg_7058" s="T49">Jahr-1PL-ACC</ta>
            <ta e="T51" id="Seg_7059" s="T50">schneiden-PTCP.PRS-1PL-DAT/LOC</ta>
            <ta e="T52" id="Seg_7060" s="T51">EMPH</ta>
            <ta e="T53" id="Seg_7061" s="T52">dieses</ta>
            <ta e="T54" id="Seg_7062" s="T53">schlecht</ta>
            <ta e="T55" id="Seg_7063" s="T54">Jahr-PL.[NOM]</ta>
            <ta e="T56" id="Seg_7064" s="T55">es.gibt-3PL</ta>
            <ta e="T57" id="Seg_7065" s="T56">doch</ta>
            <ta e="T58" id="Seg_7066" s="T57">in.Ordnung</ta>
            <ta e="T59" id="Seg_7067" s="T58">jetzt</ta>
            <ta e="T60" id="Seg_7068" s="T59">dann</ta>
            <ta e="T61" id="Seg_7069" s="T60">1PL.[NOM]</ta>
            <ta e="T62" id="Seg_7070" s="T61">Schmetterling.[NOM]</ta>
            <ta e="T63" id="Seg_7071" s="T62">dein</ta>
            <ta e="T64" id="Seg_7072" s="T783">Nahrung-2SG-ACC</ta>
            <ta e="T66" id="Seg_7073" s="T64">essen-IMP.1DU</ta>
            <ta e="T67" id="Seg_7074" s="T66">AFFIRM</ta>
            <ta e="T68" id="Seg_7075" s="T67">mit.Krücke</ta>
            <ta e="T69" id="Seg_7076" s="T68">alter.Mann.[NOM]</ta>
            <ta e="T70" id="Seg_7077" s="T69">3SG.[NOM]</ta>
            <ta e="T71" id="Seg_7078" s="T70">jenes</ta>
            <ta e="T72" id="Seg_7079" s="T71">sein</ta>
            <ta e="T73" id="Seg_7080" s="T72">Haus-3SG-DAT/LOC</ta>
            <ta e="T74" id="Seg_7081" s="T73">gehen-CVB.SEQ-1PL</ta>
            <ta e="T75" id="Seg_7082" s="T74">essen-IMP.1DU</ta>
            <ta e="T76" id="Seg_7083" s="T75">doch</ta>
            <ta e="T77" id="Seg_7084" s="T76">in.Ordnung</ta>
            <ta e="T78" id="Seg_7085" s="T77">so</ta>
            <ta e="T79" id="Seg_7086" s="T78">essen-IMP.1DU</ta>
            <ta e="T80" id="Seg_7087" s="T79">oh</ta>
            <ta e="T81" id="Seg_7088" s="T80">doch</ta>
            <ta e="T82" id="Seg_7089" s="T81">essen-PRS-3PL</ta>
            <ta e="T83" id="Seg_7090" s="T82">nur</ta>
            <ta e="T84" id="Seg_7091" s="T83">Schmetterling.[NOM]</ta>
            <ta e="T85" id="Seg_7092" s="T84">Haus-3SG-DAT/LOC</ta>
            <ta e="T86" id="Seg_7093" s="T85">essen-PRS-3PL</ta>
            <ta e="T87" id="Seg_7094" s="T86">eins</ta>
            <ta e="T88" id="Seg_7095" s="T87">Hälfte-ACC</ta>
            <ta e="T89" id="Seg_7096" s="T88">doch</ta>
            <ta e="T90" id="Seg_7097" s="T89">zu.Ende.gehen-PTCP.FUT-3SG-DAT/LOC</ta>
            <ta e="T91" id="Seg_7098" s="T90">bis.zu</ta>
            <ta e="T92" id="Seg_7099" s="T91">doch</ta>
            <ta e="T93" id="Seg_7100" s="T92">essen-CVB.SEQ-3PL</ta>
            <ta e="T94" id="Seg_7101" s="T93">jenes-3SG-3PL-GEN</ta>
            <ta e="T95" id="Seg_7102" s="T94">Nahrung-3PL.[NOM]</ta>
            <ta e="T96" id="Seg_7103" s="T95">zu.Ende.gehen-PST1-3SG</ta>
            <ta e="T97" id="Seg_7104" s="T96">Schmetterling.[NOM]</ta>
            <ta e="T98" id="Seg_7105" s="T97">eigen-3SG.[NOM]</ta>
            <ta e="T99" id="Seg_7106" s="T98">doch</ta>
            <ta e="T100" id="Seg_7107" s="T99">mit.Krücke</ta>
            <ta e="T101" id="Seg_7108" s="T100">alter.Mann-DAT/LOC</ta>
            <ta e="T102" id="Seg_7109" s="T101">gehen-IMP.1PL</ta>
            <ta e="T103" id="Seg_7110" s="T102">gehen-CVB.SEQ-3PL</ta>
            <ta e="T104" id="Seg_7111" s="T103">doch</ta>
            <ta e="T105" id="Seg_7112" s="T104">jenes</ta>
            <ta e="T106" id="Seg_7113" s="T105">Nahrung-3SG-ACC</ta>
            <ta e="T107" id="Seg_7114" s="T106">essen-CAP-3PL</ta>
            <ta e="T108" id="Seg_7115" s="T107">oh</ta>
            <ta e="T109" id="Seg_7116" s="T108">mit.Krücke</ta>
            <ta e="T110" id="Seg_7117" s="T109">alter.Mann-DAT/LOC</ta>
            <ta e="T111" id="Seg_7118" s="T110">gehen-PST1-3PL</ta>
            <ta e="T113" id="Seg_7119" s="T112">doch</ta>
            <ta e="T114" id="Seg_7120" s="T113">essen-PRS-3PL</ta>
            <ta e="T115" id="Seg_7121" s="T114">essen-PRS-3PL-essen-PRS-3PL</ta>
            <ta e="T116" id="Seg_7122" s="T115">EMPH</ta>
            <ta e="T117" id="Seg_7123" s="T116">hey</ta>
            <ta e="T118" id="Seg_7124" s="T117">eins</ta>
            <ta e="T119" id="Seg_7125" s="T118">Freund-3SG.[NOM]</ta>
            <ta e="T120" id="Seg_7126" s="T119">mein</ta>
            <ta e="T121" id="Seg_7127" s="T120">Nahrung-EP-1SG.[NOM]</ta>
            <ta e="T122" id="Seg_7128" s="T121">zu.Ende.gehen-PTCP.PRS</ta>
            <ta e="T123" id="Seg_7129" s="T122">sein-PST2.NEG.[3SG]</ta>
            <ta e="T124" id="Seg_7130" s="T784">sagen-PRS.[3SG]</ta>
            <ta e="T125" id="Seg_7131" s="T124">2SG.[NOM]</ta>
            <ta e="T126" id="Seg_7132" s="T125">Haus-2SG-DAT/LOC</ta>
            <ta e="T128" id="Seg_7133" s="T126">gehen.[IMP.2SG]</ta>
            <ta e="T129" id="Seg_7134" s="T128">selbst-2SG.[NOM]</ta>
            <ta e="T130" id="Seg_7135" s="T129">jagen-EP-MED-CVB.SEQ</ta>
            <ta e="T131" id="Seg_7136" s="T130">1SG.[NOM]</ta>
            <ta e="T132" id="Seg_7137" s="T131">Nahrung-1SG-ACC</ta>
            <ta e="T133" id="Seg_7138" s="T132">ganz-3SG-ACC</ta>
            <ta e="T134" id="Seg_7139" s="T133">essen-PTCP.FUT-1SG-ACC</ta>
            <ta e="T135" id="Seg_7140" s="T134">wollen-NEG-1SG</ta>
            <ta e="T136" id="Seg_7141" s="T135">EMPH</ta>
            <ta e="T137" id="Seg_7142" s="T136">doch</ta>
            <ta e="T138" id="Seg_7143" s="T137">gehen-CVB.SEQ</ta>
            <ta e="T139" id="Seg_7144" s="T138">bleiben-PST1-3SG</ta>
            <ta e="T140" id="Seg_7145" s="T139">Freund-3SG.[NOM]</ta>
            <ta e="T141" id="Seg_7146" s="T140">hey</ta>
            <ta e="T142" id="Seg_7147" s="T141">gehen-CVB.SEQ</ta>
            <ta e="T143" id="Seg_7148" s="T142">was-ACC</ta>
            <ta e="T144" id="Seg_7149" s="T143">essen-FUT-3SG-Q</ta>
            <ta e="T145" id="Seg_7150" s="T144">Haus-3SG-DAT/LOC</ta>
            <ta e="T146" id="Seg_7151" s="T145">Schmetterling.[NOM]</ta>
            <ta e="T147" id="Seg_7152" s="T146">gehen-PST2-3SG</ta>
            <ta e="T148" id="Seg_7153" s="T147">Haus-3SG-DAT/LOC</ta>
            <ta e="T149" id="Seg_7154" s="T148">was-3SG.[NOM]</ta>
            <ta e="T150" id="Seg_7155" s="T149">NEG</ta>
            <ta e="T151" id="Seg_7156" s="T150">NEG.EX</ta>
            <ta e="T152" id="Seg_7157" s="T151">was.[NOM]</ta>
            <ta e="T153" id="Seg_7158" s="T152">INDEF</ta>
            <ta e="T154" id="Seg_7159" s="T153">wann</ta>
            <ta e="T155" id="Seg_7160" s="T154">sehniges.Fleisch-DIM-ACC</ta>
            <ta e="T156" id="Seg_7161" s="T155">finden-EP-MED-CVB.SEQ</ta>
            <ta e="T157" id="Seg_7162" s="T156">jenes-3SG-3SG-ACC</ta>
            <ta e="T158" id="Seg_7163" s="T157">kochen-EP-MED-PST1-3SG</ta>
            <ta e="T159" id="Seg_7164" s="T158">kochen-EP-MED-CVB.SEQ</ta>
            <ta e="T160" id="Seg_7165" s="T159">sitzen-PRS.[3SG]</ta>
            <ta e="T161" id="Seg_7166" s="T160">nur</ta>
            <ta e="T162" id="Seg_7167" s="T161">nun</ta>
            <ta e="T163" id="Seg_7168" s="T162">dann</ta>
            <ta e="T164" id="Seg_7169" s="T163">sehen-PST2-3SG</ta>
            <ta e="T165" id="Seg_7170" s="T164">was-ACC</ta>
            <ta e="T166" id="Seg_7171" s="T165">INDEF</ta>
            <ta e="T167" id="Seg_7172" s="T166">jagen-EP-MED-PST2.[3SG]</ta>
            <ta e="T168" id="Seg_7173" s="T167">wieder</ta>
            <ta e="T169" id="Seg_7174" s="T168">Schmetterling.[NOM]</ta>
            <ta e="T170" id="Seg_7175" s="T169">dann</ta>
            <ta e="T171" id="Seg_7176" s="T170">kürzlich.[NOM]</ta>
            <ta e="T172" id="Seg_7177" s="T171">mit.Krücke</ta>
            <ta e="T173" id="Seg_7178" s="T172">alter.Mann.[NOM]</ta>
            <ta e="T174" id="Seg_7179" s="T173">und</ta>
            <ta e="T175" id="Seg_7180" s="T174">Nahrung-3SG.[NOM]</ta>
            <ta e="T176" id="Seg_7181" s="T175">zu.Ende.gehen-PTCP.PST</ta>
            <ta e="T177" id="Seg_7182" s="T176">sein-PST1-3SG</ta>
            <ta e="T178" id="Seg_7183" s="T177">ganz</ta>
            <ta e="T179" id="Seg_7184" s="T178">doch</ta>
            <ta e="T180" id="Seg_7185" s="T179">sitzen-PRS.[3SG]</ta>
            <ta e="T181" id="Seg_7186" s="T180">wissen-RECP/COLL-EP-NEG.[3SG]</ta>
            <ta e="T182" id="Seg_7187" s="T181">3SG-DAT/LOC</ta>
            <ta e="T183" id="Seg_7188" s="T182">doch</ta>
            <ta e="T184" id="Seg_7189" s="T183">kommen-NEG-IMP.3SG</ta>
            <ta e="T185" id="Seg_7190" s="T184">3SG.[NOM]</ta>
            <ta e="T186" id="Seg_7191" s="T185">selbst-1SG.[NOM]</ta>
            <ta e="T187" id="Seg_7192" s="T186">EMPH</ta>
            <ta e="T188" id="Seg_7193" s="T187">Jagd-VBZ-CVB.SEQ</ta>
            <ta e="T189" id="Seg_7194" s="T188">leben-CVB.SEQ</ta>
            <ta e="T190" id="Seg_7195" s="T189">eins</ta>
            <ta e="T191" id="Seg_7196" s="T190">was-INTNS.[NOM]</ta>
            <ta e="T192" id="Seg_7197" s="T191">INDEF</ta>
            <ta e="T193" id="Seg_7198" s="T192">denken-PRS.[3SG]</ta>
            <ta e="T194" id="Seg_7199" s="T193">Schmetterling.[NOM]</ta>
            <ta e="T195" id="Seg_7200" s="T194">nur</ta>
            <ta e="T196" id="Seg_7201" s="T195">leben-PRS.[3SG]</ta>
            <ta e="T197" id="Seg_7202" s="T196">dieses</ta>
            <ta e="T198" id="Seg_7203" s="T197">leben-TEMP-3SG</ta>
            <ta e="T199" id="Seg_7204" s="T198">mit.Krücke</ta>
            <ta e="T200" id="Seg_7205" s="T199">alter.Mann.[NOM]</ta>
            <ta e="T201" id="Seg_7206" s="T200">kommen-PST2.[3SG]</ta>
            <ta e="T202" id="Seg_7207" s="T201">gehen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T203" id="Seg_7208" s="T202">sehen-PST1-3SG</ta>
            <ta e="T204" id="Seg_7209" s="T203">Freund-3SG-ACC</ta>
            <ta e="T205" id="Seg_7210" s="T204">oh.nein</ta>
            <ta e="T206" id="Seg_7211" s="T205">mit.Krücke</ta>
            <ta e="T207" id="Seg_7212" s="T206">alter.Mann.[NOM]</ta>
            <ta e="T208" id="Seg_7213" s="T207">gehen-PTCP.PRS</ta>
            <ta e="T209" id="Seg_7214" s="T208">sein-PST2.[3SG]</ta>
            <ta e="T210" id="Seg_7215" s="T209">Schmetterling.[NOM]</ta>
            <ta e="T211" id="Seg_7216" s="T210">denken-PRS.[3SG]</ta>
            <ta e="T212" id="Seg_7217" s="T785">Haus-3SG-ACC</ta>
            <ta e="T213" id="Seg_7218" s="T212">verriegeln-EP-MED-CVB.SEQ</ta>
            <ta e="T214" id="Seg_7219" s="T213">schließen-CVB.SEQ</ta>
            <ta e="T216" id="Seg_7220" s="T214">werfen-PST2.[3SG]</ta>
            <ta e="T217" id="Seg_7221" s="T216">doch</ta>
            <ta e="T218" id="Seg_7222" s="T217">kommen-PRS.[3SG]</ta>
            <ta e="T219" id="Seg_7223" s="T218">nur</ta>
            <ta e="T220" id="Seg_7224" s="T219">ah</ta>
            <ta e="T223" id="Seg_7225" s="T222">Schmetterling-3SG-DAT/LOC</ta>
            <ta e="T224" id="Seg_7226" s="T223">kommen-PRS.[3SG]</ta>
            <ta e="T225" id="Seg_7227" s="T224">Schmetterling.[NOM]</ta>
            <ta e="T226" id="Seg_7228" s="T225">Freund-VBZ.[IMP.2SG]</ta>
            <ta e="T227" id="Seg_7229" s="T226">Schmetterling.[NOM]</ta>
            <ta e="T228" id="Seg_7230" s="T227">Freund-VBZ.[IMP.2SG]</ta>
            <ta e="T229" id="Seg_7231" s="T228">2SG.[NOM]</ta>
            <ta e="T230" id="Seg_7232" s="T229">geben.[IMP.2SG]</ta>
            <ta e="T231" id="Seg_7233" s="T230">INDEF</ta>
            <ta e="T232" id="Seg_7234" s="T786">sehniges.Fleisch-DIM-PART</ta>
            <ta e="T233" id="Seg_7235" s="T232">werfen-CVB.SEQ</ta>
            <ta e="T234" id="Seg_7236" s="T233">zu.Hilfe.kommen.[IMP.2SG]</ta>
            <ta e="T235" id="Seg_7237" s="T234">Schmetterling.[NOM]</ta>
            <ta e="T237" id="Seg_7238" s="T235">Freund-VBZ.[IMP.2SG]</ta>
            <ta e="T238" id="Seg_7239" s="T237">%%-CVB.SEQ</ta>
            <ta e="T239" id="Seg_7240" s="T238">Tag-1SG-DAT/LOC</ta>
            <ta e="T240" id="Seg_7241" s="T239">stehen-PST1-3SG</ta>
            <ta e="T241" id="Seg_7242" s="T240">Jahr-1SG-ACC</ta>
            <ta e="T242" id="Seg_7243" s="T241">%%</ta>
            <ta e="T243" id="Seg_7244" s="T242">Schmetterling.[NOM]</ta>
            <ta e="T244" id="Seg_7245" s="T243">Freund-VBZ.[IMP.2SG]</ta>
            <ta e="T245" id="Seg_7246" s="T244">Schmetterling.[NOM]</ta>
            <ta e="T246" id="Seg_7247" s="T245">Freund-VBZ.[IMP.2SG]</ta>
            <ta e="T247" id="Seg_7248" s="T246">sterben-PST1-1SG</ta>
            <ta e="T248" id="Seg_7249" s="T247">bleiben-FUT-1SG</ta>
            <ta e="T249" id="Seg_7250" s="T248">eins-ACC</ta>
            <ta e="T250" id="Seg_7251" s="T249">singen-CVB.SIM</ta>
            <ta e="T251" id="Seg_7252" s="T250">sitzen-PST2.[3SG]</ta>
            <ta e="T252" id="Seg_7253" s="T251">ganz</ta>
            <ta e="T253" id="Seg_7254" s="T252">nun</ta>
            <ta e="T254" id="Seg_7255" s="T253">gehen.[IMP.2SG]-gehen.[IMP.2SG]</ta>
            <ta e="T255" id="Seg_7256" s="T254">eins</ta>
            <ta e="T256" id="Seg_7257" s="T255">INDEF</ta>
            <ta e="T257" id="Seg_7258" s="T256">eins</ta>
            <ta e="T258" id="Seg_7259" s="T257">was-ACC</ta>
            <ta e="T259" id="Seg_7260" s="T258">%%-DAT/LOC</ta>
            <ta e="T260" id="Seg_7261" s="T259">haften-EP-PTCP.PST</ta>
            <ta e="T262" id="Seg_7262" s="T261">trocken.werden-PTCP.PST</ta>
            <ta e="T263" id="Seg_7263" s="T262">sehniges.Fleisch-DIM-ACC</ta>
            <ta e="T264" id="Seg_7264" s="T263">werfen-CVB.SEQ</ta>
            <ta e="T265" id="Seg_7265" s="T264">werfen-PST1-3SG</ta>
            <ta e="T266" id="Seg_7266" s="T265">gehen.[IMP.2SG]</ta>
            <ta e="T267" id="Seg_7267" s="T266">sagen-PRS.[3SG]</ta>
            <ta e="T268" id="Seg_7268" s="T267">jenes-3SG-3SG.[NOM]</ta>
            <ta e="T269" id="Seg_7269" s="T268">mit.Krücke</ta>
            <ta e="T270" id="Seg_7270" s="T269">gehen-CVB.SEQ</ta>
            <ta e="T271" id="Seg_7271" s="T270">bleiben-PST1-3SG</ta>
            <ta e="T272" id="Seg_7272" s="T271">gehen.[IMP.2SG]</ta>
            <ta e="T273" id="Seg_7273" s="T272">Haus-3SG-DAT/LOC</ta>
            <ta e="T274" id="Seg_7274" s="T273">gehen.[IMP.2SG]</ta>
            <ta e="T275" id="Seg_7275" s="T274">Haus-2SG-DAT/LOC</ta>
            <ta e="T276" id="Seg_7276" s="T275">sagen-PRS.[3SG]</ta>
            <ta e="T277" id="Seg_7277" s="T276">mehr</ta>
            <ta e="T278" id="Seg_7278" s="T277">geben-NEG-1SG</ta>
            <ta e="T279" id="Seg_7279" s="T278">2SG-DAT/LOC</ta>
            <ta e="T280" id="Seg_7280" s="T279">EMPH</ta>
            <ta e="T281" id="Seg_7281" s="T280">gehen-CVB.SEQ</ta>
            <ta e="T282" id="Seg_7282" s="T281">bleiben-PST1-3SG</ta>
            <ta e="T283" id="Seg_7283" s="T282">Mensch.[NOM]</ta>
            <ta e="T284" id="Seg_7284" s="T283">gehen-CVB.SEQ</ta>
            <ta e="T285" id="Seg_7285" s="T284">so.viel.[NOM]</ta>
            <ta e="T286" id="Seg_7286" s="T285">essen-FUT-3SG-Q</ta>
            <ta e="T287" id="Seg_7287" s="T286">3SG.[NOM]</ta>
            <ta e="T288" id="Seg_7288" s="T287">dann</ta>
            <ta e="T289" id="Seg_7289" s="T288">jenes-dieses</ta>
            <ta e="T290" id="Seg_7290" s="T289">doch</ta>
            <ta e="T291" id="Seg_7291" s="T290">Blut-3SG-ACC</ta>
            <ta e="T292" id="Seg_7292" s="T291">leer.[NOM]</ta>
            <ta e="T293" id="Seg_7293" s="T292">leer</ta>
            <ta e="T294" id="Seg_7294" s="T293">Suppe-3SG-ACC</ta>
            <ta e="T295" id="Seg_7295" s="T294">nur</ta>
            <ta e="T298" id="Seg_7296" s="T297">kochen-EP-MED-CVB.SEQ</ta>
            <ta e="T299" id="Seg_7297" s="T298">essen-PST2.[3SG]</ta>
            <ta e="T300" id="Seg_7298" s="T299">EMPH</ta>
            <ta e="T301" id="Seg_7299" s="T300">was.[NOM]</ta>
            <ta e="T302" id="Seg_7300" s="T301">NEG</ta>
            <ta e="T303" id="Seg_7301" s="T302">finden-NEG.[3SG]</ta>
            <ta e="T304" id="Seg_7302" s="T303">und</ta>
            <ta e="T305" id="Seg_7303" s="T304">und.so.weiter-NEG.[3SG]</ta>
            <ta e="T306" id="Seg_7304" s="T305">EMPH</ta>
            <ta e="T307" id="Seg_7305" s="T306">wie.viel</ta>
            <ta e="T308" id="Seg_7306" s="T307">INDEF</ta>
            <ta e="T309" id="Seg_7307" s="T308">Tag-ACC</ta>
            <ta e="T310" id="Seg_7308" s="T309">liegen-PST2-3SG</ta>
            <ta e="T311" id="Seg_7309" s="T310">EMPH</ta>
            <ta e="T312" id="Seg_7310" s="T311">nur</ta>
            <ta e="T313" id="Seg_7311" s="T312">EMPH</ta>
            <ta e="T314" id="Seg_7312" s="T313">go-CVB.SEQ</ta>
            <ta e="T315" id="Seg_7313" s="T314">hinausgehen-PTCP.PRS</ta>
            <ta e="T316" id="Seg_7314" s="T315">werden-PST2.[3SG]</ta>
            <ta e="T317" id="Seg_7315" s="T316">Ende-EP-ABL</ta>
            <ta e="T318" id="Seg_7316" s="T317">hungern-CVB.SEQ</ta>
            <ta e="T319" id="Seg_7317" s="T318">mit.Krücke</ta>
            <ta e="T320" id="Seg_7318" s="T319">alter.Mann.[NOM]</ta>
            <ta e="T321" id="Seg_7319" s="T320">wieder</ta>
            <ta e="T322" id="Seg_7320" s="T321">Schmetterling-3SG-DAT/LOC</ta>
            <ta e="T323" id="Seg_7321" s="T322">gehen-CAP.[3SG]</ta>
            <ta e="T324" id="Seg_7322" s="T323">kommen-CVB.SEQ</ta>
            <ta e="T325" id="Seg_7323" s="T324">Schmetterling-3SG-DAT/LOC</ta>
            <ta e="T326" id="Seg_7324" s="T325">wieder</ta>
            <ta e="T327" id="Seg_7325" s="T326">singen-PST2.[3SG]</ta>
            <ta e="T328" id="Seg_7326" s="T327">Schmetterling.[NOM]</ta>
            <ta e="T329" id="Seg_7327" s="T328">Freund-VBZ.[IMP.2SG]</ta>
            <ta e="T330" id="Seg_7328" s="T329">Schmetterling.[NOM]</ta>
            <ta e="T331" id="Seg_7329" s="T330">Freund-VBZ.[IMP.2SG]</ta>
            <ta e="T332" id="Seg_7330" s="T787">sterben-PST1-1SG</ta>
            <ta e="T333" id="Seg_7331" s="T332">bleiben-FUT-1SG</ta>
            <ta e="T334" id="Seg_7332" s="T333">sterben-PST1-1SG</ta>
            <ta e="T336" id="Seg_7333" s="T334">bleiben-FUT-1SG</ta>
            <ta e="T337" id="Seg_7334" s="T336">Schmetterling.[NOM]</ta>
            <ta e="T338" id="Seg_7335" s="T337">Freund-VBZ.[IMP.2SG]</ta>
            <ta e="T339" id="Seg_7336" s="T338">eins</ta>
            <ta e="T340" id="Seg_7337" s="T339">INDEF</ta>
            <ta e="T342" id="Seg_7338" s="T341">INDEF</ta>
            <ta e="T343" id="Seg_7339" s="T342">sehniges.Fleisch-DIM-PART</ta>
            <ta e="T344" id="Seg_7340" s="T343">werfen-CVB.SEQ</ta>
            <ta e="T345" id="Seg_7341" s="T344">zu.Hilfe.kommen.[IMP.2SG]</ta>
            <ta e="T346" id="Seg_7342" s="T345">Schmetterling.[NOM]</ta>
            <ta e="T347" id="Seg_7343" s="T346">Freund-VBZ.[IMP.2SG]</ta>
            <ta e="T348" id="Seg_7344" s="T347">Schmetterling.[NOM]</ta>
            <ta e="T349" id="Seg_7345" s="T348">Freund-VBZ.[IMP.2SG]</ta>
            <ta e="T350" id="Seg_7346" s="T349">%%-CVB.SEQ</ta>
            <ta e="T351" id="Seg_7347" s="T350">vergessen-CVB.SEQ</ta>
            <ta e="T352" id="Seg_7348" s="T788">sein-PST1-3SG</ta>
            <ta e="T353" id="Seg_7349" s="T352">finden-FUT.[3SG]</ta>
            <ta e="T354" id="Seg_7350" s="T353">Schmetterling.[NOM]</ta>
            <ta e="T355" id="Seg_7351" s="T354">Freund-VBZ.[IMP.2SG]</ta>
            <ta e="T356" id="Seg_7352" s="T355">%%-CVB.SEQ</ta>
            <ta e="T357" id="Seg_7353" s="T356">sehniges.Fleisch-PART</ta>
            <ta e="T358" id="Seg_7354" s="T357">holen.[IMP.2SG]</ta>
            <ta e="T360" id="Seg_7355" s="T358">singen-EP-MED-PRS-1SG</ta>
            <ta e="T361" id="Seg_7356" s="T360">EMPH</ta>
            <ta e="T362" id="Seg_7357" s="T361">eins</ta>
            <ta e="T363" id="Seg_7358" s="T362">NEG</ta>
            <ta e="T364" id="Seg_7359" s="T363">sehniges.Fleisch-POSS</ta>
            <ta e="T365" id="Seg_7360" s="T364">NEG-1SG</ta>
            <ta e="T366" id="Seg_7361" s="T365">gehen.[IMP.2SG]</ta>
            <ta e="T367" id="Seg_7362" s="T366">gehen.[IMP.2SG]</ta>
            <ta e="T368" id="Seg_7363" s="T367">sagen-PST2.[3SG]</ta>
            <ta e="T369" id="Seg_7364" s="T368">zurück</ta>
            <ta e="T370" id="Seg_7365" s="T369">jagen-CVB.SEQ</ta>
            <ta e="T371" id="Seg_7366" s="T370">schicken-PST2.[3SG]</ta>
            <ta e="T372" id="Seg_7367" s="T371">was-INTNS-ACC</ta>
            <ta e="T373" id="Seg_7368" s="T372">NEG</ta>
            <ta e="T374" id="Seg_7369" s="T373">geben-PST2.NEG.[3SG]</ta>
            <ta e="T375" id="Seg_7370" s="T374">wohin</ta>
            <ta e="T376" id="Seg_7371" s="T375">gehen-FUT.[3SG]-Q</ta>
            <ta e="T377" id="Seg_7372" s="T376">draußen</ta>
            <ta e="T378" id="Seg_7373" s="T377">dann</ta>
            <ta e="T379" id="Seg_7374" s="T378">frieren-PTCP.PRS</ta>
            <ta e="T380" id="Seg_7375" s="T379">sein-PST1-3SG</ta>
            <ta e="T381" id="Seg_7376" s="T380">wohin</ta>
            <ta e="T382" id="Seg_7377" s="T381">Haus-DAT/LOC</ta>
            <ta e="T383" id="Seg_7378" s="T382">NEG</ta>
            <ta e="T384" id="Seg_7379" s="T383">hineingehen-CAUS-NEG.[3SG]</ta>
            <ta e="T385" id="Seg_7380" s="T384">Scham.[NOM]</ta>
            <ta e="T386" id="Seg_7381" s="T385">doch</ta>
            <ta e="T387" id="Seg_7382" s="T386">gehen-PRS.[3SG]</ta>
            <ta e="T388" id="Seg_7383" s="T387">dieses</ta>
            <ta e="T389" id="Seg_7384" s="T388">Mensch.[NOM]</ta>
            <ta e="T390" id="Seg_7385" s="T389">gehen-PST2-3SG</ta>
            <ta e="T391" id="Seg_7386" s="T390">kaum</ta>
            <ta e="T392" id="Seg_7387" s="T391">dann</ta>
            <ta e="T393" id="Seg_7388" s="T392">ankommen-CVB.SEQ</ta>
            <ta e="T394" id="Seg_7389" s="T393">gehen-TEMP-3SG</ta>
            <ta e="T397" id="Seg_7390" s="T396">Haus-3SG-GEN</ta>
            <ta e="T398" id="Seg_7391" s="T397">Platz.neben-DAT/LOC</ta>
            <ta e="T399" id="Seg_7392" s="T398">ankommen-PRS.[3SG]</ta>
            <ta e="T400" id="Seg_7393" s="T399">sehen-PST2-3SG</ta>
            <ta e="T401" id="Seg_7394" s="T400">Kalb-PROPR</ta>
            <ta e="T402" id="Seg_7395" s="T401">Rentierkuh.[NOM]</ta>
            <ta e="T403" id="Seg_7396" s="T402">gehen-PRS.[3SG]</ta>
            <ta e="T404" id="Seg_7397" s="T403">Haus-3SG-DAT/LOC</ta>
            <ta e="T405" id="Seg_7398" s="T404">was.für.ein</ta>
            <ta e="T406" id="Seg_7399" s="T405">kommen-NMNZ-ABL</ta>
            <ta e="T407" id="Seg_7400" s="T406">kommen-PST2-3SG</ta>
            <ta e="T408" id="Seg_7401" s="T407">Q</ta>
            <ta e="T409" id="Seg_7402" s="T408">wissen-NEG.[3SG]</ta>
            <ta e="T410" id="Seg_7403" s="T409">Rentier-ACC</ta>
            <ta e="T411" id="Seg_7404" s="T410">3SG.[NOM]</ta>
            <ta e="T412" id="Seg_7405" s="T789">Rentier.[NOM]</ta>
            <ta e="T413" id="Seg_7406" s="T412">gehen-PRS.[3SG]</ta>
            <ta e="T414" id="Seg_7407" s="T413">nur</ta>
            <ta e="T416" id="Seg_7408" s="T414">kommen-PST2.[3SG]</ta>
            <ta e="T417" id="Seg_7409" s="T416">nach.oben</ta>
            <ta e="T418" id="Seg_7410" s="T417">3SG.[NOM]</ta>
            <ta e="T419" id="Seg_7411" s="T418">sterben-PTCP.FUT</ta>
            <ta e="T420" id="Seg_7412" s="T419">Bestimmung-3SG-DAT/LOC</ta>
            <ta e="T423" id="Seg_7413" s="T422">wieder.aufleben-PTCP.FUT</ta>
            <ta e="T424" id="Seg_7414" s="T423">Bestimmung-3SG-DAT/LOC</ta>
            <ta e="T425" id="Seg_7415" s="T424">Kalb-PROPR</ta>
            <ta e="T426" id="Seg_7416" s="T425">Rentierkuh-3SG.[NOM]</ta>
            <ta e="T427" id="Seg_7417" s="T426">selbst-3SG-DAT/LOC</ta>
            <ta e="T428" id="Seg_7418" s="T427">kommen-CVB.SEQ</ta>
            <ta e="T429" id="Seg_7419" s="T428">lecken-CVB.SIM</ta>
            <ta e="T430" id="Seg_7420" s="T429">gehen-EP-PST2-3PL</ta>
            <ta e="T431" id="Seg_7421" s="T430">3SG-ACC</ta>
            <ta e="T432" id="Seg_7422" s="T431">pinkeln-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T433" id="Seg_7423" s="T432">Inneres-3SG-DAT/LOC</ta>
            <ta e="T434" id="Seg_7424" s="T433">kommen-PST2-3SG</ta>
            <ta e="T435" id="Seg_7425" s="T434">und</ta>
            <ta e="T436" id="Seg_7426" s="T435">Kalb-DIM-3SG-ACC</ta>
            <ta e="T437" id="Seg_7427" s="T436">fangen-CVB.SEQ</ta>
            <ta e="T438" id="Seg_7428" s="T437">nehmen-CVB.SEQ</ta>
            <ta e="T439" id="Seg_7429" s="T438">Messer-DIM-ACC</ta>
            <ta e="T440" id="Seg_7430" s="T439">nehmen-PST1-3SG</ta>
            <ta e="T441" id="Seg_7431" s="T440">und</ta>
            <ta e="T442" id="Seg_7432" s="T441">kort</ta>
            <ta e="T443" id="Seg_7433" s="T442">machen-CAUS-CVB.SEQ</ta>
            <ta e="T444" id="Seg_7434" s="T443">töten-CVB.SEQ</ta>
            <ta e="T445" id="Seg_7435" s="T444">werfen-PST2.[3SG]</ta>
            <ta e="T446" id="Seg_7436" s="T445">Mutter-3SG.[NOM]</ta>
            <ta e="T447" id="Seg_7437" s="T446">gehen-PRS.[3SG]</ta>
            <ta e="T448" id="Seg_7438" s="T447">nun</ta>
            <ta e="T449" id="Seg_7439" s="T448">dieses-3SG-3SG-ACC</ta>
            <ta e="T450" id="Seg_7440" s="T449">was.[NOM]</ta>
            <ta e="T451" id="Seg_7441" s="T450">was.[NOM]</ta>
            <ta e="T454" id="Seg_7442" s="T453">Haut.abziehen-EP-MED-PST2.[3SG]</ta>
            <ta e="T455" id="Seg_7443" s="T454">Mensch.[NOM]</ta>
            <ta e="T456" id="Seg_7444" s="T455">roh-VBZ-CVB.SIM</ta>
            <ta e="T457" id="Seg_7445" s="T456">kochen-EP-CAUS-CVB.SEQ</ta>
            <ta e="T458" id="Seg_7446" s="T457">kochen-EP-MED-CVB.SEQ</ta>
            <ta e="T459" id="Seg_7447" s="T458">essen-CVB.SEQ</ta>
            <ta e="T460" id="Seg_7448" s="T459">werfen-PST2.[3SG]</ta>
            <ta e="T461" id="Seg_7449" s="T460">doch</ta>
            <ta e="T462" id="Seg_7450" s="T461">Auge-3SG.[NOM]</ta>
            <ta e="T463" id="Seg_7451" s="T462">hell.werden-PST2.[3SG]</ta>
            <ta e="T464" id="Seg_7452" s="T463">dort</ta>
            <ta e="T465" id="Seg_7453" s="T464">oh</ta>
            <ta e="T466" id="Seg_7454" s="T465">doch</ta>
            <ta e="T467" id="Seg_7455" s="T466">jetzt</ta>
            <ta e="T468" id="Seg_7456" s="T467">EMPH</ta>
            <ta e="T469" id="Seg_7457" s="T468">retten-EP-REFL-PST1-1SG</ta>
            <ta e="T470" id="Seg_7458" s="T469">Schmetterling-DAT/LOC</ta>
            <ta e="T471" id="Seg_7459" s="T470">gehen-NEG-1SG</ta>
            <ta e="T472" id="Seg_7460" s="T471">und.so.weiter-NEG-1SG</ta>
            <ta e="T473" id="Seg_7461" s="T472">Nahrung-VBZ-PST1-1SG</ta>
            <ta e="T474" id="Seg_7462" s="T473">EMPH</ta>
            <ta e="T475" id="Seg_7463" s="T474">denken-PRS.[3SG]</ta>
            <ta e="T476" id="Seg_7464" s="T475">Nahrung-VBZ-PTCP.PST</ta>
            <ta e="T477" id="Seg_7465" s="T476">Mensch.[NOM]</ta>
            <ta e="T478" id="Seg_7466" s="T477">kochen-EP-MED-CVB.SEQ</ta>
            <ta e="T479" id="Seg_7467" s="T478">nachdem</ta>
            <ta e="T480" id="Seg_7468" s="T479">essen-PRS.[3SG]</ta>
            <ta e="T481" id="Seg_7469" s="T480">ok</ta>
            <ta e="T482" id="Seg_7470" s="T481">doch</ta>
            <ta e="T483" id="Seg_7471" s="T482">wie.viel</ta>
            <ta e="T484" id="Seg_7472" s="T483">Tag-ACC</ta>
            <ta e="T485" id="Seg_7473" s="T484">leben-PST2-3SG</ta>
            <ta e="T486" id="Seg_7474" s="T485">Q</ta>
            <ta e="T487" id="Seg_7475" s="T486">Rentier-PROPR</ta>
            <ta e="T488" id="Seg_7476" s="T487">Rentier-3SG-ACC</ta>
            <ta e="T489" id="Seg_7477" s="T488">EMPH</ta>
            <ta e="T490" id="Seg_7478" s="T489">ergreifen-EP-PST2.[3SG]</ta>
            <ta e="T491" id="Seg_7479" s="T490">Rentierkuh-3SG-ACC</ta>
            <ta e="T492" id="Seg_7480" s="T491">Rentierkuh-PROPR.[NOM]</ta>
            <ta e="T493" id="Seg_7481" s="T492">was.[NOM]</ta>
            <ta e="T494" id="Seg_7482" s="T493">sein-FUT.[3SG]-Q</ta>
            <ta e="T495" id="Seg_7483" s="T494">jetzt</ta>
            <ta e="T496" id="Seg_7484" s="T495">oh</ta>
            <ta e="T497" id="Seg_7485" s="T496">dieses</ta>
            <ta e="T498" id="Seg_7486" s="T497">liegen-TEMP-3SG</ta>
            <ta e="T499" id="Seg_7487" s="T498">Schmetterling-3SG-GEN</ta>
            <ta e="T500" id="Seg_7488" s="T499">Stimme-3SG.[NOM]</ta>
            <ta e="T501" id="Seg_7489" s="T500">kommen-PST1-3SG</ta>
            <ta e="T502" id="Seg_7490" s="T501">doch</ta>
            <ta e="T503" id="Seg_7491" s="T502">nur</ta>
            <ta e="T504" id="Seg_7492" s="T503">sehen-PST2-3SG</ta>
            <ta e="T505" id="Seg_7493" s="T504">doch</ta>
            <ta e="T506" id="Seg_7494" s="T505">1SG.[NOM]</ta>
            <ta e="T507" id="Seg_7495" s="T506">jetzt</ta>
            <ta e="T508" id="Seg_7496" s="T507">so</ta>
            <ta e="T509" id="Seg_7497" s="T508">machen-FUT-1SG</ta>
            <ta e="T510" id="Seg_7498" s="T509">warte</ta>
            <ta e="T511" id="Seg_7499" s="T510">kommen-IMP.3SG</ta>
            <ta e="T512" id="Seg_7500" s="T511">nur</ta>
            <ta e="T513" id="Seg_7501" s="T512">denken-PST2.[3SG]</ta>
            <ta e="T514" id="Seg_7502" s="T513">1SG-DAT/LOC</ta>
            <ta e="T515" id="Seg_7503" s="T514">Nahrung.[NOM]</ta>
            <ta e="T516" id="Seg_7504" s="T515">Schmetterling.[NOM]</ta>
            <ta e="T517" id="Seg_7505" s="T516">was-VBZ-CVB.SEQ</ta>
            <ta e="T518" id="Seg_7506" s="T517">kaum</ta>
            <ta e="T519" id="Seg_7507" s="T518">gehen-PRS.[3SG]</ta>
            <ta e="T520" id="Seg_7508" s="T519">ganz</ta>
            <ta e="T521" id="Seg_7509" s="T520">hungern-CVB.SEQ</ta>
            <ta e="T522" id="Seg_7510" s="T521">kaum</ta>
            <ta e="T523" id="Seg_7511" s="T522">go-CVB.SEQ</ta>
            <ta e="T524" id="Seg_7512" s="T523">gehen-PRS.[3SG]</ta>
            <ta e="T525" id="Seg_7513" s="T524">kommen-PST2.[3SG]</ta>
            <ta e="T526" id="Seg_7514" s="T525">und</ta>
            <ta e="T527" id="Seg_7515" s="T526">kommen-PST2.[3SG]</ta>
            <ta e="T528" id="Seg_7516" s="T527">kommen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T529" id="Seg_7517" s="T528">mit</ta>
            <ta e="T530" id="Seg_7518" s="T529">Haus-3SG.[NOM]</ta>
            <ta e="T531" id="Seg_7519" s="T530">verriegeln-CVB.SEQ</ta>
            <ta e="T532" id="Seg_7520" s="T531">verriegeln-EP-MED-CVB.SEQ</ta>
            <ta e="T533" id="Seg_7521" s="T532">werfen-PST2.[3SG]</ta>
            <ta e="T534" id="Seg_7522" s="T533">doch</ta>
            <ta e="T535" id="Seg_7523" s="T534">3SG-ACC</ta>
            <ta e="T536" id="Seg_7524" s="T535">bestrafen-VBZ-FUT-1SG</ta>
            <ta e="T537" id="Seg_7525" s="T536">vor.Kurzem</ta>
            <ta e="T538" id="Seg_7526" s="T537">1SG-ACC</ta>
            <ta e="T539" id="Seg_7527" s="T538">wieder</ta>
            <ta e="T540" id="Seg_7528" s="T539">3SG.[NOM]</ta>
            <ta e="T541" id="Seg_7529" s="T540">quälen-PST2-3SG</ta>
            <ta e="T542" id="Seg_7530" s="T541">so</ta>
            <ta e="T543" id="Seg_7531" s="T542">mit.Krücke</ta>
            <ta e="T544" id="Seg_7532" s="T543">alter.Mann.[NOM]</ta>
            <ta e="T545" id="Seg_7533" s="T544">mit.Krücke</ta>
            <ta e="T546" id="Seg_7534" s="T545">alter.Mann.[NOM]</ta>
            <ta e="T547" id="Seg_7535" s="T546">eins</ta>
            <ta e="T548" id="Seg_7536" s="T790">INDEF</ta>
            <ta e="T549" id="Seg_7537" s="T548">sehniges.Fleisch-DIM-PART</ta>
            <ta e="T550" id="Seg_7538" s="T549">geben-CVB.SEQ</ta>
            <ta e="T551" id="Seg_7539" s="T550">zu.Hilfe.kommen.[IMP.2SG]</ta>
            <ta e="T552" id="Seg_7540" s="T551">mit.Krücke</ta>
            <ta e="T553" id="Seg_7541" s="T552">alter.Mann.[NOM]</ta>
            <ta e="T554" id="Seg_7542" s="T553">sterben-PST1-1SG</ta>
            <ta e="T555" id="Seg_7543" s="T554">bleiben-PST1-1SG</ta>
            <ta e="T556" id="Seg_7544" s="T555">sterben-PST1-1SG</ta>
            <ta e="T560" id="Seg_7545" s="T558">bleiben-PST1-1SG</ta>
            <ta e="T561" id="Seg_7546" s="T560">Tag-1SG-DAT/LOC</ta>
            <ta e="T562" id="Seg_7547" s="T561">stehen-PST1-3SG</ta>
            <ta e="T563" id="Seg_7548" s="T562">leblos</ta>
            <ta e="T564" id="Seg_7549" s="T563">Mensch-3SG.[NOM]</ta>
            <ta e="T565" id="Seg_7550" s="T564">sein-TEMP-2SG</ta>
            <ta e="T566" id="Seg_7551" s="T565">mit.Krücke</ta>
            <ta e="T567" id="Seg_7552" s="T566">alter.Mann.[NOM]</ta>
            <ta e="T568" id="Seg_7553" s="T567">mit.Krücke</ta>
            <ta e="T569" id="Seg_7554" s="T568">alter.Mann.[NOM]</ta>
            <ta e="T570" id="Seg_7555" s="T569">singen-EP-MED-CVB.SIM</ta>
            <ta e="T571" id="Seg_7556" s="T570">liegen-PST2.[3SG]</ta>
            <ta e="T572" id="Seg_7557" s="T571">was-ACC</ta>
            <ta e="T573" id="Seg_7558" s="T572">NEG</ta>
            <ta e="T574" id="Seg_7559" s="T573">geben-PST2.NEG.[3SG]</ta>
            <ta e="T575" id="Seg_7560" s="T574">leer</ta>
            <ta e="T576" id="Seg_7561" s="T575">was.[NOM]</ta>
            <ta e="T577" id="Seg_7562" s="T576">INDEF</ta>
            <ta e="T578" id="Seg_7563" s="T577">dieses</ta>
            <ta e="T579" id="Seg_7564" s="T578">Rentierkalb-3SG-GEN</ta>
            <ta e="T580" id="Seg_7565" s="T579">eigen-3SG-GEN</ta>
            <ta e="T581" id="Seg_7566" s="T580">Huf-3SG.[NOM]</ta>
            <ta e="T582" id="Seg_7567" s="T581">Huf-3PL-ACC</ta>
            <ta e="T583" id="Seg_7568" s="T582">was-VBZ-EP-MED-PST2.[3SG]</ta>
            <ta e="T584" id="Seg_7569" s="T583">%%-3SG-ACC</ta>
            <ta e="T585" id="Seg_7570" s="T584">mit</ta>
            <ta e="T586" id="Seg_7571" s="T585">Huf-3SG.[NOM]</ta>
            <ta e="T587" id="Seg_7572" s="T586">Q</ta>
            <ta e="T588" id="Seg_7573" s="T587">was.[NOM]</ta>
            <ta e="T589" id="Seg_7574" s="T588">Q</ta>
            <ta e="T590" id="Seg_7575" s="T589">jenes-ACC</ta>
            <ta e="T593" id="Seg_7576" s="T592">%%-PROPR</ta>
            <ta e="T594" id="Seg_7577" s="T593">gehen-CVB.SEQ</ta>
            <ta e="T595" id="Seg_7578" s="T594">jenes-ACC</ta>
            <ta e="T596" id="Seg_7579" s="T595">werfen-CVB.SEQ</ta>
            <ta e="T597" id="Seg_7580" s="T596">gehen.[IMP.2SG]</ta>
            <ta e="T598" id="Seg_7581" s="T597">essen.[IMP.2SG]</ta>
            <ta e="T599" id="Seg_7582" s="T598">sagen-CVB.SIM</ta>
            <ta e="T600" id="Seg_7583" s="T599">zwei</ta>
            <ta e="T601" id="Seg_7584" s="T600">Huf-ACC</ta>
            <ta e="T602" id="Seg_7585" s="T601">werfen-CVB.SEQ</ta>
            <ta e="T603" id="Seg_7586" s="T602">werfen-PST2.[3SG]</ta>
            <ta e="T604" id="Seg_7587" s="T603">gehen.[IMP.2SG]</ta>
            <ta e="T605" id="Seg_7588" s="T604">Haus-2SG-DAT/LOC</ta>
            <ta e="T606" id="Seg_7589" s="T605">sagen-PRS.[3SG]</ta>
            <ta e="T607" id="Seg_7590" s="T606">mehr</ta>
            <ta e="T608" id="Seg_7591" s="T607">kommen-EP-NEG.[IMP.2SG]</ta>
            <ta e="T611" id="Seg_7592" s="T610">vor.Kurzem</ta>
            <ta e="T612" id="Seg_7593" s="T611">1SG-ACC</ta>
            <ta e="T613" id="Seg_7594" s="T612">auch</ta>
            <ta e="T614" id="Seg_7595" s="T613">doch</ta>
            <ta e="T615" id="Seg_7596" s="T614">quälen-PST2-EP-2SG</ta>
            <ta e="T616" id="Seg_7597" s="T615">jenes-3SG-3SG.[NOM]</ta>
            <ta e="T617" id="Seg_7598" s="T616">zu.Ende.gehen-FUT.[3SG]</ta>
            <ta e="T618" id="Seg_7599" s="T617">MOD</ta>
            <ta e="T619" id="Seg_7600" s="T618">gehen-CVB.SEQ</ta>
            <ta e="T620" id="Seg_7601" s="T619">bleiben-PST2.[3SG]</ta>
            <ta e="T621" id="Seg_7602" s="T620">doch</ta>
            <ta e="T622" id="Seg_7603" s="T621">gehen-CVB.SEQ</ta>
            <ta e="T623" id="Seg_7604" s="T622">bleiben-PST2.[3SG]</ta>
            <ta e="T624" id="Seg_7605" s="T623">Haus-3SG-DAT/LOC</ta>
            <ta e="T625" id="Seg_7606" s="T624">wieder</ta>
            <ta e="T627" id="Seg_7607" s="T626">gehen-PST2.[3SG]</ta>
            <ta e="T628" id="Seg_7608" s="T627">und</ta>
            <ta e="T629" id="Seg_7609" s="T628">nächster.Morgen-3SG-ACC</ta>
            <ta e="T632" id="Seg_7610" s="T631">wie.viel</ta>
            <ta e="T633" id="Seg_7611" s="T632">INDEF</ta>
            <ta e="T634" id="Seg_7612" s="T633">lange</ta>
            <ta e="T635" id="Seg_7613" s="T634">Tag-ACC</ta>
            <ta e="T636" id="Seg_7614" s="T635">sein-CVB.SEQ</ta>
            <ta e="T637" id="Seg_7615" s="T636">nachdem</ta>
            <ta e="T638" id="Seg_7616" s="T637">wieder</ta>
            <ta e="T639" id="Seg_7617" s="T638">kommen-PRS.[3SG]</ta>
            <ta e="T640" id="Seg_7618" s="T639">doch</ta>
            <ta e="T641" id="Seg_7619" s="T640">ganz</ta>
            <ta e="T642" id="Seg_7620" s="T641">sterben-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T643" id="Seg_7621" s="T642">gehen-PST2.[3SG]</ta>
            <ta e="T644" id="Seg_7622" s="T643">offenbar</ta>
            <ta e="T645" id="Seg_7623" s="T644">Freund-3SG.[NOM]</ta>
            <ta e="T646" id="Seg_7624" s="T645">Schmetterling-3SG.[NOM]</ta>
            <ta e="T647" id="Seg_7625" s="T646">äußerste.Grenze.erreichen-PST2.[3SG]</ta>
            <ta e="T648" id="Seg_7626" s="T647">jagen-TEMP-3SG</ta>
            <ta e="T649" id="Seg_7627" s="T648">und</ta>
            <ta e="T650" id="Seg_7628" s="T649">gehen-NEG.[3SG]</ta>
            <ta e="T651" id="Seg_7629" s="T650">hineingehen-TEMP-3SG</ta>
            <ta e="T652" id="Seg_7630" s="T651">kommen-NEG.[3SG]</ta>
            <ta e="T653" id="Seg_7631" s="T652">oh</ta>
            <ta e="T654" id="Seg_7632" s="T653">doch</ta>
            <ta e="T655" id="Seg_7633" s="T654">wie</ta>
            <ta e="T656" id="Seg_7634" s="T655">NEG</ta>
            <ta e="T657" id="Seg_7635" s="T656">machen-PTCP.FUT-3SG-ACC</ta>
            <ta e="T658" id="Seg_7636" s="T657">sehr</ta>
            <ta e="T659" id="Seg_7637" s="T658">doch</ta>
            <ta e="T660" id="Seg_7638" s="T659">Herz-3SG.[NOM]</ta>
            <ta e="T661" id="Seg_7639" s="T660">Schmerz.[NOM]</ta>
            <ta e="T662" id="Seg_7640" s="T661">krank.sein-PRS.[3SG]</ta>
            <ta e="T663" id="Seg_7641" s="T662">Mensch.[NOM]</ta>
            <ta e="T664" id="Seg_7642" s="T663">Freund-3SG-ACC</ta>
            <ta e="T665" id="Seg_7643" s="T664">bemitleiden-PST1-3SG</ta>
            <ta e="T666" id="Seg_7644" s="T665">EMPH</ta>
            <ta e="T667" id="Seg_7645" s="T666">bemitleiden-CVB.SEQ</ta>
            <ta e="T668" id="Seg_7646" s="T667">wie</ta>
            <ta e="T669" id="Seg_7647" s="T668">NEG</ta>
            <ta e="T670" id="Seg_7648" s="T669">machen-PTCP.FUT-3SG-ACC</ta>
            <ta e="T671" id="Seg_7649" s="T670">sehr</ta>
            <ta e="T672" id="Seg_7650" s="T671">jagen-CAUS-PTCP.FUT-3SG-ACC</ta>
            <ta e="T673" id="Seg_7651" s="T672">und</ta>
            <ta e="T674" id="Seg_7652" s="T791">gehen-CVB.SEQ</ta>
            <ta e="T675" id="Seg_7653" s="T674">und</ta>
            <ta e="T676" id="Seg_7654" s="T675">doch</ta>
            <ta e="T677" id="Seg_7655" s="T676">Haus-3SG-DAT/LOC</ta>
            <ta e="T678" id="Seg_7656" s="T677">ankommen-FUT.[3SG]</ta>
            <ta e="T680" id="Seg_7657" s="T678">NEG</ta>
            <ta e="T681" id="Seg_7658" s="T792">wenn.so</ta>
            <ta e="T682" id="Seg_7659" s="T681">hineingehen.[IMP.2SG]</ta>
            <ta e="T683" id="Seg_7660" s="T682">sagen-PST2.[3SG]</ta>
            <ta e="T684" id="Seg_7661" s="T683">essen.[IMP.2SG]</ta>
            <ta e="T685" id="Seg_7662" s="T684">sich.unterhalten-IMP.1DU</ta>
            <ta e="T686" id="Seg_7663" s="T685">dann</ta>
            <ta e="T688" id="Seg_7664" s="T686">gut-ADVZ</ta>
            <ta e="T689" id="Seg_7665" s="T688">doch</ta>
            <ta e="T690" id="Seg_7666" s="T689">INTNS</ta>
            <ta e="T691" id="Seg_7667" s="T690">tatsächlich</ta>
            <ta e="T692" id="Seg_7668" s="T691">sich.unterhalten-PST2-3PL</ta>
            <ta e="T693" id="Seg_7669" s="T692">na</ta>
            <ta e="T694" id="Seg_7670" s="T693">dann</ta>
            <ta e="T695" id="Seg_7671" s="T694">1PL.[NOM]</ta>
            <ta e="T696" id="Seg_7672" s="T695">gut-ADVZ</ta>
            <ta e="T697" id="Seg_7673" s="T696">sich.unterhalten-IMP.1DU</ta>
            <ta e="T698" id="Seg_7674" s="T697">zusammen</ta>
            <ta e="T699" id="Seg_7675" s="T698">jetzt</ta>
            <ta e="T700" id="Seg_7676" s="T699">zusammen</ta>
            <ta e="T701" id="Seg_7677" s="T700">leben-FUT-1PL</ta>
            <ta e="T702" id="Seg_7678" s="T701">doch</ta>
            <ta e="T703" id="Seg_7679" s="T702">warum</ta>
            <ta e="T704" id="Seg_7680" s="T703">zwei</ta>
            <ta e="T705" id="Seg_7681" s="T704">Seite.[NOM]</ta>
            <ta e="T706" id="Seg_7682" s="T705">leben-PRS-1PL</ta>
            <ta e="T707" id="Seg_7683" s="T706">1PL.[NOM]</ta>
            <ta e="T708" id="Seg_7684" s="T707">sehen-PRS-2SG</ta>
            <ta e="T709" id="Seg_7685" s="T708">1SG.[NOM]</ta>
            <ta e="T710" id="Seg_7686" s="T709">Rentier-REFL/MED-PST1-1SG</ta>
            <ta e="T711" id="Seg_7687" s="T710">noch</ta>
            <ta e="T712" id="Seg_7688" s="T711">2SG.[NOM]</ta>
            <ta e="T713" id="Seg_7689" s="T712">vor.Kurzem</ta>
            <ta e="T714" id="Seg_7690" s="T713">1SG-ACC</ta>
            <ta e="T715" id="Seg_7691" s="T714">kränken-CVB.SIM</ta>
            <ta e="T717" id="Seg_7692" s="T716">was.[NOM]</ta>
            <ta e="T718" id="Seg_7693" s="T717">Rentier-3SG.[NOM]</ta>
            <ta e="T719" id="Seg_7694" s="T718">Gott.[NOM]</ta>
            <ta e="T720" id="Seg_7695" s="T719">bringen-PST2-3SG</ta>
            <ta e="T721" id="Seg_7696" s="T720">MOD</ta>
            <ta e="T722" id="Seg_7697" s="T721">Rentier-ACC</ta>
            <ta e="T723" id="Seg_7698" s="T722">essen-EP-CAUS-PST1-1SG</ta>
            <ta e="T724" id="Seg_7699" s="T723">doch</ta>
            <ta e="T725" id="Seg_7700" s="T724">jetzt</ta>
            <ta e="T726" id="Seg_7701" s="T725">Rentier-REFL/MED-CVB.SIM</ta>
            <ta e="T727" id="Seg_7702" s="T726">zusammen</ta>
            <ta e="T728" id="Seg_7703" s="T727">leben-IMP.1DU</ta>
            <ta e="T729" id="Seg_7704" s="T728">1PL.[NOM]</ta>
            <ta e="T730" id="Seg_7705" s="T729">jetzt</ta>
            <ta e="T731" id="Seg_7706" s="T730">zusammen</ta>
            <ta e="T732" id="Seg_7707" s="T731">leben-CVB.SEQ-1PL</ta>
            <ta e="T733" id="Seg_7708" s="T732">eins</ta>
            <ta e="T734" id="Seg_7709" s="T733">Rat-INSTR</ta>
            <ta e="T735" id="Seg_7710" s="T734">jagen-IMP.1DU</ta>
            <ta e="T736" id="Seg_7711" s="T735">und</ta>
            <ta e="T737" id="Seg_7712" s="T736">eins</ta>
            <ta e="T738" id="Seg_7713" s="T737">Haus-PROPR.[NOM]</ta>
            <ta e="T739" id="Seg_7714" s="T738">sein-IMP.1DU</ta>
            <ta e="T740" id="Seg_7715" s="T739">2SG.[NOM]</ta>
            <ta e="T741" id="Seg_7716" s="T740">Haus-2SG-ACC</ta>
            <ta e="T742" id="Seg_7717" s="T741">überführen-CVB.SIM</ta>
            <ta e="T743" id="Seg_7718" s="T742">bringen-IMP.1DU</ta>
            <ta e="T744" id="Seg_7719" s="T743">dieses</ta>
            <ta e="T745" id="Seg_7720" s="T744">Rentier-PROPR-1SG</ta>
            <ta e="T746" id="Seg_7721" s="T745">EMPH</ta>
            <ta e="T747" id="Seg_7722" s="T746">dieses</ta>
            <ta e="T748" id="Seg_7723" s="T747">eins</ta>
            <ta e="T749" id="Seg_7724" s="T748">Rentier-1PL.[NOM]</ta>
            <ta e="T750" id="Seg_7725" s="T749">ziehen-EP-CAUS-CVB.SEQ</ta>
            <ta e="T751" id="Seg_7726" s="T750">bringen-PRS.[3SG]</ta>
            <ta e="T752" id="Seg_7727" s="T751">was-PROPR</ta>
            <ta e="T753" id="Seg_7728" s="T752">sein-FUT.[3SG]-Q</ta>
            <ta e="T754" id="Seg_7729" s="T753">EMPH</ta>
            <ta e="T755" id="Seg_7730" s="T754">dieses.[NOM]</ta>
            <ta e="T756" id="Seg_7731" s="T755">bringen-IMP.1DU</ta>
            <ta e="T757" id="Seg_7732" s="T756">hierher</ta>
            <ta e="T758" id="Seg_7733" s="T757">Rentier-2SG-ACC</ta>
            <ta e="T759" id="Seg_7734" s="T758">äh</ta>
            <ta e="T760" id="Seg_7735" s="T759">wer-2SG-ACC</ta>
            <ta e="T761" id="Seg_7736" s="T760">hier</ta>
            <ta e="T762" id="Seg_7737" s="T761">eins</ta>
            <ta e="T763" id="Seg_7738" s="T762">zusammen</ta>
            <ta e="T764" id="Seg_7739" s="T763">leben-IMP.1DU</ta>
            <ta e="T765" id="Seg_7740" s="T764">1PL.[NOM]</ta>
            <ta e="T766" id="Seg_7741" s="T765">raten-EP-RECP/COLL-CVB.SEQ-3PL</ta>
            <ta e="T767" id="Seg_7742" s="T766">doch</ta>
            <ta e="T768" id="Seg_7743" s="T767">zusammen</ta>
            <ta e="T769" id="Seg_7744" s="T768">leben-CVB.SEQ</ta>
            <ta e="T770" id="Seg_7745" s="T769">bleiben-PST2-3PL</ta>
            <ta e="T771" id="Seg_7746" s="T770">zwei-COLL-3PL.[NOM]</ta>
            <ta e="T772" id="Seg_7747" s="T771">eins</ta>
            <ta e="T773" id="Seg_7748" s="T772">Rat.[NOM]</ta>
            <ta e="T774" id="Seg_7749" s="T773">sein-CVB.SEQ-3PL</ta>
            <ta e="T775" id="Seg_7750" s="T774">Methode.[NOM]</ta>
            <ta e="T776" id="Seg_7751" s="T775">sein-CVB.SEQ-3PL</ta>
            <ta e="T777" id="Seg_7752" s="T776">zwei-COLL-3PL.[NOM]</ta>
            <ta e="T778" id="Seg_7753" s="T777">doch</ta>
            <ta e="T779" id="Seg_7754" s="T778">reich.sein-CVB.SEQ-sich.satt.essen-CVB.SEQ</ta>
            <ta e="T780" id="Seg_7755" s="T779">leben-PST2-3PL</ta>
            <ta e="T781" id="Seg_7756" s="T780">letzter-3SG.[NOM]</ta>
            <ta e="T782" id="Seg_7757" s="T781">dieses.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_7758" s="T0">бабочка-ACC</ta>
            <ta e="T2" id="Seg_7759" s="T1">с</ta>
            <ta e="T3" id="Seg_7760" s="T2">с.тросточка</ta>
            <ta e="T4" id="Seg_7761" s="T3">старик.[NOM]</ta>
            <ta e="T5" id="Seg_7762" s="T4">жить-PST2-3PL</ta>
            <ta e="T6" id="Seg_7763" s="T5">говорят</ta>
            <ta e="T7" id="Seg_7764" s="T6">далекий-SIM</ta>
            <ta e="T8" id="Seg_7765" s="T7">жить-PRS-3PL</ta>
            <ta e="T9" id="Seg_7766" s="T8">сам</ta>
            <ta e="T10" id="Seg_7767" s="T9">сам-3PL-ACC</ta>
            <ta e="T11" id="Seg_7768" s="T10">знать-RECP/COLL-EP-RECP/COLL-PRS-3PL</ta>
            <ta e="T12" id="Seg_7769" s="T11">один.из.двух-3SG.[NOM]</ta>
            <ta e="T13" id="Seg_7770" s="T12">тот.[NOM]</ta>
            <ta e="T14" id="Seg_7771" s="T13">к</ta>
            <ta e="T15" id="Seg_7772" s="T14">дом-PROPR.[NOM]</ta>
            <ta e="T16" id="Seg_7773" s="T15">один.из.двух-3SG.[NOM]</ta>
            <ta e="T17" id="Seg_7774" s="T16">этот.[NOM]</ta>
            <ta e="T18" id="Seg_7775" s="T17">к</ta>
            <ta e="T19" id="Seg_7776" s="T18">дом-PROPR.[NOM]</ta>
            <ta e="T20" id="Seg_7777" s="T19">жить-CVB.SEQ-3PL</ta>
            <ta e="T21" id="Seg_7778" s="T20">два-COLL-3PL.[NOM]</ta>
            <ta e="T22" id="Seg_7779" s="T21">встречать-EP-PST2-3PL</ta>
            <ta e="T23" id="Seg_7780" s="T22">вместе</ta>
            <ta e="T24" id="Seg_7781" s="T23">рассказывать-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T25" id="Seg_7782" s="T24">вот</ta>
            <ta e="T26" id="Seg_7783" s="T25">говорить-PRS.[3SG]</ta>
            <ta e="T27" id="Seg_7784" s="T26">говорить-EP-RECP/COLL-PRS-3PL</ta>
            <ta e="T28" id="Seg_7785" s="T27">бабочка-3SG-ACC</ta>
            <ta e="T29" id="Seg_7786" s="T28">с</ta>
            <ta e="T30" id="Seg_7787" s="T29">с.тросточка</ta>
            <ta e="T31" id="Seg_7788" s="T30">старик.[NOM]</ta>
            <ta e="T32" id="Seg_7789" s="T31">вот</ta>
            <ta e="T33" id="Seg_7790" s="T32">1PL.[NOM]</ta>
            <ta e="T36" id="Seg_7791" s="T35">один</ta>
            <ta e="T37" id="Seg_7792" s="T36">год-ACC</ta>
            <ta e="T38" id="Seg_7793" s="T37">пережить-PTCP.PRS</ta>
            <ta e="T39" id="Seg_7794" s="T38">пища-1PL-ACC</ta>
            <ta e="T40" id="Seg_7795" s="T39">вместе</ta>
            <ta e="T41" id="Seg_7796" s="T40">связывать-CVB.SEQ-1PL</ta>
            <ta e="T42" id="Seg_7797" s="T41">есть-IMP.1DU</ta>
            <ta e="T43" id="Seg_7798" s="T42">хотеть-TEMP-1PL</ta>
            <ta e="T44" id="Seg_7799" s="T43">видно</ta>
            <ta e="T47" id="Seg_7800" s="T46">длинный-ADVZ</ta>
            <ta e="T48" id="Seg_7801" s="T47">жить-FUT-1PL</ta>
            <ta e="T49" id="Seg_7802" s="T48">видно</ta>
            <ta e="T50" id="Seg_7803" s="T49">год-1PL-ACC</ta>
            <ta e="T51" id="Seg_7804" s="T50">резать-PTCP.PRS-1PL-DAT/LOC</ta>
            <ta e="T52" id="Seg_7805" s="T51">EMPH</ta>
            <ta e="T53" id="Seg_7806" s="T52">этот</ta>
            <ta e="T54" id="Seg_7807" s="T53">плохой</ta>
            <ta e="T55" id="Seg_7808" s="T54">год-PL.[NOM]</ta>
            <ta e="T56" id="Seg_7809" s="T55">есть-3PL</ta>
            <ta e="T57" id="Seg_7810" s="T56">вот</ta>
            <ta e="T58" id="Seg_7811" s="T57">ладно</ta>
            <ta e="T59" id="Seg_7812" s="T58">теперь</ta>
            <ta e="T60" id="Seg_7813" s="T59">тогда</ta>
            <ta e="T61" id="Seg_7814" s="T60">1PL.[NOM]</ta>
            <ta e="T62" id="Seg_7815" s="T61">бабочка.[NOM]</ta>
            <ta e="T63" id="Seg_7816" s="T62">твой</ta>
            <ta e="T64" id="Seg_7817" s="T783">пища-2SG-ACC</ta>
            <ta e="T66" id="Seg_7818" s="T64">есть-IMP.1DU</ta>
            <ta e="T67" id="Seg_7819" s="T66">AFFIRM</ta>
            <ta e="T68" id="Seg_7820" s="T67">с.тросточка</ta>
            <ta e="T69" id="Seg_7821" s="T68">старик.[NOM]</ta>
            <ta e="T70" id="Seg_7822" s="T69">3SG.[NOM]</ta>
            <ta e="T71" id="Seg_7823" s="T70">тот</ta>
            <ta e="T72" id="Seg_7824" s="T71">его</ta>
            <ta e="T73" id="Seg_7825" s="T72">дом-3SG-DAT/LOC</ta>
            <ta e="T74" id="Seg_7826" s="T73">идти-CVB.SEQ-1PL</ta>
            <ta e="T75" id="Seg_7827" s="T74">есть-IMP.1DU</ta>
            <ta e="T76" id="Seg_7828" s="T75">вот</ta>
            <ta e="T77" id="Seg_7829" s="T76">ладно</ta>
            <ta e="T78" id="Seg_7830" s="T77">так</ta>
            <ta e="T79" id="Seg_7831" s="T78">есть-IMP.1DU</ta>
            <ta e="T80" id="Seg_7832" s="T79">о</ta>
            <ta e="T81" id="Seg_7833" s="T80">вот</ta>
            <ta e="T82" id="Seg_7834" s="T81">есть-PRS-3PL</ta>
            <ta e="T83" id="Seg_7835" s="T82">только</ta>
            <ta e="T84" id="Seg_7836" s="T83">бабочка.[NOM]</ta>
            <ta e="T85" id="Seg_7837" s="T84">дом-3SG-DAT/LOC</ta>
            <ta e="T86" id="Seg_7838" s="T85">есть-PRS-3PL</ta>
            <ta e="T87" id="Seg_7839" s="T86">один</ta>
            <ta e="T88" id="Seg_7840" s="T87">половина-ACC</ta>
            <ta e="T89" id="Seg_7841" s="T88">вот</ta>
            <ta e="T90" id="Seg_7842" s="T89">кончаться-PTCP.FUT-3SG-DAT/LOC</ta>
            <ta e="T91" id="Seg_7843" s="T90">пока</ta>
            <ta e="T92" id="Seg_7844" s="T91">вот</ta>
            <ta e="T93" id="Seg_7845" s="T92">есть-CVB.SEQ-3PL</ta>
            <ta e="T94" id="Seg_7846" s="T93">тот-3SG-3PL-GEN</ta>
            <ta e="T95" id="Seg_7847" s="T94">пища-3PL.[NOM]</ta>
            <ta e="T96" id="Seg_7848" s="T95">кончаться-PST1-3SG</ta>
            <ta e="T97" id="Seg_7849" s="T96">бабочка.[NOM]</ta>
            <ta e="T98" id="Seg_7850" s="T97">собственный-3SG.[NOM]</ta>
            <ta e="T99" id="Seg_7851" s="T98">вот</ta>
            <ta e="T100" id="Seg_7852" s="T99">с.тросточка</ta>
            <ta e="T101" id="Seg_7853" s="T100">старик-DAT/LOC</ta>
            <ta e="T102" id="Seg_7854" s="T101">идти-IMP.1PL</ta>
            <ta e="T103" id="Seg_7855" s="T102">идти-CVB.SEQ-3PL</ta>
            <ta e="T104" id="Seg_7856" s="T103">вот</ta>
            <ta e="T105" id="Seg_7857" s="T104">тот</ta>
            <ta e="T106" id="Seg_7858" s="T105">пища-3SG-ACC</ta>
            <ta e="T107" id="Seg_7859" s="T106">есть-CAP-3PL</ta>
            <ta e="T108" id="Seg_7860" s="T107">о</ta>
            <ta e="T109" id="Seg_7861" s="T108">с.тросточка</ta>
            <ta e="T110" id="Seg_7862" s="T109">старик-DAT/LOC</ta>
            <ta e="T111" id="Seg_7863" s="T110">идти-PST1-3PL</ta>
            <ta e="T113" id="Seg_7864" s="T112">вот</ta>
            <ta e="T114" id="Seg_7865" s="T113">есть-PRS-3PL</ta>
            <ta e="T115" id="Seg_7866" s="T114">есть-PRS-3PL-есть-PRS-3PL</ta>
            <ta e="T116" id="Seg_7867" s="T115">EMPH</ta>
            <ta e="T117" id="Seg_7868" s="T116">ой</ta>
            <ta e="T118" id="Seg_7869" s="T117">один</ta>
            <ta e="T119" id="Seg_7870" s="T118">друг-3SG.[NOM]</ta>
            <ta e="T120" id="Seg_7871" s="T119">мой</ta>
            <ta e="T121" id="Seg_7872" s="T120">пища-EP-1SG.[NOM]</ta>
            <ta e="T122" id="Seg_7873" s="T121">кончаться-PTCP.PRS</ta>
            <ta e="T123" id="Seg_7874" s="T122">быть-PST2.NEG.[3SG]</ta>
            <ta e="T124" id="Seg_7875" s="T784">говорить-PRS.[3SG]</ta>
            <ta e="T125" id="Seg_7876" s="T124">2SG.[NOM]</ta>
            <ta e="T126" id="Seg_7877" s="T125">дом-2SG-DAT/LOC</ta>
            <ta e="T128" id="Seg_7878" s="T126">идти.[IMP.2SG]</ta>
            <ta e="T129" id="Seg_7879" s="T128">сам-2SG.[NOM]</ta>
            <ta e="T130" id="Seg_7880" s="T129">охотить-EP-MED-CVB.SEQ</ta>
            <ta e="T131" id="Seg_7881" s="T130">1SG.[NOM]</ta>
            <ta e="T132" id="Seg_7882" s="T131">пища-1SG-ACC</ta>
            <ta e="T133" id="Seg_7883" s="T132">целый-3SG-ACC</ta>
            <ta e="T134" id="Seg_7884" s="T133">есть-PTCP.FUT-1SG-ACC</ta>
            <ta e="T135" id="Seg_7885" s="T134">хотеть-NEG-1SG</ta>
            <ta e="T136" id="Seg_7886" s="T135">EMPH</ta>
            <ta e="T137" id="Seg_7887" s="T136">вот</ta>
            <ta e="T138" id="Seg_7888" s="T137">идти-CVB.SEQ</ta>
            <ta e="T139" id="Seg_7889" s="T138">оставаться-PST1-3SG</ta>
            <ta e="T140" id="Seg_7890" s="T139">друг-3SG.[NOM]</ta>
            <ta e="T141" id="Seg_7891" s="T140">ой</ta>
            <ta e="T142" id="Seg_7892" s="T141">идти-CVB.SEQ</ta>
            <ta e="T143" id="Seg_7893" s="T142">что-ACC</ta>
            <ta e="T144" id="Seg_7894" s="T143">есть-FUT-3SG-Q</ta>
            <ta e="T145" id="Seg_7895" s="T144">дом-3SG-DAT/LOC</ta>
            <ta e="T146" id="Seg_7896" s="T145">бабочка.[NOM]</ta>
            <ta e="T147" id="Seg_7897" s="T146">идти-PST2-3SG</ta>
            <ta e="T148" id="Seg_7898" s="T147">дом-3SG-DAT/LOC</ta>
            <ta e="T149" id="Seg_7899" s="T148">что-3SG.[NOM]</ta>
            <ta e="T150" id="Seg_7900" s="T149">NEG</ta>
            <ta e="T151" id="Seg_7901" s="T150">NEG.EX</ta>
            <ta e="T152" id="Seg_7902" s="T151">что.[NOM]</ta>
            <ta e="T153" id="Seg_7903" s="T152">INDEF</ta>
            <ta e="T154" id="Seg_7904" s="T153">когда</ta>
            <ta e="T155" id="Seg_7905" s="T154">жилистое.мясо-DIM-ACC</ta>
            <ta e="T156" id="Seg_7906" s="T155">найти-EP-MED-CVB.SEQ</ta>
            <ta e="T157" id="Seg_7907" s="T156">тот-3SG-3SG-ACC</ta>
            <ta e="T158" id="Seg_7908" s="T157">варить-EP-MED-PST1-3SG</ta>
            <ta e="T159" id="Seg_7909" s="T158">варить-EP-MED-CVB.SEQ</ta>
            <ta e="T160" id="Seg_7910" s="T159">сидеть-PRS.[3SG]</ta>
            <ta e="T161" id="Seg_7911" s="T160">только</ta>
            <ta e="T162" id="Seg_7912" s="T161">вот</ta>
            <ta e="T163" id="Seg_7913" s="T162">потом</ta>
            <ta e="T164" id="Seg_7914" s="T163">видеть-PST2-3SG</ta>
            <ta e="T165" id="Seg_7915" s="T164">что-ACC</ta>
            <ta e="T166" id="Seg_7916" s="T165">INDEF</ta>
            <ta e="T167" id="Seg_7917" s="T166">охотить-EP-MED-PST2.[3SG]</ta>
            <ta e="T168" id="Seg_7918" s="T167">опять</ta>
            <ta e="T169" id="Seg_7919" s="T168">бабочка.[NOM]</ta>
            <ta e="T170" id="Seg_7920" s="T169">потом</ta>
            <ta e="T171" id="Seg_7921" s="T170">давешний.[NOM]</ta>
            <ta e="T172" id="Seg_7922" s="T171">с.тросточка</ta>
            <ta e="T173" id="Seg_7923" s="T172">старик.[NOM]</ta>
            <ta e="T174" id="Seg_7924" s="T173">да</ta>
            <ta e="T175" id="Seg_7925" s="T174">пища-3SG.[NOM]</ta>
            <ta e="T176" id="Seg_7926" s="T175">кончаться-PTCP.PST</ta>
            <ta e="T177" id="Seg_7927" s="T176">быть-PST1-3SG</ta>
            <ta e="T178" id="Seg_7928" s="T177">совсем</ta>
            <ta e="T179" id="Seg_7929" s="T178">вот</ta>
            <ta e="T180" id="Seg_7930" s="T179">сидеть-PRS.[3SG]</ta>
            <ta e="T181" id="Seg_7931" s="T180">знать-RECP/COLL-EP-NEG.[3SG]</ta>
            <ta e="T182" id="Seg_7932" s="T181">3SG-DAT/LOC</ta>
            <ta e="T183" id="Seg_7933" s="T182">вот</ta>
            <ta e="T184" id="Seg_7934" s="T183">приходить-NEG-IMP.3SG</ta>
            <ta e="T185" id="Seg_7935" s="T184">3SG.[NOM]</ta>
            <ta e="T186" id="Seg_7936" s="T185">сам-1SG.[NOM]</ta>
            <ta e="T187" id="Seg_7937" s="T186">EMPH</ta>
            <ta e="T188" id="Seg_7938" s="T187">охота-VBZ-CVB.SEQ</ta>
            <ta e="T189" id="Seg_7939" s="T188">жить-CVB.SEQ</ta>
            <ta e="T190" id="Seg_7940" s="T189">один</ta>
            <ta e="T191" id="Seg_7941" s="T190">что-INTNS.[NOM]</ta>
            <ta e="T192" id="Seg_7942" s="T191">INDEF</ta>
            <ta e="T193" id="Seg_7943" s="T192">думать-PRS.[3SG]</ta>
            <ta e="T194" id="Seg_7944" s="T193">бабочка.[NOM]</ta>
            <ta e="T195" id="Seg_7945" s="T194">только</ta>
            <ta e="T196" id="Seg_7946" s="T195">жить-PRS.[3SG]</ta>
            <ta e="T197" id="Seg_7947" s="T196">этот</ta>
            <ta e="T198" id="Seg_7948" s="T197">жить-TEMP-3SG</ta>
            <ta e="T199" id="Seg_7949" s="T198">с.тросточка</ta>
            <ta e="T200" id="Seg_7950" s="T199">старик.[NOM]</ta>
            <ta e="T201" id="Seg_7951" s="T200">приходить-PST2.[3SG]</ta>
            <ta e="T202" id="Seg_7952" s="T201">идти-PTCP.PRS-3SG-ACC</ta>
            <ta e="T203" id="Seg_7953" s="T202">видеть-PST1-3SG</ta>
            <ta e="T204" id="Seg_7954" s="T203">друг-3SG-ACC</ta>
            <ta e="T205" id="Seg_7955" s="T204">вот.беда</ta>
            <ta e="T206" id="Seg_7956" s="T205">с.тросточка</ta>
            <ta e="T207" id="Seg_7957" s="T206">старик.[NOM]</ta>
            <ta e="T208" id="Seg_7958" s="T207">идти-PTCP.PRS</ta>
            <ta e="T209" id="Seg_7959" s="T208">быть-PST2.[3SG]</ta>
            <ta e="T210" id="Seg_7960" s="T209">бабочка.[NOM]</ta>
            <ta e="T211" id="Seg_7961" s="T210">думать-PRS.[3SG]</ta>
            <ta e="T212" id="Seg_7962" s="T785">дом-3SG-ACC</ta>
            <ta e="T213" id="Seg_7963" s="T212">запирать-EP-MED-CVB.SEQ</ta>
            <ta e="T214" id="Seg_7964" s="T213">закрывать-CVB.SEQ</ta>
            <ta e="T216" id="Seg_7965" s="T214">бросать-PST2.[3SG]</ta>
            <ta e="T217" id="Seg_7966" s="T216">вот</ta>
            <ta e="T218" id="Seg_7967" s="T217">приходить-PRS.[3SG]</ta>
            <ta e="T219" id="Seg_7968" s="T218">только</ta>
            <ta e="T220" id="Seg_7969" s="T219">ах</ta>
            <ta e="T223" id="Seg_7970" s="T222">бабочка-3SG-DAT/LOC</ta>
            <ta e="T224" id="Seg_7971" s="T223">приходить-PRS.[3SG]</ta>
            <ta e="T225" id="Seg_7972" s="T224">бабочка.[NOM]</ta>
            <ta e="T226" id="Seg_7973" s="T225">друг-VBZ.[IMP.2SG]</ta>
            <ta e="T227" id="Seg_7974" s="T226">бабочка.[NOM]</ta>
            <ta e="T228" id="Seg_7975" s="T227">друг-VBZ.[IMP.2SG]</ta>
            <ta e="T229" id="Seg_7976" s="T228">2SG.[NOM]</ta>
            <ta e="T230" id="Seg_7977" s="T229">давать.[IMP.2SG]</ta>
            <ta e="T231" id="Seg_7978" s="T230">INDEF</ta>
            <ta e="T232" id="Seg_7979" s="T786">жилистое.мясо-DIM-PART</ta>
            <ta e="T233" id="Seg_7980" s="T232">бросать-CVB.SEQ</ta>
            <ta e="T234" id="Seg_7981" s="T233">выручать.[IMP.2SG]</ta>
            <ta e="T235" id="Seg_7982" s="T234">бабочка.[NOM]</ta>
            <ta e="T237" id="Seg_7983" s="T235">друг-VBZ.[IMP.2SG]</ta>
            <ta e="T238" id="Seg_7984" s="T237">%%-CVB.SEQ</ta>
            <ta e="T239" id="Seg_7985" s="T238">день-1SG-DAT/LOC</ta>
            <ta e="T240" id="Seg_7986" s="T239">стоять-PST1-3SG</ta>
            <ta e="T241" id="Seg_7987" s="T240">год-1SG-ACC</ta>
            <ta e="T242" id="Seg_7988" s="T241">%%</ta>
            <ta e="T243" id="Seg_7989" s="T242">бабочка.[NOM]</ta>
            <ta e="T244" id="Seg_7990" s="T243">друг-VBZ.[IMP.2SG]</ta>
            <ta e="T245" id="Seg_7991" s="T244">бабочка.[NOM]</ta>
            <ta e="T246" id="Seg_7992" s="T245">друг-VBZ.[IMP.2SG]</ta>
            <ta e="T247" id="Seg_7993" s="T246">умирать-PST1-1SG</ta>
            <ta e="T248" id="Seg_7994" s="T247">оставаться-FUT-1SG</ta>
            <ta e="T249" id="Seg_7995" s="T248">один-ACC</ta>
            <ta e="T250" id="Seg_7996" s="T249">петь-CVB.SIM</ta>
            <ta e="T251" id="Seg_7997" s="T250">сидеть-PST2.[3SG]</ta>
            <ta e="T252" id="Seg_7998" s="T251">совсем</ta>
            <ta e="T253" id="Seg_7999" s="T252">вот</ta>
            <ta e="T254" id="Seg_8000" s="T253">идти.[IMP.2SG]-идти.[IMP.2SG]</ta>
            <ta e="T255" id="Seg_8001" s="T254">один</ta>
            <ta e="T256" id="Seg_8002" s="T255">INDEF</ta>
            <ta e="T257" id="Seg_8003" s="T256">один</ta>
            <ta e="T258" id="Seg_8004" s="T257">что-ACC</ta>
            <ta e="T259" id="Seg_8005" s="T258">%%-DAT/LOC</ta>
            <ta e="T260" id="Seg_8006" s="T259">прилипать-EP-PTCP.PST</ta>
            <ta e="T262" id="Seg_8007" s="T261">высыхать-PTCP.PST</ta>
            <ta e="T263" id="Seg_8008" s="T262">жилистое.мясо-DIM-ACC</ta>
            <ta e="T264" id="Seg_8009" s="T263">бросать-CVB.SEQ</ta>
            <ta e="T265" id="Seg_8010" s="T264">бросать-PST1-3SG</ta>
            <ta e="T266" id="Seg_8011" s="T265">идти.[IMP.2SG]</ta>
            <ta e="T267" id="Seg_8012" s="T266">говорить-PRS.[3SG]</ta>
            <ta e="T268" id="Seg_8013" s="T267">тот-3SG-3SG.[NOM]</ta>
            <ta e="T269" id="Seg_8014" s="T268">с.тросточка</ta>
            <ta e="T270" id="Seg_8015" s="T269">идти-CVB.SEQ</ta>
            <ta e="T271" id="Seg_8016" s="T270">оставаться-PST1-3SG</ta>
            <ta e="T272" id="Seg_8017" s="T271">идти.[IMP.2SG]</ta>
            <ta e="T273" id="Seg_8018" s="T272">дом-3SG-DAT/LOC</ta>
            <ta e="T274" id="Seg_8019" s="T273">идти.[IMP.2SG]</ta>
            <ta e="T275" id="Seg_8020" s="T274">дом-2SG-DAT/LOC</ta>
            <ta e="T276" id="Seg_8021" s="T275">говорить-PRS.[3SG]</ta>
            <ta e="T277" id="Seg_8022" s="T276">больше</ta>
            <ta e="T278" id="Seg_8023" s="T277">давать-NEG-1SG</ta>
            <ta e="T279" id="Seg_8024" s="T278">2SG-DAT/LOC</ta>
            <ta e="T280" id="Seg_8025" s="T279">EMPH</ta>
            <ta e="T281" id="Seg_8026" s="T280">идти-CVB.SEQ</ta>
            <ta e="T282" id="Seg_8027" s="T281">оставаться-PST1-3SG</ta>
            <ta e="T283" id="Seg_8028" s="T282">человек.[NOM]</ta>
            <ta e="T284" id="Seg_8029" s="T283">идти-CVB.SEQ</ta>
            <ta e="T285" id="Seg_8030" s="T284">столько.[NOM]</ta>
            <ta e="T286" id="Seg_8031" s="T285">есть-FUT-3SG-Q</ta>
            <ta e="T287" id="Seg_8032" s="T286">3SG.[NOM]</ta>
            <ta e="T288" id="Seg_8033" s="T287">потом</ta>
            <ta e="T289" id="Seg_8034" s="T288">тот-этот</ta>
            <ta e="T290" id="Seg_8035" s="T289">вот</ta>
            <ta e="T291" id="Seg_8036" s="T290">кровь-3SG-ACC</ta>
            <ta e="T292" id="Seg_8037" s="T291">пустой.[NOM]</ta>
            <ta e="T293" id="Seg_8038" s="T292">пустой</ta>
            <ta e="T294" id="Seg_8039" s="T293">суп-3SG-ACC</ta>
            <ta e="T295" id="Seg_8040" s="T294">только</ta>
            <ta e="T298" id="Seg_8041" s="T297">варить-EP-MED-CVB.SEQ</ta>
            <ta e="T299" id="Seg_8042" s="T298">есть-PST2.[3SG]</ta>
            <ta e="T300" id="Seg_8043" s="T299">EMPH</ta>
            <ta e="T301" id="Seg_8044" s="T300">что.[NOM]</ta>
            <ta e="T302" id="Seg_8045" s="T301">NEG</ta>
            <ta e="T303" id="Seg_8046" s="T302">найти-NEG.[3SG]</ta>
            <ta e="T304" id="Seg_8047" s="T303">да</ta>
            <ta e="T305" id="Seg_8048" s="T304">и.так.далее-NEG.[3SG]</ta>
            <ta e="T306" id="Seg_8049" s="T305">EMPH</ta>
            <ta e="T307" id="Seg_8050" s="T306">сколько</ta>
            <ta e="T308" id="Seg_8051" s="T307">INDEF</ta>
            <ta e="T309" id="Seg_8052" s="T308">день-ACC</ta>
            <ta e="T310" id="Seg_8053" s="T309">лежать-PST2-3SG</ta>
            <ta e="T311" id="Seg_8054" s="T310">EMPH</ta>
            <ta e="T312" id="Seg_8055" s="T311">только</ta>
            <ta e="T313" id="Seg_8056" s="T312">EMPH</ta>
            <ta e="T314" id="Seg_8057" s="T313">идти-CVB.SEQ</ta>
            <ta e="T315" id="Seg_8058" s="T314">выйти-PTCP.PRS</ta>
            <ta e="T316" id="Seg_8059" s="T315">становиться-PST2.[3SG]</ta>
            <ta e="T317" id="Seg_8060" s="T316">конец-EP-ABL</ta>
            <ta e="T318" id="Seg_8061" s="T317">голодать-CVB.SEQ</ta>
            <ta e="T319" id="Seg_8062" s="T318">с.тросточка</ta>
            <ta e="T320" id="Seg_8063" s="T319">старик.[NOM]</ta>
            <ta e="T321" id="Seg_8064" s="T320">опять</ta>
            <ta e="T322" id="Seg_8065" s="T321">бабочка-3SG-DAT/LOC</ta>
            <ta e="T323" id="Seg_8066" s="T322">идти-CAP.[3SG]</ta>
            <ta e="T324" id="Seg_8067" s="T323">приходить-CVB.SEQ</ta>
            <ta e="T325" id="Seg_8068" s="T324">бабочка-3SG-DAT/LOC</ta>
            <ta e="T326" id="Seg_8069" s="T325">опять</ta>
            <ta e="T327" id="Seg_8070" s="T326">петь-PST2.[3SG]</ta>
            <ta e="T328" id="Seg_8071" s="T327">бабочка.[NOM]</ta>
            <ta e="T329" id="Seg_8072" s="T328">друг-VBZ.[IMP.2SG]</ta>
            <ta e="T330" id="Seg_8073" s="T329">бабочка.[NOM]</ta>
            <ta e="T331" id="Seg_8074" s="T330">друг-VBZ.[IMP.2SG]</ta>
            <ta e="T332" id="Seg_8075" s="T787">умирать-PST1-1SG</ta>
            <ta e="T333" id="Seg_8076" s="T332">оставаться-FUT-1SG</ta>
            <ta e="T334" id="Seg_8077" s="T333">умирать-PST1-1SG</ta>
            <ta e="T336" id="Seg_8078" s="T334">оставаться-FUT-1SG</ta>
            <ta e="T337" id="Seg_8079" s="T336">бабочка.[NOM]</ta>
            <ta e="T338" id="Seg_8080" s="T337">друг-VBZ.[IMP.2SG]</ta>
            <ta e="T339" id="Seg_8081" s="T338">один</ta>
            <ta e="T340" id="Seg_8082" s="T339">INDEF</ta>
            <ta e="T342" id="Seg_8083" s="T341">INDEF</ta>
            <ta e="T343" id="Seg_8084" s="T342">жилистое.мясо-DIM-PART</ta>
            <ta e="T344" id="Seg_8085" s="T343">бросать-CVB.SEQ</ta>
            <ta e="T345" id="Seg_8086" s="T344">выручать.[IMP.2SG]</ta>
            <ta e="T346" id="Seg_8087" s="T345">бабочка.[NOM]</ta>
            <ta e="T347" id="Seg_8088" s="T346">друг-VBZ.[IMP.2SG]</ta>
            <ta e="T348" id="Seg_8089" s="T347">бабочка.[NOM]</ta>
            <ta e="T349" id="Seg_8090" s="T348">друг-VBZ.[IMP.2SG]</ta>
            <ta e="T350" id="Seg_8091" s="T349">%%-CVB.SEQ</ta>
            <ta e="T351" id="Seg_8092" s="T350">забывать-CVB.SEQ</ta>
            <ta e="T352" id="Seg_8093" s="T788">быть-PST1-3SG</ta>
            <ta e="T353" id="Seg_8094" s="T352">найти-FUT.[3SG]</ta>
            <ta e="T354" id="Seg_8095" s="T353">бабочка.[NOM]</ta>
            <ta e="T355" id="Seg_8096" s="T354">друг-VBZ.[IMP.2SG]</ta>
            <ta e="T356" id="Seg_8097" s="T355">%%-CVB.SEQ</ta>
            <ta e="T357" id="Seg_8098" s="T356">жилистое.мясо-PART</ta>
            <ta e="T358" id="Seg_8099" s="T357">приносить.[IMP.2SG]</ta>
            <ta e="T360" id="Seg_8100" s="T358">петь-EP-MED-PRS-1SG</ta>
            <ta e="T361" id="Seg_8101" s="T360">EMPH</ta>
            <ta e="T362" id="Seg_8102" s="T361">один</ta>
            <ta e="T363" id="Seg_8103" s="T362">NEG</ta>
            <ta e="T364" id="Seg_8104" s="T363">жилистое.мясо-POSS</ta>
            <ta e="T365" id="Seg_8105" s="T364">NEG-1SG</ta>
            <ta e="T366" id="Seg_8106" s="T365">идти.[IMP.2SG]</ta>
            <ta e="T367" id="Seg_8107" s="T366">идти.[IMP.2SG]</ta>
            <ta e="T368" id="Seg_8108" s="T367">говорить-PST2.[3SG]</ta>
            <ta e="T369" id="Seg_8109" s="T368">назад</ta>
            <ta e="T370" id="Seg_8110" s="T369">гнать-CVB.SEQ</ta>
            <ta e="T371" id="Seg_8111" s="T370">послать-PST2.[3SG]</ta>
            <ta e="T372" id="Seg_8112" s="T371">что-INTNS-ACC</ta>
            <ta e="T373" id="Seg_8113" s="T372">NEG</ta>
            <ta e="T374" id="Seg_8114" s="T373">давать-PST2.NEG.[3SG]</ta>
            <ta e="T375" id="Seg_8115" s="T374">куда</ta>
            <ta e="T376" id="Seg_8116" s="T375">идти-FUT.[3SG]-Q</ta>
            <ta e="T377" id="Seg_8117" s="T376">на.улице</ta>
            <ta e="T378" id="Seg_8118" s="T377">потом</ta>
            <ta e="T379" id="Seg_8119" s="T378">мерзнуть-PTCP.PRS</ta>
            <ta e="T380" id="Seg_8120" s="T379">быть-PST1-3SG</ta>
            <ta e="T381" id="Seg_8121" s="T380">куда</ta>
            <ta e="T382" id="Seg_8122" s="T381">дом-DAT/LOC</ta>
            <ta e="T383" id="Seg_8123" s="T382">NEG</ta>
            <ta e="T384" id="Seg_8124" s="T383">входить-CAUS-NEG.[3SG]</ta>
            <ta e="T385" id="Seg_8125" s="T384">позор.[NOM]</ta>
            <ta e="T386" id="Seg_8126" s="T385">вот</ta>
            <ta e="T387" id="Seg_8127" s="T386">идти-PRS.[3SG]</ta>
            <ta e="T388" id="Seg_8128" s="T387">этот</ta>
            <ta e="T389" id="Seg_8129" s="T388">человек.[NOM]</ta>
            <ta e="T390" id="Seg_8130" s="T389">идти-PST2-3SG</ta>
            <ta e="T391" id="Seg_8131" s="T390">еле</ta>
            <ta e="T392" id="Seg_8132" s="T391">потом</ta>
            <ta e="T393" id="Seg_8133" s="T392">доезжать-CVB.SEQ</ta>
            <ta e="T394" id="Seg_8134" s="T393">идти-TEMP-3SG</ta>
            <ta e="T397" id="Seg_8135" s="T396">дом-3SG-GEN</ta>
            <ta e="T398" id="Seg_8136" s="T397">место.около-DAT/LOC</ta>
            <ta e="T399" id="Seg_8137" s="T398">доезжать-PRS.[3SG]</ta>
            <ta e="T400" id="Seg_8138" s="T399">видеть-PST2-3SG</ta>
            <ta e="T401" id="Seg_8139" s="T400">теленок-PROPR</ta>
            <ta e="T402" id="Seg_8140" s="T401">важенка.[NOM]</ta>
            <ta e="T403" id="Seg_8141" s="T402">идти-PRS.[3SG]</ta>
            <ta e="T404" id="Seg_8142" s="T403">дом-3SG-DAT/LOC</ta>
            <ta e="T405" id="Seg_8143" s="T404">какой</ta>
            <ta e="T406" id="Seg_8144" s="T405">приходить-NMNZ-ABL</ta>
            <ta e="T407" id="Seg_8145" s="T406">приходить-PST2-3SG</ta>
            <ta e="T408" id="Seg_8146" s="T407">Q</ta>
            <ta e="T409" id="Seg_8147" s="T408">знать-NEG.[3SG]</ta>
            <ta e="T410" id="Seg_8148" s="T409">олень-ACC</ta>
            <ta e="T411" id="Seg_8149" s="T410">3SG.[NOM]</ta>
            <ta e="T412" id="Seg_8150" s="T789">олень.[NOM]</ta>
            <ta e="T413" id="Seg_8151" s="T412">идти-PRS.[3SG]</ta>
            <ta e="T414" id="Seg_8152" s="T413">только</ta>
            <ta e="T416" id="Seg_8153" s="T414">приходить-PST2.[3SG]</ta>
            <ta e="T417" id="Seg_8154" s="T416">вверх</ta>
            <ta e="T418" id="Seg_8155" s="T417">3SG.[NOM]</ta>
            <ta e="T419" id="Seg_8156" s="T418">умирать-PTCP.FUT</ta>
            <ta e="T420" id="Seg_8157" s="T419">предназначение-3SG-DAT/LOC</ta>
            <ta e="T423" id="Seg_8158" s="T422">оживать-PTCP.FUT</ta>
            <ta e="T424" id="Seg_8159" s="T423">предназначение-3SG-DAT/LOC</ta>
            <ta e="T425" id="Seg_8160" s="T424">теленок-PROPR</ta>
            <ta e="T426" id="Seg_8161" s="T425">важенка-3SG.[NOM]</ta>
            <ta e="T427" id="Seg_8162" s="T426">сам-3SG-DAT/LOC</ta>
            <ta e="T428" id="Seg_8163" s="T427">приходить-CVB.SEQ</ta>
            <ta e="T429" id="Seg_8164" s="T428">лизать-CVB.SIM</ta>
            <ta e="T430" id="Seg_8165" s="T429">идти-EP-PST2-3PL</ta>
            <ta e="T431" id="Seg_8166" s="T430">3SG-ACC</ta>
            <ta e="T432" id="Seg_8167" s="T431">мочиться-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T433" id="Seg_8168" s="T432">нутро-3SG-DAT/LOC</ta>
            <ta e="T434" id="Seg_8169" s="T433">приходить-PST2-3SG</ta>
            <ta e="T435" id="Seg_8170" s="T434">да</ta>
            <ta e="T436" id="Seg_8171" s="T435">теленок-DIM-3SG-ACC</ta>
            <ta e="T437" id="Seg_8172" s="T436">поймать-CVB.SEQ</ta>
            <ta e="T438" id="Seg_8173" s="T437">взять-CVB.SEQ</ta>
            <ta e="T439" id="Seg_8174" s="T438">нож-DIM-ACC</ta>
            <ta e="T440" id="Seg_8175" s="T439">взять-PST1-3SG</ta>
            <ta e="T441" id="Seg_8176" s="T440">да</ta>
            <ta e="T442" id="Seg_8177" s="T441">корт</ta>
            <ta e="T443" id="Seg_8178" s="T442">делать-CAUS-CVB.SEQ</ta>
            <ta e="T444" id="Seg_8179" s="T443">убить-CVB.SEQ</ta>
            <ta e="T445" id="Seg_8180" s="T444">бросать-PST2.[3SG]</ta>
            <ta e="T446" id="Seg_8181" s="T445">мать-3SG.[NOM]</ta>
            <ta e="T447" id="Seg_8182" s="T446">идти-PRS.[3SG]</ta>
            <ta e="T448" id="Seg_8183" s="T447">вот</ta>
            <ta e="T449" id="Seg_8184" s="T448">этот-3SG-3SG-ACC</ta>
            <ta e="T450" id="Seg_8185" s="T449">что.[NOM]</ta>
            <ta e="T451" id="Seg_8186" s="T450">что.[NOM]</ta>
            <ta e="T454" id="Seg_8187" s="T453">сдирать.кожу-EP-MED-PST2.[3SG]</ta>
            <ta e="T455" id="Seg_8188" s="T454">человек.[NOM]</ta>
            <ta e="T456" id="Seg_8189" s="T455">сырой-VBZ-CVB.SIM</ta>
            <ta e="T457" id="Seg_8190" s="T456">вариться-EP-CAUS-CVB.SEQ</ta>
            <ta e="T458" id="Seg_8191" s="T457">варить-EP-MED-CVB.SEQ</ta>
            <ta e="T459" id="Seg_8192" s="T458">есть-CVB.SEQ</ta>
            <ta e="T460" id="Seg_8193" s="T459">бросать-PST2.[3SG]</ta>
            <ta e="T461" id="Seg_8194" s="T460">вот</ta>
            <ta e="T462" id="Seg_8195" s="T461">глаз-3SG.[NOM]</ta>
            <ta e="T463" id="Seg_8196" s="T462">посветлеть-PST2.[3SG]</ta>
            <ta e="T464" id="Seg_8197" s="T463">там</ta>
            <ta e="T465" id="Seg_8198" s="T464">о</ta>
            <ta e="T466" id="Seg_8199" s="T465">вот</ta>
            <ta e="T467" id="Seg_8200" s="T466">теперь</ta>
            <ta e="T468" id="Seg_8201" s="T467">EMPH</ta>
            <ta e="T469" id="Seg_8202" s="T468">спасать-EP-REFL-PST1-1SG</ta>
            <ta e="T470" id="Seg_8203" s="T469">бабочка-DAT/LOC</ta>
            <ta e="T471" id="Seg_8204" s="T470">идти-NEG-1SG</ta>
            <ta e="T472" id="Seg_8205" s="T471">и.так.далее-NEG-1SG</ta>
            <ta e="T473" id="Seg_8206" s="T472">пища-VBZ-PST1-1SG</ta>
            <ta e="T474" id="Seg_8207" s="T473">EMPH</ta>
            <ta e="T475" id="Seg_8208" s="T474">думать-PRS.[3SG]</ta>
            <ta e="T476" id="Seg_8209" s="T475">пища-VBZ-PTCP.PST</ta>
            <ta e="T477" id="Seg_8210" s="T476">человек.[NOM]</ta>
            <ta e="T478" id="Seg_8211" s="T477">варить-EP-MED-CVB.SEQ</ta>
            <ta e="T479" id="Seg_8212" s="T478">после</ta>
            <ta e="T480" id="Seg_8213" s="T479">есть-PRS.[3SG]</ta>
            <ta e="T481" id="Seg_8214" s="T480">ок</ta>
            <ta e="T482" id="Seg_8215" s="T481">вот</ta>
            <ta e="T483" id="Seg_8216" s="T482">сколько</ta>
            <ta e="T484" id="Seg_8217" s="T483">день-ACC</ta>
            <ta e="T485" id="Seg_8218" s="T484">жить-PST2-3SG</ta>
            <ta e="T486" id="Seg_8219" s="T485">Q</ta>
            <ta e="T487" id="Seg_8220" s="T486">олень-PROPR</ta>
            <ta e="T488" id="Seg_8221" s="T487">олень-3SG-ACC</ta>
            <ta e="T489" id="Seg_8222" s="T488">EMPH</ta>
            <ta e="T490" id="Seg_8223" s="T489">поймать-EP-PST2.[3SG]</ta>
            <ta e="T491" id="Seg_8224" s="T490">важенка-3SG-ACC</ta>
            <ta e="T492" id="Seg_8225" s="T491">важенка-PROPR.[NOM]</ta>
            <ta e="T493" id="Seg_8226" s="T492">что.[NOM]</ta>
            <ta e="T494" id="Seg_8227" s="T493">быть-FUT.[3SG]-Q</ta>
            <ta e="T495" id="Seg_8228" s="T494">теперь</ta>
            <ta e="T496" id="Seg_8229" s="T495">о</ta>
            <ta e="T497" id="Seg_8230" s="T496">этот</ta>
            <ta e="T498" id="Seg_8231" s="T497">лежать-TEMP-3SG</ta>
            <ta e="T499" id="Seg_8232" s="T498">бабочка-3SG-GEN</ta>
            <ta e="T500" id="Seg_8233" s="T499">голос-3SG.[NOM]</ta>
            <ta e="T501" id="Seg_8234" s="T500">приходить-PST1-3SG</ta>
            <ta e="T502" id="Seg_8235" s="T501">вот</ta>
            <ta e="T503" id="Seg_8236" s="T502">только</ta>
            <ta e="T504" id="Seg_8237" s="T503">видеть-PST2-3SG</ta>
            <ta e="T505" id="Seg_8238" s="T504">вот</ta>
            <ta e="T506" id="Seg_8239" s="T505">1SG.[NOM]</ta>
            <ta e="T507" id="Seg_8240" s="T506">теперь</ta>
            <ta e="T508" id="Seg_8241" s="T507">так</ta>
            <ta e="T509" id="Seg_8242" s="T508">делать-FUT-1SG</ta>
            <ta e="T510" id="Seg_8243" s="T509">подожди</ta>
            <ta e="T511" id="Seg_8244" s="T510">приходить-IMP.3SG</ta>
            <ta e="T512" id="Seg_8245" s="T511">только</ta>
            <ta e="T513" id="Seg_8246" s="T512">думать-PST2.[3SG]</ta>
            <ta e="T514" id="Seg_8247" s="T513">1SG-DAT/LOC</ta>
            <ta e="T515" id="Seg_8248" s="T514">пища.[NOM]</ta>
            <ta e="T516" id="Seg_8249" s="T515">бабочка.[NOM]</ta>
            <ta e="T517" id="Seg_8250" s="T516">что-VBZ-CVB.SEQ</ta>
            <ta e="T518" id="Seg_8251" s="T517">еле</ta>
            <ta e="T519" id="Seg_8252" s="T518">идти-PRS.[3SG]</ta>
            <ta e="T520" id="Seg_8253" s="T519">вовсе</ta>
            <ta e="T521" id="Seg_8254" s="T520">голодать-CVB.SEQ</ta>
            <ta e="T522" id="Seg_8255" s="T521">еле</ta>
            <ta e="T523" id="Seg_8256" s="T522">идти-CVB.SEQ</ta>
            <ta e="T524" id="Seg_8257" s="T523">идти-PRS.[3SG]</ta>
            <ta e="T525" id="Seg_8258" s="T524">приходить-PST2.[3SG]</ta>
            <ta e="T526" id="Seg_8259" s="T525">да</ta>
            <ta e="T527" id="Seg_8260" s="T526">приходить-PST2.[3SG]</ta>
            <ta e="T528" id="Seg_8261" s="T527">приходить-PTCP.PRS-3SG-ACC</ta>
            <ta e="T529" id="Seg_8262" s="T528">с</ta>
            <ta e="T530" id="Seg_8263" s="T529">дом-3SG.[NOM]</ta>
            <ta e="T531" id="Seg_8264" s="T530">запирать-CVB.SEQ</ta>
            <ta e="T532" id="Seg_8265" s="T531">запирать-EP-MED-CVB.SEQ</ta>
            <ta e="T533" id="Seg_8266" s="T532">бросать-PST2.[3SG]</ta>
            <ta e="T534" id="Seg_8267" s="T533">вот</ta>
            <ta e="T535" id="Seg_8268" s="T534">3SG-ACC</ta>
            <ta e="T536" id="Seg_8269" s="T535">наказывать-VBZ-FUT-1SG</ta>
            <ta e="T537" id="Seg_8270" s="T536">недавно</ta>
            <ta e="T538" id="Seg_8271" s="T537">1SG-ACC</ta>
            <ta e="T539" id="Seg_8272" s="T538">опять</ta>
            <ta e="T540" id="Seg_8273" s="T539">3SG.[NOM]</ta>
            <ta e="T541" id="Seg_8274" s="T540">мучить-PST2-3SG</ta>
            <ta e="T542" id="Seg_8275" s="T541">так</ta>
            <ta e="T543" id="Seg_8276" s="T542">с.тросточка</ta>
            <ta e="T544" id="Seg_8277" s="T543">старик.[NOM]</ta>
            <ta e="T545" id="Seg_8278" s="T544">с.тросточка</ta>
            <ta e="T546" id="Seg_8279" s="T545">старик.[NOM]</ta>
            <ta e="T547" id="Seg_8280" s="T546">один</ta>
            <ta e="T548" id="Seg_8281" s="T790">INDEF</ta>
            <ta e="T549" id="Seg_8282" s="T548">жилистое.мясо-DIM-PART</ta>
            <ta e="T550" id="Seg_8283" s="T549">давать-CVB.SEQ</ta>
            <ta e="T551" id="Seg_8284" s="T550">выручать.[IMP.2SG]</ta>
            <ta e="T552" id="Seg_8285" s="T551">с.тросточка</ta>
            <ta e="T553" id="Seg_8286" s="T552">старик.[NOM]</ta>
            <ta e="T554" id="Seg_8287" s="T553">умирать-PST1-1SG</ta>
            <ta e="T555" id="Seg_8288" s="T554">оставаться-PST1-1SG</ta>
            <ta e="T556" id="Seg_8289" s="T555">умирать-PST1-1SG</ta>
            <ta e="T560" id="Seg_8290" s="T558">оставаться-PST1-1SG</ta>
            <ta e="T561" id="Seg_8291" s="T560">день-1SG-DAT/LOC</ta>
            <ta e="T562" id="Seg_8292" s="T561">стоять-PST1-3SG</ta>
            <ta e="T563" id="Seg_8293" s="T562">безжизненный</ta>
            <ta e="T564" id="Seg_8294" s="T563">человек-3SG.[NOM]</ta>
            <ta e="T565" id="Seg_8295" s="T564">быть-TEMP-2SG</ta>
            <ta e="T566" id="Seg_8296" s="T565">с.тросточка</ta>
            <ta e="T567" id="Seg_8297" s="T566">старик.[NOM]</ta>
            <ta e="T568" id="Seg_8298" s="T567">с.тросточка</ta>
            <ta e="T569" id="Seg_8299" s="T568">старик.[NOM]</ta>
            <ta e="T570" id="Seg_8300" s="T569">петь-EP-MED-CVB.SIM</ta>
            <ta e="T571" id="Seg_8301" s="T570">лежать-PST2.[3SG]</ta>
            <ta e="T572" id="Seg_8302" s="T571">что-ACC</ta>
            <ta e="T573" id="Seg_8303" s="T572">NEG</ta>
            <ta e="T574" id="Seg_8304" s="T573">давать-PST2.NEG.[3SG]</ta>
            <ta e="T575" id="Seg_8305" s="T574">пустой</ta>
            <ta e="T576" id="Seg_8306" s="T575">что.[NOM]</ta>
            <ta e="T577" id="Seg_8307" s="T576">INDEF</ta>
            <ta e="T578" id="Seg_8308" s="T577">этот</ta>
            <ta e="T579" id="Seg_8309" s="T578">олененок-3SG-GEN</ta>
            <ta e="T580" id="Seg_8310" s="T579">собственный-3SG-GEN</ta>
            <ta e="T581" id="Seg_8311" s="T580">копыто-3SG.[NOM]</ta>
            <ta e="T582" id="Seg_8312" s="T581">копыто-3PL-ACC</ta>
            <ta e="T583" id="Seg_8313" s="T582">что-VBZ-EP-MED-PST2.[3SG]</ta>
            <ta e="T584" id="Seg_8314" s="T583">%%-3SG-ACC</ta>
            <ta e="T585" id="Seg_8315" s="T584">с</ta>
            <ta e="T586" id="Seg_8316" s="T585">копыто-3SG.[NOM]</ta>
            <ta e="T587" id="Seg_8317" s="T586">Q</ta>
            <ta e="T588" id="Seg_8318" s="T587">что.[NOM]</ta>
            <ta e="T589" id="Seg_8319" s="T588">Q</ta>
            <ta e="T590" id="Seg_8320" s="T589">тот-ACC</ta>
            <ta e="T593" id="Seg_8321" s="T592">%%-PROPR</ta>
            <ta e="T594" id="Seg_8322" s="T593">идти-CVB.SEQ</ta>
            <ta e="T595" id="Seg_8323" s="T594">тот-ACC</ta>
            <ta e="T596" id="Seg_8324" s="T595">бросать-CVB.SEQ</ta>
            <ta e="T597" id="Seg_8325" s="T596">идти.[IMP.2SG]</ta>
            <ta e="T598" id="Seg_8326" s="T597">есть.[IMP.2SG]</ta>
            <ta e="T599" id="Seg_8327" s="T598">говорить-CVB.SIM</ta>
            <ta e="T600" id="Seg_8328" s="T599">два</ta>
            <ta e="T601" id="Seg_8329" s="T600">копыто-ACC</ta>
            <ta e="T602" id="Seg_8330" s="T601">бросать-CVB.SEQ</ta>
            <ta e="T603" id="Seg_8331" s="T602">бросать-PST2.[3SG]</ta>
            <ta e="T604" id="Seg_8332" s="T603">идти.[IMP.2SG]</ta>
            <ta e="T605" id="Seg_8333" s="T604">дом-2SG-DAT/LOC</ta>
            <ta e="T606" id="Seg_8334" s="T605">говорить-PRS.[3SG]</ta>
            <ta e="T607" id="Seg_8335" s="T606">больше</ta>
            <ta e="T608" id="Seg_8336" s="T607">приходить-EP-NEG.[IMP.2SG]</ta>
            <ta e="T611" id="Seg_8337" s="T610">недавно</ta>
            <ta e="T612" id="Seg_8338" s="T611">1SG-ACC</ta>
            <ta e="T613" id="Seg_8339" s="T612">тоже</ta>
            <ta e="T614" id="Seg_8340" s="T613">вот</ta>
            <ta e="T615" id="Seg_8341" s="T614">мучить-PST2-EP-2SG</ta>
            <ta e="T616" id="Seg_8342" s="T615">тот-3SG-3SG.[NOM]</ta>
            <ta e="T617" id="Seg_8343" s="T616">кончаться-FUT.[3SG]</ta>
            <ta e="T618" id="Seg_8344" s="T617">MOD</ta>
            <ta e="T619" id="Seg_8345" s="T618">идти-CVB.SEQ</ta>
            <ta e="T620" id="Seg_8346" s="T619">оставаться-PST2.[3SG]</ta>
            <ta e="T621" id="Seg_8347" s="T620">вот</ta>
            <ta e="T622" id="Seg_8348" s="T621">идти-CVB.SEQ</ta>
            <ta e="T623" id="Seg_8349" s="T622">оставаться-PST2.[3SG]</ta>
            <ta e="T624" id="Seg_8350" s="T623">дом-3SG-DAT/LOC</ta>
            <ta e="T625" id="Seg_8351" s="T624">опять</ta>
            <ta e="T627" id="Seg_8352" s="T626">идти-PST2.[3SG]</ta>
            <ta e="T628" id="Seg_8353" s="T627">да</ta>
            <ta e="T629" id="Seg_8354" s="T628">следующее.утро-3SG-ACC</ta>
            <ta e="T632" id="Seg_8355" s="T631">сколько</ta>
            <ta e="T633" id="Seg_8356" s="T632">INDEF</ta>
            <ta e="T634" id="Seg_8357" s="T633">долго</ta>
            <ta e="T635" id="Seg_8358" s="T634">день-ACC</ta>
            <ta e="T636" id="Seg_8359" s="T635">быть-CVB.SEQ</ta>
            <ta e="T637" id="Seg_8360" s="T636">после</ta>
            <ta e="T638" id="Seg_8361" s="T637">опять</ta>
            <ta e="T639" id="Seg_8362" s="T638">приходить-PRS.[3SG]</ta>
            <ta e="T640" id="Seg_8363" s="T639">вот</ta>
            <ta e="T641" id="Seg_8364" s="T640">вовсе</ta>
            <ta e="T642" id="Seg_8365" s="T641">умирать-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T643" id="Seg_8366" s="T642">идти-PST2.[3SG]</ta>
            <ta e="T644" id="Seg_8367" s="T643">наверное</ta>
            <ta e="T645" id="Seg_8368" s="T644">друг-3SG.[NOM]</ta>
            <ta e="T646" id="Seg_8369" s="T645">бабочка-3SG.[NOM]</ta>
            <ta e="T647" id="Seg_8370" s="T646">доходить.до.крайней.степени-PST2.[3SG]</ta>
            <ta e="T648" id="Seg_8371" s="T647">гнать-TEMP-3SG</ta>
            <ta e="T649" id="Seg_8372" s="T648">да</ta>
            <ta e="T650" id="Seg_8373" s="T649">идти-NEG.[3SG]</ta>
            <ta e="T651" id="Seg_8374" s="T650">входить-TEMP-3SG</ta>
            <ta e="T652" id="Seg_8375" s="T651">приходить-NEG.[3SG]</ta>
            <ta e="T653" id="Seg_8376" s="T652">о</ta>
            <ta e="T654" id="Seg_8377" s="T653">вот</ta>
            <ta e="T655" id="Seg_8378" s="T654">как</ta>
            <ta e="T656" id="Seg_8379" s="T655">NEG</ta>
            <ta e="T657" id="Seg_8380" s="T656">делать-PTCP.FUT-3SG-ACC</ta>
            <ta e="T658" id="Seg_8381" s="T657">очень</ta>
            <ta e="T659" id="Seg_8382" s="T658">вот</ta>
            <ta e="T660" id="Seg_8383" s="T659">сердце-3SG.[NOM]</ta>
            <ta e="T661" id="Seg_8384" s="T660">боль.[NOM]</ta>
            <ta e="T662" id="Seg_8385" s="T661">быть.больным-PRS.[3SG]</ta>
            <ta e="T663" id="Seg_8386" s="T662">человек.[NOM]</ta>
            <ta e="T664" id="Seg_8387" s="T663">друг-3SG-ACC</ta>
            <ta e="T665" id="Seg_8388" s="T664">жалеть-PST1-3SG</ta>
            <ta e="T666" id="Seg_8389" s="T665">EMPH</ta>
            <ta e="T667" id="Seg_8390" s="T666">жалеть-CVB.SEQ</ta>
            <ta e="T668" id="Seg_8391" s="T667">как</ta>
            <ta e="T669" id="Seg_8392" s="T668">NEG</ta>
            <ta e="T670" id="Seg_8393" s="T669">делать-PTCP.FUT-3SG-ACC</ta>
            <ta e="T671" id="Seg_8394" s="T670">очень</ta>
            <ta e="T672" id="Seg_8395" s="T671">гнать-CAUS-PTCP.FUT-3SG-ACC</ta>
            <ta e="T673" id="Seg_8396" s="T672">да</ta>
            <ta e="T674" id="Seg_8397" s="T791">идти-CVB.SEQ</ta>
            <ta e="T675" id="Seg_8398" s="T674">да</ta>
            <ta e="T676" id="Seg_8399" s="T675">ведь</ta>
            <ta e="T677" id="Seg_8400" s="T676">дом-3SG-DAT/LOC</ta>
            <ta e="T678" id="Seg_8401" s="T677">доезжать-FUT.[3SG]</ta>
            <ta e="T680" id="Seg_8402" s="T678">NEG</ta>
            <ta e="T681" id="Seg_8403" s="T792">раз.так</ta>
            <ta e="T682" id="Seg_8404" s="T681">входить.[IMP.2SG]</ta>
            <ta e="T683" id="Seg_8405" s="T682">говорить-PST2.[3SG]</ta>
            <ta e="T684" id="Seg_8406" s="T683">есть.[IMP.2SG]</ta>
            <ta e="T685" id="Seg_8407" s="T684">разговаривать-IMP.1DU</ta>
            <ta e="T686" id="Seg_8408" s="T685">тогда</ta>
            <ta e="T688" id="Seg_8409" s="T686">хороший-ADVZ</ta>
            <ta e="T689" id="Seg_8410" s="T688">вот</ta>
            <ta e="T690" id="Seg_8411" s="T689">INTNS</ta>
            <ta e="T691" id="Seg_8412" s="T690">вправду</ta>
            <ta e="T692" id="Seg_8413" s="T691">разговаривать-PST2-3PL</ta>
            <ta e="T693" id="Seg_8414" s="T692">эй</ta>
            <ta e="T694" id="Seg_8415" s="T693">потом</ta>
            <ta e="T695" id="Seg_8416" s="T694">1PL.[NOM]</ta>
            <ta e="T696" id="Seg_8417" s="T695">хороший-ADVZ</ta>
            <ta e="T697" id="Seg_8418" s="T696">разговаривать-IMP.1DU</ta>
            <ta e="T698" id="Seg_8419" s="T697">вместе</ta>
            <ta e="T699" id="Seg_8420" s="T698">теперь</ta>
            <ta e="T700" id="Seg_8421" s="T699">вместе</ta>
            <ta e="T701" id="Seg_8422" s="T700">жить-FUT-1PL</ta>
            <ta e="T702" id="Seg_8423" s="T701">вот</ta>
            <ta e="T703" id="Seg_8424" s="T702">почему</ta>
            <ta e="T704" id="Seg_8425" s="T703">два</ta>
            <ta e="T705" id="Seg_8426" s="T704">сторона.[NOM]</ta>
            <ta e="T706" id="Seg_8427" s="T705">жить-PRS-1PL</ta>
            <ta e="T707" id="Seg_8428" s="T706">1PL.[NOM]</ta>
            <ta e="T708" id="Seg_8429" s="T707">видеть-PRS-2SG</ta>
            <ta e="T709" id="Seg_8430" s="T708">1SG.[NOM]</ta>
            <ta e="T710" id="Seg_8431" s="T709">олень-REFL/MED-PST1-1SG</ta>
            <ta e="T711" id="Seg_8432" s="T710">еще</ta>
            <ta e="T712" id="Seg_8433" s="T711">2SG.[NOM]</ta>
            <ta e="T713" id="Seg_8434" s="T712">недавно</ta>
            <ta e="T714" id="Seg_8435" s="T713">1SG-ACC</ta>
            <ta e="T715" id="Seg_8436" s="T714">обижать-CVB.SIM</ta>
            <ta e="T717" id="Seg_8437" s="T716">что.[NOM]</ta>
            <ta e="T718" id="Seg_8438" s="T717">олень-3SG.[NOM]</ta>
            <ta e="T719" id="Seg_8439" s="T718">Бог.[NOM]</ta>
            <ta e="T720" id="Seg_8440" s="T719">принести-PST2-3SG</ta>
            <ta e="T721" id="Seg_8441" s="T720">MOD</ta>
            <ta e="T722" id="Seg_8442" s="T721">олень-ACC</ta>
            <ta e="T723" id="Seg_8443" s="T722">есть-EP-CAUS-PST1-1SG</ta>
            <ta e="T724" id="Seg_8444" s="T723">вот</ta>
            <ta e="T725" id="Seg_8445" s="T724">теперь</ta>
            <ta e="T726" id="Seg_8446" s="T725">олень-REFL/MED-CVB.SIM</ta>
            <ta e="T727" id="Seg_8447" s="T726">вместе</ta>
            <ta e="T728" id="Seg_8448" s="T727">жить-IMP.1DU</ta>
            <ta e="T729" id="Seg_8449" s="T728">1PL.[NOM]</ta>
            <ta e="T730" id="Seg_8450" s="T729">теперь</ta>
            <ta e="T731" id="Seg_8451" s="T730">вместе</ta>
            <ta e="T732" id="Seg_8452" s="T731">жить-CVB.SEQ-1PL</ta>
            <ta e="T733" id="Seg_8453" s="T732">один</ta>
            <ta e="T734" id="Seg_8454" s="T733">совет-INSTR</ta>
            <ta e="T735" id="Seg_8455" s="T734">охотиться-IMP.1DU</ta>
            <ta e="T736" id="Seg_8456" s="T735">и</ta>
            <ta e="T737" id="Seg_8457" s="T736">один</ta>
            <ta e="T738" id="Seg_8458" s="T737">дом-PROPR.[NOM]</ta>
            <ta e="T739" id="Seg_8459" s="T738">быть-IMP.1DU</ta>
            <ta e="T740" id="Seg_8460" s="T739">2SG.[NOM]</ta>
            <ta e="T741" id="Seg_8461" s="T740">дом-2SG-ACC</ta>
            <ta e="T742" id="Seg_8462" s="T741">перевозить-CVB.SIM</ta>
            <ta e="T743" id="Seg_8463" s="T742">принести-IMP.1DU</ta>
            <ta e="T744" id="Seg_8464" s="T743">этот</ta>
            <ta e="T745" id="Seg_8465" s="T744">олень-PROPR-1SG</ta>
            <ta e="T746" id="Seg_8466" s="T745">EMPH</ta>
            <ta e="T747" id="Seg_8467" s="T746">этот</ta>
            <ta e="T748" id="Seg_8468" s="T747">один</ta>
            <ta e="T749" id="Seg_8469" s="T748">олень-1PL.[NOM]</ta>
            <ta e="T750" id="Seg_8470" s="T749">тянуть-EP-CAUS-CVB.SEQ</ta>
            <ta e="T751" id="Seg_8471" s="T750">принести-PRS.[3SG]</ta>
            <ta e="T752" id="Seg_8472" s="T751">что-PROPR</ta>
            <ta e="T753" id="Seg_8473" s="T752">быть-FUT.[3SG]-Q</ta>
            <ta e="T754" id="Seg_8474" s="T753">EMPH</ta>
            <ta e="T755" id="Seg_8475" s="T754">тот.[NOM]</ta>
            <ta e="T756" id="Seg_8476" s="T755">принести-IMP.1DU</ta>
            <ta e="T757" id="Seg_8477" s="T756">сюда</ta>
            <ta e="T758" id="Seg_8478" s="T757">олень-2SG-ACC</ta>
            <ta e="T759" id="Seg_8479" s="T758">ээ</ta>
            <ta e="T760" id="Seg_8480" s="T759">кто-2SG-ACC</ta>
            <ta e="T761" id="Seg_8481" s="T760">здесь</ta>
            <ta e="T762" id="Seg_8482" s="T761">один</ta>
            <ta e="T763" id="Seg_8483" s="T762">вместе</ta>
            <ta e="T764" id="Seg_8484" s="T763">жить-IMP.1DU</ta>
            <ta e="T765" id="Seg_8485" s="T764">1PL.[NOM]</ta>
            <ta e="T766" id="Seg_8486" s="T765">советовать-EP-RECP/COLL-CVB.SEQ-3PL</ta>
            <ta e="T767" id="Seg_8487" s="T766">вот</ta>
            <ta e="T768" id="Seg_8488" s="T767">вместе</ta>
            <ta e="T769" id="Seg_8489" s="T768">жить-CVB.SEQ</ta>
            <ta e="T770" id="Seg_8490" s="T769">оставаться-PST2-3PL</ta>
            <ta e="T771" id="Seg_8491" s="T770">два-COLL-3PL.[NOM]</ta>
            <ta e="T772" id="Seg_8492" s="T771">один</ta>
            <ta e="T773" id="Seg_8493" s="T772">совет.[NOM]</ta>
            <ta e="T774" id="Seg_8494" s="T773">быть-CVB.SEQ-3PL</ta>
            <ta e="T775" id="Seg_8495" s="T774">способ.[NOM]</ta>
            <ta e="T776" id="Seg_8496" s="T775">быть-CVB.SEQ-3PL</ta>
            <ta e="T777" id="Seg_8497" s="T776">два-COLL-3PL.[NOM]</ta>
            <ta e="T778" id="Seg_8498" s="T777">вот</ta>
            <ta e="T779" id="Seg_8499" s="T778">быть.богатым-CVB.SEQ-наесться-CVB.SEQ</ta>
            <ta e="T780" id="Seg_8500" s="T779">жить-PST2-3PL</ta>
            <ta e="T781" id="Seg_8501" s="T780">последний-3SG.[NOM]</ta>
            <ta e="T782" id="Seg_8502" s="T781">тот.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_8503" s="T0">n-n:case</ta>
            <ta e="T2" id="Seg_8504" s="T1">post</ta>
            <ta e="T3" id="Seg_8505" s="T2">propr</ta>
            <ta e="T4" id="Seg_8506" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_8507" s="T4">v-v:tense-v:pred.pn</ta>
            <ta e="T6" id="Seg_8508" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_8509" s="T6">adj-adj&gt;adv</ta>
            <ta e="T8" id="Seg_8510" s="T7">v-v:tense-v:pred.pn</ta>
            <ta e="T9" id="Seg_8511" s="T8">emphpro</ta>
            <ta e="T10" id="Seg_8512" s="T9">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T11" id="Seg_8513" s="T10">v-v&gt;v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T12" id="Seg_8514" s="T11">adj-n:(poss)-n:case</ta>
            <ta e="T13" id="Seg_8515" s="T12">dempro-pro:case</ta>
            <ta e="T14" id="Seg_8516" s="T13">post</ta>
            <ta e="T15" id="Seg_8517" s="T14">n-n&gt;adj-n:case</ta>
            <ta e="T16" id="Seg_8518" s="T15">adj-n:(poss)-n:case</ta>
            <ta e="T17" id="Seg_8519" s="T16">dempro-pro:case</ta>
            <ta e="T18" id="Seg_8520" s="T17">post</ta>
            <ta e="T19" id="Seg_8521" s="T18">n-n&gt;adj-n:case</ta>
            <ta e="T20" id="Seg_8522" s="T19">v-v:cvb-v:pred.pn</ta>
            <ta e="T21" id="Seg_8523" s="T20">cardnum-cardnum&gt;collnum-n:(poss)-n:case</ta>
            <ta e="T22" id="Seg_8524" s="T21">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T23" id="Seg_8525" s="T22">adv</ta>
            <ta e="T24" id="Seg_8526" s="T23">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T25" id="Seg_8527" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_8528" s="T25">v-v:tense-v:pred.pn</ta>
            <ta e="T27" id="Seg_8529" s="T26">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T28" id="Seg_8530" s="T27">n-n:poss-n:case</ta>
            <ta e="T29" id="Seg_8531" s="T28">post</ta>
            <ta e="T30" id="Seg_8532" s="T29">propr</ta>
            <ta e="T31" id="Seg_8533" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_8534" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_8535" s="T32">pers-pro:case</ta>
            <ta e="T36" id="Seg_8536" s="T35">cardnum</ta>
            <ta e="T37" id="Seg_8537" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_8538" s="T37">v-v:ptcp</ta>
            <ta e="T39" id="Seg_8539" s="T38">n-n:poss-n:case</ta>
            <ta e="T40" id="Seg_8540" s="T39">adv</ta>
            <ta e="T41" id="Seg_8541" s="T40">v-v:cvb-v:pred.pn</ta>
            <ta e="T42" id="Seg_8542" s="T41">v-v:mood.pn</ta>
            <ta e="T43" id="Seg_8543" s="T42">v-v:mood-v:temp.pn</ta>
            <ta e="T44" id="Seg_8544" s="T43">ptcl</ta>
            <ta e="T47" id="Seg_8545" s="T46">adj-adj&gt;adv</ta>
            <ta e="T48" id="Seg_8546" s="T47">v-v:tense-v:poss.pn</ta>
            <ta e="T49" id="Seg_8547" s="T48">ptcl</ta>
            <ta e="T50" id="Seg_8548" s="T49">n-n:poss-n:case</ta>
            <ta e="T51" id="Seg_8549" s="T50">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T52" id="Seg_8550" s="T51">ptcl</ta>
            <ta e="T53" id="Seg_8551" s="T52">dempro</ta>
            <ta e="T54" id="Seg_8552" s="T53">adj</ta>
            <ta e="T55" id="Seg_8553" s="T54">n-n:(num)-n:case</ta>
            <ta e="T56" id="Seg_8554" s="T55">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T57" id="Seg_8555" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_8556" s="T57">interj</ta>
            <ta e="T59" id="Seg_8557" s="T58">adv</ta>
            <ta e="T60" id="Seg_8558" s="T59">adv</ta>
            <ta e="T61" id="Seg_8559" s="T60">pers-pro:case</ta>
            <ta e="T62" id="Seg_8560" s="T61">n-n:case</ta>
            <ta e="T63" id="Seg_8561" s="T62">posspr</ta>
            <ta e="T64" id="Seg_8562" s="T783">n-n:poss-n:case</ta>
            <ta e="T66" id="Seg_8563" s="T64">v-v:mood.pn</ta>
            <ta e="T67" id="Seg_8564" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_8565" s="T67">propr</ta>
            <ta e="T69" id="Seg_8566" s="T68">n-n:case</ta>
            <ta e="T70" id="Seg_8567" s="T69">pers-pro:case</ta>
            <ta e="T71" id="Seg_8568" s="T70">dempro</ta>
            <ta e="T72" id="Seg_8569" s="T71">posspr</ta>
            <ta e="T73" id="Seg_8570" s="T72">n-n:poss-n:case</ta>
            <ta e="T74" id="Seg_8571" s="T73">v-v:cvb-v:pred.pn</ta>
            <ta e="T75" id="Seg_8572" s="T74">v-v:mood.pn</ta>
            <ta e="T76" id="Seg_8573" s="T75">ptcl</ta>
            <ta e="T77" id="Seg_8574" s="T76">interj</ta>
            <ta e="T78" id="Seg_8575" s="T77">adv</ta>
            <ta e="T79" id="Seg_8576" s="T78">v-v:mood.pn</ta>
            <ta e="T80" id="Seg_8577" s="T79">interj</ta>
            <ta e="T81" id="Seg_8578" s="T80">ptcl</ta>
            <ta e="T82" id="Seg_8579" s="T81">v-v:tense-v:pred.pn</ta>
            <ta e="T83" id="Seg_8580" s="T82">ptcl</ta>
            <ta e="T84" id="Seg_8581" s="T83">n-n:case</ta>
            <ta e="T85" id="Seg_8582" s="T84">n-n:poss-n:case</ta>
            <ta e="T86" id="Seg_8583" s="T85">v-v:tense-v:pred.pn</ta>
            <ta e="T87" id="Seg_8584" s="T86">cardnum</ta>
            <ta e="T88" id="Seg_8585" s="T87">n-n:case</ta>
            <ta e="T89" id="Seg_8586" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_8587" s="T89">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T91" id="Seg_8588" s="T90">post</ta>
            <ta e="T92" id="Seg_8589" s="T91">ptcl</ta>
            <ta e="T93" id="Seg_8590" s="T92">v-v:cvb-v:pred.pn</ta>
            <ta e="T94" id="Seg_8591" s="T93">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T95" id="Seg_8592" s="T94">n-n:(poss)-n:case</ta>
            <ta e="T96" id="Seg_8593" s="T95">v-v:tense-v:poss.pn</ta>
            <ta e="T97" id="Seg_8594" s="T96">n-n:case</ta>
            <ta e="T98" id="Seg_8595" s="T97">adj-n:(poss)-n:case</ta>
            <ta e="T99" id="Seg_8596" s="T98">ptcl</ta>
            <ta e="T100" id="Seg_8597" s="T99">propr</ta>
            <ta e="T101" id="Seg_8598" s="T100">n-n:case</ta>
            <ta e="T102" id="Seg_8599" s="T101">v-v:mood.pn</ta>
            <ta e="T103" id="Seg_8600" s="T102">v-v:cvb-v:pred.pn</ta>
            <ta e="T104" id="Seg_8601" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_8602" s="T104">dempro</ta>
            <ta e="T106" id="Seg_8603" s="T105">n-n:poss-n:case</ta>
            <ta e="T107" id="Seg_8604" s="T106">v-v:mood-v:pred.pn</ta>
            <ta e="T108" id="Seg_8605" s="T107">interj</ta>
            <ta e="T109" id="Seg_8606" s="T108">propr</ta>
            <ta e="T110" id="Seg_8607" s="T109">n-n:case</ta>
            <ta e="T111" id="Seg_8608" s="T110">v-v:tense-v:pred.pn</ta>
            <ta e="T113" id="Seg_8609" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_8610" s="T113">v-v:tense-v:pred.pn</ta>
            <ta e="T115" id="Seg_8611" s="T114">v-v:tense-v:pred.pn-v-v:tense-v:pred.pn</ta>
            <ta e="T116" id="Seg_8612" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_8613" s="T116">interj</ta>
            <ta e="T118" id="Seg_8614" s="T117">cardnum</ta>
            <ta e="T119" id="Seg_8615" s="T118">n-n:(poss)-n:case</ta>
            <ta e="T120" id="Seg_8616" s="T119">posspr</ta>
            <ta e="T121" id="Seg_8617" s="T120">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T122" id="Seg_8618" s="T121">v-v:ptcp</ta>
            <ta e="T123" id="Seg_8619" s="T122">v-v:neg-v:pred.pn</ta>
            <ta e="T124" id="Seg_8620" s="T784">v-v:tense-v:pred.pn</ta>
            <ta e="T125" id="Seg_8621" s="T124">pers-pro:case</ta>
            <ta e="T126" id="Seg_8622" s="T125">n-n:poss-n:case</ta>
            <ta e="T128" id="Seg_8623" s="T126">v-v:mood.pn</ta>
            <ta e="T129" id="Seg_8624" s="T128">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T130" id="Seg_8625" s="T129">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T131" id="Seg_8626" s="T130">pers-pro:case</ta>
            <ta e="T132" id="Seg_8627" s="T131">n-n:poss-n:case</ta>
            <ta e="T133" id="Seg_8628" s="T132">adj-n:poss-n:case</ta>
            <ta e="T134" id="Seg_8629" s="T133">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T135" id="Seg_8630" s="T134">v-v:(neg)-v:pred.pn</ta>
            <ta e="T136" id="Seg_8631" s="T135">ptcl</ta>
            <ta e="T137" id="Seg_8632" s="T136">ptcl</ta>
            <ta e="T138" id="Seg_8633" s="T137">v-v:cvb</ta>
            <ta e="T139" id="Seg_8634" s="T138">v-v:tense-v:poss.pn</ta>
            <ta e="T140" id="Seg_8635" s="T139">n-n:(poss)-n:case</ta>
            <ta e="T141" id="Seg_8636" s="T140">interj</ta>
            <ta e="T142" id="Seg_8637" s="T141">v-v:cvb</ta>
            <ta e="T143" id="Seg_8638" s="T142">que-pro:case</ta>
            <ta e="T144" id="Seg_8639" s="T143">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T145" id="Seg_8640" s="T144">n-n:poss-n:case</ta>
            <ta e="T146" id="Seg_8641" s="T145">n-n:case</ta>
            <ta e="T147" id="Seg_8642" s="T146">v-v:tense-v:poss.pn</ta>
            <ta e="T148" id="Seg_8643" s="T147">n-n:poss-n:case</ta>
            <ta e="T149" id="Seg_8644" s="T148">que-pro:(poss)-pro:case</ta>
            <ta e="T150" id="Seg_8645" s="T149">ptcl</ta>
            <ta e="T151" id="Seg_8646" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_8647" s="T151">que-pro:case</ta>
            <ta e="T153" id="Seg_8648" s="T152">ptcl</ta>
            <ta e="T154" id="Seg_8649" s="T153">que</ta>
            <ta e="T155" id="Seg_8650" s="T154">n-n&gt;n-n:case</ta>
            <ta e="T156" id="Seg_8651" s="T155">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T157" id="Seg_8652" s="T156">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T158" id="Seg_8653" s="T157">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T159" id="Seg_8654" s="T158">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T160" id="Seg_8655" s="T159">v-v:tense-v:pred.pn</ta>
            <ta e="T161" id="Seg_8656" s="T160">ptcl</ta>
            <ta e="T162" id="Seg_8657" s="T161">ptcl</ta>
            <ta e="T163" id="Seg_8658" s="T162">adv</ta>
            <ta e="T164" id="Seg_8659" s="T163">v-v:tense-v:poss.pn</ta>
            <ta e="T165" id="Seg_8660" s="T164">que-pro:case</ta>
            <ta e="T166" id="Seg_8661" s="T165">ptcl</ta>
            <ta e="T167" id="Seg_8662" s="T166">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T168" id="Seg_8663" s="T167">ptcl</ta>
            <ta e="T169" id="Seg_8664" s="T168">n-n:case</ta>
            <ta e="T170" id="Seg_8665" s="T169">adv</ta>
            <ta e="T171" id="Seg_8666" s="T170">adj-n:case</ta>
            <ta e="T172" id="Seg_8667" s="T171">propr</ta>
            <ta e="T173" id="Seg_8668" s="T172">n-n:case</ta>
            <ta e="T174" id="Seg_8669" s="T173">conj</ta>
            <ta e="T175" id="Seg_8670" s="T174">n-n:(poss)-n:case</ta>
            <ta e="T176" id="Seg_8671" s="T175">v-v:ptcp</ta>
            <ta e="T177" id="Seg_8672" s="T176">v-v:tense-v:poss.pn</ta>
            <ta e="T178" id="Seg_8673" s="T177">adv</ta>
            <ta e="T179" id="Seg_8674" s="T178">ptcl</ta>
            <ta e="T180" id="Seg_8675" s="T179">v-v:tense-v:pred.pn</ta>
            <ta e="T181" id="Seg_8676" s="T180">v-v&gt;v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T182" id="Seg_8677" s="T181">pers-pro:case</ta>
            <ta e="T183" id="Seg_8678" s="T182">ptcl</ta>
            <ta e="T184" id="Seg_8679" s="T183">v-v:(neg)-v:mood.pn</ta>
            <ta e="T185" id="Seg_8680" s="T184">pers-pro:case</ta>
            <ta e="T186" id="Seg_8681" s="T185">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T187" id="Seg_8682" s="T186">ptcl</ta>
            <ta e="T188" id="Seg_8683" s="T187">n-n&gt;v-v:cvb</ta>
            <ta e="T189" id="Seg_8684" s="T188">v-v:cvb</ta>
            <ta e="T190" id="Seg_8685" s="T189">cardnum</ta>
            <ta e="T191" id="Seg_8686" s="T190">que-pro&gt;pro-pro:case</ta>
            <ta e="T192" id="Seg_8687" s="T191">ptcl</ta>
            <ta e="T193" id="Seg_8688" s="T192">v-v:tense-v:pred.pn</ta>
            <ta e="T194" id="Seg_8689" s="T193">n-n:case</ta>
            <ta e="T195" id="Seg_8690" s="T194">ptcl</ta>
            <ta e="T196" id="Seg_8691" s="T195">v-v:tense-v:pred.pn</ta>
            <ta e="T197" id="Seg_8692" s="T196">dempro</ta>
            <ta e="T198" id="Seg_8693" s="T197">v-v:mood-v:temp.pn</ta>
            <ta e="T199" id="Seg_8694" s="T198">propr</ta>
            <ta e="T200" id="Seg_8695" s="T199">n-n:case</ta>
            <ta e="T201" id="Seg_8696" s="T200">v-v:tense-v:pred.pn</ta>
            <ta e="T202" id="Seg_8697" s="T201">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T203" id="Seg_8698" s="T202">v-v:tense-v:poss.pn</ta>
            <ta e="T204" id="Seg_8699" s="T203">n-n:poss-n:case</ta>
            <ta e="T205" id="Seg_8700" s="T204">interj</ta>
            <ta e="T206" id="Seg_8701" s="T205">propr</ta>
            <ta e="T207" id="Seg_8702" s="T206">n-n:case</ta>
            <ta e="T208" id="Seg_8703" s="T207">v-v:ptcp</ta>
            <ta e="T209" id="Seg_8704" s="T208">v-v:tense-v:pred.pn</ta>
            <ta e="T210" id="Seg_8705" s="T209">n-n:case</ta>
            <ta e="T211" id="Seg_8706" s="T210">v-v:tense-v:pred.pn</ta>
            <ta e="T212" id="Seg_8707" s="T785">n-n:poss-n:case</ta>
            <ta e="T213" id="Seg_8708" s="T212">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T214" id="Seg_8709" s="T213">v-v:cvb</ta>
            <ta e="T216" id="Seg_8710" s="T214">v-v:tense-v:pred.pn</ta>
            <ta e="T217" id="Seg_8711" s="T216">ptcl</ta>
            <ta e="T218" id="Seg_8712" s="T217">v-v:tense-v:pred.pn</ta>
            <ta e="T219" id="Seg_8713" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_8714" s="T219">interj</ta>
            <ta e="T223" id="Seg_8715" s="T222">n-n:poss-n:case</ta>
            <ta e="T224" id="Seg_8716" s="T223">v-v:tense-v:pred.pn</ta>
            <ta e="T225" id="Seg_8717" s="T224">n-n:case</ta>
            <ta e="T226" id="Seg_8718" s="T225">n-n&gt;v-v:mood.pn</ta>
            <ta e="T227" id="Seg_8719" s="T226">n-n:case</ta>
            <ta e="T228" id="Seg_8720" s="T227">n-n&gt;v-v:mood.pn</ta>
            <ta e="T229" id="Seg_8721" s="T228">pers-pro:case</ta>
            <ta e="T230" id="Seg_8722" s="T229">v-v:mood.pn</ta>
            <ta e="T231" id="Seg_8723" s="T230">ptcl</ta>
            <ta e="T232" id="Seg_8724" s="T786">n-n&gt;n-n:case</ta>
            <ta e="T233" id="Seg_8725" s="T232">v-v:cvb</ta>
            <ta e="T234" id="Seg_8726" s="T233">v-v:mood.pn</ta>
            <ta e="T235" id="Seg_8727" s="T234">n-n:case</ta>
            <ta e="T237" id="Seg_8728" s="T235">n-n&gt;v-v:mood.pn</ta>
            <ta e="T238" id="Seg_8729" s="T237">v-v:cvb</ta>
            <ta e="T239" id="Seg_8730" s="T238">n-n:poss-n:case</ta>
            <ta e="T240" id="Seg_8731" s="T239">v-v:tense-v:poss.pn</ta>
            <ta e="T241" id="Seg_8732" s="T240">n-n:poss-n:case</ta>
            <ta e="T242" id="Seg_8733" s="T241">v</ta>
            <ta e="T243" id="Seg_8734" s="T242">n-n:case</ta>
            <ta e="T244" id="Seg_8735" s="T243">n-n&gt;v-v:mood.pn</ta>
            <ta e="T245" id="Seg_8736" s="T244">n-n:case</ta>
            <ta e="T246" id="Seg_8737" s="T245">n-n&gt;v-v:mood.pn</ta>
            <ta e="T247" id="Seg_8738" s="T246">v-v:tense-v:poss.pn</ta>
            <ta e="T248" id="Seg_8739" s="T247">v-v:tense-v:poss.pn</ta>
            <ta e="T249" id="Seg_8740" s="T248">cardnum-n:case</ta>
            <ta e="T250" id="Seg_8741" s="T249">v-v:cvb</ta>
            <ta e="T251" id="Seg_8742" s="T250">v-v:tense-v:pred.pn</ta>
            <ta e="T252" id="Seg_8743" s="T251">adv</ta>
            <ta e="T253" id="Seg_8744" s="T252">ptcl</ta>
            <ta e="T254" id="Seg_8745" s="T253">v-v:mood.pn-v-v:mood.pn</ta>
            <ta e="T255" id="Seg_8746" s="T254">cardnum</ta>
            <ta e="T256" id="Seg_8747" s="T255">ptcl</ta>
            <ta e="T257" id="Seg_8748" s="T256">cardnum</ta>
            <ta e="T258" id="Seg_8749" s="T257">que-pro:case</ta>
            <ta e="T259" id="Seg_8750" s="T258">n-n:case</ta>
            <ta e="T260" id="Seg_8751" s="T259">v-v:(ins)-v:ptcp</ta>
            <ta e="T262" id="Seg_8752" s="T261">v-v:ptcp</ta>
            <ta e="T263" id="Seg_8753" s="T262">n-n&gt;n-n:case</ta>
            <ta e="T264" id="Seg_8754" s="T263">v-v:cvb</ta>
            <ta e="T265" id="Seg_8755" s="T264">v-v:tense-v:poss.pn</ta>
            <ta e="T266" id="Seg_8756" s="T265">v-v:mood.pn</ta>
            <ta e="T267" id="Seg_8757" s="T266">v-v:tense-v:pred.pn</ta>
            <ta e="T268" id="Seg_8758" s="T267">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T269" id="Seg_8759" s="T268">propr</ta>
            <ta e="T270" id="Seg_8760" s="T269">v-v:cvb</ta>
            <ta e="T271" id="Seg_8761" s="T270">v-v:tense-v:poss.pn</ta>
            <ta e="T272" id="Seg_8762" s="T271">v-v:mood.pn</ta>
            <ta e="T273" id="Seg_8763" s="T272">n-n:poss-n:case</ta>
            <ta e="T274" id="Seg_8764" s="T273">v-v:mood.pn</ta>
            <ta e="T275" id="Seg_8765" s="T274">n-n:poss-n:case</ta>
            <ta e="T276" id="Seg_8766" s="T275">v-v:tense-v:pred.pn</ta>
            <ta e="T277" id="Seg_8767" s="T276">adv</ta>
            <ta e="T278" id="Seg_8768" s="T277">v-v:(neg)-v:pred.pn</ta>
            <ta e="T279" id="Seg_8769" s="T278">pers-pro:case</ta>
            <ta e="T280" id="Seg_8770" s="T279">ptcl</ta>
            <ta e="T281" id="Seg_8771" s="T280">v-v:cvb</ta>
            <ta e="T282" id="Seg_8772" s="T281">v-v:tense-v:poss.pn</ta>
            <ta e="T283" id="Seg_8773" s="T282">n-n:case</ta>
            <ta e="T284" id="Seg_8774" s="T283">v-v:cvb</ta>
            <ta e="T285" id="Seg_8775" s="T284">quant-n:case</ta>
            <ta e="T286" id="Seg_8776" s="T285">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T287" id="Seg_8777" s="T286">pers-pro:case</ta>
            <ta e="T288" id="Seg_8778" s="T287">adv</ta>
            <ta e="T289" id="Seg_8779" s="T288">dempro-dempro</ta>
            <ta e="T290" id="Seg_8780" s="T289">ptcl</ta>
            <ta e="T291" id="Seg_8781" s="T290">n-n:poss-n:case</ta>
            <ta e="T292" id="Seg_8782" s="T291">adj-n:case</ta>
            <ta e="T293" id="Seg_8783" s="T292">adj</ta>
            <ta e="T294" id="Seg_8784" s="T293">n-n:poss-n:case</ta>
            <ta e="T295" id="Seg_8785" s="T294">ptcl</ta>
            <ta e="T298" id="Seg_8786" s="T297">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T299" id="Seg_8787" s="T298">v-v:tense-v:pred.pn</ta>
            <ta e="T300" id="Seg_8788" s="T299">ptcl</ta>
            <ta e="T301" id="Seg_8789" s="T300">que-pro:case</ta>
            <ta e="T302" id="Seg_8790" s="T301">ptcl</ta>
            <ta e="T303" id="Seg_8791" s="T302">v-v:(neg)-v:pred.pn</ta>
            <ta e="T304" id="Seg_8792" s="T303">conj</ta>
            <ta e="T305" id="Seg_8793" s="T304">v-v:(neg)-v:pred.pn</ta>
            <ta e="T306" id="Seg_8794" s="T305">ptcl</ta>
            <ta e="T307" id="Seg_8795" s="T306">que</ta>
            <ta e="T308" id="Seg_8796" s="T307">ptcl</ta>
            <ta e="T309" id="Seg_8797" s="T308">n-n:case</ta>
            <ta e="T310" id="Seg_8798" s="T309">v-v:tense-v:poss.pn</ta>
            <ta e="T311" id="Seg_8799" s="T310">ptcl</ta>
            <ta e="T312" id="Seg_8800" s="T311">ptcl</ta>
            <ta e="T313" id="Seg_8801" s="T312">ptcl</ta>
            <ta e="T314" id="Seg_8802" s="T313">v-v:cvb</ta>
            <ta e="T315" id="Seg_8803" s="T314">v-v:ptcp</ta>
            <ta e="T316" id="Seg_8804" s="T315">v-v:tense-v:pred.pn</ta>
            <ta e="T317" id="Seg_8805" s="T316">n-n:(ins)-n:case</ta>
            <ta e="T318" id="Seg_8806" s="T317">v-v:cvb</ta>
            <ta e="T319" id="Seg_8807" s="T318">propr</ta>
            <ta e="T320" id="Seg_8808" s="T319">n-n:case</ta>
            <ta e="T321" id="Seg_8809" s="T320">adv</ta>
            <ta e="T322" id="Seg_8810" s="T321">n-n:poss-n:case</ta>
            <ta e="T323" id="Seg_8811" s="T322">v-v:mood-v:pred.pn</ta>
            <ta e="T324" id="Seg_8812" s="T323">v-v:cvb</ta>
            <ta e="T325" id="Seg_8813" s="T324">n-n:poss-n:case</ta>
            <ta e="T326" id="Seg_8814" s="T325">ptcl</ta>
            <ta e="T327" id="Seg_8815" s="T326">v-v:tense-v:pred.pn</ta>
            <ta e="T328" id="Seg_8816" s="T327">n-n:case</ta>
            <ta e="T329" id="Seg_8817" s="T328">n-n&gt;v-v:mood.pn</ta>
            <ta e="T330" id="Seg_8818" s="T329">n-n:case</ta>
            <ta e="T331" id="Seg_8819" s="T330">n-n&gt;v-v:mood.pn</ta>
            <ta e="T332" id="Seg_8820" s="T787">v-v:tense-v:poss.pn</ta>
            <ta e="T333" id="Seg_8821" s="T332">v-v:tense-v:poss.pn</ta>
            <ta e="T334" id="Seg_8822" s="T333">v-v:tense-v:poss.pn</ta>
            <ta e="T336" id="Seg_8823" s="T334">v-v:tense-v:poss.pn</ta>
            <ta e="T337" id="Seg_8824" s="T336">n-n:case</ta>
            <ta e="T338" id="Seg_8825" s="T337">n-n&gt;v-v:mood.pn</ta>
            <ta e="T339" id="Seg_8826" s="T338">cardnum</ta>
            <ta e="T340" id="Seg_8827" s="T339">ptcl</ta>
            <ta e="T342" id="Seg_8828" s="T341">ptcl</ta>
            <ta e="T343" id="Seg_8829" s="T342">n-n&gt;n-n:case</ta>
            <ta e="T344" id="Seg_8830" s="T343">v-v:cvb</ta>
            <ta e="T345" id="Seg_8831" s="T344">v-v:mood.pn</ta>
            <ta e="T346" id="Seg_8832" s="T345">n-n:case</ta>
            <ta e="T347" id="Seg_8833" s="T346">n-n&gt;v-v:mood.pn</ta>
            <ta e="T348" id="Seg_8834" s="T347">n-n:case</ta>
            <ta e="T349" id="Seg_8835" s="T348">n-n&gt;v-v:mood.pn</ta>
            <ta e="T350" id="Seg_8836" s="T349">v-v:cvb</ta>
            <ta e="T351" id="Seg_8837" s="T350">v-v:cvb</ta>
            <ta e="T352" id="Seg_8838" s="T788">v-v:tense-v:poss.pn</ta>
            <ta e="T353" id="Seg_8839" s="T352">v-v:tense-v:poss.pn</ta>
            <ta e="T354" id="Seg_8840" s="T353">n-n:case</ta>
            <ta e="T355" id="Seg_8841" s="T354">n-n&gt;v-v:mood.pn</ta>
            <ta e="T356" id="Seg_8842" s="T355">v-v:cvb</ta>
            <ta e="T357" id="Seg_8843" s="T356">n-n:case</ta>
            <ta e="T358" id="Seg_8844" s="T357">v-v:mood.pn</ta>
            <ta e="T360" id="Seg_8845" s="T358">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T361" id="Seg_8846" s="T360">ptcl</ta>
            <ta e="T362" id="Seg_8847" s="T361">cardnum</ta>
            <ta e="T363" id="Seg_8848" s="T362">ptcl</ta>
            <ta e="T364" id="Seg_8849" s="T363">n-n:(poss)</ta>
            <ta e="T365" id="Seg_8850" s="T364">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T366" id="Seg_8851" s="T365">v-v:mood.pn</ta>
            <ta e="T367" id="Seg_8852" s="T366">v-v:mood.pn</ta>
            <ta e="T368" id="Seg_8853" s="T367">v-v:tense-v:pred.pn</ta>
            <ta e="T369" id="Seg_8854" s="T368">adv</ta>
            <ta e="T370" id="Seg_8855" s="T369">v-v:cvb</ta>
            <ta e="T371" id="Seg_8856" s="T370">v-v:tense-v:pred.pn</ta>
            <ta e="T372" id="Seg_8857" s="T371">que-pro&gt;pro-pro:case</ta>
            <ta e="T373" id="Seg_8858" s="T372">ptcl</ta>
            <ta e="T374" id="Seg_8859" s="T373">v-v:neg-v:pred.pn</ta>
            <ta e="T375" id="Seg_8860" s="T374">que</ta>
            <ta e="T376" id="Seg_8861" s="T375">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T377" id="Seg_8862" s="T376">adv</ta>
            <ta e="T378" id="Seg_8863" s="T377">adv</ta>
            <ta e="T379" id="Seg_8864" s="T378">v-v:ptcp</ta>
            <ta e="T380" id="Seg_8865" s="T379">v-v:tense-v:poss.pn</ta>
            <ta e="T381" id="Seg_8866" s="T380">que</ta>
            <ta e="T382" id="Seg_8867" s="T381">n-n:case</ta>
            <ta e="T383" id="Seg_8868" s="T382">ptcl</ta>
            <ta e="T384" id="Seg_8869" s="T383">v-v&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T385" id="Seg_8870" s="T384">n-n:case</ta>
            <ta e="T386" id="Seg_8871" s="T385">ptcl</ta>
            <ta e="T387" id="Seg_8872" s="T386">v-v:tense-v:pred.pn</ta>
            <ta e="T388" id="Seg_8873" s="T387">dempro</ta>
            <ta e="T389" id="Seg_8874" s="T388">n-n:case</ta>
            <ta e="T390" id="Seg_8875" s="T389">v-v:tense-v:poss.pn</ta>
            <ta e="T391" id="Seg_8876" s="T390">adv</ta>
            <ta e="T392" id="Seg_8877" s="T391">adv</ta>
            <ta e="T393" id="Seg_8878" s="T392">v-v:cvb</ta>
            <ta e="T394" id="Seg_8879" s="T393">v-v:mood-v:temp.pn</ta>
            <ta e="T397" id="Seg_8880" s="T396">n-n:poss-n:case</ta>
            <ta e="T398" id="Seg_8881" s="T397">n-n:case</ta>
            <ta e="T399" id="Seg_8882" s="T398">v-v:tense-v:pred.pn</ta>
            <ta e="T400" id="Seg_8883" s="T399">v-v:tense-v:poss.pn</ta>
            <ta e="T401" id="Seg_8884" s="T400">n-n&gt;adj</ta>
            <ta e="T402" id="Seg_8885" s="T401">n-n:case</ta>
            <ta e="T403" id="Seg_8886" s="T402">v-v:tense-v:pred.pn</ta>
            <ta e="T404" id="Seg_8887" s="T403">n-n:poss-n:case</ta>
            <ta e="T405" id="Seg_8888" s="T404">que</ta>
            <ta e="T406" id="Seg_8889" s="T405">v-v&gt;n-n:case</ta>
            <ta e="T407" id="Seg_8890" s="T406">v-v:tense-v:poss.pn</ta>
            <ta e="T408" id="Seg_8891" s="T407">ptcl</ta>
            <ta e="T409" id="Seg_8892" s="T408">v-v:(neg)-v:pred.pn</ta>
            <ta e="T410" id="Seg_8893" s="T409">n-n:case</ta>
            <ta e="T411" id="Seg_8894" s="T410">pers-pro:case</ta>
            <ta e="T412" id="Seg_8895" s="T789">n-n:case</ta>
            <ta e="T413" id="Seg_8896" s="T412">v-v:tense-v:pred.pn</ta>
            <ta e="T414" id="Seg_8897" s="T413">ptcl</ta>
            <ta e="T416" id="Seg_8898" s="T414">v-v:tense-v:pred.pn</ta>
            <ta e="T417" id="Seg_8899" s="T416">adv</ta>
            <ta e="T418" id="Seg_8900" s="T417">pers-pro:case</ta>
            <ta e="T419" id="Seg_8901" s="T418">v-v:ptcp</ta>
            <ta e="T420" id="Seg_8902" s="T419">n-n:poss-n:case</ta>
            <ta e="T423" id="Seg_8903" s="T422">v-v:ptcp</ta>
            <ta e="T424" id="Seg_8904" s="T423">n-n:poss-n:case</ta>
            <ta e="T425" id="Seg_8905" s="T424">n-n&gt;adj</ta>
            <ta e="T426" id="Seg_8906" s="T425">n-n:(poss)-n:case</ta>
            <ta e="T427" id="Seg_8907" s="T426">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T428" id="Seg_8908" s="T427">v-v:cvb</ta>
            <ta e="T429" id="Seg_8909" s="T428">v-v:cvb</ta>
            <ta e="T430" id="Seg_8910" s="T429">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T431" id="Seg_8911" s="T430">pers-pro:case</ta>
            <ta e="T432" id="Seg_8912" s="T431">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T433" id="Seg_8913" s="T432">n-n:poss-n:case</ta>
            <ta e="T434" id="Seg_8914" s="T433">v-v:tense-v:poss.pn</ta>
            <ta e="T435" id="Seg_8915" s="T434">conj</ta>
            <ta e="T436" id="Seg_8916" s="T435">n-n&gt;n-n:poss-n:case</ta>
            <ta e="T437" id="Seg_8917" s="T436">v-v:cvb</ta>
            <ta e="T438" id="Seg_8918" s="T437">v-v:cvb</ta>
            <ta e="T439" id="Seg_8919" s="T438">n-n&gt;n-n:case</ta>
            <ta e="T440" id="Seg_8920" s="T439">v-v:tense-v:poss.pn</ta>
            <ta e="T441" id="Seg_8921" s="T440">conj</ta>
            <ta e="T442" id="Seg_8922" s="T441">interj</ta>
            <ta e="T443" id="Seg_8923" s="T442">v-v&gt;v-v:cvb</ta>
            <ta e="T444" id="Seg_8924" s="T443">v-v:cvb</ta>
            <ta e="T445" id="Seg_8925" s="T444">v-v:tense-v:pred.pn</ta>
            <ta e="T446" id="Seg_8926" s="T445">n-n:(poss)-n:case</ta>
            <ta e="T447" id="Seg_8927" s="T446">v-v:tense-v:pred.pn</ta>
            <ta e="T448" id="Seg_8928" s="T447">ptcl</ta>
            <ta e="T449" id="Seg_8929" s="T448">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T450" id="Seg_8930" s="T449">que-pro:case</ta>
            <ta e="T451" id="Seg_8931" s="T450">que-pro:case</ta>
            <ta e="T454" id="Seg_8932" s="T453">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T455" id="Seg_8933" s="T454">n-n:case</ta>
            <ta e="T456" id="Seg_8934" s="T455">adj-adj&gt;v-v:cvb</ta>
            <ta e="T457" id="Seg_8935" s="T456">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T458" id="Seg_8936" s="T457">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T459" id="Seg_8937" s="T458">v-v:cvb</ta>
            <ta e="T460" id="Seg_8938" s="T459">v-v:tense-v:pred.pn</ta>
            <ta e="T461" id="Seg_8939" s="T460">ptcl</ta>
            <ta e="T462" id="Seg_8940" s="T461">n-n:(poss)-n:case</ta>
            <ta e="T463" id="Seg_8941" s="T462">v-v:tense-v:pred.pn</ta>
            <ta e="T464" id="Seg_8942" s="T463">adv</ta>
            <ta e="T465" id="Seg_8943" s="T464">interj</ta>
            <ta e="T466" id="Seg_8944" s="T465">ptcl</ta>
            <ta e="T467" id="Seg_8945" s="T466">adv</ta>
            <ta e="T468" id="Seg_8946" s="T467">ptcl</ta>
            <ta e="T469" id="Seg_8947" s="T468">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T470" id="Seg_8948" s="T469">n-n:case</ta>
            <ta e="T471" id="Seg_8949" s="T470">v-v:(neg)-v:pred.pn</ta>
            <ta e="T472" id="Seg_8950" s="T471">v-v:(neg)-v:pred.pn</ta>
            <ta e="T473" id="Seg_8951" s="T472">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T474" id="Seg_8952" s="T473">ptcl</ta>
            <ta e="T475" id="Seg_8953" s="T474">v-v:tense-v:pred.pn</ta>
            <ta e="T476" id="Seg_8954" s="T475">n-n&gt;v-v:ptcp</ta>
            <ta e="T477" id="Seg_8955" s="T476">n-n:case</ta>
            <ta e="T478" id="Seg_8956" s="T477">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T479" id="Seg_8957" s="T478">post</ta>
            <ta e="T480" id="Seg_8958" s="T479">v-v:tense-v:pred.pn</ta>
            <ta e="T481" id="Seg_8959" s="T480">interj</ta>
            <ta e="T482" id="Seg_8960" s="T481">ptcl</ta>
            <ta e="T483" id="Seg_8961" s="T482">que</ta>
            <ta e="T484" id="Seg_8962" s="T483">n-n:case</ta>
            <ta e="T485" id="Seg_8963" s="T484">v-v:tense-v:poss.pn</ta>
            <ta e="T486" id="Seg_8964" s="T485">ptcl</ta>
            <ta e="T487" id="Seg_8965" s="T486">n-n&gt;adj</ta>
            <ta e="T488" id="Seg_8966" s="T487">n-n:poss-n:case</ta>
            <ta e="T489" id="Seg_8967" s="T488">ptcl</ta>
            <ta e="T490" id="Seg_8968" s="T489">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T491" id="Seg_8969" s="T490">n-n:poss-n:case</ta>
            <ta e="T492" id="Seg_8970" s="T491">n-n&gt;adj-n:case</ta>
            <ta e="T493" id="Seg_8971" s="T492">que-pro:case</ta>
            <ta e="T494" id="Seg_8972" s="T493">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T495" id="Seg_8973" s="T494">adv</ta>
            <ta e="T496" id="Seg_8974" s="T495">interj</ta>
            <ta e="T497" id="Seg_8975" s="T496">dempro</ta>
            <ta e="T498" id="Seg_8976" s="T497">v-v:mood-v:temp.pn</ta>
            <ta e="T499" id="Seg_8977" s="T498">n-n:poss-n:case</ta>
            <ta e="T500" id="Seg_8978" s="T499">n-n:(poss)-n:case</ta>
            <ta e="T501" id="Seg_8979" s="T500">v-v:tense-v:poss.pn</ta>
            <ta e="T502" id="Seg_8980" s="T501">ptcl</ta>
            <ta e="T503" id="Seg_8981" s="T502">ptcl</ta>
            <ta e="T504" id="Seg_8982" s="T503">v-v:tense-v:poss.pn</ta>
            <ta e="T505" id="Seg_8983" s="T504">ptcl</ta>
            <ta e="T506" id="Seg_8984" s="T505">pers-pro:case</ta>
            <ta e="T507" id="Seg_8985" s="T506">adv</ta>
            <ta e="T508" id="Seg_8986" s="T507">adv</ta>
            <ta e="T509" id="Seg_8987" s="T508">v-v:tense-v:poss.pn</ta>
            <ta e="T510" id="Seg_8988" s="T509">interj</ta>
            <ta e="T511" id="Seg_8989" s="T510">v-v:mood.pn</ta>
            <ta e="T512" id="Seg_8990" s="T511">ptcl</ta>
            <ta e="T513" id="Seg_8991" s="T512">v-v:tense-v:pred.pn</ta>
            <ta e="T514" id="Seg_8992" s="T513">pers-pro:case</ta>
            <ta e="T515" id="Seg_8993" s="T514">n-n:case</ta>
            <ta e="T516" id="Seg_8994" s="T515">n-n:case</ta>
            <ta e="T517" id="Seg_8995" s="T516">que-que&gt;v-v:cvb</ta>
            <ta e="T518" id="Seg_8996" s="T517">adv</ta>
            <ta e="T519" id="Seg_8997" s="T518">v-v:tense-v:pred.pn</ta>
            <ta e="T520" id="Seg_8998" s="T519">adv</ta>
            <ta e="T521" id="Seg_8999" s="T520">v-v:cvb</ta>
            <ta e="T522" id="Seg_9000" s="T521">adv</ta>
            <ta e="T523" id="Seg_9001" s="T522">v-v:cvb</ta>
            <ta e="T524" id="Seg_9002" s="T523">v-v:tense-v:pred.pn</ta>
            <ta e="T525" id="Seg_9003" s="T524">v-v:tense-v:pred.pn</ta>
            <ta e="T526" id="Seg_9004" s="T525">conj</ta>
            <ta e="T527" id="Seg_9005" s="T526">v-v:tense-v:pred.pn</ta>
            <ta e="T528" id="Seg_9006" s="T527">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T529" id="Seg_9007" s="T528">post</ta>
            <ta e="T530" id="Seg_9008" s="T529">n-n:(poss)-n:case</ta>
            <ta e="T531" id="Seg_9009" s="T530">v-v:cvb</ta>
            <ta e="T532" id="Seg_9010" s="T531">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T533" id="Seg_9011" s="T532">v-v:tense-v:pred.pn</ta>
            <ta e="T534" id="Seg_9012" s="T533">ptcl</ta>
            <ta e="T535" id="Seg_9013" s="T534">pers-pro:case</ta>
            <ta e="T536" id="Seg_9014" s="T535">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T537" id="Seg_9015" s="T536">adv</ta>
            <ta e="T538" id="Seg_9016" s="T537">pers-pro:case</ta>
            <ta e="T539" id="Seg_9017" s="T538">ptcl</ta>
            <ta e="T540" id="Seg_9018" s="T539">pers-pro:case</ta>
            <ta e="T541" id="Seg_9019" s="T540">v-v:tense-v:poss.pn</ta>
            <ta e="T542" id="Seg_9020" s="T541">adv</ta>
            <ta e="T543" id="Seg_9021" s="T542">propr</ta>
            <ta e="T544" id="Seg_9022" s="T543">n-n:case</ta>
            <ta e="T545" id="Seg_9023" s="T544">propr</ta>
            <ta e="T546" id="Seg_9024" s="T545">n-n:case</ta>
            <ta e="T547" id="Seg_9025" s="T546">cardnum</ta>
            <ta e="T548" id="Seg_9026" s="T790">ptcl</ta>
            <ta e="T549" id="Seg_9027" s="T548">n-n&gt;n-n:case</ta>
            <ta e="T550" id="Seg_9028" s="T549">v-v:cvb</ta>
            <ta e="T551" id="Seg_9029" s="T550">v-v:mood.pn</ta>
            <ta e="T552" id="Seg_9030" s="T551">propr</ta>
            <ta e="T553" id="Seg_9031" s="T552">n-n:case</ta>
            <ta e="T554" id="Seg_9032" s="T553">v-v:tense-v:poss.pn</ta>
            <ta e="T555" id="Seg_9033" s="T554">v-v:tense-v:poss.pn</ta>
            <ta e="T556" id="Seg_9034" s="T555">v-v:tense-v:poss.pn</ta>
            <ta e="T560" id="Seg_9035" s="T558">v-v:tense-v:poss.pn</ta>
            <ta e="T561" id="Seg_9036" s="T560">n-n:poss-n:case</ta>
            <ta e="T562" id="Seg_9037" s="T561">v-v:tense-v:poss.pn</ta>
            <ta e="T563" id="Seg_9038" s="T562">adj</ta>
            <ta e="T564" id="Seg_9039" s="T563">n-n:(poss)-n:case</ta>
            <ta e="T565" id="Seg_9040" s="T564">v-v:mood-v:temp.pn</ta>
            <ta e="T566" id="Seg_9041" s="T565">propr</ta>
            <ta e="T567" id="Seg_9042" s="T566">n-n:case</ta>
            <ta e="T568" id="Seg_9043" s="T567">propr</ta>
            <ta e="T569" id="Seg_9044" s="T568">n-n:case</ta>
            <ta e="T570" id="Seg_9045" s="T569">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T571" id="Seg_9046" s="T570">v-v:tense-v:pred.pn</ta>
            <ta e="T572" id="Seg_9047" s="T571">que-pro:case</ta>
            <ta e="T573" id="Seg_9048" s="T572">ptcl</ta>
            <ta e="T574" id="Seg_9049" s="T573">v-v:neg-v:pred.pn</ta>
            <ta e="T575" id="Seg_9050" s="T574">adj</ta>
            <ta e="T576" id="Seg_9051" s="T575">que-pro:case</ta>
            <ta e="T577" id="Seg_9052" s="T576">ptcl</ta>
            <ta e="T578" id="Seg_9053" s="T577">dempro</ta>
            <ta e="T579" id="Seg_9054" s="T578">n-n:poss-n:case</ta>
            <ta e="T580" id="Seg_9055" s="T579">adj-n:poss-n:case</ta>
            <ta e="T581" id="Seg_9056" s="T580">n-n:(poss)-n:case</ta>
            <ta e="T582" id="Seg_9057" s="T581">n-n:poss-n:case</ta>
            <ta e="T583" id="Seg_9058" s="T582">que-que&gt;v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T584" id="Seg_9059" s="T583">n-n:poss-n:case</ta>
            <ta e="T585" id="Seg_9060" s="T584">post</ta>
            <ta e="T586" id="Seg_9061" s="T585">n-n:(poss)-n:case</ta>
            <ta e="T587" id="Seg_9062" s="T586">ptcl</ta>
            <ta e="T588" id="Seg_9063" s="T587">que-pro:case</ta>
            <ta e="T589" id="Seg_9064" s="T588">ptcl</ta>
            <ta e="T590" id="Seg_9065" s="T589">dempro-pro:case</ta>
            <ta e="T593" id="Seg_9066" s="T592">n-n&gt;adj</ta>
            <ta e="T594" id="Seg_9067" s="T593">v-v:cvb</ta>
            <ta e="T595" id="Seg_9068" s="T594">dempro-pro:case</ta>
            <ta e="T596" id="Seg_9069" s="T595">v-v:cvb</ta>
            <ta e="T597" id="Seg_9070" s="T596">v-v:mood.pn</ta>
            <ta e="T598" id="Seg_9071" s="T597">v-v:mood.pn</ta>
            <ta e="T599" id="Seg_9072" s="T598">v-v:cvb</ta>
            <ta e="T600" id="Seg_9073" s="T599">cardnum</ta>
            <ta e="T601" id="Seg_9074" s="T600">n-n:case</ta>
            <ta e="T602" id="Seg_9075" s="T601">v-v:cvb</ta>
            <ta e="T603" id="Seg_9076" s="T602">v-v:tense-v:pred.pn</ta>
            <ta e="T604" id="Seg_9077" s="T603">v-v:mood.pn</ta>
            <ta e="T605" id="Seg_9078" s="T604">n-n:poss-n:case</ta>
            <ta e="T606" id="Seg_9079" s="T605">v-v:tense-v:pred.pn</ta>
            <ta e="T607" id="Seg_9080" s="T606">adv</ta>
            <ta e="T608" id="Seg_9081" s="T607">v-v:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T611" id="Seg_9082" s="T610">adv</ta>
            <ta e="T612" id="Seg_9083" s="T611">pers-pro:case</ta>
            <ta e="T613" id="Seg_9084" s="T612">ptcl</ta>
            <ta e="T614" id="Seg_9085" s="T613">ptcl</ta>
            <ta e="T615" id="Seg_9086" s="T614">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T616" id="Seg_9087" s="T615">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T617" id="Seg_9088" s="T616">v-v:tense-v:poss.pn</ta>
            <ta e="T618" id="Seg_9089" s="T617">ptcl</ta>
            <ta e="T619" id="Seg_9090" s="T618">v-v:cvb</ta>
            <ta e="T620" id="Seg_9091" s="T619">v-v:tense-v:pred.pn</ta>
            <ta e="T621" id="Seg_9092" s="T620">ptcl</ta>
            <ta e="T622" id="Seg_9093" s="T621">v-v:cvb</ta>
            <ta e="T623" id="Seg_9094" s="T622">v-v:tense-v:pred.pn</ta>
            <ta e="T624" id="Seg_9095" s="T623">n-n:poss-n:case</ta>
            <ta e="T625" id="Seg_9096" s="T624">ptcl</ta>
            <ta e="T627" id="Seg_9097" s="T626">v-v:tense-v:pred.pn</ta>
            <ta e="T628" id="Seg_9098" s="T627">conj</ta>
            <ta e="T629" id="Seg_9099" s="T628">n-n:poss-n:case</ta>
            <ta e="T632" id="Seg_9100" s="T631">que</ta>
            <ta e="T633" id="Seg_9101" s="T632">ptcl</ta>
            <ta e="T634" id="Seg_9102" s="T633">adv</ta>
            <ta e="T635" id="Seg_9103" s="T634">n-n:case</ta>
            <ta e="T636" id="Seg_9104" s="T635">v-v:cvb</ta>
            <ta e="T637" id="Seg_9105" s="T636">post</ta>
            <ta e="T638" id="Seg_9106" s="T637">ptcl</ta>
            <ta e="T639" id="Seg_9107" s="T638">v-v:tense-v:pred.pn</ta>
            <ta e="T640" id="Seg_9108" s="T639">ptcl</ta>
            <ta e="T641" id="Seg_9109" s="T640">adv</ta>
            <ta e="T642" id="Seg_9110" s="T641">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T643" id="Seg_9111" s="T642">v-v:tense-v:pred.pn</ta>
            <ta e="T644" id="Seg_9112" s="T643">adv</ta>
            <ta e="T645" id="Seg_9113" s="T644">n-n:(poss)-n:case</ta>
            <ta e="T646" id="Seg_9114" s="T645">n-n:(poss)-n:case</ta>
            <ta e="T647" id="Seg_9115" s="T646">v-v:tense-v:pred.pn</ta>
            <ta e="T648" id="Seg_9116" s="T647">v-v:mood-v:temp.pn</ta>
            <ta e="T649" id="Seg_9117" s="T648">conj</ta>
            <ta e="T650" id="Seg_9118" s="T649">v-v:(neg)-v:pred.pn</ta>
            <ta e="T651" id="Seg_9119" s="T650">v-v:mood-v:temp.pn</ta>
            <ta e="T652" id="Seg_9120" s="T651">v-v:(neg)-v:pred.pn</ta>
            <ta e="T653" id="Seg_9121" s="T652">interj</ta>
            <ta e="T654" id="Seg_9122" s="T653">ptcl</ta>
            <ta e="T655" id="Seg_9123" s="T654">que</ta>
            <ta e="T656" id="Seg_9124" s="T655">ptcl</ta>
            <ta e="T657" id="Seg_9125" s="T656">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T658" id="Seg_9126" s="T657">adv</ta>
            <ta e="T659" id="Seg_9127" s="T658">ptcl</ta>
            <ta e="T660" id="Seg_9128" s="T659">n-n:(poss)-n:case</ta>
            <ta e="T661" id="Seg_9129" s="T660">n-n:case</ta>
            <ta e="T662" id="Seg_9130" s="T661">v-v:tense-v:pred.pn</ta>
            <ta e="T663" id="Seg_9131" s="T662">n-n:case</ta>
            <ta e="T664" id="Seg_9132" s="T663">n-n:poss-n:case</ta>
            <ta e="T665" id="Seg_9133" s="T664">v-v:tense-v:poss.pn</ta>
            <ta e="T666" id="Seg_9134" s="T665">ptcl</ta>
            <ta e="T667" id="Seg_9135" s="T666">v-v:cvb</ta>
            <ta e="T668" id="Seg_9136" s="T667">que</ta>
            <ta e="T669" id="Seg_9137" s="T668">ptcl</ta>
            <ta e="T670" id="Seg_9138" s="T669">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T671" id="Seg_9139" s="T670">adv</ta>
            <ta e="T672" id="Seg_9140" s="T671">v-v&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T673" id="Seg_9141" s="T672">conj</ta>
            <ta e="T674" id="Seg_9142" s="T791">v-v:cvb</ta>
            <ta e="T675" id="Seg_9143" s="T674">conj</ta>
            <ta e="T676" id="Seg_9144" s="T675">ptcl</ta>
            <ta e="T677" id="Seg_9145" s="T676">n-n:poss-n:case</ta>
            <ta e="T678" id="Seg_9146" s="T677">v-v:tense-v:poss.pn</ta>
            <ta e="T680" id="Seg_9147" s="T678">ptcl</ta>
            <ta e="T681" id="Seg_9148" s="T792">ptcl</ta>
            <ta e="T682" id="Seg_9149" s="T681">v-v:mood.pn</ta>
            <ta e="T683" id="Seg_9150" s="T682">v-v:tense-v:pred.pn</ta>
            <ta e="T684" id="Seg_9151" s="T683">v-v:mood.pn</ta>
            <ta e="T685" id="Seg_9152" s="T684">v-v:mood.pn</ta>
            <ta e="T686" id="Seg_9153" s="T685">adv</ta>
            <ta e="T688" id="Seg_9154" s="T686">adj-adj&gt;adv</ta>
            <ta e="T689" id="Seg_9155" s="T688">ptcl</ta>
            <ta e="T690" id="Seg_9156" s="T689">ptcl</ta>
            <ta e="T691" id="Seg_9157" s="T690">adv</ta>
            <ta e="T692" id="Seg_9158" s="T691">v-v:tense-v:pred.pn</ta>
            <ta e="T693" id="Seg_9159" s="T692">interj</ta>
            <ta e="T694" id="Seg_9160" s="T693">adv</ta>
            <ta e="T695" id="Seg_9161" s="T694">pers-pro:case</ta>
            <ta e="T696" id="Seg_9162" s="T695">adj-adj&gt;adv</ta>
            <ta e="T697" id="Seg_9163" s="T696">v-v:mood.pn</ta>
            <ta e="T698" id="Seg_9164" s="T697">adv</ta>
            <ta e="T699" id="Seg_9165" s="T698">adv</ta>
            <ta e="T700" id="Seg_9166" s="T699">adv</ta>
            <ta e="T701" id="Seg_9167" s="T700">v-v:tense-v:poss.pn</ta>
            <ta e="T702" id="Seg_9168" s="T701">ptcl</ta>
            <ta e="T703" id="Seg_9169" s="T702">que</ta>
            <ta e="T704" id="Seg_9170" s="T703">cardnum</ta>
            <ta e="T705" id="Seg_9171" s="T704">n-n:case</ta>
            <ta e="T706" id="Seg_9172" s="T705">v-v:tense-v:pred.pn</ta>
            <ta e="T707" id="Seg_9173" s="T706">pers-pro:case</ta>
            <ta e="T708" id="Seg_9174" s="T707">v-v:tense-v:pred.pn</ta>
            <ta e="T709" id="Seg_9175" s="T708">pers-pro:case</ta>
            <ta e="T710" id="Seg_9176" s="T709">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T711" id="Seg_9177" s="T710">adv</ta>
            <ta e="T712" id="Seg_9178" s="T711">pers-pro:case</ta>
            <ta e="T713" id="Seg_9179" s="T712">adv</ta>
            <ta e="T714" id="Seg_9180" s="T713">pers-pro:case</ta>
            <ta e="T715" id="Seg_9181" s="T714">v-v:cvb</ta>
            <ta e="T717" id="Seg_9182" s="T716">que-pro:case</ta>
            <ta e="T718" id="Seg_9183" s="T717">n-n:(poss)-n:case</ta>
            <ta e="T719" id="Seg_9184" s="T718">n-n:case</ta>
            <ta e="T720" id="Seg_9185" s="T719">v-v:tense-v:poss.pn</ta>
            <ta e="T721" id="Seg_9186" s="T720">ptcl</ta>
            <ta e="T722" id="Seg_9187" s="T721">n-n:case</ta>
            <ta e="T723" id="Seg_9188" s="T722">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T724" id="Seg_9189" s="T723">ptcl</ta>
            <ta e="T725" id="Seg_9190" s="T724">adv</ta>
            <ta e="T726" id="Seg_9191" s="T725">n-n&gt;v-v:cvb</ta>
            <ta e="T727" id="Seg_9192" s="T726">adv</ta>
            <ta e="T728" id="Seg_9193" s="T727">v-v:mood.pn</ta>
            <ta e="T729" id="Seg_9194" s="T728">pers-pro:case</ta>
            <ta e="T730" id="Seg_9195" s="T729">adv</ta>
            <ta e="T731" id="Seg_9196" s="T730">adv</ta>
            <ta e="T732" id="Seg_9197" s="T731">v-v:cvb-v:pred.pn</ta>
            <ta e="T733" id="Seg_9198" s="T732">cardnum</ta>
            <ta e="T734" id="Seg_9199" s="T733">n-n:case</ta>
            <ta e="T735" id="Seg_9200" s="T734">v-v:mood.pn</ta>
            <ta e="T736" id="Seg_9201" s="T735">conj</ta>
            <ta e="T737" id="Seg_9202" s="T736">cardnum</ta>
            <ta e="T738" id="Seg_9203" s="T737">n-n&gt;adj-n:case</ta>
            <ta e="T739" id="Seg_9204" s="T738">v-v:mood.pn</ta>
            <ta e="T740" id="Seg_9205" s="T739">pers-pro:case</ta>
            <ta e="T741" id="Seg_9206" s="T740">n-n:poss-n:case</ta>
            <ta e="T742" id="Seg_9207" s="T741">v-v:cvb</ta>
            <ta e="T743" id="Seg_9208" s="T742">v-v:mood.pn</ta>
            <ta e="T744" id="Seg_9209" s="T743">dempro</ta>
            <ta e="T745" id="Seg_9210" s="T744">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T746" id="Seg_9211" s="T745">ptcl</ta>
            <ta e="T747" id="Seg_9212" s="T746">dempro</ta>
            <ta e="T748" id="Seg_9213" s="T747">cardnum</ta>
            <ta e="T749" id="Seg_9214" s="T748">n-n:(poss)-n:case</ta>
            <ta e="T750" id="Seg_9215" s="T749">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T751" id="Seg_9216" s="T750">v-v:tense-v:pred.pn</ta>
            <ta e="T752" id="Seg_9217" s="T751">que-que&gt;adj</ta>
            <ta e="T753" id="Seg_9218" s="T752">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T754" id="Seg_9219" s="T753">ptcl</ta>
            <ta e="T755" id="Seg_9220" s="T754">dempro-pro:case</ta>
            <ta e="T756" id="Seg_9221" s="T755">v-v:mood.pn</ta>
            <ta e="T757" id="Seg_9222" s="T756">adv</ta>
            <ta e="T758" id="Seg_9223" s="T757">n-n:poss-n:case</ta>
            <ta e="T759" id="Seg_9224" s="T758">interj</ta>
            <ta e="T760" id="Seg_9225" s="T759">que-pro:(poss)-pro:case</ta>
            <ta e="T761" id="Seg_9226" s="T760">adv</ta>
            <ta e="T762" id="Seg_9227" s="T761">cardnum</ta>
            <ta e="T763" id="Seg_9228" s="T762">adv</ta>
            <ta e="T764" id="Seg_9229" s="T763">v-v:mood.pn</ta>
            <ta e="T765" id="Seg_9230" s="T764">pers-pro:case</ta>
            <ta e="T766" id="Seg_9231" s="T765">v-v:(ins)-v&gt;v-v:cvb-v:pred.pn</ta>
            <ta e="T767" id="Seg_9232" s="T766">ptcl</ta>
            <ta e="T768" id="Seg_9233" s="T767">adv</ta>
            <ta e="T769" id="Seg_9234" s="T768">v-v:cvb</ta>
            <ta e="T770" id="Seg_9235" s="T769">v-v:tense-v:pred.pn</ta>
            <ta e="T771" id="Seg_9236" s="T770">cardnum-cardnum&gt;collnum-n:(poss)-n:case</ta>
            <ta e="T772" id="Seg_9237" s="T771">cardnum</ta>
            <ta e="T773" id="Seg_9238" s="T772">n-n:case</ta>
            <ta e="T774" id="Seg_9239" s="T773">v-v:cvb-v:pred.pn</ta>
            <ta e="T775" id="Seg_9240" s="T774">n-n:case</ta>
            <ta e="T776" id="Seg_9241" s="T775">v-v:cvb-v:pred.pn</ta>
            <ta e="T777" id="Seg_9242" s="T776">cardnum-cardnum&gt;collnum-n:(poss)-n:case</ta>
            <ta e="T778" id="Seg_9243" s="T777">ptcl</ta>
            <ta e="T779" id="Seg_9244" s="T778">v-v:cvb-v-v:cvb</ta>
            <ta e="T780" id="Seg_9245" s="T779">v-v:tense-v:pred.pn</ta>
            <ta e="T781" id="Seg_9246" s="T780">adj-n:(poss)-n:case</ta>
            <ta e="T782" id="Seg_9247" s="T781">dempro-pro:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_9248" s="T0">propr</ta>
            <ta e="T2" id="Seg_9249" s="T1">post</ta>
            <ta e="T3" id="Seg_9250" s="T2">propr</ta>
            <ta e="T4" id="Seg_9251" s="T3">n</ta>
            <ta e="T5" id="Seg_9252" s="T4">v</ta>
            <ta e="T6" id="Seg_9253" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_9254" s="T6">adv</ta>
            <ta e="T8" id="Seg_9255" s="T7">v</ta>
            <ta e="T9" id="Seg_9256" s="T8">emphpro</ta>
            <ta e="T10" id="Seg_9257" s="T9">emphpro</ta>
            <ta e="T11" id="Seg_9258" s="T10">v</ta>
            <ta e="T12" id="Seg_9259" s="T11">n</ta>
            <ta e="T13" id="Seg_9260" s="T12">dempro</ta>
            <ta e="T14" id="Seg_9261" s="T13">post</ta>
            <ta e="T15" id="Seg_9262" s="T14">adj</ta>
            <ta e="T16" id="Seg_9263" s="T15">n</ta>
            <ta e="T17" id="Seg_9264" s="T16">dempro</ta>
            <ta e="T18" id="Seg_9265" s="T17">post</ta>
            <ta e="T19" id="Seg_9266" s="T18">adj</ta>
            <ta e="T20" id="Seg_9267" s="T19">v</ta>
            <ta e="T21" id="Seg_9268" s="T20">collnum</ta>
            <ta e="T22" id="Seg_9269" s="T21">v</ta>
            <ta e="T23" id="Seg_9270" s="T22">adv</ta>
            <ta e="T24" id="Seg_9271" s="T23">v</ta>
            <ta e="T25" id="Seg_9272" s="T24">ptcl</ta>
            <ta e="T26" id="Seg_9273" s="T25">v</ta>
            <ta e="T27" id="Seg_9274" s="T26">v</ta>
            <ta e="T28" id="Seg_9275" s="T27">propr</ta>
            <ta e="T29" id="Seg_9276" s="T28">post</ta>
            <ta e="T30" id="Seg_9277" s="T29">propr</ta>
            <ta e="T31" id="Seg_9278" s="T30">n</ta>
            <ta e="T32" id="Seg_9279" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_9280" s="T32">pers</ta>
            <ta e="T36" id="Seg_9281" s="T35">cardnum</ta>
            <ta e="T37" id="Seg_9282" s="T36">n</ta>
            <ta e="T38" id="Seg_9283" s="T37">v</ta>
            <ta e="T39" id="Seg_9284" s="T38">n</ta>
            <ta e="T40" id="Seg_9285" s="T39">adv</ta>
            <ta e="T41" id="Seg_9286" s="T40">v</ta>
            <ta e="T42" id="Seg_9287" s="T41">v</ta>
            <ta e="T43" id="Seg_9288" s="T42">v</ta>
            <ta e="T44" id="Seg_9289" s="T43">ptcl</ta>
            <ta e="T47" id="Seg_9290" s="T46">adv</ta>
            <ta e="T48" id="Seg_9291" s="T47">v</ta>
            <ta e="T49" id="Seg_9292" s="T48">ptcl</ta>
            <ta e="T50" id="Seg_9293" s="T49">n</ta>
            <ta e="T51" id="Seg_9294" s="T50">v</ta>
            <ta e="T52" id="Seg_9295" s="T51">ptcl</ta>
            <ta e="T53" id="Seg_9296" s="T52">dempro</ta>
            <ta e="T54" id="Seg_9297" s="T53">adj</ta>
            <ta e="T55" id="Seg_9298" s="T54">n</ta>
            <ta e="T56" id="Seg_9299" s="T55">ptcl</ta>
            <ta e="T57" id="Seg_9300" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_9301" s="T57">interj</ta>
            <ta e="T59" id="Seg_9302" s="T58">adv</ta>
            <ta e="T60" id="Seg_9303" s="T59">adv</ta>
            <ta e="T61" id="Seg_9304" s="T60">pers</ta>
            <ta e="T62" id="Seg_9305" s="T61">propr</ta>
            <ta e="T63" id="Seg_9306" s="T62">posspr</ta>
            <ta e="T64" id="Seg_9307" s="T783">n</ta>
            <ta e="T66" id="Seg_9308" s="T64">v</ta>
            <ta e="T67" id="Seg_9309" s="T66">ptcl</ta>
            <ta e="T68" id="Seg_9310" s="T67">propr</ta>
            <ta e="T69" id="Seg_9311" s="T68">n</ta>
            <ta e="T70" id="Seg_9312" s="T69">pers</ta>
            <ta e="T71" id="Seg_9313" s="T70">dempro</ta>
            <ta e="T72" id="Seg_9314" s="T71">posspr</ta>
            <ta e="T73" id="Seg_9315" s="T72">n</ta>
            <ta e="T74" id="Seg_9316" s="T73">v</ta>
            <ta e="T75" id="Seg_9317" s="T74">v</ta>
            <ta e="T76" id="Seg_9318" s="T75">ptcl</ta>
            <ta e="T77" id="Seg_9319" s="T76">interj</ta>
            <ta e="T78" id="Seg_9320" s="T77">adv</ta>
            <ta e="T79" id="Seg_9321" s="T78">v</ta>
            <ta e="T80" id="Seg_9322" s="T79">interj</ta>
            <ta e="T81" id="Seg_9323" s="T80">ptcl</ta>
            <ta e="T82" id="Seg_9324" s="T81">v</ta>
            <ta e="T83" id="Seg_9325" s="T82">ptcl</ta>
            <ta e="T84" id="Seg_9326" s="T83">propr</ta>
            <ta e="T85" id="Seg_9327" s="T84">n</ta>
            <ta e="T86" id="Seg_9328" s="T85">v</ta>
            <ta e="T87" id="Seg_9329" s="T86">cardnum</ta>
            <ta e="T88" id="Seg_9330" s="T87">n</ta>
            <ta e="T89" id="Seg_9331" s="T88">ptcl</ta>
            <ta e="T90" id="Seg_9332" s="T89">v</ta>
            <ta e="T91" id="Seg_9333" s="T90">post</ta>
            <ta e="T92" id="Seg_9334" s="T91">ptcl</ta>
            <ta e="T93" id="Seg_9335" s="T92">v</ta>
            <ta e="T94" id="Seg_9336" s="T93">dempro</ta>
            <ta e="T95" id="Seg_9337" s="T94">n</ta>
            <ta e="T96" id="Seg_9338" s="T95">v</ta>
            <ta e="T97" id="Seg_9339" s="T96">propr</ta>
            <ta e="T98" id="Seg_9340" s="T97">adj</ta>
            <ta e="T99" id="Seg_9341" s="T98">ptcl</ta>
            <ta e="T100" id="Seg_9342" s="T99">propr</ta>
            <ta e="T101" id="Seg_9343" s="T100">n</ta>
            <ta e="T102" id="Seg_9344" s="T101">v</ta>
            <ta e="T103" id="Seg_9345" s="T102">v</ta>
            <ta e="T104" id="Seg_9346" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_9347" s="T104">dempro</ta>
            <ta e="T106" id="Seg_9348" s="T105">n</ta>
            <ta e="T107" id="Seg_9349" s="T106">v</ta>
            <ta e="T108" id="Seg_9350" s="T107">interj</ta>
            <ta e="T109" id="Seg_9351" s="T108">propr</ta>
            <ta e="T110" id="Seg_9352" s="T109">n</ta>
            <ta e="T111" id="Seg_9353" s="T110">v</ta>
            <ta e="T113" id="Seg_9354" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_9355" s="T113">v</ta>
            <ta e="T115" id="Seg_9356" s="T114">v</ta>
            <ta e="T116" id="Seg_9357" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_9358" s="T116">interj</ta>
            <ta e="T118" id="Seg_9359" s="T117">cardnum</ta>
            <ta e="T119" id="Seg_9360" s="T118">n</ta>
            <ta e="T120" id="Seg_9361" s="T119">posspr</ta>
            <ta e="T121" id="Seg_9362" s="T120">n</ta>
            <ta e="T122" id="Seg_9363" s="T121">v</ta>
            <ta e="T123" id="Seg_9364" s="T122">aux</ta>
            <ta e="T124" id="Seg_9365" s="T784">v</ta>
            <ta e="T125" id="Seg_9366" s="T124">pers</ta>
            <ta e="T126" id="Seg_9367" s="T125">n</ta>
            <ta e="T128" id="Seg_9368" s="T126">v</ta>
            <ta e="T129" id="Seg_9369" s="T128">emphpro</ta>
            <ta e="T130" id="Seg_9370" s="T129">v</ta>
            <ta e="T131" id="Seg_9371" s="T130">pers</ta>
            <ta e="T132" id="Seg_9372" s="T131">n</ta>
            <ta e="T133" id="Seg_9373" s="T132">adj</ta>
            <ta e="T134" id="Seg_9374" s="T133">v</ta>
            <ta e="T135" id="Seg_9375" s="T134">v</ta>
            <ta e="T136" id="Seg_9376" s="T135">ptcl</ta>
            <ta e="T137" id="Seg_9377" s="T136">ptcl</ta>
            <ta e="T138" id="Seg_9378" s="T137">v</ta>
            <ta e="T139" id="Seg_9379" s="T138">aux</ta>
            <ta e="T140" id="Seg_9380" s="T139">n</ta>
            <ta e="T141" id="Seg_9381" s="T140">interj</ta>
            <ta e="T142" id="Seg_9382" s="T141">v</ta>
            <ta e="T143" id="Seg_9383" s="T142">que</ta>
            <ta e="T144" id="Seg_9384" s="T143">v</ta>
            <ta e="T145" id="Seg_9385" s="T144">n</ta>
            <ta e="T146" id="Seg_9386" s="T145">propr</ta>
            <ta e="T147" id="Seg_9387" s="T146">v</ta>
            <ta e="T148" id="Seg_9388" s="T147">n</ta>
            <ta e="T149" id="Seg_9389" s="T148">que</ta>
            <ta e="T150" id="Seg_9390" s="T149">ptcl</ta>
            <ta e="T151" id="Seg_9391" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_9392" s="T151">que</ta>
            <ta e="T153" id="Seg_9393" s="T152">ptcl</ta>
            <ta e="T154" id="Seg_9394" s="T153">que</ta>
            <ta e="T155" id="Seg_9395" s="T154">n</ta>
            <ta e="T156" id="Seg_9396" s="T155">v</ta>
            <ta e="T157" id="Seg_9397" s="T156">dempro</ta>
            <ta e="T158" id="Seg_9398" s="T157">v</ta>
            <ta e="T159" id="Seg_9399" s="T158">v</ta>
            <ta e="T160" id="Seg_9400" s="T159">v</ta>
            <ta e="T161" id="Seg_9401" s="T160">ptcl</ta>
            <ta e="T162" id="Seg_9402" s="T161">ptcl</ta>
            <ta e="T163" id="Seg_9403" s="T162">adv</ta>
            <ta e="T164" id="Seg_9404" s="T163">v</ta>
            <ta e="T165" id="Seg_9405" s="T164">que</ta>
            <ta e="T166" id="Seg_9406" s="T165">ptcl</ta>
            <ta e="T167" id="Seg_9407" s="T166">v</ta>
            <ta e="T168" id="Seg_9408" s="T167">ptcl</ta>
            <ta e="T169" id="Seg_9409" s="T168">propr</ta>
            <ta e="T170" id="Seg_9410" s="T169">adv</ta>
            <ta e="T171" id="Seg_9411" s="T170">n</ta>
            <ta e="T172" id="Seg_9412" s="T171">propr</ta>
            <ta e="T173" id="Seg_9413" s="T172">n</ta>
            <ta e="T174" id="Seg_9414" s="T173">conj</ta>
            <ta e="T175" id="Seg_9415" s="T174">n</ta>
            <ta e="T176" id="Seg_9416" s="T175">v</ta>
            <ta e="T177" id="Seg_9417" s="T176">aux</ta>
            <ta e="T178" id="Seg_9418" s="T177">adv</ta>
            <ta e="T179" id="Seg_9419" s="T178">ptcl</ta>
            <ta e="T180" id="Seg_9420" s="T179">v</ta>
            <ta e="T181" id="Seg_9421" s="T180">v</ta>
            <ta e="T182" id="Seg_9422" s="T181">pers</ta>
            <ta e="T183" id="Seg_9423" s="T182">ptcl</ta>
            <ta e="T184" id="Seg_9424" s="T183">v</ta>
            <ta e="T185" id="Seg_9425" s="T184">pers</ta>
            <ta e="T186" id="Seg_9426" s="T185">emphpro</ta>
            <ta e="T187" id="Seg_9427" s="T186">ptcl</ta>
            <ta e="T188" id="Seg_9428" s="T187">v</ta>
            <ta e="T189" id="Seg_9429" s="T188">v</ta>
            <ta e="T190" id="Seg_9430" s="T189">cardnum</ta>
            <ta e="T191" id="Seg_9431" s="T190">que</ta>
            <ta e="T192" id="Seg_9432" s="T191">ptcl</ta>
            <ta e="T193" id="Seg_9433" s="T192">v</ta>
            <ta e="T194" id="Seg_9434" s="T193">propr</ta>
            <ta e="T195" id="Seg_9435" s="T194">ptcl</ta>
            <ta e="T196" id="Seg_9436" s="T195">v</ta>
            <ta e="T197" id="Seg_9437" s="T196">dempro</ta>
            <ta e="T198" id="Seg_9438" s="T197">v</ta>
            <ta e="T199" id="Seg_9439" s="T198">propr</ta>
            <ta e="T200" id="Seg_9440" s="T199">n</ta>
            <ta e="T201" id="Seg_9441" s="T200">v</ta>
            <ta e="T202" id="Seg_9442" s="T201">v</ta>
            <ta e="T203" id="Seg_9443" s="T202">v</ta>
            <ta e="T204" id="Seg_9444" s="T203">n</ta>
            <ta e="T205" id="Seg_9445" s="T204">interj</ta>
            <ta e="T206" id="Seg_9446" s="T205">propr</ta>
            <ta e="T207" id="Seg_9447" s="T206">n</ta>
            <ta e="T208" id="Seg_9448" s="T207">v</ta>
            <ta e="T209" id="Seg_9449" s="T208">aux</ta>
            <ta e="T210" id="Seg_9450" s="T209">propr</ta>
            <ta e="T211" id="Seg_9451" s="T210">v</ta>
            <ta e="T212" id="Seg_9452" s="T785">n</ta>
            <ta e="T213" id="Seg_9453" s="T212">v</ta>
            <ta e="T214" id="Seg_9454" s="T213">v</ta>
            <ta e="T216" id="Seg_9455" s="T214">aux</ta>
            <ta e="T217" id="Seg_9456" s="T216">ptcl</ta>
            <ta e="T218" id="Seg_9457" s="T217">v</ta>
            <ta e="T219" id="Seg_9458" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_9459" s="T219">interj</ta>
            <ta e="T223" id="Seg_9460" s="T222">propr</ta>
            <ta e="T224" id="Seg_9461" s="T223">v</ta>
            <ta e="T225" id="Seg_9462" s="T224">propr</ta>
            <ta e="T226" id="Seg_9463" s="T225">v</ta>
            <ta e="T227" id="Seg_9464" s="T226">propr</ta>
            <ta e="T228" id="Seg_9465" s="T227">v</ta>
            <ta e="T229" id="Seg_9466" s="T228">pers</ta>
            <ta e="T230" id="Seg_9467" s="T229">v</ta>
            <ta e="T231" id="Seg_9468" s="T230">ptcl</ta>
            <ta e="T232" id="Seg_9469" s="T786">n</ta>
            <ta e="T233" id="Seg_9470" s="T232">v</ta>
            <ta e="T234" id="Seg_9471" s="T233">v</ta>
            <ta e="T235" id="Seg_9472" s="T234">propr</ta>
            <ta e="T237" id="Seg_9473" s="T235">v</ta>
            <ta e="T238" id="Seg_9474" s="T237">v</ta>
            <ta e="T239" id="Seg_9475" s="T238">n</ta>
            <ta e="T240" id="Seg_9476" s="T239">v</ta>
            <ta e="T241" id="Seg_9477" s="T240">n</ta>
            <ta e="T242" id="Seg_9478" s="T241">v</ta>
            <ta e="T243" id="Seg_9479" s="T242">propr</ta>
            <ta e="T244" id="Seg_9480" s="T243">v</ta>
            <ta e="T245" id="Seg_9481" s="T244">propr</ta>
            <ta e="T246" id="Seg_9482" s="T245">v</ta>
            <ta e="T247" id="Seg_9483" s="T246">v</ta>
            <ta e="T248" id="Seg_9484" s="T247">aux</ta>
            <ta e="T249" id="Seg_9485" s="T248">cardnum</ta>
            <ta e="T250" id="Seg_9486" s="T249">v</ta>
            <ta e="T251" id="Seg_9487" s="T250">aux</ta>
            <ta e="T252" id="Seg_9488" s="T251">adv</ta>
            <ta e="T253" id="Seg_9489" s="T252">ptcl</ta>
            <ta e="T254" id="Seg_9490" s="T253">v</ta>
            <ta e="T255" id="Seg_9491" s="T254">cardnum</ta>
            <ta e="T256" id="Seg_9492" s="T255">ptcl</ta>
            <ta e="T257" id="Seg_9493" s="T256">cardnum</ta>
            <ta e="T258" id="Seg_9494" s="T257">que</ta>
            <ta e="T259" id="Seg_9495" s="T258">n</ta>
            <ta e="T260" id="Seg_9496" s="T259">v</ta>
            <ta e="T262" id="Seg_9497" s="T261">v</ta>
            <ta e="T263" id="Seg_9498" s="T262">n</ta>
            <ta e="T264" id="Seg_9499" s="T263">v</ta>
            <ta e="T265" id="Seg_9500" s="T264">aux</ta>
            <ta e="T266" id="Seg_9501" s="T265">v</ta>
            <ta e="T267" id="Seg_9502" s="T266">v</ta>
            <ta e="T268" id="Seg_9503" s="T267">dempro</ta>
            <ta e="T269" id="Seg_9504" s="T268">propr</ta>
            <ta e="T270" id="Seg_9505" s="T269">v</ta>
            <ta e="T271" id="Seg_9506" s="T270">aux</ta>
            <ta e="T272" id="Seg_9507" s="T271">v</ta>
            <ta e="T273" id="Seg_9508" s="T272">n</ta>
            <ta e="T274" id="Seg_9509" s="T273">v</ta>
            <ta e="T275" id="Seg_9510" s="T274">n</ta>
            <ta e="T276" id="Seg_9511" s="T275">v</ta>
            <ta e="T277" id="Seg_9512" s="T276">adv</ta>
            <ta e="T278" id="Seg_9513" s="T277">v</ta>
            <ta e="T279" id="Seg_9514" s="T278">pers</ta>
            <ta e="T280" id="Seg_9515" s="T279">ptcl</ta>
            <ta e="T281" id="Seg_9516" s="T280">v</ta>
            <ta e="T282" id="Seg_9517" s="T281">aux</ta>
            <ta e="T283" id="Seg_9518" s="T282">n</ta>
            <ta e="T284" id="Seg_9519" s="T283">v</ta>
            <ta e="T285" id="Seg_9520" s="T284">quant</ta>
            <ta e="T286" id="Seg_9521" s="T285">v</ta>
            <ta e="T287" id="Seg_9522" s="T286">pers</ta>
            <ta e="T288" id="Seg_9523" s="T287">adv</ta>
            <ta e="T289" id="Seg_9524" s="T288">dempro</ta>
            <ta e="T290" id="Seg_9525" s="T289">ptcl</ta>
            <ta e="T291" id="Seg_9526" s="T290">n</ta>
            <ta e="T292" id="Seg_9527" s="T291">adj</ta>
            <ta e="T293" id="Seg_9528" s="T292">adj</ta>
            <ta e="T294" id="Seg_9529" s="T293">n</ta>
            <ta e="T295" id="Seg_9530" s="T294">ptcl</ta>
            <ta e="T298" id="Seg_9531" s="T297">v</ta>
            <ta e="T299" id="Seg_9532" s="T298">v</ta>
            <ta e="T300" id="Seg_9533" s="T299">ptcl</ta>
            <ta e="T301" id="Seg_9534" s="T300">que</ta>
            <ta e="T302" id="Seg_9535" s="T301">ptcl</ta>
            <ta e="T303" id="Seg_9536" s="T302">v</ta>
            <ta e="T304" id="Seg_9537" s="T303">conj</ta>
            <ta e="T305" id="Seg_9538" s="T304">v</ta>
            <ta e="T306" id="Seg_9539" s="T305">ptcl</ta>
            <ta e="T307" id="Seg_9540" s="T306">que</ta>
            <ta e="T308" id="Seg_9541" s="T307">ptcl</ta>
            <ta e="T309" id="Seg_9542" s="T308">n</ta>
            <ta e="T310" id="Seg_9543" s="T309">v</ta>
            <ta e="T311" id="Seg_9544" s="T310">ptcl</ta>
            <ta e="T312" id="Seg_9545" s="T311">ptcl</ta>
            <ta e="T313" id="Seg_9546" s="T312">ptcl</ta>
            <ta e="T314" id="Seg_9547" s="T313">v</ta>
            <ta e="T315" id="Seg_9548" s="T314">v</ta>
            <ta e="T316" id="Seg_9549" s="T315">aux</ta>
            <ta e="T317" id="Seg_9550" s="T316">n</ta>
            <ta e="T318" id="Seg_9551" s="T317">v</ta>
            <ta e="T319" id="Seg_9552" s="T318">propr</ta>
            <ta e="T320" id="Seg_9553" s="T319">n</ta>
            <ta e="T321" id="Seg_9554" s="T320">adv</ta>
            <ta e="T322" id="Seg_9555" s="T321">propr</ta>
            <ta e="T323" id="Seg_9556" s="T322">v</ta>
            <ta e="T324" id="Seg_9557" s="T323">v</ta>
            <ta e="T325" id="Seg_9558" s="T324">propr</ta>
            <ta e="T326" id="Seg_9559" s="T325">ptcl</ta>
            <ta e="T327" id="Seg_9560" s="T326">v</ta>
            <ta e="T328" id="Seg_9561" s="T327">propr</ta>
            <ta e="T329" id="Seg_9562" s="T328">v</ta>
            <ta e="T330" id="Seg_9563" s="T329">propr</ta>
            <ta e="T331" id="Seg_9564" s="T330">v</ta>
            <ta e="T332" id="Seg_9565" s="T787">v</ta>
            <ta e="T333" id="Seg_9566" s="T332">v</ta>
            <ta e="T334" id="Seg_9567" s="T333">v</ta>
            <ta e="T336" id="Seg_9568" s="T334">v</ta>
            <ta e="T337" id="Seg_9569" s="T336">propr</ta>
            <ta e="T338" id="Seg_9570" s="T337">v</ta>
            <ta e="T339" id="Seg_9571" s="T338">cardnum</ta>
            <ta e="T340" id="Seg_9572" s="T339">ptcl</ta>
            <ta e="T342" id="Seg_9573" s="T341">ptcl</ta>
            <ta e="T343" id="Seg_9574" s="T342">n</ta>
            <ta e="T344" id="Seg_9575" s="T343">v</ta>
            <ta e="T345" id="Seg_9576" s="T344">v</ta>
            <ta e="T346" id="Seg_9577" s="T345">propr</ta>
            <ta e="T347" id="Seg_9578" s="T346">v</ta>
            <ta e="T348" id="Seg_9579" s="T347">propr</ta>
            <ta e="T349" id="Seg_9580" s="T348">v</ta>
            <ta e="T350" id="Seg_9581" s="T349">v</ta>
            <ta e="T351" id="Seg_9582" s="T350">v</ta>
            <ta e="T352" id="Seg_9583" s="T788">cop</ta>
            <ta e="T353" id="Seg_9584" s="T352">v</ta>
            <ta e="T354" id="Seg_9585" s="T353">propr</ta>
            <ta e="T355" id="Seg_9586" s="T354">v</ta>
            <ta e="T356" id="Seg_9587" s="T355">v</ta>
            <ta e="T357" id="Seg_9588" s="T356">n</ta>
            <ta e="T358" id="Seg_9589" s="T357">v</ta>
            <ta e="T360" id="Seg_9590" s="T358">v</ta>
            <ta e="T361" id="Seg_9591" s="T360">ptcl</ta>
            <ta e="T362" id="Seg_9592" s="T361">cardnum</ta>
            <ta e="T363" id="Seg_9593" s="T362">ptcl</ta>
            <ta e="T364" id="Seg_9594" s="T363">n</ta>
            <ta e="T365" id="Seg_9595" s="T364">ptcl</ta>
            <ta e="T366" id="Seg_9596" s="T365">v</ta>
            <ta e="T367" id="Seg_9597" s="T366">v</ta>
            <ta e="T368" id="Seg_9598" s="T367">v</ta>
            <ta e="T369" id="Seg_9599" s="T368">adv</ta>
            <ta e="T370" id="Seg_9600" s="T369">v</ta>
            <ta e="T371" id="Seg_9601" s="T370">v</ta>
            <ta e="T372" id="Seg_9602" s="T371">pro</ta>
            <ta e="T373" id="Seg_9603" s="T372">ptcl</ta>
            <ta e="T374" id="Seg_9604" s="T373">v</ta>
            <ta e="T375" id="Seg_9605" s="T374">que</ta>
            <ta e="T376" id="Seg_9606" s="T375">v</ta>
            <ta e="T377" id="Seg_9607" s="T376">adv</ta>
            <ta e="T378" id="Seg_9608" s="T377">adv</ta>
            <ta e="T379" id="Seg_9609" s="T378">v</ta>
            <ta e="T380" id="Seg_9610" s="T379">aux</ta>
            <ta e="T381" id="Seg_9611" s="T380">que</ta>
            <ta e="T382" id="Seg_9612" s="T381">n</ta>
            <ta e="T383" id="Seg_9613" s="T382">ptcl</ta>
            <ta e="T384" id="Seg_9614" s="T383">v</ta>
            <ta e="T385" id="Seg_9615" s="T384">n</ta>
            <ta e="T386" id="Seg_9616" s="T385">ptcl</ta>
            <ta e="T387" id="Seg_9617" s="T386">v</ta>
            <ta e="T388" id="Seg_9618" s="T387">dempro</ta>
            <ta e="T389" id="Seg_9619" s="T388">n</ta>
            <ta e="T390" id="Seg_9620" s="T389">v</ta>
            <ta e="T391" id="Seg_9621" s="T390">adv</ta>
            <ta e="T392" id="Seg_9622" s="T391">adv</ta>
            <ta e="T393" id="Seg_9623" s="T392">v</ta>
            <ta e="T394" id="Seg_9624" s="T393">aux</ta>
            <ta e="T397" id="Seg_9625" s="T396">n</ta>
            <ta e="T398" id="Seg_9626" s="T397">n</ta>
            <ta e="T399" id="Seg_9627" s="T398">v</ta>
            <ta e="T400" id="Seg_9628" s="T399">v</ta>
            <ta e="T401" id="Seg_9629" s="T400">adj</ta>
            <ta e="T402" id="Seg_9630" s="T401">n</ta>
            <ta e="T403" id="Seg_9631" s="T402">v</ta>
            <ta e="T404" id="Seg_9632" s="T403">n</ta>
            <ta e="T405" id="Seg_9633" s="T404">que</ta>
            <ta e="T406" id="Seg_9634" s="T405">n</ta>
            <ta e="T407" id="Seg_9635" s="T406">v</ta>
            <ta e="T408" id="Seg_9636" s="T407">ptcl</ta>
            <ta e="T409" id="Seg_9637" s="T408">v</ta>
            <ta e="T410" id="Seg_9638" s="T409">n</ta>
            <ta e="T411" id="Seg_9639" s="T410">pers</ta>
            <ta e="T412" id="Seg_9640" s="T789">n</ta>
            <ta e="T413" id="Seg_9641" s="T412">v</ta>
            <ta e="T414" id="Seg_9642" s="T413">ptcl</ta>
            <ta e="T416" id="Seg_9643" s="T414">v</ta>
            <ta e="T417" id="Seg_9644" s="T416">adv</ta>
            <ta e="T418" id="Seg_9645" s="T417">pers</ta>
            <ta e="T419" id="Seg_9646" s="T418">v</ta>
            <ta e="T420" id="Seg_9647" s="T419">n</ta>
            <ta e="T423" id="Seg_9648" s="T422">v</ta>
            <ta e="T424" id="Seg_9649" s="T423">n</ta>
            <ta e="T425" id="Seg_9650" s="T424">adj</ta>
            <ta e="T426" id="Seg_9651" s="T425">n</ta>
            <ta e="T427" id="Seg_9652" s="T426">emphpro</ta>
            <ta e="T428" id="Seg_9653" s="T427">v</ta>
            <ta e="T429" id="Seg_9654" s="T428">v</ta>
            <ta e="T430" id="Seg_9655" s="T429">aux</ta>
            <ta e="T431" id="Seg_9656" s="T430">pers</ta>
            <ta e="T432" id="Seg_9657" s="T431">v</ta>
            <ta e="T433" id="Seg_9658" s="T432">n</ta>
            <ta e="T434" id="Seg_9659" s="T433">v</ta>
            <ta e="T435" id="Seg_9660" s="T434">conj</ta>
            <ta e="T436" id="Seg_9661" s="T435">n</ta>
            <ta e="T437" id="Seg_9662" s="T436">v</ta>
            <ta e="T438" id="Seg_9663" s="T437">aux</ta>
            <ta e="T439" id="Seg_9664" s="T438">n</ta>
            <ta e="T440" id="Seg_9665" s="T439">v</ta>
            <ta e="T441" id="Seg_9666" s="T440">conj</ta>
            <ta e="T442" id="Seg_9667" s="T441">interj</ta>
            <ta e="T443" id="Seg_9668" s="T442">v</ta>
            <ta e="T444" id="Seg_9669" s="T443">v</ta>
            <ta e="T445" id="Seg_9670" s="T444">aux</ta>
            <ta e="T446" id="Seg_9671" s="T445">n</ta>
            <ta e="T447" id="Seg_9672" s="T446">v</ta>
            <ta e="T448" id="Seg_9673" s="T447">ptcl</ta>
            <ta e="T449" id="Seg_9674" s="T448">dempro</ta>
            <ta e="T450" id="Seg_9675" s="T449">que</ta>
            <ta e="T451" id="Seg_9676" s="T450">que</ta>
            <ta e="T454" id="Seg_9677" s="T453">v</ta>
            <ta e="T455" id="Seg_9678" s="T454">n</ta>
            <ta e="T456" id="Seg_9679" s="T455">v</ta>
            <ta e="T457" id="Seg_9680" s="T456">v</ta>
            <ta e="T458" id="Seg_9681" s="T457">v</ta>
            <ta e="T459" id="Seg_9682" s="T458">v</ta>
            <ta e="T460" id="Seg_9683" s="T459">aux</ta>
            <ta e="T461" id="Seg_9684" s="T460">ptcl</ta>
            <ta e="T462" id="Seg_9685" s="T461">n</ta>
            <ta e="T463" id="Seg_9686" s="T462">v</ta>
            <ta e="T464" id="Seg_9687" s="T463">adv</ta>
            <ta e="T465" id="Seg_9688" s="T464">interj</ta>
            <ta e="T466" id="Seg_9689" s="T465">ptcl</ta>
            <ta e="T467" id="Seg_9690" s="T466">adv</ta>
            <ta e="T468" id="Seg_9691" s="T467">ptcl</ta>
            <ta e="T469" id="Seg_9692" s="T468">v</ta>
            <ta e="T470" id="Seg_9693" s="T469">propr</ta>
            <ta e="T471" id="Seg_9694" s="T470">v</ta>
            <ta e="T472" id="Seg_9695" s="T471">v</ta>
            <ta e="T473" id="Seg_9696" s="T472">v</ta>
            <ta e="T474" id="Seg_9697" s="T473">ptcl</ta>
            <ta e="T475" id="Seg_9698" s="T474">v</ta>
            <ta e="T476" id="Seg_9699" s="T475">v</ta>
            <ta e="T477" id="Seg_9700" s="T476">n</ta>
            <ta e="T478" id="Seg_9701" s="T477">v</ta>
            <ta e="T479" id="Seg_9702" s="T478">post</ta>
            <ta e="T480" id="Seg_9703" s="T479">v</ta>
            <ta e="T481" id="Seg_9704" s="T480">interj</ta>
            <ta e="T482" id="Seg_9705" s="T481">ptcl</ta>
            <ta e="T483" id="Seg_9706" s="T482">que</ta>
            <ta e="T484" id="Seg_9707" s="T483">n</ta>
            <ta e="T485" id="Seg_9708" s="T484">v</ta>
            <ta e="T486" id="Seg_9709" s="T485">ptcl</ta>
            <ta e="T487" id="Seg_9710" s="T486">adj</ta>
            <ta e="T488" id="Seg_9711" s="T487">n</ta>
            <ta e="T489" id="Seg_9712" s="T488">ptcl</ta>
            <ta e="T490" id="Seg_9713" s="T489">v</ta>
            <ta e="T491" id="Seg_9714" s="T490">n</ta>
            <ta e="T492" id="Seg_9715" s="T491">adj</ta>
            <ta e="T493" id="Seg_9716" s="T492">que</ta>
            <ta e="T494" id="Seg_9717" s="T493">cop</ta>
            <ta e="T495" id="Seg_9718" s="T494">adv</ta>
            <ta e="T496" id="Seg_9719" s="T495">interj</ta>
            <ta e="T497" id="Seg_9720" s="T496">dempro</ta>
            <ta e="T498" id="Seg_9721" s="T497">v</ta>
            <ta e="T499" id="Seg_9722" s="T498">propr</ta>
            <ta e="T500" id="Seg_9723" s="T499">n</ta>
            <ta e="T501" id="Seg_9724" s="T500">v</ta>
            <ta e="T502" id="Seg_9725" s="T501">ptcl</ta>
            <ta e="T503" id="Seg_9726" s="T502">ptcl</ta>
            <ta e="T504" id="Seg_9727" s="T503">v</ta>
            <ta e="T505" id="Seg_9728" s="T504">ptcl</ta>
            <ta e="T506" id="Seg_9729" s="T505">pers</ta>
            <ta e="T507" id="Seg_9730" s="T506">adv</ta>
            <ta e="T508" id="Seg_9731" s="T507">adv</ta>
            <ta e="T509" id="Seg_9732" s="T508">v</ta>
            <ta e="T510" id="Seg_9733" s="T509">interj</ta>
            <ta e="T511" id="Seg_9734" s="T510">v</ta>
            <ta e="T512" id="Seg_9735" s="T511">ptcl</ta>
            <ta e="T513" id="Seg_9736" s="T512">v</ta>
            <ta e="T514" id="Seg_9737" s="T513">pers</ta>
            <ta e="T515" id="Seg_9738" s="T514">n</ta>
            <ta e="T516" id="Seg_9739" s="T515">propr</ta>
            <ta e="T517" id="Seg_9740" s="T516">v</ta>
            <ta e="T518" id="Seg_9741" s="T517">adv</ta>
            <ta e="T519" id="Seg_9742" s="T518">v</ta>
            <ta e="T520" id="Seg_9743" s="T519">adv</ta>
            <ta e="T521" id="Seg_9744" s="T520">v</ta>
            <ta e="T522" id="Seg_9745" s="T521">adv</ta>
            <ta e="T523" id="Seg_9746" s="T522">v</ta>
            <ta e="T524" id="Seg_9747" s="T523">aux</ta>
            <ta e="T525" id="Seg_9748" s="T524">v</ta>
            <ta e="T526" id="Seg_9749" s="T525">conj</ta>
            <ta e="T527" id="Seg_9750" s="T526">v</ta>
            <ta e="T528" id="Seg_9751" s="T527">v</ta>
            <ta e="T529" id="Seg_9752" s="T528">post</ta>
            <ta e="T530" id="Seg_9753" s="T529">n</ta>
            <ta e="T531" id="Seg_9754" s="T530">v</ta>
            <ta e="T532" id="Seg_9755" s="T531">v</ta>
            <ta e="T533" id="Seg_9756" s="T532">aux</ta>
            <ta e="T534" id="Seg_9757" s="T533">ptcl</ta>
            <ta e="T535" id="Seg_9758" s="T534">pers</ta>
            <ta e="T536" id="Seg_9759" s="T535">v</ta>
            <ta e="T537" id="Seg_9760" s="T536">adv</ta>
            <ta e="T538" id="Seg_9761" s="T537">pers</ta>
            <ta e="T539" id="Seg_9762" s="T538">ptcl</ta>
            <ta e="T540" id="Seg_9763" s="T539">pers</ta>
            <ta e="T541" id="Seg_9764" s="T540">v</ta>
            <ta e="T542" id="Seg_9765" s="T541">adv</ta>
            <ta e="T543" id="Seg_9766" s="T542">propr</ta>
            <ta e="T544" id="Seg_9767" s="T543">n</ta>
            <ta e="T545" id="Seg_9768" s="T544">propr</ta>
            <ta e="T546" id="Seg_9769" s="T545">n</ta>
            <ta e="T547" id="Seg_9770" s="T546">cardnum</ta>
            <ta e="T548" id="Seg_9771" s="T790">ptcl</ta>
            <ta e="T549" id="Seg_9772" s="T548">n</ta>
            <ta e="T550" id="Seg_9773" s="T549">v</ta>
            <ta e="T551" id="Seg_9774" s="T550">v</ta>
            <ta e="T552" id="Seg_9775" s="T551">propr</ta>
            <ta e="T553" id="Seg_9776" s="T552">n</ta>
            <ta e="T554" id="Seg_9777" s="T553">v</ta>
            <ta e="T555" id="Seg_9778" s="T554">v</ta>
            <ta e="T556" id="Seg_9779" s="T555">v</ta>
            <ta e="T560" id="Seg_9780" s="T558">v</ta>
            <ta e="T561" id="Seg_9781" s="T560">n</ta>
            <ta e="T562" id="Seg_9782" s="T561">v</ta>
            <ta e="T563" id="Seg_9783" s="T562">adj</ta>
            <ta e="T564" id="Seg_9784" s="T563">n</ta>
            <ta e="T565" id="Seg_9785" s="T564">cop</ta>
            <ta e="T566" id="Seg_9786" s="T565">propr</ta>
            <ta e="T567" id="Seg_9787" s="T566">n</ta>
            <ta e="T568" id="Seg_9788" s="T567">propr</ta>
            <ta e="T569" id="Seg_9789" s="T568">n</ta>
            <ta e="T570" id="Seg_9790" s="T569">v</ta>
            <ta e="T571" id="Seg_9791" s="T570">aux</ta>
            <ta e="T572" id="Seg_9792" s="T571">que</ta>
            <ta e="T573" id="Seg_9793" s="T572">ptcl</ta>
            <ta e="T574" id="Seg_9794" s="T573">v</ta>
            <ta e="T575" id="Seg_9795" s="T574">adv</ta>
            <ta e="T576" id="Seg_9796" s="T575">que</ta>
            <ta e="T577" id="Seg_9797" s="T576">ptcl</ta>
            <ta e="T578" id="Seg_9798" s="T577">dempro</ta>
            <ta e="T579" id="Seg_9799" s="T578">n</ta>
            <ta e="T580" id="Seg_9800" s="T579">adj</ta>
            <ta e="T581" id="Seg_9801" s="T580">n</ta>
            <ta e="T582" id="Seg_9802" s="T581">n</ta>
            <ta e="T583" id="Seg_9803" s="T582">v</ta>
            <ta e="T584" id="Seg_9804" s="T583">n</ta>
            <ta e="T585" id="Seg_9805" s="T584">post</ta>
            <ta e="T586" id="Seg_9806" s="T585">n</ta>
            <ta e="T587" id="Seg_9807" s="T586">ptcl</ta>
            <ta e="T588" id="Seg_9808" s="T587">que</ta>
            <ta e="T589" id="Seg_9809" s="T588">ptcl</ta>
            <ta e="T590" id="Seg_9810" s="T589">dempro</ta>
            <ta e="T593" id="Seg_9811" s="T592">adj</ta>
            <ta e="T594" id="Seg_9812" s="T593">v</ta>
            <ta e="T595" id="Seg_9813" s="T594">dempro</ta>
            <ta e="T596" id="Seg_9814" s="T595">v</ta>
            <ta e="T597" id="Seg_9815" s="T596">v</ta>
            <ta e="T598" id="Seg_9816" s="T597">v</ta>
            <ta e="T599" id="Seg_9817" s="T598">v</ta>
            <ta e="T600" id="Seg_9818" s="T599">cardnum</ta>
            <ta e="T601" id="Seg_9819" s="T600">n</ta>
            <ta e="T602" id="Seg_9820" s="T601">v</ta>
            <ta e="T603" id="Seg_9821" s="T602">aux</ta>
            <ta e="T604" id="Seg_9822" s="T603">v</ta>
            <ta e="T605" id="Seg_9823" s="T604">n</ta>
            <ta e="T606" id="Seg_9824" s="T605">v</ta>
            <ta e="T607" id="Seg_9825" s="T606">adv</ta>
            <ta e="T608" id="Seg_9826" s="T607">v</ta>
            <ta e="T611" id="Seg_9827" s="T610">adv</ta>
            <ta e="T612" id="Seg_9828" s="T611">pers</ta>
            <ta e="T613" id="Seg_9829" s="T612">ptcl</ta>
            <ta e="T614" id="Seg_9830" s="T613">ptcl</ta>
            <ta e="T615" id="Seg_9831" s="T614">v</ta>
            <ta e="T616" id="Seg_9832" s="T615">dempro</ta>
            <ta e="T617" id="Seg_9833" s="T616">v</ta>
            <ta e="T618" id="Seg_9834" s="T617">ptcl</ta>
            <ta e="T619" id="Seg_9835" s="T618">v</ta>
            <ta e="T620" id="Seg_9836" s="T619">aux</ta>
            <ta e="T621" id="Seg_9837" s="T620">ptcl</ta>
            <ta e="T622" id="Seg_9838" s="T621">v</ta>
            <ta e="T623" id="Seg_9839" s="T622">aux</ta>
            <ta e="T624" id="Seg_9840" s="T623">n</ta>
            <ta e="T625" id="Seg_9841" s="T624">ptcl</ta>
            <ta e="T627" id="Seg_9842" s="T626">v</ta>
            <ta e="T628" id="Seg_9843" s="T627">conj</ta>
            <ta e="T629" id="Seg_9844" s="T628">n</ta>
            <ta e="T632" id="Seg_9845" s="T631">que</ta>
            <ta e="T633" id="Seg_9846" s="T632">ptcl</ta>
            <ta e="T634" id="Seg_9847" s="T633">adv</ta>
            <ta e="T635" id="Seg_9848" s="T634">n</ta>
            <ta e="T636" id="Seg_9849" s="T635">cop</ta>
            <ta e="T637" id="Seg_9850" s="T636">post</ta>
            <ta e="T638" id="Seg_9851" s="T637">ptcl</ta>
            <ta e="T639" id="Seg_9852" s="T638">v</ta>
            <ta e="T640" id="Seg_9853" s="T639">ptcl</ta>
            <ta e="T641" id="Seg_9854" s="T640">adv</ta>
            <ta e="T642" id="Seg_9855" s="T641">v</ta>
            <ta e="T643" id="Seg_9856" s="T642">v</ta>
            <ta e="T644" id="Seg_9857" s="T643">adv</ta>
            <ta e="T645" id="Seg_9858" s="T644">n</ta>
            <ta e="T646" id="Seg_9859" s="T645">propr</ta>
            <ta e="T647" id="Seg_9860" s="T646">v</ta>
            <ta e="T648" id="Seg_9861" s="T647">v</ta>
            <ta e="T649" id="Seg_9862" s="T648">conj</ta>
            <ta e="T650" id="Seg_9863" s="T649">v</ta>
            <ta e="T651" id="Seg_9864" s="T650">v</ta>
            <ta e="T652" id="Seg_9865" s="T651">v</ta>
            <ta e="T653" id="Seg_9866" s="T652">interj</ta>
            <ta e="T654" id="Seg_9867" s="T653">ptcl</ta>
            <ta e="T655" id="Seg_9868" s="T654">que</ta>
            <ta e="T656" id="Seg_9869" s="T655">ptcl</ta>
            <ta e="T657" id="Seg_9870" s="T656">v</ta>
            <ta e="T658" id="Seg_9871" s="T657">adv</ta>
            <ta e="T659" id="Seg_9872" s="T658">ptcl</ta>
            <ta e="T660" id="Seg_9873" s="T659">n</ta>
            <ta e="T661" id="Seg_9874" s="T660">n</ta>
            <ta e="T662" id="Seg_9875" s="T661">v</ta>
            <ta e="T663" id="Seg_9876" s="T662">n</ta>
            <ta e="T664" id="Seg_9877" s="T663">n</ta>
            <ta e="T665" id="Seg_9878" s="T664">v</ta>
            <ta e="T666" id="Seg_9879" s="T665">ptcl</ta>
            <ta e="T667" id="Seg_9880" s="T666">v</ta>
            <ta e="T668" id="Seg_9881" s="T667">que</ta>
            <ta e="T669" id="Seg_9882" s="T668">ptcl</ta>
            <ta e="T670" id="Seg_9883" s="T669">v</ta>
            <ta e="T671" id="Seg_9884" s="T670">adv</ta>
            <ta e="T672" id="Seg_9885" s="T671">v</ta>
            <ta e="T673" id="Seg_9886" s="T672">conj</ta>
            <ta e="T674" id="Seg_9887" s="T791">v</ta>
            <ta e="T675" id="Seg_9888" s="T674">conj</ta>
            <ta e="T676" id="Seg_9889" s="T675">ptcl</ta>
            <ta e="T677" id="Seg_9890" s="T676">n</ta>
            <ta e="T678" id="Seg_9891" s="T677">v</ta>
            <ta e="T680" id="Seg_9892" s="T678">ptcl</ta>
            <ta e="T681" id="Seg_9893" s="T792">ptcl</ta>
            <ta e="T682" id="Seg_9894" s="T681">v</ta>
            <ta e="T683" id="Seg_9895" s="T682">v</ta>
            <ta e="T684" id="Seg_9896" s="T683">v</ta>
            <ta e="T685" id="Seg_9897" s="T684">v</ta>
            <ta e="T686" id="Seg_9898" s="T685">adv</ta>
            <ta e="T688" id="Seg_9899" s="T686">adv</ta>
            <ta e="T689" id="Seg_9900" s="T688">ptcl</ta>
            <ta e="T690" id="Seg_9901" s="T689">ptcl</ta>
            <ta e="T691" id="Seg_9902" s="T690">adv</ta>
            <ta e="T692" id="Seg_9903" s="T691">v</ta>
            <ta e="T693" id="Seg_9904" s="T692">interj</ta>
            <ta e="T694" id="Seg_9905" s="T693">adv</ta>
            <ta e="T695" id="Seg_9906" s="T694">pers</ta>
            <ta e="T696" id="Seg_9907" s="T695">adv</ta>
            <ta e="T697" id="Seg_9908" s="T696">v</ta>
            <ta e="T698" id="Seg_9909" s="T697">adv</ta>
            <ta e="T699" id="Seg_9910" s="T698">adv</ta>
            <ta e="T700" id="Seg_9911" s="T699">adv</ta>
            <ta e="T701" id="Seg_9912" s="T700">v</ta>
            <ta e="T702" id="Seg_9913" s="T701">ptcl</ta>
            <ta e="T703" id="Seg_9914" s="T702">que</ta>
            <ta e="T704" id="Seg_9915" s="T703">cardnum</ta>
            <ta e="T705" id="Seg_9916" s="T704">n</ta>
            <ta e="T706" id="Seg_9917" s="T705">v</ta>
            <ta e="T707" id="Seg_9918" s="T706">pers</ta>
            <ta e="T708" id="Seg_9919" s="T707">v</ta>
            <ta e="T709" id="Seg_9920" s="T708">pers</ta>
            <ta e="T710" id="Seg_9921" s="T709">v</ta>
            <ta e="T711" id="Seg_9922" s="T710">adv</ta>
            <ta e="T712" id="Seg_9923" s="T711">pers</ta>
            <ta e="T713" id="Seg_9924" s="T712">adv</ta>
            <ta e="T714" id="Seg_9925" s="T713">pers</ta>
            <ta e="T715" id="Seg_9926" s="T714">v</ta>
            <ta e="T717" id="Seg_9927" s="T716">que</ta>
            <ta e="T718" id="Seg_9928" s="T717">n</ta>
            <ta e="T719" id="Seg_9929" s="T718">n</ta>
            <ta e="T720" id="Seg_9930" s="T719">v</ta>
            <ta e="T721" id="Seg_9931" s="T720">ptcl</ta>
            <ta e="T722" id="Seg_9932" s="T721">n</ta>
            <ta e="T723" id="Seg_9933" s="T722">v</ta>
            <ta e="T724" id="Seg_9934" s="T723">ptcl</ta>
            <ta e="T725" id="Seg_9935" s="T724">adv</ta>
            <ta e="T726" id="Seg_9936" s="T725">v</ta>
            <ta e="T727" id="Seg_9937" s="T726">adv</ta>
            <ta e="T728" id="Seg_9938" s="T727">v</ta>
            <ta e="T729" id="Seg_9939" s="T728">pers</ta>
            <ta e="T730" id="Seg_9940" s="T729">adv</ta>
            <ta e="T731" id="Seg_9941" s="T730">adv</ta>
            <ta e="T732" id="Seg_9942" s="T731">v</ta>
            <ta e="T733" id="Seg_9943" s="T732">cardnum</ta>
            <ta e="T734" id="Seg_9944" s="T733">n</ta>
            <ta e="T735" id="Seg_9945" s="T734">v</ta>
            <ta e="T736" id="Seg_9946" s="T735">conj</ta>
            <ta e="T737" id="Seg_9947" s="T736">cardnum</ta>
            <ta e="T738" id="Seg_9948" s="T737">adj</ta>
            <ta e="T739" id="Seg_9949" s="T738">cop</ta>
            <ta e="T740" id="Seg_9950" s="T739">pers</ta>
            <ta e="T741" id="Seg_9951" s="T740">n</ta>
            <ta e="T742" id="Seg_9952" s="T741">v</ta>
            <ta e="T743" id="Seg_9953" s="T742">v</ta>
            <ta e="T744" id="Seg_9954" s="T743">dempro</ta>
            <ta e="T745" id="Seg_9955" s="T744">adj</ta>
            <ta e="T746" id="Seg_9956" s="T745">ptcl</ta>
            <ta e="T747" id="Seg_9957" s="T746">dempro</ta>
            <ta e="T748" id="Seg_9958" s="T747">cardnum</ta>
            <ta e="T749" id="Seg_9959" s="T748">n</ta>
            <ta e="T750" id="Seg_9960" s="T749">v</ta>
            <ta e="T751" id="Seg_9961" s="T750">v</ta>
            <ta e="T752" id="Seg_9962" s="T751">adj</ta>
            <ta e="T753" id="Seg_9963" s="T752">cop</ta>
            <ta e="T754" id="Seg_9964" s="T753">ptcl</ta>
            <ta e="T755" id="Seg_9965" s="T754">dempro</ta>
            <ta e="T756" id="Seg_9966" s="T755">v</ta>
            <ta e="T757" id="Seg_9967" s="T756">adv</ta>
            <ta e="T758" id="Seg_9968" s="T757">n</ta>
            <ta e="T759" id="Seg_9969" s="T758">interj</ta>
            <ta e="T760" id="Seg_9970" s="T759">que</ta>
            <ta e="T761" id="Seg_9971" s="T760">adv</ta>
            <ta e="T762" id="Seg_9972" s="T761">cardnum</ta>
            <ta e="T763" id="Seg_9973" s="T762">adv</ta>
            <ta e="T764" id="Seg_9974" s="T763">v</ta>
            <ta e="T765" id="Seg_9975" s="T764">pers</ta>
            <ta e="T766" id="Seg_9976" s="T765">v</ta>
            <ta e="T767" id="Seg_9977" s="T766">ptcl</ta>
            <ta e="T768" id="Seg_9978" s="T767">adv</ta>
            <ta e="T769" id="Seg_9979" s="T768">v</ta>
            <ta e="T770" id="Seg_9980" s="T769">aux</ta>
            <ta e="T771" id="Seg_9981" s="T770">collnum</ta>
            <ta e="T772" id="Seg_9982" s="T771">cardnum</ta>
            <ta e="T773" id="Seg_9983" s="T772">n</ta>
            <ta e="T774" id="Seg_9984" s="T773">cop</ta>
            <ta e="T775" id="Seg_9985" s="T774">n</ta>
            <ta e="T776" id="Seg_9986" s="T775">cop</ta>
            <ta e="T777" id="Seg_9987" s="T776">collnum</ta>
            <ta e="T778" id="Seg_9988" s="T777">ptcl</ta>
            <ta e="T779" id="Seg_9989" s="T778">v</ta>
            <ta e="T780" id="Seg_9990" s="T779">v</ta>
            <ta e="T781" id="Seg_9991" s="T780">adj</ta>
            <ta e="T782" id="Seg_9992" s="T781">dempro</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T1" id="Seg_9993" s="T0">EV:cult</ta>
            <ta e="T28" id="Seg_9994" s="T27">EV:cult</ta>
            <ta e="T62" id="Seg_9995" s="T61">EV:cult</ta>
            <ta e="T84" id="Seg_9996" s="T83">EV:cult</ta>
            <ta e="T97" id="Seg_9997" s="T96">EV:cult</ta>
            <ta e="T119" id="Seg_9998" s="T118">EV:core</ta>
            <ta e="T140" id="Seg_9999" s="T139">EV:core</ta>
            <ta e="T146" id="Seg_10000" s="T145">EV:cult</ta>
            <ta e="T155" id="Seg_10001" s="T154">EV:gram (DIM)</ta>
            <ta e="T169" id="Seg_10002" s="T168">EV:cult</ta>
            <ta e="T174" id="Seg_10003" s="T173">RUS:gram</ta>
            <ta e="T178" id="Seg_10004" s="T177">RUS:mod</ta>
            <ta e="T191" id="Seg_10005" s="T190">EV:gram (INTNS)</ta>
            <ta e="T194" id="Seg_10006" s="T193">EV:cult</ta>
            <ta e="T204" id="Seg_10007" s="T203">EV:core</ta>
            <ta e="T210" id="Seg_10008" s="T209">EV:cult</ta>
            <ta e="T223" id="Seg_10009" s="T222">EV:cult</ta>
            <ta e="T225" id="Seg_10010" s="T224">EV:cult</ta>
            <ta e="T226" id="Seg_10011" s="T225">EV:core</ta>
            <ta e="T227" id="Seg_10012" s="T226">EV:cult</ta>
            <ta e="T228" id="Seg_10013" s="T227">EV:core</ta>
            <ta e="T232" id="Seg_10014" s="T786">EV:gram (DIM)</ta>
            <ta e="T235" id="Seg_10015" s="T234">EV:cult</ta>
            <ta e="T237" id="Seg_10016" s="T235">EV:core</ta>
            <ta e="T243" id="Seg_10017" s="T242">EV:cult</ta>
            <ta e="T244" id="Seg_10018" s="T243">EV:core</ta>
            <ta e="T245" id="Seg_10019" s="T244">EV:cult</ta>
            <ta e="T246" id="Seg_10020" s="T245">EV:core</ta>
            <ta e="T263" id="Seg_10021" s="T262">EV:gram (DIM)</ta>
            <ta e="T277" id="Seg_10022" s="T276">RUS:mod</ta>
            <ta e="T292" id="Seg_10023" s="T291">RUS:core</ta>
            <ta e="T293" id="Seg_10024" s="T292">RUS:core</ta>
            <ta e="T304" id="Seg_10025" s="T303">RUS:gram</ta>
            <ta e="T321" id="Seg_10026" s="T320">RUS:mod</ta>
            <ta e="T322" id="Seg_10027" s="T321">EV:cult</ta>
            <ta e="T325" id="Seg_10028" s="T324">EV:cult</ta>
            <ta e="T328" id="Seg_10029" s="T327">EV:cult</ta>
            <ta e="T329" id="Seg_10030" s="T328">EV:core</ta>
            <ta e="T330" id="Seg_10031" s="T329">EV:cult</ta>
            <ta e="T331" id="Seg_10032" s="T330">EV:core</ta>
            <ta e="T337" id="Seg_10033" s="T336">EV:cult</ta>
            <ta e="T338" id="Seg_10034" s="T337">EV:core</ta>
            <ta e="T343" id="Seg_10035" s="T342">EV:gram (DIM)</ta>
            <ta e="T346" id="Seg_10036" s="T345">EV:cult</ta>
            <ta e="T347" id="Seg_10037" s="T346">EV:core</ta>
            <ta e="T348" id="Seg_10038" s="T347">EV:cult</ta>
            <ta e="T349" id="Seg_10039" s="T348">EV:core</ta>
            <ta e="T354" id="Seg_10040" s="T353">EV:cult</ta>
            <ta e="T355" id="Seg_10041" s="T354">EV:core</ta>
            <ta e="T372" id="Seg_10042" s="T371">EV:gram (INTNS)</ta>
            <ta e="T435" id="Seg_10043" s="T434">RUS:gram</ta>
            <ta e="T436" id="Seg_10044" s="T435">EV:gram (DIM)</ta>
            <ta e="T439" id="Seg_10045" s="T438">EV:gram (DIM)</ta>
            <ta e="T441" id="Seg_10046" s="T440">RUS:gram</ta>
            <ta e="T470" id="Seg_10047" s="T469">EV:cult</ta>
            <ta e="T499" id="Seg_10048" s="T498">EV:cult</ta>
            <ta e="T516" id="Seg_10049" s="T515">EV:cult</ta>
            <ta e="T520" id="Seg_10050" s="T519">RUS:mod</ta>
            <ta e="T526" id="Seg_10051" s="T525">RUS:gram</ta>
            <ta e="T536" id="Seg_10052" s="T535">RUS:cult</ta>
            <ta e="T549" id="Seg_10053" s="T548">EV:gram (DIM)</ta>
            <ta e="T575" id="Seg_10054" s="T574">RUS:core</ta>
            <ta e="T607" id="Seg_10055" s="T606">RUS:mod</ta>
            <ta e="T628" id="Seg_10056" s="T627">RUS:gram</ta>
            <ta e="T641" id="Seg_10057" s="T640">RUS:mod</ta>
            <ta e="T645" id="Seg_10058" s="T644">EV:core</ta>
            <ta e="T646" id="Seg_10059" s="T645">EV:cult</ta>
            <ta e="T649" id="Seg_10060" s="T648">RUS:gram</ta>
            <ta e="T664" id="Seg_10061" s="T663">EV:core</ta>
            <ta e="T673" id="Seg_10062" s="T672">RUS:gram</ta>
            <ta e="T675" id="Seg_10063" s="T674">RUS:gram</ta>
            <ta e="T711" id="Seg_10064" s="T710">RUS:mod</ta>
            <ta e="T736" id="Seg_10065" s="T735">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_10066" s="T0">Once there lived the old men Butterfly and Crutch.</ta>
            <ta e="T11" id="Seg_10067" s="T6">They live far from each other, they know each other.</ta>
            <ta e="T19" id="Seg_10068" s="T11">One of them has a house in this direction, the other one has a house in that direction.</ta>
            <ta e="T24" id="Seg_10069" s="T19">Living so they meet and chat with each other.</ta>
            <ta e="T42" id="Seg_10070" s="T24">"Well", the old men Butterfly and Crutch discuss, "let us put together our food, which is enough for one year, and let us eat it."</ta>
            <ta e="T52" id="Seg_10071" s="T42">"If we want, we will live long, if we only come through the year.</ta>
            <ta e="T56" id="Seg_10072" s="T52">There are bad years."</ta>
            <ta e="T58" id="Seg_10073" s="T56">"Well, fine.</ta>
            <ta e="T66" id="Seg_10074" s="T58">Then now, Butterfly, let us eat your food."</ta>
            <ta e="T75" id="Seg_10075" s="T66">"Yes, old man Crutch, let us go into his house and eat."</ta>
            <ta e="T79" id="Seg_10076" s="T75">"Well, fine, let us eat so."</ta>
            <ta e="T83" id="Seg_10077" s="T79">Oh, and they are eating.</ta>
            <ta e="T86" id="Seg_10078" s="T83">They are eating in Butterfly's house.</ta>
            <ta e="T91" id="Seg_10079" s="T86">One half, until it runs out.</ta>
            <ta e="T98" id="Seg_10080" s="T91">Well, having eaten their food ran out, Butterfly's one.</ta>
            <ta e="T102" id="Seg_10081" s="T98">"Well, let us go to the old man Crutch."</ta>
            <ta e="T107" id="Seg_10082" s="T102">They go and start eating his food.</ta>
            <ta e="T114" id="Seg_10083" s="T107">Oh, they went to the old man Crutch and they eat.</ta>
            <ta e="T116" id="Seg_10084" s="T114">They are eating and eating.</ta>
            <ta e="T128" id="Seg_10085" s="T116">"Well", one friend [says], "my food is about to run out", he says, "go home.</ta>
            <ta e="T135" id="Seg_10086" s="T128">Hunting for yourself, I don't want to eat up all my food."</ta>
            <ta e="T140" id="Seg_10087" s="T135">Well, the [other] friend went left.</ta>
            <ta e="T146" id="Seg_10088" s="T140">Oh, going so what shall he eat at home, Butterfly?</ta>
            <ta e="T158" id="Seg_10089" s="T146">He went home, there is nothing, he found some small piece of sinewy meat, he cooked it for himself.</ta>
            <ta e="T161" id="Seg_10090" s="T158">He cooks it for himself and sits.</ta>
            <ta e="T169" id="Seg_10091" s="T161">Well, then he saw, he again had hunt something, Butterfly.</ta>
            <ta e="T178" id="Seg_10092" s="T169">Then the food of that one, of the old man Crutch has run out completely.</ta>
            <ta e="T182" id="Seg_10093" s="T178">Well, he sits, he doesn't take counsel with him.</ta>
            <ta e="T193" id="Seg_10094" s="T182">"Well, he shall not come, I am hunting and living for myself, something", he thinks.</ta>
            <ta e="T196" id="Seg_10095" s="T193">Butterfly lives.</ta>
            <ta e="T201" id="Seg_10096" s="T196">As he was living so, the old man Crutch came.</ta>
            <ta e="T204" id="Seg_10097" s="T201">He saw how he was going, his friend.</ta>
            <ta e="T216" id="Seg_10098" s="T204">"Oh, the old man Crutch is going apparently", Butterfly thinks (…) and locked his house.</ta>
            <ta e="T219" id="Seg_10099" s="T216">Well, he comes.</ta>
            <ta e="T224" id="Seg_10100" s="T219">Ah, he comes to Butterfly.</ta>
            <ta e="T228" id="Seg_10101" s="T224">"Butterfly, be kind, Butterfly, be kind,</ta>
            <ta e="T237" id="Seg_10102" s="T228">give something (…) help me throwing a piece of sinewy meat, Butterfly, be kind.</ta>
            <ta e="T242" id="Seg_10103" s="T237">(…), at my day stood up, my year (…),</ta>
            <ta e="T252" id="Seg_10104" s="T242">Butterfly, be kind, Butterfly, be kind, I am dying", one, he was singing so.</ta>
            <ta e="T254" id="Seg_10105" s="T252">"Well, go, go!"</ta>
            <ta e="T265" id="Seg_10106" s="T254">He threw some to something (…) sticking, dried piece of sinewy meat.</ta>
            <ta e="T271" id="Seg_10107" s="T265">"Go", that one says and Crutch went.</ta>
            <ta e="T279" id="Seg_10108" s="T271">"(Go to his home), go home", he says, "I won't give you more."</ta>
            <ta e="T283" id="Seg_10109" s="T279">So he went, that human.</ta>
            <ta e="T300" id="Seg_10110" s="T283">He goes and will eat that much, that blood, he cooked just soup and ate it.</ta>
            <ta e="T306" id="Seg_10111" s="T300">He doesn't find anything and so on.</ta>
            <ta e="T320" id="Seg_10112" s="T306">He lay some days like this, he hardly goes out, starving so, the old man Crutch.</ta>
            <ta e="T323" id="Seg_10113" s="T320">He is again about to go to Butterfly.</ta>
            <ta e="T327" id="Seg_10114" s="T323">He came and sang again to Butterfly: </ta>
            <ta e="T331" id="Seg_10115" s="T327">"Butterfly, be kind, Butterfly, be kind,</ta>
            <ta e="T336" id="Seg_10116" s="T331">I am dying, I am dying,</ta>
            <ta e="T349" id="Seg_10117" s="T336">Butterfly, be kind, help me throwing a piece of sinewy meat, Butterfly, be kind, Butterfly, be kind,</ta>
            <ta e="T360" id="Seg_10118" s="T349">(…) Butterfly, be kind (…) get sinewy meat, I am singing for me."</ta>
            <ta e="T374" id="Seg_10119" s="T360">"Hey, I have no sinewy meat at all, go, go", he said and chased him back, he didn't give anything.</ta>
            <ta e="T385" id="Seg_10120" s="T374">Where shall he go, outside he would freeze, whereto, at home they wouldn't let him in, shame.</ta>
            <ta e="T389" id="Seg_10121" s="T385">Well, he goes, this human.</ta>
            <ta e="T404" id="Seg_10122" s="T389">He went and as we was just arriving, he arrives beneath his house, he saw that a female reindeer with a calf is going around his house.</ta>
            <ta e="T416" id="Seg_10123" s="T404">Whereever it came from, he doesn't know the reindeer (…) the reindeer just goes, it came.</ta>
            <ta e="T431" id="Seg_10124" s="T416">To his destiny to die, to his destiny to revive, calf and female reindeer came to him and licked him.</ta>
            <ta e="T445" id="Seg_10125" s="T431">As he had wet himself, it came and having caught the reindeer calf, he took a knife, made it make "kort" and killed it.</ta>
            <ta e="T447" id="Seg_10126" s="T445">His mother is going.</ta>
            <ta e="T460" id="Seg_10127" s="T447">Well, he skinned that out, cooked the raw [meat], cooked and ate it.</ta>
            <ta e="T464" id="Seg_10128" s="T460">Well, his eyes got lighter then.</ta>
            <ta e="T475" id="Seg_10129" s="T464">"Oh, no I am saved, I don't go to Butterfly, I got food though", he thinks.</ta>
            <ta e="T480" id="Seg_10130" s="T475">The human, who has gotten food, cooks and eats it.</ta>
            <ta e="T481" id="Seg_10131" s="T480">Ook.</ta>
            <ta e="T486" id="Seg_10132" s="T481">Well, how many days he lived like that.</ta>
            <ta e="T491" id="Seg_10133" s="T486">He has a reindeer, he has caught the reindeer, the female reindeer.</ta>
            <ta e="T495" id="Seg_10134" s="T491">He has a female reindeer, what will be then?</ta>
            <ta e="T501" id="Seg_10135" s="T495">Oh, as he was lying so, Butterfly's voice came [was heard].</ta>
            <ta e="T504" id="Seg_10136" s="T501">And he just looked.</ta>
            <ta e="T515" id="Seg_10137" s="T504">"Well, I'll make it the same way, shall he come", he thought, "I have food here."</ta>
            <ta e="T521" id="Seg_10138" s="T515">Butterfly makes and is hardly able to walk out of hunger.</ta>
            <ta e="T524" id="Seg_10139" s="T521">He is hardly able to walk.</ta>
            <ta e="T527" id="Seg_10140" s="T524">He came and came.</ta>
            <ta e="T533" id="Seg_10141" s="T527">As he came there, he [Crutch?] locked the house, he locked it.</ta>
            <ta e="T542" id="Seg_10142" s="T533">"Well, I'll punish him, not long ago he tortured me the same way."</ta>
            <ta e="T546" id="Seg_10143" s="T542">"Old man Crutch, old man Crutch,</ta>
            <ta e="T560" id="Seg_10144" s="T546">help me giving some piece of sinewy meat, old man Crutch, I am dying, I am dying,</ta>
            <ta e="T571" id="Seg_10145" s="T560">at my day there stood, if you are an inanimate human, old man Crutch, old man Crutch", he sang for himself.</ta>
            <ta e="T574" id="Seg_10146" s="T571">He gave nothing.</ta>
            <ta e="T596" id="Seg_10147" s="T574">Something he made from the hoof of the reindeer calf, with (...) the hoof or what, that with ((unknown)) he threw it.</ta>
            <ta e="T603" id="Seg_10148" s="T596">"Go, eat", he said and threw two hooves.</ta>
            <ta e="T615" id="Seg_10149" s="T603">"Go home", he said, "don't come again, not long ago you tortured me, too."</ta>
            <ta e="T620" id="Seg_10150" s="T615">What shall that one do, he went.</ta>
            <ta e="T624" id="Seg_10151" s="T620">Well, he went home.</ta>
            <ta e="T639" id="Seg_10152" s="T624">Again… he went and the next morning, after several days have passed, he comes again.</ta>
            <ta e="T647" id="Seg_10153" s="T639">Well, he was apparently close to die, his friend, Butterfly reached his limits: </ta>
            <ta e="T652" id="Seg_10154" s="T647">When he chases him, he doesn't run away, if he goes in, he doesn't come.</ta>
            <ta e="T666" id="Seg_10155" s="T652">Oh, he can't do nothing, his heart aches, the human is sick, he felt sorry for his friend.</ta>
            <ta e="T680" id="Seg_10156" s="T666">Feeling sorry he can't do nothing, chasing him (…) he goes and won't arrive at home.</ta>
            <ta e="T688" id="Seg_10157" s="T680">(…) if so: "Come in", he said, "eat, let us have a good conversation."</ta>
            <ta e="T692" id="Seg_10158" s="T688">And they indeed talked to each other.</ta>
            <ta e="T699" id="Seg_10159" s="T692">"Well, now let us have a good conversation.</ta>
            <ta e="T707" id="Seg_10160" s="T699">We will live together, we shall we live in two directions?</ta>
            <ta e="T711" id="Seg_10161" s="T707">You see, I got a reindeer.</ta>
            <ta e="T716" id="Seg_10162" s="T711">You did offend me not long ago…</ta>
            <ta e="T723" id="Seg_10163" s="T716">Whatever kind of God brought the reindeer, I fed the reindeer.</ta>
            <ta e="T730" id="Seg_10164" s="T723">Well, as I got a reindeer now, let us live together.</ta>
            <ta e="T739" id="Seg_10165" s="T730">Living together let us hunt following one advise and let us have one house.</ta>
            <ta e="T755" id="Seg_10166" s="T739">Let us bring your house, I have a reindeer, though, our reindeer will pull it, whatever there is.</ta>
            <ta e="T765" id="Seg_10167" s="T755">Let us bring your reindeer here, eh, your whatchamacallit, let us live together here."</ta>
            <ta e="T770" id="Seg_10168" s="T765">They agreed and started to live together.</ta>
            <ta e="T782" id="Seg_10169" s="T770">They gave advise to each other, they were sly, they live in richness and satiety, that's the end.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_10170" s="T0">Es lebten einmal die alten Männer Schmetterling und Krücke.</ta>
            <ta e="T11" id="Seg_10171" s="T6">Sie leben weit voneinander entfernt, sie kennen einander.</ta>
            <ta e="T19" id="Seg_10172" s="T11">Der eine hat ein Haus in diese Richtung, der andere hat ein Haus in jene Richtung.</ta>
            <ta e="T24" id="Seg_10173" s="T19">Sie leben und treffen sich zu zweit, sie unterhalten sich.</ta>
            <ta e="T42" id="Seg_10174" s="T24">"Hey", besprechen die alten Männer Schmetterling und Krücke, "lass uns unsere Nahrung, die für ein Jahr reicht, zusammenlegen und essen."</ta>
            <ta e="T52" id="Seg_10175" s="T42">"Wenn wir wollen, leben wir wohl lange, wenn wir nur durch das Jahr kommen.</ta>
            <ta e="T56" id="Seg_10176" s="T52">Es gibt schlechte Jahre."</ta>
            <ta e="T58" id="Seg_10177" s="T56">"Ja, in Ordnung.</ta>
            <ta e="T66" id="Seg_10178" s="T58">Jetzt dann, Schmetterling, lass uns dein Essen essen."</ta>
            <ta e="T75" id="Seg_10179" s="T66">"Ja, alter Mann Krücke, lass uns in sein Haus gehen und essen."</ta>
            <ta e="T79" id="Seg_10180" s="T75">"Nun gut, lass uns so essen."</ta>
            <ta e="T83" id="Seg_10181" s="T79">Oh, und sie essen.</ta>
            <ta e="T86" id="Seg_10182" s="T83">Sie essen in Schmetterlings Haus.</ta>
            <ta e="T91" id="Seg_10183" s="T86">Eine Hälfte, bis die alle ist.</ta>
            <ta e="T98" id="Seg_10184" s="T91">Nun sie aßen und ihre Nahrung ging zu Ende, die von Schmetterling.</ta>
            <ta e="T102" id="Seg_10185" s="T98">"Nun, gehen wir zum alten Mann Krücke."</ta>
            <ta e="T107" id="Seg_10186" s="T102">Sie gehen und fangen an, seine Nahrung zu essen.</ta>
            <ta e="T114" id="Seg_10187" s="T107">Oh, sie gingen zum alten Mann Krücke und essen.</ta>
            <ta e="T116" id="Seg_10188" s="T114">Sie essen und essen.</ta>
            <ta e="T128" id="Seg_10189" s="T116">"Hey", [sagt] der Freund, "meine Nahrung geht bald zu Ende", sagt er, "geh zu dir nach Hause.</ta>
            <ta e="T135" id="Seg_10190" s="T128">Für dich selbst jagend, ich möchte nicht alle meine Nahrung aufessen."</ta>
            <ta e="T140" id="Seg_10191" s="T135">Nun, so ging der [andere] Freund.</ta>
            <ta e="T146" id="Seg_10192" s="T140">Oh, wenn er geht, was soll er zuhause esssen, Schmetterling?</ta>
            <ta e="T158" id="Seg_10193" s="T146">Er ging nach Hause, dort ist nichts, er fand irgendein sehniges Stückchen Fleisch, das kochte er sich. </ta>
            <ta e="T161" id="Seg_10194" s="T158">Er kocht es sich und sitzt.</ta>
            <ta e="T169" id="Seg_10195" s="T161">Nun, dann sah er, dass er sich wieder etwas gejagt hatte, Schmetterling.</ta>
            <ta e="T178" id="Seg_10196" s="T169">Dann von jenem, vom alten Mann Krücke war sein Essen ganz zu Ende gegangen.</ta>
            <ta e="T182" id="Seg_10197" s="T178">Nun er sitzt, er berät sich nicht mit ihm.</ta>
            <ta e="T193" id="Seg_10198" s="T182">"Nun, soll er nicht kommen, ich lebe und jage für mich selbst, irgendwas", denkt er.</ta>
            <ta e="T196" id="Seg_10199" s="T193">Schmetterling lebt so.</ta>
            <ta e="T201" id="Seg_10200" s="T196">Wie er so lebte, kam der alte Mann Krücke.</ta>
            <ta e="T204" id="Seg_10201" s="T201">Er sah, wie er ging, sein Freund.</ta>
            <ta e="T216" id="Seg_10202" s="T204">"Oh, der alte Mann Krücke geht umher", denkt Schmetterling (…) und verschloss sein Haus.</ta>
            <ta e="T219" id="Seg_10203" s="T216">Nun, er kommt.</ta>
            <ta e="T224" id="Seg_10204" s="T219">Ah, er kommt zu Schmetterling.</ta>
            <ta e="T228" id="Seg_10205" s="T224">"Schmetterling, sei freundlich, Schmetterling, sei freundlich,</ta>
            <ta e="T237" id="Seg_10206" s="T228">gib etwas (…) hilf und wirf ein Stückchen sehniges Fleisch, Schmetterling, sei freundlich.</ta>
            <ta e="T242" id="Seg_10207" s="T237">(…), an meinem Tag stand auf, mein Jahr (…),</ta>
            <ta e="T252" id="Seg_10208" s="T242">Schmetterling sei freundlich, Schmetterling sei freundlich, ich sterbe", eins, sang er so.</ta>
            <ta e="T254" id="Seg_10209" s="T252">"Ach, geh, geh!"</ta>
            <ta e="T265" id="Seg_10210" s="T254">Ein an irgendein (…) klebendes, vertrocknetes Stückchen sehniges Fleisch warf er.</ta>
            <ta e="T271" id="Seg_10211" s="T265">"Geh", sagt jener und Krücke ging.</ta>
            <ta e="T279" id="Seg_10212" s="T271">"(Geh zu ihm nach Hause), geh nach Hause", sagt er, "mehr gebe ich dir nicht."</ta>
            <ta e="T283" id="Seg_10213" s="T279">Nun, er ging, der Mensch.</ta>
            <ta e="T300" id="Seg_10214" s="T283">Er geht und er wird so viel essen, jenes Blut, er kochte bloße Suppe und aß sie.</ta>
            <ta e="T306" id="Seg_10215" s="T300">Er findet nichts und so weiter.</ta>
            <ta e="T320" id="Seg_10216" s="T306">Einige Tage lag er so, er ging kaum hinaus, so hungernd, der alte Mann Krücke.</ta>
            <ta e="T323" id="Seg_10217" s="T320">Er macht sich wieder auf den Weg zu Schmetterling.</ta>
            <ta e="T327" id="Seg_10218" s="T323">Er kam und besang Schmetterling wieder:</ta>
            <ta e="T331" id="Seg_10219" s="T327">"Schmetterling sei freundlich, Schmetterling sei freundlich,</ta>
            <ta e="T336" id="Seg_10220" s="T331">ich sterbe, ich sterbe,</ta>
            <ta e="T349" id="Seg_10221" s="T336">Schmetterling sei freundlich, hilf mir und wirf ein Stückchen sehniges Fleisch, Schmetterling sei freundlich, Schmetterling sein freundlich,</ta>
            <ta e="T360" id="Seg_10222" s="T349">(…) Schmetterling sei freundlich (…) hol sehniges Fleisch, ich singe für mich."</ta>
            <ta e="T374" id="Seg_10223" s="T360">"Ach, ich habe nicht ein Stückchen sehniges Fleisch, geh, geh", sagte er und jagte ihn zurück, er gab nichts.</ta>
            <ta e="T385" id="Seg_10224" s="T374">Wo soll er hingehen, draußen würde er erfrieren, wohin, nach Hause wird er nicht gelassen, Scham.</ta>
            <ta e="T389" id="Seg_10225" s="T385">Nun, er geht, dieser Mensch.</ta>
            <ta e="T404" id="Seg_10226" s="T389">Er ging und als er gerade ankam, er kommt an seinem Haus an, da sah er, dass eine Rentierkuh mit Kalb um sein Haus herumstreift.</ta>
            <ta e="T416" id="Seg_10227" s="T404">Woher auch immer es kam, er kennt das Rentier nicht (…) das Rentier geht nur, es kam.</ta>
            <ta e="T431" id="Seg_10228" s="T416">Zu seiner Bestimmung, zu sterben, zu seiner Bestimmung, aufzuleben, kamen Kalb und Rentierkuh zu ihm selbst und leckten ihn ab.</ta>
            <ta e="T445" id="Seg_10229" s="T431">Als er sich eingenässt hatte, kam es und nachdem er das Rentierkalb gefangen hatte, nahm er ein Messer, ließ es "kort" machen und tötete es.</ta>
            <ta e="T447" id="Seg_10230" s="T445">Seine Mutter geht.</ta>
            <ta e="T460" id="Seg_10231" s="T447">Nun, er zog diesem die Haut ab, kochte das rohe Fleisch, kochte und aß es.</ta>
            <ta e="T464" id="Seg_10232" s="T460">Nun seine Augen wurden da heller.</ta>
            <ta e="T475" id="Seg_10233" s="T464">"Oh, jetzt bin ich gerettet, ich gehe nicht zu Schmetterling und so, ich habe doch Essen bekommen", denkt er.</ta>
            <ta e="T480" id="Seg_10234" s="T475">Der Mensch, der Essen bekommen hat, kocht es sich und isst.</ta>
            <ta e="T481" id="Seg_10235" s="T480">Ook.</ta>
            <ta e="T486" id="Seg_10236" s="T481">Nun, wie viele Tage lebte er so.</ta>
            <ta e="T491" id="Seg_10237" s="T486">Er hat ein Rentier, er hat das Rentier gefangen, die Rentierkuh.</ta>
            <ta e="T495" id="Seg_10238" s="T491">Er hat eine Rentierkuh, was ist jetzt?</ta>
            <ta e="T501" id="Seg_10239" s="T495">Oh, wie er so dalag, kam Schmetterlings Stimme.</ta>
            <ta e="T504" id="Seg_10240" s="T501">Und er schaute.</ta>
            <ta e="T515" id="Seg_10241" s="T504">"Nun ich mache es genauso, soll er nur kommen", dachte er, "bei mir ist Essen."</ta>
            <ta e="T521" id="Seg_10242" s="T515">Schmetterling macht und kann kaum gehen vor Hunger.</ta>
            <ta e="T524" id="Seg_10243" s="T521">Er kann kaum gehen.</ta>
            <ta e="T527" id="Seg_10244" s="T524">Er kam und kam.</ta>
            <ta e="T533" id="Seg_10245" s="T527">Als er ankam, verriegelte er [Krücke?] das Haus, er verriegelte es.</ta>
            <ta e="T542" id="Seg_10246" s="T533">"Nun, ich werde ihn bestrafen, vor Kurzem hat er mich auch so gequält."</ta>
            <ta e="T546" id="Seg_10247" s="T542">"Alter Mann Krücke, alter Mann Krücke,</ta>
            <ta e="T560" id="Seg_10248" s="T546">hilf und gib mir irgendein Stückchen sehniges Fleisch, alter Mann Krücke, ich sterbe, ich sterbe,</ta>
            <ta e="T571" id="Seg_10249" s="T560">an meinem Tag stand, wenn du ein lebloser Mensch bist, alter Mann Krücke, alter Mann Krücke", sang er für sich.</ta>
            <ta e="T574" id="Seg_10250" s="T571">Er gab nichts.</ta>
            <ta e="T596" id="Seg_10251" s="T574">Irgendetwas vom Huf dieses Rentierkalbes machte er, mit (...) den Huf oder was, das mit ((unknown)) warf er ihm hin.</ta>
            <ta e="T603" id="Seg_10252" s="T596">"Geh, iss", sagte er und warf zwei Hufe.</ta>
            <ta e="T615" id="Seg_10253" s="T603">"Geh nach Hause", sagt er, "komm nicht mehr, neulich hast du mich doch auch gequält."</ta>
            <ta e="T620" id="Seg_10254" s="T615">Was bleibt jenem übrig, er ging.</ta>
            <ta e="T624" id="Seg_10255" s="T620">Und er ging nach Hause.</ta>
            <ta e="T639" id="Seg_10256" s="T624">Wieder… er ging und am nächsten Morgen, nachdem einige Tage vergangen waren, kommt er wieder.</ta>
            <ta e="T647" id="Seg_10257" s="T639">Nun er war dem Tod offenbar sehr nahe, sein Freund, Schmetterling stieß an seine Grenzen:</ta>
            <ta e="T652" id="Seg_10258" s="T647">Wenn er ihn jagt, läuft er nicht weg, wenn er hineingeht, kommt er nicht.</ta>
            <ta e="T666" id="Seg_10259" s="T652">Oh, er kann nicht nichts machen, sein Herz schmerzt, der Mensch ist krank, er bemitleidete seinen Freund.</ta>
            <ta e="T680" id="Seg_10260" s="T666">Ihn bemitleidend kann er nicht nichts machen, ihn jagen (…) er geht und kommt doch nicht nach Hause.</ta>
            <ta e="T688" id="Seg_10261" s="T680">(…) wenn es so ist: "Komm rein", sagte er, "iss, lass uns dann gut unterhalten."</ta>
            <ta e="T692" id="Seg_10262" s="T688">Und sie unterhielten sich tatsächlich.</ta>
            <ta e="T699" id="Seg_10263" s="T692">"Nun, lass uns jetzt gut unterhalten.</ta>
            <ta e="T707" id="Seg_10264" s="T699">Wir werden zusamme leben, warum sollen wir zu zwei Seiten hin leben?</ta>
            <ta e="T711" id="Seg_10265" s="T707">Du siehst, ich habe ein Rentier bekommen.</ta>
            <ta e="T716" id="Seg_10266" s="T711">Du hast mich neulich gekränkt…</ta>
            <ta e="T723" id="Seg_10267" s="T716">Was für ein Rentier Gott auch gebracht hat, ich habe das Rentier ernährt.</ta>
            <ta e="T730" id="Seg_10268" s="T723">Wo ich jetzt das Rentier bekommen habe, lass uns jetzt zusammen leben.</ta>
            <ta e="T739" id="Seg_10269" s="T730">Wenn wir zusammen leben, lass uns nach einem Rat jagen und ein Haus haben.</ta>
            <ta e="T755" id="Seg_10270" s="T739">Lass uns dein Haus holen, ich habe doch ein Rentier, unser eines Rentier wird es ziehen, egal was das ist.</ta>
            <ta e="T765" id="Seg_10271" s="T755">Lass uns dein Rentier hierher bringen, äh, dein Dings, lass uns hier zusammen leben."</ta>
            <ta e="T770" id="Seg_10272" s="T765">Sie machten es ab und fingen an, zusammen zu leben.</ta>
            <ta e="T782" id="Seg_10273" s="T770">Zu zweit berieten sie sich, waren schlau, zu zweit lebten sie in Reichtum und Wohlstand, das ist das Ende.</ta>
         </annotation>
         <annotation name="fr" tierref="fr" />
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_10274" s="T0">Бабочка (старик) вместе с Тросточкой стариком жили, говорят.</ta>
            <ta e="T11" id="Seg_10275" s="T6">На расстоянии друг от друга живут, друг друга только навещают.</ta>
            <ta e="T19" id="Seg_10276" s="T11">Один – там дом имеет, один – здесь дом имеет.</ta>
            <ta e="T24" id="Seg_10277" s="T19">Поживая вдвоём встретились, вместе поговорили.</ta>
            <ta e="T42" id="Seg_10278" s="T24">"Эй, – (говорит) поговаривают вместе с Бабочкой с Тросточкой старик, – вот мы (в один) год нужную еду нашу вместе соединив поедим".</ta>
            <ta e="T52" id="Seg_10279" s="T42">"Если захотим, возможно, мы.. долго проживём, может быть, год чтобы коротать даже.</ta>
            <ta e="T56" id="Seg_10280" s="T52">Вот плохие года есть."</ta>
            <ta e="T58" id="Seg_10281" s="T56">"Вот, правда.</ta>
            <ta e="T66" id="Seg_10282" s="T58">Сейчас тогда (мы), Бабочка, твою пищу съедим".</ta>
            <ta e="T75" id="Seg_10283" s="T66">"Э, с Тросточкой старика, в его дом пойдя, поедим".</ta>
            <ta e="T79" id="Seg_10284" s="T75">"Ну, хорошо, вот так поедим."</ta>
            <ta e="T83" id="Seg_10285" s="T79">О-о, вот едят, разве что.</ta>
            <ta e="T86" id="Seg_10286" s="T83">В доме Бабочки едят.</ta>
            <ta e="T91" id="Seg_10287" s="T86">Одну половину, вот до сюда.</ta>
            <ta e="T98" id="Seg_10288" s="T91">Вот, когда ели, вот этого еда закончилась – к Бабочке относящаяся.</ta>
            <ta e="T102" id="Seg_10289" s="T98">"Эй, с Тросточкой к старику пойдём".</ta>
            <ta e="T107" id="Seg_10290" s="T102">Пойдя вот того еду поедят.</ta>
            <ta e="T114" id="Seg_10291" s="T107">О-о, с Тросточкой к старику пошли, вот кушают.</ta>
            <ta e="T116" id="Seg_10292" s="T114">Едят-едят да..</ta>
            <ta e="T128" id="Seg_10293" s="T116">"Ну, – один друг, – моя еда заканчивается вот, – говорит, – ты домой иди, </ta>
            <ta e="T135" id="Seg_10294" s="T128">сам охотясь, я свою еду всю съесть не хочу".</ta>
            <ta e="T140" id="Seg_10295" s="T135">Э, вот ушёл друг.</ta>
            <ta e="T146" id="Seg_10296" s="T140">Ой, пойдя, что есть будет дома, Бабочка?</ta>
            <ta e="T158" id="Seg_10297" s="T146">Пошёл домой, ничего у него нет, что-то давнишнее.. жилистое мясо найдя, то сварил.</ta>
            <ta e="T161" id="Seg_10298" s="T158">Сварив сидит вот.</ta>
            <ta e="T169" id="Seg_10299" s="T161">Ну, потом посмотрел, что-то поохотился снова Бабочка.</ta>
            <ta e="T178" id="Seg_10300" s="T169">Потом того, с Тросточкой старика еда закончилась было совсем.</ta>
            <ta e="T182" id="Seg_10301" s="T178">Вот живёт, не знается с ним.</ta>
            <ta e="T193" id="Seg_10302" s="T182">"Ну, пусть не приходит он, и сам охотясь проживу, одно что-нибудь", - говорит.</ta>
            <ta e="T196" id="Seg_10303" s="T193">Бабочка вот живёт.</ta>
            <ta e="T201" id="Seg_10304" s="T196">Вот когда он жил, с Тросточкой старик пришёл.</ta>
            <ta e="T204" id="Seg_10305" s="T201">Как идёт, увидел друга.</ta>
            <ta e="T216" id="Seg_10306" s="T204">"Ой, с Тросточкой старик идёт ведь", – Бабочка дом заперев, закрыл.</ta>
            <ta e="T219" id="Seg_10307" s="T216">Вот пришёл только. </ta>
            <ta e="T224" id="Seg_10308" s="T219">Вот, к Бабочке своему приходит.</ta>
            <ta e="T228" id="Seg_10309" s="T224">"Бабочка-дружок, Бабочка-дружок,</ta>
            <ta e="T237" id="Seg_10310" s="T228">ты дай-ка .. жилистого мяса бросив, спаси, Бабочка-дружок..</ta>
            <ta e="T242" id="Seg_10311" s="T237">…, .. в мой день встал год мой …</ta>
            <ta e="T252" id="Seg_10312" s="T242">Бабочка-дружок, Бабочка-дружок, умру", – одну, пел сидел вовсе.</ta>
            <ta e="T254" id="Seg_10313" s="T252">"Ой, иди-иди!"</ta>
            <ta e="T265" id="Seg_10314" s="T254">Одно что-то к ?? прилипший, .. засохшее жилистое мяcо бросил.</ta>
            <ta e="T271" id="Seg_10315" s="T265">"Иди", – говорит. На то .. с Тросточкой и ушёл.</ta>
            <ta e="T279" id="Seg_10316" s="T271">"Иди домой, – говорит, – больше не даю тебе.</ta>
            <ta e="T283" id="Seg_10317" s="T279">Ну, ушёл он (человек).</ta>
            <ta e="T300" id="Seg_10318" s="T283">Уйдя сколько съест он вот, сколько просто бульон только сварив съел вот. </ta>
            <ta e="T306" id="Seg_10319" s="T300">Ничего не находит, и ничего даже. </ta>
            <ta e="T320" id="Seg_10320" s="T306">Несколько дней лнжал вот, еле-еле выходить стал, так оголодав, с Тросточкой старик.</ta>
            <ta e="T323" id="Seg_10321" s="T320">Опять к своему Бабочке должен пойди.</ta>
            <ta e="T327" id="Seg_10322" s="T323">Прийдя к своему Бабочке снова запел:</ta>
            <ta e="T331" id="Seg_10323" s="T327">"Бабочка-дружок, Бабочка-дружок,</ta>
            <ta e="T336" id="Seg_10324" s="T331">умру ведь, умру ведь,</ta>
            <ta e="T349" id="Seg_10325" s="T336">Бабочка-дружок, хоть одно жилистое мясо брось, спаси, Бабочка-дружок, Бабочка-дружок</ta>
            <ta e="T360" id="Seg_10326" s="T349">..ок, забыв во.. стало, Бабочка-дружок, жилистого мяса дай, пою себе".</ta>
            <ta e="T374" id="Seg_10327" s="T360">"Э, ни одного жилистого мяса у меня нет, иди, иди", – сказал, обратно выгнал, ничего не дав.</ta>
            <ta e="T385" id="Seg_10328" s="T374">"Куда пойдёт, на улице ведь замёрзнет, куда, и домой не заведёт, позор.</ta>
            <ta e="T389" id="Seg_10329" s="T385">Вот уходит этот человек. </ta>
            <ta e="T404" id="Seg_10330" s="T389">Как пошёл еле-еле и потом приближаясь, возле дома, дойдя, смотрит, с оленёнком важенка ходит у его дома.</ta>
            <ta e="T416" id="Seg_10331" s="T404">Откуда пришедший – не знает оленя он .. олень ходит только, пришёл.</ta>
            <ta e="T431" id="Seg_10332" s="T416">Высоко он к смерти своей ум.. к оживанию времени только с оленёнком важенка к самому прийдя, облизывали его. </ta>
            <ta e="T445" id="Seg_10333" s="T431">Как помочился во внутрь пришёл да, оленёнка схватив, нож взяв, со звуком "корт", убил.</ta>
            <ta e="T447" id="Seg_10334" s="T445">Мать его ходит.</ta>
            <ta e="T460" id="Seg_10335" s="T447">Ну, этого что, разделал человек, сырым сварив, приготовив съел. </ta>
            <ta e="T464" id="Seg_10336" s="T460">Вот глаз его посветлел на то.</ta>
            <ta e="T475" id="Seg_10337" s="T464">"О, сейчас вот спасён, к Бабочке не пойду, не этоваю, еду имею ведь", – говорит. </ta>
            <ta e="T480" id="Seg_10338" s="T475">Еду имеющий человек сварив, кушает. </ta>
            <ta e="T481" id="Seg_10339" s="T480">Ок.</ta>
            <ta e="T486" id="Seg_10340" s="T481">Вот сколько дней прожил вот.</ta>
            <ta e="T491" id="Seg_10341" s="T486">С оленем, оленя своего поймал, оленёнка его. </ta>
            <ta e="T495" id="Seg_10342" s="T491">С оленёнком, что будет сейчас.</ta>
            <ta e="T501" id="Seg_10343" s="T495">Вот, когда лежал так, Бабочки голос пришёл (донёсся).</ta>
            <ta e="T504" id="Seg_10344" s="T501">Вот только посмотрел. </ta>
            <ta e="T515" id="Seg_10345" s="T504">"Вот я сейчас вот так сделаю, вот-вот придёт только, – сказал, – ко мне еда". </ta>
            <ta e="T521" id="Seg_10346" s="T515">Бабочка этовая еле идёт вовсе проголодавшись. </ta>
            <ta e="T524" id="Seg_10347" s="T521">Еле шагает.</ta>
            <ta e="T527" id="Seg_10348" s="T524">Придёл да пришёл. </ta>
            <ta e="T533" id="Seg_10349" s="T527">Как пришёл, запер, заперся человек.</ta>
            <ta e="T542" id="Seg_10350" s="T533">"Вот его накажу, тогда меня тоже он мучал вот так.</ta>
            <ta e="T546" id="Seg_10351" s="T542">"С Тросточкой старик, с Тросточкой старик,</ta>
            <ta e="T560" id="Seg_10352" s="T546">одно хоть жилистое мясо отдав, спаси, с Тросточкой старик, умер я, умер я</ta>
            <ta e="T571" id="Seg_10353" s="T560">остался в мой день стать да, если настоящий человек будешь ты, с Тросточкой старик, с Тросточкой старик", – пел лежал.</ta>
            <ta e="T574" id="Seg_10354" s="T571">Ничего не дал.</ta>
            <ta e="T596" id="Seg_10355" s="T574">Напрсно что-то с этого оленёнка копытце, копытца его этовал, … вместе копыто что-то, или что – то … находясь, то, бросив.</ta>
            <ta e="T603" id="Seg_10356" s="T596">"Иди, ешь", – говоря, два копытца бросил. </ta>
            <ta e="T615" id="Seg_10357" s="T603">"Иди домой, больше не приходи. Это тогда меня тоже сильно замучал". </ta>
            <ta e="T620" id="Seg_10358" s="T615">Тот что поделает, ушёл. </ta>
            <ta e="T624" id="Seg_10359" s="T620">Вот ушёл домой.</ta>
            <ta e="T639" id="Seg_10360" s="T624">Снова .. ушёл да на завтра несколько ночей как прошло, снова пришёл.</ta>
            <ta e="T647" id="Seg_10361" s="T639">Вот вовсе к смерти пошёл, кажется, друг его, Бабочка изнемог: </ta>
            <ta e="T652" id="Seg_10362" s="T647">когда прогоняет даже не уходит, когда зашёл, не приходит.</ta>
            <ta e="T666" id="Seg_10363" s="T652">О, вот ничего поделать не может. Вот сердце больное, болеет человек, друга пожалел ведь.</ta>
            <ta e="T680" id="Seg_10364" s="T666">Сжалившись, ничего сделать не может, и прогнать .. уйдя, всё равно домой не дойдёт.</ta>
            <ta e="T688" id="Seg_10365" s="T680">И тогда – "Заходи, – сказал, – поешь, – поговорим тогда хорошо".</ta>
            <ta e="T692" id="Seg_10366" s="T688">Вот вдоволь поговорили.</ta>
            <ta e="T699" id="Seg_10367" s="T692">"Вот, так мы хорошо поговорим, мы сейчас.</ta>
            <ta e="T707" id="Seg_10368" s="T699">Вместе жить будем ведь, почему в двух сторонах живём мы. </ta>
            <ta e="T711" id="Seg_10369" s="T707">Видишь, я оленя заимел ещё. </ta>
            <ta e="T716" id="Seg_10370" s="T711">Ты тогда меня пообижал..</ta>
            <ta e="T723" id="Seg_10371" s="T716">Что за оленя бог привёл вот, оленя покормил.</ta>
            <ta e="T730" id="Seg_10372" s="T723">Вот сейчас с оленем.., вместе поживём мы сейчас.</ta>
            <ta e="T739" id="Seg_10373" s="T730">Вместе живя, с одним советом охотиться давай и вместе будет. </ta>
            <ta e="T755" id="Seg_10374" s="T739">Твой дом перевезём, вот оленя имею ведь вот – один олень наш тянуть будет, что с этого будет-то.</ta>
            <ta e="T765" id="Seg_10375" s="T755">Приведём сюда твоего оленя,э, этого, здесь вместе, вместе поживём мы".</ta>
            <ta e="T770" id="Seg_10376" s="T765">Посоветовавшись вот вместе стали жить. </ta>
            <ta e="T782" id="Seg_10377" s="T770">Вдвоём сообща став, хитростью (мудростью) став – вместе вот в достатке зажили. Всё это.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T6" id="Seg_10378" s="T0">[DCh]: The name of the second old man is literally "with crutch"; due to better readability here is simply named "crutch" and "Krücke" in the translations.</ta>
            <ta e="T75" id="Seg_10379" s="T66">[DCh]: Not clear whose house is meant in the second clause; maybe wrong possessor here.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T783" />
            <conversion-tli id="T64" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T784" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T785" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T786" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T787" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T788" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T789" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T790" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T558" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T791" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T680" />
            <conversion-tli id="T792" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
