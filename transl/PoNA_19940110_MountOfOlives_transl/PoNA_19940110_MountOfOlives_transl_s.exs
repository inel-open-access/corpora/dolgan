<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID1198EBC8-D03E-0921-8CB6-96EE5FB61AB4">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>PoNA_1994_PrayerMountOfOlives_transl</transcription-name>
         <referenced-file url="PoNA_19940110_MountOfOlives_transl.wav" />
         <referenced-file url="PoNA_19940110_MountOfOlives_transl.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\transl\PoNA_19940110_MountOfOlives_transl\PoNA_19940110_MountOfOlives_transl.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">336</ud-information>
            <ud-information attribute-name="# HIAT:w">257</ud-information>
            <ud-information attribute-name="# e">257</ud-information>
            <ud-information attribute-name="# HIAT:u">37</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PoNA">
            <abbreviation>PoNA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="1.054000000000002" type="appl" />
         <tli id="T2" time="2.1069999999999993" type="appl" />
         <tli id="T3" time="3.1610000000000014" type="appl" />
         <tli id="T4" time="4.215000000000003" type="appl" />
         <tli id="T5" time="5.268000000000001" type="appl" />
         <tli id="T6" time="6.322000000000003" type="appl" />
         <tli id="T7" time="7.376000000000005" type="appl" />
         <tli id="T8" time="8.429000000000002" type="appl" />
         <tli id="T9" time="9.483000000000004" type="appl" />
         <tli id="T10" time="10.25399999999999" type="appl" />
         <tli id="T11" time="11.025000000000006" type="appl" />
         <tli id="T12" time="11.795999999999992" type="appl" />
         <tli id="T13" time="12.567000000000007" type="appl" />
         <tli id="T14" time="13.337999999999994" type="appl" />
         <tli id="T15" time="14.109000000000009" type="appl" />
         <tli id="T16" time="14.879999999999995" type="appl" />
         <tli id="T17" time="15.65100000000001" type="appl" />
         <tli id="T18" time="16.421999999999997" type="appl" />
         <tli id="T19" time="17.193000000000012" type="appl" />
         <tli id="T20" time="17.964" type="appl" />
         <tli id="T21" time="18.735000000000014" type="appl" />
         <tli id="T22" time="19.506" type="appl" />
         <tli id="T23" time="20.242999999999995" type="appl" />
         <tli id="T24" time="20.980999999999995" type="appl" />
         <tli id="T25" time="21.71799999999999" type="appl" />
         <tli id="T26" time="22.455000000000013" type="appl" />
         <tli id="T27" time="23.193000000000012" type="appl" />
         <tli id="T28" time="23.930000000000007" type="appl" />
         <tli id="T29" time="24.667" type="appl" />
         <tli id="T30" time="25.405" type="appl" />
         <tli id="T31" time="26.141999999999996" type="appl" />
         <tli id="T32" time="26.87899999999999" type="appl" />
         <tli id="T33" time="27.61699999999999" type="appl" />
         <tli id="T34" time="28.354000000000013" type="appl" />
         <tli id="T35" time="28.962999999999994" type="appl" />
         <tli id="T36" time="29.573000000000008" type="appl" />
         <tli id="T37" time="30.181999999999988" type="appl" />
         <tli id="T38" time="30.790999999999997" type="appl" />
         <tli id="T39" time="31.40100000000001" type="appl" />
         <tli id="T40" time="32.00999999999999" type="appl" />
         <tli id="T41" time="32.619" type="appl" />
         <tli id="T42" time="33.22800000000001" type="appl" />
         <tli id="T43" time="33.837999999999994" type="appl" />
         <tli id="T44" time="34.46" />
         <tli id="T45" time="36.73333333333333" />
         <tli id="T46" />
         <tli id="T47" />
         <tli id="T48" time="37.15899999999999" type="appl" />
         <tli id="T49" time="37.662000000000006" type="appl" />
         <tli id="T50" time="38.16399999999999" type="appl" />
         <tli id="T51" time="38.667" type="appl" />
         <tli id="T52" time="39.16999999999999" type="appl" />
         <tli id="T53" time="40.003000000000014" type="appl" />
         <tli id="T54" time="40.83500000000001" type="appl" />
         <tli id="T55" time="41.668000000000006" type="appl" />
         <tli id="T56" time="42.501000000000005" type="appl" />
         <tli id="T57" time="43.333" type="appl" />
         <tli id="T58" time="44.166" type="appl" />
         <tli id="T59" time="44.804" type="appl" />
         <tli id="T60" time="45.441" type="appl" />
         <tli id="T61" time="46.07900000000001" type="appl" />
         <tli id="T62" time="46.71700000000001" type="appl" />
         <tli id="T63" time="47.35499999999999" type="appl" />
         <tli id="T64" time="47.99199999999999" type="appl" />
         <tli id="T65" time="48.629999999999995" type="appl" />
         <tli id="T66" time="49.42599999999999" type="appl" />
         <tli id="T67" time="50.22300000000001" type="appl" />
         <tli id="T68" time="51.019000000000005" type="appl" />
         <tli id="T69" time="51.815" type="appl" />
         <tli id="T70" time="52.61099999999999" type="appl" />
         <tli id="T71" time="53.408000000000015" type="appl" />
         <tli id="T72" time="54.20400000000001" type="appl" />
         <tli id="T73" time="55.10499999999999" type="appl" />
         <tli id="T74" time="56.007000000000005" type="appl" />
         <tli id="T75" time="56.908000000000015" type="appl" />
         <tli id="T76" time="58.11333333333334" />
         <tli id="T77" time="62.37333333333333" />
         <tli id="T78" />
         <tli id="T79" />
         <tli id="T80" />
         <tli id="T81" />
         <tli id="T82" time="62.94399999999999" type="appl" />
         <tli id="T83" time="63.65700000000001" type="appl" />
         <tli id="T84" time="64.37" type="appl" />
         <tli id="T85" time="65.083" type="appl" />
         <tli id="T86" time="66.089" type="appl" />
         <tli id="T87" time="67.095" type="appl" />
         <tli id="T88" time="67.988" type="appl" />
         <tli id="T89" time="68.881" type="appl" />
         <tli id="T90" time="69.774" type="appl" />
         <tli id="T91" time="70.666" type="appl" />
         <tli id="T92" time="71.559" type="appl" />
         <tli id="T93" time="72.452" type="appl" />
         <tli id="T94" time="73.345" type="appl" />
         <tli id="T95" time="74.238" type="appl" />
         <tli id="T96" time="75.09200000000001" type="appl" />
         <tli id="T97" time="75.947" type="appl" />
         <tli id="T98" time="76.80099999999999" type="appl" />
         <tli id="T99" time="77.656" type="appl" />
         <tli id="T100" time="78.50999999999999" type="appl" />
         <tli id="T101" time="79.36500000000001" type="appl" />
         <tli id="T102" time="80.219" type="appl" />
         <tli id="T103" time="81.07400000000001" type="appl" />
         <tli id="T104" time="81.928" type="appl" />
         <tli id="T105" time="82.78300000000002" type="appl" />
         <tli id="T106" time="83.637" type="appl" />
         <tli id="T107" time="84.089" type="appl" />
         <tli id="T108" time="84.53999999999999" type="appl" />
         <tli id="T109" time="84.99199999999999" type="appl" />
         <tli id="T110" time="85.44399999999999" type="appl" />
         <tli id="T111" time="85.89600000000002" type="appl" />
         <tli id="T112" time="86.34700000000001" type="appl" />
         <tli id="T113" time="86.62" />
         <tli id="T114" time="87.37299999999999" type="appl" />
         <tli id="T115" time="87.94800000000001" type="appl" />
         <tli id="T116" time="88.52199999999999" type="appl" />
         <tli id="T117" time="89.096" type="appl" />
         <tli id="T118" time="89.977" type="appl" />
         <tli id="T119" time="90.858" type="appl" />
         <tli id="T120" time="91.74000000000001" type="appl" />
         <tli id="T121" time="92.62100000000001" type="appl" />
         <tli id="T122" time="93.50200000000001" type="appl" />
         <tli id="T123" time="94.13499999999999" type="appl" />
         <tli id="T124" time="94.768" type="appl" />
         <tli id="T125" time="95.4" type="appl" />
         <tli id="T126" time="96.03300000000002" type="appl" />
         <tli id="T127" time="96.882" type="appl" />
         <tli id="T128" time="97.731" type="appl" />
         <tli id="T129" time="98.57999999999998" type="appl" />
         <tli id="T130" time="99.428" type="appl" />
         <tli id="T131" time="100.27699999999999" type="appl" />
         <tli id="T132" time="101.12599999999998" type="appl" />
         <tli id="T133" time="101.97500000000002" type="appl" />
         <tli id="T134" time="102.786" type="appl" />
         <tli id="T135" time="103.59699999999998" type="appl" />
         <tli id="T136" time="104.40800000000002" type="appl" />
         <tli id="T137" time="105.219" type="appl" />
         <tli id="T138" time="106.031" type="appl" />
         <tli id="T139" time="106.84199999999998" type="appl" />
         <tli id="T140" time="107.65300000000002" type="appl" />
         <tli id="T141" time="108.464" type="appl" />
         <tli id="T142" time="109.27499999999998" type="appl" />
         <tli id="T143" time="109.94" type="appl" />
         <tli id="T144" time="110.60500000000002" type="appl" />
         <tli id="T145" time="111.26999999999998" type="appl" />
         <tli id="T146" time="111.935" type="appl" />
         <tli id="T147" time="112.601" type="appl" />
         <tli id="T148" time="113.26600000000002" type="appl" />
         <tli id="T149" time="113.93099999999998" type="appl" />
         <tli id="T150" time="114.596" type="appl" />
         <tli id="T151" time="115.26100000000002" type="appl" />
         <tli id="T152" time="116.22000000000003" type="appl" />
         <tli id="T153" time="117.178" type="appl" />
         <tli id="T154" time="118.36" />
         <tli id="T155" time="122.96666666666665" />
         <tli id="T156" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" time="124.195" type="appl" />
         <tli id="T165" time="125.33999999999997" type="appl" />
         <tli id="T166" time="126.46100000000001" type="appl" />
         <tli id="T167" time="127.58300000000003" type="appl" />
         <tli id="T168" time="128.704" type="appl" />
         <tli id="T169" time="129.825" type="appl" />
         <tli id="T170" time="130.94600000000003" type="appl" />
         <tli id="T171" time="132.06799999999998" type="appl" />
         <tli id="T172" time="133.18900000000002" type="appl" />
         <tli id="T173" time="134.31" type="appl" />
         <tli id="T174" time="135.43200000000002" type="appl" />
         <tli id="T175" time="136.553" type="appl" />
         <tli id="T176" time="137.24599999999998" type="appl" />
         <tli id="T177" time="137.938" type="appl" />
         <tli id="T178" time="138.63100000000003" type="appl" />
         <tli id="T179" time="139.324" type="appl" />
         <tli id="T180" time="140.017" type="appl" />
         <tli id="T181" time="140.709" type="appl" />
         <tli id="T182" time="141.402" type="appl" />
         <tli id="T183" time="142.05200000000002" type="appl" />
         <tli id="T184" time="142.70299999999997" type="appl" />
         <tli id="T185" time="143.353" type="appl" />
         <tli id="T186" time="144.003" type="appl" />
         <tli id="T187" time="144.65300000000002" type="appl" />
         <tli id="T188" time="145.30399999999997" type="appl" />
         <tli id="T189" time="145.954" type="appl" />
         <tli id="T190" time="146.60399999999998" type="appl" />
         <tli id="T191" time="147.255" type="appl" />
         <tli id="T192" time="147.90500000000003" type="appl" />
         <tli id="T193" time="148.555" type="appl" />
         <tli id="T194" time="149.20499999999998" type="appl" />
         <tli id="T195" time="149.856" type="appl" />
         <tli id="T196" time="150.50600000000003" type="appl" />
         <tli id="T197" time="151.2" type="appl" />
         <tli id="T198" time="151.894" type="appl" />
         <tli id="T199" time="152.58800000000002" type="appl" />
         <tli id="T200" time="153.28300000000002" type="appl" />
         <tli id="T201" time="153.97699999999998" type="appl" />
         <tli id="T202" time="154.671" type="appl" />
         <tli id="T203" time="155.365" type="appl" />
         <tli id="T204" time="156.05900000000003" type="appl" />
         <tli id="T205" time="156.753" type="appl" />
         <tli id="T206" time="157.447" type="appl" />
         <tli id="T207" time="158.14100000000002" type="appl" />
         <tli id="T208" time="158.836" type="appl" />
         <tli id="T209" time="159.53000000000003" type="appl" />
         <tli id="T210" time="160.224" type="appl" />
         <tli id="T211" time="160.918" type="appl" />
         <tli id="T212" time="161.548" type="appl" />
         <tli id="T213" time="162.17700000000002" type="appl" />
         <tli id="T214" time="162.80700000000002" type="appl" />
         <tli id="T215" time="163.43599999999998" type="appl" />
         <tli id="T216" time="164.06599999999997" type="appl" />
         <tli id="T217" time="164.69600000000003" type="appl" />
         <tli id="T218" time="165.325" type="appl" />
         <tli id="T219" time="165.95499999999998" type="appl" />
         <tli id="T220" time="166.584" type="appl" />
         <tli id="T221" time="167.214" type="appl" />
         <tli id="T222" time="168.053" type="appl" />
         <tli id="T223" time="168.892" type="appl" />
         <tli id="T224" time="169.73000000000002" type="appl" />
         <tli id="T225" time="170.56900000000002" type="appl" />
         <tli id="T226" time="171.33800000000002" type="appl" />
         <tli id="T227" time="172.108" type="appl" />
         <tli id="T228" time="172.878" type="appl" />
         <tli id="T229" time="173.647" type="appl" />
         <tli id="T230" time="174.416" type="appl" />
         <tli id="T231" time="175.18599999999998" type="appl" />
         <tli id="T232" time="176.574" type="appl" />
         <tli id="T233" time="177.961" type="appl" />
         <tli id="T234" time="178.51100000000002" type="appl" />
         <tli id="T235" time="179.06" type="appl" />
         <tli id="T236" time="179.61" type="appl" />
         <tli id="T237" time="180.61200000000002" type="appl" />
         <tli id="T238" time="181.615" type="appl" />
         <tli id="T239" time="182.61700000000002" type="appl" />
         <tli id="T240" time="183.61900000000003" type="appl" />
         <tli id="T241" time="184.62099999999998" type="appl" />
         <tli id="T242" time="185.62400000000002" type="appl" />
         <tli id="T243" time="186.62599999999998" type="appl" />
         <tli id="T244" time="187.628" type="appl" />
         <tli id="T245" time="188.63" type="appl" />
         <tli id="T246" time="189.63299999999998" type="appl" />
         <tli id="T247" time="190.635" type="appl" />
         <tli id="T248" time="191.409" type="appl" />
         <tli id="T249" time="192.183" type="appl" />
         <tli id="T250" time="192.957" type="appl" />
         <tli id="T251" time="193.731" type="appl" />
         <tli id="T252" time="194.50600000000003" type="appl" />
         <tli id="T253" time="195.28000000000003" type="appl" />
         <tli id="T254" time="196.05399999999997" type="appl" />
         <tli id="T255" time="196.82799999999997" type="appl" />
         <tli id="T256" time="197.60199999999998" type="appl" />
         <tli id="T257" time="198.89300000000003" type="appl" />
         <tli id="T258" time="200.185" type="appl" />
         <tli id="T259" time="201.476" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PoNA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T259" id="Seg_0" n="sc" s="T0">
               <ts e="T9" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Nöŋü͡ö</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">öspüt</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">bu</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">aːta</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">bu͡olar</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_18" n="HIAT:ip">"</nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">Gʼipsʼemanʼiːja</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">olokko</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">malʼitva</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_29" n="HIAT:w" s="T8">aːgar</ts>
                  <nts id="Seg_30" n="HIAT:ip">.</nts>
                  <nts id="Seg_31" n="HIAT:ip">"</nts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T22" id="Seg_34" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_36" n="HIAT:w" s="T9">Hol</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_39" n="HIAT:w" s="T10">tüːn</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_42" n="HIAT:w" s="T11">ki͡ehe</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_45" n="HIAT:w" s="T12">körsüː</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_48" n="HIAT:w" s="T13">kennitiger</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_51" n="HIAT:w" s="T14">Isuːs</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_54" n="HIAT:w" s="T15">u͡on</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_57" n="HIAT:w" s="T16">biːr</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_60" n="HIAT:w" s="T17">ü͡öreter</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_63" n="HIAT:w" s="T18">kihilerin</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_66" n="HIAT:w" s="T19">gɨtta</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_69" n="HIAT:w" s="T20">taksɨbɨt</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_72" n="HIAT:w" s="T21">Irusalʼimtan</ts>
                  <nts id="Seg_73" n="HIAT:ip">.</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_76" n="HIAT:u" s="T22">
                  <ts e="T23" id="Seg_78" n="HIAT:w" s="T22">Kidron</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_81" n="HIAT:w" s="T23">aːttaːk</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_84" n="HIAT:w" s="T24">kuruluː</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_87" n="HIAT:w" s="T25">turar</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_90" n="HIAT:w" s="T26">uːnu</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_93" n="HIAT:w" s="T27">taksan</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_96" n="HIAT:w" s="T28">kelbit</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_99" n="HIAT:w" s="T29">Irusalʼim</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_102" n="HIAT:w" s="T30">attɨgar</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_105" n="HIAT:w" s="T31">baːr</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_108" n="HIAT:w" s="T32">Ipsomanʼiːja</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_111" n="HIAT:w" s="T33">olokko</ts>
                  <nts id="Seg_112" n="HIAT:ip">.</nts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T44" id="Seg_115" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_117" n="HIAT:w" s="T34">Onno</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_120" n="HIAT:w" s="T35">Isuːs</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_123" n="HIAT:w" s="T36">ü͡öreter</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_126" n="HIAT:w" s="T37">dʼonun</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_129" n="HIAT:w" s="T38">gɨtta</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_132" n="HIAT:w" s="T39">teŋke</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_135" n="HIAT:w" s="T40">maska</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_138" n="HIAT:w" s="T41">kiːren</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_141" n="HIAT:w" s="T42">di͡ebit</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_144" n="HIAT:w" s="T43">ginilerge</ts>
                  <nts id="Seg_145" n="HIAT:ip">:</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_148" n="HIAT:u" s="T44">
                  <nts id="Seg_149" n="HIAT:ip">"</nts>
                  <ts e="T45" id="Seg_151" n="HIAT:w" s="T44">Oloro</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_154" n="HIAT:w" s="T45">tühüŋ</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_157" n="HIAT:w" s="T46">manna</ts>
                  <nts id="Seg_158" n="HIAT:ip">.</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_161" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_163" n="HIAT:w" s="T47">Min</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_166" n="HIAT:w" s="T48">bara</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_169" n="HIAT:w" s="T49">tühü͡öm</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_172" n="HIAT:w" s="T50">taŋaraga</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_175" n="HIAT:w" s="T51">üŋe</ts>
                  <nts id="Seg_176" n="HIAT:ip">"</nts>
                  <nts id="Seg_177" n="HIAT:ip">.</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T58" id="Seg_180" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_182" n="HIAT:w" s="T52">Bejetin</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_185" n="HIAT:w" s="T53">gɨtta</ts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_188" n="HIAT:w" s="T54">ɨlbɨt</ts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_191" n="HIAT:w" s="T55">Pü͡öturu</ts>
                  <nts id="Seg_192" n="HIAT:ip">,</nts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_195" n="HIAT:w" s="T56">Dʼaːkabɨ</ts>
                  <nts id="Seg_196" n="HIAT:ip">,</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_199" n="HIAT:w" s="T57">Ujbaːnɨ</ts>
                  <nts id="Seg_200" n="HIAT:ip">.</nts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_203" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_205" n="HIAT:w" s="T58">Gini</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_208" n="HIAT:w" s="T59">togo</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_211" n="HIAT:w" s="T60">ire</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_214" n="HIAT:w" s="T61">hanaːrgɨːr</ts>
                  <nts id="Seg_215" n="HIAT:ip">,</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_218" n="HIAT:w" s="T62">čüŋküjer</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_221" n="HIAT:w" s="T63">kördük</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_224" n="HIAT:w" s="T64">ebit</ts>
                  <nts id="Seg_225" n="HIAT:ip">.</nts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_228" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_230" n="HIAT:w" s="T65">Bihigi</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_233" n="HIAT:w" s="T66">anʼɨːlarɨ</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_236" n="HIAT:w" s="T67">oŋorbupput</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_239" n="HIAT:w" s="T68">ulakannɨk</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_242" n="HIAT:w" s="T69">battɨːr</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_245" n="HIAT:w" s="T70">ebit</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_248" n="HIAT:w" s="T71">ginini</ts>
                  <nts id="Seg_249" n="HIAT:ip">.</nts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_252" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_254" n="HIAT:w" s="T72">Isuːs</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_257" n="HIAT:w" s="T73">di͡ebit</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_260" n="HIAT:w" s="T74">ü͡öreter</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_263" n="HIAT:w" s="T75">dʼonugar</ts>
                  <nts id="Seg_264" n="HIAT:ip">:</nts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_267" n="HIAT:u" s="T76">
                  <nts id="Seg_268" n="HIAT:ip">"</nts>
                  <ts e="T77" id="Seg_270" n="HIAT:w" s="T76">Min</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_273" n="HIAT:w" s="T77">kutum</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_276" n="HIAT:w" s="T78">ölör</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_279" n="HIAT:w" s="T79">kördük</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_282" n="HIAT:w" s="T80">hanaːrgaːta</ts>
                  <nts id="Seg_283" n="HIAT:ip">.</nts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_286" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_288" n="HIAT:w" s="T81">Bu͡ola</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_291" n="HIAT:w" s="T82">tühüŋ</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_294" n="HIAT:w" s="T83">minigin</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_297" n="HIAT:w" s="T84">gɨtta</ts>
                  <nts id="Seg_298" n="HIAT:ip">.</nts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_301" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_303" n="HIAT:w" s="T85">Ideliː</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_306" n="HIAT:w" s="T86">oloru͡oguŋ</ts>
                  <nts id="Seg_307" n="HIAT:ip">"</nts>
                  <nts id="Seg_308" n="HIAT:ip">.</nts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_311" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_313" n="HIAT:w" s="T87">Ginnerten</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_316" n="HIAT:w" s="T88">araga</ts>
                  <nts id="Seg_317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_319" n="HIAT:w" s="T89">tüheːt</ts>
                  <nts id="Seg_320" n="HIAT:ip">,</nts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_323" n="HIAT:w" s="T90">giniː</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_326" n="HIAT:w" s="T91">tobuktuː</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_329" n="HIAT:w" s="T92">oloron</ts>
                  <nts id="Seg_330" n="HIAT:ip">,</nts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_333" n="HIAT:w" s="T93">üŋpüt</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_336" n="HIAT:w" s="T94">taŋaraga</ts>
                  <nts id="Seg_337" n="HIAT:ip">.</nts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_340" n="HIAT:u" s="T95">
                  <nts id="Seg_341" n="HIAT:ip">"</nts>
                  <ts e="T96" id="Seg_343" n="HIAT:w" s="T95">Aga</ts>
                  <nts id="Seg_344" n="HIAT:ip">,</nts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_347" n="HIAT:w" s="T96">oː</ts>
                  <nts id="Seg_348" n="HIAT:ip">,</nts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_351" n="HIAT:w" s="T97">en</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_354" n="HIAT:w" s="T98">höbülüːrüŋ</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_357" n="HIAT:w" s="T99">ebite</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_360" n="HIAT:w" s="T100">bu͡ollar</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_363" n="HIAT:w" s="T101">ɨːtaːrɨ</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_366" n="HIAT:w" s="T102">minigin</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_369" n="HIAT:w" s="T103">hɨːha</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_372" n="HIAT:w" s="T104">bu</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_375" n="HIAT:w" s="T105">čaːskɨnɨ</ts>
                  <nts id="Seg_376" n="HIAT:ip">.</nts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_379" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_381" n="HIAT:w" s="T106">Iti</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_384" n="HIAT:w" s="T107">hin</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_387" n="HIAT:w" s="T108">bu͡olbat</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_390" n="HIAT:w" s="T109">min</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_392" n="HIAT:ip">(</nts>
                  <ts e="T112" id="Seg_394" n="HIAT:w" s="T110">han-</ts>
                  <nts id="Seg_395" n="HIAT:ip">)</nts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_398" n="HIAT:w" s="T112">hanaːbɨnan</ts>
                  <nts id="Seg_399" n="HIAT:ip">.</nts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T117" id="Seg_402" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_404" n="HIAT:w" s="T113">Bu͡ollun</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_407" n="HIAT:w" s="T114">en</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_410" n="HIAT:w" s="T115">agaj</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_413" n="HIAT:w" s="T116">hanaːgɨnan</ts>
                  <nts id="Seg_414" n="HIAT:ip">.</nts>
                  <nts id="Seg_415" n="HIAT:ip">"</nts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_418" n="HIAT:u" s="T117">
                  <ts e="T118" id="Seg_420" n="HIAT:w" s="T117">Kallaːnnartan</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_423" n="HIAT:w" s="T118">köstön</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_426" n="HIAT:w" s="T119">kelbit</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_429" n="HIAT:w" s="T120">gini͡eke</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_432" n="HIAT:w" s="T121">angʼel</ts>
                  <nts id="Seg_433" n="HIAT:ip">.</nts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_436" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_438" n="HIAT:w" s="T122">Gini</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_441" n="HIAT:w" s="T123">hanaːtɨn</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_444" n="HIAT:w" s="T124">bigergeter</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_447" n="HIAT:w" s="T125">ebit</ts>
                  <nts id="Seg_448" n="HIAT:ip">.</nts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_451" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_453" n="HIAT:w" s="T126">Isuːs</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_456" n="HIAT:w" s="T127">olus</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_459" n="HIAT:w" s="T128">erejdener</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_462" n="HIAT:w" s="T129">kördük</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_465" n="HIAT:w" s="T130">eletinen</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_468" n="HIAT:w" s="T131">üŋer</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_471" n="HIAT:w" s="T132">bu͡olbut</ts>
                  <nts id="Seg_472" n="HIAT:ip">.</nts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T142" id="Seg_475" n="HIAT:u" s="T133">
                  <ts e="T134" id="Seg_477" n="HIAT:w" s="T133">Gini</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_480" n="HIAT:w" s="T134">kölöhüne</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_483" n="HIAT:w" s="T135">agaj</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_485" n="HIAT:ip">(</nts>
                  <ts e="T138" id="Seg_487" n="HIAT:w" s="T136">kür-</ts>
                  <nts id="Seg_488" n="HIAT:ip">)</nts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_491" n="HIAT:w" s="T138">hüːrer</ts>
                  <nts id="Seg_492" n="HIAT:ip">,</nts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_495" n="HIAT:w" s="T139">kaːnnɨː</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_498" n="HIAT:w" s="T140">hirge</ts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_501" n="HIAT:w" s="T141">toktor</ts>
                  <nts id="Seg_502" n="HIAT:ip">.</nts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T151" id="Seg_505" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_507" n="HIAT:w" s="T142">Ü͡öreter</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_510" n="HIAT:w" s="T143">dʼonugar</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_513" n="HIAT:w" s="T144">tönnön</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_516" n="HIAT:w" s="T145">kelen</ts>
                  <nts id="Seg_517" n="HIAT:ip">,</nts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_520" n="HIAT:w" s="T146">gini</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_523" n="HIAT:w" s="T147">bulbut</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_526" n="HIAT:w" s="T148">ontulara</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_529" n="HIAT:w" s="T149">utuja</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_532" n="HIAT:w" s="T150">hɨtallar</ts>
                  <nts id="Seg_533" n="HIAT:ip">.</nts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_536" n="HIAT:u" s="T151">
                  <ts e="T152" id="Seg_538" n="HIAT:w" s="T151">Oloru</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_541" n="HIAT:w" s="T152">uhugunnaraːt</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_544" n="HIAT:w" s="T153">haŋarbɨt</ts>
                  <nts id="Seg_545" n="HIAT:ip">:</nts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T163" id="Seg_548" n="HIAT:u" s="T154">
                  <nts id="Seg_549" n="HIAT:ip">"</nts>
                  <ts e="T155" id="Seg_551" n="HIAT:w" s="T154">Kajtak</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_554" n="HIAT:w" s="T155">ehigi</ts>
                  <nts id="Seg_555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_557" n="HIAT:w" s="T156">biːr</ts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_560" n="HIAT:w" s="T157">čaːs</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_563" n="HIAT:w" s="T158">kuraŋɨgar</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_566" n="HIAT:w" s="T159">olorbot</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_569" n="HIAT:w" s="T160">bu͡ollugut</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_572" n="HIAT:w" s="T161">minigin</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_575" n="HIAT:w" s="T162">gɨtta</ts>
                  <nts id="Seg_576" n="HIAT:ip">?</nts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T165" id="Seg_579" n="HIAT:u" s="T163">
                  <ts e="T164" id="Seg_581" n="HIAT:w" s="T163">Oloruŋ</ts>
                  <nts id="Seg_582" n="HIAT:ip">,</nts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_585" n="HIAT:w" s="T164">üŋüŋ</ts>
                  <nts id="Seg_586" n="HIAT:ip">.</nts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T175" id="Seg_589" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_591" n="HIAT:w" s="T165">Kuhagan</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_594" n="HIAT:w" s="T166">di͡ek</ts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_597" n="HIAT:w" s="T167">tartarɨmɨ͡akkɨtɨn</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_600" n="HIAT:w" s="T168">ajɨː</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_603" n="HIAT:w" s="T169">kamnas</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_606" n="HIAT:w" s="T170">bu͡olaːččɨ</ts>
                  <nts id="Seg_607" n="HIAT:ip">,</nts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_610" n="HIAT:w" s="T171">et</ts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_613" n="HIAT:w" s="T172">bu͡ollagɨna</ts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_616" n="HIAT:w" s="T173">küːhe</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_619" n="HIAT:w" s="T174">hu͡ok</ts>
                  <nts id="Seg_620" n="HIAT:ip">.</nts>
                  <nts id="Seg_621" n="HIAT:ip">"</nts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T182" id="Seg_624" n="HIAT:u" s="T175">
                  <ts e="T176" id="Seg_626" n="HIAT:w" s="T175">Ginilerten</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_629" n="HIAT:w" s="T176">aragan</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_632" n="HIAT:w" s="T177">baraːn</ts>
                  <nts id="Seg_633" n="HIAT:ip">,</nts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_636" n="HIAT:w" s="T178">gini</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_639" n="HIAT:w" s="T179">emi͡e</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_642" n="HIAT:w" s="T180">üŋpüt</ts>
                  <nts id="Seg_643" n="HIAT:ip">,</nts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_646" n="HIAT:w" s="T181">haŋaran</ts>
                  <nts id="Seg_647" n="HIAT:ip">:</nts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_650" n="HIAT:u" s="T182">
                  <nts id="Seg_651" n="HIAT:ip">"</nts>
                  <ts e="T183" id="Seg_653" n="HIAT:w" s="T182">Min</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_656" n="HIAT:w" s="T183">agam</ts>
                  <nts id="Seg_657" n="HIAT:ip">,</nts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_660" n="HIAT:w" s="T184">iti</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_663" n="HIAT:w" s="T185">čaːskɨ</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_666" n="HIAT:w" s="T186">min</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_669" n="HIAT:w" s="T187">attɨbɨnan</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_672" n="HIAT:w" s="T188">aːhar</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_675" n="HIAT:w" s="T189">bu͡ollagɨna</ts>
                  <nts id="Seg_676" n="HIAT:ip">,</nts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_679" n="HIAT:w" s="T190">min</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_682" n="HIAT:w" s="T191">ihi͡ekpin</ts>
                  <nts id="Seg_683" n="HIAT:ip">,</nts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_686" n="HIAT:w" s="T192">bu͡ollun</ts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_689" n="HIAT:w" s="T193">en</ts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_692" n="HIAT:w" s="T194">hanaːŋ</ts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_695" n="HIAT:w" s="T195">kördük</ts>
                  <nts id="Seg_696" n="HIAT:ip">.</nts>
                  <nts id="Seg_697" n="HIAT:ip">"</nts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T211" id="Seg_700" n="HIAT:u" s="T196">
                  <ts e="T197" id="Seg_702" n="HIAT:w" s="T196">Bu</ts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_705" n="HIAT:w" s="T197">malʼitvaga</ts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_708" n="HIAT:w" s="T198">Krʼistos</ts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_711" n="HIAT:w" s="T199">haŋarar</ts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_714" n="HIAT:w" s="T200">erejdener</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_717" n="HIAT:w" s="T201">čaːskɨ</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_720" n="HIAT:w" s="T202">tuhunan</ts>
                  <nts id="Seg_721" n="HIAT:ip">,</nts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_724" n="HIAT:w" s="T203">tugu</ts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_727" n="HIAT:w" s="T204">gini</ts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_730" n="HIAT:w" s="T205">ihi͡ek</ts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_733" n="HIAT:w" s="T206">tustaːk</ts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_736" n="HIAT:w" s="T207">bihigi</ts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_739" n="HIAT:w" s="T208">anʼɨːlarɨ</ts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_742" n="HIAT:w" s="T209">oŋorbupput</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_745" n="HIAT:w" s="T210">ihin</ts>
                  <nts id="Seg_746" n="HIAT:ip">.</nts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T221" id="Seg_749" n="HIAT:u" s="T211">
                  <ts e="T212" id="Seg_751" n="HIAT:w" s="T211">Kim</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_754" n="HIAT:w" s="T212">da</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_757" n="HIAT:w" s="T213">bilbet</ts>
                  <nts id="Seg_758" n="HIAT:ip">,</nts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_761" n="HIAT:w" s="T214">kihi</ts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_764" n="HIAT:w" s="T215">hanaːta</ts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_767" n="HIAT:w" s="T216">da</ts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_770" n="HIAT:w" s="T217">koppot</ts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_773" n="HIAT:w" s="T218">ol</ts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_776" n="HIAT:w" s="T219">erejdener</ts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_779" n="HIAT:w" s="T220">kɨtaːnagɨn</ts>
                  <nts id="Seg_780" n="HIAT:ip">.</nts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T225" id="Seg_783" n="HIAT:u" s="T221">
                  <ts e="T222" id="Seg_785" n="HIAT:w" s="T221">Taŋara</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_788" n="HIAT:w" s="T222">u͡ola</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_791" n="HIAT:w" s="T223">itini</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_794" n="HIAT:w" s="T224">ɨlsɨbɨta</ts>
                  <nts id="Seg_795" n="HIAT:ip">.</nts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T231" id="Seg_798" n="HIAT:u" s="T225">
                  <ts e="T226" id="Seg_800" n="HIAT:w" s="T225">Gini</ts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_803" n="HIAT:w" s="T226">biler</ts>
                  <nts id="Seg_804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_806" n="HIAT:w" s="T227">ete</ts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_808" n="HIAT:ip">–</nts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_811" n="HIAT:w" s="T228">bejetin</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_814" n="HIAT:w" s="T229">tɨːnɨn</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_817" n="HIAT:w" s="T230">bi͡erer</ts>
                  <nts id="Seg_818" n="HIAT:ip">.</nts>
                  <nts id="Seg_819" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T233" id="Seg_821" n="HIAT:u" s="T231">
                  <ts e="T232" id="Seg_823" n="HIAT:w" s="T231">Bihigini</ts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_826" n="HIAT:w" s="T232">bɨːhaːrɨ</ts>
                  <nts id="Seg_827" n="HIAT:ip">.</nts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T236" id="Seg_830" n="HIAT:u" s="T233">
                  <ts e="T234" id="Seg_832" n="HIAT:w" s="T233">Kim</ts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_835" n="HIAT:w" s="T234">da</ts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_838" n="HIAT:w" s="T235">bu͡ollun</ts>
                  <nts id="Seg_839" n="HIAT:ip">.</nts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T247" id="Seg_842" n="HIAT:u" s="T236">
                  <ts e="T237" id="Seg_844" n="HIAT:w" s="T236">Gini</ts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_847" n="HIAT:w" s="T237">ispetege</ts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_850" n="HIAT:w" s="T238">bu͡ollar</ts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_853" n="HIAT:w" s="T239">ol</ts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_856" n="HIAT:w" s="T240">čaːskɨttan</ts>
                  <nts id="Seg_857" n="HIAT:ip">,</nts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_860" n="HIAT:w" s="T241">bihigi</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_863" n="HIAT:w" s="T242">anʼɨːlarɨ</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_866" n="HIAT:w" s="T243">oŋorbupput</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_869" n="HIAT:w" s="T244">bi͡ek</ts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_872" n="HIAT:w" s="T245">kaːlar</ts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_875" n="HIAT:w" s="T246">etilere</ts>
                  <nts id="Seg_876" n="HIAT:ip">.</nts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T256" id="Seg_879" n="HIAT:u" s="T247">
                  <ts e="T248" id="Seg_881" n="HIAT:w" s="T247">Ehigi</ts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_884" n="HIAT:w" s="T248">istibikkit</ts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_887" n="HIAT:w" s="T249">östörü</ts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_889" n="HIAT:ip">"</nts>
                  <ts e="T251" id="Seg_891" n="HIAT:w" s="T250">Isuːs</ts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_894" n="HIAT:w" s="T251">Xrʼistos</ts>
                  <nts id="Seg_895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_896" n="HIAT:ip">–</nts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_899" n="HIAT:w" s="T252">ogolor</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_902" n="HIAT:w" s="T253">dogottoro</ts>
                  <nts id="Seg_903" n="HIAT:ip">"</nts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_906" n="HIAT:w" s="T254">di͡en</ts>
                  <nts id="Seg_907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_909" n="HIAT:w" s="T255">kinigetten</ts>
                  <nts id="Seg_910" n="HIAT:ip">.</nts>
                  <nts id="Seg_911" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T259" id="Seg_913" n="HIAT:u" s="T256">
                  <ts e="T257" id="Seg_915" n="HIAT:w" s="T256">Aːkpɨta</ts>
                  <nts id="Seg_916" n="HIAT:ip">,</nts>
                  <nts id="Seg_917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_919" n="HIAT:w" s="T257">tulmaːstaːbɨta</ts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_922" n="HIAT:w" s="T258">Papov</ts>
                  <nts id="Seg_923" n="HIAT:ip">.</nts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T259" id="Seg_925" n="sc" s="T0">
               <ts e="T1" id="Seg_927" n="e" s="T0">Nöŋü͡ö </ts>
               <ts e="T2" id="Seg_929" n="e" s="T1">öspüt </ts>
               <ts e="T3" id="Seg_931" n="e" s="T2">bu </ts>
               <ts e="T4" id="Seg_933" n="e" s="T3">aːta </ts>
               <ts e="T5" id="Seg_935" n="e" s="T4">bu͡olar </ts>
               <ts e="T6" id="Seg_937" n="e" s="T5">"Gʼipsʼemanʼiːja </ts>
               <ts e="T7" id="Seg_939" n="e" s="T6">olokko </ts>
               <ts e="T8" id="Seg_941" n="e" s="T7">malʼitva </ts>
               <ts e="T9" id="Seg_943" n="e" s="T8">aːgar." </ts>
               <ts e="T10" id="Seg_945" n="e" s="T9">Hol </ts>
               <ts e="T11" id="Seg_947" n="e" s="T10">tüːn </ts>
               <ts e="T12" id="Seg_949" n="e" s="T11">ki͡ehe </ts>
               <ts e="T13" id="Seg_951" n="e" s="T12">körsüː </ts>
               <ts e="T14" id="Seg_953" n="e" s="T13">kennitiger </ts>
               <ts e="T15" id="Seg_955" n="e" s="T14">Isuːs </ts>
               <ts e="T16" id="Seg_957" n="e" s="T15">u͡on </ts>
               <ts e="T17" id="Seg_959" n="e" s="T16">biːr </ts>
               <ts e="T18" id="Seg_961" n="e" s="T17">ü͡öreter </ts>
               <ts e="T19" id="Seg_963" n="e" s="T18">kihilerin </ts>
               <ts e="T20" id="Seg_965" n="e" s="T19">gɨtta </ts>
               <ts e="T21" id="Seg_967" n="e" s="T20">taksɨbɨt </ts>
               <ts e="T22" id="Seg_969" n="e" s="T21">Irusalʼimtan. </ts>
               <ts e="T23" id="Seg_971" n="e" s="T22">Kidron </ts>
               <ts e="T24" id="Seg_973" n="e" s="T23">aːttaːk </ts>
               <ts e="T25" id="Seg_975" n="e" s="T24">kuruluː </ts>
               <ts e="T26" id="Seg_977" n="e" s="T25">turar </ts>
               <ts e="T27" id="Seg_979" n="e" s="T26">uːnu </ts>
               <ts e="T28" id="Seg_981" n="e" s="T27">taksan </ts>
               <ts e="T29" id="Seg_983" n="e" s="T28">kelbit </ts>
               <ts e="T30" id="Seg_985" n="e" s="T29">Irusalʼim </ts>
               <ts e="T31" id="Seg_987" n="e" s="T30">attɨgar </ts>
               <ts e="T32" id="Seg_989" n="e" s="T31">baːr </ts>
               <ts e="T33" id="Seg_991" n="e" s="T32">Ipsomanʼiːja </ts>
               <ts e="T34" id="Seg_993" n="e" s="T33">olokko. </ts>
               <ts e="T35" id="Seg_995" n="e" s="T34">Onno </ts>
               <ts e="T36" id="Seg_997" n="e" s="T35">Isuːs </ts>
               <ts e="T37" id="Seg_999" n="e" s="T36">ü͡öreter </ts>
               <ts e="T38" id="Seg_1001" n="e" s="T37">dʼonun </ts>
               <ts e="T39" id="Seg_1003" n="e" s="T38">gɨtta </ts>
               <ts e="T40" id="Seg_1005" n="e" s="T39">teŋke </ts>
               <ts e="T41" id="Seg_1007" n="e" s="T40">maska </ts>
               <ts e="T42" id="Seg_1009" n="e" s="T41">kiːren </ts>
               <ts e="T43" id="Seg_1011" n="e" s="T42">di͡ebit </ts>
               <ts e="T44" id="Seg_1013" n="e" s="T43">ginilerge: </ts>
               <ts e="T45" id="Seg_1015" n="e" s="T44">"Oloro </ts>
               <ts e="T46" id="Seg_1017" n="e" s="T45">tühüŋ </ts>
               <ts e="T47" id="Seg_1019" n="e" s="T46">manna. </ts>
               <ts e="T48" id="Seg_1021" n="e" s="T47">Min </ts>
               <ts e="T49" id="Seg_1023" n="e" s="T48">bara </ts>
               <ts e="T50" id="Seg_1025" n="e" s="T49">tühü͡öm </ts>
               <ts e="T51" id="Seg_1027" n="e" s="T50">taŋaraga </ts>
               <ts e="T52" id="Seg_1029" n="e" s="T51">üŋe". </ts>
               <ts e="T53" id="Seg_1031" n="e" s="T52">Bejetin </ts>
               <ts e="T54" id="Seg_1033" n="e" s="T53">gɨtta </ts>
               <ts e="T55" id="Seg_1035" n="e" s="T54">ɨlbɨt </ts>
               <ts e="T56" id="Seg_1037" n="e" s="T55">Pü͡öturu, </ts>
               <ts e="T57" id="Seg_1039" n="e" s="T56">Dʼaːkabɨ, </ts>
               <ts e="T58" id="Seg_1041" n="e" s="T57">Ujbaːnɨ. </ts>
               <ts e="T59" id="Seg_1043" n="e" s="T58">Gini </ts>
               <ts e="T60" id="Seg_1045" n="e" s="T59">togo </ts>
               <ts e="T61" id="Seg_1047" n="e" s="T60">ire </ts>
               <ts e="T62" id="Seg_1049" n="e" s="T61">hanaːrgɨːr, </ts>
               <ts e="T63" id="Seg_1051" n="e" s="T62">čüŋküjer </ts>
               <ts e="T64" id="Seg_1053" n="e" s="T63">kördük </ts>
               <ts e="T65" id="Seg_1055" n="e" s="T64">ebit. </ts>
               <ts e="T66" id="Seg_1057" n="e" s="T65">Bihigi </ts>
               <ts e="T67" id="Seg_1059" n="e" s="T66">anʼɨːlarɨ </ts>
               <ts e="T68" id="Seg_1061" n="e" s="T67">oŋorbupput </ts>
               <ts e="T69" id="Seg_1063" n="e" s="T68">ulakannɨk </ts>
               <ts e="T70" id="Seg_1065" n="e" s="T69">battɨːr </ts>
               <ts e="T71" id="Seg_1067" n="e" s="T70">ebit </ts>
               <ts e="T72" id="Seg_1069" n="e" s="T71">ginini. </ts>
               <ts e="T73" id="Seg_1071" n="e" s="T72">Isuːs </ts>
               <ts e="T74" id="Seg_1073" n="e" s="T73">di͡ebit </ts>
               <ts e="T75" id="Seg_1075" n="e" s="T74">ü͡öreter </ts>
               <ts e="T76" id="Seg_1077" n="e" s="T75">dʼonugar: </ts>
               <ts e="T77" id="Seg_1079" n="e" s="T76">"Min </ts>
               <ts e="T78" id="Seg_1081" n="e" s="T77">kutum </ts>
               <ts e="T79" id="Seg_1083" n="e" s="T78">ölör </ts>
               <ts e="T80" id="Seg_1085" n="e" s="T79">kördük </ts>
               <ts e="T81" id="Seg_1087" n="e" s="T80">hanaːrgaːta. </ts>
               <ts e="T82" id="Seg_1089" n="e" s="T81">Bu͡ola </ts>
               <ts e="T83" id="Seg_1091" n="e" s="T82">tühüŋ </ts>
               <ts e="T84" id="Seg_1093" n="e" s="T83">minigin </ts>
               <ts e="T85" id="Seg_1095" n="e" s="T84">gɨtta. </ts>
               <ts e="T86" id="Seg_1097" n="e" s="T85">Ideliː </ts>
               <ts e="T87" id="Seg_1099" n="e" s="T86">oloru͡oguŋ". </ts>
               <ts e="T88" id="Seg_1101" n="e" s="T87">Ginnerten </ts>
               <ts e="T89" id="Seg_1103" n="e" s="T88">araga </ts>
               <ts e="T90" id="Seg_1105" n="e" s="T89">tüheːt, </ts>
               <ts e="T91" id="Seg_1107" n="e" s="T90">giniː </ts>
               <ts e="T92" id="Seg_1109" n="e" s="T91">tobuktuː </ts>
               <ts e="T93" id="Seg_1111" n="e" s="T92">oloron, </ts>
               <ts e="T94" id="Seg_1113" n="e" s="T93">üŋpüt </ts>
               <ts e="T95" id="Seg_1115" n="e" s="T94">taŋaraga. </ts>
               <ts e="T96" id="Seg_1117" n="e" s="T95">"Aga, </ts>
               <ts e="T97" id="Seg_1119" n="e" s="T96">oː, </ts>
               <ts e="T98" id="Seg_1121" n="e" s="T97">en </ts>
               <ts e="T99" id="Seg_1123" n="e" s="T98">höbülüːrüŋ </ts>
               <ts e="T100" id="Seg_1125" n="e" s="T99">ebite </ts>
               <ts e="T101" id="Seg_1127" n="e" s="T100">bu͡ollar </ts>
               <ts e="T102" id="Seg_1129" n="e" s="T101">ɨːtaːrɨ </ts>
               <ts e="T103" id="Seg_1131" n="e" s="T102">minigin </ts>
               <ts e="T104" id="Seg_1133" n="e" s="T103">hɨːha </ts>
               <ts e="T105" id="Seg_1135" n="e" s="T104">bu </ts>
               <ts e="T106" id="Seg_1137" n="e" s="T105">čaːskɨnɨ. </ts>
               <ts e="T107" id="Seg_1139" n="e" s="T106">Iti </ts>
               <ts e="T108" id="Seg_1141" n="e" s="T107">hin </ts>
               <ts e="T109" id="Seg_1143" n="e" s="T108">bu͡olbat </ts>
               <ts e="T110" id="Seg_1145" n="e" s="T109">min </ts>
               <ts e="T112" id="Seg_1147" n="e" s="T110">(han-) </ts>
               <ts e="T113" id="Seg_1149" n="e" s="T112">hanaːbɨnan. </ts>
               <ts e="T114" id="Seg_1151" n="e" s="T113">Bu͡ollun </ts>
               <ts e="T115" id="Seg_1153" n="e" s="T114">en </ts>
               <ts e="T116" id="Seg_1155" n="e" s="T115">agaj </ts>
               <ts e="T117" id="Seg_1157" n="e" s="T116">hanaːgɨnan." </ts>
               <ts e="T118" id="Seg_1159" n="e" s="T117">Kallaːnnartan </ts>
               <ts e="T119" id="Seg_1161" n="e" s="T118">köstön </ts>
               <ts e="T120" id="Seg_1163" n="e" s="T119">kelbit </ts>
               <ts e="T121" id="Seg_1165" n="e" s="T120">gini͡eke </ts>
               <ts e="T122" id="Seg_1167" n="e" s="T121">angʼel. </ts>
               <ts e="T123" id="Seg_1169" n="e" s="T122">Gini </ts>
               <ts e="T124" id="Seg_1171" n="e" s="T123">hanaːtɨn </ts>
               <ts e="T125" id="Seg_1173" n="e" s="T124">bigergeter </ts>
               <ts e="T126" id="Seg_1175" n="e" s="T125">ebit. </ts>
               <ts e="T127" id="Seg_1177" n="e" s="T126">Isuːs </ts>
               <ts e="T128" id="Seg_1179" n="e" s="T127">olus </ts>
               <ts e="T129" id="Seg_1181" n="e" s="T128">erejdener </ts>
               <ts e="T130" id="Seg_1183" n="e" s="T129">kördük </ts>
               <ts e="T131" id="Seg_1185" n="e" s="T130">eletinen </ts>
               <ts e="T132" id="Seg_1187" n="e" s="T131">üŋer </ts>
               <ts e="T133" id="Seg_1189" n="e" s="T132">bu͡olbut. </ts>
               <ts e="T134" id="Seg_1191" n="e" s="T133">Gini </ts>
               <ts e="T135" id="Seg_1193" n="e" s="T134">kölöhüne </ts>
               <ts e="T136" id="Seg_1195" n="e" s="T135">agaj </ts>
               <ts e="T138" id="Seg_1197" n="e" s="T136">(kür-) </ts>
               <ts e="T139" id="Seg_1199" n="e" s="T138">hüːrer, </ts>
               <ts e="T140" id="Seg_1201" n="e" s="T139">kaːnnɨː </ts>
               <ts e="T141" id="Seg_1203" n="e" s="T140">hirge </ts>
               <ts e="T142" id="Seg_1205" n="e" s="T141">toktor. </ts>
               <ts e="T143" id="Seg_1207" n="e" s="T142">Ü͡öreter </ts>
               <ts e="T144" id="Seg_1209" n="e" s="T143">dʼonugar </ts>
               <ts e="T145" id="Seg_1211" n="e" s="T144">tönnön </ts>
               <ts e="T146" id="Seg_1213" n="e" s="T145">kelen, </ts>
               <ts e="T147" id="Seg_1215" n="e" s="T146">gini </ts>
               <ts e="T148" id="Seg_1217" n="e" s="T147">bulbut </ts>
               <ts e="T149" id="Seg_1219" n="e" s="T148">ontulara </ts>
               <ts e="T150" id="Seg_1221" n="e" s="T149">utuja </ts>
               <ts e="T151" id="Seg_1223" n="e" s="T150">hɨtallar. </ts>
               <ts e="T152" id="Seg_1225" n="e" s="T151">Oloru </ts>
               <ts e="T153" id="Seg_1227" n="e" s="T152">uhugunnaraːt </ts>
               <ts e="T154" id="Seg_1229" n="e" s="T153">haŋarbɨt: </ts>
               <ts e="T155" id="Seg_1231" n="e" s="T154">"Kajtak </ts>
               <ts e="T156" id="Seg_1233" n="e" s="T155">ehigi </ts>
               <ts e="T157" id="Seg_1235" n="e" s="T156">biːr </ts>
               <ts e="T158" id="Seg_1237" n="e" s="T157">čaːs </ts>
               <ts e="T159" id="Seg_1239" n="e" s="T158">kuraŋɨgar </ts>
               <ts e="T160" id="Seg_1241" n="e" s="T159">olorbot </ts>
               <ts e="T161" id="Seg_1243" n="e" s="T160">bu͡ollugut </ts>
               <ts e="T162" id="Seg_1245" n="e" s="T161">minigin </ts>
               <ts e="T163" id="Seg_1247" n="e" s="T162">gɨtta? </ts>
               <ts e="T164" id="Seg_1249" n="e" s="T163">Oloruŋ, </ts>
               <ts e="T165" id="Seg_1251" n="e" s="T164">üŋüŋ. </ts>
               <ts e="T166" id="Seg_1253" n="e" s="T165">Kuhagan </ts>
               <ts e="T167" id="Seg_1255" n="e" s="T166">di͡ek </ts>
               <ts e="T168" id="Seg_1257" n="e" s="T167">tartarɨmɨ͡akkɨtɨn </ts>
               <ts e="T169" id="Seg_1259" n="e" s="T168">ajɨː </ts>
               <ts e="T170" id="Seg_1261" n="e" s="T169">kamnas </ts>
               <ts e="T171" id="Seg_1263" n="e" s="T170">bu͡olaːččɨ, </ts>
               <ts e="T172" id="Seg_1265" n="e" s="T171">et </ts>
               <ts e="T173" id="Seg_1267" n="e" s="T172">bu͡ollagɨna </ts>
               <ts e="T174" id="Seg_1269" n="e" s="T173">küːhe </ts>
               <ts e="T175" id="Seg_1271" n="e" s="T174">hu͡ok." </ts>
               <ts e="T176" id="Seg_1273" n="e" s="T175">Ginilerten </ts>
               <ts e="T177" id="Seg_1275" n="e" s="T176">aragan </ts>
               <ts e="T178" id="Seg_1277" n="e" s="T177">baraːn, </ts>
               <ts e="T179" id="Seg_1279" n="e" s="T178">gini </ts>
               <ts e="T180" id="Seg_1281" n="e" s="T179">emi͡e </ts>
               <ts e="T181" id="Seg_1283" n="e" s="T180">üŋpüt, </ts>
               <ts e="T182" id="Seg_1285" n="e" s="T181">haŋaran: </ts>
               <ts e="T183" id="Seg_1287" n="e" s="T182">"Min </ts>
               <ts e="T184" id="Seg_1289" n="e" s="T183">agam, </ts>
               <ts e="T185" id="Seg_1291" n="e" s="T184">iti </ts>
               <ts e="T186" id="Seg_1293" n="e" s="T185">čaːskɨ </ts>
               <ts e="T187" id="Seg_1295" n="e" s="T186">min </ts>
               <ts e="T188" id="Seg_1297" n="e" s="T187">attɨbɨnan </ts>
               <ts e="T189" id="Seg_1299" n="e" s="T188">aːhar </ts>
               <ts e="T190" id="Seg_1301" n="e" s="T189">bu͡ollagɨna, </ts>
               <ts e="T191" id="Seg_1303" n="e" s="T190">min </ts>
               <ts e="T192" id="Seg_1305" n="e" s="T191">ihi͡ekpin, </ts>
               <ts e="T193" id="Seg_1307" n="e" s="T192">bu͡ollun </ts>
               <ts e="T194" id="Seg_1309" n="e" s="T193">en </ts>
               <ts e="T195" id="Seg_1311" n="e" s="T194">hanaːŋ </ts>
               <ts e="T196" id="Seg_1313" n="e" s="T195">kördük." </ts>
               <ts e="T197" id="Seg_1315" n="e" s="T196">Bu </ts>
               <ts e="T198" id="Seg_1317" n="e" s="T197">malʼitvaga </ts>
               <ts e="T199" id="Seg_1319" n="e" s="T198">Krʼistos </ts>
               <ts e="T200" id="Seg_1321" n="e" s="T199">haŋarar </ts>
               <ts e="T201" id="Seg_1323" n="e" s="T200">erejdener </ts>
               <ts e="T202" id="Seg_1325" n="e" s="T201">čaːskɨ </ts>
               <ts e="T203" id="Seg_1327" n="e" s="T202">tuhunan, </ts>
               <ts e="T204" id="Seg_1329" n="e" s="T203">tugu </ts>
               <ts e="T205" id="Seg_1331" n="e" s="T204">gini </ts>
               <ts e="T206" id="Seg_1333" n="e" s="T205">ihi͡ek </ts>
               <ts e="T207" id="Seg_1335" n="e" s="T206">tustaːk </ts>
               <ts e="T208" id="Seg_1337" n="e" s="T207">bihigi </ts>
               <ts e="T209" id="Seg_1339" n="e" s="T208">anʼɨːlarɨ </ts>
               <ts e="T210" id="Seg_1341" n="e" s="T209">oŋorbupput </ts>
               <ts e="T211" id="Seg_1343" n="e" s="T210">ihin. </ts>
               <ts e="T212" id="Seg_1345" n="e" s="T211">Kim </ts>
               <ts e="T213" id="Seg_1347" n="e" s="T212">da </ts>
               <ts e="T214" id="Seg_1349" n="e" s="T213">bilbet, </ts>
               <ts e="T215" id="Seg_1351" n="e" s="T214">kihi </ts>
               <ts e="T216" id="Seg_1353" n="e" s="T215">hanaːta </ts>
               <ts e="T217" id="Seg_1355" n="e" s="T216">da </ts>
               <ts e="T218" id="Seg_1357" n="e" s="T217">koppot </ts>
               <ts e="T219" id="Seg_1359" n="e" s="T218">ol </ts>
               <ts e="T220" id="Seg_1361" n="e" s="T219">erejdener </ts>
               <ts e="T221" id="Seg_1363" n="e" s="T220">kɨtaːnagɨn. </ts>
               <ts e="T222" id="Seg_1365" n="e" s="T221">Taŋara </ts>
               <ts e="T223" id="Seg_1367" n="e" s="T222">u͡ola </ts>
               <ts e="T224" id="Seg_1369" n="e" s="T223">itini </ts>
               <ts e="T225" id="Seg_1371" n="e" s="T224">ɨlsɨbɨta. </ts>
               <ts e="T226" id="Seg_1373" n="e" s="T225">Gini </ts>
               <ts e="T227" id="Seg_1375" n="e" s="T226">biler </ts>
               <ts e="T228" id="Seg_1377" n="e" s="T227">ete – </ts>
               <ts e="T229" id="Seg_1379" n="e" s="T228">bejetin </ts>
               <ts e="T230" id="Seg_1381" n="e" s="T229">tɨːnɨn </ts>
               <ts e="T231" id="Seg_1383" n="e" s="T230">bi͡erer. </ts>
               <ts e="T232" id="Seg_1385" n="e" s="T231">Bihigini </ts>
               <ts e="T233" id="Seg_1387" n="e" s="T232">bɨːhaːrɨ. </ts>
               <ts e="T234" id="Seg_1389" n="e" s="T233">Kim </ts>
               <ts e="T235" id="Seg_1391" n="e" s="T234">da </ts>
               <ts e="T236" id="Seg_1393" n="e" s="T235">bu͡ollun. </ts>
               <ts e="T237" id="Seg_1395" n="e" s="T236">Gini </ts>
               <ts e="T238" id="Seg_1397" n="e" s="T237">ispetege </ts>
               <ts e="T239" id="Seg_1399" n="e" s="T238">bu͡ollar </ts>
               <ts e="T240" id="Seg_1401" n="e" s="T239">ol </ts>
               <ts e="T241" id="Seg_1403" n="e" s="T240">čaːskɨttan, </ts>
               <ts e="T242" id="Seg_1405" n="e" s="T241">bihigi </ts>
               <ts e="T243" id="Seg_1407" n="e" s="T242">anʼɨːlarɨ </ts>
               <ts e="T244" id="Seg_1409" n="e" s="T243">oŋorbupput </ts>
               <ts e="T245" id="Seg_1411" n="e" s="T244">bi͡ek </ts>
               <ts e="T246" id="Seg_1413" n="e" s="T245">kaːlar </ts>
               <ts e="T247" id="Seg_1415" n="e" s="T246">etilere. </ts>
               <ts e="T248" id="Seg_1417" n="e" s="T247">Ehigi </ts>
               <ts e="T249" id="Seg_1419" n="e" s="T248">istibikkit </ts>
               <ts e="T250" id="Seg_1421" n="e" s="T249">östörü </ts>
               <ts e="T251" id="Seg_1423" n="e" s="T250">"Isuːs </ts>
               <ts e="T252" id="Seg_1425" n="e" s="T251">Xrʼistos – </ts>
               <ts e="T253" id="Seg_1427" n="e" s="T252">ogolor </ts>
               <ts e="T254" id="Seg_1429" n="e" s="T253">dogottoro" </ts>
               <ts e="T255" id="Seg_1431" n="e" s="T254">di͡en </ts>
               <ts e="T256" id="Seg_1433" n="e" s="T255">kinigetten. </ts>
               <ts e="T257" id="Seg_1435" n="e" s="T256">Aːkpɨta, </ts>
               <ts e="T258" id="Seg_1437" n="e" s="T257">tulmaːstaːbɨta </ts>
               <ts e="T259" id="Seg_1439" n="e" s="T258">Papov. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T9" id="Seg_1440" s="T0">PoNA_19940110_MountOfOlives_transl.001 (001.001)</ta>
            <ta e="T22" id="Seg_1441" s="T9">PoNA_19940110_MountOfOlives_transl.002 (001.002)</ta>
            <ta e="T34" id="Seg_1442" s="T22">PoNA_19940110_MountOfOlives_transl.003 (001.003)</ta>
            <ta e="T44" id="Seg_1443" s="T34">PoNA_19940110_MountOfOlives_transl.004 (001.004)</ta>
            <ta e="T47" id="Seg_1444" s="T44">PoNA_19940110_MountOfOlives_transl.005 (001.005)</ta>
            <ta e="T52" id="Seg_1445" s="T47">PoNA_19940110_MountOfOlives_transl.006 (001.006)</ta>
            <ta e="T58" id="Seg_1446" s="T52">PoNA_19940110_MountOfOlives_transl.007 (001.007)</ta>
            <ta e="T65" id="Seg_1447" s="T58">PoNA_19940110_MountOfOlives_transl.008 (001.008)</ta>
            <ta e="T72" id="Seg_1448" s="T65">PoNA_19940110_MountOfOlives_transl.009 (001.009)</ta>
            <ta e="T76" id="Seg_1449" s="T72">PoNA_19940110_MountOfOlives_transl.010 (001.010)</ta>
            <ta e="T81" id="Seg_1450" s="T76">PoNA_19940110_MountOfOlives_transl.011 (001.011)</ta>
            <ta e="T85" id="Seg_1451" s="T81">PoNA_19940110_MountOfOlives_transl.012 (001.012)</ta>
            <ta e="T87" id="Seg_1452" s="T85">PoNA_19940110_MountOfOlives_transl.013 (001.013)</ta>
            <ta e="T95" id="Seg_1453" s="T87">PoNA_19940110_MountOfOlives_transl.014 (001.014)</ta>
            <ta e="T106" id="Seg_1454" s="T95">PoNA_19940110_MountOfOlives_transl.015 (001.015)</ta>
            <ta e="T113" id="Seg_1455" s="T106">PoNA_19940110_MountOfOlives_transl.016 (001.016)</ta>
            <ta e="T117" id="Seg_1456" s="T113">PoNA_19940110_MountOfOlives_transl.017 (001.017)</ta>
            <ta e="T122" id="Seg_1457" s="T117">PoNA_19940110_MountOfOlives_transl.018 (001.018)</ta>
            <ta e="T126" id="Seg_1458" s="T122">PoNA_19940110_MountOfOlives_transl.019 (001.019)</ta>
            <ta e="T133" id="Seg_1459" s="T126">PoNA_19940110_MountOfOlives_transl.020 (001.020)</ta>
            <ta e="T142" id="Seg_1460" s="T133">PoNA_19940110_MountOfOlives_transl.021 (001.021)</ta>
            <ta e="T151" id="Seg_1461" s="T142">PoNA_19940110_MountOfOlives_transl.022 (001.022)</ta>
            <ta e="T154" id="Seg_1462" s="T151">PoNA_19940110_MountOfOlives_transl.023 (001.023)</ta>
            <ta e="T163" id="Seg_1463" s="T154">PoNA_19940110_MountOfOlives_transl.024 (001.024)</ta>
            <ta e="T165" id="Seg_1464" s="T163">PoNA_19940110_MountOfOlives_transl.025 (001.025)</ta>
            <ta e="T175" id="Seg_1465" s="T165">PoNA_19940110_MountOfOlives_transl.026 (001.026)</ta>
            <ta e="T182" id="Seg_1466" s="T175">PoNA_19940110_MountOfOlives_transl.027 (001.027)</ta>
            <ta e="T196" id="Seg_1467" s="T182">PoNA_19940110_MountOfOlives_transl.028 (001.028)</ta>
            <ta e="T211" id="Seg_1468" s="T196">PoNA_19940110_MountOfOlives_transl.029 (001.029)</ta>
            <ta e="T221" id="Seg_1469" s="T211">PoNA_19940110_MountOfOlives_transl.030 (001.030)</ta>
            <ta e="T225" id="Seg_1470" s="T221">PoNA_19940110_MountOfOlives_transl.031 (001.031)</ta>
            <ta e="T231" id="Seg_1471" s="T225">PoNA_19940110_MountOfOlives_transl.032 (001.032)</ta>
            <ta e="T233" id="Seg_1472" s="T231">PoNA_19940110_MountOfOlives_transl.033 (001.033)</ta>
            <ta e="T236" id="Seg_1473" s="T233">PoNA_19940110_MountOfOlives_transl.034 (001.034)</ta>
            <ta e="T247" id="Seg_1474" s="T236">PoNA_19940110_MountOfOlives_transl.035 (001.035)</ta>
            <ta e="T256" id="Seg_1475" s="T247">PoNA_19940110_MountOfOlives_transl.036 (001.036)</ta>
            <ta e="T259" id="Seg_1476" s="T256">PoNA_19940110_MountOfOlives_transl.037 (001.037)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T9" id="Seg_1477" s="T0">Нөӈүө өспүт бу аата буолар "Гэпсэмания (?) олокко молитва аагар."</ta>
            <ta e="T22" id="Seg_1478" s="T9">Һол түүн киэһэ көрсүү кэннитигэр Иисус уон биир үөрэтэр киһилэрин гытта таксыбыт Иерусалимтан.</ta>
            <ta e="T34" id="Seg_1479" s="T22">Кидрон ааттаак курулуу турар ууну таксан кэлбит Иерусалим аттыгар баар Ипсоманиия олокко.</ta>
            <ta e="T44" id="Seg_1480" s="T34">Онно Иисус үөрэтэр дьонун гытта тэӈкэ маска киирэн диэбит гинилэргэ: </ta>
            <ta e="T47" id="Seg_1481" s="T44">"Олоро түһүӈ манна.</ta>
            <ta e="T52" id="Seg_1482" s="T47">Мин бара түһүөм таӈарага үӈэ".</ta>
            <ta e="T58" id="Seg_1483" s="T52">Бэйтин гытта ылбыт Пүөтуру, Дьакобы, Уйбааны.</ta>
            <ta e="T65" id="Seg_1484" s="T58">Гини того ирэ һанааргыыр, чүӈкүйэр көрдүк эбит.</ta>
            <ta e="T72" id="Seg_1485" s="T65">Биһиги аньыылары оӈорбуппут улаканнык баттыыр, эбит, гинини.</ta>
            <ta e="T76" id="Seg_1486" s="T72">Иисус диэбит үөрэтэр дьонугар: </ta>
            <ta e="T81" id="Seg_1487" s="T76">"Мин кутум өлөр көрдүк һанааргаата.</ta>
            <ta e="T85" id="Seg_1488" s="T81">Буола түһүӈ минигин гытта.</ta>
            <ta e="T87" id="Seg_1489" s="T85">Идэлии олоруогуӈ".</ta>
            <ta e="T95" id="Seg_1490" s="T87">Гиннэртэн арага түһээт, гинии тобуктуу олорон, үӈпүт таӈарага.</ta>
            <ta e="T106" id="Seg_1491" s="T95">"Ага, о-о, эн һөбүлүүрүӈ эбитэ буоллар ыытаары минигин һыыһа бу чааскыны.</ta>
            <ta e="T113" id="Seg_1492" s="T106">Ити һин буолбат мин (һанаа) һанаабынан.</ta>
            <ta e="T117" id="Seg_1493" s="T113">Буоллун эн агай һанаагынан."</ta>
            <ta e="T122" id="Seg_1494" s="T117">Каллааннартан көстөн кэлбит гиниэкэ ангел.</ta>
            <ta e="T126" id="Seg_1495" s="T122">Гини һанаатын бигэргэтэр эбит. </ta>
            <ta e="T133" id="Seg_1496" s="T126">Иисус олус эрэйдэнэр көрдүк элэтинэн үӈэр буолбут.</ta>
            <ta e="T142" id="Seg_1497" s="T133">Гини көлөһүнэ агай (күр)һүүрэр, каанныы һиргэ токтор.</ta>
            <ta e="T151" id="Seg_1498" s="T142">Үөрэтэр дьонугар төннөн кэлэн, гини булбут онтулара утуйа һыталлар.</ta>
            <ta e="T154" id="Seg_1499" s="T151">Олору уһугуннараат һаӈарбыт: </ta>
            <ta e="T163" id="Seg_1500" s="T154">"Кайдак эһиги биир чаас кураӈыгар олорбот буоллугут минигин гытта?</ta>
            <ta e="T165" id="Seg_1501" s="T163">Олоруӈ, үӈүӈ.</ta>
            <ta e="T175" id="Seg_1502" s="T165">Куһаган диэк тартарымыаккытын айыы камнас буолааччы, эт буоллагына күүһэ һуок."</ta>
            <ta e="T182" id="Seg_1503" s="T175">Гинилэртэн араган бараан, гини эмиэ үӈпүт, һаӈаран:</ta>
            <ta e="T196" id="Seg_1504" s="T182">"Мин агам, ити чааскы мин аттыбынан ааһар буоллагына, мин иһэкпин, буоллун эн һанааӈ көрдүк."</ta>
            <ta e="T211" id="Seg_1505" s="T196">Бу молитвага Христос һаӈарар эрэйдэнэр чааскы туһунан, тугу гини иһиэк тустаак биһиги аньыылары оӈорбуппут иһин.</ta>
            <ta e="T221" id="Seg_1506" s="T211">Ким да билбэт, киһи һанаата да коппот ол эрэйдэнэр кытаанагын.</ta>
            <ta e="T225" id="Seg_1507" s="T221">Таӈара уола итини ылсыбыта.</ta>
            <ta e="T231" id="Seg_1508" s="T225">Гини билэр этэ – бэйэтин тыынын биэрэр.</ta>
            <ta e="T233" id="Seg_1509" s="T231">Биһигини быыһаары.</ta>
            <ta e="T236" id="Seg_1510" s="T233">Ким да буоллун.</ta>
            <ta e="T247" id="Seg_1511" s="T236">Гини испэтэгэ буоллар ол чааскыттан, биһиги аньыылары оӈорбуппут биэк каалар этилэрэ.</ta>
            <ta e="T256" id="Seg_1512" s="T247">Эһиги истибиккит өстөрү "Иисус Христос – оголор доготторо" диэн кинигэттэн.</ta>
            <ta e="T259" id="Seg_1513" s="T256">Аакпыта, тулмаастаабыта Попов.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T9" id="Seg_1514" s="T0">Nöŋü͡ö öspüt bu aːta bu͡olar "Gʼipsʼemanʼiːja olokko malʼitva aːgar." </ta>
            <ta e="T22" id="Seg_1515" s="T9">Hol tüːn ki͡ehe körsüː kennitiger Isuːs u͡on biːr ü͡öreter kihilerin gɨtta taksɨbɨt Irusalʼimtan. </ta>
            <ta e="T34" id="Seg_1516" s="T22">Kidron aːttaːk kuruluː turar uːnu taksan kelbit Irusalʼim attɨgar baːr Ipsomanʼiːja olokko. </ta>
            <ta e="T44" id="Seg_1517" s="T34">Onno Isuːs ü͡öreter dʼonun gɨtta teŋke maska kiːren di͡ebit ginilerge: </ta>
            <ta e="T47" id="Seg_1518" s="T44">"Oloro tühüŋ manna. </ta>
            <ta e="T52" id="Seg_1519" s="T47">Min bara tühü͡öm taŋaraga üŋe". </ta>
            <ta e="T58" id="Seg_1520" s="T52">Bejetin gɨtta ɨlbɨt Pü͡öturu, Dʼaːkabɨ, Ujbaːnɨ. </ta>
            <ta e="T65" id="Seg_1521" s="T58">Gini togo ire hanaːrgɨːr, čüŋküjer kördük ebit. </ta>
            <ta e="T72" id="Seg_1522" s="T65">Bihigi anʼɨːlarɨ oŋorbupput ulakannɨk battɨːr ebit ginini. </ta>
            <ta e="T76" id="Seg_1523" s="T72">Isuːs di͡ebit ü͡öreter dʼonugar: </ta>
            <ta e="T81" id="Seg_1524" s="T76">"Min kutum ölör kördük hanaːrgaːta. </ta>
            <ta e="T85" id="Seg_1525" s="T81">Bu͡ola tühüŋ minigin gɨtta. </ta>
            <ta e="T87" id="Seg_1526" s="T85">Ideliː oloru͡oguŋ". </ta>
            <ta e="T95" id="Seg_1527" s="T87">Ginnerten araga tüheːt, giniː tobuktuː oloron, üŋpüt taŋaraga. </ta>
            <ta e="T106" id="Seg_1528" s="T95">"Aga, oː, en höbülüːrüŋ ebite bu͡ollar ɨːtaːrɨ minigin hɨːha bu čaːskɨnɨ. </ta>
            <ta e="T113" id="Seg_1529" s="T106">Iti hin bu͡olbat min (han-) hanaːbɨnan. </ta>
            <ta e="T117" id="Seg_1530" s="T113">Bu͡ollun en agaj hanaːgɨnan." </ta>
            <ta e="T122" id="Seg_1531" s="T117">Kallaːnnartan köstön kelbit gini͡eke angʼel. </ta>
            <ta e="T126" id="Seg_1532" s="T122">Gini hanaːtɨn bigergeter ebit. </ta>
            <ta e="T133" id="Seg_1533" s="T126">Isuːs olus erejdener kördük eletinen üŋer bu͡olbut. </ta>
            <ta e="T142" id="Seg_1534" s="T133">Gini kölöhüne agaj (kür-) hüːrer, kaːnnɨː hirge toktor. </ta>
            <ta e="T151" id="Seg_1535" s="T142">Ü͡öreter dʼonugar tönnön kelen, gini bulbut ontulara utuja hɨtallar. </ta>
            <ta e="T154" id="Seg_1536" s="T151">Oloru uhugunnaraːt haŋarbɨt: </ta>
            <ta e="T163" id="Seg_1537" s="T154">"Kajtak ehigi biːr čaːs kuraŋɨgar olorbot bu͡ollugut minigin gɨtta? </ta>
            <ta e="T165" id="Seg_1538" s="T163">Oloruŋ, üŋüŋ. </ta>
            <ta e="T175" id="Seg_1539" s="T165">Kuhagan di͡ek tartarɨmɨ͡akkɨtɨn ajɨː kamnas bu͡olaːččɨ, et bu͡ollagɨna küːhe hu͡ok." </ta>
            <ta e="T182" id="Seg_1540" s="T175">Ginilerten aragan baraːn, gini emi͡e üŋpüt, haŋaran: </ta>
            <ta e="T196" id="Seg_1541" s="T182">"Min agam, iti čaːskɨ min attɨbɨnan aːhar bu͡ollagɨna, min ihi͡ekpin, bu͡ollun en hanaːŋ kördük." </ta>
            <ta e="T211" id="Seg_1542" s="T196">Bu malʼitvaga Krʼistos haŋarar erejdener čaːskɨ tuhunan, tugu gini ihi͡ek tustaːk bihigi anʼɨːlarɨ oŋorbupput ihin. </ta>
            <ta e="T221" id="Seg_1543" s="T211">Kim da bilbet, kihi hanaːta da koppot ol erejdener kɨtaːnagɨn. </ta>
            <ta e="T225" id="Seg_1544" s="T221">Taŋara u͡ola itini ɨlsɨbɨta. </ta>
            <ta e="T231" id="Seg_1545" s="T225">Gini biler ete – bejetin tɨːnɨn bi͡erer. </ta>
            <ta e="T233" id="Seg_1546" s="T231">Bihigini bɨːhaːrɨ. </ta>
            <ta e="T236" id="Seg_1547" s="T233">Kim da bu͡ollun. </ta>
            <ta e="T247" id="Seg_1548" s="T236">Gini ispetege bu͡ollar ol čaːskɨttan, bihigi anʼɨːlarɨ oŋorbupput bi͡ek kaːlar etilere. </ta>
            <ta e="T256" id="Seg_1549" s="T247">Ehigi istibikkit östörü "Isuːs Xrʼistos – ogolor dogottoro" di͡en kinigetten. </ta>
            <ta e="T259" id="Seg_1550" s="T256">Aːkpɨta, tulmaːstaːbɨta Papov. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1551" s="T0">nöŋü͡ö</ta>
            <ta e="T2" id="Seg_1552" s="T1">ös-püt</ta>
            <ta e="T3" id="Seg_1553" s="T2">bu</ta>
            <ta e="T4" id="Seg_1554" s="T3">aːt-a</ta>
            <ta e="T5" id="Seg_1555" s="T4">bu͡ol-ar</ta>
            <ta e="T6" id="Seg_1556" s="T5">Gʼipsʼemanʼiːja</ta>
            <ta e="T7" id="Seg_1557" s="T6">olok-ko</ta>
            <ta e="T8" id="Seg_1558" s="T7">malʼitva</ta>
            <ta e="T9" id="Seg_1559" s="T8">aːg-ar</ta>
            <ta e="T10" id="Seg_1560" s="T9">hol</ta>
            <ta e="T11" id="Seg_1561" s="T10">tüːn</ta>
            <ta e="T12" id="Seg_1562" s="T11">ki͡ehe</ta>
            <ta e="T13" id="Seg_1563" s="T12">körs-üː</ta>
            <ta e="T14" id="Seg_1564" s="T13">kenni-ti-ger</ta>
            <ta e="T15" id="Seg_1565" s="T14">Isuːs</ta>
            <ta e="T16" id="Seg_1566" s="T15">u͡on</ta>
            <ta e="T17" id="Seg_1567" s="T16">biːr</ta>
            <ta e="T18" id="Seg_1568" s="T17">ü͡öret-er</ta>
            <ta e="T19" id="Seg_1569" s="T18">kihi-ler-i-n</ta>
            <ta e="T20" id="Seg_1570" s="T19">gɨtta</ta>
            <ta e="T21" id="Seg_1571" s="T20">taks-ɨ-bɨt</ta>
            <ta e="T22" id="Seg_1572" s="T21">Irusalʼim-tan</ta>
            <ta e="T23" id="Seg_1573" s="T22">Kidron</ta>
            <ta e="T24" id="Seg_1574" s="T23">aːt-taːk</ta>
            <ta e="T25" id="Seg_1575" s="T24">kurul-uː</ta>
            <ta e="T26" id="Seg_1576" s="T25">tur-ar</ta>
            <ta e="T27" id="Seg_1577" s="T26">uː-nu</ta>
            <ta e="T28" id="Seg_1578" s="T27">taks-an</ta>
            <ta e="T29" id="Seg_1579" s="T28">kel-bit</ta>
            <ta e="T30" id="Seg_1580" s="T29">Irusalʼim</ta>
            <ta e="T31" id="Seg_1581" s="T30">attɨ-gar</ta>
            <ta e="T32" id="Seg_1582" s="T31">baːr</ta>
            <ta e="T33" id="Seg_1583" s="T32">Ipsomanʼiːja</ta>
            <ta e="T34" id="Seg_1584" s="T33">olok-ko</ta>
            <ta e="T35" id="Seg_1585" s="T34">onno</ta>
            <ta e="T36" id="Seg_1586" s="T35">Isuːs</ta>
            <ta e="T37" id="Seg_1587" s="T36">ü͡öret-er</ta>
            <ta e="T38" id="Seg_1588" s="T37">dʼon-u-n</ta>
            <ta e="T39" id="Seg_1589" s="T38">gɨtta</ta>
            <ta e="T40" id="Seg_1590" s="T39">teŋke</ta>
            <ta e="T41" id="Seg_1591" s="T40">mas-ka</ta>
            <ta e="T42" id="Seg_1592" s="T41">kiːr-en</ta>
            <ta e="T43" id="Seg_1593" s="T42">di͡e-bit</ta>
            <ta e="T44" id="Seg_1594" s="T43">giniler-ge</ta>
            <ta e="T45" id="Seg_1595" s="T44">olor-o</ta>
            <ta e="T46" id="Seg_1596" s="T45">tüh-ü-ŋ</ta>
            <ta e="T47" id="Seg_1597" s="T46">manna</ta>
            <ta e="T48" id="Seg_1598" s="T47">min</ta>
            <ta e="T49" id="Seg_1599" s="T48">bar-a</ta>
            <ta e="T50" id="Seg_1600" s="T49">tüh-ü͡ö-m</ta>
            <ta e="T51" id="Seg_1601" s="T50">taŋara-ga</ta>
            <ta e="T52" id="Seg_1602" s="T51">üŋ-e</ta>
            <ta e="T53" id="Seg_1603" s="T52">beje-ti-n</ta>
            <ta e="T54" id="Seg_1604" s="T53">gɨtta</ta>
            <ta e="T55" id="Seg_1605" s="T54">ɨl-bɨt</ta>
            <ta e="T56" id="Seg_1606" s="T55">Pü͡ötur-u</ta>
            <ta e="T57" id="Seg_1607" s="T56">Dʼaːkab-ɨ</ta>
            <ta e="T58" id="Seg_1608" s="T57">Ujbaːn-ɨ</ta>
            <ta e="T59" id="Seg_1609" s="T58">gini</ta>
            <ta e="T60" id="Seg_1610" s="T59">togo</ta>
            <ta e="T61" id="Seg_1611" s="T60">ire</ta>
            <ta e="T62" id="Seg_1612" s="T61">hanaːrgɨː-r</ta>
            <ta e="T63" id="Seg_1613" s="T62">čüŋküj-er</ta>
            <ta e="T64" id="Seg_1614" s="T63">kördük</ta>
            <ta e="T65" id="Seg_1615" s="T64">e-bit</ta>
            <ta e="T66" id="Seg_1616" s="T65">bihigi</ta>
            <ta e="T67" id="Seg_1617" s="T66">anʼɨː-lar-ɨ</ta>
            <ta e="T68" id="Seg_1618" s="T67">oŋor-bup-put</ta>
            <ta e="T69" id="Seg_1619" s="T68">ulakan-nɨk</ta>
            <ta e="T70" id="Seg_1620" s="T69">battɨː-r</ta>
            <ta e="T71" id="Seg_1621" s="T70">e-bit</ta>
            <ta e="T72" id="Seg_1622" s="T71">gini-ni</ta>
            <ta e="T73" id="Seg_1623" s="T72">Isuːs</ta>
            <ta e="T74" id="Seg_1624" s="T73">di͡e-bit</ta>
            <ta e="T75" id="Seg_1625" s="T74">ü͡öret-er</ta>
            <ta e="T76" id="Seg_1626" s="T75">dʼon-u-gar</ta>
            <ta e="T77" id="Seg_1627" s="T76">min</ta>
            <ta e="T78" id="Seg_1628" s="T77">kut-u-m</ta>
            <ta e="T79" id="Seg_1629" s="T78">öl-ör</ta>
            <ta e="T80" id="Seg_1630" s="T79">kördük</ta>
            <ta e="T81" id="Seg_1631" s="T80">hanaːrgaː-t-a</ta>
            <ta e="T82" id="Seg_1632" s="T81">bu͡ol-a</ta>
            <ta e="T83" id="Seg_1633" s="T82">tüh-ü-ŋ</ta>
            <ta e="T84" id="Seg_1634" s="T83">minigi-n</ta>
            <ta e="T85" id="Seg_1635" s="T84">gɨtta</ta>
            <ta e="T86" id="Seg_1636" s="T85">ide-liː</ta>
            <ta e="T87" id="Seg_1637" s="T86">olor-u͡oguŋ</ta>
            <ta e="T88" id="Seg_1638" s="T87">ginner-ten</ta>
            <ta e="T89" id="Seg_1639" s="T88">arag-a</ta>
            <ta e="T90" id="Seg_1640" s="T89">tüh-eːt</ta>
            <ta e="T91" id="Seg_1641" s="T90">giniː</ta>
            <ta e="T92" id="Seg_1642" s="T91">tobuk-tuː</ta>
            <ta e="T93" id="Seg_1643" s="T92">olor-on</ta>
            <ta e="T94" id="Seg_1644" s="T93">üŋ-püt</ta>
            <ta e="T95" id="Seg_1645" s="T94">taŋara-ga</ta>
            <ta e="T96" id="Seg_1646" s="T95">aga</ta>
            <ta e="T97" id="Seg_1647" s="T96">oː</ta>
            <ta e="T98" id="Seg_1648" s="T97">en</ta>
            <ta e="T99" id="Seg_1649" s="T98">höbülüː-r-ü-ŋ</ta>
            <ta e="T100" id="Seg_1650" s="T99">e-bit-e</ta>
            <ta e="T101" id="Seg_1651" s="T100">bu͡ol-lar</ta>
            <ta e="T102" id="Seg_1652" s="T101">ɨːt-aːrɨ</ta>
            <ta e="T103" id="Seg_1653" s="T102">minigi-n</ta>
            <ta e="T104" id="Seg_1654" s="T103">hɨːha</ta>
            <ta e="T105" id="Seg_1655" s="T104">bu</ta>
            <ta e="T106" id="Seg_1656" s="T105">čaːskɨ-nɨ</ta>
            <ta e="T107" id="Seg_1657" s="T106">iti</ta>
            <ta e="T108" id="Seg_1658" s="T107">hin</ta>
            <ta e="T109" id="Seg_1659" s="T108">bu͡ol-bat</ta>
            <ta e="T110" id="Seg_1660" s="T109">min</ta>
            <ta e="T113" id="Seg_1661" s="T112">hanaː-bɨ-nan</ta>
            <ta e="T114" id="Seg_1662" s="T113">bu͡ol-lun</ta>
            <ta e="T115" id="Seg_1663" s="T114">en</ta>
            <ta e="T116" id="Seg_1664" s="T115">agaj</ta>
            <ta e="T117" id="Seg_1665" s="T116">hanaː-gɨ-nan</ta>
            <ta e="T118" id="Seg_1666" s="T117">kallaːn-nar-tan</ta>
            <ta e="T119" id="Seg_1667" s="T118">köst-ön</ta>
            <ta e="T120" id="Seg_1668" s="T119">kel-bit</ta>
            <ta e="T121" id="Seg_1669" s="T120">gini͡e-ke</ta>
            <ta e="T122" id="Seg_1670" s="T121">angʼel</ta>
            <ta e="T123" id="Seg_1671" s="T122">gini</ta>
            <ta e="T124" id="Seg_1672" s="T123">hanaː-tɨ-n</ta>
            <ta e="T125" id="Seg_1673" s="T124">bigerget-er</ta>
            <ta e="T126" id="Seg_1674" s="T125">e-bit</ta>
            <ta e="T127" id="Seg_1675" s="T126">Isuːs</ta>
            <ta e="T128" id="Seg_1676" s="T127">olus</ta>
            <ta e="T129" id="Seg_1677" s="T128">erej-den-er</ta>
            <ta e="T130" id="Seg_1678" s="T129">kördük</ta>
            <ta e="T131" id="Seg_1679" s="T130">ele-ti-nen</ta>
            <ta e="T132" id="Seg_1680" s="T131">üŋ-er</ta>
            <ta e="T133" id="Seg_1681" s="T132">bu͡ol-but</ta>
            <ta e="T134" id="Seg_1682" s="T133">gini</ta>
            <ta e="T135" id="Seg_1683" s="T134">kölöhün-e</ta>
            <ta e="T136" id="Seg_1684" s="T135">agaj</ta>
            <ta e="T139" id="Seg_1685" s="T138">hüːr-er</ta>
            <ta e="T140" id="Seg_1686" s="T139">kaːn-nɨː</ta>
            <ta e="T141" id="Seg_1687" s="T140">hir-ge</ta>
            <ta e="T142" id="Seg_1688" s="T141">tok-t-or</ta>
            <ta e="T143" id="Seg_1689" s="T142">ü͡öret-er</ta>
            <ta e="T144" id="Seg_1690" s="T143">dʼon-u-gar</ta>
            <ta e="T145" id="Seg_1691" s="T144">tönn-ön</ta>
            <ta e="T146" id="Seg_1692" s="T145">kel-en</ta>
            <ta e="T147" id="Seg_1693" s="T146">gini</ta>
            <ta e="T148" id="Seg_1694" s="T147">bul-but</ta>
            <ta e="T149" id="Seg_1695" s="T148">on-tu-lara</ta>
            <ta e="T150" id="Seg_1696" s="T149">utuj-a</ta>
            <ta e="T151" id="Seg_1697" s="T150">hɨt-al-lar</ta>
            <ta e="T152" id="Seg_1698" s="T151">o-lor-u</ta>
            <ta e="T153" id="Seg_1699" s="T152">uhugun-nar-aːt</ta>
            <ta e="T154" id="Seg_1700" s="T153">haŋar-bɨt</ta>
            <ta e="T155" id="Seg_1701" s="T154">kajtak</ta>
            <ta e="T156" id="Seg_1702" s="T155">ehigi</ta>
            <ta e="T157" id="Seg_1703" s="T156">biːr</ta>
            <ta e="T158" id="Seg_1704" s="T157">čaːs</ta>
            <ta e="T159" id="Seg_1705" s="T158">kuraŋ-ɨ-gar</ta>
            <ta e="T160" id="Seg_1706" s="T159">olor-bot</ta>
            <ta e="T161" id="Seg_1707" s="T160">bu͡ol-lu-gut</ta>
            <ta e="T162" id="Seg_1708" s="T161">minigi-n</ta>
            <ta e="T163" id="Seg_1709" s="T162">gɨtta</ta>
            <ta e="T164" id="Seg_1710" s="T163">olor-u-ŋ</ta>
            <ta e="T165" id="Seg_1711" s="T164">üŋ-ü-ŋ</ta>
            <ta e="T166" id="Seg_1712" s="T165">kuhagan</ta>
            <ta e="T167" id="Seg_1713" s="T166">di͡ek</ta>
            <ta e="T168" id="Seg_1714" s="T167">tart-a-r-ɨ-m-ɨ͡ak-kɨtɨ-n</ta>
            <ta e="T169" id="Seg_1715" s="T168">ajɨː</ta>
            <ta e="T170" id="Seg_1716" s="T169">kamnas</ta>
            <ta e="T171" id="Seg_1717" s="T170">bu͡ol-aːččɨ</ta>
            <ta e="T172" id="Seg_1718" s="T171">et</ta>
            <ta e="T173" id="Seg_1719" s="T172">bu͡ollagɨna</ta>
            <ta e="T174" id="Seg_1720" s="T173">küːh-e</ta>
            <ta e="T175" id="Seg_1721" s="T174">hu͡ok</ta>
            <ta e="T176" id="Seg_1722" s="T175">giniler-ten</ta>
            <ta e="T177" id="Seg_1723" s="T176">arag-an</ta>
            <ta e="T178" id="Seg_1724" s="T177">baraːn</ta>
            <ta e="T179" id="Seg_1725" s="T178">gini</ta>
            <ta e="T180" id="Seg_1726" s="T179">emi͡e</ta>
            <ta e="T181" id="Seg_1727" s="T180">üŋ-püt</ta>
            <ta e="T182" id="Seg_1728" s="T181">haŋar-an</ta>
            <ta e="T183" id="Seg_1729" s="T182">min</ta>
            <ta e="T184" id="Seg_1730" s="T183">aga-m</ta>
            <ta e="T185" id="Seg_1731" s="T184">iti</ta>
            <ta e="T186" id="Seg_1732" s="T185">čaːskɨ</ta>
            <ta e="T187" id="Seg_1733" s="T186">min</ta>
            <ta e="T188" id="Seg_1734" s="T187">attɨ-bɨ-nan</ta>
            <ta e="T189" id="Seg_1735" s="T188">aːh-ar</ta>
            <ta e="T190" id="Seg_1736" s="T189">bu͡ol-lag-ɨna</ta>
            <ta e="T191" id="Seg_1737" s="T190">min</ta>
            <ta e="T192" id="Seg_1738" s="T191">ih-i͡ek-pi-n</ta>
            <ta e="T193" id="Seg_1739" s="T192">bu͡ol-lun</ta>
            <ta e="T194" id="Seg_1740" s="T193">en</ta>
            <ta e="T195" id="Seg_1741" s="T194">hanaː-ŋ</ta>
            <ta e="T196" id="Seg_1742" s="T195">kördük</ta>
            <ta e="T197" id="Seg_1743" s="T196">bu</ta>
            <ta e="T198" id="Seg_1744" s="T197">malʼitva-ga</ta>
            <ta e="T199" id="Seg_1745" s="T198">Krʼistos</ta>
            <ta e="T200" id="Seg_1746" s="T199">haŋar-ar</ta>
            <ta e="T201" id="Seg_1747" s="T200">erej-den-er</ta>
            <ta e="T202" id="Seg_1748" s="T201">čaːskɨ</ta>
            <ta e="T203" id="Seg_1749" s="T202">tuh-u-nan</ta>
            <ta e="T204" id="Seg_1750" s="T203">tug-u</ta>
            <ta e="T205" id="Seg_1751" s="T204">gini</ta>
            <ta e="T206" id="Seg_1752" s="T205">ih-i͡ek</ta>
            <ta e="T207" id="Seg_1753" s="T206">tus-taːk</ta>
            <ta e="T208" id="Seg_1754" s="T207">bihigi</ta>
            <ta e="T209" id="Seg_1755" s="T208">anʼɨː-lar-ɨ</ta>
            <ta e="T210" id="Seg_1756" s="T209">oŋor-bup-put</ta>
            <ta e="T211" id="Seg_1757" s="T210">ihin</ta>
            <ta e="T212" id="Seg_1758" s="T211">kim</ta>
            <ta e="T213" id="Seg_1759" s="T212">da</ta>
            <ta e="T214" id="Seg_1760" s="T213">bil-bet</ta>
            <ta e="T215" id="Seg_1761" s="T214">kihi</ta>
            <ta e="T216" id="Seg_1762" s="T215">hanaː-ta</ta>
            <ta e="T217" id="Seg_1763" s="T216">da</ta>
            <ta e="T218" id="Seg_1764" s="T217">kop-pot</ta>
            <ta e="T219" id="Seg_1765" s="T218">ol</ta>
            <ta e="T220" id="Seg_1766" s="T219">erej-den-er</ta>
            <ta e="T221" id="Seg_1767" s="T220">kɨtaːnag-ɨ-n</ta>
            <ta e="T222" id="Seg_1768" s="T221">taŋara</ta>
            <ta e="T223" id="Seg_1769" s="T222">u͡ol-a</ta>
            <ta e="T224" id="Seg_1770" s="T223">iti-ni</ta>
            <ta e="T225" id="Seg_1771" s="T224">ɨl-s-ɨ-bɨt-a</ta>
            <ta e="T226" id="Seg_1772" s="T225">gini</ta>
            <ta e="T227" id="Seg_1773" s="T226">bil-er</ta>
            <ta e="T228" id="Seg_1774" s="T227">e-t-e</ta>
            <ta e="T229" id="Seg_1775" s="T228">beje-ti-n</ta>
            <ta e="T230" id="Seg_1776" s="T229">tɨːn-ɨ-n</ta>
            <ta e="T231" id="Seg_1777" s="T230">bi͡er-er</ta>
            <ta e="T232" id="Seg_1778" s="T231">bihigi-ni</ta>
            <ta e="T233" id="Seg_1779" s="T232">bɨːh-aːrɨ</ta>
            <ta e="T234" id="Seg_1780" s="T233">kim</ta>
            <ta e="T235" id="Seg_1781" s="T234">da</ta>
            <ta e="T236" id="Seg_1782" s="T235">bu͡ol-lun</ta>
            <ta e="T237" id="Seg_1783" s="T236">gini</ta>
            <ta e="T238" id="Seg_1784" s="T237">is-peteg-e</ta>
            <ta e="T239" id="Seg_1785" s="T238">bu͡ol-lar</ta>
            <ta e="T240" id="Seg_1786" s="T239">ol</ta>
            <ta e="T241" id="Seg_1787" s="T240">čaːskɨ-ttan</ta>
            <ta e="T242" id="Seg_1788" s="T241">bihigi</ta>
            <ta e="T243" id="Seg_1789" s="T242">anʼɨː-lar-ɨ</ta>
            <ta e="T244" id="Seg_1790" s="T243">oŋor-bup-put</ta>
            <ta e="T245" id="Seg_1791" s="T244">bi͡ek</ta>
            <ta e="T246" id="Seg_1792" s="T245">kaːl-ar</ta>
            <ta e="T247" id="Seg_1793" s="T246">e-ti-lere</ta>
            <ta e="T248" id="Seg_1794" s="T247">ehigi</ta>
            <ta e="T249" id="Seg_1795" s="T248">ist-i-bik-kit</ta>
            <ta e="T250" id="Seg_1796" s="T249">ös-tör-ü</ta>
            <ta e="T251" id="Seg_1797" s="T250">Isuːs</ta>
            <ta e="T252" id="Seg_1798" s="T251">Xrʼistos</ta>
            <ta e="T253" id="Seg_1799" s="T252">ogo-lor</ta>
            <ta e="T254" id="Seg_1800" s="T253">dogot-tor-o</ta>
            <ta e="T255" id="Seg_1801" s="T254">di͡e-n</ta>
            <ta e="T256" id="Seg_1802" s="T255">kinige-tten</ta>
            <ta e="T257" id="Seg_1803" s="T256">aːk-pɨt-a</ta>
            <ta e="T258" id="Seg_1804" s="T257">tulmaːstaː-bɨt-a</ta>
            <ta e="T259" id="Seg_1805" s="T258">Papov</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1806" s="T0">nöŋü͡ö</ta>
            <ta e="T2" id="Seg_1807" s="T1">ös-BIt</ta>
            <ta e="T3" id="Seg_1808" s="T2">bu</ta>
            <ta e="T4" id="Seg_1809" s="T3">aːt-tA</ta>
            <ta e="T5" id="Seg_1810" s="T4">bu͡ol-Ar</ta>
            <ta e="T6" id="Seg_1811" s="T5">Gʼipsʼemanʼiːja</ta>
            <ta e="T7" id="Seg_1812" s="T6">olok-GA</ta>
            <ta e="T8" id="Seg_1813" s="T7">malʼitva</ta>
            <ta e="T9" id="Seg_1814" s="T8">aːk-Ar</ta>
            <ta e="T10" id="Seg_1815" s="T9">hol</ta>
            <ta e="T11" id="Seg_1816" s="T10">tüːn</ta>
            <ta e="T12" id="Seg_1817" s="T11">ki͡ehe</ta>
            <ta e="T13" id="Seg_1818" s="T12">körüs-Iː</ta>
            <ta e="T14" id="Seg_1819" s="T13">kelin-tI-GAr</ta>
            <ta e="T15" id="Seg_1820" s="T14">Isus</ta>
            <ta e="T16" id="Seg_1821" s="T15">u͡on</ta>
            <ta e="T17" id="Seg_1822" s="T16">biːr</ta>
            <ta e="T18" id="Seg_1823" s="T17">ü͡öret-Ar</ta>
            <ta e="T19" id="Seg_1824" s="T18">kihi-LAr-tI-n</ta>
            <ta e="T20" id="Seg_1825" s="T19">kɨtta</ta>
            <ta e="T21" id="Seg_1826" s="T20">tagɨs-I-BIT</ta>
            <ta e="T22" id="Seg_1827" s="T21">Irusalʼim-ttAn</ta>
            <ta e="T23" id="Seg_1828" s="T22">Kidron</ta>
            <ta e="T24" id="Seg_1829" s="T23">aːt-LAːK</ta>
            <ta e="T25" id="Seg_1830" s="T24">kürüleː-A</ta>
            <ta e="T26" id="Seg_1831" s="T25">tur-Ar</ta>
            <ta e="T27" id="Seg_1832" s="T26">uː-nI</ta>
            <ta e="T28" id="Seg_1833" s="T27">tagɨs-An</ta>
            <ta e="T29" id="Seg_1834" s="T28">kel-BIT</ta>
            <ta e="T30" id="Seg_1835" s="T29">Irusalʼim</ta>
            <ta e="T31" id="Seg_1836" s="T30">attɨ-GAr</ta>
            <ta e="T32" id="Seg_1837" s="T31">baːr</ta>
            <ta e="T33" id="Seg_1838" s="T32">Gʼipsʼemanʼiːja</ta>
            <ta e="T34" id="Seg_1839" s="T33">olok-GA</ta>
            <ta e="T35" id="Seg_1840" s="T34">onno</ta>
            <ta e="T36" id="Seg_1841" s="T35">Isus</ta>
            <ta e="T37" id="Seg_1842" s="T36">ü͡öret-Ar</ta>
            <ta e="T38" id="Seg_1843" s="T37">dʼon-tI-n</ta>
            <ta e="T39" id="Seg_1844" s="T38">kɨtta</ta>
            <ta e="T40" id="Seg_1845" s="T39">tenke</ta>
            <ta e="T41" id="Seg_1846" s="T40">mas-GA</ta>
            <ta e="T42" id="Seg_1847" s="T41">kiːr-An</ta>
            <ta e="T43" id="Seg_1848" s="T42">di͡e-BIT</ta>
            <ta e="T44" id="Seg_1849" s="T43">giniler-GA</ta>
            <ta e="T45" id="Seg_1850" s="T44">olor-A</ta>
            <ta e="T46" id="Seg_1851" s="T45">tüs-I-ŋ</ta>
            <ta e="T47" id="Seg_1852" s="T46">manna</ta>
            <ta e="T48" id="Seg_1853" s="T47">min</ta>
            <ta e="T49" id="Seg_1854" s="T48">bar-A</ta>
            <ta e="T50" id="Seg_1855" s="T49">tüs-IAK-m</ta>
            <ta e="T51" id="Seg_1856" s="T50">taŋara-GA</ta>
            <ta e="T52" id="Seg_1857" s="T51">üŋ-A</ta>
            <ta e="T53" id="Seg_1858" s="T52">beje-tI-n</ta>
            <ta e="T54" id="Seg_1859" s="T53">kɨtta</ta>
            <ta e="T55" id="Seg_1860" s="T54">ɨl-BIT</ta>
            <ta e="T56" id="Seg_1861" s="T55">Pʼötr-nI</ta>
            <ta e="T57" id="Seg_1862" s="T56">Dʼaːkab-nI</ta>
            <ta e="T58" id="Seg_1863" s="T57">Ujbaːn-nI</ta>
            <ta e="T59" id="Seg_1864" s="T58">gini</ta>
            <ta e="T60" id="Seg_1865" s="T59">togo</ta>
            <ta e="T61" id="Seg_1866" s="T60">ere</ta>
            <ta e="T62" id="Seg_1867" s="T61">hanaːrgaː-Ar</ta>
            <ta e="T63" id="Seg_1868" s="T62">čüŋküj-Ar</ta>
            <ta e="T64" id="Seg_1869" s="T63">kördük</ta>
            <ta e="T65" id="Seg_1870" s="T64">e-BIT</ta>
            <ta e="T66" id="Seg_1871" s="T65">bihigi</ta>
            <ta e="T67" id="Seg_1872" s="T66">anʼɨː-LAr-nI</ta>
            <ta e="T68" id="Seg_1873" s="T67">oŋor-BIT-BIt</ta>
            <ta e="T69" id="Seg_1874" s="T68">ulakan-LIk</ta>
            <ta e="T70" id="Seg_1875" s="T69">battaː-Ar</ta>
            <ta e="T71" id="Seg_1876" s="T70">e-BIT</ta>
            <ta e="T72" id="Seg_1877" s="T71">gini-nI</ta>
            <ta e="T73" id="Seg_1878" s="T72">Isus</ta>
            <ta e="T74" id="Seg_1879" s="T73">di͡e-BIT</ta>
            <ta e="T75" id="Seg_1880" s="T74">ü͡öret-Ar</ta>
            <ta e="T76" id="Seg_1881" s="T75">dʼon-tI-GAr</ta>
            <ta e="T77" id="Seg_1882" s="T76">min</ta>
            <ta e="T78" id="Seg_1883" s="T77">kut-I-m</ta>
            <ta e="T79" id="Seg_1884" s="T78">öl-Ar</ta>
            <ta e="T80" id="Seg_1885" s="T79">kördük</ta>
            <ta e="T81" id="Seg_1886" s="T80">hanaːrgaː-TI-tA</ta>
            <ta e="T82" id="Seg_1887" s="T81">bu͡ol-A</ta>
            <ta e="T83" id="Seg_1888" s="T82">tüs-I-ŋ</ta>
            <ta e="T84" id="Seg_1889" s="T83">min-n</ta>
            <ta e="T85" id="Seg_1890" s="T84">kɨtta</ta>
            <ta e="T86" id="Seg_1891" s="T85">ide-LIː</ta>
            <ta e="T87" id="Seg_1892" s="T86">olor-IAgIŋ</ta>
            <ta e="T88" id="Seg_1893" s="T87">giniler-ttAn</ta>
            <ta e="T89" id="Seg_1894" s="T88">araːk-A</ta>
            <ta e="T90" id="Seg_1895" s="T89">tüs-AːT</ta>
            <ta e="T91" id="Seg_1896" s="T90">gini</ta>
            <ta e="T92" id="Seg_1897" s="T91">tobuk-LIː</ta>
            <ta e="T93" id="Seg_1898" s="T92">olor-An</ta>
            <ta e="T94" id="Seg_1899" s="T93">üŋ-BIT</ta>
            <ta e="T95" id="Seg_1900" s="T94">taŋara-GA</ta>
            <ta e="T96" id="Seg_1901" s="T95">aga</ta>
            <ta e="T97" id="Seg_1902" s="T96">oː</ta>
            <ta e="T98" id="Seg_1903" s="T97">en</ta>
            <ta e="T99" id="Seg_1904" s="T98">höbüleː-Ar-I-ŋ</ta>
            <ta e="T100" id="Seg_1905" s="T99">e-BIT-tA</ta>
            <ta e="T101" id="Seg_1906" s="T100">bu͡ol-TAR</ta>
            <ta e="T102" id="Seg_1907" s="T101">ɨːt-AːrI</ta>
            <ta e="T103" id="Seg_1908" s="T102">min-n</ta>
            <ta e="T104" id="Seg_1909" s="T103">hɨːha</ta>
            <ta e="T105" id="Seg_1910" s="T104">bu</ta>
            <ta e="T106" id="Seg_1911" s="T105">čaːskɨ-nI</ta>
            <ta e="T107" id="Seg_1912" s="T106">iti</ta>
            <ta e="T108" id="Seg_1913" s="T107">hin</ta>
            <ta e="T109" id="Seg_1914" s="T108">bu͡ol-BAT</ta>
            <ta e="T110" id="Seg_1915" s="T109">min</ta>
            <ta e="T113" id="Seg_1916" s="T112">hanaː-BI-nAn</ta>
            <ta e="T114" id="Seg_1917" s="T113">bu͡ol-TIn</ta>
            <ta e="T115" id="Seg_1918" s="T114">en</ta>
            <ta e="T116" id="Seg_1919" s="T115">agaj</ta>
            <ta e="T117" id="Seg_1920" s="T116">hanaː-GI-nAn</ta>
            <ta e="T118" id="Seg_1921" s="T117">kallaːn-LAr-ttAn</ta>
            <ta e="T119" id="Seg_1922" s="T118">köhün-An</ta>
            <ta e="T120" id="Seg_1923" s="T119">kel-BIT</ta>
            <ta e="T121" id="Seg_1924" s="T120">gini-GA</ta>
            <ta e="T122" id="Seg_1925" s="T121">angʼel</ta>
            <ta e="T123" id="Seg_1926" s="T122">gini</ta>
            <ta e="T124" id="Seg_1927" s="T123">hanaː-tI-n</ta>
            <ta e="T125" id="Seg_1928" s="T124">bigerget-Ar</ta>
            <ta e="T126" id="Seg_1929" s="T125">e-BIT</ta>
            <ta e="T127" id="Seg_1930" s="T126">Isus</ta>
            <ta e="T128" id="Seg_1931" s="T127">olus</ta>
            <ta e="T129" id="Seg_1932" s="T128">erej-LAN-Ar</ta>
            <ta e="T130" id="Seg_1933" s="T129">kördük</ta>
            <ta e="T131" id="Seg_1934" s="T130">ele-tI-nAn</ta>
            <ta e="T132" id="Seg_1935" s="T131">üŋ-Ar</ta>
            <ta e="T133" id="Seg_1936" s="T132">bu͡ol-BIT</ta>
            <ta e="T134" id="Seg_1937" s="T133">gini</ta>
            <ta e="T135" id="Seg_1938" s="T134">kölöhün-tA</ta>
            <ta e="T136" id="Seg_1939" s="T135">agaj</ta>
            <ta e="T139" id="Seg_1940" s="T138">hüːr-Ar</ta>
            <ta e="T140" id="Seg_1941" s="T139">kaːn-LIː</ta>
            <ta e="T141" id="Seg_1942" s="T140">hir-GA</ta>
            <ta e="T142" id="Seg_1943" s="T141">tok-t-Ar</ta>
            <ta e="T143" id="Seg_1944" s="T142">ü͡öret-Ar</ta>
            <ta e="T144" id="Seg_1945" s="T143">dʼon-tI-GAr</ta>
            <ta e="T145" id="Seg_1946" s="T144">tönün-An</ta>
            <ta e="T146" id="Seg_1947" s="T145">kel-An</ta>
            <ta e="T147" id="Seg_1948" s="T146">gini</ta>
            <ta e="T148" id="Seg_1949" s="T147">bul-BIT</ta>
            <ta e="T149" id="Seg_1950" s="T148">ol-tI-LArA</ta>
            <ta e="T150" id="Seg_1951" s="T149">utuj-A</ta>
            <ta e="T151" id="Seg_1952" s="T150">hɨt-Ar-LAr</ta>
            <ta e="T152" id="Seg_1953" s="T151">ol-LAr-nI</ta>
            <ta e="T153" id="Seg_1954" s="T152">uhugun-TAr-AːT</ta>
            <ta e="T154" id="Seg_1955" s="T153">haŋar-BIT</ta>
            <ta e="T155" id="Seg_1956" s="T154">kajdak</ta>
            <ta e="T156" id="Seg_1957" s="T155">ehigi</ta>
            <ta e="T157" id="Seg_1958" s="T156">biːr</ta>
            <ta e="T158" id="Seg_1959" s="T157">čaːs</ta>
            <ta e="T159" id="Seg_1960" s="T158">kuraŋ-tI-GAr</ta>
            <ta e="T160" id="Seg_1961" s="T159">olor-BAT</ta>
            <ta e="T161" id="Seg_1962" s="T160">bu͡ol-TI-GIt</ta>
            <ta e="T162" id="Seg_1963" s="T161">min-n</ta>
            <ta e="T163" id="Seg_1964" s="T162">kɨtta</ta>
            <ta e="T164" id="Seg_1965" s="T163">olor-I-ŋ</ta>
            <ta e="T165" id="Seg_1966" s="T164">üŋ-I-ŋ</ta>
            <ta e="T166" id="Seg_1967" s="T165">kuhagan</ta>
            <ta e="T167" id="Seg_1968" s="T166">dek</ta>
            <ta e="T168" id="Seg_1969" s="T167">tart-A-r-I-m-IAK-GItI-n</ta>
            <ta e="T169" id="Seg_1970" s="T168">ajɨː</ta>
            <ta e="T170" id="Seg_1971" s="T169">kamnas</ta>
            <ta e="T171" id="Seg_1972" s="T170">bu͡ol-AːččI</ta>
            <ta e="T172" id="Seg_1973" s="T171">et</ta>
            <ta e="T173" id="Seg_1974" s="T172">bu͡ollagɨna</ta>
            <ta e="T174" id="Seg_1975" s="T173">küːs-tA</ta>
            <ta e="T175" id="Seg_1976" s="T174">hu͡ok</ta>
            <ta e="T176" id="Seg_1977" s="T175">giniler-ttAn</ta>
            <ta e="T177" id="Seg_1978" s="T176">araːk-An</ta>
            <ta e="T178" id="Seg_1979" s="T177">baran</ta>
            <ta e="T179" id="Seg_1980" s="T178">gini</ta>
            <ta e="T180" id="Seg_1981" s="T179">emi͡e</ta>
            <ta e="T181" id="Seg_1982" s="T180">üŋ-BIT</ta>
            <ta e="T182" id="Seg_1983" s="T181">haŋar-An</ta>
            <ta e="T183" id="Seg_1984" s="T182">min</ta>
            <ta e="T184" id="Seg_1985" s="T183">aga-m</ta>
            <ta e="T185" id="Seg_1986" s="T184">iti</ta>
            <ta e="T186" id="Seg_1987" s="T185">čaːskɨ</ta>
            <ta e="T187" id="Seg_1988" s="T186">min</ta>
            <ta e="T188" id="Seg_1989" s="T187">attɨ-BI-nAn</ta>
            <ta e="T189" id="Seg_1990" s="T188">aːs-Ar</ta>
            <ta e="T190" id="Seg_1991" s="T189">bu͡ol-TAK-InA</ta>
            <ta e="T191" id="Seg_1992" s="T190">min</ta>
            <ta e="T192" id="Seg_1993" s="T191">is-IAK-BI-n</ta>
            <ta e="T193" id="Seg_1994" s="T192">bu͡ol-TIn</ta>
            <ta e="T194" id="Seg_1995" s="T193">en</ta>
            <ta e="T195" id="Seg_1996" s="T194">hanaː-ŋ</ta>
            <ta e="T196" id="Seg_1997" s="T195">kördük</ta>
            <ta e="T197" id="Seg_1998" s="T196">bu</ta>
            <ta e="T198" id="Seg_1999" s="T197">malʼitva-GA</ta>
            <ta e="T199" id="Seg_2000" s="T198">Xrʼistos</ta>
            <ta e="T200" id="Seg_2001" s="T199">haŋar-Ar</ta>
            <ta e="T201" id="Seg_2002" s="T200">erej-LAN-Ar</ta>
            <ta e="T202" id="Seg_2003" s="T201">čaːskɨ</ta>
            <ta e="T203" id="Seg_2004" s="T202">tus-tI-nAn</ta>
            <ta e="T204" id="Seg_2005" s="T203">tu͡ok-nI</ta>
            <ta e="T205" id="Seg_2006" s="T204">gini</ta>
            <ta e="T206" id="Seg_2007" s="T205">is-IAK</ta>
            <ta e="T207" id="Seg_2008" s="T206">tüs-LAːK</ta>
            <ta e="T208" id="Seg_2009" s="T207">bihigi</ta>
            <ta e="T209" id="Seg_2010" s="T208">anʼɨː-LAr-nI</ta>
            <ta e="T210" id="Seg_2011" s="T209">oŋor-BIT-BIt</ta>
            <ta e="T211" id="Seg_2012" s="T210">ihin</ta>
            <ta e="T212" id="Seg_2013" s="T211">kim</ta>
            <ta e="T213" id="Seg_2014" s="T212">da</ta>
            <ta e="T214" id="Seg_2015" s="T213">bil-BAT</ta>
            <ta e="T215" id="Seg_2016" s="T214">kihi</ta>
            <ta e="T216" id="Seg_2017" s="T215">hanaː-tA</ta>
            <ta e="T217" id="Seg_2018" s="T216">da</ta>
            <ta e="T218" id="Seg_2019" s="T217">kot-BAT</ta>
            <ta e="T219" id="Seg_2020" s="T218">ol</ta>
            <ta e="T220" id="Seg_2021" s="T219">erej-LAN-Ar</ta>
            <ta e="T221" id="Seg_2022" s="T220">kɨtaːnak-tI-n</ta>
            <ta e="T222" id="Seg_2023" s="T221">taŋara</ta>
            <ta e="T223" id="Seg_2024" s="T222">u͡ol-tA</ta>
            <ta e="T224" id="Seg_2025" s="T223">iti-nI</ta>
            <ta e="T225" id="Seg_2026" s="T224">ɨl-s-I-BIT-tA</ta>
            <ta e="T226" id="Seg_2027" s="T225">gini</ta>
            <ta e="T227" id="Seg_2028" s="T226">bil-Ar</ta>
            <ta e="T228" id="Seg_2029" s="T227">e-TI-tA</ta>
            <ta e="T229" id="Seg_2030" s="T228">beje-tI-n</ta>
            <ta e="T230" id="Seg_2031" s="T229">tɨːn-tI-n</ta>
            <ta e="T231" id="Seg_2032" s="T230">bi͡er-Ar</ta>
            <ta e="T232" id="Seg_2033" s="T231">bihigi-nI</ta>
            <ta e="T233" id="Seg_2034" s="T232">bɨːhaː-AːrI</ta>
            <ta e="T234" id="Seg_2035" s="T233">kim</ta>
            <ta e="T235" id="Seg_2036" s="T234">da</ta>
            <ta e="T236" id="Seg_2037" s="T235">bu͡ol-TIn</ta>
            <ta e="T237" id="Seg_2038" s="T236">gini</ta>
            <ta e="T238" id="Seg_2039" s="T237">is-BAtAK-tA</ta>
            <ta e="T239" id="Seg_2040" s="T238">bu͡ol-TAR</ta>
            <ta e="T240" id="Seg_2041" s="T239">ol</ta>
            <ta e="T241" id="Seg_2042" s="T240">čaːskɨ-ttAn</ta>
            <ta e="T242" id="Seg_2043" s="T241">bihigi</ta>
            <ta e="T243" id="Seg_2044" s="T242">anʼɨː-LAr-nI</ta>
            <ta e="T244" id="Seg_2045" s="T243">oŋor-BIT-BIt</ta>
            <ta e="T245" id="Seg_2046" s="T244">bi͡ek</ta>
            <ta e="T246" id="Seg_2047" s="T245">kaːl-Ar</ta>
            <ta e="T247" id="Seg_2048" s="T246">e-TI-LArA</ta>
            <ta e="T248" id="Seg_2049" s="T247">ehigi</ta>
            <ta e="T249" id="Seg_2050" s="T248">ihit-I-BIT-GIt</ta>
            <ta e="T250" id="Seg_2051" s="T249">ös-LAr-nI</ta>
            <ta e="T251" id="Seg_2052" s="T250">Isus</ta>
            <ta e="T252" id="Seg_2053" s="T251">Xrʼistos</ta>
            <ta e="T253" id="Seg_2054" s="T252">ogo-LAr</ta>
            <ta e="T254" id="Seg_2055" s="T253">dogor-LAr-tA</ta>
            <ta e="T255" id="Seg_2056" s="T254">di͡e-An</ta>
            <ta e="T256" id="Seg_2057" s="T255">kinige-ttAn</ta>
            <ta e="T257" id="Seg_2058" s="T256">aːk-BIT-tA</ta>
            <ta e="T258" id="Seg_2059" s="T257">tulmaːstaː-BIT-tA</ta>
            <ta e="T259" id="Seg_2060" s="T258">Papov</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_2061" s="T0">next</ta>
            <ta e="T2" id="Seg_2062" s="T1">story-1PL.[NOM]</ta>
            <ta e="T3" id="Seg_2063" s="T2">this</ta>
            <ta e="T4" id="Seg_2064" s="T3">name-3SG.[NOM]</ta>
            <ta e="T5" id="Seg_2065" s="T4">be-PRS.[3SG]</ta>
            <ta e="T6" id="Seg_2066" s="T5">Gethsemane.[NOM]</ta>
            <ta e="T7" id="Seg_2067" s="T6">settlement-DAT/LOC</ta>
            <ta e="T8" id="Seg_2068" s="T7">prayer.[NOM]</ta>
            <ta e="T9" id="Seg_2069" s="T8">read-PRS.[3SG]</ta>
            <ta e="T10" id="Seg_2070" s="T9">that.EMPH</ta>
            <ta e="T11" id="Seg_2071" s="T10">night.[NOM]</ta>
            <ta e="T12" id="Seg_2072" s="T11">evening.[NOM]</ta>
            <ta e="T13" id="Seg_2073" s="T12">meet-NMNZ.[NOM]</ta>
            <ta e="T14" id="Seg_2074" s="T13">back-3SG-DAT/LOC</ta>
            <ta e="T15" id="Seg_2075" s="T14">Jesus.[NOM]</ta>
            <ta e="T16" id="Seg_2076" s="T15">ten</ta>
            <ta e="T17" id="Seg_2077" s="T16">one</ta>
            <ta e="T18" id="Seg_2078" s="T17">learn-PTCP.PRS</ta>
            <ta e="T19" id="Seg_2079" s="T18">human.being-PL-3SG-ACC</ta>
            <ta e="T20" id="Seg_2080" s="T19">with</ta>
            <ta e="T21" id="Seg_2081" s="T20">go.out-EP-PST2.[3SG]</ta>
            <ta e="T22" id="Seg_2082" s="T21">Jerusalem-ABL</ta>
            <ta e="T23" id="Seg_2083" s="T22">Kidron</ta>
            <ta e="T24" id="Seg_2084" s="T23">name-PROPR</ta>
            <ta e="T25" id="Seg_2085" s="T24">make.a.loud.noise-CVB.SIM</ta>
            <ta e="T26" id="Seg_2086" s="T25">stand-PTCP.PRS</ta>
            <ta e="T27" id="Seg_2087" s="T26">water-ACC</ta>
            <ta e="T28" id="Seg_2088" s="T27">go.out-CVB.SEQ</ta>
            <ta e="T29" id="Seg_2089" s="T28">come-PST2.[3SG]</ta>
            <ta e="T30" id="Seg_2090" s="T29">Jerusalem.[NOM]</ta>
            <ta e="T31" id="Seg_2091" s="T30">place.beneath-DAT/LOC</ta>
            <ta e="T32" id="Seg_2092" s="T31">there.is</ta>
            <ta e="T33" id="Seg_2093" s="T32">Gethsemane.[NOM]</ta>
            <ta e="T34" id="Seg_2094" s="T33">settlement-DAT/LOC</ta>
            <ta e="T35" id="Seg_2095" s="T34">there</ta>
            <ta e="T36" id="Seg_2096" s="T35">Jesus.[NOM]</ta>
            <ta e="T37" id="Seg_2097" s="T36">learn-PTCP.PRS</ta>
            <ta e="T38" id="Seg_2098" s="T37">people-3SG-ACC</ta>
            <ta e="T39" id="Seg_2099" s="T38">with</ta>
            <ta e="T40" id="Seg_2100" s="T39">thick</ta>
            <ta e="T41" id="Seg_2101" s="T40">forest-DAT/LOC</ta>
            <ta e="T42" id="Seg_2102" s="T41">go.in-CVB.SEQ</ta>
            <ta e="T43" id="Seg_2103" s="T42">say-PST2.[3SG]</ta>
            <ta e="T44" id="Seg_2104" s="T43">3PL-DAT/LOC</ta>
            <ta e="T45" id="Seg_2105" s="T44">sit.down-CVB.SIM</ta>
            <ta e="T46" id="Seg_2106" s="T45">fall-EP-IMP.2PL</ta>
            <ta e="T47" id="Seg_2107" s="T46">here</ta>
            <ta e="T48" id="Seg_2108" s="T47">1SG.[NOM]</ta>
            <ta e="T49" id="Seg_2109" s="T48">go-CVB.SIM</ta>
            <ta e="T50" id="Seg_2110" s="T49">fall-FUT-1SG</ta>
            <ta e="T51" id="Seg_2111" s="T50">god-DAT/LOC</ta>
            <ta e="T52" id="Seg_2112" s="T51">pray-CVB.SIM</ta>
            <ta e="T53" id="Seg_2113" s="T52">self-3SG-ACC</ta>
            <ta e="T54" id="Seg_2114" s="T53">with</ta>
            <ta e="T55" id="Seg_2115" s="T54">take-PST2.[3SG]</ta>
            <ta e="T56" id="Seg_2116" s="T55">Peter-ACC</ta>
            <ta e="T57" id="Seg_2117" s="T56">Jacob-ACC</ta>
            <ta e="T58" id="Seg_2118" s="T57">John-ACC</ta>
            <ta e="T59" id="Seg_2119" s="T58">3SG.[NOM]</ta>
            <ta e="T60" id="Seg_2120" s="T59">why</ta>
            <ta e="T61" id="Seg_2121" s="T60">INDEF</ta>
            <ta e="T62" id="Seg_2122" s="T61">think.about-PRS.[3SG]</ta>
            <ta e="T63" id="Seg_2123" s="T62">be.sad-PTCP.PRS.[NOM]</ta>
            <ta e="T64" id="Seg_2124" s="T63">similar</ta>
            <ta e="T65" id="Seg_2125" s="T64">be-PST2.[3SG]</ta>
            <ta e="T66" id="Seg_2126" s="T65">1PL.[NOM]</ta>
            <ta e="T67" id="Seg_2127" s="T66">sin-PL-ACC</ta>
            <ta e="T68" id="Seg_2128" s="T67">make-PTCP.PST-1PL.[NOM]</ta>
            <ta e="T69" id="Seg_2129" s="T68">big-ADVZ</ta>
            <ta e="T70" id="Seg_2130" s="T69">pull.down-PTCP.PRS</ta>
            <ta e="T71" id="Seg_2131" s="T70">be-PST2.[3SG]</ta>
            <ta e="T72" id="Seg_2132" s="T71">3SG-ACC</ta>
            <ta e="T73" id="Seg_2133" s="T72">Jesus.[NOM]</ta>
            <ta e="T74" id="Seg_2134" s="T73">say-PST2.[3SG]</ta>
            <ta e="T75" id="Seg_2135" s="T74">learn-PTCP.PRS</ta>
            <ta e="T76" id="Seg_2136" s="T75">people-3SG-DAT/LOC</ta>
            <ta e="T77" id="Seg_2137" s="T76">1SG.[NOM]</ta>
            <ta e="T78" id="Seg_2138" s="T77">soul-EP-1SG.[NOM]</ta>
            <ta e="T79" id="Seg_2139" s="T78">die-PTCP.PRS.[NOM]</ta>
            <ta e="T80" id="Seg_2140" s="T79">similar</ta>
            <ta e="T81" id="Seg_2141" s="T80">be.sad-PST1-3SG</ta>
            <ta e="T82" id="Seg_2142" s="T81">be-CVB.SIM</ta>
            <ta e="T83" id="Seg_2143" s="T82">fall-EP-IMP.2PL</ta>
            <ta e="T84" id="Seg_2144" s="T83">1SG-ACC</ta>
            <ta e="T85" id="Seg_2145" s="T84">with</ta>
            <ta e="T86" id="Seg_2146" s="T85">custom-SIM</ta>
            <ta e="T87" id="Seg_2147" s="T86">live-IMP.1PL</ta>
            <ta e="T88" id="Seg_2148" s="T87">3PL-ABL</ta>
            <ta e="T89" id="Seg_2149" s="T88">go.away-CVB.SIM</ta>
            <ta e="T90" id="Seg_2150" s="T89">fall-CVB.ANT</ta>
            <ta e="T91" id="Seg_2151" s="T90">3SG.[NOM]</ta>
            <ta e="T92" id="Seg_2152" s="T91">knee-SIM</ta>
            <ta e="T93" id="Seg_2153" s="T92">sit.down-CVB.SEQ</ta>
            <ta e="T94" id="Seg_2154" s="T93">pray-PST2.[3SG]</ta>
            <ta e="T95" id="Seg_2155" s="T94">god-DAT/LOC</ta>
            <ta e="T96" id="Seg_2156" s="T95">father.[NOM]</ta>
            <ta e="T97" id="Seg_2157" s="T96">oh</ta>
            <ta e="T98" id="Seg_2158" s="T97">2SG.[NOM]</ta>
            <ta e="T99" id="Seg_2159" s="T98">agree-PTCP.PRS-EP-2SG.[NOM]</ta>
            <ta e="T100" id="Seg_2160" s="T99">be-PST2-3SG</ta>
            <ta e="T101" id="Seg_2161" s="T100">be-COND.[3SG]</ta>
            <ta e="T102" id="Seg_2162" s="T101">release-CVB.PURP</ta>
            <ta e="T103" id="Seg_2163" s="T102">1SG-ACC</ta>
            <ta e="T104" id="Seg_2164" s="T103">mistake.[NOM]</ta>
            <ta e="T105" id="Seg_2165" s="T104">this</ta>
            <ta e="T106" id="Seg_2166" s="T105">cup-ACC</ta>
            <ta e="T107" id="Seg_2167" s="T106">that.[NOM]</ta>
            <ta e="T108" id="Seg_2168" s="T107">however</ta>
            <ta e="T109" id="Seg_2169" s="T108">be-NEG.[3SG]</ta>
            <ta e="T110" id="Seg_2170" s="T109">1SG.[NOM]</ta>
            <ta e="T113" id="Seg_2171" s="T112">wish-1SG-INSTR</ta>
            <ta e="T114" id="Seg_2172" s="T113">be-IMP.3SG</ta>
            <ta e="T115" id="Seg_2173" s="T114">2SG.[NOM]</ta>
            <ta e="T116" id="Seg_2174" s="T115">only</ta>
            <ta e="T117" id="Seg_2175" s="T116">wish-2SG-INSTR</ta>
            <ta e="T118" id="Seg_2176" s="T117">sky-PL-ABL</ta>
            <ta e="T119" id="Seg_2177" s="T118">to.be.on.view-CVB.SEQ</ta>
            <ta e="T120" id="Seg_2178" s="T119">come-PST2.[3SG]</ta>
            <ta e="T121" id="Seg_2179" s="T120">3SG-DAT/LOC</ta>
            <ta e="T122" id="Seg_2180" s="T121">angel.[NOM]</ta>
            <ta e="T123" id="Seg_2181" s="T122">3SG.[NOM]</ta>
            <ta e="T124" id="Seg_2182" s="T123">soul-3SG-ACC</ta>
            <ta e="T125" id="Seg_2183" s="T124">strengthen-PTCP.PRS</ta>
            <ta e="T126" id="Seg_2184" s="T125">be-PST2.[3SG]</ta>
            <ta e="T127" id="Seg_2185" s="T126">Jesus.[NOM]</ta>
            <ta e="T128" id="Seg_2186" s="T127">very</ta>
            <ta e="T129" id="Seg_2187" s="T128">pain-VBZ-PTCP.PRS.[NOM]</ta>
            <ta e="T130" id="Seg_2188" s="T129">similar</ta>
            <ta e="T131" id="Seg_2189" s="T130">last-3SG-INSTR</ta>
            <ta e="T132" id="Seg_2190" s="T131">pray-PTCP.PRS</ta>
            <ta e="T133" id="Seg_2191" s="T132">become-PST2.[3SG]</ta>
            <ta e="T134" id="Seg_2192" s="T133">3SG.[NOM]</ta>
            <ta e="T135" id="Seg_2193" s="T134">sweat-3SG.[NOM]</ta>
            <ta e="T136" id="Seg_2194" s="T135">only</ta>
            <ta e="T139" id="Seg_2195" s="T138">flow-PRS.[3SG]</ta>
            <ta e="T140" id="Seg_2196" s="T139">blood-SIM</ta>
            <ta e="T141" id="Seg_2197" s="T140">earth-DAT/LOC</ta>
            <ta e="T142" id="Seg_2198" s="T141">pour.out-CAUS-PRS.[3SG]</ta>
            <ta e="T143" id="Seg_2199" s="T142">learn-PTCP.PRS</ta>
            <ta e="T144" id="Seg_2200" s="T143">people-3SG-DAT/LOC</ta>
            <ta e="T145" id="Seg_2201" s="T144">come.back-CVB.SEQ</ta>
            <ta e="T146" id="Seg_2202" s="T145">come-CVB.SEQ</ta>
            <ta e="T147" id="Seg_2203" s="T146">3SG.[NOM]</ta>
            <ta e="T148" id="Seg_2204" s="T147">find-PST2.[3SG]</ta>
            <ta e="T149" id="Seg_2205" s="T148">that-3SG-3PL.[NOM]</ta>
            <ta e="T150" id="Seg_2206" s="T149">sleep-CVB.SIM</ta>
            <ta e="T151" id="Seg_2207" s="T150">lie-PRS-3PL</ta>
            <ta e="T152" id="Seg_2208" s="T151">that-PL-ACC</ta>
            <ta e="T153" id="Seg_2209" s="T152">wake.up-CAUS-CVB.ANT</ta>
            <ta e="T154" id="Seg_2210" s="T153">speak-PST2.[3SG]</ta>
            <ta e="T155" id="Seg_2211" s="T154">how</ta>
            <ta e="T156" id="Seg_2212" s="T155">2PL.[NOM]</ta>
            <ta e="T157" id="Seg_2213" s="T156">one</ta>
            <ta e="T158" id="Seg_2214" s="T157">hour.[NOM]</ta>
            <ta e="T159" id="Seg_2215" s="T158">approximately-3SG-DAT/LOC</ta>
            <ta e="T160" id="Seg_2216" s="T159">sit-NEG.PTCP</ta>
            <ta e="T161" id="Seg_2217" s="T160">be-PST1-2PL</ta>
            <ta e="T162" id="Seg_2218" s="T161">1SG-ACC</ta>
            <ta e="T163" id="Seg_2219" s="T162">with</ta>
            <ta e="T164" id="Seg_2220" s="T163">sit-EP-IMP.2PL</ta>
            <ta e="T165" id="Seg_2221" s="T164">pray-EP-IMP.2PL</ta>
            <ta e="T166" id="Seg_2222" s="T165">bad.[NOM]</ta>
            <ta e="T167" id="Seg_2223" s="T166">to</ta>
            <ta e="T168" id="Seg_2224" s="T167">pull-EP-CAUS-EP-NEG-PTCP.FUT-2PL-ACC</ta>
            <ta e="T169" id="Seg_2225" s="T168">soul.[NOM]</ta>
            <ta e="T170" id="Seg_2226" s="T169">alert.[NOM]</ta>
            <ta e="T171" id="Seg_2227" s="T170">be-HAB.[3SG]</ta>
            <ta e="T172" id="Seg_2228" s="T171">meat.[NOM]</ta>
            <ta e="T173" id="Seg_2229" s="T172">though</ta>
            <ta e="T174" id="Seg_2230" s="T173">power-POSS</ta>
            <ta e="T175" id="Seg_2231" s="T174">NEG.[3SG]</ta>
            <ta e="T176" id="Seg_2232" s="T175">3PL-ABL</ta>
            <ta e="T177" id="Seg_2233" s="T176">go.away-CVB.SEQ</ta>
            <ta e="T178" id="Seg_2234" s="T177">after</ta>
            <ta e="T179" id="Seg_2235" s="T178">3SG.[NOM]</ta>
            <ta e="T180" id="Seg_2236" s="T179">again</ta>
            <ta e="T181" id="Seg_2237" s="T180">pray-PST2.[3SG]</ta>
            <ta e="T182" id="Seg_2238" s="T181">speak-CVB.SEQ</ta>
            <ta e="T183" id="Seg_2239" s="T182">1SG.[NOM]</ta>
            <ta e="T184" id="Seg_2240" s="T183">father-1SG.[NOM]</ta>
            <ta e="T185" id="Seg_2241" s="T184">that.[NOM]</ta>
            <ta e="T186" id="Seg_2242" s="T185">cup.[NOM]</ta>
            <ta e="T187" id="Seg_2243" s="T186">1SG.[NOM]</ta>
            <ta e="T188" id="Seg_2244" s="T187">place.beneath-1SG-INSTR</ta>
            <ta e="T189" id="Seg_2245" s="T188">pass.by-PTCP.PRS</ta>
            <ta e="T190" id="Seg_2246" s="T189">be-TEMP-3SG</ta>
            <ta e="T191" id="Seg_2247" s="T190">1SG.[NOM]</ta>
            <ta e="T192" id="Seg_2248" s="T191">drink-PTCP.FUT-1SG-ACC</ta>
            <ta e="T193" id="Seg_2249" s="T192">be-IMP.3SG</ta>
            <ta e="T194" id="Seg_2250" s="T193">2SG.[NOM]</ta>
            <ta e="T195" id="Seg_2251" s="T194">wish-2SG.[NOM]</ta>
            <ta e="T196" id="Seg_2252" s="T195">similar</ta>
            <ta e="T197" id="Seg_2253" s="T196">this</ta>
            <ta e="T198" id="Seg_2254" s="T197">prayer-DAT/LOC</ta>
            <ta e="T199" id="Seg_2255" s="T198">Christ.[NOM]</ta>
            <ta e="T200" id="Seg_2256" s="T199">speak-PRS.[3SG]</ta>
            <ta e="T201" id="Seg_2257" s="T200">pain-VBZ-PTCP.PRS</ta>
            <ta e="T202" id="Seg_2258" s="T201">cup.[NOM]</ta>
            <ta e="T203" id="Seg_2259" s="T202">side-3SG-INSTR</ta>
            <ta e="T204" id="Seg_2260" s="T203">what-ACC</ta>
            <ta e="T205" id="Seg_2261" s="T204">3SG.[NOM]</ta>
            <ta e="T206" id="Seg_2262" s="T205">drink-PTCP.FUT</ta>
            <ta e="T207" id="Seg_2263" s="T206">fall-NEC.[3SG]</ta>
            <ta e="T208" id="Seg_2264" s="T207">1PL.[NOM]</ta>
            <ta e="T209" id="Seg_2265" s="T208">sin-PL-ACC</ta>
            <ta e="T210" id="Seg_2266" s="T209">make-PTCP.PST-1PL.[NOM]</ta>
            <ta e="T211" id="Seg_2267" s="T210">because.of</ta>
            <ta e="T212" id="Seg_2268" s="T211">who.[NOM]</ta>
            <ta e="T213" id="Seg_2269" s="T212">NEG</ta>
            <ta e="T214" id="Seg_2270" s="T213">know-NEG.[3SG]</ta>
            <ta e="T215" id="Seg_2271" s="T214">human.being.[NOM]</ta>
            <ta e="T216" id="Seg_2272" s="T215">soul-3SG</ta>
            <ta e="T217" id="Seg_2273" s="T216">NEG</ta>
            <ta e="T218" id="Seg_2274" s="T217">make.it-NEG.[3SG]</ta>
            <ta e="T219" id="Seg_2275" s="T218">that</ta>
            <ta e="T220" id="Seg_2276" s="T219">pain-VBZ-PTCP.PRS</ta>
            <ta e="T221" id="Seg_2277" s="T220">difficult-3SG-ACC</ta>
            <ta e="T222" id="Seg_2278" s="T221">God.[NOM]</ta>
            <ta e="T223" id="Seg_2279" s="T222">son-3SG.[NOM]</ta>
            <ta e="T224" id="Seg_2280" s="T223">that-ACC</ta>
            <ta e="T225" id="Seg_2281" s="T224">take-RECP/COLL-EP-PST2-3SG</ta>
            <ta e="T226" id="Seg_2282" s="T225">3SG.[NOM]</ta>
            <ta e="T227" id="Seg_2283" s="T226">know-PTCP.PRS</ta>
            <ta e="T228" id="Seg_2284" s="T227">be-PST1-3SG</ta>
            <ta e="T229" id="Seg_2285" s="T228">self-3SG-GEN</ta>
            <ta e="T230" id="Seg_2286" s="T229">soul-3SG-ACC</ta>
            <ta e="T231" id="Seg_2287" s="T230">give-PRS.[3SG]</ta>
            <ta e="T232" id="Seg_2288" s="T231">1PL-ACC</ta>
            <ta e="T233" id="Seg_2289" s="T232">save-CVB.PURP</ta>
            <ta e="T234" id="Seg_2290" s="T233">who.[NOM]</ta>
            <ta e="T235" id="Seg_2291" s="T234">NEG</ta>
            <ta e="T236" id="Seg_2292" s="T235">be-IMP.3SG</ta>
            <ta e="T237" id="Seg_2293" s="T236">3SG.[NOM]</ta>
            <ta e="T238" id="Seg_2294" s="T237">go-PST2.NEG-3SG</ta>
            <ta e="T239" id="Seg_2295" s="T238">be-COND.[3SG]</ta>
            <ta e="T240" id="Seg_2296" s="T239">that</ta>
            <ta e="T241" id="Seg_2297" s="T240">cup-ABL</ta>
            <ta e="T242" id="Seg_2298" s="T241">1PL.[NOM]</ta>
            <ta e="T243" id="Seg_2299" s="T242">sin-PL-ACC</ta>
            <ta e="T244" id="Seg_2300" s="T243">make-PTCP.PST-1PL.[NOM]</ta>
            <ta e="T245" id="Seg_2301" s="T244">always</ta>
            <ta e="T246" id="Seg_2302" s="T245">stay-PTCP.PRS</ta>
            <ta e="T247" id="Seg_2303" s="T246">be-PST1-3PL</ta>
            <ta e="T248" id="Seg_2304" s="T247">2PL.[NOM]</ta>
            <ta e="T249" id="Seg_2305" s="T248">hear-EP-PST2-2PL</ta>
            <ta e="T250" id="Seg_2306" s="T249">story-PL-ACC</ta>
            <ta e="T251" id="Seg_2307" s="T250">Jesus.[NOM]</ta>
            <ta e="T252" id="Seg_2308" s="T251">Christ.[NOM]</ta>
            <ta e="T253" id="Seg_2309" s="T252">child-PL.[NOM]</ta>
            <ta e="T254" id="Seg_2310" s="T253">friend-PL-3SG.[NOM]</ta>
            <ta e="T255" id="Seg_2311" s="T254">say-CVB.SEQ</ta>
            <ta e="T256" id="Seg_2312" s="T255">book-ABL</ta>
            <ta e="T257" id="Seg_2313" s="T256">read-PST2-3SG</ta>
            <ta e="T258" id="Seg_2314" s="T257">translate-PST2-3SG</ta>
            <ta e="T259" id="Seg_2315" s="T258">Popov.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_2316" s="T0">nächster</ta>
            <ta e="T2" id="Seg_2317" s="T1">Erzählung-1PL.[NOM]</ta>
            <ta e="T3" id="Seg_2318" s="T2">dieses</ta>
            <ta e="T4" id="Seg_2319" s="T3">Name-3SG.[NOM]</ta>
            <ta e="T5" id="Seg_2320" s="T4">sein-PRS.[3SG]</ta>
            <ta e="T6" id="Seg_2321" s="T5">Getsemani.[NOM]</ta>
            <ta e="T7" id="Seg_2322" s="T6">Siedlung-DAT/LOC</ta>
            <ta e="T8" id="Seg_2323" s="T7">Gebet.[NOM]</ta>
            <ta e="T9" id="Seg_2324" s="T8">lesen-PRS.[3SG]</ta>
            <ta e="T10" id="Seg_2325" s="T9">jenes.EMPH</ta>
            <ta e="T11" id="Seg_2326" s="T10">Nacht.[NOM]</ta>
            <ta e="T12" id="Seg_2327" s="T11">Abend.[NOM]</ta>
            <ta e="T13" id="Seg_2328" s="T12">treffen-NMNZ.[NOM]</ta>
            <ta e="T14" id="Seg_2329" s="T13">Hinterteil-3SG-DAT/LOC</ta>
            <ta e="T15" id="Seg_2330" s="T14">Jesus.[NOM]</ta>
            <ta e="T16" id="Seg_2331" s="T15">zehn</ta>
            <ta e="T17" id="Seg_2332" s="T16">eins</ta>
            <ta e="T18" id="Seg_2333" s="T17">lernen-PTCP.PRS</ta>
            <ta e="T19" id="Seg_2334" s="T18">Mensch-PL-3SG-ACC</ta>
            <ta e="T20" id="Seg_2335" s="T19">mit</ta>
            <ta e="T21" id="Seg_2336" s="T20">hinausgehen-EP-PST2.[3SG]</ta>
            <ta e="T22" id="Seg_2337" s="T21">Jerusalem-ABL</ta>
            <ta e="T23" id="Seg_2338" s="T22">Kidron</ta>
            <ta e="T24" id="Seg_2339" s="T23">Name-PROPR</ta>
            <ta e="T25" id="Seg_2340" s="T24">ein.lautes.Geräusch.machen-CVB.SIM</ta>
            <ta e="T26" id="Seg_2341" s="T25">stehen-PTCP.PRS</ta>
            <ta e="T27" id="Seg_2342" s="T26">Wasser-ACC</ta>
            <ta e="T28" id="Seg_2343" s="T27">hinausgehen-CVB.SEQ</ta>
            <ta e="T29" id="Seg_2344" s="T28">kommen-PST2.[3SG]</ta>
            <ta e="T30" id="Seg_2345" s="T29">Jerusalem.[NOM]</ta>
            <ta e="T31" id="Seg_2346" s="T30">Platz.neben-DAT/LOC</ta>
            <ta e="T32" id="Seg_2347" s="T31">es.gibt</ta>
            <ta e="T33" id="Seg_2348" s="T32">Getsemani.[NOM]</ta>
            <ta e="T34" id="Seg_2349" s="T33">Siedlung-DAT/LOC</ta>
            <ta e="T35" id="Seg_2350" s="T34">dort</ta>
            <ta e="T36" id="Seg_2351" s="T35">Jesus.[NOM]</ta>
            <ta e="T37" id="Seg_2352" s="T36">lernen-PTCP.PRS</ta>
            <ta e="T38" id="Seg_2353" s="T37">Volk-3SG-ACC</ta>
            <ta e="T39" id="Seg_2354" s="T38">mit</ta>
            <ta e="T40" id="Seg_2355" s="T39">dicht</ta>
            <ta e="T41" id="Seg_2356" s="T40">Wald-DAT/LOC</ta>
            <ta e="T42" id="Seg_2357" s="T41">hineingehen-CVB.SEQ</ta>
            <ta e="T43" id="Seg_2358" s="T42">sagen-PST2.[3SG]</ta>
            <ta e="T44" id="Seg_2359" s="T43">3PL-DAT/LOC</ta>
            <ta e="T45" id="Seg_2360" s="T44">sich.setzen-CVB.SIM</ta>
            <ta e="T46" id="Seg_2361" s="T45">fallen-EP-IMP.2PL</ta>
            <ta e="T47" id="Seg_2362" s="T46">hier</ta>
            <ta e="T48" id="Seg_2363" s="T47">1SG.[NOM]</ta>
            <ta e="T49" id="Seg_2364" s="T48">gehen-CVB.SIM</ta>
            <ta e="T50" id="Seg_2365" s="T49">fallen-FUT-1SG</ta>
            <ta e="T51" id="Seg_2366" s="T50">Gott-DAT/LOC</ta>
            <ta e="T52" id="Seg_2367" s="T51">beten-CVB.SIM</ta>
            <ta e="T53" id="Seg_2368" s="T52">selbst-3SG-ACC</ta>
            <ta e="T54" id="Seg_2369" s="T53">mit</ta>
            <ta e="T55" id="Seg_2370" s="T54">nehmen-PST2.[3SG]</ta>
            <ta e="T56" id="Seg_2371" s="T55">Petrus-ACC</ta>
            <ta e="T57" id="Seg_2372" s="T56">Jakob-ACC</ta>
            <ta e="T58" id="Seg_2373" s="T57">Johannes-ACC</ta>
            <ta e="T59" id="Seg_2374" s="T58">3SG.[NOM]</ta>
            <ta e="T60" id="Seg_2375" s="T59">warum</ta>
            <ta e="T61" id="Seg_2376" s="T60">INDEF</ta>
            <ta e="T62" id="Seg_2377" s="T61">nachdenken-PRS.[3SG]</ta>
            <ta e="T63" id="Seg_2378" s="T62">traurig.sein-PTCP.PRS.[NOM]</ta>
            <ta e="T64" id="Seg_2379" s="T63">ähnlich</ta>
            <ta e="T65" id="Seg_2380" s="T64">sein-PST2.[3SG]</ta>
            <ta e="T66" id="Seg_2381" s="T65">1PL.[NOM]</ta>
            <ta e="T67" id="Seg_2382" s="T66">Sünde-PL-ACC</ta>
            <ta e="T68" id="Seg_2383" s="T67">machen-PTCP.PST-1PL.[NOM]</ta>
            <ta e="T69" id="Seg_2384" s="T68">groß-ADVZ</ta>
            <ta e="T70" id="Seg_2385" s="T69">herunterziehen-PTCP.PRS</ta>
            <ta e="T71" id="Seg_2386" s="T70">sein-PST2.[3SG]</ta>
            <ta e="T72" id="Seg_2387" s="T71">3SG-ACC</ta>
            <ta e="T73" id="Seg_2388" s="T72">Jesus.[NOM]</ta>
            <ta e="T74" id="Seg_2389" s="T73">sagen-PST2.[3SG]</ta>
            <ta e="T75" id="Seg_2390" s="T74">lernen-PTCP.PRS</ta>
            <ta e="T76" id="Seg_2391" s="T75">Leute-3SG-DAT/LOC</ta>
            <ta e="T77" id="Seg_2392" s="T76">1SG.[NOM]</ta>
            <ta e="T78" id="Seg_2393" s="T77">Seele-EP-1SG.[NOM]</ta>
            <ta e="T79" id="Seg_2394" s="T78">sterben-PTCP.PRS.[NOM]</ta>
            <ta e="T80" id="Seg_2395" s="T79">ähnlich</ta>
            <ta e="T81" id="Seg_2396" s="T80">traurig.sein-PST1-3SG</ta>
            <ta e="T82" id="Seg_2397" s="T81">sein-CVB.SIM</ta>
            <ta e="T83" id="Seg_2398" s="T82">fallen-EP-IMP.2PL</ta>
            <ta e="T84" id="Seg_2399" s="T83">1SG-ACC</ta>
            <ta e="T85" id="Seg_2400" s="T84">mit</ta>
            <ta e="T86" id="Seg_2401" s="T85">Gewohnheit-SIM</ta>
            <ta e="T87" id="Seg_2402" s="T86">leben-IMP.1PL</ta>
            <ta e="T88" id="Seg_2403" s="T87">3PL-ABL</ta>
            <ta e="T89" id="Seg_2404" s="T88">weggehen-CVB.SIM</ta>
            <ta e="T90" id="Seg_2405" s="T89">fallen-CVB.ANT</ta>
            <ta e="T91" id="Seg_2406" s="T90">3SG.[NOM]</ta>
            <ta e="T92" id="Seg_2407" s="T91">Knie-SIM</ta>
            <ta e="T93" id="Seg_2408" s="T92">sich.setzen-CVB.SEQ</ta>
            <ta e="T94" id="Seg_2409" s="T93">beten-PST2.[3SG]</ta>
            <ta e="T95" id="Seg_2410" s="T94">Gott-DAT/LOC</ta>
            <ta e="T96" id="Seg_2411" s="T95">Vater.[NOM]</ta>
            <ta e="T97" id="Seg_2412" s="T96">oh</ta>
            <ta e="T98" id="Seg_2413" s="T97">2SG.[NOM]</ta>
            <ta e="T99" id="Seg_2414" s="T98">einverstanden.sein-PTCP.PRS-EP-2SG.[NOM]</ta>
            <ta e="T100" id="Seg_2415" s="T99">sein-PST2-3SG</ta>
            <ta e="T101" id="Seg_2416" s="T100">sein-COND.[3SG]</ta>
            <ta e="T102" id="Seg_2417" s="T101">lassen-CVB.PURP</ta>
            <ta e="T103" id="Seg_2418" s="T102">1SG-ACC</ta>
            <ta e="T104" id="Seg_2419" s="T103">Fehler.[NOM]</ta>
            <ta e="T105" id="Seg_2420" s="T104">dieses</ta>
            <ta e="T106" id="Seg_2421" s="T105">Tasse-ACC</ta>
            <ta e="T107" id="Seg_2422" s="T106">dieses.[NOM]</ta>
            <ta e="T108" id="Seg_2423" s="T107">doch</ta>
            <ta e="T109" id="Seg_2424" s="T108">sein-NEG.[3SG]</ta>
            <ta e="T110" id="Seg_2425" s="T109">1SG.[NOM]</ta>
            <ta e="T113" id="Seg_2426" s="T112">Wunsch-1SG-INSTR</ta>
            <ta e="T114" id="Seg_2427" s="T113">sein-IMP.3SG</ta>
            <ta e="T115" id="Seg_2428" s="T114">2SG.[NOM]</ta>
            <ta e="T116" id="Seg_2429" s="T115">nur</ta>
            <ta e="T117" id="Seg_2430" s="T116">Wunsch-2SG-INSTR</ta>
            <ta e="T118" id="Seg_2431" s="T117">Himmel-PL-ABL</ta>
            <ta e="T119" id="Seg_2432" s="T118">zu.sehen.sein-CVB.SEQ</ta>
            <ta e="T120" id="Seg_2433" s="T119">kommen-PST2.[3SG]</ta>
            <ta e="T121" id="Seg_2434" s="T120">3SG-DAT/LOC</ta>
            <ta e="T122" id="Seg_2435" s="T121">Engel.[NOM]</ta>
            <ta e="T123" id="Seg_2436" s="T122">3SG.[NOM]</ta>
            <ta e="T124" id="Seg_2437" s="T123">Seele-3SG-ACC</ta>
            <ta e="T125" id="Seg_2438" s="T124">bestärken-PTCP.PRS</ta>
            <ta e="T126" id="Seg_2439" s="T125">sein-PST2.[3SG]</ta>
            <ta e="T127" id="Seg_2440" s="T126">Jesus.[NOM]</ta>
            <ta e="T128" id="Seg_2441" s="T127">sehr</ta>
            <ta e="T129" id="Seg_2442" s="T128">Qual-VBZ-PTCP.PRS.[NOM]</ta>
            <ta e="T130" id="Seg_2443" s="T129">ähnlich</ta>
            <ta e="T131" id="Seg_2444" s="T130">letzter-3SG-INSTR</ta>
            <ta e="T132" id="Seg_2445" s="T131">beten-PTCP.PRS</ta>
            <ta e="T133" id="Seg_2446" s="T132">werden-PST2.[3SG]</ta>
            <ta e="T134" id="Seg_2447" s="T133">3SG.[NOM]</ta>
            <ta e="T135" id="Seg_2448" s="T134">Schweiß-3SG.[NOM]</ta>
            <ta e="T136" id="Seg_2449" s="T135">nur</ta>
            <ta e="T139" id="Seg_2450" s="T138">fließen-PRS.[3SG]</ta>
            <ta e="T140" id="Seg_2451" s="T139">Blut-SIM</ta>
            <ta e="T141" id="Seg_2452" s="T140">Erde-DAT/LOC</ta>
            <ta e="T142" id="Seg_2453" s="T141">ausgießen-CAUS-PRS.[3SG]</ta>
            <ta e="T143" id="Seg_2454" s="T142">lernen-PTCP.PRS</ta>
            <ta e="T144" id="Seg_2455" s="T143">Leute-3SG-DAT/LOC</ta>
            <ta e="T145" id="Seg_2456" s="T144">zurückkommen-CVB.SEQ</ta>
            <ta e="T146" id="Seg_2457" s="T145">kommen-CVB.SEQ</ta>
            <ta e="T147" id="Seg_2458" s="T146">3SG.[NOM]</ta>
            <ta e="T148" id="Seg_2459" s="T147">finden-PST2.[3SG]</ta>
            <ta e="T149" id="Seg_2460" s="T148">jenes-3SG-3PL.[NOM]</ta>
            <ta e="T150" id="Seg_2461" s="T149">schlafen-CVB.SIM</ta>
            <ta e="T151" id="Seg_2462" s="T150">liegen-PRS-3PL</ta>
            <ta e="T152" id="Seg_2463" s="T151">jenes-PL-ACC</ta>
            <ta e="T153" id="Seg_2464" s="T152">aufwachen-CAUS-CVB.ANT</ta>
            <ta e="T154" id="Seg_2465" s="T153">sprechen-PST2.[3SG]</ta>
            <ta e="T155" id="Seg_2466" s="T154">wie</ta>
            <ta e="T156" id="Seg_2467" s="T155">2PL.[NOM]</ta>
            <ta e="T157" id="Seg_2468" s="T156">eins</ta>
            <ta e="T158" id="Seg_2469" s="T157">Stunde.[NOM]</ta>
            <ta e="T159" id="Seg_2470" s="T158">ungefähr-3SG-DAT/LOC</ta>
            <ta e="T160" id="Seg_2471" s="T159">sitzen-NEG.PTCP</ta>
            <ta e="T161" id="Seg_2472" s="T160">sein-PST1-2PL</ta>
            <ta e="T162" id="Seg_2473" s="T161">1SG-ACC</ta>
            <ta e="T163" id="Seg_2474" s="T162">mit</ta>
            <ta e="T164" id="Seg_2475" s="T163">sitzen-EP-IMP.2PL</ta>
            <ta e="T165" id="Seg_2476" s="T164">beten-EP-IMP.2PL</ta>
            <ta e="T166" id="Seg_2477" s="T165">schlecht.[NOM]</ta>
            <ta e="T167" id="Seg_2478" s="T166">zu</ta>
            <ta e="T168" id="Seg_2479" s="T167">ziehen-EP-CAUS-EP-NEG-PTCP.FUT-2PL-ACC</ta>
            <ta e="T169" id="Seg_2480" s="T168">Seele.[NOM]</ta>
            <ta e="T170" id="Seg_2481" s="T169">munter.[NOM]</ta>
            <ta e="T171" id="Seg_2482" s="T170">sein-HAB.[3SG]</ta>
            <ta e="T172" id="Seg_2483" s="T171">Fleisch.[NOM]</ta>
            <ta e="T173" id="Seg_2484" s="T172">aber</ta>
            <ta e="T174" id="Seg_2485" s="T173">Kraft-POSS</ta>
            <ta e="T175" id="Seg_2486" s="T174">NEG.[3SG]</ta>
            <ta e="T176" id="Seg_2487" s="T175">3PL-ABL</ta>
            <ta e="T177" id="Seg_2488" s="T176">weggehen-CVB.SEQ</ta>
            <ta e="T178" id="Seg_2489" s="T177">nachdem</ta>
            <ta e="T179" id="Seg_2490" s="T178">3SG.[NOM]</ta>
            <ta e="T180" id="Seg_2491" s="T179">wieder</ta>
            <ta e="T181" id="Seg_2492" s="T180">beten-PST2.[3SG]</ta>
            <ta e="T182" id="Seg_2493" s="T181">sprechen-CVB.SEQ</ta>
            <ta e="T183" id="Seg_2494" s="T182">1SG.[NOM]</ta>
            <ta e="T184" id="Seg_2495" s="T183">Vater-1SG.[NOM]</ta>
            <ta e="T185" id="Seg_2496" s="T184">dieses.[NOM]</ta>
            <ta e="T186" id="Seg_2497" s="T185">Tasse.[NOM]</ta>
            <ta e="T187" id="Seg_2498" s="T186">1SG.[NOM]</ta>
            <ta e="T188" id="Seg_2499" s="T187">Platz.neben-1SG-INSTR</ta>
            <ta e="T189" id="Seg_2500" s="T188">vorbeigehen-PTCP.PRS</ta>
            <ta e="T190" id="Seg_2501" s="T189">sein-TEMP-3SG</ta>
            <ta e="T191" id="Seg_2502" s="T190">1SG.[NOM]</ta>
            <ta e="T192" id="Seg_2503" s="T191">trinken-PTCP.FUT-1SG-ACC</ta>
            <ta e="T193" id="Seg_2504" s="T192">sein-IMP.3SG</ta>
            <ta e="T194" id="Seg_2505" s="T193">2SG.[NOM]</ta>
            <ta e="T195" id="Seg_2506" s="T194">Wunsch-2SG.[NOM]</ta>
            <ta e="T196" id="Seg_2507" s="T195">ähnlich</ta>
            <ta e="T197" id="Seg_2508" s="T196">dieses</ta>
            <ta e="T198" id="Seg_2509" s="T197">Gebet-DAT/LOC</ta>
            <ta e="T199" id="Seg_2510" s="T198">Christus.[NOM]</ta>
            <ta e="T200" id="Seg_2511" s="T199">sprechen-PRS.[3SG]</ta>
            <ta e="T201" id="Seg_2512" s="T200">Qual-VBZ-PTCP.PRS</ta>
            <ta e="T202" id="Seg_2513" s="T201">Tasse.[NOM]</ta>
            <ta e="T203" id="Seg_2514" s="T202">Seite-3SG-INSTR</ta>
            <ta e="T204" id="Seg_2515" s="T203">was-ACC</ta>
            <ta e="T205" id="Seg_2516" s="T204">3SG.[NOM]</ta>
            <ta e="T206" id="Seg_2517" s="T205">trinken-PTCP.FUT</ta>
            <ta e="T207" id="Seg_2518" s="T206">fallen-NEC.[3SG]</ta>
            <ta e="T208" id="Seg_2519" s="T207">1PL.[NOM]</ta>
            <ta e="T209" id="Seg_2520" s="T208">Sünde-PL-ACC</ta>
            <ta e="T210" id="Seg_2521" s="T209">machen-PTCP.PST-1PL.[NOM]</ta>
            <ta e="T211" id="Seg_2522" s="T210">wegen</ta>
            <ta e="T212" id="Seg_2523" s="T211">wer.[NOM]</ta>
            <ta e="T213" id="Seg_2524" s="T212">NEG</ta>
            <ta e="T214" id="Seg_2525" s="T213">wissen-NEG.[3SG]</ta>
            <ta e="T215" id="Seg_2526" s="T214">Mensch.[NOM]</ta>
            <ta e="T216" id="Seg_2527" s="T215">Seele-3SG</ta>
            <ta e="T217" id="Seg_2528" s="T216">NEG</ta>
            <ta e="T218" id="Seg_2529" s="T217">schaffen-NEG.[3SG]</ta>
            <ta e="T219" id="Seg_2530" s="T218">jenes</ta>
            <ta e="T220" id="Seg_2531" s="T219">Qual-VBZ-PTCP.PRS</ta>
            <ta e="T221" id="Seg_2532" s="T220">schwer-3SG-ACC</ta>
            <ta e="T222" id="Seg_2533" s="T221">Gott.[NOM]</ta>
            <ta e="T223" id="Seg_2534" s="T222">Sohn-3SG.[NOM]</ta>
            <ta e="T224" id="Seg_2535" s="T223">dieses-ACC</ta>
            <ta e="T225" id="Seg_2536" s="T224">nehmen-RECP/COLL-EP-PST2-3SG</ta>
            <ta e="T226" id="Seg_2537" s="T225">3SG.[NOM]</ta>
            <ta e="T227" id="Seg_2538" s="T226">wissen-PTCP.PRS</ta>
            <ta e="T228" id="Seg_2539" s="T227">sein-PST1-3SG</ta>
            <ta e="T229" id="Seg_2540" s="T228">selbst-3SG-GEN</ta>
            <ta e="T230" id="Seg_2541" s="T229">Seele-3SG-ACC</ta>
            <ta e="T231" id="Seg_2542" s="T230">geben-PRS.[3SG]</ta>
            <ta e="T232" id="Seg_2543" s="T231">1PL-ACC</ta>
            <ta e="T233" id="Seg_2544" s="T232">retten-CVB.PURP</ta>
            <ta e="T234" id="Seg_2545" s="T233">wer.[NOM]</ta>
            <ta e="T235" id="Seg_2546" s="T234">NEG</ta>
            <ta e="T236" id="Seg_2547" s="T235">sein-IMP.3SG</ta>
            <ta e="T237" id="Seg_2548" s="T236">3SG.[NOM]</ta>
            <ta e="T238" id="Seg_2549" s="T237">gehen-PST2.NEG-3SG</ta>
            <ta e="T239" id="Seg_2550" s="T238">sein-COND.[3SG]</ta>
            <ta e="T240" id="Seg_2551" s="T239">jenes</ta>
            <ta e="T241" id="Seg_2552" s="T240">Tasse-ABL</ta>
            <ta e="T242" id="Seg_2553" s="T241">1PL.[NOM]</ta>
            <ta e="T243" id="Seg_2554" s="T242">Sünde-PL-ACC</ta>
            <ta e="T244" id="Seg_2555" s="T243">machen-PTCP.PST-1PL.[NOM]</ta>
            <ta e="T245" id="Seg_2556" s="T244">immer</ta>
            <ta e="T246" id="Seg_2557" s="T245">bleiben-PTCP.PRS</ta>
            <ta e="T247" id="Seg_2558" s="T246">sein-PST1-3PL</ta>
            <ta e="T248" id="Seg_2559" s="T247">2PL.[NOM]</ta>
            <ta e="T249" id="Seg_2560" s="T248">hören-EP-PST2-2PL</ta>
            <ta e="T250" id="Seg_2561" s="T249">Erzählung-PL-ACC</ta>
            <ta e="T251" id="Seg_2562" s="T250">Jesus.[NOM]</ta>
            <ta e="T252" id="Seg_2563" s="T251">Christus.[NOM]</ta>
            <ta e="T253" id="Seg_2564" s="T252">Kind-PL.[NOM]</ta>
            <ta e="T254" id="Seg_2565" s="T253">Freund-PL-3SG.[NOM]</ta>
            <ta e="T255" id="Seg_2566" s="T254">sagen-CVB.SEQ</ta>
            <ta e="T256" id="Seg_2567" s="T255">Buch-ABL</ta>
            <ta e="T257" id="Seg_2568" s="T256">lesen-PST2-3SG</ta>
            <ta e="T258" id="Seg_2569" s="T257">übersetzen-PST2-3SG</ta>
            <ta e="T259" id="Seg_2570" s="T258">Popov.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_2571" s="T0">следующий</ta>
            <ta e="T2" id="Seg_2572" s="T1">рассказ-1PL.[NOM]</ta>
            <ta e="T3" id="Seg_2573" s="T2">этот</ta>
            <ta e="T4" id="Seg_2574" s="T3">имя-3SG.[NOM]</ta>
            <ta e="T5" id="Seg_2575" s="T4">быть-PRS.[3SG]</ta>
            <ta e="T6" id="Seg_2576" s="T5">Гефсемания.[NOM]</ta>
            <ta e="T7" id="Seg_2577" s="T6">стойбище-DAT/LOC</ta>
            <ta e="T8" id="Seg_2578" s="T7">молитва.[NOM]</ta>
            <ta e="T9" id="Seg_2579" s="T8">читать-PRS.[3SG]</ta>
            <ta e="T10" id="Seg_2580" s="T9">тот.EMPH</ta>
            <ta e="T11" id="Seg_2581" s="T10">ночь.[NOM]</ta>
            <ta e="T12" id="Seg_2582" s="T11">вечер.[NOM]</ta>
            <ta e="T13" id="Seg_2583" s="T12">встречать-NMNZ.[NOM]</ta>
            <ta e="T14" id="Seg_2584" s="T13">задняя.часть-3SG-DAT/LOC</ta>
            <ta e="T15" id="Seg_2585" s="T14">Исус.[NOM]</ta>
            <ta e="T16" id="Seg_2586" s="T15">десять</ta>
            <ta e="T17" id="Seg_2587" s="T16">один</ta>
            <ta e="T18" id="Seg_2588" s="T17">учить-PTCP.PRS</ta>
            <ta e="T19" id="Seg_2589" s="T18">человек-PL-3SG-ACC</ta>
            <ta e="T20" id="Seg_2590" s="T19">с</ta>
            <ta e="T21" id="Seg_2591" s="T20">выйти-EP-PST2.[3SG]</ta>
            <ta e="T22" id="Seg_2592" s="T21">Иерусалим-ABL</ta>
            <ta e="T23" id="Seg_2593" s="T22">Кидрон</ta>
            <ta e="T24" id="Seg_2594" s="T23">имя-PROPR</ta>
            <ta e="T25" id="Seg_2595" s="T24">производить.сильный.шум-CVB.SIM</ta>
            <ta e="T26" id="Seg_2596" s="T25">стоять-PTCP.PRS</ta>
            <ta e="T27" id="Seg_2597" s="T26">вода-ACC</ta>
            <ta e="T28" id="Seg_2598" s="T27">выйти-CVB.SEQ</ta>
            <ta e="T29" id="Seg_2599" s="T28">приходить-PST2.[3SG]</ta>
            <ta e="T30" id="Seg_2600" s="T29">Иерусалим.[NOM]</ta>
            <ta e="T31" id="Seg_2601" s="T30">место.около-DAT/LOC</ta>
            <ta e="T32" id="Seg_2602" s="T31">есть</ta>
            <ta e="T33" id="Seg_2603" s="T32">Гефсемания.[NOM]</ta>
            <ta e="T34" id="Seg_2604" s="T33">стойбище-DAT/LOC</ta>
            <ta e="T35" id="Seg_2605" s="T34">там</ta>
            <ta e="T36" id="Seg_2606" s="T35">Исус.[NOM]</ta>
            <ta e="T37" id="Seg_2607" s="T36">учить-PTCP.PRS</ta>
            <ta e="T38" id="Seg_2608" s="T37">народ-3SG-ACC</ta>
            <ta e="T39" id="Seg_2609" s="T38">с</ta>
            <ta e="T40" id="Seg_2610" s="T39">густой</ta>
            <ta e="T41" id="Seg_2611" s="T40">лес-DAT/LOC</ta>
            <ta e="T42" id="Seg_2612" s="T41">входить-CVB.SEQ</ta>
            <ta e="T43" id="Seg_2613" s="T42">говорить-PST2.[3SG]</ta>
            <ta e="T44" id="Seg_2614" s="T43">3PL-DAT/LOC</ta>
            <ta e="T45" id="Seg_2615" s="T44">сесть-CVB.SIM</ta>
            <ta e="T46" id="Seg_2616" s="T45">падать-EP-IMP.2PL</ta>
            <ta e="T47" id="Seg_2617" s="T46">здесь</ta>
            <ta e="T48" id="Seg_2618" s="T47">1SG.[NOM]</ta>
            <ta e="T49" id="Seg_2619" s="T48">идти-CVB.SIM</ta>
            <ta e="T50" id="Seg_2620" s="T49">падать-FUT-1SG</ta>
            <ta e="T51" id="Seg_2621" s="T50">Бог-DAT/LOC</ta>
            <ta e="T52" id="Seg_2622" s="T51">молиться-CVB.SIM</ta>
            <ta e="T53" id="Seg_2623" s="T52">сам-3SG-ACC</ta>
            <ta e="T54" id="Seg_2624" s="T53">с</ta>
            <ta e="T55" id="Seg_2625" s="T54">взять-PST2.[3SG]</ta>
            <ta e="T56" id="Seg_2626" s="T55">Петр-ACC</ta>
            <ta e="T57" id="Seg_2627" s="T56">Яков-ACC</ta>
            <ta e="T58" id="Seg_2628" s="T57">Иван-ACC</ta>
            <ta e="T59" id="Seg_2629" s="T58">3SG.[NOM]</ta>
            <ta e="T60" id="Seg_2630" s="T59">почему</ta>
            <ta e="T61" id="Seg_2631" s="T60">INDEF</ta>
            <ta e="T62" id="Seg_2632" s="T61">задуматься-PRS.[3SG]</ta>
            <ta e="T63" id="Seg_2633" s="T62">грустить-PTCP.PRS.[NOM]</ta>
            <ta e="T64" id="Seg_2634" s="T63">подобно</ta>
            <ta e="T65" id="Seg_2635" s="T64">быть-PST2.[3SG]</ta>
            <ta e="T66" id="Seg_2636" s="T65">1PL.[NOM]</ta>
            <ta e="T67" id="Seg_2637" s="T66">грех-PL-ACC</ta>
            <ta e="T68" id="Seg_2638" s="T67">делать-PTCP.PST-1PL.[NOM]</ta>
            <ta e="T69" id="Seg_2639" s="T68">большой-ADVZ</ta>
            <ta e="T70" id="Seg_2640" s="T69">перевешивать-PTCP.PRS</ta>
            <ta e="T71" id="Seg_2641" s="T70">быть-PST2.[3SG]</ta>
            <ta e="T72" id="Seg_2642" s="T71">3SG-ACC</ta>
            <ta e="T73" id="Seg_2643" s="T72">Исус.[NOM]</ta>
            <ta e="T74" id="Seg_2644" s="T73">говорить-PST2.[3SG]</ta>
            <ta e="T75" id="Seg_2645" s="T74">учить-PTCP.PRS</ta>
            <ta e="T76" id="Seg_2646" s="T75">люди-3SG-DAT/LOC</ta>
            <ta e="T77" id="Seg_2647" s="T76">1SG.[NOM]</ta>
            <ta e="T78" id="Seg_2648" s="T77">душа-EP-1SG.[NOM]</ta>
            <ta e="T79" id="Seg_2649" s="T78">умирать-PTCP.PRS.[NOM]</ta>
            <ta e="T80" id="Seg_2650" s="T79">подобно</ta>
            <ta e="T81" id="Seg_2651" s="T80">грустить-PST1-3SG</ta>
            <ta e="T82" id="Seg_2652" s="T81">быть-CVB.SIM</ta>
            <ta e="T83" id="Seg_2653" s="T82">падать-EP-IMP.2PL</ta>
            <ta e="T84" id="Seg_2654" s="T83">1SG-ACC</ta>
            <ta e="T85" id="Seg_2655" s="T84">с</ta>
            <ta e="T86" id="Seg_2656" s="T85">обыкновение-SIM</ta>
            <ta e="T87" id="Seg_2657" s="T86">жить-IMP.1PL</ta>
            <ta e="T88" id="Seg_2658" s="T87">3PL-ABL</ta>
            <ta e="T89" id="Seg_2659" s="T88">уйти-CVB.SIM</ta>
            <ta e="T90" id="Seg_2660" s="T89">падать-CVB.ANT</ta>
            <ta e="T91" id="Seg_2661" s="T90">3SG.[NOM]</ta>
            <ta e="T92" id="Seg_2662" s="T91">колено-SIM</ta>
            <ta e="T93" id="Seg_2663" s="T92">сесть-CVB.SEQ</ta>
            <ta e="T94" id="Seg_2664" s="T93">молиться-PST2.[3SG]</ta>
            <ta e="T95" id="Seg_2665" s="T94">Бог-DAT/LOC</ta>
            <ta e="T96" id="Seg_2666" s="T95">отец.[NOM]</ta>
            <ta e="T97" id="Seg_2667" s="T96">о</ta>
            <ta e="T98" id="Seg_2668" s="T97">2SG.[NOM]</ta>
            <ta e="T99" id="Seg_2669" s="T98">согласить-PTCP.PRS-EP-2SG.[NOM]</ta>
            <ta e="T100" id="Seg_2670" s="T99">быть-PST2-3SG</ta>
            <ta e="T101" id="Seg_2671" s="T100">быть-COND.[3SG]</ta>
            <ta e="T102" id="Seg_2672" s="T101">пустить-CVB.PURP</ta>
            <ta e="T103" id="Seg_2673" s="T102">1SG-ACC</ta>
            <ta e="T104" id="Seg_2674" s="T103">ошибка.[NOM]</ta>
            <ta e="T105" id="Seg_2675" s="T104">этот</ta>
            <ta e="T106" id="Seg_2676" s="T105">чашка-ACC</ta>
            <ta e="T107" id="Seg_2677" s="T106">тот.[NOM]</ta>
            <ta e="T108" id="Seg_2678" s="T107">ведь</ta>
            <ta e="T109" id="Seg_2679" s="T108">быть-NEG.[3SG]</ta>
            <ta e="T110" id="Seg_2680" s="T109">1SG.[NOM]</ta>
            <ta e="T113" id="Seg_2681" s="T112">желание-1SG-INSTR</ta>
            <ta e="T114" id="Seg_2682" s="T113">быть-IMP.3SG</ta>
            <ta e="T115" id="Seg_2683" s="T114">2SG.[NOM]</ta>
            <ta e="T116" id="Seg_2684" s="T115">только</ta>
            <ta e="T117" id="Seg_2685" s="T116">желание-2SG-INSTR</ta>
            <ta e="T118" id="Seg_2686" s="T117">небо-PL-ABL</ta>
            <ta e="T119" id="Seg_2687" s="T118">быть.видно-CVB.SEQ</ta>
            <ta e="T120" id="Seg_2688" s="T119">приходить-PST2.[3SG]</ta>
            <ta e="T121" id="Seg_2689" s="T120">3SG-DAT/LOC</ta>
            <ta e="T122" id="Seg_2690" s="T121">ангел.[NOM]</ta>
            <ta e="T123" id="Seg_2691" s="T122">3SG.[NOM]</ta>
            <ta e="T124" id="Seg_2692" s="T123">душа-3SG-ACC</ta>
            <ta e="T125" id="Seg_2693" s="T124">подкреплять-PTCP.PRS</ta>
            <ta e="T126" id="Seg_2694" s="T125">быть-PST2.[3SG]</ta>
            <ta e="T127" id="Seg_2695" s="T126">Исус.[NOM]</ta>
            <ta e="T128" id="Seg_2696" s="T127">очень</ta>
            <ta e="T129" id="Seg_2697" s="T128">мука-VBZ-PTCP.PRS.[NOM]</ta>
            <ta e="T130" id="Seg_2698" s="T129">подобно</ta>
            <ta e="T131" id="Seg_2699" s="T130">последний-3SG-INSTR</ta>
            <ta e="T132" id="Seg_2700" s="T131">молиться-PTCP.PRS</ta>
            <ta e="T133" id="Seg_2701" s="T132">становиться-PST2.[3SG]</ta>
            <ta e="T134" id="Seg_2702" s="T133">3SG.[NOM]</ta>
            <ta e="T135" id="Seg_2703" s="T134">пот-3SG.[NOM]</ta>
            <ta e="T136" id="Seg_2704" s="T135">только</ta>
            <ta e="T139" id="Seg_2705" s="T138">течь-PRS.[3SG]</ta>
            <ta e="T140" id="Seg_2706" s="T139">кровь-SIM</ta>
            <ta e="T141" id="Seg_2707" s="T140">земля-DAT/LOC</ta>
            <ta e="T142" id="Seg_2708" s="T141">выливать-CAUS-PRS.[3SG]</ta>
            <ta e="T143" id="Seg_2709" s="T142">учить-PTCP.PRS</ta>
            <ta e="T144" id="Seg_2710" s="T143">люди-3SG-DAT/LOC</ta>
            <ta e="T145" id="Seg_2711" s="T144">возвращаться-CVB.SEQ</ta>
            <ta e="T146" id="Seg_2712" s="T145">приходить-CVB.SEQ</ta>
            <ta e="T147" id="Seg_2713" s="T146">3SG.[NOM]</ta>
            <ta e="T148" id="Seg_2714" s="T147">найти-PST2.[3SG]</ta>
            <ta e="T149" id="Seg_2715" s="T148">тот-3SG-3PL.[NOM]</ta>
            <ta e="T150" id="Seg_2716" s="T149">спать-CVB.SIM</ta>
            <ta e="T151" id="Seg_2717" s="T150">лежать-PRS-3PL</ta>
            <ta e="T152" id="Seg_2718" s="T151">тот-PL-ACC</ta>
            <ta e="T153" id="Seg_2719" s="T152">просыпаться-CAUS-CVB.ANT</ta>
            <ta e="T154" id="Seg_2720" s="T153">говорить-PST2.[3SG]</ta>
            <ta e="T155" id="Seg_2721" s="T154">как</ta>
            <ta e="T156" id="Seg_2722" s="T155">2PL.[NOM]</ta>
            <ta e="T157" id="Seg_2723" s="T156">один</ta>
            <ta e="T158" id="Seg_2724" s="T157">час.[NOM]</ta>
            <ta e="T159" id="Seg_2725" s="T158">около-3SG-DAT/LOC</ta>
            <ta e="T160" id="Seg_2726" s="T159">сидеть-NEG.PTCP</ta>
            <ta e="T161" id="Seg_2727" s="T160">быть-PST1-2PL</ta>
            <ta e="T162" id="Seg_2728" s="T161">1SG-ACC</ta>
            <ta e="T163" id="Seg_2729" s="T162">с</ta>
            <ta e="T164" id="Seg_2730" s="T163">сидеть-EP-IMP.2PL</ta>
            <ta e="T165" id="Seg_2731" s="T164">молиться-EP-IMP.2PL</ta>
            <ta e="T166" id="Seg_2732" s="T165">плохой.[NOM]</ta>
            <ta e="T167" id="Seg_2733" s="T166">к</ta>
            <ta e="T168" id="Seg_2734" s="T167">тянуть-EP-CAUS-EP-NEG-PTCP.FUT-2PL-ACC</ta>
            <ta e="T169" id="Seg_2735" s="T168">душа.[NOM]</ta>
            <ta e="T170" id="Seg_2736" s="T169">бодрый.[NOM]</ta>
            <ta e="T171" id="Seg_2737" s="T170">быть-HAB.[3SG]</ta>
            <ta e="T172" id="Seg_2738" s="T171">мясо.[NOM]</ta>
            <ta e="T173" id="Seg_2739" s="T172">однако</ta>
            <ta e="T174" id="Seg_2740" s="T173">сила-POSS</ta>
            <ta e="T175" id="Seg_2741" s="T174">NEG.[3SG]</ta>
            <ta e="T176" id="Seg_2742" s="T175">3PL-ABL</ta>
            <ta e="T177" id="Seg_2743" s="T176">уйти-CVB.SEQ</ta>
            <ta e="T178" id="Seg_2744" s="T177">после</ta>
            <ta e="T179" id="Seg_2745" s="T178">3SG.[NOM]</ta>
            <ta e="T180" id="Seg_2746" s="T179">опять</ta>
            <ta e="T181" id="Seg_2747" s="T180">молиться-PST2.[3SG]</ta>
            <ta e="T182" id="Seg_2748" s="T181">говорить-CVB.SEQ</ta>
            <ta e="T183" id="Seg_2749" s="T182">1SG.[NOM]</ta>
            <ta e="T184" id="Seg_2750" s="T183">отец-1SG.[NOM]</ta>
            <ta e="T185" id="Seg_2751" s="T184">тот.[NOM]</ta>
            <ta e="T186" id="Seg_2752" s="T185">чашка.[NOM]</ta>
            <ta e="T187" id="Seg_2753" s="T186">1SG.[NOM]</ta>
            <ta e="T188" id="Seg_2754" s="T187">место.около-1SG-INSTR</ta>
            <ta e="T189" id="Seg_2755" s="T188">проехать-PTCP.PRS</ta>
            <ta e="T190" id="Seg_2756" s="T189">быть-TEMP-3SG</ta>
            <ta e="T191" id="Seg_2757" s="T190">1SG.[NOM]</ta>
            <ta e="T192" id="Seg_2758" s="T191">пить-PTCP.FUT-1SG-ACC</ta>
            <ta e="T193" id="Seg_2759" s="T192">быть-IMP.3SG</ta>
            <ta e="T194" id="Seg_2760" s="T193">2SG.[NOM]</ta>
            <ta e="T195" id="Seg_2761" s="T194">желание-2SG.[NOM]</ta>
            <ta e="T196" id="Seg_2762" s="T195">подобно</ta>
            <ta e="T197" id="Seg_2763" s="T196">этот</ta>
            <ta e="T198" id="Seg_2764" s="T197">молитва-DAT/LOC</ta>
            <ta e="T199" id="Seg_2765" s="T198">Христос.[NOM]</ta>
            <ta e="T200" id="Seg_2766" s="T199">говорить-PRS.[3SG]</ta>
            <ta e="T201" id="Seg_2767" s="T200">мука-VBZ-PTCP.PRS</ta>
            <ta e="T202" id="Seg_2768" s="T201">чашка.[NOM]</ta>
            <ta e="T203" id="Seg_2769" s="T202">сторона-3SG-INSTR</ta>
            <ta e="T204" id="Seg_2770" s="T203">что-ACC</ta>
            <ta e="T205" id="Seg_2771" s="T204">3SG.[NOM]</ta>
            <ta e="T206" id="Seg_2772" s="T205">пить-PTCP.FUT</ta>
            <ta e="T207" id="Seg_2773" s="T206">падать-NEC.[3SG]</ta>
            <ta e="T208" id="Seg_2774" s="T207">1PL.[NOM]</ta>
            <ta e="T209" id="Seg_2775" s="T208">грех-PL-ACC</ta>
            <ta e="T210" id="Seg_2776" s="T209">делать-PTCP.PST-1PL.[NOM]</ta>
            <ta e="T211" id="Seg_2777" s="T210">из_за</ta>
            <ta e="T212" id="Seg_2778" s="T211">кто.[NOM]</ta>
            <ta e="T213" id="Seg_2779" s="T212">NEG</ta>
            <ta e="T214" id="Seg_2780" s="T213">знать-NEG.[3SG]</ta>
            <ta e="T215" id="Seg_2781" s="T214">человек.[NOM]</ta>
            <ta e="T216" id="Seg_2782" s="T215">душа-3SG</ta>
            <ta e="T217" id="Seg_2783" s="T216">NEG</ta>
            <ta e="T218" id="Seg_2784" s="T217">мочь-NEG.[3SG]</ta>
            <ta e="T219" id="Seg_2785" s="T218">тот</ta>
            <ta e="T220" id="Seg_2786" s="T219">мука-VBZ-PTCP.PRS</ta>
            <ta e="T221" id="Seg_2787" s="T220">трудный-3SG-ACC</ta>
            <ta e="T222" id="Seg_2788" s="T221">Бог.[NOM]</ta>
            <ta e="T223" id="Seg_2789" s="T222">сын-3SG.[NOM]</ta>
            <ta e="T224" id="Seg_2790" s="T223">тот-ACC</ta>
            <ta e="T225" id="Seg_2791" s="T224">взять-RECP/COLL-EP-PST2-3SG</ta>
            <ta e="T226" id="Seg_2792" s="T225">3SG.[NOM]</ta>
            <ta e="T227" id="Seg_2793" s="T226">знать-PTCP.PRS</ta>
            <ta e="T228" id="Seg_2794" s="T227">быть-PST1-3SG</ta>
            <ta e="T229" id="Seg_2795" s="T228">сам-3SG-GEN</ta>
            <ta e="T230" id="Seg_2796" s="T229">душа-3SG-ACC</ta>
            <ta e="T231" id="Seg_2797" s="T230">давать-PRS.[3SG]</ta>
            <ta e="T232" id="Seg_2798" s="T231">1PL-ACC</ta>
            <ta e="T233" id="Seg_2799" s="T232">спасать-CVB.PURP</ta>
            <ta e="T234" id="Seg_2800" s="T233">кто.[NOM]</ta>
            <ta e="T235" id="Seg_2801" s="T234">NEG</ta>
            <ta e="T236" id="Seg_2802" s="T235">быть-IMP.3SG</ta>
            <ta e="T237" id="Seg_2803" s="T236">3SG.[NOM]</ta>
            <ta e="T238" id="Seg_2804" s="T237">идти-PST2.NEG-3SG</ta>
            <ta e="T239" id="Seg_2805" s="T238">быть-COND.[3SG]</ta>
            <ta e="T240" id="Seg_2806" s="T239">тот</ta>
            <ta e="T241" id="Seg_2807" s="T240">чашка-ABL</ta>
            <ta e="T242" id="Seg_2808" s="T241">1PL.[NOM]</ta>
            <ta e="T243" id="Seg_2809" s="T242">грех-PL-ACC</ta>
            <ta e="T244" id="Seg_2810" s="T243">делать-PTCP.PST-1PL.[NOM]</ta>
            <ta e="T245" id="Seg_2811" s="T244">всегда</ta>
            <ta e="T246" id="Seg_2812" s="T245">оставаться-PTCP.PRS</ta>
            <ta e="T247" id="Seg_2813" s="T246">быть-PST1-3PL</ta>
            <ta e="T248" id="Seg_2814" s="T247">2PL.[NOM]</ta>
            <ta e="T249" id="Seg_2815" s="T248">слышать-EP-PST2-2PL</ta>
            <ta e="T250" id="Seg_2816" s="T249">рассказ-PL-ACC</ta>
            <ta e="T251" id="Seg_2817" s="T250">Исус.[NOM]</ta>
            <ta e="T252" id="Seg_2818" s="T251">Христос.[NOM]</ta>
            <ta e="T253" id="Seg_2819" s="T252">ребенок-PL.[NOM]</ta>
            <ta e="T254" id="Seg_2820" s="T253">друг-PL-3SG.[NOM]</ta>
            <ta e="T255" id="Seg_2821" s="T254">говорить-CVB.SEQ</ta>
            <ta e="T256" id="Seg_2822" s="T255">книга-ABL</ta>
            <ta e="T257" id="Seg_2823" s="T256">читать-PST2-3SG</ta>
            <ta e="T258" id="Seg_2824" s="T257">переводить-PST2-3SG</ta>
            <ta e="T259" id="Seg_2825" s="T258">Попов.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_2826" s="T0">adj</ta>
            <ta e="T2" id="Seg_2827" s="T1">n-n:(poss)-n:case</ta>
            <ta e="T3" id="Seg_2828" s="T2">dempro</ta>
            <ta e="T4" id="Seg_2829" s="T3">n-n:(poss)-n:case</ta>
            <ta e="T5" id="Seg_2830" s="T4">v-v:tense-v:pred.pn</ta>
            <ta e="T6" id="Seg_2831" s="T5">propr-n:case</ta>
            <ta e="T7" id="Seg_2832" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_2833" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_2834" s="T8">v-v:tense-v:pred.pn</ta>
            <ta e="T10" id="Seg_2835" s="T9">dempro</ta>
            <ta e="T11" id="Seg_2836" s="T10">n-n:case</ta>
            <ta e="T12" id="Seg_2837" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_2838" s="T12">v-v&gt;n-n:case</ta>
            <ta e="T14" id="Seg_2839" s="T13">n-n:poss-n:case</ta>
            <ta e="T15" id="Seg_2840" s="T14">propr-n:case</ta>
            <ta e="T16" id="Seg_2841" s="T15">cardnum</ta>
            <ta e="T17" id="Seg_2842" s="T16">cardnum</ta>
            <ta e="T18" id="Seg_2843" s="T17">v-v:ptcp</ta>
            <ta e="T19" id="Seg_2844" s="T18">n-n:(num)-n:poss-n:case</ta>
            <ta e="T20" id="Seg_2845" s="T19">post</ta>
            <ta e="T21" id="Seg_2846" s="T20">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T22" id="Seg_2847" s="T21">propr-n:case</ta>
            <ta e="T23" id="Seg_2848" s="T22">propr</ta>
            <ta e="T24" id="Seg_2849" s="T23">n-n&gt;adj</ta>
            <ta e="T25" id="Seg_2850" s="T24">v-v:cvb</ta>
            <ta e="T26" id="Seg_2851" s="T25">v-v:ptcp</ta>
            <ta e="T27" id="Seg_2852" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_2853" s="T27">v-v:cvb</ta>
            <ta e="T29" id="Seg_2854" s="T28">v-v:tense-v:pred.pn</ta>
            <ta e="T30" id="Seg_2855" s="T29">propr-n:case</ta>
            <ta e="T31" id="Seg_2856" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_2857" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_2858" s="T32">propr-n:case</ta>
            <ta e="T34" id="Seg_2859" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_2860" s="T34">adv</ta>
            <ta e="T36" id="Seg_2861" s="T35">propr-n:case</ta>
            <ta e="T37" id="Seg_2862" s="T36">v-v:ptcp</ta>
            <ta e="T38" id="Seg_2863" s="T37">n-n:poss-n:case</ta>
            <ta e="T39" id="Seg_2864" s="T38">post</ta>
            <ta e="T40" id="Seg_2865" s="T39">adj</ta>
            <ta e="T41" id="Seg_2866" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_2867" s="T41">v-v:cvb</ta>
            <ta e="T43" id="Seg_2868" s="T42">v-v:tense-v:pred.pn</ta>
            <ta e="T44" id="Seg_2869" s="T43">pers-pro:case</ta>
            <ta e="T45" id="Seg_2870" s="T44">v-v:cvb</ta>
            <ta e="T46" id="Seg_2871" s="T45">v-v:(ins)-v:mood.pn</ta>
            <ta e="T47" id="Seg_2872" s="T46">adv</ta>
            <ta e="T48" id="Seg_2873" s="T47">pers-pro:case</ta>
            <ta e="T49" id="Seg_2874" s="T48">v-v:cvb</ta>
            <ta e="T50" id="Seg_2875" s="T49">v-v:tense-v:poss.pn</ta>
            <ta e="T51" id="Seg_2876" s="T50">n-n:case</ta>
            <ta e="T52" id="Seg_2877" s="T51">v-v:cvb</ta>
            <ta e="T53" id="Seg_2878" s="T52">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T54" id="Seg_2879" s="T53">post</ta>
            <ta e="T55" id="Seg_2880" s="T54">v-v:tense-v:pred.pn</ta>
            <ta e="T56" id="Seg_2881" s="T55">propr-n:case</ta>
            <ta e="T57" id="Seg_2882" s="T56">propr-n:case</ta>
            <ta e="T58" id="Seg_2883" s="T57">propr-n:case</ta>
            <ta e="T59" id="Seg_2884" s="T58">pers-pro:case</ta>
            <ta e="T60" id="Seg_2885" s="T59">que</ta>
            <ta e="T61" id="Seg_2886" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_2887" s="T61">v-v:tense-v:pred.pn</ta>
            <ta e="T63" id="Seg_2888" s="T62">v-v:ptcp-v:(case)</ta>
            <ta e="T64" id="Seg_2889" s="T63">post</ta>
            <ta e="T65" id="Seg_2890" s="T64">v-v:tense-v:pred.pn</ta>
            <ta e="T66" id="Seg_2891" s="T65">pers-pro:case</ta>
            <ta e="T67" id="Seg_2892" s="T66">n-n:(num)-n:case</ta>
            <ta e="T68" id="Seg_2893" s="T67">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T69" id="Seg_2894" s="T68">adj-adj&gt;adv</ta>
            <ta e="T70" id="Seg_2895" s="T69">v-v:ptcp</ta>
            <ta e="T71" id="Seg_2896" s="T70">v-v:tense-v:pred.pn</ta>
            <ta e="T72" id="Seg_2897" s="T71">pers-pro:case</ta>
            <ta e="T73" id="Seg_2898" s="T72">propr-n:case</ta>
            <ta e="T74" id="Seg_2899" s="T73">v-v:tense-v:pred.pn</ta>
            <ta e="T75" id="Seg_2900" s="T74">v-v:ptcp</ta>
            <ta e="T76" id="Seg_2901" s="T75">n-n:poss-n:case</ta>
            <ta e="T77" id="Seg_2902" s="T76">pers-pro:case</ta>
            <ta e="T78" id="Seg_2903" s="T77">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T79" id="Seg_2904" s="T78">v-v:ptcp-v:(case)</ta>
            <ta e="T80" id="Seg_2905" s="T79">post</ta>
            <ta e="T81" id="Seg_2906" s="T80">v-v:tense-v:poss.pn</ta>
            <ta e="T82" id="Seg_2907" s="T81">v-v:cvb</ta>
            <ta e="T83" id="Seg_2908" s="T82">v-v:(ins)-v:mood.pn</ta>
            <ta e="T84" id="Seg_2909" s="T83">pers-pro:case</ta>
            <ta e="T85" id="Seg_2910" s="T84">post</ta>
            <ta e="T86" id="Seg_2911" s="T85">n-n&gt;adv</ta>
            <ta e="T87" id="Seg_2912" s="T86">v-v:mood.pn</ta>
            <ta e="T88" id="Seg_2913" s="T87">pers-pro:case</ta>
            <ta e="T89" id="Seg_2914" s="T88">v-v:cvb</ta>
            <ta e="T90" id="Seg_2915" s="T89">v-v:cvb</ta>
            <ta e="T91" id="Seg_2916" s="T90">pers-pro:case</ta>
            <ta e="T92" id="Seg_2917" s="T91">n-n&gt;adv</ta>
            <ta e="T93" id="Seg_2918" s="T92">v-v:cvb</ta>
            <ta e="T94" id="Seg_2919" s="T93">v-v:tense-v:pred.pn</ta>
            <ta e="T95" id="Seg_2920" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_2921" s="T95">n-n:case</ta>
            <ta e="T97" id="Seg_2922" s="T96">interj</ta>
            <ta e="T98" id="Seg_2923" s="T97">pers-pro:case</ta>
            <ta e="T99" id="Seg_2924" s="T98">v-v:ptcp-v:(ins)-v:(poss)-v:(case)</ta>
            <ta e="T100" id="Seg_2925" s="T99">v-v:tense-v:poss.pn</ta>
            <ta e="T101" id="Seg_2926" s="T100">v-v:mood-v:pred.pn</ta>
            <ta e="T102" id="Seg_2927" s="T101">v-v:cvb</ta>
            <ta e="T103" id="Seg_2928" s="T102">pers-pro:case</ta>
            <ta e="T104" id="Seg_2929" s="T103">n-n:case</ta>
            <ta e="T105" id="Seg_2930" s="T104">dempro</ta>
            <ta e="T106" id="Seg_2931" s="T105">n-n:case</ta>
            <ta e="T107" id="Seg_2932" s="T106">dempro-pro:case</ta>
            <ta e="T108" id="Seg_2933" s="T107">ptcl</ta>
            <ta e="T109" id="Seg_2934" s="T108">v-v:(neg)-v:pred.pn</ta>
            <ta e="T110" id="Seg_2935" s="T109">pers-pro:case</ta>
            <ta e="T113" id="Seg_2936" s="T112">n-n:poss-n:case</ta>
            <ta e="T114" id="Seg_2937" s="T113">v-v:mood.pn</ta>
            <ta e="T115" id="Seg_2938" s="T114">pers-pro:case</ta>
            <ta e="T116" id="Seg_2939" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_2940" s="T116">n-n:poss-n:case</ta>
            <ta e="T118" id="Seg_2941" s="T117">n-n:(num)-n:case</ta>
            <ta e="T119" id="Seg_2942" s="T118">v-v:cvb</ta>
            <ta e="T120" id="Seg_2943" s="T119">v-v:tense-v:pred.pn</ta>
            <ta e="T121" id="Seg_2944" s="T120">pers-pro:case</ta>
            <ta e="T122" id="Seg_2945" s="T121">n-n:case</ta>
            <ta e="T123" id="Seg_2946" s="T122">pers-pro:case</ta>
            <ta e="T124" id="Seg_2947" s="T123">n-n:poss-n:case</ta>
            <ta e="T125" id="Seg_2948" s="T124">v-v:ptcp</ta>
            <ta e="T126" id="Seg_2949" s="T125">v-v:tense-v:pred.pn</ta>
            <ta e="T127" id="Seg_2950" s="T126">propr-n:case</ta>
            <ta e="T128" id="Seg_2951" s="T127">adv</ta>
            <ta e="T129" id="Seg_2952" s="T128">n-n&gt;v-v:ptcp-v:(case)</ta>
            <ta e="T130" id="Seg_2953" s="T129">post</ta>
            <ta e="T131" id="Seg_2954" s="T130">adj-n:poss-n:case</ta>
            <ta e="T132" id="Seg_2955" s="T131">v-v:ptcp</ta>
            <ta e="T133" id="Seg_2956" s="T132">v-v:tense-v:pred.pn</ta>
            <ta e="T134" id="Seg_2957" s="T133">pers-pro:case</ta>
            <ta e="T135" id="Seg_2958" s="T134">n-n:(poss)-n:case</ta>
            <ta e="T136" id="Seg_2959" s="T135">ptcl</ta>
            <ta e="T139" id="Seg_2960" s="T138">v-v:tense-v:pred.pn</ta>
            <ta e="T140" id="Seg_2961" s="T139">n-n&gt;adv</ta>
            <ta e="T141" id="Seg_2962" s="T140">n-n:case</ta>
            <ta e="T142" id="Seg_2963" s="T141">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T143" id="Seg_2964" s="T142">v-v:ptcp</ta>
            <ta e="T144" id="Seg_2965" s="T143">n-n:poss-n:case</ta>
            <ta e="T145" id="Seg_2966" s="T144">v-v:cvb</ta>
            <ta e="T146" id="Seg_2967" s="T145">v-v:cvb</ta>
            <ta e="T147" id="Seg_2968" s="T146">pers-pro:case</ta>
            <ta e="T148" id="Seg_2969" s="T147">v-v:tense-v:pred.pn</ta>
            <ta e="T149" id="Seg_2970" s="T148">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T150" id="Seg_2971" s="T149">v-v:cvb</ta>
            <ta e="T151" id="Seg_2972" s="T150">v-v:tense-v:pred.pn</ta>
            <ta e="T152" id="Seg_2973" s="T151">dempro-pro:(num)-pro:case</ta>
            <ta e="T153" id="Seg_2974" s="T152">v-v&gt;v-v:cvb</ta>
            <ta e="T154" id="Seg_2975" s="T153">v-v:tense-v:pred.pn</ta>
            <ta e="T155" id="Seg_2976" s="T154">que</ta>
            <ta e="T156" id="Seg_2977" s="T155">pers-pro:case</ta>
            <ta e="T157" id="Seg_2978" s="T156">cardnum</ta>
            <ta e="T158" id="Seg_2979" s="T157">n-n:case</ta>
            <ta e="T159" id="Seg_2980" s="T158">post-n:poss-n:case</ta>
            <ta e="T160" id="Seg_2981" s="T159">v-v:ptcp</ta>
            <ta e="T161" id="Seg_2982" s="T160">v-v:tense-v:poss.pn</ta>
            <ta e="T162" id="Seg_2983" s="T161">pers-pro:case</ta>
            <ta e="T163" id="Seg_2984" s="T162">post</ta>
            <ta e="T164" id="Seg_2985" s="T163">v-v:(ins)-v:mood.pn</ta>
            <ta e="T165" id="Seg_2986" s="T164">v-v:(ins)-v:mood.pn</ta>
            <ta e="T166" id="Seg_2987" s="T165">adj-n:case</ta>
            <ta e="T167" id="Seg_2988" s="T166">post</ta>
            <ta e="T168" id="Seg_2989" s="T167">v-v:(ins)-v&gt;v-v:(ins)-v:(neg)-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T169" id="Seg_2990" s="T168">n-n:case</ta>
            <ta e="T170" id="Seg_2991" s="T169">adj-n:case</ta>
            <ta e="T171" id="Seg_2992" s="T170">v-v:mood-v:pred.pn</ta>
            <ta e="T172" id="Seg_2993" s="T171">n-n:case</ta>
            <ta e="T173" id="Seg_2994" s="T172">ptcl</ta>
            <ta e="T174" id="Seg_2995" s="T173">n-n:(poss)</ta>
            <ta e="T175" id="Seg_2996" s="T174">ptcl-ptcl:pred.pn</ta>
            <ta e="T176" id="Seg_2997" s="T175">pers-pro:case</ta>
            <ta e="T177" id="Seg_2998" s="T176">v-v:cvb</ta>
            <ta e="T178" id="Seg_2999" s="T177">post</ta>
            <ta e="T179" id="Seg_3000" s="T178">pers-pro:case</ta>
            <ta e="T180" id="Seg_3001" s="T179">ptcl</ta>
            <ta e="T181" id="Seg_3002" s="T180">v-v:tense-v:pred.pn</ta>
            <ta e="T182" id="Seg_3003" s="T181">v-v:cvb</ta>
            <ta e="T183" id="Seg_3004" s="T182">pers-pro:case</ta>
            <ta e="T184" id="Seg_3005" s="T183">n-n:(poss)-n:case</ta>
            <ta e="T185" id="Seg_3006" s="T184">dempro-pro:case</ta>
            <ta e="T186" id="Seg_3007" s="T185">n-n:case</ta>
            <ta e="T187" id="Seg_3008" s="T186">pers-pro:case</ta>
            <ta e="T188" id="Seg_3009" s="T187">n-n:poss-n:case</ta>
            <ta e="T189" id="Seg_3010" s="T188">v-v:ptcp</ta>
            <ta e="T190" id="Seg_3011" s="T189">v-v:mood-v:temp.pn</ta>
            <ta e="T191" id="Seg_3012" s="T190">pers-pro:case</ta>
            <ta e="T192" id="Seg_3013" s="T191">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T193" id="Seg_3014" s="T192">v-v:mood.pn</ta>
            <ta e="T194" id="Seg_3015" s="T193">pers-pro:case</ta>
            <ta e="T195" id="Seg_3016" s="T194">n-n:(poss)-n:case</ta>
            <ta e="T196" id="Seg_3017" s="T195">post</ta>
            <ta e="T197" id="Seg_3018" s="T196">dempro</ta>
            <ta e="T198" id="Seg_3019" s="T197">n-n:case</ta>
            <ta e="T199" id="Seg_3020" s="T198">propr-n:case</ta>
            <ta e="T200" id="Seg_3021" s="T199">v-v:tense-v:pred.pn</ta>
            <ta e="T201" id="Seg_3022" s="T200">n-n&gt;v-v:ptcp</ta>
            <ta e="T202" id="Seg_3023" s="T201">n-n:case</ta>
            <ta e="T203" id="Seg_3024" s="T202">n-n:poss-n:case</ta>
            <ta e="T204" id="Seg_3025" s="T203">que-pro:case</ta>
            <ta e="T205" id="Seg_3026" s="T204">pers-pro:case</ta>
            <ta e="T206" id="Seg_3027" s="T205">v-v:ptcp</ta>
            <ta e="T207" id="Seg_3028" s="T206">v-v:mood-v:pred.pn</ta>
            <ta e="T208" id="Seg_3029" s="T207">pers-pro:case</ta>
            <ta e="T209" id="Seg_3030" s="T208">n-n:(num)-n:case</ta>
            <ta e="T210" id="Seg_3031" s="T209">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T211" id="Seg_3032" s="T210">post</ta>
            <ta e="T212" id="Seg_3033" s="T211">que-pro:case</ta>
            <ta e="T213" id="Seg_3034" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_3035" s="T213">v-v:(neg)-v:pred.pn</ta>
            <ta e="T215" id="Seg_3036" s="T214">n-n:case</ta>
            <ta e="T216" id="Seg_3037" s="T215">n-n:(poss)-n:case</ta>
            <ta e="T217" id="Seg_3038" s="T216">ptcl</ta>
            <ta e="T218" id="Seg_3039" s="T217">v-v:(neg)-v:pred.pn</ta>
            <ta e="T219" id="Seg_3040" s="T218">dempro</ta>
            <ta e="T220" id="Seg_3041" s="T219">n-n&gt;v-v:ptcp</ta>
            <ta e="T221" id="Seg_3042" s="T220">adj-n:poss-n:case</ta>
            <ta e="T222" id="Seg_3043" s="T221">n-n:case</ta>
            <ta e="T223" id="Seg_3044" s="T222">n-n:(poss)-n:case</ta>
            <ta e="T224" id="Seg_3045" s="T223">dempro-pro:case</ta>
            <ta e="T225" id="Seg_3046" s="T224">v-v&gt;v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T226" id="Seg_3047" s="T225">pers-pro:case</ta>
            <ta e="T227" id="Seg_3048" s="T226">v-v:ptcp</ta>
            <ta e="T228" id="Seg_3049" s="T227">v-v:tense-v:poss.pn</ta>
            <ta e="T229" id="Seg_3050" s="T228">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T230" id="Seg_3051" s="T229">n-n:poss-n:case</ta>
            <ta e="T231" id="Seg_3052" s="T230">v-v:tense-v:pred.pn</ta>
            <ta e="T232" id="Seg_3053" s="T231">pers-pro:case</ta>
            <ta e="T233" id="Seg_3054" s="T232">v-v:cvb</ta>
            <ta e="T234" id="Seg_3055" s="T233">que-pro:case</ta>
            <ta e="T235" id="Seg_3056" s="T234">ptcl</ta>
            <ta e="T236" id="Seg_3057" s="T235">v-v:mood.pn</ta>
            <ta e="T237" id="Seg_3058" s="T236">pers-pro:case</ta>
            <ta e="T238" id="Seg_3059" s="T237">v-v:neg-v:poss.pn</ta>
            <ta e="T239" id="Seg_3060" s="T238">v-v:mood-v:pred.pn</ta>
            <ta e="T240" id="Seg_3061" s="T239">dempro</ta>
            <ta e="T241" id="Seg_3062" s="T240">n-n:case</ta>
            <ta e="T242" id="Seg_3063" s="T241">pers-pro:case</ta>
            <ta e="T243" id="Seg_3064" s="T242">n-n:(num)-n:case</ta>
            <ta e="T244" id="Seg_3065" s="T243">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T245" id="Seg_3066" s="T244">adv</ta>
            <ta e="T246" id="Seg_3067" s="T245">v-v:ptcp</ta>
            <ta e="T247" id="Seg_3068" s="T246">v-v:tense-v:poss.pn</ta>
            <ta e="T248" id="Seg_3069" s="T247">pers-pro:case</ta>
            <ta e="T249" id="Seg_3070" s="T248">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T250" id="Seg_3071" s="T249">n-n:(num)-n:case</ta>
            <ta e="T251" id="Seg_3072" s="T250">propr-n:case</ta>
            <ta e="T252" id="Seg_3073" s="T251">propr-n:case</ta>
            <ta e="T253" id="Seg_3074" s="T252">n-n:(num)-n:case</ta>
            <ta e="T254" id="Seg_3075" s="T253">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T255" id="Seg_3076" s="T254">v-v:cvb</ta>
            <ta e="T256" id="Seg_3077" s="T255">n-n:case</ta>
            <ta e="T257" id="Seg_3078" s="T256">v-v:tense-v:poss.pn</ta>
            <ta e="T258" id="Seg_3079" s="T257">v-v:tense-v:poss.pn</ta>
            <ta e="T259" id="Seg_3080" s="T258">propr-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_3081" s="T0">adj</ta>
            <ta e="T2" id="Seg_3082" s="T1">n</ta>
            <ta e="T3" id="Seg_3083" s="T2">dempro</ta>
            <ta e="T4" id="Seg_3084" s="T3">n</ta>
            <ta e="T5" id="Seg_3085" s="T4">cop</ta>
            <ta e="T6" id="Seg_3086" s="T5">propr</ta>
            <ta e="T7" id="Seg_3087" s="T6">n</ta>
            <ta e="T8" id="Seg_3088" s="T7">n</ta>
            <ta e="T9" id="Seg_3089" s="T8">v</ta>
            <ta e="T10" id="Seg_3090" s="T9">dempro</ta>
            <ta e="T11" id="Seg_3091" s="T10">n</ta>
            <ta e="T12" id="Seg_3092" s="T11">n</ta>
            <ta e="T13" id="Seg_3093" s="T12">n</ta>
            <ta e="T14" id="Seg_3094" s="T13">n</ta>
            <ta e="T15" id="Seg_3095" s="T14">propr</ta>
            <ta e="T16" id="Seg_3096" s="T15">cardnum</ta>
            <ta e="T17" id="Seg_3097" s="T16">cardnum</ta>
            <ta e="T18" id="Seg_3098" s="T17">v</ta>
            <ta e="T19" id="Seg_3099" s="T18">n</ta>
            <ta e="T20" id="Seg_3100" s="T19">post</ta>
            <ta e="T21" id="Seg_3101" s="T20">v</ta>
            <ta e="T22" id="Seg_3102" s="T21">propr</ta>
            <ta e="T23" id="Seg_3103" s="T22">propr</ta>
            <ta e="T24" id="Seg_3104" s="T23">adj</ta>
            <ta e="T25" id="Seg_3105" s="T24">v</ta>
            <ta e="T26" id="Seg_3106" s="T25">aux</ta>
            <ta e="T27" id="Seg_3107" s="T26">n</ta>
            <ta e="T28" id="Seg_3108" s="T27">v</ta>
            <ta e="T29" id="Seg_3109" s="T28">v</ta>
            <ta e="T30" id="Seg_3110" s="T29">propr</ta>
            <ta e="T31" id="Seg_3111" s="T30">n</ta>
            <ta e="T32" id="Seg_3112" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_3113" s="T32">propr</ta>
            <ta e="T34" id="Seg_3114" s="T33">n</ta>
            <ta e="T35" id="Seg_3115" s="T34">adv</ta>
            <ta e="T36" id="Seg_3116" s="T35">propr</ta>
            <ta e="T37" id="Seg_3117" s="T36">v</ta>
            <ta e="T38" id="Seg_3118" s="T37">n</ta>
            <ta e="T39" id="Seg_3119" s="T38">post</ta>
            <ta e="T40" id="Seg_3120" s="T39">adj</ta>
            <ta e="T41" id="Seg_3121" s="T40">n</ta>
            <ta e="T42" id="Seg_3122" s="T41">v</ta>
            <ta e="T43" id="Seg_3123" s="T42">v</ta>
            <ta e="T44" id="Seg_3124" s="T43">pers</ta>
            <ta e="T45" id="Seg_3125" s="T44">v</ta>
            <ta e="T46" id="Seg_3126" s="T45">aux</ta>
            <ta e="T47" id="Seg_3127" s="T46">adv</ta>
            <ta e="T48" id="Seg_3128" s="T47">pers</ta>
            <ta e="T49" id="Seg_3129" s="T48">v</ta>
            <ta e="T50" id="Seg_3130" s="T49">aux</ta>
            <ta e="T51" id="Seg_3131" s="T50">n</ta>
            <ta e="T52" id="Seg_3132" s="T51">v</ta>
            <ta e="T53" id="Seg_3133" s="T52">emphpro</ta>
            <ta e="T54" id="Seg_3134" s="T53">post</ta>
            <ta e="T55" id="Seg_3135" s="T54">v</ta>
            <ta e="T56" id="Seg_3136" s="T55">propr</ta>
            <ta e="T57" id="Seg_3137" s="T56">propr</ta>
            <ta e="T58" id="Seg_3138" s="T57">propr</ta>
            <ta e="T59" id="Seg_3139" s="T58">pers</ta>
            <ta e="T60" id="Seg_3140" s="T59">que</ta>
            <ta e="T61" id="Seg_3141" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_3142" s="T61">v</ta>
            <ta e="T63" id="Seg_3143" s="T62">adj</ta>
            <ta e="T64" id="Seg_3144" s="T63">post</ta>
            <ta e="T65" id="Seg_3145" s="T64">cop</ta>
            <ta e="T66" id="Seg_3146" s="T65">pers</ta>
            <ta e="T67" id="Seg_3147" s="T66">n</ta>
            <ta e="T68" id="Seg_3148" s="T67">v</ta>
            <ta e="T69" id="Seg_3149" s="T68">adv</ta>
            <ta e="T70" id="Seg_3150" s="T69">v</ta>
            <ta e="T71" id="Seg_3151" s="T70">aux</ta>
            <ta e="T72" id="Seg_3152" s="T71">pers</ta>
            <ta e="T73" id="Seg_3153" s="T72">propr</ta>
            <ta e="T74" id="Seg_3154" s="T73">v</ta>
            <ta e="T75" id="Seg_3155" s="T74">v</ta>
            <ta e="T76" id="Seg_3156" s="T75">n</ta>
            <ta e="T77" id="Seg_3157" s="T76">pers</ta>
            <ta e="T78" id="Seg_3158" s="T77">n</ta>
            <ta e="T79" id="Seg_3159" s="T78">v</ta>
            <ta e="T80" id="Seg_3160" s="T79">post</ta>
            <ta e="T81" id="Seg_3161" s="T80">v</ta>
            <ta e="T82" id="Seg_3162" s="T81">v</ta>
            <ta e="T83" id="Seg_3163" s="T82">aux</ta>
            <ta e="T84" id="Seg_3164" s="T83">pers</ta>
            <ta e="T85" id="Seg_3165" s="T84">post</ta>
            <ta e="T86" id="Seg_3166" s="T85">adv</ta>
            <ta e="T87" id="Seg_3167" s="T86">v</ta>
            <ta e="T88" id="Seg_3168" s="T87">pers</ta>
            <ta e="T89" id="Seg_3169" s="T88">v</ta>
            <ta e="T90" id="Seg_3170" s="T89">aux</ta>
            <ta e="T91" id="Seg_3171" s="T90">pers</ta>
            <ta e="T92" id="Seg_3172" s="T91">adv</ta>
            <ta e="T93" id="Seg_3173" s="T92">v</ta>
            <ta e="T94" id="Seg_3174" s="T93">v</ta>
            <ta e="T95" id="Seg_3175" s="T94">n</ta>
            <ta e="T96" id="Seg_3176" s="T95">n</ta>
            <ta e="T97" id="Seg_3177" s="T96">interj</ta>
            <ta e="T98" id="Seg_3178" s="T97">pers</ta>
            <ta e="T99" id="Seg_3179" s="T98">v</ta>
            <ta e="T100" id="Seg_3180" s="T99">aux</ta>
            <ta e="T101" id="Seg_3181" s="T100">aux</ta>
            <ta e="T102" id="Seg_3182" s="T101">v</ta>
            <ta e="T103" id="Seg_3183" s="T102">pers</ta>
            <ta e="T104" id="Seg_3184" s="T103">n</ta>
            <ta e="T105" id="Seg_3185" s="T104">dempro</ta>
            <ta e="T106" id="Seg_3186" s="T105">n</ta>
            <ta e="T107" id="Seg_3187" s="T106">dempro</ta>
            <ta e="T108" id="Seg_3188" s="T107">ptcl</ta>
            <ta e="T109" id="Seg_3189" s="T108">cop</ta>
            <ta e="T110" id="Seg_3190" s="T109">pers</ta>
            <ta e="T113" id="Seg_3191" s="T112">n</ta>
            <ta e="T114" id="Seg_3192" s="T113">cop</ta>
            <ta e="T115" id="Seg_3193" s="T114">pers</ta>
            <ta e="T116" id="Seg_3194" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_3195" s="T116">n</ta>
            <ta e="T118" id="Seg_3196" s="T117">n</ta>
            <ta e="T119" id="Seg_3197" s="T118">v</ta>
            <ta e="T120" id="Seg_3198" s="T119">aux</ta>
            <ta e="T121" id="Seg_3199" s="T120">pers</ta>
            <ta e="T122" id="Seg_3200" s="T121">n</ta>
            <ta e="T123" id="Seg_3201" s="T122">pers</ta>
            <ta e="T124" id="Seg_3202" s="T123">n</ta>
            <ta e="T125" id="Seg_3203" s="T124">v</ta>
            <ta e="T126" id="Seg_3204" s="T125">aux</ta>
            <ta e="T127" id="Seg_3205" s="T126">propr</ta>
            <ta e="T128" id="Seg_3206" s="T127">adv</ta>
            <ta e="T129" id="Seg_3207" s="T128">v</ta>
            <ta e="T130" id="Seg_3208" s="T129">post</ta>
            <ta e="T131" id="Seg_3209" s="T130">adj</ta>
            <ta e="T132" id="Seg_3210" s="T131">v</ta>
            <ta e="T133" id="Seg_3211" s="T132">aux</ta>
            <ta e="T134" id="Seg_3212" s="T133">pers</ta>
            <ta e="T135" id="Seg_3213" s="T134">n</ta>
            <ta e="T136" id="Seg_3214" s="T135">ptcl</ta>
            <ta e="T139" id="Seg_3215" s="T138">v</ta>
            <ta e="T140" id="Seg_3216" s="T139">adv</ta>
            <ta e="T141" id="Seg_3217" s="T140">n</ta>
            <ta e="T142" id="Seg_3218" s="T141">v</ta>
            <ta e="T143" id="Seg_3219" s="T142">v</ta>
            <ta e="T144" id="Seg_3220" s="T143">n</ta>
            <ta e="T145" id="Seg_3221" s="T144">v</ta>
            <ta e="T146" id="Seg_3222" s="T145">v</ta>
            <ta e="T147" id="Seg_3223" s="T146">pers</ta>
            <ta e="T148" id="Seg_3224" s="T147">v</ta>
            <ta e="T149" id="Seg_3225" s="T148">dempro</ta>
            <ta e="T150" id="Seg_3226" s="T149">v</ta>
            <ta e="T151" id="Seg_3227" s="T150">aux</ta>
            <ta e="T152" id="Seg_3228" s="T151">dempro</ta>
            <ta e="T153" id="Seg_3229" s="T152">v</ta>
            <ta e="T154" id="Seg_3230" s="T153">v</ta>
            <ta e="T155" id="Seg_3231" s="T154">que</ta>
            <ta e="T156" id="Seg_3232" s="T155">pers</ta>
            <ta e="T157" id="Seg_3233" s="T156">cardnum</ta>
            <ta e="T158" id="Seg_3234" s="T157">n</ta>
            <ta e="T159" id="Seg_3235" s="T158">post</ta>
            <ta e="T160" id="Seg_3236" s="T159">v</ta>
            <ta e="T161" id="Seg_3237" s="T160">aux</ta>
            <ta e="T162" id="Seg_3238" s="T161">pers</ta>
            <ta e="T163" id="Seg_3239" s="T162">post</ta>
            <ta e="T164" id="Seg_3240" s="T163">v</ta>
            <ta e="T165" id="Seg_3241" s="T164">v</ta>
            <ta e="T166" id="Seg_3242" s="T165">n</ta>
            <ta e="T167" id="Seg_3243" s="T166">post</ta>
            <ta e="T168" id="Seg_3244" s="T167">v</ta>
            <ta e="T169" id="Seg_3245" s="T168">n</ta>
            <ta e="T170" id="Seg_3246" s="T169">adj</ta>
            <ta e="T171" id="Seg_3247" s="T170">cop</ta>
            <ta e="T172" id="Seg_3248" s="T171">n</ta>
            <ta e="T173" id="Seg_3249" s="T172">ptcl</ta>
            <ta e="T174" id="Seg_3250" s="T173">n</ta>
            <ta e="T175" id="Seg_3251" s="T174">ptcl</ta>
            <ta e="T176" id="Seg_3252" s="T175">pers</ta>
            <ta e="T177" id="Seg_3253" s="T176">v</ta>
            <ta e="T178" id="Seg_3254" s="T177">post</ta>
            <ta e="T179" id="Seg_3255" s="T178">pers</ta>
            <ta e="T180" id="Seg_3256" s="T179">ptcl</ta>
            <ta e="T181" id="Seg_3257" s="T180">v</ta>
            <ta e="T182" id="Seg_3258" s="T181">v</ta>
            <ta e="T183" id="Seg_3259" s="T182">pers</ta>
            <ta e="T184" id="Seg_3260" s="T183">n</ta>
            <ta e="T185" id="Seg_3261" s="T184">dempro</ta>
            <ta e="T186" id="Seg_3262" s="T185">n</ta>
            <ta e="T187" id="Seg_3263" s="T186">pers</ta>
            <ta e="T188" id="Seg_3264" s="T187">n</ta>
            <ta e="T189" id="Seg_3265" s="T188">v</ta>
            <ta e="T190" id="Seg_3266" s="T189">aux</ta>
            <ta e="T191" id="Seg_3267" s="T190">pers</ta>
            <ta e="T192" id="Seg_3268" s="T191">v</ta>
            <ta e="T193" id="Seg_3269" s="T192">cop</ta>
            <ta e="T194" id="Seg_3270" s="T193">pers</ta>
            <ta e="T195" id="Seg_3271" s="T194">n</ta>
            <ta e="T196" id="Seg_3272" s="T195">post</ta>
            <ta e="T197" id="Seg_3273" s="T196">dempro</ta>
            <ta e="T198" id="Seg_3274" s="T197">n</ta>
            <ta e="T199" id="Seg_3275" s="T198">propr</ta>
            <ta e="T200" id="Seg_3276" s="T199">v</ta>
            <ta e="T201" id="Seg_3277" s="T200">v</ta>
            <ta e="T202" id="Seg_3278" s="T201">n</ta>
            <ta e="T203" id="Seg_3279" s="T202">n</ta>
            <ta e="T204" id="Seg_3280" s="T203">que</ta>
            <ta e="T205" id="Seg_3281" s="T204">pers</ta>
            <ta e="T206" id="Seg_3282" s="T205">v</ta>
            <ta e="T207" id="Seg_3283" s="T206">aux</ta>
            <ta e="T208" id="Seg_3284" s="T207">pers</ta>
            <ta e="T209" id="Seg_3285" s="T208">n</ta>
            <ta e="T210" id="Seg_3286" s="T209">v</ta>
            <ta e="T211" id="Seg_3287" s="T210">post</ta>
            <ta e="T212" id="Seg_3288" s="T211">que</ta>
            <ta e="T213" id="Seg_3289" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_3290" s="T213">v</ta>
            <ta e="T215" id="Seg_3291" s="T214">n</ta>
            <ta e="T216" id="Seg_3292" s="T215">n</ta>
            <ta e="T217" id="Seg_3293" s="T216">ptcl</ta>
            <ta e="T218" id="Seg_3294" s="T217">v</ta>
            <ta e="T219" id="Seg_3295" s="T218">dempro</ta>
            <ta e="T220" id="Seg_3296" s="T219">v</ta>
            <ta e="T221" id="Seg_3297" s="T220">n</ta>
            <ta e="T222" id="Seg_3298" s="T221">n</ta>
            <ta e="T223" id="Seg_3299" s="T222">n</ta>
            <ta e="T224" id="Seg_3300" s="T223">dempro</ta>
            <ta e="T225" id="Seg_3301" s="T224">v</ta>
            <ta e="T226" id="Seg_3302" s="T225">pers</ta>
            <ta e="T227" id="Seg_3303" s="T226">v</ta>
            <ta e="T228" id="Seg_3304" s="T227">aux</ta>
            <ta e="T229" id="Seg_3305" s="T228">emphpro</ta>
            <ta e="T230" id="Seg_3306" s="T229">n</ta>
            <ta e="T231" id="Seg_3307" s="T230">v</ta>
            <ta e="T232" id="Seg_3308" s="T231">pers</ta>
            <ta e="T233" id="Seg_3309" s="T232">v</ta>
            <ta e="T234" id="Seg_3310" s="T233">que</ta>
            <ta e="T235" id="Seg_3311" s="T234">ptcl</ta>
            <ta e="T236" id="Seg_3312" s="T235">cop</ta>
            <ta e="T237" id="Seg_3313" s="T236">pers</ta>
            <ta e="T238" id="Seg_3314" s="T237">v</ta>
            <ta e="T239" id="Seg_3315" s="T238">aux</ta>
            <ta e="T240" id="Seg_3316" s="T239">dempro</ta>
            <ta e="T241" id="Seg_3317" s="T240">n</ta>
            <ta e="T242" id="Seg_3318" s="T241">pers</ta>
            <ta e="T243" id="Seg_3319" s="T242">n</ta>
            <ta e="T244" id="Seg_3320" s="T243">v</ta>
            <ta e="T245" id="Seg_3321" s="T244">adv</ta>
            <ta e="T246" id="Seg_3322" s="T245">v</ta>
            <ta e="T247" id="Seg_3323" s="T246">aux</ta>
            <ta e="T248" id="Seg_3324" s="T247">pers</ta>
            <ta e="T249" id="Seg_3325" s="T248">v</ta>
            <ta e="T250" id="Seg_3326" s="T249">n</ta>
            <ta e="T251" id="Seg_3327" s="T250">propr</ta>
            <ta e="T252" id="Seg_3328" s="T251">propr</ta>
            <ta e="T253" id="Seg_3329" s="T252">n</ta>
            <ta e="T254" id="Seg_3330" s="T253">n</ta>
            <ta e="T255" id="Seg_3331" s="T254">v</ta>
            <ta e="T256" id="Seg_3332" s="T255">n</ta>
            <ta e="T257" id="Seg_3333" s="T256">v</ta>
            <ta e="T258" id="Seg_3334" s="T257">v</ta>
            <ta e="T259" id="Seg_3335" s="T258">propr</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T11" id="Seg_3336" s="T10">n:Time</ta>
            <ta e="T14" id="Seg_3337" s="T13">n:Time</ta>
            <ta e="T15" id="Seg_3338" s="T14">np.h:A</ta>
            <ta e="T20" id="Seg_3339" s="T18">pp:Com</ta>
            <ta e="T22" id="Seg_3340" s="T21">np:So</ta>
            <ta e="T29" id="Seg_3341" s="T28">0.3.h:A</ta>
            <ta e="T31" id="Seg_3342" s="T30">np:L</ta>
            <ta e="T34" id="Seg_3343" s="T33">np:G</ta>
            <ta e="T36" id="Seg_3344" s="T35">np.h:A</ta>
            <ta e="T39" id="Seg_3345" s="T37">pp:Com</ta>
            <ta e="T41" id="Seg_3346" s="T40">np:G</ta>
            <ta e="T44" id="Seg_3347" s="T43">pro.h:R</ta>
            <ta e="T46" id="Seg_3348" s="T44">0.2.h:A</ta>
            <ta e="T47" id="Seg_3349" s="T46">adv:L</ta>
            <ta e="T48" id="Seg_3350" s="T47">pro.h:A</ta>
            <ta e="T51" id="Seg_3351" s="T50">np.h:R</ta>
            <ta e="T54" id="Seg_3352" s="T52">pp:Com</ta>
            <ta e="T55" id="Seg_3353" s="T54">0.3.h:A</ta>
            <ta e="T56" id="Seg_3354" s="T55">np.h:Th</ta>
            <ta e="T57" id="Seg_3355" s="T56">np.h:Th</ta>
            <ta e="T58" id="Seg_3356" s="T57">np.h:Th</ta>
            <ta e="T59" id="Seg_3357" s="T58">pro.h:E</ta>
            <ta e="T65" id="Seg_3358" s="T64">0.3.h:Th</ta>
            <ta e="T72" id="Seg_3359" s="T71">pro.h:E</ta>
            <ta e="T73" id="Seg_3360" s="T72">np.h:A</ta>
            <ta e="T76" id="Seg_3361" s="T75">0.3.h:Poss np.h:R</ta>
            <ta e="T77" id="Seg_3362" s="T76">pro.h:Poss</ta>
            <ta e="T78" id="Seg_3363" s="T77">np:Th</ta>
            <ta e="T83" id="Seg_3364" s="T81">0.2.h:Th</ta>
            <ta e="T85" id="Seg_3365" s="T83">pp:Com</ta>
            <ta e="T87" id="Seg_3366" s="T86">0.1.h:A</ta>
            <ta e="T88" id="Seg_3367" s="T87">pro:So</ta>
            <ta e="T91" id="Seg_3368" s="T90">pro.h:A</ta>
            <ta e="T95" id="Seg_3369" s="T94">np.h:R</ta>
            <ta e="T107" id="Seg_3370" s="T106">pro:Th</ta>
            <ta e="T110" id="Seg_3371" s="T109">pro.h:Poss</ta>
            <ta e="T114" id="Seg_3372" s="T113">0.3:Th</ta>
            <ta e="T115" id="Seg_3373" s="T114">pro.h:Poss</ta>
            <ta e="T118" id="Seg_3374" s="T117">np:So</ta>
            <ta e="T121" id="Seg_3375" s="T120">pro.h:E</ta>
            <ta e="T122" id="Seg_3376" s="T121">np.h:St</ta>
            <ta e="T123" id="Seg_3377" s="T122">pro.h:Poss</ta>
            <ta e="T124" id="Seg_3378" s="T123">np:P</ta>
            <ta e="T126" id="Seg_3379" s="T124">0.3.h:Cau</ta>
            <ta e="T127" id="Seg_3380" s="T126">np.h:A</ta>
            <ta e="T134" id="Seg_3381" s="T133">pro.h:Poss</ta>
            <ta e="T135" id="Seg_3382" s="T134">np:Th</ta>
            <ta e="T141" id="Seg_3383" s="T140">np:G</ta>
            <ta e="T142" id="Seg_3384" s="T141">0.3:Cau</ta>
            <ta e="T144" id="Seg_3385" s="T143">0.3.h:Poss np:G</ta>
            <ta e="T147" id="Seg_3386" s="T146">pro.h:E</ta>
            <ta e="T149" id="Seg_3387" s="T148">pro.h:Th</ta>
            <ta e="T152" id="Seg_3388" s="T151">pro.h:P</ta>
            <ta e="T154" id="Seg_3389" s="T153">0.3.h:A</ta>
            <ta e="T156" id="Seg_3390" s="T155">pro.h:A</ta>
            <ta e="T159" id="Seg_3391" s="T158">n:Time</ta>
            <ta e="T163" id="Seg_3392" s="T161">pp:Com</ta>
            <ta e="T164" id="Seg_3393" s="T163">0.2.h:A</ta>
            <ta e="T165" id="Seg_3394" s="T164">0.2.h:A</ta>
            <ta e="T169" id="Seg_3395" s="T168">np:Th</ta>
            <ta e="T172" id="Seg_3396" s="T171">np:Th</ta>
            <ta e="T176" id="Seg_3397" s="T175">pro:So</ta>
            <ta e="T179" id="Seg_3398" s="T178">pro.h:A</ta>
            <ta e="T183" id="Seg_3399" s="T182">pro.h:Poss</ta>
            <ta e="T186" id="Seg_3400" s="T185">np:Th</ta>
            <ta e="T188" id="Seg_3401" s="T187">np:Path</ta>
            <ta e="T191" id="Seg_3402" s="T190">pro.h:A</ta>
            <ta e="T193" id="Seg_3403" s="T192">0.3:Th</ta>
            <ta e="T199" id="Seg_3404" s="T198">np.h:A</ta>
            <ta e="T204" id="Seg_3405" s="T203">pro:P</ta>
            <ta e="T205" id="Seg_3406" s="T204">pro.h:A</ta>
            <ta e="T212" id="Seg_3407" s="T211">pro.h:E</ta>
            <ta e="T216" id="Seg_3408" s="T215">np:Th</ta>
            <ta e="T223" id="Seg_3409" s="T222">np.h:A</ta>
            <ta e="T224" id="Seg_3410" s="T223">np:Th</ta>
            <ta e="T226" id="Seg_3411" s="T225">pro.h:E</ta>
            <ta e="T229" id="Seg_3412" s="T228">pro.h:Poss</ta>
            <ta e="T230" id="Seg_3413" s="T229">np:Th</ta>
            <ta e="T231" id="Seg_3414" s="T230">0.3.h:A</ta>
            <ta e="T232" id="Seg_3415" s="T231">pro.h:P</ta>
            <ta e="T237" id="Seg_3416" s="T236">pro.h:A</ta>
            <ta e="T248" id="Seg_3417" s="T247">pro.h:E</ta>
            <ta e="T250" id="Seg_3418" s="T249">np:St</ta>
            <ta e="T253" id="Seg_3419" s="T252">np.h:Poss</ta>
            <ta e="T256" id="Seg_3420" s="T255">np:So</ta>
            <ta e="T259" id="Seg_3421" s="T258">np.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T15" id="Seg_3422" s="T14">np.h:S</ta>
            <ta e="T21" id="Seg_3423" s="T20">v:pred</ta>
            <ta e="T28" id="Seg_3424" s="T22">s:temp</ta>
            <ta e="T29" id="Seg_3425" s="T28">0.3.h:S v:pred</ta>
            <ta e="T36" id="Seg_3426" s="T35">np.h:S</ta>
            <ta e="T42" id="Seg_3427" s="T36">s:temp</ta>
            <ta e="T43" id="Seg_3428" s="T42">v:pred</ta>
            <ta e="T46" id="Seg_3429" s="T44">0.2.h:S v:pred</ta>
            <ta e="T48" id="Seg_3430" s="T47">pro.h:S</ta>
            <ta e="T50" id="Seg_3431" s="T48">v:pred</ta>
            <ta e="T52" id="Seg_3432" s="T50">s:purp</ta>
            <ta e="T55" id="Seg_3433" s="T54">0.3.h:S v:pred</ta>
            <ta e="T56" id="Seg_3434" s="T55">np.h:O</ta>
            <ta e="T57" id="Seg_3435" s="T56">np.h:O</ta>
            <ta e="T58" id="Seg_3436" s="T57">np.h:O</ta>
            <ta e="T59" id="Seg_3437" s="T58">pro.h:S</ta>
            <ta e="T62" id="Seg_3438" s="T61">v:pred</ta>
            <ta e="T63" id="Seg_3439" s="T62">adj:pred</ta>
            <ta e="T65" id="Seg_3440" s="T64">0.3.h:S cop</ta>
            <ta e="T68" id="Seg_3441" s="T65">s:comp</ta>
            <ta e="T71" id="Seg_3442" s="T69">v:pred</ta>
            <ta e="T72" id="Seg_3443" s="T71">pro.h:O</ta>
            <ta e="T73" id="Seg_3444" s="T72">np.h:S</ta>
            <ta e="T74" id="Seg_3445" s="T73">v:pred</ta>
            <ta e="T78" id="Seg_3446" s="T77">np:S</ta>
            <ta e="T81" id="Seg_3447" s="T80">v:pred</ta>
            <ta e="T83" id="Seg_3448" s="T81">0.2.h:S v:pred</ta>
            <ta e="T87" id="Seg_3449" s="T86">0.1.h:S v:pred</ta>
            <ta e="T90" id="Seg_3450" s="T87">s:temp</ta>
            <ta e="T91" id="Seg_3451" s="T90">pro.h:S</ta>
            <ta e="T93" id="Seg_3452" s="T91">s:temp</ta>
            <ta e="T94" id="Seg_3453" s="T93">v:pred</ta>
            <ta e="T107" id="Seg_3454" s="T106">pro:S</ta>
            <ta e="T109" id="Seg_3455" s="T108">cop</ta>
            <ta e="T113" id="Seg_3456" s="T112">n:pred</ta>
            <ta e="T114" id="Seg_3457" s="T113">0.3:S cop</ta>
            <ta e="T117" id="Seg_3458" s="T116">n:pred</ta>
            <ta e="T120" id="Seg_3459" s="T118">v:pred</ta>
            <ta e="T122" id="Seg_3460" s="T121">np.h:S</ta>
            <ta e="T124" id="Seg_3461" s="T123">np:O</ta>
            <ta e="T126" id="Seg_3462" s="T124">0.3.h:S v:pred</ta>
            <ta e="T127" id="Seg_3463" s="T126">np.h:S</ta>
            <ta e="T133" id="Seg_3464" s="T131">v:pred</ta>
            <ta e="T135" id="Seg_3465" s="T134">np:S</ta>
            <ta e="T139" id="Seg_3466" s="T138">v:pred</ta>
            <ta e="T142" id="Seg_3467" s="T141">0.3:S v:pred</ta>
            <ta e="T146" id="Seg_3468" s="T142">s:temp</ta>
            <ta e="T147" id="Seg_3469" s="T146">pro.h:S</ta>
            <ta e="T148" id="Seg_3470" s="T147">v:pred</ta>
            <ta e="T151" id="Seg_3471" s="T148">s:comp</ta>
            <ta e="T153" id="Seg_3472" s="T151">s:temp</ta>
            <ta e="T154" id="Seg_3473" s="T153">0.3.h:S v:pred</ta>
            <ta e="T156" id="Seg_3474" s="T155">pro.h:S</ta>
            <ta e="T161" id="Seg_3475" s="T159">v:pred</ta>
            <ta e="T164" id="Seg_3476" s="T163">0.2.h:S v:pred</ta>
            <ta e="T165" id="Seg_3477" s="T164">0.2.h:S v:pred</ta>
            <ta e="T168" id="Seg_3478" s="T165">s:purp</ta>
            <ta e="T169" id="Seg_3479" s="T168">np:S</ta>
            <ta e="T170" id="Seg_3480" s="T169">adj:pred</ta>
            <ta e="T171" id="Seg_3481" s="T170">cop</ta>
            <ta e="T172" id="Seg_3482" s="T171">np:S</ta>
            <ta e="T175" id="Seg_3483" s="T174">ptcl:pred</ta>
            <ta e="T178" id="Seg_3484" s="T175">s:temp</ta>
            <ta e="T179" id="Seg_3485" s="T178">pro.h:S</ta>
            <ta e="T181" id="Seg_3486" s="T180">v:pred</ta>
            <ta e="T182" id="Seg_3487" s="T181">s:adv</ta>
            <ta e="T190" id="Seg_3488" s="T184">s:cond</ta>
            <ta e="T192" id="Seg_3489" s="T190">s:purp</ta>
            <ta e="T193" id="Seg_3490" s="T192">0.3:S cop</ta>
            <ta e="T195" id="Seg_3491" s="T194">n:pred</ta>
            <ta e="T199" id="Seg_3492" s="T198">np.h:S</ta>
            <ta e="T200" id="Seg_3493" s="T199">v:pred</ta>
            <ta e="T211" id="Seg_3494" s="T203">s:rel</ta>
            <ta e="T212" id="Seg_3495" s="T211">pro.h:S</ta>
            <ta e="T214" id="Seg_3496" s="T213">v:pred</ta>
            <ta e="T216" id="Seg_3497" s="T215">np:S</ta>
            <ta e="T218" id="Seg_3498" s="T217">v:pred</ta>
            <ta e="T221" id="Seg_3499" s="T220">np:O</ta>
            <ta e="T223" id="Seg_3500" s="T222">np.h:S</ta>
            <ta e="T224" id="Seg_3501" s="T223">np:O</ta>
            <ta e="T225" id="Seg_3502" s="T224">v:pred</ta>
            <ta e="T226" id="Seg_3503" s="T225">pro.h:S</ta>
            <ta e="T228" id="Seg_3504" s="T226">v:pred</ta>
            <ta e="T230" id="Seg_3505" s="T229">np:O</ta>
            <ta e="T231" id="Seg_3506" s="T230">0.3.h:S v:pred</ta>
            <ta e="T233" id="Seg_3507" s="T231">s:purp</ta>
            <ta e="T241" id="Seg_3508" s="T236">s:cond</ta>
            <ta e="T247" id="Seg_3509" s="T245">v:pred</ta>
            <ta e="T248" id="Seg_3510" s="T247">pro.h:S</ta>
            <ta e="T249" id="Seg_3511" s="T248">v:pred</ta>
            <ta e="T250" id="Seg_3512" s="T249">np:O</ta>
            <ta e="T257" id="Seg_3513" s="T256">v:pred</ta>
            <ta e="T258" id="Seg_3514" s="T257">v:pred</ta>
            <ta e="T259" id="Seg_3515" s="T258">np.h:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T6" id="Seg_3516" s="T5">RUS:cult</ta>
            <ta e="T8" id="Seg_3517" s="T7">RUS:cult</ta>
            <ta e="T15" id="Seg_3518" s="T14">RUS:cult</ta>
            <ta e="T22" id="Seg_3519" s="T21">RUS:cult</ta>
            <ta e="T23" id="Seg_3520" s="T22">RUS:cult</ta>
            <ta e="T30" id="Seg_3521" s="T29">RUS:cult</ta>
            <ta e="T33" id="Seg_3522" s="T32">RUS:cult</ta>
            <ta e="T36" id="Seg_3523" s="T35">RUS:cult</ta>
            <ta e="T56" id="Seg_3524" s="T55">RUS:cult</ta>
            <ta e="T57" id="Seg_3525" s="T56">RUS:cult</ta>
            <ta e="T58" id="Seg_3526" s="T57">RUS:cult</ta>
            <ta e="T73" id="Seg_3527" s="T72">RUS:cult</ta>
            <ta e="T106" id="Seg_3528" s="T105">RUS:cult</ta>
            <ta e="T122" id="Seg_3529" s="T121">RUS:cult</ta>
            <ta e="T127" id="Seg_3530" s="T126">RUS:cult</ta>
            <ta e="T158" id="Seg_3531" s="T157">RUS:cult</ta>
            <ta e="T186" id="Seg_3532" s="T185">RUS:cult</ta>
            <ta e="T198" id="Seg_3533" s="T197">RUS:cult</ta>
            <ta e="T199" id="Seg_3534" s="T198">RUS:cult</ta>
            <ta e="T202" id="Seg_3535" s="T201">RUS:cult</ta>
            <ta e="T241" id="Seg_3536" s="T240">RUS:cult</ta>
            <ta e="T245" id="Seg_3537" s="T244">RUS:core</ta>
            <ta e="T251" id="Seg_3538" s="T250">RUS:cult</ta>
            <ta e="T252" id="Seg_3539" s="T251">RUS:cult</ta>
            <ta e="T254" id="Seg_3540" s="T253">EV:core</ta>
            <ta e="T256" id="Seg_3541" s="T255">RUS:cult</ta>
            <ta e="T259" id="Seg_3542" s="T258">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T6" id="Seg_3543" s="T5">Csub fortition</ta>
            <ta e="T33" id="Seg_3544" s="T32">inCdel Vsub fortition Vsub</ta>
            <ta e="T56" id="Seg_3545" s="T55">Vsub medVins</ta>
            <ta e="T57" id="Seg_3546" s="T56">fortition fortition</ta>
            <ta e="T58" id="Seg_3547" s="T57">Vsub fortition</ta>
            <ta e="T106" id="Seg_3548" s="T105">Csub Vsub</ta>
            <ta e="T186" id="Seg_3549" s="T185">Csub Vsub</ta>
            <ta e="T199" id="Seg_3550" s="T198">fortition</ta>
            <ta e="T202" id="Seg_3551" s="T201">Csub Vsub</ta>
            <ta e="T241" id="Seg_3552" s="T240">Csub Vsub</ta>
            <ta e="T245" id="Seg_3553" s="T244">fortition Vsub</ta>
            <ta e="T256" id="Seg_3554" s="T255">medCins Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T6" id="Seg_3555" s="T5">dir:bare</ta>
            <ta e="T8" id="Seg_3556" s="T7">dir:bare</ta>
            <ta e="T15" id="Seg_3557" s="T14">dir:bare</ta>
            <ta e="T22" id="Seg_3558" s="T21">dir:infl</ta>
            <ta e="T23" id="Seg_3559" s="T22">dir:bare</ta>
            <ta e="T30" id="Seg_3560" s="T29">dir:bare</ta>
            <ta e="T33" id="Seg_3561" s="T32">dir:bare</ta>
            <ta e="T36" id="Seg_3562" s="T35">dir:bare</ta>
            <ta e="T56" id="Seg_3563" s="T55">dir:infl</ta>
            <ta e="T57" id="Seg_3564" s="T56">dir:infl</ta>
            <ta e="T58" id="Seg_3565" s="T57">dir:infl</ta>
            <ta e="T73" id="Seg_3566" s="T72">dir:bare</ta>
            <ta e="T106" id="Seg_3567" s="T105">dir:infl</ta>
            <ta e="T122" id="Seg_3568" s="T121">dir:bare</ta>
            <ta e="T127" id="Seg_3569" s="T126">dir:bare</ta>
            <ta e="T158" id="Seg_3570" s="T157">dir:bare</ta>
            <ta e="T186" id="Seg_3571" s="T185">dir:bare</ta>
            <ta e="T198" id="Seg_3572" s="T197">dir:infl</ta>
            <ta e="T199" id="Seg_3573" s="T198">dir:bare</ta>
            <ta e="T202" id="Seg_3574" s="T201">dir:bare</ta>
            <ta e="T241" id="Seg_3575" s="T240">dir:infl</ta>
            <ta e="T245" id="Seg_3576" s="T244">dir:bare</ta>
            <ta e="T251" id="Seg_3577" s="T250">dir:bare</ta>
            <ta e="T252" id="Seg_3578" s="T251">dir:bare</ta>
            <ta e="T254" id="Seg_3579" s="T253">dir:infl</ta>
            <ta e="T256" id="Seg_3580" s="T255">dir:infl</ta>
            <ta e="T259" id="Seg_3581" s="T258">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T9" id="Seg_3582" s="T0">The name of our next story is "He is praying in Gethsemane".</ta>
            <ta e="T22" id="Seg_3583" s="T9">In that night after the evening's meeting Jesus left Jerusalem together with eleven disciples.</ta>
            <ta e="T34" id="Seg_3584" s="T22">He crossed a roaring water with the Name Kidron and came to the settlement Gethsemane, which is located close to Jerusalem.</ta>
            <ta e="T44" id="Seg_3585" s="T34">There Jesus went together with his disciples into a dense forest and said to them:</ta>
            <ta e="T47" id="Seg_3586" s="T44">"Sit down here.</ta>
            <ta e="T52" id="Seg_3587" s="T47">I will go in order to pray to God."</ta>
            <ta e="T58" id="Seg_3588" s="T52">With himself he took Peter, Jacob and John. </ta>
            <ta e="T65" id="Seg_3589" s="T58">For some reason he was pondering, as if he was sad.</ta>
            <ta e="T72" id="Seg_3590" s="T65">It afflicted him apparently a lot that we have committed sins. </ta>
            <ta e="T76" id="Seg_3591" s="T72">Jesus said to his disciples:</ta>
            <ta e="T81" id="Seg_3592" s="T76">"My soul is deeply grieved.</ta>
            <ta e="T85" id="Seg_3593" s="T81">Be with me.</ta>
            <ta e="T87" id="Seg_3594" s="T85">Let us live according to the customs."</ta>
            <ta e="T95" id="Seg_3595" s="T87">Having gone away from them, he kneeled down and prayed to God.</ta>
            <ta e="T106" id="Seg_3596" s="T95">"Father, oh, if you just took away this cup from me.</ta>
            <ta e="T113" id="Seg_3597" s="T106">This, however, is not according to my will.</ta>
            <ta e="T117" id="Seg_3598" s="T113">It shall according to your will only."</ta>
            <ta e="T122" id="Seg_3599" s="T117">Down from heaven an angel appeared to him.</ta>
            <ta e="T126" id="Seg_3600" s="T122">It strengthened his soul apparently.</ta>
            <ta e="T133" id="Seg_3601" s="T126">Jesus started to pray with the last of his strength, as if he was suffering a lot.</ta>
            <ta e="T142" id="Seg_3602" s="T133">His sweat is just running, like blood it moistens the earth.</ta>
            <ta e="T151" id="Seg_3603" s="T142">He came back to his disciples and found them sleeping.</ta>
            <ta e="T154" id="Seg_3604" s="T151">Having woken them up, he said:</ta>
            <ta e="T163" id="Seg_3605" s="T154">"How didn't you make it to wait for an hour together with me?</ta>
            <ta e="T165" id="Seg_3606" s="T163">Sit and pray.</ta>
            <ta e="T175" id="Seg_3607" s="T165">For that you are not tempted, the spirit is willing, but the flesh is weak."</ta>
            <ta e="T182" id="Seg_3608" s="T175">He went away from them, prayed again and said:</ta>
            <ta e="T196" id="Seg_3609" s="T182">"My father, if this cup passes me, for that I drink, that it shall be according to your will."</ta>
            <ta e="T211" id="Seg_3610" s="T196">In this prayer Jesus speaks about the torturing cup, which he shall drink for the sins committed by us.</ta>
            <ta e="T221" id="Seg_3611" s="T211">Nobody knows them, not even the human soul gets over this torturous burden.</ta>
            <ta e="T225" id="Seg_3612" s="T221">The son of God shouldered it.</ta>
            <ta e="T231" id="Seg_3613" s="T225">He knew that he gave his own soul.</ta>
            <ta e="T233" id="Seg_3614" s="T231">In order to redeem us.</ta>
            <ta e="T236" id="Seg_3615" s="T233">Nobody shall be.</ta>
            <ta e="T247" id="Seg_3616" s="T236">If he had not drunken from this cup, the sins committed by us would have stayed forever.</ta>
            <ta e="T256" id="Seg_3617" s="T247">You heard stories from the book "Jesus Christ - the children's friend".</ta>
            <ta e="T259" id="Seg_3618" s="T256">Popov was reading and translating.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T9" id="Seg_3619" s="T0">Unsere nächste Geschichte heißt "Er betet in Getsemani".</ta>
            <ta e="T22" id="Seg_3620" s="T9">In jener Nacht nach dem abendlichen Treffen verließ Jesus mit elf Jüngern Jerusalem.</ta>
            <ta e="T34" id="Seg_3621" s="T22">Er überquerte ein brausendes Wasser mit dem Namen Kidron und kam in die neben Jerusalem gelegene Siedlung Getsemani.</ta>
            <ta e="T44" id="Seg_3622" s="T34">Dort ging Jesus mit seinen Jüngern in einen dichten Wald und sagte ihnen:</ta>
            <ta e="T47" id="Seg_3623" s="T44">"Setzt euch hier.</ta>
            <ta e="T52" id="Seg_3624" s="T47">Ich gehe, um zu Gott zu beten."</ta>
            <ta e="T58" id="Seg_3625" s="T52">Mit sich nahm er Petrus, Jakob und Johannes.</ta>
            <ta e="T65" id="Seg_3626" s="T58">Er dachte aus irgendeinem Grund nach, als ob er traurig wäre.</ta>
            <ta e="T72" id="Seg_3627" s="T65">Dass wir Sünden begangen haben, belastete ihn offenbar sehr.</ta>
            <ta e="T76" id="Seg_3628" s="T72">Jesus sagte zu seinen Jüngern:</ta>
            <ta e="T81" id="Seg_3629" s="T76">"Meine Seele ist zu Tode betrübt.</ta>
            <ta e="T85" id="Seg_3630" s="T81">Seid mit mir.</ta>
            <ta e="T87" id="Seg_3631" s="T85">Lasst uns nach den Bräuchen leben."</ta>
            <ta e="T95" id="Seg_3632" s="T87">Nachdem er von ihnen weggegangen war, kniete er sich hin und betete zu Gott.</ta>
            <ta e="T106" id="Seg_3633" s="T95">"Vater, oh, wenn du diesen Kelch an mir vorübergehen lassen würdest.</ta>
            <ta e="T113" id="Seg_3634" s="T106">Dies ist jedoch nicht nach meinem Willen.</ta>
            <ta e="T117" id="Seg_3635" s="T113">Es soll alleine nach deinem Willen sein."</ta>
            <ta e="T122" id="Seg_3636" s="T117">Vom Himmel erschien ihm ein Engel.</ta>
            <ta e="T126" id="Seg_3637" s="T122">Der bestärkte seine Seele offenbar.</ta>
            <ta e="T133" id="Seg_3638" s="T126">Jesus fing an, mit letzter Kraft, als litte er sehr, zu beten.</ta>
            <ta e="T142" id="Seg_3639" s="T133">Sein Schweiß läuft nur so, wie Blut benetzt er die Erde.</ta>
            <ta e="T151" id="Seg_3640" s="T142">Er kehrte zu seinen Jüngern zurück und fand sie, wie sie schliefen.</ta>
            <ta e="T154" id="Seg_3641" s="T151">Nachdem er sie geweckt hatte, sprach er:</ta>
            <ta e="T163" id="Seg_3642" s="T154">"Wie konntet ihr denn nicht eine Stunde mit mir wachen?</ta>
            <ta e="T165" id="Seg_3643" s="T163">Sitzt und betet.</ta>
            <ta e="T175" id="Seg_3644" s="T165">Damit ihr nicht in Versuchung kommt, der Geist ist willig, das Fleisch ist schwach."</ta>
            <ta e="T182" id="Seg_3645" s="T175">Er ging von ihnen weg, betete wieder und sagte:</ta>
            <ta e="T196" id="Seg_3646" s="T182">"Mein Vater, wenn dieser Kelch an mir vorbeigeht, damit ich trinke, dann sei es nach deinem Willen."</ta>
            <ta e="T211" id="Seg_3647" s="T196">In diesem Gebet spricht Jesus über den quälenden Kelch, den er für die von uns begangenen Sünden trinken soll.</ta>
            <ta e="T221" id="Seg_3648" s="T211">Niemand kennt sie, nicht einmal die menschliche Seele überwindet diese quälende Last.</ta>
            <ta e="T225" id="Seg_3649" s="T221">Gottes Sohn hat sie auf sich genommen.</ta>
            <ta e="T231" id="Seg_3650" s="T225">Er wusste, dass er seine eigene Seele gibt.</ta>
            <ta e="T233" id="Seg_3651" s="T231">Um uns zu erlösen.</ta>
            <ta e="T236" id="Seg_3652" s="T233">Niemand soll sein.</ta>
            <ta e="T247" id="Seg_3653" s="T236">Wenn er nicht aus diesem Kelch getrunken hätte, dann wären die von uns begangenen Sünden immer geblieben.</ta>
            <ta e="T256" id="Seg_3654" s="T247">Ihr hörtet Geschichten aus dem Buch "Jesus Christus - der Freund der Kinder".</ta>
            <ta e="T259" id="Seg_3655" s="T256">Es las und übersetzte Popov.</ta>
         </annotation>
         <annotation name="fr" tierref="fr" />
         <annotation name="ltr" tierref="ltr">
            <ta e="T9" id="Seg_3656" s="T0">Следующего рассказ называется "Гэпсэмания (?) местности молитву читает".</ta>
            <ta e="T22" id="Seg_3657" s="T9">Той ночью после вечерней встречи Иисус с одиннацатью учениками вышел из Иерусалима. </ta>
            <ta e="T34" id="Seg_3658" s="T22">Кидрон с именем бурлящую воду перейдя, пришёл с Иерусалимом находящейся к местности. </ta>
            <ta e="T44" id="Seg_3659" s="T34">Там Иисус с учениками, на сухое дерево забравшись, сказал им: </ta>
            <ta e="T47" id="Seg_3660" s="T44">"Посидите здесь.</ta>
            <ta e="T52" id="Seg_3661" s="T47">Я схожу богу помолюсь".</ta>
            <ta e="T58" id="Seg_3662" s="T52">С собой взял Петра, Якова и Ивана.</ta>
            <ta e="T65" id="Seg_3663" s="T58">Он почему-то грустный, как будто скучает.</ta>
            <ta e="T72" id="Seg_3664" s="T65">Мы что согрешили, сильно давит его.</ta>
            <ta e="T76" id="Seg_3665" s="T72">Иисус сказал ученикам: </ta>
            <ta e="T81" id="Seg_3666" s="T76">"Мой дух до смерти рапереживался.</ta>
            <ta e="T85" id="Seg_3667" s="T81">Побудьте со мной.</ta>
            <ta e="T87" id="Seg_3668" s="T85">С навыками давайте жить".</ta>
            <ta e="T95" id="Seg_3669" s="T87">От них сторону отойдя, он, присев на корточки, помолился богу.</ta>
            <ta e="T106" id="Seg_3670" s="T95">"Отец, о-о ты если бы одобрил отпустить меня мимо эту чашу.</ta>
            <ta e="T113" id="Seg_3671" s="T106">Это всё равно не становится, по-моему.</ta>
            <ta e="T117" id="Seg_3672" s="T113">Пусть будет только по твоему желанию."</ta>
            <ta e="T122" id="Seg_3673" s="T117">С неба привиделся ему ангел.</ta>
            <ta e="T126" id="Seg_3674" s="T122">Его душу укрепляет, оказывается.</ta>
            <ta e="T133" id="Seg_3675" s="T126">Иисус в сильных муках будто через силу помолиться стал.</ta>
            <ta e="T142" id="Seg_3676" s="T133">Его пот только стекает, кровью на пол льётся. </ta>
            <ta e="T151" id="Seg_3677" s="T142">К ученикам вернувшись, он застал их спящими.</ta>
            <ta e="T154" id="Seg_3678" s="T151">Их разбудив, сказал: </ta>
            <ta e="T163" id="Seg_3679" s="T154">"Как вы больше часа не стали сидеть со мной?</ta>
            <ta e="T165" id="Seg_3680" s="T163">Садитесь, молитесь.</ta>
            <ta e="T175" id="Seg_3681" s="T165">К плохому чтобы не тянуло вас грех шустрый бывает, а тело бывает бессильным."</ta>
            <ta e="T182" id="Seg_3682" s="T175">От них отойдя, он снова помолился, сказав:</ta>
            <ta e="T196" id="Seg_3683" s="T182">"Мой отец, это чаша со мной рядом если проходит, чтобы я испил, пусть будет как ты хочешь".</ta>
            <ta e="T211" id="Seg_3684" s="T196">В этой молитве Христос говорит о мучительной чаше, что он испить должен за наши грехи. </ta>
            <ta e="T221" id="Seg_3685" s="T211">Никто не знает, человеческая душа даже не одолеет эту мучительную боль.</ta>
            <ta e="T225" id="Seg_3686" s="T221">Бога сын за это взялся.</ta>
            <ta e="T231" id="Seg_3687" s="T225">Он знал – свою душу отдаёт.</ta>
            <ta e="T233" id="Seg_3688" s="T231">Нас чтобы спасти.</ta>
            <ta e="T236" id="Seg_3689" s="T233">Хоть кто будет.</ta>
            <ta e="T247" id="Seg_3690" s="T236">Если бы он не испил из этой чаши, наши грехи так и остались бы.</ta>
            <ta e="T256" id="Seg_3691" s="T247">Вы прослушали рассказы "Иисус Христос – друг детей" из книги. </ta>
            <ta e="T259" id="Seg_3692" s="T256">Читал, перевёл Попов.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
