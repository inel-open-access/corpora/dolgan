<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID6DC26350-C835-E185-0245-B26A98A1461C">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>PoNA_1994_LastSupper_transl</transcription-name>
         <referenced-file url="PoNA_19940110_LastSupper_transl.wav" />
         <referenced-file url="PoNA_19940110_LastSupper_transl.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\transl\PoNA_19940110_LastSupper_transl\PoNA_19940110_LastSupper_transl.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">256</ud-information>
            <ud-information attribute-name="# HIAT:w">186</ud-information>
            <ud-information attribute-name="# e">186</ud-information>
            <ud-information attribute-name="# HIAT:u">26</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PoNA">
            <abbreviation>PoNA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="0.973" type="appl" />
         <tli id="T2" time="1.946" type="appl" />
         <tli id="T3" time="2.919" type="appl" />
         <tli id="T4" time="3.892" type="appl" />
         <tli id="T5" time="4.864" type="appl" />
         <tli id="T6" time="5.837" type="appl" />
         <tli id="T7" time="6.81" type="appl" />
         <tli id="T8" time="7.783" type="appl" />
         <tli id="T9" time="8.206666666666665" />
         <tli id="T10" time="13.04" />
         <tli id="T11" />
         <tli id="T12" />
         <tli id="T13" />
         <tli id="T14" time="13.333333333333334" />
         <tli id="T15" time="14.281" type="appl" />
         <tli id="T16" time="14.738" type="appl" />
         <tli id="T17" time="15.196" type="appl" />
         <tli id="T18" time="17.08666666666667" />
         <tli id="T19" time="19.82" />
         <tli id="T20" />
         <tli id="T21" time="19.92666666666667" />
         <tli id="T22" time="20.762" type="appl" />
         <tli id="T23" time="21.41333333333333" />
         <tli id="T24" time="22.346666666666668" />
         <tli id="T25" time="23.329" type="appl" />
         <tli id="T26" time="24.01" type="appl" />
         <tli id="T27" time="24.691" type="appl" />
         <tli id="T28" time="25.372" type="appl" />
         <tli id="T29" time="26.053" type="appl" />
         <tli id="T30" time="26.734" type="appl" />
         <tli id="T31" time="27.415" type="appl" />
         <tli id="T32" time="28.096" type="appl" />
         <tli id="T33" time="28.777" type="appl" />
         <tli id="T34" time="29.458" type="appl" />
         <tli id="T35" time="30.139" type="appl" />
         <tli id="T36" time="30.82" type="appl" />
         <tli id="T37" time="31.501" type="appl" />
         <tli id="T38" time="32.182" type="appl" />
         <tli id="T39" time="32.863" type="appl" />
         <tli id="T40" time="33.832" type="appl" />
         <tli id="T41" time="34.802" type="appl" />
         <tli id="T42" time="35.771" type="appl" />
         <tli id="T43" time="36.74" type="appl" />
         <tli id="T44" time="37.501" type="appl" />
         <tli id="T45" time="38.262" type="appl" />
         <tli id="T46" time="39.023" type="appl" />
         <tli id="T47" time="39.783" type="appl" />
         <tli id="T48" time="40.544" type="appl" />
         <tli id="T49" time="41.305" type="appl" />
         <tli id="T50" time="42.066" type="appl" />
         <tli id="T51" time="42.915" type="appl" />
         <tli id="T52" time="43.765" type="appl" />
         <tli id="T53" time="44.614" type="appl" />
         <tli id="T54" time="45.464" type="appl" />
         <tli id="T55" time="46.313" type="appl" />
         <tli id="T56" time="47.163" type="appl" />
         <tli id="T57" time="48.012" type="appl" />
         <tli id="T58" time="48.862" type="appl" />
         <tli id="T59" time="49.711" type="appl" />
         <tli id="T60" time="50.561" type="appl" />
         <tli id="T61" time="51.41" type="appl" />
         <tli id="T62" time="52.26" type="appl" />
         <tli id="T63" time="53.109" type="appl" />
         <tli id="T64" time="53.959" type="appl" />
         <tli id="T65" time="54.808" type="appl" />
         <tli id="T66" time="55.433" type="appl" />
         <tli id="T67" time="56.057" type="appl" />
         <tli id="T68" time="56.682" type="appl" />
         <tli id="T69" time="57.306" type="appl" />
         <tli id="T70" time="57.931" type="appl" />
         <tli id="T71" time="58.555" type="appl" />
         <tli id="T72" time="59.18" type="appl" />
         <tli id="T73" time="60.23" type="appl" />
         <tli id="T74" time="61.28" type="appl" />
         <tli id="T75" time="62.843" type="appl" />
         <tli id="T76" time="64.406" type="appl" />
         <tli id="T77" time="65.159" type="appl" />
         <tli id="T78" time="65.911" type="appl" />
         <tli id="T79" time="66.664" type="appl" />
         <tli id="T80" time="67.417" type="appl" />
         <tli id="T81" time="68.169" type="appl" />
         <tli id="T82" time="68.922" type="appl" />
         <tli id="T83" time="69.675" type="appl" />
         <tli id="T84" time="70.427" type="appl" />
         <tli id="T85" time="71.18" type="appl" />
         <tli id="T86" time="72.921" type="appl" />
         <tli id="T87" time="74.661" type="appl" />
         <tli id="T88" time="76.402" type="appl" />
         <tli id="T89" time="77.226" type="appl" />
         <tli id="T90" time="78.051" type="appl" />
         <tli id="T91" time="78.875" type="appl" />
         <tli id="T92" time="79.699" type="appl" />
         <tli id="T93" time="80.524" type="appl" />
         <tli id="T94" time="81.348" type="appl" />
         <tli id="T95" time="82.172" type="appl" />
         <tli id="T96" time="82.997" type="appl" />
         <tli id="T97" time="83.821" type="appl" />
         <tli id="T98" time="84.645" type="appl" />
         <tli id="T99" time="85.47" type="appl" />
         <tli id="T100" time="86.294" type="appl" />
         <tli id="T101" time="87.118" type="appl" />
         <tli id="T102" time="87.943" type="appl" />
         <tli id="T103" time="88.767" type="appl" />
         <tli id="T104" time="89.622" type="appl" />
         <tli id="T105" time="90.477" type="appl" />
         <tli id="T106" time="91.333" type="appl" />
         <tli id="T107" time="92.188" type="appl" />
         <tli id="T108" time="93.043" type="appl" />
         <tli id="T109" time="93.898" type="appl" />
         <tli id="T110" time="94.753" type="appl" />
         <tli id="T111" time="95.608" type="appl" />
         <tli id="T112" time="96.464" type="appl" />
         <tli id="T113" time="97.319" type="appl" />
         <tli id="T114" time="98.12" />
         <tli id="T115" time="103.98666666666666" />
         <tli id="T116" />
         <tli id="T117" />
         <tli id="T118" />
         <tli id="T119" />
         <tli id="T120" />
         <tli id="T121" />
         <tli id="T122" time="104.72" />
         <tli id="T123" time="105.43" type="appl" />
         <tli id="T124" time="106.246" type="appl" />
         <tli id="T125" time="107.063" type="appl" />
         <tli id="T126" time="107.879" type="appl" />
         <tli id="T127" time="108.695" type="appl" />
         <tli id="T128" time="113.25333333333333" />
         <tli id="T129" time="116.51333333333334" />
         <tli id="T130" />
         <tli id="T131" />
         <tli id="T132" time="117.521" type="appl" />
         <tli id="T133" time="118.209" type="appl" />
         <tli id="T134" time="118.897" type="appl" />
         <tli id="T135" time="119.585" type="appl" />
         <tli id="T136" time="120.273" type="appl" />
         <tli id="T137" time="120.961" type="appl" />
         <tli id="T138" time="121.649" type="appl" />
         <tli id="T139" time="122.337" type="appl" />
         <tli id="T140" time="123.025" type="appl" />
         <tli id="T141" time="123.713" type="appl" />
         <tli id="T142" time="124.401" type="appl" />
         <tli id="T143" time="125.089" type="appl" />
         <tli id="T144" time="125.777" type="appl" />
         <tli id="T145" time="126.701" type="appl" />
         <tli id="T146" time="127.625" type="appl" />
         <tli id="T147" time="128.549" type="appl" />
         <tli id="T148" time="129.473" type="appl" />
         <tli id="T149" time="130.397" type="appl" />
         <tli id="T150" time="131.32" type="appl" />
         <tli id="T151" time="132.244" type="appl" />
         <tli id="T152" time="133.168" type="appl" />
         <tli id="T153" time="134.092" type="appl" />
         <tli id="T154" time="135.016" type="appl" />
         <tli id="T155" time="135.8733333333333" />
         <tli id="T156" time="141.97333333333333" />
         <tli id="T157" />
         <tli id="T158" />
         <tli id="T159" />
         <tli id="T160" />
         <tli id="T161" />
         <tli id="T162" />
         <tli id="T163" />
         <tli id="T164" />
         <tli id="T165" time="142.712" type="appl" />
         <tli id="T166" time="143.42" type="appl" />
         <tli id="T167" time="144.129" type="appl" />
         <tli id="T168" time="144.837" type="appl" />
         <tli id="T169" time="145.545" type="appl" />
         <tli id="T170" time="146.413" type="appl" />
         <tli id="T171" time="147.281" type="appl" />
         <tli id="T172" time="148.149" type="appl" />
         <tli id="T173" time="149.017" type="appl" />
         <tli id="T174" time="149.885" type="appl" />
         <tli id="T175" time="150.753" type="appl" />
         <tli id="T176" time="151.621" type="appl" />
         <tli id="T177" time="152.489" type="appl" />
         <tli id="T178" time="153.191" type="appl" />
         <tli id="T179" time="153.894" type="appl" />
         <tli id="T180" time="154.596" type="appl" />
         <tli id="T181" time="155.298" type="appl" />
         <tli id="T182" time="156.0" type="appl" />
         <tli id="T183" time="156.703" type="appl" />
         <tli id="T184" time="157.405" type="appl" />
         <tli id="T185" time="158.107" type="appl" />
         <tli id="T186" time="158.809" type="appl" />
         <tli id="T187" time="159.512" type="appl" />
         <tli id="T188" time="160.214" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PoNA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T188" id="Seg_0" n="sc" s="T0">
               <ts e="T9" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Paːsxa</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">taŋara</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">bu͡olbutugar</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">Isuːs</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">ɨjɨppɨt</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">Pʼötrtan</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">gɨtta</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">Ujbaːntan</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">haŋaran</ts>
                  <nts id="Seg_29" n="HIAT:ip">:</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T14" id="Seg_32" n="HIAT:u" s="T9">
                  <nts id="Seg_33" n="HIAT:ip">"</nts>
                  <nts id="Seg_34" n="HIAT:ip">(</nts>
                  <ts e="T10" id="Seg_36" n="HIAT:w" s="T9">Barɨː</ts>
                  <nts id="Seg_37" n="HIAT:ip">)</nts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">barɨŋ</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">belemneːŋ</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">Paːsxaga</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">ahɨːrbɨtɨn</ts>
                  <nts id="Seg_50" n="HIAT:ip">"</nts>
                  <nts id="Seg_51" n="HIAT:ip">.</nts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T18" id="Seg_54" n="HIAT:u" s="T14">
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">Giniler</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">bu͡ollaktarɨna</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_62" n="HIAT:w" s="T16">ɨjɨppɨttar</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">Isuːstan</ts>
                  <nts id="Seg_66" n="HIAT:ip">:</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T21" id="Seg_69" n="HIAT:u" s="T18">
                  <nts id="Seg_70" n="HIAT:ip">"</nts>
                  <ts e="T19" id="Seg_72" n="HIAT:w" s="T18">Kanna</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_75" n="HIAT:w" s="T19">belemnetegin</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_78" n="HIAT:w" s="T20">di͡en</ts>
                  <nts id="Seg_79" n="HIAT:ip">?</nts>
                  <nts id="Seg_80" n="HIAT:ip">"</nts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_83" n="HIAT:u" s="T21">
                  <ts e="T22" id="Seg_85" n="HIAT:w" s="T21">Isuːs</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_88" n="HIAT:w" s="T22">di͡ebit</ts>
                  <nts id="Seg_89" n="HIAT:ip">:</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_92" n="HIAT:u" s="T23">
                  <nts id="Seg_93" n="HIAT:ip">"</nts>
                  <ts e="T24" id="Seg_95" n="HIAT:w" s="T23">Onno</ts>
                  <nts id="Seg_96" n="HIAT:ip">.</nts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_99" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_101" n="HIAT:w" s="T24">Ehigi</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_104" n="HIAT:w" s="T25">gu͡orakka</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_107" n="HIAT:w" s="T26">kiːreŋŋit</ts>
                  <nts id="Seg_108" n="HIAT:ip">,</nts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_111" n="HIAT:w" s="T27">kiːren</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_113" n="HIAT:ip">(</nts>
                  <ts e="T30" id="Seg_115" n="HIAT:w" s="T28">hirbi-</ts>
                  <nts id="Seg_116" n="HIAT:ip">)</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_119" n="HIAT:w" s="T30">hirgitiger</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_122" n="HIAT:w" s="T31">körsü͡ökküt</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_125" n="HIAT:w" s="T32">biːr</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_128" n="HIAT:w" s="T33">kihini</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_131" n="HIAT:w" s="T34">gɨtta</ts>
                  <nts id="Seg_132" n="HIAT:ip">,</nts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_135" n="HIAT:w" s="T35">illen</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_138" n="HIAT:w" s="T36">iheri</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_141" n="HIAT:w" s="T37">uːlaːk</ts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_144" n="HIAT:w" s="T38">ihiti</ts>
                  <nts id="Seg_145" n="HIAT:ip">.</nts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_148" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_150" n="HIAT:w" s="T39">Ginini</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_153" n="HIAT:w" s="T40">batɨhɨŋ</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_156" n="HIAT:w" s="T41">dʼi͡ege</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_159" n="HIAT:w" s="T42">di͡eri</ts>
                  <nts id="Seg_160" n="HIAT:ip">.</nts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_163" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_165" n="HIAT:w" s="T43">Kanna</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_168" n="HIAT:w" s="T44">kiːri͡ege</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_171" n="HIAT:w" s="T45">gini</ts>
                  <nts id="Seg_172" n="HIAT:ip">,</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_175" n="HIAT:w" s="T46">ol</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_178" n="HIAT:w" s="T47">dʼi͡ege</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_181" n="HIAT:w" s="T48">küseːjniger</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_184" n="HIAT:w" s="T49">haŋarɨŋ</ts>
                  <nts id="Seg_185" n="HIAT:ip">.</nts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T65" id="Seg_188" n="HIAT:u" s="T50">
                  <nts id="Seg_189" n="HIAT:ip">"</nts>
                  <ts e="T51" id="Seg_191" n="HIAT:w" s="T50">Učʼitʼelʼ</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_194" n="HIAT:w" s="T51">ɨjɨtar</ts>
                  <nts id="Seg_195" n="HIAT:ip">"</nts>
                  <nts id="Seg_196" n="HIAT:ip">,</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_199" n="HIAT:w" s="T52">di͡eŋ</ts>
                  <nts id="Seg_200" n="HIAT:ip">,</nts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_202" n="HIAT:ip">"</nts>
                  <ts e="T54" id="Seg_204" n="HIAT:w" s="T53">enigitten</ts>
                  <nts id="Seg_205" n="HIAT:ip">,</nts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_207" n="HIAT:ip">"</nts>
                  <ts e="T55" id="Seg_209" n="HIAT:w" s="T54">kanna</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_212" n="HIAT:w" s="T55">baːrɨj</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_215" n="HIAT:w" s="T56">dʼi͡e</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_218" n="HIAT:w" s="T57">koho</ts>
                  <nts id="Seg_219" n="HIAT:ip">,</nts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_222" n="HIAT:w" s="T58">min</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_225" n="HIAT:w" s="T59">Paːsxaga</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_228" n="HIAT:w" s="T60">ahɨːrbar</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_231" n="HIAT:w" s="T61">bejem</ts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_234" n="HIAT:w" s="T62">ü͡öreter</ts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_237" n="HIAT:w" s="T63">dʼommun</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_240" n="HIAT:w" s="T64">gɨtta</ts>
                  <nts id="Seg_241" n="HIAT:ip">?</nts>
                  <nts id="Seg_242" n="HIAT:ip">"</nts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_245" n="HIAT:u" s="T65">
                  <ts e="T66" id="Seg_247" n="HIAT:w" s="T65">Gini</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_250" n="HIAT:w" s="T66">köllörü͡öge</ts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_253" n="HIAT:w" s="T67">ehi͡eke</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_256" n="HIAT:w" s="T68">ki͡eŋ</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_259" n="HIAT:w" s="T69">bagajɨː</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_262" n="HIAT:w" s="T70">dʼi͡e</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_265" n="HIAT:w" s="T71">kohun</ts>
                  <nts id="Seg_266" n="HIAT:ip">.</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_269" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_271" n="HIAT:w" s="T72">Mu͡ostata</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_274" n="HIAT:w" s="T73">habɨːlaːgɨ</ts>
                  <nts id="Seg_275" n="HIAT:ip">.</nts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_278" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_280" n="HIAT:w" s="T74">Onno</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_283" n="HIAT:w" s="T75">belemneːŋ</ts>
                  <nts id="Seg_284" n="HIAT:ip">.</nts>
                  <nts id="Seg_285" n="HIAT:ip">"</nts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_288" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_290" n="HIAT:w" s="T76">Ü͡öreter</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_293" n="HIAT:w" s="T77">dʼono</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_296" n="HIAT:w" s="T78">onno</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_299" n="HIAT:w" s="T79">baran</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_302" n="HIAT:w" s="T80">bulbuttar</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_305" n="HIAT:w" s="T81">kajdak</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_308" n="HIAT:w" s="T82">haŋarbɨtɨn</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_311" n="HIAT:w" s="T83">kördük</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_314" n="HIAT:w" s="T84">Isuːs</ts>
                  <nts id="Seg_315" n="HIAT:ip">.</nts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_318" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_320" n="HIAT:w" s="T85">Onno</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_323" n="HIAT:w" s="T86">belenneːbitter</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_326" n="HIAT:w" s="T87">Paːskanɨ</ts>
                  <nts id="Seg_327" n="HIAT:ip">.</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_330" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_332" n="HIAT:w" s="T88">Kaččaga</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_335" n="HIAT:w" s="T89">bu͡olbutugar</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_338" n="HIAT:w" s="T90">körsör</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_341" n="HIAT:w" s="T91">kemnere</ts>
                  <nts id="Seg_342" n="HIAT:ip">,</nts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_345" n="HIAT:w" s="T92">Isuːs</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_348" n="HIAT:w" s="T93">olorbut</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_351" n="HIAT:w" s="T94">ostol</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_354" n="HIAT:w" s="T95">inniger</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_357" n="HIAT:w" s="T96">u͡on</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_360" n="HIAT:w" s="T97">ikki</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_363" n="HIAT:w" s="T98">ü͡öreter</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_366" n="HIAT:w" s="T99">kihilerin</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_369" n="HIAT:w" s="T100">gɨtta</ts>
                  <nts id="Seg_370" n="HIAT:ip">,</nts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_373" n="HIAT:w" s="T101">onno</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_376" n="HIAT:w" s="T102">haŋarbɨt</ts>
                  <nts id="Seg_377" n="HIAT:ip">:</nts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_380" n="HIAT:u" s="T103">
                  <nts id="Seg_381" n="HIAT:ip">"</nts>
                  <ts e="T104" id="Seg_383" n="HIAT:w" s="T103">Min</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_386" n="HIAT:w" s="T104">olus</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_389" n="HIAT:w" s="T105">bagardɨm</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_392" n="HIAT:w" s="T106">ahaːrɨ</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_395" n="HIAT:w" s="T107">ehini</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_398" n="HIAT:w" s="T108">gɨtta</ts>
                  <nts id="Seg_399" n="HIAT:ip">,</nts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_402" n="HIAT:w" s="T109">min</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_405" n="HIAT:w" s="T110">erejdenerim</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_408" n="HIAT:w" s="T111">inniger</ts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_411" n="HIAT:w" s="T112">haŋarabɨn</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_414" n="HIAT:w" s="T113">ehi͡eke</ts>
                  <nts id="Seg_415" n="HIAT:ip">:</nts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T122" id="Seg_418" n="HIAT:u" s="T114">
                  <ts e="T115" id="Seg_420" n="HIAT:w" s="T114">Ahɨ͡am</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_423" n="HIAT:w" s="T115">hu͡oga</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_426" n="HIAT:w" s="T116">itini</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_429" n="HIAT:w" s="T117">tu͡ok</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_432" n="HIAT:w" s="T118">bu͡olan</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_435" n="HIAT:w" s="T119">iligine</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_438" n="HIAT:w" s="T120">taŋara</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_441" n="HIAT:w" s="T121">ɨraːktaːgɨtɨgar</ts>
                  <nts id="Seg_442" n="HIAT:ip">.</nts>
                  <nts id="Seg_443" n="HIAT:ip">"</nts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T128" id="Seg_446" n="HIAT:u" s="T122">
                  <ts e="T123" id="Seg_448" n="HIAT:w" s="T122">Čaːskɨnɨ</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_451" n="HIAT:w" s="T123">ɨlaːt</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_454" n="HIAT:w" s="T124">ötü͡ö</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_457" n="HIAT:w" s="T125">haŋatɨn</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_460" n="HIAT:w" s="T126">haŋaran</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_463" n="HIAT:w" s="T127">di͡ebit</ts>
                  <nts id="Seg_464" n="HIAT:ip">:</nts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T131" id="Seg_467" n="HIAT:u" s="T128">
                  <nts id="Seg_468" n="HIAT:ip">"</nts>
                  <ts e="T129" id="Seg_470" n="HIAT:w" s="T128">ɨlɨŋ</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_473" n="HIAT:w" s="T129">itini</ts>
                  <nts id="Seg_474" n="HIAT:ip">,</nts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_477" n="HIAT:w" s="T130">üllestiŋ</ts>
                  <nts id="Seg_478" n="HIAT:ip">.</nts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T144" id="Seg_481" n="HIAT:u" s="T131">
                  <ts e="T132" id="Seg_483" n="HIAT:w" s="T131">Haŋarabɨn</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_486" n="HIAT:w" s="T132">ehi͡eke</ts>
                  <nts id="Seg_487" n="HIAT:ip">,</nts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_490" n="HIAT:w" s="T133">ihi͡em</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_493" n="HIAT:w" s="T134">hu͡oga</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_495" n="HIAT:ip">(</nts>
                  <ts e="T137" id="Seg_497" n="HIAT:w" s="T135">vʼi-</ts>
                  <nts id="Seg_498" n="HIAT:ip">)</nts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_501" n="HIAT:w" s="T137">viʼnagraːd</ts>
                  <nts id="Seg_502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_504" n="HIAT:w" s="T138">hugun</ts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_507" n="HIAT:w" s="T139">aragiːtɨn</ts>
                  <nts id="Seg_508" n="HIAT:ip">,</nts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_511" n="HIAT:w" s="T140">kele</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_514" n="HIAT:w" s="T141">iligine</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_517" n="HIAT:w" s="T142">taŋara</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_520" n="HIAT:w" s="T143">ɨraːktaːgɨta</ts>
                  <nts id="Seg_521" n="HIAT:ip">.</nts>
                  <nts id="Seg_522" n="HIAT:ip">"</nts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T155" id="Seg_525" n="HIAT:u" s="T144">
                  <ts e="T145" id="Seg_527" n="HIAT:w" s="T144">Kileːbi</ts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_530" n="HIAT:w" s="T145">ɨlaːt</ts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_533" n="HIAT:w" s="T146">ötü͡ö</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_536" n="HIAT:w" s="T147">haŋanɨ</ts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_539" n="HIAT:w" s="T148">haŋaran</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_542" n="HIAT:w" s="T149">baraːn</ts>
                  <nts id="Seg_543" n="HIAT:ip">,</nts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_546" n="HIAT:w" s="T150">onu</ts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_549" n="HIAT:w" s="T151">tohutan</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_552" n="HIAT:w" s="T152">üllesteːbit</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_555" n="HIAT:w" s="T153">ginilerge</ts>
                  <nts id="Seg_556" n="HIAT:ip">,</nts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_559" n="HIAT:w" s="T154">haŋaran</ts>
                  <nts id="Seg_560" n="HIAT:ip">:</nts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T164" id="Seg_563" n="HIAT:u" s="T155">
                  <nts id="Seg_564" n="HIAT:ip">"</nts>
                  <ts e="T156" id="Seg_566" n="HIAT:w" s="T155">Iti</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_569" n="HIAT:w" s="T156">bu͡olar</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_572" n="HIAT:w" s="T157">min</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_575" n="HIAT:w" s="T158">etim</ts>
                  <nts id="Seg_576" n="HIAT:ip">,</nts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_579" n="HIAT:w" s="T159">tu͡ok</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_582" n="HIAT:w" s="T160">ehigi</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_585" n="HIAT:w" s="T161">tuskutugar</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_588" n="HIAT:w" s="T162">ölüːge</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_591" n="HIAT:w" s="T163">bi͡eriller</ts>
                  <nts id="Seg_592" n="HIAT:ip">.</nts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T169" id="Seg_595" n="HIAT:u" s="T164">
                  <ts e="T165" id="Seg_597" n="HIAT:w" s="T164">Itini</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_600" n="HIAT:w" s="T165">hi͡eŋ</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_603" n="HIAT:w" s="T166">minigin</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_606" n="HIAT:w" s="T167">öjdüːr</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_609" n="HIAT:w" s="T168">aːkka</ts>
                  <nts id="Seg_610" n="HIAT:ip">.</nts>
                  <nts id="Seg_611" n="HIAT:ip">"</nts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T177" id="Seg_614" n="HIAT:u" s="T169">
                  <ts e="T170" id="Seg_616" n="HIAT:w" s="T169">Itigirdik</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_619" n="HIAT:w" s="T170">hin</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_622" n="HIAT:w" s="T171">čaːskɨ</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_625" n="HIAT:w" s="T172">tuhunan</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_628" n="HIAT:w" s="T173">haŋarbɨta</ts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_631" n="HIAT:w" s="T174">körsüː</ts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_634" n="HIAT:w" s="T175">aːspɨtɨn</ts>
                  <nts id="Seg_635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_637" n="HIAT:w" s="T176">kenne</ts>
                  <nts id="Seg_638" n="HIAT:ip">.</nts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_641" n="HIAT:u" s="T177">
                  <nts id="Seg_642" n="HIAT:ip">"</nts>
                  <ts e="T178" id="Seg_644" n="HIAT:w" s="T177">Bu</ts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_647" n="HIAT:w" s="T178">čaːskɨ</ts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_650" n="HIAT:w" s="T179">bu͡olar</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_652" n="HIAT:ip">–</nts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_655" n="HIAT:w" s="T180">haŋa</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_658" n="HIAT:w" s="T181">kepsetiː</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_661" n="HIAT:w" s="T182">min</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_664" n="HIAT:w" s="T183">kaːmmar</ts>
                  <nts id="Seg_665" n="HIAT:ip">,</nts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_668" n="HIAT:w" s="T184">tu͡ok</ts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_671" n="HIAT:w" s="T185">toktor</ts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_674" n="HIAT:w" s="T186">ehigi</ts>
                  <nts id="Seg_675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_677" n="HIAT:w" s="T187">tuskutugar</ts>
                  <nts id="Seg_678" n="HIAT:ip">.</nts>
                  <nts id="Seg_679" n="HIAT:ip">"</nts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T188" id="Seg_681" n="sc" s="T0">
               <ts e="T1" id="Seg_683" n="e" s="T0">Paːsxa </ts>
               <ts e="T2" id="Seg_685" n="e" s="T1">taŋara </ts>
               <ts e="T3" id="Seg_687" n="e" s="T2">bu͡olbutugar </ts>
               <ts e="T4" id="Seg_689" n="e" s="T3">Isuːs </ts>
               <ts e="T5" id="Seg_691" n="e" s="T4">ɨjɨppɨt </ts>
               <ts e="T6" id="Seg_693" n="e" s="T5">Pʼötrtan </ts>
               <ts e="T7" id="Seg_695" n="e" s="T6">gɨtta </ts>
               <ts e="T8" id="Seg_697" n="e" s="T7">Ujbaːntan </ts>
               <ts e="T9" id="Seg_699" n="e" s="T8">haŋaran: </ts>
               <ts e="T10" id="Seg_701" n="e" s="T9">"(Barɨː) </ts>
               <ts e="T11" id="Seg_703" n="e" s="T10">barɨŋ </ts>
               <ts e="T12" id="Seg_705" n="e" s="T11">belemneːŋ </ts>
               <ts e="T13" id="Seg_707" n="e" s="T12">Paːsxaga </ts>
               <ts e="T14" id="Seg_709" n="e" s="T13">ahɨːrbɨtɨn". </ts>
               <ts e="T15" id="Seg_711" n="e" s="T14">Giniler </ts>
               <ts e="T16" id="Seg_713" n="e" s="T15">bu͡ollaktarɨna </ts>
               <ts e="T17" id="Seg_715" n="e" s="T16">ɨjɨppɨttar </ts>
               <ts e="T18" id="Seg_717" n="e" s="T17">Isuːstan: </ts>
               <ts e="T19" id="Seg_719" n="e" s="T18">"Kanna </ts>
               <ts e="T20" id="Seg_721" n="e" s="T19">belemnetegin </ts>
               <ts e="T21" id="Seg_723" n="e" s="T20">di͡en?" </ts>
               <ts e="T22" id="Seg_725" n="e" s="T21">Isuːs </ts>
               <ts e="T23" id="Seg_727" n="e" s="T22">di͡ebit: </ts>
               <ts e="T24" id="Seg_729" n="e" s="T23">"Onno. </ts>
               <ts e="T25" id="Seg_731" n="e" s="T24">Ehigi </ts>
               <ts e="T26" id="Seg_733" n="e" s="T25">gu͡orakka </ts>
               <ts e="T27" id="Seg_735" n="e" s="T26">kiːreŋŋit, </ts>
               <ts e="T28" id="Seg_737" n="e" s="T27">kiːren </ts>
               <ts e="T30" id="Seg_739" n="e" s="T28">(hirbi-) </ts>
               <ts e="T31" id="Seg_741" n="e" s="T30">hirgitiger </ts>
               <ts e="T32" id="Seg_743" n="e" s="T31">körsü͡ökküt </ts>
               <ts e="T33" id="Seg_745" n="e" s="T32">biːr </ts>
               <ts e="T34" id="Seg_747" n="e" s="T33">kihini </ts>
               <ts e="T35" id="Seg_749" n="e" s="T34">gɨtta, </ts>
               <ts e="T36" id="Seg_751" n="e" s="T35">illen </ts>
               <ts e="T37" id="Seg_753" n="e" s="T36">iheri </ts>
               <ts e="T38" id="Seg_755" n="e" s="T37">uːlaːk </ts>
               <ts e="T39" id="Seg_757" n="e" s="T38">ihiti. </ts>
               <ts e="T40" id="Seg_759" n="e" s="T39">Ginini </ts>
               <ts e="T41" id="Seg_761" n="e" s="T40">batɨhɨŋ </ts>
               <ts e="T42" id="Seg_763" n="e" s="T41">dʼi͡ege </ts>
               <ts e="T43" id="Seg_765" n="e" s="T42">di͡eri. </ts>
               <ts e="T44" id="Seg_767" n="e" s="T43">Kanna </ts>
               <ts e="T45" id="Seg_769" n="e" s="T44">kiːri͡ege </ts>
               <ts e="T46" id="Seg_771" n="e" s="T45">gini, </ts>
               <ts e="T47" id="Seg_773" n="e" s="T46">ol </ts>
               <ts e="T48" id="Seg_775" n="e" s="T47">dʼi͡ege </ts>
               <ts e="T49" id="Seg_777" n="e" s="T48">küseːjniger </ts>
               <ts e="T50" id="Seg_779" n="e" s="T49">haŋarɨŋ. </ts>
               <ts e="T51" id="Seg_781" n="e" s="T50">"Učʼitʼelʼ </ts>
               <ts e="T52" id="Seg_783" n="e" s="T51">ɨjɨtar", </ts>
               <ts e="T53" id="Seg_785" n="e" s="T52">di͡eŋ, </ts>
               <ts e="T54" id="Seg_787" n="e" s="T53">"enigitten, </ts>
               <ts e="T55" id="Seg_789" n="e" s="T54">"kanna </ts>
               <ts e="T56" id="Seg_791" n="e" s="T55">baːrɨj </ts>
               <ts e="T57" id="Seg_793" n="e" s="T56">dʼi͡e </ts>
               <ts e="T58" id="Seg_795" n="e" s="T57">koho, </ts>
               <ts e="T59" id="Seg_797" n="e" s="T58">min </ts>
               <ts e="T60" id="Seg_799" n="e" s="T59">Paːsxaga </ts>
               <ts e="T61" id="Seg_801" n="e" s="T60">ahɨːrbar </ts>
               <ts e="T62" id="Seg_803" n="e" s="T61">bejem </ts>
               <ts e="T63" id="Seg_805" n="e" s="T62">ü͡öreter </ts>
               <ts e="T64" id="Seg_807" n="e" s="T63">dʼommun </ts>
               <ts e="T65" id="Seg_809" n="e" s="T64">gɨtta?" </ts>
               <ts e="T66" id="Seg_811" n="e" s="T65">Gini </ts>
               <ts e="T67" id="Seg_813" n="e" s="T66">köllörü͡öge </ts>
               <ts e="T68" id="Seg_815" n="e" s="T67">ehi͡eke </ts>
               <ts e="T69" id="Seg_817" n="e" s="T68">ki͡eŋ </ts>
               <ts e="T70" id="Seg_819" n="e" s="T69">bagajɨː </ts>
               <ts e="T71" id="Seg_821" n="e" s="T70">dʼi͡e </ts>
               <ts e="T72" id="Seg_823" n="e" s="T71">kohun. </ts>
               <ts e="T73" id="Seg_825" n="e" s="T72">Mu͡ostata </ts>
               <ts e="T74" id="Seg_827" n="e" s="T73">habɨːlaːgɨ. </ts>
               <ts e="T75" id="Seg_829" n="e" s="T74">Onno </ts>
               <ts e="T76" id="Seg_831" n="e" s="T75">belemneːŋ." </ts>
               <ts e="T77" id="Seg_833" n="e" s="T76">Ü͡öreter </ts>
               <ts e="T78" id="Seg_835" n="e" s="T77">dʼono </ts>
               <ts e="T79" id="Seg_837" n="e" s="T78">onno </ts>
               <ts e="T80" id="Seg_839" n="e" s="T79">baran </ts>
               <ts e="T81" id="Seg_841" n="e" s="T80">bulbuttar </ts>
               <ts e="T82" id="Seg_843" n="e" s="T81">kajdak </ts>
               <ts e="T83" id="Seg_845" n="e" s="T82">haŋarbɨtɨn </ts>
               <ts e="T84" id="Seg_847" n="e" s="T83">kördük </ts>
               <ts e="T85" id="Seg_849" n="e" s="T84">Isuːs. </ts>
               <ts e="T86" id="Seg_851" n="e" s="T85">Onno </ts>
               <ts e="T87" id="Seg_853" n="e" s="T86">belenneːbitter </ts>
               <ts e="T88" id="Seg_855" n="e" s="T87">Paːskanɨ. </ts>
               <ts e="T89" id="Seg_857" n="e" s="T88">Kaččaga </ts>
               <ts e="T90" id="Seg_859" n="e" s="T89">bu͡olbutugar </ts>
               <ts e="T91" id="Seg_861" n="e" s="T90">körsör </ts>
               <ts e="T92" id="Seg_863" n="e" s="T91">kemnere, </ts>
               <ts e="T93" id="Seg_865" n="e" s="T92">Isuːs </ts>
               <ts e="T94" id="Seg_867" n="e" s="T93">olorbut </ts>
               <ts e="T95" id="Seg_869" n="e" s="T94">ostol </ts>
               <ts e="T96" id="Seg_871" n="e" s="T95">inniger </ts>
               <ts e="T97" id="Seg_873" n="e" s="T96">u͡on </ts>
               <ts e="T98" id="Seg_875" n="e" s="T97">ikki </ts>
               <ts e="T99" id="Seg_877" n="e" s="T98">ü͡öreter </ts>
               <ts e="T100" id="Seg_879" n="e" s="T99">kihilerin </ts>
               <ts e="T101" id="Seg_881" n="e" s="T100">gɨtta, </ts>
               <ts e="T102" id="Seg_883" n="e" s="T101">onno </ts>
               <ts e="T103" id="Seg_885" n="e" s="T102">haŋarbɨt: </ts>
               <ts e="T104" id="Seg_887" n="e" s="T103">"Min </ts>
               <ts e="T105" id="Seg_889" n="e" s="T104">olus </ts>
               <ts e="T106" id="Seg_891" n="e" s="T105">bagardɨm </ts>
               <ts e="T107" id="Seg_893" n="e" s="T106">ahaːrɨ </ts>
               <ts e="T108" id="Seg_895" n="e" s="T107">ehini </ts>
               <ts e="T109" id="Seg_897" n="e" s="T108">gɨtta, </ts>
               <ts e="T110" id="Seg_899" n="e" s="T109">min </ts>
               <ts e="T111" id="Seg_901" n="e" s="T110">erejdenerim </ts>
               <ts e="T112" id="Seg_903" n="e" s="T111">inniger </ts>
               <ts e="T113" id="Seg_905" n="e" s="T112">haŋarabɨn </ts>
               <ts e="T114" id="Seg_907" n="e" s="T113">ehi͡eke: </ts>
               <ts e="T115" id="Seg_909" n="e" s="T114">Ahɨ͡am </ts>
               <ts e="T116" id="Seg_911" n="e" s="T115">hu͡oga </ts>
               <ts e="T117" id="Seg_913" n="e" s="T116">itini </ts>
               <ts e="T118" id="Seg_915" n="e" s="T117">tu͡ok </ts>
               <ts e="T119" id="Seg_917" n="e" s="T118">bu͡olan </ts>
               <ts e="T120" id="Seg_919" n="e" s="T119">iligine </ts>
               <ts e="T121" id="Seg_921" n="e" s="T120">taŋara </ts>
               <ts e="T122" id="Seg_923" n="e" s="T121">ɨraːktaːgɨtɨgar." </ts>
               <ts e="T123" id="Seg_925" n="e" s="T122">Čaːskɨnɨ </ts>
               <ts e="T124" id="Seg_927" n="e" s="T123">ɨlaːt </ts>
               <ts e="T125" id="Seg_929" n="e" s="T124">ötü͡ö </ts>
               <ts e="T126" id="Seg_931" n="e" s="T125">haŋatɨn </ts>
               <ts e="T127" id="Seg_933" n="e" s="T126">haŋaran </ts>
               <ts e="T128" id="Seg_935" n="e" s="T127">di͡ebit: </ts>
               <ts e="T129" id="Seg_937" n="e" s="T128">"ɨlɨŋ </ts>
               <ts e="T130" id="Seg_939" n="e" s="T129">itini, </ts>
               <ts e="T131" id="Seg_941" n="e" s="T130">üllestiŋ. </ts>
               <ts e="T132" id="Seg_943" n="e" s="T131">Haŋarabɨn </ts>
               <ts e="T133" id="Seg_945" n="e" s="T132">ehi͡eke, </ts>
               <ts e="T134" id="Seg_947" n="e" s="T133">ihi͡em </ts>
               <ts e="T135" id="Seg_949" n="e" s="T134">hu͡oga </ts>
               <ts e="T137" id="Seg_951" n="e" s="T135">(vʼi-) </ts>
               <ts e="T138" id="Seg_953" n="e" s="T137">viʼnagraːd </ts>
               <ts e="T139" id="Seg_955" n="e" s="T138">hugun </ts>
               <ts e="T140" id="Seg_957" n="e" s="T139">aragiːtɨn, </ts>
               <ts e="T141" id="Seg_959" n="e" s="T140">kele </ts>
               <ts e="T142" id="Seg_961" n="e" s="T141">iligine </ts>
               <ts e="T143" id="Seg_963" n="e" s="T142">taŋara </ts>
               <ts e="T144" id="Seg_965" n="e" s="T143">ɨraːktaːgɨta." </ts>
               <ts e="T145" id="Seg_967" n="e" s="T144">Kileːbi </ts>
               <ts e="T146" id="Seg_969" n="e" s="T145">ɨlaːt </ts>
               <ts e="T147" id="Seg_971" n="e" s="T146">ötü͡ö </ts>
               <ts e="T148" id="Seg_973" n="e" s="T147">haŋanɨ </ts>
               <ts e="T149" id="Seg_975" n="e" s="T148">haŋaran </ts>
               <ts e="T150" id="Seg_977" n="e" s="T149">baraːn, </ts>
               <ts e="T151" id="Seg_979" n="e" s="T150">onu </ts>
               <ts e="T152" id="Seg_981" n="e" s="T151">tohutan </ts>
               <ts e="T153" id="Seg_983" n="e" s="T152">üllesteːbit </ts>
               <ts e="T154" id="Seg_985" n="e" s="T153">ginilerge, </ts>
               <ts e="T155" id="Seg_987" n="e" s="T154">haŋaran: </ts>
               <ts e="T156" id="Seg_989" n="e" s="T155">"Iti </ts>
               <ts e="T157" id="Seg_991" n="e" s="T156">bu͡olar </ts>
               <ts e="T158" id="Seg_993" n="e" s="T157">min </ts>
               <ts e="T159" id="Seg_995" n="e" s="T158">etim, </ts>
               <ts e="T160" id="Seg_997" n="e" s="T159">tu͡ok </ts>
               <ts e="T161" id="Seg_999" n="e" s="T160">ehigi </ts>
               <ts e="T162" id="Seg_1001" n="e" s="T161">tuskutugar </ts>
               <ts e="T163" id="Seg_1003" n="e" s="T162">ölüːge </ts>
               <ts e="T164" id="Seg_1005" n="e" s="T163">bi͡eriller. </ts>
               <ts e="T165" id="Seg_1007" n="e" s="T164">Itini </ts>
               <ts e="T166" id="Seg_1009" n="e" s="T165">hi͡eŋ </ts>
               <ts e="T167" id="Seg_1011" n="e" s="T166">minigin </ts>
               <ts e="T168" id="Seg_1013" n="e" s="T167">öjdüːr </ts>
               <ts e="T169" id="Seg_1015" n="e" s="T168">aːkka." </ts>
               <ts e="T170" id="Seg_1017" n="e" s="T169">Itigirdik </ts>
               <ts e="T171" id="Seg_1019" n="e" s="T170">hin </ts>
               <ts e="T172" id="Seg_1021" n="e" s="T171">čaːskɨ </ts>
               <ts e="T173" id="Seg_1023" n="e" s="T172">tuhunan </ts>
               <ts e="T174" id="Seg_1025" n="e" s="T173">haŋarbɨta </ts>
               <ts e="T175" id="Seg_1027" n="e" s="T174">körsüː </ts>
               <ts e="T176" id="Seg_1029" n="e" s="T175">aːspɨtɨn </ts>
               <ts e="T177" id="Seg_1031" n="e" s="T176">kenne. </ts>
               <ts e="T178" id="Seg_1033" n="e" s="T177">"Bu </ts>
               <ts e="T179" id="Seg_1035" n="e" s="T178">čaːskɨ </ts>
               <ts e="T180" id="Seg_1037" n="e" s="T179">bu͡olar – </ts>
               <ts e="T181" id="Seg_1039" n="e" s="T180">haŋa </ts>
               <ts e="T182" id="Seg_1041" n="e" s="T181">kepsetiː </ts>
               <ts e="T183" id="Seg_1043" n="e" s="T182">min </ts>
               <ts e="T184" id="Seg_1045" n="e" s="T183">kaːmmar, </ts>
               <ts e="T185" id="Seg_1047" n="e" s="T184">tu͡ok </ts>
               <ts e="T186" id="Seg_1049" n="e" s="T185">toktor </ts>
               <ts e="T187" id="Seg_1051" n="e" s="T186">ehigi </ts>
               <ts e="T188" id="Seg_1053" n="e" s="T187">tuskutugar." </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T9" id="Seg_1054" s="T0">PoNA_19940110_LastSupper_transl.001 (001.001)</ta>
            <ta e="T14" id="Seg_1055" s="T9">PoNA_19940110_LastSupper_transl.002 (001.002)</ta>
            <ta e="T18" id="Seg_1056" s="T14">PoNA_19940110_LastSupper_transl.003 (001.003)</ta>
            <ta e="T21" id="Seg_1057" s="T18">PoNA_19940110_LastSupper_transl.004 (001.003)</ta>
            <ta e="T23" id="Seg_1058" s="T21">PoNA_19940110_LastSupper_transl.005 (001.004)</ta>
            <ta e="T24" id="Seg_1059" s="T23">PoNA_19940110_LastSupper_transl.006 (001.004)</ta>
            <ta e="T39" id="Seg_1060" s="T24">PoNA_19940110_LastSupper_transl.007 (001.005)</ta>
            <ta e="T43" id="Seg_1061" s="T39">PoNA_19940110_LastSupper_transl.008 (001.006)</ta>
            <ta e="T50" id="Seg_1062" s="T43">PoNA_19940110_LastSupper_transl.009 (001.007)</ta>
            <ta e="T65" id="Seg_1063" s="T50">PoNA_19940110_LastSupper_transl.010 (001.008)</ta>
            <ta e="T72" id="Seg_1064" s="T65">PoNA_19940110_LastSupper_transl.011 (001.009)</ta>
            <ta e="T74" id="Seg_1065" s="T72">PoNA_19940110_LastSupper_transl.012 (001.010)</ta>
            <ta e="T76" id="Seg_1066" s="T74">PoNA_19940110_LastSupper_transl.013 (001.011)</ta>
            <ta e="T85" id="Seg_1067" s="T76">PoNA_19940110_LastSupper_transl.014 (001.012)</ta>
            <ta e="T88" id="Seg_1068" s="T85">PoNA_19940110_LastSupper_transl.015 (001.013)</ta>
            <ta e="T103" id="Seg_1069" s="T88">PoNA_19940110_LastSupper_transl.016 (001.014)</ta>
            <ta e="T114" id="Seg_1070" s="T103">PoNA_19940110_LastSupper_transl.017 (001.015)</ta>
            <ta e="T122" id="Seg_1071" s="T114">PoNA_19940110_LastSupper_transl.018 (001.016)</ta>
            <ta e="T128" id="Seg_1072" s="T122">PoNA_19940110_LastSupper_transl.019 (001.017)</ta>
            <ta e="T131" id="Seg_1073" s="T128">PoNA_19940110_LastSupper_transl.020 (001.017)</ta>
            <ta e="T144" id="Seg_1074" s="T131">PoNA_19940110_LastSupper_transl.021 (001.018)</ta>
            <ta e="T155" id="Seg_1075" s="T144">PoNA_19940110_LastSupper_transl.022 (001.019)</ta>
            <ta e="T164" id="Seg_1076" s="T155">PoNA_19940110_LastSupper_transl.023 (001.020)</ta>
            <ta e="T169" id="Seg_1077" s="T164">PoNA_19940110_LastSupper_transl.024 (001.021)</ta>
            <ta e="T177" id="Seg_1078" s="T169">PoNA_19940110_LastSupper_transl.025 (001.022)</ta>
            <ta e="T188" id="Seg_1079" s="T177">PoNA_19940110_LastSupper_transl.026 (001.023)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T9" id="Seg_1080" s="T0">Паасха таӈара буолбутугар Иисус ыйыппыт Пөтртан гытта Уйбаантан һаӈаран: </ta>
            <ta e="T14" id="Seg_1081" s="T9">"(Бары) барыӈ бэлэмнээӈ Паасхага аһыырбытын".</ta>
            <ta e="T18" id="Seg_1082" s="T14">Гинилэр буоллактарына ыыппыттар Иисустан: </ta>
            <ta e="T21" id="Seg_1083" s="T18">"Канна бэлэмнэтэгин диэн?"</ta>
            <ta e="T23" id="Seg_1084" s="T21">Иисус диэбит: </ta>
            <ta e="T24" id="Seg_1085" s="T23">"Онно.</ta>
            <ta e="T39" id="Seg_1086" s="T24">Эһиги гуородка киирэӈӈит, киирэн һирбитигэр көрсүөккүт биир киһини гытта, иллэн иһэри уулаак иһити.</ta>
            <ta e="T43" id="Seg_1087" s="T39">Гинини батыһыӈ дьиэгэ диэри.</ta>
            <ta e="T50" id="Seg_1088" s="T43">Канна киириэги гини, ол дьигэ күсээинигэр һаӈарыӈ.</ta>
            <ta e="T65" id="Seg_1089" s="T50">"Учитель ыйытар, – диэӈ, – энигиттэн, канна баарый дьиэ коһо, мин Паасхага аһыырбар бэйэм үөрэтэр дьонмун гытта?"</ta>
            <ta e="T72" id="Seg_1090" s="T65">Гини көллөрүөгэ эһиэкэ киэӈ багайыы дьиэ коһун.</ta>
            <ta e="T74" id="Seg_1091" s="T72">Муостата һабыылаагы.</ta>
            <ta e="T76" id="Seg_1092" s="T74">Онно бэлэмнээӈ."</ta>
            <ta e="T85" id="Seg_1093" s="T76">Үөрэтэр дьону онно баран булбуттар кайдак һаӈарбытын көрдүк Иисус.</ta>
            <ta e="T88" id="Seg_1094" s="T85">Онно бэлэннээбиттэр паасканы.</ta>
            <ta e="T103" id="Seg_1095" s="T88">Каччага буолбутугар көрсөр кэмнэрэ, Иисус олорбут остол иннигэр уон икки үөрэтэр киһилэрин гытта, онно һаӈарбыт.</ta>
            <ta e="T114" id="Seg_1096" s="T103">"Мин олус багардым аһаары эһини гытта, мин эрэйдэнэрим иннигэр һаӈарабын эһиэкэ: </ta>
            <ta e="T122" id="Seg_1097" s="T114">Аһыам һуога итини туок буолан илигинэ таӈара ыраактаагытыгар."</ta>
            <ta e="T128" id="Seg_1098" s="T122">Чааскыны ылаат өтүө һаӈатын һаӈаран диэбит: </ta>
            <ta e="T131" id="Seg_1099" s="T128">"Ылыӈ итини, үллэстиӈ.</ta>
            <ta e="T144" id="Seg_1100" s="T131">Һаӈарабын эһиэкэ, иһиэм һуога винограад һугун арагиитын, кэлэ илигинэ таӈара ыраактаагыта."</ta>
            <ta e="T155" id="Seg_1101" s="T144">Килээби ылаат өтүө (үтүө) һаӈаны һаӈаран бараан, ону тоһутан үллэстээбит (үллэрбит) гинилэргэ, һаӈаран: </ta>
            <ta e="T164" id="Seg_1102" s="T155">"Ити буолар мин этим, туок эһиги тускутугар өлүүгэ биэриллэр.</ta>
            <ta e="T169" id="Seg_1103" s="T164">Итини һиэк минигин өйдүүр аакка."</ta>
            <ta e="T177" id="Seg_1104" s="T169">Итигирдик һин чааскы туһунан һаӈарбыта көрсүү ааспытын кэннэ.</ta>
            <ta e="T188" id="Seg_1105" s="T177">"Бу чааскы буолар – һаӈа кэпсэтии мин кааммар, туок токтор эһиги тускутугар."</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T9" id="Seg_1106" s="T0">Paːsxa taŋara bu͡olbutugar Isuːs ɨjɨppɨt Pʼötrtan gɨtta Ujbaːntan haŋaran: </ta>
            <ta e="T14" id="Seg_1107" s="T9">"(Barɨː) barɨŋ belemneːŋ Paːsxaga ahɨːrbɨtɨn". </ta>
            <ta e="T18" id="Seg_1108" s="T14">Giniler bu͡ollaktarɨna ɨjɨppɨttar Isuːstan: </ta>
            <ta e="T21" id="Seg_1109" s="T18">"Kanna belemnetegin di͡en?" </ta>
            <ta e="T23" id="Seg_1110" s="T21">Isuːs di͡ebit: </ta>
            <ta e="T24" id="Seg_1111" s="T23">"Onno. </ta>
            <ta e="T39" id="Seg_1112" s="T24">Ehigi gu͡orakka kiːreŋŋit, kiːren (hirbi-) hirgitiger körsü͡ökküt biːr kihini gɨtta, illen iheri uːlaːk ihiti. </ta>
            <ta e="T43" id="Seg_1113" s="T39">Ginini batɨhɨŋ dʼi͡ege di͡eri. </ta>
            <ta e="T50" id="Seg_1114" s="T43">Kanna kiːri͡ege gini, ol dʼi͡ege küseːjniger haŋarɨŋ. </ta>
            <ta e="T65" id="Seg_1115" s="T50">"Učʼitʼelʼ ɨjɨtar", di͡eŋ, "enigitten, "kanna baːrɨj dʼi͡e koho, min Paːsxaga ahɨːrbar bejem ü͡öreter dʼommun gɨtta?" </ta>
            <ta e="T72" id="Seg_1116" s="T65">Gini köllörü͡öge ehi͡eke ki͡eŋ bagajɨː dʼi͡e kohun. </ta>
            <ta e="T74" id="Seg_1117" s="T72">Mu͡ostata habɨːlaːgɨ. </ta>
            <ta e="T76" id="Seg_1118" s="T74">Onno belemneːŋ." </ta>
            <ta e="T85" id="Seg_1119" s="T76">Ü͡öreter dʼono onno baran bulbuttar kajdak haŋarbɨtɨn kördük Isuːs. </ta>
            <ta e="T88" id="Seg_1120" s="T85">Onno belenneːbitter Paːskanɨ. </ta>
            <ta e="T103" id="Seg_1121" s="T88">Kaččaga bu͡olbutugar körsör kemnere, Isuːs olorbut ostol inniger u͡on ikki ü͡öreter kihilerin gɨtta, onno haŋarbɨt: </ta>
            <ta e="T114" id="Seg_1122" s="T103">"Min olus bagardɨm ahaːrɨ ehini gɨtta, min erejdenerim inniger haŋarabɨn ehi͡eke: </ta>
            <ta e="T122" id="Seg_1123" s="T114">Ahɨ͡am hu͡oga itini tu͡ok bu͡olan iligine taŋara ɨraːktaːgɨtɨgar." </ta>
            <ta e="T128" id="Seg_1124" s="T122">Čaːskɨnɨ ɨlaːt ötü͡ö haŋatɨn haŋaran di͡ebit: </ta>
            <ta e="T131" id="Seg_1125" s="T128">"ɨlɨŋ itini, üllestiŋ. </ta>
            <ta e="T144" id="Seg_1126" s="T131">Haŋarabɨn ehi͡eke, ihi͡em hu͡oga (vʼi-) viʼnagraːd hugun aragiːtɨn, kele iligine taŋara ɨraːktaːgɨta." </ta>
            <ta e="T155" id="Seg_1127" s="T144">Kileːbi ɨlaːt ötü͡ö haŋanɨ haŋaran baraːn, onu tohutan üllesteːbit ginilerge, haŋaran: </ta>
            <ta e="T164" id="Seg_1128" s="T155">"Iti bu͡olar min etim, tu͡ok ehigi tuskutugar ölüːge bi͡eriller. </ta>
            <ta e="T169" id="Seg_1129" s="T164">Itini hi͡eŋ minigin öjdüːr aːkka." </ta>
            <ta e="T177" id="Seg_1130" s="T169">Itigirdik hin čaːskɨ tuhunan haŋarbɨta körsüː aːspɨtɨn kenne. </ta>
            <ta e="T188" id="Seg_1131" s="T177">"Bu čaːskɨ bu͡olar – haŋa kepsetiː min kaːmmar, tu͡ok toktor ehigi tuskutugar." </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1132" s="T0">Paːsxa</ta>
            <ta e="T2" id="Seg_1133" s="T1">taŋara</ta>
            <ta e="T3" id="Seg_1134" s="T2">bu͡ol-but-u-gar</ta>
            <ta e="T4" id="Seg_1135" s="T3">Isuːs</ta>
            <ta e="T5" id="Seg_1136" s="T4">ɨjɨp-pɨt</ta>
            <ta e="T6" id="Seg_1137" s="T5">Pʼötr-tan</ta>
            <ta e="T7" id="Seg_1138" s="T6">gɨtta</ta>
            <ta e="T8" id="Seg_1139" s="T7">Ujbaːn-tan</ta>
            <ta e="T9" id="Seg_1140" s="T8">haŋar-an</ta>
            <ta e="T11" id="Seg_1141" s="T10">bar-ɨ-ŋ</ta>
            <ta e="T12" id="Seg_1142" s="T11">belemneː-ŋ</ta>
            <ta e="T13" id="Seg_1143" s="T12">Paːsxa-ga</ta>
            <ta e="T14" id="Seg_1144" s="T13">ahɨː-r-bɨtɨ-n</ta>
            <ta e="T15" id="Seg_1145" s="T14">giniler</ta>
            <ta e="T16" id="Seg_1146" s="T15">bu͡ollaktarɨna</ta>
            <ta e="T17" id="Seg_1147" s="T16">ɨjɨp-pɨt-tar</ta>
            <ta e="T18" id="Seg_1148" s="T17">Isuːs-tan</ta>
            <ta e="T19" id="Seg_1149" s="T18">kanna</ta>
            <ta e="T20" id="Seg_1150" s="T19">belemn-e-t-e-gin</ta>
            <ta e="T21" id="Seg_1151" s="T20">di͡e-n</ta>
            <ta e="T22" id="Seg_1152" s="T21">Isuːs</ta>
            <ta e="T23" id="Seg_1153" s="T22">di͡e-bit</ta>
            <ta e="T24" id="Seg_1154" s="T23">onno</ta>
            <ta e="T25" id="Seg_1155" s="T24">ehigi</ta>
            <ta e="T26" id="Seg_1156" s="T25">gu͡orak-ka</ta>
            <ta e="T27" id="Seg_1157" s="T26">kiːr-eŋ-ŋit</ta>
            <ta e="T28" id="Seg_1158" s="T27">kiːr-en</ta>
            <ta e="T31" id="Seg_1159" s="T30">hir-giti-ger</ta>
            <ta e="T32" id="Seg_1160" s="T31">körs-ü͡ök-küt</ta>
            <ta e="T33" id="Seg_1161" s="T32">biːr</ta>
            <ta e="T34" id="Seg_1162" s="T33">kihi-ni</ta>
            <ta e="T35" id="Seg_1163" s="T34">gɨtta</ta>
            <ta e="T36" id="Seg_1164" s="T35">ill-en</ta>
            <ta e="T37" id="Seg_1165" s="T36">ih-er-i</ta>
            <ta e="T38" id="Seg_1166" s="T37">uː-laːk</ta>
            <ta e="T39" id="Seg_1167" s="T38">ihit-i</ta>
            <ta e="T40" id="Seg_1168" s="T39">gini-ni</ta>
            <ta e="T41" id="Seg_1169" s="T40">batɨh-ɨ-ŋ</ta>
            <ta e="T42" id="Seg_1170" s="T41">dʼi͡e-ge</ta>
            <ta e="T43" id="Seg_1171" s="T42">di͡eri</ta>
            <ta e="T44" id="Seg_1172" s="T43">kanna</ta>
            <ta e="T45" id="Seg_1173" s="T44">kiːr-i͡eg-e</ta>
            <ta e="T46" id="Seg_1174" s="T45">gini</ta>
            <ta e="T47" id="Seg_1175" s="T46">ol</ta>
            <ta e="T48" id="Seg_1176" s="T47">dʼi͡e-ge</ta>
            <ta e="T49" id="Seg_1177" s="T48">küseːjn-i-ger</ta>
            <ta e="T50" id="Seg_1178" s="T49">haŋar-ɨ-ŋ</ta>
            <ta e="T51" id="Seg_1179" s="T50">učʼitʼelʼ</ta>
            <ta e="T52" id="Seg_1180" s="T51">ɨjɨt-ar</ta>
            <ta e="T53" id="Seg_1181" s="T52">di͡e-ŋ</ta>
            <ta e="T54" id="Seg_1182" s="T53">enigi-tten</ta>
            <ta e="T55" id="Seg_1183" s="T54">kanna</ta>
            <ta e="T56" id="Seg_1184" s="T55">baːr=ɨj</ta>
            <ta e="T57" id="Seg_1185" s="T56">dʼi͡e</ta>
            <ta e="T58" id="Seg_1186" s="T57">koh-o</ta>
            <ta e="T59" id="Seg_1187" s="T58">min</ta>
            <ta e="T60" id="Seg_1188" s="T59">Paːsxa-ga</ta>
            <ta e="T61" id="Seg_1189" s="T60">ahɨː-r-ba-r</ta>
            <ta e="T62" id="Seg_1190" s="T61">beje-m</ta>
            <ta e="T63" id="Seg_1191" s="T62">ü͡öret-er</ta>
            <ta e="T64" id="Seg_1192" s="T63">dʼom-mu-n</ta>
            <ta e="T65" id="Seg_1193" s="T64">gɨtta</ta>
            <ta e="T66" id="Seg_1194" s="T65">gini</ta>
            <ta e="T67" id="Seg_1195" s="T66">köllör-ü͡ög-e</ta>
            <ta e="T68" id="Seg_1196" s="T67">ehi͡e-ke</ta>
            <ta e="T69" id="Seg_1197" s="T68">ki͡eŋ</ta>
            <ta e="T70" id="Seg_1198" s="T69">bagajɨː</ta>
            <ta e="T71" id="Seg_1199" s="T70">dʼi͡e</ta>
            <ta e="T72" id="Seg_1200" s="T71">koh-u-n</ta>
            <ta e="T73" id="Seg_1201" s="T72">mu͡osta-ta</ta>
            <ta e="T74" id="Seg_1202" s="T73">hab-ɨː-laːg-ɨ</ta>
            <ta e="T75" id="Seg_1203" s="T74">onno</ta>
            <ta e="T76" id="Seg_1204" s="T75">belemneː-ŋ</ta>
            <ta e="T77" id="Seg_1205" s="T76">ü͡öret-er</ta>
            <ta e="T78" id="Seg_1206" s="T77">dʼon-o</ta>
            <ta e="T79" id="Seg_1207" s="T78">onno</ta>
            <ta e="T80" id="Seg_1208" s="T79">bar-an</ta>
            <ta e="T81" id="Seg_1209" s="T80">bul-but-tar</ta>
            <ta e="T82" id="Seg_1210" s="T81">kajdak</ta>
            <ta e="T83" id="Seg_1211" s="T82">haŋar-bɨt-ɨ-n</ta>
            <ta e="T84" id="Seg_1212" s="T83">kördük</ta>
            <ta e="T85" id="Seg_1213" s="T84">Isuːs</ta>
            <ta e="T86" id="Seg_1214" s="T85">onno</ta>
            <ta e="T87" id="Seg_1215" s="T86">belenneː-bit-ter</ta>
            <ta e="T88" id="Seg_1216" s="T87">Paːska-nɨ</ta>
            <ta e="T89" id="Seg_1217" s="T88">kaččaga</ta>
            <ta e="T90" id="Seg_1218" s="T89">bu͡ol-but-u-gar</ta>
            <ta e="T91" id="Seg_1219" s="T90">körs-ör</ta>
            <ta e="T92" id="Seg_1220" s="T91">kem-nere</ta>
            <ta e="T93" id="Seg_1221" s="T92">Isuːs</ta>
            <ta e="T94" id="Seg_1222" s="T93">olor-but</ta>
            <ta e="T95" id="Seg_1223" s="T94">ostol</ta>
            <ta e="T96" id="Seg_1224" s="T95">inn-i-ger</ta>
            <ta e="T97" id="Seg_1225" s="T96">u͡on</ta>
            <ta e="T98" id="Seg_1226" s="T97">ikki</ta>
            <ta e="T99" id="Seg_1227" s="T98">ü͡öret-er</ta>
            <ta e="T100" id="Seg_1228" s="T99">kihi-ler-i-n</ta>
            <ta e="T101" id="Seg_1229" s="T100">gɨtta</ta>
            <ta e="T102" id="Seg_1230" s="T101">onno</ta>
            <ta e="T103" id="Seg_1231" s="T102">haŋar-bɨt</ta>
            <ta e="T104" id="Seg_1232" s="T103">min</ta>
            <ta e="T105" id="Seg_1233" s="T104">olus</ta>
            <ta e="T106" id="Seg_1234" s="T105">bagar-dɨ-m</ta>
            <ta e="T107" id="Seg_1235" s="T106">ah-aːrɨ</ta>
            <ta e="T108" id="Seg_1236" s="T107">ehi-ni</ta>
            <ta e="T109" id="Seg_1237" s="T108">gɨtta</ta>
            <ta e="T110" id="Seg_1238" s="T109">min</ta>
            <ta e="T111" id="Seg_1239" s="T110">erej-den-er-i-m</ta>
            <ta e="T112" id="Seg_1240" s="T111">inn-i-ger</ta>
            <ta e="T113" id="Seg_1241" s="T112">haŋar-a-bɨn</ta>
            <ta e="T114" id="Seg_1242" s="T113">ehi͡e-ke</ta>
            <ta e="T115" id="Seg_1243" s="T114">ah-ɨ͡a-m</ta>
            <ta e="T116" id="Seg_1244" s="T115">hu͡og-a</ta>
            <ta e="T117" id="Seg_1245" s="T116">iti-ni</ta>
            <ta e="T118" id="Seg_1246" s="T117">tu͡ok</ta>
            <ta e="T119" id="Seg_1247" s="T118">bu͡ol-an</ta>
            <ta e="T120" id="Seg_1248" s="T119">ilig-ine</ta>
            <ta e="T121" id="Seg_1249" s="T120">taŋara</ta>
            <ta e="T122" id="Seg_1250" s="T121">ɨraːktaːgɨ-tɨ-gar</ta>
            <ta e="T123" id="Seg_1251" s="T122">čaːskɨ-nɨ</ta>
            <ta e="T124" id="Seg_1252" s="T123">ɨl-aːt</ta>
            <ta e="T125" id="Seg_1253" s="T124">ötü͡ö</ta>
            <ta e="T126" id="Seg_1254" s="T125">haŋa-tɨ-n</ta>
            <ta e="T127" id="Seg_1255" s="T126">haŋar-an</ta>
            <ta e="T128" id="Seg_1256" s="T127">di͡e-bit</ta>
            <ta e="T129" id="Seg_1257" s="T128">ɨl-ɨ-ŋ</ta>
            <ta e="T130" id="Seg_1258" s="T129">iti-ni</ta>
            <ta e="T131" id="Seg_1259" s="T130">üllest-i-ŋ</ta>
            <ta e="T132" id="Seg_1260" s="T131">haŋar-a-bɨn</ta>
            <ta e="T133" id="Seg_1261" s="T132">ehi͡e-ke</ta>
            <ta e="T134" id="Seg_1262" s="T133">ih-i͡e-m</ta>
            <ta e="T135" id="Seg_1263" s="T134">hu͡og-a</ta>
            <ta e="T138" id="Seg_1264" s="T137">viʼnagraːd</ta>
            <ta e="T139" id="Seg_1265" s="T138">hugun</ta>
            <ta e="T140" id="Seg_1266" s="T139">aragiː-tɨ-n</ta>
            <ta e="T141" id="Seg_1267" s="T140">kel-e</ta>
            <ta e="T142" id="Seg_1268" s="T141">ilig-ine</ta>
            <ta e="T143" id="Seg_1269" s="T142">taŋara</ta>
            <ta e="T144" id="Seg_1270" s="T143">ɨraːktaːgɨ-ta</ta>
            <ta e="T145" id="Seg_1271" s="T144">kileːb-i</ta>
            <ta e="T146" id="Seg_1272" s="T145">ɨl-aːt</ta>
            <ta e="T147" id="Seg_1273" s="T146">ötü͡ö</ta>
            <ta e="T148" id="Seg_1274" s="T147">haŋa-nɨ</ta>
            <ta e="T149" id="Seg_1275" s="T148">haŋar-an</ta>
            <ta e="T150" id="Seg_1276" s="T149">baraːn</ta>
            <ta e="T151" id="Seg_1277" s="T150">o-nu</ta>
            <ta e="T152" id="Seg_1278" s="T151">tohut-an</ta>
            <ta e="T153" id="Seg_1279" s="T152">üllest-eː-bit</ta>
            <ta e="T154" id="Seg_1280" s="T153">giniler-ge</ta>
            <ta e="T155" id="Seg_1281" s="T154">haŋar-an</ta>
            <ta e="T156" id="Seg_1282" s="T155">iti</ta>
            <ta e="T157" id="Seg_1283" s="T156">bu͡ol-ar</ta>
            <ta e="T158" id="Seg_1284" s="T157">min</ta>
            <ta e="T159" id="Seg_1285" s="T158">et-i-m</ta>
            <ta e="T160" id="Seg_1286" s="T159">tu͡ok</ta>
            <ta e="T161" id="Seg_1287" s="T160">ehigi</ta>
            <ta e="T162" id="Seg_1288" s="T161">tus-kutu-gar</ta>
            <ta e="T163" id="Seg_1289" s="T162">ölüː-ge</ta>
            <ta e="T164" id="Seg_1290" s="T163">bi͡er-il-ler</ta>
            <ta e="T165" id="Seg_1291" s="T164">iti-ni</ta>
            <ta e="T166" id="Seg_1292" s="T165">hi͡e-ŋ</ta>
            <ta e="T167" id="Seg_1293" s="T166">minigi-n</ta>
            <ta e="T168" id="Seg_1294" s="T167">öjdüː-r</ta>
            <ta e="T169" id="Seg_1295" s="T168">aːk-ka</ta>
            <ta e="T170" id="Seg_1296" s="T169">itigirdik</ta>
            <ta e="T171" id="Seg_1297" s="T170">hin</ta>
            <ta e="T172" id="Seg_1298" s="T171">čaːskɨ</ta>
            <ta e="T173" id="Seg_1299" s="T172">tuh-u-nan</ta>
            <ta e="T174" id="Seg_1300" s="T173">haŋar-bɨt-a</ta>
            <ta e="T175" id="Seg_1301" s="T174">körs-üː</ta>
            <ta e="T176" id="Seg_1302" s="T175">aːs-pɨt-ɨ-n</ta>
            <ta e="T177" id="Seg_1303" s="T176">kenne</ta>
            <ta e="T178" id="Seg_1304" s="T177">bu</ta>
            <ta e="T179" id="Seg_1305" s="T178">čaːskɨ</ta>
            <ta e="T180" id="Seg_1306" s="T179">bu͡ol-ar</ta>
            <ta e="T181" id="Seg_1307" s="T180">haŋa</ta>
            <ta e="T182" id="Seg_1308" s="T181">kepset-iː</ta>
            <ta e="T183" id="Seg_1309" s="T182">min</ta>
            <ta e="T184" id="Seg_1310" s="T183">kaːm-ma-r</ta>
            <ta e="T185" id="Seg_1311" s="T184">tu͡ok</ta>
            <ta e="T186" id="Seg_1312" s="T185">tok-t-or</ta>
            <ta e="T187" id="Seg_1313" s="T186">ehigi</ta>
            <ta e="T188" id="Seg_1314" s="T187">tus-kutu-gar</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1315" s="T0">Paːsxa</ta>
            <ta e="T2" id="Seg_1316" s="T1">taŋara</ta>
            <ta e="T3" id="Seg_1317" s="T2">bu͡ol-BIT-tI-GAr</ta>
            <ta e="T4" id="Seg_1318" s="T3">Isus</ta>
            <ta e="T5" id="Seg_1319" s="T4">ɨjɨt-BIT</ta>
            <ta e="T6" id="Seg_1320" s="T5">Pʼötr-ttAn</ta>
            <ta e="T7" id="Seg_1321" s="T6">kɨtta</ta>
            <ta e="T8" id="Seg_1322" s="T7">Ujbaːn-ttAn</ta>
            <ta e="T9" id="Seg_1323" s="T8">haŋar-An</ta>
            <ta e="T11" id="Seg_1324" s="T10">bar-I-ŋ</ta>
            <ta e="T12" id="Seg_1325" s="T11">belemneː-ŋ</ta>
            <ta e="T13" id="Seg_1326" s="T12">Paːsxa-GA</ta>
            <ta e="T14" id="Seg_1327" s="T13">ahaː-Ar-BItI-n</ta>
            <ta e="T15" id="Seg_1328" s="T14">giniler</ta>
            <ta e="T16" id="Seg_1329" s="T15">bu͡ollagɨna</ta>
            <ta e="T17" id="Seg_1330" s="T16">ɨjɨt-BIT-LAr</ta>
            <ta e="T18" id="Seg_1331" s="T17">Isus-ttAn</ta>
            <ta e="T19" id="Seg_1332" s="T18">kanna</ta>
            <ta e="T20" id="Seg_1333" s="T19">belemneː-A-t-A-GIn</ta>
            <ta e="T21" id="Seg_1334" s="T20">di͡e-An</ta>
            <ta e="T22" id="Seg_1335" s="T21">Isus</ta>
            <ta e="T23" id="Seg_1336" s="T22">di͡e-BIT</ta>
            <ta e="T24" id="Seg_1337" s="T23">onno</ta>
            <ta e="T25" id="Seg_1338" s="T24">ehigi</ta>
            <ta e="T26" id="Seg_1339" s="T25">gu͡orat-GA</ta>
            <ta e="T27" id="Seg_1340" s="T26">kiːr-An-GIt</ta>
            <ta e="T28" id="Seg_1341" s="T27">kiːr-An</ta>
            <ta e="T31" id="Seg_1342" s="T30">hir-GItI-GAr</ta>
            <ta e="T32" id="Seg_1343" s="T31">körüs-IAK-GIt</ta>
            <ta e="T33" id="Seg_1344" s="T32">biːr</ta>
            <ta e="T34" id="Seg_1345" s="T33">kihi-nI</ta>
            <ta e="T35" id="Seg_1346" s="T34">kɨtta</ta>
            <ta e="T36" id="Seg_1347" s="T35">ilin-An</ta>
            <ta e="T37" id="Seg_1348" s="T36">is-Ar-nI</ta>
            <ta e="T38" id="Seg_1349" s="T37">uː-LAːK</ta>
            <ta e="T39" id="Seg_1350" s="T38">ihit-nI</ta>
            <ta e="T40" id="Seg_1351" s="T39">gini-nI</ta>
            <ta e="T41" id="Seg_1352" s="T40">batɨs-I-ŋ</ta>
            <ta e="T42" id="Seg_1353" s="T41">dʼi͡e-GA</ta>
            <ta e="T43" id="Seg_1354" s="T42">di͡eri</ta>
            <ta e="T44" id="Seg_1355" s="T43">kanna</ta>
            <ta e="T45" id="Seg_1356" s="T44">kiːr-IAK-tA</ta>
            <ta e="T46" id="Seg_1357" s="T45">gini</ta>
            <ta e="T47" id="Seg_1358" s="T46">ol</ta>
            <ta e="T48" id="Seg_1359" s="T47">dʼi͡e-GA</ta>
            <ta e="T49" id="Seg_1360" s="T48">küheːjin-tI-GAr</ta>
            <ta e="T50" id="Seg_1361" s="T49">haŋar-I-ŋ</ta>
            <ta e="T51" id="Seg_1362" s="T50">učuːtal</ta>
            <ta e="T52" id="Seg_1363" s="T51">ɨjɨt-Ar</ta>
            <ta e="T53" id="Seg_1364" s="T52">di͡e-ŋ</ta>
            <ta e="T54" id="Seg_1365" s="T53">en-ttAn</ta>
            <ta e="T55" id="Seg_1366" s="T54">kanna</ta>
            <ta e="T56" id="Seg_1367" s="T55">baːr=Ij</ta>
            <ta e="T57" id="Seg_1368" s="T56">dʼi͡e</ta>
            <ta e="T58" id="Seg_1369" s="T57">kos-tA</ta>
            <ta e="T59" id="Seg_1370" s="T58">min</ta>
            <ta e="T60" id="Seg_1371" s="T59">Paːsxa-GA</ta>
            <ta e="T61" id="Seg_1372" s="T60">ahaː-Ar-BA-r</ta>
            <ta e="T62" id="Seg_1373" s="T61">beje-m</ta>
            <ta e="T63" id="Seg_1374" s="T62">ü͡öret-Ar</ta>
            <ta e="T64" id="Seg_1375" s="T63">dʼon-BI-n</ta>
            <ta e="T65" id="Seg_1376" s="T64">kɨtta</ta>
            <ta e="T66" id="Seg_1377" s="T65">gini</ta>
            <ta e="T67" id="Seg_1378" s="T66">köllör-IAK-tA</ta>
            <ta e="T68" id="Seg_1379" s="T67">ehigi-GA</ta>
            <ta e="T69" id="Seg_1380" s="T68">ki͡eŋ</ta>
            <ta e="T70" id="Seg_1381" s="T69">bagajɨ</ta>
            <ta e="T71" id="Seg_1382" s="T70">dʼi͡e</ta>
            <ta e="T72" id="Seg_1383" s="T71">kos-tI-n</ta>
            <ta e="T73" id="Seg_1384" s="T72">mu͡osta-tA</ta>
            <ta e="T74" id="Seg_1385" s="T73">hap-Iː-LAːK-nI</ta>
            <ta e="T75" id="Seg_1386" s="T74">onno</ta>
            <ta e="T76" id="Seg_1387" s="T75">belemneː-ŋ</ta>
            <ta e="T77" id="Seg_1388" s="T76">ü͡öret-Ar</ta>
            <ta e="T78" id="Seg_1389" s="T77">dʼon-tA</ta>
            <ta e="T79" id="Seg_1390" s="T78">onno</ta>
            <ta e="T80" id="Seg_1391" s="T79">bar-An</ta>
            <ta e="T81" id="Seg_1392" s="T80">bul-BIT-LAr</ta>
            <ta e="T82" id="Seg_1393" s="T81">kajdak</ta>
            <ta e="T83" id="Seg_1394" s="T82">haŋar-BIT-tI-n</ta>
            <ta e="T84" id="Seg_1395" s="T83">kördük</ta>
            <ta e="T85" id="Seg_1396" s="T84">Isus</ta>
            <ta e="T86" id="Seg_1397" s="T85">onno</ta>
            <ta e="T87" id="Seg_1398" s="T86">belemneː-BIT-LAr</ta>
            <ta e="T88" id="Seg_1399" s="T87">Paːsxa-nI</ta>
            <ta e="T89" id="Seg_1400" s="T88">kaččaga</ta>
            <ta e="T90" id="Seg_1401" s="T89">bu͡ol-BIT-tI-GAr</ta>
            <ta e="T91" id="Seg_1402" s="T90">körüs-Ar</ta>
            <ta e="T92" id="Seg_1403" s="T91">kem-LArA</ta>
            <ta e="T93" id="Seg_1404" s="T92">Isus</ta>
            <ta e="T94" id="Seg_1405" s="T93">olor-BIT</ta>
            <ta e="T95" id="Seg_1406" s="T94">ostu͡ol</ta>
            <ta e="T96" id="Seg_1407" s="T95">ilin-tI-GAr</ta>
            <ta e="T97" id="Seg_1408" s="T96">u͡on</ta>
            <ta e="T98" id="Seg_1409" s="T97">ikki</ta>
            <ta e="T99" id="Seg_1410" s="T98">ü͡öret-Ar</ta>
            <ta e="T100" id="Seg_1411" s="T99">kihi-LAr-tI-n</ta>
            <ta e="T101" id="Seg_1412" s="T100">kɨtta</ta>
            <ta e="T102" id="Seg_1413" s="T101">onno</ta>
            <ta e="T103" id="Seg_1414" s="T102">haŋar-BIT</ta>
            <ta e="T104" id="Seg_1415" s="T103">min</ta>
            <ta e="T105" id="Seg_1416" s="T104">olus</ta>
            <ta e="T106" id="Seg_1417" s="T105">bagar-TI-m</ta>
            <ta e="T107" id="Seg_1418" s="T106">ahaː-AːrI</ta>
            <ta e="T108" id="Seg_1419" s="T107">ehigi-nI</ta>
            <ta e="T109" id="Seg_1420" s="T108">kɨtta</ta>
            <ta e="T110" id="Seg_1421" s="T109">min</ta>
            <ta e="T111" id="Seg_1422" s="T110">erej-LAN-Ar-I-m</ta>
            <ta e="T112" id="Seg_1423" s="T111">ilin-tI-GAr</ta>
            <ta e="T113" id="Seg_1424" s="T112">haŋar-A-BIn</ta>
            <ta e="T114" id="Seg_1425" s="T113">ehigi-GA</ta>
            <ta e="T115" id="Seg_1426" s="T114">ahaː-IAK-m</ta>
            <ta e="T116" id="Seg_1427" s="T115">hu͡ok-tA</ta>
            <ta e="T117" id="Seg_1428" s="T116">iti-nI</ta>
            <ta e="T118" id="Seg_1429" s="T117">tu͡ok</ta>
            <ta e="T119" id="Seg_1430" s="T118">bu͡ol-An</ta>
            <ta e="T120" id="Seg_1431" s="T119">ilik-InA</ta>
            <ta e="T121" id="Seg_1432" s="T120">taŋara</ta>
            <ta e="T122" id="Seg_1433" s="T121">ɨraːktaːgɨ-tI-GAr</ta>
            <ta e="T123" id="Seg_1434" s="T122">čaːskɨ-nI</ta>
            <ta e="T124" id="Seg_1435" s="T123">ɨl-AːT</ta>
            <ta e="T125" id="Seg_1436" s="T124">ötü͡ö</ta>
            <ta e="T126" id="Seg_1437" s="T125">haŋa-tI-n</ta>
            <ta e="T127" id="Seg_1438" s="T126">haŋar-An</ta>
            <ta e="T128" id="Seg_1439" s="T127">di͡e-BIT</ta>
            <ta e="T129" id="Seg_1440" s="T128">ɨl-I-ŋ</ta>
            <ta e="T130" id="Seg_1441" s="T129">iti-nI</ta>
            <ta e="T131" id="Seg_1442" s="T130">üllehin-I-ŋ</ta>
            <ta e="T132" id="Seg_1443" s="T131">haŋar-A-BIn</ta>
            <ta e="T133" id="Seg_1444" s="T132">ehigi-GA</ta>
            <ta e="T134" id="Seg_1445" s="T133">is-IAK-m</ta>
            <ta e="T135" id="Seg_1446" s="T134">hu͡ok-tA</ta>
            <ta e="T138" id="Seg_1447" s="T137">viʼnagraːd</ta>
            <ta e="T139" id="Seg_1448" s="T138">hugun</ta>
            <ta e="T140" id="Seg_1449" s="T139">aragiː-tI-n</ta>
            <ta e="T141" id="Seg_1450" s="T140">kel-A</ta>
            <ta e="T142" id="Seg_1451" s="T141">ilik-InA</ta>
            <ta e="T143" id="Seg_1452" s="T142">taŋara</ta>
            <ta e="T144" id="Seg_1453" s="T143">ɨraːktaːgɨ-tA</ta>
            <ta e="T145" id="Seg_1454" s="T144">keli͡ep-nI</ta>
            <ta e="T146" id="Seg_1455" s="T145">ɨl-AːT</ta>
            <ta e="T147" id="Seg_1456" s="T146">ötü͡ö</ta>
            <ta e="T148" id="Seg_1457" s="T147">haŋa-nI</ta>
            <ta e="T149" id="Seg_1458" s="T148">haŋar-An</ta>
            <ta e="T150" id="Seg_1459" s="T149">baran</ta>
            <ta e="T151" id="Seg_1460" s="T150">ol-nI</ta>
            <ta e="T152" id="Seg_1461" s="T151">tohut-An</ta>
            <ta e="T153" id="Seg_1462" s="T152">üllehin-A-BIT</ta>
            <ta e="T154" id="Seg_1463" s="T153">giniler-GA</ta>
            <ta e="T155" id="Seg_1464" s="T154">haŋar-An</ta>
            <ta e="T156" id="Seg_1465" s="T155">iti</ta>
            <ta e="T157" id="Seg_1466" s="T156">bu͡ol-Ar</ta>
            <ta e="T158" id="Seg_1467" s="T157">min</ta>
            <ta e="T159" id="Seg_1468" s="T158">et-I-m</ta>
            <ta e="T160" id="Seg_1469" s="T159">tu͡ok</ta>
            <ta e="T161" id="Seg_1470" s="T160">ehigi</ta>
            <ta e="T162" id="Seg_1471" s="T161">tus-GItI-GAr</ta>
            <ta e="T163" id="Seg_1472" s="T162">ölüː-GA</ta>
            <ta e="T164" id="Seg_1473" s="T163">bi͡er-Ar-LAr</ta>
            <ta e="T165" id="Seg_1474" s="T164">iti-nI</ta>
            <ta e="T166" id="Seg_1475" s="T165">hi͡e-ŋ</ta>
            <ta e="T167" id="Seg_1476" s="T166">min-n</ta>
            <ta e="T168" id="Seg_1477" s="T167">öjdöː-Ar</ta>
            <ta e="T169" id="Seg_1478" s="T168">aːt-GA</ta>
            <ta e="T170" id="Seg_1479" s="T169">itigirdik</ta>
            <ta e="T171" id="Seg_1480" s="T170">hin</ta>
            <ta e="T172" id="Seg_1481" s="T171">čaːskɨ</ta>
            <ta e="T173" id="Seg_1482" s="T172">tus-tI-nAn</ta>
            <ta e="T174" id="Seg_1483" s="T173">haŋar-BIT-tA</ta>
            <ta e="T175" id="Seg_1484" s="T174">körüs-Iː</ta>
            <ta e="T176" id="Seg_1485" s="T175">aːs-BIT-tI-n</ta>
            <ta e="T177" id="Seg_1486" s="T176">genne</ta>
            <ta e="T178" id="Seg_1487" s="T177">bu</ta>
            <ta e="T179" id="Seg_1488" s="T178">čaːskɨ</ta>
            <ta e="T180" id="Seg_1489" s="T179">bu͡ol-Ar</ta>
            <ta e="T181" id="Seg_1490" s="T180">haŋa</ta>
            <ta e="T182" id="Seg_1491" s="T181">kepset-Iː</ta>
            <ta e="T183" id="Seg_1492" s="T182">min</ta>
            <ta e="T184" id="Seg_1493" s="T183">kaːn-BA-r</ta>
            <ta e="T185" id="Seg_1494" s="T184">tu͡ok</ta>
            <ta e="T186" id="Seg_1495" s="T185">tok-t-Ar</ta>
            <ta e="T187" id="Seg_1496" s="T186">ehigi</ta>
            <ta e="T188" id="Seg_1497" s="T187">tus-GItI-GAr</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1498" s="T0">Passover.[NOM]</ta>
            <ta e="T2" id="Seg_1499" s="T1">holiday.[NOM]</ta>
            <ta e="T3" id="Seg_1500" s="T2">become-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T4" id="Seg_1501" s="T3">Jesus.[NOM]</ta>
            <ta e="T5" id="Seg_1502" s="T4">ask-PST2.[3SG]</ta>
            <ta e="T6" id="Seg_1503" s="T5">Peter-ABL</ta>
            <ta e="T7" id="Seg_1504" s="T6">with</ta>
            <ta e="T8" id="Seg_1505" s="T7">John-ABL</ta>
            <ta e="T9" id="Seg_1506" s="T8">speak-CVB.SEQ</ta>
            <ta e="T11" id="Seg_1507" s="T10">go-EP-IMP.2PL</ta>
            <ta e="T12" id="Seg_1508" s="T11">prepare-IMP.2PL</ta>
            <ta e="T13" id="Seg_1509" s="T12">Passover-DAT/LOC</ta>
            <ta e="T14" id="Seg_1510" s="T13">eat-PTCP.PRS-1PL-ACC</ta>
            <ta e="T15" id="Seg_1511" s="T14">3PL.[NOM]</ta>
            <ta e="T16" id="Seg_1512" s="T15">though</ta>
            <ta e="T17" id="Seg_1513" s="T16">ask-PST2-3PL</ta>
            <ta e="T18" id="Seg_1514" s="T17">Jesus-ABL</ta>
            <ta e="T19" id="Seg_1515" s="T18">where</ta>
            <ta e="T20" id="Seg_1516" s="T19">prepare-EP-CAUS-PRS-2SG</ta>
            <ta e="T21" id="Seg_1517" s="T20">say-CVB.SEQ</ta>
            <ta e="T22" id="Seg_1518" s="T21">Jesus.[NOM]</ta>
            <ta e="T23" id="Seg_1519" s="T22">say-PST2.[3SG]</ta>
            <ta e="T24" id="Seg_1520" s="T23">there</ta>
            <ta e="T25" id="Seg_1521" s="T24">2PL.[NOM]</ta>
            <ta e="T26" id="Seg_1522" s="T25">city-DAT/LOC</ta>
            <ta e="T27" id="Seg_1523" s="T26">go.in-CVB.SEQ-2PL</ta>
            <ta e="T28" id="Seg_1524" s="T27">go.in-CVB.SEQ</ta>
            <ta e="T31" id="Seg_1525" s="T30">place-2PL-DAT/LOC</ta>
            <ta e="T32" id="Seg_1526" s="T31">meet-FUT-2PL</ta>
            <ta e="T33" id="Seg_1527" s="T32">one</ta>
            <ta e="T34" id="Seg_1528" s="T33">human.being-ACC</ta>
            <ta e="T35" id="Seg_1529" s="T34">with</ta>
            <ta e="T36" id="Seg_1530" s="T35">take.away-CVB.SEQ</ta>
            <ta e="T37" id="Seg_1531" s="T36">go-PTCP.PRS-ACC</ta>
            <ta e="T38" id="Seg_1532" s="T37">water-PROPR</ta>
            <ta e="T39" id="Seg_1533" s="T38">jar-ACC</ta>
            <ta e="T40" id="Seg_1534" s="T39">3SG-ACC</ta>
            <ta e="T41" id="Seg_1535" s="T40">follow-EP-IMP.2PL</ta>
            <ta e="T42" id="Seg_1536" s="T41">house-DAT/LOC</ta>
            <ta e="T43" id="Seg_1537" s="T42">until</ta>
            <ta e="T44" id="Seg_1538" s="T43">whereto</ta>
            <ta e="T45" id="Seg_1539" s="T44">go.in-FUT-3SG</ta>
            <ta e="T46" id="Seg_1540" s="T45">3SG.[NOM]</ta>
            <ta e="T47" id="Seg_1541" s="T46">that</ta>
            <ta e="T48" id="Seg_1542" s="T47">house-DAT/LOC</ta>
            <ta e="T49" id="Seg_1543" s="T48">man.of.the.house-3SG-DAT/LOC</ta>
            <ta e="T50" id="Seg_1544" s="T49">say-EP-IMP.2PL</ta>
            <ta e="T51" id="Seg_1545" s="T50">teacher.[NOM]</ta>
            <ta e="T52" id="Seg_1546" s="T51">ask-PRS.[3SG]</ta>
            <ta e="T53" id="Seg_1547" s="T52">say-IMP.2PL</ta>
            <ta e="T54" id="Seg_1548" s="T53">2SG-ABL</ta>
            <ta e="T55" id="Seg_1549" s="T54">where</ta>
            <ta e="T56" id="Seg_1550" s="T55">there.is=Q</ta>
            <ta e="T57" id="Seg_1551" s="T56">house.[NOM]</ta>
            <ta e="T58" id="Seg_1552" s="T57">room-3SG.[NOM]</ta>
            <ta e="T59" id="Seg_1553" s="T58">1SG.[NOM]</ta>
            <ta e="T60" id="Seg_1554" s="T59">Passover-DAT/LOC</ta>
            <ta e="T61" id="Seg_1555" s="T60">eat-PTCP.PRS-1SG-DAT/LOC</ta>
            <ta e="T62" id="Seg_1556" s="T61">self-1SG.[NOM]</ta>
            <ta e="T63" id="Seg_1557" s="T62">learn-PTCP.PRS</ta>
            <ta e="T64" id="Seg_1558" s="T63">people-1SG-ACC</ta>
            <ta e="T65" id="Seg_1559" s="T64">with</ta>
            <ta e="T66" id="Seg_1560" s="T65">3SG.[NOM]</ta>
            <ta e="T67" id="Seg_1561" s="T66">show-FUT-3SG</ta>
            <ta e="T68" id="Seg_1562" s="T67">2PL-DAT/LOC</ta>
            <ta e="T69" id="Seg_1563" s="T68">broad</ta>
            <ta e="T70" id="Seg_1564" s="T69">very</ta>
            <ta e="T71" id="Seg_1565" s="T70">house.[NOM]</ta>
            <ta e="T72" id="Seg_1566" s="T71">room-3SG-ACC</ta>
            <ta e="T73" id="Seg_1567" s="T72">floor-3SG.[NOM]</ta>
            <ta e="T74" id="Seg_1568" s="T73">cover-NMNZ-PROPR-ACC</ta>
            <ta e="T75" id="Seg_1569" s="T74">there</ta>
            <ta e="T76" id="Seg_1570" s="T75">prepare-IMP.2PL</ta>
            <ta e="T77" id="Seg_1571" s="T76">learn-PTCP.PRS</ta>
            <ta e="T78" id="Seg_1572" s="T77">people-3SG.[NOM]</ta>
            <ta e="T79" id="Seg_1573" s="T78">thither</ta>
            <ta e="T80" id="Seg_1574" s="T79">go-CVB.SEQ</ta>
            <ta e="T81" id="Seg_1575" s="T80">find-PST2-3PL</ta>
            <ta e="T82" id="Seg_1576" s="T81">how</ta>
            <ta e="T83" id="Seg_1577" s="T82">speak-PTCP.PST-3SG-ACC</ta>
            <ta e="T84" id="Seg_1578" s="T83">similar</ta>
            <ta e="T85" id="Seg_1579" s="T84">Jesus.[NOM]</ta>
            <ta e="T86" id="Seg_1580" s="T85">there</ta>
            <ta e="T87" id="Seg_1581" s="T86">prepare-PST2-3PL</ta>
            <ta e="T88" id="Seg_1582" s="T87">Passover-ACC</ta>
            <ta e="T89" id="Seg_1583" s="T88">when</ta>
            <ta e="T90" id="Seg_1584" s="T89">be-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T91" id="Seg_1585" s="T90">meet-PTCP.PRS</ta>
            <ta e="T92" id="Seg_1586" s="T91">time-3PL.[NOM]</ta>
            <ta e="T93" id="Seg_1587" s="T92">Jesus.[NOM]</ta>
            <ta e="T94" id="Seg_1588" s="T93">sit.down-PST2.[3SG]</ta>
            <ta e="T95" id="Seg_1589" s="T94">table.[NOM]</ta>
            <ta e="T96" id="Seg_1590" s="T95">front-3SG-DAT/LOC</ta>
            <ta e="T97" id="Seg_1591" s="T96">ten</ta>
            <ta e="T98" id="Seg_1592" s="T97">two</ta>
            <ta e="T99" id="Seg_1593" s="T98">learn-PTCP.PRS</ta>
            <ta e="T100" id="Seg_1594" s="T99">human.being-PL-3SG-ACC</ta>
            <ta e="T101" id="Seg_1595" s="T100">with</ta>
            <ta e="T102" id="Seg_1596" s="T101">then</ta>
            <ta e="T103" id="Seg_1597" s="T102">speak-PST2.[3SG]</ta>
            <ta e="T104" id="Seg_1598" s="T103">1SG.[NOM]</ta>
            <ta e="T105" id="Seg_1599" s="T104">very</ta>
            <ta e="T106" id="Seg_1600" s="T105">want-PST1-1SG</ta>
            <ta e="T107" id="Seg_1601" s="T106">eat-CVB.PURP</ta>
            <ta e="T108" id="Seg_1602" s="T107">2PL-ACC</ta>
            <ta e="T109" id="Seg_1603" s="T108">with</ta>
            <ta e="T110" id="Seg_1604" s="T109">1SG.[NOM]</ta>
            <ta e="T111" id="Seg_1605" s="T110">pain-VBZ-PTCP.PRS-EP-1SG.[NOM]</ta>
            <ta e="T112" id="Seg_1606" s="T111">front-3SG-DAT/LOC</ta>
            <ta e="T113" id="Seg_1607" s="T112">speak-PRS-1SG</ta>
            <ta e="T114" id="Seg_1608" s="T113">2PL-DAT/LOC</ta>
            <ta e="T115" id="Seg_1609" s="T114">eat-FUT-1SG</ta>
            <ta e="T116" id="Seg_1610" s="T115">NEG-3SG</ta>
            <ta e="T117" id="Seg_1611" s="T116">that-ACC</ta>
            <ta e="T118" id="Seg_1612" s="T117">what.[NOM]</ta>
            <ta e="T119" id="Seg_1613" s="T118">be-CVB.SEQ</ta>
            <ta e="T120" id="Seg_1614" s="T119">not.yet-3SG</ta>
            <ta e="T121" id="Seg_1615" s="T120">sky.[NOM]</ta>
            <ta e="T122" id="Seg_1616" s="T121">czar-3SG-DAT/LOC</ta>
            <ta e="T123" id="Seg_1617" s="T122">cup-ACC</ta>
            <ta e="T124" id="Seg_1618" s="T123">take-CVB.ANT</ta>
            <ta e="T125" id="Seg_1619" s="T124">good</ta>
            <ta e="T126" id="Seg_1620" s="T125">word-3SG-ACC</ta>
            <ta e="T127" id="Seg_1621" s="T126">speak-CVB.SEQ</ta>
            <ta e="T128" id="Seg_1622" s="T127">say-PST2.[3SG]</ta>
            <ta e="T129" id="Seg_1623" s="T128">take-EP-IMP.2PL</ta>
            <ta e="T130" id="Seg_1624" s="T129">that-ACC</ta>
            <ta e="T131" id="Seg_1625" s="T130">divide-EP-IMP.2PL</ta>
            <ta e="T132" id="Seg_1626" s="T131">speak-PRS-1SG</ta>
            <ta e="T133" id="Seg_1627" s="T132">2PL-DAT/LOC</ta>
            <ta e="T134" id="Seg_1628" s="T133">drink-FUT-1SG</ta>
            <ta e="T135" id="Seg_1629" s="T134">NEG-3SG</ta>
            <ta e="T138" id="Seg_1630" s="T137">grape.[NOM]</ta>
            <ta e="T139" id="Seg_1631" s="T138">berry.[NOM]</ta>
            <ta e="T140" id="Seg_1632" s="T139">wine-3SG-ACC</ta>
            <ta e="T141" id="Seg_1633" s="T140">come-CVB.SIM</ta>
            <ta e="T142" id="Seg_1634" s="T141">not.yet-3SG</ta>
            <ta e="T143" id="Seg_1635" s="T142">sky.[NOM]</ta>
            <ta e="T144" id="Seg_1636" s="T143">czar-3SG.[NOM]</ta>
            <ta e="T145" id="Seg_1637" s="T144">bread-ACC</ta>
            <ta e="T146" id="Seg_1638" s="T145">take-CVB.ANT</ta>
            <ta e="T147" id="Seg_1639" s="T146">good</ta>
            <ta e="T148" id="Seg_1640" s="T147">word-ACC</ta>
            <ta e="T149" id="Seg_1641" s="T148">speak-CVB.SEQ</ta>
            <ta e="T150" id="Seg_1642" s="T149">after</ta>
            <ta e="T151" id="Seg_1643" s="T150">that-ACC</ta>
            <ta e="T152" id="Seg_1644" s="T151">break-CVB.SEQ</ta>
            <ta e="T153" id="Seg_1645" s="T152">divide-EP-PST2.[3SG]</ta>
            <ta e="T154" id="Seg_1646" s="T153">3PL-DAT/LOC</ta>
            <ta e="T155" id="Seg_1647" s="T154">speak-CVB.SEQ</ta>
            <ta e="T156" id="Seg_1648" s="T155">that.[NOM]</ta>
            <ta e="T157" id="Seg_1649" s="T156">be-PRS.[3SG]</ta>
            <ta e="T158" id="Seg_1650" s="T157">1SG.[NOM]</ta>
            <ta e="T159" id="Seg_1651" s="T158">body-EP-1SG.[NOM]</ta>
            <ta e="T160" id="Seg_1652" s="T159">what.[NOM]</ta>
            <ta e="T161" id="Seg_1653" s="T160">2PL.[NOM]</ta>
            <ta e="T162" id="Seg_1654" s="T161">side-2PL-DAT/LOC</ta>
            <ta e="T163" id="Seg_1655" s="T162">death-DAT/LOC</ta>
            <ta e="T164" id="Seg_1656" s="T163">give-PRS-3PL</ta>
            <ta e="T165" id="Seg_1657" s="T164">that-ACC</ta>
            <ta e="T166" id="Seg_1658" s="T165">eat-IMP.2PL</ta>
            <ta e="T167" id="Seg_1659" s="T166">1SG-ACC</ta>
            <ta e="T168" id="Seg_1660" s="T167">remember-PTCP.PRS</ta>
            <ta e="T169" id="Seg_1661" s="T168">name-DAT/LOC</ta>
            <ta e="T170" id="Seg_1662" s="T169">like.that</ta>
            <ta e="T171" id="Seg_1663" s="T170">however</ta>
            <ta e="T172" id="Seg_1664" s="T171">cup.[NOM]</ta>
            <ta e="T173" id="Seg_1665" s="T172">side-3SG-INSTR</ta>
            <ta e="T174" id="Seg_1666" s="T173">speak-PST2-3SG</ta>
            <ta e="T175" id="Seg_1667" s="T174">meet-NMNZ.[NOM]</ta>
            <ta e="T176" id="Seg_1668" s="T175">pass.by-PTCP.PST-3SG-ACC</ta>
            <ta e="T177" id="Seg_1669" s="T176">after</ta>
            <ta e="T178" id="Seg_1670" s="T177">this</ta>
            <ta e="T179" id="Seg_1671" s="T178">cup.[NOM]</ta>
            <ta e="T180" id="Seg_1672" s="T179">be-PRS.[3SG]</ta>
            <ta e="T181" id="Seg_1673" s="T180">new</ta>
            <ta e="T182" id="Seg_1674" s="T181">chat-NMNZ.[NOM]</ta>
            <ta e="T183" id="Seg_1675" s="T182">1SG.[NOM]</ta>
            <ta e="T184" id="Seg_1676" s="T183">blood-1SG-DAT/LOC</ta>
            <ta e="T185" id="Seg_1677" s="T184">what.[NOM]</ta>
            <ta e="T186" id="Seg_1678" s="T185">pour.out-CAUS-PRS.[3SG]</ta>
            <ta e="T187" id="Seg_1679" s="T186">2PL.[NOM]</ta>
            <ta e="T188" id="Seg_1680" s="T187">side-2PL-DAT/LOC</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_1681" s="T0">Pessach.[NOM]</ta>
            <ta e="T2" id="Seg_1682" s="T1">Feiertag.[NOM]</ta>
            <ta e="T3" id="Seg_1683" s="T2">werden-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T4" id="Seg_1684" s="T3">Jesus.[NOM]</ta>
            <ta e="T5" id="Seg_1685" s="T4">fragen-PST2.[3SG]</ta>
            <ta e="T6" id="Seg_1686" s="T5">Petrus-ABL</ta>
            <ta e="T7" id="Seg_1687" s="T6">mit</ta>
            <ta e="T8" id="Seg_1688" s="T7">Johannes-ABL</ta>
            <ta e="T9" id="Seg_1689" s="T8">sprechen-CVB.SEQ</ta>
            <ta e="T11" id="Seg_1690" s="T10">gehen-EP-IMP.2PL</ta>
            <ta e="T12" id="Seg_1691" s="T11">vorbereiten-IMP.2PL</ta>
            <ta e="T13" id="Seg_1692" s="T12">Pessach-DAT/LOC</ta>
            <ta e="T14" id="Seg_1693" s="T13">essen-PTCP.PRS-1PL-ACC</ta>
            <ta e="T15" id="Seg_1694" s="T14">3PL.[NOM]</ta>
            <ta e="T16" id="Seg_1695" s="T15">aber</ta>
            <ta e="T17" id="Seg_1696" s="T16">fragen-PST2-3PL</ta>
            <ta e="T18" id="Seg_1697" s="T17">Jesus-ABL</ta>
            <ta e="T19" id="Seg_1698" s="T18">wo</ta>
            <ta e="T20" id="Seg_1699" s="T19">vorbereiten-EP-CAUS-PRS-2SG</ta>
            <ta e="T21" id="Seg_1700" s="T20">sagen-CVB.SEQ</ta>
            <ta e="T22" id="Seg_1701" s="T21">Jesus.[NOM]</ta>
            <ta e="T23" id="Seg_1702" s="T22">sagen-PST2.[3SG]</ta>
            <ta e="T24" id="Seg_1703" s="T23">dort</ta>
            <ta e="T25" id="Seg_1704" s="T24">2PL.[NOM]</ta>
            <ta e="T26" id="Seg_1705" s="T25">Stadt-DAT/LOC</ta>
            <ta e="T27" id="Seg_1706" s="T26">hineingehen-CVB.SEQ-2PL</ta>
            <ta e="T28" id="Seg_1707" s="T27">hineingehen-CVB.SEQ</ta>
            <ta e="T31" id="Seg_1708" s="T30">Ort-2PL-DAT/LOC</ta>
            <ta e="T32" id="Seg_1709" s="T31">treffen-FUT-2PL</ta>
            <ta e="T33" id="Seg_1710" s="T32">eins</ta>
            <ta e="T34" id="Seg_1711" s="T33">Mensch-ACC</ta>
            <ta e="T35" id="Seg_1712" s="T34">mit</ta>
            <ta e="T36" id="Seg_1713" s="T35">mitnehmen-CVB.SEQ</ta>
            <ta e="T37" id="Seg_1714" s="T36">gehen-PTCP.PRS-ACC</ta>
            <ta e="T38" id="Seg_1715" s="T37">Wasser-PROPR</ta>
            <ta e="T39" id="Seg_1716" s="T38">Gefäß-ACC</ta>
            <ta e="T40" id="Seg_1717" s="T39">3SG-ACC</ta>
            <ta e="T41" id="Seg_1718" s="T40">folgen-EP-IMP.2PL</ta>
            <ta e="T42" id="Seg_1719" s="T41">Haus-DAT/LOC</ta>
            <ta e="T43" id="Seg_1720" s="T42">bis.zu</ta>
            <ta e="T44" id="Seg_1721" s="T43">wohin</ta>
            <ta e="T45" id="Seg_1722" s="T44">hineingehen-FUT-3SG</ta>
            <ta e="T46" id="Seg_1723" s="T45">3SG.[NOM]</ta>
            <ta e="T47" id="Seg_1724" s="T46">jenes</ta>
            <ta e="T48" id="Seg_1725" s="T47">Haus-DAT/LOC</ta>
            <ta e="T49" id="Seg_1726" s="T48">Hausherr-3SG-DAT/LOC</ta>
            <ta e="T50" id="Seg_1727" s="T49">sagen-EP-IMP.2PL</ta>
            <ta e="T51" id="Seg_1728" s="T50">Lehrer.[NOM]</ta>
            <ta e="T52" id="Seg_1729" s="T51">fragen-PRS.[3SG]</ta>
            <ta e="T53" id="Seg_1730" s="T52">sagen-IMP.2PL</ta>
            <ta e="T54" id="Seg_1731" s="T53">2SG-ABL</ta>
            <ta e="T55" id="Seg_1732" s="T54">wo</ta>
            <ta e="T56" id="Seg_1733" s="T55">es.gibt=Q</ta>
            <ta e="T57" id="Seg_1734" s="T56">Haus.[NOM]</ta>
            <ta e="T58" id="Seg_1735" s="T57">Zimmer-3SG.[NOM]</ta>
            <ta e="T59" id="Seg_1736" s="T58">1SG.[NOM]</ta>
            <ta e="T60" id="Seg_1737" s="T59">Pessach-DAT/LOC</ta>
            <ta e="T61" id="Seg_1738" s="T60">essen-PTCP.PRS-1SG-DAT/LOC</ta>
            <ta e="T62" id="Seg_1739" s="T61">selbst-1SG.[NOM]</ta>
            <ta e="T63" id="Seg_1740" s="T62">lernen-PTCP.PRS</ta>
            <ta e="T64" id="Seg_1741" s="T63">Leute-1SG-ACC</ta>
            <ta e="T65" id="Seg_1742" s="T64">mit</ta>
            <ta e="T66" id="Seg_1743" s="T65">3SG.[NOM]</ta>
            <ta e="T67" id="Seg_1744" s="T66">zeigen-FUT-3SG</ta>
            <ta e="T68" id="Seg_1745" s="T67">2PL-DAT/LOC</ta>
            <ta e="T69" id="Seg_1746" s="T68">breit</ta>
            <ta e="T70" id="Seg_1747" s="T69">sehr</ta>
            <ta e="T71" id="Seg_1748" s="T70">Haus.[NOM]</ta>
            <ta e="T72" id="Seg_1749" s="T71">Zimmer-3SG-ACC</ta>
            <ta e="T73" id="Seg_1750" s="T72">Boden-3SG.[NOM]</ta>
            <ta e="T74" id="Seg_1751" s="T73">bedecken-NMNZ-PROPR-ACC</ta>
            <ta e="T75" id="Seg_1752" s="T74">dort</ta>
            <ta e="T76" id="Seg_1753" s="T75">vorbereiten-IMP.2PL</ta>
            <ta e="T77" id="Seg_1754" s="T76">lernen-PTCP.PRS</ta>
            <ta e="T78" id="Seg_1755" s="T77">Volk-3SG.[NOM]</ta>
            <ta e="T79" id="Seg_1756" s="T78">dorthin</ta>
            <ta e="T80" id="Seg_1757" s="T79">gehen-CVB.SEQ</ta>
            <ta e="T81" id="Seg_1758" s="T80">finden-PST2-3PL</ta>
            <ta e="T82" id="Seg_1759" s="T81">wie</ta>
            <ta e="T83" id="Seg_1760" s="T82">sprechen-PTCP.PST-3SG-ACC</ta>
            <ta e="T84" id="Seg_1761" s="T83">ähnlich</ta>
            <ta e="T85" id="Seg_1762" s="T84">Jesus.[NOM]</ta>
            <ta e="T86" id="Seg_1763" s="T85">dort</ta>
            <ta e="T87" id="Seg_1764" s="T86">vorbereiten-PST2-3PL</ta>
            <ta e="T88" id="Seg_1765" s="T87">Pessach-ACC</ta>
            <ta e="T89" id="Seg_1766" s="T88">als</ta>
            <ta e="T90" id="Seg_1767" s="T89">sein-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T91" id="Seg_1768" s="T90">treffen-PTCP.PRS</ta>
            <ta e="T92" id="Seg_1769" s="T91">Zeit-3PL.[NOM]</ta>
            <ta e="T93" id="Seg_1770" s="T92">Jesus.[NOM]</ta>
            <ta e="T94" id="Seg_1771" s="T93">sich.setzen-PST2.[3SG]</ta>
            <ta e="T95" id="Seg_1772" s="T94">Tisch.[NOM]</ta>
            <ta e="T96" id="Seg_1773" s="T95">Vorderteil-3SG-DAT/LOC</ta>
            <ta e="T97" id="Seg_1774" s="T96">zehn</ta>
            <ta e="T98" id="Seg_1775" s="T97">zwei</ta>
            <ta e="T99" id="Seg_1776" s="T98">lernen-PTCP.PRS</ta>
            <ta e="T100" id="Seg_1777" s="T99">Mensch-PL-3SG-ACC</ta>
            <ta e="T101" id="Seg_1778" s="T100">mit</ta>
            <ta e="T102" id="Seg_1779" s="T101">dann</ta>
            <ta e="T103" id="Seg_1780" s="T102">sprechen-PST2.[3SG]</ta>
            <ta e="T104" id="Seg_1781" s="T103">1SG.[NOM]</ta>
            <ta e="T105" id="Seg_1782" s="T104">sehr</ta>
            <ta e="T106" id="Seg_1783" s="T105">wollen-PST1-1SG</ta>
            <ta e="T107" id="Seg_1784" s="T106">essen-CVB.PURP</ta>
            <ta e="T108" id="Seg_1785" s="T107">2PL-ACC</ta>
            <ta e="T109" id="Seg_1786" s="T108">mit</ta>
            <ta e="T110" id="Seg_1787" s="T109">1SG.[NOM]</ta>
            <ta e="T111" id="Seg_1788" s="T110">Qual-VBZ-PTCP.PRS-EP-1SG.[NOM]</ta>
            <ta e="T112" id="Seg_1789" s="T111">Vorderteil-3SG-DAT/LOC</ta>
            <ta e="T113" id="Seg_1790" s="T112">sprechen-PRS-1SG</ta>
            <ta e="T114" id="Seg_1791" s="T113">2PL-DAT/LOC</ta>
            <ta e="T115" id="Seg_1792" s="T114">essen-FUT-1SG</ta>
            <ta e="T116" id="Seg_1793" s="T115">NEG-3SG</ta>
            <ta e="T117" id="Seg_1794" s="T116">dieses-ACC</ta>
            <ta e="T118" id="Seg_1795" s="T117">was.[NOM]</ta>
            <ta e="T119" id="Seg_1796" s="T118">sein-CVB.SEQ</ta>
            <ta e="T120" id="Seg_1797" s="T119">noch.nicht-3SG</ta>
            <ta e="T121" id="Seg_1798" s="T120">Himmel.[NOM]</ta>
            <ta e="T122" id="Seg_1799" s="T121">Zar-3SG-DAT/LOC</ta>
            <ta e="T123" id="Seg_1800" s="T122">Tasse-ACC</ta>
            <ta e="T124" id="Seg_1801" s="T123">nehmen-CVB.ANT</ta>
            <ta e="T125" id="Seg_1802" s="T124">gut</ta>
            <ta e="T126" id="Seg_1803" s="T125">Wort-3SG-ACC</ta>
            <ta e="T127" id="Seg_1804" s="T126">sprechen-CVB.SEQ</ta>
            <ta e="T128" id="Seg_1805" s="T127">sagen-PST2.[3SG]</ta>
            <ta e="T129" id="Seg_1806" s="T128">nehmen-EP-IMP.2PL</ta>
            <ta e="T130" id="Seg_1807" s="T129">dieses-ACC</ta>
            <ta e="T131" id="Seg_1808" s="T130">aufteilen-EP-IMP.2PL</ta>
            <ta e="T132" id="Seg_1809" s="T131">sprechen-PRS-1SG</ta>
            <ta e="T133" id="Seg_1810" s="T132">2PL-DAT/LOC</ta>
            <ta e="T134" id="Seg_1811" s="T133">trinken-FUT-1SG</ta>
            <ta e="T135" id="Seg_1812" s="T134">NEG-3SG</ta>
            <ta e="T138" id="Seg_1813" s="T137">Weintraube.[NOM]</ta>
            <ta e="T139" id="Seg_1814" s="T138">Beere.[NOM]</ta>
            <ta e="T140" id="Seg_1815" s="T139">Wein-3SG-ACC</ta>
            <ta e="T141" id="Seg_1816" s="T140">kommen-CVB.SIM</ta>
            <ta e="T142" id="Seg_1817" s="T141">noch.nicht-3SG</ta>
            <ta e="T143" id="Seg_1818" s="T142">Himmel.[NOM]</ta>
            <ta e="T144" id="Seg_1819" s="T143">Zar-3SG.[NOM]</ta>
            <ta e="T145" id="Seg_1820" s="T144">Brot-ACC</ta>
            <ta e="T146" id="Seg_1821" s="T145">nehmen-CVB.ANT</ta>
            <ta e="T147" id="Seg_1822" s="T146">gut</ta>
            <ta e="T148" id="Seg_1823" s="T147">Wort-ACC</ta>
            <ta e="T149" id="Seg_1824" s="T148">sprechen-CVB.SEQ</ta>
            <ta e="T150" id="Seg_1825" s="T149">nachdem</ta>
            <ta e="T151" id="Seg_1826" s="T150">jenes-ACC</ta>
            <ta e="T152" id="Seg_1827" s="T151">brechen-CVB.SEQ</ta>
            <ta e="T153" id="Seg_1828" s="T152">aufteilen-EP-PST2.[3SG]</ta>
            <ta e="T154" id="Seg_1829" s="T153">3PL-DAT/LOC</ta>
            <ta e="T155" id="Seg_1830" s="T154">sprechen-CVB.SEQ</ta>
            <ta e="T156" id="Seg_1831" s="T155">dieses.[NOM]</ta>
            <ta e="T157" id="Seg_1832" s="T156">sein-PRS.[3SG]</ta>
            <ta e="T158" id="Seg_1833" s="T157">1SG.[NOM]</ta>
            <ta e="T159" id="Seg_1834" s="T158">Körper-EP-1SG.[NOM]</ta>
            <ta e="T160" id="Seg_1835" s="T159">was.[NOM]</ta>
            <ta e="T161" id="Seg_1836" s="T160">2PL.[NOM]</ta>
            <ta e="T162" id="Seg_1837" s="T161">Seite-2PL-DAT/LOC</ta>
            <ta e="T163" id="Seg_1838" s="T162">Tod-DAT/LOC</ta>
            <ta e="T164" id="Seg_1839" s="T163">geben-PRS-3PL</ta>
            <ta e="T165" id="Seg_1840" s="T164">dieses-ACC</ta>
            <ta e="T166" id="Seg_1841" s="T165">essen-IMP.2PL</ta>
            <ta e="T167" id="Seg_1842" s="T166">1SG-ACC</ta>
            <ta e="T168" id="Seg_1843" s="T167">erinnern-PTCP.PRS</ta>
            <ta e="T169" id="Seg_1844" s="T168">Name-DAT/LOC</ta>
            <ta e="T170" id="Seg_1845" s="T169">so</ta>
            <ta e="T171" id="Seg_1846" s="T170">doch</ta>
            <ta e="T172" id="Seg_1847" s="T171">Tasse.[NOM]</ta>
            <ta e="T173" id="Seg_1848" s="T172">Seite-3SG-INSTR</ta>
            <ta e="T174" id="Seg_1849" s="T173">sprechen-PST2-3SG</ta>
            <ta e="T175" id="Seg_1850" s="T174">treffen-NMNZ.[NOM]</ta>
            <ta e="T176" id="Seg_1851" s="T175">vorbeigehen-PTCP.PST-3SG-ACC</ta>
            <ta e="T177" id="Seg_1852" s="T176">nachdem</ta>
            <ta e="T178" id="Seg_1853" s="T177">dieses</ta>
            <ta e="T179" id="Seg_1854" s="T178">Tasse.[NOM]</ta>
            <ta e="T180" id="Seg_1855" s="T179">sein-PRS.[3SG]</ta>
            <ta e="T181" id="Seg_1856" s="T180">neu</ta>
            <ta e="T182" id="Seg_1857" s="T181">sich.unterhalten-NMNZ.[NOM]</ta>
            <ta e="T183" id="Seg_1858" s="T182">1SG.[NOM]</ta>
            <ta e="T184" id="Seg_1859" s="T183">Blut-1SG-DAT/LOC</ta>
            <ta e="T185" id="Seg_1860" s="T184">was.[NOM]</ta>
            <ta e="T186" id="Seg_1861" s="T185">ausgießen-CAUS-PRS.[3SG]</ta>
            <ta e="T187" id="Seg_1862" s="T186">2PL.[NOM]</ta>
            <ta e="T188" id="Seg_1863" s="T187">Seite-2PL-DAT/LOC</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_1864" s="T0">Пасха.[NOM]</ta>
            <ta e="T2" id="Seg_1865" s="T1">праздник.[NOM]</ta>
            <ta e="T3" id="Seg_1866" s="T2">становиться-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T4" id="Seg_1867" s="T3">Исус.[NOM]</ta>
            <ta e="T5" id="Seg_1868" s="T4">спрашивать-PST2.[3SG]</ta>
            <ta e="T6" id="Seg_1869" s="T5">Петр-ABL</ta>
            <ta e="T7" id="Seg_1870" s="T6">с</ta>
            <ta e="T8" id="Seg_1871" s="T7">Иван-ABL</ta>
            <ta e="T9" id="Seg_1872" s="T8">говорить-CVB.SEQ</ta>
            <ta e="T11" id="Seg_1873" s="T10">идти-EP-IMP.2PL</ta>
            <ta e="T12" id="Seg_1874" s="T11">готовить-IMP.2PL</ta>
            <ta e="T13" id="Seg_1875" s="T12">Пасха-DAT/LOC</ta>
            <ta e="T14" id="Seg_1876" s="T13">есть-PTCP.PRS-1PL-ACC</ta>
            <ta e="T15" id="Seg_1877" s="T14">3PL.[NOM]</ta>
            <ta e="T16" id="Seg_1878" s="T15">однако</ta>
            <ta e="T17" id="Seg_1879" s="T16">спрашивать-PST2-3PL</ta>
            <ta e="T18" id="Seg_1880" s="T17">Исус-ABL</ta>
            <ta e="T19" id="Seg_1881" s="T18">где</ta>
            <ta e="T20" id="Seg_1882" s="T19">готовить-EP-CAUS-PRS-2SG</ta>
            <ta e="T21" id="Seg_1883" s="T20">говорить-CVB.SEQ</ta>
            <ta e="T22" id="Seg_1884" s="T21">Исус.[NOM]</ta>
            <ta e="T23" id="Seg_1885" s="T22">говорить-PST2.[3SG]</ta>
            <ta e="T24" id="Seg_1886" s="T23">там</ta>
            <ta e="T25" id="Seg_1887" s="T24">2PL.[NOM]</ta>
            <ta e="T26" id="Seg_1888" s="T25">город-DAT/LOC</ta>
            <ta e="T27" id="Seg_1889" s="T26">входить-CVB.SEQ-2PL</ta>
            <ta e="T28" id="Seg_1890" s="T27">входить-CVB.SEQ</ta>
            <ta e="T31" id="Seg_1891" s="T30">место-2PL-DAT/LOC</ta>
            <ta e="T32" id="Seg_1892" s="T31">встречать-FUT-2PL</ta>
            <ta e="T33" id="Seg_1893" s="T32">один</ta>
            <ta e="T34" id="Seg_1894" s="T33">человек-ACC</ta>
            <ta e="T35" id="Seg_1895" s="T34">с</ta>
            <ta e="T36" id="Seg_1896" s="T35">уносить-CVB.SEQ</ta>
            <ta e="T37" id="Seg_1897" s="T36">идти-PTCP.PRS-ACC</ta>
            <ta e="T38" id="Seg_1898" s="T37">вода-PROPR</ta>
            <ta e="T39" id="Seg_1899" s="T38">сосуд-ACC</ta>
            <ta e="T40" id="Seg_1900" s="T39">3SG-ACC</ta>
            <ta e="T41" id="Seg_1901" s="T40">следовать-EP-IMP.2PL</ta>
            <ta e="T42" id="Seg_1902" s="T41">дом-DAT/LOC</ta>
            <ta e="T43" id="Seg_1903" s="T42">пока</ta>
            <ta e="T44" id="Seg_1904" s="T43">куда</ta>
            <ta e="T45" id="Seg_1905" s="T44">входить-FUT-3SG</ta>
            <ta e="T46" id="Seg_1906" s="T45">3SG.[NOM]</ta>
            <ta e="T47" id="Seg_1907" s="T46">тот</ta>
            <ta e="T48" id="Seg_1908" s="T47">дом-DAT/LOC</ta>
            <ta e="T49" id="Seg_1909" s="T48">хозяин-3SG-DAT/LOC</ta>
            <ta e="T50" id="Seg_1910" s="T49">говорить-EP-IMP.2PL</ta>
            <ta e="T51" id="Seg_1911" s="T50">учитель.[NOM]</ta>
            <ta e="T52" id="Seg_1912" s="T51">спрашивать-PRS.[3SG]</ta>
            <ta e="T53" id="Seg_1913" s="T52">говорить-IMP.2PL</ta>
            <ta e="T54" id="Seg_1914" s="T53">2SG-ABL</ta>
            <ta e="T55" id="Seg_1915" s="T54">где</ta>
            <ta e="T56" id="Seg_1916" s="T55">есть=Q</ta>
            <ta e="T57" id="Seg_1917" s="T56">дом.[NOM]</ta>
            <ta e="T58" id="Seg_1918" s="T57">комната-3SG.[NOM]</ta>
            <ta e="T59" id="Seg_1919" s="T58">1SG.[NOM]</ta>
            <ta e="T60" id="Seg_1920" s="T59">Пасха-DAT/LOC</ta>
            <ta e="T61" id="Seg_1921" s="T60">есть-PTCP.PRS-1SG-DAT/LOC</ta>
            <ta e="T62" id="Seg_1922" s="T61">сам-1SG.[NOM]</ta>
            <ta e="T63" id="Seg_1923" s="T62">учить-PTCP.PRS</ta>
            <ta e="T64" id="Seg_1924" s="T63">люди-1SG-ACC</ta>
            <ta e="T65" id="Seg_1925" s="T64">с</ta>
            <ta e="T66" id="Seg_1926" s="T65">3SG.[NOM]</ta>
            <ta e="T67" id="Seg_1927" s="T66">показывать-FUT-3SG</ta>
            <ta e="T68" id="Seg_1928" s="T67">2PL-DAT/LOC</ta>
            <ta e="T69" id="Seg_1929" s="T68">широкий</ta>
            <ta e="T70" id="Seg_1930" s="T69">очень</ta>
            <ta e="T71" id="Seg_1931" s="T70">дом.[NOM]</ta>
            <ta e="T72" id="Seg_1932" s="T71">комната-3SG-ACC</ta>
            <ta e="T73" id="Seg_1933" s="T72">пол-3SG.[NOM]</ta>
            <ta e="T74" id="Seg_1934" s="T73">покрывать-NMNZ-PROPR-ACC</ta>
            <ta e="T75" id="Seg_1935" s="T74">там</ta>
            <ta e="T76" id="Seg_1936" s="T75">готовить-IMP.2PL</ta>
            <ta e="T77" id="Seg_1937" s="T76">учить-PTCP.PRS</ta>
            <ta e="T78" id="Seg_1938" s="T77">народ-3SG.[NOM]</ta>
            <ta e="T79" id="Seg_1939" s="T78">туда</ta>
            <ta e="T80" id="Seg_1940" s="T79">идти-CVB.SEQ</ta>
            <ta e="T81" id="Seg_1941" s="T80">найти-PST2-3PL</ta>
            <ta e="T82" id="Seg_1942" s="T81">как</ta>
            <ta e="T83" id="Seg_1943" s="T82">говорить-PTCP.PST-3SG-ACC</ta>
            <ta e="T84" id="Seg_1944" s="T83">подобно</ta>
            <ta e="T85" id="Seg_1945" s="T84">Исус.[NOM]</ta>
            <ta e="T86" id="Seg_1946" s="T85">там</ta>
            <ta e="T87" id="Seg_1947" s="T86">готовить-PST2-3PL</ta>
            <ta e="T88" id="Seg_1948" s="T87">Пасха-ACC</ta>
            <ta e="T89" id="Seg_1949" s="T88">когда</ta>
            <ta e="T90" id="Seg_1950" s="T89">быть-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T91" id="Seg_1951" s="T90">встречать-PTCP.PRS</ta>
            <ta e="T92" id="Seg_1952" s="T91">час-3PL.[NOM]</ta>
            <ta e="T93" id="Seg_1953" s="T92">Исус.[NOM]</ta>
            <ta e="T94" id="Seg_1954" s="T93">сесть-PST2.[3SG]</ta>
            <ta e="T95" id="Seg_1955" s="T94">стол.[NOM]</ta>
            <ta e="T96" id="Seg_1956" s="T95">передняя.часть-3SG-DAT/LOC</ta>
            <ta e="T97" id="Seg_1957" s="T96">десять</ta>
            <ta e="T98" id="Seg_1958" s="T97">два</ta>
            <ta e="T99" id="Seg_1959" s="T98">учить-PTCP.PRS</ta>
            <ta e="T100" id="Seg_1960" s="T99">человек-PL-3SG-ACC</ta>
            <ta e="T101" id="Seg_1961" s="T100">с</ta>
            <ta e="T102" id="Seg_1962" s="T101">тогда</ta>
            <ta e="T103" id="Seg_1963" s="T102">говорить-PST2.[3SG]</ta>
            <ta e="T104" id="Seg_1964" s="T103">1SG.[NOM]</ta>
            <ta e="T105" id="Seg_1965" s="T104">очень</ta>
            <ta e="T106" id="Seg_1966" s="T105">хотеть-PST1-1SG</ta>
            <ta e="T107" id="Seg_1967" s="T106">есть-CVB.PURP</ta>
            <ta e="T108" id="Seg_1968" s="T107">2PL-ACC</ta>
            <ta e="T109" id="Seg_1969" s="T108">с</ta>
            <ta e="T110" id="Seg_1970" s="T109">1SG.[NOM]</ta>
            <ta e="T111" id="Seg_1971" s="T110">мука-VBZ-PTCP.PRS-EP-1SG.[NOM]</ta>
            <ta e="T112" id="Seg_1972" s="T111">передняя.часть-3SG-DAT/LOC</ta>
            <ta e="T113" id="Seg_1973" s="T112">говорить-PRS-1SG</ta>
            <ta e="T114" id="Seg_1974" s="T113">2PL-DAT/LOC</ta>
            <ta e="T115" id="Seg_1975" s="T114">есть-FUT-1SG</ta>
            <ta e="T116" id="Seg_1976" s="T115">NEG-3SG</ta>
            <ta e="T117" id="Seg_1977" s="T116">тот-ACC</ta>
            <ta e="T118" id="Seg_1978" s="T117">что.[NOM]</ta>
            <ta e="T119" id="Seg_1979" s="T118">быть-CVB.SEQ</ta>
            <ta e="T120" id="Seg_1980" s="T119">еще.не-3SG</ta>
            <ta e="T121" id="Seg_1981" s="T120">небо.[NOM]</ta>
            <ta e="T122" id="Seg_1982" s="T121">царь-3SG-DAT/LOC</ta>
            <ta e="T123" id="Seg_1983" s="T122">чашка-ACC</ta>
            <ta e="T124" id="Seg_1984" s="T123">взять-CVB.ANT</ta>
            <ta e="T125" id="Seg_1985" s="T124">хороший</ta>
            <ta e="T126" id="Seg_1986" s="T125">слово-3SG-ACC</ta>
            <ta e="T127" id="Seg_1987" s="T126">говорить-CVB.SEQ</ta>
            <ta e="T128" id="Seg_1988" s="T127">говорить-PST2.[3SG]</ta>
            <ta e="T129" id="Seg_1989" s="T128">взять-EP-IMP.2PL</ta>
            <ta e="T130" id="Seg_1990" s="T129">тот-ACC</ta>
            <ta e="T131" id="Seg_1991" s="T130">разделить-EP-IMP.2PL</ta>
            <ta e="T132" id="Seg_1992" s="T131">говорить-PRS-1SG</ta>
            <ta e="T133" id="Seg_1993" s="T132">2PL-DAT/LOC</ta>
            <ta e="T134" id="Seg_1994" s="T133">пить-FUT-1SG</ta>
            <ta e="T135" id="Seg_1995" s="T134">NEG-3SG</ta>
            <ta e="T138" id="Seg_1996" s="T137">виноград.[NOM]</ta>
            <ta e="T139" id="Seg_1997" s="T138">ягода.[NOM]</ta>
            <ta e="T140" id="Seg_1998" s="T139">вино-3SG-ACC</ta>
            <ta e="T141" id="Seg_1999" s="T140">приходить-CVB.SIM</ta>
            <ta e="T142" id="Seg_2000" s="T141">еще.не-3SG</ta>
            <ta e="T143" id="Seg_2001" s="T142">небо.[NOM]</ta>
            <ta e="T144" id="Seg_2002" s="T143">царь-3SG.[NOM]</ta>
            <ta e="T145" id="Seg_2003" s="T144">хлеб-ACC</ta>
            <ta e="T146" id="Seg_2004" s="T145">взять-CVB.ANT</ta>
            <ta e="T147" id="Seg_2005" s="T146">хороший</ta>
            <ta e="T148" id="Seg_2006" s="T147">слово-ACC</ta>
            <ta e="T149" id="Seg_2007" s="T148">говорить-CVB.SEQ</ta>
            <ta e="T150" id="Seg_2008" s="T149">после</ta>
            <ta e="T151" id="Seg_2009" s="T150">тот-ACC</ta>
            <ta e="T152" id="Seg_2010" s="T151">ломать-CVB.SEQ</ta>
            <ta e="T153" id="Seg_2011" s="T152">разделить-EP-PST2.[3SG]</ta>
            <ta e="T154" id="Seg_2012" s="T153">3PL-DAT/LOC</ta>
            <ta e="T155" id="Seg_2013" s="T154">говорить-CVB.SEQ</ta>
            <ta e="T156" id="Seg_2014" s="T155">тот.[NOM]</ta>
            <ta e="T157" id="Seg_2015" s="T156">быть-PRS.[3SG]</ta>
            <ta e="T158" id="Seg_2016" s="T157">1SG.[NOM]</ta>
            <ta e="T159" id="Seg_2017" s="T158">тело-EP-1SG.[NOM]</ta>
            <ta e="T160" id="Seg_2018" s="T159">что.[NOM]</ta>
            <ta e="T161" id="Seg_2019" s="T160">2PL.[NOM]</ta>
            <ta e="T162" id="Seg_2020" s="T161">сторона-2PL-DAT/LOC</ta>
            <ta e="T163" id="Seg_2021" s="T162">смерть-DAT/LOC</ta>
            <ta e="T164" id="Seg_2022" s="T163">давать-PRS-3PL</ta>
            <ta e="T165" id="Seg_2023" s="T164">тот-ACC</ta>
            <ta e="T166" id="Seg_2024" s="T165">есть-IMP.2PL</ta>
            <ta e="T167" id="Seg_2025" s="T166">1SG-ACC</ta>
            <ta e="T168" id="Seg_2026" s="T167">помнить-PTCP.PRS</ta>
            <ta e="T169" id="Seg_2027" s="T168">имя-DAT/LOC</ta>
            <ta e="T170" id="Seg_2028" s="T169">так</ta>
            <ta e="T171" id="Seg_2029" s="T170">ведь</ta>
            <ta e="T172" id="Seg_2030" s="T171">чашка.[NOM]</ta>
            <ta e="T173" id="Seg_2031" s="T172">сторона-3SG-INSTR</ta>
            <ta e="T174" id="Seg_2032" s="T173">говорить-PST2-3SG</ta>
            <ta e="T175" id="Seg_2033" s="T174">встречать-NMNZ.[NOM]</ta>
            <ta e="T176" id="Seg_2034" s="T175">проехать-PTCP.PST-3SG-ACC</ta>
            <ta e="T177" id="Seg_2035" s="T176">после.того</ta>
            <ta e="T178" id="Seg_2036" s="T177">этот</ta>
            <ta e="T179" id="Seg_2037" s="T178">чашка.[NOM]</ta>
            <ta e="T180" id="Seg_2038" s="T179">быть-PRS.[3SG]</ta>
            <ta e="T181" id="Seg_2039" s="T180">новый</ta>
            <ta e="T182" id="Seg_2040" s="T181">разговаривать-NMNZ.[NOM]</ta>
            <ta e="T183" id="Seg_2041" s="T182">1SG.[NOM]</ta>
            <ta e="T184" id="Seg_2042" s="T183">кровь-1SG-DAT/LOC</ta>
            <ta e="T185" id="Seg_2043" s="T184">что.[NOM]</ta>
            <ta e="T186" id="Seg_2044" s="T185">выливать-CAUS-PRS.[3SG]</ta>
            <ta e="T187" id="Seg_2045" s="T186">2PL.[NOM]</ta>
            <ta e="T188" id="Seg_2046" s="T187">сторона-2PL-DAT/LOC</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_2047" s="T0">propr-n:case</ta>
            <ta e="T2" id="Seg_2048" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_2049" s="T2">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T4" id="Seg_2050" s="T3">propr-n:case</ta>
            <ta e="T5" id="Seg_2051" s="T4">v-v:tense-v:pred.pn</ta>
            <ta e="T6" id="Seg_2052" s="T5">propr-n:case</ta>
            <ta e="T7" id="Seg_2053" s="T6">post</ta>
            <ta e="T8" id="Seg_2054" s="T7">propr-n:case</ta>
            <ta e="T9" id="Seg_2055" s="T8">v-v:cvb</ta>
            <ta e="T11" id="Seg_2056" s="T10">v-v:(ins)-v:mood.pn</ta>
            <ta e="T12" id="Seg_2057" s="T11">v-v:mood.pn</ta>
            <ta e="T13" id="Seg_2058" s="T12">propr-n:case</ta>
            <ta e="T14" id="Seg_2059" s="T13">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T15" id="Seg_2060" s="T14">pers-pro:case</ta>
            <ta e="T16" id="Seg_2061" s="T15">ptcl</ta>
            <ta e="T17" id="Seg_2062" s="T16">v-v:tense-v:pred.pn</ta>
            <ta e="T18" id="Seg_2063" s="T17">propr-n:case</ta>
            <ta e="T19" id="Seg_2064" s="T18">que</ta>
            <ta e="T20" id="Seg_2065" s="T19">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T21" id="Seg_2066" s="T20">v-v:cvb</ta>
            <ta e="T22" id="Seg_2067" s="T21">propr-n:case</ta>
            <ta e="T23" id="Seg_2068" s="T22">v-v:tense-v:pred.pn</ta>
            <ta e="T24" id="Seg_2069" s="T23">adv</ta>
            <ta e="T25" id="Seg_2070" s="T24">pers-pro:case</ta>
            <ta e="T26" id="Seg_2071" s="T25">n-n:case</ta>
            <ta e="T27" id="Seg_2072" s="T26">v-v:cvb-v:pred.pn</ta>
            <ta e="T28" id="Seg_2073" s="T27">v-v:cvb</ta>
            <ta e="T31" id="Seg_2074" s="T30">n-n:poss-n:case</ta>
            <ta e="T32" id="Seg_2075" s="T31">v-v:tense-v:poss.pn</ta>
            <ta e="T33" id="Seg_2076" s="T32">cardnum</ta>
            <ta e="T34" id="Seg_2077" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_2078" s="T34">post</ta>
            <ta e="T36" id="Seg_2079" s="T35">v-v:cvb</ta>
            <ta e="T37" id="Seg_2080" s="T36">v-v:ptcp-v:(case)</ta>
            <ta e="T38" id="Seg_2081" s="T37">n-n&gt;adj</ta>
            <ta e="T39" id="Seg_2082" s="T38">n-n:case</ta>
            <ta e="T40" id="Seg_2083" s="T39">pers-pro:case</ta>
            <ta e="T41" id="Seg_2084" s="T40">v-v:(ins)-v:mood.pn</ta>
            <ta e="T42" id="Seg_2085" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_2086" s="T42">post</ta>
            <ta e="T44" id="Seg_2087" s="T43">que</ta>
            <ta e="T45" id="Seg_2088" s="T44">v-v:tense-v:poss.pn</ta>
            <ta e="T46" id="Seg_2089" s="T45">pers-pro:case</ta>
            <ta e="T47" id="Seg_2090" s="T46">dempro</ta>
            <ta e="T48" id="Seg_2091" s="T47">n-n:case</ta>
            <ta e="T49" id="Seg_2092" s="T48">n-n:poss-n:case</ta>
            <ta e="T50" id="Seg_2093" s="T49">v-v:(ins)-v:mood.pn</ta>
            <ta e="T51" id="Seg_2094" s="T50">n-n:case</ta>
            <ta e="T52" id="Seg_2095" s="T51">v-v:tense-v:pred.pn</ta>
            <ta e="T53" id="Seg_2096" s="T52">v-v:mood.pn</ta>
            <ta e="T54" id="Seg_2097" s="T53">pers-pro:case</ta>
            <ta e="T55" id="Seg_2098" s="T54">que</ta>
            <ta e="T56" id="Seg_2099" s="T55">ptcl-ptcl</ta>
            <ta e="T57" id="Seg_2100" s="T56">n-n:case</ta>
            <ta e="T58" id="Seg_2101" s="T57">n-n:(poss)-n:case</ta>
            <ta e="T59" id="Seg_2102" s="T58">pers-pro:case</ta>
            <ta e="T60" id="Seg_2103" s="T59">propr-n:case</ta>
            <ta e="T61" id="Seg_2104" s="T60">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T62" id="Seg_2105" s="T61">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T63" id="Seg_2106" s="T62">v-v:ptcp</ta>
            <ta e="T64" id="Seg_2107" s="T63">n-n:poss-n:case</ta>
            <ta e="T65" id="Seg_2108" s="T64">post</ta>
            <ta e="T66" id="Seg_2109" s="T65">pers-pro:case</ta>
            <ta e="T67" id="Seg_2110" s="T66">v-v:tense-v:poss.pn</ta>
            <ta e="T68" id="Seg_2111" s="T67">pers-pro:case</ta>
            <ta e="T69" id="Seg_2112" s="T68">adj</ta>
            <ta e="T70" id="Seg_2113" s="T69">ptcl</ta>
            <ta e="T71" id="Seg_2114" s="T70">n-n:case</ta>
            <ta e="T72" id="Seg_2115" s="T71">n-n:poss-n:case</ta>
            <ta e="T73" id="Seg_2116" s="T72">n-n:(poss)-n:case</ta>
            <ta e="T74" id="Seg_2117" s="T73">v-v&gt;n-n&gt;adj-n:case</ta>
            <ta e="T75" id="Seg_2118" s="T74">adv</ta>
            <ta e="T76" id="Seg_2119" s="T75">v-v:mood.pn</ta>
            <ta e="T77" id="Seg_2120" s="T76">v-v:ptcp</ta>
            <ta e="T78" id="Seg_2121" s="T77">n-n:(poss)-n:case</ta>
            <ta e="T79" id="Seg_2122" s="T78">adv</ta>
            <ta e="T80" id="Seg_2123" s="T79">v-v:cvb</ta>
            <ta e="T81" id="Seg_2124" s="T80">v-v:tense-v:pred.pn</ta>
            <ta e="T82" id="Seg_2125" s="T81">que</ta>
            <ta e="T83" id="Seg_2126" s="T82">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T84" id="Seg_2127" s="T83">post</ta>
            <ta e="T85" id="Seg_2128" s="T84">propr-n:case</ta>
            <ta e="T86" id="Seg_2129" s="T85">adv</ta>
            <ta e="T87" id="Seg_2130" s="T86">v-v:tense-v:pred.pn</ta>
            <ta e="T88" id="Seg_2131" s="T87">propr-n:case</ta>
            <ta e="T89" id="Seg_2132" s="T88">conj</ta>
            <ta e="T90" id="Seg_2133" s="T89">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T91" id="Seg_2134" s="T90">v-v:ptcp</ta>
            <ta e="T92" id="Seg_2135" s="T91">n-n:(poss)-n:case</ta>
            <ta e="T93" id="Seg_2136" s="T92">propr-n:case</ta>
            <ta e="T94" id="Seg_2137" s="T93">v-v:tense-v:pred.pn</ta>
            <ta e="T95" id="Seg_2138" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_2139" s="T95">n-n:poss-n:case</ta>
            <ta e="T97" id="Seg_2140" s="T96">cardnum</ta>
            <ta e="T98" id="Seg_2141" s="T97">cardnum</ta>
            <ta e="T99" id="Seg_2142" s="T98">v-v:ptcp</ta>
            <ta e="T100" id="Seg_2143" s="T99">n-n:(num)-n:poss-n:case</ta>
            <ta e="T101" id="Seg_2144" s="T100">post</ta>
            <ta e="T102" id="Seg_2145" s="T101">adv</ta>
            <ta e="T103" id="Seg_2146" s="T102">v-v:tense-v:pred.pn</ta>
            <ta e="T104" id="Seg_2147" s="T103">pers-pro:case</ta>
            <ta e="T105" id="Seg_2148" s="T104">adv</ta>
            <ta e="T106" id="Seg_2149" s="T105">v-v:tense-v:poss.pn</ta>
            <ta e="T107" id="Seg_2150" s="T106">v-v:cvb</ta>
            <ta e="T108" id="Seg_2151" s="T107">pers-pro:case</ta>
            <ta e="T109" id="Seg_2152" s="T108">post</ta>
            <ta e="T110" id="Seg_2153" s="T109">pers-pro:case</ta>
            <ta e="T111" id="Seg_2154" s="T110">n-n&gt;v-v:ptcp-v:(ins)-v:(poss)-v:(case)</ta>
            <ta e="T112" id="Seg_2155" s="T111">n-n:poss-n:case</ta>
            <ta e="T113" id="Seg_2156" s="T112">v-v:tense-v:pred.pn</ta>
            <ta e="T114" id="Seg_2157" s="T113">pers-pro:case</ta>
            <ta e="T115" id="Seg_2158" s="T114">v-v:tense-v:poss.pn</ta>
            <ta e="T116" id="Seg_2159" s="T115">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T117" id="Seg_2160" s="T116">dempro-pro:case</ta>
            <ta e="T118" id="Seg_2161" s="T117">que-pro:case</ta>
            <ta e="T119" id="Seg_2162" s="T118">v-v:cvb</ta>
            <ta e="T120" id="Seg_2163" s="T119">ptcl-ptcl:(temp.pn)</ta>
            <ta e="T121" id="Seg_2164" s="T120">n-n:case</ta>
            <ta e="T122" id="Seg_2165" s="T121">n-n:poss-n:case</ta>
            <ta e="T123" id="Seg_2166" s="T122">n-n:case</ta>
            <ta e="T124" id="Seg_2167" s="T123">v-v:cvb</ta>
            <ta e="T125" id="Seg_2168" s="T124">adj</ta>
            <ta e="T126" id="Seg_2169" s="T125">n-n:poss-n:case</ta>
            <ta e="T127" id="Seg_2170" s="T126">v-v:cvb</ta>
            <ta e="T128" id="Seg_2171" s="T127">v-v:tense-v:pred.pn</ta>
            <ta e="T129" id="Seg_2172" s="T128">v-v:(ins)-v:mood.pn</ta>
            <ta e="T130" id="Seg_2173" s="T129">dempro-pro:case</ta>
            <ta e="T131" id="Seg_2174" s="T130">v-v:(ins)-v:mood.pn</ta>
            <ta e="T132" id="Seg_2175" s="T131">v-v:tense-v:pred.pn</ta>
            <ta e="T133" id="Seg_2176" s="T132">pers-pro:case</ta>
            <ta e="T134" id="Seg_2177" s="T133">v-v:tense-v:poss.pn</ta>
            <ta e="T135" id="Seg_2178" s="T134">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T138" id="Seg_2179" s="T137">n-n:case</ta>
            <ta e="T139" id="Seg_2180" s="T138">n-n:case</ta>
            <ta e="T140" id="Seg_2181" s="T139">n-n:poss-n:case</ta>
            <ta e="T141" id="Seg_2182" s="T140">v-v:cvb</ta>
            <ta e="T142" id="Seg_2183" s="T141">ptcl-ptcl:(temp.pn)</ta>
            <ta e="T143" id="Seg_2184" s="T142">n-n:case</ta>
            <ta e="T144" id="Seg_2185" s="T143">n-n:(poss)-n:case</ta>
            <ta e="T145" id="Seg_2186" s="T144">n-n:case</ta>
            <ta e="T146" id="Seg_2187" s="T145">v-v:cvb</ta>
            <ta e="T147" id="Seg_2188" s="T146">adj</ta>
            <ta e="T148" id="Seg_2189" s="T147">n-n:case</ta>
            <ta e="T149" id="Seg_2190" s="T148">v-v:cvb</ta>
            <ta e="T150" id="Seg_2191" s="T149">post</ta>
            <ta e="T151" id="Seg_2192" s="T150">dempro-pro:case</ta>
            <ta e="T152" id="Seg_2193" s="T151">v-v:cvb</ta>
            <ta e="T153" id="Seg_2194" s="T152">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T154" id="Seg_2195" s="T153">pers-pro:case</ta>
            <ta e="T155" id="Seg_2196" s="T154">v-v:cvb</ta>
            <ta e="T156" id="Seg_2197" s="T155">dempro-pro:case</ta>
            <ta e="T157" id="Seg_2198" s="T156">v-v:tense-v:pred.pn</ta>
            <ta e="T158" id="Seg_2199" s="T157">pers-pro:case</ta>
            <ta e="T159" id="Seg_2200" s="T158">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T160" id="Seg_2201" s="T159">que-pro:case</ta>
            <ta e="T161" id="Seg_2202" s="T160">pers-pro:case</ta>
            <ta e="T162" id="Seg_2203" s="T161">n-n:poss-n:case</ta>
            <ta e="T163" id="Seg_2204" s="T162">n-n:case</ta>
            <ta e="T164" id="Seg_2205" s="T163">v-v:tense-v:pred.pn</ta>
            <ta e="T165" id="Seg_2206" s="T164">dempro-pro:case</ta>
            <ta e="T166" id="Seg_2207" s="T165">v-v:mood.pn</ta>
            <ta e="T167" id="Seg_2208" s="T166">pers-pro:case</ta>
            <ta e="T168" id="Seg_2209" s="T167">v-v:ptcp</ta>
            <ta e="T169" id="Seg_2210" s="T168">n-n:case</ta>
            <ta e="T170" id="Seg_2211" s="T169">adv</ta>
            <ta e="T171" id="Seg_2212" s="T170">ptcl</ta>
            <ta e="T172" id="Seg_2213" s="T171">n-n:case</ta>
            <ta e="T173" id="Seg_2214" s="T172">n-n:poss-n:case</ta>
            <ta e="T174" id="Seg_2215" s="T173">v-v:tense-v:poss.pn</ta>
            <ta e="T175" id="Seg_2216" s="T174">v-v&gt;n-n:case</ta>
            <ta e="T176" id="Seg_2217" s="T175">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T177" id="Seg_2218" s="T176">post</ta>
            <ta e="T178" id="Seg_2219" s="T177">dempro</ta>
            <ta e="T179" id="Seg_2220" s="T178">n-n:case</ta>
            <ta e="T180" id="Seg_2221" s="T179">v-v:tense-v:pred.pn</ta>
            <ta e="T181" id="Seg_2222" s="T180">adj</ta>
            <ta e="T182" id="Seg_2223" s="T181">v-v&gt;n-n:case</ta>
            <ta e="T183" id="Seg_2224" s="T182">pers-pro:case</ta>
            <ta e="T184" id="Seg_2225" s="T183">n-n:poss-n:case</ta>
            <ta e="T185" id="Seg_2226" s="T184">que-pro:case</ta>
            <ta e="T186" id="Seg_2227" s="T185">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T187" id="Seg_2228" s="T186">pers-pro:case</ta>
            <ta e="T188" id="Seg_2229" s="T187">n-n:poss-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_2230" s="T0">propr</ta>
            <ta e="T2" id="Seg_2231" s="T1">n</ta>
            <ta e="T3" id="Seg_2232" s="T2">v</ta>
            <ta e="T4" id="Seg_2233" s="T3">propr</ta>
            <ta e="T5" id="Seg_2234" s="T4">v</ta>
            <ta e="T6" id="Seg_2235" s="T5">propr</ta>
            <ta e="T7" id="Seg_2236" s="T6">post</ta>
            <ta e="T8" id="Seg_2237" s="T7">propr</ta>
            <ta e="T9" id="Seg_2238" s="T8">v</ta>
            <ta e="T11" id="Seg_2239" s="T10">v</ta>
            <ta e="T12" id="Seg_2240" s="T11">v</ta>
            <ta e="T13" id="Seg_2241" s="T12">propr</ta>
            <ta e="T14" id="Seg_2242" s="T13">v</ta>
            <ta e="T15" id="Seg_2243" s="T14">pers</ta>
            <ta e="T16" id="Seg_2244" s="T15">ptcl</ta>
            <ta e="T17" id="Seg_2245" s="T16">v</ta>
            <ta e="T18" id="Seg_2246" s="T17">propr</ta>
            <ta e="T19" id="Seg_2247" s="T18">que</ta>
            <ta e="T20" id="Seg_2248" s="T19">v</ta>
            <ta e="T21" id="Seg_2249" s="T20">v</ta>
            <ta e="T22" id="Seg_2250" s="T21">propr</ta>
            <ta e="T23" id="Seg_2251" s="T22">v</ta>
            <ta e="T24" id="Seg_2252" s="T23">adv</ta>
            <ta e="T25" id="Seg_2253" s="T24">pers</ta>
            <ta e="T26" id="Seg_2254" s="T25">n</ta>
            <ta e="T27" id="Seg_2255" s="T26">v</ta>
            <ta e="T28" id="Seg_2256" s="T27">v</ta>
            <ta e="T31" id="Seg_2257" s="T30">n</ta>
            <ta e="T32" id="Seg_2258" s="T31">v</ta>
            <ta e="T33" id="Seg_2259" s="T32">cardnum</ta>
            <ta e="T34" id="Seg_2260" s="T33">n</ta>
            <ta e="T35" id="Seg_2261" s="T34">post</ta>
            <ta e="T36" id="Seg_2262" s="T35">v</ta>
            <ta e="T37" id="Seg_2263" s="T36">aux</ta>
            <ta e="T38" id="Seg_2264" s="T37">adj</ta>
            <ta e="T39" id="Seg_2265" s="T38">n</ta>
            <ta e="T40" id="Seg_2266" s="T39">pers</ta>
            <ta e="T41" id="Seg_2267" s="T40">v</ta>
            <ta e="T42" id="Seg_2268" s="T41">n</ta>
            <ta e="T43" id="Seg_2269" s="T42">post</ta>
            <ta e="T44" id="Seg_2270" s="T43">que</ta>
            <ta e="T45" id="Seg_2271" s="T44">v</ta>
            <ta e="T46" id="Seg_2272" s="T45">pers</ta>
            <ta e="T47" id="Seg_2273" s="T46">dempro</ta>
            <ta e="T48" id="Seg_2274" s="T47">n</ta>
            <ta e="T49" id="Seg_2275" s="T48">n</ta>
            <ta e="T50" id="Seg_2276" s="T49">v</ta>
            <ta e="T51" id="Seg_2277" s="T50">n</ta>
            <ta e="T52" id="Seg_2278" s="T51">v</ta>
            <ta e="T53" id="Seg_2279" s="T52">v</ta>
            <ta e="T54" id="Seg_2280" s="T53">pers</ta>
            <ta e="T55" id="Seg_2281" s="T54">que</ta>
            <ta e="T56" id="Seg_2282" s="T55">ptcl</ta>
            <ta e="T57" id="Seg_2283" s="T56">n</ta>
            <ta e="T58" id="Seg_2284" s="T57">n</ta>
            <ta e="T59" id="Seg_2285" s="T58">pers</ta>
            <ta e="T60" id="Seg_2286" s="T59">propr</ta>
            <ta e="T61" id="Seg_2287" s="T60">v</ta>
            <ta e="T62" id="Seg_2288" s="T61">emphpro</ta>
            <ta e="T63" id="Seg_2289" s="T62">v</ta>
            <ta e="T64" id="Seg_2290" s="T63">n</ta>
            <ta e="T65" id="Seg_2291" s="T64">post</ta>
            <ta e="T66" id="Seg_2292" s="T65">pers</ta>
            <ta e="T67" id="Seg_2293" s="T66">v</ta>
            <ta e="T68" id="Seg_2294" s="T67">pers</ta>
            <ta e="T69" id="Seg_2295" s="T68">adj</ta>
            <ta e="T70" id="Seg_2296" s="T69">ptcl</ta>
            <ta e="T71" id="Seg_2297" s="T70">n</ta>
            <ta e="T72" id="Seg_2298" s="T71">n</ta>
            <ta e="T73" id="Seg_2299" s="T72">n</ta>
            <ta e="T74" id="Seg_2300" s="T73">adj</ta>
            <ta e="T75" id="Seg_2301" s="T74">adv</ta>
            <ta e="T76" id="Seg_2302" s="T75">v</ta>
            <ta e="T77" id="Seg_2303" s="T76">v</ta>
            <ta e="T78" id="Seg_2304" s="T77">n</ta>
            <ta e="T79" id="Seg_2305" s="T78">adv</ta>
            <ta e="T80" id="Seg_2306" s="T79">v</ta>
            <ta e="T81" id="Seg_2307" s="T80">v</ta>
            <ta e="T82" id="Seg_2308" s="T81">que</ta>
            <ta e="T83" id="Seg_2309" s="T82">v</ta>
            <ta e="T84" id="Seg_2310" s="T83">post</ta>
            <ta e="T85" id="Seg_2311" s="T84">propr</ta>
            <ta e="T86" id="Seg_2312" s="T85">adv</ta>
            <ta e="T87" id="Seg_2313" s="T86">v</ta>
            <ta e="T88" id="Seg_2314" s="T87">propr</ta>
            <ta e="T89" id="Seg_2315" s="T88">conj</ta>
            <ta e="T90" id="Seg_2316" s="T89">cop</ta>
            <ta e="T91" id="Seg_2317" s="T90">v</ta>
            <ta e="T92" id="Seg_2318" s="T91">n</ta>
            <ta e="T93" id="Seg_2319" s="T92">propr</ta>
            <ta e="T94" id="Seg_2320" s="T93">v</ta>
            <ta e="T95" id="Seg_2321" s="T94">n</ta>
            <ta e="T96" id="Seg_2322" s="T95">n</ta>
            <ta e="T97" id="Seg_2323" s="T96">cardnum</ta>
            <ta e="T98" id="Seg_2324" s="T97">cardnum</ta>
            <ta e="T99" id="Seg_2325" s="T98">v</ta>
            <ta e="T100" id="Seg_2326" s="T99">n</ta>
            <ta e="T101" id="Seg_2327" s="T100">post</ta>
            <ta e="T102" id="Seg_2328" s="T101">adv</ta>
            <ta e="T103" id="Seg_2329" s="T102">v</ta>
            <ta e="T104" id="Seg_2330" s="T103">pers</ta>
            <ta e="T105" id="Seg_2331" s="T104">adv</ta>
            <ta e="T106" id="Seg_2332" s="T105">v</ta>
            <ta e="T107" id="Seg_2333" s="T106">v</ta>
            <ta e="T108" id="Seg_2334" s="T107">pers</ta>
            <ta e="T109" id="Seg_2335" s="T108">post</ta>
            <ta e="T110" id="Seg_2336" s="T109">pers</ta>
            <ta e="T111" id="Seg_2337" s="T110">v</ta>
            <ta e="T112" id="Seg_2338" s="T111">n</ta>
            <ta e="T113" id="Seg_2339" s="T112">v</ta>
            <ta e="T114" id="Seg_2340" s="T113">pers</ta>
            <ta e="T115" id="Seg_2341" s="T114">v</ta>
            <ta e="T116" id="Seg_2342" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_2343" s="T116">dempro</ta>
            <ta e="T118" id="Seg_2344" s="T117">que</ta>
            <ta e="T119" id="Seg_2345" s="T118">cop</ta>
            <ta e="T120" id="Seg_2346" s="T119">ptcl</ta>
            <ta e="T121" id="Seg_2347" s="T120">n</ta>
            <ta e="T122" id="Seg_2348" s="T121">n</ta>
            <ta e="T123" id="Seg_2349" s="T122">n</ta>
            <ta e="T124" id="Seg_2350" s="T123">v</ta>
            <ta e="T125" id="Seg_2351" s="T124">adj</ta>
            <ta e="T126" id="Seg_2352" s="T125">n</ta>
            <ta e="T127" id="Seg_2353" s="T126">v</ta>
            <ta e="T128" id="Seg_2354" s="T127">v</ta>
            <ta e="T129" id="Seg_2355" s="T128">v</ta>
            <ta e="T130" id="Seg_2356" s="T129">dempro</ta>
            <ta e="T131" id="Seg_2357" s="T130">v</ta>
            <ta e="T132" id="Seg_2358" s="T131">v</ta>
            <ta e="T133" id="Seg_2359" s="T132">pers</ta>
            <ta e="T134" id="Seg_2360" s="T133">v</ta>
            <ta e="T135" id="Seg_2361" s="T134">ptcl</ta>
            <ta e="T138" id="Seg_2362" s="T137">n</ta>
            <ta e="T139" id="Seg_2363" s="T138">n</ta>
            <ta e="T140" id="Seg_2364" s="T139">n</ta>
            <ta e="T141" id="Seg_2365" s="T140">v</ta>
            <ta e="T142" id="Seg_2366" s="T141">ptcl</ta>
            <ta e="T143" id="Seg_2367" s="T142">n</ta>
            <ta e="T144" id="Seg_2368" s="T143">n</ta>
            <ta e="T145" id="Seg_2369" s="T144">n</ta>
            <ta e="T146" id="Seg_2370" s="T145">v</ta>
            <ta e="T147" id="Seg_2371" s="T146">adj</ta>
            <ta e="T148" id="Seg_2372" s="T147">n</ta>
            <ta e="T149" id="Seg_2373" s="T148">v</ta>
            <ta e="T150" id="Seg_2374" s="T149">post</ta>
            <ta e="T151" id="Seg_2375" s="T150">dempro</ta>
            <ta e="T152" id="Seg_2376" s="T151">v</ta>
            <ta e="T153" id="Seg_2377" s="T152">v</ta>
            <ta e="T154" id="Seg_2378" s="T153">pers</ta>
            <ta e="T155" id="Seg_2379" s="T154">v</ta>
            <ta e="T156" id="Seg_2380" s="T155">dempro</ta>
            <ta e="T157" id="Seg_2381" s="T156">cop</ta>
            <ta e="T158" id="Seg_2382" s="T157">pers</ta>
            <ta e="T159" id="Seg_2383" s="T158">n</ta>
            <ta e="T160" id="Seg_2384" s="T159">que</ta>
            <ta e="T161" id="Seg_2385" s="T160">pers</ta>
            <ta e="T162" id="Seg_2386" s="T161">n</ta>
            <ta e="T163" id="Seg_2387" s="T162">n</ta>
            <ta e="T164" id="Seg_2388" s="T163">v</ta>
            <ta e="T165" id="Seg_2389" s="T164">dempro</ta>
            <ta e="T166" id="Seg_2390" s="T165">v</ta>
            <ta e="T167" id="Seg_2391" s="T166">pers</ta>
            <ta e="T168" id="Seg_2392" s="T167">v</ta>
            <ta e="T169" id="Seg_2393" s="T168">n</ta>
            <ta e="T170" id="Seg_2394" s="T169">adv</ta>
            <ta e="T171" id="Seg_2395" s="T170">ptcl</ta>
            <ta e="T172" id="Seg_2396" s="T171">n</ta>
            <ta e="T173" id="Seg_2397" s="T172">n</ta>
            <ta e="T174" id="Seg_2398" s="T173">v</ta>
            <ta e="T175" id="Seg_2399" s="T174">n</ta>
            <ta e="T176" id="Seg_2400" s="T175">v</ta>
            <ta e="T177" id="Seg_2401" s="T176">post</ta>
            <ta e="T178" id="Seg_2402" s="T177">dempro</ta>
            <ta e="T179" id="Seg_2403" s="T178">n</ta>
            <ta e="T180" id="Seg_2404" s="T179">cop</ta>
            <ta e="T181" id="Seg_2405" s="T180">adj</ta>
            <ta e="T182" id="Seg_2406" s="T181">n</ta>
            <ta e="T183" id="Seg_2407" s="T182">pers</ta>
            <ta e="T184" id="Seg_2408" s="T183">n</ta>
            <ta e="T185" id="Seg_2409" s="T184">que</ta>
            <ta e="T186" id="Seg_2410" s="T185">v</ta>
            <ta e="T187" id="Seg_2411" s="T186">pers</ta>
            <ta e="T188" id="Seg_2412" s="T187">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T2" id="Seg_2413" s="T1">np:Th</ta>
            <ta e="T4" id="Seg_2414" s="T3">np.h:A</ta>
            <ta e="T7" id="Seg_2415" s="T5">pp:Com</ta>
            <ta e="T8" id="Seg_2416" s="T7">np.h:R</ta>
            <ta e="T11" id="Seg_2417" s="T10">0.2.h:A</ta>
            <ta e="T12" id="Seg_2418" s="T11">0.2.h:A</ta>
            <ta e="T13" id="Seg_2419" s="T12">n:Time</ta>
            <ta e="T15" id="Seg_2420" s="T14">pro.h:A</ta>
            <ta e="T18" id="Seg_2421" s="T17">np.h:R</ta>
            <ta e="T19" id="Seg_2422" s="T18">pro:L</ta>
            <ta e="T20" id="Seg_2423" s="T19">0.2.h:A</ta>
            <ta e="T22" id="Seg_2424" s="T21">np.h:A</ta>
            <ta e="T24" id="Seg_2425" s="T23">adv:L</ta>
            <ta e="T25" id="Seg_2426" s="T24">pro.h:A</ta>
            <ta e="T26" id="Seg_2427" s="T25">np:G</ta>
            <ta e="T31" id="Seg_2428" s="T30">np:L</ta>
            <ta e="T32" id="Seg_2429" s="T31">0.2.h:E</ta>
            <ta e="T35" id="Seg_2430" s="T33">pp:Com</ta>
            <ta e="T39" id="Seg_2431" s="T38">np:Th</ta>
            <ta e="T40" id="Seg_2432" s="T39">pro.h:Th</ta>
            <ta e="T41" id="Seg_2433" s="T40">0.2.h:A</ta>
            <ta e="T43" id="Seg_2434" s="T41">pp:G</ta>
            <ta e="T46" id="Seg_2435" s="T45">pro.h:A</ta>
            <ta e="T48" id="Seg_2436" s="T47">np:L</ta>
            <ta e="T49" id="Seg_2437" s="T48">np.h:R</ta>
            <ta e="T50" id="Seg_2438" s="T49">0.2.h:A</ta>
            <ta e="T51" id="Seg_2439" s="T50">np.h:A</ta>
            <ta e="T53" id="Seg_2440" s="T52">0.2.h:A</ta>
            <ta e="T54" id="Seg_2441" s="T53">pro.h:R</ta>
            <ta e="T57" id="Seg_2442" s="T56">np:Poss</ta>
            <ta e="T58" id="Seg_2443" s="T57">np:Th</ta>
            <ta e="T59" id="Seg_2444" s="T58">pro.h:A</ta>
            <ta e="T60" id="Seg_2445" s="T59">n:Time</ta>
            <ta e="T65" id="Seg_2446" s="T63">pp:Com</ta>
            <ta e="T66" id="Seg_2447" s="T65">pro.h:A</ta>
            <ta e="T68" id="Seg_2448" s="T67">pro.h:R</ta>
            <ta e="T72" id="Seg_2449" s="T71">np:Th</ta>
            <ta e="T73" id="Seg_2450" s="T72">0.3:Poss</ta>
            <ta e="T75" id="Seg_2451" s="T74">adv:L</ta>
            <ta e="T76" id="Seg_2452" s="T75">0.2.h:A</ta>
            <ta e="T78" id="Seg_2453" s="T77">np.h:E</ta>
            <ta e="T85" id="Seg_2454" s="T84">np.h:A</ta>
            <ta e="T86" id="Seg_2455" s="T85">adv:L</ta>
            <ta e="T87" id="Seg_2456" s="T86">0.3.h:A</ta>
            <ta e="T88" id="Seg_2457" s="T87">np:Th</ta>
            <ta e="T92" id="Seg_2458" s="T91">np:Th</ta>
            <ta e="T93" id="Seg_2459" s="T92">np.h:A</ta>
            <ta e="T96" id="Seg_2460" s="T95">np:L</ta>
            <ta e="T101" id="Seg_2461" s="T99">pp:Com</ta>
            <ta e="T103" id="Seg_2462" s="T102">0.3.h:A</ta>
            <ta e="T104" id="Seg_2463" s="T103">pro.h:E</ta>
            <ta e="T109" id="Seg_2464" s="T107">pp:Com</ta>
            <ta e="T110" id="Seg_2465" s="T109">pro.h:A</ta>
            <ta e="T112" id="Seg_2466" s="T111">n:Time</ta>
            <ta e="T114" id="Seg_2467" s="T113">pro.h:R</ta>
            <ta e="T116" id="Seg_2468" s="T114">0.1.h:A</ta>
            <ta e="T117" id="Seg_2469" s="T116">pro:P</ta>
            <ta e="T120" id="Seg_2470" s="T118">0.3:Th</ta>
            <ta e="T122" id="Seg_2471" s="T121">np:L</ta>
            <ta e="T123" id="Seg_2472" s="T122">np:Th</ta>
            <ta e="T126" id="Seg_2473" s="T125">np:Th</ta>
            <ta e="T128" id="Seg_2474" s="T127">0.3.h:A</ta>
            <ta e="T129" id="Seg_2475" s="T128">0.2.h:A</ta>
            <ta e="T130" id="Seg_2476" s="T129">pro:Th</ta>
            <ta e="T131" id="Seg_2477" s="T130">0.2.h:A</ta>
            <ta e="T132" id="Seg_2478" s="T131">0.1.h:A</ta>
            <ta e="T133" id="Seg_2479" s="T132">pro.h:R</ta>
            <ta e="T135" id="Seg_2480" s="T133">0.1.h:A</ta>
            <ta e="T140" id="Seg_2481" s="T139">np:P</ta>
            <ta e="T144" id="Seg_2482" s="T143">np.h:A</ta>
            <ta e="T145" id="Seg_2483" s="T144">np:Th</ta>
            <ta e="T148" id="Seg_2484" s="T147">np:Th</ta>
            <ta e="T151" id="Seg_2485" s="T150">pro:Th</ta>
            <ta e="T153" id="Seg_2486" s="T152">0.3.h:A</ta>
            <ta e="T154" id="Seg_2487" s="T153">pro.h:B</ta>
            <ta e="T156" id="Seg_2488" s="T155">pro:Th</ta>
            <ta e="T158" id="Seg_2489" s="T157">pro.h:Poss</ta>
            <ta e="T160" id="Seg_2490" s="T159">pro:P</ta>
            <ta e="T161" id="Seg_2491" s="T160">pro.h:B</ta>
            <ta e="T164" id="Seg_2492" s="T163">0.3.h:A</ta>
            <ta e="T165" id="Seg_2493" s="T164">pro:P</ta>
            <ta e="T166" id="Seg_2494" s="T165">0.2.h:A</ta>
            <ta e="T174" id="Seg_2495" s="T173">0.3.h:A</ta>
            <ta e="T175" id="Seg_2496" s="T174">np:Th</ta>
            <ta e="T179" id="Seg_2497" s="T178">np:Th</ta>
            <ta e="T183" id="Seg_2498" s="T182">pro.h:Poss</ta>
            <ta e="T184" id="Seg_2499" s="T183">np:L</ta>
            <ta e="T185" id="Seg_2500" s="T184">pro:P</ta>
            <ta e="T187" id="Seg_2501" s="T186">pro.h:B</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T3" id="Seg_2502" s="T0">s:temp</ta>
            <ta e="T4" id="Seg_2503" s="T3">np.h:S</ta>
            <ta e="T5" id="Seg_2504" s="T4">v:pred</ta>
            <ta e="T9" id="Seg_2505" s="T8">s:adv</ta>
            <ta e="T11" id="Seg_2506" s="T10">0.2.h:S v:pred</ta>
            <ta e="T12" id="Seg_2507" s="T11">0.2.h:S v:pred</ta>
            <ta e="T14" id="Seg_2508" s="T12">s:comp</ta>
            <ta e="T15" id="Seg_2509" s="T14">pro.h:S</ta>
            <ta e="T17" id="Seg_2510" s="T16">v:pred</ta>
            <ta e="T20" id="Seg_2511" s="T19">0.2.h:S v:pred</ta>
            <ta e="T22" id="Seg_2512" s="T21">np.h:S</ta>
            <ta e="T23" id="Seg_2513" s="T22">v:pred</ta>
            <ta e="T27" id="Seg_2514" s="T24">s:temp</ta>
            <ta e="T28" id="Seg_2515" s="T27">s:temp</ta>
            <ta e="T32" id="Seg_2516" s="T31">0.2.h:S v:pred</ta>
            <ta e="T39" id="Seg_2517" s="T35">s:rel</ta>
            <ta e="T40" id="Seg_2518" s="T39">pro.h:O</ta>
            <ta e="T41" id="Seg_2519" s="T40">0.2.h:S v:pred</ta>
            <ta e="T46" id="Seg_2520" s="T43">s:rel</ta>
            <ta e="T50" id="Seg_2521" s="T49">0.2.h:S v:pred</ta>
            <ta e="T51" id="Seg_2522" s="T50">np.h:S</ta>
            <ta e="T52" id="Seg_2523" s="T51">v:pred</ta>
            <ta e="T53" id="Seg_2524" s="T52">0.2.h:S v:pred</ta>
            <ta e="T55" id="Seg_2525" s="T54">pro:pred</ta>
            <ta e="T58" id="Seg_2526" s="T57">np:S</ta>
            <ta e="T65" id="Seg_2527" s="T58">s:rel</ta>
            <ta e="T66" id="Seg_2528" s="T65">pro.h:S</ta>
            <ta e="T67" id="Seg_2529" s="T66">v:pred</ta>
            <ta e="T72" id="Seg_2530" s="T71">np:O</ta>
            <ta e="T76" id="Seg_2531" s="T75">0.2.h:S v:pred</ta>
            <ta e="T78" id="Seg_2532" s="T77">np.h:S</ta>
            <ta e="T80" id="Seg_2533" s="T78">s:temp</ta>
            <ta e="T81" id="Seg_2534" s="T80">v:pred</ta>
            <ta e="T85" id="Seg_2535" s="T81">s:adv</ta>
            <ta e="T87" id="Seg_2536" s="T86">0.3.h:S v:pred</ta>
            <ta e="T88" id="Seg_2537" s="T87">np:O</ta>
            <ta e="T92" id="Seg_2538" s="T88">s:adv</ta>
            <ta e="T93" id="Seg_2539" s="T92">np.h:S</ta>
            <ta e="T94" id="Seg_2540" s="T93">v:pred</ta>
            <ta e="T103" id="Seg_2541" s="T102">0.3.h:S v:pred</ta>
            <ta e="T104" id="Seg_2542" s="T103">pro.h:S</ta>
            <ta e="T106" id="Seg_2543" s="T105">v:pred</ta>
            <ta e="T109" id="Seg_2544" s="T106">s:comp</ta>
            <ta e="T110" id="Seg_2545" s="T109">pro.h:S</ta>
            <ta e="T113" id="Seg_2546" s="T112">v:pred</ta>
            <ta e="T116" id="Seg_2547" s="T114">0.1.h:S v:pred</ta>
            <ta e="T117" id="Seg_2548" s="T116">pro:O</ta>
            <ta e="T122" id="Seg_2549" s="T117">s:temp</ta>
            <ta e="T124" id="Seg_2550" s="T122">s:temp</ta>
            <ta e="T126" id="Seg_2551" s="T125">np:O</ta>
            <ta e="T128" id="Seg_2552" s="T127">0.3.h:S v:pred</ta>
            <ta e="T129" id="Seg_2553" s="T128">0.2.h:S v:pred</ta>
            <ta e="T130" id="Seg_2554" s="T129">np:O</ta>
            <ta e="T131" id="Seg_2555" s="T130">0.2.h:S v:pred</ta>
            <ta e="T132" id="Seg_2556" s="T131">0.1.h:S v:pred</ta>
            <ta e="T135" id="Seg_2557" s="T133">0.1.h:S v:pred</ta>
            <ta e="T140" id="Seg_2558" s="T139">np:O</ta>
            <ta e="T144" id="Seg_2559" s="T140">s:temp</ta>
            <ta e="T146" id="Seg_2560" s="T144">s:temp</ta>
            <ta e="T150" id="Seg_2561" s="T146">s:temp</ta>
            <ta e="T151" id="Seg_2562" s="T150">pro:O</ta>
            <ta e="T153" id="Seg_2563" s="T152">0.3.h:S v:pred</ta>
            <ta e="T155" id="Seg_2564" s="T154">s:adv</ta>
            <ta e="T156" id="Seg_2565" s="T155">pro:S</ta>
            <ta e="T157" id="Seg_2566" s="T156">cop</ta>
            <ta e="T159" id="Seg_2567" s="T158">n:pred</ta>
            <ta e="T164" id="Seg_2568" s="T159">s:rel</ta>
            <ta e="T165" id="Seg_2569" s="T164">pro:O</ta>
            <ta e="T166" id="Seg_2570" s="T165">0.2.h:S v:pred</ta>
            <ta e="T168" id="Seg_2571" s="T166">s:rel</ta>
            <ta e="T174" id="Seg_2572" s="T173">0.3.h:S v:pred</ta>
            <ta e="T177" id="Seg_2573" s="T174">s:temp</ta>
            <ta e="T179" id="Seg_2574" s="T178">np:S</ta>
            <ta e="T180" id="Seg_2575" s="T179">cop</ta>
            <ta e="T182" id="Seg_2576" s="T181">n:pred</ta>
            <ta e="T188" id="Seg_2577" s="T184">s:rel</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T2" id="Seg_2578" s="T1">accs-gen</ta>
            <ta e="T4" id="Seg_2579" s="T3">accs-gen</ta>
            <ta e="T5" id="Seg_2580" s="T4">quot-sp</ta>
            <ta e="T6" id="Seg_2581" s="T5">accs-gen</ta>
            <ta e="T8" id="Seg_2582" s="T7">accs-gen</ta>
            <ta e="T11" id="Seg_2583" s="T10">0.accs-aggr-Q</ta>
            <ta e="T12" id="Seg_2584" s="T11">0.giv-active-Q</ta>
            <ta e="T13" id="Seg_2585" s="T12">giv-active-Q</ta>
            <ta e="T15" id="Seg_2586" s="T14">giv-active</ta>
            <ta e="T17" id="Seg_2587" s="T16">quot-sp</ta>
            <ta e="T18" id="Seg_2588" s="T17">giv-active</ta>
            <ta e="T20" id="Seg_2589" s="T19">0.giv-active-Q</ta>
            <ta e="T22" id="Seg_2590" s="T21">giv-active</ta>
            <ta e="T23" id="Seg_2591" s="T22">quot-sp</ta>
            <ta e="T25" id="Seg_2592" s="T24">giv-inactive-Q</ta>
            <ta e="T26" id="Seg_2593" s="T25">accs-sit-Q</ta>
            <ta e="T31" id="Seg_2594" s="T30">new-Q</ta>
            <ta e="T32" id="Seg_2595" s="T31">0.giv-active-Q</ta>
            <ta e="T34" id="Seg_2596" s="T33">new-Q</ta>
            <ta e="T39" id="Seg_2597" s="T38">new-Q</ta>
            <ta e="T40" id="Seg_2598" s="T39">giv-active-Q</ta>
            <ta e="T41" id="Seg_2599" s="T40">0.giv-active-Q</ta>
            <ta e="T42" id="Seg_2600" s="T41">accs-inf-Q</ta>
            <ta e="T46" id="Seg_2601" s="T45">giv-active-Q</ta>
            <ta e="T48" id="Seg_2602" s="T47">giv-active-Q</ta>
            <ta e="T49" id="Seg_2603" s="T48">accs-inf-Q</ta>
            <ta e="T50" id="Seg_2604" s="T49">0.giv-active-Q</ta>
            <ta e="T51" id="Seg_2605" s="T50">giv-inactive-Q-Q</ta>
            <ta e="T52" id="Seg_2606" s="T51">quot-sp-Q</ta>
            <ta e="T53" id="Seg_2607" s="T52">0.giv-active-Q</ta>
            <ta e="T54" id="Seg_2608" s="T53">giv-active-Q-Q</ta>
            <ta e="T57" id="Seg_2609" s="T56">giv-active-Q-Q-Q</ta>
            <ta e="T58" id="Seg_2610" s="T57">accs-inf-Q-Q-Q</ta>
            <ta e="T59" id="Seg_2611" s="T58">giv-active-Q-Q-Q</ta>
            <ta e="T60" id="Seg_2612" s="T59">giv-inactive-Q-Q-Q</ta>
            <ta e="T64" id="Seg_2613" s="T63">accs-inf-Q-Q-Q</ta>
            <ta e="T66" id="Seg_2614" s="T65">giv-active-Q</ta>
            <ta e="T68" id="Seg_2615" s="T67">giv-active-Q</ta>
            <ta e="T72" id="Seg_2616" s="T71">new-Q</ta>
            <ta e="T73" id="Seg_2617" s="T72">accs-inf-Q</ta>
            <ta e="T76" id="Seg_2618" s="T75">0.giv-inactive-Q</ta>
            <ta e="T78" id="Seg_2619" s="T77">giv-active</ta>
            <ta e="T85" id="Seg_2620" s="T84">giv-inactive</ta>
            <ta e="T87" id="Seg_2621" s="T86">0.giv-active</ta>
            <ta e="T88" id="Seg_2622" s="T87">giv-inactive</ta>
            <ta e="T92" id="Seg_2623" s="T91">accs-inf</ta>
            <ta e="T93" id="Seg_2624" s="T92">giv-inactive</ta>
            <ta e="T95" id="Seg_2625" s="T94">accs-sit</ta>
            <ta e="T100" id="Seg_2626" s="T99">giv-inactive</ta>
            <ta e="T103" id="Seg_2627" s="T102">0.giv-active quot-sp</ta>
            <ta e="T104" id="Seg_2628" s="T103">giv-active-Q</ta>
            <ta e="T108" id="Seg_2629" s="T107">giv-active-Q</ta>
            <ta e="T110" id="Seg_2630" s="T109">giv-active-Q</ta>
            <ta e="T111" id="Seg_2631" s="T110">accs-inf-Q</ta>
            <ta e="T113" id="Seg_2632" s="T112">quot-sp-Q</ta>
            <ta e="T114" id="Seg_2633" s="T113">giv-active-Q</ta>
            <ta e="T116" id="Seg_2634" s="T114">0.giv-active-Q</ta>
            <ta e="T117" id="Seg_2635" s="T116">accs-sit-Q</ta>
            <ta e="T122" id="Seg_2636" s="T121">accs-gen-Q</ta>
            <ta e="T123" id="Seg_2637" s="T122">new</ta>
            <ta e="T126" id="Seg_2638" s="T125">new</ta>
            <ta e="T128" id="Seg_2639" s="T127">0.giv-active 0.quot-sp</ta>
            <ta e="T129" id="Seg_2640" s="T128">0.giv-inactive-Q</ta>
            <ta e="T130" id="Seg_2641" s="T129">giv-active-Q</ta>
            <ta e="T131" id="Seg_2642" s="T130">0.giv-active-Q</ta>
            <ta e="T132" id="Seg_2643" s="T131">0.giv-active-Q</ta>
            <ta e="T133" id="Seg_2644" s="T132">giv-active-Q</ta>
            <ta e="T135" id="Seg_2645" s="T133">0.giv-active-Q</ta>
            <ta e="T140" id="Seg_2646" s="T139">new-Q</ta>
            <ta e="T144" id="Seg_2647" s="T143">giv-inactive-Q</ta>
            <ta e="T145" id="Seg_2648" s="T144">new</ta>
            <ta e="T148" id="Seg_2649" s="T147">giv-inactive</ta>
            <ta e="T151" id="Seg_2650" s="T150">giv-active</ta>
            <ta e="T153" id="Seg_2651" s="T152">0.giv-active</ta>
            <ta e="T154" id="Seg_2652" s="T153">giv-inactive</ta>
            <ta e="T155" id="Seg_2653" s="T154">quot-sp</ta>
            <ta e="T156" id="Seg_2654" s="T155">giv-active-Q</ta>
            <ta e="T158" id="Seg_2655" s="T157">giv-active-Q</ta>
            <ta e="T159" id="Seg_2656" s="T158">accs-inf-Q</ta>
            <ta e="T160" id="Seg_2657" s="T159">giv-active-Q</ta>
            <ta e="T161" id="Seg_2658" s="T160">giv-active-Q</ta>
            <ta e="T165" id="Seg_2659" s="T164">giv-active-Q</ta>
            <ta e="T166" id="Seg_2660" s="T165">0.giv-active-Q</ta>
            <ta e="T167" id="Seg_2661" s="T166">giv-active-Q</ta>
            <ta e="T172" id="Seg_2662" s="T171">giv-inactive</ta>
            <ta e="T174" id="Seg_2663" s="T173">0.giv-active</ta>
            <ta e="T175" id="Seg_2664" s="T174">giv-inactive</ta>
            <ta e="T179" id="Seg_2665" s="T178">giv-active-Q</ta>
            <ta e="T183" id="Seg_2666" s="T182">giv-active-Q</ta>
            <ta e="T184" id="Seg_2667" s="T183">accs-inf-Q</ta>
            <ta e="T185" id="Seg_2668" s="T184">giv-active-Q</ta>
            <ta e="T187" id="Seg_2669" s="T186">giv-inactive-Q</ta>
         </annotation>
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T1" id="Seg_2670" s="T0">RUS:cult</ta>
            <ta e="T4" id="Seg_2671" s="T3">RUS:cult</ta>
            <ta e="T6" id="Seg_2672" s="T5">RUS:cult</ta>
            <ta e="T8" id="Seg_2673" s="T7">RUS:cult</ta>
            <ta e="T13" id="Seg_2674" s="T12">RUS:cult</ta>
            <ta e="T18" id="Seg_2675" s="T17">RUS:cult</ta>
            <ta e="T22" id="Seg_2676" s="T21">RUS:cult</ta>
            <ta e="T26" id="Seg_2677" s="T25">RUS:cult</ta>
            <ta e="T49" id="Seg_2678" s="T48">RUS:cult</ta>
            <ta e="T51" id="Seg_2679" s="T50">RUS:cult</ta>
            <ta e="T60" id="Seg_2680" s="T59">RUS:cult</ta>
            <ta e="T85" id="Seg_2681" s="T84">RUS:cult</ta>
            <ta e="T88" id="Seg_2682" s="T87">RUS:cult</ta>
            <ta e="T93" id="Seg_2683" s="T92">RUS:cult</ta>
            <ta e="T95" id="Seg_2684" s="T94">RUS:cult</ta>
            <ta e="T123" id="Seg_2685" s="T122">RUS:cult</ta>
            <ta e="T138" id="Seg_2686" s="T137">RUS:cult</ta>
            <ta e="T145" id="Seg_2687" s="T144">RUS:cult</ta>
            <ta e="T172" id="Seg_2688" s="T171">RUS:cult</ta>
            <ta e="T179" id="Seg_2689" s="T178">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T6" id="Seg_2690" s="T5">Vsub</ta>
            <ta e="T8" id="Seg_2691" s="T7">Vsub fortition</ta>
            <ta e="T26" id="Seg_2692" s="T25">Vsub</ta>
            <ta e="T49" id="Seg_2693" s="T48">fortition Vsub Vsub</ta>
            <ta e="T88" id="Seg_2694" s="T87">fortition</ta>
            <ta e="T95" id="Seg_2695" s="T94">inVins</ta>
            <ta e="T123" id="Seg_2696" s="T122">Csub Vsub</ta>
            <ta e="T145" id="Seg_2697" s="T144">fortition medVins</ta>
            <ta e="T172" id="Seg_2698" s="T171">Csub Vsub</ta>
            <ta e="T179" id="Seg_2699" s="T178">Csub Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T1" id="Seg_2700" s="T0">dir:bare</ta>
            <ta e="T4" id="Seg_2701" s="T3">dir:bare</ta>
            <ta e="T6" id="Seg_2702" s="T5">dir:infl</ta>
            <ta e="T8" id="Seg_2703" s="T7">dir:infl</ta>
            <ta e="T13" id="Seg_2704" s="T12">dir:infl</ta>
            <ta e="T18" id="Seg_2705" s="T17">dir:infl</ta>
            <ta e="T22" id="Seg_2706" s="T21">dir:bare</ta>
            <ta e="T26" id="Seg_2707" s="T25">dir:infl</ta>
            <ta e="T49" id="Seg_2708" s="T48">dir:infl</ta>
            <ta e="T51" id="Seg_2709" s="T50">dir:bare</ta>
            <ta e="T60" id="Seg_2710" s="T59">dir:infl</ta>
            <ta e="T85" id="Seg_2711" s="T84">dir:bare</ta>
            <ta e="T88" id="Seg_2712" s="T87">dir:infl</ta>
            <ta e="T93" id="Seg_2713" s="T92">dir:bare</ta>
            <ta e="T95" id="Seg_2714" s="T94">dir:bare</ta>
            <ta e="T123" id="Seg_2715" s="T122">dir:infl</ta>
            <ta e="T138" id="Seg_2716" s="T137">dir:bare</ta>
            <ta e="T145" id="Seg_2717" s="T144">dir:infl</ta>
            <ta e="T172" id="Seg_2718" s="T171">dir:bare</ta>
            <ta e="T179" id="Seg_2719" s="T178">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T9" id="Seg_2720" s="T0">When Passover was coming, Jesus asked Peter and John:</ta>
            <ta e="T14" id="Seg_2721" s="T9">"Go, prepare what we will eat at Passover."</ta>
            <ta e="T18" id="Seg_2722" s="T14">But they asked Jesus: </ta>
            <ta e="T21" id="Seg_2723" s="T18">"Where do you make us prepare [it]?"</ta>
            <ta e="T23" id="Seg_2724" s="T21">Jesus said: </ta>
            <ta e="T24" id="Seg_2725" s="T23">"There.</ta>
            <ta e="T39" id="Seg_2726" s="T24">When you come into town, you'll go in and meet at a place a man carrying a jar with water.</ta>
            <ta e="T43" id="Seg_2727" s="T39">Follow that one to his house.</ta>
            <ta e="T50" id="Seg_2728" s="T43">Where he will go in, in that house speak to the man of the house.</ta>
            <ta e="T65" id="Seg_2729" s="T50">Say: "The teacher asks you: "Where is the room in the house, where I myself together with my disciples will eat at Passover?"</ta>
            <ta e="T72" id="Seg_2730" s="T65">He will show a very spacious room.</ta>
            <ta e="T74" id="Seg_2731" s="T72">Where the floor is covered. </ta>
            <ta e="T76" id="Seg_2732" s="T74">Prepare [it] there."</ta>
            <ta e="T85" id="Seg_2733" s="T76">The discpiles went there and found everything like Jesus had said.</ta>
            <ta e="T88" id="Seg_2734" s="T85">There they prepared Passover.</ta>
            <ta e="T103" id="Seg_2735" s="T88">When the time of the meeting had come, Jesus sat down at the table together with his twelve disciples and said:</ta>
            <ta e="T114" id="Seg_2736" s="T103">"I very much want to eat with you, I tell you before my pains:</ta>
            <ta e="T122" id="Seg_2737" s="T114">I will not eat it, until it is with the Holy Father."</ta>
            <ta e="T128" id="Seg_2738" s="T122">He took a cup and spoke the good words: </ta>
            <ta e="T131" id="Seg_2739" s="T128">"Take this one, share it.</ta>
            <ta e="T144" id="Seg_2740" s="T131">I say to you, I will not drink any wine, until the Holy Father has not come."</ta>
            <ta e="T155" id="Seg_2741" s="T144">Having taken bread and having spoken a good word, he divided it for them, saying:</ta>
            <ta e="T164" id="Seg_2742" s="T155">"This is my body, which will be given to death for you.</ta>
            <ta e="T169" id="Seg_2743" s="T164">Eat it as a remembrance of my name."</ta>
            <ta e="T177" id="Seg_2744" s="T169">Like that he also spoke about the cup, when the meeting was over.</ta>
            <ta e="T188" id="Seg_2745" s="T177">"This cup is the New Testament in my blood (?), which will be poured out for you."</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T9" id="Seg_2746" s="T0">Als das Pessachfest kam, fragt Jesus Petrus und Johannes:</ta>
            <ta e="T14" id="Seg_2747" s="T9">"Geht, bereitet vor, was wir an Pessach essen werden."</ta>
            <ta e="T18" id="Seg_2748" s="T14">Sie aber fragten Jesus: </ta>
            <ta e="T21" id="Seg_2749" s="T18">"Wo lässt du uns das vorbereiten?"</ta>
            <ta e="T23" id="Seg_2750" s="T21">Jesus sagte: </ta>
            <ta e="T24" id="Seg_2751" s="T23">"Dort.</ta>
            <ta e="T39" id="Seg_2752" s="T24">Wenn ihr in die Stadt hineinkommt, geht ihr hinein und trefft an einem Ort einen Menschen, der ein Gefäß mit Wasser trägt.</ta>
            <ta e="T43" id="Seg_2753" s="T39">Demjenigen folgt bis nach Hause.</ta>
            <ta e="T50" id="Seg_2754" s="T43">Wo er hineingeht, in jenem Haus sprecht zum Hausherrn.</ta>
            <ta e="T65" id="Seg_2755" s="T50">"Der Lehrer fragt dich", sagt, ""wo ist das Zimmer des Hauses, in dem ich selbst mit meinen Jüngern an Pessach essen werde?"</ta>
            <ta e="T72" id="Seg_2756" s="T65">Er wird euch ein sehr geräumiges Zimmer zeigen.</ta>
            <ta e="T74" id="Seg_2757" s="T72">Wo der Boden bedeckt ist.</ta>
            <ta e="T76" id="Seg_2758" s="T74">Dort bereitet ihr es vor."</ta>
            <ta e="T85" id="Seg_2759" s="T76">Die Jünger gingen dorthin und fanden es, wie es Jesus gesagt hatte.</ta>
            <ta e="T88" id="Seg_2760" s="T85">Dort bereiteten sie Pessach vor.</ta>
            <ta e="T103" id="Seg_2761" s="T88">Als die Zeit des Treffens gekommen war, setzte sich Jesus mit seinen zwölf Jüngern an den Tisch und sagte:</ta>
            <ta e="T114" id="Seg_2762" s="T103">"Ich möchte sehr mit euch essen, ich sage euch vor meinen Qualen:</ta>
            <ta e="T122" id="Seg_2763" s="T114">Ich werde es nicht essen, solange es nicht beim heiligen Vater ist."</ta>
            <ta e="T128" id="Seg_2764" s="T122">Er nahm einen Kelch und sprach die guten Worte: </ta>
            <ta e="T131" id="Seg_2765" s="T128">"Nehmt diesen, teilt ihn unter euch.</ta>
            <ta e="T144" id="Seg_2766" s="T131">Ich sage euch, ich trinke keinen Wein, solange der heilige Vater nicht gekommen ist."</ta>
            <ta e="T155" id="Seg_2767" s="T144">Nachdem er Brot genommen hatte und ein gutes Wort gesprochen hatte, teilte er dieses unter ihnen und sagte: </ta>
            <ta e="T164" id="Seg_2768" s="T155">"Das ist mein Leib, der für euch dem Tode gegeben wird.</ta>
            <ta e="T169" id="Seg_2769" s="T164">Esst dies als Erinnerung an meinen Namen."</ta>
            <ta e="T177" id="Seg_2770" s="T169">So sprach er auch über den Kelch, als das Treffen vorüber war.</ta>
            <ta e="T188" id="Seg_2771" s="T177">"Dieser Kelch ist das neue Testament in meinem Blut (?), das für euch vergossen wird."</ta>
         </annotation>
         <annotation name="fr" tierref="fr" />
         <annotation name="ltr" tierref="ltr">
            <ta e="T9" id="Seg_2772" s="T0">Когда Пасха праздник наступил, Иисус спросил у Петра и Ивана, говоря: </ta>
            <ta e="T14" id="Seg_2773" s="T9">"Идите, приготовьте в Пасху что будем есть".</ta>
            <ta e="T18" id="Seg_2774" s="T14">Они, значит, спросили у Иисуса: </ta>
            <ta e="T21" id="Seg_2775" s="T18">"Куда готовить ты просишь?"</ta>
            <ta e="T23" id="Seg_2776" s="T21">Иисус ответил: </ta>
            <ta e="T24" id="Seg_2777" s="T23">"Туда.</ta>
            <ta e="T39" id="Seg_2778" s="T24">Вы в город как зайдёте, зайдя, на земле встретитесь с одним человеком, несущего с водой посуду.</ta>
            <ta e="T43" id="Seg_2779" s="T39">За ним идите до дома.</ta>
            <ta e="T50" id="Seg_2780" s="T43">Куда он будет заходить, того дома хозяйну скажите.</ta>
            <ta e="T65" id="Seg_2781" s="T50">"Учитель спрашивает, – скажите, – у тебя, где дома часть (комната), я в Пасху где бы покушал со своими учениками?"</ta>
            <ta e="T72" id="Seg_2782" s="T65">Он покажет вам просторную комнату.</ta>
            <ta e="T74" id="Seg_2783" s="T72">Полы где покрытые.</ta>
            <ta e="T76" id="Seg_2784" s="T74">Там и подготовьте."</ta>
            <ta e="T85" id="Seg_2785" s="T76">Учеников туда дойдя и нашли, как сказал Иисус.</ta>
            <ta e="T88" id="Seg_2786" s="T85">Там и приготовили пасху.</ta>
            <ta e="T103" id="Seg_2787" s="T88">Когда наступил день встречи, Иисус сел перед столом со своими двенадцатью учениками и там произнёс:</ta>
            <ta e="T114" id="Seg_2788" s="T103">"Я сильно захотел покушать с вами, я перед мучениями говорю вам: </ta>
            <ta e="T122" id="Seg_2789" s="T114">Не буду есть это, ничего пока не будет с божьим священником."</ta>
            <ta e="T128" id="Seg_2790" s="T122">Чашу взяв, доброе напуствие произнеся, сказал: </ta>
            <ta e="T131" id="Seg_2791" s="T128">"Возьмите это, поделитесь.</ta>
            <ta e="T144" id="Seg_2792" s="T131">Говорю вам, не буду пить винограда ягоды вино, пока не придёт божий священник".</ta>
            <ta e="T155" id="Seg_2793" s="T144">Хлеб взяв, напуствие доброе произнеся, его надломив, разделил им, говоря: </ta>
            <ta e="T164" id="Seg_2794" s="T155">"Это и есть моё тело, что ради вас на смерть отдаётся.</ta>
            <ta e="T169" id="Seg_2795" s="T164">Это съешьте, меня вспоминающего имени ради".</ta>
            <ta e="T177" id="Seg_2796" s="T169">Таким образом про чашу сказал встреча когда прошла.</ta>
            <ta e="T188" id="Seg_2797" s="T177">"Это чаша будет новой беседой в моей крови (?), что проливается ради вас."</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
