<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID709673A6-8C4D-6C2C-60BD-EF1D36FF6F96">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>PoNA_1994_JesusAndPilate_transl</transcription-name>
         <referenced-file url="PoNA_19940110_JesusAndPilate_transl.wav" />
         <referenced-file url="PoNA_19940110_JesusAndPilate_transl.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\transl\PoNA_19940110_JesusAndPilate_transl\PoNA_19940110_JesusAndPilate_transl.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">100</ud-information>
            <ud-information attribute-name="# HIAT:w">80</ud-information>
            <ud-information attribute-name="# e">79</ud-information>
            <ud-information attribute-name="# HIAT:u">10</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PoNA">
            <abbreviation>PoNA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="1.481999999999971" type="appl" />
         <tli id="T2" time="3.4666666666666663" />
         <tli id="T3" time="6.259999999999999" />
         <tli id="T4" />
         <tli id="T5" />
         <tli id="T6" time="7.646666666666667" />
         <tli id="T7" time="8.275999999999954" type="appl" />
         <tli id="T8" time="9.168000000000006" type="appl" />
         <tli id="T9" time="10.058999999999969" type="appl" />
         <tli id="T10" time="10.950000000000045" type="appl" />
         <tli id="T11" time="11.841999999999985" type="appl" />
         <tli id="T12" time="12.732999999999947" type="appl" />
         <tli id="T13" time="13.625" type="appl" />
         <tli id="T14" time="14.515999999999963" type="appl" />
         <tli id="T15" time="15.407000000000039" type="appl" />
         <tli id="T16" time="16.298999999999978" type="appl" />
         <tli id="T17" time="17.190000000000055" type="appl" />
         <tli id="T18" time="18.081000000000017" type="appl" />
         <tli id="T19" time="18.972999999999956" type="appl" />
         <tli id="T20" time="19.864000000000033" type="appl" />
         <tli id="T21" time="20.83400000000006" type="appl" />
         <tli id="T22" time="21.802999999999997" type="appl" />
         <tli id="T23" time="22.773000000000025" type="appl" />
         <tli id="T24" time="23.741999999999962" type="appl" />
         <tli id="T25" time="24.711000000000013" type="appl" />
         <tli id="T26" time="25.68100000000004" type="appl" />
         <tli id="T27" time="26.58000000000004" type="appl" />
         <tli id="T28" time="27.479000000000042" type="appl" />
         <tli id="T29" time="28.378000000000043" type="appl" />
         <tli id="T30" time="29.277000000000044" type="appl" />
         <tli id="T31" time="30.176000000000045" type="appl" />
         <tli id="T32" time="31.075000000000045" type="appl" />
         <tli id="T33" time="31.974000000000046" type="appl" />
         <tli id="T34" time="32.87300000000005" type="appl" />
         <tli id="T35" time="33.77200000000005" type="appl" />
         <tli id="T36" time="34.682000000000016" type="appl" />
         <tli id="T37" time="35.59100000000001" type="appl" />
         <tli id="T38" time="36.500999999999976" type="appl" />
         <tli id="T39" time="37.41100000000006" type="appl" />
         <tli id="T40" time="38.32000000000005" type="appl" />
         <tli id="T41" time="39.23000000000002" type="appl" />
         <tli id="T42" time="40.13900000000001" type="appl" />
         <tli id="T43" time="41.04899999999998" type="appl" />
         <tli id="T44" time="41.928999999999974" type="appl" />
         <tli id="T45" time="42.80799999999999" type="appl" />
         <tli id="T46" time="43.68799999999999" type="appl" />
         <tli id="T47" time="44.56700000000001" type="appl" />
         <tli id="T48" time="45.447" type="appl" />
         <tli id="T49" time="46.32600000000002" type="appl" />
         <tli id="T50" time="47.20600000000002" type="appl" />
         <tli id="T51" time="48.023000000000025" type="appl" />
         <tli id="T52" time="48.84100000000001" type="appl" />
         <tli id="T53" time="49.658000000000015" type="appl" />
         <tli id="T54" time="50.47500000000002" type="appl" />
         <tli id="T55" time="51.293000000000006" type="appl" />
         <tli id="T56" time="52.110000000000014" type="appl" />
         <tli id="T57" time="52.928" type="appl" />
         <tli id="T58" time="53.745000000000005" type="appl" />
         <tli id="T59" time="54.56200000000001" type="appl" />
         <tli id="T60" time="55.379999999999995" type="appl" />
         <tli id="T61" time="56.197" type="appl" />
         <tli id="T62" time="57.014999999999986" type="appl" />
         <tli id="T63" time="57.831999999999994" type="appl" />
         <tli id="T64" time="58.649" type="appl" />
         <tli id="T65" time="59.466999999999985" type="appl" />
         <tli id="T66" time="60.28399999999999" type="appl" />
         <tli id="T67" time="61.17399999999998" type="appl" />
         <tli id="T68" time="62.063999999999965" type="appl" />
         <tli id="T69" time="62.95399999999995" type="appl" />
         <tli id="T70" time="63.84400000000005" type="appl" />
         <tli id="T71" time="64.73400000000004" type="appl" />
         <tli id="T72" time="65.62399739583333" />
         <tli id="T73" time="70.2" />
         <tli id="T74" />
         <tli id="T75" />
         <tli id="T76" />
         <tli id="T77" />
         <tli id="T78" />
         <tli id="T79" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PoNA"
                      type="t">
         <timeline-fork end="T35" start="T34">
            <tli id="T34.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T79" id="Seg_0" n="sc" s="T0">
               <ts e="T2" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Nöŋü͡ö</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">ös</ts>
                  <nts id="Seg_8" n="HIAT:ip">:</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T6" id="Seg_11" n="HIAT:u" s="T2">
                  <ts e="T3" id="Seg_13" n="HIAT:w" s="T2">Isuːs</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">Pʼilaːt</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">innitiger</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">turuːta</ts>
                  <nts id="Seg_23" n="HIAT:ip">.</nts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_26" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_28" n="HIAT:w" s="T6">Kaččaga</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_31" n="HIAT:w" s="T7">harsi͡ersa</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_34" n="HIAT:w" s="T8">bu͡olaːtɨn</ts>
                  <nts id="Seg_35" n="HIAT:ip">,</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_38" n="HIAT:w" s="T9">barɨ</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">bastɨŋ</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">agabɨttar</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">onno</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">kɨrdʼagas</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_53" n="HIAT:w" s="T14">hübehitter</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_56" n="HIAT:w" s="T15">munnʼaktaːbɨttar</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">ös</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_62" n="HIAT:w" s="T17">oŋoron</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_65" n="HIAT:w" s="T18">Isuːs</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_68" n="HIAT:w" s="T19">tuhunan</ts>
                  <nts id="Seg_69" n="HIAT:ip">.</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_72" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_74" n="HIAT:w" s="T20">Ginini</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_76" n="HIAT:ip">(</nts>
                  <ts e="T22" id="Seg_78" n="HIAT:w" s="T21">ölü͡öge</ts>
                  <nts id="Seg_79" n="HIAT:ip">)</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">ölüːge</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_85" n="HIAT:w" s="T23">bi͡eri͡ekke</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_88" n="HIAT:w" s="T24">di͡en</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_91" n="HIAT:w" s="T25">hübelispitter</ts>
                  <nts id="Seg_92" n="HIAT:ip">.</nts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_95" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_97" n="HIAT:w" s="T26">Ginini</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_100" n="HIAT:w" s="T27">kam</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_103" n="HIAT:w" s="T28">baːjan</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_106" n="HIAT:w" s="T29">baraːn</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_109" n="HIAT:w" s="T30">ilpitter</ts>
                  <nts id="Seg_110" n="HIAT:ip">,</nts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_113" n="HIAT:w" s="T31">onno</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_116" n="HIAT:w" s="T32">bi͡erbitter</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_119" n="HIAT:w" s="T33">ginini</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34.tx.1" id="Seg_122" n="HIAT:w" s="T34">Poːntʼija</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_125" n="HIAT:w" s="T34.tx.1">Pʼilaːtka</ts>
                  <nts id="Seg_126" n="HIAT:ip">.</nts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T43" id="Seg_129" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_131" n="HIAT:w" s="T35">Pʼilaːt</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_134" n="HIAT:w" s="T36">ete</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_137" n="HIAT:w" s="T37">Rʼiːmtan</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_140" n="HIAT:w" s="T38">tojon</ts>
                  <nts id="Seg_141" n="HIAT:ip">–</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_144" n="HIAT:w" s="T39">Judʼeːja</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_147" n="HIAT:w" s="T40">onno</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_150" n="HIAT:w" s="T41">Samarʼiːja</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_153" n="HIAT:w" s="T42">dojdularga</ts>
                  <nts id="Seg_154" n="HIAT:ip">.</nts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_157" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_159" n="HIAT:w" s="T43">Oččogo</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_162" n="HIAT:w" s="T44">rʼiːmnar</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_165" n="HIAT:w" s="T45">taŋara</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_168" n="HIAT:w" s="T46">hirin</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_171" n="HIAT:w" s="T47">bɨldʼaːn</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_174" n="HIAT:w" s="T48">ɨlbɨt</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_177" n="HIAT:w" s="T49">etilere</ts>
                  <nts id="Seg_178" n="HIAT:ip">.</nts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T66" id="Seg_181" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_183" n="HIAT:w" s="T50">Oččogo</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_186" n="HIAT:w" s="T51">Juːda</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_189" n="HIAT:w" s="T52">ginini</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_192" n="HIAT:w" s="T53">ölüːge</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_195" n="HIAT:w" s="T54">bi͡ereːčči</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_198" n="HIAT:w" s="T55">körön</ts>
                  <nts id="Seg_199" n="HIAT:ip">,</nts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_202" n="HIAT:w" s="T56">gini</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_205" n="HIAT:w" s="T57">huːttammɨtɨn</ts>
                  <nts id="Seg_206" n="HIAT:ip">,</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_209" n="HIAT:w" s="T58">bejete</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_212" n="HIAT:w" s="T59">kuhaganɨ</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_215" n="HIAT:w" s="T60">oŋorbutun</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_218" n="HIAT:w" s="T61">bilen</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_221" n="HIAT:w" s="T62">töttörü</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_224" n="HIAT:w" s="T63">bi͡erbit</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_227" n="HIAT:w" s="T64">ɨlbɨt</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_230" n="HIAT:w" s="T65">karčɨlarɨn</ts>
                  <nts id="Seg_231" n="HIAT:ip">.</nts>
                  <nts id="Seg_232" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_234" n="HIAT:u" s="T66">
                  <ts e="T67" id="Seg_236" n="HIAT:w" s="T66">Bastɨŋ</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_239" n="HIAT:w" s="T67">agabɨttarga</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_242" n="HIAT:w" s="T68">onno</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_245" n="HIAT:w" s="T69">kɨrdʼagas</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_248" n="HIAT:w" s="T70">hübehitterge</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_251" n="HIAT:w" s="T71">haŋaran</ts>
                  <nts id="Seg_252" n="HIAT:ip">:</nts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_255" n="HIAT:u" s="T72">
                  <nts id="Seg_256" n="HIAT:ip">"</nts>
                  <ts e="T73" id="Seg_258" n="HIAT:w" s="T72">Min</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_261" n="HIAT:w" s="T73">anʼɨːnɨ</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_264" n="HIAT:w" s="T74">oŋorbutum</ts>
                  <nts id="Seg_265" n="HIAT:ip">,</nts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_268" n="HIAT:w" s="T75">ölüːge</ts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_271" n="HIAT:w" s="T76">bi͡eren</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_274" n="HIAT:w" s="T77">buruja</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_277" n="HIAT:w" s="T78">hu͡ogu</ts>
                  <nts id="Seg_278" n="HIAT:ip">"</nts>
                  <nts id="Seg_279" n="HIAT:ip">.</nts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T79" id="Seg_281" n="sc" s="T0">
               <ts e="T1" id="Seg_283" n="e" s="T0">Nöŋü͡ö </ts>
               <ts e="T2" id="Seg_285" n="e" s="T1">ös: </ts>
               <ts e="T3" id="Seg_287" n="e" s="T2">Isuːs </ts>
               <ts e="T4" id="Seg_289" n="e" s="T3">Pʼilaːt </ts>
               <ts e="T5" id="Seg_291" n="e" s="T4">innitiger </ts>
               <ts e="T6" id="Seg_293" n="e" s="T5">turuːta. </ts>
               <ts e="T7" id="Seg_295" n="e" s="T6">Kaččaga </ts>
               <ts e="T8" id="Seg_297" n="e" s="T7">harsi͡ersa </ts>
               <ts e="T9" id="Seg_299" n="e" s="T8">bu͡olaːtɨn, </ts>
               <ts e="T10" id="Seg_301" n="e" s="T9">barɨ </ts>
               <ts e="T11" id="Seg_303" n="e" s="T10">bastɨŋ </ts>
               <ts e="T12" id="Seg_305" n="e" s="T11">agabɨttar </ts>
               <ts e="T13" id="Seg_307" n="e" s="T12">onno </ts>
               <ts e="T14" id="Seg_309" n="e" s="T13">kɨrdʼagas </ts>
               <ts e="T15" id="Seg_311" n="e" s="T14">hübehitter </ts>
               <ts e="T16" id="Seg_313" n="e" s="T15">munnʼaktaːbɨttar </ts>
               <ts e="T17" id="Seg_315" n="e" s="T16">ös </ts>
               <ts e="T18" id="Seg_317" n="e" s="T17">oŋoron </ts>
               <ts e="T19" id="Seg_319" n="e" s="T18">Isuːs </ts>
               <ts e="T20" id="Seg_321" n="e" s="T19">tuhunan. </ts>
               <ts e="T21" id="Seg_323" n="e" s="T20">Ginini </ts>
               <ts e="T22" id="Seg_325" n="e" s="T21">(ölü͡öge) </ts>
               <ts e="T23" id="Seg_327" n="e" s="T22">ölüːge </ts>
               <ts e="T24" id="Seg_329" n="e" s="T23">bi͡eri͡ekke </ts>
               <ts e="T25" id="Seg_331" n="e" s="T24">di͡en </ts>
               <ts e="T26" id="Seg_333" n="e" s="T25">hübelispitter. </ts>
               <ts e="T27" id="Seg_335" n="e" s="T26">Ginini </ts>
               <ts e="T28" id="Seg_337" n="e" s="T27">kam </ts>
               <ts e="T29" id="Seg_339" n="e" s="T28">baːjan </ts>
               <ts e="T30" id="Seg_341" n="e" s="T29">baraːn </ts>
               <ts e="T31" id="Seg_343" n="e" s="T30">ilpitter, </ts>
               <ts e="T32" id="Seg_345" n="e" s="T31">onno </ts>
               <ts e="T33" id="Seg_347" n="e" s="T32">bi͡erbitter </ts>
               <ts e="T34" id="Seg_349" n="e" s="T33">ginini </ts>
               <ts e="T35" id="Seg_351" n="e" s="T34">Poːntʼija Pʼilaːtka. </ts>
               <ts e="T36" id="Seg_353" n="e" s="T35">Pʼilaːt </ts>
               <ts e="T37" id="Seg_355" n="e" s="T36">ete </ts>
               <ts e="T38" id="Seg_357" n="e" s="T37">Rʼiːmtan </ts>
               <ts e="T39" id="Seg_359" n="e" s="T38">tojon– </ts>
               <ts e="T40" id="Seg_361" n="e" s="T39">Judʼeːja </ts>
               <ts e="T41" id="Seg_363" n="e" s="T40">onno </ts>
               <ts e="T42" id="Seg_365" n="e" s="T41">Samarʼiːja </ts>
               <ts e="T43" id="Seg_367" n="e" s="T42">dojdularga. </ts>
               <ts e="T44" id="Seg_369" n="e" s="T43">Oččogo </ts>
               <ts e="T45" id="Seg_371" n="e" s="T44">rʼiːmnar </ts>
               <ts e="T46" id="Seg_373" n="e" s="T45">taŋara </ts>
               <ts e="T47" id="Seg_375" n="e" s="T46">hirin </ts>
               <ts e="T48" id="Seg_377" n="e" s="T47">bɨldʼaːn </ts>
               <ts e="T49" id="Seg_379" n="e" s="T48">ɨlbɨt </ts>
               <ts e="T50" id="Seg_381" n="e" s="T49">etilere. </ts>
               <ts e="T51" id="Seg_383" n="e" s="T50">Oččogo </ts>
               <ts e="T52" id="Seg_385" n="e" s="T51">Juːda </ts>
               <ts e="T53" id="Seg_387" n="e" s="T52">ginini </ts>
               <ts e="T54" id="Seg_389" n="e" s="T53">ölüːge </ts>
               <ts e="T55" id="Seg_391" n="e" s="T54">bi͡ereːčči </ts>
               <ts e="T56" id="Seg_393" n="e" s="T55">körön, </ts>
               <ts e="T57" id="Seg_395" n="e" s="T56">gini </ts>
               <ts e="T58" id="Seg_397" n="e" s="T57">huːttammɨtɨn, </ts>
               <ts e="T59" id="Seg_399" n="e" s="T58">bejete </ts>
               <ts e="T60" id="Seg_401" n="e" s="T59">kuhaganɨ </ts>
               <ts e="T61" id="Seg_403" n="e" s="T60">oŋorbutun </ts>
               <ts e="T62" id="Seg_405" n="e" s="T61">bilen </ts>
               <ts e="T63" id="Seg_407" n="e" s="T62">töttörü </ts>
               <ts e="T64" id="Seg_409" n="e" s="T63">bi͡erbit </ts>
               <ts e="T65" id="Seg_411" n="e" s="T64">ɨlbɨt </ts>
               <ts e="T66" id="Seg_413" n="e" s="T65">karčɨlarɨn. </ts>
               <ts e="T67" id="Seg_415" n="e" s="T66">Bastɨŋ </ts>
               <ts e="T68" id="Seg_417" n="e" s="T67">agabɨttarga </ts>
               <ts e="T69" id="Seg_419" n="e" s="T68">onno </ts>
               <ts e="T70" id="Seg_421" n="e" s="T69">kɨrdʼagas </ts>
               <ts e="T71" id="Seg_423" n="e" s="T70">hübehitterge </ts>
               <ts e="T72" id="Seg_425" n="e" s="T71">haŋaran: </ts>
               <ts e="T73" id="Seg_427" n="e" s="T72">"Min </ts>
               <ts e="T74" id="Seg_429" n="e" s="T73">anʼɨːnɨ </ts>
               <ts e="T75" id="Seg_431" n="e" s="T74">oŋorbutum, </ts>
               <ts e="T76" id="Seg_433" n="e" s="T75">ölüːge </ts>
               <ts e="T77" id="Seg_435" n="e" s="T76">bi͡eren </ts>
               <ts e="T78" id="Seg_437" n="e" s="T77">buruja </ts>
               <ts e="T79" id="Seg_439" n="e" s="T78">hu͡ogu". </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T2" id="Seg_440" s="T0">PoNA_19940110_JesusAndPilate_transl.001 (001.001)</ta>
            <ta e="T6" id="Seg_441" s="T2">PoNA_19940110_JesusAndPilate_transl.002 (001.002)</ta>
            <ta e="T20" id="Seg_442" s="T6">PoNA_19940110_JesusAndPilate_transl.003 (001.003)</ta>
            <ta e="T26" id="Seg_443" s="T20">PoNA_19940110_JesusAndPilate_transl.004 (001.004)</ta>
            <ta e="T35" id="Seg_444" s="T26">PoNA_19940110_JesusAndPilate_transl.005 (001.005)</ta>
            <ta e="T43" id="Seg_445" s="T35">PoNA_19940110_JesusAndPilate_transl.006 (001.006)</ta>
            <ta e="T50" id="Seg_446" s="T43">PoNA_19940110_JesusAndPilate_transl.007 (001.007)</ta>
            <ta e="T66" id="Seg_447" s="T50">PoNA_19940110_JesusAndPilate_transl.008 (001.008)</ta>
            <ta e="T72" id="Seg_448" s="T66">PoNA_19940110_JesusAndPilate_transl.009 (001.009)</ta>
            <ta e="T79" id="Seg_449" s="T72">PoNA_19940110_JesusAndPilate_transl.010 (001.010)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T2" id="Seg_450" s="T0">Нөӈүө өс: </ta>
            <ta e="T6" id="Seg_451" s="T2">Иисуус Пилаат иннитигэр туруута.</ta>
            <ta e="T20" id="Seg_452" s="T6">Каччага һарсиэрса (һарсиэрда) буолаатын, бары бастыӈ агабыттар онно кырдьагас һүбэһиттэр мунньактаабыттар өс оӈорон Иисуус туһунан.</ta>
            <ta e="T26" id="Seg_453" s="T20">Гинини (өлүөгэ) өлүүгэ биэриэккэ диэн һүбэлиспиттэр.</ta>
            <ta e="T35" id="Seg_454" s="T26">Гинини кам баайан бараан илпиттэр, онно биэрбиттэр гинини Понтийа Пилаатка.</ta>
            <ta e="T43" id="Seg_455" s="T35">Пилаат этэ Риимтан тойон – Йудьээя онно Самариия дойдуларга.</ta>
            <ta e="T50" id="Seg_456" s="T43">Оччого Риимнар таӈара һирин былдьаан ылбыт этилэрэ.</ta>
            <ta e="T66" id="Seg_457" s="T50">Оччого Иуда гинини өлүүгэ биэрээччи көрөн, гини һууттаммытын, бэйэтэ куһаганы оӈорбутун билэн төттөрү биэрбит ылбыт карчыларын.</ta>
            <ta e="T72" id="Seg_458" s="T66">Бастыӈ агабыттарга онно кырдьагас һүбэһиттэргэ һаӈаран: </ta>
            <ta e="T79" id="Seg_459" s="T72">"Мин аньыыны оӈорбутум, өлүүгэ биэрэн буруйа һуогу".</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T2" id="Seg_460" s="T0">Nöŋü͡ö ös: </ta>
            <ta e="T6" id="Seg_461" s="T2">Isuːs Pʼilaːt innitiger turuːta. </ta>
            <ta e="T20" id="Seg_462" s="T6">Kaččaga harsi͡ersa bu͡olaːtɨn, barɨ bastɨŋ agabɨttar onno kɨrdʼagas hübehitter munnʼaktaːbɨttar ös oŋoron Isuːs tuhunan. </ta>
            <ta e="T26" id="Seg_463" s="T20">Ginini (ölü͡öge) ölüːge bi͡eri͡ekke di͡en hübelispitter. </ta>
            <ta e="T35" id="Seg_464" s="T26">Ginini kam baːjan baraːn ilpitter, onno bi͡erbitter ginini Poːntʼija Pʼilaːtka. </ta>
            <ta e="T43" id="Seg_465" s="T35">Pʼilaːt ete Rʼiːmtan tojon– Judʼeːja onno Samarʼiːja dojdularga. </ta>
            <ta e="T50" id="Seg_466" s="T43">Oččogo rʼiːmnar taŋara hirin bɨldʼaːn ɨlbɨt etilere. </ta>
            <ta e="T66" id="Seg_467" s="T50">Oččogo Juːda ginini ölüːge bi͡ereːčči körön, gini huːttammɨtɨn, bejete kuhaganɨ oŋorbutun bilen töttörü bi͡erbit ɨlbɨt karčɨlarɨn. </ta>
            <ta e="T72" id="Seg_468" s="T66">Bastɨŋ agabɨttarga onno kɨrdʼagas hübehitterge haŋaran: </ta>
            <ta e="T79" id="Seg_469" s="T72">"Min anʼɨːnɨ oŋorbutum, ölüːge bi͡eren buruja hu͡ogu". </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_470" s="T0">nöŋü͡ö</ta>
            <ta e="T2" id="Seg_471" s="T1">ös</ta>
            <ta e="T3" id="Seg_472" s="T2">Isuːs</ta>
            <ta e="T4" id="Seg_473" s="T3">Pʼilaːt</ta>
            <ta e="T5" id="Seg_474" s="T4">inni-ti-ger</ta>
            <ta e="T6" id="Seg_475" s="T5">tur-uː-ta</ta>
            <ta e="T7" id="Seg_476" s="T6">kaččaga</ta>
            <ta e="T8" id="Seg_477" s="T7">harsi͡ersa</ta>
            <ta e="T9" id="Seg_478" s="T8">bu͡ol-aːt-ɨn</ta>
            <ta e="T10" id="Seg_479" s="T9">barɨ</ta>
            <ta e="T11" id="Seg_480" s="T10">bastɨŋ</ta>
            <ta e="T12" id="Seg_481" s="T11">agabɨt-tar</ta>
            <ta e="T13" id="Seg_482" s="T12">onno</ta>
            <ta e="T14" id="Seg_483" s="T13">kɨrdʼagas</ta>
            <ta e="T15" id="Seg_484" s="T14">hübehit-ter</ta>
            <ta e="T16" id="Seg_485" s="T15">munnʼak-taː-bɨt-tar</ta>
            <ta e="T17" id="Seg_486" s="T16">ös</ta>
            <ta e="T18" id="Seg_487" s="T17">oŋor-on</ta>
            <ta e="T19" id="Seg_488" s="T18">Isuːs</ta>
            <ta e="T20" id="Seg_489" s="T19">tuh-u-nan</ta>
            <ta e="T21" id="Seg_490" s="T20">gini-ni</ta>
            <ta e="T22" id="Seg_491" s="T21">öl-ü͡ö-ge</ta>
            <ta e="T23" id="Seg_492" s="T22">ölüː-ge</ta>
            <ta e="T24" id="Seg_493" s="T23">bi͡er-i͡ek-ke</ta>
            <ta e="T25" id="Seg_494" s="T24">di͡e-n</ta>
            <ta e="T26" id="Seg_495" s="T25">hübel-i-s-pit-ter</ta>
            <ta e="T27" id="Seg_496" s="T26">gini-ni</ta>
            <ta e="T28" id="Seg_497" s="T27">kam</ta>
            <ta e="T29" id="Seg_498" s="T28">baːj-an</ta>
            <ta e="T30" id="Seg_499" s="T29">baraːn</ta>
            <ta e="T31" id="Seg_500" s="T30">il-pit-ter</ta>
            <ta e="T32" id="Seg_501" s="T31">onno</ta>
            <ta e="T33" id="Seg_502" s="T32">bi͡er-bit-ter</ta>
            <ta e="T34" id="Seg_503" s="T33">gini-ni</ta>
            <ta e="T35" id="Seg_504" s="T34">Poːntʼija Pʼilaːt-ka</ta>
            <ta e="T36" id="Seg_505" s="T35">Pʼilaːt</ta>
            <ta e="T37" id="Seg_506" s="T36">e-t-e</ta>
            <ta e="T38" id="Seg_507" s="T37">Rʼiːm-tan</ta>
            <ta e="T39" id="Seg_508" s="T38">tojon</ta>
            <ta e="T40" id="Seg_509" s="T39">Judʼeːja</ta>
            <ta e="T41" id="Seg_510" s="T40">onno</ta>
            <ta e="T42" id="Seg_511" s="T41">Samarʼiːja</ta>
            <ta e="T43" id="Seg_512" s="T42">dojdu-lar-ga</ta>
            <ta e="T44" id="Seg_513" s="T43">oččogo</ta>
            <ta e="T45" id="Seg_514" s="T44">rʼiːm-nar</ta>
            <ta e="T46" id="Seg_515" s="T45">taŋara</ta>
            <ta e="T47" id="Seg_516" s="T46">hir-i-n</ta>
            <ta e="T48" id="Seg_517" s="T47">bɨldʼaː-n</ta>
            <ta e="T49" id="Seg_518" s="T48">ɨl-bɨt</ta>
            <ta e="T50" id="Seg_519" s="T49">e-ti-lere</ta>
            <ta e="T51" id="Seg_520" s="T50">oččogo</ta>
            <ta e="T52" id="Seg_521" s="T51">Juːda</ta>
            <ta e="T53" id="Seg_522" s="T52">gini-ni</ta>
            <ta e="T54" id="Seg_523" s="T53">ölüː-ge</ta>
            <ta e="T55" id="Seg_524" s="T54">bi͡er-eːčči</ta>
            <ta e="T56" id="Seg_525" s="T55">kör-ön</ta>
            <ta e="T57" id="Seg_526" s="T56">gini</ta>
            <ta e="T58" id="Seg_527" s="T57">huːt-tam-mɨt-ɨ-n</ta>
            <ta e="T59" id="Seg_528" s="T58">beje-te</ta>
            <ta e="T60" id="Seg_529" s="T59">kuhagan-ɨ</ta>
            <ta e="T61" id="Seg_530" s="T60">oŋor-but-u-n</ta>
            <ta e="T62" id="Seg_531" s="T61">bil-en</ta>
            <ta e="T63" id="Seg_532" s="T62">töttörü</ta>
            <ta e="T64" id="Seg_533" s="T63">bi͡er-bit</ta>
            <ta e="T65" id="Seg_534" s="T64">ɨl-bɨt</ta>
            <ta e="T66" id="Seg_535" s="T65">karčɨ-lar-ɨ-n</ta>
            <ta e="T67" id="Seg_536" s="T66">bastɨŋ</ta>
            <ta e="T68" id="Seg_537" s="T67">agabɨt-tar-ga</ta>
            <ta e="T69" id="Seg_538" s="T68">onno</ta>
            <ta e="T70" id="Seg_539" s="T69">kɨrdʼagas</ta>
            <ta e="T71" id="Seg_540" s="T70">hübehit-ter-ge</ta>
            <ta e="T72" id="Seg_541" s="T71">haŋar-an</ta>
            <ta e="T73" id="Seg_542" s="T72">min</ta>
            <ta e="T74" id="Seg_543" s="T73">anʼɨː-nɨ</ta>
            <ta e="T75" id="Seg_544" s="T74">oŋor-but-u-m</ta>
            <ta e="T76" id="Seg_545" s="T75">ölüː-ge</ta>
            <ta e="T77" id="Seg_546" s="T76">bi͡er-en</ta>
            <ta e="T78" id="Seg_547" s="T77">buruj-a</ta>
            <ta e="T79" id="Seg_548" s="T78">hu͡og-u</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_549" s="T0">nöŋü͡ö</ta>
            <ta e="T2" id="Seg_550" s="T1">ös</ta>
            <ta e="T3" id="Seg_551" s="T2">Isus</ta>
            <ta e="T4" id="Seg_552" s="T3">Pʼilaːt</ta>
            <ta e="T5" id="Seg_553" s="T4">ilin-tI-GAr</ta>
            <ta e="T6" id="Seg_554" s="T5">tur-Iː-tA</ta>
            <ta e="T7" id="Seg_555" s="T6">kaččaga</ta>
            <ta e="T8" id="Seg_556" s="T7">harsi͡erda</ta>
            <ta e="T9" id="Seg_557" s="T8">bu͡ol-AːT-In</ta>
            <ta e="T10" id="Seg_558" s="T9">barɨ</ta>
            <ta e="T11" id="Seg_559" s="T10">bastɨŋ</ta>
            <ta e="T12" id="Seg_560" s="T11">agabɨt-LAr</ta>
            <ta e="T13" id="Seg_561" s="T12">onno</ta>
            <ta e="T14" id="Seg_562" s="T13">kɨrdʼagas</ta>
            <ta e="T15" id="Seg_563" s="T14">hübehit-LAr</ta>
            <ta e="T16" id="Seg_564" s="T15">munnʼak-LAː-BIT-LAr</ta>
            <ta e="T17" id="Seg_565" s="T16">ös</ta>
            <ta e="T18" id="Seg_566" s="T17">oŋor-An</ta>
            <ta e="T19" id="Seg_567" s="T18">Isus</ta>
            <ta e="T20" id="Seg_568" s="T19">tus-tI-nAn</ta>
            <ta e="T21" id="Seg_569" s="T20">gini-nI</ta>
            <ta e="T22" id="Seg_570" s="T21">öl-IAK-GA</ta>
            <ta e="T23" id="Seg_571" s="T22">ölüː-GA</ta>
            <ta e="T24" id="Seg_572" s="T23">bi͡er-IAK-GA</ta>
            <ta e="T25" id="Seg_573" s="T24">di͡e-An</ta>
            <ta e="T26" id="Seg_574" s="T25">hübeleː-I-s-BIT-LAr</ta>
            <ta e="T27" id="Seg_575" s="T26">gini-nI</ta>
            <ta e="T28" id="Seg_576" s="T27">kam</ta>
            <ta e="T29" id="Seg_577" s="T28">baːj-An</ta>
            <ta e="T30" id="Seg_578" s="T29">baran</ta>
            <ta e="T31" id="Seg_579" s="T30">ilt-BIT-LAr</ta>
            <ta e="T32" id="Seg_580" s="T31">onno</ta>
            <ta e="T33" id="Seg_581" s="T32">bi͡er-BIT-LAr</ta>
            <ta e="T34" id="Seg_582" s="T33">gini-nI</ta>
            <ta e="T35" id="Seg_583" s="T34">Poːntʼija Pʼilaːt-GA</ta>
            <ta e="T36" id="Seg_584" s="T35">Pʼilaːt</ta>
            <ta e="T37" id="Seg_585" s="T36">e-TI-tA</ta>
            <ta e="T38" id="Seg_586" s="T37">Rʼiːm-ttAn</ta>
            <ta e="T39" id="Seg_587" s="T38">tojon</ta>
            <ta e="T40" id="Seg_588" s="T39">Judʼeːja</ta>
            <ta e="T41" id="Seg_589" s="T40">onno</ta>
            <ta e="T42" id="Seg_590" s="T41">Samarʼiːja</ta>
            <ta e="T43" id="Seg_591" s="T42">dojdu-LAr-GA</ta>
            <ta e="T44" id="Seg_592" s="T43">oččogo</ta>
            <ta e="T45" id="Seg_593" s="T44">Rʼiːm-LAr</ta>
            <ta e="T46" id="Seg_594" s="T45">taŋara</ta>
            <ta e="T47" id="Seg_595" s="T46">hir-tI-n</ta>
            <ta e="T48" id="Seg_596" s="T47">bɨldʼaː-An</ta>
            <ta e="T49" id="Seg_597" s="T48">ɨl-BIT</ta>
            <ta e="T50" id="Seg_598" s="T49">e-TI-LArA</ta>
            <ta e="T51" id="Seg_599" s="T50">oččogo</ta>
            <ta e="T52" id="Seg_600" s="T51">Juːda</ta>
            <ta e="T53" id="Seg_601" s="T52">gini-nI</ta>
            <ta e="T54" id="Seg_602" s="T53">ölüː-GA</ta>
            <ta e="T55" id="Seg_603" s="T54">bi͡er-AːččI</ta>
            <ta e="T56" id="Seg_604" s="T55">kör-An</ta>
            <ta e="T57" id="Seg_605" s="T56">gini</ta>
            <ta e="T58" id="Seg_606" s="T57">huːt-LAN-BIT-tI-n</ta>
            <ta e="T59" id="Seg_607" s="T58">beje-tA</ta>
            <ta e="T60" id="Seg_608" s="T59">kuhagan-nI</ta>
            <ta e="T61" id="Seg_609" s="T60">oŋor-BIT-tI-n</ta>
            <ta e="T62" id="Seg_610" s="T61">bil-An</ta>
            <ta e="T63" id="Seg_611" s="T62">töttörü</ta>
            <ta e="T64" id="Seg_612" s="T63">bi͡er-BIT</ta>
            <ta e="T65" id="Seg_613" s="T64">ɨl-BIT</ta>
            <ta e="T66" id="Seg_614" s="T65">karčɨ-LAr-tI-n</ta>
            <ta e="T67" id="Seg_615" s="T66">bastɨŋ</ta>
            <ta e="T68" id="Seg_616" s="T67">agabɨt-LAr-GA</ta>
            <ta e="T69" id="Seg_617" s="T68">onno</ta>
            <ta e="T70" id="Seg_618" s="T69">kɨrdʼagas</ta>
            <ta e="T71" id="Seg_619" s="T70">hübehit-LAr-GA</ta>
            <ta e="T72" id="Seg_620" s="T71">haŋar-An</ta>
            <ta e="T73" id="Seg_621" s="T72">min</ta>
            <ta e="T74" id="Seg_622" s="T73">anʼɨː-nI</ta>
            <ta e="T75" id="Seg_623" s="T74">oŋor-BIT-I-m</ta>
            <ta e="T76" id="Seg_624" s="T75">ölüː-GA</ta>
            <ta e="T77" id="Seg_625" s="T76">bi͡er-An</ta>
            <ta e="T78" id="Seg_626" s="T77">buruj-tA</ta>
            <ta e="T79" id="Seg_627" s="T78">hu͡ok-nI</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_628" s="T0">next</ta>
            <ta e="T2" id="Seg_629" s="T1">story.[NOM]</ta>
            <ta e="T3" id="Seg_630" s="T2">Jesus.[NOM]</ta>
            <ta e="T4" id="Seg_631" s="T3">Pilate.[NOM]</ta>
            <ta e="T5" id="Seg_632" s="T4">front-3SG-DAT/LOC</ta>
            <ta e="T6" id="Seg_633" s="T5">stand-NMNZ-3SG.[NOM]</ta>
            <ta e="T7" id="Seg_634" s="T6">when</ta>
            <ta e="T8" id="Seg_635" s="T7">morning.[NOM]</ta>
            <ta e="T9" id="Seg_636" s="T8">be-CVB.ANT-3SG</ta>
            <ta e="T10" id="Seg_637" s="T9">every</ta>
            <ta e="T11" id="Seg_638" s="T10">leading</ta>
            <ta e="T12" id="Seg_639" s="T11">clergyman-PL.[NOM]</ta>
            <ta e="T13" id="Seg_640" s="T12">then</ta>
            <ta e="T14" id="Seg_641" s="T13">old</ta>
            <ta e="T15" id="Seg_642" s="T14">leader-PL.[NOM]</ta>
            <ta e="T16" id="Seg_643" s="T15">gathering-VBZ-PST2-3PL</ta>
            <ta e="T17" id="Seg_644" s="T16">word.[NOM]</ta>
            <ta e="T18" id="Seg_645" s="T17">make-CVB.SEQ</ta>
            <ta e="T19" id="Seg_646" s="T18">Jesus.[NOM]</ta>
            <ta e="T20" id="Seg_647" s="T19">side-3SG-INSTR</ta>
            <ta e="T21" id="Seg_648" s="T20">3SG-ACC</ta>
            <ta e="T22" id="Seg_649" s="T21">die-PTCP.FUT-DAT/LOC</ta>
            <ta e="T23" id="Seg_650" s="T22">death-DAT/LOC</ta>
            <ta e="T24" id="Seg_651" s="T23">give-PTCP.FUT-DAT/LOC</ta>
            <ta e="T25" id="Seg_652" s="T24">say-CVB.SEQ</ta>
            <ta e="T26" id="Seg_653" s="T25">advise-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T27" id="Seg_654" s="T26">3SG-ACC</ta>
            <ta e="T28" id="Seg_655" s="T27">strongly</ta>
            <ta e="T29" id="Seg_656" s="T28">tie-CVB.SEQ</ta>
            <ta e="T30" id="Seg_657" s="T29">after</ta>
            <ta e="T31" id="Seg_658" s="T30">bring-PST2-3PL</ta>
            <ta e="T32" id="Seg_659" s="T31">there</ta>
            <ta e="T33" id="Seg_660" s="T32">give-PST2-3PL</ta>
            <ta e="T34" id="Seg_661" s="T33">3SG-ACC</ta>
            <ta e="T35" id="Seg_662" s="T34">Pontius.Pilate-DAT/LOC</ta>
            <ta e="T36" id="Seg_663" s="T35">Pilate.[NOM]</ta>
            <ta e="T37" id="Seg_664" s="T36">be-PST1-3SG</ta>
            <ta e="T38" id="Seg_665" s="T37">Rome-ABL</ta>
            <ta e="T39" id="Seg_666" s="T38">lord.[NOM]</ta>
            <ta e="T40" id="Seg_667" s="T39">Judea.[NOM]</ta>
            <ta e="T41" id="Seg_668" s="T40">then</ta>
            <ta e="T42" id="Seg_669" s="T41">Samaria.[NOM]</ta>
            <ta e="T43" id="Seg_670" s="T42">country-PL-DAT/LOC</ta>
            <ta e="T44" id="Seg_671" s="T43">then</ta>
            <ta e="T45" id="Seg_672" s="T44">Rome-PL.[NOM]</ta>
            <ta e="T46" id="Seg_673" s="T45">god.[NOM]</ta>
            <ta e="T47" id="Seg_674" s="T46">earth-3SG-ACC</ta>
            <ta e="T48" id="Seg_675" s="T47">take.away-CVB.SEQ</ta>
            <ta e="T49" id="Seg_676" s="T48">take-PTCP.PST</ta>
            <ta e="T50" id="Seg_677" s="T49">be-PST1-3PL</ta>
            <ta e="T51" id="Seg_678" s="T50">then</ta>
            <ta e="T52" id="Seg_679" s="T51">Judas.[NOM]</ta>
            <ta e="T53" id="Seg_680" s="T52">3SG-ACC</ta>
            <ta e="T54" id="Seg_681" s="T53">death-DAT/LOC</ta>
            <ta e="T55" id="Seg_682" s="T54">give-PTCP.HAB.[NOM]</ta>
            <ta e="T56" id="Seg_683" s="T55">see-CVB.SEQ</ta>
            <ta e="T57" id="Seg_684" s="T56">3SG.[NOM]</ta>
            <ta e="T58" id="Seg_685" s="T57">court-VBZ-PTCP.PST-3SG-ACC</ta>
            <ta e="T59" id="Seg_686" s="T58">self-3SG.[NOM]</ta>
            <ta e="T60" id="Seg_687" s="T59">bad-ACC</ta>
            <ta e="T61" id="Seg_688" s="T60">make-PTCP.PST-3SG-ACC</ta>
            <ta e="T62" id="Seg_689" s="T61">notice-CVB.SEQ</ta>
            <ta e="T63" id="Seg_690" s="T62">back</ta>
            <ta e="T64" id="Seg_691" s="T63">give-PST2.[3SG]</ta>
            <ta e="T65" id="Seg_692" s="T64">take-PTCP.PST</ta>
            <ta e="T66" id="Seg_693" s="T65">money-PL-3SG-ACC</ta>
            <ta e="T67" id="Seg_694" s="T66">best</ta>
            <ta e="T68" id="Seg_695" s="T67">clergyman-PL-DAT/LOC</ta>
            <ta e="T69" id="Seg_696" s="T68">then</ta>
            <ta e="T70" id="Seg_697" s="T69">old</ta>
            <ta e="T71" id="Seg_698" s="T70">leader-PL-DAT/LOC</ta>
            <ta e="T72" id="Seg_699" s="T71">speak-CVB.SEQ</ta>
            <ta e="T73" id="Seg_700" s="T72">1SG.[NOM]</ta>
            <ta e="T74" id="Seg_701" s="T73">sin-ACC</ta>
            <ta e="T75" id="Seg_702" s="T74">make-PST2-EP-1SG</ta>
            <ta e="T76" id="Seg_703" s="T75">death-DAT/LOC</ta>
            <ta e="T77" id="Seg_704" s="T76">give-CVB.SEQ</ta>
            <ta e="T78" id="Seg_705" s="T77">guilt-POSS</ta>
            <ta e="T79" id="Seg_706" s="T78">NEG-ACC</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_707" s="T0">nächster</ta>
            <ta e="T2" id="Seg_708" s="T1">Erzählung.[NOM]</ta>
            <ta e="T3" id="Seg_709" s="T2">Jesus.[NOM]</ta>
            <ta e="T4" id="Seg_710" s="T3">Pilatus.[NOM]</ta>
            <ta e="T5" id="Seg_711" s="T4">Vorderteil-3SG-DAT/LOC</ta>
            <ta e="T6" id="Seg_712" s="T5">stehen-NMNZ-3SG.[NOM]</ta>
            <ta e="T7" id="Seg_713" s="T6">als</ta>
            <ta e="T8" id="Seg_714" s="T7">Morgen.[NOM]</ta>
            <ta e="T9" id="Seg_715" s="T8">sein-CVB.ANT-3SG</ta>
            <ta e="T10" id="Seg_716" s="T9">jeder</ta>
            <ta e="T11" id="Seg_717" s="T10">führend</ta>
            <ta e="T12" id="Seg_718" s="T11">Geistlicher-PL.[NOM]</ta>
            <ta e="T13" id="Seg_719" s="T12">dann</ta>
            <ta e="T14" id="Seg_720" s="T13">alt</ta>
            <ta e="T15" id="Seg_721" s="T14">Anführer-PL.[NOM]</ta>
            <ta e="T16" id="Seg_722" s="T15">Versammlung-VBZ-PST2-3PL</ta>
            <ta e="T17" id="Seg_723" s="T16">Wort.[NOM]</ta>
            <ta e="T18" id="Seg_724" s="T17">machen-CVB.SEQ</ta>
            <ta e="T19" id="Seg_725" s="T18">Jesus.[NOM]</ta>
            <ta e="T20" id="Seg_726" s="T19">Seite-3SG-INSTR</ta>
            <ta e="T21" id="Seg_727" s="T20">3SG-ACC</ta>
            <ta e="T22" id="Seg_728" s="T21">sterben-PTCP.FUT-DAT/LOC</ta>
            <ta e="T23" id="Seg_729" s="T22">Tod-DAT/LOC</ta>
            <ta e="T24" id="Seg_730" s="T23">geben-PTCP.FUT-DAT/LOC</ta>
            <ta e="T25" id="Seg_731" s="T24">sagen-CVB.SEQ</ta>
            <ta e="T26" id="Seg_732" s="T25">raten-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T27" id="Seg_733" s="T26">3SG-ACC</ta>
            <ta e="T28" id="Seg_734" s="T27">heftig</ta>
            <ta e="T29" id="Seg_735" s="T28">binden-CVB.SEQ</ta>
            <ta e="T30" id="Seg_736" s="T29">nachdem</ta>
            <ta e="T31" id="Seg_737" s="T30">bringen-PST2-3PL</ta>
            <ta e="T32" id="Seg_738" s="T31">dort</ta>
            <ta e="T33" id="Seg_739" s="T32">geben-PST2-3PL</ta>
            <ta e="T34" id="Seg_740" s="T33">3SG-ACC</ta>
            <ta e="T35" id="Seg_741" s="T34">Pontius.Pilatus-DAT/LOC</ta>
            <ta e="T36" id="Seg_742" s="T35">Pilatus.[NOM]</ta>
            <ta e="T37" id="Seg_743" s="T36">sein-PST1-3SG</ta>
            <ta e="T38" id="Seg_744" s="T37">Rom-ABL</ta>
            <ta e="T39" id="Seg_745" s="T38">Herr.[NOM]</ta>
            <ta e="T40" id="Seg_746" s="T39">Judäa.[NOM]</ta>
            <ta e="T41" id="Seg_747" s="T40">dann</ta>
            <ta e="T42" id="Seg_748" s="T41">Samaria.[NOM]</ta>
            <ta e="T43" id="Seg_749" s="T42">Land-PL-DAT/LOC</ta>
            <ta e="T44" id="Seg_750" s="T43">dann</ta>
            <ta e="T45" id="Seg_751" s="T44">Rom-PL.[NOM]</ta>
            <ta e="T46" id="Seg_752" s="T45">Gott.[NOM]</ta>
            <ta e="T47" id="Seg_753" s="T46">Erde-3SG-ACC</ta>
            <ta e="T48" id="Seg_754" s="T47">wegnehmen-CVB.SEQ</ta>
            <ta e="T49" id="Seg_755" s="T48">nehmen-PTCP.PST</ta>
            <ta e="T50" id="Seg_756" s="T49">sein-PST1-3PL</ta>
            <ta e="T51" id="Seg_757" s="T50">dann</ta>
            <ta e="T52" id="Seg_758" s="T51">Judas.[NOM]</ta>
            <ta e="T53" id="Seg_759" s="T52">3SG-ACC</ta>
            <ta e="T54" id="Seg_760" s="T53">Tod-DAT/LOC</ta>
            <ta e="T55" id="Seg_761" s="T54">geben-PTCP.HAB.[NOM]</ta>
            <ta e="T56" id="Seg_762" s="T55">sehen-CVB.SEQ</ta>
            <ta e="T57" id="Seg_763" s="T56">3SG.[NOM]</ta>
            <ta e="T58" id="Seg_764" s="T57">Gericht-VBZ-PTCP.PST-3SG-ACC</ta>
            <ta e="T59" id="Seg_765" s="T58">selbst-3SG.[NOM]</ta>
            <ta e="T60" id="Seg_766" s="T59">schlecht-ACC</ta>
            <ta e="T61" id="Seg_767" s="T60">machen-PTCP.PST-3SG-ACC</ta>
            <ta e="T62" id="Seg_768" s="T61">bemerken-CVB.SEQ</ta>
            <ta e="T63" id="Seg_769" s="T62">zurück</ta>
            <ta e="T64" id="Seg_770" s="T63">geben-PST2.[3SG]</ta>
            <ta e="T65" id="Seg_771" s="T64">nehmen-PTCP.PST</ta>
            <ta e="T66" id="Seg_772" s="T65">Geld-PL-3SG-ACC</ta>
            <ta e="T67" id="Seg_773" s="T66">bester</ta>
            <ta e="T68" id="Seg_774" s="T67">Geistlicher-PL-DAT/LOC</ta>
            <ta e="T69" id="Seg_775" s="T68">dann</ta>
            <ta e="T70" id="Seg_776" s="T69">alt</ta>
            <ta e="T71" id="Seg_777" s="T70">Anführer-PL-DAT/LOC</ta>
            <ta e="T72" id="Seg_778" s="T71">sprechen-CVB.SEQ</ta>
            <ta e="T73" id="Seg_779" s="T72">1SG.[NOM]</ta>
            <ta e="T74" id="Seg_780" s="T73">Sünde-ACC</ta>
            <ta e="T75" id="Seg_781" s="T74">machen-PST2-EP-1SG</ta>
            <ta e="T76" id="Seg_782" s="T75">Tod-DAT/LOC</ta>
            <ta e="T77" id="Seg_783" s="T76">geben-CVB.SEQ</ta>
            <ta e="T78" id="Seg_784" s="T77">Schuld-POSS</ta>
            <ta e="T79" id="Seg_785" s="T78">NEG-ACC</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_786" s="T0">следующий</ta>
            <ta e="T2" id="Seg_787" s="T1">рассказ.[NOM]</ta>
            <ta e="T3" id="Seg_788" s="T2">Исус.[NOM]</ta>
            <ta e="T4" id="Seg_789" s="T3">Пилат.[NOM]</ta>
            <ta e="T5" id="Seg_790" s="T4">передняя.часть-3SG-DAT/LOC</ta>
            <ta e="T6" id="Seg_791" s="T5">стоять-NMNZ-3SG.[NOM]</ta>
            <ta e="T7" id="Seg_792" s="T6">когда</ta>
            <ta e="T8" id="Seg_793" s="T7">утро.[NOM]</ta>
            <ta e="T9" id="Seg_794" s="T8">быть-CVB.ANT-3SG</ta>
            <ta e="T10" id="Seg_795" s="T9">каждый</ta>
            <ta e="T11" id="Seg_796" s="T10">передовой</ta>
            <ta e="T12" id="Seg_797" s="T11">священник-PL.[NOM]</ta>
            <ta e="T13" id="Seg_798" s="T12">тогда</ta>
            <ta e="T14" id="Seg_799" s="T13">старый</ta>
            <ta e="T15" id="Seg_800" s="T14">руководитель-PL.[NOM]</ta>
            <ta e="T16" id="Seg_801" s="T15">сход-VBZ-PST2-3PL</ta>
            <ta e="T17" id="Seg_802" s="T16">слово.[NOM]</ta>
            <ta e="T18" id="Seg_803" s="T17">делать-CVB.SEQ</ta>
            <ta e="T19" id="Seg_804" s="T18">Исус.[NOM]</ta>
            <ta e="T20" id="Seg_805" s="T19">сторона-3SG-INSTR</ta>
            <ta e="T21" id="Seg_806" s="T20">3SG-ACC</ta>
            <ta e="T22" id="Seg_807" s="T21">умирать-PTCP.FUT-DAT/LOC</ta>
            <ta e="T23" id="Seg_808" s="T22">смерть-DAT/LOC</ta>
            <ta e="T24" id="Seg_809" s="T23">давать-PTCP.FUT-DAT/LOC</ta>
            <ta e="T25" id="Seg_810" s="T24">говорить-CVB.SEQ</ta>
            <ta e="T26" id="Seg_811" s="T25">советовать-EP-RECP/COLL-PST2-3PL</ta>
            <ta e="T27" id="Seg_812" s="T26">3SG-ACC</ta>
            <ta e="T28" id="Seg_813" s="T27">крепко</ta>
            <ta e="T29" id="Seg_814" s="T28">связывать-CVB.SEQ</ta>
            <ta e="T30" id="Seg_815" s="T29">после</ta>
            <ta e="T31" id="Seg_816" s="T30">приносить-PST2-3PL</ta>
            <ta e="T32" id="Seg_817" s="T31">там</ta>
            <ta e="T33" id="Seg_818" s="T32">давать-PST2-3PL</ta>
            <ta e="T34" id="Seg_819" s="T33">3SG-ACC</ta>
            <ta e="T35" id="Seg_820" s="T34">Понтий.Пилат-DAT/LOC</ta>
            <ta e="T36" id="Seg_821" s="T35">Пилат.[NOM]</ta>
            <ta e="T37" id="Seg_822" s="T36">быть-PST1-3SG</ta>
            <ta e="T38" id="Seg_823" s="T37">Рим-ABL</ta>
            <ta e="T39" id="Seg_824" s="T38">господин.[NOM]</ta>
            <ta e="T40" id="Seg_825" s="T39">Иудея.[NOM]</ta>
            <ta e="T41" id="Seg_826" s="T40">тогда</ta>
            <ta e="T42" id="Seg_827" s="T41">Самария.[NOM]</ta>
            <ta e="T43" id="Seg_828" s="T42">страна-PL-DAT/LOC</ta>
            <ta e="T44" id="Seg_829" s="T43">тогда</ta>
            <ta e="T45" id="Seg_830" s="T44">Рим-PL.[NOM]</ta>
            <ta e="T46" id="Seg_831" s="T45">Бог.[NOM]</ta>
            <ta e="T47" id="Seg_832" s="T46">земля-3SG-ACC</ta>
            <ta e="T48" id="Seg_833" s="T47">отнимать-CVB.SEQ</ta>
            <ta e="T49" id="Seg_834" s="T48">взять-PTCP.PST</ta>
            <ta e="T50" id="Seg_835" s="T49">быть-PST1-3PL</ta>
            <ta e="T51" id="Seg_836" s="T50">тогда</ta>
            <ta e="T52" id="Seg_837" s="T51">Иуда.[NOM]</ta>
            <ta e="T53" id="Seg_838" s="T52">3SG-ACC</ta>
            <ta e="T54" id="Seg_839" s="T53">смерть-DAT/LOC</ta>
            <ta e="T55" id="Seg_840" s="T54">давать-PTCP.HAB.[NOM]</ta>
            <ta e="T56" id="Seg_841" s="T55">видеть-CVB.SEQ</ta>
            <ta e="T57" id="Seg_842" s="T56">3SG.[NOM]</ta>
            <ta e="T58" id="Seg_843" s="T57">суд-VBZ-PTCP.PST-3SG-ACC</ta>
            <ta e="T59" id="Seg_844" s="T58">сам-3SG.[NOM]</ta>
            <ta e="T60" id="Seg_845" s="T59">плохой-ACC</ta>
            <ta e="T61" id="Seg_846" s="T60">делать-PTCP.PST-3SG-ACC</ta>
            <ta e="T62" id="Seg_847" s="T61">замечать-CVB.SEQ</ta>
            <ta e="T63" id="Seg_848" s="T62">назад</ta>
            <ta e="T64" id="Seg_849" s="T63">давать-PST2.[3SG]</ta>
            <ta e="T65" id="Seg_850" s="T64">взять-PTCP.PST</ta>
            <ta e="T66" id="Seg_851" s="T65">деньги-PL-3SG-ACC</ta>
            <ta e="T67" id="Seg_852" s="T66">лучший</ta>
            <ta e="T68" id="Seg_853" s="T67">священник-PL-DAT/LOC</ta>
            <ta e="T69" id="Seg_854" s="T68">тогда</ta>
            <ta e="T70" id="Seg_855" s="T69">старый</ta>
            <ta e="T71" id="Seg_856" s="T70">руководитель-PL-DAT/LOC</ta>
            <ta e="T72" id="Seg_857" s="T71">говорить-CVB.SEQ</ta>
            <ta e="T73" id="Seg_858" s="T72">1SG.[NOM]</ta>
            <ta e="T74" id="Seg_859" s="T73">грех-ACC</ta>
            <ta e="T75" id="Seg_860" s="T74">делать-PST2-EP-1SG</ta>
            <ta e="T76" id="Seg_861" s="T75">смерть-DAT/LOC</ta>
            <ta e="T77" id="Seg_862" s="T76">давать-CVB.SEQ</ta>
            <ta e="T78" id="Seg_863" s="T77">вина-POSS</ta>
            <ta e="T79" id="Seg_864" s="T78">NEG-ACC</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_865" s="T0">adj</ta>
            <ta e="T2" id="Seg_866" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_867" s="T2">propr-n:case</ta>
            <ta e="T4" id="Seg_868" s="T3">propr-n:case</ta>
            <ta e="T5" id="Seg_869" s="T4">n-n:poss-n:case</ta>
            <ta e="T6" id="Seg_870" s="T5">v-v&gt;n-n:(poss)-n:case</ta>
            <ta e="T7" id="Seg_871" s="T6">conj</ta>
            <ta e="T8" id="Seg_872" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_873" s="T8">v-v:cvb-v:pred.pn</ta>
            <ta e="T10" id="Seg_874" s="T9">adj</ta>
            <ta e="T11" id="Seg_875" s="T10">adj</ta>
            <ta e="T12" id="Seg_876" s="T11">n-n:(num)-n:case</ta>
            <ta e="T13" id="Seg_877" s="T12">adv</ta>
            <ta e="T14" id="Seg_878" s="T13">adj</ta>
            <ta e="T15" id="Seg_879" s="T14">n-n:(num)-n:case</ta>
            <ta e="T16" id="Seg_880" s="T15">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T17" id="Seg_881" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_882" s="T17">v-v:cvb</ta>
            <ta e="T19" id="Seg_883" s="T18">propr-n:case</ta>
            <ta e="T20" id="Seg_884" s="T19">n-n:poss-n:case</ta>
            <ta e="T21" id="Seg_885" s="T20">pers-pro:case</ta>
            <ta e="T22" id="Seg_886" s="T21">v-v:ptcp-v:(case)</ta>
            <ta e="T23" id="Seg_887" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_888" s="T23">v-v:ptcp-v:(case)</ta>
            <ta e="T25" id="Seg_889" s="T24">v-v:cvb</ta>
            <ta e="T26" id="Seg_890" s="T25">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T27" id="Seg_891" s="T26">pers-pro:case</ta>
            <ta e="T28" id="Seg_892" s="T27">adv</ta>
            <ta e="T29" id="Seg_893" s="T28">v-v:cvb</ta>
            <ta e="T30" id="Seg_894" s="T29">post</ta>
            <ta e="T31" id="Seg_895" s="T30">v-v:tense-v:pred.pn</ta>
            <ta e="T32" id="Seg_896" s="T31">adv</ta>
            <ta e="T33" id="Seg_897" s="T32">v-v:tense-v:pred.pn</ta>
            <ta e="T34" id="Seg_898" s="T33">pers-pro:case</ta>
            <ta e="T35" id="Seg_899" s="T34">propr-n:case</ta>
            <ta e="T36" id="Seg_900" s="T35">propr-n:case</ta>
            <ta e="T37" id="Seg_901" s="T36">v-v:tense-v:poss.pn</ta>
            <ta e="T38" id="Seg_902" s="T37">propr-n:case</ta>
            <ta e="T39" id="Seg_903" s="T38">n-n:case</ta>
            <ta e="T40" id="Seg_904" s="T39">propr-n:case</ta>
            <ta e="T41" id="Seg_905" s="T40">adv</ta>
            <ta e="T42" id="Seg_906" s="T41">propr-n:case</ta>
            <ta e="T43" id="Seg_907" s="T42">n-n:(num)-n:case</ta>
            <ta e="T44" id="Seg_908" s="T43">adv</ta>
            <ta e="T45" id="Seg_909" s="T44">propr-n:(num)-n:case</ta>
            <ta e="T46" id="Seg_910" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_911" s="T46">n-n:poss-n:case</ta>
            <ta e="T48" id="Seg_912" s="T47">v-v:cvb</ta>
            <ta e="T49" id="Seg_913" s="T48">v-v:ptcp</ta>
            <ta e="T50" id="Seg_914" s="T49">v-v:tense-v:poss.pn</ta>
            <ta e="T51" id="Seg_915" s="T50">adv</ta>
            <ta e="T52" id="Seg_916" s="T51">propr-n:case</ta>
            <ta e="T53" id="Seg_917" s="T52">pers-pro:case</ta>
            <ta e="T54" id="Seg_918" s="T53">n-n:case</ta>
            <ta e="T55" id="Seg_919" s="T54">v-v:ptcp-v:(case)</ta>
            <ta e="T56" id="Seg_920" s="T55">v-v:cvb</ta>
            <ta e="T57" id="Seg_921" s="T56">pers-pro:case</ta>
            <ta e="T58" id="Seg_922" s="T57">n-n&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T59" id="Seg_923" s="T58">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T60" id="Seg_924" s="T59">adj-n:case</ta>
            <ta e="T61" id="Seg_925" s="T60">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T62" id="Seg_926" s="T61">v-v:cvb</ta>
            <ta e="T63" id="Seg_927" s="T62">adv</ta>
            <ta e="T64" id="Seg_928" s="T63">v-v:tense-v:pred.pn</ta>
            <ta e="T65" id="Seg_929" s="T64">v-v:ptcp</ta>
            <ta e="T66" id="Seg_930" s="T65">n-n:(num)-n:poss-n:case</ta>
            <ta e="T67" id="Seg_931" s="T66">adj</ta>
            <ta e="T68" id="Seg_932" s="T67">n-n:(num)-n:case</ta>
            <ta e="T69" id="Seg_933" s="T68">adv</ta>
            <ta e="T70" id="Seg_934" s="T69">adj</ta>
            <ta e="T71" id="Seg_935" s="T70">n-n:(num)-n:case</ta>
            <ta e="T72" id="Seg_936" s="T71">v-v:cvb</ta>
            <ta e="T73" id="Seg_937" s="T72">pers-pro:case</ta>
            <ta e="T74" id="Seg_938" s="T73">n-n:case</ta>
            <ta e="T75" id="Seg_939" s="T74">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T76" id="Seg_940" s="T75">n-n:case</ta>
            <ta e="T77" id="Seg_941" s="T76">v-v:cvb</ta>
            <ta e="T78" id="Seg_942" s="T77">n-n:(poss)</ta>
            <ta e="T79" id="Seg_943" s="T78">ptcl-ptcl:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_944" s="T0">adj</ta>
            <ta e="T2" id="Seg_945" s="T1">n</ta>
            <ta e="T3" id="Seg_946" s="T2">propr</ta>
            <ta e="T4" id="Seg_947" s="T3">propr</ta>
            <ta e="T5" id="Seg_948" s="T4">n</ta>
            <ta e="T6" id="Seg_949" s="T5">n</ta>
            <ta e="T7" id="Seg_950" s="T6">conj</ta>
            <ta e="T8" id="Seg_951" s="T7">n</ta>
            <ta e="T9" id="Seg_952" s="T8">cop</ta>
            <ta e="T10" id="Seg_953" s="T9">adj</ta>
            <ta e="T11" id="Seg_954" s="T10">adj</ta>
            <ta e="T12" id="Seg_955" s="T11">n</ta>
            <ta e="T13" id="Seg_956" s="T12">adv</ta>
            <ta e="T14" id="Seg_957" s="T13">adj</ta>
            <ta e="T15" id="Seg_958" s="T14">n</ta>
            <ta e="T16" id="Seg_959" s="T15">v</ta>
            <ta e="T17" id="Seg_960" s="T16">n</ta>
            <ta e="T18" id="Seg_961" s="T17">v</ta>
            <ta e="T19" id="Seg_962" s="T18">propr</ta>
            <ta e="T20" id="Seg_963" s="T19">n</ta>
            <ta e="T21" id="Seg_964" s="T20">pers</ta>
            <ta e="T22" id="Seg_965" s="T21">v</ta>
            <ta e="T23" id="Seg_966" s="T22">n</ta>
            <ta e="T24" id="Seg_967" s="T23">v</ta>
            <ta e="T25" id="Seg_968" s="T24">v</ta>
            <ta e="T26" id="Seg_969" s="T25">v</ta>
            <ta e="T27" id="Seg_970" s="T26">pers</ta>
            <ta e="T28" id="Seg_971" s="T27">adv</ta>
            <ta e="T29" id="Seg_972" s="T28">v</ta>
            <ta e="T30" id="Seg_973" s="T29">post</ta>
            <ta e="T31" id="Seg_974" s="T30">v</ta>
            <ta e="T32" id="Seg_975" s="T31">adv</ta>
            <ta e="T33" id="Seg_976" s="T32">v</ta>
            <ta e="T34" id="Seg_977" s="T33">pers</ta>
            <ta e="T35" id="Seg_978" s="T34">propr</ta>
            <ta e="T36" id="Seg_979" s="T35">propr</ta>
            <ta e="T37" id="Seg_980" s="T36">cop</ta>
            <ta e="T38" id="Seg_981" s="T37">propr</ta>
            <ta e="T39" id="Seg_982" s="T38">n</ta>
            <ta e="T40" id="Seg_983" s="T39">propr</ta>
            <ta e="T41" id="Seg_984" s="T40">adv</ta>
            <ta e="T42" id="Seg_985" s="T41">propr</ta>
            <ta e="T43" id="Seg_986" s="T42">n</ta>
            <ta e="T44" id="Seg_987" s="T43">adv</ta>
            <ta e="T45" id="Seg_988" s="T44">propr</ta>
            <ta e="T46" id="Seg_989" s="T45">n</ta>
            <ta e="T47" id="Seg_990" s="T46">n</ta>
            <ta e="T48" id="Seg_991" s="T47">v</ta>
            <ta e="T49" id="Seg_992" s="T48">aux</ta>
            <ta e="T50" id="Seg_993" s="T49">aux</ta>
            <ta e="T51" id="Seg_994" s="T50">adv</ta>
            <ta e="T52" id="Seg_995" s="T51">propr</ta>
            <ta e="T53" id="Seg_996" s="T52">pers</ta>
            <ta e="T54" id="Seg_997" s="T53">n</ta>
            <ta e="T55" id="Seg_998" s="T54">n</ta>
            <ta e="T56" id="Seg_999" s="T55">v</ta>
            <ta e="T57" id="Seg_1000" s="T56">pers</ta>
            <ta e="T58" id="Seg_1001" s="T57">n</ta>
            <ta e="T59" id="Seg_1002" s="T58">emphpro</ta>
            <ta e="T60" id="Seg_1003" s="T59">n</ta>
            <ta e="T61" id="Seg_1004" s="T60">v</ta>
            <ta e="T62" id="Seg_1005" s="T61">v</ta>
            <ta e="T63" id="Seg_1006" s="T62">adv</ta>
            <ta e="T64" id="Seg_1007" s="T63">v</ta>
            <ta e="T65" id="Seg_1008" s="T64">v</ta>
            <ta e="T66" id="Seg_1009" s="T65">n</ta>
            <ta e="T67" id="Seg_1010" s="T66">adj</ta>
            <ta e="T68" id="Seg_1011" s="T67">n</ta>
            <ta e="T69" id="Seg_1012" s="T68">adv</ta>
            <ta e="T70" id="Seg_1013" s="T69">adj</ta>
            <ta e="T71" id="Seg_1014" s="T70">n</ta>
            <ta e="T72" id="Seg_1015" s="T71">v</ta>
            <ta e="T73" id="Seg_1016" s="T72">pers</ta>
            <ta e="T74" id="Seg_1017" s="T73">n</ta>
            <ta e="T75" id="Seg_1018" s="T74">v</ta>
            <ta e="T76" id="Seg_1019" s="T75">n</ta>
            <ta e="T77" id="Seg_1020" s="T76">v</ta>
            <ta e="T78" id="Seg_1021" s="T77">n</ta>
            <ta e="T79" id="Seg_1022" s="T78">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T12" id="Seg_1023" s="T11">np.h:A</ta>
            <ta e="T15" id="Seg_1024" s="T14">np.h:A</ta>
            <ta e="T21" id="Seg_1025" s="T20">pro.h:P</ta>
            <ta e="T26" id="Seg_1026" s="T25">0.3.h:A</ta>
            <ta e="T27" id="Seg_1027" s="T26">pro.h:Th</ta>
            <ta e="T31" id="Seg_1028" s="T30">0.3.h:A</ta>
            <ta e="T33" id="Seg_1029" s="T32">0.3.h:A</ta>
            <ta e="T34" id="Seg_1030" s="T33">pro.h:Th</ta>
            <ta e="T35" id="Seg_1031" s="T34">np.h:R</ta>
            <ta e="T36" id="Seg_1032" s="T35">np.h:Th</ta>
            <ta e="T43" id="Seg_1033" s="T42">np:L</ta>
            <ta e="T44" id="Seg_1034" s="T43">adv:Time</ta>
            <ta e="T45" id="Seg_1035" s="T44">np.h:A</ta>
            <ta e="T47" id="Seg_1036" s="T46">np:P</ta>
            <ta e="T52" id="Seg_1037" s="T51">np.h:A</ta>
            <ta e="T66" id="Seg_1038" s="T65">np:Th</ta>
            <ta e="T68" id="Seg_1039" s="T67">np.h:R</ta>
            <ta e="T71" id="Seg_1040" s="T70">np.h:R</ta>
            <ta e="T73" id="Seg_1041" s="T72">pro.h:A</ta>
            <ta e="T74" id="Seg_1042" s="T73">np:P</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T9" id="Seg_1043" s="T6">s:temp</ta>
            <ta e="T12" id="Seg_1044" s="T11">np.h:S</ta>
            <ta e="T15" id="Seg_1045" s="T14">np.h:S</ta>
            <ta e="T16" id="Seg_1046" s="T15">v:pred</ta>
            <ta e="T20" id="Seg_1047" s="T16">s:purp</ta>
            <ta e="T25" id="Seg_1048" s="T20">s:adv</ta>
            <ta e="T26" id="Seg_1049" s="T25">0.3.h:S v:pred</ta>
            <ta e="T27" id="Seg_1050" s="T26">pro.h:O</ta>
            <ta e="T30" id="Seg_1051" s="T27">s:temp</ta>
            <ta e="T31" id="Seg_1052" s="T30">0.3.h:S v:pred</ta>
            <ta e="T33" id="Seg_1053" s="T32">0.3.h:S v:pred</ta>
            <ta e="T34" id="Seg_1054" s="T33">pro.h:O</ta>
            <ta e="T36" id="Seg_1055" s="T35">np.h:S</ta>
            <ta e="T37" id="Seg_1056" s="T36">cop</ta>
            <ta e="T39" id="Seg_1057" s="T38">n:pred</ta>
            <ta e="T45" id="Seg_1058" s="T44">np.h:S</ta>
            <ta e="T47" id="Seg_1059" s="T46">np:O</ta>
            <ta e="T50" id="Seg_1060" s="T47">v:pred</ta>
            <ta e="T52" id="Seg_1061" s="T51">np.h:S</ta>
            <ta e="T58" id="Seg_1062" s="T52">s:temp</ta>
            <ta e="T62" id="Seg_1063" s="T58">s:temp</ta>
            <ta e="T64" id="Seg_1064" s="T63">v:pred</ta>
            <ta e="T65" id="Seg_1065" s="T64">s:rel</ta>
            <ta e="T66" id="Seg_1066" s="T65">np:O</ta>
            <ta e="T72" id="Seg_1067" s="T66">s:adv</ta>
            <ta e="T73" id="Seg_1068" s="T72">pro.h:S</ta>
            <ta e="T74" id="Seg_1069" s="T73">np:O</ta>
            <ta e="T75" id="Seg_1070" s="T74">v:pred</ta>
            <ta e="T79" id="Seg_1071" s="T75">s:adv</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T3" id="Seg_1072" s="T2">accs-gen</ta>
            <ta e="T4" id="Seg_1073" s="T3">new</ta>
            <ta e="T12" id="Seg_1074" s="T11">new</ta>
            <ta e="T15" id="Seg_1075" s="T14">new</ta>
            <ta e="T19" id="Seg_1076" s="T18">giv-active</ta>
            <ta e="T21" id="Seg_1077" s="T20">giv-active</ta>
            <ta e="T26" id="Seg_1078" s="T25">0.accs-aggr</ta>
            <ta e="T27" id="Seg_1079" s="T26">giv-active</ta>
            <ta e="T31" id="Seg_1080" s="T30">0.giv-active</ta>
            <ta e="T33" id="Seg_1081" s="T32">0.giv-active</ta>
            <ta e="T34" id="Seg_1082" s="T33">giv-active</ta>
            <ta e="T35" id="Seg_1083" s="T34">giv-inactive</ta>
            <ta e="T36" id="Seg_1084" s="T35">giv-active</ta>
            <ta e="T38" id="Seg_1085" s="T37">accs-gen</ta>
            <ta e="T40" id="Seg_1086" s="T39">accs-gen</ta>
            <ta e="T42" id="Seg_1087" s="T41">accs-gen</ta>
            <ta e="T45" id="Seg_1088" s="T44">accs-inf</ta>
            <ta e="T47" id="Seg_1089" s="T46">accs-gen</ta>
            <ta e="T52" id="Seg_1090" s="T51">accs-gen</ta>
            <ta e="T53" id="Seg_1091" s="T52">giv-inactive</ta>
            <ta e="T57" id="Seg_1092" s="T56">giv-active</ta>
            <ta e="T59" id="Seg_1093" s="T58">giv-active</ta>
            <ta e="T60" id="Seg_1094" s="T59">new</ta>
            <ta e="T66" id="Seg_1095" s="T65">new</ta>
            <ta e="T68" id="Seg_1096" s="T67">giv-inactive</ta>
            <ta e="T71" id="Seg_1097" s="T70">giv-inactive</ta>
            <ta e="T72" id="Seg_1098" s="T71">quot-sp</ta>
            <ta e="T73" id="Seg_1099" s="T72">giv-inactive-Q</ta>
            <ta e="T74" id="Seg_1100" s="T73">giv-inactive-Q</ta>
            <ta e="T79" id="Seg_1101" s="T77">giv-inactive-Q</ta>
         </annotation>
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T3" id="Seg_1102" s="T2">RUS:cult</ta>
            <ta e="T4" id="Seg_1103" s="T3">RUS:cult</ta>
            <ta e="T19" id="Seg_1104" s="T18">RUS:cult</ta>
            <ta e="T35" id="Seg_1105" s="T34">RUS:cult</ta>
            <ta e="T36" id="Seg_1106" s="T35">RUS:cult</ta>
            <ta e="T38" id="Seg_1107" s="T37">RUS:cult</ta>
            <ta e="T40" id="Seg_1108" s="T39">RUS:cult</ta>
            <ta e="T42" id="Seg_1109" s="T41">RUS:cult</ta>
            <ta e="T45" id="Seg_1110" s="T44">RUS:cult</ta>
            <ta e="T52" id="Seg_1111" s="T51">RUS:cult</ta>
            <ta e="T58" id="Seg_1112" s="T57">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T58" id="Seg_1113" s="T57">Csub fortition</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T3" id="Seg_1114" s="T2">dir:bare</ta>
            <ta e="T4" id="Seg_1115" s="T3">dir:bare</ta>
            <ta e="T19" id="Seg_1116" s="T18">dir:bare</ta>
            <ta e="T35" id="Seg_1117" s="T34">dir:infl</ta>
            <ta e="T36" id="Seg_1118" s="T35">dir:bare</ta>
            <ta e="T38" id="Seg_1119" s="T37">dir:infl</ta>
            <ta e="T40" id="Seg_1120" s="T39">dir:bare</ta>
            <ta e="T42" id="Seg_1121" s="T41">dir:bare</ta>
            <ta e="T45" id="Seg_1122" s="T44">dir:infl</ta>
            <ta e="T52" id="Seg_1123" s="T51">dir:bare</ta>
            <ta e="T58" id="Seg_1124" s="T57">indir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T2" id="Seg_1125" s="T0">The next story:</ta>
            <ta e="T6" id="Seg_1126" s="T2">Jesus stands in front of Pilate.</ta>
            <ta e="T20" id="Seg_1127" s="T6">When morning had broken, all high clergymen and old leaders gathered in order to take counsel about Jesus.</ta>
            <ta e="T26" id="Seg_1128" s="T20">They came to the decision to sentence him to death.</ta>
            <ta e="T35" id="Seg_1129" s="T26">Having tied him up strongly, they brought him and gave him to Pontius Pilate.</ta>
            <ta e="T43" id="Seg_1130" s="T35">Pilate was the governer from Rome, in the provinces of Judea and Samaria.</ta>
            <ta e="T50" id="Seg_1131" s="T43">At that time the Romans had conquered the Holy Land.</ta>
            <ta e="T66" id="Seg_1132" s="T50">When Judas saw him - the person whom he sentenced to death, the person whom he had betrayed - he recognized that he himself had done wrong and he gave back the money which he had received.</ta>
            <ta e="T72" id="Seg_1133" s="T66">Saying to the high clergymen and old leaders:</ta>
            <ta e="T79" id="Seg_1134" s="T72">"I have committed a sin, giving an innocent person to the death."</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T2" id="Seg_1135" s="T0">Die nächste Geschichte:</ta>
            <ta e="T6" id="Seg_1136" s="T2">Jesus steht vor Pilatus.</ta>
            <ta e="T20" id="Seg_1137" s="T6">Als es Morgen geworden war, versammelten sich alle hohen Geistlichen und alten Anführer, um sich über Jesus zu besprechen.</ta>
            <ta e="T26" id="Seg_1138" s="T20">Sie kamen zu dem Entschluss, ihm zum Tode zu verurteilen.</ta>
            <ta e="T35" id="Seg_1139" s="T26">Nachdem sie ihn festgebunden hatten, brachten sie ihn und übergaben ihn dort an Pontius Pilatus.</ta>
            <ta e="T43" id="Seg_1140" s="T35">Pilatus war Statthalter aus Rom, in den Provinzen Judäa und Samaria.</ta>
            <ta e="T50" id="Seg_1141" s="T43">Damals hatten die Römer des Heilige Land erorbert.</ta>
            <ta e="T66" id="Seg_1142" s="T50">Als Juda ihn, den er zum Tode verurteilt hat, sah, den von ihm Verratenen, bemerkte er, dass er selber Unrecht getan hatte und gab sein erhaltenes Geld zurück.</ta>
            <ta e="T72" id="Seg_1143" s="T66">Den hohen Geistlichen und alten Anführeren sagend: </ta>
            <ta e="T79" id="Seg_1144" s="T72">"Ich habe eine Sünde begangen, ich habe einen Unschuldigen dem Tode gegeben."</ta>
         </annotation>
         <annotation name="fr" tierref="fr" />
         <annotation name="ltr" tierref="ltr">
            <ta e="T2" id="Seg_1145" s="T0">Следующий рассказ: </ta>
            <ta e="T6" id="Seg_1146" s="T2">Иисус перед Пилатом нахождение.</ta>
            <ta e="T20" id="Seg_1147" s="T6">Когда утро наступило, все верховные и старые советчики провели собрание, рассказ делая (обсудив) об Иисусе.</ta>
            <ta e="T26" id="Seg_1148" s="T20">Его на смерть отдать, думая, посоветовали.</ta>
            <ta e="T35" id="Seg_1149" s="T26">Его сильно обмотав, увели, там отдали его к Понтию Пилату.</ta>
            <ta e="T43" id="Seg_1150" s="T35">Пилат был из Рима начальник – Иудея и Самария в землях.</ta>
            <ta e="T50" id="Seg_1151" s="T43">Тогда римляне божью землю, отобрав, забрали.</ta>
            <ta e="T66" id="Seg_1152" s="T50">Тогда Иуда его к смерти отдающего увидев, его судимого, сам плохие дела совершившего узнав, вернул получившие деньги.</ta>
            <ta e="T72" id="Seg_1153" s="T66">Первосвященникам и старым советующим сказал: </ta>
            <ta e="T79" id="Seg_1154" s="T72">"Я грех совершил, смерти придав не виновного".</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
