<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID8AF36E40-0467-9A06-9EFA-A0DC84CC432B">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>PoNA_1994_JesusIsArrested_transl</transcription-name>
         <referenced-file url="PoNA_19940110_JesusIsArrested_transl.wav" />
         <referenced-file url="PoNA_19940110_JesusIsArrested_transl.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\transl\PoNA_19940110_JesusIsArrested_transl\PoNA_19940110_JesusIsArrested_transl.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">309</ud-information>
            <ud-information attribute-name="# HIAT:w">230</ud-information>
            <ud-information attribute-name="# e">230</ud-information>
            <ud-information attribute-name="# HIAT:u">37</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PoNA">
            <abbreviation>PoNA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="1.0079999999999814" type="appl" />
         <tli id="T2" time="2.016999999999996" type="appl" />
         <tli id="T3" time="3.0249999999999773" type="appl" />
         <tli id="T4" time="4.0330000000000155" type="appl" />
         <tli id="T5" time="5.04200000000003" type="appl" />
         <tli id="T6" time="6.050000000000011" type="appl" />
         <tli id="T7" time="7.057999999999993" type="appl" />
         <tli id="T8" time="8.067000000000007" type="appl" />
         <tli id="T9" time="9.074999999999989" type="appl" />
         <tli id="T10" time="9.940999999999974" type="appl" />
         <tli id="T11" time="10.807999999999993" type="appl" />
         <tli id="T12" time="11.673999999999978" type="appl" />
         <tli id="T13" time="12.333000000000027" type="appl" />
         <tli id="T14" time="12.992000000000019" type="appl" />
         <tli id="T15" time="13.65100000000001" type="appl" />
         <tli id="T16" time="14.310000000000002" type="appl" />
         <tli id="T17" time="14.968999999999994" type="appl" />
         <tli id="T18" time="15.627999999999986" type="appl" />
         <tli id="T19" time="16.286999999999978" type="appl" />
         <tli id="T20" time="17.237000000000023" type="appl" />
         <tli id="T21" time="18.18599999999998" type="appl" />
         <tli id="T22" time="19.136000000000024" type="appl" />
         <tli id="T23" time="20.086000000000013" type="appl" />
         <tli id="T24" time="21.036" type="appl" />
         <tli id="T25" time="21.985000000000014" type="appl" />
         <tli id="T26" time="22.935000000000002" type="appl" />
         <tli id="T27" time="23.706999999999994" type="appl" />
         <tli id="T28" time="24.478999999999985" type="appl" />
         <tli id="T29" time="25.25200000000001" type="appl" />
         <tli id="T30" time="26.024" type="appl" />
         <tli id="T31" time="26.845739832907547" />
         <tli id="T32" time="31.305585859283298" />
         <tli id="T33" />
         <tli id="T34" />
         <tli id="T35" />
         <tli id="T36" />
         <tli id="T37" />
         <tli id="T38" />
         <tli id="T39" />
         <tli id="T40" />
         <tli id="T41" time="31.385583097424565" />
         <tli id="T42" time="32.3922150107022" />
         <tli id="T43" time="32.793000000000006" type="appl" />
         <tli id="T44" time="33.521000000000015" type="appl" />
         <tli id="T45" time="34.249000000000024" type="appl" />
         <tli id="T46" time="34.976999999999975" type="appl" />
         <tli id="T47" time="35.704999999999984" type="appl" />
         <tli id="T48" time="36.43299999999999" type="appl" />
         <tli id="T49" time="37.161" type="appl" />
         <tli id="T50" time="37.88900000000001" type="appl" />
         <tli id="T51" time="38.61700000000002" type="appl" />
         <tli id="T52" time="39.34500000000003" type="appl" />
         <tli id="T53" time="40.65199999999999" type="appl" />
         <tli id="T54" time="41.95800000000003" type="appl" />
         <tli id="T55" time="42.65100000000001" type="appl" />
         <tli id="T56" time="43.343999999999994" type="appl" />
         <tli id="T57" time="44.03800000000001" type="appl" />
         <tli id="T58" time="44.730999999999995" type="appl" />
         <tli id="T59" time="45.42399999999998" type="appl" />
         <tli id="T60" time="46.18799999999999" type="appl" />
         <tli id="T61" time="46.952" type="appl" />
         <tli id="T62" time="47.716999999999985" type="appl" />
         <tli id="T63" time="48.480999999999995" type="appl" />
         <tli id="T64" time="49.245000000000005" type="appl" />
         <tli id="T65" time="50.00999999999999" type="appl" />
         <tli id="T66" time="50.774" type="appl" />
         <tli id="T67" time="51.53800000000001" type="appl" />
         <tli id="T68" time="52.30200000000002" type="appl" />
         <tli id="T69" time="53.065999999999974" type="appl" />
         <tli id="T70" time="53.83100000000002" type="appl" />
         <tli id="T71" time="54.59500000000003" type="appl" />
         <tli id="T72" time="55.309000000000026" type="appl" />
         <tli id="T73" time="56.023000000000025" type="appl" />
         <tli id="T74" time="56.738" type="appl" />
         <tli id="T75" time="57.452" type="appl" />
         <tli id="T76" time="58.166" type="appl" />
         <tli id="T77" time="58.879999999999995" type="appl" />
         <tli id="T78" time="59.593999999999994" type="appl" />
         <tli id="T79" time="60.309000000000026" type="appl" />
         <tli id="T80" time="61.023000000000025" type="appl" />
         <tli id="T81" time="61.73700000000002" type="appl" />
         <tli id="T82" time="62.45100000000002" type="appl" />
         <tli id="T83" time="63.166" type="appl" />
         <tli id="T84" time="63.879999999999995" type="appl" />
         <tli id="T85" time="64.594" type="appl" />
         <tli id="T86" time="65.27800000000002" type="appl" />
         <tli id="T87" time="65.96300000000002" type="appl" />
         <tli id="T88" time="66.64800000000002" type="appl" />
         <tli id="T89" time="67.332" type="appl" />
         <tli id="T90" time="68.01600000000002" type="appl" />
         <tli id="T91" time="68.70100000000002" type="appl" />
         <tli id="T92" time="69.47800000000001" type="appl" />
         <tli id="T93" time="70.25600000000003" type="appl" />
         <tli id="T94" time="71.03399999999999" type="appl" />
         <tli id="T95" time="71.81099999999998" type="appl" />
         <tli id="T96" time="72.58800000000002" type="appl" />
         <tli id="T97" time="73.36599999999999" type="appl" />
         <tli id="T98" time="74.60199999999998" type="appl" />
         <tli id="T99" time="75.83699999999999" type="appl" />
         <tli id="T100" time="77.072" type="appl" />
         <tli id="T101" time="78.30799999999999" type="appl" />
         <tli id="T102" time="79.54399999999998" type="appl" />
         <tli id="T103" time="80.779" type="appl" />
         <tli id="T104" time="81.16199999999998" type="appl" />
         <tli id="T105" time="81.54399999999998" type="appl" />
         <tli id="T106" time="81.92599999999999" type="appl" />
         <tli id="T107" time="82.30900000000003" type="appl" />
         <tli id="T108" time="84.77707312020989" />
         <tli id="T109" time="88.1369571221432" />
         <tli id="T110" />
         <tli id="T111" />
         <tli id="T112" />
         <tli id="T113" time="88.15695643167852" />
         <tli id="T114" time="88.74000000000001" type="appl" />
         <tli id="T115" time="89.25200000000001" type="appl" />
         <tli id="T116" time="89.76400000000001" type="appl" />
         <tli id="T117" time="90.27600000000001" type="appl" />
         <tli id="T118" time="91.05000000000001" type="appl" />
         <tli id="T119" time="91.82400000000001" type="appl" />
         <tli id="T120" time="92.59800000000001" type="appl" />
         <tli id="T121" time="93.37099999999998" type="appl" />
         <tli id="T122" time="94.14499999999998" type="appl" />
         <tli id="T123" time="94.91672305461576" />
         <tli id="T124" time="101.68982255057652" />
         <tli id="T125" />
         <tli id="T126" />
         <tli id="T127" />
         <tli id="T128" />
         <tli id="T129" time="102.04981012221225" />
         <tli id="T130" time="102.58499999999998" type="appl" />
         <tli id="T131" time="103.36700000000002" type="appl" />
         <tli id="T132" time="104.149" type="appl" />
         <tli id="T133" time="104.65638679831527" />
         <tli id="T134" time="107.1429676172064" />
         <tli id="T135" />
         <tli id="T136" />
         <tli id="T137" time="107.8896085065249" />
         <tli id="T138" time="112.00946627080026" />
         <tli id="T139" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" />
         <tli id="T143" />
         <tli id="T144" time="112.56400000000002" type="appl" />
         <tli id="T145" time="113.32600000000002" type="appl" />
         <tli id="T146" time="114.08800000000002" type="appl" />
         <tli id="T147" time="114.851" type="appl" />
         <tli id="T148" time="115.613" type="appl" />
         <tli id="T149" time="116.375" type="appl" />
         <tli id="T150" time="117.4092798453359" />
         <tli id="T151" time="119.36254574328522" />
         <tli id="T152" time="119.73586618794448" />
         <tli id="T153" time="120.31" type="appl" />
         <tli id="T154" time="121.72246426845267" />
         <tli id="T155" time="124.78235862735622" />
         <tli id="T156" time="124.80235793689153" />
         <tli id="T157" time="125.66000000000003" type="appl" />
         <tli id="T158" time="126.72800000000001" type="appl" />
         <tli id="T159" time="127.78892149416558" />
         <tli id="T160" time="129.1888731616378" />
         <tli id="T161" />
         <tli id="T162" time="129.721" type="appl" />
         <tli id="T163" time="130.598" type="appl" />
         <tli id="T164" time="131.476" type="appl" />
         <tli id="T165" time="132.353" type="appl" />
         <tli id="T166" time="133.23000000000002" type="appl" />
         <tli id="T167" time="134.10700000000003" type="appl" />
         <tli id="T168" time="134.98399999999998" type="appl" />
         <tli id="T169" time="135.86200000000002" type="appl" />
         <tli id="T170" time="136.73899999999998" type="appl" />
         <tli id="T171" time="137.61599999999999" type="appl" />
         <tli id="T172" time="138.29200000000003" type="appl" />
         <tli id="T173" time="138.96800000000002" type="appl" />
         <tli id="T174" time="139.64300000000003" type="appl" />
         <tli id="T175" time="140.31900000000002" type="appl" />
         <tli id="T176" time="141.57999999999998" type="appl" />
         <tli id="T177" time="142.83999999999997" type="appl" />
         <tli id="T178" time="144.101" type="appl" />
         <tli id="T179" time="145.36200000000002" type="appl" />
         <tli id="T180" time="146.622" type="appl" />
         <tli id="T181" time="147.88299999999998" type="appl" />
         <tli id="T182" time="149.144" type="appl" />
         <tli id="T183" time="150.40500000000003" type="appl" />
         <tli id="T184" time="151.66500000000002" type="appl" />
         <tli id="T185" time="152.926" type="appl" />
         <tli id="T186" time="153.40699999999998" type="appl" />
         <tli id="T187" time="153.887" type="appl" />
         <tli id="T188" time="154.368" type="appl" />
         <tli id="T189" time="154.848" type="appl" />
         <tli id="T190" time="155.329" type="appl" />
         <tli id="T191" time="155.81" type="appl" />
         <tli id="T192" time="156.29000000000002" type="appl" />
         <tli id="T193" time="156.77100000000002" type="appl" />
         <tli id="T194" time="157.25100000000003" type="appl" />
         <tli id="T195" time="157.73200000000003" type="appl" />
         <tli id="T196" time="158.21200000000005" type="appl" />
         <tli id="T197" time="158.69300000000004" type="appl" />
         <tli id="T198" time="159.56400000000002" type="appl" />
         <tli id="T199" time="160.43400000000003" type="appl" />
         <tli id="T200" time="161.305" type="appl" />
         <tli id="T201" time="162.176" type="appl" />
         <tli id="T202" time="163.497" type="appl" />
         <tli id="T203" time="164.81699999999995" type="appl" />
         <tli id="T204" time="166.13799999999998" type="appl" />
         <tli id="T205" time="167.45800000000003" type="appl" />
         <tli id="T206" time="168.77900000000005" type="appl" />
         <tli id="T207" time="170.09999999999997" type="appl" />
         <tli id="T208" time="171.42000000000002" type="appl" />
         <tli id="T209" time="172.74100000000004" type="appl" />
         <tli id="T210" time="173.878" type="appl" />
         <tli id="T211" time="175.01399999999995" type="appl" />
         <tli id="T212" time="176.151" type="appl" />
         <tli id="T213" time="177.28799999999995" type="appl" />
         <tli id="T214" time="178.13299999999998" type="appl" />
         <tli id="T215" time="178.97899999999998" type="appl" />
         <tli id="T216" time="179.824" type="appl" />
         <tli id="T217" time="180.66900000000004" type="appl" />
         <tli id="T218" time="181.51399999999995" type="appl" />
         <tli id="T219" time="182.35999999999996" type="appl" />
         <tli id="T220" time="183.20499999999998" type="appl" />
         <tli id="T221" time="184.05" type="appl" />
         <tli id="T222" time="184.89500000000004" type="appl" />
         <tli id="T223" time="185.74100000000004" type="appl" />
         <tli id="T224" time="186.58599999999996" type="appl" />
         <tli id="T225" time="187.51500000000004" type="appl" />
         <tli id="T226" time="188.44400000000002" type="appl" />
         <tli id="T227" time="189.373" type="appl" />
         <tli id="T228" time="190.30199999999996" type="appl" />
         <tli id="T229" time="191.23100000000005" type="appl" />
         <tli id="T230" time="192.16000000000003" type="appl" />
         <tli id="T231" time="193.089" type="appl" />
         <tli id="T232" time="193.1" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PoNA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T231" id="Seg_0" n="sc" s="T0">
               <ts e="T9" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Ogolorgo</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">aːgabɨt</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">östörü</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">kinigetten</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_15" n="HIAT:ip">"</nts>
                  <ts e="T5" id="Seg_17" n="HIAT:w" s="T4">Isuːs</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">Xrʼistos</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_22" n="HIAT:ip">–</nts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">ogolor</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">dogordoro</ts>
                  <nts id="Seg_29" n="HIAT:ip">"</nts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">di͡en</ts>
                  <nts id="Seg_33" n="HIAT:ip">.</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_36" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_38" n="HIAT:w" s="T9">Isuːhu</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_41" n="HIAT:w" s="T10">tutan</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">ɨlbɨttar</ts>
                  <nts id="Seg_45" n="HIAT:ip">.</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_48" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_50" n="HIAT:w" s="T12">Üste</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_53" n="HIAT:w" s="T13">kat</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">Isuːs</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">kele</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_62" n="HIAT:w" s="T16">hɨldʼɨbɨt</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">ü͡öreter</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">kihileriger</ts>
                  <nts id="Seg_69" n="HIAT:ip">.</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_72" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_74" n="HIAT:w" s="T19">Onno</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_77" n="HIAT:w" s="T20">bulaːt</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_80" n="HIAT:w" s="T21">ebit</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_83" n="HIAT:w" s="T22">ginileri</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_86" n="HIAT:w" s="T23">körbüt</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_89" n="HIAT:w" s="T24">huptu</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_92" n="HIAT:w" s="T25">utujallarɨ</ts>
                  <nts id="Seg_93" n="HIAT:ip">.</nts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_96" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_98" n="HIAT:w" s="T26">Ühüs</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_101" n="HIAT:w" s="T27">keliːtiger</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">gini</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_107" n="HIAT:w" s="T29">haŋarbɨt</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_110" n="HIAT:w" s="T30">ginilerge</ts>
                  <nts id="Seg_111" n="HIAT:ip">:</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_114" n="HIAT:u" s="T31">
                  <nts id="Seg_115" n="HIAT:ip">"</nts>
                  <ts e="T32" id="Seg_117" n="HIAT:w" s="T31">Ehigi</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_120" n="HIAT:w" s="T32">anɨga</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_123" n="HIAT:w" s="T33">di͡eri</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_126" n="HIAT:w" s="T34">anɨ</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_129" n="HIAT:w" s="T35">össü͡ö</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_132" n="HIAT:w" s="T36">utuja</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_135" n="HIAT:w" s="T37">hɨtagɨt</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_138" n="HIAT:w" s="T38">du͡o</ts>
                  <nts id="Seg_139" n="HIAT:ip">,</nts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_142" n="HIAT:w" s="T39">nalɨjan</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_145" n="HIAT:w" s="T40">baraːn</ts>
                  <nts id="Seg_146" n="HIAT:ip">?</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T52" id="Seg_149" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_151" n="HIAT:w" s="T41">Dʼe</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_154" n="HIAT:w" s="T42">čugahaːta</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_157" n="HIAT:w" s="T43">čaːs</ts>
                  <nts id="Seg_158" n="HIAT:ip">,</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_161" n="HIAT:w" s="T44">kihitten</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_164" n="HIAT:w" s="T45">u͡ol</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_167" n="HIAT:w" s="T46">bi͡eriller</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_170" n="HIAT:w" s="T47">anʼɨːlarɨ</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_173" n="HIAT:w" s="T48">oŋorollor</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_175" n="HIAT:ip">(</nts>
                  <ts e="T50" id="Seg_177" n="HIAT:w" s="T49">kihi</ts>
                  <nts id="Seg_178" n="HIAT:ip">)</nts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_181" n="HIAT:w" s="T50">kihi</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_184" n="HIAT:w" s="T51">aːjɨ</ts>
                  <nts id="Seg_185" n="HIAT:ip">.</nts>
                  <nts id="Seg_186" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_188" n="HIAT:u" s="T52">
                  <ts e="T53" id="Seg_190" n="HIAT:w" s="T52">Turuŋ</ts>
                  <nts id="Seg_191" n="HIAT:ip">,</nts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_194" n="HIAT:w" s="T53">barɨ͡agɨŋ</ts>
                  <nts id="Seg_195" n="HIAT:ip">.</nts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T59" id="Seg_198" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_200" n="HIAT:w" s="T54">Dʼe</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_203" n="HIAT:w" s="T55">čugahaːta</ts>
                  <nts id="Seg_204" n="HIAT:ip">,</nts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_207" n="HIAT:w" s="T56">minigin</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_210" n="HIAT:w" s="T57">ölüːge</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_213" n="HIAT:w" s="T58">bi͡eriːhi</ts>
                  <nts id="Seg_214" n="HIAT:ip">.</nts>
                  <nts id="Seg_215" n="HIAT:ip">"</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T71" id="Seg_218" n="HIAT:u" s="T59">
                  <ts e="T60" id="Seg_220" n="HIAT:w" s="T59">Gini</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_223" n="HIAT:w" s="T60">itigirdik</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_226" n="HIAT:w" s="T61">haŋara</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_229" n="HIAT:w" s="T62">turdagɨna</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_232" n="HIAT:w" s="T63">kelbit</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_235" n="HIAT:w" s="T64">Juːda</ts>
                  <nts id="Seg_236" n="HIAT:ip">,</nts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_239" n="HIAT:w" s="T65">iti</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_242" n="HIAT:w" s="T66">biːr</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_245" n="HIAT:w" s="T67">ü͡öreter</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_248" n="HIAT:w" s="T68">kihite</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_251" n="HIAT:w" s="T69">u͡on</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_254" n="HIAT:w" s="T70">ikkitten</ts>
                  <nts id="Seg_255" n="HIAT:ip">.</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T85" id="Seg_258" n="HIAT:u" s="T71">
                  <ts e="T72" id="Seg_260" n="HIAT:w" s="T71">Giniː</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_263" n="HIAT:w" s="T72">bastɨŋ</ts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_266" n="HIAT:w" s="T73">agabɨttarga</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_269" n="HIAT:w" s="T74">bi͡erbit</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_272" n="HIAT:w" s="T75">ebit</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_275" n="HIAT:w" s="T76">tɨlɨn</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_278" n="HIAT:w" s="T77">otut</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_281" n="HIAT:w" s="T78">kömüs</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_284" n="HIAT:w" s="T79">karčɨlar</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_287" n="HIAT:w" s="T80">ihin</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_290" n="HIAT:w" s="T81">Isuːhu</ts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_293" n="HIAT:w" s="T82">ölüːge</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_296" n="HIAT:w" s="T83">bi͡eri͡em</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_299" n="HIAT:w" s="T84">di͡en</ts>
                  <nts id="Seg_300" n="HIAT:ip">.</nts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_303" n="HIAT:u" s="T85">
                  <ts e="T86" id="Seg_305" n="HIAT:w" s="T85">Juːdanɨ</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_308" n="HIAT:w" s="T86">gɨtta</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_311" n="HIAT:w" s="T87">kelbittere</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_314" n="HIAT:w" s="T88">elbek</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_317" n="HIAT:w" s="T89">heriː</ts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_320" n="HIAT:w" s="T90">dʼono</ts>
                  <nts id="Seg_321" n="HIAT:ip">.</nts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_324" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_326" n="HIAT:w" s="T91">Onno</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_329" n="HIAT:w" s="T92">bütte</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_332" n="HIAT:w" s="T93">kihiler</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_335" n="HIAT:w" s="T94">farʼisʼejdartan</ts>
                  <nts id="Seg_336" n="HIAT:ip">,</nts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_339" n="HIAT:w" s="T95">bastɨŋ</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_342" n="HIAT:w" s="T96">agabɨttartan</ts>
                  <nts id="Seg_343" n="HIAT:ip">.</nts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T103" id="Seg_346" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_348" n="HIAT:w" s="T97">Iliːleriger</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_351" n="HIAT:w" s="T98">holuːrdaːktar</ts>
                  <nts id="Seg_352" n="HIAT:ip">,</nts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_355" n="HIAT:w" s="T99">batɨjɨlaːktar</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_358" n="HIAT:w" s="T100">onno</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_361" n="HIAT:w" s="T101">öröhö</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_364" n="HIAT:w" s="T102">mastaːktar</ts>
                  <nts id="Seg_365" n="HIAT:ip">.</nts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_368" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_370" n="HIAT:w" s="T103">Ölüːge</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_373" n="HIAT:w" s="T104">bi͡erer</ts>
                  <nts id="Seg_374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_376" n="HIAT:w" s="T105">Juːda</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_379" n="HIAT:w" s="T106">ginilerge</ts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_382" n="HIAT:w" s="T107">di͡ebit</ts>
                  <nts id="Seg_383" n="HIAT:ip">:</nts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T113" id="Seg_386" n="HIAT:u" s="T108">
                  <nts id="Seg_387" n="HIAT:ip">"</nts>
                  <ts e="T109" id="Seg_389" n="HIAT:w" s="T108">Kimi</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_392" n="HIAT:w" s="T109">min</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_395" n="HIAT:w" s="T110">uguru͡om</ts>
                  <nts id="Seg_396" n="HIAT:ip">,</nts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_399" n="HIAT:w" s="T111">ol</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_402" n="HIAT:w" s="T112">bu͡olu͡oga</ts>
                  <nts id="Seg_403" n="HIAT:ip">.</nts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T117" id="Seg_406" n="HIAT:u" s="T113">
                  <ts e="T114" id="Seg_408" n="HIAT:w" s="T113">Oččogo</ts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_411" n="HIAT:w" s="T114">onu</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_414" n="HIAT:w" s="T115">kaban</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_417" n="HIAT:w" s="T116">ɨlɨŋ</ts>
                  <nts id="Seg_418" n="HIAT:ip">"</nts>
                  <nts id="Seg_419" n="HIAT:ip">.</nts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_422" n="HIAT:u" s="T117">
                  <ts e="T118" id="Seg_424" n="HIAT:w" s="T117">Gini</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_427" n="HIAT:w" s="T118">kelbit</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_430" n="HIAT:w" s="T119">Isuːs</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_433" n="HIAT:w" s="T120">attɨgar</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_436" n="HIAT:w" s="T121">onno</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_439" n="HIAT:w" s="T122">di͡ebit</ts>
                  <nts id="Seg_440" n="HIAT:ip">:</nts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T129" id="Seg_443" n="HIAT:u" s="T123">
                  <nts id="Seg_444" n="HIAT:ip">"</nts>
                  <ts e="T124" id="Seg_446" n="HIAT:w" s="T123">Ü͡ör</ts>
                  <nts id="Seg_447" n="HIAT:ip">,</nts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_450" n="HIAT:w" s="T124">učʼital</ts>
                  <nts id="Seg_451" n="HIAT:ip">"</nts>
                  <nts id="Seg_452" n="HIAT:ip">,</nts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_455" n="HIAT:w" s="T125">itigirdik</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_458" n="HIAT:w" s="T126">haŋaraːt</ts>
                  <nts id="Seg_459" n="HIAT:ip">,</nts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_462" n="HIAT:w" s="T127">uguraːbɨt</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_465" n="HIAT:w" s="T128">ginini</ts>
                  <nts id="Seg_466" n="HIAT:ip">.</nts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_469" n="HIAT:u" s="T129">
                  <ts e="T130" id="Seg_471" n="HIAT:w" s="T129">Isuːs</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_474" n="HIAT:w" s="T130">bu͡ollagɨna</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_477" n="HIAT:w" s="T131">haŋarbɨt</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_480" n="HIAT:w" s="T132">gini͡eke</ts>
                  <nts id="Seg_481" n="HIAT:ip">:</nts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T137" id="Seg_484" n="HIAT:u" s="T133">
                  <nts id="Seg_485" n="HIAT:ip">"</nts>
                  <ts e="T134" id="Seg_487" n="HIAT:w" s="T133">Dogor</ts>
                  <nts id="Seg_488" n="HIAT:ip">,</nts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_491" n="HIAT:w" s="T134">togo</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_494" n="HIAT:w" s="T135">en</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_497" n="HIAT:w" s="T136">kelliŋ</ts>
                  <nts id="Seg_498" n="HIAT:ip">?</nts>
                  <nts id="Seg_499" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T143" id="Seg_501" n="HIAT:u" s="T137">
                  <ts e="T138" id="Seg_503" n="HIAT:w" s="T137">Uguraːŋŋɨn</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_506" n="HIAT:w" s="T138">du</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_509" n="HIAT:w" s="T139">bi͡eregin</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_512" n="HIAT:w" s="T140">ölüːge</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_515" n="HIAT:w" s="T141">kihitten</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_518" n="HIAT:w" s="T142">u͡olu</ts>
                  <nts id="Seg_519" n="HIAT:ip">"</nts>
                  <nts id="Seg_520" n="HIAT:ip">.</nts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T150" id="Seg_523" n="HIAT:u" s="T143">
                  <ts e="T144" id="Seg_525" n="HIAT:w" s="T143">Isuːs</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_528" n="HIAT:w" s="T144">ɨjɨppɨt</ts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_531" n="HIAT:w" s="T145">Juːdanɨ</ts>
                  <nts id="Seg_532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_534" n="HIAT:w" s="T146">gɨtta</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_537" n="HIAT:w" s="T147">kelbit</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_540" n="HIAT:w" s="T148">heriː</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_543" n="HIAT:w" s="T149">dʼonuttan</ts>
                  <nts id="Seg_544" n="HIAT:ip">:</nts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_547" n="HIAT:u" s="T150">
                  <nts id="Seg_548" n="HIAT:ip">"</nts>
                  <ts e="T151" id="Seg_550" n="HIAT:w" s="T150">Kimi</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_553" n="HIAT:w" s="T151">kördüːgüt</ts>
                  <nts id="Seg_554" n="HIAT:ip">?</nts>
                  <nts id="Seg_555" n="HIAT:ip">"</nts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_558" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_560" n="HIAT:w" s="T152">Ontulara</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_563" n="HIAT:w" s="T153">di͡ebitter</ts>
                  <nts id="Seg_564" n="HIAT:ip">:</nts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_567" n="HIAT:u" s="T154">
                  <nts id="Seg_568" n="HIAT:ip">"</nts>
                  <ts e="T155" id="Seg_570" n="HIAT:w" s="T154">Nazarʼejtan</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_573" n="HIAT:w" s="T155">Isuːhu</ts>
                  <nts id="Seg_574" n="HIAT:ip">"</nts>
                  <nts id="Seg_575" n="HIAT:ip">.</nts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T159" id="Seg_578" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_580" n="HIAT:w" s="T156">Küseːjin</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_583" n="HIAT:w" s="T157">haŋarbɨt</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_586" n="HIAT:w" s="T158">ginilerge</ts>
                  <nts id="Seg_587" n="HIAT:ip">:</nts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_590" n="HIAT:u" s="T159">
                  <nts id="Seg_591" n="HIAT:ip">"</nts>
                  <ts e="T160" id="Seg_593" n="HIAT:w" s="T159">Iti</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_596" n="HIAT:w" s="T160">min</ts>
                  <nts id="Seg_597" n="HIAT:ip">"</nts>
                  <nts id="Seg_598" n="HIAT:ip">.</nts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T171" id="Seg_601" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_603" n="HIAT:w" s="T161">Itini</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_606" n="HIAT:w" s="T162">isteːt</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_609" n="HIAT:w" s="T163">heriː</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_612" n="HIAT:w" s="T164">dʼono</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_615" n="HIAT:w" s="T165">kennilerinen</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_618" n="HIAT:w" s="T166">kaːmpɨttar</ts>
                  <nts id="Seg_619" n="HIAT:ip">,</nts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_622" n="HIAT:w" s="T167">onno</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_625" n="HIAT:w" s="T168">hirge</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_628" n="HIAT:w" s="T169">tüŋneri</ts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_631" n="HIAT:w" s="T170">tühetteːbitter</ts>
                  <nts id="Seg_632" n="HIAT:ip">.</nts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T175" id="Seg_635" n="HIAT:u" s="T171">
                  <ts e="T172" id="Seg_637" n="HIAT:w" s="T171">Onton</ts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_640" n="HIAT:w" s="T172">Isuːs</ts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_643" n="HIAT:w" s="T173">haŋarbɨt</ts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_646" n="HIAT:w" s="T174">ginilerge</ts>
                  <nts id="Seg_647" n="HIAT:ip">:</nts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T185" id="Seg_650" n="HIAT:u" s="T175">
                  <nts id="Seg_651" n="HIAT:ip">"</nts>
                  <ts e="T176" id="Seg_653" n="HIAT:w" s="T175">Ehigi</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_656" n="HIAT:w" s="T176">ölörü͡öksüttüː</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_659" n="HIAT:w" s="T177">kördüːr</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_662" n="HIAT:w" s="T178">kördük</ts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_665" n="HIAT:w" s="T179">tagɨstɨgɨt</ts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_668" n="HIAT:w" s="T180">batɨjalaːk</ts>
                  <nts id="Seg_669" n="HIAT:ip">,</nts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_672" n="HIAT:w" s="T181">öröhö</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_675" n="HIAT:w" s="T182">mastaːk</ts>
                  <nts id="Seg_676" n="HIAT:ip">,</nts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_679" n="HIAT:w" s="T183">minigin</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_682" n="HIAT:w" s="T184">tutaːrɨ</ts>
                  <nts id="Seg_683" n="HIAT:ip">.</nts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T197" id="Seg_686" n="HIAT:u" s="T185">
                  <ts e="T186" id="Seg_688" n="HIAT:w" s="T185">Kün</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_691" n="HIAT:w" s="T186">aːjɨ</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_694" n="HIAT:w" s="T187">min</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_697" n="HIAT:w" s="T188">baːr</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_700" n="HIAT:w" s="T189">etim</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_703" n="HIAT:w" s="T190">ehigini</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_706" n="HIAT:w" s="T191">gɨtta</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_709" n="HIAT:w" s="T192">ü͡örete</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_711" n="HIAT:ip">(</nts>
                  <ts e="T195" id="Seg_713" n="HIAT:w" s="T193">ta-</ts>
                  <nts id="Seg_714" n="HIAT:ip">)</nts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_717" n="HIAT:w" s="T195">taŋara</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_720" n="HIAT:w" s="T196">dʼi͡etiger</ts>
                  <nts id="Seg_721" n="HIAT:ip">.</nts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T201" id="Seg_724" n="HIAT:u" s="T197">
                  <ts e="T198" id="Seg_726" n="HIAT:w" s="T197">Onno</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_729" n="HIAT:w" s="T198">ehigi</ts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_732" n="HIAT:w" s="T199">minigin</ts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_735" n="HIAT:w" s="T200">ɨlbatakpɨt</ts>
                  <nts id="Seg_736" n="HIAT:ip">.</nts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T209" id="Seg_739" n="HIAT:u" s="T201">
                  <ts e="T202" id="Seg_741" n="HIAT:w" s="T201">Anɨ</ts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_744" n="HIAT:w" s="T202">kelbite</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_747" n="HIAT:w" s="T203">ehigi</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_750" n="HIAT:w" s="T204">kemŋit</ts>
                  <nts id="Seg_751" n="HIAT:ip">,</nts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_754" n="HIAT:w" s="T205">karaŋa</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_757" n="HIAT:w" s="T206">ɨjaːk</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_760" n="HIAT:w" s="T207">ɨraːktaːk</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_763" n="HIAT:w" s="T208">kem</ts>
                  <nts id="Seg_764" n="HIAT:ip">.</nts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T213" id="Seg_767" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_769" n="HIAT:w" s="T209">Keli͡ektere</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_772" n="HIAT:w" s="T210">hɨlɨkčɨttar</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_775" n="HIAT:w" s="T211">hurujbut</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_778" n="HIAT:w" s="T212">östörö</ts>
                  <nts id="Seg_779" n="HIAT:ip">.</nts>
                  <nts id="Seg_780" n="HIAT:ip">"</nts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T224" id="Seg_783" n="HIAT:u" s="T213">
                  <ts e="T214" id="Seg_785" n="HIAT:w" s="T213">Heriː</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_788" n="HIAT:w" s="T214">dʼono</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_791" n="HIAT:w" s="T215">tutan</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_794" n="HIAT:w" s="T216">ɨlaːt</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_797" n="HIAT:w" s="T217">Isuːs</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_800" n="HIAT:w" s="T218">Xrʼistoːhu</ts>
                  <nts id="Seg_801" n="HIAT:ip">,</nts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_804" n="HIAT:w" s="T219">ginini</ts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_807" n="HIAT:w" s="T220">ilpittere</ts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_810" n="HIAT:w" s="T221">bastɨŋ</ts>
                  <nts id="Seg_811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_813" n="HIAT:w" s="T222">agabɨt</ts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_816" n="HIAT:w" s="T223">dʼi͡etiger</ts>
                  <nts id="Seg_817" n="HIAT:ip">.</nts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T231" id="Seg_820" n="HIAT:u" s="T224">
                  <ts e="T225" id="Seg_822" n="HIAT:w" s="T224">Ü͡öreter</ts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_825" n="HIAT:w" s="T225">kihilere</ts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_828" n="HIAT:w" s="T226">bu͡ollaktarɨna</ts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_831" n="HIAT:w" s="T227">ginini</ts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_834" n="HIAT:w" s="T228">keːhen</ts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_837" n="HIAT:w" s="T229">barbɨttar</ts>
                  <nts id="Seg_838" n="HIAT:ip">,</nts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_841" n="HIAT:w" s="T230">küreːbitter</ts>
                  <nts id="Seg_842" n="HIAT:ip">.</nts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T231" id="Seg_844" n="sc" s="T0">
               <ts e="T1" id="Seg_846" n="e" s="T0">Ogolorgo </ts>
               <ts e="T2" id="Seg_848" n="e" s="T1">aːgabɨt </ts>
               <ts e="T3" id="Seg_850" n="e" s="T2">östörü </ts>
               <ts e="T4" id="Seg_852" n="e" s="T3">kinigetten </ts>
               <ts e="T5" id="Seg_854" n="e" s="T4">"Isuːs </ts>
               <ts e="T6" id="Seg_856" n="e" s="T5">Xrʼistos – </ts>
               <ts e="T7" id="Seg_858" n="e" s="T6">ogolor </ts>
               <ts e="T8" id="Seg_860" n="e" s="T7">dogordoro" </ts>
               <ts e="T9" id="Seg_862" n="e" s="T8">di͡en. </ts>
               <ts e="T10" id="Seg_864" n="e" s="T9">Isuːhu </ts>
               <ts e="T11" id="Seg_866" n="e" s="T10">tutan </ts>
               <ts e="T12" id="Seg_868" n="e" s="T11">ɨlbɨttar. </ts>
               <ts e="T13" id="Seg_870" n="e" s="T12">Üste </ts>
               <ts e="T14" id="Seg_872" n="e" s="T13">kat </ts>
               <ts e="T15" id="Seg_874" n="e" s="T14">Isuːs </ts>
               <ts e="T16" id="Seg_876" n="e" s="T15">kele </ts>
               <ts e="T17" id="Seg_878" n="e" s="T16">hɨldʼɨbɨt </ts>
               <ts e="T18" id="Seg_880" n="e" s="T17">ü͡öreter </ts>
               <ts e="T19" id="Seg_882" n="e" s="T18">kihileriger. </ts>
               <ts e="T20" id="Seg_884" n="e" s="T19">Onno </ts>
               <ts e="T21" id="Seg_886" n="e" s="T20">bulaːt </ts>
               <ts e="T22" id="Seg_888" n="e" s="T21">ebit </ts>
               <ts e="T23" id="Seg_890" n="e" s="T22">ginileri </ts>
               <ts e="T24" id="Seg_892" n="e" s="T23">körbüt </ts>
               <ts e="T25" id="Seg_894" n="e" s="T24">huptu </ts>
               <ts e="T26" id="Seg_896" n="e" s="T25">utujallarɨ. </ts>
               <ts e="T27" id="Seg_898" n="e" s="T26">Ühüs </ts>
               <ts e="T28" id="Seg_900" n="e" s="T27">keliːtiger </ts>
               <ts e="T29" id="Seg_902" n="e" s="T28">gini </ts>
               <ts e="T30" id="Seg_904" n="e" s="T29">haŋarbɨt </ts>
               <ts e="T31" id="Seg_906" n="e" s="T30">ginilerge: </ts>
               <ts e="T32" id="Seg_908" n="e" s="T31">"Ehigi </ts>
               <ts e="T33" id="Seg_910" n="e" s="T32">anɨga </ts>
               <ts e="T34" id="Seg_912" n="e" s="T33">di͡eri </ts>
               <ts e="T35" id="Seg_914" n="e" s="T34">anɨ </ts>
               <ts e="T36" id="Seg_916" n="e" s="T35">össü͡ö </ts>
               <ts e="T37" id="Seg_918" n="e" s="T36">utuja </ts>
               <ts e="T38" id="Seg_920" n="e" s="T37">hɨtagɨt </ts>
               <ts e="T39" id="Seg_922" n="e" s="T38">du͡o, </ts>
               <ts e="T40" id="Seg_924" n="e" s="T39">nalɨjan </ts>
               <ts e="T41" id="Seg_926" n="e" s="T40">baraːn? </ts>
               <ts e="T42" id="Seg_928" n="e" s="T41">Dʼe </ts>
               <ts e="T43" id="Seg_930" n="e" s="T42">čugahaːta </ts>
               <ts e="T44" id="Seg_932" n="e" s="T43">čaːs, </ts>
               <ts e="T45" id="Seg_934" n="e" s="T44">kihitten </ts>
               <ts e="T46" id="Seg_936" n="e" s="T45">u͡ol </ts>
               <ts e="T47" id="Seg_938" n="e" s="T46">bi͡eriller </ts>
               <ts e="T48" id="Seg_940" n="e" s="T47">anʼɨːlarɨ </ts>
               <ts e="T49" id="Seg_942" n="e" s="T48">oŋorollor </ts>
               <ts e="T50" id="Seg_944" n="e" s="T49">(kihi) </ts>
               <ts e="T51" id="Seg_946" n="e" s="T50">kihi </ts>
               <ts e="T52" id="Seg_948" n="e" s="T51">aːjɨ. </ts>
               <ts e="T53" id="Seg_950" n="e" s="T52">Turuŋ, </ts>
               <ts e="T54" id="Seg_952" n="e" s="T53">barɨ͡agɨŋ. </ts>
               <ts e="T55" id="Seg_954" n="e" s="T54">Dʼe </ts>
               <ts e="T56" id="Seg_956" n="e" s="T55">čugahaːta, </ts>
               <ts e="T57" id="Seg_958" n="e" s="T56">minigin </ts>
               <ts e="T58" id="Seg_960" n="e" s="T57">ölüːge </ts>
               <ts e="T59" id="Seg_962" n="e" s="T58">bi͡eriːhi." </ts>
               <ts e="T60" id="Seg_964" n="e" s="T59">Gini </ts>
               <ts e="T61" id="Seg_966" n="e" s="T60">itigirdik </ts>
               <ts e="T62" id="Seg_968" n="e" s="T61">haŋara </ts>
               <ts e="T63" id="Seg_970" n="e" s="T62">turdagɨna </ts>
               <ts e="T64" id="Seg_972" n="e" s="T63">kelbit </ts>
               <ts e="T65" id="Seg_974" n="e" s="T64">Juːda, </ts>
               <ts e="T66" id="Seg_976" n="e" s="T65">iti </ts>
               <ts e="T67" id="Seg_978" n="e" s="T66">biːr </ts>
               <ts e="T68" id="Seg_980" n="e" s="T67">ü͡öreter </ts>
               <ts e="T69" id="Seg_982" n="e" s="T68">kihite </ts>
               <ts e="T70" id="Seg_984" n="e" s="T69">u͡on </ts>
               <ts e="T71" id="Seg_986" n="e" s="T70">ikkitten. </ts>
               <ts e="T72" id="Seg_988" n="e" s="T71">Giniː </ts>
               <ts e="T73" id="Seg_990" n="e" s="T72">bastɨŋ </ts>
               <ts e="T74" id="Seg_992" n="e" s="T73">agabɨttarga </ts>
               <ts e="T75" id="Seg_994" n="e" s="T74">bi͡erbit </ts>
               <ts e="T76" id="Seg_996" n="e" s="T75">ebit </ts>
               <ts e="T77" id="Seg_998" n="e" s="T76">tɨlɨn </ts>
               <ts e="T78" id="Seg_1000" n="e" s="T77">otut </ts>
               <ts e="T79" id="Seg_1002" n="e" s="T78">kömüs </ts>
               <ts e="T80" id="Seg_1004" n="e" s="T79">karčɨlar </ts>
               <ts e="T81" id="Seg_1006" n="e" s="T80">ihin </ts>
               <ts e="T82" id="Seg_1008" n="e" s="T81">Isuːhu </ts>
               <ts e="T83" id="Seg_1010" n="e" s="T82">ölüːge </ts>
               <ts e="T84" id="Seg_1012" n="e" s="T83">bi͡eri͡em </ts>
               <ts e="T85" id="Seg_1014" n="e" s="T84">di͡en. </ts>
               <ts e="T86" id="Seg_1016" n="e" s="T85">Juːdanɨ </ts>
               <ts e="T87" id="Seg_1018" n="e" s="T86">gɨtta </ts>
               <ts e="T88" id="Seg_1020" n="e" s="T87">kelbittere </ts>
               <ts e="T89" id="Seg_1022" n="e" s="T88">elbek </ts>
               <ts e="T90" id="Seg_1024" n="e" s="T89">heriː </ts>
               <ts e="T91" id="Seg_1026" n="e" s="T90">dʼono. </ts>
               <ts e="T92" id="Seg_1028" n="e" s="T91">Onno </ts>
               <ts e="T93" id="Seg_1030" n="e" s="T92">bütte </ts>
               <ts e="T94" id="Seg_1032" n="e" s="T93">kihiler </ts>
               <ts e="T95" id="Seg_1034" n="e" s="T94">farʼisʼejdartan, </ts>
               <ts e="T96" id="Seg_1036" n="e" s="T95">bastɨŋ </ts>
               <ts e="T97" id="Seg_1038" n="e" s="T96">agabɨttartan. </ts>
               <ts e="T98" id="Seg_1040" n="e" s="T97">Iliːleriger </ts>
               <ts e="T99" id="Seg_1042" n="e" s="T98">holuːrdaːktar, </ts>
               <ts e="T100" id="Seg_1044" n="e" s="T99">batɨjɨlaːktar </ts>
               <ts e="T101" id="Seg_1046" n="e" s="T100">onno </ts>
               <ts e="T102" id="Seg_1048" n="e" s="T101">öröhö </ts>
               <ts e="T103" id="Seg_1050" n="e" s="T102">mastaːktar. </ts>
               <ts e="T104" id="Seg_1052" n="e" s="T103">Ölüːge </ts>
               <ts e="T105" id="Seg_1054" n="e" s="T104">bi͡erer </ts>
               <ts e="T106" id="Seg_1056" n="e" s="T105">Juːda </ts>
               <ts e="T107" id="Seg_1058" n="e" s="T106">ginilerge </ts>
               <ts e="T108" id="Seg_1060" n="e" s="T107">di͡ebit: </ts>
               <ts e="T109" id="Seg_1062" n="e" s="T108">"Kimi </ts>
               <ts e="T110" id="Seg_1064" n="e" s="T109">min </ts>
               <ts e="T111" id="Seg_1066" n="e" s="T110">uguru͡om, </ts>
               <ts e="T112" id="Seg_1068" n="e" s="T111">ol </ts>
               <ts e="T113" id="Seg_1070" n="e" s="T112">bu͡olu͡oga. </ts>
               <ts e="T114" id="Seg_1072" n="e" s="T113">Oččogo </ts>
               <ts e="T115" id="Seg_1074" n="e" s="T114">onu </ts>
               <ts e="T116" id="Seg_1076" n="e" s="T115">kaban </ts>
               <ts e="T117" id="Seg_1078" n="e" s="T116">ɨlɨŋ". </ts>
               <ts e="T118" id="Seg_1080" n="e" s="T117">Gini </ts>
               <ts e="T119" id="Seg_1082" n="e" s="T118">kelbit </ts>
               <ts e="T120" id="Seg_1084" n="e" s="T119">Isuːs </ts>
               <ts e="T121" id="Seg_1086" n="e" s="T120">attɨgar </ts>
               <ts e="T122" id="Seg_1088" n="e" s="T121">onno </ts>
               <ts e="T123" id="Seg_1090" n="e" s="T122">di͡ebit: </ts>
               <ts e="T124" id="Seg_1092" n="e" s="T123">"Ü͡ör, </ts>
               <ts e="T125" id="Seg_1094" n="e" s="T124">učʼital", </ts>
               <ts e="T126" id="Seg_1096" n="e" s="T125">itigirdik </ts>
               <ts e="T127" id="Seg_1098" n="e" s="T126">haŋaraːt, </ts>
               <ts e="T128" id="Seg_1100" n="e" s="T127">uguraːbɨt </ts>
               <ts e="T129" id="Seg_1102" n="e" s="T128">ginini. </ts>
               <ts e="T130" id="Seg_1104" n="e" s="T129">Isuːs </ts>
               <ts e="T131" id="Seg_1106" n="e" s="T130">bu͡ollagɨna </ts>
               <ts e="T132" id="Seg_1108" n="e" s="T131">haŋarbɨt </ts>
               <ts e="T133" id="Seg_1110" n="e" s="T132">gini͡eke: </ts>
               <ts e="T134" id="Seg_1112" n="e" s="T133">"Dogor, </ts>
               <ts e="T135" id="Seg_1114" n="e" s="T134">togo </ts>
               <ts e="T136" id="Seg_1116" n="e" s="T135">en </ts>
               <ts e="T137" id="Seg_1118" n="e" s="T136">kelliŋ? </ts>
               <ts e="T138" id="Seg_1120" n="e" s="T137">Uguraːŋŋɨn </ts>
               <ts e="T139" id="Seg_1122" n="e" s="T138">du </ts>
               <ts e="T140" id="Seg_1124" n="e" s="T139">bi͡eregin </ts>
               <ts e="T141" id="Seg_1126" n="e" s="T140">ölüːge </ts>
               <ts e="T142" id="Seg_1128" n="e" s="T141">kihitten </ts>
               <ts e="T143" id="Seg_1130" n="e" s="T142">u͡olu". </ts>
               <ts e="T144" id="Seg_1132" n="e" s="T143">Isuːs </ts>
               <ts e="T145" id="Seg_1134" n="e" s="T144">ɨjɨppɨt </ts>
               <ts e="T146" id="Seg_1136" n="e" s="T145">Juːdanɨ </ts>
               <ts e="T147" id="Seg_1138" n="e" s="T146">gɨtta </ts>
               <ts e="T148" id="Seg_1140" n="e" s="T147">kelbit </ts>
               <ts e="T149" id="Seg_1142" n="e" s="T148">heriː </ts>
               <ts e="T150" id="Seg_1144" n="e" s="T149">dʼonuttan: </ts>
               <ts e="T151" id="Seg_1146" n="e" s="T150">"Kimi </ts>
               <ts e="T152" id="Seg_1148" n="e" s="T151">kördüːgüt?" </ts>
               <ts e="T153" id="Seg_1150" n="e" s="T152">Ontulara </ts>
               <ts e="T154" id="Seg_1152" n="e" s="T153">di͡ebitter: </ts>
               <ts e="T155" id="Seg_1154" n="e" s="T154">"Nazarʼejtan </ts>
               <ts e="T156" id="Seg_1156" n="e" s="T155">Isuːhu". </ts>
               <ts e="T157" id="Seg_1158" n="e" s="T156">Küseːjin </ts>
               <ts e="T158" id="Seg_1160" n="e" s="T157">haŋarbɨt </ts>
               <ts e="T159" id="Seg_1162" n="e" s="T158">ginilerge: </ts>
               <ts e="T160" id="Seg_1164" n="e" s="T159">"Iti </ts>
               <ts e="T161" id="Seg_1166" n="e" s="T160">min". </ts>
               <ts e="T162" id="Seg_1168" n="e" s="T161">Itini </ts>
               <ts e="T163" id="Seg_1170" n="e" s="T162">isteːt </ts>
               <ts e="T164" id="Seg_1172" n="e" s="T163">heriː </ts>
               <ts e="T165" id="Seg_1174" n="e" s="T164">dʼono </ts>
               <ts e="T166" id="Seg_1176" n="e" s="T165">kennilerinen </ts>
               <ts e="T167" id="Seg_1178" n="e" s="T166">kaːmpɨttar, </ts>
               <ts e="T168" id="Seg_1180" n="e" s="T167">onno </ts>
               <ts e="T169" id="Seg_1182" n="e" s="T168">hirge </ts>
               <ts e="T170" id="Seg_1184" n="e" s="T169">tüŋneri </ts>
               <ts e="T171" id="Seg_1186" n="e" s="T170">tühetteːbitter. </ts>
               <ts e="T172" id="Seg_1188" n="e" s="T171">Onton </ts>
               <ts e="T173" id="Seg_1190" n="e" s="T172">Isuːs </ts>
               <ts e="T174" id="Seg_1192" n="e" s="T173">haŋarbɨt </ts>
               <ts e="T175" id="Seg_1194" n="e" s="T174">ginilerge: </ts>
               <ts e="T176" id="Seg_1196" n="e" s="T175">"Ehigi </ts>
               <ts e="T177" id="Seg_1198" n="e" s="T176">ölörü͡öksüttüː </ts>
               <ts e="T178" id="Seg_1200" n="e" s="T177">kördüːr </ts>
               <ts e="T179" id="Seg_1202" n="e" s="T178">kördük </ts>
               <ts e="T180" id="Seg_1204" n="e" s="T179">tagɨstɨgɨt </ts>
               <ts e="T181" id="Seg_1206" n="e" s="T180">batɨjalaːk, </ts>
               <ts e="T182" id="Seg_1208" n="e" s="T181">öröhö </ts>
               <ts e="T183" id="Seg_1210" n="e" s="T182">mastaːk, </ts>
               <ts e="T184" id="Seg_1212" n="e" s="T183">minigin </ts>
               <ts e="T185" id="Seg_1214" n="e" s="T184">tutaːrɨ. </ts>
               <ts e="T186" id="Seg_1216" n="e" s="T185">Kün </ts>
               <ts e="T187" id="Seg_1218" n="e" s="T186">aːjɨ </ts>
               <ts e="T188" id="Seg_1220" n="e" s="T187">min </ts>
               <ts e="T189" id="Seg_1222" n="e" s="T188">baːr </ts>
               <ts e="T190" id="Seg_1224" n="e" s="T189">etim </ts>
               <ts e="T191" id="Seg_1226" n="e" s="T190">ehigini </ts>
               <ts e="T192" id="Seg_1228" n="e" s="T191">gɨtta </ts>
               <ts e="T193" id="Seg_1230" n="e" s="T192">ü͡örete </ts>
               <ts e="T195" id="Seg_1232" n="e" s="T193">(ta-) </ts>
               <ts e="T196" id="Seg_1234" n="e" s="T195">taŋara </ts>
               <ts e="T197" id="Seg_1236" n="e" s="T196">dʼi͡etiger. </ts>
               <ts e="T198" id="Seg_1238" n="e" s="T197">Onno </ts>
               <ts e="T199" id="Seg_1240" n="e" s="T198">ehigi </ts>
               <ts e="T200" id="Seg_1242" n="e" s="T199">minigin </ts>
               <ts e="T201" id="Seg_1244" n="e" s="T200">ɨlbatakpɨt. </ts>
               <ts e="T202" id="Seg_1246" n="e" s="T201">Anɨ </ts>
               <ts e="T203" id="Seg_1248" n="e" s="T202">kelbite </ts>
               <ts e="T204" id="Seg_1250" n="e" s="T203">ehigi </ts>
               <ts e="T205" id="Seg_1252" n="e" s="T204">kemŋit, </ts>
               <ts e="T206" id="Seg_1254" n="e" s="T205">karaŋa </ts>
               <ts e="T207" id="Seg_1256" n="e" s="T206">ɨjaːk </ts>
               <ts e="T208" id="Seg_1258" n="e" s="T207">ɨraːktaːk </ts>
               <ts e="T209" id="Seg_1260" n="e" s="T208">kem. </ts>
               <ts e="T210" id="Seg_1262" n="e" s="T209">Keli͡ektere </ts>
               <ts e="T211" id="Seg_1264" n="e" s="T210">hɨlɨkčɨttar </ts>
               <ts e="T212" id="Seg_1266" n="e" s="T211">hurujbut </ts>
               <ts e="T213" id="Seg_1268" n="e" s="T212">östörö." </ts>
               <ts e="T214" id="Seg_1270" n="e" s="T213">Heriː </ts>
               <ts e="T215" id="Seg_1272" n="e" s="T214">dʼono </ts>
               <ts e="T216" id="Seg_1274" n="e" s="T215">tutan </ts>
               <ts e="T217" id="Seg_1276" n="e" s="T216">ɨlaːt </ts>
               <ts e="T218" id="Seg_1278" n="e" s="T217">Isuːs </ts>
               <ts e="T219" id="Seg_1280" n="e" s="T218">Xrʼistoːhu, </ts>
               <ts e="T220" id="Seg_1282" n="e" s="T219">ginini </ts>
               <ts e="T221" id="Seg_1284" n="e" s="T220">ilpittere </ts>
               <ts e="T222" id="Seg_1286" n="e" s="T221">bastɨŋ </ts>
               <ts e="T223" id="Seg_1288" n="e" s="T222">agabɨt </ts>
               <ts e="T224" id="Seg_1290" n="e" s="T223">dʼi͡etiger. </ts>
               <ts e="T225" id="Seg_1292" n="e" s="T224">Ü͡öreter </ts>
               <ts e="T226" id="Seg_1294" n="e" s="T225">kihilere </ts>
               <ts e="T227" id="Seg_1296" n="e" s="T226">bu͡ollaktarɨna </ts>
               <ts e="T228" id="Seg_1298" n="e" s="T227">ginini </ts>
               <ts e="T229" id="Seg_1300" n="e" s="T228">keːhen </ts>
               <ts e="T230" id="Seg_1302" n="e" s="T229">barbɨttar, </ts>
               <ts e="T231" id="Seg_1304" n="e" s="T230">küreːbitter. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T9" id="Seg_1305" s="T0">PoNA_19940110_JesusIsArrested_transl.001 (001.001)</ta>
            <ta e="T12" id="Seg_1306" s="T9">PoNA_19940110_JesusIsArrested_transl.002 (001.002)</ta>
            <ta e="T19" id="Seg_1307" s="T12">PoNA_19940110_JesusIsArrested_transl.003 (001.003)</ta>
            <ta e="T26" id="Seg_1308" s="T19">PoNA_19940110_JesusIsArrested_transl.004 (001.004)</ta>
            <ta e="T31" id="Seg_1309" s="T26">PoNA_19940110_JesusIsArrested_transl.005 (001.005)</ta>
            <ta e="T41" id="Seg_1310" s="T31">PoNA_19940110_JesusIsArrested_transl.006 (001.006)</ta>
            <ta e="T52" id="Seg_1311" s="T41">PoNA_19940110_JesusIsArrested_transl.007 (001.007)</ta>
            <ta e="T54" id="Seg_1312" s="T52">PoNA_19940110_JesusIsArrested_transl.008 (001.008)</ta>
            <ta e="T59" id="Seg_1313" s="T54">PoNA_19940110_JesusIsArrested_transl.009 (001.009)</ta>
            <ta e="T71" id="Seg_1314" s="T59">PoNA_19940110_JesusIsArrested_transl.010 (001.010)</ta>
            <ta e="T85" id="Seg_1315" s="T71">PoNA_19940110_JesusIsArrested_transl.011 (001.011)</ta>
            <ta e="T91" id="Seg_1316" s="T85">PoNA_19940110_JesusIsArrested_transl.012 (001.012)</ta>
            <ta e="T97" id="Seg_1317" s="T91">PoNA_19940110_JesusIsArrested_transl.013 (001.013)</ta>
            <ta e="T103" id="Seg_1318" s="T97">PoNA_19940110_JesusIsArrested_transl.014 (001.014)</ta>
            <ta e="T108" id="Seg_1319" s="T103">PoNA_19940110_JesusIsArrested_transl.015 (001.015)</ta>
            <ta e="T113" id="Seg_1320" s="T108">PoNA_19940110_JesusIsArrested_transl.016 (001.015)</ta>
            <ta e="T117" id="Seg_1321" s="T113">PoNA_19940110_JesusIsArrested_transl.017 (001.016)</ta>
            <ta e="T123" id="Seg_1322" s="T117">PoNA_19940110_JesusIsArrested_transl.018 (001.017)</ta>
            <ta e="T129" id="Seg_1323" s="T123">PoNA_19940110_JesusIsArrested_transl.019 (001.018)</ta>
            <ta e="T133" id="Seg_1324" s="T129">PoNA_19940110_JesusIsArrested_transl.020 (001.019)</ta>
            <ta e="T137" id="Seg_1325" s="T133">PoNA_19940110_JesusIsArrested_transl.021 (001.020)</ta>
            <ta e="T143" id="Seg_1326" s="T137">PoNA_19940110_JesusIsArrested_transl.022 (001.021)</ta>
            <ta e="T150" id="Seg_1327" s="T143">PoNA_19940110_JesusIsArrested_transl.023 (001.022)</ta>
            <ta e="T152" id="Seg_1328" s="T150">PoNA_19940110_JesusIsArrested_transl.024 (001.023)</ta>
            <ta e="T154" id="Seg_1329" s="T152">PoNA_19940110_JesusIsArrested_transl.025 (001.024)</ta>
            <ta e="T156" id="Seg_1330" s="T154">PoNA_19940110_JesusIsArrested_transl.026 (001.025)</ta>
            <ta e="T159" id="Seg_1331" s="T156">PoNA_19940110_JesusIsArrested_transl.027 (001.026)</ta>
            <ta e="T161" id="Seg_1332" s="T159">PoNA_19940110_JesusIsArrested_transl.028 (001.027)</ta>
            <ta e="T171" id="Seg_1333" s="T161">PoNA_19940110_JesusIsArrested_transl.029 (001.028)</ta>
            <ta e="T175" id="Seg_1334" s="T171">PoNA_19940110_JesusIsArrested_transl.030 (001.029)</ta>
            <ta e="T185" id="Seg_1335" s="T175">PoNA_19940110_JesusIsArrested_transl.031 (001.030)</ta>
            <ta e="T197" id="Seg_1336" s="T185">PoNA_19940110_JesusIsArrested_transl.032 (001.031)</ta>
            <ta e="T201" id="Seg_1337" s="T197">PoNA_19940110_JesusIsArrested_transl.033 (001.032)</ta>
            <ta e="T209" id="Seg_1338" s="T201">PoNA_19940110_JesusIsArrested_transl.034 (001.033)</ta>
            <ta e="T213" id="Seg_1339" s="T209">PoNA_19940110_JesusIsArrested_transl.035 (001.034)</ta>
            <ta e="T224" id="Seg_1340" s="T213">PoNA_19940110_JesusIsArrested_transl.036 (001.035)</ta>
            <ta e="T231" id="Seg_1341" s="T224">PoNA_19940110_JesusIsArrested_transl.037 (001.036)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T9" id="Seg_1342" s="T0">Оголорго аагабыт өстөрү кинигэттан "Иисус Христос – оголор догордоро" диэн. </ta>
            <ta e="T12" id="Seg_1343" s="T9">Исууһу тутан ылбыттар.</ta>
            <ta e="T19" id="Seg_1344" s="T12">Үстэ кат Иисус кэлэ һылдьыбыт үөрэтэр киһилэригэр.</ta>
            <ta e="T26" id="Seg_1345" s="T19">Онно булат эбит гинилэри көрбүт һупту утуйаллары.</ta>
            <ta e="T31" id="Seg_1346" s="T26">Үһүс кэлиитигэр гини һаӈарбыт гинилэргэ: </ta>
            <ta e="T41" id="Seg_1347" s="T31">"Эһиги аныга диэри аны өссүө утуйа һытагыт дуо, налыйан бараан?</ta>
            <ta e="T52" id="Seg_1348" s="T41">Дьэ чугаһаата чаас, киһиттэн ол биэриллэр аньыылары оӈороллор киһи (киһи) айыы.</ta>
            <ta e="T54" id="Seg_1349" s="T52">Туруӈ, барыагыӈ.</ta>
            <ta e="T59" id="Seg_1350" s="T54">Дьэ чугаһаата, минигин өлүүгэ биэрииһи."</ta>
            <ta e="T71" id="Seg_1351" s="T59">Гини итигирдик һаӈара турдагына кэлбит Иуда, ити биир үөрэтэр киһитэ уон иккиттэн.</ta>
            <ta e="T85" id="Seg_1352" s="T71">Гинии бастыӈ агабыттарга биэрбит эбит тылын отут көмүс карчылар иһин Иисууһу өлүүгэ биэриэм диэн.</ta>
            <ta e="T91" id="Seg_1353" s="T85">Иуданы гытта кэлбиттэрэ элбэк һэрии дьоно.</ta>
            <ta e="T97" id="Seg_1354" s="T91">Онно бүттэ киһилэр фарисейдартан, бастыӈ агабыттартан.</ta>
            <ta e="T103" id="Seg_1355" s="T97">Илиилэригэр һолуурдаактар, батыйылаактар онно өрөһө мастаактар.</ta>
            <ta e="T108" id="Seg_1356" s="T103">Өлүүгэ биэрэр Иуда гинилэргэ диэбит: </ta>
            <ta e="T113" id="Seg_1357" s="T108">"Кими мин угуруом, ол буолуога.</ta>
            <ta e="T117" id="Seg_1358" s="T113">Оччого ону кабан ылыӈ".</ta>
            <ta e="T123" id="Seg_1359" s="T117">Гини кэлбит Иисус аттыгар онно диээбит: </ta>
            <ta e="T129" id="Seg_1360" s="T123">"Үөр, учитал," – итигирдик һаӈараат, угураабыт гинини. </ta>
            <ta e="T133" id="Seg_1361" s="T129">Иисус буоллагына һаӈарбыт гиниэкэ: </ta>
            <ta e="T137" id="Seg_1362" s="T133">"Дого, того эн кэллиӈ?</ta>
            <ta e="T143" id="Seg_1363" s="T137">Угурааӈӈын дуу биэрэгин өлүүгэ киһиттэн уолу".</ta>
            <ta e="T150" id="Seg_1364" s="T143">Иисус ыыппыт Йуданы гытта кэлбит һэрии дьонуттан: </ta>
            <ta e="T152" id="Seg_1365" s="T150">"Кими көрдүүгүт?"</ta>
            <ta e="T154" id="Seg_1366" s="T152">Онтулара диэбиттэр: </ta>
            <ta e="T156" id="Seg_1367" s="T154">"Назарейтан Иисууһу".</ta>
            <ta e="T159" id="Seg_1368" s="T156">Күсээин һаӈарбыт гинилэргэ: </ta>
            <ta e="T161" id="Seg_1369" s="T159">"Ити мин".</ta>
            <ta e="T171" id="Seg_1370" s="T161">Итини истээт һэрии дьоно кэннилэринэн каампыттар, онно һиргэ түӈнэри түһэттээбиттэр.</ta>
            <ta e="T175" id="Seg_1371" s="T171">Онтон Иисус һаӈарбыт гинилэргэ:</ta>
            <ta e="T185" id="Seg_1372" s="T175">"Эһиги өлөрүөксүттүү көрдүүр көрдүк тагыстыгыт батыйалаак, өрөһө мастаак, минигин тутаары.</ta>
            <ta e="T197" id="Seg_1373" s="T185">Түн аайы мин баар этим эһигини гытта үөрэтэ таӈара дьиэтигэр.</ta>
            <ta e="T201" id="Seg_1374" s="T197">Онно эһиги минигин ылбатаккыт.</ta>
            <ta e="T209" id="Seg_1375" s="T201">Аны кэлбитэ эһиги кэмӈит: караӈа ыйаак ыраактаак кэм.</ta>
            <ta e="T213" id="Seg_1376" s="T209">Кэлиэктэрэ һылыкчыттар һуруйбут өстөрө."</ta>
            <ta e="T224" id="Seg_1377" s="T213">Һэрии дьоно тутан ылаат Иисус Христооһу, гинини илпиттэрэ бастыӈ агабыт дьиэтигэр.</ta>
            <ta e="T231" id="Seg_1378" s="T224">Үөрэтэр киһилэрэ буоллактарына гинини кээһэн барбыттар, күрээбиттэр.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T9" id="Seg_1379" s="T0">Ogolorgo aːgabɨt östörü kinigetten "Isuːs Xrʼistos – ogolor dogordoro" di͡en. </ta>
            <ta e="T12" id="Seg_1380" s="T9">Isuːhu tutan ɨlbɨttar. </ta>
            <ta e="T19" id="Seg_1381" s="T12">Üste kat Isuːs kele hɨldʼɨbɨt ü͡öreter kihileriger. </ta>
            <ta e="T26" id="Seg_1382" s="T19">Onno bulaːt ebit ginileri körbüt huptu utujallarɨ. </ta>
            <ta e="T31" id="Seg_1383" s="T26">Ühüs keliːtiger gini haŋarbɨt ginilerge: </ta>
            <ta e="T41" id="Seg_1384" s="T31">"Ehigi anɨga di͡eri anɨ össü͡ö utuja hɨtagɨt du͡o, nalɨjan baraːn? </ta>
            <ta e="T52" id="Seg_1385" s="T41">Dʼe čugahaːta čaːs, kihitten u͡ol bi͡eriller anʼɨːlarɨ oŋorollor (kihi) kihi aːjɨ. </ta>
            <ta e="T54" id="Seg_1386" s="T52">Turuŋ, barɨ͡agɨŋ. </ta>
            <ta e="T59" id="Seg_1387" s="T54">Dʼe čugahaːta, minigin ölüːge bi͡eriːhi." </ta>
            <ta e="T71" id="Seg_1388" s="T59">Gini itigirdik haŋara turdagɨna kelbit Juːda, iti biːr ü͡öreter kihite u͡on ikkitten. </ta>
            <ta e="T85" id="Seg_1389" s="T71">Giniː bastɨŋ agabɨttarga bi͡erbit ebit tɨlɨn otut kömüs karčɨlar ihin Isuːhu ölüːge bi͡eri͡em di͡en. </ta>
            <ta e="T91" id="Seg_1390" s="T85">Juːdanɨ gɨtta kelbittere elbek heriː dʼono. </ta>
            <ta e="T97" id="Seg_1391" s="T91">Onno bütte kihiler farʼisʼejdartan, bastɨŋ agabɨttartan. </ta>
            <ta e="T103" id="Seg_1392" s="T97">Iliːleriger holuːrdaːktar, batɨjɨlaːktar onno öröhö mastaːktar. </ta>
            <ta e="T108" id="Seg_1393" s="T103">Ölüːge bi͡erer Juːda ginilerge di͡ebit: </ta>
            <ta e="T113" id="Seg_1394" s="T108">"Kimi min uguru͡om, ol bu͡olu͡oga. </ta>
            <ta e="T117" id="Seg_1395" s="T113">Oččogo onu kaban ɨlɨŋ". </ta>
            <ta e="T123" id="Seg_1396" s="T117">Gini kelbit Isuːs attɨgar onno di͡ebit: </ta>
            <ta e="T129" id="Seg_1397" s="T123">"Ü͡ör, učʼital", itigirdik haŋaraːt, uguraːbɨt ginini. </ta>
            <ta e="T133" id="Seg_1398" s="T129">Isuːs bu͡ollagɨna haŋarbɨt gini͡eke: </ta>
            <ta e="T137" id="Seg_1399" s="T133">"Dogor, togo en kelliŋ? </ta>
            <ta e="T143" id="Seg_1400" s="T137">Uguraːŋŋɨn du bi͡eregin ölüːge kihitten u͡olu". </ta>
            <ta e="T150" id="Seg_1401" s="T143">Isuːs ɨjɨppɨt Juːdanɨ gɨtta kelbit heriː dʼonuttan: </ta>
            <ta e="T152" id="Seg_1402" s="T150">"Kimi kördüːgüt?" </ta>
            <ta e="T154" id="Seg_1403" s="T152">Ontulara di͡ebitter: </ta>
            <ta e="T156" id="Seg_1404" s="T154">"Nazarʼejtan Isuːhu". </ta>
            <ta e="T159" id="Seg_1405" s="T156">Küseːjin haŋarbɨt ginilerge: </ta>
            <ta e="T161" id="Seg_1406" s="T159">"Iti min". </ta>
            <ta e="T171" id="Seg_1407" s="T161">Itini isteːt heriː dʼono kennilerinen kaːmpɨttar, onno hirge tüŋneri tühetteːbitter. </ta>
            <ta e="T175" id="Seg_1408" s="T171">Onton Isuːs haŋarbɨt ginilerge: </ta>
            <ta e="T185" id="Seg_1409" s="T175">"Ehigi ölörü͡öksüttüː kördüːr kördük tagɨstɨgɨt batɨjalaːk, öröhö mastaːk, minigin tutaːrɨ. </ta>
            <ta e="T197" id="Seg_1410" s="T185">Kün aːjɨ min baːr etim ehigini gɨtta ü͡örete (ta-) taŋara dʼi͡etiger. </ta>
            <ta e="T201" id="Seg_1411" s="T197">Onno ehigi minigin ɨlbatakpɨt. </ta>
            <ta e="T209" id="Seg_1412" s="T201">Anɨ kelbite ehigi kemŋit, karaŋa ɨjaːk ɨraːktaːk kem. </ta>
            <ta e="T213" id="Seg_1413" s="T209">Keli͡ektere hɨlɨkčɨttar hurujbut östörö." </ta>
            <ta e="T224" id="Seg_1414" s="T213">Heriː dʼono tutan ɨlaːt Isuːs Xrʼistoːhu, ginini ilpittere bastɨŋ agabɨt dʼi͡etiger. </ta>
            <ta e="T231" id="Seg_1415" s="T224">Ü͡öreter kihilere bu͡ollaktarɨna ginini keːhen barbɨttar, küreːbitter. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_1416" s="T0">ogo-lor-go</ta>
            <ta e="T2" id="Seg_1417" s="T1">aːg-a-bɨt</ta>
            <ta e="T3" id="Seg_1418" s="T2">ös-tör-ü</ta>
            <ta e="T4" id="Seg_1419" s="T3">kinige-tten</ta>
            <ta e="T5" id="Seg_1420" s="T4">Isuːs</ta>
            <ta e="T6" id="Seg_1421" s="T5">Xrʼistos</ta>
            <ta e="T7" id="Seg_1422" s="T6">ogo-lor</ta>
            <ta e="T8" id="Seg_1423" s="T7">dogor-doro</ta>
            <ta e="T9" id="Seg_1424" s="T8">di͡e-n</ta>
            <ta e="T10" id="Seg_1425" s="T9">Isuːh-u</ta>
            <ta e="T11" id="Seg_1426" s="T10">tut-an</ta>
            <ta e="T12" id="Seg_1427" s="T11">ɨl-bɨt-tar</ta>
            <ta e="T13" id="Seg_1428" s="T12">üs-te</ta>
            <ta e="T14" id="Seg_1429" s="T13">kat</ta>
            <ta e="T15" id="Seg_1430" s="T14">Isuːs</ta>
            <ta e="T16" id="Seg_1431" s="T15">kel-e</ta>
            <ta e="T17" id="Seg_1432" s="T16">hɨldʼ-ɨ-bɨt</ta>
            <ta e="T18" id="Seg_1433" s="T17">ü͡öret-er</ta>
            <ta e="T19" id="Seg_1434" s="T18">kihi-ler-i-ger</ta>
            <ta e="T20" id="Seg_1435" s="T19">onno</ta>
            <ta e="T21" id="Seg_1436" s="T20">bul-aːt</ta>
            <ta e="T22" id="Seg_1437" s="T21">e-bit</ta>
            <ta e="T23" id="Seg_1438" s="T22">giniler-i</ta>
            <ta e="T24" id="Seg_1439" s="T23">kör-büt</ta>
            <ta e="T25" id="Seg_1440" s="T24">huptu</ta>
            <ta e="T26" id="Seg_1441" s="T25">utuj-al-lar-ɨ</ta>
            <ta e="T27" id="Seg_1442" s="T26">üh-üs</ta>
            <ta e="T28" id="Seg_1443" s="T27">kel-iː-ti-ger</ta>
            <ta e="T29" id="Seg_1444" s="T28">gini</ta>
            <ta e="T30" id="Seg_1445" s="T29">haŋar-bɨt</ta>
            <ta e="T31" id="Seg_1446" s="T30">giniler-ge</ta>
            <ta e="T32" id="Seg_1447" s="T31">ehigi</ta>
            <ta e="T33" id="Seg_1448" s="T32">anɨ-ga</ta>
            <ta e="T34" id="Seg_1449" s="T33">di͡eri</ta>
            <ta e="T35" id="Seg_1450" s="T34">anɨ</ta>
            <ta e="T36" id="Seg_1451" s="T35">össü͡ö</ta>
            <ta e="T37" id="Seg_1452" s="T36">utuj-a</ta>
            <ta e="T38" id="Seg_1453" s="T37">hɨt-a-gɨt</ta>
            <ta e="T39" id="Seg_1454" s="T38">du͡o</ta>
            <ta e="T40" id="Seg_1455" s="T39">nalɨj-an</ta>
            <ta e="T41" id="Seg_1456" s="T40">baraːn</ta>
            <ta e="T42" id="Seg_1457" s="T41">dʼe</ta>
            <ta e="T43" id="Seg_1458" s="T42">čugahaː-t-a</ta>
            <ta e="T44" id="Seg_1459" s="T43">čaːs</ta>
            <ta e="T45" id="Seg_1460" s="T44">kihi-tten</ta>
            <ta e="T46" id="Seg_1461" s="T45">u͡ol</ta>
            <ta e="T47" id="Seg_1462" s="T46">bi͡er-il-ler</ta>
            <ta e="T48" id="Seg_1463" s="T47">anʼɨː-lar-ɨ</ta>
            <ta e="T49" id="Seg_1464" s="T48">oŋor-ol-lor</ta>
            <ta e="T50" id="Seg_1465" s="T49">kihi</ta>
            <ta e="T51" id="Seg_1466" s="T50">kihi</ta>
            <ta e="T52" id="Seg_1467" s="T51">aːjɨ</ta>
            <ta e="T53" id="Seg_1468" s="T52">tur-u-ŋ</ta>
            <ta e="T54" id="Seg_1469" s="T53">bar-ɨ͡agɨŋ</ta>
            <ta e="T55" id="Seg_1470" s="T54">dʼe</ta>
            <ta e="T56" id="Seg_1471" s="T55">čugahaː-t-a</ta>
            <ta e="T57" id="Seg_1472" s="T56">minigi-n</ta>
            <ta e="T58" id="Seg_1473" s="T57">ölüː-ge</ta>
            <ta e="T59" id="Seg_1474" s="T58">bi͡er-iːhi</ta>
            <ta e="T60" id="Seg_1475" s="T59">gini</ta>
            <ta e="T61" id="Seg_1476" s="T60">itigirdik</ta>
            <ta e="T62" id="Seg_1477" s="T61">haŋar-a</ta>
            <ta e="T63" id="Seg_1478" s="T62">tur-dag-ɨna</ta>
            <ta e="T64" id="Seg_1479" s="T63">kel-bit</ta>
            <ta e="T65" id="Seg_1480" s="T64">Juːda</ta>
            <ta e="T66" id="Seg_1481" s="T65">iti</ta>
            <ta e="T67" id="Seg_1482" s="T66">biːr</ta>
            <ta e="T68" id="Seg_1483" s="T67">ü͡öret-er</ta>
            <ta e="T69" id="Seg_1484" s="T68">kihi-te</ta>
            <ta e="T70" id="Seg_1485" s="T69">u͡on</ta>
            <ta e="T71" id="Seg_1486" s="T70">ikki-tten</ta>
            <ta e="T72" id="Seg_1487" s="T71">giniː</ta>
            <ta e="T73" id="Seg_1488" s="T72">bastɨŋ</ta>
            <ta e="T74" id="Seg_1489" s="T73">agabɨt-tar-ga</ta>
            <ta e="T75" id="Seg_1490" s="T74">bi͡er-bit</ta>
            <ta e="T76" id="Seg_1491" s="T75">e-bit</ta>
            <ta e="T77" id="Seg_1492" s="T76">tɨl-ɨ-n</ta>
            <ta e="T78" id="Seg_1493" s="T77">otut</ta>
            <ta e="T79" id="Seg_1494" s="T78">kömüs</ta>
            <ta e="T80" id="Seg_1495" s="T79">karčɨ-lar</ta>
            <ta e="T81" id="Seg_1496" s="T80">ihin</ta>
            <ta e="T82" id="Seg_1497" s="T81">Isuːh-u</ta>
            <ta e="T83" id="Seg_1498" s="T82">ölüː-ge</ta>
            <ta e="T84" id="Seg_1499" s="T83">bi͡er-i͡e-m</ta>
            <ta e="T85" id="Seg_1500" s="T84">di͡e-n</ta>
            <ta e="T86" id="Seg_1501" s="T85">Juːda-nɨ</ta>
            <ta e="T87" id="Seg_1502" s="T86">gɨtta</ta>
            <ta e="T88" id="Seg_1503" s="T87">kel-bit-tere</ta>
            <ta e="T89" id="Seg_1504" s="T88">elbek</ta>
            <ta e="T90" id="Seg_1505" s="T89">heriː</ta>
            <ta e="T91" id="Seg_1506" s="T90">dʼon-o</ta>
            <ta e="T92" id="Seg_1507" s="T91">onno</ta>
            <ta e="T93" id="Seg_1508" s="T92">bütte</ta>
            <ta e="T94" id="Seg_1509" s="T93">kihi-ler</ta>
            <ta e="T95" id="Seg_1510" s="T94">farʼisʼej-dar-tan</ta>
            <ta e="T96" id="Seg_1511" s="T95">bastɨŋ</ta>
            <ta e="T97" id="Seg_1512" s="T96">agabɨt-tar-tan</ta>
            <ta e="T98" id="Seg_1513" s="T97">iliː-leri-ger</ta>
            <ta e="T99" id="Seg_1514" s="T98">holuːr-daːk-tar</ta>
            <ta e="T100" id="Seg_1515" s="T99">batɨjɨ-laːk-tar</ta>
            <ta e="T101" id="Seg_1516" s="T100">onno</ta>
            <ta e="T102" id="Seg_1517" s="T101">öröhö</ta>
            <ta e="T103" id="Seg_1518" s="T102">mas-taːk-tar</ta>
            <ta e="T104" id="Seg_1519" s="T103">ölüː-ge</ta>
            <ta e="T105" id="Seg_1520" s="T104">bi͡er-er</ta>
            <ta e="T106" id="Seg_1521" s="T105">Juːda</ta>
            <ta e="T107" id="Seg_1522" s="T106">giniler-ge</ta>
            <ta e="T108" id="Seg_1523" s="T107">di͡e-bit</ta>
            <ta e="T109" id="Seg_1524" s="T108">kim-i</ta>
            <ta e="T110" id="Seg_1525" s="T109">min</ta>
            <ta e="T111" id="Seg_1526" s="T110">ugur-u͡o-m</ta>
            <ta e="T112" id="Seg_1527" s="T111">ol</ta>
            <ta e="T113" id="Seg_1528" s="T112">bu͡ol-u͡og-a</ta>
            <ta e="T114" id="Seg_1529" s="T113">oččogo</ta>
            <ta e="T115" id="Seg_1530" s="T114">o-nu</ta>
            <ta e="T116" id="Seg_1531" s="T115">kab-an</ta>
            <ta e="T117" id="Seg_1532" s="T116">ɨl-ɨ-ŋ</ta>
            <ta e="T118" id="Seg_1533" s="T117">gini</ta>
            <ta e="T119" id="Seg_1534" s="T118">kel-bit</ta>
            <ta e="T120" id="Seg_1535" s="T119">Isuːs</ta>
            <ta e="T121" id="Seg_1536" s="T120">attɨ-gar</ta>
            <ta e="T122" id="Seg_1537" s="T121">onno</ta>
            <ta e="T123" id="Seg_1538" s="T122">di͡e-bit</ta>
            <ta e="T124" id="Seg_1539" s="T123">ü͡ör</ta>
            <ta e="T125" id="Seg_1540" s="T124">učʼital</ta>
            <ta e="T126" id="Seg_1541" s="T125">itigirdik</ta>
            <ta e="T127" id="Seg_1542" s="T126">haŋar-aːt</ta>
            <ta e="T128" id="Seg_1543" s="T127">uguraː-bɨt</ta>
            <ta e="T129" id="Seg_1544" s="T128">gini-ni</ta>
            <ta e="T130" id="Seg_1545" s="T129">Isuːs</ta>
            <ta e="T131" id="Seg_1546" s="T130">bu͡ollagɨna</ta>
            <ta e="T132" id="Seg_1547" s="T131">haŋar-bɨt</ta>
            <ta e="T133" id="Seg_1548" s="T132">gini͡e-ke</ta>
            <ta e="T134" id="Seg_1549" s="T133">dogor</ta>
            <ta e="T135" id="Seg_1550" s="T134">togo</ta>
            <ta e="T136" id="Seg_1551" s="T135">en</ta>
            <ta e="T137" id="Seg_1552" s="T136">kel-li-ŋ</ta>
            <ta e="T138" id="Seg_1553" s="T137">uguraː-ŋ-ŋɨn</ta>
            <ta e="T139" id="Seg_1554" s="T138">du</ta>
            <ta e="T140" id="Seg_1555" s="T139">bi͡er-e-gin</ta>
            <ta e="T141" id="Seg_1556" s="T140">ölüː-ge</ta>
            <ta e="T142" id="Seg_1557" s="T141">kihi-tten</ta>
            <ta e="T143" id="Seg_1558" s="T142">u͡ol-u</ta>
            <ta e="T144" id="Seg_1559" s="T143">Isuːs</ta>
            <ta e="T145" id="Seg_1560" s="T144">ɨjɨp-pɨt</ta>
            <ta e="T146" id="Seg_1561" s="T145">Juːda-nɨ</ta>
            <ta e="T147" id="Seg_1562" s="T146">gɨtta</ta>
            <ta e="T148" id="Seg_1563" s="T147">kel-bit</ta>
            <ta e="T149" id="Seg_1564" s="T148">heriː</ta>
            <ta e="T150" id="Seg_1565" s="T149">dʼon-u-ttan</ta>
            <ta e="T151" id="Seg_1566" s="T150">kim-i</ta>
            <ta e="T152" id="Seg_1567" s="T151">körd-üː-güt</ta>
            <ta e="T153" id="Seg_1568" s="T152">on-tu-lara</ta>
            <ta e="T154" id="Seg_1569" s="T153">di͡e-bit-ter</ta>
            <ta e="T155" id="Seg_1570" s="T154">Nazarʼej-tan</ta>
            <ta e="T156" id="Seg_1571" s="T155">Isuːh-u</ta>
            <ta e="T157" id="Seg_1572" s="T156">küseːjin</ta>
            <ta e="T158" id="Seg_1573" s="T157">haŋar-bɨt</ta>
            <ta e="T159" id="Seg_1574" s="T158">giniler-ge</ta>
            <ta e="T160" id="Seg_1575" s="T159">iti</ta>
            <ta e="T161" id="Seg_1576" s="T160">min</ta>
            <ta e="T162" id="Seg_1577" s="T161">iti-ni</ta>
            <ta e="T163" id="Seg_1578" s="T162">ist-eːt</ta>
            <ta e="T164" id="Seg_1579" s="T163">heriː</ta>
            <ta e="T165" id="Seg_1580" s="T164">dʼon-o</ta>
            <ta e="T166" id="Seg_1581" s="T165">kenni-leri-nen</ta>
            <ta e="T167" id="Seg_1582" s="T166">kaːm-pɨt-tar</ta>
            <ta e="T168" id="Seg_1583" s="T167">onno</ta>
            <ta e="T169" id="Seg_1584" s="T168">hir-ge</ta>
            <ta e="T170" id="Seg_1585" s="T169">tüŋner-i</ta>
            <ta e="T171" id="Seg_1586" s="T170">tüh-etteː-bit-ter</ta>
            <ta e="T172" id="Seg_1587" s="T171">onton</ta>
            <ta e="T173" id="Seg_1588" s="T172">Isuːs</ta>
            <ta e="T174" id="Seg_1589" s="T173">haŋar-bɨt</ta>
            <ta e="T175" id="Seg_1590" s="T174">giniler-ge</ta>
            <ta e="T176" id="Seg_1591" s="T175">ehigi</ta>
            <ta e="T177" id="Seg_1592" s="T176">ölör-ü͡ök-süt-tüː</ta>
            <ta e="T178" id="Seg_1593" s="T177">kördüː-r</ta>
            <ta e="T179" id="Seg_1594" s="T178">kördük</ta>
            <ta e="T180" id="Seg_1595" s="T179">tagɨs-tɨ-gɨt</ta>
            <ta e="T181" id="Seg_1596" s="T180">batɨja-laːk</ta>
            <ta e="T182" id="Seg_1597" s="T181">öröhö</ta>
            <ta e="T183" id="Seg_1598" s="T182">mas-taːk</ta>
            <ta e="T184" id="Seg_1599" s="T183">minigi-n</ta>
            <ta e="T185" id="Seg_1600" s="T184">tut-aːrɨ</ta>
            <ta e="T186" id="Seg_1601" s="T185">kün</ta>
            <ta e="T187" id="Seg_1602" s="T186">aːjɨ</ta>
            <ta e="T188" id="Seg_1603" s="T187">min</ta>
            <ta e="T189" id="Seg_1604" s="T188">baːr</ta>
            <ta e="T190" id="Seg_1605" s="T189">e-ti-m</ta>
            <ta e="T191" id="Seg_1606" s="T190">ehigi-ni</ta>
            <ta e="T192" id="Seg_1607" s="T191">gɨtta</ta>
            <ta e="T193" id="Seg_1608" s="T192">ü͡öret-e</ta>
            <ta e="T196" id="Seg_1609" s="T195">taŋara</ta>
            <ta e="T197" id="Seg_1610" s="T196">dʼi͡e-ti-ger</ta>
            <ta e="T198" id="Seg_1611" s="T197">onno</ta>
            <ta e="T199" id="Seg_1612" s="T198">ehigi</ta>
            <ta e="T200" id="Seg_1613" s="T199">minigi-n</ta>
            <ta e="T201" id="Seg_1614" s="T200">ɨl-batak-pɨt</ta>
            <ta e="T202" id="Seg_1615" s="T201">anɨ</ta>
            <ta e="T203" id="Seg_1616" s="T202">kel-bit-e</ta>
            <ta e="T204" id="Seg_1617" s="T203">ehigi</ta>
            <ta e="T205" id="Seg_1618" s="T204">kem-ŋit</ta>
            <ta e="T206" id="Seg_1619" s="T205">karaŋa</ta>
            <ta e="T207" id="Seg_1620" s="T206">ɨjaːk</ta>
            <ta e="T208" id="Seg_1621" s="T207">ɨraːk-taːk</ta>
            <ta e="T209" id="Seg_1622" s="T208">kem</ta>
            <ta e="T210" id="Seg_1623" s="T209">kel-i͡ek-tere</ta>
            <ta e="T211" id="Seg_1624" s="T210">hɨlɨkčɨt-tar</ta>
            <ta e="T212" id="Seg_1625" s="T211">huruj-but</ta>
            <ta e="T213" id="Seg_1626" s="T212">ös-törö</ta>
            <ta e="T214" id="Seg_1627" s="T213">heriː</ta>
            <ta e="T215" id="Seg_1628" s="T214">dʼon-o</ta>
            <ta e="T216" id="Seg_1629" s="T215">tut-an</ta>
            <ta e="T217" id="Seg_1630" s="T216">ɨl-aːt</ta>
            <ta e="T218" id="Seg_1631" s="T217">Isuːs</ta>
            <ta e="T219" id="Seg_1632" s="T218">Xrʼistoːh-u</ta>
            <ta e="T220" id="Seg_1633" s="T219">gini-ni</ta>
            <ta e="T221" id="Seg_1634" s="T220">il-pit-tere</ta>
            <ta e="T222" id="Seg_1635" s="T221">bastɨŋ</ta>
            <ta e="T223" id="Seg_1636" s="T222">agabɨt</ta>
            <ta e="T224" id="Seg_1637" s="T223">dʼi͡e-ti-ger</ta>
            <ta e="T225" id="Seg_1638" s="T224">ü͡öret-er</ta>
            <ta e="T226" id="Seg_1639" s="T225">kihi-ler-e</ta>
            <ta e="T227" id="Seg_1640" s="T226">bu͡ollaktarɨna</ta>
            <ta e="T228" id="Seg_1641" s="T227">gini-ni</ta>
            <ta e="T229" id="Seg_1642" s="T228">keːh-en</ta>
            <ta e="T230" id="Seg_1643" s="T229">bar-bɨt-tar</ta>
            <ta e="T231" id="Seg_1644" s="T230">küreː-bit-ter</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_1645" s="T0">ogo-LAr-GA</ta>
            <ta e="T2" id="Seg_1646" s="T1">aːk-A-BIt</ta>
            <ta e="T3" id="Seg_1647" s="T2">ös-LAr-nI</ta>
            <ta e="T4" id="Seg_1648" s="T3">kinige-ttAn</ta>
            <ta e="T5" id="Seg_1649" s="T4">Isus</ta>
            <ta e="T6" id="Seg_1650" s="T5">Xrʼistos</ta>
            <ta e="T7" id="Seg_1651" s="T6">ogo-LAr</ta>
            <ta e="T8" id="Seg_1652" s="T7">dogor-LArA</ta>
            <ta e="T9" id="Seg_1653" s="T8">di͡e-An</ta>
            <ta e="T10" id="Seg_1654" s="T9">Isus-nI</ta>
            <ta e="T11" id="Seg_1655" s="T10">tut-An</ta>
            <ta e="T12" id="Seg_1656" s="T11">ɨl-BIT-LAr</ta>
            <ta e="T13" id="Seg_1657" s="T12">üs-TA</ta>
            <ta e="T14" id="Seg_1658" s="T13">kat</ta>
            <ta e="T15" id="Seg_1659" s="T14">Isus</ta>
            <ta e="T16" id="Seg_1660" s="T15">kel-A</ta>
            <ta e="T17" id="Seg_1661" s="T16">hɨrɨt-I-BIT</ta>
            <ta e="T18" id="Seg_1662" s="T17">ü͡öret-Ar</ta>
            <ta e="T19" id="Seg_1663" s="T18">kihi-LAr-tI-GAr</ta>
            <ta e="T20" id="Seg_1664" s="T19">onno</ta>
            <ta e="T21" id="Seg_1665" s="T20">bul-AːT</ta>
            <ta e="T22" id="Seg_1666" s="T21">e-BIT</ta>
            <ta e="T23" id="Seg_1667" s="T22">giniler-nI</ta>
            <ta e="T24" id="Seg_1668" s="T23">kör-BIT</ta>
            <ta e="T25" id="Seg_1669" s="T24">huptu</ta>
            <ta e="T26" id="Seg_1670" s="T25">utuj-Ar-LAr-nI</ta>
            <ta e="T27" id="Seg_1671" s="T26">üs-Is</ta>
            <ta e="T28" id="Seg_1672" s="T27">kel-Iː-tI-GAr</ta>
            <ta e="T29" id="Seg_1673" s="T28">gini</ta>
            <ta e="T30" id="Seg_1674" s="T29">haŋar-BIT</ta>
            <ta e="T31" id="Seg_1675" s="T30">giniler-GA</ta>
            <ta e="T32" id="Seg_1676" s="T31">ehigi</ta>
            <ta e="T33" id="Seg_1677" s="T32">anɨ-GA</ta>
            <ta e="T34" id="Seg_1678" s="T33">di͡eri</ta>
            <ta e="T35" id="Seg_1679" s="T34">anɨ</ta>
            <ta e="T36" id="Seg_1680" s="T35">össü͡ö</ta>
            <ta e="T37" id="Seg_1681" s="T36">utuj-A</ta>
            <ta e="T38" id="Seg_1682" s="T37">hɨt-A-GIt</ta>
            <ta e="T39" id="Seg_1683" s="T38">du͡o</ta>
            <ta e="T40" id="Seg_1684" s="T39">nalɨj-An</ta>
            <ta e="T41" id="Seg_1685" s="T40">baran</ta>
            <ta e="T42" id="Seg_1686" s="T41">dʼe</ta>
            <ta e="T43" id="Seg_1687" s="T42">čugahaː-TI-tA</ta>
            <ta e="T44" id="Seg_1688" s="T43">čaːs</ta>
            <ta e="T45" id="Seg_1689" s="T44">kihi-ttAn</ta>
            <ta e="T46" id="Seg_1690" s="T45">u͡ol</ta>
            <ta e="T47" id="Seg_1691" s="T46">bi͡er-Ar-LAr</ta>
            <ta e="T48" id="Seg_1692" s="T47">anʼɨː-LAr-nI</ta>
            <ta e="T49" id="Seg_1693" s="T48">oŋor-Ar-LAr</ta>
            <ta e="T50" id="Seg_1694" s="T49">kihi</ta>
            <ta e="T51" id="Seg_1695" s="T50">kihi</ta>
            <ta e="T52" id="Seg_1696" s="T51">aːjɨ</ta>
            <ta e="T53" id="Seg_1697" s="T52">tur-I-ŋ</ta>
            <ta e="T54" id="Seg_1698" s="T53">bar-IAgIŋ</ta>
            <ta e="T55" id="Seg_1699" s="T54">dʼe</ta>
            <ta e="T56" id="Seg_1700" s="T55">čugahaː-TI-tA</ta>
            <ta e="T57" id="Seg_1701" s="T56">min-n</ta>
            <ta e="T58" id="Seg_1702" s="T57">ölüː-GA</ta>
            <ta e="T59" id="Seg_1703" s="T58">bi͡er-IːhI</ta>
            <ta e="T60" id="Seg_1704" s="T59">gini</ta>
            <ta e="T61" id="Seg_1705" s="T60">itigirdik</ta>
            <ta e="T62" id="Seg_1706" s="T61">haŋar-A</ta>
            <ta e="T63" id="Seg_1707" s="T62">tur-TAK-InA</ta>
            <ta e="T64" id="Seg_1708" s="T63">kel-BIT</ta>
            <ta e="T65" id="Seg_1709" s="T64">Juːda</ta>
            <ta e="T66" id="Seg_1710" s="T65">iti</ta>
            <ta e="T67" id="Seg_1711" s="T66">biːr</ta>
            <ta e="T68" id="Seg_1712" s="T67">ü͡öret-Ar</ta>
            <ta e="T69" id="Seg_1713" s="T68">kihi-tA</ta>
            <ta e="T70" id="Seg_1714" s="T69">u͡on</ta>
            <ta e="T71" id="Seg_1715" s="T70">ikki-ttAn</ta>
            <ta e="T72" id="Seg_1716" s="T71">gini</ta>
            <ta e="T73" id="Seg_1717" s="T72">bastɨŋ</ta>
            <ta e="T74" id="Seg_1718" s="T73">agabɨt-LAr-GA</ta>
            <ta e="T75" id="Seg_1719" s="T74">bi͡er-BIT</ta>
            <ta e="T76" id="Seg_1720" s="T75">e-BIT</ta>
            <ta e="T77" id="Seg_1721" s="T76">tɨl-tI-n</ta>
            <ta e="T78" id="Seg_1722" s="T77">otut</ta>
            <ta e="T79" id="Seg_1723" s="T78">kömüs</ta>
            <ta e="T80" id="Seg_1724" s="T79">karčɨ-LAr</ta>
            <ta e="T81" id="Seg_1725" s="T80">ihin</ta>
            <ta e="T82" id="Seg_1726" s="T81">Isus-nI</ta>
            <ta e="T83" id="Seg_1727" s="T82">ölüː-GA</ta>
            <ta e="T84" id="Seg_1728" s="T83">bi͡er-IAK-m</ta>
            <ta e="T85" id="Seg_1729" s="T84">di͡e-An</ta>
            <ta e="T86" id="Seg_1730" s="T85">Juːda-nI</ta>
            <ta e="T87" id="Seg_1731" s="T86">kɨtta</ta>
            <ta e="T88" id="Seg_1732" s="T87">kel-BIT-LArA</ta>
            <ta e="T89" id="Seg_1733" s="T88">elbek</ta>
            <ta e="T90" id="Seg_1734" s="T89">heriː</ta>
            <ta e="T91" id="Seg_1735" s="T90">dʼon-tA</ta>
            <ta e="T92" id="Seg_1736" s="T91">onno</ta>
            <ta e="T93" id="Seg_1737" s="T92">bütte</ta>
            <ta e="T94" id="Seg_1738" s="T93">kihi-LAr</ta>
            <ta e="T95" id="Seg_1739" s="T94">farʼisʼej-LAr-ttAn</ta>
            <ta e="T96" id="Seg_1740" s="T95">bastɨŋ</ta>
            <ta e="T97" id="Seg_1741" s="T96">agabɨt-LAr-ttAn</ta>
            <ta e="T98" id="Seg_1742" s="T97">iliː-LArI-GAr</ta>
            <ta e="T99" id="Seg_1743" s="T98">holuːr-LAːK-LAr</ta>
            <ta e="T100" id="Seg_1744" s="T99">batɨja-LAːK-LAr</ta>
            <ta e="T101" id="Seg_1745" s="T100">onno</ta>
            <ta e="T102" id="Seg_1746" s="T101">öröhö</ta>
            <ta e="T103" id="Seg_1747" s="T102">mas-LAːK-LAr</ta>
            <ta e="T104" id="Seg_1748" s="T103">ölüː-GA</ta>
            <ta e="T105" id="Seg_1749" s="T104">bi͡er-Ar</ta>
            <ta e="T106" id="Seg_1750" s="T105">Juːda</ta>
            <ta e="T107" id="Seg_1751" s="T106">giniler-GA</ta>
            <ta e="T108" id="Seg_1752" s="T107">di͡e-BIT</ta>
            <ta e="T109" id="Seg_1753" s="T108">kim-nI</ta>
            <ta e="T110" id="Seg_1754" s="T109">min</ta>
            <ta e="T111" id="Seg_1755" s="T110">uguraː-IAK-m</ta>
            <ta e="T112" id="Seg_1756" s="T111">ol</ta>
            <ta e="T113" id="Seg_1757" s="T112">bu͡ol-IAK-tA</ta>
            <ta e="T114" id="Seg_1758" s="T113">oččogo</ta>
            <ta e="T115" id="Seg_1759" s="T114">ol-nI</ta>
            <ta e="T116" id="Seg_1760" s="T115">kap-An</ta>
            <ta e="T117" id="Seg_1761" s="T116">ɨl-I-ŋ</ta>
            <ta e="T118" id="Seg_1762" s="T117">gini</ta>
            <ta e="T119" id="Seg_1763" s="T118">kel-BIT</ta>
            <ta e="T120" id="Seg_1764" s="T119">Isus</ta>
            <ta e="T121" id="Seg_1765" s="T120">attɨ-GAr</ta>
            <ta e="T122" id="Seg_1766" s="T121">onno</ta>
            <ta e="T123" id="Seg_1767" s="T122">di͡e-BIT</ta>
            <ta e="T124" id="Seg_1768" s="T123">ü͡ör</ta>
            <ta e="T125" id="Seg_1769" s="T124">učuːtal</ta>
            <ta e="T126" id="Seg_1770" s="T125">itigirdik</ta>
            <ta e="T127" id="Seg_1771" s="T126">haŋar-AːT</ta>
            <ta e="T128" id="Seg_1772" s="T127">uguraː-BIT</ta>
            <ta e="T129" id="Seg_1773" s="T128">gini-nI</ta>
            <ta e="T130" id="Seg_1774" s="T129">Isus</ta>
            <ta e="T131" id="Seg_1775" s="T130">bu͡ollagɨna</ta>
            <ta e="T132" id="Seg_1776" s="T131">haŋar-BIT</ta>
            <ta e="T133" id="Seg_1777" s="T132">gini-GA</ta>
            <ta e="T134" id="Seg_1778" s="T133">dogor</ta>
            <ta e="T135" id="Seg_1779" s="T134">togo</ta>
            <ta e="T136" id="Seg_1780" s="T135">en</ta>
            <ta e="T137" id="Seg_1781" s="T136">kel-TI-ŋ</ta>
            <ta e="T138" id="Seg_1782" s="T137">uguraː-An-GIn</ta>
            <ta e="T139" id="Seg_1783" s="T138">du͡o</ta>
            <ta e="T140" id="Seg_1784" s="T139">bi͡er-A-GIn</ta>
            <ta e="T141" id="Seg_1785" s="T140">ölüː-GA</ta>
            <ta e="T142" id="Seg_1786" s="T141">kihi-ttAn</ta>
            <ta e="T143" id="Seg_1787" s="T142">u͡ol-nI</ta>
            <ta e="T144" id="Seg_1788" s="T143">Isus</ta>
            <ta e="T145" id="Seg_1789" s="T144">ɨjɨt-BIT</ta>
            <ta e="T146" id="Seg_1790" s="T145">Juːda-nI</ta>
            <ta e="T147" id="Seg_1791" s="T146">kɨtta</ta>
            <ta e="T148" id="Seg_1792" s="T147">kel-BIT</ta>
            <ta e="T149" id="Seg_1793" s="T148">heriː</ta>
            <ta e="T150" id="Seg_1794" s="T149">dʼon-tI-ttAn</ta>
            <ta e="T151" id="Seg_1795" s="T150">kim-nI</ta>
            <ta e="T152" id="Seg_1796" s="T151">kördöː-A-GIt</ta>
            <ta e="T153" id="Seg_1797" s="T152">ol-tI-LArA</ta>
            <ta e="T154" id="Seg_1798" s="T153">di͡e-BIT-LAr</ta>
            <ta e="T155" id="Seg_1799" s="T154">Nazarʼej-ttAn</ta>
            <ta e="T156" id="Seg_1800" s="T155">Isus-nI</ta>
            <ta e="T157" id="Seg_1801" s="T156">küheːjin</ta>
            <ta e="T158" id="Seg_1802" s="T157">haŋar-BIT</ta>
            <ta e="T159" id="Seg_1803" s="T158">giniler-GA</ta>
            <ta e="T160" id="Seg_1804" s="T159">iti</ta>
            <ta e="T161" id="Seg_1805" s="T160">min</ta>
            <ta e="T162" id="Seg_1806" s="T161">iti-nI</ta>
            <ta e="T163" id="Seg_1807" s="T162">ihit-AːT</ta>
            <ta e="T164" id="Seg_1808" s="T163">heriː</ta>
            <ta e="T165" id="Seg_1809" s="T164">dʼon-tA</ta>
            <ta e="T166" id="Seg_1810" s="T165">kelin-LArI-nAn</ta>
            <ta e="T167" id="Seg_1811" s="T166">kaːm-BIT-LAr</ta>
            <ta e="T168" id="Seg_1812" s="T167">onno</ta>
            <ta e="T169" id="Seg_1813" s="T168">hir-GA</ta>
            <ta e="T170" id="Seg_1814" s="T169">tüŋner-I</ta>
            <ta e="T171" id="Seg_1815" s="T170">tüs-AːktAː-BIT-LAr</ta>
            <ta e="T172" id="Seg_1816" s="T171">onton</ta>
            <ta e="T173" id="Seg_1817" s="T172">Isus</ta>
            <ta e="T174" id="Seg_1818" s="T173">haŋar-BIT</ta>
            <ta e="T175" id="Seg_1819" s="T174">giniler-GA</ta>
            <ta e="T176" id="Seg_1820" s="T175">ehigi</ta>
            <ta e="T177" id="Seg_1821" s="T176">ölör-IAK-ČIt-LIː</ta>
            <ta e="T178" id="Seg_1822" s="T177">kördöː-Ar</ta>
            <ta e="T179" id="Seg_1823" s="T178">kördük</ta>
            <ta e="T180" id="Seg_1824" s="T179">tagɨs-TI-GIt</ta>
            <ta e="T181" id="Seg_1825" s="T180">batɨja-LAːK</ta>
            <ta e="T182" id="Seg_1826" s="T181">öröhö</ta>
            <ta e="T183" id="Seg_1827" s="T182">mas-LAːK</ta>
            <ta e="T184" id="Seg_1828" s="T183">min-n</ta>
            <ta e="T185" id="Seg_1829" s="T184">tut-AːrI</ta>
            <ta e="T186" id="Seg_1830" s="T185">kün</ta>
            <ta e="T187" id="Seg_1831" s="T186">aːjɨ</ta>
            <ta e="T188" id="Seg_1832" s="T187">min</ta>
            <ta e="T189" id="Seg_1833" s="T188">baːr</ta>
            <ta e="T190" id="Seg_1834" s="T189">e-TI-m</ta>
            <ta e="T191" id="Seg_1835" s="T190">ehigi-nI</ta>
            <ta e="T192" id="Seg_1836" s="T191">kɨtta</ta>
            <ta e="T193" id="Seg_1837" s="T192">ü͡öret-A</ta>
            <ta e="T196" id="Seg_1838" s="T195">taŋara</ta>
            <ta e="T197" id="Seg_1839" s="T196">dʼi͡e-tI-GAr</ta>
            <ta e="T198" id="Seg_1840" s="T197">onno</ta>
            <ta e="T199" id="Seg_1841" s="T198">ehigi</ta>
            <ta e="T200" id="Seg_1842" s="T199">min-n</ta>
            <ta e="T201" id="Seg_1843" s="T200">ɨl-BAtAK-BIt</ta>
            <ta e="T202" id="Seg_1844" s="T201">anɨ</ta>
            <ta e="T203" id="Seg_1845" s="T202">kel-BIT-tA</ta>
            <ta e="T204" id="Seg_1846" s="T203">ehigi</ta>
            <ta e="T205" id="Seg_1847" s="T204">kem-GIt</ta>
            <ta e="T206" id="Seg_1848" s="T205">karaŋa</ta>
            <ta e="T207" id="Seg_1849" s="T206">ɨjaːk</ta>
            <ta e="T208" id="Seg_1850" s="T207">ɨraːk-LAːK</ta>
            <ta e="T209" id="Seg_1851" s="T208">kem</ta>
            <ta e="T210" id="Seg_1852" s="T209">kel-IAK-LArA</ta>
            <ta e="T211" id="Seg_1853" s="T210">hɨlɨkčɨt-LAr</ta>
            <ta e="T212" id="Seg_1854" s="T211">huruj-BIT</ta>
            <ta e="T213" id="Seg_1855" s="T212">ös-LArA</ta>
            <ta e="T214" id="Seg_1856" s="T213">heriː</ta>
            <ta e="T215" id="Seg_1857" s="T214">dʼon-tA</ta>
            <ta e="T216" id="Seg_1858" s="T215">tut-An</ta>
            <ta e="T217" id="Seg_1859" s="T216">ɨl-AːT</ta>
            <ta e="T218" id="Seg_1860" s="T217">Isus</ta>
            <ta e="T219" id="Seg_1861" s="T218">Xrʼistos-nI</ta>
            <ta e="T220" id="Seg_1862" s="T219">gini-nI</ta>
            <ta e="T221" id="Seg_1863" s="T220">ilt-BIT-LArA</ta>
            <ta e="T222" id="Seg_1864" s="T221">bastɨŋ</ta>
            <ta e="T223" id="Seg_1865" s="T222">agabɨt</ta>
            <ta e="T224" id="Seg_1866" s="T223">dʼi͡e-tI-GAr</ta>
            <ta e="T225" id="Seg_1867" s="T224">ü͡öret-Ar</ta>
            <ta e="T226" id="Seg_1868" s="T225">kihi-LAr-tA</ta>
            <ta e="T227" id="Seg_1869" s="T226">bu͡ollagɨna</ta>
            <ta e="T228" id="Seg_1870" s="T227">gini-nI</ta>
            <ta e="T229" id="Seg_1871" s="T228">keːs-An</ta>
            <ta e="T230" id="Seg_1872" s="T229">bar-BIT-LAr</ta>
            <ta e="T231" id="Seg_1873" s="T230">küreː-BIT-LAr</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_1874" s="T0">child-PL-DAT/LOC</ta>
            <ta e="T2" id="Seg_1875" s="T1">read-PRS-1PL</ta>
            <ta e="T3" id="Seg_1876" s="T2">story-PL-ACC</ta>
            <ta e="T4" id="Seg_1877" s="T3">book-ABL</ta>
            <ta e="T5" id="Seg_1878" s="T4">Jesus</ta>
            <ta e="T6" id="Seg_1879" s="T5">Christ.[NOM]</ta>
            <ta e="T7" id="Seg_1880" s="T6">child-PL.[NOM]</ta>
            <ta e="T8" id="Seg_1881" s="T7">friend-3PL.[NOM]</ta>
            <ta e="T9" id="Seg_1882" s="T8">say-CVB.SEQ</ta>
            <ta e="T10" id="Seg_1883" s="T9">Jesus-ACC</ta>
            <ta e="T11" id="Seg_1884" s="T10">grab-CVB.SEQ</ta>
            <ta e="T12" id="Seg_1885" s="T11">take-PST2-3PL</ta>
            <ta e="T13" id="Seg_1886" s="T12">three-MLTP</ta>
            <ta e="T14" id="Seg_1887" s="T13">time.[NOM]</ta>
            <ta e="T15" id="Seg_1888" s="T14">Jesus.[NOM]</ta>
            <ta e="T16" id="Seg_1889" s="T15">come-CVB.SIM</ta>
            <ta e="T17" id="Seg_1890" s="T16">go-EP-PST2.[3SG]</ta>
            <ta e="T18" id="Seg_1891" s="T17">learn-PTCP.PRS</ta>
            <ta e="T19" id="Seg_1892" s="T18">human.being-PL-3SG-DAT/LOC</ta>
            <ta e="T20" id="Seg_1893" s="T19">there</ta>
            <ta e="T21" id="Seg_1894" s="T20">find-CVB.ANT</ta>
            <ta e="T22" id="Seg_1895" s="T21">be-PST2.[3SG]</ta>
            <ta e="T23" id="Seg_1896" s="T22">3PL-ACC</ta>
            <ta e="T24" id="Seg_1897" s="T23">see-PST2.[3SG]</ta>
            <ta e="T25" id="Seg_1898" s="T24">straight</ta>
            <ta e="T26" id="Seg_1899" s="T25">sleep-PTCP.PRS-PL-ACC</ta>
            <ta e="T27" id="Seg_1900" s="T26">three-ORD</ta>
            <ta e="T28" id="Seg_1901" s="T27">come-NMNZ-3SG-DAT/LOC</ta>
            <ta e="T29" id="Seg_1902" s="T28">3SG.[NOM]</ta>
            <ta e="T30" id="Seg_1903" s="T29">speak-PST2.[3SG]</ta>
            <ta e="T31" id="Seg_1904" s="T30">3PL-DAT/LOC</ta>
            <ta e="T32" id="Seg_1905" s="T31">2PL.[NOM]</ta>
            <ta e="T33" id="Seg_1906" s="T32">now-DAT/LOC</ta>
            <ta e="T34" id="Seg_1907" s="T33">until</ta>
            <ta e="T35" id="Seg_1908" s="T34">now</ta>
            <ta e="T36" id="Seg_1909" s="T35">still</ta>
            <ta e="T37" id="Seg_1910" s="T36">sleep-CVB.SIM</ta>
            <ta e="T38" id="Seg_1911" s="T37">lie-PRS-2PL</ta>
            <ta e="T39" id="Seg_1912" s="T38">Q</ta>
            <ta e="T40" id="Seg_1913" s="T39">relax-CVB.SEQ</ta>
            <ta e="T41" id="Seg_1914" s="T40">after</ta>
            <ta e="T42" id="Seg_1915" s="T41">well</ta>
            <ta e="T43" id="Seg_1916" s="T42">come.closer-PST1-3SG</ta>
            <ta e="T44" id="Seg_1917" s="T43">hour.[NOM]</ta>
            <ta e="T45" id="Seg_1918" s="T44">human.being-ABL</ta>
            <ta e="T46" id="Seg_1919" s="T45">son.[NOM]</ta>
            <ta e="T47" id="Seg_1920" s="T46">give-PRS-3PL</ta>
            <ta e="T48" id="Seg_1921" s="T47">sin-PL-ACC</ta>
            <ta e="T49" id="Seg_1922" s="T48">make-PTCP.PRS-PL.[NOM]</ta>
            <ta e="T50" id="Seg_1923" s="T49">human.being.[NOM]</ta>
            <ta e="T51" id="Seg_1924" s="T50">human.being.[NOM]</ta>
            <ta e="T52" id="Seg_1925" s="T51">every</ta>
            <ta e="T53" id="Seg_1926" s="T52">stand.up-EP-IMP.2PL</ta>
            <ta e="T54" id="Seg_1927" s="T53">go-IMP.1PL</ta>
            <ta e="T55" id="Seg_1928" s="T54">well</ta>
            <ta e="T56" id="Seg_1929" s="T55">come.closer-PST1-3SG</ta>
            <ta e="T57" id="Seg_1930" s="T56">1SG-ACC</ta>
            <ta e="T58" id="Seg_1931" s="T57">death-DAT/LOC</ta>
            <ta e="T59" id="Seg_1932" s="T58">give-CAP.[3SG]</ta>
            <ta e="T60" id="Seg_1933" s="T59">3SG.[NOM]</ta>
            <ta e="T61" id="Seg_1934" s="T60">like.that</ta>
            <ta e="T62" id="Seg_1935" s="T61">speak-CVB.SIM</ta>
            <ta e="T63" id="Seg_1936" s="T62">stand-TEMP-3SG</ta>
            <ta e="T64" id="Seg_1937" s="T63">come-PST2.[3SG]</ta>
            <ta e="T65" id="Seg_1938" s="T64">Judas.[NOM]</ta>
            <ta e="T66" id="Seg_1939" s="T65">that.[NOM]</ta>
            <ta e="T67" id="Seg_1940" s="T66">one</ta>
            <ta e="T68" id="Seg_1941" s="T67">teach-PTCP.PRS</ta>
            <ta e="T69" id="Seg_1942" s="T68">human.being-3SG.[NOM]</ta>
            <ta e="T70" id="Seg_1943" s="T69">ten</ta>
            <ta e="T71" id="Seg_1944" s="T70">two-ABL</ta>
            <ta e="T72" id="Seg_1945" s="T71">3SG.[NOM]</ta>
            <ta e="T73" id="Seg_1946" s="T72">leading</ta>
            <ta e="T74" id="Seg_1947" s="T73">clergyman-PL-DAT/LOC</ta>
            <ta e="T75" id="Seg_1948" s="T74">give-PTCP.PST</ta>
            <ta e="T76" id="Seg_1949" s="T75">be-PST2.[3SG]</ta>
            <ta e="T77" id="Seg_1950" s="T76">word-3SG-ACC</ta>
            <ta e="T78" id="Seg_1951" s="T77">thirty</ta>
            <ta e="T79" id="Seg_1952" s="T78">gold.[NOM]</ta>
            <ta e="T80" id="Seg_1953" s="T79">money-PL.[NOM]</ta>
            <ta e="T81" id="Seg_1954" s="T80">because.of</ta>
            <ta e="T82" id="Seg_1955" s="T81">Jesus-ACC</ta>
            <ta e="T83" id="Seg_1956" s="T82">death-DAT/LOC</ta>
            <ta e="T84" id="Seg_1957" s="T83">give-FUT-1SG</ta>
            <ta e="T85" id="Seg_1958" s="T84">say-CVB.SEQ</ta>
            <ta e="T86" id="Seg_1959" s="T85">Judas-ACC</ta>
            <ta e="T87" id="Seg_1960" s="T86">with</ta>
            <ta e="T88" id="Seg_1961" s="T87">come-PST2-3PL</ta>
            <ta e="T89" id="Seg_1962" s="T88">many</ta>
            <ta e="T90" id="Seg_1963" s="T89">army.[NOM]</ta>
            <ta e="T91" id="Seg_1964" s="T90">people-3SG.[NOM]</ta>
            <ta e="T92" id="Seg_1965" s="T91">then</ta>
            <ta e="T93" id="Seg_1966" s="T92">various</ta>
            <ta e="T94" id="Seg_1967" s="T93">human.being-PL.[NOM]</ta>
            <ta e="T95" id="Seg_1968" s="T94">Pharisee-PL-ABL</ta>
            <ta e="T96" id="Seg_1969" s="T95">leading</ta>
            <ta e="T97" id="Seg_1970" s="T96">clergyman-PL-ABL</ta>
            <ta e="T98" id="Seg_1971" s="T97">hand-3PL-DAT/LOC</ta>
            <ta e="T99" id="Seg_1972" s="T98">kettle-PROPR-3PL</ta>
            <ta e="T100" id="Seg_1973" s="T99">sword-PROPR-3PL</ta>
            <ta e="T101" id="Seg_1974" s="T100">then</ta>
            <ta e="T102" id="Seg_1975" s="T101">post.[NOM]</ta>
            <ta e="T103" id="Seg_1976" s="T102">wood-PROPR-3PL</ta>
            <ta e="T104" id="Seg_1977" s="T103">death-DAT/LOC</ta>
            <ta e="T105" id="Seg_1978" s="T104">give-PTCP.PRS</ta>
            <ta e="T106" id="Seg_1979" s="T105">Judas.[NOM]</ta>
            <ta e="T107" id="Seg_1980" s="T106">3PL-DAT/LOC</ta>
            <ta e="T108" id="Seg_1981" s="T107">say-PST2.[3SG]</ta>
            <ta e="T109" id="Seg_1982" s="T108">who-ACC</ta>
            <ta e="T110" id="Seg_1983" s="T109">1SG.[NOM]</ta>
            <ta e="T111" id="Seg_1984" s="T110">kiss-FUT-1SG</ta>
            <ta e="T112" id="Seg_1985" s="T111">that.[NOM]</ta>
            <ta e="T113" id="Seg_1986" s="T112">be-FUT-3SG</ta>
            <ta e="T114" id="Seg_1987" s="T113">then</ta>
            <ta e="T115" id="Seg_1988" s="T114">that-ACC</ta>
            <ta e="T116" id="Seg_1989" s="T115">catch-CVB.SEQ</ta>
            <ta e="T117" id="Seg_1990" s="T116">take-EP-IMP.2PL</ta>
            <ta e="T118" id="Seg_1991" s="T117">3SG.[NOM]</ta>
            <ta e="T119" id="Seg_1992" s="T118">come-PST2.[3SG]</ta>
            <ta e="T120" id="Seg_1993" s="T119">Jesus.[NOM]</ta>
            <ta e="T121" id="Seg_1994" s="T120">place.beneath-DAT/LOC</ta>
            <ta e="T122" id="Seg_1995" s="T121">then</ta>
            <ta e="T123" id="Seg_1996" s="T122">say-PST2.[3SG]</ta>
            <ta e="T124" id="Seg_1997" s="T123">be.happy.[IMP.2SG]</ta>
            <ta e="T125" id="Seg_1998" s="T124">teacher.[NOM]</ta>
            <ta e="T126" id="Seg_1999" s="T125">like.that</ta>
            <ta e="T127" id="Seg_2000" s="T126">speak-CVB.ANT</ta>
            <ta e="T128" id="Seg_2001" s="T127">kiss-PST2.[3SG]</ta>
            <ta e="T129" id="Seg_2002" s="T128">3SG-ACC</ta>
            <ta e="T130" id="Seg_2003" s="T129">Jesus.[NOM]</ta>
            <ta e="T131" id="Seg_2004" s="T130">though</ta>
            <ta e="T132" id="Seg_2005" s="T131">speak-PST2.[3SG]</ta>
            <ta e="T133" id="Seg_2006" s="T132">3SG-DAT/LOC</ta>
            <ta e="T134" id="Seg_2007" s="T133">friend.[NOM]</ta>
            <ta e="T135" id="Seg_2008" s="T134">why</ta>
            <ta e="T136" id="Seg_2009" s="T135">2SG.[NOM]</ta>
            <ta e="T137" id="Seg_2010" s="T136">come-PST1-2SG</ta>
            <ta e="T138" id="Seg_2011" s="T137">kiss-CVB.SEQ-2SG</ta>
            <ta e="T139" id="Seg_2012" s="T138">Q</ta>
            <ta e="T140" id="Seg_2013" s="T139">give-PRS-2SG</ta>
            <ta e="T141" id="Seg_2014" s="T140">death-DAT/LOC</ta>
            <ta e="T142" id="Seg_2015" s="T141">human.being-ABL</ta>
            <ta e="T143" id="Seg_2016" s="T142">son-ACC</ta>
            <ta e="T144" id="Seg_2017" s="T143">Jesus.[NOM]</ta>
            <ta e="T145" id="Seg_2018" s="T144">ask-PST2.[3SG]</ta>
            <ta e="T146" id="Seg_2019" s="T145">Judas-ACC</ta>
            <ta e="T147" id="Seg_2020" s="T146">with</ta>
            <ta e="T148" id="Seg_2021" s="T147">come-PTCP.PST</ta>
            <ta e="T149" id="Seg_2022" s="T148">army.[NOM]</ta>
            <ta e="T150" id="Seg_2023" s="T149">people-3SG-ABL</ta>
            <ta e="T151" id="Seg_2024" s="T150">who-ACC</ta>
            <ta e="T152" id="Seg_2025" s="T151">search-PRS-2PL</ta>
            <ta e="T153" id="Seg_2026" s="T152">that-3SG-3PL.[NOM]</ta>
            <ta e="T154" id="Seg_2027" s="T153">say-PST2-3PL</ta>
            <ta e="T155" id="Seg_2028" s="T154">Nazareth-ABL</ta>
            <ta e="T156" id="Seg_2029" s="T155">Jesus-ACC</ta>
            <ta e="T157" id="Seg_2030" s="T156">man.of.the.house.[NOM]</ta>
            <ta e="T158" id="Seg_2031" s="T157">speak-PST2.[3SG]</ta>
            <ta e="T159" id="Seg_2032" s="T158">3PL-DAT/LOC</ta>
            <ta e="T160" id="Seg_2033" s="T159">that.[NOM]</ta>
            <ta e="T161" id="Seg_2034" s="T160">1SG.[NOM]</ta>
            <ta e="T162" id="Seg_2035" s="T161">that-ACC</ta>
            <ta e="T163" id="Seg_2036" s="T162">hear-CVB.ANT</ta>
            <ta e="T164" id="Seg_2037" s="T163">army.[NOM]</ta>
            <ta e="T165" id="Seg_2038" s="T164">people-3SG.[NOM]</ta>
            <ta e="T166" id="Seg_2039" s="T165">back-3PL-INSTR</ta>
            <ta e="T167" id="Seg_2040" s="T166">walk-PST2-3PL</ta>
            <ta e="T168" id="Seg_2041" s="T167">there</ta>
            <ta e="T169" id="Seg_2042" s="T168">earth-DAT/LOC</ta>
            <ta e="T170" id="Seg_2043" s="T169">upset-ADVZ</ta>
            <ta e="T171" id="Seg_2044" s="T170">fall-EMOT-PST2-3PL</ta>
            <ta e="T172" id="Seg_2045" s="T171">then</ta>
            <ta e="T173" id="Seg_2046" s="T172">Jesus.[NOM]</ta>
            <ta e="T174" id="Seg_2047" s="T173">speak-PST2.[3SG]</ta>
            <ta e="T175" id="Seg_2048" s="T174">3PL-DAT/LOC</ta>
            <ta e="T176" id="Seg_2049" s="T175">2PL.[NOM]</ta>
            <ta e="T177" id="Seg_2050" s="T176">kill-PTCP.FUT-AG-SIM</ta>
            <ta e="T178" id="Seg_2051" s="T177">search-PTCP.PRS.[NOM]</ta>
            <ta e="T179" id="Seg_2052" s="T178">similar</ta>
            <ta e="T180" id="Seg_2053" s="T179">go.out-PST1-2PL</ta>
            <ta e="T181" id="Seg_2054" s="T180">sword-PROPR.[NOM]</ta>
            <ta e="T182" id="Seg_2055" s="T181">post.[NOM]</ta>
            <ta e="T183" id="Seg_2056" s="T182">wood-PROPR</ta>
            <ta e="T184" id="Seg_2057" s="T183">1SG-ACC</ta>
            <ta e="T185" id="Seg_2058" s="T184">grab-CVB.PURP</ta>
            <ta e="T186" id="Seg_2059" s="T185">day.[NOM]</ta>
            <ta e="T187" id="Seg_2060" s="T186">every</ta>
            <ta e="T188" id="Seg_2061" s="T187">1SG.[NOM]</ta>
            <ta e="T189" id="Seg_2062" s="T188">there.is</ta>
            <ta e="T190" id="Seg_2063" s="T189">be-PST1-1SG</ta>
            <ta e="T191" id="Seg_2064" s="T190">2PL-ACC</ta>
            <ta e="T192" id="Seg_2065" s="T191">with</ta>
            <ta e="T193" id="Seg_2066" s="T192">learn-CVB.SIM</ta>
            <ta e="T196" id="Seg_2067" s="T195">god.[NOM]</ta>
            <ta e="T197" id="Seg_2068" s="T196">house-3SG-DAT/LOC</ta>
            <ta e="T198" id="Seg_2069" s="T197">there</ta>
            <ta e="T199" id="Seg_2070" s="T198">2PL.[NOM]</ta>
            <ta e="T200" id="Seg_2071" s="T199">1SG-ACC</ta>
            <ta e="T201" id="Seg_2072" s="T200">get-PST2.NEG-1PL</ta>
            <ta e="T202" id="Seg_2073" s="T201">now</ta>
            <ta e="T203" id="Seg_2074" s="T202">come-PST2-3SG</ta>
            <ta e="T204" id="Seg_2075" s="T203">2PL.[NOM]</ta>
            <ta e="T205" id="Seg_2076" s="T204">time-2PL.[NOM]</ta>
            <ta e="T206" id="Seg_2077" s="T205">dark</ta>
            <ta e="T207" id="Seg_2078" s="T206">destiny.[NOM]</ta>
            <ta e="T208" id="Seg_2079" s="T207">distant-PROPR</ta>
            <ta e="T209" id="Seg_2080" s="T208">time.[NOM]</ta>
            <ta e="T210" id="Seg_2081" s="T209">come-FUT-3PL</ta>
            <ta e="T211" id="Seg_2082" s="T210">prophet-PL.[NOM]</ta>
            <ta e="T212" id="Seg_2083" s="T211">write-PTCP.PST</ta>
            <ta e="T213" id="Seg_2084" s="T212">word-3PL.[NOM]</ta>
            <ta e="T214" id="Seg_2085" s="T213">army.[NOM]</ta>
            <ta e="T215" id="Seg_2086" s="T214">people-3SG.[NOM]</ta>
            <ta e="T216" id="Seg_2087" s="T215">grab-CVB.SEQ</ta>
            <ta e="T217" id="Seg_2088" s="T216">take-CVB.ANT</ta>
            <ta e="T218" id="Seg_2089" s="T217">Jesus</ta>
            <ta e="T219" id="Seg_2090" s="T218">Christ-ACC</ta>
            <ta e="T220" id="Seg_2091" s="T219">3SG-ACC</ta>
            <ta e="T221" id="Seg_2092" s="T220">carry-PST2-3PL</ta>
            <ta e="T222" id="Seg_2093" s="T221">leading</ta>
            <ta e="T223" id="Seg_2094" s="T222">clergyman.[NOM]</ta>
            <ta e="T224" id="Seg_2095" s="T223">house-3SG-DAT/LOC</ta>
            <ta e="T225" id="Seg_2096" s="T224">learn-PTCP.PRS</ta>
            <ta e="T226" id="Seg_2097" s="T225">human.being-PL-3SG.[NOM]</ta>
            <ta e="T227" id="Seg_2098" s="T226">though</ta>
            <ta e="T228" id="Seg_2099" s="T227">3SG-ACC</ta>
            <ta e="T229" id="Seg_2100" s="T228">let-CVB.SEQ</ta>
            <ta e="T230" id="Seg_2101" s="T229">go-PST2-3PL</ta>
            <ta e="T231" id="Seg_2102" s="T230">escape-PST2-3PL</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_2103" s="T0">Kind-PL-DAT/LOC</ta>
            <ta e="T2" id="Seg_2104" s="T1">lesen-PRS-1PL</ta>
            <ta e="T3" id="Seg_2105" s="T2">Erzählung-PL-ACC</ta>
            <ta e="T4" id="Seg_2106" s="T3">Buch-ABL</ta>
            <ta e="T5" id="Seg_2107" s="T4">Jesus</ta>
            <ta e="T6" id="Seg_2108" s="T5">Christus.[NOM]</ta>
            <ta e="T7" id="Seg_2109" s="T6">Kind-PL.[NOM]</ta>
            <ta e="T8" id="Seg_2110" s="T7">Freund-3PL.[NOM]</ta>
            <ta e="T9" id="Seg_2111" s="T8">sagen-CVB.SEQ</ta>
            <ta e="T10" id="Seg_2112" s="T9">Jesus-ACC</ta>
            <ta e="T11" id="Seg_2113" s="T10">greifen-CVB.SEQ</ta>
            <ta e="T12" id="Seg_2114" s="T11">nehmen-PST2-3PL</ta>
            <ta e="T13" id="Seg_2115" s="T12">drei-MLTP</ta>
            <ta e="T14" id="Seg_2116" s="T13">Mal.[NOM]</ta>
            <ta e="T15" id="Seg_2117" s="T14">Jesus.[NOM]</ta>
            <ta e="T16" id="Seg_2118" s="T15">kommen-CVB.SIM</ta>
            <ta e="T17" id="Seg_2119" s="T16">gehen-EP-PST2.[3SG]</ta>
            <ta e="T18" id="Seg_2120" s="T17">lernen-PTCP.PRS</ta>
            <ta e="T19" id="Seg_2121" s="T18">Mensch-PL-3SG-DAT/LOC</ta>
            <ta e="T20" id="Seg_2122" s="T19">dort</ta>
            <ta e="T21" id="Seg_2123" s="T20">finden-CVB.ANT</ta>
            <ta e="T22" id="Seg_2124" s="T21">sein-PST2.[3SG]</ta>
            <ta e="T23" id="Seg_2125" s="T22">3PL-ACC</ta>
            <ta e="T24" id="Seg_2126" s="T23">sehen-PST2.[3SG]</ta>
            <ta e="T25" id="Seg_2127" s="T24">direkt</ta>
            <ta e="T26" id="Seg_2128" s="T25">schlafen-PTCP.PRS-PL-ACC</ta>
            <ta e="T27" id="Seg_2129" s="T26">drei-ORD</ta>
            <ta e="T28" id="Seg_2130" s="T27">kommen-NMNZ-3SG-DAT/LOC</ta>
            <ta e="T29" id="Seg_2131" s="T28">3SG.[NOM]</ta>
            <ta e="T30" id="Seg_2132" s="T29">sprechen-PST2.[3SG]</ta>
            <ta e="T31" id="Seg_2133" s="T30">3PL-DAT/LOC</ta>
            <ta e="T32" id="Seg_2134" s="T31">2PL.[NOM]</ta>
            <ta e="T33" id="Seg_2135" s="T32">jetzt-DAT/LOC</ta>
            <ta e="T34" id="Seg_2136" s="T33">bis.zu</ta>
            <ta e="T35" id="Seg_2137" s="T34">jetzt</ta>
            <ta e="T36" id="Seg_2138" s="T35">noch</ta>
            <ta e="T37" id="Seg_2139" s="T36">schlafen-CVB.SIM</ta>
            <ta e="T38" id="Seg_2140" s="T37">liegen-PRS-2PL</ta>
            <ta e="T39" id="Seg_2141" s="T38">Q</ta>
            <ta e="T40" id="Seg_2142" s="T39">sich.entspannen-CVB.SEQ</ta>
            <ta e="T41" id="Seg_2143" s="T40">nachdem</ta>
            <ta e="T42" id="Seg_2144" s="T41">doch</ta>
            <ta e="T43" id="Seg_2145" s="T42">sich.nähern-PST1-3SG</ta>
            <ta e="T44" id="Seg_2146" s="T43">Stunde.[NOM]</ta>
            <ta e="T45" id="Seg_2147" s="T44">Mensch-ABL</ta>
            <ta e="T46" id="Seg_2148" s="T45">Sohn.[NOM]</ta>
            <ta e="T47" id="Seg_2149" s="T46">geben-PRS-3PL</ta>
            <ta e="T48" id="Seg_2150" s="T47">Sünde-PL-ACC</ta>
            <ta e="T49" id="Seg_2151" s="T48">machen-PTCP.PRS-PL.[NOM]</ta>
            <ta e="T50" id="Seg_2152" s="T49">Mensch.[NOM]</ta>
            <ta e="T51" id="Seg_2153" s="T50">Mensch.[NOM]</ta>
            <ta e="T52" id="Seg_2154" s="T51">jeder</ta>
            <ta e="T53" id="Seg_2155" s="T52">aufstehen-EP-IMP.2PL</ta>
            <ta e="T54" id="Seg_2156" s="T53">gehen-IMP.1PL</ta>
            <ta e="T55" id="Seg_2157" s="T54">doch</ta>
            <ta e="T56" id="Seg_2158" s="T55">sich.nähern-PST1-3SG</ta>
            <ta e="T57" id="Seg_2159" s="T56">1SG-ACC</ta>
            <ta e="T58" id="Seg_2160" s="T57">Tod-DAT/LOC</ta>
            <ta e="T59" id="Seg_2161" s="T58">geben-CAP.[3SG]</ta>
            <ta e="T60" id="Seg_2162" s="T59">3SG.[NOM]</ta>
            <ta e="T61" id="Seg_2163" s="T60">so</ta>
            <ta e="T62" id="Seg_2164" s="T61">sprechen-CVB.SIM</ta>
            <ta e="T63" id="Seg_2165" s="T62">stehen-TEMP-3SG</ta>
            <ta e="T64" id="Seg_2166" s="T63">kommen-PST2.[3SG]</ta>
            <ta e="T65" id="Seg_2167" s="T64">Judas.[NOM]</ta>
            <ta e="T66" id="Seg_2168" s="T65">dieses.[NOM]</ta>
            <ta e="T67" id="Seg_2169" s="T66">eins</ta>
            <ta e="T68" id="Seg_2170" s="T67">lehren-PTCP.PRS</ta>
            <ta e="T69" id="Seg_2171" s="T68">Mensch-3SG.[NOM]</ta>
            <ta e="T70" id="Seg_2172" s="T69">zehn</ta>
            <ta e="T71" id="Seg_2173" s="T70">zwei-ABL</ta>
            <ta e="T72" id="Seg_2174" s="T71">3SG.[NOM]</ta>
            <ta e="T73" id="Seg_2175" s="T72">führend</ta>
            <ta e="T74" id="Seg_2176" s="T73">Geistlicher-PL-DAT/LOC</ta>
            <ta e="T75" id="Seg_2177" s="T74">geben-PTCP.PST</ta>
            <ta e="T76" id="Seg_2178" s="T75">sein-PST2.[3SG]</ta>
            <ta e="T77" id="Seg_2179" s="T76">Wort-3SG-ACC</ta>
            <ta e="T78" id="Seg_2180" s="T77">dreißig</ta>
            <ta e="T79" id="Seg_2181" s="T78">Gold.[NOM]</ta>
            <ta e="T80" id="Seg_2182" s="T79">Geld-PL.[NOM]</ta>
            <ta e="T81" id="Seg_2183" s="T80">wegen</ta>
            <ta e="T82" id="Seg_2184" s="T81">Jesus-ACC</ta>
            <ta e="T83" id="Seg_2185" s="T82">Tod-DAT/LOC</ta>
            <ta e="T84" id="Seg_2186" s="T83">geben-FUT-1SG</ta>
            <ta e="T85" id="Seg_2187" s="T84">sagen-CVB.SEQ</ta>
            <ta e="T86" id="Seg_2188" s="T85">Judas-ACC</ta>
            <ta e="T87" id="Seg_2189" s="T86">mit</ta>
            <ta e="T88" id="Seg_2190" s="T87">kommen-PST2-3PL</ta>
            <ta e="T89" id="Seg_2191" s="T88">viel</ta>
            <ta e="T90" id="Seg_2192" s="T89">Heer.[NOM]</ta>
            <ta e="T91" id="Seg_2193" s="T90">Volk-3SG.[NOM]</ta>
            <ta e="T92" id="Seg_2194" s="T91">dann</ta>
            <ta e="T93" id="Seg_2195" s="T92">verschieden</ta>
            <ta e="T94" id="Seg_2196" s="T93">Mensch-PL.[NOM]</ta>
            <ta e="T95" id="Seg_2197" s="T94">Pharisäer-PL-ABL</ta>
            <ta e="T96" id="Seg_2198" s="T95">führend</ta>
            <ta e="T97" id="Seg_2199" s="T96">Geistlicher-PL-ABL</ta>
            <ta e="T98" id="Seg_2200" s="T97">Hand-3PL-DAT/LOC</ta>
            <ta e="T99" id="Seg_2201" s="T98">Kessel-PROPR-3PL</ta>
            <ta e="T100" id="Seg_2202" s="T99">Schwert-PROPR-3PL</ta>
            <ta e="T101" id="Seg_2203" s="T100">dann</ta>
            <ta e="T102" id="Seg_2204" s="T101">Pflock.[NOM]</ta>
            <ta e="T103" id="Seg_2205" s="T102">Holz-PROPR-3PL</ta>
            <ta e="T104" id="Seg_2206" s="T103">Tod-DAT/LOC</ta>
            <ta e="T105" id="Seg_2207" s="T104">geben-PTCP.PRS</ta>
            <ta e="T106" id="Seg_2208" s="T105">Judas.[NOM]</ta>
            <ta e="T107" id="Seg_2209" s="T106">3PL-DAT/LOC</ta>
            <ta e="T108" id="Seg_2210" s="T107">sagen-PST2.[3SG]</ta>
            <ta e="T109" id="Seg_2211" s="T108">wer-ACC</ta>
            <ta e="T110" id="Seg_2212" s="T109">1SG.[NOM]</ta>
            <ta e="T111" id="Seg_2213" s="T110">küssen-FUT-1SG</ta>
            <ta e="T112" id="Seg_2214" s="T111">jenes.[NOM]</ta>
            <ta e="T113" id="Seg_2215" s="T112">sein-FUT-3SG</ta>
            <ta e="T114" id="Seg_2216" s="T113">dann</ta>
            <ta e="T115" id="Seg_2217" s="T114">jenes-ACC</ta>
            <ta e="T116" id="Seg_2218" s="T115">fangen-CVB.SEQ</ta>
            <ta e="T117" id="Seg_2219" s="T116">nehmen-EP-IMP.2PL</ta>
            <ta e="T118" id="Seg_2220" s="T117">3SG.[NOM]</ta>
            <ta e="T119" id="Seg_2221" s="T118">kommen-PST2.[3SG]</ta>
            <ta e="T120" id="Seg_2222" s="T119">Jesus.[NOM]</ta>
            <ta e="T121" id="Seg_2223" s="T120">Platz.neben-DAT/LOC</ta>
            <ta e="T122" id="Seg_2224" s="T121">dann</ta>
            <ta e="T123" id="Seg_2225" s="T122">sagen-PST2.[3SG]</ta>
            <ta e="T124" id="Seg_2226" s="T123">sich.freuen.[IMP.2SG]</ta>
            <ta e="T125" id="Seg_2227" s="T124">Lehrer.[NOM]</ta>
            <ta e="T126" id="Seg_2228" s="T125">so</ta>
            <ta e="T127" id="Seg_2229" s="T126">sprechen-CVB.ANT</ta>
            <ta e="T128" id="Seg_2230" s="T127">küssen-PST2.[3SG]</ta>
            <ta e="T129" id="Seg_2231" s="T128">3SG-ACC</ta>
            <ta e="T130" id="Seg_2232" s="T129">Jesus.[NOM]</ta>
            <ta e="T131" id="Seg_2233" s="T130">aber</ta>
            <ta e="T132" id="Seg_2234" s="T131">sprechen-PST2.[3SG]</ta>
            <ta e="T133" id="Seg_2235" s="T132">3SG-DAT/LOC</ta>
            <ta e="T134" id="Seg_2236" s="T133">Freund.[NOM]</ta>
            <ta e="T135" id="Seg_2237" s="T134">warum</ta>
            <ta e="T136" id="Seg_2238" s="T135">2SG.[NOM]</ta>
            <ta e="T137" id="Seg_2239" s="T136">kommen-PST1-2SG</ta>
            <ta e="T138" id="Seg_2240" s="T137">küssen-CVB.SEQ-2SG</ta>
            <ta e="T139" id="Seg_2241" s="T138">Q</ta>
            <ta e="T140" id="Seg_2242" s="T139">geben-PRS-2SG</ta>
            <ta e="T141" id="Seg_2243" s="T140">Tod-DAT/LOC</ta>
            <ta e="T142" id="Seg_2244" s="T141">Mensch-ABL</ta>
            <ta e="T143" id="Seg_2245" s="T142">Sohn-ACC</ta>
            <ta e="T144" id="Seg_2246" s="T143">Jesus.[NOM]</ta>
            <ta e="T145" id="Seg_2247" s="T144">fragen-PST2.[3SG]</ta>
            <ta e="T146" id="Seg_2248" s="T145">Judas-ACC</ta>
            <ta e="T147" id="Seg_2249" s="T146">mit</ta>
            <ta e="T148" id="Seg_2250" s="T147">kommen-PTCP.PST</ta>
            <ta e="T149" id="Seg_2251" s="T148">Heer.[NOM]</ta>
            <ta e="T150" id="Seg_2252" s="T149">Volk-3SG-ABL</ta>
            <ta e="T151" id="Seg_2253" s="T150">wer-ACC</ta>
            <ta e="T152" id="Seg_2254" s="T151">suchen-PRS-2PL</ta>
            <ta e="T153" id="Seg_2255" s="T152">jenes-3SG-3PL.[NOM]</ta>
            <ta e="T154" id="Seg_2256" s="T153">sagen-PST2-3PL</ta>
            <ta e="T155" id="Seg_2257" s="T154">Nazareth-ABL</ta>
            <ta e="T156" id="Seg_2258" s="T155">Jesus-ACC</ta>
            <ta e="T157" id="Seg_2259" s="T156">Hausherr.[NOM]</ta>
            <ta e="T158" id="Seg_2260" s="T157">sprechen-PST2.[3SG]</ta>
            <ta e="T159" id="Seg_2261" s="T158">3PL-DAT/LOC</ta>
            <ta e="T160" id="Seg_2262" s="T159">dieses.[NOM]</ta>
            <ta e="T161" id="Seg_2263" s="T160">1SG.[NOM]</ta>
            <ta e="T162" id="Seg_2264" s="T161">dieses-ACC</ta>
            <ta e="T163" id="Seg_2265" s="T162">hören-CVB.ANT</ta>
            <ta e="T164" id="Seg_2266" s="T163">Heer.[NOM]</ta>
            <ta e="T165" id="Seg_2267" s="T164">Volk-3SG.[NOM]</ta>
            <ta e="T166" id="Seg_2268" s="T165">Hinterteil-3PL-INSTR</ta>
            <ta e="T167" id="Seg_2269" s="T166">go-PST2-3PL</ta>
            <ta e="T168" id="Seg_2270" s="T167">dort</ta>
            <ta e="T169" id="Seg_2271" s="T168">Erde-DAT/LOC</ta>
            <ta e="T170" id="Seg_2272" s="T169">umwerfen-ADVZ</ta>
            <ta e="T171" id="Seg_2273" s="T170">fallen-EMOT-PST2-3PL</ta>
            <ta e="T172" id="Seg_2274" s="T171">dann</ta>
            <ta e="T173" id="Seg_2275" s="T172">Jesus.[NOM]</ta>
            <ta e="T174" id="Seg_2276" s="T173">sprechen-PST2.[3SG]</ta>
            <ta e="T175" id="Seg_2277" s="T174">3PL-DAT/LOC</ta>
            <ta e="T176" id="Seg_2278" s="T175">2PL.[NOM]</ta>
            <ta e="T177" id="Seg_2279" s="T176">töten-PTCP.FUT-AG-SIM</ta>
            <ta e="T178" id="Seg_2280" s="T177">suchen-PTCP.PRS.[NOM]</ta>
            <ta e="T179" id="Seg_2281" s="T178">ähnlich</ta>
            <ta e="T180" id="Seg_2282" s="T179">hinausgehen-PST1-2PL</ta>
            <ta e="T181" id="Seg_2283" s="T180">Schwert-PROPR.[NOM]</ta>
            <ta e="T182" id="Seg_2284" s="T181">Pflock.[NOM]</ta>
            <ta e="T183" id="Seg_2285" s="T182">Holz-PROPR</ta>
            <ta e="T184" id="Seg_2286" s="T183">1SG-ACC</ta>
            <ta e="T185" id="Seg_2287" s="T184">greifen-CVB.PURP</ta>
            <ta e="T186" id="Seg_2288" s="T185">Tag.[NOM]</ta>
            <ta e="T187" id="Seg_2289" s="T186">jeder</ta>
            <ta e="T188" id="Seg_2290" s="T187">1SG.[NOM]</ta>
            <ta e="T189" id="Seg_2291" s="T188">es.gibt</ta>
            <ta e="T190" id="Seg_2292" s="T189">sein-PST1-1SG</ta>
            <ta e="T191" id="Seg_2293" s="T190">2PL-ACC</ta>
            <ta e="T192" id="Seg_2294" s="T191">mit</ta>
            <ta e="T193" id="Seg_2295" s="T192">lernen-CVB.SIM</ta>
            <ta e="T196" id="Seg_2296" s="T195">Gott.[NOM]</ta>
            <ta e="T197" id="Seg_2297" s="T196">Haus-3SG-DAT/LOC</ta>
            <ta e="T198" id="Seg_2298" s="T197">dort</ta>
            <ta e="T199" id="Seg_2299" s="T198">2PL.[NOM]</ta>
            <ta e="T200" id="Seg_2300" s="T199">1SG-ACC</ta>
            <ta e="T201" id="Seg_2301" s="T200">bekommen-PST2.NEG-1PL</ta>
            <ta e="T202" id="Seg_2302" s="T201">jetzt</ta>
            <ta e="T203" id="Seg_2303" s="T202">kommen-PST2-3SG</ta>
            <ta e="T204" id="Seg_2304" s="T203">2PL.[NOM]</ta>
            <ta e="T205" id="Seg_2305" s="T204">Zeit-2PL.[NOM]</ta>
            <ta e="T206" id="Seg_2306" s="T205">dunkel</ta>
            <ta e="T207" id="Seg_2307" s="T206">Bestimmung.[NOM]</ta>
            <ta e="T208" id="Seg_2308" s="T207">fern-PROPR</ta>
            <ta e="T209" id="Seg_2309" s="T208">Zeit.[NOM]</ta>
            <ta e="T210" id="Seg_2310" s="T209">kommen-FUT-3PL</ta>
            <ta e="T211" id="Seg_2311" s="T210">Prophet-PL.[NOM]</ta>
            <ta e="T212" id="Seg_2312" s="T211">schreiben-PTCP.PST</ta>
            <ta e="T213" id="Seg_2313" s="T212">Wort-3PL.[NOM]</ta>
            <ta e="T214" id="Seg_2314" s="T213">Heer.[NOM]</ta>
            <ta e="T215" id="Seg_2315" s="T214">Volk-3SG.[NOM]</ta>
            <ta e="T216" id="Seg_2316" s="T215">greifen-CVB.SEQ</ta>
            <ta e="T217" id="Seg_2317" s="T216">nehmen-CVB.ANT</ta>
            <ta e="T218" id="Seg_2318" s="T217">Jesus</ta>
            <ta e="T219" id="Seg_2319" s="T218">Christus-ACC</ta>
            <ta e="T220" id="Seg_2320" s="T219">3SG-ACC</ta>
            <ta e="T221" id="Seg_2321" s="T220">tragen-PST2-3PL</ta>
            <ta e="T222" id="Seg_2322" s="T221">führend</ta>
            <ta e="T223" id="Seg_2323" s="T222">Geistlicher.[NOM]</ta>
            <ta e="T224" id="Seg_2324" s="T223">Haus-3SG-DAT/LOC</ta>
            <ta e="T225" id="Seg_2325" s="T224">lernen-PTCP.PRS</ta>
            <ta e="T226" id="Seg_2326" s="T225">Mensch-PL-3SG.[NOM]</ta>
            <ta e="T227" id="Seg_2327" s="T226">aber</ta>
            <ta e="T228" id="Seg_2328" s="T227">3SG-ACC</ta>
            <ta e="T229" id="Seg_2329" s="T228">lassen-CVB.SEQ</ta>
            <ta e="T230" id="Seg_2330" s="T229">gehen-PST2-3PL</ta>
            <ta e="T231" id="Seg_2331" s="T230">entfliehen-PST2-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_2332" s="T0">ребенок-PL-DAT/LOC</ta>
            <ta e="T2" id="Seg_2333" s="T1">читать-PRS-1PL</ta>
            <ta e="T3" id="Seg_2334" s="T2">рассказ-PL-ACC</ta>
            <ta e="T4" id="Seg_2335" s="T3">книга-ABL</ta>
            <ta e="T5" id="Seg_2336" s="T4">Исус</ta>
            <ta e="T6" id="Seg_2337" s="T5">Христос.[NOM]</ta>
            <ta e="T7" id="Seg_2338" s="T6">ребенок-PL.[NOM]</ta>
            <ta e="T8" id="Seg_2339" s="T7">друг-3PL.[NOM]</ta>
            <ta e="T9" id="Seg_2340" s="T8">говорить-CVB.SEQ</ta>
            <ta e="T10" id="Seg_2341" s="T9">Исус-ACC</ta>
            <ta e="T11" id="Seg_2342" s="T10">хватать-CVB.SEQ</ta>
            <ta e="T12" id="Seg_2343" s="T11">взять-PST2-3PL</ta>
            <ta e="T13" id="Seg_2344" s="T12">три-MLTP</ta>
            <ta e="T14" id="Seg_2345" s="T13">раз.[NOM]</ta>
            <ta e="T15" id="Seg_2346" s="T14">Исус.[NOM]</ta>
            <ta e="T16" id="Seg_2347" s="T15">приходить-CVB.SIM</ta>
            <ta e="T17" id="Seg_2348" s="T16">идти-EP-PST2.[3SG]</ta>
            <ta e="T18" id="Seg_2349" s="T17">учить-PTCP.PRS</ta>
            <ta e="T19" id="Seg_2350" s="T18">человек-PL-3SG-DAT/LOC</ta>
            <ta e="T20" id="Seg_2351" s="T19">там</ta>
            <ta e="T21" id="Seg_2352" s="T20">найти-CVB.ANT</ta>
            <ta e="T22" id="Seg_2353" s="T21">быть-PST2.[3SG]</ta>
            <ta e="T23" id="Seg_2354" s="T22">3PL-ACC</ta>
            <ta e="T24" id="Seg_2355" s="T23">видеть-PST2.[3SG]</ta>
            <ta e="T25" id="Seg_2356" s="T24">прямо</ta>
            <ta e="T26" id="Seg_2357" s="T25">спать-PTCP.PRS-PL-ACC</ta>
            <ta e="T27" id="Seg_2358" s="T26">три-ORD</ta>
            <ta e="T28" id="Seg_2359" s="T27">приходить-NMNZ-3SG-DAT/LOC</ta>
            <ta e="T29" id="Seg_2360" s="T28">3SG.[NOM]</ta>
            <ta e="T30" id="Seg_2361" s="T29">говорить-PST2.[3SG]</ta>
            <ta e="T31" id="Seg_2362" s="T30">3PL-DAT/LOC</ta>
            <ta e="T32" id="Seg_2363" s="T31">2PL.[NOM]</ta>
            <ta e="T33" id="Seg_2364" s="T32">теперь-DAT/LOC</ta>
            <ta e="T34" id="Seg_2365" s="T33">пока</ta>
            <ta e="T35" id="Seg_2366" s="T34">теперь</ta>
            <ta e="T36" id="Seg_2367" s="T35">еще</ta>
            <ta e="T37" id="Seg_2368" s="T36">спать-CVB.SIM</ta>
            <ta e="T38" id="Seg_2369" s="T37">лежать-PRS-2PL</ta>
            <ta e="T39" id="Seg_2370" s="T38">Q</ta>
            <ta e="T40" id="Seg_2371" s="T39">расслабиться-CVB.SEQ</ta>
            <ta e="T41" id="Seg_2372" s="T40">после</ta>
            <ta e="T42" id="Seg_2373" s="T41">вот</ta>
            <ta e="T43" id="Seg_2374" s="T42">приближаться-PST1-3SG</ta>
            <ta e="T44" id="Seg_2375" s="T43">час.[NOM]</ta>
            <ta e="T45" id="Seg_2376" s="T44">человек-ABL</ta>
            <ta e="T46" id="Seg_2377" s="T45">сын.[NOM]</ta>
            <ta e="T47" id="Seg_2378" s="T46">давать-PRS-3PL</ta>
            <ta e="T48" id="Seg_2379" s="T47">грех-PL-ACC</ta>
            <ta e="T49" id="Seg_2380" s="T48">делать-PTCP.PRS-PL.[NOM]</ta>
            <ta e="T50" id="Seg_2381" s="T49">человек.[NOM]</ta>
            <ta e="T51" id="Seg_2382" s="T50">человек.[NOM]</ta>
            <ta e="T52" id="Seg_2383" s="T51">каждый</ta>
            <ta e="T53" id="Seg_2384" s="T52">вставать-EP-IMP.2PL</ta>
            <ta e="T54" id="Seg_2385" s="T53">идти-IMP.1PL</ta>
            <ta e="T55" id="Seg_2386" s="T54">вот</ta>
            <ta e="T56" id="Seg_2387" s="T55">приближаться-PST1-3SG</ta>
            <ta e="T57" id="Seg_2388" s="T56">1SG-ACC</ta>
            <ta e="T58" id="Seg_2389" s="T57">смерть-DAT/LOC</ta>
            <ta e="T59" id="Seg_2390" s="T58">давать-CAP.[3SG]</ta>
            <ta e="T60" id="Seg_2391" s="T59">3SG.[NOM]</ta>
            <ta e="T61" id="Seg_2392" s="T60">так</ta>
            <ta e="T62" id="Seg_2393" s="T61">говорить-CVB.SIM</ta>
            <ta e="T63" id="Seg_2394" s="T62">стоять-TEMP-3SG</ta>
            <ta e="T64" id="Seg_2395" s="T63">приходить-PST2.[3SG]</ta>
            <ta e="T65" id="Seg_2396" s="T64">Иуда.[NOM]</ta>
            <ta e="T66" id="Seg_2397" s="T65">тот.[NOM]</ta>
            <ta e="T67" id="Seg_2398" s="T66">один</ta>
            <ta e="T68" id="Seg_2399" s="T67">учить-PTCP.PRS</ta>
            <ta e="T69" id="Seg_2400" s="T68">человек-3SG.[NOM]</ta>
            <ta e="T70" id="Seg_2401" s="T69">десять</ta>
            <ta e="T71" id="Seg_2402" s="T70">два-ABL</ta>
            <ta e="T72" id="Seg_2403" s="T71">3SG.[NOM]</ta>
            <ta e="T73" id="Seg_2404" s="T72">передовой</ta>
            <ta e="T74" id="Seg_2405" s="T73">священник-PL-DAT/LOC</ta>
            <ta e="T75" id="Seg_2406" s="T74">давать-PTCP.PST</ta>
            <ta e="T76" id="Seg_2407" s="T75">быть-PST2.[3SG]</ta>
            <ta e="T77" id="Seg_2408" s="T76">слово-3SG-ACC</ta>
            <ta e="T78" id="Seg_2409" s="T77">тридцать</ta>
            <ta e="T79" id="Seg_2410" s="T78">золото.[NOM]</ta>
            <ta e="T80" id="Seg_2411" s="T79">деньги-PL.[NOM]</ta>
            <ta e="T81" id="Seg_2412" s="T80">из_за</ta>
            <ta e="T82" id="Seg_2413" s="T81">Исус-ACC</ta>
            <ta e="T83" id="Seg_2414" s="T82">смерть-DAT/LOC</ta>
            <ta e="T84" id="Seg_2415" s="T83">давать-FUT-1SG</ta>
            <ta e="T85" id="Seg_2416" s="T84">говорить-CVB.SEQ</ta>
            <ta e="T86" id="Seg_2417" s="T85">Иуда-ACC</ta>
            <ta e="T87" id="Seg_2418" s="T86">с</ta>
            <ta e="T88" id="Seg_2419" s="T87">приходить-PST2-3PL</ta>
            <ta e="T89" id="Seg_2420" s="T88">много</ta>
            <ta e="T90" id="Seg_2421" s="T89">войско.[NOM]</ta>
            <ta e="T91" id="Seg_2422" s="T90">народ-3SG.[NOM]</ta>
            <ta e="T92" id="Seg_2423" s="T91">тогда</ta>
            <ta e="T93" id="Seg_2424" s="T92">разный</ta>
            <ta e="T94" id="Seg_2425" s="T93">человек-PL.[NOM]</ta>
            <ta e="T95" id="Seg_2426" s="T94">фарисей-PL-ABL</ta>
            <ta e="T96" id="Seg_2427" s="T95">передовой</ta>
            <ta e="T97" id="Seg_2428" s="T96">священник-PL-ABL</ta>
            <ta e="T98" id="Seg_2429" s="T97">рука-3PL-DAT/LOC</ta>
            <ta e="T99" id="Seg_2430" s="T98">котел-PROPR-3PL</ta>
            <ta e="T100" id="Seg_2431" s="T99">меч-PROPR-3PL</ta>
            <ta e="T101" id="Seg_2432" s="T100">тогда</ta>
            <ta e="T102" id="Seg_2433" s="T101">кол.[NOM]</ta>
            <ta e="T103" id="Seg_2434" s="T102">дерево-PROPR-3PL</ta>
            <ta e="T104" id="Seg_2435" s="T103">смерть-DAT/LOC</ta>
            <ta e="T105" id="Seg_2436" s="T104">давать-PTCP.PRS</ta>
            <ta e="T106" id="Seg_2437" s="T105">Иуда.[NOM]</ta>
            <ta e="T107" id="Seg_2438" s="T106">3PL-DAT/LOC</ta>
            <ta e="T108" id="Seg_2439" s="T107">говорить-PST2.[3SG]</ta>
            <ta e="T109" id="Seg_2440" s="T108">кто-ACC</ta>
            <ta e="T110" id="Seg_2441" s="T109">1SG.[NOM]</ta>
            <ta e="T111" id="Seg_2442" s="T110">целовать-FUT-1SG</ta>
            <ta e="T112" id="Seg_2443" s="T111">тот.[NOM]</ta>
            <ta e="T113" id="Seg_2444" s="T112">быть-FUT-3SG</ta>
            <ta e="T114" id="Seg_2445" s="T113">тогда</ta>
            <ta e="T115" id="Seg_2446" s="T114">тот-ACC</ta>
            <ta e="T116" id="Seg_2447" s="T115">поймать-CVB.SEQ</ta>
            <ta e="T117" id="Seg_2448" s="T116">взять-EP-IMP.2PL</ta>
            <ta e="T118" id="Seg_2449" s="T117">3SG.[NOM]</ta>
            <ta e="T119" id="Seg_2450" s="T118">приходить-PST2.[3SG]</ta>
            <ta e="T120" id="Seg_2451" s="T119">Исус.[NOM]</ta>
            <ta e="T121" id="Seg_2452" s="T120">место.около-DAT/LOC</ta>
            <ta e="T122" id="Seg_2453" s="T121">тогда</ta>
            <ta e="T123" id="Seg_2454" s="T122">говорить-PST2.[3SG]</ta>
            <ta e="T124" id="Seg_2455" s="T123">радоваться.[IMP.2SG]</ta>
            <ta e="T125" id="Seg_2456" s="T124">учитель.[NOM]</ta>
            <ta e="T126" id="Seg_2457" s="T125">так</ta>
            <ta e="T127" id="Seg_2458" s="T126">говорить-CVB.ANT</ta>
            <ta e="T128" id="Seg_2459" s="T127">целовать-PST2.[3SG]</ta>
            <ta e="T129" id="Seg_2460" s="T128">3SG-ACC</ta>
            <ta e="T130" id="Seg_2461" s="T129">Исус.[NOM]</ta>
            <ta e="T131" id="Seg_2462" s="T130">однако</ta>
            <ta e="T132" id="Seg_2463" s="T131">говорить-PST2.[3SG]</ta>
            <ta e="T133" id="Seg_2464" s="T132">3SG-DAT/LOC</ta>
            <ta e="T134" id="Seg_2465" s="T133">друг.[NOM]</ta>
            <ta e="T135" id="Seg_2466" s="T134">почему</ta>
            <ta e="T136" id="Seg_2467" s="T135">2SG.[NOM]</ta>
            <ta e="T137" id="Seg_2468" s="T136">приходить-PST1-2SG</ta>
            <ta e="T138" id="Seg_2469" s="T137">целовать-CVB.SEQ-2SG</ta>
            <ta e="T139" id="Seg_2470" s="T138">Q</ta>
            <ta e="T140" id="Seg_2471" s="T139">давать-PRS-2SG</ta>
            <ta e="T141" id="Seg_2472" s="T140">смерть-DAT/LOC</ta>
            <ta e="T142" id="Seg_2473" s="T141">человек-ABL</ta>
            <ta e="T143" id="Seg_2474" s="T142">сын-ACC</ta>
            <ta e="T144" id="Seg_2475" s="T143">Исус.[NOM]</ta>
            <ta e="T145" id="Seg_2476" s="T144">спрашивать-PST2.[3SG]</ta>
            <ta e="T146" id="Seg_2477" s="T145">Иуда-ACC</ta>
            <ta e="T147" id="Seg_2478" s="T146">с</ta>
            <ta e="T148" id="Seg_2479" s="T147">приходить-PTCP.PST</ta>
            <ta e="T149" id="Seg_2480" s="T148">войско.[NOM]</ta>
            <ta e="T150" id="Seg_2481" s="T149">народ-3SG-ABL</ta>
            <ta e="T151" id="Seg_2482" s="T150">кто-ACC</ta>
            <ta e="T152" id="Seg_2483" s="T151">искать-PRS-2PL</ta>
            <ta e="T153" id="Seg_2484" s="T152">тот-3SG-3PL.[NOM]</ta>
            <ta e="T154" id="Seg_2485" s="T153">говорить-PST2-3PL</ta>
            <ta e="T155" id="Seg_2486" s="T154">Назарет-ABL</ta>
            <ta e="T156" id="Seg_2487" s="T155">Исус-ACC</ta>
            <ta e="T157" id="Seg_2488" s="T156">хозяин.[NOM]</ta>
            <ta e="T158" id="Seg_2489" s="T157">говорить-PST2.[3SG]</ta>
            <ta e="T159" id="Seg_2490" s="T158">3PL-DAT/LOC</ta>
            <ta e="T160" id="Seg_2491" s="T159">тот.[NOM]</ta>
            <ta e="T161" id="Seg_2492" s="T160">1SG.[NOM]</ta>
            <ta e="T162" id="Seg_2493" s="T161">тот-ACC</ta>
            <ta e="T163" id="Seg_2494" s="T162">слышать-CVB.ANT</ta>
            <ta e="T164" id="Seg_2495" s="T163">войско.[NOM]</ta>
            <ta e="T165" id="Seg_2496" s="T164">народ-3SG.[NOM]</ta>
            <ta e="T166" id="Seg_2497" s="T165">задняя.часть-3PL-INSTR</ta>
            <ta e="T167" id="Seg_2498" s="T166">идти-PST2-3PL</ta>
            <ta e="T168" id="Seg_2499" s="T167">там</ta>
            <ta e="T169" id="Seg_2500" s="T168">земля-DAT/LOC</ta>
            <ta e="T170" id="Seg_2501" s="T169">опрокинуть-ADVZ</ta>
            <ta e="T171" id="Seg_2502" s="T170">падать-EMOT-PST2-3PL</ta>
            <ta e="T172" id="Seg_2503" s="T171">потом</ta>
            <ta e="T173" id="Seg_2504" s="T172">Исус.[NOM]</ta>
            <ta e="T174" id="Seg_2505" s="T173">говорить-PST2.[3SG]</ta>
            <ta e="T175" id="Seg_2506" s="T174">3PL-DAT/LOC</ta>
            <ta e="T176" id="Seg_2507" s="T175">2PL.[NOM]</ta>
            <ta e="T177" id="Seg_2508" s="T176">убить-PTCP.FUT-AG-SIM</ta>
            <ta e="T178" id="Seg_2509" s="T177">искать-PTCP.PRS.[NOM]</ta>
            <ta e="T179" id="Seg_2510" s="T178">подобно</ta>
            <ta e="T180" id="Seg_2511" s="T179">выйти-PST1-2PL</ta>
            <ta e="T181" id="Seg_2512" s="T180">меч-PROPR.[NOM]</ta>
            <ta e="T182" id="Seg_2513" s="T181">кол.[NOM]</ta>
            <ta e="T183" id="Seg_2514" s="T182">дерево-PROPR</ta>
            <ta e="T184" id="Seg_2515" s="T183">1SG-ACC</ta>
            <ta e="T185" id="Seg_2516" s="T184">хватать-CVB.PURP</ta>
            <ta e="T186" id="Seg_2517" s="T185">день.[NOM]</ta>
            <ta e="T187" id="Seg_2518" s="T186">каждый</ta>
            <ta e="T188" id="Seg_2519" s="T187">1SG.[NOM]</ta>
            <ta e="T189" id="Seg_2520" s="T188">есть</ta>
            <ta e="T190" id="Seg_2521" s="T189">быть-PST1-1SG</ta>
            <ta e="T191" id="Seg_2522" s="T190">2PL-ACC</ta>
            <ta e="T192" id="Seg_2523" s="T191">с</ta>
            <ta e="T193" id="Seg_2524" s="T192">учить-CVB.SIM</ta>
            <ta e="T196" id="Seg_2525" s="T195">Бог.[NOM]</ta>
            <ta e="T197" id="Seg_2526" s="T196">дом-3SG-DAT/LOC</ta>
            <ta e="T198" id="Seg_2527" s="T197">там</ta>
            <ta e="T199" id="Seg_2528" s="T198">2PL.[NOM]</ta>
            <ta e="T200" id="Seg_2529" s="T199">1SG-ACC</ta>
            <ta e="T201" id="Seg_2530" s="T200">получить-PST2.NEG-1PL</ta>
            <ta e="T202" id="Seg_2531" s="T201">теперь</ta>
            <ta e="T203" id="Seg_2532" s="T202">приходить-PST2-3SG</ta>
            <ta e="T204" id="Seg_2533" s="T203">2PL.[NOM]</ta>
            <ta e="T205" id="Seg_2534" s="T204">час-2PL.[NOM]</ta>
            <ta e="T206" id="Seg_2535" s="T205">темный</ta>
            <ta e="T207" id="Seg_2536" s="T206">предназначение.[NOM]</ta>
            <ta e="T208" id="Seg_2537" s="T207">далекий-PROPR</ta>
            <ta e="T209" id="Seg_2538" s="T208">час.[NOM]</ta>
            <ta e="T210" id="Seg_2539" s="T209">приходить-FUT-3PL</ta>
            <ta e="T211" id="Seg_2540" s="T210">пророк-PL.[NOM]</ta>
            <ta e="T212" id="Seg_2541" s="T211">писать-PTCP.PST</ta>
            <ta e="T213" id="Seg_2542" s="T212">слово-3PL.[NOM]</ta>
            <ta e="T214" id="Seg_2543" s="T213">войско.[NOM]</ta>
            <ta e="T215" id="Seg_2544" s="T214">народ-3SG.[NOM]</ta>
            <ta e="T216" id="Seg_2545" s="T215">хватать-CVB.SEQ</ta>
            <ta e="T217" id="Seg_2546" s="T216">взять-CVB.ANT</ta>
            <ta e="T218" id="Seg_2547" s="T217">Исус</ta>
            <ta e="T219" id="Seg_2548" s="T218">Христос-ACC</ta>
            <ta e="T220" id="Seg_2549" s="T219">3SG-ACC</ta>
            <ta e="T221" id="Seg_2550" s="T220">носить-PST2-3PL</ta>
            <ta e="T222" id="Seg_2551" s="T221">передовой</ta>
            <ta e="T223" id="Seg_2552" s="T222">священник.[NOM]</ta>
            <ta e="T224" id="Seg_2553" s="T223">дом-3SG-DAT/LOC</ta>
            <ta e="T225" id="Seg_2554" s="T224">учить-PTCP.PRS</ta>
            <ta e="T226" id="Seg_2555" s="T225">человек-PL-3SG.[NOM]</ta>
            <ta e="T227" id="Seg_2556" s="T226">однако</ta>
            <ta e="T228" id="Seg_2557" s="T227">3SG-ACC</ta>
            <ta e="T229" id="Seg_2558" s="T228">оставлять-CVB.SEQ</ta>
            <ta e="T230" id="Seg_2559" s="T229">идти-PST2-3PL</ta>
            <ta e="T231" id="Seg_2560" s="T230">спасаться-PST2-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_2561" s="T0">n-n:(num)-n:case</ta>
            <ta e="T2" id="Seg_2562" s="T1">v-v:tense-v:pred.pn</ta>
            <ta e="T3" id="Seg_2563" s="T2">n-n:(num)-n:case</ta>
            <ta e="T4" id="Seg_2564" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_2565" s="T4">propr</ta>
            <ta e="T6" id="Seg_2566" s="T5">propr-n:case</ta>
            <ta e="T7" id="Seg_2567" s="T6">n-n:(num)-n:case</ta>
            <ta e="T8" id="Seg_2568" s="T7">n-n:(poss)-n:case</ta>
            <ta e="T9" id="Seg_2569" s="T8">v-v:cvb</ta>
            <ta e="T10" id="Seg_2570" s="T9">propr-n:case</ta>
            <ta e="T11" id="Seg_2571" s="T10">v-v:cvb</ta>
            <ta e="T12" id="Seg_2572" s="T11">v-v:tense-v:pred.pn</ta>
            <ta e="T13" id="Seg_2573" s="T12">cardnum-cardnum&gt;adv</ta>
            <ta e="T14" id="Seg_2574" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_2575" s="T14">propr-n:case</ta>
            <ta e="T16" id="Seg_2576" s="T15">v-v:cvb</ta>
            <ta e="T17" id="Seg_2577" s="T16">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T18" id="Seg_2578" s="T17">v-v:ptcp</ta>
            <ta e="T19" id="Seg_2579" s="T18">n-n:(num)-n:poss-n:case</ta>
            <ta e="T20" id="Seg_2580" s="T19">adv</ta>
            <ta e="T21" id="Seg_2581" s="T20">v-v:cvb</ta>
            <ta e="T22" id="Seg_2582" s="T21">v-v:tense-v:pred.pn</ta>
            <ta e="T23" id="Seg_2583" s="T22">pers-pro:case</ta>
            <ta e="T24" id="Seg_2584" s="T23">v-v:tense-v:pred.pn</ta>
            <ta e="T25" id="Seg_2585" s="T24">adv</ta>
            <ta e="T26" id="Seg_2586" s="T25">v-v:ptcp-v:(num)-v:(case)</ta>
            <ta e="T27" id="Seg_2587" s="T26">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T28" id="Seg_2588" s="T27">v-v&gt;n-n:poss-n:case</ta>
            <ta e="T29" id="Seg_2589" s="T28">pers-pro:case</ta>
            <ta e="T30" id="Seg_2590" s="T29">v-v:tense-v:pred.pn</ta>
            <ta e="T31" id="Seg_2591" s="T30">pers-pro:case</ta>
            <ta e="T32" id="Seg_2592" s="T31">pers-pro:case</ta>
            <ta e="T33" id="Seg_2593" s="T32">adv-n:case</ta>
            <ta e="T34" id="Seg_2594" s="T33">post</ta>
            <ta e="T35" id="Seg_2595" s="T34">adv</ta>
            <ta e="T36" id="Seg_2596" s="T35">adv</ta>
            <ta e="T37" id="Seg_2597" s="T36">v-v:cvb</ta>
            <ta e="T38" id="Seg_2598" s="T37">v-v:tense-v:pred.pn</ta>
            <ta e="T39" id="Seg_2599" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_2600" s="T39">v-v:cvb</ta>
            <ta e="T41" id="Seg_2601" s="T40">post</ta>
            <ta e="T42" id="Seg_2602" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_2603" s="T42">v-v:tense-v:poss.pn</ta>
            <ta e="T44" id="Seg_2604" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_2605" s="T44">n-n:case</ta>
            <ta e="T46" id="Seg_2606" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_2607" s="T46">v-v:tense-v:pred.pn</ta>
            <ta e="T48" id="Seg_2608" s="T47">n-n:(num)-n:case</ta>
            <ta e="T49" id="Seg_2609" s="T48">v-v:ptcp-v:(num)-v:(case)</ta>
            <ta e="T50" id="Seg_2610" s="T49">n-n:case</ta>
            <ta e="T51" id="Seg_2611" s="T50">n-n:case</ta>
            <ta e="T52" id="Seg_2612" s="T51">adj</ta>
            <ta e="T53" id="Seg_2613" s="T52">v-v:(ins)-v:mood.pn</ta>
            <ta e="T54" id="Seg_2614" s="T53">v-v:mood.pn</ta>
            <ta e="T55" id="Seg_2615" s="T54">ptcl</ta>
            <ta e="T56" id="Seg_2616" s="T55">v-v:tense-v:poss.pn</ta>
            <ta e="T57" id="Seg_2617" s="T56">pers-pro:case</ta>
            <ta e="T58" id="Seg_2618" s="T57">n-n:case</ta>
            <ta e="T59" id="Seg_2619" s="T58">v-v:mood-v:pred.pn</ta>
            <ta e="T60" id="Seg_2620" s="T59">pers-pro:case</ta>
            <ta e="T61" id="Seg_2621" s="T60">adv</ta>
            <ta e="T62" id="Seg_2622" s="T61">v-v:cvb</ta>
            <ta e="T63" id="Seg_2623" s="T62">v-v:mood-v:temp.pn</ta>
            <ta e="T64" id="Seg_2624" s="T63">v-v:tense-v:pred.pn</ta>
            <ta e="T65" id="Seg_2625" s="T64">propr-n:case</ta>
            <ta e="T66" id="Seg_2626" s="T65">dempro-pro:case</ta>
            <ta e="T67" id="Seg_2627" s="T66">cardnum</ta>
            <ta e="T68" id="Seg_2628" s="T67">v-v:ptcp</ta>
            <ta e="T69" id="Seg_2629" s="T68">n-n:(poss)-n:case</ta>
            <ta e="T70" id="Seg_2630" s="T69">cardnum</ta>
            <ta e="T71" id="Seg_2631" s="T70">cardnum-n:case</ta>
            <ta e="T72" id="Seg_2632" s="T71">pers-pro:case</ta>
            <ta e="T73" id="Seg_2633" s="T72">adj</ta>
            <ta e="T74" id="Seg_2634" s="T73">n-n:(num)-n:case</ta>
            <ta e="T75" id="Seg_2635" s="T74">v-v:ptcp</ta>
            <ta e="T76" id="Seg_2636" s="T75">v-v:tense-v:pred.pn</ta>
            <ta e="T77" id="Seg_2637" s="T76">n-n:poss-n:case</ta>
            <ta e="T78" id="Seg_2638" s="T77">cardnum</ta>
            <ta e="T79" id="Seg_2639" s="T78">n-n:case</ta>
            <ta e="T80" id="Seg_2640" s="T79">n-n:(num)-n:case</ta>
            <ta e="T81" id="Seg_2641" s="T80">post</ta>
            <ta e="T82" id="Seg_2642" s="T81">propr-n:case</ta>
            <ta e="T83" id="Seg_2643" s="T82">n-n:case</ta>
            <ta e="T84" id="Seg_2644" s="T83">v-v:tense-v:poss.pn</ta>
            <ta e="T85" id="Seg_2645" s="T84">v-v:cvb</ta>
            <ta e="T86" id="Seg_2646" s="T85">propr-n:case</ta>
            <ta e="T87" id="Seg_2647" s="T86">post</ta>
            <ta e="T88" id="Seg_2648" s="T87">v-v:tense-v:poss.pn</ta>
            <ta e="T89" id="Seg_2649" s="T88">quant</ta>
            <ta e="T90" id="Seg_2650" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_2651" s="T90">n-n:(poss)-n:case</ta>
            <ta e="T92" id="Seg_2652" s="T91">adv</ta>
            <ta e="T93" id="Seg_2653" s="T92">adj</ta>
            <ta e="T94" id="Seg_2654" s="T93">n-n:(num)-n:case</ta>
            <ta e="T95" id="Seg_2655" s="T94">n-n:(num)-n:case</ta>
            <ta e="T96" id="Seg_2656" s="T95">adj</ta>
            <ta e="T97" id="Seg_2657" s="T96">n-n:(num)-n:case</ta>
            <ta e="T98" id="Seg_2658" s="T97">n-n:poss-n:case</ta>
            <ta e="T99" id="Seg_2659" s="T98">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T100" id="Seg_2660" s="T99">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T101" id="Seg_2661" s="T100">adv</ta>
            <ta e="T102" id="Seg_2662" s="T101">n-n:case</ta>
            <ta e="T103" id="Seg_2663" s="T102">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T104" id="Seg_2664" s="T103">n-n:case</ta>
            <ta e="T105" id="Seg_2665" s="T104">v-v:ptcp</ta>
            <ta e="T106" id="Seg_2666" s="T105">propr-n:case</ta>
            <ta e="T107" id="Seg_2667" s="T106">pers-pro:case</ta>
            <ta e="T108" id="Seg_2668" s="T107">v-v:tense-v:pred.pn</ta>
            <ta e="T109" id="Seg_2669" s="T108">que-pro:case</ta>
            <ta e="T110" id="Seg_2670" s="T109">pers-pro:case</ta>
            <ta e="T111" id="Seg_2671" s="T110">v-v:tense-v:poss.pn</ta>
            <ta e="T112" id="Seg_2672" s="T111">dempro-pro:case</ta>
            <ta e="T113" id="Seg_2673" s="T112">v-v:tense-v:poss.pn</ta>
            <ta e="T114" id="Seg_2674" s="T113">adv</ta>
            <ta e="T115" id="Seg_2675" s="T114">dempro-pro:case</ta>
            <ta e="T116" id="Seg_2676" s="T115">v-v:cvb</ta>
            <ta e="T117" id="Seg_2677" s="T116">v-v:(ins)-v:mood.pn</ta>
            <ta e="T118" id="Seg_2678" s="T117">pers-pro:case</ta>
            <ta e="T119" id="Seg_2679" s="T118">v-v:tense-v:pred.pn</ta>
            <ta e="T120" id="Seg_2680" s="T119">propr-n:case</ta>
            <ta e="T121" id="Seg_2681" s="T120">n-n:case</ta>
            <ta e="T122" id="Seg_2682" s="T121">adv</ta>
            <ta e="T123" id="Seg_2683" s="T122">v-v:tense-v:pred.pn</ta>
            <ta e="T124" id="Seg_2684" s="T123">v-v:mood.pn</ta>
            <ta e="T125" id="Seg_2685" s="T124">n-n:case</ta>
            <ta e="T126" id="Seg_2686" s="T125">dempro</ta>
            <ta e="T127" id="Seg_2687" s="T126">v-v:cvb</ta>
            <ta e="T128" id="Seg_2688" s="T127">v-v:tense-v:pred.pn</ta>
            <ta e="T129" id="Seg_2689" s="T128">pers-pro:case</ta>
            <ta e="T130" id="Seg_2690" s="T129">propr-n:case</ta>
            <ta e="T131" id="Seg_2691" s="T130">ptcl</ta>
            <ta e="T132" id="Seg_2692" s="T131">v-v:tense-v:pred.pn</ta>
            <ta e="T133" id="Seg_2693" s="T132">pers-pro:case</ta>
            <ta e="T134" id="Seg_2694" s="T133">n-n:case</ta>
            <ta e="T135" id="Seg_2695" s="T134">que</ta>
            <ta e="T136" id="Seg_2696" s="T135">pers-pro:case</ta>
            <ta e="T137" id="Seg_2697" s="T136">v-v:tense-v:poss.pn</ta>
            <ta e="T138" id="Seg_2698" s="T137">v-v:cvb-v:pred.pn</ta>
            <ta e="T139" id="Seg_2699" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_2700" s="T139">v-v:tense-v:pred.pn</ta>
            <ta e="T141" id="Seg_2701" s="T140">n-n:case</ta>
            <ta e="T142" id="Seg_2702" s="T141">n-n:case</ta>
            <ta e="T143" id="Seg_2703" s="T142">n-n:case</ta>
            <ta e="T144" id="Seg_2704" s="T143">propr-n:case</ta>
            <ta e="T145" id="Seg_2705" s="T144">v-v:tense-v:pred.pn</ta>
            <ta e="T146" id="Seg_2706" s="T145">propr-n:case</ta>
            <ta e="T147" id="Seg_2707" s="T146">post</ta>
            <ta e="T148" id="Seg_2708" s="T147">v-v:ptcp</ta>
            <ta e="T149" id="Seg_2709" s="T148">n-n:case</ta>
            <ta e="T150" id="Seg_2710" s="T149">n-n:poss-n:case</ta>
            <ta e="T151" id="Seg_2711" s="T150">que-pro:case</ta>
            <ta e="T152" id="Seg_2712" s="T151">v-v:tense-v:pred.pn</ta>
            <ta e="T153" id="Seg_2713" s="T152">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T154" id="Seg_2714" s="T153">v-v:tense-v:pred.pn</ta>
            <ta e="T155" id="Seg_2715" s="T154">propr-n:case</ta>
            <ta e="T156" id="Seg_2716" s="T155">propr-n:case</ta>
            <ta e="T157" id="Seg_2717" s="T156">n-n:case</ta>
            <ta e="T158" id="Seg_2718" s="T157">v-v:tense-v:pred.pn</ta>
            <ta e="T159" id="Seg_2719" s="T158">pers-pro:case</ta>
            <ta e="T160" id="Seg_2720" s="T159">dempro-pro:case</ta>
            <ta e="T161" id="Seg_2721" s="T160">pers-pro:case</ta>
            <ta e="T162" id="Seg_2722" s="T161">dempro-pro:case</ta>
            <ta e="T163" id="Seg_2723" s="T162">v-v:cvb</ta>
            <ta e="T164" id="Seg_2724" s="T163">n-n:case</ta>
            <ta e="T165" id="Seg_2725" s="T164">n-n:(poss)-n:case</ta>
            <ta e="T166" id="Seg_2726" s="T165">n-n:poss-n:case</ta>
            <ta e="T167" id="Seg_2727" s="T166">v-v:tense-v:pred.pn</ta>
            <ta e="T168" id="Seg_2728" s="T167">adv</ta>
            <ta e="T169" id="Seg_2729" s="T168">n-n:case</ta>
            <ta e="T170" id="Seg_2730" s="T169">v-v&gt;adv</ta>
            <ta e="T171" id="Seg_2731" s="T170">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T172" id="Seg_2732" s="T171">adv</ta>
            <ta e="T173" id="Seg_2733" s="T172">propr-n:case</ta>
            <ta e="T174" id="Seg_2734" s="T173">v-v:tense-v:pred.pn</ta>
            <ta e="T175" id="Seg_2735" s="T174">pers-pro:case</ta>
            <ta e="T176" id="Seg_2736" s="T175">pers-pro:case</ta>
            <ta e="T177" id="Seg_2737" s="T176">v-v:ptcp-v&gt;n-n&gt;adv</ta>
            <ta e="T178" id="Seg_2738" s="T177">v-v:ptcp-v:(case)</ta>
            <ta e="T179" id="Seg_2739" s="T178">post</ta>
            <ta e="T180" id="Seg_2740" s="T179">v-v:tense-v:poss.pn</ta>
            <ta e="T181" id="Seg_2741" s="T180">n-n&gt;adj-n:case</ta>
            <ta e="T182" id="Seg_2742" s="T181">n-n:case</ta>
            <ta e="T183" id="Seg_2743" s="T182">n-n&gt;adj</ta>
            <ta e="T184" id="Seg_2744" s="T183">pers-pro:case</ta>
            <ta e="T185" id="Seg_2745" s="T184">v-v:cvb</ta>
            <ta e="T186" id="Seg_2746" s="T185">n-n:case</ta>
            <ta e="T187" id="Seg_2747" s="T186">adj</ta>
            <ta e="T188" id="Seg_2748" s="T187">pers-pro:case</ta>
            <ta e="T189" id="Seg_2749" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_2750" s="T189">v-v:tense-v:poss.pn</ta>
            <ta e="T191" id="Seg_2751" s="T190">pers-pro:case</ta>
            <ta e="T192" id="Seg_2752" s="T191">post</ta>
            <ta e="T193" id="Seg_2753" s="T192">v-v:cvb</ta>
            <ta e="T196" id="Seg_2754" s="T195">n-n:case</ta>
            <ta e="T197" id="Seg_2755" s="T196">n-n:poss-n:case</ta>
            <ta e="T198" id="Seg_2756" s="T197">adv</ta>
            <ta e="T199" id="Seg_2757" s="T198">pers-pro:case</ta>
            <ta e="T200" id="Seg_2758" s="T199">pers-pro:case</ta>
            <ta e="T201" id="Seg_2759" s="T200">v-v:neg-v:pred.pn</ta>
            <ta e="T202" id="Seg_2760" s="T201">adv</ta>
            <ta e="T203" id="Seg_2761" s="T202">v-v:tense-v:poss.pn</ta>
            <ta e="T204" id="Seg_2762" s="T203">pers-pro:case</ta>
            <ta e="T205" id="Seg_2763" s="T204">n-n:(poss)-n:case</ta>
            <ta e="T206" id="Seg_2764" s="T205">adj</ta>
            <ta e="T207" id="Seg_2765" s="T206">n-n:case</ta>
            <ta e="T208" id="Seg_2766" s="T207">adj-adj&gt;adj</ta>
            <ta e="T209" id="Seg_2767" s="T208">n-n:case</ta>
            <ta e="T210" id="Seg_2768" s="T209">v-v:tense-v:poss.pn</ta>
            <ta e="T211" id="Seg_2769" s="T210">n-n:(num)-n:case</ta>
            <ta e="T212" id="Seg_2770" s="T211">v-v:ptcp</ta>
            <ta e="T213" id="Seg_2771" s="T212">n-n:(poss)-n:case</ta>
            <ta e="T214" id="Seg_2772" s="T213">n-n:case</ta>
            <ta e="T215" id="Seg_2773" s="T214">n-n:(poss)-n:case</ta>
            <ta e="T216" id="Seg_2774" s="T215">v-v:cvb</ta>
            <ta e="T217" id="Seg_2775" s="T216">v-v:cvb</ta>
            <ta e="T218" id="Seg_2776" s="T217">propr</ta>
            <ta e="T219" id="Seg_2777" s="T218">propr-n:case</ta>
            <ta e="T220" id="Seg_2778" s="T219">pers-pro:case</ta>
            <ta e="T221" id="Seg_2779" s="T220">v-v:tense-v:poss.pn</ta>
            <ta e="T222" id="Seg_2780" s="T221">adj</ta>
            <ta e="T223" id="Seg_2781" s="T222">n-n:case</ta>
            <ta e="T224" id="Seg_2782" s="T223">n-n:poss-n:case</ta>
            <ta e="T225" id="Seg_2783" s="T224">v-v:ptcp</ta>
            <ta e="T226" id="Seg_2784" s="T225">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T227" id="Seg_2785" s="T226">ptcl</ta>
            <ta e="T228" id="Seg_2786" s="T227">pers-pro:case</ta>
            <ta e="T229" id="Seg_2787" s="T228">v-v:cvb</ta>
            <ta e="T230" id="Seg_2788" s="T229">v-v:tense-v:pred.pn</ta>
            <ta e="T231" id="Seg_2789" s="T230">v-v:tense-v:pred.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_2790" s="T0">n</ta>
            <ta e="T2" id="Seg_2791" s="T1">v</ta>
            <ta e="T3" id="Seg_2792" s="T2">n</ta>
            <ta e="T4" id="Seg_2793" s="T3">n</ta>
            <ta e="T5" id="Seg_2794" s="T4">propr</ta>
            <ta e="T6" id="Seg_2795" s="T5">propr</ta>
            <ta e="T7" id="Seg_2796" s="T6">n</ta>
            <ta e="T8" id="Seg_2797" s="T7">n</ta>
            <ta e="T9" id="Seg_2798" s="T8">v</ta>
            <ta e="T10" id="Seg_2799" s="T9">propr</ta>
            <ta e="T11" id="Seg_2800" s="T10">v</ta>
            <ta e="T12" id="Seg_2801" s="T11">aux</ta>
            <ta e="T13" id="Seg_2802" s="T12">adv</ta>
            <ta e="T14" id="Seg_2803" s="T13">n</ta>
            <ta e="T15" id="Seg_2804" s="T14">propr</ta>
            <ta e="T16" id="Seg_2805" s="T15">v</ta>
            <ta e="T17" id="Seg_2806" s="T16">aux</ta>
            <ta e="T18" id="Seg_2807" s="T17">v</ta>
            <ta e="T19" id="Seg_2808" s="T18">n</ta>
            <ta e="T20" id="Seg_2809" s="T19">adv</ta>
            <ta e="T21" id="Seg_2810" s="T20">v</ta>
            <ta e="T22" id="Seg_2811" s="T21">aux</ta>
            <ta e="T23" id="Seg_2812" s="T22">pers</ta>
            <ta e="T24" id="Seg_2813" s="T23">v</ta>
            <ta e="T25" id="Seg_2814" s="T24">adv</ta>
            <ta e="T26" id="Seg_2815" s="T25">v</ta>
            <ta e="T27" id="Seg_2816" s="T26">ordnum</ta>
            <ta e="T28" id="Seg_2817" s="T27">n</ta>
            <ta e="T29" id="Seg_2818" s="T28">pers</ta>
            <ta e="T30" id="Seg_2819" s="T29">v</ta>
            <ta e="T31" id="Seg_2820" s="T30">pers</ta>
            <ta e="T32" id="Seg_2821" s="T31">pers</ta>
            <ta e="T33" id="Seg_2822" s="T32">adv</ta>
            <ta e="T34" id="Seg_2823" s="T33">post</ta>
            <ta e="T35" id="Seg_2824" s="T34">adv</ta>
            <ta e="T36" id="Seg_2825" s="T35">adv</ta>
            <ta e="T37" id="Seg_2826" s="T36">v</ta>
            <ta e="T38" id="Seg_2827" s="T37">aux</ta>
            <ta e="T39" id="Seg_2828" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_2829" s="T39">v</ta>
            <ta e="T41" id="Seg_2830" s="T40">post</ta>
            <ta e="T42" id="Seg_2831" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_2832" s="T42">v</ta>
            <ta e="T44" id="Seg_2833" s="T43">n</ta>
            <ta e="T45" id="Seg_2834" s="T44">n</ta>
            <ta e="T46" id="Seg_2835" s="T45">n</ta>
            <ta e="T47" id="Seg_2836" s="T46">v</ta>
            <ta e="T48" id="Seg_2837" s="T47">n</ta>
            <ta e="T49" id="Seg_2838" s="T48">v</ta>
            <ta e="T50" id="Seg_2839" s="T49">n</ta>
            <ta e="T51" id="Seg_2840" s="T50">n</ta>
            <ta e="T52" id="Seg_2841" s="T51">adj</ta>
            <ta e="T53" id="Seg_2842" s="T52">v</ta>
            <ta e="T54" id="Seg_2843" s="T53">v</ta>
            <ta e="T55" id="Seg_2844" s="T54">ptcl</ta>
            <ta e="T56" id="Seg_2845" s="T55">v</ta>
            <ta e="T57" id="Seg_2846" s="T56">pers</ta>
            <ta e="T58" id="Seg_2847" s="T57">n</ta>
            <ta e="T59" id="Seg_2848" s="T58">v</ta>
            <ta e="T60" id="Seg_2849" s="T59">pers</ta>
            <ta e="T61" id="Seg_2850" s="T60">adv</ta>
            <ta e="T62" id="Seg_2851" s="T61">v</ta>
            <ta e="T63" id="Seg_2852" s="T62">aux</ta>
            <ta e="T64" id="Seg_2853" s="T63">v</ta>
            <ta e="T65" id="Seg_2854" s="T64">propr</ta>
            <ta e="T66" id="Seg_2855" s="T65">dempro</ta>
            <ta e="T67" id="Seg_2856" s="T66">cardnum</ta>
            <ta e="T68" id="Seg_2857" s="T67">v</ta>
            <ta e="T69" id="Seg_2858" s="T68">n</ta>
            <ta e="T70" id="Seg_2859" s="T69">cardnum</ta>
            <ta e="T71" id="Seg_2860" s="T70">cardnum</ta>
            <ta e="T72" id="Seg_2861" s="T71">pers</ta>
            <ta e="T73" id="Seg_2862" s="T72">adj</ta>
            <ta e="T74" id="Seg_2863" s="T73">n</ta>
            <ta e="T75" id="Seg_2864" s="T74">v</ta>
            <ta e="T76" id="Seg_2865" s="T75">aux</ta>
            <ta e="T77" id="Seg_2866" s="T76">n</ta>
            <ta e="T78" id="Seg_2867" s="T77">cardnum</ta>
            <ta e="T79" id="Seg_2868" s="T78">n</ta>
            <ta e="T80" id="Seg_2869" s="T79">n</ta>
            <ta e="T81" id="Seg_2870" s="T80">post</ta>
            <ta e="T82" id="Seg_2871" s="T81">propr</ta>
            <ta e="T83" id="Seg_2872" s="T82">n</ta>
            <ta e="T84" id="Seg_2873" s="T83">v</ta>
            <ta e="T85" id="Seg_2874" s="T84">v</ta>
            <ta e="T86" id="Seg_2875" s="T85">propr</ta>
            <ta e="T87" id="Seg_2876" s="T86">post</ta>
            <ta e="T88" id="Seg_2877" s="T87">v</ta>
            <ta e="T89" id="Seg_2878" s="T88">quant</ta>
            <ta e="T90" id="Seg_2879" s="T89">n</ta>
            <ta e="T91" id="Seg_2880" s="T90">n</ta>
            <ta e="T92" id="Seg_2881" s="T91">adv</ta>
            <ta e="T93" id="Seg_2882" s="T92">adj</ta>
            <ta e="T94" id="Seg_2883" s="T93">n</ta>
            <ta e="T95" id="Seg_2884" s="T94">n</ta>
            <ta e="T96" id="Seg_2885" s="T95">adj</ta>
            <ta e="T97" id="Seg_2886" s="T96">n</ta>
            <ta e="T98" id="Seg_2887" s="T97">n</ta>
            <ta e="T99" id="Seg_2888" s="T98">adj</ta>
            <ta e="T100" id="Seg_2889" s="T99">adj</ta>
            <ta e="T101" id="Seg_2890" s="T100">adv</ta>
            <ta e="T102" id="Seg_2891" s="T101">n</ta>
            <ta e="T103" id="Seg_2892" s="T102">adj</ta>
            <ta e="T104" id="Seg_2893" s="T103">n</ta>
            <ta e="T105" id="Seg_2894" s="T104">v</ta>
            <ta e="T106" id="Seg_2895" s="T105">propr</ta>
            <ta e="T107" id="Seg_2896" s="T106">pers</ta>
            <ta e="T108" id="Seg_2897" s="T107">v</ta>
            <ta e="T109" id="Seg_2898" s="T108">que</ta>
            <ta e="T110" id="Seg_2899" s="T109">pers</ta>
            <ta e="T111" id="Seg_2900" s="T110">v</ta>
            <ta e="T112" id="Seg_2901" s="T111">dempro</ta>
            <ta e="T113" id="Seg_2902" s="T112">v</ta>
            <ta e="T114" id="Seg_2903" s="T113">adv</ta>
            <ta e="T115" id="Seg_2904" s="T114">dempro</ta>
            <ta e="T116" id="Seg_2905" s="T115">v</ta>
            <ta e="T117" id="Seg_2906" s="T116">aux</ta>
            <ta e="T118" id="Seg_2907" s="T117">pers</ta>
            <ta e="T119" id="Seg_2908" s="T118">v</ta>
            <ta e="T120" id="Seg_2909" s="T119">propr</ta>
            <ta e="T121" id="Seg_2910" s="T120">n</ta>
            <ta e="T122" id="Seg_2911" s="T121">adv</ta>
            <ta e="T123" id="Seg_2912" s="T122">v</ta>
            <ta e="T124" id="Seg_2913" s="T123">v</ta>
            <ta e="T125" id="Seg_2914" s="T124">n</ta>
            <ta e="T126" id="Seg_2915" s="T125">dempro</ta>
            <ta e="T127" id="Seg_2916" s="T126">v</ta>
            <ta e="T128" id="Seg_2917" s="T127">v</ta>
            <ta e="T129" id="Seg_2918" s="T128">pers</ta>
            <ta e="T130" id="Seg_2919" s="T129">propr</ta>
            <ta e="T131" id="Seg_2920" s="T130">ptcl</ta>
            <ta e="T132" id="Seg_2921" s="T131">v</ta>
            <ta e="T133" id="Seg_2922" s="T132">pers</ta>
            <ta e="T134" id="Seg_2923" s="T133">n</ta>
            <ta e="T135" id="Seg_2924" s="T134">que</ta>
            <ta e="T136" id="Seg_2925" s="T135">pers</ta>
            <ta e="T137" id="Seg_2926" s="T136">v</ta>
            <ta e="T138" id="Seg_2927" s="T137">v</ta>
            <ta e="T139" id="Seg_2928" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_2929" s="T139">v</ta>
            <ta e="T141" id="Seg_2930" s="T140">n</ta>
            <ta e="T142" id="Seg_2931" s="T141">n</ta>
            <ta e="T143" id="Seg_2932" s="T142">n</ta>
            <ta e="T144" id="Seg_2933" s="T143">propr</ta>
            <ta e="T145" id="Seg_2934" s="T144">v</ta>
            <ta e="T146" id="Seg_2935" s="T145">propr</ta>
            <ta e="T147" id="Seg_2936" s="T146">post</ta>
            <ta e="T148" id="Seg_2937" s="T147">v</ta>
            <ta e="T149" id="Seg_2938" s="T148">n</ta>
            <ta e="T150" id="Seg_2939" s="T149">n</ta>
            <ta e="T151" id="Seg_2940" s="T150">que</ta>
            <ta e="T152" id="Seg_2941" s="T151">v</ta>
            <ta e="T153" id="Seg_2942" s="T152">dempro</ta>
            <ta e="T154" id="Seg_2943" s="T153">v</ta>
            <ta e="T155" id="Seg_2944" s="T154">propr</ta>
            <ta e="T156" id="Seg_2945" s="T155">propr</ta>
            <ta e="T157" id="Seg_2946" s="T156">n</ta>
            <ta e="T158" id="Seg_2947" s="T157">v</ta>
            <ta e="T159" id="Seg_2948" s="T158">pers</ta>
            <ta e="T160" id="Seg_2949" s="T159">dempro</ta>
            <ta e="T161" id="Seg_2950" s="T160">pers</ta>
            <ta e="T162" id="Seg_2951" s="T161">dempro</ta>
            <ta e="T163" id="Seg_2952" s="T162">v</ta>
            <ta e="T164" id="Seg_2953" s="T163">n</ta>
            <ta e="T165" id="Seg_2954" s="T164">n</ta>
            <ta e="T166" id="Seg_2955" s="T165">n</ta>
            <ta e="T167" id="Seg_2956" s="T166">v</ta>
            <ta e="T168" id="Seg_2957" s="T167">adv</ta>
            <ta e="T169" id="Seg_2958" s="T168">n</ta>
            <ta e="T170" id="Seg_2959" s="T169">adv</ta>
            <ta e="T171" id="Seg_2960" s="T170">v</ta>
            <ta e="T172" id="Seg_2961" s="T171">adv</ta>
            <ta e="T173" id="Seg_2962" s="T172">propr</ta>
            <ta e="T174" id="Seg_2963" s="T173">v</ta>
            <ta e="T175" id="Seg_2964" s="T174">pers</ta>
            <ta e="T176" id="Seg_2965" s="T175">pers</ta>
            <ta e="T177" id="Seg_2966" s="T176">adv</ta>
            <ta e="T178" id="Seg_2967" s="T177">v</ta>
            <ta e="T179" id="Seg_2968" s="T178">post</ta>
            <ta e="T180" id="Seg_2969" s="T179">v</ta>
            <ta e="T181" id="Seg_2970" s="T180">adj</ta>
            <ta e="T182" id="Seg_2971" s="T181">n</ta>
            <ta e="T183" id="Seg_2972" s="T182">adj</ta>
            <ta e="T184" id="Seg_2973" s="T183">pers</ta>
            <ta e="T185" id="Seg_2974" s="T184">v</ta>
            <ta e="T186" id="Seg_2975" s="T185">n</ta>
            <ta e="T187" id="Seg_2976" s="T186">adj</ta>
            <ta e="T188" id="Seg_2977" s="T187">pers</ta>
            <ta e="T189" id="Seg_2978" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_2979" s="T189">cop</ta>
            <ta e="T191" id="Seg_2980" s="T190">pers</ta>
            <ta e="T192" id="Seg_2981" s="T191">post</ta>
            <ta e="T193" id="Seg_2982" s="T192">v</ta>
            <ta e="T196" id="Seg_2983" s="T195">n</ta>
            <ta e="T197" id="Seg_2984" s="T196">n</ta>
            <ta e="T198" id="Seg_2985" s="T197">adv</ta>
            <ta e="T199" id="Seg_2986" s="T198">pers</ta>
            <ta e="T200" id="Seg_2987" s="T199">pers</ta>
            <ta e="T201" id="Seg_2988" s="T200">v</ta>
            <ta e="T202" id="Seg_2989" s="T201">adv</ta>
            <ta e="T203" id="Seg_2990" s="T202">v</ta>
            <ta e="T204" id="Seg_2991" s="T203">pers</ta>
            <ta e="T205" id="Seg_2992" s="T204">n</ta>
            <ta e="T206" id="Seg_2993" s="T205">adj</ta>
            <ta e="T207" id="Seg_2994" s="T206">n</ta>
            <ta e="T208" id="Seg_2995" s="T207">adj</ta>
            <ta e="T209" id="Seg_2996" s="T208">n</ta>
            <ta e="T210" id="Seg_2997" s="T209">v</ta>
            <ta e="T211" id="Seg_2998" s="T210">n</ta>
            <ta e="T212" id="Seg_2999" s="T211">v</ta>
            <ta e="T213" id="Seg_3000" s="T212">n</ta>
            <ta e="T214" id="Seg_3001" s="T213">n</ta>
            <ta e="T215" id="Seg_3002" s="T214">n</ta>
            <ta e="T216" id="Seg_3003" s="T215">v</ta>
            <ta e="T217" id="Seg_3004" s="T216">aux</ta>
            <ta e="T218" id="Seg_3005" s="T217">propr</ta>
            <ta e="T219" id="Seg_3006" s="T218">propr</ta>
            <ta e="T220" id="Seg_3007" s="T219">pers</ta>
            <ta e="T221" id="Seg_3008" s="T220">v</ta>
            <ta e="T222" id="Seg_3009" s="T221">adj</ta>
            <ta e="T223" id="Seg_3010" s="T222">n</ta>
            <ta e="T224" id="Seg_3011" s="T223">n</ta>
            <ta e="T225" id="Seg_3012" s="T224">v</ta>
            <ta e="T226" id="Seg_3013" s="T225">n</ta>
            <ta e="T227" id="Seg_3014" s="T226">ptcl</ta>
            <ta e="T228" id="Seg_3015" s="T227">pers</ta>
            <ta e="T229" id="Seg_3016" s="T228">v</ta>
            <ta e="T230" id="Seg_3017" s="T229">aux</ta>
            <ta e="T231" id="Seg_3018" s="T230">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_3019" s="T0">np.h:B</ta>
            <ta e="T2" id="Seg_3020" s="T1">0.1.h:A</ta>
            <ta e="T3" id="Seg_3021" s="T2">np:Th</ta>
            <ta e="T4" id="Seg_3022" s="T3">np:So</ta>
            <ta e="T7" id="Seg_3023" s="T6">np.h:Poss</ta>
            <ta e="T10" id="Seg_3024" s="T9">np.h:P</ta>
            <ta e="T12" id="Seg_3025" s="T10">0.3.h:A</ta>
            <ta e="T15" id="Seg_3026" s="T14">np.h:A</ta>
            <ta e="T19" id="Seg_3027" s="T18">0.3.h:Poss np:G</ta>
            <ta e="T23" id="Seg_3028" s="T22">pro.h:St</ta>
            <ta e="T24" id="Seg_3029" s="T23">0.3.h:E</ta>
            <ta e="T28" id="Seg_3030" s="T27">n:Time</ta>
            <ta e="T29" id="Seg_3031" s="T28">pro.h:A</ta>
            <ta e="T31" id="Seg_3032" s="T30">pro.h:R</ta>
            <ta e="T32" id="Seg_3033" s="T31">pro.h:Th</ta>
            <ta e="T34" id="Seg_3034" s="T32">pp:Time</ta>
            <ta e="T35" id="Seg_3035" s="T34">adv:Time</ta>
            <ta e="T44" id="Seg_3036" s="T43">np:Th</ta>
            <ta e="T53" id="Seg_3037" s="T52">0.2.h:A</ta>
            <ta e="T54" id="Seg_3038" s="T53">0.1.h:A</ta>
            <ta e="T56" id="Seg_3039" s="T55">0.3.h:A</ta>
            <ta e="T57" id="Seg_3040" s="T56">pro.h:P</ta>
            <ta e="T59" id="Seg_3041" s="T58">0.3.h:A</ta>
            <ta e="T60" id="Seg_3042" s="T59">pro.h:A</ta>
            <ta e="T65" id="Seg_3043" s="T64">np.h:A</ta>
            <ta e="T66" id="Seg_3044" s="T65">pro.h:Th</ta>
            <ta e="T71" id="Seg_3045" s="T70">np:So</ta>
            <ta e="T72" id="Seg_3046" s="T71">pro.h:A</ta>
            <ta e="T74" id="Seg_3047" s="T73">np.h:R</ta>
            <ta e="T77" id="Seg_3048" s="T76">0.3.h:Poss np:Th</ta>
            <ta e="T82" id="Seg_3049" s="T81">np.h:P</ta>
            <ta e="T84" id="Seg_3050" s="T83">0.1.h:A</ta>
            <ta e="T87" id="Seg_3051" s="T85">pp:Com</ta>
            <ta e="T91" id="Seg_3052" s="T90">np.h:A</ta>
            <ta e="T95" id="Seg_3053" s="T94">np:So</ta>
            <ta e="T97" id="Seg_3054" s="T96">np:So</ta>
            <ta e="T98" id="Seg_3055" s="T97">0.3.h:Poss np:L</ta>
            <ta e="T99" id="Seg_3056" s="T98">0.3.h:Poss np:Th</ta>
            <ta e="T100" id="Seg_3057" s="T99">0.3.h:Poss np:Th</ta>
            <ta e="T103" id="Seg_3058" s="T102">0.3.h:Poss np:Th</ta>
            <ta e="T106" id="Seg_3059" s="T105">np.h:A</ta>
            <ta e="T107" id="Seg_3060" s="T106">pro.h:R</ta>
            <ta e="T109" id="Seg_3061" s="T108">pro.h:P</ta>
            <ta e="T110" id="Seg_3062" s="T109">pro.h:A</ta>
            <ta e="T112" id="Seg_3063" s="T111">pro.h:Th</ta>
            <ta e="T114" id="Seg_3064" s="T113">adv:Time</ta>
            <ta e="T115" id="Seg_3065" s="T114">pro.h:P</ta>
            <ta e="T117" id="Seg_3066" s="T115">0.2.h:A</ta>
            <ta e="T118" id="Seg_3067" s="T117">pro.h:A</ta>
            <ta e="T121" id="Seg_3068" s="T120">np:G</ta>
            <ta e="T124" id="Seg_3069" s="T123">0.2.h:E</ta>
            <ta e="T128" id="Seg_3070" s="T127">0.3.h:A </ta>
            <ta e="T129" id="Seg_3071" s="T128">pro.h:P</ta>
            <ta e="T130" id="Seg_3072" s="T129">pro.h:A</ta>
            <ta e="T133" id="Seg_3073" s="T132">pro.h:R</ta>
            <ta e="T136" id="Seg_3074" s="T135">pro.h:A</ta>
            <ta e="T138" id="Seg_3075" s="T137">0.2.h:A</ta>
            <ta e="T140" id="Seg_3076" s="T139">0.2.h:A</ta>
            <ta e="T143" id="Seg_3077" s="T142">np.h:P</ta>
            <ta e="T144" id="Seg_3078" s="T143">np.h:A</ta>
            <ta e="T147" id="Seg_3079" s="T145">pp:Com</ta>
            <ta e="T150" id="Seg_3080" s="T149">np.h:R</ta>
            <ta e="T151" id="Seg_3081" s="T150">pro.h:Th</ta>
            <ta e="T152" id="Seg_3082" s="T151">0.2.h:A</ta>
            <ta e="T153" id="Seg_3083" s="T152">pro.h:A</ta>
            <ta e="T157" id="Seg_3084" s="T156">np.h:A</ta>
            <ta e="T159" id="Seg_3085" s="T158">pro.h:R</ta>
            <ta e="T160" id="Seg_3086" s="T159">pro.h:Th</ta>
            <ta e="T165" id="Seg_3087" s="T164">np.h:A</ta>
            <ta e="T169" id="Seg_3088" s="T168">np:G</ta>
            <ta e="T171" id="Seg_3089" s="T170">0.3.h:P</ta>
            <ta e="T172" id="Seg_3090" s="T171">adv:Time</ta>
            <ta e="T173" id="Seg_3091" s="T172">np.h:A</ta>
            <ta e="T175" id="Seg_3092" s="T174">pro.h:R</ta>
            <ta e="T176" id="Seg_3093" s="T175">pro.h:A</ta>
            <ta e="T184" id="Seg_3094" s="T183">pro.h:Th</ta>
            <ta e="T186" id="Seg_3095" s="T185">n:Time</ta>
            <ta e="T188" id="Seg_3096" s="T187">pro.h:Th</ta>
            <ta e="T192" id="Seg_3097" s="T190">pp:Com</ta>
            <ta e="T197" id="Seg_3098" s="T196">np:L</ta>
            <ta e="T198" id="Seg_3099" s="T197">adv:L</ta>
            <ta e="T199" id="Seg_3100" s="T198">pro.h:A</ta>
            <ta e="T200" id="Seg_3101" s="T199">pro.h:Th</ta>
            <ta e="T202" id="Seg_3102" s="T201">adv:Time</ta>
            <ta e="T204" id="Seg_3103" s="T203">pro.h:Poss</ta>
            <ta e="T205" id="Seg_3104" s="T204">np:Th</ta>
            <ta e="T211" id="Seg_3105" s="T210">np.h:A</ta>
            <ta e="T213" id="Seg_3106" s="T212">np:Th</ta>
            <ta e="T215" id="Seg_3107" s="T214">np.h:A</ta>
            <ta e="T219" id="Seg_3108" s="T218">np.h:Th</ta>
            <ta e="T220" id="Seg_3109" s="T219">pro.h:Th</ta>
            <ta e="T221" id="Seg_3110" s="T220">0.3.h:A</ta>
            <ta e="T223" id="Seg_3111" s="T222">np.h:Poss</ta>
            <ta e="T224" id="Seg_3112" s="T223">np:G</ta>
            <ta e="T226" id="Seg_3113" s="T225">np.h:A</ta>
            <ta e="T228" id="Seg_3114" s="T227">pro.h:Th</ta>
            <ta e="T231" id="Seg_3115" s="T230">0.3.h:A</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T2" id="Seg_3116" s="T1">0.1.h:S v:pred</ta>
            <ta e="T3" id="Seg_3117" s="T2">np:O</ta>
            <ta e="T9" id="Seg_3118" s="T4">s:rel</ta>
            <ta e="T10" id="Seg_3119" s="T9">np.h:O</ta>
            <ta e="T12" id="Seg_3120" s="T10">0.3.h:S v:pred</ta>
            <ta e="T15" id="Seg_3121" s="T14">np.h:S</ta>
            <ta e="T17" id="Seg_3122" s="T15">v:pred</ta>
            <ta e="T21" id="Seg_3123" s="T19">s:temp</ta>
            <ta e="T23" id="Seg_3124" s="T22">pro.h:O</ta>
            <ta e="T24" id="Seg_3125" s="T23">0.3.h:S v:pred</ta>
            <ta e="T26" id="Seg_3126" s="T24">s:comp</ta>
            <ta e="T29" id="Seg_3127" s="T28">pro.h:S</ta>
            <ta e="T30" id="Seg_3128" s="T29">v:pred</ta>
            <ta e="T32" id="Seg_3129" s="T31">pro.h:S</ta>
            <ta e="T38" id="Seg_3130" s="T36">v:pred</ta>
            <ta e="T41" id="Seg_3131" s="T39">s:adv</ta>
            <ta e="T43" id="Seg_3132" s="T42">v:pred</ta>
            <ta e="T44" id="Seg_3133" s="T43">np:S</ta>
            <ta e="T53" id="Seg_3134" s="T52">0.2.h:S v:pred</ta>
            <ta e="T54" id="Seg_3135" s="T53">0.1.h:S v:pred</ta>
            <ta e="T56" id="Seg_3136" s="T55">0.3.h:S v:pred</ta>
            <ta e="T57" id="Seg_3137" s="T56">pro.h:O</ta>
            <ta e="T59" id="Seg_3138" s="T58">0.3.h:S v:pred</ta>
            <ta e="T63" id="Seg_3139" s="T59">s:temp</ta>
            <ta e="T64" id="Seg_3140" s="T63">v:pred</ta>
            <ta e="T65" id="Seg_3141" s="T64">np.h:S</ta>
            <ta e="T66" id="Seg_3142" s="T65">pro.h:S</ta>
            <ta e="T69" id="Seg_3143" s="T68">n:pred</ta>
            <ta e="T72" id="Seg_3144" s="T71">pro.h:S</ta>
            <ta e="T76" id="Seg_3145" s="T74">v:pred</ta>
            <ta e="T77" id="Seg_3146" s="T76">np:O</ta>
            <ta e="T85" id="Seg_3147" s="T81">s:adv</ta>
            <ta e="T88" id="Seg_3148" s="T87">v:pred</ta>
            <ta e="T91" id="Seg_3149" s="T90">np.h:S</ta>
            <ta e="T99" id="Seg_3150" s="T98">0.3.h:S adj:pred</ta>
            <ta e="T100" id="Seg_3151" s="T99">0.3.h:S adj:pred</ta>
            <ta e="T103" id="Seg_3152" s="T102">0.3.h:S adj:pred</ta>
            <ta e="T105" id="Seg_3153" s="T103">s:rel</ta>
            <ta e="T106" id="Seg_3154" s="T105">np.h:S</ta>
            <ta e="T108" id="Seg_3155" s="T107">v:pred</ta>
            <ta e="T111" id="Seg_3156" s="T108">s:comp</ta>
            <ta e="T112" id="Seg_3157" s="T111">pro.h:S</ta>
            <ta e="T113" id="Seg_3158" s="T112">v:pred</ta>
            <ta e="T115" id="Seg_3159" s="T114">pro.h:O</ta>
            <ta e="T117" id="Seg_3160" s="T115">0.2.h:S v:pred</ta>
            <ta e="T118" id="Seg_3161" s="T117">pro.h:S</ta>
            <ta e="T119" id="Seg_3162" s="T118">v:pred</ta>
            <ta e="T123" id="Seg_3163" s="T122">v:pred</ta>
            <ta e="T124" id="Seg_3164" s="T123">0.2.h:S v:pred</ta>
            <ta e="T127" id="Seg_3165" s="T125">s:temp</ta>
            <ta e="T128" id="Seg_3166" s="T127">0.3.h:S v:pred</ta>
            <ta e="T129" id="Seg_3167" s="T128">pro.h:O</ta>
            <ta e="T130" id="Seg_3168" s="T129">np.h:S</ta>
            <ta e="T132" id="Seg_3169" s="T131">v:pred</ta>
            <ta e="T136" id="Seg_3170" s="T135">pro.h:S</ta>
            <ta e="T137" id="Seg_3171" s="T136">v:pred</ta>
            <ta e="T138" id="Seg_3172" s="T137">s:adv</ta>
            <ta e="T140" id="Seg_3173" s="T139">0.2.h:S v:pred</ta>
            <ta e="T143" id="Seg_3174" s="T142">np.h:O</ta>
            <ta e="T144" id="Seg_3175" s="T143">np.h:S</ta>
            <ta e="T145" id="Seg_3176" s="T144">v:pred</ta>
            <ta e="T148" id="Seg_3177" s="T145">s:rel</ta>
            <ta e="T151" id="Seg_3178" s="T150">pro.h:O</ta>
            <ta e="T152" id="Seg_3179" s="T151">0.2.h:S v:pred</ta>
            <ta e="T153" id="Seg_3180" s="T152">pro.h:S</ta>
            <ta e="T154" id="Seg_3181" s="T153">v:pred</ta>
            <ta e="T157" id="Seg_3182" s="T156">np.h:S</ta>
            <ta e="T158" id="Seg_3183" s="T157">v:pred</ta>
            <ta e="T160" id="Seg_3184" s="T159">pro.h:S</ta>
            <ta e="T161" id="Seg_3185" s="T160">pro:pred</ta>
            <ta e="T163" id="Seg_3186" s="T161">s:temp</ta>
            <ta e="T165" id="Seg_3187" s="T164">np.h:S</ta>
            <ta e="T167" id="Seg_3188" s="T166">v:pred</ta>
            <ta e="T171" id="Seg_3189" s="T170">0.3.h:S v:pred</ta>
            <ta e="T173" id="Seg_3190" s="T172">np.h:S</ta>
            <ta e="T174" id="Seg_3191" s="T173">v:pred</ta>
            <ta e="T176" id="Seg_3192" s="T175">pro.h:S</ta>
            <ta e="T180" id="Seg_3193" s="T179">v:pred</ta>
            <ta e="T185" id="Seg_3194" s="T183">s:purp</ta>
            <ta e="T188" id="Seg_3195" s="T187">pro.h:S</ta>
            <ta e="T189" id="Seg_3196" s="T188">ptcl:pred</ta>
            <ta e="T190" id="Seg_3197" s="T189">cop</ta>
            <ta e="T193" id="Seg_3198" s="T192">s:purp</ta>
            <ta e="T199" id="Seg_3199" s="T198">pro.h:S</ta>
            <ta e="T200" id="Seg_3200" s="T199">pro.h:O</ta>
            <ta e="T201" id="Seg_3201" s="T200">v:pred</ta>
            <ta e="T203" id="Seg_3202" s="T202">v:pred</ta>
            <ta e="T205" id="Seg_3203" s="T204">np:S</ta>
            <ta e="T210" id="Seg_3204" s="T209">v:pred</ta>
            <ta e="T212" id="Seg_3205" s="T210">s:rel</ta>
            <ta e="T213" id="Seg_3206" s="T212">np:S</ta>
            <ta e="T219" id="Seg_3207" s="T213">s:temp</ta>
            <ta e="T220" id="Seg_3208" s="T219">pro.h:O</ta>
            <ta e="T221" id="Seg_3209" s="T220">0.3.h:S v:pred</ta>
            <ta e="T226" id="Seg_3210" s="T225">np.h:S</ta>
            <ta e="T228" id="Seg_3211" s="T227">pro.h:O</ta>
            <ta e="T230" id="Seg_3212" s="T228">v:pred</ta>
            <ta e="T231" id="Seg_3213" s="T230">0.3.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T10" id="Seg_3214" s="T9">accs-gen</ta>
            <ta e="T15" id="Seg_3215" s="T14">giv-active</ta>
            <ta e="T19" id="Seg_3216" s="T18">accs-gen</ta>
            <ta e="T23" id="Seg_3217" s="T22">giv-active</ta>
            <ta e="T24" id="Seg_3218" s="T23">0.giv-active</ta>
            <ta e="T29" id="Seg_3219" s="T28">giv-active</ta>
            <ta e="T30" id="Seg_3220" s="T29">quot-sp</ta>
            <ta e="T31" id="Seg_3221" s="T30">giv-active</ta>
            <ta e="T32" id="Seg_3222" s="T31">giv-active-Q</ta>
            <ta e="T46" id="Seg_3223" s="T45">giv-inactive-Q</ta>
            <ta e="T53" id="Seg_3224" s="T52">0.giv-inactive-Q</ta>
            <ta e="T54" id="Seg_3225" s="T53">0.accs-aggr-Q</ta>
            <ta e="T56" id="Seg_3226" s="T55">0.accs-inf-Q</ta>
            <ta e="T57" id="Seg_3227" s="T56">giv-active-Q</ta>
            <ta e="T59" id="Seg_3228" s="T58">0.giv-active-Q</ta>
            <ta e="T60" id="Seg_3229" s="T59">giv-active</ta>
            <ta e="T65" id="Seg_3230" s="T64">giv-active</ta>
            <ta e="T66" id="Seg_3231" s="T65">giv-active</ta>
            <ta e="T71" id="Seg_3232" s="T70">giv-active</ta>
            <ta e="T72" id="Seg_3233" s="T71">giv-active</ta>
            <ta e="T74" id="Seg_3234" s="T73">accs-gen</ta>
            <ta e="T77" id="Seg_3235" s="T76">new</ta>
            <ta e="T80" id="Seg_3236" s="T79">new</ta>
            <ta e="T82" id="Seg_3237" s="T81">giv-active-Q</ta>
            <ta e="T84" id="Seg_3238" s="T83">0.giv-active-Q</ta>
            <ta e="T86" id="Seg_3239" s="T85">giv-active</ta>
            <ta e="T91" id="Seg_3240" s="T90">new</ta>
            <ta e="T94" id="Seg_3241" s="T93">accs-inf</ta>
            <ta e="T95" id="Seg_3242" s="T94">accs-gen</ta>
            <ta e="T97" id="Seg_3243" s="T96">giv-inactive</ta>
            <ta e="T98" id="Seg_3244" s="T97">accs-inf</ta>
            <ta e="T99" id="Seg_3245" s="T98">0.giv-active new</ta>
            <ta e="T100" id="Seg_3246" s="T99">0.giv-active new</ta>
            <ta e="T103" id="Seg_3247" s="T102">0.giv-active new</ta>
            <ta e="T106" id="Seg_3248" s="T105">giv-inactive</ta>
            <ta e="T107" id="Seg_3249" s="T106">giv-active</ta>
            <ta e="T108" id="Seg_3250" s="T107">quot-sp</ta>
            <ta e="T109" id="Seg_3251" s="T108">accs-sit-Q</ta>
            <ta e="T110" id="Seg_3252" s="T109">giv-active-Q</ta>
            <ta e="T112" id="Seg_3253" s="T111">giv-active-Q</ta>
            <ta e="T115" id="Seg_3254" s="T114">giv-active-Q</ta>
            <ta e="T117" id="Seg_3255" s="T115">0.giv-active-Q</ta>
            <ta e="T118" id="Seg_3256" s="T117">giv-inactive</ta>
            <ta e="T120" id="Seg_3257" s="T119">giv-active</ta>
            <ta e="T123" id="Seg_3258" s="T122">quot-sp</ta>
            <ta e="T124" id="Seg_3259" s="T123">0.giv-active-Q</ta>
            <ta e="T125" id="Seg_3260" s="T124">giv-active-Q</ta>
            <ta e="T128" id="Seg_3261" s="T127">0.giv-active</ta>
            <ta e="T129" id="Seg_3262" s="T128">giv-active</ta>
            <ta e="T130" id="Seg_3263" s="T129">giv-active</ta>
            <ta e="T132" id="Seg_3264" s="T131">quot-sp</ta>
            <ta e="T133" id="Seg_3265" s="T132">giv-active</ta>
            <ta e="T134" id="Seg_3266" s="T133">giv-active-Q</ta>
            <ta e="T136" id="Seg_3267" s="T135">giv-active-Q</ta>
            <ta e="T138" id="Seg_3268" s="T137">0.giv-active-Q</ta>
            <ta e="T140" id="Seg_3269" s="T139">0.giv-active-Q</ta>
            <ta e="T143" id="Seg_3270" s="T142">giv-inactive-Q</ta>
            <ta e="T144" id="Seg_3271" s="T143">giv-active</ta>
            <ta e="T145" id="Seg_3272" s="T144">quot-sp</ta>
            <ta e="T146" id="Seg_3273" s="T145">giv-active</ta>
            <ta e="T150" id="Seg_3274" s="T149">giv-inactive</ta>
            <ta e="T152" id="Seg_3275" s="T151">0.giv-active-Q</ta>
            <ta e="T153" id="Seg_3276" s="T152">giv-active</ta>
            <ta e="T154" id="Seg_3277" s="T153">quot-sp</ta>
            <ta e="T155" id="Seg_3278" s="T154">accs-gen-Q</ta>
            <ta e="T156" id="Seg_3279" s="T155">giv-inactive-Q</ta>
            <ta e="T157" id="Seg_3280" s="T156">giv-active</ta>
            <ta e="T158" id="Seg_3281" s="T157">quot-sp</ta>
            <ta e="T159" id="Seg_3282" s="T158">giv-inactive</ta>
            <ta e="T161" id="Seg_3283" s="T160">giv-active-Q</ta>
            <ta e="T165" id="Seg_3284" s="T164">giv-inactive</ta>
            <ta e="T166" id="Seg_3285" s="T165">accs-inf</ta>
            <ta e="T169" id="Seg_3286" s="T168">accs-sit</ta>
            <ta e="T171" id="Seg_3287" s="T170">0.giv-active</ta>
            <ta e="T173" id="Seg_3288" s="T172">giv-inactive</ta>
            <ta e="T174" id="Seg_3289" s="T173">quot-sp</ta>
            <ta e="T175" id="Seg_3290" s="T174">giv-active</ta>
            <ta e="T176" id="Seg_3291" s="T175">giv-active-Q</ta>
            <ta e="T181" id="Seg_3292" s="T180">giv-inactive-Q</ta>
            <ta e="T183" id="Seg_3293" s="T182">giv-inactive-Q</ta>
            <ta e="T184" id="Seg_3294" s="T183">giv-active-Q</ta>
            <ta e="T188" id="Seg_3295" s="T187">giv-active-Q</ta>
            <ta e="T191" id="Seg_3296" s="T190">giv-active-Q</ta>
            <ta e="T197" id="Seg_3297" s="T196">accs-gen-Q</ta>
            <ta e="T199" id="Seg_3298" s="T198">giv-active-Q</ta>
            <ta e="T200" id="Seg_3299" s="T199">giv-active-Q</ta>
            <ta e="T204" id="Seg_3300" s="T203">giv-active-Q</ta>
            <ta e="T205" id="Seg_3301" s="T204">accs-inf</ta>
            <ta e="T211" id="Seg_3302" s="T210">accs-gen-Q</ta>
            <ta e="T213" id="Seg_3303" s="T212">accs-inf-Q</ta>
            <ta e="T215" id="Seg_3304" s="T214">giv-inactive</ta>
            <ta e="T219" id="Seg_3305" s="T218">giv-inactive</ta>
            <ta e="T220" id="Seg_3306" s="T219">giv-active</ta>
            <ta e="T221" id="Seg_3307" s="T220">0.giv-active</ta>
            <ta e="T223" id="Seg_3308" s="T222">giv-inactive</ta>
            <ta e="T224" id="Seg_3309" s="T223">accs-inf</ta>
            <ta e="T226" id="Seg_3310" s="T225">giv-inactive</ta>
            <ta e="T228" id="Seg_3311" s="T227">giv-active</ta>
            <ta e="T231" id="Seg_3312" s="T230">0.giv-active</ta>
         </annotation>
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T4" id="Seg_3313" s="T3">RUS:cult</ta>
            <ta e="T5" id="Seg_3314" s="T4">RUS:cult</ta>
            <ta e="T6" id="Seg_3315" s="T5">RUS:cult</ta>
            <ta e="T8" id="Seg_3316" s="T7">EV:core</ta>
            <ta e="T10" id="Seg_3317" s="T9">RUS:cult</ta>
            <ta e="T15" id="Seg_3318" s="T14">RUS:cult</ta>
            <ta e="T36" id="Seg_3319" s="T35">RUS:mod</ta>
            <ta e="T44" id="Seg_3320" s="T43">RUS:cult</ta>
            <ta e="T65" id="Seg_3321" s="T64">RUS:cult</ta>
            <ta e="T82" id="Seg_3322" s="T81">RUS:cult</ta>
            <ta e="T86" id="Seg_3323" s="T85">RUS:cult</ta>
            <ta e="T95" id="Seg_3324" s="T94">RUS:cult</ta>
            <ta e="T106" id="Seg_3325" s="T105">RUS:cult</ta>
            <ta e="T120" id="Seg_3326" s="T119">RUS:cult</ta>
            <ta e="T125" id="Seg_3327" s="T124">RUS:cult</ta>
            <ta e="T130" id="Seg_3328" s="T129">RUS:cult</ta>
            <ta e="T134" id="Seg_3329" s="T133">EV:core</ta>
            <ta e="T144" id="Seg_3330" s="T143">RUS:cult</ta>
            <ta e="T146" id="Seg_3331" s="T145">RUS:cult</ta>
            <ta e="T155" id="Seg_3332" s="T154">RUS:cult</ta>
            <ta e="T156" id="Seg_3333" s="T155">RUS:cult</ta>
            <ta e="T157" id="Seg_3334" s="T156">RUS:cult</ta>
            <ta e="T173" id="Seg_3335" s="T172">RUS:cult</ta>
            <ta e="T218" id="Seg_3336" s="T217">RUS:cult</ta>
            <ta e="T219" id="Seg_3337" s="T218">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T4" id="Seg_3338" s="T3">medVins Vsub</ta>
            <ta e="T36" id="Seg_3339" s="T35">Vsub Csub Vsub</ta>
            <ta e="T125" id="Seg_3340" s="T124">Vsub</ta>
            <ta e="T155" id="Seg_3341" s="T154">Csub</ta>
            <ta e="T157" id="Seg_3342" s="T156">fortition Vsub medVins</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T4" id="Seg_3343" s="T3">dir:infl</ta>
            <ta e="T5" id="Seg_3344" s="T4">dir:bare</ta>
            <ta e="T6" id="Seg_3345" s="T5">dir:bare</ta>
            <ta e="T8" id="Seg_3346" s="T7">dir:infl</ta>
            <ta e="T10" id="Seg_3347" s="T9">dir:infl</ta>
            <ta e="T15" id="Seg_3348" s="T14">dir:bare</ta>
            <ta e="T36" id="Seg_3349" s="T35">dir:bare</ta>
            <ta e="T44" id="Seg_3350" s="T43">dir:bare</ta>
            <ta e="T65" id="Seg_3351" s="T64">dir:bare</ta>
            <ta e="T82" id="Seg_3352" s="T81">dir:infl</ta>
            <ta e="T86" id="Seg_3353" s="T85">dir:infl</ta>
            <ta e="T95" id="Seg_3354" s="T94">dir:infl</ta>
            <ta e="T106" id="Seg_3355" s="T105">dir:bare</ta>
            <ta e="T120" id="Seg_3356" s="T119">dir:bare</ta>
            <ta e="T125" id="Seg_3357" s="T124">dir:bare</ta>
            <ta e="T130" id="Seg_3358" s="T129">dir:bare</ta>
            <ta e="T134" id="Seg_3359" s="T133">dir:bare</ta>
            <ta e="T144" id="Seg_3360" s="T143">dir:bare</ta>
            <ta e="T146" id="Seg_3361" s="T145">dir:infl</ta>
            <ta e="T155" id="Seg_3362" s="T154">dir:infl</ta>
            <ta e="T156" id="Seg_3363" s="T155">dir:infl</ta>
            <ta e="T157" id="Seg_3364" s="T156">dir:bare</ta>
            <ta e="T173" id="Seg_3365" s="T172">dir:bare</ta>
            <ta e="T218" id="Seg_3366" s="T217">dir:bare</ta>
            <ta e="T219" id="Seg_3367" s="T218">dir:infl</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T9" id="Seg_3368" s="T0">We read for the children stories from a book which is called "Jesus Christ - the children's friend".</ta>
            <ta e="T12" id="Seg_3369" s="T9">Jesus is arrested.</ta>
            <ta e="T19" id="Seg_3370" s="T12">Jesus came thrice to his disciples.</ta>
            <ta e="T26" id="Seg_3371" s="T19">Having found them there, he saw them sleeping.</ta>
            <ta e="T31" id="Seg_3372" s="T26">When he came the third time, he said to them:</ta>
            <ta e="T41" id="Seg_3373" s="T31">"Do you still sleep here until now, relaxing?</ta>
            <ta e="T52" id="Seg_3374" s="T41">The time has come, when the humans' son is given to the people who have committed sins (?).</ta>
            <ta e="T54" id="Seg_3375" s="T52">Stand up, let us go.</ta>
            <ta e="T59" id="Seg_3376" s="T54">The one, [who] will sentence me to death, has come." </ta>
            <ta e="T71" id="Seg_3377" s="T59">When he was speaking so, Judas came, that is one disciple out of twelve.</ta>
            <ta e="T85" id="Seg_3378" s="T71">He apparently had given his word for thirty gold coins to the pontiffs, saying "I will give Jesus to the death".</ta>
            <ta e="T91" id="Seg_3379" s="T85">Together with Judas many soldiers came.</ta>
            <ta e="T97" id="Seg_3380" s="T91">Then various people of the Pharisees, of the pontiffs.</ta>
            <ta e="T103" id="Seg_3381" s="T97">In their hands they have kettles, swords and wooden posts.</ta>
            <ta e="T108" id="Seg_3382" s="T103">Judas, sentencing to death, said to them: </ta>
            <ta e="T113" id="Seg_3383" s="T108">"It will be that one, whom I will kiss.</ta>
            <ta e="T117" id="Seg_3384" s="T113">Then arrest that one."</ta>
            <ta e="T123" id="Seg_3385" s="T117">He came to Jesus and said:</ta>
            <ta e="T129" id="Seg_3386" s="T123">"Be happy, teacher", he said and kissed him.</ta>
            <ta e="T133" id="Seg_3387" s="T129">But Jesus said to him:</ta>
            <ta e="T137" id="Seg_3388" s="T133">"Friend, why did you come?</ta>
            <ta e="T143" id="Seg_3389" s="T137">Kissing him, you are sentencing the humans' son to death."</ta>
            <ta e="T150" id="Seg_3390" s="T143">Jesus asked the soldiers, who had come together with Judas:</ta>
            <ta e="T152" id="Seg_3391" s="T150">"Whom do you search?"</ta>
            <ta e="T154" id="Seg_3392" s="T152">They said:</ta>
            <ta e="T156" id="Seg_3393" s="T154">"Jesus from Nazareth."</ta>
            <ta e="T159" id="Seg_3394" s="T156">The man of the house told them:</ta>
            <ta e="T161" id="Seg_3395" s="T159">"That's me."</ta>
            <ta e="T171" id="Seg_3396" s="T161">Having heard this, the soldiers stepped back and fell down on their back. </ta>
            <ta e="T175" id="Seg_3397" s="T171">Then Jesus said to them:</ta>
            <ta e="T185" id="Seg_3398" s="T175">"You have gone out, searching like killers, with swords and wooden posts, in order to catch me.</ta>
            <ta e="T197" id="Seg_3399" s="T185">Every day I was in the church together with you in order to teach [you].</ta>
            <ta e="T201" id="Seg_3400" s="T197">There you didn't take me.</ta>
            <ta e="T209" id="Seg_3401" s="T201">Now, your time has come: a dark and long time.</ta>
            <ta e="T213" id="Seg_3402" s="T209">The prophets' writings come true."</ta>
            <ta e="T224" id="Seg_3403" s="T213">After the soldiers had arrested Jesus Christ, they brought him to the pontiff's house.</ta>
            <ta e="T231" id="Seg_3404" s="T224">But his disciples left him back and escaped.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T9" id="Seg_3405" s="T0">Wir lesen für die Kinder Geschichten aus einem Buch, das "Jesus Christus - der Freund der Kinder" heißt.</ta>
            <ta e="T12" id="Seg_3406" s="T9">Jesus wird gefangen genommen.</ta>
            <ta e="T19" id="Seg_3407" s="T12">Drei mal kam Jesus zu seinen Jüngern.</ta>
            <ta e="T26" id="Seg_3408" s="T19">Nachdem er sie dort gefunden hatte, sah er, wie sie schliefen.</ta>
            <ta e="T31" id="Seg_3409" s="T26">Als er das dritte Mal kam, sagte er zu ihnen:</ta>
            <ta e="T41" id="Seg_3410" s="T31">"Schlaft ihr hier jetzt immer noch und entspannt euch?</ta>
            <ta e="T52" id="Seg_3411" s="T41">Es ist die Stunde gekommen, dass der Sohn der Menschen den Menschen übergeben wird, die Sünden begangen haben (?).</ta>
            <ta e="T54" id="Seg_3412" s="T52">Steht auf, lasst uns gehen.</ta>
            <ta e="T59" id="Seg_3413" s="T54">Es ist der gekommen, [der] mich dem Tode geben wird."</ta>
            <ta e="T71" id="Seg_3414" s="T59">Als er so sprach, kam Judas, das ist ein Jünger von zwölfen.</ta>
            <ta e="T85" id="Seg_3415" s="T71">Er hatte offenbar für dreißig Goldmünzen den Hohepriestern sein Wort gegeben, "ich werde Jesus dem Tod geben" sagend.</ta>
            <ta e="T91" id="Seg_3416" s="T85">Mit Judas kamen viele Soldaten.</ta>
            <ta e="T97" id="Seg_3417" s="T91">Dann unterschiedliche Menschen von den Pharisäern, von den Hohepriestern.</ta>
            <ta e="T103" id="Seg_3418" s="T97">In ihren Händen haben sie Kessel, Schwerter und Holzpflöcke.</ta>
            <ta e="T108" id="Seg_3419" s="T103">Der todbringende Judas sagte ihnen: </ta>
            <ta e="T113" id="Seg_3420" s="T108">"Wen ich küsse, der wird es sein.</ta>
            <ta e="T117" id="Seg_3421" s="T113">Dann nehmt jenen gefangen."</ta>
            <ta e="T123" id="Seg_3422" s="T117">Er kam zu Jesus und sagte:</ta>
            <ta e="T129" id="Seg_3423" s="T123">"Freue dich, Lehrer", sprach er und küsste ihn.</ta>
            <ta e="T133" id="Seg_3424" s="T129">Jesus aber sagte ihm:</ta>
            <ta e="T137" id="Seg_3425" s="T133">"Freund, warum bist du gekommen?</ta>
            <ta e="T143" id="Seg_3426" s="T137">Ihn küssend weihst du den Sohn der Menschen dem Tode."</ta>
            <ta e="T150" id="Seg_3427" s="T143">Jesus fragte die mit Judas gekommenen Soldaten:</ta>
            <ta e="T152" id="Seg_3428" s="T150">"Wen sucht ihr?"</ta>
            <ta e="T154" id="Seg_3429" s="T152">Jene sagten:</ta>
            <ta e="T156" id="Seg_3430" s="T154">"Jesus aus Nazareth."</ta>
            <ta e="T159" id="Seg_3431" s="T156">Das Herr des Hauses sagte ihnen:</ta>
            <ta e="T161" id="Seg_3432" s="T159">"Das bin ich."</ta>
            <ta e="T171" id="Seg_3433" s="T161">Als sie das hörten, traten die Soldaten zurück und fielen rücklings auf die Erde.</ta>
            <ta e="T175" id="Seg_3434" s="T171">Dann sagte Jesus ihnen:</ta>
            <ta e="T185" id="Seg_3435" s="T175">"Ihr seid gegangen, suchend wie Mörder, mit Schwertern und Holzpfosten, um mich zu ergreifen.</ta>
            <ta e="T197" id="Seg_3436" s="T185">Jeden Tag war ich mit euch in der Kirche, um [euch] zu lehren.</ta>
            <ta e="T201" id="Seg_3437" s="T197">Dort habt ihr mich nicht genommen.</ta>
            <ta e="T209" id="Seg_3438" s="T201">Jetzt ist eure Zeit gekommen: eine dunkle, lange Zeit.</ta>
            <ta e="T213" id="Seg_3439" s="T209">Die Schriften der Propheten werden wahr."</ta>
            <ta e="T224" id="Seg_3440" s="T213">Nachdem die Soldaten Jesus Christus festgenommen hatten, brachten sie ihn in das Haus des Hohepriesters.</ta>
            <ta e="T231" id="Seg_3441" s="T224">Seine Jünger aber ließen ihn zurück und flohen.</ta>
         </annotation>
         <annotation name="fr" tierref="fr" />
         <annotation name="ltr" tierref="ltr">
            <ta e="T9" id="Seg_3442" s="T0">Детям читаем рассказы из книги "Иисус Христос – друг детей" называющейся.</ta>
            <ta e="T12" id="Seg_3443" s="T9">Иисуса поймали.</ta>
            <ta e="T19" id="Seg_3444" s="T12">Три раза Иисус приходил к обучвющи людям.</ta>
            <ta e="T26" id="Seg_3445" s="T19">Там нашедши, их увидел спящих.</ta>
            <ta e="T31" id="Seg_3446" s="T26">На третий приход он сказал им: </ta>
            <ta e="T41" id="Seg_3447" s="T31">"Вы до сих пор ещё спите что-ли, расслабившись.</ta>
            <ta e="T52" id="Seg_3448" s="T41">Вот подошло время, от человека вот дающееся грехи по людям, делающим грехи. </ta>
            <ta e="T54" id="Seg_3449" s="T52">Вставайте, пойдёмте.</ta>
            <ta e="T59" id="Seg_3450" s="T54">Вот и подошло время меня на смерть отдавать."</ta>
            <ta e="T71" id="Seg_3451" s="T59">Он так когда говорил, подошёл Иуда, это один из его учеников из двенадцати.</ta>
            <ta e="T85" id="Seg_3452" s="T71">Он первосвященникам отдал, оказывается, свой язык за тридцать золотых рублей Иисуса на смерть отдам, думая.</ta>
            <ta e="T91" id="Seg_3453" s="T85">С Иудой пришли много воюющих людей.</ta>
            <ta e="T97" id="Seg_3454" s="T91">Там разные люди из фарисеев, первосвященников.</ta>
            <ta e="T103" id="Seg_3455" s="T97">В руках кастрюли (котелки) имеют, мечь имеют и высокие доски имеют.</ta>
            <ta e="T108" id="Seg_3456" s="T103">Смерти придающий Иуда им сказал: </ta>
            <ta e="T113" id="Seg_3457" s="T108">"Кого я поцелую, тот и будет.</ta>
            <ta e="T117" id="Seg_3458" s="T113">Тогда его и поймайте".</ta>
            <ta e="T123" id="Seg_3459" s="T117">Он подошёл к Иисусу и тут сказал: </ta>
            <ta e="T129" id="Seg_3460" s="T123">"Радуйся, учитель," – так сказав, поцеловал его.</ta>
            <ta e="T133" id="Seg_3461" s="T129">Иисус вот сказал ему: </ta>
            <ta e="T137" id="Seg_3462" s="T133">"Друг, почему ты пришёл?</ta>
            <ta e="T143" id="Seg_3463" s="T137">Поцеловав что-ли, отдаёшь на смерть от человека юношу".</ta>
            <ta e="T150" id="Seg_3464" s="T143">Иисус спросил у, с Иудой пришедших, солдат: </ta>
            <ta e="T152" id="Seg_3465" s="T150">"Кого ищите?"</ta>
            <ta e="T154" id="Seg_3466" s="T152">Те ответили: </ta>
            <ta e="T156" id="Seg_3467" s="T154">"Из Назарея Иисуса".</ta>
            <ta e="T159" id="Seg_3468" s="T156">Хозяин сказал им: </ta>
            <ta e="T161" id="Seg_3469" s="T159">"Это я".</ta>
            <ta e="T171" id="Seg_3470" s="T161">Это услышав, войны люди (солдаты) назад попятились, и тут же на землю верх ногами попадали.</ta>
            <ta e="T175" id="Seg_3471" s="T171">Затем Иисус сказал им:</ta>
            <ta e="T185" id="Seg_3472" s="T175">"Вы как убийцы ищущие вышли с мечом, с высокой палкой, меня чтобы поймать.</ta>
            <ta e="T197" id="Seg_3473" s="T185">Каждый день я был с вами, обучая в храме.</ta>
            <ta e="T201" id="Seg_3474" s="T197">Там вы меня не взяли.</ta>
            <ta e="T209" id="Seg_3475" s="T201">Сейчас пришло ваше время: тёмное долгое время.</ta>
            <ta e="T213" id="Seg_3476" s="T209">Дойдут извещающими написанные рассказы."</ta>
            <ta e="T224" id="Seg_3477" s="T213">Воюющий народ, схватив Иисуса Христоса, его привели первосвященника в дом.</ta>
            <ta e="T231" id="Seg_3478" s="T224">Ученики, значит, его оставив ушли, сбежали.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T201" id="Seg_3479" s="T197">[DCh]: The form "ɨlbatakpɨt" (1PL) is unexpected and would be correct as "ɨlbatakkɨt" (2PL).</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
