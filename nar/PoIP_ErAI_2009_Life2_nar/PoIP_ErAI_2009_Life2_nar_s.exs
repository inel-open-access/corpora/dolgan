<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDCC12E5F1-977B-CF59-36C1-51F78E97F61D">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>PoIP_2009_Life2_nar</transcription-name>
         <referenced-file url="PoIP_ErAI_2009_Life2_nar.wav" />
         <referenced-file url="PoIP_ErAI_2009_Life2_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\PoIP_ErAI_2009_Life2_nar\PoIP_ErAI_2009_Life2_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">467</ud-information>
            <ud-information attribute-name="# HIAT:w">383</ud-information>
            <ud-information attribute-name="# e">383</ud-information>
            <ud-information attribute-name="# HIAT:u">39</ud-information>
            <ud-information attribute-name="# sc">40</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PoIP">
            <abbreviation>PoIP</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="ErAI">
            <abbreviation>ErAI</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="0.655" type="appl" />
         <tli id="T2" time="1.311" type="appl" />
         <tli id="T3" time="1.966" type="appl" />
         <tli id="T4" time="2.621" type="appl" />
         <tli id="T5" time="3.277" type="appl" />
         <tli id="T6" time="3.932" type="appl" />
         <tli id="T7" time="4.587" type="appl" />
         <tli id="T8" time="5.243" type="appl" />
         <tli id="T9" time="5.898" type="appl" />
         <tli id="T10" time="6.554" type="appl" />
         <tli id="T11" time="7.209" type="appl" />
         <tli id="T12" time="7.864" type="appl" />
         <tli id="T13" time="8.52" type="appl" />
         <tli id="T14" time="9.175" type="appl" />
         <tli id="T15" time="9.83" type="appl" />
         <tli id="T16" time="10.486" type="appl" />
         <tli id="T17" time="11.141" type="appl" />
         <tli id="T18" time="11.63" type="appl" />
         <tli id="T19" time="12.118" type="appl" />
         <tli id="T20" time="12.607" type="appl" />
         <tli id="T21" time="13.096" type="appl" />
         <tli id="T22" time="13.584" type="appl" />
         <tli id="T23" time="14.073" type="appl" />
         <tli id="T24" time="14.562" type="appl" />
         <tli id="T25" time="15.05" type="appl" />
         <tli id="T26" time="15.539" type="appl" />
         <tli id="T27" time="16.028" type="appl" />
         <tli id="T28" time="16.516" type="appl" />
         <tli id="T29" time="17.005" type="appl" />
         <tli id="T30" time="17.494" type="appl" />
         <tli id="T31" time="17.982" type="appl" />
         <tli id="T32" time="18.471" type="appl" />
         <tli id="T33" time="18.96" type="appl" />
         <tli id="T34" time="19.448" type="appl" />
         <tli id="T35" time="19.937" type="appl" />
         <tli id="T36" time="20.445" type="appl" />
         <tli id="T37" time="20.953" type="appl" />
         <tli id="T38" time="21.461" type="appl" />
         <tli id="T39" time="21.968" type="appl" />
         <tli id="T40" time="22.476" type="appl" />
         <tli id="T41" time="22.984" type="appl" />
         <tli id="T42" time="23.492" type="appl" />
         <tli id="T380" time="23.65576038627905" type="intp" />
         <tli id="T43" time="24.63950427522382" />
         <tli id="T44" time="25.012830097575698" />
         <tli id="T45" time="25.39282245246957" />
         <tli id="T46" time="25.55" type="appl" />
         <tli id="T47" time="26.067" type="appl" />
         <tli id="T48" time="26.584" type="appl" />
         <tli id="T50" time="27.617" type="appl" />
         <tli id="T51" time="28.134" type="appl" />
         <tli id="T52" time="28.65" type="appl" />
         <tli id="T53" time="29.167" type="appl" />
         <tli id="T54" time="29.684" type="appl" />
         <tli id="T55" time="30.201" type="appl" />
         <tli id="T56" time="30.717" type="appl" />
         <tli id="T57" time="31.234" type="appl" />
         <tli id="T58" time="31.756" type="appl" />
         <tli id="T59" time="32.278" type="appl" />
         <tli id="T60" time="32.801" type="appl" />
         <tli id="T61" time="33.323" type="appl" />
         <tli id="T62" time="33.845" type="appl" />
         <tli id="T63" time="34.368" type="appl" />
         <tli id="T64" time="34.89" type="appl" />
         <tli id="T65" time="35.412" type="appl" />
         <tli id="T66" time="35.934" type="appl" />
         <tli id="T67" time="36.457" type="appl" />
         <tli id="T68" time="36.979" type="appl" />
         <tli id="T69" time="37.501" type="appl" />
         <tli id="T70" time="38.023" type="appl" />
         <tli id="T71" time="38.546" type="appl" />
         <tli id="T72" time="39.068" type="appl" />
         <tli id="T73" time="39.59" type="appl" />
         <tli id="T74" time="40.171" type="appl" />
         <tli id="T75" time="40.752" type="appl" />
         <tli id="T76" time="41.333" type="appl" />
         <tli id="T77" time="41.914" type="appl" />
         <tli id="T78" time="42.495" type="appl" />
         <tli id="T79" time="43.076" type="appl" />
         <tli id="T80" time="43.657" type="appl" />
         <tli id="T81" time="44.238" type="appl" />
         <tli id="T82" time="44.819" type="appl" />
         <tli id="T83" time="45.4" type="appl" />
         <tli id="T84" time="45.982" type="appl" />
         <tli id="T85" time="46.563" type="appl" />
         <tli id="T86" time="47.144" type="appl" />
         <tli id="T87" time="47.725" type="appl" />
         <tli id="T88" time="48.306" type="appl" />
         <tli id="T89" time="48.887" type="appl" />
         <tli id="T90" time="49.468" type="appl" />
         <tli id="T91" time="50.049" type="appl" />
         <tli id="T92" time="50.63" type="appl" />
         <tli id="T93" time="51.211" type="appl" />
         <tli id="T94" time="52.31894738959864" />
         <tli id="T95" time="52.373" type="appl" />
         <tli id="T96" time="52.455749999999995" type="intp" />
         <tli id="T99" time="52.704" type="appl" />
         <tli id="T100" time="53.038" type="appl" />
         <tli id="T101" time="53.371" type="appl" />
         <tli id="T102" time="53.705" type="appl" />
         <tli id="T103" time="54.039" type="appl" />
         <tli id="T104" time="54.567" type="appl" />
         <tli id="T105" time="55.093" type="appl" />
         <tli id="T106" time="55.618" type="appl" />
         <tli id="T107" time="56.143" type="appl" />
         <tli id="T108" time="56.669" type="appl" />
         <tli id="T109" time="57.194" type="appl" />
         <tli id="T110" time="57.719" type="appl" />
         <tli id="T111" time="58.245" type="appl" />
         <tli id="T112" time="58.77" type="appl" />
         <tli id="T113" time="59.295" type="appl" />
         <tli id="T114" time="59.821" type="appl" />
         <tli id="T115" time="60.346" type="appl" />
         <tli id="T116" time="60.59878080675989" />
         <tli id="T117" time="61.397" type="appl" />
         <tli id="T118" time="61.922" type="appl" />
         <tli id="T119" time="62.447" type="appl" />
         <tli id="T120" time="62.973" type="appl" />
         <tli id="T121" time="63.38466876760386" />
         <tli id="T124" time="63.56650755708681" />
         <tli id="T125" time="64.016" type="appl" />
         <tli id="T126" time="64.534" type="appl" />
         <tli id="T127" time="65.051" type="appl" />
         <tli id="T128" time="65.569" type="appl" />
         <tli id="T129" time="66.087" type="appl" />
         <tli id="T130" time="66.604" type="appl" />
         <tli id="T131" time="67.122" type="appl" />
         <tli id="T132" time="67.75333087403179" />
         <tli id="T133" time="68.164" type="appl" />
         <tli id="T134" time="68.687" type="appl" />
         <tli id="T135" time="69.211" type="appl" />
         <tli id="T136" time="69.734" type="appl" />
         <tli id="T137" time="70.258" type="appl" />
         <tli id="T138" time="70.781" type="appl" />
         <tli id="T139" time="71.305" type="appl" />
         <tli id="T140" time="71.829" type="appl" />
         <tli id="T141" time="72.352" type="appl" />
         <tli id="T142" time="72.876" type="appl" />
         <tli id="T143" time="73.399" type="appl" />
         <tli id="T144" time="73.923" type="appl" />
         <tli id="T145" time="74.446" type="appl" />
         <tli id="T146" time="74.97" type="appl" />
         <tli id="T147" time="75.392" type="appl" />
         <tli id="T148" time="75.813" type="appl" />
         <tli id="T149" time="76.235" type="appl" />
         <tli id="T150" time="76.657" type="appl" />
         <tli id="T151" time="77.079" type="appl" />
         <tli id="T152" time="77.5" type="appl" />
         <tli id="T153" time="77.922" type="appl" />
         <tli id="T154" time="78.344" type="appl" />
         <tli id="T155" time="78.766" type="appl" />
         <tli id="T156" time="79.187" type="appl" />
         <tli id="T157" time="79.609" type="appl" />
         <tli id="T158" time="80.031" type="appl" />
         <tli id="T159" time="80.452" type="appl" />
         <tli id="T160" time="80.874" type="appl" />
         <tli id="T161" time="81.296" type="appl" />
         <tli id="T162" time="81.718" type="appl" />
         <tli id="T163" time="82.139" type="appl" />
         <tli id="T164" time="82.59433436965597" />
         <tli id="T165" time="82.91100768597224" />
         <tli id="T166" time="83.07166200583443" />
         <tli id="T167" time="83.25165838446836" />
         <tli id="T168" time="83.86497937833217" />
         <tli id="T169" time="84.55829876269993" />
         <tli id="T170" time="84.71829554370788" />
         <tli id="T171" time="87.112" type="appl" />
         <tli id="T172" time="87.762" type="appl" />
         <tli id="T173" time="88.412" type="appl" />
         <tli id="T174" time="89.062" type="appl" />
         <tli id="T175" time="89.35967351750327" />
         <tli id="T176" time="89.70967949652953" />
         <tli id="T177" time="89.97302055250981" />
         <tli id="T178" time="90.03818851222212" />
         <tli id="T179" time="90.0715211749321" />
         <tli id="T180" time="90.17818569560407" />
         <tli id="T181" time="90.51151232270396" />
         <tli id="T183" time="91.71696984081078" />
         <tli id="T184" time="91.99" type="appl" />
         <tli id="T185" time="92.319" type="appl" />
         <tli id="T186" time="92.647" type="appl" />
         <tli id="T187" time="92.975" type="appl" />
         <tli id="T188" time="93.304" type="appl" />
         <tli id="T189" time="93.632" type="appl" />
         <tli id="T190" time="94.021" type="appl" />
         <tli id="T191" time="94.592" type="appl" />
         <tli id="T192" time="95.163" type="appl" />
         <tli id="T193" time="95.734" type="appl" />
         <tli id="T194" time="96.305" type="appl" />
         <tli id="T195" time="96.876" type="appl" />
         <tli id="T196" time="97.446" type="appl" />
         <tli id="T197" time="98.017" type="appl" />
         <tli id="T198" time="98.588" type="appl" />
         <tli id="T199" time="99.159" type="appl" />
         <tli id="T200" time="99.73" type="appl" />
         <tli id="T201" time="100.301" type="appl" />
         <tli id="T202" time="100.872" type="appl" />
         <tli id="T203" time="101.322" type="appl" />
         <tli id="T204" time="101.771" type="appl" />
         <tli id="T205" time="102.221" type="appl" />
         <tli id="T206" time="102.67" type="appl" />
         <tli id="T207" time="103.12" type="appl" />
         <tli id="T208" time="103.569" type="appl" />
         <tli id="T209" time="104.019" type="appl" />
         <tli id="T210" time="104.468" type="appl" />
         <tli id="T211" time="104.918" type="appl" />
         <tli id="T212" time="105.368" type="appl" />
         <tli id="T213" time="105.817" type="appl" />
         <tli id="T214" time="106.267" type="appl" />
         <tli id="T215" time="106.716" type="appl" />
         <tli id="T216" time="107.166" type="appl" />
         <tli id="T217" time="107.615" type="appl" />
         <tli id="T218" time="108.065" type="appl" />
         <tli id="T219" time="108.593" type="appl" />
         <tli id="T220" time="109.122" type="appl" />
         <tli id="T221" time="109.65" type="appl" />
         <tli id="T222" time="109.976" type="appl" />
         <tli id="T223" time="110.281" type="appl" />
         <tli id="T224" time="110.587" type="appl" />
         <tli id="T225" time="110.892" type="appl" />
         <tli id="T226" time="111.198" type="appl" />
         <tli id="T227" time="111.31776038627905" />
         <tli id="T228" time="111.809" type="appl" />
         <tli id="T229" time="112.114" type="appl" />
         <tli id="T230" time="112.69773262247259" />
         <tli id="T231" time="112.853" type="appl" />
         <tli id="T232" time="113.3065" type="intp" />
         <tli id="T233" time="113.76" type="appl" />
         <tli id="T234" time="114.213" type="appl" />
         <tli id="T235" time="114.667" type="appl" />
         <tli id="T236" time="115.12" type="appl" />
         <tli id="T237" time="115.573" type="appl" />
         <tli id="T238" time="116.027" type="appl" />
         <tli id="T239" time="116.42666801629616" />
         <tli id="T240" time="116.975" type="appl" />
         <tli id="T241" time="117.471" type="appl" />
         <tli id="T242" time="117.966" type="appl" />
         <tli id="T243" time="118.462" type="appl" />
         <tli id="T244" time="118.957" type="appl" />
         <tli id="T245" time="119.453" type="appl" />
         <tli id="T246" time="119.948" type="appl" />
         <tli id="T247" time="120.444" type="appl" />
         <tli id="T248" time="120.939" type="appl" />
         <tli id="T97" time="121.28899999999999" type="intp" />
         <tli id="T249" time="121.639" type="appl" />
         <tli id="T250" time="122.339" type="appl" />
         <tli id="T251" time="123.039" type="appl" />
         <tli id="T252" time="123.74" type="appl" />
         <tli id="T253" time="124.44" type="appl" />
         <tli id="T254" time="125.14" type="appl" />
         <tli id="T255" time="125.84" type="appl" />
         <tli id="T256" time="126.54" type="appl" />
         <tli id="T257" time="127.24" type="appl" />
         <tli id="T258" time="127.941" type="appl" />
         <tli id="T259" time="128.641" type="appl" />
         <tli id="T261" time="130.14766800372198" />
         <tli id="T262" time="130.477" type="appl" />
         <tli id="T263" time="130.913" type="appl" />
         <tli id="T264" time="131.349" type="appl" />
         <tli id="T265" time="131.785" type="appl" />
         <tli id="T266" time="132.221" type="appl" />
         <tli id="T267" time="132.657" type="appl" />
         <tli id="T268" time="133.093" type="appl" />
         <tli id="T269" time="133.529" type="appl" />
         <tli id="T270" time="133.965" type="appl" />
         <tli id="T271" time="134.401" type="appl" />
         <tli id="T272" time="134.837" type="appl" />
         <tli id="T273" time="135.273" type="appl" />
         <tli id="T274" time="135.709" type="appl" />
         <tli id="T275" time="136.145" type="appl" />
         <tli id="T276" time="136.644" type="appl" />
         <tli id="T277" time="137.143" type="appl" />
         <tli id="T278" time="137.47633566165376" />
         <tli id="T279" time="137.59056513429232" />
         <tli id="T280" time="137.6838965898803" />
         <tli id="T281" time="139.4838603762197" />
         <tli id="T282" time="139.639" type="appl" />
         <tli id="T283" time="140.139" type="appl" />
         <tli id="T284" time="140.638" type="appl" />
         <tli id="T285" time="141.137" type="appl" />
         <tli id="T286" time="141.636" type="appl" />
         <tli id="T287" time="142.135" type="appl" />
         <tli id="T288" time="142.635" type="appl" />
         <tli id="T289" time="143.134" type="appl" />
         <tli id="T290" time="143.67966137712503" />
         <tli id="T296" time="143.704817121014" />
         <tli id="T297" time="144.129" type="appl" />
         <tli id="T298" time="144.40376139221408" />
         <tli id="T299" time="145.019" type="appl" />
         <tli id="T300" time="145.464" type="appl" />
         <tli id="T303" time="146.04" type="appl" />
         <tli id="T304" time="146.633" type="appl" />
         <tli id="T305" time="147.226" type="appl" />
         <tli id="T306" time="147.707" type="appl" />
         <tli id="T307" time="148.187" type="appl" />
         <tli id="T308" time="148.668" type="appl" />
         <tli id="T309" time="149.148" type="appl" />
         <tli id="T310" time="149.629" type="appl" />
         <tli id="T311" time="150.109" type="appl" />
         <tli id="T312" time="150.59" type="appl" />
         <tli id="T313" time="151.157" type="appl" />
         <tli id="T314" time="151.723" type="appl" />
         <tli id="T315" time="152.29" type="appl" />
         <tli id="T316" time="152.856" type="appl" />
         <tli id="T317" time="153.423" type="appl" />
         <tli id="T318" time="153.989" type="appl" />
         <tli id="T319" time="154.556" type="appl" />
         <tli id="T320" time="155.122" type="appl" />
         <tli id="T321" time="155.689" type="appl" />
         <tli id="T322" time="156.256" type="appl" />
         <tli id="T323" time="156.822" type="appl" />
         <tli id="T324" time="157.389" type="appl" />
         <tli id="T325" time="157.955" type="appl" />
         <tli id="T326" time="158.522" type="appl" />
         <tli id="T327" time="159.088" type="appl" />
         <tli id="T328" time="159.655" type="appl" />
         <tli id="T329" time="160.205" type="appl" />
         <tli id="T330" time="160.754" type="appl" />
         <tli id="T331" time="161.304" type="appl" />
         <tli id="T332" time="161.853" type="appl" />
         <tli id="T333" time="162.403" type="appl" />
         <tli id="T334" time="162.952" type="appl" />
         <tli id="T335" time="163.502" type="appl" />
         <tli id="T336" time="164.051" type="appl" />
         <tli id="T337" time="165.15667719545317" />
         <tli id="T338" time="165.606" type="appl" />
         <tli id="T339" time="166.612" type="appl" />
         <tli id="T340" time="167.618" type="appl" />
         <tli id="T341" time="168.623" type="appl" />
         <tli id="T342" time="169.628" type="appl" />
         <tli id="T343" time="170.634" type="appl" />
         <tli id="T344" time="171.64" type="appl" />
         <tli id="T345" time="172.645" type="appl" />
         <tli id="T346" time="173.65" type="appl" />
         <tli id="T347" time="175.0698110854039" />
         <tli id="T348" time="175.53646836334374" type="intp" />
         <tli id="T349" time="176.0031256412836" type="intp" />
         <tli id="T350" time="176.4697829192234" type="intp" />
         <tli id="T351" time="177.54976119102707" />
         <tli id="T352" time="177.85419951044494" type="intp" />
         <tli id="T353" time="178.77195882372664" type="intp" />
         <tli id="T354" time="179.68971813700836" type="intp" />
         <tli id="T355" time="180.60747745029005" type="intp" />
         <tli id="T182" time="181.47423676357175" type="intp" />
         <tli id="T356" time="181.52523676357174" type="intp" />
         <tli id="T385" time="181.73867588773766" type="intp" />
         <tli id="T357" time="181.78967588773764" />
         <tli id="T358" time="182.46966405123564" type="intp" />
         <tli id="T359" time="182.49633202561782" type="intp" />
         <tli id="T360" time="182.523" type="appl" />
         <tli id="T361" time="185.2659991763907" />
         <tli id="T369" time="190.9068882909164" />
         <tli id="T370" time="191.747" type="appl" />
         <tli id="T371" time="192.309" type="appl" />
         <tli id="T372" time="192.871" type="appl" />
         <tli id="T373" time="193.433" type="appl" />
         <tli id="T374" time="193.995" type="appl" />
         <tli id="T375" time="194.8694127351373" />
         <tli id="T376" time="195.60939784729905" />
         <tli id="T377" time="195.77432942485666" />
         <tli id="T49" time="196.22518623943768" type="intp" />
         <tli id="T378" time="197.00270314857661" />
         <tli id="T379" time="197.82935318378432" />
         <tli id="T384" time="198.816" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-PoIP"
                      id="tx-PoIP"
                      speaker="PoIP"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-PoIP">
            <ts e="T380" id="Seg_0" n="sc" s="T0">
               <ts e="T17" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Haːmajdarɨŋ</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">tu͡oktarɨŋ</ts>
                  <nts id="Seg_8" n="HIAT:ip">,</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_11" n="HIAT:w" s="T2">a</ts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T3">haːmajdarɨŋ</ts>
                  <nts id="Seg_15" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_17" n="HIAT:w" s="T4">köstöktörüne</ts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_20" n="HIAT:w" s="T5">bu͡ollagɨna</ts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_23" n="HIAT:w" s="T6">dʼɨl</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_26" n="HIAT:w" s="T7">ajɨ</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_29" n="HIAT:w" s="T8">taba</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_32" n="HIAT:w" s="T9">tiriːtin</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_35" n="HIAT:w" s="T10">keterdeller</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_38" n="HIAT:w" s="T11">ebit</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_41" n="HIAT:w" s="T12">ol</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_44" n="HIAT:w" s="T13">taːska</ts>
                  <nts id="Seg_45" n="HIAT:ip">,</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_48" n="HIAT:w" s="T14">heː</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_51" n="HIAT:w" s="T15">ol</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_54" n="HIAT:w" s="T16">ogonnʼorgor</ts>
                  <nts id="Seg_55" n="HIAT:ip">.</nts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_58" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_60" n="HIAT:w" s="T17">Dʼɨl</ts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_63" n="HIAT:w" s="T18">ajɨ</ts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_66" n="HIAT:w" s="T19">dʼe</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_69" n="HIAT:w" s="T20">ol</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_72" n="HIAT:w" s="T21">tiriːler</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_75" n="HIAT:w" s="T22">hɨtɨjan</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_78" n="HIAT:w" s="T23">hirge</ts>
                  <nts id="Seg_79" n="HIAT:ip">,</nts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_82" n="HIAT:w" s="T24">hirge</ts>
                  <nts id="Seg_83" n="HIAT:ip">,</nts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_86" n="HIAT:w" s="T25">nöŋü͡ö</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_89" n="HIAT:w" s="T26">dʼɨlɨn</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_92" n="HIAT:w" s="T27">emi͡e</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_95" n="HIAT:w" s="T28">köhön</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_98" n="HIAT:w" s="T29">keleller</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_101" n="HIAT:w" s="T30">haːmajdarɨŋ</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_104" n="HIAT:w" s="T31">opjatʼ</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_107" n="HIAT:w" s="T32">atɨn</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_110" n="HIAT:w" s="T33">tiriːni</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_113" n="HIAT:w" s="T34">emi͡e</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T380" id="Seg_117" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_119" n="HIAT:w" s="T35">Tabanɨ</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_122" n="HIAT:w" s="T36">ölörön</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_125" n="HIAT:w" s="T37">baran</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_128" n="HIAT:w" s="T38">emi͡e</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_131" n="HIAT:w" s="T39">keterdeller</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_134" n="HIAT:w" s="T40">ti</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_137" n="HIAT:w" s="T41">kör</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_140" n="HIAT:w" s="T42">da</ts>
                  <nts id="Seg_141" n="HIAT:ip">.</nts>
                  <nts id="Seg_142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T95" id="Seg_143" n="sc" s="T43">
               <ts e="T57" id="Seg_145" n="HIAT:u" s="T43">
                  <ts e="T44" id="Seg_147" n="HIAT:w" s="T43">Voːbščem</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_150" n="HIAT:w" s="T44">ol</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_153" n="HIAT:w" s="T45">hirge</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_156" n="HIAT:w" s="T46">ol</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_159" n="HIAT:w" s="T47">ile</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_161" n="HIAT:ip">(</nts>
                  <ts e="T50" id="Seg_163" n="HIAT:w" s="T48">č-</ts>
                  <nts id="Seg_164" n="HIAT:ip">)</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_167" n="HIAT:w" s="T50">min</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_170" n="HIAT:w" s="T51">vezde</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_173" n="HIAT:w" s="T52">hɨldʼɨbɨt</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_176" n="HIAT:w" s="T53">emeːksimmin</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_179" n="HIAT:w" s="T54">bu</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_182" n="HIAT:w" s="T55">dojduga</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_185" n="HIAT:w" s="T56">kelemmin</ts>
                  <nts id="Seg_186" n="HIAT:ip">.</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T73" id="Seg_189" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_191" n="HIAT:w" s="T57">Ol</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_194" n="HIAT:w" s="T58">ühe</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_197" n="HIAT:w" s="T59">bagas</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_200" n="HIAT:w" s="T60">kajdi͡ek</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_203" n="HIAT:w" s="T61">hɨldʼɨ͡akpɨtɨj</ts>
                  <nts id="Seg_204" n="HIAT:ip">,</nts>
                  <nts id="Seg_205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_207" n="HIAT:w" s="T62">ühe</ts>
                  <nts id="Seg_208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_210" n="HIAT:w" s="T63">ol</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_213" n="HIAT:w" s="T64">di͡ek</ts>
                  <nts id="Seg_214" n="HIAT:ip">,</nts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_217" n="HIAT:w" s="T65">bu</ts>
                  <nts id="Seg_218" n="HIAT:ip">,</nts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_221" n="HIAT:w" s="T66">ogonnʼorbor</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_224" n="HIAT:w" s="T67">kelemmin</ts>
                  <nts id="Seg_225" n="HIAT:ip">,</nts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_228" n="HIAT:w" s="T68">iti</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_231" n="HIAT:w" s="T69">köspütüm</ts>
                  <nts id="Seg_232" n="HIAT:ip">,</nts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_235" n="HIAT:w" s="T70">bu</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_238" n="HIAT:w" s="T71">ogonnʼorbor</ts>
                  <nts id="Seg_239" n="HIAT:ip">,</nts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_242" n="HIAT:w" s="T72">amattan</ts>
                  <nts id="Seg_243" n="HIAT:ip">.</nts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T95" id="Seg_246" n="HIAT:u" s="T73">
                  <ts e="T74" id="Seg_248" n="HIAT:w" s="T73">Dʼe</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_251" n="HIAT:w" s="T74">ol</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_254" n="HIAT:w" s="T75">araː</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_257" n="HIAT:w" s="T76">dogo</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_260" n="HIAT:w" s="T77">ile</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_263" n="HIAT:w" s="T78">ile</ts>
                  <nts id="Seg_264" n="HIAT:ip">,</nts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_267" n="HIAT:w" s="T79">onton</ts>
                  <nts id="Seg_268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_270" n="HIAT:w" s="T80">bu͡ollagɨna</ts>
                  <nts id="Seg_271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_273" n="HIAT:w" s="T81">kime</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_276" n="HIAT:w" s="T82">iti</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_279" n="HIAT:w" s="T83">Tonuskajga</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_282" n="HIAT:w" s="T84">emi͡e</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_285" n="HIAT:w" s="T85">baːr</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_288" n="HIAT:w" s="T86">ühü</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_291" n="HIAT:w" s="T87">dogo</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_294" n="HIAT:w" s="T88">iti</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_297" n="HIAT:w" s="T89">kim</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_300" n="HIAT:w" s="T90">taːs</ts>
                  <nts id="Seg_301" n="HIAT:ip">,</nts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_304" n="HIAT:w" s="T91">min</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_307" n="HIAT:w" s="T92">onu</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_310" n="HIAT:w" s="T93">dʼe</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_313" n="HIAT:w" s="T94">körbötögüm</ts>
                  <nts id="Seg_314" n="HIAT:ip">.</nts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T121" id="Seg_316" n="sc" s="T103">
               <ts e="T121" id="Seg_318" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_320" n="HIAT:w" s="T103">Körbötögüm</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_323" n="HIAT:w" s="T104">onno</ts>
                  <nts id="Seg_324" n="HIAT:ip">,</nts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_327" n="HIAT:w" s="T105">iti</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_330" n="HIAT:w" s="T106">Tonuskajga</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_333" n="HIAT:w" s="T107">baːr</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_336" n="HIAT:w" s="T108">diːller</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_339" n="HIAT:w" s="T109">ol</ts>
                  <nts id="Seg_340" n="HIAT:ip">,</nts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_343" n="HIAT:w" s="T110">ol</ts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_346" n="HIAT:w" s="T111">bu</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_349" n="HIAT:w" s="T112">min</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_352" n="HIAT:w" s="T113">ogonnʼorum</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_355" n="HIAT:w" s="T114">teːtete</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_358" n="HIAT:w" s="T115">ke</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_361" n="HIAT:w" s="T116">ogonnʼor</ts>
                  <nts id="Seg_362" n="HIAT:ip">,</nts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_365" n="HIAT:w" s="T117">Leŋtej</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_368" n="HIAT:w" s="T118">Leŋtej</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_371" n="HIAT:w" s="T119">ogonnʼor</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_374" n="HIAT:w" s="T120">kɨrdʼagas</ts>
                  <nts id="Seg_375" n="HIAT:ip">.</nts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T164" id="Seg_377" n="sc" s="T124">
               <ts e="T132" id="Seg_379" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_381" n="HIAT:w" s="T124">Ontuŋ</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_384" n="HIAT:w" s="T125">ol</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_387" n="HIAT:w" s="T126">taːhɨ</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_390" n="HIAT:w" s="T127">dʼe</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_393" n="HIAT:w" s="T128">teber</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_396" n="HIAT:w" s="T129">ebit</ts>
                  <nts id="Seg_397" n="HIAT:ip">,</nts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_400" n="HIAT:w" s="T130">teppit</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_403" n="HIAT:w" s="T131">uslučajno</ts>
                  <nts id="Seg_404" n="HIAT:ip">.</nts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T146" id="Seg_407" n="HIAT:u" s="T132">
                  <ts e="T133" id="Seg_409" n="HIAT:w" s="T132">Ol</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_412" n="HIAT:w" s="T133">taːhɨŋ</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_415" n="HIAT:w" s="T134">bu͡ollagɨna</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_418" n="HIAT:w" s="T135">balɨk</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_421" n="HIAT:w" s="T136">gi͡ene</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_424" n="HIAT:w" s="T137">ebit</ts>
                  <nts id="Seg_425" n="HIAT:ip">,</nts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_428" n="HIAT:w" s="T138">balɨk</ts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_431" n="HIAT:w" s="T139">bi͡erer</ts>
                  <nts id="Seg_432" n="HIAT:ip">,</nts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_435" n="HIAT:w" s="T140">emi͡e</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_438" n="HIAT:w" s="T141">kim</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_441" n="HIAT:w" s="T142">bu͡olla</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_444" n="HIAT:w" s="T143">iččileːk</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_447" n="HIAT:w" s="T144">taːs</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_450" n="HIAT:w" s="T145">ebit</ts>
                  <nts id="Seg_451" n="HIAT:ip">.</nts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T164" id="Seg_454" n="HIAT:u" s="T146">
                  <ts e="T147" id="Seg_456" n="HIAT:w" s="T146">Ontuŋ</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_459" n="HIAT:w" s="T147">hirten</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_462" n="HIAT:w" s="T148">köstübet</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_465" n="HIAT:w" s="T149">e</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_468" n="HIAT:w" s="T150">tak</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_471" n="HIAT:w" s="T151">ta</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_474" n="HIAT:w" s="T152">oččukaːn</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_477" n="HIAT:w" s="T153">ühü</ts>
                  <nts id="Seg_478" n="HIAT:ip">,</nts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_481" n="HIAT:w" s="T154">tak</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_484" n="HIAT:w" s="T155">ta</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_487" n="HIAT:w" s="T156">min</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_490" n="HIAT:w" s="T157">körbötögüm</ts>
                  <nts id="Seg_491" n="HIAT:ip">,</nts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_494" n="HIAT:w" s="T158">hiti</ts>
                  <nts id="Seg_495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_497" n="HIAT:w" s="T159">hiti</ts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_500" n="HIAT:w" s="T160">kanna</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_503" n="HIAT:w" s="T161">di͡ebitterej</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_506" n="HIAT:w" s="T162">ke</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_509" n="HIAT:w" s="T163">hitinne</ts>
                  <nts id="Seg_510" n="HIAT:ip">.</nts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T181" id="Seg_512" n="sc" s="T167">
               <ts e="T181" id="Seg_514" n="HIAT:u" s="T167">
                  <ts e="T168" id="Seg_516" n="HIAT:w" s="T167">Hitinne</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_519" n="HIAT:w" s="T168">heː</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_522" n="HIAT:w" s="T169">hitinne</ts>
                  <nts id="Seg_523" n="HIAT:ip">,</nts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_526" n="HIAT:w" s="T170">hitinne</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_529" n="HIAT:w" s="T171">čugas</ts>
                  <nts id="Seg_530" n="HIAT:ip">,</nts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_533" n="HIAT:w" s="T172">čugas</ts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_536" n="HIAT:w" s="T173">bu</ts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_539" n="HIAT:w" s="T174">nʼu͡oguhuttar</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_542" n="HIAT:w" s="T175">oloror</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_545" n="HIAT:w" s="T176">hirderitten</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_548" n="HIAT:w" s="T177">bu</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_551" n="HIAT:w" s="T178">di͡ekkaːn</ts>
                  <nts id="Seg_552" n="HIAT:ip">,</nts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_555" n="HIAT:w" s="T179">hol</ts>
                  <nts id="Seg_556" n="HIAT:ip">,</nts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_559" n="HIAT:w" s="T180">baːr</ts>
                  <nts id="Seg_560" n="HIAT:ip">.</nts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T221" id="Seg_562" n="sc" s="T189">
               <ts e="T202" id="Seg_564" n="HIAT:u" s="T189">
                  <ts e="T190" id="Seg_566" n="HIAT:w" s="T189">Dʼe</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_569" n="HIAT:w" s="T190">onno</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_572" n="HIAT:w" s="T191">barɨtɨn</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_575" n="HIAT:w" s="T192">bɨragagɨn</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_578" n="HIAT:w" s="T193">podarok</ts>
                  <nts id="Seg_579" n="HIAT:ip">,</nts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_582" n="HIAT:w" s="T194">patron</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_585" n="HIAT:w" s="T195">bu͡ol</ts>
                  <nts id="Seg_586" n="HIAT:ip">,</nts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_589" n="HIAT:w" s="T196">timek</ts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_592" n="HIAT:w" s="T197">bu͡ol</ts>
                  <nts id="Seg_593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_595" n="HIAT:w" s="T198">barɨtɨn</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_598" n="HIAT:w" s="T199">žilet</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_601" n="HIAT:w" s="T200">eŋin-eŋin</ts>
                  <nts id="Seg_602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_604" n="HIAT:w" s="T201">hepter</ts>
                  <nts id="Seg_605" n="HIAT:ip">.</nts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T218" id="Seg_608" n="HIAT:u" s="T202">
                  <ts e="T203" id="Seg_610" n="HIAT:w" s="T202">Ol</ts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_613" n="HIAT:w" s="T203">ogonnʼoruŋ</ts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_616" n="HIAT:w" s="T204">ol</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_619" n="HIAT:w" s="T205">dʼe</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_622" n="HIAT:w" s="T206">teppit</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_625" n="HIAT:w" s="T207">ol</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_628" n="HIAT:w" s="T208">taːskɨn</ts>
                  <nts id="Seg_629" n="HIAT:ip">,</nts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_632" n="HIAT:w" s="T209">ol</ts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_635" n="HIAT:w" s="T210">kɨrdʼagas</ts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_638" n="HIAT:w" s="T211">ogonnʼor</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_641" n="HIAT:w" s="T212">ke</ts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_644" n="HIAT:w" s="T213">haːtar</ts>
                  <nts id="Seg_645" n="HIAT:ip">,</nts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_648" n="HIAT:w" s="T214">kör</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_651" n="HIAT:w" s="T215">da</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_654" n="HIAT:w" s="T216">ontuŋ</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_657" n="HIAT:w" s="T217">atagɨnan</ts>
                  <nts id="Seg_658" n="HIAT:ip">.</nts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T221" id="Seg_661" n="HIAT:u" s="T218">
                  <ts e="T219" id="Seg_663" n="HIAT:w" s="T218">Kohu͡on</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_666" n="HIAT:w" s="T219">künü</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_669" n="HIAT:w" s="T220">hɨppɨt</ts>
                  <nts id="Seg_670" n="HIAT:ip">.</nts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T290" id="Seg_672" n="sc" s="T227">
               <ts e="T239" id="Seg_674" n="HIAT:u" s="T227">
                  <ts e="T231" id="Seg_676" n="HIAT:w" s="T227">Heː</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_679" n="HIAT:w" s="T231">slɨšal</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_682" n="HIAT:w" s="T232">heː</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_685" n="HIAT:w" s="T233">bu͡o</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_688" n="HIAT:w" s="T234">bu͡o</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_691" n="HIAT:w" s="T235">bu͡o</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_694" n="HIAT:w" s="T236">itinnik</ts>
                  <nts id="Seg_695" n="HIAT:ip">,</nts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_698" n="HIAT:w" s="T237">itinnik</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_701" n="HIAT:w" s="T238">dogo</ts>
                  <nts id="Seg_702" n="HIAT:ip">.</nts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T248" id="Seg_705" n="HIAT:u" s="T239">
                  <ts e="T240" id="Seg_707" n="HIAT:w" s="T239">Bu</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_710" n="HIAT:w" s="T240">hirin</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_713" n="HIAT:w" s="T241">da</ts>
                  <nts id="Seg_714" n="HIAT:ip">,</nts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_717" n="HIAT:w" s="T242">iččileːkter</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_720" n="HIAT:w" s="T243">bu͡o</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_723" n="HIAT:w" s="T244">bu</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_726" n="HIAT:w" s="T245">hirderbit</ts>
                  <nts id="Seg_727" n="HIAT:ip">,</nts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_730" n="HIAT:w" s="T246">kihi</ts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_733" n="HIAT:w" s="T247">kuttanɨ͡ak</ts>
                  <nts id="Seg_734" n="HIAT:ip">.</nts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T261" id="Seg_737" n="HIAT:u" s="T248">
                  <ts e="T97" id="Seg_739" n="HIAT:w" s="T248">Mmm</ts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_742" n="HIAT:w" s="T97">ile</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_745" n="HIAT:w" s="T249">eŋin-eŋin</ts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_748" n="HIAT:w" s="T250">bu͡olaːččɨ</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_751" n="HIAT:w" s="T251">bu͡olla</ts>
                  <nts id="Seg_752" n="HIAT:ip">,</nts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_755" n="HIAT:w" s="T252">bihigi</ts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_758" n="HIAT:w" s="T253">ol</ts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_761" n="HIAT:w" s="T254">bejelerge</ts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_764" n="HIAT:w" s="T255">bagas</ts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_767" n="HIAT:w" s="T256">ile</ts>
                  <nts id="Seg_768" n="HIAT:ip">,</nts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_771" n="HIAT:w" s="T257">delʼbi</ts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_774" n="HIAT:w" s="T258">belek</ts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_777" n="HIAT:w" s="T259">keːspippit</ts>
                  <nts id="Seg_778" n="HIAT:ip">.</nts>
                  <nts id="Seg_779" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T275" id="Seg_781" n="HIAT:u" s="T261">
                  <ts e="T262" id="Seg_783" n="HIAT:w" s="T261">Dogo</ts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_786" n="HIAT:w" s="T262">interehɨn</ts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_789" n="HIAT:w" s="T263">da</ts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_792" n="HIAT:w" s="T264">e</ts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_795" n="HIAT:w" s="T265">da</ts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_798" n="HIAT:w" s="T266">ol</ts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_801" n="HIAT:w" s="T267">huruktar</ts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_804" n="HIAT:w" s="T268">hɨtallara</ts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_807" n="HIAT:w" s="T269">habɨːlaːk</ts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_810" n="HIAT:w" s="T270">bu͡o</ts>
                  <nts id="Seg_811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_813" n="HIAT:w" s="T271">ikki</ts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_816" n="HIAT:w" s="T272">habɨːlaːk</ts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_819" n="HIAT:w" s="T273">ol</ts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_822" n="HIAT:w" s="T274">kolbujaŋ</ts>
                  <nts id="Seg_823" n="HIAT:ip">.</nts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T290" id="Seg_826" n="HIAT:u" s="T275">
                  <ts e="T276" id="Seg_828" n="HIAT:w" s="T275">Iti</ts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_831" n="HIAT:w" s="T276">ke</ts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_834" n="HIAT:w" s="T277">urut</ts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_837" n="HIAT:w" s="T278">čaːj</ts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_840" n="HIAT:w" s="T279">kaːlara</ts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_843" n="HIAT:w" s="T280">kanna</ts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_846" n="HIAT:w" s="T281">bulaːččɨ</ts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_849" n="HIAT:w" s="T282">etilere</ts>
                  <nts id="Seg_850" n="HIAT:ip">,</nts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_853" n="HIAT:w" s="T283">dʼe</ts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_856" n="HIAT:w" s="T284">dʼe</ts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_859" n="HIAT:w" s="T285">takie</ts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_862" n="HIAT:w" s="T286">korobki</ts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_865" n="HIAT:w" s="T287">bɨli</ts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_868" n="HIAT:w" s="T288">onton</ts>
                  <nts id="Seg_869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_871" n="HIAT:w" s="T289">ol</ts>
                  <nts id="Seg_872" n="HIAT:ip">.</nts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T361" id="Seg_874" n="sc" s="T298">
               <ts e="T305" id="Seg_876" n="HIAT:u" s="T298">
                  <ts e="T299" id="Seg_878" n="HIAT:w" s="T298">Bugurdukkaːn</ts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_881" n="HIAT:w" s="T299">haban</ts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_884" n="HIAT:w" s="T300">keːhi͡e</ts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_887" n="HIAT:w" s="T303">heː</ts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_890" n="HIAT:w" s="T304">bugurdukkaːn</ts>
                  <nts id="Seg_891" n="HIAT:ip">.</nts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T312" id="Seg_894" n="HIAT:u" s="T305">
                  <ts e="T306" id="Seg_896" n="HIAT:w" s="T305">Onton</ts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_899" n="HIAT:w" s="T306">ikkihin</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_902" n="HIAT:w" s="T307">habagɨn</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_905" n="HIAT:w" s="T308">dʼe</ts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_908" n="HIAT:w" s="T309">onnuguŋ</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_911" n="HIAT:w" s="T310">aldʼammat</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_914" n="HIAT:w" s="T311">bu͡o</ts>
                  <nts id="Seg_915" n="HIAT:ip">.</nts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T328" id="Seg_918" n="HIAT:u" s="T312">
                  <ts e="T313" id="Seg_920" n="HIAT:w" s="T312">Koloruk</ts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_923" n="HIAT:w" s="T313">kördükter</ts>
                  <nts id="Seg_924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_926" n="HIAT:w" s="T314">bugurduk</ts>
                  <nts id="Seg_927" n="HIAT:ip">,</nts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_930" n="HIAT:w" s="T315">bugurduk</ts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_933" n="HIAT:w" s="T316">ile</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_936" n="HIAT:w" s="T317">basku͡ojin</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_939" n="HIAT:w" s="T318">da</ts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_942" n="HIAT:w" s="T319">ile</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_945" n="HIAT:w" s="T320">bulbuta</ts>
                  <nts id="Seg_946" n="HIAT:ip">,</nts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_949" n="HIAT:w" s="T321">ol</ts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_952" n="HIAT:w" s="T322">bu</ts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_955" n="HIAT:w" s="T323">taŋaraŋ</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_958" n="HIAT:w" s="T324">oŋorbut</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_961" n="HIAT:w" s="T325">ühü</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_964" n="HIAT:w" s="T326">hir</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_967" n="HIAT:w" s="T327">turarga</ts>
                  <nts id="Seg_968" n="HIAT:ip">.</nts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T337" id="Seg_971" n="HIAT:u" s="T328">
                  <ts e="T329" id="Seg_973" n="HIAT:w" s="T328">Araː</ts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_976" n="HIAT:w" s="T329">min</ts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_979" n="HIAT:w" s="T330">diːr</ts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_982" n="HIAT:w" s="T331">etim</ts>
                  <nts id="Seg_983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_985" n="HIAT:w" s="T332">ara</ts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_988" n="HIAT:w" s="T333">ile</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_991" n="HIAT:w" s="T334">gorod</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_994" n="HIAT:w" s="T335">basku͡ojin</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_997" n="HIAT:w" s="T336">daː</ts>
                  <nts id="Seg_998" n="HIAT:ip">.</nts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T347" id="Seg_1001" n="HIAT:u" s="T337">
                  <ts e="T338" id="Seg_1003" n="HIAT:w" s="T337">Tönnöːrübüt</ts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1006" n="HIAT:w" s="T338">onno</ts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1009" n="HIAT:w" s="T339">tagajbatakpɨt</ts>
                  <nts id="Seg_1010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1012" n="HIAT:w" s="T340">tönnöːrübüt</ts>
                  <nts id="Seg_1013" n="HIAT:ip">,</nts>
                  <nts id="Seg_1014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1016" n="HIAT:w" s="T341">baraːrɨbɨt</ts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1019" n="HIAT:w" s="T342">agaj</ts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1022" n="HIAT:w" s="T343">ol</ts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1025" n="HIAT:w" s="T344">barɨta</ts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1028" n="HIAT:w" s="T345">podarok</ts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1031" n="HIAT:w" s="T346">keːhetteːbippit</ts>
                  <nts id="Seg_1032" n="HIAT:ip">.</nts>
                  <nts id="Seg_1033" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T351" id="Seg_1035" n="HIAT:u" s="T347">
                  <ts e="T348" id="Seg_1037" n="HIAT:w" s="T347">min</ts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1040" n="HIAT:w" s="T348">kimi͡eke</ts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1043" n="HIAT:w" s="T349">kepseːbitimij</ts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1046" n="HIAT:w" s="T350">ke</ts>
                  <nts id="Seg_1047" n="HIAT:ip">?</nts>
                  <nts id="Seg_1048" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T357" id="Seg_1050" n="HIAT:u" s="T351">
                  <ts e="T352" id="Seg_1052" n="HIAT:w" s="T351">Eː</ts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1055" n="HIAT:w" s="T352">Tatʼjana</ts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1058" n="HIAT:w" s="T353">kimi͡eke</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1061" n="HIAT:w" s="T354">Ivanovnaga</ts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1064" n="HIAT:w" s="T355">kepseːbippin</ts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1067" n="HIAT:w" s="T356">ebit</ts>
                  <nts id="Seg_1068" n="HIAT:ip">.</nts>
                  <nts id="Seg_1069" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T361" id="Seg_1071" n="HIAT:u" s="T357">
                  <ts e="T358" id="Seg_1073" n="HIAT:w" s="T357">Tatʼjana</ts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1076" n="HIAT:w" s="T358">Ivanovnaga</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1079" n="HIAT:w" s="T359">oloru</ts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1082" n="HIAT:w" s="T360">kepseːbitim</ts>
                  <nts id="Seg_1083" n="HIAT:ip">.</nts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T384" id="Seg_1085" n="sc" s="T375">
               <ts e="T384" id="Seg_1087" n="HIAT:u" s="T375">
                  <ts e="T376" id="Seg_1089" n="HIAT:w" s="T375">Onton</ts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1092" n="HIAT:w" s="T376">tugu</ts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1095" n="HIAT:w" s="T377">ɨjɨtɨ͡ak</ts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1098" n="HIAT:w" s="T378">araː</ts>
                  <nts id="Seg_1099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1101" n="HIAT:w" s="T379">ontum</ts>
                  <nts id="Seg_1102" n="HIAT:ip">.</nts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-PoIP">
            <ts e="T380" id="Seg_1104" n="sc" s="T0">
               <ts e="T1" id="Seg_1106" n="e" s="T0">Haːmajdarɨŋ </ts>
               <ts e="T2" id="Seg_1108" n="e" s="T1">tu͡oktarɨŋ, </ts>
               <ts e="T3" id="Seg_1110" n="e" s="T2">a </ts>
               <ts e="T4" id="Seg_1112" n="e" s="T3">haːmajdarɨŋ </ts>
               <ts e="T5" id="Seg_1114" n="e" s="T4">köstöktörüne </ts>
               <ts e="T6" id="Seg_1116" n="e" s="T5">bu͡ollagɨna </ts>
               <ts e="T7" id="Seg_1118" n="e" s="T6">dʼɨl </ts>
               <ts e="T8" id="Seg_1120" n="e" s="T7">ajɨ </ts>
               <ts e="T9" id="Seg_1122" n="e" s="T8">taba </ts>
               <ts e="T10" id="Seg_1124" n="e" s="T9">tiriːtin </ts>
               <ts e="T11" id="Seg_1126" n="e" s="T10">keterdeller </ts>
               <ts e="T12" id="Seg_1128" n="e" s="T11">ebit </ts>
               <ts e="T13" id="Seg_1130" n="e" s="T12">ol </ts>
               <ts e="T14" id="Seg_1132" n="e" s="T13">taːska, </ts>
               <ts e="T15" id="Seg_1134" n="e" s="T14">heː </ts>
               <ts e="T16" id="Seg_1136" n="e" s="T15">ol </ts>
               <ts e="T17" id="Seg_1138" n="e" s="T16">ogonnʼorgor. </ts>
               <ts e="T18" id="Seg_1140" n="e" s="T17">Dʼɨl </ts>
               <ts e="T19" id="Seg_1142" n="e" s="T18">ajɨ </ts>
               <ts e="T20" id="Seg_1144" n="e" s="T19">dʼe </ts>
               <ts e="T21" id="Seg_1146" n="e" s="T20">ol </ts>
               <ts e="T22" id="Seg_1148" n="e" s="T21">tiriːler </ts>
               <ts e="T23" id="Seg_1150" n="e" s="T22">hɨtɨjan </ts>
               <ts e="T24" id="Seg_1152" n="e" s="T23">hirge, </ts>
               <ts e="T25" id="Seg_1154" n="e" s="T24">hirge, </ts>
               <ts e="T26" id="Seg_1156" n="e" s="T25">nöŋü͡ö </ts>
               <ts e="T27" id="Seg_1158" n="e" s="T26">dʼɨlɨn </ts>
               <ts e="T28" id="Seg_1160" n="e" s="T27">emi͡e </ts>
               <ts e="T29" id="Seg_1162" n="e" s="T28">köhön </ts>
               <ts e="T30" id="Seg_1164" n="e" s="T29">keleller </ts>
               <ts e="T31" id="Seg_1166" n="e" s="T30">haːmajdarɨŋ </ts>
               <ts e="T32" id="Seg_1168" n="e" s="T31">opjatʼ </ts>
               <ts e="T33" id="Seg_1170" n="e" s="T32">atɨn </ts>
               <ts e="T34" id="Seg_1172" n="e" s="T33">tiriːni </ts>
               <ts e="T35" id="Seg_1174" n="e" s="T34">emi͡e. </ts>
               <ts e="T36" id="Seg_1176" n="e" s="T35">Tabanɨ </ts>
               <ts e="T37" id="Seg_1178" n="e" s="T36">ölörön </ts>
               <ts e="T38" id="Seg_1180" n="e" s="T37">baran </ts>
               <ts e="T39" id="Seg_1182" n="e" s="T38">emi͡e </ts>
               <ts e="T40" id="Seg_1184" n="e" s="T39">keterdeller </ts>
               <ts e="T41" id="Seg_1186" n="e" s="T40">ti </ts>
               <ts e="T42" id="Seg_1188" n="e" s="T41">kör </ts>
               <ts e="T380" id="Seg_1190" n="e" s="T42">da. </ts>
            </ts>
            <ts e="T95" id="Seg_1191" n="sc" s="T43">
               <ts e="T44" id="Seg_1193" n="e" s="T43">Voːbščem </ts>
               <ts e="T45" id="Seg_1195" n="e" s="T44">ol </ts>
               <ts e="T46" id="Seg_1197" n="e" s="T45">hirge </ts>
               <ts e="T47" id="Seg_1199" n="e" s="T46">ol </ts>
               <ts e="T48" id="Seg_1201" n="e" s="T47">ile </ts>
               <ts e="T50" id="Seg_1203" n="e" s="T48">(č-) </ts>
               <ts e="T51" id="Seg_1205" n="e" s="T50">min </ts>
               <ts e="T52" id="Seg_1207" n="e" s="T51">vezde </ts>
               <ts e="T53" id="Seg_1209" n="e" s="T52">hɨldʼɨbɨt </ts>
               <ts e="T54" id="Seg_1211" n="e" s="T53">emeːksimmin </ts>
               <ts e="T55" id="Seg_1213" n="e" s="T54">bu </ts>
               <ts e="T56" id="Seg_1215" n="e" s="T55">dojduga </ts>
               <ts e="T57" id="Seg_1217" n="e" s="T56">kelemmin. </ts>
               <ts e="T58" id="Seg_1219" n="e" s="T57">Ol </ts>
               <ts e="T59" id="Seg_1221" n="e" s="T58">ühe </ts>
               <ts e="T60" id="Seg_1223" n="e" s="T59">bagas </ts>
               <ts e="T61" id="Seg_1225" n="e" s="T60">kajdi͡ek </ts>
               <ts e="T62" id="Seg_1227" n="e" s="T61">hɨldʼɨ͡akpɨtɨj, </ts>
               <ts e="T63" id="Seg_1229" n="e" s="T62">ühe </ts>
               <ts e="T64" id="Seg_1231" n="e" s="T63">ol </ts>
               <ts e="T65" id="Seg_1233" n="e" s="T64">di͡ek, </ts>
               <ts e="T66" id="Seg_1235" n="e" s="T65">bu, </ts>
               <ts e="T67" id="Seg_1237" n="e" s="T66">ogonnʼorbor </ts>
               <ts e="T68" id="Seg_1239" n="e" s="T67">kelemmin, </ts>
               <ts e="T69" id="Seg_1241" n="e" s="T68">iti </ts>
               <ts e="T70" id="Seg_1243" n="e" s="T69">köspütüm, </ts>
               <ts e="T71" id="Seg_1245" n="e" s="T70">bu </ts>
               <ts e="T72" id="Seg_1247" n="e" s="T71">ogonnʼorbor, </ts>
               <ts e="T73" id="Seg_1249" n="e" s="T72">amattan. </ts>
               <ts e="T74" id="Seg_1251" n="e" s="T73">Dʼe </ts>
               <ts e="T75" id="Seg_1253" n="e" s="T74">ol </ts>
               <ts e="T76" id="Seg_1255" n="e" s="T75">araː </ts>
               <ts e="T77" id="Seg_1257" n="e" s="T76">dogo </ts>
               <ts e="T78" id="Seg_1259" n="e" s="T77">ile </ts>
               <ts e="T79" id="Seg_1261" n="e" s="T78">ile, </ts>
               <ts e="T80" id="Seg_1263" n="e" s="T79">onton </ts>
               <ts e="T81" id="Seg_1265" n="e" s="T80">bu͡ollagɨna </ts>
               <ts e="T82" id="Seg_1267" n="e" s="T81">kime </ts>
               <ts e="T83" id="Seg_1269" n="e" s="T82">iti </ts>
               <ts e="T84" id="Seg_1271" n="e" s="T83">Tonuskajga </ts>
               <ts e="T85" id="Seg_1273" n="e" s="T84">emi͡e </ts>
               <ts e="T86" id="Seg_1275" n="e" s="T85">baːr </ts>
               <ts e="T87" id="Seg_1277" n="e" s="T86">ühü </ts>
               <ts e="T88" id="Seg_1279" n="e" s="T87">dogo </ts>
               <ts e="T89" id="Seg_1281" n="e" s="T88">iti </ts>
               <ts e="T90" id="Seg_1283" n="e" s="T89">kim </ts>
               <ts e="T91" id="Seg_1285" n="e" s="T90">taːs, </ts>
               <ts e="T92" id="Seg_1287" n="e" s="T91">min </ts>
               <ts e="T93" id="Seg_1289" n="e" s="T92">onu </ts>
               <ts e="T94" id="Seg_1291" n="e" s="T93">dʼe </ts>
               <ts e="T95" id="Seg_1293" n="e" s="T94">körbötögüm. </ts>
            </ts>
            <ts e="T121" id="Seg_1294" n="sc" s="T103">
               <ts e="T104" id="Seg_1296" n="e" s="T103">Körbötögüm </ts>
               <ts e="T105" id="Seg_1298" n="e" s="T104">onno, </ts>
               <ts e="T106" id="Seg_1300" n="e" s="T105">iti </ts>
               <ts e="T107" id="Seg_1302" n="e" s="T106">Tonuskajga </ts>
               <ts e="T108" id="Seg_1304" n="e" s="T107">baːr </ts>
               <ts e="T109" id="Seg_1306" n="e" s="T108">diːller </ts>
               <ts e="T110" id="Seg_1308" n="e" s="T109">ol, </ts>
               <ts e="T111" id="Seg_1310" n="e" s="T110">ol </ts>
               <ts e="T112" id="Seg_1312" n="e" s="T111">bu </ts>
               <ts e="T113" id="Seg_1314" n="e" s="T112">min </ts>
               <ts e="T114" id="Seg_1316" n="e" s="T113">ogonnʼorum </ts>
               <ts e="T115" id="Seg_1318" n="e" s="T114">teːtete </ts>
               <ts e="T116" id="Seg_1320" n="e" s="T115">ke </ts>
               <ts e="T117" id="Seg_1322" n="e" s="T116">ogonnʼor, </ts>
               <ts e="T118" id="Seg_1324" n="e" s="T117">Leŋtej </ts>
               <ts e="T119" id="Seg_1326" n="e" s="T118">Leŋtej </ts>
               <ts e="T120" id="Seg_1328" n="e" s="T119">ogonnʼor </ts>
               <ts e="T121" id="Seg_1330" n="e" s="T120">kɨrdʼagas. </ts>
            </ts>
            <ts e="T164" id="Seg_1331" n="sc" s="T124">
               <ts e="T125" id="Seg_1333" n="e" s="T124">Ontuŋ </ts>
               <ts e="T126" id="Seg_1335" n="e" s="T125">ol </ts>
               <ts e="T127" id="Seg_1337" n="e" s="T126">taːhɨ </ts>
               <ts e="T128" id="Seg_1339" n="e" s="T127">dʼe </ts>
               <ts e="T129" id="Seg_1341" n="e" s="T128">teber </ts>
               <ts e="T130" id="Seg_1343" n="e" s="T129">ebit, </ts>
               <ts e="T131" id="Seg_1345" n="e" s="T130">teppit </ts>
               <ts e="T132" id="Seg_1347" n="e" s="T131">uslučajno. </ts>
               <ts e="T133" id="Seg_1349" n="e" s="T132">Ol </ts>
               <ts e="T134" id="Seg_1351" n="e" s="T133">taːhɨŋ </ts>
               <ts e="T135" id="Seg_1353" n="e" s="T134">bu͡ollagɨna </ts>
               <ts e="T136" id="Seg_1355" n="e" s="T135">balɨk </ts>
               <ts e="T137" id="Seg_1357" n="e" s="T136">gi͡ene </ts>
               <ts e="T138" id="Seg_1359" n="e" s="T137">ebit, </ts>
               <ts e="T139" id="Seg_1361" n="e" s="T138">balɨk </ts>
               <ts e="T140" id="Seg_1363" n="e" s="T139">bi͡erer, </ts>
               <ts e="T141" id="Seg_1365" n="e" s="T140">emi͡e </ts>
               <ts e="T142" id="Seg_1367" n="e" s="T141">kim </ts>
               <ts e="T143" id="Seg_1369" n="e" s="T142">bu͡olla </ts>
               <ts e="T144" id="Seg_1371" n="e" s="T143">iččileːk </ts>
               <ts e="T145" id="Seg_1373" n="e" s="T144">taːs </ts>
               <ts e="T146" id="Seg_1375" n="e" s="T145">ebit. </ts>
               <ts e="T147" id="Seg_1377" n="e" s="T146">Ontuŋ </ts>
               <ts e="T148" id="Seg_1379" n="e" s="T147">hirten </ts>
               <ts e="T149" id="Seg_1381" n="e" s="T148">köstübet </ts>
               <ts e="T150" id="Seg_1383" n="e" s="T149">e </ts>
               <ts e="T151" id="Seg_1385" n="e" s="T150">tak </ts>
               <ts e="T152" id="Seg_1387" n="e" s="T151">ta </ts>
               <ts e="T153" id="Seg_1389" n="e" s="T152">oččukaːn </ts>
               <ts e="T154" id="Seg_1391" n="e" s="T153">ühü, </ts>
               <ts e="T155" id="Seg_1393" n="e" s="T154">tak </ts>
               <ts e="T156" id="Seg_1395" n="e" s="T155">ta </ts>
               <ts e="T157" id="Seg_1397" n="e" s="T156">min </ts>
               <ts e="T158" id="Seg_1399" n="e" s="T157">körbötögüm, </ts>
               <ts e="T159" id="Seg_1401" n="e" s="T158">hiti </ts>
               <ts e="T160" id="Seg_1403" n="e" s="T159">hiti </ts>
               <ts e="T161" id="Seg_1405" n="e" s="T160">kanna </ts>
               <ts e="T162" id="Seg_1407" n="e" s="T161">di͡ebitterej </ts>
               <ts e="T163" id="Seg_1409" n="e" s="T162">ke </ts>
               <ts e="T164" id="Seg_1411" n="e" s="T163">hitinne. </ts>
            </ts>
            <ts e="T181" id="Seg_1412" n="sc" s="T167">
               <ts e="T168" id="Seg_1414" n="e" s="T167">Hitinne </ts>
               <ts e="T169" id="Seg_1416" n="e" s="T168">heː </ts>
               <ts e="T170" id="Seg_1418" n="e" s="T169">hitinne, </ts>
               <ts e="T171" id="Seg_1420" n="e" s="T170">hitinne </ts>
               <ts e="T172" id="Seg_1422" n="e" s="T171">čugas, </ts>
               <ts e="T173" id="Seg_1424" n="e" s="T172">čugas </ts>
               <ts e="T174" id="Seg_1426" n="e" s="T173">bu </ts>
               <ts e="T175" id="Seg_1428" n="e" s="T174">nʼu͡oguhuttar </ts>
               <ts e="T176" id="Seg_1430" n="e" s="T175">oloror </ts>
               <ts e="T177" id="Seg_1432" n="e" s="T176">hirderitten </ts>
               <ts e="T178" id="Seg_1434" n="e" s="T177">bu </ts>
               <ts e="T179" id="Seg_1436" n="e" s="T178">di͡ekkaːn, </ts>
               <ts e="T180" id="Seg_1438" n="e" s="T179">hol, </ts>
               <ts e="T181" id="Seg_1440" n="e" s="T180">baːr. </ts>
            </ts>
            <ts e="T221" id="Seg_1441" n="sc" s="T189">
               <ts e="T190" id="Seg_1443" n="e" s="T189">Dʼe </ts>
               <ts e="T191" id="Seg_1445" n="e" s="T190">onno </ts>
               <ts e="T192" id="Seg_1447" n="e" s="T191">barɨtɨn </ts>
               <ts e="T193" id="Seg_1449" n="e" s="T192">bɨragagɨn </ts>
               <ts e="T194" id="Seg_1451" n="e" s="T193">podarok, </ts>
               <ts e="T195" id="Seg_1453" n="e" s="T194">patron </ts>
               <ts e="T196" id="Seg_1455" n="e" s="T195">bu͡ol, </ts>
               <ts e="T197" id="Seg_1457" n="e" s="T196">timek </ts>
               <ts e="T198" id="Seg_1459" n="e" s="T197">bu͡ol </ts>
               <ts e="T199" id="Seg_1461" n="e" s="T198">barɨtɨn </ts>
               <ts e="T200" id="Seg_1463" n="e" s="T199">žilet </ts>
               <ts e="T201" id="Seg_1465" n="e" s="T200">eŋin-eŋin </ts>
               <ts e="T202" id="Seg_1467" n="e" s="T201">hepter. </ts>
               <ts e="T203" id="Seg_1469" n="e" s="T202">Ol </ts>
               <ts e="T204" id="Seg_1471" n="e" s="T203">ogonnʼoruŋ </ts>
               <ts e="T205" id="Seg_1473" n="e" s="T204">ol </ts>
               <ts e="T206" id="Seg_1475" n="e" s="T205">dʼe </ts>
               <ts e="T207" id="Seg_1477" n="e" s="T206">teppit </ts>
               <ts e="T208" id="Seg_1479" n="e" s="T207">ol </ts>
               <ts e="T209" id="Seg_1481" n="e" s="T208">taːskɨn, </ts>
               <ts e="T210" id="Seg_1483" n="e" s="T209">ol </ts>
               <ts e="T211" id="Seg_1485" n="e" s="T210">kɨrdʼagas </ts>
               <ts e="T212" id="Seg_1487" n="e" s="T211">ogonnʼor </ts>
               <ts e="T213" id="Seg_1489" n="e" s="T212">ke </ts>
               <ts e="T214" id="Seg_1491" n="e" s="T213">haːtar, </ts>
               <ts e="T215" id="Seg_1493" n="e" s="T214">kör </ts>
               <ts e="T216" id="Seg_1495" n="e" s="T215">da </ts>
               <ts e="T217" id="Seg_1497" n="e" s="T216">ontuŋ </ts>
               <ts e="T218" id="Seg_1499" n="e" s="T217">atagɨnan. </ts>
               <ts e="T219" id="Seg_1501" n="e" s="T218">Kohu͡on </ts>
               <ts e="T220" id="Seg_1503" n="e" s="T219">künü </ts>
               <ts e="T221" id="Seg_1505" n="e" s="T220">hɨppɨt. </ts>
            </ts>
            <ts e="T290" id="Seg_1506" n="sc" s="T227">
               <ts e="T231" id="Seg_1508" n="e" s="T227">Heː </ts>
               <ts e="T232" id="Seg_1510" n="e" s="T231">slɨšal </ts>
               <ts e="T233" id="Seg_1512" n="e" s="T232">heː </ts>
               <ts e="T234" id="Seg_1514" n="e" s="T233">bu͡o </ts>
               <ts e="T235" id="Seg_1516" n="e" s="T234">bu͡o </ts>
               <ts e="T236" id="Seg_1518" n="e" s="T235">bu͡o </ts>
               <ts e="T237" id="Seg_1520" n="e" s="T236">itinnik, </ts>
               <ts e="T238" id="Seg_1522" n="e" s="T237">itinnik </ts>
               <ts e="T239" id="Seg_1524" n="e" s="T238">dogo. </ts>
               <ts e="T240" id="Seg_1526" n="e" s="T239">Bu </ts>
               <ts e="T241" id="Seg_1528" n="e" s="T240">hirin </ts>
               <ts e="T242" id="Seg_1530" n="e" s="T241">da, </ts>
               <ts e="T243" id="Seg_1532" n="e" s="T242">iččileːkter </ts>
               <ts e="T244" id="Seg_1534" n="e" s="T243">bu͡o </ts>
               <ts e="T245" id="Seg_1536" n="e" s="T244">bu </ts>
               <ts e="T246" id="Seg_1538" n="e" s="T245">hirderbit, </ts>
               <ts e="T247" id="Seg_1540" n="e" s="T246">kihi </ts>
               <ts e="T248" id="Seg_1542" n="e" s="T247">kuttanɨ͡ak. </ts>
               <ts e="T97" id="Seg_1544" n="e" s="T248">Mmm </ts>
               <ts e="T249" id="Seg_1546" n="e" s="T97">ile </ts>
               <ts e="T250" id="Seg_1548" n="e" s="T249">eŋin-eŋin </ts>
               <ts e="T251" id="Seg_1550" n="e" s="T250">bu͡olaːččɨ </ts>
               <ts e="T252" id="Seg_1552" n="e" s="T251">bu͡olla, </ts>
               <ts e="T253" id="Seg_1554" n="e" s="T252">bihigi </ts>
               <ts e="T254" id="Seg_1556" n="e" s="T253">ol </ts>
               <ts e="T255" id="Seg_1558" n="e" s="T254">bejelerge </ts>
               <ts e="T256" id="Seg_1560" n="e" s="T255">bagas </ts>
               <ts e="T257" id="Seg_1562" n="e" s="T256">ile, </ts>
               <ts e="T258" id="Seg_1564" n="e" s="T257">delʼbi </ts>
               <ts e="T259" id="Seg_1566" n="e" s="T258">belek </ts>
               <ts e="T261" id="Seg_1568" n="e" s="T259">keːspippit. </ts>
               <ts e="T262" id="Seg_1570" n="e" s="T261">Dogo </ts>
               <ts e="T263" id="Seg_1572" n="e" s="T262">interehɨn </ts>
               <ts e="T264" id="Seg_1574" n="e" s="T263">da </ts>
               <ts e="T265" id="Seg_1576" n="e" s="T264">e </ts>
               <ts e="T266" id="Seg_1578" n="e" s="T265">da </ts>
               <ts e="T267" id="Seg_1580" n="e" s="T266">ol </ts>
               <ts e="T268" id="Seg_1582" n="e" s="T267">huruktar </ts>
               <ts e="T269" id="Seg_1584" n="e" s="T268">hɨtallara </ts>
               <ts e="T270" id="Seg_1586" n="e" s="T269">habɨːlaːk </ts>
               <ts e="T271" id="Seg_1588" n="e" s="T270">bu͡o </ts>
               <ts e="T272" id="Seg_1590" n="e" s="T271">ikki </ts>
               <ts e="T273" id="Seg_1592" n="e" s="T272">habɨːlaːk </ts>
               <ts e="T274" id="Seg_1594" n="e" s="T273">ol </ts>
               <ts e="T275" id="Seg_1596" n="e" s="T274">kolbujaŋ. </ts>
               <ts e="T276" id="Seg_1598" n="e" s="T275">Iti </ts>
               <ts e="T277" id="Seg_1600" n="e" s="T276">ke </ts>
               <ts e="T278" id="Seg_1602" n="e" s="T277">urut </ts>
               <ts e="T279" id="Seg_1604" n="e" s="T278">čaːj </ts>
               <ts e="T280" id="Seg_1606" n="e" s="T279">kaːlara </ts>
               <ts e="T281" id="Seg_1608" n="e" s="T280">kanna </ts>
               <ts e="T282" id="Seg_1610" n="e" s="T281">bulaːččɨ </ts>
               <ts e="T283" id="Seg_1612" n="e" s="T282">etilere, </ts>
               <ts e="T284" id="Seg_1614" n="e" s="T283">dʼe </ts>
               <ts e="T285" id="Seg_1616" n="e" s="T284">dʼe </ts>
               <ts e="T286" id="Seg_1618" n="e" s="T285">takie </ts>
               <ts e="T287" id="Seg_1620" n="e" s="T286">korobki </ts>
               <ts e="T288" id="Seg_1622" n="e" s="T287">bɨli </ts>
               <ts e="T289" id="Seg_1624" n="e" s="T288">onton </ts>
               <ts e="T290" id="Seg_1626" n="e" s="T289">ol. </ts>
            </ts>
            <ts e="T361" id="Seg_1627" n="sc" s="T298">
               <ts e="T299" id="Seg_1629" n="e" s="T298">Bugurdukkaːn </ts>
               <ts e="T300" id="Seg_1631" n="e" s="T299">haban </ts>
               <ts e="T303" id="Seg_1633" n="e" s="T300">keːhi͡e </ts>
               <ts e="T304" id="Seg_1635" n="e" s="T303">heː </ts>
               <ts e="T305" id="Seg_1637" n="e" s="T304">bugurdukkaːn. </ts>
               <ts e="T306" id="Seg_1639" n="e" s="T305">Onton </ts>
               <ts e="T307" id="Seg_1641" n="e" s="T306">ikkihin </ts>
               <ts e="T308" id="Seg_1643" n="e" s="T307">habagɨn </ts>
               <ts e="T309" id="Seg_1645" n="e" s="T308">dʼe </ts>
               <ts e="T310" id="Seg_1647" n="e" s="T309">onnuguŋ </ts>
               <ts e="T311" id="Seg_1649" n="e" s="T310">aldʼammat </ts>
               <ts e="T312" id="Seg_1651" n="e" s="T311">bu͡o. </ts>
               <ts e="T313" id="Seg_1653" n="e" s="T312">Koloruk </ts>
               <ts e="T314" id="Seg_1655" n="e" s="T313">kördükter </ts>
               <ts e="T315" id="Seg_1657" n="e" s="T314">bugurduk, </ts>
               <ts e="T316" id="Seg_1659" n="e" s="T315">bugurduk </ts>
               <ts e="T317" id="Seg_1661" n="e" s="T316">ile </ts>
               <ts e="T318" id="Seg_1663" n="e" s="T317">basku͡ojin </ts>
               <ts e="T319" id="Seg_1665" n="e" s="T318">da </ts>
               <ts e="T320" id="Seg_1667" n="e" s="T319">ile </ts>
               <ts e="T321" id="Seg_1669" n="e" s="T320">bulbuta, </ts>
               <ts e="T322" id="Seg_1671" n="e" s="T321">ol </ts>
               <ts e="T323" id="Seg_1673" n="e" s="T322">bu </ts>
               <ts e="T324" id="Seg_1675" n="e" s="T323">taŋaraŋ </ts>
               <ts e="T325" id="Seg_1677" n="e" s="T324">oŋorbut </ts>
               <ts e="T326" id="Seg_1679" n="e" s="T325">ühü </ts>
               <ts e="T327" id="Seg_1681" n="e" s="T326">hir </ts>
               <ts e="T328" id="Seg_1683" n="e" s="T327">turarga. </ts>
               <ts e="T329" id="Seg_1685" n="e" s="T328">Araː </ts>
               <ts e="T330" id="Seg_1687" n="e" s="T329">min </ts>
               <ts e="T331" id="Seg_1689" n="e" s="T330">diːr </ts>
               <ts e="T332" id="Seg_1691" n="e" s="T331">etim </ts>
               <ts e="T333" id="Seg_1693" n="e" s="T332">ara </ts>
               <ts e="T334" id="Seg_1695" n="e" s="T333">ile </ts>
               <ts e="T335" id="Seg_1697" n="e" s="T334">gorod </ts>
               <ts e="T336" id="Seg_1699" n="e" s="T335">basku͡ojin </ts>
               <ts e="T337" id="Seg_1701" n="e" s="T336">daː. </ts>
               <ts e="T338" id="Seg_1703" n="e" s="T337">Tönnöːrübüt </ts>
               <ts e="T339" id="Seg_1705" n="e" s="T338">onno </ts>
               <ts e="T340" id="Seg_1707" n="e" s="T339">tagajbatakpɨt </ts>
               <ts e="T341" id="Seg_1709" n="e" s="T340">tönnöːrübüt, </ts>
               <ts e="T342" id="Seg_1711" n="e" s="T341">baraːrɨbɨt </ts>
               <ts e="T343" id="Seg_1713" n="e" s="T342">agaj </ts>
               <ts e="T344" id="Seg_1715" n="e" s="T343">ol </ts>
               <ts e="T345" id="Seg_1717" n="e" s="T344">barɨta </ts>
               <ts e="T346" id="Seg_1719" n="e" s="T345">podarok </ts>
               <ts e="T347" id="Seg_1721" n="e" s="T346">keːhetteːbippit. </ts>
               <ts e="T348" id="Seg_1723" n="e" s="T347">min </ts>
               <ts e="T349" id="Seg_1725" n="e" s="T348">kimi͡eke </ts>
               <ts e="T350" id="Seg_1727" n="e" s="T349">kepseːbitimij </ts>
               <ts e="T351" id="Seg_1729" n="e" s="T350">ke? </ts>
               <ts e="T352" id="Seg_1731" n="e" s="T351">Eː </ts>
               <ts e="T353" id="Seg_1733" n="e" s="T352">Tatʼjana </ts>
               <ts e="T354" id="Seg_1735" n="e" s="T353">kimi͡eke </ts>
               <ts e="T355" id="Seg_1737" n="e" s="T354">Ivanovnaga </ts>
               <ts e="T356" id="Seg_1739" n="e" s="T355">kepseːbippin </ts>
               <ts e="T357" id="Seg_1741" n="e" s="T356">ebit. </ts>
               <ts e="T358" id="Seg_1743" n="e" s="T357">Tatʼjana </ts>
               <ts e="T359" id="Seg_1745" n="e" s="T358">Ivanovnaga </ts>
               <ts e="T360" id="Seg_1747" n="e" s="T359">oloru </ts>
               <ts e="T361" id="Seg_1749" n="e" s="T360">kepseːbitim. </ts>
            </ts>
            <ts e="T384" id="Seg_1750" n="sc" s="T375">
               <ts e="T376" id="Seg_1752" n="e" s="T375">Onton </ts>
               <ts e="T377" id="Seg_1754" n="e" s="T376">tugu </ts>
               <ts e="T378" id="Seg_1756" n="e" s="T377">ɨjɨtɨ͡ak </ts>
               <ts e="T379" id="Seg_1758" n="e" s="T378">araː </ts>
               <ts e="T384" id="Seg_1760" n="e" s="T379">ontum. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-PoIP">
            <ta e="T17" id="Seg_1761" s="T0">PoIP_ErAI_2009_Life2_nar.PoIP.001 (001.001)</ta>
            <ta e="T35" id="Seg_1762" s="T17">PoIP_ErAI_2009_Life2_nar.PoIP.002 (001.002)</ta>
            <ta e="T380" id="Seg_1763" s="T35">PoIP_ErAI_2009_Life2_nar.PoIP.003 (001.003)</ta>
            <ta e="T57" id="Seg_1764" s="T43">PoIP_ErAI_2009_Life2_nar.PoIP.004 (001.004)</ta>
            <ta e="T73" id="Seg_1765" s="T57">PoIP_ErAI_2009_Life2_nar.PoIP.005 (001.005)</ta>
            <ta e="T95" id="Seg_1766" s="T73">PoIP_ErAI_2009_Life2_nar.PoIP.006 (001.006)</ta>
            <ta e="T121" id="Seg_1767" s="T103">PoIP_ErAI_2009_Life2_nar.PoIP.007 (001.009)</ta>
            <ta e="T132" id="Seg_1768" s="T124">PoIP_ErAI_2009_Life2_nar.PoIP.008 (001.011)</ta>
            <ta e="T146" id="Seg_1769" s="T132">PoIP_ErAI_2009_Life2_nar.PoIP.009 (001.012)</ta>
            <ta e="T164" id="Seg_1770" s="T146">PoIP_ErAI_2009_Life2_nar.PoIP.010 (001.013)</ta>
            <ta e="T181" id="Seg_1771" s="T167">PoIP_ErAI_2009_Life2_nar.PoIP.011 (001.014)</ta>
            <ta e="T202" id="Seg_1772" s="T189">PoIP_ErAI_2009_Life2_nar.PoIP.012 (001.017)</ta>
            <ta e="T218" id="Seg_1773" s="T202">PoIP_ErAI_2009_Life2_nar.PoIP.013 (001.018)</ta>
            <ta e="T221" id="Seg_1774" s="T218">PoIP_ErAI_2009_Life2_nar.PoIP.014 (001.019)</ta>
            <ta e="T239" id="Seg_1775" s="T227">PoIP_ErAI_2009_Life2_nar.PoIP.015 (001.021)</ta>
            <ta e="T248" id="Seg_1776" s="T239">PoIP_ErAI_2009_Life2_nar.PoIP.016 (001.022)</ta>
            <ta e="T261" id="Seg_1777" s="T248">PoIP_ErAI_2009_Life2_nar.PoIP.017 (001.023)</ta>
            <ta e="T275" id="Seg_1778" s="T261">PoIP_ErAI_2009_Life2_nar.PoIP.018 (001.024)</ta>
            <ta e="T290" id="Seg_1779" s="T275">PoIP_ErAI_2009_Life2_nar.PoIP.019 (001.025)</ta>
            <ta e="T305" id="Seg_1780" s="T298">PoIP_ErAI_2009_Life2_nar.PoIP.020 (001.028)</ta>
            <ta e="T312" id="Seg_1781" s="T305">PoIP_ErAI_2009_Life2_nar.PoIP.021 (001.029)</ta>
            <ta e="T328" id="Seg_1782" s="T312">PoIP_ErAI_2009_Life2_nar.PoIP.022 (001.030)</ta>
            <ta e="T337" id="Seg_1783" s="T328">PoIP_ErAI_2009_Life2_nar.PoIP.023 (001.031)</ta>
            <ta e="T347" id="Seg_1784" s="T337">PoIP_ErAI_2009_Life2_nar.PoIP.024 (001.032)</ta>
            <ta e="T351" id="Seg_1785" s="T347">PoIP_ErAI_2009_Life2_nar.PoIP.025 (001.033)</ta>
            <ta e="T357" id="Seg_1786" s="T351">PoIP_ErAI_2009_Life2_nar.PoIP.026 (001.034)</ta>
            <ta e="T361" id="Seg_1787" s="T357">PoIP_ErAI_2009_Life2_nar.PoIP.027 (001.035)</ta>
            <ta e="T384" id="Seg_1788" s="T375">PoIP_ErAI_2009_Life2_nar.PoIP.028 (001.039)</ta>
         </annotation>
         <annotation name="st" tierref="st-PoIP">
            <ta e="T17" id="Seg_1789" s="T0">Һаамайдарыӈ туокатарыӈ, а һамайдарыӈ көстөктөрүнэ буоллагына дьыл айы таба тириитин кэтэрдэллэр эбит ол тааска. һээ ол огонньоргор.</ta>
            <ta e="T35" id="Seg_1790" s="T17">Дьыл айы дьэ ол тириилэрин кэ һытыйан һиргэ, һиргэ нөӈүө дьылын эмиэ көһөн кэлэллэр һаамайдарыӈ опять атын тириини эмиэ.</ta>
            <ta e="T380" id="Seg_1791" s="T35">Табаны өлөрөн баран эмиэ кэтэрдэллэр ти көр да.</ta>
            <ta e="T57" id="Seg_1792" s="T43">Вообщем ол һиргэ ол илэ ч… мин везде һылдьыбыт эмээксиммин бу дойдуга кэлэммин.</ta>
            <ta e="T73" id="Seg_1793" s="T57">Ол үһэ багас кайдиэк һылдьыакпытый үһэ ол диэк бу огонньорбор кэлэммин ити көспүтүм бу огонньорбор аматтан.</ta>
            <ta e="T95" id="Seg_1794" s="T73">Дьэ ол араа дого илэ илэ онтон буоллагына кимэ ити Тонускайга эмиэ баар үһү дого ити ким таас, мин ону дьэ көрбөтөгүм.</ta>
            <ta e="T121" id="Seg_1795" s="T103">Көрбөтөгүм онно ити Тонускайга баар дииллэр ол бу мин огонньорум тээтэтэ кэ огонньор, Лэӈтэй Лэӈтэй огонньор кырдьагас.</ta>
            <ta e="T132" id="Seg_1796" s="T124">Онтуӈ ол тааһы дьэ тэбэр эбит тэппит услучайно.</ta>
            <ta e="T146" id="Seg_1797" s="T132">Ол тааһыӈ буоллагына балык гиэнэ эбит, балык биэрэр, эмиэ ким буолла иччилээк тас эбит.</ta>
            <ta e="T164" id="Seg_1798" s="T146">Онтуӈ һиртэн көстүбэт э так та оччукаан үһү так та мин көрбөтөгүм, һити һити канна диэбиттэрэй кэ һитиннэ.</ta>
            <ta e="T181" id="Seg_1799" s="T167">Б: һитиннэ һээ һитиннэ, һитиннэ чугас, чугас бу Нөӈуһуттар олорор һирдэриттэн бу диэккaан һол баар.</ta>
            <ta e="T202" id="Seg_1800" s="T189">Б: дьэ онно барытын бырагагын подарок, патрон буол, тимэк буол барытын жилет эӈин-эӈин һэптэр.</ta>
            <ta e="T218" id="Seg_1801" s="T202">Ол огонньоруӈ ол дьэ тэппит ол тааскын ол кырдьагас огонньор кэ һаатар көр да онтуӈ атагынан.</ta>
            <ta e="T221" id="Seg_1802" s="T218">Коһуон күнү һыппыт.</ta>
            <ta e="T239" id="Seg_1803" s="T227">Б: һээ слышал һээ буо буо буо итинник, итинник дого.</ta>
            <ta e="T248" id="Seg_1804" s="T239">Бу һирин да иччилээктэр буо бу һирдэрбит киһи куттаныак.</ta>
            <ta e="T261" id="Seg_1805" s="T248">Ммм илэ эӈин-эӈин буолаaччы буолла, биһиги ол бэйэлэргэ багас илэ дэльби бэлэк кээспиппит.</ta>
            <ta e="T275" id="Seg_1806" s="T261">Дого интереһын да э да ол һуруктар һыталлара һабылаак буо икки һабыылак ол колбуйаӈ.</ta>
            <ta e="T290" id="Seg_1807" s="T275">Ити кэ урут чай каалара канна булааччы этилэрэ, дьэ дьэ такие коробки были онтон ол.</ta>
            <ta e="T305" id="Seg_1808" s="T298">Б: бугурдуккаан һабан кэһиэ һээ бубурдуккаан.</ta>
            <ta e="T312" id="Seg_1809" s="T305">Онтон иккиһин һабагын дьэ оннугуӈ алдьаммат буо.</ta>
            <ta e="T328" id="Seg_1810" s="T312">Колорук көрдүктэр бугурдук, бугурдук илэ баскуойин да илэ булбута, ол бу таӈараӈ оӈорбут үһү һир турарга.</ta>
            <ta e="T337" id="Seg_1811" s="T328">Араа мин диир этим ара илэ город баскуойин.</ta>
            <ta e="T347" id="Seg_1812" s="T337">Төннөрүбүт онно тагайбатакпыт төннөрүбүт, барараарыбыт агай ол барыта подарок кэһэттээбиппит.</ta>
            <ta e="T361" id="Seg_1813" s="T357">Б: мин кимиэкэ кэпсэбитимий кэ? ээ Татьяна һ.. кимиэкэ, Ивановнага кэпсээбиппин эбит. Ммм Татьяна Ивановнага олору кэпсээбитим.</ta>
            <ta e="T384" id="Seg_1814" s="T375">Б: онтон тугу ыйтыaк араа онтум.</ta>
         </annotation>
         <annotation name="ts" tierref="ts-PoIP">
            <ta e="T17" id="Seg_1815" s="T0">Haːmajdarɨŋ tu͡oktarɨŋ, a haːmajdarɨŋ köstöktörüne bu͡ollagɨna dʼɨl ajɨ taba tiriːtin keterdeller ebit ol taːska, heː ol ogonnʼorgor. </ta>
            <ta e="T35" id="Seg_1816" s="T17">Dʼɨl ajɨ dʼe ol tiriːler hɨtɨjan hirge, hirge, nöŋü͡ö dʼɨlɨn emi͡e köhön keleller haːmajdarɨŋ opjatʼ atɨn tiriːni emi͡e. </ta>
            <ta e="T380" id="Seg_1817" s="T35">Tabanɨ ölörön baran emi͡e keterdeller ti kör da. </ta>
            <ta e="T57" id="Seg_1818" s="T43">Voːbščem ol hirge ol ile (č-) min vezde hɨldʼɨbɨt emeːksimmin bu dojduga kelemmin. </ta>
            <ta e="T73" id="Seg_1819" s="T57">Ol ühe bagas kajdi͡ek hɨldʼɨ͡akpɨtɨj, ühe ol di͡ek, bu, ogonnʼorbor kelemmin, iti köspütüm, bu ogonnʼorbor, amattan. </ta>
            <ta e="T95" id="Seg_1820" s="T73">Dʼe ol araː dogo ile ile, onton bu͡ollagɨna kime iti Tonuskajga emi͡e baːr ühü dogo iti kim taːs, min onu dʼe körbötögüm. </ta>
            <ta e="T121" id="Seg_1821" s="T103">Körbötögüm onno, iti Tonuskajga baːr diːller ol, ol bu min ogonnʼorum teːtete ke ogonnʼor, Leŋtej Leŋtej ogonnʼor kɨrdʼagas. </ta>
            <ta e="T132" id="Seg_1822" s="T124">Ontuŋ ol taːhɨ dʼe teber ebit, teppit uslučajno. </ta>
            <ta e="T146" id="Seg_1823" s="T132">Ol taːhɨŋ bu͡ollagɨna balɨk gi͡ene ebit, balɨk bi͡erer, emi͡e kim bu͡olla iččileːk taːs ebit. </ta>
            <ta e="T164" id="Seg_1824" s="T146">Ontuŋ hirten köstübet e tak ta oččukaːn ühü, tak ta min körbötögüm, hiti hiti kanna di͡ebitterej ke hitinne. </ta>
            <ta e="T181" id="Seg_1825" s="T167">Hitinne heː hitinne, hitinne čugas, čugas bu nʼu͡oguhuttar oloror hirderitten bu di͡ekkaːn, hol, baːr. </ta>
            <ta e="T202" id="Seg_1826" s="T189">Dʼe onno barɨtɨn bɨragagɨn podarok, patron bu͡ol, timek bu͡ol barɨtɨn žilet eŋin-eŋin hepter. </ta>
            <ta e="T218" id="Seg_1827" s="T202">Ol ogonnʼoruŋ ol dʼe teppit ol taːskɨn, ol kɨrdʼagas ogonnʼor ke haːtar, kör da ontuŋ atagɨnan. </ta>
            <ta e="T221" id="Seg_1828" s="T218">Kohu͡on künü hɨppɨt. </ta>
            <ta e="T239" id="Seg_1829" s="T227">Heː slɨšal heː bu͡o bu͡o bu͡o itinnik, itinnik dogo. </ta>
            <ta e="T248" id="Seg_1830" s="T239">Bu hirin da, iččileːkter bu͡o bu hirderbit, kihi kuttanɨ͡ak. </ta>
            <ta e="T261" id="Seg_1831" s="T248">Mmm ile eŋin-eŋin bu͡olaːččɨ bu͡olla, bihigi ol bejelerge bagas ile, delʼbi belek keːspippit. </ta>
            <ta e="T275" id="Seg_1832" s="T261">Dogo interehɨn da e da ol huruktar hɨtallara habɨːlaːk bu͡o ikki habɨːlaːk ol kolbujaŋ. </ta>
            <ta e="T290" id="Seg_1833" s="T275">Iti ke urut čaːj kaːlara kanna bulaːččɨ etilere, dʼe dʼe takie korobki bɨli onton ol. </ta>
            <ta e="T305" id="Seg_1834" s="T298">Bugurdukkaːn haban keːhi͡e heː bugurdukkaːn. </ta>
            <ta e="T312" id="Seg_1835" s="T305">Onton ikkihin habagɨn dʼe onnuguŋ aldʼammat bu͡o. </ta>
            <ta e="T328" id="Seg_1836" s="T312">Koloruk kördükter bugurduk, bugurduk ile basku͡ojin da ile bulbuta, ol bu taŋaraŋ oŋorbut ühü hir turarga. </ta>
            <ta e="T337" id="Seg_1837" s="T328">Araː min diːr etim ara ile gorod basku͡ojin daː. </ta>
            <ta e="T347" id="Seg_1838" s="T337">Tönnöːrübüt onno tagajbatakpɨt tönnöːrübüt, baraːrɨbɨt agaj ol barɨta podarok keːhetteːbippit. </ta>
            <ta e="T351" id="Seg_1839" s="T347">Min kimi͡eke kepseːbitimij ke? </ta>
            <ta e="T357" id="Seg_1840" s="T351">Eː Tatʼjana kimi͡eke Ivanovnaga kepseːbippin ebit. </ta>
            <ta e="T361" id="Seg_1841" s="T357">Tatʼjana Ivanovnaga oloru kepseːbitim. </ta>
            <ta e="T384" id="Seg_1842" s="T375">Onton tugu ɨjɨtɨ͡ak araː ontum. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-PoIP">
            <ta e="T1" id="Seg_1843" s="T0">haːmaj-dar-ɨ-ŋ</ta>
            <ta e="T2" id="Seg_1844" s="T1">tu͡ok-tar-ɨ-ŋ</ta>
            <ta e="T3" id="Seg_1845" s="T2">a</ta>
            <ta e="T4" id="Seg_1846" s="T3">haːmaj-dar-ɨ-ŋ</ta>
            <ta e="T5" id="Seg_1847" s="T4">kös-tök-törüne</ta>
            <ta e="T6" id="Seg_1848" s="T5">bu͡ollagɨna</ta>
            <ta e="T7" id="Seg_1849" s="T6">dʼɨl</ta>
            <ta e="T8" id="Seg_1850" s="T7">ajɨ</ta>
            <ta e="T9" id="Seg_1851" s="T8">taba</ta>
            <ta e="T10" id="Seg_1852" s="T9">tiriː-ti-n</ta>
            <ta e="T11" id="Seg_1853" s="T10">ket-er-d-el-ler</ta>
            <ta e="T12" id="Seg_1854" s="T11">e-bit</ta>
            <ta e="T13" id="Seg_1855" s="T12">ol</ta>
            <ta e="T14" id="Seg_1856" s="T13">taːs-ka</ta>
            <ta e="T15" id="Seg_1857" s="T14">heː</ta>
            <ta e="T16" id="Seg_1858" s="T15">ol</ta>
            <ta e="T17" id="Seg_1859" s="T16">ogonnʼor-gor</ta>
            <ta e="T18" id="Seg_1860" s="T17">dʼɨl</ta>
            <ta e="T19" id="Seg_1861" s="T18">ajɨ</ta>
            <ta e="T20" id="Seg_1862" s="T19">dʼe</ta>
            <ta e="T21" id="Seg_1863" s="T20">ol</ta>
            <ta e="T22" id="Seg_1864" s="T21">tiriː-ler</ta>
            <ta e="T23" id="Seg_1865" s="T22">hɨtɨj-an</ta>
            <ta e="T24" id="Seg_1866" s="T23">hir-ge</ta>
            <ta e="T25" id="Seg_1867" s="T24">hir-ge</ta>
            <ta e="T26" id="Seg_1868" s="T25">nöŋü͡ö</ta>
            <ta e="T27" id="Seg_1869" s="T26">dʼɨl-ɨ-n</ta>
            <ta e="T28" id="Seg_1870" s="T27">emi͡e</ta>
            <ta e="T29" id="Seg_1871" s="T28">köh-ön</ta>
            <ta e="T30" id="Seg_1872" s="T29">kel-el-ler</ta>
            <ta e="T31" id="Seg_1873" s="T30">haːmaj-dar-ɨ-ŋ</ta>
            <ta e="T33" id="Seg_1874" s="T32">atɨn</ta>
            <ta e="T34" id="Seg_1875" s="T33">tiriː-ni</ta>
            <ta e="T35" id="Seg_1876" s="T34">emi͡e</ta>
            <ta e="T36" id="Seg_1877" s="T35">taba-nɨ</ta>
            <ta e="T37" id="Seg_1878" s="T36">ölör-ön</ta>
            <ta e="T38" id="Seg_1879" s="T37">bar-an</ta>
            <ta e="T39" id="Seg_1880" s="T38">emi͡e</ta>
            <ta e="T40" id="Seg_1881" s="T39">ket-er-d-el-ler</ta>
            <ta e="T41" id="Seg_1882" s="T40">ti</ta>
            <ta e="T42" id="Seg_1883" s="T41">kör</ta>
            <ta e="T380" id="Seg_1884" s="T42">da</ta>
            <ta e="T44" id="Seg_1885" s="T43">voːbščem</ta>
            <ta e="T45" id="Seg_1886" s="T44">ol</ta>
            <ta e="T46" id="Seg_1887" s="T45">hir-ge</ta>
            <ta e="T47" id="Seg_1888" s="T46">ol</ta>
            <ta e="T48" id="Seg_1889" s="T47">ile</ta>
            <ta e="T51" id="Seg_1890" s="T50">min</ta>
            <ta e="T53" id="Seg_1891" s="T52">hɨldʼ-ɨ-bɨt</ta>
            <ta e="T54" id="Seg_1892" s="T53">emeːksim-min</ta>
            <ta e="T55" id="Seg_1893" s="T54">bu</ta>
            <ta e="T56" id="Seg_1894" s="T55">dojdu-ga</ta>
            <ta e="T57" id="Seg_1895" s="T56">kel-em-min</ta>
            <ta e="T58" id="Seg_1896" s="T57">ol</ta>
            <ta e="T59" id="Seg_1897" s="T58">ühe</ta>
            <ta e="T60" id="Seg_1898" s="T59">bagas</ta>
            <ta e="T61" id="Seg_1899" s="T60">kajdi͡ek</ta>
            <ta e="T62" id="Seg_1900" s="T61">hɨldʼ-ɨ͡ak-pɨt=ɨj</ta>
            <ta e="T63" id="Seg_1901" s="T62">ühe</ta>
            <ta e="T64" id="Seg_1902" s="T63">ol</ta>
            <ta e="T65" id="Seg_1903" s="T64">di͡ek</ta>
            <ta e="T66" id="Seg_1904" s="T65">bu</ta>
            <ta e="T67" id="Seg_1905" s="T66">ogonnʼor-bo-r</ta>
            <ta e="T68" id="Seg_1906" s="T67">kel-em-min</ta>
            <ta e="T69" id="Seg_1907" s="T68">iti</ta>
            <ta e="T70" id="Seg_1908" s="T69">kös-püt-ü-m</ta>
            <ta e="T71" id="Seg_1909" s="T70">bu</ta>
            <ta e="T72" id="Seg_1910" s="T71">ogonnʼor-bo-r</ta>
            <ta e="T73" id="Seg_1911" s="T72">amattan</ta>
            <ta e="T74" id="Seg_1912" s="T73">dʼe</ta>
            <ta e="T75" id="Seg_1913" s="T74">ol</ta>
            <ta e="T76" id="Seg_1914" s="T75">araː</ta>
            <ta e="T77" id="Seg_1915" s="T76">dogo</ta>
            <ta e="T78" id="Seg_1916" s="T77">ile</ta>
            <ta e="T79" id="Seg_1917" s="T78">ile</ta>
            <ta e="T80" id="Seg_1918" s="T79">onton</ta>
            <ta e="T81" id="Seg_1919" s="T80">bu͡ollagɨna</ta>
            <ta e="T82" id="Seg_1920" s="T81">kim-e</ta>
            <ta e="T83" id="Seg_1921" s="T82">iti</ta>
            <ta e="T84" id="Seg_1922" s="T83">Tonuskaj-ga</ta>
            <ta e="T85" id="Seg_1923" s="T84">emi͡e</ta>
            <ta e="T86" id="Seg_1924" s="T85">baːr</ta>
            <ta e="T87" id="Seg_1925" s="T86">ühü</ta>
            <ta e="T88" id="Seg_1926" s="T87">dogo</ta>
            <ta e="T89" id="Seg_1927" s="T88">iti</ta>
            <ta e="T90" id="Seg_1928" s="T89">kim</ta>
            <ta e="T91" id="Seg_1929" s="T90">taːs</ta>
            <ta e="T92" id="Seg_1930" s="T91">min</ta>
            <ta e="T93" id="Seg_1931" s="T92">o-nu</ta>
            <ta e="T94" id="Seg_1932" s="T93">dʼe</ta>
            <ta e="T95" id="Seg_1933" s="T94">kör-bötög-ü-m</ta>
            <ta e="T104" id="Seg_1934" s="T103">kör-bötög-ü-m</ta>
            <ta e="T105" id="Seg_1935" s="T104">onno</ta>
            <ta e="T106" id="Seg_1936" s="T105">iti</ta>
            <ta e="T107" id="Seg_1937" s="T106">Tonuskaj-ga</ta>
            <ta e="T108" id="Seg_1938" s="T107">baːr</ta>
            <ta e="T109" id="Seg_1939" s="T108">diː-l-ler</ta>
            <ta e="T110" id="Seg_1940" s="T109">ol</ta>
            <ta e="T111" id="Seg_1941" s="T110">ol</ta>
            <ta e="T112" id="Seg_1942" s="T111">bu</ta>
            <ta e="T113" id="Seg_1943" s="T112">min</ta>
            <ta e="T114" id="Seg_1944" s="T113">ogonnʼor-u-m</ta>
            <ta e="T115" id="Seg_1945" s="T114">teːte-te</ta>
            <ta e="T116" id="Seg_1946" s="T115">ke</ta>
            <ta e="T117" id="Seg_1947" s="T116">ogonnʼor</ta>
            <ta e="T118" id="Seg_1948" s="T117">Leŋtej</ta>
            <ta e="T119" id="Seg_1949" s="T118">Leŋtej</ta>
            <ta e="T120" id="Seg_1950" s="T119">ogonnʼor</ta>
            <ta e="T121" id="Seg_1951" s="T120">kɨrdʼagas</ta>
            <ta e="T125" id="Seg_1952" s="T124">on-tu-ŋ</ta>
            <ta e="T126" id="Seg_1953" s="T125">ol</ta>
            <ta e="T127" id="Seg_1954" s="T126">taːh-ɨ</ta>
            <ta e="T128" id="Seg_1955" s="T127">dʼe</ta>
            <ta e="T129" id="Seg_1956" s="T128">teb-er</ta>
            <ta e="T130" id="Seg_1957" s="T129">e-bit</ta>
            <ta e="T131" id="Seg_1958" s="T130">tep-pit</ta>
            <ta e="T132" id="Seg_1959" s="T131">uslučajno</ta>
            <ta e="T133" id="Seg_1960" s="T132">ol</ta>
            <ta e="T134" id="Seg_1961" s="T133">taːh-ɨ-ŋ</ta>
            <ta e="T135" id="Seg_1962" s="T134">bu͡ollagɨna</ta>
            <ta e="T136" id="Seg_1963" s="T135">balɨk</ta>
            <ta e="T137" id="Seg_1964" s="T136">gi͡en-e</ta>
            <ta e="T138" id="Seg_1965" s="T137">e-bit</ta>
            <ta e="T139" id="Seg_1966" s="T138">balɨk</ta>
            <ta e="T140" id="Seg_1967" s="T139">bi͡er-er</ta>
            <ta e="T141" id="Seg_1968" s="T140">emi͡e</ta>
            <ta e="T142" id="Seg_1969" s="T141">kim</ta>
            <ta e="T143" id="Seg_1970" s="T142">bu͡ol-l-a</ta>
            <ta e="T144" id="Seg_1971" s="T143">ičči-leːk</ta>
            <ta e="T145" id="Seg_1972" s="T144">taːs</ta>
            <ta e="T146" id="Seg_1973" s="T145">e-bit</ta>
            <ta e="T147" id="Seg_1974" s="T146">on-tu-ŋ</ta>
            <ta e="T148" id="Seg_1975" s="T147">hir-ten</ta>
            <ta e="T149" id="Seg_1976" s="T148">köst-ü-bet</ta>
            <ta e="T150" id="Seg_1977" s="T149">e</ta>
            <ta e="T151" id="Seg_1978" s="T150">tak</ta>
            <ta e="T152" id="Seg_1979" s="T151">ta</ta>
            <ta e="T153" id="Seg_1980" s="T152">oččukaːn</ta>
            <ta e="T154" id="Seg_1981" s="T153">ühü</ta>
            <ta e="T155" id="Seg_1982" s="T154">tak</ta>
            <ta e="T156" id="Seg_1983" s="T155">ta</ta>
            <ta e="T157" id="Seg_1984" s="T156">min</ta>
            <ta e="T158" id="Seg_1985" s="T157">kör-bötög-ü-m</ta>
            <ta e="T159" id="Seg_1986" s="T158">hiti</ta>
            <ta e="T160" id="Seg_1987" s="T159">hiti</ta>
            <ta e="T161" id="Seg_1988" s="T160">kanna</ta>
            <ta e="T162" id="Seg_1989" s="T161">di͡e-bit-tere=j</ta>
            <ta e="T163" id="Seg_1990" s="T162">ke</ta>
            <ta e="T164" id="Seg_1991" s="T163">h-itinne</ta>
            <ta e="T168" id="Seg_1992" s="T167">h-itinne</ta>
            <ta e="T169" id="Seg_1993" s="T168">heː</ta>
            <ta e="T170" id="Seg_1994" s="T169">h-itinne</ta>
            <ta e="T171" id="Seg_1995" s="T170">h-itinne</ta>
            <ta e="T172" id="Seg_1996" s="T171">čugas</ta>
            <ta e="T173" id="Seg_1997" s="T172">čugas</ta>
            <ta e="T174" id="Seg_1998" s="T173">bu</ta>
            <ta e="T175" id="Seg_1999" s="T174">nʼu͡oguhut-tar</ta>
            <ta e="T176" id="Seg_2000" s="T175">olor-or</ta>
            <ta e="T177" id="Seg_2001" s="T176">hir-der-i-tten</ta>
            <ta e="T178" id="Seg_2002" s="T177">bu</ta>
            <ta e="T179" id="Seg_2003" s="T178">di͡ek-kaːn</ta>
            <ta e="T180" id="Seg_2004" s="T179">hol</ta>
            <ta e="T181" id="Seg_2005" s="T180">baːr</ta>
            <ta e="T190" id="Seg_2006" s="T189">dʼe</ta>
            <ta e="T191" id="Seg_2007" s="T190">onno</ta>
            <ta e="T192" id="Seg_2008" s="T191">barɨ-tɨ-n</ta>
            <ta e="T193" id="Seg_2009" s="T192">bɨrag-a-gɨn</ta>
            <ta e="T194" id="Seg_2010" s="T193">podarok</ta>
            <ta e="T195" id="Seg_2011" s="T194">patron</ta>
            <ta e="T196" id="Seg_2012" s="T195">bu͡ol</ta>
            <ta e="T197" id="Seg_2013" s="T196">timek</ta>
            <ta e="T198" id="Seg_2014" s="T197">bu͡ol</ta>
            <ta e="T199" id="Seg_2015" s="T198">barɨ-tɨ-n</ta>
            <ta e="T201" id="Seg_2016" s="T200">eŋin-eŋin</ta>
            <ta e="T202" id="Seg_2017" s="T201">hep-ter</ta>
            <ta e="T203" id="Seg_2018" s="T202">ol</ta>
            <ta e="T204" id="Seg_2019" s="T203">ogonnʼor-u-ŋ</ta>
            <ta e="T205" id="Seg_2020" s="T204">ol</ta>
            <ta e="T206" id="Seg_2021" s="T205">dʼe</ta>
            <ta e="T207" id="Seg_2022" s="T206">tep-pit</ta>
            <ta e="T208" id="Seg_2023" s="T207">ol</ta>
            <ta e="T209" id="Seg_2024" s="T208">taːs-kɨ-n</ta>
            <ta e="T210" id="Seg_2025" s="T209">ol</ta>
            <ta e="T211" id="Seg_2026" s="T210">kɨrdʼagas</ta>
            <ta e="T212" id="Seg_2027" s="T211">ogonnʼor</ta>
            <ta e="T213" id="Seg_2028" s="T212">ke</ta>
            <ta e="T214" id="Seg_2029" s="T213">haːtar</ta>
            <ta e="T215" id="Seg_2030" s="T214">kör</ta>
            <ta e="T216" id="Seg_2031" s="T215">da</ta>
            <ta e="T217" id="Seg_2032" s="T216">on-tu-ŋ</ta>
            <ta e="T218" id="Seg_2033" s="T217">atag-ɨ-nan</ta>
            <ta e="T219" id="Seg_2034" s="T218">kohu͡on</ta>
            <ta e="T220" id="Seg_2035" s="T219">kün-ü</ta>
            <ta e="T221" id="Seg_2036" s="T220">hɨp-pɨt</ta>
            <ta e="T231" id="Seg_2037" s="T227">heː</ta>
            <ta e="T233" id="Seg_2038" s="T232">heː</ta>
            <ta e="T234" id="Seg_2039" s="T233">bu͡o</ta>
            <ta e="T235" id="Seg_2040" s="T234">bu͡o</ta>
            <ta e="T236" id="Seg_2041" s="T235">bu͡o</ta>
            <ta e="T237" id="Seg_2042" s="T236">itinnik</ta>
            <ta e="T238" id="Seg_2043" s="T237">itinnik</ta>
            <ta e="T239" id="Seg_2044" s="T238">dogo</ta>
            <ta e="T240" id="Seg_2045" s="T239">bu</ta>
            <ta e="T241" id="Seg_2046" s="T240">hir-i-n</ta>
            <ta e="T242" id="Seg_2047" s="T241">da</ta>
            <ta e="T243" id="Seg_2048" s="T242">ičči-leːk-ter</ta>
            <ta e="T244" id="Seg_2049" s="T243">bu͡o</ta>
            <ta e="T245" id="Seg_2050" s="T244">bu</ta>
            <ta e="T246" id="Seg_2051" s="T245">hir-der-bit</ta>
            <ta e="T247" id="Seg_2052" s="T246">kihi</ta>
            <ta e="T248" id="Seg_2053" s="T247">kuttan-ɨ͡ak</ta>
            <ta e="T97" id="Seg_2054" s="T248">mmm</ta>
            <ta e="T249" id="Seg_2055" s="T97">ile</ta>
            <ta e="T250" id="Seg_2056" s="T249">eŋin-eŋin</ta>
            <ta e="T251" id="Seg_2057" s="T250">bu͡ol-aːččɨ</ta>
            <ta e="T252" id="Seg_2058" s="T251">bu͡ol-l-a</ta>
            <ta e="T253" id="Seg_2059" s="T252">bihigi</ta>
            <ta e="T254" id="Seg_2060" s="T253">ol</ta>
            <ta e="T255" id="Seg_2061" s="T254">beje-ler-ge</ta>
            <ta e="T256" id="Seg_2062" s="T255">bagas</ta>
            <ta e="T257" id="Seg_2063" s="T256">ile</ta>
            <ta e="T258" id="Seg_2064" s="T257">delʼbi</ta>
            <ta e="T259" id="Seg_2065" s="T258">belek</ta>
            <ta e="T261" id="Seg_2066" s="T259">keːs-pip-pit</ta>
            <ta e="T262" id="Seg_2067" s="T261">dogo</ta>
            <ta e="T263" id="Seg_2068" s="T262">interehɨn</ta>
            <ta e="T264" id="Seg_2069" s="T263">da</ta>
            <ta e="T265" id="Seg_2070" s="T264">e</ta>
            <ta e="T266" id="Seg_2071" s="T265">da</ta>
            <ta e="T267" id="Seg_2072" s="T266">ol</ta>
            <ta e="T268" id="Seg_2073" s="T267">huruk-tar</ta>
            <ta e="T269" id="Seg_2074" s="T268">hɨt-al-lara</ta>
            <ta e="T270" id="Seg_2075" s="T269">habɨː-laːk</ta>
            <ta e="T271" id="Seg_2076" s="T270">bu͡o</ta>
            <ta e="T272" id="Seg_2077" s="T271">ikki</ta>
            <ta e="T273" id="Seg_2078" s="T272">habɨː-laːk</ta>
            <ta e="T274" id="Seg_2079" s="T273">ol</ta>
            <ta e="T275" id="Seg_2080" s="T274">kolbuja-ŋ</ta>
            <ta e="T276" id="Seg_2081" s="T275">iti</ta>
            <ta e="T277" id="Seg_2082" s="T276">ke</ta>
            <ta e="T278" id="Seg_2083" s="T277">urut</ta>
            <ta e="T279" id="Seg_2084" s="T278">čaːj</ta>
            <ta e="T280" id="Seg_2085" s="T279">kaː-lar-a</ta>
            <ta e="T281" id="Seg_2086" s="T280">kanna</ta>
            <ta e="T282" id="Seg_2087" s="T281">bul-aːččɨ</ta>
            <ta e="T283" id="Seg_2088" s="T282">e-ti-lere</ta>
            <ta e="T284" id="Seg_2089" s="T283">dʼe</ta>
            <ta e="T285" id="Seg_2090" s="T284">dʼe</ta>
            <ta e="T289" id="Seg_2091" s="T288">onton</ta>
            <ta e="T290" id="Seg_2092" s="T289">ol</ta>
            <ta e="T299" id="Seg_2093" s="T298">bugurduk-kaːn</ta>
            <ta e="T300" id="Seg_2094" s="T299">hab-an</ta>
            <ta e="T303" id="Seg_2095" s="T300">keːh-i͡e</ta>
            <ta e="T304" id="Seg_2096" s="T303">heː</ta>
            <ta e="T305" id="Seg_2097" s="T304">bugurduk-kaːn</ta>
            <ta e="T306" id="Seg_2098" s="T305">onton</ta>
            <ta e="T307" id="Seg_2099" s="T306">ikki-h-i-n</ta>
            <ta e="T308" id="Seg_2100" s="T307">hab-a-gɨn</ta>
            <ta e="T309" id="Seg_2101" s="T308">dʼe</ta>
            <ta e="T310" id="Seg_2102" s="T309">onnug-u-ŋ</ta>
            <ta e="T311" id="Seg_2103" s="T310">aldʼam-mat</ta>
            <ta e="T312" id="Seg_2104" s="T311">bu͡o</ta>
            <ta e="T313" id="Seg_2105" s="T312">koloruk</ta>
            <ta e="T314" id="Seg_2106" s="T313">kördük-ter</ta>
            <ta e="T315" id="Seg_2107" s="T314">bugurduk</ta>
            <ta e="T316" id="Seg_2108" s="T315">bugurduk</ta>
            <ta e="T317" id="Seg_2109" s="T316">ile</ta>
            <ta e="T318" id="Seg_2110" s="T317">basku͡oj-in</ta>
            <ta e="T319" id="Seg_2111" s="T318">da</ta>
            <ta e="T320" id="Seg_2112" s="T319">ile</ta>
            <ta e="T321" id="Seg_2113" s="T320">bul-but-a</ta>
            <ta e="T322" id="Seg_2114" s="T321">ol</ta>
            <ta e="T323" id="Seg_2115" s="T322">bu</ta>
            <ta e="T324" id="Seg_2116" s="T323">taŋara-ŋ</ta>
            <ta e="T325" id="Seg_2117" s="T324">oŋor-but</ta>
            <ta e="T326" id="Seg_2118" s="T325">ühü</ta>
            <ta e="T327" id="Seg_2119" s="T326">hir</ta>
            <ta e="T328" id="Seg_2120" s="T327">tur-ar-ga</ta>
            <ta e="T329" id="Seg_2121" s="T328">araː</ta>
            <ta e="T330" id="Seg_2122" s="T329">min</ta>
            <ta e="T331" id="Seg_2123" s="T330">diː-r</ta>
            <ta e="T332" id="Seg_2124" s="T331">e-ti-m</ta>
            <ta e="T333" id="Seg_2125" s="T332">ara</ta>
            <ta e="T334" id="Seg_2126" s="T333">ile</ta>
            <ta e="T335" id="Seg_2127" s="T334">gorod</ta>
            <ta e="T336" id="Seg_2128" s="T335">basku͡oj-in</ta>
            <ta e="T337" id="Seg_2129" s="T336">daː</ta>
            <ta e="T338" id="Seg_2130" s="T337">tönn-öːrü-büt</ta>
            <ta e="T339" id="Seg_2131" s="T338">onno</ta>
            <ta e="T340" id="Seg_2132" s="T339">tagaj-batak-pɨt</ta>
            <ta e="T341" id="Seg_2133" s="T340">tönn-öːrü-büt</ta>
            <ta e="T342" id="Seg_2134" s="T341">bar-aːrɨ-bɨt</ta>
            <ta e="T343" id="Seg_2135" s="T342">agaj</ta>
            <ta e="T344" id="Seg_2136" s="T343">ol</ta>
            <ta e="T345" id="Seg_2137" s="T344">barɨ-ta</ta>
            <ta e="T346" id="Seg_2138" s="T345">podarok</ta>
            <ta e="T347" id="Seg_2139" s="T346">keːh-etteː-bip-pit</ta>
            <ta e="T348" id="Seg_2140" s="T347">min</ta>
            <ta e="T349" id="Seg_2141" s="T348">kimi͡e-ke</ta>
            <ta e="T350" id="Seg_2142" s="T349">kepseː-bit-i-m=ij</ta>
            <ta e="T351" id="Seg_2143" s="T350">ke</ta>
            <ta e="T352" id="Seg_2144" s="T351">eː</ta>
            <ta e="T353" id="Seg_2145" s="T352">Tatʼjana</ta>
            <ta e="T354" id="Seg_2146" s="T353">kimi͡e-ke</ta>
            <ta e="T355" id="Seg_2147" s="T354">Ivanovna-ga</ta>
            <ta e="T356" id="Seg_2148" s="T355">kepseː-bip-pin</ta>
            <ta e="T357" id="Seg_2149" s="T356">e-bit</ta>
            <ta e="T358" id="Seg_2150" s="T357">Tatʼjana</ta>
            <ta e="T359" id="Seg_2151" s="T358">Ivanovna-ga</ta>
            <ta e="T360" id="Seg_2152" s="T359">o-lor-u</ta>
            <ta e="T361" id="Seg_2153" s="T360">kepseː-bit-i-m</ta>
            <ta e="T376" id="Seg_2154" s="T375">onton</ta>
            <ta e="T377" id="Seg_2155" s="T376">tug-u</ta>
            <ta e="T378" id="Seg_2156" s="T377">ɨjɨt-ɨ͡ak</ta>
            <ta e="T379" id="Seg_2157" s="T378">araː</ta>
            <ta e="T384" id="Seg_2158" s="T379">on-tu-m</ta>
         </annotation>
         <annotation name="mp" tierref="mp-PoIP">
            <ta e="T1" id="Seg_2159" s="T0">haːmaj-LAr-I-ŋ</ta>
            <ta e="T2" id="Seg_2160" s="T1">tu͡ok-LAr-I-ŋ</ta>
            <ta e="T3" id="Seg_2161" s="T2">a</ta>
            <ta e="T4" id="Seg_2162" s="T3">haːmaj-LAr-I-ŋ</ta>
            <ta e="T5" id="Seg_2163" s="T4">kös-TAK-TArInA</ta>
            <ta e="T6" id="Seg_2164" s="T5">bu͡ollagɨna</ta>
            <ta e="T7" id="Seg_2165" s="T6">dʼɨl</ta>
            <ta e="T8" id="Seg_2166" s="T7">aːjɨ</ta>
            <ta e="T9" id="Seg_2167" s="T8">taba</ta>
            <ta e="T10" id="Seg_2168" s="T9">tiriː-tI-n</ta>
            <ta e="T11" id="Seg_2169" s="T10">ket-IAr-t-Ar-LAr</ta>
            <ta e="T12" id="Seg_2170" s="T11">e-BIT</ta>
            <ta e="T13" id="Seg_2171" s="T12">ol</ta>
            <ta e="T14" id="Seg_2172" s="T13">taːs-GA</ta>
            <ta e="T15" id="Seg_2173" s="T14">eː</ta>
            <ta e="T16" id="Seg_2174" s="T15">ol</ta>
            <ta e="T17" id="Seg_2175" s="T16">ogonnʼor-GAr</ta>
            <ta e="T18" id="Seg_2176" s="T17">dʼɨl</ta>
            <ta e="T19" id="Seg_2177" s="T18">aːjɨ</ta>
            <ta e="T20" id="Seg_2178" s="T19">dʼe</ta>
            <ta e="T21" id="Seg_2179" s="T20">ol</ta>
            <ta e="T22" id="Seg_2180" s="T21">tiriː-LAr</ta>
            <ta e="T23" id="Seg_2181" s="T22">hɨtɨj-An</ta>
            <ta e="T24" id="Seg_2182" s="T23">hir-GA</ta>
            <ta e="T25" id="Seg_2183" s="T24">hir-GA</ta>
            <ta e="T26" id="Seg_2184" s="T25">nöŋü͡ö</ta>
            <ta e="T27" id="Seg_2185" s="T26">dʼɨl-tI-n</ta>
            <ta e="T28" id="Seg_2186" s="T27">emi͡e</ta>
            <ta e="T29" id="Seg_2187" s="T28">kös-An</ta>
            <ta e="T30" id="Seg_2188" s="T29">kel-Ar-LAr</ta>
            <ta e="T31" id="Seg_2189" s="T30">haːmaj-LAr-I-ŋ</ta>
            <ta e="T33" id="Seg_2190" s="T32">atɨn</ta>
            <ta e="T34" id="Seg_2191" s="T33">tiriː-nI</ta>
            <ta e="T35" id="Seg_2192" s="T34">emi͡e</ta>
            <ta e="T36" id="Seg_2193" s="T35">taba-nI</ta>
            <ta e="T37" id="Seg_2194" s="T36">ölör-An</ta>
            <ta e="T38" id="Seg_2195" s="T37">bar-An</ta>
            <ta e="T39" id="Seg_2196" s="T38">emi͡e</ta>
            <ta e="T40" id="Seg_2197" s="T39">ket-IAr-t-Ar-LAr</ta>
            <ta e="T41" id="Seg_2198" s="T40">iti</ta>
            <ta e="T42" id="Seg_2199" s="T41">kör</ta>
            <ta e="T380" id="Seg_2200" s="T42">da</ta>
            <ta e="T44" id="Seg_2201" s="T43">vobšʼem</ta>
            <ta e="T45" id="Seg_2202" s="T44">ol</ta>
            <ta e="T46" id="Seg_2203" s="T45">hir-GA</ta>
            <ta e="T47" id="Seg_2204" s="T46">ol</ta>
            <ta e="T48" id="Seg_2205" s="T47">ile</ta>
            <ta e="T51" id="Seg_2206" s="T50">min</ta>
            <ta e="T53" id="Seg_2207" s="T52">hɨrɨt-I-BIT</ta>
            <ta e="T54" id="Seg_2208" s="T53">emeːksin-BIn</ta>
            <ta e="T55" id="Seg_2209" s="T54">bu</ta>
            <ta e="T56" id="Seg_2210" s="T55">dojdu-GA</ta>
            <ta e="T57" id="Seg_2211" s="T56">kel-An-BIn</ta>
            <ta e="T58" id="Seg_2212" s="T57">ol</ta>
            <ta e="T59" id="Seg_2213" s="T58">ü͡öhe</ta>
            <ta e="T60" id="Seg_2214" s="T59">bagas</ta>
            <ta e="T61" id="Seg_2215" s="T60">kajdi͡ek</ta>
            <ta e="T62" id="Seg_2216" s="T61">hɨrɨt-IAK-BIt=Ij</ta>
            <ta e="T63" id="Seg_2217" s="T62">ü͡öhe</ta>
            <ta e="T64" id="Seg_2218" s="T63">ol</ta>
            <ta e="T65" id="Seg_2219" s="T64">dek</ta>
            <ta e="T66" id="Seg_2220" s="T65">bu</ta>
            <ta e="T67" id="Seg_2221" s="T66">ogonnʼor-BA-r</ta>
            <ta e="T68" id="Seg_2222" s="T67">kel-An-BIn</ta>
            <ta e="T69" id="Seg_2223" s="T68">iti</ta>
            <ta e="T70" id="Seg_2224" s="T69">kös-BIT-I-m</ta>
            <ta e="T71" id="Seg_2225" s="T70">bu</ta>
            <ta e="T72" id="Seg_2226" s="T71">ogonnʼor-BA-r</ta>
            <ta e="T73" id="Seg_2227" s="T72">amattan</ta>
            <ta e="T74" id="Seg_2228" s="T73">dʼe</ta>
            <ta e="T75" id="Seg_2229" s="T74">ol</ta>
            <ta e="T76" id="Seg_2230" s="T75">araː</ta>
            <ta e="T77" id="Seg_2231" s="T76">dogor</ta>
            <ta e="T78" id="Seg_2232" s="T77">ile</ta>
            <ta e="T79" id="Seg_2233" s="T78">ile</ta>
            <ta e="T80" id="Seg_2234" s="T79">onton</ta>
            <ta e="T81" id="Seg_2235" s="T80">bu͡ollagɨna</ta>
            <ta e="T82" id="Seg_2236" s="T81">kim-tA</ta>
            <ta e="T83" id="Seg_2237" s="T82">iti</ta>
            <ta e="T84" id="Seg_2238" s="T83">Tonuskaj-GA</ta>
            <ta e="T85" id="Seg_2239" s="T84">emi͡e</ta>
            <ta e="T86" id="Seg_2240" s="T85">baːr</ta>
            <ta e="T87" id="Seg_2241" s="T86">ühü</ta>
            <ta e="T88" id="Seg_2242" s="T87">dogor</ta>
            <ta e="T89" id="Seg_2243" s="T88">iti</ta>
            <ta e="T90" id="Seg_2244" s="T89">kim</ta>
            <ta e="T91" id="Seg_2245" s="T90">taːs</ta>
            <ta e="T92" id="Seg_2246" s="T91">min</ta>
            <ta e="T93" id="Seg_2247" s="T92">ol-nI</ta>
            <ta e="T94" id="Seg_2248" s="T93">dʼe</ta>
            <ta e="T95" id="Seg_2249" s="T94">kör-BAtAK-I-m</ta>
            <ta e="T104" id="Seg_2250" s="T103">kör-BAtAK-I-m</ta>
            <ta e="T105" id="Seg_2251" s="T104">onno</ta>
            <ta e="T106" id="Seg_2252" s="T105">iti</ta>
            <ta e="T107" id="Seg_2253" s="T106">Tonuskaj-GA</ta>
            <ta e="T108" id="Seg_2254" s="T107">baːr</ta>
            <ta e="T109" id="Seg_2255" s="T108">di͡e-Ar-LAr</ta>
            <ta e="T110" id="Seg_2256" s="T109">ol</ta>
            <ta e="T111" id="Seg_2257" s="T110">ol</ta>
            <ta e="T112" id="Seg_2258" s="T111">bu</ta>
            <ta e="T113" id="Seg_2259" s="T112">min</ta>
            <ta e="T114" id="Seg_2260" s="T113">ogonnʼor-I-m</ta>
            <ta e="T115" id="Seg_2261" s="T114">teːte-tA</ta>
            <ta e="T116" id="Seg_2262" s="T115">ka</ta>
            <ta e="T117" id="Seg_2263" s="T116">ogonnʼor</ta>
            <ta e="T118" id="Seg_2264" s="T117">Leŋtej</ta>
            <ta e="T119" id="Seg_2265" s="T118">Leŋtej</ta>
            <ta e="T120" id="Seg_2266" s="T119">ogonnʼor</ta>
            <ta e="T121" id="Seg_2267" s="T120">kɨrdʼagas</ta>
            <ta e="T125" id="Seg_2268" s="T124">ol-tI-ŋ</ta>
            <ta e="T126" id="Seg_2269" s="T125">ol</ta>
            <ta e="T127" id="Seg_2270" s="T126">taːs-nI</ta>
            <ta e="T128" id="Seg_2271" s="T127">dʼe</ta>
            <ta e="T129" id="Seg_2272" s="T128">tep-Ar</ta>
            <ta e="T130" id="Seg_2273" s="T129">e-BIT</ta>
            <ta e="T131" id="Seg_2274" s="T130">tep-BIT</ta>
            <ta e="T132" id="Seg_2275" s="T131">slučʼajna</ta>
            <ta e="T133" id="Seg_2276" s="T132">ol</ta>
            <ta e="T134" id="Seg_2277" s="T133">taːs-I-ŋ</ta>
            <ta e="T135" id="Seg_2278" s="T134">bu͡ollagɨna</ta>
            <ta e="T136" id="Seg_2279" s="T135">balɨk</ta>
            <ta e="T137" id="Seg_2280" s="T136">gi͡en-tA</ta>
            <ta e="T138" id="Seg_2281" s="T137">e-BIT</ta>
            <ta e="T139" id="Seg_2282" s="T138">balɨk</ta>
            <ta e="T140" id="Seg_2283" s="T139">bi͡er-Ar</ta>
            <ta e="T141" id="Seg_2284" s="T140">emi͡e</ta>
            <ta e="T142" id="Seg_2285" s="T141">kim</ta>
            <ta e="T143" id="Seg_2286" s="T142">bu͡ol-TI-tA</ta>
            <ta e="T144" id="Seg_2287" s="T143">ičči-LAːK</ta>
            <ta e="T145" id="Seg_2288" s="T144">taːs</ta>
            <ta e="T146" id="Seg_2289" s="T145">e-BIT</ta>
            <ta e="T147" id="Seg_2290" s="T146">ol-tI-ŋ</ta>
            <ta e="T148" id="Seg_2291" s="T147">hir-ttAn</ta>
            <ta e="T149" id="Seg_2292" s="T148">köhün-I-BAT</ta>
            <ta e="T150" id="Seg_2293" s="T149">eː</ta>
            <ta e="T151" id="Seg_2294" s="T150">taːk</ta>
            <ta e="T152" id="Seg_2295" s="T151">da</ta>
            <ta e="T153" id="Seg_2296" s="T152">küččükkeːn</ta>
            <ta e="T154" id="Seg_2297" s="T153">ühü</ta>
            <ta e="T155" id="Seg_2298" s="T154">taːk</ta>
            <ta e="T156" id="Seg_2299" s="T155">da</ta>
            <ta e="T157" id="Seg_2300" s="T156">min</ta>
            <ta e="T158" id="Seg_2301" s="T157">kör-BAtAK-I-m</ta>
            <ta e="T159" id="Seg_2302" s="T158">hiti</ta>
            <ta e="T160" id="Seg_2303" s="T159">hiti</ta>
            <ta e="T161" id="Seg_2304" s="T160">kanna</ta>
            <ta e="T162" id="Seg_2305" s="T161">di͡e-BIT-LArA=Ij</ta>
            <ta e="T163" id="Seg_2306" s="T162">ka</ta>
            <ta e="T164" id="Seg_2307" s="T163">h-itinne</ta>
            <ta e="T168" id="Seg_2308" s="T167">h-itinne</ta>
            <ta e="T169" id="Seg_2309" s="T168">eː</ta>
            <ta e="T170" id="Seg_2310" s="T169">h-itinne</ta>
            <ta e="T171" id="Seg_2311" s="T170">h-itinne</ta>
            <ta e="T172" id="Seg_2312" s="T171">hugas</ta>
            <ta e="T173" id="Seg_2313" s="T172">hugas</ta>
            <ta e="T174" id="Seg_2314" s="T173">bu</ta>
            <ta e="T175" id="Seg_2315" s="T174">nʼu͡oguhut-LAr</ta>
            <ta e="T176" id="Seg_2316" s="T175">olor-Ar</ta>
            <ta e="T177" id="Seg_2317" s="T176">hir-LAr-tI-ttAn</ta>
            <ta e="T178" id="Seg_2318" s="T177">bu</ta>
            <ta e="T179" id="Seg_2319" s="T178">dek-kAːN</ta>
            <ta e="T180" id="Seg_2320" s="T179">hol</ta>
            <ta e="T181" id="Seg_2321" s="T180">baːr</ta>
            <ta e="T190" id="Seg_2322" s="T189">dʼe</ta>
            <ta e="T191" id="Seg_2323" s="T190">onno</ta>
            <ta e="T192" id="Seg_2324" s="T191">barɨ-tI-n</ta>
            <ta e="T193" id="Seg_2325" s="T192">bɨrak-A-GIn</ta>
            <ta e="T194" id="Seg_2326" s="T193">padarak</ta>
            <ta e="T195" id="Seg_2327" s="T194">patru͡on</ta>
            <ta e="T196" id="Seg_2328" s="T195">bu͡ol</ta>
            <ta e="T197" id="Seg_2329" s="T196">timek</ta>
            <ta e="T198" id="Seg_2330" s="T197">bu͡ol</ta>
            <ta e="T199" id="Seg_2331" s="T198">barɨ-tI-n</ta>
            <ta e="T201" id="Seg_2332" s="T200">eŋin-eŋin</ta>
            <ta e="T202" id="Seg_2333" s="T201">hep-LAr</ta>
            <ta e="T203" id="Seg_2334" s="T202">ol</ta>
            <ta e="T204" id="Seg_2335" s="T203">ogonnʼor-I-ŋ</ta>
            <ta e="T205" id="Seg_2336" s="T204">ol</ta>
            <ta e="T206" id="Seg_2337" s="T205">dʼe</ta>
            <ta e="T207" id="Seg_2338" s="T206">tep-BIT</ta>
            <ta e="T208" id="Seg_2339" s="T207">ol</ta>
            <ta e="T209" id="Seg_2340" s="T208">taːs-GI-n</ta>
            <ta e="T210" id="Seg_2341" s="T209">ol</ta>
            <ta e="T211" id="Seg_2342" s="T210">kɨrdʼagas</ta>
            <ta e="T212" id="Seg_2343" s="T211">ogonnʼor</ta>
            <ta e="T213" id="Seg_2344" s="T212">ka</ta>
            <ta e="T214" id="Seg_2345" s="T213">haːtar</ta>
            <ta e="T215" id="Seg_2346" s="T214">kör</ta>
            <ta e="T216" id="Seg_2347" s="T215">da</ta>
            <ta e="T217" id="Seg_2348" s="T216">ol-tI-ŋ</ta>
            <ta e="T218" id="Seg_2349" s="T217">atak-tI-nAn</ta>
            <ta e="T219" id="Seg_2350" s="T218">kahu͡on</ta>
            <ta e="T220" id="Seg_2351" s="T219">kün-nI</ta>
            <ta e="T221" id="Seg_2352" s="T220">hɨt-BIT</ta>
            <ta e="T231" id="Seg_2353" s="T227">eː</ta>
            <ta e="T233" id="Seg_2354" s="T232">eː</ta>
            <ta e="T234" id="Seg_2355" s="T233">bu͡o</ta>
            <ta e="T235" id="Seg_2356" s="T234">bu͡o</ta>
            <ta e="T236" id="Seg_2357" s="T235">bu͡o</ta>
            <ta e="T237" id="Seg_2358" s="T236">itinnik</ta>
            <ta e="T238" id="Seg_2359" s="T237">itinnik</ta>
            <ta e="T239" id="Seg_2360" s="T238">dogor</ta>
            <ta e="T240" id="Seg_2361" s="T239">bu</ta>
            <ta e="T241" id="Seg_2362" s="T240">hir-tI-n</ta>
            <ta e="T242" id="Seg_2363" s="T241">da</ta>
            <ta e="T243" id="Seg_2364" s="T242">ičči-LAːK-LAr</ta>
            <ta e="T244" id="Seg_2365" s="T243">bu͡o</ta>
            <ta e="T245" id="Seg_2366" s="T244">bu</ta>
            <ta e="T246" id="Seg_2367" s="T245">hir-LAr-BIt</ta>
            <ta e="T247" id="Seg_2368" s="T246">kihi</ta>
            <ta e="T248" id="Seg_2369" s="T247">kuttan-IAK</ta>
            <ta e="T97" id="Seg_2370" s="T248">hmm</ta>
            <ta e="T249" id="Seg_2371" s="T97">ile</ta>
            <ta e="T250" id="Seg_2372" s="T249">eŋin-eŋin</ta>
            <ta e="T251" id="Seg_2373" s="T250">bu͡ol-AːččI</ta>
            <ta e="T252" id="Seg_2374" s="T251">bu͡ol-TI-tA</ta>
            <ta e="T253" id="Seg_2375" s="T252">bihigi</ta>
            <ta e="T254" id="Seg_2376" s="T253">ol</ta>
            <ta e="T255" id="Seg_2377" s="T254">beje-LAr-GA</ta>
            <ta e="T256" id="Seg_2378" s="T255">bagas</ta>
            <ta e="T257" id="Seg_2379" s="T256">ile</ta>
            <ta e="T258" id="Seg_2380" s="T257">delbi</ta>
            <ta e="T259" id="Seg_2381" s="T258">belek</ta>
            <ta e="T261" id="Seg_2382" s="T259">keːs-BIT-BIt</ta>
            <ta e="T262" id="Seg_2383" s="T261">dogor</ta>
            <ta e="T263" id="Seg_2384" s="T262">intiri͡es</ta>
            <ta e="T264" id="Seg_2385" s="T263">da</ta>
            <ta e="T265" id="Seg_2386" s="T264">e</ta>
            <ta e="T266" id="Seg_2387" s="T265">da</ta>
            <ta e="T267" id="Seg_2388" s="T266">ol</ta>
            <ta e="T268" id="Seg_2389" s="T267">huruk-LAr</ta>
            <ta e="T269" id="Seg_2390" s="T268">hɨt-Ar-LArA</ta>
            <ta e="T270" id="Seg_2391" s="T269">habɨː-LAːK</ta>
            <ta e="T271" id="Seg_2392" s="T270">bu͡o</ta>
            <ta e="T272" id="Seg_2393" s="T271">ikki</ta>
            <ta e="T273" id="Seg_2394" s="T272">habɨː-LAːK</ta>
            <ta e="T274" id="Seg_2395" s="T273">ol</ta>
            <ta e="T275" id="Seg_2396" s="T274">kolbuja-ŋ</ta>
            <ta e="T276" id="Seg_2397" s="T275">iti</ta>
            <ta e="T277" id="Seg_2398" s="T276">ka</ta>
            <ta e="T278" id="Seg_2399" s="T277">urut</ta>
            <ta e="T279" id="Seg_2400" s="T278">čaːj</ta>
            <ta e="T280" id="Seg_2401" s="T279">kaː-LAr-tA</ta>
            <ta e="T281" id="Seg_2402" s="T280">kanna</ta>
            <ta e="T282" id="Seg_2403" s="T281">bul-AːččI</ta>
            <ta e="T283" id="Seg_2404" s="T282">e-TI-LArA</ta>
            <ta e="T284" id="Seg_2405" s="T283">dʼe</ta>
            <ta e="T285" id="Seg_2406" s="T284">dʼe</ta>
            <ta e="T289" id="Seg_2407" s="T288">onton</ta>
            <ta e="T290" id="Seg_2408" s="T289">ol</ta>
            <ta e="T299" id="Seg_2409" s="T298">bugurduk-kAːN</ta>
            <ta e="T300" id="Seg_2410" s="T299">hap-An</ta>
            <ta e="T303" id="Seg_2411" s="T300">keːs-IAK.[tA]</ta>
            <ta e="T304" id="Seg_2412" s="T303">eː</ta>
            <ta e="T305" id="Seg_2413" s="T304">bugurduk-kAːN</ta>
            <ta e="T306" id="Seg_2414" s="T305">onton</ta>
            <ta e="T307" id="Seg_2415" s="T306">ikki-Is-I-n</ta>
            <ta e="T308" id="Seg_2416" s="T307">hap-A-GIn</ta>
            <ta e="T309" id="Seg_2417" s="T308">dʼe</ta>
            <ta e="T310" id="Seg_2418" s="T309">onnuk-I-ŋ</ta>
            <ta e="T311" id="Seg_2419" s="T310">aldʼat-BAT</ta>
            <ta e="T312" id="Seg_2420" s="T311">bu͡o</ta>
            <ta e="T313" id="Seg_2421" s="T312">koloru͡ok</ta>
            <ta e="T314" id="Seg_2422" s="T313">kördük-LAr</ta>
            <ta e="T315" id="Seg_2423" s="T314">bugurduk</ta>
            <ta e="T316" id="Seg_2424" s="T315">bugurduk</ta>
            <ta e="T317" id="Seg_2425" s="T316">ile</ta>
            <ta e="T318" id="Seg_2426" s="T317">bosku͡oj-In</ta>
            <ta e="T319" id="Seg_2427" s="T318">da</ta>
            <ta e="T320" id="Seg_2428" s="T319">ile</ta>
            <ta e="T321" id="Seg_2429" s="T320">bul-BIT-tA</ta>
            <ta e="T322" id="Seg_2430" s="T321">ol</ta>
            <ta e="T323" id="Seg_2431" s="T322">bu</ta>
            <ta e="T324" id="Seg_2432" s="T323">taŋara-ŋ</ta>
            <ta e="T325" id="Seg_2433" s="T324">oŋor-BIT</ta>
            <ta e="T326" id="Seg_2434" s="T325">ühü</ta>
            <ta e="T327" id="Seg_2435" s="T326">hir</ta>
            <ta e="T328" id="Seg_2436" s="T327">tur-Ar-GA</ta>
            <ta e="T329" id="Seg_2437" s="T328">araː</ta>
            <ta e="T330" id="Seg_2438" s="T329">min</ta>
            <ta e="T331" id="Seg_2439" s="T330">di͡e-Ar</ta>
            <ta e="T332" id="Seg_2440" s="T331">e-TI-m</ta>
            <ta e="T333" id="Seg_2441" s="T332">araː</ta>
            <ta e="T334" id="Seg_2442" s="T333">ile</ta>
            <ta e="T335" id="Seg_2443" s="T334">gu͡orat</ta>
            <ta e="T336" id="Seg_2444" s="T335">bosku͡oj-In</ta>
            <ta e="T337" id="Seg_2445" s="T336">da</ta>
            <ta e="T338" id="Seg_2446" s="T337">tönün-AːrI-BIt</ta>
            <ta e="T339" id="Seg_2447" s="T338">onno</ta>
            <ta e="T340" id="Seg_2448" s="T339">tagaj-BAtAK-BIt</ta>
            <ta e="T341" id="Seg_2449" s="T340">tönün-AːrI-BIt</ta>
            <ta e="T342" id="Seg_2450" s="T341">bar-AːrI-BIt</ta>
            <ta e="T343" id="Seg_2451" s="T342">agaj</ta>
            <ta e="T344" id="Seg_2452" s="T343">ol</ta>
            <ta e="T345" id="Seg_2453" s="T344">barɨ-tA</ta>
            <ta e="T346" id="Seg_2454" s="T345">padarak</ta>
            <ta e="T347" id="Seg_2455" s="T346">keːs-AttAː-BIT-BIt</ta>
            <ta e="T348" id="Seg_2456" s="T347">min</ta>
            <ta e="T349" id="Seg_2457" s="T348">kim-GA</ta>
            <ta e="T350" id="Seg_2458" s="T349">kepseː-BIT-I-m=Ij</ta>
            <ta e="T351" id="Seg_2459" s="T350">ka</ta>
            <ta e="T352" id="Seg_2460" s="T351">eː</ta>
            <ta e="T353" id="Seg_2461" s="T352">Tatʼjana</ta>
            <ta e="T354" id="Seg_2462" s="T353">kim-GA</ta>
            <ta e="T355" id="Seg_2463" s="T354">Ivanavna-GA</ta>
            <ta e="T356" id="Seg_2464" s="T355">kepseː-BIT-BIn</ta>
            <ta e="T357" id="Seg_2465" s="T356">e-BIT</ta>
            <ta e="T358" id="Seg_2466" s="T357">Tatʼjana</ta>
            <ta e="T359" id="Seg_2467" s="T358">Ivanavna-GA</ta>
            <ta e="T360" id="Seg_2468" s="T359">ol-LAr-nI</ta>
            <ta e="T361" id="Seg_2469" s="T360">kepseː-BIT-I-m</ta>
            <ta e="T376" id="Seg_2470" s="T375">onton</ta>
            <ta e="T377" id="Seg_2471" s="T376">tu͡ok-nI</ta>
            <ta e="T378" id="Seg_2472" s="T377">ɨjɨt-IAk</ta>
            <ta e="T379" id="Seg_2473" s="T378">araː</ta>
            <ta e="T384" id="Seg_2474" s="T379">ol-tI-m</ta>
         </annotation>
         <annotation name="ge" tierref="ge-PoIP">
            <ta e="T1" id="Seg_2475" s="T0">Nganasan-PL-EP-2SG.[NOM]</ta>
            <ta e="T2" id="Seg_2476" s="T1">what-PL-EP-2SG.[NOM]</ta>
            <ta e="T3" id="Seg_2477" s="T2">and</ta>
            <ta e="T4" id="Seg_2478" s="T3">Nganasan-PL-EP-2SG.[NOM]</ta>
            <ta e="T5" id="Seg_2479" s="T4">nomadize-TEMP-3PL</ta>
            <ta e="T6" id="Seg_2480" s="T5">though</ta>
            <ta e="T7" id="Seg_2481" s="T6">year.[NOM]</ta>
            <ta e="T8" id="Seg_2482" s="T7">every</ta>
            <ta e="T9" id="Seg_2483" s="T8">reindeer.[NOM]</ta>
            <ta e="T10" id="Seg_2484" s="T9">fur-3SG-ACC</ta>
            <ta e="T11" id="Seg_2485" s="T10">dress-CAUS-CAUS-PRS-3PL</ta>
            <ta e="T12" id="Seg_2486" s="T11">be-PST2.[3SG]</ta>
            <ta e="T13" id="Seg_2487" s="T12">that</ta>
            <ta e="T14" id="Seg_2488" s="T13">stone-DAT/LOC</ta>
            <ta e="T15" id="Seg_2489" s="T14">AFFIRM</ta>
            <ta e="T16" id="Seg_2490" s="T15">that</ta>
            <ta e="T17" id="Seg_2491" s="T16">old.man-DAT/LOC</ta>
            <ta e="T18" id="Seg_2492" s="T17">year.[NOM]</ta>
            <ta e="T19" id="Seg_2493" s="T18">every</ta>
            <ta e="T20" id="Seg_2494" s="T19">well</ta>
            <ta e="T21" id="Seg_2495" s="T20">that</ta>
            <ta e="T22" id="Seg_2496" s="T21">fur-PL.[NOM]</ta>
            <ta e="T23" id="Seg_2497" s="T22">rot-CVB.SEQ</ta>
            <ta e="T24" id="Seg_2498" s="T23">earth-DAT/LOC</ta>
            <ta e="T25" id="Seg_2499" s="T24">earth-DAT/LOC</ta>
            <ta e="T26" id="Seg_2500" s="T25">next</ta>
            <ta e="T27" id="Seg_2501" s="T26">year-3SG-ACC</ta>
            <ta e="T28" id="Seg_2502" s="T27">again</ta>
            <ta e="T29" id="Seg_2503" s="T28">nomadize-CVB.SEQ</ta>
            <ta e="T30" id="Seg_2504" s="T29">come-PRS-3PL</ta>
            <ta e="T31" id="Seg_2505" s="T30">Nganasan-PL-EP-2SG.[NOM]</ta>
            <ta e="T33" id="Seg_2506" s="T32">different</ta>
            <ta e="T34" id="Seg_2507" s="T33">fur-ACC</ta>
            <ta e="T35" id="Seg_2508" s="T34">again</ta>
            <ta e="T36" id="Seg_2509" s="T35">reindeer-ACC</ta>
            <ta e="T37" id="Seg_2510" s="T36">kill-CVB.SEQ</ta>
            <ta e="T38" id="Seg_2511" s="T37">go-CVB.SEQ</ta>
            <ta e="T39" id="Seg_2512" s="T38">again</ta>
            <ta e="T40" id="Seg_2513" s="T39">dress-CAUS-CAUS-PRS-3PL</ta>
            <ta e="T41" id="Seg_2514" s="T40">that</ta>
            <ta e="T42" id="Seg_2515" s="T41">see.[IMP.2SG]</ta>
            <ta e="T380" id="Seg_2516" s="T42">NEG</ta>
            <ta e="T44" id="Seg_2517" s="T43">generally</ta>
            <ta e="T45" id="Seg_2518" s="T44">that</ta>
            <ta e="T46" id="Seg_2519" s="T45">place-DAT/LOC</ta>
            <ta e="T47" id="Seg_2520" s="T46">that</ta>
            <ta e="T48" id="Seg_2521" s="T47">indeed</ta>
            <ta e="T51" id="Seg_2522" s="T50">1SG.[NOM]</ta>
            <ta e="T53" id="Seg_2523" s="T52">go-EP-PST2.[3SG]</ta>
            <ta e="T54" id="Seg_2524" s="T53">old.woman-1SG</ta>
            <ta e="T55" id="Seg_2525" s="T54">this</ta>
            <ta e="T56" id="Seg_2526" s="T55">world-DAT/LOC</ta>
            <ta e="T57" id="Seg_2527" s="T56">come-CVB.SEQ-1SG</ta>
            <ta e="T58" id="Seg_2528" s="T57">that</ta>
            <ta e="T59" id="Seg_2529" s="T58">upper</ta>
            <ta e="T60" id="Seg_2530" s="T59">EMPH</ta>
            <ta e="T61" id="Seg_2531" s="T60">whereto</ta>
            <ta e="T62" id="Seg_2532" s="T61">go-FUT-1PL=Q</ta>
            <ta e="T63" id="Seg_2533" s="T62">upper</ta>
            <ta e="T64" id="Seg_2534" s="T63">that.[NOM]</ta>
            <ta e="T65" id="Seg_2535" s="T64">side</ta>
            <ta e="T66" id="Seg_2536" s="T65">this</ta>
            <ta e="T67" id="Seg_2537" s="T66">old.man-1SG-DAT/LOC</ta>
            <ta e="T68" id="Seg_2538" s="T67">come-CVB.SEQ-1SG</ta>
            <ta e="T69" id="Seg_2539" s="T68">that.[NOM]</ta>
            <ta e="T70" id="Seg_2540" s="T69">start-PST2-EP-1SG</ta>
            <ta e="T71" id="Seg_2541" s="T70">this</ta>
            <ta e="T72" id="Seg_2542" s="T71">old.man-1SG-DAT/LOC</ta>
            <ta e="T73" id="Seg_2543" s="T72">at.all</ta>
            <ta e="T74" id="Seg_2544" s="T73">well</ta>
            <ta e="T75" id="Seg_2545" s="T74">that</ta>
            <ta e="T76" id="Seg_2546" s="T75">oh.dear</ta>
            <ta e="T77" id="Seg_2547" s="T76">friend.[NOM]</ta>
            <ta e="T78" id="Seg_2548" s="T77">indeed</ta>
            <ta e="T79" id="Seg_2549" s="T78">indeed</ta>
            <ta e="T80" id="Seg_2550" s="T79">then</ta>
            <ta e="T81" id="Seg_2551" s="T80">though</ta>
            <ta e="T82" id="Seg_2552" s="T81">who-3SG.[NOM]</ta>
            <ta e="T83" id="Seg_2553" s="T82">that.[NOM]</ta>
            <ta e="T84" id="Seg_2554" s="T83">Tonuskij-DAT/LOC</ta>
            <ta e="T85" id="Seg_2555" s="T84">again</ta>
            <ta e="T86" id="Seg_2556" s="T85">there.is</ta>
            <ta e="T87" id="Seg_2557" s="T86">it.is.said</ta>
            <ta e="T88" id="Seg_2558" s="T87">friend.[NOM]</ta>
            <ta e="T89" id="Seg_2559" s="T88">that.[NOM]</ta>
            <ta e="T90" id="Seg_2560" s="T89">who.[NOM]</ta>
            <ta e="T91" id="Seg_2561" s="T90">stone.[NOM]</ta>
            <ta e="T92" id="Seg_2562" s="T91">1SG.[NOM]</ta>
            <ta e="T93" id="Seg_2563" s="T92">that-ACC</ta>
            <ta e="T94" id="Seg_2564" s="T93">well</ta>
            <ta e="T95" id="Seg_2565" s="T94">see-PST2.NEG-EP-1SG</ta>
            <ta e="T104" id="Seg_2566" s="T103">see-PST2.NEG-EP-1SG</ta>
            <ta e="T105" id="Seg_2567" s="T104">there</ta>
            <ta e="T106" id="Seg_2568" s="T105">that.[NOM]</ta>
            <ta e="T107" id="Seg_2569" s="T106">Tonuskij-DAT/LOC</ta>
            <ta e="T108" id="Seg_2570" s="T107">there.is</ta>
            <ta e="T109" id="Seg_2571" s="T108">say-PRS-3PL</ta>
            <ta e="T110" id="Seg_2572" s="T109">that</ta>
            <ta e="T111" id="Seg_2573" s="T110">that</ta>
            <ta e="T112" id="Seg_2574" s="T111">this</ta>
            <ta e="T113" id="Seg_2575" s="T112">1SG.[NOM]</ta>
            <ta e="T114" id="Seg_2576" s="T113">old.man-EP-1SG.[NOM]</ta>
            <ta e="T115" id="Seg_2577" s="T114">father-3SG.[NOM]</ta>
            <ta e="T116" id="Seg_2578" s="T115">well</ta>
            <ta e="T117" id="Seg_2579" s="T116">old.man.[NOM]</ta>
            <ta e="T118" id="Seg_2580" s="T117">Lentej</ta>
            <ta e="T119" id="Seg_2581" s="T118">Lentej</ta>
            <ta e="T120" id="Seg_2582" s="T119">old.man.[NOM]</ta>
            <ta e="T121" id="Seg_2583" s="T120">old</ta>
            <ta e="T125" id="Seg_2584" s="T124">that-3SG-2SG.[NOM]</ta>
            <ta e="T126" id="Seg_2585" s="T125">that</ta>
            <ta e="T127" id="Seg_2586" s="T126">stone-ACC</ta>
            <ta e="T128" id="Seg_2587" s="T127">well</ta>
            <ta e="T129" id="Seg_2588" s="T128">kick-PTCP.PRS</ta>
            <ta e="T130" id="Seg_2589" s="T129">be-PST2.[3SG]</ta>
            <ta e="T131" id="Seg_2590" s="T130">kick-PST2.[3SG]</ta>
            <ta e="T132" id="Seg_2591" s="T131">accidentally</ta>
            <ta e="T133" id="Seg_2592" s="T132">that</ta>
            <ta e="T134" id="Seg_2593" s="T133">stone-EP-2SG.[NOM]</ta>
            <ta e="T135" id="Seg_2594" s="T134">though</ta>
            <ta e="T136" id="Seg_2595" s="T135">fish.[NOM]</ta>
            <ta e="T137" id="Seg_2596" s="T136">belonging.to-3SG.[NOM]</ta>
            <ta e="T138" id="Seg_2597" s="T137">be-PST2.[3SG]</ta>
            <ta e="T139" id="Seg_2598" s="T138">fish.[NOM]</ta>
            <ta e="T140" id="Seg_2599" s="T139">give-PRS.[3SG]</ta>
            <ta e="T141" id="Seg_2600" s="T140">again</ta>
            <ta e="T142" id="Seg_2601" s="T141">who.[NOM]</ta>
            <ta e="T143" id="Seg_2602" s="T142">be-PST1-3SG</ta>
            <ta e="T144" id="Seg_2603" s="T143">spirit-PROPR.[NOM]</ta>
            <ta e="T145" id="Seg_2604" s="T144">stone.[NOM]</ta>
            <ta e="T146" id="Seg_2605" s="T145">be-PST2.[3SG]</ta>
            <ta e="T147" id="Seg_2606" s="T146">that-3SG-2SG.[NOM]</ta>
            <ta e="T148" id="Seg_2607" s="T147">earth-ABL</ta>
            <ta e="T149" id="Seg_2608" s="T148">to.be.on.view-EP-NEG.[3SG]</ta>
            <ta e="T150" id="Seg_2609" s="T149">eh</ta>
            <ta e="T151" id="Seg_2610" s="T150">so</ta>
            <ta e="T152" id="Seg_2611" s="T151">and</ta>
            <ta e="T153" id="Seg_2612" s="T152">tiny.[NOM]</ta>
            <ta e="T154" id="Seg_2613" s="T153">it.is.said</ta>
            <ta e="T155" id="Seg_2614" s="T154">so</ta>
            <ta e="T156" id="Seg_2615" s="T155">and</ta>
            <ta e="T157" id="Seg_2616" s="T156">1SG.[NOM]</ta>
            <ta e="T158" id="Seg_2617" s="T157">see-PST2.NEG-EP-1SG</ta>
            <ta e="T159" id="Seg_2618" s="T158">that.EMPH</ta>
            <ta e="T160" id="Seg_2619" s="T159">that.EMPH</ta>
            <ta e="T161" id="Seg_2620" s="T160">where</ta>
            <ta e="T162" id="Seg_2621" s="T161">say-PST2-3PL=Q</ta>
            <ta e="T163" id="Seg_2622" s="T162">well</ta>
            <ta e="T164" id="Seg_2623" s="T163">EMPH-there</ta>
            <ta e="T168" id="Seg_2624" s="T167">EMPH-there</ta>
            <ta e="T169" id="Seg_2625" s="T168">AFFIRM</ta>
            <ta e="T170" id="Seg_2626" s="T169">EMPH-there</ta>
            <ta e="T171" id="Seg_2627" s="T170">EMPH-there</ta>
            <ta e="T172" id="Seg_2628" s="T171">close</ta>
            <ta e="T173" id="Seg_2629" s="T172">close</ta>
            <ta e="T174" id="Seg_2630" s="T173">this</ta>
            <ta e="T175" id="Seg_2631" s="T174">front.reindeer-PL.[NOM]</ta>
            <ta e="T176" id="Seg_2632" s="T175">live-PTCP.PRS</ta>
            <ta e="T177" id="Seg_2633" s="T176">place-PL-3SG-ABL</ta>
            <ta e="T178" id="Seg_2634" s="T177">this.[NOM]</ta>
            <ta e="T179" id="Seg_2635" s="T178">to-DIM</ta>
            <ta e="T180" id="Seg_2636" s="T179">that.EMPH.[NOM]</ta>
            <ta e="T181" id="Seg_2637" s="T180">there.is</ta>
            <ta e="T190" id="Seg_2638" s="T189">well</ta>
            <ta e="T191" id="Seg_2639" s="T190">there</ta>
            <ta e="T192" id="Seg_2640" s="T191">whole-3SG-ACC</ta>
            <ta e="T193" id="Seg_2641" s="T192">throw-PRS-2SG</ta>
            <ta e="T194" id="Seg_2642" s="T193">gift.[NOM]</ta>
            <ta e="T195" id="Seg_2643" s="T194">cartridge.[NOM]</ta>
            <ta e="T196" id="Seg_2644" s="T195">maybe</ta>
            <ta e="T197" id="Seg_2645" s="T196">button.[NOM]</ta>
            <ta e="T198" id="Seg_2646" s="T197">maybe</ta>
            <ta e="T199" id="Seg_2647" s="T198">whole-3SG-ACC</ta>
            <ta e="T201" id="Seg_2648" s="T200">different-different</ta>
            <ta e="T202" id="Seg_2649" s="T201">thing-PL.[NOM]</ta>
            <ta e="T203" id="Seg_2650" s="T202">that</ta>
            <ta e="T204" id="Seg_2651" s="T203">old.man-EP-2SG.[NOM]</ta>
            <ta e="T205" id="Seg_2652" s="T204">that</ta>
            <ta e="T206" id="Seg_2653" s="T205">well</ta>
            <ta e="T207" id="Seg_2654" s="T206">kick-PST2.[3SG]</ta>
            <ta e="T208" id="Seg_2655" s="T207">that</ta>
            <ta e="T209" id="Seg_2656" s="T208">stone-2SG-ACC</ta>
            <ta e="T210" id="Seg_2657" s="T209">that</ta>
            <ta e="T211" id="Seg_2658" s="T210">old</ta>
            <ta e="T212" id="Seg_2659" s="T211">old.man.[NOM]</ta>
            <ta e="T213" id="Seg_2660" s="T212">well</ta>
            <ta e="T214" id="Seg_2661" s="T213">though</ta>
            <ta e="T215" id="Seg_2662" s="T214">see.[IMP.2SG]</ta>
            <ta e="T216" id="Seg_2663" s="T215">EMPH</ta>
            <ta e="T217" id="Seg_2664" s="T216">that-3SG-2SG.[NOM]</ta>
            <ta e="T218" id="Seg_2665" s="T217">leg-3SG-INSTR</ta>
            <ta e="T219" id="Seg_2666" s="T218">some</ta>
            <ta e="T220" id="Seg_2667" s="T219">day-ACC</ta>
            <ta e="T221" id="Seg_2668" s="T220">lie-PST2.[3SG]</ta>
            <ta e="T231" id="Seg_2669" s="T227">AFFIRM</ta>
            <ta e="T233" id="Seg_2670" s="T232">AFFIRM</ta>
            <ta e="T234" id="Seg_2671" s="T233">EMPH</ta>
            <ta e="T235" id="Seg_2672" s="T234">EMPH</ta>
            <ta e="T236" id="Seg_2673" s="T235">EMPH</ta>
            <ta e="T237" id="Seg_2674" s="T236">such</ta>
            <ta e="T238" id="Seg_2675" s="T237">such</ta>
            <ta e="T239" id="Seg_2676" s="T238">friend.[NOM]</ta>
            <ta e="T240" id="Seg_2677" s="T239">this</ta>
            <ta e="T241" id="Seg_2678" s="T240">earth-3SG-ACC</ta>
            <ta e="T242" id="Seg_2679" s="T241">EMPH</ta>
            <ta e="T243" id="Seg_2680" s="T242">master-PROPR-PL</ta>
            <ta e="T244" id="Seg_2681" s="T243">EMPH</ta>
            <ta e="T245" id="Seg_2682" s="T244">this</ta>
            <ta e="T246" id="Seg_2683" s="T245">earth-PL-1PL.[NOM]</ta>
            <ta e="T247" id="Seg_2684" s="T246">human.being.[NOM]</ta>
            <ta e="T248" id="Seg_2685" s="T247">be.afraid-PTCP.FUT.[NOM]</ta>
            <ta e="T97" id="Seg_2686" s="T248">hmm</ta>
            <ta e="T249" id="Seg_2687" s="T97">indeed</ta>
            <ta e="T250" id="Seg_2688" s="T249">different-different</ta>
            <ta e="T251" id="Seg_2689" s="T250">be-HAB.[3SG]</ta>
            <ta e="T252" id="Seg_2690" s="T251">be-PST1-3SG</ta>
            <ta e="T253" id="Seg_2691" s="T252">1PL.[NOM]</ta>
            <ta e="T254" id="Seg_2692" s="T253">that</ta>
            <ta e="T255" id="Seg_2693" s="T254">self-PL-DAT/LOC</ta>
            <ta e="T256" id="Seg_2694" s="T255">EMPH</ta>
            <ta e="T257" id="Seg_2695" s="T256">indeed</ta>
            <ta e="T258" id="Seg_2696" s="T257">enough</ta>
            <ta e="T259" id="Seg_2697" s="T258">present.[NOM]</ta>
            <ta e="T261" id="Seg_2698" s="T259">throw-PST2-1PL</ta>
            <ta e="T262" id="Seg_2699" s="T261">friend.[NOM]</ta>
            <ta e="T263" id="Seg_2700" s="T262">interesting.[NOM]</ta>
            <ta e="T264" id="Seg_2701" s="T263">EMPH</ta>
            <ta e="T265" id="Seg_2702" s="T264">eh</ta>
            <ta e="T266" id="Seg_2703" s="T265">EMPH</ta>
            <ta e="T267" id="Seg_2704" s="T266">that</ta>
            <ta e="T268" id="Seg_2705" s="T267">writing-PL.[NOM]</ta>
            <ta e="T269" id="Seg_2706" s="T268">lie-PRS-3PL</ta>
            <ta e="T270" id="Seg_2707" s="T269">lid-PROPR</ta>
            <ta e="T271" id="Seg_2708" s="T270">EMPH</ta>
            <ta e="T272" id="Seg_2709" s="T271">two</ta>
            <ta e="T273" id="Seg_2710" s="T272">lid-PROPR</ta>
            <ta e="T274" id="Seg_2711" s="T273">that</ta>
            <ta e="T275" id="Seg_2712" s="T274">box-2SG.[NOM]</ta>
            <ta e="T276" id="Seg_2713" s="T275">that.[NOM]</ta>
            <ta e="T277" id="Seg_2714" s="T276">well</ta>
            <ta e="T278" id="Seg_2715" s="T277">before</ta>
            <ta e="T279" id="Seg_2716" s="T278">tea.[NOM]</ta>
            <ta e="T280" id="Seg_2717" s="T279">storage-PL-3SG.[NOM]</ta>
            <ta e="T281" id="Seg_2718" s="T280">where</ta>
            <ta e="T282" id="Seg_2719" s="T281">find-PTCP.HAB</ta>
            <ta e="T283" id="Seg_2720" s="T282">be-PST1-3PL</ta>
            <ta e="T284" id="Seg_2721" s="T283">well</ta>
            <ta e="T285" id="Seg_2722" s="T284">well</ta>
            <ta e="T289" id="Seg_2723" s="T288">then</ta>
            <ta e="T290" id="Seg_2724" s="T289">that</ta>
            <ta e="T299" id="Seg_2725" s="T298">like.this-INTNS</ta>
            <ta e="T300" id="Seg_2726" s="T299">close-CVB.SEQ</ta>
            <ta e="T303" id="Seg_2727" s="T300">throw-FUT.[3SG]</ta>
            <ta e="T304" id="Seg_2728" s="T303">AFFIRM</ta>
            <ta e="T305" id="Seg_2729" s="T304">like.this-INTNS</ta>
            <ta e="T306" id="Seg_2730" s="T305">then</ta>
            <ta e="T307" id="Seg_2731" s="T306">two-ORD-EP-ACC</ta>
            <ta e="T308" id="Seg_2732" s="T307">close-PRS-2SG</ta>
            <ta e="T309" id="Seg_2733" s="T308">well</ta>
            <ta e="T310" id="Seg_2734" s="T309">such-EP-2SG.[NOM]</ta>
            <ta e="T311" id="Seg_2735" s="T310">break-NEG.[3SG]</ta>
            <ta e="T312" id="Seg_2736" s="T311">EMPH</ta>
            <ta e="T313" id="Seg_2737" s="T312">shelf.[NOM]</ta>
            <ta e="T314" id="Seg_2738" s="T313">like-PL</ta>
            <ta e="T315" id="Seg_2739" s="T314">like.this</ta>
            <ta e="T316" id="Seg_2740" s="T315">like.this</ta>
            <ta e="T317" id="Seg_2741" s="T316">indeed</ta>
            <ta e="T318" id="Seg_2742" s="T317">beautiful-ADVZ</ta>
            <ta e="T319" id="Seg_2743" s="T318">EMPH</ta>
            <ta e="T320" id="Seg_2744" s="T319">indeed</ta>
            <ta e="T321" id="Seg_2745" s="T320">find-PST2-3SG</ta>
            <ta e="T322" id="Seg_2746" s="T321">that</ta>
            <ta e="T323" id="Seg_2747" s="T322">this</ta>
            <ta e="T324" id="Seg_2748" s="T323">god-2SG.[NOM]</ta>
            <ta e="T325" id="Seg_2749" s="T324">make-PST2.[3SG]</ta>
            <ta e="T326" id="Seg_2750" s="T325">it.is.said</ta>
            <ta e="T327" id="Seg_2751" s="T326">earth.[NOM]</ta>
            <ta e="T328" id="Seg_2752" s="T327">stand-PTCP.PRS-DAT/LOC</ta>
            <ta e="T329" id="Seg_2753" s="T328">oh.dear</ta>
            <ta e="T330" id="Seg_2754" s="T329">1SG.[NOM]</ta>
            <ta e="T331" id="Seg_2755" s="T330">say-PTCP.PRS</ta>
            <ta e="T332" id="Seg_2756" s="T331">be-PST1-1SG</ta>
            <ta e="T333" id="Seg_2757" s="T332">oh.dear</ta>
            <ta e="T334" id="Seg_2758" s="T333">indeed</ta>
            <ta e="T335" id="Seg_2759" s="T334">city.[NOM]</ta>
            <ta e="T336" id="Seg_2760" s="T335">beautiful-ADVZ</ta>
            <ta e="T337" id="Seg_2761" s="T336">EMPH</ta>
            <ta e="T338" id="Seg_2762" s="T337">come.back-CVB.PURP-1PL</ta>
            <ta e="T339" id="Seg_2763" s="T338">there</ta>
            <ta e="T340" id="Seg_2764" s="T339">touch-PST2.NEG-1PL</ta>
            <ta e="T341" id="Seg_2765" s="T340">come.back-CVB.PURP-1PL</ta>
            <ta e="T342" id="Seg_2766" s="T341">go-CVB.PURP-1PL</ta>
            <ta e="T343" id="Seg_2767" s="T342">only</ta>
            <ta e="T344" id="Seg_2768" s="T343">that</ta>
            <ta e="T345" id="Seg_2769" s="T344">every-3SG.[NOM]</ta>
            <ta e="T346" id="Seg_2770" s="T345">gift.[NOM]</ta>
            <ta e="T347" id="Seg_2771" s="T346">throw-MULT-PST2-1PL</ta>
            <ta e="T348" id="Seg_2772" s="T347">1SG.[NOM]</ta>
            <ta e="T349" id="Seg_2773" s="T348">who-DAT/LOC</ta>
            <ta e="T350" id="Seg_2774" s="T349">tell-PST2-EP-1SG=Q</ta>
            <ta e="T351" id="Seg_2775" s="T350">well</ta>
            <ta e="T352" id="Seg_2776" s="T351">eh</ta>
            <ta e="T353" id="Seg_2777" s="T352">Tatyana</ta>
            <ta e="T354" id="Seg_2778" s="T353">who-DAT/LOC</ta>
            <ta e="T355" id="Seg_2779" s="T354">Ivanovna-DAT/LOC</ta>
            <ta e="T356" id="Seg_2780" s="T355">tell-PST2-1SG</ta>
            <ta e="T357" id="Seg_2781" s="T356">be-PST2.[3SG]</ta>
            <ta e="T358" id="Seg_2782" s="T357">Tatyana</ta>
            <ta e="T359" id="Seg_2783" s="T358">Ivanovna-DAT/LOC</ta>
            <ta e="T360" id="Seg_2784" s="T359">that-PL-ACC</ta>
            <ta e="T361" id="Seg_2785" s="T360">tell-PST2-EP-1SG</ta>
            <ta e="T376" id="Seg_2786" s="T375">then</ta>
            <ta e="T377" id="Seg_2787" s="T376">what-ACC</ta>
            <ta e="T378" id="Seg_2788" s="T377">ask-IMP.1DU</ta>
            <ta e="T379" id="Seg_2789" s="T378">oh.dear</ta>
            <ta e="T384" id="Seg_2790" s="T379">that-3SG-1SG.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg-PoIP">
            <ta e="T1" id="Seg_2791" s="T0">Nganasane-PL-EP-2SG.[NOM]</ta>
            <ta e="T2" id="Seg_2792" s="T1">was-PL-EP-2SG.[NOM]</ta>
            <ta e="T3" id="Seg_2793" s="T2">und</ta>
            <ta e="T4" id="Seg_2794" s="T3">Nganasane-PL-EP-2SG.[NOM]</ta>
            <ta e="T5" id="Seg_2795" s="T4">nomadisieren-TEMP-3PL</ta>
            <ta e="T6" id="Seg_2796" s="T5">aber</ta>
            <ta e="T7" id="Seg_2797" s="T6">Jahr.[NOM]</ta>
            <ta e="T8" id="Seg_2798" s="T7">jeder</ta>
            <ta e="T9" id="Seg_2799" s="T8">Rentier.[NOM]</ta>
            <ta e="T10" id="Seg_2800" s="T9">Fell-3SG-ACC</ta>
            <ta e="T11" id="Seg_2801" s="T10">anziehen-CAUS-CAUS-PRS-3PL</ta>
            <ta e="T12" id="Seg_2802" s="T11">sein-PST2.[3SG]</ta>
            <ta e="T13" id="Seg_2803" s="T12">jenes</ta>
            <ta e="T14" id="Seg_2804" s="T13">Stein-DAT/LOC</ta>
            <ta e="T15" id="Seg_2805" s="T14">AFFIRM</ta>
            <ta e="T16" id="Seg_2806" s="T15">jenes</ta>
            <ta e="T17" id="Seg_2807" s="T16">alter.Mann-DAT/LOC</ta>
            <ta e="T18" id="Seg_2808" s="T17">Jahr.[NOM]</ta>
            <ta e="T19" id="Seg_2809" s="T18">jeder</ta>
            <ta e="T20" id="Seg_2810" s="T19">doch</ta>
            <ta e="T21" id="Seg_2811" s="T20">jenes</ta>
            <ta e="T22" id="Seg_2812" s="T21">Fell-PL.[NOM]</ta>
            <ta e="T23" id="Seg_2813" s="T22">faulen-CVB.SEQ</ta>
            <ta e="T24" id="Seg_2814" s="T23">Erde-DAT/LOC</ta>
            <ta e="T25" id="Seg_2815" s="T24">Erde-DAT/LOC</ta>
            <ta e="T26" id="Seg_2816" s="T25">nächster</ta>
            <ta e="T27" id="Seg_2817" s="T26">Jahr-3SG-ACC</ta>
            <ta e="T28" id="Seg_2818" s="T27">wieder</ta>
            <ta e="T29" id="Seg_2819" s="T28">nomadisieren-CVB.SEQ</ta>
            <ta e="T30" id="Seg_2820" s="T29">kommen-PRS-3PL</ta>
            <ta e="T31" id="Seg_2821" s="T30">Nganasane-PL-EP-2SG.[NOM]</ta>
            <ta e="T33" id="Seg_2822" s="T32">anders</ta>
            <ta e="T34" id="Seg_2823" s="T33">Fell-ACC</ta>
            <ta e="T35" id="Seg_2824" s="T34">wieder</ta>
            <ta e="T36" id="Seg_2825" s="T35">Rentier-ACC</ta>
            <ta e="T37" id="Seg_2826" s="T36">töten-CVB.SEQ</ta>
            <ta e="T38" id="Seg_2827" s="T37">gehen-CVB.SEQ</ta>
            <ta e="T39" id="Seg_2828" s="T38">wieder</ta>
            <ta e="T40" id="Seg_2829" s="T39">anziehen-CAUS-CAUS-PRS-3PL</ta>
            <ta e="T41" id="Seg_2830" s="T40">dieses</ta>
            <ta e="T42" id="Seg_2831" s="T41">sehen.[IMP.2SG]</ta>
            <ta e="T380" id="Seg_2832" s="T42">NEG</ta>
            <ta e="T44" id="Seg_2833" s="T43">allgemein</ta>
            <ta e="T45" id="Seg_2834" s="T44">jenes</ta>
            <ta e="T46" id="Seg_2835" s="T45">Ort-DAT/LOC</ta>
            <ta e="T47" id="Seg_2836" s="T46">jenes</ta>
            <ta e="T48" id="Seg_2837" s="T47">tatsächlich</ta>
            <ta e="T51" id="Seg_2838" s="T50">1SG.[NOM]</ta>
            <ta e="T53" id="Seg_2839" s="T52">gehen-EP-PST2.[3SG]</ta>
            <ta e="T54" id="Seg_2840" s="T53">Alte-1SG</ta>
            <ta e="T55" id="Seg_2841" s="T54">dieses</ta>
            <ta e="T56" id="Seg_2842" s="T55">Welt-DAT/LOC</ta>
            <ta e="T57" id="Seg_2843" s="T56">kommen-CVB.SEQ-1SG</ta>
            <ta e="T58" id="Seg_2844" s="T57">jenes</ta>
            <ta e="T59" id="Seg_2845" s="T58">oberer</ta>
            <ta e="T60" id="Seg_2846" s="T59">EMPH</ta>
            <ta e="T61" id="Seg_2847" s="T60">wohin</ta>
            <ta e="T62" id="Seg_2848" s="T61">gehen-FUT-1PL=Q</ta>
            <ta e="T63" id="Seg_2849" s="T62">oberer</ta>
            <ta e="T64" id="Seg_2850" s="T63">jenes.[NOM]</ta>
            <ta e="T65" id="Seg_2851" s="T64">Seite</ta>
            <ta e="T66" id="Seg_2852" s="T65">dieses</ta>
            <ta e="T67" id="Seg_2853" s="T66">alter.Mann-1SG-DAT/LOC</ta>
            <ta e="T68" id="Seg_2854" s="T67">kommen-CVB.SEQ-1SG</ta>
            <ta e="T69" id="Seg_2855" s="T68">dieses.[NOM]</ta>
            <ta e="T70" id="Seg_2856" s="T69">wegfahren-PST2-EP-1SG</ta>
            <ta e="T71" id="Seg_2857" s="T70">dieses</ta>
            <ta e="T72" id="Seg_2858" s="T71">alter.Mann-1SG-DAT/LOC</ta>
            <ta e="T73" id="Seg_2859" s="T72">völlig</ta>
            <ta e="T74" id="Seg_2860" s="T73">doch</ta>
            <ta e="T75" id="Seg_2861" s="T74">jenes</ta>
            <ta e="T76" id="Seg_2862" s="T75">oh.nein</ta>
            <ta e="T77" id="Seg_2863" s="T76">Freund.[NOM]</ta>
            <ta e="T78" id="Seg_2864" s="T77">tatsächlich</ta>
            <ta e="T79" id="Seg_2865" s="T78">tatsächlich</ta>
            <ta e="T80" id="Seg_2866" s="T79">dann</ta>
            <ta e="T81" id="Seg_2867" s="T80">aber</ta>
            <ta e="T82" id="Seg_2868" s="T81">wer-3SG.[NOM]</ta>
            <ta e="T83" id="Seg_2869" s="T82">dieses.[NOM]</ta>
            <ta e="T84" id="Seg_2870" s="T83">Tonuskij-DAT/LOC</ta>
            <ta e="T85" id="Seg_2871" s="T84">wieder</ta>
            <ta e="T86" id="Seg_2872" s="T85">es.gibt</ta>
            <ta e="T87" id="Seg_2873" s="T86">man.sagt</ta>
            <ta e="T88" id="Seg_2874" s="T87">Freund.[NOM]</ta>
            <ta e="T89" id="Seg_2875" s="T88">dieses.[NOM]</ta>
            <ta e="T90" id="Seg_2876" s="T89">wer.[NOM]</ta>
            <ta e="T91" id="Seg_2877" s="T90">Stein.[NOM]</ta>
            <ta e="T92" id="Seg_2878" s="T91">1SG.[NOM]</ta>
            <ta e="T93" id="Seg_2879" s="T92">jenes-ACC</ta>
            <ta e="T94" id="Seg_2880" s="T93">doch</ta>
            <ta e="T95" id="Seg_2881" s="T94">sehen-PST2.NEG-EP-1SG</ta>
            <ta e="T104" id="Seg_2882" s="T103">sehen-PST2.NEG-EP-1SG</ta>
            <ta e="T105" id="Seg_2883" s="T104">dort</ta>
            <ta e="T106" id="Seg_2884" s="T105">dieses.[NOM]</ta>
            <ta e="T107" id="Seg_2885" s="T106">Tonuskij-DAT/LOC</ta>
            <ta e="T108" id="Seg_2886" s="T107">es.gibt</ta>
            <ta e="T109" id="Seg_2887" s="T108">sagen-PRS-3PL</ta>
            <ta e="T110" id="Seg_2888" s="T109">jenes</ta>
            <ta e="T111" id="Seg_2889" s="T110">jenes</ta>
            <ta e="T112" id="Seg_2890" s="T111">dieses</ta>
            <ta e="T113" id="Seg_2891" s="T112">1SG.[NOM]</ta>
            <ta e="T114" id="Seg_2892" s="T113">alter.Mann-EP-1SG.[NOM]</ta>
            <ta e="T115" id="Seg_2893" s="T114">Vater-3SG.[NOM]</ta>
            <ta e="T116" id="Seg_2894" s="T115">nun</ta>
            <ta e="T117" id="Seg_2895" s="T116">alter.Mann.[NOM]</ta>
            <ta e="T118" id="Seg_2896" s="T117">Lentej</ta>
            <ta e="T119" id="Seg_2897" s="T118">Lentej</ta>
            <ta e="T120" id="Seg_2898" s="T119">alter.Mann.[NOM]</ta>
            <ta e="T121" id="Seg_2899" s="T120">alt</ta>
            <ta e="T125" id="Seg_2900" s="T124">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T126" id="Seg_2901" s="T125">jenes</ta>
            <ta e="T127" id="Seg_2902" s="T126">Stein-ACC</ta>
            <ta e="T128" id="Seg_2903" s="T127">doch</ta>
            <ta e="T129" id="Seg_2904" s="T128">treten-PTCP.PRS</ta>
            <ta e="T130" id="Seg_2905" s="T129">sein-PST2.[3SG]</ta>
            <ta e="T131" id="Seg_2906" s="T130">treten-PST2.[3SG]</ta>
            <ta e="T132" id="Seg_2907" s="T131">zufällig</ta>
            <ta e="T133" id="Seg_2908" s="T132">jenes</ta>
            <ta e="T134" id="Seg_2909" s="T133">Stein-EP-2SG.[NOM]</ta>
            <ta e="T135" id="Seg_2910" s="T134">aber</ta>
            <ta e="T136" id="Seg_2911" s="T135">Fisch.[NOM]</ta>
            <ta e="T137" id="Seg_2912" s="T136">zugehörig-3SG.[NOM]</ta>
            <ta e="T138" id="Seg_2913" s="T137">sein-PST2.[3SG]</ta>
            <ta e="T139" id="Seg_2914" s="T138">Fisch.[NOM]</ta>
            <ta e="T140" id="Seg_2915" s="T139">geben-PRS.[3SG]</ta>
            <ta e="T141" id="Seg_2916" s="T140">wieder</ta>
            <ta e="T142" id="Seg_2917" s="T141">wer.[NOM]</ta>
            <ta e="T143" id="Seg_2918" s="T142">sein-PST1-3SG</ta>
            <ta e="T144" id="Seg_2919" s="T143">Geist-PROPR.[NOM]</ta>
            <ta e="T145" id="Seg_2920" s="T144">Stein.[NOM]</ta>
            <ta e="T146" id="Seg_2921" s="T145">sein-PST2.[3SG]</ta>
            <ta e="T147" id="Seg_2922" s="T146">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T148" id="Seg_2923" s="T147">Erde-ABL</ta>
            <ta e="T149" id="Seg_2924" s="T148">zu.sehen.sein-EP-NEG.[3SG]</ta>
            <ta e="T150" id="Seg_2925" s="T149">äh</ta>
            <ta e="T151" id="Seg_2926" s="T150">so</ta>
            <ta e="T152" id="Seg_2927" s="T151">und</ta>
            <ta e="T153" id="Seg_2928" s="T152">winzig.[NOM]</ta>
            <ta e="T154" id="Seg_2929" s="T153">man.sagt</ta>
            <ta e="T155" id="Seg_2930" s="T154">so</ta>
            <ta e="T156" id="Seg_2931" s="T155">und</ta>
            <ta e="T157" id="Seg_2932" s="T156">1SG.[NOM]</ta>
            <ta e="T158" id="Seg_2933" s="T157">sehen-PST2.NEG-EP-1SG</ta>
            <ta e="T159" id="Seg_2934" s="T158">dieses.EMPH</ta>
            <ta e="T160" id="Seg_2935" s="T159">dieses.EMPH</ta>
            <ta e="T161" id="Seg_2936" s="T160">wo</ta>
            <ta e="T162" id="Seg_2937" s="T161">sagen-PST2-3PL=Q</ta>
            <ta e="T163" id="Seg_2938" s="T162">nun</ta>
            <ta e="T164" id="Seg_2939" s="T163">EMPH-dort</ta>
            <ta e="T168" id="Seg_2940" s="T167">EMPH-dort</ta>
            <ta e="T169" id="Seg_2941" s="T168">AFFIRM</ta>
            <ta e="T170" id="Seg_2942" s="T169">EMPH-dort</ta>
            <ta e="T171" id="Seg_2943" s="T170">EMPH-dort</ta>
            <ta e="T172" id="Seg_2944" s="T171">nah</ta>
            <ta e="T173" id="Seg_2945" s="T172">nah</ta>
            <ta e="T174" id="Seg_2946" s="T173">dieses</ta>
            <ta e="T175" id="Seg_2947" s="T174">vorderstes.Rentier-PL.[NOM]</ta>
            <ta e="T176" id="Seg_2948" s="T175">leben-PTCP.PRS</ta>
            <ta e="T177" id="Seg_2949" s="T176">Ort-PL-3SG-ABL</ta>
            <ta e="T178" id="Seg_2950" s="T177">dieses.[NOM]</ta>
            <ta e="T179" id="Seg_2951" s="T178">zu-DIM</ta>
            <ta e="T180" id="Seg_2952" s="T179">jenes.EMPH.[NOM]</ta>
            <ta e="T181" id="Seg_2953" s="T180">es.gibt</ta>
            <ta e="T190" id="Seg_2954" s="T189">doch</ta>
            <ta e="T191" id="Seg_2955" s="T190">dort</ta>
            <ta e="T192" id="Seg_2956" s="T191">ganz-3SG-ACC</ta>
            <ta e="T193" id="Seg_2957" s="T192">werfen-PRS-2SG</ta>
            <ta e="T194" id="Seg_2958" s="T193">Geschenk.[NOM]</ta>
            <ta e="T195" id="Seg_2959" s="T194">Patrone.[NOM]</ta>
            <ta e="T196" id="Seg_2960" s="T195">vielleicht</ta>
            <ta e="T197" id="Seg_2961" s="T196">Knopf.[NOM]</ta>
            <ta e="T198" id="Seg_2962" s="T197">vielleicht</ta>
            <ta e="T199" id="Seg_2963" s="T198">ganz-3SG-ACC</ta>
            <ta e="T201" id="Seg_2964" s="T200">unterschiedlich-unterschiedlich</ta>
            <ta e="T202" id="Seg_2965" s="T201">Sache-PL.[NOM]</ta>
            <ta e="T203" id="Seg_2966" s="T202">jenes</ta>
            <ta e="T204" id="Seg_2967" s="T203">alter.Mann-EP-2SG.[NOM]</ta>
            <ta e="T205" id="Seg_2968" s="T204">jenes</ta>
            <ta e="T206" id="Seg_2969" s="T205">doch</ta>
            <ta e="T207" id="Seg_2970" s="T206">treten-PST2.[3SG]</ta>
            <ta e="T208" id="Seg_2971" s="T207">jenes</ta>
            <ta e="T209" id="Seg_2972" s="T208">Stein-2SG-ACC</ta>
            <ta e="T210" id="Seg_2973" s="T209">jenes</ta>
            <ta e="T211" id="Seg_2974" s="T210">alt</ta>
            <ta e="T212" id="Seg_2975" s="T211">alter.Mann.[NOM]</ta>
            <ta e="T213" id="Seg_2976" s="T212">nun</ta>
            <ta e="T214" id="Seg_2977" s="T213">wenn.auch</ta>
            <ta e="T215" id="Seg_2978" s="T214">sehen.[IMP.2SG]</ta>
            <ta e="T216" id="Seg_2979" s="T215">EMPH</ta>
            <ta e="T217" id="Seg_2980" s="T216">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T218" id="Seg_2981" s="T217">Bein-3SG-INSTR</ta>
            <ta e="T219" id="Seg_2982" s="T218">einige</ta>
            <ta e="T220" id="Seg_2983" s="T219">Tag-ACC</ta>
            <ta e="T221" id="Seg_2984" s="T220">liegen-PST2.[3SG]</ta>
            <ta e="T231" id="Seg_2985" s="T227">AFFIRM</ta>
            <ta e="T233" id="Seg_2986" s="T232">AFFIRM</ta>
            <ta e="T234" id="Seg_2987" s="T233">EMPH</ta>
            <ta e="T235" id="Seg_2988" s="T234">EMPH</ta>
            <ta e="T236" id="Seg_2989" s="T235">EMPH</ta>
            <ta e="T237" id="Seg_2990" s="T236">solch</ta>
            <ta e="T238" id="Seg_2991" s="T237">solch</ta>
            <ta e="T239" id="Seg_2992" s="T238">Freund.[NOM]</ta>
            <ta e="T240" id="Seg_2993" s="T239">dieses</ta>
            <ta e="T241" id="Seg_2994" s="T240">Erde-3SG-ACC</ta>
            <ta e="T242" id="Seg_2995" s="T241">EMPH</ta>
            <ta e="T243" id="Seg_2996" s="T242">Herr-PROPR-PL</ta>
            <ta e="T244" id="Seg_2997" s="T243">EMPH</ta>
            <ta e="T245" id="Seg_2998" s="T244">dieses</ta>
            <ta e="T246" id="Seg_2999" s="T245">Erde-PL-1PL.[NOM]</ta>
            <ta e="T247" id="Seg_3000" s="T246">Mensch.[NOM]</ta>
            <ta e="T248" id="Seg_3001" s="T247">Angst.haben-PTCP.FUT.[NOM]</ta>
            <ta e="T97" id="Seg_3002" s="T248">hmm</ta>
            <ta e="T249" id="Seg_3003" s="T97">tatsächlich</ta>
            <ta e="T250" id="Seg_3004" s="T249">unterschiedlich-unterschiedlich</ta>
            <ta e="T251" id="Seg_3005" s="T250">sein-HAB.[3SG]</ta>
            <ta e="T252" id="Seg_3006" s="T251">sein-PST1-3SG</ta>
            <ta e="T253" id="Seg_3007" s="T252">1PL.[NOM]</ta>
            <ta e="T254" id="Seg_3008" s="T253">jenes</ta>
            <ta e="T255" id="Seg_3009" s="T254">selbst-PL-DAT/LOC</ta>
            <ta e="T256" id="Seg_3010" s="T255">EMPH</ta>
            <ta e="T257" id="Seg_3011" s="T256">tatsächlich</ta>
            <ta e="T258" id="Seg_3012" s="T257">genug</ta>
            <ta e="T259" id="Seg_3013" s="T258">Geschenk.[NOM]</ta>
            <ta e="T261" id="Seg_3014" s="T259">werfen-PST2-1PL</ta>
            <ta e="T262" id="Seg_3015" s="T261">Freund.[NOM]</ta>
            <ta e="T263" id="Seg_3016" s="T262">interessant.[NOM]</ta>
            <ta e="T264" id="Seg_3017" s="T263">EMPH</ta>
            <ta e="T265" id="Seg_3018" s="T264">äh</ta>
            <ta e="T266" id="Seg_3019" s="T265">EMPH</ta>
            <ta e="T267" id="Seg_3020" s="T266">jenes</ta>
            <ta e="T268" id="Seg_3021" s="T267">Schrift-PL.[NOM]</ta>
            <ta e="T269" id="Seg_3022" s="T268">liegen-PRS-3PL</ta>
            <ta e="T270" id="Seg_3023" s="T269">Deckel-PROPR</ta>
            <ta e="T271" id="Seg_3024" s="T270">EMPH</ta>
            <ta e="T272" id="Seg_3025" s="T271">zwei</ta>
            <ta e="T273" id="Seg_3026" s="T272">Deckel-PROPR</ta>
            <ta e="T274" id="Seg_3027" s="T273">jenes</ta>
            <ta e="T275" id="Seg_3028" s="T274">Kiste-2SG.[NOM]</ta>
            <ta e="T276" id="Seg_3029" s="T275">dieses.[NOM]</ta>
            <ta e="T277" id="Seg_3030" s="T276">nun</ta>
            <ta e="T278" id="Seg_3031" s="T277">früher</ta>
            <ta e="T279" id="Seg_3032" s="T278">Tee.[NOM]</ta>
            <ta e="T280" id="Seg_3033" s="T279">Lager-PL-3SG.[NOM]</ta>
            <ta e="T281" id="Seg_3034" s="T280">wo</ta>
            <ta e="T282" id="Seg_3035" s="T281">finden-PTCP.HAB</ta>
            <ta e="T283" id="Seg_3036" s="T282">sein-PST1-3PL</ta>
            <ta e="T284" id="Seg_3037" s="T283">doch</ta>
            <ta e="T285" id="Seg_3038" s="T284">doch</ta>
            <ta e="T289" id="Seg_3039" s="T288">dann</ta>
            <ta e="T290" id="Seg_3040" s="T289">jenes</ta>
            <ta e="T299" id="Seg_3041" s="T298">so-INTNS</ta>
            <ta e="T300" id="Seg_3042" s="T299">schließen-CVB.SEQ</ta>
            <ta e="T303" id="Seg_3043" s="T300">werfen-FUT.[3SG]</ta>
            <ta e="T304" id="Seg_3044" s="T303">AFFIRM</ta>
            <ta e="T305" id="Seg_3045" s="T304">so-INTNS</ta>
            <ta e="T306" id="Seg_3046" s="T305">dann</ta>
            <ta e="T307" id="Seg_3047" s="T306">zwei-ORD-EP-ACC</ta>
            <ta e="T308" id="Seg_3048" s="T307">schließen-PRS-2SG</ta>
            <ta e="T309" id="Seg_3049" s="T308">doch</ta>
            <ta e="T310" id="Seg_3050" s="T309">solch-EP-2SG.[NOM]</ta>
            <ta e="T311" id="Seg_3051" s="T310">zerbrechen-NEG.[3SG]</ta>
            <ta e="T312" id="Seg_3052" s="T311">EMPH</ta>
            <ta e="T313" id="Seg_3053" s="T312">Regalbrett.[NOM]</ta>
            <ta e="T314" id="Seg_3054" s="T313">wie-PL</ta>
            <ta e="T315" id="Seg_3055" s="T314">so</ta>
            <ta e="T316" id="Seg_3056" s="T315">so</ta>
            <ta e="T317" id="Seg_3057" s="T316">tatsächlich</ta>
            <ta e="T318" id="Seg_3058" s="T317">schön-ADVZ</ta>
            <ta e="T319" id="Seg_3059" s="T318">EMPH</ta>
            <ta e="T320" id="Seg_3060" s="T319">tatsächlich</ta>
            <ta e="T321" id="Seg_3061" s="T320">finden-PST2-3SG</ta>
            <ta e="T322" id="Seg_3062" s="T321">jenes</ta>
            <ta e="T323" id="Seg_3063" s="T322">dieses</ta>
            <ta e="T324" id="Seg_3064" s="T323">Gott-2SG.[NOM]</ta>
            <ta e="T325" id="Seg_3065" s="T324">machen-PST2.[3SG]</ta>
            <ta e="T326" id="Seg_3066" s="T325">man.sagt</ta>
            <ta e="T327" id="Seg_3067" s="T326">Erde.[NOM]</ta>
            <ta e="T328" id="Seg_3068" s="T327">stehen-PTCP.PRS-DAT/LOC</ta>
            <ta e="T329" id="Seg_3069" s="T328">oh.nein</ta>
            <ta e="T330" id="Seg_3070" s="T329">1SG.[NOM]</ta>
            <ta e="T331" id="Seg_3071" s="T330">sagen-PTCP.PRS</ta>
            <ta e="T332" id="Seg_3072" s="T331">sein-PST1-1SG</ta>
            <ta e="T333" id="Seg_3073" s="T332">oh.nein</ta>
            <ta e="T334" id="Seg_3074" s="T333">tatsächlich</ta>
            <ta e="T335" id="Seg_3075" s="T334">Stadt.[NOM]</ta>
            <ta e="T336" id="Seg_3076" s="T335">schön-ADVZ</ta>
            <ta e="T337" id="Seg_3077" s="T336">EMPH</ta>
            <ta e="T338" id="Seg_3078" s="T337">zurückkommen-CVB.PURP-1PL</ta>
            <ta e="T339" id="Seg_3079" s="T338">dort</ta>
            <ta e="T340" id="Seg_3080" s="T339">berühren-PST2.NEG-1PL</ta>
            <ta e="T341" id="Seg_3081" s="T340">zurückkommen-CVB.PURP-1PL</ta>
            <ta e="T342" id="Seg_3082" s="T341">gehen-CVB.PURP-1PL</ta>
            <ta e="T343" id="Seg_3083" s="T342">nur</ta>
            <ta e="T344" id="Seg_3084" s="T343">jenes</ta>
            <ta e="T345" id="Seg_3085" s="T344">jeder-3SG.[NOM]</ta>
            <ta e="T346" id="Seg_3086" s="T345">Geschenk.[NOM]</ta>
            <ta e="T347" id="Seg_3087" s="T346">werfen-MULT-PST2-1PL</ta>
            <ta e="T348" id="Seg_3088" s="T347">1SG.[NOM]</ta>
            <ta e="T349" id="Seg_3089" s="T348">wer-DAT/LOC</ta>
            <ta e="T350" id="Seg_3090" s="T349">erzählen-PST2-EP-1SG=Q</ta>
            <ta e="T351" id="Seg_3091" s="T350">nun</ta>
            <ta e="T352" id="Seg_3092" s="T351">äh</ta>
            <ta e="T353" id="Seg_3093" s="T352">Tatjana</ta>
            <ta e="T354" id="Seg_3094" s="T353">wer-DAT/LOC</ta>
            <ta e="T355" id="Seg_3095" s="T354">Ivanovna-DAT/LOC</ta>
            <ta e="T356" id="Seg_3096" s="T355">erzählen-PST2-1SG</ta>
            <ta e="T357" id="Seg_3097" s="T356">sein-PST2.[3SG]</ta>
            <ta e="T358" id="Seg_3098" s="T357">Tatjana</ta>
            <ta e="T359" id="Seg_3099" s="T358">Ivanovna-DAT/LOC</ta>
            <ta e="T360" id="Seg_3100" s="T359">jenes-PL-ACC</ta>
            <ta e="T361" id="Seg_3101" s="T360">erzählen-PST2-EP-1SG</ta>
            <ta e="T376" id="Seg_3102" s="T375">dann</ta>
            <ta e="T377" id="Seg_3103" s="T376">was-ACC</ta>
            <ta e="T378" id="Seg_3104" s="T377">fragen-IMP.1DU</ta>
            <ta e="T379" id="Seg_3105" s="T378">oh.nein</ta>
            <ta e="T384" id="Seg_3106" s="T379">jenes-3SG-1SG.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr-PoIP">
            <ta e="T1" id="Seg_3107" s="T0">Нганасан-PL-EP-2SG.[NOM]</ta>
            <ta e="T2" id="Seg_3108" s="T1">что-PL-EP-2SG.[NOM]</ta>
            <ta e="T3" id="Seg_3109" s="T2">а</ta>
            <ta e="T4" id="Seg_3110" s="T3">Нганасан-PL-EP-2SG.[NOM]</ta>
            <ta e="T5" id="Seg_3111" s="T4">кочевать-TEMP-3PL</ta>
            <ta e="T6" id="Seg_3112" s="T5">однако</ta>
            <ta e="T7" id="Seg_3113" s="T6">год.[NOM]</ta>
            <ta e="T8" id="Seg_3114" s="T7">каждый</ta>
            <ta e="T9" id="Seg_3115" s="T8">олень.[NOM]</ta>
            <ta e="T10" id="Seg_3116" s="T9">шкура-3SG-ACC</ta>
            <ta e="T11" id="Seg_3117" s="T10">одеть-CAUS-CAUS-PRS-3PL</ta>
            <ta e="T12" id="Seg_3118" s="T11">быть-PST2.[3SG]</ta>
            <ta e="T13" id="Seg_3119" s="T12">тот</ta>
            <ta e="T14" id="Seg_3120" s="T13">камень-DAT/LOC</ta>
            <ta e="T15" id="Seg_3121" s="T14">AFFIRM</ta>
            <ta e="T16" id="Seg_3122" s="T15">тот</ta>
            <ta e="T17" id="Seg_3123" s="T16">старик-DAT/LOC</ta>
            <ta e="T18" id="Seg_3124" s="T17">год.[NOM]</ta>
            <ta e="T19" id="Seg_3125" s="T18">каждый</ta>
            <ta e="T20" id="Seg_3126" s="T19">вот</ta>
            <ta e="T21" id="Seg_3127" s="T20">тот</ta>
            <ta e="T22" id="Seg_3128" s="T21">шкура-PL.[NOM]</ta>
            <ta e="T23" id="Seg_3129" s="T22">гнить-CVB.SEQ</ta>
            <ta e="T24" id="Seg_3130" s="T23">земля-DAT/LOC</ta>
            <ta e="T25" id="Seg_3131" s="T24">земля-DAT/LOC</ta>
            <ta e="T26" id="Seg_3132" s="T25">следующий</ta>
            <ta e="T27" id="Seg_3133" s="T26">год-3SG-ACC</ta>
            <ta e="T28" id="Seg_3134" s="T27">опять</ta>
            <ta e="T29" id="Seg_3135" s="T28">кочевать-CVB.SEQ</ta>
            <ta e="T30" id="Seg_3136" s="T29">приходить-PRS-3PL</ta>
            <ta e="T31" id="Seg_3137" s="T30">Нганасан-PL-EP-2SG.[NOM]</ta>
            <ta e="T33" id="Seg_3138" s="T32">другой</ta>
            <ta e="T34" id="Seg_3139" s="T33">шкура-ACC</ta>
            <ta e="T35" id="Seg_3140" s="T34">опять</ta>
            <ta e="T36" id="Seg_3141" s="T35">олень-ACC</ta>
            <ta e="T37" id="Seg_3142" s="T36">убить-CVB.SEQ</ta>
            <ta e="T38" id="Seg_3143" s="T37">идти-CVB.SEQ</ta>
            <ta e="T39" id="Seg_3144" s="T38">опять</ta>
            <ta e="T40" id="Seg_3145" s="T39">одеть-CAUS-CAUS-PRS-3PL</ta>
            <ta e="T41" id="Seg_3146" s="T40">тот</ta>
            <ta e="T42" id="Seg_3147" s="T41">видеть.[IMP.2SG]</ta>
            <ta e="T380" id="Seg_3148" s="T42">NEG</ta>
            <ta e="T44" id="Seg_3149" s="T43">вобщем</ta>
            <ta e="T45" id="Seg_3150" s="T44">тот</ta>
            <ta e="T46" id="Seg_3151" s="T45">место-DAT/LOC</ta>
            <ta e="T47" id="Seg_3152" s="T46">тот</ta>
            <ta e="T48" id="Seg_3153" s="T47">вправду</ta>
            <ta e="T51" id="Seg_3154" s="T50">1SG.[NOM]</ta>
            <ta e="T53" id="Seg_3155" s="T52">идти-EP-PST2.[3SG]</ta>
            <ta e="T54" id="Seg_3156" s="T53">старуха-1SG</ta>
            <ta e="T55" id="Seg_3157" s="T54">этот</ta>
            <ta e="T56" id="Seg_3158" s="T55">мир-DAT/LOC</ta>
            <ta e="T57" id="Seg_3159" s="T56">приходить-CVB.SEQ-1SG</ta>
            <ta e="T58" id="Seg_3160" s="T57">тот</ta>
            <ta e="T59" id="Seg_3161" s="T58">верхний</ta>
            <ta e="T60" id="Seg_3162" s="T59">EMPH</ta>
            <ta e="T61" id="Seg_3163" s="T60">куда</ta>
            <ta e="T62" id="Seg_3164" s="T61">идти-FUT-1PL=Q</ta>
            <ta e="T63" id="Seg_3165" s="T62">верхний</ta>
            <ta e="T64" id="Seg_3166" s="T63">тот.[NOM]</ta>
            <ta e="T65" id="Seg_3167" s="T64">сторона</ta>
            <ta e="T66" id="Seg_3168" s="T65">этот</ta>
            <ta e="T67" id="Seg_3169" s="T66">старик-1SG-DAT/LOC</ta>
            <ta e="T68" id="Seg_3170" s="T67">приходить-CVB.SEQ-1SG</ta>
            <ta e="T69" id="Seg_3171" s="T68">тот.[NOM]</ta>
            <ta e="T70" id="Seg_3172" s="T69">уезжать-PST2-EP-1SG</ta>
            <ta e="T71" id="Seg_3173" s="T70">этот</ta>
            <ta e="T72" id="Seg_3174" s="T71">старик-1SG-DAT/LOC</ta>
            <ta e="T73" id="Seg_3175" s="T72">вообще</ta>
            <ta e="T74" id="Seg_3176" s="T73">вот</ta>
            <ta e="T75" id="Seg_3177" s="T74">тот</ta>
            <ta e="T76" id="Seg_3178" s="T75">вот.беда</ta>
            <ta e="T77" id="Seg_3179" s="T76">друг.[NOM]</ta>
            <ta e="T78" id="Seg_3180" s="T77">вправду</ta>
            <ta e="T79" id="Seg_3181" s="T78">вправду</ta>
            <ta e="T80" id="Seg_3182" s="T79">потом</ta>
            <ta e="T81" id="Seg_3183" s="T80">однако</ta>
            <ta e="T82" id="Seg_3184" s="T81">кто-3SG.[NOM]</ta>
            <ta e="T83" id="Seg_3185" s="T82">тот.[NOM]</ta>
            <ta e="T84" id="Seg_3186" s="T83">Тонуский-DAT/LOC</ta>
            <ta e="T85" id="Seg_3187" s="T84">опять</ta>
            <ta e="T86" id="Seg_3188" s="T85">есть</ta>
            <ta e="T87" id="Seg_3189" s="T86">говорят</ta>
            <ta e="T88" id="Seg_3190" s="T87">друг.[NOM]</ta>
            <ta e="T89" id="Seg_3191" s="T88">тот.[NOM]</ta>
            <ta e="T90" id="Seg_3192" s="T89">кто.[NOM]</ta>
            <ta e="T91" id="Seg_3193" s="T90">камень.[NOM]</ta>
            <ta e="T92" id="Seg_3194" s="T91">1SG.[NOM]</ta>
            <ta e="T93" id="Seg_3195" s="T92">тот-ACC</ta>
            <ta e="T94" id="Seg_3196" s="T93">вот</ta>
            <ta e="T95" id="Seg_3197" s="T94">видеть-PST2.NEG-EP-1SG</ta>
            <ta e="T104" id="Seg_3198" s="T103">видеть-PST2.NEG-EP-1SG</ta>
            <ta e="T105" id="Seg_3199" s="T104">там</ta>
            <ta e="T106" id="Seg_3200" s="T105">тот.[NOM]</ta>
            <ta e="T107" id="Seg_3201" s="T106">Тонуский-DAT/LOC</ta>
            <ta e="T108" id="Seg_3202" s="T107">есть</ta>
            <ta e="T109" id="Seg_3203" s="T108">говорить-PRS-3PL</ta>
            <ta e="T110" id="Seg_3204" s="T109">тот</ta>
            <ta e="T111" id="Seg_3205" s="T110">тот</ta>
            <ta e="T112" id="Seg_3206" s="T111">этот</ta>
            <ta e="T113" id="Seg_3207" s="T112">1SG.[NOM]</ta>
            <ta e="T114" id="Seg_3208" s="T113">старик-EP-1SG.[NOM]</ta>
            <ta e="T115" id="Seg_3209" s="T114">отец-3SG.[NOM]</ta>
            <ta e="T116" id="Seg_3210" s="T115">вот</ta>
            <ta e="T117" id="Seg_3211" s="T116">старик.[NOM]</ta>
            <ta e="T118" id="Seg_3212" s="T117">Лентей</ta>
            <ta e="T119" id="Seg_3213" s="T118">Лентей</ta>
            <ta e="T120" id="Seg_3214" s="T119">старик.[NOM]</ta>
            <ta e="T121" id="Seg_3215" s="T120">старый</ta>
            <ta e="T125" id="Seg_3216" s="T124">тот-3SG-2SG.[NOM]</ta>
            <ta e="T126" id="Seg_3217" s="T125">тот</ta>
            <ta e="T127" id="Seg_3218" s="T126">камень-ACC</ta>
            <ta e="T128" id="Seg_3219" s="T127">вот</ta>
            <ta e="T129" id="Seg_3220" s="T128">пнуть-PTCP.PRS</ta>
            <ta e="T130" id="Seg_3221" s="T129">быть-PST2.[3SG]</ta>
            <ta e="T131" id="Seg_3222" s="T130">пнуть-PST2.[3SG]</ta>
            <ta e="T132" id="Seg_3223" s="T131">случайно</ta>
            <ta e="T133" id="Seg_3224" s="T132">тот</ta>
            <ta e="T134" id="Seg_3225" s="T133">камень-EP-2SG.[NOM]</ta>
            <ta e="T135" id="Seg_3226" s="T134">однако</ta>
            <ta e="T136" id="Seg_3227" s="T135">рыба.[NOM]</ta>
            <ta e="T137" id="Seg_3228" s="T136">принадлежащий-3SG.[NOM]</ta>
            <ta e="T138" id="Seg_3229" s="T137">быть-PST2.[3SG]</ta>
            <ta e="T139" id="Seg_3230" s="T138">рыба.[NOM]</ta>
            <ta e="T140" id="Seg_3231" s="T139">давать-PRS.[3SG]</ta>
            <ta e="T141" id="Seg_3232" s="T140">опять</ta>
            <ta e="T142" id="Seg_3233" s="T141">кто.[NOM]</ta>
            <ta e="T143" id="Seg_3234" s="T142">быть-PST1-3SG</ta>
            <ta e="T144" id="Seg_3235" s="T143">дух-PROPR.[NOM]</ta>
            <ta e="T145" id="Seg_3236" s="T144">камень.[NOM]</ta>
            <ta e="T146" id="Seg_3237" s="T145">быть-PST2.[3SG]</ta>
            <ta e="T147" id="Seg_3238" s="T146">тот-3SG-2SG.[NOM]</ta>
            <ta e="T148" id="Seg_3239" s="T147">земля-ABL</ta>
            <ta e="T149" id="Seg_3240" s="T148">быть.видно-EP-NEG.[3SG]</ta>
            <ta e="T150" id="Seg_3241" s="T149">ээ</ta>
            <ta e="T151" id="Seg_3242" s="T150">так</ta>
            <ta e="T152" id="Seg_3243" s="T151">да</ta>
            <ta e="T153" id="Seg_3244" s="T152">крошечный.[NOM]</ta>
            <ta e="T154" id="Seg_3245" s="T153">говорят</ta>
            <ta e="T155" id="Seg_3246" s="T154">так</ta>
            <ta e="T156" id="Seg_3247" s="T155">да</ta>
            <ta e="T157" id="Seg_3248" s="T156">1SG.[NOM]</ta>
            <ta e="T158" id="Seg_3249" s="T157">видеть-PST2.NEG-EP-1SG</ta>
            <ta e="T159" id="Seg_3250" s="T158">тот.EMPH</ta>
            <ta e="T160" id="Seg_3251" s="T159">тот.EMPH</ta>
            <ta e="T161" id="Seg_3252" s="T160">где</ta>
            <ta e="T162" id="Seg_3253" s="T161">говорить-PST2-3PL=Q</ta>
            <ta e="T163" id="Seg_3254" s="T162">вот</ta>
            <ta e="T164" id="Seg_3255" s="T163">EMPH-там</ta>
            <ta e="T168" id="Seg_3256" s="T167">EMPH-там</ta>
            <ta e="T169" id="Seg_3257" s="T168">AFFIRM</ta>
            <ta e="T170" id="Seg_3258" s="T169">EMPH-там</ta>
            <ta e="T171" id="Seg_3259" s="T170">EMPH-там</ta>
            <ta e="T172" id="Seg_3260" s="T171">близкий</ta>
            <ta e="T173" id="Seg_3261" s="T172">близкий</ta>
            <ta e="T174" id="Seg_3262" s="T173">этот</ta>
            <ta e="T175" id="Seg_3263" s="T174">передовой.олень-PL.[NOM]</ta>
            <ta e="T176" id="Seg_3264" s="T175">жить-PTCP.PRS</ta>
            <ta e="T177" id="Seg_3265" s="T176">место-PL-3SG-ABL</ta>
            <ta e="T178" id="Seg_3266" s="T177">этот.[NOM]</ta>
            <ta e="T179" id="Seg_3267" s="T178">к-DIM</ta>
            <ta e="T180" id="Seg_3268" s="T179">тот.EMPH.[NOM]</ta>
            <ta e="T181" id="Seg_3269" s="T180">есть</ta>
            <ta e="T190" id="Seg_3270" s="T189">вот</ta>
            <ta e="T191" id="Seg_3271" s="T190">там</ta>
            <ta e="T192" id="Seg_3272" s="T191">целый-3SG-ACC</ta>
            <ta e="T193" id="Seg_3273" s="T192">бросать-PRS-2SG</ta>
            <ta e="T194" id="Seg_3274" s="T193">подарок.[NOM]</ta>
            <ta e="T195" id="Seg_3275" s="T194">патрон.[NOM]</ta>
            <ta e="T196" id="Seg_3276" s="T195">может</ta>
            <ta e="T197" id="Seg_3277" s="T196">пуговица.[NOM]</ta>
            <ta e="T198" id="Seg_3278" s="T197">может</ta>
            <ta e="T199" id="Seg_3279" s="T198">целый-3SG-ACC</ta>
            <ta e="T201" id="Seg_3280" s="T200">разный-разный</ta>
            <ta e="T202" id="Seg_3281" s="T201">вещь-PL.[NOM]</ta>
            <ta e="T203" id="Seg_3282" s="T202">тот</ta>
            <ta e="T204" id="Seg_3283" s="T203">старик-EP-2SG.[NOM]</ta>
            <ta e="T205" id="Seg_3284" s="T204">тот</ta>
            <ta e="T206" id="Seg_3285" s="T205">вот</ta>
            <ta e="T207" id="Seg_3286" s="T206">пнуть-PST2.[3SG]</ta>
            <ta e="T208" id="Seg_3287" s="T207">тот</ta>
            <ta e="T209" id="Seg_3288" s="T208">камень-2SG-ACC</ta>
            <ta e="T210" id="Seg_3289" s="T209">тот</ta>
            <ta e="T211" id="Seg_3290" s="T210">старый</ta>
            <ta e="T212" id="Seg_3291" s="T211">старик.[NOM]</ta>
            <ta e="T213" id="Seg_3292" s="T212">вот</ta>
            <ta e="T214" id="Seg_3293" s="T213">хоть</ta>
            <ta e="T215" id="Seg_3294" s="T214">видеть.[IMP.2SG]</ta>
            <ta e="T216" id="Seg_3295" s="T215">EMPH</ta>
            <ta e="T217" id="Seg_3296" s="T216">тот-3SG-2SG.[NOM]</ta>
            <ta e="T218" id="Seg_3297" s="T217">нога-3SG-INSTR</ta>
            <ta e="T219" id="Seg_3298" s="T218">несколько</ta>
            <ta e="T220" id="Seg_3299" s="T219">день-ACC</ta>
            <ta e="T221" id="Seg_3300" s="T220">лежать-PST2.[3SG]</ta>
            <ta e="T231" id="Seg_3301" s="T227">AFFIRM</ta>
            <ta e="T233" id="Seg_3302" s="T232">AFFIRM</ta>
            <ta e="T234" id="Seg_3303" s="T233">EMPH</ta>
            <ta e="T235" id="Seg_3304" s="T234">EMPH</ta>
            <ta e="T236" id="Seg_3305" s="T235">EMPH</ta>
            <ta e="T237" id="Seg_3306" s="T236">такой</ta>
            <ta e="T238" id="Seg_3307" s="T237">такой</ta>
            <ta e="T239" id="Seg_3308" s="T238">друг.[NOM]</ta>
            <ta e="T240" id="Seg_3309" s="T239">этот</ta>
            <ta e="T241" id="Seg_3310" s="T240">земля-3SG-ACC</ta>
            <ta e="T242" id="Seg_3311" s="T241">EMPH</ta>
            <ta e="T243" id="Seg_3312" s="T242">господин-PROPR-PL</ta>
            <ta e="T244" id="Seg_3313" s="T243">EMPH</ta>
            <ta e="T245" id="Seg_3314" s="T244">этот</ta>
            <ta e="T246" id="Seg_3315" s="T245">земля-PL-1PL.[NOM]</ta>
            <ta e="T247" id="Seg_3316" s="T246">человек.[NOM]</ta>
            <ta e="T248" id="Seg_3317" s="T247">бояться-PTCP.FUT.[NOM]</ta>
            <ta e="T97" id="Seg_3318" s="T248">мм</ta>
            <ta e="T249" id="Seg_3319" s="T97">вправду</ta>
            <ta e="T250" id="Seg_3320" s="T249">разный-разный</ta>
            <ta e="T251" id="Seg_3321" s="T250">быть-HAB.[3SG]</ta>
            <ta e="T252" id="Seg_3322" s="T251">быть-PST1-3SG</ta>
            <ta e="T253" id="Seg_3323" s="T252">1PL.[NOM]</ta>
            <ta e="T254" id="Seg_3324" s="T253">тот</ta>
            <ta e="T255" id="Seg_3325" s="T254">сам-PL-DAT/LOC</ta>
            <ta e="T256" id="Seg_3326" s="T255">EMPH</ta>
            <ta e="T257" id="Seg_3327" s="T256">вправду</ta>
            <ta e="T258" id="Seg_3328" s="T257">вдоволь</ta>
            <ta e="T259" id="Seg_3329" s="T258">подарок.[NOM]</ta>
            <ta e="T261" id="Seg_3330" s="T259">бросать-PST2-1PL</ta>
            <ta e="T262" id="Seg_3331" s="T261">друг.[NOM]</ta>
            <ta e="T263" id="Seg_3332" s="T262">интересный.[NOM]</ta>
            <ta e="T264" id="Seg_3333" s="T263">EMPH</ta>
            <ta e="T265" id="Seg_3334" s="T264">э</ta>
            <ta e="T266" id="Seg_3335" s="T265">EMPH</ta>
            <ta e="T267" id="Seg_3336" s="T266">тот</ta>
            <ta e="T268" id="Seg_3337" s="T267">письмо-PL.[NOM]</ta>
            <ta e="T269" id="Seg_3338" s="T268">лежать-PRS-3PL</ta>
            <ta e="T270" id="Seg_3339" s="T269">крышка-PROPR</ta>
            <ta e="T271" id="Seg_3340" s="T270">EMPH</ta>
            <ta e="T272" id="Seg_3341" s="T271">два</ta>
            <ta e="T273" id="Seg_3342" s="T272">крышка-PROPR</ta>
            <ta e="T274" id="Seg_3343" s="T273">тот</ta>
            <ta e="T275" id="Seg_3344" s="T274">ящичек-2SG.[NOM]</ta>
            <ta e="T276" id="Seg_3345" s="T275">тот.[NOM]</ta>
            <ta e="T277" id="Seg_3346" s="T276">вот</ta>
            <ta e="T278" id="Seg_3347" s="T277">раньше</ta>
            <ta e="T279" id="Seg_3348" s="T278">чай.[NOM]</ta>
            <ta e="T280" id="Seg_3349" s="T279">склад-PL-3SG.[NOM]</ta>
            <ta e="T281" id="Seg_3350" s="T280">где</ta>
            <ta e="T282" id="Seg_3351" s="T281">найти-PTCP.HAB</ta>
            <ta e="T283" id="Seg_3352" s="T282">быть-PST1-3PL</ta>
            <ta e="T284" id="Seg_3353" s="T283">вот</ta>
            <ta e="T285" id="Seg_3354" s="T284">вот</ta>
            <ta e="T289" id="Seg_3355" s="T288">потом</ta>
            <ta e="T290" id="Seg_3356" s="T289">тот</ta>
            <ta e="T299" id="Seg_3357" s="T298">так-INTNS</ta>
            <ta e="T300" id="Seg_3358" s="T299">закрывать-CVB.SEQ</ta>
            <ta e="T303" id="Seg_3359" s="T300">бросать-FUT.[3SG]</ta>
            <ta e="T304" id="Seg_3360" s="T303">AFFIRM</ta>
            <ta e="T305" id="Seg_3361" s="T304">так-INTNS</ta>
            <ta e="T306" id="Seg_3362" s="T305">потом</ta>
            <ta e="T307" id="Seg_3363" s="T306">два-ORD-EP-ACC</ta>
            <ta e="T308" id="Seg_3364" s="T307">закрывать-PRS-2SG</ta>
            <ta e="T309" id="Seg_3365" s="T308">вот</ta>
            <ta e="T310" id="Seg_3366" s="T309">такой-EP-2SG.[NOM]</ta>
            <ta e="T311" id="Seg_3367" s="T310">разбивать-NEG.[3sG]</ta>
            <ta e="T312" id="Seg_3368" s="T311">EMPH</ta>
            <ta e="T313" id="Seg_3369" s="T312">доска.[NOM]</ta>
            <ta e="T314" id="Seg_3370" s="T313">как-PL</ta>
            <ta e="T315" id="Seg_3371" s="T314">так</ta>
            <ta e="T316" id="Seg_3372" s="T315">так</ta>
            <ta e="T317" id="Seg_3373" s="T316">вправду</ta>
            <ta e="T318" id="Seg_3374" s="T317">красивый-ADVZ</ta>
            <ta e="T319" id="Seg_3375" s="T318">EMPH</ta>
            <ta e="T320" id="Seg_3376" s="T319">вправду</ta>
            <ta e="T321" id="Seg_3377" s="T320">найти-PST2-3SG</ta>
            <ta e="T322" id="Seg_3378" s="T321">тот</ta>
            <ta e="T323" id="Seg_3379" s="T322">этот</ta>
            <ta e="T324" id="Seg_3380" s="T323">Бог-2SG.[NOM]</ta>
            <ta e="T325" id="Seg_3381" s="T324">делать-PST2.[3SG]</ta>
            <ta e="T326" id="Seg_3382" s="T325">говорят</ta>
            <ta e="T327" id="Seg_3383" s="T326">земля.[NOM]</ta>
            <ta e="T328" id="Seg_3384" s="T327">стоять-PTCP.PRS-DAT/LOC</ta>
            <ta e="T329" id="Seg_3385" s="T328">вот.беда</ta>
            <ta e="T330" id="Seg_3386" s="T329">1SG.[NOM]</ta>
            <ta e="T331" id="Seg_3387" s="T330">говорить-PTCP.PRS</ta>
            <ta e="T332" id="Seg_3388" s="T331">быть-PST1-1SG</ta>
            <ta e="T333" id="Seg_3389" s="T332">вот.беда</ta>
            <ta e="T334" id="Seg_3390" s="T333">вправду</ta>
            <ta e="T335" id="Seg_3391" s="T334">город.[NOM]</ta>
            <ta e="T336" id="Seg_3392" s="T335">красивый-ADVZ</ta>
            <ta e="T337" id="Seg_3393" s="T336">EMPH</ta>
            <ta e="T338" id="Seg_3394" s="T337">возвращаться-CVB.PURP-1PL</ta>
            <ta e="T339" id="Seg_3395" s="T338">там</ta>
            <ta e="T340" id="Seg_3396" s="T339">притронуться-PST2.NEG-1PL</ta>
            <ta e="T341" id="Seg_3397" s="T340">возвращаться-CVB.PURP-1PL</ta>
            <ta e="T342" id="Seg_3398" s="T341">идти-CVB.PURP-1PL</ta>
            <ta e="T343" id="Seg_3399" s="T342">только</ta>
            <ta e="T344" id="Seg_3400" s="T343">тот</ta>
            <ta e="T345" id="Seg_3401" s="T344">каждый-3SG.[NOM]</ta>
            <ta e="T346" id="Seg_3402" s="T345">подарок.[NOM]</ta>
            <ta e="T347" id="Seg_3403" s="T346">бросать-MULT-PST2-1PL</ta>
            <ta e="T348" id="Seg_3404" s="T347">1SG.[NOM]</ta>
            <ta e="T349" id="Seg_3405" s="T348">кто-DAT/LOC</ta>
            <ta e="T350" id="Seg_3406" s="T349">рассказывать-PST2-EP-1SG=Q</ta>
            <ta e="T351" id="Seg_3407" s="T350">вот</ta>
            <ta e="T352" id="Seg_3408" s="T351">ээ</ta>
            <ta e="T353" id="Seg_3409" s="T352">Татьяна</ta>
            <ta e="T354" id="Seg_3410" s="T353">кто-DAT/LOC</ta>
            <ta e="T355" id="Seg_3411" s="T354">Ивановна-DAT/LOC</ta>
            <ta e="T356" id="Seg_3412" s="T355">рассказывать-PST2-1SG</ta>
            <ta e="T357" id="Seg_3413" s="T356">быть-PST2.[3SG]</ta>
            <ta e="T358" id="Seg_3414" s="T357">Татьяна</ta>
            <ta e="T359" id="Seg_3415" s="T358">Ивановна-DAT/LOC</ta>
            <ta e="T360" id="Seg_3416" s="T359">тот-PL-ACC</ta>
            <ta e="T361" id="Seg_3417" s="T360">рассказывать-PST2-EP-1SG</ta>
            <ta e="T376" id="Seg_3418" s="T375">потом</ta>
            <ta e="T377" id="Seg_3419" s="T376">что-ACC</ta>
            <ta e="T378" id="Seg_3420" s="T377">спрашивать-IMP.1DU</ta>
            <ta e="T379" id="Seg_3421" s="T378">вот.беда</ta>
            <ta e="T384" id="Seg_3422" s="T379">тот-3SG-1SG.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc-PoIP">
            <ta e="T1" id="Seg_3423" s="T0">n-n:(num)-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T2" id="Seg_3424" s="T1">que-pro:(num)-pro:(ins)-pro:(poss)-pro:case</ta>
            <ta e="T3" id="Seg_3425" s="T2">conj</ta>
            <ta e="T4" id="Seg_3426" s="T3">n-n:(num)-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T5" id="Seg_3427" s="T4">v-v:mood-v:temp.pn</ta>
            <ta e="T6" id="Seg_3428" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_3429" s="T6">n-n:case</ta>
            <ta e="T8" id="Seg_3430" s="T7">adj</ta>
            <ta e="T9" id="Seg_3431" s="T8">n-n:case</ta>
            <ta e="T10" id="Seg_3432" s="T9">n-n:poss-n:case</ta>
            <ta e="T11" id="Seg_3433" s="T10">v-v&gt;v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T12" id="Seg_3434" s="T11">v-v:tense-v:pred.pn</ta>
            <ta e="T13" id="Seg_3435" s="T12">dempro</ta>
            <ta e="T14" id="Seg_3436" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_3437" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_3438" s="T15">dempro</ta>
            <ta e="T17" id="Seg_3439" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_3440" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_3441" s="T18">adj</ta>
            <ta e="T20" id="Seg_3442" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_3443" s="T20">dempro</ta>
            <ta e="T22" id="Seg_3444" s="T21">n-n:(num)-n:case</ta>
            <ta e="T23" id="Seg_3445" s="T22">v-v:cvb</ta>
            <ta e="T24" id="Seg_3446" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_3447" s="T24">n-n:case</ta>
            <ta e="T26" id="Seg_3448" s="T25">adj</ta>
            <ta e="T27" id="Seg_3449" s="T26">n-n:poss-n:case</ta>
            <ta e="T28" id="Seg_3450" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_3451" s="T28">v-v:cvb</ta>
            <ta e="T30" id="Seg_3452" s="T29">v-v:tense-v:pred.pn</ta>
            <ta e="T31" id="Seg_3453" s="T30">n-n:(num)-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T33" id="Seg_3454" s="T32">adj</ta>
            <ta e="T34" id="Seg_3455" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_3456" s="T34">ptcl</ta>
            <ta e="T36" id="Seg_3457" s="T35">n-n:case</ta>
            <ta e="T37" id="Seg_3458" s="T36">v-v:cvb</ta>
            <ta e="T38" id="Seg_3459" s="T37">v-v:cvb</ta>
            <ta e="T39" id="Seg_3460" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_3461" s="T39">v-v&gt;v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T41" id="Seg_3462" s="T40">dempro</ta>
            <ta e="T42" id="Seg_3463" s="T41">v-v:mood.pn</ta>
            <ta e="T380" id="Seg_3464" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_3465" s="T43">adv</ta>
            <ta e="T45" id="Seg_3466" s="T44">dempro</ta>
            <ta e="T46" id="Seg_3467" s="T45">n-n:case</ta>
            <ta e="T47" id="Seg_3468" s="T46">dempro</ta>
            <ta e="T48" id="Seg_3469" s="T47">adv</ta>
            <ta e="T51" id="Seg_3470" s="T50">pers-pro:case</ta>
            <ta e="T53" id="Seg_3471" s="T52">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T54" id="Seg_3472" s="T53">n-n:(pred.pn)</ta>
            <ta e="T55" id="Seg_3473" s="T54">dempro</ta>
            <ta e="T56" id="Seg_3474" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_3475" s="T56">v-v:cvb-v:pred.pn</ta>
            <ta e="T58" id="Seg_3476" s="T57">dempro</ta>
            <ta e="T59" id="Seg_3477" s="T58">adj</ta>
            <ta e="T60" id="Seg_3478" s="T59">ptcl</ta>
            <ta e="T61" id="Seg_3479" s="T60">que</ta>
            <ta e="T62" id="Seg_3480" s="T61">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T63" id="Seg_3481" s="T62">adj</ta>
            <ta e="T64" id="Seg_3482" s="T63">dempro-pro:case</ta>
            <ta e="T65" id="Seg_3483" s="T64">n</ta>
            <ta e="T66" id="Seg_3484" s="T65">dempro</ta>
            <ta e="T67" id="Seg_3485" s="T66">n-n:poss-n:case</ta>
            <ta e="T68" id="Seg_3486" s="T67">v-v:cvb-v:pred.pn</ta>
            <ta e="T69" id="Seg_3487" s="T68">dempro-pro:case</ta>
            <ta e="T70" id="Seg_3488" s="T69">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T71" id="Seg_3489" s="T70">dempro</ta>
            <ta e="T72" id="Seg_3490" s="T71">n-n:poss-n:case</ta>
            <ta e="T73" id="Seg_3491" s="T72">adv</ta>
            <ta e="T74" id="Seg_3492" s="T73">ptcl</ta>
            <ta e="T75" id="Seg_3493" s="T74">dempro</ta>
            <ta e="T76" id="Seg_3494" s="T75">interj</ta>
            <ta e="T77" id="Seg_3495" s="T76">n-n:case</ta>
            <ta e="T78" id="Seg_3496" s="T77">adv</ta>
            <ta e="T79" id="Seg_3497" s="T78">adv</ta>
            <ta e="T80" id="Seg_3498" s="T79">adv</ta>
            <ta e="T81" id="Seg_3499" s="T80">ptcl</ta>
            <ta e="T82" id="Seg_3500" s="T81">que-pro:(poss)-pro:case</ta>
            <ta e="T83" id="Seg_3501" s="T82">dempro-pro:case</ta>
            <ta e="T84" id="Seg_3502" s="T83">propr-n:case</ta>
            <ta e="T85" id="Seg_3503" s="T84">ptcl</ta>
            <ta e="T86" id="Seg_3504" s="T85">ptcl</ta>
            <ta e="T87" id="Seg_3505" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_3506" s="T87">n-n:case</ta>
            <ta e="T89" id="Seg_3507" s="T88">dempro-pro:case</ta>
            <ta e="T90" id="Seg_3508" s="T89">que-pro:case</ta>
            <ta e="T91" id="Seg_3509" s="T90">n-n:case</ta>
            <ta e="T92" id="Seg_3510" s="T91">pers-pro:case</ta>
            <ta e="T93" id="Seg_3511" s="T92">dempro-pro:case</ta>
            <ta e="T94" id="Seg_3512" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_3513" s="T94">v-v:neg-v:(ins)-v:poss.pn</ta>
            <ta e="T104" id="Seg_3514" s="T103">v-v:neg-v:(ins)-v:poss.pn</ta>
            <ta e="T105" id="Seg_3515" s="T104">adv</ta>
            <ta e="T106" id="Seg_3516" s="T105">dempro-pro:case</ta>
            <ta e="T107" id="Seg_3517" s="T106">propr-n:case</ta>
            <ta e="T108" id="Seg_3518" s="T107">ptcl</ta>
            <ta e="T109" id="Seg_3519" s="T108">v-v:tense-v:pred.pn</ta>
            <ta e="T110" id="Seg_3520" s="T109">dempro</ta>
            <ta e="T111" id="Seg_3521" s="T110">dempro</ta>
            <ta e="T112" id="Seg_3522" s="T111">dempro</ta>
            <ta e="T113" id="Seg_3523" s="T112">pers-pro:case</ta>
            <ta e="T114" id="Seg_3524" s="T113">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T115" id="Seg_3525" s="T114">n-n:(poss)-n:case</ta>
            <ta e="T116" id="Seg_3526" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_3527" s="T116">n-n:case</ta>
            <ta e="T118" id="Seg_3528" s="T117">propr</ta>
            <ta e="T119" id="Seg_3529" s="T118">propr</ta>
            <ta e="T120" id="Seg_3530" s="T119">n-n:case</ta>
            <ta e="T121" id="Seg_3531" s="T120">adj</ta>
            <ta e="T125" id="Seg_3532" s="T124">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T126" id="Seg_3533" s="T125">dempro</ta>
            <ta e="T127" id="Seg_3534" s="T126">n-n:case</ta>
            <ta e="T128" id="Seg_3535" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_3536" s="T128">v-v:ptcp</ta>
            <ta e="T130" id="Seg_3537" s="T129">v-v:tense-v:pred.pn</ta>
            <ta e="T131" id="Seg_3538" s="T130">v-v:tense-v:pred.pn</ta>
            <ta e="T132" id="Seg_3539" s="T131">adv</ta>
            <ta e="T133" id="Seg_3540" s="T132">dempro</ta>
            <ta e="T134" id="Seg_3541" s="T133">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T135" id="Seg_3542" s="T134">ptcl</ta>
            <ta e="T136" id="Seg_3543" s="T135">n-n:case</ta>
            <ta e="T137" id="Seg_3544" s="T136">adj-n:(poss)-n:case</ta>
            <ta e="T138" id="Seg_3545" s="T137">v-v:tense-v:pred.pn</ta>
            <ta e="T139" id="Seg_3546" s="T138">n-n:case</ta>
            <ta e="T140" id="Seg_3547" s="T139">v-v:tense-v:pred.pn</ta>
            <ta e="T141" id="Seg_3548" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_3549" s="T141">que-pro:case</ta>
            <ta e="T143" id="Seg_3550" s="T142">v-v:tense-v:poss.pn</ta>
            <ta e="T144" id="Seg_3551" s="T143">n-n&gt;adj-n:case</ta>
            <ta e="T145" id="Seg_3552" s="T144">n-n:case</ta>
            <ta e="T146" id="Seg_3553" s="T145">v-v:tense-v:pred.pn</ta>
            <ta e="T147" id="Seg_3554" s="T146">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T148" id="Seg_3555" s="T147">n-n:case</ta>
            <ta e="T149" id="Seg_3556" s="T148">v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T150" id="Seg_3557" s="T149">interj</ta>
            <ta e="T151" id="Seg_3558" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_3559" s="T151">conj</ta>
            <ta e="T153" id="Seg_3560" s="T152">adj-n:case</ta>
            <ta e="T154" id="Seg_3561" s="T153">ptcl</ta>
            <ta e="T155" id="Seg_3562" s="T154">ptcl</ta>
            <ta e="T156" id="Seg_3563" s="T155">conj</ta>
            <ta e="T157" id="Seg_3564" s="T156">pers-pro:case</ta>
            <ta e="T158" id="Seg_3565" s="T157">v-v:neg-v:(ins)-v:poss.pn</ta>
            <ta e="T159" id="Seg_3566" s="T158">dempro</ta>
            <ta e="T160" id="Seg_3567" s="T159">dempro</ta>
            <ta e="T161" id="Seg_3568" s="T160">que</ta>
            <ta e="T162" id="Seg_3569" s="T161">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T163" id="Seg_3570" s="T162">ptcl</ta>
            <ta e="T164" id="Seg_3571" s="T163">adv&gt;adv-adv</ta>
            <ta e="T168" id="Seg_3572" s="T167">adv&gt;adv-adv</ta>
            <ta e="T169" id="Seg_3573" s="T168">ptcl</ta>
            <ta e="T170" id="Seg_3574" s="T169">adv&gt;adv-adv</ta>
            <ta e="T171" id="Seg_3575" s="T170">adv&gt;adv-adv</ta>
            <ta e="T172" id="Seg_3576" s="T171">adj</ta>
            <ta e="T173" id="Seg_3577" s="T172">adj</ta>
            <ta e="T174" id="Seg_3578" s="T173">dempro</ta>
            <ta e="T175" id="Seg_3579" s="T174">n-n:(num)-n:case</ta>
            <ta e="T176" id="Seg_3580" s="T175">v-v:ptcp</ta>
            <ta e="T177" id="Seg_3581" s="T176">n-n:(num)-n:poss-n:case</ta>
            <ta e="T178" id="Seg_3582" s="T177">dempro-pro:case</ta>
            <ta e="T179" id="Seg_3583" s="T178">post-n&gt;n</ta>
            <ta e="T180" id="Seg_3584" s="T179">dempro-pro:case</ta>
            <ta e="T181" id="Seg_3585" s="T180">ptcl</ta>
            <ta e="T190" id="Seg_3586" s="T189">ptcl</ta>
            <ta e="T191" id="Seg_3587" s="T190">adv</ta>
            <ta e="T192" id="Seg_3588" s="T191">adj-n:poss-n:case</ta>
            <ta e="T193" id="Seg_3589" s="T192">v-v:tense-v:pred.pn</ta>
            <ta e="T194" id="Seg_3590" s="T193">n-n:case</ta>
            <ta e="T195" id="Seg_3591" s="T194">n-n:case</ta>
            <ta e="T196" id="Seg_3592" s="T195">ptcl</ta>
            <ta e="T197" id="Seg_3593" s="T196">n-n:case</ta>
            <ta e="T198" id="Seg_3594" s="T197">ptcl</ta>
            <ta e="T199" id="Seg_3595" s="T198">adj-n:poss-n:case</ta>
            <ta e="T201" id="Seg_3596" s="T200">adj-adj</ta>
            <ta e="T202" id="Seg_3597" s="T201">n-n:(num)-n:case</ta>
            <ta e="T203" id="Seg_3598" s="T202">dempro</ta>
            <ta e="T204" id="Seg_3599" s="T203">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T205" id="Seg_3600" s="T204">dempro</ta>
            <ta e="T206" id="Seg_3601" s="T205">ptcl</ta>
            <ta e="T207" id="Seg_3602" s="T206">v-v:tense-v:pred.pn</ta>
            <ta e="T208" id="Seg_3603" s="T207">dempro</ta>
            <ta e="T209" id="Seg_3604" s="T208">n-n:poss-n:case</ta>
            <ta e="T210" id="Seg_3605" s="T209">dempro</ta>
            <ta e="T211" id="Seg_3606" s="T210">adj</ta>
            <ta e="T212" id="Seg_3607" s="T211">n-n:case</ta>
            <ta e="T213" id="Seg_3608" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_3609" s="T213">ptcl</ta>
            <ta e="T215" id="Seg_3610" s="T214">v-v:mood.pn</ta>
            <ta e="T216" id="Seg_3611" s="T215">ptcl</ta>
            <ta e="T217" id="Seg_3612" s="T216">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T218" id="Seg_3613" s="T217">n-n:poss-n:case</ta>
            <ta e="T219" id="Seg_3614" s="T218">quant</ta>
            <ta e="T220" id="Seg_3615" s="T219">n-n:case</ta>
            <ta e="T221" id="Seg_3616" s="T220">v-v:tense-v:pred.pn</ta>
            <ta e="T231" id="Seg_3617" s="T227">ptcl</ta>
            <ta e="T233" id="Seg_3618" s="T232">ptcl</ta>
            <ta e="T234" id="Seg_3619" s="T233">ptcl</ta>
            <ta e="T235" id="Seg_3620" s="T234">ptcl</ta>
            <ta e="T236" id="Seg_3621" s="T235">ptcl</ta>
            <ta e="T237" id="Seg_3622" s="T236">dempro</ta>
            <ta e="T238" id="Seg_3623" s="T237">dempro</ta>
            <ta e="T239" id="Seg_3624" s="T238">n-n:case</ta>
            <ta e="T240" id="Seg_3625" s="T239">dempro</ta>
            <ta e="T241" id="Seg_3626" s="T240">n-n:poss-n:case</ta>
            <ta e="T242" id="Seg_3627" s="T241">ptcl</ta>
            <ta e="T243" id="Seg_3628" s="T242">n-n&gt;adj-n:(num)</ta>
            <ta e="T244" id="Seg_3629" s="T243">ptcl</ta>
            <ta e="T245" id="Seg_3630" s="T244">dempro</ta>
            <ta e="T246" id="Seg_3631" s="T245">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T247" id="Seg_3632" s="T246">n-n:case</ta>
            <ta e="T248" id="Seg_3633" s="T247">v-v:ptcp-v:(case)</ta>
            <ta e="T97" id="Seg_3634" s="T248">interj</ta>
            <ta e="T249" id="Seg_3635" s="T97">adv</ta>
            <ta e="T250" id="Seg_3636" s="T249">adj-adj</ta>
            <ta e="T251" id="Seg_3637" s="T250">v-v:mood-v:pred.pn</ta>
            <ta e="T252" id="Seg_3638" s="T251">v-v:tense-v:poss.pn</ta>
            <ta e="T253" id="Seg_3639" s="T252">pers-pro:case</ta>
            <ta e="T254" id="Seg_3640" s="T253">dempro</ta>
            <ta e="T255" id="Seg_3641" s="T254">emphpro-pro:(num)-pro:case</ta>
            <ta e="T256" id="Seg_3642" s="T255">ptcl</ta>
            <ta e="T257" id="Seg_3643" s="T256">adv</ta>
            <ta e="T258" id="Seg_3644" s="T257">adv</ta>
            <ta e="T259" id="Seg_3645" s="T258">n-n:case</ta>
            <ta e="T261" id="Seg_3646" s="T259">v-v:tense-v:pred.pn</ta>
            <ta e="T262" id="Seg_3647" s="T261">n-n:case</ta>
            <ta e="T263" id="Seg_3648" s="T262">adj-n:case</ta>
            <ta e="T264" id="Seg_3649" s="T263">ptcl</ta>
            <ta e="T265" id="Seg_3650" s="T264">interj</ta>
            <ta e="T266" id="Seg_3651" s="T265">ptcl</ta>
            <ta e="T267" id="Seg_3652" s="T266">dempro</ta>
            <ta e="T268" id="Seg_3653" s="T267">n-n:(num)-n:case</ta>
            <ta e="T269" id="Seg_3654" s="T268">v-v:tense-v:poss.pn</ta>
            <ta e="T270" id="Seg_3655" s="T269">n-n&gt;adj</ta>
            <ta e="T271" id="Seg_3656" s="T270">ptcl</ta>
            <ta e="T272" id="Seg_3657" s="T271">cardnum</ta>
            <ta e="T273" id="Seg_3658" s="T272">n-n&gt;adj</ta>
            <ta e="T274" id="Seg_3659" s="T273">dempro</ta>
            <ta e="T275" id="Seg_3660" s="T274">n-n:(poss)-n:case</ta>
            <ta e="T276" id="Seg_3661" s="T275">dempro-pro:case</ta>
            <ta e="T277" id="Seg_3662" s="T276">ptcl</ta>
            <ta e="T278" id="Seg_3663" s="T277">adv</ta>
            <ta e="T279" id="Seg_3664" s="T278">n-n:case</ta>
            <ta e="T280" id="Seg_3665" s="T279">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T281" id="Seg_3666" s="T280">que</ta>
            <ta e="T282" id="Seg_3667" s="T281">v-v:ptcp</ta>
            <ta e="T283" id="Seg_3668" s="T282">v-v:tense-v:poss.pn</ta>
            <ta e="T284" id="Seg_3669" s="T283">ptcl</ta>
            <ta e="T285" id="Seg_3670" s="T284">ptcl</ta>
            <ta e="T289" id="Seg_3671" s="T288">adv</ta>
            <ta e="T290" id="Seg_3672" s="T289">dempro</ta>
            <ta e="T299" id="Seg_3673" s="T298">adv-adv&gt;adv</ta>
            <ta e="T300" id="Seg_3674" s="T299">v-v:cvb</ta>
            <ta e="T303" id="Seg_3675" s="T300">v-v:tense-v:poss.pn</ta>
            <ta e="T304" id="Seg_3676" s="T303">ptcl</ta>
            <ta e="T305" id="Seg_3677" s="T304">adv-adv&gt;adv</ta>
            <ta e="T306" id="Seg_3678" s="T305">adv</ta>
            <ta e="T307" id="Seg_3679" s="T306">cardnum-cardnum&gt;ordnum-n:(ins)-n:case</ta>
            <ta e="T308" id="Seg_3680" s="T307">v-v:tense-v:pred.pn</ta>
            <ta e="T309" id="Seg_3681" s="T308">ptcl</ta>
            <ta e="T310" id="Seg_3682" s="T309">dempro-pro:(ins)-pro:(poss)-pro:case</ta>
            <ta e="T311" id="Seg_3683" s="T310">v-v:(neg)-v:pred.pn</ta>
            <ta e="T312" id="Seg_3684" s="T311">ptcl</ta>
            <ta e="T313" id="Seg_3685" s="T312">n-n:case</ta>
            <ta e="T314" id="Seg_3686" s="T313">post-n:(num)</ta>
            <ta e="T315" id="Seg_3687" s="T314">adv</ta>
            <ta e="T316" id="Seg_3688" s="T315">adv</ta>
            <ta e="T317" id="Seg_3689" s="T316">adv</ta>
            <ta e="T318" id="Seg_3690" s="T317">adj-adj&gt;adv</ta>
            <ta e="T319" id="Seg_3691" s="T318">ptcl</ta>
            <ta e="T320" id="Seg_3692" s="T319">adv</ta>
            <ta e="T321" id="Seg_3693" s="T320">v-v:tense-v:poss.pn</ta>
            <ta e="T322" id="Seg_3694" s="T321">dempro</ta>
            <ta e="T323" id="Seg_3695" s="T322">dempro</ta>
            <ta e="T324" id="Seg_3696" s="T323">n-n:(poss)-n:case</ta>
            <ta e="T325" id="Seg_3697" s="T324">v-v:tense-v:pred.pn</ta>
            <ta e="T326" id="Seg_3698" s="T325">ptcl</ta>
            <ta e="T327" id="Seg_3699" s="T326">n-n:case</ta>
            <ta e="T328" id="Seg_3700" s="T327">v-v:ptcp-v:(case)</ta>
            <ta e="T329" id="Seg_3701" s="T328">interj</ta>
            <ta e="T330" id="Seg_3702" s="T329">pers-pro:case</ta>
            <ta e="T331" id="Seg_3703" s="T330">v-v:ptcp</ta>
            <ta e="T332" id="Seg_3704" s="T331">v-v:tense-v:poss.pn</ta>
            <ta e="T333" id="Seg_3705" s="T332">interj</ta>
            <ta e="T334" id="Seg_3706" s="T333">adv</ta>
            <ta e="T335" id="Seg_3707" s="T334">n-n:case</ta>
            <ta e="T336" id="Seg_3708" s="T335">adj-adj&gt;adv</ta>
            <ta e="T337" id="Seg_3709" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_3710" s="T337">v-v:cvb-v:pred.pn</ta>
            <ta e="T339" id="Seg_3711" s="T338">adv</ta>
            <ta e="T340" id="Seg_3712" s="T339">v-v:neg-v:pred.pn</ta>
            <ta e="T341" id="Seg_3713" s="T340">v-v:cvb-v:pred.pn</ta>
            <ta e="T342" id="Seg_3714" s="T341">v-v:cvb-v:pred.pn</ta>
            <ta e="T343" id="Seg_3715" s="T342">ptcl</ta>
            <ta e="T344" id="Seg_3716" s="T343">dempro</ta>
            <ta e="T345" id="Seg_3717" s="T344">adj-n:(poss)-n:case</ta>
            <ta e="T346" id="Seg_3718" s="T345">n-n:case</ta>
            <ta e="T347" id="Seg_3719" s="T346">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T348" id="Seg_3720" s="T347">pers-pro:case</ta>
            <ta e="T349" id="Seg_3721" s="T348">que-pro:case</ta>
            <ta e="T350" id="Seg_3722" s="T349">v-v:tense-v:(ins)-v:poss.pn-ptcl</ta>
            <ta e="T351" id="Seg_3723" s="T350">ptcl</ta>
            <ta e="T352" id="Seg_3724" s="T351">interj</ta>
            <ta e="T353" id="Seg_3725" s="T352">propr</ta>
            <ta e="T354" id="Seg_3726" s="T353">que-pro:case</ta>
            <ta e="T355" id="Seg_3727" s="T354">propr-n:case</ta>
            <ta e="T356" id="Seg_3728" s="T355">v-v:tense-v:pred.pn</ta>
            <ta e="T357" id="Seg_3729" s="T356">v-v:tense-v:pred.pn</ta>
            <ta e="T358" id="Seg_3730" s="T357">propr</ta>
            <ta e="T359" id="Seg_3731" s="T358">propr-n:case</ta>
            <ta e="T360" id="Seg_3732" s="T359">dempro-pro:(num)-pro:case</ta>
            <ta e="T361" id="Seg_3733" s="T360">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T376" id="Seg_3734" s="T375">adv</ta>
            <ta e="T377" id="Seg_3735" s="T376">que-pro:case</ta>
            <ta e="T378" id="Seg_3736" s="T377">v-v:mood.pn</ta>
            <ta e="T379" id="Seg_3737" s="T378">interj</ta>
            <ta e="T384" id="Seg_3738" s="T379">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps-PoIP">
            <ta e="T1" id="Seg_3739" s="T0">n</ta>
            <ta e="T2" id="Seg_3740" s="T1">que</ta>
            <ta e="T3" id="Seg_3741" s="T2">conj</ta>
            <ta e="T4" id="Seg_3742" s="T3">n</ta>
            <ta e="T5" id="Seg_3743" s="T4">v</ta>
            <ta e="T6" id="Seg_3744" s="T5">ptcl</ta>
            <ta e="T7" id="Seg_3745" s="T6">n</ta>
            <ta e="T8" id="Seg_3746" s="T7">adj</ta>
            <ta e="T9" id="Seg_3747" s="T8">n</ta>
            <ta e="T10" id="Seg_3748" s="T9">n</ta>
            <ta e="T11" id="Seg_3749" s="T10">v</ta>
            <ta e="T12" id="Seg_3750" s="T11">aux</ta>
            <ta e="T13" id="Seg_3751" s="T12">dempro</ta>
            <ta e="T14" id="Seg_3752" s="T13">n</ta>
            <ta e="T15" id="Seg_3753" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_3754" s="T15">dempro</ta>
            <ta e="T17" id="Seg_3755" s="T16">n</ta>
            <ta e="T18" id="Seg_3756" s="T17">n</ta>
            <ta e="T19" id="Seg_3757" s="T18">adj</ta>
            <ta e="T20" id="Seg_3758" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_3759" s="T20">dempro</ta>
            <ta e="T22" id="Seg_3760" s="T21">n</ta>
            <ta e="T23" id="Seg_3761" s="T22">v</ta>
            <ta e="T24" id="Seg_3762" s="T23">n</ta>
            <ta e="T25" id="Seg_3763" s="T24">n</ta>
            <ta e="T26" id="Seg_3764" s="T25">adj</ta>
            <ta e="T27" id="Seg_3765" s="T26">n</ta>
            <ta e="T28" id="Seg_3766" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_3767" s="T28">v</ta>
            <ta e="T30" id="Seg_3768" s="T29">v</ta>
            <ta e="T31" id="Seg_3769" s="T30">n</ta>
            <ta e="T33" id="Seg_3770" s="T32">adj</ta>
            <ta e="T34" id="Seg_3771" s="T33">n</ta>
            <ta e="T35" id="Seg_3772" s="T34">ptcl</ta>
            <ta e="T36" id="Seg_3773" s="T35">n</ta>
            <ta e="T37" id="Seg_3774" s="T36">v</ta>
            <ta e="T38" id="Seg_3775" s="T37">aux</ta>
            <ta e="T39" id="Seg_3776" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_3777" s="T39">v</ta>
            <ta e="T41" id="Seg_3778" s="T40">dempro</ta>
            <ta e="T42" id="Seg_3779" s="T41">v</ta>
            <ta e="T380" id="Seg_3780" s="T42">ptcl</ta>
            <ta e="T44" id="Seg_3781" s="T43">adv</ta>
            <ta e="T45" id="Seg_3782" s="T44">dempro</ta>
            <ta e="T46" id="Seg_3783" s="T45">n</ta>
            <ta e="T47" id="Seg_3784" s="T46">dempro</ta>
            <ta e="T48" id="Seg_3785" s="T47">adv</ta>
            <ta e="T51" id="Seg_3786" s="T50">pers</ta>
            <ta e="T53" id="Seg_3787" s="T52">v</ta>
            <ta e="T54" id="Seg_3788" s="T53">n</ta>
            <ta e="T55" id="Seg_3789" s="T54">dempro</ta>
            <ta e="T56" id="Seg_3790" s="T55">n</ta>
            <ta e="T57" id="Seg_3791" s="T56">v</ta>
            <ta e="T58" id="Seg_3792" s="T57">dempro</ta>
            <ta e="T59" id="Seg_3793" s="T58">adj</ta>
            <ta e="T60" id="Seg_3794" s="T59">ptcl</ta>
            <ta e="T61" id="Seg_3795" s="T60">que</ta>
            <ta e="T62" id="Seg_3796" s="T61">v</ta>
            <ta e="T63" id="Seg_3797" s="T62">adj</ta>
            <ta e="T64" id="Seg_3798" s="T63">dempro</ta>
            <ta e="T65" id="Seg_3799" s="T64">n</ta>
            <ta e="T66" id="Seg_3800" s="T65">dempro</ta>
            <ta e="T67" id="Seg_3801" s="T66">n</ta>
            <ta e="T68" id="Seg_3802" s="T67">v</ta>
            <ta e="T69" id="Seg_3803" s="T68">dempro</ta>
            <ta e="T70" id="Seg_3804" s="T69">v</ta>
            <ta e="T71" id="Seg_3805" s="T70">dempro</ta>
            <ta e="T72" id="Seg_3806" s="T71">n</ta>
            <ta e="T73" id="Seg_3807" s="T72">adv</ta>
            <ta e="T74" id="Seg_3808" s="T73">ptcl</ta>
            <ta e="T75" id="Seg_3809" s="T74">dempro</ta>
            <ta e="T76" id="Seg_3810" s="T75">interj</ta>
            <ta e="T77" id="Seg_3811" s="T76">n</ta>
            <ta e="T78" id="Seg_3812" s="T77">adv</ta>
            <ta e="T79" id="Seg_3813" s="T78">adv</ta>
            <ta e="T80" id="Seg_3814" s="T79">adv</ta>
            <ta e="T81" id="Seg_3815" s="T80">ptcl</ta>
            <ta e="T82" id="Seg_3816" s="T81">que</ta>
            <ta e="T83" id="Seg_3817" s="T82">dempro</ta>
            <ta e="T84" id="Seg_3818" s="T83">propr</ta>
            <ta e="T85" id="Seg_3819" s="T84">ptcl</ta>
            <ta e="T86" id="Seg_3820" s="T85">ptcl</ta>
            <ta e="T87" id="Seg_3821" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_3822" s="T87">n</ta>
            <ta e="T89" id="Seg_3823" s="T88">dempro</ta>
            <ta e="T90" id="Seg_3824" s="T89">que</ta>
            <ta e="T91" id="Seg_3825" s="T90">n</ta>
            <ta e="T92" id="Seg_3826" s="T91">pers</ta>
            <ta e="T93" id="Seg_3827" s="T92">dempro</ta>
            <ta e="T94" id="Seg_3828" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_3829" s="T94">v</ta>
            <ta e="T104" id="Seg_3830" s="T103">v</ta>
            <ta e="T105" id="Seg_3831" s="T104">adv</ta>
            <ta e="T106" id="Seg_3832" s="T105">dempro</ta>
            <ta e="T107" id="Seg_3833" s="T106">propr</ta>
            <ta e="T108" id="Seg_3834" s="T107">ptcl</ta>
            <ta e="T109" id="Seg_3835" s="T108">v</ta>
            <ta e="T110" id="Seg_3836" s="T109">dempro</ta>
            <ta e="T111" id="Seg_3837" s="T110">dempro</ta>
            <ta e="T112" id="Seg_3838" s="T111">dempro</ta>
            <ta e="T113" id="Seg_3839" s="T112">pers</ta>
            <ta e="T114" id="Seg_3840" s="T113">n</ta>
            <ta e="T115" id="Seg_3841" s="T114">n</ta>
            <ta e="T116" id="Seg_3842" s="T115">ptcl</ta>
            <ta e="T117" id="Seg_3843" s="T116">n</ta>
            <ta e="T118" id="Seg_3844" s="T117">propr</ta>
            <ta e="T119" id="Seg_3845" s="T118">propr</ta>
            <ta e="T120" id="Seg_3846" s="T119">n</ta>
            <ta e="T121" id="Seg_3847" s="T120">adj</ta>
            <ta e="T125" id="Seg_3848" s="T124">dempro</ta>
            <ta e="T126" id="Seg_3849" s="T125">dempro</ta>
            <ta e="T127" id="Seg_3850" s="T126">n</ta>
            <ta e="T128" id="Seg_3851" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_3852" s="T128">v</ta>
            <ta e="T130" id="Seg_3853" s="T129">aux</ta>
            <ta e="T131" id="Seg_3854" s="T130">v</ta>
            <ta e="T132" id="Seg_3855" s="T131">adv</ta>
            <ta e="T133" id="Seg_3856" s="T132">dempro</ta>
            <ta e="T134" id="Seg_3857" s="T133">n</ta>
            <ta e="T135" id="Seg_3858" s="T134">ptcl</ta>
            <ta e="T136" id="Seg_3859" s="T135">n</ta>
            <ta e="T137" id="Seg_3860" s="T136">adj</ta>
            <ta e="T138" id="Seg_3861" s="T137">cop</ta>
            <ta e="T139" id="Seg_3862" s="T138">n</ta>
            <ta e="T140" id="Seg_3863" s="T139">v</ta>
            <ta e="T141" id="Seg_3864" s="T140">ptcl</ta>
            <ta e="T142" id="Seg_3865" s="T141">que</ta>
            <ta e="T143" id="Seg_3866" s="T142">cop</ta>
            <ta e="T144" id="Seg_3867" s="T143">adj</ta>
            <ta e="T145" id="Seg_3868" s="T144">n</ta>
            <ta e="T146" id="Seg_3869" s="T145">cop</ta>
            <ta e="T147" id="Seg_3870" s="T146">dempro</ta>
            <ta e="T148" id="Seg_3871" s="T147">n</ta>
            <ta e="T149" id="Seg_3872" s="T148">v</ta>
            <ta e="T150" id="Seg_3873" s="T149">interj</ta>
            <ta e="T151" id="Seg_3874" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_3875" s="T151">conj</ta>
            <ta e="T153" id="Seg_3876" s="T152">adj</ta>
            <ta e="T154" id="Seg_3877" s="T153">ptcl</ta>
            <ta e="T155" id="Seg_3878" s="T154">ptcl</ta>
            <ta e="T156" id="Seg_3879" s="T155">conj</ta>
            <ta e="T157" id="Seg_3880" s="T156">pers</ta>
            <ta e="T158" id="Seg_3881" s="T157">v</ta>
            <ta e="T159" id="Seg_3882" s="T158">dempro</ta>
            <ta e="T160" id="Seg_3883" s="T159">dempro</ta>
            <ta e="T161" id="Seg_3884" s="T160">que</ta>
            <ta e="T162" id="Seg_3885" s="T161">v</ta>
            <ta e="T163" id="Seg_3886" s="T162">ptcl</ta>
            <ta e="T164" id="Seg_3887" s="T163">adv</ta>
            <ta e="T168" id="Seg_3888" s="T167">adv</ta>
            <ta e="T169" id="Seg_3889" s="T168">ptcl</ta>
            <ta e="T170" id="Seg_3890" s="T169">adv</ta>
            <ta e="T171" id="Seg_3891" s="T170">adv</ta>
            <ta e="T172" id="Seg_3892" s="T171">adj</ta>
            <ta e="T173" id="Seg_3893" s="T172">adj</ta>
            <ta e="T174" id="Seg_3894" s="T173">dempro</ta>
            <ta e="T175" id="Seg_3895" s="T174">n</ta>
            <ta e="T176" id="Seg_3896" s="T175">v</ta>
            <ta e="T177" id="Seg_3897" s="T176">n</ta>
            <ta e="T178" id="Seg_3898" s="T177">dempro</ta>
            <ta e="T179" id="Seg_3899" s="T178">post</ta>
            <ta e="T180" id="Seg_3900" s="T179">dempro</ta>
            <ta e="T181" id="Seg_3901" s="T180">ptcl</ta>
            <ta e="T190" id="Seg_3902" s="T189">ptcl</ta>
            <ta e="T191" id="Seg_3903" s="T190">adv</ta>
            <ta e="T192" id="Seg_3904" s="T191">adj</ta>
            <ta e="T193" id="Seg_3905" s="T192">v</ta>
            <ta e="T194" id="Seg_3906" s="T193">n</ta>
            <ta e="T195" id="Seg_3907" s="T194">n</ta>
            <ta e="T196" id="Seg_3908" s="T195">ptcl</ta>
            <ta e="T197" id="Seg_3909" s="T196">n</ta>
            <ta e="T198" id="Seg_3910" s="T197">ptcl</ta>
            <ta e="T199" id="Seg_3911" s="T198">adj</ta>
            <ta e="T201" id="Seg_3912" s="T200">adj</ta>
            <ta e="T202" id="Seg_3913" s="T201">n</ta>
            <ta e="T203" id="Seg_3914" s="T202">dempro</ta>
            <ta e="T204" id="Seg_3915" s="T203">n</ta>
            <ta e="T205" id="Seg_3916" s="T204">dempro</ta>
            <ta e="T206" id="Seg_3917" s="T205">ptcl</ta>
            <ta e="T207" id="Seg_3918" s="T206">v</ta>
            <ta e="T208" id="Seg_3919" s="T207">dempro</ta>
            <ta e="T209" id="Seg_3920" s="T208">n</ta>
            <ta e="T210" id="Seg_3921" s="T209">dempro</ta>
            <ta e="T211" id="Seg_3922" s="T210">adj</ta>
            <ta e="T212" id="Seg_3923" s="T211">n</ta>
            <ta e="T213" id="Seg_3924" s="T212">ptcl</ta>
            <ta e="T214" id="Seg_3925" s="T213">ptcl</ta>
            <ta e="T215" id="Seg_3926" s="T214">v</ta>
            <ta e="T216" id="Seg_3927" s="T215">ptcl</ta>
            <ta e="T217" id="Seg_3928" s="T216">dempro</ta>
            <ta e="T218" id="Seg_3929" s="T217">n</ta>
            <ta e="T219" id="Seg_3930" s="T218">quant</ta>
            <ta e="T220" id="Seg_3931" s="T219">n</ta>
            <ta e="T221" id="Seg_3932" s="T220">v</ta>
            <ta e="T231" id="Seg_3933" s="T227">ptcl</ta>
            <ta e="T233" id="Seg_3934" s="T232">ptcl</ta>
            <ta e="T234" id="Seg_3935" s="T233">ptcl</ta>
            <ta e="T235" id="Seg_3936" s="T234">ptcl</ta>
            <ta e="T236" id="Seg_3937" s="T235">ptcl</ta>
            <ta e="T237" id="Seg_3938" s="T236">dempro</ta>
            <ta e="T238" id="Seg_3939" s="T237">dempro</ta>
            <ta e="T239" id="Seg_3940" s="T238">n</ta>
            <ta e="T240" id="Seg_3941" s="T239">dempro</ta>
            <ta e="T241" id="Seg_3942" s="T240">n</ta>
            <ta e="T242" id="Seg_3943" s="T241">ptcl</ta>
            <ta e="T243" id="Seg_3944" s="T242">adj</ta>
            <ta e="T244" id="Seg_3945" s="T243">ptcl</ta>
            <ta e="T245" id="Seg_3946" s="T244">dempro</ta>
            <ta e="T246" id="Seg_3947" s="T245">n</ta>
            <ta e="T247" id="Seg_3948" s="T246">n</ta>
            <ta e="T248" id="Seg_3949" s="T247">adj</ta>
            <ta e="T97" id="Seg_3950" s="T248">interj</ta>
            <ta e="T249" id="Seg_3951" s="T97">adv</ta>
            <ta e="T250" id="Seg_3952" s="T249">adj</ta>
            <ta e="T251" id="Seg_3953" s="T250">cop</ta>
            <ta e="T252" id="Seg_3954" s="T251">aux</ta>
            <ta e="T253" id="Seg_3955" s="T252">pers</ta>
            <ta e="T254" id="Seg_3956" s="T253">dempro</ta>
            <ta e="T255" id="Seg_3957" s="T254">emphpro</ta>
            <ta e="T256" id="Seg_3958" s="T255">ptcl</ta>
            <ta e="T257" id="Seg_3959" s="T256">adv</ta>
            <ta e="T258" id="Seg_3960" s="T257">adv</ta>
            <ta e="T259" id="Seg_3961" s="T258">n</ta>
            <ta e="T261" id="Seg_3962" s="T259">v</ta>
            <ta e="T262" id="Seg_3963" s="T261">n</ta>
            <ta e="T263" id="Seg_3964" s="T262">adj</ta>
            <ta e="T264" id="Seg_3965" s="T263">ptcl</ta>
            <ta e="T265" id="Seg_3966" s="T264">interj</ta>
            <ta e="T266" id="Seg_3967" s="T265">ptcl</ta>
            <ta e="T267" id="Seg_3968" s="T266">dempro</ta>
            <ta e="T268" id="Seg_3969" s="T267">n</ta>
            <ta e="T269" id="Seg_3970" s="T268">v</ta>
            <ta e="T270" id="Seg_3971" s="T269">adj</ta>
            <ta e="T271" id="Seg_3972" s="T270">ptcl</ta>
            <ta e="T272" id="Seg_3973" s="T271">cardnum</ta>
            <ta e="T273" id="Seg_3974" s="T272">adj</ta>
            <ta e="T274" id="Seg_3975" s="T273">dempro</ta>
            <ta e="T275" id="Seg_3976" s="T274">n</ta>
            <ta e="T276" id="Seg_3977" s="T275">dempro</ta>
            <ta e="T277" id="Seg_3978" s="T276">ptcl</ta>
            <ta e="T278" id="Seg_3979" s="T277">adv</ta>
            <ta e="T279" id="Seg_3980" s="T278">n</ta>
            <ta e="T280" id="Seg_3981" s="T279">n</ta>
            <ta e="T281" id="Seg_3982" s="T280">que</ta>
            <ta e="T282" id="Seg_3983" s="T281">v</ta>
            <ta e="T283" id="Seg_3984" s="T282">aux</ta>
            <ta e="T284" id="Seg_3985" s="T283">ptcl</ta>
            <ta e="T285" id="Seg_3986" s="T284">ptcl</ta>
            <ta e="T289" id="Seg_3987" s="T288">adv</ta>
            <ta e="T290" id="Seg_3988" s="T289">dempro</ta>
            <ta e="T299" id="Seg_3989" s="T298">adv</ta>
            <ta e="T300" id="Seg_3990" s="T299">v</ta>
            <ta e="T303" id="Seg_3991" s="T300">aux</ta>
            <ta e="T304" id="Seg_3992" s="T303">ptcl</ta>
            <ta e="T305" id="Seg_3993" s="T304">adv</ta>
            <ta e="T306" id="Seg_3994" s="T305">adv</ta>
            <ta e="T307" id="Seg_3995" s="T306">ordnum</ta>
            <ta e="T308" id="Seg_3996" s="T307">v</ta>
            <ta e="T309" id="Seg_3997" s="T308">ptcl</ta>
            <ta e="T310" id="Seg_3998" s="T309">dempro</ta>
            <ta e="T311" id="Seg_3999" s="T310">v</ta>
            <ta e="T312" id="Seg_4000" s="T311">ptcl</ta>
            <ta e="T313" id="Seg_4001" s="T312">n</ta>
            <ta e="T314" id="Seg_4002" s="T313">post</ta>
            <ta e="T315" id="Seg_4003" s="T314">adv</ta>
            <ta e="T316" id="Seg_4004" s="T315">adv</ta>
            <ta e="T317" id="Seg_4005" s="T316">adv</ta>
            <ta e="T318" id="Seg_4006" s="T317">adv</ta>
            <ta e="T319" id="Seg_4007" s="T318">ptcl</ta>
            <ta e="T320" id="Seg_4008" s="T319">adv</ta>
            <ta e="T321" id="Seg_4009" s="T320">v</ta>
            <ta e="T322" id="Seg_4010" s="T321">dempro</ta>
            <ta e="T323" id="Seg_4011" s="T322">dempro</ta>
            <ta e="T324" id="Seg_4012" s="T323">n</ta>
            <ta e="T325" id="Seg_4013" s="T324">v</ta>
            <ta e="T326" id="Seg_4014" s="T325">ptcl</ta>
            <ta e="T327" id="Seg_4015" s="T326">n</ta>
            <ta e="T328" id="Seg_4016" s="T327">v</ta>
            <ta e="T329" id="Seg_4017" s="T328">interj</ta>
            <ta e="T330" id="Seg_4018" s="T329">pers</ta>
            <ta e="T331" id="Seg_4019" s="T330">v</ta>
            <ta e="T332" id="Seg_4020" s="T331">aux</ta>
            <ta e="T333" id="Seg_4021" s="T332">interj</ta>
            <ta e="T334" id="Seg_4022" s="T333">adv</ta>
            <ta e="T335" id="Seg_4023" s="T334">n</ta>
            <ta e="T336" id="Seg_4024" s="T335">adv</ta>
            <ta e="T337" id="Seg_4025" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_4026" s="T337">v</ta>
            <ta e="T339" id="Seg_4027" s="T338">adv</ta>
            <ta e="T340" id="Seg_4028" s="T339">v</ta>
            <ta e="T341" id="Seg_4029" s="T340">v</ta>
            <ta e="T342" id="Seg_4030" s="T341">v</ta>
            <ta e="T343" id="Seg_4031" s="T342">ptcl</ta>
            <ta e="T344" id="Seg_4032" s="T343">dempro</ta>
            <ta e="T345" id="Seg_4033" s="T344">adj</ta>
            <ta e="T346" id="Seg_4034" s="T345">n</ta>
            <ta e="T347" id="Seg_4035" s="T346">v</ta>
            <ta e="T348" id="Seg_4036" s="T347">pers</ta>
            <ta e="T349" id="Seg_4037" s="T348">que</ta>
            <ta e="T350" id="Seg_4038" s="T349">v</ta>
            <ta e="T351" id="Seg_4039" s="T350">ptcl</ta>
            <ta e="T352" id="Seg_4040" s="T351">interj</ta>
            <ta e="T353" id="Seg_4041" s="T352">propr</ta>
            <ta e="T354" id="Seg_4042" s="T353">que</ta>
            <ta e="T355" id="Seg_4043" s="T354">propr</ta>
            <ta e="T356" id="Seg_4044" s="T355">v</ta>
            <ta e="T357" id="Seg_4045" s="T356">aux</ta>
            <ta e="T358" id="Seg_4046" s="T357">propr</ta>
            <ta e="T359" id="Seg_4047" s="T358">propr</ta>
            <ta e="T360" id="Seg_4048" s="T359">dempro</ta>
            <ta e="T361" id="Seg_4049" s="T360">v</ta>
            <ta e="T376" id="Seg_4050" s="T375">adv</ta>
            <ta e="T377" id="Seg_4051" s="T376">que</ta>
            <ta e="T378" id="Seg_4052" s="T377">v</ta>
            <ta e="T379" id="Seg_4053" s="T378">interj</ta>
            <ta e="T384" id="Seg_4054" s="T379">dempro</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-PoIP" />
         <annotation name="SyF" tierref="SyF-PoIP" />
         <annotation name="IST" tierref="IST-PoIP" />
         <annotation name="Top" tierref="Top-PoIP" />
         <annotation name="Foc" tierref="Foc-PoIP" />
         <annotation name="BOR" tierref="BOR-PoIP">
            <ta e="T1" id="Seg_4055" s="T0">RUS:cult</ta>
            <ta e="T3" id="Seg_4056" s="T2">RUS:gram</ta>
            <ta e="T4" id="Seg_4057" s="T3">RUS:cult</ta>
            <ta e="T31" id="Seg_4058" s="T30">RUS:cult</ta>
            <ta e="T44" id="Seg_4059" s="T43">RUS:mod</ta>
            <ta e="T77" id="Seg_4060" s="T76">EV:core</ta>
            <ta e="T88" id="Seg_4061" s="T87">EV:core</ta>
            <ta e="T132" id="Seg_4062" s="T131">RUS:mod</ta>
            <ta e="T151" id="Seg_4063" s="T150">RUS:disc</ta>
            <ta e="T152" id="Seg_4064" s="T151">RUS:gram</ta>
            <ta e="T155" id="Seg_4065" s="T154">RUS:disc</ta>
            <ta e="T156" id="Seg_4066" s="T155">RUS:gram</ta>
            <ta e="T175" id="Seg_4067" s="T174">EV:cult</ta>
            <ta e="T179" id="Seg_4068" s="T178">EV:gram (DIM)</ta>
            <ta e="T194" id="Seg_4069" s="T193">RUS:cult</ta>
            <ta e="T195" id="Seg_4070" s="T194">RUS:cult</ta>
            <ta e="T239" id="Seg_4071" s="T238">EV:core</ta>
            <ta e="T262" id="Seg_4072" s="T261">EV:core</ta>
            <ta e="T263" id="Seg_4073" s="T262">RUS:cult</ta>
            <ta e="T279" id="Seg_4074" s="T278">RUS:cult</ta>
            <ta e="T299" id="Seg_4075" s="T298">EV:gram (DIM)</ta>
            <ta e="T305" id="Seg_4076" s="T304">EV:gram (DIM)</ta>
            <ta e="T318" id="Seg_4077" s="T317">RUS:core</ta>
            <ta e="T335" id="Seg_4078" s="T334">RUS:cult</ta>
            <ta e="T336" id="Seg_4079" s="T335">RUS:core</ta>
            <ta e="T346" id="Seg_4080" s="T345">RUS:cult</ta>
            <ta e="T353" id="Seg_4081" s="T352">RUS:cult</ta>
            <ta e="T355" id="Seg_4082" s="T354">RUS:cult</ta>
            <ta e="T358" id="Seg_4083" s="T357">RUS:cult</ta>
            <ta e="T359" id="Seg_4084" s="T358">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-PoIP" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-PoIP" />
         <annotation name="CS" tierref="CS-PoIP" />
         <annotation name="fe" tierref="fe-PoIP">
            <ta e="T17" id="Seg_4085" s="T0">The Nganasan whatever… and the Nganasan put every year when they nomadize a reindeer fur on that stone, for that old man.</ta>
            <ta e="T35" id="Seg_4086" s="T17">Every year those furs were rotting on the ground, on the ground, and the next year they come nomadizing again, the Nganasans, again another fur, again.</ta>
            <ta e="T380" id="Seg_4087" s="T35">After they killed a reindeer, they put it again, look.</ta>
            <ta e="T57" id="Seg_4088" s="T43">Generally on that spot, really… I travelled everywhere me old woman, who had come to this land.</ta>
            <ta e="T73" id="Seg_4089" s="T57">On the high side where can we go, on the high side? Well, to the old man I came, I nomadized, to the old man.</ta>
            <ta e="T95" id="Seg_4090" s="T73">Well, oh dear, friend, indeed indeed, and then, whatchamacallit, in Tonuskij they say there is stone, my friend, I did not see it.</ta>
            <ta e="T121" id="Seg_4091" s="T103">I didn't see it there, they say there is in Tonuskij, the father of my husband, the old man Lentej.</ta>
            <ta e="T132" id="Seg_4092" s="T124">He kicked that stone accidentally, kicked it accidentally.</ta>
            <ta e="T146" id="Seg_4093" s="T132">That stone was fishy, it gives fish, also there was whatchamacallit, it was a stone with a spirit.</ta>
            <ta e="T164" id="Seg_4094" s="T146">From the earth it was not visible, it is snall they say, I didn't see it, there there somewhere they said, there.</ta>
            <ta e="T181" id="Seg_4095" s="T167">There, yes there there close by, close to the place where the Njunguhu live, in that direction it is.</ta>
            <ta e="T202" id="Seg_4096" s="T189">Yes, there you throw everything, presents, maybe cartridges, maybe buttons, everything, waistcoats, all kind of things.</ta>
            <ta e="T218" id="Seg_4097" s="T202">That old man kicked it, the stone, the old man though, look, with his foot.</ta>
            <ta e="T221" id="Seg_4098" s="T218">He lied there several days.</ta>
            <ta e="T239" id="Seg_4099" s="T227">Ah you heard it, that's how it is, that's how it is, friend.</ta>
            <ta e="T248" id="Seg_4100" s="T239">That place, those places with a spirit are scary.</ta>
            <ta e="T261" id="Seg_4101" s="T248">Mmm indeed all kinds of things happen, we (for ourselves?) left many presents.</ta>
            <ta e="T275" id="Seg_4102" s="T261">Friend, it is interesting those letters lie locked with two locks [in a] box.</ta>
            <ta e="T290" id="Seg_4103" s="T275">Long ago they used to find those tea boxes somewhere, well, well, there were those boxes then.</ta>
            <ta e="T305" id="Seg_4104" s="T298">Like this it is closed, yeah like this.</ta>
            <ta e="T312" id="Seg_4105" s="T305">Then you close the second one and such a thing doesn't break.</ta>
            <ta e="T328" id="Seg_4106" s="T312">Like a shelf, like this, like this, it is really beautiful, he found it, as if it was made by a God, the earth stands (?).</ta>
            <ta e="T337" id="Seg_4107" s="T328">Oh dear, I said, the city is really beautiful.</ta>
            <ta e="T347" id="Seg_4108" s="T337">When we returned there we did not go by when we returned, when we went there we only threw all presents.</ta>
            <ta e="T351" id="Seg_4109" s="T347">Whom did I tell?</ta>
            <ta e="T357" id="Seg_4110" s="T351">Aaah I told Tatjana Ivanovna.</ta>
            <ta e="T361" id="Seg_4111" s="T357">I told Tatjana Ivanovna those things.</ta>
            <ta e="T384" id="Seg_4112" s="T375">Then ask something, oh dear…</ta>
         </annotation>
         <annotation name="fg" tierref="fg-PoIP">
            <ta e="T17" id="Seg_4113" s="T0">Die Nganasanen Dings, wenn die Nganasanen nomadisieren, legen sie jedes Jahr ein Rentierfell auf den Stein, für den alten Mann.</ta>
            <ta e="T35" id="Seg_4114" s="T17">Jedes Jahr verfaulen diese Felle auf der Erde, im nächsten Jahr kommen sie wieder nomadisierend, die Nganasanen, wieder ein anderes Fell, wieder.</ta>
            <ta e="T380" id="Seg_4115" s="T35">Nachdem sie ein Rentier getötet haben, legen sie [es] auch hin, schau.</ta>
            <ta e="T57" id="Seg_4116" s="T43">Überhaupt an diesem Ort, ich bin wirklich eine Frau, die überall hingefahren ist, die in dieses Land gekommen ist.</ta>
            <ta e="T73" id="Seg_4117" s="T57">Da oben, wo können wir hingehen, da oben, nun ich kam zu dem meinem alten Mann, ich nomadisierte, zu meinem alten Mann.</ta>
            <ta e="T95" id="Seg_4118" s="T73">Nun, oh nein, Freund, wirklich, und dann Dings, in Tonuskij, sagt man, gibt es einen Stein, mein Freund, ich habe ihn nicht gesehen.</ta>
            <ta e="T121" id="Seg_4119" s="T103">Ich habe [ihn] dort nicht gesehen, sie sagen, er ist dort in Tonuskij, der Vater meines Mannes, der alte Mann Lentej.</ta>
            <ta e="T132" id="Seg_4120" s="T124">Der trat den Stein offenbar, er trat [ihn] ausversehen.</ta>
            <ta e="T146" id="Seg_4121" s="T132">Der Stein war offenbar fischig, er gibt Fisch, und er war Dings, es war ein Stein mit Geist. </ta>
            <ta e="T164" id="Seg_4122" s="T146">Von der Erde ist er nicht zu sehen, so klein ist er, sagt man, ich habe ihn nicht gesehen, dort dort irgendwo sagten sie.</ta>
            <ta e="T181" id="Seg_4123" s="T167">Dort, ja, dort dort nahe, nahe zu den Orten, wo die Njuoguhut leben, in die Richtung ist es.</ta>
            <ta e="T202" id="Seg_4124" s="T189">Ja, du wirfst da alles mögliche hin, Geschenke, vielleicht eine Patrone, vielleicht einen Knopf, alles, eine Weste, unterschiedliche Sachen.</ta>
            <ta e="T218" id="Seg_4125" s="T202">Der alte Mann trat ihn, den Stein, der alte Mann also, schau, mit seinem Fuß.</ta>
            <ta e="T221" id="Seg_4126" s="T218">Einige Tage lag er [da].</ta>
            <ta e="T239" id="Seg_4127" s="T227">Ah, du hast es gehört, so ist es, mein Freund.</ta>
            <ta e="T248" id="Seg_4128" s="T239">Dieser Ort, diese Orte, die einen Geist haben, sind furchteinflößend.</ta>
            <ta e="T261" id="Seg_4129" s="T248">Hm, alles mögliche passiert, wir haben (für uns selbst?) Geschenke dort gelassen.</ta>
            <ta e="T275" id="Seg_4130" s="T261">Freund, es ist interessant, diese Briefe liegen mit einem Deckel, mit zwei Deckeln [in einer] Kiste.</ta>
            <ta e="T290" id="Seg_4131" s="T275">Früher, wo man Teekisten irgendwo fand, nun, nun, da waren diese Kisten.</ta>
            <ta e="T305" id="Seg_4132" s="T298">So schließt man es, so.</ta>
            <ta e="T312" id="Seg_4133" s="T305">Dann schließt du das zweite und so [ein Ding] geht nicht kaputt.</ta>
            <ta e="T328" id="Seg_4134" s="T312">Wie Regale, so, so, wirklich sehr schön, er fand es, als ob es von Gott gemacht sei, die Erde steht (?).</ta>
            <ta e="T337" id="Seg_4135" s="T328">Oh nein, ich sagte, die Stadt ist wirklich schön.</ta>
            <ta e="T347" id="Seg_4136" s="T337">Als wir zurückkamen, fassten wir es nicht an, als wir zurückkamen, als wir hingingen, warfen wir nur alle Geschenke hin. </ta>
            <ta e="T351" id="Seg_4137" s="T347">Wem habe ich das erzählt?</ta>
            <ta e="T357" id="Seg_4138" s="T351">Ah, Tatjana Dings, Ivanovna habe ich das wohl erzählt.</ta>
            <ta e="T361" id="Seg_4139" s="T357">Tatjana Ivanovna habe ich diese Sachen erzählt.</ta>
            <ta e="T384" id="Seg_4140" s="T375">Dann frag etwas, oh nein...</ta>
         </annotation>
         <annotation name="fr" tierref="fr-PoIP" />
         <annotation name="ltr" tierref="ltr-PoIP">
            <ta e="T17" id="Seg_4141" s="T0">Нганасаны всякие, а ….. при кочевье каждый год оленью шкуру надевали оказывается на этот камень да этому старику.</ta>
            <ta e="T35" id="Seg_4142" s="T17">Каждый год эти шкуры гнили на земле, на земле следующий год снова кочуют приходят …. опять другую шкуру опять.</ta>
            <ta e="T380" id="Seg_4143" s="T35">Оленя забивали опять надевали смотри ка.</ta>
            <ta e="T57" id="Seg_4144" s="T43">Вообщем на той земле это правда ч… я везде побывала бабушка на эту землю приехав.</ta>
            <ta e="T73" id="Seg_4145" s="T57">Там на верховье куда ходить на верху вот к старику приехала вот и кочевала к старику совсем.</ta>
            <ta e="T95" id="Seg_4146" s="T73">Вот араа друг правда правда потом бывало это это в Тонуском тоже есть вроде друг такой таас, я его не видела.</ta>
            <ta e="T121" id="Seg_4147" s="T103">Не видела там это в Тонуском есть говорят это моего мужа отец, Лэӈтэй Лэӈтэй дедушка старый.</ta>
            <ta e="T132" id="Seg_4148" s="T124">Вот он этот камень пнул оказывается пнул случайно.</ta>
            <ta e="T146" id="Seg_4149" s="T132">Этот камень рыбий оказывается, рыбу дает, тоже это будет с духом камень оказывается.</ta>
            <ta e="T164" id="Seg_4150" s="T146">А его с земли не видать так то маленький оказывается так то я не видела,вот вот где-то говорили там.</ta>
            <ta e="T181" id="Seg_4151" s="T167">Б: там да там, там близко, близко от Нёнухуты живут место не далеко там есть.</ta>
            <ta e="T202" id="Seg_4152" s="T189">Б: да туда всё кидают подарки, патрон, пуговицу всё жилет всяко-разные вещи …</ta>
            <ta e="T218" id="Seg_4153" s="T202">Тот дедушка вот пнул этот камень старый дедушка хоть смотри-ка тот ногой.</ta>
            <ta e="T221" id="Seg_4154" s="T218">Много дней лежал.</ta>
            <ta e="T239" id="Seg_4155" s="T227">Б: да слышала да вот вот вот вот так вот подруга.</ta>
            <ta e="T248" id="Seg_4156" s="T239">Эти земли с духом земли человеку страшно.</ta>
            <ta e="T261" id="Seg_4157" s="T248">Ммм правда всяко-разное бывает, мы …. лично правда много подарков оставили.</ta>
            <ta e="T275" id="Seg_4158" s="T261">Подруга интересно так да эти письма лежат закрытыми два замка в сундуке.</ta>
            <ta e="T290" id="Seg_4159" s="T275">Эти раньше от чая коробки откуда находили, да да такие коробки был потом.</ta>
            <ta e="T305" id="Seg_4160" s="T298">Б: вот так вот закрывали да вот так вот.</ta>
            <ta e="T312" id="Seg_4161" s="T305">Потом второй закрываешь вот и такой не ломался.</ta>
            <ta e="T328" id="Seg_4162" s="T312">Полка как вот так, вот так правда красиво да правда нашел, вот боженька сделал вроде земля стоит (?).</ta>
            <ta e="T337" id="Seg_4163" s="T328">Я говорила правда город красивый.</ta>
            <ta e="T347" id="Seg_4164" s="T337">Возвращаясь туда не заезжали возвращаясь, когда ехали туда только все подарки кидали.</ta>
            <ta e="T361" id="Seg_4165" s="T357">Б: я кому рааскзавала то? а Татьяне этой, Ивановне рассказывала оказывается. да Татьяне Ивановне эти рассказывала.</ta>
            <ta e="T384" id="Seg_4166" s="T375">Б: ещё что спроси …</ta>
         </annotation>
         <annotation name="nt" tierref="nt-PoIP">
            <ta e="T384" id="Seg_4167" s="T375">[DCh]: Not clear why IMP.1DU here.</ta>
         </annotation>
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-ErAI"
                      id="tx-ErAI"
                      speaker="ErAI"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-ErAI">
            <ts e="T94" id="Seg_4168" n="sc" s="T91">
               <ts e="T94" id="Seg_4170" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_4172" n="HIAT:w" s="T91">Iti</ts>
                  <nts id="Seg_4173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_4175" n="HIAT:w" s="T92">taːs</ts>
                  <nts id="Seg_4176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_4178" n="HIAT:w" s="T93">heː</ts>
                  <nts id="Seg_4179" n="HIAT:ip">.</nts>
                  <nts id="Seg_4180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T103" id="Seg_4181" n="sc" s="T96">
               <ts e="T103" id="Seg_4183" n="HIAT:u" s="T96">
                  <ts e="T99" id="Seg_4185" n="HIAT:w" s="T96">Min</ts>
                  <nts id="Seg_4186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_4188" n="HIAT:w" s="T99">ol</ts>
                  <nts id="Seg_4189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_4191" n="HIAT:w" s="T100">baraːrɨ</ts>
                  <nts id="Seg_4192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_4194" n="HIAT:w" s="T101">gɨnar</ts>
                  <nts id="Seg_4195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_4197" n="HIAT:w" s="T102">etim</ts>
                  <nts id="Seg_4198" n="HIAT:ip">.</nts>
                  <nts id="Seg_4199" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T119" id="Seg_4200" n="sc" s="T116">
               <ts e="T119" id="Seg_4202" n="HIAT:u" s="T116">
                  <ts e="T117" id="Seg_4204" n="HIAT:w" s="T116">Eː</ts>
                  <nts id="Seg_4205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_4207" n="HIAT:w" s="T117">Leŋtej</ts>
                  <nts id="Seg_4208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_4210" n="HIAT:w" s="T118">ogonnʼor</ts>
                  <nts id="Seg_4211" n="HIAT:ip">.</nts>
                  <nts id="Seg_4212" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T170" id="Seg_4213" n="sc" s="T164">
               <ts e="T170" id="Seg_4215" n="HIAT:u" s="T164">
                  <ts e="T165" id="Seg_4217" n="HIAT:w" s="T164">Dʼe</ts>
                  <nts id="Seg_4218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_4220" n="HIAT:w" s="T165">iti</ts>
                  <nts id="Seg_4221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_4223" n="HIAT:w" s="T166">min</ts>
                  <nts id="Seg_4224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_4226" n="HIAT:w" s="T167">baraːrɨ</ts>
                  <nts id="Seg_4227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_4229" n="HIAT:w" s="T168">gɨnabɨn</ts>
                  <nts id="Seg_4230" n="HIAT:ip">.</nts>
                  <nts id="Seg_4231" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T189" id="Seg_4232" n="sc" s="T183">
               <ts e="T189" id="Seg_4234" n="HIAT:u" s="T183">
                  <ts e="T184" id="Seg_4236" n="HIAT:w" s="T183">Onno</ts>
                  <nts id="Seg_4237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_4239" n="HIAT:w" s="T184">emi͡e</ts>
                  <nts id="Seg_4240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_4242" n="HIAT:w" s="T185">uːrallar</ts>
                  <nts id="Seg_4243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_4245" n="HIAT:w" s="T186">ühü</ts>
                  <nts id="Seg_4246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_4248" n="HIAT:w" s="T187">bu͡o</ts>
                  <nts id="Seg_4249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_4251" n="HIAT:w" s="T188">barɨtɨn</ts>
                  <nts id="Seg_4252" n="HIAT:ip">?</nts>
                  <nts id="Seg_4253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T230" id="Seg_4254" n="sc" s="T221">
               <ts e="T230" id="Seg_4256" n="HIAT:u" s="T221">
                  <ts e="T222" id="Seg_4258" n="HIAT:w" s="T221">Dʼe</ts>
                  <nts id="Seg_4259" n="HIAT:ip">,</nts>
                  <nts id="Seg_4260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_4262" n="HIAT:w" s="T222">dʼe</ts>
                  <nts id="Seg_4263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_4265" n="HIAT:w" s="T223">iti</ts>
                  <nts id="Seg_4266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_4268" n="HIAT:w" s="T224">öhü</ts>
                  <nts id="Seg_4269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_4271" n="HIAT:w" s="T225">istibitim</ts>
                  <nts id="Seg_4272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_4274" n="HIAT:w" s="T226">min</ts>
                  <nts id="Seg_4275" n="HIAT:ip">,</nts>
                  <nts id="Seg_4276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_4278" n="HIAT:w" s="T227">min</ts>
                  <nts id="Seg_4279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_4281" n="HIAT:w" s="T228">emi͡e</ts>
                  <nts id="Seg_4282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_4284" n="HIAT:w" s="T229">oččuguj</ts>
                  <nts id="Seg_4285" n="HIAT:ip">.</nts>
                  <nts id="Seg_4286" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T290" id="Seg_4287" n="sc" s="T280">
               <ts e="T290" id="Seg_4289" n="HIAT:u" s="T280">
                  <ts e="T281" id="Seg_4291" n="HIAT:w" s="T280">Amerikanskijdar</ts>
                  <nts id="Seg_4292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_4294" n="HIAT:w" s="T281">itte</ts>
                  <nts id="Seg_4295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_4297" n="HIAT:w" s="T282">keleːčči</ts>
                  <nts id="Seg_4298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_4300" n="HIAT:w" s="T283">egeleːčči</ts>
                  <nts id="Seg_4301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_4303" n="HIAT:w" s="T284">etilere</ts>
                  <nts id="Seg_4304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_4306" n="HIAT:w" s="T285">čaːjkaːttarɨ</ts>
                  <nts id="Seg_4307" n="HIAT:ip">.</nts>
                  <nts id="Seg_4308" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T300" id="Seg_4309" n="sc" s="T296">
               <ts e="T300" id="Seg_4311" n="HIAT:u" s="T296">
                  <ts e="T297" id="Seg_4313" n="HIAT:w" s="T296">Bihi͡eke</ts>
                  <nts id="Seg_4314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_4316" n="HIAT:w" s="T297">bu͡olaːččɨ</ts>
                  <nts id="Seg_4317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_4319" n="HIAT:w" s="T298">ete</ts>
                  <nts id="Seg_4320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_4322" n="HIAT:w" s="T299">heː</ts>
                  <nts id="Seg_4323" n="HIAT:ip">.</nts>
                  <nts id="Seg_4324" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T353" id="Seg_4325" n="sc" s="T350">
               <ts e="T353" id="Seg_4327" n="HIAT:u" s="T350">
                  <ts e="T351" id="Seg_4329" n="HIAT:w" s="T350">mini͡eke</ts>
                  <nts id="Seg_4330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_4332" n="HIAT:w" s="T351">kepseːbetegiŋ</ts>
                  <nts id="Seg_4333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_4335" n="HIAT:w" s="T352">hu͡ok</ts>
                  <nts id="Seg_4336" n="HIAT:ip">.</nts>
                  <nts id="Seg_4337" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T357" id="Seg_4338" n="sc" s="T354">
               <ts e="T357" id="Seg_4340" n="HIAT:u" s="T354">
                  <ts e="T355" id="Seg_4342" n="HIAT:w" s="T354">heː</ts>
                  <nts id="Seg_4343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_4344" n="HIAT:ip">(</nts>
                  <ts e="T182" id="Seg_4346" n="HIAT:w" s="T355">onnu</ts>
                  <nts id="Seg_4347" n="HIAT:ip">)</nts>
                  <nts id="Seg_4348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_4350" n="HIAT:w" s="T182">gɨnnɨŋ</ts>
                  <nts id="Seg_4351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_4353" n="HIAT:w" s="T356">eni</ts>
                  <nts id="Seg_4354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_4356" n="HIAT:w" s="T385">eː</ts>
                  <nts id="Seg_4357" n="HIAT:ip">.</nts>
                  <nts id="Seg_4358" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T378" id="Seg_4359" n="sc" s="T369">
               <ts e="T378" id="Seg_4361" n="HIAT:u" s="T369">
                  <ts e="T370" id="Seg_4363" n="HIAT:w" s="T369">Tuguj</ts>
                  <nts id="Seg_4364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_4366" n="HIAT:w" s="T370">dʼe</ts>
                  <nts id="Seg_4367" n="HIAT:ip">,</nts>
                  <nts id="Seg_4368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_4370" n="HIAT:w" s="T371">kepseː</ts>
                  <nts id="Seg_4371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_4373" n="HIAT:w" s="T372">beseleː</ts>
                  <nts id="Seg_4374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_4376" n="HIAT:w" s="T373">össü͡ö</ts>
                  <nts id="Seg_4377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_4379" n="HIAT:w" s="T374">tu͡ok</ts>
                  <nts id="Seg_4380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_4382" n="HIAT:w" s="T375">eme</ts>
                  <nts id="Seg_4383" n="HIAT:ip">,</nts>
                  <nts id="Seg_4384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_4386" n="HIAT:w" s="T376">tugu</ts>
                  <nts id="Seg_4387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_4389" n="HIAT:w" s="T377">eme</ts>
                  <nts id="Seg_4390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_4392" n="HIAT:w" s="T49">öjdöːŋŋün</ts>
                  <nts id="Seg_4393" n="HIAT:ip">.</nts>
                  <nts id="Seg_4394" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-ErAI">
            <ts e="T94" id="Seg_4395" n="sc" s="T91">
               <ts e="T92" id="Seg_4397" n="e" s="T91">Iti </ts>
               <ts e="T93" id="Seg_4399" n="e" s="T92">taːs </ts>
               <ts e="T94" id="Seg_4401" n="e" s="T93">heː. </ts>
            </ts>
            <ts e="T103" id="Seg_4402" n="sc" s="T96">
               <ts e="T99" id="Seg_4404" n="e" s="T96">Min </ts>
               <ts e="T100" id="Seg_4406" n="e" s="T99">ol </ts>
               <ts e="T101" id="Seg_4408" n="e" s="T100">baraːrɨ </ts>
               <ts e="T102" id="Seg_4410" n="e" s="T101">gɨnar </ts>
               <ts e="T103" id="Seg_4412" n="e" s="T102">etim. </ts>
            </ts>
            <ts e="T119" id="Seg_4413" n="sc" s="T116">
               <ts e="T117" id="Seg_4415" n="e" s="T116">Eː </ts>
               <ts e="T118" id="Seg_4417" n="e" s="T117">Leŋtej </ts>
               <ts e="T119" id="Seg_4419" n="e" s="T118">ogonnʼor. </ts>
            </ts>
            <ts e="T170" id="Seg_4420" n="sc" s="T164">
               <ts e="T165" id="Seg_4422" n="e" s="T164">Dʼe </ts>
               <ts e="T166" id="Seg_4424" n="e" s="T165">iti </ts>
               <ts e="T167" id="Seg_4426" n="e" s="T166">min </ts>
               <ts e="T168" id="Seg_4428" n="e" s="T167">baraːrɨ </ts>
               <ts e="T170" id="Seg_4430" n="e" s="T168">gɨnabɨn. </ts>
            </ts>
            <ts e="T189" id="Seg_4431" n="sc" s="T183">
               <ts e="T184" id="Seg_4433" n="e" s="T183">Onno </ts>
               <ts e="T185" id="Seg_4435" n="e" s="T184">emi͡e </ts>
               <ts e="T186" id="Seg_4437" n="e" s="T185">uːrallar </ts>
               <ts e="T187" id="Seg_4439" n="e" s="T186">ühü </ts>
               <ts e="T188" id="Seg_4441" n="e" s="T187">bu͡o </ts>
               <ts e="T189" id="Seg_4443" n="e" s="T188">barɨtɨn? </ts>
            </ts>
            <ts e="T230" id="Seg_4444" n="sc" s="T221">
               <ts e="T222" id="Seg_4446" n="e" s="T221">Dʼe, </ts>
               <ts e="T223" id="Seg_4448" n="e" s="T222">dʼe </ts>
               <ts e="T224" id="Seg_4450" n="e" s="T223">iti </ts>
               <ts e="T225" id="Seg_4452" n="e" s="T224">öhü </ts>
               <ts e="T226" id="Seg_4454" n="e" s="T225">istibitim </ts>
               <ts e="T227" id="Seg_4456" n="e" s="T226">min, </ts>
               <ts e="T228" id="Seg_4458" n="e" s="T227">min </ts>
               <ts e="T229" id="Seg_4460" n="e" s="T228">emi͡e </ts>
               <ts e="T230" id="Seg_4462" n="e" s="T229">oččuguj. </ts>
            </ts>
            <ts e="T290" id="Seg_4463" n="sc" s="T280">
               <ts e="T281" id="Seg_4465" n="e" s="T280">Amerikanskijdar </ts>
               <ts e="T282" id="Seg_4467" n="e" s="T281">itte </ts>
               <ts e="T283" id="Seg_4469" n="e" s="T282">keleːčči </ts>
               <ts e="T284" id="Seg_4471" n="e" s="T283">egeleːčči </ts>
               <ts e="T285" id="Seg_4473" n="e" s="T284">etilere </ts>
               <ts e="T290" id="Seg_4475" n="e" s="T285">čaːjkaːttarɨ. </ts>
            </ts>
            <ts e="T300" id="Seg_4476" n="sc" s="T296">
               <ts e="T297" id="Seg_4478" n="e" s="T296">Bihi͡eke </ts>
               <ts e="T298" id="Seg_4480" n="e" s="T297">bu͡olaːččɨ </ts>
               <ts e="T299" id="Seg_4482" n="e" s="T298">ete </ts>
               <ts e="T300" id="Seg_4484" n="e" s="T299">heː. </ts>
            </ts>
            <ts e="T353" id="Seg_4485" n="sc" s="T350">
               <ts e="T351" id="Seg_4487" n="e" s="T350">mini͡eke </ts>
               <ts e="T352" id="Seg_4489" n="e" s="T351">kepseːbetegiŋ </ts>
               <ts e="T353" id="Seg_4491" n="e" s="T352">hu͡ok. </ts>
            </ts>
            <ts e="T357" id="Seg_4492" n="sc" s="T354">
               <ts e="T355" id="Seg_4494" n="e" s="T354">heː </ts>
               <ts e="T182" id="Seg_4496" n="e" s="T355">(onnu) </ts>
               <ts e="T356" id="Seg_4498" n="e" s="T182">gɨnnɨŋ </ts>
               <ts e="T385" id="Seg_4500" n="e" s="T356">eni </ts>
               <ts e="T357" id="Seg_4502" n="e" s="T385">eː. </ts>
            </ts>
            <ts e="T378" id="Seg_4503" n="sc" s="T369">
               <ts e="T370" id="Seg_4505" n="e" s="T369">Tuguj </ts>
               <ts e="T371" id="Seg_4507" n="e" s="T370">dʼe, </ts>
               <ts e="T372" id="Seg_4509" n="e" s="T371">kepseː </ts>
               <ts e="T373" id="Seg_4511" n="e" s="T372">beseleː </ts>
               <ts e="T374" id="Seg_4513" n="e" s="T373">össü͡ö </ts>
               <ts e="T375" id="Seg_4515" n="e" s="T374">tu͡ok </ts>
               <ts e="T376" id="Seg_4517" n="e" s="T375">eme, </ts>
               <ts e="T377" id="Seg_4519" n="e" s="T376">tugu </ts>
               <ts e="T49" id="Seg_4521" n="e" s="T377">eme </ts>
               <ts e="T378" id="Seg_4523" n="e" s="T49">öjdöːŋŋün. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-ErAI">
            <ta e="T94" id="Seg_4524" s="T91">PoIP_ErAI_2009_Life2_nar.ErAI.001 (001.007)</ta>
            <ta e="T103" id="Seg_4525" s="T96">PoIP_ErAI_2009_Life2_nar.ErAI.002 (001.008)</ta>
            <ta e="T119" id="Seg_4526" s="T116">PoIP_ErAI_2009_Life2_nar.ErAI.003 (001.010)</ta>
            <ta e="T170" id="Seg_4527" s="T164">PoIP_ErAI_2009_Life2_nar.ErAI.004 (001.015) </ta>
            <ta e="T189" id="Seg_4528" s="T183">PoIP_ErAI_2009_Life2_nar.ErAI.005 (001.016)</ta>
            <ta e="T230" id="Seg_4529" s="T221">PoIP_ErAI_2009_Life2_nar.ErAI.006 (001.020)</ta>
            <ta e="T290" id="Seg_4530" s="T280">PoIP_ErAI_2009_Life2_nar.ErAI.007 (001.026) </ta>
            <ta e="T300" id="Seg_4531" s="T296">PoIP_ErAI_2009_Life2_nar.ErAI.008 (001.027)</ta>
            <ta e="T353" id="Seg_4532" s="T350">PoIP_ErAI_2009_Life2_nar.ErAI.009 (001.036)</ta>
            <ta e="T357" id="Seg_4533" s="T354">PoIP_ErAI_2009_Life2_nar.ErAI.010 (001.037)</ta>
            <ta e="T378" id="Seg_4534" s="T369">PoIP_ErAI_2009_Life2_nar.ErAI.011 (001.038)</ta>
         </annotation>
         <annotation name="st" tierref="st-ErAI" />
         <annotation name="ts" tierref="ts-ErAI">
            <ta e="T94" id="Seg_4535" s="T91">Iti taːs heː. </ta>
            <ta e="T103" id="Seg_4536" s="T96">Min ol baraːrɨ gɨnar etim. </ta>
            <ta e="T119" id="Seg_4537" s="T116">Eː Leŋtej ogonnʼor. </ta>
            <ta e="T170" id="Seg_4538" s="T164">Dʼe iti min baraːrɨ gɨnabɨn. </ta>
            <ta e="T189" id="Seg_4539" s="T183">Onno emi͡e uːrallar ühü bu͡o barɨtɨn? </ta>
            <ta e="T230" id="Seg_4540" s="T221">Dʼe, dʼe iti öhü istibitim min, min emi͡e oččuguj. </ta>
            <ta e="T290" id="Seg_4541" s="T280">Amerikanskijdar itte keleːčči egeleːčči etilere čaːjkaːttarɨ. </ta>
            <ta e="T300" id="Seg_4542" s="T296">Bihi͡eke bu͡olaːččɨ ete heː. </ta>
            <ta e="T353" id="Seg_4543" s="T350">Mini͡eke kepseːbetegiŋ hu͡ok. </ta>
            <ta e="T357" id="Seg_4544" s="T354">Heː (onnu) gɨnnɨŋ eni eː. </ta>
            <ta e="T378" id="Seg_4545" s="T369">Tuguj dʼe, kepseː beseleː össü͡ö tu͡ok eme, tugu eme öjdöːŋŋün. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-ErAI">
            <ta e="T92" id="Seg_4546" s="T91">iti</ta>
            <ta e="T93" id="Seg_4547" s="T92">taːs</ta>
            <ta e="T94" id="Seg_4548" s="T93">heː</ta>
            <ta e="T99" id="Seg_4549" s="T96">min</ta>
            <ta e="T100" id="Seg_4550" s="T99">ol</ta>
            <ta e="T101" id="Seg_4551" s="T100">bar-aːrɨ</ta>
            <ta e="T102" id="Seg_4552" s="T101">gɨn-ar</ta>
            <ta e="T103" id="Seg_4553" s="T102">e-ti-m</ta>
            <ta e="T117" id="Seg_4554" s="T116">eː</ta>
            <ta e="T118" id="Seg_4555" s="T117">Leŋtej</ta>
            <ta e="T119" id="Seg_4556" s="T118">ogonnʼor</ta>
            <ta e="T165" id="Seg_4557" s="T164">dʼe</ta>
            <ta e="T166" id="Seg_4558" s="T165">iti</ta>
            <ta e="T167" id="Seg_4559" s="T166">min</ta>
            <ta e="T168" id="Seg_4560" s="T167">bar-aːrɨ</ta>
            <ta e="T170" id="Seg_4561" s="T168">gɨn-a-bɨn</ta>
            <ta e="T184" id="Seg_4562" s="T183">onno</ta>
            <ta e="T185" id="Seg_4563" s="T184">emi͡e</ta>
            <ta e="T186" id="Seg_4564" s="T185">uːr-al-lar</ta>
            <ta e="T187" id="Seg_4565" s="T186">ühü</ta>
            <ta e="T188" id="Seg_4566" s="T187">bu͡o</ta>
            <ta e="T189" id="Seg_4567" s="T188">barɨ-tɨ-n</ta>
            <ta e="T222" id="Seg_4568" s="T221">dʼe</ta>
            <ta e="T223" id="Seg_4569" s="T222">dʼe</ta>
            <ta e="T224" id="Seg_4570" s="T223">iti</ta>
            <ta e="T225" id="Seg_4571" s="T224">öh-ü</ta>
            <ta e="T226" id="Seg_4572" s="T225">ist-i-bit-i-m</ta>
            <ta e="T227" id="Seg_4573" s="T226">min</ta>
            <ta e="T228" id="Seg_4574" s="T227">min</ta>
            <ta e="T229" id="Seg_4575" s="T228">emi͡e</ta>
            <ta e="T230" id="Seg_4576" s="T229">oččuguj</ta>
            <ta e="T281" id="Seg_4577" s="T280">amerikanskij-dar</ta>
            <ta e="T282" id="Seg_4578" s="T281">itte</ta>
            <ta e="T283" id="Seg_4579" s="T282">kel-eːčči</ta>
            <ta e="T284" id="Seg_4580" s="T283">egel-eːčči</ta>
            <ta e="T285" id="Seg_4581" s="T284">e-ti-lere</ta>
            <ta e="T290" id="Seg_4582" s="T285">čaːj-kaːt-tar-ɨ</ta>
            <ta e="T297" id="Seg_4583" s="T296">bihi͡e-ke</ta>
            <ta e="T298" id="Seg_4584" s="T297">bu͡ol-aːččɨ</ta>
            <ta e="T299" id="Seg_4585" s="T298">e-t-e</ta>
            <ta e="T300" id="Seg_4586" s="T299">heː</ta>
            <ta e="T351" id="Seg_4587" s="T350">mini͡e-ke</ta>
            <ta e="T352" id="Seg_4588" s="T351">kepseː-beteg-i-ŋ</ta>
            <ta e="T353" id="Seg_4589" s="T352">hu͡ok</ta>
            <ta e="T355" id="Seg_4590" s="T354">heː</ta>
            <ta e="T182" id="Seg_4591" s="T355">on-nu</ta>
            <ta e="T356" id="Seg_4592" s="T182">gɨn-nɨ-ŋ</ta>
            <ta e="T385" id="Seg_4593" s="T356">eni</ta>
            <ta e="T357" id="Seg_4594" s="T385">eː</ta>
            <ta e="T370" id="Seg_4595" s="T369">tug-u=j</ta>
            <ta e="T371" id="Seg_4596" s="T370">dʼe</ta>
            <ta e="T372" id="Seg_4597" s="T371">kepseː</ta>
            <ta e="T373" id="Seg_4598" s="T372">beseleː</ta>
            <ta e="T374" id="Seg_4599" s="T373">össü͡ö</ta>
            <ta e="T375" id="Seg_4600" s="T374">tu͡ok</ta>
            <ta e="T376" id="Seg_4601" s="T375">eme</ta>
            <ta e="T377" id="Seg_4602" s="T376">tug-u</ta>
            <ta e="T49" id="Seg_4603" s="T377">eme</ta>
            <ta e="T378" id="Seg_4604" s="T49">öjdöː-ŋ-ŋün</ta>
         </annotation>
         <annotation name="mp" tierref="mp-ErAI">
            <ta e="T92" id="Seg_4605" s="T91">iti</ta>
            <ta e="T93" id="Seg_4606" s="T92">taːs</ta>
            <ta e="T94" id="Seg_4607" s="T93">eː</ta>
            <ta e="T99" id="Seg_4608" s="T96">min</ta>
            <ta e="T100" id="Seg_4609" s="T99">ol</ta>
            <ta e="T101" id="Seg_4610" s="T100">bar-AːrI</ta>
            <ta e="T102" id="Seg_4611" s="T101">gɨn-Ar</ta>
            <ta e="T103" id="Seg_4612" s="T102">e-TI-m</ta>
            <ta e="T117" id="Seg_4613" s="T116">eː</ta>
            <ta e="T118" id="Seg_4614" s="T117">Leŋtej</ta>
            <ta e="T119" id="Seg_4615" s="T118">ogonnʼor</ta>
            <ta e="T165" id="Seg_4616" s="T164">dʼe</ta>
            <ta e="T166" id="Seg_4617" s="T165">iti</ta>
            <ta e="T167" id="Seg_4618" s="T166">min</ta>
            <ta e="T168" id="Seg_4619" s="T167">bar-AːrI</ta>
            <ta e="T170" id="Seg_4620" s="T168">gɨn-A-BIn</ta>
            <ta e="T184" id="Seg_4621" s="T183">onno</ta>
            <ta e="T185" id="Seg_4622" s="T184">emi͡e</ta>
            <ta e="T186" id="Seg_4623" s="T185">uːr-Ar-LAr</ta>
            <ta e="T187" id="Seg_4624" s="T186">ühü</ta>
            <ta e="T188" id="Seg_4625" s="T187">bu͡o</ta>
            <ta e="T189" id="Seg_4626" s="T188">barɨ-tI-n</ta>
            <ta e="T222" id="Seg_4627" s="T221">dʼe</ta>
            <ta e="T223" id="Seg_4628" s="T222">dʼe</ta>
            <ta e="T224" id="Seg_4629" s="T223">iti</ta>
            <ta e="T225" id="Seg_4630" s="T224">ös-nI</ta>
            <ta e="T226" id="Seg_4631" s="T225">ihit-I-BIT-I-m</ta>
            <ta e="T227" id="Seg_4632" s="T226">min</ta>
            <ta e="T228" id="Seg_4633" s="T227">min</ta>
            <ta e="T229" id="Seg_4634" s="T228">emi͡e</ta>
            <ta e="T230" id="Seg_4635" s="T229">küččügüj</ta>
            <ta e="T281" id="Seg_4636" s="T280">amerikanskij-LAr</ta>
            <ta e="T282" id="Seg_4637" s="T281">itte</ta>
            <ta e="T283" id="Seg_4638" s="T282">kel-AːččI</ta>
            <ta e="T284" id="Seg_4639" s="T283">egel-AːččI</ta>
            <ta e="T285" id="Seg_4640" s="T284">e-TI-LArA</ta>
            <ta e="T290" id="Seg_4641" s="T285">čaːj-kAːN-LAr-nI</ta>
            <ta e="T297" id="Seg_4642" s="T296">bihigi-GA</ta>
            <ta e="T298" id="Seg_4643" s="T297">bu͡ol-AːččI</ta>
            <ta e="T299" id="Seg_4644" s="T298">e-TI-tA</ta>
            <ta e="T300" id="Seg_4645" s="T299">eː</ta>
            <ta e="T351" id="Seg_4646" s="T350">min-GA</ta>
            <ta e="T352" id="Seg_4647" s="T351">kepseː-BAtAK-I-ŋ</ta>
            <ta e="T353" id="Seg_4648" s="T352">hu͡ok</ta>
            <ta e="T355" id="Seg_4649" s="T354">eː</ta>
            <ta e="T182" id="Seg_4650" s="T355">ol-nI</ta>
            <ta e="T356" id="Seg_4651" s="T182">gɨn-TI-ŋ</ta>
            <ta e="T385" id="Seg_4652" s="T356">eni</ta>
            <ta e="T357" id="Seg_4653" s="T385">eː</ta>
            <ta e="T370" id="Seg_4654" s="T369">tu͡ok-nI=Ij</ta>
            <ta e="T371" id="Seg_4655" s="T370">dʼe</ta>
            <ta e="T372" id="Seg_4656" s="T371">kepseː</ta>
            <ta e="T373" id="Seg_4657" s="T372">beseleː</ta>
            <ta e="T374" id="Seg_4658" s="T373">össü͡ö</ta>
            <ta e="T375" id="Seg_4659" s="T374">tu͡ok</ta>
            <ta e="T376" id="Seg_4660" s="T375">eme</ta>
            <ta e="T377" id="Seg_4661" s="T376">tu͡ok-nI</ta>
            <ta e="T49" id="Seg_4662" s="T377">eme</ta>
            <ta e="T378" id="Seg_4663" s="T49">öjdöː-An-GIn</ta>
         </annotation>
         <annotation name="ge" tierref="ge-ErAI">
            <ta e="T92" id="Seg_4664" s="T91">that.[NOM]</ta>
            <ta e="T93" id="Seg_4665" s="T92">stone.[NOM]</ta>
            <ta e="T94" id="Seg_4666" s="T93">AFFIRM</ta>
            <ta e="T99" id="Seg_4667" s="T96">1SG.[NOM]</ta>
            <ta e="T100" id="Seg_4668" s="T99">that</ta>
            <ta e="T101" id="Seg_4669" s="T100">go-CVB.PURP</ta>
            <ta e="T102" id="Seg_4670" s="T101">want-PTCP.PRS</ta>
            <ta e="T103" id="Seg_4671" s="T102">be-PST1-1SG</ta>
            <ta e="T117" id="Seg_4672" s="T116">AFFIRM</ta>
            <ta e="T118" id="Seg_4673" s="T117">Lentej</ta>
            <ta e="T119" id="Seg_4674" s="T118">old.man.[NOM]</ta>
            <ta e="T165" id="Seg_4675" s="T164">well</ta>
            <ta e="T166" id="Seg_4676" s="T165">that.[NOM]</ta>
            <ta e="T167" id="Seg_4677" s="T166">1SG.[NOM]</ta>
            <ta e="T168" id="Seg_4678" s="T167">go-CVB.PURP</ta>
            <ta e="T170" id="Seg_4679" s="T168">want-PRS-1SG</ta>
            <ta e="T184" id="Seg_4680" s="T183">there</ta>
            <ta e="T185" id="Seg_4681" s="T184">also</ta>
            <ta e="T186" id="Seg_4682" s="T185">lay-PRS-3PL</ta>
            <ta e="T187" id="Seg_4683" s="T186">it.is.said</ta>
            <ta e="T188" id="Seg_4684" s="T187">EMPH</ta>
            <ta e="T189" id="Seg_4685" s="T188">whole-3SG-ACC</ta>
            <ta e="T222" id="Seg_4686" s="T221">well</ta>
            <ta e="T223" id="Seg_4687" s="T222">well</ta>
            <ta e="T224" id="Seg_4688" s="T223">that.[NOM]</ta>
            <ta e="T225" id="Seg_4689" s="T224">story-ACC</ta>
            <ta e="T226" id="Seg_4690" s="T225">hear-EP-PST2-EP-1SG</ta>
            <ta e="T227" id="Seg_4691" s="T226">1SG.[NOM]</ta>
            <ta e="T228" id="Seg_4692" s="T227">1SG.[NOM]</ta>
            <ta e="T229" id="Seg_4693" s="T228">also</ta>
            <ta e="T230" id="Seg_4694" s="T229">small.[NOM]</ta>
            <ta e="T281" id="Seg_4695" s="T280">American-PL.[NOM]</ta>
            <ta e="T282" id="Seg_4696" s="T281">EMPH</ta>
            <ta e="T283" id="Seg_4697" s="T282">come-PTCP.HAB</ta>
            <ta e="T284" id="Seg_4698" s="T283">bring-PTCP.HAB</ta>
            <ta e="T285" id="Seg_4699" s="T284">be-PST1-3PL</ta>
            <ta e="T290" id="Seg_4700" s="T285">tea-DIM-PL-ACC</ta>
            <ta e="T297" id="Seg_4701" s="T296">1PL-DAT/LOC</ta>
            <ta e="T298" id="Seg_4702" s="T297">be-PTCP.HAB</ta>
            <ta e="T299" id="Seg_4703" s="T298">be-PST1-3SG</ta>
            <ta e="T300" id="Seg_4704" s="T299">AFFIRM</ta>
            <ta e="T351" id="Seg_4705" s="T350">1SG-DAT/LOC</ta>
            <ta e="T352" id="Seg_4706" s="T351">tell-PST2.NEG-EP-2SG</ta>
            <ta e="T353" id="Seg_4707" s="T352">NEG</ta>
            <ta e="T355" id="Seg_4708" s="T354">AFFIRM</ta>
            <ta e="T182" id="Seg_4709" s="T355">that-ACC</ta>
            <ta e="T356" id="Seg_4710" s="T182">make-PST1-2SG</ta>
            <ta e="T385" id="Seg_4711" s="T356">apparently</ta>
            <ta e="T357" id="Seg_4712" s="T385">eh</ta>
            <ta e="T370" id="Seg_4713" s="T369">what-ACC=Q</ta>
            <ta e="T371" id="Seg_4714" s="T370">well</ta>
            <ta e="T372" id="Seg_4715" s="T371">tell.[IMP.2SG]</ta>
            <ta e="T373" id="Seg_4716" s="T372">happily</ta>
            <ta e="T374" id="Seg_4717" s="T373">still</ta>
            <ta e="T375" id="Seg_4718" s="T374">what.[NOM]</ta>
            <ta e="T376" id="Seg_4719" s="T375">INDEF</ta>
            <ta e="T377" id="Seg_4720" s="T376">what-ACC</ta>
            <ta e="T49" id="Seg_4721" s="T377">INDEF</ta>
            <ta e="T378" id="Seg_4722" s="T49">remember-CVB.SEQ-2SG</ta>
         </annotation>
         <annotation name="gg" tierref="gg-ErAI">
            <ta e="T92" id="Seg_4723" s="T91">dieses.[NOM]</ta>
            <ta e="T93" id="Seg_4724" s="T92">Stein.[NOM]</ta>
            <ta e="T94" id="Seg_4725" s="T93">AFFIRM</ta>
            <ta e="T99" id="Seg_4726" s="T96">1SG.[NOM]</ta>
            <ta e="T100" id="Seg_4727" s="T99">jenes</ta>
            <ta e="T101" id="Seg_4728" s="T100">gehen-CVB.PURP</ta>
            <ta e="T102" id="Seg_4729" s="T101">wollen-PTCP.PRS</ta>
            <ta e="T103" id="Seg_4730" s="T102">sein-PST1-1SG</ta>
            <ta e="T117" id="Seg_4731" s="T116">AFFIRM</ta>
            <ta e="T118" id="Seg_4732" s="T117">Lentej</ta>
            <ta e="T119" id="Seg_4733" s="T118">alter.Mann.[NOM]</ta>
            <ta e="T165" id="Seg_4734" s="T164">doch</ta>
            <ta e="T166" id="Seg_4735" s="T165">dieses.[NOM]</ta>
            <ta e="T167" id="Seg_4736" s="T166">1SG.[NOM]</ta>
            <ta e="T168" id="Seg_4737" s="T167">gehen-CVB.PURP</ta>
            <ta e="T170" id="Seg_4738" s="T168">wollen-PRS-1SG</ta>
            <ta e="T184" id="Seg_4739" s="T183">dort</ta>
            <ta e="T185" id="Seg_4740" s="T184">auch</ta>
            <ta e="T186" id="Seg_4741" s="T185">legen-PRS-3PL</ta>
            <ta e="T187" id="Seg_4742" s="T186">man.sagt</ta>
            <ta e="T188" id="Seg_4743" s="T187">EMPH</ta>
            <ta e="T189" id="Seg_4744" s="T188">ganz-3SG-ACC</ta>
            <ta e="T222" id="Seg_4745" s="T221">doch</ta>
            <ta e="T223" id="Seg_4746" s="T222">doch</ta>
            <ta e="T224" id="Seg_4747" s="T223">dieses.[NOM]</ta>
            <ta e="T225" id="Seg_4748" s="T224">Erzählung-ACC</ta>
            <ta e="T226" id="Seg_4749" s="T225">hören-EP-PST2-EP-1SG</ta>
            <ta e="T227" id="Seg_4750" s="T226">1SG.[NOM]</ta>
            <ta e="T228" id="Seg_4751" s="T227">1SG.[NOM]</ta>
            <ta e="T229" id="Seg_4752" s="T228">auch</ta>
            <ta e="T230" id="Seg_4753" s="T229">klein.[NOM]</ta>
            <ta e="T281" id="Seg_4754" s="T280">Amerikaner-PL.[NOM]</ta>
            <ta e="T282" id="Seg_4755" s="T281">EMPH</ta>
            <ta e="T283" id="Seg_4756" s="T282">kommen-PTCP.HAB</ta>
            <ta e="T284" id="Seg_4757" s="T283">bringen-PTCP.HAB</ta>
            <ta e="T285" id="Seg_4758" s="T284">sein-PST1-3PL</ta>
            <ta e="T290" id="Seg_4759" s="T285">Tee-DIM-PL-ACC</ta>
            <ta e="T297" id="Seg_4760" s="T296">1PL-DAT/LOC</ta>
            <ta e="T298" id="Seg_4761" s="T297">sein-PTCP.HAB</ta>
            <ta e="T299" id="Seg_4762" s="T298">sein-PST1-3SG</ta>
            <ta e="T300" id="Seg_4763" s="T299">AFFIRM</ta>
            <ta e="T351" id="Seg_4764" s="T350">1SG-DAT/LOC</ta>
            <ta e="T352" id="Seg_4765" s="T351">erzählen-PST2.NEG-EP-2SG</ta>
            <ta e="T353" id="Seg_4766" s="T352">NEG</ta>
            <ta e="T355" id="Seg_4767" s="T354">AFFIRM</ta>
            <ta e="T182" id="Seg_4768" s="T355">jener-ACC</ta>
            <ta e="T356" id="Seg_4769" s="T182">machen-PST1-2SG</ta>
            <ta e="T385" id="Seg_4770" s="T356">offenbar</ta>
            <ta e="T357" id="Seg_4771" s="T385">äh</ta>
            <ta e="T370" id="Seg_4772" s="T369">was-ACC=Q</ta>
            <ta e="T371" id="Seg_4773" s="T370">doch</ta>
            <ta e="T372" id="Seg_4774" s="T371">erzählen.[IMP.2SG]</ta>
            <ta e="T373" id="Seg_4775" s="T372">fröhlich</ta>
            <ta e="T374" id="Seg_4776" s="T373">noch</ta>
            <ta e="T375" id="Seg_4777" s="T374">was.[NOM]</ta>
            <ta e="T376" id="Seg_4778" s="T375">INDEF</ta>
            <ta e="T377" id="Seg_4779" s="T376">was-ACC</ta>
            <ta e="T49" id="Seg_4780" s="T377">INDEF</ta>
            <ta e="T378" id="Seg_4781" s="T49">erinnern-CVB.SEQ-2SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr-ErAI">
            <ta e="T92" id="Seg_4782" s="T91">тот.[NOM]</ta>
            <ta e="T93" id="Seg_4783" s="T92">камень.[NOM]</ta>
            <ta e="T94" id="Seg_4784" s="T93">AFFIRM</ta>
            <ta e="T99" id="Seg_4785" s="T96">1SG.[NOM]</ta>
            <ta e="T100" id="Seg_4786" s="T99">тот</ta>
            <ta e="T101" id="Seg_4787" s="T100">идти-CVB.PURP</ta>
            <ta e="T102" id="Seg_4788" s="T101">хотеть-PTCP.PRS</ta>
            <ta e="T103" id="Seg_4789" s="T102">быть-PST1-1SG</ta>
            <ta e="T117" id="Seg_4790" s="T116">AFFIRM</ta>
            <ta e="T118" id="Seg_4791" s="T117">Лентей</ta>
            <ta e="T119" id="Seg_4792" s="T118">старик.[NOM]</ta>
            <ta e="T165" id="Seg_4793" s="T164">вот</ta>
            <ta e="T166" id="Seg_4794" s="T165">тот.[NOM]</ta>
            <ta e="T167" id="Seg_4795" s="T166">1SG.[NOM]</ta>
            <ta e="T168" id="Seg_4796" s="T167">идти-CVB.PURP</ta>
            <ta e="T170" id="Seg_4797" s="T168">хотеть-PRS-1SG</ta>
            <ta e="T184" id="Seg_4798" s="T183">там</ta>
            <ta e="T185" id="Seg_4799" s="T184">тоже</ta>
            <ta e="T186" id="Seg_4800" s="T185">класть-PRS-3PL</ta>
            <ta e="T187" id="Seg_4801" s="T186">говорят</ta>
            <ta e="T188" id="Seg_4802" s="T187">EMPH</ta>
            <ta e="T189" id="Seg_4803" s="T188">целый-3SG-ACC</ta>
            <ta e="T222" id="Seg_4804" s="T221">вот</ta>
            <ta e="T223" id="Seg_4805" s="T222">вот</ta>
            <ta e="T224" id="Seg_4806" s="T223">тот.[NOM]</ta>
            <ta e="T225" id="Seg_4807" s="T224">рассказ-ACC</ta>
            <ta e="T226" id="Seg_4808" s="T225">слышать-EP-PST2-EP-1SG</ta>
            <ta e="T227" id="Seg_4809" s="T226">1SG.[NOM]</ta>
            <ta e="T228" id="Seg_4810" s="T227">1SG.[NOM]</ta>
            <ta e="T229" id="Seg_4811" s="T228">тоже</ta>
            <ta e="T230" id="Seg_4812" s="T229">маленький.[NOM]</ta>
            <ta e="T281" id="Seg_4813" s="T280">Американец-PL.[NOM]</ta>
            <ta e="T282" id="Seg_4814" s="T281">EMPH</ta>
            <ta e="T283" id="Seg_4815" s="T282">приходить-PTCP.HAB</ta>
            <ta e="T284" id="Seg_4816" s="T283">принести-PTCP.HAB</ta>
            <ta e="T285" id="Seg_4817" s="T284">быть-PST1-3PL</ta>
            <ta e="T290" id="Seg_4818" s="T285">чай-DIM-PL-ACC</ta>
            <ta e="T297" id="Seg_4819" s="T296">1PL-DAT/LOC</ta>
            <ta e="T298" id="Seg_4820" s="T297">быть-PTCP.HAB</ta>
            <ta e="T299" id="Seg_4821" s="T298">быть-PST1-3SG</ta>
            <ta e="T300" id="Seg_4822" s="T299">AFFIRM</ta>
            <ta e="T351" id="Seg_4823" s="T350">1SG-DAT/LOC</ta>
            <ta e="T352" id="Seg_4824" s="T351">рассказывать-PST2.NEG-EP-2SG</ta>
            <ta e="T353" id="Seg_4825" s="T352">NEG</ta>
            <ta e="T355" id="Seg_4826" s="T354">AFFIRM</ta>
            <ta e="T182" id="Seg_4827" s="T355">тот-ACC</ta>
            <ta e="T356" id="Seg_4828" s="T182">делать-PST1-2SG</ta>
            <ta e="T385" id="Seg_4829" s="T356">очевидно</ta>
            <ta e="T357" id="Seg_4830" s="T385">ээ</ta>
            <ta e="T370" id="Seg_4831" s="T369">что-ACC=Q</ta>
            <ta e="T371" id="Seg_4832" s="T370">вот</ta>
            <ta e="T372" id="Seg_4833" s="T371">рассказывать.[IMP.2SG]</ta>
            <ta e="T373" id="Seg_4834" s="T372">весело</ta>
            <ta e="T374" id="Seg_4835" s="T373">еще</ta>
            <ta e="T375" id="Seg_4836" s="T374">что.[NOM]</ta>
            <ta e="T376" id="Seg_4837" s="T375">INDEF</ta>
            <ta e="T377" id="Seg_4838" s="T376">что-ACC</ta>
            <ta e="T49" id="Seg_4839" s="T377">INDEF</ta>
            <ta e="T378" id="Seg_4840" s="T49">помнить-CVB.SEQ-2SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc-ErAI">
            <ta e="T92" id="Seg_4841" s="T91">dempro-pro:case</ta>
            <ta e="T93" id="Seg_4842" s="T92">n-n:case</ta>
            <ta e="T94" id="Seg_4843" s="T93">ptcl</ta>
            <ta e="T99" id="Seg_4844" s="T96">pers-pro:case</ta>
            <ta e="T100" id="Seg_4845" s="T99">dempro</ta>
            <ta e="T101" id="Seg_4846" s="T100">v-v:cvb</ta>
            <ta e="T102" id="Seg_4847" s="T101">v-v:ptcp</ta>
            <ta e="T103" id="Seg_4848" s="T102">v-v:tense-v:poss.pn</ta>
            <ta e="T117" id="Seg_4849" s="T116">interj</ta>
            <ta e="T118" id="Seg_4850" s="T117">propr</ta>
            <ta e="T119" id="Seg_4851" s="T118">n-n:case</ta>
            <ta e="T165" id="Seg_4852" s="T164">ptcl</ta>
            <ta e="T166" id="Seg_4853" s="T165">dempro-pro:case</ta>
            <ta e="T167" id="Seg_4854" s="T166">pers-pro:case</ta>
            <ta e="T168" id="Seg_4855" s="T167">v-v:cvb</ta>
            <ta e="T170" id="Seg_4856" s="T168">v-v:tense-v:pred.pn</ta>
            <ta e="T184" id="Seg_4857" s="T183">adv</ta>
            <ta e="T185" id="Seg_4858" s="T184">ptcl</ta>
            <ta e="T186" id="Seg_4859" s="T185">v-v:tense-v:pred.pn</ta>
            <ta e="T187" id="Seg_4860" s="T186">ptcl</ta>
            <ta e="T188" id="Seg_4861" s="T187">ptcl</ta>
            <ta e="T189" id="Seg_4862" s="T188">adj-n:poss-n:case</ta>
            <ta e="T222" id="Seg_4863" s="T221">ptcl</ta>
            <ta e="T223" id="Seg_4864" s="T222">ptcl</ta>
            <ta e="T224" id="Seg_4865" s="T223">dempro-pro:case</ta>
            <ta e="T225" id="Seg_4866" s="T224">n-n:case</ta>
            <ta e="T226" id="Seg_4867" s="T225">v-v:(ins)-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T227" id="Seg_4868" s="T226">pers-pro:case</ta>
            <ta e="T228" id="Seg_4869" s="T227">pers-pro:case</ta>
            <ta e="T229" id="Seg_4870" s="T228">ptcl</ta>
            <ta e="T230" id="Seg_4871" s="T229">adj-n:case</ta>
            <ta e="T281" id="Seg_4872" s="T280">n-n:(num)-n:case</ta>
            <ta e="T282" id="Seg_4873" s="T281">ptcl</ta>
            <ta e="T283" id="Seg_4874" s="T282">v-v:ptcp</ta>
            <ta e="T284" id="Seg_4875" s="T283">v-v:ptcp</ta>
            <ta e="T285" id="Seg_4876" s="T284">v-v:tense-v:poss.pn</ta>
            <ta e="T290" id="Seg_4877" s="T285">n-n&gt;n-n:(num)-n:case</ta>
            <ta e="T297" id="Seg_4878" s="T296">pers-pro:case</ta>
            <ta e="T298" id="Seg_4879" s="T297">v-v:ptcp</ta>
            <ta e="T299" id="Seg_4880" s="T298">v-v:tense-v:poss.pn</ta>
            <ta e="T300" id="Seg_4881" s="T299">ptcl</ta>
            <ta e="T351" id="Seg_4882" s="T350">pers-pro:case</ta>
            <ta e="T352" id="Seg_4883" s="T351">v-v:neg-v:(ins)-v:poss.pn</ta>
            <ta e="T353" id="Seg_4884" s="T352">ptcl</ta>
            <ta e="T355" id="Seg_4885" s="T354">ptcl</ta>
            <ta e="T182" id="Seg_4886" s="T355">dempro-pro:case</ta>
            <ta e="T356" id="Seg_4887" s="T182">v-v:tense-v:poss.pn</ta>
            <ta e="T385" id="Seg_4888" s="T356">ptcl</ta>
            <ta e="T357" id="Seg_4889" s="T385">interj</ta>
            <ta e="T370" id="Seg_4890" s="T369">que-pro:case-ptcl</ta>
            <ta e="T371" id="Seg_4891" s="T370">ptcl</ta>
            <ta e="T372" id="Seg_4892" s="T371">v-v:mood.pn</ta>
            <ta e="T373" id="Seg_4893" s="T372">adv</ta>
            <ta e="T374" id="Seg_4894" s="T373">adv</ta>
            <ta e="T375" id="Seg_4895" s="T374">que-pro:case</ta>
            <ta e="T376" id="Seg_4896" s="T375">ptcl</ta>
            <ta e="T377" id="Seg_4897" s="T376">que-pro:case</ta>
            <ta e="T49" id="Seg_4898" s="T377">ptcl</ta>
            <ta e="T378" id="Seg_4899" s="T49">v-v:cvb-v:pred.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps-ErAI">
            <ta e="T92" id="Seg_4900" s="T91">dempro</ta>
            <ta e="T93" id="Seg_4901" s="T92">n</ta>
            <ta e="T94" id="Seg_4902" s="T93">ptcl</ta>
            <ta e="T99" id="Seg_4903" s="T96">pers</ta>
            <ta e="T100" id="Seg_4904" s="T99">dempro</ta>
            <ta e="T101" id="Seg_4905" s="T100">v</ta>
            <ta e="T102" id="Seg_4906" s="T101">v</ta>
            <ta e="T103" id="Seg_4907" s="T102">aux</ta>
            <ta e="T117" id="Seg_4908" s="T116">interj</ta>
            <ta e="T118" id="Seg_4909" s="T117">propr</ta>
            <ta e="T119" id="Seg_4910" s="T118">n</ta>
            <ta e="T165" id="Seg_4911" s="T164">ptcl</ta>
            <ta e="T166" id="Seg_4912" s="T165">dempro</ta>
            <ta e="T167" id="Seg_4913" s="T166">pers</ta>
            <ta e="T168" id="Seg_4914" s="T167">v</ta>
            <ta e="T170" id="Seg_4915" s="T168">v</ta>
            <ta e="T184" id="Seg_4916" s="T183">adv</ta>
            <ta e="T185" id="Seg_4917" s="T184">ptcl</ta>
            <ta e="T186" id="Seg_4918" s="T185">v</ta>
            <ta e="T187" id="Seg_4919" s="T186">ptcl</ta>
            <ta e="T188" id="Seg_4920" s="T187">ptcl</ta>
            <ta e="T189" id="Seg_4921" s="T188">adj</ta>
            <ta e="T222" id="Seg_4922" s="T221">ptcl</ta>
            <ta e="T223" id="Seg_4923" s="T222">ptcl</ta>
            <ta e="T224" id="Seg_4924" s="T223">dempro</ta>
            <ta e="T225" id="Seg_4925" s="T224">n</ta>
            <ta e="T226" id="Seg_4926" s="T225">v</ta>
            <ta e="T227" id="Seg_4927" s="T226">pers</ta>
            <ta e="T228" id="Seg_4928" s="T227">pers</ta>
            <ta e="T229" id="Seg_4929" s="T228">ptcl</ta>
            <ta e="T230" id="Seg_4930" s="T229">adj</ta>
            <ta e="T281" id="Seg_4931" s="T280">n</ta>
            <ta e="T282" id="Seg_4932" s="T281">ptcl</ta>
            <ta e="T283" id="Seg_4933" s="T282">v</ta>
            <ta e="T284" id="Seg_4934" s="T283">v</ta>
            <ta e="T285" id="Seg_4935" s="T284">aux</ta>
            <ta e="T290" id="Seg_4936" s="T285">n</ta>
            <ta e="T297" id="Seg_4937" s="T296">pers</ta>
            <ta e="T298" id="Seg_4938" s="T297">cop</ta>
            <ta e="T299" id="Seg_4939" s="T298">aux</ta>
            <ta e="T300" id="Seg_4940" s="T299">ptcl</ta>
            <ta e="T351" id="Seg_4941" s="T350">pers</ta>
            <ta e="T352" id="Seg_4942" s="T351">v</ta>
            <ta e="T353" id="Seg_4943" s="T352">ptcl</ta>
            <ta e="T355" id="Seg_4944" s="T354">ptcl</ta>
            <ta e="T182" id="Seg_4945" s="T355">dempro</ta>
            <ta e="T356" id="Seg_4946" s="T182">v</ta>
            <ta e="T385" id="Seg_4947" s="T356">ptcl</ta>
            <ta e="T357" id="Seg_4948" s="T385">interj</ta>
            <ta e="T370" id="Seg_4949" s="T369">que</ta>
            <ta e="T371" id="Seg_4950" s="T370">ptcl</ta>
            <ta e="T372" id="Seg_4951" s="T371">v</ta>
            <ta e="T373" id="Seg_4952" s="T372">adv</ta>
            <ta e="T374" id="Seg_4953" s="T373">adv</ta>
            <ta e="T375" id="Seg_4954" s="T374">que</ta>
            <ta e="T376" id="Seg_4955" s="T375">ptcl</ta>
            <ta e="T377" id="Seg_4956" s="T376">que</ta>
            <ta e="T49" id="Seg_4957" s="T377">ptcl</ta>
            <ta e="T378" id="Seg_4958" s="T49">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-ErAI" />
         <annotation name="SyF" tierref="SyF-ErAI" />
         <annotation name="IST" tierref="IST-ErAI" />
         <annotation name="Top" tierref="Top-ErAI" />
         <annotation name="Foc" tierref="Foc-ErAI" />
         <annotation name="BOR" tierref="BOR-ErAI">
            <ta e="T290" id="Seg_4959" s="T285">RUS:cultEV:gram (DIM)</ta>
            <ta e="T373" id="Seg_4960" s="T372">RUS:core</ta>
            <ta e="T374" id="Seg_4961" s="T373">RUS:mod</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-ErAI" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-ErAI" />
         <annotation name="CS" tierref="CS-ErAI" />
         <annotation name="fe" tierref="fe-ErAI">
            <ta e="T94" id="Seg_4962" s="T91">That stone, yeah.</ta>
            <ta e="T103" id="Seg_4963" s="T96">I wanted to go there.</ta>
            <ta e="T119" id="Seg_4964" s="T116">Yeah the old man Lentej.</ta>
            <ta e="T170" id="Seg_4965" s="T164">Well I want to go there.</ta>
            <ta e="T189" id="Seg_4966" s="T183">There also they put everything, they say?</ta>
            <ta e="T230" id="Seg_4967" s="T221">Yes yes, I heard that story, I [was] also little.</ta>
            <ta e="T290" id="Seg_4968" s="T280">The Americans used to come and bring teas.</ta>
            <ta e="T300" id="Seg_4969" s="T296">We had that yeah.</ta>
            <ta e="T353" id="Seg_4970" s="T350">To me you did not tell, no.</ta>
            <ta e="T357" id="Seg_4971" s="T354">Yeah that is what you mean probably.</ta>
            <ta e="T378" id="Seg_4972" s="T369">Well what, tell something nice, remember something.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-ErAI">
            <ta e="T94" id="Seg_4973" s="T91">Der Stein, ja.</ta>
            <ta e="T103" id="Seg_4974" s="T96">Ich wollte dorthin gehen.</ta>
            <ta e="T119" id="Seg_4975" s="T116">Ja, der alte Mann Lentej.</ta>
            <ta e="T170" id="Seg_4976" s="T164">Nun, ich möchte da hingehen.</ta>
            <ta e="T189" id="Seg_4977" s="T183">Dort legen sie auch alles hin, sagt man?</ta>
            <ta e="T230" id="Seg_4978" s="T221">Ja, ja, die Geschichte habe ich gehört, ich [war] auch klein.</ta>
            <ta e="T290" id="Seg_4979" s="T280">Die Amerikaner kamen und brachten Tee.</ta>
            <ta e="T300" id="Seg_4980" s="T296">Wir hatten [das], ja.</ta>
            <ta e="T353" id="Seg_4981" s="T350">Mir hast du es nicht erzählt.</ta>
            <ta e="T357" id="Seg_4982" s="T354">Ja, das meinst du wahrscheinlich.</ta>
            <ta e="T378" id="Seg_4983" s="T369">Nun, was, erzähl irgendetwas Fröhliches, erinnere etwas.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-ErAI" />
         <annotation name="ltr" tierref="ltr-ErAI">
            <ta e="T94" id="Seg_4984" s="T91">В: а это камень да.</ta>
            <ta e="T103" id="Seg_4985" s="T96">В: я поехать хотела.</ta>
            <ta e="T119" id="Seg_4986" s="T116">В: а Лэӈтэй дедушка.</ta>
            <ta e="T170" id="Seg_4987" s="T164">В: ну это я поехать хочу.</ta>
            <ta e="T189" id="Seg_4988" s="T183">В: туда тоже кладут вроде всё…?</ta>
            <ta e="T230" id="Seg_4989" s="T221">В: да, да эту нвость слышала я, я тоже маленькая… ?</ta>
            <ta e="T290" id="Seg_4990" s="T280">В: американцы порой приезжали привозили чай.</ta>
            <ta e="T300" id="Seg_4991" s="T296">В: у нас было да.</ta>
            <ta e="T353" id="Seg_4992" s="T350">В: мне не рассказывала нет..</ta>
            <ta e="T357" id="Seg_4993" s="T354">В: да это имеешь ввиду наверное.</ta>
            <ta e="T378" id="Seg_4994" s="T369">В: расскажи веселое ещё что-нибудь, что-нибудь вспомни.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-ErAI" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T380" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T97" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T182" />
            <conversion-tli id="T356" />
            <conversion-tli id="T385" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T49" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T384" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-PoIP"
                          name="ref"
                          segmented-tier-id="tx-PoIP"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-PoIP"
                          name="st"
                          segmented-tier-id="tx-PoIP"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-PoIP"
                          name="ts"
                          segmented-tier-id="tx-PoIP"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-PoIP"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-PoIP"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-PoIP"
                          name="mb"
                          segmented-tier-id="tx-PoIP"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-PoIP"
                          name="mp"
                          segmented-tier-id="tx-PoIP"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-PoIP"
                          name="ge"
                          segmented-tier-id="tx-PoIP"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-PoIP"
                          name="gg"
                          segmented-tier-id="tx-PoIP"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-PoIP"
                          name="gr"
                          segmented-tier-id="tx-PoIP"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-PoIP"
                          name="mc"
                          segmented-tier-id="tx-PoIP"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-PoIP"
                          name="ps"
                          segmented-tier-id="tx-PoIP"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-PoIP"
                          name="SeR"
                          segmented-tier-id="tx-PoIP"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-PoIP"
                          name="SyF"
                          segmented-tier-id="tx-PoIP"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-PoIP"
                          name="IST"
                          segmented-tier-id="tx-PoIP"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-PoIP"
                          name="Top"
                          segmented-tier-id="tx-PoIP"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-PoIP"
                          name="Foc"
                          segmented-tier-id="tx-PoIP"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-PoIP"
                          name="BOR"
                          segmented-tier-id="tx-PoIP"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-PoIP"
                          name="BOR-Phon"
                          segmented-tier-id="tx-PoIP"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-PoIP"
                          name="BOR-Morph"
                          segmented-tier-id="tx-PoIP"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-PoIP"
                          name="CS"
                          segmented-tier-id="tx-PoIP"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-PoIP"
                          name="fe"
                          segmented-tier-id="tx-PoIP"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-PoIP"
                          name="fg"
                          segmented-tier-id="tx-PoIP"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-PoIP"
                          name="fr"
                          segmented-tier-id="tx-PoIP"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-PoIP"
                          name="ltr"
                          segmented-tier-id="tx-PoIP"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-PoIP"
                          name="nt"
                          segmented-tier-id="tx-PoIP"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-ErAI"
                          name="ref"
                          segmented-tier-id="tx-ErAI"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-ErAI"
                          name="st"
                          segmented-tier-id="tx-ErAI"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-ErAI"
                          name="ts"
                          segmented-tier-id="tx-ErAI"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-ErAI"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-ErAI"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-ErAI"
                          name="mb"
                          segmented-tier-id="tx-ErAI"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-ErAI"
                          name="mp"
                          segmented-tier-id="tx-ErAI"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-ErAI"
                          name="ge"
                          segmented-tier-id="tx-ErAI"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-ErAI"
                          name="gg"
                          segmented-tier-id="tx-ErAI"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-ErAI"
                          name="gr"
                          segmented-tier-id="tx-ErAI"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-ErAI"
                          name="mc"
                          segmented-tier-id="tx-ErAI"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-ErAI"
                          name="ps"
                          segmented-tier-id="tx-ErAI"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-ErAI"
                          name="SeR"
                          segmented-tier-id="tx-ErAI"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-ErAI"
                          name="SyF"
                          segmented-tier-id="tx-ErAI"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-ErAI"
                          name="IST"
                          segmented-tier-id="tx-ErAI"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-ErAI"
                          name="Top"
                          segmented-tier-id="tx-ErAI"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-ErAI"
                          name="Foc"
                          segmented-tier-id="tx-ErAI"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-ErAI"
                          name="BOR"
                          segmented-tier-id="tx-ErAI"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-ErAI"
                          name="BOR-Phon"
                          segmented-tier-id="tx-ErAI"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-ErAI"
                          name="BOR-Morph"
                          segmented-tier-id="tx-ErAI"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-ErAI"
                          name="CS"
                          segmented-tier-id="tx-ErAI"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-ErAI"
                          name="fe"
                          segmented-tier-id="tx-ErAI"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-ErAI"
                          name="fg"
                          segmented-tier-id="tx-ErAI"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-ErAI"
                          name="fr"
                          segmented-tier-id="tx-ErAI"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-ErAI"
                          name="ltr"
                          segmented-tier-id="tx-ErAI"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-ErAI"
                          name="nt"
                          segmented-tier-id="tx-ErAI"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
