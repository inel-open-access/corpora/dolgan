<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDC3C98DAB-2184-47EF-C0FD-E3784FD666C0">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>PoNA_2004_MikaMukulajAloneAtHome_nar</transcription-name>
         <referenced-file url="PoNA_2004_MikaMukulajAloneAtHome_nar.wav" />
         <referenced-file url="PoNA_2004_MikaMukulajAloneAtHome_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\PoNA_2004_MikaMukulajAloneAtHome_nar\PoNA_2004_MikaMukulajAloneAtHome_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">575</ud-information>
            <ud-information attribute-name="# HIAT:w">448</ud-information>
            <ud-information attribute-name="# e">446</ud-information>
            <ud-information attribute-name="# HIAT:u">69</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PoNA">
            <abbreviation>PoNA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="2.1933245761079467" />
         <tli id="T2" time="2.251" type="appl" />
         <tli id="T3" time="2.973" type="appl" />
         <tli id="T4" time="3.694" type="appl" />
         <tli id="T5" time="4.415" type="appl" />
         <tli id="T6" time="5.137" type="appl" />
         <tli id="T7" time="5.858" type="appl" />
         <tli id="T8" time="6.58" type="appl" />
         <tli id="T9" time="7.301" type="appl" />
         <tli id="T10" time="8.022" type="appl" />
         <tli id="T11" time="8.744" type="appl" />
         <tli id="T12" time="10.233292475154098" />
         <tli id="T13" time="10.336" type="appl" />
         <tli id="T14" time="11.207" type="appl" />
         <tli id="T15" time="12.078" type="appl" />
         <tli id="T16" time="12.95" type="appl" />
         <tli id="T17" time="13.821" type="appl" />
         <tli id="T18" time="14.692" type="appl" />
         <tli id="T19" time="16.29326827965903" />
         <tli id="T20" time="17.051412589687445" type="intp" />
         <tli id="T21" time="17.809556899715858" type="intp" />
         <tli id="T22" time="18.56770120974427" type="intp" />
         <tli id="T23" time="19.325845519772685" type="intp" />
         <tli id="T24" time="20.083989829801098" type="intp" />
         <tli id="T25" time="20.84213413982951" type="intp" />
         <tli id="T26" time="21.600278449857925" type="intp" />
         <tli id="T27" time="22.358422759886338" type="intp" />
         <tli id="T28" time="23.11656706991475" type="intp" />
         <tli id="T29" time="23.874711379943168" type="intp" />
         <tli id="T30" time="24.632855689971585" type="intp" />
         <tli id="T31" time="25.391" type="appl" />
         <tli id="T32" time="26.219" type="appl" />
         <tli id="T33" time="27.047" type="appl" />
         <tli id="T34" time="27.875" type="appl" />
         <tli id="T35" time="28.703" type="appl" />
         <tli id="T36" time="29.531" type="appl" />
         <tli id="T37" time="30.713210705560215" />
         <tli id="T38" time="30.94" type="appl" />
         <tli id="T39" time="31.521" type="appl" />
         <tli id="T40" time="32.102" type="appl" />
         <tli id="T41" time="32.684" type="appl" />
         <tli id="T42" time="33.265" type="appl" />
         <tli id="T43" time="33.846" type="appl" />
         <tli id="T44" time="34.427" type="appl" />
         <tli id="T45" time="35.008" type="appl" />
         <tli id="T46" time="35.589" type="appl" />
         <tli id="T47" time="36.171" type="appl" />
         <tli id="T48" time="36.752" type="appl" />
         <tli id="T49" time="37.333" type="appl" />
         <tli id="T50" time="38.107331573886654" />
         <tli id="T51" time="38.657" type="appl" />
         <tli id="T52" time="39.4" type="appl" />
         <tli id="T53" time="40.143" type="appl" />
         <tli id="T54" time="40.886" type="appl" />
         <tli id="T55" time="41.629" type="appl" />
         <tli id="T56" time="42.373" type="appl" />
         <tli id="T57" time="43.116" type="appl" />
         <tli id="T58" time="43.859" type="appl" />
         <tli id="T59" time="44.602" type="appl" />
         <tli id="T60" time="45.345" type="appl" />
         <tli id="T61" time="46.088" type="appl" />
         <tli id="T62" time="46.649" type="appl" />
         <tli id="T63" time="47.21" type="appl" />
         <tli id="T64" time="47.771" type="appl" />
         <tli id="T65" time="48.332" type="appl" />
         <tli id="T66" time="48.892" type="appl" />
         <tli id="T67" time="49.453" type="appl" />
         <tli id="T68" time="50.014" type="appl" />
         <tli id="T69" time="50.48833357289964" />
         <tli id="T70" time="51.30977777777778" type="intp" />
         <tli id="T71" time="52.04455555555556" type="intp" />
         <tli id="T72" time="52.77933333333334" type="intp" />
         <tli id="T73" time="53.51411111111112" type="intp" />
         <tli id="T74" time="54.2488888888889" type="intp" />
         <tli id="T75" time="54.98366666666668" type="intp" />
         <tli id="T76" time="55.71844444444445" type="intp" />
         <tli id="T77" time="56.66655760319487" />
         <tli id="T78" time="57.188" type="appl" />
         <tli id="T79" time="57.866" type="appl" />
         <tli id="T80" time="58.544" type="appl" />
         <tli id="T81" time="59.221" type="appl" />
         <tli id="T82" time="59.899" type="appl" />
         <tli id="T83" time="60.577" type="appl" />
         <tli id="T84" time="61.37500234558655" />
         <tli id="T85" time="62.142" type="appl" />
         <tli id="T86" time="63.03" type="appl" />
         <tli id="T87" time="63.917" type="appl" />
         <tli id="T88" time="64.804" type="appl" />
         <tli id="T89" time="65.691" type="appl" />
         <tli id="T90" time="66.579" type="appl" />
         <tli id="T91" time="67.54599984223496" />
         <tli id="T92" time="68.062" type="appl" />
         <tli id="T93" time="68.657" type="appl" />
         <tli id="T94" time="69.252" type="appl" />
         <tli id="T95" time="69.848" type="appl" />
         <tli id="T96" time="70.444" type="appl" />
         <tli id="T97" time="71.039" type="appl" />
         <tli id="T98" time="71.634" type="appl" />
         <tli id="T99" time="72.06999870647519" />
         <tli id="T100" time="72.849" type="appl" />
         <tli id="T101" time="73.469" type="appl" />
         <tli id="T102" time="74.088" type="appl" />
         <tli id="T103" time="74.707" type="appl" />
         <tli id="T104" time="75.326" type="appl" />
         <tli id="T105" time="75.946" type="appl" />
         <tli id="T106" time="76.95166541349842" />
         <tli id="T107" time="77.272" type="appl" />
         <tli id="T108" time="77.979" type="appl" />
         <tli id="T109" time="78.686" type="appl" />
         <tli id="T110" time="79.393" type="appl" />
         <tli id="T111" time="80.1" type="appl" />
         <tli id="T112" time="80.806" type="appl" />
         <tli id="T113" time="81.513" type="appl" />
         <tli id="T114" time="82.22" type="appl" />
         <tli id="T115" time="82.927" type="appl" />
         <tli id="T116" time="84.04633109724281" />
         <tli id="T117" time="84.5" type="appl" />
         <tli id="T118" time="85.367" type="appl" />
         <tli id="T119" time="86.233" type="appl" />
         <tli id="T120" time="87.099" type="appl" />
         <tli id="T121" time="87.965" type="appl" />
         <tli id="T122" time="88.832" type="appl" />
         <tli id="T123" time="89.698" type="appl" />
         <tli id="T124" time="90.564" type="appl" />
         <tli id="T125" time="91.431" type="appl" />
         <tli id="T126" time="92.67296332059749" />
         <tli id="T127" time="92.951" type="appl" />
         <tli id="T128" time="93.604" type="appl" />
         <tli id="T129" time="94.258" type="appl" />
         <tli id="T130" time="94.912" type="appl" />
         <tli id="T131" time="95.566" type="appl" />
         <tli id="T132" time="96.22" type="appl" />
         <tli id="T133" time="96.873" type="appl" />
         <tli id="T134" time="98.13294152069598" />
         <tli id="T135" time="98.26" type="appl" />
         <tli id="T136" time="98.992" type="appl" />
         <tli id="T137" time="99.725" type="appl" />
         <tli id="T138" time="100.457" type="appl" />
         <tli id="T139" time="101.01666698609303" />
         <tli id="T140" time="101.96" type="appl" />
         <tli id="T141" time="102.87666607014363" />
         <tli id="T142" time="103.522" type="appl" />
         <tli id="T143" time="104.313" type="appl" />
         <tli id="T144" time="105.105" type="appl" />
         <tli id="T145" time="105.897" type="appl" />
         <tli id="T146" time="106.688" type="appl" />
         <tli id="T147" time="107.14666855267473" />
         <tli id="T148" time="108.03" type="appl" />
         <tli id="T149" time="108.58" type="appl" />
         <tli id="T150" time="109.13" type="appl" />
         <tli id="T151" time="109.681" type="appl" />
         <tli id="T152" time="110.231" type="appl" />
         <tli id="T153" time="110.781" type="appl" />
         <tli id="T154" time="111.79288698101567" />
         <tli id="T155" time="112.129" type="appl" />
         <tli id="T156" time="112.927" type="appl" />
         <tli id="T157" time="113.725" type="appl" />
         <tli id="T158" time="114.523" type="appl" />
         <tli id="T159" time="115.32" type="appl" />
         <tli id="T160" time="116.118" type="appl" />
         <tli id="T161" time="116.916" type="appl" />
         <tli id="T162" time="117.714" type="appl" />
         <tli id="T163" time="118.9461917535501" />
         <tli id="T164" time="119.426" type="appl" />
         <tli id="T165" time="120.339" type="appl" />
         <tli id="T166" time="121.253" type="appl" />
         <tli id="T167" time="122.166" type="appl" />
         <tli id="T168" time="122.86667089202992" />
         <tli id="T169" time="123.897" type="appl" />
         <tli id="T170" time="124.714" type="appl" />
         <tli id="T171" time="125.58432931214486" />
         <tli id="T172" time="126.396" type="appl" />
         <tli id="T173" time="127.261" type="appl" />
         <tli id="T174" time="128.126" type="appl" />
         <tli id="T175" time="128.991" type="appl" />
         <tli id="T176" time="129.856" type="appl" />
         <tli id="T177" time="130.721" type="appl" />
         <tli id="T178" time="131.318" type="appl" />
         <tli id="T179" time="131.916" type="appl" />
         <tli id="T180" time="132.513" type="appl" />
         <tli id="T181" time="133.0566692692098" />
         <tli id="T182" time="134.298" type="appl" />
         <tli id="T183" time="135.485" type="appl" />
         <tli id="T184" time="136.673" type="appl" />
         <tli id="T185" time="137.86" type="appl" />
         <tli id="T186" time="139.048" type="appl" />
         <tli id="T187" time="139.819" type="appl" />
         <tli id="T188" time="140.59" type="appl" />
         <tli id="T189" time="141.36" type="appl" />
         <tli id="T190" time="142.131" type="appl" />
         <tli id="T191" time="142.902" type="appl" />
         <tli id="T192" time="143.673" type="appl" />
         <tli id="T193" time="144.443" type="appl" />
         <tli id="T194" time="145.214" type="appl" />
         <tli id="T195" time="145.985" type="appl" />
         <tli id="T196" time="146.725" type="appl" />
         <tli id="T197" time="147.464" type="appl" />
         <tli id="T198" time="148.204" type="appl" />
         <tli id="T199" time="148.943" type="appl" />
         <tli id="T200" time="149.683" type="appl" />
         <tli id="T201" time="150.666" type="appl" />
         <tli id="T202" time="151.65" type="appl" />
         <tli id="T203" time="152.633" type="appl" />
         <tli id="T204" time="153.616" type="appl" />
         <tli id="T205" time="154.6" type="appl" />
         <tli id="T206" time="155.583" type="appl" />
         <tli id="T207" time="156.303" type="appl" />
         <tli id="T208" time="157.024" type="appl" />
         <tli id="T209" time="157.745" type="appl" />
         <tli id="T210" time="158.465" type="appl" />
         <tli id="T211" time="159.229" type="appl" />
         <tli id="T212" time="159.993" type="appl" />
         <tli id="T213" time="160.757" type="appl" />
         <tli id="T214" time="161.521" type="appl" />
         <tli id="T215" time="162.286" type="appl" />
         <tli id="T216" time="163.05" type="appl" />
         <tli id="T217" time="163.814" type="appl" />
         <tli id="T218" time="164.578" type="appl" />
         <tli id="T219" time="165.342" type="appl" />
         <tli id="T220" time="166.106" type="appl" />
         <tli id="T221" time="166.94" type="appl" />
         <tli id="T222" time="167.774" type="appl" />
         <tli id="T223" time="168.608" type="appl" />
         <tli id="T224" time="169.442" type="appl" />
         <tli id="T225" time="170.276" type="appl" />
         <tli id="T226" time="171.11" type="appl" />
         <tli id="T227" time="171.944" type="appl" />
         <tli id="T228" time="173.43264087370287" />
         <tli id="T229" time="173.555" type="appl" />
         <tli id="T230" time="174.332" type="appl" />
         <tli id="T231" time="175.109" type="appl" />
         <tli id="T232" time="175.886" type="appl" />
         <tli id="T233" time="176.663" type="appl" />
         <tli id="T234" time="177.4666612255165" />
         <tli id="T235" time="178.17" type="appl" />
         <tli id="T236" time="178.901" type="appl" />
         <tli id="T237" time="179.632" type="appl" />
         <tli id="T238" time="180.66200003007705" />
         <tli id="T239" time="181.395" type="appl" />
         <tli id="T240" time="182.428" type="appl" />
         <tli id="T241" time="183.461" type="appl" />
         <tli id="T242" time="184.494" type="appl" />
         <tli id="T243" time="185.527" type="appl" />
         <tli id="T244" time="186.56" type="appl" />
         <tli id="T245" time="187.225" type="appl" />
         <tli id="T246" time="187.89" type="appl" />
         <tli id="T247" time="188.555" type="appl" />
         <tli id="T248" time="189.14666667442037" />
         <tli id="T249" time="190.09" type="appl" />
         <tli id="T250" time="190.735" type="appl" />
         <tli id="T251" time="191.38" type="appl" />
         <tli id="T252" time="192.026" type="appl" />
         <tli id="T253" time="192.671" type="appl" />
         <tli id="T254" time="193.72266402896432" />
         <tli id="T255" time="193.895" type="appl" />
         <tli id="T256" time="194.474" type="appl" />
         <tli id="T257" time="195.053" type="appl" />
         <tli id="T258" time="195.632" type="appl" />
         <tli id="T259" time="196.211" type="appl" />
         <tli id="T260" time="196.79" type="appl" />
         <tli id="T261" time="197.50900307802152" />
         <tli id="T262" time="198.202" type="appl" />
         <tli id="T263" time="199.035" type="appl" />
         <tli id="T264" time="199.868" type="appl" />
         <tli id="T265" time="201.14586355723088" />
         <tli id="T266" time="201.736" type="appl" />
         <tli id="T267" time="202.77" type="appl" />
         <tli id="T268" time="203.804" type="appl" />
         <tli id="T269" time="205.6858454305729" />
         <tli id="T270" time="205.712" type="appl" />
         <tli id="T271" time="206.585" type="appl" />
         <tli id="T272" time="207.458" type="appl" />
         <tli id="T273" time="208.33" type="appl" />
         <tli id="T274" time="209.203" type="appl" />
         <tli id="T275" time="210.076" type="appl" />
         <tli id="T276" time="211.00900126029154" />
         <tli id="T277" time="211.913" type="appl" />
         <tli id="T278" time="212.876" type="appl" />
         <tli id="T279" time="213.84" type="appl" />
         <tli id="T280" time="214.804" type="appl" />
         <tli id="T281" time="215.768" type="appl" />
         <tli id="T282" time="216.731" type="appl" />
         <tli id="T283" time="217.72833641135762" />
         <tli id="T284" time="218.386" type="appl" />
         <tli id="T285" time="219.077" type="appl" />
         <tli id="T286" time="219.767" type="appl" />
         <tli id="T287" time="220.458" type="appl" />
         <tli id="T288" time="221.149" type="appl" />
         <tli id="T289" time="221.78666656194358" />
         <tli id="T290" time="222.548" type="appl" />
         <tli id="T291" time="223.257" type="appl" />
         <tli id="T292" time="223.965" type="appl" />
         <tli id="T293" time="224.673" type="appl" />
         <tli id="T294" time="225.381" type="appl" />
         <tli id="T295" time="226.09" type="appl" />
         <tli id="T296" time="226.9846666413681" />
         <tli id="T297" time="227.775" type="appl" />
         <tli id="T298" time="228.753" type="appl" />
         <tli id="T299" time="229.8766603029196" />
         <tli id="T300" time="230.735" type="appl" />
         <tli id="T301" time="231.55334631679585" />
         <tli id="T302" time="232.283" type="appl" />
         <tli id="T303" time="232.826" type="appl" />
         <tli id="T304" time="233.369" type="appl" />
         <tli id="T305" time="233.912" type="appl" />
         <tli id="T306" time="234.455" type="appl" />
         <tli id="T307" time="235.45239324918074" />
         <tli id="T308" time="235.844" type="appl" />
         <tli id="T309" time="236.691" type="appl" />
         <tli id="T310" time="237.537" type="appl" />
         <tli id="T311" time="238.383" type="appl" />
         <tli id="T312" time="239.23" type="appl" />
         <tli id="T313" time="240.18932746115664" />
         <tli id="T314" time="240.778" type="appl" />
         <tli id="T315" time="241.48" type="appl" />
         <tli id="T316" time="242.182" type="appl" />
         <tli id="T317" time="242.883" type="appl" />
         <tli id="T318" time="243.585" type="appl" />
         <tli id="T319" time="244.287" type="appl" />
         <tli id="T320" time="244.989" type="appl" />
         <tli id="T321" time="245.691" type="appl" />
         <tli id="T322" time="246.392" type="appl" />
         <tli id="T323" time="247.094" type="appl" />
         <tli id="T324" time="247.796" type="appl" />
         <tli id="T325" time="248.9523393483254" />
         <tli id="T326" time="249.246" type="appl" />
         <tli id="T327" time="249.995" type="appl" />
         <tli id="T328" time="250.743" type="appl" />
         <tli id="T329" time="251.491" type="appl" />
         <tli id="T330" time="252.24" type="appl" />
         <tli id="T331" time="253.04800007902924" />
         <tli id="T332" time="253.79436363636364" type="intp" />
         <tli id="T333" time="254.60072727272728" type="intp" />
         <tli id="T334" time="255.40709090909093" type="intp" />
         <tli id="T335" time="256.21345454545457" type="intp" />
         <tli id="T336" time="257.0198181818182" type="intp" />
         <tli id="T337" time="257.82618181818185" type="intp" />
         <tli id="T338" time="258.6325454545455" type="intp" />
         <tli id="T339" time="259.43890909090914" type="intp" />
         <tli id="T340" time="260.2452727272728" type="intp" />
         <tli id="T341" time="261.3916386406181" />
         <tli id="T342" time="261.858" type="appl" />
         <tli id="T343" time="262.577" type="appl" />
         <tli id="T344" time="263.295" type="appl" />
         <tli id="T345" time="264.014" type="appl" />
         <tli id="T346" time="264.732" type="appl" />
         <tli id="T347" time="265.451" type="appl" />
         <tli id="T348" time="266.169" type="appl" />
         <tli id="T349" time="266.888" type="appl" />
         <tli id="T350" time="267.606" type="appl" />
         <tli id="T351" time="268.325" type="appl" />
         <tli id="T352" time="269.043" type="appl" />
         <tli id="T353" time="269.762" type="appl" />
         <tli id="T354" time="270.48" type="appl" />
         <tli id="T355" time="271.076" type="appl" />
         <tli id="T356" time="271.671" type="appl" />
         <tli id="T357" time="272.266" type="appl" />
         <tli id="T358" time="272.862" type="appl" />
         <tli id="T359" time="273.557" type="appl" />
         <tli id="T360" time="274.251" type="appl" />
         <tli id="T361" time="274.946" type="appl" />
         <tli id="T362" time="275.641" type="appl" />
         <tli id="T363" time="276.336" type="appl" />
         <tli id="T364" time="277.03" type="appl" />
         <tli id="T365" time="277.725" type="appl" />
         <tli id="T366" time="278.42" type="appl" />
         <tli id="T367" time="279.114" type="appl" />
         <tli id="T368" time="279.809" type="appl" />
         <tli id="T369" time="280.354" type="appl" />
         <tli id="T370" time="280.9" type="appl" />
         <tli id="T371" time="281.445" type="appl" />
         <tli id="T372" time="281.99" type="appl" />
         <tli id="T373" time="282.536" type="appl" />
         <tli id="T374" time="283.081" type="appl" />
         <tli id="T375" time="283.764" type="appl" />
         <tli id="T376" time="284.447" type="appl" />
         <tli id="T377" time="285.13" type="appl" />
         <tli id="T378" time="285.812" type="appl" />
         <tli id="T379" time="286.495" type="appl" />
         <tli id="T380" time="287.178" type="appl" />
         <tli id="T381" time="287.861" type="appl" />
         <tli id="T382" time="288.544" type="appl" />
         <tli id="T383" time="289.572" type="appl" />
         <tli id="T384" time="290.6" type="appl" />
         <tli id="T385" time="291.628" type="appl" />
         <tli id="T386" time="292.656" type="appl" />
         <tli id="T387" time="293.684" type="appl" />
         <tli id="T388" time="294.712" type="appl" />
         <tli id="T389" time="295.85999060313134" />
         <tli id="T390" time="296.5615" type="intp" />
         <tli id="T391" time="297.38300000000004" type="intp" />
         <tli id="T392" time="298.20450000000005" type="intp" />
         <tli id="T393" time="299.026" type="intp" />
         <tli id="T394" time="300.0674998455941" />
         <tli id="T395" time="300.669" type="appl" />
         <tli id="T396" time="301.285" type="appl" />
         <tli id="T397" time="301.902" type="appl" />
         <tli id="T398" time="302.518" type="appl" />
         <tli id="T399" time="303.134" type="appl" />
         <tli id="T400" time="303.6566782235158" />
         <tli id="T401" time="304.522" type="appl" />
         <tli id="T402" time="305.294" type="appl" />
         <tli id="T403" time="306.066" type="appl" />
         <tli id="T404" time="306.838" type="appl" />
         <tli id="T405" time="307.61" type="appl" />
         <tli id="T406" time="308.382" type="appl" />
         <tli id="T407" time="309.9587624363613" />
         <tli id="T408" time="310.136" type="appl" />
         <tli id="T409" time="311.118" type="appl" />
         <tli id="T410" time="311.9866710062544" />
         <tli id="T411" time="313.246" type="appl" />
         <tli id="T412" time="314.392" type="appl" />
         <tli id="T413" time="315.57801083387955" />
         <tli id="T414" time="316.3394285714286" type="intp" />
         <tli id="T415" time="317.14085714285716" type="intp" />
         <tli id="T416" time="317.94228571428573" type="intp" />
         <tli id="T417" time="318.7437142857143" type="intp" />
         <tli id="T418" time="319.5451428571429" type="intp" />
         <tli id="T419" time="320.5132255041866" />
         <tli id="T420" time="321.148" type="appl" />
         <tli id="T421" time="322.12998988231027" />
         <tli id="T422" time="322.947" type="appl" />
         <tli id="T423" time="323.604" type="appl" />
         <tli id="T424" time="324.26" type="appl" />
         <tli id="T425" time="325.26367528719925" />
         <tli id="T426" time="325.598" type="appl" />
         <tli id="T427" time="326.278" type="appl" />
         <tli id="T428" time="326.959" type="appl" />
         <tli id="T429" time="327.64" type="appl" />
         <tli id="T430" time="328.32" type="appl" />
         <tli id="T431" time="329.001" type="appl" />
         <tli id="T432" time="329.681" type="appl" />
         <tli id="T433" time="331.0053450709291" />
         <tli id="T434" time="331.095" type="appl" />
         <tli id="T435" time="331.827" type="appl" />
         <tli id="T436" time="332.56" type="appl" />
         <tli id="T437" time="333.292" type="appl" />
         <tli id="T438" time="334.025" type="appl" />
         <tli id="T439" time="334.758" type="appl" />
         <tli id="T440" time="335.49" type="appl" />
         <tli id="T441" time="336.223" type="appl" />
         <tli id="T442" time="336.955" type="appl" />
         <tli id="T443" time="337.70800060109144" />
         <tli id="T444" time="338.508" type="appl" />
         <tli id="T445" time="339.327" type="appl" />
         <tli id="T446" time="340.147" type="appl" />
         <tli id="T447" time="340.966" type="appl" />
         <tli id="T448" time="341.786" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PoNA"
                      type="t">
         <timeline-fork end="T29" start="T27">
            <tli id="T27.tx.1" />
         </timeline-fork>
         <timeline-fork end="T430" start="T428">
            <tli id="T428.tx.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T448" id="Seg_0" n="sc" s="T0">
               <ts e="T1" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Kuttaːbɨta</ts>
                  <nts id="Seg_5" n="HIAT:ip">.</nts>
                  <nts id="Seg_6" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_8" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_10" n="HIAT:w" s="T1">Ikki</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_13" n="HIAT:w" s="T2">u͡ol</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">ogolor</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">Mikaːnɨ</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">kɨtta</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">Mukulaːj</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">kergettere</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">barbɨttarɨn</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">kenne</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_37" n="HIAT:w" s="T10">dʼi͡eleriger</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">kommuttara</ts>
                  <nts id="Seg_41" n="HIAT:ip">.</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_44" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_46" n="HIAT:w" s="T12">Giniler</ts>
                  <nts id="Seg_47" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_49" n="HIAT:w" s="T13">ü͡örener</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_52" n="HIAT:w" s="T14">kemneriger</ts>
                  <nts id="Seg_53" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_55" n="HIAT:w" s="T15">kɨhɨnɨ</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_58" n="HIAT:w" s="T16">huptu</ts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_61" n="HIAT:w" s="T17">intʼernaːkka</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_64" n="HIAT:w" s="T18">bu͡olaːččɨlar</ts>
                  <nts id="Seg_65" n="HIAT:ip">.</nts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_68" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_70" n="HIAT:w" s="T19">Bɨlɨrgɨ</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_73" n="HIAT:w" s="T20">nʼuːčča</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_76" n="HIAT:w" s="T21">dʼi͡e</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_79" n="HIAT:w" s="T22">isti͡enetin</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_82" n="HIAT:w" s="T23">mastara</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_85" n="HIAT:w" s="T24">elete</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_88" n="HIAT:w" s="T25">bu͡olannar</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_91" n="HIAT:w" s="T26">olus</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_93" n="HIAT:ip">(</nts>
                  <ts e="T27.tx.1" id="Seg_95" n="HIAT:w" s="T27">emir</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_98" n="HIAT:w" s="T27.tx.1">-</ts>
                  <nts id="Seg_99" n="HIAT:ip">)</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_102" n="HIAT:w" s="T29">emegerbitter</ts>
                  <nts id="Seg_103" n="HIAT:ip">.</nts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_106" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_108" n="HIAT:w" s="T30">Tahaːraːgɨ</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_111" n="HIAT:w" s="T31">güːletin</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_114" n="HIAT:w" s="T32">aːna</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_117" n="HIAT:w" s="T33">kihi</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_120" n="HIAT:w" s="T34">arɨjdagɨna</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_123" n="HIAT:w" s="T35">haːrkɨrɨːra</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_126" n="HIAT:w" s="T36">bert</ts>
                  <nts id="Seg_127" n="HIAT:ip">.</nts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_130" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_132" n="HIAT:w" s="T37">Dʼi͡e</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_135" n="HIAT:w" s="T38">ihiger</ts>
                  <nts id="Seg_136" n="HIAT:ip">,</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_139" n="HIAT:w" s="T39">töhö</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_142" n="HIAT:w" s="T40">da</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_145" n="HIAT:w" s="T41">aːjdaːn</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_148" n="HIAT:w" s="T42">bu͡ollun</ts>
                  <nts id="Seg_149" n="HIAT:ip">,</nts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_152" n="HIAT:w" s="T43">kim</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_155" n="HIAT:w" s="T44">eme</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_158" n="HIAT:w" s="T45">arɨjdagɨna</ts>
                  <nts id="Seg_159" n="HIAT:ip">,</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_162" n="HIAT:w" s="T46">haːrkɨrɨːr</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_165" n="HIAT:w" s="T47">tɨ͡aha</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_168" n="HIAT:w" s="T48">honno</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_171" n="HIAT:w" s="T49">ihiller</ts>
                  <nts id="Seg_172" n="HIAT:ip">.</nts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_175" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_177" n="HIAT:w" s="T50">Horok</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_180" n="HIAT:w" s="T51">kenne</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_183" n="HIAT:w" s="T52">tɨ͡al</ts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_186" n="HIAT:w" s="T53">okson</ts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_189" n="HIAT:w" s="T54">arɨjdagɨna</ts>
                  <nts id="Seg_190" n="HIAT:ip">,</nts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_193" n="HIAT:w" s="T55">haːrkɨraːn</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_196" n="HIAT:w" s="T56">dʼi͡eleːkteri</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_199" n="HIAT:w" s="T57">olorpot</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_202" n="HIAT:w" s="T58">da</ts>
                  <nts id="Seg_203" n="HIAT:ip">,</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_206" n="HIAT:w" s="T59">utuppat</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_209" n="HIAT:w" s="T60">da</ts>
                  <nts id="Seg_210" n="HIAT:ip">.</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T69" id="Seg_213" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_215" n="HIAT:w" s="T61">Arajon</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_218" n="HIAT:w" s="T62">kiːn</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_221" n="HIAT:w" s="T63">ologugar</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_224" n="HIAT:w" s="T64">itinnik</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_227" n="HIAT:w" s="T65">olus</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_230" n="HIAT:w" s="T66">erge</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_233" n="HIAT:w" s="T67">dʼi͡e</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_236" n="HIAT:w" s="T68">hu͡ok</ts>
                  <nts id="Seg_237" n="HIAT:ip">.</nts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_240" n="HIAT:u" s="T69">
                  <ts e="T70" id="Seg_242" n="HIAT:w" s="T69">Dʼi͡eleːkter</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_245" n="HIAT:w" s="T70">hotoru</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_248" n="HIAT:w" s="T71">haŋa</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_251" n="HIAT:w" s="T72">dʼi͡ege</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_253" n="HIAT:ip">(</nts>
                  <ts e="T74" id="Seg_255" n="HIAT:w" s="T73">tahɨnan</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_258" n="HIAT:w" s="T74">tus-</ts>
                  <nts id="Seg_259" n="HIAT:ip">)</nts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_262" n="HIAT:w" s="T75">tahɨnɨ͡ak</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_265" n="HIAT:w" s="T76">tustaːktar</ts>
                  <nts id="Seg_266" n="HIAT:ip">.</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T84" id="Seg_269" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_271" n="HIAT:w" s="T77">Ulakan</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_274" n="HIAT:w" s="T78">haŋa</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_277" n="HIAT:w" s="T79">dʼi͡e</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_280" n="HIAT:w" s="T80">tutullan</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_283" n="HIAT:w" s="T81">büterin</ts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_286" n="HIAT:w" s="T82">küːte</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_289" n="HIAT:w" s="T83">olorollor</ts>
                  <nts id="Seg_290" n="HIAT:ip">.</nts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_293" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_295" n="HIAT:w" s="T84">Anɨ</ts>
                  <nts id="Seg_296" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_298" n="HIAT:w" s="T85">ogolor</ts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_301" n="HIAT:w" s="T86">kergettere</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_304" n="HIAT:w" s="T87">čugas</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_307" n="HIAT:w" s="T88">uruːlarɨgar</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_310" n="HIAT:w" s="T89">ɨ͡allana</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_313" n="HIAT:w" s="T90">barbɨttara</ts>
                  <nts id="Seg_314" n="HIAT:ip">.</nts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T99" id="Seg_317" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_319" n="HIAT:w" s="T91">Dʼi͡eleriger</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_322" n="HIAT:w" s="T92">harsɨn</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_325" n="HIAT:w" s="T93">duː</ts>
                  <nts id="Seg_326" n="HIAT:ip">,</nts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_329" n="HIAT:w" s="T94">öjüːn</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_332" n="HIAT:w" s="T95">duː</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_335" n="HIAT:w" s="T96">keli͡ek</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_338" n="HIAT:w" s="T97">tustaːk</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_341" n="HIAT:w" s="T98">etilere</ts>
                  <nts id="Seg_342" n="HIAT:ip">.</nts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T106" id="Seg_345" n="HIAT:u" s="T99">
                  <ts e="T100" id="Seg_347" n="HIAT:w" s="T99">Kɨhɨn</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_350" n="HIAT:w" s="T100">karaŋa</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_353" n="HIAT:w" s="T101">kün</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_356" n="HIAT:w" s="T102">kɨlgas</ts>
                  <nts id="Seg_357" n="HIAT:ip">,</nts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_360" n="HIAT:w" s="T103">ki͡ehelere</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_363" n="HIAT:w" s="T104">olus</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_366" n="HIAT:w" s="T105">erde</ts>
                  <nts id="Seg_367" n="HIAT:ip">.</nts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_370" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_372" n="HIAT:w" s="T106">U͡ol</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_375" n="HIAT:w" s="T107">ogolor</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_378" n="HIAT:w" s="T108">ahɨːr</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_381" n="HIAT:w" s="T109">astarɨn</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_384" n="HIAT:w" s="T110">totu͡oktarɨgar</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_387" n="HIAT:w" s="T111">di͡eri</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_390" n="HIAT:w" s="T112">ahaːn</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_393" n="HIAT:w" s="T113">baraːn</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_396" n="HIAT:w" s="T114">utujaːrɨ</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_399" n="HIAT:w" s="T115">terimmittere</ts>
                  <nts id="Seg_400" n="HIAT:ip">.</nts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_403" n="HIAT:u" s="T116">
                  <nts id="Seg_404" n="HIAT:ip">"</nts>
                  <ts e="T117" id="Seg_406" n="HIAT:w" s="T116">Doː</ts>
                  <nts id="Seg_407" n="HIAT:ip">,</nts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_410" n="HIAT:w" s="T117">bihigi</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_413" n="HIAT:w" s="T118">erge</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_416" n="HIAT:w" s="T119">dʼi͡ebit</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_419" n="HIAT:w" s="T120">tüːn</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_422" n="HIAT:w" s="T121">kuttuːra</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_425" n="HIAT:w" s="T122">bu͡olu͡o</ts>
                  <nts id="Seg_426" n="HIAT:ip">"</nts>
                  <nts id="Seg_427" n="HIAT:ip">,</nts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_430" n="HIAT:w" s="T123">Mukulaːj</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_433" n="HIAT:w" s="T124">ubajɨgar</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_436" n="HIAT:w" s="T125">di͡ebite</ts>
                  <nts id="Seg_437" n="HIAT:ip">.</nts>
                  <nts id="Seg_438" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_440" n="HIAT:u" s="T126">
                  <nts id="Seg_441" n="HIAT:ip">"</nts>
                  <ts e="T127" id="Seg_443" n="HIAT:w" s="T126">En</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_446" n="HIAT:w" s="T127">emi͡e</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_449" n="HIAT:w" s="T128">kuhagannɨk</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_452" n="HIAT:w" s="T129">esten</ts>
                  <nts id="Seg_453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_455" n="HIAT:w" s="T130">eregin</ts>
                  <nts id="Seg_456" n="HIAT:ip">"</nts>
                  <nts id="Seg_457" n="HIAT:ip">,</nts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_460" n="HIAT:w" s="T131">ginini</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_463" n="HIAT:w" s="T132">ulakana</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_466" n="HIAT:w" s="T133">bu͡ojbuta</ts>
                  <nts id="Seg_467" n="HIAT:ip">.</nts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_470" n="HIAT:u" s="T134">
                  <nts id="Seg_471" n="HIAT:ip">"</nts>
                  <ts e="T135" id="Seg_473" n="HIAT:w" s="T134">Kata</ts>
                  <nts id="Seg_474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_476" n="HIAT:w" s="T135">hɨgɨnnʼaktan</ts>
                  <nts id="Seg_477" n="HIAT:ip">,</nts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_480" n="HIAT:w" s="T136">onno</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_483" n="HIAT:w" s="T137">oruŋŋa</ts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_486" n="HIAT:w" s="T138">hɨt</ts>
                  <nts id="Seg_487" n="HIAT:ip">.</nts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_490" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_492" n="HIAT:w" s="T139">Biːrge</ts>
                  <nts id="Seg_493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_495" n="HIAT:w" s="T140">utuju͡okput</ts>
                  <nts id="Seg_496" n="HIAT:ip">.</nts>
                  <nts id="Seg_497" n="HIAT:ip">"</nts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_500" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_502" n="HIAT:w" s="T141">Ubaja</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_505" n="HIAT:w" s="T142">ihiger</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_508" n="HIAT:w" s="T143">hin</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_511" n="HIAT:w" s="T144">daːgɨnɨ</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_514" n="HIAT:w" s="T145">dʼiksiner</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_517" n="HIAT:w" s="T146">bɨhɨlaːk</ts>
                  <nts id="Seg_518" n="HIAT:ip">.</nts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_521" n="HIAT:u" s="T147">
                  <ts e="T148" id="Seg_523" n="HIAT:w" s="T147">Ginner</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_526" n="HIAT:w" s="T148">iččitek</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_529" n="HIAT:w" s="T149">dʼi͡ege</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_532" n="HIAT:w" s="T150">kergene</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_535" n="HIAT:w" s="T151">hu͡ok</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_538" n="HIAT:w" s="T152">haŋardɨː</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_541" n="HIAT:w" s="T153">konollor</ts>
                  <nts id="Seg_542" n="HIAT:ip">.</nts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T163" id="Seg_545" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_547" n="HIAT:w" s="T154">Ubaja</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_550" n="HIAT:w" s="T155">laːmpatɨn</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_553" n="HIAT:w" s="T156">ututaːt</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_556" n="HIAT:w" s="T157">baltɨtɨn</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_559" n="HIAT:w" s="T158">kohunan</ts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_562" n="HIAT:w" s="T159">hu͡orgaːn</ts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_565" n="HIAT:w" s="T160">ihiger</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_568" n="HIAT:w" s="T161">kiːren</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_571" n="HIAT:w" s="T162">hɨppɨta</ts>
                  <nts id="Seg_572" n="HIAT:ip">.</nts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T168" id="Seg_575" n="HIAT:u" s="T163">
                  <ts e="T164" id="Seg_577" n="HIAT:w" s="T163">Ogolor</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_580" n="HIAT:w" s="T164">hu͡organnarɨn</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_583" n="HIAT:w" s="T165">ihiger</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_586" n="HIAT:w" s="T166">tɨːmmakka</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_589" n="HIAT:w" s="T167">hɨppɨttara</ts>
                  <nts id="Seg_590" n="HIAT:ip">.</nts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T171" id="Seg_593" n="HIAT:u" s="T168">
                  <ts e="T169" id="Seg_595" n="HIAT:w" s="T168">Kuttana</ts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_598" n="HIAT:w" s="T169">hanaːn</ts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_601" n="HIAT:w" s="T170">ihilliːller</ts>
                  <nts id="Seg_602" n="HIAT:ip">.</nts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T177" id="Seg_605" n="HIAT:u" s="T171">
                  <ts e="T172" id="Seg_607" n="HIAT:w" s="T171">Hotorukaːn</ts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_610" n="HIAT:w" s="T172">karaktarɨn</ts>
                  <nts id="Seg_611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_613" n="HIAT:w" s="T173">himen</ts>
                  <nts id="Seg_614" n="HIAT:ip">,</nts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_617" n="HIAT:w" s="T174">utujan</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_620" n="HIAT:w" s="T175">pahɨgɨraːn</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_623" n="HIAT:w" s="T176">kaːlbɨttara</ts>
                  <nts id="Seg_624" n="HIAT:ip">.</nts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_627" n="HIAT:u" s="T177">
                  <ts e="T178" id="Seg_629" n="HIAT:w" s="T177">Töhö</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_632" n="HIAT:w" s="T178">kačča</ts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_635" n="HIAT:w" s="T179">bu͡olbuta</ts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_638" n="HIAT:w" s="T180">dʼürü</ts>
                  <nts id="Seg_639" n="HIAT:ip">.</nts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_642" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_644" n="HIAT:w" s="T181">Mikaː</ts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_647" n="HIAT:w" s="T182">baltɨta</ts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_650" n="HIAT:w" s="T183">ojogohugar</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_653" n="HIAT:w" s="T184">ütü͡ölüːrütten</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_656" n="HIAT:w" s="T185">uhuktubuta</ts>
                  <nts id="Seg_657" n="HIAT:ip">.</nts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T195" id="Seg_660" n="HIAT:u" s="T186">
                  <nts id="Seg_661" n="HIAT:ip">"</nts>
                  <ts e="T187" id="Seg_663" n="HIAT:w" s="T186">Ubaː</ts>
                  <nts id="Seg_664" n="HIAT:ip">,</nts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_667" n="HIAT:w" s="T187">ihilleː</ts>
                  <nts id="Seg_668" n="HIAT:ip">,</nts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_671" n="HIAT:w" s="T188">kuttuːr</ts>
                  <nts id="Seg_672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_674" n="HIAT:w" s="T189">bɨhɨlaːk</ts>
                  <nts id="Seg_675" n="HIAT:ip">"</nts>
                  <nts id="Seg_676" n="HIAT:ip">,</nts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_679" n="HIAT:w" s="T190">Mukulaːj</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_682" n="HIAT:w" s="T191">kuttanan</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_685" n="HIAT:w" s="T192">haŋata</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_688" n="HIAT:w" s="T193">arɨččɨ</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_691" n="HIAT:w" s="T194">taksɨbɨta</ts>
                  <nts id="Seg_692" n="HIAT:ip">.</nts>
                  <nts id="Seg_693" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T200" id="Seg_695" n="HIAT:u" s="T195">
                  <ts e="T196" id="Seg_697" n="HIAT:w" s="T195">Mikaː</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_700" n="HIAT:w" s="T196">hu͡organɨn</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_703" n="HIAT:w" s="T197">kɨrɨːtɨn</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_706" n="HIAT:w" s="T198">kötögön</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_709" n="HIAT:w" s="T199">ihilleːbite</ts>
                  <nts id="Seg_710" n="HIAT:ip">.</nts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T206" id="Seg_713" n="HIAT:u" s="T200">
                  <ts e="T201" id="Seg_715" n="HIAT:w" s="T200">Kirdik</ts>
                  <nts id="Seg_716" n="HIAT:ip">,</nts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_719" n="HIAT:w" s="T201">ahɨːr</ts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_722" n="HIAT:w" s="T202">ostoːllorugar</ts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_725" n="HIAT:w" s="T203">teri͡elkeler</ts>
                  <nts id="Seg_726" n="HIAT:ip">,</nts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_729" n="HIAT:w" s="T204">lu͡oskalar</ts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_732" n="HIAT:w" s="T205">lahɨrgɨːllar</ts>
                  <nts id="Seg_733" n="HIAT:ip">.</nts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T210" id="Seg_736" n="HIAT:u" s="T206">
                  <ts e="T207" id="Seg_738" n="HIAT:w" s="T206">Kabɨs-karaŋa</ts>
                  <nts id="Seg_739" n="HIAT:ip">,</nts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_742" n="HIAT:w" s="T207">tu͡ok</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_745" n="HIAT:w" s="T208">daː</ts>
                  <nts id="Seg_746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_748" n="HIAT:w" s="T209">köstübet</ts>
                  <nts id="Seg_749" n="HIAT:ip">.</nts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T220" id="Seg_752" n="HIAT:u" s="T210">
                  <ts e="T211" id="Seg_754" n="HIAT:w" s="T210">Kaja</ts>
                  <nts id="Seg_755" n="HIAT:ip">,</nts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_758" n="HIAT:w" s="T211">ɨraːk</ts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_761" n="HIAT:w" s="T212">turar</ts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_764" n="HIAT:w" s="T213">ostoːl</ts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_767" n="HIAT:w" s="T214">ürdüger</ts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_770" n="HIAT:w" s="T215">kumaːkɨlarɨ</ts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_773" n="HIAT:w" s="T216">tu͡ok</ts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_776" n="HIAT:w" s="T217">ere</ts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_778" n="HIAT:ip">(</nts>
                  <ts e="T219" id="Seg_780" n="HIAT:w" s="T218">kajɨtɨlɨːr</ts>
                  <nts id="Seg_781" n="HIAT:ip">)</nts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_784" n="HIAT:w" s="T219">kajɨtallɨːr</ts>
                  <nts id="Seg_785" n="HIAT:ip">.</nts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T228" id="Seg_788" n="HIAT:u" s="T220">
                  <ts e="T221" id="Seg_790" n="HIAT:w" s="T220">Ogolor</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_793" n="HIAT:w" s="T221">hu͡organ</ts>
                  <nts id="Seg_794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_796" n="HIAT:w" s="T222">ihiger</ts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_799" n="HIAT:w" s="T223">kuːstahan</ts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_802" n="HIAT:w" s="T224">baraːn</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_805" n="HIAT:w" s="T225">kuttanan</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_808" n="HIAT:w" s="T226">tɨːmmakka</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_811" n="HIAT:w" s="T227">hɨtallar</ts>
                  <nts id="Seg_812" n="HIAT:ip">.</nts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T234" id="Seg_815" n="HIAT:u" s="T228">
                  <ts e="T229" id="Seg_817" n="HIAT:w" s="T228">Tahaːra</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_820" n="HIAT:w" s="T229">güːlege</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_823" n="HIAT:w" s="T230">tu͡ok</ts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_826" n="HIAT:w" s="T231">ere</ts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_829" n="HIAT:w" s="T232">kaːmalɨːra</ts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_832" n="HIAT:w" s="T233">ihillibite</ts>
                  <nts id="Seg_833" n="HIAT:ip">.</nts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T238" id="Seg_836" n="HIAT:u" s="T234">
                  <ts e="T235" id="Seg_838" n="HIAT:w" s="T234">Dʼi͡e</ts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_841" n="HIAT:w" s="T235">isti͡enetin</ts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_844" n="HIAT:w" s="T236">tɨŋnɨk</ts>
                  <nts id="Seg_845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_847" n="HIAT:w" s="T237">oksu͡oluːr</ts>
                  <nts id="Seg_848" n="HIAT:ip">.</nts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T244" id="Seg_851" n="HIAT:u" s="T238">
                  <ts e="T239" id="Seg_853" n="HIAT:w" s="T238">Koloru͡oktartan</ts>
                  <nts id="Seg_854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_856" n="HIAT:w" s="T239">holuːrdarɨ</ts>
                  <nts id="Seg_857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_859" n="HIAT:w" s="T240">bütte</ts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_862" n="HIAT:w" s="T241">ihitteri</ts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_865" n="HIAT:w" s="T242">hirge</ts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_868" n="HIAT:w" s="T243">tühertiːr</ts>
                  <nts id="Seg_869" n="HIAT:ip">.</nts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T248" id="Seg_872" n="HIAT:u" s="T244">
                  <ts e="T245" id="Seg_874" n="HIAT:w" s="T244">Onton</ts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_877" n="HIAT:w" s="T245">oloru</ts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_880" n="HIAT:w" s="T246">tebi͡eliːre</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_883" n="HIAT:w" s="T247">ihiller</ts>
                  <nts id="Seg_884" n="HIAT:ip">.</nts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T249" id="Seg_887" n="HIAT:u" s="T248">
                  <nts id="Seg_888" n="HIAT:ip">"</nts>
                  <ts e="T249" id="Seg_890" n="HIAT:w" s="T248">Uču</ts>
                  <nts id="Seg_891" n="HIAT:ip">!</nts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T254" id="Seg_894" n="HIAT:u" s="T249">
                  <ts e="T250" id="Seg_896" n="HIAT:w" s="T249">Mömpökkö</ts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_899" n="HIAT:w" s="T250">hɨt</ts>
                  <nts id="Seg_900" n="HIAT:ip">"</nts>
                  <nts id="Seg_901" n="HIAT:ip">,</nts>
                  <nts id="Seg_902" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_904" n="HIAT:w" s="T251">Mikaː</ts>
                  <nts id="Seg_905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_907" n="HIAT:w" s="T252">baltɨtɨn</ts>
                  <nts id="Seg_908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_910" n="HIAT:w" s="T253">bu͡ojbuta</ts>
                  <nts id="Seg_911" n="HIAT:ip">.</nts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T261" id="Seg_914" n="HIAT:u" s="T254">
                  <ts e="T255" id="Seg_916" n="HIAT:w" s="T254">Dʼi͡e</ts>
                  <nts id="Seg_917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_919" n="HIAT:w" s="T255">ihiger</ts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_922" n="HIAT:w" s="T256">güːlege</ts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_925" n="HIAT:w" s="T257">aːjdaːn</ts>
                  <nts id="Seg_926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_928" n="HIAT:w" s="T258">kahan</ts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_931" n="HIAT:w" s="T259">daː</ts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_934" n="HIAT:w" s="T260">büppet</ts>
                  <nts id="Seg_935" n="HIAT:ip">.</nts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T265" id="Seg_938" n="HIAT:u" s="T261">
                  <ts e="T262" id="Seg_940" n="HIAT:w" s="T261">Harsi͡erdaga</ts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_943" n="HIAT:w" s="T262">di͡eri</ts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_946" n="HIAT:w" s="T263">össü͡ö</ts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_949" n="HIAT:w" s="T264">ɨraːk</ts>
                  <nts id="Seg_950" n="HIAT:ip">.</nts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T269" id="Seg_953" n="HIAT:u" s="T265">
                  <ts e="T266" id="Seg_955" n="HIAT:w" s="T265">Ginneri</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_958" n="HIAT:w" s="T266">kim</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_961" n="HIAT:w" s="T267">kelen</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_964" n="HIAT:w" s="T268">bɨːhɨ͡aj</ts>
                  <nts id="Seg_965" n="HIAT:ip">.</nts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T276" id="Seg_968" n="HIAT:u" s="T269">
                  <ts e="T270" id="Seg_970" n="HIAT:w" s="T269">Ogolor</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_973" n="HIAT:w" s="T270">kaːttaran</ts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_976" n="HIAT:w" s="T271">ɨtɨ͡aktarɨn</ts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_979" n="HIAT:w" s="T272">daː</ts>
                  <nts id="Seg_980" n="HIAT:ip">,</nts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_983" n="HIAT:w" s="T273">ü͡ögülü͡ökterin</ts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_986" n="HIAT:w" s="T274">daː</ts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_989" n="HIAT:w" s="T275">bert</ts>
                  <nts id="Seg_990" n="HIAT:ip">.</nts>
                  <nts id="Seg_991" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T283" id="Seg_993" n="HIAT:u" s="T276">
                  <ts e="T277" id="Seg_995" n="HIAT:w" s="T276">Ol</ts>
                  <nts id="Seg_996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_998" n="HIAT:w" s="T277">kuttana</ts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1001" n="HIAT:w" s="T278">hɨtannar</ts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1004" n="HIAT:w" s="T279">uːlarɨgar</ts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1007" n="HIAT:w" s="T280">battatan</ts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1010" n="HIAT:w" s="T281">utujan</ts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1013" n="HIAT:w" s="T282">kaːlbɨttara</ts>
                  <nts id="Seg_1014" n="HIAT:ip">.</nts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T289" id="Seg_1017" n="HIAT:u" s="T283">
                  <ts e="T284" id="Seg_1019" n="HIAT:w" s="T283">Tu͡oktaːgar</ts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1022" n="HIAT:w" s="T284">da</ts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1025" n="HIAT:w" s="T285">ulakan</ts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1028" n="HIAT:w" s="T286">aːjdaːntan</ts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1031" n="HIAT:w" s="T287">emiske</ts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1034" n="HIAT:w" s="T288">uhuktubuttara</ts>
                  <nts id="Seg_1035" n="HIAT:ip">.</nts>
                  <nts id="Seg_1036" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T296" id="Seg_1038" n="HIAT:u" s="T289">
                  <ts e="T290" id="Seg_1040" n="HIAT:w" s="T289">Güːlege</ts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1043" n="HIAT:w" s="T290">tu͡ok</ts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1046" n="HIAT:w" s="T291">ere</ts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1049" n="HIAT:w" s="T292">kɨtaːnaktɨk</ts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1052" n="HIAT:w" s="T293">taːstɨː</ts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1055" n="HIAT:w" s="T294">huːlan</ts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1058" n="HIAT:w" s="T295">tüspüte</ts>
                  <nts id="Seg_1059" n="HIAT:ip">.</nts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T299" id="Seg_1062" n="HIAT:u" s="T296">
                  <ts e="T297" id="Seg_1064" n="HIAT:w" s="T296">Ü͡ögüː</ts>
                  <nts id="Seg_1065" n="HIAT:ip">,</nts>
                  <nts id="Seg_1066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1068" n="HIAT:w" s="T297">kahɨː</ts>
                  <nts id="Seg_1069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1071" n="HIAT:w" s="T298">ihillibite</ts>
                  <nts id="Seg_1072" n="HIAT:ip">.</nts>
                  <nts id="Seg_1073" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T301" id="Seg_1075" n="HIAT:u" s="T299">
                  <ts e="T300" id="Seg_1077" n="HIAT:w" s="T299">Aːn</ts>
                  <nts id="Seg_1078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1080" n="HIAT:w" s="T300">arɨllɨbɨta</ts>
                  <nts id="Seg_1081" n="HIAT:ip">.</nts>
                  <nts id="Seg_1082" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T307" id="Seg_1084" n="HIAT:u" s="T301">
                  <ts e="T302" id="Seg_1086" n="HIAT:w" s="T301">Dʼi͡ege</ts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1089" n="HIAT:w" s="T302">kim</ts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1092" n="HIAT:w" s="T303">ere</ts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1095" n="HIAT:w" s="T304">kiːren</ts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1098" n="HIAT:w" s="T305">laːmpanɨ</ts>
                  <nts id="Seg_1099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1101" n="HIAT:w" s="T306">ubappɨta</ts>
                  <nts id="Seg_1102" n="HIAT:ip">.</nts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T313" id="Seg_1105" n="HIAT:u" s="T307">
                  <nts id="Seg_1106" n="HIAT:ip">"</nts>
                  <ts e="T308" id="Seg_1108" n="HIAT:w" s="T307">Turuŋ</ts>
                  <nts id="Seg_1109" n="HIAT:ip">,</nts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1112" n="HIAT:w" s="T308">ehigi</ts>
                  <nts id="Seg_1113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1115" n="HIAT:w" s="T309">güːlegit</ts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1118" n="HIAT:w" s="T310">aːnɨn</ts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1121" n="HIAT:w" s="T311">togo</ts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1124" n="HIAT:w" s="T312">happatakkɨtɨj</ts>
                  <nts id="Seg_1125" n="HIAT:ip">?</nts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T325" id="Seg_1128" n="HIAT:u" s="T313">
                  <ts e="T314" id="Seg_1130" n="HIAT:w" s="T313">Koru͡obalar</ts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1133" n="HIAT:w" s="T314">kiːrenner</ts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1136" n="HIAT:w" s="T315">hiri</ts>
                  <nts id="Seg_1137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1139" n="HIAT:w" s="T316">delbi</ts>
                  <nts id="Seg_1140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1142" n="HIAT:w" s="T317">tepsibitter</ts>
                  <nts id="Seg_1143" n="HIAT:ip">,</nts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1146" n="HIAT:w" s="T318">güːle</ts>
                  <nts id="Seg_1147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1149" n="HIAT:w" s="T319">ihin</ts>
                  <nts id="Seg_1150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1152" n="HIAT:w" s="T320">biːr</ts>
                  <nts id="Seg_1153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1155" n="HIAT:w" s="T321">oŋorbuttar</ts>
                  <nts id="Seg_1156" n="HIAT:ip">,</nts>
                  <nts id="Seg_1157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1159" n="HIAT:w" s="T322">hiri</ts>
                  <nts id="Seg_1160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1162" n="HIAT:w" s="T323">toloru</ts>
                  <nts id="Seg_1163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1165" n="HIAT:w" s="T324">haːktaːbɨttar</ts>
                  <nts id="Seg_1166" n="HIAT:ip">.</nts>
                  <nts id="Seg_1167" n="HIAT:ip">"</nts>
                  <nts id="Seg_1168" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T331" id="Seg_1170" n="HIAT:u" s="T325">
                  <ts e="T326" id="Seg_1172" n="HIAT:w" s="T325">Ogolor</ts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1175" n="HIAT:w" s="T326">karaktarɨn</ts>
                  <nts id="Seg_1176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1178" n="HIAT:w" s="T327">arɨjannar</ts>
                  <nts id="Seg_1179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1181" n="HIAT:w" s="T328">kihi</ts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1184" n="HIAT:w" s="T329">turarɨn</ts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1187" n="HIAT:w" s="T330">körbüttere</ts>
                  <nts id="Seg_1188" n="HIAT:ip">.</nts>
                  <nts id="Seg_1189" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T341" id="Seg_1191" n="HIAT:u" s="T331">
                  <ts e="T332" id="Seg_1193" n="HIAT:w" s="T331">Ontulara</ts>
                  <nts id="Seg_1194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1196" n="HIAT:w" s="T332">abalaha-abalaha</ts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1199" n="HIAT:w" s="T333">taŋahɨn</ts>
                  <nts id="Seg_1200" n="HIAT:ip">,</nts>
                  <nts id="Seg_1201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1203" n="HIAT:w" s="T334">galipi͡e</ts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1206" n="HIAT:w" s="T335">hɨ͡alɨjatɨn</ts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1209" n="HIAT:w" s="T336">haːkka</ts>
                  <nts id="Seg_1210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1212" n="HIAT:w" s="T337">bihillibitin</ts>
                  <nts id="Seg_1213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1215" n="HIAT:w" s="T338">iliːlerinen</ts>
                  <nts id="Seg_1216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1218" n="HIAT:w" s="T339">hoto</ts>
                  <nts id="Seg_1219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1221" n="HIAT:w" s="T340">turbuta</ts>
                  <nts id="Seg_1222" n="HIAT:ip">.</nts>
                  <nts id="Seg_1223" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T354" id="Seg_1225" n="HIAT:u" s="T341">
                  <ts e="T342" id="Seg_1227" n="HIAT:w" s="T341">Opu͡o</ts>
                  <nts id="Seg_1228" n="HIAT:ip">,</nts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1231" n="HIAT:w" s="T342">kolxoz</ts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1234" n="HIAT:w" s="T343">sopxu͡oha</ts>
                  <nts id="Seg_1235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1236" n="HIAT:ip">(</nts>
                  <ts e="T345" id="Seg_1238" n="HIAT:w" s="T344">ületiger</ts>
                  <nts id="Seg_1239" n="HIAT:ip">)</nts>
                  <nts id="Seg_1240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1242" n="HIAT:w" s="T345">ületiger</ts>
                  <nts id="Seg_1243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1245" n="HIAT:w" s="T346">dʼi͡e</ts>
                  <nts id="Seg_1246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1248" n="HIAT:w" s="T347">attɨnan</ts>
                  <nts id="Seg_1249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1251" n="HIAT:w" s="T348">baran</ts>
                  <nts id="Seg_1252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1254" n="HIAT:w" s="T349">ihen</ts>
                  <nts id="Seg_1255" n="HIAT:ip">,</nts>
                  <nts id="Seg_1256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1258" n="HIAT:w" s="T350">güːle</ts>
                  <nts id="Seg_1259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1261" n="HIAT:w" s="T351">koru͡obalar</ts>
                  <nts id="Seg_1262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1264" n="HIAT:w" s="T352">kiːrbitterin</ts>
                  <nts id="Seg_1265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1267" n="HIAT:w" s="T353">körbüt</ts>
                  <nts id="Seg_1268" n="HIAT:ip">.</nts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T358" id="Seg_1271" n="HIAT:u" s="T354">
                  <ts e="T355" id="Seg_1273" n="HIAT:w" s="T354">Onton</ts>
                  <nts id="Seg_1274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1276" n="HIAT:w" s="T355">barɨlarɨn</ts>
                  <nts id="Seg_1277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1279" n="HIAT:w" s="T356">batan</ts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1282" n="HIAT:w" s="T357">tahaːrbɨta</ts>
                  <nts id="Seg_1283" n="HIAT:ip">.</nts>
                  <nts id="Seg_1284" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T368" id="Seg_1286" n="HIAT:u" s="T358">
                  <ts e="T359" id="Seg_1288" n="HIAT:w" s="T358">Ol</ts>
                  <nts id="Seg_1289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1291" n="HIAT:w" s="T359">kennitten</ts>
                  <nts id="Seg_1292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1294" n="HIAT:w" s="T360">kiːren</ts>
                  <nts id="Seg_1295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1297" n="HIAT:w" s="T361">ihen</ts>
                  <nts id="Seg_1298" n="HIAT:ip">,</nts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1301" n="HIAT:w" s="T362">kaltɨrɨjan</ts>
                  <nts id="Seg_1302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1304" n="HIAT:w" s="T363">tühen</ts>
                  <nts id="Seg_1305" n="HIAT:ip">,</nts>
                  <nts id="Seg_1306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1308" n="HIAT:w" s="T364">koru͡obalar</ts>
                  <nts id="Seg_1309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1311" n="HIAT:w" s="T365">haːktarɨgar</ts>
                  <nts id="Seg_1312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1314" n="HIAT:w" s="T366">delbi</ts>
                  <nts id="Seg_1315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1317" n="HIAT:w" s="T367">bihillibite</ts>
                  <nts id="Seg_1318" n="HIAT:ip">.</nts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T374" id="Seg_1321" n="HIAT:u" s="T368">
                  <ts e="T369" id="Seg_1323" n="HIAT:w" s="T368">Tura</ts>
                  <nts id="Seg_1324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1326" n="HIAT:w" s="T369">hataːn</ts>
                  <nts id="Seg_1327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1329" n="HIAT:w" s="T370">iliːlerin</ts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1332" n="HIAT:w" s="T371">emi͡e</ts>
                  <nts id="Seg_1333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1335" n="HIAT:w" s="T372">olus</ts>
                  <nts id="Seg_1336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1338" n="HIAT:w" s="T373">bispite</ts>
                  <nts id="Seg_1339" n="HIAT:ip">.</nts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T382" id="Seg_1342" n="HIAT:u" s="T374">
                  <ts e="T375" id="Seg_1344" n="HIAT:w" s="T374">Mikaːnɨ</ts>
                  <nts id="Seg_1345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1347" n="HIAT:w" s="T375">kɨtta</ts>
                  <nts id="Seg_1348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1350" n="HIAT:w" s="T376">Mukulaːj</ts>
                  <nts id="Seg_1351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1353" n="HIAT:w" s="T377">kɨjŋammɨt</ts>
                  <nts id="Seg_1354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1356" n="HIAT:w" s="T378">kihi</ts>
                  <nts id="Seg_1357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1359" n="HIAT:w" s="T379">kelbitiger</ts>
                  <nts id="Seg_1360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1362" n="HIAT:w" s="T380">ulakannɨk</ts>
                  <nts id="Seg_1363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1365" n="HIAT:w" s="T381">ü͡örbüttere</ts>
                  <nts id="Seg_1366" n="HIAT:ip">.</nts>
                  <nts id="Seg_1367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T389" id="Seg_1369" n="HIAT:u" s="T382">
                  <ts e="T383" id="Seg_1371" n="HIAT:w" s="T382">Mikaː</ts>
                  <nts id="Seg_1372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1374" n="HIAT:w" s="T383">bilbet</ts>
                  <nts id="Seg_1375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1377" n="HIAT:w" s="T384">kihititten</ts>
                  <nts id="Seg_1378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1380" n="HIAT:w" s="T385">atɨŋɨrgaːn</ts>
                  <nts id="Seg_1381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1383" n="HIAT:w" s="T386">karagɨn</ts>
                  <nts id="Seg_1384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1386" n="HIAT:w" s="T387">kistiː-kistiː</ts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1389" n="HIAT:w" s="T388">di͡ebite</ts>
                  <nts id="Seg_1390" n="HIAT:ip">:</nts>
                  <nts id="Seg_1391" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T394" id="Seg_1393" n="HIAT:u" s="T389">
                  <nts id="Seg_1394" n="HIAT:ip">"</nts>
                  <ts e="T390" id="Seg_1396" n="HIAT:w" s="T389">Bihigini</ts>
                  <nts id="Seg_1397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1399" n="HIAT:w" s="T390">tüːn</ts>
                  <nts id="Seg_1400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1402" n="HIAT:w" s="T391">kuttaːbɨta</ts>
                  <nts id="Seg_1403" n="HIAT:ip">,</nts>
                  <nts id="Seg_1404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1406" n="HIAT:w" s="T392">koton</ts>
                  <nts id="Seg_1407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1409" n="HIAT:w" s="T393">utujbatɨbɨt</ts>
                  <nts id="Seg_1410" n="HIAT:ip">.</nts>
                  <nts id="Seg_1411" n="HIAT:ip">"</nts>
                  <nts id="Seg_1412" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T400" id="Seg_1414" n="HIAT:u" s="T394">
                  <ts e="T395" id="Seg_1416" n="HIAT:w" s="T394">Tüːnü</ts>
                  <nts id="Seg_1417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1419" n="HIAT:w" s="T395">huptu</ts>
                  <nts id="Seg_1420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1422" n="HIAT:w" s="T396">tu͡ok</ts>
                  <nts id="Seg_1423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1425" n="HIAT:w" s="T397">bu͡olbutun</ts>
                  <nts id="Seg_1426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1428" n="HIAT:w" s="T398">barɨtɨn</ts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1431" n="HIAT:w" s="T399">kepseːbite</ts>
                  <nts id="Seg_1432" n="HIAT:ip">.</nts>
                  <nts id="Seg_1433" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T407" id="Seg_1435" n="HIAT:u" s="T400">
                  <ts e="T401" id="Seg_1437" n="HIAT:w" s="T400">Opu͡o</ts>
                  <nts id="Seg_1438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1440" n="HIAT:w" s="T401">ogolor</ts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1443" n="HIAT:w" s="T402">kuttammɨt</ts>
                  <nts id="Seg_1444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1446" n="HIAT:w" s="T403">dʼühünnerin</ts>
                  <nts id="Seg_1447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1449" n="HIAT:w" s="T404">körön</ts>
                  <nts id="Seg_1450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1452" n="HIAT:w" s="T405">ginneri</ts>
                  <nts id="Seg_1453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1455" n="HIAT:w" s="T406">ahɨmmɨta</ts>
                  <nts id="Seg_1456" n="HIAT:ip">.</nts>
                  <nts id="Seg_1457" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T410" id="Seg_1459" n="HIAT:u" s="T407">
                  <ts e="T408" id="Seg_1461" n="HIAT:w" s="T407">Kɨjŋammɨta</ts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1464" n="HIAT:w" s="T408">honno</ts>
                  <nts id="Seg_1465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1467" n="HIAT:w" s="T409">aːspɨta</ts>
                  <nts id="Seg_1468" n="HIAT:ip">.</nts>
                  <nts id="Seg_1469" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T413" id="Seg_1471" n="HIAT:u" s="T410">
                  <ts e="T411" id="Seg_1473" n="HIAT:w" s="T410">Küle-küle</ts>
                  <nts id="Seg_1474" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1476" n="HIAT:w" s="T411">hordoːktorugar</ts>
                  <nts id="Seg_1477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1479" n="HIAT:w" s="T412">haŋarbɨta</ts>
                  <nts id="Seg_1480" n="HIAT:ip">:</nts>
                  <nts id="Seg_1481" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T419" id="Seg_1483" n="HIAT:u" s="T413">
                  <nts id="Seg_1484" n="HIAT:ip">"</nts>
                  <ts e="T414" id="Seg_1486" n="HIAT:w" s="T413">Tüːn</ts>
                  <nts id="Seg_1487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1489" n="HIAT:w" s="T414">ehigini</ts>
                  <nts id="Seg_1490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1492" n="HIAT:w" s="T415">koru͡obalarɨ</ts>
                  <nts id="Seg_1493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1495" n="HIAT:w" s="T416">gɨtta</ts>
                  <nts id="Seg_1496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1498" n="HIAT:w" s="T417">kutujaktar</ts>
                  <nts id="Seg_1499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1501" n="HIAT:w" s="T418">kuttaːbɨttara</ts>
                  <nts id="Seg_1502" n="HIAT:ip">.</nts>
                  <nts id="Seg_1503" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T421" id="Seg_1505" n="HIAT:u" s="T419">
                  <ts e="T420" id="Seg_1507" n="HIAT:w" s="T419">Haːppakkɨt</ts>
                  <nts id="Seg_1508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1510" n="HIAT:w" s="T420">du͡o</ts>
                  <nts id="Seg_1511" n="HIAT:ip">?</nts>
                  <nts id="Seg_1512" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T425" id="Seg_1514" n="HIAT:u" s="T421">
                  <ts e="T422" id="Seg_1516" n="HIAT:w" s="T421">Tuhata</ts>
                  <nts id="Seg_1517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1519" n="HIAT:w" s="T422">hu͡oktan</ts>
                  <nts id="Seg_1520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1522" n="HIAT:w" s="T423">kuttammɨkkɨtɨn</ts>
                  <nts id="Seg_1523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1525" n="HIAT:w" s="T424">bileŋŋit</ts>
                  <nts id="Seg_1526" n="HIAT:ip">?</nts>
                  <nts id="Seg_1527" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T433" id="Seg_1529" n="HIAT:u" s="T425">
                  <ts e="T426" id="Seg_1531" n="HIAT:w" s="T425">Ehigi</ts>
                  <nts id="Seg_1532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1534" n="HIAT:w" s="T426">kuhagaŋŋɨtɨttan</ts>
                  <nts id="Seg_1535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1536" n="HIAT:ip">(</nts>
                  <ts e="T428" id="Seg_1538" n="HIAT:w" s="T427">min</ts>
                  <nts id="Seg_1539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428.tx.1" id="Seg_1541" n="HIAT:w" s="T428">ɨs</ts>
                  <nts id="Seg_1542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1544" n="HIAT:w" s="T428.tx.1">-</ts>
                  <nts id="Seg_1545" n="HIAT:ip">)</nts>
                  <nts id="Seg_1546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1548" n="HIAT:w" s="T430">min</ts>
                  <nts id="Seg_1549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1551" n="HIAT:w" s="T431">olus</ts>
                  <nts id="Seg_1552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1554" n="HIAT:w" s="T432">bihilinnim</ts>
                  <nts id="Seg_1555" n="HIAT:ip">.</nts>
                  <nts id="Seg_1556" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T443" id="Seg_1558" n="HIAT:u" s="T433">
                  <ts e="T434" id="Seg_1560" n="HIAT:w" s="T433">Inʼegit</ts>
                  <nts id="Seg_1561" n="HIAT:ip">,</nts>
                  <nts id="Seg_1562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1564" n="HIAT:w" s="T434">agagɨt</ts>
                  <nts id="Seg_1565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1567" n="HIAT:w" s="T435">keli͡ekteriger</ts>
                  <nts id="Seg_1568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1570" n="HIAT:w" s="T436">di͡eri</ts>
                  <nts id="Seg_1571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1573" n="HIAT:w" s="T437">güːlegitin</ts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1576" n="HIAT:w" s="T438">gɨtta</ts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1579" n="HIAT:w" s="T439">dʼi͡egit</ts>
                  <nts id="Seg_1580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1582" n="HIAT:w" s="T440">ihin</ts>
                  <nts id="Seg_1583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1585" n="HIAT:w" s="T441">üčügejdik</ts>
                  <nts id="Seg_1586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1588" n="HIAT:w" s="T442">karajɨŋ</ts>
                  <nts id="Seg_1589" n="HIAT:ip">.</nts>
                  <nts id="Seg_1590" n="HIAT:ip">"</nts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T448" id="Seg_1593" n="HIAT:u" s="T443">
                  <ts e="T444" id="Seg_1595" n="HIAT:w" s="T443">Opu͡o</ts>
                  <nts id="Seg_1596" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1598" n="HIAT:w" s="T444">taksaːrɨ</ts>
                  <nts id="Seg_1599" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1601" n="HIAT:w" s="T445">turan</ts>
                  <nts id="Seg_1602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1604" n="HIAT:w" s="T446">ogoloru</ts>
                  <nts id="Seg_1605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1607" n="HIAT:w" s="T447">mohujbuta</ts>
                  <nts id="Seg_1608" n="HIAT:ip">.</nts>
                  <nts id="Seg_1609" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T448" id="Seg_1610" n="sc" s="T0">
               <ts e="T1" id="Seg_1612" n="e" s="T0">Kuttaːbɨta. </ts>
               <ts e="T2" id="Seg_1614" n="e" s="T1">Ikki </ts>
               <ts e="T3" id="Seg_1616" n="e" s="T2">u͡ol </ts>
               <ts e="T4" id="Seg_1618" n="e" s="T3">ogolor </ts>
               <ts e="T5" id="Seg_1620" n="e" s="T4">Mikaːnɨ </ts>
               <ts e="T6" id="Seg_1622" n="e" s="T5">kɨtta </ts>
               <ts e="T7" id="Seg_1624" n="e" s="T6">Mukulaːj </ts>
               <ts e="T8" id="Seg_1626" n="e" s="T7">kergettere </ts>
               <ts e="T9" id="Seg_1628" n="e" s="T8">barbɨttarɨn </ts>
               <ts e="T10" id="Seg_1630" n="e" s="T9">kenne </ts>
               <ts e="T11" id="Seg_1632" n="e" s="T10">dʼi͡eleriger </ts>
               <ts e="T12" id="Seg_1634" n="e" s="T11">kommuttara. </ts>
               <ts e="T13" id="Seg_1636" n="e" s="T12">Giniler </ts>
               <ts e="T14" id="Seg_1638" n="e" s="T13">ü͡örener </ts>
               <ts e="T15" id="Seg_1640" n="e" s="T14">kemneriger </ts>
               <ts e="T16" id="Seg_1642" n="e" s="T15">kɨhɨnɨ </ts>
               <ts e="T17" id="Seg_1644" n="e" s="T16">huptu </ts>
               <ts e="T18" id="Seg_1646" n="e" s="T17">intʼernaːkka </ts>
               <ts e="T19" id="Seg_1648" n="e" s="T18">bu͡olaːččɨlar. </ts>
               <ts e="T20" id="Seg_1650" n="e" s="T19">Bɨlɨrgɨ </ts>
               <ts e="T21" id="Seg_1652" n="e" s="T20">nʼuːčča </ts>
               <ts e="T22" id="Seg_1654" n="e" s="T21">dʼi͡e </ts>
               <ts e="T23" id="Seg_1656" n="e" s="T22">isti͡enetin </ts>
               <ts e="T24" id="Seg_1658" n="e" s="T23">mastara </ts>
               <ts e="T25" id="Seg_1660" n="e" s="T24">elete </ts>
               <ts e="T26" id="Seg_1662" n="e" s="T25">bu͡olannar </ts>
               <ts e="T27" id="Seg_1664" n="e" s="T26">olus </ts>
               <ts e="T29" id="Seg_1666" n="e" s="T27">(emir -) </ts>
               <ts e="T30" id="Seg_1668" n="e" s="T29">emegerbitter. </ts>
               <ts e="T31" id="Seg_1670" n="e" s="T30">Tahaːraːgɨ </ts>
               <ts e="T32" id="Seg_1672" n="e" s="T31">güːletin </ts>
               <ts e="T33" id="Seg_1674" n="e" s="T32">aːna </ts>
               <ts e="T34" id="Seg_1676" n="e" s="T33">kihi </ts>
               <ts e="T35" id="Seg_1678" n="e" s="T34">arɨjdagɨna </ts>
               <ts e="T36" id="Seg_1680" n="e" s="T35">haːrkɨrɨːra </ts>
               <ts e="T37" id="Seg_1682" n="e" s="T36">bert. </ts>
               <ts e="T38" id="Seg_1684" n="e" s="T37">Dʼi͡e </ts>
               <ts e="T39" id="Seg_1686" n="e" s="T38">ihiger, </ts>
               <ts e="T40" id="Seg_1688" n="e" s="T39">töhö </ts>
               <ts e="T41" id="Seg_1690" n="e" s="T40">da </ts>
               <ts e="T42" id="Seg_1692" n="e" s="T41">aːjdaːn </ts>
               <ts e="T43" id="Seg_1694" n="e" s="T42">bu͡ollun, </ts>
               <ts e="T44" id="Seg_1696" n="e" s="T43">kim </ts>
               <ts e="T45" id="Seg_1698" n="e" s="T44">eme </ts>
               <ts e="T46" id="Seg_1700" n="e" s="T45">arɨjdagɨna, </ts>
               <ts e="T47" id="Seg_1702" n="e" s="T46">haːrkɨrɨːr </ts>
               <ts e="T48" id="Seg_1704" n="e" s="T47">tɨ͡aha </ts>
               <ts e="T49" id="Seg_1706" n="e" s="T48">honno </ts>
               <ts e="T50" id="Seg_1708" n="e" s="T49">ihiller. </ts>
               <ts e="T51" id="Seg_1710" n="e" s="T50">Horok </ts>
               <ts e="T52" id="Seg_1712" n="e" s="T51">kenne </ts>
               <ts e="T53" id="Seg_1714" n="e" s="T52">tɨ͡al </ts>
               <ts e="T54" id="Seg_1716" n="e" s="T53">okson </ts>
               <ts e="T55" id="Seg_1718" n="e" s="T54">arɨjdagɨna, </ts>
               <ts e="T56" id="Seg_1720" n="e" s="T55">haːrkɨraːn </ts>
               <ts e="T57" id="Seg_1722" n="e" s="T56">dʼi͡eleːkteri </ts>
               <ts e="T58" id="Seg_1724" n="e" s="T57">olorpot </ts>
               <ts e="T59" id="Seg_1726" n="e" s="T58">da, </ts>
               <ts e="T60" id="Seg_1728" n="e" s="T59">utuppat </ts>
               <ts e="T61" id="Seg_1730" n="e" s="T60">da. </ts>
               <ts e="T62" id="Seg_1732" n="e" s="T61">Arajon </ts>
               <ts e="T63" id="Seg_1734" n="e" s="T62">kiːn </ts>
               <ts e="T64" id="Seg_1736" n="e" s="T63">ologugar </ts>
               <ts e="T65" id="Seg_1738" n="e" s="T64">itinnik </ts>
               <ts e="T66" id="Seg_1740" n="e" s="T65">olus </ts>
               <ts e="T67" id="Seg_1742" n="e" s="T66">erge </ts>
               <ts e="T68" id="Seg_1744" n="e" s="T67">dʼi͡e </ts>
               <ts e="T69" id="Seg_1746" n="e" s="T68">hu͡ok. </ts>
               <ts e="T70" id="Seg_1748" n="e" s="T69">Dʼi͡eleːkter </ts>
               <ts e="T71" id="Seg_1750" n="e" s="T70">hotoru </ts>
               <ts e="T72" id="Seg_1752" n="e" s="T71">haŋa </ts>
               <ts e="T73" id="Seg_1754" n="e" s="T72">dʼi͡ege </ts>
               <ts e="T74" id="Seg_1756" n="e" s="T73">(tahɨnan </ts>
               <ts e="T75" id="Seg_1758" n="e" s="T74">tus-) </ts>
               <ts e="T76" id="Seg_1760" n="e" s="T75">tahɨnɨ͡ak </ts>
               <ts e="T77" id="Seg_1762" n="e" s="T76">tustaːktar. </ts>
               <ts e="T78" id="Seg_1764" n="e" s="T77">Ulakan </ts>
               <ts e="T79" id="Seg_1766" n="e" s="T78">haŋa </ts>
               <ts e="T80" id="Seg_1768" n="e" s="T79">dʼi͡e </ts>
               <ts e="T81" id="Seg_1770" n="e" s="T80">tutullan </ts>
               <ts e="T82" id="Seg_1772" n="e" s="T81">büterin </ts>
               <ts e="T83" id="Seg_1774" n="e" s="T82">küːte </ts>
               <ts e="T84" id="Seg_1776" n="e" s="T83">olorollor. </ts>
               <ts e="T85" id="Seg_1778" n="e" s="T84">Anɨ </ts>
               <ts e="T86" id="Seg_1780" n="e" s="T85">ogolor </ts>
               <ts e="T87" id="Seg_1782" n="e" s="T86">kergettere </ts>
               <ts e="T88" id="Seg_1784" n="e" s="T87">čugas </ts>
               <ts e="T89" id="Seg_1786" n="e" s="T88">uruːlarɨgar </ts>
               <ts e="T90" id="Seg_1788" n="e" s="T89">ɨ͡allana </ts>
               <ts e="T91" id="Seg_1790" n="e" s="T90">barbɨttara. </ts>
               <ts e="T92" id="Seg_1792" n="e" s="T91">Dʼi͡eleriger </ts>
               <ts e="T93" id="Seg_1794" n="e" s="T92">harsɨn </ts>
               <ts e="T94" id="Seg_1796" n="e" s="T93">duː, </ts>
               <ts e="T95" id="Seg_1798" n="e" s="T94">öjüːn </ts>
               <ts e="T96" id="Seg_1800" n="e" s="T95">duː </ts>
               <ts e="T97" id="Seg_1802" n="e" s="T96">keli͡ek </ts>
               <ts e="T98" id="Seg_1804" n="e" s="T97">tustaːk </ts>
               <ts e="T99" id="Seg_1806" n="e" s="T98">etilere. </ts>
               <ts e="T100" id="Seg_1808" n="e" s="T99">Kɨhɨn </ts>
               <ts e="T101" id="Seg_1810" n="e" s="T100">karaŋa </ts>
               <ts e="T102" id="Seg_1812" n="e" s="T101">kün </ts>
               <ts e="T103" id="Seg_1814" n="e" s="T102">kɨlgas, </ts>
               <ts e="T104" id="Seg_1816" n="e" s="T103">ki͡ehelere </ts>
               <ts e="T105" id="Seg_1818" n="e" s="T104">olus </ts>
               <ts e="T106" id="Seg_1820" n="e" s="T105">erde. </ts>
               <ts e="T107" id="Seg_1822" n="e" s="T106">U͡ol </ts>
               <ts e="T108" id="Seg_1824" n="e" s="T107">ogolor </ts>
               <ts e="T109" id="Seg_1826" n="e" s="T108">ahɨːr </ts>
               <ts e="T110" id="Seg_1828" n="e" s="T109">astarɨn </ts>
               <ts e="T111" id="Seg_1830" n="e" s="T110">totu͡oktarɨgar </ts>
               <ts e="T112" id="Seg_1832" n="e" s="T111">di͡eri </ts>
               <ts e="T113" id="Seg_1834" n="e" s="T112">ahaːn </ts>
               <ts e="T114" id="Seg_1836" n="e" s="T113">baraːn </ts>
               <ts e="T115" id="Seg_1838" n="e" s="T114">utujaːrɨ </ts>
               <ts e="T116" id="Seg_1840" n="e" s="T115">terimmittere. </ts>
               <ts e="T117" id="Seg_1842" n="e" s="T116">"Doː, </ts>
               <ts e="T118" id="Seg_1844" n="e" s="T117">bihigi </ts>
               <ts e="T119" id="Seg_1846" n="e" s="T118">erge </ts>
               <ts e="T120" id="Seg_1848" n="e" s="T119">dʼi͡ebit </ts>
               <ts e="T121" id="Seg_1850" n="e" s="T120">tüːn </ts>
               <ts e="T122" id="Seg_1852" n="e" s="T121">kuttuːra </ts>
               <ts e="T123" id="Seg_1854" n="e" s="T122">bu͡olu͡o", </ts>
               <ts e="T124" id="Seg_1856" n="e" s="T123">Mukulaːj </ts>
               <ts e="T125" id="Seg_1858" n="e" s="T124">ubajɨgar </ts>
               <ts e="T126" id="Seg_1860" n="e" s="T125">di͡ebite. </ts>
               <ts e="T127" id="Seg_1862" n="e" s="T126">"En </ts>
               <ts e="T128" id="Seg_1864" n="e" s="T127">emi͡e </ts>
               <ts e="T129" id="Seg_1866" n="e" s="T128">kuhagannɨk </ts>
               <ts e="T130" id="Seg_1868" n="e" s="T129">esten </ts>
               <ts e="T131" id="Seg_1870" n="e" s="T130">eregin", </ts>
               <ts e="T132" id="Seg_1872" n="e" s="T131">ginini </ts>
               <ts e="T133" id="Seg_1874" n="e" s="T132">ulakana </ts>
               <ts e="T134" id="Seg_1876" n="e" s="T133">bu͡ojbuta. </ts>
               <ts e="T135" id="Seg_1878" n="e" s="T134">"Kata </ts>
               <ts e="T136" id="Seg_1880" n="e" s="T135">hɨgɨnnʼaktan, </ts>
               <ts e="T137" id="Seg_1882" n="e" s="T136">onno </ts>
               <ts e="T138" id="Seg_1884" n="e" s="T137">oruŋŋa </ts>
               <ts e="T139" id="Seg_1886" n="e" s="T138">hɨt. </ts>
               <ts e="T140" id="Seg_1888" n="e" s="T139">Biːrge </ts>
               <ts e="T141" id="Seg_1890" n="e" s="T140">utuju͡okput." </ts>
               <ts e="T142" id="Seg_1892" n="e" s="T141">Ubaja </ts>
               <ts e="T143" id="Seg_1894" n="e" s="T142">ihiger </ts>
               <ts e="T144" id="Seg_1896" n="e" s="T143">hin </ts>
               <ts e="T145" id="Seg_1898" n="e" s="T144">daːgɨnɨ </ts>
               <ts e="T146" id="Seg_1900" n="e" s="T145">dʼiksiner </ts>
               <ts e="T147" id="Seg_1902" n="e" s="T146">bɨhɨlaːk. </ts>
               <ts e="T148" id="Seg_1904" n="e" s="T147">Ginner </ts>
               <ts e="T149" id="Seg_1906" n="e" s="T148">iččitek </ts>
               <ts e="T150" id="Seg_1908" n="e" s="T149">dʼi͡ege </ts>
               <ts e="T151" id="Seg_1910" n="e" s="T150">kergene </ts>
               <ts e="T152" id="Seg_1912" n="e" s="T151">hu͡ok </ts>
               <ts e="T153" id="Seg_1914" n="e" s="T152">haŋardɨː </ts>
               <ts e="T154" id="Seg_1916" n="e" s="T153">konollor. </ts>
               <ts e="T155" id="Seg_1918" n="e" s="T154">Ubaja </ts>
               <ts e="T156" id="Seg_1920" n="e" s="T155">laːmpatɨn </ts>
               <ts e="T157" id="Seg_1922" n="e" s="T156">ututaːt </ts>
               <ts e="T158" id="Seg_1924" n="e" s="T157">baltɨtɨn </ts>
               <ts e="T159" id="Seg_1926" n="e" s="T158">kohunan </ts>
               <ts e="T160" id="Seg_1928" n="e" s="T159">hu͡orgaːn </ts>
               <ts e="T161" id="Seg_1930" n="e" s="T160">ihiger </ts>
               <ts e="T162" id="Seg_1932" n="e" s="T161">kiːren </ts>
               <ts e="T163" id="Seg_1934" n="e" s="T162">hɨppɨta. </ts>
               <ts e="T164" id="Seg_1936" n="e" s="T163">Ogolor </ts>
               <ts e="T165" id="Seg_1938" n="e" s="T164">hu͡organnarɨn </ts>
               <ts e="T166" id="Seg_1940" n="e" s="T165">ihiger </ts>
               <ts e="T167" id="Seg_1942" n="e" s="T166">tɨːmmakka </ts>
               <ts e="T168" id="Seg_1944" n="e" s="T167">hɨppɨttara. </ts>
               <ts e="T169" id="Seg_1946" n="e" s="T168">Kuttana </ts>
               <ts e="T170" id="Seg_1948" n="e" s="T169">hanaːn </ts>
               <ts e="T171" id="Seg_1950" n="e" s="T170">ihilliːller. </ts>
               <ts e="T172" id="Seg_1952" n="e" s="T171">Hotorukaːn </ts>
               <ts e="T173" id="Seg_1954" n="e" s="T172">karaktarɨn </ts>
               <ts e="T174" id="Seg_1956" n="e" s="T173">himen, </ts>
               <ts e="T175" id="Seg_1958" n="e" s="T174">utujan </ts>
               <ts e="T176" id="Seg_1960" n="e" s="T175">pahɨgɨraːn </ts>
               <ts e="T177" id="Seg_1962" n="e" s="T176">kaːlbɨttara. </ts>
               <ts e="T178" id="Seg_1964" n="e" s="T177">Töhö </ts>
               <ts e="T179" id="Seg_1966" n="e" s="T178">kačča </ts>
               <ts e="T180" id="Seg_1968" n="e" s="T179">bu͡olbuta </ts>
               <ts e="T181" id="Seg_1970" n="e" s="T180">dʼürü. </ts>
               <ts e="T182" id="Seg_1972" n="e" s="T181">Mikaː </ts>
               <ts e="T183" id="Seg_1974" n="e" s="T182">baltɨta </ts>
               <ts e="T184" id="Seg_1976" n="e" s="T183">ojogohugar </ts>
               <ts e="T185" id="Seg_1978" n="e" s="T184">ütü͡ölüːrütten </ts>
               <ts e="T186" id="Seg_1980" n="e" s="T185">uhuktubuta. </ts>
               <ts e="T187" id="Seg_1982" n="e" s="T186">"Ubaː, </ts>
               <ts e="T188" id="Seg_1984" n="e" s="T187">ihilleː, </ts>
               <ts e="T189" id="Seg_1986" n="e" s="T188">kuttuːr </ts>
               <ts e="T190" id="Seg_1988" n="e" s="T189">bɨhɨlaːk", </ts>
               <ts e="T191" id="Seg_1990" n="e" s="T190">Mukulaːj </ts>
               <ts e="T192" id="Seg_1992" n="e" s="T191">kuttanan </ts>
               <ts e="T193" id="Seg_1994" n="e" s="T192">haŋata </ts>
               <ts e="T194" id="Seg_1996" n="e" s="T193">arɨččɨ </ts>
               <ts e="T195" id="Seg_1998" n="e" s="T194">taksɨbɨta. </ts>
               <ts e="T196" id="Seg_2000" n="e" s="T195">Mikaː </ts>
               <ts e="T197" id="Seg_2002" n="e" s="T196">hu͡organɨn </ts>
               <ts e="T198" id="Seg_2004" n="e" s="T197">kɨrɨːtɨn </ts>
               <ts e="T199" id="Seg_2006" n="e" s="T198">kötögön </ts>
               <ts e="T200" id="Seg_2008" n="e" s="T199">ihilleːbite. </ts>
               <ts e="T201" id="Seg_2010" n="e" s="T200">Kirdik, </ts>
               <ts e="T202" id="Seg_2012" n="e" s="T201">ahɨːr </ts>
               <ts e="T203" id="Seg_2014" n="e" s="T202">ostoːllorugar </ts>
               <ts e="T204" id="Seg_2016" n="e" s="T203">teri͡elkeler, </ts>
               <ts e="T205" id="Seg_2018" n="e" s="T204">lu͡oskalar </ts>
               <ts e="T206" id="Seg_2020" n="e" s="T205">lahɨrgɨːllar. </ts>
               <ts e="T207" id="Seg_2022" n="e" s="T206">Kabɨs-karaŋa, </ts>
               <ts e="T208" id="Seg_2024" n="e" s="T207">tu͡ok </ts>
               <ts e="T209" id="Seg_2026" n="e" s="T208">daː </ts>
               <ts e="T210" id="Seg_2028" n="e" s="T209">köstübet. </ts>
               <ts e="T211" id="Seg_2030" n="e" s="T210">Kaja, </ts>
               <ts e="T212" id="Seg_2032" n="e" s="T211">ɨraːk </ts>
               <ts e="T213" id="Seg_2034" n="e" s="T212">turar </ts>
               <ts e="T214" id="Seg_2036" n="e" s="T213">ostoːl </ts>
               <ts e="T215" id="Seg_2038" n="e" s="T214">ürdüger </ts>
               <ts e="T216" id="Seg_2040" n="e" s="T215">kumaːkɨlarɨ </ts>
               <ts e="T217" id="Seg_2042" n="e" s="T216">tu͡ok </ts>
               <ts e="T218" id="Seg_2044" n="e" s="T217">ere </ts>
               <ts e="T219" id="Seg_2046" n="e" s="T218">(kajɨtɨlɨːr) </ts>
               <ts e="T220" id="Seg_2048" n="e" s="T219">kajɨtallɨːr. </ts>
               <ts e="T221" id="Seg_2050" n="e" s="T220">Ogolor </ts>
               <ts e="T222" id="Seg_2052" n="e" s="T221">hu͡organ </ts>
               <ts e="T223" id="Seg_2054" n="e" s="T222">ihiger </ts>
               <ts e="T224" id="Seg_2056" n="e" s="T223">kuːstahan </ts>
               <ts e="T225" id="Seg_2058" n="e" s="T224">baraːn </ts>
               <ts e="T226" id="Seg_2060" n="e" s="T225">kuttanan </ts>
               <ts e="T227" id="Seg_2062" n="e" s="T226">tɨːmmakka </ts>
               <ts e="T228" id="Seg_2064" n="e" s="T227">hɨtallar. </ts>
               <ts e="T229" id="Seg_2066" n="e" s="T228">Tahaːra </ts>
               <ts e="T230" id="Seg_2068" n="e" s="T229">güːlege </ts>
               <ts e="T231" id="Seg_2070" n="e" s="T230">tu͡ok </ts>
               <ts e="T232" id="Seg_2072" n="e" s="T231">ere </ts>
               <ts e="T233" id="Seg_2074" n="e" s="T232">kaːmalɨːra </ts>
               <ts e="T234" id="Seg_2076" n="e" s="T233">ihillibite. </ts>
               <ts e="T235" id="Seg_2078" n="e" s="T234">Dʼi͡e </ts>
               <ts e="T236" id="Seg_2080" n="e" s="T235">isti͡enetin </ts>
               <ts e="T237" id="Seg_2082" n="e" s="T236">tɨŋnɨk </ts>
               <ts e="T238" id="Seg_2084" n="e" s="T237">oksu͡oluːr. </ts>
               <ts e="T239" id="Seg_2086" n="e" s="T238">Koloru͡oktartan </ts>
               <ts e="T240" id="Seg_2088" n="e" s="T239">holuːrdarɨ </ts>
               <ts e="T241" id="Seg_2090" n="e" s="T240">bütte </ts>
               <ts e="T242" id="Seg_2092" n="e" s="T241">ihitteri </ts>
               <ts e="T243" id="Seg_2094" n="e" s="T242">hirge </ts>
               <ts e="T244" id="Seg_2096" n="e" s="T243">tühertiːr. </ts>
               <ts e="T245" id="Seg_2098" n="e" s="T244">Onton </ts>
               <ts e="T246" id="Seg_2100" n="e" s="T245">oloru </ts>
               <ts e="T247" id="Seg_2102" n="e" s="T246">tebi͡eliːre </ts>
               <ts e="T248" id="Seg_2104" n="e" s="T247">ihiller. </ts>
               <ts e="T249" id="Seg_2106" n="e" s="T248">"Uču! </ts>
               <ts e="T250" id="Seg_2108" n="e" s="T249">Mömpökkö </ts>
               <ts e="T251" id="Seg_2110" n="e" s="T250">hɨt", </ts>
               <ts e="T252" id="Seg_2112" n="e" s="T251">Mikaː </ts>
               <ts e="T253" id="Seg_2114" n="e" s="T252">baltɨtɨn </ts>
               <ts e="T254" id="Seg_2116" n="e" s="T253">bu͡ojbuta. </ts>
               <ts e="T255" id="Seg_2118" n="e" s="T254">Dʼi͡e </ts>
               <ts e="T256" id="Seg_2120" n="e" s="T255">ihiger </ts>
               <ts e="T257" id="Seg_2122" n="e" s="T256">güːlege </ts>
               <ts e="T258" id="Seg_2124" n="e" s="T257">aːjdaːn </ts>
               <ts e="T259" id="Seg_2126" n="e" s="T258">kahan </ts>
               <ts e="T260" id="Seg_2128" n="e" s="T259">daː </ts>
               <ts e="T261" id="Seg_2130" n="e" s="T260">büppet. </ts>
               <ts e="T262" id="Seg_2132" n="e" s="T261">Harsi͡erdaga </ts>
               <ts e="T263" id="Seg_2134" n="e" s="T262">di͡eri </ts>
               <ts e="T264" id="Seg_2136" n="e" s="T263">össü͡ö </ts>
               <ts e="T265" id="Seg_2138" n="e" s="T264">ɨraːk. </ts>
               <ts e="T266" id="Seg_2140" n="e" s="T265">Ginneri </ts>
               <ts e="T267" id="Seg_2142" n="e" s="T266">kim </ts>
               <ts e="T268" id="Seg_2144" n="e" s="T267">kelen </ts>
               <ts e="T269" id="Seg_2146" n="e" s="T268">bɨːhɨ͡aj. </ts>
               <ts e="T270" id="Seg_2148" n="e" s="T269">Ogolor </ts>
               <ts e="T271" id="Seg_2150" n="e" s="T270">kaːttaran </ts>
               <ts e="T272" id="Seg_2152" n="e" s="T271">ɨtɨ͡aktarɨn </ts>
               <ts e="T273" id="Seg_2154" n="e" s="T272">daː, </ts>
               <ts e="T274" id="Seg_2156" n="e" s="T273">ü͡ögülü͡ökterin </ts>
               <ts e="T275" id="Seg_2158" n="e" s="T274">daː </ts>
               <ts e="T276" id="Seg_2160" n="e" s="T275">bert. </ts>
               <ts e="T277" id="Seg_2162" n="e" s="T276">Ol </ts>
               <ts e="T278" id="Seg_2164" n="e" s="T277">kuttana </ts>
               <ts e="T279" id="Seg_2166" n="e" s="T278">hɨtannar </ts>
               <ts e="T280" id="Seg_2168" n="e" s="T279">uːlarɨgar </ts>
               <ts e="T281" id="Seg_2170" n="e" s="T280">battatan </ts>
               <ts e="T282" id="Seg_2172" n="e" s="T281">utujan </ts>
               <ts e="T283" id="Seg_2174" n="e" s="T282">kaːlbɨttara. </ts>
               <ts e="T284" id="Seg_2176" n="e" s="T283">Tu͡oktaːgar </ts>
               <ts e="T285" id="Seg_2178" n="e" s="T284">da </ts>
               <ts e="T286" id="Seg_2180" n="e" s="T285">ulakan </ts>
               <ts e="T287" id="Seg_2182" n="e" s="T286">aːjdaːntan </ts>
               <ts e="T288" id="Seg_2184" n="e" s="T287">emiske </ts>
               <ts e="T289" id="Seg_2186" n="e" s="T288">uhuktubuttara. </ts>
               <ts e="T290" id="Seg_2188" n="e" s="T289">Güːlege </ts>
               <ts e="T291" id="Seg_2190" n="e" s="T290">tu͡ok </ts>
               <ts e="T292" id="Seg_2192" n="e" s="T291">ere </ts>
               <ts e="T293" id="Seg_2194" n="e" s="T292">kɨtaːnaktɨk </ts>
               <ts e="T294" id="Seg_2196" n="e" s="T293">taːstɨː </ts>
               <ts e="T295" id="Seg_2198" n="e" s="T294">huːlan </ts>
               <ts e="T296" id="Seg_2200" n="e" s="T295">tüspüte. </ts>
               <ts e="T297" id="Seg_2202" n="e" s="T296">Ü͡ögüː, </ts>
               <ts e="T298" id="Seg_2204" n="e" s="T297">kahɨː </ts>
               <ts e="T299" id="Seg_2206" n="e" s="T298">ihillibite. </ts>
               <ts e="T300" id="Seg_2208" n="e" s="T299">Aːn </ts>
               <ts e="T301" id="Seg_2210" n="e" s="T300">arɨllɨbɨta. </ts>
               <ts e="T302" id="Seg_2212" n="e" s="T301">Dʼi͡ege </ts>
               <ts e="T303" id="Seg_2214" n="e" s="T302">kim </ts>
               <ts e="T304" id="Seg_2216" n="e" s="T303">ere </ts>
               <ts e="T305" id="Seg_2218" n="e" s="T304">kiːren </ts>
               <ts e="T306" id="Seg_2220" n="e" s="T305">laːmpanɨ </ts>
               <ts e="T307" id="Seg_2222" n="e" s="T306">ubappɨta. </ts>
               <ts e="T308" id="Seg_2224" n="e" s="T307">"Turuŋ, </ts>
               <ts e="T309" id="Seg_2226" n="e" s="T308">ehigi </ts>
               <ts e="T310" id="Seg_2228" n="e" s="T309">güːlegit </ts>
               <ts e="T311" id="Seg_2230" n="e" s="T310">aːnɨn </ts>
               <ts e="T312" id="Seg_2232" n="e" s="T311">togo </ts>
               <ts e="T313" id="Seg_2234" n="e" s="T312">happatakkɨtɨj? </ts>
               <ts e="T314" id="Seg_2236" n="e" s="T313">Koru͡obalar </ts>
               <ts e="T315" id="Seg_2238" n="e" s="T314">kiːrenner </ts>
               <ts e="T316" id="Seg_2240" n="e" s="T315">hiri </ts>
               <ts e="T317" id="Seg_2242" n="e" s="T316">delbi </ts>
               <ts e="T318" id="Seg_2244" n="e" s="T317">tepsibitter, </ts>
               <ts e="T319" id="Seg_2246" n="e" s="T318">güːle </ts>
               <ts e="T320" id="Seg_2248" n="e" s="T319">ihin </ts>
               <ts e="T321" id="Seg_2250" n="e" s="T320">biːr </ts>
               <ts e="T322" id="Seg_2252" n="e" s="T321">oŋorbuttar, </ts>
               <ts e="T323" id="Seg_2254" n="e" s="T322">hiri </ts>
               <ts e="T324" id="Seg_2256" n="e" s="T323">toloru </ts>
               <ts e="T325" id="Seg_2258" n="e" s="T324">haːktaːbɨttar." </ts>
               <ts e="T326" id="Seg_2260" n="e" s="T325">Ogolor </ts>
               <ts e="T327" id="Seg_2262" n="e" s="T326">karaktarɨn </ts>
               <ts e="T328" id="Seg_2264" n="e" s="T327">arɨjannar </ts>
               <ts e="T329" id="Seg_2266" n="e" s="T328">kihi </ts>
               <ts e="T330" id="Seg_2268" n="e" s="T329">turarɨn </ts>
               <ts e="T331" id="Seg_2270" n="e" s="T330">körbüttere. </ts>
               <ts e="T332" id="Seg_2272" n="e" s="T331">Ontulara </ts>
               <ts e="T333" id="Seg_2274" n="e" s="T332">abalaha-abalaha </ts>
               <ts e="T334" id="Seg_2276" n="e" s="T333">taŋahɨn, </ts>
               <ts e="T335" id="Seg_2278" n="e" s="T334">galipi͡e </ts>
               <ts e="T336" id="Seg_2280" n="e" s="T335">hɨ͡alɨjatɨn </ts>
               <ts e="T337" id="Seg_2282" n="e" s="T336">haːkka </ts>
               <ts e="T338" id="Seg_2284" n="e" s="T337">bihillibitin </ts>
               <ts e="T339" id="Seg_2286" n="e" s="T338">iliːlerinen </ts>
               <ts e="T340" id="Seg_2288" n="e" s="T339">hoto </ts>
               <ts e="T341" id="Seg_2290" n="e" s="T340">turbuta. </ts>
               <ts e="T342" id="Seg_2292" n="e" s="T341">Opu͡o, </ts>
               <ts e="T343" id="Seg_2294" n="e" s="T342">kolxoz </ts>
               <ts e="T344" id="Seg_2296" n="e" s="T343">sopxu͡oha </ts>
               <ts e="T345" id="Seg_2298" n="e" s="T344">(ületiger) </ts>
               <ts e="T346" id="Seg_2300" n="e" s="T345">ületiger </ts>
               <ts e="T347" id="Seg_2302" n="e" s="T346">dʼi͡e </ts>
               <ts e="T348" id="Seg_2304" n="e" s="T347">attɨnan </ts>
               <ts e="T349" id="Seg_2306" n="e" s="T348">baran </ts>
               <ts e="T350" id="Seg_2308" n="e" s="T349">ihen, </ts>
               <ts e="T351" id="Seg_2310" n="e" s="T350">güːle </ts>
               <ts e="T352" id="Seg_2312" n="e" s="T351">koru͡obalar </ts>
               <ts e="T353" id="Seg_2314" n="e" s="T352">kiːrbitterin </ts>
               <ts e="T354" id="Seg_2316" n="e" s="T353">körbüt. </ts>
               <ts e="T355" id="Seg_2318" n="e" s="T354">Onton </ts>
               <ts e="T356" id="Seg_2320" n="e" s="T355">barɨlarɨn </ts>
               <ts e="T357" id="Seg_2322" n="e" s="T356">batan </ts>
               <ts e="T358" id="Seg_2324" n="e" s="T357">tahaːrbɨta. </ts>
               <ts e="T359" id="Seg_2326" n="e" s="T358">Ol </ts>
               <ts e="T360" id="Seg_2328" n="e" s="T359">kennitten </ts>
               <ts e="T361" id="Seg_2330" n="e" s="T360">kiːren </ts>
               <ts e="T362" id="Seg_2332" n="e" s="T361">ihen, </ts>
               <ts e="T363" id="Seg_2334" n="e" s="T362">kaltɨrɨjan </ts>
               <ts e="T364" id="Seg_2336" n="e" s="T363">tühen, </ts>
               <ts e="T365" id="Seg_2338" n="e" s="T364">koru͡obalar </ts>
               <ts e="T366" id="Seg_2340" n="e" s="T365">haːktarɨgar </ts>
               <ts e="T367" id="Seg_2342" n="e" s="T366">delbi </ts>
               <ts e="T368" id="Seg_2344" n="e" s="T367">bihillibite. </ts>
               <ts e="T369" id="Seg_2346" n="e" s="T368">Tura </ts>
               <ts e="T370" id="Seg_2348" n="e" s="T369">hataːn </ts>
               <ts e="T371" id="Seg_2350" n="e" s="T370">iliːlerin </ts>
               <ts e="T372" id="Seg_2352" n="e" s="T371">emi͡e </ts>
               <ts e="T373" id="Seg_2354" n="e" s="T372">olus </ts>
               <ts e="T374" id="Seg_2356" n="e" s="T373">bispite. </ts>
               <ts e="T375" id="Seg_2358" n="e" s="T374">Mikaːnɨ </ts>
               <ts e="T376" id="Seg_2360" n="e" s="T375">kɨtta </ts>
               <ts e="T377" id="Seg_2362" n="e" s="T376">Mukulaːj </ts>
               <ts e="T378" id="Seg_2364" n="e" s="T377">kɨjŋammɨt </ts>
               <ts e="T379" id="Seg_2366" n="e" s="T378">kihi </ts>
               <ts e="T380" id="Seg_2368" n="e" s="T379">kelbitiger </ts>
               <ts e="T381" id="Seg_2370" n="e" s="T380">ulakannɨk </ts>
               <ts e="T382" id="Seg_2372" n="e" s="T381">ü͡örbüttere. </ts>
               <ts e="T383" id="Seg_2374" n="e" s="T382">Mikaː </ts>
               <ts e="T384" id="Seg_2376" n="e" s="T383">bilbet </ts>
               <ts e="T385" id="Seg_2378" n="e" s="T384">kihititten </ts>
               <ts e="T386" id="Seg_2380" n="e" s="T385">atɨŋɨrgaːn </ts>
               <ts e="T387" id="Seg_2382" n="e" s="T386">karagɨn </ts>
               <ts e="T388" id="Seg_2384" n="e" s="T387">kistiː-kistiː </ts>
               <ts e="T389" id="Seg_2386" n="e" s="T388">di͡ebite: </ts>
               <ts e="T390" id="Seg_2388" n="e" s="T389">"Bihigini </ts>
               <ts e="T391" id="Seg_2390" n="e" s="T390">tüːn </ts>
               <ts e="T392" id="Seg_2392" n="e" s="T391">kuttaːbɨta, </ts>
               <ts e="T393" id="Seg_2394" n="e" s="T392">koton </ts>
               <ts e="T394" id="Seg_2396" n="e" s="T393">utujbatɨbɨt." </ts>
               <ts e="T395" id="Seg_2398" n="e" s="T394">Tüːnü </ts>
               <ts e="T396" id="Seg_2400" n="e" s="T395">huptu </ts>
               <ts e="T397" id="Seg_2402" n="e" s="T396">tu͡ok </ts>
               <ts e="T398" id="Seg_2404" n="e" s="T397">bu͡olbutun </ts>
               <ts e="T399" id="Seg_2406" n="e" s="T398">barɨtɨn </ts>
               <ts e="T400" id="Seg_2408" n="e" s="T399">kepseːbite. </ts>
               <ts e="T401" id="Seg_2410" n="e" s="T400">Opu͡o </ts>
               <ts e="T402" id="Seg_2412" n="e" s="T401">ogolor </ts>
               <ts e="T403" id="Seg_2414" n="e" s="T402">kuttammɨt </ts>
               <ts e="T404" id="Seg_2416" n="e" s="T403">dʼühünnerin </ts>
               <ts e="T405" id="Seg_2418" n="e" s="T404">körön </ts>
               <ts e="T406" id="Seg_2420" n="e" s="T405">ginneri </ts>
               <ts e="T407" id="Seg_2422" n="e" s="T406">ahɨmmɨta. </ts>
               <ts e="T408" id="Seg_2424" n="e" s="T407">Kɨjŋammɨta </ts>
               <ts e="T409" id="Seg_2426" n="e" s="T408">honno </ts>
               <ts e="T410" id="Seg_2428" n="e" s="T409">aːspɨta. </ts>
               <ts e="T411" id="Seg_2430" n="e" s="T410">Küle-küle </ts>
               <ts e="T412" id="Seg_2432" n="e" s="T411">hordoːktorugar </ts>
               <ts e="T413" id="Seg_2434" n="e" s="T412">haŋarbɨta: </ts>
               <ts e="T414" id="Seg_2436" n="e" s="T413">"Tüːn </ts>
               <ts e="T415" id="Seg_2438" n="e" s="T414">ehigini </ts>
               <ts e="T416" id="Seg_2440" n="e" s="T415">koru͡obalarɨ </ts>
               <ts e="T417" id="Seg_2442" n="e" s="T416">gɨtta </ts>
               <ts e="T418" id="Seg_2444" n="e" s="T417">kutujaktar </ts>
               <ts e="T419" id="Seg_2446" n="e" s="T418">kuttaːbɨttara. </ts>
               <ts e="T420" id="Seg_2448" n="e" s="T419">Haːppakkɨt </ts>
               <ts e="T421" id="Seg_2450" n="e" s="T420">du͡o? </ts>
               <ts e="T422" id="Seg_2452" n="e" s="T421">Tuhata </ts>
               <ts e="T423" id="Seg_2454" n="e" s="T422">hu͡oktan </ts>
               <ts e="T424" id="Seg_2456" n="e" s="T423">kuttammɨkkɨtɨn </ts>
               <ts e="T425" id="Seg_2458" n="e" s="T424">bileŋŋit? </ts>
               <ts e="T426" id="Seg_2460" n="e" s="T425">Ehigi </ts>
               <ts e="T427" id="Seg_2462" n="e" s="T426">kuhagaŋŋɨtɨttan </ts>
               <ts e="T428" id="Seg_2464" n="e" s="T427">(min </ts>
               <ts e="T430" id="Seg_2466" n="e" s="T428">ɨs -) </ts>
               <ts e="T431" id="Seg_2468" n="e" s="T430">min </ts>
               <ts e="T432" id="Seg_2470" n="e" s="T431">olus </ts>
               <ts e="T433" id="Seg_2472" n="e" s="T432">bihilinnim. </ts>
               <ts e="T434" id="Seg_2474" n="e" s="T433">Inʼegit, </ts>
               <ts e="T435" id="Seg_2476" n="e" s="T434">agagɨt </ts>
               <ts e="T436" id="Seg_2478" n="e" s="T435">keli͡ekteriger </ts>
               <ts e="T437" id="Seg_2480" n="e" s="T436">di͡eri </ts>
               <ts e="T438" id="Seg_2482" n="e" s="T437">güːlegitin </ts>
               <ts e="T439" id="Seg_2484" n="e" s="T438">gɨtta </ts>
               <ts e="T440" id="Seg_2486" n="e" s="T439">dʼi͡egit </ts>
               <ts e="T441" id="Seg_2488" n="e" s="T440">ihin </ts>
               <ts e="T442" id="Seg_2490" n="e" s="T441">üčügejdik </ts>
               <ts e="T443" id="Seg_2492" n="e" s="T442">karajɨŋ." </ts>
               <ts e="T444" id="Seg_2494" n="e" s="T443">Opu͡o </ts>
               <ts e="T445" id="Seg_2496" n="e" s="T444">taksaːrɨ </ts>
               <ts e="T446" id="Seg_2498" n="e" s="T445">turan </ts>
               <ts e="T447" id="Seg_2500" n="e" s="T446">ogoloru </ts>
               <ts e="T448" id="Seg_2502" n="e" s="T447">mohujbuta. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T1" id="Seg_2503" s="T0">PoNA_2004_MikaMukulajAloneAtHome_nar.001 (001.001)</ta>
            <ta e="T12" id="Seg_2504" s="T1">PoNA_2004_MikaMukulajAloneAtHome_nar.002 (001.002)</ta>
            <ta e="T19" id="Seg_2505" s="T12">PoNA_2004_MikaMukulajAloneAtHome_nar.003 (001.003)</ta>
            <ta e="T30" id="Seg_2506" s="T19">PoNA_2004_MikaMukulajAloneAtHome_nar.004 (001.004)</ta>
            <ta e="T37" id="Seg_2507" s="T30">PoNA_2004_MikaMukulajAloneAtHome_nar.005 (001.005)</ta>
            <ta e="T50" id="Seg_2508" s="T37">PoNA_2004_MikaMukulajAloneAtHome_nar.006 (001.006)</ta>
            <ta e="T61" id="Seg_2509" s="T50">PoNA_2004_MikaMukulajAloneAtHome_nar.007 (001.007)</ta>
            <ta e="T69" id="Seg_2510" s="T61">PoNA_2004_MikaMukulajAloneAtHome_nar.008 (001.008)</ta>
            <ta e="T77" id="Seg_2511" s="T69">PoNA_2004_MikaMukulajAloneAtHome_nar.009 (001.009)</ta>
            <ta e="T84" id="Seg_2512" s="T77">PoNA_2004_MikaMukulajAloneAtHome_nar.010 (001.010)</ta>
            <ta e="T91" id="Seg_2513" s="T84">PoNA_2004_MikaMukulajAloneAtHome_nar.011 (001.011)</ta>
            <ta e="T99" id="Seg_2514" s="T91">PoNA_2004_MikaMukulajAloneAtHome_nar.012 (001.012)</ta>
            <ta e="T106" id="Seg_2515" s="T99">PoNA_2004_MikaMukulajAloneAtHome_nar.013 (001.013)</ta>
            <ta e="T116" id="Seg_2516" s="T106">PoNA_2004_MikaMukulajAloneAtHome_nar.014 (001.014)</ta>
            <ta e="T126" id="Seg_2517" s="T116">PoNA_2004_MikaMukulajAloneAtHome_nar.015 (001.015)</ta>
            <ta e="T134" id="Seg_2518" s="T126">PoNA_2004_MikaMukulajAloneAtHome_nar.016 (001.016)</ta>
            <ta e="T139" id="Seg_2519" s="T134">PoNA_2004_MikaMukulajAloneAtHome_nar.017 (001.017)</ta>
            <ta e="T141" id="Seg_2520" s="T139">PoNA_2004_MikaMukulajAloneAtHome_nar.018 (001.018)</ta>
            <ta e="T147" id="Seg_2521" s="T141">PoNA_2004_MikaMukulajAloneAtHome_nar.019 (001.019)</ta>
            <ta e="T154" id="Seg_2522" s="T147">PoNA_2004_MikaMukulajAloneAtHome_nar.020 (001.020)</ta>
            <ta e="T163" id="Seg_2523" s="T154">PoNA_2004_MikaMukulajAloneAtHome_nar.021 (001.021)</ta>
            <ta e="T168" id="Seg_2524" s="T163">PoNA_2004_MikaMukulajAloneAtHome_nar.022 (001.022)</ta>
            <ta e="T171" id="Seg_2525" s="T168">PoNA_2004_MikaMukulajAloneAtHome_nar.023 (001.023)</ta>
            <ta e="T177" id="Seg_2526" s="T171">PoNA_2004_MikaMukulajAloneAtHome_nar.024 (001.024)</ta>
            <ta e="T181" id="Seg_2527" s="T177">PoNA_2004_MikaMukulajAloneAtHome_nar.025 (001.025)</ta>
            <ta e="T186" id="Seg_2528" s="T181">PoNA_2004_MikaMukulajAloneAtHome_nar.026 (001.026)</ta>
            <ta e="T195" id="Seg_2529" s="T186">PoNA_2004_MikaMukulajAloneAtHome_nar.027 (001.027)</ta>
            <ta e="T200" id="Seg_2530" s="T195">PoNA_2004_MikaMukulajAloneAtHome_nar.028 (001.028)</ta>
            <ta e="T206" id="Seg_2531" s="T200">PoNA_2004_MikaMukulajAloneAtHome_nar.029 (001.029)</ta>
            <ta e="T210" id="Seg_2532" s="T206">PoNA_2004_MikaMukulajAloneAtHome_nar.030 (001.030)</ta>
            <ta e="T220" id="Seg_2533" s="T210">PoNA_2004_MikaMukulajAloneAtHome_nar.031 (001.031)</ta>
            <ta e="T228" id="Seg_2534" s="T220">PoNA_2004_MikaMukulajAloneAtHome_nar.032 (001.032)</ta>
            <ta e="T234" id="Seg_2535" s="T228">PoNA_2004_MikaMukulajAloneAtHome_nar.033 (001.033)</ta>
            <ta e="T238" id="Seg_2536" s="T234">PoNA_2004_MikaMukulajAloneAtHome_nar.034 (001.034)</ta>
            <ta e="T244" id="Seg_2537" s="T238">PoNA_2004_MikaMukulajAloneAtHome_nar.035 (001.035)</ta>
            <ta e="T248" id="Seg_2538" s="T244">PoNA_2004_MikaMukulajAloneAtHome_nar.036 (001.036)</ta>
            <ta e="T249" id="Seg_2539" s="T248">PoNA_2004_MikaMukulajAloneAtHome_nar.037 (001.037)</ta>
            <ta e="T254" id="Seg_2540" s="T249">PoNA_2004_MikaMukulajAloneAtHome_nar.038 (001.038)</ta>
            <ta e="T261" id="Seg_2541" s="T254">PoNA_2004_MikaMukulajAloneAtHome_nar.039 (001.039)</ta>
            <ta e="T265" id="Seg_2542" s="T261">PoNA_2004_MikaMukulajAloneAtHome_nar.040 (001.040)</ta>
            <ta e="T269" id="Seg_2543" s="T265">PoNA_2004_MikaMukulajAloneAtHome_nar.041 (001.041)</ta>
            <ta e="T276" id="Seg_2544" s="T269">PoNA_2004_MikaMukulajAloneAtHome_nar.042 (001.042)</ta>
            <ta e="T283" id="Seg_2545" s="T276">PoNA_2004_MikaMukulajAloneAtHome_nar.043 (001.043)</ta>
            <ta e="T289" id="Seg_2546" s="T283">PoNA_2004_MikaMukulajAloneAtHome_nar.044 (001.044)</ta>
            <ta e="T296" id="Seg_2547" s="T289">PoNA_2004_MikaMukulajAloneAtHome_nar.045 (001.045)</ta>
            <ta e="T299" id="Seg_2548" s="T296">PoNA_2004_MikaMukulajAloneAtHome_nar.046 (001.046)</ta>
            <ta e="T301" id="Seg_2549" s="T299">PoNA_2004_MikaMukulajAloneAtHome_nar.047 (001.047)</ta>
            <ta e="T307" id="Seg_2550" s="T301">PoNA_2004_MikaMukulajAloneAtHome_nar.048 (001.048)</ta>
            <ta e="T313" id="Seg_2551" s="T307">PoNA_2004_MikaMukulajAloneAtHome_nar.049 (001.049)</ta>
            <ta e="T325" id="Seg_2552" s="T313">PoNA_2004_MikaMukulajAloneAtHome_nar.050 (001.050)</ta>
            <ta e="T331" id="Seg_2553" s="T325">PoNA_2004_MikaMukulajAloneAtHome_nar.051 (001.051)</ta>
            <ta e="T341" id="Seg_2554" s="T331">PoNA_2004_MikaMukulajAloneAtHome_nar.052 (001.052)</ta>
            <ta e="T354" id="Seg_2555" s="T341">PoNA_2004_MikaMukulajAloneAtHome_nar.053 (001.053)</ta>
            <ta e="T358" id="Seg_2556" s="T354">PoNA_2004_MikaMukulajAloneAtHome_nar.054 (001.054)</ta>
            <ta e="T368" id="Seg_2557" s="T358">PoNA_2004_MikaMukulajAloneAtHome_nar.055 (001.055)</ta>
            <ta e="T374" id="Seg_2558" s="T368">PoNA_2004_MikaMukulajAloneAtHome_nar.056 (001.056)</ta>
            <ta e="T382" id="Seg_2559" s="T374">PoNA_2004_MikaMukulajAloneAtHome_nar.057 (001.057)</ta>
            <ta e="T389" id="Seg_2560" s="T382">PoNA_2004_MikaMukulajAloneAtHome_nar.058 (001.058)</ta>
            <ta e="T394" id="Seg_2561" s="T389">PoNA_2004_MikaMukulajAloneAtHome_nar.059 (001.059)</ta>
            <ta e="T400" id="Seg_2562" s="T394">PoNA_2004_MikaMukulajAloneAtHome_nar.060 (001.060)</ta>
            <ta e="T407" id="Seg_2563" s="T400">PoNA_2004_MikaMukulajAloneAtHome_nar.061 (001.061)</ta>
            <ta e="T410" id="Seg_2564" s="T407">PoNA_2004_MikaMukulajAloneAtHome_nar.062 (001.062)</ta>
            <ta e="T413" id="Seg_2565" s="T410">PoNA_2004_MikaMukulajAloneAtHome_nar.063 (001.063)</ta>
            <ta e="T419" id="Seg_2566" s="T413">PoNA_2004_MikaMukulajAloneAtHome_nar.064 (001.064)</ta>
            <ta e="T421" id="Seg_2567" s="T419">PoNA_2004_MikaMukulajAloneAtHome_nar.065 (001.065)</ta>
            <ta e="T425" id="Seg_2568" s="T421">PoNA_2004_MikaMukulajAloneAtHome_nar.066 (001.066)</ta>
            <ta e="T433" id="Seg_2569" s="T425">PoNA_2004_MikaMukulajAloneAtHome_nar.067 (001.067)</ta>
            <ta e="T443" id="Seg_2570" s="T433">PoNA_2004_MikaMukulajAloneAtHome_nar.068 (001.068)</ta>
            <ta e="T448" id="Seg_2571" s="T443">PoNA_2004_MikaMukulajAloneAtHome_nar.069 (001.069)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T1" id="Seg_2572" s="T0">Куттаабыта.</ta>
            <ta e="T12" id="Seg_2573" s="T1">Икки уол оголор Микааны кытта Мукулаай кэргэттэрэ барбыттарын кэннэ дьиэлэригэр коммуттара. </ta>
            <ta e="T19" id="Seg_2574" s="T12">Гинилэр үөрэнэр кэмнэригэр кыһыны һупту интернаатка буолааччылар.</ta>
            <ta e="T30" id="Seg_2575" s="T19">Былыргы ньуучча дьиэ истиэнэтин мастара элэтэ буоланнар олус эмир.. эмэгэрбиттэр.</ta>
            <ta e="T37" id="Seg_2576" s="T30">Таһаараагы гүүлэтин аана киһи арыйдагына һааркырыыра бэрт.</ta>
            <ta e="T50" id="Seg_2577" s="T37">Дьиэ иһигэр, төһө да аайдаан буоллун, ким эмэ арыйдагына, һааркырыыр тыаһа һонно иһиллэр.</ta>
            <ta e="T61" id="Seg_2578" s="T50">Һорок кэннэ тыал оксон арыйдагына, һааркыраан дьиэлээктэри олорпот да, утуппат да.</ta>
            <ta e="T69" id="Seg_2579" s="T61">Арайон киин ологугар итинник олус эргэ дьиэ һуок.</ta>
            <ta e="T77" id="Seg_2580" s="T69">Дьиэлээктэр һотору һаӈа дьиэгэ (таһынан тус..) таһыныак тустаактар.</ta>
            <ta e="T84" id="Seg_2581" s="T77">Улакан һаӈа дьиэ тутуллан бүтэрин күүтэ олороллор.</ta>
            <ta e="T91" id="Seg_2582" s="T84">Аны оголор кэргэттэрэ чугас урууларыгар ыаллана барбыттара.</ta>
            <ta e="T99" id="Seg_2583" s="T91">Дьиэлэригэр һарсын дуу, өйүүн дуу кэлиэк тустаак этилэрэ.</ta>
            <ta e="T106" id="Seg_2584" s="T99">Кыһын караӈа күн кылгас, киэһэлэрэ олус эрдэ.</ta>
            <ta e="T116" id="Seg_2585" s="T106">Уол оголор аһыыр астарын тотуоктарыгар диэри аһан бараан утуйаары тэриммиттэрэ.</ta>
            <ta e="T126" id="Seg_2586" s="T116">"До-о (Догоо), биһиги эргэ дьиэбит түүн куттуура буолуо," – Мукулаай убайыгар диэбитэ. </ta>
            <ta e="T134" id="Seg_2587" s="T126">"Эн эмиэ куһаганнык эстэн эрэгин, – гинини улакана буойбута.</ta>
            <ta e="T139" id="Seg_2588" s="T134">– Ката һыгынньактан, онно оруӈӈа һыт.</ta>
            <ta e="T141" id="Seg_2589" s="T139">Бииргэ утуйуокпут".</ta>
            <ta e="T147" id="Seg_2590" s="T141">Убайа иһигэр һин даагыны (дааганы) дьиксинэр быһылак.</ta>
            <ta e="T154" id="Seg_2591" s="T147">Гиннэр иччитэк дьиэгэ кэргэнэ һуок һаӈардыы коноллор.</ta>
            <ta e="T163" id="Seg_2592" s="T154">Убайа лаампатын утутаат балтытын коһунан һуоргаан иһигэр киирэн һыппыта.</ta>
            <ta e="T168" id="Seg_2593" s="T163">Оголор һуорганнарын иһигэр тыыммакка һыппыттара.</ta>
            <ta e="T171" id="Seg_2594" s="T168">Куттана һанаан иһиллииллэр.</ta>
            <ta e="T177" id="Seg_2595" s="T171">Һоторукаан карактарын һимэн, утуйан паһыгыраан каалбыттара.</ta>
            <ta e="T181" id="Seg_2596" s="T177">Төһө качча буолбута дьүрү.</ta>
            <ta e="T186" id="Seg_2597" s="T181">Микаа балтыта ойогоһугар үтүөлүүрүттэн уһуктубута.</ta>
            <ta e="T195" id="Seg_2598" s="T186">"Убаа, иһиллээ, куттуур быһылаак," – Мукулаай куттанан һаӈата арыччы таксыбыта.</ta>
            <ta e="T200" id="Seg_2599" s="T195">Микаа һуорганын кырыытын көтөгөн иһиллээбитэ.</ta>
            <ta e="T206" id="Seg_2600" s="T200">Кирдик, аһыр остооллоругар тэриэлкэлэр, луоскалар (луоскулар) лаһыргыыллар.</ta>
            <ta e="T210" id="Seg_2601" s="T206">Кабыс-караӈа, туок даа көстүбэт.</ta>
            <ta e="T220" id="Seg_2602" s="T210">Кайа, ыраак турар остоол үрдүгэр кумаакылары туок эрэ кайытылыыр (кайыталлыыр).</ta>
            <ta e="T228" id="Seg_2603" s="T220">Оголор һуорган иһигэр куустаһан бараан куттанан тыыммакка һытталлар.</ta>
            <ta e="T234" id="Seg_2604" s="T228">Таһаара гүүлэгэ туок эрэ камалыыра иһиллибитэ.</ta>
            <ta e="T238" id="Seg_2605" s="T234">Дьиэ истенэтин (истиэнэтин) түүннүк оксуолуур.</ta>
            <ta e="T244" id="Seg_2606" s="T238">Колоруоктартан (колоруктартан) һолурдары бүттэ иһиттэри һиргэ түһэртиир.</ta>
            <ta e="T248" id="Seg_2607" s="T244">Онтон олору тэбиэлиирэ иһиллэр.</ta>
            <ta e="T249" id="Seg_2608" s="T248">"Учу!</ta>
            <ta e="T254" id="Seg_2609" s="T249">Мөмпөккө һыт," – Микаа балтытын буойбута.</ta>
            <ta e="T261" id="Seg_2610" s="T254">Дьиэ иһигэр гүүлэгэ аайдаан каһан даа бүппэт.</ta>
            <ta e="T265" id="Seg_2611" s="T261">Һарсиэрдага диэри өссүө ыраак.</ta>
            <ta e="T269" id="Seg_2612" s="T265">Гинэри (Гиннэри, Гинилэри) ким кэлэн быһыай.</ta>
            <ta e="T276" id="Seg_2613" s="T269">Оголор кааттаран ытыактарын даа, үөгүлүөктэрин даа бэрт.</ta>
            <ta e="T283" id="Seg_2614" s="T276">Ол куттана һытаннар ууларыгар баттатан утуйан каалбыттара.</ta>
            <ta e="T289" id="Seg_2615" s="T283">Туоктаагар да улакан аайдаантан эмискэ уһуктубуттара.</ta>
            <ta e="T296" id="Seg_2616" s="T289">Гүүлэгэ туок эрэ кытаанактык таастыы һуулан түспүтэ.</ta>
            <ta e="T299" id="Seg_2617" s="T296">Үөгүү, каһыы иһиллибитэ.</ta>
            <ta e="T301" id="Seg_2618" s="T299">Аан арыллыбыта.</ta>
            <ta e="T307" id="Seg_2619" s="T301">Дьиэгэ ким эрэ киирэн лаампаны убаппыта.</ta>
            <ta e="T313" id="Seg_2620" s="T307">"Туруӈ, эһиги гүүлэгит аанын того һаппатаккытый!?"</ta>
            <ta e="T325" id="Seg_2621" s="T313">Коруобалар киирэннэр һири дэлби тэпсибиттэр, гүүлэ иһин биир оӈорбуттар, һири толору һаактаабыттар.</ta>
            <ta e="T331" id="Seg_2622" s="T325">Оголор карактарын арыйаннар киһи турарын көрбүттэрэ.</ta>
            <ta e="T341" id="Seg_2623" s="T331">Онтула.. (Онтулара) абалаһа-абалаһа таӈаһын, галипиэ һыалыйатын һаакка биһиллибитин илиилэринэн һото турбута.</ta>
            <ta e="T354" id="Seg_2624" s="T341">Опуо, колхоз сопкуоһа үлэтигэр дьиэ аттынан баран иһэн, гүүлэ (гүүлэгэ) коруобалар киирбиттэрин көрбүт.</ta>
            <ta e="T358" id="Seg_2625" s="T354">Онтон барыларын батан таһаарбыта.</ta>
            <ta e="T368" id="Seg_2626" s="T358">Ол кэнниттэн киирэн иһэн, калтырыйан түһэн, коруобалар һаактарыгар дэлби биһиллибитэ.</ta>
            <ta e="T374" id="Seg_2627" s="T368">Тура һатаан илиилэрин эмиэ олус биспитэ.</ta>
            <ta e="T382" id="Seg_2628" s="T374">Микааны кытта Мукулаай кыйӈаммыт киһи кэлбитигэр улаканнык үөрбүттэрэ.</ta>
            <ta e="T389" id="Seg_2629" s="T382">Микаа билбэт киһититтэн атыӈыргаан карагын кисти-кисти диэбитэ: </ta>
            <ta e="T394" id="Seg_2630" s="T389">"Биһигини түүн куттаабыта, котон утуйбатыбыт".</ta>
            <ta e="T400" id="Seg_2631" s="T394">Түүнү һупту туок буолбутун барытын кэпсээбитэ.</ta>
            <ta e="T407" id="Seg_2632" s="T400">Опуо оголор куттанмын дьүһүннэрин көрөн гиннэри аһыммыта.</ta>
            <ta e="T410" id="Seg_2633" s="T407">Кыйӈаммыта һонно ааспыта.</ta>
            <ta e="T413" id="Seg_2634" s="T410">Күлэ-күлэ һордоокторо һаӈарбыта: </ta>
            <ta e="T419" id="Seg_2635" s="T413">"Түүн эһигини коруобалары гытта кутуйактар куттаабыттара.</ta>
            <ta e="T421" id="Seg_2636" s="T419">Һаппаккыт дуо?</ta>
            <ta e="T425" id="Seg_2637" s="T421">Туһата һуоктан куттаммыккытын билэӈӈит?</ta>
            <ta e="T433" id="Seg_2638" s="T425">Эһиги куһагаӈӈытыттан мин олус биһилинним.</ta>
            <ta e="T443" id="Seg_2639" s="T433">Иньэгит, агагыт кэлиэктэригэр диэри гүүлэгитин гытта дьиэгит иһин үчүгэйдик карайыӈ".</ta>
            <ta e="T448" id="Seg_2640" s="T443">Опуо таксаары туран оголору муһуйбута.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T1" id="Seg_2641" s="T0">Kuttaːbɨta. </ta>
            <ta e="T12" id="Seg_2642" s="T1">Ikki u͡ol ogolor Mikaːnɨ kɨtta Mukulaːj kergettere barbɨttarɨn kenne dʼi͡eleriger kommuttara. </ta>
            <ta e="T19" id="Seg_2643" s="T12">Giniler ü͡örener kemneriger kɨhɨnɨ huptu intʼernaːkka bu͡olaːččɨlar. </ta>
            <ta e="T30" id="Seg_2644" s="T19">Bɨlɨrgɨ nʼuːčča dʼi͡e isti͡enetin mastara elete bu͡olannar olus (emir -) emegerbitter. </ta>
            <ta e="T37" id="Seg_2645" s="T30">Tahaːraːgɨ güːletin aːna kihi arɨjdagɨna haːrkɨrɨːra bert. </ta>
            <ta e="T50" id="Seg_2646" s="T37">Dʼi͡e ihiger, töhö da aːjdaːn bu͡ollun, kim eme arɨjdagɨna, haːrkɨrɨːr tɨ͡aha honno ihiller. </ta>
            <ta e="T61" id="Seg_2647" s="T50">Horok kenne tɨ͡al okson arɨjdagɨna, haːrkɨraːn dʼi͡eleːkteri olorpot da, utuppat da. </ta>
            <ta e="T69" id="Seg_2648" s="T61">Arajon kiːn ologugar itinnik olus erge dʼi͡e hu͡ok. </ta>
            <ta e="T77" id="Seg_2649" s="T69">Dʼi͡eleːkter hotoru haŋa dʼi͡ege (tahɨnan tus-) tahɨnɨ͡ak tustaːktar. </ta>
            <ta e="T84" id="Seg_2650" s="T77">Ulakan haŋa dʼi͡e tutullan büterin küːte olorollor. </ta>
            <ta e="T91" id="Seg_2651" s="T84">Anɨ ogolor kergettere čugas uruːlarɨgar ɨ͡allana barbɨttara. </ta>
            <ta e="T99" id="Seg_2652" s="T91">Dʼi͡eleriger harsɨn duː, öjüːn duː keli͡ek tustaːk etilere. </ta>
            <ta e="T106" id="Seg_2653" s="T99">Kɨhɨn karaŋa kün kɨlgas, ki͡ehelere olus erde. </ta>
            <ta e="T116" id="Seg_2654" s="T106">U͡ol ogolor ahɨːr astarɨn totu͡oktarɨgar di͡eri ahaːn baraːn utujaːrɨ terimmittere. </ta>
            <ta e="T126" id="Seg_2655" s="T116">"Doː, bihigi erge dʼi͡ebit tüːn kuttuːra bu͡olu͡o", Mukulaːj ubajɨgar di͡ebite. </ta>
            <ta e="T134" id="Seg_2656" s="T126">"En emi͡e kuhagannɨk esten eregin", ginini ulakana bu͡ojbuta. </ta>
            <ta e="T139" id="Seg_2657" s="T134">"Kata hɨgɨnnʼaktan, onno oruŋŋa hɨt. </ta>
            <ta e="T141" id="Seg_2658" s="T139">Biːrge utuju͡okput." </ta>
            <ta e="T147" id="Seg_2659" s="T141">Ubaja ihiger hin daːgɨnɨ dʼiksiner bɨhɨlaːk. </ta>
            <ta e="T154" id="Seg_2660" s="T147">Ginner iččitek dʼi͡ege kergene hu͡ok haŋardɨː konollor. </ta>
            <ta e="T163" id="Seg_2661" s="T154">Ubaja laːmpatɨn ututaːt baltɨtɨn kohunan hu͡orgaːn ihiger kiːren hɨppɨta. </ta>
            <ta e="T168" id="Seg_2662" s="T163">Ogolor hu͡organnarɨn ihiger tɨːmmakka hɨppɨttara. </ta>
            <ta e="T171" id="Seg_2663" s="T168">Kuttana hanaːn ihilliːller. </ta>
            <ta e="T177" id="Seg_2664" s="T171">Hotorukaːn karaktarɨn himen, utujan pahɨgɨraːn kaːlbɨttara. </ta>
            <ta e="T181" id="Seg_2665" s="T177">Töhö kačča bu͡olbuta dʼürü. </ta>
            <ta e="T186" id="Seg_2666" s="T181">Mikaː baltɨta ojogohugar ütü͡ölüːrütten uhuktubuta. </ta>
            <ta e="T195" id="Seg_2667" s="T186">"Ubaː, ihilleː, kuttuːr bɨhɨlaːk", Mukulaːj kuttanan haŋata arɨččɨ taksɨbɨta. </ta>
            <ta e="T200" id="Seg_2668" s="T195">Mikaː hu͡organɨn kɨrɨːtɨn kötögön ihilleːbite. </ta>
            <ta e="T206" id="Seg_2669" s="T200">Kirdik, ahɨːr ostoːllorugar teri͡elkeler, lu͡oskalar lahɨrgɨːllar. </ta>
            <ta e="T210" id="Seg_2670" s="T206">Kabɨs-karaŋa, tu͡ok daː köstübet. </ta>
            <ta e="T220" id="Seg_2671" s="T210">Kaja, ɨraːk turar ostoːl ürdüger kumaːkɨlarɨ tu͡ok ere (kajɨtɨlɨːr) kajɨtallɨːr. </ta>
            <ta e="T228" id="Seg_2672" s="T220">Ogolor hu͡organ ihiger kuːstahan baraːn kuttanan tɨːmmakka hɨtallar. </ta>
            <ta e="T234" id="Seg_2673" s="T228">Tahaːra güːlege tu͡ok ere kaːmalɨːra ihillibite. </ta>
            <ta e="T238" id="Seg_2674" s="T234">Dʼi͡e isti͡enetin tɨŋnɨk oksu͡oluːr. </ta>
            <ta e="T244" id="Seg_2675" s="T238">Koloru͡oktartan holuːrdarɨ bütte ihitteri hirge tühertiːr. </ta>
            <ta e="T248" id="Seg_2676" s="T244">Onton oloru tebi͡eliːre ihiller. </ta>
            <ta e="T249" id="Seg_2677" s="T248">"Uču! </ta>
            <ta e="T254" id="Seg_2678" s="T249">Mömpökkö hɨt", Mikaː baltɨtɨn bu͡ojbuta. </ta>
            <ta e="T261" id="Seg_2679" s="T254">Dʼi͡e ihiger güːlege aːjdaːn kahan daː büppet. </ta>
            <ta e="T265" id="Seg_2680" s="T261">Harsi͡erdaga di͡eri össü͡ö ɨraːk. </ta>
            <ta e="T269" id="Seg_2681" s="T265">Ginneri kim kelen bɨːhɨ͡aj. </ta>
            <ta e="T276" id="Seg_2682" s="T269">Ogolor kaːttaran ɨtɨ͡aktarɨn daː, ü͡ögülü͡ökterin daː bert. </ta>
            <ta e="T283" id="Seg_2683" s="T276">Ol kuttana hɨtannar uːlarɨgar battatan utujan kaːlbɨttara. </ta>
            <ta e="T289" id="Seg_2684" s="T283">Tu͡oktaːgar da ulakan aːjdaːntan emiske uhuktubuttara. </ta>
            <ta e="T296" id="Seg_2685" s="T289">Güːlege tu͡ok ere kɨtaːnaktɨk taːstɨː huːlan tüspüte. </ta>
            <ta e="T299" id="Seg_2686" s="T296">Ü͡ögüː, kahɨː ihillibite. </ta>
            <ta e="T301" id="Seg_2687" s="T299">Aːn arɨllɨbɨta. </ta>
            <ta e="T307" id="Seg_2688" s="T301">Dʼi͡ege kim ere kiːren laːmpanɨ ubappɨta. </ta>
            <ta e="T313" id="Seg_2689" s="T307">"Turuŋ, ehigi güːlegit aːnɨn togo happatakkɨtɨj? </ta>
            <ta e="T325" id="Seg_2690" s="T313">Koru͡obalar kiːrenner hiri delbi tepsibitter, güːle ihin biːr oŋorbuttar, hiri toloru haːktaːbɨttar." </ta>
            <ta e="T331" id="Seg_2691" s="T325">Ogolor karaktarɨn arɨjannar kihi turarɨn körbüttere. </ta>
            <ta e="T341" id="Seg_2692" s="T331">Ontulara abalaha-abalaha taŋahɨn, galipi͡e hɨ͡alɨjatɨn haːkka bihillibitin iliːlerinen hoto turbuta. </ta>
            <ta e="T354" id="Seg_2693" s="T341">Opu͡o, kolxoz sopxu͡oha (ületiger) ületiger dʼi͡e attɨnan baran ihen, güːle koru͡obalar kiːrbitterin körbüt. </ta>
            <ta e="T358" id="Seg_2694" s="T354">Onton barɨlarɨn batan tahaːrbɨta. </ta>
            <ta e="T368" id="Seg_2695" s="T358">Ol kennitten kiːren ihen, kaltɨrɨjan tühen, koru͡obalar haːktarɨgar delbi bihillibite. </ta>
            <ta e="T374" id="Seg_2696" s="T368">Tura hataːn iliːlerin emi͡e olus bispite. </ta>
            <ta e="T382" id="Seg_2697" s="T374">Mikaːnɨ kɨtta Mukulaːj kɨjŋammɨt kihi kelbitiger ulakannɨk ü͡örbüttere. </ta>
            <ta e="T389" id="Seg_2698" s="T382">Mikaː bilbet kihititten atɨŋɨrgaːn karagɨn kistiː-kistiː di͡ebite: </ta>
            <ta e="T394" id="Seg_2699" s="T389">"Bihigini tüːn kuttaːbɨta, koton utujbatɨbɨt." </ta>
            <ta e="T400" id="Seg_2700" s="T394">Tüːnü huptu tu͡ok bu͡olbutun barɨtɨn kepseːbite. </ta>
            <ta e="T407" id="Seg_2701" s="T400">Opu͡o ogolor kuttammɨt dʼühünnerin körön ginneri ahɨmmɨta. </ta>
            <ta e="T410" id="Seg_2702" s="T407">Kɨjŋammɨta honno aːspɨta. </ta>
            <ta e="T413" id="Seg_2703" s="T410">Küle-küle hordoːktorugar haŋarbɨta: </ta>
            <ta e="T419" id="Seg_2704" s="T413">"Tüːn ehigini koru͡obalarɨ gɨtta kutujaktar kuttaːbɨttara. </ta>
            <ta e="T421" id="Seg_2705" s="T419">Haːppakkɨt du͡o? </ta>
            <ta e="T425" id="Seg_2706" s="T421">Tuhata hu͡oktan kuttammɨkkɨtɨn bileŋŋit? </ta>
            <ta e="T433" id="Seg_2707" s="T425">Ehigi kuhagaŋŋɨtɨttan (min ɨs -) min olus bihilinnim. </ta>
            <ta e="T443" id="Seg_2708" s="T433">Inʼegit, agagɨt keli͡ekteriger di͡eri güːlegitin gɨtta dʼi͡egit ihin üčügejdik karajɨŋ." </ta>
            <ta e="T448" id="Seg_2709" s="T443">Opu͡o taksaːrɨ turan ogoloru mohujbuta. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_2710" s="T0">kuttaː-bɨt-a</ta>
            <ta e="T2" id="Seg_2711" s="T1">ikki</ta>
            <ta e="T3" id="Seg_2712" s="T2">u͡ol</ta>
            <ta e="T4" id="Seg_2713" s="T3">ogo-lor</ta>
            <ta e="T5" id="Seg_2714" s="T4">Mikaː-nɨ</ta>
            <ta e="T6" id="Seg_2715" s="T5">kɨtta</ta>
            <ta e="T7" id="Seg_2716" s="T6">Mukulaːj</ta>
            <ta e="T8" id="Seg_2717" s="T7">kerget-tere</ta>
            <ta e="T9" id="Seg_2718" s="T8">bar-bɨt-tarɨ-n</ta>
            <ta e="T10" id="Seg_2719" s="T9">kenne</ta>
            <ta e="T11" id="Seg_2720" s="T10">dʼi͡e-leri-ger</ta>
            <ta e="T12" id="Seg_2721" s="T11">kom-mut-tara</ta>
            <ta e="T13" id="Seg_2722" s="T12">giniler</ta>
            <ta e="T14" id="Seg_2723" s="T13">ü͡ören-er</ta>
            <ta e="T15" id="Seg_2724" s="T14">kem-neri-ger</ta>
            <ta e="T16" id="Seg_2725" s="T15">kɨhɨn-ɨ</ta>
            <ta e="T17" id="Seg_2726" s="T16">huptu</ta>
            <ta e="T18" id="Seg_2727" s="T17">intʼernaːk-ka</ta>
            <ta e="T19" id="Seg_2728" s="T18">bu͡ol-aːččɨ-lar</ta>
            <ta e="T20" id="Seg_2729" s="T19">bɨlɨr-gɨ</ta>
            <ta e="T21" id="Seg_2730" s="T20">nʼuːčča</ta>
            <ta e="T22" id="Seg_2731" s="T21">dʼi͡e</ta>
            <ta e="T23" id="Seg_2732" s="T22">isti͡ene-ti-n</ta>
            <ta e="T24" id="Seg_2733" s="T23">mas-tar-a</ta>
            <ta e="T25" id="Seg_2734" s="T24">ele-te</ta>
            <ta e="T26" id="Seg_2735" s="T25">bu͡ol-an-nar</ta>
            <ta e="T27" id="Seg_2736" s="T26">olus</ta>
            <ta e="T30" id="Seg_2737" s="T29">emeger-bit-ter</ta>
            <ta e="T31" id="Seg_2738" s="T30">tahaːraː-gɨ</ta>
            <ta e="T32" id="Seg_2739" s="T31">güːle-ti-n</ta>
            <ta e="T33" id="Seg_2740" s="T32">aːn-a</ta>
            <ta e="T34" id="Seg_2741" s="T33">kihi</ta>
            <ta e="T35" id="Seg_2742" s="T34">arɨj-dag-ɨna</ta>
            <ta e="T36" id="Seg_2743" s="T35">haːrkɨrɨː-r-a</ta>
            <ta e="T37" id="Seg_2744" s="T36">bert</ta>
            <ta e="T38" id="Seg_2745" s="T37">dʼi͡e</ta>
            <ta e="T39" id="Seg_2746" s="T38">ih-i-ger</ta>
            <ta e="T40" id="Seg_2747" s="T39">töhö</ta>
            <ta e="T41" id="Seg_2748" s="T40">da</ta>
            <ta e="T42" id="Seg_2749" s="T41">aːjdaːn</ta>
            <ta e="T43" id="Seg_2750" s="T42">bu͡ol-lun</ta>
            <ta e="T44" id="Seg_2751" s="T43">kim</ta>
            <ta e="T45" id="Seg_2752" s="T44">eme</ta>
            <ta e="T46" id="Seg_2753" s="T45">arɨj-dag-ɨna</ta>
            <ta e="T47" id="Seg_2754" s="T46">haːrkɨrɨː-r</ta>
            <ta e="T48" id="Seg_2755" s="T47">tɨ͡ah-a</ta>
            <ta e="T49" id="Seg_2756" s="T48">honno</ta>
            <ta e="T50" id="Seg_2757" s="T49">ihill-er</ta>
            <ta e="T51" id="Seg_2758" s="T50">horok</ta>
            <ta e="T52" id="Seg_2759" s="T51">kenne</ta>
            <ta e="T53" id="Seg_2760" s="T52">tɨ͡al</ta>
            <ta e="T54" id="Seg_2761" s="T53">oks-on</ta>
            <ta e="T55" id="Seg_2762" s="T54">arɨj-dag-ɨna</ta>
            <ta e="T56" id="Seg_2763" s="T55">haːrkɨraː-n</ta>
            <ta e="T57" id="Seg_2764" s="T56">dʼi͡e-leːk-ter-i</ta>
            <ta e="T58" id="Seg_2765" s="T57">olor-pot</ta>
            <ta e="T59" id="Seg_2766" s="T58">da</ta>
            <ta e="T60" id="Seg_2767" s="T59">utu-p-pat</ta>
            <ta e="T61" id="Seg_2768" s="T60">da</ta>
            <ta e="T62" id="Seg_2769" s="T61">arajon</ta>
            <ta e="T63" id="Seg_2770" s="T62">kiːn</ta>
            <ta e="T64" id="Seg_2771" s="T63">olog-u-gar</ta>
            <ta e="T65" id="Seg_2772" s="T64">itinnik</ta>
            <ta e="T66" id="Seg_2773" s="T65">olus</ta>
            <ta e="T67" id="Seg_2774" s="T66">erge</ta>
            <ta e="T68" id="Seg_2775" s="T67">dʼi͡e</ta>
            <ta e="T69" id="Seg_2776" s="T68">hu͡ok</ta>
            <ta e="T70" id="Seg_2777" s="T69">dʼi͡e-leːk-ter</ta>
            <ta e="T71" id="Seg_2778" s="T70">hotoru</ta>
            <ta e="T72" id="Seg_2779" s="T71">haŋa</ta>
            <ta e="T73" id="Seg_2780" s="T72">dʼi͡e-ge</ta>
            <ta e="T74" id="Seg_2781" s="T73">tah-ɨ-n-an</ta>
            <ta e="T75" id="Seg_2782" s="T74">tus</ta>
            <ta e="T76" id="Seg_2783" s="T75">tah-ɨ-n-ɨ͡ak</ta>
            <ta e="T77" id="Seg_2784" s="T76">tus-taːk-tar</ta>
            <ta e="T78" id="Seg_2785" s="T77">ulakan</ta>
            <ta e="T79" id="Seg_2786" s="T78">haŋa</ta>
            <ta e="T80" id="Seg_2787" s="T79">dʼi͡e</ta>
            <ta e="T81" id="Seg_2788" s="T80">tut-u-ll-an</ta>
            <ta e="T82" id="Seg_2789" s="T81">büt-er-i-n</ta>
            <ta e="T83" id="Seg_2790" s="T82">küːt-e</ta>
            <ta e="T84" id="Seg_2791" s="T83">olor-ol-lor</ta>
            <ta e="T85" id="Seg_2792" s="T84">anɨ</ta>
            <ta e="T86" id="Seg_2793" s="T85">ogo-lor</ta>
            <ta e="T87" id="Seg_2794" s="T86">kerget-tere</ta>
            <ta e="T88" id="Seg_2795" s="T87">čugas</ta>
            <ta e="T89" id="Seg_2796" s="T88">uruː-larɨ-gar</ta>
            <ta e="T90" id="Seg_2797" s="T89">ɨ͡allan-a</ta>
            <ta e="T91" id="Seg_2798" s="T90">bar-bɨt-tara</ta>
            <ta e="T92" id="Seg_2799" s="T91">dʼi͡e-leri-ger</ta>
            <ta e="T93" id="Seg_2800" s="T92">harsɨn</ta>
            <ta e="T94" id="Seg_2801" s="T93">duː</ta>
            <ta e="T95" id="Seg_2802" s="T94">öjüːn</ta>
            <ta e="T96" id="Seg_2803" s="T95">duː</ta>
            <ta e="T97" id="Seg_2804" s="T96">kel-i͡ek</ta>
            <ta e="T98" id="Seg_2805" s="T97">tus-taːk</ta>
            <ta e="T99" id="Seg_2806" s="T98">e-ti-lere</ta>
            <ta e="T100" id="Seg_2807" s="T99">kɨhɨn</ta>
            <ta e="T101" id="Seg_2808" s="T100">karaŋa</ta>
            <ta e="T102" id="Seg_2809" s="T101">kün</ta>
            <ta e="T103" id="Seg_2810" s="T102">kɨlgas</ta>
            <ta e="T104" id="Seg_2811" s="T103">ki͡ehe-ler-e</ta>
            <ta e="T105" id="Seg_2812" s="T104">olus</ta>
            <ta e="T106" id="Seg_2813" s="T105">erde</ta>
            <ta e="T107" id="Seg_2814" s="T106">u͡ol</ta>
            <ta e="T108" id="Seg_2815" s="T107">ogo-lor</ta>
            <ta e="T109" id="Seg_2816" s="T108">ahɨː-r</ta>
            <ta e="T110" id="Seg_2817" s="T109">as-tarɨ-n</ta>
            <ta e="T111" id="Seg_2818" s="T110">tot-u͡ok-tarɨ-gar</ta>
            <ta e="T112" id="Seg_2819" s="T111">di͡eri</ta>
            <ta e="T113" id="Seg_2820" s="T112">ahaː-n</ta>
            <ta e="T114" id="Seg_2821" s="T113">baraːn</ta>
            <ta e="T115" id="Seg_2822" s="T114">utuj-aːrɨ</ta>
            <ta e="T116" id="Seg_2823" s="T115">teri-m-mit-tere</ta>
            <ta e="T117" id="Seg_2824" s="T116">doː</ta>
            <ta e="T118" id="Seg_2825" s="T117">bihigi</ta>
            <ta e="T119" id="Seg_2826" s="T118">erge</ta>
            <ta e="T120" id="Seg_2827" s="T119">dʼi͡e-bit</ta>
            <ta e="T121" id="Seg_2828" s="T120">tüːn</ta>
            <ta e="T122" id="Seg_2829" s="T121">kuttuː-r-a</ta>
            <ta e="T123" id="Seg_2830" s="T122">bu͡olu͡o</ta>
            <ta e="T124" id="Seg_2831" s="T123">Mukulaːj</ta>
            <ta e="T125" id="Seg_2832" s="T124">ubaj-ɨ-gar</ta>
            <ta e="T126" id="Seg_2833" s="T125">di͡e-bit-e</ta>
            <ta e="T127" id="Seg_2834" s="T126">en</ta>
            <ta e="T128" id="Seg_2835" s="T127">emi͡e</ta>
            <ta e="T129" id="Seg_2836" s="T128">kuhagan-nɨk</ta>
            <ta e="T130" id="Seg_2837" s="T129">es-t-en</ta>
            <ta e="T131" id="Seg_2838" s="T130">er-e-gin</ta>
            <ta e="T132" id="Seg_2839" s="T131">gini-ni</ta>
            <ta e="T133" id="Seg_2840" s="T132">ulakan-a</ta>
            <ta e="T134" id="Seg_2841" s="T133">bu͡oj-but-a</ta>
            <ta e="T135" id="Seg_2842" s="T134">kata</ta>
            <ta e="T136" id="Seg_2843" s="T135">hɨgɨnnʼak-tan</ta>
            <ta e="T137" id="Seg_2844" s="T136">onno</ta>
            <ta e="T138" id="Seg_2845" s="T137">oruŋ-ŋa</ta>
            <ta e="T139" id="Seg_2846" s="T138">hɨt</ta>
            <ta e="T140" id="Seg_2847" s="T139">biːrge</ta>
            <ta e="T141" id="Seg_2848" s="T140">utuj-u͡ok-put</ta>
            <ta e="T142" id="Seg_2849" s="T141">ubaj-a</ta>
            <ta e="T143" id="Seg_2850" s="T142">ih-i-ger</ta>
            <ta e="T144" id="Seg_2851" s="T143">hin</ta>
            <ta e="T145" id="Seg_2852" s="T144">daːgɨnɨ</ta>
            <ta e="T146" id="Seg_2853" s="T145">dʼiks-i-n-er</ta>
            <ta e="T147" id="Seg_2854" s="T146">bɨhɨlaːk</ta>
            <ta e="T148" id="Seg_2855" s="T147">ginner</ta>
            <ta e="T149" id="Seg_2856" s="T148">iččitek</ta>
            <ta e="T150" id="Seg_2857" s="T149">dʼi͡e-ge</ta>
            <ta e="T151" id="Seg_2858" s="T150">kergen-e</ta>
            <ta e="T152" id="Seg_2859" s="T151">hu͡ok</ta>
            <ta e="T153" id="Seg_2860" s="T152">haŋardɨː</ta>
            <ta e="T154" id="Seg_2861" s="T153">kon-ol-lor</ta>
            <ta e="T155" id="Seg_2862" s="T154">ubaj-a</ta>
            <ta e="T156" id="Seg_2863" s="T155">laːmpa-tɨ-n</ta>
            <ta e="T157" id="Seg_2864" s="T156">utut-aːt</ta>
            <ta e="T158" id="Seg_2865" s="T157">balt-ɨ-tɨ-n</ta>
            <ta e="T159" id="Seg_2866" s="T158">koh-u-nan</ta>
            <ta e="T160" id="Seg_2867" s="T159">hu͡orgaːn</ta>
            <ta e="T161" id="Seg_2868" s="T160">ih-i-ger</ta>
            <ta e="T162" id="Seg_2869" s="T161">kiːr-en</ta>
            <ta e="T163" id="Seg_2870" s="T162">hɨp-pɨt-a</ta>
            <ta e="T164" id="Seg_2871" s="T163">ogo-lor</ta>
            <ta e="T165" id="Seg_2872" s="T164">hu͡organ-narɨ-n</ta>
            <ta e="T166" id="Seg_2873" s="T165">ih-i-ger</ta>
            <ta e="T167" id="Seg_2874" s="T166">tɨːm-makka</ta>
            <ta e="T168" id="Seg_2875" s="T167">hɨp-pɨt-tara</ta>
            <ta e="T169" id="Seg_2876" s="T168">kuttan-a</ta>
            <ta e="T170" id="Seg_2877" s="T169">hanaː-n</ta>
            <ta e="T171" id="Seg_2878" s="T170">ihilliː-l-ler</ta>
            <ta e="T172" id="Seg_2879" s="T171">hotoru-kaːn</ta>
            <ta e="T173" id="Seg_2880" s="T172">karak-tarɨ-n</ta>
            <ta e="T174" id="Seg_2881" s="T173">him-en</ta>
            <ta e="T175" id="Seg_2882" s="T174">utuj-an</ta>
            <ta e="T176" id="Seg_2883" s="T175">pahɨgɨraː-n</ta>
            <ta e="T177" id="Seg_2884" s="T176">kaːl-bɨt-tara</ta>
            <ta e="T178" id="Seg_2885" s="T177">töhö</ta>
            <ta e="T179" id="Seg_2886" s="T178">kačča</ta>
            <ta e="T180" id="Seg_2887" s="T179">bu͡ol-but-a</ta>
            <ta e="T181" id="Seg_2888" s="T180">dʼürü</ta>
            <ta e="T182" id="Seg_2889" s="T181">Mikaː</ta>
            <ta e="T183" id="Seg_2890" s="T182">balt-ɨ-ta</ta>
            <ta e="T184" id="Seg_2891" s="T183">ojogoh-u-gar</ta>
            <ta e="T185" id="Seg_2892" s="T184">üt-ü͡ölüː-r-ü-tten</ta>
            <ta e="T186" id="Seg_2893" s="T185">uhukt-u-but-a</ta>
            <ta e="T187" id="Seg_2894" s="T186">ubaː</ta>
            <ta e="T188" id="Seg_2895" s="T187">ihilleː</ta>
            <ta e="T189" id="Seg_2896" s="T188">kuttuː-r</ta>
            <ta e="T190" id="Seg_2897" s="T189">bɨhɨlaːk</ta>
            <ta e="T191" id="Seg_2898" s="T190">Mukulaːj</ta>
            <ta e="T192" id="Seg_2899" s="T191">kuttan-an</ta>
            <ta e="T193" id="Seg_2900" s="T192">haŋa-ta</ta>
            <ta e="T194" id="Seg_2901" s="T193">arɨččɨ</ta>
            <ta e="T195" id="Seg_2902" s="T194">taks-ɨ-bɨt-a</ta>
            <ta e="T196" id="Seg_2903" s="T195">Mikaː</ta>
            <ta e="T197" id="Seg_2904" s="T196">hu͡organ-ɨ-n</ta>
            <ta e="T198" id="Seg_2905" s="T197">kɨrɨː-tɨ-n</ta>
            <ta e="T199" id="Seg_2906" s="T198">kötög-ön</ta>
            <ta e="T200" id="Seg_2907" s="T199">ihilleː-bit-e</ta>
            <ta e="T201" id="Seg_2908" s="T200">kirdik</ta>
            <ta e="T202" id="Seg_2909" s="T201">ahɨː-r</ta>
            <ta e="T203" id="Seg_2910" s="T202">ostoːl-loru-gar</ta>
            <ta e="T204" id="Seg_2911" s="T203">teri͡elke-ler</ta>
            <ta e="T205" id="Seg_2912" s="T204">lu͡oska-lar</ta>
            <ta e="T206" id="Seg_2913" s="T205">lahɨrgɨː-l-lar</ta>
            <ta e="T207" id="Seg_2914" s="T206">kabɨs-karaŋa</ta>
            <ta e="T208" id="Seg_2915" s="T207">tu͡ok</ta>
            <ta e="T209" id="Seg_2916" s="T208">daː</ta>
            <ta e="T210" id="Seg_2917" s="T209">köst-ü-bet</ta>
            <ta e="T211" id="Seg_2918" s="T210">kaja</ta>
            <ta e="T212" id="Seg_2919" s="T211">ɨraːk</ta>
            <ta e="T213" id="Seg_2920" s="T212">tur-ar</ta>
            <ta e="T214" id="Seg_2921" s="T213">ostoːl</ta>
            <ta e="T215" id="Seg_2922" s="T214">ürd-ü-ger</ta>
            <ta e="T216" id="Seg_2923" s="T215">kumaːkɨ-lar-ɨ</ta>
            <ta e="T217" id="Seg_2924" s="T216">tu͡ok</ta>
            <ta e="T218" id="Seg_2925" s="T217">ere</ta>
            <ta e="T220" id="Seg_2926" s="T219">kajɨt-allɨː-r</ta>
            <ta e="T221" id="Seg_2927" s="T220">ogo-lor</ta>
            <ta e="T222" id="Seg_2928" s="T221">hu͡organ</ta>
            <ta e="T223" id="Seg_2929" s="T222">ih-i-ger</ta>
            <ta e="T224" id="Seg_2930" s="T223">kuːstah-an</ta>
            <ta e="T225" id="Seg_2931" s="T224">baraːn</ta>
            <ta e="T226" id="Seg_2932" s="T225">kuttan-an</ta>
            <ta e="T227" id="Seg_2933" s="T226">tɨːm-makka</ta>
            <ta e="T228" id="Seg_2934" s="T227">hɨt-al-lar</ta>
            <ta e="T229" id="Seg_2935" s="T228">tahaːra</ta>
            <ta e="T230" id="Seg_2936" s="T229">güːle-ge</ta>
            <ta e="T231" id="Seg_2937" s="T230">tu͡ok</ta>
            <ta e="T232" id="Seg_2938" s="T231">ere</ta>
            <ta e="T233" id="Seg_2939" s="T232">kaːm-alɨː-r-a</ta>
            <ta e="T234" id="Seg_2940" s="T233">ihill-i-bit-e</ta>
            <ta e="T235" id="Seg_2941" s="T234">dʼi͡e</ta>
            <ta e="T236" id="Seg_2942" s="T235">isti͡ene-ti-n</ta>
            <ta e="T237" id="Seg_2943" s="T236">tɨŋ-nɨk</ta>
            <ta e="T238" id="Seg_2944" s="T237">oks-u͡oluː-r</ta>
            <ta e="T239" id="Seg_2945" s="T238">koloru͡ok-tar-tan</ta>
            <ta e="T240" id="Seg_2946" s="T239">holuːr-dar-ɨ</ta>
            <ta e="T241" id="Seg_2947" s="T240">bütte</ta>
            <ta e="T242" id="Seg_2948" s="T241">ihit-ter-i</ta>
            <ta e="T243" id="Seg_2949" s="T242">hir-ge</ta>
            <ta e="T244" id="Seg_2950" s="T243">tüher-tiː-r</ta>
            <ta e="T245" id="Seg_2951" s="T244">onton</ta>
            <ta e="T246" id="Seg_2952" s="T245">o-lor-u</ta>
            <ta e="T247" id="Seg_2953" s="T246">teb-i͡eliː-r-e</ta>
            <ta e="T248" id="Seg_2954" s="T247">ihill-er</ta>
            <ta e="T249" id="Seg_2955" s="T248">uču</ta>
            <ta e="T250" id="Seg_2956" s="T249">möm-pökkö</ta>
            <ta e="T251" id="Seg_2957" s="T250">hɨt</ta>
            <ta e="T252" id="Seg_2958" s="T251">Mikaː</ta>
            <ta e="T253" id="Seg_2959" s="T252">balt-ɨ-tɨ-n</ta>
            <ta e="T254" id="Seg_2960" s="T253">bu͡oj-but-a</ta>
            <ta e="T255" id="Seg_2961" s="T254">dʼi͡e</ta>
            <ta e="T256" id="Seg_2962" s="T255">ih-i-ger</ta>
            <ta e="T257" id="Seg_2963" s="T256">güːle-ge</ta>
            <ta e="T258" id="Seg_2964" s="T257">aːjdaːn</ta>
            <ta e="T259" id="Seg_2965" s="T258">kahan</ta>
            <ta e="T260" id="Seg_2966" s="T259">daː</ta>
            <ta e="T261" id="Seg_2967" s="T260">büp-pet</ta>
            <ta e="T262" id="Seg_2968" s="T261">harsi͡erda-ga</ta>
            <ta e="T263" id="Seg_2969" s="T262">di͡eri</ta>
            <ta e="T264" id="Seg_2970" s="T263">össü͡ö</ta>
            <ta e="T265" id="Seg_2971" s="T264">ɨraːk</ta>
            <ta e="T266" id="Seg_2972" s="T265">ginner-i</ta>
            <ta e="T267" id="Seg_2973" s="T266">kim</ta>
            <ta e="T268" id="Seg_2974" s="T267">kel-en</ta>
            <ta e="T269" id="Seg_2975" s="T268">bɨːh-ɨ͡a=j</ta>
            <ta e="T270" id="Seg_2976" s="T269">ogo-lor</ta>
            <ta e="T271" id="Seg_2977" s="T270">kaːttar-an</ta>
            <ta e="T272" id="Seg_2978" s="T271">ɨt-ɨ͡ak-tarɨ-n</ta>
            <ta e="T273" id="Seg_2979" s="T272">daː</ta>
            <ta e="T274" id="Seg_2980" s="T273">ü͡ögül-ü͡ök-teri-n</ta>
            <ta e="T275" id="Seg_2981" s="T274">daː</ta>
            <ta e="T276" id="Seg_2982" s="T275">bert</ta>
            <ta e="T277" id="Seg_2983" s="T276">ol</ta>
            <ta e="T278" id="Seg_2984" s="T277">kuttan-a</ta>
            <ta e="T279" id="Seg_2985" s="T278">hɨt-an-nar</ta>
            <ta e="T280" id="Seg_2986" s="T279">uː-larɨ-gar</ta>
            <ta e="T281" id="Seg_2987" s="T280">batt-a-t-an</ta>
            <ta e="T282" id="Seg_2988" s="T281">utuj-an</ta>
            <ta e="T283" id="Seg_2989" s="T282">kaːl-bɨt-tara</ta>
            <ta e="T284" id="Seg_2990" s="T283">tu͡ok-taːgar</ta>
            <ta e="T285" id="Seg_2991" s="T284">da</ta>
            <ta e="T286" id="Seg_2992" s="T285">ulakan</ta>
            <ta e="T287" id="Seg_2993" s="T286">aːjdaːn-tan</ta>
            <ta e="T288" id="Seg_2994" s="T287">emiske</ta>
            <ta e="T289" id="Seg_2995" s="T288">uhukt-u-but-tara</ta>
            <ta e="T290" id="Seg_2996" s="T289">güːle-ge</ta>
            <ta e="T291" id="Seg_2997" s="T290">tu͡ok</ta>
            <ta e="T292" id="Seg_2998" s="T291">ere</ta>
            <ta e="T293" id="Seg_2999" s="T292">kɨtaːnak-tɨk</ta>
            <ta e="T294" id="Seg_3000" s="T293">taːs-tɨː</ta>
            <ta e="T295" id="Seg_3001" s="T294">huːl-an</ta>
            <ta e="T296" id="Seg_3002" s="T295">tüs-püt-e</ta>
            <ta e="T297" id="Seg_3003" s="T296">ü͡ögüː</ta>
            <ta e="T298" id="Seg_3004" s="T297">kahɨː</ta>
            <ta e="T299" id="Seg_3005" s="T298">ihill-i-bit-e</ta>
            <ta e="T300" id="Seg_3006" s="T299">aːn</ta>
            <ta e="T301" id="Seg_3007" s="T300">arɨ-ll-ɨ-bɨt-a</ta>
            <ta e="T302" id="Seg_3008" s="T301">dʼi͡e-ge</ta>
            <ta e="T303" id="Seg_3009" s="T302">kim</ta>
            <ta e="T304" id="Seg_3010" s="T303">ere</ta>
            <ta e="T305" id="Seg_3011" s="T304">kiːr-en</ta>
            <ta e="T306" id="Seg_3012" s="T305">laːmpa-nɨ</ta>
            <ta e="T307" id="Seg_3013" s="T306">ubap-pɨt-a</ta>
            <ta e="T308" id="Seg_3014" s="T307">tur-u-ŋ</ta>
            <ta e="T309" id="Seg_3015" s="T308">ehigi</ta>
            <ta e="T310" id="Seg_3016" s="T309">güːle-git</ta>
            <ta e="T311" id="Seg_3017" s="T310">aːn-ɨ-n</ta>
            <ta e="T312" id="Seg_3018" s="T311">togo</ta>
            <ta e="T313" id="Seg_3019" s="T312">hap-patak-kɨt=ɨj</ta>
            <ta e="T314" id="Seg_3020" s="T313">koru͡oba-lar</ta>
            <ta e="T315" id="Seg_3021" s="T314">kiːr-en-ner</ta>
            <ta e="T316" id="Seg_3022" s="T315">hir-i</ta>
            <ta e="T317" id="Seg_3023" s="T316">delbi</ta>
            <ta e="T318" id="Seg_3024" s="T317">tep-s-i-bit-ter</ta>
            <ta e="T319" id="Seg_3025" s="T318">güːle</ta>
            <ta e="T320" id="Seg_3026" s="T319">ih-i-n</ta>
            <ta e="T321" id="Seg_3027" s="T320">biːr</ta>
            <ta e="T322" id="Seg_3028" s="T321">oŋor-but-tar</ta>
            <ta e="T323" id="Seg_3029" s="T322">hir-i</ta>
            <ta e="T324" id="Seg_3030" s="T323">toloru</ta>
            <ta e="T325" id="Seg_3031" s="T324">haːktaː-bɨt-tar</ta>
            <ta e="T326" id="Seg_3032" s="T325">ogo-lor</ta>
            <ta e="T327" id="Seg_3033" s="T326">karak-tarɨ-n</ta>
            <ta e="T328" id="Seg_3034" s="T327">arɨj-an-nar</ta>
            <ta e="T329" id="Seg_3035" s="T328">kihi</ta>
            <ta e="T330" id="Seg_3036" s="T329">tur-ar-ɨ-n</ta>
            <ta e="T331" id="Seg_3037" s="T330">kör-büt-tere</ta>
            <ta e="T332" id="Seg_3038" s="T331">on-tu-lara</ta>
            <ta e="T333" id="Seg_3039" s="T332">abal-a-h-a-abal-a-h-a</ta>
            <ta e="T334" id="Seg_3040" s="T333">taŋah-ɨ-n</ta>
            <ta e="T335" id="Seg_3041" s="T334">galipi͡e</ta>
            <ta e="T336" id="Seg_3042" s="T335">hɨ͡alɨja-tɨ-n</ta>
            <ta e="T337" id="Seg_3043" s="T336">haːk-ka</ta>
            <ta e="T338" id="Seg_3044" s="T337">bih-i-ll-i-bit-i-n</ta>
            <ta e="T339" id="Seg_3045" s="T338">iliː-ler-i-nen</ta>
            <ta e="T340" id="Seg_3046" s="T339">hot-o</ta>
            <ta e="T341" id="Seg_3047" s="T340">tur-but-a</ta>
            <ta e="T342" id="Seg_3048" s="T341">Opu͡o</ta>
            <ta e="T343" id="Seg_3049" s="T342">kolxoz</ta>
            <ta e="T344" id="Seg_3050" s="T343">sopxu͡oh-a</ta>
            <ta e="T345" id="Seg_3051" s="T344">üle-ti-ger</ta>
            <ta e="T346" id="Seg_3052" s="T345">üle-ti-ger</ta>
            <ta e="T347" id="Seg_3053" s="T346">dʼi͡e</ta>
            <ta e="T348" id="Seg_3054" s="T347">attɨ-nan</ta>
            <ta e="T349" id="Seg_3055" s="T348">bar-an</ta>
            <ta e="T350" id="Seg_3056" s="T349">ih-en</ta>
            <ta e="T351" id="Seg_3057" s="T350">güːle</ta>
            <ta e="T352" id="Seg_3058" s="T351">koru͡oba-lar</ta>
            <ta e="T353" id="Seg_3059" s="T352">kiːr-bit-teri-n</ta>
            <ta e="T354" id="Seg_3060" s="T353">kör-büt</ta>
            <ta e="T355" id="Seg_3061" s="T354">onton</ta>
            <ta e="T356" id="Seg_3062" s="T355">barɨ-larɨ-n</ta>
            <ta e="T357" id="Seg_3063" s="T356">bat-an</ta>
            <ta e="T358" id="Seg_3064" s="T357">tahaːr-bɨt-a</ta>
            <ta e="T359" id="Seg_3065" s="T358">ol</ta>
            <ta e="T360" id="Seg_3066" s="T359">kenn-i-tten</ta>
            <ta e="T361" id="Seg_3067" s="T360">kiːr-en</ta>
            <ta e="T362" id="Seg_3068" s="T361">ih-en</ta>
            <ta e="T363" id="Seg_3069" s="T362">kaltɨrɨj-an</ta>
            <ta e="T364" id="Seg_3070" s="T363">tüh-en</ta>
            <ta e="T365" id="Seg_3071" s="T364">koru͡oba-lar</ta>
            <ta e="T366" id="Seg_3072" s="T365">haːk-tarɨ-gar</ta>
            <ta e="T367" id="Seg_3073" s="T366">delbi</ta>
            <ta e="T368" id="Seg_3074" s="T367">bih-i-ll-i-bit-e</ta>
            <ta e="T369" id="Seg_3075" s="T368">tur-a</ta>
            <ta e="T370" id="Seg_3076" s="T369">hataː-n</ta>
            <ta e="T371" id="Seg_3077" s="T370">iliː-ler-i-n</ta>
            <ta e="T372" id="Seg_3078" s="T371">emi͡e</ta>
            <ta e="T373" id="Seg_3079" s="T372">olus</ta>
            <ta e="T374" id="Seg_3080" s="T373">bis-pit-e</ta>
            <ta e="T375" id="Seg_3081" s="T374">Mikaː-nɨ</ta>
            <ta e="T376" id="Seg_3082" s="T375">kɨtta</ta>
            <ta e="T377" id="Seg_3083" s="T376">Mukulaːj</ta>
            <ta e="T378" id="Seg_3084" s="T377">kɨjŋam-mɨt</ta>
            <ta e="T379" id="Seg_3085" s="T378">kihi</ta>
            <ta e="T380" id="Seg_3086" s="T379">kel-bit-i-ger</ta>
            <ta e="T381" id="Seg_3087" s="T380">ulakan-nɨk</ta>
            <ta e="T382" id="Seg_3088" s="T381">ü͡ör-büt-tere</ta>
            <ta e="T383" id="Seg_3089" s="T382">Mikaː</ta>
            <ta e="T384" id="Seg_3090" s="T383">bil-bet</ta>
            <ta e="T385" id="Seg_3091" s="T384">kihi-ti-tten</ta>
            <ta e="T386" id="Seg_3092" s="T385">atɨŋɨrgaː-n</ta>
            <ta e="T387" id="Seg_3093" s="T386">karag-ɨ-n</ta>
            <ta e="T388" id="Seg_3094" s="T387">kist-iː-kist-iː</ta>
            <ta e="T389" id="Seg_3095" s="T388">di͡e-bit-e</ta>
            <ta e="T390" id="Seg_3096" s="T389">bihigi-ni</ta>
            <ta e="T391" id="Seg_3097" s="T390">tüːn</ta>
            <ta e="T392" id="Seg_3098" s="T391">kuttaː-bɨt-a</ta>
            <ta e="T393" id="Seg_3099" s="T392">kot-on</ta>
            <ta e="T394" id="Seg_3100" s="T393">utuj-ba-tɨ-bɨt</ta>
            <ta e="T395" id="Seg_3101" s="T394">tüːn-ü</ta>
            <ta e="T396" id="Seg_3102" s="T395">huptu</ta>
            <ta e="T397" id="Seg_3103" s="T396">tu͡ok</ta>
            <ta e="T398" id="Seg_3104" s="T397">bu͡ol-but-u-n</ta>
            <ta e="T399" id="Seg_3105" s="T398">barɨ-tɨ-n</ta>
            <ta e="T400" id="Seg_3106" s="T399">kepseː-bit-e</ta>
            <ta e="T401" id="Seg_3107" s="T400">Opu͡o</ta>
            <ta e="T402" id="Seg_3108" s="T401">ogo-lor</ta>
            <ta e="T403" id="Seg_3109" s="T402">kuttam-mɨt</ta>
            <ta e="T404" id="Seg_3110" s="T403">dʼühün-neri-n</ta>
            <ta e="T405" id="Seg_3111" s="T404">kör-ön</ta>
            <ta e="T406" id="Seg_3112" s="T405">ginner-i</ta>
            <ta e="T407" id="Seg_3113" s="T406">ahɨm-mɨt-a</ta>
            <ta e="T408" id="Seg_3114" s="T407">kɨjŋam-mɨt-a</ta>
            <ta e="T409" id="Seg_3115" s="T408">honno</ta>
            <ta e="T410" id="Seg_3116" s="T409">aːs-pɨt-a</ta>
            <ta e="T411" id="Seg_3117" s="T410">kül-e-kül-e</ta>
            <ta e="T412" id="Seg_3118" s="T411">hordoːk-tor-u-gar</ta>
            <ta e="T413" id="Seg_3119" s="T412">haŋar-bɨt-a</ta>
            <ta e="T414" id="Seg_3120" s="T413">tüːn</ta>
            <ta e="T415" id="Seg_3121" s="T414">ehigi-ni</ta>
            <ta e="T416" id="Seg_3122" s="T415">koru͡oba-lar-ɨ</ta>
            <ta e="T417" id="Seg_3123" s="T416">gɨtta</ta>
            <ta e="T418" id="Seg_3124" s="T417">kutujak-tar</ta>
            <ta e="T419" id="Seg_3125" s="T418">kuttaː-bɨt-tara</ta>
            <ta e="T420" id="Seg_3126" s="T419">haːp-pak-kɨt</ta>
            <ta e="T421" id="Seg_3127" s="T420">du͡o</ta>
            <ta e="T422" id="Seg_3128" s="T421">tuha-ta</ta>
            <ta e="T423" id="Seg_3129" s="T422">hu͡ok-tan</ta>
            <ta e="T424" id="Seg_3130" s="T423">kuttam-mɨk-kɨtɨ-n</ta>
            <ta e="T425" id="Seg_3131" s="T424">bil-eŋ-ŋit</ta>
            <ta e="T426" id="Seg_3132" s="T425">ehigi</ta>
            <ta e="T427" id="Seg_3133" s="T426">kuhagaŋ-ŋɨtɨ-ttan</ta>
            <ta e="T428" id="Seg_3134" s="T427">min</ta>
            <ta e="T431" id="Seg_3135" s="T430">min</ta>
            <ta e="T432" id="Seg_3136" s="T431">olus</ta>
            <ta e="T433" id="Seg_3137" s="T432">bih-i-lin-ni-m</ta>
            <ta e="T434" id="Seg_3138" s="T433">inʼe-git</ta>
            <ta e="T435" id="Seg_3139" s="T434">aga-gɨt</ta>
            <ta e="T436" id="Seg_3140" s="T435">kel-i͡ek-teri-ger</ta>
            <ta e="T437" id="Seg_3141" s="T436">di͡eri</ta>
            <ta e="T438" id="Seg_3142" s="T437">güːle-giti-n</ta>
            <ta e="T439" id="Seg_3143" s="T438">gɨtta</ta>
            <ta e="T440" id="Seg_3144" s="T439">dʼi͡e-git</ta>
            <ta e="T441" id="Seg_3145" s="T440">ihin</ta>
            <ta e="T442" id="Seg_3146" s="T441">üčügej-dik</ta>
            <ta e="T443" id="Seg_3147" s="T442">karaj-ɨ-ŋ</ta>
            <ta e="T444" id="Seg_3148" s="T443">Opu͡o</ta>
            <ta e="T445" id="Seg_3149" s="T444">taks-aːrɨ</ta>
            <ta e="T446" id="Seg_3150" s="T445">tur-an</ta>
            <ta e="T447" id="Seg_3151" s="T446">ogo-lor-u</ta>
            <ta e="T448" id="Seg_3152" s="T447">mohuj-but-a</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_3153" s="T0">kuttaː-BIT-tA</ta>
            <ta e="T2" id="Seg_3154" s="T1">ikki</ta>
            <ta e="T3" id="Seg_3155" s="T2">u͡ol</ta>
            <ta e="T4" id="Seg_3156" s="T3">ogo-LAr</ta>
            <ta e="T5" id="Seg_3157" s="T4">Mikaː-nI</ta>
            <ta e="T6" id="Seg_3158" s="T5">kɨtta</ta>
            <ta e="T7" id="Seg_3159" s="T6">Mukulaːj</ta>
            <ta e="T8" id="Seg_3160" s="T7">kergen-LArA</ta>
            <ta e="T9" id="Seg_3161" s="T8">bar-BIT-LArI-n</ta>
            <ta e="T10" id="Seg_3162" s="T9">genne</ta>
            <ta e="T11" id="Seg_3163" s="T10">dʼi͡e-LArI-GAr</ta>
            <ta e="T12" id="Seg_3164" s="T11">kon-BIT-LArA</ta>
            <ta e="T13" id="Seg_3165" s="T12">giniler</ta>
            <ta e="T14" id="Seg_3166" s="T13">ü͡ören-Ar</ta>
            <ta e="T15" id="Seg_3167" s="T14">kem-LArI-GAr</ta>
            <ta e="T16" id="Seg_3168" s="T15">kɨhɨn-nI</ta>
            <ta e="T17" id="Seg_3169" s="T16">huptu</ta>
            <ta e="T18" id="Seg_3170" s="T17">internat-GA</ta>
            <ta e="T19" id="Seg_3171" s="T18">bu͡ol-AːččI-LAr</ta>
            <ta e="T20" id="Seg_3172" s="T19">bɨlɨr-GI</ta>
            <ta e="T21" id="Seg_3173" s="T20">nuːčča</ta>
            <ta e="T22" id="Seg_3174" s="T21">dʼi͡e</ta>
            <ta e="T23" id="Seg_3175" s="T22">isti͡ene-tI-n</ta>
            <ta e="T24" id="Seg_3176" s="T23">mas-LAr-tA</ta>
            <ta e="T25" id="Seg_3177" s="T24">ele-tA</ta>
            <ta e="T26" id="Seg_3178" s="T25">bu͡ol-An-LAr</ta>
            <ta e="T27" id="Seg_3179" s="T26">olus</ta>
            <ta e="T30" id="Seg_3180" s="T29">emeger-BIT-LAr</ta>
            <ta e="T31" id="Seg_3181" s="T30">tahaːra-GI</ta>
            <ta e="T32" id="Seg_3182" s="T31">güːle-tI-n</ta>
            <ta e="T33" id="Seg_3183" s="T32">aːn-tA</ta>
            <ta e="T34" id="Seg_3184" s="T33">kihi</ta>
            <ta e="T35" id="Seg_3185" s="T34">arɨj-TAK-InA</ta>
            <ta e="T36" id="Seg_3186" s="T35">haːrkɨraː-Ar-tA</ta>
            <ta e="T37" id="Seg_3187" s="T36">bert</ta>
            <ta e="T38" id="Seg_3188" s="T37">dʼi͡e</ta>
            <ta e="T39" id="Seg_3189" s="T38">is-tI-GAr</ta>
            <ta e="T40" id="Seg_3190" s="T39">töhö</ta>
            <ta e="T41" id="Seg_3191" s="T40">da</ta>
            <ta e="T42" id="Seg_3192" s="T41">ajdaːn</ta>
            <ta e="T43" id="Seg_3193" s="T42">bu͡ol-TIn</ta>
            <ta e="T44" id="Seg_3194" s="T43">kim</ta>
            <ta e="T45" id="Seg_3195" s="T44">eme</ta>
            <ta e="T46" id="Seg_3196" s="T45">arɨj-TAK-InA</ta>
            <ta e="T47" id="Seg_3197" s="T46">haːrkɨraː-Ar</ta>
            <ta e="T48" id="Seg_3198" s="T47">tɨ͡as-tA</ta>
            <ta e="T49" id="Seg_3199" s="T48">honno</ta>
            <ta e="T50" id="Seg_3200" s="T49">ihilin-Ar</ta>
            <ta e="T51" id="Seg_3201" s="T50">horok</ta>
            <ta e="T52" id="Seg_3202" s="T51">genne</ta>
            <ta e="T53" id="Seg_3203" s="T52">tɨ͡al</ta>
            <ta e="T54" id="Seg_3204" s="T53">ogus-An</ta>
            <ta e="T55" id="Seg_3205" s="T54">arɨj-TAK-InA</ta>
            <ta e="T56" id="Seg_3206" s="T55">haːrkɨraː-An</ta>
            <ta e="T57" id="Seg_3207" s="T56">dʼi͡e-LAːK-LAr-nI</ta>
            <ta e="T58" id="Seg_3208" s="T57">olor.[t]-BAT</ta>
            <ta e="T59" id="Seg_3209" s="T58">da</ta>
            <ta e="T60" id="Seg_3210" s="T59">utuj-t-BAT</ta>
            <ta e="T61" id="Seg_3211" s="T60">da</ta>
            <ta e="T62" id="Seg_3212" s="T61">rajon</ta>
            <ta e="T63" id="Seg_3213" s="T62">kiːn</ta>
            <ta e="T64" id="Seg_3214" s="T63">olok-tI-GAr</ta>
            <ta e="T65" id="Seg_3215" s="T64">itinnik</ta>
            <ta e="T66" id="Seg_3216" s="T65">olus</ta>
            <ta e="T67" id="Seg_3217" s="T66">erge</ta>
            <ta e="T68" id="Seg_3218" s="T67">dʼi͡e</ta>
            <ta e="T69" id="Seg_3219" s="T68">hu͡ok</ta>
            <ta e="T70" id="Seg_3220" s="T69">dʼi͡e-LAːK-LAr</ta>
            <ta e="T71" id="Seg_3221" s="T70">hotoru</ta>
            <ta e="T72" id="Seg_3222" s="T71">haŋa</ta>
            <ta e="T73" id="Seg_3223" s="T72">dʼi͡e-GA</ta>
            <ta e="T74" id="Seg_3224" s="T73">tas-I-n-An</ta>
            <ta e="T75" id="Seg_3225" s="T74">tüs</ta>
            <ta e="T76" id="Seg_3226" s="T75">tas-I-n-IAK</ta>
            <ta e="T77" id="Seg_3227" s="T76">tüs-LAːK-LAr</ta>
            <ta e="T78" id="Seg_3228" s="T77">ulakan</ta>
            <ta e="T79" id="Seg_3229" s="T78">haŋa</ta>
            <ta e="T80" id="Seg_3230" s="T79">dʼi͡e</ta>
            <ta e="T81" id="Seg_3231" s="T80">tut-I-LIN-An</ta>
            <ta e="T82" id="Seg_3232" s="T81">büt-Ar-tI-n</ta>
            <ta e="T83" id="Seg_3233" s="T82">köhüt-A</ta>
            <ta e="T84" id="Seg_3234" s="T83">olor-Ar-LAr</ta>
            <ta e="T85" id="Seg_3235" s="T84">anɨ</ta>
            <ta e="T86" id="Seg_3236" s="T85">ogo-LAr</ta>
            <ta e="T87" id="Seg_3237" s="T86">kergen-LArA</ta>
            <ta e="T88" id="Seg_3238" s="T87">hugas</ta>
            <ta e="T89" id="Seg_3239" s="T88">uruː-LArI-GAr</ta>
            <ta e="T90" id="Seg_3240" s="T89">ɨ͡allan-A</ta>
            <ta e="T91" id="Seg_3241" s="T90">bar-BIT-LArA</ta>
            <ta e="T92" id="Seg_3242" s="T91">dʼi͡e-LArI-GAr</ta>
            <ta e="T93" id="Seg_3243" s="T92">harsɨn</ta>
            <ta e="T94" id="Seg_3244" s="T93">du͡o</ta>
            <ta e="T95" id="Seg_3245" s="T94">öjüːn</ta>
            <ta e="T96" id="Seg_3246" s="T95">du͡o</ta>
            <ta e="T97" id="Seg_3247" s="T96">kel-IAK</ta>
            <ta e="T98" id="Seg_3248" s="T97">tüs-LAːK</ta>
            <ta e="T99" id="Seg_3249" s="T98">e-TI-LArA</ta>
            <ta e="T100" id="Seg_3250" s="T99">kɨhɨn</ta>
            <ta e="T101" id="Seg_3251" s="T100">karaŋa</ta>
            <ta e="T102" id="Seg_3252" s="T101">kün</ta>
            <ta e="T103" id="Seg_3253" s="T102">kɨlgas</ta>
            <ta e="T104" id="Seg_3254" s="T103">ki͡ehe-LAr-tA</ta>
            <ta e="T105" id="Seg_3255" s="T104">olus</ta>
            <ta e="T106" id="Seg_3256" s="T105">erde</ta>
            <ta e="T107" id="Seg_3257" s="T106">u͡ol</ta>
            <ta e="T108" id="Seg_3258" s="T107">ogo-LAr</ta>
            <ta e="T109" id="Seg_3259" s="T108">ahaː-Ar</ta>
            <ta e="T110" id="Seg_3260" s="T109">as-LArI-n</ta>
            <ta e="T111" id="Seg_3261" s="T110">tot-IAK-LArI-GAr</ta>
            <ta e="T112" id="Seg_3262" s="T111">di͡eri</ta>
            <ta e="T113" id="Seg_3263" s="T112">ahaː-An</ta>
            <ta e="T114" id="Seg_3264" s="T113">baran</ta>
            <ta e="T115" id="Seg_3265" s="T114">utuj-AːrI</ta>
            <ta e="T116" id="Seg_3266" s="T115">terij-n-BIT-LArA</ta>
            <ta e="T117" id="Seg_3267" s="T116">doː</ta>
            <ta e="T118" id="Seg_3268" s="T117">bihigi</ta>
            <ta e="T119" id="Seg_3269" s="T118">erge</ta>
            <ta e="T120" id="Seg_3270" s="T119">dʼi͡e-BIt</ta>
            <ta e="T121" id="Seg_3271" s="T120">tüːn</ta>
            <ta e="T122" id="Seg_3272" s="T121">kuttaː-Ar-tA</ta>
            <ta e="T123" id="Seg_3273" s="T122">bu͡olu͡o</ta>
            <ta e="T124" id="Seg_3274" s="T123">Mukulaːj</ta>
            <ta e="T125" id="Seg_3275" s="T124">ubaj-tI-GAr</ta>
            <ta e="T126" id="Seg_3276" s="T125">di͡e-BIT-tA</ta>
            <ta e="T127" id="Seg_3277" s="T126">en</ta>
            <ta e="T128" id="Seg_3278" s="T127">emi͡e</ta>
            <ta e="T129" id="Seg_3279" s="T128">kuhagan-LIk</ta>
            <ta e="T130" id="Seg_3280" s="T129">es-n-An</ta>
            <ta e="T131" id="Seg_3281" s="T130">er-A-GIn</ta>
            <ta e="T132" id="Seg_3282" s="T131">gini-nI</ta>
            <ta e="T133" id="Seg_3283" s="T132">ulakan-tA</ta>
            <ta e="T134" id="Seg_3284" s="T133">bu͡oj-BIT-tA</ta>
            <ta e="T135" id="Seg_3285" s="T134">kata</ta>
            <ta e="T136" id="Seg_3286" s="T135">hɨgɨnʼak-LAN</ta>
            <ta e="T137" id="Seg_3287" s="T136">onno</ta>
            <ta e="T138" id="Seg_3288" s="T137">oron-GA</ta>
            <ta e="T139" id="Seg_3289" s="T138">hɨt</ta>
            <ta e="T140" id="Seg_3290" s="T139">biːrge</ta>
            <ta e="T141" id="Seg_3291" s="T140">utuj-IAK-BIt</ta>
            <ta e="T142" id="Seg_3292" s="T141">ubaj-tA</ta>
            <ta e="T143" id="Seg_3293" s="T142">is-tI-GAr</ta>
            <ta e="T144" id="Seg_3294" s="T143">hin</ta>
            <ta e="T145" id="Seg_3295" s="T144">daːganɨ</ta>
            <ta e="T146" id="Seg_3296" s="T145">dʼigis-I-n-Ar</ta>
            <ta e="T147" id="Seg_3297" s="T146">bɨhɨːlaːk</ta>
            <ta e="T148" id="Seg_3298" s="T147">giniler</ta>
            <ta e="T149" id="Seg_3299" s="T148">iččitek</ta>
            <ta e="T150" id="Seg_3300" s="T149">dʼi͡e-GA</ta>
            <ta e="T151" id="Seg_3301" s="T150">kergen-tA</ta>
            <ta e="T152" id="Seg_3302" s="T151">hu͡ok</ta>
            <ta e="T153" id="Seg_3303" s="T152">haŋardɨː</ta>
            <ta e="T154" id="Seg_3304" s="T153">kon-Ar-LAr</ta>
            <ta e="T155" id="Seg_3305" s="T154">ubaj-tA</ta>
            <ta e="T156" id="Seg_3306" s="T155">laːmpa-tI-n</ta>
            <ta e="T157" id="Seg_3307" s="T156">utut-AːT</ta>
            <ta e="T158" id="Seg_3308" s="T157">balɨs-I-tI-n</ta>
            <ta e="T159" id="Seg_3309" s="T158">kos-tI-nAn</ta>
            <ta e="T160" id="Seg_3310" s="T159">hu͡organ</ta>
            <ta e="T161" id="Seg_3311" s="T160">is-tI-GAr</ta>
            <ta e="T162" id="Seg_3312" s="T161">kiːr-An</ta>
            <ta e="T163" id="Seg_3313" s="T162">hɨt-BIT-tA</ta>
            <ta e="T164" id="Seg_3314" s="T163">ogo-LAr</ta>
            <ta e="T165" id="Seg_3315" s="T164">hu͡organ-LArI-n</ta>
            <ta e="T166" id="Seg_3316" s="T165">is-tI-GAr</ta>
            <ta e="T167" id="Seg_3317" s="T166">tɨːn-BAkkA</ta>
            <ta e="T168" id="Seg_3318" s="T167">hɨt-BIT-LArA</ta>
            <ta e="T169" id="Seg_3319" s="T168">kuttan-A</ta>
            <ta e="T170" id="Seg_3320" s="T169">hanaː-An</ta>
            <ta e="T171" id="Seg_3321" s="T170">ihilleː-Ar-LAr</ta>
            <ta e="T172" id="Seg_3322" s="T171">hotoru-kAːN</ta>
            <ta e="T173" id="Seg_3323" s="T172">karak-LArI-n</ta>
            <ta e="T174" id="Seg_3324" s="T173">him-An</ta>
            <ta e="T175" id="Seg_3325" s="T174">utuj-An</ta>
            <ta e="T176" id="Seg_3326" s="T175">bahɨgɨraː-An</ta>
            <ta e="T177" id="Seg_3327" s="T176">kaːl-BIT-LArA</ta>
            <ta e="T178" id="Seg_3328" s="T177">töhö</ta>
            <ta e="T179" id="Seg_3329" s="T178">kačča</ta>
            <ta e="T180" id="Seg_3330" s="T179">bu͡ol-BIT-tA</ta>
            <ta e="T181" id="Seg_3331" s="T180">dʼürü</ta>
            <ta e="T182" id="Seg_3332" s="T181">Mikaː</ta>
            <ta e="T183" id="Seg_3333" s="T182">balɨs-I-tA</ta>
            <ta e="T184" id="Seg_3334" s="T183">ojogos-tI-GAr</ta>
            <ta e="T185" id="Seg_3335" s="T184">üt-IAlAː-Ar-tI-ttAn</ta>
            <ta e="T186" id="Seg_3336" s="T185">uhugun-I-BIT-tA</ta>
            <ta e="T187" id="Seg_3337" s="T186">ubaj</ta>
            <ta e="T188" id="Seg_3338" s="T187">ihilleː</ta>
            <ta e="T189" id="Seg_3339" s="T188">kuttaː-Ar</ta>
            <ta e="T190" id="Seg_3340" s="T189">bɨhɨːlaːk</ta>
            <ta e="T191" id="Seg_3341" s="T190">Mukulaːj</ta>
            <ta e="T192" id="Seg_3342" s="T191">kuttan-An</ta>
            <ta e="T193" id="Seg_3343" s="T192">haŋa-tA</ta>
            <ta e="T194" id="Seg_3344" s="T193">arɨːččɨ</ta>
            <ta e="T195" id="Seg_3345" s="T194">tagɨs-I-BIT-tA</ta>
            <ta e="T196" id="Seg_3346" s="T195">Mikaː</ta>
            <ta e="T197" id="Seg_3347" s="T196">hu͡organ-tI-n</ta>
            <ta e="T198" id="Seg_3348" s="T197">kɨrɨː-tI-n</ta>
            <ta e="T199" id="Seg_3349" s="T198">kötök-An</ta>
            <ta e="T200" id="Seg_3350" s="T199">ihilleː-BIT-tA</ta>
            <ta e="T201" id="Seg_3351" s="T200">kirdik</ta>
            <ta e="T202" id="Seg_3352" s="T201">ahaː-Ar</ta>
            <ta e="T203" id="Seg_3353" s="T202">ostu͡ol-LArI-GAr</ta>
            <ta e="T204" id="Seg_3354" s="T203">teri͡elke-LAr</ta>
            <ta e="T205" id="Seg_3355" s="T204">lu͡osku-LAr</ta>
            <ta e="T206" id="Seg_3356" s="T205">lahɨrgaː-Ar-LAr</ta>
            <ta e="T207" id="Seg_3357" s="T206">[C^1][V^1][C^2]-karaŋa</ta>
            <ta e="T208" id="Seg_3358" s="T207">tu͡ok</ta>
            <ta e="T209" id="Seg_3359" s="T208">da</ta>
            <ta e="T210" id="Seg_3360" s="T209">köhün-I-BAT</ta>
            <ta e="T211" id="Seg_3361" s="T210">kaja</ta>
            <ta e="T212" id="Seg_3362" s="T211">ɨraːk</ta>
            <ta e="T213" id="Seg_3363" s="T212">tur-Ar</ta>
            <ta e="T214" id="Seg_3364" s="T213">ostu͡ol</ta>
            <ta e="T215" id="Seg_3365" s="T214">ürüt-tI-GAr</ta>
            <ta e="T216" id="Seg_3366" s="T215">kumaːkɨ-LAr-nI</ta>
            <ta e="T217" id="Seg_3367" s="T216">tu͡ok</ta>
            <ta e="T218" id="Seg_3368" s="T217">ere</ta>
            <ta e="T220" id="Seg_3369" s="T219">kajɨt-AlAː-Ar</ta>
            <ta e="T221" id="Seg_3370" s="T220">ogo-LAr</ta>
            <ta e="T222" id="Seg_3371" s="T221">hu͡organ</ta>
            <ta e="T223" id="Seg_3372" s="T222">is-tI-GAr</ta>
            <ta e="T224" id="Seg_3373" s="T223">kuːstas-An</ta>
            <ta e="T225" id="Seg_3374" s="T224">baran</ta>
            <ta e="T226" id="Seg_3375" s="T225">kuttan-An</ta>
            <ta e="T227" id="Seg_3376" s="T226">tɨːn-BAkkA</ta>
            <ta e="T228" id="Seg_3377" s="T227">hɨt-Ar-LAr</ta>
            <ta e="T229" id="Seg_3378" s="T228">tahaːra</ta>
            <ta e="T230" id="Seg_3379" s="T229">güːle-GA</ta>
            <ta e="T231" id="Seg_3380" s="T230">tu͡ok</ta>
            <ta e="T232" id="Seg_3381" s="T231">ere</ta>
            <ta e="T233" id="Seg_3382" s="T232">kaːm-AlAː-Ar-tA</ta>
            <ta e="T234" id="Seg_3383" s="T233">ihilin-I-BIT-tA</ta>
            <ta e="T235" id="Seg_3384" s="T234">dʼi͡e</ta>
            <ta e="T236" id="Seg_3385" s="T235">isti͡ene-tI-n</ta>
            <ta e="T237" id="Seg_3386" s="T236">tɨŋ-LIk</ta>
            <ta e="T238" id="Seg_3387" s="T237">ogus-IAlAː-Ar</ta>
            <ta e="T239" id="Seg_3388" s="T238">koloru͡ok-LAr-ttAn</ta>
            <ta e="T240" id="Seg_3389" s="T239">holuːr-LAr-nI</ta>
            <ta e="T241" id="Seg_3390" s="T240">bütte</ta>
            <ta e="T242" id="Seg_3391" s="T241">ihit-LAr-nI</ta>
            <ta e="T243" id="Seg_3392" s="T242">hir-GA</ta>
            <ta e="T244" id="Seg_3393" s="T243">tüher-TAː-Ar</ta>
            <ta e="T245" id="Seg_3394" s="T244">onton</ta>
            <ta e="T246" id="Seg_3395" s="T245">ol-LAr-nI</ta>
            <ta e="T247" id="Seg_3396" s="T246">tep-IAlAː-Ar-tA</ta>
            <ta e="T248" id="Seg_3397" s="T247">ihilin-Ar</ta>
            <ta e="T249" id="Seg_3398" s="T248">učum</ta>
            <ta e="T250" id="Seg_3399" s="T249">möŋ-BAkkA</ta>
            <ta e="T251" id="Seg_3400" s="T250">hɨt</ta>
            <ta e="T252" id="Seg_3401" s="T251">Mikaː</ta>
            <ta e="T253" id="Seg_3402" s="T252">balɨs-I-tI-n</ta>
            <ta e="T254" id="Seg_3403" s="T253">bu͡oj-BIT-tA</ta>
            <ta e="T255" id="Seg_3404" s="T254">dʼi͡e</ta>
            <ta e="T256" id="Seg_3405" s="T255">is-tI-GAr</ta>
            <ta e="T257" id="Seg_3406" s="T256">güːle-GA</ta>
            <ta e="T258" id="Seg_3407" s="T257">ajdaːn</ta>
            <ta e="T259" id="Seg_3408" s="T258">kahan</ta>
            <ta e="T260" id="Seg_3409" s="T259">da</ta>
            <ta e="T261" id="Seg_3410" s="T260">büt-BAT</ta>
            <ta e="T262" id="Seg_3411" s="T261">harsi͡erda-GA</ta>
            <ta e="T263" id="Seg_3412" s="T262">di͡eri</ta>
            <ta e="T264" id="Seg_3413" s="T263">össü͡ö</ta>
            <ta e="T265" id="Seg_3414" s="T264">ɨraːk</ta>
            <ta e="T266" id="Seg_3415" s="T265">giniler-nI</ta>
            <ta e="T267" id="Seg_3416" s="T266">kim</ta>
            <ta e="T268" id="Seg_3417" s="T267">kel-An</ta>
            <ta e="T269" id="Seg_3418" s="T268">bɨːhaː-IAK.[tA]=Ij</ta>
            <ta e="T270" id="Seg_3419" s="T269">ogo-LAr</ta>
            <ta e="T271" id="Seg_3420" s="T270">kaːttar-An</ta>
            <ta e="T272" id="Seg_3421" s="T271">ɨtaː-IAK-LArI-n</ta>
            <ta e="T273" id="Seg_3422" s="T272">da</ta>
            <ta e="T274" id="Seg_3423" s="T273">ü͡ögüleː-IAK-LArI-n</ta>
            <ta e="T275" id="Seg_3424" s="T274">da</ta>
            <ta e="T276" id="Seg_3425" s="T275">bert</ta>
            <ta e="T277" id="Seg_3426" s="T276">ol</ta>
            <ta e="T278" id="Seg_3427" s="T277">kuttan-A</ta>
            <ta e="T279" id="Seg_3428" s="T278">hɨt-An-LAr</ta>
            <ta e="T280" id="Seg_3429" s="T279">uː-LArI-GAr</ta>
            <ta e="T281" id="Seg_3430" s="T280">battaː-A-t-An</ta>
            <ta e="T282" id="Seg_3431" s="T281">utuj-An</ta>
            <ta e="T283" id="Seg_3432" s="T282">kaːl-BIT-LArA</ta>
            <ta e="T284" id="Seg_3433" s="T283">tu͡ok-TAːgAr</ta>
            <ta e="T285" id="Seg_3434" s="T284">da</ta>
            <ta e="T286" id="Seg_3435" s="T285">ulakan</ta>
            <ta e="T287" id="Seg_3436" s="T286">ajdaːn-ttAn</ta>
            <ta e="T288" id="Seg_3437" s="T287">emiske</ta>
            <ta e="T289" id="Seg_3438" s="T288">uhugun-I-BIT-LArA</ta>
            <ta e="T290" id="Seg_3439" s="T289">güːle-GA</ta>
            <ta e="T291" id="Seg_3440" s="T290">tu͡ok</ta>
            <ta e="T292" id="Seg_3441" s="T291">ere</ta>
            <ta e="T293" id="Seg_3442" s="T292">kɨtaːnak-LIk</ta>
            <ta e="T294" id="Seg_3443" s="T293">taːs-LIː</ta>
            <ta e="T295" id="Seg_3444" s="T294">huːl-An</ta>
            <ta e="T296" id="Seg_3445" s="T295">tüs-BIT-tA</ta>
            <ta e="T297" id="Seg_3446" s="T296">ü͡ögüː</ta>
            <ta e="T298" id="Seg_3447" s="T297">kahɨː</ta>
            <ta e="T299" id="Seg_3448" s="T298">ihilin-I-BIT-tA</ta>
            <ta e="T300" id="Seg_3449" s="T299">aːn</ta>
            <ta e="T301" id="Seg_3450" s="T300">arɨj-LIN-I-BIT-tA</ta>
            <ta e="T302" id="Seg_3451" s="T301">dʼi͡e-GA</ta>
            <ta e="T303" id="Seg_3452" s="T302">kim</ta>
            <ta e="T304" id="Seg_3453" s="T303">ere</ta>
            <ta e="T305" id="Seg_3454" s="T304">kiːr-An</ta>
            <ta e="T306" id="Seg_3455" s="T305">laːmpa-nI</ta>
            <ta e="T307" id="Seg_3456" s="T306">ubat-BIT-tA</ta>
            <ta e="T308" id="Seg_3457" s="T307">tur-I-ŋ</ta>
            <ta e="T309" id="Seg_3458" s="T308">ehigi</ta>
            <ta e="T310" id="Seg_3459" s="T309">güːle-GIt</ta>
            <ta e="T311" id="Seg_3460" s="T310">aːn-tI-n</ta>
            <ta e="T312" id="Seg_3461" s="T311">togo</ta>
            <ta e="T313" id="Seg_3462" s="T312">hap-BAtAK-GIt=Ij</ta>
            <ta e="T314" id="Seg_3463" s="T313">koru͡oba-LAr</ta>
            <ta e="T315" id="Seg_3464" s="T314">kiːr-An-LAr</ta>
            <ta e="T316" id="Seg_3465" s="T315">hir-nI</ta>
            <ta e="T317" id="Seg_3466" s="T316">delbi</ta>
            <ta e="T318" id="Seg_3467" s="T317">tep-s-I-BIT-LAr</ta>
            <ta e="T319" id="Seg_3468" s="T318">güːle</ta>
            <ta e="T320" id="Seg_3469" s="T319">is-tI-n</ta>
            <ta e="T321" id="Seg_3470" s="T320">biːr</ta>
            <ta e="T322" id="Seg_3471" s="T321">oŋor-BIT-LAr</ta>
            <ta e="T323" id="Seg_3472" s="T322">hir-nI</ta>
            <ta e="T324" id="Seg_3473" s="T323">toloru</ta>
            <ta e="T325" id="Seg_3474" s="T324">haːktaː-BIT-LAr</ta>
            <ta e="T326" id="Seg_3475" s="T325">ogo-LAr</ta>
            <ta e="T327" id="Seg_3476" s="T326">karak-LArI-n</ta>
            <ta e="T328" id="Seg_3477" s="T327">arɨj-An-LAr</ta>
            <ta e="T329" id="Seg_3478" s="T328">kihi</ta>
            <ta e="T330" id="Seg_3479" s="T329">tur-Ar-tI-n</ta>
            <ta e="T331" id="Seg_3480" s="T330">kör-BIT-LArA</ta>
            <ta e="T332" id="Seg_3481" s="T331">ol-tI-LArA</ta>
            <ta e="T333" id="Seg_3482" s="T332">abalaː-A-s-A-abalaː-A-s-A</ta>
            <ta e="T334" id="Seg_3483" s="T333">taŋas-tI-n</ta>
            <ta e="T335" id="Seg_3484" s="T334">galipi͡e</ta>
            <ta e="T336" id="Seg_3485" s="T335">hɨ͡aldʼa-tI-n</ta>
            <ta e="T337" id="Seg_3486" s="T336">haːk-GA</ta>
            <ta e="T338" id="Seg_3487" s="T337">bis-I-LIN-I-BIT-tI-n</ta>
            <ta e="T339" id="Seg_3488" s="T338">iliː-LAr-tI-nAn</ta>
            <ta e="T340" id="Seg_3489" s="T339">hot-A</ta>
            <ta e="T341" id="Seg_3490" s="T340">tur-BIT-tA</ta>
            <ta e="T342" id="Seg_3491" s="T341">Opu͡o</ta>
            <ta e="T343" id="Seg_3492" s="T342">kolxoz</ta>
            <ta e="T344" id="Seg_3493" s="T343">hopku͡os-tA</ta>
            <ta e="T345" id="Seg_3494" s="T344">üle-tI-GAr</ta>
            <ta e="T346" id="Seg_3495" s="T345">üle-tI-GAr</ta>
            <ta e="T347" id="Seg_3496" s="T346">dʼi͡e</ta>
            <ta e="T348" id="Seg_3497" s="T347">attɨ-nAn</ta>
            <ta e="T349" id="Seg_3498" s="T348">bar-An</ta>
            <ta e="T350" id="Seg_3499" s="T349">is-An</ta>
            <ta e="T351" id="Seg_3500" s="T350">güːle</ta>
            <ta e="T352" id="Seg_3501" s="T351">koru͡oba-LAr</ta>
            <ta e="T353" id="Seg_3502" s="T352">kiːr-BIT-LArI-n</ta>
            <ta e="T354" id="Seg_3503" s="T353">kör-BIT</ta>
            <ta e="T355" id="Seg_3504" s="T354">onton</ta>
            <ta e="T356" id="Seg_3505" s="T355">barɨ-LArI-n</ta>
            <ta e="T357" id="Seg_3506" s="T356">bat-An</ta>
            <ta e="T358" id="Seg_3507" s="T357">tahaːr-BIT-tA</ta>
            <ta e="T359" id="Seg_3508" s="T358">ol</ta>
            <ta e="T360" id="Seg_3509" s="T359">kelin-tI-ttAn</ta>
            <ta e="T361" id="Seg_3510" s="T360">kiːr-An</ta>
            <ta e="T362" id="Seg_3511" s="T361">is-An</ta>
            <ta e="T363" id="Seg_3512" s="T362">kaltɨrɨj-An</ta>
            <ta e="T364" id="Seg_3513" s="T363">tüs-An</ta>
            <ta e="T365" id="Seg_3514" s="T364">koru͡oba-LAr</ta>
            <ta e="T366" id="Seg_3515" s="T365">haːk-LArI-GAr</ta>
            <ta e="T367" id="Seg_3516" s="T366">delbi</ta>
            <ta e="T368" id="Seg_3517" s="T367">bis-I-LIN-I-BIT-tA</ta>
            <ta e="T369" id="Seg_3518" s="T368">tur-A</ta>
            <ta e="T370" id="Seg_3519" s="T369">hataː-An</ta>
            <ta e="T371" id="Seg_3520" s="T370">iliː-LAr-tI-n</ta>
            <ta e="T372" id="Seg_3521" s="T371">emi͡e</ta>
            <ta e="T373" id="Seg_3522" s="T372">olus</ta>
            <ta e="T374" id="Seg_3523" s="T373">bis-BIT-tA</ta>
            <ta e="T375" id="Seg_3524" s="T374">Mikaː-nI</ta>
            <ta e="T376" id="Seg_3525" s="T375">kɨtta</ta>
            <ta e="T377" id="Seg_3526" s="T376">Mukulaːj</ta>
            <ta e="T378" id="Seg_3527" s="T377">kɨjgan-BIT</ta>
            <ta e="T379" id="Seg_3528" s="T378">kihi</ta>
            <ta e="T380" id="Seg_3529" s="T379">kel-BIT-tI-GAr</ta>
            <ta e="T381" id="Seg_3530" s="T380">ulakan-LIk</ta>
            <ta e="T382" id="Seg_3531" s="T381">ü͡ör-BIT-LArA</ta>
            <ta e="T383" id="Seg_3532" s="T382">Mikaː</ta>
            <ta e="T384" id="Seg_3533" s="T383">bil-BAT</ta>
            <ta e="T385" id="Seg_3534" s="T384">kihi-tI-ttAn</ta>
            <ta e="T386" id="Seg_3535" s="T385">atɨŋɨrgaː-An</ta>
            <ta e="T387" id="Seg_3536" s="T386">karak-tI-n</ta>
            <ta e="T388" id="Seg_3537" s="T387">kisteː-A-kisteː-A</ta>
            <ta e="T389" id="Seg_3538" s="T388">di͡e-BIT-tA</ta>
            <ta e="T390" id="Seg_3539" s="T389">bihigi-nI</ta>
            <ta e="T391" id="Seg_3540" s="T390">tüːn</ta>
            <ta e="T392" id="Seg_3541" s="T391">kuttaː-BIT-tA</ta>
            <ta e="T393" id="Seg_3542" s="T392">kot-An</ta>
            <ta e="T394" id="Seg_3543" s="T393">utuj-BA-TI-BIt</ta>
            <ta e="T395" id="Seg_3544" s="T394">tüːn-nI</ta>
            <ta e="T396" id="Seg_3545" s="T395">huptu</ta>
            <ta e="T397" id="Seg_3546" s="T396">tu͡ok</ta>
            <ta e="T398" id="Seg_3547" s="T397">bu͡ol-BIT-tI-n</ta>
            <ta e="T399" id="Seg_3548" s="T398">barɨ-tI-n</ta>
            <ta e="T400" id="Seg_3549" s="T399">kepseː-BIT-tA</ta>
            <ta e="T401" id="Seg_3550" s="T400">Opu͡o</ta>
            <ta e="T402" id="Seg_3551" s="T401">ogo-LAr</ta>
            <ta e="T403" id="Seg_3552" s="T402">kuttan-BIT</ta>
            <ta e="T404" id="Seg_3553" s="T403">dʼühün-LArI-n</ta>
            <ta e="T405" id="Seg_3554" s="T404">kör-An</ta>
            <ta e="T406" id="Seg_3555" s="T405">giniler-nI</ta>
            <ta e="T407" id="Seg_3556" s="T406">ahɨn-BIT-tA</ta>
            <ta e="T408" id="Seg_3557" s="T407">kɨjgan-BIT-tA</ta>
            <ta e="T409" id="Seg_3558" s="T408">honno</ta>
            <ta e="T410" id="Seg_3559" s="T409">aːs-BIT-tA</ta>
            <ta e="T411" id="Seg_3560" s="T410">kül-A-kül-A</ta>
            <ta e="T412" id="Seg_3561" s="T411">hordoːk-LAr-tI-GAr</ta>
            <ta e="T413" id="Seg_3562" s="T412">haŋar-BIT-tA</ta>
            <ta e="T414" id="Seg_3563" s="T413">tüːn</ta>
            <ta e="T415" id="Seg_3564" s="T414">ehigi-nI</ta>
            <ta e="T416" id="Seg_3565" s="T415">koru͡oba-LAr-nI</ta>
            <ta e="T417" id="Seg_3566" s="T416">kɨtta</ta>
            <ta e="T418" id="Seg_3567" s="T417">kutujak-LAr</ta>
            <ta e="T419" id="Seg_3568" s="T418">kuttaː-BIT-LArA</ta>
            <ta e="T420" id="Seg_3569" s="T419">haːt-BAT-GIt</ta>
            <ta e="T421" id="Seg_3570" s="T420">du͡o</ta>
            <ta e="T422" id="Seg_3571" s="T421">tuha-tA</ta>
            <ta e="T423" id="Seg_3572" s="T422">hu͡ok-ttAn</ta>
            <ta e="T424" id="Seg_3573" s="T423">kuttan-BIT-GItI-n</ta>
            <ta e="T425" id="Seg_3574" s="T424">bil-An-GIt</ta>
            <ta e="T426" id="Seg_3575" s="T425">ehigi</ta>
            <ta e="T427" id="Seg_3576" s="T426">kuhagan-GItI-ttAn</ta>
            <ta e="T428" id="Seg_3577" s="T427">min</ta>
            <ta e="T431" id="Seg_3578" s="T430">min</ta>
            <ta e="T432" id="Seg_3579" s="T431">olus</ta>
            <ta e="T433" id="Seg_3580" s="T432">bis-I-LIN-TI-m</ta>
            <ta e="T434" id="Seg_3581" s="T433">inʼe-GIt</ta>
            <ta e="T435" id="Seg_3582" s="T434">aga-GIt</ta>
            <ta e="T436" id="Seg_3583" s="T435">kel-IAK-LArI-GAr</ta>
            <ta e="T437" id="Seg_3584" s="T436">di͡eri</ta>
            <ta e="T438" id="Seg_3585" s="T437">güːle-GItI-n</ta>
            <ta e="T439" id="Seg_3586" s="T438">kɨtta</ta>
            <ta e="T440" id="Seg_3587" s="T439">dʼi͡e-GIt</ta>
            <ta e="T441" id="Seg_3588" s="T440">ihin</ta>
            <ta e="T442" id="Seg_3589" s="T441">üčügej-LIk</ta>
            <ta e="T443" id="Seg_3590" s="T442">karaj-I-ŋ</ta>
            <ta e="T444" id="Seg_3591" s="T443">Opu͡o</ta>
            <ta e="T445" id="Seg_3592" s="T444">tagɨs-AːrI</ta>
            <ta e="T446" id="Seg_3593" s="T445">tur-An</ta>
            <ta e="T447" id="Seg_3594" s="T446">ogo-LAr-nI</ta>
            <ta e="T448" id="Seg_3595" s="T447">mohuj-BIT-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_3596" s="T0">scare-PTCP.PST-3SG.[NOM]</ta>
            <ta e="T2" id="Seg_3597" s="T1">two</ta>
            <ta e="T3" id="Seg_3598" s="T2">boy.[NOM]</ta>
            <ta e="T4" id="Seg_3599" s="T3">child-PL.[NOM]</ta>
            <ta e="T5" id="Seg_3600" s="T4">Mika-ACC</ta>
            <ta e="T6" id="Seg_3601" s="T5">with</ta>
            <ta e="T7" id="Seg_3602" s="T6">Mukulaj.[NOM]</ta>
            <ta e="T8" id="Seg_3603" s="T7">parents-3PL.[NOM]</ta>
            <ta e="T9" id="Seg_3604" s="T8">go-PTCP.PST-3PL-ACC</ta>
            <ta e="T10" id="Seg_3605" s="T9">after</ta>
            <ta e="T11" id="Seg_3606" s="T10">house-3PL-DAT/LOC</ta>
            <ta e="T12" id="Seg_3607" s="T11">overnight-PST2-3PL</ta>
            <ta e="T13" id="Seg_3608" s="T12">3PL.[NOM]</ta>
            <ta e="T14" id="Seg_3609" s="T13">learn-PTCP.PRS</ta>
            <ta e="T15" id="Seg_3610" s="T14">time-3PL-DAT/LOC</ta>
            <ta e="T16" id="Seg_3611" s="T15">winter-ACC</ta>
            <ta e="T17" id="Seg_3612" s="T16">through</ta>
            <ta e="T18" id="Seg_3613" s="T17">boarding.school-DAT/LOC</ta>
            <ta e="T19" id="Seg_3614" s="T18">be-HAB-3PL</ta>
            <ta e="T20" id="Seg_3615" s="T19">long.ago-ADJZ</ta>
            <ta e="T21" id="Seg_3616" s="T20">Russian</ta>
            <ta e="T22" id="Seg_3617" s="T21">house.[NOM]</ta>
            <ta e="T23" id="Seg_3618" s="T22">wall-3SG-GEN</ta>
            <ta e="T24" id="Seg_3619" s="T23">wood-PL-3SG.[NOM]</ta>
            <ta e="T25" id="Seg_3620" s="T24">last-3SG.[NOM]</ta>
            <ta e="T26" id="Seg_3621" s="T25">be-CVB.SEQ-3PL</ta>
            <ta e="T27" id="Seg_3622" s="T26">very</ta>
            <ta e="T30" id="Seg_3623" s="T29">moulder-PST2-3PL</ta>
            <ta e="T31" id="Seg_3624" s="T30">out-ADJZ</ta>
            <ta e="T32" id="Seg_3625" s="T31">entryway-3SG-GEN</ta>
            <ta e="T33" id="Seg_3626" s="T32">door-3SG.[NOM]</ta>
            <ta e="T34" id="Seg_3627" s="T33">human.being.[NOM]</ta>
            <ta e="T35" id="Seg_3628" s="T34">open-TEMP-3SG</ta>
            <ta e="T36" id="Seg_3629" s="T35">creak-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T37" id="Seg_3630" s="T36">very</ta>
            <ta e="T38" id="Seg_3631" s="T37">house.[NOM]</ta>
            <ta e="T39" id="Seg_3632" s="T38">inside-3SG-DAT/LOC</ta>
            <ta e="T40" id="Seg_3633" s="T39">how.much</ta>
            <ta e="T41" id="Seg_3634" s="T40">EMPH</ta>
            <ta e="T42" id="Seg_3635" s="T41">noise.[NOM]</ta>
            <ta e="T43" id="Seg_3636" s="T42">be-IMP.3SG</ta>
            <ta e="T44" id="Seg_3637" s="T43">who.[NOM]</ta>
            <ta e="T45" id="Seg_3638" s="T44">INDEF</ta>
            <ta e="T46" id="Seg_3639" s="T45">open-TEMP-3SG</ta>
            <ta e="T47" id="Seg_3640" s="T46">creak-PTCP.PRS</ta>
            <ta e="T48" id="Seg_3641" s="T47">sound-3SG.[NOM]</ta>
            <ta e="T49" id="Seg_3642" s="T48">immediately</ta>
            <ta e="T50" id="Seg_3643" s="T49">be.heard-PRS.[3SG]</ta>
            <ta e="T51" id="Seg_3644" s="T50">some.[NOM]</ta>
            <ta e="T52" id="Seg_3645" s="T51">as</ta>
            <ta e="T53" id="Seg_3646" s="T52">wind.[NOM]</ta>
            <ta e="T54" id="Seg_3647" s="T53">beat-CVB.SEQ</ta>
            <ta e="T55" id="Seg_3648" s="T54">open-TEMP-3SG</ta>
            <ta e="T56" id="Seg_3649" s="T55">creak-CVB.SEQ</ta>
            <ta e="T57" id="Seg_3650" s="T56">house-PROPR-PL-ACC</ta>
            <ta e="T58" id="Seg_3651" s="T57">live.[CAUS]-NEG.[3SG]</ta>
            <ta e="T59" id="Seg_3652" s="T58">NEG</ta>
            <ta e="T60" id="Seg_3653" s="T59">sleep-CAUS-NEG.[3SG]</ta>
            <ta e="T61" id="Seg_3654" s="T60">NEG</ta>
            <ta e="T62" id="Seg_3655" s="T61">rayon.[NOM]</ta>
            <ta e="T63" id="Seg_3656" s="T62">centre</ta>
            <ta e="T64" id="Seg_3657" s="T63">settlement-3SG-DAT/LOC</ta>
            <ta e="T65" id="Seg_3658" s="T64">such</ta>
            <ta e="T66" id="Seg_3659" s="T65">very</ta>
            <ta e="T67" id="Seg_3660" s="T66">old</ta>
            <ta e="T68" id="Seg_3661" s="T67">house.[NOM]</ta>
            <ta e="T69" id="Seg_3662" s="T68">NEG.EX</ta>
            <ta e="T70" id="Seg_3663" s="T69">house-PROPR-PL.[NOM]</ta>
            <ta e="T71" id="Seg_3664" s="T70">soon</ta>
            <ta e="T72" id="Seg_3665" s="T71">new</ta>
            <ta e="T73" id="Seg_3666" s="T72">house-DAT/LOC</ta>
            <ta e="T74" id="Seg_3667" s="T73">get-EP-REFL-CVB.SEQ</ta>
            <ta e="T75" id="Seg_3668" s="T74">fall</ta>
            <ta e="T76" id="Seg_3669" s="T75">carry-EP-REFL-PTCP.FUT</ta>
            <ta e="T77" id="Seg_3670" s="T76">fall-NEC-3PL</ta>
            <ta e="T78" id="Seg_3671" s="T77">big</ta>
            <ta e="T79" id="Seg_3672" s="T78">new</ta>
            <ta e="T80" id="Seg_3673" s="T79">house.[NOM]</ta>
            <ta e="T81" id="Seg_3674" s="T80">build-EP-PASS/REFL-CVB.SEQ</ta>
            <ta e="T82" id="Seg_3675" s="T81">stop-PTCP.PRS-3SG-ACC</ta>
            <ta e="T83" id="Seg_3676" s="T82">wait-CVB.SIM</ta>
            <ta e="T84" id="Seg_3677" s="T83">sit-PRS-3PL</ta>
            <ta e="T85" id="Seg_3678" s="T84">now</ta>
            <ta e="T86" id="Seg_3679" s="T85">child-PL.[NOM]</ta>
            <ta e="T87" id="Seg_3680" s="T86">parents-3PL.[NOM]</ta>
            <ta e="T88" id="Seg_3681" s="T87">close</ta>
            <ta e="T89" id="Seg_3682" s="T88">sibling-3PL-DAT/LOC</ta>
            <ta e="T90" id="Seg_3683" s="T89">visit-CVB.SIM</ta>
            <ta e="T91" id="Seg_3684" s="T90">go-PST2-3PL</ta>
            <ta e="T92" id="Seg_3685" s="T91">house-3PL-DAT/LOC</ta>
            <ta e="T93" id="Seg_3686" s="T92">tomorrow</ta>
            <ta e="T94" id="Seg_3687" s="T93">Q</ta>
            <ta e="T95" id="Seg_3688" s="T94">day.after.tomorrow</ta>
            <ta e="T96" id="Seg_3689" s="T95">Q</ta>
            <ta e="T97" id="Seg_3690" s="T96">come-PTCP.FUT</ta>
            <ta e="T98" id="Seg_3691" s="T97">fall-NEC</ta>
            <ta e="T99" id="Seg_3692" s="T98">be-PST1-3PL</ta>
            <ta e="T100" id="Seg_3693" s="T99">in.winter</ta>
            <ta e="T101" id="Seg_3694" s="T100">dark</ta>
            <ta e="T102" id="Seg_3695" s="T101">day.[NOM]</ta>
            <ta e="T103" id="Seg_3696" s="T102">short.[NOM]</ta>
            <ta e="T104" id="Seg_3697" s="T103">evening-PL-3SG.[NOM]</ta>
            <ta e="T105" id="Seg_3698" s="T104">very</ta>
            <ta e="T106" id="Seg_3699" s="T105">early</ta>
            <ta e="T107" id="Seg_3700" s="T106">boy.[NOM]</ta>
            <ta e="T108" id="Seg_3701" s="T107">child-PL.[NOM]</ta>
            <ta e="T109" id="Seg_3702" s="T108">eat-PTCP.PRS</ta>
            <ta e="T110" id="Seg_3703" s="T109">food-3PL-ACC</ta>
            <ta e="T111" id="Seg_3704" s="T110">eat.ones.fill-PTCP.FUT-3PL-DAT/LOC</ta>
            <ta e="T112" id="Seg_3705" s="T111">until</ta>
            <ta e="T113" id="Seg_3706" s="T112">eat-CVB.SEQ</ta>
            <ta e="T114" id="Seg_3707" s="T113">after</ta>
            <ta e="T115" id="Seg_3708" s="T114">sleep-CVB.PURP</ta>
            <ta e="T116" id="Seg_3709" s="T115">prepare-REFL-PST2-3PL</ta>
            <ta e="T117" id="Seg_3710" s="T116">well</ta>
            <ta e="T118" id="Seg_3711" s="T117">1PL.[NOM]</ta>
            <ta e="T119" id="Seg_3712" s="T118">old</ta>
            <ta e="T120" id="Seg_3713" s="T119">house-1PL.[NOM]</ta>
            <ta e="T121" id="Seg_3714" s="T120">at.night</ta>
            <ta e="T122" id="Seg_3715" s="T121">scare-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T123" id="Seg_3716" s="T122">probably</ta>
            <ta e="T124" id="Seg_3717" s="T123">Mukulaj.[NOM]</ta>
            <ta e="T125" id="Seg_3718" s="T124">elder.brother-3SG-DAT/LOC</ta>
            <ta e="T126" id="Seg_3719" s="T125">say-PST2-3SG</ta>
            <ta e="T127" id="Seg_3720" s="T126">2SG.[NOM]</ta>
            <ta e="T128" id="Seg_3721" s="T127">again</ta>
            <ta e="T129" id="Seg_3722" s="T128">bad-ADVZ</ta>
            <ta e="T130" id="Seg_3723" s="T129">throw.away-REFL-CVB.SEQ</ta>
            <ta e="T131" id="Seg_3724" s="T130">be-PRS-2SG</ta>
            <ta e="T132" id="Seg_3725" s="T131">3SG-ACC</ta>
            <ta e="T133" id="Seg_3726" s="T132">big-3SG.[NOM]</ta>
            <ta e="T134" id="Seg_3727" s="T133">scold-PST2-3SG</ta>
            <ta e="T135" id="Seg_3728" s="T134">instead</ta>
            <ta e="T136" id="Seg_3729" s="T135">naked-VBZ.[IMP.2SG]</ta>
            <ta e="T137" id="Seg_3730" s="T136">there</ta>
            <ta e="T138" id="Seg_3731" s="T137">bed-DAT/LOC</ta>
            <ta e="T139" id="Seg_3732" s="T138">lie.down.[IMP.2SG]</ta>
            <ta e="T140" id="Seg_3733" s="T139">together</ta>
            <ta e="T141" id="Seg_3734" s="T140">sleep-FUT-1PL</ta>
            <ta e="T142" id="Seg_3735" s="T141">elder.brother-3SG.[NOM]</ta>
            <ta e="T143" id="Seg_3736" s="T142">inside-3SG-DAT/LOC</ta>
            <ta e="T144" id="Seg_3737" s="T143">however</ta>
            <ta e="T145" id="Seg_3738" s="T144">EMPH</ta>
            <ta e="T146" id="Seg_3739" s="T145">alert-EP-MED-PRS.[3SG]</ta>
            <ta e="T147" id="Seg_3740" s="T146">apparently</ta>
            <ta e="T148" id="Seg_3741" s="T147">3PL.[NOM]</ta>
            <ta e="T149" id="Seg_3742" s="T148">empty</ta>
            <ta e="T150" id="Seg_3743" s="T149">house-DAT/LOC</ta>
            <ta e="T151" id="Seg_3744" s="T150">parents-POSS</ta>
            <ta e="T152" id="Seg_3745" s="T151">NEG</ta>
            <ta e="T153" id="Seg_3746" s="T152">for.the.first.time</ta>
            <ta e="T154" id="Seg_3747" s="T153">overnight-PRS-3PL</ta>
            <ta e="T155" id="Seg_3748" s="T154">elder.brother-3SG.[NOM]</ta>
            <ta e="T156" id="Seg_3749" s="T155">lamp-3SG-ACC</ta>
            <ta e="T157" id="Seg_3750" s="T156">put.out-CVB.ANT</ta>
            <ta e="T158" id="Seg_3751" s="T157">younger.brother-EP-3SG-GEN</ta>
            <ta e="T159" id="Seg_3752" s="T158">row-3SG-INSTR</ta>
            <ta e="T160" id="Seg_3753" s="T159">blanket.[NOM]</ta>
            <ta e="T161" id="Seg_3754" s="T160">inside-3SG-DAT/LOC</ta>
            <ta e="T162" id="Seg_3755" s="T161">go.in-CVB.SEQ</ta>
            <ta e="T163" id="Seg_3756" s="T162">lie.down-PST2-3SG</ta>
            <ta e="T164" id="Seg_3757" s="T163">child-PL.[NOM]</ta>
            <ta e="T165" id="Seg_3758" s="T164">blanket-3PL-GEN</ta>
            <ta e="T166" id="Seg_3759" s="T165">inside-3SG-DAT/LOC</ta>
            <ta e="T167" id="Seg_3760" s="T166">breath-NEG.CVB.SIM</ta>
            <ta e="T168" id="Seg_3761" s="T167">lie-PST2-3PL</ta>
            <ta e="T169" id="Seg_3762" s="T168">be.afraid-CVB.SIM</ta>
            <ta e="T170" id="Seg_3763" s="T169">think-CVB.SEQ</ta>
            <ta e="T171" id="Seg_3764" s="T170">listen-PRS-3PL</ta>
            <ta e="T172" id="Seg_3765" s="T171">soon-INTNS</ta>
            <ta e="T173" id="Seg_3766" s="T172">eye-3PL-ACC</ta>
            <ta e="T174" id="Seg_3767" s="T173">close.eyes-CVB.SEQ</ta>
            <ta e="T175" id="Seg_3768" s="T174">fall.asleep-CVB.SEQ</ta>
            <ta e="T176" id="Seg_3769" s="T175">snore-CVB.SEQ</ta>
            <ta e="T177" id="Seg_3770" s="T176">stay-PST2-3PL</ta>
            <ta e="T178" id="Seg_3771" s="T177">how.much</ta>
            <ta e="T179" id="Seg_3772" s="T178">how.much</ta>
            <ta e="T180" id="Seg_3773" s="T179">be-PST2-3SG</ta>
            <ta e="T181" id="Seg_3774" s="T180">Q</ta>
            <ta e="T182" id="Seg_3775" s="T181">Mika.[NOM]</ta>
            <ta e="T183" id="Seg_3776" s="T182">younger.brother-EP-3SG.[NOM]</ta>
            <ta e="T184" id="Seg_3777" s="T183">side-3SG-DAT/LOC</ta>
            <ta e="T185" id="Seg_3778" s="T184">push-FREQ-PTCP.PRS-3SG-ABL</ta>
            <ta e="T186" id="Seg_3779" s="T185">wake.up-EP-PST2-3SG</ta>
            <ta e="T187" id="Seg_3780" s="T186">elder.brother.[NOM]</ta>
            <ta e="T188" id="Seg_3781" s="T187">listen.[IMP.2SG]</ta>
            <ta e="T189" id="Seg_3782" s="T188">scare-PTCP.PRS.[NOM]</ta>
            <ta e="T190" id="Seg_3783" s="T189">apparently</ta>
            <ta e="T191" id="Seg_3784" s="T190">Mukulaj.[NOM]</ta>
            <ta e="T192" id="Seg_3785" s="T191">be.afraid-CVB.SEQ</ta>
            <ta e="T193" id="Seg_3786" s="T192">word-3SG.[NOM]</ta>
            <ta e="T194" id="Seg_3787" s="T193">hardly</ta>
            <ta e="T195" id="Seg_3788" s="T194">go.out-EP-PST2-3SG</ta>
            <ta e="T196" id="Seg_3789" s="T195">Mika.[NOM]</ta>
            <ta e="T197" id="Seg_3790" s="T196">blanket-3SG-GEN</ta>
            <ta e="T198" id="Seg_3791" s="T197">edge-3SG-ACC</ta>
            <ta e="T199" id="Seg_3792" s="T198">raise-CVB.SEQ</ta>
            <ta e="T200" id="Seg_3793" s="T199">listen-PST2-3SG</ta>
            <ta e="T201" id="Seg_3794" s="T200">truth.[NOM]</ta>
            <ta e="T202" id="Seg_3795" s="T201">eat-PTCP.PRS</ta>
            <ta e="T203" id="Seg_3796" s="T202">table-3PL-DAT/LOC</ta>
            <ta e="T204" id="Seg_3797" s="T203">plate-PL.[NOM]</ta>
            <ta e="T205" id="Seg_3798" s="T204">spoon-PL.[NOM]</ta>
            <ta e="T206" id="Seg_3799" s="T205">rattle-PRS-3PL</ta>
            <ta e="T207" id="Seg_3800" s="T206">EMPH-dark.[NOM]</ta>
            <ta e="T208" id="Seg_3801" s="T207">what.[NOM]</ta>
            <ta e="T209" id="Seg_3802" s="T208">NEG</ta>
            <ta e="T210" id="Seg_3803" s="T209">to.be.on.view-EP-NEG.[3SG]</ta>
            <ta e="T211" id="Seg_3804" s="T210">well</ta>
            <ta e="T212" id="Seg_3805" s="T211">far.away</ta>
            <ta e="T213" id="Seg_3806" s="T212">stand-PTCP.PRS</ta>
            <ta e="T214" id="Seg_3807" s="T213">table.[NOM]</ta>
            <ta e="T215" id="Seg_3808" s="T214">upper.part-3SG-DAT/LOC</ta>
            <ta e="T216" id="Seg_3809" s="T215">papers-PL-ACC</ta>
            <ta e="T217" id="Seg_3810" s="T216">what.[NOM]</ta>
            <ta e="T218" id="Seg_3811" s="T217">INDEF</ta>
            <ta e="T220" id="Seg_3812" s="T219">tear-FREQ-PRS.[3SG]</ta>
            <ta e="T221" id="Seg_3813" s="T220">child-PL.[NOM]</ta>
            <ta e="T222" id="Seg_3814" s="T221">blanket.[NOM]</ta>
            <ta e="T223" id="Seg_3815" s="T222">inside-3SG-DAT/LOC</ta>
            <ta e="T224" id="Seg_3816" s="T223">embrace.each.other-CVB.SEQ</ta>
            <ta e="T225" id="Seg_3817" s="T224">after</ta>
            <ta e="T226" id="Seg_3818" s="T225">be.afraid-CVB.SEQ</ta>
            <ta e="T227" id="Seg_3819" s="T226">breath-NEG.CVB.SIM</ta>
            <ta e="T228" id="Seg_3820" s="T227">lie-PRS-3PL</ta>
            <ta e="T229" id="Seg_3821" s="T228">outside</ta>
            <ta e="T230" id="Seg_3822" s="T229">entryway-DAT/LOC</ta>
            <ta e="T231" id="Seg_3823" s="T230">what.[NOM]</ta>
            <ta e="T232" id="Seg_3824" s="T231">INDEF</ta>
            <ta e="T233" id="Seg_3825" s="T232">walk-FREQ-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T234" id="Seg_3826" s="T233">be.heard-EP-PST2-3SG</ta>
            <ta e="T235" id="Seg_3827" s="T234">house.[NOM]</ta>
            <ta e="T236" id="Seg_3828" s="T235">wall-3SG-ACC</ta>
            <ta e="T237" id="Seg_3829" s="T236">loud-ADVZ</ta>
            <ta e="T238" id="Seg_3830" s="T237">beat-FREQ-PRS.[3SG]</ta>
            <ta e="T239" id="Seg_3831" s="T238">shelf-PL-ABL</ta>
            <ta e="T240" id="Seg_3832" s="T239">kettle-PL-ACC</ta>
            <ta e="T241" id="Seg_3833" s="T240">various</ta>
            <ta e="T242" id="Seg_3834" s="T241">jar-PL-ACC</ta>
            <ta e="T243" id="Seg_3835" s="T242">earth-DAT/LOC</ta>
            <ta e="T244" id="Seg_3836" s="T243">drop-ITER-PRS.[3SG]</ta>
            <ta e="T245" id="Seg_3837" s="T244">then</ta>
            <ta e="T246" id="Seg_3838" s="T245">that-PL-ACC</ta>
            <ta e="T247" id="Seg_3839" s="T246">kick-FREQ-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T248" id="Seg_3840" s="T247">be.heard-PRS.[3SG]</ta>
            <ta e="T249" id="Seg_3841" s="T248">quiet</ta>
            <ta e="T250" id="Seg_3842" s="T249">move-NEG.CVB.SIM</ta>
            <ta e="T251" id="Seg_3843" s="T250">lie.[IMP.2SG]</ta>
            <ta e="T252" id="Seg_3844" s="T251">Mika.[NOM]</ta>
            <ta e="T253" id="Seg_3845" s="T252">younger.brother-EP-3SG-ACC</ta>
            <ta e="T254" id="Seg_3846" s="T253">scold-PST2-3SG</ta>
            <ta e="T255" id="Seg_3847" s="T254">house.[NOM]</ta>
            <ta e="T256" id="Seg_3848" s="T255">inside-3SG-DAT/LOC</ta>
            <ta e="T257" id="Seg_3849" s="T256">entryway-DAT/LOC</ta>
            <ta e="T258" id="Seg_3850" s="T257">noise.[NOM]</ta>
            <ta e="T259" id="Seg_3851" s="T258">when</ta>
            <ta e="T260" id="Seg_3852" s="T259">NEG</ta>
            <ta e="T261" id="Seg_3853" s="T260">stop-NEG.[3SG]</ta>
            <ta e="T262" id="Seg_3854" s="T261">morning-DAT/LOC</ta>
            <ta e="T263" id="Seg_3855" s="T262">until</ta>
            <ta e="T264" id="Seg_3856" s="T263">still</ta>
            <ta e="T265" id="Seg_3857" s="T264">distant.[NOM]</ta>
            <ta e="T266" id="Seg_3858" s="T265">3PL-ACC</ta>
            <ta e="T267" id="Seg_3859" s="T266">who.[NOM]</ta>
            <ta e="T268" id="Seg_3860" s="T267">come-CVB.SEQ</ta>
            <ta e="T269" id="Seg_3861" s="T268">save-FUT.[3SG]=Q</ta>
            <ta e="T270" id="Seg_3862" s="T269">child-PL.[NOM]</ta>
            <ta e="T271" id="Seg_3863" s="T270">freeze.in.shock-CVB.SEQ</ta>
            <ta e="T272" id="Seg_3864" s="T271">cry-PTCP.FUT-3PL-ACC</ta>
            <ta e="T273" id="Seg_3865" s="T272">NEG</ta>
            <ta e="T274" id="Seg_3866" s="T273">shout-PTCP.FUT-3PL-ACC</ta>
            <ta e="T275" id="Seg_3867" s="T274">NEG</ta>
            <ta e="T276" id="Seg_3868" s="T275">very</ta>
            <ta e="T277" id="Seg_3869" s="T276">that</ta>
            <ta e="T278" id="Seg_3870" s="T277">be.afraid-CVB.SIM</ta>
            <ta e="T279" id="Seg_3871" s="T278">lie-CVB.SEQ-3PL</ta>
            <ta e="T280" id="Seg_3872" s="T279">sleep-3PL-DAT/LOC</ta>
            <ta e="T281" id="Seg_3873" s="T280">pull.down-EP-CAUS-CVB.SEQ</ta>
            <ta e="T282" id="Seg_3874" s="T281">sleep-CVB.SEQ</ta>
            <ta e="T283" id="Seg_3875" s="T282">stay-PST2-3PL</ta>
            <ta e="T284" id="Seg_3876" s="T283">what-COMP</ta>
            <ta e="T285" id="Seg_3877" s="T284">NEG</ta>
            <ta e="T286" id="Seg_3878" s="T285">big</ta>
            <ta e="T287" id="Seg_3879" s="T286">noise-ABL</ta>
            <ta e="T288" id="Seg_3880" s="T287">unexpectedly</ta>
            <ta e="T289" id="Seg_3881" s="T288">wake.up-EP-PST2-3PL</ta>
            <ta e="T290" id="Seg_3882" s="T289">entryway-DAT/LOC</ta>
            <ta e="T291" id="Seg_3883" s="T290">what.[NOM]</ta>
            <ta e="T292" id="Seg_3884" s="T291">INDEF</ta>
            <ta e="T293" id="Seg_3885" s="T292">hard-ADVZ</ta>
            <ta e="T294" id="Seg_3886" s="T293">stone-SIM</ta>
            <ta e="T295" id="Seg_3887" s="T294">crash-CVB.SEQ</ta>
            <ta e="T296" id="Seg_3888" s="T295">fall-PST2-3SG</ta>
            <ta e="T297" id="Seg_3889" s="T296">shout.[NOM]</ta>
            <ta e="T298" id="Seg_3890" s="T297">scream.[NOM]</ta>
            <ta e="T299" id="Seg_3891" s="T298">be.heard-EP-PST2-3SG</ta>
            <ta e="T300" id="Seg_3892" s="T299">door.[NOM]</ta>
            <ta e="T301" id="Seg_3893" s="T300">open-PASS/REFL-EP-PST2-3SG</ta>
            <ta e="T302" id="Seg_3894" s="T301">house-DAT/LOC</ta>
            <ta e="T303" id="Seg_3895" s="T302">who.[NOM]</ta>
            <ta e="T304" id="Seg_3896" s="T303">INDEF</ta>
            <ta e="T305" id="Seg_3897" s="T304">go.in-CVB.SEQ</ta>
            <ta e="T306" id="Seg_3898" s="T305">lamp-ACC</ta>
            <ta e="T307" id="Seg_3899" s="T306">light-PST2-3SG</ta>
            <ta e="T308" id="Seg_3900" s="T307">stand.up-EP-IMP.2PL</ta>
            <ta e="T309" id="Seg_3901" s="T308">2PL.[NOM]</ta>
            <ta e="T310" id="Seg_3902" s="T309">entryway-2PL.[NOM]</ta>
            <ta e="T311" id="Seg_3903" s="T310">door-3SG-ACC</ta>
            <ta e="T312" id="Seg_3904" s="T311">why</ta>
            <ta e="T313" id="Seg_3905" s="T312">close-PST2.NEG-2PL=Q</ta>
            <ta e="T314" id="Seg_3906" s="T313">cow-PL.[NOM]</ta>
            <ta e="T315" id="Seg_3907" s="T314">go.in-CVB.SEQ-3PL</ta>
            <ta e="T316" id="Seg_3908" s="T315">earth-ACC</ta>
            <ta e="T317" id="Seg_3909" s="T316">enough</ta>
            <ta e="T318" id="Seg_3910" s="T317">kick-RECP/COLL-EP-PST2-3PL</ta>
            <ta e="T319" id="Seg_3911" s="T318">entryway.[NOM]</ta>
            <ta e="T320" id="Seg_3912" s="T319">inside-3SG-ACC</ta>
            <ta e="T321" id="Seg_3913" s="T320">one</ta>
            <ta e="T322" id="Seg_3914" s="T321">make-PST2-3PL</ta>
            <ta e="T323" id="Seg_3915" s="T322">earth-ACC</ta>
            <ta e="T324" id="Seg_3916" s="T323">full</ta>
            <ta e="T325" id="Seg_3917" s="T324">empty.oneself-PST2-3PL</ta>
            <ta e="T326" id="Seg_3918" s="T325">child-PL.[NOM]</ta>
            <ta e="T327" id="Seg_3919" s="T326">eye-3PL-ACC</ta>
            <ta e="T328" id="Seg_3920" s="T327">open-CVB.SEQ-3PL</ta>
            <ta e="T329" id="Seg_3921" s="T328">human.being.[NOM]</ta>
            <ta e="T330" id="Seg_3922" s="T329">stand-PTCP.PRS-3SG-ACC</ta>
            <ta e="T331" id="Seg_3923" s="T330">see-PST2-3PL</ta>
            <ta e="T332" id="Seg_3924" s="T331">that-3SG-3PL.[NOM]</ta>
            <ta e="T333" id="Seg_3925" s="T332">be.upset-EP-RECP/COLL-CVB.SIM-be.upset-EP-RECP/COLL-CVB.SIM</ta>
            <ta e="T334" id="Seg_3926" s="T333">clothes-3SG-ACC</ta>
            <ta e="T335" id="Seg_3927" s="T334">flared.breeches</ta>
            <ta e="T336" id="Seg_3928" s="T335">trousers-3SG-ACC</ta>
            <ta e="T337" id="Seg_3929" s="T336">faeces-DAT/LOC</ta>
            <ta e="T338" id="Seg_3930" s="T337">make.dirty-EP-PASS/REFL-EP-PTCP.PST-3SG-ACC</ta>
            <ta e="T339" id="Seg_3931" s="T338">hand-PL-3SG-INSTR</ta>
            <ta e="T340" id="Seg_3932" s="T339">wipe-CVB.SIM</ta>
            <ta e="T341" id="Seg_3933" s="T340">stand-PST2-3SG</ta>
            <ta e="T342" id="Seg_3934" s="T341">Opuo.[NOM]</ta>
            <ta e="T343" id="Seg_3935" s="T342">kolkhoz.[NOM]</ta>
            <ta e="T344" id="Seg_3936" s="T343">sovkhoz-3SG.[NOM]</ta>
            <ta e="T345" id="Seg_3937" s="T344">work-3SG-DAT/LOC</ta>
            <ta e="T346" id="Seg_3938" s="T345">work-3SG-DAT/LOC</ta>
            <ta e="T347" id="Seg_3939" s="T346">house.[NOM]</ta>
            <ta e="T348" id="Seg_3940" s="T347">place.beneath-INSTR</ta>
            <ta e="T349" id="Seg_3941" s="T348">go-CVB.SEQ</ta>
            <ta e="T350" id="Seg_3942" s="T349">go-CVB.SEQ</ta>
            <ta e="T351" id="Seg_3943" s="T350">entryway.[NOM]</ta>
            <ta e="T352" id="Seg_3944" s="T351">cow-PL.[NOM]</ta>
            <ta e="T353" id="Seg_3945" s="T352">go.in-PTCP.PST-3PL-ACC</ta>
            <ta e="T354" id="Seg_3946" s="T353">see-PST2.[3SG]</ta>
            <ta e="T355" id="Seg_3947" s="T354">then</ta>
            <ta e="T356" id="Seg_3948" s="T355">every-3PL-ACC</ta>
            <ta e="T357" id="Seg_3949" s="T356">chase-CVB.SEQ</ta>
            <ta e="T358" id="Seg_3950" s="T357">take.out-PST2-3SG</ta>
            <ta e="T359" id="Seg_3951" s="T358">that.[NOM]</ta>
            <ta e="T360" id="Seg_3952" s="T359">back-3SG-ABL</ta>
            <ta e="T361" id="Seg_3953" s="T360">go.in-CVB.SEQ</ta>
            <ta e="T362" id="Seg_3954" s="T361">go-CVB.SEQ</ta>
            <ta e="T363" id="Seg_3955" s="T362">slip-CVB.SEQ</ta>
            <ta e="T364" id="Seg_3956" s="T363">fall-CVB.SEQ</ta>
            <ta e="T365" id="Seg_3957" s="T364">cow-PL.[NOM]</ta>
            <ta e="T366" id="Seg_3958" s="T365">faeces-3PL-DAT/LOC</ta>
            <ta e="T367" id="Seg_3959" s="T366">enough</ta>
            <ta e="T368" id="Seg_3960" s="T367">make.dirty-EP-PASS/REFL-EP-PST2-3SG</ta>
            <ta e="T369" id="Seg_3961" s="T368">stand.up-CVB.SIM</ta>
            <ta e="T370" id="Seg_3962" s="T369">try-CVB.SEQ</ta>
            <ta e="T371" id="Seg_3963" s="T370">hand-PL-3SG-ACC</ta>
            <ta e="T372" id="Seg_3964" s="T371">also</ta>
            <ta e="T373" id="Seg_3965" s="T372">very</ta>
            <ta e="T374" id="Seg_3966" s="T373">make.dirty-PST2-3SG</ta>
            <ta e="T375" id="Seg_3967" s="T374">Mika-ACC</ta>
            <ta e="T376" id="Seg_3968" s="T375">with</ta>
            <ta e="T377" id="Seg_3969" s="T376">Mukulaj.[NOM]</ta>
            <ta e="T378" id="Seg_3970" s="T377">get.angry-PTCP.PST</ta>
            <ta e="T379" id="Seg_3971" s="T378">human.being.[NOM]</ta>
            <ta e="T380" id="Seg_3972" s="T379">come-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T381" id="Seg_3973" s="T380">big-ADVZ</ta>
            <ta e="T382" id="Seg_3974" s="T381">be.happy-PST2-3PL</ta>
            <ta e="T383" id="Seg_3975" s="T382">Mika.[NOM]</ta>
            <ta e="T384" id="Seg_3976" s="T383">know-NEG.PTCP</ta>
            <ta e="T385" id="Seg_3977" s="T384">human.being-3SG-ABL</ta>
            <ta e="T386" id="Seg_3978" s="T385">be.afraid-CVB.SEQ</ta>
            <ta e="T387" id="Seg_3979" s="T386">eye-3SG-ACC</ta>
            <ta e="T388" id="Seg_3980" s="T387">hide-CVB.SIM-hide-CVB.SIM</ta>
            <ta e="T389" id="Seg_3981" s="T388">say-PST2-3SG</ta>
            <ta e="T390" id="Seg_3982" s="T389">1PL-ACC</ta>
            <ta e="T391" id="Seg_3983" s="T390">at.night</ta>
            <ta e="T392" id="Seg_3984" s="T391">scare-PST2-3SG</ta>
            <ta e="T393" id="Seg_3985" s="T392">make.it-CVB.SEQ</ta>
            <ta e="T394" id="Seg_3986" s="T393">sleep-NEG-PST1-1PL</ta>
            <ta e="T395" id="Seg_3987" s="T394">night-ACC</ta>
            <ta e="T396" id="Seg_3988" s="T395">through</ta>
            <ta e="T397" id="Seg_3989" s="T396">what.[NOM]</ta>
            <ta e="T398" id="Seg_3990" s="T397">be-PTCP.PST-3SG-ACC</ta>
            <ta e="T399" id="Seg_3991" s="T398">whole-3SG-ACC</ta>
            <ta e="T400" id="Seg_3992" s="T399">tell-PST2-3SG</ta>
            <ta e="T401" id="Seg_3993" s="T400">Opuo.[NOM]</ta>
            <ta e="T402" id="Seg_3994" s="T401">child-PL.[NOM]</ta>
            <ta e="T403" id="Seg_3995" s="T402">be.afraid-PTCP.PST</ta>
            <ta e="T404" id="Seg_3996" s="T403">shape-3PL-ACC</ta>
            <ta e="T405" id="Seg_3997" s="T404">see-CVB.SEQ</ta>
            <ta e="T406" id="Seg_3998" s="T405">3PL-ACC</ta>
            <ta e="T407" id="Seg_3999" s="T406">feel.sorry-PST2-3SG</ta>
            <ta e="T408" id="Seg_4000" s="T407">get.angry-PTCP.PST-3SG.[NOM]</ta>
            <ta e="T409" id="Seg_4001" s="T408">immediately</ta>
            <ta e="T410" id="Seg_4002" s="T409">pass.by-PST2-3SG</ta>
            <ta e="T411" id="Seg_4003" s="T410">laugh-CVB.SIM-laugh-CVB.SIM</ta>
            <ta e="T412" id="Seg_4004" s="T411">unfortunate-PL-3SG-DAT/LOC</ta>
            <ta e="T413" id="Seg_4005" s="T412">speak-PST2-3SG</ta>
            <ta e="T414" id="Seg_4006" s="T413">at.night</ta>
            <ta e="T415" id="Seg_4007" s="T414">2PL-ACC</ta>
            <ta e="T416" id="Seg_4008" s="T415">cow-PL-ACC</ta>
            <ta e="T417" id="Seg_4009" s="T416">with</ta>
            <ta e="T418" id="Seg_4010" s="T417">mouse-PL.[NOM]</ta>
            <ta e="T419" id="Seg_4011" s="T418">scare-PST2-3PL</ta>
            <ta e="T420" id="Seg_4012" s="T419">feel.ashamed-NEG-2PL</ta>
            <ta e="T421" id="Seg_4013" s="T420">Q</ta>
            <ta e="T422" id="Seg_4014" s="T421">use-POSS</ta>
            <ta e="T423" id="Seg_4015" s="T422">NEG-ABL</ta>
            <ta e="T424" id="Seg_4016" s="T423">be.afraid-PTCP.PST-2PL-ACC</ta>
            <ta e="T425" id="Seg_4017" s="T424">know-CVB.SEQ-2PL</ta>
            <ta e="T426" id="Seg_4018" s="T425">2PL.[NOM]</ta>
            <ta e="T427" id="Seg_4019" s="T426">bad-2PL-ABL</ta>
            <ta e="T428" id="Seg_4020" s="T427">1SG.[NOM]</ta>
            <ta e="T431" id="Seg_4021" s="T430">1SG.[NOM]</ta>
            <ta e="T432" id="Seg_4022" s="T431">very</ta>
            <ta e="T433" id="Seg_4023" s="T432">make.dirty-EP-PASS/REFL-PST1-1SG</ta>
            <ta e="T434" id="Seg_4024" s="T433">mother-2PL.[NOM]</ta>
            <ta e="T435" id="Seg_4025" s="T434">father-2PL.[NOM]</ta>
            <ta e="T436" id="Seg_4026" s="T435">come-PTCP.FUT-3PL-DAT/LOC</ta>
            <ta e="T437" id="Seg_4027" s="T436">until</ta>
            <ta e="T438" id="Seg_4028" s="T437">entryway-2PL-ACC</ta>
            <ta e="T439" id="Seg_4029" s="T438">with</ta>
            <ta e="T440" id="Seg_4030" s="T439">house-2PL.[NOM]</ta>
            <ta e="T441" id="Seg_4031" s="T440">for</ta>
            <ta e="T442" id="Seg_4032" s="T441">good-ADVZ</ta>
            <ta e="T443" id="Seg_4033" s="T442">care.about-EP-IMP.2PL</ta>
            <ta e="T444" id="Seg_4034" s="T443">Opuo.[NOM]</ta>
            <ta e="T445" id="Seg_4035" s="T444">go.out-CVB.PURP</ta>
            <ta e="T446" id="Seg_4036" s="T445">stand.up-CVB.SEQ</ta>
            <ta e="T447" id="Seg_4037" s="T446">child-PL-ACC</ta>
            <ta e="T448" id="Seg_4038" s="T447">demand-PST2-3SG</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_4039" s="T0">erschrecken-PTCP.PST-3SG.[NOM]</ta>
            <ta e="T2" id="Seg_4040" s="T1">zwei</ta>
            <ta e="T3" id="Seg_4041" s="T2">Junge.[NOM]</ta>
            <ta e="T4" id="Seg_4042" s="T3">Kind-PL.[NOM]</ta>
            <ta e="T5" id="Seg_4043" s="T4">Mika-ACC</ta>
            <ta e="T6" id="Seg_4044" s="T5">mit</ta>
            <ta e="T7" id="Seg_4045" s="T6">Mukulaj.[NOM]</ta>
            <ta e="T8" id="Seg_4046" s="T7">Eltern-3PL.[NOM]</ta>
            <ta e="T9" id="Seg_4047" s="T8">gehen-PTCP.PST-3PL-ACC</ta>
            <ta e="T10" id="Seg_4048" s="T9">nachdem</ta>
            <ta e="T11" id="Seg_4049" s="T10">Haus-3PL-DAT/LOC</ta>
            <ta e="T12" id="Seg_4050" s="T11">übernachten-PST2-3PL</ta>
            <ta e="T13" id="Seg_4051" s="T12">3PL.[NOM]</ta>
            <ta e="T14" id="Seg_4052" s="T13">lernen-PTCP.PRS</ta>
            <ta e="T15" id="Seg_4053" s="T14">Zeit-3PL-DAT/LOC</ta>
            <ta e="T16" id="Seg_4054" s="T15">Winter-ACC</ta>
            <ta e="T17" id="Seg_4055" s="T16">durch</ta>
            <ta e="T18" id="Seg_4056" s="T17">Internat-DAT/LOC</ta>
            <ta e="T19" id="Seg_4057" s="T18">sein-HAB-3PL</ta>
            <ta e="T20" id="Seg_4058" s="T19">vor.langer.Zeit-ADJZ</ta>
            <ta e="T21" id="Seg_4059" s="T20">russisch</ta>
            <ta e="T22" id="Seg_4060" s="T21">Haus.[NOM]</ta>
            <ta e="T23" id="Seg_4061" s="T22">Wand-3SG-GEN</ta>
            <ta e="T24" id="Seg_4062" s="T23">Holz-PL-3SG.[NOM]</ta>
            <ta e="T25" id="Seg_4063" s="T24">letzter-3SG.[NOM]</ta>
            <ta e="T26" id="Seg_4064" s="T25">sein-CVB.SEQ-3PL</ta>
            <ta e="T27" id="Seg_4065" s="T26">sehr</ta>
            <ta e="T30" id="Seg_4066" s="T29">vermodern-PST2-3PL</ta>
            <ta e="T31" id="Seg_4067" s="T30">nach.draußen-ADJZ</ta>
            <ta e="T32" id="Seg_4068" s="T31">Diele-3SG-GEN</ta>
            <ta e="T33" id="Seg_4069" s="T32">Tür-3SG.[NOM]</ta>
            <ta e="T34" id="Seg_4070" s="T33">Mensch.[NOM]</ta>
            <ta e="T35" id="Seg_4071" s="T34">öffnen-TEMP-3SG</ta>
            <ta e="T36" id="Seg_4072" s="T35">knarren-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T37" id="Seg_4073" s="T36">sehr</ta>
            <ta e="T38" id="Seg_4074" s="T37">Haus.[NOM]</ta>
            <ta e="T39" id="Seg_4075" s="T38">Inneres-3SG-DAT/LOC</ta>
            <ta e="T40" id="Seg_4076" s="T39">wie.viel</ta>
            <ta e="T41" id="Seg_4077" s="T40">EMPH</ta>
            <ta e="T42" id="Seg_4078" s="T41">Lärm.[NOM]</ta>
            <ta e="T43" id="Seg_4079" s="T42">sein-IMP.3SG</ta>
            <ta e="T44" id="Seg_4080" s="T43">wer.[NOM]</ta>
            <ta e="T45" id="Seg_4081" s="T44">INDEF</ta>
            <ta e="T46" id="Seg_4082" s="T45">öffnen-TEMP-3SG</ta>
            <ta e="T47" id="Seg_4083" s="T46">knarren-PTCP.PRS</ta>
            <ta e="T48" id="Seg_4084" s="T47">Geräusch-3SG.[NOM]</ta>
            <ta e="T49" id="Seg_4085" s="T48">sofort</ta>
            <ta e="T50" id="Seg_4086" s="T49">gehört.werden-PRS.[3SG]</ta>
            <ta e="T51" id="Seg_4087" s="T50">mancher.[NOM]</ta>
            <ta e="T52" id="Seg_4088" s="T51">als</ta>
            <ta e="T53" id="Seg_4089" s="T52">Wind.[NOM]</ta>
            <ta e="T54" id="Seg_4090" s="T53">schlagen-CVB.SEQ</ta>
            <ta e="T55" id="Seg_4091" s="T54">öffnen-TEMP-3SG</ta>
            <ta e="T56" id="Seg_4092" s="T55">knarren-CVB.SEQ</ta>
            <ta e="T57" id="Seg_4093" s="T56">Haus-PROPR-PL-ACC</ta>
            <ta e="T58" id="Seg_4094" s="T57">leben.[CAUS]-NEG.[3SG]</ta>
            <ta e="T59" id="Seg_4095" s="T58">NEG</ta>
            <ta e="T60" id="Seg_4096" s="T59">schlafen-CAUS-NEG.[3SG]</ta>
            <ta e="T61" id="Seg_4097" s="T60">NEG</ta>
            <ta e="T62" id="Seg_4098" s="T61">Rajon.[NOM]</ta>
            <ta e="T63" id="Seg_4099" s="T62">Zentrum</ta>
            <ta e="T64" id="Seg_4100" s="T63">Siedlung-3SG-DAT/LOC</ta>
            <ta e="T65" id="Seg_4101" s="T64">solch</ta>
            <ta e="T66" id="Seg_4102" s="T65">sehr</ta>
            <ta e="T67" id="Seg_4103" s="T66">alt</ta>
            <ta e="T68" id="Seg_4104" s="T67">Haus.[NOM]</ta>
            <ta e="T69" id="Seg_4105" s="T68">NEG.EX</ta>
            <ta e="T70" id="Seg_4106" s="T69">Haus-PROPR-PL.[NOM]</ta>
            <ta e="T71" id="Seg_4107" s="T70">bald</ta>
            <ta e="T72" id="Seg_4108" s="T71">neu</ta>
            <ta e="T73" id="Seg_4109" s="T72">Haus-DAT/LOC</ta>
            <ta e="T74" id="Seg_4110" s="T73">holen-EP-REFL-CVB.SEQ</ta>
            <ta e="T75" id="Seg_4111" s="T74">fallen</ta>
            <ta e="T76" id="Seg_4112" s="T75">tragen-EP-REFL-PTCP.FUT</ta>
            <ta e="T77" id="Seg_4113" s="T76">fallen-NEC-3PL</ta>
            <ta e="T78" id="Seg_4114" s="T77">groß</ta>
            <ta e="T79" id="Seg_4115" s="T78">neu</ta>
            <ta e="T80" id="Seg_4116" s="T79">Haus.[NOM]</ta>
            <ta e="T81" id="Seg_4117" s="T80">bauen-EP-PASS/REFL-CVB.SEQ</ta>
            <ta e="T82" id="Seg_4118" s="T81">aufhören-PTCP.PRS-3SG-ACC</ta>
            <ta e="T83" id="Seg_4119" s="T82">warten-CVB.SIM</ta>
            <ta e="T84" id="Seg_4120" s="T83">sitzen-PRS-3PL</ta>
            <ta e="T85" id="Seg_4121" s="T84">jetzt</ta>
            <ta e="T86" id="Seg_4122" s="T85">Kind-PL.[NOM]</ta>
            <ta e="T87" id="Seg_4123" s="T86">Eltern-3PL.[NOM]</ta>
            <ta e="T88" id="Seg_4124" s="T87">nah</ta>
            <ta e="T89" id="Seg_4125" s="T88">Verwandter-3PL-DAT/LOC</ta>
            <ta e="T90" id="Seg_4126" s="T89">besuchen-CVB.SIM</ta>
            <ta e="T91" id="Seg_4127" s="T90">gehen-PST2-3PL</ta>
            <ta e="T92" id="Seg_4128" s="T91">Haus-3PL-DAT/LOC</ta>
            <ta e="T93" id="Seg_4129" s="T92">morgen</ta>
            <ta e="T94" id="Seg_4130" s="T93">Q</ta>
            <ta e="T95" id="Seg_4131" s="T94">übermorgen</ta>
            <ta e="T96" id="Seg_4132" s="T95">Q</ta>
            <ta e="T97" id="Seg_4133" s="T96">kommen-PTCP.FUT</ta>
            <ta e="T98" id="Seg_4134" s="T97">fallen-NEC</ta>
            <ta e="T99" id="Seg_4135" s="T98">sein-PST1-3PL</ta>
            <ta e="T100" id="Seg_4136" s="T99">im.Winter</ta>
            <ta e="T101" id="Seg_4137" s="T100">dunkel</ta>
            <ta e="T102" id="Seg_4138" s="T101">Tag.[NOM]</ta>
            <ta e="T103" id="Seg_4139" s="T102">kurz.[NOM]</ta>
            <ta e="T104" id="Seg_4140" s="T103">Abend-PL-3SG.[NOM]</ta>
            <ta e="T105" id="Seg_4141" s="T104">sehr</ta>
            <ta e="T106" id="Seg_4142" s="T105">früh</ta>
            <ta e="T107" id="Seg_4143" s="T106">Junge.[NOM]</ta>
            <ta e="T108" id="Seg_4144" s="T107">Kind-PL.[NOM]</ta>
            <ta e="T109" id="Seg_4145" s="T108">essen-PTCP.PRS</ta>
            <ta e="T110" id="Seg_4146" s="T109">Nahrung-3PL-ACC</ta>
            <ta e="T111" id="Seg_4147" s="T110">sich.satt.essen-PTCP.FUT-3PL-DAT/LOC</ta>
            <ta e="T112" id="Seg_4148" s="T111">bis.zu</ta>
            <ta e="T113" id="Seg_4149" s="T112">essen-CVB.SEQ</ta>
            <ta e="T114" id="Seg_4150" s="T113">nachdem</ta>
            <ta e="T115" id="Seg_4151" s="T114">schlafen-CVB.PURP</ta>
            <ta e="T116" id="Seg_4152" s="T115">vorbereiten-REFL-PST2-3PL</ta>
            <ta e="T117" id="Seg_4153" s="T116">nun</ta>
            <ta e="T118" id="Seg_4154" s="T117">1PL.[NOM]</ta>
            <ta e="T119" id="Seg_4155" s="T118">alt</ta>
            <ta e="T120" id="Seg_4156" s="T119">Haus-1PL.[NOM]</ta>
            <ta e="T121" id="Seg_4157" s="T120">nachts</ta>
            <ta e="T122" id="Seg_4158" s="T121">erschrecken-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T123" id="Seg_4159" s="T122">wahrscheinlich</ta>
            <ta e="T124" id="Seg_4160" s="T123">Mukulaj.[NOM]</ta>
            <ta e="T125" id="Seg_4161" s="T124">älterer.Bruder-3SG-DAT/LOC</ta>
            <ta e="T126" id="Seg_4162" s="T125">sagen-PST2-3SG</ta>
            <ta e="T127" id="Seg_4163" s="T126">2SG.[NOM]</ta>
            <ta e="T128" id="Seg_4164" s="T127">wieder</ta>
            <ta e="T129" id="Seg_4165" s="T128">schlecht-ADVZ</ta>
            <ta e="T130" id="Seg_4166" s="T129">wegwerfen-REFL-CVB.SEQ</ta>
            <ta e="T131" id="Seg_4167" s="T130">sein-PRS-2SG</ta>
            <ta e="T132" id="Seg_4168" s="T131">3SG-ACC</ta>
            <ta e="T133" id="Seg_4169" s="T132">groß-3SG.[NOM]</ta>
            <ta e="T134" id="Seg_4170" s="T133">schelten-PST2-3SG</ta>
            <ta e="T135" id="Seg_4171" s="T134">dafür</ta>
            <ta e="T136" id="Seg_4172" s="T135">nackt-VBZ.[IMP.2SG]</ta>
            <ta e="T137" id="Seg_4173" s="T136">dort</ta>
            <ta e="T138" id="Seg_4174" s="T137">Bett-DAT/LOC</ta>
            <ta e="T139" id="Seg_4175" s="T138">sich.hinlegen.[IMP.2SG]</ta>
            <ta e="T140" id="Seg_4176" s="T139">zusammen</ta>
            <ta e="T141" id="Seg_4177" s="T140">schlafen-FUT-1PL</ta>
            <ta e="T142" id="Seg_4178" s="T141">älterer.Bruder-3SG.[NOM]</ta>
            <ta e="T143" id="Seg_4179" s="T142">Inneres-3SG-DAT/LOC</ta>
            <ta e="T144" id="Seg_4180" s="T143">doch</ta>
            <ta e="T145" id="Seg_4181" s="T144">EMPH</ta>
            <ta e="T146" id="Seg_4182" s="T145">aufschrecken-EP-MED-PRS.[3SG]</ta>
            <ta e="T147" id="Seg_4183" s="T146">offenbar</ta>
            <ta e="T148" id="Seg_4184" s="T147">3PL.[NOM]</ta>
            <ta e="T149" id="Seg_4185" s="T148">leer</ta>
            <ta e="T150" id="Seg_4186" s="T149">Haus-DAT/LOC</ta>
            <ta e="T151" id="Seg_4187" s="T150">Eltern-POSS</ta>
            <ta e="T152" id="Seg_4188" s="T151">NEG</ta>
            <ta e="T153" id="Seg_4189" s="T152">erstmals</ta>
            <ta e="T154" id="Seg_4190" s="T153">übernachten-PRS-3PL</ta>
            <ta e="T155" id="Seg_4191" s="T154">älterer.Bruder-3SG.[NOM]</ta>
            <ta e="T156" id="Seg_4192" s="T155">Lampe-3SG-ACC</ta>
            <ta e="T157" id="Seg_4193" s="T156">ausmachen-CVB.ANT</ta>
            <ta e="T158" id="Seg_4194" s="T157">jüngerer.Bruder-EP-3SG-GEN</ta>
            <ta e="T159" id="Seg_4195" s="T158">Reihe-3SG-INSTR</ta>
            <ta e="T160" id="Seg_4196" s="T159">Bettdecke.[NOM]</ta>
            <ta e="T161" id="Seg_4197" s="T160">Inneres-3SG-DAT/LOC</ta>
            <ta e="T162" id="Seg_4198" s="T161">hineingehen-CVB.SEQ</ta>
            <ta e="T163" id="Seg_4199" s="T162">sich.hinlegen-PST2-3SG</ta>
            <ta e="T164" id="Seg_4200" s="T163">Kind-PL.[NOM]</ta>
            <ta e="T165" id="Seg_4201" s="T164">Bettdecke-3PL-GEN</ta>
            <ta e="T166" id="Seg_4202" s="T165">Inneres-3SG-DAT/LOC</ta>
            <ta e="T167" id="Seg_4203" s="T166">atmen-NEG.CVB.SIM</ta>
            <ta e="T168" id="Seg_4204" s="T167">liegen-PST2-3PL</ta>
            <ta e="T169" id="Seg_4205" s="T168">Angst.haben-CVB.SIM</ta>
            <ta e="T170" id="Seg_4206" s="T169">denken-CVB.SEQ</ta>
            <ta e="T171" id="Seg_4207" s="T170">zuhören-PRS-3PL</ta>
            <ta e="T172" id="Seg_4208" s="T171">bald-INTNS</ta>
            <ta e="T173" id="Seg_4209" s="T172">Auge-3PL-ACC</ta>
            <ta e="T174" id="Seg_4210" s="T173">Augen.schließen-CVB.SEQ</ta>
            <ta e="T175" id="Seg_4211" s="T174">einschlafen-CVB.SEQ</ta>
            <ta e="T176" id="Seg_4212" s="T175">schnarchen-CVB.SEQ</ta>
            <ta e="T177" id="Seg_4213" s="T176">bleiben-PST2-3PL</ta>
            <ta e="T178" id="Seg_4214" s="T177">wie.viel</ta>
            <ta e="T179" id="Seg_4215" s="T178">wie.viel</ta>
            <ta e="T180" id="Seg_4216" s="T179">sein-PST2-3SG</ta>
            <ta e="T181" id="Seg_4217" s="T180">Q</ta>
            <ta e="T182" id="Seg_4218" s="T181">Mika.[NOM]</ta>
            <ta e="T183" id="Seg_4219" s="T182">jüngerer.Bruder-EP-3SG.[NOM]</ta>
            <ta e="T184" id="Seg_4220" s="T183">Seite-3SG-DAT/LOC</ta>
            <ta e="T185" id="Seg_4221" s="T184">stoßen-FREQ-PTCP.PRS-3SG-ABL</ta>
            <ta e="T186" id="Seg_4222" s="T185">aufwachen-EP-PST2-3SG</ta>
            <ta e="T187" id="Seg_4223" s="T186">älterer.Bruder.[NOM]</ta>
            <ta e="T188" id="Seg_4224" s="T187">zuhören.[IMP.2SG]</ta>
            <ta e="T189" id="Seg_4225" s="T188">erschrecken-PTCP.PRS.[NOM]</ta>
            <ta e="T190" id="Seg_4226" s="T189">offenbar</ta>
            <ta e="T191" id="Seg_4227" s="T190">Mukulaj.[NOM]</ta>
            <ta e="T192" id="Seg_4228" s="T191">Angst.haben-CVB.SEQ</ta>
            <ta e="T193" id="Seg_4229" s="T192">Wort-3SG.[NOM]</ta>
            <ta e="T194" id="Seg_4230" s="T193">kaum</ta>
            <ta e="T195" id="Seg_4231" s="T194">hinausgehen-EP-PST2-3SG</ta>
            <ta e="T196" id="Seg_4232" s="T195">Mika.[NOM]</ta>
            <ta e="T197" id="Seg_4233" s="T196">Bettdecke-3SG-GEN</ta>
            <ta e="T198" id="Seg_4234" s="T197">Rand-3SG-ACC</ta>
            <ta e="T199" id="Seg_4235" s="T198">heben-CVB.SEQ</ta>
            <ta e="T200" id="Seg_4236" s="T199">zuhören-PST2-3SG</ta>
            <ta e="T201" id="Seg_4237" s="T200">Wahrheit.[NOM]</ta>
            <ta e="T202" id="Seg_4238" s="T201">essen-PTCP.PRS</ta>
            <ta e="T203" id="Seg_4239" s="T202">Tisch-3PL-DAT/LOC</ta>
            <ta e="T204" id="Seg_4240" s="T203">Teller-PL.[NOM]</ta>
            <ta e="T205" id="Seg_4241" s="T204">Löffel-PL.[NOM]</ta>
            <ta e="T206" id="Seg_4242" s="T205">klappern-PRS-3PL</ta>
            <ta e="T207" id="Seg_4243" s="T206">EMPH-dunkel.[NOM]</ta>
            <ta e="T208" id="Seg_4244" s="T207">was.[NOM]</ta>
            <ta e="T209" id="Seg_4245" s="T208">NEG</ta>
            <ta e="T210" id="Seg_4246" s="T209">zu.sehen.sein-EP-NEG.[3SG]</ta>
            <ta e="T211" id="Seg_4247" s="T210">na</ta>
            <ta e="T212" id="Seg_4248" s="T211">weit.weg</ta>
            <ta e="T213" id="Seg_4249" s="T212">stehen-PTCP.PRS</ta>
            <ta e="T214" id="Seg_4250" s="T213">Tisch.[NOM]</ta>
            <ta e="T215" id="Seg_4251" s="T214">oberer.Teil-3SG-DAT/LOC</ta>
            <ta e="T216" id="Seg_4252" s="T215">Papiere-PL-ACC</ta>
            <ta e="T217" id="Seg_4253" s="T216">was.[NOM]</ta>
            <ta e="T218" id="Seg_4254" s="T217">INDEF</ta>
            <ta e="T220" id="Seg_4255" s="T219">zerreißen-FREQ-PRS.[3SG]</ta>
            <ta e="T221" id="Seg_4256" s="T220">Kind-PL.[NOM]</ta>
            <ta e="T222" id="Seg_4257" s="T221">Bettdecke.[NOM]</ta>
            <ta e="T223" id="Seg_4258" s="T222">Inneres-3SG-DAT/LOC</ta>
            <ta e="T224" id="Seg_4259" s="T223">sich.umarmen-CVB.SEQ</ta>
            <ta e="T225" id="Seg_4260" s="T224">nachdem</ta>
            <ta e="T226" id="Seg_4261" s="T225">Angst.haben-CVB.SEQ</ta>
            <ta e="T227" id="Seg_4262" s="T226">atmen-NEG.CVB.SIM</ta>
            <ta e="T228" id="Seg_4263" s="T227">liegen-PRS-3PL</ta>
            <ta e="T229" id="Seg_4264" s="T228">draußen</ta>
            <ta e="T230" id="Seg_4265" s="T229">Diele-DAT/LOC</ta>
            <ta e="T231" id="Seg_4266" s="T230">was.[NOM]</ta>
            <ta e="T232" id="Seg_4267" s="T231">INDEF</ta>
            <ta e="T233" id="Seg_4268" s="T232">go-FREQ-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T234" id="Seg_4269" s="T233">gehört.werden-EP-PST2-3SG</ta>
            <ta e="T235" id="Seg_4270" s="T234">Haus.[NOM]</ta>
            <ta e="T236" id="Seg_4271" s="T235">Wand-3SG-ACC</ta>
            <ta e="T237" id="Seg_4272" s="T236">laut-ADVZ</ta>
            <ta e="T238" id="Seg_4273" s="T237">schlagen-FREQ-PRS.[3SG]</ta>
            <ta e="T239" id="Seg_4274" s="T238">Regal-PL-ABL</ta>
            <ta e="T240" id="Seg_4275" s="T239">Kessel-PL-ACC</ta>
            <ta e="T241" id="Seg_4276" s="T240">verschieden</ta>
            <ta e="T242" id="Seg_4277" s="T241">Gefäß-PL-ACC</ta>
            <ta e="T243" id="Seg_4278" s="T242">Erde-DAT/LOC</ta>
            <ta e="T244" id="Seg_4279" s="T243">fallen.lassen-ITER-PRS.[3SG]</ta>
            <ta e="T245" id="Seg_4280" s="T244">dann</ta>
            <ta e="T246" id="Seg_4281" s="T245">jenes-PL-ACC</ta>
            <ta e="T247" id="Seg_4282" s="T246">treten-FREQ-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T248" id="Seg_4283" s="T247">gehört.werden-PRS.[3SG]</ta>
            <ta e="T249" id="Seg_4284" s="T248">Ruhe</ta>
            <ta e="T250" id="Seg_4285" s="T249">sich.bewegen-NEG.CVB.SIM</ta>
            <ta e="T251" id="Seg_4286" s="T250">liegen.[IMP.2SG]</ta>
            <ta e="T252" id="Seg_4287" s="T251">Mika.[NOM]</ta>
            <ta e="T253" id="Seg_4288" s="T252">jüngerer.Bruder-EP-3SG-ACC</ta>
            <ta e="T254" id="Seg_4289" s="T253">schelten-PST2-3SG</ta>
            <ta e="T255" id="Seg_4290" s="T254">Haus.[NOM]</ta>
            <ta e="T256" id="Seg_4291" s="T255">Inneres-3SG-DAT/LOC</ta>
            <ta e="T257" id="Seg_4292" s="T256">Diele-DAT/LOC</ta>
            <ta e="T258" id="Seg_4293" s="T257">Lärm.[NOM]</ta>
            <ta e="T259" id="Seg_4294" s="T258">wann</ta>
            <ta e="T260" id="Seg_4295" s="T259">NEG</ta>
            <ta e="T261" id="Seg_4296" s="T260">aufhören-NEG.[3SG]</ta>
            <ta e="T262" id="Seg_4297" s="T261">Morgen-DAT/LOC</ta>
            <ta e="T263" id="Seg_4298" s="T262">bis.zu</ta>
            <ta e="T264" id="Seg_4299" s="T263">noch</ta>
            <ta e="T265" id="Seg_4300" s="T264">fern.[NOM]</ta>
            <ta e="T266" id="Seg_4301" s="T265">3PL-ACC</ta>
            <ta e="T267" id="Seg_4302" s="T266">wer.[NOM]</ta>
            <ta e="T268" id="Seg_4303" s="T267">kommen-CVB.SEQ</ta>
            <ta e="T269" id="Seg_4304" s="T268">retten-FUT.[3SG]=Q</ta>
            <ta e="T270" id="Seg_4305" s="T269">Kind-PL.[NOM]</ta>
            <ta e="T271" id="Seg_4306" s="T270">vor.Schreck.erstarren-CVB.SEQ</ta>
            <ta e="T272" id="Seg_4307" s="T271">weinen-PTCP.FUT-3PL-ACC</ta>
            <ta e="T273" id="Seg_4308" s="T272">NEG</ta>
            <ta e="T274" id="Seg_4309" s="T273">schreien-PTCP.FUT-3PL-ACC</ta>
            <ta e="T275" id="Seg_4310" s="T274">NEG</ta>
            <ta e="T276" id="Seg_4311" s="T275">sehr</ta>
            <ta e="T277" id="Seg_4312" s="T276">jenes</ta>
            <ta e="T278" id="Seg_4313" s="T277">Angst.haben-CVB.SIM</ta>
            <ta e="T279" id="Seg_4314" s="T278">liegen-CVB.SEQ-3PL</ta>
            <ta e="T280" id="Seg_4315" s="T279">Schlaf-3PL-DAT/LOC</ta>
            <ta e="T281" id="Seg_4316" s="T280">herunterziehen-EP-CAUS-CVB.SEQ</ta>
            <ta e="T282" id="Seg_4317" s="T281">schlafen-CVB.SEQ</ta>
            <ta e="T283" id="Seg_4318" s="T282">bleiben-PST2-3PL</ta>
            <ta e="T284" id="Seg_4319" s="T283">was-COMP</ta>
            <ta e="T285" id="Seg_4320" s="T284">NEG</ta>
            <ta e="T286" id="Seg_4321" s="T285">groß</ta>
            <ta e="T287" id="Seg_4322" s="T286">Lärm-ABL</ta>
            <ta e="T288" id="Seg_4323" s="T287">unerwartet</ta>
            <ta e="T289" id="Seg_4324" s="T288">aufwachen-EP-PST2-3PL</ta>
            <ta e="T290" id="Seg_4325" s="T289">Diele-DAT/LOC</ta>
            <ta e="T291" id="Seg_4326" s="T290">was.[NOM]</ta>
            <ta e="T292" id="Seg_4327" s="T291">INDEF</ta>
            <ta e="T293" id="Seg_4328" s="T292">hart-ADVZ</ta>
            <ta e="T294" id="Seg_4329" s="T293">Stein-SIM</ta>
            <ta e="T295" id="Seg_4330" s="T294">krachen-CVB.SEQ</ta>
            <ta e="T296" id="Seg_4331" s="T295">fallen-PST2-3SG</ta>
            <ta e="T297" id="Seg_4332" s="T296">Schrei.[NOM]</ta>
            <ta e="T298" id="Seg_4333" s="T297">Schrei.[NOM]</ta>
            <ta e="T299" id="Seg_4334" s="T298">gehört.werden-EP-PST2-3SG</ta>
            <ta e="T300" id="Seg_4335" s="T299">Tür.[NOM]</ta>
            <ta e="T301" id="Seg_4336" s="T300">öffnen-PASS/REFL-EP-PST2-3SG</ta>
            <ta e="T302" id="Seg_4337" s="T301">Haus-DAT/LOC</ta>
            <ta e="T303" id="Seg_4338" s="T302">wer.[NOM]</ta>
            <ta e="T304" id="Seg_4339" s="T303">INDEF</ta>
            <ta e="T305" id="Seg_4340" s="T304">hineingehen-CVB.SEQ</ta>
            <ta e="T306" id="Seg_4341" s="T305">Lampe-ACC</ta>
            <ta e="T307" id="Seg_4342" s="T306">anzünden-PST2-3SG</ta>
            <ta e="T308" id="Seg_4343" s="T307">aufstehen-EP-IMP.2PL</ta>
            <ta e="T309" id="Seg_4344" s="T308">2PL.[NOM]</ta>
            <ta e="T310" id="Seg_4345" s="T309">Diele-2PL.[NOM]</ta>
            <ta e="T311" id="Seg_4346" s="T310">Tür-3SG-ACC</ta>
            <ta e="T312" id="Seg_4347" s="T311">warum</ta>
            <ta e="T313" id="Seg_4348" s="T312">schließen-PST2.NEG-2PL=Q</ta>
            <ta e="T314" id="Seg_4349" s="T313">Kuh-PL.[NOM]</ta>
            <ta e="T315" id="Seg_4350" s="T314">hineingehen-CVB.SEQ-3PL</ta>
            <ta e="T316" id="Seg_4351" s="T315">Erde-ACC</ta>
            <ta e="T317" id="Seg_4352" s="T316">genug</ta>
            <ta e="T318" id="Seg_4353" s="T317">treten-RECP/COLL-EP-PST2-3PL</ta>
            <ta e="T319" id="Seg_4354" s="T318">Diele.[NOM]</ta>
            <ta e="T320" id="Seg_4355" s="T319">Inneres-3SG-ACC</ta>
            <ta e="T321" id="Seg_4356" s="T320">eins</ta>
            <ta e="T322" id="Seg_4357" s="T321">machen-PST2-3PL</ta>
            <ta e="T323" id="Seg_4358" s="T322">Erde-ACC</ta>
            <ta e="T324" id="Seg_4359" s="T323">voll</ta>
            <ta e="T325" id="Seg_4360" s="T324">sich.entleeren-PST2-3PL</ta>
            <ta e="T326" id="Seg_4361" s="T325">Kind-PL.[NOM]</ta>
            <ta e="T327" id="Seg_4362" s="T326">Auge-3PL-ACC</ta>
            <ta e="T328" id="Seg_4363" s="T327">öffnen-CVB.SEQ-3PL</ta>
            <ta e="T329" id="Seg_4364" s="T328">Mensch.[NOM]</ta>
            <ta e="T330" id="Seg_4365" s="T329">stehen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T331" id="Seg_4366" s="T330">sehen-PST2-3PL</ta>
            <ta e="T332" id="Seg_4367" s="T331">jenes-3SG-3PL.[NOM]</ta>
            <ta e="T333" id="Seg_4368" s="T332">sich.ärgern-EP-RECP/COLL-CVB.SIM-sich.ärgern-EP-RECP/COLL-CVB.SIM</ta>
            <ta e="T334" id="Seg_4369" s="T333">Kleidung-3SG-ACC</ta>
            <ta e="T335" id="Seg_4370" s="T334">weite.Kniehosen</ta>
            <ta e="T336" id="Seg_4371" s="T335">Hose-3SG-ACC</ta>
            <ta e="T337" id="Seg_4372" s="T336">Kot-DAT/LOC</ta>
            <ta e="T338" id="Seg_4373" s="T337">vollschmieren-EP-PASS/REFL-EP-PTCP.PST-3SG-ACC</ta>
            <ta e="T339" id="Seg_4374" s="T338">Hand-PL-3SG-INSTR</ta>
            <ta e="T340" id="Seg_4375" s="T339">wischen-CVB.SIM</ta>
            <ta e="T341" id="Seg_4376" s="T340">stehen-PST2-3SG</ta>
            <ta e="T342" id="Seg_4377" s="T341">Opuo.[NOM]</ta>
            <ta e="T343" id="Seg_4378" s="T342">Kolchose.[NOM]</ta>
            <ta e="T344" id="Seg_4379" s="T343">Sowchose-3SG.[NOM]</ta>
            <ta e="T345" id="Seg_4380" s="T344">Arbeit-3SG-DAT/LOC</ta>
            <ta e="T346" id="Seg_4381" s="T345">Arbeit-3SG-DAT/LOC</ta>
            <ta e="T347" id="Seg_4382" s="T346">Haus.[NOM]</ta>
            <ta e="T348" id="Seg_4383" s="T347">Platz.neben-INSTR</ta>
            <ta e="T349" id="Seg_4384" s="T348">gehen-CVB.SEQ</ta>
            <ta e="T350" id="Seg_4385" s="T349">gehen-CVB.SEQ</ta>
            <ta e="T351" id="Seg_4386" s="T350">Diele.[NOM]</ta>
            <ta e="T352" id="Seg_4387" s="T351">Kuh-PL.[NOM]</ta>
            <ta e="T353" id="Seg_4388" s="T352">hineingehen-PTCP.PST-3PL-ACC</ta>
            <ta e="T354" id="Seg_4389" s="T353">sehen-PST2.[3SG]</ta>
            <ta e="T355" id="Seg_4390" s="T354">dann</ta>
            <ta e="T356" id="Seg_4391" s="T355">jeder-3PL-ACC</ta>
            <ta e="T357" id="Seg_4392" s="T356">jagen-CVB.SEQ</ta>
            <ta e="T358" id="Seg_4393" s="T357">herausnehmen-PST2-3SG</ta>
            <ta e="T359" id="Seg_4394" s="T358">jenes.[NOM]</ta>
            <ta e="T360" id="Seg_4395" s="T359">Hinterteil-3SG-ABL</ta>
            <ta e="T361" id="Seg_4396" s="T360">hineingehen-CVB.SEQ</ta>
            <ta e="T362" id="Seg_4397" s="T361">gehen-CVB.SEQ</ta>
            <ta e="T363" id="Seg_4398" s="T362">ausrutschen-CVB.SEQ</ta>
            <ta e="T364" id="Seg_4399" s="T363">fallen-CVB.SEQ</ta>
            <ta e="T365" id="Seg_4400" s="T364">Kuh-PL.[NOM]</ta>
            <ta e="T366" id="Seg_4401" s="T365">Kot-3PL-DAT/LOC</ta>
            <ta e="T367" id="Seg_4402" s="T366">genug</ta>
            <ta e="T368" id="Seg_4403" s="T367">vollschmieren-EP-PASS/REFL-EP-PST2-3SG</ta>
            <ta e="T369" id="Seg_4404" s="T368">aufstehen-CVB.SIM</ta>
            <ta e="T370" id="Seg_4405" s="T369">versuchen-CVB.SEQ</ta>
            <ta e="T371" id="Seg_4406" s="T370">Hand-PL-3SG-ACC</ta>
            <ta e="T372" id="Seg_4407" s="T371">auch</ta>
            <ta e="T373" id="Seg_4408" s="T372">sehr</ta>
            <ta e="T374" id="Seg_4409" s="T373">vollschmieren-PST2-3SG</ta>
            <ta e="T375" id="Seg_4410" s="T374">Mika-ACC</ta>
            <ta e="T376" id="Seg_4411" s="T375">mit</ta>
            <ta e="T377" id="Seg_4412" s="T376">Mukulaj.[NOM]</ta>
            <ta e="T378" id="Seg_4413" s="T377">böse.werden-PTCP.PST</ta>
            <ta e="T379" id="Seg_4414" s="T378">Mensch.[NOM]</ta>
            <ta e="T380" id="Seg_4415" s="T379">kommen-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T381" id="Seg_4416" s="T380">groß-ADVZ</ta>
            <ta e="T382" id="Seg_4417" s="T381">sich.freuen-PST2-3PL</ta>
            <ta e="T383" id="Seg_4418" s="T382">Mika.[NOM]</ta>
            <ta e="T384" id="Seg_4419" s="T383">wissen-NEG.PTCP</ta>
            <ta e="T385" id="Seg_4420" s="T384">Mensch-3SG-ABL</ta>
            <ta e="T386" id="Seg_4421" s="T385">sich.fürchten-CVB.SEQ</ta>
            <ta e="T387" id="Seg_4422" s="T386">Auge-3SG-ACC</ta>
            <ta e="T388" id="Seg_4423" s="T387">verstecken-CVB.SIM-verstecken-CVB.SIM</ta>
            <ta e="T389" id="Seg_4424" s="T388">sagen-PST2-3SG</ta>
            <ta e="T390" id="Seg_4425" s="T389">1PL-ACC</ta>
            <ta e="T391" id="Seg_4426" s="T390">nachts</ta>
            <ta e="T392" id="Seg_4427" s="T391">erschrecken-PST2-3SG</ta>
            <ta e="T393" id="Seg_4428" s="T392">schaffen-CVB.SEQ</ta>
            <ta e="T394" id="Seg_4429" s="T393">schlafen-NEG-PST1-1PL</ta>
            <ta e="T395" id="Seg_4430" s="T394">Nacht-ACC</ta>
            <ta e="T396" id="Seg_4431" s="T395">durch</ta>
            <ta e="T397" id="Seg_4432" s="T396">was.[NOM]</ta>
            <ta e="T398" id="Seg_4433" s="T397">sein-PTCP.PST-3SG-ACC</ta>
            <ta e="T399" id="Seg_4434" s="T398">ganz-3SG-ACC</ta>
            <ta e="T400" id="Seg_4435" s="T399">erzählen-PST2-3SG</ta>
            <ta e="T401" id="Seg_4436" s="T400">Opuo.[NOM]</ta>
            <ta e="T402" id="Seg_4437" s="T401">Kind-PL.[NOM]</ta>
            <ta e="T403" id="Seg_4438" s="T402">Angst.haben-PTCP.PST</ta>
            <ta e="T404" id="Seg_4439" s="T403">Gestalt-3PL-ACC</ta>
            <ta e="T405" id="Seg_4440" s="T404">sehen-CVB.SEQ</ta>
            <ta e="T406" id="Seg_4441" s="T405">3PL-ACC</ta>
            <ta e="T407" id="Seg_4442" s="T406">bemitleiden-PST2-3SG</ta>
            <ta e="T408" id="Seg_4443" s="T407">böse.werden-PTCP.PST-3SG.[NOM]</ta>
            <ta e="T409" id="Seg_4444" s="T408">sofort</ta>
            <ta e="T410" id="Seg_4445" s="T409">vorbeigehen-PST2-3SG</ta>
            <ta e="T411" id="Seg_4446" s="T410">lachen-CVB.SIM-lachen-CVB.SIM</ta>
            <ta e="T412" id="Seg_4447" s="T411">unglückselig-PL-3SG-DAT/LOC</ta>
            <ta e="T413" id="Seg_4448" s="T412">sprechen-PST2-3SG</ta>
            <ta e="T414" id="Seg_4449" s="T413">nachts</ta>
            <ta e="T415" id="Seg_4450" s="T414">2PL-ACC</ta>
            <ta e="T416" id="Seg_4451" s="T415">Kuh-PL-ACC</ta>
            <ta e="T417" id="Seg_4452" s="T416">mit</ta>
            <ta e="T418" id="Seg_4453" s="T417">Maus-PL.[NOM]</ta>
            <ta e="T419" id="Seg_4454" s="T418">erschrecken-PST2-3PL</ta>
            <ta e="T420" id="Seg_4455" s="T419">sich.schämen-NEG-2PL</ta>
            <ta e="T421" id="Seg_4456" s="T420">Q</ta>
            <ta e="T422" id="Seg_4457" s="T421">Nutzen-POSS</ta>
            <ta e="T423" id="Seg_4458" s="T422">NEG-ABL</ta>
            <ta e="T424" id="Seg_4459" s="T423">Angst.haben-PTCP.PST-2PL-ACC</ta>
            <ta e="T425" id="Seg_4460" s="T424">wissen-CVB.SEQ-2PL</ta>
            <ta e="T426" id="Seg_4461" s="T425">2PL.[NOM]</ta>
            <ta e="T427" id="Seg_4462" s="T426">schlecht-2PL-ABL</ta>
            <ta e="T428" id="Seg_4463" s="T427">1SG.[NOM]</ta>
            <ta e="T431" id="Seg_4464" s="T430">1SG.[NOM]</ta>
            <ta e="T432" id="Seg_4465" s="T431">sehr</ta>
            <ta e="T433" id="Seg_4466" s="T432">vollschmieren-EP-PASS/REFL-PST1-1SG</ta>
            <ta e="T434" id="Seg_4467" s="T433">Mutter-2PL.[NOM]</ta>
            <ta e="T435" id="Seg_4468" s="T434">Vater-2PL.[NOM]</ta>
            <ta e="T436" id="Seg_4469" s="T435">kommen-PTCP.FUT-3PL-DAT/LOC</ta>
            <ta e="T437" id="Seg_4470" s="T436">bis.zu</ta>
            <ta e="T438" id="Seg_4471" s="T437">Diele-2PL-ACC</ta>
            <ta e="T439" id="Seg_4472" s="T438">mit</ta>
            <ta e="T440" id="Seg_4473" s="T439">Haus-2PL.[NOM]</ta>
            <ta e="T441" id="Seg_4474" s="T440">für</ta>
            <ta e="T442" id="Seg_4475" s="T441">gut-ADVZ</ta>
            <ta e="T443" id="Seg_4476" s="T442">sich.kümmern-EP-IMP.2PL</ta>
            <ta e="T444" id="Seg_4477" s="T443">Opuo.[NOM]</ta>
            <ta e="T445" id="Seg_4478" s="T444">hinausgehen-CVB.PURP</ta>
            <ta e="T446" id="Seg_4479" s="T445">aufstehen-CVB.SEQ</ta>
            <ta e="T447" id="Seg_4480" s="T446">Kind-PL-ACC</ta>
            <ta e="T448" id="Seg_4481" s="T447">fordern-PST2-3SG</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_4482" s="T0">путать-PTCP.PST-3SG.[NOM]</ta>
            <ta e="T2" id="Seg_4483" s="T1">два</ta>
            <ta e="T3" id="Seg_4484" s="T2">мальчик.[NOM]</ta>
            <ta e="T4" id="Seg_4485" s="T3">ребенок-PL.[NOM]</ta>
            <ta e="T5" id="Seg_4486" s="T4">Мика-ACC</ta>
            <ta e="T6" id="Seg_4487" s="T5">с</ta>
            <ta e="T7" id="Seg_4488" s="T6">Мукулай.[NOM]</ta>
            <ta e="T8" id="Seg_4489" s="T7">родители-3PL.[NOM]</ta>
            <ta e="T9" id="Seg_4490" s="T8">идти-PTCP.PST-3PL-ACC</ta>
            <ta e="T10" id="Seg_4491" s="T9">после.того</ta>
            <ta e="T11" id="Seg_4492" s="T10">дом-3PL-DAT/LOC</ta>
            <ta e="T12" id="Seg_4493" s="T11">ночевать-PST2-3PL</ta>
            <ta e="T13" id="Seg_4494" s="T12">3PL.[NOM]</ta>
            <ta e="T14" id="Seg_4495" s="T13">учиться-PTCP.PRS</ta>
            <ta e="T15" id="Seg_4496" s="T14">час-3PL-DAT/LOC</ta>
            <ta e="T16" id="Seg_4497" s="T15">зима-ACC</ta>
            <ta e="T17" id="Seg_4498" s="T16">сквозь</ta>
            <ta e="T18" id="Seg_4499" s="T17">интернат-DAT/LOC</ta>
            <ta e="T19" id="Seg_4500" s="T18">быть-HAB-3PL</ta>
            <ta e="T20" id="Seg_4501" s="T19">давно-ADJZ</ta>
            <ta e="T21" id="Seg_4502" s="T20">русский</ta>
            <ta e="T22" id="Seg_4503" s="T21">дом.[NOM]</ta>
            <ta e="T23" id="Seg_4504" s="T22">стена-3SG-GEN</ta>
            <ta e="T24" id="Seg_4505" s="T23">дерево-PL-3SG.[NOM]</ta>
            <ta e="T25" id="Seg_4506" s="T24">последний-3SG.[NOM]</ta>
            <ta e="T26" id="Seg_4507" s="T25">быть-CVB.SEQ-3PL</ta>
            <ta e="T27" id="Seg_4508" s="T26">очень</ta>
            <ta e="T30" id="Seg_4509" s="T29">истлевать-PST2-3PL</ta>
            <ta e="T31" id="Seg_4510" s="T30">на.улицу-ADJZ</ta>
            <ta e="T32" id="Seg_4511" s="T31">сени-3SG-GEN</ta>
            <ta e="T33" id="Seg_4512" s="T32">дверь-3SG.[NOM]</ta>
            <ta e="T34" id="Seg_4513" s="T33">человек.[NOM]</ta>
            <ta e="T35" id="Seg_4514" s="T34">открывать-TEMP-3SG</ta>
            <ta e="T36" id="Seg_4515" s="T35">скрипеть-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T37" id="Seg_4516" s="T36">очень</ta>
            <ta e="T38" id="Seg_4517" s="T37">дом.[NOM]</ta>
            <ta e="T39" id="Seg_4518" s="T38">нутро-3SG-DAT/LOC</ta>
            <ta e="T40" id="Seg_4519" s="T39">сколько</ta>
            <ta e="T41" id="Seg_4520" s="T40">EMPH</ta>
            <ta e="T42" id="Seg_4521" s="T41">шум.[NOM]</ta>
            <ta e="T43" id="Seg_4522" s="T42">быть-IMP.3SG</ta>
            <ta e="T44" id="Seg_4523" s="T43">кто.[NOM]</ta>
            <ta e="T45" id="Seg_4524" s="T44">INDEF</ta>
            <ta e="T46" id="Seg_4525" s="T45">открывать-TEMP-3SG</ta>
            <ta e="T47" id="Seg_4526" s="T46">скрипеть-PTCP.PRS</ta>
            <ta e="T48" id="Seg_4527" s="T47">звук-3SG.[NOM]</ta>
            <ta e="T49" id="Seg_4528" s="T48">сразу</ta>
            <ta e="T50" id="Seg_4529" s="T49">слышаться-PRS.[3SG]</ta>
            <ta e="T51" id="Seg_4530" s="T50">некоторый.[NOM]</ta>
            <ta e="T52" id="Seg_4531" s="T51">когда</ta>
            <ta e="T53" id="Seg_4532" s="T52">ветер.[NOM]</ta>
            <ta e="T54" id="Seg_4533" s="T53">бить-CVB.SEQ</ta>
            <ta e="T55" id="Seg_4534" s="T54">открывать-TEMP-3SG</ta>
            <ta e="T56" id="Seg_4535" s="T55">скрипеть-CVB.SEQ</ta>
            <ta e="T57" id="Seg_4536" s="T56">дом-PROPR-PL-ACC</ta>
            <ta e="T58" id="Seg_4537" s="T57">жить.[CAUS]-NEG.[3SG]</ta>
            <ta e="T59" id="Seg_4538" s="T58">NEG</ta>
            <ta e="T60" id="Seg_4539" s="T59">спать-CAUS-NEG.[3SG]</ta>
            <ta e="T61" id="Seg_4540" s="T60">NEG</ta>
            <ta e="T62" id="Seg_4541" s="T61">район.[NOM]</ta>
            <ta e="T63" id="Seg_4542" s="T62">центр</ta>
            <ta e="T64" id="Seg_4543" s="T63">стойбище-3SG-DAT/LOC</ta>
            <ta e="T65" id="Seg_4544" s="T64">такой</ta>
            <ta e="T66" id="Seg_4545" s="T65">очень</ta>
            <ta e="T67" id="Seg_4546" s="T66">старый</ta>
            <ta e="T68" id="Seg_4547" s="T67">дом.[NOM]</ta>
            <ta e="T69" id="Seg_4548" s="T68">NEG.EX</ta>
            <ta e="T70" id="Seg_4549" s="T69">дом-PROPR-PL.[NOM]</ta>
            <ta e="T71" id="Seg_4550" s="T70">скоро</ta>
            <ta e="T72" id="Seg_4551" s="T71">новый</ta>
            <ta e="T73" id="Seg_4552" s="T72">дом-DAT/LOC</ta>
            <ta e="T74" id="Seg_4553" s="T73">приносить-EP-REFL-CVB.SEQ</ta>
            <ta e="T75" id="Seg_4554" s="T74">падать</ta>
            <ta e="T76" id="Seg_4555" s="T75">носить-EP-REFL-PTCP.FUT</ta>
            <ta e="T77" id="Seg_4556" s="T76">падать-NEC-3PL</ta>
            <ta e="T78" id="Seg_4557" s="T77">большой</ta>
            <ta e="T79" id="Seg_4558" s="T78">новый</ta>
            <ta e="T80" id="Seg_4559" s="T79">дом.[NOM]</ta>
            <ta e="T81" id="Seg_4560" s="T80">строить-EP-PASS/REFL-CVB.SEQ</ta>
            <ta e="T82" id="Seg_4561" s="T81">кончать-PTCP.PRS-3SG-ACC</ta>
            <ta e="T83" id="Seg_4562" s="T82">ждать-CVB.SIM</ta>
            <ta e="T84" id="Seg_4563" s="T83">сидеть-PRS-3PL</ta>
            <ta e="T85" id="Seg_4564" s="T84">теперь</ta>
            <ta e="T86" id="Seg_4565" s="T85">ребенок-PL.[NOM]</ta>
            <ta e="T87" id="Seg_4566" s="T86">родители-3PL.[NOM]</ta>
            <ta e="T88" id="Seg_4567" s="T87">близкий</ta>
            <ta e="T89" id="Seg_4568" s="T88">родственник-3PL-DAT/LOC</ta>
            <ta e="T90" id="Seg_4569" s="T89">посещать-CVB.SIM</ta>
            <ta e="T91" id="Seg_4570" s="T90">идти-PST2-3PL</ta>
            <ta e="T92" id="Seg_4571" s="T91">дом-3PL-DAT/LOC</ta>
            <ta e="T93" id="Seg_4572" s="T92">завтра</ta>
            <ta e="T94" id="Seg_4573" s="T93">Q</ta>
            <ta e="T95" id="Seg_4574" s="T94">послезавтра</ta>
            <ta e="T96" id="Seg_4575" s="T95">Q</ta>
            <ta e="T97" id="Seg_4576" s="T96">приходить-PTCP.FUT</ta>
            <ta e="T98" id="Seg_4577" s="T97">падать-NEC</ta>
            <ta e="T99" id="Seg_4578" s="T98">быть-PST1-3PL</ta>
            <ta e="T100" id="Seg_4579" s="T99">зимой</ta>
            <ta e="T101" id="Seg_4580" s="T100">темный</ta>
            <ta e="T102" id="Seg_4581" s="T101">день.[NOM]</ta>
            <ta e="T103" id="Seg_4582" s="T102">короткий.[NOM]</ta>
            <ta e="T104" id="Seg_4583" s="T103">вечер-PL-3SG.[NOM]</ta>
            <ta e="T105" id="Seg_4584" s="T104">очень</ta>
            <ta e="T106" id="Seg_4585" s="T105">рано</ta>
            <ta e="T107" id="Seg_4586" s="T106">мальчик.[NOM]</ta>
            <ta e="T108" id="Seg_4587" s="T107">ребенок-PL.[NOM]</ta>
            <ta e="T109" id="Seg_4588" s="T108">есть-PTCP.PRS</ta>
            <ta e="T110" id="Seg_4589" s="T109">пища-3PL-ACC</ta>
            <ta e="T111" id="Seg_4590" s="T110">наесться-PTCP.FUT-3PL-DAT/LOC</ta>
            <ta e="T112" id="Seg_4591" s="T111">пока</ta>
            <ta e="T113" id="Seg_4592" s="T112">есть-CVB.SEQ</ta>
            <ta e="T114" id="Seg_4593" s="T113">после</ta>
            <ta e="T115" id="Seg_4594" s="T114">спать-CVB.PURP</ta>
            <ta e="T116" id="Seg_4595" s="T115">готовить-REFL-PST2-3PL</ta>
            <ta e="T117" id="Seg_4596" s="T116">ну</ta>
            <ta e="T118" id="Seg_4597" s="T117">1PL.[NOM]</ta>
            <ta e="T119" id="Seg_4598" s="T118">старый</ta>
            <ta e="T120" id="Seg_4599" s="T119">дом-1PL.[NOM]</ta>
            <ta e="T121" id="Seg_4600" s="T120">ночью</ta>
            <ta e="T122" id="Seg_4601" s="T121">путать-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T123" id="Seg_4602" s="T122">наверное</ta>
            <ta e="T124" id="Seg_4603" s="T123">Мукулай.[NOM]</ta>
            <ta e="T125" id="Seg_4604" s="T124">старший.брат-3SG-DAT/LOC</ta>
            <ta e="T126" id="Seg_4605" s="T125">говорить-PST2-3SG</ta>
            <ta e="T127" id="Seg_4606" s="T126">2SG.[NOM]</ta>
            <ta e="T128" id="Seg_4607" s="T127">опять</ta>
            <ta e="T129" id="Seg_4608" s="T128">плохой-ADVZ</ta>
            <ta e="T130" id="Seg_4609" s="T129">выбрасывать-REFL-CVB.SEQ</ta>
            <ta e="T131" id="Seg_4610" s="T130">быть-PRS-2SG</ta>
            <ta e="T132" id="Seg_4611" s="T131">3SG-ACC</ta>
            <ta e="T133" id="Seg_4612" s="T132">большой-3SG.[NOM]</ta>
            <ta e="T134" id="Seg_4613" s="T133">ругать-PST2-3SG</ta>
            <ta e="T135" id="Seg_4614" s="T134">зато</ta>
            <ta e="T136" id="Seg_4615" s="T135">голый-VBZ.[IMP.2SG]</ta>
            <ta e="T137" id="Seg_4616" s="T136">там</ta>
            <ta e="T138" id="Seg_4617" s="T137">постель-DAT/LOC</ta>
            <ta e="T139" id="Seg_4618" s="T138">ложиться.[IMP.2SG]</ta>
            <ta e="T140" id="Seg_4619" s="T139">вместе</ta>
            <ta e="T141" id="Seg_4620" s="T140">спать-FUT-1PL</ta>
            <ta e="T142" id="Seg_4621" s="T141">старший.брат-3SG.[NOM]</ta>
            <ta e="T143" id="Seg_4622" s="T142">нутро-3SG-DAT/LOC</ta>
            <ta e="T144" id="Seg_4623" s="T143">ведь</ta>
            <ta e="T145" id="Seg_4624" s="T144">EMPH</ta>
            <ta e="T146" id="Seg_4625" s="T145">насторожиться-EP-MED-PRS.[3SG]</ta>
            <ta e="T147" id="Seg_4626" s="T146">наверное</ta>
            <ta e="T148" id="Seg_4627" s="T147">3PL.[NOM]</ta>
            <ta e="T149" id="Seg_4628" s="T148">пустой</ta>
            <ta e="T150" id="Seg_4629" s="T149">дом-DAT/LOC</ta>
            <ta e="T151" id="Seg_4630" s="T150">родители-POSS</ta>
            <ta e="T152" id="Seg_4631" s="T151">NEG</ta>
            <ta e="T153" id="Seg_4632" s="T152">впервые</ta>
            <ta e="T154" id="Seg_4633" s="T153">ночевать-PRS-3PL</ta>
            <ta e="T155" id="Seg_4634" s="T154">старший.брат-3SG.[NOM]</ta>
            <ta e="T156" id="Seg_4635" s="T155">лампа-3SG-ACC</ta>
            <ta e="T157" id="Seg_4636" s="T156">гасить-CVB.ANT</ta>
            <ta e="T158" id="Seg_4637" s="T157">младший.брат-EP-3SG-GEN</ta>
            <ta e="T159" id="Seg_4638" s="T158">ряд-3SG-INSTR</ta>
            <ta e="T160" id="Seg_4639" s="T159">одеяло.[NOM]</ta>
            <ta e="T161" id="Seg_4640" s="T160">нутро-3SG-DAT/LOC</ta>
            <ta e="T162" id="Seg_4641" s="T161">входить-CVB.SEQ</ta>
            <ta e="T163" id="Seg_4642" s="T162">ложиться-PST2-3SG</ta>
            <ta e="T164" id="Seg_4643" s="T163">ребенок-PL.[NOM]</ta>
            <ta e="T165" id="Seg_4644" s="T164">одеяло-3PL-GEN</ta>
            <ta e="T166" id="Seg_4645" s="T165">нутро-3SG-DAT/LOC</ta>
            <ta e="T167" id="Seg_4646" s="T166">дышать-NEG.CVB.SIM</ta>
            <ta e="T168" id="Seg_4647" s="T167">лежать-PST2-3PL</ta>
            <ta e="T169" id="Seg_4648" s="T168">бояться-CVB.SIM</ta>
            <ta e="T170" id="Seg_4649" s="T169">думать-CVB.SEQ</ta>
            <ta e="T171" id="Seg_4650" s="T170">слушать-PRS-3PL</ta>
            <ta e="T172" id="Seg_4651" s="T171">скоро-INTNS</ta>
            <ta e="T173" id="Seg_4652" s="T172">глаз-3PL-ACC</ta>
            <ta e="T174" id="Seg_4653" s="T173">закрывать.глаза-CVB.SEQ</ta>
            <ta e="T175" id="Seg_4654" s="T174">уснуть-CVB.SEQ</ta>
            <ta e="T176" id="Seg_4655" s="T175">храпеть-CVB.SEQ</ta>
            <ta e="T177" id="Seg_4656" s="T176">оставаться-PST2-3PL</ta>
            <ta e="T178" id="Seg_4657" s="T177">сколько</ta>
            <ta e="T179" id="Seg_4658" s="T178">сколько</ta>
            <ta e="T180" id="Seg_4659" s="T179">быть-PST2-3SG</ta>
            <ta e="T181" id="Seg_4660" s="T180">Q</ta>
            <ta e="T182" id="Seg_4661" s="T181">Мика.[NOM]</ta>
            <ta e="T183" id="Seg_4662" s="T182">младший.брат-EP-3SG.[NOM]</ta>
            <ta e="T184" id="Seg_4663" s="T183">сторона-3SG-DAT/LOC</ta>
            <ta e="T185" id="Seg_4664" s="T184">толкать-FREQ-PTCP.PRS-3SG-ABL</ta>
            <ta e="T186" id="Seg_4665" s="T185">просыпаться-EP-PST2-3SG</ta>
            <ta e="T187" id="Seg_4666" s="T186">старший.брат.[NOM]</ta>
            <ta e="T188" id="Seg_4667" s="T187">слушать.[IMP.2SG]</ta>
            <ta e="T189" id="Seg_4668" s="T188">путать-PTCP.PRS.[NOM]</ta>
            <ta e="T190" id="Seg_4669" s="T189">наверное</ta>
            <ta e="T191" id="Seg_4670" s="T190">Мукулай.[NOM]</ta>
            <ta e="T192" id="Seg_4671" s="T191">бояться-CVB.SEQ</ta>
            <ta e="T193" id="Seg_4672" s="T192">слово-3SG.[NOM]</ta>
            <ta e="T194" id="Seg_4673" s="T193">еле</ta>
            <ta e="T195" id="Seg_4674" s="T194">выйти-EP-PST2-3SG</ta>
            <ta e="T196" id="Seg_4675" s="T195">Мика.[NOM]</ta>
            <ta e="T197" id="Seg_4676" s="T196">одеяло-3SG-GEN</ta>
            <ta e="T198" id="Seg_4677" s="T197">край-3SG-ACC</ta>
            <ta e="T199" id="Seg_4678" s="T198">поднимать-CVB.SEQ</ta>
            <ta e="T200" id="Seg_4679" s="T199">слушать-PST2-3SG</ta>
            <ta e="T201" id="Seg_4680" s="T200">правда.[NOM]</ta>
            <ta e="T202" id="Seg_4681" s="T201">есть-PTCP.PRS</ta>
            <ta e="T203" id="Seg_4682" s="T202">стол-3PL-DAT/LOC</ta>
            <ta e="T204" id="Seg_4683" s="T203">тарелка-PL.[NOM]</ta>
            <ta e="T205" id="Seg_4684" s="T204">ложка-PL.[NOM]</ta>
            <ta e="T206" id="Seg_4685" s="T205">греметь-PRS-3PL</ta>
            <ta e="T207" id="Seg_4686" s="T206">EMPH-темный.[NOM]</ta>
            <ta e="T208" id="Seg_4687" s="T207">что.[NOM]</ta>
            <ta e="T209" id="Seg_4688" s="T208">NEG</ta>
            <ta e="T210" id="Seg_4689" s="T209">быть.видно-EP-NEG.[3SG]</ta>
            <ta e="T211" id="Seg_4690" s="T210">эй</ta>
            <ta e="T212" id="Seg_4691" s="T211">далеко</ta>
            <ta e="T213" id="Seg_4692" s="T212">стоять-PTCP.PRS</ta>
            <ta e="T214" id="Seg_4693" s="T213">стол.[NOM]</ta>
            <ta e="T215" id="Seg_4694" s="T214">верхняя.часть-3SG-DAT/LOC</ta>
            <ta e="T216" id="Seg_4695" s="T215">бумаги-PL-ACC</ta>
            <ta e="T217" id="Seg_4696" s="T216">что.[NOM]</ta>
            <ta e="T218" id="Seg_4697" s="T217">INDEF</ta>
            <ta e="T220" id="Seg_4698" s="T219">разорвать-FREQ-PRS.[3SG]</ta>
            <ta e="T221" id="Seg_4699" s="T220">ребенок-PL.[NOM]</ta>
            <ta e="T222" id="Seg_4700" s="T221">одеяло.[NOM]</ta>
            <ta e="T223" id="Seg_4701" s="T222">нутро-3SG-DAT/LOC</ta>
            <ta e="T224" id="Seg_4702" s="T223">обниматься-CVB.SEQ</ta>
            <ta e="T225" id="Seg_4703" s="T224">после</ta>
            <ta e="T226" id="Seg_4704" s="T225">бояться-CVB.SEQ</ta>
            <ta e="T227" id="Seg_4705" s="T226">дышать-NEG.CVB.SIM</ta>
            <ta e="T228" id="Seg_4706" s="T227">лежать-PRS-3PL</ta>
            <ta e="T229" id="Seg_4707" s="T228">на.улице</ta>
            <ta e="T230" id="Seg_4708" s="T229">сени-DAT/LOC</ta>
            <ta e="T231" id="Seg_4709" s="T230">что.[NOM]</ta>
            <ta e="T232" id="Seg_4710" s="T231">INDEF</ta>
            <ta e="T233" id="Seg_4711" s="T232">идти-FREQ-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T234" id="Seg_4712" s="T233">слышаться-EP-PST2-3SG</ta>
            <ta e="T235" id="Seg_4713" s="T234">дом.[NOM]</ta>
            <ta e="T236" id="Seg_4714" s="T235">стена-3SG-ACC</ta>
            <ta e="T237" id="Seg_4715" s="T236">громкий-ADVZ</ta>
            <ta e="T238" id="Seg_4716" s="T237">бить-FREQ-PRS.[3SG]</ta>
            <ta e="T239" id="Seg_4717" s="T238">полка-PL-ABL</ta>
            <ta e="T240" id="Seg_4718" s="T239">котел-PL-ACC</ta>
            <ta e="T241" id="Seg_4719" s="T240">разный</ta>
            <ta e="T242" id="Seg_4720" s="T241">сосуд-PL-ACC</ta>
            <ta e="T243" id="Seg_4721" s="T242">земля-DAT/LOC</ta>
            <ta e="T244" id="Seg_4722" s="T243">спустить-ITER-PRS.[3SG]</ta>
            <ta e="T245" id="Seg_4723" s="T244">потом</ta>
            <ta e="T246" id="Seg_4724" s="T245">тот-PL-ACC</ta>
            <ta e="T247" id="Seg_4725" s="T246">пнуть-FREQ-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T248" id="Seg_4726" s="T247">слышаться-PRS.[3SG]</ta>
            <ta e="T249" id="Seg_4727" s="T248">тихо</ta>
            <ta e="T250" id="Seg_4728" s="T249">двигаться-NEG.CVB.SIM</ta>
            <ta e="T251" id="Seg_4729" s="T250">лежать.[IMP.2SG]</ta>
            <ta e="T252" id="Seg_4730" s="T251">Мика.[NOM]</ta>
            <ta e="T253" id="Seg_4731" s="T252">младший.брат-EP-3SG-ACC</ta>
            <ta e="T254" id="Seg_4732" s="T253">ругать-PST2-3SG</ta>
            <ta e="T255" id="Seg_4733" s="T254">дом.[NOM]</ta>
            <ta e="T256" id="Seg_4734" s="T255">нутро-3SG-DAT/LOC</ta>
            <ta e="T257" id="Seg_4735" s="T256">сени-DAT/LOC</ta>
            <ta e="T258" id="Seg_4736" s="T257">шум.[NOM]</ta>
            <ta e="T259" id="Seg_4737" s="T258">когда</ta>
            <ta e="T260" id="Seg_4738" s="T259">NEG</ta>
            <ta e="T261" id="Seg_4739" s="T260">кончать-NEG.[3SG]</ta>
            <ta e="T262" id="Seg_4740" s="T261">утро-DAT/LOC</ta>
            <ta e="T263" id="Seg_4741" s="T262">пока</ta>
            <ta e="T264" id="Seg_4742" s="T263">еще</ta>
            <ta e="T265" id="Seg_4743" s="T264">далекий.[NOM]</ta>
            <ta e="T266" id="Seg_4744" s="T265">3PL-ACC</ta>
            <ta e="T267" id="Seg_4745" s="T266">кто.[NOM]</ta>
            <ta e="T268" id="Seg_4746" s="T267">приходить-CVB.SEQ</ta>
            <ta e="T269" id="Seg_4747" s="T268">спасать-FUT.[3SG]=Q</ta>
            <ta e="T270" id="Seg_4748" s="T269">ребенок-PL.[NOM]</ta>
            <ta e="T271" id="Seg_4749" s="T270">остолбенеть.от.страха-CVB.SEQ</ta>
            <ta e="T272" id="Seg_4750" s="T271">плакать-PTCP.FUT-3PL-ACC</ta>
            <ta e="T273" id="Seg_4751" s="T272">NEG</ta>
            <ta e="T274" id="Seg_4752" s="T273">кричать-PTCP.FUT-3PL-ACC</ta>
            <ta e="T275" id="Seg_4753" s="T274">NEG</ta>
            <ta e="T276" id="Seg_4754" s="T275">очень</ta>
            <ta e="T277" id="Seg_4755" s="T276">тот</ta>
            <ta e="T278" id="Seg_4756" s="T277">бояться-CVB.SIM</ta>
            <ta e="T279" id="Seg_4757" s="T278">лежать-CVB.SEQ-3PL</ta>
            <ta e="T280" id="Seg_4758" s="T279">сон-3PL-DAT/LOC</ta>
            <ta e="T281" id="Seg_4759" s="T280">перевешивать-EP-CAUS-CVB.SEQ</ta>
            <ta e="T282" id="Seg_4760" s="T281">спать-CVB.SEQ</ta>
            <ta e="T283" id="Seg_4761" s="T282">оставаться-PST2-3PL</ta>
            <ta e="T284" id="Seg_4762" s="T283">что-COMP</ta>
            <ta e="T285" id="Seg_4763" s="T284">NEG</ta>
            <ta e="T286" id="Seg_4764" s="T285">большой</ta>
            <ta e="T287" id="Seg_4765" s="T286">шум-ABL</ta>
            <ta e="T288" id="Seg_4766" s="T287">неожиданно</ta>
            <ta e="T289" id="Seg_4767" s="T288">просыпаться-EP-PST2-3PL</ta>
            <ta e="T290" id="Seg_4768" s="T289">сени-DAT/LOC</ta>
            <ta e="T291" id="Seg_4769" s="T290">что.[NOM]</ta>
            <ta e="T292" id="Seg_4770" s="T291">INDEF</ta>
            <ta e="T293" id="Seg_4771" s="T292">твердый-ADVZ</ta>
            <ta e="T294" id="Seg_4772" s="T293">камень-SIM</ta>
            <ta e="T295" id="Seg_4773" s="T294">рухнуть-CVB.SEQ</ta>
            <ta e="T296" id="Seg_4774" s="T295">падать-PST2-3SG</ta>
            <ta e="T297" id="Seg_4775" s="T296">крик.[NOM]</ta>
            <ta e="T298" id="Seg_4776" s="T297">вопль.[NOM]</ta>
            <ta e="T299" id="Seg_4777" s="T298">слышаться-EP-PST2-3SG</ta>
            <ta e="T300" id="Seg_4778" s="T299">дверь.[NOM]</ta>
            <ta e="T301" id="Seg_4779" s="T300">открывать-PASS/REFL-EP-PST2-3SG</ta>
            <ta e="T302" id="Seg_4780" s="T301">дом-DAT/LOC</ta>
            <ta e="T303" id="Seg_4781" s="T302">кто.[NOM]</ta>
            <ta e="T304" id="Seg_4782" s="T303">INDEF</ta>
            <ta e="T305" id="Seg_4783" s="T304">входить-CVB.SEQ</ta>
            <ta e="T306" id="Seg_4784" s="T305">лампа-ACC</ta>
            <ta e="T307" id="Seg_4785" s="T306">зажигать-PST2-3SG</ta>
            <ta e="T308" id="Seg_4786" s="T307">вставать-EP-IMP.2PL</ta>
            <ta e="T309" id="Seg_4787" s="T308">2PL.[NOM]</ta>
            <ta e="T310" id="Seg_4788" s="T309">сени-2PL.[NOM]</ta>
            <ta e="T311" id="Seg_4789" s="T310">дверь-3SG-ACC</ta>
            <ta e="T312" id="Seg_4790" s="T311">почему</ta>
            <ta e="T313" id="Seg_4791" s="T312">закрывать-PST2.NEG-2PL=Q</ta>
            <ta e="T314" id="Seg_4792" s="T313">корова-PL.[NOM]</ta>
            <ta e="T315" id="Seg_4793" s="T314">входить-CVB.SEQ-3PL</ta>
            <ta e="T316" id="Seg_4794" s="T315">земля-ACC</ta>
            <ta e="T317" id="Seg_4795" s="T316">вдоволь</ta>
            <ta e="T318" id="Seg_4796" s="T317">пнуть-RECP/COLL-EP-PST2-3PL</ta>
            <ta e="T319" id="Seg_4797" s="T318">сени.[NOM]</ta>
            <ta e="T320" id="Seg_4798" s="T319">нутро-3SG-ACC</ta>
            <ta e="T321" id="Seg_4799" s="T320">один</ta>
            <ta e="T322" id="Seg_4800" s="T321">делать-PST2-3PL</ta>
            <ta e="T323" id="Seg_4801" s="T322">земля-ACC</ta>
            <ta e="T324" id="Seg_4802" s="T323">полный</ta>
            <ta e="T325" id="Seg_4803" s="T324">испражняться-PST2-3PL</ta>
            <ta e="T326" id="Seg_4804" s="T325">ребенок-PL.[NOM]</ta>
            <ta e="T327" id="Seg_4805" s="T326">глаз-3PL-ACC</ta>
            <ta e="T328" id="Seg_4806" s="T327">открывать-CVB.SEQ-3PL</ta>
            <ta e="T329" id="Seg_4807" s="T328">человек.[NOM]</ta>
            <ta e="T330" id="Seg_4808" s="T329">стоять-PTCP.PRS-3SG-ACC</ta>
            <ta e="T331" id="Seg_4809" s="T330">видеть-PST2-3PL</ta>
            <ta e="T332" id="Seg_4810" s="T331">тот-3SG-3PL.[NOM]</ta>
            <ta e="T333" id="Seg_4811" s="T332">сердиться-EP-RECP/COLL-CVB.SIM-сердиться-EP-RECP/COLL-CVB.SIM</ta>
            <ta e="T334" id="Seg_4812" s="T333">одежда-3SG-ACC</ta>
            <ta e="T335" id="Seg_4813" s="T334">галифе</ta>
            <ta e="T336" id="Seg_4814" s="T335">штаны-3SG-ACC</ta>
            <ta e="T337" id="Seg_4815" s="T336">кал-DAT/LOC</ta>
            <ta e="T338" id="Seg_4816" s="T337">замазать-EP-PASS/REFL-EP-PTCP.PST-3SG-ACC</ta>
            <ta e="T339" id="Seg_4817" s="T338">рука-PL-3SG-INSTR</ta>
            <ta e="T340" id="Seg_4818" s="T339">вытирать-CVB.SIM</ta>
            <ta e="T341" id="Seg_4819" s="T340">стоять-PST2-3SG</ta>
            <ta e="T342" id="Seg_4820" s="T341">Опуо.[NOM]</ta>
            <ta e="T343" id="Seg_4821" s="T342">колхоз.[NOM]</ta>
            <ta e="T344" id="Seg_4822" s="T343">совхоз-3SG.[NOM]</ta>
            <ta e="T345" id="Seg_4823" s="T344">работа-3SG-DAT/LOC</ta>
            <ta e="T346" id="Seg_4824" s="T345">работа-3SG-DAT/LOC</ta>
            <ta e="T347" id="Seg_4825" s="T346">дом.[NOM]</ta>
            <ta e="T348" id="Seg_4826" s="T347">место.около-INSTR</ta>
            <ta e="T349" id="Seg_4827" s="T348">идти-CVB.SEQ</ta>
            <ta e="T350" id="Seg_4828" s="T349">идти-CVB.SEQ</ta>
            <ta e="T351" id="Seg_4829" s="T350">сени.[NOM]</ta>
            <ta e="T352" id="Seg_4830" s="T351">корова-PL.[NOM]</ta>
            <ta e="T353" id="Seg_4831" s="T352">входить-PTCP.PST-3PL-ACC</ta>
            <ta e="T354" id="Seg_4832" s="T353">видеть-PST2.[3SG]</ta>
            <ta e="T355" id="Seg_4833" s="T354">потом</ta>
            <ta e="T356" id="Seg_4834" s="T355">каждый-3PL-ACC</ta>
            <ta e="T357" id="Seg_4835" s="T356">гнать-CVB.SEQ</ta>
            <ta e="T358" id="Seg_4836" s="T357">вынимать-PST2-3SG</ta>
            <ta e="T359" id="Seg_4837" s="T358">тот.[NOM]</ta>
            <ta e="T360" id="Seg_4838" s="T359">задняя.часть-3SG-ABL</ta>
            <ta e="T361" id="Seg_4839" s="T360">входить-CVB.SEQ</ta>
            <ta e="T362" id="Seg_4840" s="T361">идти-CVB.SEQ</ta>
            <ta e="T363" id="Seg_4841" s="T362">поскользнуться-CVB.SEQ</ta>
            <ta e="T364" id="Seg_4842" s="T363">падать-CVB.SEQ</ta>
            <ta e="T365" id="Seg_4843" s="T364">корова-PL.[NOM]</ta>
            <ta e="T366" id="Seg_4844" s="T365">кал-3PL-DAT/LOC</ta>
            <ta e="T367" id="Seg_4845" s="T366">вдоволь</ta>
            <ta e="T368" id="Seg_4846" s="T367">замазать-EP-PASS/REFL-EP-PST2-3SG</ta>
            <ta e="T369" id="Seg_4847" s="T368">вставать-CVB.SIM</ta>
            <ta e="T370" id="Seg_4848" s="T369">пытаться-CVB.SEQ</ta>
            <ta e="T371" id="Seg_4849" s="T370">рука-PL-3SG-ACC</ta>
            <ta e="T372" id="Seg_4850" s="T371">тоже</ta>
            <ta e="T373" id="Seg_4851" s="T372">очень</ta>
            <ta e="T374" id="Seg_4852" s="T373">замазать-PST2-3SG</ta>
            <ta e="T375" id="Seg_4853" s="T374">Мика-ACC</ta>
            <ta e="T376" id="Seg_4854" s="T375">с</ta>
            <ta e="T377" id="Seg_4855" s="T376">Мукулай.[NOM]</ta>
            <ta e="T378" id="Seg_4856" s="T377">рассердиться-PTCP.PST</ta>
            <ta e="T379" id="Seg_4857" s="T378">человек.[NOM]</ta>
            <ta e="T380" id="Seg_4858" s="T379">приходить-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T381" id="Seg_4859" s="T380">большой-ADVZ</ta>
            <ta e="T382" id="Seg_4860" s="T381">радоваться-PST2-3PL</ta>
            <ta e="T383" id="Seg_4861" s="T382">Мика.[NOM]</ta>
            <ta e="T384" id="Seg_4862" s="T383">знать-NEG.PTCP</ta>
            <ta e="T385" id="Seg_4863" s="T384">человек-3SG-ABL</ta>
            <ta e="T386" id="Seg_4864" s="T385">стесняться-CVB.SEQ</ta>
            <ta e="T387" id="Seg_4865" s="T386">глаз-3SG-ACC</ta>
            <ta e="T388" id="Seg_4866" s="T387">скрывать-CVB.SIM-скрывать-CVB.SIM</ta>
            <ta e="T389" id="Seg_4867" s="T388">говорить-PST2-3SG</ta>
            <ta e="T390" id="Seg_4868" s="T389">1PL-ACC</ta>
            <ta e="T391" id="Seg_4869" s="T390">ночью</ta>
            <ta e="T392" id="Seg_4870" s="T391">путать-PST2-3SG</ta>
            <ta e="T393" id="Seg_4871" s="T392">мочь-CVB.SEQ</ta>
            <ta e="T394" id="Seg_4872" s="T393">спать-NEG-PST1-1PL</ta>
            <ta e="T395" id="Seg_4873" s="T394">ночь-ACC</ta>
            <ta e="T396" id="Seg_4874" s="T395">сквозь</ta>
            <ta e="T397" id="Seg_4875" s="T396">что.[NOM]</ta>
            <ta e="T398" id="Seg_4876" s="T397">быть-PTCP.PST-3SG-ACC</ta>
            <ta e="T399" id="Seg_4877" s="T398">целый-3SG-ACC</ta>
            <ta e="T400" id="Seg_4878" s="T399">рассказывать-PST2-3SG</ta>
            <ta e="T401" id="Seg_4879" s="T400">Опуо.[NOM]</ta>
            <ta e="T402" id="Seg_4880" s="T401">ребенок-PL.[NOM]</ta>
            <ta e="T403" id="Seg_4881" s="T402">бояться-PTCP.PST</ta>
            <ta e="T404" id="Seg_4882" s="T403">образ-3PL-ACC</ta>
            <ta e="T405" id="Seg_4883" s="T404">видеть-CVB.SEQ</ta>
            <ta e="T406" id="Seg_4884" s="T405">3PL-ACC</ta>
            <ta e="T407" id="Seg_4885" s="T406">жалеть-PST2-3SG</ta>
            <ta e="T408" id="Seg_4886" s="T407">рассердиться-PTCP.PST-3SG.[NOM]</ta>
            <ta e="T409" id="Seg_4887" s="T408">сразу</ta>
            <ta e="T410" id="Seg_4888" s="T409">проехать-PST2-3SG</ta>
            <ta e="T411" id="Seg_4889" s="T410">смеяться-CVB.SIM-смеяться-CVB.SIM</ta>
            <ta e="T412" id="Seg_4890" s="T411">злосчастный-PL-3SG-DAT/LOC</ta>
            <ta e="T413" id="Seg_4891" s="T412">говорить-PST2-3SG</ta>
            <ta e="T414" id="Seg_4892" s="T413">ночью</ta>
            <ta e="T415" id="Seg_4893" s="T414">2PL-ACC</ta>
            <ta e="T416" id="Seg_4894" s="T415">корова-PL-ACC</ta>
            <ta e="T417" id="Seg_4895" s="T416">с</ta>
            <ta e="T418" id="Seg_4896" s="T417">мышь-PL.[NOM]</ta>
            <ta e="T419" id="Seg_4897" s="T418">путать-PST2-3PL</ta>
            <ta e="T420" id="Seg_4898" s="T419">стесняться-NEG-2PL</ta>
            <ta e="T421" id="Seg_4899" s="T420">Q</ta>
            <ta e="T422" id="Seg_4900" s="T421">польза-POSS</ta>
            <ta e="T423" id="Seg_4901" s="T422">NEG-ABL</ta>
            <ta e="T424" id="Seg_4902" s="T423">бояться-PTCP.PST-2PL-ACC</ta>
            <ta e="T425" id="Seg_4903" s="T424">знать-CVB.SEQ-2PL</ta>
            <ta e="T426" id="Seg_4904" s="T425">2PL.[NOM]</ta>
            <ta e="T427" id="Seg_4905" s="T426">плохой-2PL-ABL</ta>
            <ta e="T428" id="Seg_4906" s="T427">1SG.[NOM]</ta>
            <ta e="T431" id="Seg_4907" s="T430">1SG.[NOM]</ta>
            <ta e="T432" id="Seg_4908" s="T431">очень</ta>
            <ta e="T433" id="Seg_4909" s="T432">замазать-EP-PASS/REFL-PST1-1SG</ta>
            <ta e="T434" id="Seg_4910" s="T433">мать-2PL.[NOM]</ta>
            <ta e="T435" id="Seg_4911" s="T434">отец-2PL.[NOM]</ta>
            <ta e="T436" id="Seg_4912" s="T435">приходить-PTCP.FUT-3PL-DAT/LOC</ta>
            <ta e="T437" id="Seg_4913" s="T436">пока</ta>
            <ta e="T438" id="Seg_4914" s="T437">сени-2PL-ACC</ta>
            <ta e="T439" id="Seg_4915" s="T438">с</ta>
            <ta e="T440" id="Seg_4916" s="T439">дом-2PL.[NOM]</ta>
            <ta e="T441" id="Seg_4917" s="T440">для</ta>
            <ta e="T442" id="Seg_4918" s="T441">хороший-ADVZ</ta>
            <ta e="T443" id="Seg_4919" s="T442">заботиться-EP-IMP.2PL</ta>
            <ta e="T444" id="Seg_4920" s="T443">Опуо.[NOM]</ta>
            <ta e="T445" id="Seg_4921" s="T444">выйти-CVB.PURP</ta>
            <ta e="T446" id="Seg_4922" s="T445">вставать-CVB.SEQ</ta>
            <ta e="T447" id="Seg_4923" s="T446">ребенок-PL-ACC</ta>
            <ta e="T448" id="Seg_4924" s="T447">требовать-PST2-3SG</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_4925" s="T0">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T2" id="Seg_4926" s="T1">cardnum</ta>
            <ta e="T3" id="Seg_4927" s="T2">n-n:case</ta>
            <ta e="T4" id="Seg_4928" s="T3">n-n:(num)-n:case</ta>
            <ta e="T5" id="Seg_4929" s="T4">propr-n:case</ta>
            <ta e="T6" id="Seg_4930" s="T5">post</ta>
            <ta e="T7" id="Seg_4931" s="T6">propr-n:case</ta>
            <ta e="T8" id="Seg_4932" s="T7">n-n:(poss)-n:case</ta>
            <ta e="T9" id="Seg_4933" s="T8">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T10" id="Seg_4934" s="T9">post</ta>
            <ta e="T11" id="Seg_4935" s="T10">n-n:poss-n:case</ta>
            <ta e="T12" id="Seg_4936" s="T11">v-v:tense-v:poss.pn</ta>
            <ta e="T13" id="Seg_4937" s="T12">pers-pro:case</ta>
            <ta e="T14" id="Seg_4938" s="T13">v-v:ptcp</ta>
            <ta e="T15" id="Seg_4939" s="T14">n-n:poss-n:case</ta>
            <ta e="T16" id="Seg_4940" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_4941" s="T16">post</ta>
            <ta e="T18" id="Seg_4942" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_4943" s="T18">v-v:mood-v:pred.pn</ta>
            <ta e="T20" id="Seg_4944" s="T19">adv-adv&gt;adj</ta>
            <ta e="T21" id="Seg_4945" s="T20">adj</ta>
            <ta e="T22" id="Seg_4946" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_4947" s="T22">n-n:poss-n:case</ta>
            <ta e="T24" id="Seg_4948" s="T23">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T25" id="Seg_4949" s="T24">adj-n:(poss)-n:case</ta>
            <ta e="T26" id="Seg_4950" s="T25">v-v:cvb-v:pred.pn</ta>
            <ta e="T27" id="Seg_4951" s="T26">adv</ta>
            <ta e="T30" id="Seg_4952" s="T29">v-v:tense-v:pred.pn</ta>
            <ta e="T31" id="Seg_4953" s="T30">adv-adv&gt;adj</ta>
            <ta e="T32" id="Seg_4954" s="T31">n-n:poss-n:case</ta>
            <ta e="T33" id="Seg_4955" s="T32">n-n:(poss)-n:case</ta>
            <ta e="T34" id="Seg_4956" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_4957" s="T34">v-v:mood-v:temp.pn</ta>
            <ta e="T36" id="Seg_4958" s="T35">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T37" id="Seg_4959" s="T36">adv</ta>
            <ta e="T38" id="Seg_4960" s="T37">n-n:case</ta>
            <ta e="T39" id="Seg_4961" s="T38">n-n:poss-n:case</ta>
            <ta e="T40" id="Seg_4962" s="T39">que</ta>
            <ta e="T41" id="Seg_4963" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_4964" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_4965" s="T42">v-v:mood.pn</ta>
            <ta e="T44" id="Seg_4966" s="T43">que-pro:case</ta>
            <ta e="T45" id="Seg_4967" s="T44">ptcl</ta>
            <ta e="T46" id="Seg_4968" s="T45">v-v:mood-v:temp.pn</ta>
            <ta e="T47" id="Seg_4969" s="T46">v-v:ptcp</ta>
            <ta e="T48" id="Seg_4970" s="T47">n-n:(poss)-n:case</ta>
            <ta e="T49" id="Seg_4971" s="T48">adv</ta>
            <ta e="T50" id="Seg_4972" s="T49">v-v:tense-v:pred.pn</ta>
            <ta e="T51" id="Seg_4973" s="T50">indfpro-pro:case</ta>
            <ta e="T52" id="Seg_4974" s="T51">post</ta>
            <ta e="T53" id="Seg_4975" s="T52">n-n:case</ta>
            <ta e="T54" id="Seg_4976" s="T53">v-v:cvb</ta>
            <ta e="T55" id="Seg_4977" s="T54">v-v:mood-v:temp.pn</ta>
            <ta e="T56" id="Seg_4978" s="T55">v-v:cvb</ta>
            <ta e="T57" id="Seg_4979" s="T56">n-n&gt;adj-n:(num)-n:case</ta>
            <ta e="T58" id="Seg_4980" s="T57">v-v&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T59" id="Seg_4981" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_4982" s="T59">v-v&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T61" id="Seg_4983" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_4984" s="T61">n-n:case</ta>
            <ta e="T63" id="Seg_4985" s="T62">n</ta>
            <ta e="T64" id="Seg_4986" s="T63">n-n:poss-n:case</ta>
            <ta e="T65" id="Seg_4987" s="T64">dempro</ta>
            <ta e="T66" id="Seg_4988" s="T65">adv</ta>
            <ta e="T67" id="Seg_4989" s="T66">adj</ta>
            <ta e="T68" id="Seg_4990" s="T67">n-n:case</ta>
            <ta e="T69" id="Seg_4991" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_4992" s="T69">n-n&gt;adj-n:(num)-n:case</ta>
            <ta e="T71" id="Seg_4993" s="T70">adv</ta>
            <ta e="T72" id="Seg_4994" s="T71">adj</ta>
            <ta e="T73" id="Seg_4995" s="T72">n-n:case</ta>
            <ta e="T74" id="Seg_4996" s="T73">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T75" id="Seg_4997" s="T74">v</ta>
            <ta e="T76" id="Seg_4998" s="T75">v-v:(ins)-v&gt;v-v:ptcp</ta>
            <ta e="T77" id="Seg_4999" s="T76">v-v:mood-v:pred.pn</ta>
            <ta e="T78" id="Seg_5000" s="T77">adj</ta>
            <ta e="T79" id="Seg_5001" s="T78">adj</ta>
            <ta e="T80" id="Seg_5002" s="T79">n-n:case</ta>
            <ta e="T81" id="Seg_5003" s="T80">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T82" id="Seg_5004" s="T81">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T83" id="Seg_5005" s="T82">v-v:cvb</ta>
            <ta e="T84" id="Seg_5006" s="T83">v-v:tense-v:pred.pn</ta>
            <ta e="T85" id="Seg_5007" s="T84">adv</ta>
            <ta e="T86" id="Seg_5008" s="T85">n-n:(num)-n:case</ta>
            <ta e="T87" id="Seg_5009" s="T86">n-n:(poss)-n:case</ta>
            <ta e="T88" id="Seg_5010" s="T87">adj</ta>
            <ta e="T89" id="Seg_5011" s="T88">n-n:poss-n:case</ta>
            <ta e="T90" id="Seg_5012" s="T89">v-v:cvb</ta>
            <ta e="T91" id="Seg_5013" s="T90">v-v:tense-v:poss.pn</ta>
            <ta e="T92" id="Seg_5014" s="T91">n-n:poss-n:case</ta>
            <ta e="T93" id="Seg_5015" s="T92">adv</ta>
            <ta e="T94" id="Seg_5016" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_5017" s="T94">adv</ta>
            <ta e="T96" id="Seg_5018" s="T95">ptcl</ta>
            <ta e="T97" id="Seg_5019" s="T96">v-v:ptcp</ta>
            <ta e="T98" id="Seg_5020" s="T97">v-v:mood</ta>
            <ta e="T99" id="Seg_5021" s="T98">v-v:tense-v:poss.pn</ta>
            <ta e="T100" id="Seg_5022" s="T99">adv</ta>
            <ta e="T101" id="Seg_5023" s="T100">adj</ta>
            <ta e="T102" id="Seg_5024" s="T101">n-n:case</ta>
            <ta e="T103" id="Seg_5025" s="T102">adj-n:case</ta>
            <ta e="T104" id="Seg_5026" s="T103">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T105" id="Seg_5027" s="T104">adv</ta>
            <ta e="T106" id="Seg_5028" s="T105">adv</ta>
            <ta e="T107" id="Seg_5029" s="T106">n-n:case</ta>
            <ta e="T108" id="Seg_5030" s="T107">n-n:(num)-n:case</ta>
            <ta e="T109" id="Seg_5031" s="T108">v-v:ptcp</ta>
            <ta e="T110" id="Seg_5032" s="T109">n-n:poss-n:case</ta>
            <ta e="T111" id="Seg_5033" s="T110">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T112" id="Seg_5034" s="T111">post</ta>
            <ta e="T113" id="Seg_5035" s="T112">v-v:cvb</ta>
            <ta e="T114" id="Seg_5036" s="T113">post</ta>
            <ta e="T115" id="Seg_5037" s="T114">v-v:cvb</ta>
            <ta e="T116" id="Seg_5038" s="T115">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T117" id="Seg_5039" s="T116">ptcl</ta>
            <ta e="T118" id="Seg_5040" s="T117">pers-pro:case</ta>
            <ta e="T119" id="Seg_5041" s="T118">adj</ta>
            <ta e="T120" id="Seg_5042" s="T119">n-n:(poss)-n:case</ta>
            <ta e="T121" id="Seg_5043" s="T120">adv</ta>
            <ta e="T122" id="Seg_5044" s="T121">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T123" id="Seg_5045" s="T122">adv</ta>
            <ta e="T124" id="Seg_5046" s="T123">propr-n:case</ta>
            <ta e="T125" id="Seg_5047" s="T124">n-n:poss-n:case</ta>
            <ta e="T126" id="Seg_5048" s="T125">v-v:tense-v:poss.pn</ta>
            <ta e="T127" id="Seg_5049" s="T126">pers-pro:case</ta>
            <ta e="T128" id="Seg_5050" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_5051" s="T128">adj-adj&gt;adv</ta>
            <ta e="T130" id="Seg_5052" s="T129">v-v&gt;v-v:cvb</ta>
            <ta e="T131" id="Seg_5053" s="T130">v-v:tense-v:pred.pn</ta>
            <ta e="T132" id="Seg_5054" s="T131">pers-pro:case</ta>
            <ta e="T133" id="Seg_5055" s="T132">adj-n:(poss)-n:case</ta>
            <ta e="T134" id="Seg_5056" s="T133">v-v:tense-v:poss.pn</ta>
            <ta e="T135" id="Seg_5057" s="T134">adv</ta>
            <ta e="T136" id="Seg_5058" s="T135">adj-adj&gt;v-v:mood.pn</ta>
            <ta e="T137" id="Seg_5059" s="T136">adv</ta>
            <ta e="T138" id="Seg_5060" s="T137">n-n:case</ta>
            <ta e="T139" id="Seg_5061" s="T138">v-v:mood.pn</ta>
            <ta e="T140" id="Seg_5062" s="T139">adv</ta>
            <ta e="T141" id="Seg_5063" s="T140">v-v:tense-v:poss.pn</ta>
            <ta e="T142" id="Seg_5064" s="T141">n-n:(poss)-n:case</ta>
            <ta e="T143" id="Seg_5065" s="T142">n-n:poss-n:case</ta>
            <ta e="T144" id="Seg_5066" s="T143">ptcl</ta>
            <ta e="T145" id="Seg_5067" s="T144">ptcl</ta>
            <ta e="T146" id="Seg_5068" s="T145">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T147" id="Seg_5069" s="T146">adv</ta>
            <ta e="T148" id="Seg_5070" s="T147">pers-pro:case</ta>
            <ta e="T149" id="Seg_5071" s="T148">adj</ta>
            <ta e="T150" id="Seg_5072" s="T149">n-n:case</ta>
            <ta e="T151" id="Seg_5073" s="T150">n-n:(poss)</ta>
            <ta e="T152" id="Seg_5074" s="T151">ptcl</ta>
            <ta e="T153" id="Seg_5075" s="T152">adv</ta>
            <ta e="T154" id="Seg_5076" s="T153">v-v:tense-v:pred.pn</ta>
            <ta e="T155" id="Seg_5077" s="T154">n-n:(poss)-n:case</ta>
            <ta e="T156" id="Seg_5078" s="T155">n-n:poss-n:case</ta>
            <ta e="T157" id="Seg_5079" s="T156">v-v:cvb</ta>
            <ta e="T158" id="Seg_5080" s="T157">n-n:(ins)-n:poss-n:case</ta>
            <ta e="T159" id="Seg_5081" s="T158">n-n:poss-n:case</ta>
            <ta e="T160" id="Seg_5082" s="T159">n-n:case</ta>
            <ta e="T161" id="Seg_5083" s="T160">n-n:poss-n:case</ta>
            <ta e="T162" id="Seg_5084" s="T161">v-v:cvb</ta>
            <ta e="T163" id="Seg_5085" s="T162">v-v:tense-v:poss.pn</ta>
            <ta e="T164" id="Seg_5086" s="T163">n-n:(num)-n:case</ta>
            <ta e="T165" id="Seg_5087" s="T164">n-n:poss-n:case</ta>
            <ta e="T166" id="Seg_5088" s="T165">n-n:poss-n:case</ta>
            <ta e="T167" id="Seg_5089" s="T166">v-v:cvb</ta>
            <ta e="T168" id="Seg_5090" s="T167">v-v:tense-v:poss.pn</ta>
            <ta e="T169" id="Seg_5091" s="T168">v-v:cvb</ta>
            <ta e="T170" id="Seg_5092" s="T169">v-v:cvb</ta>
            <ta e="T171" id="Seg_5093" s="T170">v-v:tense-v:pred.pn</ta>
            <ta e="T172" id="Seg_5094" s="T171">adv-adv&gt;adv</ta>
            <ta e="T173" id="Seg_5095" s="T172">n-n:poss-n:case</ta>
            <ta e="T174" id="Seg_5096" s="T173">v-v:cvb</ta>
            <ta e="T175" id="Seg_5097" s="T174">v-v:cvb</ta>
            <ta e="T176" id="Seg_5098" s="T175">v-v:cvb</ta>
            <ta e="T177" id="Seg_5099" s="T176">v-v:tense-v:poss.pn</ta>
            <ta e="T178" id="Seg_5100" s="T177">que</ta>
            <ta e="T179" id="Seg_5101" s="T178">que</ta>
            <ta e="T180" id="Seg_5102" s="T179">v-v:tense-v:poss.pn</ta>
            <ta e="T181" id="Seg_5103" s="T180">ptcl</ta>
            <ta e="T182" id="Seg_5104" s="T181">propr-n:case</ta>
            <ta e="T183" id="Seg_5105" s="T182">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T184" id="Seg_5106" s="T183">n-n:poss-n:case</ta>
            <ta e="T185" id="Seg_5107" s="T184">v-v&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T186" id="Seg_5108" s="T185">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T187" id="Seg_5109" s="T186">n-n:case</ta>
            <ta e="T188" id="Seg_5110" s="T187">v-v:mood.pn</ta>
            <ta e="T189" id="Seg_5111" s="T188">v-v:ptcp-v:(case)</ta>
            <ta e="T190" id="Seg_5112" s="T189">adv</ta>
            <ta e="T191" id="Seg_5113" s="T190">propr-n:case</ta>
            <ta e="T192" id="Seg_5114" s="T191">v-v:cvb</ta>
            <ta e="T193" id="Seg_5115" s="T192">n-n:(poss)-n:case</ta>
            <ta e="T194" id="Seg_5116" s="T193">adv</ta>
            <ta e="T195" id="Seg_5117" s="T194">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T196" id="Seg_5118" s="T195">propr-n:case</ta>
            <ta e="T197" id="Seg_5119" s="T196">n-n:poss-n:case</ta>
            <ta e="T198" id="Seg_5120" s="T197">n-n:poss-n:case</ta>
            <ta e="T199" id="Seg_5121" s="T198">v-v:cvb</ta>
            <ta e="T200" id="Seg_5122" s="T199">v-v:tense-v:poss.pn</ta>
            <ta e="T201" id="Seg_5123" s="T200">n-n:case</ta>
            <ta e="T202" id="Seg_5124" s="T201">v-v:ptcp</ta>
            <ta e="T203" id="Seg_5125" s="T202">n-n:poss-n:case</ta>
            <ta e="T204" id="Seg_5126" s="T203">n-n:(num)-n:case</ta>
            <ta e="T205" id="Seg_5127" s="T204">n-n:(num)-n:case</ta>
            <ta e="T206" id="Seg_5128" s="T205">v-v:tense-v:pred.pn</ta>
            <ta e="T207" id="Seg_5129" s="T206">adj&gt;adj-adj-n:case</ta>
            <ta e="T208" id="Seg_5130" s="T207">que-pro:case</ta>
            <ta e="T209" id="Seg_5131" s="T208">ptcl</ta>
            <ta e="T210" id="Seg_5132" s="T209">v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T211" id="Seg_5133" s="T210">interj</ta>
            <ta e="T212" id="Seg_5134" s="T211">adv</ta>
            <ta e="T213" id="Seg_5135" s="T212">v-v:ptcp</ta>
            <ta e="T214" id="Seg_5136" s="T213">n-n:case</ta>
            <ta e="T215" id="Seg_5137" s="T214">n-n:poss-n:case</ta>
            <ta e="T216" id="Seg_5138" s="T215">n-n:(num)-n:case</ta>
            <ta e="T217" id="Seg_5139" s="T216">que-pro:case</ta>
            <ta e="T218" id="Seg_5140" s="T217">ptcl</ta>
            <ta e="T220" id="Seg_5141" s="T219">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T221" id="Seg_5142" s="T220">n-n:(num)-n:case</ta>
            <ta e="T222" id="Seg_5143" s="T221">n-n:case</ta>
            <ta e="T223" id="Seg_5144" s="T222">n-n:poss-n:case</ta>
            <ta e="T224" id="Seg_5145" s="T223">v-v:cvb</ta>
            <ta e="T225" id="Seg_5146" s="T224">post</ta>
            <ta e="T226" id="Seg_5147" s="T225">v-v:cvb</ta>
            <ta e="T227" id="Seg_5148" s="T226">v-v:cvb</ta>
            <ta e="T228" id="Seg_5149" s="T227">v-v:tense-v:pred.pn</ta>
            <ta e="T229" id="Seg_5150" s="T228">adv</ta>
            <ta e="T230" id="Seg_5151" s="T229">n-n:case</ta>
            <ta e="T231" id="Seg_5152" s="T230">que-pro:case</ta>
            <ta e="T232" id="Seg_5153" s="T231">ptcl</ta>
            <ta e="T233" id="Seg_5154" s="T232">v-v&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T234" id="Seg_5155" s="T233">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T235" id="Seg_5156" s="T234">n-n:case</ta>
            <ta e="T236" id="Seg_5157" s="T235">n-n:poss-n:case</ta>
            <ta e="T237" id="Seg_5158" s="T236">adj-adj&gt;adv</ta>
            <ta e="T238" id="Seg_5159" s="T237">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T239" id="Seg_5160" s="T238">n-n:(num)-n:case</ta>
            <ta e="T240" id="Seg_5161" s="T239">n-n:(num)-n:case</ta>
            <ta e="T241" id="Seg_5162" s="T240">adj</ta>
            <ta e="T242" id="Seg_5163" s="T241">n-n:(num)-n:case</ta>
            <ta e="T243" id="Seg_5164" s="T242">n-n:case</ta>
            <ta e="T244" id="Seg_5165" s="T243">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T245" id="Seg_5166" s="T244">adv</ta>
            <ta e="T246" id="Seg_5167" s="T245">dempro-pro:(num)-pro:case</ta>
            <ta e="T247" id="Seg_5168" s="T246">v-v&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T248" id="Seg_5169" s="T247">v-v:tense-v:pred.pn</ta>
            <ta e="T249" id="Seg_5170" s="T248">interj</ta>
            <ta e="T250" id="Seg_5171" s="T249">v-v:cvb</ta>
            <ta e="T251" id="Seg_5172" s="T250">v-v:mood.pn</ta>
            <ta e="T252" id="Seg_5173" s="T251">propr-n:case</ta>
            <ta e="T253" id="Seg_5174" s="T252">n-n:(ins)-n:poss-n:case</ta>
            <ta e="T254" id="Seg_5175" s="T253">v-v:tense-v:poss.pn</ta>
            <ta e="T255" id="Seg_5176" s="T254">n-n:case</ta>
            <ta e="T256" id="Seg_5177" s="T255">n-n:poss-n:case</ta>
            <ta e="T257" id="Seg_5178" s="T256">n-n:case</ta>
            <ta e="T258" id="Seg_5179" s="T257">n-n:case</ta>
            <ta e="T259" id="Seg_5180" s="T258">que</ta>
            <ta e="T260" id="Seg_5181" s="T259">ptcl</ta>
            <ta e="T261" id="Seg_5182" s="T260">v-v:(neg)-v:pred.pn</ta>
            <ta e="T262" id="Seg_5183" s="T261">n-n:case</ta>
            <ta e="T263" id="Seg_5184" s="T262">post</ta>
            <ta e="T264" id="Seg_5185" s="T263">adv</ta>
            <ta e="T265" id="Seg_5186" s="T264">adj-n:case</ta>
            <ta e="T266" id="Seg_5187" s="T265">pers-pro:case</ta>
            <ta e="T267" id="Seg_5188" s="T266">que-pro:case</ta>
            <ta e="T268" id="Seg_5189" s="T267">v-v:cvb</ta>
            <ta e="T269" id="Seg_5190" s="T268">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T270" id="Seg_5191" s="T269">n-n:(num)-n:case</ta>
            <ta e="T271" id="Seg_5192" s="T270">v-v:cvb</ta>
            <ta e="T272" id="Seg_5193" s="T271">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T273" id="Seg_5194" s="T272">ptcl</ta>
            <ta e="T274" id="Seg_5195" s="T273">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T275" id="Seg_5196" s="T274">ptcl</ta>
            <ta e="T276" id="Seg_5197" s="T275">adv</ta>
            <ta e="T277" id="Seg_5198" s="T276">dempro</ta>
            <ta e="T278" id="Seg_5199" s="T277">v-v:cvb</ta>
            <ta e="T279" id="Seg_5200" s="T278">v-v:cvb-v:pred.pn</ta>
            <ta e="T280" id="Seg_5201" s="T279">n-n:poss-n:case</ta>
            <ta e="T281" id="Seg_5202" s="T280">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T282" id="Seg_5203" s="T281">v-v:cvb</ta>
            <ta e="T283" id="Seg_5204" s="T282">v-v:tense-v:poss.pn</ta>
            <ta e="T284" id="Seg_5205" s="T283">que-pro:case</ta>
            <ta e="T285" id="Seg_5206" s="T284">ptcl</ta>
            <ta e="T286" id="Seg_5207" s="T285">adj</ta>
            <ta e="T287" id="Seg_5208" s="T286">n-n:case</ta>
            <ta e="T288" id="Seg_5209" s="T287">adv</ta>
            <ta e="T289" id="Seg_5210" s="T288">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T290" id="Seg_5211" s="T289">n-n:case</ta>
            <ta e="T291" id="Seg_5212" s="T290">que-pro:case</ta>
            <ta e="T292" id="Seg_5213" s="T291">ptcl</ta>
            <ta e="T293" id="Seg_5214" s="T292">adj-adj&gt;adv</ta>
            <ta e="T294" id="Seg_5215" s="T293">n-n&gt;adv</ta>
            <ta e="T295" id="Seg_5216" s="T294">v-v:cvb</ta>
            <ta e="T296" id="Seg_5217" s="T295">v-v:tense-v:poss.pn</ta>
            <ta e="T297" id="Seg_5218" s="T296">n-n:case</ta>
            <ta e="T298" id="Seg_5219" s="T297">n-n:case</ta>
            <ta e="T299" id="Seg_5220" s="T298">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T300" id="Seg_5221" s="T299">n-n:case</ta>
            <ta e="T301" id="Seg_5222" s="T300">v-v&gt;v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T302" id="Seg_5223" s="T301">n-n:case</ta>
            <ta e="T303" id="Seg_5224" s="T302">que-pro:case</ta>
            <ta e="T304" id="Seg_5225" s="T303">ptcl</ta>
            <ta e="T305" id="Seg_5226" s="T304">v-v:cvb</ta>
            <ta e="T306" id="Seg_5227" s="T305">n-n:case</ta>
            <ta e="T307" id="Seg_5228" s="T306">v-v:tense-v:poss.pn</ta>
            <ta e="T308" id="Seg_5229" s="T307">v-v:(ins)-v:mood.pn</ta>
            <ta e="T309" id="Seg_5230" s="T308">pers-pro:case</ta>
            <ta e="T310" id="Seg_5231" s="T309">n-n:(poss)-n:case</ta>
            <ta e="T311" id="Seg_5232" s="T310">n-n:poss-n:case</ta>
            <ta e="T312" id="Seg_5233" s="T311">que</ta>
            <ta e="T313" id="Seg_5234" s="T312">v-v:neg-v:pred.pn-ptcl</ta>
            <ta e="T314" id="Seg_5235" s="T313">n-n:(num)-n:case</ta>
            <ta e="T315" id="Seg_5236" s="T314">v-v:cvb-v:pred.pn</ta>
            <ta e="T316" id="Seg_5237" s="T315">n-n:case</ta>
            <ta e="T317" id="Seg_5238" s="T316">adv</ta>
            <ta e="T318" id="Seg_5239" s="T317">v-v&gt;v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T319" id="Seg_5240" s="T318">n-n:case</ta>
            <ta e="T320" id="Seg_5241" s="T319">n-n:poss-n:case</ta>
            <ta e="T321" id="Seg_5242" s="T320">cardnum</ta>
            <ta e="T322" id="Seg_5243" s="T321">v-v:tense-v:pred.pn</ta>
            <ta e="T323" id="Seg_5244" s="T322">n-n:case</ta>
            <ta e="T324" id="Seg_5245" s="T323">adj</ta>
            <ta e="T325" id="Seg_5246" s="T324">v-v:tense-v:pred.pn</ta>
            <ta e="T326" id="Seg_5247" s="T325">n-n:(num)-n:case</ta>
            <ta e="T327" id="Seg_5248" s="T326">n-n:poss-n:case</ta>
            <ta e="T328" id="Seg_5249" s="T327">v-v:cvb-v:pred.pn</ta>
            <ta e="T329" id="Seg_5250" s="T328">n-n:case</ta>
            <ta e="T330" id="Seg_5251" s="T329">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T331" id="Seg_5252" s="T330">v-v:tense-v:poss.pn</ta>
            <ta e="T332" id="Seg_5253" s="T331">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T333" id="Seg_5254" s="T332">v-v:(ins)-v&gt;v-v:cvb-v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T334" id="Seg_5255" s="T333">n-n:poss-n:case</ta>
            <ta e="T335" id="Seg_5256" s="T334">n</ta>
            <ta e="T336" id="Seg_5257" s="T335">n-n:poss-n:case</ta>
            <ta e="T337" id="Seg_5258" s="T336">n-n:case</ta>
            <ta e="T338" id="Seg_5259" s="T337">v-v:(ins)-v&gt;v-n:(ins)-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T339" id="Seg_5260" s="T338">n-n:(num)-n:poss-n:case</ta>
            <ta e="T340" id="Seg_5261" s="T339">v-v:cvb</ta>
            <ta e="T341" id="Seg_5262" s="T340">v-v:tense-v:poss.pn</ta>
            <ta e="T342" id="Seg_5263" s="T341">propr-n:case</ta>
            <ta e="T343" id="Seg_5264" s="T342">n-n:case</ta>
            <ta e="T344" id="Seg_5265" s="T343">n-n:(poss)-n:case</ta>
            <ta e="T345" id="Seg_5266" s="T344">n-n:poss-n:case</ta>
            <ta e="T346" id="Seg_5267" s="T345">n-n:poss-n:case</ta>
            <ta e="T347" id="Seg_5268" s="T346">n-n:case</ta>
            <ta e="T348" id="Seg_5269" s="T347">n-n:case</ta>
            <ta e="T349" id="Seg_5270" s="T348">v-v:cvb</ta>
            <ta e="T350" id="Seg_5271" s="T349">v-v:cvb</ta>
            <ta e="T351" id="Seg_5272" s="T350">n-n:case</ta>
            <ta e="T352" id="Seg_5273" s="T351">n-n:(num)-n:case</ta>
            <ta e="T353" id="Seg_5274" s="T352">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T354" id="Seg_5275" s="T353">v-v:tense-v:pred.pn</ta>
            <ta e="T355" id="Seg_5276" s="T354">adv</ta>
            <ta e="T356" id="Seg_5277" s="T355">adj-n:poss-n:case</ta>
            <ta e="T357" id="Seg_5278" s="T356">v-v:cvb</ta>
            <ta e="T358" id="Seg_5279" s="T357">v-v:tense-v:poss.pn</ta>
            <ta e="T359" id="Seg_5280" s="T358">dempro-pro:case</ta>
            <ta e="T360" id="Seg_5281" s="T359">n-n:poss-n:case</ta>
            <ta e="T361" id="Seg_5282" s="T360">v-v:cvb</ta>
            <ta e="T362" id="Seg_5283" s="T361">v-v:cvb</ta>
            <ta e="T363" id="Seg_5284" s="T362">v-v:cvb</ta>
            <ta e="T364" id="Seg_5285" s="T363">v-v:cvb</ta>
            <ta e="T365" id="Seg_5286" s="T364">n-n:(num)-n:case</ta>
            <ta e="T366" id="Seg_5287" s="T365">n-n:poss-n:case</ta>
            <ta e="T367" id="Seg_5288" s="T366">adv</ta>
            <ta e="T368" id="Seg_5289" s="T367">v-v:(ins)-v&gt;v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T369" id="Seg_5290" s="T368">v-v:cvb</ta>
            <ta e="T370" id="Seg_5291" s="T369">v-v:cvb</ta>
            <ta e="T371" id="Seg_5292" s="T370">n-n:(num)-n:poss-n:case</ta>
            <ta e="T372" id="Seg_5293" s="T371">ptcl</ta>
            <ta e="T373" id="Seg_5294" s="T372">adv</ta>
            <ta e="T374" id="Seg_5295" s="T373">v-v:tense-v:poss.pn</ta>
            <ta e="T375" id="Seg_5296" s="T374">propr-n:case</ta>
            <ta e="T376" id="Seg_5297" s="T375">post</ta>
            <ta e="T377" id="Seg_5298" s="T376">propr-n:case</ta>
            <ta e="T378" id="Seg_5299" s="T377">v-v:ptcp</ta>
            <ta e="T379" id="Seg_5300" s="T378">n-n:case</ta>
            <ta e="T380" id="Seg_5301" s="T379">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T381" id="Seg_5302" s="T380">adj-adj&gt;adv</ta>
            <ta e="T382" id="Seg_5303" s="T381">v-v:tense-v:poss.pn</ta>
            <ta e="T383" id="Seg_5304" s="T382">propr-n:case</ta>
            <ta e="T384" id="Seg_5305" s="T383">v-v:ptcp</ta>
            <ta e="T385" id="Seg_5306" s="T384">n-n:poss-n:case</ta>
            <ta e="T386" id="Seg_5307" s="T385">v-v:cvb</ta>
            <ta e="T387" id="Seg_5308" s="T386">n-n:poss-n:case</ta>
            <ta e="T388" id="Seg_5309" s="T387">v-v:cvb-v-v:cvb</ta>
            <ta e="T389" id="Seg_5310" s="T388">v-v:tense-v:poss.pn</ta>
            <ta e="T390" id="Seg_5311" s="T389">pers-pro:case</ta>
            <ta e="T391" id="Seg_5312" s="T390">adv</ta>
            <ta e="T392" id="Seg_5313" s="T391">v-v:tense-v:poss.pn</ta>
            <ta e="T393" id="Seg_5314" s="T392">v-v:cvb</ta>
            <ta e="T394" id="Seg_5315" s="T393">v-v:(neg)-v:tense-v:poss.pn</ta>
            <ta e="T395" id="Seg_5316" s="T394">n-n:case</ta>
            <ta e="T396" id="Seg_5317" s="T395">post</ta>
            <ta e="T397" id="Seg_5318" s="T396">que-pro:case</ta>
            <ta e="T398" id="Seg_5319" s="T397">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T399" id="Seg_5320" s="T398">adj-n:poss-n:case</ta>
            <ta e="T400" id="Seg_5321" s="T399">v-v:tense-v:poss.pn</ta>
            <ta e="T401" id="Seg_5322" s="T400">propr-n:case</ta>
            <ta e="T402" id="Seg_5323" s="T401">n-n:(num)-n:case</ta>
            <ta e="T403" id="Seg_5324" s="T402">v-v:ptcp</ta>
            <ta e="T404" id="Seg_5325" s="T403">n-n:poss-n:case</ta>
            <ta e="T405" id="Seg_5326" s="T404">v-v:cvb</ta>
            <ta e="T406" id="Seg_5327" s="T405">pers-pro:case</ta>
            <ta e="T407" id="Seg_5328" s="T406">v-v:tense-v:poss.pn</ta>
            <ta e="T408" id="Seg_5329" s="T407">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T409" id="Seg_5330" s="T408">adv</ta>
            <ta e="T410" id="Seg_5331" s="T409">v-v:tense-v:poss.pn</ta>
            <ta e="T411" id="Seg_5332" s="T410">v-v:cvb-v-v:cvb</ta>
            <ta e="T412" id="Seg_5333" s="T411">adj-n:(num)-n:poss-n:case</ta>
            <ta e="T413" id="Seg_5334" s="T412">v-v:tense-v:poss.pn</ta>
            <ta e="T414" id="Seg_5335" s="T413">adv</ta>
            <ta e="T415" id="Seg_5336" s="T414">pers-pro:case</ta>
            <ta e="T416" id="Seg_5337" s="T415">n-n:(num)-n:case</ta>
            <ta e="T417" id="Seg_5338" s="T416">post</ta>
            <ta e="T418" id="Seg_5339" s="T417">n-n:(num)-n:case</ta>
            <ta e="T419" id="Seg_5340" s="T418">v-v:tense-v:poss.pn</ta>
            <ta e="T420" id="Seg_5341" s="T419">v-v:(neg)-v:pred.pn</ta>
            <ta e="T421" id="Seg_5342" s="T420">ptcl</ta>
            <ta e="T422" id="Seg_5343" s="T421">n-n:(poss)</ta>
            <ta e="T423" id="Seg_5344" s="T422">ptcl-ptcl:case</ta>
            <ta e="T424" id="Seg_5345" s="T423">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T425" id="Seg_5346" s="T424">v-v:cvb-v:pred.pn</ta>
            <ta e="T426" id="Seg_5347" s="T425">pers-pro:case</ta>
            <ta e="T427" id="Seg_5348" s="T426">adj-n:poss-n:case</ta>
            <ta e="T428" id="Seg_5349" s="T427">pers-pro:case</ta>
            <ta e="T431" id="Seg_5350" s="T430">pers-pro:case</ta>
            <ta e="T432" id="Seg_5351" s="T431">adv</ta>
            <ta e="T433" id="Seg_5352" s="T432">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T434" id="Seg_5353" s="T433">n-n:(poss)-n:case</ta>
            <ta e="T435" id="Seg_5354" s="T434">n-n:(poss)-n:case</ta>
            <ta e="T436" id="Seg_5355" s="T435">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T437" id="Seg_5356" s="T436">post</ta>
            <ta e="T438" id="Seg_5357" s="T437">n-n:poss-n:case</ta>
            <ta e="T439" id="Seg_5358" s="T438">post</ta>
            <ta e="T440" id="Seg_5359" s="T439">n-n:(poss)-n:case</ta>
            <ta e="T441" id="Seg_5360" s="T440">post</ta>
            <ta e="T442" id="Seg_5361" s="T441">adj-adj&gt;adv</ta>
            <ta e="T443" id="Seg_5362" s="T442">v-v:(ins)-v:mood.pn</ta>
            <ta e="T444" id="Seg_5363" s="T443">propr-n:case</ta>
            <ta e="T445" id="Seg_5364" s="T444">v-v:cvb</ta>
            <ta e="T446" id="Seg_5365" s="T445">v-v:cvb</ta>
            <ta e="T447" id="Seg_5366" s="T446">n-n:(num)-n:case</ta>
            <ta e="T448" id="Seg_5367" s="T447">v-v:tense-v:poss.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_5368" s="T0">v</ta>
            <ta e="T2" id="Seg_5369" s="T1">cardnum</ta>
            <ta e="T3" id="Seg_5370" s="T2">n</ta>
            <ta e="T4" id="Seg_5371" s="T3">n</ta>
            <ta e="T5" id="Seg_5372" s="T4">propr</ta>
            <ta e="T6" id="Seg_5373" s="T5">post</ta>
            <ta e="T7" id="Seg_5374" s="T6">propr</ta>
            <ta e="T8" id="Seg_5375" s="T7">n</ta>
            <ta e="T9" id="Seg_5376" s="T8">v</ta>
            <ta e="T10" id="Seg_5377" s="T9">post</ta>
            <ta e="T11" id="Seg_5378" s="T10">n</ta>
            <ta e="T12" id="Seg_5379" s="T11">v</ta>
            <ta e="T13" id="Seg_5380" s="T12">pers</ta>
            <ta e="T14" id="Seg_5381" s="T13">v</ta>
            <ta e="T15" id="Seg_5382" s="T14">n</ta>
            <ta e="T16" id="Seg_5383" s="T15">n</ta>
            <ta e="T17" id="Seg_5384" s="T16">post</ta>
            <ta e="T18" id="Seg_5385" s="T17">n</ta>
            <ta e="T19" id="Seg_5386" s="T18">cop</ta>
            <ta e="T20" id="Seg_5387" s="T19">adj</ta>
            <ta e="T21" id="Seg_5388" s="T20">adj</ta>
            <ta e="T22" id="Seg_5389" s="T21">n</ta>
            <ta e="T23" id="Seg_5390" s="T22">n</ta>
            <ta e="T24" id="Seg_5391" s="T23">n</ta>
            <ta e="T25" id="Seg_5392" s="T24">adj</ta>
            <ta e="T26" id="Seg_5393" s="T25">cop</ta>
            <ta e="T27" id="Seg_5394" s="T26">adv</ta>
            <ta e="T30" id="Seg_5395" s="T29">v</ta>
            <ta e="T31" id="Seg_5396" s="T30">adj</ta>
            <ta e="T32" id="Seg_5397" s="T31">n</ta>
            <ta e="T33" id="Seg_5398" s="T32">n</ta>
            <ta e="T34" id="Seg_5399" s="T33">n</ta>
            <ta e="T35" id="Seg_5400" s="T34">v</ta>
            <ta e="T36" id="Seg_5401" s="T35">v</ta>
            <ta e="T37" id="Seg_5402" s="T36">adv</ta>
            <ta e="T38" id="Seg_5403" s="T37">n</ta>
            <ta e="T39" id="Seg_5404" s="T38">n</ta>
            <ta e="T40" id="Seg_5405" s="T39">que</ta>
            <ta e="T41" id="Seg_5406" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_5407" s="T41">n</ta>
            <ta e="T43" id="Seg_5408" s="T42">cop</ta>
            <ta e="T44" id="Seg_5409" s="T43">que</ta>
            <ta e="T45" id="Seg_5410" s="T44">ptcl</ta>
            <ta e="T46" id="Seg_5411" s="T45">v</ta>
            <ta e="T47" id="Seg_5412" s="T46">v</ta>
            <ta e="T48" id="Seg_5413" s="T47">n</ta>
            <ta e="T49" id="Seg_5414" s="T48">adv</ta>
            <ta e="T50" id="Seg_5415" s="T49">v</ta>
            <ta e="T51" id="Seg_5416" s="T50">indfpro</ta>
            <ta e="T52" id="Seg_5417" s="T51">post</ta>
            <ta e="T53" id="Seg_5418" s="T52">n</ta>
            <ta e="T54" id="Seg_5419" s="T53">v</ta>
            <ta e="T55" id="Seg_5420" s="T54">v</ta>
            <ta e="T56" id="Seg_5421" s="T55">v</ta>
            <ta e="T57" id="Seg_5422" s="T56">n</ta>
            <ta e="T58" id="Seg_5423" s="T57">v</ta>
            <ta e="T59" id="Seg_5424" s="T58">ptcl</ta>
            <ta e="T60" id="Seg_5425" s="T59">v</ta>
            <ta e="T61" id="Seg_5426" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_5427" s="T61">n</ta>
            <ta e="T63" id="Seg_5428" s="T62">n</ta>
            <ta e="T64" id="Seg_5429" s="T63">n</ta>
            <ta e="T65" id="Seg_5430" s="T64">dempro</ta>
            <ta e="T66" id="Seg_5431" s="T65">adv</ta>
            <ta e="T67" id="Seg_5432" s="T66">adj</ta>
            <ta e="T68" id="Seg_5433" s="T67">n</ta>
            <ta e="T69" id="Seg_5434" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_5435" s="T69">n</ta>
            <ta e="T71" id="Seg_5436" s="T70">adv</ta>
            <ta e="T72" id="Seg_5437" s="T71">adj</ta>
            <ta e="T73" id="Seg_5438" s="T72">n</ta>
            <ta e="T74" id="Seg_5439" s="T73">v</ta>
            <ta e="T75" id="Seg_5440" s="T74">aux</ta>
            <ta e="T76" id="Seg_5441" s="T75">v</ta>
            <ta e="T77" id="Seg_5442" s="T76">aux</ta>
            <ta e="T78" id="Seg_5443" s="T77">adj</ta>
            <ta e="T79" id="Seg_5444" s="T78">adj</ta>
            <ta e="T80" id="Seg_5445" s="T79">n</ta>
            <ta e="T81" id="Seg_5446" s="T80">v</ta>
            <ta e="T82" id="Seg_5447" s="T81">v</ta>
            <ta e="T83" id="Seg_5448" s="T82">v</ta>
            <ta e="T84" id="Seg_5449" s="T83">aux</ta>
            <ta e="T85" id="Seg_5450" s="T84">adv</ta>
            <ta e="T86" id="Seg_5451" s="T85">n</ta>
            <ta e="T87" id="Seg_5452" s="T86">n</ta>
            <ta e="T88" id="Seg_5453" s="T87">adj</ta>
            <ta e="T89" id="Seg_5454" s="T88">n</ta>
            <ta e="T90" id="Seg_5455" s="T89">v</ta>
            <ta e="T91" id="Seg_5456" s="T90">v</ta>
            <ta e="T92" id="Seg_5457" s="T91">n</ta>
            <ta e="T93" id="Seg_5458" s="T92">adv</ta>
            <ta e="T94" id="Seg_5459" s="T93">ptcl</ta>
            <ta e="T95" id="Seg_5460" s="T94">adv</ta>
            <ta e="T96" id="Seg_5461" s="T95">ptcl</ta>
            <ta e="T97" id="Seg_5462" s="T96">v</ta>
            <ta e="T98" id="Seg_5463" s="T97">aux</ta>
            <ta e="T99" id="Seg_5464" s="T98">aux</ta>
            <ta e="T100" id="Seg_5465" s="T99">adv</ta>
            <ta e="T101" id="Seg_5466" s="T100">adj</ta>
            <ta e="T102" id="Seg_5467" s="T101">n</ta>
            <ta e="T103" id="Seg_5468" s="T102">adj</ta>
            <ta e="T104" id="Seg_5469" s="T103">n</ta>
            <ta e="T105" id="Seg_5470" s="T104">adv</ta>
            <ta e="T106" id="Seg_5471" s="T105">adv</ta>
            <ta e="T107" id="Seg_5472" s="T106">n</ta>
            <ta e="T108" id="Seg_5473" s="T107">n</ta>
            <ta e="T109" id="Seg_5474" s="T108">v</ta>
            <ta e="T110" id="Seg_5475" s="T109">n</ta>
            <ta e="T111" id="Seg_5476" s="T110">v</ta>
            <ta e="T112" id="Seg_5477" s="T111">post</ta>
            <ta e="T113" id="Seg_5478" s="T112">v</ta>
            <ta e="T114" id="Seg_5479" s="T113">post</ta>
            <ta e="T115" id="Seg_5480" s="T114">v</ta>
            <ta e="T116" id="Seg_5481" s="T115">v</ta>
            <ta e="T117" id="Seg_5482" s="T116">ptcl</ta>
            <ta e="T118" id="Seg_5483" s="T117">pers</ta>
            <ta e="T119" id="Seg_5484" s="T118">adj</ta>
            <ta e="T120" id="Seg_5485" s="T119">n</ta>
            <ta e="T121" id="Seg_5486" s="T120">adv</ta>
            <ta e="T122" id="Seg_5487" s="T121">v</ta>
            <ta e="T123" id="Seg_5488" s="T122">adv</ta>
            <ta e="T124" id="Seg_5489" s="T123">propr</ta>
            <ta e="T125" id="Seg_5490" s="T124">n</ta>
            <ta e="T126" id="Seg_5491" s="T125">v</ta>
            <ta e="T127" id="Seg_5492" s="T126">pers</ta>
            <ta e="T128" id="Seg_5493" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_5494" s="T128">adv</ta>
            <ta e="T130" id="Seg_5495" s="T129">v</ta>
            <ta e="T131" id="Seg_5496" s="T130">aux</ta>
            <ta e="T132" id="Seg_5497" s="T131">pers</ta>
            <ta e="T133" id="Seg_5498" s="T132">n</ta>
            <ta e="T134" id="Seg_5499" s="T133">v</ta>
            <ta e="T135" id="Seg_5500" s="T134">adv</ta>
            <ta e="T136" id="Seg_5501" s="T135">v</ta>
            <ta e="T137" id="Seg_5502" s="T136">adv</ta>
            <ta e="T138" id="Seg_5503" s="T137">n</ta>
            <ta e="T139" id="Seg_5504" s="T138">v</ta>
            <ta e="T140" id="Seg_5505" s="T139">adv</ta>
            <ta e="T141" id="Seg_5506" s="T140">v</ta>
            <ta e="T142" id="Seg_5507" s="T141">n</ta>
            <ta e="T143" id="Seg_5508" s="T142">n</ta>
            <ta e="T144" id="Seg_5509" s="T143">ptcl</ta>
            <ta e="T145" id="Seg_5510" s="T144">ptcl</ta>
            <ta e="T146" id="Seg_5511" s="T145">v</ta>
            <ta e="T147" id="Seg_5512" s="T146">adv</ta>
            <ta e="T148" id="Seg_5513" s="T147">pers</ta>
            <ta e="T149" id="Seg_5514" s="T148">adj</ta>
            <ta e="T150" id="Seg_5515" s="T149">n</ta>
            <ta e="T151" id="Seg_5516" s="T150">n</ta>
            <ta e="T152" id="Seg_5517" s="T151">ptcl</ta>
            <ta e="T153" id="Seg_5518" s="T152">adv</ta>
            <ta e="T154" id="Seg_5519" s="T153">v</ta>
            <ta e="T155" id="Seg_5520" s="T154">n</ta>
            <ta e="T156" id="Seg_5521" s="T155">n</ta>
            <ta e="T157" id="Seg_5522" s="T156">v</ta>
            <ta e="T158" id="Seg_5523" s="T157">n</ta>
            <ta e="T159" id="Seg_5524" s="T158">n</ta>
            <ta e="T160" id="Seg_5525" s="T159">n</ta>
            <ta e="T161" id="Seg_5526" s="T160">n</ta>
            <ta e="T162" id="Seg_5527" s="T161">v</ta>
            <ta e="T163" id="Seg_5528" s="T162">v</ta>
            <ta e="T164" id="Seg_5529" s="T163">n</ta>
            <ta e="T165" id="Seg_5530" s="T164">n</ta>
            <ta e="T166" id="Seg_5531" s="T165">n</ta>
            <ta e="T167" id="Seg_5532" s="T166">v</ta>
            <ta e="T168" id="Seg_5533" s="T167">v</ta>
            <ta e="T169" id="Seg_5534" s="T168">v</ta>
            <ta e="T170" id="Seg_5535" s="T169">v</ta>
            <ta e="T171" id="Seg_5536" s="T170">v</ta>
            <ta e="T172" id="Seg_5537" s="T171">adv</ta>
            <ta e="T173" id="Seg_5538" s="T172">n</ta>
            <ta e="T174" id="Seg_5539" s="T173">v</ta>
            <ta e="T175" id="Seg_5540" s="T174">v</ta>
            <ta e="T176" id="Seg_5541" s="T175">v</ta>
            <ta e="T177" id="Seg_5542" s="T176">aux</ta>
            <ta e="T178" id="Seg_5543" s="T177">que</ta>
            <ta e="T179" id="Seg_5544" s="T178">que</ta>
            <ta e="T180" id="Seg_5545" s="T179">v</ta>
            <ta e="T181" id="Seg_5546" s="T180">ptcl</ta>
            <ta e="T182" id="Seg_5547" s="T181">propr</ta>
            <ta e="T183" id="Seg_5548" s="T182">n</ta>
            <ta e="T184" id="Seg_5549" s="T183">n</ta>
            <ta e="T185" id="Seg_5550" s="T184">v</ta>
            <ta e="T186" id="Seg_5551" s="T185">v</ta>
            <ta e="T187" id="Seg_5552" s="T186">n</ta>
            <ta e="T188" id="Seg_5553" s="T187">v</ta>
            <ta e="T189" id="Seg_5554" s="T188">v</ta>
            <ta e="T190" id="Seg_5555" s="T189">adv</ta>
            <ta e="T191" id="Seg_5556" s="T190">propr</ta>
            <ta e="T192" id="Seg_5557" s="T191">v</ta>
            <ta e="T193" id="Seg_5558" s="T192">n</ta>
            <ta e="T194" id="Seg_5559" s="T193">adv</ta>
            <ta e="T195" id="Seg_5560" s="T194">v</ta>
            <ta e="T196" id="Seg_5561" s="T195">propr</ta>
            <ta e="T197" id="Seg_5562" s="T196">n</ta>
            <ta e="T198" id="Seg_5563" s="T197">n</ta>
            <ta e="T199" id="Seg_5564" s="T198">v</ta>
            <ta e="T200" id="Seg_5565" s="T199">v</ta>
            <ta e="T201" id="Seg_5566" s="T200">n</ta>
            <ta e="T202" id="Seg_5567" s="T201">v</ta>
            <ta e="T203" id="Seg_5568" s="T202">n</ta>
            <ta e="T204" id="Seg_5569" s="T203">n</ta>
            <ta e="T205" id="Seg_5570" s="T204">n</ta>
            <ta e="T206" id="Seg_5571" s="T205">v</ta>
            <ta e="T207" id="Seg_5572" s="T206">adj</ta>
            <ta e="T208" id="Seg_5573" s="T207">que</ta>
            <ta e="T209" id="Seg_5574" s="T208">ptcl</ta>
            <ta e="T210" id="Seg_5575" s="T209">v</ta>
            <ta e="T211" id="Seg_5576" s="T210">interj</ta>
            <ta e="T212" id="Seg_5577" s="T211">adv</ta>
            <ta e="T213" id="Seg_5578" s="T212">v</ta>
            <ta e="T214" id="Seg_5579" s="T213">n</ta>
            <ta e="T215" id="Seg_5580" s="T214">n</ta>
            <ta e="T216" id="Seg_5581" s="T215">n</ta>
            <ta e="T217" id="Seg_5582" s="T216">que</ta>
            <ta e="T218" id="Seg_5583" s="T217">ptcl</ta>
            <ta e="T220" id="Seg_5584" s="T219">v</ta>
            <ta e="T221" id="Seg_5585" s="T220">n</ta>
            <ta e="T222" id="Seg_5586" s="T221">n</ta>
            <ta e="T223" id="Seg_5587" s="T222">n</ta>
            <ta e="T224" id="Seg_5588" s="T223">v</ta>
            <ta e="T225" id="Seg_5589" s="T224">post</ta>
            <ta e="T226" id="Seg_5590" s="T225">v</ta>
            <ta e="T227" id="Seg_5591" s="T226">v</ta>
            <ta e="T228" id="Seg_5592" s="T227">v</ta>
            <ta e="T229" id="Seg_5593" s="T228">adv</ta>
            <ta e="T230" id="Seg_5594" s="T229">n</ta>
            <ta e="T231" id="Seg_5595" s="T230">que</ta>
            <ta e="T232" id="Seg_5596" s="T231">ptcl</ta>
            <ta e="T233" id="Seg_5597" s="T232">v</ta>
            <ta e="T234" id="Seg_5598" s="T233">v</ta>
            <ta e="T235" id="Seg_5599" s="T234">n</ta>
            <ta e="T236" id="Seg_5600" s="T235">n</ta>
            <ta e="T237" id="Seg_5601" s="T236">adv</ta>
            <ta e="T238" id="Seg_5602" s="T237">v</ta>
            <ta e="T239" id="Seg_5603" s="T238">n</ta>
            <ta e="T240" id="Seg_5604" s="T239">n</ta>
            <ta e="T241" id="Seg_5605" s="T240">adj</ta>
            <ta e="T242" id="Seg_5606" s="T241">n</ta>
            <ta e="T243" id="Seg_5607" s="T242">n</ta>
            <ta e="T244" id="Seg_5608" s="T243">v</ta>
            <ta e="T245" id="Seg_5609" s="T244">adv</ta>
            <ta e="T246" id="Seg_5610" s="T245">dempro</ta>
            <ta e="T247" id="Seg_5611" s="T246">v</ta>
            <ta e="T248" id="Seg_5612" s="T247">v</ta>
            <ta e="T249" id="Seg_5613" s="T248">interj</ta>
            <ta e="T250" id="Seg_5614" s="T249">v</ta>
            <ta e="T251" id="Seg_5615" s="T250">v</ta>
            <ta e="T252" id="Seg_5616" s="T251">propr</ta>
            <ta e="T253" id="Seg_5617" s="T252">n</ta>
            <ta e="T254" id="Seg_5618" s="T253">v</ta>
            <ta e="T255" id="Seg_5619" s="T254">n</ta>
            <ta e="T256" id="Seg_5620" s="T255">n</ta>
            <ta e="T257" id="Seg_5621" s="T256">n</ta>
            <ta e="T258" id="Seg_5622" s="T257">n</ta>
            <ta e="T259" id="Seg_5623" s="T258">que</ta>
            <ta e="T260" id="Seg_5624" s="T259">ptcl</ta>
            <ta e="T261" id="Seg_5625" s="T260">v</ta>
            <ta e="T262" id="Seg_5626" s="T261">n</ta>
            <ta e="T263" id="Seg_5627" s="T262">post</ta>
            <ta e="T264" id="Seg_5628" s="T263">adv</ta>
            <ta e="T265" id="Seg_5629" s="T264">adj</ta>
            <ta e="T266" id="Seg_5630" s="T265">pers</ta>
            <ta e="T267" id="Seg_5631" s="T266">que</ta>
            <ta e="T268" id="Seg_5632" s="T267">v</ta>
            <ta e="T269" id="Seg_5633" s="T268">v</ta>
            <ta e="T270" id="Seg_5634" s="T269">n</ta>
            <ta e="T271" id="Seg_5635" s="T270">v</ta>
            <ta e="T272" id="Seg_5636" s="T271">v</ta>
            <ta e="T273" id="Seg_5637" s="T272">ptcl</ta>
            <ta e="T274" id="Seg_5638" s="T273">v</ta>
            <ta e="T275" id="Seg_5639" s="T274">ptcl</ta>
            <ta e="T276" id="Seg_5640" s="T275">adv</ta>
            <ta e="T277" id="Seg_5641" s="T276">dempro</ta>
            <ta e="T278" id="Seg_5642" s="T277">v</ta>
            <ta e="T279" id="Seg_5643" s="T278">aux</ta>
            <ta e="T280" id="Seg_5644" s="T279">n</ta>
            <ta e="T281" id="Seg_5645" s="T280">v</ta>
            <ta e="T282" id="Seg_5646" s="T281">v</ta>
            <ta e="T283" id="Seg_5647" s="T282">aux</ta>
            <ta e="T284" id="Seg_5648" s="T283">que</ta>
            <ta e="T285" id="Seg_5649" s="T284">ptcl</ta>
            <ta e="T286" id="Seg_5650" s="T285">adj</ta>
            <ta e="T287" id="Seg_5651" s="T286">n</ta>
            <ta e="T288" id="Seg_5652" s="T287">adv</ta>
            <ta e="T289" id="Seg_5653" s="T288">v</ta>
            <ta e="T290" id="Seg_5654" s="T289">n</ta>
            <ta e="T291" id="Seg_5655" s="T290">que</ta>
            <ta e="T292" id="Seg_5656" s="T291">ptcl</ta>
            <ta e="T293" id="Seg_5657" s="T292">adv</ta>
            <ta e="T294" id="Seg_5658" s="T293">adv</ta>
            <ta e="T295" id="Seg_5659" s="T294">v</ta>
            <ta e="T296" id="Seg_5660" s="T295">aux</ta>
            <ta e="T297" id="Seg_5661" s="T296">n</ta>
            <ta e="T298" id="Seg_5662" s="T297">n</ta>
            <ta e="T299" id="Seg_5663" s="T298">v</ta>
            <ta e="T300" id="Seg_5664" s="T299">n</ta>
            <ta e="T301" id="Seg_5665" s="T300">v</ta>
            <ta e="T302" id="Seg_5666" s="T301">n</ta>
            <ta e="T303" id="Seg_5667" s="T302">que</ta>
            <ta e="T304" id="Seg_5668" s="T303">ptcl</ta>
            <ta e="T305" id="Seg_5669" s="T304">v</ta>
            <ta e="T306" id="Seg_5670" s="T305">n</ta>
            <ta e="T307" id="Seg_5671" s="T306">v</ta>
            <ta e="T308" id="Seg_5672" s="T307">v</ta>
            <ta e="T309" id="Seg_5673" s="T308">pers</ta>
            <ta e="T310" id="Seg_5674" s="T309">n</ta>
            <ta e="T311" id="Seg_5675" s="T310">n</ta>
            <ta e="T312" id="Seg_5676" s="T311">que</ta>
            <ta e="T313" id="Seg_5677" s="T312">v</ta>
            <ta e="T314" id="Seg_5678" s="T313">n</ta>
            <ta e="T315" id="Seg_5679" s="T314">v</ta>
            <ta e="T316" id="Seg_5680" s="T315">n</ta>
            <ta e="T317" id="Seg_5681" s="T316">adv</ta>
            <ta e="T318" id="Seg_5682" s="T317">v</ta>
            <ta e="T319" id="Seg_5683" s="T318">n</ta>
            <ta e="T320" id="Seg_5684" s="T319">n</ta>
            <ta e="T321" id="Seg_5685" s="T320">cardnum</ta>
            <ta e="T322" id="Seg_5686" s="T321">v</ta>
            <ta e="T323" id="Seg_5687" s="T322">n</ta>
            <ta e="T324" id="Seg_5688" s="T323">adj</ta>
            <ta e="T325" id="Seg_5689" s="T324">v</ta>
            <ta e="T326" id="Seg_5690" s="T325">n</ta>
            <ta e="T327" id="Seg_5691" s="T326">n</ta>
            <ta e="T328" id="Seg_5692" s="T327">v</ta>
            <ta e="T329" id="Seg_5693" s="T328">n</ta>
            <ta e="T330" id="Seg_5694" s="T329">v</ta>
            <ta e="T331" id="Seg_5695" s="T330">v</ta>
            <ta e="T332" id="Seg_5696" s="T331">dempro</ta>
            <ta e="T333" id="Seg_5697" s="T332">v</ta>
            <ta e="T334" id="Seg_5698" s="T333">n</ta>
            <ta e="T335" id="Seg_5699" s="T334">n</ta>
            <ta e="T336" id="Seg_5700" s="T335">n</ta>
            <ta e="T337" id="Seg_5701" s="T336">n</ta>
            <ta e="T338" id="Seg_5702" s="T337">v</ta>
            <ta e="T339" id="Seg_5703" s="T338">n</ta>
            <ta e="T340" id="Seg_5704" s="T339">v</ta>
            <ta e="T341" id="Seg_5705" s="T340">aux</ta>
            <ta e="T342" id="Seg_5706" s="T341">propr</ta>
            <ta e="T343" id="Seg_5707" s="T342">n</ta>
            <ta e="T344" id="Seg_5708" s="T343">n</ta>
            <ta e="T345" id="Seg_5709" s="T344">n</ta>
            <ta e="T346" id="Seg_5710" s="T345">n</ta>
            <ta e="T347" id="Seg_5711" s="T346">n</ta>
            <ta e="T348" id="Seg_5712" s="T347">n</ta>
            <ta e="T349" id="Seg_5713" s="T348">v</ta>
            <ta e="T350" id="Seg_5714" s="T349">aux</ta>
            <ta e="T351" id="Seg_5715" s="T350">n</ta>
            <ta e="T352" id="Seg_5716" s="T351">n</ta>
            <ta e="T353" id="Seg_5717" s="T352">v</ta>
            <ta e="T354" id="Seg_5718" s="T353">v</ta>
            <ta e="T355" id="Seg_5719" s="T354">adv</ta>
            <ta e="T356" id="Seg_5720" s="T355">adj</ta>
            <ta e="T357" id="Seg_5721" s="T356">v</ta>
            <ta e="T358" id="Seg_5722" s="T357">v</ta>
            <ta e="T359" id="Seg_5723" s="T358">dempro</ta>
            <ta e="T360" id="Seg_5724" s="T359">n</ta>
            <ta e="T361" id="Seg_5725" s="T360">v</ta>
            <ta e="T362" id="Seg_5726" s="T361">aux</ta>
            <ta e="T363" id="Seg_5727" s="T362">v</ta>
            <ta e="T364" id="Seg_5728" s="T363">aux</ta>
            <ta e="T365" id="Seg_5729" s="T364">n</ta>
            <ta e="T366" id="Seg_5730" s="T365">n</ta>
            <ta e="T367" id="Seg_5731" s="T366">adv</ta>
            <ta e="T368" id="Seg_5732" s="T367">v</ta>
            <ta e="T369" id="Seg_5733" s="T368">v</ta>
            <ta e="T370" id="Seg_5734" s="T369">v</ta>
            <ta e="T371" id="Seg_5735" s="T370">n</ta>
            <ta e="T372" id="Seg_5736" s="T371">ptcl</ta>
            <ta e="T373" id="Seg_5737" s="T372">adv</ta>
            <ta e="T374" id="Seg_5738" s="T373">v</ta>
            <ta e="T375" id="Seg_5739" s="T374">propr</ta>
            <ta e="T376" id="Seg_5740" s="T375">post</ta>
            <ta e="T377" id="Seg_5741" s="T376">propr</ta>
            <ta e="T378" id="Seg_5742" s="T377">v</ta>
            <ta e="T379" id="Seg_5743" s="T378">n</ta>
            <ta e="T380" id="Seg_5744" s="T379">v</ta>
            <ta e="T381" id="Seg_5745" s="T380">adv</ta>
            <ta e="T382" id="Seg_5746" s="T381">v</ta>
            <ta e="T383" id="Seg_5747" s="T382">propr</ta>
            <ta e="T384" id="Seg_5748" s="T383">v</ta>
            <ta e="T385" id="Seg_5749" s="T384">n</ta>
            <ta e="T386" id="Seg_5750" s="T385">v</ta>
            <ta e="T387" id="Seg_5751" s="T386">n</ta>
            <ta e="T388" id="Seg_5752" s="T387">v</ta>
            <ta e="T389" id="Seg_5753" s="T388">v</ta>
            <ta e="T390" id="Seg_5754" s="T389">pers</ta>
            <ta e="T391" id="Seg_5755" s="T390">adv</ta>
            <ta e="T392" id="Seg_5756" s="T391">v</ta>
            <ta e="T393" id="Seg_5757" s="T392">v</ta>
            <ta e="T394" id="Seg_5758" s="T393">v</ta>
            <ta e="T395" id="Seg_5759" s="T394">n</ta>
            <ta e="T396" id="Seg_5760" s="T395">post</ta>
            <ta e="T397" id="Seg_5761" s="T396">que</ta>
            <ta e="T398" id="Seg_5762" s="T397">cop</ta>
            <ta e="T399" id="Seg_5763" s="T398">adj</ta>
            <ta e="T400" id="Seg_5764" s="T399">v</ta>
            <ta e="T401" id="Seg_5765" s="T400">propr</ta>
            <ta e="T402" id="Seg_5766" s="T401">n</ta>
            <ta e="T403" id="Seg_5767" s="T402">v</ta>
            <ta e="T404" id="Seg_5768" s="T403">n</ta>
            <ta e="T405" id="Seg_5769" s="T404">v</ta>
            <ta e="T406" id="Seg_5770" s="T405">pers</ta>
            <ta e="T407" id="Seg_5771" s="T406">v</ta>
            <ta e="T408" id="Seg_5772" s="T407">n</ta>
            <ta e="T409" id="Seg_5773" s="T408">adv</ta>
            <ta e="T410" id="Seg_5774" s="T409">v</ta>
            <ta e="T411" id="Seg_5775" s="T410">v</ta>
            <ta e="T412" id="Seg_5776" s="T411">n</ta>
            <ta e="T413" id="Seg_5777" s="T412">v</ta>
            <ta e="T414" id="Seg_5778" s="T413">adv</ta>
            <ta e="T415" id="Seg_5779" s="T414">pers</ta>
            <ta e="T416" id="Seg_5780" s="T415">n</ta>
            <ta e="T417" id="Seg_5781" s="T416">post</ta>
            <ta e="T418" id="Seg_5782" s="T417">n</ta>
            <ta e="T419" id="Seg_5783" s="T418">v</ta>
            <ta e="T420" id="Seg_5784" s="T419">v</ta>
            <ta e="T421" id="Seg_5785" s="T420">ptcl</ta>
            <ta e="T422" id="Seg_5786" s="T421">n</ta>
            <ta e="T423" id="Seg_5787" s="T422">ptcl</ta>
            <ta e="T424" id="Seg_5788" s="T423">v</ta>
            <ta e="T425" id="Seg_5789" s="T424">v</ta>
            <ta e="T426" id="Seg_5790" s="T425">pers</ta>
            <ta e="T427" id="Seg_5791" s="T426">n</ta>
            <ta e="T428" id="Seg_5792" s="T427">pers</ta>
            <ta e="T431" id="Seg_5793" s="T430">pers</ta>
            <ta e="T432" id="Seg_5794" s="T431">adv</ta>
            <ta e="T433" id="Seg_5795" s="T432">v</ta>
            <ta e="T434" id="Seg_5796" s="T433">n</ta>
            <ta e="T435" id="Seg_5797" s="T434">n</ta>
            <ta e="T436" id="Seg_5798" s="T435">v</ta>
            <ta e="T437" id="Seg_5799" s="T436">post</ta>
            <ta e="T438" id="Seg_5800" s="T437">n</ta>
            <ta e="T439" id="Seg_5801" s="T438">post</ta>
            <ta e="T440" id="Seg_5802" s="T439">n</ta>
            <ta e="T441" id="Seg_5803" s="T440">post</ta>
            <ta e="T442" id="Seg_5804" s="T441">adv</ta>
            <ta e="T443" id="Seg_5805" s="T442">v</ta>
            <ta e="T444" id="Seg_5806" s="T443">propr</ta>
            <ta e="T445" id="Seg_5807" s="T444">v</ta>
            <ta e="T446" id="Seg_5808" s="T445">v</ta>
            <ta e="T447" id="Seg_5809" s="T446">n</ta>
            <ta e="T448" id="Seg_5810" s="T447">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T4" id="Seg_5811" s="T3">np.h:A</ta>
            <ta e="T6" id="Seg_5812" s="T4">pp:Com</ta>
            <ta e="T8" id="Seg_5813" s="T7">0.3.h:Poss np.h:A</ta>
            <ta e="T11" id="Seg_5814" s="T10">np:L</ta>
            <ta e="T13" id="Seg_5815" s="T12">pro.h:Th</ta>
            <ta e="T15" id="Seg_5816" s="T14">n:Time</ta>
            <ta e="T17" id="Seg_5817" s="T15">pp:Time</ta>
            <ta e="T18" id="Seg_5818" s="T17">np:L</ta>
            <ta e="T22" id="Seg_5819" s="T21">np:Poss</ta>
            <ta e="T23" id="Seg_5820" s="T22">np:Poss</ta>
            <ta e="T24" id="Seg_5821" s="T23">np:Th</ta>
            <ta e="T26" id="Seg_5822" s="T25">0.3:Th</ta>
            <ta e="T32" id="Seg_5823" s="T31">0.3:Poss np:Poss</ta>
            <ta e="T33" id="Seg_5824" s="T32">np:Th</ta>
            <ta e="T34" id="Seg_5825" s="T33">np.h:A</ta>
            <ta e="T36" id="Seg_5826" s="T35">0.3:Th</ta>
            <ta e="T39" id="Seg_5827" s="T38">np:L</ta>
            <ta e="T42" id="Seg_5828" s="T41">np:Th</ta>
            <ta e="T44" id="Seg_5829" s="T43">pro.h:A</ta>
            <ta e="T48" id="Seg_5830" s="T47">np:Th</ta>
            <ta e="T49" id="Seg_5831" s="T48">adv:Time</ta>
            <ta e="T52" id="Seg_5832" s="T50">pp:Time</ta>
            <ta e="T53" id="Seg_5833" s="T52">np:Cau</ta>
            <ta e="T57" id="Seg_5834" s="T56">np.h:Th</ta>
            <ta e="T58" id="Seg_5835" s="T57">0.3:Cau</ta>
            <ta e="T60" id="Seg_5836" s="T59">0.3:Cau</ta>
            <ta e="T62" id="Seg_5837" s="T61">np:Poss</ta>
            <ta e="T64" id="Seg_5838" s="T63">np:L</ta>
            <ta e="T68" id="Seg_5839" s="T67">np:Th</ta>
            <ta e="T70" id="Seg_5840" s="T69">np.h:A</ta>
            <ta e="T71" id="Seg_5841" s="T70">adv:Time</ta>
            <ta e="T73" id="Seg_5842" s="T72">np:G</ta>
            <ta e="T80" id="Seg_5843" s="T79">np:P</ta>
            <ta e="T84" id="Seg_5844" s="T82">0.3:Th</ta>
            <ta e="T85" id="Seg_5845" s="T84">adv:Time</ta>
            <ta e="T86" id="Seg_5846" s="T85">np.h:Poss</ta>
            <ta e="T87" id="Seg_5847" s="T86">np.h:A</ta>
            <ta e="T89" id="Seg_5848" s="T88">np:G</ta>
            <ta e="T92" id="Seg_5849" s="T91">np:G</ta>
            <ta e="T93" id="Seg_5850" s="T92">adv:Time</ta>
            <ta e="T95" id="Seg_5851" s="T94">adv:Time</ta>
            <ta e="T99" id="Seg_5852" s="T96">0.3.h:A</ta>
            <ta e="T100" id="Seg_5853" s="T99">adv:Time</ta>
            <ta e="T102" id="Seg_5854" s="T101">np:Th</ta>
            <ta e="T108" id="Seg_5855" s="T107">np.h:A</ta>
            <ta e="T110" id="Seg_5856" s="T109">0.3.h:Poss np:P</ta>
            <ta e="T111" id="Seg_5857" s="T110">0.3.h:A</ta>
            <ta e="T118" id="Seg_5858" s="T117">pro.h:Poss</ta>
            <ta e="T120" id="Seg_5859" s="T119">np:Th</ta>
            <ta e="T121" id="Seg_5860" s="T120">adv:Time</ta>
            <ta e="T124" id="Seg_5861" s="T123">np.h:A</ta>
            <ta e="T125" id="Seg_5862" s="T124">0.3.h:Poss np.h:R</ta>
            <ta e="T127" id="Seg_5863" s="T126">pro.h:A</ta>
            <ta e="T132" id="Seg_5864" s="T131">pro.h:Th</ta>
            <ta e="T133" id="Seg_5865" s="T132">np.h:A</ta>
            <ta e="T136" id="Seg_5866" s="T135">0.2.h:A</ta>
            <ta e="T138" id="Seg_5867" s="T137">np:G</ta>
            <ta e="T139" id="Seg_5868" s="T138">0.2.h:A</ta>
            <ta e="T141" id="Seg_5869" s="T140">0.1.h:A</ta>
            <ta e="T142" id="Seg_5870" s="T141">np.h:E</ta>
            <ta e="T148" id="Seg_5871" s="T147">pro.h:A</ta>
            <ta e="T150" id="Seg_5872" s="T149">np:L</ta>
            <ta e="T155" id="Seg_5873" s="T154">np.h:A</ta>
            <ta e="T156" id="Seg_5874" s="T155">np:P</ta>
            <ta e="T158" id="Seg_5875" s="T157">0.3.h:Poss</ta>
            <ta e="T159" id="Seg_5876" s="T158">np:L</ta>
            <ta e="T161" id="Seg_5877" s="T160">np:G</ta>
            <ta e="T164" id="Seg_5878" s="T163">np.h:Th</ta>
            <ta e="T166" id="Seg_5879" s="T165">np:L</ta>
            <ta e="T171" id="Seg_5880" s="T170">0.3.h:A</ta>
            <ta e="T172" id="Seg_5881" s="T171">adv:Time</ta>
            <ta e="T173" id="Seg_5882" s="T172">0.3.h:Poss np:Th</ta>
            <ta e="T177" id="Seg_5883" s="T175">0.3.h:A</ta>
            <ta e="T178" id="Seg_5884" s="T177">pro:Th</ta>
            <ta e="T179" id="Seg_5885" s="T178">pro:Th</ta>
            <ta e="T182" id="Seg_5886" s="T181">np.h:Th</ta>
            <ta e="T183" id="Seg_5887" s="T182">0.3.h:Poss np.h:A</ta>
            <ta e="T188" id="Seg_5888" s="T187">0.2.h:A</ta>
            <ta e="T191" id="Seg_5889" s="T190">np.h:Poss</ta>
            <ta e="T193" id="Seg_5890" s="T192">np:Th</ta>
            <ta e="T196" id="Seg_5891" s="T195">np.h:A</ta>
            <ta e="T197" id="Seg_5892" s="T196">np:Poss</ta>
            <ta e="T198" id="Seg_5893" s="T197">np:Th</ta>
            <ta e="T203" id="Seg_5894" s="T202">np:L</ta>
            <ta e="T204" id="Seg_5895" s="T203">np:Th</ta>
            <ta e="T205" id="Seg_5896" s="T204">np:Th</ta>
            <ta e="T208" id="Seg_5897" s="T207">pro:Th</ta>
            <ta e="T215" id="Seg_5898" s="T214">np:L</ta>
            <ta e="T216" id="Seg_5899" s="T215">np:P</ta>
            <ta e="T217" id="Seg_5900" s="T216">pro:Cau</ta>
            <ta e="T221" id="Seg_5901" s="T220">np.h:Th</ta>
            <ta e="T223" id="Seg_5902" s="T222">np:L</ta>
            <ta e="T229" id="Seg_5903" s="T228">adv:L</ta>
            <ta e="T230" id="Seg_5904" s="T229">np:L</ta>
            <ta e="T233" id="Seg_5905" s="T232">np:Th</ta>
            <ta e="T235" id="Seg_5906" s="T234">np:Poss</ta>
            <ta e="T236" id="Seg_5907" s="T235">np:Th</ta>
            <ta e="T238" id="Seg_5908" s="T237">0.3:A</ta>
            <ta e="T239" id="Seg_5909" s="T238">np:So</ta>
            <ta e="T240" id="Seg_5910" s="T239">np:Th</ta>
            <ta e="T242" id="Seg_5911" s="T241">np:Th</ta>
            <ta e="T243" id="Seg_5912" s="T242">np:G</ta>
            <ta e="T244" id="Seg_5913" s="T243">0.3:A</ta>
            <ta e="T247" id="Seg_5914" s="T246">np:Th</ta>
            <ta e="T251" id="Seg_5915" s="T250">0.2.h:A</ta>
            <ta e="T252" id="Seg_5916" s="T251">np.h:A</ta>
            <ta e="T253" id="Seg_5917" s="T252">0.3.h:Poss np.h:Th</ta>
            <ta e="T256" id="Seg_5918" s="T255">np:L</ta>
            <ta e="T257" id="Seg_5919" s="T256">np:L</ta>
            <ta e="T258" id="Seg_5920" s="T257">np:Th</ta>
            <ta e="T263" id="Seg_5921" s="T261">pp:Time</ta>
            <ta e="T266" id="Seg_5922" s="T265">pro.h:Th</ta>
            <ta e="T267" id="Seg_5923" s="T266">pro.h:A</ta>
            <ta e="T279" id="Seg_5924" s="T277">0.3.h:E</ta>
            <ta e="T283" id="Seg_5925" s="T281">0.3.h:Th</ta>
            <ta e="T287" id="Seg_5926" s="T286">np:Cau</ta>
            <ta e="T289" id="Seg_5927" s="T288">0.3.h:Th</ta>
            <ta e="T290" id="Seg_5928" s="T289">np:L</ta>
            <ta e="T291" id="Seg_5929" s="T290">pro:Th</ta>
            <ta e="T297" id="Seg_5930" s="T296">np:Th</ta>
            <ta e="T298" id="Seg_5931" s="T297">np:Th</ta>
            <ta e="T300" id="Seg_5932" s="T299">np:Th</ta>
            <ta e="T302" id="Seg_5933" s="T301">np:G</ta>
            <ta e="T303" id="Seg_5934" s="T302">pro.h:A</ta>
            <ta e="T306" id="Seg_5935" s="T305">np:P</ta>
            <ta e="T308" id="Seg_5936" s="T307">0.2.h:A</ta>
            <ta e="T309" id="Seg_5937" s="T308">pro.h:A</ta>
            <ta e="T310" id="Seg_5938" s="T309">0.2.h:Poss np:Poss</ta>
            <ta e="T311" id="Seg_5939" s="T310">np:Th</ta>
            <ta e="T314" id="Seg_5940" s="T313">np:A</ta>
            <ta e="T315" id="Seg_5941" s="T314">0.3:A</ta>
            <ta e="T316" id="Seg_5942" s="T315">np:P</ta>
            <ta e="T320" id="Seg_5943" s="T319">np:P</ta>
            <ta e="T322" id="Seg_5944" s="T321">0.3:A</ta>
            <ta e="T323" id="Seg_5945" s="T322">np:P</ta>
            <ta e="T325" id="Seg_5946" s="T324">0.3:A</ta>
            <ta e="T326" id="Seg_5947" s="T325">np.h:E</ta>
            <ta e="T327" id="Seg_5948" s="T326">0.3.h:Poss np:Th</ta>
            <ta e="T328" id="Seg_5949" s="T327">0.3.h:A</ta>
            <ta e="T329" id="Seg_5950" s="T328">np.h:Th</ta>
            <ta e="T332" id="Seg_5951" s="T331">pro.h:A</ta>
            <ta e="T334" id="Seg_5952" s="T333">0.3.h:Poss np:P</ta>
            <ta e="T336" id="Seg_5953" s="T335">0.3.h:Poss np:P</ta>
            <ta e="T338" id="Seg_5954" s="T337">0.3.h:P</ta>
            <ta e="T339" id="Seg_5955" s="T338">0.3.h:Poss np:Ins</ta>
            <ta e="T342" id="Seg_5956" s="T341">np.h:E</ta>
            <ta e="T346" id="Seg_5957" s="T345">np:G</ta>
            <ta e="T348" id="Seg_5958" s="T347">np:Path</ta>
            <ta e="T352" id="Seg_5959" s="T351">np:A</ta>
            <ta e="T355" id="Seg_5960" s="T354">adv:Time</ta>
            <ta e="T356" id="Seg_5961" s="T355">np:Th</ta>
            <ta e="T358" id="Seg_5962" s="T357">0.3.h:A</ta>
            <ta e="T360" id="Seg_5963" s="T359">n:Time</ta>
            <ta e="T365" id="Seg_5964" s="T364">np:Poss</ta>
            <ta e="T368" id="Seg_5965" s="T367">0.3.h:P</ta>
            <ta e="T371" id="Seg_5966" s="T370">0.3.h:Poss np:Th</ta>
            <ta e="T374" id="Seg_5967" s="T373">0.3.h:A</ta>
            <ta e="T376" id="Seg_5968" s="T374">pp:Com</ta>
            <ta e="T377" id="Seg_5969" s="T376">np.h:E</ta>
            <ta e="T379" id="Seg_5970" s="T378">np.h:A</ta>
            <ta e="T383" id="Seg_5971" s="T382">np.h:A</ta>
            <ta e="T387" id="Seg_5972" s="T386">0.3.h:Poss np:Th</ta>
            <ta e="T390" id="Seg_5973" s="T389">pro.h:E</ta>
            <ta e="T394" id="Seg_5974" s="T393">0.1.h:A</ta>
            <ta e="T396" id="Seg_5975" s="T394">pp:Time</ta>
            <ta e="T397" id="Seg_5976" s="T396">pro:Th</ta>
            <ta e="T399" id="Seg_5977" s="T398">np:Th</ta>
            <ta e="T400" id="Seg_5978" s="T399">0.3.h:A</ta>
            <ta e="T401" id="Seg_5979" s="T400">np.h:E</ta>
            <ta e="T404" id="Seg_5980" s="T403">np:St</ta>
            <ta e="T406" id="Seg_5981" s="T405">pro.h:Th</ta>
            <ta e="T408" id="Seg_5982" s="T407">0.3.h:Poss np:Th</ta>
            <ta e="T412" id="Seg_5983" s="T411">np.h:R</ta>
            <ta e="T413" id="Seg_5984" s="T412">0.3.h:A</ta>
            <ta e="T414" id="Seg_5985" s="T413">adv:Time</ta>
            <ta e="T415" id="Seg_5986" s="T414">pro.h:E</ta>
            <ta e="T417" id="Seg_5987" s="T415">pp:Com</ta>
            <ta e="T418" id="Seg_5988" s="T417">np:Cau</ta>
            <ta e="T420" id="Seg_5989" s="T419">0.2.h:E</ta>
            <ta e="T424" id="Seg_5990" s="T423">0.2.h:E</ta>
            <ta e="T425" id="Seg_5991" s="T424">0.2.h:E</ta>
            <ta e="T426" id="Seg_5992" s="T425">pro.h:Poss</ta>
            <ta e="T427" id="Seg_5993" s="T426">np:Cau</ta>
            <ta e="T431" id="Seg_5994" s="T430">pro.h:P</ta>
            <ta e="T434" id="Seg_5995" s="T433">0.2.h:Poss np.h:A</ta>
            <ta e="T435" id="Seg_5996" s="T434">0.2.h:Poss np.h:A</ta>
            <ta e="T439" id="Seg_5997" s="T437">pp:Com</ta>
            <ta e="T440" id="Seg_5998" s="T439">0.2.h:Poss</ta>
            <ta e="T443" id="Seg_5999" s="T442">0.2.h:A</ta>
            <ta e="T444" id="Seg_6000" s="T443">np.h:A</ta>
            <ta e="T447" id="Seg_6001" s="T446">np.h:R</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T4" id="Seg_6002" s="T3">np.h:S</ta>
            <ta e="T10" id="Seg_6003" s="T7">s:temp</ta>
            <ta e="T12" id="Seg_6004" s="T11">v:pred</ta>
            <ta e="T13" id="Seg_6005" s="T12">pro.h:S</ta>
            <ta e="T19" id="Seg_6006" s="T18">v:pred</ta>
            <ta e="T24" id="Seg_6007" s="T23">np:S</ta>
            <ta e="T26" id="Seg_6008" s="T24">s:adv</ta>
            <ta e="T30" id="Seg_6009" s="T29">v:pred</ta>
            <ta e="T35" id="Seg_6010" s="T30">s:temp</ta>
            <ta e="T36" id="Seg_6011" s="T35">0.3:S v:pred</ta>
            <ta e="T42" id="Seg_6012" s="T41">np:S</ta>
            <ta e="T43" id="Seg_6013" s="T42">v:pred</ta>
            <ta e="T46" id="Seg_6014" s="T43">s:cond</ta>
            <ta e="T48" id="Seg_6015" s="T47">np:S</ta>
            <ta e="T50" id="Seg_6016" s="T49">v:pred</ta>
            <ta e="T55" id="Seg_6017" s="T52">s:temp</ta>
            <ta e="T56" id="Seg_6018" s="T55">s:adv</ta>
            <ta e="T57" id="Seg_6019" s="T56">np.h:O</ta>
            <ta e="T58" id="Seg_6020" s="T57">0.3:S v:pred</ta>
            <ta e="T60" id="Seg_6021" s="T59">0.3:S v:pred</ta>
            <ta e="T68" id="Seg_6022" s="T67">np:S</ta>
            <ta e="T69" id="Seg_6023" s="T68">ptcl:pred</ta>
            <ta e="T70" id="Seg_6024" s="T69">np.h:S</ta>
            <ta e="T77" id="Seg_6025" s="T75">v:pred</ta>
            <ta e="T82" id="Seg_6026" s="T77">s:comp</ta>
            <ta e="T84" id="Seg_6027" s="T82">0.3:S v:pred</ta>
            <ta e="T87" id="Seg_6028" s="T86">np.h:S</ta>
            <ta e="T90" id="Seg_6029" s="T87">s:adv</ta>
            <ta e="T91" id="Seg_6030" s="T90">v:pred</ta>
            <ta e="T99" id="Seg_6031" s="T96">0.3.h:S v:pred</ta>
            <ta e="T102" id="Seg_6032" s="T101">np:S</ta>
            <ta e="T103" id="Seg_6033" s="T102">adj:pred</ta>
            <ta e="T108" id="Seg_6034" s="T107">np.h:S</ta>
            <ta e="T114" id="Seg_6035" s="T108">s:temp</ta>
            <ta e="T116" id="Seg_6036" s="T115">v:pred</ta>
            <ta e="T120" id="Seg_6037" s="T119">np:S</ta>
            <ta e="T122" id="Seg_6038" s="T121">adj:pred</ta>
            <ta e="T124" id="Seg_6039" s="T123">np.h:S</ta>
            <ta e="T126" id="Seg_6040" s="T125">v:pred</ta>
            <ta e="T127" id="Seg_6041" s="T126">pro.h:S</ta>
            <ta e="T131" id="Seg_6042" s="T129">v:pred</ta>
            <ta e="T132" id="Seg_6043" s="T131">pro.h:O</ta>
            <ta e="T133" id="Seg_6044" s="T132">np.h:S</ta>
            <ta e="T134" id="Seg_6045" s="T133">v:pred</ta>
            <ta e="T136" id="Seg_6046" s="T135">0.2.h:S v:pred</ta>
            <ta e="T139" id="Seg_6047" s="T138">0.2.h:S v:pred</ta>
            <ta e="T141" id="Seg_6048" s="T140">0.1.h:S v:pred</ta>
            <ta e="T142" id="Seg_6049" s="T141">np.h:S</ta>
            <ta e="T146" id="Seg_6050" s="T145">v:pred</ta>
            <ta e="T148" id="Seg_6051" s="T147">pro.h:S</ta>
            <ta e="T154" id="Seg_6052" s="T153">v:pred</ta>
            <ta e="T155" id="Seg_6053" s="T154">np.h:S</ta>
            <ta e="T157" id="Seg_6054" s="T155">s:temp</ta>
            <ta e="T162" id="Seg_6055" s="T157">s:adv</ta>
            <ta e="T163" id="Seg_6056" s="T162">v:pred</ta>
            <ta e="T164" id="Seg_6057" s="T163">np.h:S</ta>
            <ta e="T167" id="Seg_6058" s="T166">s:adv</ta>
            <ta e="T168" id="Seg_6059" s="T167">v:pred</ta>
            <ta e="T170" id="Seg_6060" s="T168">s:adv</ta>
            <ta e="T171" id="Seg_6061" s="T170">0.3.h:S v:pred</ta>
            <ta e="T174" id="Seg_6062" s="T172">s:temp</ta>
            <ta e="T175" id="Seg_6063" s="T174">s:temp</ta>
            <ta e="T177" id="Seg_6064" s="T175">0.3.h:S v:pred</ta>
            <ta e="T178" id="Seg_6065" s="T177">pro:S</ta>
            <ta e="T179" id="Seg_6066" s="T178">pro:S</ta>
            <ta e="T180" id="Seg_6067" s="T179">v:pred</ta>
            <ta e="T182" id="Seg_6068" s="T181">np.h:S</ta>
            <ta e="T185" id="Seg_6069" s="T182">s:adv</ta>
            <ta e="T186" id="Seg_6070" s="T185">v:pred</ta>
            <ta e="T188" id="Seg_6071" s="T187">0.2.h:S v:pred</ta>
            <ta e="T192" id="Seg_6072" s="T191">s:adv</ta>
            <ta e="T193" id="Seg_6073" s="T192">np:S</ta>
            <ta e="T195" id="Seg_6074" s="T194">v:pred</ta>
            <ta e="T196" id="Seg_6075" s="T195">np.h:S</ta>
            <ta e="T199" id="Seg_6076" s="T196">s:adv</ta>
            <ta e="T200" id="Seg_6077" s="T199">v:pred</ta>
            <ta e="T204" id="Seg_6078" s="T203">np:S</ta>
            <ta e="T205" id="Seg_6079" s="T204">np:S</ta>
            <ta e="T206" id="Seg_6080" s="T205">v:pred</ta>
            <ta e="T207" id="Seg_6081" s="T206">adj:pred</ta>
            <ta e="T208" id="Seg_6082" s="T207">pro:S</ta>
            <ta e="T210" id="Seg_6083" s="T209">v:pred</ta>
            <ta e="T213" id="Seg_6084" s="T211">s:rel</ta>
            <ta e="T217" id="Seg_6085" s="T216">pro:S</ta>
            <ta e="T220" id="Seg_6086" s="T219">v:pred</ta>
            <ta e="T221" id="Seg_6087" s="T220">np.h:S</ta>
            <ta e="T225" id="Seg_6088" s="T221">s:temp</ta>
            <ta e="T226" id="Seg_6089" s="T225">s:adv</ta>
            <ta e="T227" id="Seg_6090" s="T226">s:adv</ta>
            <ta e="T228" id="Seg_6091" s="T227">v:pred</ta>
            <ta e="T233" id="Seg_6092" s="T232">np:S</ta>
            <ta e="T234" id="Seg_6093" s="T233">v:pred</ta>
            <ta e="T236" id="Seg_6094" s="T235">np:O</ta>
            <ta e="T238" id="Seg_6095" s="T237">0.3:S v:pred</ta>
            <ta e="T240" id="Seg_6096" s="T239">np:O</ta>
            <ta e="T242" id="Seg_6097" s="T241">np:O</ta>
            <ta e="T244" id="Seg_6098" s="T243">0.3:S v:pred</ta>
            <ta e="T247" id="Seg_6099" s="T246">np:S</ta>
            <ta e="T248" id="Seg_6100" s="T247">v:pred</ta>
            <ta e="T250" id="Seg_6101" s="T249">s:adv</ta>
            <ta e="T251" id="Seg_6102" s="T250">0.2.h:S v:pred</ta>
            <ta e="T252" id="Seg_6103" s="T251">np.h:S</ta>
            <ta e="T254" id="Seg_6104" s="T253">v:pred</ta>
            <ta e="T258" id="Seg_6105" s="T257">np:S</ta>
            <ta e="T261" id="Seg_6106" s="T260">v:pred</ta>
            <ta e="T265" id="Seg_6107" s="T264">adj:pred</ta>
            <ta e="T266" id="Seg_6108" s="T265">pro.h:O</ta>
            <ta e="T267" id="Seg_6109" s="T266">pro.h:S</ta>
            <ta e="T268" id="Seg_6110" s="T267">s:adv</ta>
            <ta e="T269" id="Seg_6111" s="T268">v:pred</ta>
            <ta e="T279" id="Seg_6112" s="T276">s:adv</ta>
            <ta e="T281" id="Seg_6113" s="T279">s:adv</ta>
            <ta e="T283" id="Seg_6114" s="T281">0.3.h:S v:pred</ta>
            <ta e="T289" id="Seg_6115" s="T288">0.3.h:S v:pred</ta>
            <ta e="T291" id="Seg_6116" s="T290">pro:S</ta>
            <ta e="T296" id="Seg_6117" s="T294">v:pred</ta>
            <ta e="T297" id="Seg_6118" s="T296">np:S</ta>
            <ta e="T298" id="Seg_6119" s="T297">np:S</ta>
            <ta e="T299" id="Seg_6120" s="T298">v:pred</ta>
            <ta e="T300" id="Seg_6121" s="T299">np:S</ta>
            <ta e="T301" id="Seg_6122" s="T300">v:pred</ta>
            <ta e="T303" id="Seg_6123" s="T302">pro.h:S</ta>
            <ta e="T305" id="Seg_6124" s="T304">s:temp</ta>
            <ta e="T306" id="Seg_6125" s="T305">np:O</ta>
            <ta e="T307" id="Seg_6126" s="T306">v:pred</ta>
            <ta e="T308" id="Seg_6127" s="T307">0.2.h:S v:pred</ta>
            <ta e="T309" id="Seg_6128" s="T308">pro.h:S</ta>
            <ta e="T311" id="Seg_6129" s="T310">np:O</ta>
            <ta e="T313" id="Seg_6130" s="T312">v:pred</ta>
            <ta e="T314" id="Seg_6131" s="T313">np:S</ta>
            <ta e="T315" id="Seg_6132" s="T314">s:adv</ta>
            <ta e="T316" id="Seg_6133" s="T315">np:O</ta>
            <ta e="T318" id="Seg_6134" s="T317">v:pred</ta>
            <ta e="T320" id="Seg_6135" s="T319">np:O</ta>
            <ta e="T322" id="Seg_6136" s="T321">0.3:S v:pred</ta>
            <ta e="T323" id="Seg_6137" s="T322">np:O</ta>
            <ta e="T325" id="Seg_6138" s="T324">0.3:S v:pred</ta>
            <ta e="T326" id="Seg_6139" s="T325">np.h:S</ta>
            <ta e="T328" id="Seg_6140" s="T326">s:adv</ta>
            <ta e="T330" id="Seg_6141" s="T328">s:comp</ta>
            <ta e="T331" id="Seg_6142" s="T330">v:pred</ta>
            <ta e="T332" id="Seg_6143" s="T331">pro.h:S</ta>
            <ta e="T333" id="Seg_6144" s="T332">s:adv</ta>
            <ta e="T338" id="Seg_6145" s="T333">s:comp</ta>
            <ta e="T341" id="Seg_6146" s="T339">v:pred</ta>
            <ta e="T342" id="Seg_6147" s="T341">np.h:S</ta>
            <ta e="T350" id="Seg_6148" s="T342">s:temp</ta>
            <ta e="T353" id="Seg_6149" s="T350">s:comp</ta>
            <ta e="T354" id="Seg_6150" s="T353">v:pred</ta>
            <ta e="T356" id="Seg_6151" s="T355">np:O</ta>
            <ta e="T358" id="Seg_6152" s="T357">0.3.h:S v:pred</ta>
            <ta e="T362" id="Seg_6153" s="T358">s:adv</ta>
            <ta e="T364" id="Seg_6154" s="T362">s:adv</ta>
            <ta e="T368" id="Seg_6155" s="T367">0.3.h:S v:pred</ta>
            <ta e="T370" id="Seg_6156" s="T368">s:adv</ta>
            <ta e="T371" id="Seg_6157" s="T370">np:O</ta>
            <ta e="T374" id="Seg_6158" s="T373">0.3.h:S v:pred</ta>
            <ta e="T377" id="Seg_6159" s="T376">np.h:S</ta>
            <ta e="T380" id="Seg_6160" s="T377">s:temp</ta>
            <ta e="T382" id="Seg_6161" s="T381">v:pred</ta>
            <ta e="T383" id="Seg_6162" s="T382">np.h:S</ta>
            <ta e="T386" id="Seg_6163" s="T383">s:adv</ta>
            <ta e="T388" id="Seg_6164" s="T386">s:adv</ta>
            <ta e="T389" id="Seg_6165" s="T388">v:pred</ta>
            <ta e="T392" id="Seg_6166" s="T391">v:pred</ta>
            <ta e="T394" id="Seg_6167" s="T393">0.1.h:S v:pred</ta>
            <ta e="T398" id="Seg_6168" s="T394">s:rel</ta>
            <ta e="T399" id="Seg_6169" s="T398">np:O</ta>
            <ta e="T400" id="Seg_6170" s="T399">0.3.h:S v:pred</ta>
            <ta e="T401" id="Seg_6171" s="T400">np.h:S</ta>
            <ta e="T405" id="Seg_6172" s="T401">s:temp</ta>
            <ta e="T406" id="Seg_6173" s="T405">pro.h:O</ta>
            <ta e="T407" id="Seg_6174" s="T406">v:pred</ta>
            <ta e="T408" id="Seg_6175" s="T407">np:S</ta>
            <ta e="T410" id="Seg_6176" s="T409">v:pred</ta>
            <ta e="T411" id="Seg_6177" s="T410">s:adv</ta>
            <ta e="T413" id="Seg_6178" s="T412">0.3.h:S v:pred</ta>
            <ta e="T415" id="Seg_6179" s="T414">pro.h:O</ta>
            <ta e="T418" id="Seg_6180" s="T417">np:S</ta>
            <ta e="T419" id="Seg_6181" s="T418">v:pred</ta>
            <ta e="T420" id="Seg_6182" s="T419">0.2.h:S v:pred</ta>
            <ta e="T425" id="Seg_6183" s="T421">s:adv</ta>
            <ta e="T431" id="Seg_6184" s="T430">pro.h:S</ta>
            <ta e="T433" id="Seg_6185" s="T432">v:pred</ta>
            <ta e="T437" id="Seg_6186" s="T433">s:temp</ta>
            <ta e="T443" id="Seg_6187" s="T442">0.2.h:S v:pred</ta>
            <ta e="T444" id="Seg_6188" s="T443">np.h:S</ta>
            <ta e="T446" id="Seg_6189" s="T444">v:pred</ta>
            <ta e="T447" id="Seg_6190" s="T446">np.h:O</ta>
            <ta e="T448" id="Seg_6191" s="T447">v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T4" id="Seg_6192" s="T3">new</ta>
            <ta e="T5" id="Seg_6193" s="T4">accs-inf</ta>
            <ta e="T7" id="Seg_6194" s="T6">accs-inf</ta>
            <ta e="T8" id="Seg_6195" s="T7">accs-inf</ta>
            <ta e="T11" id="Seg_6196" s="T10">accs-inf</ta>
            <ta e="T13" id="Seg_6197" s="T12">giv-active</ta>
            <ta e="T18" id="Seg_6198" s="T17">new</ta>
            <ta e="T22" id="Seg_6199" s="T21">giv-inactive</ta>
            <ta e="T23" id="Seg_6200" s="T22">accs-inf</ta>
            <ta e="T24" id="Seg_6201" s="T23">accs-inf</ta>
            <ta e="T32" id="Seg_6202" s="T31">accs-inf</ta>
            <ta e="T33" id="Seg_6203" s="T32">accs-inf</ta>
            <ta e="T36" id="Seg_6204" s="T35">new</ta>
            <ta e="T38" id="Seg_6205" s="T37">giv-active</ta>
            <ta e="T48" id="Seg_6206" s="T47">giv-active</ta>
            <ta e="T57" id="Seg_6207" s="T56">accs-aggr</ta>
            <ta e="T62" id="Seg_6208" s="T61">new</ta>
            <ta e="T64" id="Seg_6209" s="T63">accs-inf</ta>
            <ta e="T70" id="Seg_6210" s="T69">giv-inactive</ta>
            <ta e="T73" id="Seg_6211" s="T72">new</ta>
            <ta e="T80" id="Seg_6212" s="T79">giv-active</ta>
            <ta e="T84" id="Seg_6213" s="T82">0.giv-active</ta>
            <ta e="T86" id="Seg_6214" s="T85">giv-active</ta>
            <ta e="T87" id="Seg_6215" s="T86">giv-active</ta>
            <ta e="T89" id="Seg_6216" s="T88">new</ta>
            <ta e="T92" id="Seg_6217" s="T91">giv-inactive</ta>
            <ta e="T99" id="Seg_6218" s="T96">0.giv-active</ta>
            <ta e="T108" id="Seg_6219" s="T107">giv-inactive</ta>
            <ta e="T110" id="Seg_6220" s="T109">new</ta>
            <ta e="T111" id="Seg_6221" s="T110">0.giv-active</ta>
            <ta e="T118" id="Seg_6222" s="T117">giv-active-Q</ta>
            <ta e="T120" id="Seg_6223" s="T119">giv-inactive-Q</ta>
            <ta e="T124" id="Seg_6224" s="T123">giv-active</ta>
            <ta e="T125" id="Seg_6225" s="T124">giv-active</ta>
            <ta e="T126" id="Seg_6226" s="T125">quot-sp</ta>
            <ta e="T127" id="Seg_6227" s="T126">giv-active-Q</ta>
            <ta e="T132" id="Seg_6228" s="T131">giv-active</ta>
            <ta e="T133" id="Seg_6229" s="T132">giv-active</ta>
            <ta e="T134" id="Seg_6230" s="T133">quot-sp</ta>
            <ta e="T136" id="Seg_6231" s="T135">0.giv-active-Q</ta>
            <ta e="T138" id="Seg_6232" s="T137">accs-inf-Q</ta>
            <ta e="T139" id="Seg_6233" s="T138">0.giv-active-Q</ta>
            <ta e="T141" id="Seg_6234" s="T140">0.giv-active-Q</ta>
            <ta e="T142" id="Seg_6235" s="T141">giv-active</ta>
            <ta e="T148" id="Seg_6236" s="T147">giv-active</ta>
            <ta e="T150" id="Seg_6237" s="T149">giv-inactive</ta>
            <ta e="T151" id="Seg_6238" s="T150">giv-inactive</ta>
            <ta e="T155" id="Seg_6239" s="T154">giv-active</ta>
            <ta e="T156" id="Seg_6240" s="T155">accs-sit</ta>
            <ta e="T158" id="Seg_6241" s="T157">giv-active</ta>
            <ta e="T160" id="Seg_6242" s="T159">accs-inf</ta>
            <ta e="T164" id="Seg_6243" s="T163">giv-active</ta>
            <ta e="T165" id="Seg_6244" s="T164">giv-active</ta>
            <ta e="T171" id="Seg_6245" s="T170">0.giv-active</ta>
            <ta e="T173" id="Seg_6246" s="T172">accs-inf</ta>
            <ta e="T177" id="Seg_6247" s="T175">0.giv-active</ta>
            <ta e="T182" id="Seg_6248" s="T181">giv-inactive</ta>
            <ta e="T183" id="Seg_6249" s="T182">giv-inactive</ta>
            <ta e="T184" id="Seg_6250" s="T183">accs-inf</ta>
            <ta e="T187" id="Seg_6251" s="T186">giv-active-Q</ta>
            <ta e="T188" id="Seg_6252" s="T187">0.giv-active-Q</ta>
            <ta e="T191" id="Seg_6253" s="T190">giv-active</ta>
            <ta e="T193" id="Seg_6254" s="T192">accs-inf</ta>
            <ta e="T196" id="Seg_6255" s="T195">giv-active</ta>
            <ta e="T197" id="Seg_6256" s="T196">giv-inactive</ta>
            <ta e="T203" id="Seg_6257" s="T202">accs-inf</ta>
            <ta e="T204" id="Seg_6258" s="T203">new</ta>
            <ta e="T205" id="Seg_6259" s="T204">new</ta>
            <ta e="T214" id="Seg_6260" s="T213">giv-inactive</ta>
            <ta e="T216" id="Seg_6261" s="T215">new</ta>
            <ta e="T221" id="Seg_6262" s="T220">giv-inactive</ta>
            <ta e="T222" id="Seg_6263" s="T221">giv-inactive</ta>
            <ta e="T230" id="Seg_6264" s="T229">giv-inactive</ta>
            <ta e="T235" id="Seg_6265" s="T234">giv-inactive</ta>
            <ta e="T236" id="Seg_6266" s="T235">giv-inactive</ta>
            <ta e="T239" id="Seg_6267" s="T238">accs-inf</ta>
            <ta e="T240" id="Seg_6268" s="T239">new</ta>
            <ta e="T242" id="Seg_6269" s="T241">new</ta>
            <ta e="T243" id="Seg_6270" s="T242">accs-inf</ta>
            <ta e="T246" id="Seg_6271" s="T245">accs-aggr</ta>
            <ta e="T251" id="Seg_6272" s="T250">0.giv-inactive-Q</ta>
            <ta e="T252" id="Seg_6273" s="T251">giv-inactive</ta>
            <ta e="T253" id="Seg_6274" s="T252">giv-active</ta>
            <ta e="T254" id="Seg_6275" s="T253">quot-sp</ta>
            <ta e="T255" id="Seg_6276" s="T254">giv-inactive</ta>
            <ta e="T257" id="Seg_6277" s="T256">giv-inactive</ta>
            <ta e="T258" id="Seg_6278" s="T257">accs-sit</ta>
            <ta e="T266" id="Seg_6279" s="T265">giv-inactive</ta>
            <ta e="T270" id="Seg_6280" s="T269">giv-active</ta>
            <ta e="T279" id="Seg_6281" s="T277">0.giv-active</ta>
            <ta e="T283" id="Seg_6282" s="T281">0.giv-active</ta>
            <ta e="T287" id="Seg_6283" s="T286">new</ta>
            <ta e="T289" id="Seg_6284" s="T288">0.giv-active</ta>
            <ta e="T290" id="Seg_6285" s="T289">giv-inactive</ta>
            <ta e="T297" id="Seg_6286" s="T296">new</ta>
            <ta e="T298" id="Seg_6287" s="T297">new</ta>
            <ta e="T300" id="Seg_6288" s="T299">giv-inactive</ta>
            <ta e="T302" id="Seg_6289" s="T301">giv-inactive</ta>
            <ta e="T303" id="Seg_6290" s="T302">new</ta>
            <ta e="T306" id="Seg_6291" s="T305">giv-inactive</ta>
            <ta e="T308" id="Seg_6292" s="T307">0.giv-inactive-Q</ta>
            <ta e="T309" id="Seg_6293" s="T308">giv-active-Q</ta>
            <ta e="T310" id="Seg_6294" s="T309">giv-inactive-Q</ta>
            <ta e="T311" id="Seg_6295" s="T310">giv-inactive-Q</ta>
            <ta e="T314" id="Seg_6296" s="T313">new-Q</ta>
            <ta e="T316" id="Seg_6297" s="T315">accs-inf-Q</ta>
            <ta e="T319" id="Seg_6298" s="T318">giv-active-Q</ta>
            <ta e="T320" id="Seg_6299" s="T319">accs-inf-Q</ta>
            <ta e="T322" id="Seg_6300" s="T321">0.giv-active-Q</ta>
            <ta e="T323" id="Seg_6301" s="T322">giv-active-Q</ta>
            <ta e="T325" id="Seg_6302" s="T324">0.giv-active-Q</ta>
            <ta e="T326" id="Seg_6303" s="T325">giv-inactive</ta>
            <ta e="T327" id="Seg_6304" s="T326">giv-inactive</ta>
            <ta e="T329" id="Seg_6305" s="T328">giv-inactive</ta>
            <ta e="T332" id="Seg_6306" s="T331">giv-active</ta>
            <ta e="T334" id="Seg_6307" s="T333">accs-inf</ta>
            <ta e="T336" id="Seg_6308" s="T335">accs-inf</ta>
            <ta e="T337" id="Seg_6309" s="T336">accs-sit</ta>
            <ta e="T338" id="Seg_6310" s="T337">0.giv-active</ta>
            <ta e="T339" id="Seg_6311" s="T338">accs-inf</ta>
            <ta e="T342" id="Seg_6312" s="T341">giv-active</ta>
            <ta e="T343" id="Seg_6313" s="T342">new</ta>
            <ta e="T344" id="Seg_6314" s="T343">new</ta>
            <ta e="T346" id="Seg_6315" s="T345">accs-inf</ta>
            <ta e="T347" id="Seg_6316" s="T346">giv-inactive</ta>
            <ta e="T351" id="Seg_6317" s="T350">giv-inactive</ta>
            <ta e="T352" id="Seg_6318" s="T351">giv-inactive</ta>
            <ta e="T356" id="Seg_6319" s="T355">giv-active</ta>
            <ta e="T358" id="Seg_6320" s="T357">0.giv-active</ta>
            <ta e="T365" id="Seg_6321" s="T364">giv-active</ta>
            <ta e="T366" id="Seg_6322" s="T365">giv-inactive</ta>
            <ta e="T368" id="Seg_6323" s="T367">0.giv-active</ta>
            <ta e="T371" id="Seg_6324" s="T370">giv-inactive</ta>
            <ta e="T374" id="Seg_6325" s="T373">0.giv-active</ta>
            <ta e="T375" id="Seg_6326" s="T374">giv-inactive</ta>
            <ta e="T377" id="Seg_6327" s="T376">giv-inactive</ta>
            <ta e="T379" id="Seg_6328" s="T378">giv-active</ta>
            <ta e="T383" id="Seg_6329" s="T382">giv-active</ta>
            <ta e="T385" id="Seg_6330" s="T384">giv-active</ta>
            <ta e="T387" id="Seg_6331" s="T386">giv-active</ta>
            <ta e="T390" id="Seg_6332" s="T389">giv-inactive-Q</ta>
            <ta e="T394" id="Seg_6333" s="T393">0.giv-active-Q</ta>
            <ta e="T400" id="Seg_6334" s="T399">0.giv-active</ta>
            <ta e="T401" id="Seg_6335" s="T400">giv-inactive</ta>
            <ta e="T402" id="Seg_6336" s="T401">giv-active</ta>
            <ta e="T406" id="Seg_6337" s="T405">giv-active</ta>
            <ta e="T408" id="Seg_6338" s="T407">accs-sit</ta>
            <ta e="T412" id="Seg_6339" s="T411">giv-inactive</ta>
            <ta e="T413" id="Seg_6340" s="T412">0.giv-active 0.quot-sp</ta>
            <ta e="T415" id="Seg_6341" s="T414">giv-active-Q</ta>
            <ta e="T416" id="Seg_6342" s="T415">giv-inactive-Q</ta>
            <ta e="T418" id="Seg_6343" s="T417">new-Q</ta>
            <ta e="T420" id="Seg_6344" s="T419">0.giv-active-Q</ta>
            <ta e="T424" id="Seg_6345" s="T423">0.giv-active-Q</ta>
            <ta e="T425" id="Seg_6346" s="T424">0.giv-active-Q</ta>
            <ta e="T426" id="Seg_6347" s="T425">giv-active-Q</ta>
            <ta e="T427" id="Seg_6348" s="T426">accs-sit-Q</ta>
            <ta e="T431" id="Seg_6349" s="T430">giv-inactive-Q</ta>
            <ta e="T434" id="Seg_6350" s="T433">giv-inactive-Q</ta>
            <ta e="T435" id="Seg_6351" s="T434">giv-inactive-Q</ta>
            <ta e="T438" id="Seg_6352" s="T437">giv-inactive-Q</ta>
            <ta e="T440" id="Seg_6353" s="T439">giv-inactive-Q</ta>
            <ta e="T443" id="Seg_6354" s="T442">0.giv-active-Q</ta>
            <ta e="T444" id="Seg_6355" s="T443">giv-active</ta>
            <ta e="T447" id="Seg_6356" s="T446">giv-active</ta>
         </annotation>
         <annotation name="Top" tierref="Top">
            <ta e="T12" id="Seg_6357" s="T11">0.top.int.abstr</ta>
            <ta e="T13" id="Seg_6358" s="T12">top.int.concr</ta>
            <ta e="T24" id="Seg_6359" s="T19">top.int.concr</ta>
            <ta e="T35" id="Seg_6360" s="T30">top.int.concr</ta>
            <ta e="T39" id="Seg_6361" s="T37">top.int.concr</ta>
            <ta e="T52" id="Seg_6362" s="T50">top.int.concr</ta>
            <ta e="T64" id="Seg_6363" s="T61">top.int.concr</ta>
            <ta e="T70" id="Seg_6364" s="T69">top.int.concr</ta>
            <ta e="T84" id="Seg_6365" s="T82">0.top.int.concr</ta>
            <ta e="T85" id="Seg_6366" s="T84">top.int.concr</ta>
            <ta e="T92" id="Seg_6367" s="T91">top.int.concr</ta>
            <ta e="T100" id="Seg_6368" s="T99">top.int.concr</ta>
            <ta e="T108" id="Seg_6369" s="T106">top.int.concr</ta>
            <ta e="T120" id="Seg_6370" s="T117">top.int.concr</ta>
            <ta e="T124" id="Seg_6371" s="T123">top.int.concr</ta>
            <ta e="T127" id="Seg_6372" s="T126">top.int.concr</ta>
            <ta e="T132" id="Seg_6373" s="T131">top.int.concr</ta>
            <ta e="T141" id="Seg_6374" s="T140">0.top.int.concr</ta>
            <ta e="T142" id="Seg_6375" s="T141">top.int.concr</ta>
            <ta e="T148" id="Seg_6376" s="T147">top.int.concr</ta>
            <ta e="T155" id="Seg_6377" s="T154">top.int.concr</ta>
            <ta e="T164" id="Seg_6378" s="T163">top.int.concr</ta>
            <ta e="T171" id="Seg_6379" s="T170">0.top.int.concr</ta>
            <ta e="T172" id="Seg_6380" s="T171">top.int.concr</ta>
            <ta e="T180" id="Seg_6381" s="T179">0.top.int.abstr</ta>
            <ta e="T182" id="Seg_6382" s="T181">top.int.concr</ta>
            <ta e="T191" id="Seg_6383" s="T190">top.int.concr</ta>
            <ta e="T196" id="Seg_6384" s="T195">top.int.concr</ta>
            <ta e="T206" id="Seg_6385" s="T205">0.top.int.abstr</ta>
            <ta e="T207" id="Seg_6386" s="T206">0.top.int.abstr</ta>
            <ta e="T210" id="Seg_6387" s="T209">0.top.int.abstr</ta>
            <ta e="T215" id="Seg_6388" s="T211">top.int.concr</ta>
            <ta e="T221" id="Seg_6389" s="T220">top.int.concr</ta>
            <ta e="T230" id="Seg_6390" s="T228">top.int.concr</ta>
            <ta e="T239" id="Seg_6391" s="T238">top.int.concr</ta>
            <ta e="T245" id="Seg_6392" s="T244">top.int.concr</ta>
            <ta e="T252" id="Seg_6393" s="T251">top.int.concr</ta>
            <ta e="T256" id="Seg_6394" s="T254">top.int.concr</ta>
            <ta e="T257" id="Seg_6395" s="T256">top.int.concr</ta>
            <ta e="T263" id="Seg_6396" s="T261">top.int.concr</ta>
            <ta e="T266" id="Seg_6397" s="T265">top.int.concr</ta>
            <ta e="T283" id="Seg_6398" s="T281">0.top.int.concr</ta>
            <ta e="T289" id="Seg_6399" s="T288">0.top.int.concr</ta>
            <ta e="T290" id="Seg_6400" s="T289">top.int.concr</ta>
            <ta e="T299" id="Seg_6401" s="T298">0.top.int.abstr</ta>
            <ta e="T300" id="Seg_6402" s="T299">top.int.concr</ta>
            <ta e="T302" id="Seg_6403" s="T301">top.int.concr</ta>
            <ta e="T318" id="Seg_6404" s="T317">0.top.int.abstr</ta>
            <ta e="T322" id="Seg_6405" s="T321">0.top.int.concr</ta>
            <ta e="T325" id="Seg_6406" s="T324">0.top.int.concr</ta>
            <ta e="T326" id="Seg_6407" s="T325">top.int.concr</ta>
            <ta e="T332" id="Seg_6408" s="T331">top.int.concr</ta>
            <ta e="T342" id="Seg_6409" s="T341">top.int.concr</ta>
            <ta e="T355" id="Seg_6410" s="T354">top.int.concr</ta>
            <ta e="T360" id="Seg_6411" s="T358">top.int.concr</ta>
            <ta e="T374" id="Seg_6412" s="T373">0.top.int.concr</ta>
            <ta e="T377" id="Seg_6413" s="T374">top.int.concr</ta>
            <ta e="T383" id="Seg_6414" s="T382">top.int.concr</ta>
            <ta e="T390" id="Seg_6415" s="T389">top.int.concr</ta>
            <ta e="T394" id="Seg_6416" s="T393">0.top.int.concr</ta>
            <ta e="T400" id="Seg_6417" s="T399">0.top.int.concr</ta>
            <ta e="T401" id="Seg_6418" s="T400">top.int.concr</ta>
            <ta e="T408" id="Seg_6419" s="T407">top.int.concr</ta>
            <ta e="T413" id="Seg_6420" s="T412">0.top.int.concr</ta>
            <ta e="T414" id="Seg_6421" s="T413">top.int.concr</ta>
            <ta e="T427" id="Seg_6422" s="T425">top.int.concr</ta>
            <ta e="T444" id="Seg_6423" s="T443">top.int.concr</ta>
         </annotation>
         <annotation name="Foc" tierref="Foc">
            <ta e="T12" id="Seg_6424" s="T1">foc.wid</ta>
            <ta e="T19" id="Seg_6425" s="T13">foc.int</ta>
            <ta e="T30" id="Seg_6426" s="T24">foc.int</ta>
            <ta e="T37" id="Seg_6427" s="T35">foc.int</ta>
            <ta e="T49" id="Seg_6428" s="T48">foc.nar</ta>
            <ta e="T61" id="Seg_6429" s="T55">foc.int</ta>
            <ta e="T69" id="Seg_6430" s="T64">foc.int</ta>
            <ta e="T77" id="Seg_6431" s="T70">foc.int</ta>
            <ta e="T84" id="Seg_6432" s="T77">foc.int</ta>
            <ta e="T91" id="Seg_6433" s="T85">foc.int</ta>
            <ta e="T93" id="Seg_6434" s="T92">foc.nar</ta>
            <ta e="T95" id="Seg_6435" s="T94">foc.nar</ta>
            <ta e="T103" id="Seg_6436" s="T102">foc.nar</ta>
            <ta e="T116" id="Seg_6437" s="T108">foc.int</ta>
            <ta e="T123" id="Seg_6438" s="T120">foc.int</ta>
            <ta e="T126" id="Seg_6439" s="T124">foc.int</ta>
            <ta e="T131" id="Seg_6440" s="T127">foc.int</ta>
            <ta e="T134" id="Seg_6441" s="T132">foc.int</ta>
            <ta e="T136" id="Seg_6442" s="T135">foc.contr</ta>
            <ta e="T139" id="Seg_6443" s="T137">foc.int</ta>
            <ta e="T140" id="Seg_6444" s="T139">foc.nar</ta>
            <ta e="T147" id="Seg_6445" s="T142">foc.int</ta>
            <ta e="T153" id="Seg_6446" s="T152">foc.nar</ta>
            <ta e="T163" id="Seg_6447" s="T155">foc.int</ta>
            <ta e="T168" id="Seg_6448" s="T164">foc.int</ta>
            <ta e="T171" id="Seg_6449" s="T168">foc.int</ta>
            <ta e="T177" id="Seg_6450" s="T172">foc.int</ta>
            <ta e="T181" id="Seg_6451" s="T177">foc.wid</ta>
            <ta e="T186" id="Seg_6452" s="T182">foc.int</ta>
            <ta e="T188" id="Seg_6453" s="T187">foc.int</ta>
            <ta e="T195" id="Seg_6454" s="T191">foc.int</ta>
            <ta e="T200" id="Seg_6455" s="T196">foc.int</ta>
            <ta e="T206" id="Seg_6456" s="T201">foc.wid</ta>
            <ta e="T207" id="Seg_6457" s="T206">foc.wid</ta>
            <ta e="T210" id="Seg_6458" s="T207">foc.wid</ta>
            <ta e="T220" id="Seg_6459" s="T216">foc.int</ta>
            <ta e="T228" id="Seg_6460" s="T221">foc.int</ta>
            <ta e="T234" id="Seg_6461" s="T230">foc.int</ta>
            <ta e="T238" id="Seg_6462" s="T234">foc.int</ta>
            <ta e="T244" id="Seg_6463" s="T239">foc.int</ta>
            <ta e="T248" id="Seg_6464" s="T245">foc.int</ta>
            <ta e="T251" id="Seg_6465" s="T249">foc.int</ta>
            <ta e="T254" id="Seg_6466" s="T252">foc.int</ta>
            <ta e="T261" id="Seg_6467" s="T258">foc.int</ta>
            <ta e="T265" id="Seg_6468" s="T264">foc.nar</ta>
            <ta e="T267" id="Seg_6469" s="T266">foc.nar</ta>
            <ta e="T283" id="Seg_6470" s="T276">foc.int</ta>
            <ta e="T288" id="Seg_6471" s="T287">foc.nar</ta>
            <ta e="T296" id="Seg_6472" s="T290">foc.int</ta>
            <ta e="T299" id="Seg_6473" s="T296">foc.wid</ta>
            <ta e="T301" id="Seg_6474" s="T300">foc.int</ta>
            <ta e="T307" id="Seg_6475" s="T302">foc.int</ta>
            <ta e="T308" id="Seg_6476" s="T307">foc.int</ta>
            <ta e="T312" id="Seg_6477" s="T311">foc.nar</ta>
            <ta e="T318" id="Seg_6478" s="T313">foc.wid</ta>
            <ta e="T322" id="Seg_6479" s="T318">foc.int</ta>
            <ta e="T325" id="Seg_6480" s="T322">foc.int</ta>
            <ta e="T331" id="Seg_6481" s="T326">foc.int</ta>
            <ta e="T341" id="Seg_6482" s="T332">foc.int</ta>
            <ta e="T354" id="Seg_6483" s="T350">foc.int</ta>
            <ta e="T358" id="Seg_6484" s="T356">foc.int</ta>
            <ta e="T368" id="Seg_6485" s="T360">foc.int</ta>
            <ta e="T374" id="Seg_6486" s="T368">foc.int</ta>
            <ta e="T382" id="Seg_6487" s="T380">foc.int</ta>
            <ta e="T389" id="Seg_6488" s="T383">foc.int</ta>
            <ta e="T392" id="Seg_6489" s="T391">foc.int</ta>
            <ta e="T394" id="Seg_6490" s="T392">foc.int</ta>
            <ta e="T400" id="Seg_6491" s="T394">foc.int</ta>
            <ta e="T407" id="Seg_6492" s="T405">foc.int</ta>
            <ta e="T410" id="Seg_6493" s="T408">foc.int</ta>
            <ta e="T413" id="Seg_6494" s="T410">foc.int</ta>
            <ta e="T418" id="Seg_6495" s="T415">foc.nar</ta>
            <ta e="T433" id="Seg_6496" s="T431">foc.int</ta>
            <ta e="T448" id="Seg_6497" s="T444">foc.int</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T5" id="Seg_6498" s="T4">RUS:cult</ta>
            <ta e="T7" id="Seg_6499" s="T6">RUS:cult</ta>
            <ta e="T23" id="Seg_6500" s="T22">RUS:cult</ta>
            <ta e="T62" id="Seg_6501" s="T61">RUS:cult</ta>
            <ta e="T124" id="Seg_6502" s="T123">RUS:cult</ta>
            <ta e="T156" id="Seg_6503" s="T155">RUS:cult</ta>
            <ta e="T172" id="Seg_6504" s="T171">EV:gram (DIM)</ta>
            <ta e="T182" id="Seg_6505" s="T181">RUS:cult</ta>
            <ta e="T191" id="Seg_6506" s="T190">RUS:cult</ta>
            <ta e="T196" id="Seg_6507" s="T195">RUS:cult</ta>
            <ta e="T203" id="Seg_6508" s="T202">RUS:cult</ta>
            <ta e="T204" id="Seg_6509" s="T203">RUS:cult</ta>
            <ta e="T205" id="Seg_6510" s="T204">RUS:cult</ta>
            <ta e="T214" id="Seg_6511" s="T213">RUS:cult</ta>
            <ta e="T216" id="Seg_6512" s="T215">RUS:cult</ta>
            <ta e="T236" id="Seg_6513" s="T235">RUS:cult</ta>
            <ta e="T249" id="Seg_6514" s="T248">EV:disc</ta>
            <ta e="T252" id="Seg_6515" s="T251">RUS:cult</ta>
            <ta e="T264" id="Seg_6516" s="T263">RUS:mod</ta>
            <ta e="T306" id="Seg_6517" s="T305">RUS:cult</ta>
            <ta e="T314" id="Seg_6518" s="T313">RUS:cult</ta>
            <ta e="T335" id="Seg_6519" s="T334">RUS:cult</ta>
            <ta e="T342" id="Seg_6520" s="T341">RUS:cult</ta>
            <ta e="T343" id="Seg_6521" s="T342">RUS:cult</ta>
            <ta e="T344" id="Seg_6522" s="T343">RUS:cult</ta>
            <ta e="T352" id="Seg_6523" s="T351">RUS:cult</ta>
            <ta e="T365" id="Seg_6524" s="T364">RUS:cult</ta>
            <ta e="T375" id="Seg_6525" s="T374">RUS:cult</ta>
            <ta e="T377" id="Seg_6526" s="T376">RUS:cult</ta>
            <ta e="T383" id="Seg_6527" s="T382">RUS:cult</ta>
            <ta e="T401" id="Seg_6528" s="T400">RUS:cult</ta>
            <ta e="T416" id="Seg_6529" s="T415">RUS:cult</ta>
            <ta e="T444" id="Seg_6530" s="T443">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T5" id="Seg_6531" s="T4">Vsub</ta>
            <ta e="T7" id="Seg_6532" s="T6">Csub Vsub Vsub</ta>
            <ta e="T23" id="Seg_6533" s="T22">inVins Vsub Vsub</ta>
            <ta e="T62" id="Seg_6534" s="T61">inVins</ta>
            <ta e="T124" id="Seg_6535" s="T123">Csub Vsub Vsub</ta>
            <ta e="T156" id="Seg_6536" s="T155">Vsub</ta>
            <ta e="T182" id="Seg_6537" s="T181">Vsub</ta>
            <ta e="T191" id="Seg_6538" s="T190">Csub Vsub Vsub</ta>
            <ta e="T196" id="Seg_6539" s="T195">Vsub</ta>
            <ta e="T203" id="Seg_6540" s="T202">inVins Vsub</ta>
            <ta e="T204" id="Seg_6541" s="T203">Vsub Vsub Vsub</ta>
            <ta e="T205" id="Seg_6542" s="T204">Vsub Csub</ta>
            <ta e="T214" id="Seg_6543" s="T213">inVins Vsub</ta>
            <ta e="T216" id="Seg_6544" s="T215">Csub Vsub fortition</ta>
            <ta e="T236" id="Seg_6545" s="T235">inVins Vsub Vsub</ta>
            <ta e="T252" id="Seg_6546" s="T251">Vsub</ta>
            <ta e="T264" id="Seg_6547" s="T263">Vsub Csub Vsub</ta>
            <ta e="T306" id="Seg_6548" s="T305">Vsub</ta>
            <ta e="T314" id="Seg_6549" s="T313">Vsub fortition</ta>
            <ta e="T335" id="Seg_6550" s="T334">fortition Vsub</ta>
            <ta e="T342" id="Seg_6551" s="T341">Vsub fortition Vsub</ta>
            <ta e="T344" id="Seg_6552" s="T343">lenition Vsub</ta>
            <ta e="T352" id="Seg_6553" s="T351">Vsub fortition</ta>
            <ta e="T365" id="Seg_6554" s="T364">Vsub fortition</ta>
            <ta e="T375" id="Seg_6555" s="T374">Vsub</ta>
            <ta e="T377" id="Seg_6556" s="T376">Csub Vsub Vsub</ta>
            <ta e="T383" id="Seg_6557" s="T382">Vsub</ta>
            <ta e="T401" id="Seg_6558" s="T400">Vsub fortition Vsub</ta>
            <ta e="T416" id="Seg_6559" s="T415">Vsub fortition</ta>
            <ta e="T444" id="Seg_6560" s="T443">Vsub fortition Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T5" id="Seg_6561" s="T4">dir:infl</ta>
            <ta e="T7" id="Seg_6562" s="T6">dir:infl</ta>
            <ta e="T23" id="Seg_6563" s="T22">dir:infl</ta>
            <ta e="T62" id="Seg_6564" s="T61">dir:bare</ta>
            <ta e="T124" id="Seg_6565" s="T123">dir:bare</ta>
            <ta e="T156" id="Seg_6566" s="T155">dir:infl</ta>
            <ta e="T172" id="Seg_6567" s="T171">dir:bare</ta>
            <ta e="T182" id="Seg_6568" s="T181">dir:bare</ta>
            <ta e="T191" id="Seg_6569" s="T190">dir:bare</ta>
            <ta e="T196" id="Seg_6570" s="T195">dir:bare</ta>
            <ta e="T203" id="Seg_6571" s="T202">dir:infl</ta>
            <ta e="T204" id="Seg_6572" s="T203">dir:infl</ta>
            <ta e="T205" id="Seg_6573" s="T204">dir:infl</ta>
            <ta e="T214" id="Seg_6574" s="T213">dir:bare</ta>
            <ta e="T216" id="Seg_6575" s="T215">parad:infl</ta>
            <ta e="T236" id="Seg_6576" s="T235">dir:infl</ta>
            <ta e="T249" id="Seg_6577" s="T248">dir:bare</ta>
            <ta e="T252" id="Seg_6578" s="T251">dir:bare</ta>
            <ta e="T264" id="Seg_6579" s="T263">dir:bare</ta>
            <ta e="T306" id="Seg_6580" s="T305">dir:infl</ta>
            <ta e="T314" id="Seg_6581" s="T313">dir:infl</ta>
            <ta e="T335" id="Seg_6582" s="T334">dir:bare</ta>
            <ta e="T342" id="Seg_6583" s="T341">dir:bare</ta>
            <ta e="T343" id="Seg_6584" s="T342">dir:bare</ta>
            <ta e="T344" id="Seg_6585" s="T343">dir:infl</ta>
            <ta e="T352" id="Seg_6586" s="T351">dir:infl</ta>
            <ta e="T365" id="Seg_6587" s="T364">dir:infl</ta>
            <ta e="T375" id="Seg_6588" s="T374">dir:infl</ta>
            <ta e="T377" id="Seg_6589" s="T376">dir:bare</ta>
            <ta e="T383" id="Seg_6590" s="T382">dir:bare</ta>
            <ta e="T401" id="Seg_6591" s="T400">dir:bare</ta>
            <ta e="T416" id="Seg_6592" s="T415">dir:infl</ta>
            <ta e="T444" id="Seg_6593" s="T443">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T1" id="Seg_6594" s="T0">The ghost.</ta>
            <ta e="T12" id="Seg_6595" s="T1">Two boys, Mika and Mukulaj spent the night at home when their parents had left.</ta>
            <ta e="T19" id="Seg_6596" s="T12">During their study time, throughout winter, they are always at boarding school.</ta>
            <ta e="T30" id="Seg_6597" s="T19">The old wooden beams of the wall of the Russian house have mouldered badly.</ta>
            <ta e="T37" id="Seg_6598" s="T30">When someone opens the door of the entry, it creaks loudly.</ta>
            <ta e="T50" id="Seg_6599" s="T37">Inside the house, however much noise there may be, the creaking can be heard when someone opens the door.</ta>
            <ta e="T61" id="Seg_6600" s="T50">Sometimes when the wind beats it open, it does not allow the people of the house to live or to sleep.</ta>
            <ta e="T69" id="Seg_6601" s="T61">In the centre of the rayon there are no such very old houses.</ta>
            <ta e="T77" id="Seg_6602" s="T69">The inhabitants should soon move to a new house.</ta>
            <ta e="T84" id="Seg_6603" s="T77">They are waiting until the large new house will be completed.</ta>
            <ta e="T91" id="Seg_6604" s="T84">Now the parents of the children have come to visit their close relatives.</ta>
            <ta e="T99" id="Seg_6605" s="T91">They should come home tomorrow or the day after tomorrow.</ta>
            <ta e="T106" id="Seg_6606" s="T99">In winter the dark day is short, the evenings come very early.</ta>
            <ta e="T116" id="Seg_6607" s="T106">After the boys had eaten their food up to the point where they were full, they prepared to go to sleep.</ta>
            <ta e="T126" id="Seg_6608" s="T116">"Hey man, our old house is probably haunted at night", said Mukulaj to his elder brother.</ta>
            <ta e="T134" id="Seg_6609" s="T126">"You conjure up bad things again", swore the big one to him.</ta>
            <ta e="T139" id="Seg_6610" s="T134">"Rather undress and lie down there on the bed.</ta>
            <ta e="T141" id="Seg_6611" s="T139">We will sleep together."</ta>
            <ta e="T147" id="Seg_6612" s="T141">The elder brother is still afraid inside, apparently.</ta>
            <ta e="T154" id="Seg_6613" s="T147">They spend the night in the empty house for the first time without parents.</ta>
            <ta e="T163" id="Seg_6614" s="T154">After the elder brother had extinguished the light, he lay down next to his little brother under the blanket.</ta>
            <ta e="T168" id="Seg_6615" s="T163">The children were lying under the blanket without breathing.</ta>
            <ta e="T171" id="Seg_6616" s="T168">They are afraid and listen.</ta>
            <ta e="T177" id="Seg_6617" s="T171">Very soon they closed their eyes, fell asleep and snored.</ta>
            <ta e="T181" id="Seg_6618" s="T177">Some time passed.</ta>
            <ta e="T186" id="Seg_6619" s="T181">Mika woke up from punches in his side from his little brother.</ta>
            <ta e="T195" id="Seg_6620" s="T186">"Brother, listen, there is a ghost apparently!" Being afraid, Mukulaj could barely utter a word.</ta>
            <ta e="T200" id="Seg_6621" s="T195">Mika lifted the edge of the blanket and listened.</ta>
            <ta e="T206" id="Seg_6622" s="T200">Indeed, plates and spoons rattle on the dining table.</ta>
            <ta e="T210" id="Seg_6623" s="T206">It is very dark, nothing is visible.</ta>
            <ta e="T220" id="Seg_6624" s="T210">Oh, something is ripping papers on the table at a distance.</ta>
            <ta e="T228" id="Seg_6625" s="T220">Embracing each other, being agraid, the children are lying under the blanket without breathing.</ta>
            <ta e="T234" id="Seg_6626" s="T228">Outside in the entry way something was heard walking around.</ta>
            <ta e="T238" id="Seg_6627" s="T234">It knocks loudly the walls of the house.</ta>
            <ta e="T244" id="Seg_6628" s="T238">It knocks kettles and various jars off the shelves on the floor.</ta>
            <ta e="T248" id="Seg_6629" s="T244">Then it is heard that it kicks them.</ta>
            <ta e="T249" id="Seg_6630" s="T248">"Be quiet!</ta>
            <ta e="T254" id="Seg_6631" s="T249">Lie still", threatened Mika his little brother.</ta>
            <ta e="T261" id="Seg_6632" s="T254">Inside the house, in the entryway the noise does not stop.</ta>
            <ta e="T265" id="Seg_6633" s="T261">It is still a long time until the morning.</ta>
            <ta e="T269" id="Seg_6634" s="T265">Who will come and save them?</ta>
            <ta e="T276" id="Seg_6635" s="T269">The children are scared stiff and cannot cry or scream.</ta>
            <ta e="T283" id="Seg_6636" s="T276">Being scared that way they were overcome by sleep.</ta>
            <ta e="T289" id="Seg_6637" s="T283">Suddenly they woke up because of a very loud noise.</ta>
            <ta e="T296" id="Seg_6638" s="T289">In the entryway something crashed like a hard stone.</ta>
            <ta e="T299" id="Seg_6639" s="T296">A shout, a scream was heard.</ta>
            <ta e="T301" id="Seg_6640" s="T299">The door opened.</ta>
            <ta e="T307" id="Seg_6641" s="T301">Someone entered the house and lit the light.</ta>
            <ta e="T313" id="Seg_6642" s="T307">"Get up, why did you not lock the door of the entryway!?</ta>
            <ta e="T325" id="Seg_6643" s="T313">Cows came in and have trampled down the floor, destroyed the inside of the entryway, and pooed all over the floor."</ta>
            <ta e="T331" id="Seg_6644" s="T325">The children opened their eyes and saw a person standing.</ta>
            <ta e="T341" id="Seg_6645" s="T331">He got very angry that he had soiled his trousers with flared breeches in excrements, and wiped it off with his hands.</ta>
            <ta e="T354" id="Seg_6646" s="T341">When Opuo on his way to work at the kolkhoz sovkhoz passed the house, he saw the cows enter the entryway.</ta>
            <ta e="T358" id="Seg_6647" s="T354">Then he chased them all away.</ta>
            <ta e="T368" id="Seg_6648" s="T358">After that he went in, slipped and fell down, and got dirty in the excrements of the cows.</ta>
            <ta e="T374" id="Seg_6649" s="T368">He tried to get up and also got his hands very dirty.</ta>
            <ta e="T382" id="Seg_6650" s="T374">Mika and Mukulaj were very happy about the arrival of the angry man.</ta>
            <ta e="T389" id="Seg_6651" s="T382">Mika, being shy with the unknown person, said, while covering his eyes:</ta>
            <ta e="T394" id="Seg_6652" s="T389">"Something scared us at night, we could not sleep".</ta>
            <ta e="T400" id="Seg_6653" s="T394">He told everything that had happened during the night.</ta>
            <ta e="T407" id="Seg_6654" s="T400">When Opuo noticed the scared state of the children, he felt sorry for them.</ta>
            <ta e="T410" id="Seg_6655" s="T407">His anger disappeared immediately.</ta>
            <ta e="T413" id="Seg_6656" s="T410">He laughed and laughed and said to the poor ones:</ta>
            <ta e="T419" id="Seg_6657" s="T413">"At night cows and mice scared you.</ta>
            <ta e="T421" id="Seg_6658" s="T419">Aren't you embarrassed?</ta>
            <ta e="T425" id="Seg_6659" s="T421">Do you know that you were afraid for nothing?</ta>
            <ta e="T433" id="Seg_6660" s="T425">Because of your badness I got very dirty.</ta>
            <ta e="T443" id="Seg_6661" s="T433">Take well care of your entryway, and your house, until your mother and your father will returm."</ta>
            <ta e="T448" id="Seg_6662" s="T443">Opuo got up to leave and asked the children to do so.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T1" id="Seg_6663" s="T0">Das Gespenst.</ta>
            <ta e="T12" id="Seg_6664" s="T1">Zwei Jungen, Mika und Mukulaj, übernachteten zuhause, nachdem ihre Eltern fortgegangen waren.</ta>
            <ta e="T19" id="Seg_6665" s="T12">Während der Zeit des Lernen, den Winter hindurch, sind sie immer im Internat.</ta>
            <ta e="T30" id="Seg_6666" s="T19">Die Hölzer der Wand des uralten russischen Hauses sind sehr vermodert.</ta>
            <ta e="T37" id="Seg_6667" s="T30">Wenn jemand die äußere Tür der Diele öffnet, dann gibt es ein starkes Knarren.</ta>
            <ta e="T50" id="Seg_6668" s="T37">Im Haus, wieviel Lärm auch sein mag, wenn jemand öffnet, ist das knarrende Geräusch sofort zu hören.</ta>
            <ta e="T61" id="Seg_6669" s="T50">Manchmal, wenn der Wind sie aufreißt, dann lässt sie mit ihrem Knarren die Bewohner weder leben noch schlafen.</ta>
            <ta e="T69" id="Seg_6670" s="T61">Im Hauptort des Rajons gibt es keine solch sehr alten Häuser.</ta>
            <ta e="T77" id="Seg_6671" s="T69">Die Bewohner sollten bald in ein neues Haus umziehen.</ta>
            <ta e="T84" id="Seg_6672" s="T77">Sie warten darauf, dass das große, neue Haus fertig gebaut ist.</ta>
            <ta e="T91" id="Seg_6673" s="T84">Jetzt sind die Eltern der Kinder zu nahe Verwandte zu Besuch gegangen.</ta>
            <ta e="T99" id="Seg_6674" s="T91">Nach Hause sollen sie morgen oder übermorgen kommen.</ta>
            <ta e="T106" id="Seg_6675" s="T99">Im Winter ist der dunkle Tag kurz, die Abende kommen sehr früh.</ta>
            <ta e="T116" id="Seg_6676" s="T106">Nachdem die Jungen sich an ihrem Essen satt gegessen hatten, bereiteten sie sich zum Schlafen vor.</ta>
            <ta e="T126" id="Seg_6677" s="T116">"Hey, in unserem alten Haus spukt es nachts wahrscheinlich", sagte Mukulaj zu seinem älteren Bruder.</ta>
            <ta e="T134" id="Seg_6678" s="T126">"Du beschwörst wieder Schlechtes herauf", schalt ihn der Große.</ta>
            <ta e="T139" id="Seg_6679" s="T134">"Zieh dich lieber aus und leg dich dort aufs Bett.</ta>
            <ta e="T141" id="Seg_6680" s="T139">Wir werden zusammen schlafen."</ta>
            <ta e="T147" id="Seg_6681" s="T141">Der ältere Bruder ängstigt sich in seinem Innern wohl dennoch.</ta>
            <ta e="T154" id="Seg_6682" s="T147">Sie schlafen das erste Mal im leeren Haus ohne Eltern.</ta>
            <ta e="T163" id="Seg_6683" s="T154">Nachdem der ältere Bruder die Lampe gelöscht hatte, legte er sich neben seinen kleinen Bruder unter die Bettdecke.</ta>
            <ta e="T168" id="Seg_6684" s="T163">Die Kinder lagen unter der Bettdecke, ohne zu atmen.</ta>
            <ta e="T171" id="Seg_6685" s="T168">Sie haben Angst und lauschen.</ta>
            <ta e="T177" id="Seg_6686" s="T171">Sehr bald schlossen sie die Augen, schliefen ein und schnarchten.</ta>
            <ta e="T181" id="Seg_6687" s="T177">Es verging einige Zeit.</ta>
            <ta e="T186" id="Seg_6688" s="T181">Mika wachte davon auf, dass ihn sein kleiner Bruder in die Seite stieß.</ta>
            <ta e="T195" id="Seg_6689" s="T186">"Bruder, hör, offenbar ein Gespenst", sagte Mukulaj, vor Angst kaum ein Wort herausbringend.</ta>
            <ta e="T200" id="Seg_6690" s="T195">Mika hob den Rand der Bettdecke an und lauschte.</ta>
            <ta e="T206" id="Seg_6691" s="T200">Tatsächlich, auf dem Esstisch klappern Teller und Löffel.</ta>
            <ta e="T210" id="Seg_6692" s="T206">Es ist ganz dunkel, nichts ist zu sehen.</ta>
            <ta e="T220" id="Seg_6693" s="T210">Oh, irgendetwas zerreißt das Papier auf dem weit entfernt stehenden Tisch.</ta>
            <ta e="T228" id="Seg_6694" s="T220">Die Kinder umarmen sich unter der Bettdecke und liegen vor Angst ohne zu atmen da.</ta>
            <ta e="T234" id="Seg_6695" s="T228">Draußen in der Diele war zu hören, dass etwas herumlief.</ta>
            <ta e="T238" id="Seg_6696" s="T234">Es schlägt laut an die Wände des Hauses.</ta>
            <ta e="T244" id="Seg_6697" s="T238">Es wirft Kessel und unterschiedliche Gefäße von den Regalen auf die Erde.</ta>
            <ta e="T248" id="Seg_6698" s="T244">Dann ist zu hören, wie es dagegen tritt.</ta>
            <ta e="T249" id="Seg_6699" s="T248">"Ruhe!</ta>
            <ta e="T254" id="Seg_6700" s="T249">Lieg ruhig", schalt Mika seinen kleinen Bruder.</ta>
            <ta e="T261" id="Seg_6701" s="T254">Im Haus, in der Diele hört der Lärm nicht auf.</ta>
            <ta e="T265" id="Seg_6702" s="T261">Bis zum Morgen ist es noch lange.</ta>
            <ta e="T269" id="Seg_6703" s="T265">Wer kommt und rettet sie?</ta>
            <ta e="T276" id="Seg_6704" s="T269">Die Kinder, vor Schreck erstarrt, können weder weinen noch schreien.</ta>
            <ta e="T283" id="Seg_6705" s="T276">Sie hatten Angst und wurden vom Schlaf übermannt.</ta>
            <ta e="T289" id="Seg_6706" s="T283">Sie wachten plötzlich von einem sehr großen Lärm auf.</ta>
            <ta e="T296" id="Seg_6707" s="T289">In der Diele krachte irgendetwas hart wie ein Stein.</ta>
            <ta e="T299" id="Seg_6708" s="T296">Ein Schreien, ein Schrei war zu hören.</ta>
            <ta e="T301" id="Seg_6709" s="T299">Die Tür wurde geöffnet.</ta>
            <ta e="T307" id="Seg_6710" s="T301">Es kam jemand ins Haus herein und zündete die Lampe an.</ta>
            <ta e="T313" id="Seg_6711" s="T307">"Steht auf, warum habt ihr die Tür eurer Diele nicht zugemacht?!</ta>
            <ta e="T325" id="Seg_6712" s="T313">Kühe sind hineingelaufen, haben den Boden zertrampelt, das Innere der Diele ganz zerstört und den Boden vollgekotet."</ta>
            <ta e="T331" id="Seg_6713" s="T325">Die Kinder öffneten ihre Augen und sahen, dass dort ein Mensch stand.</ta>
            <ta e="T341" id="Seg_6714" s="T331">Jener schimpfte und schimpfte, dass er sich seine weiten Kniehosen mit Kot vollgeschmiert hatte, und fing an, sie mit seinen Händen abzuwischen.</ta>
            <ta e="T354" id="Seg_6715" s="T341">Als Opuo zur Arbeit auf der Kolchose-Sowchose am Haus entlang ging, sah er die Kühe in die Diele hineingehen.</ta>
            <ta e="T358" id="Seg_6716" s="T354">Dann jagte er alle hinaus.</ta>
            <ta e="T368" id="Seg_6717" s="T358">Danach ging er hinein, rutschte aus und beschmutzte sich völlig mit dem Kot der Kühe.</ta>
            <ta e="T374" id="Seg_6718" s="T368">Er versuchte aufzustehen und machte auch seine Hände sehr dreckig.</ta>
            <ta e="T382" id="Seg_6719" s="T374">Mika und Mukulaj freuten sich sehr, als der wütende Mann kam.</ta>
            <ta e="T389" id="Seg_6720" s="T382">Mika fürchtete sich vor dem fremden Menschen, verdeckte seine Augen und sagte:</ta>
            <ta e="T394" id="Seg_6721" s="T389">"Uns hat in der Nacht etwas geängstigt, wir konnten nicht schlafen."</ta>
            <ta e="T400" id="Seg_6722" s="T394">Er erzählte alles, was in der Nacht passiert war.</ta>
            <ta e="T407" id="Seg_6723" s="T400">Als Opuo sah, wie verängstigt die Kinder waren, hatte er Mitleid mit ihnen.</ta>
            <ta e="T410" id="Seg_6724" s="T407">Sein Zorn ging sofort vorbei.</ta>
            <ta e="T413" id="Seg_6725" s="T410">Er lachte und lachte und sagte zu den Unglückseligen:</ta>
            <ta e="T419" id="Seg_6726" s="T413">"In der Nacht haben euch Kühe und Mäuse geängstigt.</ta>
            <ta e="T421" id="Seg_6727" s="T419">Schämt ihr euch nicht?</ta>
            <ta e="T425" id="Seg_6728" s="T421">Wisst ihr, dass ihr wegen nichts Angst hattet?</ta>
            <ta e="T433" id="Seg_6729" s="T425">Wegen eurer Schlechtheit habe ich mich ganz dreckig gemacht.</ta>
            <ta e="T443" id="Seg_6730" s="T433">Kümmert euch gut um eure Diele, um euer Haus, bis eure Mutter und euer Vater wiederkommen."</ta>
            <ta e="T448" id="Seg_6731" s="T443">Opuo stand auf, um hinauszugehen, und forderte die Kinder [dazu] auf.</ta>
         </annotation>
         <annotation name="fr" tierref="fr" />
         <annotation name="ltr" tierref="ltr">
            <ta e="T1" id="Seg_6732" s="T0">Привидение.</ta>
            <ta e="T12" id="Seg_6733" s="T1">Два мальчика Мика и Мукулай после того, как родители уехали дома ночевали.</ta>
            <ta e="T19" id="Seg_6734" s="T12">Они во время учёбы, всю зиму в интернате бывали.</ta>
            <ta e="T30" id="Seg_6735" s="T19">Старого русского (по-русски сделаного) дома стенные доски от ветхости сгнили.</ta>
            <ta e="T37" id="Seg_6736" s="T30">Уличную дверь если человек откроет, сильно заскрипит. </ta>
            <ta e="T50" id="Seg_6737" s="T37">Внутри дома, хоть как бы не было шумно, кто-нибудь если откроет, скрипа звук тут же слышно.</ta>
            <ta e="T61" id="Seg_6738" s="T50">Иногда, если от сильного ветра откроет, своим скрипом домашним не даст ни сидеть, ни спать.</ta>
            <ta e="T69" id="Seg_6739" s="T61">В районном центре такого очень старого дома нет.</ta>
            <ta e="T77" id="Seg_6740" s="T69">Домашние скоро в новые дом должны переехать.</ta>
            <ta e="T84" id="Seg_6741" s="T77">Большой новый дом когда построится в ожидании сидят.</ta>
            <ta e="T91" id="Seg_6742" s="T84">Сейчас родители детей к ближайшим родственникам в гости поехали (пошли).</ta>
            <ta e="T99" id="Seg_6743" s="T91">Домой завтра или послезавтра должны приехать (прийти).</ta>
            <ta e="T106" id="Seg_6744" s="T99">Зимой тёмный день короткий, вечереет слишком рано.</ta>
            <ta e="T116" id="Seg_6745" s="T106">Мальчики своей еды до сыта наевшись, ко сну начали готовиться.</ta>
            <ta e="T126" id="Seg_6746" s="T116">"Друг (слушай), наш старый дом ночью с привидениями, наверное, – Мукулай старшему брату сказал.</ta>
            <ta e="T134" id="Seg_6747" s="T126">"Ты опять плохое наговариваешь ("плохо начинаешь взрываеться"), – его старший одёрнул.</ta>
            <ta e="T139" id="Seg_6748" s="T134">– Лучше разденься, там на кровать ложись.</ta>
            <ta e="T141" id="Seg_6749" s="T139">Вместе будем спать."</ta>
            <ta e="T147" id="Seg_6750" s="T141">Старший брат в глубине души всё-таки боится, кажется.</ta>
            <ta e="T154" id="Seg_6751" s="T147">Они в пустой квартире без родителей впервые ночуют.</ta>
            <ta e="T163" id="Seg_6752" s="T154">Старший брат, лампу (керосиновую) погасив, с братишкой рядышком под одеяло забравшись, лёг.</ta>
            <ta e="T168" id="Seg_6753" s="T163">Дети под одеялом, не дыша, лежали.</ta>
            <ta e="T171" id="Seg_6754" s="T168">Страшась, слушают.</ta>
            <ta e="T177" id="Seg_6755" s="T171">Скоренько, закрыв глазки, заснули крепко.</ta>
            <ta e="T181" id="Seg_6756" s="T177">Сколько времени прошло, неизвестно.</ta>
            <ta e="T186" id="Seg_6757" s="T181">Мика от толчков братишкиных в бок проснулся.</ta>
            <ta e="T195" id="Seg_6758" s="T186">"Брат, послушай, привидение, кажется," – Мукулай испугавшись, слова еле выговорил .</ta>
            <ta e="T200" id="Seg_6759" s="T195">Мика одеяла краешек приоткрыв, послушал.</ta>
            <ta e="T206" id="Seg_6760" s="T200">Правда, на обеденном столе тарелки ложки гремят.</ta>
            <ta e="T210" id="Seg_6761" s="T206">Совсем темно, ничего не видно.</ta>
            <ta e="T220" id="Seg_6762" s="T210">О-ой, на далеко стоящем столе бумаги что-то (кто-то) рвёт.</ta>
            <ta e="T228" id="Seg_6763" s="T220">Дети под одеялом обнявшись, напуганные, не дыша лежат.</ta>
            <ta e="T234" id="Seg_6764" s="T228">На улице в сенях что-то ходит было слышно.</ta>
            <ta e="T238" id="Seg_6765" s="T234">Стену дома окно ??стучит (постукивает). </ta>
            <ta e="T244" id="Seg_6766" s="T238">С полок кастрюли и разное на пол бросает.</ta>
            <ta e="T248" id="Seg_6767" s="T244">Потом их как пинает, слышно.</ta>
            <ta e="T249" id="Seg_6768" s="T248">"Тихо!</ta>
            <ta e="T254" id="Seg_6769" s="T249">Смирно лежи," – Мика братишке пригрозил.</ta>
            <ta e="T261" id="Seg_6770" s="T254">В домае, в сенях шум никак не прекращается.</ta>
            <ta e="T265" id="Seg_6771" s="T261">До утра ещё далеко.</ta>
            <ta e="T269" id="Seg_6772" s="T265">Кто к ним придёт и поможет.</ta>
            <ta e="T276" id="Seg_6773" s="T269">Дети с перепугу ни плакать, ни кричать не могут.</ta>
            <ta e="T283" id="Seg_6774" s="T276">И таким образом боясь, подавленные сном так и заснули.</ta>
            <ta e="T289" id="Seg_6775" s="T283">От какого-то сильного шума резко проснулись.</ta>
            <ta e="T296" id="Seg_6776" s="T289">В сенях что-то тяжёлым камнем рухнуло. </ta>
            <ta e="T299" id="Seg_6777" s="T296">Крик, шум послышался.</ta>
            <ta e="T301" id="Seg_6778" s="T299">Дверь открылась.</ta>
            <ta e="T307" id="Seg_6779" s="T301">В дом кто-то зайдя, лапму зажёг.</ta>
            <ta e="T313" id="Seg_6780" s="T307">"Вставайте, вы в сенях дверь почему не закрыли!?</ta>
            <ta e="T325" id="Seg_6781" s="T313">Коровы зайдя туда, полы сильно затоптали, в сенях всё сравняли, полы полностью загадили.</ta>
            <ta e="T331" id="Seg_6782" s="T325">Дети глаза открыв, человека стоящего увидели.</ta>
            <ta e="T341" id="Seg_6783" s="T331">Он, ругаясь, одежду и штаны-галифе в навозе вымаженных, руками вытирал.</ta>
            <ta e="T354" id="Seg_6784" s="T341">Опуо (Афоня), на колхозную работу рядом с домом проходя, в сени, как коровы зашли, увидел.</ta>
            <ta e="T358" id="Seg_6785" s="T354">Потом всех повыгонял.</ta>
            <ta e="T368" id="Seg_6786" s="T358">После этого, когда заходил, подскользувшись, в коровьем навозе сильно вымазался.</ta>
            <ta e="T374" id="Seg_6787" s="T368">Поднимаясь руки тоже сильно испачкал.</ta>
            <ta e="T382" id="Seg_6788" s="T374">Мика с Мукулаем приходу рассерженного человека очень обрадовались.</ta>
            <ta e="T389" id="Seg_6789" s="T382">Мика незнакомого человека стесняясь, глаза пряча сказал: </ta>
            <ta e="T394" id="Seg_6790" s="T389">"Нас ночью что-то напугало, никак не смогли поспать."</ta>
            <ta e="T400" id="Seg_6791" s="T394">Всю ночь что произошло, обо всём рассказал.</ta>
            <ta e="T407" id="Seg_6792" s="T400">Опуо (Афоня) детей напуганный вид увидев, их пожалел.</ta>
            <ta e="T410" id="Seg_6793" s="T407">Гнев сразу прошёл.</ta>
            <ta e="T413" id="Seg_6794" s="T410">Смеясь беденьким сказал: </ta>
            <ta e="T419" id="Seg_6795" s="T413">"Ночью вас коровы с мышами напугали.</ta>
            <ta e="T421" id="Seg_6796" s="T419">Не стыдно вам?</ta>
            <ta e="T425" id="Seg_6797" s="T421">Из-за ничего испугались зная?</ta>
            <ta e="T433" id="Seg_6798" s="T425">Из-за вас проказников я сильно заморался.</ta>
            <ta e="T443" id="Seg_6799" s="T433">Мама, папа пока не приехали в сенях и в доме хорошенько приберитесь".</ta>
            <ta e="T448" id="Seg_6800" s="T443">Опуо (Афоня) уходя детям наказал.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
