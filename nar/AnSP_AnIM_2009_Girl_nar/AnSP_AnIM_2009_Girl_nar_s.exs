<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID4AD658EA-3684-B7A6-25FA-AF27B3A63E76">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>AnSP_AnIM_2009_Girl_nar</transcription-name>
         <referenced-file url="AnSP_AnIM_2009_Girl_nar.wav" />
         <referenced-file url="AnSP_AnIM_2009_Girl_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\AnSP_AnIM_2009_Girl_nar\AnSP_AnIM_2009_Girl_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">225</ud-information>
            <ud-information attribute-name="# HIAT:w">184</ud-information>
            <ud-information attribute-name="# e">184</ud-information>
            <ud-information attribute-name="# HIAT:u">25</ud-information>
            <ud-information attribute-name="# sc">16</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AnIM">
            <abbreviation>AnIM</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="AnSP">
            <abbreviation>AnSP</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="SE">
            <abbreviation>SE</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.0" type="appl" />
         <tli id="T2" time="0.37000000000000005" type="appl" />
         <tli id="T3" time="0.7400000000000001" type="appl" />
         <tli id="T4" time="1.11" type="appl" />
         <tli id="T5" time="1.4800000000000002" type="appl" />
         <tli id="T6" time="1.8500000000000003" type="appl" />
         <tli id="T7" time="2.22" type="appl" />
         <tli id="T8" time="3.6196" type="appl" />
         <tli id="T9" time="5.0192" type="appl" />
         <tli id="T10" time="6.418800000000001" type="appl" />
         <tli id="T11" time="7.8184000000000005" type="appl" />
         <tli id="T12" time="9.218" type="appl" />
         <tli id="T13" time="10.062181818181818" type="appl" />
         <tli id="T14" time="10.906363636363636" type="appl" />
         <tli id="T15" time="11.750545454545454" type="appl" />
         <tli id="T16" time="12.594727272727273" type="appl" />
         <tli id="T17" time="13.438909090909092" type="appl" />
         <tli id="T18" time="14.283090909090909" type="appl" />
         <tli id="T19" time="15.127272727272729" type="appl" />
         <tli id="T20" time="15.971454545454545" type="appl" />
         <tli id="T21" time="16.815636363636365" type="appl" />
         <tli id="T22" time="17.65981818181818" type="appl" />
         <tli id="T23" time="18.504" type="appl" />
         <tli id="T24" time="19.405875" type="appl" />
         <tli id="T25" time="20.307750000000002" type="appl" />
         <tli id="T26" time="21.209625000000003" type="appl" />
         <tli id="T27" time="22.1115" type="appl" />
         <tli id="T28" time="23.013375000000003" type="appl" />
         <tli id="T29" time="23.91525" type="appl" />
         <tli id="T30" time="24.817125" type="appl" />
         <tli id="T31" time="25.719" type="appl" />
         <tli id="T32" time="26.708000000000002" type="appl" />
         <tli id="T33" time="27.697" type="appl" />
         <tli id="T34" time="28.686" type="appl" />
         <tli id="T35" time="29.675" type="appl" />
         <tli id="T36" time="30.663999999999998" type="appl" />
         <tli id="T37" time="31.653" type="appl" />
         <tli id="T38" time="32.23835714285714" type="appl" />
         <tli id="T39" time="32.82371428571428" type="appl" />
         <tli id="T40" time="33.40907142857143" type="appl" />
         <tli id="T41" time="33.99442857142857" type="appl" />
         <tli id="T42" time="34.57978571428571" type="appl" />
         <tli id="T43" time="35.165142857142854" type="appl" />
         <tli id="T44" time="35.7505" type="appl" />
         <tli id="T45" time="36.335857142857144" type="appl" />
         <tli id="T46" time="36.921214285714285" type="appl" />
         <tli id="T47" time="37.50657142857143" type="appl" />
         <tli id="T48" time="38.09192857142857" type="appl" />
         <tli id="T49" time="38.677285714285716" type="appl" />
         <tli id="T50" time="39.26264285714286" type="appl" />
         <tli id="T51" time="39.848" type="appl" />
         <tli id="T52" time="40.483999999999995" type="appl" />
         <tli id="T53" time="41.12" type="appl" />
         <tli id="T54" time="41.339999999999996" type="appl" />
         <tli id="T55" time="41.56" type="appl" />
         <tli id="T56" time="41.78" type="appl" />
         <tli id="T57" time="42.0" type="appl" />
         <tli id="T58" time="43.201" type="appl" />
         <tli id="T59" time="44.402" type="appl" />
         <tli id="T60" time="45.603" type="appl" />
         <tli id="T61" time="46.303000000000004" type="appl" />
         <tli id="T62" time="47.003" type="appl" />
         <tli id="T63" time="47.703" type="appl" />
         <tli id="T64" time="48.403" type="appl" />
         <tli id="T65" time="49.103" type="appl" />
         <tli id="T66" time="49.803" type="appl" />
         <tli id="T67" time="50.503" type="appl" />
         <tli id="T68" time="51.203" type="appl" />
         <tli id="T69" time="51.903" type="appl" />
         <tli id="T70" time="52.603" type="appl" />
         <tli id="T71" time="53.303" type="appl" />
         <tli id="T72" time="54.003" type="appl" />
         <tli id="T73" time="54.9174" type="appl" />
         <tli id="T74" time="55.8318" type="appl" />
         <tli id="T75" time="56.7462" type="appl" />
         <tli id="T76" time="57.6606" type="appl" />
         <tli id="T77" time="58.575" type="appl" />
         <tli id="T78" time="59.4894" type="appl" />
         <tli id="T79" time="60.4038" type="appl" />
         <tli id="T80" time="61.3182" type="appl" />
         <tli id="T81" time="62.2326" type="appl" />
         <tli id="T82" time="63.147" type="appl" />
         <tli id="T83" time="63.9434" type="appl" />
         <tli id="T84" time="64.7398" type="appl" />
         <tli id="T85" time="65.5362" type="appl" />
         <tli id="T86" time="66.3326" type="appl" />
         <tli id="T87" time="67.129" type="appl" />
         <tli id="T88" time="67.9254" type="appl" />
         <tli id="T89" time="68.7218" type="appl" />
         <tli id="T90" time="69.51820000000001" type="appl" />
         <tli id="T91" time="70.3146" type="appl" />
         <tli id="T92" time="71.111" type="appl" />
         <tli id="T93" time="72.33500000000001" type="appl" />
         <tli id="T94" time="73.559" type="appl" />
         <tli id="T95" time="74.783" type="appl" />
         <tli id="T96" time="76.00699999999999" type="appl" />
         <tli id="T97" time="77.231" type="appl" />
         <tli id="T98" time="77.8870909090909" type="appl" />
         <tli id="T99" time="78.54318181818181" type="appl" />
         <tli id="T100" time="79.19927272727273" type="appl" />
         <tli id="T101" time="79.85536363636363" type="appl" />
         <tli id="T102" time="80.51145454545454" type="appl" />
         <tli id="T103" time="81.16754545454545" type="appl" />
         <tli id="T104" time="81.82363636363635" type="appl" />
         <tli id="T105" time="82.47972727272726" type="appl" />
         <tli id="T106" time="83.13581818181818" type="appl" />
         <tli id="T107" time="83.79190909090909" type="appl" />
         <tli id="T108" time="84.448" type="appl" />
         <tli id="T109" time="85.09990909090908" type="appl" />
         <tli id="T110" time="85.75181818181818" type="appl" />
         <tli id="T111" time="86.40372727272727" type="appl" />
         <tli id="T112" time="87.05563636363635" type="appl" />
         <tli id="T113" time="87.70754545454545" type="appl" />
         <tli id="T114" time="88.35945454545454" type="appl" />
         <tli id="T115" time="89.01136363636364" type="appl" />
         <tli id="T116" time="89.66327272727273" type="appl" />
         <tli id="T117" time="90.31518181818181" type="appl" />
         <tli id="T118" time="90.96709090909091" type="appl" />
         <tli id="T119" time="91.619" type="appl" />
         <tli id="T120" time="93.848" type="appl" />
         <tli id="T121" time="94.42475" type="appl" />
         <tli id="T122" time="95.0015" type="appl" />
         <tli id="T123" time="95.57825" type="appl" />
         <tli id="T124" time="96.155" type="appl" />
         <tli id="T125" time="96.80507142857142" type="appl" />
         <tli id="T126" time="97.45514285714286" type="appl" />
         <tli id="T127" time="98.10521428571428" type="appl" />
         <tli id="T128" time="98.75528571428572" type="appl" />
         <tli id="T129" time="99.40535714285714" type="appl" />
         <tli id="T130" time="100.05542857142858" type="appl" />
         <tli id="T131" time="100.7055" type="appl" />
         <tli id="T132" time="101.35557142857142" type="appl" />
         <tli id="T133" time="102.00564285714286" type="appl" />
         <tli id="T134" time="102.65571428571428" type="appl" />
         <tli id="T135" time="103.30578571428572" type="appl" />
         <tli id="T136" time="103.95585714285714" type="appl" />
         <tli id="T137" time="104.60592857142858" type="appl" />
         <tli id="T138" time="106.49333333333333" />
         <tli id="T139" time="107.95333333333333" />
         <tli id="T140" />
         <tli id="T141" />
         <tli id="T142" time="108.071" type="appl" />
         <tli id="T143" time="108.6902" type="appl" />
         <tli id="T144" time="109.3094" type="appl" />
         <tli id="T145" time="109.9286" type="appl" />
         <tli id="T146" time="110.5478" type="appl" />
         <tli id="T147" time="111.167" type="appl" />
         <tli id="T148" time="111.7862" type="appl" />
         <tli id="T149" time="112.4054" type="appl" />
         <tli id="T150" time="113.02459999999999" type="appl" />
         <tli id="T151" time="113.6438" type="appl" />
         <tli id="T152" time="114.26299999999999" type="appl" />
         <tli id="T153" time="114.8822" type="appl" />
         <tli id="T154" time="115.50139999999999" type="appl" />
         <tli id="T155" time="116.1206" type="appl" />
         <tli id="T156" time="116.7398" type="appl" />
         <tli id="T157" time="117.359" type="appl" />
         <tli id="T158" time="118.15725" type="appl" />
         <tli id="T159" time="118.9555" type="appl" />
         <tli id="T160" time="119.75375" type="appl" />
         <tli id="T161" time="120.552" type="appl" />
         <tli id="T162" time="120.97245454545455" type="appl" />
         <tli id="T163" time="121.3929090909091" type="appl" />
         <tli id="T164" time="121.81336363636365" type="appl" />
         <tli id="T165" time="122.2338181818182" type="appl" />
         <tli id="T166" time="122.65427272727274" type="appl" />
         <tli id="T167" time="123.07472727272727" type="appl" />
         <tli id="T168" time="123.49518181818182" type="appl" />
         <tli id="T169" time="123.91563636363637" type="appl" />
         <tli id="T170" time="124.33609090909091" type="appl" />
         <tli id="T171" time="124.75654545454546" type="appl" />
         <tli id="T172" time="125.177" type="appl" />
         <tli id="T173" time="125.9726" type="appl" />
         <tli id="T174" time="126.76820000000001" type="appl" />
         <tli id="T175" time="127.5638" type="appl" />
         <tli id="T176" time="128.3594" type="appl" />
         <tli id="T177" time="129.155" type="appl" />
         <tli id="T178" time="129.77" type="appl" />
         <tli id="T179" time="130.385" type="appl" />
         <tli id="T180" time="131.0" type="appl" />
         <tli id="T181" time="131.536" type="appl" />
         <tli id="T182" time="132.072" type="appl" />
         <tli id="T183" time="132.608" type="appl" />
         <tli id="T184" time="133.144" type="appl" />
         <tli id="T185" time="133.68" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-AnIM"
                      id="tx-AnIM"
                      speaker="AnIM"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-AnIM">
            <ts e="T7" id="Seg_0" n="sc" s="T1">
               <ts e="T7" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Munnʼak</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">kenne</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">min</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">onton</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">kelebin</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_19" n="HIAT:w" s="T6">dʼe</ts>
                  <nts id="Seg_20" n="HIAT:ip">.</nts>
                  <nts id="Seg_21" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T57" id="Seg_22" n="sc" s="T53">
               <ts e="T57" id="Seg_24" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_26" n="HIAT:w" s="T53">Togo</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_29" n="HIAT:w" s="T54">sožitelʼnica</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_32" n="HIAT:w" s="T55">bu͡olar</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_35" n="HIAT:w" s="T56">ol</ts>
                  <nts id="Seg_36" n="HIAT:ip">.</nts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T172" id="Seg_38" n="sc" s="T161">
               <ts e="T172" id="Seg_40" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_42" n="HIAT:w" s="T161">Küle</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_45" n="HIAT:w" s="T162">hɨtallar</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_48" n="HIAT:w" s="T163">algɨs</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_51" n="HIAT:w" s="T164">ɨrɨ͡ata</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_53" n="HIAT:ip">(</nts>
                  <ts e="T166" id="Seg_55" n="HIAT:w" s="T165">di-</ts>
                  <nts id="Seg_56" n="HIAT:ip">)</nts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_59" n="HIAT:w" s="T166">algɨs</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_62" n="HIAT:w" s="T167">ɨrɨ͡a</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_65" n="HIAT:w" s="T168">algɨs</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_68" n="HIAT:w" s="T169">ɨrɨ͡ata</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_70" n="HIAT:ip">(</nts>
                  <ts e="T171" id="Seg_72" n="HIAT:w" s="T170">ɨllaː-</ts>
                  <nts id="Seg_73" n="HIAT:ip">)</nts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_76" n="HIAT:w" s="T171">diller</ts>
                  <nts id="Seg_77" n="HIAT:ip">.</nts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T180" id="Seg_79" n="sc" s="T177">
               <ts e="T180" id="Seg_81" n="HIAT:u" s="T177">
                  <ts e="T178" id="Seg_83" n="HIAT:w" s="T177">Pesnju</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_86" n="HIAT:w" s="T178">govorju</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_89" n="HIAT:w" s="T179">spoj</ts>
                  <nts id="Seg_90" n="HIAT:ip">.</nts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-AnIM">
            <ts e="T7" id="Seg_92" n="sc" s="T1">
               <ts e="T2" id="Seg_94" n="e" s="T1">Munnʼak </ts>
               <ts e="T3" id="Seg_96" n="e" s="T2">kenne </ts>
               <ts e="T4" id="Seg_98" n="e" s="T3">min </ts>
               <ts e="T5" id="Seg_100" n="e" s="T4">onton </ts>
               <ts e="T6" id="Seg_102" n="e" s="T5">kelebin </ts>
               <ts e="T7" id="Seg_104" n="e" s="T6">dʼe. </ts>
            </ts>
            <ts e="T57" id="Seg_105" n="sc" s="T53">
               <ts e="T54" id="Seg_107" n="e" s="T53">Togo </ts>
               <ts e="T55" id="Seg_109" n="e" s="T54">sožitelʼnica </ts>
               <ts e="T56" id="Seg_111" n="e" s="T55">bu͡olar </ts>
               <ts e="T57" id="Seg_113" n="e" s="T56">ol. </ts>
            </ts>
            <ts e="T172" id="Seg_114" n="sc" s="T161">
               <ts e="T162" id="Seg_116" n="e" s="T161">Küle </ts>
               <ts e="T163" id="Seg_118" n="e" s="T162">hɨtallar </ts>
               <ts e="T164" id="Seg_120" n="e" s="T163">algɨs </ts>
               <ts e="T165" id="Seg_122" n="e" s="T164">ɨrɨ͡ata </ts>
               <ts e="T166" id="Seg_124" n="e" s="T165">(di-) </ts>
               <ts e="T167" id="Seg_126" n="e" s="T166">algɨs </ts>
               <ts e="T168" id="Seg_128" n="e" s="T167">ɨrɨ͡a </ts>
               <ts e="T169" id="Seg_130" n="e" s="T168">algɨs </ts>
               <ts e="T170" id="Seg_132" n="e" s="T169">ɨrɨ͡ata </ts>
               <ts e="T171" id="Seg_134" n="e" s="T170">(ɨllaː-) </ts>
               <ts e="T172" id="Seg_136" n="e" s="T171">diller. </ts>
            </ts>
            <ts e="T180" id="Seg_137" n="sc" s="T177">
               <ts e="T178" id="Seg_139" n="e" s="T177">Pesnju </ts>
               <ts e="T179" id="Seg_141" n="e" s="T178">govorju </ts>
               <ts e="T180" id="Seg_143" n="e" s="T179">spoj. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-AnIM">
            <ta e="T7" id="Seg_144" s="T1">AnSP_AnIM_2009_Girl_nar.AnIM.001 (001)</ta>
            <ta e="T57" id="Seg_145" s="T53">AnSP_AnIM_2009_Girl_nar.AnIM.002 (008)</ta>
            <ta e="T172" id="Seg_146" s="T161">AnSP_AnIM_2009_Girl_nar.AnIM.003 (022)</ta>
            <ta e="T180" id="Seg_147" s="T177">AnSP_AnIM_2009_Girl_nar.AnIM.004 (024.001)</ta>
         </annotation>
         <annotation name="st" tierref="st-AnIM">
            <ta e="T7" id="Seg_148" s="T1">Жен: мунньак кэннэ мин онтон кэлэбин дьэ</ta>
            <ta e="T57" id="Seg_149" s="T53">Жен: того сожительница буолар ол.</ta>
            <ta e="T172" id="Seg_150" s="T161">Жен: Күлэ һыталлар алгыс ырыата ди алгыс ырыа алгыс ырыата ылла диллэр.</ta>
            <ta e="T180" id="Seg_151" s="T177">Жен: песню говорю спой. </ta>
         </annotation>
         <annotation name="ts" tierref="ts-AnIM">
            <ta e="T7" id="Seg_152" s="T1">Munnʼak kenne min onton kelebin dʼe. </ta>
            <ta e="T57" id="Seg_153" s="T53">Togo sožitelʼnica bu͡olar ol. </ta>
            <ta e="T172" id="Seg_154" s="T161">Küle hɨtallar algɨs ɨrɨ͡ata (di-) algɨs ɨrɨ͡a algɨs ɨrɨ͡ata (ɨllaː-) diller. </ta>
            <ta e="T180" id="Seg_155" s="T177">Pesnju govorju spoj. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-AnIM">
            <ta e="T2" id="Seg_156" s="T1">munnʼak</ta>
            <ta e="T3" id="Seg_157" s="T2">kenne</ta>
            <ta e="T4" id="Seg_158" s="T3">min</ta>
            <ta e="T5" id="Seg_159" s="T4">on-ton</ta>
            <ta e="T6" id="Seg_160" s="T5">kel-e-bin</ta>
            <ta e="T7" id="Seg_161" s="T6">dʼe</ta>
            <ta e="T54" id="Seg_162" s="T53">togo</ta>
            <ta e="T56" id="Seg_163" s="T55">bu͡ol-ar</ta>
            <ta e="T57" id="Seg_164" s="T56">ol</ta>
            <ta e="T162" id="Seg_165" s="T161">kül-e</ta>
            <ta e="T163" id="Seg_166" s="T162">hɨt-al-lar</ta>
            <ta e="T164" id="Seg_167" s="T163">algɨs</ta>
            <ta e="T165" id="Seg_168" s="T164">ɨrɨ͡a-ta</ta>
            <ta e="T166" id="Seg_169" s="T165">di</ta>
            <ta e="T167" id="Seg_170" s="T166">algɨs</ta>
            <ta e="T168" id="Seg_171" s="T167">ɨrɨ͡a</ta>
            <ta e="T169" id="Seg_172" s="T168">algɨs</ta>
            <ta e="T170" id="Seg_173" s="T169">ɨrɨ͡a-ta</ta>
            <ta e="T171" id="Seg_174" s="T170">ɨllaː</ta>
            <ta e="T172" id="Seg_175" s="T171">di-l-ler</ta>
         </annotation>
         <annotation name="mp" tierref="mp-AnIM">
            <ta e="T2" id="Seg_176" s="T1">munnʼak</ta>
            <ta e="T3" id="Seg_177" s="T2">genne</ta>
            <ta e="T4" id="Seg_178" s="T3">min</ta>
            <ta e="T5" id="Seg_179" s="T4">ol-ttAn</ta>
            <ta e="T6" id="Seg_180" s="T5">kel-A-BIn</ta>
            <ta e="T7" id="Seg_181" s="T6">dʼe</ta>
            <ta e="T54" id="Seg_182" s="T53">togo</ta>
            <ta e="T56" id="Seg_183" s="T55">bu͡ol-Ar</ta>
            <ta e="T57" id="Seg_184" s="T56">ol</ta>
            <ta e="T162" id="Seg_185" s="T161">kül-A</ta>
            <ta e="T163" id="Seg_186" s="T162">hɨt-Ar-LAr</ta>
            <ta e="T164" id="Seg_187" s="T163">algɨs</ta>
            <ta e="T165" id="Seg_188" s="T164">ɨrɨ͡a-tA</ta>
            <ta e="T166" id="Seg_189" s="T165">di͡e</ta>
            <ta e="T167" id="Seg_190" s="T166">algɨs</ta>
            <ta e="T168" id="Seg_191" s="T167">ɨrɨ͡a</ta>
            <ta e="T169" id="Seg_192" s="T168">algɨs</ta>
            <ta e="T170" id="Seg_193" s="T169">ɨrɨ͡a-tA</ta>
            <ta e="T171" id="Seg_194" s="T170">ɨllaː</ta>
            <ta e="T172" id="Seg_195" s="T171">di͡e-Ar-LAr</ta>
         </annotation>
         <annotation name="ge" tierref="ge-AnIM">
            <ta e="T2" id="Seg_196" s="T1">gathering.[NOM]</ta>
            <ta e="T3" id="Seg_197" s="T2">after</ta>
            <ta e="T4" id="Seg_198" s="T3">1SG.[NOM]</ta>
            <ta e="T5" id="Seg_199" s="T4">that-ABL</ta>
            <ta e="T6" id="Seg_200" s="T5">come-PRS-1SG</ta>
            <ta e="T7" id="Seg_201" s="T6">well</ta>
            <ta e="T54" id="Seg_202" s="T53">why</ta>
            <ta e="T56" id="Seg_203" s="T55">be-PRS.[3SG]</ta>
            <ta e="T57" id="Seg_204" s="T56">that</ta>
            <ta e="T162" id="Seg_205" s="T161">laugh-CVB.SIM</ta>
            <ta e="T163" id="Seg_206" s="T162">lie.down-PRS-3PL</ta>
            <ta e="T164" id="Seg_207" s="T163">incantation.[NOM]</ta>
            <ta e="T165" id="Seg_208" s="T164">song-3SG.[NOM]</ta>
            <ta e="T166" id="Seg_209" s="T165">say</ta>
            <ta e="T167" id="Seg_210" s="T166">incantation.[NOM]</ta>
            <ta e="T168" id="Seg_211" s="T167">song.[NOM]</ta>
            <ta e="T169" id="Seg_212" s="T168">incantation.[NOM]</ta>
            <ta e="T170" id="Seg_213" s="T169">song-3SG.[NOM]</ta>
            <ta e="T171" id="Seg_214" s="T170">sing</ta>
            <ta e="T172" id="Seg_215" s="T171">say-PRS-3PL</ta>
         </annotation>
         <annotation name="gg" tierref="gg-AnIM">
            <ta e="T2" id="Seg_216" s="T1">Versammlung.[NOM]</ta>
            <ta e="T3" id="Seg_217" s="T2">nachdem</ta>
            <ta e="T4" id="Seg_218" s="T3">1SG.[NOM]</ta>
            <ta e="T5" id="Seg_219" s="T4">jenes-ABL</ta>
            <ta e="T6" id="Seg_220" s="T5">kommen-PRS-1SG</ta>
            <ta e="T7" id="Seg_221" s="T6">doch</ta>
            <ta e="T54" id="Seg_222" s="T53">warum</ta>
            <ta e="T56" id="Seg_223" s="T55">sein-PRS.[3SG]</ta>
            <ta e="T57" id="Seg_224" s="T56">jenes</ta>
            <ta e="T162" id="Seg_225" s="T161">lachen-CVB.SIM</ta>
            <ta e="T163" id="Seg_226" s="T162">sich.hinlegen-PRS-3PL</ta>
            <ta e="T164" id="Seg_227" s="T163">Beschwörung.[NOM]</ta>
            <ta e="T165" id="Seg_228" s="T164">Lied-3SG.[NOM]</ta>
            <ta e="T166" id="Seg_229" s="T165">sagen</ta>
            <ta e="T167" id="Seg_230" s="T166">Beschwörung.[NOM]</ta>
            <ta e="T168" id="Seg_231" s="T167">Lied.[NOM]</ta>
            <ta e="T169" id="Seg_232" s="T168">Beschwörung.[NOM]</ta>
            <ta e="T170" id="Seg_233" s="T169">Lied-3SG.[NOM]</ta>
            <ta e="T171" id="Seg_234" s="T170">singen</ta>
            <ta e="T172" id="Seg_235" s="T171">sagen-PRS-3PL</ta>
         </annotation>
         <annotation name="gr" tierref="gr-AnIM">
            <ta e="T2" id="Seg_236" s="T1">сход.[NOM]</ta>
            <ta e="T3" id="Seg_237" s="T2">после.того</ta>
            <ta e="T4" id="Seg_238" s="T3">1SG.[NOM]</ta>
            <ta e="T5" id="Seg_239" s="T4">тот-ABL</ta>
            <ta e="T6" id="Seg_240" s="T5">приходить-PRS-1SG</ta>
            <ta e="T7" id="Seg_241" s="T6">вот</ta>
            <ta e="T54" id="Seg_242" s="T53">почему</ta>
            <ta e="T56" id="Seg_243" s="T55">быть-PRS.[3SG]</ta>
            <ta e="T57" id="Seg_244" s="T56">тот</ta>
            <ta e="T162" id="Seg_245" s="T161">смеяться-CVB.SIM</ta>
            <ta e="T163" id="Seg_246" s="T162">ложиться-PRS-3PL</ta>
            <ta e="T164" id="Seg_247" s="T163">заклинание.[NOM]</ta>
            <ta e="T165" id="Seg_248" s="T164">песня-3SG.[NOM]</ta>
            <ta e="T166" id="Seg_249" s="T165">говорить</ta>
            <ta e="T167" id="Seg_250" s="T166">заклинание.[NOM]</ta>
            <ta e="T168" id="Seg_251" s="T167">песня.[NOM]</ta>
            <ta e="T169" id="Seg_252" s="T168">заклинание.[NOM]</ta>
            <ta e="T170" id="Seg_253" s="T169">песня-3SG.[NOM]</ta>
            <ta e="T171" id="Seg_254" s="T170">петь</ta>
            <ta e="T172" id="Seg_255" s="T171">говорить-PRS-3PL</ta>
         </annotation>
         <annotation name="mc" tierref="mc-AnIM">
            <ta e="T2" id="Seg_256" s="T1">n.[n:case]</ta>
            <ta e="T3" id="Seg_257" s="T2">post</ta>
            <ta e="T4" id="Seg_258" s="T3">pers.[pro:case]</ta>
            <ta e="T5" id="Seg_259" s="T4">dempro-pro:case</ta>
            <ta e="T6" id="Seg_260" s="T5">v-v:tense-v:pred.pn</ta>
            <ta e="T7" id="Seg_261" s="T6">ptcl</ta>
            <ta e="T54" id="Seg_262" s="T53">que</ta>
            <ta e="T56" id="Seg_263" s="T55">v-v:tense.[v:pred.pn]</ta>
            <ta e="T57" id="Seg_264" s="T56">dempro</ta>
            <ta e="T162" id="Seg_265" s="T161">v-v:cvb</ta>
            <ta e="T163" id="Seg_266" s="T162">v-v:tense-v:pred.pn</ta>
            <ta e="T164" id="Seg_267" s="T163">n.[n:case]</ta>
            <ta e="T165" id="Seg_268" s="T164">n-n:(poss).[n:case]</ta>
            <ta e="T166" id="Seg_269" s="T165">v</ta>
            <ta e="T167" id="Seg_270" s="T166">n.[n:case]</ta>
            <ta e="T168" id="Seg_271" s="T167">n.[n:case]</ta>
            <ta e="T169" id="Seg_272" s="T168">n.[n:case]</ta>
            <ta e="T170" id="Seg_273" s="T169">n-n:(poss).[n:case]</ta>
            <ta e="T171" id="Seg_274" s="T170">v</ta>
            <ta e="T172" id="Seg_275" s="T171">v-v:tense-v:pred.pn</ta>
         </annotation>
         <annotation name="ps" tierref="ps-AnIM">
            <ta e="T2" id="Seg_276" s="T1">n</ta>
            <ta e="T3" id="Seg_277" s="T2">post</ta>
            <ta e="T4" id="Seg_278" s="T3">pers</ta>
            <ta e="T5" id="Seg_279" s="T4">dempro</ta>
            <ta e="T6" id="Seg_280" s="T5">v</ta>
            <ta e="T7" id="Seg_281" s="T6">ptcl</ta>
            <ta e="T54" id="Seg_282" s="T53">que</ta>
            <ta e="T56" id="Seg_283" s="T55">cop</ta>
            <ta e="T57" id="Seg_284" s="T56">dempro</ta>
            <ta e="T162" id="Seg_285" s="T161">v</ta>
            <ta e="T163" id="Seg_286" s="T162">aux</ta>
            <ta e="T164" id="Seg_287" s="T163">n</ta>
            <ta e="T165" id="Seg_288" s="T164">n</ta>
            <ta e="T166" id="Seg_289" s="T165">v</ta>
            <ta e="T167" id="Seg_290" s="T166">n</ta>
            <ta e="T168" id="Seg_291" s="T167">n</ta>
            <ta e="T169" id="Seg_292" s="T168">n</ta>
            <ta e="T170" id="Seg_293" s="T169">n</ta>
            <ta e="T171" id="Seg_294" s="T170">v</ta>
            <ta e="T172" id="Seg_295" s="T171">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-AnIM" />
         <annotation name="SyF" tierref="SyF-AnIM" />
         <annotation name="IST" tierref="IST-AnIM" />
         <annotation name="Top" tierref="Top-AnIM" />
         <annotation name="Foc" tierref="Foc-AnIM" />
         <annotation name="BOR" tierref="BOR-AnIM" />
         <annotation name="BOR-Phon" tierref="BOR-Phon-AnIM" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-AnIM" />
         <annotation name="CS" tierref="CS-AnIM" />
         <annotation name="fe" tierref="fe-AnIM">
            <ta e="T7" id="Seg_296" s="T1">After the meeting I came from there.</ta>
            <ta e="T57" id="Seg_297" s="T53">Why a housemate?</ta>
            <ta e="T172" id="Seg_298" s="T161">They begin to laugh, sing an incantation, sing an incantation an incantation, they say.</ta>
            <ta e="T180" id="Seg_299" s="T177">I say sing a song.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-AnIM">
            <ta e="T7" id="Seg_300" s="T1">Nach der Versammlung komme ich von dort.</ta>
            <ta e="T57" id="Seg_301" s="T53">Warum eine Mitbewohnerin?</ta>
            <ta e="T172" id="Seg_302" s="T161">Sie fangen an zu lachen, eine Beschwörung zu singen, sie singen eine Beschwörung.</ta>
            <ta e="T180" id="Seg_303" s="T177">Ich sage, ich singe ein Lied.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-AnIM" />
         <annotation name="ltr" tierref="ltr-AnIM">
            <ta e="T7" id="Seg_304" s="T1">Жен:с собрания я пришел оттуда </ta>
            <ta e="T57" id="Seg_305" s="T53">Жен: почему сожительница будет.</ta>
            <ta e="T172" id="Seg_306" s="T161">Жен: смеются лежат алгыс песню говорят алгыс песню алгыс песню спой говорят.</ta>
            <ta e="T180" id="Seg_307" s="T177">Жен: песню гворят спой. </ta>
         </annotation>
         <annotation name="nt" tierref="nt-AnIM" />
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-AnSP"
                      id="tx-AnSP"
                      speaker="AnSP"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-AnSP">
            <ts e="T53" id="Seg_308" n="sc" s="T7">
               <ts e="T12" id="Seg_310" n="HIAT:u" s="T7">
                  <ts e="T8" id="Seg_312" n="HIAT:w" s="T7">Heː</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_315" n="HIAT:w" s="T8">otto</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_317" n="HIAT:ip">(</nts>
                  <ts e="T10" id="Seg_319" n="HIAT:w" s="T9">kɨr-</ts>
                  <nts id="Seg_320" n="HIAT:ip">)</nts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_323" n="HIAT:w" s="T10">kɨrakaːnnɨk</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_326" n="HIAT:w" s="T11">kɨra</ts>
                  <nts id="Seg_327" n="HIAT:ip">.</nts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_330" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_332" n="HIAT:w" s="T12">Eː</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_335" n="HIAT:w" s="T13">kɨːska</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_338" n="HIAT:w" s="T14">di͡e</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_341" n="HIAT:w" s="T15">kɨːsčaːŋŋa</ts>
                  <nts id="Seg_342" n="HIAT:ip">,</nts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_345" n="HIAT:w" s="T16">ol</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_348" n="HIAT:w" s="T17">kehiːni</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_351" n="HIAT:w" s="T18">egelen</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_354" n="HIAT:w" s="T19">kelebin</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_357" n="HIAT:w" s="T20">bu</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_360" n="HIAT:w" s="T21">ostolgo</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_363" n="HIAT:w" s="T22">olorobun</ts>
                  <nts id="Seg_364" n="HIAT:ip">.</nts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_367" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_369" n="HIAT:w" s="T23">Kɨːhɨ</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_372" n="HIAT:w" s="T24">köröbün</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_375" n="HIAT:w" s="T25">dʼaktarbɨn</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_378" n="HIAT:w" s="T26">kördüm</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_381" n="HIAT:w" s="T27">barɨkaːnɨ</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_384" n="HIAT:w" s="T28">ü͡öretellerin</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_387" n="HIAT:w" s="T29">kajdak</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_390" n="HIAT:w" s="T30">jazɨgɨ</ts>
                  <nts id="Seg_391" n="HIAT:ip">.</nts>
                  <nts id="Seg_392" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_394" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_396" n="HIAT:w" s="T31">Ljuboj</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_399" n="HIAT:w" s="T32">jazɨk</ts>
                  <nts id="Seg_400" n="HIAT:ip">,</nts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_403" n="HIAT:w" s="T33">kanʼaːn</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_406" n="HIAT:w" s="T34">da</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_409" n="HIAT:w" s="T35">ogom</ts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_412" n="HIAT:w" s="T36">ü͡örette</ts>
                  <nts id="Seg_413" n="HIAT:ip">.</nts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T51" id="Seg_416" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_418" n="HIAT:w" s="T37">Moja</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_420" n="HIAT:ip">(</nts>
                  <ts e="T39" id="Seg_422" n="HIAT:w" s="T38">s-</ts>
                  <nts id="Seg_423" n="HIAT:ip">)</nts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_426" n="HIAT:w" s="T39">bu</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_429" n="HIAT:w" s="T40">ke</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_432" n="HIAT:w" s="T41">otto</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_435" n="HIAT:w" s="T42">kajdak</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_438" n="HIAT:w" s="T43">di͡eččilerij</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_441" n="HIAT:w" s="T44">bihi͡eke</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_444" n="HIAT:w" s="T45">dʼi͡eleːk</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_447" n="HIAT:w" s="T46">kɨːs</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_450" n="HIAT:w" s="T47">ke</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_453" n="HIAT:w" s="T48">di͡eččiler</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_456" n="HIAT:w" s="T49">e</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_459" n="HIAT:w" s="T50">de</ts>
                  <nts id="Seg_460" n="HIAT:ip">.</nts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_463" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_465" n="HIAT:w" s="T51">Sožitelʼnica</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_468" n="HIAT:w" s="T52">da</ts>
                  <nts id="Seg_469" n="HIAT:ip">?</nts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T161" id="Seg_471" n="sc" s="T57">
               <ts e="T60" id="Seg_473" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_475" n="HIAT:w" s="T57">Po</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_478" n="HIAT:w" s="T58">kvartire</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_481" n="HIAT:w" s="T59">no</ts>
                  <nts id="Seg_482" n="HIAT:ip">.</nts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_485" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_487" n="HIAT:w" s="T60">Gini</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_490" n="HIAT:w" s="T61">ü͡örde</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_493" n="HIAT:w" s="T62">bu</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_496" n="HIAT:w" s="T63">min</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_499" n="HIAT:w" s="T64">egelbipper</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_502" n="HIAT:w" s="T65">ol</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_505" n="HIAT:w" s="T66">emeːksin</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_508" n="HIAT:w" s="T67">emi͡e</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_511" n="HIAT:w" s="T68">ɨːtaːččɨ</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_514" n="HIAT:w" s="T69">dʼaktar</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_517" n="HIAT:w" s="T70">üčügejdik</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_520" n="HIAT:w" s="T71">hanaːtɨn</ts>
                  <nts id="Seg_521" n="HIAT:ip">.</nts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T82" id="Seg_524" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_526" n="HIAT:w" s="T72">Onton</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_529" n="HIAT:w" s="T73">bu͡olla</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_532" n="HIAT:w" s="T74">tu͡ok</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_535" n="HIAT:w" s="T75">di͡ek</ts>
                  <nts id="Seg_536" n="HIAT:ip">,</nts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_539" n="HIAT:w" s="T76">po</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_542" n="HIAT:w" s="T77">priljotu</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_545" n="HIAT:w" s="T78">eščjo</ts>
                  <nts id="Seg_546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_548" n="HIAT:w" s="T79">raz</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_551" n="HIAT:w" s="T80">körsön</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_554" n="HIAT:w" s="T81">ihi͡eppit</ts>
                  <nts id="Seg_555" n="HIAT:ip">.</nts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T92" id="Seg_558" n="HIAT:u" s="T82">
                  <ts e="T83" id="Seg_560" n="HIAT:w" s="T82">Onton</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_563" n="HIAT:w" s="T83">želajdɨːbɨn</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_566" n="HIAT:w" s="T84">bu</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_569" n="HIAT:w" s="T85">kɨːska</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_572" n="HIAT:w" s="T86">üčügej</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_575" n="HIAT:w" s="T87">u͡olla</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_578" n="HIAT:w" s="T88">bulu͡okka</ts>
                  <nts id="Seg_579" n="HIAT:ip">,</nts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_582" n="HIAT:w" s="T89">če</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_585" n="HIAT:w" s="T90">menʼiːleːk</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_588" n="HIAT:w" s="T91">hogusta</ts>
                  <nts id="Seg_589" n="HIAT:ip">.</nts>
                  <nts id="Seg_590" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T97" id="Seg_592" n="HIAT:u" s="T92">
                  <ts e="T93" id="Seg_594" n="HIAT:w" s="T92">Onnuk</ts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_597" n="HIAT:w" s="T93">dʼeŋke</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_600" n="HIAT:w" s="T94">dʼeŋketik</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_603" n="HIAT:w" s="T95">eteŋŋe</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_606" n="HIAT:w" s="T96">tijdegine</ts>
                  <nts id="Seg_607" n="HIAT:ip">.</nts>
                  <nts id="Seg_608" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T108" id="Seg_610" n="HIAT:u" s="T97">
                  <ts e="T98" id="Seg_612" n="HIAT:w" s="T97">Kün</ts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_615" n="HIAT:w" s="T98">da</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_618" n="HIAT:w" s="T99">tɨgan</ts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_621" n="HIAT:w" s="T100">turara</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_624" n="HIAT:w" s="T101">üčügej</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_627" n="HIAT:w" s="T102">bejete</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_630" n="HIAT:w" s="T103">bu</ts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_633" n="HIAT:w" s="T104">kepsete</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_636" n="HIAT:w" s="T105">da</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_639" n="HIAT:w" s="T106">olororo</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_642" n="HIAT:w" s="T107">üčügej</ts>
                  <nts id="Seg_643" n="HIAT:ip">.</nts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T119" id="Seg_646" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_648" n="HIAT:w" s="T108">Kihi</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_651" n="HIAT:w" s="T109">da</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_654" n="HIAT:w" s="T110">kihini</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_657" n="HIAT:w" s="T111">körsön</ts>
                  <nts id="Seg_658" n="HIAT:ip">,</nts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_661" n="HIAT:w" s="T112">elbek</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_664" n="HIAT:w" s="T113">kihi</ts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_667" n="HIAT:w" s="T114">haŋatɨn</ts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_670" n="HIAT:w" s="T115">bulaːn</ts>
                  <nts id="Seg_671" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_673" n="HIAT:w" s="T116">omuk</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_676" n="HIAT:w" s="T117">ügüs</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_679" n="HIAT:w" s="T118">bu͡o</ts>
                  <nts id="Seg_680" n="HIAT:ip">.</nts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T120" id="Seg_683" n="HIAT:u" s="T119">
                  <ts e="T120" id="Seg_685" n="HIAT:w" s="T119">Planeta</ts>
                  <nts id="Seg_686" n="HIAT:ip">.</nts>
                  <nts id="Seg_687" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T124" id="Seg_689" n="HIAT:u" s="T120">
                  <ts e="T121" id="Seg_691" n="HIAT:w" s="T120">Bu</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_694" n="HIAT:w" s="T121">ke</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_697" n="HIAT:w" s="T122">otto</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_700" n="HIAT:w" s="T123">di͡etekke</ts>
                  <nts id="Seg_701" n="HIAT:ip">.</nts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T138" id="Seg_704" n="HIAT:u" s="T124">
                  <ts e="T125" id="Seg_706" n="HIAT:w" s="T124">Kajdak</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_709" n="HIAT:w" s="T125">di͡ekpinij</ts>
                  <nts id="Seg_710" n="HIAT:ip">,</nts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_713" n="HIAT:w" s="T126">omuk</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_716" n="HIAT:w" s="T127">bu͡o</ts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_719" n="HIAT:w" s="T128">olus</ts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_722" n="HIAT:w" s="T129">bu͡o</ts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_725" n="HIAT:w" s="T130">urut</ts>
                  <nts id="Seg_726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_728" n="HIAT:w" s="T131">bu͡o</ts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_731" n="HIAT:w" s="T132">ügüs</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_734" n="HIAT:w" s="T133">omuk</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_737" n="HIAT:w" s="T134">ete</ts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_740" n="HIAT:w" s="T135">bu͡o</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_743" n="HIAT:w" s="T136">ü͡örener</ts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_746" n="HIAT:w" s="T137">erdekke</ts>
                  <nts id="Seg_747" n="HIAT:ip">.</nts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T142" id="Seg_750" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_752" n="HIAT:w" s="T138">Anɨ</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_755" n="HIAT:w" s="T139">össü͡ö</ts>
                  <nts id="Seg_756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_758" n="HIAT:w" s="T140">üksen</ts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_761" n="HIAT:w" s="T141">iher</ts>
                  <nts id="Seg_762" n="HIAT:ip">.</nts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_765" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_767" n="HIAT:w" s="T142">Anɨ</ts>
                  <nts id="Seg_768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_770" n="HIAT:w" s="T143">bɨhatɨn</ts>
                  <nts id="Seg_771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_773" n="HIAT:w" s="T144">haŋarbappɨn</ts>
                  <nts id="Seg_774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_776" n="HIAT:w" s="T145">kanta</ts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_779" n="HIAT:w" s="T146">anɨ</ts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_782" n="HIAT:w" s="T147">össü͡ö</ts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_785" n="HIAT:w" s="T148">da</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_788" n="HIAT:w" s="T149">bulan</ts>
                  <nts id="Seg_789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_791" n="HIAT:w" s="T150">iheller</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_794" n="HIAT:w" s="T151">kihileri</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_797" n="HIAT:w" s="T152">ke</ts>
                  <nts id="Seg_798" n="HIAT:ip">,</nts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_801" n="HIAT:w" s="T153">gde</ts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_804" n="HIAT:w" s="T154">to</ts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_807" n="HIAT:w" s="T155">na</ts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_810" n="HIAT:w" s="T156">ostrovax</ts>
                  <nts id="Seg_811" n="HIAT:ip">.</nts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_814" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_816" n="HIAT:w" s="T157">Kihi</ts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_819" n="HIAT:w" s="T158">hüter</ts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_822" n="HIAT:w" s="T159">kihiler</ts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_825" n="HIAT:w" s="T160">bulullallar</ts>
                  <nts id="Seg_826" n="HIAT:ip">.</nts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T177" id="Seg_828" n="sc" s="T172">
               <ts e="T177" id="Seg_830" n="HIAT:u" s="T172">
                  <ts e="T173" id="Seg_832" n="HIAT:w" s="T172">Tugu</ts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_835" n="HIAT:w" s="T173">ɨllɨ͡akpɨnɨj</ts>
                  <nts id="Seg_836" n="HIAT:ip">,</nts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_839" n="HIAT:w" s="T174">če</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_842" n="HIAT:w" s="T175">spetʼ</ts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_845" n="HIAT:w" s="T176">to</ts>
                  <nts id="Seg_846" n="HIAT:ip">?</nts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-AnSP">
            <ts e="T53" id="Seg_848" n="sc" s="T7">
               <ts e="T8" id="Seg_850" n="e" s="T7">Heː </ts>
               <ts e="T9" id="Seg_852" n="e" s="T8">otto </ts>
               <ts e="T10" id="Seg_854" n="e" s="T9">(kɨr-) </ts>
               <ts e="T11" id="Seg_856" n="e" s="T10">kɨrakaːnnɨk </ts>
               <ts e="T12" id="Seg_858" n="e" s="T11">kɨra. </ts>
               <ts e="T13" id="Seg_860" n="e" s="T12">Eː </ts>
               <ts e="T14" id="Seg_862" n="e" s="T13">kɨːska </ts>
               <ts e="T15" id="Seg_864" n="e" s="T14">di͡e </ts>
               <ts e="T16" id="Seg_866" n="e" s="T15">kɨːsčaːŋŋa, </ts>
               <ts e="T17" id="Seg_868" n="e" s="T16">ol </ts>
               <ts e="T18" id="Seg_870" n="e" s="T17">kehiːni </ts>
               <ts e="T19" id="Seg_872" n="e" s="T18">egelen </ts>
               <ts e="T20" id="Seg_874" n="e" s="T19">kelebin </ts>
               <ts e="T21" id="Seg_876" n="e" s="T20">bu </ts>
               <ts e="T22" id="Seg_878" n="e" s="T21">ostolgo </ts>
               <ts e="T23" id="Seg_880" n="e" s="T22">olorobun. </ts>
               <ts e="T24" id="Seg_882" n="e" s="T23">Kɨːhɨ </ts>
               <ts e="T25" id="Seg_884" n="e" s="T24">köröbün </ts>
               <ts e="T26" id="Seg_886" n="e" s="T25">dʼaktarbɨn </ts>
               <ts e="T27" id="Seg_888" n="e" s="T26">kördüm </ts>
               <ts e="T28" id="Seg_890" n="e" s="T27">barɨkaːnɨ </ts>
               <ts e="T29" id="Seg_892" n="e" s="T28">ü͡öretellerin </ts>
               <ts e="T30" id="Seg_894" n="e" s="T29">kajdak </ts>
               <ts e="T31" id="Seg_896" n="e" s="T30">jazɨgɨ. </ts>
               <ts e="T32" id="Seg_898" n="e" s="T31">Ljuboj </ts>
               <ts e="T33" id="Seg_900" n="e" s="T32">jazɨk, </ts>
               <ts e="T34" id="Seg_902" n="e" s="T33">kanʼaːn </ts>
               <ts e="T35" id="Seg_904" n="e" s="T34">da </ts>
               <ts e="T36" id="Seg_906" n="e" s="T35">ogom </ts>
               <ts e="T37" id="Seg_908" n="e" s="T36">ü͡örette. </ts>
               <ts e="T38" id="Seg_910" n="e" s="T37">Moja </ts>
               <ts e="T39" id="Seg_912" n="e" s="T38">(s-) </ts>
               <ts e="T40" id="Seg_914" n="e" s="T39">bu </ts>
               <ts e="T41" id="Seg_916" n="e" s="T40">ke </ts>
               <ts e="T42" id="Seg_918" n="e" s="T41">otto </ts>
               <ts e="T43" id="Seg_920" n="e" s="T42">kajdak </ts>
               <ts e="T44" id="Seg_922" n="e" s="T43">di͡eččilerij </ts>
               <ts e="T45" id="Seg_924" n="e" s="T44">bihi͡eke </ts>
               <ts e="T46" id="Seg_926" n="e" s="T45">dʼi͡eleːk </ts>
               <ts e="T47" id="Seg_928" n="e" s="T46">kɨːs </ts>
               <ts e="T48" id="Seg_930" n="e" s="T47">ke </ts>
               <ts e="T49" id="Seg_932" n="e" s="T48">di͡eččiler </ts>
               <ts e="T50" id="Seg_934" n="e" s="T49">e </ts>
               <ts e="T51" id="Seg_936" n="e" s="T50">de. </ts>
               <ts e="T52" id="Seg_938" n="e" s="T51">Sožitelʼnica </ts>
               <ts e="T53" id="Seg_940" n="e" s="T52">da? </ts>
            </ts>
            <ts e="T161" id="Seg_941" n="sc" s="T57">
               <ts e="T58" id="Seg_943" n="e" s="T57">Po </ts>
               <ts e="T59" id="Seg_945" n="e" s="T58">kvartire </ts>
               <ts e="T60" id="Seg_947" n="e" s="T59">no. </ts>
               <ts e="T61" id="Seg_949" n="e" s="T60">Gini </ts>
               <ts e="T62" id="Seg_951" n="e" s="T61">ü͡örde </ts>
               <ts e="T63" id="Seg_953" n="e" s="T62">bu </ts>
               <ts e="T64" id="Seg_955" n="e" s="T63">min </ts>
               <ts e="T65" id="Seg_957" n="e" s="T64">egelbipper </ts>
               <ts e="T66" id="Seg_959" n="e" s="T65">ol </ts>
               <ts e="T67" id="Seg_961" n="e" s="T66">emeːksin </ts>
               <ts e="T68" id="Seg_963" n="e" s="T67">emi͡e </ts>
               <ts e="T69" id="Seg_965" n="e" s="T68">ɨːtaːččɨ </ts>
               <ts e="T70" id="Seg_967" n="e" s="T69">dʼaktar </ts>
               <ts e="T71" id="Seg_969" n="e" s="T70">üčügejdik </ts>
               <ts e="T72" id="Seg_971" n="e" s="T71">hanaːtɨn. </ts>
               <ts e="T73" id="Seg_973" n="e" s="T72">Onton </ts>
               <ts e="T74" id="Seg_975" n="e" s="T73">bu͡olla </ts>
               <ts e="T75" id="Seg_977" n="e" s="T74">tu͡ok </ts>
               <ts e="T76" id="Seg_979" n="e" s="T75">di͡ek, </ts>
               <ts e="T77" id="Seg_981" n="e" s="T76">po </ts>
               <ts e="T78" id="Seg_983" n="e" s="T77">priljotu </ts>
               <ts e="T79" id="Seg_985" n="e" s="T78">eščjo </ts>
               <ts e="T80" id="Seg_987" n="e" s="T79">raz </ts>
               <ts e="T81" id="Seg_989" n="e" s="T80">körsön </ts>
               <ts e="T82" id="Seg_991" n="e" s="T81">ihi͡eppit. </ts>
               <ts e="T83" id="Seg_993" n="e" s="T82">Onton </ts>
               <ts e="T84" id="Seg_995" n="e" s="T83">želajdɨːbɨn </ts>
               <ts e="T85" id="Seg_997" n="e" s="T84">bu </ts>
               <ts e="T86" id="Seg_999" n="e" s="T85">kɨːska </ts>
               <ts e="T87" id="Seg_1001" n="e" s="T86">üčügej </ts>
               <ts e="T88" id="Seg_1003" n="e" s="T87">u͡olla </ts>
               <ts e="T89" id="Seg_1005" n="e" s="T88">bulu͡okka, </ts>
               <ts e="T90" id="Seg_1007" n="e" s="T89">če </ts>
               <ts e="T91" id="Seg_1009" n="e" s="T90">menʼiːleːk </ts>
               <ts e="T92" id="Seg_1011" n="e" s="T91">hogusta. </ts>
               <ts e="T93" id="Seg_1013" n="e" s="T92">Onnuk </ts>
               <ts e="T94" id="Seg_1015" n="e" s="T93">dʼeŋke </ts>
               <ts e="T95" id="Seg_1017" n="e" s="T94">dʼeŋketik </ts>
               <ts e="T96" id="Seg_1019" n="e" s="T95">eteŋŋe </ts>
               <ts e="T97" id="Seg_1021" n="e" s="T96">tijdegine. </ts>
               <ts e="T98" id="Seg_1023" n="e" s="T97">Kün </ts>
               <ts e="T99" id="Seg_1025" n="e" s="T98">da </ts>
               <ts e="T100" id="Seg_1027" n="e" s="T99">tɨgan </ts>
               <ts e="T101" id="Seg_1029" n="e" s="T100">turara </ts>
               <ts e="T102" id="Seg_1031" n="e" s="T101">üčügej </ts>
               <ts e="T103" id="Seg_1033" n="e" s="T102">bejete </ts>
               <ts e="T104" id="Seg_1035" n="e" s="T103">bu </ts>
               <ts e="T105" id="Seg_1037" n="e" s="T104">kepsete </ts>
               <ts e="T106" id="Seg_1039" n="e" s="T105">da </ts>
               <ts e="T107" id="Seg_1041" n="e" s="T106">olororo </ts>
               <ts e="T108" id="Seg_1043" n="e" s="T107">üčügej. </ts>
               <ts e="T109" id="Seg_1045" n="e" s="T108">Kihi </ts>
               <ts e="T110" id="Seg_1047" n="e" s="T109">da </ts>
               <ts e="T111" id="Seg_1049" n="e" s="T110">kihini </ts>
               <ts e="T112" id="Seg_1051" n="e" s="T111">körsön, </ts>
               <ts e="T113" id="Seg_1053" n="e" s="T112">elbek </ts>
               <ts e="T114" id="Seg_1055" n="e" s="T113">kihi </ts>
               <ts e="T115" id="Seg_1057" n="e" s="T114">haŋatɨn </ts>
               <ts e="T116" id="Seg_1059" n="e" s="T115">bulaːn </ts>
               <ts e="T117" id="Seg_1061" n="e" s="T116">omuk </ts>
               <ts e="T118" id="Seg_1063" n="e" s="T117">ügüs </ts>
               <ts e="T119" id="Seg_1065" n="e" s="T118">bu͡o. </ts>
               <ts e="T120" id="Seg_1067" n="e" s="T119">Planeta. </ts>
               <ts e="T121" id="Seg_1069" n="e" s="T120">Bu </ts>
               <ts e="T122" id="Seg_1071" n="e" s="T121">ke </ts>
               <ts e="T123" id="Seg_1073" n="e" s="T122">otto </ts>
               <ts e="T124" id="Seg_1075" n="e" s="T123">di͡etekke. </ts>
               <ts e="T125" id="Seg_1077" n="e" s="T124">Kajdak </ts>
               <ts e="T126" id="Seg_1079" n="e" s="T125">di͡ekpinij, </ts>
               <ts e="T127" id="Seg_1081" n="e" s="T126">omuk </ts>
               <ts e="T128" id="Seg_1083" n="e" s="T127">bu͡o </ts>
               <ts e="T129" id="Seg_1085" n="e" s="T128">olus </ts>
               <ts e="T130" id="Seg_1087" n="e" s="T129">bu͡o </ts>
               <ts e="T131" id="Seg_1089" n="e" s="T130">urut </ts>
               <ts e="T132" id="Seg_1091" n="e" s="T131">bu͡o </ts>
               <ts e="T133" id="Seg_1093" n="e" s="T132">ügüs </ts>
               <ts e="T134" id="Seg_1095" n="e" s="T133">omuk </ts>
               <ts e="T135" id="Seg_1097" n="e" s="T134">ete </ts>
               <ts e="T136" id="Seg_1099" n="e" s="T135">bu͡o </ts>
               <ts e="T137" id="Seg_1101" n="e" s="T136">ü͡örener </ts>
               <ts e="T138" id="Seg_1103" n="e" s="T137">erdekke. </ts>
               <ts e="T139" id="Seg_1105" n="e" s="T138">Anɨ </ts>
               <ts e="T140" id="Seg_1107" n="e" s="T139">össü͡ö </ts>
               <ts e="T141" id="Seg_1109" n="e" s="T140">üksen </ts>
               <ts e="T142" id="Seg_1111" n="e" s="T141">iher. </ts>
               <ts e="T143" id="Seg_1113" n="e" s="T142">Anɨ </ts>
               <ts e="T144" id="Seg_1115" n="e" s="T143">bɨhatɨn </ts>
               <ts e="T145" id="Seg_1117" n="e" s="T144">haŋarbappɨn </ts>
               <ts e="T146" id="Seg_1119" n="e" s="T145">kanta </ts>
               <ts e="T147" id="Seg_1121" n="e" s="T146">anɨ </ts>
               <ts e="T148" id="Seg_1123" n="e" s="T147">össü͡ö </ts>
               <ts e="T149" id="Seg_1125" n="e" s="T148">da </ts>
               <ts e="T150" id="Seg_1127" n="e" s="T149">bulan </ts>
               <ts e="T151" id="Seg_1129" n="e" s="T150">iheller </ts>
               <ts e="T152" id="Seg_1131" n="e" s="T151">kihileri </ts>
               <ts e="T153" id="Seg_1133" n="e" s="T152">ke, </ts>
               <ts e="T154" id="Seg_1135" n="e" s="T153">gde </ts>
               <ts e="T155" id="Seg_1137" n="e" s="T154">to </ts>
               <ts e="T156" id="Seg_1139" n="e" s="T155">na </ts>
               <ts e="T157" id="Seg_1141" n="e" s="T156">ostrovax. </ts>
               <ts e="T158" id="Seg_1143" n="e" s="T157">Kihi </ts>
               <ts e="T159" id="Seg_1145" n="e" s="T158">hüter </ts>
               <ts e="T160" id="Seg_1147" n="e" s="T159">kihiler </ts>
               <ts e="T161" id="Seg_1149" n="e" s="T160">bulullallar. </ts>
            </ts>
            <ts e="T177" id="Seg_1150" n="sc" s="T172">
               <ts e="T173" id="Seg_1152" n="e" s="T172">Tugu </ts>
               <ts e="T174" id="Seg_1154" n="e" s="T173">ɨllɨ͡akpɨnɨj, </ts>
               <ts e="T175" id="Seg_1156" n="e" s="T174">če </ts>
               <ts e="T176" id="Seg_1158" n="e" s="T175">spetʼ </ts>
               <ts e="T177" id="Seg_1160" n="e" s="T176">to? </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-AnSP">
            <ta e="T12" id="Seg_1161" s="T7">AnSP_AnIM_2009_Girl_nar.AnSP.001 (002)</ta>
            <ta e="T23" id="Seg_1162" s="T12">AnSP_AnIM_2009_Girl_nar.AnSP.002 (003)</ta>
            <ta e="T31" id="Seg_1163" s="T23">AnSP_AnIM_2009_Girl_nar.AnSP.003 (004)</ta>
            <ta e="T37" id="Seg_1164" s="T31">AnSP_AnIM_2009_Girl_nar.AnSP.004 (005)</ta>
            <ta e="T51" id="Seg_1165" s="T37">AnSP_AnIM_2009_Girl_nar.AnSP.005 (006)</ta>
            <ta e="T53" id="Seg_1166" s="T51">AnSP_AnIM_2009_Girl_nar.AnSP.006 (007)</ta>
            <ta e="T60" id="Seg_1167" s="T57">AnSP_AnIM_2009_Girl_nar.AnSP.007 (009)</ta>
            <ta e="T72" id="Seg_1168" s="T60">AnSP_AnIM_2009_Girl_nar.AnSP.008 (010)</ta>
            <ta e="T82" id="Seg_1169" s="T72">AnSP_AnIM_2009_Girl_nar.AnSP.009 (011)</ta>
            <ta e="T92" id="Seg_1170" s="T82">AnSP_AnIM_2009_Girl_nar.AnSP.010 (012)</ta>
            <ta e="T97" id="Seg_1171" s="T92">AnSP_AnIM_2009_Girl_nar.AnSP.011 (013)</ta>
            <ta e="T108" id="Seg_1172" s="T97">AnSP_AnIM_2009_Girl_nar.AnSP.012 (014)</ta>
            <ta e="T119" id="Seg_1173" s="T108">AnSP_AnIM_2009_Girl_nar.AnSP.013 (015)</ta>
            <ta e="T120" id="Seg_1174" s="T119">AnSP_AnIM_2009_Girl_nar.AnSP.014 (016)</ta>
            <ta e="T124" id="Seg_1175" s="T120">AnSP_AnIM_2009_Girl_nar.AnSP.015 (017)</ta>
            <ta e="T138" id="Seg_1176" s="T124">AnSP_AnIM_2009_Girl_nar.AnSP.016 (018)</ta>
            <ta e="T142" id="Seg_1177" s="T138">AnSP_AnIM_2009_Girl_nar.AnSP.017 (019)</ta>
            <ta e="T157" id="Seg_1178" s="T142">AnSP_AnIM_2009_Girl_nar.AnSP.018 (020)</ta>
            <ta e="T161" id="Seg_1179" s="T157">AnSP_AnIM_2009_Girl_nar.AnSP.019 (021)</ta>
            <ta e="T177" id="Seg_1180" s="T172">AnSP_AnIM_2009_Girl_nar.AnSP.020 (023)</ta>
         </annotation>
         <annotation name="st" tierref="st-AnSP">
            <ta e="T12" id="Seg_1181" s="T7">Муж: һээ отто кыр.. кыраканнык кыра</ta>
            <ta e="T23" id="Seg_1182" s="T12">Муж: ээ кыыска диэ кысчааӈӈа, ол кэһиини эгэлэн кэлэбин бу остолго олоробун</ta>
            <ta e="T31" id="Seg_1183" s="T23">Муж: кыыһы көрөбүн дьактарбын көрдүм барыканы үөрэтэллэрин кайдак языгы</ta>
            <ta e="T37" id="Seg_1184" s="T31">Муж: любой язык, каньан да огом үөрэттэ</ta>
            <ta e="T51" id="Seg_1185" s="T37">Муж: моя с… бу кэ отто кайдак диэччилэрий биһиэкэ диэлэк кыыс кэ диэччилэр е дэ</ta>
            <ta e="T53" id="Seg_1186" s="T51">Муж: сожительница да?</ta>
            <ta e="T60" id="Seg_1187" s="T57">Муж: по квартире но.</ta>
            <ta e="T72" id="Seg_1188" s="T60">Муж: гини үөрдэ бу мин эгэлбиппэр ол эмэксин эмиэ ытааччи дьактар учугуойдук һанаатын</ta>
            <ta e="T82" id="Seg_1189" s="T72">Муж: онтон буолла туок диэк, по прилёту ещё раз көрсөн иһиэппит</ta>
            <ta e="T92" id="Seg_1190" s="T82">муж: онтон желайдыбын бу кыыска учугуой уолла булуокка, чэ мэньиилээк һогуста</ta>
            <ta e="T97" id="Seg_1191" s="T92">Муж: оннук дьэӈкэ дьэӈкэтик этэӈӈэ тийдэгинэ</ta>
            <ta e="T108" id="Seg_1192" s="T97">Муж: күн да тыган турара үчүгэй бэйэтэ бу кэпсэтэ да олороро үчүгэй</ta>
            <ta e="T119" id="Seg_1193" s="T108">Муж: киһи да киһини көрсөн, элбэк киһи һаӈатын булаан омук үгүс буо</ta>
            <ta e="T120" id="Seg_1194" s="T119">Муж: планета</ta>
            <ta e="T124" id="Seg_1195" s="T120">Муж бу кэ отто диэтэккэ</ta>
            <ta e="T138" id="Seg_1196" s="T124">Муж: кайдак диэкпиний, омук буо олус буо урут буо үгүс омук этэ буо </ta>
            <ta e="T142" id="Seg_1197" s="T138">Муж: үөрэнэр эрдэккэ аны өссүө үксэн иһэр</ta>
            <ta e="T157" id="Seg_1198" s="T142">Муж: аны быһатын һаӈарбаппын канта аны өссүө да булан иһэллэр киһилэри кэ, где то на островах</ta>
            <ta e="T161" id="Seg_1199" s="T157">Муж: киһи һүтэр киһилэр булуллаллар</ta>
            <ta e="T177" id="Seg_1200" s="T172">Муж: тугу ыллыакпыный, че спеть то?</ta>
         </annotation>
         <annotation name="ts" tierref="ts-AnSP">
            <ta e="T12" id="Seg_1201" s="T7">Heː otto (kɨr-) kɨrakaːnnɨk kɨra ((...)). </ta>
            <ta e="T23" id="Seg_1202" s="T12">Eː kɨːska di͡e kɨːsčaːŋŋa, ol kehiːni egelen kelebin bu ostolgo olorobun. </ta>
            <ta e="T31" id="Seg_1203" s="T23">Kɨːhɨ köröbün dʼaktarbɨn kördüm barɨkaːnɨ ü͡öretellerin kajdak jazɨgɨ. </ta>
            <ta e="T37" id="Seg_1204" s="T31">Ljuboj jazɨk, kanʼaːn da ogom ü͡örette. </ta>
            <ta e="T51" id="Seg_1205" s="T37">Moja (s-) bu ke otto kajdak di͡eččilerij bihi͡eke dʼi͡eleːk kɨːs ke di͡eččiler e de. </ta>
            <ta e="T53" id="Seg_1206" s="T51">Sožitelʼnica da? </ta>
            <ta e="T60" id="Seg_1207" s="T57">Po kvartire no. </ta>
            <ta e="T72" id="Seg_1208" s="T60">Gini ü͡örde bu min egelbipper ol emeːksin emi͡e ɨːtaːččɨ dʼaktar üčügejdik hanaːtɨn. </ta>
            <ta e="T82" id="Seg_1209" s="T72">Onton bu͡olla tu͡ok di͡ek, po priljotu eščjo raz körsön ihi͡eppit. </ta>
            <ta e="T92" id="Seg_1210" s="T82">Onton želajdɨːbɨn bu kɨːska üčügej u͡olla bulu͡okka, če menʼiːleːk hogusta. </ta>
            <ta e="T97" id="Seg_1211" s="T92">Onnuk dʼeŋke dʼeŋketik eteŋŋe tijdegine. </ta>
            <ta e="T108" id="Seg_1212" s="T97">Kün da tɨgan turara üčügej bejete bu kepsete da olororo üčügej. </ta>
            <ta e="T119" id="Seg_1213" s="T108">Kihi da kihini körsön, elbek kihi haŋatɨn bulaːn omuk ügüs bu͡o. </ta>
            <ta e="T120" id="Seg_1214" s="T119">Planeta. </ta>
            <ta e="T124" id="Seg_1215" s="T120">Bu ke otto di͡etekke. </ta>
            <ta e="T138" id="Seg_1216" s="T124">Kajdak di͡ekpinij, omuk bu͡o olus bu͡o urut bu͡o ügüs omuk ete bu͡o ü͡örener erdekke. </ta>
            <ta e="T142" id="Seg_1217" s="T138">Anɨ össü͡ö üksen iher. </ta>
            <ta e="T157" id="Seg_1218" s="T142">Anɨ bɨhatɨn haŋarbappɨn kanta anɨ össü͡ö da bulan iheller kihileri ke, gde to na ostrovax. </ta>
            <ta e="T161" id="Seg_1219" s="T157">Kihi hüter kihiler bulullallar. </ta>
            <ta e="T177" id="Seg_1220" s="T172">Tugu ɨllɨ͡akpɨnɨj, če spetʼ to? </ta>
         </annotation>
         <annotation name="mb" tierref="mb-AnSP">
            <ta e="T8" id="Seg_1221" s="T7">heː</ta>
            <ta e="T9" id="Seg_1222" s="T8">otto</ta>
            <ta e="T11" id="Seg_1223" s="T10">kɨra-kaːn-nɨk</ta>
            <ta e="T12" id="Seg_1224" s="T11">kɨra</ta>
            <ta e="T13" id="Seg_1225" s="T12">eː</ta>
            <ta e="T14" id="Seg_1226" s="T13">kɨːs-ka</ta>
            <ta e="T15" id="Seg_1227" s="T14">di͡e</ta>
            <ta e="T16" id="Seg_1228" s="T15">kɨːs-čaːŋ-ŋa</ta>
            <ta e="T17" id="Seg_1229" s="T16">ol</ta>
            <ta e="T18" id="Seg_1230" s="T17">kehiː-ni</ta>
            <ta e="T19" id="Seg_1231" s="T18">egel-en</ta>
            <ta e="T20" id="Seg_1232" s="T19">kel-e-bin</ta>
            <ta e="T21" id="Seg_1233" s="T20">bu</ta>
            <ta e="T22" id="Seg_1234" s="T21">ostol-go</ta>
            <ta e="T23" id="Seg_1235" s="T22">olor-o-bun</ta>
            <ta e="T24" id="Seg_1236" s="T23">kɨːh-ɨ</ta>
            <ta e="T25" id="Seg_1237" s="T24">kör-ö-bün</ta>
            <ta e="T26" id="Seg_1238" s="T25">dʼaktar-bɨ-n</ta>
            <ta e="T27" id="Seg_1239" s="T26">kör-dü-m</ta>
            <ta e="T28" id="Seg_1240" s="T27">barɨ-kaːn-ɨ</ta>
            <ta e="T29" id="Seg_1241" s="T28">ü͡öret-el-leri-n</ta>
            <ta e="T30" id="Seg_1242" s="T29">kajdak</ta>
            <ta e="T31" id="Seg_1243" s="T30">jazɨg-ɨ</ta>
            <ta e="T34" id="Seg_1244" s="T33">kanʼaː-n</ta>
            <ta e="T35" id="Seg_1245" s="T34">da</ta>
            <ta e="T36" id="Seg_1246" s="T35">ogo-m</ta>
            <ta e="T37" id="Seg_1247" s="T36">ü͡öret-t-e</ta>
            <ta e="T40" id="Seg_1248" s="T39">bu</ta>
            <ta e="T41" id="Seg_1249" s="T40">ke</ta>
            <ta e="T42" id="Seg_1250" s="T41">otto</ta>
            <ta e="T43" id="Seg_1251" s="T42">kajdak</ta>
            <ta e="T44" id="Seg_1252" s="T43">di͡e-čči-ler=ij</ta>
            <ta e="T45" id="Seg_1253" s="T44">bihi͡e-ke</ta>
            <ta e="T46" id="Seg_1254" s="T45">dʼi͡e-leːk</ta>
            <ta e="T47" id="Seg_1255" s="T46">kɨːs</ta>
            <ta e="T48" id="Seg_1256" s="T47">ke</ta>
            <ta e="T49" id="Seg_1257" s="T48">di͡e-čči-ler</ta>
            <ta e="T50" id="Seg_1258" s="T49">e</ta>
            <ta e="T51" id="Seg_1259" s="T50">de</ta>
            <ta e="T61" id="Seg_1260" s="T60">gini</ta>
            <ta e="T62" id="Seg_1261" s="T61">ü͡ör-d-e</ta>
            <ta e="T63" id="Seg_1262" s="T62">bu</ta>
            <ta e="T64" id="Seg_1263" s="T63">min</ta>
            <ta e="T65" id="Seg_1264" s="T64">egel-bip-pe-r</ta>
            <ta e="T66" id="Seg_1265" s="T65">ol</ta>
            <ta e="T67" id="Seg_1266" s="T66">emeːksin</ta>
            <ta e="T68" id="Seg_1267" s="T67">emi͡e</ta>
            <ta e="T69" id="Seg_1268" s="T68">ɨːt-aːččɨ</ta>
            <ta e="T70" id="Seg_1269" s="T69">dʼaktar</ta>
            <ta e="T71" id="Seg_1270" s="T70">üčügej-dik</ta>
            <ta e="T72" id="Seg_1271" s="T71">hanaː-tɨ-n</ta>
            <ta e="T73" id="Seg_1272" s="T72">onton</ta>
            <ta e="T74" id="Seg_1273" s="T73">bu͡olla</ta>
            <ta e="T75" id="Seg_1274" s="T74">tu͡ok</ta>
            <ta e="T76" id="Seg_1275" s="T75">d-i͡ek</ta>
            <ta e="T81" id="Seg_1276" s="T80">körs-ön</ta>
            <ta e="T82" id="Seg_1277" s="T81">ih-i͡ep-pit</ta>
            <ta e="T83" id="Seg_1278" s="T82">onton</ta>
            <ta e="T84" id="Seg_1279" s="T83">želaj-d-ɨː-bɨn</ta>
            <ta e="T85" id="Seg_1280" s="T84">bu</ta>
            <ta e="T86" id="Seg_1281" s="T85">kɨːs-ka</ta>
            <ta e="T87" id="Seg_1282" s="T86">üčügej</ta>
            <ta e="T88" id="Seg_1283" s="T87">u͡ol-la</ta>
            <ta e="T89" id="Seg_1284" s="T88">bul-u͡ok-ka</ta>
            <ta e="T90" id="Seg_1285" s="T89">če</ta>
            <ta e="T91" id="Seg_1286" s="T90">menʼiː-leːk</ta>
            <ta e="T92" id="Seg_1287" s="T91">hogus-ta</ta>
            <ta e="T93" id="Seg_1288" s="T92">onnuk</ta>
            <ta e="T94" id="Seg_1289" s="T93">dʼeŋke</ta>
            <ta e="T95" id="Seg_1290" s="T94">dʼeŋke-tik</ta>
            <ta e="T96" id="Seg_1291" s="T95">eteŋŋe</ta>
            <ta e="T97" id="Seg_1292" s="T96">tij-deg-ine</ta>
            <ta e="T98" id="Seg_1293" s="T97">kün</ta>
            <ta e="T99" id="Seg_1294" s="T98">da</ta>
            <ta e="T100" id="Seg_1295" s="T99">tɨg-an</ta>
            <ta e="T101" id="Seg_1296" s="T100">tur-ar-a</ta>
            <ta e="T102" id="Seg_1297" s="T101">üčügej</ta>
            <ta e="T103" id="Seg_1298" s="T102">beje-te</ta>
            <ta e="T104" id="Seg_1299" s="T103">bu</ta>
            <ta e="T105" id="Seg_1300" s="T104">kepset-e</ta>
            <ta e="T106" id="Seg_1301" s="T105">da</ta>
            <ta e="T107" id="Seg_1302" s="T106">olor-or-o</ta>
            <ta e="T108" id="Seg_1303" s="T107">üčügej</ta>
            <ta e="T109" id="Seg_1304" s="T108">kihi</ta>
            <ta e="T110" id="Seg_1305" s="T109">da</ta>
            <ta e="T111" id="Seg_1306" s="T110">kihi-ni</ta>
            <ta e="T112" id="Seg_1307" s="T111">körs-ön</ta>
            <ta e="T113" id="Seg_1308" s="T112">elbek</ta>
            <ta e="T114" id="Seg_1309" s="T113">kihi</ta>
            <ta e="T115" id="Seg_1310" s="T114">haŋa-tɨ-n</ta>
            <ta e="T116" id="Seg_1311" s="T115">bulaː-n</ta>
            <ta e="T117" id="Seg_1312" s="T116">omuk</ta>
            <ta e="T118" id="Seg_1313" s="T117">ügüs</ta>
            <ta e="T119" id="Seg_1314" s="T118">bu͡o</ta>
            <ta e="T121" id="Seg_1315" s="T120">bu</ta>
            <ta e="T122" id="Seg_1316" s="T121">ke</ta>
            <ta e="T123" id="Seg_1317" s="T122">otto</ta>
            <ta e="T124" id="Seg_1318" s="T123">di͡e-tek-ke</ta>
            <ta e="T125" id="Seg_1319" s="T124">kajdak</ta>
            <ta e="T126" id="Seg_1320" s="T125">d-i͡ek-pin=ij</ta>
            <ta e="T127" id="Seg_1321" s="T126">omuk</ta>
            <ta e="T128" id="Seg_1322" s="T127">bu͡o</ta>
            <ta e="T129" id="Seg_1323" s="T128">olus</ta>
            <ta e="T130" id="Seg_1324" s="T129">bu͡o</ta>
            <ta e="T131" id="Seg_1325" s="T130">urut</ta>
            <ta e="T132" id="Seg_1326" s="T131">bu͡o</ta>
            <ta e="T133" id="Seg_1327" s="T132">ügüs</ta>
            <ta e="T134" id="Seg_1328" s="T133">omuk</ta>
            <ta e="T135" id="Seg_1329" s="T134">e-t-e</ta>
            <ta e="T136" id="Seg_1330" s="T135">bu͡o</ta>
            <ta e="T137" id="Seg_1331" s="T136">ü͡ören-er</ta>
            <ta e="T138" id="Seg_1332" s="T137">er-dek-ke</ta>
            <ta e="T139" id="Seg_1333" s="T138">anɨ</ta>
            <ta e="T140" id="Seg_1334" s="T139">össü͡ö</ta>
            <ta e="T141" id="Seg_1335" s="T140">üks-en</ta>
            <ta e="T142" id="Seg_1336" s="T141">ih-er</ta>
            <ta e="T143" id="Seg_1337" s="T142">anɨ</ta>
            <ta e="T144" id="Seg_1338" s="T143">bɨhatɨn</ta>
            <ta e="T145" id="Seg_1339" s="T144">haŋar-bap-pɨn</ta>
            <ta e="T146" id="Seg_1340" s="T145">kanta</ta>
            <ta e="T147" id="Seg_1341" s="T146">anɨ</ta>
            <ta e="T148" id="Seg_1342" s="T147">össü͡ö</ta>
            <ta e="T149" id="Seg_1343" s="T148">da</ta>
            <ta e="T150" id="Seg_1344" s="T149">bul-an</ta>
            <ta e="T151" id="Seg_1345" s="T150">ih-el-ler</ta>
            <ta e="T152" id="Seg_1346" s="T151">kihi-ler-i</ta>
            <ta e="T153" id="Seg_1347" s="T152">ke</ta>
            <ta e="T158" id="Seg_1348" s="T157">kihi</ta>
            <ta e="T159" id="Seg_1349" s="T158">hüt-er</ta>
            <ta e="T160" id="Seg_1350" s="T159">kihi-ler</ta>
            <ta e="T161" id="Seg_1351" s="T160">bul-u-ll-al-lar</ta>
            <ta e="T173" id="Seg_1352" s="T172">tug-u</ta>
            <ta e="T174" id="Seg_1353" s="T173">ɨll-ɨ͡ak-pɨn=ɨj</ta>
         </annotation>
         <annotation name="mp" tierref="mp-AnSP">
            <ta e="T8" id="Seg_1354" s="T7">eː</ta>
            <ta e="T9" id="Seg_1355" s="T8">otto</ta>
            <ta e="T11" id="Seg_1356" s="T10">kɨra-kAːN-LIk</ta>
            <ta e="T12" id="Seg_1357" s="T11">kɨra</ta>
            <ta e="T13" id="Seg_1358" s="T12">eː</ta>
            <ta e="T14" id="Seg_1359" s="T13">kɨːs-GA</ta>
            <ta e="T15" id="Seg_1360" s="T14">di͡e</ta>
            <ta e="T16" id="Seg_1361" s="T15">kɨːs-čAːn-GA</ta>
            <ta e="T17" id="Seg_1362" s="T16">ol</ta>
            <ta e="T18" id="Seg_1363" s="T17">kehiː-nI</ta>
            <ta e="T19" id="Seg_1364" s="T18">egel-An</ta>
            <ta e="T20" id="Seg_1365" s="T19">kel-A-BIn</ta>
            <ta e="T21" id="Seg_1366" s="T20">bu</ta>
            <ta e="T22" id="Seg_1367" s="T21">ostu͡ol-GA</ta>
            <ta e="T23" id="Seg_1368" s="T22">olor-A-BIn</ta>
            <ta e="T24" id="Seg_1369" s="T23">kɨːs-nI</ta>
            <ta e="T25" id="Seg_1370" s="T24">kör-A-BIn</ta>
            <ta e="T26" id="Seg_1371" s="T25">dʼaktar-BI-n</ta>
            <ta e="T27" id="Seg_1372" s="T26">kör-TI-m</ta>
            <ta e="T28" id="Seg_1373" s="T27">barɨ-kAːN-nI</ta>
            <ta e="T29" id="Seg_1374" s="T28">ü͡öret-Ar-LArI-n</ta>
            <ta e="T30" id="Seg_1375" s="T29">kajdak</ta>
            <ta e="T31" id="Seg_1376" s="T30">jazɨk-nI</ta>
            <ta e="T34" id="Seg_1377" s="T33">kanʼaː-An</ta>
            <ta e="T35" id="Seg_1378" s="T34">da</ta>
            <ta e="T36" id="Seg_1379" s="T35">ogo-m</ta>
            <ta e="T37" id="Seg_1380" s="T36">ü͡öret-TI-tA</ta>
            <ta e="T40" id="Seg_1381" s="T39">bu</ta>
            <ta e="T41" id="Seg_1382" s="T40">ka</ta>
            <ta e="T42" id="Seg_1383" s="T41">otto</ta>
            <ta e="T43" id="Seg_1384" s="T42">kajdak</ta>
            <ta e="T44" id="Seg_1385" s="T43">di͡e-AːččI-LAr=Ij</ta>
            <ta e="T45" id="Seg_1386" s="T44">bihigi-GA</ta>
            <ta e="T46" id="Seg_1387" s="T45">dʼi͡e-LAːK</ta>
            <ta e="T47" id="Seg_1388" s="T46">kɨːs</ta>
            <ta e="T48" id="Seg_1389" s="T47">ka</ta>
            <ta e="T49" id="Seg_1390" s="T48">di͡e-AːččI-LAr</ta>
            <ta e="T50" id="Seg_1391" s="T49">e</ta>
            <ta e="T51" id="Seg_1392" s="T50">dʼe</ta>
            <ta e="T61" id="Seg_1393" s="T60">gini</ta>
            <ta e="T62" id="Seg_1394" s="T61">ü͡ör-TI-tA</ta>
            <ta e="T63" id="Seg_1395" s="T62">bu</ta>
            <ta e="T64" id="Seg_1396" s="T63">min</ta>
            <ta e="T65" id="Seg_1397" s="T64">egel-BIT-BA-r</ta>
            <ta e="T66" id="Seg_1398" s="T65">ol</ta>
            <ta e="T67" id="Seg_1399" s="T66">emeːksin</ta>
            <ta e="T68" id="Seg_1400" s="T67">emi͡e</ta>
            <ta e="T69" id="Seg_1401" s="T68">ɨːt-AːččI</ta>
            <ta e="T70" id="Seg_1402" s="T69">dʼaktar</ta>
            <ta e="T71" id="Seg_1403" s="T70">üčügej-LIk</ta>
            <ta e="T72" id="Seg_1404" s="T71">hanaː-tI-n</ta>
            <ta e="T73" id="Seg_1405" s="T72">onton</ta>
            <ta e="T74" id="Seg_1406" s="T73">bu͡olla</ta>
            <ta e="T75" id="Seg_1407" s="T74">tu͡ok</ta>
            <ta e="T76" id="Seg_1408" s="T75">di͡e-IAK</ta>
            <ta e="T81" id="Seg_1409" s="T80">körüs-An</ta>
            <ta e="T82" id="Seg_1410" s="T81">is-IAK-BIt</ta>
            <ta e="T83" id="Seg_1411" s="T82">onton</ta>
            <ta e="T84" id="Seg_1412" s="T83">želaj-LAː-A-BIn</ta>
            <ta e="T85" id="Seg_1413" s="T84">bu</ta>
            <ta e="T86" id="Seg_1414" s="T85">kɨːs-GA</ta>
            <ta e="T87" id="Seg_1415" s="T86">üčügej</ta>
            <ta e="T88" id="Seg_1416" s="T87">u͡ol-TA</ta>
            <ta e="T89" id="Seg_1417" s="T88">bul-IAK-GA</ta>
            <ta e="T90" id="Seg_1418" s="T89">dʼe</ta>
            <ta e="T91" id="Seg_1419" s="T90">menʼiː-LAːK</ta>
            <ta e="T92" id="Seg_1420" s="T91">hogus-TA</ta>
            <ta e="T93" id="Seg_1421" s="T92">onnuk</ta>
            <ta e="T94" id="Seg_1422" s="T93">dʼeŋke</ta>
            <ta e="T95" id="Seg_1423" s="T94">dʼeŋke-LIk</ta>
            <ta e="T96" id="Seg_1424" s="T95">eteŋŋe</ta>
            <ta e="T97" id="Seg_1425" s="T96">tij-TAK-InA</ta>
            <ta e="T98" id="Seg_1426" s="T97">kün</ta>
            <ta e="T99" id="Seg_1427" s="T98">da</ta>
            <ta e="T100" id="Seg_1428" s="T99">tɨk-An</ta>
            <ta e="T101" id="Seg_1429" s="T100">tur-Ar-tA</ta>
            <ta e="T102" id="Seg_1430" s="T101">üčügej</ta>
            <ta e="T103" id="Seg_1431" s="T102">beje-tA</ta>
            <ta e="T104" id="Seg_1432" s="T103">bu</ta>
            <ta e="T105" id="Seg_1433" s="T104">kepset-A</ta>
            <ta e="T106" id="Seg_1434" s="T105">da</ta>
            <ta e="T107" id="Seg_1435" s="T106">olor-Ar-tA</ta>
            <ta e="T108" id="Seg_1436" s="T107">üčügej</ta>
            <ta e="T109" id="Seg_1437" s="T108">kihi</ta>
            <ta e="T110" id="Seg_1438" s="T109">da</ta>
            <ta e="T111" id="Seg_1439" s="T110">kihi-nI</ta>
            <ta e="T112" id="Seg_1440" s="T111">körüs-An</ta>
            <ta e="T113" id="Seg_1441" s="T112">elbek</ta>
            <ta e="T114" id="Seg_1442" s="T113">kihi</ta>
            <ta e="T115" id="Seg_1443" s="T114">haŋa-tI-n</ta>
            <ta e="T116" id="Seg_1444" s="T115">bulaː-An</ta>
            <ta e="T117" id="Seg_1445" s="T116">omuk</ta>
            <ta e="T118" id="Seg_1446" s="T117">ügüs</ta>
            <ta e="T119" id="Seg_1447" s="T118">bu͡o</ta>
            <ta e="T121" id="Seg_1448" s="T120">bu</ta>
            <ta e="T122" id="Seg_1449" s="T121">ka</ta>
            <ta e="T123" id="Seg_1450" s="T122">otto</ta>
            <ta e="T124" id="Seg_1451" s="T123">di͡e-TAK-GA</ta>
            <ta e="T125" id="Seg_1452" s="T124">kajdak</ta>
            <ta e="T126" id="Seg_1453" s="T125">di͡e-IAK-BIn=Ij</ta>
            <ta e="T127" id="Seg_1454" s="T126">omuk</ta>
            <ta e="T128" id="Seg_1455" s="T127">bu͡o</ta>
            <ta e="T129" id="Seg_1456" s="T128">olus</ta>
            <ta e="T130" id="Seg_1457" s="T129">bu͡o</ta>
            <ta e="T131" id="Seg_1458" s="T130">urut</ta>
            <ta e="T132" id="Seg_1459" s="T131">bu͡o</ta>
            <ta e="T133" id="Seg_1460" s="T132">ügüs</ta>
            <ta e="T134" id="Seg_1461" s="T133">omuk</ta>
            <ta e="T135" id="Seg_1462" s="T134">e-TI-tA</ta>
            <ta e="T136" id="Seg_1463" s="T135">bu͡o</ta>
            <ta e="T137" id="Seg_1464" s="T136">ü͡ören-Ar</ta>
            <ta e="T138" id="Seg_1465" s="T137">er-TAK-GA</ta>
            <ta e="T139" id="Seg_1466" s="T138">anɨ</ta>
            <ta e="T140" id="Seg_1467" s="T139">össü͡ö</ta>
            <ta e="T141" id="Seg_1468" s="T140">ükseː-An</ta>
            <ta e="T142" id="Seg_1469" s="T141">is-Ar</ta>
            <ta e="T143" id="Seg_1470" s="T142">anɨ</ta>
            <ta e="T144" id="Seg_1471" s="T143">bɨhatɨn</ta>
            <ta e="T145" id="Seg_1472" s="T144">haŋar-BAT-BIn</ta>
            <ta e="T146" id="Seg_1473" s="T145">kantan</ta>
            <ta e="T147" id="Seg_1474" s="T146">anɨ</ta>
            <ta e="T148" id="Seg_1475" s="T147">össü͡ö</ta>
            <ta e="T149" id="Seg_1476" s="T148">da</ta>
            <ta e="T150" id="Seg_1477" s="T149">bul-An</ta>
            <ta e="T151" id="Seg_1478" s="T150">is-Ar-LAr</ta>
            <ta e="T152" id="Seg_1479" s="T151">kihi-LAr-nI</ta>
            <ta e="T153" id="Seg_1480" s="T152">ka</ta>
            <ta e="T158" id="Seg_1481" s="T157">kihi</ta>
            <ta e="T159" id="Seg_1482" s="T158">hüt-Ar</ta>
            <ta e="T160" id="Seg_1483" s="T159">kihi-LAr</ta>
            <ta e="T161" id="Seg_1484" s="T160">bul-I-LIN-Ar-LAr</ta>
            <ta e="T173" id="Seg_1485" s="T172">tu͡ok-nI</ta>
            <ta e="T174" id="Seg_1486" s="T173">ɨllaː-IAK-BIn=Ij</ta>
         </annotation>
         <annotation name="ge" tierref="ge-AnSP">
            <ta e="T8" id="Seg_1487" s="T7">EMPH</ta>
            <ta e="T9" id="Seg_1488" s="T8">EMPH</ta>
            <ta e="T11" id="Seg_1489" s="T10">small-INTNS-ADVZ</ta>
            <ta e="T12" id="Seg_1490" s="T11">small.[NOM]</ta>
            <ta e="T13" id="Seg_1491" s="T12">eh</ta>
            <ta e="T14" id="Seg_1492" s="T13">girl-DAT/LOC</ta>
            <ta e="T15" id="Seg_1493" s="T14">say.[IMP.2SG]</ta>
            <ta e="T16" id="Seg_1494" s="T15">girl-DIM-DAT/LOC</ta>
            <ta e="T17" id="Seg_1495" s="T16">that</ta>
            <ta e="T18" id="Seg_1496" s="T17">gift-ACC</ta>
            <ta e="T19" id="Seg_1497" s="T18">get-CVB.SEQ</ta>
            <ta e="T20" id="Seg_1498" s="T19">come-PRS-1SG</ta>
            <ta e="T21" id="Seg_1499" s="T20">this</ta>
            <ta e="T22" id="Seg_1500" s="T21">table-DAT/LOC</ta>
            <ta e="T23" id="Seg_1501" s="T22">sit-PRS-1SG</ta>
            <ta e="T24" id="Seg_1502" s="T23">girl-ACC</ta>
            <ta e="T25" id="Seg_1503" s="T24">see-PRS-1SG</ta>
            <ta e="T26" id="Seg_1504" s="T25">woman-1SG-ACC</ta>
            <ta e="T27" id="Seg_1505" s="T26">see-PST1-1SG</ta>
            <ta e="T28" id="Seg_1506" s="T27">every-INTNS-ACC</ta>
            <ta e="T29" id="Seg_1507" s="T28">learn-PTCP.PRS-3PL-ACC</ta>
            <ta e="T30" id="Seg_1508" s="T29">how</ta>
            <ta e="T31" id="Seg_1509" s="T30">language-ACC</ta>
            <ta e="T34" id="Seg_1510" s="T33">and.so.on-CVB.SEQ</ta>
            <ta e="T35" id="Seg_1511" s="T34">and</ta>
            <ta e="T36" id="Seg_1512" s="T35">child-1SG.[NOM]</ta>
            <ta e="T37" id="Seg_1513" s="T36">learn-PST1-3SG</ta>
            <ta e="T40" id="Seg_1514" s="T39">this</ta>
            <ta e="T41" id="Seg_1515" s="T40">well</ta>
            <ta e="T42" id="Seg_1516" s="T41">EMPH</ta>
            <ta e="T43" id="Seg_1517" s="T42">how</ta>
            <ta e="T44" id="Seg_1518" s="T43">say-HAB-3PL=Q</ta>
            <ta e="T45" id="Seg_1519" s="T44">1PL-DAT/LOC</ta>
            <ta e="T46" id="Seg_1520" s="T45">house-PROPR</ta>
            <ta e="T47" id="Seg_1521" s="T46">girl.[NOM]</ta>
            <ta e="T48" id="Seg_1522" s="T47">well</ta>
            <ta e="T49" id="Seg_1523" s="T48">say-HAB-3PL</ta>
            <ta e="T50" id="Seg_1524" s="T49">eh</ta>
            <ta e="T51" id="Seg_1525" s="T50">well</ta>
            <ta e="T61" id="Seg_1526" s="T60">3SG.[NOM]</ta>
            <ta e="T62" id="Seg_1527" s="T61">be.happy-PST1-3SG</ta>
            <ta e="T63" id="Seg_1528" s="T62">this</ta>
            <ta e="T64" id="Seg_1529" s="T63">1SG.[NOM]</ta>
            <ta e="T65" id="Seg_1530" s="T64">bring-PTCP.PST-1SG-DAT/LOC</ta>
            <ta e="T66" id="Seg_1531" s="T65">that </ta>
            <ta e="T67" id="Seg_1532" s="T66">old.woman.[NOM]</ta>
            <ta e="T68" id="Seg_1533" s="T67">again</ta>
            <ta e="T69" id="Seg_1534" s="T68">send-PTCP.HAB</ta>
            <ta e="T70" id="Seg_1535" s="T69">woman.[NOM]</ta>
            <ta e="T71" id="Seg_1536" s="T70">good-ADVZ</ta>
            <ta e="T72" id="Seg_1537" s="T71">thought-3SG-ACC</ta>
            <ta e="T73" id="Seg_1538" s="T72">then</ta>
            <ta e="T74" id="Seg_1539" s="T73">MOD</ta>
            <ta e="T75" id="Seg_1540" s="T74">what.[NOM]</ta>
            <ta e="T76" id="Seg_1541" s="T75">say-PTCP.FUT</ta>
            <ta e="T81" id="Seg_1542" s="T80">meet-CVB.SEQ</ta>
            <ta e="T82" id="Seg_1543" s="T81">go-FUT-1PL</ta>
            <ta e="T83" id="Seg_1544" s="T82">then</ta>
            <ta e="T84" id="Seg_1545" s="T83">wish-VBZ-PRS-1SG</ta>
            <ta e="T85" id="Seg_1546" s="T84">this</ta>
            <ta e="T86" id="Seg_1547" s="T85">girl-DAT/LOC</ta>
            <ta e="T87" id="Seg_1548" s="T86">good.[NOM]</ta>
            <ta e="T88" id="Seg_1549" s="T87">boy-PART</ta>
            <ta e="T89" id="Seg_1550" s="T88">find-PTCP.FUT-DAT/LOC</ta>
            <ta e="T90" id="Seg_1551" s="T89">well</ta>
            <ta e="T91" id="Seg_1552" s="T90">head-PROPR</ta>
            <ta e="T92" id="Seg_1553" s="T91">EMPH-PART</ta>
            <ta e="T93" id="Seg_1554" s="T92">such</ta>
            <ta e="T94" id="Seg_1555" s="T93">clear</ta>
            <ta e="T95" id="Seg_1556" s="T94">clear-ADVZ</ta>
            <ta e="T96" id="Seg_1557" s="T95">happy</ta>
            <ta e="T97" id="Seg_1558" s="T96">reach-TEMP-3SG</ta>
            <ta e="T98" id="Seg_1559" s="T97">sun.[NOM]</ta>
            <ta e="T99" id="Seg_1560" s="T98">EMPH</ta>
            <ta e="T100" id="Seg_1561" s="T99">shine-CVB.SEQ</ta>
            <ta e="T101" id="Seg_1562" s="T100">stand-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T102" id="Seg_1563" s="T101">good.[NOM]</ta>
            <ta e="T103" id="Seg_1564" s="T102">self-3SG.[NOM]</ta>
            <ta e="T104" id="Seg_1565" s="T103">this</ta>
            <ta e="T105" id="Seg_1566" s="T104">chat-CVB.SIM</ta>
            <ta e="T106" id="Seg_1567" s="T105">and</ta>
            <ta e="T107" id="Seg_1568" s="T106">sit-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T108" id="Seg_1569" s="T107">good.[NOM]</ta>
            <ta e="T109" id="Seg_1570" s="T108">human.being.[NOM]</ta>
            <ta e="T110" id="Seg_1571" s="T109">and</ta>
            <ta e="T111" id="Seg_1572" s="T110">human.being-ACC</ta>
            <ta e="T112" id="Seg_1573" s="T111">meet-CVB.SEQ</ta>
            <ta e="T113" id="Seg_1574" s="T112">many</ta>
            <ta e="T114" id="Seg_1575" s="T113">human.being.[NOM]</ta>
            <ta e="T115" id="Seg_1576" s="T114">language-3SG-ACC</ta>
            <ta e="T116" id="Seg_1577" s="T115">mix-CVB.SEQ</ta>
            <ta e="T117" id="Seg_1578" s="T116">people.[NOM]</ta>
            <ta e="T118" id="Seg_1579" s="T117">many</ta>
            <ta e="T119" id="Seg_1580" s="T118">EMPH</ta>
            <ta e="T121" id="Seg_1581" s="T120">this</ta>
            <ta e="T122" id="Seg_1582" s="T121">well</ta>
            <ta e="T123" id="Seg_1583" s="T122">EMPH</ta>
            <ta e="T124" id="Seg_1584" s="T123">say-PTCP.COND-DAT/LOC</ta>
            <ta e="T125" id="Seg_1585" s="T124">how</ta>
            <ta e="T126" id="Seg_1586" s="T125">say-FUT-1SG=Q</ta>
            <ta e="T127" id="Seg_1587" s="T126">foreign.[NOM]</ta>
            <ta e="T128" id="Seg_1588" s="T127">EMPH</ta>
            <ta e="T129" id="Seg_1589" s="T128">very</ta>
            <ta e="T130" id="Seg_1590" s="T129">EMPH</ta>
            <ta e="T131" id="Seg_1591" s="T130">before</ta>
            <ta e="T132" id="Seg_1592" s="T131">EMPH</ta>
            <ta e="T133" id="Seg_1593" s="T132">many</ta>
            <ta e="T134" id="Seg_1594" s="T133">foreign.[NOM]</ta>
            <ta e="T135" id="Seg_1595" s="T134">be-PST1-3SG</ta>
            <ta e="T136" id="Seg_1596" s="T135">EMPH</ta>
            <ta e="T137" id="Seg_1597" s="T136">learn-PTCP.PRS</ta>
            <ta e="T138" id="Seg_1598" s="T137">be-TEMP-DAT/LOC</ta>
            <ta e="T139" id="Seg_1599" s="T138">now</ta>
            <ta e="T140" id="Seg_1600" s="T139">still</ta>
            <ta e="T141" id="Seg_1601" s="T140">increase-CVB.SEQ</ta>
            <ta e="T142" id="Seg_1602" s="T141">go-PRS.[3SG]</ta>
            <ta e="T143" id="Seg_1603" s="T142">now</ta>
            <ta e="T144" id="Seg_1604" s="T143">directly</ta>
            <ta e="T145" id="Seg_1605" s="T144">speak-NEG-1SG</ta>
            <ta e="T146" id="Seg_1606" s="T145">where.from</ta>
            <ta e="T147" id="Seg_1607" s="T146">now</ta>
            <ta e="T148" id="Seg_1608" s="T147">still</ta>
            <ta e="T149" id="Seg_1609" s="T148">and</ta>
            <ta e="T150" id="Seg_1610" s="T149">find-CVB.SEQ</ta>
            <ta e="T151" id="Seg_1611" s="T150">go-PRS-3PL</ta>
            <ta e="T152" id="Seg_1612" s="T151">human.being-PL-ACC</ta>
            <ta e="T153" id="Seg_1613" s="T152">well</ta>
            <ta e="T158" id="Seg_1614" s="T157">human.being.[NOM]</ta>
            <ta e="T159" id="Seg_1615" s="T158">get.lost-PRS.[3SG]</ta>
            <ta e="T160" id="Seg_1616" s="T159">human.being-PL.[NOM]</ta>
            <ta e="T161" id="Seg_1617" s="T160">find-EP-PASS/REFL-PRS-3PL</ta>
            <ta e="T173" id="Seg_1618" s="T172">what-ACC</ta>
            <ta e="T174" id="Seg_1619" s="T173">sing-FUT-1SG=Q</ta>
         </annotation>
         <annotation name="gg" tierref="gg-AnSP">
            <ta e="T8" id="Seg_1620" s="T7">EMPH</ta>
            <ta e="T9" id="Seg_1621" s="T8">EMPH</ta>
            <ta e="T11" id="Seg_1622" s="T10">klein-INTNS-ADVZ</ta>
            <ta e="T12" id="Seg_1623" s="T11">klein.[NOM]</ta>
            <ta e="T13" id="Seg_1624" s="T12">äh</ta>
            <ta e="T14" id="Seg_1625" s="T13">Mädchen-DAT/LOC</ta>
            <ta e="T15" id="Seg_1626" s="T14">sagen.[IMP.2SG]</ta>
            <ta e="T16" id="Seg_1627" s="T15">Mädchen-DIM-DAT/LOC</ta>
            <ta e="T17" id="Seg_1628" s="T16">jenes</ta>
            <ta e="T18" id="Seg_1629" s="T17">Geschenk-ACC</ta>
            <ta e="T19" id="Seg_1630" s="T18">holen-CVB.SEQ</ta>
            <ta e="T20" id="Seg_1631" s="T19">kommen-PRS-1SG</ta>
            <ta e="T21" id="Seg_1632" s="T20">dieses</ta>
            <ta e="T22" id="Seg_1633" s="T21">Tisch-DAT/LOC</ta>
            <ta e="T23" id="Seg_1634" s="T22">sitzen-PRS-1SG</ta>
            <ta e="T24" id="Seg_1635" s="T23">Mädchen-ACC</ta>
            <ta e="T25" id="Seg_1636" s="T24">sehen-PRS-1SG</ta>
            <ta e="T26" id="Seg_1637" s="T25">Frau-1SG-ACC</ta>
            <ta e="T27" id="Seg_1638" s="T26">sehen-PST1-1SG</ta>
            <ta e="T28" id="Seg_1639" s="T27">jeder-INTNS-ACC</ta>
            <ta e="T29" id="Seg_1640" s="T28">lernen-PTCP.PRS-3PL-ACC</ta>
            <ta e="T30" id="Seg_1641" s="T29">wie</ta>
            <ta e="T31" id="Seg_1642" s="T30">Sprache-ACC</ta>
            <ta e="T34" id="Seg_1643" s="T33">und.so.weiter-CVB.SEQ</ta>
            <ta e="T35" id="Seg_1644" s="T34">und</ta>
            <ta e="T36" id="Seg_1645" s="T35">Kind-1SG.[NOM]</ta>
            <ta e="T37" id="Seg_1646" s="T36">lernen-PST1-3SG</ta>
            <ta e="T40" id="Seg_1647" s="T39">dieses</ta>
            <ta e="T41" id="Seg_1648" s="T40">nun</ta>
            <ta e="T42" id="Seg_1649" s="T41">EMPH</ta>
            <ta e="T43" id="Seg_1650" s="T42">wie</ta>
            <ta e="T44" id="Seg_1651" s="T43">sagen-HAB-3PL=Q</ta>
            <ta e="T45" id="Seg_1652" s="T44">1PL-DAT/LOC</ta>
            <ta e="T46" id="Seg_1653" s="T45">Haus-PROPR</ta>
            <ta e="T47" id="Seg_1654" s="T46">Mädchen.[NOM]</ta>
            <ta e="T48" id="Seg_1655" s="T47">nun</ta>
            <ta e="T49" id="Seg_1656" s="T48">sagen-HAB-3PL</ta>
            <ta e="T50" id="Seg_1657" s="T49">äh</ta>
            <ta e="T51" id="Seg_1658" s="T50">doch</ta>
            <ta e="T61" id="Seg_1659" s="T60">3SG.[NOM]</ta>
            <ta e="T62" id="Seg_1660" s="T61">sich.freuen-PST1-3SG</ta>
            <ta e="T63" id="Seg_1661" s="T62">dieses</ta>
            <ta e="T64" id="Seg_1662" s="T63">1SG.[NOM]</ta>
            <ta e="T65" id="Seg_1663" s="T64">bringen-PTCP.PST-1SG-DAT/LOC</ta>
            <ta e="T66" id="Seg_1664" s="T65">jenes</ta>
            <ta e="T67" id="Seg_1665" s="T66">Alte.[NOM]</ta>
            <ta e="T68" id="Seg_1666" s="T67">wieder</ta>
            <ta e="T69" id="Seg_1667" s="T68">schicken-PTCP.HAB</ta>
            <ta e="T70" id="Seg_1668" s="T69">Frau.[NOM]</ta>
            <ta e="T71" id="Seg_1669" s="T70">gut-ADVZ</ta>
            <ta e="T72" id="Seg_1670" s="T71">Gedanke-3SG-ACC</ta>
            <ta e="T73" id="Seg_1671" s="T72">dann</ta>
            <ta e="T74" id="Seg_1672" s="T73">MOD</ta>
            <ta e="T75" id="Seg_1673" s="T74">was.[NOM]</ta>
            <ta e="T76" id="Seg_1674" s="T75">sagen-PTCP.FUT</ta>
            <ta e="T81" id="Seg_1675" s="T80">treffen-CVB.SEQ</ta>
            <ta e="T82" id="Seg_1676" s="T81">gehen-FUT-1PL</ta>
            <ta e="T83" id="Seg_1677" s="T82">dann</ta>
            <ta e="T84" id="Seg_1678" s="T83">wünschen-VBZ-PRS-1SG</ta>
            <ta e="T85" id="Seg_1679" s="T84">dieses</ta>
            <ta e="T86" id="Seg_1680" s="T85">Mädchen-DAT/LOC</ta>
            <ta e="T87" id="Seg_1681" s="T86">gut.[NOM]</ta>
            <ta e="T88" id="Seg_1682" s="T87">Junge-PART</ta>
            <ta e="T89" id="Seg_1683" s="T88">finden-PTCP.FUT-DAT/LOC</ta>
            <ta e="T90" id="Seg_1684" s="T89">doch</ta>
            <ta e="T91" id="Seg_1685" s="T90">Kopf-PROPR</ta>
            <ta e="T92" id="Seg_1686" s="T91">EMPH-PART</ta>
            <ta e="T93" id="Seg_1687" s="T92">solch</ta>
            <ta e="T94" id="Seg_1688" s="T93">klar</ta>
            <ta e="T95" id="Seg_1689" s="T94">klar-ADVZ</ta>
            <ta e="T96" id="Seg_1690" s="T95">glücklich</ta>
            <ta e="T97" id="Seg_1691" s="T96">ankommen-TEMP-3SG</ta>
            <ta e="T98" id="Seg_1692" s="T97">Sonne.[NOM]</ta>
            <ta e="T99" id="Seg_1693" s="T98">EMPH</ta>
            <ta e="T100" id="Seg_1694" s="T99">scheinen-CVB.SEQ</ta>
            <ta e="T101" id="Seg_1695" s="T100">stehen-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T102" id="Seg_1696" s="T101">gut.[NOM]</ta>
            <ta e="T103" id="Seg_1697" s="T102">selbst-3SG.[NOM]</ta>
            <ta e="T104" id="Seg_1698" s="T103">dieses</ta>
            <ta e="T105" id="Seg_1699" s="T104">sich.unterhalten-CVB.SIM</ta>
            <ta e="T106" id="Seg_1700" s="T105">und</ta>
            <ta e="T107" id="Seg_1701" s="T106">sitzen-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T108" id="Seg_1702" s="T107">gut.[NOM]</ta>
            <ta e="T109" id="Seg_1703" s="T108">Mensch.[NOM]</ta>
            <ta e="T110" id="Seg_1704" s="T109">und</ta>
            <ta e="T111" id="Seg_1705" s="T110">Mensch-ACC</ta>
            <ta e="T112" id="Seg_1706" s="T111">treffen-CVB.SEQ</ta>
            <ta e="T113" id="Seg_1707" s="T112">viel</ta>
            <ta e="T114" id="Seg_1708" s="T113">Mensch.[NOM]</ta>
            <ta e="T115" id="Seg_1709" s="T114">Sprache-3SG-ACC</ta>
            <ta e="T116" id="Seg_1710" s="T115">mischen-CVB.SEQ</ta>
            <ta e="T117" id="Seg_1711" s="T116">Volk.[NOM]</ta>
            <ta e="T118" id="Seg_1712" s="T117">viel</ta>
            <ta e="T119" id="Seg_1713" s="T118">EMPH</ta>
            <ta e="T121" id="Seg_1714" s="T120">dieses</ta>
            <ta e="T122" id="Seg_1715" s="T121">nun</ta>
            <ta e="T123" id="Seg_1716" s="T122">EMPH</ta>
            <ta e="T124" id="Seg_1717" s="T123">sagen-PTCP.COND-DAT/LOC</ta>
            <ta e="T125" id="Seg_1718" s="T124">wie</ta>
            <ta e="T126" id="Seg_1719" s="T125">sagen-FUT-1SG=Q</ta>
            <ta e="T127" id="Seg_1720" s="T126">fremd.[NOM]</ta>
            <ta e="T128" id="Seg_1721" s="T127">EMPH</ta>
            <ta e="T129" id="Seg_1722" s="T128">sehr</ta>
            <ta e="T130" id="Seg_1723" s="T129">EMPH</ta>
            <ta e="T131" id="Seg_1724" s="T130">früher</ta>
            <ta e="T132" id="Seg_1725" s="T131">EMPH</ta>
            <ta e="T133" id="Seg_1726" s="T132">viel</ta>
            <ta e="T134" id="Seg_1727" s="T133">fremd.[NOM]</ta>
            <ta e="T135" id="Seg_1728" s="T134">sein-PST1-3SG</ta>
            <ta e="T136" id="Seg_1729" s="T135">EMPH</ta>
            <ta e="T137" id="Seg_1730" s="T136">lernen-PTCP.PRS</ta>
            <ta e="T138" id="Seg_1731" s="T137">sein-TEMP-DAT/LOC</ta>
            <ta e="T139" id="Seg_1732" s="T138">jetzt</ta>
            <ta e="T140" id="Seg_1733" s="T139">noch</ta>
            <ta e="T141" id="Seg_1734" s="T140">zunehmen-CVB.SEQ</ta>
            <ta e="T142" id="Seg_1735" s="T141">gehen-PRS.[3SG]</ta>
            <ta e="T143" id="Seg_1736" s="T142">jetzt</ta>
            <ta e="T144" id="Seg_1737" s="T143">gerade</ta>
            <ta e="T145" id="Seg_1738" s="T144">sprechen-NEG-1SG</ta>
            <ta e="T146" id="Seg_1739" s="T145">woher</ta>
            <ta e="T147" id="Seg_1740" s="T146">jetzt</ta>
            <ta e="T148" id="Seg_1741" s="T147">noch</ta>
            <ta e="T149" id="Seg_1742" s="T148">und</ta>
            <ta e="T150" id="Seg_1743" s="T149">finden-CVB.SEQ</ta>
            <ta e="T151" id="Seg_1744" s="T150">gehen-PRS-3PL</ta>
            <ta e="T152" id="Seg_1745" s="T151">Mensch-PL-ACC</ta>
            <ta e="T153" id="Seg_1746" s="T152">nun</ta>
            <ta e="T158" id="Seg_1747" s="T157">Mensch.[NOM]</ta>
            <ta e="T159" id="Seg_1748" s="T158">verlorengehen-PRS.[3SG]</ta>
            <ta e="T160" id="Seg_1749" s="T159">Mensch-PL.[NOM]</ta>
            <ta e="T161" id="Seg_1750" s="T160">finden-EP-PASS/REFL-PRS-3PL</ta>
            <ta e="T173" id="Seg_1751" s="T172">was-ACC</ta>
            <ta e="T174" id="Seg_1752" s="T173">singen-FUT-1SG=Q</ta>
         </annotation>
         <annotation name="gr" tierref="gr-AnSP">
            <ta e="T8" id="Seg_1753" s="T7">EMPH</ta>
            <ta e="T9" id="Seg_1754" s="T8">EMPH</ta>
            <ta e="T11" id="Seg_1755" s="T10">маленький-INTNS-ADVZ</ta>
            <ta e="T12" id="Seg_1756" s="T11">маленький.[NOM]</ta>
            <ta e="T13" id="Seg_1757" s="T12">ээ</ta>
            <ta e="T14" id="Seg_1758" s="T13">девушка-DAT/LOC</ta>
            <ta e="T15" id="Seg_1759" s="T14">говорить.[IMP.2SG]</ta>
            <ta e="T16" id="Seg_1760" s="T15">девушка-DIM-DAT/LOC</ta>
            <ta e="T17" id="Seg_1761" s="T16">тот</ta>
            <ta e="T18" id="Seg_1762" s="T17">подарок-ACC</ta>
            <ta e="T19" id="Seg_1763" s="T18">приносить-CVB.SEQ</ta>
            <ta e="T20" id="Seg_1764" s="T19">приходить-PRS-1SG</ta>
            <ta e="T21" id="Seg_1765" s="T20">этот</ta>
            <ta e="T22" id="Seg_1766" s="T21">стол-DAT/LOC</ta>
            <ta e="T23" id="Seg_1767" s="T22">сидеть-PRS-1SG</ta>
            <ta e="T24" id="Seg_1768" s="T23">девушка-ACC</ta>
            <ta e="T25" id="Seg_1769" s="T24">видеть-PRS-1SG</ta>
            <ta e="T26" id="Seg_1770" s="T25">жена-1SG-ACC</ta>
            <ta e="T27" id="Seg_1771" s="T26">видеть-PST1-1SG</ta>
            <ta e="T28" id="Seg_1772" s="T27">каждый-INTNS-ACC</ta>
            <ta e="T29" id="Seg_1773" s="T28">учить-PTCP.PRS-3PL-ACC</ta>
            <ta e="T30" id="Seg_1774" s="T29">как</ta>
            <ta e="T31" id="Seg_1775" s="T30">язык-ACC</ta>
            <ta e="T34" id="Seg_1776" s="T33">и.так.далее-CVB.SEQ</ta>
            <ta e="T35" id="Seg_1777" s="T34">да</ta>
            <ta e="T36" id="Seg_1778" s="T35">ребенок-1SG.[NOM]</ta>
            <ta e="T37" id="Seg_1779" s="T36">учить-PST1-3SG</ta>
            <ta e="T40" id="Seg_1780" s="T39">этот</ta>
            <ta e="T41" id="Seg_1781" s="T40">вот</ta>
            <ta e="T42" id="Seg_1782" s="T41">EMPH</ta>
            <ta e="T43" id="Seg_1783" s="T42">как</ta>
            <ta e="T44" id="Seg_1784" s="T43">говорить-HAB-3PL=Q</ta>
            <ta e="T45" id="Seg_1785" s="T44">1PL-DAT/LOC</ta>
            <ta e="T46" id="Seg_1786" s="T45">дом-PROPR</ta>
            <ta e="T47" id="Seg_1787" s="T46">девушка.[NOM]</ta>
            <ta e="T48" id="Seg_1788" s="T47">вот</ta>
            <ta e="T49" id="Seg_1789" s="T48">говорить-HAB-3PL</ta>
            <ta e="T50" id="Seg_1790" s="T49">э</ta>
            <ta e="T51" id="Seg_1791" s="T50">вот</ta>
            <ta e="T61" id="Seg_1792" s="T60">3SG.[NOM]</ta>
            <ta e="T62" id="Seg_1793" s="T61">радоваться-PST1-3SG</ta>
            <ta e="T63" id="Seg_1794" s="T62">этот</ta>
            <ta e="T64" id="Seg_1795" s="T63">1SG.[NOM]</ta>
            <ta e="T65" id="Seg_1796" s="T64">принести-PTCP.PST-1SG-DAT/LOC</ta>
            <ta e="T66" id="Seg_1797" s="T65">тот</ta>
            <ta e="T67" id="Seg_1798" s="T66">старуха.[NOM]</ta>
            <ta e="T68" id="Seg_1799" s="T67">опять</ta>
            <ta e="T69" id="Seg_1800" s="T68">послать-PTCP.HAB</ta>
            <ta e="T70" id="Seg_1801" s="T69">жена.[NOM]</ta>
            <ta e="T71" id="Seg_1802" s="T70">хороший-ADVZ</ta>
            <ta e="T72" id="Seg_1803" s="T71">мысль-3SG-ACC</ta>
            <ta e="T73" id="Seg_1804" s="T72">потом</ta>
            <ta e="T74" id="Seg_1805" s="T73">MOD</ta>
            <ta e="T75" id="Seg_1806" s="T74">что.[NOM]</ta>
            <ta e="T76" id="Seg_1807" s="T75">говорить-PTCP.FUT</ta>
            <ta e="T81" id="Seg_1808" s="T80">встречать-CVB.SEQ</ta>
            <ta e="T82" id="Seg_1809" s="T81">идти-FUT-1PL</ta>
            <ta e="T83" id="Seg_1810" s="T82">потом</ta>
            <ta e="T84" id="Seg_1811" s="T83">желать-VBZ-PRS-1SG</ta>
            <ta e="T85" id="Seg_1812" s="T84">этот</ta>
            <ta e="T86" id="Seg_1813" s="T85">девушка-DAT/LOC</ta>
            <ta e="T87" id="Seg_1814" s="T86">хороший.[NOM]</ta>
            <ta e="T88" id="Seg_1815" s="T87">мальчик-PART</ta>
            <ta e="T89" id="Seg_1816" s="T88">найти-PTCP.FUT-DAT/LOC</ta>
            <ta e="T90" id="Seg_1817" s="T89">вот</ta>
            <ta e="T91" id="Seg_1818" s="T90">голова-PROPR</ta>
            <ta e="T92" id="Seg_1819" s="T91">EMPH-PART</ta>
            <ta e="T93" id="Seg_1820" s="T92">такой</ta>
            <ta e="T94" id="Seg_1821" s="T93">ясный</ta>
            <ta e="T95" id="Seg_1822" s="T94">ясный-ADVZ</ta>
            <ta e="T96" id="Seg_1823" s="T95">счастливый</ta>
            <ta e="T97" id="Seg_1824" s="T96">доезжать-TEMP-3SG</ta>
            <ta e="T98" id="Seg_1825" s="T97">солнце.[NOM]</ta>
            <ta e="T99" id="Seg_1826" s="T98">EMPH</ta>
            <ta e="T100" id="Seg_1827" s="T99">светить-CVB.SEQ</ta>
            <ta e="T101" id="Seg_1828" s="T100">стоять-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T102" id="Seg_1829" s="T101">хороший.[NOM]</ta>
            <ta e="T103" id="Seg_1830" s="T102">сам-3SG.[NOM]</ta>
            <ta e="T104" id="Seg_1831" s="T103">этот</ta>
            <ta e="T105" id="Seg_1832" s="T104">разговаривать-CVB.SIM</ta>
            <ta e="T106" id="Seg_1833" s="T105">да</ta>
            <ta e="T107" id="Seg_1834" s="T106">сидеть-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T108" id="Seg_1835" s="T107">хороший.[NOM]</ta>
            <ta e="T109" id="Seg_1836" s="T108">человек.[NOM]</ta>
            <ta e="T110" id="Seg_1837" s="T109">да</ta>
            <ta e="T111" id="Seg_1838" s="T110">человек-ACC</ta>
            <ta e="T112" id="Seg_1839" s="T111">встречать-CVB.SEQ</ta>
            <ta e="T113" id="Seg_1840" s="T112">много</ta>
            <ta e="T114" id="Seg_1841" s="T113">человек.[NOM]</ta>
            <ta e="T115" id="Seg_1842" s="T114">язык-3SG-ACC</ta>
            <ta e="T116" id="Seg_1843" s="T115">мешать-CVB.SEQ</ta>
            <ta e="T117" id="Seg_1844" s="T116">народ.[NOM]</ta>
            <ta e="T118" id="Seg_1845" s="T117">много</ta>
            <ta e="T119" id="Seg_1846" s="T118">EMPH</ta>
            <ta e="T121" id="Seg_1847" s="T120">этот</ta>
            <ta e="T122" id="Seg_1848" s="T121">вот</ta>
            <ta e="T123" id="Seg_1849" s="T122">EMPH</ta>
            <ta e="T124" id="Seg_1850" s="T123">говорить-PTCP.COND-DAT/LOC</ta>
            <ta e="T125" id="Seg_1851" s="T124">как</ta>
            <ta e="T126" id="Seg_1852" s="T125">говорить-FUT-1SG=Q</ta>
            <ta e="T127" id="Seg_1853" s="T126">чужой.[NOM]</ta>
            <ta e="T128" id="Seg_1854" s="T127">EMPH</ta>
            <ta e="T129" id="Seg_1855" s="T128">очень</ta>
            <ta e="T130" id="Seg_1856" s="T129">EMPH</ta>
            <ta e="T131" id="Seg_1857" s="T130">раньше</ta>
            <ta e="T132" id="Seg_1858" s="T131">EMPH</ta>
            <ta e="T133" id="Seg_1859" s="T132">много</ta>
            <ta e="T134" id="Seg_1860" s="T133">чужой.[NOM]</ta>
            <ta e="T135" id="Seg_1861" s="T134">быть-PST1-3SG</ta>
            <ta e="T136" id="Seg_1862" s="T135">EMPH</ta>
            <ta e="T137" id="Seg_1863" s="T136">учиться-PTCP.PRS</ta>
            <ta e="T138" id="Seg_1864" s="T137">быть-TEMP-DAT/LOC</ta>
            <ta e="T139" id="Seg_1865" s="T138">теперь</ta>
            <ta e="T140" id="Seg_1866" s="T139">еще</ta>
            <ta e="T141" id="Seg_1867" s="T140">прибавляться-CVB.SEQ</ta>
            <ta e="T142" id="Seg_1868" s="T141">идти-PRS.[3SG]</ta>
            <ta e="T143" id="Seg_1869" s="T142">теперь</ta>
            <ta e="T144" id="Seg_1870" s="T143">прямо</ta>
            <ta e="T145" id="Seg_1871" s="T144">говорить-NEG-1SG</ta>
            <ta e="T146" id="Seg_1872" s="T145">откуда</ta>
            <ta e="T147" id="Seg_1873" s="T146">теперь</ta>
            <ta e="T148" id="Seg_1874" s="T147">еще</ta>
            <ta e="T149" id="Seg_1875" s="T148">да</ta>
            <ta e="T150" id="Seg_1876" s="T149">найти-CVB.SEQ</ta>
            <ta e="T151" id="Seg_1877" s="T150">идти-PRS-3PL</ta>
            <ta e="T152" id="Seg_1878" s="T151">человек-PL-ACC</ta>
            <ta e="T153" id="Seg_1879" s="T152">вот</ta>
            <ta e="T158" id="Seg_1880" s="T157">человек.[NOM]</ta>
            <ta e="T159" id="Seg_1881" s="T158">пропадать-PRS.[3SG]</ta>
            <ta e="T160" id="Seg_1882" s="T159">человек-PL.[NOM]</ta>
            <ta e="T161" id="Seg_1883" s="T160">найти-EP-PASS/REFL-PRS-3PL</ta>
            <ta e="T173" id="Seg_1884" s="T172">что-ACC</ta>
            <ta e="T174" id="Seg_1885" s="T173">петь-FUT-1SG=Q</ta>
         </annotation>
         <annotation name="mc" tierref="mc-AnSP">
            <ta e="T8" id="Seg_1886" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_1887" s="T8">ptcl</ta>
            <ta e="T11" id="Seg_1888" s="T10">adj-adj&gt;adj-adj&gt;adv</ta>
            <ta e="T12" id="Seg_1889" s="T11">adj.[n:case]</ta>
            <ta e="T13" id="Seg_1890" s="T12">interj</ta>
            <ta e="T14" id="Seg_1891" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_1892" s="T14">v.[v:mood.pn]</ta>
            <ta e="T16" id="Seg_1893" s="T15">n-n&gt;n-n:case</ta>
            <ta e="T17" id="Seg_1894" s="T16">dempro</ta>
            <ta e="T18" id="Seg_1895" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_1896" s="T18">v-v:cvb</ta>
            <ta e="T20" id="Seg_1897" s="T19">v-v:tense-v:pred.pn</ta>
            <ta e="T21" id="Seg_1898" s="T20">dempro</ta>
            <ta e="T22" id="Seg_1899" s="T21">n-n:case</ta>
            <ta e="T23" id="Seg_1900" s="T22">v-v:tense-v:pred.pn</ta>
            <ta e="T24" id="Seg_1901" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_1902" s="T24">v-v:tense-v:pred.pn</ta>
            <ta e="T26" id="Seg_1903" s="T25">n-n:poss-n:case</ta>
            <ta e="T27" id="Seg_1904" s="T26">v-v:tense-v:poss.pn</ta>
            <ta e="T28" id="Seg_1905" s="T27">adj-adj&gt;adj-n:case</ta>
            <ta e="T29" id="Seg_1906" s="T28">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T30" id="Seg_1907" s="T29">que</ta>
            <ta e="T31" id="Seg_1908" s="T30">n-n:case</ta>
            <ta e="T34" id="Seg_1909" s="T33">v-v:cvb</ta>
            <ta e="T35" id="Seg_1910" s="T34">conj</ta>
            <ta e="T36" id="Seg_1911" s="T35">n-n:(poss).[n:case]</ta>
            <ta e="T37" id="Seg_1912" s="T36">v-v:tense-v:poss.pn</ta>
            <ta e="T40" id="Seg_1913" s="T39">dempro</ta>
            <ta e="T41" id="Seg_1914" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_1915" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_1916" s="T42">que</ta>
            <ta e="T44" id="Seg_1917" s="T43">v-v:mood-v:pred.pn=ptcl</ta>
            <ta e="T45" id="Seg_1918" s="T44">pers-pro:case</ta>
            <ta e="T46" id="Seg_1919" s="T45">n-n&gt;adj</ta>
            <ta e="T47" id="Seg_1920" s="T46">n.[n:case]</ta>
            <ta e="T48" id="Seg_1921" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_1922" s="T48">v-v:mood-v:pred.pn</ta>
            <ta e="T50" id="Seg_1923" s="T49">interj</ta>
            <ta e="T51" id="Seg_1924" s="T50">ptcl</ta>
            <ta e="T61" id="Seg_1925" s="T60">pers.[pro:case]</ta>
            <ta e="T62" id="Seg_1926" s="T61">v-v:tense-v:poss.pn</ta>
            <ta e="T63" id="Seg_1927" s="T62">dempro</ta>
            <ta e="T64" id="Seg_1928" s="T63">pers.[pro:case]</ta>
            <ta e="T65" id="Seg_1929" s="T64">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T66" id="Seg_1930" s="T65">dempro</ta>
            <ta e="T67" id="Seg_1931" s="T66">n.[n:case]</ta>
            <ta e="T68" id="Seg_1932" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_1933" s="T68">v-v:ptcp</ta>
            <ta e="T70" id="Seg_1934" s="T69">n.[n:case]</ta>
            <ta e="T71" id="Seg_1935" s="T70">adj-adj&gt;adv</ta>
            <ta e="T72" id="Seg_1936" s="T71">n-n:poss-n:case</ta>
            <ta e="T73" id="Seg_1937" s="T72">adv</ta>
            <ta e="T74" id="Seg_1938" s="T73">ptcl</ta>
            <ta e="T75" id="Seg_1939" s="T74">que.[pro:case]</ta>
            <ta e="T76" id="Seg_1940" s="T75">v-v:ptcp</ta>
            <ta e="T81" id="Seg_1941" s="T80">v-v:cvb</ta>
            <ta e="T82" id="Seg_1942" s="T81">v-v:tense-v:poss.pn</ta>
            <ta e="T83" id="Seg_1943" s="T82">adv</ta>
            <ta e="T84" id="Seg_1944" s="T83">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T85" id="Seg_1945" s="T84">dempro</ta>
            <ta e="T86" id="Seg_1946" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_1947" s="T86">adj.[n:case]</ta>
            <ta e="T88" id="Seg_1948" s="T87">n-n:case</ta>
            <ta e="T89" id="Seg_1949" s="T88">v-v:ptcp-v:(case)</ta>
            <ta e="T90" id="Seg_1950" s="T89">ptcl</ta>
            <ta e="T91" id="Seg_1951" s="T90">n-n&gt;adj</ta>
            <ta e="T92" id="Seg_1952" s="T91">ptcl-n:case</ta>
            <ta e="T93" id="Seg_1953" s="T92">dempro</ta>
            <ta e="T94" id="Seg_1954" s="T93">adj</ta>
            <ta e="T95" id="Seg_1955" s="T94">adj-adj&gt;adv</ta>
            <ta e="T96" id="Seg_1956" s="T95">adj</ta>
            <ta e="T97" id="Seg_1957" s="T96">v-v:mood-v:temp.pn</ta>
            <ta e="T98" id="Seg_1958" s="T97">n.[n:case]</ta>
            <ta e="T99" id="Seg_1959" s="T98">ptcl</ta>
            <ta e="T100" id="Seg_1960" s="T99">v-v:cvb</ta>
            <ta e="T101" id="Seg_1961" s="T100">v-v:ptcp-v:(poss).[v:(case)]</ta>
            <ta e="T102" id="Seg_1962" s="T101">adj.[n:case]</ta>
            <ta e="T103" id="Seg_1963" s="T102">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T104" id="Seg_1964" s="T103">dempro</ta>
            <ta e="T105" id="Seg_1965" s="T104">v-v:cvb</ta>
            <ta e="T106" id="Seg_1966" s="T105">conj</ta>
            <ta e="T107" id="Seg_1967" s="T106">v-v:ptcp-v:(poss).[v:(case)]</ta>
            <ta e="T108" id="Seg_1968" s="T107">adj.[n:case]</ta>
            <ta e="T109" id="Seg_1969" s="T108">n.[n:case]</ta>
            <ta e="T110" id="Seg_1970" s="T109">conj</ta>
            <ta e="T111" id="Seg_1971" s="T110">n-n:case</ta>
            <ta e="T112" id="Seg_1972" s="T111">v-v:cvb</ta>
            <ta e="T113" id="Seg_1973" s="T112">quant</ta>
            <ta e="T114" id="Seg_1974" s="T113">n.[n:case]</ta>
            <ta e="T115" id="Seg_1975" s="T114">n-n:poss-n:case</ta>
            <ta e="T116" id="Seg_1976" s="T115">v-v:cvb</ta>
            <ta e="T117" id="Seg_1977" s="T116">n.[n:case]</ta>
            <ta e="T118" id="Seg_1978" s="T117">quant</ta>
            <ta e="T119" id="Seg_1979" s="T118">ptcl</ta>
            <ta e="T121" id="Seg_1980" s="T120">dempro</ta>
            <ta e="T122" id="Seg_1981" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_1982" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_1983" s="T123">v-v:ptcp-v:(case)</ta>
            <ta e="T125" id="Seg_1984" s="T124">que</ta>
            <ta e="T126" id="Seg_1985" s="T125">v-v:tense-v:pred.pn=ptcl</ta>
            <ta e="T127" id="Seg_1986" s="T126">adj.[n:case]</ta>
            <ta e="T128" id="Seg_1987" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_1988" s="T128">adv</ta>
            <ta e="T130" id="Seg_1989" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_1990" s="T130">adv</ta>
            <ta e="T132" id="Seg_1991" s="T131">ptcl</ta>
            <ta e="T133" id="Seg_1992" s="T132">quant</ta>
            <ta e="T134" id="Seg_1993" s="T133">adj.[n:case]</ta>
            <ta e="T135" id="Seg_1994" s="T134">v-v:tense-v:poss.pn</ta>
            <ta e="T136" id="Seg_1995" s="T135">ptcl</ta>
            <ta e="T137" id="Seg_1996" s="T136">v-v:ptcp</ta>
            <ta e="T138" id="Seg_1997" s="T137">v-v:mood-v:(case)</ta>
            <ta e="T139" id="Seg_1998" s="T138">adv</ta>
            <ta e="T140" id="Seg_1999" s="T139">adv</ta>
            <ta e="T141" id="Seg_2000" s="T140">v-v:cvb</ta>
            <ta e="T142" id="Seg_2001" s="T141">v-v:tense.[v:pred.pn]</ta>
            <ta e="T143" id="Seg_2002" s="T142">adv</ta>
            <ta e="T144" id="Seg_2003" s="T143">adv</ta>
            <ta e="T145" id="Seg_2004" s="T144">v-v:(neg)-v:pred.pn</ta>
            <ta e="T146" id="Seg_2005" s="T145">que</ta>
            <ta e="T147" id="Seg_2006" s="T146">adv</ta>
            <ta e="T148" id="Seg_2007" s="T147">adv</ta>
            <ta e="T149" id="Seg_2008" s="T148">conj</ta>
            <ta e="T150" id="Seg_2009" s="T149">v-v:cvb</ta>
            <ta e="T151" id="Seg_2010" s="T150">v-v:tense-v:pred.pn</ta>
            <ta e="T152" id="Seg_2011" s="T151">n-n:(num)-n:case</ta>
            <ta e="T153" id="Seg_2012" s="T152">ptcl</ta>
            <ta e="T158" id="Seg_2013" s="T157">n.[n:case]</ta>
            <ta e="T159" id="Seg_2014" s="T158">v-v:tense.[v:pred.pn]</ta>
            <ta e="T160" id="Seg_2015" s="T159">n-n:(num).[n:case]</ta>
            <ta e="T161" id="Seg_2016" s="T160">v-n:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T173" id="Seg_2017" s="T172">que-pro:case</ta>
            <ta e="T174" id="Seg_2018" s="T173">v-v:tense-v:pred.pn=ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps-AnSP">
            <ta e="T8" id="Seg_2019" s="T7">ptcl</ta>
            <ta e="T9" id="Seg_2020" s="T8">ptcl</ta>
            <ta e="T11" id="Seg_2021" s="T10">adv</ta>
            <ta e="T12" id="Seg_2022" s="T11">adj</ta>
            <ta e="T13" id="Seg_2023" s="T12">interj</ta>
            <ta e="T14" id="Seg_2024" s="T13">n</ta>
            <ta e="T15" id="Seg_2025" s="T14">v</ta>
            <ta e="T16" id="Seg_2026" s="T15">n</ta>
            <ta e="T17" id="Seg_2027" s="T16">dempro</ta>
            <ta e="T18" id="Seg_2028" s="T17">n</ta>
            <ta e="T19" id="Seg_2029" s="T18">v</ta>
            <ta e="T20" id="Seg_2030" s="T19">v</ta>
            <ta e="T21" id="Seg_2031" s="T20">dempro</ta>
            <ta e="T22" id="Seg_2032" s="T21">n</ta>
            <ta e="T23" id="Seg_2033" s="T22">v</ta>
            <ta e="T24" id="Seg_2034" s="T23">n</ta>
            <ta e="T25" id="Seg_2035" s="T24">v</ta>
            <ta e="T26" id="Seg_2036" s="T25">n</ta>
            <ta e="T27" id="Seg_2037" s="T26">v</ta>
            <ta e="T28" id="Seg_2038" s="T27">n</ta>
            <ta e="T29" id="Seg_2039" s="T28">v</ta>
            <ta e="T30" id="Seg_2040" s="T29">que</ta>
            <ta e="T31" id="Seg_2041" s="T30">n</ta>
            <ta e="T34" id="Seg_2042" s="T33">v</ta>
            <ta e="T35" id="Seg_2043" s="T34">conj</ta>
            <ta e="T36" id="Seg_2044" s="T35">n</ta>
            <ta e="T37" id="Seg_2045" s="T36">v</ta>
            <ta e="T40" id="Seg_2046" s="T39">dempro</ta>
            <ta e="T41" id="Seg_2047" s="T40">ptcl</ta>
            <ta e="T42" id="Seg_2048" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_2049" s="T42">que</ta>
            <ta e="T44" id="Seg_2050" s="T43">v</ta>
            <ta e="T45" id="Seg_2051" s="T44">pers</ta>
            <ta e="T46" id="Seg_2052" s="T45">adj</ta>
            <ta e="T47" id="Seg_2053" s="T46">n</ta>
            <ta e="T48" id="Seg_2054" s="T47">ptcl</ta>
            <ta e="T49" id="Seg_2055" s="T48">v</ta>
            <ta e="T50" id="Seg_2056" s="T49">interj</ta>
            <ta e="T51" id="Seg_2057" s="T50">ptcl</ta>
            <ta e="T61" id="Seg_2058" s="T60">pers</ta>
            <ta e="T62" id="Seg_2059" s="T61">v</ta>
            <ta e="T63" id="Seg_2060" s="T62">dempro</ta>
            <ta e="T64" id="Seg_2061" s="T63">pers</ta>
            <ta e="T65" id="Seg_2062" s="T64">v</ta>
            <ta e="T66" id="Seg_2063" s="T65">dempro</ta>
            <ta e="T67" id="Seg_2064" s="T66">n</ta>
            <ta e="T68" id="Seg_2065" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_2066" s="T68">v</ta>
            <ta e="T70" id="Seg_2067" s="T69">n</ta>
            <ta e="T71" id="Seg_2068" s="T70">adv</ta>
            <ta e="T72" id="Seg_2069" s="T71">n</ta>
            <ta e="T73" id="Seg_2070" s="T72">adv</ta>
            <ta e="T74" id="Seg_2071" s="T73">ptcl</ta>
            <ta e="T75" id="Seg_2072" s="T74">que</ta>
            <ta e="T76" id="Seg_2073" s="T75">v</ta>
            <ta e="T81" id="Seg_2074" s="T80">v</ta>
            <ta e="T82" id="Seg_2075" s="T81">aux</ta>
            <ta e="T83" id="Seg_2076" s="T82">adv</ta>
            <ta e="T84" id="Seg_2077" s="T83">v</ta>
            <ta e="T85" id="Seg_2078" s="T84">dempro</ta>
            <ta e="T86" id="Seg_2079" s="T85">n</ta>
            <ta e="T87" id="Seg_2080" s="T86">adj</ta>
            <ta e="T88" id="Seg_2081" s="T87">n</ta>
            <ta e="T89" id="Seg_2082" s="T88">v</ta>
            <ta e="T90" id="Seg_2083" s="T89">ptcl</ta>
            <ta e="T91" id="Seg_2084" s="T90">adj</ta>
            <ta e="T92" id="Seg_2085" s="T91">ptcl</ta>
            <ta e="T93" id="Seg_2086" s="T92">dempro</ta>
            <ta e="T94" id="Seg_2087" s="T93">adj</ta>
            <ta e="T95" id="Seg_2088" s="T94">adv</ta>
            <ta e="T96" id="Seg_2089" s="T95">adj</ta>
            <ta e="T97" id="Seg_2090" s="T96">v</ta>
            <ta e="T98" id="Seg_2091" s="T97">n</ta>
            <ta e="T99" id="Seg_2092" s="T98">ptcl</ta>
            <ta e="T100" id="Seg_2093" s="T99">v</ta>
            <ta e="T101" id="Seg_2094" s="T100">aux</ta>
            <ta e="T102" id="Seg_2095" s="T101">adj</ta>
            <ta e="T103" id="Seg_2096" s="T102">emphpro</ta>
            <ta e="T104" id="Seg_2097" s="T103">dempro</ta>
            <ta e="T105" id="Seg_2098" s="T104">v</ta>
            <ta e="T106" id="Seg_2099" s="T105">conj</ta>
            <ta e="T107" id="Seg_2100" s="T106">v</ta>
            <ta e="T108" id="Seg_2101" s="T107">adj</ta>
            <ta e="T109" id="Seg_2102" s="T108">n</ta>
            <ta e="T110" id="Seg_2103" s="T109">conj</ta>
            <ta e="T111" id="Seg_2104" s="T110">n</ta>
            <ta e="T112" id="Seg_2105" s="T111">v</ta>
            <ta e="T113" id="Seg_2106" s="T112">quant</ta>
            <ta e="T114" id="Seg_2107" s="T113">n</ta>
            <ta e="T115" id="Seg_2108" s="T114">n</ta>
            <ta e="T116" id="Seg_2109" s="T115">v</ta>
            <ta e="T117" id="Seg_2110" s="T116">n</ta>
            <ta e="T118" id="Seg_2111" s="T117">quant</ta>
            <ta e="T119" id="Seg_2112" s="T118">ptcl</ta>
            <ta e="T121" id="Seg_2113" s="T120">dempro</ta>
            <ta e="T122" id="Seg_2114" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_2115" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_2116" s="T123">v</ta>
            <ta e="T125" id="Seg_2117" s="T124">que</ta>
            <ta e="T126" id="Seg_2118" s="T125">v</ta>
            <ta e="T127" id="Seg_2119" s="T126">n</ta>
            <ta e="T128" id="Seg_2120" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_2121" s="T128">adv</ta>
            <ta e="T130" id="Seg_2122" s="T129">ptcl</ta>
            <ta e="T131" id="Seg_2123" s="T130">adv</ta>
            <ta e="T132" id="Seg_2124" s="T131">ptcl</ta>
            <ta e="T133" id="Seg_2125" s="T132">quant</ta>
            <ta e="T134" id="Seg_2126" s="T133">n</ta>
            <ta e="T135" id="Seg_2127" s="T134">cop</ta>
            <ta e="T136" id="Seg_2128" s="T135">ptcl</ta>
            <ta e="T137" id="Seg_2129" s="T136">v</ta>
            <ta e="T138" id="Seg_2130" s="T137">aux</ta>
            <ta e="T139" id="Seg_2131" s="T138">adv</ta>
            <ta e="T140" id="Seg_2132" s="T139">adv</ta>
            <ta e="T141" id="Seg_2133" s="T140">v</ta>
            <ta e="T142" id="Seg_2134" s="T141">aux</ta>
            <ta e="T143" id="Seg_2135" s="T142">adv</ta>
            <ta e="T144" id="Seg_2136" s="T143">adv</ta>
            <ta e="T145" id="Seg_2137" s="T144">v</ta>
            <ta e="T146" id="Seg_2138" s="T145">que</ta>
            <ta e="T147" id="Seg_2139" s="T146">adv</ta>
            <ta e="T148" id="Seg_2140" s="T147">adv</ta>
            <ta e="T149" id="Seg_2141" s="T148">conj</ta>
            <ta e="T150" id="Seg_2142" s="T149">v</ta>
            <ta e="T151" id="Seg_2143" s="T150">aux</ta>
            <ta e="T152" id="Seg_2144" s="T151">n</ta>
            <ta e="T153" id="Seg_2145" s="T152">ptcl</ta>
            <ta e="T158" id="Seg_2146" s="T157">n</ta>
            <ta e="T159" id="Seg_2147" s="T158">v</ta>
            <ta e="T160" id="Seg_2148" s="T159">n</ta>
            <ta e="T161" id="Seg_2149" s="T160">v</ta>
            <ta e="T173" id="Seg_2150" s="T172">que</ta>
            <ta e="T174" id="Seg_2151" s="T173">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-AnSP" />
         <annotation name="SyF" tierref="SyF-AnSP" />
         <annotation name="IST" tierref="IST-AnSP" />
         <annotation name="Top" tierref="Top-AnSP" />
         <annotation name="Foc" tierref="Foc-AnSP" />
         <annotation name="BOR" tierref="BOR-AnSP">
            <ta e="T11" id="Seg_2152" s="T10">EV:gram (DIM)</ta>
            <ta e="T22" id="Seg_2153" s="T21">RUS:cult</ta>
            <ta e="T28" id="Seg_2154" s="T27">EV:gram (DIM)</ta>
            <ta e="T31" id="Seg_2155" s="T30">RUS:core</ta>
            <ta e="T35" id="Seg_2156" s="T34">RUS:gram</ta>
            <ta e="T84" id="Seg_2157" s="T83">RUS:core</ta>
            <ta e="T106" id="Seg_2158" s="T105">RUS:gram</ta>
            <ta e="T110" id="Seg_2159" s="T109">RUS:gram</ta>
            <ta e="T140" id="Seg_2160" s="T139">RUS:mod</ta>
            <ta e="T148" id="Seg_2161" s="T147">RUS:mod</ta>
            <ta e="T149" id="Seg_2162" s="T148">RUS:gram</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-AnSP" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-AnSP" />
         <annotation name="CS" tierref="CS-AnSP" />
         <annotation name="fe" tierref="fe-AnSP">
            <ta e="T12" id="Seg_2163" s="T7">Well a little bit, a little.</ta>
            <ta e="T23" id="Seg_2164" s="T12">Tell the girl, I came to bring a present, I sit behind this table.</ta>
            <ta e="T31" id="Seg_2165" s="T23">I see the girl, I saw my wife, that everybody studies some languages.</ta>
            <ta e="T37" id="Seg_2166" s="T31">Whatever language, any child can learn.</ta>
            <ta e="T51" id="Seg_2167" s="T37">My, how to say, in our house lives a girl.</ta>
            <ta e="T53" id="Seg_2168" s="T51">A house mate?</ta>
            <ta e="T60" id="Seg_2169" s="T57">In the flat.</ta>
            <ta e="T72" id="Seg_2170" s="T60">She was happy when I brought the good words that the old woman sent.</ta>
            <ta e="T82" id="Seg_2171" s="T72">Then what should I say, we will see each other again on arrival.</ta>
            <ta e="T92" id="Seg_2172" s="T82">Then I wish the girl to find a good boy, with some brain!</ta>
            <ta e="T97" id="Seg_2173" s="T92">Like that to arrive well (?).</ta>
            <ta e="T108" id="Seg_2174" s="T97">The sun is shining, that's good, she talks and sits thats good.</ta>
            <ta e="T119" id="Seg_2175" s="T108">A person meets a person, after having mixed many peoples languages, many foreigners (nationalities?).</ta>
            <ta e="T120" id="Seg_2176" s="T119">Planet.</ta>
            <ta e="T124" id="Seg_2177" s="T120">So to say.</ta>
            <ta e="T138" id="Seg_2178" s="T124">How to say, earlier there were many foreigners, when I was studying.</ta>
            <ta e="T142" id="Seg_2179" s="T138">Now it becomes even more.</ta>
            <ta e="T157" id="Seg_2180" s="T142">Now I don't say directly, now they still find people, somewhere on islands…</ta>
            <ta e="T161" id="Seg_2181" s="T157">People get lost and people are found.</ta>
            <ta e="T177" id="Seg_2182" s="T172">What should I sing?</ta>
         </annotation>
         <annotation name="fg" tierref="fg-AnSP">
            <ta e="T12" id="Seg_2183" s="T7">Nun, ein ganz bisschen, ein bisschen.</ta>
            <ta e="T23" id="Seg_2184" s="T12">Äh, sag dem Mädchen, dem kleinen Mädchen, ich komme und bringe ein Geschenk, ich sitze am Tisch.</ta>
            <ta e="T31" id="Seg_2185" s="T23">Ich sehe das Mädchen, ich sah meine Frau, dass jeder irgendwie Sprachen lernt.</ta>
            <ta e="T37" id="Seg_2186" s="T31">Irgendeine Sprache und so weiter, mein Kind hat gelernt.</ta>
            <ta e="T51" id="Seg_2187" s="T37">Mein, wie sagt man, wir haben ein Hausmädchen, nun sagen sie doch.</ta>
            <ta e="T53" id="Seg_2188" s="T51">Eine Mitbewohnerin ja?</ta>
            <ta e="T60" id="Seg_2189" s="T57">Nun, in der Wohnung.</ta>
            <ta e="T72" id="Seg_2190" s="T60">Sie freute sich, als die guten Gedanken brachte, die die alte Frau geschickt hatte.</ta>
            <ta e="T82" id="Seg_2191" s="T72">Dann was [soll ich] sagen, bei der Ankunft treffen wir uns wieder.</ta>
            <ta e="T92" id="Seg_2192" s="T82">Dann wünsche ich dem Mädchen, dass es einen guten Jungen findet, einen sehr klugen.</ta>
            <ta e="T97" id="Seg_2193" s="T92">Einer, der glücklich ankommt (?).</ta>
            <ta e="T108" id="Seg_2194" s="T97">Es ist gut, dass die Sonne scheint, es ist gut, dass sie sitzt und sich unterhält.</ta>
            <ta e="T119" id="Seg_2195" s="T108">Menschen treffen Menschen, die Sprachen vieler Menschen mischen sich, [es gibt] viele Völker.</ta>
            <ta e="T120" id="Seg_2196" s="T119">Planet.</ta>
            <ta e="T124" id="Seg_2197" s="T120">Sozusagen.</ta>
            <ta e="T138" id="Seg_2198" s="T124">Wie soll ich sagen, früher gab es viele Fremde, als ich studierte.</ta>
            <ta e="T142" id="Seg_2199" s="T138">Jetzt wird es noch mehr.</ta>
            <ta e="T157" id="Seg_2200" s="T142">Nun sage ich nicht direkt, jetzt finden sie immer noch Menschen, irgendwo auf Inseln.</ta>
            <ta e="T161" id="Seg_2201" s="T157">Menschen gehen verloren und Menschen werden gefunden.</ta>
            <ta e="T177" id="Seg_2202" s="T172">Was soll ich singen?</ta>
         </annotation>
         <annotation name="fr" tierref="fr-AnSP" />
         <annotation name="ltr" tierref="ltr-AnSP">
            <ta e="T12" id="Seg_2203" s="T7">Муж: аха ну мал…маленько</ta>
            <ta e="T23" id="Seg_2204" s="T12">девушке девчуше скажи, вот посылку принес пришел это за столом сижу</ta>
            <ta e="T31" id="Seg_2205" s="T23">Муж: девушку вижу на жену посмотрел как все учат какие языки</ta>
            <ta e="T37" id="Seg_2206" s="T31">Муж: любой язык, все таки ребенок научила</ta>
            <ta e="T51" id="Seg_2207" s="T37">Муж: моя эта как говорят у нас живет девушка говорят да</ta>
            <ta e="T53" id="Seg_2208" s="T51">Муж: сожительница да?</ta>
            <ta e="T60" id="Seg_2209" s="T57">Муж: по квартире ну</ta>
            <ta e="T72" id="Seg_2210" s="T60">Муж: она обрадовалась когда я привез, эта бабушка тоже которая отправила c хорошими пожеланиями</ta>
            <ta e="T82" id="Seg_2211" s="T72">Муж: что ещё сказать, по прилёту ещё раз увидимся</ta>
            <ta e="T92" id="Seg_2212" s="T82">Муж: потом желаю этой девушке хорошего парня найти, умного немного</ta>
            <ta e="T97" id="Seg_2213" s="T92">Муж: вот так удачно (светло) успешно доехать</ta>
            <ta e="T108" id="Seg_2214" s="T97">Муж: солнце светит это хорошо сама вот разговаривает сидит хорошо</ta>
            <ta e="T119" id="Seg_2215" s="T108">Муж: человек человека встречает, много людей языков смешала национальностей (инностранцев??) много</ta>
            <ta e="T120" id="Seg_2216" s="T119">Муж: планета</ta>
            <ta e="T124" id="Seg_2217" s="T120">Муж: вот так сказать</ta>
            <ta e="T138" id="Seg_2218" s="T124">Муж: как сказть раньше много иностранцев было когда я учился</ta>
            <ta e="T142" id="Seg_2219" s="T138">Муж: учился когда сейчас ещё много становиться</ta>
            <ta e="T157" id="Seg_2220" s="T142">МУж: сейчас слишком не говорю сейчас ещё находят до сих пор людей, где то на островах </ta>
            <ta e="T161" id="Seg_2221" s="T157">Муж: человек теряется люди находятся</ta>
            <ta e="T177" id="Seg_2222" s="T172">Муж: что спеть то?</ta>
         </annotation>
         <annotation name="nt" tierref="nt-AnSP" />
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-SE"
                      id="tx-SE"
                      speaker="SE"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-SE">
            <ts e="T185" id="Seg_2223" n="sc" s="T180">
               <ts e="T185" id="Seg_2225" n="HIAT:u" s="T180">
                  <ts e="T181" id="Seg_2227" n="HIAT:w" s="T180">Nu</ts>
                  <nts id="Seg_2228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_2230" n="HIAT:w" s="T181">o</ts>
                  <nts id="Seg_2231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_2233" n="HIAT:w" s="T182">davajte</ts>
                  <nts id="Seg_2234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_2236" n="HIAT:w" s="T183">eščjo</ts>
                  <nts id="Seg_2237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_2239" n="HIAT:w" s="T184">lučše</ts>
                  <nts id="Seg_2240" n="HIAT:ip">.</nts>
                  <nts id="Seg_2241" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-SE">
            <ts e="T185" id="Seg_2242" n="sc" s="T180">
               <ts e="T181" id="Seg_2244" n="e" s="T180">Nu </ts>
               <ts e="T182" id="Seg_2246" n="e" s="T181">o </ts>
               <ts e="T183" id="Seg_2248" n="e" s="T182">davajte </ts>
               <ts e="T184" id="Seg_2250" n="e" s="T183">eščjo </ts>
               <ts e="T185" id="Seg_2252" n="e" s="T184">lučše. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-SE">
            <ta e="T185" id="Seg_2253" s="T180">AnSP_AnIM_2009_Girl_nar.SE.001 (024.002)</ta>
         </annotation>
         <annotation name="st" tierref="st-SE">
            <ta e="T185" id="Seg_2254" s="T180">Стаперт: ну о давайте ещё лучше.</ta>
         </annotation>
         <annotation name="ts" tierref="ts-SE">
            <ta e="T185" id="Seg_2255" s="T180">Nu o davajte eščjo lučše. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-SE" />
         <annotation name="mp" tierref="mp-SE" />
         <annotation name="ge" tierref="ge-SE" />
         <annotation name="gg" tierref="gg-SE" />
         <annotation name="gr" tierref="gr-SE" />
         <annotation name="mc" tierref="mc-SE" />
         <annotation name="ps" tierref="ps-SE" />
         <annotation name="SeR" tierref="SeR-SE" />
         <annotation name="SyF" tierref="SyF-SE" />
         <annotation name="IST" tierref="IST-SE" />
         <annotation name="Top" tierref="Top-SE" />
         <annotation name="Foc" tierref="Foc-SE" />
         <annotation name="BOR" tierref="BOR-SE" />
         <annotation name="BOR-Phon" tierref="BOR-Phon-SE" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-SE" />
         <annotation name="CS" tierref="CS-SE" />
         <annotation name="fe" tierref="fe-SE">
            <ta e="T185" id="Seg_2256" s="T180">Well go ahead, that's even better.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-SE">
            <ta e="T185" id="Seg_2257" s="T180">Nun, mach weiter, das ist noch besser.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-SE" />
         <annotation name="ltr" tierref="ltr-SE">
            <ta e="T185" id="Seg_2258" s="T180">Стаперт: ну о давайте ещё лучше.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-SE" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-AnIM"
                          name="ref"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-AnIM"
                          name="st"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-AnIM"
                          name="ts"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-AnIM"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-AnIM"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-AnIM"
                          name="mb"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-AnIM"
                          name="mp"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-AnIM"
                          name="ge"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-AnIM"
                          name="gg"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-AnIM"
                          name="gr"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-AnIM"
                          name="mc"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-AnIM"
                          name="ps"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-AnIM"
                          name="SeR"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-AnIM"
                          name="SyF"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-AnIM"
                          name="IST"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-AnIM"
                          name="Top"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-AnIM"
                          name="Foc"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-AnIM"
                          name="BOR"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-AnIM"
                          name="BOR-Phon"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-AnIM"
                          name="BOR-Morph"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-AnIM"
                          name="CS"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-AnIM"
                          name="fe"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-AnIM"
                          name="fg"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-AnIM"
                          name="fr"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-AnIM"
                          name="ltr"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-AnIM"
                          name="nt"
                          segmented-tier-id="tx-AnIM"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-AnSP"
                          name="ref"
                          segmented-tier-id="tx-AnSP"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-AnSP"
                          name="st"
                          segmented-tier-id="tx-AnSP"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-AnSP"
                          name="ts"
                          segmented-tier-id="tx-AnSP"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-AnSP"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-AnSP"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-AnSP"
                          name="mb"
                          segmented-tier-id="tx-AnSP"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-AnSP"
                          name="mp"
                          segmented-tier-id="tx-AnSP"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-AnSP"
                          name="ge"
                          segmented-tier-id="tx-AnSP"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-AnSP"
                          name="gg"
                          segmented-tier-id="tx-AnSP"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-AnSP"
                          name="gr"
                          segmented-tier-id="tx-AnSP"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-AnSP"
                          name="mc"
                          segmented-tier-id="tx-AnSP"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-AnSP"
                          name="ps"
                          segmented-tier-id="tx-AnSP"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-AnSP"
                          name="SeR"
                          segmented-tier-id="tx-AnSP"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-AnSP"
                          name="SyF"
                          segmented-tier-id="tx-AnSP"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-AnSP"
                          name="IST"
                          segmented-tier-id="tx-AnSP"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-AnSP"
                          name="Top"
                          segmented-tier-id="tx-AnSP"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-AnSP"
                          name="Foc"
                          segmented-tier-id="tx-AnSP"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-AnSP"
                          name="BOR"
                          segmented-tier-id="tx-AnSP"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-AnSP"
                          name="BOR-Phon"
                          segmented-tier-id="tx-AnSP"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-AnSP"
                          name="BOR-Morph"
                          segmented-tier-id="tx-AnSP"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-AnSP"
                          name="CS"
                          segmented-tier-id="tx-AnSP"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-AnSP"
                          name="fe"
                          segmented-tier-id="tx-AnSP"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-AnSP"
                          name="fg"
                          segmented-tier-id="tx-AnSP"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-AnSP"
                          name="fr"
                          segmented-tier-id="tx-AnSP"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-AnSP"
                          name="ltr"
                          segmented-tier-id="tx-AnSP"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-AnSP"
                          name="nt"
                          segmented-tier-id="tx-AnSP"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-SE"
                          name="ref"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-SE"
                          name="st"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-SE"
                          name="ts"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-SE"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-SE"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-SE"
                          name="mb"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-SE"
                          name="mp"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-SE"
                          name="ge"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-SE"
                          name="gg"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-SE"
                          name="gr"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-SE"
                          name="mc"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-SE"
                          name="ps"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-SE"
                          name="SeR"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-SE"
                          name="SyF"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-SE"
                          name="IST"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-SE"
                          name="Top"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-SE"
                          name="Foc"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-SE"
                          name="BOR"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-SE"
                          name="BOR-Phon"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-SE"
                          name="BOR-Morph"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-SE"
                          name="CS"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-SE"
                          name="fe"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-SE"
                          name="fg"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-SE"
                          name="fr"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-SE"
                          name="ltr"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-SE"
                          name="nt"
                          segmented-tier-id="tx-SE"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
