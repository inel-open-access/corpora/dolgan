<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDE88D0B6F-70EA-008C-FDA1-CCF16897ED68">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>PoNA_19900810_TripToVolochanka_nar</transcription-name>
         <referenced-file url="PoNA_19900810_TripToVolochanka_nar.wav" />
         <referenced-file url="PoNA_19900810_TripToVolochanka_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\PoNA_19900810_TripToVolochanka_nar\PoNA_19900810_TripToVolochanka_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1232</ud-information>
            <ud-information attribute-name="# HIAT:w">915</ud-information>
            <ud-information attribute-name="# e">918</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">3</ud-information>
            <ud-information attribute-name="# HIAT:u">142</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="PoNA">
            <abbreviation>PoNA</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="1.197" type="appl" />
         <tli id="T2" time="2.394" type="appl" />
         <tli id="T3" time="4.299994386315292" />
         <tli id="T4" time="4.314" type="appl" />
         <tli id="T5" time="5.036" type="appl" />
         <tli id="T6" time="5.759" type="appl" />
         <tli id="T7" time="6.481" type="appl" />
         <tli id="T8" time="7.204" type="appl" />
         <tli id="T9" time="7.926" type="appl" />
         <tli id="T10" time="8.775666440417085" />
         <tli id="T11" time="10.139" type="appl" />
         <tli id="T12" time="11.629" type="appl" />
         <tli id="T13" time="13.119" type="appl" />
         <tli id="T14" time="13.82" type="appl" />
         <tli id="T15" time="14.52" type="appl" />
         <tli id="T16" time="15.221" type="appl" />
         <tli id="T17" time="15.922" type="appl" />
         <tli id="T18" time="16.622" type="appl" />
         <tli id="T19" time="17.456333004857438" />
         <tli id="T20" time="18.192" type="appl" />
         <tli id="T21" time="19.061" type="appl" />
         <tli id="T22" time="19.931" type="appl" />
         <tli id="T23" time="20.8" type="appl" />
         <tli id="T24" time="21.669" type="appl" />
         <tli id="T25" time="22.538" type="appl" />
         <tli id="T26" time="23.408" type="appl" />
         <tli id="T27" time="24.277" type="appl" />
         <tli id="T28" time="25.192667306039045" />
         <tli id="T29" time="26.155" type="appl" />
         <tli id="T30" time="27.164" type="appl" />
         <tli id="T31" time="28.172" type="appl" />
         <tli id="T32" time="29.181" type="appl" />
         <tli id="T33" time="30.19" type="appl" />
         <tli id="T34" time="31.913291670219067" />
         <tli id="T35" time="31.925" type="appl" />
         <tli id="T36" time="32.651" type="appl" />
         <tli id="T37" time="33.377" type="appl" />
         <tli id="T38" time="34.104" type="appl" />
         <tli id="T39" time="34.83" type="appl" />
         <tli id="T40" time="35.556" type="appl" />
         <tli id="T41" time="36.282" type="appl" />
         <tli id="T42" time="37.167998351814795" />
         <tli id="T43" time="37.686" type="appl" />
         <tli id="T44" time="38.364" type="appl" />
         <tli id="T45" time="39.042" type="appl" />
         <tli id="T46" time="39.719" type="appl" />
         <tli id="T47" time="40.397" type="appl" />
         <tli id="T48" time="41.15499835508064" />
         <tli id="T49" time="42.517" type="appl" />
         <tli id="T50" time="43.959" type="appl" />
         <tli id="T51" time="45.402" type="appl" />
         <tli id="T52" time="46.844" type="appl" />
         <tli id="T53" time="48.192668203792195" />
         <tli id="T54" time="48.998" type="appl" />
         <tli id="T55" time="49.71" type="appl" />
         <tli id="T56" time="50.422" type="appl" />
         <tli id="T57" time="51.135" type="appl" />
         <tli id="T58" time="51.847" type="appl" />
         <tli id="T59" time="52.559" type="appl" />
         <tli id="T60" time="53.464334498663646" />
         <tli id="T61" time="54.003" type="appl" />
         <tli id="T62" time="54.736" type="appl" />
         <tli id="T63" time="55.468" type="appl" />
         <tli id="T64" time="56.201" type="appl" />
         <tli id="T65" time="56.933" type="appl" />
         <tli id="T66" time="57.666" type="appl" />
         <tli id="T67" time="58.398" type="appl" />
         <tli id="T68" time="59.13" type="appl" />
         <tli id="T69" time="59.863" type="appl" />
         <tli id="T70" time="60.595" type="appl" />
         <tli id="T71" time="61.328" type="appl" />
         <tli id="T72" time="62.346663397488086" />
         <tli id="T73" time="62.951" type="appl" />
         <tli id="T74" time="63.842" type="appl" />
         <tli id="T75" time="64.732" type="appl" />
         <tli id="T76" time="65.83633332085655" />
         <tli id="T77" time="66.29" type="appl" />
         <tli id="T78" time="66.956" type="appl" />
         <tli id="T79" time="67.623" type="appl" />
         <tli id="T80" time="68.289" type="appl" />
         <tli id="T81" time="68.956" type="appl" />
         <tli id="T82" time="69.623" type="appl" />
         <tli id="T83" time="70.289" type="appl" />
         <tli id="T84" time="70.956" type="appl" />
         <tli id="T85" time="71.622" type="appl" />
         <tli id="T86" time="72.289" type="appl" />
         <tli id="T87" time="72.956" type="appl" />
         <tli id="T88" time="73.622" type="appl" />
         <tli id="T89" time="74.289" type="appl" />
         <tli id="T90" time="74.955" type="appl" />
         <tli id="T91" time="77.25989913644638" />
         <tli id="T92" time="77.47193275763092" type="intp" />
         <tli id="T93" time="77.68396637881546" type="intp" />
         <tli id="T94" time="77.896" type="appl" />
         <tli id="T95" time="78.654" type="appl" />
         <tli id="T96" time="79.411" type="appl" />
         <tli id="T97" time="80.169" type="appl" />
         <tli id="T98" time="80.927" type="appl" />
         <tli id="T99" time="81.685" type="appl" />
         <tli id="T100" time="82.6563308934353" />
         <tli id="T101" time="83.097" type="appl" />
         <tli id="T102" time="83.75" type="appl" />
         <tli id="T103" time="84.404" type="appl" />
         <tli id="T104" time="85.058" type="appl" />
         <tli id="T105" time="85.712" type="appl" />
         <tli id="T106" time="86.365" type="appl" />
         <tli id="T107" time="87.019" type="appl" />
         <tli id="T108" time="87.673" type="appl" />
         <tli id="T109" time="88.326" type="appl" />
         <tli id="T110" time="88.98" type="appl" />
         <tli id="T111" time="89.948" type="appl" />
         <tli id="T112" time="90.915" type="appl" />
         <tli id="T113" time="91.882" type="appl" />
         <tli id="T114" time="92.85" type="appl" />
         <tli id="T115" time="93.818" type="appl" />
         <tli id="T116" time="95.54654192972211" />
         <tli id="T117" time="95.799" type="appl" />
         <tli id="T118" time="96.812" type="appl" />
         <tli id="T119" time="97.826" type="appl" />
         <tli id="T120" time="98.839" type="appl" />
         <tli id="T121" time="99.853" type="appl" />
         <tli id="T122" time="100.866" type="appl" />
         <tli id="T123" time="102.00666370430329" />
         <tli id="T124" time="102.883" type="appl" />
         <tli id="T125" time="103.886" type="appl" />
         <tli id="T126" time="104.89" type="appl" />
         <tli id="T127" time="105.893" type="appl" />
         <tli id="T128" time="106.896" type="appl" />
         <tli id="T129" time="107.899" type="appl" />
         <tli id="T130" time="108.902" type="appl" />
         <tli id="T131" time="109.906" type="appl" />
         <tli id="T132" time="110.909" type="appl" />
         <tli id="T133" time="111.93866896714" />
         <tli id="T134" time="112.514" type="appl" />
         <tli id="T135" time="113.116" type="appl" />
         <tli id="T136" time="113.718" type="appl" />
         <tli id="T137" time="114.32" type="appl" />
         <tli id="T138" time="115.179" type="appl" />
         <tli id="T139" time="116.038" type="appl" />
         <tli id="T140" time="117.46651331298516" />
         <tli id="T141" time="117.633" type="appl" />
         <tli id="T142" time="118.37" type="appl" />
         <tli id="T143" time="119.106" type="appl" />
         <tli id="T144" time="119.842" type="appl" />
         <tli id="T145" time="120.579" type="appl" />
         <tli id="T146" time="121.58166419061547" />
         <tli id="T147" time="122.407" type="appl" />
         <tli id="T148" time="123.499" type="appl" />
         <tli id="T149" time="124.591" type="appl" />
         <tli id="T150" time="125.683" type="appl" />
         <tli id="T151" time="126.775" type="appl" />
         <tli id="T152" time="128.7798318766705" />
         <tli id="T153" time="128.994" type="appl" />
         <tli id="T154" time="130.05999947623255" />
         <tli id="T155" time="130.72" type="appl" />
         <tli id="T156" time="131.32" type="appl" />
         <tli id="T157" time="131.9733303116938" />
         <tli id="T158" time="132.517" type="appl" />
         <tli id="T159" time="133.114" type="appl" />
         <tli id="T160" time="133.711" type="appl" />
         <tli id="T161" time="134.308" type="appl" />
         <tli id="T162" time="134.906" type="appl" />
         <tli id="T163" time="135.503" type="appl" />
         <tli id="T164" time="136.1" type="appl" />
         <tli id="T165" time="136.697" type="appl" />
         <tli id="T166" time="138.3531527119399" />
         <tli id="T167" time="138.356" type="appl" />
         <tli id="T168" time="139.417" type="appl" />
         <tli id="T169" time="140.478" type="appl" />
         <tli id="T170" time="141.49333090407325" />
         <tli id="T171" time="142.564" type="appl" />
         <tli id="T172" time="143.589" type="appl" />
         <tli id="T173" time="144.613" type="appl" />
         <tli id="T174" time="145.638" type="appl" />
         <tli id="T175" time="146.662" type="appl" />
         <tli id="T176" time="147.687" type="appl" />
         <tli id="T177" time="148.711" type="appl" />
         <tli id="T178" time="149.736" type="appl" />
         <tli id="T179" time="151.09999804997537" />
         <tli id="T180" time="151.44" type="appl" />
         <tli id="T181" time="152.12" type="appl" />
         <tli id="T182" time="152.73332925097887" />
         <tli id="T183" time="153.413" type="appl" />
         <tli id="T184" time="154.025" type="appl" />
         <tli id="T185" time="154.638" type="appl" />
         <tli id="T186" time="155.4100054440519" />
         <tli id="T187" time="155.982" type="appl" />
         <tli id="T188" time="156.714" type="appl" />
         <tli id="T189" time="157.447" type="appl" />
         <tli id="T190" time="158.39899893772503" />
         <tli id="T191" time="159.002" type="appl" />
         <tli id="T192" time="159.824" type="appl" />
         <tli id="T193" time="160.647" type="appl" />
         <tli id="T194" time="161.4099976110035" />
         <tli id="T195" time="162.02" type="appl" />
         <tli id="T196" time="162.569" type="appl" />
         <tli id="T197" time="163.119" type="appl" />
         <tli id="T198" time="163.669" type="appl" />
         <tli id="T199" time="164.219" type="appl" />
         <tli id="T200" time="164.768" type="appl" />
         <tli id="T201" time="166.51311594883262" />
         <tli id="T202" time="166.525" type="appl" />
         <tli id="T203" time="167.731" type="appl" />
         <tli id="T204" time="168.938" type="appl" />
         <tli id="T205" time="170.144" type="appl" />
         <tli id="T206" time="171.35" type="appl" />
         <tli id="T207" time="172.557" type="appl" />
         <tli id="T208" time="173.764" type="appl" />
         <tli id="T209" time="175.1700056888451" />
         <tli id="T210" time="175.725" type="appl" />
         <tli id="T211" time="176.48" type="appl" />
         <tli id="T212" time="177.236" type="appl" />
         <tli id="T213" time="178.31767085037077" />
         <tli id="T214" time="178.654" type="appl" />
         <tli id="T215" time="179.318" type="appl" />
         <tli id="T216" time="179.981" type="appl" />
         <tli id="T217" time="180.644" type="appl" />
         <tli id="T218" time="181.307" type="appl" />
         <tli id="T219" time="181.971" type="appl" />
         <tli id="T220" time="182.634" type="appl" />
         <tli id="T221" time="183.6636664752879" />
         <tli id="T222" time="183.965" type="appl" />
         <tli id="T223" time="184.632" type="appl" />
         <tli id="T224" time="185.3" type="appl" />
         <tli id="T225" time="185.968" type="appl" />
         <tli id="T226" time="186.635" type="appl" />
         <tli id="T227" time="187.303" type="appl" />
         <tli id="T228" time="187.97" type="appl" />
         <tli id="T229" time="188.65799589264205" />
         <tli id="T230" time="189.294" type="appl" />
         <tli id="T231" time="189.949" type="appl" />
         <tli id="T232" time="190.604" type="appl" />
         <tli id="T233" time="192.03308263226663" />
         <tli id="T234" time="192.815" type="appl" />
         <tli id="T235" time="194.37" type="appl" />
         <tli id="T236" time="195.924" type="appl" />
         <tli id="T237" time="197.479" type="appl" />
         <tli id="T238" time="199.034" type="appl" />
         <tli id="T239" time="200.99566207736487" />
         <tli id="T240" time="201.396" type="appl" />
         <tli id="T241" time="202.203" type="appl" />
         <tli id="T242" time="203.009" type="appl" />
         <tli id="T243" time="203.816" type="appl" />
         <tli id="T244" time="204.623" type="appl" />
         <tli id="T245" time="205.43" type="appl" />
         <tli id="T246" time="206.236" type="appl" />
         <tli id="T247" time="207.043" type="appl" />
         <tli id="T248" time="207.80333548154735" />
         <tli id="T249" time="208.694" type="appl" />
         <tli id="T250" time="209.539" type="appl" />
         <tli id="T251" time="210.383" type="appl" />
         <tli id="T252" time="211.228" type="appl" />
         <tli id="T253" time="212.072" type="appl" />
         <tli id="T254" time="212.917" type="appl" />
         <tli id="T255" time="213.761" type="appl" />
         <tli id="T256" time="214.606" type="appl" />
         <tli id="T257" time="215.54333839773122" />
         <tli id="T258" time="216.198" type="appl" />
         <tli id="T259" time="216.946" type="appl" />
         <tli id="T260" time="217.694" type="appl" />
         <tli id="T261" time="218.442" type="appl" />
         <tli id="T262" time="219.38333338458023" />
         <tli id="T263" time="219.93" type="appl" />
         <tli id="T264" time="220.67" type="appl" />
         <tli id="T265" time="221.41" type="appl" />
         <tli id="T266" time="222.15" type="appl" />
         <tli id="T267" time="222.89" type="appl" />
         <tli id="T268" time="223.63" type="appl" />
         <tli id="T269" time="224.37" type="appl" />
         <tli id="T270" time="225.167" type="appl" />
         <tli id="T271" time="225.964" type="appl" />
         <tli id="T272" time="226.76" type="appl" />
         <tli id="T273" time="227.557" type="appl" />
         <tli id="T274" time="228.6939983127495" />
         <tli id="T275" time="229.164" type="appl" />
         <tli id="T276" time="229.975" type="appl" />
         <tli id="T277" time="230.786" type="appl" />
         <tli id="T278" time="231.596" type="appl" />
         <tli id="T279" time="232.406" type="appl" />
         <tli id="T280" time="233.217" type="appl" />
         <tli id="T281" time="234.028" type="appl" />
         <tli id="T282" time="235.21800021226787" />
         <tli id="T283" time="235.628" type="appl" />
         <tli id="T284" time="236.419" type="appl" />
         <tli id="T285" time="237.209" type="appl" />
         <tli id="T286" time="238.0" type="appl" />
         <tli id="T287" time="238.79" type="appl" />
         <tli id="T288" time="239.27" type="appl" />
         <tli id="T289" time="239.849999373481" />
         <tli id="T290" time="240.413" type="appl" />
         <tli id="T291" time="241.075" type="appl" />
         <tli id="T292" time="242.2046577577848" />
         <tli id="T293" time="242.538" type="appl" />
         <tli id="T294" time="243.338" type="appl" />
         <tli id="T295" time="244.138" type="appl" />
         <tli id="T296" time="244.938" type="appl" />
         <tli id="T297" time="245.738" type="appl" />
         <tli id="T298" time="246.538" type="appl" />
         <tli id="T299" time="247.337" type="appl" />
         <tli id="T300" time="248.137" type="appl" />
         <tli id="T301" time="248.937" type="appl" />
         <tli id="T302" time="249.737" type="appl" />
         <tli id="T303" time="250.537" type="appl" />
         <tli id="T304" time="253.886335215574" />
         <tli id="T305" time="254.07155681038267" type="intp" />
         <tli id="T306" time="254.25677840519134" type="intp" />
         <tli id="T307" time="254.442" type="appl" />
         <tli id="T308" time="255.478" type="appl" />
         <tli id="T309" time="256.513" type="appl" />
         <tli id="T310" time="257.548" type="appl" />
         <tli id="T311" time="258.72966222589656" />
         <tli id="T312" time="259.524" type="appl" />
         <tli id="T313" time="260.464" type="appl" />
         <tli id="T314" time="261.405" type="appl" />
         <tli id="T315" time="262.346" type="appl" />
         <tli id="T316" time="263.287" type="appl" />
         <tli id="T317" time="264.227" type="appl" />
         <tli id="T318" time="265.168" type="appl" />
         <tli id="T319" time="266.109" type="appl" />
         <tli id="T320" time="267.049" type="appl" />
         <tli id="T321" time="268.3566548660969" />
         <tli id="T322" time="269.004" type="appl" />
         <tli id="T323" time="270.018" type="appl" />
         <tli id="T324" time="271.032" type="appl" />
         <tli id="T325" time="272.045" type="appl" />
         <tli id="T326" time="273.059" type="appl" />
         <tli id="T327" time="274.18633475539286" />
         <tli id="T328" time="274.743" type="appl" />
         <tli id="T329" time="275.412" type="appl" />
         <tli id="T330" time="276.082" type="appl" />
         <tli id="T331" time="276.752" type="appl" />
         <tli id="T332" time="277.422" type="appl" />
         <tli id="T333" time="278.091" type="appl" />
         <tli id="T334" time="279.3529686353018" />
         <tli id="T335" time="279.633" type="appl" />
         <tli id="T336" time="280.506" type="appl" />
         <tli id="T337" time="281.378" type="appl" />
         <tli id="T338" time="282.61666229118447" />
         <tli id="T339" time="282.908" type="appl" />
         <tli id="T340" time="283.566" type="appl" />
         <tli id="T341" time="284.224" type="appl" />
         <tli id="T342" time="284.882" type="appl" />
         <tli id="T343" time="285.54" type="appl" />
         <tli id="T344" time="286.198" type="appl" />
         <tli id="T345" time="286.856" type="appl" />
         <tli id="T346" time="287.514" type="appl" />
         <tli id="T347" time="288.181" type="appl" />
         <tli id="T348" time="288.848" type="appl" />
         <tli id="T349" time="289.516" type="appl" />
         <tli id="T350" time="290.183" type="appl" />
         <tli id="T351" time="291.90628558015715" />
         <tli id="T352" time="291.974" type="appl" />
         <tli id="T353" time="293.098" type="appl" />
         <tli id="T354" time="294.222" type="appl" />
         <tli id="T355" time="295.346" type="appl" />
         <tli id="T356" time="296.47" type="appl" />
         <tli id="T357" time="297.594" type="appl" />
         <tli id="T358" time="298.718" type="appl" />
         <tli id="T359" time="299.842" type="appl" />
         <tli id="T360" time="300.83932079299234" />
         <tli id="T361" time="301.648" type="appl" />
         <tli id="T362" time="302.33" type="appl" />
         <tli id="T363" time="303.013" type="appl" />
         <tli id="T364" time="303.695" type="appl" />
         <tli id="T365" time="304.68366473249705" />
         <tli id="T366" time="305.406" type="appl" />
         <tli id="T367" time="306.436" type="appl" />
         <tli id="T368" time="307.465" type="appl" />
         <tli id="T369" time="308.494" type="appl" />
         <tli id="T370" time="309.524" type="appl" />
         <tli id="T371" time="310.9196722163283" />
         <tli id="T372" time="311.219" type="appl" />
         <tli id="T373" time="311.885" type="appl" />
         <tli id="T375" time="313.218" type="appl" />
         <tli id="T376" time="313.884" type="appl" />
         <tli id="T377" time="314.51000607077907" />
         <tli id="T378" time="315.731" type="appl" />
         <tli id="T379" time="316.912" type="appl" />
         <tli id="T380" time="318.094" type="appl" />
         <tli id="T381" time="320.01958221130684" />
         <tli id="T382" time="320.378" type="appl" />
         <tli id="T383" time="321.481" type="appl" />
         <tli id="T384" time="322.584" type="appl" />
         <tli id="T385" time="323.687" type="appl" />
         <tli id="T386" time="324.79" type="appl" />
         <tli id="T387" time="325.893" type="appl" />
         <tli id="T388" time="326.996" type="appl" />
         <tli id="T389" time="328.099" type="appl" />
         <tli id="T390" time="329.52865833878167" />
         <tli id="T391" time="330.189" type="appl" />
         <tli id="T392" time="331.176" type="appl" />
         <tli id="T393" time="332.164" type="appl" />
         <tli id="T394" time="333.151" type="appl" />
         <tli id="T395" time="334.07800135790325" />
         <tli id="T396" time="335.513" type="appl" />
         <tli id="T397" time="336.888" type="appl" />
         <tli id="T398" time="338.264" type="appl" />
         <tli id="T399" time="339.639" type="appl" />
         <tli id="T400" time="341.014" type="appl" />
         <tli id="T401" time="342.39" type="appl" />
         <tli id="T402" time="343.765" type="appl" />
         <tli id="T403" time="345.14" type="appl" />
         <tli id="T404" time="346.469" type="appl" />
         <tli id="T405" time="347.797" type="appl" />
         <tli id="T406" time="349.50600204983084" />
         <tli id="T407" time="350.107" type="appl" />
         <tli id="T408" time="351.087" type="appl" />
         <tli id="T409" time="352.068" type="appl" />
         <tli id="T410" time="353.048" type="appl" />
         <tli id="T411" time="354.029" type="appl" />
         <tli id="T412" time="355.01" type="appl" />
         <tli id="T413" time="355.99" type="appl" />
         <tli id="T414" time="356.83765914494745" />
         <tli id="T415" time="357.738" type="appl" />
         <tli id="T416" time="358.506" type="appl" />
         <tli id="T417" time="359.273" type="appl" />
         <tli id="T418" time="360.041" type="appl" />
         <tli id="T419" time="360.808" type="appl" />
         <tli id="T420" time="361.575" type="appl" />
         <tli id="T421" time="362.343" type="appl" />
         <tli id="T422" time="363.11" type="appl" />
         <tli id="T423" time="363.878" type="appl" />
         <tli id="T424" time="365.2183252872392" />
         <tli id="T425" time="365.288" type="appl" />
         <tli id="T426" time="365.931" type="appl" />
         <tli id="T428" time="367.217" type="appl" />
         <tli id="T429" time="367.861" type="appl" />
         <tli id="T430" time="368.504" type="appl" />
         <tli id="T431" time="369.147" type="appl" />
         <tli id="T432" time="369.79" type="appl" />
         <tli id="T433" time="370.586339112578" />
         <tli id="T434" time="371.177" type="appl" />
         <tli id="T435" time="371.922" type="appl" />
         <tli id="T436" time="372.666" type="appl" />
         <tli id="T437" time="373.42334061717816" />
         <tli id="T438" time="374.181" type="appl" />
         <tli id="T439" time="374.952" type="appl" />
         <tli id="T440" time="375.722" type="appl" />
         <tli id="T441" time="376.493" type="appl" />
         <tli id="T442" time="378.09950638739804" />
         <tli id="T443" time="378.515" type="appl" />
         <tli id="T444" time="379.766" type="appl" />
         <tli id="T445" time="381.018" type="appl" />
         <tli id="T446" time="382.269" type="appl" />
         <tli id="T447" time="383.54666073441814" />
         <tli id="T448" time="384.393" type="appl" />
         <tli id="T449" time="385.265" type="appl" />
         <tli id="T450" time="386.138" type="appl" />
         <tli id="T451" time="387.011" type="appl" />
         <tli id="T452" time="387.883" type="appl" />
         <tli id="T453" time="388.9960025785824" />
         <tli id="T454" time="389.625" type="appl" />
         <tli id="T455" time="390.494" type="appl" />
         <tli id="T456" time="391.362" type="appl" />
         <tli id="T457" time="392.231" type="appl" />
         <tli id="T458" time="393.0600076896508" />
         <tli id="T459" time="394.244" type="appl" />
         <tli id="T460" time="395.388" type="appl" />
         <tli id="T461" time="396.531" type="appl" />
         <tli id="T462" time="398.45281314929036" />
         <tli id="T463" time="398.456" type="appl" />
         <tli id="T464" time="399.237" type="appl" />
         <tli id="T465" time="400.018" type="appl" />
         <tli id="T466" time="400.798" type="appl" />
         <tli id="T467" time="401.579" type="appl" />
         <tli id="T468" time="402.36" type="appl" />
         <tli id="T469" time="403.141" type="appl" />
         <tli id="T470" time="404.8661381099654" />
         <tli id="T471" time="404.877" type="appl" />
         <tli id="T472" time="405.833" type="appl" />
         <tli id="T473" time="406.788" type="appl" />
         <tli id="T474" time="407.744" type="appl" />
         <tli id="T475" time="408.699" type="appl" />
         <tli id="T476" time="409.655" type="appl" />
         <tli id="T477" time="411.03667692926655" />
         <tli id="T478" time="411.478" type="appl" />
         <tli id="T479" time="412.346" type="appl" />
         <tli id="T480" time="413.214" type="appl" />
         <tli id="T481" time="414.082" type="appl" />
         <tli id="T482" time="415.2100048142796" />
         <tli id="T483" time="415.856" type="appl" />
         <tli id="T484" time="416.761" type="appl" />
         <tli id="T485" time="417.667" type="appl" />
         <tli id="T486" time="418.572" type="appl" />
         <tli id="T487" time="419.478" type="appl" />
         <tli id="T488" time="420.383" type="appl" />
         <tli id="T489" time="422.08611562911636" />
         <tli id="T490" time="422.091" type="appl" />
         <tli id="T491" time="422.892" type="appl" />
         <tli id="T492" time="423.694" type="appl" />
         <tli id="T493" time="424.495" type="appl" />
         <tli id="T494" time="425.297" type="appl" />
         <tli id="T495" time="426.098" type="appl" />
         <tli id="T496" time="427.65944168641806" />
         <tli id="T497" time="427.778" type="appl" />
         <tli id="T498" time="428.655" type="appl" />
         <tli id="T499" time="429.533" type="appl" />
         <tli id="T500" time="430.41" type="appl" />
         <tli id="T501" time="431.9194361249536" />
         <tli id="T502" time="432.373" type="appl" />
         <tli id="T503" time="433.458" type="appl" />
         <tli id="T504" time="434.543" type="appl" />
         <tli id="T505" time="436.172763905526" />
         <tli id="T506" time="436.695" type="appl" />
         <tli id="T507" time="437.762" type="appl" />
         <tli id="T508" time="438.829" type="appl" />
         <tli id="T509" time="439.896" type="appl" />
         <tli id="T510" time="441.2030177551471" />
         <tli id="T511" time="441.979" type="appl" />
         <tli id="T512" time="442.995" type="appl" />
         <tli id="T513" time="444.011" type="appl" />
         <tli id="T514" time="445.027" type="appl" />
         <tli id="T515" time="446.043" type="appl" />
         <tli id="T516" time="447.059" type="appl" />
         <tli id="T517" time="448.075" type="appl" />
         <tli id="T518" time="448.95" type="appl" />
         <tli id="T519" time="449.825" type="appl" />
         <tli id="T520" time="450.7" type="appl" />
         <tli id="T521" time="451.726" type="appl" />
         <tli id="T522" time="452.753" type="appl" />
         <tli id="T523" time="453.78" type="appl" />
         <tli id="T524" time="455.586071894596" />
         <tli id="T525" time="455.679" type="appl" />
         <tli id="T526" time="456.552" type="appl" />
         <tli id="T527" time="457.425" type="appl" />
         <tli id="T528" time="458.297" type="appl" />
         <tli id="T529" time="459.17" type="appl" />
         <tli id="T530" time="460.043" type="appl" />
         <tli id="T531" time="461.23601243521006" />
         <tli id="T532" time="462.091" type="appl" />
         <tli id="T533" time="463.265" type="appl" />
         <tli id="T534" time="464.580018486244" />
         <tli id="T535" time="465.556" type="appl" />
         <tli id="T536" time="466.672" type="appl" />
         <tli id="T537" time="467.788" type="appl" />
         <tli id="T538" time="468.903" type="appl" />
         <tli id="T539" time="470.019" type="appl" />
         <tli id="T540" time="471.135" type="appl" />
         <tli id="T541" time="472.4993831474361" />
         <tli id="T542" time="473.147" type="appl" />
         <tli id="T543" time="474.044" type="appl" />
         <tli id="T544" time="474.94" type="appl" />
         <tli id="T545" time="475.836" type="appl" />
         <tli id="T546" time="476.732" type="appl" />
         <tli id="T547" time="477.629" type="appl" />
         <tli id="T548" time="478.525" type="appl" />
         <tli id="T549" time="479.189" type="appl" />
         <tli id="T550" time="479.854" type="appl" />
         <tli id="T551" time="480.518" type="appl" />
         <tli id="T552" time="481.182" type="appl" />
         <tli id="T553" time="481.847" type="appl" />
         <tli id="T554" time="482.511" type="appl" />
         <tli id="T555" time="483.176" type="appl" />
         <tli id="T556" time="483.88665994790153" />
         <tli id="T557" time="484.796" type="appl" />
         <tli id="T558" time="485.751" type="appl" />
         <tli id="T559" time="486.707" type="appl" />
         <tli id="T560" time="487.663" type="appl" />
         <tli id="T561" time="488.618" type="appl" />
         <tli id="T562" time="489.38732985005663" />
         <tli id="T563" time="490.286" type="appl" />
         <tli id="T564" time="490.998" type="appl" />
         <tli id="T565" time="491.8366495691124" />
         <tli id="T566" time="492.693" type="appl" />
         <tli id="T567" time="493.675" type="appl" />
         <tli id="T568" time="494.658" type="appl" />
         <tli id="T569" time="495.64" type="appl" />
         <tli id="T570" time="496.623" type="appl" />
         <tli id="T571" time="497.606" type="appl" />
         <tli id="T572" time="498.588" type="appl" />
         <tli id="T573" time="500.3326801441281" />
         <tli id="T574" time="500.334" type="appl" />
         <tli id="T575" time="501.097" type="appl" />
         <tli id="T576" time="501.86" type="appl" />
         <tli id="T577" time="502.623" type="appl" />
         <tli id="T578" time="503.387" type="appl" />
         <tli id="T579" time="504.15" type="appl" />
         <tli id="T580" time="504.913" type="appl" />
         <tli id="T581" time="505.676" type="appl" />
         <tli id="T582" time="506.7989737856519" />
         <tli id="T583" time="507.065" type="appl" />
         <tli id="T584" time="507.692" type="appl" />
         <tli id="T585" time="508.318" type="appl" />
         <tli id="T586" time="508.944" type="appl" />
         <tli id="T587" time="509.571" type="appl" />
         <tli id="T588" time="510.197" type="appl" />
         <tli id="T589" time="510.824" type="appl" />
         <tli id="T590" time="511.45" type="appl" />
         <tli id="T591" time="512.475" type="appl" />
         <tli id="T592" time="513.499" type="appl" />
         <tli id="T593" time="514.5706823896103" />
         <tli id="T594" time="515.24" type="appl" />
         <tli id="T595" time="515.957" type="appl" />
         <tli id="T596" time="516.674" type="appl" />
         <tli id="T597" time="517.39" type="appl" />
         <tli id="T598" time="518.364" type="appl" />
         <tli id="T599" time="519.337" type="appl" />
         <tli id="T600" time="520.311" type="appl" />
         <tli id="T601" time="521.285" type="appl" />
         <tli id="T602" time="522.258" type="appl" />
         <tli id="T603" time="523.8593160965414" />
         <tli id="T604" time="524.041" type="appl" />
         <tli id="T605" time="524.851" type="appl" />
         <tli id="T606" time="525.66" type="appl" />
         <tli id="T607" time="526.469" type="appl" />
         <tli id="T608" time="527.279" type="appl" />
         <tli id="T609" time="528.088" type="appl" />
         <tli id="T610" time="528.897" type="appl" />
         <tli id="T611" time="529.707" type="appl" />
         <tli id="T612" time="530.655973890105" />
         <tli id="T613" time="531.41" type="appl" />
         <tli id="T614" time="532.304" type="appl" />
         <tli id="T615" time="533.198" type="appl" />
         <tli id="T616" time="534.092" type="appl" />
         <tli id="T617" time="534.986" type="appl" />
         <tli id="T618" time="535.88" type="appl" />
         <tli id="T619" time="536.774" type="appl" />
         <tli id="T620" time="537.668" type="appl" />
         <tli id="T621" time="538.562" type="appl" />
         <tli id="T622" time="539.456" type="appl" />
         <tli id="T623" time="540.6833566325921" />
         <tli id="T624" time="541.049" type="appl" />
         <tli id="T625" time="541.747" type="appl" />
         <tli id="T626" time="542.446" type="appl" />
         <tli id="T627" time="543.145" type="appl" />
         <tli id="T628" time="543.843" type="appl" />
         <tli id="T629" time="544.7953304310367" />
         <tli id="T630" time="545.221" type="appl" />
         <tli id="T631" time="545.901" type="appl" />
         <tli id="T632" time="546.58" type="appl" />
         <tli id="T633" time="547.259" type="appl" />
         <tli id="T634" time="547.938" type="appl" />
         <tli id="T635" time="548.618" type="appl" />
         <tli id="T636" time="549.297" type="appl" />
         <tli id="T637" time="549.976" type="appl" />
         <tli id="T638" time="550.655" type="appl" />
         <tli id="T639" time="551.335" type="appl" />
         <tli id="T640" time="552.7659450253216" />
         <tli id="T641" time="552.772" type="appl" />
         <tli id="T642" time="553.53" type="appl" />
         <tli id="T643" time="554.288" type="appl" />
         <tli id="T644" time="555.046" type="appl" />
         <tli id="T645" time="555.803" type="appl" />
         <tli id="T646" time="556.561" type="appl" />
         <tli id="T647" time="557.319" type="appl" />
         <tli id="T648" time="558.163646311903" />
         <tli id="T649" time="558.811" type="appl" />
         <tli id="T650" time="559.544" type="appl" />
         <tli id="T651" time="560.278" type="appl" />
         <tli id="T652" time="561.012" type="appl" />
         <tli id="T653" time="561.745" type="appl" />
         <tli id="T654" time="562.479" type="appl" />
         <tli id="T655" time="563.213" type="appl" />
         <tli id="T656" time="563.947" type="appl" />
         <tli id="T657" time="564.68" type="appl" />
         <tli id="T658" time="565.414" type="appl" />
         <tli id="T659" time="566.148" type="appl" />
         <tli id="T660" time="566.881" type="appl" />
         <tli id="T661" time="567.5816548499341" />
         <tli id="T662" time="568.516" type="appl" />
         <tli id="T663" time="569.418" type="appl" />
         <tli id="T664" time="570.319" type="appl" />
         <tli id="T665" time="571.221" type="appl" />
         <tli id="T666" time="572.122" type="appl" />
         <tli id="T667" time="573.024" type="appl" />
         <tli id="T668" time="574.599249855062" />
         <tli id="T669" time="575.016" type="appl" />
         <tli id="T670" time="576.108" type="appl" />
         <tli id="T671" time="577.199" type="appl" />
         <tli id="T672" time="578.29" type="appl" />
         <tli id="T673" time="579.382" type="appl" />
         <tli id="T674" time="580.473" type="appl" />
         <tli id="T675" time="581.564" type="appl" />
         <tli id="T676" time="582.655" type="appl" />
         <tli id="T677" time="583.747" type="appl" />
         <tli id="T678" time="585.3246525196106" />
         <tli id="T679" time="585.798" type="appl" />
         <tli id="T680" time="586.759" type="appl" />
         <tli id="T681" time="587.719" type="appl" />
         <tli id="T682" time="588.679" type="appl" />
         <tli id="T683" time="589.64" type="appl" />
         <tli id="T684" time="590.5533436101699" />
         <tli id="T685" time="591.649" type="appl" />
         <tli id="T686" time="592.698" type="appl" />
         <tli id="T687" time="593.748" type="appl" />
         <tli id="T688" time="594.797" type="appl" />
         <tli id="T689" time="597.472553327074" />
         <tli id="T690" time="597.696276663537" type="intp" />
         <tli id="T691" time="597.92" type="appl" />
         <tli id="T692" time="598.957" type="appl" />
         <tli id="T693" time="599.993" type="appl" />
         <tli id="T694" time="601.03" type="appl" />
         <tli id="T695" time="602.067" type="appl" />
         <tli id="T696" time="603.104" type="appl" />
         <tli id="T697" time="604.587" type="appl" />
         <tli id="T698" time="606.069" type="appl" />
         <tli id="T699" time="607.552" type="appl" />
         <tli id="T700" time="609.034" type="appl" />
         <tli id="T701" time="610.4236822525819" />
         <tli id="T702" time="611.228" type="appl" />
         <tli id="T703" time="611.938" type="appl" />
         <tli id="T704" time="612.648" type="appl" />
         <tli id="T705" time="613.359" type="appl" />
         <tli id="T706" time="614.07" type="appl" />
         <tli id="T707" time="614.6999787531684" />
         <tli id="T708" time="615.29" type="appl" />
         <tli id="T709" time="615.8" type="appl" />
         <tli id="T710" time="616.31" type="appl" />
         <tli id="T711" time="616.82" type="appl" />
         <tli id="T712" time="617.33" type="appl" />
         <tli id="T713" time="617.84" type="appl" />
         <tli id="T714" time="618.3433073301006" />
         <tli id="T715" time="619.33" type="appl" />
         <tli id="T716" time="620.309" type="appl" />
         <tli id="T717" time="621.289" type="appl" />
         <tli id="T718" time="622.269" type="appl" />
         <tli id="T719" time="623.248" type="appl" />
         <tli id="T720" time="624.228" type="appl" />
         <tli id="T721" time="624.759" type="appl" />
         <tli id="T722" time="625.289" type="appl" />
         <tli id="T723" time="626.0600160058619" />
         <tli id="T724" time="626.625" type="appl" />
         <tli id="T725" time="627.43" type="appl" />
         <tli id="T726" time="628.235" type="appl" />
         <tli id="T727" time="629.04" type="appl" />
         <tli id="T728" time="629.845" type="appl" />
         <tli id="T729" time="630.65" type="appl" />
         <tli id="T730" time="631.455" type="appl" />
         <tli id="T731" time="632.26" type="appl" />
         <tli id="T732" time="633.065" type="appl" />
         <tli id="T733" time="633.87" type="appl" />
         <tli id="T734" time="634.675" type="appl" />
         <tli id="T735" time="635.334" type="appl" />
         <tli id="T736" time="635.994" type="appl" />
         <tli id="T737" time="636.653" type="appl" />
         <tli id="T738" time="637.312" type="appl" />
         <tli id="T739" time="637.972" type="appl" />
         <tli id="T741" time="639.291" type="appl" />
         <tli id="T742" time="639.95" type="appl" />
         <tli id="T743" time="640.633" type="appl" />
         <tli id="T744" time="641.316" type="appl" />
         <tli id="T745" time="642.8191607933013" />
         <tli id="T746" time="642.918" type="appl" />
         <tli id="T747" time="643.838" type="appl" />
         <tli id="T748" time="644.757" type="appl" />
         <tli id="T749" time="645.676" type="appl" />
         <tli id="T750" time="646.595" type="appl" />
         <tli id="T751" time="647.514" type="appl" />
         <tli id="T752" time="648.434" type="appl" />
         <tli id="T753" time="649.9858181038269" />
         <tli id="T754" time="650.312" type="appl" />
         <tli id="T755" time="651.271" type="appl" />
         <tli id="T756" time="652.23" type="appl" />
         <tli id="T757" time="653.094" type="appl" />
         <tli id="T758" time="653.958" type="appl" />
         <tli id="T759" time="654.822" type="appl" />
         <tli id="T760" time="655.686" type="appl" />
         <tli id="T761" time="656.55" type="appl" />
         <tli id="T762" time="657.566" type="appl" />
         <tli id="T763" time="658.583" type="appl" />
         <tli id="T764" time="659.599" type="appl" />
         <tli id="T765" time="660.615" type="appl" />
         <tli id="T766" time="661.632" type="appl" />
         <tli id="T767" time="662.648" type="appl" />
         <tli id="T768" time="663.664" type="appl" />
         <tli id="T769" time="664.681" type="appl" />
         <tli id="T770" time="665.7836620629204" />
         <tli id="T771" time="666.482" type="appl" />
         <tli id="T772" time="667.266" type="appl" />
         <tli id="T773" time="668.051" type="appl" />
         <tli id="T774" time="668.836" type="appl" />
         <tli id="T775" time="669.621" type="appl" />
         <tli id="T776" time="670.406" type="appl" />
         <tli id="T777" time="671.19" type="appl" />
         <tli id="T778" time="671.975" type="appl" />
         <tli id="T779" time="672.8" type="appl" />
         <tli id="T780" time="673.625" type="appl" />
         <tli id="T781" time="674.45" type="appl" />
         <tli id="T782" time="675.275" type="appl" />
         <tli id="T783" time="676.1" type="appl" />
         <tli id="T784" time="676.925" type="appl" />
         <tli id="T785" time="677.75" type="appl" />
         <tli id="T786" time="678.575" type="appl" />
         <tli id="T787" time="679.8866644846462" />
         <tli id="T788" time="680.271" type="appl" />
         <tli id="T789" time="681.142" type="appl" />
         <tli id="T790" time="682.013" type="appl" />
         <tli id="T791" time="682.884" type="appl" />
         <tli id="T792" time="683.755" type="appl" />
         <tli id="T793" time="684.626" type="appl" />
         <tli id="T794" time="685.497" type="appl" />
         <tli id="T795" time="686.368" type="appl" />
         <tli id="T796" time="687.6389981138989" />
         <tli id="T797" time="688.21" type="appl" />
         <tli id="T798" time="689.182" type="appl" />
         <tli id="T799" time="690.153" type="appl" />
         <tli id="T800" time="691.125" type="appl" />
         <tli id="T801" time="692.096" type="appl" />
         <tli id="T802" time="693.068" type="appl" />
         <tli id="T803" time="694.039" type="appl" />
         <tli id="T804" time="695.011" type="appl" />
         <tli id="T805" time="695.982" type="appl" />
         <tli id="T806" time="696.954" type="appl" />
         <tli id="T807" time="697.7916411095242" />
         <tli id="T808" time="698.659" type="appl" />
         <tli id="T809" time="699.393" type="appl" />
         <tli id="T810" time="700.128" type="appl" />
         <tli id="T811" time="700.862" type="appl" />
         <tli id="T812" time="701.596" type="appl" />
         <tli id="T813" time="702.423353812778" />
         <tli id="T814" time="702.763" type="appl" />
         <tli id="T815" time="703.196" type="appl" />
         <tli id="T817" time="704.061" type="appl" />
         <tli id="T818" time="704.494" type="appl" />
         <tli id="T819" time="704.927" type="appl" />
         <tli id="T820" time="705.3933499354189" />
         <tli id="T821" time="706.036" type="appl" />
         <tli id="T822" time="706.713" type="appl" />
         <tli id="T823" time="707.5623575204252" />
         <tli id="T824" time="708.087" type="appl" />
         <tli id="T825" time="708.785" type="appl" />
         <tli id="T826" time="709.483" type="appl" />
         <tli id="T827" time="710.181" type="appl" />
         <tli id="T828" time="710.879" type="appl" />
         <tli id="T829" time="711.577" type="appl" />
         <tli id="T830" time="712.555007252472" />
         <tli id="T831" time="713.073" type="appl" />
         <tli id="T832" time="713.872" type="appl" />
         <tli id="T833" time="714.67" type="appl" />
         <tli id="T834" time="715.468" type="appl" />
         <tli id="T835" time="716.267" type="appl" />
         <tli id="T836" time="717.065" type="appl" />
         <tli id="T837" time="717.864" type="appl" />
         <tli id="T838" time="718.662" type="appl" />
         <tli id="T839" time="719.41" type="appl" />
         <tli id="T840" time="720.157" type="appl" />
         <tli id="T841" time="720.905" type="appl" />
         <tli id="T842" time="721.652" type="appl" />
         <tli id="T843" time="722.4" type="appl" />
         <tli id="T844" time="723.062" type="appl" />
         <tli id="T845" time="723.725" type="appl" />
         <tli id="T846" time="724.387" type="appl" />
         <tli id="T847" time="724.9766577025525" />
         <tli id="T848" time="726.515" type="appl" />
         <tli id="T849" time="727.981" type="appl" />
         <tli id="T850" time="730.1857134020794" />
         <tli id="T851" time="730.243" type="appl" />
         <tli id="T852" time="731.039" type="appl" />
         <tli id="T853" time="731.836" type="appl" />
         <tli id="T854" time="732.633" type="appl" />
         <tli id="T855" time="733.429" type="appl" />
         <tli id="T856" time="734.226" type="appl" />
         <tli id="T857" time="735.023" type="appl" />
         <tli id="T858" time="735.819" type="appl" />
         <tli id="T859" time="736.616" type="appl" />
         <tli id="T860" time="737.633" type="appl" />
         <tli id="T861" time="738.651" type="appl" />
         <tli id="T862" time="739.668" type="appl" />
         <tli id="T863" time="740.686" type="appl" />
         <tli id="T864" time="741.703" type="appl" />
         <tli id="T865" time="742.721" type="appl" />
         <tli id="T866" time="744.1046535641003" />
         <tli id="T867" time="744.641" type="appl" />
         <tli id="T868" time="745.544" type="appl" />
         <tli id="T869" time="746.447" type="appl" />
         <tli id="T870" time="747.35" type="appl" />
         <tli id="T871" time="748.253" type="appl" />
         <tli id="T872" time="749.156" type="appl" />
         <tli id="T873" time="750.059" type="appl" />
         <tli id="T874" time="750.962" type="appl" />
         <tli id="T875" time="751.865" type="appl" />
         <tli id="T876" time="753.1213084594358" />
         <tli id="T877" time="753.556" type="appl" />
         <tli id="T878" time="754.344" type="appl" />
         <tli id="T879" time="755.131" type="appl" />
         <tli id="T880" time="755.919" type="appl" />
         <tli id="T881" time="756.707" type="appl" />
         <tli id="T882" time="757.494" type="appl" />
         <tli id="T883" time="758.282" type="appl" />
         <tli id="T884" time="759.583331273202" />
         <tli id="T885" time="759.787" type="appl" />
         <tli id="T886" time="760.503" type="appl" />
         <tli id="T887" time="761.1533292235542" />
         <tli id="T888" time="761.977" type="appl" />
         <tli id="T889" time="762.734" type="appl" />
         <tli id="T890" time="763.492" type="appl" />
         <tli id="T891" time="766.0056666387804" />
         <tli id="T892" time="766.1944444258536" type="intp" />
         <tli id="T893" time="766.3832222129267" type="intp" />
         <tli id="T894" time="766.572" type="appl" />
         <tli id="T895" time="767.346" type="appl" />
         <tli id="T896" time="768.12" type="appl" />
         <tli id="T897" time="768.895" type="appl" />
         <tli id="T898" time="769.669" type="appl" />
         <tli id="T899" time="770.443" type="appl" />
         <tli id="T900" time="771.217" type="appl" />
         <tli id="T901" time="771.992" type="appl" />
         <tli id="T902" time="772.766" type="appl" />
         <tli id="T903" time="773.54" type="appl" />
         <tli id="T904" time="774.314" type="appl" />
         <tli id="T905" time="775.089" type="appl" />
         <tli id="T906" time="776.5056529309455" />
         <tli id="T907" time="776.88" type="appl" />
         <tli id="T908" time="777.897" type="appl" />
         <tli id="T909" time="778.914" type="appl" />
         <tli id="T910" time="779.932" type="appl" />
         <tli id="T911" time="780.949" type="appl" />
         <tli id="T912" time="781.966" type="appl" />
         <tli id="T913" time="782.983" type="appl" />
         <tli id="T914" time="784.718975541706" />
         <tli id="T915" time="784.798" type="appl" />
         <tli id="T916" time="785.597" type="appl" />
         <tli id="T917" time="786.395" type="appl" />
         <tli id="T918" time="787.193" type="appl" />
         <tli id="T919" time="787.992" type="appl" />
         <tli id="T920" time="788.79" type="appl" />
         <tli id="T921" time="789.77" type="appl" />
         <tli id="T922" time="790.7976655225834" />
         <tli id="T374" time="1563.884" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="PoNA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T922" id="Seg_0" n="sc" s="T0">
               <ts e="T3" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Valačanka</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">di͡ek</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">köhüː</ts>
                  <nts id="Seg_11" n="HIAT:ip">.</nts>
                  <nts id="Seg_12" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T10" id="Seg_14" n="HIAT:u" s="T3">
                  <ts e="T4" id="Seg_16" n="HIAT:w" s="T3">ɨrgakta</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_19" n="HIAT:w" s="T4">ɨja</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">baranar</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">künnere</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">turbuttara</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">itiː</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">bagajɨ</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_38" n="HIAT:u" s="T10">
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">Ottor</ts>
                  <nts id="Seg_41" n="HIAT:ip">,</nts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">hebirdekter</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">kagdʼarɨjbɨttar</ts>
                  <nts id="Seg_48" n="HIAT:ip">.</nts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T19" id="Seg_51" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_53" n="HIAT:w" s="T13">Mu͡ora</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_56" n="HIAT:w" s="T14">onno-manna</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_59" n="HIAT:w" s="T15">kɨhɨl</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_62" n="HIAT:w" s="T16">kömüs</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">bu͡olan</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">ispit</ts>
                  <nts id="Seg_69" n="HIAT:ip">.</nts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_72" n="HIAT:u" s="T19">
                  <ts e="T20" id="Seg_74" n="HIAT:w" s="T19">Ol</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_77" n="HIAT:w" s="T20">kütterge</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_80" n="HIAT:w" s="T21">biliːleːk</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_83" n="HIAT:w" s="T22">ulakan</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_86" n="HIAT:w" s="T23">kü͡öl</ts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_89" n="HIAT:w" s="T24">kɨtɨlɨgar</ts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_92" n="HIAT:w" s="T25">ulakan</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_95" n="HIAT:w" s="T26">komuːnu</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_98" n="HIAT:w" s="T27">turbuta</ts>
                  <nts id="Seg_99" n="HIAT:ip">:</nts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T34" id="Seg_102" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_104" n="HIAT:w" s="T28">Ogo</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_107" n="HIAT:w" s="T29">aːjmagɨ</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_110" n="HIAT:w" s="T30">belenniːller</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_113" n="HIAT:w" s="T31">Valačankaga</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_116" n="HIAT:w" s="T32">illeːri</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_119" n="HIAT:w" s="T33">ü͡örekke</ts>
                  <nts id="Seg_120" n="HIAT:ip">.</nts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_123" n="HIAT:u" s="T34">
                  <ts e="T35" id="Seg_125" n="HIAT:w" s="T34">Mu͡ora</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_128" n="HIAT:w" s="T35">nehili͡ege</ts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_131" n="HIAT:w" s="T36">urut</ts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_134" n="HIAT:w" s="T37">bilbet</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_137" n="HIAT:w" s="T38">etilere</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_140" n="HIAT:w" s="T39">ogolorun</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_143" n="HIAT:w" s="T40">ü͡örekke</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_146" n="HIAT:w" s="T41">ɨːtarɨ</ts>
                  <nts id="Seg_147" n="HIAT:ip">.</nts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_150" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_152" n="HIAT:w" s="T42">Ol</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_155" n="HIAT:w" s="T43">ihin</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_158" n="HIAT:w" s="T44">hanaːlara</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_161" n="HIAT:w" s="T45">barbat</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_164" n="HIAT:w" s="T46">osku͡olaga</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_167" n="HIAT:w" s="T47">bi͡ererge</ts>
                  <nts id="Seg_168" n="HIAT:ip">.</nts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T53" id="Seg_171" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_173" n="HIAT:w" s="T48">Bütte</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_176" n="HIAT:w" s="T49">öhü</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_179" n="HIAT:w" s="T50">bulallar</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_182" n="HIAT:w" s="T51">ɨːtɨmaːrɨ</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_185" n="HIAT:w" s="T52">ogolorun</ts>
                  <nts id="Seg_186" n="HIAT:ip">.</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T60" id="Seg_189" n="HIAT:u" s="T53">
                  <ts e="T54" id="Seg_191" n="HIAT:w" s="T53">Radavojdarɨgar</ts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_194" n="HIAT:w" s="T54">ol</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_197" n="HIAT:w" s="T55">künnerge</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_200" n="HIAT:w" s="T56">olus</ts>
                  <nts id="Seg_201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_203" n="HIAT:w" s="T57">erejdeːk</ts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_206" n="HIAT:w" s="T58">üleni</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_209" n="HIAT:w" s="T59">oŋorbuttara</ts>
                  <nts id="Seg_210" n="HIAT:ip">.</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T72" id="Seg_213" n="HIAT:u" s="T60">
                  <ts e="T61" id="Seg_215" n="HIAT:w" s="T60">Barɨ</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_218" n="HIAT:w" s="T61">uraha</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_221" n="HIAT:w" s="T62">dʼi͡e</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_224" n="HIAT:w" s="T63">aːjɨ</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_227" n="HIAT:w" s="T64">hɨldʼan</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_230" n="HIAT:w" s="T65">kepsete</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_233" n="HIAT:w" s="T66">hataːbɨt</ts>
                  <nts id="Seg_234" n="HIAT:ip">,</nts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_237" n="HIAT:w" s="T67">ogolorun</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_240" n="HIAT:w" s="T68">usku͡olaga</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_243" n="HIAT:w" s="T69">bi͡eri͡ekterin</ts>
                  <nts id="Seg_244" n="HIAT:ip">,</nts>
                  <nts id="Seg_245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_247" n="HIAT:w" s="T70">ü͡örekke</ts>
                  <nts id="Seg_248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_250" n="HIAT:w" s="T71">ɨːtɨ͡aktarɨn</ts>
                  <nts id="Seg_251" n="HIAT:ip">.</nts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T76" id="Seg_254" n="HIAT:u" s="T72">
                  <ts e="T73" id="Seg_256" n="HIAT:w" s="T72">Itinten</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_259" n="HIAT:w" s="T73">tu͡ok</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_262" n="HIAT:w" s="T74">da</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_265" n="HIAT:w" s="T75">taksɨbatak</ts>
                  <nts id="Seg_266" n="HIAT:ip">.</nts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T91" id="Seg_269" n="HIAT:u" s="T76">
                  <ts e="T77" id="Seg_271" n="HIAT:w" s="T76">Ol</ts>
                  <nts id="Seg_272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_274" n="HIAT:w" s="T77">ihin</ts>
                  <nts id="Seg_275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_277" n="HIAT:w" s="T78">hübe</ts>
                  <nts id="Seg_278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_280" n="HIAT:w" s="T79">bu͡olbut</ts>
                  <nts id="Seg_281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_282" n="HIAT:ip">–</nts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_285" n="HIAT:w" s="T80">oŋoru͡orga</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_288" n="HIAT:w" s="T81">di͡en</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_291" n="HIAT:w" s="T82">munnʼagɨ</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_294" n="HIAT:w" s="T83">mu͡ora</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_297" n="HIAT:w" s="T84">ürdünen</ts>
                  <nts id="Seg_298" n="HIAT:ip">,</nts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_301" n="HIAT:w" s="T85">barɨlara</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_304" n="HIAT:w" s="T86">komullu͡oktarɨn</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_307" n="HIAT:w" s="T87">hɨraj</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_310" n="HIAT:w" s="T88">hɨrajga</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_313" n="HIAT:w" s="T89">kepsetiː</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_316" n="HIAT:w" s="T90">bu͡olu͡ogun</ts>
                  <nts id="Seg_317" n="HIAT:ip">.</nts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_320" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_322" n="HIAT:w" s="T91">Biːr</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_325" n="HIAT:w" s="T92">ulakan</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_328" n="HIAT:w" s="T93">uraha</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_331" n="HIAT:w" s="T94">dʼi͡ege</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_334" n="HIAT:w" s="T95">araččɨ</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_337" n="HIAT:w" s="T96">batan</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_340" n="HIAT:w" s="T97">komullubuttar</ts>
                  <nts id="Seg_341" n="HIAT:ip">,</nts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_344" n="HIAT:w" s="T98">turar</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_347" n="HIAT:w" s="T99">turbat</ts>
                  <nts id="Seg_348" n="HIAT:ip">.</nts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_351" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_353" n="HIAT:w" s="T100">Er</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_356" n="HIAT:w" s="T101">kihiler</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_359" n="HIAT:w" s="T102">tu͡ok</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_362" n="HIAT:w" s="T103">da</ts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_365" n="HIAT:w" s="T104">di͡ekterin</ts>
                  <nts id="Seg_366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_368" n="HIAT:w" s="T105">berditten</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_371" n="HIAT:w" s="T106">hir</ts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_374" n="HIAT:w" s="T107">di͡ek</ts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_377" n="HIAT:w" s="T108">agaj</ts>
                  <nts id="Seg_378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_380" n="HIAT:w" s="T109">köröllör</ts>
                  <nts id="Seg_381" n="HIAT:ip">.</nts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_384" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_386" n="HIAT:w" s="T110">Dʼaktar</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_389" n="HIAT:w" s="T111">ajmak</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_392" n="HIAT:w" s="T112">pɨlaːttarɨnan</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_395" n="HIAT:w" s="T113">karaktarɨn</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_398" n="HIAT:w" s="T114">uːlarɨn</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_401" n="HIAT:w" s="T115">hotunallar</ts>
                  <nts id="Seg_402" n="HIAT:ip">.</nts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T123" id="Seg_405" n="HIAT:u" s="T116">
                  <nts id="Seg_406" n="HIAT:ip">"</nts>
                  <ts e="T117" id="Seg_408" n="HIAT:w" s="T116">Kebeler</ts>
                  <nts id="Seg_409" n="HIAT:ip">,</nts>
                  <nts id="Seg_410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_412" n="HIAT:w" s="T117">tu͡ok</ts>
                  <nts id="Seg_413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_415" n="HIAT:w" s="T118">bu͡olaŋŋɨt</ts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_418" n="HIAT:w" s="T119">bastargɨtɨn</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_421" n="HIAT:w" s="T120">tobuktargɨt</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_424" n="HIAT:w" s="T121">ihiger</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_427" n="HIAT:w" s="T122">kiːllerdigit</ts>
                  <nts id="Seg_428" n="HIAT:ip">?</nts>
                  <nts id="Seg_429" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T133" id="Seg_431" n="HIAT:u" s="T123">
                  <ts e="T124" id="Seg_433" n="HIAT:w" s="T123">Togo</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_436" n="HIAT:w" s="T124">nʼimaha</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_439" n="HIAT:w" s="T125">hɨtagɨt</ts>
                  <nts id="Seg_440" n="HIAT:ip">"</nts>
                  <nts id="Seg_441" n="HIAT:ip">,</nts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_444" n="HIAT:w" s="T126">haŋarbɨt</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_447" n="HIAT:w" s="T127">di͡en</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_450" n="HIAT:w" s="T128">Dʼebgeːn</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_453" n="HIAT:w" s="T129">haŋalaːk</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_456" n="HIAT:w" s="T130">emeːksin</ts>
                  <nts id="Seg_457" n="HIAT:ip">,</nts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_460" n="HIAT:w" s="T131">ü͡ögüleːn</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_463" n="HIAT:w" s="T132">bi͡erbit</ts>
                  <nts id="Seg_464" n="HIAT:ip">.</nts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T137" id="Seg_467" n="HIAT:u" s="T133">
                  <ts e="T134" id="Seg_469" n="HIAT:w" s="T133">Gini</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_472" n="HIAT:w" s="T134">oburgu</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_475" n="HIAT:w" s="T135">balamata</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_478" n="HIAT:w" s="T136">bert</ts>
                  <nts id="Seg_479" n="HIAT:ip">.</nts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T140" id="Seg_482" n="HIAT:u" s="T137">
                  <ts e="T138" id="Seg_484" n="HIAT:w" s="T137">Kimten</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_487" n="HIAT:w" s="T138">da</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_490" n="HIAT:w" s="T139">tolullubat</ts>
                  <nts id="Seg_491" n="HIAT:ip">.</nts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T146" id="Seg_494" n="HIAT:u" s="T140">
                  <ts e="T141" id="Seg_496" n="HIAT:w" s="T140">Oloror</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_499" n="HIAT:w" s="T141">hirten</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_502" n="HIAT:w" s="T142">orguːjakaːn</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_505" n="HIAT:w" s="T143">turbut</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_508" n="HIAT:w" s="T144">Parfiraj</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_511" n="HIAT:w" s="T145">ogonnʼor</ts>
                  <nts id="Seg_512" n="HIAT:ip">.</nts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_515" n="HIAT:u" s="T146">
                  <ts e="T147" id="Seg_517" n="HIAT:w" s="T146">Melden</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_520" n="HIAT:w" s="T147">meniːtin</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_523" n="HIAT:w" s="T148">imerije</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_526" n="HIAT:w" s="T149">tüheːt</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_529" n="HIAT:w" s="T150">i͡edejbekke</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_532" n="HIAT:w" s="T151">oduːlammɨt</ts>
                  <nts id="Seg_533" n="HIAT:ip">.</nts>
                  <nts id="Seg_534" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_536" n="HIAT:u" s="T152">
                  <nts id="Seg_537" n="HIAT:ip">"</nts>
                  <ts e="T153" id="Seg_539" n="HIAT:w" s="T152">Kajtak</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_542" n="HIAT:w" s="T153">di͡emij</ts>
                  <nts id="Seg_543" n="HIAT:ip">!</nts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T157" id="Seg_546" n="HIAT:u" s="T154">
                  <ts e="T155" id="Seg_548" n="HIAT:w" s="T154">Iti</ts>
                  <nts id="Seg_549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_551" n="HIAT:w" s="T155">itigirdik</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_554" n="HIAT:w" s="T156">bu͡olar</ts>
                  <nts id="Seg_555" n="HIAT:ip">.</nts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T166" id="Seg_558" n="HIAT:u" s="T157">
                  <ts e="T158" id="Seg_560" n="HIAT:w" s="T157">Ogolor</ts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_563" n="HIAT:w" s="T158">bu</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_566" n="HIAT:w" s="T159">haŋa</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_569" n="HIAT:w" s="T160">üjege</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_572" n="HIAT:w" s="T161">ü͡örenellere</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_575" n="HIAT:w" s="T162">hin</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_578" n="HIAT:w" s="T163">daːganɨ</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_581" n="HIAT:w" s="T164">olus</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_584" n="HIAT:w" s="T165">höpsöllöːk</ts>
                  <nts id="Seg_585" n="HIAT:ip">.</nts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T170" id="Seg_588" n="HIAT:u" s="T166">
                  <ts e="T167" id="Seg_590" n="HIAT:w" s="T166">Giniler</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_593" n="HIAT:w" s="T167">huruksut</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_596" n="HIAT:w" s="T168">bu͡olu͡ok</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_599" n="HIAT:w" s="T169">tustaːktar</ts>
                  <nts id="Seg_600" n="HIAT:ip">.</nts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T179" id="Seg_603" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_605" n="HIAT:w" s="T170">Bu</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_608" n="HIAT:w" s="T171">min</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_611" n="HIAT:w" s="T172">Ujbaːn</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_614" n="HIAT:w" s="T173">u͡olbun</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_617" n="HIAT:w" s="T174">oččugujuttan</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_620" n="HIAT:w" s="T175">ü͡öreppitim</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_623" n="HIAT:w" s="T176">balɨk</ts>
                  <nts id="Seg_624" n="HIAT:ip">,</nts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_627" n="HIAT:w" s="T177">bult</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_630" n="HIAT:w" s="T178">kapkaːnnɨ͡agɨn</ts>
                  <nts id="Seg_631" n="HIAT:ip">.</nts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T182" id="Seg_634" n="HIAT:u" s="T179">
                  <ts e="T180" id="Seg_636" n="HIAT:w" s="T179">Min</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_639" n="HIAT:w" s="T180">anɨ</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_642" n="HIAT:w" s="T181">kɨrɨjdɨm</ts>
                  <nts id="Seg_643" n="HIAT:ip">.</nts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T186" id="Seg_646" n="HIAT:u" s="T182">
                  <ts e="T183" id="Seg_648" n="HIAT:w" s="T182">Üleni</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_651" n="HIAT:w" s="T183">kotummat</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_654" n="HIAT:w" s="T184">bu͡olan</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_657" n="HIAT:w" s="T185">erebin</ts>
                  <nts id="Seg_658" n="HIAT:ip">.</nts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T190" id="Seg_661" n="HIAT:u" s="T186">
                  <ts e="T187" id="Seg_663" n="HIAT:w" s="T186">Kim</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_666" n="HIAT:w" s="T187">min</ts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_669" n="HIAT:w" s="T188">kergemmin</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_672" n="HIAT:w" s="T189">ahatɨ͡agaj</ts>
                  <nts id="Seg_673" n="HIAT:ip">?</nts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T194" id="Seg_676" n="HIAT:u" s="T190">
                  <ts e="T191" id="Seg_678" n="HIAT:w" s="T190">Mu͡orabɨt</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_681" n="HIAT:w" s="T191">tɨmnɨː</ts>
                  <nts id="Seg_682" n="HIAT:ip">,</nts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_685" n="HIAT:w" s="T192">olus</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_688" n="HIAT:w" s="T193">purgaːlaːk</ts>
                  <nts id="Seg_689" n="HIAT:ip">.</nts>
                  <nts id="Seg_690" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T201" id="Seg_692" n="HIAT:u" s="T194">
                  <ts e="T195" id="Seg_694" n="HIAT:w" s="T194">Küːsteːk</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_697" n="HIAT:w" s="T195">ire</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_700" n="HIAT:w" s="T196">ogo</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_703" n="HIAT:w" s="T197">agaj</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_706" n="HIAT:w" s="T198">kihi</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_709" n="HIAT:w" s="T199">buldu</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_712" n="HIAT:w" s="T200">bulaːččɨ</ts>
                  <nts id="Seg_713" n="HIAT:ip">.</nts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T209" id="Seg_716" n="HIAT:u" s="T201">
                  <ts e="T202" id="Seg_718" n="HIAT:w" s="T201">Ujbaːnɨ</ts>
                  <nts id="Seg_719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_721" n="HIAT:w" s="T202">osku͡olaga</ts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_723" n="HIAT:ip">(</nts>
                  <nts id="Seg_724" n="HIAT:ip">(</nts>
                  <ats e="T204" id="Seg_725" n="HIAT:non-pho" s="T203">PAUSE</ats>
                  <nts id="Seg_726" n="HIAT:ip">)</nts>
                  <nts id="Seg_727" n="HIAT:ip">)</nts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_730" n="HIAT:w" s="T204">paːhɨ</ts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_733" n="HIAT:w" s="T205">kapkaːnɨ</ts>
                  <nts id="Seg_734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_736" n="HIAT:w" s="T206">iːte</ts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_739" n="HIAT:w" s="T207">ü͡öreti͡ektere</ts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_742" n="HIAT:w" s="T208">hu͡oga</ts>
                  <nts id="Seg_743" n="HIAT:ip">.</nts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T213" id="Seg_746" n="HIAT:u" s="T209">
                  <ts e="T210" id="Seg_748" n="HIAT:w" s="T209">Gini</ts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_751" n="HIAT:w" s="T210">umnu͡oga</ts>
                  <nts id="Seg_752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_754" n="HIAT:w" s="T211">mu͡ora</ts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_757" n="HIAT:w" s="T212">ületin</ts>
                  <nts id="Seg_758" n="HIAT:ip">.</nts>
                  <nts id="Seg_759" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T221" id="Seg_761" n="HIAT:u" s="T213">
                  <ts e="T214" id="Seg_763" n="HIAT:w" s="T213">Dʼaktar</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_766" n="HIAT:w" s="T214">kördük</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_769" n="HIAT:w" s="T215">dʼi͡ege</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_772" n="HIAT:w" s="T216">agaj</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_775" n="HIAT:w" s="T217">oloru͡oga</ts>
                  <nts id="Seg_776" n="HIAT:ip">,</nts>
                  <nts id="Seg_777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_779" n="HIAT:w" s="T218">tuːgu</ts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_782" n="HIAT:w" s="T219">da</ts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_785" n="HIAT:w" s="T220">gɨmmakka</ts>
                  <nts id="Seg_786" n="HIAT:ip">.</nts>
                  <nts id="Seg_787" n="HIAT:ip">"</nts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T229" id="Seg_790" n="HIAT:u" s="T221">
                  <nts id="Seg_791" n="HIAT:ip">"</nts>
                  <ts e="T222" id="Seg_793" n="HIAT:w" s="T221">En</ts>
                  <nts id="Seg_794" n="HIAT:ip">,</nts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_797" n="HIAT:w" s="T222">ogonnʼor</ts>
                  <nts id="Seg_798" n="HIAT:ip">,</nts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_801" n="HIAT:w" s="T223">dʼaktattarɨ</ts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_804" n="HIAT:w" s="T224">tɨːtɨma</ts>
                  <nts id="Seg_805" n="HIAT:ip">"</nts>
                  <nts id="Seg_806" n="HIAT:ip">,</nts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_809" n="HIAT:w" s="T225">emi͡e</ts>
                  <nts id="Seg_810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_812" n="HIAT:w" s="T226">Dʼebgeːn</ts>
                  <nts id="Seg_813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_815" n="HIAT:w" s="T227">üːgüːte</ts>
                  <nts id="Seg_816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_818" n="HIAT:w" s="T228">ihillibit</ts>
                  <nts id="Seg_819" n="HIAT:ip">.</nts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T233" id="Seg_822" n="HIAT:u" s="T229">
                  <nts id="Seg_823" n="HIAT:ip">"</nts>
                  <ts e="T230" id="Seg_825" n="HIAT:w" s="T229">Kim</ts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_828" n="HIAT:w" s="T230">eni͡eke</ts>
                  <nts id="Seg_829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_831" n="HIAT:w" s="T231">ahɨ</ts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_834" n="HIAT:w" s="T232">belenniːr</ts>
                  <nts id="Seg_835" n="HIAT:ip">?</nts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T239" id="Seg_838" n="HIAT:u" s="T233">
                  <ts e="T234" id="Seg_840" n="HIAT:w" s="T233">Dʼi͡egin</ts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_843" n="HIAT:w" s="T234">itiːtik</ts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_846" n="HIAT:w" s="T235">tutar</ts>
                  <nts id="Seg_847" n="HIAT:ip">,</nts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_850" n="HIAT:w" s="T236">ataktargɨn</ts>
                  <nts id="Seg_851" n="HIAT:ip">,</nts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_854" n="HIAT:w" s="T237">taŋaskɨn</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_857" n="HIAT:w" s="T238">abɨraktɨːr</ts>
                  <nts id="Seg_858" n="HIAT:ip">?</nts>
                  <nts id="Seg_859" n="HIAT:ip">"</nts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T248" id="Seg_862" n="HIAT:u" s="T239">
                  <nts id="Seg_863" n="HIAT:ip">"</nts>
                  <ts e="T240" id="Seg_865" n="HIAT:w" s="T239">Iti</ts>
                  <nts id="Seg_866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_868" n="HIAT:w" s="T240">kirdik</ts>
                  <nts id="Seg_869" n="HIAT:ip">,</nts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_872" n="HIAT:w" s="T241">itigirdik</ts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_875" n="HIAT:w" s="T242">bu͡olar</ts>
                  <nts id="Seg_876" n="HIAT:ip">"</nts>
                  <nts id="Seg_877" n="HIAT:ip">,</nts>
                  <nts id="Seg_878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_880" n="HIAT:w" s="T243">olus</ts>
                  <nts id="Seg_881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_883" n="HIAT:w" s="T244">ürpekke</ts>
                  <nts id="Seg_884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_886" n="HIAT:w" s="T245">Parfʼiraj</ts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_889" n="HIAT:w" s="T246">haŋara</ts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_892" n="HIAT:w" s="T247">turbut</ts>
                  <nts id="Seg_893" n="HIAT:ip">.</nts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T257" id="Seg_896" n="HIAT:u" s="T248">
                  <nts id="Seg_897" n="HIAT:ip">"</nts>
                  <ts e="T249" id="Seg_899" n="HIAT:w" s="T248">Min</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_902" n="HIAT:w" s="T249">Kɨːčaː</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_905" n="HIAT:w" s="T250">kɨːhɨm</ts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_908" n="HIAT:w" s="T251">astɨː</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_911" n="HIAT:w" s="T252">hatɨːr</ts>
                  <nts id="Seg_912" n="HIAT:ip">,</nts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_915" n="HIAT:w" s="T253">atagɨ</ts>
                  <nts id="Seg_916" n="HIAT:ip">,</nts>
                  <nts id="Seg_917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_919" n="HIAT:w" s="T254">taŋahɨ</ts>
                  <nts id="Seg_920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_922" n="HIAT:w" s="T255">oŋoro</ts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_925" n="HIAT:w" s="T256">hatɨːr</ts>
                  <nts id="Seg_926" n="HIAT:ip">.</nts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T262" id="Seg_929" n="HIAT:u" s="T257">
                  <ts e="T258" id="Seg_931" n="HIAT:w" s="T257">Osku͡olaga</ts>
                  <nts id="Seg_932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_934" n="HIAT:w" s="T258">hin</ts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_937" n="HIAT:w" s="T259">daːgɨnɨ</ts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_940" n="HIAT:w" s="T260">ontutun</ts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_943" n="HIAT:w" s="T261">umnu͡oga</ts>
                  <nts id="Seg_944" n="HIAT:ip">.</nts>
                  <nts id="Seg_945" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T269" id="Seg_947" n="HIAT:u" s="T262">
                  <ts e="T263" id="Seg_949" n="HIAT:w" s="T262">Er</ts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_952" n="HIAT:w" s="T263">kihi</ts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_955" n="HIAT:w" s="T264">kördük</ts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_958" n="HIAT:w" s="T265">končoŋnuː</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_961" n="HIAT:w" s="T266">hɨldʼɨ͡aga</ts>
                  <nts id="Seg_962" n="HIAT:ip">,</nts>
                  <nts id="Seg_963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_965" n="HIAT:w" s="T267">dʼi͡eni</ts>
                  <nts id="Seg_966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_968" n="HIAT:w" s="T268">tögürüje</ts>
                  <nts id="Seg_969" n="HIAT:ip">.</nts>
                  <nts id="Seg_970" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T274" id="Seg_972" n="HIAT:u" s="T269">
                  <ts e="T270" id="Seg_974" n="HIAT:w" s="T269">Kajtak</ts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_977" n="HIAT:w" s="T270">min</ts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_980" n="HIAT:w" s="T271">oloru͡omuj</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_983" n="HIAT:w" s="T272">ogolorum</ts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_986" n="HIAT:w" s="T273">bardaktarɨna</ts>
                  <nts id="Seg_987" n="HIAT:ip">?</nts>
                  <nts id="Seg_988" n="HIAT:ip">"</nts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T282" id="Seg_991" n="HIAT:u" s="T274">
                  <ts e="T275" id="Seg_993" n="HIAT:w" s="T274">Radavoːja</ts>
                  <nts id="Seg_994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_996" n="HIAT:w" s="T275">Anʼisʼim</ts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_999" n="HIAT:w" s="T276">Nazaravʼičʼ</ts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_1002" n="HIAT:w" s="T277">Papov</ts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_1005" n="HIAT:w" s="T278">külümnüː</ts>
                  <nts id="Seg_1006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_1008" n="HIAT:w" s="T279">tüheːt</ts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_1011" n="HIAT:w" s="T280">ogonnʼoru</ts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_1014" n="HIAT:w" s="T281">toktopput</ts>
                  <nts id="Seg_1015" n="HIAT:ip">.</nts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T287" id="Seg_1018" n="HIAT:u" s="T282">
                  <nts id="Seg_1019" n="HIAT:ip">"</nts>
                  <ts e="T283" id="Seg_1021" n="HIAT:w" s="T282">Dʼe</ts>
                  <nts id="Seg_1022" n="HIAT:ip">,</nts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1025" n="HIAT:w" s="T283">ogonnʼor</ts>
                  <nts id="Seg_1026" n="HIAT:ip">,</nts>
                  <nts id="Seg_1027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1029" n="HIAT:w" s="T284">en</ts>
                  <nts id="Seg_1030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1032" n="HIAT:w" s="T285">hɨlajdɨŋ</ts>
                  <nts id="Seg_1033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1035" n="HIAT:w" s="T286">bɨhɨlaːk</ts>
                  <nts id="Seg_1036" n="HIAT:ip">.</nts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T289" id="Seg_1039" n="HIAT:u" s="T287">
                  <ts e="T288" id="Seg_1041" n="HIAT:w" s="T287">Oloro</ts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1044" n="HIAT:w" s="T288">tüs</ts>
                  <nts id="Seg_1045" n="HIAT:ip">.</nts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T292" id="Seg_1048" n="HIAT:u" s="T289">
                  <ts e="T290" id="Seg_1050" n="HIAT:w" s="T289">Atɨn</ts>
                  <nts id="Seg_1051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1053" n="HIAT:w" s="T290">dʼon</ts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1056" n="HIAT:w" s="T291">haŋardɨn</ts>
                  <nts id="Seg_1057" n="HIAT:ip">.</nts>
                  <nts id="Seg_1058" n="HIAT:ip">"</nts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T304" id="Seg_1061" n="HIAT:u" s="T292">
                  <ts e="T293" id="Seg_1063" n="HIAT:w" s="T292">Ogonnʼor</ts>
                  <nts id="Seg_1064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1066" n="HIAT:w" s="T293">tu͡ok</ts>
                  <nts id="Seg_1067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1069" n="HIAT:w" s="T294">da</ts>
                  <nts id="Seg_1070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1072" n="HIAT:w" s="T295">di͡ebekke</ts>
                  <nts id="Seg_1073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1075" n="HIAT:w" s="T296">orguːjakaːn</ts>
                  <nts id="Seg_1076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1078" n="HIAT:w" s="T297">ologugar</ts>
                  <nts id="Seg_1079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1081" n="HIAT:w" s="T298">lak</ts>
                  <nts id="Seg_1082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1084" n="HIAT:w" s="T299">gɨna</ts>
                  <nts id="Seg_1085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1087" n="HIAT:w" s="T300">olorbut</ts>
                  <nts id="Seg_1088" n="HIAT:ip">,</nts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1091" n="HIAT:w" s="T301">tobuktarɨttan</ts>
                  <nts id="Seg_1092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1094" n="HIAT:w" s="T302">tutta</ts>
                  <nts id="Seg_1095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1097" n="HIAT:w" s="T303">tüheːt</ts>
                  <nts id="Seg_1098" n="HIAT:ip">.</nts>
                  <nts id="Seg_1099" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T311" id="Seg_1101" n="HIAT:u" s="T304">
                  <nts id="Seg_1102" n="HIAT:ip">"</nts>
                  <ts e="T305" id="Seg_1104" n="HIAT:w" s="T304">Kim</ts>
                  <nts id="Seg_1105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1107" n="HIAT:w" s="T305">össü͡ö</ts>
                  <nts id="Seg_1108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1110" n="HIAT:w" s="T306">tu͡ok</ts>
                  <nts id="Seg_1111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1113" n="HIAT:w" s="T307">haŋalaːgɨj</ts>
                  <nts id="Seg_1114" n="HIAT:ip">"</nts>
                  <nts id="Seg_1115" n="HIAT:ip">,</nts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1118" n="HIAT:w" s="T308">di͡en</ts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1121" n="HIAT:w" s="T309">ɨjɨppɨt</ts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1124" n="HIAT:w" s="T310">aradavojdara</ts>
                  <nts id="Seg_1125" n="HIAT:ip">.</nts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T321" id="Seg_1128" n="HIAT:u" s="T311">
                  <ts e="T312" id="Seg_1130" n="HIAT:w" s="T311">Kimnere</ts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1133" n="HIAT:w" s="T312">ere</ts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1136" n="HIAT:w" s="T313">haŋararɨn</ts>
                  <nts id="Seg_1137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1139" n="HIAT:w" s="T314">küːte</ts>
                  <nts id="Seg_1140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1142" n="HIAT:w" s="T315">hataːn</ts>
                  <nts id="Seg_1143" n="HIAT:ip">,</nts>
                  <nts id="Seg_1144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1146" n="HIAT:w" s="T316">aːkpɨt</ts>
                  <nts id="Seg_1147" n="HIAT:ip">,</nts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1150" n="HIAT:w" s="T317">kannuk</ts>
                  <nts id="Seg_1151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1153" n="HIAT:w" s="T318">ogoloro</ts>
                  <nts id="Seg_1154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1156" n="HIAT:w" s="T319">osku͡olaga</ts>
                  <nts id="Seg_1157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1159" n="HIAT:w" s="T320">barallarɨn</ts>
                  <nts id="Seg_1160" n="HIAT:ip">.</nts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T327" id="Seg_1163" n="HIAT:u" s="T321">
                  <ts e="T322" id="Seg_1165" n="HIAT:w" s="T321">Kim</ts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1168" n="HIAT:w" s="T322">kaːlarɨn</ts>
                  <nts id="Seg_1169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1171" n="HIAT:w" s="T323">osku͡olaga</ts>
                  <nts id="Seg_1172" n="HIAT:ip">,</nts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1175" n="HIAT:w" s="T324">barbatɨn</ts>
                  <nts id="Seg_1176" n="HIAT:ip">,</nts>
                  <nts id="Seg_1177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1179" n="HIAT:w" s="T325">haːstara</ts>
                  <nts id="Seg_1180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1182" n="HIAT:w" s="T326">ulakattarɨ</ts>
                  <nts id="Seg_1183" n="HIAT:ip">.</nts>
                  <nts id="Seg_1184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T334" id="Seg_1186" n="HIAT:u" s="T327">
                  <ts e="T328" id="Seg_1188" n="HIAT:w" s="T327">Ol</ts>
                  <nts id="Seg_1189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1191" n="HIAT:w" s="T328">ogolorgo</ts>
                  <nts id="Seg_1192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1194" n="HIAT:w" s="T329">tübespit</ts>
                  <nts id="Seg_1195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1197" n="HIAT:w" s="T330">Parfʼiraj</ts>
                  <nts id="Seg_1198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1200" n="HIAT:w" s="T331">ogonnʼor</ts>
                  <nts id="Seg_1201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1203" n="HIAT:w" s="T332">u͡ola</ts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1206" n="HIAT:w" s="T333">Ujbaːn</ts>
                  <nts id="Seg_1207" n="HIAT:ip">.</nts>
                  <nts id="Seg_1208" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T338" id="Seg_1210" n="HIAT:u" s="T334">
                  <nts id="Seg_1211" n="HIAT:ip">"</nts>
                  <ts e="T335" id="Seg_1213" n="HIAT:w" s="T334">Kɨːčaːnɨ</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1216" n="HIAT:w" s="T335">emi͡e</ts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1219" n="HIAT:w" s="T336">keːhi͡ekke</ts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1222" n="HIAT:w" s="T337">tustaːk</ts>
                  <nts id="Seg_1223" n="HIAT:ip">.</nts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T346" id="Seg_1226" n="HIAT:u" s="T338">
                  <ts e="T339" id="Seg_1228" n="HIAT:w" s="T338">Gini</ts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1231" n="HIAT:w" s="T339">haːha</ts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1234" n="HIAT:w" s="T340">hin</ts>
                  <nts id="Seg_1235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1237" n="HIAT:w" s="T341">daːgɨnɨ</ts>
                  <nts id="Seg_1238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1240" n="HIAT:w" s="T342">elbek</ts>
                  <nts id="Seg_1241" n="HIAT:ip">"</nts>
                  <nts id="Seg_1242" n="HIAT:ip">,</nts>
                  <nts id="Seg_1243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1245" n="HIAT:w" s="T343">kördömmüt</ts>
                  <nts id="Seg_1246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1248" n="HIAT:w" s="T344">Parfʼiraj</ts>
                  <nts id="Seg_1249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1251" n="HIAT:w" s="T345">ogonnʼor</ts>
                  <nts id="Seg_1252" n="HIAT:ip">.</nts>
                  <nts id="Seg_1253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T351" id="Seg_1255" n="HIAT:u" s="T346">
                  <nts id="Seg_1256" n="HIAT:ip">"</nts>
                  <ts e="T347" id="Seg_1258" n="HIAT:w" s="T346">Hu͡ok</ts>
                  <nts id="Seg_1259" n="HIAT:ip">,</nts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1262" n="HIAT:w" s="T347">Kɨːčaː</ts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1265" n="HIAT:w" s="T348">barɨ͡a</ts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1268" n="HIAT:w" s="T349">hu͡oga</ts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1271" n="HIAT:w" s="T350">osku͡olaga</ts>
                  <nts id="Seg_1272" n="HIAT:ip">.</nts>
                  <nts id="Seg_1273" n="HIAT:ip">"</nts>
                  <nts id="Seg_1274" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T360" id="Seg_1276" n="HIAT:u" s="T351">
                  <nts id="Seg_1277" n="HIAT:ip">"</nts>
                  <ts e="T352" id="Seg_1279" n="HIAT:w" s="T351">Ogonnʼor</ts>
                  <nts id="Seg_1280" n="HIAT:ip">,</nts>
                  <nts id="Seg_1281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1283" n="HIAT:w" s="T352">kečehime</ts>
                  <nts id="Seg_1284" n="HIAT:ip">,</nts>
                  <nts id="Seg_1285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1287" n="HIAT:w" s="T353">ü͡örekteːk</ts>
                  <nts id="Seg_1288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1290" n="HIAT:w" s="T354">bu͡ollagɨna</ts>
                  <nts id="Seg_1291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1293" n="HIAT:w" s="T355">er</ts>
                  <nts id="Seg_1294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1296" n="HIAT:w" s="T356">kihi</ts>
                  <nts id="Seg_1297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1299" n="HIAT:w" s="T357">hɨ͡alɨjatɨn</ts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1302" n="HIAT:w" s="T358">keti͡e</ts>
                  <nts id="Seg_1303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1305" n="HIAT:w" s="T359">hu͡oga</ts>
                  <nts id="Seg_1306" n="HIAT:ip">.</nts>
                  <nts id="Seg_1307" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T365" id="Seg_1309" n="HIAT:u" s="T360">
                  <ts e="T361" id="Seg_1311" n="HIAT:w" s="T360">Dʼi͡eni</ts>
                  <nts id="Seg_1312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1314" n="HIAT:w" s="T361">tögürüččü</ts>
                  <nts id="Seg_1315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1317" n="HIAT:w" s="T362">končoŋnuː</ts>
                  <nts id="Seg_1318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1320" n="HIAT:w" s="T363">hɨldʼɨ͡a</ts>
                  <nts id="Seg_1321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1323" n="HIAT:w" s="T364">hu͡oga</ts>
                  <nts id="Seg_1324" n="HIAT:ip">.</nts>
                  <nts id="Seg_1325" n="HIAT:ip">"</nts>
                  <nts id="Seg_1326" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T371" id="Seg_1328" n="HIAT:u" s="T365">
                  <ts e="T366" id="Seg_1330" n="HIAT:w" s="T365">Munnʼakka</ts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1333" n="HIAT:w" s="T366">oloror</ts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1336" n="HIAT:w" s="T367">dʼon</ts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1339" n="HIAT:w" s="T368">ele</ts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1342" n="HIAT:w" s="T369">isterinen</ts>
                  <nts id="Seg_1343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1345" n="HIAT:w" s="T370">külsübütter</ts>
                  <nts id="Seg_1346" n="HIAT:ip">.</nts>
                  <nts id="Seg_1347" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T377" id="Seg_1349" n="HIAT:u" s="T371">
                  <nts id="Seg_1350" n="HIAT:ip">"</nts>
                  <ts e="T372" id="Seg_1352" n="HIAT:w" s="T371">Barar</ts>
                  <nts id="Seg_1353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1355" n="HIAT:w" s="T372">ogoloru</ts>
                  <nts id="Seg_1356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1357" n="HIAT:ip">(</nts>
                  <ts e="T375" id="Seg_1359" n="HIAT:w" s="T373">belin-</ts>
                  <nts id="Seg_1360" n="HIAT:ip">)</nts>
                  <nts id="Seg_1361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1363" n="HIAT:w" s="T375">belenneːŋ</ts>
                  <nts id="Seg_1364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1366" n="HIAT:w" s="T376">ötü͡ötük</ts>
                  <nts id="Seg_1367" n="HIAT:ip">.</nts>
                  <nts id="Seg_1368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T381" id="Seg_1370" n="HIAT:u" s="T377">
                  <ts e="T378" id="Seg_1372" n="HIAT:w" s="T377">Taŋɨnnarɨŋ</ts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1375" n="HIAT:w" s="T378">taŋaraga</ts>
                  <nts id="Seg_1376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1378" n="HIAT:w" s="T379">taŋɨnnarar</ts>
                  <nts id="Seg_1379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1381" n="HIAT:w" s="T380">kördük</ts>
                  <nts id="Seg_1382" n="HIAT:ip">.</nts>
                  <nts id="Seg_1383" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T390" id="Seg_1385" n="HIAT:u" s="T381">
                  <ts e="T382" id="Seg_1387" n="HIAT:w" s="T381">Ogolorbut</ts>
                  <nts id="Seg_1388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1390" n="HIAT:w" s="T382">bejem</ts>
                  <nts id="Seg_1391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1393" n="HIAT:w" s="T383">illi͡em</ts>
                  <nts id="Seg_1394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1396" n="HIAT:w" s="T384">Valačankattan</ts>
                  <nts id="Seg_1397" n="HIAT:ip">"</nts>
                  <nts id="Seg_1398" n="HIAT:ip">,</nts>
                  <nts id="Seg_1399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1401" n="HIAT:w" s="T385">munnʼagɨ</ts>
                  <nts id="Seg_1402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1404" n="HIAT:w" s="T386">bütere</ts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1407" n="HIAT:w" s="T387">gɨna</ts>
                  <nts id="Seg_1408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1410" n="HIAT:w" s="T388">di͡ebit</ts>
                  <nts id="Seg_1411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1413" n="HIAT:w" s="T389">hübehittere</ts>
                  <nts id="Seg_1414" n="HIAT:ip">.</nts>
                  <nts id="Seg_1415" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T395" id="Seg_1417" n="HIAT:u" s="T390">
                  <ts e="T391" id="Seg_1419" n="HIAT:w" s="T390">Munnʼak</ts>
                  <nts id="Seg_1420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1422" n="HIAT:w" s="T391">ahaːtɨn</ts>
                  <nts id="Seg_1423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1425" n="HIAT:w" s="T392">komunuː</ts>
                  <nts id="Seg_1426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1428" n="HIAT:w" s="T393">ajdaːna</ts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1431" n="HIAT:w" s="T394">elbeːbit</ts>
                  <nts id="Seg_1432" n="HIAT:ip">.</nts>
                  <nts id="Seg_1433" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T403" id="Seg_1435" n="HIAT:u" s="T395">
                  <ts e="T396" id="Seg_1437" n="HIAT:w" s="T395">Dʼaktattar</ts>
                  <nts id="Seg_1438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1440" n="HIAT:w" s="T396">matagalarɨn</ts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1443" n="HIAT:w" s="T397">ihitten</ts>
                  <nts id="Seg_1444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1446" n="HIAT:w" s="T398">tahartɨːllar</ts>
                  <nts id="Seg_1447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1449" n="HIAT:w" s="T399">ogolorun</ts>
                  <nts id="Seg_1450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1452" n="HIAT:w" s="T400">ki͡eŋ</ts>
                  <nts id="Seg_1453" n="HIAT:ip">,</nts>
                  <nts id="Seg_1454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1456" n="HIAT:w" s="T401">ki͡eŋ</ts>
                  <nts id="Seg_1457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1459" n="HIAT:w" s="T402">taŋastarɨn</ts>
                  <nts id="Seg_1460" n="HIAT:ip">.</nts>
                  <nts id="Seg_1461" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T406" id="Seg_1463" n="HIAT:u" s="T403">
                  <ts e="T404" id="Seg_1465" n="HIAT:w" s="T403">Taŋaraga</ts>
                  <nts id="Seg_1466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1468" n="HIAT:w" s="T404">keter</ts>
                  <nts id="Seg_1469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1471" n="HIAT:w" s="T405">ɨrbaːkɨlarɨn</ts>
                  <nts id="Seg_1472" n="HIAT:ip">.</nts>
                  <nts id="Seg_1473" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T414" id="Seg_1475" n="HIAT:u" s="T406">
                  <ts e="T407" id="Seg_1477" n="HIAT:w" s="T406">Horoktor</ts>
                  <nts id="Seg_1478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1480" n="HIAT:w" s="T407">köstübet</ts>
                  <nts id="Seg_1481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1483" n="HIAT:w" s="T408">hirge</ts>
                  <nts id="Seg_1484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1486" n="HIAT:w" s="T409">tigettiːller</ts>
                  <nts id="Seg_1487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1489" n="HIAT:w" s="T410">ebit</ts>
                  <nts id="Seg_1490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1492" n="HIAT:w" s="T411">oččuguj</ts>
                  <nts id="Seg_1493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1495" n="HIAT:w" s="T412">taŋaralarɨn</ts>
                  <nts id="Seg_1496" n="HIAT:ip">,</nts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1499" n="HIAT:w" s="T413">kiri͡esteri</ts>
                  <nts id="Seg_1500" n="HIAT:ip">.</nts>
                  <nts id="Seg_1501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T424" id="Seg_1503" n="HIAT:u" s="T414">
                  <ts e="T415" id="Seg_1505" n="HIAT:w" s="T414">Ogolorun</ts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1507" n="HIAT:ip">(</nts>
                  <ts e="T416" id="Seg_1509" n="HIAT:w" s="T415">či͡estijdar</ts>
                  <nts id="Seg_1510" n="HIAT:ip">)</nts>
                  <nts id="Seg_1511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1513" n="HIAT:w" s="T416">barɨ</ts>
                  <nts id="Seg_1514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1516" n="HIAT:w" s="T417">ele</ts>
                  <nts id="Seg_1517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1519" n="HIAT:w" s="T418">minnʼiges</ts>
                  <nts id="Seg_1520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1522" n="HIAT:w" s="T419">astarɨnan</ts>
                  <nts id="Seg_1523" n="HIAT:ip">,</nts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1526" n="HIAT:w" s="T420">amahannan</ts>
                  <nts id="Seg_1527" n="HIAT:ip">,</nts>
                  <nts id="Seg_1528" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1530" n="HIAT:w" s="T421">kagɨnan</ts>
                  <nts id="Seg_1531" n="HIAT:ip">,</nts>
                  <nts id="Seg_1532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1534" n="HIAT:w" s="T422">taba</ts>
                  <nts id="Seg_1535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1537" n="HIAT:w" s="T423">tɨlɨnan</ts>
                  <nts id="Seg_1538" n="HIAT:ip">.</nts>
                  <nts id="Seg_1539" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T433" id="Seg_1541" n="HIAT:u" s="T424">
                  <ts e="T425" id="Seg_1543" n="HIAT:w" s="T424">Ki͡ehe</ts>
                  <nts id="Seg_1544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1546" n="HIAT:w" s="T425">dek</ts>
                  <nts id="Seg_1547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1548" n="HIAT:ip">(</nts>
                  <ts e="T428" id="Seg_1550" n="HIAT:w" s="T426">rada-</ts>
                  <nts id="Seg_1551" n="HIAT:ip">)</nts>
                  <nts id="Seg_1552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1554" n="HIAT:w" s="T428">radavojugar</ts>
                  <nts id="Seg_1555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1557" n="HIAT:w" s="T429">ɨ͡allana</ts>
                  <nts id="Seg_1558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1560" n="HIAT:w" s="T430">kiːrbit</ts>
                  <nts id="Seg_1561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1563" n="HIAT:w" s="T431">Hemen</ts>
                  <nts id="Seg_1564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1566" n="HIAT:w" s="T432">ogonnʼor</ts>
                  <nts id="Seg_1567" n="HIAT:ip">.</nts>
                  <nts id="Seg_1568" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T437" id="Seg_1570" n="HIAT:u" s="T433">
                  <ts e="T434" id="Seg_1572" n="HIAT:w" s="T433">Togo</ts>
                  <nts id="Seg_1573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1575" n="HIAT:w" s="T434">kelbitin</ts>
                  <nts id="Seg_1576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1578" n="HIAT:w" s="T435">bɨha</ts>
                  <nts id="Seg_1579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1581" n="HIAT:w" s="T436">haŋarbat</ts>
                  <nts id="Seg_1582" n="HIAT:ip">.</nts>
                  <nts id="Seg_1583" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T442" id="Seg_1585" n="HIAT:u" s="T437">
                  <ts e="T438" id="Seg_1587" n="HIAT:w" s="T437">Čaːj</ts>
                  <nts id="Seg_1588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1590" n="HIAT:w" s="T438">ihe</ts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1593" n="HIAT:w" s="T439">agaj</ts>
                  <nts id="Seg_1594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1596" n="HIAT:w" s="T440">oloron</ts>
                  <nts id="Seg_1597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1599" n="HIAT:w" s="T441">di͡ebit</ts>
                  <nts id="Seg_1600" n="HIAT:ip">:</nts>
                  <nts id="Seg_1601" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T447" id="Seg_1603" n="HIAT:u" s="T442">
                  <nts id="Seg_1604" n="HIAT:ip">"</nts>
                  <ts e="T443" id="Seg_1606" n="HIAT:w" s="T442">Bɨlɨr</ts>
                  <nts id="Seg_1607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1609" n="HIAT:w" s="T443">ehelerbit</ts>
                  <nts id="Seg_1610" n="HIAT:ip">,</nts>
                  <nts id="Seg_1611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1613" n="HIAT:w" s="T444">agalarbɨt</ts>
                  <nts id="Seg_1614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1616" n="HIAT:w" s="T445">taŋarahɨt</ts>
                  <nts id="Seg_1617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1619" n="HIAT:w" s="T446">etilere</ts>
                  <nts id="Seg_1620" n="HIAT:ip">.</nts>
                  <nts id="Seg_1621" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T453" id="Seg_1623" n="HIAT:u" s="T447">
                  <ts e="T448" id="Seg_1625" n="HIAT:w" s="T447">Ulakan</ts>
                  <nts id="Seg_1626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1628" n="HIAT:w" s="T448">hajtaːŋŋa</ts>
                  <nts id="Seg_1629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1631" n="HIAT:w" s="T449">bi͡ek</ts>
                  <nts id="Seg_1632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1634" n="HIAT:w" s="T450">belektiːr</ts>
                  <nts id="Seg_1635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1637" n="HIAT:w" s="T451">ideleːk</ts>
                  <nts id="Seg_1638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1640" n="HIAT:w" s="T452">etilere</ts>
                  <nts id="Seg_1641" n="HIAT:ip">.</nts>
                  <nts id="Seg_1642" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T458" id="Seg_1644" n="HIAT:u" s="T453">
                  <ts e="T454" id="Seg_1646" n="HIAT:w" s="T453">Barɨ</ts>
                  <nts id="Seg_1647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1649" n="HIAT:w" s="T454">aːjmak</ts>
                  <nts id="Seg_1650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1652" n="HIAT:w" s="T455">ötü͡ötük</ts>
                  <nts id="Seg_1653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1655" n="HIAT:w" s="T456">oloru͡oktarɨn</ts>
                  <nts id="Seg_1656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1658" n="HIAT:w" s="T457">bagarallar</ts>
                  <nts id="Seg_1659" n="HIAT:ip">.</nts>
                  <nts id="Seg_1660" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T462" id="Seg_1662" n="HIAT:u" s="T458">
                  <ts e="T459" id="Seg_1664" n="HIAT:w" s="T458">Tuːgunan</ts>
                  <nts id="Seg_1665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1667" n="HIAT:w" s="T459">da</ts>
                  <nts id="Seg_1668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1670" n="HIAT:w" s="T460">muskuraːbakka</ts>
                  <nts id="Seg_1671" n="HIAT:ip">,</nts>
                  <nts id="Seg_1672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1674" n="HIAT:w" s="T461">ɨ͡aldʼɨbakka</ts>
                  <nts id="Seg_1675" n="HIAT:ip">.</nts>
                  <nts id="Seg_1676" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T470" id="Seg_1678" n="HIAT:u" s="T462">
                  <ts e="T463" id="Seg_1680" n="HIAT:w" s="T462">Ulakan</ts>
                  <nts id="Seg_1681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1683" n="HIAT:w" s="T463">ojuttar</ts>
                  <nts id="Seg_1684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1686" n="HIAT:w" s="T464">kihi</ts>
                  <nts id="Seg_1687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1689" n="HIAT:w" s="T465">hɨldʼar</ts>
                  <nts id="Seg_1690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1692" n="HIAT:w" s="T466">hu͡olun</ts>
                  <nts id="Seg_1693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1695" n="HIAT:w" s="T467">bilellere</ts>
                  <nts id="Seg_1696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1698" n="HIAT:w" s="T468">bert</ts>
                  <nts id="Seg_1699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1701" n="HIAT:w" s="T469">ete</ts>
                  <nts id="Seg_1702" n="HIAT:ip">.</nts>
                  <nts id="Seg_1703" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T477" id="Seg_1705" n="HIAT:u" s="T470">
                  <ts e="T471" id="Seg_1707" n="HIAT:w" s="T470">Bu</ts>
                  <nts id="Seg_1708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1710" n="HIAT:w" s="T471">olokpututtan</ts>
                  <nts id="Seg_1711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1713" n="HIAT:w" s="T472">ɨraːga</ts>
                  <nts id="Seg_1714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1716" n="HIAT:w" s="T473">hu͡ok</ts>
                  <nts id="Seg_1717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1719" n="HIAT:w" s="T474">baːr</ts>
                  <nts id="Seg_1720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1722" n="HIAT:w" s="T475">ulakan</ts>
                  <nts id="Seg_1723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1725" n="HIAT:w" s="T476">haːjtan-taːs</ts>
                  <nts id="Seg_1726" n="HIAT:ip">.</nts>
                  <nts id="Seg_1727" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T482" id="Seg_1729" n="HIAT:u" s="T477">
                  <ts e="T478" id="Seg_1731" n="HIAT:w" s="T477">Barɨ</ts>
                  <nts id="Seg_1732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1734" n="HIAT:w" s="T478">kihi</ts>
                  <nts id="Seg_1735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1737" n="HIAT:w" s="T479">onno</ts>
                  <nts id="Seg_1738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1740" n="HIAT:w" s="T480">belegi</ts>
                  <nts id="Seg_1741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1743" n="HIAT:w" s="T481">bi͡ereːčči</ts>
                  <nts id="Seg_1744" n="HIAT:ip">.</nts>
                  <nts id="Seg_1745" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T489" id="Seg_1747" n="HIAT:u" s="T482">
                  <ts e="T483" id="Seg_1749" n="HIAT:w" s="T482">Hotoru</ts>
                  <nts id="Seg_1750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1752" n="HIAT:w" s="T483">ogolorbut</ts>
                  <nts id="Seg_1753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1755" n="HIAT:w" s="T484">atɨn</ts>
                  <nts id="Seg_1756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1758" n="HIAT:w" s="T485">hirge</ts>
                  <nts id="Seg_1759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1761" n="HIAT:w" s="T486">barallar</ts>
                  <nts id="Seg_1762" n="HIAT:ip">,</nts>
                  <nts id="Seg_1763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1765" n="HIAT:w" s="T487">atɨn</ts>
                  <nts id="Seg_1766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1768" n="HIAT:w" s="T488">kihilerge</ts>
                  <nts id="Seg_1769" n="HIAT:ip">.</nts>
                  <nts id="Seg_1770" n="HIAT:ip">"</nts>
                  <nts id="Seg_1771" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T496" id="Seg_1773" n="HIAT:u" s="T489">
                  <ts e="T490" id="Seg_1775" n="HIAT:w" s="T489">Radavoja</ts>
                  <nts id="Seg_1776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1778" n="HIAT:w" s="T490">ɨ͡aldʼɨtɨn</ts>
                  <nts id="Seg_1779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1781" n="HIAT:w" s="T491">öhün</ts>
                  <nts id="Seg_1782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1784" n="HIAT:w" s="T492">ihilliː-ihilliː</ts>
                  <nts id="Seg_1785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1787" n="HIAT:w" s="T493">ihiger</ts>
                  <nts id="Seg_1788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1790" n="HIAT:w" s="T494">diː</ts>
                  <nts id="Seg_1791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1793" n="HIAT:w" s="T495">hanɨːr</ts>
                  <nts id="Seg_1794" n="HIAT:ip">:</nts>
                  <nts id="Seg_1795" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T501" id="Seg_1797" n="HIAT:u" s="T496">
                  <nts id="Seg_1798" n="HIAT:ip">"</nts>
                  <ts e="T497" id="Seg_1800" n="HIAT:w" s="T496">Tu͡ok</ts>
                  <nts id="Seg_1801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1803" n="HIAT:w" s="T497">hanaːtɨnan</ts>
                  <nts id="Seg_1804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1806" n="HIAT:w" s="T498">itinnik</ts>
                  <nts id="Seg_1807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1809" n="HIAT:w" s="T499">öhü</ts>
                  <nts id="Seg_1810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1812" n="HIAT:w" s="T500">tahaːrar</ts>
                  <nts id="Seg_1813" n="HIAT:ip">?</nts>
                  <nts id="Seg_1814" n="HIAT:ip">"</nts>
                  <nts id="Seg_1815" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T505" id="Seg_1817" n="HIAT:u" s="T501">
                  <ts e="T502" id="Seg_1819" n="HIAT:w" s="T501">ɨ͡aldʼɨta</ts>
                  <nts id="Seg_1820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1822" n="HIAT:w" s="T502">i͡edejbekke</ts>
                  <nts id="Seg_1823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1825" n="HIAT:w" s="T503">haŋartaːn</ts>
                  <nts id="Seg_1826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1828" n="HIAT:w" s="T504">ispit</ts>
                  <nts id="Seg_1829" n="HIAT:ip">.</nts>
                  <nts id="Seg_1830" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T510" id="Seg_1832" n="HIAT:u" s="T505">
                  <nts id="Seg_1833" n="HIAT:ip">"</nts>
                  <ts e="T506" id="Seg_1835" n="HIAT:w" s="T505">Dʼaktattar</ts>
                  <nts id="Seg_1836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1838" n="HIAT:w" s="T506">muŋnaːktar</ts>
                  <nts id="Seg_1839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1841" n="HIAT:w" s="T507">olus</ts>
                  <nts id="Seg_1842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1844" n="HIAT:w" s="T508">hoŋguraːtɨlar</ts>
                  <nts id="Seg_1845" n="HIAT:ip">,</nts>
                  <nts id="Seg_1846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1848" n="HIAT:w" s="T509">ɨtahallar</ts>
                  <nts id="Seg_1849" n="HIAT:ip">.</nts>
                  <nts id="Seg_1850" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T517" id="Seg_1852" n="HIAT:u" s="T510">
                  <ts e="T511" id="Seg_1854" n="HIAT:w" s="T510">Kɨra</ts>
                  <nts id="Seg_1855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1857" n="HIAT:w" s="T511">ogonu</ts>
                  <nts id="Seg_1858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1860" n="HIAT:w" s="T512">dʼi͡etten</ts>
                  <nts id="Seg_1861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1863" n="HIAT:w" s="T513">bɨldʼɨːr</ts>
                  <nts id="Seg_1864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1866" n="HIAT:w" s="T514">ide</ts>
                  <nts id="Seg_1867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1869" n="HIAT:w" s="T515">olus</ts>
                  <nts id="Seg_1870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1872" n="HIAT:w" s="T516">anʼɨːlaːk</ts>
                  <nts id="Seg_1873" n="HIAT:ip">.</nts>
                  <nts id="Seg_1874" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T520" id="Seg_1876" n="HIAT:u" s="T517">
                  <ts e="T518" id="Seg_1878" n="HIAT:w" s="T517">Dʼon</ts>
                  <nts id="Seg_1879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1881" n="HIAT:w" s="T518">olus</ts>
                  <nts id="Seg_1882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1884" n="HIAT:w" s="T519">hanaːrgɨːr</ts>
                  <nts id="Seg_1885" n="HIAT:ip">.</nts>
                  <nts id="Seg_1886" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T524" id="Seg_1888" n="HIAT:u" s="T520">
                  <ts e="T521" id="Seg_1890" n="HIAT:w" s="T520">Badaga</ts>
                  <nts id="Seg_1891" n="HIAT:ip">,</nts>
                  <nts id="Seg_1892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1894" n="HIAT:w" s="T521">hajtaːŋŋa</ts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1897" n="HIAT:w" s="T522">belek</ts>
                  <nts id="Seg_1898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1900" n="HIAT:w" s="T523">bi͡eri͡ekke</ts>
                  <nts id="Seg_1901" n="HIAT:ip">.</nts>
                  <nts id="Seg_1902" n="HIAT:ip">"</nts>
                  <nts id="Seg_1903" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T531" id="Seg_1905" n="HIAT:u" s="T524">
                  <nts id="Seg_1906" n="HIAT:ip">"</nts>
                  <ts e="T525" id="Seg_1908" n="HIAT:w" s="T524">Dʼe</ts>
                  <nts id="Seg_1909" n="HIAT:ip">,</nts>
                  <nts id="Seg_1910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1912" n="HIAT:w" s="T525">ogonnʼor</ts>
                  <nts id="Seg_1913" n="HIAT:ip">,</nts>
                  <nts id="Seg_1914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1916" n="HIAT:w" s="T526">minigin</ts>
                  <nts id="Seg_1917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1919" n="HIAT:w" s="T527">hiːleːme</ts>
                  <nts id="Seg_1920" n="HIAT:ip">,</nts>
                  <nts id="Seg_1921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1923" n="HIAT:w" s="T528">itini</ts>
                  <nts id="Seg_1924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_1926" n="HIAT:w" s="T529">gɨnɨ͡akpɨt</ts>
                  <nts id="Seg_1927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_1929" n="HIAT:w" s="T530">hu͡oga</ts>
                  <nts id="Seg_1930" n="HIAT:ip">.</nts>
                  <nts id="Seg_1931" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T534" id="Seg_1933" n="HIAT:u" s="T531">
                  <ts e="T532" id="Seg_1935" n="HIAT:w" s="T531">Usku͡ola</ts>
                  <nts id="Seg_1936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_1938" n="HIAT:w" s="T532">hajtaːnɨ</ts>
                  <nts id="Seg_1939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1941" n="HIAT:w" s="T533">kördöːböt</ts>
                  <nts id="Seg_1942" n="HIAT:ip">.</nts>
                  <nts id="Seg_1943" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T541" id="Seg_1945" n="HIAT:u" s="T534">
                  <ts e="T535" id="Seg_1947" n="HIAT:w" s="T534">Itigirdik</ts>
                  <nts id="Seg_1948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1950" n="HIAT:w" s="T535">barɨ</ts>
                  <nts id="Seg_1951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_1953" n="HIAT:w" s="T536">kihi͡eke</ts>
                  <nts id="Seg_1954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_1956" n="HIAT:w" s="T537">kepseː</ts>
                  <nts id="Seg_1957" n="HIAT:ip">,</nts>
                  <nts id="Seg_1958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1960" n="HIAT:w" s="T538">hanargaːbatɨnnar</ts>
                  <nts id="Seg_1961" n="HIAT:ip">"</nts>
                  <nts id="Seg_1962" n="HIAT:ip">,</nts>
                  <nts id="Seg_1963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1965" n="HIAT:w" s="T539">di͡ebit</ts>
                  <nts id="Seg_1966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_1968" n="HIAT:w" s="T540">radavoja</ts>
                  <nts id="Seg_1969" n="HIAT:ip">.</nts>
                  <nts id="Seg_1970" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T548" id="Seg_1972" n="HIAT:u" s="T541">
                  <ts e="T542" id="Seg_1974" n="HIAT:w" s="T541">Ogonnʼor</ts>
                  <nts id="Seg_1975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_1977" n="HIAT:w" s="T542">tu͡ok</ts>
                  <nts id="Seg_1978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_1980" n="HIAT:w" s="T543">da</ts>
                  <nts id="Seg_1981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_1983" n="HIAT:w" s="T544">di͡ebekke</ts>
                  <nts id="Seg_1984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_1986" n="HIAT:w" s="T545">čaːskɨtɨgar</ts>
                  <nts id="Seg_1987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_1989" n="HIAT:w" s="T546">čaːj</ts>
                  <nts id="Seg_1990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_1992" n="HIAT:w" s="T547">kutummut</ts>
                  <nts id="Seg_1993" n="HIAT:ip">.</nts>
                  <nts id="Seg_1994" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T556" id="Seg_1996" n="HIAT:u" s="T548">
                  <nts id="Seg_1997" n="HIAT:ip">"</nts>
                  <ts e="T549" id="Seg_1999" n="HIAT:w" s="T548">Bihigi</ts>
                  <nts id="Seg_2000" n="HIAT:ip">,</nts>
                  <nts id="Seg_2001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_2003" n="HIAT:w" s="T549">ogonnʼottor</ts>
                  <nts id="Seg_2004" n="HIAT:ip">,</nts>
                  <nts id="Seg_2005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_2007" n="HIAT:w" s="T550">kajtak</ts>
                  <nts id="Seg_2008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_2010" n="HIAT:w" s="T551">ötü͡ö</ts>
                  <nts id="Seg_2011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_2013" n="HIAT:w" s="T552">bu͡olu͡on</ts>
                  <nts id="Seg_2014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_2016" n="HIAT:w" s="T553">di͡en</ts>
                  <nts id="Seg_2017" n="HIAT:ip">,</nts>
                  <nts id="Seg_2018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_2020" n="HIAT:w" s="T554">hübe</ts>
                  <nts id="Seg_2021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_2023" n="HIAT:w" s="T555">bi͡erebit</ts>
                  <nts id="Seg_2024" n="HIAT:ip">.</nts>
                  <nts id="Seg_2025" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T562" id="Seg_2027" n="HIAT:u" s="T556">
                  <ts e="T557" id="Seg_2029" n="HIAT:w" s="T556">Kirdik</ts>
                  <nts id="Seg_2030" n="HIAT:ip">,</nts>
                  <nts id="Seg_2031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_2033" n="HIAT:w" s="T557">horok</ts>
                  <nts id="Seg_2034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_2036" n="HIAT:w" s="T558">kenne</ts>
                  <nts id="Seg_2037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_2039" n="HIAT:w" s="T559">hɨːha</ts>
                  <nts id="Seg_2040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_2042" n="HIAT:w" s="T560">haŋarabɨt</ts>
                  <nts id="Seg_2043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_2045" n="HIAT:w" s="T561">bu͡olu͡oga</ts>
                  <nts id="Seg_2046" n="HIAT:ip">.</nts>
                  <nts id="Seg_2047" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T565" id="Seg_2049" n="HIAT:u" s="T562">
                  <ts e="T563" id="Seg_2051" n="HIAT:w" s="T562">Dʼaktattar</ts>
                  <nts id="Seg_2052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_2054" n="HIAT:w" s="T563">ɨtanallara</ts>
                  <nts id="Seg_2055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_2057" n="HIAT:w" s="T564">bert</ts>
                  <nts id="Seg_2058" n="HIAT:ip">.</nts>
                  <nts id="Seg_2059" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T573" id="Seg_2061" n="HIAT:u" s="T565">
                  <ts e="T566" id="Seg_2063" n="HIAT:w" s="T565">Kihi</ts>
                  <nts id="Seg_2064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_2066" n="HIAT:w" s="T566">ü͡öregin</ts>
                  <nts id="Seg_2067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2068" n="HIAT:ip">(</nts>
                  <nts id="Seg_2069" n="HIAT:ip">(</nts>
                  <ats e="T568" id="Seg_2070" n="HIAT:non-pho" s="T567">PAUSE</ats>
                  <nts id="Seg_2071" n="HIAT:ip">)</nts>
                  <nts id="Seg_2072" n="HIAT:ip">)</nts>
                  <nts id="Seg_2073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_2075" n="HIAT:w" s="T568">kihi</ts>
                  <nts id="Seg_2076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_2078" n="HIAT:w" s="T569">ü͡öregin</ts>
                  <nts id="Seg_2079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_2081" n="HIAT:w" s="T570">karɨjara</ts>
                  <nts id="Seg_2082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_2084" n="HIAT:w" s="T571">olus</ts>
                  <nts id="Seg_2085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_2087" n="HIAT:w" s="T572">hürdeːk</ts>
                  <nts id="Seg_2088" n="HIAT:ip">.</nts>
                  <nts id="Seg_2089" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T582" id="Seg_2091" n="HIAT:u" s="T573">
                  <ts e="T574" id="Seg_2093" n="HIAT:w" s="T573">Olus</ts>
                  <nts id="Seg_2094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_2096" n="HIAT:w" s="T574">kɨra</ts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2099" n="HIAT:w" s="T575">ogolorgo</ts>
                  <nts id="Seg_2100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2102" n="HIAT:w" s="T576">ulakan</ts>
                  <nts id="Seg_2103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2105" n="HIAT:w" s="T577">dʼonu</ts>
                  <nts id="Seg_2106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2108" n="HIAT:w" s="T578">targatɨ͡akka</ts>
                  <nts id="Seg_2109" n="HIAT:ip">,</nts>
                  <nts id="Seg_2110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2112" n="HIAT:w" s="T579">huptu</ts>
                  <nts id="Seg_2113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2115" n="HIAT:w" s="T580">körör</ts>
                  <nts id="Seg_2116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2118" n="HIAT:w" s="T581">bu͡olu͡oktarɨn</ts>
                  <nts id="Seg_2119" n="HIAT:ip">.</nts>
                  <nts id="Seg_2120" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T590" id="Seg_2122" n="HIAT:u" s="T582">
                  <ts e="T583" id="Seg_2124" n="HIAT:w" s="T582">Olus</ts>
                  <nts id="Seg_2125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2127" n="HIAT:w" s="T583">oččuguj</ts>
                  <nts id="Seg_2128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2130" n="HIAT:w" s="T584">ogo</ts>
                  <nts id="Seg_2131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2133" n="HIAT:w" s="T585">tugut</ts>
                  <nts id="Seg_2134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2136" n="HIAT:w" s="T586">kördük</ts>
                  <nts id="Seg_2137" n="HIAT:ip">,</nts>
                  <nts id="Seg_2138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2140" n="HIAT:w" s="T587">kanna</ts>
                  <nts id="Seg_2141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2143" n="HIAT:w" s="T588">bararɨn</ts>
                  <nts id="Seg_2144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2146" n="HIAT:w" s="T589">bilbet</ts>
                  <nts id="Seg_2147" n="HIAT:ip">.</nts>
                  <nts id="Seg_2148" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T593" id="Seg_2150" n="HIAT:u" s="T590">
                  <ts e="T591" id="Seg_2152" n="HIAT:w" s="T590">Ötü͡ötük</ts>
                  <nts id="Seg_2153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2155" n="HIAT:w" s="T591">taŋna</ts>
                  <nts id="Seg_2156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2158" n="HIAT:w" s="T592">hataːbat</ts>
                  <nts id="Seg_2159" n="HIAT:ip">.</nts>
                  <nts id="Seg_2160" n="HIAT:ip">"</nts>
                  <nts id="Seg_2161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T597" id="Seg_2163" n="HIAT:u" s="T593">
                  <nts id="Seg_2164" n="HIAT:ip">"</nts>
                  <ts e="T594" id="Seg_2166" n="HIAT:w" s="T593">Hiti</ts>
                  <nts id="Seg_2167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2169" n="HIAT:w" s="T594">kirdik</ts>
                  <nts id="Seg_2170" n="HIAT:ip">"</nts>
                  <nts id="Seg_2171" n="HIAT:ip">,</nts>
                  <nts id="Seg_2172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2174" n="HIAT:w" s="T595">di͡ebit</ts>
                  <nts id="Seg_2175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2177" n="HIAT:w" s="T596">radavoja</ts>
                  <nts id="Seg_2178" n="HIAT:ip">.</nts>
                  <nts id="Seg_2179" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T603" id="Seg_2181" n="HIAT:u" s="T597">
                  <ts e="T598" id="Seg_2183" n="HIAT:w" s="T597">Ogonnʼor</ts>
                  <nts id="Seg_2184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2186" n="HIAT:w" s="T598">bu</ts>
                  <nts id="Seg_2187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2189" n="HIAT:w" s="T599">munnʼak</ts>
                  <nts id="Seg_2190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2192" n="HIAT:w" s="T600">aːjdaːnɨgar</ts>
                  <nts id="Seg_2193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2195" n="HIAT:w" s="T601">umnan</ts>
                  <nts id="Seg_2196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2198" n="HIAT:w" s="T602">keːspit</ts>
                  <nts id="Seg_2199" n="HIAT:ip">.</nts>
                  <nts id="Seg_2200" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T612" id="Seg_2202" n="HIAT:u" s="T603">
                  <nts id="Seg_2203" n="HIAT:ip">"</nts>
                  <ts e="T604" id="Seg_2205" n="HIAT:w" s="T603">Kɨhɨn</ts>
                  <nts id="Seg_2206" n="HIAT:ip">,</nts>
                  <nts id="Seg_2207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2209" n="HIAT:w" s="T604">haŋa</ts>
                  <nts id="Seg_2210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2212" n="HIAT:w" s="T605">dʼɨl</ts>
                  <nts id="Seg_2213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2215" n="HIAT:w" s="T606">keler</ts>
                  <nts id="Seg_2216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2218" n="HIAT:w" s="T607">kemiger</ts>
                  <nts id="Seg_2219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2221" n="HIAT:w" s="T608">ogolor</ts>
                  <nts id="Seg_2222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2224" n="HIAT:w" s="T609">hɨnnʼana</ts>
                  <nts id="Seg_2225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2227" n="HIAT:w" s="T610">keli͡ektere</ts>
                  <nts id="Seg_2228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2230" n="HIAT:w" s="T611">dʼi͡eleriger</ts>
                  <nts id="Seg_2231" n="HIAT:ip">.</nts>
                  <nts id="Seg_2232" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T623" id="Seg_2234" n="HIAT:u" s="T612">
                  <ts e="T613" id="Seg_2236" n="HIAT:w" s="T612">Haːs</ts>
                  <nts id="Seg_2237" n="HIAT:ip">,</nts>
                  <nts id="Seg_2238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2240" n="HIAT:w" s="T613">maːj</ts>
                  <nts id="Seg_2241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2243" n="HIAT:w" s="T614">ɨjɨgar</ts>
                  <nts id="Seg_2244" n="HIAT:ip">,</nts>
                  <nts id="Seg_2245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2247" n="HIAT:w" s="T615">biːr</ts>
                  <nts id="Seg_2248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2250" n="HIAT:w" s="T616">dʼɨllaːgɨ</ts>
                  <nts id="Seg_2251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2253" n="HIAT:w" s="T617">ü͡örekterin</ts>
                  <nts id="Seg_2254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2256" n="HIAT:w" s="T618">büttekterine</ts>
                  <nts id="Seg_2257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2259" n="HIAT:w" s="T619">kühüŋŋe</ts>
                  <nts id="Seg_2260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2262" n="HIAT:w" s="T620">di͡eri</ts>
                  <nts id="Seg_2263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2265" n="HIAT:w" s="T621">dʼi͡eleriger</ts>
                  <nts id="Seg_2266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2268" n="HIAT:w" s="T622">bu͡olu͡oktara</ts>
                  <nts id="Seg_2269" n="HIAT:ip">.</nts>
                  <nts id="Seg_2270" n="HIAT:ip">"</nts>
                  <nts id="Seg_2271" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T629" id="Seg_2273" n="HIAT:u" s="T623">
                  <nts id="Seg_2274" n="HIAT:ip">"</nts>
                  <ts e="T624" id="Seg_2276" n="HIAT:w" s="T623">Iti</ts>
                  <nts id="Seg_2277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2279" n="HIAT:w" s="T624">öhü</ts>
                  <nts id="Seg_2280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2282" n="HIAT:w" s="T625">min</ts>
                  <nts id="Seg_2283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2285" n="HIAT:w" s="T626">barɨ</ts>
                  <nts id="Seg_2286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2288" n="HIAT:w" s="T627">kihi͡eke</ts>
                  <nts id="Seg_2289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2291" n="HIAT:w" s="T628">kepsi͡em</ts>
                  <nts id="Seg_2292" n="HIAT:ip">.</nts>
                  <nts id="Seg_2293" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T640" id="Seg_2295" n="HIAT:u" s="T629">
                  <ts e="T630" id="Seg_2297" n="HIAT:w" s="T629">Kihiler</ts>
                  <nts id="Seg_2298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2300" n="HIAT:w" s="T630">diː</ts>
                  <nts id="Seg_2301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2303" n="HIAT:w" s="T631">hanɨːllar</ts>
                  <nts id="Seg_2304" n="HIAT:ip">,</nts>
                  <nts id="Seg_2305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2307" n="HIAT:w" s="T632">kahan</ts>
                  <nts id="Seg_2308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2310" n="HIAT:w" s="T633">da</ts>
                  <nts id="Seg_2311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2313" n="HIAT:w" s="T634">tönnörbökkö</ts>
                  <nts id="Seg_2314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2316" n="HIAT:w" s="T635">ɨlallar</ts>
                  <nts id="Seg_2317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2319" n="HIAT:w" s="T636">ogoloru</ts>
                  <nts id="Seg_2320" n="HIAT:ip">"</nts>
                  <nts id="Seg_2321" n="HIAT:ip">,</nts>
                  <nts id="Seg_2322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2324" n="HIAT:w" s="T637">ü͡örbüčče</ts>
                  <nts id="Seg_2325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2327" n="HIAT:w" s="T638">ogonnʼor</ts>
                  <nts id="Seg_2328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2330" n="HIAT:w" s="T639">haŋarbɨt</ts>
                  <nts id="Seg_2331" n="HIAT:ip">.</nts>
                  <nts id="Seg_2332" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T648" id="Seg_2334" n="HIAT:u" s="T640">
                  <ts e="T641" id="Seg_2336" n="HIAT:w" s="T640">Ogonnʼor</ts>
                  <nts id="Seg_2337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2339" n="HIAT:w" s="T641">dʼi͡etiger</ts>
                  <nts id="Seg_2340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2342" n="HIAT:w" s="T642">barbɨtɨn</ts>
                  <nts id="Seg_2343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2345" n="HIAT:w" s="T643">kenne</ts>
                  <nts id="Seg_2346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2348" n="HIAT:w" s="T644">Anʼisʼim</ts>
                  <nts id="Seg_2349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2351" n="HIAT:w" s="T645">bejetin</ts>
                  <nts id="Seg_2352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2354" n="HIAT:w" s="T646">gɨtta</ts>
                  <nts id="Seg_2355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2357" n="HIAT:w" s="T647">kepseter</ts>
                  <nts id="Seg_2358" n="HIAT:ip">:</nts>
                  <nts id="Seg_2359" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T661" id="Seg_2361" n="HIAT:u" s="T648">
                  <nts id="Seg_2362" n="HIAT:ip">"</nts>
                  <ts e="T649" id="Seg_2364" n="HIAT:w" s="T648">Barɨ</ts>
                  <nts id="Seg_2365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2367" n="HIAT:w" s="T649">kihi͡eke</ts>
                  <nts id="Seg_2368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2370" n="HIAT:w" s="T650">öjdötön</ts>
                  <nts id="Seg_2371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2373" n="HIAT:w" s="T651">ihi͡ekke</ts>
                  <nts id="Seg_2374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2376" n="HIAT:w" s="T652">naːda</ts>
                  <nts id="Seg_2377" n="HIAT:ip">,</nts>
                  <nts id="Seg_2378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2380" n="HIAT:w" s="T653">kajdak</ts>
                  <nts id="Seg_2381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2383" n="HIAT:w" s="T654">haŋa</ts>
                  <nts id="Seg_2384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2386" n="HIAT:w" s="T655">ɨjaːk</ts>
                  <nts id="Seg_2387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2389" n="HIAT:w" s="T656">ötü͡önü</ts>
                  <nts id="Seg_2390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2392" n="HIAT:w" s="T657">oŋororun</ts>
                  <nts id="Seg_2393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2395" n="HIAT:w" s="T658">tɨ͡a</ts>
                  <nts id="Seg_2396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2398" n="HIAT:w" s="T659">dʼonun</ts>
                  <nts id="Seg_2399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2401" n="HIAT:w" s="T660">inniger</ts>
                  <nts id="Seg_2402" n="HIAT:ip">.</nts>
                  <nts id="Seg_2403" n="HIAT:ip">"</nts>
                  <nts id="Seg_2404" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T668" id="Seg_2406" n="HIAT:u" s="T661">
                  <ts e="T662" id="Seg_2408" n="HIAT:w" s="T661">Itinnik</ts>
                  <nts id="Seg_2409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2411" n="HIAT:w" s="T662">ogonnʼottor</ts>
                  <nts id="Seg_2412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2414" n="HIAT:w" s="T663">ös</ts>
                  <nts id="Seg_2415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2417" n="HIAT:w" s="T664">ɨːtallara</ts>
                  <nts id="Seg_2418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2420" n="HIAT:w" s="T665">kirdikteːk</ts>
                  <nts id="Seg_2421" n="HIAT:ip">,</nts>
                  <nts id="Seg_2422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2424" n="HIAT:w" s="T666">kotuːlaːk</ts>
                  <nts id="Seg_2425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2427" n="HIAT:w" s="T667">bu͡olu͡oga</ts>
                  <nts id="Seg_2428" n="HIAT:ip">.</nts>
                  <nts id="Seg_2429" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T678" id="Seg_2431" n="HIAT:u" s="T668">
                  <ts e="T669" id="Seg_2433" n="HIAT:w" s="T668">Nöŋü͡ö</ts>
                  <nts id="Seg_2434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2436" n="HIAT:w" s="T669">kün</ts>
                  <nts id="Seg_2437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2439" n="HIAT:w" s="T670">emi͡e</ts>
                  <nts id="Seg_2440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2442" n="HIAT:w" s="T671">munnʼakka</ts>
                  <nts id="Seg_2443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2445" n="HIAT:w" s="T672">kihiler</ts>
                  <nts id="Seg_2446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2448" n="HIAT:w" s="T673">komullubuttar</ts>
                  <nts id="Seg_2449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2450" n="HIAT:ip">(</nts>
                  <nts id="Seg_2451" n="HIAT:ip">(</nts>
                  <ats e="T675" id="Seg_2452" n="HIAT:non-pho" s="T674">PAUSE</ats>
                  <nts id="Seg_2453" n="HIAT:ip">)</nts>
                  <nts id="Seg_2454" n="HIAT:ip">)</nts>
                  <nts id="Seg_2455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2457" n="HIAT:w" s="T675">Hemen</ts>
                  <nts id="Seg_2458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2460" n="HIAT:w" s="T676">ogonnʼor</ts>
                  <nts id="Seg_2461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2463" n="HIAT:w" s="T677">dʼi͡etiger</ts>
                  <nts id="Seg_2464" n="HIAT:ip">.</nts>
                  <nts id="Seg_2465" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T684" id="Seg_2467" n="HIAT:u" s="T678">
                  <ts e="T679" id="Seg_2469" n="HIAT:w" s="T678">Dʼon</ts>
                  <nts id="Seg_2470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2472" n="HIAT:w" s="T679">begeheŋiteːger</ts>
                  <nts id="Seg_2473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2475" n="HIAT:w" s="T680">kajtak</ts>
                  <nts id="Seg_2476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2478" n="HIAT:w" s="T681">da</ts>
                  <nts id="Seg_2479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2481" n="HIAT:w" s="T682">ü͡örbelere</ts>
                  <nts id="Seg_2482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2484" n="HIAT:w" s="T683">kötögüllübüt</ts>
                  <nts id="Seg_2485" n="HIAT:ip">.</nts>
                  <nts id="Seg_2486" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T689" id="Seg_2488" n="HIAT:u" s="T684">
                  <ts e="T685" id="Seg_2490" n="HIAT:w" s="T684">Aːjdaːn</ts>
                  <nts id="Seg_2491" n="HIAT:ip">,</nts>
                  <nts id="Seg_2492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2494" n="HIAT:w" s="T685">külsüː-halsɨː</ts>
                  <nts id="Seg_2495" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2497" n="HIAT:w" s="T686">büteːtin</ts>
                  <nts id="Seg_2498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2500" n="HIAT:w" s="T687">radavojdara</ts>
                  <nts id="Seg_2501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2503" n="HIAT:w" s="T688">di͡ebit</ts>
                  <nts id="Seg_2504" n="HIAT:ip">:</nts>
                  <nts id="Seg_2505" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T696" id="Seg_2507" n="HIAT:u" s="T689">
                  <nts id="Seg_2508" n="HIAT:ip">"</nts>
                  <ts e="T690" id="Seg_2510" n="HIAT:w" s="T689">Bejegit</ts>
                  <nts id="Seg_2511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2513" n="HIAT:w" s="T690">hanaːgɨtɨttan</ts>
                  <nts id="Seg_2514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2516" n="HIAT:w" s="T691">kɨradaːj</ts>
                  <nts id="Seg_2517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2519" n="HIAT:w" s="T692">aːjmaktarɨ</ts>
                  <nts id="Seg_2520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2522" n="HIAT:w" s="T693">tuttarɨŋ</ts>
                  <nts id="Seg_2523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2525" n="HIAT:w" s="T694">ulakan</ts>
                  <nts id="Seg_2526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2528" n="HIAT:w" s="T695">ogolorgo</ts>
                  <nts id="Seg_2529" n="HIAT:ip">.</nts>
                  <nts id="Seg_2530" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T701" id="Seg_2532" n="HIAT:u" s="T696">
                  <ts e="T697" id="Seg_2534" n="HIAT:w" s="T696">Köhör</ts>
                  <nts id="Seg_2535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2537" n="HIAT:w" s="T697">kemŋe</ts>
                  <nts id="Seg_2538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2540" n="HIAT:w" s="T698">körör</ts>
                  <nts id="Seg_2541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2543" n="HIAT:w" s="T699">bu͡olu͡oktarɨn</ts>
                  <nts id="Seg_2544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2546" n="HIAT:w" s="T700">ginileri</ts>
                  <nts id="Seg_2547" n="HIAT:ip">.</nts>
                  <nts id="Seg_2548" n="HIAT:ip">"</nts>
                  <nts id="Seg_2549" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T707" id="Seg_2551" n="HIAT:u" s="T701">
                  <nts id="Seg_2552" n="HIAT:ip">"</nts>
                  <ts e="T702" id="Seg_2554" n="HIAT:w" s="T701">Bihi͡eke</ts>
                  <nts id="Seg_2555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2557" n="HIAT:w" s="T702">di͡ebittere</ts>
                  <nts id="Seg_2558" n="HIAT:ip">,</nts>
                  <nts id="Seg_2559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_2561" n="HIAT:w" s="T703">ogolorbut</ts>
                  <nts id="Seg_2562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2564" n="HIAT:w" s="T704">kɨhɨn</ts>
                  <nts id="Seg_2565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2567" n="HIAT:w" s="T705">hɨnnʼana</ts>
                  <nts id="Seg_2568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2570" n="HIAT:w" s="T706">keli͡ektere</ts>
                  <nts id="Seg_2571" n="HIAT:ip">.</nts>
                  <nts id="Seg_2572" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T714" id="Seg_2574" n="HIAT:u" s="T707">
                  <ts e="T708" id="Seg_2576" n="HIAT:w" s="T707">Iti</ts>
                  <nts id="Seg_2577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2579" n="HIAT:w" s="T708">kirdik</ts>
                  <nts id="Seg_2580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2582" n="HIAT:w" s="T709">haŋa</ts>
                  <nts id="Seg_2583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2585" n="HIAT:w" s="T710">du͡o</ts>
                  <nts id="Seg_2586" n="HIAT:ip">"</nts>
                  <nts id="Seg_2587" n="HIAT:ip">,</nts>
                  <nts id="Seg_2588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2590" n="HIAT:w" s="T711">ɨjɨppɨt</ts>
                  <nts id="Seg_2591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2593" n="HIAT:w" s="T712">biːr</ts>
                  <nts id="Seg_2594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2596" n="HIAT:w" s="T713">dʼaktar</ts>
                  <nts id="Seg_2597" n="HIAT:ip">.</nts>
                  <nts id="Seg_2598" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T720" id="Seg_2600" n="HIAT:u" s="T714">
                  <ts e="T715" id="Seg_2602" n="HIAT:w" s="T714">Hemen</ts>
                  <nts id="Seg_2603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2605" n="HIAT:w" s="T715">ogonnʼor</ts>
                  <nts id="Seg_2606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2608" n="HIAT:w" s="T716">oloror</ts>
                  <nts id="Seg_2609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2611" n="HIAT:w" s="T717">hiritten</ts>
                  <nts id="Seg_2612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2613" n="HIAT:ip">(</nts>
                  <ts e="T719" id="Seg_2615" n="HIAT:w" s="T718">öjdö-</ts>
                  <nts id="Seg_2616" n="HIAT:ip">)</nts>
                  <nts id="Seg_2617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2619" n="HIAT:w" s="T719">öjdöŋnöːbüt</ts>
                  <nts id="Seg_2620" n="HIAT:ip">.</nts>
                  <nts id="Seg_2621" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T723" id="Seg_2623" n="HIAT:u" s="T720">
                  <nts id="Seg_2624" n="HIAT:ip">"</nts>
                  <ts e="T721" id="Seg_2626" n="HIAT:w" s="T720">Iti</ts>
                  <nts id="Seg_2627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_2629" n="HIAT:w" s="T721">kirdik</ts>
                  <nts id="Seg_2630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_2632" n="HIAT:w" s="T722">ös</ts>
                  <nts id="Seg_2633" n="HIAT:ip">.</nts>
                  <nts id="Seg_2634" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T734" id="Seg_2636" n="HIAT:u" s="T723">
                  <ts e="T724" id="Seg_2638" n="HIAT:w" s="T723">Kɨhɨn</ts>
                  <nts id="Seg_2639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2641" n="HIAT:w" s="T724">hɨnnʼana</ts>
                  <nts id="Seg_2642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_2644" n="HIAT:w" s="T725">keli͡ektere</ts>
                  <nts id="Seg_2645" n="HIAT:ip">,</nts>
                  <nts id="Seg_2646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2648" n="HIAT:w" s="T726">onton</ts>
                  <nts id="Seg_2649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2651" n="HIAT:w" s="T727">hajɨn</ts>
                  <nts id="Seg_2652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2654" n="HIAT:w" s="T728">huptu</ts>
                  <nts id="Seg_2655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2657" n="HIAT:w" s="T729">dʼi͡eleriger</ts>
                  <nts id="Seg_2658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2660" n="HIAT:w" s="T730">bu͡ola</ts>
                  <nts id="Seg_2661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_2663" n="HIAT:w" s="T731">keli͡ektere</ts>
                  <nts id="Seg_2664" n="HIAT:ip">"</nts>
                  <nts id="Seg_2665" n="HIAT:ip">,</nts>
                  <nts id="Seg_2666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2668" n="HIAT:w" s="T732">di͡ebit</ts>
                  <nts id="Seg_2669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2671" n="HIAT:w" s="T733">Anʼisʼim</ts>
                  <nts id="Seg_2672" n="HIAT:ip">.</nts>
                  <nts id="Seg_2673" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T742" id="Seg_2675" n="HIAT:u" s="T734">
                  <ts e="T735" id="Seg_2677" n="HIAT:w" s="T734">Hemen</ts>
                  <nts id="Seg_2678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_2680" n="HIAT:w" s="T735">ogonnʼor</ts>
                  <nts id="Seg_2681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_2683" n="HIAT:w" s="T736">nalɨja</ts>
                  <nts id="Seg_2684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_2686" n="HIAT:w" s="T737">tüheːt</ts>
                  <nts id="Seg_2687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_2689" n="HIAT:w" s="T738">haŋa</ts>
                  <nts id="Seg_2690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2691" n="HIAT:ip">(</nts>
                  <ts e="T741" id="Seg_2693" n="HIAT:w" s="T739">ɨll-</ts>
                  <nts id="Seg_2694" n="HIAT:ip">)</nts>
                  <nts id="Seg_2695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_2697" n="HIAT:w" s="T741">allɨbɨt</ts>
                  <nts id="Seg_2698" n="HIAT:ip">:</nts>
                  <nts id="Seg_2699" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T745" id="Seg_2701" n="HIAT:u" s="T742">
                  <nts id="Seg_2702" n="HIAT:ip">"</nts>
                  <ts e="T743" id="Seg_2704" n="HIAT:w" s="T742">Min</ts>
                  <nts id="Seg_2705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_2707" n="HIAT:w" s="T743">tuːgu</ts>
                  <nts id="Seg_2708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_2710" n="HIAT:w" s="T744">di͡ebitimij</ts>
                  <nts id="Seg_2711" n="HIAT:ip">?</nts>
                  <nts id="Seg_2712" n="HIAT:ip">"</nts>
                  <nts id="Seg_2713" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T753" id="Seg_2715" n="HIAT:u" s="T745">
                  <ts e="T746" id="Seg_2717" n="HIAT:w" s="T745">Barɨlara</ts>
                  <nts id="Seg_2718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_2720" n="HIAT:w" s="T746">hɨlaːs</ts>
                  <nts id="Seg_2721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_2723" n="HIAT:w" s="T747">karagɨnan</ts>
                  <nts id="Seg_2724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_2726" n="HIAT:w" s="T748">körön</ts>
                  <nts id="Seg_2727" n="HIAT:ip">,</nts>
                  <nts id="Seg_2728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_2730" n="HIAT:w" s="T749">Hemen</ts>
                  <nts id="Seg_2731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_2733" n="HIAT:w" s="T750">ogonnʼor</ts>
                  <nts id="Seg_2734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_2736" n="HIAT:w" s="T751">di͡ek</ts>
                  <nts id="Seg_2737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_2739" n="HIAT:w" s="T752">ergillibitter</ts>
                  <nts id="Seg_2740" n="HIAT:ip">.</nts>
                  <nts id="Seg_2741" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T756" id="Seg_2743" n="HIAT:u" s="T753">
                  <ts e="T754" id="Seg_2745" n="HIAT:w" s="T753">Munnʼaktara</ts>
                  <nts id="Seg_2746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_2748" n="HIAT:w" s="T754">aːspɨt</ts>
                  <nts id="Seg_2749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_2751" n="HIAT:w" s="T755">türgennik</ts>
                  <nts id="Seg_2752" n="HIAT:ip">.</nts>
                  <nts id="Seg_2753" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T761" id="Seg_2755" n="HIAT:u" s="T756">
                  <ts e="T757" id="Seg_2757" n="HIAT:w" s="T756">Kihiler</ts>
                  <nts id="Seg_2758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_2760" n="HIAT:w" s="T757">uraha</ts>
                  <nts id="Seg_2761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_2763" n="HIAT:w" s="T758">dʼi͡elerin</ts>
                  <nts id="Seg_2764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_2766" n="HIAT:w" s="T759">aːjɨ</ts>
                  <nts id="Seg_2767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_2769" n="HIAT:w" s="T760">barattaːbɨttar</ts>
                  <nts id="Seg_2770" n="HIAT:ip">.</nts>
                  <nts id="Seg_2771" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T770" id="Seg_2773" n="HIAT:u" s="T761">
                  <ts e="T762" id="Seg_2775" n="HIAT:w" s="T761">Nöŋü͡ö</ts>
                  <nts id="Seg_2776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_2778" n="HIAT:w" s="T762">kühün</ts>
                  <nts id="Seg_2779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_2781" n="HIAT:w" s="T763">ü͡örderin</ts>
                  <nts id="Seg_2782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_2784" n="HIAT:w" s="T764">hi͡egileːn</ts>
                  <nts id="Seg_2785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_2787" n="HIAT:w" s="T765">baraːn</ts>
                  <nts id="Seg_2788" n="HIAT:ip">,</nts>
                  <nts id="Seg_2789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_2791" n="HIAT:w" s="T766">taba</ts>
                  <nts id="Seg_2792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T768" id="Seg_2794" n="HIAT:w" s="T767">tutuːta</ts>
                  <nts id="Seg_2795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_2797" n="HIAT:w" s="T768">bu͡olbut</ts>
                  <nts id="Seg_2798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_2800" n="HIAT:w" s="T769">köhörgö</ts>
                  <nts id="Seg_2801" n="HIAT:ip">.</nts>
                  <nts id="Seg_2802" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T778" id="Seg_2804" n="HIAT:u" s="T770">
                  <ts e="T771" id="Seg_2806" n="HIAT:w" s="T770">Dʼaktattar</ts>
                  <nts id="Seg_2807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_2809" n="HIAT:w" s="T771">dʼi͡elerin</ts>
                  <nts id="Seg_2810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_2812" n="HIAT:w" s="T772">attɨgar</ts>
                  <nts id="Seg_2813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_2815" n="HIAT:w" s="T773">ogolorun</ts>
                  <nts id="Seg_2816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_2818" n="HIAT:w" s="T774">gɨtta</ts>
                  <nts id="Seg_2819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_2821" n="HIAT:w" s="T775">holoto</ts>
                  <nts id="Seg_2822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_2824" n="HIAT:w" s="T776">hu͡oktar</ts>
                  <nts id="Seg_2825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_2827" n="HIAT:w" s="T777">komunannar</ts>
                  <nts id="Seg_2828" n="HIAT:ip">.</nts>
                  <nts id="Seg_2829" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T787" id="Seg_2831" n="HIAT:u" s="T778">
                  <ts e="T779" id="Seg_2833" n="HIAT:w" s="T778">Ogolorugar</ts>
                  <nts id="Seg_2834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_2836" n="HIAT:w" s="T779">huptu</ts>
                  <nts id="Seg_2837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_2839" n="HIAT:w" s="T780">haŋarallar</ts>
                  <nts id="Seg_2840" n="HIAT:ip">,</nts>
                  <nts id="Seg_2841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_2843" n="HIAT:w" s="T781">möŋpöt</ts>
                  <nts id="Seg_2844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_2846" n="HIAT:w" s="T782">bu͡olu͡oktarɨn</ts>
                  <nts id="Seg_2847" n="HIAT:ip">,</nts>
                  <nts id="Seg_2848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_2850" n="HIAT:w" s="T783">ulakan</ts>
                  <nts id="Seg_2851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_2853" n="HIAT:w" s="T784">ogoloru</ts>
                  <nts id="Seg_2854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T786" id="Seg_2856" n="HIAT:w" s="T785">ister</ts>
                  <nts id="Seg_2857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_2859" n="HIAT:w" s="T786">bu͡olu͡oktarɨn</ts>
                  <nts id="Seg_2860" n="HIAT:ip">.</nts>
                  <nts id="Seg_2861" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T796" id="Seg_2863" n="HIAT:u" s="T787">
                  <ts e="T788" id="Seg_2865" n="HIAT:w" s="T787">Taba</ts>
                  <nts id="Seg_2866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_2868" n="HIAT:w" s="T788">tutaːt</ts>
                  <nts id="Seg_2869" n="HIAT:ip">,</nts>
                  <nts id="Seg_2870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_2872" n="HIAT:w" s="T789">kölüneːt</ts>
                  <nts id="Seg_2873" n="HIAT:ip">,</nts>
                  <nts id="Seg_2874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_2876" n="HIAT:w" s="T790">uraha</ts>
                  <nts id="Seg_2877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_2879" n="HIAT:w" s="T791">dʼi͡e</ts>
                  <nts id="Seg_2880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_2882" n="HIAT:w" s="T792">aːjɨ</ts>
                  <nts id="Seg_2883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_2885" n="HIAT:w" s="T793">ahɨːllar</ts>
                  <nts id="Seg_2886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_2888" n="HIAT:w" s="T794">köhör</ts>
                  <nts id="Seg_2889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_2891" n="HIAT:w" s="T795">inniger</ts>
                  <nts id="Seg_2892" n="HIAT:ip">.</nts>
                  <nts id="Seg_2893" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T807" id="Seg_2895" n="HIAT:u" s="T796">
                  <ts e="T797" id="Seg_2897" n="HIAT:w" s="T796">Onton</ts>
                  <nts id="Seg_2898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_2900" n="HIAT:w" s="T797">uraha</ts>
                  <nts id="Seg_2901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_2903" n="HIAT:w" s="T798">dʼi͡e</ts>
                  <nts id="Seg_2904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_2906" n="HIAT:w" s="T799">aːjɨttan</ts>
                  <nts id="Seg_2907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_2909" n="HIAT:w" s="T800">barɨlara</ts>
                  <nts id="Seg_2910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_2912" n="HIAT:w" s="T801">bargɨhan</ts>
                  <nts id="Seg_2913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_2915" n="HIAT:w" s="T802">taksɨbɨttar</ts>
                  <nts id="Seg_2916" n="HIAT:ip">,</nts>
                  <nts id="Seg_2917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_2919" n="HIAT:w" s="T803">ɨraːk</ts>
                  <nts id="Seg_2920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_2922" n="HIAT:w" s="T804">barar</ts>
                  <nts id="Seg_2923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_2925" n="HIAT:w" s="T805">ogoloru</ts>
                  <nts id="Seg_2926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_2928" n="HIAT:w" s="T806">ataːraːrɨ</ts>
                  <nts id="Seg_2929" n="HIAT:ip">.</nts>
                  <nts id="Seg_2930" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T813" id="Seg_2932" n="HIAT:u" s="T807">
                  <nts id="Seg_2933" n="HIAT:ip">"</nts>
                  <ts e="T808" id="Seg_2935" n="HIAT:w" s="T807">Kahan</ts>
                  <nts id="Seg_2936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_2938" n="HIAT:w" s="T808">min</ts>
                  <nts id="Seg_2939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_2941" n="HIAT:w" s="T809">enʼigin</ts>
                  <nts id="Seg_2942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_2944" n="HIAT:w" s="T810">körü͡ömüj</ts>
                  <nts id="Seg_2945" n="HIAT:ip">"</nts>
                  <nts id="Seg_2946" n="HIAT:ip">,</nts>
                  <nts id="Seg_2947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_2949" n="HIAT:w" s="T811">Dʼebgeːn</ts>
                  <nts id="Seg_2950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T813" id="Seg_2952" n="HIAT:w" s="T812">ɨtaːbɨt</ts>
                  <nts id="Seg_2953" n="HIAT:ip">.</nts>
                  <nts id="Seg_2954" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T820" id="Seg_2956" n="HIAT:u" s="T813">
                  <nts id="Seg_2957" n="HIAT:ip">"</nts>
                  <ts e="T814" id="Seg_2959" n="HIAT:w" s="T813">Kajtak</ts>
                  <nts id="Seg_2960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_2962" n="HIAT:w" s="T814">enʼigin</ts>
                  <nts id="Seg_2963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2964" n="HIAT:ip">(</nts>
                  <ts e="T817" id="Seg_2966" n="HIAT:w" s="T815">hu͡o-</ts>
                  <nts id="Seg_2967" n="HIAT:ip">)</nts>
                  <nts id="Seg_2968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_2970" n="HIAT:w" s="T817">enʼigine</ts>
                  <nts id="Seg_2971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_2973" n="HIAT:w" s="T818">hu͡ok</ts>
                  <nts id="Seg_2974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_2976" n="HIAT:w" s="T819">oloru͡omuj</ts>
                  <nts id="Seg_2977" n="HIAT:ip">?</nts>
                  <nts id="Seg_2978" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T823" id="Seg_2980" n="HIAT:u" s="T820">
                  <ts e="T821" id="Seg_2982" n="HIAT:w" s="T820">Kim</ts>
                  <nts id="Seg_2983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_2985" n="HIAT:w" s="T821">bihi͡eke</ts>
                  <nts id="Seg_2986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_2988" n="HIAT:w" s="T822">kömölöhü͡ögej</ts>
                  <nts id="Seg_2989" n="HIAT:ip">?</nts>
                  <nts id="Seg_2990" n="HIAT:ip">"</nts>
                  <nts id="Seg_2991" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T830" id="Seg_2993" n="HIAT:u" s="T823">
                  <ts e="T824" id="Seg_2995" n="HIAT:w" s="T823">Ogotun</ts>
                  <nts id="Seg_2996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_2998" n="HIAT:w" s="T824">kam</ts>
                  <nts id="Seg_2999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_3001" n="HIAT:w" s="T825">tutaːt</ts>
                  <nts id="Seg_3002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_3004" n="HIAT:w" s="T826">baraːn</ts>
                  <nts id="Seg_3005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_3007" n="HIAT:w" s="T827">dʼi͡etin</ts>
                  <nts id="Seg_3008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_3010" n="HIAT:w" s="T828">di͡ek</ts>
                  <nts id="Seg_3011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_3013" n="HIAT:w" s="T829">hohor</ts>
                  <nts id="Seg_3014" n="HIAT:ip">.</nts>
                  <nts id="Seg_3015" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T838" id="Seg_3017" n="HIAT:u" s="T830">
                  <nts id="Seg_3018" n="HIAT:ip">"</nts>
                  <ts e="T831" id="Seg_3020" n="HIAT:w" s="T830">En</ts>
                  <nts id="Seg_3021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_3023" n="HIAT:w" s="T831">tu͡ok</ts>
                  <nts id="Seg_3024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_3026" n="HIAT:w" s="T832">bu͡olagɨn</ts>
                  <nts id="Seg_3027" n="HIAT:ip">"</nts>
                  <nts id="Seg_3028" n="HIAT:ip">,</nts>
                  <nts id="Seg_3029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_3031" n="HIAT:w" s="T833">biːr</ts>
                  <nts id="Seg_3032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_3034" n="HIAT:w" s="T834">biːr</ts>
                  <nts id="Seg_3035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_3037" n="HIAT:w" s="T835">dʼaktar</ts>
                  <nts id="Seg_3038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T837" id="Seg_3040" n="HIAT:w" s="T836">di͡ebit</ts>
                  <nts id="Seg_3041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_3043" n="HIAT:w" s="T837">dogorugar</ts>
                  <nts id="Seg_3044" n="HIAT:ip">.</nts>
                  <nts id="Seg_3045" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T843" id="Seg_3047" n="HIAT:u" s="T838">
                  <nts id="Seg_3048" n="HIAT:ip">"</nts>
                  <ts e="T839" id="Seg_3050" n="HIAT:w" s="T838">Ogonu</ts>
                  <nts id="Seg_3051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3052" n="HIAT:ip">(</nts>
                  <ts e="T840" id="Seg_3054" n="HIAT:w" s="T839">hanaːrga</ts>
                  <nts id="Seg_3055" n="HIAT:ip">)</nts>
                  <nts id="Seg_3056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T841" id="Seg_3058" n="HIAT:w" s="T840">battɨma</ts>
                  <nts id="Seg_3059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_3061" n="HIAT:w" s="T841">köhör</ts>
                  <nts id="Seg_3062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_3064" n="HIAT:w" s="T842">inniger</ts>
                  <nts id="Seg_3065" n="HIAT:ip">.</nts>
                  <nts id="Seg_3066" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T847" id="Seg_3068" n="HIAT:u" s="T843">
                  <ts e="T844" id="Seg_3070" n="HIAT:w" s="T843">Itigirdik</ts>
                  <nts id="Seg_3071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_3073" n="HIAT:w" s="T844">ataːrar</ts>
                  <nts id="Seg_3074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_3076" n="HIAT:w" s="T845">olus</ts>
                  <nts id="Seg_3077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_3079" n="HIAT:w" s="T846">kuhagan</ts>
                  <nts id="Seg_3080" n="HIAT:ip">.</nts>
                  <nts id="Seg_3081" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T850" id="Seg_3083" n="HIAT:u" s="T847">
                  <ts e="T848" id="Seg_3085" n="HIAT:w" s="T847">Anʼɨː</ts>
                  <nts id="Seg_3086" n="HIAT:ip">,</nts>
                  <nts id="Seg_3087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T849" id="Seg_3089" n="HIAT:w" s="T848">ataːr</ts>
                  <nts id="Seg_3090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_3092" n="HIAT:w" s="T849">kihiliː</ts>
                  <nts id="Seg_3093" n="HIAT:ip">.</nts>
                  <nts id="Seg_3094" n="HIAT:ip">"</nts>
                  <nts id="Seg_3095" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T859" id="Seg_3097" n="HIAT:u" s="T850">
                  <ts e="T851" id="Seg_3099" n="HIAT:w" s="T850">Barɨ</ts>
                  <nts id="Seg_3100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_3102" n="HIAT:w" s="T851">uːčaktaːk</ts>
                  <nts id="Seg_3103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_3105" n="HIAT:w" s="T852">ogolor</ts>
                  <nts id="Seg_3106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_3108" n="HIAT:w" s="T853">ehelerin</ts>
                  <nts id="Seg_3109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T855" id="Seg_3111" n="HIAT:w" s="T854">kennitten</ts>
                  <nts id="Seg_3112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_3114" n="HIAT:w" s="T855">baran</ts>
                  <nts id="Seg_3115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T857" id="Seg_3117" n="HIAT:w" s="T856">uhun</ts>
                  <nts id="Seg_3118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T858" id="Seg_3120" n="HIAT:w" s="T857">kös</ts>
                  <nts id="Seg_3121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T859" id="Seg_3123" n="HIAT:w" s="T858">bu͡olbuttar</ts>
                  <nts id="Seg_3124" n="HIAT:ip">.</nts>
                  <nts id="Seg_3125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T866" id="Seg_3127" n="HIAT:u" s="T859">
                  <ts e="T860" id="Seg_3129" n="HIAT:w" s="T859">Turbat-turar</ts>
                  <nts id="Seg_3130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_3132" n="HIAT:w" s="T860">ogolorun</ts>
                  <nts id="Seg_3133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T862" id="Seg_3135" n="HIAT:w" s="T861">ataːran</ts>
                  <nts id="Seg_3136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T863" id="Seg_3138" n="HIAT:w" s="T862">ol</ts>
                  <nts id="Seg_3139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864" id="Seg_3141" n="HIAT:w" s="T863">kös</ts>
                  <nts id="Seg_3142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T865" id="Seg_3144" n="HIAT:w" s="T864">kennitten</ts>
                  <nts id="Seg_3145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_3147" n="HIAT:w" s="T865">huburujbuttar</ts>
                  <nts id="Seg_3148" n="HIAT:ip">.</nts>
                  <nts id="Seg_3149" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T876" id="Seg_3151" n="HIAT:u" s="T866">
                  <ts e="T867" id="Seg_3153" n="HIAT:w" s="T866">Parfʼiraj</ts>
                  <nts id="Seg_3154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T868" id="Seg_3156" n="HIAT:w" s="T867">ogonnʼor</ts>
                  <nts id="Seg_3157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T869" id="Seg_3159" n="HIAT:w" s="T868">karaktarɨn</ts>
                  <nts id="Seg_3160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T870" id="Seg_3162" n="HIAT:w" s="T869">bɨlčaččɨ</ts>
                  <nts id="Seg_3163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T871" id="Seg_3165" n="HIAT:w" s="T870">körön</ts>
                  <nts id="Seg_3166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_3168" n="HIAT:w" s="T871">turan</ts>
                  <nts id="Seg_3169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873" id="Seg_3171" n="HIAT:w" s="T872">ele</ts>
                  <nts id="Seg_3172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874" id="Seg_3174" n="HIAT:w" s="T873">küːhünen</ts>
                  <nts id="Seg_3175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T875" id="Seg_3177" n="HIAT:w" s="T874">algɨː</ts>
                  <nts id="Seg_3178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_3180" n="HIAT:w" s="T875">turbut</ts>
                  <nts id="Seg_3181" n="HIAT:ip">.</nts>
                  <nts id="Seg_3182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T884" id="Seg_3184" n="HIAT:u" s="T876">
                  <nts id="Seg_3185" n="HIAT:ip">"</nts>
                  <ts e="T877" id="Seg_3187" n="HIAT:w" s="T876">Dʼe</ts>
                  <nts id="Seg_3188" n="HIAT:ip">,</nts>
                  <nts id="Seg_3189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878" id="Seg_3191" n="HIAT:w" s="T877">eteŋŋe</ts>
                  <nts id="Seg_3192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T879" id="Seg_3194" n="HIAT:w" s="T878">hɨldʼɨŋ</ts>
                  <nts id="Seg_3195" n="HIAT:ip">,</nts>
                  <nts id="Seg_3196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T880" id="Seg_3198" n="HIAT:w" s="T879">ehigi</ts>
                  <nts id="Seg_3199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T881" id="Seg_3201" n="HIAT:w" s="T880">hu͡olgutugar</ts>
                  <nts id="Seg_3202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T882" id="Seg_3204" n="HIAT:w" s="T881">kim</ts>
                  <nts id="Seg_3205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T883" id="Seg_3207" n="HIAT:w" s="T882">da</ts>
                  <nts id="Seg_3208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T884" id="Seg_3210" n="HIAT:w" s="T883">haːrappatɨn</ts>
                  <nts id="Seg_3211" n="HIAT:ip">.</nts>
                  <nts id="Seg_3212" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T887" id="Seg_3214" n="HIAT:u" s="T884">
                  <ts e="T885" id="Seg_3216" n="HIAT:w" s="T884">Ulakan</ts>
                  <nts id="Seg_3217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T886" id="Seg_3219" n="HIAT:w" s="T885">öjü</ts>
                  <nts id="Seg_3220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T887" id="Seg_3222" n="HIAT:w" s="T886">ɨlɨŋ</ts>
                  <nts id="Seg_3223" n="HIAT:ip">.</nts>
                  <nts id="Seg_3224" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T891" id="Seg_3226" n="HIAT:u" s="T887">
                  <ts e="T888" id="Seg_3228" n="HIAT:w" s="T887">Türgennik</ts>
                  <nts id="Seg_3229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T889" id="Seg_3231" n="HIAT:w" s="T888">tönnüŋ</ts>
                  <nts id="Seg_3232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890" id="Seg_3234" n="HIAT:w" s="T889">töröːbüt</ts>
                  <nts id="Seg_3235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T891" id="Seg_3237" n="HIAT:w" s="T890">hirgitiger</ts>
                  <nts id="Seg_3238" n="HIAT:ip">!</nts>
                  <nts id="Seg_3239" n="HIAT:ip">"</nts>
                  <nts id="Seg_3240" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T906" id="Seg_3242" n="HIAT:u" s="T891">
                  <ts e="T892" id="Seg_3244" n="HIAT:w" s="T891">Kaːlɨː</ts>
                  <nts id="Seg_3245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T893" id="Seg_3247" n="HIAT:w" s="T892">dʼon</ts>
                  <nts id="Seg_3248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T894" id="Seg_3250" n="HIAT:w" s="T893">öːr</ts>
                  <nts id="Seg_3251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T895" id="Seg_3253" n="HIAT:w" s="T894">bagajɨ</ts>
                  <nts id="Seg_3254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T896" id="Seg_3256" n="HIAT:w" s="T895">körö</ts>
                  <nts id="Seg_3257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T897" id="Seg_3259" n="HIAT:w" s="T896">turbuttar</ts>
                  <nts id="Seg_3260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898" id="Seg_3262" n="HIAT:w" s="T897">mu͡ora</ts>
                  <nts id="Seg_3263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T899" id="Seg_3265" n="HIAT:w" s="T898">ürdünen</ts>
                  <nts id="Seg_3266" n="HIAT:ip">,</nts>
                  <nts id="Seg_3267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T900" id="Seg_3269" n="HIAT:w" s="T899">uhun</ts>
                  <nts id="Seg_3270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T901" id="Seg_3272" n="HIAT:w" s="T900">kös</ts>
                  <nts id="Seg_3273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T902" id="Seg_3275" n="HIAT:w" s="T901">hütü͡ör</ts>
                  <nts id="Seg_3276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T903" id="Seg_3278" n="HIAT:w" s="T902">di͡eri</ts>
                  <nts id="Seg_3279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T904" id="Seg_3281" n="HIAT:w" s="T903">his</ts>
                  <nts id="Seg_3282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T905" id="Seg_3284" n="HIAT:w" s="T904">ɨnaraː</ts>
                  <nts id="Seg_3285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T906" id="Seg_3287" n="HIAT:w" s="T905">öttüger</ts>
                  <nts id="Seg_3288" n="HIAT:ip">.</nts>
                  <nts id="Seg_3289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T914" id="Seg_3291" n="HIAT:u" s="T906">
                  <ts e="T907" id="Seg_3293" n="HIAT:w" s="T906">Ogo</ts>
                  <nts id="Seg_3294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T908" id="Seg_3296" n="HIAT:w" s="T907">aːjmakka</ts>
                  <nts id="Seg_3297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909" id="Seg_3299" n="HIAT:w" s="T908">törüttenne</ts>
                  <nts id="Seg_3300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T910" id="Seg_3302" n="HIAT:w" s="T909">haŋa</ts>
                  <nts id="Seg_3303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T911" id="Seg_3305" n="HIAT:w" s="T910">üje</ts>
                  <nts id="Seg_3306" n="HIAT:ip">,</nts>
                  <nts id="Seg_3307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T912" id="Seg_3309" n="HIAT:w" s="T911">ü͡örener</ts>
                  <nts id="Seg_3310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T913" id="Seg_3312" n="HIAT:w" s="T912">üje</ts>
                  <nts id="Seg_3313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T914" id="Seg_3315" n="HIAT:w" s="T913">hurukka</ts>
                  <nts id="Seg_3316" n="HIAT:ip">.</nts>
                  <nts id="Seg_3317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T920" id="Seg_3319" n="HIAT:u" s="T914">
                  <ts e="T915" id="Seg_3321" n="HIAT:w" s="T914">Ehigi</ts>
                  <nts id="Seg_3322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T916" id="Seg_3324" n="HIAT:w" s="T915">ihilleːtigit</ts>
                  <nts id="Seg_3325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T917" id="Seg_3327" n="HIAT:w" s="T916">öhü</ts>
                  <nts id="Seg_3328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_3329" n="HIAT:ip">"</nts>
                  <ts e="T918" id="Seg_3331" n="HIAT:w" s="T917">Valačanka</ts>
                  <nts id="Seg_3332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T919" id="Seg_3334" n="HIAT:w" s="T918">di͡ek</ts>
                  <nts id="Seg_3335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T920" id="Seg_3337" n="HIAT:w" s="T919">köhüː</ts>
                  <nts id="Seg_3338" n="HIAT:ip">"</nts>
                  <nts id="Seg_3339" n="HIAT:ip">.</nts>
                  <nts id="Seg_3340" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T922" id="Seg_3342" n="HIAT:u" s="T920">
                  <ts e="T921" id="Seg_3344" n="HIAT:w" s="T920">Aːkpɨta</ts>
                  <nts id="Seg_3345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T922" id="Seg_3347" n="HIAT:w" s="T921">Papov</ts>
                  <nts id="Seg_3348" n="HIAT:ip">.</nts>
                  <nts id="Seg_3349" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T922" id="Seg_3350" n="sc" s="T0">
               <ts e="T1" id="Seg_3352" n="e" s="T0">Valačanka </ts>
               <ts e="T2" id="Seg_3354" n="e" s="T1">di͡ek </ts>
               <ts e="T3" id="Seg_3356" n="e" s="T2">köhüː. </ts>
               <ts e="T4" id="Seg_3358" n="e" s="T3">ɨrgakta </ts>
               <ts e="T5" id="Seg_3360" n="e" s="T4">ɨja </ts>
               <ts e="T6" id="Seg_3362" n="e" s="T5">baranar </ts>
               <ts e="T7" id="Seg_3364" n="e" s="T6">künnere </ts>
               <ts e="T8" id="Seg_3366" n="e" s="T7">turbuttara </ts>
               <ts e="T9" id="Seg_3368" n="e" s="T8">itiː </ts>
               <ts e="T10" id="Seg_3370" n="e" s="T9">bagajɨ. </ts>
               <ts e="T11" id="Seg_3372" n="e" s="T10">Ottor, </ts>
               <ts e="T12" id="Seg_3374" n="e" s="T11">hebirdekter </ts>
               <ts e="T13" id="Seg_3376" n="e" s="T12">kagdʼarɨjbɨttar. </ts>
               <ts e="T14" id="Seg_3378" n="e" s="T13">Mu͡ora </ts>
               <ts e="T15" id="Seg_3380" n="e" s="T14">onno-manna </ts>
               <ts e="T16" id="Seg_3382" n="e" s="T15">kɨhɨl </ts>
               <ts e="T17" id="Seg_3384" n="e" s="T16">kömüs </ts>
               <ts e="T18" id="Seg_3386" n="e" s="T17">bu͡olan </ts>
               <ts e="T19" id="Seg_3388" n="e" s="T18">ispit. </ts>
               <ts e="T20" id="Seg_3390" n="e" s="T19">Ol </ts>
               <ts e="T21" id="Seg_3392" n="e" s="T20">kütterge </ts>
               <ts e="T22" id="Seg_3394" n="e" s="T21">biliːleːk </ts>
               <ts e="T23" id="Seg_3396" n="e" s="T22">ulakan </ts>
               <ts e="T24" id="Seg_3398" n="e" s="T23">kü͡öl </ts>
               <ts e="T25" id="Seg_3400" n="e" s="T24">kɨtɨlɨgar </ts>
               <ts e="T26" id="Seg_3402" n="e" s="T25">ulakan </ts>
               <ts e="T27" id="Seg_3404" n="e" s="T26">komuːnu </ts>
               <ts e="T28" id="Seg_3406" n="e" s="T27">turbuta: </ts>
               <ts e="T29" id="Seg_3408" n="e" s="T28">Ogo </ts>
               <ts e="T30" id="Seg_3410" n="e" s="T29">aːjmagɨ </ts>
               <ts e="T31" id="Seg_3412" n="e" s="T30">belenniːller </ts>
               <ts e="T32" id="Seg_3414" n="e" s="T31">Valačankaga </ts>
               <ts e="T33" id="Seg_3416" n="e" s="T32">illeːri </ts>
               <ts e="T34" id="Seg_3418" n="e" s="T33">ü͡örekke. </ts>
               <ts e="T35" id="Seg_3420" n="e" s="T34">Mu͡ora </ts>
               <ts e="T36" id="Seg_3422" n="e" s="T35">nehili͡ege </ts>
               <ts e="T37" id="Seg_3424" n="e" s="T36">urut </ts>
               <ts e="T38" id="Seg_3426" n="e" s="T37">bilbet </ts>
               <ts e="T39" id="Seg_3428" n="e" s="T38">etilere </ts>
               <ts e="T40" id="Seg_3430" n="e" s="T39">ogolorun </ts>
               <ts e="T41" id="Seg_3432" n="e" s="T40">ü͡örekke </ts>
               <ts e="T42" id="Seg_3434" n="e" s="T41">ɨːtarɨ. </ts>
               <ts e="T43" id="Seg_3436" n="e" s="T42">Ol </ts>
               <ts e="T44" id="Seg_3438" n="e" s="T43">ihin </ts>
               <ts e="T45" id="Seg_3440" n="e" s="T44">hanaːlara </ts>
               <ts e="T46" id="Seg_3442" n="e" s="T45">barbat </ts>
               <ts e="T47" id="Seg_3444" n="e" s="T46">osku͡olaga </ts>
               <ts e="T48" id="Seg_3446" n="e" s="T47">bi͡ererge. </ts>
               <ts e="T49" id="Seg_3448" n="e" s="T48">Bütte </ts>
               <ts e="T50" id="Seg_3450" n="e" s="T49">öhü </ts>
               <ts e="T51" id="Seg_3452" n="e" s="T50">bulallar </ts>
               <ts e="T52" id="Seg_3454" n="e" s="T51">ɨːtɨmaːrɨ </ts>
               <ts e="T53" id="Seg_3456" n="e" s="T52">ogolorun. </ts>
               <ts e="T54" id="Seg_3458" n="e" s="T53">Radavojdarɨgar </ts>
               <ts e="T55" id="Seg_3460" n="e" s="T54">ol </ts>
               <ts e="T56" id="Seg_3462" n="e" s="T55">künnerge </ts>
               <ts e="T57" id="Seg_3464" n="e" s="T56">olus </ts>
               <ts e="T58" id="Seg_3466" n="e" s="T57">erejdeːk </ts>
               <ts e="T59" id="Seg_3468" n="e" s="T58">üleni </ts>
               <ts e="T60" id="Seg_3470" n="e" s="T59">oŋorbuttara. </ts>
               <ts e="T61" id="Seg_3472" n="e" s="T60">Barɨ </ts>
               <ts e="T62" id="Seg_3474" n="e" s="T61">uraha </ts>
               <ts e="T63" id="Seg_3476" n="e" s="T62">dʼi͡e </ts>
               <ts e="T64" id="Seg_3478" n="e" s="T63">aːjɨ </ts>
               <ts e="T65" id="Seg_3480" n="e" s="T64">hɨldʼan </ts>
               <ts e="T66" id="Seg_3482" n="e" s="T65">kepsete </ts>
               <ts e="T67" id="Seg_3484" n="e" s="T66">hataːbɨt, </ts>
               <ts e="T68" id="Seg_3486" n="e" s="T67">ogolorun </ts>
               <ts e="T69" id="Seg_3488" n="e" s="T68">usku͡olaga </ts>
               <ts e="T70" id="Seg_3490" n="e" s="T69">bi͡eri͡ekterin, </ts>
               <ts e="T71" id="Seg_3492" n="e" s="T70">ü͡örekke </ts>
               <ts e="T72" id="Seg_3494" n="e" s="T71">ɨːtɨ͡aktarɨn. </ts>
               <ts e="T73" id="Seg_3496" n="e" s="T72">Itinten </ts>
               <ts e="T74" id="Seg_3498" n="e" s="T73">tu͡ok </ts>
               <ts e="T75" id="Seg_3500" n="e" s="T74">da </ts>
               <ts e="T76" id="Seg_3502" n="e" s="T75">taksɨbatak. </ts>
               <ts e="T77" id="Seg_3504" n="e" s="T76">Ol </ts>
               <ts e="T78" id="Seg_3506" n="e" s="T77">ihin </ts>
               <ts e="T79" id="Seg_3508" n="e" s="T78">hübe </ts>
               <ts e="T80" id="Seg_3510" n="e" s="T79">bu͡olbut – </ts>
               <ts e="T81" id="Seg_3512" n="e" s="T80">oŋoru͡orga </ts>
               <ts e="T82" id="Seg_3514" n="e" s="T81">di͡en </ts>
               <ts e="T83" id="Seg_3516" n="e" s="T82">munnʼagɨ </ts>
               <ts e="T84" id="Seg_3518" n="e" s="T83">mu͡ora </ts>
               <ts e="T85" id="Seg_3520" n="e" s="T84">ürdünen, </ts>
               <ts e="T86" id="Seg_3522" n="e" s="T85">barɨlara </ts>
               <ts e="T87" id="Seg_3524" n="e" s="T86">komullu͡oktarɨn </ts>
               <ts e="T88" id="Seg_3526" n="e" s="T87">hɨraj </ts>
               <ts e="T89" id="Seg_3528" n="e" s="T88">hɨrajga </ts>
               <ts e="T90" id="Seg_3530" n="e" s="T89">kepsetiː </ts>
               <ts e="T91" id="Seg_3532" n="e" s="T90">bu͡olu͡ogun. </ts>
               <ts e="T92" id="Seg_3534" n="e" s="T91">Biːr </ts>
               <ts e="T93" id="Seg_3536" n="e" s="T92">ulakan </ts>
               <ts e="T94" id="Seg_3538" n="e" s="T93">uraha </ts>
               <ts e="T95" id="Seg_3540" n="e" s="T94">dʼi͡ege </ts>
               <ts e="T96" id="Seg_3542" n="e" s="T95">araččɨ </ts>
               <ts e="T97" id="Seg_3544" n="e" s="T96">batan </ts>
               <ts e="T98" id="Seg_3546" n="e" s="T97">komullubuttar, </ts>
               <ts e="T99" id="Seg_3548" n="e" s="T98">turar </ts>
               <ts e="T100" id="Seg_3550" n="e" s="T99">turbat. </ts>
               <ts e="T101" id="Seg_3552" n="e" s="T100">Er </ts>
               <ts e="T102" id="Seg_3554" n="e" s="T101">kihiler </ts>
               <ts e="T103" id="Seg_3556" n="e" s="T102">tu͡ok </ts>
               <ts e="T104" id="Seg_3558" n="e" s="T103">da </ts>
               <ts e="T105" id="Seg_3560" n="e" s="T104">di͡ekterin </ts>
               <ts e="T106" id="Seg_3562" n="e" s="T105">berditten </ts>
               <ts e="T107" id="Seg_3564" n="e" s="T106">hir </ts>
               <ts e="T108" id="Seg_3566" n="e" s="T107">di͡ek </ts>
               <ts e="T109" id="Seg_3568" n="e" s="T108">agaj </ts>
               <ts e="T110" id="Seg_3570" n="e" s="T109">köröllör. </ts>
               <ts e="T111" id="Seg_3572" n="e" s="T110">Dʼaktar </ts>
               <ts e="T112" id="Seg_3574" n="e" s="T111">ajmak </ts>
               <ts e="T113" id="Seg_3576" n="e" s="T112">pɨlaːttarɨnan </ts>
               <ts e="T114" id="Seg_3578" n="e" s="T113">karaktarɨn </ts>
               <ts e="T115" id="Seg_3580" n="e" s="T114">uːlarɨn </ts>
               <ts e="T116" id="Seg_3582" n="e" s="T115">hotunallar. </ts>
               <ts e="T117" id="Seg_3584" n="e" s="T116">"Kebeler, </ts>
               <ts e="T118" id="Seg_3586" n="e" s="T117">tu͡ok </ts>
               <ts e="T119" id="Seg_3588" n="e" s="T118">bu͡olaŋŋɨt </ts>
               <ts e="T120" id="Seg_3590" n="e" s="T119">bastargɨtɨn </ts>
               <ts e="T121" id="Seg_3592" n="e" s="T120">tobuktargɨt </ts>
               <ts e="T122" id="Seg_3594" n="e" s="T121">ihiger </ts>
               <ts e="T123" id="Seg_3596" n="e" s="T122">kiːllerdigit? </ts>
               <ts e="T124" id="Seg_3598" n="e" s="T123">Togo </ts>
               <ts e="T125" id="Seg_3600" n="e" s="T124">nʼimaha </ts>
               <ts e="T126" id="Seg_3602" n="e" s="T125">hɨtagɨt", </ts>
               <ts e="T127" id="Seg_3604" n="e" s="T126">haŋarbɨt </ts>
               <ts e="T128" id="Seg_3606" n="e" s="T127">di͡en </ts>
               <ts e="T129" id="Seg_3608" n="e" s="T128">Dʼebgeːn </ts>
               <ts e="T130" id="Seg_3610" n="e" s="T129">haŋalaːk </ts>
               <ts e="T131" id="Seg_3612" n="e" s="T130">emeːksin, </ts>
               <ts e="T132" id="Seg_3614" n="e" s="T131">ü͡ögüleːn </ts>
               <ts e="T133" id="Seg_3616" n="e" s="T132">bi͡erbit. </ts>
               <ts e="T134" id="Seg_3618" n="e" s="T133">Gini </ts>
               <ts e="T135" id="Seg_3620" n="e" s="T134">oburgu </ts>
               <ts e="T136" id="Seg_3622" n="e" s="T135">balamata </ts>
               <ts e="T137" id="Seg_3624" n="e" s="T136">bert. </ts>
               <ts e="T138" id="Seg_3626" n="e" s="T137">Kimten </ts>
               <ts e="T139" id="Seg_3628" n="e" s="T138">da </ts>
               <ts e="T140" id="Seg_3630" n="e" s="T139">tolullubat. </ts>
               <ts e="T141" id="Seg_3632" n="e" s="T140">Oloror </ts>
               <ts e="T142" id="Seg_3634" n="e" s="T141">hirten </ts>
               <ts e="T143" id="Seg_3636" n="e" s="T142">orguːjakaːn </ts>
               <ts e="T144" id="Seg_3638" n="e" s="T143">turbut </ts>
               <ts e="T145" id="Seg_3640" n="e" s="T144">Parfiraj </ts>
               <ts e="T146" id="Seg_3642" n="e" s="T145">ogonnʼor. </ts>
               <ts e="T147" id="Seg_3644" n="e" s="T146">Melden </ts>
               <ts e="T148" id="Seg_3646" n="e" s="T147">meniːtin </ts>
               <ts e="T149" id="Seg_3648" n="e" s="T148">imerije </ts>
               <ts e="T150" id="Seg_3650" n="e" s="T149">tüheːt </ts>
               <ts e="T151" id="Seg_3652" n="e" s="T150">i͡edejbekke </ts>
               <ts e="T152" id="Seg_3654" n="e" s="T151">oduːlammɨt. </ts>
               <ts e="T153" id="Seg_3656" n="e" s="T152">"Kajtak </ts>
               <ts e="T154" id="Seg_3658" n="e" s="T153">di͡emij! </ts>
               <ts e="T155" id="Seg_3660" n="e" s="T154">Iti </ts>
               <ts e="T156" id="Seg_3662" n="e" s="T155">itigirdik </ts>
               <ts e="T157" id="Seg_3664" n="e" s="T156">bu͡olar. </ts>
               <ts e="T158" id="Seg_3666" n="e" s="T157">Ogolor </ts>
               <ts e="T159" id="Seg_3668" n="e" s="T158">bu </ts>
               <ts e="T160" id="Seg_3670" n="e" s="T159">haŋa </ts>
               <ts e="T161" id="Seg_3672" n="e" s="T160">üjege </ts>
               <ts e="T162" id="Seg_3674" n="e" s="T161">ü͡örenellere </ts>
               <ts e="T163" id="Seg_3676" n="e" s="T162">hin </ts>
               <ts e="T164" id="Seg_3678" n="e" s="T163">daːganɨ </ts>
               <ts e="T165" id="Seg_3680" n="e" s="T164">olus </ts>
               <ts e="T166" id="Seg_3682" n="e" s="T165">höpsöllöːk. </ts>
               <ts e="T167" id="Seg_3684" n="e" s="T166">Giniler </ts>
               <ts e="T168" id="Seg_3686" n="e" s="T167">huruksut </ts>
               <ts e="T169" id="Seg_3688" n="e" s="T168">bu͡olu͡ok </ts>
               <ts e="T170" id="Seg_3690" n="e" s="T169">tustaːktar. </ts>
               <ts e="T171" id="Seg_3692" n="e" s="T170">Bu </ts>
               <ts e="T172" id="Seg_3694" n="e" s="T171">min </ts>
               <ts e="T173" id="Seg_3696" n="e" s="T172">Ujbaːn </ts>
               <ts e="T174" id="Seg_3698" n="e" s="T173">u͡olbun </ts>
               <ts e="T175" id="Seg_3700" n="e" s="T174">oččugujuttan </ts>
               <ts e="T176" id="Seg_3702" n="e" s="T175">ü͡öreppitim </ts>
               <ts e="T177" id="Seg_3704" n="e" s="T176">balɨk, </ts>
               <ts e="T178" id="Seg_3706" n="e" s="T177">bult </ts>
               <ts e="T179" id="Seg_3708" n="e" s="T178">kapkaːnnɨ͡agɨn. </ts>
               <ts e="T180" id="Seg_3710" n="e" s="T179">Min </ts>
               <ts e="T181" id="Seg_3712" n="e" s="T180">anɨ </ts>
               <ts e="T182" id="Seg_3714" n="e" s="T181">kɨrɨjdɨm. </ts>
               <ts e="T183" id="Seg_3716" n="e" s="T182">Üleni </ts>
               <ts e="T184" id="Seg_3718" n="e" s="T183">kotummat </ts>
               <ts e="T185" id="Seg_3720" n="e" s="T184">bu͡olan </ts>
               <ts e="T186" id="Seg_3722" n="e" s="T185">erebin. </ts>
               <ts e="T187" id="Seg_3724" n="e" s="T186">Kim </ts>
               <ts e="T188" id="Seg_3726" n="e" s="T187">min </ts>
               <ts e="T189" id="Seg_3728" n="e" s="T188">kergemmin </ts>
               <ts e="T190" id="Seg_3730" n="e" s="T189">ahatɨ͡agaj? </ts>
               <ts e="T191" id="Seg_3732" n="e" s="T190">Mu͡orabɨt </ts>
               <ts e="T192" id="Seg_3734" n="e" s="T191">tɨmnɨː, </ts>
               <ts e="T193" id="Seg_3736" n="e" s="T192">olus </ts>
               <ts e="T194" id="Seg_3738" n="e" s="T193">purgaːlaːk. </ts>
               <ts e="T195" id="Seg_3740" n="e" s="T194">Küːsteːk </ts>
               <ts e="T196" id="Seg_3742" n="e" s="T195">ire </ts>
               <ts e="T197" id="Seg_3744" n="e" s="T196">ogo </ts>
               <ts e="T198" id="Seg_3746" n="e" s="T197">agaj </ts>
               <ts e="T199" id="Seg_3748" n="e" s="T198">kihi </ts>
               <ts e="T200" id="Seg_3750" n="e" s="T199">buldu </ts>
               <ts e="T201" id="Seg_3752" n="e" s="T200">bulaːččɨ. </ts>
               <ts e="T202" id="Seg_3754" n="e" s="T201">Ujbaːnɨ </ts>
               <ts e="T203" id="Seg_3756" n="e" s="T202">osku͡olaga </ts>
               <ts e="T204" id="Seg_3758" n="e" s="T203">((PAUSE)) </ts>
               <ts e="T205" id="Seg_3760" n="e" s="T204">paːhɨ </ts>
               <ts e="T206" id="Seg_3762" n="e" s="T205">kapkaːnɨ </ts>
               <ts e="T207" id="Seg_3764" n="e" s="T206">iːte </ts>
               <ts e="T208" id="Seg_3766" n="e" s="T207">ü͡öreti͡ektere </ts>
               <ts e="T209" id="Seg_3768" n="e" s="T208">hu͡oga. </ts>
               <ts e="T210" id="Seg_3770" n="e" s="T209">Gini </ts>
               <ts e="T211" id="Seg_3772" n="e" s="T210">umnu͡oga </ts>
               <ts e="T212" id="Seg_3774" n="e" s="T211">mu͡ora </ts>
               <ts e="T213" id="Seg_3776" n="e" s="T212">ületin. </ts>
               <ts e="T214" id="Seg_3778" n="e" s="T213">Dʼaktar </ts>
               <ts e="T215" id="Seg_3780" n="e" s="T214">kördük </ts>
               <ts e="T216" id="Seg_3782" n="e" s="T215">dʼi͡ege </ts>
               <ts e="T217" id="Seg_3784" n="e" s="T216">agaj </ts>
               <ts e="T218" id="Seg_3786" n="e" s="T217">oloru͡oga, </ts>
               <ts e="T219" id="Seg_3788" n="e" s="T218">tuːgu </ts>
               <ts e="T220" id="Seg_3790" n="e" s="T219">da </ts>
               <ts e="T221" id="Seg_3792" n="e" s="T220">gɨmmakka." </ts>
               <ts e="T222" id="Seg_3794" n="e" s="T221">"En, </ts>
               <ts e="T223" id="Seg_3796" n="e" s="T222">ogonnʼor, </ts>
               <ts e="T224" id="Seg_3798" n="e" s="T223">dʼaktattarɨ </ts>
               <ts e="T225" id="Seg_3800" n="e" s="T224">tɨːtɨma", </ts>
               <ts e="T226" id="Seg_3802" n="e" s="T225">emi͡e </ts>
               <ts e="T227" id="Seg_3804" n="e" s="T226">Dʼebgeːn </ts>
               <ts e="T228" id="Seg_3806" n="e" s="T227">üːgüːte </ts>
               <ts e="T229" id="Seg_3808" n="e" s="T228">ihillibit. </ts>
               <ts e="T230" id="Seg_3810" n="e" s="T229">"Kim </ts>
               <ts e="T231" id="Seg_3812" n="e" s="T230">eni͡eke </ts>
               <ts e="T232" id="Seg_3814" n="e" s="T231">ahɨ </ts>
               <ts e="T233" id="Seg_3816" n="e" s="T232">belenniːr? </ts>
               <ts e="T234" id="Seg_3818" n="e" s="T233">Dʼi͡egin </ts>
               <ts e="T235" id="Seg_3820" n="e" s="T234">itiːtik </ts>
               <ts e="T236" id="Seg_3822" n="e" s="T235">tutar, </ts>
               <ts e="T237" id="Seg_3824" n="e" s="T236">ataktargɨn, </ts>
               <ts e="T238" id="Seg_3826" n="e" s="T237">taŋaskɨn </ts>
               <ts e="T239" id="Seg_3828" n="e" s="T238">abɨraktɨːr?" </ts>
               <ts e="T240" id="Seg_3830" n="e" s="T239">"Iti </ts>
               <ts e="T241" id="Seg_3832" n="e" s="T240">kirdik, </ts>
               <ts e="T242" id="Seg_3834" n="e" s="T241">itigirdik </ts>
               <ts e="T243" id="Seg_3836" n="e" s="T242">bu͡olar", </ts>
               <ts e="T244" id="Seg_3838" n="e" s="T243">olus </ts>
               <ts e="T245" id="Seg_3840" n="e" s="T244">ürpekke </ts>
               <ts e="T246" id="Seg_3842" n="e" s="T245">Parfʼiraj </ts>
               <ts e="T247" id="Seg_3844" n="e" s="T246">haŋara </ts>
               <ts e="T248" id="Seg_3846" n="e" s="T247">turbut. </ts>
               <ts e="T249" id="Seg_3848" n="e" s="T248">"Min </ts>
               <ts e="T250" id="Seg_3850" n="e" s="T249">Kɨːčaː </ts>
               <ts e="T251" id="Seg_3852" n="e" s="T250">kɨːhɨm </ts>
               <ts e="T252" id="Seg_3854" n="e" s="T251">astɨː </ts>
               <ts e="T253" id="Seg_3856" n="e" s="T252">hatɨːr, </ts>
               <ts e="T254" id="Seg_3858" n="e" s="T253">atagɨ, </ts>
               <ts e="T255" id="Seg_3860" n="e" s="T254">taŋahɨ </ts>
               <ts e="T256" id="Seg_3862" n="e" s="T255">oŋoro </ts>
               <ts e="T257" id="Seg_3864" n="e" s="T256">hatɨːr. </ts>
               <ts e="T258" id="Seg_3866" n="e" s="T257">Osku͡olaga </ts>
               <ts e="T259" id="Seg_3868" n="e" s="T258">hin </ts>
               <ts e="T260" id="Seg_3870" n="e" s="T259">daːgɨnɨ </ts>
               <ts e="T261" id="Seg_3872" n="e" s="T260">ontutun </ts>
               <ts e="T262" id="Seg_3874" n="e" s="T261">umnu͡oga. </ts>
               <ts e="T263" id="Seg_3876" n="e" s="T262">Er </ts>
               <ts e="T264" id="Seg_3878" n="e" s="T263">kihi </ts>
               <ts e="T265" id="Seg_3880" n="e" s="T264">kördük </ts>
               <ts e="T266" id="Seg_3882" n="e" s="T265">končoŋnuː </ts>
               <ts e="T267" id="Seg_3884" n="e" s="T266">hɨldʼɨ͡aga, </ts>
               <ts e="T268" id="Seg_3886" n="e" s="T267">dʼi͡eni </ts>
               <ts e="T269" id="Seg_3888" n="e" s="T268">tögürüje. </ts>
               <ts e="T270" id="Seg_3890" n="e" s="T269">Kajtak </ts>
               <ts e="T271" id="Seg_3892" n="e" s="T270">min </ts>
               <ts e="T272" id="Seg_3894" n="e" s="T271">oloru͡omuj </ts>
               <ts e="T273" id="Seg_3896" n="e" s="T272">ogolorum </ts>
               <ts e="T274" id="Seg_3898" n="e" s="T273">bardaktarɨna?" </ts>
               <ts e="T275" id="Seg_3900" n="e" s="T274">Radavoːja </ts>
               <ts e="T276" id="Seg_3902" n="e" s="T275">Anʼisʼim </ts>
               <ts e="T277" id="Seg_3904" n="e" s="T276">Nazaravʼičʼ </ts>
               <ts e="T278" id="Seg_3906" n="e" s="T277">Papov </ts>
               <ts e="T279" id="Seg_3908" n="e" s="T278">külümnüː </ts>
               <ts e="T280" id="Seg_3910" n="e" s="T279">tüheːt </ts>
               <ts e="T281" id="Seg_3912" n="e" s="T280">ogonnʼoru </ts>
               <ts e="T282" id="Seg_3914" n="e" s="T281">toktopput. </ts>
               <ts e="T283" id="Seg_3916" n="e" s="T282">"Dʼe, </ts>
               <ts e="T284" id="Seg_3918" n="e" s="T283">ogonnʼor, </ts>
               <ts e="T285" id="Seg_3920" n="e" s="T284">en </ts>
               <ts e="T286" id="Seg_3922" n="e" s="T285">hɨlajdɨŋ </ts>
               <ts e="T287" id="Seg_3924" n="e" s="T286">bɨhɨlaːk. </ts>
               <ts e="T288" id="Seg_3926" n="e" s="T287">Oloro </ts>
               <ts e="T289" id="Seg_3928" n="e" s="T288">tüs. </ts>
               <ts e="T290" id="Seg_3930" n="e" s="T289">Atɨn </ts>
               <ts e="T291" id="Seg_3932" n="e" s="T290">dʼon </ts>
               <ts e="T292" id="Seg_3934" n="e" s="T291">haŋardɨn." </ts>
               <ts e="T293" id="Seg_3936" n="e" s="T292">Ogonnʼor </ts>
               <ts e="T294" id="Seg_3938" n="e" s="T293">tu͡ok </ts>
               <ts e="T295" id="Seg_3940" n="e" s="T294">da </ts>
               <ts e="T296" id="Seg_3942" n="e" s="T295">di͡ebekke </ts>
               <ts e="T297" id="Seg_3944" n="e" s="T296">orguːjakaːn </ts>
               <ts e="T298" id="Seg_3946" n="e" s="T297">ologugar </ts>
               <ts e="T299" id="Seg_3948" n="e" s="T298">lak </ts>
               <ts e="T300" id="Seg_3950" n="e" s="T299">gɨna </ts>
               <ts e="T301" id="Seg_3952" n="e" s="T300">olorbut, </ts>
               <ts e="T302" id="Seg_3954" n="e" s="T301">tobuktarɨttan </ts>
               <ts e="T303" id="Seg_3956" n="e" s="T302">tutta </ts>
               <ts e="T304" id="Seg_3958" n="e" s="T303">tüheːt. </ts>
               <ts e="T305" id="Seg_3960" n="e" s="T304">"Kim </ts>
               <ts e="T306" id="Seg_3962" n="e" s="T305">össü͡ö </ts>
               <ts e="T307" id="Seg_3964" n="e" s="T306">tu͡ok </ts>
               <ts e="T308" id="Seg_3966" n="e" s="T307">haŋalaːgɨj", </ts>
               <ts e="T309" id="Seg_3968" n="e" s="T308">di͡en </ts>
               <ts e="T310" id="Seg_3970" n="e" s="T309">ɨjɨppɨt </ts>
               <ts e="T311" id="Seg_3972" n="e" s="T310">aradavojdara. </ts>
               <ts e="T312" id="Seg_3974" n="e" s="T311">Kimnere </ts>
               <ts e="T313" id="Seg_3976" n="e" s="T312">ere </ts>
               <ts e="T314" id="Seg_3978" n="e" s="T313">haŋararɨn </ts>
               <ts e="T315" id="Seg_3980" n="e" s="T314">küːte </ts>
               <ts e="T316" id="Seg_3982" n="e" s="T315">hataːn, </ts>
               <ts e="T317" id="Seg_3984" n="e" s="T316">aːkpɨt, </ts>
               <ts e="T318" id="Seg_3986" n="e" s="T317">kannuk </ts>
               <ts e="T319" id="Seg_3988" n="e" s="T318">ogoloro </ts>
               <ts e="T320" id="Seg_3990" n="e" s="T319">osku͡olaga </ts>
               <ts e="T321" id="Seg_3992" n="e" s="T320">barallarɨn. </ts>
               <ts e="T322" id="Seg_3994" n="e" s="T321">Kim </ts>
               <ts e="T323" id="Seg_3996" n="e" s="T322">kaːlarɨn </ts>
               <ts e="T324" id="Seg_3998" n="e" s="T323">osku͡olaga, </ts>
               <ts e="T325" id="Seg_4000" n="e" s="T324">barbatɨn, </ts>
               <ts e="T326" id="Seg_4002" n="e" s="T325">haːstara </ts>
               <ts e="T327" id="Seg_4004" n="e" s="T326">ulakattarɨ. </ts>
               <ts e="T328" id="Seg_4006" n="e" s="T327">Ol </ts>
               <ts e="T329" id="Seg_4008" n="e" s="T328">ogolorgo </ts>
               <ts e="T330" id="Seg_4010" n="e" s="T329">tübespit </ts>
               <ts e="T331" id="Seg_4012" n="e" s="T330">Parfʼiraj </ts>
               <ts e="T332" id="Seg_4014" n="e" s="T331">ogonnʼor </ts>
               <ts e="T333" id="Seg_4016" n="e" s="T332">u͡ola </ts>
               <ts e="T334" id="Seg_4018" n="e" s="T333">Ujbaːn. </ts>
               <ts e="T335" id="Seg_4020" n="e" s="T334">"Kɨːčaːnɨ </ts>
               <ts e="T336" id="Seg_4022" n="e" s="T335">emi͡e </ts>
               <ts e="T337" id="Seg_4024" n="e" s="T336">keːhi͡ekke </ts>
               <ts e="T338" id="Seg_4026" n="e" s="T337">tustaːk. </ts>
               <ts e="T339" id="Seg_4028" n="e" s="T338">Gini </ts>
               <ts e="T340" id="Seg_4030" n="e" s="T339">haːha </ts>
               <ts e="T341" id="Seg_4032" n="e" s="T340">hin </ts>
               <ts e="T342" id="Seg_4034" n="e" s="T341">daːgɨnɨ </ts>
               <ts e="T343" id="Seg_4036" n="e" s="T342">elbek", </ts>
               <ts e="T344" id="Seg_4038" n="e" s="T343">kördömmüt </ts>
               <ts e="T345" id="Seg_4040" n="e" s="T344">Parfʼiraj </ts>
               <ts e="T346" id="Seg_4042" n="e" s="T345">ogonnʼor. </ts>
               <ts e="T347" id="Seg_4044" n="e" s="T346">"Hu͡ok, </ts>
               <ts e="T348" id="Seg_4046" n="e" s="T347">Kɨːčaː </ts>
               <ts e="T349" id="Seg_4048" n="e" s="T348">barɨ͡a </ts>
               <ts e="T350" id="Seg_4050" n="e" s="T349">hu͡oga </ts>
               <ts e="T351" id="Seg_4052" n="e" s="T350">osku͡olaga." </ts>
               <ts e="T352" id="Seg_4054" n="e" s="T351">"Ogonnʼor, </ts>
               <ts e="T353" id="Seg_4056" n="e" s="T352">kečehime, </ts>
               <ts e="T354" id="Seg_4058" n="e" s="T353">ü͡örekteːk </ts>
               <ts e="T355" id="Seg_4060" n="e" s="T354">bu͡ollagɨna </ts>
               <ts e="T356" id="Seg_4062" n="e" s="T355">er </ts>
               <ts e="T357" id="Seg_4064" n="e" s="T356">kihi </ts>
               <ts e="T358" id="Seg_4066" n="e" s="T357">hɨ͡alɨjatɨn </ts>
               <ts e="T359" id="Seg_4068" n="e" s="T358">keti͡e </ts>
               <ts e="T360" id="Seg_4070" n="e" s="T359">hu͡oga. </ts>
               <ts e="T361" id="Seg_4072" n="e" s="T360">Dʼi͡eni </ts>
               <ts e="T362" id="Seg_4074" n="e" s="T361">tögürüččü </ts>
               <ts e="T363" id="Seg_4076" n="e" s="T362">končoŋnuː </ts>
               <ts e="T364" id="Seg_4078" n="e" s="T363">hɨldʼɨ͡a </ts>
               <ts e="T365" id="Seg_4080" n="e" s="T364">hu͡oga." </ts>
               <ts e="T366" id="Seg_4082" n="e" s="T365">Munnʼakka </ts>
               <ts e="T367" id="Seg_4084" n="e" s="T366">oloror </ts>
               <ts e="T368" id="Seg_4086" n="e" s="T367">dʼon </ts>
               <ts e="T369" id="Seg_4088" n="e" s="T368">ele </ts>
               <ts e="T370" id="Seg_4090" n="e" s="T369">isterinen </ts>
               <ts e="T371" id="Seg_4092" n="e" s="T370">külsübütter. </ts>
               <ts e="T372" id="Seg_4094" n="e" s="T371">"Barar </ts>
               <ts e="T373" id="Seg_4096" n="e" s="T372">ogoloru </ts>
               <ts e="T375" id="Seg_4098" n="e" s="T373">(belin-) </ts>
               <ts e="T376" id="Seg_4100" n="e" s="T375">belenneːŋ </ts>
               <ts e="T377" id="Seg_4102" n="e" s="T376">ötü͡ötük. </ts>
               <ts e="T378" id="Seg_4104" n="e" s="T377">Taŋɨnnarɨŋ </ts>
               <ts e="T379" id="Seg_4106" n="e" s="T378">taŋaraga </ts>
               <ts e="T380" id="Seg_4108" n="e" s="T379">taŋɨnnarar </ts>
               <ts e="T381" id="Seg_4110" n="e" s="T380">kördük. </ts>
               <ts e="T382" id="Seg_4112" n="e" s="T381">Ogolorbut </ts>
               <ts e="T383" id="Seg_4114" n="e" s="T382">bejem </ts>
               <ts e="T384" id="Seg_4116" n="e" s="T383">illi͡em </ts>
               <ts e="T385" id="Seg_4118" n="e" s="T384">Valačankattan", </ts>
               <ts e="T386" id="Seg_4120" n="e" s="T385">munnʼagɨ </ts>
               <ts e="T387" id="Seg_4122" n="e" s="T386">bütere </ts>
               <ts e="T388" id="Seg_4124" n="e" s="T387">gɨna </ts>
               <ts e="T389" id="Seg_4126" n="e" s="T388">di͡ebit </ts>
               <ts e="T390" id="Seg_4128" n="e" s="T389">hübehittere. </ts>
               <ts e="T391" id="Seg_4130" n="e" s="T390">Munnʼak </ts>
               <ts e="T392" id="Seg_4132" n="e" s="T391">ahaːtɨn </ts>
               <ts e="T393" id="Seg_4134" n="e" s="T392">komunuː </ts>
               <ts e="T394" id="Seg_4136" n="e" s="T393">ajdaːna </ts>
               <ts e="T395" id="Seg_4138" n="e" s="T394">elbeːbit. </ts>
               <ts e="T396" id="Seg_4140" n="e" s="T395">Dʼaktattar </ts>
               <ts e="T397" id="Seg_4142" n="e" s="T396">matagalarɨn </ts>
               <ts e="T398" id="Seg_4144" n="e" s="T397">ihitten </ts>
               <ts e="T399" id="Seg_4146" n="e" s="T398">tahartɨːllar </ts>
               <ts e="T400" id="Seg_4148" n="e" s="T399">ogolorun </ts>
               <ts e="T401" id="Seg_4150" n="e" s="T400">ki͡eŋ, </ts>
               <ts e="T402" id="Seg_4152" n="e" s="T401">ki͡eŋ </ts>
               <ts e="T403" id="Seg_4154" n="e" s="T402">taŋastarɨn. </ts>
               <ts e="T404" id="Seg_4156" n="e" s="T403">Taŋaraga </ts>
               <ts e="T405" id="Seg_4158" n="e" s="T404">keter </ts>
               <ts e="T406" id="Seg_4160" n="e" s="T405">ɨrbaːkɨlarɨn. </ts>
               <ts e="T407" id="Seg_4162" n="e" s="T406">Horoktor </ts>
               <ts e="T408" id="Seg_4164" n="e" s="T407">köstübet </ts>
               <ts e="T409" id="Seg_4166" n="e" s="T408">hirge </ts>
               <ts e="T410" id="Seg_4168" n="e" s="T409">tigettiːller </ts>
               <ts e="T411" id="Seg_4170" n="e" s="T410">ebit </ts>
               <ts e="T412" id="Seg_4172" n="e" s="T411">oččuguj </ts>
               <ts e="T413" id="Seg_4174" n="e" s="T412">taŋaralarɨn, </ts>
               <ts e="T414" id="Seg_4176" n="e" s="T413">kiri͡esteri. </ts>
               <ts e="T415" id="Seg_4178" n="e" s="T414">Ogolorun </ts>
               <ts e="T416" id="Seg_4180" n="e" s="T415">(či͡estijdar) </ts>
               <ts e="T417" id="Seg_4182" n="e" s="T416">barɨ </ts>
               <ts e="T418" id="Seg_4184" n="e" s="T417">ele </ts>
               <ts e="T419" id="Seg_4186" n="e" s="T418">minnʼiges </ts>
               <ts e="T420" id="Seg_4188" n="e" s="T419">astarɨnan, </ts>
               <ts e="T421" id="Seg_4190" n="e" s="T420">amahannan, </ts>
               <ts e="T422" id="Seg_4192" n="e" s="T421">kagɨnan, </ts>
               <ts e="T423" id="Seg_4194" n="e" s="T422">taba </ts>
               <ts e="T424" id="Seg_4196" n="e" s="T423">tɨlɨnan. </ts>
               <ts e="T425" id="Seg_4198" n="e" s="T424">Ki͡ehe </ts>
               <ts e="T426" id="Seg_4200" n="e" s="T425">dek </ts>
               <ts e="T428" id="Seg_4202" n="e" s="T426">(rada-) </ts>
               <ts e="T429" id="Seg_4204" n="e" s="T428">radavojugar </ts>
               <ts e="T430" id="Seg_4206" n="e" s="T429">ɨ͡allana </ts>
               <ts e="T431" id="Seg_4208" n="e" s="T430">kiːrbit </ts>
               <ts e="T432" id="Seg_4210" n="e" s="T431">Hemen </ts>
               <ts e="T433" id="Seg_4212" n="e" s="T432">ogonnʼor. </ts>
               <ts e="T434" id="Seg_4214" n="e" s="T433">Togo </ts>
               <ts e="T435" id="Seg_4216" n="e" s="T434">kelbitin </ts>
               <ts e="T436" id="Seg_4218" n="e" s="T435">bɨha </ts>
               <ts e="T437" id="Seg_4220" n="e" s="T436">haŋarbat. </ts>
               <ts e="T438" id="Seg_4222" n="e" s="T437">Čaːj </ts>
               <ts e="T439" id="Seg_4224" n="e" s="T438">ihe </ts>
               <ts e="T440" id="Seg_4226" n="e" s="T439">agaj </ts>
               <ts e="T441" id="Seg_4228" n="e" s="T440">oloron </ts>
               <ts e="T442" id="Seg_4230" n="e" s="T441">di͡ebit: </ts>
               <ts e="T443" id="Seg_4232" n="e" s="T442">"Bɨlɨr </ts>
               <ts e="T444" id="Seg_4234" n="e" s="T443">ehelerbit, </ts>
               <ts e="T445" id="Seg_4236" n="e" s="T444">agalarbɨt </ts>
               <ts e="T446" id="Seg_4238" n="e" s="T445">taŋarahɨt </ts>
               <ts e="T447" id="Seg_4240" n="e" s="T446">etilere. </ts>
               <ts e="T448" id="Seg_4242" n="e" s="T447">Ulakan </ts>
               <ts e="T449" id="Seg_4244" n="e" s="T448">hajtaːŋŋa </ts>
               <ts e="T450" id="Seg_4246" n="e" s="T449">bi͡ek </ts>
               <ts e="T451" id="Seg_4248" n="e" s="T450">belektiːr </ts>
               <ts e="T452" id="Seg_4250" n="e" s="T451">ideleːk </ts>
               <ts e="T453" id="Seg_4252" n="e" s="T452">etilere. </ts>
               <ts e="T454" id="Seg_4254" n="e" s="T453">Barɨ </ts>
               <ts e="T455" id="Seg_4256" n="e" s="T454">aːjmak </ts>
               <ts e="T456" id="Seg_4258" n="e" s="T455">ötü͡ötük </ts>
               <ts e="T457" id="Seg_4260" n="e" s="T456">oloru͡oktarɨn </ts>
               <ts e="T458" id="Seg_4262" n="e" s="T457">bagarallar. </ts>
               <ts e="T459" id="Seg_4264" n="e" s="T458">Tuːgunan </ts>
               <ts e="T460" id="Seg_4266" n="e" s="T459">da </ts>
               <ts e="T461" id="Seg_4268" n="e" s="T460">muskuraːbakka, </ts>
               <ts e="T462" id="Seg_4270" n="e" s="T461">ɨ͡aldʼɨbakka. </ts>
               <ts e="T463" id="Seg_4272" n="e" s="T462">Ulakan </ts>
               <ts e="T464" id="Seg_4274" n="e" s="T463">ojuttar </ts>
               <ts e="T465" id="Seg_4276" n="e" s="T464">kihi </ts>
               <ts e="T466" id="Seg_4278" n="e" s="T465">hɨldʼar </ts>
               <ts e="T467" id="Seg_4280" n="e" s="T466">hu͡olun </ts>
               <ts e="T468" id="Seg_4282" n="e" s="T467">bilellere </ts>
               <ts e="T469" id="Seg_4284" n="e" s="T468">bert </ts>
               <ts e="T470" id="Seg_4286" n="e" s="T469">ete. </ts>
               <ts e="T471" id="Seg_4288" n="e" s="T470">Bu </ts>
               <ts e="T472" id="Seg_4290" n="e" s="T471">olokpututtan </ts>
               <ts e="T473" id="Seg_4292" n="e" s="T472">ɨraːga </ts>
               <ts e="T474" id="Seg_4294" n="e" s="T473">hu͡ok </ts>
               <ts e="T475" id="Seg_4296" n="e" s="T474">baːr </ts>
               <ts e="T476" id="Seg_4298" n="e" s="T475">ulakan </ts>
               <ts e="T477" id="Seg_4300" n="e" s="T476">haːjtan-taːs. </ts>
               <ts e="T478" id="Seg_4302" n="e" s="T477">Barɨ </ts>
               <ts e="T479" id="Seg_4304" n="e" s="T478">kihi </ts>
               <ts e="T480" id="Seg_4306" n="e" s="T479">onno </ts>
               <ts e="T481" id="Seg_4308" n="e" s="T480">belegi </ts>
               <ts e="T482" id="Seg_4310" n="e" s="T481">bi͡ereːčči. </ts>
               <ts e="T483" id="Seg_4312" n="e" s="T482">Hotoru </ts>
               <ts e="T484" id="Seg_4314" n="e" s="T483">ogolorbut </ts>
               <ts e="T485" id="Seg_4316" n="e" s="T484">atɨn </ts>
               <ts e="T486" id="Seg_4318" n="e" s="T485">hirge </ts>
               <ts e="T487" id="Seg_4320" n="e" s="T486">barallar, </ts>
               <ts e="T488" id="Seg_4322" n="e" s="T487">atɨn </ts>
               <ts e="T489" id="Seg_4324" n="e" s="T488">kihilerge." </ts>
               <ts e="T490" id="Seg_4326" n="e" s="T489">Radavoja </ts>
               <ts e="T491" id="Seg_4328" n="e" s="T490">ɨ͡aldʼɨtɨn </ts>
               <ts e="T492" id="Seg_4330" n="e" s="T491">öhün </ts>
               <ts e="T493" id="Seg_4332" n="e" s="T492">ihilliː-ihilliː </ts>
               <ts e="T494" id="Seg_4334" n="e" s="T493">ihiger </ts>
               <ts e="T495" id="Seg_4336" n="e" s="T494">diː </ts>
               <ts e="T496" id="Seg_4338" n="e" s="T495">hanɨːr: </ts>
               <ts e="T497" id="Seg_4340" n="e" s="T496">"Tu͡ok </ts>
               <ts e="T498" id="Seg_4342" n="e" s="T497">hanaːtɨnan </ts>
               <ts e="T499" id="Seg_4344" n="e" s="T498">itinnik </ts>
               <ts e="T500" id="Seg_4346" n="e" s="T499">öhü </ts>
               <ts e="T501" id="Seg_4348" n="e" s="T500">tahaːrar?" </ts>
               <ts e="T502" id="Seg_4350" n="e" s="T501">ɨ͡aldʼɨta </ts>
               <ts e="T503" id="Seg_4352" n="e" s="T502">i͡edejbekke </ts>
               <ts e="T504" id="Seg_4354" n="e" s="T503">haŋartaːn </ts>
               <ts e="T505" id="Seg_4356" n="e" s="T504">ispit. </ts>
               <ts e="T506" id="Seg_4358" n="e" s="T505">"Dʼaktattar </ts>
               <ts e="T507" id="Seg_4360" n="e" s="T506">muŋnaːktar </ts>
               <ts e="T508" id="Seg_4362" n="e" s="T507">olus </ts>
               <ts e="T509" id="Seg_4364" n="e" s="T508">hoŋguraːtɨlar, </ts>
               <ts e="T510" id="Seg_4366" n="e" s="T509">ɨtahallar. </ts>
               <ts e="T511" id="Seg_4368" n="e" s="T510">Kɨra </ts>
               <ts e="T512" id="Seg_4370" n="e" s="T511">ogonu </ts>
               <ts e="T513" id="Seg_4372" n="e" s="T512">dʼi͡etten </ts>
               <ts e="T514" id="Seg_4374" n="e" s="T513">bɨldʼɨːr </ts>
               <ts e="T515" id="Seg_4376" n="e" s="T514">ide </ts>
               <ts e="T516" id="Seg_4378" n="e" s="T515">olus </ts>
               <ts e="T517" id="Seg_4380" n="e" s="T516">anʼɨːlaːk. </ts>
               <ts e="T518" id="Seg_4382" n="e" s="T517">Dʼon </ts>
               <ts e="T519" id="Seg_4384" n="e" s="T518">olus </ts>
               <ts e="T520" id="Seg_4386" n="e" s="T519">hanaːrgɨːr. </ts>
               <ts e="T521" id="Seg_4388" n="e" s="T520">Badaga, </ts>
               <ts e="T522" id="Seg_4390" n="e" s="T521">hajtaːŋŋa </ts>
               <ts e="T523" id="Seg_4392" n="e" s="T522">belek </ts>
               <ts e="T524" id="Seg_4394" n="e" s="T523">bi͡eri͡ekke." </ts>
               <ts e="T525" id="Seg_4396" n="e" s="T524">"Dʼe, </ts>
               <ts e="T526" id="Seg_4398" n="e" s="T525">ogonnʼor, </ts>
               <ts e="T527" id="Seg_4400" n="e" s="T526">minigin </ts>
               <ts e="T528" id="Seg_4402" n="e" s="T527">hiːleːme, </ts>
               <ts e="T529" id="Seg_4404" n="e" s="T528">itini </ts>
               <ts e="T530" id="Seg_4406" n="e" s="T529">gɨnɨ͡akpɨt </ts>
               <ts e="T531" id="Seg_4408" n="e" s="T530">hu͡oga. </ts>
               <ts e="T532" id="Seg_4410" n="e" s="T531">Usku͡ola </ts>
               <ts e="T533" id="Seg_4412" n="e" s="T532">hajtaːnɨ </ts>
               <ts e="T534" id="Seg_4414" n="e" s="T533">kördöːböt. </ts>
               <ts e="T535" id="Seg_4416" n="e" s="T534">Itigirdik </ts>
               <ts e="T536" id="Seg_4418" n="e" s="T535">barɨ </ts>
               <ts e="T537" id="Seg_4420" n="e" s="T536">kihi͡eke </ts>
               <ts e="T538" id="Seg_4422" n="e" s="T537">kepseː, </ts>
               <ts e="T539" id="Seg_4424" n="e" s="T538">hanargaːbatɨnnar", </ts>
               <ts e="T540" id="Seg_4426" n="e" s="T539">di͡ebit </ts>
               <ts e="T541" id="Seg_4428" n="e" s="T540">radavoja. </ts>
               <ts e="T542" id="Seg_4430" n="e" s="T541">Ogonnʼor </ts>
               <ts e="T543" id="Seg_4432" n="e" s="T542">tu͡ok </ts>
               <ts e="T544" id="Seg_4434" n="e" s="T543">da </ts>
               <ts e="T545" id="Seg_4436" n="e" s="T544">di͡ebekke </ts>
               <ts e="T546" id="Seg_4438" n="e" s="T545">čaːskɨtɨgar </ts>
               <ts e="T547" id="Seg_4440" n="e" s="T546">čaːj </ts>
               <ts e="T548" id="Seg_4442" n="e" s="T547">kutummut. </ts>
               <ts e="T549" id="Seg_4444" n="e" s="T548">"Bihigi, </ts>
               <ts e="T550" id="Seg_4446" n="e" s="T549">ogonnʼottor, </ts>
               <ts e="T551" id="Seg_4448" n="e" s="T550">kajtak </ts>
               <ts e="T552" id="Seg_4450" n="e" s="T551">ötü͡ö </ts>
               <ts e="T553" id="Seg_4452" n="e" s="T552">bu͡olu͡on </ts>
               <ts e="T554" id="Seg_4454" n="e" s="T553">di͡en, </ts>
               <ts e="T555" id="Seg_4456" n="e" s="T554">hübe </ts>
               <ts e="T556" id="Seg_4458" n="e" s="T555">bi͡erebit. </ts>
               <ts e="T557" id="Seg_4460" n="e" s="T556">Kirdik, </ts>
               <ts e="T558" id="Seg_4462" n="e" s="T557">horok </ts>
               <ts e="T559" id="Seg_4464" n="e" s="T558">kenne </ts>
               <ts e="T560" id="Seg_4466" n="e" s="T559">hɨːha </ts>
               <ts e="T561" id="Seg_4468" n="e" s="T560">haŋarabɨt </ts>
               <ts e="T562" id="Seg_4470" n="e" s="T561">bu͡olu͡oga. </ts>
               <ts e="T563" id="Seg_4472" n="e" s="T562">Dʼaktattar </ts>
               <ts e="T564" id="Seg_4474" n="e" s="T563">ɨtanallara </ts>
               <ts e="T565" id="Seg_4476" n="e" s="T564">bert. </ts>
               <ts e="T566" id="Seg_4478" n="e" s="T565">Kihi </ts>
               <ts e="T567" id="Seg_4480" n="e" s="T566">ü͡öregin </ts>
               <ts e="T568" id="Seg_4482" n="e" s="T567">((PAUSE)) </ts>
               <ts e="T569" id="Seg_4484" n="e" s="T568">kihi </ts>
               <ts e="T570" id="Seg_4486" n="e" s="T569">ü͡öregin </ts>
               <ts e="T571" id="Seg_4488" n="e" s="T570">karɨjara </ts>
               <ts e="T572" id="Seg_4490" n="e" s="T571">olus </ts>
               <ts e="T573" id="Seg_4492" n="e" s="T572">hürdeːk. </ts>
               <ts e="T574" id="Seg_4494" n="e" s="T573">Olus </ts>
               <ts e="T575" id="Seg_4496" n="e" s="T574">kɨra </ts>
               <ts e="T576" id="Seg_4498" n="e" s="T575">ogolorgo </ts>
               <ts e="T577" id="Seg_4500" n="e" s="T576">ulakan </ts>
               <ts e="T578" id="Seg_4502" n="e" s="T577">dʼonu </ts>
               <ts e="T579" id="Seg_4504" n="e" s="T578">targatɨ͡akka, </ts>
               <ts e="T580" id="Seg_4506" n="e" s="T579">huptu </ts>
               <ts e="T581" id="Seg_4508" n="e" s="T580">körör </ts>
               <ts e="T582" id="Seg_4510" n="e" s="T581">bu͡olu͡oktarɨn. </ts>
               <ts e="T583" id="Seg_4512" n="e" s="T582">Olus </ts>
               <ts e="T584" id="Seg_4514" n="e" s="T583">oččuguj </ts>
               <ts e="T585" id="Seg_4516" n="e" s="T584">ogo </ts>
               <ts e="T586" id="Seg_4518" n="e" s="T585">tugut </ts>
               <ts e="T587" id="Seg_4520" n="e" s="T586">kördük, </ts>
               <ts e="T588" id="Seg_4522" n="e" s="T587">kanna </ts>
               <ts e="T589" id="Seg_4524" n="e" s="T588">bararɨn </ts>
               <ts e="T590" id="Seg_4526" n="e" s="T589">bilbet. </ts>
               <ts e="T591" id="Seg_4528" n="e" s="T590">Ötü͡ötük </ts>
               <ts e="T592" id="Seg_4530" n="e" s="T591">taŋna </ts>
               <ts e="T593" id="Seg_4532" n="e" s="T592">hataːbat." </ts>
               <ts e="T594" id="Seg_4534" n="e" s="T593">"Hiti </ts>
               <ts e="T595" id="Seg_4536" n="e" s="T594">kirdik", </ts>
               <ts e="T596" id="Seg_4538" n="e" s="T595">di͡ebit </ts>
               <ts e="T597" id="Seg_4540" n="e" s="T596">radavoja. </ts>
               <ts e="T598" id="Seg_4542" n="e" s="T597">Ogonnʼor </ts>
               <ts e="T599" id="Seg_4544" n="e" s="T598">bu </ts>
               <ts e="T600" id="Seg_4546" n="e" s="T599">munnʼak </ts>
               <ts e="T601" id="Seg_4548" n="e" s="T600">aːjdaːnɨgar </ts>
               <ts e="T602" id="Seg_4550" n="e" s="T601">umnan </ts>
               <ts e="T603" id="Seg_4552" n="e" s="T602">keːspit. </ts>
               <ts e="T604" id="Seg_4554" n="e" s="T603">"Kɨhɨn, </ts>
               <ts e="T605" id="Seg_4556" n="e" s="T604">haŋa </ts>
               <ts e="T606" id="Seg_4558" n="e" s="T605">dʼɨl </ts>
               <ts e="T607" id="Seg_4560" n="e" s="T606">keler </ts>
               <ts e="T608" id="Seg_4562" n="e" s="T607">kemiger </ts>
               <ts e="T609" id="Seg_4564" n="e" s="T608">ogolor </ts>
               <ts e="T610" id="Seg_4566" n="e" s="T609">hɨnnʼana </ts>
               <ts e="T611" id="Seg_4568" n="e" s="T610">keli͡ektere </ts>
               <ts e="T612" id="Seg_4570" n="e" s="T611">dʼi͡eleriger. </ts>
               <ts e="T613" id="Seg_4572" n="e" s="T612">Haːs, </ts>
               <ts e="T614" id="Seg_4574" n="e" s="T613">maːj </ts>
               <ts e="T615" id="Seg_4576" n="e" s="T614">ɨjɨgar, </ts>
               <ts e="T616" id="Seg_4578" n="e" s="T615">biːr </ts>
               <ts e="T617" id="Seg_4580" n="e" s="T616">dʼɨllaːgɨ </ts>
               <ts e="T618" id="Seg_4582" n="e" s="T617">ü͡örekterin </ts>
               <ts e="T619" id="Seg_4584" n="e" s="T618">büttekterine </ts>
               <ts e="T620" id="Seg_4586" n="e" s="T619">kühüŋŋe </ts>
               <ts e="T621" id="Seg_4588" n="e" s="T620">di͡eri </ts>
               <ts e="T622" id="Seg_4590" n="e" s="T621">dʼi͡eleriger </ts>
               <ts e="T623" id="Seg_4592" n="e" s="T622">bu͡olu͡oktara." </ts>
               <ts e="T624" id="Seg_4594" n="e" s="T623">"Iti </ts>
               <ts e="T625" id="Seg_4596" n="e" s="T624">öhü </ts>
               <ts e="T626" id="Seg_4598" n="e" s="T625">min </ts>
               <ts e="T627" id="Seg_4600" n="e" s="T626">barɨ </ts>
               <ts e="T628" id="Seg_4602" n="e" s="T627">kihi͡eke </ts>
               <ts e="T629" id="Seg_4604" n="e" s="T628">kepsi͡em. </ts>
               <ts e="T630" id="Seg_4606" n="e" s="T629">Kihiler </ts>
               <ts e="T631" id="Seg_4608" n="e" s="T630">diː </ts>
               <ts e="T632" id="Seg_4610" n="e" s="T631">hanɨːllar, </ts>
               <ts e="T633" id="Seg_4612" n="e" s="T632">kahan </ts>
               <ts e="T634" id="Seg_4614" n="e" s="T633">da </ts>
               <ts e="T635" id="Seg_4616" n="e" s="T634">tönnörbökkö </ts>
               <ts e="T636" id="Seg_4618" n="e" s="T635">ɨlallar </ts>
               <ts e="T637" id="Seg_4620" n="e" s="T636">ogoloru", </ts>
               <ts e="T638" id="Seg_4622" n="e" s="T637">ü͡örbüčče </ts>
               <ts e="T639" id="Seg_4624" n="e" s="T638">ogonnʼor </ts>
               <ts e="T640" id="Seg_4626" n="e" s="T639">haŋarbɨt. </ts>
               <ts e="T641" id="Seg_4628" n="e" s="T640">Ogonnʼor </ts>
               <ts e="T642" id="Seg_4630" n="e" s="T641">dʼi͡etiger </ts>
               <ts e="T643" id="Seg_4632" n="e" s="T642">barbɨtɨn </ts>
               <ts e="T644" id="Seg_4634" n="e" s="T643">kenne </ts>
               <ts e="T645" id="Seg_4636" n="e" s="T644">Anʼisʼim </ts>
               <ts e="T646" id="Seg_4638" n="e" s="T645">bejetin </ts>
               <ts e="T647" id="Seg_4640" n="e" s="T646">gɨtta </ts>
               <ts e="T648" id="Seg_4642" n="e" s="T647">kepseter: </ts>
               <ts e="T649" id="Seg_4644" n="e" s="T648">"Barɨ </ts>
               <ts e="T650" id="Seg_4646" n="e" s="T649">kihi͡eke </ts>
               <ts e="T651" id="Seg_4648" n="e" s="T650">öjdötön </ts>
               <ts e="T652" id="Seg_4650" n="e" s="T651">ihi͡ekke </ts>
               <ts e="T653" id="Seg_4652" n="e" s="T652">naːda, </ts>
               <ts e="T654" id="Seg_4654" n="e" s="T653">kajdak </ts>
               <ts e="T655" id="Seg_4656" n="e" s="T654">haŋa </ts>
               <ts e="T656" id="Seg_4658" n="e" s="T655">ɨjaːk </ts>
               <ts e="T657" id="Seg_4660" n="e" s="T656">ötü͡önü </ts>
               <ts e="T658" id="Seg_4662" n="e" s="T657">oŋororun </ts>
               <ts e="T659" id="Seg_4664" n="e" s="T658">tɨ͡a </ts>
               <ts e="T660" id="Seg_4666" n="e" s="T659">dʼonun </ts>
               <ts e="T661" id="Seg_4668" n="e" s="T660">inniger." </ts>
               <ts e="T662" id="Seg_4670" n="e" s="T661">Itinnik </ts>
               <ts e="T663" id="Seg_4672" n="e" s="T662">ogonnʼottor </ts>
               <ts e="T664" id="Seg_4674" n="e" s="T663">ös </ts>
               <ts e="T665" id="Seg_4676" n="e" s="T664">ɨːtallara </ts>
               <ts e="T666" id="Seg_4678" n="e" s="T665">kirdikteːk, </ts>
               <ts e="T667" id="Seg_4680" n="e" s="T666">kotuːlaːk </ts>
               <ts e="T668" id="Seg_4682" n="e" s="T667">bu͡olu͡oga. </ts>
               <ts e="T669" id="Seg_4684" n="e" s="T668">Nöŋü͡ö </ts>
               <ts e="T670" id="Seg_4686" n="e" s="T669">kün </ts>
               <ts e="T671" id="Seg_4688" n="e" s="T670">emi͡e </ts>
               <ts e="T672" id="Seg_4690" n="e" s="T671">munnʼakka </ts>
               <ts e="T673" id="Seg_4692" n="e" s="T672">kihiler </ts>
               <ts e="T674" id="Seg_4694" n="e" s="T673">komullubuttar </ts>
               <ts e="T675" id="Seg_4696" n="e" s="T674">((PAUSE)) </ts>
               <ts e="T676" id="Seg_4698" n="e" s="T675">Hemen </ts>
               <ts e="T677" id="Seg_4700" n="e" s="T676">ogonnʼor </ts>
               <ts e="T678" id="Seg_4702" n="e" s="T677">dʼi͡etiger. </ts>
               <ts e="T679" id="Seg_4704" n="e" s="T678">Dʼon </ts>
               <ts e="T680" id="Seg_4706" n="e" s="T679">begeheŋiteːger </ts>
               <ts e="T681" id="Seg_4708" n="e" s="T680">kajtak </ts>
               <ts e="T682" id="Seg_4710" n="e" s="T681">da </ts>
               <ts e="T683" id="Seg_4712" n="e" s="T682">ü͡örbelere </ts>
               <ts e="T684" id="Seg_4714" n="e" s="T683">kötögüllübüt. </ts>
               <ts e="T685" id="Seg_4716" n="e" s="T684">Aːjdaːn, </ts>
               <ts e="T686" id="Seg_4718" n="e" s="T685">külsüː-halsɨː </ts>
               <ts e="T687" id="Seg_4720" n="e" s="T686">büteːtin </ts>
               <ts e="T688" id="Seg_4722" n="e" s="T687">radavojdara </ts>
               <ts e="T689" id="Seg_4724" n="e" s="T688">di͡ebit: </ts>
               <ts e="T690" id="Seg_4726" n="e" s="T689">"Bejegit </ts>
               <ts e="T691" id="Seg_4728" n="e" s="T690">hanaːgɨtɨttan </ts>
               <ts e="T692" id="Seg_4730" n="e" s="T691">kɨradaːj </ts>
               <ts e="T693" id="Seg_4732" n="e" s="T692">aːjmaktarɨ </ts>
               <ts e="T694" id="Seg_4734" n="e" s="T693">tuttarɨŋ </ts>
               <ts e="T695" id="Seg_4736" n="e" s="T694">ulakan </ts>
               <ts e="T696" id="Seg_4738" n="e" s="T695">ogolorgo. </ts>
               <ts e="T697" id="Seg_4740" n="e" s="T696">Köhör </ts>
               <ts e="T698" id="Seg_4742" n="e" s="T697">kemŋe </ts>
               <ts e="T699" id="Seg_4744" n="e" s="T698">körör </ts>
               <ts e="T700" id="Seg_4746" n="e" s="T699">bu͡olu͡oktarɨn </ts>
               <ts e="T701" id="Seg_4748" n="e" s="T700">ginileri." </ts>
               <ts e="T702" id="Seg_4750" n="e" s="T701">"Bihi͡eke </ts>
               <ts e="T703" id="Seg_4752" n="e" s="T702">di͡ebittere, </ts>
               <ts e="T704" id="Seg_4754" n="e" s="T703">ogolorbut </ts>
               <ts e="T705" id="Seg_4756" n="e" s="T704">kɨhɨn </ts>
               <ts e="T706" id="Seg_4758" n="e" s="T705">hɨnnʼana </ts>
               <ts e="T707" id="Seg_4760" n="e" s="T706">keli͡ektere. </ts>
               <ts e="T708" id="Seg_4762" n="e" s="T707">Iti </ts>
               <ts e="T709" id="Seg_4764" n="e" s="T708">kirdik </ts>
               <ts e="T710" id="Seg_4766" n="e" s="T709">haŋa </ts>
               <ts e="T711" id="Seg_4768" n="e" s="T710">du͡o", </ts>
               <ts e="T712" id="Seg_4770" n="e" s="T711">ɨjɨppɨt </ts>
               <ts e="T713" id="Seg_4772" n="e" s="T712">biːr </ts>
               <ts e="T714" id="Seg_4774" n="e" s="T713">dʼaktar. </ts>
               <ts e="T715" id="Seg_4776" n="e" s="T714">Hemen </ts>
               <ts e="T716" id="Seg_4778" n="e" s="T715">ogonnʼor </ts>
               <ts e="T717" id="Seg_4780" n="e" s="T716">oloror </ts>
               <ts e="T718" id="Seg_4782" n="e" s="T717">hiritten </ts>
               <ts e="T719" id="Seg_4784" n="e" s="T718">(öjdö-) </ts>
               <ts e="T720" id="Seg_4786" n="e" s="T719">öjdöŋnöːbüt. </ts>
               <ts e="T721" id="Seg_4788" n="e" s="T720">"Iti </ts>
               <ts e="T722" id="Seg_4790" n="e" s="T721">kirdik </ts>
               <ts e="T723" id="Seg_4792" n="e" s="T722">ös. </ts>
               <ts e="T724" id="Seg_4794" n="e" s="T723">Kɨhɨn </ts>
               <ts e="T725" id="Seg_4796" n="e" s="T724">hɨnnʼana </ts>
               <ts e="T726" id="Seg_4798" n="e" s="T725">keli͡ektere, </ts>
               <ts e="T727" id="Seg_4800" n="e" s="T726">onton </ts>
               <ts e="T728" id="Seg_4802" n="e" s="T727">hajɨn </ts>
               <ts e="T729" id="Seg_4804" n="e" s="T728">huptu </ts>
               <ts e="T730" id="Seg_4806" n="e" s="T729">dʼi͡eleriger </ts>
               <ts e="T731" id="Seg_4808" n="e" s="T730">bu͡ola </ts>
               <ts e="T732" id="Seg_4810" n="e" s="T731">keli͡ektere", </ts>
               <ts e="T733" id="Seg_4812" n="e" s="T732">di͡ebit </ts>
               <ts e="T734" id="Seg_4814" n="e" s="T733">Anʼisʼim. </ts>
               <ts e="T735" id="Seg_4816" n="e" s="T734">Hemen </ts>
               <ts e="T736" id="Seg_4818" n="e" s="T735">ogonnʼor </ts>
               <ts e="T737" id="Seg_4820" n="e" s="T736">nalɨja </ts>
               <ts e="T738" id="Seg_4822" n="e" s="T737">tüheːt </ts>
               <ts e="T739" id="Seg_4824" n="e" s="T738">haŋa </ts>
               <ts e="T741" id="Seg_4826" n="e" s="T739">(ɨll-) </ts>
               <ts e="T742" id="Seg_4828" n="e" s="T741">allɨbɨt: </ts>
               <ts e="T743" id="Seg_4830" n="e" s="T742">"Min </ts>
               <ts e="T744" id="Seg_4832" n="e" s="T743">tuːgu </ts>
               <ts e="T745" id="Seg_4834" n="e" s="T744">di͡ebitimij?" </ts>
               <ts e="T746" id="Seg_4836" n="e" s="T745">Barɨlara </ts>
               <ts e="T747" id="Seg_4838" n="e" s="T746">hɨlaːs </ts>
               <ts e="T748" id="Seg_4840" n="e" s="T747">karagɨnan </ts>
               <ts e="T749" id="Seg_4842" n="e" s="T748">körön, </ts>
               <ts e="T750" id="Seg_4844" n="e" s="T749">Hemen </ts>
               <ts e="T751" id="Seg_4846" n="e" s="T750">ogonnʼor </ts>
               <ts e="T752" id="Seg_4848" n="e" s="T751">di͡ek </ts>
               <ts e="T753" id="Seg_4850" n="e" s="T752">ergillibitter. </ts>
               <ts e="T754" id="Seg_4852" n="e" s="T753">Munnʼaktara </ts>
               <ts e="T755" id="Seg_4854" n="e" s="T754">aːspɨt </ts>
               <ts e="T756" id="Seg_4856" n="e" s="T755">türgennik. </ts>
               <ts e="T757" id="Seg_4858" n="e" s="T756">Kihiler </ts>
               <ts e="T758" id="Seg_4860" n="e" s="T757">uraha </ts>
               <ts e="T759" id="Seg_4862" n="e" s="T758">dʼi͡elerin </ts>
               <ts e="T760" id="Seg_4864" n="e" s="T759">aːjɨ </ts>
               <ts e="T761" id="Seg_4866" n="e" s="T760">barattaːbɨttar. </ts>
               <ts e="T762" id="Seg_4868" n="e" s="T761">Nöŋü͡ö </ts>
               <ts e="T763" id="Seg_4870" n="e" s="T762">kühün </ts>
               <ts e="T764" id="Seg_4872" n="e" s="T763">ü͡örderin </ts>
               <ts e="T765" id="Seg_4874" n="e" s="T764">hi͡egileːn </ts>
               <ts e="T766" id="Seg_4876" n="e" s="T765">baraːn, </ts>
               <ts e="T767" id="Seg_4878" n="e" s="T766">taba </ts>
               <ts e="T768" id="Seg_4880" n="e" s="T767">tutuːta </ts>
               <ts e="T769" id="Seg_4882" n="e" s="T768">bu͡olbut </ts>
               <ts e="T770" id="Seg_4884" n="e" s="T769">köhörgö. </ts>
               <ts e="T771" id="Seg_4886" n="e" s="T770">Dʼaktattar </ts>
               <ts e="T772" id="Seg_4888" n="e" s="T771">dʼi͡elerin </ts>
               <ts e="T773" id="Seg_4890" n="e" s="T772">attɨgar </ts>
               <ts e="T774" id="Seg_4892" n="e" s="T773">ogolorun </ts>
               <ts e="T775" id="Seg_4894" n="e" s="T774">gɨtta </ts>
               <ts e="T776" id="Seg_4896" n="e" s="T775">holoto </ts>
               <ts e="T777" id="Seg_4898" n="e" s="T776">hu͡oktar </ts>
               <ts e="T778" id="Seg_4900" n="e" s="T777">komunannar. </ts>
               <ts e="T779" id="Seg_4902" n="e" s="T778">Ogolorugar </ts>
               <ts e="T780" id="Seg_4904" n="e" s="T779">huptu </ts>
               <ts e="T781" id="Seg_4906" n="e" s="T780">haŋarallar, </ts>
               <ts e="T782" id="Seg_4908" n="e" s="T781">möŋpöt </ts>
               <ts e="T783" id="Seg_4910" n="e" s="T782">bu͡olu͡oktarɨn, </ts>
               <ts e="T784" id="Seg_4912" n="e" s="T783">ulakan </ts>
               <ts e="T785" id="Seg_4914" n="e" s="T784">ogoloru </ts>
               <ts e="T786" id="Seg_4916" n="e" s="T785">ister </ts>
               <ts e="T787" id="Seg_4918" n="e" s="T786">bu͡olu͡oktarɨn. </ts>
               <ts e="T788" id="Seg_4920" n="e" s="T787">Taba </ts>
               <ts e="T789" id="Seg_4922" n="e" s="T788">tutaːt, </ts>
               <ts e="T790" id="Seg_4924" n="e" s="T789">kölüneːt, </ts>
               <ts e="T791" id="Seg_4926" n="e" s="T790">uraha </ts>
               <ts e="T792" id="Seg_4928" n="e" s="T791">dʼi͡e </ts>
               <ts e="T793" id="Seg_4930" n="e" s="T792">aːjɨ </ts>
               <ts e="T794" id="Seg_4932" n="e" s="T793">ahɨːllar </ts>
               <ts e="T795" id="Seg_4934" n="e" s="T794">köhör </ts>
               <ts e="T796" id="Seg_4936" n="e" s="T795">inniger. </ts>
               <ts e="T797" id="Seg_4938" n="e" s="T796">Onton </ts>
               <ts e="T798" id="Seg_4940" n="e" s="T797">uraha </ts>
               <ts e="T799" id="Seg_4942" n="e" s="T798">dʼi͡e </ts>
               <ts e="T800" id="Seg_4944" n="e" s="T799">aːjɨttan </ts>
               <ts e="T801" id="Seg_4946" n="e" s="T800">barɨlara </ts>
               <ts e="T802" id="Seg_4948" n="e" s="T801">bargɨhan </ts>
               <ts e="T803" id="Seg_4950" n="e" s="T802">taksɨbɨttar, </ts>
               <ts e="T804" id="Seg_4952" n="e" s="T803">ɨraːk </ts>
               <ts e="T805" id="Seg_4954" n="e" s="T804">barar </ts>
               <ts e="T806" id="Seg_4956" n="e" s="T805">ogoloru </ts>
               <ts e="T807" id="Seg_4958" n="e" s="T806">ataːraːrɨ. </ts>
               <ts e="T808" id="Seg_4960" n="e" s="T807">"Kahan </ts>
               <ts e="T809" id="Seg_4962" n="e" s="T808">min </ts>
               <ts e="T810" id="Seg_4964" n="e" s="T809">enʼigin </ts>
               <ts e="T811" id="Seg_4966" n="e" s="T810">körü͡ömüj", </ts>
               <ts e="T812" id="Seg_4968" n="e" s="T811">Dʼebgeːn </ts>
               <ts e="T813" id="Seg_4970" n="e" s="T812">ɨtaːbɨt. </ts>
               <ts e="T814" id="Seg_4972" n="e" s="T813">"Kajtak </ts>
               <ts e="T815" id="Seg_4974" n="e" s="T814">enʼigin </ts>
               <ts e="T817" id="Seg_4976" n="e" s="T815">(hu͡o-) </ts>
               <ts e="T818" id="Seg_4978" n="e" s="T817">enʼigine </ts>
               <ts e="T819" id="Seg_4980" n="e" s="T818">hu͡ok </ts>
               <ts e="T820" id="Seg_4982" n="e" s="T819">oloru͡omuj? </ts>
               <ts e="T821" id="Seg_4984" n="e" s="T820">Kim </ts>
               <ts e="T822" id="Seg_4986" n="e" s="T821">bihi͡eke </ts>
               <ts e="T823" id="Seg_4988" n="e" s="T822">kömölöhü͡ögej?" </ts>
               <ts e="T824" id="Seg_4990" n="e" s="T823">Ogotun </ts>
               <ts e="T825" id="Seg_4992" n="e" s="T824">kam </ts>
               <ts e="T826" id="Seg_4994" n="e" s="T825">tutaːt </ts>
               <ts e="T827" id="Seg_4996" n="e" s="T826">baraːn </ts>
               <ts e="T828" id="Seg_4998" n="e" s="T827">dʼi͡etin </ts>
               <ts e="T829" id="Seg_5000" n="e" s="T828">di͡ek </ts>
               <ts e="T830" id="Seg_5002" n="e" s="T829">hohor. </ts>
               <ts e="T831" id="Seg_5004" n="e" s="T830">"En </ts>
               <ts e="T832" id="Seg_5006" n="e" s="T831">tu͡ok </ts>
               <ts e="T833" id="Seg_5008" n="e" s="T832">bu͡olagɨn", </ts>
               <ts e="T834" id="Seg_5010" n="e" s="T833">biːr </ts>
               <ts e="T835" id="Seg_5012" n="e" s="T834">biːr </ts>
               <ts e="T836" id="Seg_5014" n="e" s="T835">dʼaktar </ts>
               <ts e="T837" id="Seg_5016" n="e" s="T836">di͡ebit </ts>
               <ts e="T838" id="Seg_5018" n="e" s="T837">dogorugar. </ts>
               <ts e="T839" id="Seg_5020" n="e" s="T838">"Ogonu </ts>
               <ts e="T840" id="Seg_5022" n="e" s="T839">(hanaːrga) </ts>
               <ts e="T841" id="Seg_5024" n="e" s="T840">battɨma </ts>
               <ts e="T842" id="Seg_5026" n="e" s="T841">köhör </ts>
               <ts e="T843" id="Seg_5028" n="e" s="T842">inniger. </ts>
               <ts e="T844" id="Seg_5030" n="e" s="T843">Itigirdik </ts>
               <ts e="T845" id="Seg_5032" n="e" s="T844">ataːrar </ts>
               <ts e="T846" id="Seg_5034" n="e" s="T845">olus </ts>
               <ts e="T847" id="Seg_5036" n="e" s="T846">kuhagan. </ts>
               <ts e="T848" id="Seg_5038" n="e" s="T847">Anʼɨː, </ts>
               <ts e="T849" id="Seg_5040" n="e" s="T848">ataːr </ts>
               <ts e="T850" id="Seg_5042" n="e" s="T849">kihiliː." </ts>
               <ts e="T851" id="Seg_5044" n="e" s="T850">Barɨ </ts>
               <ts e="T852" id="Seg_5046" n="e" s="T851">uːčaktaːk </ts>
               <ts e="T853" id="Seg_5048" n="e" s="T852">ogolor </ts>
               <ts e="T854" id="Seg_5050" n="e" s="T853">ehelerin </ts>
               <ts e="T855" id="Seg_5052" n="e" s="T854">kennitten </ts>
               <ts e="T856" id="Seg_5054" n="e" s="T855">baran </ts>
               <ts e="T857" id="Seg_5056" n="e" s="T856">uhun </ts>
               <ts e="T858" id="Seg_5058" n="e" s="T857">kös </ts>
               <ts e="T859" id="Seg_5060" n="e" s="T858">bu͡olbuttar. </ts>
               <ts e="T860" id="Seg_5062" n="e" s="T859">Turbat-turar </ts>
               <ts e="T861" id="Seg_5064" n="e" s="T860">ogolorun </ts>
               <ts e="T862" id="Seg_5066" n="e" s="T861">ataːran </ts>
               <ts e="T863" id="Seg_5068" n="e" s="T862">ol </ts>
               <ts e="T864" id="Seg_5070" n="e" s="T863">kös </ts>
               <ts e="T865" id="Seg_5072" n="e" s="T864">kennitten </ts>
               <ts e="T866" id="Seg_5074" n="e" s="T865">huburujbuttar. </ts>
               <ts e="T867" id="Seg_5076" n="e" s="T866">Parfʼiraj </ts>
               <ts e="T868" id="Seg_5078" n="e" s="T867">ogonnʼor </ts>
               <ts e="T869" id="Seg_5080" n="e" s="T868">karaktarɨn </ts>
               <ts e="T870" id="Seg_5082" n="e" s="T869">bɨlčaččɨ </ts>
               <ts e="T871" id="Seg_5084" n="e" s="T870">körön </ts>
               <ts e="T872" id="Seg_5086" n="e" s="T871">turan </ts>
               <ts e="T873" id="Seg_5088" n="e" s="T872">ele </ts>
               <ts e="T874" id="Seg_5090" n="e" s="T873">küːhünen </ts>
               <ts e="T875" id="Seg_5092" n="e" s="T874">algɨː </ts>
               <ts e="T876" id="Seg_5094" n="e" s="T875">turbut. </ts>
               <ts e="T877" id="Seg_5096" n="e" s="T876">"Dʼe, </ts>
               <ts e="T878" id="Seg_5098" n="e" s="T877">eteŋŋe </ts>
               <ts e="T879" id="Seg_5100" n="e" s="T878">hɨldʼɨŋ, </ts>
               <ts e="T880" id="Seg_5102" n="e" s="T879">ehigi </ts>
               <ts e="T881" id="Seg_5104" n="e" s="T880">hu͡olgutugar </ts>
               <ts e="T882" id="Seg_5106" n="e" s="T881">kim </ts>
               <ts e="T883" id="Seg_5108" n="e" s="T882">da </ts>
               <ts e="T884" id="Seg_5110" n="e" s="T883">haːrappatɨn. </ts>
               <ts e="T885" id="Seg_5112" n="e" s="T884">Ulakan </ts>
               <ts e="T886" id="Seg_5114" n="e" s="T885">öjü </ts>
               <ts e="T887" id="Seg_5116" n="e" s="T886">ɨlɨŋ. </ts>
               <ts e="T888" id="Seg_5118" n="e" s="T887">Türgennik </ts>
               <ts e="T889" id="Seg_5120" n="e" s="T888">tönnüŋ </ts>
               <ts e="T890" id="Seg_5122" n="e" s="T889">töröːbüt </ts>
               <ts e="T891" id="Seg_5124" n="e" s="T890">hirgitiger!" </ts>
               <ts e="T892" id="Seg_5126" n="e" s="T891">Kaːlɨː </ts>
               <ts e="T893" id="Seg_5128" n="e" s="T892">dʼon </ts>
               <ts e="T894" id="Seg_5130" n="e" s="T893">öːr </ts>
               <ts e="T895" id="Seg_5132" n="e" s="T894">bagajɨ </ts>
               <ts e="T896" id="Seg_5134" n="e" s="T895">körö </ts>
               <ts e="T897" id="Seg_5136" n="e" s="T896">turbuttar </ts>
               <ts e="T898" id="Seg_5138" n="e" s="T897">mu͡ora </ts>
               <ts e="T899" id="Seg_5140" n="e" s="T898">ürdünen, </ts>
               <ts e="T900" id="Seg_5142" n="e" s="T899">uhun </ts>
               <ts e="T901" id="Seg_5144" n="e" s="T900">kös </ts>
               <ts e="T902" id="Seg_5146" n="e" s="T901">hütü͡ör </ts>
               <ts e="T903" id="Seg_5148" n="e" s="T902">di͡eri </ts>
               <ts e="T904" id="Seg_5150" n="e" s="T903">his </ts>
               <ts e="T905" id="Seg_5152" n="e" s="T904">ɨnaraː </ts>
               <ts e="T906" id="Seg_5154" n="e" s="T905">öttüger. </ts>
               <ts e="T907" id="Seg_5156" n="e" s="T906">Ogo </ts>
               <ts e="T908" id="Seg_5158" n="e" s="T907">aːjmakka </ts>
               <ts e="T909" id="Seg_5160" n="e" s="T908">törüttenne </ts>
               <ts e="T910" id="Seg_5162" n="e" s="T909">haŋa </ts>
               <ts e="T911" id="Seg_5164" n="e" s="T910">üje, </ts>
               <ts e="T912" id="Seg_5166" n="e" s="T911">ü͡örener </ts>
               <ts e="T913" id="Seg_5168" n="e" s="T912">üje </ts>
               <ts e="T914" id="Seg_5170" n="e" s="T913">hurukka. </ts>
               <ts e="T915" id="Seg_5172" n="e" s="T914">Ehigi </ts>
               <ts e="T916" id="Seg_5174" n="e" s="T915">ihilleːtigit </ts>
               <ts e="T917" id="Seg_5176" n="e" s="T916">öhü </ts>
               <ts e="T918" id="Seg_5178" n="e" s="T917">"Valačanka </ts>
               <ts e="T919" id="Seg_5180" n="e" s="T918">di͡ek </ts>
               <ts e="T920" id="Seg_5182" n="e" s="T919">köhüː". </ts>
               <ts e="T921" id="Seg_5184" n="e" s="T920">Aːkpɨta </ts>
               <ts e="T922" id="Seg_5186" n="e" s="T921">Papov. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T3" id="Seg_5187" s="T0">PoNA_19900810_TripToVolochanka_nar.001 (001.001)</ta>
            <ta e="T10" id="Seg_5188" s="T3">PoNA_19900810_TripToVolochanka_nar.002 (001.002)</ta>
            <ta e="T13" id="Seg_5189" s="T10">PoNA_19900810_TripToVolochanka_nar.003 (001.003)</ta>
            <ta e="T19" id="Seg_5190" s="T13">PoNA_19900810_TripToVolochanka_nar.004 (001.004)</ta>
            <ta e="T28" id="Seg_5191" s="T19">PoNA_19900810_TripToVolochanka_nar.005 (001.005)</ta>
            <ta e="T34" id="Seg_5192" s="T28">PoNA_19900810_TripToVolochanka_nar.006 (001.006)</ta>
            <ta e="T42" id="Seg_5193" s="T34">PoNA_19900810_TripToVolochanka_nar.007 (001.007)</ta>
            <ta e="T48" id="Seg_5194" s="T42">PoNA_19900810_TripToVolochanka_nar.008 (001.008)</ta>
            <ta e="T53" id="Seg_5195" s="T48">PoNA_19900810_TripToVolochanka_nar.009 (001.009)</ta>
            <ta e="T60" id="Seg_5196" s="T53">PoNA_19900810_TripToVolochanka_nar.010 (001.010)</ta>
            <ta e="T72" id="Seg_5197" s="T60">PoNA_19900810_TripToVolochanka_nar.011 (001.011)</ta>
            <ta e="T76" id="Seg_5198" s="T72">PoNA_19900810_TripToVolochanka_nar.012 (001.012)</ta>
            <ta e="T91" id="Seg_5199" s="T76">PoNA_19900810_TripToVolochanka_nar.013 (001.013)</ta>
            <ta e="T100" id="Seg_5200" s="T91">PoNA_19900810_TripToVolochanka_nar.014 (001.014)</ta>
            <ta e="T110" id="Seg_5201" s="T100">PoNA_19900810_TripToVolochanka_nar.015 (001.015)</ta>
            <ta e="T116" id="Seg_5202" s="T110">PoNA_19900810_TripToVolochanka_nar.016 (001.016)</ta>
            <ta e="T123" id="Seg_5203" s="T116">PoNA_19900810_TripToVolochanka_nar.017 (001.017)</ta>
            <ta e="T133" id="Seg_5204" s="T123">PoNA_19900810_TripToVolochanka_nar.018 (001.018)</ta>
            <ta e="T137" id="Seg_5205" s="T133">PoNA_19900810_TripToVolochanka_nar.019 (001.019)</ta>
            <ta e="T140" id="Seg_5206" s="T137">PoNA_19900810_TripToVolochanka_nar.020 (001.020)</ta>
            <ta e="T146" id="Seg_5207" s="T140">PoNA_19900810_TripToVolochanka_nar.021 (001.021)</ta>
            <ta e="T152" id="Seg_5208" s="T146">PoNA_19900810_TripToVolochanka_nar.022 (001.022)</ta>
            <ta e="T154" id="Seg_5209" s="T152">PoNA_19900810_TripToVolochanka_nar.023 (001.023)</ta>
            <ta e="T157" id="Seg_5210" s="T154">PoNA_19900810_TripToVolochanka_nar.024 (001.024)</ta>
            <ta e="T166" id="Seg_5211" s="T157">PoNA_19900810_TripToVolochanka_nar.025 (001.025)</ta>
            <ta e="T170" id="Seg_5212" s="T166">PoNA_19900810_TripToVolochanka_nar.026 (001.026)</ta>
            <ta e="T179" id="Seg_5213" s="T170">PoNA_19900810_TripToVolochanka_nar.027 (001.027)</ta>
            <ta e="T182" id="Seg_5214" s="T179">PoNA_19900810_TripToVolochanka_nar.028 (001.028)</ta>
            <ta e="T186" id="Seg_5215" s="T182">PoNA_19900810_TripToVolochanka_nar.029 (001.029)</ta>
            <ta e="T190" id="Seg_5216" s="T186">PoNA_19900810_TripToVolochanka_nar.030 (001.030)</ta>
            <ta e="T194" id="Seg_5217" s="T190">PoNA_19900810_TripToVolochanka_nar.031 (001.031)</ta>
            <ta e="T201" id="Seg_5218" s="T194">PoNA_19900810_TripToVolochanka_nar.032 (001.032)</ta>
            <ta e="T209" id="Seg_5219" s="T201">PoNA_19900810_TripToVolochanka_nar.033 (001.033)</ta>
            <ta e="T213" id="Seg_5220" s="T209">PoNA_19900810_TripToVolochanka_nar.034 (001.034)</ta>
            <ta e="T221" id="Seg_5221" s="T213">PoNA_19900810_TripToVolochanka_nar.035 (001.035)</ta>
            <ta e="T229" id="Seg_5222" s="T221">PoNA_19900810_TripToVolochanka_nar.036 (001.036)</ta>
            <ta e="T233" id="Seg_5223" s="T229">PoNA_19900810_TripToVolochanka_nar.037 (001.037)</ta>
            <ta e="T239" id="Seg_5224" s="T233">PoNA_19900810_TripToVolochanka_nar.038 (001.038)</ta>
            <ta e="T248" id="Seg_5225" s="T239">PoNA_19900810_TripToVolochanka_nar.039 (001.039)</ta>
            <ta e="T257" id="Seg_5226" s="T248">PoNA_19900810_TripToVolochanka_nar.040 (001.040)</ta>
            <ta e="T262" id="Seg_5227" s="T257">PoNA_19900810_TripToVolochanka_nar.041 (001.041)</ta>
            <ta e="T269" id="Seg_5228" s="T262">PoNA_19900810_TripToVolochanka_nar.042 (001.042)</ta>
            <ta e="T274" id="Seg_5229" s="T269">PoNA_19900810_TripToVolochanka_nar.043 (001.043)</ta>
            <ta e="T282" id="Seg_5230" s="T274">PoNA_19900810_TripToVolochanka_nar.044 (001.044)</ta>
            <ta e="T287" id="Seg_5231" s="T282">PoNA_19900810_TripToVolochanka_nar.045 (001.045)</ta>
            <ta e="T289" id="Seg_5232" s="T287">PoNA_19900810_TripToVolochanka_nar.046 (001.046)</ta>
            <ta e="T292" id="Seg_5233" s="T289">PoNA_19900810_TripToVolochanka_nar.047 (001.047)</ta>
            <ta e="T304" id="Seg_5234" s="T292">PoNA_19900810_TripToVolochanka_nar.048 (001.048)</ta>
            <ta e="T311" id="Seg_5235" s="T304">PoNA_19900810_TripToVolochanka_nar.049 (001.049)</ta>
            <ta e="T321" id="Seg_5236" s="T311">PoNA_19900810_TripToVolochanka_nar.050 (001.050)</ta>
            <ta e="T327" id="Seg_5237" s="T321">PoNA_19900810_TripToVolochanka_nar.051 (001.051)</ta>
            <ta e="T334" id="Seg_5238" s="T327">PoNA_19900810_TripToVolochanka_nar.052 (001.052)</ta>
            <ta e="T338" id="Seg_5239" s="T334">PoNA_19900810_TripToVolochanka_nar.053 (001.053)</ta>
            <ta e="T346" id="Seg_5240" s="T338">PoNA_19900810_TripToVolochanka_nar.054 (001.054)</ta>
            <ta e="T351" id="Seg_5241" s="T346">PoNA_19900810_TripToVolochanka_nar.055 (001.055)</ta>
            <ta e="T360" id="Seg_5242" s="T351">PoNA_19900810_TripToVolochanka_nar.056 (001.056)</ta>
            <ta e="T365" id="Seg_5243" s="T360">PoNA_19900810_TripToVolochanka_nar.057 (001.057)</ta>
            <ta e="T371" id="Seg_5244" s="T365">PoNA_19900810_TripToVolochanka_nar.058 (001.058)</ta>
            <ta e="T377" id="Seg_5245" s="T371">PoNA_19900810_TripToVolochanka_nar.059 (001.059)</ta>
            <ta e="T381" id="Seg_5246" s="T377">PoNA_19900810_TripToVolochanka_nar.060 (001.060)</ta>
            <ta e="T390" id="Seg_5247" s="T381">PoNA_19900810_TripToVolochanka_nar.061 (001.061)</ta>
            <ta e="T395" id="Seg_5248" s="T390">PoNA_19900810_TripToVolochanka_nar.062 (001.062)</ta>
            <ta e="T403" id="Seg_5249" s="T395">PoNA_19900810_TripToVolochanka_nar.063 (001.063)</ta>
            <ta e="T406" id="Seg_5250" s="T403">PoNA_19900810_TripToVolochanka_nar.064 (001.064)</ta>
            <ta e="T414" id="Seg_5251" s="T406">PoNA_19900810_TripToVolochanka_nar.065 (001.065)</ta>
            <ta e="T424" id="Seg_5252" s="T414">PoNA_19900810_TripToVolochanka_nar.066 (001.066)</ta>
            <ta e="T433" id="Seg_5253" s="T424">PoNA_19900810_TripToVolochanka_nar.067 (001.067)</ta>
            <ta e="T437" id="Seg_5254" s="T433">PoNA_19900810_TripToVolochanka_nar.068 (001.068)</ta>
            <ta e="T442" id="Seg_5255" s="T437">PoNA_19900810_TripToVolochanka_nar.069 (001.069)</ta>
            <ta e="T447" id="Seg_5256" s="T442">PoNA_19900810_TripToVolochanka_nar.070 (001.070)</ta>
            <ta e="T453" id="Seg_5257" s="T447">PoNA_19900810_TripToVolochanka_nar.071 (001.071)</ta>
            <ta e="T458" id="Seg_5258" s="T453">PoNA_19900810_TripToVolochanka_nar.072 (001.072)</ta>
            <ta e="T462" id="Seg_5259" s="T458">PoNA_19900810_TripToVolochanka_nar.073 (001.073)</ta>
            <ta e="T470" id="Seg_5260" s="T462">PoNA_19900810_TripToVolochanka_nar.074 (001.074)</ta>
            <ta e="T477" id="Seg_5261" s="T470">PoNA_19900810_TripToVolochanka_nar.075 (001.075)</ta>
            <ta e="T482" id="Seg_5262" s="T477">PoNA_19900810_TripToVolochanka_nar.076 (001.076)</ta>
            <ta e="T489" id="Seg_5263" s="T482">PoNA_19900810_TripToVolochanka_nar.077 (001.077)</ta>
            <ta e="T496" id="Seg_5264" s="T489">PoNA_19900810_TripToVolochanka_nar.078 (001.078)</ta>
            <ta e="T501" id="Seg_5265" s="T496">PoNA_19900810_TripToVolochanka_nar.079 (001.079)</ta>
            <ta e="T505" id="Seg_5266" s="T501">PoNA_19900810_TripToVolochanka_nar.080 (001.080)</ta>
            <ta e="T510" id="Seg_5267" s="T505">PoNA_19900810_TripToVolochanka_nar.081 (001.081)</ta>
            <ta e="T517" id="Seg_5268" s="T510">PoNA_19900810_TripToVolochanka_nar.082 (001.082)</ta>
            <ta e="T520" id="Seg_5269" s="T517">PoNA_19900810_TripToVolochanka_nar.083 (001.083)</ta>
            <ta e="T524" id="Seg_5270" s="T520">PoNA_19900810_TripToVolochanka_nar.084 (001.084)</ta>
            <ta e="T531" id="Seg_5271" s="T524">PoNA_19900810_TripToVolochanka_nar.085 (001.085)</ta>
            <ta e="T534" id="Seg_5272" s="T531">PoNA_19900810_TripToVolochanka_nar.086 (001.086)</ta>
            <ta e="T541" id="Seg_5273" s="T534">PoNA_19900810_TripToVolochanka_nar.087 (001.087)</ta>
            <ta e="T548" id="Seg_5274" s="T541">PoNA_19900810_TripToVolochanka_nar.088 (001.088)</ta>
            <ta e="T556" id="Seg_5275" s="T548">PoNA_19900810_TripToVolochanka_nar.089 (001.089)</ta>
            <ta e="T562" id="Seg_5276" s="T556">PoNA_19900810_TripToVolochanka_nar.090 (001.090)</ta>
            <ta e="T565" id="Seg_5277" s="T562">PoNA_19900810_TripToVolochanka_nar.091 (001.091)</ta>
            <ta e="T573" id="Seg_5278" s="T565">PoNA_19900810_TripToVolochanka_nar.092 (001.092)</ta>
            <ta e="T582" id="Seg_5279" s="T573">PoNA_19900810_TripToVolochanka_nar.093 (001.093)</ta>
            <ta e="T590" id="Seg_5280" s="T582">PoNA_19900810_TripToVolochanka_nar.094 (001.094)</ta>
            <ta e="T593" id="Seg_5281" s="T590">PoNA_19900810_TripToVolochanka_nar.095 (001.095)</ta>
            <ta e="T597" id="Seg_5282" s="T593">PoNA_19900810_TripToVolochanka_nar.096 (001.096)</ta>
            <ta e="T603" id="Seg_5283" s="T597">PoNA_19900810_TripToVolochanka_nar.097 (001.097)</ta>
            <ta e="T612" id="Seg_5284" s="T603">PoNA_19900810_TripToVolochanka_nar.098 (001.098)</ta>
            <ta e="T623" id="Seg_5285" s="T612">PoNA_19900810_TripToVolochanka_nar.099 (001.099)</ta>
            <ta e="T629" id="Seg_5286" s="T623">PoNA_19900810_TripToVolochanka_nar.100 (001.100)</ta>
            <ta e="T640" id="Seg_5287" s="T629">PoNA_19900810_TripToVolochanka_nar.101 (001.101)</ta>
            <ta e="T648" id="Seg_5288" s="T640">PoNA_19900810_TripToVolochanka_nar.102 (001.102)</ta>
            <ta e="T661" id="Seg_5289" s="T648">PoNA_19900810_TripToVolochanka_nar.103 (001.103)</ta>
            <ta e="T668" id="Seg_5290" s="T661">PoNA_19900810_TripToVolochanka_nar.104 (001.104)</ta>
            <ta e="T678" id="Seg_5291" s="T668">PoNA_19900810_TripToVolochanka_nar.105 (001.105)</ta>
            <ta e="T684" id="Seg_5292" s="T678">PoNA_19900810_TripToVolochanka_nar.106 (001.106)</ta>
            <ta e="T689" id="Seg_5293" s="T684">PoNA_19900810_TripToVolochanka_nar.107 (001.107)</ta>
            <ta e="T696" id="Seg_5294" s="T689">PoNA_19900810_TripToVolochanka_nar.108 (001.108)</ta>
            <ta e="T701" id="Seg_5295" s="T696">PoNA_19900810_TripToVolochanka_nar.109 (001.109)</ta>
            <ta e="T707" id="Seg_5296" s="T701">PoNA_19900810_TripToVolochanka_nar.110 (001.110)</ta>
            <ta e="T714" id="Seg_5297" s="T707">PoNA_19900810_TripToVolochanka_nar.111 (001.111)</ta>
            <ta e="T720" id="Seg_5298" s="T714">PoNA_19900810_TripToVolochanka_nar.112 (001.112)</ta>
            <ta e="T723" id="Seg_5299" s="T720">PoNA_19900810_TripToVolochanka_nar.113 (001.113)</ta>
            <ta e="T734" id="Seg_5300" s="T723">PoNA_19900810_TripToVolochanka_nar.114 (001.114)</ta>
            <ta e="T742" id="Seg_5301" s="T734">PoNA_19900810_TripToVolochanka_nar.115 (001.115)</ta>
            <ta e="T745" id="Seg_5302" s="T742">PoNA_19900810_TripToVolochanka_nar.116 (001.116)</ta>
            <ta e="T753" id="Seg_5303" s="T745">PoNA_19900810_TripToVolochanka_nar.117 (001.117)</ta>
            <ta e="T756" id="Seg_5304" s="T753">PoNA_19900810_TripToVolochanka_nar.118 (001.118)</ta>
            <ta e="T761" id="Seg_5305" s="T756">PoNA_19900810_TripToVolochanka_nar.119 (001.119)</ta>
            <ta e="T770" id="Seg_5306" s="T761">PoNA_19900810_TripToVolochanka_nar.120 (001.120)</ta>
            <ta e="T778" id="Seg_5307" s="T770">PoNA_19900810_TripToVolochanka_nar.121 (001.121)</ta>
            <ta e="T787" id="Seg_5308" s="T778">PoNA_19900810_TripToVolochanka_nar.122 (001.122)</ta>
            <ta e="T796" id="Seg_5309" s="T787">PoNA_19900810_TripToVolochanka_nar.123 (001.123)</ta>
            <ta e="T807" id="Seg_5310" s="T796">PoNA_19900810_TripToVolochanka_nar.124 (001.124)</ta>
            <ta e="T813" id="Seg_5311" s="T807">PoNA_19900810_TripToVolochanka_nar.125 (001.125)</ta>
            <ta e="T820" id="Seg_5312" s="T813">PoNA_19900810_TripToVolochanka_nar.126 (001.126)</ta>
            <ta e="T823" id="Seg_5313" s="T820">PoNA_19900810_TripToVolochanka_nar.127 (001.127)</ta>
            <ta e="T830" id="Seg_5314" s="T823">PoNA_19900810_TripToVolochanka_nar.128 (001.128)</ta>
            <ta e="T838" id="Seg_5315" s="T830">PoNA_19900810_TripToVolochanka_nar.129 (001.129)</ta>
            <ta e="T843" id="Seg_5316" s="T838">PoNA_19900810_TripToVolochanka_nar.130 (001.130)</ta>
            <ta e="T847" id="Seg_5317" s="T843">PoNA_19900810_TripToVolochanka_nar.131 (001.131)</ta>
            <ta e="T850" id="Seg_5318" s="T847">PoNA_19900810_TripToVolochanka_nar.132 (001.132)</ta>
            <ta e="T859" id="Seg_5319" s="T850">PoNA_19900810_TripToVolochanka_nar.133 (001.133)</ta>
            <ta e="T866" id="Seg_5320" s="T859">PoNA_19900810_TripToVolochanka_nar.134 (001.134)</ta>
            <ta e="T876" id="Seg_5321" s="T866">PoNA_19900810_TripToVolochanka_nar.135 (001.135)</ta>
            <ta e="T884" id="Seg_5322" s="T876">PoNA_19900810_TripToVolochanka_nar.136 (001.136)</ta>
            <ta e="T887" id="Seg_5323" s="T884">PoNA_19900810_TripToVolochanka_nar.137 (001.137)</ta>
            <ta e="T891" id="Seg_5324" s="T887">PoNA_19900810_TripToVolochanka_nar.138 (001.138)</ta>
            <ta e="T906" id="Seg_5325" s="T891">PoNA_19900810_TripToVolochanka_nar.139 (001.139)</ta>
            <ta e="T914" id="Seg_5326" s="T906">PoNA_19900810_TripToVolochanka_nar.140 (001.140)</ta>
            <ta e="T920" id="Seg_5327" s="T914">PoNA_19900810_TripToVolochanka_nar.141 (001.141)</ta>
            <ta e="T922" id="Seg_5328" s="T920">PoNA_19900810_TripToVolochanka_nar.142 (001.142)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T3" id="Seg_5329" s="T0">Волочанка диэк көһүү.</ta>
            <ta e="T10" id="Seg_5330" s="T3">Ыргакта ыйа баранар күннэрэ турбуттара итии багайы.</ta>
            <ta e="T13" id="Seg_5331" s="T10">Оттор, һэбирдэктэр кагдьарыйбыттар.</ta>
            <ta e="T19" id="Seg_5332" s="T13">Муора онно-манна кыһыл көмүс буолан испит.</ta>
            <ta e="T28" id="Seg_5333" s="T19">Ол күттэргэ билиилээк биллиилээк?) улакан күөл кытылыгар улакан комууну (комунуу) турбута:</ta>
            <ta e="T34" id="Seg_5334" s="T28">ого ааймагы бэлэннииллэр Волочанкага иллээри үөрэккэ.</ta>
            <ta e="T42" id="Seg_5335" s="T34">Муора нэһилиэгэ урут билбэт этилэрэ оголорун үөрэккэ ыытары.</ta>
            <ta e="T48" id="Seg_5336" s="T42">Ол иһин һанаалара барбат оскуолага биэрэргэ.</ta>
            <ta e="T53" id="Seg_5337" s="T48">Бүттэ өһү булаллар ыытымаары оголорун.</ta>
            <ta e="T60" id="Seg_5338" s="T53">Родовойдарыгар ол күннэргэ олус эрэйдээк үлэни оӈорбуттара.</ta>
            <ta e="T72" id="Seg_5339" s="T60">Бары ураһа дьиэ аайы һылдьан кэпсэтэ һатаабыт, оголорун ускуолага биэриэктэрин, үөрэккэ ыытыактарын.</ta>
            <ta e="T76" id="Seg_5340" s="T72">Итинтэн туок да таксыбатак.</ta>
            <ta e="T91" id="Seg_5341" s="T76">Ол иһин һүбэ буолбут -- оӈоруорга диэн мунньагы муора үрдүнэн, барылара комуллуоктарын һырай һырайга кэпсэтии буолуогун.</ta>
            <ta e="T100" id="Seg_5342" s="T91">Биир улакан ураһа дьиэгэ арыччы батан комуллубуттар турар турбат.</ta>
            <ta e="T110" id="Seg_5343" s="T100">Эр киһилэр туок да диэктэрин бэрдиттэн һир диэк агай көрөллөр. </ta>
            <ta e="T116" id="Seg_5344" s="T110">Дьактар ааймак пылаттарынан карактарын ууларын һотуналлар.</ta>
            <ta e="T123" id="Seg_5345" s="T116">"Кэбэлэр, туок буолаӈӈыт бастаргытын тобуктаргыт иһигэр кииллэрдигит? </ta>
            <ta e="T133" id="Seg_5346" s="T123">Того ньимаһа һытагыт?" -- һаӈарбыт диэн Диэбгиэн һаӈалаак эмиэксин, үөгүлээн биэрбит.</ta>
            <ta e="T137" id="Seg_5347" s="T133">Гини обургу баламата бэрт. </ta>
            <ta e="T140" id="Seg_5348" s="T137">Кимтэн да толлубат.</ta>
            <ta e="T146" id="Seg_5349" s="T140">Олорор һиртэн оргуйакаан турбут Парфирай огонньор </ta>
            <ta e="T152" id="Seg_5350" s="T146">Мэльдэн мэниитин имэрийэ түһээт иэдэйбэккэ одууламмыт.</ta>
            <ta e="T154" id="Seg_5351" s="T152">"Кайтак диэмий! </ta>
            <ta e="T157" id="Seg_5352" s="T154">Ити итигирдик буолар. </ta>
            <ta e="T166" id="Seg_5353" s="T157">Оголор бу һаӈа үйэгэ үөрэнэллэрэ һин дааганы олус һөпсөллөөк.</ta>
            <ta e="T170" id="Seg_5354" s="T166">Гинилэр һуруксут буолуок тустаактар. </ta>
            <ta e="T179" id="Seg_5355" s="T170">Бу мин Уйбаан уолбун оччугуйуттан үөрэппитим балык .. , булт капканныагын.</ta>
            <ta e="T182" id="Seg_5356" s="T179">Мин аны кырыйдым. </ta>
            <ta e="T186" id="Seg_5357" s="T182">Үлэни котуммат буолан эрэбин. </ta>
            <ta e="T190" id="Seg_5358" s="T186">Ким мин кэргэммин аһатыагай?</ta>
            <ta e="T194" id="Seg_5359" s="T190">Муорабыт тымныы, олус пургаалаак. </ta>
            <ta e="T201" id="Seg_5360" s="T194">Күүстээк эрэ ого агай киһи булду булааччы.</ta>
            <ta e="T209" id="Seg_5361" s="T201">Уйбаны оскуолага .. пааһы капкааны иитэ үөрэтиэктэрэ һуога. </ta>
            <ta e="T213" id="Seg_5362" s="T209">Гини умнуога муора үлэтин.</ta>
            <ta e="T221" id="Seg_5363" s="T213">Дьактар көрдүк дьиэгэ агай олоруога, туугу да гыммакка.</ta>
            <ta e="T229" id="Seg_5364" s="T221">"Эн, огонньор, дьактаттары тыытыма! -- эмиэ Дьиэбгиэн үөгүүтэ иһиллибит. </ta>
            <ta e="T233" id="Seg_5365" s="T229">Ким эниэкэ аһы бэлэнниир? </ta>
            <ta e="T239" id="Seg_5366" s="T233">Дьиэгин итиитик тутар, атактаргын,.. таӈаскын абырактыыр?"</ta>
            <ta e="T248" id="Seg_5367" s="T239">"Ити кирдик, итигирдик буолар, -- олус үртпэккэ Парфирай һаӈара турбут, --</ta>
            <ta e="T257" id="Seg_5368" s="T248">Мин Кыычаа кыыһым астыы һатыыр, атагы, таӈаһыы оӈоро һатыыр. </ta>
            <ta e="T262" id="Seg_5369" s="T257">Оскуолага һин даагыны онтутун умнуога.</ta>
            <ta e="T269" id="Seg_5370" s="T262">Эр киһи көрдүк кончоӈнуу һылдьыага, дьиэни төгүрүйэ. </ta>
            <ta e="T274" id="Seg_5371" s="T269">Кайдак мин олоруомуй оголорум бардактарына?"</ta>
            <ta e="T282" id="Seg_5372" s="T274">Родовойа Анисим Назарович Попов күлүмнүү түһээт огонньору токтоппут.</ta>
            <ta e="T287" id="Seg_5373" s="T282">"Дьэ, огонньор, эн һылайдыӈ быһылаак. </ta>
            <ta e="T289" id="Seg_5374" s="T287">Олоро түс. </ta>
            <ta e="T292" id="Seg_5375" s="T289">Атын дьон һаӈардын".</ta>
            <ta e="T304" id="Seg_5376" s="T292">Огонньор туок да диэбэккэ оргуйакаан ологугар лак гына олорбут, тобуктарыттан тутта түһээт.</ta>
            <ta e="T311" id="Seg_5377" s="T304">"Ким өссүө туок һаӈалаагый?" -- диэн ыыппыт Родовойдара.</ta>
            <ta e="T321" id="Seg_5378" s="T311">Кимнэрэ эрэ һаӈарарын күүтэ һатаан, аакпыт, каннук (каннык) оголоро оскуолага баралларын. </ta>
            <ta e="T327" id="Seg_5379" s="T321">Ким кааларын оскуолага, барбатын, һаастара улакаттары.</ta>
            <ta e="T334" id="Seg_5380" s="T327">Ол оголорго түбэспит Парфирай огонньор уола Уйбан.</ta>
            <ta e="T338" id="Seg_5381" s="T334">Кычааны эмиэ кэһиэккэ тустаак. </ta>
            <ta e="T346" id="Seg_5382" s="T338">Гини һааӈа һин даагыны (дааганы) элбэк, көрдөммүт Парфирай огонньор.</ta>
            <ta e="T351" id="Seg_5383" s="T346">"Һуок, Кыычаа барыа һуога оскуолага. </ta>
            <ta e="T360" id="Seg_5384" s="T351">Огонньор, кэчэһимэ, үөрэктээк буоллагына эр киһи һыалыйатын кэтиэ һуога.</ta>
            <ta e="T365" id="Seg_5385" s="T360">Дьиэни төгүрүччу кончоӈнуу һылдьыа һуога".</ta>
            <ta e="T371" id="Seg_5386" s="T365">Мунньакка олорор дьон элэ истэринэн күлсүбүттэр.</ta>
            <ta e="T377" id="Seg_5387" s="T371">"Барар оголору бэлэннээӈ өтүөтүк. </ta>
            <ta e="T381" id="Seg_5388" s="T377">Тагыннарыӈ таӈарага тагыннарар көрдүк.</ta>
            <ta e="T390" id="Seg_5389" s="T381">Оголорбут (оголорбун) бэйэм иллиэм Волочанкаттан", -- мунньагы бүтэрэ гына диэбит һүбэһиттэрэ.</ta>
            <ta e="T395" id="Seg_5390" s="T390">Мунньак аһаатын комунуу айдаана элбээбит.</ta>
            <ta e="T403" id="Seg_5391" s="T395">Дьактаттар матакаларын иһиттэн таһартыыллар оголорун киэӈ, киэӈ таӈастарын. </ta>
            <ta e="T406" id="Seg_5392" s="T403">Таӈарага кэтэр ырбаакыларын.</ta>
            <ta e="T414" id="Seg_5393" s="T406">Һороктор көстүбэт һиргэ тигэттииллэр эбит оччугуй таӈаларын, кириэстэри.</ta>
            <ta e="T424" id="Seg_5394" s="T414">Оголорун (честийдар?) бары элэ минньигэс астарынан. Амаһаннан, (кагынан?), таба тылынан.</ta>
            <ta e="T433" id="Seg_5395" s="T424">Киэһэ диэк .. Родовойыгар ыаллана киирбит Һэмэн огонньор.</ta>
            <ta e="T437" id="Seg_5396" s="T433">Того кэлбитин быһа һаӈарбат. </ta>
            <ta e="T442" id="Seg_5397" s="T437">Чай иһэ агай олорон диэбит:</ta>
            <ta e="T447" id="Seg_5398" s="T442">"Былыр эһэлэрбит, агаларбыт таӈараһыт этилэрэ. </ta>
            <ta e="T453" id="Seg_5399" s="T447">Улакан һайтааӈӈа биэк бэлэктиир идэлээк этилэрэ.</ta>
            <ta e="T458" id="Seg_5400" s="T453">Бары ааймак өтүөтүк олоруоктарын багараллар. </ta>
            <ta e="T462" id="Seg_5401" s="T458">Туугунан да мускураабакка, ыалдьыбакка.</ta>
            <ta e="T470" id="Seg_5402" s="T462">Улакан ойүттар киһи һылдьар һуолун билэллэрэ бэрт этэ.</ta>
            <ta e="T477" id="Seg_5403" s="T470">Бу олокпутуттан ыраага һуок баар улакан һайтан-таас. </ta>
            <ta e="T482" id="Seg_5404" s="T477">Баары киһи онно бэлэги биэриээччи.</ta>
            <ta e="T489" id="Seg_5405" s="T482">Һотору оголорбут атын һиргэ бараллар, атын киһилэргэ".</ta>
            <ta e="T496" id="Seg_5406" s="T489">Родовойа ыалдьытын өһүн иһиллии-иһиллии иһигэр дии һаныыр: </ta>
            <ta e="T501" id="Seg_5407" s="T496">"Туок һанаатынан итинник өһү таһаарар?"</ta>
            <ta e="T505" id="Seg_5408" s="T501">Ыалдьыта иэдйэбэккэ һаӈартаан испит.</ta>
            <ta e="T510" id="Seg_5409" s="T505">Дьактаттар муӈнаактар олус һоӈгураатылар. Ытаһаллар.</ta>
            <ta e="T517" id="Seg_5410" s="T510">Кыра огону дьиэттэн былдьыыр идэ олус аньыылаак.</ta>
            <ta e="T520" id="Seg_5411" s="T517">Дьон олус һанааргыыр. </ta>
            <ta e="T524" id="Seg_5412" s="T520">Бадага, һайтааӈӈа бэлэк биэриэккэ.</ta>
            <ta e="T531" id="Seg_5413" s="T524">"Дьэ, огонньор, минигин һиилээмэ, итини гыныакпыт һуога".</ta>
            <ta e="T534" id="Seg_5414" s="T531">"Ускуола һатааны көрдөөбөт. </ta>
            <ta e="T541" id="Seg_5415" s="T534">Итигирдик бары киһиэкэ кэпсээ. Һанаргаабатыннар", -- диэбит Родовойа.</ta>
            <ta e="T548" id="Seg_5416" s="T541">Огонньуор туок да диэбэккэ чааскытыгар чаай кутуммут.</ta>
            <ta e="T556" id="Seg_5417" s="T548">"Биһиги, огонньуоттор, кайдак өтүө буолуон диэн, һүбэ биэрэбит. </ta>
            <ta e="T562" id="Seg_5418" s="T556">Кирдик, һорок кэннэ һыыһа һаӈарабыт буолуога".</ta>
            <ta e="T565" id="Seg_5419" s="T562">Дьактаттар ытаналлара бэрт. </ta>
            <ta e="T573" id="Seg_5420" s="T565">Киһи үөрэгин… киһи үөрэгин карыйара олус һүрдээк.</ta>
            <ta e="T582" id="Seg_5421" s="T573">Олус кыра оголорго улакан дьону таргатыкка, һупту көрөр буолуоктарын.</ta>
            <ta e="T590" id="Seg_5422" s="T582">Олус оччугуй ого тугут көрдүк, канна барарын билбэт. </ta>
            <ta e="T593" id="Seg_5423" s="T590">Өтүөтүк таӈна һатаабат.</ta>
            <ta e="T597" id="Seg_5424" s="T593">"Һити кирдик," -- диэбит Родовоя. </ta>
            <ta e="T603" id="Seg_5425" s="T597">Огонньор бу мунньак аайдааныгар умнан кээспит.</ta>
            <ta e="T612" id="Seg_5426" s="T603">Кыһын, һаӈа дьыл кэлэр кэмигэр оголор һынньана кэлиэктэрэ дьиэлэригэр.</ta>
            <ta e="T623" id="Seg_5427" s="T612">Һас, май ыйыгар, биир дьыллаагы үөрэктэртэрин бүттэктэринэ күһүӈӈэ диэри дьиэлэрэгэр буолуоктара.</ta>
            <ta e="T629" id="Seg_5428" s="T623">Ити өһү мин бары киһиэкэ кэпсиэм.</ta>
            <ta e="T640" id="Seg_5429" s="T629">"Киһилэр дии һаныыллар, каһан да төннөрбөккө ылаллар оголору ", -- үөрбүччэ огонньор һаӈарбыт.</ta>
            <ta e="T648" id="Seg_5430" s="T640">Огонньор дьиэтигэр барбытын кэннэ Анисим бэйэтин гытта кэпсэтэр:</ta>
            <ta e="T661" id="Seg_5431" s="T648">"Бары киһиэкэ өйдөтөн иһиэккэ нада, кайдак һаӈа ыйаак өтүөнү оӈорорун тыа дьонун иннигэр".</ta>
            <ta e="T668" id="Seg_5432" s="T661">Итинник огонньоттор өс ыыталлара кирдиктээк, котуулаак буолуога.</ta>
            <ta e="T678" id="Seg_5433" s="T668">Нөӈүө күн эмиэ мунньакка киһилэр комуллубуттар Һэмэн огонньор дьиэтигэр.</ta>
            <ta e="T684" id="Seg_5434" s="T678">Дьон бэгэһэгиӈӈитээгэр кайдак да үөрбэрэлэрэ көтөгүллүбүт.</ta>
            <ta e="T689" id="Seg_5435" s="T684">Аайдаан, күлсүү-һалсыы бүтээтин Родовойдара диэбит:</ta>
            <ta e="T696" id="Seg_5436" s="T689">"Бэйэгит һанаагытыттан кырадай ааймактары туттарыӈ улакан оголорго.</ta>
            <ta e="T701" id="Seg_5437" s="T696">Көһөр кэмӈэ көрөр буолуоктарын гинилэри".</ta>
            <ta e="T707" id="Seg_5438" s="T701">"Биһиэкэ диэбиттэрэ, оголорбут кыһын һынньана кэлиэктэрэ. </ta>
            <ta e="T714" id="Seg_5439" s="T707">Ити кирдик һаӈа дуо?" -- ыыппыт биир дьактар.</ta>
            <ta e="T720" id="Seg_5440" s="T714">Һэмэн огонньор олорор һириттэн өйдө.. өйдөӈнөөбүт.</ta>
            <ta e="T723" id="Seg_5441" s="T720">"Ити кирдик өс. </ta>
            <ta e="T734" id="Seg_5442" s="T723">Кыһын һынньана кэлиэктэрэ, онтон һайын һупту дьиэлэригэр буола кэлиэктэрэ", -- диэбит Анисим.</ta>
            <ta e="T742" id="Seg_5443" s="T734">Һэмэн огонньор налыйа түһээт һаӈа ал.. ыллыбыт (ылбыт):</ta>
            <ta e="T745" id="Seg_5444" s="T742">"Мин туугу диэбитимий?"</ta>
            <ta e="T753" id="Seg_5445" s="T745">Барылара һылаас карагынан көрөн, Һэмэн огонньор диэк эргиллибиттэр.</ta>
            <ta e="T756" id="Seg_5446" s="T753">Мунньактара ааспыт түргэнник. </ta>
            <ta e="T761" id="Seg_5447" s="T756">Киһилэр ураһа дьиэлэрин аайы бараттаабыттар.</ta>
            <ta e="T770" id="Seg_5448" s="T761">Нөӈүө күһүн (күнүн) үөрдэрин һиэгилээн бараан, таба тутуута буолбут көһөргө.</ta>
            <ta e="T778" id="Seg_5449" s="T770">Дьактаттар дьиэлэрин аттыгар оголорун гытта һолото һуоктар комунаннар.</ta>
            <ta e="T787" id="Seg_5450" s="T778">Оголоругар һупту һаӈараллар, мөӈпөт буолуоктарын, улакан оголору истэр буолуоктарын.</ta>
            <ta e="T796" id="Seg_5451" s="T787">Таба тутаат, көлүнээт, ураһа дьиэ аайы аһыыллар көһөр иннигэр.</ta>
            <ta e="T807" id="Seg_5452" s="T796">Онтон ураһа дьиэ аайыттан барылара баргыһан таксыбыттар, ыраак багай барар оголору атараары.</ta>
            <ta e="T813" id="Seg_5453" s="T807">"Каһан мин энигин көрүөмүй? -- Дьэбгиэн ытаабыт</ta>
            <ta e="T820" id="Seg_5454" s="T813">-- кайтак энигинэ һуок олоруомуй? </ta>
            <ta e="T823" id="Seg_5455" s="T820">Ким биһиэкэ көмөлөһүөгэй?"</ta>
            <ta e="T830" id="Seg_5456" s="T823">Оготут кам тутаат (тутан) бараан дьиэтин диэк һоһор.</ta>
            <ta e="T838" id="Seg_5457" s="T830">"Эн туок буолагын? -- биир.. биир дьактар диэбит догоругар.</ta>
            <ta e="T843" id="Seg_5458" s="T838">Огону һанааргатыма көһөр иннигэр. </ta>
            <ta e="T847" id="Seg_5459" s="T843">Итигирдик атаарар олус куһаган. </ta>
            <ta e="T850" id="Seg_5460" s="T847">Аньыы! Атаар киһилии".</ta>
            <ta e="T859" id="Seg_5461" s="T850">Бары уучактаак оголор эһэлэрин кэнниттэн баран уһун көс буолбуттар.</ta>
            <ta e="T866" id="Seg_5462" s="T859">Турбат-турар оголорун атааран ол көс кэнниттэн һубуруйбуттар.</ta>
            <ta e="T876" id="Seg_5463" s="T866">Парфирай огонньор карактарын былчаччы көрөн туран элэ күүһүнэн алгыы турбут.</ta>
            <ta e="T884" id="Seg_5464" s="T876">"Дэ, этэӈӈэ һылдьыӈ, эһиги һуолгутугар ким да һаараппатын. </ta>
            <ta e="T887" id="Seg_5465" s="T884">Улакан өйү ылыӈ. </ta>
            <ta e="T891" id="Seg_5466" s="T887">Түргэнник төннүӈ төрөөбүт һиргитигэр!"</ta>
            <ta e="T906" id="Seg_5467" s="T891">Каалыы дьон өөр багайы көрө турбуттар муора үрдүнэн, уһун көс һүтүөр диэри һис ынараа өттүгэр. </ta>
            <ta e="T914" id="Seg_5468" s="T906">Ого ааймакка төрүттэннэ һаӈа үйэ -- үөрэнэр үйэ һурукка.</ta>
            <ta e="T920" id="Seg_5469" s="T914">Эһиги иһиллээтигит өһү "Волочанка диэк көһүү". </ta>
            <ta e="T922" id="Seg_5470" s="T920">Аакпыта Попов.</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T3" id="Seg_5471" s="T0">Valačanka di͡ek köhüː. </ta>
            <ta e="T10" id="Seg_5472" s="T3">ɨrgakta ɨja baranar künnere turbuttara itiː bagajɨ. </ta>
            <ta e="T13" id="Seg_5473" s="T10">Ottor, hebirdekter kagdʼarɨjbɨttar. </ta>
            <ta e="T19" id="Seg_5474" s="T13">Mu͡ora onno-manna kɨhɨl kömüs bu͡olan ispit. </ta>
            <ta e="T28" id="Seg_5475" s="T19">Ol kütterge biliːleːk ulakan kü͡öl kɨtɨlɨgar ulakan komuːnu turbuta: </ta>
            <ta e="T34" id="Seg_5476" s="T28">Ogo aːjmagɨ belenniːller Valačankaga illeːri ü͡örekke. </ta>
            <ta e="T42" id="Seg_5477" s="T34">Mu͡ora nehili͡ege urut bilbet etilere ogolorun ü͡örekke ɨːtarɨ. </ta>
            <ta e="T48" id="Seg_5478" s="T42">Ol ihin hanaːlara barbat osku͡olaga bi͡ererge. </ta>
            <ta e="T53" id="Seg_5479" s="T48">Bütte öhü bulallar ɨːtɨmaːrɨ ogolorun. </ta>
            <ta e="T60" id="Seg_5480" s="T53">Radavojdarɨgar ol künnerge olus erejdeːk üleni oŋorbuttara. </ta>
            <ta e="T72" id="Seg_5481" s="T60">Barɨ uraha dʼi͡e aːjɨ hɨldʼan kepsete hataːbɨt, ogolorun usku͡olaga bi͡eri͡ekterin, ü͡örekke ɨːtɨ͡aktarɨn. </ta>
            <ta e="T76" id="Seg_5482" s="T72">Itinten tu͡ok da taksɨbatak. </ta>
            <ta e="T91" id="Seg_5483" s="T76">Ol ihin hübe bu͡olbut – oŋoru͡orga di͡en munnʼagɨ mu͡ora ürdünen, barɨlara komullu͡oktarɨn hɨraj hɨrajga kepsetiː bu͡olu͡ogun. </ta>
            <ta e="T100" id="Seg_5484" s="T91">Biːr ulakan uraha dʼi͡ege araččɨ batan komullubuttar, turar turbat. </ta>
            <ta e="T110" id="Seg_5485" s="T100">Er kihiler tu͡ok da di͡ekterin berditten hir di͡ek agaj köröllör. </ta>
            <ta e="T116" id="Seg_5486" s="T110">Dʼaktar ajmak pɨlaːttarɨnan karaktarɨn uːlarɨn hotunallar. </ta>
            <ta e="T123" id="Seg_5487" s="T116">"Kebeler, tu͡ok bu͡olaŋŋɨt bastargɨtɨn tobuktargɨt ihiger kiːllerdigit? </ta>
            <ta e="T133" id="Seg_5488" s="T123">Togo nʼimaha hɨtagɨt", haŋarbɨt di͡en Dʼebgeːn haŋalaːk emeːksin, ü͡ögüleːn bi͡erbit. </ta>
            <ta e="T137" id="Seg_5489" s="T133">Gini oburgu balamata bert. </ta>
            <ta e="T140" id="Seg_5490" s="T137">Kimten da tolullubat. </ta>
            <ta e="T146" id="Seg_5491" s="T140">Oloror hirten orguːjakaːn turbut Parfiraj ogonnʼor. </ta>
            <ta e="T152" id="Seg_5492" s="T146">Melden meniːtin imerije tüheːt i͡edejbekke oduːlammɨt. </ta>
            <ta e="T154" id="Seg_5493" s="T152">"Kajtak di͡emij! </ta>
            <ta e="T157" id="Seg_5494" s="T154">Iti itigirdik bu͡olar. </ta>
            <ta e="T166" id="Seg_5495" s="T157">Ogolor bu haŋa üjege ü͡örenellere hin daːganɨ olus höpsöllöːk. </ta>
            <ta e="T170" id="Seg_5496" s="T166">Giniler huruksut bu͡olu͡ok tustaːktar. </ta>
            <ta e="T179" id="Seg_5497" s="T170">Bu min Ujbaːn u͡olbun oččugujuttan ü͡öreppitim balɨk, bult kapkaːnnɨ͡agɨn. </ta>
            <ta e="T182" id="Seg_5498" s="T179">Min anɨ kɨrɨjdɨm. </ta>
            <ta e="T186" id="Seg_5499" s="T182">Üleni kotummat bu͡olan erebin. </ta>
            <ta e="T190" id="Seg_5500" s="T186">Kim min kergemmin ahatɨ͡agaj? </ta>
            <ta e="T194" id="Seg_5501" s="T190">Mu͡orabɨt tɨmnɨː, olus purgaːlaːk. </ta>
            <ta e="T201" id="Seg_5502" s="T194">Küːsteːk ire ogo agaj kihi buldu bulaːččɨ. </ta>
            <ta e="T209" id="Seg_5503" s="T201">Ujbaːnɨ osku͡olaga ((PAUSE)) paːhɨ kapkaːnɨ iːte ü͡öreti͡ektere hu͡oga. </ta>
            <ta e="T213" id="Seg_5504" s="T209">Gini umnu͡oga mu͡ora ületin. </ta>
            <ta e="T221" id="Seg_5505" s="T213">Dʼaktar kördük dʼi͡ege agaj oloru͡oga, tuːgu da gɨmmakka." </ta>
            <ta e="T229" id="Seg_5506" s="T221">"En, ogonnʼor, dʼaktattarɨ tɨːtɨma", emi͡e Dʼebgeːn üːgüːte ihillibit. </ta>
            <ta e="T233" id="Seg_5507" s="T229">"Kim eni͡eke ahɨ belenniːr? </ta>
            <ta e="T239" id="Seg_5508" s="T233">Dʼi͡egin itiːtik tutar, ataktargɨn, taŋaskɨn abɨraktɨːr?" </ta>
            <ta e="T248" id="Seg_5509" s="T239">"Iti kirdik, itigirdik bu͡olar", olus ürpekke Parfʼiraj haŋara turbut. </ta>
            <ta e="T257" id="Seg_5510" s="T248">"Min Kɨːčaː kɨːhɨm astɨː hatɨːr, atagɨ, taŋahɨ oŋoro hatɨːr. </ta>
            <ta e="T262" id="Seg_5511" s="T257">Osku͡olaga hin daːgɨnɨ ontutun umnu͡oga. </ta>
            <ta e="T269" id="Seg_5512" s="T262">Er kihi kördük končoŋnuː hɨldʼɨ͡aga, dʼi͡eni tögürüje. </ta>
            <ta e="T274" id="Seg_5513" s="T269">Kajtak min oloru͡omuj ogolorum bardaktarɨna?" </ta>
            <ta e="T282" id="Seg_5514" s="T274">Radavoːja Anʼisʼim Nazaravʼičʼ Papov külümnüː tüheːt ogonnʼoru toktopput. </ta>
            <ta e="T287" id="Seg_5515" s="T282">"Dʼe, ogonnʼor, en hɨlajdɨŋ bɨhɨlaːk. </ta>
            <ta e="T289" id="Seg_5516" s="T287">Oloro tüs. </ta>
            <ta e="T292" id="Seg_5517" s="T289">Atɨn dʼon haŋardɨn." </ta>
            <ta e="T304" id="Seg_5518" s="T292">Ogonnʼor tu͡ok da di͡ebekke orguːjakaːn ologugar lak gɨna olorbut, tobuktarɨttan tutta tüheːt. </ta>
            <ta e="T311" id="Seg_5519" s="T304">"Kim össü͡ö tu͡ok haŋalaːgɨj", di͡en ɨjɨppɨt aradavojdara. </ta>
            <ta e="T321" id="Seg_5520" s="T311">Kimnere ere haŋararɨn küːte hataːn, aːkpɨt, kannuk ogoloro osku͡olaga barallarɨn. </ta>
            <ta e="T327" id="Seg_5521" s="T321">Kim kaːlarɨn osku͡olaga, barbatɨn, haːstara ulakattarɨ. </ta>
            <ta e="T334" id="Seg_5522" s="T327">Ol ogolorgo tübespit Parfʼiraj ogonnʼor u͡ola Ujbaːn. </ta>
            <ta e="T338" id="Seg_5523" s="T334">"Kɨːčaːnɨ emi͡e keːhi͡ekke tustaːk. </ta>
            <ta e="T346" id="Seg_5524" s="T338">Gini haːha hin daːgɨnɨ elbek", kördömmüt Parfʼiraj ogonnʼor. </ta>
            <ta e="T351" id="Seg_5525" s="T346">"Hu͡ok, Kɨːčaː barɨ͡a hu͡oga osku͡olaga." </ta>
            <ta e="T360" id="Seg_5526" s="T351">"Ogonnʼor, kečehime, ü͡örekteːk bu͡ollagɨna er kihi hɨ͡alɨjatɨn keti͡e hu͡oga. </ta>
            <ta e="T365" id="Seg_5527" s="T360">Dʼi͡eni tögürüččü končoŋnuː hɨldʼɨ͡a hu͡oga." </ta>
            <ta e="T371" id="Seg_5528" s="T365">Munnʼakka oloror dʼon ele isterinen külsübütter. </ta>
            <ta e="T377" id="Seg_5529" s="T371">"Barar ogoloru (belin-) belenneːŋ ötü͡ötük. </ta>
            <ta e="T381" id="Seg_5530" s="T377">Taŋɨnnarɨŋ taŋaraga taŋɨnnarar kördük. </ta>
            <ta e="T390" id="Seg_5531" s="T381">Ogolorbut bejem illi͡em Valačankattan", munnʼagɨ bütere gɨna di͡ebit hübehittere. </ta>
            <ta e="T395" id="Seg_5532" s="T390">Munnʼak ahaːtɨn komunuː ajdaːna elbeːbit. </ta>
            <ta e="T403" id="Seg_5533" s="T395">Dʼaktattar matagalarɨn ihitten tahartɨːllar ogolorun ki͡eŋ, ki͡eŋ taŋastarɨn. </ta>
            <ta e="T406" id="Seg_5534" s="T403">Taŋaraga keter ɨrbaːkɨlarɨn. </ta>
            <ta e="T414" id="Seg_5535" s="T406">Horoktor köstübet hirge tigettiːller ebit oččuguj taŋaralarɨn, kiri͡esteri. </ta>
            <ta e="T424" id="Seg_5536" s="T414">Ogolorun (či͡estijdar) barɨ ele minnʼiges astarɨnan, amahannan, kagɨnan, taba tɨlɨnan. </ta>
            <ta e="T433" id="Seg_5537" s="T424">Ki͡ehe dek (rada-) radavojugar ɨ͡allana kiːrbit Hemen ogonnʼor. </ta>
            <ta e="T437" id="Seg_5538" s="T433">Togo kelbitin bɨha haŋarbat. </ta>
            <ta e="T442" id="Seg_5539" s="T437">Čaːj ihe agaj oloron di͡ebit: </ta>
            <ta e="T447" id="Seg_5540" s="T442">"Bɨlɨr ehelerbit, agalarbɨt taŋarahɨt etilere. </ta>
            <ta e="T453" id="Seg_5541" s="T447">Ulakan hajtaːŋŋa bi͡ek belektiːr ideleːk etilere. </ta>
            <ta e="T458" id="Seg_5542" s="T453">Barɨ aːjmak ötü͡ötük oloru͡oktarɨn bagarallar. </ta>
            <ta e="T462" id="Seg_5543" s="T458">Tuːgunan da muskuraːbakka, ɨ͡aldʼɨbakka. </ta>
            <ta e="T470" id="Seg_5544" s="T462">Ulakan ojuttar kihi hɨldʼar hu͡olun bilellere bert ete. </ta>
            <ta e="T477" id="Seg_5545" s="T470">Bu olokpututtan ɨraːga hu͡ok baːr ulakan haːjtan-taːs. </ta>
            <ta e="T482" id="Seg_5546" s="T477">Barɨ kihi onno belegi bi͡ereːčči. </ta>
            <ta e="T489" id="Seg_5547" s="T482">Hotoru ogolorbut atɨn hirge barallar, atɨn kihilerge." </ta>
            <ta e="T496" id="Seg_5548" s="T489">Radavoja ɨ͡aldʼɨtɨn öhün ihilliː-ihilliː ihiger diː hanɨːr: </ta>
            <ta e="T501" id="Seg_5549" s="T496">"Tu͡ok hanaːtɨnan itinnik öhü tahaːrar?" </ta>
            <ta e="T505" id="Seg_5550" s="T501">ɨ͡aldʼɨta i͡edejbekke haŋartaːn ispit. </ta>
            <ta e="T510" id="Seg_5551" s="T505">"Dʼaktattar muŋnaːktar olus hoŋguraːtɨlar, ɨtahallar. </ta>
            <ta e="T517" id="Seg_5552" s="T510">Kɨra ogonu dʼi͡etten bɨldʼɨːr ide olus anʼɨːlaːk. </ta>
            <ta e="T520" id="Seg_5553" s="T517">Dʼon olus hanaːrgɨːr. </ta>
            <ta e="T524" id="Seg_5554" s="T520">Badaga, hajtaːŋŋa belek bi͡eri͡ekke." </ta>
            <ta e="T531" id="Seg_5555" s="T524">"Dʼe, ogonnʼor, minigin hiːleːme, itini gɨnɨ͡akpɨt hu͡oga. </ta>
            <ta e="T534" id="Seg_5556" s="T531">Usku͡ola hajtaːnɨ kördöːböt. </ta>
            <ta e="T541" id="Seg_5557" s="T534">Itigirdik barɨ kihi͡eke kepseː, hanargaːbatɨnnar", di͡ebit radavoja. </ta>
            <ta e="T548" id="Seg_5558" s="T541">Ogonnʼor tu͡ok da di͡ebekke čaːskɨtɨgar čaːj kutummut. </ta>
            <ta e="T556" id="Seg_5559" s="T548">"Bihigi, ogonnʼottor, kajtak ötü͡ö bu͡olu͡on di͡en, hübe bi͡erebit. </ta>
            <ta e="T562" id="Seg_5560" s="T556">Kirdik, horok kenne hɨːha haŋarabɨt bu͡olu͡oga. </ta>
            <ta e="T565" id="Seg_5561" s="T562">Dʼaktattar ɨtanallara bert. </ta>
            <ta e="T573" id="Seg_5562" s="T565">Kihi ü͡öregin ((PAUSE)) kihi ü͡öregin karɨjara olus hürdeːk. </ta>
            <ta e="T582" id="Seg_5563" s="T573">Olus kɨra ogolorgo ulakan dʼonu targatɨ͡akka, huptu körör bu͡olu͡oktarɨn. </ta>
            <ta e="T590" id="Seg_5564" s="T582">Olus oččuguj ogo tugut kördük, kanna bararɨn bilbet. </ta>
            <ta e="T593" id="Seg_5565" s="T590">Ötü͡ötük taŋna hataːbat." </ta>
            <ta e="T597" id="Seg_5566" s="T593">"Hiti kirdik", di͡ebit radavoja. </ta>
            <ta e="T603" id="Seg_5567" s="T597">Ogonnʼor bu munnʼak aːjdaːnɨgar umnan keːspit. </ta>
            <ta e="T612" id="Seg_5568" s="T603">"Kɨhɨn, haŋa dʼɨl keler kemiger ogolor hɨnnʼana keli͡ektere dʼi͡eleriger. </ta>
            <ta e="T623" id="Seg_5569" s="T612">Haːs, maːj ɨjɨgar, biːr dʼɨllaːgɨ ü͡örekterin büttekterine kühüŋŋe di͡eri dʼi͡eleriger bu͡olu͡oktara." </ta>
            <ta e="T629" id="Seg_5570" s="T623">"Iti öhü min barɨ kihi͡eke kepsi͡em. </ta>
            <ta e="T640" id="Seg_5571" s="T629">Kihiler diː hanɨːllar, kahan da tönnörbökkö ɨlallar ogoloru", ü͡örbüčče ogonnʼor haŋarbɨt. </ta>
            <ta e="T648" id="Seg_5572" s="T640">Ogonnʼor dʼi͡etiger barbɨtɨn kenne Anʼisʼim bejetin gɨtta kepseter: </ta>
            <ta e="T661" id="Seg_5573" s="T648">"Barɨ kihi͡eke öjdötön ihi͡ekke naːda, kajdak haŋa ɨjaːk ötü͡önü oŋororun tɨ͡a dʼonun inniger." </ta>
            <ta e="T668" id="Seg_5574" s="T661">Itinnik ogonnʼottor ös ɨːtallara kirdikteːk, kotuːlaːk bu͡olu͡oga. </ta>
            <ta e="T678" id="Seg_5575" s="T668">Nöŋü͡ö kün emi͡e munnʼakka kihiler komullubuttar ((PAUSE)) Hemen ogonnʼor dʼi͡etiger. </ta>
            <ta e="T684" id="Seg_5576" s="T678">Dʼon begeheŋiteːger kajtak da ü͡örbelere kötögüllübüt. </ta>
            <ta e="T689" id="Seg_5577" s="T684">Aːjdaːn, külsüː-halsɨː büteːtin radavojdara di͡ebit: </ta>
            <ta e="T696" id="Seg_5578" s="T689">"Bejegit hanaːgɨtɨttan kɨradaːj aːjmaktarɨ tuttarɨŋ ulakan ogolorgo. </ta>
            <ta e="T701" id="Seg_5579" s="T696">Köhör kemŋe körör bu͡olu͡oktarɨn ginileri." </ta>
            <ta e="T707" id="Seg_5580" s="T701">"Bihi͡eke di͡ebittere, ogolorbut kɨhɨn hɨnnʼana keli͡ektere. </ta>
            <ta e="T714" id="Seg_5581" s="T707">Iti kirdik haŋa du͡o", ɨjɨppɨt biːr dʼaktar. </ta>
            <ta e="T720" id="Seg_5582" s="T714">Hemen ogonnʼor oloror hiritten (öjdö-) öjdöŋnöːbüt. </ta>
            <ta e="T723" id="Seg_5583" s="T720">"Iti kirdik ös. </ta>
            <ta e="T734" id="Seg_5584" s="T723">Kɨhɨn hɨnnʼana keli͡ektere, onton hajɨn huptu dʼi͡eleriger bu͡ola keli͡ektere", di͡ebit Anʼisʼim. </ta>
            <ta e="T742" id="Seg_5585" s="T734">Hemen ogonnʼor nalɨja tüheːt haŋa (ɨll-) allɨbɨt: </ta>
            <ta e="T745" id="Seg_5586" s="T742">"Min tuːgu di͡ebitimij?" </ta>
            <ta e="T753" id="Seg_5587" s="T745">Barɨlara hɨlaːs karagɨnan körön, Hemen ogonnʼor di͡ek ergillibitter. </ta>
            <ta e="T756" id="Seg_5588" s="T753">Munnʼaktara aːspɨt türgennik. </ta>
            <ta e="T761" id="Seg_5589" s="T756">Kihiler uraha dʼi͡elerin aːjɨ barattaːbɨttar. </ta>
            <ta e="T770" id="Seg_5590" s="T761">Nöŋü͡ö kühün ü͡örderin hi͡egileːn baraːn, taba tutuːta bu͡olbut köhörgö. </ta>
            <ta e="T778" id="Seg_5591" s="T770">Dʼaktattar dʼi͡elerin attɨgar ogolorun gɨtta holoto hu͡oktar komunannar. </ta>
            <ta e="T787" id="Seg_5592" s="T778">Ogolorugar huptu haŋarallar, möŋpöt bu͡olu͡oktarɨn, ulakan ogoloru ister bu͡olu͡oktarɨn. </ta>
            <ta e="T796" id="Seg_5593" s="T787">Taba tutaːt, kölüneːt, uraha dʼi͡e aːjɨ ahɨːllar köhör inniger. </ta>
            <ta e="T807" id="Seg_5594" s="T796">Onton uraha dʼi͡e aːjɨttan barɨlara bargɨhan taksɨbɨttar, ɨraːk barar ogoloru ataːraːrɨ. </ta>
            <ta e="T813" id="Seg_5595" s="T807">"Kahan min enʼigin körü͡ömüj", Dʼebgeːn ɨtaːbɨt. </ta>
            <ta e="T820" id="Seg_5596" s="T813">"Kajtak enʼigin (hu͡o-) enʼigine hu͡ok oloru͡omuj? </ta>
            <ta e="T823" id="Seg_5597" s="T820">Kim bihi͡eke kömölöhü͡ögej?" </ta>
            <ta e="T830" id="Seg_5598" s="T823">Ogotun kam tutaːt baraːn dʼi͡etin di͡ek hohor. </ta>
            <ta e="T838" id="Seg_5599" s="T830">"En tu͡ok bu͡olagɨn", biːr biːr dʼaktar di͡ebit dogorugar. </ta>
            <ta e="T843" id="Seg_5600" s="T838">"Ogonu (hanaːrga) battɨma köhör inniger. </ta>
            <ta e="T847" id="Seg_5601" s="T843">Itigirdik ataːrar olus kuhagan. </ta>
            <ta e="T850" id="Seg_5602" s="T847">Anʼɨː, ataːr kihiliː." </ta>
            <ta e="T859" id="Seg_5603" s="T850">Barɨ uːčaktaːk ogolor ehelerin kennitten baran uhun kös bu͡olbuttar. </ta>
            <ta e="T866" id="Seg_5604" s="T859">Turbat-turar ogolorun ataːran ol kös kennitten huburujbuttar. </ta>
            <ta e="T876" id="Seg_5605" s="T866">Parfʼiraj ogonnʼor karaktarɨn bɨlčaččɨ körön turan ele küːhünen algɨː turbut. </ta>
            <ta e="T884" id="Seg_5606" s="T876">"Dʼe, eteŋŋe hɨldʼɨŋ, ehigi hu͡olgutugar kim da haːrappatɨn. </ta>
            <ta e="T887" id="Seg_5607" s="T884">Ulakan öjü ɨlɨŋ. </ta>
            <ta e="T891" id="Seg_5608" s="T887">Türgennik tönnüŋ töröːbüt hirgitiger!" </ta>
            <ta e="T906" id="Seg_5609" s="T891">Kaːlɨː dʼon öːr bagajɨ körö turbuttar mu͡ora ürdünen, uhun kös hütü͡ör di͡eri his ɨnaraː öttüger. </ta>
            <ta e="T914" id="Seg_5610" s="T906">Ogo aːjmakka törüttenne haŋa üje, ü͡örener üje hurukka. </ta>
            <ta e="T920" id="Seg_5611" s="T914">Ehigi ihilleːtigit öhü "Valačanka di͡ek köhüː". </ta>
            <ta e="T922" id="Seg_5612" s="T920">Aːkpɨta Papov. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_5613" s="T0">Valačanka</ta>
            <ta e="T2" id="Seg_5614" s="T1">di͡ek</ta>
            <ta e="T3" id="Seg_5615" s="T2">köh-üː</ta>
            <ta e="T4" id="Seg_5616" s="T3">ɨrgakta</ta>
            <ta e="T5" id="Seg_5617" s="T4">ɨj-a</ta>
            <ta e="T6" id="Seg_5618" s="T5">baran-ar</ta>
            <ta e="T7" id="Seg_5619" s="T6">kün-ner-e</ta>
            <ta e="T8" id="Seg_5620" s="T7">tur-but-tara</ta>
            <ta e="T9" id="Seg_5621" s="T8">itiː</ta>
            <ta e="T10" id="Seg_5622" s="T9">bagajɨ</ta>
            <ta e="T11" id="Seg_5623" s="T10">ot-tor</ta>
            <ta e="T12" id="Seg_5624" s="T11">hebirdek-ter</ta>
            <ta e="T13" id="Seg_5625" s="T12">kagdʼarɨj-bɨt-tar</ta>
            <ta e="T14" id="Seg_5626" s="T13">mu͡ora</ta>
            <ta e="T15" id="Seg_5627" s="T14">onno-manna</ta>
            <ta e="T16" id="Seg_5628" s="T15">kɨhɨl</ta>
            <ta e="T17" id="Seg_5629" s="T16">kömüs</ta>
            <ta e="T18" id="Seg_5630" s="T17">bu͡ol-an</ta>
            <ta e="T19" id="Seg_5631" s="T18">is-pit</ta>
            <ta e="T20" id="Seg_5632" s="T19">ol</ta>
            <ta e="T21" id="Seg_5633" s="T20">küt-ter-ge</ta>
            <ta e="T22" id="Seg_5634" s="T21">bil-iː-leːk</ta>
            <ta e="T23" id="Seg_5635" s="T22">ulakan</ta>
            <ta e="T24" id="Seg_5636" s="T23">kü͡öl</ta>
            <ta e="T25" id="Seg_5637" s="T24">kɨtɨl-ɨ-gar</ta>
            <ta e="T26" id="Seg_5638" s="T25">ulakan</ta>
            <ta e="T27" id="Seg_5639" s="T26">komuː-n-u</ta>
            <ta e="T28" id="Seg_5640" s="T27">tur-but-a</ta>
            <ta e="T29" id="Seg_5641" s="T28">ogo</ta>
            <ta e="T30" id="Seg_5642" s="T29">aːjmag-ɨ</ta>
            <ta e="T31" id="Seg_5643" s="T30">belenniː-l-ler</ta>
            <ta e="T32" id="Seg_5644" s="T31">Valačanka-ga</ta>
            <ta e="T33" id="Seg_5645" s="T32">ill-eːri</ta>
            <ta e="T34" id="Seg_5646" s="T33">ü͡örek-ke</ta>
            <ta e="T35" id="Seg_5647" s="T34">mu͡ora</ta>
            <ta e="T36" id="Seg_5648" s="T35">nehili͡ege</ta>
            <ta e="T37" id="Seg_5649" s="T36">urut</ta>
            <ta e="T38" id="Seg_5650" s="T37">bil-bet</ta>
            <ta e="T39" id="Seg_5651" s="T38">e-ti-lere</ta>
            <ta e="T40" id="Seg_5652" s="T39">ogo-lor-u-n</ta>
            <ta e="T41" id="Seg_5653" s="T40">ü͡örek-ke</ta>
            <ta e="T42" id="Seg_5654" s="T41">ɨːt-ar-ɨ</ta>
            <ta e="T43" id="Seg_5655" s="T42">ol</ta>
            <ta e="T44" id="Seg_5656" s="T43">ihin</ta>
            <ta e="T45" id="Seg_5657" s="T44">hanaː-lar-a</ta>
            <ta e="T46" id="Seg_5658" s="T45">bar-bat</ta>
            <ta e="T47" id="Seg_5659" s="T46">osku͡ola-ga</ta>
            <ta e="T48" id="Seg_5660" s="T47">bi͡er-er-ge</ta>
            <ta e="T49" id="Seg_5661" s="T48">bütte</ta>
            <ta e="T50" id="Seg_5662" s="T49">öh-ü</ta>
            <ta e="T51" id="Seg_5663" s="T50">bul-al-lar</ta>
            <ta e="T52" id="Seg_5664" s="T51">ɨːt-ɨ-m-aːrɨ</ta>
            <ta e="T53" id="Seg_5665" s="T52">ogo-lor-u-n</ta>
            <ta e="T54" id="Seg_5666" s="T53">radavoj-darɨ-gar</ta>
            <ta e="T55" id="Seg_5667" s="T54">ol</ta>
            <ta e="T56" id="Seg_5668" s="T55">kün-ner-ge</ta>
            <ta e="T57" id="Seg_5669" s="T56">olus</ta>
            <ta e="T58" id="Seg_5670" s="T57">erej-deːk</ta>
            <ta e="T59" id="Seg_5671" s="T58">üle-ni</ta>
            <ta e="T60" id="Seg_5672" s="T59">oŋor-but-tara</ta>
            <ta e="T61" id="Seg_5673" s="T60">barɨ</ta>
            <ta e="T62" id="Seg_5674" s="T61">uraha</ta>
            <ta e="T63" id="Seg_5675" s="T62">dʼi͡e</ta>
            <ta e="T64" id="Seg_5676" s="T63">aːjɨ</ta>
            <ta e="T65" id="Seg_5677" s="T64">hɨldʼ-an</ta>
            <ta e="T66" id="Seg_5678" s="T65">kepset-e</ta>
            <ta e="T67" id="Seg_5679" s="T66">hataː-bɨt</ta>
            <ta e="T68" id="Seg_5680" s="T67">ogo-lor-u-n</ta>
            <ta e="T69" id="Seg_5681" s="T68">usku͡ola-ga</ta>
            <ta e="T70" id="Seg_5682" s="T69">bi͡er-i͡ek-teri-n</ta>
            <ta e="T71" id="Seg_5683" s="T70">ü͡örek-ke</ta>
            <ta e="T72" id="Seg_5684" s="T71">ɨːt-ɨ͡ak-tarɨ-n</ta>
            <ta e="T73" id="Seg_5685" s="T72">itin-ten</ta>
            <ta e="T74" id="Seg_5686" s="T73">tu͡ok</ta>
            <ta e="T75" id="Seg_5687" s="T74">da</ta>
            <ta e="T76" id="Seg_5688" s="T75">taks-ɨ-batak</ta>
            <ta e="T77" id="Seg_5689" s="T76">ol</ta>
            <ta e="T78" id="Seg_5690" s="T77">ihin</ta>
            <ta e="T79" id="Seg_5691" s="T78">hübe</ta>
            <ta e="T80" id="Seg_5692" s="T79">bu͡ol-but</ta>
            <ta e="T81" id="Seg_5693" s="T80">oŋor-u͡or-ga</ta>
            <ta e="T82" id="Seg_5694" s="T81">di͡e-n</ta>
            <ta e="T83" id="Seg_5695" s="T82">munnʼag-ɨ</ta>
            <ta e="T84" id="Seg_5696" s="T83">mu͡ora</ta>
            <ta e="T85" id="Seg_5697" s="T84">ürd-ü-nen</ta>
            <ta e="T86" id="Seg_5698" s="T85">barɨ-lara</ta>
            <ta e="T87" id="Seg_5699" s="T86">komu-ll-u͡ok-tarɨ-n</ta>
            <ta e="T88" id="Seg_5700" s="T87">hɨraj</ta>
            <ta e="T89" id="Seg_5701" s="T88">hɨraj-ga</ta>
            <ta e="T90" id="Seg_5702" s="T89">kepset-iː</ta>
            <ta e="T91" id="Seg_5703" s="T90">bu͡ol-u͡og-u-n</ta>
            <ta e="T92" id="Seg_5704" s="T91">biːr</ta>
            <ta e="T93" id="Seg_5705" s="T92">ulakan</ta>
            <ta e="T94" id="Seg_5706" s="T93">uraha</ta>
            <ta e="T95" id="Seg_5707" s="T94">dʼi͡e-ge</ta>
            <ta e="T96" id="Seg_5708" s="T95">araččɨ</ta>
            <ta e="T97" id="Seg_5709" s="T96">bat-an</ta>
            <ta e="T98" id="Seg_5710" s="T97">komu-ll-u-but-tar</ta>
            <ta e="T99" id="Seg_5711" s="T98">tur-ar</ta>
            <ta e="T100" id="Seg_5712" s="T99">tur-bat</ta>
            <ta e="T101" id="Seg_5713" s="T100">er</ta>
            <ta e="T102" id="Seg_5714" s="T101">kihi-ler</ta>
            <ta e="T103" id="Seg_5715" s="T102">tu͡ok</ta>
            <ta e="T104" id="Seg_5716" s="T103">da</ta>
            <ta e="T105" id="Seg_5717" s="T104">d-i͡ek-teri-n</ta>
            <ta e="T106" id="Seg_5718" s="T105">berd-i-tten</ta>
            <ta e="T107" id="Seg_5719" s="T106">hir</ta>
            <ta e="T108" id="Seg_5720" s="T107">di͡ek</ta>
            <ta e="T109" id="Seg_5721" s="T108">agaj</ta>
            <ta e="T110" id="Seg_5722" s="T109">kör-öl-lör</ta>
            <ta e="T111" id="Seg_5723" s="T110">dʼaktar</ta>
            <ta e="T112" id="Seg_5724" s="T111">ajmak</ta>
            <ta e="T113" id="Seg_5725" s="T112">pɨlaːt-tarɨ-nan</ta>
            <ta e="T114" id="Seg_5726" s="T113">karak-tarɨ-n</ta>
            <ta e="T115" id="Seg_5727" s="T114">uː-larɨ-n</ta>
            <ta e="T116" id="Seg_5728" s="T115">hot-u-n-al-lar</ta>
            <ta e="T117" id="Seg_5729" s="T116">kebe-ler</ta>
            <ta e="T118" id="Seg_5730" s="T117">tu͡ok</ta>
            <ta e="T119" id="Seg_5731" s="T118">bu͡ol-aŋ-ŋɨt</ta>
            <ta e="T120" id="Seg_5732" s="T119">bas-tar-gɨtɨ-n</ta>
            <ta e="T121" id="Seg_5733" s="T120">tobuk-tar-gɨt</ta>
            <ta e="T122" id="Seg_5734" s="T121">ih-i-ger</ta>
            <ta e="T123" id="Seg_5735" s="T122">kiːl-ler-di-git</ta>
            <ta e="T124" id="Seg_5736" s="T123">togo</ta>
            <ta e="T125" id="Seg_5737" s="T124">nʼima-h-a</ta>
            <ta e="T126" id="Seg_5738" s="T125">hɨt-a-gɨt</ta>
            <ta e="T127" id="Seg_5739" s="T126">haŋar-bɨt</ta>
            <ta e="T128" id="Seg_5740" s="T127">di͡e-n</ta>
            <ta e="T129" id="Seg_5741" s="T128">Dʼebgeːn</ta>
            <ta e="T130" id="Seg_5742" s="T129">haŋa-laːk</ta>
            <ta e="T131" id="Seg_5743" s="T130">emeːksin</ta>
            <ta e="T132" id="Seg_5744" s="T131">ü͡ögüleː-n</ta>
            <ta e="T133" id="Seg_5745" s="T132">bi͡er-bit</ta>
            <ta e="T134" id="Seg_5746" s="T133">gini</ta>
            <ta e="T135" id="Seg_5747" s="T134">oburgu</ta>
            <ta e="T136" id="Seg_5748" s="T135">balamat-a</ta>
            <ta e="T137" id="Seg_5749" s="T136">bert</ta>
            <ta e="T138" id="Seg_5750" s="T137">kim-ten</ta>
            <ta e="T139" id="Seg_5751" s="T138">da</ta>
            <ta e="T140" id="Seg_5752" s="T139">tolull-u-bat</ta>
            <ta e="T141" id="Seg_5753" s="T140">olor-or</ta>
            <ta e="T142" id="Seg_5754" s="T141">hir-ten</ta>
            <ta e="T143" id="Seg_5755" s="T142">orguːjakaːn</ta>
            <ta e="T144" id="Seg_5756" s="T143">tur-but</ta>
            <ta e="T145" id="Seg_5757" s="T144">Parfiraj</ta>
            <ta e="T146" id="Seg_5758" s="T145">ogonnʼor</ta>
            <ta e="T147" id="Seg_5759" s="T146">melden</ta>
            <ta e="T148" id="Seg_5760" s="T147">meniː-ti-n</ta>
            <ta e="T149" id="Seg_5761" s="T148">imerij-e</ta>
            <ta e="T150" id="Seg_5762" s="T149">tüh-eːt</ta>
            <ta e="T151" id="Seg_5763" s="T150">i͡edej-bekke</ta>
            <ta e="T152" id="Seg_5764" s="T151">oduːl-a-m-mɨt</ta>
            <ta e="T153" id="Seg_5765" s="T152">kajtak</ta>
            <ta e="T154" id="Seg_5766" s="T153">d-i͡e-m=ij</ta>
            <ta e="T155" id="Seg_5767" s="T154">iti</ta>
            <ta e="T156" id="Seg_5768" s="T155">itigirdik</ta>
            <ta e="T157" id="Seg_5769" s="T156">bu͡ol-ar</ta>
            <ta e="T158" id="Seg_5770" s="T157">ogo-lor</ta>
            <ta e="T159" id="Seg_5771" s="T158">bu</ta>
            <ta e="T160" id="Seg_5772" s="T159">haŋa</ta>
            <ta e="T161" id="Seg_5773" s="T160">üje-ge</ta>
            <ta e="T162" id="Seg_5774" s="T161">ü͡ören-el-lere</ta>
            <ta e="T163" id="Seg_5775" s="T162">hin</ta>
            <ta e="T164" id="Seg_5776" s="T163">daːganɨ</ta>
            <ta e="T165" id="Seg_5777" s="T164">olus</ta>
            <ta e="T166" id="Seg_5778" s="T165">höpsöllöːk</ta>
            <ta e="T167" id="Seg_5779" s="T166">giniler</ta>
            <ta e="T168" id="Seg_5780" s="T167">huruk-sut</ta>
            <ta e="T169" id="Seg_5781" s="T168">bu͡ol-u͡ok</ta>
            <ta e="T170" id="Seg_5782" s="T169">tus-taːk-tar</ta>
            <ta e="T171" id="Seg_5783" s="T170">bu</ta>
            <ta e="T172" id="Seg_5784" s="T171">min</ta>
            <ta e="T173" id="Seg_5785" s="T172">Ujbaːn</ta>
            <ta e="T174" id="Seg_5786" s="T173">u͡ol-bu-n</ta>
            <ta e="T175" id="Seg_5787" s="T174">oččuguj-u-ttan</ta>
            <ta e="T176" id="Seg_5788" s="T175">ü͡örep-pit-i-m</ta>
            <ta e="T177" id="Seg_5789" s="T176">balɨk</ta>
            <ta e="T178" id="Seg_5790" s="T177">bult</ta>
            <ta e="T179" id="Seg_5791" s="T178">kapkaːn-n-ɨ͡ag-ɨ-n</ta>
            <ta e="T180" id="Seg_5792" s="T179">min</ta>
            <ta e="T181" id="Seg_5793" s="T180">anɨ</ta>
            <ta e="T182" id="Seg_5794" s="T181">kɨrɨj-dɨ-m</ta>
            <ta e="T183" id="Seg_5795" s="T182">üle-ni</ta>
            <ta e="T184" id="Seg_5796" s="T183">kot-u-m-mat</ta>
            <ta e="T185" id="Seg_5797" s="T184">bu͡ol-an</ta>
            <ta e="T186" id="Seg_5798" s="T185">er-e-bin</ta>
            <ta e="T187" id="Seg_5799" s="T186">kim</ta>
            <ta e="T188" id="Seg_5800" s="T187">min</ta>
            <ta e="T189" id="Seg_5801" s="T188">kergem-mi-n</ta>
            <ta e="T190" id="Seg_5802" s="T189">ah-a-t-ɨ͡ag-a=j</ta>
            <ta e="T191" id="Seg_5803" s="T190">mu͡ora-bɨt</ta>
            <ta e="T192" id="Seg_5804" s="T191">tɨmnɨː</ta>
            <ta e="T193" id="Seg_5805" s="T192">olus</ta>
            <ta e="T194" id="Seg_5806" s="T193">purgaː-laːk</ta>
            <ta e="T195" id="Seg_5807" s="T194">küːsteːk</ta>
            <ta e="T196" id="Seg_5808" s="T195">ire</ta>
            <ta e="T197" id="Seg_5809" s="T196">ogo</ta>
            <ta e="T198" id="Seg_5810" s="T197">agaj</ta>
            <ta e="T199" id="Seg_5811" s="T198">kihi</ta>
            <ta e="T200" id="Seg_5812" s="T199">buld-u</ta>
            <ta e="T201" id="Seg_5813" s="T200">bul-aːččɨ</ta>
            <ta e="T202" id="Seg_5814" s="T201">Ujbaːn-ɨ</ta>
            <ta e="T203" id="Seg_5815" s="T202">osku͡ola-ga</ta>
            <ta e="T205" id="Seg_5816" s="T204">paːh-ɨ</ta>
            <ta e="T206" id="Seg_5817" s="T205">kapkaːn-ɨ</ta>
            <ta e="T207" id="Seg_5818" s="T206">iːt-e</ta>
            <ta e="T208" id="Seg_5819" s="T207">ü͡öret-i͡ek-tere</ta>
            <ta e="T209" id="Seg_5820" s="T208">hu͡og-a</ta>
            <ta e="T210" id="Seg_5821" s="T209">gini</ta>
            <ta e="T211" id="Seg_5822" s="T210">umn-u͡og-a</ta>
            <ta e="T212" id="Seg_5823" s="T211">mu͡ora</ta>
            <ta e="T213" id="Seg_5824" s="T212">üle-ti-n</ta>
            <ta e="T214" id="Seg_5825" s="T213">dʼaktar</ta>
            <ta e="T215" id="Seg_5826" s="T214">kördük</ta>
            <ta e="T216" id="Seg_5827" s="T215">dʼi͡e-ge</ta>
            <ta e="T217" id="Seg_5828" s="T216">agaj</ta>
            <ta e="T218" id="Seg_5829" s="T217">olor-u͡og-a</ta>
            <ta e="T219" id="Seg_5830" s="T218">tuːg-u</ta>
            <ta e="T220" id="Seg_5831" s="T219">da</ta>
            <ta e="T221" id="Seg_5832" s="T220">gɨm-makka</ta>
            <ta e="T222" id="Seg_5833" s="T221">en</ta>
            <ta e="T223" id="Seg_5834" s="T222">ogonnʼor</ta>
            <ta e="T224" id="Seg_5835" s="T223">dʼaktat-tar-ɨ</ta>
            <ta e="T225" id="Seg_5836" s="T224">tɨːt-ɨ-ma</ta>
            <ta e="T226" id="Seg_5837" s="T225">emi͡e</ta>
            <ta e="T227" id="Seg_5838" s="T226">Dʼebgeːn</ta>
            <ta e="T228" id="Seg_5839" s="T227">üːgüː-te</ta>
            <ta e="T229" id="Seg_5840" s="T228">ihill-i-bit</ta>
            <ta e="T230" id="Seg_5841" s="T229">kim</ta>
            <ta e="T231" id="Seg_5842" s="T230">eni͡e-ke</ta>
            <ta e="T232" id="Seg_5843" s="T231">ah-ɨ</ta>
            <ta e="T233" id="Seg_5844" s="T232">belenniː-r</ta>
            <ta e="T234" id="Seg_5845" s="T233">dʼi͡e-gi-n</ta>
            <ta e="T235" id="Seg_5846" s="T234">itiː-tik</ta>
            <ta e="T236" id="Seg_5847" s="T235">tut-ar</ta>
            <ta e="T237" id="Seg_5848" s="T236">atak-tar-gɨ-n</ta>
            <ta e="T238" id="Seg_5849" s="T237">taŋas-kɨ-n</ta>
            <ta e="T239" id="Seg_5850" s="T238">abɨraktɨː-r</ta>
            <ta e="T240" id="Seg_5851" s="T239">iti</ta>
            <ta e="T241" id="Seg_5852" s="T240">kirdik</ta>
            <ta e="T242" id="Seg_5853" s="T241">itigirdik</ta>
            <ta e="T243" id="Seg_5854" s="T242">bu͡ol-ar</ta>
            <ta e="T244" id="Seg_5855" s="T243">olus</ta>
            <ta e="T245" id="Seg_5856" s="T244">ür-pekke</ta>
            <ta e="T246" id="Seg_5857" s="T245">Parfʼiraj</ta>
            <ta e="T247" id="Seg_5858" s="T246">haŋar-a</ta>
            <ta e="T248" id="Seg_5859" s="T247">tur-but</ta>
            <ta e="T249" id="Seg_5860" s="T248">min</ta>
            <ta e="T250" id="Seg_5861" s="T249">Kɨːčaː</ta>
            <ta e="T251" id="Seg_5862" s="T250">kɨːh-ɨ-m</ta>
            <ta e="T252" id="Seg_5863" s="T251">ast-ɨː</ta>
            <ta e="T253" id="Seg_5864" s="T252">hatɨː-r</ta>
            <ta e="T254" id="Seg_5865" s="T253">atag-ɨ</ta>
            <ta e="T255" id="Seg_5866" s="T254">taŋah-ɨ</ta>
            <ta e="T256" id="Seg_5867" s="T255">oŋor-o</ta>
            <ta e="T257" id="Seg_5868" s="T256">hatɨː-r</ta>
            <ta e="T258" id="Seg_5869" s="T257">osku͡ola-ga</ta>
            <ta e="T259" id="Seg_5870" s="T258">hin</ta>
            <ta e="T260" id="Seg_5871" s="T259">daːgɨnɨ</ta>
            <ta e="T261" id="Seg_5872" s="T260">on-tu-tu-n</ta>
            <ta e="T262" id="Seg_5873" s="T261">umn-u͡og-a</ta>
            <ta e="T263" id="Seg_5874" s="T262">er</ta>
            <ta e="T264" id="Seg_5875" s="T263">kihi</ta>
            <ta e="T265" id="Seg_5876" s="T264">kördük</ta>
            <ta e="T266" id="Seg_5877" s="T265">končo-ŋn-uː</ta>
            <ta e="T267" id="Seg_5878" s="T266">hɨldʼ-ɨ͡ag-a</ta>
            <ta e="T268" id="Seg_5879" s="T267">dʼi͡e-ni</ta>
            <ta e="T269" id="Seg_5880" s="T268">tögürüj-e</ta>
            <ta e="T270" id="Seg_5881" s="T269">kajtak</ta>
            <ta e="T271" id="Seg_5882" s="T270">min</ta>
            <ta e="T272" id="Seg_5883" s="T271">olor-u͡o-m=uj</ta>
            <ta e="T273" id="Seg_5884" s="T272">ogo-lor-u-m</ta>
            <ta e="T274" id="Seg_5885" s="T273">bar-dak-tarɨna</ta>
            <ta e="T275" id="Seg_5886" s="T274">radavoːj-a</ta>
            <ta e="T276" id="Seg_5887" s="T275">Anʼisʼim</ta>
            <ta e="T277" id="Seg_5888" s="T276">Nazaravʼičʼ</ta>
            <ta e="T278" id="Seg_5889" s="T277">Papov</ta>
            <ta e="T279" id="Seg_5890" s="T278">külümn-üː</ta>
            <ta e="T280" id="Seg_5891" s="T279">tüh-eːt</ta>
            <ta e="T281" id="Seg_5892" s="T280">ogonnʼor-u</ta>
            <ta e="T282" id="Seg_5893" s="T281">tokt-o-p-put</ta>
            <ta e="T283" id="Seg_5894" s="T282">dʼe</ta>
            <ta e="T284" id="Seg_5895" s="T283">ogonnʼor</ta>
            <ta e="T285" id="Seg_5896" s="T284">en</ta>
            <ta e="T286" id="Seg_5897" s="T285">hɨlaj-dɨ-ŋ</ta>
            <ta e="T287" id="Seg_5898" s="T286">bɨhɨlaːk</ta>
            <ta e="T288" id="Seg_5899" s="T287">olor-o</ta>
            <ta e="T289" id="Seg_5900" s="T288">tüs</ta>
            <ta e="T290" id="Seg_5901" s="T289">atɨn</ta>
            <ta e="T291" id="Seg_5902" s="T290">dʼon</ta>
            <ta e="T292" id="Seg_5903" s="T291">haŋar-dɨn</ta>
            <ta e="T293" id="Seg_5904" s="T292">ogonnʼor</ta>
            <ta e="T294" id="Seg_5905" s="T293">tu͡ok</ta>
            <ta e="T295" id="Seg_5906" s="T294">da</ta>
            <ta e="T296" id="Seg_5907" s="T295">di͡e-bekke</ta>
            <ta e="T297" id="Seg_5908" s="T296">orguːjakaːn</ta>
            <ta e="T298" id="Seg_5909" s="T297">olog-u-gar</ta>
            <ta e="T299" id="Seg_5910" s="T298">lak</ta>
            <ta e="T300" id="Seg_5911" s="T299">gɨn-a</ta>
            <ta e="T301" id="Seg_5912" s="T300">olor-but</ta>
            <ta e="T302" id="Seg_5913" s="T301">tobuk-tar-ɨ-ttan</ta>
            <ta e="T303" id="Seg_5914" s="T302">tutt-a</ta>
            <ta e="T304" id="Seg_5915" s="T303">tüh-eːt</ta>
            <ta e="T305" id="Seg_5916" s="T304">kim</ta>
            <ta e="T306" id="Seg_5917" s="T305">össü͡ö</ta>
            <ta e="T307" id="Seg_5918" s="T306">tu͡ok</ta>
            <ta e="T308" id="Seg_5919" s="T307">haŋa-laːg=ɨj</ta>
            <ta e="T309" id="Seg_5920" s="T308">di͡e-n</ta>
            <ta e="T310" id="Seg_5921" s="T309">ɨjɨp-pɨt</ta>
            <ta e="T311" id="Seg_5922" s="T310">aradavoj-dara</ta>
            <ta e="T312" id="Seg_5923" s="T311">kim-nere</ta>
            <ta e="T313" id="Seg_5924" s="T312">ere</ta>
            <ta e="T314" id="Seg_5925" s="T313">haŋar-ar-ɨ-n</ta>
            <ta e="T315" id="Seg_5926" s="T314">küːt-e</ta>
            <ta e="T316" id="Seg_5927" s="T315">hataː-n</ta>
            <ta e="T317" id="Seg_5928" s="T316">aːk-pɨt</ta>
            <ta e="T318" id="Seg_5929" s="T317">kannuk</ta>
            <ta e="T319" id="Seg_5930" s="T318">ogo-loro</ta>
            <ta e="T320" id="Seg_5931" s="T319">osku͡ola-ga</ta>
            <ta e="T321" id="Seg_5932" s="T320">bar-al-larɨ-n</ta>
            <ta e="T322" id="Seg_5933" s="T321">kim</ta>
            <ta e="T323" id="Seg_5934" s="T322">kaːl-ar-ɨ-n</ta>
            <ta e="T324" id="Seg_5935" s="T323">osku͡ola-ga</ta>
            <ta e="T325" id="Seg_5936" s="T324">bar-bat-ɨ-n</ta>
            <ta e="T326" id="Seg_5937" s="T325">haːs-tara</ta>
            <ta e="T327" id="Seg_5938" s="T326">ulakat-tar-ɨ</ta>
            <ta e="T328" id="Seg_5939" s="T327">ol</ta>
            <ta e="T329" id="Seg_5940" s="T328">ogo-lor-go</ta>
            <ta e="T330" id="Seg_5941" s="T329">tübes-pit</ta>
            <ta e="T331" id="Seg_5942" s="T330">Parfʼiraj</ta>
            <ta e="T332" id="Seg_5943" s="T331">ogonnʼor</ta>
            <ta e="T333" id="Seg_5944" s="T332">u͡ol-a</ta>
            <ta e="T334" id="Seg_5945" s="T333">Ujbaːn</ta>
            <ta e="T335" id="Seg_5946" s="T334">Kɨːčaː-nɨ</ta>
            <ta e="T336" id="Seg_5947" s="T335">emi͡e</ta>
            <ta e="T337" id="Seg_5948" s="T336">keːh-i͡ek-ke</ta>
            <ta e="T338" id="Seg_5949" s="T337">tus-taːk</ta>
            <ta e="T339" id="Seg_5950" s="T338">gini</ta>
            <ta e="T340" id="Seg_5951" s="T339">haːh-a</ta>
            <ta e="T341" id="Seg_5952" s="T340">hin</ta>
            <ta e="T342" id="Seg_5953" s="T341">daːgɨnɨ</ta>
            <ta e="T343" id="Seg_5954" s="T342">elbek</ta>
            <ta e="T344" id="Seg_5955" s="T343">körd-ö-m-müt</ta>
            <ta e="T345" id="Seg_5956" s="T344">Parfʼiraj</ta>
            <ta e="T346" id="Seg_5957" s="T345">ogonnʼor</ta>
            <ta e="T347" id="Seg_5958" s="T346">hu͡ok</ta>
            <ta e="T348" id="Seg_5959" s="T347">Kɨːčaː</ta>
            <ta e="T349" id="Seg_5960" s="T348">bar-ɨ͡a</ta>
            <ta e="T350" id="Seg_5961" s="T349">hu͡og-a</ta>
            <ta e="T351" id="Seg_5962" s="T350">osku͡ola-ga</ta>
            <ta e="T352" id="Seg_5963" s="T351">ogonnʼor</ta>
            <ta e="T353" id="Seg_5964" s="T352">kečeh-i-me</ta>
            <ta e="T354" id="Seg_5965" s="T353">ü͡örek-teːk</ta>
            <ta e="T355" id="Seg_5966" s="T354">bu͡ol-lag-ɨna</ta>
            <ta e="T356" id="Seg_5967" s="T355">er</ta>
            <ta e="T357" id="Seg_5968" s="T356">kihi</ta>
            <ta e="T358" id="Seg_5969" s="T357">hɨ͡alɨja-tɨ-n</ta>
            <ta e="T359" id="Seg_5970" s="T358">ket-i͡e</ta>
            <ta e="T360" id="Seg_5971" s="T359">hu͡og-a</ta>
            <ta e="T361" id="Seg_5972" s="T360">dʼi͡e-ni</ta>
            <ta e="T362" id="Seg_5973" s="T361">tögürüččü</ta>
            <ta e="T363" id="Seg_5974" s="T362">končo-ŋn-uː</ta>
            <ta e="T364" id="Seg_5975" s="T363">hɨldʼ-ɨ͡a</ta>
            <ta e="T365" id="Seg_5976" s="T364">hu͡og-a</ta>
            <ta e="T366" id="Seg_5977" s="T365">munnʼak-ka</ta>
            <ta e="T367" id="Seg_5978" s="T366">olor-or</ta>
            <ta e="T368" id="Seg_5979" s="T367">dʼon</ta>
            <ta e="T369" id="Seg_5980" s="T368">ele</ta>
            <ta e="T370" id="Seg_5981" s="T369">is-teri-nen</ta>
            <ta e="T371" id="Seg_5982" s="T370">kül-s-ü-büt-ter</ta>
            <ta e="T372" id="Seg_5983" s="T371">bar-ar</ta>
            <ta e="T373" id="Seg_5984" s="T372">ogo-lor-u</ta>
            <ta e="T376" id="Seg_5985" s="T375">belenneː-ŋ</ta>
            <ta e="T377" id="Seg_5986" s="T376">ötü͡ö-tük</ta>
            <ta e="T378" id="Seg_5987" s="T377">taŋɨn-nar-ɨ-ŋ</ta>
            <ta e="T379" id="Seg_5988" s="T378">taŋara-ga</ta>
            <ta e="T380" id="Seg_5989" s="T379">taŋɨn-nar-ar</ta>
            <ta e="T381" id="Seg_5990" s="T380">kördük</ta>
            <ta e="T382" id="Seg_5991" s="T381">ogo-lor-but</ta>
            <ta e="T383" id="Seg_5992" s="T382">beje-m</ta>
            <ta e="T384" id="Seg_5993" s="T383">ill-i͡e-m</ta>
            <ta e="T385" id="Seg_5994" s="T384">Valačanka-ttan</ta>
            <ta e="T386" id="Seg_5995" s="T385">munnʼag-ɨ</ta>
            <ta e="T387" id="Seg_5996" s="T386">büt-e-r-e</ta>
            <ta e="T388" id="Seg_5997" s="T387">gɨn-a</ta>
            <ta e="T389" id="Seg_5998" s="T388">di͡e-bit</ta>
            <ta e="T390" id="Seg_5999" s="T389">hübehit-tere</ta>
            <ta e="T391" id="Seg_6000" s="T390">munnʼak</ta>
            <ta e="T392" id="Seg_6001" s="T391">ah-aːt-ɨn</ta>
            <ta e="T393" id="Seg_6002" s="T392">komu-n-uː</ta>
            <ta e="T394" id="Seg_6003" s="T393">ajdaːn-a</ta>
            <ta e="T395" id="Seg_6004" s="T394">elbeː-bit</ta>
            <ta e="T396" id="Seg_6005" s="T395">dʼaktat-tar</ta>
            <ta e="T397" id="Seg_6006" s="T396">mataga-larɨ-n</ta>
            <ta e="T398" id="Seg_6007" s="T397">ih-i-tten</ta>
            <ta e="T399" id="Seg_6008" s="T398">tahar-tɨː-l-lar</ta>
            <ta e="T400" id="Seg_6009" s="T399">ogo-loru-n</ta>
            <ta e="T401" id="Seg_6010" s="T400">ki͡eŋ</ta>
            <ta e="T402" id="Seg_6011" s="T401">ki͡eŋ</ta>
            <ta e="T403" id="Seg_6012" s="T402">taŋas-tarɨ-n</ta>
            <ta e="T404" id="Seg_6013" s="T403">taŋara-ga</ta>
            <ta e="T405" id="Seg_6014" s="T404">ket-er</ta>
            <ta e="T406" id="Seg_6015" s="T405">ɨrbaːkɨ-larɨ-n</ta>
            <ta e="T407" id="Seg_6016" s="T406">horok-tor</ta>
            <ta e="T408" id="Seg_6017" s="T407">köst-ü-bet</ta>
            <ta e="T409" id="Seg_6018" s="T408">hir-ge</ta>
            <ta e="T410" id="Seg_6019" s="T409">tig-ettiː-l-ler</ta>
            <ta e="T411" id="Seg_6020" s="T410">e-bit</ta>
            <ta e="T412" id="Seg_6021" s="T411">oččuguj</ta>
            <ta e="T413" id="Seg_6022" s="T412">taŋara-larɨ-n</ta>
            <ta e="T414" id="Seg_6023" s="T413">kiri͡es-ter-i</ta>
            <ta e="T415" id="Seg_6024" s="T414">ogo-lor-u-n</ta>
            <ta e="T416" id="Seg_6025" s="T415">či͡estij-dar</ta>
            <ta e="T417" id="Seg_6026" s="T416">barɨ</ta>
            <ta e="T418" id="Seg_6027" s="T417">ele</ta>
            <ta e="T419" id="Seg_6028" s="T418">minnʼiges</ta>
            <ta e="T420" id="Seg_6029" s="T419">as-tar-ɨ-nan</ta>
            <ta e="T421" id="Seg_6030" s="T420">amaha-nnan</ta>
            <ta e="T422" id="Seg_6031" s="T421">kag-ɨ-nan</ta>
            <ta e="T423" id="Seg_6032" s="T422">taba</ta>
            <ta e="T424" id="Seg_6033" s="T423">tɨl-ɨ-nan</ta>
            <ta e="T425" id="Seg_6034" s="T424">ki͡ehe</ta>
            <ta e="T426" id="Seg_6035" s="T425">dek</ta>
            <ta e="T429" id="Seg_6036" s="T428">radavoj-u-gar</ta>
            <ta e="T430" id="Seg_6037" s="T429">ɨ͡allan-a</ta>
            <ta e="T431" id="Seg_6038" s="T430">kiːr-bit</ta>
            <ta e="T432" id="Seg_6039" s="T431">Hemen</ta>
            <ta e="T433" id="Seg_6040" s="T432">ogonnʼor</ta>
            <ta e="T434" id="Seg_6041" s="T433">togo</ta>
            <ta e="T435" id="Seg_6042" s="T434">kel-bit-i-n</ta>
            <ta e="T436" id="Seg_6043" s="T435">bɨha</ta>
            <ta e="T437" id="Seg_6044" s="T436">haŋar-bat</ta>
            <ta e="T438" id="Seg_6045" s="T437">čaːj</ta>
            <ta e="T439" id="Seg_6046" s="T438">ih-e</ta>
            <ta e="T440" id="Seg_6047" s="T439">agaj</ta>
            <ta e="T441" id="Seg_6048" s="T440">olor-on</ta>
            <ta e="T442" id="Seg_6049" s="T441">di͡e-bit</ta>
            <ta e="T443" id="Seg_6050" s="T442">bɨlɨr</ta>
            <ta e="T444" id="Seg_6051" s="T443">ehe-ler-bit</ta>
            <ta e="T445" id="Seg_6052" s="T444">aga-lar-bɨt</ta>
            <ta e="T446" id="Seg_6053" s="T445">taŋara-hɨt</ta>
            <ta e="T447" id="Seg_6054" s="T446">e-ti-lere</ta>
            <ta e="T448" id="Seg_6055" s="T447">ulakan</ta>
            <ta e="T449" id="Seg_6056" s="T448">hajtaːŋ-ŋa</ta>
            <ta e="T450" id="Seg_6057" s="T449">bi͡ek</ta>
            <ta e="T451" id="Seg_6058" s="T450">belek-tiː-r</ta>
            <ta e="T452" id="Seg_6059" s="T451">ide-leːk</ta>
            <ta e="T453" id="Seg_6060" s="T452">e-ti-lere</ta>
            <ta e="T454" id="Seg_6061" s="T453">barɨ</ta>
            <ta e="T455" id="Seg_6062" s="T454">aːjmak</ta>
            <ta e="T456" id="Seg_6063" s="T455">ötü͡ö-tük</ta>
            <ta e="T457" id="Seg_6064" s="T456">olor-u͡ok-tarɨ-n</ta>
            <ta e="T458" id="Seg_6065" s="T457">bagar-al-lar</ta>
            <ta e="T459" id="Seg_6066" s="T458">tuːg-u-nan</ta>
            <ta e="T460" id="Seg_6067" s="T459">da</ta>
            <ta e="T461" id="Seg_6068" s="T460">muskuraː-bakka</ta>
            <ta e="T462" id="Seg_6069" s="T461">ɨ͡aldʼ-ɨ-bakka</ta>
            <ta e="T463" id="Seg_6070" s="T462">ulakan</ta>
            <ta e="T464" id="Seg_6071" s="T463">ojut-tar</ta>
            <ta e="T465" id="Seg_6072" s="T464">kihi</ta>
            <ta e="T466" id="Seg_6073" s="T465">hɨldʼ-ar</ta>
            <ta e="T467" id="Seg_6074" s="T466">hu͡ol-u-n</ta>
            <ta e="T468" id="Seg_6075" s="T467">bil-el-lere</ta>
            <ta e="T469" id="Seg_6076" s="T468">bert</ta>
            <ta e="T470" id="Seg_6077" s="T469">e-t-e</ta>
            <ta e="T471" id="Seg_6078" s="T470">bu</ta>
            <ta e="T472" id="Seg_6079" s="T471">olok-putu-ttan</ta>
            <ta e="T473" id="Seg_6080" s="T472">ɨraːg-a</ta>
            <ta e="T474" id="Seg_6081" s="T473">hu͡ok</ta>
            <ta e="T475" id="Seg_6082" s="T474">baːr</ta>
            <ta e="T476" id="Seg_6083" s="T475">ulakan</ta>
            <ta e="T477" id="Seg_6084" s="T476">haːjtan-taːs</ta>
            <ta e="T478" id="Seg_6085" s="T477">barɨ</ta>
            <ta e="T479" id="Seg_6086" s="T478">kihi</ta>
            <ta e="T480" id="Seg_6087" s="T479">onno</ta>
            <ta e="T481" id="Seg_6088" s="T480">beleg-i</ta>
            <ta e="T482" id="Seg_6089" s="T481">bi͡er-eːčči</ta>
            <ta e="T483" id="Seg_6090" s="T482">hotoru</ta>
            <ta e="T484" id="Seg_6091" s="T483">ogo-lor-but</ta>
            <ta e="T485" id="Seg_6092" s="T484">atɨn</ta>
            <ta e="T486" id="Seg_6093" s="T485">hir-ge</ta>
            <ta e="T487" id="Seg_6094" s="T486">bar-al-lar</ta>
            <ta e="T488" id="Seg_6095" s="T487">atɨn</ta>
            <ta e="T489" id="Seg_6096" s="T488">kihi-ler-ge</ta>
            <ta e="T490" id="Seg_6097" s="T489">radavoj-a</ta>
            <ta e="T491" id="Seg_6098" s="T490">ɨ͡aldʼɨt-ɨ-n</ta>
            <ta e="T492" id="Seg_6099" s="T491">öh-ü-n</ta>
            <ta e="T493" id="Seg_6100" s="T492">ihill-iː-ihill-iː</ta>
            <ta e="T494" id="Seg_6101" s="T493">ih-i-ger</ta>
            <ta e="T495" id="Seg_6102" s="T494">d-iː</ta>
            <ta e="T496" id="Seg_6103" s="T495">hanɨː-r</ta>
            <ta e="T497" id="Seg_6104" s="T496">tu͡ok</ta>
            <ta e="T498" id="Seg_6105" s="T497">hanaː-tɨ-nan</ta>
            <ta e="T499" id="Seg_6106" s="T498">itinnik</ta>
            <ta e="T500" id="Seg_6107" s="T499">öh-ü</ta>
            <ta e="T501" id="Seg_6108" s="T500">tahaːr-ar</ta>
            <ta e="T502" id="Seg_6109" s="T501">ɨ͡aldʼɨt-a</ta>
            <ta e="T503" id="Seg_6110" s="T502">i͡edej-bekke</ta>
            <ta e="T504" id="Seg_6111" s="T503">haŋar-taː-n</ta>
            <ta e="T505" id="Seg_6112" s="T504">is-pit</ta>
            <ta e="T506" id="Seg_6113" s="T505">dʼaktat-tar</ta>
            <ta e="T507" id="Seg_6114" s="T506">muŋ-naːk-tar</ta>
            <ta e="T508" id="Seg_6115" s="T507">olus</ta>
            <ta e="T509" id="Seg_6116" s="T508">hoŋguraː-tɨ-lar</ta>
            <ta e="T510" id="Seg_6117" s="T509">ɨt-a-h-al-lar</ta>
            <ta e="T511" id="Seg_6118" s="T510">kɨra</ta>
            <ta e="T512" id="Seg_6119" s="T511">ogo-nu</ta>
            <ta e="T513" id="Seg_6120" s="T512">dʼi͡e-tten</ta>
            <ta e="T514" id="Seg_6121" s="T513">bɨldʼɨː-r</ta>
            <ta e="T515" id="Seg_6122" s="T514">ide</ta>
            <ta e="T516" id="Seg_6123" s="T515">olus</ta>
            <ta e="T517" id="Seg_6124" s="T516">anʼɨː-laːk</ta>
            <ta e="T518" id="Seg_6125" s="T517">dʼon</ta>
            <ta e="T519" id="Seg_6126" s="T518">olus</ta>
            <ta e="T520" id="Seg_6127" s="T519">hanaːrgɨː-r</ta>
            <ta e="T521" id="Seg_6128" s="T520">badaga</ta>
            <ta e="T522" id="Seg_6129" s="T521">hajtaːŋ-ŋa</ta>
            <ta e="T523" id="Seg_6130" s="T522">belek</ta>
            <ta e="T524" id="Seg_6131" s="T523">bi͡er-i͡ek-ke</ta>
            <ta e="T525" id="Seg_6132" s="T524">dʼe</ta>
            <ta e="T526" id="Seg_6133" s="T525">ogonnʼor</ta>
            <ta e="T527" id="Seg_6134" s="T526">minigi-n</ta>
            <ta e="T528" id="Seg_6135" s="T527">hiːleː-me</ta>
            <ta e="T529" id="Seg_6136" s="T528">iti-ni</ta>
            <ta e="T530" id="Seg_6137" s="T529">gɨn-ɨ͡ak-pɨt</ta>
            <ta e="T531" id="Seg_6138" s="T530">hu͡og-a</ta>
            <ta e="T532" id="Seg_6139" s="T531">usku͡ola</ta>
            <ta e="T533" id="Seg_6140" s="T532">hajtaːn-ɨ</ta>
            <ta e="T534" id="Seg_6141" s="T533">kördöː-böt</ta>
            <ta e="T535" id="Seg_6142" s="T534">itigirdik</ta>
            <ta e="T536" id="Seg_6143" s="T535">barɨ</ta>
            <ta e="T537" id="Seg_6144" s="T536">kihi͡e-ke</ta>
            <ta e="T538" id="Seg_6145" s="T537">kepseː</ta>
            <ta e="T539" id="Seg_6146" s="T538">hanargaː-ba-tɨnnar</ta>
            <ta e="T540" id="Seg_6147" s="T539">di͡e-bit</ta>
            <ta e="T541" id="Seg_6148" s="T540">radavoj-a</ta>
            <ta e="T542" id="Seg_6149" s="T541">ogonnʼor</ta>
            <ta e="T543" id="Seg_6150" s="T542">tu͡ok</ta>
            <ta e="T544" id="Seg_6151" s="T543">da</ta>
            <ta e="T545" id="Seg_6152" s="T544">di͡e-bekke</ta>
            <ta e="T546" id="Seg_6153" s="T545">čaːskɨ-tɨ-gar</ta>
            <ta e="T547" id="Seg_6154" s="T546">čaːj</ta>
            <ta e="T548" id="Seg_6155" s="T547">kut-u-m-mut</ta>
            <ta e="T549" id="Seg_6156" s="T548">bihigi</ta>
            <ta e="T550" id="Seg_6157" s="T549">ogonnʼot-tor</ta>
            <ta e="T551" id="Seg_6158" s="T550">kajtak</ta>
            <ta e="T552" id="Seg_6159" s="T551">ötü͡ö</ta>
            <ta e="T553" id="Seg_6160" s="T552">bu͡ol-u͡o-n</ta>
            <ta e="T554" id="Seg_6161" s="T553">di͡e-n</ta>
            <ta e="T555" id="Seg_6162" s="T554">hübe</ta>
            <ta e="T556" id="Seg_6163" s="T555">bi͡er-e-bit</ta>
            <ta e="T557" id="Seg_6164" s="T556">kirdik</ta>
            <ta e="T558" id="Seg_6165" s="T557">horok</ta>
            <ta e="T559" id="Seg_6166" s="T558">kenne</ta>
            <ta e="T560" id="Seg_6167" s="T559">hɨːha</ta>
            <ta e="T561" id="Seg_6168" s="T560">haŋar-a-bɨt</ta>
            <ta e="T562" id="Seg_6169" s="T561">bu͡ol-u͡og-a</ta>
            <ta e="T563" id="Seg_6170" s="T562">dʼaktat-tar</ta>
            <ta e="T564" id="Seg_6171" s="T563">ɨt-a-n-al-lara</ta>
            <ta e="T565" id="Seg_6172" s="T564">bert</ta>
            <ta e="T566" id="Seg_6173" s="T565">kihi</ta>
            <ta e="T567" id="Seg_6174" s="T566">ü͡öreg-i-n</ta>
            <ta e="T569" id="Seg_6175" s="T568">kihi</ta>
            <ta e="T570" id="Seg_6176" s="T569">ü͡öreg-i-n</ta>
            <ta e="T571" id="Seg_6177" s="T570">karɨj-ar-a</ta>
            <ta e="T572" id="Seg_6178" s="T571">olus</ta>
            <ta e="T573" id="Seg_6179" s="T572">hürdeːk</ta>
            <ta e="T574" id="Seg_6180" s="T573">olus</ta>
            <ta e="T575" id="Seg_6181" s="T574">kɨra</ta>
            <ta e="T576" id="Seg_6182" s="T575">ogo-lor-go</ta>
            <ta e="T577" id="Seg_6183" s="T576">ulakan</ta>
            <ta e="T578" id="Seg_6184" s="T577">dʼon-u</ta>
            <ta e="T579" id="Seg_6185" s="T578">targat-ɨ͡ak-ka</ta>
            <ta e="T580" id="Seg_6186" s="T579">huptu</ta>
            <ta e="T581" id="Seg_6187" s="T580">kör-ör</ta>
            <ta e="T582" id="Seg_6188" s="T581">bu͡ol-u͡ok-tarɨ-n</ta>
            <ta e="T583" id="Seg_6189" s="T582">olus</ta>
            <ta e="T584" id="Seg_6190" s="T583">oččuguj</ta>
            <ta e="T585" id="Seg_6191" s="T584">ogo</ta>
            <ta e="T586" id="Seg_6192" s="T585">tugut</ta>
            <ta e="T587" id="Seg_6193" s="T586">kördük</ta>
            <ta e="T588" id="Seg_6194" s="T587">kanna</ta>
            <ta e="T589" id="Seg_6195" s="T588">bar-ar-ɨ-n</ta>
            <ta e="T590" id="Seg_6196" s="T589">bil-bet</ta>
            <ta e="T591" id="Seg_6197" s="T590">ötü͡ö-tük</ta>
            <ta e="T592" id="Seg_6198" s="T591">taŋn-a</ta>
            <ta e="T593" id="Seg_6199" s="T592">hataː-bat</ta>
            <ta e="T594" id="Seg_6200" s="T593">hiti</ta>
            <ta e="T595" id="Seg_6201" s="T594">kirdik</ta>
            <ta e="T596" id="Seg_6202" s="T595">di͡e-bit</ta>
            <ta e="T597" id="Seg_6203" s="T596">radavoj-a</ta>
            <ta e="T598" id="Seg_6204" s="T597">ogonnʼor</ta>
            <ta e="T599" id="Seg_6205" s="T598">bu</ta>
            <ta e="T600" id="Seg_6206" s="T599">munnʼak</ta>
            <ta e="T601" id="Seg_6207" s="T600">aːjdaːn-ɨ-gar</ta>
            <ta e="T602" id="Seg_6208" s="T601">umn-an</ta>
            <ta e="T603" id="Seg_6209" s="T602">keːs-pit</ta>
            <ta e="T604" id="Seg_6210" s="T603">kɨhɨn</ta>
            <ta e="T605" id="Seg_6211" s="T604">haŋa</ta>
            <ta e="T606" id="Seg_6212" s="T605">dʼɨl</ta>
            <ta e="T607" id="Seg_6213" s="T606">kel-er</ta>
            <ta e="T608" id="Seg_6214" s="T607">kem-i-ger</ta>
            <ta e="T609" id="Seg_6215" s="T608">ogo-lor</ta>
            <ta e="T610" id="Seg_6216" s="T609">hɨnnʼan-a</ta>
            <ta e="T611" id="Seg_6217" s="T610">kel-i͡ek-tere</ta>
            <ta e="T612" id="Seg_6218" s="T611">dʼi͡e-leri-ger</ta>
            <ta e="T613" id="Seg_6219" s="T612">haːs</ta>
            <ta e="T614" id="Seg_6220" s="T613">maːj</ta>
            <ta e="T615" id="Seg_6221" s="T614">ɨj-ɨ-gar</ta>
            <ta e="T616" id="Seg_6222" s="T615">biːr</ta>
            <ta e="T617" id="Seg_6223" s="T616">dʼɨl-laːgɨ</ta>
            <ta e="T618" id="Seg_6224" s="T617">ü͡örek-teri-n</ta>
            <ta e="T619" id="Seg_6225" s="T618">büt-tek-terine</ta>
            <ta e="T620" id="Seg_6226" s="T619">kühüŋ-ŋe</ta>
            <ta e="T621" id="Seg_6227" s="T620">di͡eri</ta>
            <ta e="T622" id="Seg_6228" s="T621">dʼi͡e-leri-ger</ta>
            <ta e="T623" id="Seg_6229" s="T622">bu͡ol-u͡ok-tara</ta>
            <ta e="T624" id="Seg_6230" s="T623">iti</ta>
            <ta e="T625" id="Seg_6231" s="T624">öh-ü</ta>
            <ta e="T626" id="Seg_6232" s="T625">min</ta>
            <ta e="T627" id="Seg_6233" s="T626">barɨ</ta>
            <ta e="T628" id="Seg_6234" s="T627">kihi͡e-ke</ta>
            <ta e="T629" id="Seg_6235" s="T628">keps-i͡e-m</ta>
            <ta e="T630" id="Seg_6236" s="T629">kihi-ler</ta>
            <ta e="T631" id="Seg_6237" s="T630">d-iː</ta>
            <ta e="T632" id="Seg_6238" s="T631">hanɨː-l-lar</ta>
            <ta e="T633" id="Seg_6239" s="T632">kahan</ta>
            <ta e="T634" id="Seg_6240" s="T633">da</ta>
            <ta e="T635" id="Seg_6241" s="T634">tönn-ö-r-bökkö</ta>
            <ta e="T636" id="Seg_6242" s="T635">ɨl-al-lar</ta>
            <ta e="T637" id="Seg_6243" s="T636">ogo-lor-u</ta>
            <ta e="T638" id="Seg_6244" s="T637">ü͡ör-büčče</ta>
            <ta e="T639" id="Seg_6245" s="T638">ogonnʼor</ta>
            <ta e="T640" id="Seg_6246" s="T639">haŋar-bɨt</ta>
            <ta e="T641" id="Seg_6247" s="T640">ogonnʼor</ta>
            <ta e="T642" id="Seg_6248" s="T641">dʼi͡e-ti-ger</ta>
            <ta e="T643" id="Seg_6249" s="T642">bar-bɨt-ɨ-n</ta>
            <ta e="T644" id="Seg_6250" s="T643">kenne</ta>
            <ta e="T645" id="Seg_6251" s="T644">Anʼisʼim</ta>
            <ta e="T646" id="Seg_6252" s="T645">beje-ti-n</ta>
            <ta e="T647" id="Seg_6253" s="T646">gɨtta</ta>
            <ta e="T648" id="Seg_6254" s="T647">kepset-er</ta>
            <ta e="T649" id="Seg_6255" s="T648">barɨ</ta>
            <ta e="T650" id="Seg_6256" s="T649">kihi͡e-ke</ta>
            <ta e="T651" id="Seg_6257" s="T650">öjd-ö-t-ön</ta>
            <ta e="T652" id="Seg_6258" s="T651">ih-i͡ek-ke</ta>
            <ta e="T653" id="Seg_6259" s="T652">naːda</ta>
            <ta e="T654" id="Seg_6260" s="T653">kajdak</ta>
            <ta e="T655" id="Seg_6261" s="T654">haŋa</ta>
            <ta e="T656" id="Seg_6262" s="T655">ɨjaːk</ta>
            <ta e="T657" id="Seg_6263" s="T656">ötü͡ö-nü</ta>
            <ta e="T658" id="Seg_6264" s="T657">oŋor-or-u-n</ta>
            <ta e="T659" id="Seg_6265" s="T658">tɨ͡a</ta>
            <ta e="T660" id="Seg_6266" s="T659">dʼon-u-n</ta>
            <ta e="T661" id="Seg_6267" s="T660">inn-i-ger</ta>
            <ta e="T662" id="Seg_6268" s="T661">itinnik</ta>
            <ta e="T663" id="Seg_6269" s="T662">ogonnʼot-tor</ta>
            <ta e="T664" id="Seg_6270" s="T663">ös</ta>
            <ta e="T665" id="Seg_6271" s="T664">ɨːt-al-lara</ta>
            <ta e="T666" id="Seg_6272" s="T665">kirdik-teːk</ta>
            <ta e="T667" id="Seg_6273" s="T666">kotuː-laːk</ta>
            <ta e="T668" id="Seg_6274" s="T667">bu͡ol-u͡og-a</ta>
            <ta e="T669" id="Seg_6275" s="T668">nöŋü͡ö</ta>
            <ta e="T670" id="Seg_6276" s="T669">kün</ta>
            <ta e="T671" id="Seg_6277" s="T670">emi͡e</ta>
            <ta e="T672" id="Seg_6278" s="T671">munnʼak-ka</ta>
            <ta e="T673" id="Seg_6279" s="T672">kihi-ler</ta>
            <ta e="T674" id="Seg_6280" s="T673">komu-ll-u-but-tar</ta>
            <ta e="T676" id="Seg_6281" s="T675">Hemen</ta>
            <ta e="T677" id="Seg_6282" s="T676">ogonnʼor</ta>
            <ta e="T678" id="Seg_6283" s="T677">dʼi͡e-ti-ger</ta>
            <ta e="T679" id="Seg_6284" s="T678">dʼon</ta>
            <ta e="T680" id="Seg_6285" s="T679">begehe-ŋi-teːger</ta>
            <ta e="T681" id="Seg_6286" s="T680">kajtak</ta>
            <ta e="T682" id="Seg_6287" s="T681">da</ta>
            <ta e="T683" id="Seg_6288" s="T682">ü͡örbe-lere</ta>
            <ta e="T684" id="Seg_6289" s="T683">kötög-ü-ll-ü-büt</ta>
            <ta e="T685" id="Seg_6290" s="T684">aːjdaːn</ta>
            <ta e="T686" id="Seg_6291" s="T685">kül-s-üː-hal-s-ɨː</ta>
            <ta e="T687" id="Seg_6292" s="T686">büt-eːt-in</ta>
            <ta e="T688" id="Seg_6293" s="T687">radavoj-dara</ta>
            <ta e="T689" id="Seg_6294" s="T688">di͡e-bit</ta>
            <ta e="T690" id="Seg_6295" s="T689">beje-git</ta>
            <ta e="T691" id="Seg_6296" s="T690">hanaː-gɨtɨ-ttan</ta>
            <ta e="T692" id="Seg_6297" s="T691">kɨradaːj</ta>
            <ta e="T693" id="Seg_6298" s="T692">aːjmak-tar-ɨ</ta>
            <ta e="T694" id="Seg_6299" s="T693">tut-tar-ɨ-ŋ</ta>
            <ta e="T695" id="Seg_6300" s="T694">ulakan</ta>
            <ta e="T696" id="Seg_6301" s="T695">ogo-lor-go</ta>
            <ta e="T697" id="Seg_6302" s="T696">köh-ör</ta>
            <ta e="T698" id="Seg_6303" s="T697">kem-ŋe</ta>
            <ta e="T699" id="Seg_6304" s="T698">kör-ör</ta>
            <ta e="T700" id="Seg_6305" s="T699">bu͡ol-u͡ok-tarɨ-n</ta>
            <ta e="T701" id="Seg_6306" s="T700">giniler-i</ta>
            <ta e="T702" id="Seg_6307" s="T701">bihi͡e-ke</ta>
            <ta e="T703" id="Seg_6308" s="T702">di͡e-bit-tere</ta>
            <ta e="T704" id="Seg_6309" s="T703">ogo-lor-but</ta>
            <ta e="T705" id="Seg_6310" s="T704">kɨhɨn</ta>
            <ta e="T706" id="Seg_6311" s="T705">hɨnnʼan-a</ta>
            <ta e="T707" id="Seg_6312" s="T706">kel-i͡ek-tere</ta>
            <ta e="T708" id="Seg_6313" s="T707">iti</ta>
            <ta e="T709" id="Seg_6314" s="T708">kirdik</ta>
            <ta e="T710" id="Seg_6315" s="T709">haŋa</ta>
            <ta e="T711" id="Seg_6316" s="T710">du͡o</ta>
            <ta e="T712" id="Seg_6317" s="T711">ɨjɨp-pɨt</ta>
            <ta e="T713" id="Seg_6318" s="T712">biːr</ta>
            <ta e="T714" id="Seg_6319" s="T713">dʼaktar</ta>
            <ta e="T715" id="Seg_6320" s="T714">Hemen</ta>
            <ta e="T716" id="Seg_6321" s="T715">ogonnʼor</ta>
            <ta e="T717" id="Seg_6322" s="T716">olor-or</ta>
            <ta e="T718" id="Seg_6323" s="T717">hir-i-tten</ta>
            <ta e="T719" id="Seg_6324" s="T718">öjd-ö</ta>
            <ta e="T720" id="Seg_6325" s="T719">öjd-ö-ŋnöː-büt</ta>
            <ta e="T721" id="Seg_6326" s="T720">iti</ta>
            <ta e="T722" id="Seg_6327" s="T721">kirdik</ta>
            <ta e="T723" id="Seg_6328" s="T722">ös</ta>
            <ta e="T724" id="Seg_6329" s="T723">kɨhɨn</ta>
            <ta e="T725" id="Seg_6330" s="T724">hɨnnʼan-a</ta>
            <ta e="T726" id="Seg_6331" s="T725">kel-i͡ek-tere</ta>
            <ta e="T727" id="Seg_6332" s="T726">onton</ta>
            <ta e="T728" id="Seg_6333" s="T727">hajɨn</ta>
            <ta e="T729" id="Seg_6334" s="T728">huptu</ta>
            <ta e="T730" id="Seg_6335" s="T729">dʼi͡e-leri-ger</ta>
            <ta e="T731" id="Seg_6336" s="T730">bu͡ol-a</ta>
            <ta e="T732" id="Seg_6337" s="T731">kel-i͡ek-tere</ta>
            <ta e="T733" id="Seg_6338" s="T732">di͡e-bit</ta>
            <ta e="T734" id="Seg_6339" s="T733">Anʼisʼim</ta>
            <ta e="T735" id="Seg_6340" s="T734">Hemen</ta>
            <ta e="T736" id="Seg_6341" s="T735">ogonnʼor</ta>
            <ta e="T737" id="Seg_6342" s="T736">nalɨj-a</ta>
            <ta e="T738" id="Seg_6343" s="T737">tüh-eːt</ta>
            <ta e="T739" id="Seg_6344" s="T738">haŋa</ta>
            <ta e="T742" id="Seg_6345" s="T741">all-ɨ-bɨt</ta>
            <ta e="T743" id="Seg_6346" s="T742">min</ta>
            <ta e="T744" id="Seg_6347" s="T743">tuːg-u</ta>
            <ta e="T745" id="Seg_6348" s="T744">di͡e-bit-i-m=ij</ta>
            <ta e="T746" id="Seg_6349" s="T745">barɨ-lara</ta>
            <ta e="T747" id="Seg_6350" s="T746">hɨlaːs</ta>
            <ta e="T748" id="Seg_6351" s="T747">karag-ɨ-nan</ta>
            <ta e="T749" id="Seg_6352" s="T748">kör-ön</ta>
            <ta e="T750" id="Seg_6353" s="T749">Hemen</ta>
            <ta e="T751" id="Seg_6354" s="T750">ogonnʼor</ta>
            <ta e="T752" id="Seg_6355" s="T751">di͡ek</ta>
            <ta e="T753" id="Seg_6356" s="T752">ergi-ll-i-bit-ter</ta>
            <ta e="T754" id="Seg_6357" s="T753">munnʼak-tara</ta>
            <ta e="T755" id="Seg_6358" s="T754">aːs-pɨt</ta>
            <ta e="T756" id="Seg_6359" s="T755">türgen-nik</ta>
            <ta e="T757" id="Seg_6360" s="T756">kihi-ler</ta>
            <ta e="T758" id="Seg_6361" s="T757">uraha</ta>
            <ta e="T759" id="Seg_6362" s="T758">dʼi͡e-leri-n</ta>
            <ta e="T760" id="Seg_6363" s="T759">aːjɨ</ta>
            <ta e="T761" id="Seg_6364" s="T760">bar-attaː-bɨt-tar</ta>
            <ta e="T762" id="Seg_6365" s="T761">nöŋü͡ö</ta>
            <ta e="T763" id="Seg_6366" s="T762">kühün</ta>
            <ta e="T764" id="Seg_6367" s="T763">ü͡ör-deri-n</ta>
            <ta e="T765" id="Seg_6368" s="T764">hi͡egileː-n</ta>
            <ta e="T766" id="Seg_6369" s="T765">baraːn</ta>
            <ta e="T767" id="Seg_6370" s="T766">taba</ta>
            <ta e="T768" id="Seg_6371" s="T767">tut-uː-ta</ta>
            <ta e="T769" id="Seg_6372" s="T768">bu͡ol-but</ta>
            <ta e="T770" id="Seg_6373" s="T769">köh-ör-gö</ta>
            <ta e="T771" id="Seg_6374" s="T770">dʼaktat-tar</ta>
            <ta e="T772" id="Seg_6375" s="T771">dʼi͡e-leri-n</ta>
            <ta e="T773" id="Seg_6376" s="T772">attɨ-gar</ta>
            <ta e="T774" id="Seg_6377" s="T773">ogo-lor-u-n</ta>
            <ta e="T775" id="Seg_6378" s="T774">gɨtta</ta>
            <ta e="T776" id="Seg_6379" s="T775">holo-to</ta>
            <ta e="T777" id="Seg_6380" s="T776">hu͡ok-tar</ta>
            <ta e="T778" id="Seg_6381" s="T777">komu-n-an-nar</ta>
            <ta e="T779" id="Seg_6382" s="T778">ogo-lor-u-gar</ta>
            <ta e="T780" id="Seg_6383" s="T779">huptu</ta>
            <ta e="T781" id="Seg_6384" s="T780">haŋar-al-lar</ta>
            <ta e="T782" id="Seg_6385" s="T781">möŋ-pöt</ta>
            <ta e="T783" id="Seg_6386" s="T782">bu͡ol-u͡ok-tarɨ-n</ta>
            <ta e="T784" id="Seg_6387" s="T783">ulakan</ta>
            <ta e="T785" id="Seg_6388" s="T784">ogo-lor-u</ta>
            <ta e="T786" id="Seg_6389" s="T785">ist-er</ta>
            <ta e="T787" id="Seg_6390" s="T786">bu͡ol-u͡ok-tarɨ-n</ta>
            <ta e="T788" id="Seg_6391" s="T787">taba</ta>
            <ta e="T789" id="Seg_6392" s="T788">tut-aːt</ta>
            <ta e="T790" id="Seg_6393" s="T789">kölün-eːt</ta>
            <ta e="T791" id="Seg_6394" s="T790">uraha</ta>
            <ta e="T792" id="Seg_6395" s="T791">dʼi͡e</ta>
            <ta e="T793" id="Seg_6396" s="T792">aːjɨ</ta>
            <ta e="T794" id="Seg_6397" s="T793">ahɨː-l-lar</ta>
            <ta e="T795" id="Seg_6398" s="T794">köh-ör</ta>
            <ta e="T796" id="Seg_6399" s="T795">inn-i-ger</ta>
            <ta e="T797" id="Seg_6400" s="T796">onton</ta>
            <ta e="T798" id="Seg_6401" s="T797">uraha</ta>
            <ta e="T799" id="Seg_6402" s="T798">dʼi͡e</ta>
            <ta e="T800" id="Seg_6403" s="T799">aːjɨ-ttan</ta>
            <ta e="T801" id="Seg_6404" s="T800">barɨ-lara</ta>
            <ta e="T802" id="Seg_6405" s="T801">bargɨ-h-an</ta>
            <ta e="T803" id="Seg_6406" s="T802">taks-ɨ-bɨt-tar</ta>
            <ta e="T804" id="Seg_6407" s="T803">ɨraːk</ta>
            <ta e="T805" id="Seg_6408" s="T804">bar-ar</ta>
            <ta e="T806" id="Seg_6409" s="T805">ogo-lor-u</ta>
            <ta e="T807" id="Seg_6410" s="T806">ataːr-aːrɨ</ta>
            <ta e="T808" id="Seg_6411" s="T807">kahan</ta>
            <ta e="T809" id="Seg_6412" s="T808">min</ta>
            <ta e="T810" id="Seg_6413" s="T809">enʼigi-n</ta>
            <ta e="T811" id="Seg_6414" s="T810">kör-ü͡ö-m=üj</ta>
            <ta e="T812" id="Seg_6415" s="T811">Dʼebgeːn</ta>
            <ta e="T813" id="Seg_6416" s="T812">ɨtaː-bɨt</ta>
            <ta e="T814" id="Seg_6417" s="T813">kajtak</ta>
            <ta e="T815" id="Seg_6418" s="T814">enʼigi-n</ta>
            <ta e="T818" id="Seg_6419" s="T817">enʼigi-n-e</ta>
            <ta e="T819" id="Seg_6420" s="T818">hu͡ok</ta>
            <ta e="T820" id="Seg_6421" s="T819">olor-u͡o-m=uj</ta>
            <ta e="T821" id="Seg_6422" s="T820">kim</ta>
            <ta e="T822" id="Seg_6423" s="T821">bihi͡e-ke</ta>
            <ta e="T823" id="Seg_6424" s="T822">kömölöh-ü͡ög-e=j</ta>
            <ta e="T824" id="Seg_6425" s="T823">ogo-tu-n</ta>
            <ta e="T825" id="Seg_6426" s="T824">kam</ta>
            <ta e="T826" id="Seg_6427" s="T825">tut-aːt</ta>
            <ta e="T827" id="Seg_6428" s="T826">baraːn</ta>
            <ta e="T828" id="Seg_6429" s="T827">dʼi͡e-ti-n</ta>
            <ta e="T829" id="Seg_6430" s="T828">di͡ek</ta>
            <ta e="T830" id="Seg_6431" s="T829">hoh-or</ta>
            <ta e="T831" id="Seg_6432" s="T830">en</ta>
            <ta e="T832" id="Seg_6433" s="T831">tu͡ok</ta>
            <ta e="T833" id="Seg_6434" s="T832">bu͡ol-a-gɨn</ta>
            <ta e="T834" id="Seg_6435" s="T833">biːr</ta>
            <ta e="T835" id="Seg_6436" s="T834">biːr</ta>
            <ta e="T836" id="Seg_6437" s="T835">dʼaktar</ta>
            <ta e="T837" id="Seg_6438" s="T836">di͡e-bit</ta>
            <ta e="T838" id="Seg_6439" s="T837">dogor-u-gar</ta>
            <ta e="T839" id="Seg_6440" s="T838">ogo-nu</ta>
            <ta e="T840" id="Seg_6441" s="T839">hanaː-r-ga</ta>
            <ta e="T841" id="Seg_6442" s="T840">batt-ɨ-ma</ta>
            <ta e="T842" id="Seg_6443" s="T841">köh-ör</ta>
            <ta e="T843" id="Seg_6444" s="T842">inn-i-ger</ta>
            <ta e="T844" id="Seg_6445" s="T843">itigirdik</ta>
            <ta e="T845" id="Seg_6446" s="T844">ataːr-ar</ta>
            <ta e="T846" id="Seg_6447" s="T845">olus</ta>
            <ta e="T847" id="Seg_6448" s="T846">kuhagan</ta>
            <ta e="T848" id="Seg_6449" s="T847">anʼɨː</ta>
            <ta e="T849" id="Seg_6450" s="T848">ataːr</ta>
            <ta e="T850" id="Seg_6451" s="T849">kihi-liː</ta>
            <ta e="T851" id="Seg_6452" s="T850">barɨ</ta>
            <ta e="T852" id="Seg_6453" s="T851">uːčak-taːk</ta>
            <ta e="T853" id="Seg_6454" s="T852">ogo-lor</ta>
            <ta e="T854" id="Seg_6455" s="T853">ehe-leri-n</ta>
            <ta e="T855" id="Seg_6456" s="T854">kenn-i-tten</ta>
            <ta e="T856" id="Seg_6457" s="T855">bar-an</ta>
            <ta e="T857" id="Seg_6458" s="T856">uhun</ta>
            <ta e="T858" id="Seg_6459" s="T857">kös</ta>
            <ta e="T859" id="Seg_6460" s="T858">bu͡ol-but-tar</ta>
            <ta e="T860" id="Seg_6461" s="T859">tur-bat-tur-ar</ta>
            <ta e="T861" id="Seg_6462" s="T860">ogo-loru-n</ta>
            <ta e="T862" id="Seg_6463" s="T861">ataːr-an</ta>
            <ta e="T863" id="Seg_6464" s="T862">ol</ta>
            <ta e="T864" id="Seg_6465" s="T863">kös</ta>
            <ta e="T865" id="Seg_6466" s="T864">kenn-i-tten</ta>
            <ta e="T866" id="Seg_6467" s="T865">huburuj-but-tar</ta>
            <ta e="T867" id="Seg_6468" s="T866">Parfʼiraj</ta>
            <ta e="T868" id="Seg_6469" s="T867">ogonnʼor</ta>
            <ta e="T869" id="Seg_6470" s="T868">karak-tar-ɨ-n</ta>
            <ta e="T870" id="Seg_6471" s="T869">bɨlča-ččɨ</ta>
            <ta e="T871" id="Seg_6472" s="T870">kör-ön</ta>
            <ta e="T872" id="Seg_6473" s="T871">tur-an</ta>
            <ta e="T873" id="Seg_6474" s="T872">ele</ta>
            <ta e="T874" id="Seg_6475" s="T873">küːh-ü-nen</ta>
            <ta e="T875" id="Seg_6476" s="T874">alg-ɨː</ta>
            <ta e="T876" id="Seg_6477" s="T875">tur-but</ta>
            <ta e="T877" id="Seg_6478" s="T876">dʼe</ta>
            <ta e="T878" id="Seg_6479" s="T877">eteŋŋe</ta>
            <ta e="T879" id="Seg_6480" s="T878">hɨldʼ-ɨ-ŋ</ta>
            <ta e="T880" id="Seg_6481" s="T879">ehigi</ta>
            <ta e="T881" id="Seg_6482" s="T880">hu͡ol-gutu-gar</ta>
            <ta e="T882" id="Seg_6483" s="T881">kim</ta>
            <ta e="T883" id="Seg_6484" s="T882">da</ta>
            <ta e="T884" id="Seg_6485" s="T883">haːrap-pat-ɨ-n</ta>
            <ta e="T885" id="Seg_6486" s="T884">ulakan</ta>
            <ta e="T886" id="Seg_6487" s="T885">öj-ü</ta>
            <ta e="T887" id="Seg_6488" s="T886">ɨl-ɨ-ŋ</ta>
            <ta e="T888" id="Seg_6489" s="T887">türgen-nik</ta>
            <ta e="T889" id="Seg_6490" s="T888">tönn-ü-ŋ</ta>
            <ta e="T890" id="Seg_6491" s="T889">töröː-büt</ta>
            <ta e="T891" id="Seg_6492" s="T890">hir-giti-ger</ta>
            <ta e="T892" id="Seg_6493" s="T891">kaːl-ɨː</ta>
            <ta e="T893" id="Seg_6494" s="T892">dʼon</ta>
            <ta e="T894" id="Seg_6495" s="T893">öːr</ta>
            <ta e="T895" id="Seg_6496" s="T894">bagajɨ</ta>
            <ta e="T896" id="Seg_6497" s="T895">kör-ö</ta>
            <ta e="T897" id="Seg_6498" s="T896">tur-but-tar</ta>
            <ta e="T898" id="Seg_6499" s="T897">mu͡ora</ta>
            <ta e="T899" id="Seg_6500" s="T898">ürd-ü-nen</ta>
            <ta e="T900" id="Seg_6501" s="T899">uhun</ta>
            <ta e="T901" id="Seg_6502" s="T900">kös</ta>
            <ta e="T902" id="Seg_6503" s="T901">hüt-ü͡ö-r</ta>
            <ta e="T903" id="Seg_6504" s="T902">di͡eri</ta>
            <ta e="T904" id="Seg_6505" s="T903">his</ta>
            <ta e="T905" id="Seg_6506" s="T904">ɨnaraː</ta>
            <ta e="T906" id="Seg_6507" s="T905">ött-ü-ger</ta>
            <ta e="T907" id="Seg_6508" s="T906">ogo</ta>
            <ta e="T908" id="Seg_6509" s="T907">aːjmak-ka</ta>
            <ta e="T909" id="Seg_6510" s="T908">törüt-ten-n-e</ta>
            <ta e="T910" id="Seg_6511" s="T909">haŋa</ta>
            <ta e="T911" id="Seg_6512" s="T910">üje</ta>
            <ta e="T912" id="Seg_6513" s="T911">ü͡ören-er</ta>
            <ta e="T913" id="Seg_6514" s="T912">üje</ta>
            <ta e="T914" id="Seg_6515" s="T913">huruk-ka</ta>
            <ta e="T915" id="Seg_6516" s="T914">ehigi</ta>
            <ta e="T916" id="Seg_6517" s="T915">ihilleː-ti-git</ta>
            <ta e="T917" id="Seg_6518" s="T916">öh-ü</ta>
            <ta e="T918" id="Seg_6519" s="T917">Valačanka</ta>
            <ta e="T919" id="Seg_6520" s="T918">di͡ek</ta>
            <ta e="T920" id="Seg_6521" s="T919">köh-üː</ta>
            <ta e="T921" id="Seg_6522" s="T920">aːk-pɨt-a</ta>
            <ta e="T922" id="Seg_6523" s="T921">Papov</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_6524" s="T0">Voločanka</ta>
            <ta e="T2" id="Seg_6525" s="T1">dek</ta>
            <ta e="T3" id="Seg_6526" s="T2">kös-Iː</ta>
            <ta e="T4" id="Seg_6527" s="T3">ɨrgakta</ta>
            <ta e="T5" id="Seg_6528" s="T4">ɨj-tA</ta>
            <ta e="T6" id="Seg_6529" s="T5">baran-Ar</ta>
            <ta e="T7" id="Seg_6530" s="T6">kün-LAr-tA</ta>
            <ta e="T8" id="Seg_6531" s="T7">tur-BIT-LArA</ta>
            <ta e="T9" id="Seg_6532" s="T8">itiː</ta>
            <ta e="T10" id="Seg_6533" s="T9">bagajɨ</ta>
            <ta e="T11" id="Seg_6534" s="T10">ot-LAr</ta>
            <ta e="T12" id="Seg_6535" s="T11">hebirdek-LAr</ta>
            <ta e="T13" id="Seg_6536" s="T12">kagdʼarɨj-BIT-LAr</ta>
            <ta e="T14" id="Seg_6537" s="T13">mu͡ora</ta>
            <ta e="T15" id="Seg_6538" s="T14">onno-manna</ta>
            <ta e="T16" id="Seg_6539" s="T15">kɨhɨl</ta>
            <ta e="T17" id="Seg_6540" s="T16">kömüs</ta>
            <ta e="T18" id="Seg_6541" s="T17">bu͡ol-An</ta>
            <ta e="T19" id="Seg_6542" s="T18">is-BIT</ta>
            <ta e="T20" id="Seg_6543" s="T19">ol</ta>
            <ta e="T21" id="Seg_6544" s="T20">kün-LAr-GA</ta>
            <ta e="T22" id="Seg_6545" s="T21">bil-Iː-LAːK</ta>
            <ta e="T23" id="Seg_6546" s="T22">ulakan</ta>
            <ta e="T24" id="Seg_6547" s="T23">kü͡öl</ta>
            <ta e="T25" id="Seg_6548" s="T24">kɨtɨl-tI-GAr</ta>
            <ta e="T26" id="Seg_6549" s="T25">ulakan</ta>
            <ta e="T27" id="Seg_6550" s="T26">komuj-n-Iː</ta>
            <ta e="T28" id="Seg_6551" s="T27">tur-BIT-tA</ta>
            <ta e="T29" id="Seg_6552" s="T28">ogo</ta>
            <ta e="T30" id="Seg_6553" s="T29">ajmak-nI</ta>
            <ta e="T31" id="Seg_6554" s="T30">belemneː-Ar-LAr</ta>
            <ta e="T32" id="Seg_6555" s="T31">Voločanka-GA</ta>
            <ta e="T33" id="Seg_6556" s="T32">ilin-AːrI</ta>
            <ta e="T34" id="Seg_6557" s="T33">ü͡örek-GA</ta>
            <ta e="T35" id="Seg_6558" s="T34">mu͡ora</ta>
            <ta e="T36" id="Seg_6559" s="T35">nehili͡ege</ta>
            <ta e="T37" id="Seg_6560" s="T36">urut</ta>
            <ta e="T38" id="Seg_6561" s="T37">bil-BAT</ta>
            <ta e="T39" id="Seg_6562" s="T38">e-TI-LArA</ta>
            <ta e="T40" id="Seg_6563" s="T39">ogo-LAr-tI-n</ta>
            <ta e="T41" id="Seg_6564" s="T40">ü͡örek-GA</ta>
            <ta e="T42" id="Seg_6565" s="T41">ɨːt-Ar-nI</ta>
            <ta e="T43" id="Seg_6566" s="T42">ol</ta>
            <ta e="T44" id="Seg_6567" s="T43">ihin</ta>
            <ta e="T45" id="Seg_6568" s="T44">hanaː-LAr-tA</ta>
            <ta e="T46" id="Seg_6569" s="T45">bar-BAT</ta>
            <ta e="T47" id="Seg_6570" s="T46">usku͡ola-GA</ta>
            <ta e="T48" id="Seg_6571" s="T47">bi͡er-Ar-GA</ta>
            <ta e="T49" id="Seg_6572" s="T48">bütte</ta>
            <ta e="T50" id="Seg_6573" s="T49">ös-nI</ta>
            <ta e="T51" id="Seg_6574" s="T50">bul-Ar-LAr</ta>
            <ta e="T52" id="Seg_6575" s="T51">ɨːt-I-m-AːrI</ta>
            <ta e="T53" id="Seg_6576" s="T52">ogo-LAr-tI-n</ta>
            <ta e="T54" id="Seg_6577" s="T53">aradavoj-LArI-GAr</ta>
            <ta e="T55" id="Seg_6578" s="T54">ol</ta>
            <ta e="T56" id="Seg_6579" s="T55">kün-LAr-GA</ta>
            <ta e="T57" id="Seg_6580" s="T56">olus</ta>
            <ta e="T58" id="Seg_6581" s="T57">erej-LAːK</ta>
            <ta e="T59" id="Seg_6582" s="T58">üle-nI</ta>
            <ta e="T60" id="Seg_6583" s="T59">oŋor-BIT-LArA</ta>
            <ta e="T61" id="Seg_6584" s="T60">barɨ</ta>
            <ta e="T62" id="Seg_6585" s="T61">uraha</ta>
            <ta e="T63" id="Seg_6586" s="T62">dʼi͡e</ta>
            <ta e="T64" id="Seg_6587" s="T63">aːjɨ</ta>
            <ta e="T65" id="Seg_6588" s="T64">hɨrɨt-An</ta>
            <ta e="T66" id="Seg_6589" s="T65">kepset-A</ta>
            <ta e="T67" id="Seg_6590" s="T66">hataː-BIT</ta>
            <ta e="T68" id="Seg_6591" s="T67">ogo-LAr-tI-n</ta>
            <ta e="T69" id="Seg_6592" s="T68">usku͡ola-GA</ta>
            <ta e="T70" id="Seg_6593" s="T69">bi͡er-IAK-LArI-n</ta>
            <ta e="T71" id="Seg_6594" s="T70">ü͡örek-GA</ta>
            <ta e="T72" id="Seg_6595" s="T71">ɨːt-IAK-LArI-n</ta>
            <ta e="T73" id="Seg_6596" s="T72">iti-ttAn</ta>
            <ta e="T74" id="Seg_6597" s="T73">tu͡ok</ta>
            <ta e="T75" id="Seg_6598" s="T74">da</ta>
            <ta e="T76" id="Seg_6599" s="T75">tagɨs-I-BAtAK</ta>
            <ta e="T77" id="Seg_6600" s="T76">ol</ta>
            <ta e="T78" id="Seg_6601" s="T77">ihin</ta>
            <ta e="T79" id="Seg_6602" s="T78">hübe</ta>
            <ta e="T80" id="Seg_6603" s="T79">bu͡ol-BIT</ta>
            <ta e="T81" id="Seg_6604" s="T80">oŋor-Ar-GA</ta>
            <ta e="T82" id="Seg_6605" s="T81">di͡e-An</ta>
            <ta e="T83" id="Seg_6606" s="T82">munnʼak-nI</ta>
            <ta e="T84" id="Seg_6607" s="T83">mu͡ora</ta>
            <ta e="T85" id="Seg_6608" s="T84">ürüt-tI-nAn</ta>
            <ta e="T86" id="Seg_6609" s="T85">barɨ-LArA</ta>
            <ta e="T87" id="Seg_6610" s="T86">komuj-LIN-IAK-LArI-n</ta>
            <ta e="T88" id="Seg_6611" s="T87">hɨraj</ta>
            <ta e="T89" id="Seg_6612" s="T88">hɨraj-GA</ta>
            <ta e="T90" id="Seg_6613" s="T89">kepset-Iː</ta>
            <ta e="T91" id="Seg_6614" s="T90">bu͡ol-IAK-tI-n</ta>
            <ta e="T92" id="Seg_6615" s="T91">biːr</ta>
            <ta e="T93" id="Seg_6616" s="T92">ulakan</ta>
            <ta e="T94" id="Seg_6617" s="T93">uraha</ta>
            <ta e="T95" id="Seg_6618" s="T94">dʼi͡e-GA</ta>
            <ta e="T96" id="Seg_6619" s="T95">arɨːččɨ</ta>
            <ta e="T97" id="Seg_6620" s="T96">bat-An</ta>
            <ta e="T98" id="Seg_6621" s="T97">komuj-LIN-I-BIT-LAr</ta>
            <ta e="T99" id="Seg_6622" s="T98">tur-Ar</ta>
            <ta e="T100" id="Seg_6623" s="T99">tur-BAT</ta>
            <ta e="T101" id="Seg_6624" s="T100">er</ta>
            <ta e="T102" id="Seg_6625" s="T101">kihi-LAr</ta>
            <ta e="T103" id="Seg_6626" s="T102">tu͡ok</ta>
            <ta e="T104" id="Seg_6627" s="T103">da</ta>
            <ta e="T105" id="Seg_6628" s="T104">di͡e-IAK-LArI-n</ta>
            <ta e="T106" id="Seg_6629" s="T105">bert-tI-ttAn</ta>
            <ta e="T107" id="Seg_6630" s="T106">hir</ta>
            <ta e="T108" id="Seg_6631" s="T107">dek</ta>
            <ta e="T109" id="Seg_6632" s="T108">agaj</ta>
            <ta e="T110" id="Seg_6633" s="T109">kör-Ar-LAr</ta>
            <ta e="T111" id="Seg_6634" s="T110">dʼaktar</ta>
            <ta e="T112" id="Seg_6635" s="T111">ajmak</ta>
            <ta e="T113" id="Seg_6636" s="T112">pɨlaːt-LArI-nAn</ta>
            <ta e="T114" id="Seg_6637" s="T113">karak-LArI-n</ta>
            <ta e="T115" id="Seg_6638" s="T114">uː-LArI-n</ta>
            <ta e="T116" id="Seg_6639" s="T115">hot-I-n-Ar-LAr</ta>
            <ta e="T117" id="Seg_6640" s="T116">kebe-LAr</ta>
            <ta e="T118" id="Seg_6641" s="T117">tu͡ok</ta>
            <ta e="T119" id="Seg_6642" s="T118">bu͡ol-An-GIt</ta>
            <ta e="T120" id="Seg_6643" s="T119">bas-LAr-GItI-n</ta>
            <ta e="T121" id="Seg_6644" s="T120">tobuk-LAr-GIt</ta>
            <ta e="T122" id="Seg_6645" s="T121">is-tI-GAr</ta>
            <ta e="T123" id="Seg_6646" s="T122">kiːr-TAr-TI-GIt</ta>
            <ta e="T124" id="Seg_6647" s="T123">togo</ta>
            <ta e="T125" id="Seg_6648" s="T124">nʼimij-s-A</ta>
            <ta e="T126" id="Seg_6649" s="T125">hɨt-A-GIt</ta>
            <ta e="T127" id="Seg_6650" s="T126">haŋar-BIT</ta>
            <ta e="T128" id="Seg_6651" s="T127">di͡e-An</ta>
            <ta e="T129" id="Seg_6652" s="T128">Debgeːn</ta>
            <ta e="T130" id="Seg_6653" s="T129">haŋa-LAːK</ta>
            <ta e="T131" id="Seg_6654" s="T130">emeːksin</ta>
            <ta e="T132" id="Seg_6655" s="T131">ü͡ögüleː-An</ta>
            <ta e="T133" id="Seg_6656" s="T132">bi͡er-BIT</ta>
            <ta e="T134" id="Seg_6657" s="T133">gini</ta>
            <ta e="T135" id="Seg_6658" s="T134">oburgu</ta>
            <ta e="T136" id="Seg_6659" s="T135">balamat-tA</ta>
            <ta e="T137" id="Seg_6660" s="T136">bert</ta>
            <ta e="T138" id="Seg_6661" s="T137">kim-ttAn</ta>
            <ta e="T139" id="Seg_6662" s="T138">da</ta>
            <ta e="T140" id="Seg_6663" s="T139">tolun-I-BAT</ta>
            <ta e="T141" id="Seg_6664" s="T140">olor-Ar</ta>
            <ta e="T142" id="Seg_6665" s="T141">hir-ttAn</ta>
            <ta e="T143" id="Seg_6666" s="T142">orgujakaːn</ta>
            <ta e="T144" id="Seg_6667" s="T143">tur-BIT</ta>
            <ta e="T145" id="Seg_6668" s="T144">Parfiraj</ta>
            <ta e="T146" id="Seg_6669" s="T145">ogonnʼor</ta>
            <ta e="T147" id="Seg_6670" s="T146">meldʼen</ta>
            <ta e="T148" id="Seg_6671" s="T147">menʼiː-tI-n</ta>
            <ta e="T149" id="Seg_6672" s="T148">imerij-A</ta>
            <ta e="T150" id="Seg_6673" s="T149">tüs-AːT</ta>
            <ta e="T151" id="Seg_6674" s="T150">i͡edej-BAkkA</ta>
            <ta e="T152" id="Seg_6675" s="T151">oduːlaː-A-n-BIT</ta>
            <ta e="T153" id="Seg_6676" s="T152">kajdak</ta>
            <ta e="T154" id="Seg_6677" s="T153">di͡e-IAK-m=Ij</ta>
            <ta e="T155" id="Seg_6678" s="T154">iti</ta>
            <ta e="T156" id="Seg_6679" s="T155">itigirdik</ta>
            <ta e="T157" id="Seg_6680" s="T156">bu͡ol-Ar</ta>
            <ta e="T158" id="Seg_6681" s="T157">ogo-LAr</ta>
            <ta e="T159" id="Seg_6682" s="T158">bu</ta>
            <ta e="T160" id="Seg_6683" s="T159">haŋa</ta>
            <ta e="T161" id="Seg_6684" s="T160">üje-GA</ta>
            <ta e="T162" id="Seg_6685" s="T161">ü͡ören-Ar-LArA</ta>
            <ta e="T163" id="Seg_6686" s="T162">hin</ta>
            <ta e="T164" id="Seg_6687" s="T163">daːganɨ</ta>
            <ta e="T165" id="Seg_6688" s="T164">olus</ta>
            <ta e="T166" id="Seg_6689" s="T165">höpsöːk</ta>
            <ta e="T167" id="Seg_6690" s="T166">giniler</ta>
            <ta e="T168" id="Seg_6691" s="T167">huruk-ČIt</ta>
            <ta e="T169" id="Seg_6692" s="T168">bu͡ol-IAK</ta>
            <ta e="T170" id="Seg_6693" s="T169">tüs-LAːK-LAr</ta>
            <ta e="T171" id="Seg_6694" s="T170">bu</ta>
            <ta e="T172" id="Seg_6695" s="T171">min</ta>
            <ta e="T173" id="Seg_6696" s="T172">Ujbaːn</ta>
            <ta e="T174" id="Seg_6697" s="T173">u͡ol-BI-n</ta>
            <ta e="T175" id="Seg_6698" s="T174">küččügüj-tI-ttAn</ta>
            <ta e="T176" id="Seg_6699" s="T175">ü͡öret-BIT-I-m</ta>
            <ta e="T177" id="Seg_6700" s="T176">balɨk</ta>
            <ta e="T178" id="Seg_6701" s="T177">bult</ta>
            <ta e="T179" id="Seg_6702" s="T178">kapkaːn-LAː-IAK-tI-n</ta>
            <ta e="T180" id="Seg_6703" s="T179">min</ta>
            <ta e="T181" id="Seg_6704" s="T180">anɨ</ta>
            <ta e="T182" id="Seg_6705" s="T181">kɨrɨj-TI-m</ta>
            <ta e="T183" id="Seg_6706" s="T182">üle-nI</ta>
            <ta e="T184" id="Seg_6707" s="T183">kot-I-n-BAT</ta>
            <ta e="T185" id="Seg_6708" s="T184">bu͡ol-An</ta>
            <ta e="T186" id="Seg_6709" s="T185">er-A-BIn</ta>
            <ta e="T187" id="Seg_6710" s="T186">kim</ta>
            <ta e="T188" id="Seg_6711" s="T187">min</ta>
            <ta e="T189" id="Seg_6712" s="T188">kergen-BI-n</ta>
            <ta e="T190" id="Seg_6713" s="T189">ahaː-A-t-IAK-tA=Ij</ta>
            <ta e="T191" id="Seg_6714" s="T190">mu͡ora-BIt</ta>
            <ta e="T192" id="Seg_6715" s="T191">tɨmnɨː</ta>
            <ta e="T193" id="Seg_6716" s="T192">olus</ta>
            <ta e="T194" id="Seg_6717" s="T193">purgaː-LAːK</ta>
            <ta e="T195" id="Seg_6718" s="T194">küːsteːk</ta>
            <ta e="T196" id="Seg_6719" s="T195">ere</ta>
            <ta e="T197" id="Seg_6720" s="T196">ogo</ta>
            <ta e="T198" id="Seg_6721" s="T197">agaj</ta>
            <ta e="T199" id="Seg_6722" s="T198">kihi</ta>
            <ta e="T200" id="Seg_6723" s="T199">bult-nI</ta>
            <ta e="T201" id="Seg_6724" s="T200">bul-AːččI</ta>
            <ta e="T202" id="Seg_6725" s="T201">Ujbaːn-nI</ta>
            <ta e="T203" id="Seg_6726" s="T202">usku͡ola-GA</ta>
            <ta e="T205" id="Seg_6727" s="T204">paːs-nI</ta>
            <ta e="T206" id="Seg_6728" s="T205">kapkaːn-nI</ta>
            <ta e="T207" id="Seg_6729" s="T206">iːt-A</ta>
            <ta e="T208" id="Seg_6730" s="T207">ü͡öret-IAK-LArA</ta>
            <ta e="T209" id="Seg_6731" s="T208">hu͡ok-tA</ta>
            <ta e="T210" id="Seg_6732" s="T209">gini</ta>
            <ta e="T211" id="Seg_6733" s="T210">umun-IAK-tA</ta>
            <ta e="T212" id="Seg_6734" s="T211">mu͡ora</ta>
            <ta e="T213" id="Seg_6735" s="T212">üle-tI-n</ta>
            <ta e="T214" id="Seg_6736" s="T213">dʼaktar</ta>
            <ta e="T215" id="Seg_6737" s="T214">kördük</ta>
            <ta e="T216" id="Seg_6738" s="T215">dʼi͡e-GA</ta>
            <ta e="T217" id="Seg_6739" s="T216">agaj</ta>
            <ta e="T218" id="Seg_6740" s="T217">olor-IAK-tA</ta>
            <ta e="T219" id="Seg_6741" s="T218">tu͡ok-nI</ta>
            <ta e="T220" id="Seg_6742" s="T219">da</ta>
            <ta e="T221" id="Seg_6743" s="T220">gɨn-BAkkA</ta>
            <ta e="T222" id="Seg_6744" s="T221">en</ta>
            <ta e="T223" id="Seg_6745" s="T222">ogonnʼor</ta>
            <ta e="T224" id="Seg_6746" s="T223">dʼaktar-LAr-nI</ta>
            <ta e="T225" id="Seg_6747" s="T224">tɨːt-I-BA</ta>
            <ta e="T226" id="Seg_6748" s="T225">emi͡e</ta>
            <ta e="T227" id="Seg_6749" s="T226">Debgeːn</ta>
            <ta e="T228" id="Seg_6750" s="T227">ü͡ögüː-tA</ta>
            <ta e="T229" id="Seg_6751" s="T228">ihilin-I-BIT</ta>
            <ta e="T230" id="Seg_6752" s="T229">kim</ta>
            <ta e="T231" id="Seg_6753" s="T230">en-GA</ta>
            <ta e="T232" id="Seg_6754" s="T231">as-nI</ta>
            <ta e="T233" id="Seg_6755" s="T232">belemneː-Ar</ta>
            <ta e="T234" id="Seg_6756" s="T233">dʼi͡e-GI-n</ta>
            <ta e="T235" id="Seg_6757" s="T234">itiː-LIk</ta>
            <ta e="T236" id="Seg_6758" s="T235">tut-Ar</ta>
            <ta e="T237" id="Seg_6759" s="T236">atak-LAr-GI-n</ta>
            <ta e="T238" id="Seg_6760" s="T237">taŋas-GI-n</ta>
            <ta e="T239" id="Seg_6761" s="T238">abɨraktaː-Ar</ta>
            <ta e="T240" id="Seg_6762" s="T239">iti</ta>
            <ta e="T241" id="Seg_6763" s="T240">kirdik</ta>
            <ta e="T242" id="Seg_6764" s="T241">itigirdik</ta>
            <ta e="T243" id="Seg_6765" s="T242">bu͡ol-Ar</ta>
            <ta e="T244" id="Seg_6766" s="T243">olus</ta>
            <ta e="T245" id="Seg_6767" s="T244">ür.[t]-BAkkA</ta>
            <ta e="T246" id="Seg_6768" s="T245">Parfiraj</ta>
            <ta e="T247" id="Seg_6769" s="T246">haŋar-A</ta>
            <ta e="T248" id="Seg_6770" s="T247">tur-BIT</ta>
            <ta e="T249" id="Seg_6771" s="T248">min</ta>
            <ta e="T250" id="Seg_6772" s="T249">Kɨča</ta>
            <ta e="T251" id="Seg_6773" s="T250">kɨːs-I-m</ta>
            <ta e="T252" id="Seg_6774" s="T251">astaː-A</ta>
            <ta e="T253" id="Seg_6775" s="T252">hataː-Ar</ta>
            <ta e="T254" id="Seg_6776" s="T253">atak-nI</ta>
            <ta e="T255" id="Seg_6777" s="T254">taŋas-nI</ta>
            <ta e="T256" id="Seg_6778" s="T255">oŋor-A</ta>
            <ta e="T257" id="Seg_6779" s="T256">hataː-Ar</ta>
            <ta e="T258" id="Seg_6780" s="T257">usku͡ola-GA</ta>
            <ta e="T259" id="Seg_6781" s="T258">hin</ta>
            <ta e="T260" id="Seg_6782" s="T259">daːganɨ</ta>
            <ta e="T261" id="Seg_6783" s="T260">ol-tI-tI-n</ta>
            <ta e="T262" id="Seg_6784" s="T261">umun-IAK-tA</ta>
            <ta e="T263" id="Seg_6785" s="T262">er</ta>
            <ta e="T264" id="Seg_6786" s="T263">kihi</ta>
            <ta e="T265" id="Seg_6787" s="T264">kördük</ta>
            <ta e="T266" id="Seg_6788" s="T265">končoj-ŋnAː-A</ta>
            <ta e="T267" id="Seg_6789" s="T266">hɨrɨt-IAK-tA</ta>
            <ta e="T268" id="Seg_6790" s="T267">dʼi͡e-nI</ta>
            <ta e="T269" id="Seg_6791" s="T268">tögürüj-A</ta>
            <ta e="T270" id="Seg_6792" s="T269">kajdak</ta>
            <ta e="T271" id="Seg_6793" s="T270">min</ta>
            <ta e="T272" id="Seg_6794" s="T271">olor-IAK-m=Ij</ta>
            <ta e="T273" id="Seg_6795" s="T272">ogo-LAr-I-m</ta>
            <ta e="T274" id="Seg_6796" s="T273">bar-TAK-TArInA</ta>
            <ta e="T275" id="Seg_6797" s="T274">aradavoj-tA</ta>
            <ta e="T276" id="Seg_6798" s="T275">Anʼiːsʼim</ta>
            <ta e="T277" id="Seg_6799" s="T276">Nazaravʼičʼ</ta>
            <ta e="T278" id="Seg_6800" s="T277">Papov</ta>
            <ta e="T279" id="Seg_6801" s="T278">külümneː-A</ta>
            <ta e="T280" id="Seg_6802" s="T279">tüs-AːT</ta>
            <ta e="T281" id="Seg_6803" s="T280">ogonnʼor-nI</ta>
            <ta e="T282" id="Seg_6804" s="T281">toktoː-A-t-BIT</ta>
            <ta e="T283" id="Seg_6805" s="T282">dʼe</ta>
            <ta e="T284" id="Seg_6806" s="T283">ogonnʼor</ta>
            <ta e="T285" id="Seg_6807" s="T284">en</ta>
            <ta e="T286" id="Seg_6808" s="T285">hɨlaj-TI-ŋ</ta>
            <ta e="T287" id="Seg_6809" s="T286">bɨhɨːlaːk</ta>
            <ta e="T288" id="Seg_6810" s="T287">olor-A</ta>
            <ta e="T289" id="Seg_6811" s="T288">tüs</ta>
            <ta e="T290" id="Seg_6812" s="T289">atɨn</ta>
            <ta e="T291" id="Seg_6813" s="T290">dʼon</ta>
            <ta e="T292" id="Seg_6814" s="T291">haŋar-TIn</ta>
            <ta e="T293" id="Seg_6815" s="T292">ogonnʼor</ta>
            <ta e="T294" id="Seg_6816" s="T293">tu͡ok</ta>
            <ta e="T295" id="Seg_6817" s="T294">da</ta>
            <ta e="T296" id="Seg_6818" s="T295">di͡e-BAkkA</ta>
            <ta e="T297" id="Seg_6819" s="T296">orgujakaːn</ta>
            <ta e="T298" id="Seg_6820" s="T297">olok-tI-GAr</ta>
            <ta e="T299" id="Seg_6821" s="T298">laŋ</ta>
            <ta e="T300" id="Seg_6822" s="T299">gɨn-A</ta>
            <ta e="T301" id="Seg_6823" s="T300">olor-BIT</ta>
            <ta e="T302" id="Seg_6824" s="T301">tobuk-LAr-tI-ttAn</ta>
            <ta e="T303" id="Seg_6825" s="T302">tutun-A</ta>
            <ta e="T304" id="Seg_6826" s="T303">tüs-AːT</ta>
            <ta e="T305" id="Seg_6827" s="T304">kim</ta>
            <ta e="T306" id="Seg_6828" s="T305">össü͡ö</ta>
            <ta e="T307" id="Seg_6829" s="T306">tu͡ok</ta>
            <ta e="T308" id="Seg_6830" s="T307">haŋa-LAːK=Ij</ta>
            <ta e="T309" id="Seg_6831" s="T308">di͡e-An</ta>
            <ta e="T310" id="Seg_6832" s="T309">ɨjɨt-BIT</ta>
            <ta e="T311" id="Seg_6833" s="T310">aradavoj-LArA</ta>
            <ta e="T312" id="Seg_6834" s="T311">kim-LArA</ta>
            <ta e="T313" id="Seg_6835" s="T312">ere</ta>
            <ta e="T314" id="Seg_6836" s="T313">haŋar-Ar-tI-n</ta>
            <ta e="T315" id="Seg_6837" s="T314">köhüt-A</ta>
            <ta e="T316" id="Seg_6838" s="T315">hataː-An</ta>
            <ta e="T317" id="Seg_6839" s="T316">aːk-BIT</ta>
            <ta e="T318" id="Seg_6840" s="T317">kannɨk</ta>
            <ta e="T319" id="Seg_6841" s="T318">ogo-LArA</ta>
            <ta e="T320" id="Seg_6842" s="T319">usku͡ola-GA</ta>
            <ta e="T321" id="Seg_6843" s="T320">bar-Ar-LArI-n</ta>
            <ta e="T322" id="Seg_6844" s="T321">kim</ta>
            <ta e="T323" id="Seg_6845" s="T322">kaːl-Ar-tI-n</ta>
            <ta e="T324" id="Seg_6846" s="T323">usku͡ola-GA</ta>
            <ta e="T325" id="Seg_6847" s="T324">bar-BAT-tI-n</ta>
            <ta e="T326" id="Seg_6848" s="T325">haːs-LArA</ta>
            <ta e="T327" id="Seg_6849" s="T326">ulakan-LAr-nI</ta>
            <ta e="T328" id="Seg_6850" s="T327">ol</ta>
            <ta e="T329" id="Seg_6851" s="T328">ogo-LAr-GA</ta>
            <ta e="T330" id="Seg_6852" s="T329">tübes-BIT</ta>
            <ta e="T331" id="Seg_6853" s="T330">Parfiraj</ta>
            <ta e="T332" id="Seg_6854" s="T331">ogonnʼor</ta>
            <ta e="T333" id="Seg_6855" s="T332">u͡ol-tA</ta>
            <ta e="T334" id="Seg_6856" s="T333">Ujbaːn</ta>
            <ta e="T335" id="Seg_6857" s="T334">Kɨča-nI</ta>
            <ta e="T336" id="Seg_6858" s="T335">emi͡e</ta>
            <ta e="T337" id="Seg_6859" s="T336">keːs-IAK-GA</ta>
            <ta e="T338" id="Seg_6860" s="T337">tüs-LAːK</ta>
            <ta e="T339" id="Seg_6861" s="T338">gini</ta>
            <ta e="T340" id="Seg_6862" s="T339">haːs-tA</ta>
            <ta e="T341" id="Seg_6863" s="T340">hin</ta>
            <ta e="T342" id="Seg_6864" s="T341">daːganɨ</ta>
            <ta e="T343" id="Seg_6865" s="T342">elbek</ta>
            <ta e="T344" id="Seg_6866" s="T343">kördöː-A-n-BIT</ta>
            <ta e="T345" id="Seg_6867" s="T344">Parfiraj</ta>
            <ta e="T346" id="Seg_6868" s="T345">ogonnʼor</ta>
            <ta e="T347" id="Seg_6869" s="T346">hu͡ok</ta>
            <ta e="T348" id="Seg_6870" s="T347">Kɨča</ta>
            <ta e="T349" id="Seg_6871" s="T348">bar-IAK.[tA]</ta>
            <ta e="T350" id="Seg_6872" s="T349">hu͡ok-tA</ta>
            <ta e="T351" id="Seg_6873" s="T350">usku͡ola-GA</ta>
            <ta e="T352" id="Seg_6874" s="T351">ogonnʼor</ta>
            <ta e="T353" id="Seg_6875" s="T352">kečes-I-m</ta>
            <ta e="T354" id="Seg_6876" s="T353">ü͡örek-LAːK</ta>
            <ta e="T355" id="Seg_6877" s="T354">bu͡ol-TAK-InA</ta>
            <ta e="T356" id="Seg_6878" s="T355">er</ta>
            <ta e="T357" id="Seg_6879" s="T356">kihi</ta>
            <ta e="T358" id="Seg_6880" s="T357">hɨ͡aldʼa-tI-n</ta>
            <ta e="T359" id="Seg_6881" s="T358">ket-IAK.[tA]</ta>
            <ta e="T360" id="Seg_6882" s="T359">hu͡ok-tA</ta>
            <ta e="T361" id="Seg_6883" s="T360">dʼi͡e-nI</ta>
            <ta e="T362" id="Seg_6884" s="T361">tögürüččü</ta>
            <ta e="T363" id="Seg_6885" s="T362">končoj-ŋnAː-A</ta>
            <ta e="T364" id="Seg_6886" s="T363">hɨrɨt-IAK.[tA]</ta>
            <ta e="T365" id="Seg_6887" s="T364">hu͡ok-tA</ta>
            <ta e="T366" id="Seg_6888" s="T365">munnʼak-GA</ta>
            <ta e="T367" id="Seg_6889" s="T366">olor-Ar</ta>
            <ta e="T368" id="Seg_6890" s="T367">dʼon</ta>
            <ta e="T369" id="Seg_6891" s="T368">ele</ta>
            <ta e="T370" id="Seg_6892" s="T369">is-LArI-nAn</ta>
            <ta e="T371" id="Seg_6893" s="T370">kül-s-I-BIT-LAr</ta>
            <ta e="T372" id="Seg_6894" s="T371">bar-Ar</ta>
            <ta e="T373" id="Seg_6895" s="T372">ogo-LAr-nI</ta>
            <ta e="T376" id="Seg_6896" s="T375">belemneː-ŋ</ta>
            <ta e="T377" id="Seg_6897" s="T376">ötü͡ö-LIk</ta>
            <ta e="T378" id="Seg_6898" s="T377">taŋɨn-TAr-I-ŋ</ta>
            <ta e="T379" id="Seg_6899" s="T378">taŋara-GA</ta>
            <ta e="T380" id="Seg_6900" s="T379">taŋɨn-TAr-Ar</ta>
            <ta e="T381" id="Seg_6901" s="T380">kördük</ta>
            <ta e="T382" id="Seg_6902" s="T381">ogo-LAr-BIt</ta>
            <ta e="T383" id="Seg_6903" s="T382">beje-m</ta>
            <ta e="T384" id="Seg_6904" s="T383">ilin-IAK-m</ta>
            <ta e="T385" id="Seg_6905" s="T384">Voločanka-ttAn</ta>
            <ta e="T386" id="Seg_6906" s="T385">munnʼak-nI</ta>
            <ta e="T387" id="Seg_6907" s="T386">büt-A-r-A</ta>
            <ta e="T388" id="Seg_6908" s="T387">gɨn-A</ta>
            <ta e="T389" id="Seg_6909" s="T388">di͡e-BIT</ta>
            <ta e="T390" id="Seg_6910" s="T389">hübehit-LArA</ta>
            <ta e="T391" id="Seg_6911" s="T390">munnʼak</ta>
            <ta e="T392" id="Seg_6912" s="T391">aːs-AːT-In</ta>
            <ta e="T393" id="Seg_6913" s="T392">komuj-n-Iː</ta>
            <ta e="T394" id="Seg_6914" s="T393">ajdaːn-tA</ta>
            <ta e="T395" id="Seg_6915" s="T394">elbeː-BIT</ta>
            <ta e="T396" id="Seg_6916" s="T395">dʼaktar-LAr</ta>
            <ta e="T397" id="Seg_6917" s="T396">mataŋa-LArI-n</ta>
            <ta e="T398" id="Seg_6918" s="T397">is-tI-ttAn</ta>
            <ta e="T399" id="Seg_6919" s="T398">tahaːr-TAː-Ar-LAr</ta>
            <ta e="T400" id="Seg_6920" s="T399">ogo-LArI-n</ta>
            <ta e="T401" id="Seg_6921" s="T400">ki͡eŋ</ta>
            <ta e="T402" id="Seg_6922" s="T401">ki͡eŋ</ta>
            <ta e="T403" id="Seg_6923" s="T402">taŋas-LArI-n</ta>
            <ta e="T404" id="Seg_6924" s="T403">taŋara-GA</ta>
            <ta e="T405" id="Seg_6925" s="T404">ket-Ar</ta>
            <ta e="T406" id="Seg_6926" s="T405">ɨrbaːkɨ-LArI-n</ta>
            <ta e="T407" id="Seg_6927" s="T406">horok-LAr</ta>
            <ta e="T408" id="Seg_6928" s="T407">köhün-I-BAT</ta>
            <ta e="T409" id="Seg_6929" s="T408">hir-GA</ta>
            <ta e="T410" id="Seg_6930" s="T409">tik-AttAː-Ar-LAr</ta>
            <ta e="T411" id="Seg_6931" s="T410">e-BIT</ta>
            <ta e="T412" id="Seg_6932" s="T411">küččügüj</ta>
            <ta e="T413" id="Seg_6933" s="T412">taŋara-LArI-n</ta>
            <ta e="T414" id="Seg_6934" s="T413">kiri͡es-LAr-nI</ta>
            <ta e="T415" id="Seg_6935" s="T414">ogo-LAr-tI-n</ta>
            <ta e="T416" id="Seg_6936" s="T415">čie͡steː-LAr</ta>
            <ta e="T417" id="Seg_6937" s="T416">barɨ</ta>
            <ta e="T418" id="Seg_6938" s="T417">ele</ta>
            <ta e="T419" id="Seg_6939" s="T418">minnʼiges</ta>
            <ta e="T420" id="Seg_6940" s="T419">as-LAr-I-nAn</ta>
            <ta e="T421" id="Seg_6941" s="T420">amaha-nAn</ta>
            <ta e="T422" id="Seg_6942" s="T421">kak-I-nAn</ta>
            <ta e="T423" id="Seg_6943" s="T422">taba</ta>
            <ta e="T424" id="Seg_6944" s="T423">tɨl-I-nAn</ta>
            <ta e="T425" id="Seg_6945" s="T424">ki͡ehe</ta>
            <ta e="T426" id="Seg_6946" s="T425">dek</ta>
            <ta e="T429" id="Seg_6947" s="T428">aradavoj-tI-GAr</ta>
            <ta e="T430" id="Seg_6948" s="T429">ɨ͡allan-A</ta>
            <ta e="T431" id="Seg_6949" s="T430">kiːr-BIT</ta>
            <ta e="T432" id="Seg_6950" s="T431">Hemen</ta>
            <ta e="T433" id="Seg_6951" s="T432">ogonnʼor</ta>
            <ta e="T434" id="Seg_6952" s="T433">togo</ta>
            <ta e="T435" id="Seg_6953" s="T434">kel-BIT-tI-n</ta>
            <ta e="T436" id="Seg_6954" s="T435">bɨha</ta>
            <ta e="T437" id="Seg_6955" s="T436">haŋar-BAT</ta>
            <ta e="T438" id="Seg_6956" s="T437">čaːj</ta>
            <ta e="T439" id="Seg_6957" s="T438">is-A</ta>
            <ta e="T440" id="Seg_6958" s="T439">agaj</ta>
            <ta e="T441" id="Seg_6959" s="T440">olor-An</ta>
            <ta e="T442" id="Seg_6960" s="T441">di͡e-BIT</ta>
            <ta e="T443" id="Seg_6961" s="T442">bɨlɨr</ta>
            <ta e="T444" id="Seg_6962" s="T443">ehe-LAr-BIt</ta>
            <ta e="T445" id="Seg_6963" s="T444">aga-LAr-BIt</ta>
            <ta e="T446" id="Seg_6964" s="T445">taŋara-ČIt</ta>
            <ta e="T447" id="Seg_6965" s="T446">e-TI-LArA</ta>
            <ta e="T448" id="Seg_6966" s="T447">ulakan</ta>
            <ta e="T449" id="Seg_6967" s="T448">hajtan-GA</ta>
            <ta e="T450" id="Seg_6968" s="T449">bi͡ek</ta>
            <ta e="T451" id="Seg_6969" s="T450">belek-LAː-Ar</ta>
            <ta e="T452" id="Seg_6970" s="T451">ide-LAːK</ta>
            <ta e="T453" id="Seg_6971" s="T452">e-TI-LArA</ta>
            <ta e="T454" id="Seg_6972" s="T453">barɨ</ta>
            <ta e="T455" id="Seg_6973" s="T454">ajmak</ta>
            <ta e="T456" id="Seg_6974" s="T455">ötü͡ö-LIk</ta>
            <ta e="T457" id="Seg_6975" s="T456">olor-IAK-LArI-n</ta>
            <ta e="T458" id="Seg_6976" s="T457">bagar-Ar-LAr</ta>
            <ta e="T459" id="Seg_6977" s="T458">tu͡ok-I-nAn</ta>
            <ta e="T460" id="Seg_6978" s="T459">da</ta>
            <ta e="T461" id="Seg_6979" s="T460">muskuraː-BAkkA</ta>
            <ta e="T462" id="Seg_6980" s="T461">ɨ͡arɨj-I-BAkkA</ta>
            <ta e="T463" id="Seg_6981" s="T462">ulakan</ta>
            <ta e="T464" id="Seg_6982" s="T463">ojun-LAr</ta>
            <ta e="T465" id="Seg_6983" s="T464">kihi</ta>
            <ta e="T466" id="Seg_6984" s="T465">hɨrɨt-Ar</ta>
            <ta e="T467" id="Seg_6985" s="T466">hu͡ol-tI-n</ta>
            <ta e="T468" id="Seg_6986" s="T467">bil-Ar-LArA</ta>
            <ta e="T469" id="Seg_6987" s="T468">bert</ta>
            <ta e="T470" id="Seg_6988" s="T469">e-TI-tA</ta>
            <ta e="T471" id="Seg_6989" s="T470">bu</ta>
            <ta e="T472" id="Seg_6990" s="T471">olok-BItI-ttAn</ta>
            <ta e="T473" id="Seg_6991" s="T472">ɨraːk-tA</ta>
            <ta e="T474" id="Seg_6992" s="T473">hu͡ok</ta>
            <ta e="T475" id="Seg_6993" s="T474">baːr</ta>
            <ta e="T476" id="Seg_6994" s="T475">ulakan</ta>
            <ta e="T477" id="Seg_6995" s="T476">hajtan-taːs</ta>
            <ta e="T478" id="Seg_6996" s="T477">barɨ</ta>
            <ta e="T479" id="Seg_6997" s="T478">kihi</ta>
            <ta e="T480" id="Seg_6998" s="T479">onno</ta>
            <ta e="T481" id="Seg_6999" s="T480">belek-nI</ta>
            <ta e="T482" id="Seg_7000" s="T481">bi͡er-AːččI</ta>
            <ta e="T483" id="Seg_7001" s="T482">hotoru</ta>
            <ta e="T484" id="Seg_7002" s="T483">ogo-LAr-BIt</ta>
            <ta e="T485" id="Seg_7003" s="T484">atɨn</ta>
            <ta e="T486" id="Seg_7004" s="T485">hir-GA</ta>
            <ta e="T487" id="Seg_7005" s="T486">bar-Ar-LAr</ta>
            <ta e="T488" id="Seg_7006" s="T487">atɨn</ta>
            <ta e="T489" id="Seg_7007" s="T488">kihi-LAr-GA</ta>
            <ta e="T490" id="Seg_7008" s="T489">aradavoj-tA</ta>
            <ta e="T491" id="Seg_7009" s="T490">ɨ͡aldʼɨt-tI-n</ta>
            <ta e="T492" id="Seg_7010" s="T491">ös-tI-n</ta>
            <ta e="T493" id="Seg_7011" s="T492">ihilleː-A-ihilleː-A</ta>
            <ta e="T494" id="Seg_7012" s="T493">is-tI-GAr</ta>
            <ta e="T495" id="Seg_7013" s="T494">di͡e-A</ta>
            <ta e="T496" id="Seg_7014" s="T495">hanaː-Ar</ta>
            <ta e="T497" id="Seg_7015" s="T496">tu͡ok</ta>
            <ta e="T498" id="Seg_7016" s="T497">hanaː-tI-nAn</ta>
            <ta e="T499" id="Seg_7017" s="T498">itinnik</ta>
            <ta e="T500" id="Seg_7018" s="T499">ös-nI</ta>
            <ta e="T501" id="Seg_7019" s="T500">tahaːr-Ar</ta>
            <ta e="T502" id="Seg_7020" s="T501">ɨ͡aldʼɨt-tA</ta>
            <ta e="T503" id="Seg_7021" s="T502">i͡edej-BAkkA</ta>
            <ta e="T504" id="Seg_7022" s="T503">haŋar-TAː-An</ta>
            <ta e="T505" id="Seg_7023" s="T504">is-BIT</ta>
            <ta e="T506" id="Seg_7024" s="T505">dʼaktar-LAr</ta>
            <ta e="T507" id="Seg_7025" s="T506">muŋ-LAːK-LAr</ta>
            <ta e="T508" id="Seg_7026" s="T507">olus</ta>
            <ta e="T509" id="Seg_7027" s="T508">hoŋguraː-TI-LAr</ta>
            <ta e="T510" id="Seg_7028" s="T509">ɨtaː-A-s-Ar-LAr</ta>
            <ta e="T511" id="Seg_7029" s="T510">kɨra</ta>
            <ta e="T512" id="Seg_7030" s="T511">ogo-nI</ta>
            <ta e="T513" id="Seg_7031" s="T512">dʼi͡e-ttAn</ta>
            <ta e="T514" id="Seg_7032" s="T513">bɨldʼaː-Ar</ta>
            <ta e="T515" id="Seg_7033" s="T514">ide</ta>
            <ta e="T516" id="Seg_7034" s="T515">olus</ta>
            <ta e="T517" id="Seg_7035" s="T516">anʼɨː-LAːK</ta>
            <ta e="T518" id="Seg_7036" s="T517">dʼon</ta>
            <ta e="T519" id="Seg_7037" s="T518">olus</ta>
            <ta e="T520" id="Seg_7038" s="T519">hanaːrgaː-Ar</ta>
            <ta e="T521" id="Seg_7039" s="T520">badaga</ta>
            <ta e="T522" id="Seg_7040" s="T521">hajtan-GA</ta>
            <ta e="T523" id="Seg_7041" s="T522">belek</ta>
            <ta e="T524" id="Seg_7042" s="T523">bi͡er-IAK-GA</ta>
            <ta e="T525" id="Seg_7043" s="T524">dʼe</ta>
            <ta e="T526" id="Seg_7044" s="T525">ogonnʼor</ta>
            <ta e="T527" id="Seg_7045" s="T526">min-n</ta>
            <ta e="T528" id="Seg_7046" s="T527">hiːlleː-m</ta>
            <ta e="T529" id="Seg_7047" s="T528">iti-nI</ta>
            <ta e="T530" id="Seg_7048" s="T529">gɨn-IAK-BIt</ta>
            <ta e="T531" id="Seg_7049" s="T530">hu͡ok-tA</ta>
            <ta e="T532" id="Seg_7050" s="T531">usku͡ola</ta>
            <ta e="T533" id="Seg_7051" s="T532">hajtan-nI</ta>
            <ta e="T534" id="Seg_7052" s="T533">kördöː-BAT</ta>
            <ta e="T535" id="Seg_7053" s="T534">itigirdik</ta>
            <ta e="T536" id="Seg_7054" s="T535">barɨ</ta>
            <ta e="T537" id="Seg_7055" s="T536">kihi-GA</ta>
            <ta e="T538" id="Seg_7056" s="T537">kepseː</ta>
            <ta e="T539" id="Seg_7057" s="T538">hanaːrgaː-BA-TInnAr</ta>
            <ta e="T540" id="Seg_7058" s="T539">di͡e-BIT</ta>
            <ta e="T541" id="Seg_7059" s="T540">aradavoj-tA</ta>
            <ta e="T542" id="Seg_7060" s="T541">ogonnʼor</ta>
            <ta e="T543" id="Seg_7061" s="T542">tu͡ok</ta>
            <ta e="T544" id="Seg_7062" s="T543">da</ta>
            <ta e="T545" id="Seg_7063" s="T544">di͡e-BAkkA</ta>
            <ta e="T546" id="Seg_7064" s="T545">čaːskɨ-tI-GAr</ta>
            <ta e="T547" id="Seg_7065" s="T546">čaːj</ta>
            <ta e="T548" id="Seg_7066" s="T547">kut-I-n-BIT</ta>
            <ta e="T549" id="Seg_7067" s="T548">bihigi</ta>
            <ta e="T550" id="Seg_7068" s="T549">ogonnʼor-LAr</ta>
            <ta e="T551" id="Seg_7069" s="T550">kajdak</ta>
            <ta e="T552" id="Seg_7070" s="T551">ötü͡ö</ta>
            <ta e="T553" id="Seg_7071" s="T552">bu͡ol-IAK.[tI]-n</ta>
            <ta e="T554" id="Seg_7072" s="T553">di͡e-An</ta>
            <ta e="T555" id="Seg_7073" s="T554">hübe</ta>
            <ta e="T556" id="Seg_7074" s="T555">bi͡er-A-BIt</ta>
            <ta e="T557" id="Seg_7075" s="T556">kirdik</ta>
            <ta e="T558" id="Seg_7076" s="T557">horok</ta>
            <ta e="T559" id="Seg_7077" s="T558">genne</ta>
            <ta e="T560" id="Seg_7078" s="T559">hɨːha</ta>
            <ta e="T561" id="Seg_7079" s="T560">haŋar-A-BIt</ta>
            <ta e="T562" id="Seg_7080" s="T561">bu͡ol-IAK-tA</ta>
            <ta e="T563" id="Seg_7081" s="T562">dʼaktar-LAr</ta>
            <ta e="T564" id="Seg_7082" s="T563">ɨtaː-A-n-Ar-LArA</ta>
            <ta e="T565" id="Seg_7083" s="T564">bert</ta>
            <ta e="T566" id="Seg_7084" s="T565">kihi</ta>
            <ta e="T567" id="Seg_7085" s="T566">ü͡örek-tI-n</ta>
            <ta e="T569" id="Seg_7086" s="T568">kihi</ta>
            <ta e="T570" id="Seg_7087" s="T569">ü͡örek-tI-n</ta>
            <ta e="T571" id="Seg_7088" s="T570">karaj-Ar-tA</ta>
            <ta e="T572" id="Seg_7089" s="T571">olus</ta>
            <ta e="T573" id="Seg_7090" s="T572">hürdeːk</ta>
            <ta e="T574" id="Seg_7091" s="T573">olus</ta>
            <ta e="T575" id="Seg_7092" s="T574">kɨra</ta>
            <ta e="T576" id="Seg_7093" s="T575">ogo-LAr-GA</ta>
            <ta e="T577" id="Seg_7094" s="T576">ulakan</ta>
            <ta e="T578" id="Seg_7095" s="T577">dʼon-nI</ta>
            <ta e="T579" id="Seg_7096" s="T578">targat-IAK-GA</ta>
            <ta e="T580" id="Seg_7097" s="T579">huptu</ta>
            <ta e="T581" id="Seg_7098" s="T580">kör-Ar</ta>
            <ta e="T582" id="Seg_7099" s="T581">bu͡ol-IAK-LArI-n</ta>
            <ta e="T583" id="Seg_7100" s="T582">olus</ta>
            <ta e="T584" id="Seg_7101" s="T583">küččügüj</ta>
            <ta e="T585" id="Seg_7102" s="T584">ogo</ta>
            <ta e="T586" id="Seg_7103" s="T585">tugut</ta>
            <ta e="T587" id="Seg_7104" s="T586">kördük</ta>
            <ta e="T588" id="Seg_7105" s="T587">kanna</ta>
            <ta e="T589" id="Seg_7106" s="T588">bar-Ar-tI-n</ta>
            <ta e="T590" id="Seg_7107" s="T589">bil-BAT</ta>
            <ta e="T591" id="Seg_7108" s="T590">ötü͡ö-LIk</ta>
            <ta e="T592" id="Seg_7109" s="T591">taŋɨn-A</ta>
            <ta e="T593" id="Seg_7110" s="T592">hataː-BAT</ta>
            <ta e="T594" id="Seg_7111" s="T593">hiti</ta>
            <ta e="T595" id="Seg_7112" s="T594">kirdik</ta>
            <ta e="T596" id="Seg_7113" s="T595">di͡e-BIT</ta>
            <ta e="T597" id="Seg_7114" s="T596">aradavoj-tA</ta>
            <ta e="T598" id="Seg_7115" s="T597">ogonnʼor</ta>
            <ta e="T599" id="Seg_7116" s="T598">bu</ta>
            <ta e="T600" id="Seg_7117" s="T599">munnʼak</ta>
            <ta e="T601" id="Seg_7118" s="T600">ajdaːn-tI-GAr</ta>
            <ta e="T602" id="Seg_7119" s="T601">umun-An</ta>
            <ta e="T603" id="Seg_7120" s="T602">keːs-BIT</ta>
            <ta e="T604" id="Seg_7121" s="T603">kɨhɨn</ta>
            <ta e="T605" id="Seg_7122" s="T604">haŋa</ta>
            <ta e="T606" id="Seg_7123" s="T605">dʼɨl</ta>
            <ta e="T607" id="Seg_7124" s="T606">kel-Ar</ta>
            <ta e="T608" id="Seg_7125" s="T607">kem-tI-GAr</ta>
            <ta e="T609" id="Seg_7126" s="T608">ogo-LAr</ta>
            <ta e="T610" id="Seg_7127" s="T609">hɨnnʼan-A</ta>
            <ta e="T611" id="Seg_7128" s="T610">kel-IAK-LArA</ta>
            <ta e="T612" id="Seg_7129" s="T611">dʼi͡e-LArI-GAr</ta>
            <ta e="T613" id="Seg_7130" s="T612">haːs</ta>
            <ta e="T614" id="Seg_7131" s="T613">maːj</ta>
            <ta e="T615" id="Seg_7132" s="T614">ɨj-tI-GAr</ta>
            <ta e="T616" id="Seg_7133" s="T615">biːr</ta>
            <ta e="T617" id="Seg_7134" s="T616">dʼɨl-LAːgI</ta>
            <ta e="T618" id="Seg_7135" s="T617">ü͡örek-LArI-n</ta>
            <ta e="T619" id="Seg_7136" s="T618">büt-TAK-TArInA</ta>
            <ta e="T620" id="Seg_7137" s="T619">kühün-GA</ta>
            <ta e="T621" id="Seg_7138" s="T620">di͡eri</ta>
            <ta e="T622" id="Seg_7139" s="T621">dʼi͡e-LArI-GAr</ta>
            <ta e="T623" id="Seg_7140" s="T622">bu͡ol-IAK-LArA</ta>
            <ta e="T624" id="Seg_7141" s="T623">iti</ta>
            <ta e="T625" id="Seg_7142" s="T624">ös-nI</ta>
            <ta e="T626" id="Seg_7143" s="T625">min</ta>
            <ta e="T627" id="Seg_7144" s="T626">barɨ</ta>
            <ta e="T628" id="Seg_7145" s="T627">kihi-GA</ta>
            <ta e="T629" id="Seg_7146" s="T628">kepseː-IAK-m</ta>
            <ta e="T630" id="Seg_7147" s="T629">kihi-LAr</ta>
            <ta e="T631" id="Seg_7148" s="T630">di͡e-A</ta>
            <ta e="T632" id="Seg_7149" s="T631">hanaː-Ar-LAr</ta>
            <ta e="T633" id="Seg_7150" s="T632">kahan</ta>
            <ta e="T634" id="Seg_7151" s="T633">da</ta>
            <ta e="T635" id="Seg_7152" s="T634">tönün-A-r-BAkkA</ta>
            <ta e="T636" id="Seg_7153" s="T635">ɨl-Ar-LAr</ta>
            <ta e="T637" id="Seg_7154" s="T636">ogo-LAr-nI</ta>
            <ta e="T638" id="Seg_7155" s="T637">ü͡ör-BIččA</ta>
            <ta e="T639" id="Seg_7156" s="T638">ogonnʼor</ta>
            <ta e="T640" id="Seg_7157" s="T639">haŋar-BIT</ta>
            <ta e="T641" id="Seg_7158" s="T640">ogonnʼor</ta>
            <ta e="T642" id="Seg_7159" s="T641">dʼi͡e-tI-GAr</ta>
            <ta e="T643" id="Seg_7160" s="T642">bar-BIT-tI-n</ta>
            <ta e="T644" id="Seg_7161" s="T643">genne</ta>
            <ta e="T645" id="Seg_7162" s="T644">Anʼiːsʼim</ta>
            <ta e="T646" id="Seg_7163" s="T645">beje-tI-n</ta>
            <ta e="T647" id="Seg_7164" s="T646">kɨtta</ta>
            <ta e="T648" id="Seg_7165" s="T647">kepset-Ar</ta>
            <ta e="T649" id="Seg_7166" s="T648">barɨ</ta>
            <ta e="T650" id="Seg_7167" s="T649">kihi-GA</ta>
            <ta e="T651" id="Seg_7168" s="T650">öjdöː-A-t-An</ta>
            <ta e="T652" id="Seg_7169" s="T651">is-IAK-GA</ta>
            <ta e="T653" id="Seg_7170" s="T652">naːda</ta>
            <ta e="T654" id="Seg_7171" s="T653">kajdak</ta>
            <ta e="T655" id="Seg_7172" s="T654">haŋa</ta>
            <ta e="T656" id="Seg_7173" s="T655">ɨjaːk</ta>
            <ta e="T657" id="Seg_7174" s="T656">ötü͡ö-nI</ta>
            <ta e="T658" id="Seg_7175" s="T657">oŋor-Ar-tI-n</ta>
            <ta e="T659" id="Seg_7176" s="T658">tɨ͡a</ta>
            <ta e="T660" id="Seg_7177" s="T659">dʼon-tI-n</ta>
            <ta e="T661" id="Seg_7178" s="T660">ilin-tI-GAr</ta>
            <ta e="T662" id="Seg_7179" s="T661">itinnik</ta>
            <ta e="T663" id="Seg_7180" s="T662">ogonnʼor-LAr</ta>
            <ta e="T664" id="Seg_7181" s="T663">ös</ta>
            <ta e="T665" id="Seg_7182" s="T664">ɨːt-Ar-LArA</ta>
            <ta e="T666" id="Seg_7183" s="T665">kirdik-LAːK</ta>
            <ta e="T667" id="Seg_7184" s="T666">kotuː-LAːK</ta>
            <ta e="T668" id="Seg_7185" s="T667">bu͡ol-IAK-tA</ta>
            <ta e="T669" id="Seg_7186" s="T668">nöŋü͡ö</ta>
            <ta e="T670" id="Seg_7187" s="T669">kün</ta>
            <ta e="T671" id="Seg_7188" s="T670">emi͡e</ta>
            <ta e="T672" id="Seg_7189" s="T671">munnʼak-GA</ta>
            <ta e="T673" id="Seg_7190" s="T672">kihi-LAr</ta>
            <ta e="T674" id="Seg_7191" s="T673">komuj-LIN-I-BIT-LAr</ta>
            <ta e="T676" id="Seg_7192" s="T675">Hemen</ta>
            <ta e="T677" id="Seg_7193" s="T676">ogonnʼor</ta>
            <ta e="T678" id="Seg_7194" s="T677">dʼi͡e-tI-GAr</ta>
            <ta e="T679" id="Seg_7195" s="T678">dʼon</ta>
            <ta e="T680" id="Seg_7196" s="T679">begeheː-GI-TAːgAr</ta>
            <ta e="T681" id="Seg_7197" s="T680">kajdak</ta>
            <ta e="T682" id="Seg_7198" s="T681">da</ta>
            <ta e="T683" id="Seg_7199" s="T682">ü͡örbe-LArA</ta>
            <ta e="T684" id="Seg_7200" s="T683">kötök-I-LIN-I-BIT</ta>
            <ta e="T685" id="Seg_7201" s="T684">ajdaːn</ta>
            <ta e="T686" id="Seg_7202" s="T685">kül-s-Iː-kül-s-Iː</ta>
            <ta e="T687" id="Seg_7203" s="T686">büt-AːT-In</ta>
            <ta e="T688" id="Seg_7204" s="T687">aradavoj-LArA</ta>
            <ta e="T689" id="Seg_7205" s="T688">di͡e-BIT</ta>
            <ta e="T690" id="Seg_7206" s="T689">beje-GIt</ta>
            <ta e="T691" id="Seg_7207" s="T690">hanaː-GItI-ttAn</ta>
            <ta e="T692" id="Seg_7208" s="T691">kɨradaːj</ta>
            <ta e="T693" id="Seg_7209" s="T692">ajmak-LAr-nI</ta>
            <ta e="T694" id="Seg_7210" s="T693">tut-TAr-I-ŋ</ta>
            <ta e="T695" id="Seg_7211" s="T694">ulakan</ta>
            <ta e="T696" id="Seg_7212" s="T695">ogo-LAr-GA</ta>
            <ta e="T697" id="Seg_7213" s="T696">kös-Ar</ta>
            <ta e="T698" id="Seg_7214" s="T697">kem-GA</ta>
            <ta e="T699" id="Seg_7215" s="T698">kör-Ar</ta>
            <ta e="T700" id="Seg_7216" s="T699">bu͡ol-IAK-LArI-n</ta>
            <ta e="T701" id="Seg_7217" s="T700">giniler-nI</ta>
            <ta e="T702" id="Seg_7218" s="T701">bihigi-GA</ta>
            <ta e="T703" id="Seg_7219" s="T702">di͡e-BIT-LArA</ta>
            <ta e="T704" id="Seg_7220" s="T703">ogo-LAr-BIt</ta>
            <ta e="T705" id="Seg_7221" s="T704">kɨhɨn</ta>
            <ta e="T706" id="Seg_7222" s="T705">hɨnnʼan-A</ta>
            <ta e="T707" id="Seg_7223" s="T706">kel-IAK-LArA</ta>
            <ta e="T708" id="Seg_7224" s="T707">iti</ta>
            <ta e="T709" id="Seg_7225" s="T708">kirdik</ta>
            <ta e="T710" id="Seg_7226" s="T709">haŋa</ta>
            <ta e="T711" id="Seg_7227" s="T710">du͡o</ta>
            <ta e="T712" id="Seg_7228" s="T711">ɨjɨt-BIT</ta>
            <ta e="T713" id="Seg_7229" s="T712">biːr</ta>
            <ta e="T714" id="Seg_7230" s="T713">dʼaktar</ta>
            <ta e="T715" id="Seg_7231" s="T714">Hemen</ta>
            <ta e="T716" id="Seg_7232" s="T715">ogonnʼor</ta>
            <ta e="T717" id="Seg_7233" s="T716">olor-Ar</ta>
            <ta e="T718" id="Seg_7234" s="T717">hir-tI-ttAn</ta>
            <ta e="T719" id="Seg_7235" s="T718">öjdöː-A</ta>
            <ta e="T720" id="Seg_7236" s="T719">öjdöː-A-ŋnAː-BIT</ta>
            <ta e="T721" id="Seg_7237" s="T720">iti</ta>
            <ta e="T722" id="Seg_7238" s="T721">kirdik</ta>
            <ta e="T723" id="Seg_7239" s="T722">ös</ta>
            <ta e="T724" id="Seg_7240" s="T723">kɨhɨn</ta>
            <ta e="T725" id="Seg_7241" s="T724">hɨnnʼan-A</ta>
            <ta e="T726" id="Seg_7242" s="T725">kel-IAK-LArA</ta>
            <ta e="T727" id="Seg_7243" s="T726">onton</ta>
            <ta e="T728" id="Seg_7244" s="T727">hajɨn</ta>
            <ta e="T729" id="Seg_7245" s="T728">huptu</ta>
            <ta e="T730" id="Seg_7246" s="T729">dʼi͡e-LArI-GAr</ta>
            <ta e="T731" id="Seg_7247" s="T730">bu͡ol-A</ta>
            <ta e="T732" id="Seg_7248" s="T731">kel-IAK-LArA</ta>
            <ta e="T733" id="Seg_7249" s="T732">di͡e-BIT</ta>
            <ta e="T734" id="Seg_7250" s="T733">Anʼiːsʼim</ta>
            <ta e="T735" id="Seg_7251" s="T734">Hemen</ta>
            <ta e="T736" id="Seg_7252" s="T735">ogonnʼor</ta>
            <ta e="T737" id="Seg_7253" s="T736">nalɨj-A</ta>
            <ta e="T738" id="Seg_7254" s="T737">tüs-AːT</ta>
            <ta e="T739" id="Seg_7255" s="T738">haŋa</ta>
            <ta e="T742" id="Seg_7256" s="T741">alɨn-I-BIT</ta>
            <ta e="T743" id="Seg_7257" s="T742">min</ta>
            <ta e="T744" id="Seg_7258" s="T743">tu͡ok-nI</ta>
            <ta e="T745" id="Seg_7259" s="T744">di͡e-BIT-I-m=Ij</ta>
            <ta e="T746" id="Seg_7260" s="T745">barɨ-LArA</ta>
            <ta e="T747" id="Seg_7261" s="T746">hɨlaːs</ta>
            <ta e="T748" id="Seg_7262" s="T747">karak-I-nAn</ta>
            <ta e="T749" id="Seg_7263" s="T748">kör-An</ta>
            <ta e="T750" id="Seg_7264" s="T749">Hemen</ta>
            <ta e="T751" id="Seg_7265" s="T750">ogonnʼor</ta>
            <ta e="T752" id="Seg_7266" s="T751">dek</ta>
            <ta e="T753" id="Seg_7267" s="T752">ergij-LIN-I-BIT-LAr</ta>
            <ta e="T754" id="Seg_7268" s="T753">munnʼak-LArA</ta>
            <ta e="T755" id="Seg_7269" s="T754">aːs-BIT</ta>
            <ta e="T756" id="Seg_7270" s="T755">türgen-LIk</ta>
            <ta e="T757" id="Seg_7271" s="T756">kihi-LAr</ta>
            <ta e="T758" id="Seg_7272" s="T757">uraha</ta>
            <ta e="T759" id="Seg_7273" s="T758">dʼi͡e-LArI-n</ta>
            <ta e="T760" id="Seg_7274" s="T759">aːjɨ</ta>
            <ta e="T761" id="Seg_7275" s="T760">bar-AttAː-BIT-LAr</ta>
            <ta e="T762" id="Seg_7276" s="T761">nöŋü͡ö</ta>
            <ta e="T763" id="Seg_7277" s="T762">kühün</ta>
            <ta e="T764" id="Seg_7278" s="T763">ü͡ör-LArI-n</ta>
            <ta e="T765" id="Seg_7279" s="T764">hi͡egileː-An</ta>
            <ta e="T766" id="Seg_7280" s="T765">baran</ta>
            <ta e="T767" id="Seg_7281" s="T766">taba</ta>
            <ta e="T768" id="Seg_7282" s="T767">tut-Iː-tA</ta>
            <ta e="T769" id="Seg_7283" s="T768">bu͡ol-BIT</ta>
            <ta e="T770" id="Seg_7284" s="T769">kös-Ar-GA</ta>
            <ta e="T771" id="Seg_7285" s="T770">dʼaktar-LAr</ta>
            <ta e="T772" id="Seg_7286" s="T771">dʼi͡e-LArI-n</ta>
            <ta e="T773" id="Seg_7287" s="T772">attɨ-GAr</ta>
            <ta e="T774" id="Seg_7288" s="T773">ogo-LAr-tI-n</ta>
            <ta e="T775" id="Seg_7289" s="T774">kɨtta</ta>
            <ta e="T776" id="Seg_7290" s="T775">holo-tA</ta>
            <ta e="T777" id="Seg_7291" s="T776">hu͡ok-LAr</ta>
            <ta e="T778" id="Seg_7292" s="T777">komuj-n-An-LAr</ta>
            <ta e="T779" id="Seg_7293" s="T778">ogo-LAr-tI-GAr</ta>
            <ta e="T780" id="Seg_7294" s="T779">huptu</ta>
            <ta e="T781" id="Seg_7295" s="T780">haŋar-Ar-LAr</ta>
            <ta e="T782" id="Seg_7296" s="T781">möŋ-BAT</ta>
            <ta e="T783" id="Seg_7297" s="T782">bu͡ol-IAK-LArI-n</ta>
            <ta e="T784" id="Seg_7298" s="T783">ulakan</ta>
            <ta e="T785" id="Seg_7299" s="T784">ogo-LAr-nI</ta>
            <ta e="T786" id="Seg_7300" s="T785">ihit-Ar</ta>
            <ta e="T787" id="Seg_7301" s="T786">bu͡ol-IAK-LArI-n</ta>
            <ta e="T788" id="Seg_7302" s="T787">taba</ta>
            <ta e="T789" id="Seg_7303" s="T788">tut-AːT</ta>
            <ta e="T790" id="Seg_7304" s="T789">kölün-AːT</ta>
            <ta e="T791" id="Seg_7305" s="T790">uraha</ta>
            <ta e="T792" id="Seg_7306" s="T791">dʼi͡e</ta>
            <ta e="T793" id="Seg_7307" s="T792">aːjɨ</ta>
            <ta e="T794" id="Seg_7308" s="T793">ahaː-Ar-LAr</ta>
            <ta e="T795" id="Seg_7309" s="T794">kös-Ar</ta>
            <ta e="T796" id="Seg_7310" s="T795">ilin-tI-GAr</ta>
            <ta e="T797" id="Seg_7311" s="T796">onton</ta>
            <ta e="T798" id="Seg_7312" s="T797">uraha</ta>
            <ta e="T799" id="Seg_7313" s="T798">dʼi͡e</ta>
            <ta e="T800" id="Seg_7314" s="T799">aːjɨ-ttAn</ta>
            <ta e="T801" id="Seg_7315" s="T800">barɨ-LArA</ta>
            <ta e="T802" id="Seg_7316" s="T801">bargɨj-s-An</ta>
            <ta e="T803" id="Seg_7317" s="T802">tagɨs-I-BIT-LAr</ta>
            <ta e="T804" id="Seg_7318" s="T803">ɨraːk</ta>
            <ta e="T805" id="Seg_7319" s="T804">bar-Ar</ta>
            <ta e="T806" id="Seg_7320" s="T805">ogo-LAr-nI</ta>
            <ta e="T807" id="Seg_7321" s="T806">ataːr-AːrI</ta>
            <ta e="T808" id="Seg_7322" s="T807">kahan</ta>
            <ta e="T809" id="Seg_7323" s="T808">min</ta>
            <ta e="T810" id="Seg_7324" s="T809">en-n</ta>
            <ta e="T811" id="Seg_7325" s="T810">kör-IAK-m=Ij</ta>
            <ta e="T812" id="Seg_7326" s="T811">Debgeːn</ta>
            <ta e="T813" id="Seg_7327" s="T812">ɨtaː-BIT</ta>
            <ta e="T814" id="Seg_7328" s="T813">kajdak</ta>
            <ta e="T815" id="Seg_7329" s="T814">en-n</ta>
            <ta e="T818" id="Seg_7330" s="T817">en-n-tA</ta>
            <ta e="T819" id="Seg_7331" s="T818">hu͡ok</ta>
            <ta e="T820" id="Seg_7332" s="T819">olor-IAK-m=Ij</ta>
            <ta e="T821" id="Seg_7333" s="T820">kim</ta>
            <ta e="T822" id="Seg_7334" s="T821">bihigi-GA</ta>
            <ta e="T823" id="Seg_7335" s="T822">kömölös-IAK-tA=Ij</ta>
            <ta e="T824" id="Seg_7336" s="T823">ogo-tI-n</ta>
            <ta e="T825" id="Seg_7337" s="T824">kam</ta>
            <ta e="T826" id="Seg_7338" s="T825">tut-AːT</ta>
            <ta e="T827" id="Seg_7339" s="T826">baran</ta>
            <ta e="T828" id="Seg_7340" s="T827">dʼi͡e-tI-n</ta>
            <ta e="T829" id="Seg_7341" s="T828">dek</ta>
            <ta e="T830" id="Seg_7342" s="T829">hos-Ar</ta>
            <ta e="T831" id="Seg_7343" s="T830">en</ta>
            <ta e="T832" id="Seg_7344" s="T831">tu͡ok</ta>
            <ta e="T833" id="Seg_7345" s="T832">bu͡ol-A-GIn</ta>
            <ta e="T834" id="Seg_7346" s="T833">biːr</ta>
            <ta e="T835" id="Seg_7347" s="T834">biːr</ta>
            <ta e="T836" id="Seg_7348" s="T835">dʼaktar</ta>
            <ta e="T837" id="Seg_7349" s="T836">di͡e-BIT</ta>
            <ta e="T838" id="Seg_7350" s="T837">dogor-tI-GAr</ta>
            <ta e="T839" id="Seg_7351" s="T838">ogo-nI</ta>
            <ta e="T840" id="Seg_7352" s="T839">hanaː-Ar-GA</ta>
            <ta e="T841" id="Seg_7353" s="T840">battaː-I-BA</ta>
            <ta e="T842" id="Seg_7354" s="T841">kös-Ar</ta>
            <ta e="T843" id="Seg_7355" s="T842">ilin-tI-GAr</ta>
            <ta e="T844" id="Seg_7356" s="T843">itigirdik</ta>
            <ta e="T845" id="Seg_7357" s="T844">ataːr-Ar</ta>
            <ta e="T846" id="Seg_7358" s="T845">olus</ta>
            <ta e="T847" id="Seg_7359" s="T846">kuhagan</ta>
            <ta e="T848" id="Seg_7360" s="T847">anʼɨː</ta>
            <ta e="T849" id="Seg_7361" s="T848">ataːr</ta>
            <ta e="T850" id="Seg_7362" s="T849">kihi-LIː</ta>
            <ta e="T851" id="Seg_7363" s="T850">barɨ</ta>
            <ta e="T852" id="Seg_7364" s="T851">uːčak-LAːK</ta>
            <ta e="T853" id="Seg_7365" s="T852">ogo-LAr</ta>
            <ta e="T854" id="Seg_7366" s="T853">ehe-LArI-n</ta>
            <ta e="T855" id="Seg_7367" s="T854">kelin-tI-ttAn</ta>
            <ta e="T856" id="Seg_7368" s="T855">bar-An</ta>
            <ta e="T857" id="Seg_7369" s="T856">uhun</ta>
            <ta e="T858" id="Seg_7370" s="T857">kös</ta>
            <ta e="T859" id="Seg_7371" s="T858">bu͡ol-BIT-LAr</ta>
            <ta e="T860" id="Seg_7372" s="T859">tur-BAT-tur-Ar</ta>
            <ta e="T861" id="Seg_7373" s="T860">ogo-LArI-n</ta>
            <ta e="T862" id="Seg_7374" s="T861">ataːr-An</ta>
            <ta e="T863" id="Seg_7375" s="T862">ol</ta>
            <ta e="T864" id="Seg_7376" s="T863">kös</ta>
            <ta e="T865" id="Seg_7377" s="T864">kelin-tI-ttAn</ta>
            <ta e="T866" id="Seg_7378" s="T865">huburuj-BIT-LAr</ta>
            <ta e="T867" id="Seg_7379" s="T866">Parfiraj</ta>
            <ta e="T868" id="Seg_7380" s="T867">ogonnʼor</ta>
            <ta e="T869" id="Seg_7381" s="T868">karak-LAr-tI-n</ta>
            <ta e="T870" id="Seg_7382" s="T869">bɨlčaj-ččI</ta>
            <ta e="T871" id="Seg_7383" s="T870">kör-An</ta>
            <ta e="T872" id="Seg_7384" s="T871">tur-An</ta>
            <ta e="T873" id="Seg_7385" s="T872">ele</ta>
            <ta e="T874" id="Seg_7386" s="T873">küːs-tI-nAn</ta>
            <ta e="T875" id="Seg_7387" s="T874">algaː-A</ta>
            <ta e="T876" id="Seg_7388" s="T875">tur-BIT</ta>
            <ta e="T877" id="Seg_7389" s="T876">dʼe</ta>
            <ta e="T878" id="Seg_7390" s="T877">eteŋŋe</ta>
            <ta e="T879" id="Seg_7391" s="T878">hɨrɨt-I-ŋ</ta>
            <ta e="T880" id="Seg_7392" s="T879">ehigi</ta>
            <ta e="T881" id="Seg_7393" s="T880">hu͡ol-GItI-GAr</ta>
            <ta e="T882" id="Seg_7394" s="T881">kim</ta>
            <ta e="T883" id="Seg_7395" s="T882">da</ta>
            <ta e="T884" id="Seg_7396" s="T883">haːrat-BAT-tI-n</ta>
            <ta e="T885" id="Seg_7397" s="T884">ulakan</ta>
            <ta e="T886" id="Seg_7398" s="T885">öj-nI</ta>
            <ta e="T887" id="Seg_7399" s="T886">ɨl-I-ŋ</ta>
            <ta e="T888" id="Seg_7400" s="T887">türgen-LIk</ta>
            <ta e="T889" id="Seg_7401" s="T888">tönün-I-ŋ</ta>
            <ta e="T890" id="Seg_7402" s="T889">töröː-BIT</ta>
            <ta e="T891" id="Seg_7403" s="T890">hir-GItI-GAr</ta>
            <ta e="T892" id="Seg_7404" s="T891">kaːl-A</ta>
            <ta e="T893" id="Seg_7405" s="T892">dʼon</ta>
            <ta e="T894" id="Seg_7406" s="T893">ör</ta>
            <ta e="T895" id="Seg_7407" s="T894">bagajɨ</ta>
            <ta e="T896" id="Seg_7408" s="T895">kör-A</ta>
            <ta e="T897" id="Seg_7409" s="T896">tur-BIT-LAr</ta>
            <ta e="T898" id="Seg_7410" s="T897">mu͡ora</ta>
            <ta e="T899" id="Seg_7411" s="T898">ürüt-tI-nAn</ta>
            <ta e="T900" id="Seg_7412" s="T899">uhun</ta>
            <ta e="T901" id="Seg_7413" s="T900">kös</ta>
            <ta e="T902" id="Seg_7414" s="T901">hüt-IAK.[tI]-r</ta>
            <ta e="T903" id="Seg_7415" s="T902">di͡eri</ta>
            <ta e="T904" id="Seg_7416" s="T903">his</ta>
            <ta e="T905" id="Seg_7417" s="T904">ɨnaraː</ta>
            <ta e="T906" id="Seg_7418" s="T905">örüt-tI-GAr</ta>
            <ta e="T907" id="Seg_7419" s="T906">ogo</ta>
            <ta e="T908" id="Seg_7420" s="T907">ajmak-GA</ta>
            <ta e="T909" id="Seg_7421" s="T908">törüt-LAN-TI-tA</ta>
            <ta e="T910" id="Seg_7422" s="T909">haŋa</ta>
            <ta e="T911" id="Seg_7423" s="T910">üje</ta>
            <ta e="T912" id="Seg_7424" s="T911">ü͡ören-Ar</ta>
            <ta e="T913" id="Seg_7425" s="T912">üje</ta>
            <ta e="T914" id="Seg_7426" s="T913">huruk-GA</ta>
            <ta e="T915" id="Seg_7427" s="T914">ehigi</ta>
            <ta e="T916" id="Seg_7428" s="T915">ihilleː-TI-GIt</ta>
            <ta e="T917" id="Seg_7429" s="T916">ös-nI</ta>
            <ta e="T918" id="Seg_7430" s="T917">Voločanka</ta>
            <ta e="T919" id="Seg_7431" s="T918">dek</ta>
            <ta e="T920" id="Seg_7432" s="T919">kös-Iː</ta>
            <ta e="T921" id="Seg_7433" s="T920">aːk-BIT-tA</ta>
            <ta e="T922" id="Seg_7434" s="T921">Papov</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_7435" s="T0">Volochanka.[NOM]</ta>
            <ta e="T2" id="Seg_7436" s="T1">to</ta>
            <ta e="T3" id="Seg_7437" s="T2">nomadize-NMNZ.[NOM]</ta>
            <ta e="T4" id="Seg_7438" s="T3">botfly.[NOM]</ta>
            <ta e="T5" id="Seg_7439" s="T4">month-3SG.[NOM]</ta>
            <ta e="T6" id="Seg_7440" s="T5">end-PTCP.PRS</ta>
            <ta e="T7" id="Seg_7441" s="T6">day-PL-3SG.[NOM]</ta>
            <ta e="T8" id="Seg_7442" s="T7">stand.up-PST2-3PL</ta>
            <ta e="T9" id="Seg_7443" s="T8">warm.[NOM]</ta>
            <ta e="T10" id="Seg_7444" s="T9">very</ta>
            <ta e="T11" id="Seg_7445" s="T10">grass-PL.[NOM]</ta>
            <ta e="T12" id="Seg_7446" s="T11">leaf-PL.[NOM]</ta>
            <ta e="T13" id="Seg_7447" s="T12">yellow-PST2-3PL</ta>
            <ta e="T14" id="Seg_7448" s="T13">tundra.[NOM]</ta>
            <ta e="T15" id="Seg_7449" s="T14">there-here</ta>
            <ta e="T16" id="Seg_7450" s="T15">red</ta>
            <ta e="T17" id="Seg_7451" s="T16">gold.[NOM]</ta>
            <ta e="T18" id="Seg_7452" s="T17">become-CVB.SEQ</ta>
            <ta e="T19" id="Seg_7453" s="T18">go-PST2.[3SG]</ta>
            <ta e="T20" id="Seg_7454" s="T19">that</ta>
            <ta e="T21" id="Seg_7455" s="T20">day-PL-DAT/LOC</ta>
            <ta e="T22" id="Seg_7456" s="T21">know-NMNZ-PROPR</ta>
            <ta e="T23" id="Seg_7457" s="T22">big</ta>
            <ta e="T24" id="Seg_7458" s="T23">lake.[NOM]</ta>
            <ta e="T25" id="Seg_7459" s="T24">shore-3SG-DAT/LOC</ta>
            <ta e="T26" id="Seg_7460" s="T25">big</ta>
            <ta e="T27" id="Seg_7461" s="T26">gather-REFL-NMNZ.[NOM]</ta>
            <ta e="T28" id="Seg_7462" s="T27">stand-PST2-3SG</ta>
            <ta e="T29" id="Seg_7463" s="T28">child.[NOM]</ta>
            <ta e="T30" id="Seg_7464" s="T29">people-ACC</ta>
            <ta e="T31" id="Seg_7465" s="T30">prepare-PRS-3PL</ta>
            <ta e="T32" id="Seg_7466" s="T31">Volochanka-DAT/LOC</ta>
            <ta e="T33" id="Seg_7467" s="T32">take.away-CVB.PURP</ta>
            <ta e="T34" id="Seg_7468" s="T33">education-DAT/LOC</ta>
            <ta e="T35" id="Seg_7469" s="T34">tundra.[NOM]</ta>
            <ta e="T36" id="Seg_7470" s="T35">population.[NOM]</ta>
            <ta e="T37" id="Seg_7471" s="T36">before</ta>
            <ta e="T38" id="Seg_7472" s="T37">know-NEG.PTCP</ta>
            <ta e="T39" id="Seg_7473" s="T38">be-PST1-3PL</ta>
            <ta e="T40" id="Seg_7474" s="T39">child-PL-3SG-ACC</ta>
            <ta e="T41" id="Seg_7475" s="T40">education-DAT/LOC</ta>
            <ta e="T42" id="Seg_7476" s="T41">send-PTCP.PRS-ACC</ta>
            <ta e="T43" id="Seg_7477" s="T42">that.[NOM]</ta>
            <ta e="T44" id="Seg_7478" s="T43">because.of</ta>
            <ta e="T45" id="Seg_7479" s="T44">wish-PL-3SG.[NOM]</ta>
            <ta e="T46" id="Seg_7480" s="T45">go-NEG.[3SG]</ta>
            <ta e="T47" id="Seg_7481" s="T46">school-DAT/LOC</ta>
            <ta e="T48" id="Seg_7482" s="T47">give-PTCP.PRS-DAT/LOC</ta>
            <ta e="T49" id="Seg_7483" s="T48">various</ta>
            <ta e="T50" id="Seg_7484" s="T49">word-ACC</ta>
            <ta e="T51" id="Seg_7485" s="T50">find-PRS-3PL</ta>
            <ta e="T52" id="Seg_7486" s="T51">send-EP-NEG-CVB.PURP</ta>
            <ta e="T53" id="Seg_7487" s="T52">child-PL-3SG-ACC</ta>
            <ta e="T54" id="Seg_7488" s="T53">tribal-3PL-DAT/LOC</ta>
            <ta e="T55" id="Seg_7489" s="T54">that</ta>
            <ta e="T56" id="Seg_7490" s="T55">day-PL-DAT/LOC</ta>
            <ta e="T57" id="Seg_7491" s="T56">very</ta>
            <ta e="T58" id="Seg_7492" s="T57">pain-PROPR</ta>
            <ta e="T59" id="Seg_7493" s="T58">work-ACC</ta>
            <ta e="T60" id="Seg_7494" s="T59">make-PST2-3PL</ta>
            <ta e="T61" id="Seg_7495" s="T60">every</ta>
            <ta e="T62" id="Seg_7496" s="T61">pole.[NOM]</ta>
            <ta e="T63" id="Seg_7497" s="T62">tent.[NOM]</ta>
            <ta e="T64" id="Seg_7498" s="T63">every</ta>
            <ta e="T65" id="Seg_7499" s="T64">go-CVB.SEQ</ta>
            <ta e="T66" id="Seg_7500" s="T65">chat-CVB.SIM</ta>
            <ta e="T67" id="Seg_7501" s="T66">try-PST2.[3SG]</ta>
            <ta e="T68" id="Seg_7502" s="T67">child-PL-3SG-ACC</ta>
            <ta e="T69" id="Seg_7503" s="T68">school-DAT/LOC</ta>
            <ta e="T70" id="Seg_7504" s="T69">give-PTCP.FUT-3PL-ACC</ta>
            <ta e="T71" id="Seg_7505" s="T70">education-DAT/LOC</ta>
            <ta e="T72" id="Seg_7506" s="T71">send-PTCP.FUT-3PL-ACC</ta>
            <ta e="T73" id="Seg_7507" s="T72">that-ABL</ta>
            <ta e="T74" id="Seg_7508" s="T73">what.[NOM]</ta>
            <ta e="T75" id="Seg_7509" s="T74">NEG</ta>
            <ta e="T76" id="Seg_7510" s="T75">appear-EP-PST2.NEG.[3SG]</ta>
            <ta e="T77" id="Seg_7511" s="T76">that.[NOM]</ta>
            <ta e="T78" id="Seg_7512" s="T77">because.of</ta>
            <ta e="T79" id="Seg_7513" s="T78">agreement.[NOM]</ta>
            <ta e="T80" id="Seg_7514" s="T79">become-PST2.[3SG]</ta>
            <ta e="T81" id="Seg_7515" s="T80">make-PTCP.PRS-DAT/LOC</ta>
            <ta e="T82" id="Seg_7516" s="T81">say-CVB.SEQ</ta>
            <ta e="T83" id="Seg_7517" s="T82">gathering-ACC</ta>
            <ta e="T84" id="Seg_7518" s="T83">tundra.[NOM]</ta>
            <ta e="T85" id="Seg_7519" s="T84">upper.part-3SG-INSTR</ta>
            <ta e="T86" id="Seg_7520" s="T85">every-3PL.[NOM]</ta>
            <ta e="T87" id="Seg_7521" s="T86">gather-PASS/REFL-PTCP.FUT-3PL-ACC</ta>
            <ta e="T88" id="Seg_7522" s="T87">face.[NOM]</ta>
            <ta e="T89" id="Seg_7523" s="T88">face-DAT/LOC</ta>
            <ta e="T90" id="Seg_7524" s="T89">chat-NMNZ.[NOM]</ta>
            <ta e="T91" id="Seg_7525" s="T90">become-PTCP.FUT-3SG-ACC</ta>
            <ta e="T92" id="Seg_7526" s="T91">one</ta>
            <ta e="T93" id="Seg_7527" s="T92">big</ta>
            <ta e="T94" id="Seg_7528" s="T93">pole.[NOM]</ta>
            <ta e="T95" id="Seg_7529" s="T94">house-DAT/LOC</ta>
            <ta e="T96" id="Seg_7530" s="T95">hardly</ta>
            <ta e="T97" id="Seg_7531" s="T96">fit-CVB.SEQ</ta>
            <ta e="T98" id="Seg_7532" s="T97">gather-PASS/REFL-EP-PST2-3PL</ta>
            <ta e="T99" id="Seg_7533" s="T98">stand-PRS.[3SG]</ta>
            <ta e="T100" id="Seg_7534" s="T99">stand-NEG.[3SG]</ta>
            <ta e="T101" id="Seg_7535" s="T100">man.[NOM]</ta>
            <ta e="T102" id="Seg_7536" s="T101">human.being-PL.[NOM]</ta>
            <ta e="T103" id="Seg_7537" s="T102">what.[NOM]</ta>
            <ta e="T104" id="Seg_7538" s="T103">NEG</ta>
            <ta e="T105" id="Seg_7539" s="T104">say-PTCP.FUT-3PL-ACC</ta>
            <ta e="T106" id="Seg_7540" s="T105">power-3SG-ABL</ta>
            <ta e="T107" id="Seg_7541" s="T106">earth.[NOM]</ta>
            <ta e="T108" id="Seg_7542" s="T107">to</ta>
            <ta e="T109" id="Seg_7543" s="T108">only</ta>
            <ta e="T110" id="Seg_7544" s="T109">see-PRS-3PL</ta>
            <ta e="T111" id="Seg_7545" s="T110">woman.[NOM]</ta>
            <ta e="T112" id="Seg_7546" s="T111">people.[NOM]</ta>
            <ta e="T113" id="Seg_7547" s="T112">kerchief-3PL-INSTR</ta>
            <ta e="T114" id="Seg_7548" s="T113">eye-3PL-GEN</ta>
            <ta e="T115" id="Seg_7549" s="T114">water-3PL-ACC</ta>
            <ta e="T116" id="Seg_7550" s="T115">wipe-EP-MED-PRS-3PL</ta>
            <ta e="T117" id="Seg_7551" s="T116">%%-PL.[NOM]</ta>
            <ta e="T118" id="Seg_7552" s="T117">what.[NOM]</ta>
            <ta e="T119" id="Seg_7553" s="T118">be-CVB.SEQ-2PL</ta>
            <ta e="T120" id="Seg_7554" s="T119">head-PL-2PL-ACC</ta>
            <ta e="T121" id="Seg_7555" s="T120">knee-PL-2PL.[NOM]</ta>
            <ta e="T122" id="Seg_7556" s="T121">inside-3SG-DAT/LOC</ta>
            <ta e="T123" id="Seg_7557" s="T122">go.in-CAUS-PST1-2PL</ta>
            <ta e="T124" id="Seg_7558" s="T123">why</ta>
            <ta e="T125" id="Seg_7559" s="T124">get.silent-RECP/COLL-CVB.SIM</ta>
            <ta e="T126" id="Seg_7560" s="T125">lie-PRS-2PL</ta>
            <ta e="T127" id="Seg_7561" s="T126">speak-PST2.[3SG]</ta>
            <ta e="T128" id="Seg_7562" s="T127">say-CVB.SEQ</ta>
            <ta e="T129" id="Seg_7563" s="T128">Evgeniya</ta>
            <ta e="T130" id="Seg_7564" s="T129">language-PROPR</ta>
            <ta e="T131" id="Seg_7565" s="T130">old.woman.[NOM]</ta>
            <ta e="T132" id="Seg_7566" s="T131">shout-CVB.SEQ</ta>
            <ta e="T133" id="Seg_7567" s="T132">give-PST2.[3SG]</ta>
            <ta e="T134" id="Seg_7568" s="T133">3SG.[NOM]</ta>
            <ta e="T135" id="Seg_7569" s="T134">MOD</ta>
            <ta e="T136" id="Seg_7570" s="T135">pert-3SG.[NOM]</ta>
            <ta e="T137" id="Seg_7571" s="T136">very</ta>
            <ta e="T138" id="Seg_7572" s="T137">who-ABL</ta>
            <ta e="T139" id="Seg_7573" s="T138">NEG</ta>
            <ta e="T140" id="Seg_7574" s="T139">be.afraid-EP-NEG.[3SG]</ta>
            <ta e="T141" id="Seg_7575" s="T140">sit-PTCP.PRS</ta>
            <ta e="T142" id="Seg_7576" s="T141">place-ABL</ta>
            <ta e="T143" id="Seg_7577" s="T142">silently</ta>
            <ta e="T144" id="Seg_7578" s="T143">stand.up-PST2.[3SG]</ta>
            <ta e="T145" id="Seg_7579" s="T144">Porfirij</ta>
            <ta e="T146" id="Seg_7580" s="T145">old.man.[NOM]</ta>
            <ta e="T147" id="Seg_7581" s="T146">bald</ta>
            <ta e="T148" id="Seg_7582" s="T147">head-3SG-ACC</ta>
            <ta e="T149" id="Seg_7583" s="T148">stroke-CVB.SIM</ta>
            <ta e="T150" id="Seg_7584" s="T149">fall-CVB.ANT</ta>
            <ta e="T151" id="Seg_7585" s="T150">hurry-NEG.CVB.SIM</ta>
            <ta e="T152" id="Seg_7586" s="T151">watch-EP-MED-PST2.[3SG]</ta>
            <ta e="T153" id="Seg_7587" s="T152">how</ta>
            <ta e="T154" id="Seg_7588" s="T153">say-FUT-1SG=Q</ta>
            <ta e="T155" id="Seg_7589" s="T154">that.[NOM]</ta>
            <ta e="T156" id="Seg_7590" s="T155">like.that</ta>
            <ta e="T157" id="Seg_7591" s="T156">be-PRS.[3SG]</ta>
            <ta e="T158" id="Seg_7592" s="T157">child-PL.[NOM]</ta>
            <ta e="T159" id="Seg_7593" s="T158">this</ta>
            <ta e="T160" id="Seg_7594" s="T159">new</ta>
            <ta e="T161" id="Seg_7595" s="T160">time-DAT/LOC</ta>
            <ta e="T162" id="Seg_7596" s="T161">learn-PTCP.PRS-3PL.[NOM]</ta>
            <ta e="T163" id="Seg_7597" s="T162">however</ta>
            <ta e="T164" id="Seg_7598" s="T163">EMPH</ta>
            <ta e="T165" id="Seg_7599" s="T164">very</ta>
            <ta e="T166" id="Seg_7600" s="T165">right.[NOM]</ta>
            <ta e="T167" id="Seg_7601" s="T166">3PL.[NOM]</ta>
            <ta e="T168" id="Seg_7602" s="T167">writing-AG.[NOM]</ta>
            <ta e="T169" id="Seg_7603" s="T168">be-PTCP.FUT</ta>
            <ta e="T170" id="Seg_7604" s="T169">fall-NEC-3PL</ta>
            <ta e="T171" id="Seg_7605" s="T170">this</ta>
            <ta e="T172" id="Seg_7606" s="T171">1SG.[NOM]</ta>
            <ta e="T173" id="Seg_7607" s="T172">Ivan.[NOM]</ta>
            <ta e="T174" id="Seg_7608" s="T173">son-1SG-ACC</ta>
            <ta e="T175" id="Seg_7609" s="T174">small-3SG-ABL</ta>
            <ta e="T176" id="Seg_7610" s="T175">teach-PST2-EP-1SG</ta>
            <ta e="T177" id="Seg_7611" s="T176">fish.[NOM]</ta>
            <ta e="T178" id="Seg_7612" s="T177">animal.[NOM]</ta>
            <ta e="T179" id="Seg_7613" s="T178">gin.trap-VBZ-PTCP.FUT-3SG-ACC</ta>
            <ta e="T180" id="Seg_7614" s="T179">1SG.[NOM]</ta>
            <ta e="T181" id="Seg_7615" s="T180">now</ta>
            <ta e="T182" id="Seg_7616" s="T181">age-PST1-1SG</ta>
            <ta e="T183" id="Seg_7617" s="T182">work-ACC</ta>
            <ta e="T184" id="Seg_7618" s="T183">make.it-EP-MED-NEG.PTCP</ta>
            <ta e="T185" id="Seg_7619" s="T184">be-CVB.SEQ</ta>
            <ta e="T186" id="Seg_7620" s="T185">be-EP-1SG</ta>
            <ta e="T187" id="Seg_7621" s="T186">who.[NOM]</ta>
            <ta e="T188" id="Seg_7622" s="T187">1SG.[NOM]</ta>
            <ta e="T189" id="Seg_7623" s="T188">family-1SG-ACC</ta>
            <ta e="T190" id="Seg_7624" s="T189">eat-EP-CAUS-FUT-3SG=Q</ta>
            <ta e="T191" id="Seg_7625" s="T190">tundra-1PL.[NOM]</ta>
            <ta e="T192" id="Seg_7626" s="T191">cold.[NOM]</ta>
            <ta e="T193" id="Seg_7627" s="T192">very</ta>
            <ta e="T194" id="Seg_7628" s="T193">snowstorm-PROPR.[NOM]</ta>
            <ta e="T195" id="Seg_7629" s="T194">strong</ta>
            <ta e="T196" id="Seg_7630" s="T195">just</ta>
            <ta e="T197" id="Seg_7631" s="T196">young.[NOM]</ta>
            <ta e="T198" id="Seg_7632" s="T197">only</ta>
            <ta e="T199" id="Seg_7633" s="T198">human.being.[NOM]</ta>
            <ta e="T200" id="Seg_7634" s="T199">haul-ACC</ta>
            <ta e="T201" id="Seg_7635" s="T200">find-HAB.[3SG]</ta>
            <ta e="T202" id="Seg_7636" s="T201">Ivan-ACC</ta>
            <ta e="T203" id="Seg_7637" s="T202">school-DAT/LOC</ta>
            <ta e="T205" id="Seg_7638" s="T204">deadfall-ACC</ta>
            <ta e="T206" id="Seg_7639" s="T205">gin.trap-ACC</ta>
            <ta e="T207" id="Seg_7640" s="T206">install-CVB.SIM</ta>
            <ta e="T208" id="Seg_7641" s="T207">teach-FUT-3PL</ta>
            <ta e="T209" id="Seg_7642" s="T208">NEG-3SG</ta>
            <ta e="T210" id="Seg_7643" s="T209">3SG.[NOM]</ta>
            <ta e="T211" id="Seg_7644" s="T210">forget-FUT-3SG</ta>
            <ta e="T212" id="Seg_7645" s="T211">tundra.[NOM]</ta>
            <ta e="T213" id="Seg_7646" s="T212">work-3SG-ACC</ta>
            <ta e="T214" id="Seg_7647" s="T213">woman.[NOM]</ta>
            <ta e="T215" id="Seg_7648" s="T214">similar</ta>
            <ta e="T216" id="Seg_7649" s="T215">house-DAT/LOC</ta>
            <ta e="T217" id="Seg_7650" s="T216">only</ta>
            <ta e="T218" id="Seg_7651" s="T217">sit-FUT-3SG</ta>
            <ta e="T219" id="Seg_7652" s="T218">what-ACC</ta>
            <ta e="T220" id="Seg_7653" s="T219">NEG</ta>
            <ta e="T221" id="Seg_7654" s="T220">make-NEG.CVB.SIM</ta>
            <ta e="T222" id="Seg_7655" s="T221">2SG.[NOM]</ta>
            <ta e="T223" id="Seg_7656" s="T222">old.man.[NOM]</ta>
            <ta e="T224" id="Seg_7657" s="T223">woman-PL-ACC</ta>
            <ta e="T225" id="Seg_7658" s="T224">touch-EP-NEG.[IMP.2SG]</ta>
            <ta e="T226" id="Seg_7659" s="T225">again</ta>
            <ta e="T227" id="Seg_7660" s="T226">Evgeniya.[NOM]</ta>
            <ta e="T228" id="Seg_7661" s="T227">shout-3SG.[NOM]</ta>
            <ta e="T229" id="Seg_7662" s="T228">be.heard-EP-PST2.[3SG]</ta>
            <ta e="T230" id="Seg_7663" s="T229">who.[NOM]</ta>
            <ta e="T231" id="Seg_7664" s="T230">2SG-DAT/LOC</ta>
            <ta e="T232" id="Seg_7665" s="T231">food-ACC</ta>
            <ta e="T233" id="Seg_7666" s="T232">prepare-PRS.[3SG]</ta>
            <ta e="T234" id="Seg_7667" s="T233">house-2SG-ACC</ta>
            <ta e="T235" id="Seg_7668" s="T234">warm-ADVZ</ta>
            <ta e="T236" id="Seg_7669" s="T235">hold-PRS.[3SG]</ta>
            <ta e="T237" id="Seg_7670" s="T236">shoes-PL-2SG-ACC</ta>
            <ta e="T238" id="Seg_7671" s="T237">clothes-2SG-ACC</ta>
            <ta e="T239" id="Seg_7672" s="T238">patch-PRS.[3SG]</ta>
            <ta e="T240" id="Seg_7673" s="T239">that.[NOM]</ta>
            <ta e="T241" id="Seg_7674" s="T240">truth.[NOM]</ta>
            <ta e="T242" id="Seg_7675" s="T241">like.that</ta>
            <ta e="T243" id="Seg_7676" s="T242">be-PRS.[3SG]</ta>
            <ta e="T244" id="Seg_7677" s="T243">very</ta>
            <ta e="T245" id="Seg_7678" s="T244">be.afraid.[CAUS]-NEG.CVB.SIM</ta>
            <ta e="T246" id="Seg_7679" s="T245">Porfirij.[NOM]</ta>
            <ta e="T247" id="Seg_7680" s="T246">speak-CVB.SIM</ta>
            <ta e="T248" id="Seg_7681" s="T247">stand-PST2.[3SG]</ta>
            <ta e="T249" id="Seg_7682" s="T248">1SG.[NOM]</ta>
            <ta e="T250" id="Seg_7683" s="T249">Kycha</ta>
            <ta e="T251" id="Seg_7684" s="T250">daughter-EP-1SG.[NOM]</ta>
            <ta e="T252" id="Seg_7685" s="T251">prepare-CVB.SIM</ta>
            <ta e="T253" id="Seg_7686" s="T252">can-PRS.[3SG]</ta>
            <ta e="T254" id="Seg_7687" s="T253">shoes-ACC</ta>
            <ta e="T255" id="Seg_7688" s="T254">clothes-ACC</ta>
            <ta e="T256" id="Seg_7689" s="T255">make-CVB.SIM</ta>
            <ta e="T257" id="Seg_7690" s="T256">can-PRS.[3SG]</ta>
            <ta e="T258" id="Seg_7691" s="T257">school-DAT/LOC</ta>
            <ta e="T259" id="Seg_7692" s="T258">however</ta>
            <ta e="T260" id="Seg_7693" s="T259">EMPH</ta>
            <ta e="T261" id="Seg_7694" s="T260">that-3SG-3SG-ACC</ta>
            <ta e="T262" id="Seg_7695" s="T261">forget-FUT-3SG</ta>
            <ta e="T263" id="Seg_7696" s="T262">man.[NOM]</ta>
            <ta e="T264" id="Seg_7697" s="T263">human.being.[NOM]</ta>
            <ta e="T265" id="Seg_7698" s="T264">similar</ta>
            <ta e="T266" id="Seg_7699" s="T265">stand.upright-ITER-CVB.SIM</ta>
            <ta e="T267" id="Seg_7700" s="T266">go-FUT-3SG</ta>
            <ta e="T268" id="Seg_7701" s="T267">house-ACC</ta>
            <ta e="T269" id="Seg_7702" s="T268">go.around-CVB.SIM</ta>
            <ta e="T270" id="Seg_7703" s="T269">how</ta>
            <ta e="T271" id="Seg_7704" s="T270">1SG.[NOM]</ta>
            <ta e="T272" id="Seg_7705" s="T271">live-FUT-1SG=Q</ta>
            <ta e="T273" id="Seg_7706" s="T272">child-PL-EP-1SG.[NOM]</ta>
            <ta e="T274" id="Seg_7707" s="T273">go-TEMP-3PL</ta>
            <ta e="T275" id="Seg_7708" s="T274">tribal-3SG.[NOM]</ta>
            <ta e="T276" id="Seg_7709" s="T275">Anisim</ta>
            <ta e="T277" id="Seg_7710" s="T276">Nazarovich</ta>
            <ta e="T278" id="Seg_7711" s="T277">Popov.[NOM]</ta>
            <ta e="T279" id="Seg_7712" s="T278">burst.into.laughter-CVB.SIM</ta>
            <ta e="T280" id="Seg_7713" s="T279">fall-CVB.ANT</ta>
            <ta e="T281" id="Seg_7714" s="T280">old.man-ACC</ta>
            <ta e="T282" id="Seg_7715" s="T281">stop-EP-CAUS-PST2.[3SG]</ta>
            <ta e="T283" id="Seg_7716" s="T282">well</ta>
            <ta e="T284" id="Seg_7717" s="T283">old.man.[NOM]</ta>
            <ta e="T285" id="Seg_7718" s="T284">2SG.[NOM]</ta>
            <ta e="T286" id="Seg_7719" s="T285">get.tired-PST1-2SG</ta>
            <ta e="T287" id="Seg_7720" s="T286">apparently</ta>
            <ta e="T288" id="Seg_7721" s="T287">sit.down-CVB.SIM</ta>
            <ta e="T289" id="Seg_7722" s="T288">fall.[IMP.2SG]</ta>
            <ta e="T290" id="Seg_7723" s="T289">different</ta>
            <ta e="T291" id="Seg_7724" s="T290">people.[NOM]</ta>
            <ta e="T292" id="Seg_7725" s="T291">speak-IMP.3SG</ta>
            <ta e="T293" id="Seg_7726" s="T292">old.man.[NOM]</ta>
            <ta e="T294" id="Seg_7727" s="T293">what.[NOM]</ta>
            <ta e="T295" id="Seg_7728" s="T294">NEG</ta>
            <ta e="T296" id="Seg_7729" s="T295">say-NEG.CVB.SIM</ta>
            <ta e="T297" id="Seg_7730" s="T296">silently</ta>
            <ta e="T298" id="Seg_7731" s="T297">seat-3SG-DAT/LOC</ta>
            <ta e="T299" id="Seg_7732" s="T298">splash</ta>
            <ta e="T300" id="Seg_7733" s="T299">make-CVB.SIM</ta>
            <ta e="T301" id="Seg_7734" s="T300">sit.down-PST2.[3SG]</ta>
            <ta e="T302" id="Seg_7735" s="T301">knee-PL-3SG-ABL</ta>
            <ta e="T303" id="Seg_7736" s="T302">catch-CVB.SIM</ta>
            <ta e="T304" id="Seg_7737" s="T303">fall-CVB.ANT</ta>
            <ta e="T305" id="Seg_7738" s="T304">who.[NOM]</ta>
            <ta e="T306" id="Seg_7739" s="T305">still</ta>
            <ta e="T307" id="Seg_7740" s="T306">what.[NOM]</ta>
            <ta e="T308" id="Seg_7741" s="T307">word-PROPR.[NOM]=Q</ta>
            <ta e="T309" id="Seg_7742" s="T308">say-CVB.SEQ</ta>
            <ta e="T310" id="Seg_7743" s="T309">ask-PST2.[3SG]</ta>
            <ta e="T311" id="Seg_7744" s="T310">tribal-3PL.[NOM]</ta>
            <ta e="T312" id="Seg_7745" s="T311">who-3PL.[NOM]</ta>
            <ta e="T313" id="Seg_7746" s="T312">INDEF</ta>
            <ta e="T314" id="Seg_7747" s="T313">speak-PTCP.PRS-3SG-ACC</ta>
            <ta e="T315" id="Seg_7748" s="T314">wait-CVB.SIM</ta>
            <ta e="T316" id="Seg_7749" s="T315">do.in.vain-CVB.SEQ</ta>
            <ta e="T317" id="Seg_7750" s="T316">count-PST2.[3SG]</ta>
            <ta e="T318" id="Seg_7751" s="T317">what.kind.of.[NOM]</ta>
            <ta e="T319" id="Seg_7752" s="T318">child-3PL.[NOM]</ta>
            <ta e="T320" id="Seg_7753" s="T319">school-DAT/LOC</ta>
            <ta e="T321" id="Seg_7754" s="T320">go-PTCP.PRS-3PL-ACC</ta>
            <ta e="T322" id="Seg_7755" s="T321">who.[NOM]</ta>
            <ta e="T323" id="Seg_7756" s="T322">stay-PTCP.PRS-3SG-ACC</ta>
            <ta e="T324" id="Seg_7757" s="T323">school-DAT/LOC</ta>
            <ta e="T325" id="Seg_7758" s="T324">go-NEG.PTCP-3SG-ACC</ta>
            <ta e="T326" id="Seg_7759" s="T325">age-3PL.[NOM]</ta>
            <ta e="T327" id="Seg_7760" s="T326">big-PL-ACC</ta>
            <ta e="T328" id="Seg_7761" s="T327">that</ta>
            <ta e="T329" id="Seg_7762" s="T328">child-PL-DAT/LOC</ta>
            <ta e="T330" id="Seg_7763" s="T329">get.into-PST2.[3SG]</ta>
            <ta e="T331" id="Seg_7764" s="T330">Porfirij.[NOM]</ta>
            <ta e="T332" id="Seg_7765" s="T331">old.man.[NOM]</ta>
            <ta e="T333" id="Seg_7766" s="T332">son-3SG.[NOM]</ta>
            <ta e="T334" id="Seg_7767" s="T333">Ivan.[NOM]</ta>
            <ta e="T335" id="Seg_7768" s="T334">Kycha-ACC</ta>
            <ta e="T336" id="Seg_7769" s="T335">also</ta>
            <ta e="T337" id="Seg_7770" s="T336">let-PTCP.FUT-DAT/LOC</ta>
            <ta e="T338" id="Seg_7771" s="T337">fall-NEC.[3SG]</ta>
            <ta e="T339" id="Seg_7772" s="T338">3SG.[NOM]</ta>
            <ta e="T340" id="Seg_7773" s="T339">age-3SG.[NOM]</ta>
            <ta e="T341" id="Seg_7774" s="T340">however</ta>
            <ta e="T342" id="Seg_7775" s="T341">EMPH</ta>
            <ta e="T343" id="Seg_7776" s="T342">many</ta>
            <ta e="T344" id="Seg_7777" s="T343">beg-EP-MED-PST2.[3SG]</ta>
            <ta e="T345" id="Seg_7778" s="T344">Porfirij.[NOM]</ta>
            <ta e="T346" id="Seg_7779" s="T345">old.man.[NOM]</ta>
            <ta e="T347" id="Seg_7780" s="T346">no</ta>
            <ta e="T348" id="Seg_7781" s="T347">Kycha.[NOM]</ta>
            <ta e="T349" id="Seg_7782" s="T348">go-FUT.[3SG]</ta>
            <ta e="T350" id="Seg_7783" s="T349">NEG-3SG</ta>
            <ta e="T351" id="Seg_7784" s="T350">school-DAT/LOC</ta>
            <ta e="T352" id="Seg_7785" s="T351">old.man.[NOM]</ta>
            <ta e="T353" id="Seg_7786" s="T352">fear-EP-NEG.[IMP.2SG]</ta>
            <ta e="T354" id="Seg_7787" s="T353">education-PROPR.[NOM]</ta>
            <ta e="T355" id="Seg_7788" s="T354">be-TEMP-3SG</ta>
            <ta e="T356" id="Seg_7789" s="T355">man.[NOM]</ta>
            <ta e="T357" id="Seg_7790" s="T356">human.being.[NOM]</ta>
            <ta e="T358" id="Seg_7791" s="T357">trousers-3SG-ACC</ta>
            <ta e="T359" id="Seg_7792" s="T358">dress-FUT.[3SG]</ta>
            <ta e="T360" id="Seg_7793" s="T359">NEG-3SG</ta>
            <ta e="T361" id="Seg_7794" s="T360">house-ACC</ta>
            <ta e="T362" id="Seg_7795" s="T361">around</ta>
            <ta e="T363" id="Seg_7796" s="T362">stand.upright-ITER-CVB.SIM</ta>
            <ta e="T364" id="Seg_7797" s="T363">go-FUT.[3SG]</ta>
            <ta e="T365" id="Seg_7798" s="T364">NEG-3SG</ta>
            <ta e="T366" id="Seg_7799" s="T365">gathering-DAT/LOC</ta>
            <ta e="T367" id="Seg_7800" s="T366">sit-PTCP.PRS</ta>
            <ta e="T368" id="Seg_7801" s="T367">people.[NOM]</ta>
            <ta e="T369" id="Seg_7802" s="T368">last</ta>
            <ta e="T370" id="Seg_7803" s="T369">soul-3PL-INSTR</ta>
            <ta e="T371" id="Seg_7804" s="T370">laugh-RECP/COLL-EP-PST2-3PL</ta>
            <ta e="T372" id="Seg_7805" s="T371">go-PTCP.PRS</ta>
            <ta e="T373" id="Seg_7806" s="T372">child-PL-ACC</ta>
            <ta e="T376" id="Seg_7807" s="T375">prepare-IMP.2PL</ta>
            <ta e="T377" id="Seg_7808" s="T376">good-ADVZ</ta>
            <ta e="T378" id="Seg_7809" s="T377">dress-CAUS-EP-IMP.2PL</ta>
            <ta e="T379" id="Seg_7810" s="T378">holiday-DAT/LOC</ta>
            <ta e="T380" id="Seg_7811" s="T379">dress-CAUS-PTCP.PRS.[NOM]</ta>
            <ta e="T381" id="Seg_7812" s="T380">similar</ta>
            <ta e="T382" id="Seg_7813" s="T381">child-PL-1PL.[NOM]</ta>
            <ta e="T383" id="Seg_7814" s="T382">self-1SG.[NOM]</ta>
            <ta e="T384" id="Seg_7815" s="T383">take.away-FUT-1SG</ta>
            <ta e="T385" id="Seg_7816" s="T384">Volochanka-ABL</ta>
            <ta e="T386" id="Seg_7817" s="T385">gathering-ACC</ta>
            <ta e="T387" id="Seg_7818" s="T386">stop-EP-CAUS-CVB.SIM</ta>
            <ta e="T388" id="Seg_7819" s="T387">make-CVB.SIM</ta>
            <ta e="T389" id="Seg_7820" s="T388">say-PST2.[3SG]</ta>
            <ta e="T390" id="Seg_7821" s="T389">leader-3PL.[NOM]</ta>
            <ta e="T391" id="Seg_7822" s="T390">gathering.[NOM]</ta>
            <ta e="T392" id="Seg_7823" s="T391">pass.by-CVB.ANT-3SG</ta>
            <ta e="T393" id="Seg_7824" s="T392">gather-MED-NMNZ.[NOM]</ta>
            <ta e="T394" id="Seg_7825" s="T393">noise-3SG.[NOM]</ta>
            <ta e="T395" id="Seg_7826" s="T394">become.more-PST2.[3SG]</ta>
            <ta e="T396" id="Seg_7827" s="T395">woman-PL.[NOM]</ta>
            <ta e="T397" id="Seg_7828" s="T396">bag-3PL-GEN</ta>
            <ta e="T398" id="Seg_7829" s="T397">inside-3SG-ABL</ta>
            <ta e="T399" id="Seg_7830" s="T398">take.out-ITER-PRS-3PL</ta>
            <ta e="T400" id="Seg_7831" s="T399">child-3PL-GEN</ta>
            <ta e="T401" id="Seg_7832" s="T400">broad</ta>
            <ta e="T402" id="Seg_7833" s="T401">broad</ta>
            <ta e="T403" id="Seg_7834" s="T402">clothes-3PL-ACC</ta>
            <ta e="T404" id="Seg_7835" s="T403">holiday-DAT/LOC</ta>
            <ta e="T405" id="Seg_7836" s="T404">dress-PTCP.PRS</ta>
            <ta e="T406" id="Seg_7837" s="T405">shirt-3PL-ACC</ta>
            <ta e="T407" id="Seg_7838" s="T406">some-PL.[NOM]</ta>
            <ta e="T408" id="Seg_7839" s="T407">to.be.on.view-EP-NEG.PTCP</ta>
            <ta e="T409" id="Seg_7840" s="T408">place-DAT/LOC</ta>
            <ta e="T410" id="Seg_7841" s="T409">sew-MULT-PRS-3PL</ta>
            <ta e="T411" id="Seg_7842" s="T410">be-PST2.[3SG]</ta>
            <ta e="T412" id="Seg_7843" s="T411">small</ta>
            <ta e="T413" id="Seg_7844" s="T412">icon-3PL-ACC</ta>
            <ta e="T414" id="Seg_7845" s="T413">cross-PL-ACC</ta>
            <ta e="T415" id="Seg_7846" s="T414">child-PL-3SG-ACC</ta>
            <ta e="T416" id="Seg_7847" s="T415">honour-3PL</ta>
            <ta e="T417" id="Seg_7848" s="T416">every</ta>
            <ta e="T418" id="Seg_7849" s="T417">last</ta>
            <ta e="T419" id="Seg_7850" s="T418">tasty</ta>
            <ta e="T420" id="Seg_7851" s="T419">food-PL-EP-INSTR</ta>
            <ta e="T421" id="Seg_7852" s="T420">minced.meat.with.fat-INSTR</ta>
            <ta e="T422" id="Seg_7853" s="T421">dried.fish-EP-INSTR</ta>
            <ta e="T423" id="Seg_7854" s="T422">reindeer.[NOM]</ta>
            <ta e="T424" id="Seg_7855" s="T423">tongue-EP-INSTR</ta>
            <ta e="T425" id="Seg_7856" s="T424">evening.[NOM]</ta>
            <ta e="T426" id="Seg_7857" s="T425">to</ta>
            <ta e="T429" id="Seg_7858" s="T428">tribal-3SG-DAT/LOC</ta>
            <ta e="T430" id="Seg_7859" s="T429">visit-CVB.SIM</ta>
            <ta e="T431" id="Seg_7860" s="T430">go.in-PST2.[3SG]</ta>
            <ta e="T432" id="Seg_7861" s="T431">Semyon</ta>
            <ta e="T433" id="Seg_7862" s="T432">old.man.[NOM]</ta>
            <ta e="T434" id="Seg_7863" s="T433">why</ta>
            <ta e="T435" id="Seg_7864" s="T434">come-PTCP.PST-3SG-ACC</ta>
            <ta e="T436" id="Seg_7865" s="T435">through</ta>
            <ta e="T437" id="Seg_7866" s="T436">speak-NEG.[3SG]</ta>
            <ta e="T438" id="Seg_7867" s="T437">tea.[NOM]</ta>
            <ta e="T439" id="Seg_7868" s="T438">drink-CVB.SIM</ta>
            <ta e="T440" id="Seg_7869" s="T439">only</ta>
            <ta e="T441" id="Seg_7870" s="T440">sit-CVB.SEQ</ta>
            <ta e="T442" id="Seg_7871" s="T441">say-PST2.[3SG]</ta>
            <ta e="T443" id="Seg_7872" s="T442">long.ago</ta>
            <ta e="T444" id="Seg_7873" s="T443">grandfather-PL-1PL.[NOM]</ta>
            <ta e="T445" id="Seg_7874" s="T444">father-PL-1PL.[NOM]</ta>
            <ta e="T446" id="Seg_7875" s="T445">god-AG.[NOM]</ta>
            <ta e="T447" id="Seg_7876" s="T446">be-PST1-3PL</ta>
            <ta e="T448" id="Seg_7877" s="T447">big</ta>
            <ta e="T449" id="Seg_7878" s="T448">devil-DAT/LOC</ta>
            <ta e="T450" id="Seg_7879" s="T449">always</ta>
            <ta e="T451" id="Seg_7880" s="T450">present-VBZ-PTCP.PRS</ta>
            <ta e="T452" id="Seg_7881" s="T451">custom-PROPR.[NOM]</ta>
            <ta e="T453" id="Seg_7882" s="T452">be-PST1-3PL</ta>
            <ta e="T454" id="Seg_7883" s="T453">every</ta>
            <ta e="T455" id="Seg_7884" s="T454">relative.[NOM]</ta>
            <ta e="T456" id="Seg_7885" s="T455">good-ADVZ</ta>
            <ta e="T457" id="Seg_7886" s="T456">live-PTCP.FUT-3PL-ACC</ta>
            <ta e="T458" id="Seg_7887" s="T457">want-PRS-3PL</ta>
            <ta e="T459" id="Seg_7888" s="T458">what-EP-INSTR</ta>
            <ta e="T460" id="Seg_7889" s="T459">NEG</ta>
            <ta e="T461" id="Seg_7890" s="T460">require-NEG.CVB.SIM</ta>
            <ta e="T462" id="Seg_7891" s="T461">be.sick-EP-NEG.CVB.SIM</ta>
            <ta e="T463" id="Seg_7892" s="T462">big</ta>
            <ta e="T464" id="Seg_7893" s="T463">shaman-PL.[NOM]</ta>
            <ta e="T465" id="Seg_7894" s="T464">human.being.[NOM]</ta>
            <ta e="T466" id="Seg_7895" s="T465">go-PTCP.PRS</ta>
            <ta e="T467" id="Seg_7896" s="T466">way-3SG-ACC</ta>
            <ta e="T468" id="Seg_7897" s="T467">know-PTCP.PRS-3PL.[NOM]</ta>
            <ta e="T469" id="Seg_7898" s="T468">very</ta>
            <ta e="T470" id="Seg_7899" s="T469">be-PST1-3SG</ta>
            <ta e="T471" id="Seg_7900" s="T470">this</ta>
            <ta e="T472" id="Seg_7901" s="T471">place.of.living-1PL-ABL</ta>
            <ta e="T473" id="Seg_7902" s="T472">distant-POSS</ta>
            <ta e="T474" id="Seg_7903" s="T473">NEG</ta>
            <ta e="T475" id="Seg_7904" s="T474">there.is</ta>
            <ta e="T476" id="Seg_7905" s="T475">big</ta>
            <ta e="T477" id="Seg_7906" s="T476">devil-stone.[NOM]</ta>
            <ta e="T478" id="Seg_7907" s="T477">every</ta>
            <ta e="T479" id="Seg_7908" s="T478">human.being.[NOM]</ta>
            <ta e="T480" id="Seg_7909" s="T479">there</ta>
            <ta e="T481" id="Seg_7910" s="T480">present-ACC</ta>
            <ta e="T482" id="Seg_7911" s="T481">give-HAB.[3SG]</ta>
            <ta e="T483" id="Seg_7912" s="T482">soon</ta>
            <ta e="T484" id="Seg_7913" s="T483">child-PL-1PL.[NOM]</ta>
            <ta e="T485" id="Seg_7914" s="T484">foreign</ta>
            <ta e="T486" id="Seg_7915" s="T485">place-DAT/LOC</ta>
            <ta e="T487" id="Seg_7916" s="T486">go-PRS-3PL</ta>
            <ta e="T488" id="Seg_7917" s="T487">foreign</ta>
            <ta e="T489" id="Seg_7918" s="T488">human.being-PL-DAT/LOC</ta>
            <ta e="T490" id="Seg_7919" s="T489">tribal-3SG.[NOM]</ta>
            <ta e="T491" id="Seg_7920" s="T490">guest-3SG-GEN</ta>
            <ta e="T492" id="Seg_7921" s="T491">story-3SG-ACC</ta>
            <ta e="T493" id="Seg_7922" s="T492">listen-CVB.SIM-listen-CVB.SIM</ta>
            <ta e="T494" id="Seg_7923" s="T493">inside-3SG-DAT/LOC</ta>
            <ta e="T495" id="Seg_7924" s="T494">say-CVB.SIM</ta>
            <ta e="T496" id="Seg_7925" s="T495">think-PRS.[3SG]</ta>
            <ta e="T497" id="Seg_7926" s="T496">what</ta>
            <ta e="T498" id="Seg_7927" s="T497">thought-3SG-INSTR</ta>
            <ta e="T499" id="Seg_7928" s="T498">such</ta>
            <ta e="T500" id="Seg_7929" s="T499">story-ACC</ta>
            <ta e="T501" id="Seg_7930" s="T500">take.out-PRS.[3SG]</ta>
            <ta e="T502" id="Seg_7931" s="T501">guest-3SG.[NOM]</ta>
            <ta e="T503" id="Seg_7932" s="T502">hurry-NEG.CVB.SIM</ta>
            <ta e="T504" id="Seg_7933" s="T503">speak-ITER-CVB.SEQ</ta>
            <ta e="T505" id="Seg_7934" s="T504">go-PST2.[3SG]</ta>
            <ta e="T506" id="Seg_7935" s="T505">woman-PL.[NOM]</ta>
            <ta e="T507" id="Seg_7936" s="T506">misery-PROPR-PL</ta>
            <ta e="T508" id="Seg_7937" s="T507">very</ta>
            <ta e="T509" id="Seg_7938" s="T508">grieve-PST1-3PL</ta>
            <ta e="T510" id="Seg_7939" s="T509">cry-EP-RECP/COLL-PRS-3PL</ta>
            <ta e="T511" id="Seg_7940" s="T510">small</ta>
            <ta e="T512" id="Seg_7941" s="T511">child-ACC</ta>
            <ta e="T513" id="Seg_7942" s="T512">tent-ABL</ta>
            <ta e="T514" id="Seg_7943" s="T513">take.away-PTCP.PRS</ta>
            <ta e="T515" id="Seg_7944" s="T514">matter.[NOM]</ta>
            <ta e="T516" id="Seg_7945" s="T515">very</ta>
            <ta e="T517" id="Seg_7946" s="T516">sin-PROPR.[NOM]</ta>
            <ta e="T518" id="Seg_7947" s="T517">people.[NOM]</ta>
            <ta e="T519" id="Seg_7948" s="T518">very</ta>
            <ta e="T520" id="Seg_7949" s="T519">be.sad-PRS.[3SG]</ta>
            <ta e="T521" id="Seg_7950" s="T520">probably</ta>
            <ta e="T522" id="Seg_7951" s="T521">devil-DAT/LOC</ta>
            <ta e="T523" id="Seg_7952" s="T522">present.[NOM]</ta>
            <ta e="T524" id="Seg_7953" s="T523">give-PTCP.FUT-DAT/LOC</ta>
            <ta e="T525" id="Seg_7954" s="T524">well</ta>
            <ta e="T526" id="Seg_7955" s="T525">old.man.[NOM]</ta>
            <ta e="T527" id="Seg_7956" s="T526">1SG-ACC</ta>
            <ta e="T528" id="Seg_7957" s="T527">spit-NEG.[IMP.2SG]</ta>
            <ta e="T529" id="Seg_7958" s="T528">that-ACC</ta>
            <ta e="T530" id="Seg_7959" s="T529">make-FUT-1PL</ta>
            <ta e="T531" id="Seg_7960" s="T530">NEG-3SG</ta>
            <ta e="T532" id="Seg_7961" s="T531">school.[NOM]</ta>
            <ta e="T533" id="Seg_7962" s="T532">bad.spirit-ACC</ta>
            <ta e="T534" id="Seg_7963" s="T533">beg-NEG.[3SG]</ta>
            <ta e="T535" id="Seg_7964" s="T534">like.that</ta>
            <ta e="T536" id="Seg_7965" s="T535">every</ta>
            <ta e="T537" id="Seg_7966" s="T536">human.being-DAT/LOC</ta>
            <ta e="T538" id="Seg_7967" s="T537">tell.[IMP.2SG]</ta>
            <ta e="T539" id="Seg_7968" s="T538">be.sad-NEG-IMP.3PL</ta>
            <ta e="T540" id="Seg_7969" s="T539">say-PST2.[3SG]</ta>
            <ta e="T541" id="Seg_7970" s="T540">tribal-3SG.[NOM]</ta>
            <ta e="T542" id="Seg_7971" s="T541">old.man.[NOM]</ta>
            <ta e="T543" id="Seg_7972" s="T542">what.[NOM]</ta>
            <ta e="T544" id="Seg_7973" s="T543">NEG</ta>
            <ta e="T545" id="Seg_7974" s="T544">say-NEG.CVB.SIM</ta>
            <ta e="T546" id="Seg_7975" s="T545">cup-3SG-DAT/LOC</ta>
            <ta e="T547" id="Seg_7976" s="T546">tea.[NOM]</ta>
            <ta e="T548" id="Seg_7977" s="T547">pour-EP-MED-PST2.[3SG]</ta>
            <ta e="T549" id="Seg_7978" s="T548">1PL.[NOM]</ta>
            <ta e="T550" id="Seg_7979" s="T549">old.man-PL.[NOM]</ta>
            <ta e="T551" id="Seg_7980" s="T550">how</ta>
            <ta e="T552" id="Seg_7981" s="T551">good.[NOM]</ta>
            <ta e="T553" id="Seg_7982" s="T552">be-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T554" id="Seg_7983" s="T553">think-CVB.SEQ</ta>
            <ta e="T555" id="Seg_7984" s="T554">advise.[NOM]</ta>
            <ta e="T556" id="Seg_7985" s="T555">give-PRS-1PL</ta>
            <ta e="T557" id="Seg_7986" s="T556">truth.[NOM]</ta>
            <ta e="T558" id="Seg_7987" s="T557">some</ta>
            <ta e="T559" id="Seg_7988" s="T558">after</ta>
            <ta e="T560" id="Seg_7989" s="T559">mistake.[NOM]</ta>
            <ta e="T561" id="Seg_7990" s="T560">speak-PRS-1PL</ta>
            <ta e="T562" id="Seg_7991" s="T561">be-FUT-3SG</ta>
            <ta e="T563" id="Seg_7992" s="T562">woman-PL.[NOM]</ta>
            <ta e="T564" id="Seg_7993" s="T563">cry-EP-MED-PTCP.PRS-3PL.[3SG]</ta>
            <ta e="T565" id="Seg_7994" s="T564">powerful.[NOM]</ta>
            <ta e="T566" id="Seg_7995" s="T565">human.being.[NOM]</ta>
            <ta e="T567" id="Seg_7996" s="T566">education-3SG-ACC</ta>
            <ta e="T569" id="Seg_7997" s="T568">human.being.[NOM]</ta>
            <ta e="T570" id="Seg_7998" s="T569">education-3SG-ACC</ta>
            <ta e="T571" id="Seg_7999" s="T570">care.about-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T572" id="Seg_8000" s="T571">very</ta>
            <ta e="T573" id="Seg_8001" s="T572">very</ta>
            <ta e="T574" id="Seg_8002" s="T573">very</ta>
            <ta e="T575" id="Seg_8003" s="T574">small</ta>
            <ta e="T576" id="Seg_8004" s="T575">child-PL-DAT/LOC</ta>
            <ta e="T577" id="Seg_8005" s="T576">big</ta>
            <ta e="T578" id="Seg_8006" s="T577">people-ACC</ta>
            <ta e="T579" id="Seg_8007" s="T578">spread-PTCP.FUT-DAT/LOC</ta>
            <ta e="T580" id="Seg_8008" s="T579">straight</ta>
            <ta e="T581" id="Seg_8009" s="T580">see-PTCP.PRS</ta>
            <ta e="T582" id="Seg_8010" s="T581">be-PTCP.FUT-3PL-ACC</ta>
            <ta e="T583" id="Seg_8011" s="T582">very</ta>
            <ta e="T584" id="Seg_8012" s="T583">small</ta>
            <ta e="T585" id="Seg_8013" s="T584">child.[NOM]</ta>
            <ta e="T586" id="Seg_8014" s="T585">reindeer.calf.[NOM]</ta>
            <ta e="T587" id="Seg_8015" s="T586">similar</ta>
            <ta e="T588" id="Seg_8016" s="T587">where</ta>
            <ta e="T589" id="Seg_8017" s="T588">go-PTCP.PRS-3SG-ACC</ta>
            <ta e="T590" id="Seg_8018" s="T589">know-NEG.[3SG]</ta>
            <ta e="T591" id="Seg_8019" s="T590">good-ADVZ</ta>
            <ta e="T592" id="Seg_8020" s="T591">dress-CVB.SIM</ta>
            <ta e="T593" id="Seg_8021" s="T592">can-NEG.[3SG]</ta>
            <ta e="T594" id="Seg_8022" s="T593">that.EMPH.[NOM]</ta>
            <ta e="T595" id="Seg_8023" s="T594">truth.[NOM]</ta>
            <ta e="T596" id="Seg_8024" s="T595">say-PST2.[3SG]</ta>
            <ta e="T597" id="Seg_8025" s="T596">tribal-3SG.[NOM]</ta>
            <ta e="T598" id="Seg_8026" s="T597">old.man.[NOM]</ta>
            <ta e="T599" id="Seg_8027" s="T598">this</ta>
            <ta e="T600" id="Seg_8028" s="T599">gathering.[NOM]</ta>
            <ta e="T601" id="Seg_8029" s="T600">noise-3SG-DAT/LOC</ta>
            <ta e="T602" id="Seg_8030" s="T601">forget-CVB.SEQ</ta>
            <ta e="T603" id="Seg_8031" s="T602">throw-PST2.[3SG]</ta>
            <ta e="T604" id="Seg_8032" s="T603">in.winter</ta>
            <ta e="T605" id="Seg_8033" s="T604">new</ta>
            <ta e="T606" id="Seg_8034" s="T605">year.[NOM]</ta>
            <ta e="T607" id="Seg_8035" s="T606">come-PTCP.PRS</ta>
            <ta e="T608" id="Seg_8036" s="T607">time-3SG-DAT/LOC</ta>
            <ta e="T609" id="Seg_8037" s="T608">child-PL.[NOM]</ta>
            <ta e="T610" id="Seg_8038" s="T609">relax-CVB.SIM</ta>
            <ta e="T611" id="Seg_8039" s="T610">come-FUT-3PL</ta>
            <ta e="T612" id="Seg_8040" s="T611">house-3PL-DAT/LOC</ta>
            <ta e="T613" id="Seg_8041" s="T612">spring.[NOM]</ta>
            <ta e="T614" id="Seg_8042" s="T613">May</ta>
            <ta e="T615" id="Seg_8043" s="T614">month-3SG-DAT/LOC</ta>
            <ta e="T616" id="Seg_8044" s="T615">one</ta>
            <ta e="T617" id="Seg_8045" s="T616">year-ADJZ</ta>
            <ta e="T618" id="Seg_8046" s="T617">education-3PL-ACC</ta>
            <ta e="T619" id="Seg_8047" s="T618">stop-TEMP-3PL</ta>
            <ta e="T620" id="Seg_8048" s="T619">autumn-DAT/LOC</ta>
            <ta e="T621" id="Seg_8049" s="T620">until</ta>
            <ta e="T622" id="Seg_8050" s="T621">house-3PL-DAT/LOC</ta>
            <ta e="T623" id="Seg_8051" s="T622">be-FUT-3PL</ta>
            <ta e="T624" id="Seg_8052" s="T623">that</ta>
            <ta e="T625" id="Seg_8053" s="T624">news-ACC</ta>
            <ta e="T626" id="Seg_8054" s="T625">1SG.[NOM]</ta>
            <ta e="T627" id="Seg_8055" s="T626">every</ta>
            <ta e="T628" id="Seg_8056" s="T627">human.being-DAT/LOC</ta>
            <ta e="T629" id="Seg_8057" s="T628">tell-FUT-1SG</ta>
            <ta e="T630" id="Seg_8058" s="T629">human.being-PL.[NOM]</ta>
            <ta e="T631" id="Seg_8059" s="T630">say-CVB.SIM</ta>
            <ta e="T632" id="Seg_8060" s="T631">think-PRS-3PL</ta>
            <ta e="T633" id="Seg_8061" s="T632">when</ta>
            <ta e="T634" id="Seg_8062" s="T633">NEG</ta>
            <ta e="T635" id="Seg_8063" s="T634">come.back-EP-CAUS-NEG.CVB.SIM</ta>
            <ta e="T636" id="Seg_8064" s="T635">take-PRS-3PL</ta>
            <ta e="T637" id="Seg_8065" s="T636">child-PL-ACC</ta>
            <ta e="T638" id="Seg_8066" s="T637">be.happy-CVB.COND</ta>
            <ta e="T639" id="Seg_8067" s="T638">old.man.[NOM]</ta>
            <ta e="T640" id="Seg_8068" s="T639">speak-PST2.[3SG]</ta>
            <ta e="T641" id="Seg_8069" s="T640">old.man.[NOM]</ta>
            <ta e="T642" id="Seg_8070" s="T641">house-3SG-DAT/LOC</ta>
            <ta e="T643" id="Seg_8071" s="T642">go-PTCP.PST-3SG-ACC</ta>
            <ta e="T644" id="Seg_8072" s="T643">after</ta>
            <ta e="T645" id="Seg_8073" s="T644">Anisim.[NOM]</ta>
            <ta e="T646" id="Seg_8074" s="T645">self-3SG-ACC</ta>
            <ta e="T647" id="Seg_8075" s="T646">with</ta>
            <ta e="T648" id="Seg_8076" s="T647">chat-PRS.[3SG]</ta>
            <ta e="T649" id="Seg_8077" s="T648">every</ta>
            <ta e="T650" id="Seg_8078" s="T649">human.being-DAT/LOC</ta>
            <ta e="T651" id="Seg_8079" s="T650">remember-EP-CAUS-CVB.SEQ</ta>
            <ta e="T652" id="Seg_8080" s="T651">go-PTCP.FUT-DAT/LOC</ta>
            <ta e="T653" id="Seg_8081" s="T652">need.to</ta>
            <ta e="T654" id="Seg_8082" s="T653">how</ta>
            <ta e="T655" id="Seg_8083" s="T654">new</ta>
            <ta e="T656" id="Seg_8084" s="T655">law.[NOM]</ta>
            <ta e="T657" id="Seg_8085" s="T656">good-ACC</ta>
            <ta e="T658" id="Seg_8086" s="T657">make-PTCP.PRS-3SG-ACC</ta>
            <ta e="T659" id="Seg_8087" s="T658">tundra.[NOM]</ta>
            <ta e="T660" id="Seg_8088" s="T659">people-3SG-GEN</ta>
            <ta e="T661" id="Seg_8089" s="T660">front-3SG-DAT/LOC</ta>
            <ta e="T662" id="Seg_8090" s="T661">such</ta>
            <ta e="T663" id="Seg_8091" s="T662">old.man-PL.[NOM]</ta>
            <ta e="T664" id="Seg_8092" s="T663">news.[NOM]</ta>
            <ta e="T665" id="Seg_8093" s="T664">send-PTCP.PRS-3PL</ta>
            <ta e="T666" id="Seg_8094" s="T665">truth-PROPR.[NOM]</ta>
            <ta e="T667" id="Seg_8095" s="T666">powerful-PROPR.[NOM]</ta>
            <ta e="T668" id="Seg_8096" s="T667">be-FUT-3SG</ta>
            <ta e="T669" id="Seg_8097" s="T668">next</ta>
            <ta e="T670" id="Seg_8098" s="T669">day.[NOM]</ta>
            <ta e="T671" id="Seg_8099" s="T670">again</ta>
            <ta e="T672" id="Seg_8100" s="T671">gathering-DAT/LOC</ta>
            <ta e="T673" id="Seg_8101" s="T672">human.being-PL.[NOM]</ta>
            <ta e="T674" id="Seg_8102" s="T673">gather-PASS/REFL-EP-PST2-3PL</ta>
            <ta e="T676" id="Seg_8103" s="T675">Semyon</ta>
            <ta e="T677" id="Seg_8104" s="T676">old.man.[NOM]</ta>
            <ta e="T678" id="Seg_8105" s="T677">tent-3SG-DAT/LOC</ta>
            <ta e="T679" id="Seg_8106" s="T678">people.[NOM]</ta>
            <ta e="T680" id="Seg_8107" s="T679">yesterday-ADJZ-COMP</ta>
            <ta e="T681" id="Seg_8108" s="T680">how</ta>
            <ta e="T682" id="Seg_8109" s="T681">INDEF</ta>
            <ta e="T683" id="Seg_8110" s="T682">mood-3PL.[NOM]</ta>
            <ta e="T684" id="Seg_8111" s="T683">raise-EP-PASS/REFL-EP-PST2.[3SG]</ta>
            <ta e="T685" id="Seg_8112" s="T684">noise.[NOM]</ta>
            <ta e="T686" id="Seg_8113" s="T685">laugh-RECP/COLL-NMNZ.[NOM]-laugh-RECP/COLL-NMNZ.[NOM]</ta>
            <ta e="T687" id="Seg_8114" s="T686">stop-CVB.ANT-3SG</ta>
            <ta e="T688" id="Seg_8115" s="T687">tribal-3PL.[NOM]</ta>
            <ta e="T689" id="Seg_8116" s="T688">say-PST2.[3SG]</ta>
            <ta e="T690" id="Seg_8117" s="T689">self-2PL.[NOM]</ta>
            <ta e="T691" id="Seg_8118" s="T690">wish-2PL-ABL</ta>
            <ta e="T692" id="Seg_8119" s="T691">small</ta>
            <ta e="T693" id="Seg_8120" s="T692">people-PL-ACC</ta>
            <ta e="T694" id="Seg_8121" s="T693">hold-CAUS-EP-IMP.2PL</ta>
            <ta e="T695" id="Seg_8122" s="T694">big</ta>
            <ta e="T696" id="Seg_8123" s="T695">child-PL-DAT/LOC</ta>
            <ta e="T697" id="Seg_8124" s="T696">start-PTCP.PRS</ta>
            <ta e="T698" id="Seg_8125" s="T697">time-DAT/LOC</ta>
            <ta e="T699" id="Seg_8126" s="T698">see-PTCP.PRS</ta>
            <ta e="T700" id="Seg_8127" s="T699">be-PTCP.FUT-3PL-ACC</ta>
            <ta e="T701" id="Seg_8128" s="T700">3PL-ACC</ta>
            <ta e="T702" id="Seg_8129" s="T701">1PL-DAT/LOC</ta>
            <ta e="T703" id="Seg_8130" s="T702">say-PST2-3PL</ta>
            <ta e="T704" id="Seg_8131" s="T703">child-PL-1PL.[NOM]</ta>
            <ta e="T705" id="Seg_8132" s="T704">in.winter</ta>
            <ta e="T706" id="Seg_8133" s="T705">relax-CVB.SIM</ta>
            <ta e="T707" id="Seg_8134" s="T706">come-FUT-3PL</ta>
            <ta e="T708" id="Seg_8135" s="T707">that.[NOM]</ta>
            <ta e="T709" id="Seg_8136" s="T708">truth.[NOM]</ta>
            <ta e="T710" id="Seg_8137" s="T709">word.[NOM]</ta>
            <ta e="T711" id="Seg_8138" s="T710">Q</ta>
            <ta e="T712" id="Seg_8139" s="T711">ask-PST2.[3SG]</ta>
            <ta e="T713" id="Seg_8140" s="T712">one</ta>
            <ta e="T714" id="Seg_8141" s="T713">woman.[NOM]</ta>
            <ta e="T715" id="Seg_8142" s="T714">Semyon</ta>
            <ta e="T716" id="Seg_8143" s="T715">old.man.[NOM]</ta>
            <ta e="T717" id="Seg_8144" s="T716">sit-PTCP.PRS</ta>
            <ta e="T718" id="Seg_8145" s="T717">place-3SG-ABL</ta>
            <ta e="T719" id="Seg_8146" s="T718">consider-EP</ta>
            <ta e="T720" id="Seg_8147" s="T719">consider-EP-ITER-PST2.[3SG]</ta>
            <ta e="T721" id="Seg_8148" s="T720">that.[NOM]</ta>
            <ta e="T722" id="Seg_8149" s="T721">truth.[NOM]</ta>
            <ta e="T723" id="Seg_8150" s="T722">news.[NOM]</ta>
            <ta e="T724" id="Seg_8151" s="T723">in.winter</ta>
            <ta e="T725" id="Seg_8152" s="T724">relax-CVB.SIM</ta>
            <ta e="T726" id="Seg_8153" s="T725">come-FUT-3PL</ta>
            <ta e="T727" id="Seg_8154" s="T726">then</ta>
            <ta e="T728" id="Seg_8155" s="T727">in.summer</ta>
            <ta e="T729" id="Seg_8156" s="T728">straight</ta>
            <ta e="T730" id="Seg_8157" s="T729">house-3PL-DAT/LOC</ta>
            <ta e="T731" id="Seg_8158" s="T730">be-CVB.SIM</ta>
            <ta e="T732" id="Seg_8159" s="T731">come-FUT-3PL</ta>
            <ta e="T733" id="Seg_8160" s="T732">say-PST2.[3SG]</ta>
            <ta e="T734" id="Seg_8161" s="T733">Anisim.[NOM]</ta>
            <ta e="T735" id="Seg_8162" s="T734">Semyon</ta>
            <ta e="T736" id="Seg_8163" s="T735">old.man.[NOM]</ta>
            <ta e="T737" id="Seg_8164" s="T736">relax-CVB.SIM</ta>
            <ta e="T738" id="Seg_8165" s="T737">fall-CVB.ANT</ta>
            <ta e="T739" id="Seg_8166" s="T738">word.[NOM]</ta>
            <ta e="T742" id="Seg_8167" s="T741">occur-EP-PST2.[3SG]</ta>
            <ta e="T743" id="Seg_8168" s="T742">1SG.[NOM]</ta>
            <ta e="T744" id="Seg_8169" s="T743">what-ACC</ta>
            <ta e="T745" id="Seg_8170" s="T744">say-PST2-EP-1SG=Q</ta>
            <ta e="T746" id="Seg_8171" s="T745">every-3PL.[NOM]</ta>
            <ta e="T747" id="Seg_8172" s="T746">warm</ta>
            <ta e="T748" id="Seg_8173" s="T747">eye-EP-INSTR</ta>
            <ta e="T749" id="Seg_8174" s="T748">see-CVB.SEQ</ta>
            <ta e="T750" id="Seg_8175" s="T749">Semyon</ta>
            <ta e="T751" id="Seg_8176" s="T750">old.man.[NOM]</ta>
            <ta e="T752" id="Seg_8177" s="T751">to</ta>
            <ta e="T753" id="Seg_8178" s="T752">turn-PASS/REFL-EP-PST2-3PL</ta>
            <ta e="T754" id="Seg_8179" s="T753">gathering-3PL.[NOM]</ta>
            <ta e="T755" id="Seg_8180" s="T754">pass.by-PST2.[3SG]</ta>
            <ta e="T756" id="Seg_8181" s="T755">fast-ADVZ</ta>
            <ta e="T757" id="Seg_8182" s="T756">human.being-PL.[NOM]</ta>
            <ta e="T758" id="Seg_8183" s="T757">pole.[NOM]</ta>
            <ta e="T759" id="Seg_8184" s="T758">tent-3PL-ACC</ta>
            <ta e="T760" id="Seg_8185" s="T759">every</ta>
            <ta e="T761" id="Seg_8186" s="T760">go-MULT-PST2-3PL</ta>
            <ta e="T762" id="Seg_8187" s="T761">next</ta>
            <ta e="T763" id="Seg_8188" s="T762">autumn.[NOM]</ta>
            <ta e="T764" id="Seg_8189" s="T763">herd-3PL-ACC</ta>
            <ta e="T765" id="Seg_8190" s="T764">%%-CVB.SEQ</ta>
            <ta e="T766" id="Seg_8191" s="T765">after</ta>
            <ta e="T767" id="Seg_8192" s="T766">reindeer.[NOM]</ta>
            <ta e="T768" id="Seg_8193" s="T767">grab-NMNZ-3SG.[NOM]</ta>
            <ta e="T769" id="Seg_8194" s="T768">become-PST2.[3SG]</ta>
            <ta e="T770" id="Seg_8195" s="T769">start-PTCP.PRS-DAT/LOC</ta>
            <ta e="T771" id="Seg_8196" s="T770">woman-PL.[NOM]</ta>
            <ta e="T772" id="Seg_8197" s="T771">tent-3PL-GEN</ta>
            <ta e="T773" id="Seg_8198" s="T772">place.beneath-DAT/LOC</ta>
            <ta e="T774" id="Seg_8199" s="T773">child-PL-3SG-ACC</ta>
            <ta e="T775" id="Seg_8200" s="T774">with</ta>
            <ta e="T776" id="Seg_8201" s="T775">free.time-POSS</ta>
            <ta e="T777" id="Seg_8202" s="T776">NEG-3PL</ta>
            <ta e="T778" id="Seg_8203" s="T777">gather-REFL-CVB.SEQ-3PL</ta>
            <ta e="T779" id="Seg_8204" s="T778">child-PL-3SG-DAT/LOC</ta>
            <ta e="T780" id="Seg_8205" s="T779">straight</ta>
            <ta e="T781" id="Seg_8206" s="T780">speak-PRS-3PL</ta>
            <ta e="T782" id="Seg_8207" s="T781">move-NEG.PTCP</ta>
            <ta e="T783" id="Seg_8208" s="T782">be-PTCP.FUT-3PL-ACC</ta>
            <ta e="T784" id="Seg_8209" s="T783">big</ta>
            <ta e="T785" id="Seg_8210" s="T784">child-PL-ACC</ta>
            <ta e="T786" id="Seg_8211" s="T785">hear-PTCP.PRS</ta>
            <ta e="T787" id="Seg_8212" s="T786">be-PTCP.FUT-3PL-ACC</ta>
            <ta e="T788" id="Seg_8213" s="T787">reindeer.[NOM]</ta>
            <ta e="T789" id="Seg_8214" s="T788">hold-CVB.ANT</ta>
            <ta e="T790" id="Seg_8215" s="T789">harness-CVB.ANT</ta>
            <ta e="T791" id="Seg_8216" s="T790">pole.[NOM]</ta>
            <ta e="T792" id="Seg_8217" s="T791">tent.[NOM]</ta>
            <ta e="T793" id="Seg_8218" s="T792">every</ta>
            <ta e="T794" id="Seg_8219" s="T793">eat-PRS-3PL</ta>
            <ta e="T795" id="Seg_8220" s="T794">start-PTCP.PRS</ta>
            <ta e="T796" id="Seg_8221" s="T795">front-3SG-DAT/LOC</ta>
            <ta e="T797" id="Seg_8222" s="T796">then</ta>
            <ta e="T798" id="Seg_8223" s="T797">pole.[NOM]</ta>
            <ta e="T799" id="Seg_8224" s="T798">tent.[NOM]</ta>
            <ta e="T800" id="Seg_8225" s="T799">every-ABL</ta>
            <ta e="T801" id="Seg_8226" s="T800">every-3PL.[NOM]</ta>
            <ta e="T802" id="Seg_8227" s="T801">flame.up-RECP/COLL-CVB.SEQ</ta>
            <ta e="T803" id="Seg_8228" s="T802">go.out-EP-PST2-3PL</ta>
            <ta e="T804" id="Seg_8229" s="T803">distant.[NOM]</ta>
            <ta e="T805" id="Seg_8230" s="T804">go-PTCP.PRS</ta>
            <ta e="T806" id="Seg_8231" s="T805">child-PL-ACC</ta>
            <ta e="T807" id="Seg_8232" s="T806">accompany-CVB.PURP</ta>
            <ta e="T808" id="Seg_8233" s="T807">when</ta>
            <ta e="T809" id="Seg_8234" s="T808">1SG.[NOM]</ta>
            <ta e="T810" id="Seg_8235" s="T809">2SG-ACC</ta>
            <ta e="T811" id="Seg_8236" s="T810">see-FUT-1SG=Q</ta>
            <ta e="T812" id="Seg_8237" s="T811">Evgeniya.[NOM]</ta>
            <ta e="T813" id="Seg_8238" s="T812">cry-PST2.[3SG]</ta>
            <ta e="T814" id="Seg_8239" s="T813">how</ta>
            <ta e="T815" id="Seg_8240" s="T814">2SG-ACC</ta>
            <ta e="T818" id="Seg_8241" s="T817">2SG-ACC-POSS</ta>
            <ta e="T819" id="Seg_8242" s="T818">NEG</ta>
            <ta e="T820" id="Seg_8243" s="T819">live-FUT-1SG=Q</ta>
            <ta e="T821" id="Seg_8244" s="T820">who.[NOM]</ta>
            <ta e="T822" id="Seg_8245" s="T821">1PL-DAT/LOC</ta>
            <ta e="T823" id="Seg_8246" s="T822">help-FUT-3SG=Q</ta>
            <ta e="T824" id="Seg_8247" s="T823">child-3SG-ACC</ta>
            <ta e="T825" id="Seg_8248" s="T824">strongly</ta>
            <ta e="T826" id="Seg_8249" s="T825">grab-CVB.ANT</ta>
            <ta e="T827" id="Seg_8250" s="T826">after</ta>
            <ta e="T828" id="Seg_8251" s="T827">tent-3SG-ACC</ta>
            <ta e="T829" id="Seg_8252" s="T828">to</ta>
            <ta e="T830" id="Seg_8253" s="T829">pull-PRS.[3SG]</ta>
            <ta e="T831" id="Seg_8254" s="T830">2SG.[NOM]</ta>
            <ta e="T832" id="Seg_8255" s="T831">what.[NOM]</ta>
            <ta e="T833" id="Seg_8256" s="T832">become-PRS-2SG</ta>
            <ta e="T834" id="Seg_8257" s="T833">one</ta>
            <ta e="T835" id="Seg_8258" s="T834">one</ta>
            <ta e="T836" id="Seg_8259" s="T835">woman.[NOM]</ta>
            <ta e="T837" id="Seg_8260" s="T836">say-PST2.[3SG]</ta>
            <ta e="T838" id="Seg_8261" s="T837">friend-3SG-DAT/LOC</ta>
            <ta e="T839" id="Seg_8262" s="T838">child-ACC</ta>
            <ta e="T840" id="Seg_8263" s="T839">think-PTCP.PRS-DAT/LOC</ta>
            <ta e="T841" id="Seg_8264" s="T840">pull.down-EP-NEG.[IMP.2SG]</ta>
            <ta e="T842" id="Seg_8265" s="T841">start-PTCP.PRS.[NOM]</ta>
            <ta e="T843" id="Seg_8266" s="T842">front-3SG-DAT/LOC</ta>
            <ta e="T844" id="Seg_8267" s="T843">such</ta>
            <ta e="T845" id="Seg_8268" s="T844">accompany-PTCP.PRS.[NOM]</ta>
            <ta e="T846" id="Seg_8269" s="T845">very</ta>
            <ta e="T847" id="Seg_8270" s="T846">bad.[NOM]</ta>
            <ta e="T848" id="Seg_8271" s="T847">sin.[NOM]</ta>
            <ta e="T849" id="Seg_8272" s="T848">accompany.[IMP.2SG]</ta>
            <ta e="T850" id="Seg_8273" s="T849">human.being-SIM</ta>
            <ta e="T851" id="Seg_8274" s="T850">every</ta>
            <ta e="T852" id="Seg_8275" s="T851">riding.reindeer-PROPR</ta>
            <ta e="T853" id="Seg_8276" s="T852">child-PL.[NOM]</ta>
            <ta e="T854" id="Seg_8277" s="T853">grandfather-3PL-GEN</ta>
            <ta e="T855" id="Seg_8278" s="T854">back-3SG-ABL</ta>
            <ta e="T856" id="Seg_8279" s="T855">go-CVB.SEQ</ta>
            <ta e="T857" id="Seg_8280" s="T856">long</ta>
            <ta e="T858" id="Seg_8281" s="T857">reindeer.caravan.[NOM]</ta>
            <ta e="T859" id="Seg_8282" s="T858">become-PST2-3PL</ta>
            <ta e="T860" id="Seg_8283" s="T859">stand-NEG.PTCP-stand-PTCP.PRS</ta>
            <ta e="T861" id="Seg_8284" s="T860">child-3PL-ACC</ta>
            <ta e="T862" id="Seg_8285" s="T861">accompany-CVB.SEQ</ta>
            <ta e="T863" id="Seg_8286" s="T862">that</ta>
            <ta e="T864" id="Seg_8287" s="T863">reindeer.caravan.[NOM]</ta>
            <ta e="T865" id="Seg_8288" s="T864">back-3SG-ABL</ta>
            <ta e="T866" id="Seg_8289" s="T865">stretch.out-PST2-3PL</ta>
            <ta e="T867" id="Seg_8290" s="T866">Porfirij</ta>
            <ta e="T868" id="Seg_8291" s="T867">old.man.[NOM]</ta>
            <ta e="T869" id="Seg_8292" s="T868">eye-PL-3SG-ACC</ta>
            <ta e="T870" id="Seg_8293" s="T869">come.out-ADVZ</ta>
            <ta e="T871" id="Seg_8294" s="T870">see-CVB.SEQ</ta>
            <ta e="T872" id="Seg_8295" s="T871">stand.up-CVB.SEQ</ta>
            <ta e="T873" id="Seg_8296" s="T872">last</ta>
            <ta e="T874" id="Seg_8297" s="T873">power-3SG-INSTR</ta>
            <ta e="T875" id="Seg_8298" s="T874">bless-CVB.SIM</ta>
            <ta e="T876" id="Seg_8299" s="T875">stand-PST2.[3SG]</ta>
            <ta e="T877" id="Seg_8300" s="T876">well</ta>
            <ta e="T878" id="Seg_8301" s="T877">happy</ta>
            <ta e="T879" id="Seg_8302" s="T878">go-EP-IMP.2PL</ta>
            <ta e="T880" id="Seg_8303" s="T879">2PL.[NOM]</ta>
            <ta e="T881" id="Seg_8304" s="T880">way-2PL-DAT/LOC</ta>
            <ta e="T882" id="Seg_8305" s="T881">who.[NOM]</ta>
            <ta e="T883" id="Seg_8306" s="T882">NEG</ta>
            <ta e="T884" id="Seg_8307" s="T883">disturb-NEG.PTCP-3SG-ACC</ta>
            <ta e="T885" id="Seg_8308" s="T884">big</ta>
            <ta e="T886" id="Seg_8309" s="T885">mind-ACC</ta>
            <ta e="T887" id="Seg_8310" s="T886">take-EP-IMP.2PL</ta>
            <ta e="T888" id="Seg_8311" s="T887">fast-ADVZ</ta>
            <ta e="T889" id="Seg_8312" s="T888">come.back-EP-IMP.2PL</ta>
            <ta e="T890" id="Seg_8313" s="T889">be.born-PTCP.PST</ta>
            <ta e="T891" id="Seg_8314" s="T890">place-2PL-DAT/LOC</ta>
            <ta e="T892" id="Seg_8315" s="T891">stay-CVB.SIM</ta>
            <ta e="T893" id="Seg_8316" s="T892">people.[NOM]</ta>
            <ta e="T894" id="Seg_8317" s="T893">long</ta>
            <ta e="T895" id="Seg_8318" s="T894">very</ta>
            <ta e="T896" id="Seg_8319" s="T895">see-CVB.SIM</ta>
            <ta e="T897" id="Seg_8320" s="T896">stand-PST2-3PL</ta>
            <ta e="T898" id="Seg_8321" s="T897">tundra.[NOM]</ta>
            <ta e="T899" id="Seg_8322" s="T898">upper.part-3SG-INSTR</ta>
            <ta e="T900" id="Seg_8323" s="T899">long</ta>
            <ta e="T901" id="Seg_8324" s="T900">reindeer.caravan.[NOM]</ta>
            <ta e="T902" id="Seg_8325" s="T901">get.lost-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T903" id="Seg_8326" s="T902">until</ta>
            <ta e="T904" id="Seg_8327" s="T903">hill.[NOM]</ta>
            <ta e="T905" id="Seg_8328" s="T904">remote</ta>
            <ta e="T906" id="Seg_8329" s="T905">side-3SG-DAT/LOC</ta>
            <ta e="T907" id="Seg_8330" s="T906">child</ta>
            <ta e="T908" id="Seg_8331" s="T907">people-DAT/LOC</ta>
            <ta e="T909" id="Seg_8332" s="T908">root-VBZ-PST1-3SG</ta>
            <ta e="T910" id="Seg_8333" s="T909">new</ta>
            <ta e="T911" id="Seg_8334" s="T910">time.[NOM]</ta>
            <ta e="T912" id="Seg_8335" s="T911">learn-PTCP.PRS</ta>
            <ta e="T913" id="Seg_8336" s="T912">time.[NOM]</ta>
            <ta e="T914" id="Seg_8337" s="T913">writing-DAT/LOC</ta>
            <ta e="T915" id="Seg_8338" s="T914">2PL.[NOM]</ta>
            <ta e="T916" id="Seg_8339" s="T915">listen-PST1-2PL</ta>
            <ta e="T917" id="Seg_8340" s="T916">story-ACC</ta>
            <ta e="T918" id="Seg_8341" s="T917">Volochanka.[NOM]</ta>
            <ta e="T919" id="Seg_8342" s="T918">to</ta>
            <ta e="T920" id="Seg_8343" s="T919">nomadize-NMNZ.[NOM]</ta>
            <ta e="T921" id="Seg_8344" s="T920">read-PST2-3SG</ta>
            <ta e="T922" id="Seg_8345" s="T921">Popov.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_8346" s="T0">Volochanka.[NOM]</ta>
            <ta e="T2" id="Seg_8347" s="T1">zu</ta>
            <ta e="T3" id="Seg_8348" s="T2">nomadisieren-NMNZ.[NOM]</ta>
            <ta e="T4" id="Seg_8349" s="T3">Rentierbremse.[NOM]</ta>
            <ta e="T5" id="Seg_8350" s="T4">Monat-3SG.[NOM]</ta>
            <ta e="T6" id="Seg_8351" s="T5">enden-PTCP.PRS</ta>
            <ta e="T7" id="Seg_8352" s="T6">Tag-PL-3SG.[NOM]</ta>
            <ta e="T8" id="Seg_8353" s="T7">aufstehen-PST2-3PL</ta>
            <ta e="T9" id="Seg_8354" s="T8">warm.[NOM]</ta>
            <ta e="T10" id="Seg_8355" s="T9">sehr</ta>
            <ta e="T11" id="Seg_8356" s="T10">Gras-PL.[NOM]</ta>
            <ta e="T12" id="Seg_8357" s="T11">Blatt-PL.[NOM]</ta>
            <ta e="T13" id="Seg_8358" s="T12">vergilben-PST2-3PL</ta>
            <ta e="T14" id="Seg_8359" s="T13">Tundra.[NOM]</ta>
            <ta e="T15" id="Seg_8360" s="T14">dort-hier</ta>
            <ta e="T16" id="Seg_8361" s="T15">rot</ta>
            <ta e="T17" id="Seg_8362" s="T16">Gold.[NOM]</ta>
            <ta e="T18" id="Seg_8363" s="T17">werden-CVB.SEQ</ta>
            <ta e="T19" id="Seg_8364" s="T18">gehen-PST2.[3SG]</ta>
            <ta e="T20" id="Seg_8365" s="T19">jenes</ta>
            <ta e="T21" id="Seg_8366" s="T20">Tag-PL-DAT/LOC</ta>
            <ta e="T22" id="Seg_8367" s="T21">wissen-NMNZ-PROPR</ta>
            <ta e="T23" id="Seg_8368" s="T22">groß</ta>
            <ta e="T24" id="Seg_8369" s="T23">See.[NOM]</ta>
            <ta e="T25" id="Seg_8370" s="T24">Ufer-3SG-DAT/LOC</ta>
            <ta e="T26" id="Seg_8371" s="T25">groß</ta>
            <ta e="T27" id="Seg_8372" s="T26">sammeln-REFL-NMNZ.[NOM]</ta>
            <ta e="T28" id="Seg_8373" s="T27">stehen-PST2-3SG</ta>
            <ta e="T29" id="Seg_8374" s="T28">Kind.[NOM]</ta>
            <ta e="T30" id="Seg_8375" s="T29">Volk-ACC</ta>
            <ta e="T31" id="Seg_8376" s="T30">vorbereiten-PRS-3PL</ta>
            <ta e="T32" id="Seg_8377" s="T31">Volochanka-DAT/LOC</ta>
            <ta e="T33" id="Seg_8378" s="T32">mitnehmen-CVB.PURP</ta>
            <ta e="T34" id="Seg_8379" s="T33">Ausbildung-DAT/LOC</ta>
            <ta e="T35" id="Seg_8380" s="T34">Tundra.[NOM]</ta>
            <ta e="T36" id="Seg_8381" s="T35">Bevölkerung.[NOM]</ta>
            <ta e="T37" id="Seg_8382" s="T36">früher</ta>
            <ta e="T38" id="Seg_8383" s="T37">wissen-NEG.PTCP</ta>
            <ta e="T39" id="Seg_8384" s="T38">sein-PST1-3PL</ta>
            <ta e="T40" id="Seg_8385" s="T39">Kind-PL-3SG-ACC</ta>
            <ta e="T41" id="Seg_8386" s="T40">Ausbildung-DAT/LOC</ta>
            <ta e="T42" id="Seg_8387" s="T41">schicken-PTCP.PRS-ACC</ta>
            <ta e="T43" id="Seg_8388" s="T42">jenes.[NOM]</ta>
            <ta e="T44" id="Seg_8389" s="T43">wegen</ta>
            <ta e="T45" id="Seg_8390" s="T44">Wunsch-PL-3SG.[NOM]</ta>
            <ta e="T46" id="Seg_8391" s="T45">gehen-NEG.[3SG]</ta>
            <ta e="T47" id="Seg_8392" s="T46">Schule-DAT/LOC</ta>
            <ta e="T48" id="Seg_8393" s="T47">geben-PTCP.PRS-DAT/LOC</ta>
            <ta e="T49" id="Seg_8394" s="T48">verschieden</ta>
            <ta e="T50" id="Seg_8395" s="T49">Wort-ACC</ta>
            <ta e="T51" id="Seg_8396" s="T50">finden-PRS-3PL</ta>
            <ta e="T52" id="Seg_8397" s="T51">schicken-EP-NEG-CVB.PURP</ta>
            <ta e="T53" id="Seg_8398" s="T52">Kind-PL-3SG-ACC</ta>
            <ta e="T54" id="Seg_8399" s="T53">vom.Stamm-3PL-DAT/LOC</ta>
            <ta e="T55" id="Seg_8400" s="T54">jenes</ta>
            <ta e="T56" id="Seg_8401" s="T55">Tag-PL-DAT/LOC</ta>
            <ta e="T57" id="Seg_8402" s="T56">sehr</ta>
            <ta e="T58" id="Seg_8403" s="T57">Qual-PROPR</ta>
            <ta e="T59" id="Seg_8404" s="T58">Arbeit-ACC</ta>
            <ta e="T60" id="Seg_8405" s="T59">machen-PST2-3PL</ta>
            <ta e="T61" id="Seg_8406" s="T60">jeder</ta>
            <ta e="T62" id="Seg_8407" s="T61">Stange.[NOM]</ta>
            <ta e="T63" id="Seg_8408" s="T62">Zelt.[NOM]</ta>
            <ta e="T64" id="Seg_8409" s="T63">jeder</ta>
            <ta e="T65" id="Seg_8410" s="T64">gehen-CVB.SEQ</ta>
            <ta e="T66" id="Seg_8411" s="T65">sich.unterhalten-CVB.SIM</ta>
            <ta e="T67" id="Seg_8412" s="T66">versuchen-PST2.[3SG]</ta>
            <ta e="T68" id="Seg_8413" s="T67">Kind-PL-3SG-ACC</ta>
            <ta e="T69" id="Seg_8414" s="T68">Schule-DAT/LOC</ta>
            <ta e="T70" id="Seg_8415" s="T69">geben-PTCP.FUT-3PL-ACC</ta>
            <ta e="T71" id="Seg_8416" s="T70">Ausbildung-DAT/LOC</ta>
            <ta e="T72" id="Seg_8417" s="T71">schicken-PTCP.FUT-3PL-ACC</ta>
            <ta e="T73" id="Seg_8418" s="T72">dieses-ABL</ta>
            <ta e="T74" id="Seg_8419" s="T73">was.[NOM]</ta>
            <ta e="T75" id="Seg_8420" s="T74">NEG</ta>
            <ta e="T76" id="Seg_8421" s="T75">erscheinen-EP-PST2.NEG.[3SG]</ta>
            <ta e="T77" id="Seg_8422" s="T76">jenes.[NOM]</ta>
            <ta e="T78" id="Seg_8423" s="T77">wegen</ta>
            <ta e="T79" id="Seg_8424" s="T78">Abmachung.[NOM]</ta>
            <ta e="T80" id="Seg_8425" s="T79">werden-PST2.[3SG]</ta>
            <ta e="T81" id="Seg_8426" s="T80">machen-PTCP.PRS-DAT/LOC</ta>
            <ta e="T82" id="Seg_8427" s="T81">sagen-CVB.SEQ</ta>
            <ta e="T83" id="Seg_8428" s="T82">Versammlung-ACC</ta>
            <ta e="T84" id="Seg_8429" s="T83">Tundra.[NOM]</ta>
            <ta e="T85" id="Seg_8430" s="T84">oberer.Teil-3SG-INSTR</ta>
            <ta e="T86" id="Seg_8431" s="T85">jeder-3PL.[NOM]</ta>
            <ta e="T87" id="Seg_8432" s="T86">sammeln-PASS/REFL-PTCP.FUT-3PL-ACC</ta>
            <ta e="T88" id="Seg_8433" s="T87">Gesicht.[NOM]</ta>
            <ta e="T89" id="Seg_8434" s="T88">Gesicht-DAT/LOC</ta>
            <ta e="T90" id="Seg_8435" s="T89">sich.unterhalten-NMNZ.[NOM]</ta>
            <ta e="T91" id="Seg_8436" s="T90">werden-PTCP.FUT-3SG-ACC</ta>
            <ta e="T92" id="Seg_8437" s="T91">eins</ta>
            <ta e="T93" id="Seg_8438" s="T92">groß</ta>
            <ta e="T94" id="Seg_8439" s="T93">Stange.[NOM]</ta>
            <ta e="T95" id="Seg_8440" s="T94">Haus-DAT/LOC</ta>
            <ta e="T96" id="Seg_8441" s="T95">kaum</ta>
            <ta e="T97" id="Seg_8442" s="T96">passen-CVB.SEQ</ta>
            <ta e="T98" id="Seg_8443" s="T97">sammeln-PASS/REFL-EP-PST2-3PL</ta>
            <ta e="T99" id="Seg_8444" s="T98">stehen-PRS.[3SG]</ta>
            <ta e="T100" id="Seg_8445" s="T99">stehen-NEG.[3SG]</ta>
            <ta e="T101" id="Seg_8446" s="T100">Mann.[NOM]</ta>
            <ta e="T102" id="Seg_8447" s="T101">Mensch-PL.[NOM]</ta>
            <ta e="T103" id="Seg_8448" s="T102">was.[NOM]</ta>
            <ta e="T104" id="Seg_8449" s="T103">NEG</ta>
            <ta e="T105" id="Seg_8450" s="T104">sagen-PTCP.FUT-3PL-ACC</ta>
            <ta e="T106" id="Seg_8451" s="T105">Kraft-3SG-ABL</ta>
            <ta e="T107" id="Seg_8452" s="T106">Erde.[NOM]</ta>
            <ta e="T108" id="Seg_8453" s="T107">zu</ta>
            <ta e="T109" id="Seg_8454" s="T108">nur</ta>
            <ta e="T110" id="Seg_8455" s="T109">sehen-PRS-3PL</ta>
            <ta e="T111" id="Seg_8456" s="T110">Frau.[NOM]</ta>
            <ta e="T112" id="Seg_8457" s="T111">Volk.[NOM]</ta>
            <ta e="T113" id="Seg_8458" s="T112">Tuch-3PL-INSTR</ta>
            <ta e="T114" id="Seg_8459" s="T113">Auge-3PL-GEN</ta>
            <ta e="T115" id="Seg_8460" s="T114">Wasser-3PL-ACC</ta>
            <ta e="T116" id="Seg_8461" s="T115">wischen-EP-MED-PRS-3PL</ta>
            <ta e="T117" id="Seg_8462" s="T116">%%-PL.[NOM]</ta>
            <ta e="T118" id="Seg_8463" s="T117">was.[NOM]</ta>
            <ta e="T119" id="Seg_8464" s="T118">sein-CVB.SEQ-2PL</ta>
            <ta e="T120" id="Seg_8465" s="T119">Kopf-PL-2PL-ACC</ta>
            <ta e="T121" id="Seg_8466" s="T120">Knie-PL-2PL.[NOM]</ta>
            <ta e="T122" id="Seg_8467" s="T121">Inneres-3SG-DAT/LOC</ta>
            <ta e="T123" id="Seg_8468" s="T122">hineingehen-CAUS-PST1-2PL</ta>
            <ta e="T124" id="Seg_8469" s="T123">warum</ta>
            <ta e="T125" id="Seg_8470" s="T124">leise.werden-RECP/COLL-CVB.SIM</ta>
            <ta e="T126" id="Seg_8471" s="T125">liegen-PRS-2PL</ta>
            <ta e="T127" id="Seg_8472" s="T126">sprechen-PST2.[3SG]</ta>
            <ta e="T128" id="Seg_8473" s="T127">sagen-CVB.SEQ</ta>
            <ta e="T129" id="Seg_8474" s="T128">Jevgenija</ta>
            <ta e="T130" id="Seg_8475" s="T129">Sprache-PROPR</ta>
            <ta e="T131" id="Seg_8476" s="T130">Alte.[NOM]</ta>
            <ta e="T132" id="Seg_8477" s="T131">schreien-CVB.SEQ</ta>
            <ta e="T133" id="Seg_8478" s="T132">geben-PST2.[3SG]</ta>
            <ta e="T134" id="Seg_8479" s="T133">3SG.[NOM]</ta>
            <ta e="T135" id="Seg_8480" s="T134">MOD</ta>
            <ta e="T136" id="Seg_8481" s="T135">keck-3SG.[NOM]</ta>
            <ta e="T137" id="Seg_8482" s="T136">sehr</ta>
            <ta e="T138" id="Seg_8483" s="T137">wer-ABL</ta>
            <ta e="T139" id="Seg_8484" s="T138">NEG</ta>
            <ta e="T140" id="Seg_8485" s="T139">Angst.haben-EP-NEG.[3SG]</ta>
            <ta e="T141" id="Seg_8486" s="T140">sitzen-PTCP.PRS</ta>
            <ta e="T142" id="Seg_8487" s="T141">Ort-ABL</ta>
            <ta e="T143" id="Seg_8488" s="T142">ganz.leise</ta>
            <ta e="T144" id="Seg_8489" s="T143">aufstehen-PST2.[3SG]</ta>
            <ta e="T145" id="Seg_8490" s="T144">Porfirij</ta>
            <ta e="T146" id="Seg_8491" s="T145">alter.Mann.[NOM]</ta>
            <ta e="T147" id="Seg_8492" s="T146">kahl</ta>
            <ta e="T148" id="Seg_8493" s="T147">Kopf-3SG-ACC</ta>
            <ta e="T149" id="Seg_8494" s="T148">streichen.über-CVB.SIM</ta>
            <ta e="T150" id="Seg_8495" s="T149">fallen-CVB.ANT</ta>
            <ta e="T151" id="Seg_8496" s="T150">eilen-NEG.CVB.SIM</ta>
            <ta e="T152" id="Seg_8497" s="T151">betrachten-EP-MED-PST2.[3SG]</ta>
            <ta e="T153" id="Seg_8498" s="T152">wie</ta>
            <ta e="T154" id="Seg_8499" s="T153">sagen-FUT-1SG=Q</ta>
            <ta e="T155" id="Seg_8500" s="T154">dieses.[NOM]</ta>
            <ta e="T156" id="Seg_8501" s="T155">so</ta>
            <ta e="T157" id="Seg_8502" s="T156">sein-PRS.[3SG]</ta>
            <ta e="T158" id="Seg_8503" s="T157">Kind-PL.[NOM]</ta>
            <ta e="T159" id="Seg_8504" s="T158">dieses</ta>
            <ta e="T160" id="Seg_8505" s="T159">neu</ta>
            <ta e="T161" id="Seg_8506" s="T160">Zeit-DAT/LOC</ta>
            <ta e="T162" id="Seg_8507" s="T161">lernen-PTCP.PRS-3PL.[NOM]</ta>
            <ta e="T163" id="Seg_8508" s="T162">doch</ta>
            <ta e="T164" id="Seg_8509" s="T163">EMPH</ta>
            <ta e="T165" id="Seg_8510" s="T164">sehr</ta>
            <ta e="T166" id="Seg_8511" s="T165">richtig.[NOM]</ta>
            <ta e="T167" id="Seg_8512" s="T166">3PL.[NOM]</ta>
            <ta e="T168" id="Seg_8513" s="T167">Schrift-AG.[NOM]</ta>
            <ta e="T169" id="Seg_8514" s="T168">sein-PTCP.FUT</ta>
            <ta e="T170" id="Seg_8515" s="T169">fallen-NEC-3PL</ta>
            <ta e="T171" id="Seg_8516" s="T170">dieses</ta>
            <ta e="T172" id="Seg_8517" s="T171">1SG.[NOM]</ta>
            <ta e="T173" id="Seg_8518" s="T172">Ivan.[NOM]</ta>
            <ta e="T174" id="Seg_8519" s="T173">Sohn-1SG-ACC</ta>
            <ta e="T175" id="Seg_8520" s="T174">klein-3SG-ABL</ta>
            <ta e="T176" id="Seg_8521" s="T175">lehren-PST2-EP-1SG</ta>
            <ta e="T177" id="Seg_8522" s="T176">Fisch.[NOM]</ta>
            <ta e="T178" id="Seg_8523" s="T177">Tier.[NOM]</ta>
            <ta e="T179" id="Seg_8524" s="T178">Fangeisen-VBZ-PTCP.FUT-3SG-ACC</ta>
            <ta e="T180" id="Seg_8525" s="T179">1SG.[NOM]</ta>
            <ta e="T181" id="Seg_8526" s="T180">jetzt</ta>
            <ta e="T182" id="Seg_8527" s="T181">altern-PST1-1SG</ta>
            <ta e="T183" id="Seg_8528" s="T182">Arbeit-ACC</ta>
            <ta e="T184" id="Seg_8529" s="T183">schaffen-EP-MED-NEG.PTCP</ta>
            <ta e="T185" id="Seg_8530" s="T184">sein-CVB.SEQ</ta>
            <ta e="T186" id="Seg_8531" s="T185">sein-EP-1SG</ta>
            <ta e="T187" id="Seg_8532" s="T186">wer.[NOM]</ta>
            <ta e="T188" id="Seg_8533" s="T187">1SG.[NOM]</ta>
            <ta e="T189" id="Seg_8534" s="T188">Familie-1SG-ACC</ta>
            <ta e="T190" id="Seg_8535" s="T189">essen-EP-CAUS-FUT-3SG=Q</ta>
            <ta e="T191" id="Seg_8536" s="T190">Tundra-1PL.[NOM]</ta>
            <ta e="T192" id="Seg_8537" s="T191">kalt.[NOM]</ta>
            <ta e="T193" id="Seg_8538" s="T192">sehr</ta>
            <ta e="T194" id="Seg_8539" s="T193">Schneesturm-PROPR.[NOM]</ta>
            <ta e="T195" id="Seg_8540" s="T194">stark</ta>
            <ta e="T196" id="Seg_8541" s="T195">nur</ta>
            <ta e="T197" id="Seg_8542" s="T196">jung.[NOM]</ta>
            <ta e="T198" id="Seg_8543" s="T197">nur</ta>
            <ta e="T199" id="Seg_8544" s="T198">Mensch.[NOM]</ta>
            <ta e="T200" id="Seg_8545" s="T199">Fang-ACC</ta>
            <ta e="T201" id="Seg_8546" s="T200">finden-HAB.[3SG]</ta>
            <ta e="T202" id="Seg_8547" s="T201">Ivan-ACC</ta>
            <ta e="T203" id="Seg_8548" s="T202">Schule-DAT/LOC</ta>
            <ta e="T205" id="Seg_8549" s="T204">Totfalle-ACC</ta>
            <ta e="T206" id="Seg_8550" s="T205">Fangeisen-ACC</ta>
            <ta e="T207" id="Seg_8551" s="T206">aufstellen-CVB.SIM</ta>
            <ta e="T208" id="Seg_8552" s="T207">lehren-FUT-3PL</ta>
            <ta e="T209" id="Seg_8553" s="T208">NEG-3SG</ta>
            <ta e="T210" id="Seg_8554" s="T209">3SG.[NOM]</ta>
            <ta e="T211" id="Seg_8555" s="T210">vergessen-FUT-3SG</ta>
            <ta e="T212" id="Seg_8556" s="T211">Tundra.[NOM]</ta>
            <ta e="T213" id="Seg_8557" s="T212">Arbeit-3SG-ACC</ta>
            <ta e="T214" id="Seg_8558" s="T213">Frau.[NOM]</ta>
            <ta e="T215" id="Seg_8559" s="T214">ähnlich</ta>
            <ta e="T216" id="Seg_8560" s="T215">Haus-DAT/LOC</ta>
            <ta e="T217" id="Seg_8561" s="T216">nur</ta>
            <ta e="T218" id="Seg_8562" s="T217">sitzen-FUT-3SG</ta>
            <ta e="T219" id="Seg_8563" s="T218">was-ACC</ta>
            <ta e="T220" id="Seg_8564" s="T219">NEG</ta>
            <ta e="T221" id="Seg_8565" s="T220">machen-NEG.CVB.SIM</ta>
            <ta e="T222" id="Seg_8566" s="T221">2SG.[NOM]</ta>
            <ta e="T223" id="Seg_8567" s="T222">alter.Mann.[NOM]</ta>
            <ta e="T224" id="Seg_8568" s="T223">Frau-PL-ACC</ta>
            <ta e="T225" id="Seg_8569" s="T224">berühren-EP-NEG.[IMP.2SG]</ta>
            <ta e="T226" id="Seg_8570" s="T225">wieder</ta>
            <ta e="T227" id="Seg_8571" s="T226">Jevgenija.[NOM]</ta>
            <ta e="T228" id="Seg_8572" s="T227">Schrei-3SG.[NOM]</ta>
            <ta e="T229" id="Seg_8573" s="T228">gehört.werden-EP-PST2.[3SG]</ta>
            <ta e="T230" id="Seg_8574" s="T229">wer.[NOM]</ta>
            <ta e="T231" id="Seg_8575" s="T230">2SG-DAT/LOC</ta>
            <ta e="T232" id="Seg_8576" s="T231">Nahrung-ACC</ta>
            <ta e="T233" id="Seg_8577" s="T232">vorbereiten-PRS.[3SG]</ta>
            <ta e="T234" id="Seg_8578" s="T233">Haus-2SG-ACC</ta>
            <ta e="T235" id="Seg_8579" s="T234">warm-ADVZ</ta>
            <ta e="T236" id="Seg_8580" s="T235">halten-PRS.[3SG]</ta>
            <ta e="T237" id="Seg_8581" s="T236">Schuhe-PL-2SG-ACC</ta>
            <ta e="T238" id="Seg_8582" s="T237">Kleidung-2SG-ACC</ta>
            <ta e="T239" id="Seg_8583" s="T238">flicken-PRS.[3SG]</ta>
            <ta e="T240" id="Seg_8584" s="T239">dieses.[NOM]</ta>
            <ta e="T241" id="Seg_8585" s="T240">Wahrheit.[NOM]</ta>
            <ta e="T242" id="Seg_8586" s="T241">so</ta>
            <ta e="T243" id="Seg_8587" s="T242">sein-PRS.[3SG]</ta>
            <ta e="T244" id="Seg_8588" s="T243">sehr</ta>
            <ta e="T245" id="Seg_8589" s="T244">Angst.haben.[CAUS]-NEG.CVB.SIM</ta>
            <ta e="T246" id="Seg_8590" s="T245">Porfirij.[NOM]</ta>
            <ta e="T247" id="Seg_8591" s="T246">sprechen-CVB.SIM</ta>
            <ta e="T248" id="Seg_8592" s="T247">stehen-PST2.[3SG]</ta>
            <ta e="T249" id="Seg_8593" s="T248">1SG.[NOM]</ta>
            <ta e="T250" id="Seg_8594" s="T249">Kytscha</ta>
            <ta e="T251" id="Seg_8595" s="T250">Tochter-EP-1SG.[NOM]</ta>
            <ta e="T252" id="Seg_8596" s="T251">zubereiten-CVB.SIM</ta>
            <ta e="T253" id="Seg_8597" s="T252">können-PRS.[3SG]</ta>
            <ta e="T254" id="Seg_8598" s="T253">Schuhe-ACC</ta>
            <ta e="T255" id="Seg_8599" s="T254">Kleidung-ACC</ta>
            <ta e="T256" id="Seg_8600" s="T255">machen-CVB.SIM</ta>
            <ta e="T257" id="Seg_8601" s="T256">können-PRS.[3SG]</ta>
            <ta e="T258" id="Seg_8602" s="T257">Schule-DAT/LOC</ta>
            <ta e="T259" id="Seg_8603" s="T258">doch</ta>
            <ta e="T260" id="Seg_8604" s="T259">EMPH</ta>
            <ta e="T261" id="Seg_8605" s="T260">jenes-3SG-3SG-ACC</ta>
            <ta e="T262" id="Seg_8606" s="T261">vergessen-FUT-3SG</ta>
            <ta e="T263" id="Seg_8607" s="T262">Mann.[NOM]</ta>
            <ta e="T264" id="Seg_8608" s="T263">Mensch.[NOM]</ta>
            <ta e="T265" id="Seg_8609" s="T264">ähnlich</ta>
            <ta e="T266" id="Seg_8610" s="T265">aufrecht.stehen-ITER-CVB.SIM</ta>
            <ta e="T267" id="Seg_8611" s="T266">gehen-FUT-3SG</ta>
            <ta e="T268" id="Seg_8612" s="T267">Haus-ACC</ta>
            <ta e="T269" id="Seg_8613" s="T268">umgehen-CVB.SIM</ta>
            <ta e="T270" id="Seg_8614" s="T269">wie</ta>
            <ta e="T271" id="Seg_8615" s="T270">1SG.[NOM]</ta>
            <ta e="T272" id="Seg_8616" s="T271">leben-FUT-1SG=Q</ta>
            <ta e="T273" id="Seg_8617" s="T272">Kind-PL-EP-1SG.[NOM]</ta>
            <ta e="T274" id="Seg_8618" s="T273">gehen-TEMP-3PL</ta>
            <ta e="T275" id="Seg_8619" s="T274">vom.Stamm-3SG.[NOM]</ta>
            <ta e="T276" id="Seg_8620" s="T275">Anisim</ta>
            <ta e="T277" id="Seg_8621" s="T276">Nazarowitsch</ta>
            <ta e="T278" id="Seg_8622" s="T277">Popov.[NOM]</ta>
            <ta e="T279" id="Seg_8623" s="T278">in.Lachen.ausbrechen-CVB.SIM</ta>
            <ta e="T280" id="Seg_8624" s="T279">fallen-CVB.ANT</ta>
            <ta e="T281" id="Seg_8625" s="T280">alter.Mann-ACC</ta>
            <ta e="T282" id="Seg_8626" s="T281">halten-EP-CAUS-PST2.[3SG]</ta>
            <ta e="T283" id="Seg_8627" s="T282">doch</ta>
            <ta e="T284" id="Seg_8628" s="T283">alter.Mann.[NOM]</ta>
            <ta e="T285" id="Seg_8629" s="T284">2SG.[NOM]</ta>
            <ta e="T286" id="Seg_8630" s="T285">müde.werden-PST1-2SG</ta>
            <ta e="T287" id="Seg_8631" s="T286">offenbar</ta>
            <ta e="T288" id="Seg_8632" s="T287">sich.setzen-CVB.SIM</ta>
            <ta e="T289" id="Seg_8633" s="T288">fallen.[IMP.2SG]</ta>
            <ta e="T290" id="Seg_8634" s="T289">anders</ta>
            <ta e="T291" id="Seg_8635" s="T290">Leute.[NOM]</ta>
            <ta e="T292" id="Seg_8636" s="T291">sprechen-IMP.3SG</ta>
            <ta e="T293" id="Seg_8637" s="T292">alter.Mann.[NOM]</ta>
            <ta e="T294" id="Seg_8638" s="T293">was.[NOM]</ta>
            <ta e="T295" id="Seg_8639" s="T294">NEG</ta>
            <ta e="T296" id="Seg_8640" s="T295">sagen-NEG.CVB.SIM</ta>
            <ta e="T297" id="Seg_8641" s="T296">ganz.leise</ta>
            <ta e="T298" id="Seg_8642" s="T297">Sitz-3SG-DAT/LOC</ta>
            <ta e="T299" id="Seg_8643" s="T298">platsch</ta>
            <ta e="T300" id="Seg_8644" s="T299">machen-CVB.SIM</ta>
            <ta e="T301" id="Seg_8645" s="T300">sich.setzen-PST2.[3SG]</ta>
            <ta e="T302" id="Seg_8646" s="T301">Knie-PL-3SG-ABL</ta>
            <ta e="T303" id="Seg_8647" s="T302">ergreifen-CVB.SIM</ta>
            <ta e="T304" id="Seg_8648" s="T303">fallen-CVB.ANT</ta>
            <ta e="T305" id="Seg_8649" s="T304">wer.[NOM]</ta>
            <ta e="T306" id="Seg_8650" s="T305">noch</ta>
            <ta e="T307" id="Seg_8651" s="T306">was.[NOM]</ta>
            <ta e="T308" id="Seg_8652" s="T307">Wort-PROPR.[NOM]=Q</ta>
            <ta e="T309" id="Seg_8653" s="T308">sagen-CVB.SEQ</ta>
            <ta e="T310" id="Seg_8654" s="T309">fragen-PST2.[3SG]</ta>
            <ta e="T311" id="Seg_8655" s="T310">vom.Stamm-3PL.[NOM]</ta>
            <ta e="T312" id="Seg_8656" s="T311">wer-3PL.[NOM]</ta>
            <ta e="T313" id="Seg_8657" s="T312">INDEF</ta>
            <ta e="T314" id="Seg_8658" s="T313">sprechen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T315" id="Seg_8659" s="T314">warten-CVB.SIM</ta>
            <ta e="T316" id="Seg_8660" s="T315">vergeblich.tun-CVB.SEQ</ta>
            <ta e="T317" id="Seg_8661" s="T316">zählen-PST2.[3SG]</ta>
            <ta e="T318" id="Seg_8662" s="T317">was.für.ein.[NOM]</ta>
            <ta e="T319" id="Seg_8663" s="T318">Kind-3PL.[NOM]</ta>
            <ta e="T320" id="Seg_8664" s="T319">Schule-DAT/LOC</ta>
            <ta e="T321" id="Seg_8665" s="T320">gehen-PTCP.PRS-3PL-ACC</ta>
            <ta e="T322" id="Seg_8666" s="T321">wer.[NOM]</ta>
            <ta e="T323" id="Seg_8667" s="T322">bleiben-PTCP.PRS-3SG-ACC</ta>
            <ta e="T324" id="Seg_8668" s="T323">Schule-DAT/LOC</ta>
            <ta e="T325" id="Seg_8669" s="T324">gehen-NEG.PTCP-3SG-ACC</ta>
            <ta e="T326" id="Seg_8670" s="T325">Alter-3PL.[NOM]</ta>
            <ta e="T327" id="Seg_8671" s="T326">groß-PL-ACC</ta>
            <ta e="T328" id="Seg_8672" s="T327">jenes</ta>
            <ta e="T329" id="Seg_8673" s="T328">Kind-PL-DAT/LOC</ta>
            <ta e="T330" id="Seg_8674" s="T329">geraten-PST2.[3SG]</ta>
            <ta e="T331" id="Seg_8675" s="T330">Porfirij.[NOM]</ta>
            <ta e="T332" id="Seg_8676" s="T331">alter.Mann.[NOM]</ta>
            <ta e="T333" id="Seg_8677" s="T332">Sohn-3SG.[NOM]</ta>
            <ta e="T334" id="Seg_8678" s="T333">Ivan.[NOM]</ta>
            <ta e="T335" id="Seg_8679" s="T334">Kytscha-ACC</ta>
            <ta e="T336" id="Seg_8680" s="T335">auch</ta>
            <ta e="T337" id="Seg_8681" s="T336">lassen-PTCP.FUT-DAT/LOC</ta>
            <ta e="T338" id="Seg_8682" s="T337">fallen-NEC.[3SG]</ta>
            <ta e="T339" id="Seg_8683" s="T338">3SG.[NOM]</ta>
            <ta e="T340" id="Seg_8684" s="T339">Alter-3SG.[NOM]</ta>
            <ta e="T341" id="Seg_8685" s="T340">doch</ta>
            <ta e="T342" id="Seg_8686" s="T341">EMPH</ta>
            <ta e="T343" id="Seg_8687" s="T342">viel</ta>
            <ta e="T344" id="Seg_8688" s="T343">bitten-EP-MED-PST2.[3SG]</ta>
            <ta e="T345" id="Seg_8689" s="T344">Porfirij.[NOM]</ta>
            <ta e="T346" id="Seg_8690" s="T345">alter.Mann.[NOM]</ta>
            <ta e="T347" id="Seg_8691" s="T346">nein</ta>
            <ta e="T348" id="Seg_8692" s="T347">Kytscha.[NOM]</ta>
            <ta e="T349" id="Seg_8693" s="T348">gehen-FUT.[3SG]</ta>
            <ta e="T350" id="Seg_8694" s="T349">NEG-3SG</ta>
            <ta e="T351" id="Seg_8695" s="T350">Schule-DAT/LOC</ta>
            <ta e="T352" id="Seg_8696" s="T351">alter.Mann.[NOM]</ta>
            <ta e="T353" id="Seg_8697" s="T352">fürchten-EP-NEG.[IMP.2SG]</ta>
            <ta e="T354" id="Seg_8698" s="T353">Ausbildung-PROPR.[NOM]</ta>
            <ta e="T355" id="Seg_8699" s="T354">sein-TEMP-3SG</ta>
            <ta e="T356" id="Seg_8700" s="T355">Mann.[NOM]</ta>
            <ta e="T357" id="Seg_8701" s="T356">Mensch.[NOM]</ta>
            <ta e="T358" id="Seg_8702" s="T357">Hose-3SG-ACC</ta>
            <ta e="T359" id="Seg_8703" s="T358">anziehen-FUT.[3SG]</ta>
            <ta e="T360" id="Seg_8704" s="T359">NEG-3SG</ta>
            <ta e="T361" id="Seg_8705" s="T360">Haus-ACC</ta>
            <ta e="T362" id="Seg_8706" s="T361">um.herum</ta>
            <ta e="T363" id="Seg_8707" s="T362">aufrecht.stehen-ITER-CVB.SIM</ta>
            <ta e="T364" id="Seg_8708" s="T363">gehen-FUT.[3SG]</ta>
            <ta e="T365" id="Seg_8709" s="T364">NEG-3SG</ta>
            <ta e="T366" id="Seg_8710" s="T365">Versammlung-DAT/LOC</ta>
            <ta e="T367" id="Seg_8711" s="T366">sitzen-PTCP.PRS</ta>
            <ta e="T368" id="Seg_8712" s="T367">Leute.[NOM]</ta>
            <ta e="T369" id="Seg_8713" s="T368">letzter</ta>
            <ta e="T370" id="Seg_8714" s="T369">Seele-3PL-INSTR</ta>
            <ta e="T371" id="Seg_8715" s="T370">lachen-RECP/COLL-EP-PST2-3PL</ta>
            <ta e="T372" id="Seg_8716" s="T371">gehen-PTCP.PRS</ta>
            <ta e="T373" id="Seg_8717" s="T372">Kind-PL-ACC</ta>
            <ta e="T376" id="Seg_8718" s="T375">vorbereiten-IMP.2PL</ta>
            <ta e="T377" id="Seg_8719" s="T376">gut-ADVZ</ta>
            <ta e="T378" id="Seg_8720" s="T377">sich.anziehen-CAUS-EP-IMP.2PL</ta>
            <ta e="T379" id="Seg_8721" s="T378">Feiertag-DAT/LOC</ta>
            <ta e="T380" id="Seg_8722" s="T379">sich.anziehen-CAUS-PTCP.PRS.[NOM]</ta>
            <ta e="T381" id="Seg_8723" s="T380">ähnlich</ta>
            <ta e="T382" id="Seg_8724" s="T381">Kind-PL-1PL.[NOM]</ta>
            <ta e="T383" id="Seg_8725" s="T382">selbst-1SG.[NOM]</ta>
            <ta e="T384" id="Seg_8726" s="T383">mitnehmen-FUT-1SG</ta>
            <ta e="T385" id="Seg_8727" s="T384">Volochanka-ABL</ta>
            <ta e="T386" id="Seg_8728" s="T385">Versammlung-ACC</ta>
            <ta e="T387" id="Seg_8729" s="T386">aufhören-EP-CAUS-CVB.SIM</ta>
            <ta e="T388" id="Seg_8730" s="T387">machen-CVB.SIM</ta>
            <ta e="T389" id="Seg_8731" s="T388">sagen-PST2.[3SG]</ta>
            <ta e="T390" id="Seg_8732" s="T389">Anführer-3PL.[NOM]</ta>
            <ta e="T391" id="Seg_8733" s="T390">Versammlung.[NOM]</ta>
            <ta e="T392" id="Seg_8734" s="T391">vorbeigehen-CVB.ANT-3SG</ta>
            <ta e="T393" id="Seg_8735" s="T392">sammeln-MED-NMNZ.[NOM]</ta>
            <ta e="T394" id="Seg_8736" s="T393">Lärm-3SG.[NOM]</ta>
            <ta e="T395" id="Seg_8737" s="T394">mehr.werden-PST2.[3SG]</ta>
            <ta e="T396" id="Seg_8738" s="T395">Frau-PL.[NOM]</ta>
            <ta e="T397" id="Seg_8739" s="T396">Tasche-3PL-GEN</ta>
            <ta e="T398" id="Seg_8740" s="T397">Inneres-3SG-ABL</ta>
            <ta e="T399" id="Seg_8741" s="T398">herausnehmen-ITER-PRS-3PL</ta>
            <ta e="T400" id="Seg_8742" s="T399">Kind-3PL-GEN</ta>
            <ta e="T401" id="Seg_8743" s="T400">breit</ta>
            <ta e="T402" id="Seg_8744" s="T401">breit</ta>
            <ta e="T403" id="Seg_8745" s="T402">Kleidung-3PL-ACC</ta>
            <ta e="T404" id="Seg_8746" s="T403">Feiertag-DAT/LOC</ta>
            <ta e="T405" id="Seg_8747" s="T404">anziehen-PTCP.PRS</ta>
            <ta e="T406" id="Seg_8748" s="T405">Hemd-3PL-ACC</ta>
            <ta e="T407" id="Seg_8749" s="T406">(irgend)ein-PL.[NOM]</ta>
            <ta e="T408" id="Seg_8750" s="T407">zu.sehen.sein-EP-NEG.PTCP</ta>
            <ta e="T409" id="Seg_8751" s="T408">Ort-DAT/LOC</ta>
            <ta e="T410" id="Seg_8752" s="T409">nähen-MULT-PRS-3PL</ta>
            <ta e="T411" id="Seg_8753" s="T410">sein-PST2.[3SG]</ta>
            <ta e="T412" id="Seg_8754" s="T411">klein</ta>
            <ta e="T413" id="Seg_8755" s="T412">Ikone-3PL-ACC</ta>
            <ta e="T414" id="Seg_8756" s="T413">Kreuz-PL-ACC</ta>
            <ta e="T415" id="Seg_8757" s="T414">Kind-PL-3SG-ACC</ta>
            <ta e="T416" id="Seg_8758" s="T415">ehren-3PL</ta>
            <ta e="T417" id="Seg_8759" s="T416">jeder</ta>
            <ta e="T418" id="Seg_8760" s="T417">letzter</ta>
            <ta e="T419" id="Seg_8761" s="T418">schmackhaft</ta>
            <ta e="T420" id="Seg_8762" s="T419">Nahrung-PL-EP-INSTR</ta>
            <ta e="T421" id="Seg_8763" s="T420">gehacktes.Fleisch.mit.Fett-INSTR</ta>
            <ta e="T422" id="Seg_8764" s="T421">getrockneter.Fisch-EP-INSTR</ta>
            <ta e="T423" id="Seg_8765" s="T422">Rentier.[NOM]</ta>
            <ta e="T424" id="Seg_8766" s="T423">Zunge-EP-INSTR</ta>
            <ta e="T425" id="Seg_8767" s="T424">Abend.[NOM]</ta>
            <ta e="T426" id="Seg_8768" s="T425">zu</ta>
            <ta e="T429" id="Seg_8769" s="T428">vom.Stamm-3SG-DAT/LOC</ta>
            <ta e="T430" id="Seg_8770" s="T429">besuchen-CVB.SIM</ta>
            <ta e="T431" id="Seg_8771" s="T430">hineingehen-PST2.[3SG]</ta>
            <ta e="T432" id="Seg_8772" s="T431">Semjon</ta>
            <ta e="T433" id="Seg_8773" s="T432">alter.Mann.[NOM]</ta>
            <ta e="T434" id="Seg_8774" s="T433">warum</ta>
            <ta e="T435" id="Seg_8775" s="T434">kommen-PTCP.PST-3SG-ACC</ta>
            <ta e="T436" id="Seg_8776" s="T435">durch</ta>
            <ta e="T437" id="Seg_8777" s="T436">sprechen-NEG.[3SG]</ta>
            <ta e="T438" id="Seg_8778" s="T437">Tee.[NOM]</ta>
            <ta e="T439" id="Seg_8779" s="T438">trinken-CVB.SIM</ta>
            <ta e="T440" id="Seg_8780" s="T439">nur</ta>
            <ta e="T441" id="Seg_8781" s="T440">sitzen-CVB.SEQ</ta>
            <ta e="T442" id="Seg_8782" s="T441">sagen-PST2.[3SG]</ta>
            <ta e="T443" id="Seg_8783" s="T442">vor.langer.Zeit</ta>
            <ta e="T444" id="Seg_8784" s="T443">Großvater-PL-1PL.[NOM]</ta>
            <ta e="T445" id="Seg_8785" s="T444">Vater-PL-1PL.[NOM]</ta>
            <ta e="T446" id="Seg_8786" s="T445">Gott-AG.[NOM]</ta>
            <ta e="T447" id="Seg_8787" s="T446">sein-PST1-3PL</ta>
            <ta e="T448" id="Seg_8788" s="T447">groß</ta>
            <ta e="T449" id="Seg_8789" s="T448">Teufel-DAT/LOC</ta>
            <ta e="T450" id="Seg_8790" s="T449">immer</ta>
            <ta e="T451" id="Seg_8791" s="T450">Geschenk-VBZ-PTCP.PRS</ta>
            <ta e="T452" id="Seg_8792" s="T451">Gewohnheit-PROPR.[NOM]</ta>
            <ta e="T453" id="Seg_8793" s="T452">sein-PST1-3PL</ta>
            <ta e="T454" id="Seg_8794" s="T453">jeder</ta>
            <ta e="T455" id="Seg_8795" s="T454">Verwandter.[NOM]</ta>
            <ta e="T456" id="Seg_8796" s="T455">gut-ADVZ</ta>
            <ta e="T457" id="Seg_8797" s="T456">leben-PTCP.FUT-3PL-ACC</ta>
            <ta e="T458" id="Seg_8798" s="T457">wollen-PRS-3PL</ta>
            <ta e="T459" id="Seg_8799" s="T458">was-EP-INSTR</ta>
            <ta e="T460" id="Seg_8800" s="T459">NEG</ta>
            <ta e="T461" id="Seg_8801" s="T460">bedürfen-NEG.CVB.SIM</ta>
            <ta e="T462" id="Seg_8802" s="T461">krank.sein-EP-NEG.CVB.SIM</ta>
            <ta e="T463" id="Seg_8803" s="T462">groß</ta>
            <ta e="T464" id="Seg_8804" s="T463">Schamane-PL.[NOM]</ta>
            <ta e="T465" id="Seg_8805" s="T464">Mensch.[NOM]</ta>
            <ta e="T466" id="Seg_8806" s="T465">gehen-PTCP.PRS</ta>
            <ta e="T467" id="Seg_8807" s="T466">Weg-3SG-ACC</ta>
            <ta e="T468" id="Seg_8808" s="T467">wissen-PTCP.PRS-3PL.[NOM]</ta>
            <ta e="T469" id="Seg_8809" s="T468">sehr</ta>
            <ta e="T470" id="Seg_8810" s="T469">sein-PST1-3SG</ta>
            <ta e="T471" id="Seg_8811" s="T470">dieses</ta>
            <ta e="T472" id="Seg_8812" s="T471">Aufenthaltsort-1PL-ABL</ta>
            <ta e="T473" id="Seg_8813" s="T472">fern-POSS</ta>
            <ta e="T474" id="Seg_8814" s="T473">NEG</ta>
            <ta e="T475" id="Seg_8815" s="T474">es.gibt</ta>
            <ta e="T476" id="Seg_8816" s="T475">groß</ta>
            <ta e="T477" id="Seg_8817" s="T476">Teufel-Stein.[NOM]</ta>
            <ta e="T478" id="Seg_8818" s="T477">jeder</ta>
            <ta e="T479" id="Seg_8819" s="T478">Mensch.[NOM]</ta>
            <ta e="T480" id="Seg_8820" s="T479">dort</ta>
            <ta e="T481" id="Seg_8821" s="T480">Geschenk-ACC</ta>
            <ta e="T482" id="Seg_8822" s="T481">geben-HAB.[3SG]</ta>
            <ta e="T483" id="Seg_8823" s="T482">bald</ta>
            <ta e="T484" id="Seg_8824" s="T483">Kind-PL-1PL.[NOM]</ta>
            <ta e="T485" id="Seg_8825" s="T484">fremd</ta>
            <ta e="T486" id="Seg_8826" s="T485">Ort-DAT/LOC</ta>
            <ta e="T487" id="Seg_8827" s="T486">gehen-PRS-3PL</ta>
            <ta e="T488" id="Seg_8828" s="T487">fremd</ta>
            <ta e="T489" id="Seg_8829" s="T488">Mensch-PL-DAT/LOC</ta>
            <ta e="T490" id="Seg_8830" s="T489">vom.Stamm-3SG.[NOM]</ta>
            <ta e="T491" id="Seg_8831" s="T490">Gast-3SG-GEN</ta>
            <ta e="T492" id="Seg_8832" s="T491">Erzählung-3SG-ACC</ta>
            <ta e="T493" id="Seg_8833" s="T492">zuhören-CVB.SIM-zuhören-CVB.SIM</ta>
            <ta e="T494" id="Seg_8834" s="T493">Inneres-3SG-DAT/LOC</ta>
            <ta e="T495" id="Seg_8835" s="T494">sagen-CVB.SIM</ta>
            <ta e="T496" id="Seg_8836" s="T495">denken-PRS.[3SG]</ta>
            <ta e="T497" id="Seg_8837" s="T496">was</ta>
            <ta e="T498" id="Seg_8838" s="T497">Gedanke-3SG-INSTR</ta>
            <ta e="T499" id="Seg_8839" s="T498">solch</ta>
            <ta e="T500" id="Seg_8840" s="T499">Erzählung-ACC</ta>
            <ta e="T501" id="Seg_8841" s="T500">herausnehmen-PRS.[3SG]</ta>
            <ta e="T502" id="Seg_8842" s="T501">Gast-3SG.[NOM]</ta>
            <ta e="T503" id="Seg_8843" s="T502">eilen-NEG.CVB.SIM</ta>
            <ta e="T504" id="Seg_8844" s="T503">sprechen-ITER-CVB.SEQ</ta>
            <ta e="T505" id="Seg_8845" s="T504">gehen-PST2.[3SG]</ta>
            <ta e="T506" id="Seg_8846" s="T505">Frau-PL.[NOM]</ta>
            <ta e="T507" id="Seg_8847" s="T506">Unglück-PROPR-PL</ta>
            <ta e="T508" id="Seg_8848" s="T507">sehr</ta>
            <ta e="T509" id="Seg_8849" s="T508">betrübt.sein-PST1-3PL</ta>
            <ta e="T510" id="Seg_8850" s="T509">weinen-EP-RECP/COLL-PRS-3PL</ta>
            <ta e="T511" id="Seg_8851" s="T510">klein</ta>
            <ta e="T512" id="Seg_8852" s="T511">Kind-ACC</ta>
            <ta e="T513" id="Seg_8853" s="T512">Zelt-ABL</ta>
            <ta e="T514" id="Seg_8854" s="T513">wegnehmen-PTCP.PRS</ta>
            <ta e="T515" id="Seg_8855" s="T514">Angelegenheit.[NOM]</ta>
            <ta e="T516" id="Seg_8856" s="T515">sehr</ta>
            <ta e="T517" id="Seg_8857" s="T516">Sünde-PROPR.[NOM]</ta>
            <ta e="T518" id="Seg_8858" s="T517">Leute.[NOM]</ta>
            <ta e="T519" id="Seg_8859" s="T518">sehr</ta>
            <ta e="T520" id="Seg_8860" s="T519">traurig.sein-PRS.[3SG]</ta>
            <ta e="T521" id="Seg_8861" s="T520">wohl</ta>
            <ta e="T522" id="Seg_8862" s="T521">Teufel-DAT/LOC</ta>
            <ta e="T523" id="Seg_8863" s="T522">Geschenk.[NOM]</ta>
            <ta e="T524" id="Seg_8864" s="T523">geben-PTCP.FUT-DAT/LOC</ta>
            <ta e="T525" id="Seg_8865" s="T524">doch</ta>
            <ta e="T526" id="Seg_8866" s="T525">alter.Mann.[NOM]</ta>
            <ta e="T527" id="Seg_8867" s="T526">1SG-ACC</ta>
            <ta e="T528" id="Seg_8868" s="T527">spucken-NEG.[IMP.2SG]</ta>
            <ta e="T529" id="Seg_8869" s="T528">dieses-ACC</ta>
            <ta e="T530" id="Seg_8870" s="T529">machen-FUT-1PL</ta>
            <ta e="T531" id="Seg_8871" s="T530">NEG-3SG</ta>
            <ta e="T532" id="Seg_8872" s="T531">Schule.[NOM]</ta>
            <ta e="T533" id="Seg_8873" s="T532">böser.Geist-ACC</ta>
            <ta e="T534" id="Seg_8874" s="T533">bitten-NEG.[3SG]</ta>
            <ta e="T535" id="Seg_8875" s="T534">so</ta>
            <ta e="T536" id="Seg_8876" s="T535">jeder</ta>
            <ta e="T537" id="Seg_8877" s="T536">Mensch-DAT/LOC</ta>
            <ta e="T538" id="Seg_8878" s="T537">erzählen.[IMP.2SG]</ta>
            <ta e="T539" id="Seg_8879" s="T538">traurig.sein-NEG-IMP.3PL</ta>
            <ta e="T540" id="Seg_8880" s="T539">sagen-PST2.[3SG]</ta>
            <ta e="T541" id="Seg_8881" s="T540">vom.Stamm-3SG.[NOM]</ta>
            <ta e="T542" id="Seg_8882" s="T541">alter.Mann.[NOM]</ta>
            <ta e="T543" id="Seg_8883" s="T542">was.[NOM]</ta>
            <ta e="T544" id="Seg_8884" s="T543">NEG</ta>
            <ta e="T545" id="Seg_8885" s="T544">sagen-NEG.CVB.SIM</ta>
            <ta e="T546" id="Seg_8886" s="T545">Tasse-3SG-DAT/LOC</ta>
            <ta e="T547" id="Seg_8887" s="T546">Tee.[NOM]</ta>
            <ta e="T548" id="Seg_8888" s="T547">gießen-EP-MED-PST2.[3SG]</ta>
            <ta e="T549" id="Seg_8889" s="T548">1PL.[NOM]</ta>
            <ta e="T550" id="Seg_8890" s="T549">alter.Mann-PL.[NOM]</ta>
            <ta e="T551" id="Seg_8891" s="T550">wie</ta>
            <ta e="T552" id="Seg_8892" s="T551">gut.[NOM]</ta>
            <ta e="T553" id="Seg_8893" s="T552">sein-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T554" id="Seg_8894" s="T553">denken-CVB.SEQ</ta>
            <ta e="T555" id="Seg_8895" s="T554">Rat.[NOM]</ta>
            <ta e="T556" id="Seg_8896" s="T555">geben-PRS-1PL</ta>
            <ta e="T557" id="Seg_8897" s="T556">Wahrheit.[NOM]</ta>
            <ta e="T558" id="Seg_8898" s="T557">(irgend)ein</ta>
            <ta e="T559" id="Seg_8899" s="T558">nachdem</ta>
            <ta e="T560" id="Seg_8900" s="T559">Fehler.[NOM]</ta>
            <ta e="T561" id="Seg_8901" s="T560">sprechen-PRS-1PL</ta>
            <ta e="T562" id="Seg_8902" s="T561">sein-FUT-3SG</ta>
            <ta e="T563" id="Seg_8903" s="T562">Frau-PL.[NOM]</ta>
            <ta e="T564" id="Seg_8904" s="T563">weinen-EP-MED-PTCP.PRS-3PL.[3SG]</ta>
            <ta e="T565" id="Seg_8905" s="T564">kräftig.[NOM]</ta>
            <ta e="T566" id="Seg_8906" s="T565">Mensch.[NOM]</ta>
            <ta e="T567" id="Seg_8907" s="T566">Ausbildung-3SG-ACC</ta>
            <ta e="T569" id="Seg_8908" s="T568">Mensch.[NOM]</ta>
            <ta e="T570" id="Seg_8909" s="T569">Ausbildung-3SG-ACC</ta>
            <ta e="T571" id="Seg_8910" s="T570">sich.kümmern-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T572" id="Seg_8911" s="T571">sehr</ta>
            <ta e="T573" id="Seg_8912" s="T572">sehr</ta>
            <ta e="T574" id="Seg_8913" s="T573">sehr</ta>
            <ta e="T575" id="Seg_8914" s="T574">klein</ta>
            <ta e="T576" id="Seg_8915" s="T575">Kind-PL-DAT/LOC</ta>
            <ta e="T577" id="Seg_8916" s="T576">groß</ta>
            <ta e="T578" id="Seg_8917" s="T577">Leute-ACC</ta>
            <ta e="T579" id="Seg_8918" s="T578">verbreiten-PTCP.FUT-DAT/LOC</ta>
            <ta e="T580" id="Seg_8919" s="T579">direkt</ta>
            <ta e="T581" id="Seg_8920" s="T580">sehen-PTCP.PRS</ta>
            <ta e="T582" id="Seg_8921" s="T581">sein-PTCP.FUT-3PL-ACC</ta>
            <ta e="T583" id="Seg_8922" s="T582">sehr</ta>
            <ta e="T584" id="Seg_8923" s="T583">klein</ta>
            <ta e="T585" id="Seg_8924" s="T584">Kind.[NOM]</ta>
            <ta e="T586" id="Seg_8925" s="T585">Rentierkalb.[NOM]</ta>
            <ta e="T587" id="Seg_8926" s="T586">ähnlich</ta>
            <ta e="T588" id="Seg_8927" s="T587">wo</ta>
            <ta e="T589" id="Seg_8928" s="T588">gehen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T590" id="Seg_8929" s="T589">wissen-NEG.[3SG]</ta>
            <ta e="T591" id="Seg_8930" s="T590">gut-ADVZ</ta>
            <ta e="T592" id="Seg_8931" s="T591">sich.anziehen-CVB.SIM</ta>
            <ta e="T593" id="Seg_8932" s="T592">können-NEG.[3SG]</ta>
            <ta e="T594" id="Seg_8933" s="T593">dieses.EMPH.[NOM]</ta>
            <ta e="T595" id="Seg_8934" s="T594">Wahrheit.[NOM]</ta>
            <ta e="T596" id="Seg_8935" s="T595">sagen-PST2.[3SG]</ta>
            <ta e="T597" id="Seg_8936" s="T596">vom.Stamm-3SG.[NOM]</ta>
            <ta e="T598" id="Seg_8937" s="T597">alter.Mann.[NOM]</ta>
            <ta e="T599" id="Seg_8938" s="T598">dieses</ta>
            <ta e="T600" id="Seg_8939" s="T599">Versammlung.[NOM]</ta>
            <ta e="T601" id="Seg_8940" s="T600">Lärm-3SG-DAT/LOC</ta>
            <ta e="T602" id="Seg_8941" s="T601">vergessen-CVB.SEQ</ta>
            <ta e="T603" id="Seg_8942" s="T602">werfen-PST2.[3SG]</ta>
            <ta e="T604" id="Seg_8943" s="T603">im.Winter</ta>
            <ta e="T605" id="Seg_8944" s="T604">neu</ta>
            <ta e="T606" id="Seg_8945" s="T605">Jahr.[NOM]</ta>
            <ta e="T607" id="Seg_8946" s="T606">kommen-PTCP.PRS</ta>
            <ta e="T608" id="Seg_8947" s="T607">Zeit-3SG-DAT/LOC</ta>
            <ta e="T609" id="Seg_8948" s="T608">Kind-PL.[NOM]</ta>
            <ta e="T610" id="Seg_8949" s="T609">sich.ausruhen-CVB.SIM</ta>
            <ta e="T611" id="Seg_8950" s="T610">kommen-FUT-3PL</ta>
            <ta e="T612" id="Seg_8951" s="T611">Haus-3PL-DAT/LOC</ta>
            <ta e="T613" id="Seg_8952" s="T612">Frühling.[NOM]</ta>
            <ta e="T614" id="Seg_8953" s="T613">Mai</ta>
            <ta e="T615" id="Seg_8954" s="T614">Monat-3SG-DAT/LOC</ta>
            <ta e="T616" id="Seg_8955" s="T615">eins</ta>
            <ta e="T617" id="Seg_8956" s="T616">Jahr-ADJZ</ta>
            <ta e="T618" id="Seg_8957" s="T617">Ausbildung-3PL-ACC</ta>
            <ta e="T619" id="Seg_8958" s="T618">aufhören-TEMP-3PL</ta>
            <ta e="T620" id="Seg_8959" s="T619">Herbst-DAT/LOC</ta>
            <ta e="T621" id="Seg_8960" s="T620">bis.zu</ta>
            <ta e="T622" id="Seg_8961" s="T621">Haus-3PL-DAT/LOC</ta>
            <ta e="T623" id="Seg_8962" s="T622">sein-FUT-3PL</ta>
            <ta e="T624" id="Seg_8963" s="T623">dieses</ta>
            <ta e="T625" id="Seg_8964" s="T624">Neuigkeit-ACC</ta>
            <ta e="T626" id="Seg_8965" s="T625">1SG.[NOM]</ta>
            <ta e="T627" id="Seg_8966" s="T626">jeder</ta>
            <ta e="T628" id="Seg_8967" s="T627">Mensch-DAT/LOC</ta>
            <ta e="T629" id="Seg_8968" s="T628">erzählen-FUT-1SG</ta>
            <ta e="T630" id="Seg_8969" s="T629">Mensch-PL.[NOM]</ta>
            <ta e="T631" id="Seg_8970" s="T630">sagen-CVB.SIM</ta>
            <ta e="T632" id="Seg_8971" s="T631">denken-PRS-3PL</ta>
            <ta e="T633" id="Seg_8972" s="T632">wann</ta>
            <ta e="T634" id="Seg_8973" s="T633">NEG</ta>
            <ta e="T635" id="Seg_8974" s="T634">zurückkommen-EP-CAUS-NEG.CVB.SIM</ta>
            <ta e="T636" id="Seg_8975" s="T635">nehmen-PRS-3PL</ta>
            <ta e="T637" id="Seg_8976" s="T636">Kind-PL-ACC</ta>
            <ta e="T638" id="Seg_8977" s="T637">sich.freuen-CVB.COND</ta>
            <ta e="T639" id="Seg_8978" s="T638">alter.Mann.[NOM]</ta>
            <ta e="T640" id="Seg_8979" s="T639">sprechen-PST2.[3SG]</ta>
            <ta e="T641" id="Seg_8980" s="T640">alter.Mann.[NOM]</ta>
            <ta e="T642" id="Seg_8981" s="T641">Haus-3SG-DAT/LOC</ta>
            <ta e="T643" id="Seg_8982" s="T642">gehen-PTCP.PST-3SG-ACC</ta>
            <ta e="T644" id="Seg_8983" s="T643">nachdem</ta>
            <ta e="T645" id="Seg_8984" s="T644">Anisim.[NOM]</ta>
            <ta e="T646" id="Seg_8985" s="T645">selbst-3SG-ACC</ta>
            <ta e="T647" id="Seg_8986" s="T646">mit</ta>
            <ta e="T648" id="Seg_8987" s="T647">sich.unterhalten-PRS.[3SG]</ta>
            <ta e="T649" id="Seg_8988" s="T648">jeder</ta>
            <ta e="T650" id="Seg_8989" s="T649">Mensch-DAT/LOC</ta>
            <ta e="T651" id="Seg_8990" s="T650">erinnern-EP-CAUS-CVB.SEQ</ta>
            <ta e="T652" id="Seg_8991" s="T651">gehen-PTCP.FUT-DAT/LOC</ta>
            <ta e="T653" id="Seg_8992" s="T652">man.muss</ta>
            <ta e="T654" id="Seg_8993" s="T653">wie</ta>
            <ta e="T655" id="Seg_8994" s="T654">neu</ta>
            <ta e="T656" id="Seg_8995" s="T655">Gesetz.[NOM]</ta>
            <ta e="T657" id="Seg_8996" s="T656">gut-ACC</ta>
            <ta e="T658" id="Seg_8997" s="T657">machen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T659" id="Seg_8998" s="T658">Tundra.[NOM]</ta>
            <ta e="T660" id="Seg_8999" s="T659">Volk-3SG-GEN</ta>
            <ta e="T661" id="Seg_9000" s="T660">Vorderteil-3SG-DAT/LOC</ta>
            <ta e="T662" id="Seg_9001" s="T661">solch</ta>
            <ta e="T663" id="Seg_9002" s="T662">alter.Mann-PL.[NOM]</ta>
            <ta e="T664" id="Seg_9003" s="T663">Neuigkeit.[NOM]</ta>
            <ta e="T665" id="Seg_9004" s="T664">schicken-PTCP.PRS-3PL</ta>
            <ta e="T666" id="Seg_9005" s="T665">Wahrheit-PROPR.[NOM]</ta>
            <ta e="T667" id="Seg_9006" s="T666">stark-PROPR.[NOM]</ta>
            <ta e="T668" id="Seg_9007" s="T667">sein-FUT-3SG</ta>
            <ta e="T669" id="Seg_9008" s="T668">nächster</ta>
            <ta e="T670" id="Seg_9009" s="T669">Tag.[NOM]</ta>
            <ta e="T671" id="Seg_9010" s="T670">wieder</ta>
            <ta e="T672" id="Seg_9011" s="T671">Versammlung-DAT/LOC</ta>
            <ta e="T673" id="Seg_9012" s="T672">Mensch-PL.[NOM]</ta>
            <ta e="T674" id="Seg_9013" s="T673">sammeln-PASS/REFL-EP-PST2-3PL</ta>
            <ta e="T676" id="Seg_9014" s="T675">Semjon</ta>
            <ta e="T677" id="Seg_9015" s="T676">alter.Mann.[NOM]</ta>
            <ta e="T678" id="Seg_9016" s="T677">Zelt-3SG-DAT/LOC</ta>
            <ta e="T679" id="Seg_9017" s="T678">Leute.[NOM]</ta>
            <ta e="T680" id="Seg_9018" s="T679">gesteren-ADJZ-COMP</ta>
            <ta e="T681" id="Seg_9019" s="T680">wie</ta>
            <ta e="T682" id="Seg_9020" s="T681">INDEF</ta>
            <ta e="T683" id="Seg_9021" s="T682">Stimmung-3PL.[NOM]</ta>
            <ta e="T684" id="Seg_9022" s="T683">heben-EP-PASS/REFL-EP-PST2.[3SG]</ta>
            <ta e="T685" id="Seg_9023" s="T684">Lärm.[NOM]</ta>
            <ta e="T686" id="Seg_9024" s="T685">lachen-RECP/COLL-NMNZ.[NOM]-lachen-RECP/COLL-NMNZ.[NOM]</ta>
            <ta e="T687" id="Seg_9025" s="T686">aufhören-CVB.ANT-3SG</ta>
            <ta e="T688" id="Seg_9026" s="T687">vom.Stamm-3PL.[NOM]</ta>
            <ta e="T689" id="Seg_9027" s="T688">sagen-PST2.[3SG]</ta>
            <ta e="T690" id="Seg_9028" s="T689">selbst-2PL.[NOM]</ta>
            <ta e="T691" id="Seg_9029" s="T690">Wunsch-2PL-ABL</ta>
            <ta e="T692" id="Seg_9030" s="T691">klein</ta>
            <ta e="T693" id="Seg_9031" s="T692">Volk-PL-ACC</ta>
            <ta e="T694" id="Seg_9032" s="T693">halten-CAUS-EP-IMP.2PL</ta>
            <ta e="T695" id="Seg_9033" s="T694">groß</ta>
            <ta e="T696" id="Seg_9034" s="T695">Kind-PL-DAT/LOC</ta>
            <ta e="T697" id="Seg_9035" s="T696">wegfahren-PTCP.PRS</ta>
            <ta e="T698" id="Seg_9036" s="T697">Zeit-DAT/LOC</ta>
            <ta e="T699" id="Seg_9037" s="T698">sehen-PTCP.PRS</ta>
            <ta e="T700" id="Seg_9038" s="T699">sein-PTCP.FUT-3PL-ACC</ta>
            <ta e="T701" id="Seg_9039" s="T700">3PL-ACC</ta>
            <ta e="T702" id="Seg_9040" s="T701">1PL-DAT/LOC</ta>
            <ta e="T703" id="Seg_9041" s="T702">sagen-PST2-3PL</ta>
            <ta e="T704" id="Seg_9042" s="T703">Kind-PL-1PL.[NOM]</ta>
            <ta e="T705" id="Seg_9043" s="T704">im.Winter</ta>
            <ta e="T706" id="Seg_9044" s="T705">sich.ausruhen-CVB.SIM</ta>
            <ta e="T707" id="Seg_9045" s="T706">kommen-FUT-3PL</ta>
            <ta e="T708" id="Seg_9046" s="T707">dieses.[NOM]</ta>
            <ta e="T709" id="Seg_9047" s="T708">Wahrheit.[NOM]</ta>
            <ta e="T710" id="Seg_9048" s="T709">Wort.[NOM]</ta>
            <ta e="T711" id="Seg_9049" s="T710">Q</ta>
            <ta e="T712" id="Seg_9050" s="T711">fragen-PST2.[3SG]</ta>
            <ta e="T713" id="Seg_9051" s="T712">eins</ta>
            <ta e="T714" id="Seg_9052" s="T713">Frau.[NOM]</ta>
            <ta e="T715" id="Seg_9053" s="T714">Semjon</ta>
            <ta e="T716" id="Seg_9054" s="T715">alter.Mann.[NOM]</ta>
            <ta e="T717" id="Seg_9055" s="T716">sitzen-PTCP.PRS</ta>
            <ta e="T718" id="Seg_9056" s="T717">Ort-3SG-ABL</ta>
            <ta e="T719" id="Seg_9057" s="T718">überlegen-EP</ta>
            <ta e="T720" id="Seg_9058" s="T719">überlegen-EP-ITER-PST2.[3SG]</ta>
            <ta e="T721" id="Seg_9059" s="T720">dieses.[NOM]</ta>
            <ta e="T722" id="Seg_9060" s="T721">Wahrheit.[NOM]</ta>
            <ta e="T723" id="Seg_9061" s="T722">Neuigkeit.[NOM]</ta>
            <ta e="T724" id="Seg_9062" s="T723">im.Winter</ta>
            <ta e="T725" id="Seg_9063" s="T724">sich.ausruhen-CVB.SIM</ta>
            <ta e="T726" id="Seg_9064" s="T725">kommen-FUT-3PL</ta>
            <ta e="T727" id="Seg_9065" s="T726">dann</ta>
            <ta e="T728" id="Seg_9066" s="T727">im.Sommer</ta>
            <ta e="T729" id="Seg_9067" s="T728">direkt</ta>
            <ta e="T730" id="Seg_9068" s="T729">Haus-3PL-DAT/LOC</ta>
            <ta e="T731" id="Seg_9069" s="T730">sein-CVB.SIM</ta>
            <ta e="T732" id="Seg_9070" s="T731">kommen-FUT-3PL</ta>
            <ta e="T733" id="Seg_9071" s="T732">sagen-PST2.[3SG]</ta>
            <ta e="T734" id="Seg_9072" s="T733">Anisim.[NOM]</ta>
            <ta e="T735" id="Seg_9073" s="T734">Semjon</ta>
            <ta e="T736" id="Seg_9074" s="T735">alter.Mann.[NOM]</ta>
            <ta e="T737" id="Seg_9075" s="T736">sich.entspannen-CVB.SIM</ta>
            <ta e="T738" id="Seg_9076" s="T737">fallen-CVB.ANT</ta>
            <ta e="T739" id="Seg_9077" s="T738">Wort.[NOM]</ta>
            <ta e="T742" id="Seg_9078" s="T741">auftreten-EP-PST2.[3SG]</ta>
            <ta e="T743" id="Seg_9079" s="T742">1SG.[NOM]</ta>
            <ta e="T744" id="Seg_9080" s="T743">was-ACC</ta>
            <ta e="T745" id="Seg_9081" s="T744">sagen-PST2-EP-1SG=Q</ta>
            <ta e="T746" id="Seg_9082" s="T745">jeder-3PL.[NOM]</ta>
            <ta e="T747" id="Seg_9083" s="T746">warm</ta>
            <ta e="T748" id="Seg_9084" s="T747">Auge-EP-INSTR</ta>
            <ta e="T749" id="Seg_9085" s="T748">sehen-CVB.SEQ</ta>
            <ta e="T750" id="Seg_9086" s="T749">Semjon</ta>
            <ta e="T751" id="Seg_9087" s="T750">alter.Mann.[NOM]</ta>
            <ta e="T752" id="Seg_9088" s="T751">zu</ta>
            <ta e="T753" id="Seg_9089" s="T752">sich.drehen-PASS/REFL-EP-PST2-3PL</ta>
            <ta e="T754" id="Seg_9090" s="T753">Versammlung-3PL.[NOM]</ta>
            <ta e="T755" id="Seg_9091" s="T754">vorbeigehen-PST2.[3SG]</ta>
            <ta e="T756" id="Seg_9092" s="T755">schnell-ADVZ</ta>
            <ta e="T757" id="Seg_9093" s="T756">Mensch-PL.[NOM]</ta>
            <ta e="T758" id="Seg_9094" s="T757">Stange.[NOM]</ta>
            <ta e="T759" id="Seg_9095" s="T758">Zelt-3PL-ACC</ta>
            <ta e="T760" id="Seg_9096" s="T759">jeder</ta>
            <ta e="T761" id="Seg_9097" s="T760">gehen-MULT-PST2-3PL</ta>
            <ta e="T762" id="Seg_9098" s="T761">nächster</ta>
            <ta e="T763" id="Seg_9099" s="T762">Herbst.[NOM]</ta>
            <ta e="T764" id="Seg_9100" s="T763">Herde-3PL-ACC</ta>
            <ta e="T765" id="Seg_9101" s="T764">%%-CVB.SEQ</ta>
            <ta e="T766" id="Seg_9102" s="T765">nachdem</ta>
            <ta e="T767" id="Seg_9103" s="T766">Rentier.[NOM]</ta>
            <ta e="T768" id="Seg_9104" s="T767">greifen-NMNZ-3SG.[NOM]</ta>
            <ta e="T769" id="Seg_9105" s="T768">werden-PST2.[3SG]</ta>
            <ta e="T770" id="Seg_9106" s="T769">wegfahren-PTCP.PRS-DAT/LOC</ta>
            <ta e="T771" id="Seg_9107" s="T770">Frau-PL.[NOM]</ta>
            <ta e="T772" id="Seg_9108" s="T771">Zelt-3PL-GEN</ta>
            <ta e="T773" id="Seg_9109" s="T772">Platz.neben-DAT/LOC</ta>
            <ta e="T774" id="Seg_9110" s="T773">Kind-PL-3SG-ACC</ta>
            <ta e="T775" id="Seg_9111" s="T774">mit</ta>
            <ta e="T776" id="Seg_9112" s="T775">Freizeit-POSS</ta>
            <ta e="T777" id="Seg_9113" s="T776">NEG-3PL</ta>
            <ta e="T778" id="Seg_9114" s="T777">sammeln-REFL-CVB.SEQ-3PL</ta>
            <ta e="T779" id="Seg_9115" s="T778">Kind-PL-3SG-DAT/LOC</ta>
            <ta e="T780" id="Seg_9116" s="T779">direkt</ta>
            <ta e="T781" id="Seg_9117" s="T780">sprechen-PRS-3PL</ta>
            <ta e="T782" id="Seg_9118" s="T781">sich.bewegen-NEG.PTCP</ta>
            <ta e="T783" id="Seg_9119" s="T782">sein-PTCP.FUT-3PL-ACC</ta>
            <ta e="T784" id="Seg_9120" s="T783">groß</ta>
            <ta e="T785" id="Seg_9121" s="T784">Kind-PL-ACC</ta>
            <ta e="T786" id="Seg_9122" s="T785">hören-PTCP.PRS</ta>
            <ta e="T787" id="Seg_9123" s="T786">sein-PTCP.FUT-3PL-ACC</ta>
            <ta e="T788" id="Seg_9124" s="T787">Rentier.[NOM]</ta>
            <ta e="T789" id="Seg_9125" s="T788">halten-CVB.ANT</ta>
            <ta e="T790" id="Seg_9126" s="T789">einspannen-CVB.ANT</ta>
            <ta e="T791" id="Seg_9127" s="T790">Stange.[NOM]</ta>
            <ta e="T792" id="Seg_9128" s="T791">Zelt.[NOM]</ta>
            <ta e="T793" id="Seg_9129" s="T792">jeder</ta>
            <ta e="T794" id="Seg_9130" s="T793">essen-PRS-3PL</ta>
            <ta e="T795" id="Seg_9131" s="T794">wegfahren-PTCP.PRS</ta>
            <ta e="T796" id="Seg_9132" s="T795">Vorderteil-3SG-DAT/LOC</ta>
            <ta e="T797" id="Seg_9133" s="T796">dann</ta>
            <ta e="T798" id="Seg_9134" s="T797">Stange.[NOM]</ta>
            <ta e="T799" id="Seg_9135" s="T798">Zelt.[NOM]</ta>
            <ta e="T800" id="Seg_9136" s="T799">jeder-ABL</ta>
            <ta e="T801" id="Seg_9137" s="T800">jeder-3PL.[NOM]</ta>
            <ta e="T802" id="Seg_9138" s="T801">aufflammen-RECP/COLL-CVB.SEQ</ta>
            <ta e="T803" id="Seg_9139" s="T802">hinausgehen-EP-PST2-3PL</ta>
            <ta e="T804" id="Seg_9140" s="T803">fern.[NOM]</ta>
            <ta e="T805" id="Seg_9141" s="T804">gehen-PTCP.PRS</ta>
            <ta e="T806" id="Seg_9142" s="T805">Kind-PL-ACC</ta>
            <ta e="T807" id="Seg_9143" s="T806">begleiten-CVB.PURP</ta>
            <ta e="T808" id="Seg_9144" s="T807">wann</ta>
            <ta e="T809" id="Seg_9145" s="T808">1SG.[NOM]</ta>
            <ta e="T810" id="Seg_9146" s="T809">2SG-ACC</ta>
            <ta e="T811" id="Seg_9147" s="T810">sehen-FUT-1SG=Q</ta>
            <ta e="T812" id="Seg_9148" s="T811">Jevgenija.[NOM]</ta>
            <ta e="T813" id="Seg_9149" s="T812">weinen-PST2.[3SG]</ta>
            <ta e="T814" id="Seg_9150" s="T813">wie</ta>
            <ta e="T815" id="Seg_9151" s="T814">2SG-ACC</ta>
            <ta e="T818" id="Seg_9152" s="T817">2SG-ACC-POSS</ta>
            <ta e="T819" id="Seg_9153" s="T818">NEG</ta>
            <ta e="T820" id="Seg_9154" s="T819">leben-FUT-1SG=Q</ta>
            <ta e="T821" id="Seg_9155" s="T820">wer.[NOM]</ta>
            <ta e="T822" id="Seg_9156" s="T821">1PL-DAT/LOC</ta>
            <ta e="T823" id="Seg_9157" s="T822">helfen-FUT-3SG=Q</ta>
            <ta e="T824" id="Seg_9158" s="T823">Kind-3SG-ACC</ta>
            <ta e="T825" id="Seg_9159" s="T824">heftig</ta>
            <ta e="T826" id="Seg_9160" s="T825">greifen-CVB.ANT</ta>
            <ta e="T827" id="Seg_9161" s="T826">nachdem</ta>
            <ta e="T828" id="Seg_9162" s="T827">Zelt-3SG-ACC</ta>
            <ta e="T829" id="Seg_9163" s="T828">zu</ta>
            <ta e="T830" id="Seg_9164" s="T829">ziehen-PRS.[3SG]</ta>
            <ta e="T831" id="Seg_9165" s="T830">2SG.[NOM]</ta>
            <ta e="T832" id="Seg_9166" s="T831">was.[NOM]</ta>
            <ta e="T833" id="Seg_9167" s="T832">werden-PRS-2SG</ta>
            <ta e="T834" id="Seg_9168" s="T833">eins</ta>
            <ta e="T835" id="Seg_9169" s="T834">eins</ta>
            <ta e="T836" id="Seg_9170" s="T835">Frau.[NOM]</ta>
            <ta e="T837" id="Seg_9171" s="T836">sagen-PST2.[3SG]</ta>
            <ta e="T838" id="Seg_9172" s="T837">Freund-3SG-DAT/LOC</ta>
            <ta e="T839" id="Seg_9173" s="T838">Kind-ACC</ta>
            <ta e="T840" id="Seg_9174" s="T839">denken-PTCP.PRS-DAT/LOC</ta>
            <ta e="T841" id="Seg_9175" s="T840">herunterziehen-EP-NEG.[IMP.2SG]</ta>
            <ta e="T842" id="Seg_9176" s="T841">wegfahren-PTCP.PRS.[NOM]</ta>
            <ta e="T843" id="Seg_9177" s="T842">Vorderteil-3SG-DAT/LOC</ta>
            <ta e="T844" id="Seg_9178" s="T843">solch</ta>
            <ta e="T845" id="Seg_9179" s="T844">begleiten-PTCP.PRS.[NOM]</ta>
            <ta e="T846" id="Seg_9180" s="T845">sehr</ta>
            <ta e="T847" id="Seg_9181" s="T846">schlecht.[NOM]</ta>
            <ta e="T848" id="Seg_9182" s="T847">Sünde.[NOM]</ta>
            <ta e="T849" id="Seg_9183" s="T848">begleiten.[IMP.2SG]</ta>
            <ta e="T850" id="Seg_9184" s="T849">Mensch-SIM</ta>
            <ta e="T851" id="Seg_9185" s="T850">jeder</ta>
            <ta e="T852" id="Seg_9186" s="T851">Reitrentier-PROPR</ta>
            <ta e="T853" id="Seg_9187" s="T852">Kind-PL.[NOM]</ta>
            <ta e="T854" id="Seg_9188" s="T853">Großvater-3PL-GEN</ta>
            <ta e="T855" id="Seg_9189" s="T854">Hinterteil-3SG-ABL</ta>
            <ta e="T856" id="Seg_9190" s="T855">gehen-CVB.SEQ</ta>
            <ta e="T857" id="Seg_9191" s="T856">lang</ta>
            <ta e="T858" id="Seg_9192" s="T857">Rentierkarawane.[NOM]</ta>
            <ta e="T859" id="Seg_9193" s="T858">werden-PST2-3PL</ta>
            <ta e="T860" id="Seg_9194" s="T859">stehen-NEG.PTCP-stehen-PTCP.PRS</ta>
            <ta e="T861" id="Seg_9195" s="T860">Kind-3PL-ACC</ta>
            <ta e="T862" id="Seg_9196" s="T861">begleiten-CVB.SEQ</ta>
            <ta e="T863" id="Seg_9197" s="T862">jenes</ta>
            <ta e="T864" id="Seg_9198" s="T863">Rentierkarawane.[NOM]</ta>
            <ta e="T865" id="Seg_9199" s="T864">Hinterteil-3SG-ABL</ta>
            <ta e="T866" id="Seg_9200" s="T865">sich.ausstrecken-PST2-3PL</ta>
            <ta e="T867" id="Seg_9201" s="T866">Porfirij</ta>
            <ta e="T868" id="Seg_9202" s="T867">alter.Mann.[NOM]</ta>
            <ta e="T869" id="Seg_9203" s="T868">Auge-PL-3SG-ACC</ta>
            <ta e="T870" id="Seg_9204" s="T869">herauskommen-ADVZ</ta>
            <ta e="T871" id="Seg_9205" s="T870">sehen-CVB.SEQ</ta>
            <ta e="T872" id="Seg_9206" s="T871">aufstehen-CVB.SEQ</ta>
            <ta e="T873" id="Seg_9207" s="T872">letzter</ta>
            <ta e="T874" id="Seg_9208" s="T873">Kraft-3SG-INSTR</ta>
            <ta e="T875" id="Seg_9209" s="T874">beglücken-CVB.SIM</ta>
            <ta e="T876" id="Seg_9210" s="T875">stehen-PST2.[3SG]</ta>
            <ta e="T877" id="Seg_9211" s="T876">doch</ta>
            <ta e="T878" id="Seg_9212" s="T877">glücklich</ta>
            <ta e="T879" id="Seg_9213" s="T878">gehen-EP-IMP.2PL</ta>
            <ta e="T880" id="Seg_9214" s="T879">2PL.[NOM]</ta>
            <ta e="T881" id="Seg_9215" s="T880">Weg-2PL-DAT/LOC</ta>
            <ta e="T882" id="Seg_9216" s="T881">wer.[NOM]</ta>
            <ta e="T883" id="Seg_9217" s="T882">NEG</ta>
            <ta e="T884" id="Seg_9218" s="T883">stören-NEG.PTCP-3SG-ACC</ta>
            <ta e="T885" id="Seg_9219" s="T884">groß</ta>
            <ta e="T886" id="Seg_9220" s="T885">Verstand-ACC</ta>
            <ta e="T887" id="Seg_9221" s="T886">nehmen-EP-IMP.2PL</ta>
            <ta e="T888" id="Seg_9222" s="T887">schnell-ADVZ</ta>
            <ta e="T889" id="Seg_9223" s="T888">zurückkommen-EP-IMP.2PL</ta>
            <ta e="T890" id="Seg_9224" s="T889">geboren.werden-PTCP.PST</ta>
            <ta e="T891" id="Seg_9225" s="T890">Ort-2PL-DAT/LOC</ta>
            <ta e="T892" id="Seg_9226" s="T891">bleiben-CVB.SIM</ta>
            <ta e="T893" id="Seg_9227" s="T892">Leute.[NOM]</ta>
            <ta e="T894" id="Seg_9228" s="T893">lange</ta>
            <ta e="T895" id="Seg_9229" s="T894">sehr</ta>
            <ta e="T896" id="Seg_9230" s="T895">sehen-CVB.SIM</ta>
            <ta e="T897" id="Seg_9231" s="T896">stehen-PST2-3PL</ta>
            <ta e="T898" id="Seg_9232" s="T897">Tundra.[NOM]</ta>
            <ta e="T899" id="Seg_9233" s="T898">oberer.Teil-3SG-INSTR</ta>
            <ta e="T900" id="Seg_9234" s="T899">lang</ta>
            <ta e="T901" id="Seg_9235" s="T900">Rentierkarawane.[NOM]</ta>
            <ta e="T902" id="Seg_9236" s="T901">verlorengehen-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T903" id="Seg_9237" s="T902">bis.zu</ta>
            <ta e="T904" id="Seg_9238" s="T903">Anhöhe.[NOM]</ta>
            <ta e="T905" id="Seg_9239" s="T904">entfernt</ta>
            <ta e="T906" id="Seg_9240" s="T905">Seite-3SG-DAT/LOC</ta>
            <ta e="T907" id="Seg_9241" s="T906">Kind</ta>
            <ta e="T908" id="Seg_9242" s="T907">Volk-DAT/LOC</ta>
            <ta e="T909" id="Seg_9243" s="T908">Wurzel-VBZ-PST1-3SG</ta>
            <ta e="T910" id="Seg_9244" s="T909">neu</ta>
            <ta e="T911" id="Seg_9245" s="T910">Zeit.[NOM]</ta>
            <ta e="T912" id="Seg_9246" s="T911">lernen-PTCP.PRS</ta>
            <ta e="T913" id="Seg_9247" s="T912">Zeit.[NOM]</ta>
            <ta e="T914" id="Seg_9248" s="T913">Schrift-DAT/LOC</ta>
            <ta e="T915" id="Seg_9249" s="T914">2PL.[NOM]</ta>
            <ta e="T916" id="Seg_9250" s="T915">zuhören-PST1-2PL</ta>
            <ta e="T917" id="Seg_9251" s="T916">Erzählung-ACC</ta>
            <ta e="T918" id="Seg_9252" s="T917">Volochanka.[NOM]</ta>
            <ta e="T919" id="Seg_9253" s="T918">zu</ta>
            <ta e="T920" id="Seg_9254" s="T919">nomadisieren-NMNZ.[NOM]</ta>
            <ta e="T921" id="Seg_9255" s="T920">lesen-PST2-3SG</ta>
            <ta e="T922" id="Seg_9256" s="T921">Popov.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_9257" s="T0">Волочанка.[NOM]</ta>
            <ta e="T2" id="Seg_9258" s="T1">к</ta>
            <ta e="T3" id="Seg_9259" s="T2">кочевать-NMNZ.[NOM]</ta>
            <ta e="T4" id="Seg_9260" s="T3">овод.[NOM]</ta>
            <ta e="T5" id="Seg_9261" s="T4">месяц-3SG.[NOM]</ta>
            <ta e="T6" id="Seg_9262" s="T5">кончаться-PTCP.PRS</ta>
            <ta e="T7" id="Seg_9263" s="T6">день-PL-3SG.[NOM]</ta>
            <ta e="T8" id="Seg_9264" s="T7">вставать-PST2-3PL</ta>
            <ta e="T9" id="Seg_9265" s="T8">теплый.[NOM]</ta>
            <ta e="T10" id="Seg_9266" s="T9">очень</ta>
            <ta e="T11" id="Seg_9267" s="T10">трава-PL.[NOM]</ta>
            <ta e="T12" id="Seg_9268" s="T11">лист-PL.[NOM]</ta>
            <ta e="T13" id="Seg_9269" s="T12">желтеть-PST2-3PL</ta>
            <ta e="T14" id="Seg_9270" s="T13">тундра.[NOM]</ta>
            <ta e="T15" id="Seg_9271" s="T14">там-здесь</ta>
            <ta e="T16" id="Seg_9272" s="T15">красный</ta>
            <ta e="T17" id="Seg_9273" s="T16">золото.[NOM]</ta>
            <ta e="T18" id="Seg_9274" s="T17">становиться-CVB.SEQ</ta>
            <ta e="T19" id="Seg_9275" s="T18">идти-PST2.[3SG]</ta>
            <ta e="T20" id="Seg_9276" s="T19">тот</ta>
            <ta e="T21" id="Seg_9277" s="T20">день-PL-DAT/LOC</ta>
            <ta e="T22" id="Seg_9278" s="T21">знать-NMNZ-PROPR</ta>
            <ta e="T23" id="Seg_9279" s="T22">большой</ta>
            <ta e="T24" id="Seg_9280" s="T23">озеро.[NOM]</ta>
            <ta e="T25" id="Seg_9281" s="T24">берег-3SG-DAT/LOC</ta>
            <ta e="T26" id="Seg_9282" s="T25">большой</ta>
            <ta e="T27" id="Seg_9283" s="T26">собирать-REFL-NMNZ.[NOM]</ta>
            <ta e="T28" id="Seg_9284" s="T27">стоять-PST2-3SG</ta>
            <ta e="T29" id="Seg_9285" s="T28">ребенок.[NOM]</ta>
            <ta e="T30" id="Seg_9286" s="T29">народ-ACC</ta>
            <ta e="T31" id="Seg_9287" s="T30">готовить-PRS-3PL</ta>
            <ta e="T32" id="Seg_9288" s="T31">Волочанка-DAT/LOC</ta>
            <ta e="T33" id="Seg_9289" s="T32">уносить-CVB.PURP</ta>
            <ta e="T34" id="Seg_9290" s="T33">учеба-DAT/LOC</ta>
            <ta e="T35" id="Seg_9291" s="T34">тундра.[NOM]</ta>
            <ta e="T36" id="Seg_9292" s="T35">население.[NOM]</ta>
            <ta e="T37" id="Seg_9293" s="T36">раньше</ta>
            <ta e="T38" id="Seg_9294" s="T37">знать-NEG.PTCP</ta>
            <ta e="T39" id="Seg_9295" s="T38">быть-PST1-3PL</ta>
            <ta e="T40" id="Seg_9296" s="T39">ребенок-PL-3SG-ACC</ta>
            <ta e="T41" id="Seg_9297" s="T40">учеба-DAT/LOC</ta>
            <ta e="T42" id="Seg_9298" s="T41">послать-PTCP.PRS-ACC</ta>
            <ta e="T43" id="Seg_9299" s="T42">тот.[NOM]</ta>
            <ta e="T44" id="Seg_9300" s="T43">из_за</ta>
            <ta e="T45" id="Seg_9301" s="T44">желание-PL-3SG.[NOM]</ta>
            <ta e="T46" id="Seg_9302" s="T45">идти-NEG.[3SG]</ta>
            <ta e="T47" id="Seg_9303" s="T46">школа-DAT/LOC</ta>
            <ta e="T48" id="Seg_9304" s="T47">давать-PTCP.PRS-DAT/LOC</ta>
            <ta e="T49" id="Seg_9305" s="T48">разный</ta>
            <ta e="T50" id="Seg_9306" s="T49">слово-ACC</ta>
            <ta e="T51" id="Seg_9307" s="T50">найти-PRS-3PL</ta>
            <ta e="T52" id="Seg_9308" s="T51">послать-EP-NEG-CVB.PURP</ta>
            <ta e="T53" id="Seg_9309" s="T52">ребенок-PL-3SG-ACC</ta>
            <ta e="T54" id="Seg_9310" s="T53">родовой-3PL-DAT/LOC</ta>
            <ta e="T55" id="Seg_9311" s="T54">тот</ta>
            <ta e="T56" id="Seg_9312" s="T55">день-PL-DAT/LOC</ta>
            <ta e="T57" id="Seg_9313" s="T56">очень</ta>
            <ta e="T58" id="Seg_9314" s="T57">мука-PROPR</ta>
            <ta e="T59" id="Seg_9315" s="T58">работа-ACC</ta>
            <ta e="T60" id="Seg_9316" s="T59">делать-PST2-3PL</ta>
            <ta e="T61" id="Seg_9317" s="T60">каждый</ta>
            <ta e="T62" id="Seg_9318" s="T61">шест.[NOM]</ta>
            <ta e="T63" id="Seg_9319" s="T62">чум.[NOM]</ta>
            <ta e="T64" id="Seg_9320" s="T63">каждый</ta>
            <ta e="T65" id="Seg_9321" s="T64">идти-CVB.SEQ</ta>
            <ta e="T66" id="Seg_9322" s="T65">разговаривать-CVB.SIM</ta>
            <ta e="T67" id="Seg_9323" s="T66">пытаться-PST2.[3SG]</ta>
            <ta e="T68" id="Seg_9324" s="T67">ребенок-PL-3SG-ACC</ta>
            <ta e="T69" id="Seg_9325" s="T68">школа-DAT/LOC</ta>
            <ta e="T70" id="Seg_9326" s="T69">давать-PTCP.FUT-3PL-ACC</ta>
            <ta e="T71" id="Seg_9327" s="T70">учеба-DAT/LOC</ta>
            <ta e="T72" id="Seg_9328" s="T71">послать-PTCP.FUT-3PL-ACC</ta>
            <ta e="T73" id="Seg_9329" s="T72">тот-ABL</ta>
            <ta e="T74" id="Seg_9330" s="T73">что.[NOM]</ta>
            <ta e="T75" id="Seg_9331" s="T74">NEG</ta>
            <ta e="T76" id="Seg_9332" s="T75">появляться-EP-PST2.NEG.[3SG]</ta>
            <ta e="T77" id="Seg_9333" s="T76">тот.[NOM]</ta>
            <ta e="T78" id="Seg_9334" s="T77">из_за</ta>
            <ta e="T79" id="Seg_9335" s="T78">уговор.[NOM]</ta>
            <ta e="T80" id="Seg_9336" s="T79">становиться-PST2.[3SG]</ta>
            <ta e="T81" id="Seg_9337" s="T80">делать-PTCP.PRS-DAT/LOC</ta>
            <ta e="T82" id="Seg_9338" s="T81">говорить-CVB.SEQ</ta>
            <ta e="T83" id="Seg_9339" s="T82">сход-ACC</ta>
            <ta e="T84" id="Seg_9340" s="T83">тундра.[NOM]</ta>
            <ta e="T85" id="Seg_9341" s="T84">верхняя.часть-3SG-INSTR</ta>
            <ta e="T86" id="Seg_9342" s="T85">каждый-3PL.[NOM]</ta>
            <ta e="T87" id="Seg_9343" s="T86">собирать-PASS/REFL-PTCP.FUT-3PL-ACC</ta>
            <ta e="T88" id="Seg_9344" s="T87">лицо.[NOM]</ta>
            <ta e="T89" id="Seg_9345" s="T88">лицо-DAT/LOC</ta>
            <ta e="T90" id="Seg_9346" s="T89">разговаривать-NMNZ.[NOM]</ta>
            <ta e="T91" id="Seg_9347" s="T90">становиться-PTCP.FUT-3SG-ACC</ta>
            <ta e="T92" id="Seg_9348" s="T91">один</ta>
            <ta e="T93" id="Seg_9349" s="T92">большой</ta>
            <ta e="T94" id="Seg_9350" s="T93">шест.[NOM]</ta>
            <ta e="T95" id="Seg_9351" s="T94">дом-DAT/LOC</ta>
            <ta e="T96" id="Seg_9352" s="T95">еле</ta>
            <ta e="T97" id="Seg_9353" s="T96">подходить-CVB.SEQ</ta>
            <ta e="T98" id="Seg_9354" s="T97">собирать-PASS/REFL-EP-PST2-3PL</ta>
            <ta e="T99" id="Seg_9355" s="T98">стоять-PRS.[3SG]</ta>
            <ta e="T100" id="Seg_9356" s="T99">стоять-NEG.[3SG]</ta>
            <ta e="T101" id="Seg_9357" s="T100">мужчина.[NOM]</ta>
            <ta e="T102" id="Seg_9358" s="T101">человек-PL.[NOM]</ta>
            <ta e="T103" id="Seg_9359" s="T102">что.[NOM]</ta>
            <ta e="T104" id="Seg_9360" s="T103">NEG</ta>
            <ta e="T105" id="Seg_9361" s="T104">говорить-PTCP.FUT-3PL-ACC</ta>
            <ta e="T106" id="Seg_9362" s="T105">сила-3SG-ABL</ta>
            <ta e="T107" id="Seg_9363" s="T106">земля.[NOM]</ta>
            <ta e="T108" id="Seg_9364" s="T107">к</ta>
            <ta e="T109" id="Seg_9365" s="T108">только</ta>
            <ta e="T110" id="Seg_9366" s="T109">видеть-PRS-3PL</ta>
            <ta e="T111" id="Seg_9367" s="T110">жена.[NOM]</ta>
            <ta e="T112" id="Seg_9368" s="T111">народ.[NOM]</ta>
            <ta e="T113" id="Seg_9369" s="T112">платок-3PL-INSTR</ta>
            <ta e="T114" id="Seg_9370" s="T113">глаз-3PL-GEN</ta>
            <ta e="T115" id="Seg_9371" s="T114">вода-3PL-ACC</ta>
            <ta e="T116" id="Seg_9372" s="T115">вытирать-EP-MED-PRS-3PL</ta>
            <ta e="T117" id="Seg_9373" s="T116">%%-PL.[NOM]</ta>
            <ta e="T118" id="Seg_9374" s="T117">что.[NOM]</ta>
            <ta e="T119" id="Seg_9375" s="T118">быть-CVB.SEQ-2PL</ta>
            <ta e="T120" id="Seg_9376" s="T119">голова-PL-2PL-ACC</ta>
            <ta e="T121" id="Seg_9377" s="T120">колено-PL-2PL.[NOM]</ta>
            <ta e="T122" id="Seg_9378" s="T121">нутро-3SG-DAT/LOC</ta>
            <ta e="T123" id="Seg_9379" s="T122">входить-CAUS-PST1-2PL</ta>
            <ta e="T124" id="Seg_9380" s="T123">почему</ta>
            <ta e="T125" id="Seg_9381" s="T124">замолчать-RECP/COLL-CVB.SIM</ta>
            <ta e="T126" id="Seg_9382" s="T125">лежать-PRS-2PL</ta>
            <ta e="T127" id="Seg_9383" s="T126">говорить-PST2.[3SG]</ta>
            <ta e="T128" id="Seg_9384" s="T127">говорить-CVB.SEQ</ta>
            <ta e="T129" id="Seg_9385" s="T128">Евгения</ta>
            <ta e="T130" id="Seg_9386" s="T129">язык-PROPR</ta>
            <ta e="T131" id="Seg_9387" s="T130">старуха.[NOM]</ta>
            <ta e="T132" id="Seg_9388" s="T131">кричать-CVB.SEQ</ta>
            <ta e="T133" id="Seg_9389" s="T132">давать-PST2.[3SG]</ta>
            <ta e="T134" id="Seg_9390" s="T133">3SG.[NOM]</ta>
            <ta e="T135" id="Seg_9391" s="T134">MOD</ta>
            <ta e="T136" id="Seg_9392" s="T135">дерзкий-3SG.[NOM]</ta>
            <ta e="T137" id="Seg_9393" s="T136">очень</ta>
            <ta e="T138" id="Seg_9394" s="T137">кто-ABL</ta>
            <ta e="T139" id="Seg_9395" s="T138">NEG</ta>
            <ta e="T140" id="Seg_9396" s="T139">боиться-EP-NEG.[3SG]</ta>
            <ta e="T141" id="Seg_9397" s="T140">сидеть-PTCP.PRS</ta>
            <ta e="T142" id="Seg_9398" s="T141">место-ABL</ta>
            <ta e="T143" id="Seg_9399" s="T142">потихоньку</ta>
            <ta e="T144" id="Seg_9400" s="T143">вставать-PST2.[3SG]</ta>
            <ta e="T145" id="Seg_9401" s="T144">Порфирий</ta>
            <ta e="T146" id="Seg_9402" s="T145">старик.[NOM]</ta>
            <ta e="T147" id="Seg_9403" s="T146">лысый</ta>
            <ta e="T148" id="Seg_9404" s="T147">голова-3SG-ACC</ta>
            <ta e="T149" id="Seg_9405" s="T148">гладить-CVB.SIM</ta>
            <ta e="T150" id="Seg_9406" s="T149">падать-CVB.ANT</ta>
            <ta e="T151" id="Seg_9407" s="T150">спешить-NEG.CVB.SIM</ta>
            <ta e="T152" id="Seg_9408" s="T151">смотреть-EP-MED-PST2.[3SG]</ta>
            <ta e="T153" id="Seg_9409" s="T152">как</ta>
            <ta e="T154" id="Seg_9410" s="T153">говорить-FUT-1SG=Q</ta>
            <ta e="T155" id="Seg_9411" s="T154">тот.[NOM]</ta>
            <ta e="T156" id="Seg_9412" s="T155">так</ta>
            <ta e="T157" id="Seg_9413" s="T156">быть-PRS.[3SG]</ta>
            <ta e="T158" id="Seg_9414" s="T157">ребенок-PL.[NOM]</ta>
            <ta e="T159" id="Seg_9415" s="T158">этот</ta>
            <ta e="T160" id="Seg_9416" s="T159">новый</ta>
            <ta e="T161" id="Seg_9417" s="T160">время-DAT/LOC</ta>
            <ta e="T162" id="Seg_9418" s="T161">учиться-PTCP.PRS-3PL.[NOM]</ta>
            <ta e="T163" id="Seg_9419" s="T162">ведь</ta>
            <ta e="T164" id="Seg_9420" s="T163">EMPH</ta>
            <ta e="T165" id="Seg_9421" s="T164">очень</ta>
            <ta e="T166" id="Seg_9422" s="T165">правильный.[NOM]</ta>
            <ta e="T167" id="Seg_9423" s="T166">3PL.[NOM]</ta>
            <ta e="T168" id="Seg_9424" s="T167">письмо-AG.[NOM]</ta>
            <ta e="T169" id="Seg_9425" s="T168">быть-PTCP.FUT</ta>
            <ta e="T170" id="Seg_9426" s="T169">падать-NEC-3PL</ta>
            <ta e="T171" id="Seg_9427" s="T170">этот</ta>
            <ta e="T172" id="Seg_9428" s="T171">1SG.[NOM]</ta>
            <ta e="T173" id="Seg_9429" s="T172">Иван.[NOM]</ta>
            <ta e="T174" id="Seg_9430" s="T173">сын-1SG-ACC</ta>
            <ta e="T175" id="Seg_9431" s="T174">маленький-3SG-ABL</ta>
            <ta e="T176" id="Seg_9432" s="T175">учить-PST2-EP-1SG</ta>
            <ta e="T177" id="Seg_9433" s="T176">рыба.[NOM]</ta>
            <ta e="T178" id="Seg_9434" s="T177">животное.[NOM]</ta>
            <ta e="T179" id="Seg_9435" s="T178">капкан-VBZ-PTCP.FUT-3SG-ACC</ta>
            <ta e="T180" id="Seg_9436" s="T179">1SG.[NOM]</ta>
            <ta e="T181" id="Seg_9437" s="T180">теперь</ta>
            <ta e="T182" id="Seg_9438" s="T181">постареть-PST1-1SG</ta>
            <ta e="T183" id="Seg_9439" s="T182">работа-ACC</ta>
            <ta e="T184" id="Seg_9440" s="T183">мочь-EP-MED-NEG.PTCP</ta>
            <ta e="T185" id="Seg_9441" s="T184">быть-CVB.SEQ</ta>
            <ta e="T186" id="Seg_9442" s="T185">быть-EP-1SG</ta>
            <ta e="T187" id="Seg_9443" s="T186">кто.[NOM]</ta>
            <ta e="T188" id="Seg_9444" s="T187">1SG.[NOM]</ta>
            <ta e="T189" id="Seg_9445" s="T188">семья-1SG-ACC</ta>
            <ta e="T190" id="Seg_9446" s="T189">есть-EP-CAUS-FUT-3SG=Q</ta>
            <ta e="T191" id="Seg_9447" s="T190">тундра-1PL.[NOM]</ta>
            <ta e="T192" id="Seg_9448" s="T191">холодний.[NOM]</ta>
            <ta e="T193" id="Seg_9449" s="T192">очень</ta>
            <ta e="T194" id="Seg_9450" s="T193">пурга-PROPR.[NOM]</ta>
            <ta e="T195" id="Seg_9451" s="T194">сильный</ta>
            <ta e="T196" id="Seg_9452" s="T195">только</ta>
            <ta e="T197" id="Seg_9453" s="T196">молодой.[NOM]</ta>
            <ta e="T198" id="Seg_9454" s="T197">только</ta>
            <ta e="T199" id="Seg_9455" s="T198">человек.[NOM]</ta>
            <ta e="T200" id="Seg_9456" s="T199">добыча-ACC</ta>
            <ta e="T201" id="Seg_9457" s="T200">найти-HAB.[3SG]</ta>
            <ta e="T202" id="Seg_9458" s="T201">Иван-ACC</ta>
            <ta e="T203" id="Seg_9459" s="T202">школа-DAT/LOC</ta>
            <ta e="T205" id="Seg_9460" s="T204">пасть-ACC</ta>
            <ta e="T206" id="Seg_9461" s="T205">капкан-ACC</ta>
            <ta e="T207" id="Seg_9462" s="T206">ставить-CVB.SIM</ta>
            <ta e="T208" id="Seg_9463" s="T207">учить-FUT-3PL</ta>
            <ta e="T209" id="Seg_9464" s="T208">NEG-3SG</ta>
            <ta e="T210" id="Seg_9465" s="T209">3SG.[NOM]</ta>
            <ta e="T211" id="Seg_9466" s="T210">забывать-FUT-3SG</ta>
            <ta e="T212" id="Seg_9467" s="T211">тундра.[NOM]</ta>
            <ta e="T213" id="Seg_9468" s="T212">работа-3SG-ACC</ta>
            <ta e="T214" id="Seg_9469" s="T213">жена.[NOM]</ta>
            <ta e="T215" id="Seg_9470" s="T214">подобно</ta>
            <ta e="T216" id="Seg_9471" s="T215">дом-DAT/LOC</ta>
            <ta e="T217" id="Seg_9472" s="T216">только</ta>
            <ta e="T218" id="Seg_9473" s="T217">сидеть-FUT-3SG</ta>
            <ta e="T219" id="Seg_9474" s="T218">что-ACC</ta>
            <ta e="T220" id="Seg_9475" s="T219">NEG</ta>
            <ta e="T221" id="Seg_9476" s="T220">делать-NEG.CVB.SIM</ta>
            <ta e="T222" id="Seg_9477" s="T221">2SG.[NOM]</ta>
            <ta e="T223" id="Seg_9478" s="T222">старик.[NOM]</ta>
            <ta e="T224" id="Seg_9479" s="T223">жена-PL-ACC</ta>
            <ta e="T225" id="Seg_9480" s="T224">трогать-EP-NEG.[IMP.2SG]</ta>
            <ta e="T226" id="Seg_9481" s="T225">опять</ta>
            <ta e="T227" id="Seg_9482" s="T226">Евгения.[NOM]</ta>
            <ta e="T228" id="Seg_9483" s="T227">крик-3SG.[NOM]</ta>
            <ta e="T229" id="Seg_9484" s="T228">слышаться-EP-PST2.[3SG]</ta>
            <ta e="T230" id="Seg_9485" s="T229">кто.[NOM]</ta>
            <ta e="T231" id="Seg_9486" s="T230">2SG-DAT/LOC</ta>
            <ta e="T232" id="Seg_9487" s="T231">пища-ACC</ta>
            <ta e="T233" id="Seg_9488" s="T232">готовить-PRS.[3SG]</ta>
            <ta e="T234" id="Seg_9489" s="T233">дом-2SG-ACC</ta>
            <ta e="T235" id="Seg_9490" s="T234">теплый-ADVZ</ta>
            <ta e="T236" id="Seg_9491" s="T235">держать-PRS.[3SG]</ta>
            <ta e="T237" id="Seg_9492" s="T236">обувь-PL-2SG-ACC</ta>
            <ta e="T238" id="Seg_9493" s="T237">одежда-2SG-ACC</ta>
            <ta e="T239" id="Seg_9494" s="T238">латать-PRS.[3SG]</ta>
            <ta e="T240" id="Seg_9495" s="T239">тот.[NOM]</ta>
            <ta e="T241" id="Seg_9496" s="T240">правда.[NOM]</ta>
            <ta e="T242" id="Seg_9497" s="T241">так</ta>
            <ta e="T243" id="Seg_9498" s="T242">быть-PRS.[3SG]</ta>
            <ta e="T244" id="Seg_9499" s="T243">очень</ta>
            <ta e="T245" id="Seg_9500" s="T244">бояться.[CAUS]-NEG.CVB.SIM</ta>
            <ta e="T246" id="Seg_9501" s="T245">Порфирий.[NOM]</ta>
            <ta e="T247" id="Seg_9502" s="T246">говорить-CVB.SIM</ta>
            <ta e="T248" id="Seg_9503" s="T247">стоять-PST2.[3SG]</ta>
            <ta e="T249" id="Seg_9504" s="T248">1SG.[NOM]</ta>
            <ta e="T250" id="Seg_9505" s="T249">Кыча</ta>
            <ta e="T251" id="Seg_9506" s="T250">дочь-EP-1SG.[NOM]</ta>
            <ta e="T252" id="Seg_9507" s="T251">готовить-CVB.SIM</ta>
            <ta e="T253" id="Seg_9508" s="T252">мочь-PRS.[3SG]</ta>
            <ta e="T254" id="Seg_9509" s="T253">обувь-ACC</ta>
            <ta e="T255" id="Seg_9510" s="T254">одежда-ACC</ta>
            <ta e="T256" id="Seg_9511" s="T255">делать-CVB.SIM</ta>
            <ta e="T257" id="Seg_9512" s="T256">мочь-PRS.[3SG]</ta>
            <ta e="T258" id="Seg_9513" s="T257">школа-DAT/LOC</ta>
            <ta e="T259" id="Seg_9514" s="T258">ведь</ta>
            <ta e="T260" id="Seg_9515" s="T259">EMPH</ta>
            <ta e="T261" id="Seg_9516" s="T260">тот-3SG-3SG-ACC</ta>
            <ta e="T262" id="Seg_9517" s="T261">забывать-FUT-3SG</ta>
            <ta e="T263" id="Seg_9518" s="T262">мужчина.[NOM]</ta>
            <ta e="T264" id="Seg_9519" s="T263">человек.[NOM]</ta>
            <ta e="T265" id="Seg_9520" s="T264">подобно</ta>
            <ta e="T266" id="Seg_9521" s="T265">стоять.прямо-ITER-CVB.SIM</ta>
            <ta e="T267" id="Seg_9522" s="T266">идти-FUT-3SG</ta>
            <ta e="T268" id="Seg_9523" s="T267">дом-ACC</ta>
            <ta e="T269" id="Seg_9524" s="T268">обходить-CVB.SIM</ta>
            <ta e="T270" id="Seg_9525" s="T269">как</ta>
            <ta e="T271" id="Seg_9526" s="T270">1SG.[NOM]</ta>
            <ta e="T272" id="Seg_9527" s="T271">жить-FUT-1SG=Q</ta>
            <ta e="T273" id="Seg_9528" s="T272">ребенок-PL-EP-1SG.[NOM]</ta>
            <ta e="T274" id="Seg_9529" s="T273">идти-TEMP-3PL</ta>
            <ta e="T275" id="Seg_9530" s="T274">родовой-3SG.[NOM]</ta>
            <ta e="T276" id="Seg_9531" s="T275">Анисим</ta>
            <ta e="T277" id="Seg_9532" s="T276">Назарович</ta>
            <ta e="T278" id="Seg_9533" s="T277">Попов.[NOM]</ta>
            <ta e="T279" id="Seg_9534" s="T278">рассмеяться-CVB.SIM</ta>
            <ta e="T280" id="Seg_9535" s="T279">падать-CVB.ANT</ta>
            <ta e="T281" id="Seg_9536" s="T280">старик-ACC</ta>
            <ta e="T282" id="Seg_9537" s="T281">останавливаться-EP-CAUS-PST2.[3SG]</ta>
            <ta e="T283" id="Seg_9538" s="T282">вот</ta>
            <ta e="T284" id="Seg_9539" s="T283">старик.[NOM]</ta>
            <ta e="T285" id="Seg_9540" s="T284">2SG.[NOM]</ta>
            <ta e="T286" id="Seg_9541" s="T285">устать-PST1-2SG</ta>
            <ta e="T287" id="Seg_9542" s="T286">наверное</ta>
            <ta e="T288" id="Seg_9543" s="T287">сесть-CVB.SIM</ta>
            <ta e="T289" id="Seg_9544" s="T288">падать.[IMP.2SG]</ta>
            <ta e="T290" id="Seg_9545" s="T289">другой</ta>
            <ta e="T291" id="Seg_9546" s="T290">люди.[NOM]</ta>
            <ta e="T292" id="Seg_9547" s="T291">говорить-IMP.3SG</ta>
            <ta e="T293" id="Seg_9548" s="T292">старик.[NOM]</ta>
            <ta e="T294" id="Seg_9549" s="T293">что.[NOM]</ta>
            <ta e="T295" id="Seg_9550" s="T294">NEG</ta>
            <ta e="T296" id="Seg_9551" s="T295">говорить-NEG.CVB.SIM</ta>
            <ta e="T297" id="Seg_9552" s="T296">потихоньку</ta>
            <ta e="T298" id="Seg_9553" s="T297">сидение-3SG-DAT/LOC</ta>
            <ta e="T299" id="Seg_9554" s="T298">всплеск</ta>
            <ta e="T300" id="Seg_9555" s="T299">делать-CVB.SIM</ta>
            <ta e="T301" id="Seg_9556" s="T300">сесть-PST2.[3SG]</ta>
            <ta e="T302" id="Seg_9557" s="T301">колено-PL-3SG-ABL</ta>
            <ta e="T303" id="Seg_9558" s="T302">поймать-CVB.SIM</ta>
            <ta e="T304" id="Seg_9559" s="T303">падать-CVB.ANT</ta>
            <ta e="T305" id="Seg_9560" s="T304">кто.[NOM]</ta>
            <ta e="T306" id="Seg_9561" s="T305">еще</ta>
            <ta e="T307" id="Seg_9562" s="T306">что.[NOM]</ta>
            <ta e="T308" id="Seg_9563" s="T307">слово-PROPR.[NOM]=Q</ta>
            <ta e="T309" id="Seg_9564" s="T308">говорить-CVB.SEQ</ta>
            <ta e="T310" id="Seg_9565" s="T309">спрашивать-PST2.[3SG]</ta>
            <ta e="T311" id="Seg_9566" s="T310">родовой-3PL.[NOM]</ta>
            <ta e="T312" id="Seg_9567" s="T311">кто-3PL.[NOM]</ta>
            <ta e="T313" id="Seg_9568" s="T312">INDEF</ta>
            <ta e="T314" id="Seg_9569" s="T313">говорить-PTCP.PRS-3SG-ACC</ta>
            <ta e="T315" id="Seg_9570" s="T314">ждать-CVB.SIM</ta>
            <ta e="T316" id="Seg_9571" s="T315">делать.напрасно-CVB.SEQ</ta>
            <ta e="T317" id="Seg_9572" s="T316">считать-PST2.[3SG]</ta>
            <ta e="T318" id="Seg_9573" s="T317">какой.[NOM]</ta>
            <ta e="T319" id="Seg_9574" s="T318">ребенок-3PL.[NOM]</ta>
            <ta e="T320" id="Seg_9575" s="T319">школа-DAT/LOC</ta>
            <ta e="T321" id="Seg_9576" s="T320">идти-PTCP.PRS-3PL-ACC</ta>
            <ta e="T322" id="Seg_9577" s="T321">кто.[NOM]</ta>
            <ta e="T323" id="Seg_9578" s="T322">оставаться-PTCP.PRS-3SG-ACC</ta>
            <ta e="T324" id="Seg_9579" s="T323">школа-DAT/LOC</ta>
            <ta e="T325" id="Seg_9580" s="T324">идти-NEG.PTCP-3SG-ACC</ta>
            <ta e="T326" id="Seg_9581" s="T325">возраст-3PL.[NOM]</ta>
            <ta e="T327" id="Seg_9582" s="T326">большой-PL-ACC</ta>
            <ta e="T328" id="Seg_9583" s="T327">тот</ta>
            <ta e="T329" id="Seg_9584" s="T328">ребенок-PL-DAT/LOC</ta>
            <ta e="T330" id="Seg_9585" s="T329">попадать-PST2.[3SG]</ta>
            <ta e="T331" id="Seg_9586" s="T330">Порфирий.[NOM]</ta>
            <ta e="T332" id="Seg_9587" s="T331">старик.[NOM]</ta>
            <ta e="T333" id="Seg_9588" s="T332">сын-3SG.[NOM]</ta>
            <ta e="T334" id="Seg_9589" s="T333">Иван.[NOM]</ta>
            <ta e="T335" id="Seg_9590" s="T334">Кыча-ACC</ta>
            <ta e="T336" id="Seg_9591" s="T335">тоже</ta>
            <ta e="T337" id="Seg_9592" s="T336">оставлять-PTCP.FUT-DAT/LOC</ta>
            <ta e="T338" id="Seg_9593" s="T337">падать-NEC.[3SG]</ta>
            <ta e="T339" id="Seg_9594" s="T338">3SG.[NOM]</ta>
            <ta e="T340" id="Seg_9595" s="T339">возраст-3SG.[NOM]</ta>
            <ta e="T341" id="Seg_9596" s="T340">ведь</ta>
            <ta e="T342" id="Seg_9597" s="T341">EMPH</ta>
            <ta e="T343" id="Seg_9598" s="T342">много</ta>
            <ta e="T344" id="Seg_9599" s="T343">попросить-EP-MED-PST2.[3SG]</ta>
            <ta e="T345" id="Seg_9600" s="T344">Порфирий.[NOM]</ta>
            <ta e="T346" id="Seg_9601" s="T345">старик.[NOM]</ta>
            <ta e="T347" id="Seg_9602" s="T346">нет</ta>
            <ta e="T348" id="Seg_9603" s="T347">Кыча.[NOM]</ta>
            <ta e="T349" id="Seg_9604" s="T348">идти-FUT.[3SG]</ta>
            <ta e="T350" id="Seg_9605" s="T349">NEG-3SG</ta>
            <ta e="T351" id="Seg_9606" s="T350">школа-DAT/LOC</ta>
            <ta e="T352" id="Seg_9607" s="T351">старик.[NOM]</ta>
            <ta e="T353" id="Seg_9608" s="T352">опасаться-EP-NEG.[IMP.2SG]</ta>
            <ta e="T354" id="Seg_9609" s="T353">учеба-PROPR.[NOM]</ta>
            <ta e="T355" id="Seg_9610" s="T354">быть-TEMP-3SG</ta>
            <ta e="T356" id="Seg_9611" s="T355">мужчина.[NOM]</ta>
            <ta e="T357" id="Seg_9612" s="T356">человек.[NOM]</ta>
            <ta e="T358" id="Seg_9613" s="T357">штаны-3SG-ACC</ta>
            <ta e="T359" id="Seg_9614" s="T358">одеть-FUT.[3SG]</ta>
            <ta e="T360" id="Seg_9615" s="T359">NEG-3SG</ta>
            <ta e="T361" id="Seg_9616" s="T360">дом-ACC</ta>
            <ta e="T362" id="Seg_9617" s="T361">вокруг</ta>
            <ta e="T363" id="Seg_9618" s="T362">стоять.прямо-ITER-CVB.SIM</ta>
            <ta e="T364" id="Seg_9619" s="T363">идти-FUT.[3SG]</ta>
            <ta e="T365" id="Seg_9620" s="T364">NEG-3SG</ta>
            <ta e="T366" id="Seg_9621" s="T365">сход-DAT/LOC</ta>
            <ta e="T367" id="Seg_9622" s="T366">сидеть-PTCP.PRS</ta>
            <ta e="T368" id="Seg_9623" s="T367">люди.[NOM]</ta>
            <ta e="T369" id="Seg_9624" s="T368">последний</ta>
            <ta e="T370" id="Seg_9625" s="T369">душа-3PL-INSTR</ta>
            <ta e="T371" id="Seg_9626" s="T370">смеяться-RECP/COLL-EP-PST2-3PL</ta>
            <ta e="T372" id="Seg_9627" s="T371">идти-PTCP.PRS</ta>
            <ta e="T373" id="Seg_9628" s="T372">ребенок-PL-ACC</ta>
            <ta e="T376" id="Seg_9629" s="T375">готовить-IMP.2PL</ta>
            <ta e="T377" id="Seg_9630" s="T376">добрый-ADVZ</ta>
            <ta e="T378" id="Seg_9631" s="T377">одеваться-CAUS-EP-IMP.2PL</ta>
            <ta e="T379" id="Seg_9632" s="T378">праздник-DAT/LOC</ta>
            <ta e="T380" id="Seg_9633" s="T379">одеваться-CAUS-PTCP.PRS.[NOM]</ta>
            <ta e="T381" id="Seg_9634" s="T380">подобно</ta>
            <ta e="T382" id="Seg_9635" s="T381">ребенок-PL-1PL.[NOM]</ta>
            <ta e="T383" id="Seg_9636" s="T382">сам-1SG.[NOM]</ta>
            <ta e="T384" id="Seg_9637" s="T383">уносить-FUT-1SG</ta>
            <ta e="T385" id="Seg_9638" s="T384">Волочанка-ABL</ta>
            <ta e="T386" id="Seg_9639" s="T385">сход-ACC</ta>
            <ta e="T387" id="Seg_9640" s="T386">кончать-EP-CAUS-CVB.SIM</ta>
            <ta e="T388" id="Seg_9641" s="T387">делать-CVB.SIM</ta>
            <ta e="T389" id="Seg_9642" s="T388">говорить-PST2.[3SG]</ta>
            <ta e="T390" id="Seg_9643" s="T389">руководитель-3PL.[NOM]</ta>
            <ta e="T391" id="Seg_9644" s="T390">сход.[NOM]</ta>
            <ta e="T392" id="Seg_9645" s="T391">проехать-CVB.ANT-3SG</ta>
            <ta e="T393" id="Seg_9646" s="T392">собирать-MED-NMNZ.[NOM]</ta>
            <ta e="T394" id="Seg_9647" s="T393">шум-3SG.[NOM]</ta>
            <ta e="T395" id="Seg_9648" s="T394">умножаться-PST2.[3SG]</ta>
            <ta e="T396" id="Seg_9649" s="T395">жена-PL.[NOM]</ta>
            <ta e="T397" id="Seg_9650" s="T396">сума-3PL-GEN</ta>
            <ta e="T398" id="Seg_9651" s="T397">нутро-3SG-ABL</ta>
            <ta e="T399" id="Seg_9652" s="T398">вынимать-ITER-PRS-3PL</ta>
            <ta e="T400" id="Seg_9653" s="T399">ребенок-3PL-GEN</ta>
            <ta e="T401" id="Seg_9654" s="T400">широкий</ta>
            <ta e="T402" id="Seg_9655" s="T401">широкий</ta>
            <ta e="T403" id="Seg_9656" s="T402">одежда-3PL-ACC</ta>
            <ta e="T404" id="Seg_9657" s="T403">праздник-DAT/LOC</ta>
            <ta e="T405" id="Seg_9658" s="T404">одеть-PTCP.PRS</ta>
            <ta e="T406" id="Seg_9659" s="T405">рубашка-3PL-ACC</ta>
            <ta e="T407" id="Seg_9660" s="T406">некоторый-PL.[NOM]</ta>
            <ta e="T408" id="Seg_9661" s="T407">быть.видно-EP-NEG.PTCP</ta>
            <ta e="T409" id="Seg_9662" s="T408">место-DAT/LOC</ta>
            <ta e="T410" id="Seg_9663" s="T409">шить-MULT-PRS-3PL</ta>
            <ta e="T411" id="Seg_9664" s="T410">быть-PST2.[3SG]</ta>
            <ta e="T412" id="Seg_9665" s="T411">маленький</ta>
            <ta e="T413" id="Seg_9666" s="T412">икона-3PL-ACC</ta>
            <ta e="T414" id="Seg_9667" s="T413">кресть-PL-ACC</ta>
            <ta e="T415" id="Seg_9668" s="T414">ребенок-PL-3SG-ACC</ta>
            <ta e="T416" id="Seg_9669" s="T415">чествовать-3PL</ta>
            <ta e="T417" id="Seg_9670" s="T416">каждый</ta>
            <ta e="T418" id="Seg_9671" s="T417">последний</ta>
            <ta e="T419" id="Seg_9672" s="T418">вкусный</ta>
            <ta e="T420" id="Seg_9673" s="T419">пища-PL-EP-INSTR</ta>
            <ta e="T421" id="Seg_9674" s="T420">измельченное.мясо.с.жиром-INSTR</ta>
            <ta e="T422" id="Seg_9675" s="T421">сушеная.рыба-EP-INSTR</ta>
            <ta e="T423" id="Seg_9676" s="T422">олень.[NOM]</ta>
            <ta e="T424" id="Seg_9677" s="T423">язык-EP-INSTR</ta>
            <ta e="T425" id="Seg_9678" s="T424">вечер.[NOM]</ta>
            <ta e="T426" id="Seg_9679" s="T425">к</ta>
            <ta e="T429" id="Seg_9680" s="T428">родовой-3SG-DAT/LOC</ta>
            <ta e="T430" id="Seg_9681" s="T429">посещать-CVB.SIM</ta>
            <ta e="T431" id="Seg_9682" s="T430">входить-PST2.[3SG]</ta>
            <ta e="T432" id="Seg_9683" s="T431">Семен</ta>
            <ta e="T433" id="Seg_9684" s="T432">старик.[NOM]</ta>
            <ta e="T434" id="Seg_9685" s="T433">почему</ta>
            <ta e="T435" id="Seg_9686" s="T434">приходить-PTCP.PST-3SG-ACC</ta>
            <ta e="T436" id="Seg_9687" s="T435">сквозь</ta>
            <ta e="T437" id="Seg_9688" s="T436">говорить-NEG.[3SG]</ta>
            <ta e="T438" id="Seg_9689" s="T437">чай.[NOM]</ta>
            <ta e="T439" id="Seg_9690" s="T438">пить-CVB.SIM</ta>
            <ta e="T440" id="Seg_9691" s="T439">только</ta>
            <ta e="T441" id="Seg_9692" s="T440">сидеть-CVB.SEQ</ta>
            <ta e="T442" id="Seg_9693" s="T441">говорить-PST2.[3SG]</ta>
            <ta e="T443" id="Seg_9694" s="T442">давно</ta>
            <ta e="T444" id="Seg_9695" s="T443">дедушка-PL-1PL.[NOM]</ta>
            <ta e="T445" id="Seg_9696" s="T444">отец-PL-1PL.[NOM]</ta>
            <ta e="T446" id="Seg_9697" s="T445">Бог-AG.[NOM]</ta>
            <ta e="T447" id="Seg_9698" s="T446">быть-PST1-3PL</ta>
            <ta e="T448" id="Seg_9699" s="T447">большой</ta>
            <ta e="T449" id="Seg_9700" s="T448">шайтан-DAT/LOC</ta>
            <ta e="T450" id="Seg_9701" s="T449">всегда</ta>
            <ta e="T451" id="Seg_9702" s="T450">подарок-VBZ-PTCP.PRS</ta>
            <ta e="T452" id="Seg_9703" s="T451">обыкновение-PROPR.[NOM]</ta>
            <ta e="T453" id="Seg_9704" s="T452">быть-PST1-3PL</ta>
            <ta e="T454" id="Seg_9705" s="T453">каждый</ta>
            <ta e="T455" id="Seg_9706" s="T454">родственник.[NOM]</ta>
            <ta e="T456" id="Seg_9707" s="T455">добрый-ADVZ</ta>
            <ta e="T457" id="Seg_9708" s="T456">жить-PTCP.FUT-3PL-ACC</ta>
            <ta e="T458" id="Seg_9709" s="T457">хотеть-PRS-3PL</ta>
            <ta e="T459" id="Seg_9710" s="T458">что-EP-INSTR</ta>
            <ta e="T460" id="Seg_9711" s="T459">NEG</ta>
            <ta e="T461" id="Seg_9712" s="T460">нуждаться-NEG.CVB.SIM</ta>
            <ta e="T462" id="Seg_9713" s="T461">быть.больным-EP-NEG.CVB.SIM</ta>
            <ta e="T463" id="Seg_9714" s="T462">большой</ta>
            <ta e="T464" id="Seg_9715" s="T463">шаман-PL.[NOM]</ta>
            <ta e="T465" id="Seg_9716" s="T464">человек.[NOM]</ta>
            <ta e="T466" id="Seg_9717" s="T465">идти-PTCP.PRS</ta>
            <ta e="T467" id="Seg_9718" s="T466">дорога-3SG-ACC</ta>
            <ta e="T468" id="Seg_9719" s="T467">знать-PTCP.PRS-3PL.[NOM]</ta>
            <ta e="T469" id="Seg_9720" s="T468">очень</ta>
            <ta e="T470" id="Seg_9721" s="T469">быть-PST1-3SG</ta>
            <ta e="T471" id="Seg_9722" s="T470">этот</ta>
            <ta e="T472" id="Seg_9723" s="T471">место.жизни-1PL-ABL</ta>
            <ta e="T473" id="Seg_9724" s="T472">далекий-POSS</ta>
            <ta e="T474" id="Seg_9725" s="T473">NEG</ta>
            <ta e="T475" id="Seg_9726" s="T474">есть</ta>
            <ta e="T476" id="Seg_9727" s="T475">большой</ta>
            <ta e="T477" id="Seg_9728" s="T476">шайтан-камень.[NOM]</ta>
            <ta e="T478" id="Seg_9729" s="T477">каждый</ta>
            <ta e="T479" id="Seg_9730" s="T478">человек.[NOM]</ta>
            <ta e="T480" id="Seg_9731" s="T479">там</ta>
            <ta e="T481" id="Seg_9732" s="T480">подарок-ACC</ta>
            <ta e="T482" id="Seg_9733" s="T481">давать-HAB.[3SG]</ta>
            <ta e="T483" id="Seg_9734" s="T482">скоро</ta>
            <ta e="T484" id="Seg_9735" s="T483">ребенок-PL-1PL.[NOM]</ta>
            <ta e="T485" id="Seg_9736" s="T484">чужой</ta>
            <ta e="T486" id="Seg_9737" s="T485">место-DAT/LOC</ta>
            <ta e="T487" id="Seg_9738" s="T486">идти-PRS-3PL</ta>
            <ta e="T488" id="Seg_9739" s="T487">чужой</ta>
            <ta e="T489" id="Seg_9740" s="T488">человек-PL-DAT/LOC</ta>
            <ta e="T490" id="Seg_9741" s="T489">родовой-3SG.[NOM]</ta>
            <ta e="T491" id="Seg_9742" s="T490">гость-3SG-GEN</ta>
            <ta e="T492" id="Seg_9743" s="T491">рассказ-3SG-ACC</ta>
            <ta e="T493" id="Seg_9744" s="T492">слушать-CVB.SIM-слушать-CVB.SIM</ta>
            <ta e="T494" id="Seg_9745" s="T493">нутро-3SG-DAT/LOC</ta>
            <ta e="T495" id="Seg_9746" s="T494">говорить-CVB.SIM</ta>
            <ta e="T496" id="Seg_9747" s="T495">думать-PRS.[3SG]</ta>
            <ta e="T497" id="Seg_9748" s="T496">что</ta>
            <ta e="T498" id="Seg_9749" s="T497">мысль-3SG-INSTR</ta>
            <ta e="T499" id="Seg_9750" s="T498">такой</ta>
            <ta e="T500" id="Seg_9751" s="T499">рассказ-ACC</ta>
            <ta e="T501" id="Seg_9752" s="T500">вынимать-PRS.[3SG]</ta>
            <ta e="T502" id="Seg_9753" s="T501">гость-3SG.[NOM]</ta>
            <ta e="T503" id="Seg_9754" s="T502">спешить-NEG.CVB.SIM</ta>
            <ta e="T504" id="Seg_9755" s="T503">говорить-ITER-CVB.SEQ</ta>
            <ta e="T505" id="Seg_9756" s="T504">идти-PST2.[3SG]</ta>
            <ta e="T506" id="Seg_9757" s="T505">жена-PL.[NOM]</ta>
            <ta e="T507" id="Seg_9758" s="T506">беда-PROPR-PL</ta>
            <ta e="T508" id="Seg_9759" s="T507">очень</ta>
            <ta e="T509" id="Seg_9760" s="T508">горюниться-PST1-3PL</ta>
            <ta e="T510" id="Seg_9761" s="T509">плакать-EP-RECP/COLL-PRS-3PL</ta>
            <ta e="T511" id="Seg_9762" s="T510">маленький</ta>
            <ta e="T512" id="Seg_9763" s="T511">ребенок-ACC</ta>
            <ta e="T513" id="Seg_9764" s="T512">чум-ABL</ta>
            <ta e="T514" id="Seg_9765" s="T513">отнимать-PTCP.PRS</ta>
            <ta e="T515" id="Seg_9766" s="T514">дело.[NOM]</ta>
            <ta e="T516" id="Seg_9767" s="T515">очень</ta>
            <ta e="T517" id="Seg_9768" s="T516">грех-PROPR.[NOM]</ta>
            <ta e="T518" id="Seg_9769" s="T517">люди.[NOM]</ta>
            <ta e="T519" id="Seg_9770" s="T518">очень</ta>
            <ta e="T520" id="Seg_9771" s="T519">грустить-PRS.[3SG]</ta>
            <ta e="T521" id="Seg_9772" s="T520">видно</ta>
            <ta e="T522" id="Seg_9773" s="T521">шайтан-DAT/LOC</ta>
            <ta e="T523" id="Seg_9774" s="T522">подарок.[NOM]</ta>
            <ta e="T524" id="Seg_9775" s="T523">давать-PTCP.FUT-DAT/LOC</ta>
            <ta e="T525" id="Seg_9776" s="T524">вот</ta>
            <ta e="T526" id="Seg_9777" s="T525">старик.[NOM]</ta>
            <ta e="T527" id="Seg_9778" s="T526">1SG-ACC</ta>
            <ta e="T528" id="Seg_9779" s="T527">плевать-NEG.[IMP.2SG]</ta>
            <ta e="T529" id="Seg_9780" s="T528">тот-ACC</ta>
            <ta e="T530" id="Seg_9781" s="T529">делать-FUT-1PL</ta>
            <ta e="T531" id="Seg_9782" s="T530">NEG-3SG</ta>
            <ta e="T532" id="Seg_9783" s="T531">школа.[NOM]</ta>
            <ta e="T533" id="Seg_9784" s="T532">шайтан-ACC</ta>
            <ta e="T534" id="Seg_9785" s="T533">попросить-NEG.[3SG]</ta>
            <ta e="T535" id="Seg_9786" s="T534">так</ta>
            <ta e="T536" id="Seg_9787" s="T535">каждый</ta>
            <ta e="T537" id="Seg_9788" s="T536">человек-DAT/LOC</ta>
            <ta e="T538" id="Seg_9789" s="T537">рассказывать.[IMP.2SG]</ta>
            <ta e="T539" id="Seg_9790" s="T538">грустить-NEG-IMP.3PL</ta>
            <ta e="T540" id="Seg_9791" s="T539">говорить-PST2.[3SG]</ta>
            <ta e="T541" id="Seg_9792" s="T540">родовой-3SG.[NOM]</ta>
            <ta e="T542" id="Seg_9793" s="T541">старик.[NOM]</ta>
            <ta e="T543" id="Seg_9794" s="T542">что.[NOM]</ta>
            <ta e="T544" id="Seg_9795" s="T543">NEG</ta>
            <ta e="T545" id="Seg_9796" s="T544">говорить-NEG.CVB.SIM</ta>
            <ta e="T546" id="Seg_9797" s="T545">чашка-3SG-DAT/LOC</ta>
            <ta e="T547" id="Seg_9798" s="T546">чай.[NOM]</ta>
            <ta e="T548" id="Seg_9799" s="T547">лить-EP-MED-PST2.[3SG]</ta>
            <ta e="T549" id="Seg_9800" s="T548">1PL.[NOM]</ta>
            <ta e="T550" id="Seg_9801" s="T549">старик-PL.[NOM]</ta>
            <ta e="T551" id="Seg_9802" s="T550">как</ta>
            <ta e="T552" id="Seg_9803" s="T551">хороший.[NOM]</ta>
            <ta e="T553" id="Seg_9804" s="T552">быть-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T554" id="Seg_9805" s="T553">думать-CVB.SEQ</ta>
            <ta e="T555" id="Seg_9806" s="T554">совет.[NOM]</ta>
            <ta e="T556" id="Seg_9807" s="T555">давать-PRS-1PL</ta>
            <ta e="T557" id="Seg_9808" s="T556">правда.[NOM]</ta>
            <ta e="T558" id="Seg_9809" s="T557">некоторый</ta>
            <ta e="T559" id="Seg_9810" s="T558">после.того</ta>
            <ta e="T560" id="Seg_9811" s="T559">ошибка.[NOM]</ta>
            <ta e="T561" id="Seg_9812" s="T560">говорить-PRS-1PL</ta>
            <ta e="T562" id="Seg_9813" s="T561">быть-FUT-3SG</ta>
            <ta e="T563" id="Seg_9814" s="T562">жена-PL.[NOM]</ta>
            <ta e="T564" id="Seg_9815" s="T563">плакать-EP-MED-PTCP.PRS-3PL.[3SG]</ta>
            <ta e="T565" id="Seg_9816" s="T564">сильный.[NOM]</ta>
            <ta e="T566" id="Seg_9817" s="T565">человек.[NOM]</ta>
            <ta e="T567" id="Seg_9818" s="T566">учеба-3SG-ACC</ta>
            <ta e="T569" id="Seg_9819" s="T568">человек.[NOM]</ta>
            <ta e="T570" id="Seg_9820" s="T569">учеба-3SG-ACC</ta>
            <ta e="T571" id="Seg_9821" s="T570">заботиться-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T572" id="Seg_9822" s="T571">очень</ta>
            <ta e="T573" id="Seg_9823" s="T572">очень</ta>
            <ta e="T574" id="Seg_9824" s="T573">очень</ta>
            <ta e="T575" id="Seg_9825" s="T574">маленький</ta>
            <ta e="T576" id="Seg_9826" s="T575">ребенок-PL-DAT/LOC</ta>
            <ta e="T577" id="Seg_9827" s="T576">большой</ta>
            <ta e="T578" id="Seg_9828" s="T577">люди-ACC</ta>
            <ta e="T579" id="Seg_9829" s="T578">распространять-PTCP.FUT-DAT/LOC</ta>
            <ta e="T580" id="Seg_9830" s="T579">прямо</ta>
            <ta e="T581" id="Seg_9831" s="T580">видеть-PTCP.PRS</ta>
            <ta e="T582" id="Seg_9832" s="T581">быть-PTCP.FUT-3PL-ACC</ta>
            <ta e="T583" id="Seg_9833" s="T582">очень</ta>
            <ta e="T584" id="Seg_9834" s="T583">маленький</ta>
            <ta e="T585" id="Seg_9835" s="T584">ребенок.[NOM]</ta>
            <ta e="T586" id="Seg_9836" s="T585">олененок.[NOM]</ta>
            <ta e="T587" id="Seg_9837" s="T586">подобно</ta>
            <ta e="T588" id="Seg_9838" s="T587">где</ta>
            <ta e="T589" id="Seg_9839" s="T588">идти-PTCP.PRS-3SG-ACC</ta>
            <ta e="T590" id="Seg_9840" s="T589">знать-NEG.[3SG]</ta>
            <ta e="T591" id="Seg_9841" s="T590">добрый-ADVZ</ta>
            <ta e="T592" id="Seg_9842" s="T591">одеваться-CVB.SIM</ta>
            <ta e="T593" id="Seg_9843" s="T592">мочь-NEG.[3SG]</ta>
            <ta e="T594" id="Seg_9844" s="T593">тот.EMPH.[NOM]</ta>
            <ta e="T595" id="Seg_9845" s="T594">правда.[NOM]</ta>
            <ta e="T596" id="Seg_9846" s="T595">говорить-PST2.[3SG]</ta>
            <ta e="T597" id="Seg_9847" s="T596">родовой-3SG.[NOM]</ta>
            <ta e="T598" id="Seg_9848" s="T597">старик.[NOM]</ta>
            <ta e="T599" id="Seg_9849" s="T598">этот</ta>
            <ta e="T600" id="Seg_9850" s="T599">сход.[NOM]</ta>
            <ta e="T601" id="Seg_9851" s="T600">шум-3SG-DAT/LOC</ta>
            <ta e="T602" id="Seg_9852" s="T601">забывать-CVB.SEQ</ta>
            <ta e="T603" id="Seg_9853" s="T602">бросать-PST2.[3SG]</ta>
            <ta e="T604" id="Seg_9854" s="T603">зимой</ta>
            <ta e="T605" id="Seg_9855" s="T604">новый</ta>
            <ta e="T606" id="Seg_9856" s="T605">год.[NOM]</ta>
            <ta e="T607" id="Seg_9857" s="T606">приходить-PTCP.PRS</ta>
            <ta e="T608" id="Seg_9858" s="T607">час-3SG-DAT/LOC</ta>
            <ta e="T609" id="Seg_9859" s="T608">ребенок-PL.[NOM]</ta>
            <ta e="T610" id="Seg_9860" s="T609">отдыхать-CVB.SIM</ta>
            <ta e="T611" id="Seg_9861" s="T610">приходить-FUT-3PL</ta>
            <ta e="T612" id="Seg_9862" s="T611">дом-3PL-DAT/LOC</ta>
            <ta e="T613" id="Seg_9863" s="T612">весна.[NOM]</ta>
            <ta e="T614" id="Seg_9864" s="T613">май</ta>
            <ta e="T615" id="Seg_9865" s="T614">месяц-3SG-DAT/LOC</ta>
            <ta e="T616" id="Seg_9866" s="T615">один</ta>
            <ta e="T617" id="Seg_9867" s="T616">год-ADJZ</ta>
            <ta e="T618" id="Seg_9868" s="T617">учеба-3PL-ACC</ta>
            <ta e="T619" id="Seg_9869" s="T618">кончать-TEMP-3PL</ta>
            <ta e="T620" id="Seg_9870" s="T619">осень-DAT/LOC</ta>
            <ta e="T621" id="Seg_9871" s="T620">пока</ta>
            <ta e="T622" id="Seg_9872" s="T621">дом-3PL-DAT/LOC</ta>
            <ta e="T623" id="Seg_9873" s="T622">быть-FUT-3PL</ta>
            <ta e="T624" id="Seg_9874" s="T623">тот</ta>
            <ta e="T625" id="Seg_9875" s="T624">новость-ACC</ta>
            <ta e="T626" id="Seg_9876" s="T625">1SG.[NOM]</ta>
            <ta e="T627" id="Seg_9877" s="T626">каждый</ta>
            <ta e="T628" id="Seg_9878" s="T627">человек-DAT/LOC</ta>
            <ta e="T629" id="Seg_9879" s="T628">рассказывать-FUT-1SG</ta>
            <ta e="T630" id="Seg_9880" s="T629">человек-PL.[NOM]</ta>
            <ta e="T631" id="Seg_9881" s="T630">говорить-CVB.SIM</ta>
            <ta e="T632" id="Seg_9882" s="T631">думать-PRS-3PL</ta>
            <ta e="T633" id="Seg_9883" s="T632">когда</ta>
            <ta e="T634" id="Seg_9884" s="T633">NEG</ta>
            <ta e="T635" id="Seg_9885" s="T634">возвращаться-EP-CAUS-NEG.CVB.SIM</ta>
            <ta e="T636" id="Seg_9886" s="T635">взять-PRS-3PL</ta>
            <ta e="T637" id="Seg_9887" s="T636">ребенок-PL-ACC</ta>
            <ta e="T638" id="Seg_9888" s="T637">радоваться-CVB.COND</ta>
            <ta e="T639" id="Seg_9889" s="T638">старик.[NOM]</ta>
            <ta e="T640" id="Seg_9890" s="T639">говорить-PST2.[3SG]</ta>
            <ta e="T641" id="Seg_9891" s="T640">старик.[NOM]</ta>
            <ta e="T642" id="Seg_9892" s="T641">дом-3SG-DAT/LOC</ta>
            <ta e="T643" id="Seg_9893" s="T642">идти-PTCP.PST-3SG-ACC</ta>
            <ta e="T644" id="Seg_9894" s="T643">после.того</ta>
            <ta e="T645" id="Seg_9895" s="T644">Анисим.[NOM]</ta>
            <ta e="T646" id="Seg_9896" s="T645">сам-3SG-ACC</ta>
            <ta e="T647" id="Seg_9897" s="T646">с</ta>
            <ta e="T648" id="Seg_9898" s="T647">разговаривать-PRS.[3SG]</ta>
            <ta e="T649" id="Seg_9899" s="T648">каждый</ta>
            <ta e="T650" id="Seg_9900" s="T649">человек-DAT/LOC</ta>
            <ta e="T651" id="Seg_9901" s="T650">помнить-EP-CAUS-CVB.SEQ</ta>
            <ta e="T652" id="Seg_9902" s="T651">идти-PTCP.FUT-DAT/LOC</ta>
            <ta e="T653" id="Seg_9903" s="T652">надо</ta>
            <ta e="T654" id="Seg_9904" s="T653">как</ta>
            <ta e="T655" id="Seg_9905" s="T654">новый</ta>
            <ta e="T656" id="Seg_9906" s="T655">закон.[NOM]</ta>
            <ta e="T657" id="Seg_9907" s="T656">добрый-ACC</ta>
            <ta e="T658" id="Seg_9908" s="T657">делать-PTCP.PRS-3SG-ACC</ta>
            <ta e="T659" id="Seg_9909" s="T658">тундра.[NOM]</ta>
            <ta e="T660" id="Seg_9910" s="T659">народ-3SG-GEN</ta>
            <ta e="T661" id="Seg_9911" s="T660">передняя.часть-3SG-DAT/LOC</ta>
            <ta e="T662" id="Seg_9912" s="T661">такой</ta>
            <ta e="T663" id="Seg_9913" s="T662">старик-PL.[NOM]</ta>
            <ta e="T664" id="Seg_9914" s="T663">новость.[NOM]</ta>
            <ta e="T665" id="Seg_9915" s="T664">послать-PTCP.PRS-3PL</ta>
            <ta e="T666" id="Seg_9916" s="T665">правда-PROPR.[NOM]</ta>
            <ta e="T667" id="Seg_9917" s="T666">силный-PROPR.[NOM]</ta>
            <ta e="T668" id="Seg_9918" s="T667">быть-FUT-3SG</ta>
            <ta e="T669" id="Seg_9919" s="T668">следующий</ta>
            <ta e="T670" id="Seg_9920" s="T669">день.[NOM]</ta>
            <ta e="T671" id="Seg_9921" s="T670">опять</ta>
            <ta e="T672" id="Seg_9922" s="T671">сход-DAT/LOC</ta>
            <ta e="T673" id="Seg_9923" s="T672">человек-PL.[NOM]</ta>
            <ta e="T674" id="Seg_9924" s="T673">собирать-PASS/REFL-EP-PST2-3PL</ta>
            <ta e="T676" id="Seg_9925" s="T675">Семен</ta>
            <ta e="T677" id="Seg_9926" s="T676">старик.[NOM]</ta>
            <ta e="T678" id="Seg_9927" s="T677">чум-3SG-DAT/LOC</ta>
            <ta e="T679" id="Seg_9928" s="T678">люди.[NOM]</ta>
            <ta e="T680" id="Seg_9929" s="T679">вчера-ADJZ-COMP</ta>
            <ta e="T681" id="Seg_9930" s="T680">как</ta>
            <ta e="T682" id="Seg_9931" s="T681">INDEF</ta>
            <ta e="T683" id="Seg_9932" s="T682">настроение-3PL.[NOM]</ta>
            <ta e="T684" id="Seg_9933" s="T683">поднимать-EP-PASS/REFL-EP-PST2.[3SG]</ta>
            <ta e="T685" id="Seg_9934" s="T684">шум.[NOM]</ta>
            <ta e="T686" id="Seg_9935" s="T685">смеяться-RECP/COLL-NMNZ.[NOM]-смеяться-RECP/COLL-NMNZ.[NOM]</ta>
            <ta e="T687" id="Seg_9936" s="T686">кончать-CVB.ANT-3SG</ta>
            <ta e="T688" id="Seg_9937" s="T687">родовой-3PL.[NOM]</ta>
            <ta e="T689" id="Seg_9938" s="T688">говорить-PST2.[3SG]</ta>
            <ta e="T690" id="Seg_9939" s="T689">сам-2PL.[NOM]</ta>
            <ta e="T691" id="Seg_9940" s="T690">желание-2PL-ABL</ta>
            <ta e="T692" id="Seg_9941" s="T691">маленький</ta>
            <ta e="T693" id="Seg_9942" s="T692">народ-PL-ACC</ta>
            <ta e="T694" id="Seg_9943" s="T693">держать-CAUS-EP-IMP.2PL</ta>
            <ta e="T695" id="Seg_9944" s="T694">большой</ta>
            <ta e="T696" id="Seg_9945" s="T695">ребенок-PL-DAT/LOC</ta>
            <ta e="T697" id="Seg_9946" s="T696">уезжать-PTCP.PRS</ta>
            <ta e="T698" id="Seg_9947" s="T697">час-DAT/LOC</ta>
            <ta e="T699" id="Seg_9948" s="T698">видеть-PTCP.PRS</ta>
            <ta e="T700" id="Seg_9949" s="T699">быть-PTCP.FUT-3PL-ACC</ta>
            <ta e="T701" id="Seg_9950" s="T700">3PL-ACC</ta>
            <ta e="T702" id="Seg_9951" s="T701">1PL-DAT/LOC</ta>
            <ta e="T703" id="Seg_9952" s="T702">говорить-PST2-3PL</ta>
            <ta e="T704" id="Seg_9953" s="T703">ребенок-PL-1PL.[NOM]</ta>
            <ta e="T705" id="Seg_9954" s="T704">зимой</ta>
            <ta e="T706" id="Seg_9955" s="T705">отдыхать-CVB.SIM</ta>
            <ta e="T707" id="Seg_9956" s="T706">приходить-FUT-3PL</ta>
            <ta e="T708" id="Seg_9957" s="T707">тот.[NOM]</ta>
            <ta e="T709" id="Seg_9958" s="T708">правда.[NOM]</ta>
            <ta e="T710" id="Seg_9959" s="T709">слово.[NOM]</ta>
            <ta e="T711" id="Seg_9960" s="T710">Q</ta>
            <ta e="T712" id="Seg_9961" s="T711">спрашивать-PST2.[3SG]</ta>
            <ta e="T713" id="Seg_9962" s="T712">один</ta>
            <ta e="T714" id="Seg_9963" s="T713">жена.[NOM]</ta>
            <ta e="T715" id="Seg_9964" s="T714">Семен</ta>
            <ta e="T716" id="Seg_9965" s="T715">старик.[NOM]</ta>
            <ta e="T717" id="Seg_9966" s="T716">сидеть-PTCP.PRS</ta>
            <ta e="T718" id="Seg_9967" s="T717">место-3SG-ABL</ta>
            <ta e="T719" id="Seg_9968" s="T718">обдумывать-EP</ta>
            <ta e="T720" id="Seg_9969" s="T719">обдумывать-EP-ITER-PST2.[3SG]</ta>
            <ta e="T721" id="Seg_9970" s="T720">тот.[NOM]</ta>
            <ta e="T722" id="Seg_9971" s="T721">правда.[NOM]</ta>
            <ta e="T723" id="Seg_9972" s="T722">новость.[NOM]</ta>
            <ta e="T724" id="Seg_9973" s="T723">зимой</ta>
            <ta e="T725" id="Seg_9974" s="T724">отдыхать-CVB.SIM</ta>
            <ta e="T726" id="Seg_9975" s="T725">приходить-FUT-3PL</ta>
            <ta e="T727" id="Seg_9976" s="T726">потом</ta>
            <ta e="T728" id="Seg_9977" s="T727">летом</ta>
            <ta e="T729" id="Seg_9978" s="T728">прямо</ta>
            <ta e="T730" id="Seg_9979" s="T729">дом-3PL-DAT/LOC</ta>
            <ta e="T731" id="Seg_9980" s="T730">быть-CVB.SIM</ta>
            <ta e="T732" id="Seg_9981" s="T731">приходить-FUT-3PL</ta>
            <ta e="T733" id="Seg_9982" s="T732">говорить-PST2.[3SG]</ta>
            <ta e="T734" id="Seg_9983" s="T733">Анисим.[NOM]</ta>
            <ta e="T735" id="Seg_9984" s="T734">Семен</ta>
            <ta e="T736" id="Seg_9985" s="T735">старик.[NOM]</ta>
            <ta e="T737" id="Seg_9986" s="T736">расслабиться-CVB.SIM</ta>
            <ta e="T738" id="Seg_9987" s="T737">падать-CVB.ANT</ta>
            <ta e="T739" id="Seg_9988" s="T738">слово.[NOM]</ta>
            <ta e="T742" id="Seg_9989" s="T741">вступать-EP-PST2.[3SG]</ta>
            <ta e="T743" id="Seg_9990" s="T742">1SG.[NOM]</ta>
            <ta e="T744" id="Seg_9991" s="T743">что-ACC</ta>
            <ta e="T745" id="Seg_9992" s="T744">говорить-PST2-EP-1SG=Q</ta>
            <ta e="T746" id="Seg_9993" s="T745">каждый-3PL.[NOM]</ta>
            <ta e="T747" id="Seg_9994" s="T746">теплый</ta>
            <ta e="T748" id="Seg_9995" s="T747">глаз-EP-INSTR</ta>
            <ta e="T749" id="Seg_9996" s="T748">видеть-CVB.SEQ</ta>
            <ta e="T750" id="Seg_9997" s="T749">Семен</ta>
            <ta e="T751" id="Seg_9998" s="T750">старик.[NOM]</ta>
            <ta e="T752" id="Seg_9999" s="T751">к</ta>
            <ta e="T753" id="Seg_10000" s="T752">вертеться-PASS/REFL-EP-PST2-3PL</ta>
            <ta e="T754" id="Seg_10001" s="T753">сход-3PL.[NOM]</ta>
            <ta e="T755" id="Seg_10002" s="T754">проехать-PST2.[3SG]</ta>
            <ta e="T756" id="Seg_10003" s="T755">быстрый-ADVZ</ta>
            <ta e="T757" id="Seg_10004" s="T756">человек-PL.[NOM]</ta>
            <ta e="T758" id="Seg_10005" s="T757">шест.[NOM]</ta>
            <ta e="T759" id="Seg_10006" s="T758">чум-3PL-ACC</ta>
            <ta e="T760" id="Seg_10007" s="T759">каждый</ta>
            <ta e="T761" id="Seg_10008" s="T760">идти-MULT-PST2-3PL</ta>
            <ta e="T762" id="Seg_10009" s="T761">следующий</ta>
            <ta e="T763" id="Seg_10010" s="T762">осень.[NOM]</ta>
            <ta e="T764" id="Seg_10011" s="T763">стадо-3PL-ACC</ta>
            <ta e="T765" id="Seg_10012" s="T764">%%-CVB.SEQ</ta>
            <ta e="T766" id="Seg_10013" s="T765">после</ta>
            <ta e="T767" id="Seg_10014" s="T766">олень.[NOM]</ta>
            <ta e="T768" id="Seg_10015" s="T767">хватать-NMNZ-3SG.[NOM]</ta>
            <ta e="T769" id="Seg_10016" s="T768">становиться-PST2.[3SG]</ta>
            <ta e="T770" id="Seg_10017" s="T769">уезжать-PTCP.PRS-DAT/LOC</ta>
            <ta e="T771" id="Seg_10018" s="T770">жена-PL.[NOM]</ta>
            <ta e="T772" id="Seg_10019" s="T771">чум-3PL-GEN</ta>
            <ta e="T773" id="Seg_10020" s="T772">место.около-DAT/LOC</ta>
            <ta e="T774" id="Seg_10021" s="T773">ребенок-PL-3SG-ACC</ta>
            <ta e="T775" id="Seg_10022" s="T774">с</ta>
            <ta e="T776" id="Seg_10023" s="T775">досуг-POSS</ta>
            <ta e="T777" id="Seg_10024" s="T776">NEG-3PL</ta>
            <ta e="T778" id="Seg_10025" s="T777">собирать-REFL-CVB.SEQ-3PL</ta>
            <ta e="T779" id="Seg_10026" s="T778">ребенок-PL-3SG-DAT/LOC</ta>
            <ta e="T780" id="Seg_10027" s="T779">прямо</ta>
            <ta e="T781" id="Seg_10028" s="T780">говорить-PRS-3PL</ta>
            <ta e="T782" id="Seg_10029" s="T781">двигаться-NEG.PTCP</ta>
            <ta e="T783" id="Seg_10030" s="T782">быть-PTCP.FUT-3PL-ACC</ta>
            <ta e="T784" id="Seg_10031" s="T783">большой</ta>
            <ta e="T785" id="Seg_10032" s="T784">ребенок-PL-ACC</ta>
            <ta e="T786" id="Seg_10033" s="T785">слышать-PTCP.PRS</ta>
            <ta e="T787" id="Seg_10034" s="T786">быть-PTCP.FUT-3PL-ACC</ta>
            <ta e="T788" id="Seg_10035" s="T787">олень.[NOM]</ta>
            <ta e="T789" id="Seg_10036" s="T788">держать-CVB.ANT</ta>
            <ta e="T790" id="Seg_10037" s="T789">запрячь-CVB.ANT</ta>
            <ta e="T791" id="Seg_10038" s="T790">шест.[NOM]</ta>
            <ta e="T792" id="Seg_10039" s="T791">чум.[NOM]</ta>
            <ta e="T793" id="Seg_10040" s="T792">каждый</ta>
            <ta e="T794" id="Seg_10041" s="T793">есть-PRS-3PL</ta>
            <ta e="T795" id="Seg_10042" s="T794">уезжать-PTCP.PRS</ta>
            <ta e="T796" id="Seg_10043" s="T795">передняя.часть-3SG-DAT/LOC</ta>
            <ta e="T797" id="Seg_10044" s="T796">потом</ta>
            <ta e="T798" id="Seg_10045" s="T797">шест.[NOM]</ta>
            <ta e="T799" id="Seg_10046" s="T798">чум.[NOM]</ta>
            <ta e="T800" id="Seg_10047" s="T799">каждый-ABL</ta>
            <ta e="T801" id="Seg_10048" s="T800">каждый-3PL.[NOM]</ta>
            <ta e="T802" id="Seg_10049" s="T801">вспыхивать-RECP/COLL-CVB.SEQ</ta>
            <ta e="T803" id="Seg_10050" s="T802">выйти-EP-PST2-3PL</ta>
            <ta e="T804" id="Seg_10051" s="T803">далекий.[NOM]</ta>
            <ta e="T805" id="Seg_10052" s="T804">идти-PTCP.PRS</ta>
            <ta e="T806" id="Seg_10053" s="T805">ребенок-PL-ACC</ta>
            <ta e="T807" id="Seg_10054" s="T806">провожать-CVB.PURP</ta>
            <ta e="T808" id="Seg_10055" s="T807">когда</ta>
            <ta e="T809" id="Seg_10056" s="T808">1SG.[NOM]</ta>
            <ta e="T810" id="Seg_10057" s="T809">2SG-ACC</ta>
            <ta e="T811" id="Seg_10058" s="T810">видеть-FUT-1SG=Q</ta>
            <ta e="T812" id="Seg_10059" s="T811">Евгения.[NOM]</ta>
            <ta e="T813" id="Seg_10060" s="T812">плакать-PST2.[3SG]</ta>
            <ta e="T814" id="Seg_10061" s="T813">как</ta>
            <ta e="T815" id="Seg_10062" s="T814">2SG-ACC</ta>
            <ta e="T818" id="Seg_10063" s="T817">2SG-ACC-POSS</ta>
            <ta e="T819" id="Seg_10064" s="T818">NEG</ta>
            <ta e="T820" id="Seg_10065" s="T819">жить-FUT-1SG=Q</ta>
            <ta e="T821" id="Seg_10066" s="T820">кто.[NOM]</ta>
            <ta e="T822" id="Seg_10067" s="T821">1PL-DAT/LOC</ta>
            <ta e="T823" id="Seg_10068" s="T822">помогать-FUT-3SG=Q</ta>
            <ta e="T824" id="Seg_10069" s="T823">ребенок-3SG-ACC</ta>
            <ta e="T825" id="Seg_10070" s="T824">крепко</ta>
            <ta e="T826" id="Seg_10071" s="T825">хватать-CVB.ANT</ta>
            <ta e="T827" id="Seg_10072" s="T826">после</ta>
            <ta e="T828" id="Seg_10073" s="T827">чум-3SG-ACC</ta>
            <ta e="T829" id="Seg_10074" s="T828">к</ta>
            <ta e="T830" id="Seg_10075" s="T829">тащить-PRS.[3SG]</ta>
            <ta e="T831" id="Seg_10076" s="T830">2SG.[NOM]</ta>
            <ta e="T832" id="Seg_10077" s="T831">что.[NOM]</ta>
            <ta e="T833" id="Seg_10078" s="T832">становиться-PRS-2SG</ta>
            <ta e="T834" id="Seg_10079" s="T833">один</ta>
            <ta e="T835" id="Seg_10080" s="T834">один</ta>
            <ta e="T836" id="Seg_10081" s="T835">жена.[NOM]</ta>
            <ta e="T837" id="Seg_10082" s="T836">говорить-PST2.[3SG]</ta>
            <ta e="T838" id="Seg_10083" s="T837">друг-3SG-DAT/LOC</ta>
            <ta e="T839" id="Seg_10084" s="T838">ребенок-ACC</ta>
            <ta e="T840" id="Seg_10085" s="T839">думать-PTCP.PRS-DAT/LOC</ta>
            <ta e="T841" id="Seg_10086" s="T840">перевешивать-EP-NEG.[IMP.2SG]</ta>
            <ta e="T842" id="Seg_10087" s="T841">уезжать-PTCP.PRS.[NOM]</ta>
            <ta e="T843" id="Seg_10088" s="T842">передняя.часть-3SG-DAT/LOC</ta>
            <ta e="T844" id="Seg_10089" s="T843">такой</ta>
            <ta e="T845" id="Seg_10090" s="T844">провожать-PTCP.PRS.[NOM]</ta>
            <ta e="T846" id="Seg_10091" s="T845">очень</ta>
            <ta e="T847" id="Seg_10092" s="T846">плохой.[NOM]</ta>
            <ta e="T848" id="Seg_10093" s="T847">грех.[NOM]</ta>
            <ta e="T849" id="Seg_10094" s="T848">провожать.[IMP.2SG]</ta>
            <ta e="T850" id="Seg_10095" s="T849">человек-SIM</ta>
            <ta e="T851" id="Seg_10096" s="T850">каждый</ta>
            <ta e="T852" id="Seg_10097" s="T851">верховой.олень-PROPR</ta>
            <ta e="T853" id="Seg_10098" s="T852">ребенок-PL.[NOM]</ta>
            <ta e="T854" id="Seg_10099" s="T853">дедушка-3PL-GEN</ta>
            <ta e="T855" id="Seg_10100" s="T854">задняя.часть-3SG-ABL</ta>
            <ta e="T856" id="Seg_10101" s="T855">идти-CVB.SEQ</ta>
            <ta e="T857" id="Seg_10102" s="T856">длинный</ta>
            <ta e="T858" id="Seg_10103" s="T857">аргиш.[NOM]</ta>
            <ta e="T859" id="Seg_10104" s="T858">становиться-PST2-3PL</ta>
            <ta e="T860" id="Seg_10105" s="T859">стоять-NEG.PTCP-стоять-PTCP.PRS</ta>
            <ta e="T861" id="Seg_10106" s="T860">ребенок-3PL-ACC</ta>
            <ta e="T862" id="Seg_10107" s="T861">провожать-CVB.SEQ</ta>
            <ta e="T863" id="Seg_10108" s="T862">тот</ta>
            <ta e="T864" id="Seg_10109" s="T863">аргиш.[NOM]</ta>
            <ta e="T865" id="Seg_10110" s="T864">задняя.часть-3SG-ABL</ta>
            <ta e="T866" id="Seg_10111" s="T865">растянуться-PST2-3PL</ta>
            <ta e="T867" id="Seg_10112" s="T866">Порфирий</ta>
            <ta e="T868" id="Seg_10113" s="T867">старик.[NOM]</ta>
            <ta e="T869" id="Seg_10114" s="T868">глаз-PL-3SG-ACC</ta>
            <ta e="T870" id="Seg_10115" s="T869">выкатывать-ADVZ</ta>
            <ta e="T871" id="Seg_10116" s="T870">видеть-CVB.SEQ</ta>
            <ta e="T872" id="Seg_10117" s="T871">вставать-CVB.SEQ</ta>
            <ta e="T873" id="Seg_10118" s="T872">последний</ta>
            <ta e="T874" id="Seg_10119" s="T873">сила-3SG-INSTR</ta>
            <ta e="T875" id="Seg_10120" s="T874">благословлять-CVB.SIM</ta>
            <ta e="T876" id="Seg_10121" s="T875">стоять-PST2.[3SG]</ta>
            <ta e="T877" id="Seg_10122" s="T876">вот</ta>
            <ta e="T878" id="Seg_10123" s="T877">счастливый</ta>
            <ta e="T879" id="Seg_10124" s="T878">идти-EP-IMP.2PL</ta>
            <ta e="T880" id="Seg_10125" s="T879">2PL.[NOM]</ta>
            <ta e="T881" id="Seg_10126" s="T880">дорога-2PL-DAT/LOC</ta>
            <ta e="T882" id="Seg_10127" s="T881">кто.[NOM]</ta>
            <ta e="T883" id="Seg_10128" s="T882">NEG</ta>
            <ta e="T884" id="Seg_10129" s="T883">мешать-NEG.PTCP-3SG-ACC</ta>
            <ta e="T885" id="Seg_10130" s="T884">большой</ta>
            <ta e="T886" id="Seg_10131" s="T885">ум-ACC</ta>
            <ta e="T887" id="Seg_10132" s="T886">взять-EP-IMP.2PL</ta>
            <ta e="T888" id="Seg_10133" s="T887">быстрый-ADVZ</ta>
            <ta e="T889" id="Seg_10134" s="T888">возвращаться-EP-IMP.2PL</ta>
            <ta e="T890" id="Seg_10135" s="T889">родиться-PTCP.PST</ta>
            <ta e="T891" id="Seg_10136" s="T890">место-2PL-DAT/LOC</ta>
            <ta e="T892" id="Seg_10137" s="T891">оставаться-CVB.SIM</ta>
            <ta e="T893" id="Seg_10138" s="T892">люди.[NOM]</ta>
            <ta e="T894" id="Seg_10139" s="T893">долго</ta>
            <ta e="T895" id="Seg_10140" s="T894">очень</ta>
            <ta e="T896" id="Seg_10141" s="T895">видеть-CVB.SIM</ta>
            <ta e="T897" id="Seg_10142" s="T896">стоять-PST2-3PL</ta>
            <ta e="T898" id="Seg_10143" s="T897">тундра.[NOM]</ta>
            <ta e="T899" id="Seg_10144" s="T898">верхняя.часть-3SG-INSTR</ta>
            <ta e="T900" id="Seg_10145" s="T899">длинный</ta>
            <ta e="T901" id="Seg_10146" s="T900">аргиш.[NOM]</ta>
            <ta e="T902" id="Seg_10147" s="T901">пропадать-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T903" id="Seg_10148" s="T902">пока</ta>
            <ta e="T904" id="Seg_10149" s="T903">пригорок.[NOM]</ta>
            <ta e="T905" id="Seg_10150" s="T904">далекий</ta>
            <ta e="T906" id="Seg_10151" s="T905">сторона-3SG-DAT/LOC</ta>
            <ta e="T907" id="Seg_10152" s="T906">ребенок</ta>
            <ta e="T908" id="Seg_10153" s="T907">народ-DAT/LOC</ta>
            <ta e="T909" id="Seg_10154" s="T908">корень-VBZ-PST1-3SG</ta>
            <ta e="T910" id="Seg_10155" s="T909">новый</ta>
            <ta e="T911" id="Seg_10156" s="T910">время.[NOM]</ta>
            <ta e="T912" id="Seg_10157" s="T911">учиться-PTCP.PRS</ta>
            <ta e="T913" id="Seg_10158" s="T912">время.[NOM]</ta>
            <ta e="T914" id="Seg_10159" s="T913">письмо-DAT/LOC</ta>
            <ta e="T915" id="Seg_10160" s="T914">2PL.[NOM]</ta>
            <ta e="T916" id="Seg_10161" s="T915">слушать-PST1-2PL</ta>
            <ta e="T917" id="Seg_10162" s="T916">рассказ-ACC</ta>
            <ta e="T918" id="Seg_10163" s="T917">Волочанка.[NOM]</ta>
            <ta e="T919" id="Seg_10164" s="T918">к</ta>
            <ta e="T920" id="Seg_10165" s="T919">кочевать-NMNZ.[NOM]</ta>
            <ta e="T921" id="Seg_10166" s="T920">читать-PST2-3SG</ta>
            <ta e="T922" id="Seg_10167" s="T921">Попов.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_10168" s="T0">propr-n:case</ta>
            <ta e="T2" id="Seg_10169" s="T1">post</ta>
            <ta e="T3" id="Seg_10170" s="T2">v-v&gt;n-n:case</ta>
            <ta e="T4" id="Seg_10171" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_10172" s="T4">n-n:(poss)-n:case</ta>
            <ta e="T6" id="Seg_10173" s="T5">v-v:ptcp</ta>
            <ta e="T7" id="Seg_10174" s="T6">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T8" id="Seg_10175" s="T7">v-v:tense-v:poss.pn</ta>
            <ta e="T9" id="Seg_10176" s="T8">adj-n:case</ta>
            <ta e="T10" id="Seg_10177" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_10178" s="T10">n-n:(num)-n:case</ta>
            <ta e="T12" id="Seg_10179" s="T11">n-n:(num)-n:case</ta>
            <ta e="T13" id="Seg_10180" s="T12">v-v:tense-v:pred.pn</ta>
            <ta e="T14" id="Seg_10181" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_10182" s="T14">adv-adv</ta>
            <ta e="T16" id="Seg_10183" s="T15">adj</ta>
            <ta e="T17" id="Seg_10184" s="T16">n-n:case</ta>
            <ta e="T18" id="Seg_10185" s="T17">v-v:cvb</ta>
            <ta e="T19" id="Seg_10186" s="T18">v-v:tense-v:pred.pn</ta>
            <ta e="T20" id="Seg_10187" s="T19">dempro</ta>
            <ta e="T21" id="Seg_10188" s="T20">n-v:(num)-n:case</ta>
            <ta e="T22" id="Seg_10189" s="T21">v-v&gt;n-n&gt;adj</ta>
            <ta e="T23" id="Seg_10190" s="T22">adj</ta>
            <ta e="T24" id="Seg_10191" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_10192" s="T24">n-n:poss-n:case</ta>
            <ta e="T26" id="Seg_10193" s="T25">adj</ta>
            <ta e="T27" id="Seg_10194" s="T26">v-v&gt;v-v&gt;n-n:case</ta>
            <ta e="T28" id="Seg_10195" s="T27">v-v:tense-v:poss.pn</ta>
            <ta e="T29" id="Seg_10196" s="T28">n-n:case</ta>
            <ta e="T30" id="Seg_10197" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_10198" s="T30">v-v:tense-v:pred.pn</ta>
            <ta e="T32" id="Seg_10199" s="T31">propr-n:case</ta>
            <ta e="T33" id="Seg_10200" s="T32">v-v:cvb</ta>
            <ta e="T34" id="Seg_10201" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_10202" s="T34">n-n:case</ta>
            <ta e="T36" id="Seg_10203" s="T35">n-n:case</ta>
            <ta e="T37" id="Seg_10204" s="T36">adv</ta>
            <ta e="T38" id="Seg_10205" s="T37">v-v:ptcp</ta>
            <ta e="T39" id="Seg_10206" s="T38">v-v:tense-v:poss.pn</ta>
            <ta e="T40" id="Seg_10207" s="T39">n-n:(num)-n:poss-n:case</ta>
            <ta e="T41" id="Seg_10208" s="T40">n-n:case</ta>
            <ta e="T42" id="Seg_10209" s="T41">v-v:ptcp-v:(case)</ta>
            <ta e="T43" id="Seg_10210" s="T42">dempro-pro:case</ta>
            <ta e="T44" id="Seg_10211" s="T43">post</ta>
            <ta e="T45" id="Seg_10212" s="T44">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T46" id="Seg_10213" s="T45">v-v:(neg)-v:pred.pn</ta>
            <ta e="T47" id="Seg_10214" s="T46">n-n:case</ta>
            <ta e="T48" id="Seg_10215" s="T47">v-v:ptcp-v:(case)</ta>
            <ta e="T49" id="Seg_10216" s="T48">adj</ta>
            <ta e="T50" id="Seg_10217" s="T49">n-n:case</ta>
            <ta e="T51" id="Seg_10218" s="T50">v-v:tense-v:pred.pn</ta>
            <ta e="T52" id="Seg_10219" s="T51">v-v:(ins)-v:(neg)-v:cvb</ta>
            <ta e="T53" id="Seg_10220" s="T52">n-n:(num)-n:poss-n:case</ta>
            <ta e="T54" id="Seg_10221" s="T53">adj-n:poss-n:case</ta>
            <ta e="T55" id="Seg_10222" s="T54">dempro</ta>
            <ta e="T56" id="Seg_10223" s="T55">n-n:(num)-n:case</ta>
            <ta e="T57" id="Seg_10224" s="T56">adv</ta>
            <ta e="T58" id="Seg_10225" s="T57">n-n&gt;adj</ta>
            <ta e="T59" id="Seg_10226" s="T58">n-n:case</ta>
            <ta e="T60" id="Seg_10227" s="T59">v-v:tense-v:poss.pn</ta>
            <ta e="T61" id="Seg_10228" s="T60">adj</ta>
            <ta e="T62" id="Seg_10229" s="T61">n-n:case</ta>
            <ta e="T63" id="Seg_10230" s="T62">n-n:case</ta>
            <ta e="T64" id="Seg_10231" s="T63">adj</ta>
            <ta e="T65" id="Seg_10232" s="T64">v-v:cvb</ta>
            <ta e="T66" id="Seg_10233" s="T65">v-v:cvb</ta>
            <ta e="T67" id="Seg_10234" s="T66">v-v:tense-v:pred.pn</ta>
            <ta e="T68" id="Seg_10235" s="T67">n-n:(num)-n:poss-n:case</ta>
            <ta e="T69" id="Seg_10236" s="T68">n-n:case</ta>
            <ta e="T70" id="Seg_10237" s="T69">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T71" id="Seg_10238" s="T70">n-n:case</ta>
            <ta e="T72" id="Seg_10239" s="T71">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T73" id="Seg_10240" s="T72">dempro-pro:case</ta>
            <ta e="T74" id="Seg_10241" s="T73">que-pro:case</ta>
            <ta e="T75" id="Seg_10242" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_10243" s="T75">v-v:(ins)-v:neg-v:pred.pn</ta>
            <ta e="T77" id="Seg_10244" s="T76">dempro-pro:case</ta>
            <ta e="T78" id="Seg_10245" s="T77">post</ta>
            <ta e="T79" id="Seg_10246" s="T78">n-n:case</ta>
            <ta e="T80" id="Seg_10247" s="T79">v-v:tense-v:pred.pn</ta>
            <ta e="T81" id="Seg_10248" s="T80">v-v:ptcp-v:(case)</ta>
            <ta e="T82" id="Seg_10249" s="T81">v-v:cvb</ta>
            <ta e="T83" id="Seg_10250" s="T82">n-n:case</ta>
            <ta e="T84" id="Seg_10251" s="T83">n-n:case</ta>
            <ta e="T85" id="Seg_10252" s="T84">n-n:poss-n:case</ta>
            <ta e="T86" id="Seg_10253" s="T85">adj-n:(poss)-n:case</ta>
            <ta e="T87" id="Seg_10254" s="T86">v-v&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T88" id="Seg_10255" s="T87">n-n:case</ta>
            <ta e="T89" id="Seg_10256" s="T88">n-n:case</ta>
            <ta e="T90" id="Seg_10257" s="T89">v-v&gt;n-n:case</ta>
            <ta e="T91" id="Seg_10258" s="T90">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T92" id="Seg_10259" s="T91">cardnum</ta>
            <ta e="T93" id="Seg_10260" s="T92">adj</ta>
            <ta e="T94" id="Seg_10261" s="T93">n-n:case</ta>
            <ta e="T95" id="Seg_10262" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_10263" s="T95">adv</ta>
            <ta e="T97" id="Seg_10264" s="T96">v-v:cvb</ta>
            <ta e="T98" id="Seg_10265" s="T97">v-v&gt;v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T99" id="Seg_10266" s="T98">v-v:tense-v:pred.pn</ta>
            <ta e="T100" id="Seg_10267" s="T99">v-v:(neg)-v:pred.pn</ta>
            <ta e="T101" id="Seg_10268" s="T100">n-n:case</ta>
            <ta e="T102" id="Seg_10269" s="T101">n-n:(num)-n:case</ta>
            <ta e="T103" id="Seg_10270" s="T102">que-pro:case</ta>
            <ta e="T104" id="Seg_10271" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_10272" s="T104">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T106" id="Seg_10273" s="T105">n-n:poss-n:case</ta>
            <ta e="T107" id="Seg_10274" s="T106">n-n:case</ta>
            <ta e="T108" id="Seg_10275" s="T107">post</ta>
            <ta e="T109" id="Seg_10276" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_10277" s="T109">v-v:tense-v:pred.pn</ta>
            <ta e="T111" id="Seg_10278" s="T110">n-n:case</ta>
            <ta e="T112" id="Seg_10279" s="T111">n-n:case</ta>
            <ta e="T113" id="Seg_10280" s="T112">n-n:poss-n:case</ta>
            <ta e="T114" id="Seg_10281" s="T113">n-n:poss-n:case</ta>
            <ta e="T115" id="Seg_10282" s="T114">n-n:poss-n:case</ta>
            <ta e="T116" id="Seg_10283" s="T115">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T117" id="Seg_10284" s="T116">n-n:(num)-n:case</ta>
            <ta e="T118" id="Seg_10285" s="T117">que-pro:case</ta>
            <ta e="T119" id="Seg_10286" s="T118">v-v:cvb-v:pred.pn</ta>
            <ta e="T120" id="Seg_10287" s="T119">n-n:(num)-n:poss-n:case</ta>
            <ta e="T121" id="Seg_10288" s="T120">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T122" id="Seg_10289" s="T121">n-n:poss-n:case</ta>
            <ta e="T123" id="Seg_10290" s="T122">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T124" id="Seg_10291" s="T123">que</ta>
            <ta e="T125" id="Seg_10292" s="T124">v-v&gt;v-v:cvb</ta>
            <ta e="T126" id="Seg_10293" s="T125">v-v:tense-v:pred.pn</ta>
            <ta e="T127" id="Seg_10294" s="T126">v-v:tense-v:pred.pn</ta>
            <ta e="T128" id="Seg_10295" s="T127">v-v:cvb</ta>
            <ta e="T129" id="Seg_10296" s="T128">propr</ta>
            <ta e="T130" id="Seg_10297" s="T129">n-n&gt;adj</ta>
            <ta e="T131" id="Seg_10298" s="T130">n-n:case</ta>
            <ta e="T132" id="Seg_10299" s="T131">v-v:cvb</ta>
            <ta e="T133" id="Seg_10300" s="T132">v-v:tense-v:pred.pn</ta>
            <ta e="T134" id="Seg_10301" s="T133">pers-pro:case</ta>
            <ta e="T135" id="Seg_10302" s="T134">ptcl</ta>
            <ta e="T136" id="Seg_10303" s="T135">adj-n:(poss)-n:case</ta>
            <ta e="T137" id="Seg_10304" s="T136">adv</ta>
            <ta e="T138" id="Seg_10305" s="T137">que-pro:case</ta>
            <ta e="T139" id="Seg_10306" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_10307" s="T139">v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T141" id="Seg_10308" s="T140">v-v:ptcp</ta>
            <ta e="T142" id="Seg_10309" s="T141">n-n:case</ta>
            <ta e="T143" id="Seg_10310" s="T142">adv</ta>
            <ta e="T144" id="Seg_10311" s="T143">v-v:tense-v:pred.pn</ta>
            <ta e="T145" id="Seg_10312" s="T144">propr</ta>
            <ta e="T146" id="Seg_10313" s="T145">n-n:case</ta>
            <ta e="T147" id="Seg_10314" s="T146">adj</ta>
            <ta e="T148" id="Seg_10315" s="T147">n-n:poss-n:case</ta>
            <ta e="T149" id="Seg_10316" s="T148">v-v:cvb</ta>
            <ta e="T150" id="Seg_10317" s="T149">v-v:cvb</ta>
            <ta e="T151" id="Seg_10318" s="T150">v-v:cvb</ta>
            <ta e="T152" id="Seg_10319" s="T151">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T153" id="Seg_10320" s="T152">que</ta>
            <ta e="T154" id="Seg_10321" s="T153">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T155" id="Seg_10322" s="T154">dempro-pro:case</ta>
            <ta e="T156" id="Seg_10323" s="T155">adv</ta>
            <ta e="T157" id="Seg_10324" s="T156">v-v:tense-v:pred.pn</ta>
            <ta e="T158" id="Seg_10325" s="T157">n-n:(num)-n:case</ta>
            <ta e="T159" id="Seg_10326" s="T158">dempro</ta>
            <ta e="T160" id="Seg_10327" s="T159">adj</ta>
            <ta e="T161" id="Seg_10328" s="T160">n-n:case</ta>
            <ta e="T162" id="Seg_10329" s="T161">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T163" id="Seg_10330" s="T162">ptcl</ta>
            <ta e="T164" id="Seg_10331" s="T163">ptcl</ta>
            <ta e="T165" id="Seg_10332" s="T164">adv</ta>
            <ta e="T166" id="Seg_10333" s="T165">adj-n:case</ta>
            <ta e="T167" id="Seg_10334" s="T166">pers-pro:case</ta>
            <ta e="T168" id="Seg_10335" s="T167">n-n&gt;n-n:case</ta>
            <ta e="T169" id="Seg_10336" s="T168">v-v:ptcp</ta>
            <ta e="T170" id="Seg_10337" s="T169">v-v:mood-v:pred.pn</ta>
            <ta e="T171" id="Seg_10338" s="T170">dempro</ta>
            <ta e="T172" id="Seg_10339" s="T171">pers-pro:case</ta>
            <ta e="T173" id="Seg_10340" s="T172">propr-n:case</ta>
            <ta e="T174" id="Seg_10341" s="T173">n-n:poss-n:case</ta>
            <ta e="T175" id="Seg_10342" s="T174">adj-n:poss-n:case</ta>
            <ta e="T176" id="Seg_10343" s="T175">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T177" id="Seg_10344" s="T176">n-n:case</ta>
            <ta e="T178" id="Seg_10345" s="T177">n-n:case</ta>
            <ta e="T179" id="Seg_10346" s="T178">n-n&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T180" id="Seg_10347" s="T179">pers-pro:case</ta>
            <ta e="T181" id="Seg_10348" s="T180">adv</ta>
            <ta e="T182" id="Seg_10349" s="T181">v-v:tense-v:poss.pn</ta>
            <ta e="T183" id="Seg_10350" s="T182">n-n:case</ta>
            <ta e="T184" id="Seg_10351" s="T183">v-v:(ins)-v&gt;v-v:ptcp</ta>
            <ta e="T185" id="Seg_10352" s="T184">v-v:cvb</ta>
            <ta e="T186" id="Seg_10353" s="T185">v-v:(ins)-v:pred.pn</ta>
            <ta e="T187" id="Seg_10354" s="T186">que-pro:case</ta>
            <ta e="T188" id="Seg_10355" s="T187">pers-pro:case</ta>
            <ta e="T189" id="Seg_10356" s="T188">n-n:poss-n:case</ta>
            <ta e="T190" id="Seg_10357" s="T189">v-v:(ins)-v&gt;v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T191" id="Seg_10358" s="T190">n-n:(poss)-n:case</ta>
            <ta e="T192" id="Seg_10359" s="T191">adj-n:case</ta>
            <ta e="T193" id="Seg_10360" s="T192">adv</ta>
            <ta e="T194" id="Seg_10361" s="T193">n-n&gt;adj-n:case</ta>
            <ta e="T195" id="Seg_10362" s="T194">adj</ta>
            <ta e="T196" id="Seg_10363" s="T195">ptcl</ta>
            <ta e="T197" id="Seg_10364" s="T196">adj-n:case</ta>
            <ta e="T198" id="Seg_10365" s="T197">ptcl</ta>
            <ta e="T199" id="Seg_10366" s="T198">n-n:case</ta>
            <ta e="T200" id="Seg_10367" s="T199">n-n:case</ta>
            <ta e="T201" id="Seg_10368" s="T200">v-v:mood-v:pred.pn</ta>
            <ta e="T202" id="Seg_10369" s="T201">propr-n:case</ta>
            <ta e="T203" id="Seg_10370" s="T202">n-n:case</ta>
            <ta e="T205" id="Seg_10371" s="T204">n-n:case</ta>
            <ta e="T206" id="Seg_10372" s="T205">n-n:case</ta>
            <ta e="T207" id="Seg_10373" s="T206">v-v:cvb</ta>
            <ta e="T208" id="Seg_10374" s="T207">v-v:tense-v:poss.pn</ta>
            <ta e="T209" id="Seg_10375" s="T208">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T210" id="Seg_10376" s="T209">pers-pro:case</ta>
            <ta e="T211" id="Seg_10377" s="T210">v-v:tense-v:poss.pn</ta>
            <ta e="T212" id="Seg_10378" s="T211">n-n:case</ta>
            <ta e="T213" id="Seg_10379" s="T212">n-n:poss-n:case</ta>
            <ta e="T214" id="Seg_10380" s="T213">n-n:case</ta>
            <ta e="T215" id="Seg_10381" s="T214">post</ta>
            <ta e="T216" id="Seg_10382" s="T215">n-n:case</ta>
            <ta e="T217" id="Seg_10383" s="T216">ptcl</ta>
            <ta e="T218" id="Seg_10384" s="T217">v-v:tense-v:poss.pn</ta>
            <ta e="T219" id="Seg_10385" s="T218">que-pro:case</ta>
            <ta e="T220" id="Seg_10386" s="T219">ptcl</ta>
            <ta e="T221" id="Seg_10387" s="T220">v-v:cvb</ta>
            <ta e="T222" id="Seg_10388" s="T221">pers-pro:case</ta>
            <ta e="T223" id="Seg_10389" s="T222">n-n:case</ta>
            <ta e="T224" id="Seg_10390" s="T223">n-n:(num)-n:case</ta>
            <ta e="T225" id="Seg_10391" s="T224">v-v:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T226" id="Seg_10392" s="T225">ptcl</ta>
            <ta e="T227" id="Seg_10393" s="T226">propr-n:case</ta>
            <ta e="T228" id="Seg_10394" s="T227">n-n:(poss)-n:case</ta>
            <ta e="T229" id="Seg_10395" s="T228">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T230" id="Seg_10396" s="T229">que-pro:case</ta>
            <ta e="T231" id="Seg_10397" s="T230">pers-pro:case</ta>
            <ta e="T232" id="Seg_10398" s="T231">n-n:case</ta>
            <ta e="T233" id="Seg_10399" s="T232">v-v:tense-v:pred.pn</ta>
            <ta e="T234" id="Seg_10400" s="T233">n-n:poss-n:case</ta>
            <ta e="T235" id="Seg_10401" s="T234">adj-adj&gt;adv</ta>
            <ta e="T236" id="Seg_10402" s="T235">v-v:tense-v:pred.pn</ta>
            <ta e="T237" id="Seg_10403" s="T236">n-n:(num)-n:poss-n:case</ta>
            <ta e="T238" id="Seg_10404" s="T237">n-n:poss-n:case</ta>
            <ta e="T239" id="Seg_10405" s="T238">v-v:tense-v:pred.pn</ta>
            <ta e="T240" id="Seg_10406" s="T239">dempro-pro:case</ta>
            <ta e="T241" id="Seg_10407" s="T240">n-n:case</ta>
            <ta e="T242" id="Seg_10408" s="T241">adv</ta>
            <ta e="T243" id="Seg_10409" s="T242">v-v:tense-v:pred.pn</ta>
            <ta e="T244" id="Seg_10410" s="T243">adv</ta>
            <ta e="T245" id="Seg_10411" s="T244">v-v&gt;v-v:cvb</ta>
            <ta e="T246" id="Seg_10412" s="T245">propr-n:case</ta>
            <ta e="T247" id="Seg_10413" s="T246">v-v:cvb</ta>
            <ta e="T248" id="Seg_10414" s="T247">v-v:tense-v:pred.pn</ta>
            <ta e="T249" id="Seg_10415" s="T248">pers-pro:case</ta>
            <ta e="T250" id="Seg_10416" s="T249">propr</ta>
            <ta e="T251" id="Seg_10417" s="T250">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T252" id="Seg_10418" s="T251">v-v:cvb</ta>
            <ta e="T253" id="Seg_10419" s="T252">v-v:tense-v:pred.pn</ta>
            <ta e="T254" id="Seg_10420" s="T253">n-n:case</ta>
            <ta e="T255" id="Seg_10421" s="T254">n-n:case</ta>
            <ta e="T256" id="Seg_10422" s="T255">v-v:cvb</ta>
            <ta e="T257" id="Seg_10423" s="T256">v-v:tense-v:pred.pn</ta>
            <ta e="T258" id="Seg_10424" s="T257">n-n:case</ta>
            <ta e="T259" id="Seg_10425" s="T258">ptcl</ta>
            <ta e="T260" id="Seg_10426" s="T259">ptcl</ta>
            <ta e="T261" id="Seg_10427" s="T260">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T262" id="Seg_10428" s="T261">v-v:tense-v:poss.pn</ta>
            <ta e="T263" id="Seg_10429" s="T262">n-n:case</ta>
            <ta e="T264" id="Seg_10430" s="T263">n-n:case</ta>
            <ta e="T265" id="Seg_10431" s="T264">post</ta>
            <ta e="T266" id="Seg_10432" s="T265">v-v&gt;v-v:cvb</ta>
            <ta e="T267" id="Seg_10433" s="T266">v-v:tense-v:poss.pn</ta>
            <ta e="T268" id="Seg_10434" s="T267">n-n:case</ta>
            <ta e="T269" id="Seg_10435" s="T268">v-v:cvb</ta>
            <ta e="T270" id="Seg_10436" s="T269">que</ta>
            <ta e="T271" id="Seg_10437" s="T270">pers-pro:case</ta>
            <ta e="T272" id="Seg_10438" s="T271">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T273" id="Seg_10439" s="T272">n-n:(num)-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T274" id="Seg_10440" s="T273">v-v:mood-v:temp.pn</ta>
            <ta e="T275" id="Seg_10441" s="T274">adj-n:(poss)-n:case</ta>
            <ta e="T276" id="Seg_10442" s="T275">propr</ta>
            <ta e="T277" id="Seg_10443" s="T276">propr</ta>
            <ta e="T278" id="Seg_10444" s="T277">propr-n:case</ta>
            <ta e="T279" id="Seg_10445" s="T278">v-v:cvb</ta>
            <ta e="T280" id="Seg_10446" s="T279">v-v:cvb</ta>
            <ta e="T281" id="Seg_10447" s="T280">n-n:case</ta>
            <ta e="T282" id="Seg_10448" s="T281">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T283" id="Seg_10449" s="T282">ptcl</ta>
            <ta e="T284" id="Seg_10450" s="T283">n-n:case</ta>
            <ta e="T285" id="Seg_10451" s="T284">pers-pro:case</ta>
            <ta e="T286" id="Seg_10452" s="T285">v-v:tense-v:poss.pn</ta>
            <ta e="T287" id="Seg_10453" s="T286">adv</ta>
            <ta e="T288" id="Seg_10454" s="T287">v-v:cvb</ta>
            <ta e="T289" id="Seg_10455" s="T288">v-v:mood.pn</ta>
            <ta e="T290" id="Seg_10456" s="T289">adj</ta>
            <ta e="T291" id="Seg_10457" s="T290">n-n:case</ta>
            <ta e="T292" id="Seg_10458" s="T291">v-v:mood.pn</ta>
            <ta e="T293" id="Seg_10459" s="T292">n-n:case</ta>
            <ta e="T294" id="Seg_10460" s="T293">que-pro:case</ta>
            <ta e="T295" id="Seg_10461" s="T294">ptcl</ta>
            <ta e="T296" id="Seg_10462" s="T295">v-v:cvb</ta>
            <ta e="T297" id="Seg_10463" s="T296">adv</ta>
            <ta e="T298" id="Seg_10464" s="T297">n-n:poss-n:case</ta>
            <ta e="T299" id="Seg_10465" s="T298">interj</ta>
            <ta e="T300" id="Seg_10466" s="T299">v-v:cvb</ta>
            <ta e="T301" id="Seg_10467" s="T300">v-v:tense-v:pred.pn</ta>
            <ta e="T302" id="Seg_10468" s="T301">n-n:(num)-n:poss-n:case</ta>
            <ta e="T303" id="Seg_10469" s="T302">v-v:cvb</ta>
            <ta e="T304" id="Seg_10470" s="T303">v-v:cvb</ta>
            <ta e="T305" id="Seg_10471" s="T304">que-pro:case</ta>
            <ta e="T306" id="Seg_10472" s="T305">adv</ta>
            <ta e="T307" id="Seg_10473" s="T306">que-pro:case</ta>
            <ta e="T308" id="Seg_10474" s="T307">n-n&gt;adj-n:case-ptcl</ta>
            <ta e="T309" id="Seg_10475" s="T308">v-v:cvb</ta>
            <ta e="T310" id="Seg_10476" s="T309">v-v:tense-v:pred.pn</ta>
            <ta e="T311" id="Seg_10477" s="T310">adj-n:(poss)-n:case</ta>
            <ta e="T312" id="Seg_10478" s="T311">que-pro:(poss)-pro:case</ta>
            <ta e="T313" id="Seg_10479" s="T312">ptcl</ta>
            <ta e="T314" id="Seg_10480" s="T313">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T315" id="Seg_10481" s="T314">v-v:cvb</ta>
            <ta e="T316" id="Seg_10482" s="T315">v-v:cvb</ta>
            <ta e="T317" id="Seg_10483" s="T316">v-v:tense-v:pred.pn</ta>
            <ta e="T318" id="Seg_10484" s="T317">que-pro:case</ta>
            <ta e="T319" id="Seg_10485" s="T318">n-n:(poss)-n:case</ta>
            <ta e="T320" id="Seg_10486" s="T319">n-n:case</ta>
            <ta e="T321" id="Seg_10487" s="T320">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T322" id="Seg_10488" s="T321">que-pro:case</ta>
            <ta e="T323" id="Seg_10489" s="T322">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T324" id="Seg_10490" s="T323">n-n:case</ta>
            <ta e="T325" id="Seg_10491" s="T324">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T326" id="Seg_10492" s="T325">n-n:(poss)-n:case</ta>
            <ta e="T327" id="Seg_10493" s="T326">adj-n:(num)-n:case</ta>
            <ta e="T328" id="Seg_10494" s="T327">dempro</ta>
            <ta e="T329" id="Seg_10495" s="T328">n-n:(num)-n:case</ta>
            <ta e="T330" id="Seg_10496" s="T329">v-v:tense-v:pred.pn</ta>
            <ta e="T331" id="Seg_10497" s="T330">propr-n:case</ta>
            <ta e="T332" id="Seg_10498" s="T331">n-n:case</ta>
            <ta e="T333" id="Seg_10499" s="T332">n-n:(poss)-n:case</ta>
            <ta e="T334" id="Seg_10500" s="T333">propr-n:case</ta>
            <ta e="T335" id="Seg_10501" s="T334">propr-n:case</ta>
            <ta e="T336" id="Seg_10502" s="T335">ptcl</ta>
            <ta e="T337" id="Seg_10503" s="T336">v-v:ptcp-v:(case)</ta>
            <ta e="T338" id="Seg_10504" s="T337">v-v:mood-v:pred.pn</ta>
            <ta e="T339" id="Seg_10505" s="T338">pers-pro:case</ta>
            <ta e="T340" id="Seg_10506" s="T339">n-n:(poss)-n:case</ta>
            <ta e="T341" id="Seg_10507" s="T340">ptcl</ta>
            <ta e="T342" id="Seg_10508" s="T341">ptcl</ta>
            <ta e="T343" id="Seg_10509" s="T342">quant</ta>
            <ta e="T344" id="Seg_10510" s="T343">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T345" id="Seg_10511" s="T344">propr-n:case</ta>
            <ta e="T346" id="Seg_10512" s="T345">n-n:case</ta>
            <ta e="T347" id="Seg_10513" s="T346">ptcl</ta>
            <ta e="T348" id="Seg_10514" s="T347">propr-n:case</ta>
            <ta e="T349" id="Seg_10515" s="T348">v-v:tense-v:poss.pn</ta>
            <ta e="T350" id="Seg_10516" s="T349">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T351" id="Seg_10517" s="T350">n-n:case</ta>
            <ta e="T352" id="Seg_10518" s="T351">n-n:case</ta>
            <ta e="T353" id="Seg_10519" s="T352">v-v:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T354" id="Seg_10520" s="T353">n-n&gt;adj-n:case</ta>
            <ta e="T355" id="Seg_10521" s="T354">v-v:mood-v:temp.pn</ta>
            <ta e="T356" id="Seg_10522" s="T355">n-n:case</ta>
            <ta e="T357" id="Seg_10523" s="T356">n-n:case</ta>
            <ta e="T358" id="Seg_10524" s="T357">n-n:poss-n:case</ta>
            <ta e="T359" id="Seg_10525" s="T358">v-v:tense-v:poss.pn</ta>
            <ta e="T360" id="Seg_10526" s="T359">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T361" id="Seg_10527" s="T360">n-n:case</ta>
            <ta e="T362" id="Seg_10528" s="T361">post</ta>
            <ta e="T363" id="Seg_10529" s="T362">v-v&gt;v-v:cvb</ta>
            <ta e="T364" id="Seg_10530" s="T363">v-v:tense-v:poss.pn</ta>
            <ta e="T365" id="Seg_10531" s="T364">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T366" id="Seg_10532" s="T365">n-n:case</ta>
            <ta e="T367" id="Seg_10533" s="T366">v-v:ptcp</ta>
            <ta e="T368" id="Seg_10534" s="T367">n-n:case</ta>
            <ta e="T369" id="Seg_10535" s="T368">adj</ta>
            <ta e="T370" id="Seg_10536" s="T369">n-n:poss-n:case</ta>
            <ta e="T371" id="Seg_10537" s="T370">v-v&gt;v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T372" id="Seg_10538" s="T371">v-v:ptcp</ta>
            <ta e="T373" id="Seg_10539" s="T372">n-n:(num)-n:case</ta>
            <ta e="T376" id="Seg_10540" s="T375">v-v:mood.pn</ta>
            <ta e="T377" id="Seg_10541" s="T376">adj-adj&gt;adv</ta>
            <ta e="T378" id="Seg_10542" s="T377">v-v&gt;v-v:(ins)-v:mood.pn</ta>
            <ta e="T379" id="Seg_10543" s="T378">n-n:case</ta>
            <ta e="T380" id="Seg_10544" s="T379">v-v&gt;v-v:ptcp-v:(case)</ta>
            <ta e="T381" id="Seg_10545" s="T380">post</ta>
            <ta e="T382" id="Seg_10546" s="T381">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T383" id="Seg_10547" s="T382">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T384" id="Seg_10548" s="T383">v-v:tense-v:poss.pn</ta>
            <ta e="T385" id="Seg_10549" s="T384">propr-n:case</ta>
            <ta e="T386" id="Seg_10550" s="T385">n-n:case</ta>
            <ta e="T387" id="Seg_10551" s="T386">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T388" id="Seg_10552" s="T387">v-v:cvb</ta>
            <ta e="T389" id="Seg_10553" s="T388">v-v:tense-v:pred.pn</ta>
            <ta e="T390" id="Seg_10554" s="T389">n-n:(poss)-n:case</ta>
            <ta e="T391" id="Seg_10555" s="T390">n-n:case</ta>
            <ta e="T392" id="Seg_10556" s="T391">v-v:cvb-v:pred.pn</ta>
            <ta e="T393" id="Seg_10557" s="T392">v-v&gt;v-v&gt;n-n:case</ta>
            <ta e="T394" id="Seg_10558" s="T393">n-n:(poss)-n:case</ta>
            <ta e="T395" id="Seg_10559" s="T394">v-v:tense-v:pred.pn</ta>
            <ta e="T396" id="Seg_10560" s="T395">n-n:(num)-n:case</ta>
            <ta e="T397" id="Seg_10561" s="T396">n-n:poss-n:case</ta>
            <ta e="T398" id="Seg_10562" s="T397">n-n:poss-n:case</ta>
            <ta e="T399" id="Seg_10563" s="T398">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T400" id="Seg_10564" s="T399">n-n:poss-n:case</ta>
            <ta e="T401" id="Seg_10565" s="T400">adj</ta>
            <ta e="T402" id="Seg_10566" s="T401">adj</ta>
            <ta e="T403" id="Seg_10567" s="T402">n-n:poss-n:case</ta>
            <ta e="T404" id="Seg_10568" s="T403">n-n:case</ta>
            <ta e="T405" id="Seg_10569" s="T404">v-v:ptcp</ta>
            <ta e="T406" id="Seg_10570" s="T405">n-n:poss-n:case</ta>
            <ta e="T407" id="Seg_10571" s="T406">indfpro-pro:(num)-pro:case</ta>
            <ta e="T408" id="Seg_10572" s="T407">v-v:(ins)-v:ptcp</ta>
            <ta e="T409" id="Seg_10573" s="T408">n-n:case</ta>
            <ta e="T410" id="Seg_10574" s="T409">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T411" id="Seg_10575" s="T410">v-v:tense-v:pred.pn</ta>
            <ta e="T412" id="Seg_10576" s="T411">adj</ta>
            <ta e="T413" id="Seg_10577" s="T412">n-n:poss-n:case</ta>
            <ta e="T414" id="Seg_10578" s="T413">n-n:(num)-n:case</ta>
            <ta e="T415" id="Seg_10579" s="T414">n-n:(num)-n:poss-n:case</ta>
            <ta e="T416" id="Seg_10580" s="T415">v-v:pred.pn</ta>
            <ta e="T417" id="Seg_10581" s="T416">adj</ta>
            <ta e="T418" id="Seg_10582" s="T417">adj</ta>
            <ta e="T419" id="Seg_10583" s="T418">adj</ta>
            <ta e="T420" id="Seg_10584" s="T419">n-n:(num)-n:(ins)-n:case</ta>
            <ta e="T421" id="Seg_10585" s="T420">n-n:case</ta>
            <ta e="T422" id="Seg_10586" s="T421">n-n:(ins)-n:case</ta>
            <ta e="T423" id="Seg_10587" s="T422">n-n:case</ta>
            <ta e="T424" id="Seg_10588" s="T423">n-n:(ins)-n:case</ta>
            <ta e="T425" id="Seg_10589" s="T424">n-n:case</ta>
            <ta e="T426" id="Seg_10590" s="T425">post</ta>
            <ta e="T429" id="Seg_10591" s="T428">adj-n:poss-n:case</ta>
            <ta e="T430" id="Seg_10592" s="T429">v-v:cvb</ta>
            <ta e="T431" id="Seg_10593" s="T430">v-v:tense-v:pred.pn</ta>
            <ta e="T432" id="Seg_10594" s="T431">propr</ta>
            <ta e="T433" id="Seg_10595" s="T432">n-n:case</ta>
            <ta e="T434" id="Seg_10596" s="T433">que</ta>
            <ta e="T435" id="Seg_10597" s="T434">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T436" id="Seg_10598" s="T435">adv</ta>
            <ta e="T437" id="Seg_10599" s="T436">v-v:(neg)-v:pred.pn</ta>
            <ta e="T438" id="Seg_10600" s="T437">n-n:case</ta>
            <ta e="T439" id="Seg_10601" s="T438">v-v:cvb</ta>
            <ta e="T440" id="Seg_10602" s="T439">ptcl</ta>
            <ta e="T441" id="Seg_10603" s="T440">v-v:cvb</ta>
            <ta e="T442" id="Seg_10604" s="T441">v-v:tense-v:pred.pn</ta>
            <ta e="T443" id="Seg_10605" s="T442">adv</ta>
            <ta e="T444" id="Seg_10606" s="T443">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T445" id="Seg_10607" s="T444">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T446" id="Seg_10608" s="T445">n-n&gt;n-n:case</ta>
            <ta e="T447" id="Seg_10609" s="T446">v-v:tense-v:poss.pn</ta>
            <ta e="T448" id="Seg_10610" s="T447">adj</ta>
            <ta e="T449" id="Seg_10611" s="T448">n-n:case</ta>
            <ta e="T450" id="Seg_10612" s="T449">adv</ta>
            <ta e="T451" id="Seg_10613" s="T450">n-n&gt;v-v:ptcp</ta>
            <ta e="T452" id="Seg_10614" s="T451">n-n&gt;adj-n:case</ta>
            <ta e="T453" id="Seg_10615" s="T452">v-v:tense-v:poss.pn</ta>
            <ta e="T454" id="Seg_10616" s="T453">adj</ta>
            <ta e="T455" id="Seg_10617" s="T454">n-n:case</ta>
            <ta e="T456" id="Seg_10618" s="T455">adj-adj&gt;adv</ta>
            <ta e="T457" id="Seg_10619" s="T456">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T458" id="Seg_10620" s="T457">v-v:tense-v:pred.pn</ta>
            <ta e="T459" id="Seg_10621" s="T458">que-pro:(ins)-pro:case</ta>
            <ta e="T460" id="Seg_10622" s="T459">ptcl</ta>
            <ta e="T461" id="Seg_10623" s="T460">v-v:cvb</ta>
            <ta e="T462" id="Seg_10624" s="T461">v-v:(ins)-v:cvb</ta>
            <ta e="T463" id="Seg_10625" s="T462">adj</ta>
            <ta e="T464" id="Seg_10626" s="T463">n-n:(num)-n:case</ta>
            <ta e="T465" id="Seg_10627" s="T464">n-n:case</ta>
            <ta e="T466" id="Seg_10628" s="T465">v-v:ptcp</ta>
            <ta e="T467" id="Seg_10629" s="T466">n-n:poss-n:case</ta>
            <ta e="T468" id="Seg_10630" s="T467">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T469" id="Seg_10631" s="T468">adv</ta>
            <ta e="T470" id="Seg_10632" s="T469">v-v:tense-v:poss.pn</ta>
            <ta e="T471" id="Seg_10633" s="T470">dempro</ta>
            <ta e="T472" id="Seg_10634" s="T471">n-n:poss-n:case</ta>
            <ta e="T473" id="Seg_10635" s="T472">adj-n:(poss)</ta>
            <ta e="T474" id="Seg_10636" s="T473">ptcl</ta>
            <ta e="T475" id="Seg_10637" s="T474">ptcl</ta>
            <ta e="T476" id="Seg_10638" s="T475">adj</ta>
            <ta e="T477" id="Seg_10639" s="T476">n-n-n:case</ta>
            <ta e="T478" id="Seg_10640" s="T477">adj</ta>
            <ta e="T479" id="Seg_10641" s="T478">n-n:case</ta>
            <ta e="T480" id="Seg_10642" s="T479">adv</ta>
            <ta e="T481" id="Seg_10643" s="T480">n-n:case</ta>
            <ta e="T482" id="Seg_10644" s="T481">v-v:mood-v:pred.pn</ta>
            <ta e="T483" id="Seg_10645" s="T482">adv</ta>
            <ta e="T484" id="Seg_10646" s="T483">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T485" id="Seg_10647" s="T484">adj</ta>
            <ta e="T486" id="Seg_10648" s="T485">n-n:case</ta>
            <ta e="T487" id="Seg_10649" s="T486">v-v:tense-v:pred.pn</ta>
            <ta e="T488" id="Seg_10650" s="T487">adj</ta>
            <ta e="T489" id="Seg_10651" s="T488">n-n:(num)-n:case</ta>
            <ta e="T490" id="Seg_10652" s="T489">adj-n:(poss)-n:case</ta>
            <ta e="T491" id="Seg_10653" s="T490">n-n:poss-n:case</ta>
            <ta e="T492" id="Seg_10654" s="T491">n-n:poss-n:case</ta>
            <ta e="T493" id="Seg_10655" s="T492">v-v:cvb-v-v:cvb</ta>
            <ta e="T494" id="Seg_10656" s="T493">n-n:poss-n:case</ta>
            <ta e="T495" id="Seg_10657" s="T494">v-v:cvb</ta>
            <ta e="T496" id="Seg_10658" s="T495">v-v:tense-v:pred.pn</ta>
            <ta e="T497" id="Seg_10659" s="T496">que</ta>
            <ta e="T498" id="Seg_10660" s="T497">n-n:poss-n:case</ta>
            <ta e="T499" id="Seg_10661" s="T498">dempro</ta>
            <ta e="T500" id="Seg_10662" s="T499">n-n:case</ta>
            <ta e="T501" id="Seg_10663" s="T500">v-v:tense-v:pred.pn</ta>
            <ta e="T502" id="Seg_10664" s="T501">n-n:(poss)-n:case</ta>
            <ta e="T503" id="Seg_10665" s="T502">v-v:cvb</ta>
            <ta e="T504" id="Seg_10666" s="T503">v-v&gt;v-v:cvb</ta>
            <ta e="T505" id="Seg_10667" s="T504">v-v:tense-v:pred.pn</ta>
            <ta e="T506" id="Seg_10668" s="T505">n-n:(num)-n:case</ta>
            <ta e="T507" id="Seg_10669" s="T506">n-n&gt;adj-n:(num)</ta>
            <ta e="T508" id="Seg_10670" s="T507">adv</ta>
            <ta e="T509" id="Seg_10671" s="T508">v-v:tense-v:pred.pn</ta>
            <ta e="T510" id="Seg_10672" s="T509">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T511" id="Seg_10673" s="T510">adj</ta>
            <ta e="T512" id="Seg_10674" s="T511">n-n:case</ta>
            <ta e="T513" id="Seg_10675" s="T512">n-n:case</ta>
            <ta e="T514" id="Seg_10676" s="T513">v-v:ptcp</ta>
            <ta e="T515" id="Seg_10677" s="T514">n-n:case</ta>
            <ta e="T516" id="Seg_10678" s="T515">adv</ta>
            <ta e="T517" id="Seg_10679" s="T516">n-n&gt;adj-n:case</ta>
            <ta e="T518" id="Seg_10680" s="T517">n-n:case</ta>
            <ta e="T519" id="Seg_10681" s="T518">adv</ta>
            <ta e="T520" id="Seg_10682" s="T519">v-v:tense-v:pred.pn</ta>
            <ta e="T521" id="Seg_10683" s="T520">ptcl</ta>
            <ta e="T522" id="Seg_10684" s="T521">n-n:case</ta>
            <ta e="T523" id="Seg_10685" s="T522">n-n:case</ta>
            <ta e="T524" id="Seg_10686" s="T523">v-v:ptcp-v:(case)</ta>
            <ta e="T525" id="Seg_10687" s="T524">ptcl</ta>
            <ta e="T526" id="Seg_10688" s="T525">n-n:case</ta>
            <ta e="T527" id="Seg_10689" s="T526">pers-pro:case</ta>
            <ta e="T528" id="Seg_10690" s="T527">v-v:(neg)-v:mood.pn</ta>
            <ta e="T529" id="Seg_10691" s="T528">dempro-pro:case</ta>
            <ta e="T530" id="Seg_10692" s="T529">v-v:tense-v:poss.pn</ta>
            <ta e="T531" id="Seg_10693" s="T530">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T532" id="Seg_10694" s="T531">n-n:case</ta>
            <ta e="T533" id="Seg_10695" s="T532">n-n:case</ta>
            <ta e="T534" id="Seg_10696" s="T533">v-v:(neg)-v:pred.pn</ta>
            <ta e="T535" id="Seg_10697" s="T534">adv</ta>
            <ta e="T536" id="Seg_10698" s="T535">adj</ta>
            <ta e="T537" id="Seg_10699" s="T536">n-n:case</ta>
            <ta e="T538" id="Seg_10700" s="T537">v-v:mood.pn</ta>
            <ta e="T539" id="Seg_10701" s="T538">v-v:(neg)-v:mood.pn</ta>
            <ta e="T540" id="Seg_10702" s="T539">v-v:tense-v:pred.pn</ta>
            <ta e="T541" id="Seg_10703" s="T540">adj-n:(poss)-n:case</ta>
            <ta e="T542" id="Seg_10704" s="T541">n-n:case</ta>
            <ta e="T543" id="Seg_10705" s="T542">que-pro:case</ta>
            <ta e="T544" id="Seg_10706" s="T543">ptcl</ta>
            <ta e="T545" id="Seg_10707" s="T544">v-v:cvb</ta>
            <ta e="T546" id="Seg_10708" s="T545">n-n:poss-n:case</ta>
            <ta e="T547" id="Seg_10709" s="T546">n-n:case</ta>
            <ta e="T548" id="Seg_10710" s="T547">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T549" id="Seg_10711" s="T548">pers-pro:case</ta>
            <ta e="T550" id="Seg_10712" s="T549">n-n:(num)-n:case</ta>
            <ta e="T551" id="Seg_10713" s="T550">que</ta>
            <ta e="T552" id="Seg_10714" s="T551">adj-n:case</ta>
            <ta e="T553" id="Seg_10715" s="T552">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T554" id="Seg_10716" s="T553">v-v:cvb</ta>
            <ta e="T555" id="Seg_10717" s="T554">n-n:case</ta>
            <ta e="T556" id="Seg_10718" s="T555">v-v:tense-v:pred.pn</ta>
            <ta e="T557" id="Seg_10719" s="T556">n-n:case</ta>
            <ta e="T558" id="Seg_10720" s="T557">indfpro</ta>
            <ta e="T559" id="Seg_10721" s="T558">post</ta>
            <ta e="T560" id="Seg_10722" s="T559">n-n:case</ta>
            <ta e="T561" id="Seg_10723" s="T560">v-v:tense-v:pred.pn</ta>
            <ta e="T562" id="Seg_10724" s="T561">v-v:tense-v:poss.pn</ta>
            <ta e="T563" id="Seg_10725" s="T562">n-n:(num)-n:case</ta>
            <ta e="T564" id="Seg_10726" s="T563">v-v:(ins)-v&gt;v-v:ptcp-v:(poss)-v:pred.pn</ta>
            <ta e="T565" id="Seg_10727" s="T564">adj-n:case</ta>
            <ta e="T566" id="Seg_10728" s="T565">n-n:case</ta>
            <ta e="T567" id="Seg_10729" s="T566">n-n:poss-n:case</ta>
            <ta e="T569" id="Seg_10730" s="T568">n-n:case</ta>
            <ta e="T570" id="Seg_10731" s="T569">n-n:poss-n:case</ta>
            <ta e="T571" id="Seg_10732" s="T570">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T572" id="Seg_10733" s="T571">adv</ta>
            <ta e="T573" id="Seg_10734" s="T572">adv</ta>
            <ta e="T574" id="Seg_10735" s="T573">adv</ta>
            <ta e="T575" id="Seg_10736" s="T574">adj</ta>
            <ta e="T576" id="Seg_10737" s="T575">n-n:(num)-n:case</ta>
            <ta e="T577" id="Seg_10738" s="T576">adj</ta>
            <ta e="T578" id="Seg_10739" s="T577">n-n:case</ta>
            <ta e="T579" id="Seg_10740" s="T578">v-v:ptcp-v:(case)</ta>
            <ta e="T580" id="Seg_10741" s="T579">adv</ta>
            <ta e="T581" id="Seg_10742" s="T580">v-v:ptcp</ta>
            <ta e="T582" id="Seg_10743" s="T581">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T583" id="Seg_10744" s="T582">adv</ta>
            <ta e="T584" id="Seg_10745" s="T583">adj</ta>
            <ta e="T585" id="Seg_10746" s="T584">n-n:case</ta>
            <ta e="T586" id="Seg_10747" s="T585">n-n:case</ta>
            <ta e="T587" id="Seg_10748" s="T586">post</ta>
            <ta e="T588" id="Seg_10749" s="T587">que</ta>
            <ta e="T589" id="Seg_10750" s="T588">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T590" id="Seg_10751" s="T589">v-v:(neg)-v:pred.pn</ta>
            <ta e="T591" id="Seg_10752" s="T590">adj-adj&gt;adv</ta>
            <ta e="T592" id="Seg_10753" s="T591">v-v:cvb</ta>
            <ta e="T593" id="Seg_10754" s="T592">v-v:(neg)-v:pred.pn</ta>
            <ta e="T594" id="Seg_10755" s="T593">dempro-pro:case</ta>
            <ta e="T595" id="Seg_10756" s="T594">n-n:case</ta>
            <ta e="T596" id="Seg_10757" s="T595">v-v:tense-v:pred.pn</ta>
            <ta e="T597" id="Seg_10758" s="T596">adj-n:(poss)-n:case</ta>
            <ta e="T598" id="Seg_10759" s="T597">n-n:case</ta>
            <ta e="T599" id="Seg_10760" s="T598">dempro</ta>
            <ta e="T600" id="Seg_10761" s="T599">n-n:case</ta>
            <ta e="T601" id="Seg_10762" s="T600">n-n:poss-n:case</ta>
            <ta e="T602" id="Seg_10763" s="T601">v-v:cvb</ta>
            <ta e="T603" id="Seg_10764" s="T602">v-v:tense-v:pred.pn</ta>
            <ta e="T604" id="Seg_10765" s="T603">adv</ta>
            <ta e="T605" id="Seg_10766" s="T604">adj</ta>
            <ta e="T606" id="Seg_10767" s="T605">n-n:case</ta>
            <ta e="T607" id="Seg_10768" s="T606">v-v:ptcp</ta>
            <ta e="T608" id="Seg_10769" s="T607">n-n:poss-n:case</ta>
            <ta e="T609" id="Seg_10770" s="T608">n-n:(num)-n:case</ta>
            <ta e="T610" id="Seg_10771" s="T609">v-v:cvb</ta>
            <ta e="T611" id="Seg_10772" s="T610">v-v:tense-v:poss.pn</ta>
            <ta e="T612" id="Seg_10773" s="T611">n-n:poss-n:case</ta>
            <ta e="T613" id="Seg_10774" s="T612">n-n:case</ta>
            <ta e="T614" id="Seg_10775" s="T613">n</ta>
            <ta e="T615" id="Seg_10776" s="T614">n-n:poss-n:case</ta>
            <ta e="T616" id="Seg_10777" s="T615">cardnum</ta>
            <ta e="T617" id="Seg_10778" s="T616">n-n&gt;adj</ta>
            <ta e="T618" id="Seg_10779" s="T617">n-n:poss-n:case</ta>
            <ta e="T619" id="Seg_10780" s="T618">v-v:mood-v:temp.pn</ta>
            <ta e="T620" id="Seg_10781" s="T619">n-n:case</ta>
            <ta e="T621" id="Seg_10782" s="T620">post</ta>
            <ta e="T622" id="Seg_10783" s="T621">n-n:poss-n:case</ta>
            <ta e="T623" id="Seg_10784" s="T622">v-v:tense-v:poss.pn</ta>
            <ta e="T624" id="Seg_10785" s="T623">dempro</ta>
            <ta e="T625" id="Seg_10786" s="T624">n-n:case</ta>
            <ta e="T626" id="Seg_10787" s="T625">pers-pro:case</ta>
            <ta e="T627" id="Seg_10788" s="T626">adj</ta>
            <ta e="T628" id="Seg_10789" s="T627">n-n:case</ta>
            <ta e="T629" id="Seg_10790" s="T628">v-v:tense-v:poss.pn</ta>
            <ta e="T630" id="Seg_10791" s="T629">n-n:(num)-n:case</ta>
            <ta e="T631" id="Seg_10792" s="T630">v-v:cvb</ta>
            <ta e="T632" id="Seg_10793" s="T631">v-v:tense-v:pred.pn</ta>
            <ta e="T633" id="Seg_10794" s="T632">que</ta>
            <ta e="T634" id="Seg_10795" s="T633">ptcl</ta>
            <ta e="T635" id="Seg_10796" s="T634">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T636" id="Seg_10797" s="T635">v-v:tense-v:pred.pn</ta>
            <ta e="T637" id="Seg_10798" s="T636">n-n:(num)-n:case</ta>
            <ta e="T638" id="Seg_10799" s="T637">v-v:cvb</ta>
            <ta e="T639" id="Seg_10800" s="T638">n-n:case</ta>
            <ta e="T640" id="Seg_10801" s="T639">v-v:tense-v:pred.pn</ta>
            <ta e="T641" id="Seg_10802" s="T640">n-n:case</ta>
            <ta e="T642" id="Seg_10803" s="T641">n-n:poss-n:case</ta>
            <ta e="T643" id="Seg_10804" s="T642">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T644" id="Seg_10805" s="T643">post</ta>
            <ta e="T645" id="Seg_10806" s="T644">propr-n:case</ta>
            <ta e="T646" id="Seg_10807" s="T645">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T647" id="Seg_10808" s="T646">post</ta>
            <ta e="T648" id="Seg_10809" s="T647">v-v:tense-v:pred.pn</ta>
            <ta e="T649" id="Seg_10810" s="T648">adj</ta>
            <ta e="T650" id="Seg_10811" s="T649">n-n:case</ta>
            <ta e="T651" id="Seg_10812" s="T650">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T652" id="Seg_10813" s="T651">v-v:ptcp-v:(case)</ta>
            <ta e="T653" id="Seg_10814" s="T652">ptcl</ta>
            <ta e="T654" id="Seg_10815" s="T653">que</ta>
            <ta e="T655" id="Seg_10816" s="T654">adj</ta>
            <ta e="T656" id="Seg_10817" s="T655">n-n:case</ta>
            <ta e="T657" id="Seg_10818" s="T656">adj-n:case</ta>
            <ta e="T658" id="Seg_10819" s="T657">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T659" id="Seg_10820" s="T658">n-n:case</ta>
            <ta e="T660" id="Seg_10821" s="T659">n-n:poss-n:case</ta>
            <ta e="T661" id="Seg_10822" s="T660">n-n:poss-n:case</ta>
            <ta e="T662" id="Seg_10823" s="T661">dempro</ta>
            <ta e="T663" id="Seg_10824" s="T662">n-n:(num)-n:case</ta>
            <ta e="T664" id="Seg_10825" s="T663">n-n:case</ta>
            <ta e="T665" id="Seg_10826" s="T664">v-v:ptcp-v:(poss)</ta>
            <ta e="T666" id="Seg_10827" s="T665">n-n&gt;adj-n:case</ta>
            <ta e="T667" id="Seg_10828" s="T666">adj-adj&gt;adj-n:case</ta>
            <ta e="T668" id="Seg_10829" s="T667">v-v:tense-v:poss.pn</ta>
            <ta e="T669" id="Seg_10830" s="T668">adj</ta>
            <ta e="T670" id="Seg_10831" s="T669">n-n:case</ta>
            <ta e="T671" id="Seg_10832" s="T670">ptcl</ta>
            <ta e="T672" id="Seg_10833" s="T671">n-n:case</ta>
            <ta e="T673" id="Seg_10834" s="T672">n-n:(num)-n:case</ta>
            <ta e="T674" id="Seg_10835" s="T673">v-v&gt;v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T676" id="Seg_10836" s="T675">propr</ta>
            <ta e="T677" id="Seg_10837" s="T676">n-n:case</ta>
            <ta e="T678" id="Seg_10838" s="T677">n-n:poss-n:case</ta>
            <ta e="T679" id="Seg_10839" s="T678">n-n:case</ta>
            <ta e="T680" id="Seg_10840" s="T679">adv-adv&gt;adj-n:case</ta>
            <ta e="T681" id="Seg_10841" s="T680">que</ta>
            <ta e="T682" id="Seg_10842" s="T681">ptcl</ta>
            <ta e="T683" id="Seg_10843" s="T682">n-n:(poss)-n:case</ta>
            <ta e="T684" id="Seg_10844" s="T683">v-v:(ins)-v&gt;v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T685" id="Seg_10845" s="T684">n-n:case</ta>
            <ta e="T686" id="Seg_10846" s="T685">v-v&gt;v-v&gt;n-n:case-v-v&gt;v-v&gt;n-n:case</ta>
            <ta e="T687" id="Seg_10847" s="T686">v-v:cvb-v:pred.pn</ta>
            <ta e="T688" id="Seg_10848" s="T687">adj-n:(poss)-n:case</ta>
            <ta e="T689" id="Seg_10849" s="T688">v-v:tense-v:pred.pn</ta>
            <ta e="T690" id="Seg_10850" s="T689">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T691" id="Seg_10851" s="T690">n-n:poss-n:case</ta>
            <ta e="T692" id="Seg_10852" s="T691">adj</ta>
            <ta e="T693" id="Seg_10853" s="T692">n-n:(num)-n:case</ta>
            <ta e="T694" id="Seg_10854" s="T693">v-v&gt;v-v:(ins)-v:mood.pn</ta>
            <ta e="T695" id="Seg_10855" s="T694">adj</ta>
            <ta e="T696" id="Seg_10856" s="T695">n-n:(num)-n:case</ta>
            <ta e="T697" id="Seg_10857" s="T696">v-v:ptcp</ta>
            <ta e="T698" id="Seg_10858" s="T697">n-n:case</ta>
            <ta e="T699" id="Seg_10859" s="T698">v-v:ptcp</ta>
            <ta e="T700" id="Seg_10860" s="T699">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T701" id="Seg_10861" s="T700">pers-pro:case</ta>
            <ta e="T702" id="Seg_10862" s="T701">pers-pro:case</ta>
            <ta e="T703" id="Seg_10863" s="T702">v-v:tense-v:poss.pn</ta>
            <ta e="T704" id="Seg_10864" s="T703">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T705" id="Seg_10865" s="T704">adv</ta>
            <ta e="T706" id="Seg_10866" s="T705">v-v:cvb</ta>
            <ta e="T707" id="Seg_10867" s="T706">v-v:tense-v:poss.pn</ta>
            <ta e="T708" id="Seg_10868" s="T707">dempro-pro:case</ta>
            <ta e="T709" id="Seg_10869" s="T708">n-n:case</ta>
            <ta e="T710" id="Seg_10870" s="T709">n-n:case</ta>
            <ta e="T711" id="Seg_10871" s="T710">ptcl</ta>
            <ta e="T712" id="Seg_10872" s="T711">v-v:tense-v:pred.pn</ta>
            <ta e="T713" id="Seg_10873" s="T712">cardnum</ta>
            <ta e="T714" id="Seg_10874" s="T713">n-n:case</ta>
            <ta e="T715" id="Seg_10875" s="T714">propr</ta>
            <ta e="T716" id="Seg_10876" s="T715">n-n:case</ta>
            <ta e="T717" id="Seg_10877" s="T716">v-v:ptcp</ta>
            <ta e="T718" id="Seg_10878" s="T717">n-n:poss-n:case</ta>
            <ta e="T719" id="Seg_10879" s="T718">v-v:(ins)</ta>
            <ta e="T720" id="Seg_10880" s="T719">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T721" id="Seg_10881" s="T720">dempro-pro:case</ta>
            <ta e="T722" id="Seg_10882" s="T721">n-n:case</ta>
            <ta e="T723" id="Seg_10883" s="T722">n-n:case</ta>
            <ta e="T724" id="Seg_10884" s="T723">adv</ta>
            <ta e="T725" id="Seg_10885" s="T724">v-v:cvb</ta>
            <ta e="T726" id="Seg_10886" s="T725">v-v:tense-v:poss.pn</ta>
            <ta e="T727" id="Seg_10887" s="T726">adv</ta>
            <ta e="T728" id="Seg_10888" s="T727">adv</ta>
            <ta e="T729" id="Seg_10889" s="T728">adv</ta>
            <ta e="T730" id="Seg_10890" s="T729">n-n:poss-n:case</ta>
            <ta e="T731" id="Seg_10891" s="T730">v-v:cvb</ta>
            <ta e="T732" id="Seg_10892" s="T731">v-v:tense-v:poss.pn</ta>
            <ta e="T733" id="Seg_10893" s="T732">v-v:tense-v:pred.pn</ta>
            <ta e="T734" id="Seg_10894" s="T733">propr-n:case</ta>
            <ta e="T735" id="Seg_10895" s="T734">propr</ta>
            <ta e="T736" id="Seg_10896" s="T735">n-n:case</ta>
            <ta e="T737" id="Seg_10897" s="T736">v-v:cvb</ta>
            <ta e="T738" id="Seg_10898" s="T737">v-v:cvb</ta>
            <ta e="T739" id="Seg_10899" s="T738">n-n:case</ta>
            <ta e="T742" id="Seg_10900" s="T741">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T743" id="Seg_10901" s="T742">pers-pro:case</ta>
            <ta e="T744" id="Seg_10902" s="T743">que-pro:case</ta>
            <ta e="T745" id="Seg_10903" s="T744">v-v:tense-v:(ins)-v:poss.pn-ptcl</ta>
            <ta e="T746" id="Seg_10904" s="T745">adj-n:(poss)-n:case</ta>
            <ta e="T747" id="Seg_10905" s="T746">adj</ta>
            <ta e="T748" id="Seg_10906" s="T747">n-n:(ins)-n:case</ta>
            <ta e="T749" id="Seg_10907" s="T748">v-v:cvb</ta>
            <ta e="T750" id="Seg_10908" s="T749">propr</ta>
            <ta e="T751" id="Seg_10909" s="T750">n-n:case</ta>
            <ta e="T752" id="Seg_10910" s="T751">post</ta>
            <ta e="T753" id="Seg_10911" s="T752">v-v&gt;v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T754" id="Seg_10912" s="T753">n-n:(poss)-n:case</ta>
            <ta e="T755" id="Seg_10913" s="T754">v-v:tense-v:pred.pn</ta>
            <ta e="T756" id="Seg_10914" s="T755">adj-adj&gt;adv</ta>
            <ta e="T757" id="Seg_10915" s="T756">n-n:(num)-n:case</ta>
            <ta e="T758" id="Seg_10916" s="T757">n-n:case</ta>
            <ta e="T759" id="Seg_10917" s="T758">n-n:poss-n:case</ta>
            <ta e="T760" id="Seg_10918" s="T759">adj</ta>
            <ta e="T761" id="Seg_10919" s="T760">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T762" id="Seg_10920" s="T761">adj</ta>
            <ta e="T763" id="Seg_10921" s="T762">n-n:case</ta>
            <ta e="T764" id="Seg_10922" s="T763">n-n:poss-n:case</ta>
            <ta e="T765" id="Seg_10923" s="T764">v-v:cvb</ta>
            <ta e="T766" id="Seg_10924" s="T765">post</ta>
            <ta e="T767" id="Seg_10925" s="T766">n-n:case</ta>
            <ta e="T768" id="Seg_10926" s="T767">v-v&gt;n-n:(poss)-n:case</ta>
            <ta e="T769" id="Seg_10927" s="T768">v-v:tense-v:pred.pn</ta>
            <ta e="T770" id="Seg_10928" s="T769">v-v:ptcp-v:(case)</ta>
            <ta e="T771" id="Seg_10929" s="T770">n-n:(num)-n:case</ta>
            <ta e="T772" id="Seg_10930" s="T771">n-n:poss-n:case</ta>
            <ta e="T773" id="Seg_10931" s="T772">n-n:case</ta>
            <ta e="T774" id="Seg_10932" s="T773">n-n:(num)-n:poss-n:case</ta>
            <ta e="T775" id="Seg_10933" s="T774">post</ta>
            <ta e="T776" id="Seg_10934" s="T775">n-n:(poss)</ta>
            <ta e="T777" id="Seg_10935" s="T776">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T778" id="Seg_10936" s="T777">v-v&gt;v-v:cvb-v:pred.pn</ta>
            <ta e="T779" id="Seg_10937" s="T778">n-n:(num)-n:poss-n:case</ta>
            <ta e="T780" id="Seg_10938" s="T779">adv</ta>
            <ta e="T781" id="Seg_10939" s="T780">v-v:tense-v:pred.pn</ta>
            <ta e="T782" id="Seg_10940" s="T781">v-v:ptcp</ta>
            <ta e="T783" id="Seg_10941" s="T782">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T784" id="Seg_10942" s="T783">adj</ta>
            <ta e="T785" id="Seg_10943" s="T784">n-n:(num)-n:case</ta>
            <ta e="T786" id="Seg_10944" s="T785">v-v:ptcp</ta>
            <ta e="T787" id="Seg_10945" s="T786">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T788" id="Seg_10946" s="T787">n-n:case</ta>
            <ta e="T789" id="Seg_10947" s="T788">v-v:cvb</ta>
            <ta e="T790" id="Seg_10948" s="T789">v-v:cvb</ta>
            <ta e="T791" id="Seg_10949" s="T790">n-n:case</ta>
            <ta e="T792" id="Seg_10950" s="T791">n-n:case</ta>
            <ta e="T793" id="Seg_10951" s="T792">adj</ta>
            <ta e="T794" id="Seg_10952" s="T793">v-v:tense-v:pred.pn</ta>
            <ta e="T795" id="Seg_10953" s="T794">v-v:ptcp</ta>
            <ta e="T796" id="Seg_10954" s="T795">n-n:poss-n:case</ta>
            <ta e="T797" id="Seg_10955" s="T796">adv</ta>
            <ta e="T798" id="Seg_10956" s="T797">n-n:case</ta>
            <ta e="T799" id="Seg_10957" s="T798">n-n:case</ta>
            <ta e="T800" id="Seg_10958" s="T799">adj-n:case</ta>
            <ta e="T801" id="Seg_10959" s="T800">adj-n:(poss)-n:case</ta>
            <ta e="T802" id="Seg_10960" s="T801">v-v&gt;v-v:cvb</ta>
            <ta e="T803" id="Seg_10961" s="T802">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T804" id="Seg_10962" s="T803">adj-n:case</ta>
            <ta e="T805" id="Seg_10963" s="T804">v-v:ptcp</ta>
            <ta e="T806" id="Seg_10964" s="T805">n-n:(num)-n:case</ta>
            <ta e="T807" id="Seg_10965" s="T806">v-v:cvb</ta>
            <ta e="T808" id="Seg_10966" s="T807">que</ta>
            <ta e="T809" id="Seg_10967" s="T808">pers-pro:case</ta>
            <ta e="T810" id="Seg_10968" s="T809">pers-pro:case</ta>
            <ta e="T811" id="Seg_10969" s="T810">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T812" id="Seg_10970" s="T811">propr-n:case</ta>
            <ta e="T813" id="Seg_10971" s="T812">v-v:tense-v:pred.pn</ta>
            <ta e="T814" id="Seg_10972" s="T813">que</ta>
            <ta e="T815" id="Seg_10973" s="T814">pers-pro:case</ta>
            <ta e="T818" id="Seg_10974" s="T817">pers-pro:case-pro:(poss)</ta>
            <ta e="T819" id="Seg_10975" s="T818">ptcl</ta>
            <ta e="T820" id="Seg_10976" s="T819">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T821" id="Seg_10977" s="T820">que-pro:case</ta>
            <ta e="T822" id="Seg_10978" s="T821">pers-pro:case</ta>
            <ta e="T823" id="Seg_10979" s="T822">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T824" id="Seg_10980" s="T823">n-n:poss-n:case</ta>
            <ta e="T825" id="Seg_10981" s="T824">adv</ta>
            <ta e="T826" id="Seg_10982" s="T825">v-v:cvb</ta>
            <ta e="T827" id="Seg_10983" s="T826">post</ta>
            <ta e="T828" id="Seg_10984" s="T827">n-n:poss-n:case</ta>
            <ta e="T829" id="Seg_10985" s="T828">post</ta>
            <ta e="T830" id="Seg_10986" s="T829">v-v:tense-v:pred.pn</ta>
            <ta e="T831" id="Seg_10987" s="T830">pers-pro:case</ta>
            <ta e="T832" id="Seg_10988" s="T831">que-pro:case</ta>
            <ta e="T833" id="Seg_10989" s="T832">v-v:tense-v:pred.pn</ta>
            <ta e="T834" id="Seg_10990" s="T833">cardnum</ta>
            <ta e="T835" id="Seg_10991" s="T834">cardnum</ta>
            <ta e="T836" id="Seg_10992" s="T835">n-n:case</ta>
            <ta e="T837" id="Seg_10993" s="T836">v-v:tense-v:pred.pn</ta>
            <ta e="T838" id="Seg_10994" s="T837">n-n:poss-n:case</ta>
            <ta e="T839" id="Seg_10995" s="T838">n-n:case</ta>
            <ta e="T840" id="Seg_10996" s="T839">v-v:ptcp-v:(case)</ta>
            <ta e="T841" id="Seg_10997" s="T840">v-v:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T842" id="Seg_10998" s="T841">v-v:ptcp-v:(case)</ta>
            <ta e="T843" id="Seg_10999" s="T842">n-n:poss-n:case</ta>
            <ta e="T844" id="Seg_11000" s="T843">dempro</ta>
            <ta e="T845" id="Seg_11001" s="T844">v-v:ptcp-v:(case)</ta>
            <ta e="T846" id="Seg_11002" s="T845">adv</ta>
            <ta e="T847" id="Seg_11003" s="T846">adj-n:case</ta>
            <ta e="T848" id="Seg_11004" s="T847">n-n:case</ta>
            <ta e="T849" id="Seg_11005" s="T848">v-v:mood.pn</ta>
            <ta e="T850" id="Seg_11006" s="T849">n-n&gt;adv</ta>
            <ta e="T851" id="Seg_11007" s="T850">adj</ta>
            <ta e="T852" id="Seg_11008" s="T851">n-n&gt;adj</ta>
            <ta e="T853" id="Seg_11009" s="T852">n-n:(num)-n:case</ta>
            <ta e="T854" id="Seg_11010" s="T853">n-n:poss-n:case</ta>
            <ta e="T855" id="Seg_11011" s="T854">n-n:poss-n:case</ta>
            <ta e="T856" id="Seg_11012" s="T855">v-v:cvb</ta>
            <ta e="T857" id="Seg_11013" s="T856">adj</ta>
            <ta e="T858" id="Seg_11014" s="T857">n-n:case</ta>
            <ta e="T859" id="Seg_11015" s="T858">v-v:tense-v:pred.pn</ta>
            <ta e="T860" id="Seg_11016" s="T859">v-v:ptcp-v-v:ptcp</ta>
            <ta e="T861" id="Seg_11017" s="T860">n-n:poss-n:case</ta>
            <ta e="T862" id="Seg_11018" s="T861">v-v:cvb</ta>
            <ta e="T863" id="Seg_11019" s="T862">dempro</ta>
            <ta e="T864" id="Seg_11020" s="T863">n-n:case</ta>
            <ta e="T865" id="Seg_11021" s="T864">n-n:poss-n:case</ta>
            <ta e="T866" id="Seg_11022" s="T865">v-v:tense-v:pred.pn</ta>
            <ta e="T867" id="Seg_11023" s="T866">propr</ta>
            <ta e="T868" id="Seg_11024" s="T867">n-n:case</ta>
            <ta e="T869" id="Seg_11025" s="T868">n-n:(num)-n:poss-n:case</ta>
            <ta e="T870" id="Seg_11026" s="T869">v-v&gt;adv</ta>
            <ta e="T871" id="Seg_11027" s="T870">v-v:cvb</ta>
            <ta e="T872" id="Seg_11028" s="T871">v-v:cvb</ta>
            <ta e="T873" id="Seg_11029" s="T872">adj</ta>
            <ta e="T874" id="Seg_11030" s="T873">n-n:poss-n:case</ta>
            <ta e="T875" id="Seg_11031" s="T874">v-v:cvb</ta>
            <ta e="T876" id="Seg_11032" s="T875">v-v:tense-v:pred.pn</ta>
            <ta e="T877" id="Seg_11033" s="T876">ptcl</ta>
            <ta e="T878" id="Seg_11034" s="T877">adj</ta>
            <ta e="T879" id="Seg_11035" s="T878">v-v:(ins)-v:mood.pn</ta>
            <ta e="T880" id="Seg_11036" s="T879">pers-pro:case</ta>
            <ta e="T881" id="Seg_11037" s="T880">n-n:poss-n:case</ta>
            <ta e="T882" id="Seg_11038" s="T881">que-pro:case</ta>
            <ta e="T883" id="Seg_11039" s="T882">ptcl</ta>
            <ta e="T884" id="Seg_11040" s="T883">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T885" id="Seg_11041" s="T884">adj</ta>
            <ta e="T886" id="Seg_11042" s="T885">n-n:case</ta>
            <ta e="T887" id="Seg_11043" s="T886">v-v:(ins)-v:mood.pn</ta>
            <ta e="T888" id="Seg_11044" s="T887">adj-adj&gt;adv</ta>
            <ta e="T889" id="Seg_11045" s="T888">v-v:(ins)-v:mood.pn</ta>
            <ta e="T890" id="Seg_11046" s="T889">v-v:ptcp</ta>
            <ta e="T891" id="Seg_11047" s="T890">n-n:poss-n:case</ta>
            <ta e="T892" id="Seg_11048" s="T891">v-v:cvb</ta>
            <ta e="T893" id="Seg_11049" s="T892">n-n:case</ta>
            <ta e="T894" id="Seg_11050" s="T893">adv</ta>
            <ta e="T895" id="Seg_11051" s="T894">ptcl</ta>
            <ta e="T896" id="Seg_11052" s="T895">v-v:cvb</ta>
            <ta e="T897" id="Seg_11053" s="T896">v-v:tense-v:pred.pn</ta>
            <ta e="T898" id="Seg_11054" s="T897">n-n:case</ta>
            <ta e="T899" id="Seg_11055" s="T898">n-n:poss-n:case</ta>
            <ta e="T900" id="Seg_11056" s="T899">adj</ta>
            <ta e="T901" id="Seg_11057" s="T900">n-n:case</ta>
            <ta e="T902" id="Seg_11058" s="T901">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T903" id="Seg_11059" s="T902">post</ta>
            <ta e="T904" id="Seg_11060" s="T903">n-n:case</ta>
            <ta e="T905" id="Seg_11061" s="T904">adj</ta>
            <ta e="T906" id="Seg_11062" s="T905">n-n:poss-n:case</ta>
            <ta e="T907" id="Seg_11063" s="T906">n</ta>
            <ta e="T908" id="Seg_11064" s="T907">n-n:case</ta>
            <ta e="T909" id="Seg_11065" s="T908">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T910" id="Seg_11066" s="T909">adj</ta>
            <ta e="T911" id="Seg_11067" s="T910">n-n:case</ta>
            <ta e="T912" id="Seg_11068" s="T911">v-v:ptcp</ta>
            <ta e="T913" id="Seg_11069" s="T912">n-n:case</ta>
            <ta e="T914" id="Seg_11070" s="T913">n-n:case</ta>
            <ta e="T915" id="Seg_11071" s="T914">pers-pro:case</ta>
            <ta e="T916" id="Seg_11072" s="T915">v-v:tense-v:poss.pn</ta>
            <ta e="T917" id="Seg_11073" s="T916">n-n:case</ta>
            <ta e="T918" id="Seg_11074" s="T917">propr-n:case</ta>
            <ta e="T919" id="Seg_11075" s="T918">post</ta>
            <ta e="T920" id="Seg_11076" s="T919">v-v&gt;n-n:case</ta>
            <ta e="T921" id="Seg_11077" s="T920">v-v:tense-v:poss.pn</ta>
            <ta e="T922" id="Seg_11078" s="T921">propr-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_11079" s="T0">propr</ta>
            <ta e="T2" id="Seg_11080" s="T1">post</ta>
            <ta e="T3" id="Seg_11081" s="T2">n</ta>
            <ta e="T4" id="Seg_11082" s="T3">n</ta>
            <ta e="T5" id="Seg_11083" s="T4">n</ta>
            <ta e="T6" id="Seg_11084" s="T5">v</ta>
            <ta e="T7" id="Seg_11085" s="T6">n</ta>
            <ta e="T8" id="Seg_11086" s="T7">v</ta>
            <ta e="T9" id="Seg_11087" s="T8">adj</ta>
            <ta e="T10" id="Seg_11088" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_11089" s="T10">n</ta>
            <ta e="T12" id="Seg_11090" s="T11">n</ta>
            <ta e="T13" id="Seg_11091" s="T12">v</ta>
            <ta e="T14" id="Seg_11092" s="T13">n</ta>
            <ta e="T15" id="Seg_11093" s="T14">adv</ta>
            <ta e="T16" id="Seg_11094" s="T15">adj</ta>
            <ta e="T17" id="Seg_11095" s="T16">n</ta>
            <ta e="T18" id="Seg_11096" s="T17">cop</ta>
            <ta e="T19" id="Seg_11097" s="T18">aux</ta>
            <ta e="T20" id="Seg_11098" s="T19">dempro</ta>
            <ta e="T21" id="Seg_11099" s="T20">n</ta>
            <ta e="T22" id="Seg_11100" s="T21">adj</ta>
            <ta e="T23" id="Seg_11101" s="T22">adj</ta>
            <ta e="T24" id="Seg_11102" s="T23">n</ta>
            <ta e="T25" id="Seg_11103" s="T24">n</ta>
            <ta e="T26" id="Seg_11104" s="T25">adj</ta>
            <ta e="T27" id="Seg_11105" s="T26">n</ta>
            <ta e="T28" id="Seg_11106" s="T27">v</ta>
            <ta e="T29" id="Seg_11107" s="T28">n</ta>
            <ta e="T30" id="Seg_11108" s="T29">n</ta>
            <ta e="T31" id="Seg_11109" s="T30">v</ta>
            <ta e="T32" id="Seg_11110" s="T31">propr</ta>
            <ta e="T33" id="Seg_11111" s="T32">v</ta>
            <ta e="T34" id="Seg_11112" s="T33">n</ta>
            <ta e="T35" id="Seg_11113" s="T34">n</ta>
            <ta e="T36" id="Seg_11114" s="T35">n</ta>
            <ta e="T37" id="Seg_11115" s="T36">adv</ta>
            <ta e="T38" id="Seg_11116" s="T37">v</ta>
            <ta e="T39" id="Seg_11117" s="T38">aux</ta>
            <ta e="T40" id="Seg_11118" s="T39">n</ta>
            <ta e="T41" id="Seg_11119" s="T40">n</ta>
            <ta e="T42" id="Seg_11120" s="T41">v</ta>
            <ta e="T43" id="Seg_11121" s="T42">dempro</ta>
            <ta e="T44" id="Seg_11122" s="T43">post</ta>
            <ta e="T45" id="Seg_11123" s="T44">n</ta>
            <ta e="T46" id="Seg_11124" s="T45">v</ta>
            <ta e="T47" id="Seg_11125" s="T46">n</ta>
            <ta e="T48" id="Seg_11126" s="T47">v</ta>
            <ta e="T49" id="Seg_11127" s="T48">adj</ta>
            <ta e="T50" id="Seg_11128" s="T49">n</ta>
            <ta e="T51" id="Seg_11129" s="T50">v</ta>
            <ta e="T52" id="Seg_11130" s="T51">v</ta>
            <ta e="T53" id="Seg_11131" s="T52">n</ta>
            <ta e="T54" id="Seg_11132" s="T53">n</ta>
            <ta e="T55" id="Seg_11133" s="T54">dempro</ta>
            <ta e="T56" id="Seg_11134" s="T55">n</ta>
            <ta e="T57" id="Seg_11135" s="T56">adv</ta>
            <ta e="T58" id="Seg_11136" s="T57">adj</ta>
            <ta e="T59" id="Seg_11137" s="T58">n</ta>
            <ta e="T60" id="Seg_11138" s="T59">v</ta>
            <ta e="T61" id="Seg_11139" s="T60">adj</ta>
            <ta e="T62" id="Seg_11140" s="T61">n</ta>
            <ta e="T63" id="Seg_11141" s="T62">n</ta>
            <ta e="T64" id="Seg_11142" s="T63">adj</ta>
            <ta e="T65" id="Seg_11143" s="T64">v</ta>
            <ta e="T66" id="Seg_11144" s="T65">v</ta>
            <ta e="T67" id="Seg_11145" s="T66">v</ta>
            <ta e="T68" id="Seg_11146" s="T67">n</ta>
            <ta e="T69" id="Seg_11147" s="T68">n</ta>
            <ta e="T70" id="Seg_11148" s="T69">v</ta>
            <ta e="T71" id="Seg_11149" s="T70">n</ta>
            <ta e="T72" id="Seg_11150" s="T71">v</ta>
            <ta e="T73" id="Seg_11151" s="T72">dempro</ta>
            <ta e="T74" id="Seg_11152" s="T73">que</ta>
            <ta e="T75" id="Seg_11153" s="T74">ptcl</ta>
            <ta e="T76" id="Seg_11154" s="T75">v</ta>
            <ta e="T77" id="Seg_11155" s="T76">dempro</ta>
            <ta e="T78" id="Seg_11156" s="T77">post</ta>
            <ta e="T79" id="Seg_11157" s="T78">n</ta>
            <ta e="T80" id="Seg_11158" s="T79">cop</ta>
            <ta e="T81" id="Seg_11159" s="T80">v</ta>
            <ta e="T82" id="Seg_11160" s="T81">v</ta>
            <ta e="T83" id="Seg_11161" s="T82">n</ta>
            <ta e="T84" id="Seg_11162" s="T83">n</ta>
            <ta e="T85" id="Seg_11163" s="T84">n</ta>
            <ta e="T86" id="Seg_11164" s="T85">adj</ta>
            <ta e="T87" id="Seg_11165" s="T86">v</ta>
            <ta e="T88" id="Seg_11166" s="T87">n</ta>
            <ta e="T89" id="Seg_11167" s="T88">n</ta>
            <ta e="T90" id="Seg_11168" s="T89">n</ta>
            <ta e="T91" id="Seg_11169" s="T90">cop</ta>
            <ta e="T92" id="Seg_11170" s="T91">cardnum</ta>
            <ta e="T93" id="Seg_11171" s="T92">adj</ta>
            <ta e="T94" id="Seg_11172" s="T93">n</ta>
            <ta e="T95" id="Seg_11173" s="T94">n</ta>
            <ta e="T96" id="Seg_11174" s="T95">adv</ta>
            <ta e="T97" id="Seg_11175" s="T96">v</ta>
            <ta e="T98" id="Seg_11176" s="T97">v</ta>
            <ta e="T99" id="Seg_11177" s="T98">v</ta>
            <ta e="T100" id="Seg_11178" s="T99">v</ta>
            <ta e="T101" id="Seg_11179" s="T100">n</ta>
            <ta e="T102" id="Seg_11180" s="T101">n</ta>
            <ta e="T103" id="Seg_11181" s="T102">que</ta>
            <ta e="T104" id="Seg_11182" s="T103">ptcl</ta>
            <ta e="T105" id="Seg_11183" s="T104">v</ta>
            <ta e="T106" id="Seg_11184" s="T105">n</ta>
            <ta e="T107" id="Seg_11185" s="T106">n</ta>
            <ta e="T108" id="Seg_11186" s="T107">post</ta>
            <ta e="T109" id="Seg_11187" s="T108">ptcl</ta>
            <ta e="T110" id="Seg_11188" s="T109">v</ta>
            <ta e="T111" id="Seg_11189" s="T110">n</ta>
            <ta e="T112" id="Seg_11190" s="T111">n</ta>
            <ta e="T113" id="Seg_11191" s="T112">n</ta>
            <ta e="T114" id="Seg_11192" s="T113">n</ta>
            <ta e="T115" id="Seg_11193" s="T114">n</ta>
            <ta e="T116" id="Seg_11194" s="T115">v</ta>
            <ta e="T117" id="Seg_11195" s="T116">n</ta>
            <ta e="T118" id="Seg_11196" s="T117">que</ta>
            <ta e="T119" id="Seg_11197" s="T118">cop</ta>
            <ta e="T120" id="Seg_11198" s="T119">n</ta>
            <ta e="T121" id="Seg_11199" s="T120">n</ta>
            <ta e="T122" id="Seg_11200" s="T121">n</ta>
            <ta e="T123" id="Seg_11201" s="T122">v</ta>
            <ta e="T124" id="Seg_11202" s="T123">que</ta>
            <ta e="T125" id="Seg_11203" s="T124">v</ta>
            <ta e="T126" id="Seg_11204" s="T125">aux</ta>
            <ta e="T127" id="Seg_11205" s="T126">v</ta>
            <ta e="T128" id="Seg_11206" s="T127">v</ta>
            <ta e="T129" id="Seg_11207" s="T128">propr</ta>
            <ta e="T130" id="Seg_11208" s="T129">adj</ta>
            <ta e="T131" id="Seg_11209" s="T130">n</ta>
            <ta e="T132" id="Seg_11210" s="T131">v</ta>
            <ta e="T133" id="Seg_11211" s="T132">aux</ta>
            <ta e="T134" id="Seg_11212" s="T133">pers</ta>
            <ta e="T135" id="Seg_11213" s="T134">ptcl</ta>
            <ta e="T136" id="Seg_11214" s="T135">adj</ta>
            <ta e="T137" id="Seg_11215" s="T136">adv</ta>
            <ta e="T138" id="Seg_11216" s="T137">que</ta>
            <ta e="T139" id="Seg_11217" s="T138">ptcl</ta>
            <ta e="T140" id="Seg_11218" s="T139">v</ta>
            <ta e="T141" id="Seg_11219" s="T140">v</ta>
            <ta e="T142" id="Seg_11220" s="T141">n</ta>
            <ta e="T143" id="Seg_11221" s="T142">adv</ta>
            <ta e="T144" id="Seg_11222" s="T143">v</ta>
            <ta e="T145" id="Seg_11223" s="T144">propr</ta>
            <ta e="T146" id="Seg_11224" s="T145">n</ta>
            <ta e="T147" id="Seg_11225" s="T146">adj</ta>
            <ta e="T148" id="Seg_11226" s="T147">n</ta>
            <ta e="T149" id="Seg_11227" s="T148">v</ta>
            <ta e="T150" id="Seg_11228" s="T149">aux</ta>
            <ta e="T151" id="Seg_11229" s="T150">v</ta>
            <ta e="T152" id="Seg_11230" s="T151">v</ta>
            <ta e="T153" id="Seg_11231" s="T152">que</ta>
            <ta e="T154" id="Seg_11232" s="T153">v</ta>
            <ta e="T155" id="Seg_11233" s="T154">dempro</ta>
            <ta e="T156" id="Seg_11234" s="T155">adv</ta>
            <ta e="T157" id="Seg_11235" s="T156">cop</ta>
            <ta e="T158" id="Seg_11236" s="T157">n</ta>
            <ta e="T159" id="Seg_11237" s="T158">dempro</ta>
            <ta e="T160" id="Seg_11238" s="T159">adj</ta>
            <ta e="T161" id="Seg_11239" s="T160">n</ta>
            <ta e="T162" id="Seg_11240" s="T161">v</ta>
            <ta e="T163" id="Seg_11241" s="T162">ptcl</ta>
            <ta e="T164" id="Seg_11242" s="T163">ptcl</ta>
            <ta e="T165" id="Seg_11243" s="T164">adv</ta>
            <ta e="T166" id="Seg_11244" s="T165">adj</ta>
            <ta e="T167" id="Seg_11245" s="T166">pers</ta>
            <ta e="T168" id="Seg_11246" s="T167">n</ta>
            <ta e="T169" id="Seg_11247" s="T168">cop</ta>
            <ta e="T170" id="Seg_11248" s="T169">aux</ta>
            <ta e="T171" id="Seg_11249" s="T170">dempro</ta>
            <ta e="T172" id="Seg_11250" s="T171">pers</ta>
            <ta e="T173" id="Seg_11251" s="T172">propr</ta>
            <ta e="T174" id="Seg_11252" s="T173">n</ta>
            <ta e="T175" id="Seg_11253" s="T174">n</ta>
            <ta e="T176" id="Seg_11254" s="T175">v</ta>
            <ta e="T177" id="Seg_11255" s="T176">n</ta>
            <ta e="T178" id="Seg_11256" s="T177">n</ta>
            <ta e="T179" id="Seg_11257" s="T178">v</ta>
            <ta e="T180" id="Seg_11258" s="T179">pers</ta>
            <ta e="T181" id="Seg_11259" s="T180">adv</ta>
            <ta e="T182" id="Seg_11260" s="T181">v</ta>
            <ta e="T183" id="Seg_11261" s="T182">n</ta>
            <ta e="T184" id="Seg_11262" s="T183">v</ta>
            <ta e="T185" id="Seg_11263" s="T184">cop</ta>
            <ta e="T186" id="Seg_11264" s="T185">aux</ta>
            <ta e="T187" id="Seg_11265" s="T186">que</ta>
            <ta e="T188" id="Seg_11266" s="T187">pers</ta>
            <ta e="T189" id="Seg_11267" s="T188">n</ta>
            <ta e="T190" id="Seg_11268" s="T189">v</ta>
            <ta e="T191" id="Seg_11269" s="T190">n</ta>
            <ta e="T192" id="Seg_11270" s="T191">adj</ta>
            <ta e="T193" id="Seg_11271" s="T192">adv</ta>
            <ta e="T194" id="Seg_11272" s="T193">adj</ta>
            <ta e="T195" id="Seg_11273" s="T194">adj</ta>
            <ta e="T196" id="Seg_11274" s="T195">ptcl</ta>
            <ta e="T197" id="Seg_11275" s="T196">adj</ta>
            <ta e="T198" id="Seg_11276" s="T197">ptcl</ta>
            <ta e="T199" id="Seg_11277" s="T198">n</ta>
            <ta e="T200" id="Seg_11278" s="T199">n</ta>
            <ta e="T201" id="Seg_11279" s="T200">v</ta>
            <ta e="T202" id="Seg_11280" s="T201">propr</ta>
            <ta e="T203" id="Seg_11281" s="T202">n</ta>
            <ta e="T205" id="Seg_11282" s="T204">n</ta>
            <ta e="T206" id="Seg_11283" s="T205">n</ta>
            <ta e="T207" id="Seg_11284" s="T206">v</ta>
            <ta e="T208" id="Seg_11285" s="T207">v</ta>
            <ta e="T209" id="Seg_11286" s="T208">ptcl</ta>
            <ta e="T210" id="Seg_11287" s="T209">pers</ta>
            <ta e="T211" id="Seg_11288" s="T210">v</ta>
            <ta e="T212" id="Seg_11289" s="T211">n</ta>
            <ta e="T213" id="Seg_11290" s="T212">n</ta>
            <ta e="T214" id="Seg_11291" s="T213">n</ta>
            <ta e="T215" id="Seg_11292" s="T214">post</ta>
            <ta e="T216" id="Seg_11293" s="T215">n</ta>
            <ta e="T217" id="Seg_11294" s="T216">ptcl</ta>
            <ta e="T218" id="Seg_11295" s="T217">v</ta>
            <ta e="T219" id="Seg_11296" s="T218">que</ta>
            <ta e="T220" id="Seg_11297" s="T219">ptcl</ta>
            <ta e="T221" id="Seg_11298" s="T220">v</ta>
            <ta e="T222" id="Seg_11299" s="T221">pers</ta>
            <ta e="T223" id="Seg_11300" s="T222">n</ta>
            <ta e="T224" id="Seg_11301" s="T223">n</ta>
            <ta e="T225" id="Seg_11302" s="T224">v</ta>
            <ta e="T226" id="Seg_11303" s="T225">ptcl</ta>
            <ta e="T227" id="Seg_11304" s="T226">propr</ta>
            <ta e="T228" id="Seg_11305" s="T227">n</ta>
            <ta e="T229" id="Seg_11306" s="T228">v</ta>
            <ta e="T230" id="Seg_11307" s="T229">que</ta>
            <ta e="T231" id="Seg_11308" s="T230">pers</ta>
            <ta e="T232" id="Seg_11309" s="T231">n</ta>
            <ta e="T233" id="Seg_11310" s="T232">v</ta>
            <ta e="T234" id="Seg_11311" s="T233">n</ta>
            <ta e="T235" id="Seg_11312" s="T234">adv</ta>
            <ta e="T236" id="Seg_11313" s="T235">v</ta>
            <ta e="T237" id="Seg_11314" s="T236">n</ta>
            <ta e="T238" id="Seg_11315" s="T237">n</ta>
            <ta e="T239" id="Seg_11316" s="T238">v</ta>
            <ta e="T240" id="Seg_11317" s="T239">dempro</ta>
            <ta e="T241" id="Seg_11318" s="T240">n</ta>
            <ta e="T242" id="Seg_11319" s="T241">adv</ta>
            <ta e="T243" id="Seg_11320" s="T242">cop</ta>
            <ta e="T244" id="Seg_11321" s="T243">adv</ta>
            <ta e="T245" id="Seg_11322" s="T244">v</ta>
            <ta e="T246" id="Seg_11323" s="T245">propr</ta>
            <ta e="T247" id="Seg_11324" s="T246">v</ta>
            <ta e="T248" id="Seg_11325" s="T247">aux</ta>
            <ta e="T249" id="Seg_11326" s="T248">pers</ta>
            <ta e="T250" id="Seg_11327" s="T249">propr</ta>
            <ta e="T251" id="Seg_11328" s="T250">n</ta>
            <ta e="T252" id="Seg_11329" s="T251">v</ta>
            <ta e="T253" id="Seg_11330" s="T252">v</ta>
            <ta e="T254" id="Seg_11331" s="T253">n</ta>
            <ta e="T255" id="Seg_11332" s="T254">n</ta>
            <ta e="T256" id="Seg_11333" s="T255">v</ta>
            <ta e="T257" id="Seg_11334" s="T256">v</ta>
            <ta e="T258" id="Seg_11335" s="T257">n</ta>
            <ta e="T259" id="Seg_11336" s="T258">ptcl</ta>
            <ta e="T260" id="Seg_11337" s="T259">ptcl</ta>
            <ta e="T261" id="Seg_11338" s="T260">dempro</ta>
            <ta e="T262" id="Seg_11339" s="T261">v</ta>
            <ta e="T263" id="Seg_11340" s="T262">n</ta>
            <ta e="T264" id="Seg_11341" s="T263">n</ta>
            <ta e="T265" id="Seg_11342" s="T264">post</ta>
            <ta e="T266" id="Seg_11343" s="T265">v</ta>
            <ta e="T267" id="Seg_11344" s="T266">aux</ta>
            <ta e="T268" id="Seg_11345" s="T267">n</ta>
            <ta e="T269" id="Seg_11346" s="T268">v</ta>
            <ta e="T270" id="Seg_11347" s="T269">que</ta>
            <ta e="T271" id="Seg_11348" s="T270">pers</ta>
            <ta e="T272" id="Seg_11349" s="T271">v</ta>
            <ta e="T273" id="Seg_11350" s="T272">n</ta>
            <ta e="T274" id="Seg_11351" s="T273">v</ta>
            <ta e="T275" id="Seg_11352" s="T274">n</ta>
            <ta e="T276" id="Seg_11353" s="T275">propr</ta>
            <ta e="T277" id="Seg_11354" s="T276">propr</ta>
            <ta e="T278" id="Seg_11355" s="T277">propr</ta>
            <ta e="T279" id="Seg_11356" s="T278">v</ta>
            <ta e="T280" id="Seg_11357" s="T279">aux</ta>
            <ta e="T281" id="Seg_11358" s="T280">n</ta>
            <ta e="T282" id="Seg_11359" s="T281">v</ta>
            <ta e="T283" id="Seg_11360" s="T282">ptcl</ta>
            <ta e="T284" id="Seg_11361" s="T283">n</ta>
            <ta e="T285" id="Seg_11362" s="T284">pers</ta>
            <ta e="T286" id="Seg_11363" s="T285">v</ta>
            <ta e="T287" id="Seg_11364" s="T286">adv</ta>
            <ta e="T288" id="Seg_11365" s="T287">v</ta>
            <ta e="T289" id="Seg_11366" s="T288">aux</ta>
            <ta e="T290" id="Seg_11367" s="T289">adj</ta>
            <ta e="T291" id="Seg_11368" s="T290">n</ta>
            <ta e="T292" id="Seg_11369" s="T291">v</ta>
            <ta e="T293" id="Seg_11370" s="T292">n</ta>
            <ta e="T294" id="Seg_11371" s="T293">que</ta>
            <ta e="T295" id="Seg_11372" s="T294">ptcl</ta>
            <ta e="T296" id="Seg_11373" s="T295">v</ta>
            <ta e="T297" id="Seg_11374" s="T296">adv</ta>
            <ta e="T298" id="Seg_11375" s="T297">n</ta>
            <ta e="T299" id="Seg_11376" s="T298">interj</ta>
            <ta e="T300" id="Seg_11377" s="T299">v</ta>
            <ta e="T301" id="Seg_11378" s="T300">v</ta>
            <ta e="T302" id="Seg_11379" s="T301">n</ta>
            <ta e="T303" id="Seg_11380" s="T302">v</ta>
            <ta e="T304" id="Seg_11381" s="T303">aux</ta>
            <ta e="T305" id="Seg_11382" s="T304">que</ta>
            <ta e="T306" id="Seg_11383" s="T305">adv</ta>
            <ta e="T307" id="Seg_11384" s="T306">que</ta>
            <ta e="T308" id="Seg_11385" s="T307">adj</ta>
            <ta e="T309" id="Seg_11386" s="T308">v</ta>
            <ta e="T310" id="Seg_11387" s="T309">v</ta>
            <ta e="T311" id="Seg_11388" s="T310">n</ta>
            <ta e="T312" id="Seg_11389" s="T311">que</ta>
            <ta e="T313" id="Seg_11390" s="T312">ptcl</ta>
            <ta e="T314" id="Seg_11391" s="T313">v</ta>
            <ta e="T315" id="Seg_11392" s="T314">v</ta>
            <ta e="T316" id="Seg_11393" s="T315">v</ta>
            <ta e="T317" id="Seg_11394" s="T316">v</ta>
            <ta e="T318" id="Seg_11395" s="T317">que</ta>
            <ta e="T319" id="Seg_11396" s="T318">n</ta>
            <ta e="T320" id="Seg_11397" s="T319">n</ta>
            <ta e="T321" id="Seg_11398" s="T320">v</ta>
            <ta e="T322" id="Seg_11399" s="T321">que</ta>
            <ta e="T323" id="Seg_11400" s="T322">v</ta>
            <ta e="T324" id="Seg_11401" s="T323">n</ta>
            <ta e="T325" id="Seg_11402" s="T324">v</ta>
            <ta e="T326" id="Seg_11403" s="T325">n</ta>
            <ta e="T327" id="Seg_11404" s="T326">n</ta>
            <ta e="T328" id="Seg_11405" s="T327">dempro</ta>
            <ta e="T329" id="Seg_11406" s="T328">n</ta>
            <ta e="T330" id="Seg_11407" s="T329">v</ta>
            <ta e="T331" id="Seg_11408" s="T330">propr</ta>
            <ta e="T332" id="Seg_11409" s="T331">n</ta>
            <ta e="T333" id="Seg_11410" s="T332">n</ta>
            <ta e="T334" id="Seg_11411" s="T333">propr</ta>
            <ta e="T335" id="Seg_11412" s="T334">propr</ta>
            <ta e="T336" id="Seg_11413" s="T335">ptcl</ta>
            <ta e="T337" id="Seg_11414" s="T336">v</ta>
            <ta e="T338" id="Seg_11415" s="T337">aux</ta>
            <ta e="T339" id="Seg_11416" s="T338">pers</ta>
            <ta e="T340" id="Seg_11417" s="T339">n</ta>
            <ta e="T341" id="Seg_11418" s="T340">ptcl</ta>
            <ta e="T342" id="Seg_11419" s="T341">ptcl</ta>
            <ta e="T343" id="Seg_11420" s="T342">quant</ta>
            <ta e="T344" id="Seg_11421" s="T343">v</ta>
            <ta e="T345" id="Seg_11422" s="T344">propr</ta>
            <ta e="T346" id="Seg_11423" s="T345">n</ta>
            <ta e="T347" id="Seg_11424" s="T346">ptcl</ta>
            <ta e="T348" id="Seg_11425" s="T347">propr</ta>
            <ta e="T349" id="Seg_11426" s="T348">v</ta>
            <ta e="T350" id="Seg_11427" s="T349">ptcl</ta>
            <ta e="T351" id="Seg_11428" s="T350">n</ta>
            <ta e="T352" id="Seg_11429" s="T351">n</ta>
            <ta e="T353" id="Seg_11430" s="T352">v</ta>
            <ta e="T354" id="Seg_11431" s="T353">adj</ta>
            <ta e="T355" id="Seg_11432" s="T354">cop</ta>
            <ta e="T356" id="Seg_11433" s="T355">n</ta>
            <ta e="T357" id="Seg_11434" s="T356">n</ta>
            <ta e="T358" id="Seg_11435" s="T357">n</ta>
            <ta e="T359" id="Seg_11436" s="T358">v</ta>
            <ta e="T360" id="Seg_11437" s="T359">ptcl</ta>
            <ta e="T361" id="Seg_11438" s="T360">n</ta>
            <ta e="T362" id="Seg_11439" s="T361">post</ta>
            <ta e="T363" id="Seg_11440" s="T362">v</ta>
            <ta e="T364" id="Seg_11441" s="T363">v</ta>
            <ta e="T365" id="Seg_11442" s="T364">ptcl</ta>
            <ta e="T366" id="Seg_11443" s="T365">n</ta>
            <ta e="T367" id="Seg_11444" s="T366">v</ta>
            <ta e="T368" id="Seg_11445" s="T367">n</ta>
            <ta e="T369" id="Seg_11446" s="T368">adj</ta>
            <ta e="T370" id="Seg_11447" s="T369">n</ta>
            <ta e="T371" id="Seg_11448" s="T370">v</ta>
            <ta e="T372" id="Seg_11449" s="T371">v</ta>
            <ta e="T373" id="Seg_11450" s="T372">n</ta>
            <ta e="T376" id="Seg_11451" s="T375">v</ta>
            <ta e="T377" id="Seg_11452" s="T376">adv</ta>
            <ta e="T378" id="Seg_11453" s="T377">v</ta>
            <ta e="T379" id="Seg_11454" s="T378">n</ta>
            <ta e="T380" id="Seg_11455" s="T379">v</ta>
            <ta e="T381" id="Seg_11456" s="T380">post</ta>
            <ta e="T382" id="Seg_11457" s="T381">n</ta>
            <ta e="T383" id="Seg_11458" s="T382">emphpro</ta>
            <ta e="T384" id="Seg_11459" s="T383">v</ta>
            <ta e="T385" id="Seg_11460" s="T384">propr</ta>
            <ta e="T386" id="Seg_11461" s="T385">n</ta>
            <ta e="T387" id="Seg_11462" s="T386">v</ta>
            <ta e="T388" id="Seg_11463" s="T387">v</ta>
            <ta e="T389" id="Seg_11464" s="T388">v</ta>
            <ta e="T390" id="Seg_11465" s="T389">n</ta>
            <ta e="T391" id="Seg_11466" s="T390">n</ta>
            <ta e="T392" id="Seg_11467" s="T391">v</ta>
            <ta e="T393" id="Seg_11468" s="T392">n</ta>
            <ta e="T394" id="Seg_11469" s="T393">n</ta>
            <ta e="T395" id="Seg_11470" s="T394">v</ta>
            <ta e="T396" id="Seg_11471" s="T395">n</ta>
            <ta e="T397" id="Seg_11472" s="T396">n</ta>
            <ta e="T398" id="Seg_11473" s="T397">n</ta>
            <ta e="T399" id="Seg_11474" s="T398">v</ta>
            <ta e="T400" id="Seg_11475" s="T399">n</ta>
            <ta e="T401" id="Seg_11476" s="T400">adj</ta>
            <ta e="T402" id="Seg_11477" s="T401">adj</ta>
            <ta e="T403" id="Seg_11478" s="T402">n</ta>
            <ta e="T404" id="Seg_11479" s="T403">n</ta>
            <ta e="T405" id="Seg_11480" s="T404">v</ta>
            <ta e="T406" id="Seg_11481" s="T405">n</ta>
            <ta e="T407" id="Seg_11482" s="T406">indfpro</ta>
            <ta e="T408" id="Seg_11483" s="T407">v</ta>
            <ta e="T409" id="Seg_11484" s="T408">n</ta>
            <ta e="T410" id="Seg_11485" s="T409">v</ta>
            <ta e="T411" id="Seg_11486" s="T410">aux</ta>
            <ta e="T412" id="Seg_11487" s="T411">adj</ta>
            <ta e="T413" id="Seg_11488" s="T412">n</ta>
            <ta e="T414" id="Seg_11489" s="T413">n</ta>
            <ta e="T415" id="Seg_11490" s="T414">n</ta>
            <ta e="T416" id="Seg_11491" s="T415">v</ta>
            <ta e="T417" id="Seg_11492" s="T416">adj</ta>
            <ta e="T418" id="Seg_11493" s="T417">adj</ta>
            <ta e="T419" id="Seg_11494" s="T418">adj</ta>
            <ta e="T420" id="Seg_11495" s="T419">n</ta>
            <ta e="T421" id="Seg_11496" s="T420">n</ta>
            <ta e="T422" id="Seg_11497" s="T421">n</ta>
            <ta e="T423" id="Seg_11498" s="T422">n</ta>
            <ta e="T424" id="Seg_11499" s="T423">n</ta>
            <ta e="T425" id="Seg_11500" s="T424">n</ta>
            <ta e="T426" id="Seg_11501" s="T425">post</ta>
            <ta e="T429" id="Seg_11502" s="T428">n</ta>
            <ta e="T430" id="Seg_11503" s="T429">v</ta>
            <ta e="T431" id="Seg_11504" s="T430">v</ta>
            <ta e="T432" id="Seg_11505" s="T431">propr</ta>
            <ta e="T433" id="Seg_11506" s="T432">n</ta>
            <ta e="T434" id="Seg_11507" s="T433">que</ta>
            <ta e="T435" id="Seg_11508" s="T434">v</ta>
            <ta e="T436" id="Seg_11509" s="T435">adv</ta>
            <ta e="T437" id="Seg_11510" s="T436">v</ta>
            <ta e="T438" id="Seg_11511" s="T437">n</ta>
            <ta e="T439" id="Seg_11512" s="T438">v</ta>
            <ta e="T440" id="Seg_11513" s="T439">ptcl</ta>
            <ta e="T441" id="Seg_11514" s="T440">aux</ta>
            <ta e="T442" id="Seg_11515" s="T441">v</ta>
            <ta e="T443" id="Seg_11516" s="T442">adv</ta>
            <ta e="T444" id="Seg_11517" s="T443">n</ta>
            <ta e="T445" id="Seg_11518" s="T444">n</ta>
            <ta e="T446" id="Seg_11519" s="T445">n</ta>
            <ta e="T447" id="Seg_11520" s="T446">cop</ta>
            <ta e="T448" id="Seg_11521" s="T447">adj</ta>
            <ta e="T449" id="Seg_11522" s="T448">n</ta>
            <ta e="T450" id="Seg_11523" s="T449">adv</ta>
            <ta e="T451" id="Seg_11524" s="T450">v</ta>
            <ta e="T452" id="Seg_11525" s="T451">adj</ta>
            <ta e="T453" id="Seg_11526" s="T452">cop</ta>
            <ta e="T454" id="Seg_11527" s="T453">adj</ta>
            <ta e="T455" id="Seg_11528" s="T454">n</ta>
            <ta e="T456" id="Seg_11529" s="T455">adv</ta>
            <ta e="T457" id="Seg_11530" s="T456">v</ta>
            <ta e="T458" id="Seg_11531" s="T457">v</ta>
            <ta e="T459" id="Seg_11532" s="T458">que</ta>
            <ta e="T460" id="Seg_11533" s="T459">ptcl</ta>
            <ta e="T461" id="Seg_11534" s="T460">v</ta>
            <ta e="T462" id="Seg_11535" s="T461">v</ta>
            <ta e="T463" id="Seg_11536" s="T462">adj</ta>
            <ta e="T464" id="Seg_11537" s="T463">n</ta>
            <ta e="T465" id="Seg_11538" s="T464">n</ta>
            <ta e="T466" id="Seg_11539" s="T465">v</ta>
            <ta e="T467" id="Seg_11540" s="T466">n</ta>
            <ta e="T468" id="Seg_11541" s="T467">v</ta>
            <ta e="T469" id="Seg_11542" s="T468">adv</ta>
            <ta e="T470" id="Seg_11543" s="T469">cop</ta>
            <ta e="T471" id="Seg_11544" s="T470">dempro</ta>
            <ta e="T472" id="Seg_11545" s="T471">n</ta>
            <ta e="T473" id="Seg_11546" s="T472">adj</ta>
            <ta e="T474" id="Seg_11547" s="T473">ptcl</ta>
            <ta e="T475" id="Seg_11548" s="T474">ptcl</ta>
            <ta e="T476" id="Seg_11549" s="T475">adj</ta>
            <ta e="T477" id="Seg_11550" s="T476">n</ta>
            <ta e="T478" id="Seg_11551" s="T477">adj</ta>
            <ta e="T479" id="Seg_11552" s="T478">n</ta>
            <ta e="T480" id="Seg_11553" s="T479">adv</ta>
            <ta e="T481" id="Seg_11554" s="T480">n</ta>
            <ta e="T482" id="Seg_11555" s="T481">v</ta>
            <ta e="T483" id="Seg_11556" s="T482">adv</ta>
            <ta e="T484" id="Seg_11557" s="T483">n</ta>
            <ta e="T485" id="Seg_11558" s="T484">adj</ta>
            <ta e="T486" id="Seg_11559" s="T485">n</ta>
            <ta e="T487" id="Seg_11560" s="T486">v</ta>
            <ta e="T488" id="Seg_11561" s="T487">adj</ta>
            <ta e="T489" id="Seg_11562" s="T488">n</ta>
            <ta e="T490" id="Seg_11563" s="T489">n</ta>
            <ta e="T491" id="Seg_11564" s="T490">n</ta>
            <ta e="T492" id="Seg_11565" s="T491">n</ta>
            <ta e="T493" id="Seg_11566" s="T492">v</ta>
            <ta e="T494" id="Seg_11567" s="T493">n</ta>
            <ta e="T495" id="Seg_11568" s="T494">v</ta>
            <ta e="T496" id="Seg_11569" s="T495">v</ta>
            <ta e="T497" id="Seg_11570" s="T496">que</ta>
            <ta e="T498" id="Seg_11571" s="T497">n</ta>
            <ta e="T499" id="Seg_11572" s="T498">dempro</ta>
            <ta e="T500" id="Seg_11573" s="T499">n</ta>
            <ta e="T501" id="Seg_11574" s="T500">v</ta>
            <ta e="T502" id="Seg_11575" s="T501">n</ta>
            <ta e="T503" id="Seg_11576" s="T502">v</ta>
            <ta e="T504" id="Seg_11577" s="T503">v</ta>
            <ta e="T505" id="Seg_11578" s="T504">aux</ta>
            <ta e="T506" id="Seg_11579" s="T505">n</ta>
            <ta e="T507" id="Seg_11580" s="T506">adj</ta>
            <ta e="T508" id="Seg_11581" s="T507">adv</ta>
            <ta e="T509" id="Seg_11582" s="T508">v</ta>
            <ta e="T510" id="Seg_11583" s="T509">v</ta>
            <ta e="T511" id="Seg_11584" s="T510">adj</ta>
            <ta e="T512" id="Seg_11585" s="T511">n</ta>
            <ta e="T513" id="Seg_11586" s="T512">n</ta>
            <ta e="T514" id="Seg_11587" s="T513">v</ta>
            <ta e="T515" id="Seg_11588" s="T514">n</ta>
            <ta e="T516" id="Seg_11589" s="T515">adv</ta>
            <ta e="T517" id="Seg_11590" s="T516">adj</ta>
            <ta e="T518" id="Seg_11591" s="T517">n</ta>
            <ta e="T519" id="Seg_11592" s="T518">adv</ta>
            <ta e="T520" id="Seg_11593" s="T519">v</ta>
            <ta e="T521" id="Seg_11594" s="T520">ptcl</ta>
            <ta e="T522" id="Seg_11595" s="T521">n</ta>
            <ta e="T523" id="Seg_11596" s="T522">n</ta>
            <ta e="T524" id="Seg_11597" s="T523">v</ta>
            <ta e="T525" id="Seg_11598" s="T524">ptcl</ta>
            <ta e="T526" id="Seg_11599" s="T525">n</ta>
            <ta e="T527" id="Seg_11600" s="T526">pers</ta>
            <ta e="T528" id="Seg_11601" s="T527">v</ta>
            <ta e="T529" id="Seg_11602" s="T528">dempro</ta>
            <ta e="T530" id="Seg_11603" s="T529">v</ta>
            <ta e="T531" id="Seg_11604" s="T530">ptcl</ta>
            <ta e="T532" id="Seg_11605" s="T531">n</ta>
            <ta e="T533" id="Seg_11606" s="T532">n</ta>
            <ta e="T534" id="Seg_11607" s="T533">v</ta>
            <ta e="T535" id="Seg_11608" s="T534">adv</ta>
            <ta e="T536" id="Seg_11609" s="T535">adj</ta>
            <ta e="T537" id="Seg_11610" s="T536">n</ta>
            <ta e="T538" id="Seg_11611" s="T537">v</ta>
            <ta e="T539" id="Seg_11612" s="T538">v</ta>
            <ta e="T540" id="Seg_11613" s="T539">v</ta>
            <ta e="T541" id="Seg_11614" s="T540">n</ta>
            <ta e="T542" id="Seg_11615" s="T541">n</ta>
            <ta e="T543" id="Seg_11616" s="T542">que</ta>
            <ta e="T544" id="Seg_11617" s="T543">ptcl</ta>
            <ta e="T545" id="Seg_11618" s="T544">v</ta>
            <ta e="T546" id="Seg_11619" s="T545">n</ta>
            <ta e="T547" id="Seg_11620" s="T546">n</ta>
            <ta e="T548" id="Seg_11621" s="T547">v</ta>
            <ta e="T549" id="Seg_11622" s="T548">pers</ta>
            <ta e="T550" id="Seg_11623" s="T549">n</ta>
            <ta e="T551" id="Seg_11624" s="T550">que</ta>
            <ta e="T552" id="Seg_11625" s="T551">adj</ta>
            <ta e="T553" id="Seg_11626" s="T552">cop</ta>
            <ta e="T554" id="Seg_11627" s="T553">v</ta>
            <ta e="T555" id="Seg_11628" s="T554">n</ta>
            <ta e="T556" id="Seg_11629" s="T555">v</ta>
            <ta e="T557" id="Seg_11630" s="T556">n</ta>
            <ta e="T558" id="Seg_11631" s="T557">indfpro</ta>
            <ta e="T559" id="Seg_11632" s="T558">post</ta>
            <ta e="T560" id="Seg_11633" s="T559">n</ta>
            <ta e="T561" id="Seg_11634" s="T560">v</ta>
            <ta e="T562" id="Seg_11635" s="T561">aux</ta>
            <ta e="T563" id="Seg_11636" s="T562">n</ta>
            <ta e="T564" id="Seg_11637" s="T563">v</ta>
            <ta e="T565" id="Seg_11638" s="T564">adj</ta>
            <ta e="T566" id="Seg_11639" s="T565">n</ta>
            <ta e="T567" id="Seg_11640" s="T566">n</ta>
            <ta e="T569" id="Seg_11641" s="T568">n</ta>
            <ta e="T570" id="Seg_11642" s="T569">n</ta>
            <ta e="T571" id="Seg_11643" s="T570">v</ta>
            <ta e="T572" id="Seg_11644" s="T571">adv</ta>
            <ta e="T573" id="Seg_11645" s="T572">adv</ta>
            <ta e="T574" id="Seg_11646" s="T573">adv</ta>
            <ta e="T575" id="Seg_11647" s="T574">adj</ta>
            <ta e="T576" id="Seg_11648" s="T575">n</ta>
            <ta e="T577" id="Seg_11649" s="T576">adj</ta>
            <ta e="T578" id="Seg_11650" s="T577">n</ta>
            <ta e="T579" id="Seg_11651" s="T578">v</ta>
            <ta e="T580" id="Seg_11652" s="T579">adv</ta>
            <ta e="T581" id="Seg_11653" s="T580">v</ta>
            <ta e="T582" id="Seg_11654" s="T581">aux</ta>
            <ta e="T583" id="Seg_11655" s="T582">adv</ta>
            <ta e="T584" id="Seg_11656" s="T583">adj</ta>
            <ta e="T585" id="Seg_11657" s="T584">n</ta>
            <ta e="T586" id="Seg_11658" s="T585">n</ta>
            <ta e="T587" id="Seg_11659" s="T586">post</ta>
            <ta e="T588" id="Seg_11660" s="T587">que</ta>
            <ta e="T589" id="Seg_11661" s="T588">v</ta>
            <ta e="T590" id="Seg_11662" s="T589">v</ta>
            <ta e="T591" id="Seg_11663" s="T590">adv</ta>
            <ta e="T592" id="Seg_11664" s="T591">v</ta>
            <ta e="T593" id="Seg_11665" s="T592">v</ta>
            <ta e="T594" id="Seg_11666" s="T593">dempro</ta>
            <ta e="T595" id="Seg_11667" s="T594">n</ta>
            <ta e="T596" id="Seg_11668" s="T595">v</ta>
            <ta e="T597" id="Seg_11669" s="T596">n</ta>
            <ta e="T598" id="Seg_11670" s="T597">n</ta>
            <ta e="T599" id="Seg_11671" s="T598">dempro</ta>
            <ta e="T600" id="Seg_11672" s="T599">n</ta>
            <ta e="T601" id="Seg_11673" s="T600">n</ta>
            <ta e="T602" id="Seg_11674" s="T601">v</ta>
            <ta e="T603" id="Seg_11675" s="T602">aux</ta>
            <ta e="T604" id="Seg_11676" s="T603">adv</ta>
            <ta e="T605" id="Seg_11677" s="T604">adj</ta>
            <ta e="T606" id="Seg_11678" s="T605">n</ta>
            <ta e="T607" id="Seg_11679" s="T606">v</ta>
            <ta e="T608" id="Seg_11680" s="T607">n</ta>
            <ta e="T609" id="Seg_11681" s="T608">n</ta>
            <ta e="T610" id="Seg_11682" s="T609">v</ta>
            <ta e="T611" id="Seg_11683" s="T610">v</ta>
            <ta e="T612" id="Seg_11684" s="T611">n</ta>
            <ta e="T613" id="Seg_11685" s="T612">n</ta>
            <ta e="T614" id="Seg_11686" s="T613">n</ta>
            <ta e="T615" id="Seg_11687" s="T614">n</ta>
            <ta e="T616" id="Seg_11688" s="T615">cardnum</ta>
            <ta e="T617" id="Seg_11689" s="T616">adj</ta>
            <ta e="T618" id="Seg_11690" s="T617">n</ta>
            <ta e="T619" id="Seg_11691" s="T618">v</ta>
            <ta e="T620" id="Seg_11692" s="T619">n</ta>
            <ta e="T621" id="Seg_11693" s="T620">post</ta>
            <ta e="T622" id="Seg_11694" s="T621">n</ta>
            <ta e="T623" id="Seg_11695" s="T622">cop</ta>
            <ta e="T624" id="Seg_11696" s="T623">dempro</ta>
            <ta e="T625" id="Seg_11697" s="T624">n</ta>
            <ta e="T626" id="Seg_11698" s="T625">pers</ta>
            <ta e="T627" id="Seg_11699" s="T626">adj</ta>
            <ta e="T628" id="Seg_11700" s="T627">n</ta>
            <ta e="T629" id="Seg_11701" s="T628">v</ta>
            <ta e="T630" id="Seg_11702" s="T629">n</ta>
            <ta e="T631" id="Seg_11703" s="T630">v</ta>
            <ta e="T632" id="Seg_11704" s="T631">v</ta>
            <ta e="T633" id="Seg_11705" s="T632">que</ta>
            <ta e="T634" id="Seg_11706" s="T633">ptcl</ta>
            <ta e="T635" id="Seg_11707" s="T634">v</ta>
            <ta e="T636" id="Seg_11708" s="T635">v</ta>
            <ta e="T637" id="Seg_11709" s="T636">n</ta>
            <ta e="T638" id="Seg_11710" s="T637">v</ta>
            <ta e="T639" id="Seg_11711" s="T638">n</ta>
            <ta e="T640" id="Seg_11712" s="T639">v</ta>
            <ta e="T641" id="Seg_11713" s="T640">n</ta>
            <ta e="T642" id="Seg_11714" s="T641">n</ta>
            <ta e="T643" id="Seg_11715" s="T642">v</ta>
            <ta e="T644" id="Seg_11716" s="T643">post</ta>
            <ta e="T645" id="Seg_11717" s="T644">propr</ta>
            <ta e="T646" id="Seg_11718" s="T645">emphpro</ta>
            <ta e="T647" id="Seg_11719" s="T646">post</ta>
            <ta e="T648" id="Seg_11720" s="T647">v</ta>
            <ta e="T649" id="Seg_11721" s="T648">adj</ta>
            <ta e="T650" id="Seg_11722" s="T649">n</ta>
            <ta e="T651" id="Seg_11723" s="T650">v</ta>
            <ta e="T652" id="Seg_11724" s="T651">aux</ta>
            <ta e="T653" id="Seg_11725" s="T652">ptcl</ta>
            <ta e="T654" id="Seg_11726" s="T653">que</ta>
            <ta e="T655" id="Seg_11727" s="T654">adj</ta>
            <ta e="T656" id="Seg_11728" s="T655">n</ta>
            <ta e="T657" id="Seg_11729" s="T656">n</ta>
            <ta e="T658" id="Seg_11730" s="T657">v</ta>
            <ta e="T659" id="Seg_11731" s="T658">n</ta>
            <ta e="T660" id="Seg_11732" s="T659">n</ta>
            <ta e="T661" id="Seg_11733" s="T660">n</ta>
            <ta e="T662" id="Seg_11734" s="T661">dempro</ta>
            <ta e="T663" id="Seg_11735" s="T662">n</ta>
            <ta e="T664" id="Seg_11736" s="T663">n</ta>
            <ta e="T665" id="Seg_11737" s="T664">v</ta>
            <ta e="T666" id="Seg_11738" s="T665">adj</ta>
            <ta e="T667" id="Seg_11739" s="T666">adj</ta>
            <ta e="T668" id="Seg_11740" s="T667">cop</ta>
            <ta e="T669" id="Seg_11741" s="T668">adj</ta>
            <ta e="T670" id="Seg_11742" s="T669">n</ta>
            <ta e="T671" id="Seg_11743" s="T670">ptcl</ta>
            <ta e="T672" id="Seg_11744" s="T671">n</ta>
            <ta e="T673" id="Seg_11745" s="T672">n</ta>
            <ta e="T674" id="Seg_11746" s="T673">v</ta>
            <ta e="T676" id="Seg_11747" s="T675">propr</ta>
            <ta e="T677" id="Seg_11748" s="T676">n</ta>
            <ta e="T678" id="Seg_11749" s="T677">n</ta>
            <ta e="T679" id="Seg_11750" s="T678">n</ta>
            <ta e="T680" id="Seg_11751" s="T679">adj</ta>
            <ta e="T681" id="Seg_11752" s="T680">que</ta>
            <ta e="T682" id="Seg_11753" s="T681">ptcl</ta>
            <ta e="T683" id="Seg_11754" s="T682">n</ta>
            <ta e="T684" id="Seg_11755" s="T683">v</ta>
            <ta e="T685" id="Seg_11756" s="T684">n</ta>
            <ta e="T686" id="Seg_11757" s="T685">n</ta>
            <ta e="T687" id="Seg_11758" s="T686">v</ta>
            <ta e="T688" id="Seg_11759" s="T687">n</ta>
            <ta e="T689" id="Seg_11760" s="T688">v</ta>
            <ta e="T690" id="Seg_11761" s="T689">emphpro</ta>
            <ta e="T691" id="Seg_11762" s="T690">n</ta>
            <ta e="T692" id="Seg_11763" s="T691">adj</ta>
            <ta e="T693" id="Seg_11764" s="T692">n</ta>
            <ta e="T694" id="Seg_11765" s="T693">v</ta>
            <ta e="T695" id="Seg_11766" s="T694">adj</ta>
            <ta e="T696" id="Seg_11767" s="T695">n</ta>
            <ta e="T697" id="Seg_11768" s="T696">v</ta>
            <ta e="T698" id="Seg_11769" s="T697">n</ta>
            <ta e="T699" id="Seg_11770" s="T698">v</ta>
            <ta e="T700" id="Seg_11771" s="T699">aux</ta>
            <ta e="T701" id="Seg_11772" s="T700">pers</ta>
            <ta e="T702" id="Seg_11773" s="T701">pers</ta>
            <ta e="T703" id="Seg_11774" s="T702">v</ta>
            <ta e="T704" id="Seg_11775" s="T703">n</ta>
            <ta e="T705" id="Seg_11776" s="T704">adv</ta>
            <ta e="T706" id="Seg_11777" s="T705">v</ta>
            <ta e="T707" id="Seg_11778" s="T706">v</ta>
            <ta e="T708" id="Seg_11779" s="T707">dempro</ta>
            <ta e="T709" id="Seg_11780" s="T708">n</ta>
            <ta e="T710" id="Seg_11781" s="T709">n</ta>
            <ta e="T711" id="Seg_11782" s="T710">ptcl</ta>
            <ta e="T712" id="Seg_11783" s="T711">v</ta>
            <ta e="T713" id="Seg_11784" s="T712">cardnum</ta>
            <ta e="T714" id="Seg_11785" s="T713">n</ta>
            <ta e="T715" id="Seg_11786" s="T714">propr</ta>
            <ta e="T716" id="Seg_11787" s="T715">n</ta>
            <ta e="T717" id="Seg_11788" s="T716">v</ta>
            <ta e="T718" id="Seg_11789" s="T717">n</ta>
            <ta e="T719" id="Seg_11790" s="T718">v</ta>
            <ta e="T720" id="Seg_11791" s="T719">v</ta>
            <ta e="T721" id="Seg_11792" s="T720">dempro</ta>
            <ta e="T722" id="Seg_11793" s="T721">n</ta>
            <ta e="T723" id="Seg_11794" s="T722">n</ta>
            <ta e="T724" id="Seg_11795" s="T723">adv</ta>
            <ta e="T725" id="Seg_11796" s="T724">v</ta>
            <ta e="T726" id="Seg_11797" s="T725">v</ta>
            <ta e="T727" id="Seg_11798" s="T726">adv</ta>
            <ta e="T728" id="Seg_11799" s="T727">adv</ta>
            <ta e="T729" id="Seg_11800" s="T728">adv</ta>
            <ta e="T730" id="Seg_11801" s="T729">n</ta>
            <ta e="T731" id="Seg_11802" s="T730">cop</ta>
            <ta e="T732" id="Seg_11803" s="T731">v</ta>
            <ta e="T733" id="Seg_11804" s="T732">v</ta>
            <ta e="T734" id="Seg_11805" s="T733">propr</ta>
            <ta e="T735" id="Seg_11806" s="T734">propr</ta>
            <ta e="T736" id="Seg_11807" s="T735">n</ta>
            <ta e="T737" id="Seg_11808" s="T736">v</ta>
            <ta e="T738" id="Seg_11809" s="T737">aux</ta>
            <ta e="T739" id="Seg_11810" s="T738">n</ta>
            <ta e="T742" id="Seg_11811" s="T741">v</ta>
            <ta e="T743" id="Seg_11812" s="T742">pers</ta>
            <ta e="T744" id="Seg_11813" s="T743">que</ta>
            <ta e="T745" id="Seg_11814" s="T744">v</ta>
            <ta e="T746" id="Seg_11815" s="T745">adj</ta>
            <ta e="T747" id="Seg_11816" s="T746">adj</ta>
            <ta e="T748" id="Seg_11817" s="T747">n</ta>
            <ta e="T749" id="Seg_11818" s="T748">v</ta>
            <ta e="T750" id="Seg_11819" s="T749">propr</ta>
            <ta e="T751" id="Seg_11820" s="T750">n</ta>
            <ta e="T752" id="Seg_11821" s="T751">post</ta>
            <ta e="T753" id="Seg_11822" s="T752">v</ta>
            <ta e="T754" id="Seg_11823" s="T753">n</ta>
            <ta e="T755" id="Seg_11824" s="T754">v</ta>
            <ta e="T756" id="Seg_11825" s="T755">adv</ta>
            <ta e="T757" id="Seg_11826" s="T756">n</ta>
            <ta e="T758" id="Seg_11827" s="T757">n</ta>
            <ta e="T759" id="Seg_11828" s="T758">n</ta>
            <ta e="T760" id="Seg_11829" s="T759">adj</ta>
            <ta e="T761" id="Seg_11830" s="T760">v</ta>
            <ta e="T762" id="Seg_11831" s="T761">adj</ta>
            <ta e="T763" id="Seg_11832" s="T762">n</ta>
            <ta e="T764" id="Seg_11833" s="T763">n</ta>
            <ta e="T765" id="Seg_11834" s="T764">v</ta>
            <ta e="T766" id="Seg_11835" s="T765">post</ta>
            <ta e="T767" id="Seg_11836" s="T766">n</ta>
            <ta e="T768" id="Seg_11837" s="T767">n</ta>
            <ta e="T769" id="Seg_11838" s="T768">cop</ta>
            <ta e="T770" id="Seg_11839" s="T769">v</ta>
            <ta e="T771" id="Seg_11840" s="T770">n</ta>
            <ta e="T772" id="Seg_11841" s="T771">n</ta>
            <ta e="T773" id="Seg_11842" s="T772">n</ta>
            <ta e="T774" id="Seg_11843" s="T773">n</ta>
            <ta e="T775" id="Seg_11844" s="T774">post</ta>
            <ta e="T776" id="Seg_11845" s="T775">n</ta>
            <ta e="T777" id="Seg_11846" s="T776">ptcl</ta>
            <ta e="T778" id="Seg_11847" s="T777">v</ta>
            <ta e="T779" id="Seg_11848" s="T778">n</ta>
            <ta e="T780" id="Seg_11849" s="T779">adv</ta>
            <ta e="T781" id="Seg_11850" s="T780">v</ta>
            <ta e="T782" id="Seg_11851" s="T781">v</ta>
            <ta e="T783" id="Seg_11852" s="T782">aux</ta>
            <ta e="T784" id="Seg_11853" s="T783">adj</ta>
            <ta e="T785" id="Seg_11854" s="T784">n</ta>
            <ta e="T786" id="Seg_11855" s="T785">v</ta>
            <ta e="T787" id="Seg_11856" s="T786">aux</ta>
            <ta e="T788" id="Seg_11857" s="T787">n</ta>
            <ta e="T789" id="Seg_11858" s="T788">v</ta>
            <ta e="T790" id="Seg_11859" s="T789">v</ta>
            <ta e="T791" id="Seg_11860" s="T790">n</ta>
            <ta e="T792" id="Seg_11861" s="T791">n</ta>
            <ta e="T793" id="Seg_11862" s="T792">adj</ta>
            <ta e="T794" id="Seg_11863" s="T793">v</ta>
            <ta e="T795" id="Seg_11864" s="T794">v</ta>
            <ta e="T796" id="Seg_11865" s="T795">n</ta>
            <ta e="T797" id="Seg_11866" s="T796">adv</ta>
            <ta e="T798" id="Seg_11867" s="T797">n</ta>
            <ta e="T799" id="Seg_11868" s="T798">n</ta>
            <ta e="T800" id="Seg_11869" s="T799">adj</ta>
            <ta e="T801" id="Seg_11870" s="T800">adj</ta>
            <ta e="T802" id="Seg_11871" s="T801">v</ta>
            <ta e="T803" id="Seg_11872" s="T802">v</ta>
            <ta e="T804" id="Seg_11873" s="T803">adj</ta>
            <ta e="T805" id="Seg_11874" s="T804">v</ta>
            <ta e="T806" id="Seg_11875" s="T805">n</ta>
            <ta e="T807" id="Seg_11876" s="T806">v</ta>
            <ta e="T808" id="Seg_11877" s="T807">que</ta>
            <ta e="T809" id="Seg_11878" s="T808">pers</ta>
            <ta e="T810" id="Seg_11879" s="T809">pers</ta>
            <ta e="T811" id="Seg_11880" s="T810">v</ta>
            <ta e="T812" id="Seg_11881" s="T811">propr</ta>
            <ta e="T813" id="Seg_11882" s="T812">v</ta>
            <ta e="T814" id="Seg_11883" s="T813">que</ta>
            <ta e="T815" id="Seg_11884" s="T814">pers</ta>
            <ta e="T818" id="Seg_11885" s="T817">pers</ta>
            <ta e="T819" id="Seg_11886" s="T818">ptcl</ta>
            <ta e="T820" id="Seg_11887" s="T819">v</ta>
            <ta e="T821" id="Seg_11888" s="T820">que</ta>
            <ta e="T822" id="Seg_11889" s="T821">pers</ta>
            <ta e="T823" id="Seg_11890" s="T822">v</ta>
            <ta e="T824" id="Seg_11891" s="T823">n</ta>
            <ta e="T825" id="Seg_11892" s="T824">adv</ta>
            <ta e="T826" id="Seg_11893" s="T825">v</ta>
            <ta e="T827" id="Seg_11894" s="T826">post</ta>
            <ta e="T828" id="Seg_11895" s="T827">n</ta>
            <ta e="T829" id="Seg_11896" s="T828">post</ta>
            <ta e="T830" id="Seg_11897" s="T829">v</ta>
            <ta e="T831" id="Seg_11898" s="T830">pers</ta>
            <ta e="T832" id="Seg_11899" s="T831">que</ta>
            <ta e="T833" id="Seg_11900" s="T832">cop</ta>
            <ta e="T834" id="Seg_11901" s="T833">cardnum</ta>
            <ta e="T835" id="Seg_11902" s="T834">cardnum</ta>
            <ta e="T836" id="Seg_11903" s="T835">n</ta>
            <ta e="T837" id="Seg_11904" s="T836">v</ta>
            <ta e="T838" id="Seg_11905" s="T837">n</ta>
            <ta e="T839" id="Seg_11906" s="T838">n</ta>
            <ta e="T840" id="Seg_11907" s="T839">v</ta>
            <ta e="T841" id="Seg_11908" s="T840">v</ta>
            <ta e="T842" id="Seg_11909" s="T841">v</ta>
            <ta e="T843" id="Seg_11910" s="T842">n</ta>
            <ta e="T844" id="Seg_11911" s="T843">dempro</ta>
            <ta e="T845" id="Seg_11912" s="T844">v</ta>
            <ta e="T846" id="Seg_11913" s="T845">adv</ta>
            <ta e="T847" id="Seg_11914" s="T846">adj</ta>
            <ta e="T848" id="Seg_11915" s="T847">n</ta>
            <ta e="T849" id="Seg_11916" s="T848">v</ta>
            <ta e="T850" id="Seg_11917" s="T849">adv</ta>
            <ta e="T851" id="Seg_11918" s="T850">adj</ta>
            <ta e="T852" id="Seg_11919" s="T851">adj</ta>
            <ta e="T853" id="Seg_11920" s="T852">n</ta>
            <ta e="T854" id="Seg_11921" s="T853">n</ta>
            <ta e="T855" id="Seg_11922" s="T854">n</ta>
            <ta e="T856" id="Seg_11923" s="T855">v</ta>
            <ta e="T857" id="Seg_11924" s="T856">adj</ta>
            <ta e="T858" id="Seg_11925" s="T857">n</ta>
            <ta e="T859" id="Seg_11926" s="T858">cop</ta>
            <ta e="T860" id="Seg_11927" s="T859">v</ta>
            <ta e="T861" id="Seg_11928" s="T860">n</ta>
            <ta e="T862" id="Seg_11929" s="T861">v</ta>
            <ta e="T863" id="Seg_11930" s="T862">dempro</ta>
            <ta e="T864" id="Seg_11931" s="T863">n</ta>
            <ta e="T865" id="Seg_11932" s="T864">n</ta>
            <ta e="T866" id="Seg_11933" s="T865">v</ta>
            <ta e="T867" id="Seg_11934" s="T866">propr</ta>
            <ta e="T868" id="Seg_11935" s="T867">n</ta>
            <ta e="T869" id="Seg_11936" s="T868">n</ta>
            <ta e="T870" id="Seg_11937" s="T869">adv</ta>
            <ta e="T871" id="Seg_11938" s="T870">v</ta>
            <ta e="T872" id="Seg_11939" s="T871">aux</ta>
            <ta e="T873" id="Seg_11940" s="T872">adj</ta>
            <ta e="T874" id="Seg_11941" s="T873">n</ta>
            <ta e="T875" id="Seg_11942" s="T874">v</ta>
            <ta e="T876" id="Seg_11943" s="T875">aux</ta>
            <ta e="T877" id="Seg_11944" s="T876">ptcl</ta>
            <ta e="T878" id="Seg_11945" s="T877">adj</ta>
            <ta e="T879" id="Seg_11946" s="T878">v</ta>
            <ta e="T880" id="Seg_11947" s="T879">pers</ta>
            <ta e="T881" id="Seg_11948" s="T880">n</ta>
            <ta e="T882" id="Seg_11949" s="T881">que</ta>
            <ta e="T883" id="Seg_11950" s="T882">ptcl</ta>
            <ta e="T884" id="Seg_11951" s="T883">v</ta>
            <ta e="T885" id="Seg_11952" s="T884">adj</ta>
            <ta e="T886" id="Seg_11953" s="T885">n</ta>
            <ta e="T887" id="Seg_11954" s="T886">v</ta>
            <ta e="T888" id="Seg_11955" s="T887">adv</ta>
            <ta e="T889" id="Seg_11956" s="T888">v</ta>
            <ta e="T890" id="Seg_11957" s="T889">v</ta>
            <ta e="T891" id="Seg_11958" s="T890">n</ta>
            <ta e="T892" id="Seg_11959" s="T891">v</ta>
            <ta e="T893" id="Seg_11960" s="T892">n</ta>
            <ta e="T894" id="Seg_11961" s="T893">adv</ta>
            <ta e="T895" id="Seg_11962" s="T894">ptcl</ta>
            <ta e="T896" id="Seg_11963" s="T895">v</ta>
            <ta e="T897" id="Seg_11964" s="T896">aux</ta>
            <ta e="T898" id="Seg_11965" s="T897">n</ta>
            <ta e="T899" id="Seg_11966" s="T898">n</ta>
            <ta e="T900" id="Seg_11967" s="T899">adj</ta>
            <ta e="T901" id="Seg_11968" s="T900">n</ta>
            <ta e="T902" id="Seg_11969" s="T901">v</ta>
            <ta e="T903" id="Seg_11970" s="T902">post</ta>
            <ta e="T904" id="Seg_11971" s="T903">n</ta>
            <ta e="T905" id="Seg_11972" s="T904">adj</ta>
            <ta e="T906" id="Seg_11973" s="T905">n</ta>
            <ta e="T907" id="Seg_11974" s="T906">n</ta>
            <ta e="T908" id="Seg_11975" s="T907">n</ta>
            <ta e="T909" id="Seg_11976" s="T908">v</ta>
            <ta e="T910" id="Seg_11977" s="T909">adj</ta>
            <ta e="T911" id="Seg_11978" s="T910">n</ta>
            <ta e="T912" id="Seg_11979" s="T911">v</ta>
            <ta e="T913" id="Seg_11980" s="T912">n</ta>
            <ta e="T914" id="Seg_11981" s="T913">n</ta>
            <ta e="T915" id="Seg_11982" s="T914">pers</ta>
            <ta e="T916" id="Seg_11983" s="T915">v</ta>
            <ta e="T917" id="Seg_11984" s="T916">n</ta>
            <ta e="T918" id="Seg_11985" s="T917">propr</ta>
            <ta e="T919" id="Seg_11986" s="T918">post</ta>
            <ta e="T920" id="Seg_11987" s="T919">n</ta>
            <ta e="T921" id="Seg_11988" s="T920">v</ta>
            <ta e="T922" id="Seg_11989" s="T921">propr</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T1" id="Seg_11990" s="T0">RUS:cult</ta>
            <ta e="T4" id="Seg_11991" s="T3">EV:cult</ta>
            <ta e="T32" id="Seg_11992" s="T31">RUS:cult</ta>
            <ta e="T36" id="Seg_11993" s="T35">RUS:cult</ta>
            <ta e="T47" id="Seg_11994" s="T46">RUS:cult</ta>
            <ta e="T54" id="Seg_11995" s="T53">RUS:cult</ta>
            <ta e="T69" id="Seg_11996" s="T68">RUS:cult</ta>
            <ta e="T113" id="Seg_11997" s="T112">RUS:cult</ta>
            <ta e="T129" id="Seg_11998" s="T128">RUS:cult</ta>
            <ta e="T145" id="Seg_11999" s="T144">RUS:cult</ta>
            <ta e="T173" id="Seg_12000" s="T172">RUS:cult</ta>
            <ta e="T179" id="Seg_12001" s="T178">RUS:cult</ta>
            <ta e="T194" id="Seg_12002" s="T193">RUS:core</ta>
            <ta e="T202" id="Seg_12003" s="T201">RUS:cult</ta>
            <ta e="T203" id="Seg_12004" s="T202">RUS:cult</ta>
            <ta e="T205" id="Seg_12005" s="T204">RUS:cult</ta>
            <ta e="T206" id="Seg_12006" s="T205">RUS:cult</ta>
            <ta e="T227" id="Seg_12007" s="T226">RUS:cult</ta>
            <ta e="T246" id="Seg_12008" s="T245">RUS:cult</ta>
            <ta e="T258" id="Seg_12009" s="T257">RUS:cult</ta>
            <ta e="T275" id="Seg_12010" s="T274">RUS:cult</ta>
            <ta e="T276" id="Seg_12011" s="T275">RUS:cult</ta>
            <ta e="T277" id="Seg_12012" s="T276">RUS:cult</ta>
            <ta e="T278" id="Seg_12013" s="T277">RUS:cult</ta>
            <ta e="T306" id="Seg_12014" s="T305">RUS:mod</ta>
            <ta e="T311" id="Seg_12015" s="T310">RUS:cult</ta>
            <ta e="T320" id="Seg_12016" s="T319">RUS:cult</ta>
            <ta e="T324" id="Seg_12017" s="T323">RUS:cult</ta>
            <ta e="T331" id="Seg_12018" s="T330">RUS:cult</ta>
            <ta e="T334" id="Seg_12019" s="T333">RUS:cult</ta>
            <ta e="T345" id="Seg_12020" s="T344">RUS:cult</ta>
            <ta e="T351" id="Seg_12021" s="T350">RUS:cult</ta>
            <ta e="T385" id="Seg_12022" s="T384">RUS:cult</ta>
            <ta e="T406" id="Seg_12023" s="T405">RUS:cult</ta>
            <ta e="T414" id="Seg_12024" s="T413">RUS:cult</ta>
            <ta e="T416" id="Seg_12025" s="T415">RUS:cult</ta>
            <ta e="T429" id="Seg_12026" s="T428">RUS:cult</ta>
            <ta e="T432" id="Seg_12027" s="T431">RUS:cult</ta>
            <ta e="T438" id="Seg_12028" s="T437">RUS:cult</ta>
            <ta e="T449" id="Seg_12029" s="T448">RUS:cult</ta>
            <ta e="T450" id="Seg_12030" s="T449">RUS:core</ta>
            <ta e="T477" id="Seg_12031" s="T476">RUS:cult</ta>
            <ta e="T490" id="Seg_12032" s="T489">RUS:cult</ta>
            <ta e="T522" id="Seg_12033" s="T521">RUS:cult</ta>
            <ta e="T532" id="Seg_12034" s="T531">RUS:cult</ta>
            <ta e="T533" id="Seg_12035" s="T532">RUS:cult</ta>
            <ta e="T541" id="Seg_12036" s="T540">RUS:cult</ta>
            <ta e="T546" id="Seg_12037" s="T545">RUS:cult</ta>
            <ta e="T547" id="Seg_12038" s="T546">RUS:cult</ta>
            <ta e="T597" id="Seg_12039" s="T596">RUS:cult</ta>
            <ta e="T614" id="Seg_12040" s="T613">RUS:cult</ta>
            <ta e="T645" id="Seg_12041" s="T644">RUS:cult</ta>
            <ta e="T653" id="Seg_12042" s="T652">RUS:mod</ta>
            <ta e="T676" id="Seg_12043" s="T675">RUS:cult</ta>
            <ta e="T688" id="Seg_12044" s="T687">RUS:cult</ta>
            <ta e="T715" id="Seg_12045" s="T714">RUS:cult</ta>
            <ta e="T734" id="Seg_12046" s="T733">RUS:cult</ta>
            <ta e="T735" id="Seg_12047" s="T734">RUS:cult</ta>
            <ta e="T750" id="Seg_12048" s="T749">RUS:cult</ta>
            <ta e="T812" id="Seg_12049" s="T811">RUS:cult</ta>
            <ta e="T838" id="Seg_12050" s="T837">EV:core</ta>
            <ta e="T852" id="Seg_12051" s="T851">EV:cult</ta>
            <ta e="T867" id="Seg_12052" s="T866">RUS:cult</ta>
            <ta e="T918" id="Seg_12053" s="T917">RUS:cult</ta>
            <ta e="T922" id="Seg_12054" s="T921">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T3" id="Seg_12055" s="T0">The trip to Volochanka.</ta>
            <ta e="T10" id="Seg_12056" s="T3">The last days of the month of the botfly (= August) became very warm.</ta>
            <ta e="T13" id="Seg_12057" s="T10">Grasses and leaves turned yellow.</ta>
            <ta e="T19" id="Seg_12058" s="T13">The tundra turned here and there red-gold.</ta>
            <ta e="T28" id="Seg_12059" s="T19">On those days a big gathering took place at the shore of the known big lake:</ta>
            <ta e="T34" id="Seg_12060" s="T28">The children are prepared for being taken to Volochanka for education.</ta>
            <ta e="T42" id="Seg_12061" s="T34">The population of the tundra did not know sending their children for education earlier.</ta>
            <ta e="T48" id="Seg_12062" s="T42">Therefore they don't want to give their children to the school.</ta>
            <ta e="T53" id="Seg_12063" s="T48">They find various reasons for not sending their children.</ta>
            <ta e="T60" id="Seg_12064" s="T53">They made the work very hard for the tribal leader in those days.</ta>
            <ta e="T72" id="Seg_12065" s="T60">He went to every chum and tried to talk, for that they will give their children to the school, for that they will send [them] for [getting] education.</ta>
            <ta e="T76" id="Seg_12066" s="T72">Nothing resulted out of that.</ta>
            <ta e="T91" id="Seg_12067" s="T76">Therefore a decision was taken, saying that a gathering of the whole tundra would take place, for that all [people] would gather and talk face to face.</ta>
            <ta e="T100" id="Seg_12068" s="T91">They gathered in one big chum, where they hardly fit in, the one is standing, the other not.</ta>
            <ta e="T110" id="Seg_12069" s="T100">The men just looked to the floor because they had no power to say anything.</ta>
            <ta e="T116" id="Seg_12070" s="T110">The women wiped off the tears with their kerchiefs.</ta>
            <ta e="T123" id="Seg_12071" s="T116">"Friends (?), why do you lay your heads onto your knees?</ta>
            <ta e="T133" id="Seg_12072" s="T123">Why do you keep quiet?", an old woman named Evgeniya said, she shouted.</ta>
            <ta e="T137" id="Seg_12073" s="T133">She is very pert.</ta>
            <ta e="T140" id="Seg_12074" s="T137">She isn't afraid of anybody.</ta>
            <ta e="T146" id="Seg_12075" s="T140">The old man Porfiriy got up silently from his seat.</ta>
            <ta e="T152" id="Seg_12076" s="T146">He swept over his bald head and looked around without hurrying.</ta>
            <ta e="T154" id="Seg_12077" s="T152">"What shall I say!</ta>
            <ta e="T157" id="Seg_12078" s="T154">It's like that.</ta>
            <ta e="T166" id="Seg_12079" s="T157">It's very good, though, that the children learn in this new era.</ta>
            <ta e="T170" id="Seg_12080" s="T166">They shall be made literate.</ta>
            <ta e="T179" id="Seg_12081" s="T170">I taught my son Ivan from an early age to catch fish and animals (with gin traps).</ta>
            <ta e="T182" id="Seg_12082" s="T179">Now I have become old.</ta>
            <ta e="T186" id="Seg_12083" s="T182">I don't make the work anymore.</ta>
            <ta e="T190" id="Seg_12084" s="T186">Who will feed my family?</ta>
            <ta e="T194" id="Seg_12085" s="T190">Our tundra is cold, with heavy snowstorms.</ta>
            <ta e="T201" id="Seg_12086" s="T194">Only a strong young human does get a haul. </ta>
            <ta e="T209" id="Seg_12087" s="T201">They won't teach Ivan in school to install deadfall traps and gin traps.</ta>
            <ta e="T213" id="Seg_12088" s="T209">He will forget the work of the tundra.</ta>
            <ta e="T221" id="Seg_12089" s="T213">He will only sit at home like a woman, not doing anything."</ta>
            <ta e="T229" id="Seg_12090" s="T221">"You, old man, leave the women alone", again a scream of Evgeniya was heard.</ta>
            <ta e="T233" id="Seg_12091" s="T229">"Who prepares food for you?</ta>
            <ta e="T239" id="Seg_12092" s="T233">[Who] keeps your house warm, patches your shoes and clothes?"</ta>
            <ta e="T248" id="Seg_12093" s="T239">That's right, it's like that", Porfiriy said not being very afraid.</ta>
            <ta e="T257" id="Seg_12094" s="T248">"My daughter Kycha knows how to cook, she knows how to make shoes and clothes.</ta>
            <ta e="T262" id="Seg_12095" s="T257">In school she will nevertheless forget.</ta>
            <ta e="T269" id="Seg_12096" s="T262">Like a man she will be haughty and go around the houses.</ta>
            <ta e="T274" id="Seg_12097" s="T269">How will I live, if my children go away?"</ta>
            <ta e="T282" id="Seg_12098" s="T274">The tribal leader Anisim Nazarovich Popov burst into laughter and stopped the old man.</ta>
            <ta e="T287" id="Seg_12099" s="T282">"Well, old man, you are tired apparently.</ta>
            <ta e="T289" id="Seg_12100" s="T287">Sit down.</ta>
            <ta e="T292" id="Seg_12101" s="T289">The other people shall speak."</ta>
            <ta e="T304" id="Seg_12102" s="T292">The old man sat down, not saying anything, making a silent splash, having grabbed his knees.</ta>
            <ta e="T311" id="Seg_12103" s="T304">"Who else has something to say?", the tribal leader asked.</ta>
            <ta e="T321" id="Seg_12104" s="T311">He waited in vain that somebody would say something, and he counted which children would go to school.</ta>
            <ta e="T327" id="Seg_12105" s="T321">Who stays in school, who does not go, those of higher age.</ta>
            <ta e="T334" id="Seg_12106" s="T327">Ivan, the son of the old man Porfiriy, belonged to these children.</ta>
            <ta e="T338" id="Seg_12107" s="T334">"Kycha should also be left [here].</ta>
            <ta e="T346" id="Seg_12108" s="T338">She is already old, though", the old man Porfiriy begged. </ta>
            <ta e="T351" id="Seg_12109" s="T346">"Nein, Kycha won't go to school."</ta>
            <ta e="T360" id="Seg_12110" s="T351">"Old man, don't be afraid, she won't dress trousers of men when she got education.</ta>
            <ta e="T365" id="Seg_12111" s="T360">She won't go around the houses haughtily."</ta>
            <ta e="T371" id="Seg_12112" s="T365">The people sitting at the gathering laughed heavily.</ta>
            <ta e="T377" id="Seg_12113" s="T371">"Prepare the children, who will go, well.</ta>
            <ta e="T381" id="Seg_12114" s="T377">Dress them, like one dresses [them] on a holiday.</ta>
            <ta e="T390" id="Seg_12115" s="T381">I myself will bring the children from Volochanka", the leader said when closing the gathering.</ta>
            <ta e="T395" id="Seg_12116" s="T390">After the gathering had passed by, the noise of the gathered [people] became more.</ta>
            <ta e="T403" id="Seg_12117" s="T395">The women took out of their bags the broad clothes of their children.</ta>
            <ta e="T406" id="Seg_12118" s="T403">Their holiday clothes.</ta>
            <ta e="T414" id="Seg_12119" s="T406">Some people apparently sew on small icons and crosses at unvisible spots.</ta>
            <ta e="T424" id="Seg_12120" s="T414">They (honour?) the children with the tastiest food: with minced meat with fat, with dried fish, with reindeer tongue.</ta>
            <ta e="T433" id="Seg_12121" s="T424">In the evening the old man Semyon visited the tribal leader.</ta>
            <ta e="T437" id="Seg_12122" s="T433">He didn't say directly why he came.</ta>
            <ta e="T442" id="Seg_12123" s="T437">Only when drinking tea he said:</ta>
            <ta e="T447" id="Seg_12124" s="T442">"Long ago our grandfathers and fathers were believers.</ta>
            <ta e="T453" id="Seg_12125" s="T447">They used to always make the big devil a present.</ta>
            <ta e="T458" id="Seg_12126" s="T453">The whole family wants to live well.</ta>
            <ta e="T462" id="Seg_12127" s="T458">Not being short of anything, not being sick.</ta>
            <ta e="T470" id="Seg_12128" s="T462">The knowledge of theshamans about the way being gone by the people was very big (?).</ta>
            <ta e="T477" id="Seg_12129" s="T470">Not far away from our place of living there was a big devil stone.</ta>
            <ta e="T482" id="Seg_12130" s="T477">All people use to give presents there.</ta>
            <ta e="T489" id="Seg_12131" s="T482">Soon our children will go to a foreign place, to foreign people."</ta>
            <ta e="T496" id="Seg_12132" s="T489">Listening to the story of his guest the tribal leader thinks:</ta>
            <ta e="T501" id="Seg_12133" s="T496">"With which tongue in cheek he takes out such a story?"</ta>
            <ta e="T505" id="Seg_12134" s="T501">His continued talking without hurrying.</ta>
            <ta e="T510" id="Seg_12135" s="T505">"The poor women are grieved, they are crying.</ta>
            <ta e="T517" id="Seg_12136" s="T510">It is very sinful to take away the small children out of the tent.</ta>
            <ta e="T520" id="Seg_12137" s="T517">The people are very sad.</ta>
            <ta e="T524" id="Seg_12138" s="T520">Probably, one really has to give the devil a present."</ta>
            <ta e="T531" id="Seg_12139" s="T524">"Well, old man, don't spit at me, [but] we won't do that."</ta>
            <ta e="T534" id="Seg_12140" s="T531">The school does not beg the devil.</ta>
            <ta e="T541" id="Seg_12141" s="T534">Tell it to all people like that, they shall not be sad", the tribal leader said.</ta>
            <ta e="T548" id="Seg_12142" s="T541">The old man poured tea into his cup without saying anything.</ta>
            <ta e="T556" id="Seg_12143" s="T548">"We, the old men, give advise how we think it would be good.</ta>
            <ta e="T562" id="Seg_12144" s="T556">It's right, sometimes we do say something wrong.</ta>
            <ta e="T565" id="Seg_12145" s="T562">The women's crying is heavy.</ta>
            <ta e="T573" id="Seg_12146" s="T565">It is very serious to care about the people's education.</ta>
            <ta e="T582" id="Seg_12147" s="T573">The big people have to be allocated to the very small children, for that they always see [them].</ta>
            <ta e="T590" id="Seg_12148" s="T582">A very small child is like a reindeer calf, it does not know where it goes.</ta>
            <ta e="T593" id="Seg_12149" s="T590">It cannot dress well."</ta>
            <ta e="T597" id="Seg_12150" s="T593">"That's right", the tribal leader said.</ta>
            <ta e="T603" id="Seg_12151" s="T597">The old man had forgotten [it] in the noise of the gathering.</ta>
            <ta e="T612" id="Seg_12152" s="T603">"In winter, when the new year comes, the children will come home to relax.</ta>
            <ta e="T623" id="Seg_12153" s="T612">In spring, in the month of May, when one year's education is finished, they will be at home until autumn."</ta>
            <ta e="T629" id="Seg_12154" s="T623">"This news I will tell to all people.</ta>
            <ta e="T640" id="Seg_12155" s="T629">The people think that the children are taken away not letting them go back", the old man said being happy.</ta>
            <ta e="T648" id="Seg_12156" s="T640">After the old man has come home, Anisim talks to himself:</ta>
            <ta e="T661" id="Seg_12157" s="T648">"One has to explain to all people how the new system brings good to the people of the tundra."</ta>
            <ta e="T668" id="Seg_12158" s="T661">When such old men spread the news, then it will be true and strong (?).</ta>
            <ta e="T678" id="Seg_12159" s="T668">On the next day the people gathered again in the tent of the old Semyon.</ta>
            <ta e="T684" id="Seg_12160" s="T678">The people's mood has become better than yesterday.</ta>
            <ta e="T689" id="Seg_12161" s="T684">When noise and laughter had stopped, the tribal leader said:</ta>
            <ta e="T696" id="Seg_12162" s="T689">"According to your wish, make the small children keep themselves close to the big children.</ta>
            <ta e="T701" id="Seg_12163" s="T696">For that they look after them during the trip."</ta>
            <ta e="T707" id="Seg_12164" s="T701">"We were told that our children will come in winter to relax.</ta>
            <ta e="T714" id="Seg_12165" s="T707">Is that right?", one woman asked.</ta>
            <ta e="T720" id="Seg_12166" s="T714">The old Semyon was thinking about it (?) on his seat. </ta>
            <ta e="T723" id="Seg_12167" s="T720">"That's true news.</ta>
            <ta e="T734" id="Seg_12168" s="T723">In winter they will come to relax, and in summer they will come and be at home", Anisim said.</ta>
            <ta e="T742" id="Seg_12169" s="T734">The old Semyon relaxed and said:</ta>
            <ta e="T745" id="Seg_12170" s="T742">"What did I say?"</ta>
            <ta e="T753" id="Seg_12171" s="T745">All people, looking favourably (lit. with warm eyes), turned to old Semyon.</ta>
            <ta e="T756" id="Seg_12172" s="T753">The gathering went by quickly.</ta>
            <ta e="T761" id="Seg_12173" s="T756">The people all went to their pole tents.</ta>
            <ta e="T770" id="Seg_12174" s="T761">In the next autumn, having collected the herd, the catching of reindeer for the trip started.</ta>
            <ta e="T778" id="Seg_12175" s="T770">The women beneath the tents have no rest with the children, having gathered.</ta>
            <ta e="T787" id="Seg_12176" s="T778">They constantly talk to the children, for that they don't make noise, for that they listen to the big children.</ta>
            <ta e="T796" id="Seg_12177" s="T787">After the reindeer were caught and harnessed, [the people of] every tent are eating before the trip.</ta>
            <ta e="T807" id="Seg_12178" s="T796">Then all people flash out of the tents to accompany the children who are going far away.</ta>
            <ta e="T813" id="Seg_12179" s="T807">"When will I see you?", Evgeniya cried.</ta>
            <ta e="T820" id="Seg_12180" s="T813">"How will I live without you?</ta>
            <ta e="T823" id="Seg_12181" s="T820">Who will help us?"</ta>
            <ta e="T830" id="Seg_12182" s="T823">Having grabbed her child strongly, she pulls it towards her tent.</ta>
            <ta e="T838" id="Seg_12183" s="T830">"What's up with you?", one woman said to her friend.</ta>
            <ta e="T843" id="Seg_12184" s="T838">"Don't bother the child before the trip.</ta>
            <ta e="T847" id="Seg_12185" s="T843">It is very bad to accompany it like that.</ta>
            <ta e="T850" id="Seg_12186" s="T847">What a sin, accompany it like a human."</ta>
            <ta e="T859" id="Seg_12187" s="T850">All children having a riding reindeer followed the old people and became a long reindeer caravan.</ta>
            <ta e="T866" id="Seg_12188" s="T859">Those, who are standing or not and accompanying the children, stretched out behind the caravan (?).</ta>
            <ta e="T876" id="Seg_12189" s="T866">The old man Porfiriy, wide-eyed, blessed [them] with all his power.</ta>
            <ta e="T884" id="Seg_12190" s="T876">"Well, good luck, may nothing disturb [you] on your way. </ta>
            <ta e="T887" id="Seg_12191" s="T884">Learn a lot.</ta>
            <ta e="T891" id="Seg_12192" s="T887">Come back quickly to your homeland!"</ta>
            <ta e="T906" id="Seg_12193" s="T891">The people, who stayed back, were looking a long time at the tundra, until the long caravan disappeared behind a hill.</ta>
            <ta e="T914" id="Seg_12194" s="T906">For the children a new time began: the time of learn to write.</ta>
            <ta e="T920" id="Seg_12195" s="T914">You heard the story "The trip to Volochanka".</ta>
            <ta e="T922" id="Seg_12196" s="T920">Popov was reading.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T3" id="Seg_12197" s="T0">Die Fahrt nach Volochanka.</ta>
            <ta e="T10" id="Seg_12198" s="T3">Die letzten Tage des Rentierbremsenmonats (=August) wurden sehr warm.</ta>
            <ta e="T13" id="Seg_12199" s="T10">Gräser und Blätter wurden gelb.</ta>
            <ta e="T19" id="Seg_12200" s="T13">Die Tundra wurde hier und dort rot-golden.</ta>
            <ta e="T28" id="Seg_12201" s="T19">An diesen Tagen fand am Ufer des bekannten großen Sees eine große Versammlung statt:</ta>
            <ta e="T34" id="Seg_12202" s="T28">Die Kinder werden vorbereitet, damit sie nach Volochanka zum Unterricht gebracht werden.</ta>
            <ta e="T42" id="Seg_12203" s="T34">Früher kannte es die Bevölkerung der Tundra nicht, ihre Kinder zum Unterricht zu bringen.</ta>
            <ta e="T48" id="Seg_12204" s="T42">Deshalb wollen sie ihre Kinder nicht in die Schule geben.</ta>
            <ta e="T53" id="Seg_12205" s="T48">Sie finden verschiedene Dinge, um ihre Kinder nicht zu schicken.</ta>
            <ta e="T60" id="Seg_12206" s="T53">Sie machten dem Stammesältesten an diesen Tage sehr quälende Arbeit.</ta>
            <ta e="T72" id="Seg_12207" s="T60">Er ging zu jedem Tschum, versuchte sich zu unterhalten, damit sie ihre Kinder in die Schule gäben, damit sie sie zum Unterricht schickten.</ta>
            <ta e="T76" id="Seg_12208" s="T72">Dabei ist nichts herausgekommen.</ta>
            <ta e="T91" id="Seg_12209" s="T76">Deshalb wurde ein Beschluss gefasst, der besagte, dass eine Versammlung der ganzen Tundra abgehalten wird, damit alle sich versammelten und sich von Angesicht zu Angesicht unterhalten würde.</ta>
            <ta e="T100" id="Seg_12210" s="T91">In einem großen Tschum, in das sie kaum hineinpassten, versammelten sie sich, der eine stand, der andere nicht.</ta>
            <ta e="T110" id="Seg_12211" s="T100">Die Männer sahen nur auf den Boden, weil sie keine Kraft hatten etwas zu sagen.</ta>
            <ta e="T116" id="Seg_12212" s="T110">Die Frauen wischen sich mit ihren Tüchern die Tränen ab.</ta>
            <ta e="T123" id="Seg_12213" s="T116">"Freunde (?), warum legt ihr eure Knöpfe in die Knie?</ta>
            <ta e="T133" id="Seg_12214" s="T123">Warum schweigt ihr?", sagte eine alte Frau namens Jevgenija, schrie sie.</ta>
            <ta e="T137" id="Seg_12215" s="T133">Sie ist sehr keck.</ta>
            <ta e="T140" id="Seg_12216" s="T137">Sie hat vor niemandem Angst.</ta>
            <ta e="T146" id="Seg_12217" s="T140">Von seinem Sitzplatz erhob sich leise der alte Parfirij.</ta>
            <ta e="T152" id="Seg_12218" s="T146">Er strich sich über seinen kahlen Kopf und sah sich ohne Eile um.</ta>
            <ta e="T154" id="Seg_12219" s="T152">"Was soll ich sagen!</ta>
            <ta e="T157" id="Seg_12220" s="T154">Das ist so.</ta>
            <ta e="T166" id="Seg_12221" s="T157">Dass die Kinder in dieser neuen Zeit lernen, ist doch sehr zu begrüßen.</ta>
            <ta e="T170" id="Seg_12222" s="T166">Sie sollen alphabetisiert werden.</ta>
            <ta e="T179" id="Seg_12223" s="T170">Meinem Sohn Ivan habe ich von kleinauf beigebracht, Fisch und Tiere (mit Fangeisen) zu fangen.</ta>
            <ta e="T182" id="Seg_12224" s="T179">Ich bin jetzt alt geworden.</ta>
            <ta e="T186" id="Seg_12225" s="T182">Ich schaffe die Arbeit nicht mehr.</ta>
            <ta e="T190" id="Seg_12226" s="T186">Wer wird meine Familie ernähren?</ta>
            <ta e="T194" id="Seg_12227" s="T190">Unsere Tundra ist kalt, mit heftigen Schneestürmen.</ta>
            <ta e="T201" id="Seg_12228" s="T194">Nur ein kräftiger junger Mensch findet Beute.</ta>
            <ta e="T209" id="Seg_12229" s="T201">Sie werden Ivan in der Schule nicht beibringen, Totfallen und Fangeisen aufzustellen.</ta>
            <ta e="T213" id="Seg_12230" s="T209">Er wird die Arbeit der Tundra vergessen.</ta>
            <ta e="T221" id="Seg_12231" s="T213">Er wird wie eine Frau nur zuhause sitzen, ohne etwas zu machen."</ta>
            <ta e="T229" id="Seg_12232" s="T221">"Du, alter Mann, lass die Frauen in Ruhe", war wieder ein Schrei von Jevgenija zu hören.</ta>
            <ta e="T233" id="Seg_12233" s="T229">"Wer macht dir Essen?</ta>
            <ta e="T239" id="Seg_12234" s="T233">[Wer] hält dein Haus warm, flickt deine Schuhe und deine Kleidung?"</ta>
            <ta e="T248" id="Seg_12235" s="T239">"Das stimmt, so ist es", sagte Porfirij, ohne sich sehr zu ängstigen.</ta>
            <ta e="T257" id="Seg_12236" s="T248">"Meine Tochter Kɨːča kann kochen, sie kann Schuhe und Kleidung machen.</ta>
            <ta e="T262" id="Seg_12237" s="T257">In der Schule aber wird sie das dennoch vergessen.</ta>
            <ta e="T269" id="Seg_12238" s="T262">Wie ein Mann wird sie hochnäsig sein und um die Häuser ziehen.</ta>
            <ta e="T274" id="Seg_12239" s="T269">Wie werde ich leben, wenn meine Kinder gehen?"</ta>
            <ta e="T282" id="Seg_12240" s="T274">Der Stammesälteste Anisim Nazarowitsch Popov lachte auf und stoppte den alten Mann.</ta>
            <ta e="T287" id="Seg_12241" s="T282">"Nun, alter Mann, du bist offenbar müde.</ta>
            <ta e="T289" id="Seg_12242" s="T287">Setz dich.</ta>
            <ta e="T292" id="Seg_12243" s="T289">Die anderen Leute sollen sprechen."</ta>
            <ta e="T304" id="Seg_12244" s="T292">Der alte Mann setzte sich, ohne etwas zu sagen, ganz leise mit einem Platschen, nachdem er sich an die Knie gefasst hatten.</ta>
            <ta e="T311" id="Seg_12245" s="T304">"Wer hat noch etwas zu sagen?", fragte der Stammesälteste.</ta>
            <ta e="T321" id="Seg_12246" s="T311">Es wartete vergeblich, dass jemand etwas sagen würde, und zählte, welche Kinder in die Schule gehen würden.</ta>
            <ta e="T327" id="Seg_12247" s="T321">Wer in der Schule bleibt, wer nicht geht, die mit höherem Alter.</ta>
            <ta e="T334" id="Seg_12248" s="T327">Zu diesen Kinder gehörte Ivan, der Sohn des alten Porfirij.</ta>
            <ta e="T338" id="Seg_12249" s="T334">"Kɨːča soll auch [hier] gelassen werden.</ta>
            <ta e="T346" id="Seg_12250" s="T338">Sie ist doch schon alt", bat der alte Porfirij.</ta>
            <ta e="T351" id="Seg_12251" s="T346">"Nein, Kɨːča wird nicht in die Schule gehen."</ta>
            <ta e="T360" id="Seg_12252" s="T351">"Alter Mann, hab keine Angst, wenn sie Ausbildung hat, wird sie keine Männerhosen anziehen.</ta>
            <ta e="T365" id="Seg_12253" s="T360">Sie wird nicht hochnäsig um die Häuser ziehen."</ta>
            <ta e="T371" id="Seg_12254" s="T365">Die Leute auf der Versammlung lachten aus vollem Herzen.</ta>
            <ta e="T377" id="Seg_12255" s="T371">"Bereitet die Kinder, die fahren, gut vor.</ta>
            <ta e="T381" id="Seg_12256" s="T377">Zieht sie an, wie man [sie] am Feiertag anzieht.</ta>
            <ta e="T390" id="Seg_12257" s="T381">Ich selbst bringe die Kinder aus Volochanka", sagte der Anführer, als er die Versammlung beendete.</ta>
            <ta e="T395" id="Seg_12258" s="T390">Nachdem die Versammlung vorbei war, wurde der Lärm der Versammelten mehr.</ta>
            <ta e="T403" id="Seg_12259" s="T395">Die Frauen nehmen aus ihren Taschen die breite, weite Kleidung ihrer Kinder heraus.</ta>
            <ta e="T406" id="Seg_12260" s="T403">Ihre Feiertagshemden.</ta>
            <ta e="T414" id="Seg_12261" s="T406">Einige nähen wohl an unsichtbaren Stellen kleine Ikonen und Kreuze an.</ta>
            <ta e="T424" id="Seg_12262" s="T414">Die Kinder (ehren?) sie mit dem leckersten Essen: mit gehacktem Fleisch mit Fett, mit getrocknetem Fisch, mit Rentierzunge.</ta>
            <ta e="T433" id="Seg_12263" s="T424">Gegen Abend besuchte der alte Semjon den Stammesältesten.</ta>
            <ta e="T437" id="Seg_12264" s="T433">Warum er kam, sagt er nicht direkt.</ta>
            <ta e="T442" id="Seg_12265" s="T437">Erst beim Teetrinken sagte er:</ta>
            <ta e="T447" id="Seg_12266" s="T442">"Früher waren unsere Großväter und Väter gläubig.</ta>
            <ta e="T453" id="Seg_12267" s="T447">Sie hatten die Gewohnheit, den großen Teufel immer zu beschenken.</ta>
            <ta e="T458" id="Seg_12268" s="T453">Die ganze Familie möchte gut leben.</ta>
            <ta e="T462" id="Seg_12269" s="T458">Ohne an etwas Mangel zu leiden, ohne krank zu sein.</ta>
            <ta e="T470" id="Seg_12270" s="T462">Das Wissen der Schamanen über den Weg, den die Menschen gehen, war sehr groß (?).</ta>
            <ta e="T477" id="Seg_12271" s="T470">Nicht weit von unserem Wohnort gibt es einen großen Teufelsstein.</ta>
            <ta e="T482" id="Seg_12272" s="T477">Alle Leute geben dort Geschenke.</ta>
            <ta e="T489" id="Seg_12273" s="T482">Bald gehen unsere Kinder an einen fremden Ort, zu fremden Menschen."</ta>
            <ta e="T496" id="Seg_12274" s="T489">Der Erzählung seines Gastes zuhörend denkt der Stammesälteste für sich:</ta>
            <ta e="T501" id="Seg_12275" s="T496">"Mit welchem Gedanken holt er so eine Geschichte hervor?"</ta>
            <ta e="T505" id="Seg_12276" s="T501">Sein Gast sprach weiter, ohne sich zu beeilen.</ta>
            <ta e="T510" id="Seg_12277" s="T505">"Die armen Frauen sind betrübt, sie weinen.</ta>
            <ta e="T517" id="Seg_12278" s="T510">Die kleinen Kinder aus dem Zelt wegzunehmen, ist sehr sündhaft.</ta>
            <ta e="T520" id="Seg_12279" s="T517">Die Leute sind sehr traurig.</ta>
            <ta e="T524" id="Seg_12280" s="T520">Man muss dem Teufel wohl ein Geschenk darbieten."</ta>
            <ta e="T531" id="Seg_12281" s="T524">"Nun, alter Mann, spuck nicht auf mich, [aber] das werden wir nicht tun.</ta>
            <ta e="T534" id="Seg_12282" s="T531">Die Schule bittet den Teufel nicht.</ta>
            <ta e="T541" id="Seg_12283" s="T534">Sag das so allen Leuten, sie sollen nicht traurig sein", sagte der Stammesälteste.</ta>
            <ta e="T548" id="Seg_12284" s="T541">Der alte Mann goss sich, ohne etwas zu sagen, Tee in seine Tasse.</ta>
            <ta e="T556" id="Seg_12285" s="T548">"Wir, die alten Männer, geben einen Rat, wie wir denken, dass es gut wäre.</ta>
            <ta e="T562" id="Seg_12286" s="T556">Es stimmt, manchmal sagen wir auch etwas Falsches.</ta>
            <ta e="T565" id="Seg_12287" s="T562">Das Weinen der Frauen ist kräftig.</ta>
            <ta e="T573" id="Seg_12288" s="T565">Sich darum zu kümmern, Menschen auszubilden, ist sehr ernst.</ta>
            <ta e="T582" id="Seg_12289" s="T573">Den sehr kleinen Kindern müssen die großen Leute zugeteilt werden, damit sie [sie] immer sehen.</ta>
            <ta e="T590" id="Seg_12290" s="T582">Ein ganz kleines Kind ist wie ist Rentierkalb, es weiß nicht, wo es hingeht.</ta>
            <ta e="T593" id="Seg_12291" s="T590">Es kann sich nicht gut anziehen."</ta>
            <ta e="T597" id="Seg_12292" s="T593">"Das stimmt", sagte der Stammesälteste.</ta>
            <ta e="T603" id="Seg_12293" s="T597">Der alte Mann hatte [es] beim Lärm der Versammlung vergessen.</ta>
            <ta e="T612" id="Seg_12294" s="T603">"Im Winter, wenn das neue Jahr kommt, werden die Kinder nach Hause kommen, um sich auszuruhen.</ta>
            <ta e="T623" id="Seg_12295" s="T612">Im Frühling, im Mai, wenn ein Ausbildungsjahr zuende ist, werden sie bis zum Herbst zuhause sein."</ta>
            <ta e="T629" id="Seg_12296" s="T623">"Diese Neuigkeit werde ich allen Leuten erzählen.</ta>
            <ta e="T640" id="Seg_12297" s="T629">Die Leute denken, dass die Kinder weggenommen werden, ohne sie zurückkehren zu lassen", sagte der alte Mann und freute sich.</ta>
            <ta e="T648" id="Seg_12298" s="T640">Nachdem der alte Mann nach Hause gekommen ist, spricht Anisim mit sich selbst:</ta>
            <ta e="T661" id="Seg_12299" s="T648">"Man muss allen Leuten erklären, wie die neue Ordnung den Leuten der Tundra Gutes bringt."</ta>
            <ta e="T668" id="Seg_12300" s="T661">Wenn solche alten Männer die Nachricht verbreiten, dann wird es wahr und stark sein (?).</ta>
            <ta e="T678" id="Seg_12301" s="T668">Am nächsten Tag sammelten sich die Leute wieder im Zelt des alten Semjon.</ta>
            <ta e="T684" id="Seg_12302" s="T678">Die Stimmung der Leute ist besser als gestern geworden.</ta>
            <ta e="T689" id="Seg_12303" s="T684">Als Lärm und Gelächter aufhörten, sagte der Stammesälteste:</ta>
            <ta e="T696" id="Seg_12304" s="T689">"Gemäß eurem Wunsch lasst die kleinen Kinder sich an die großen Kinder halten.</ta>
            <ta e="T701" id="Seg_12305" s="T696">Damit sie auf dem Weg nach ihnen sehen."</ta>
            <ta e="T707" id="Seg_12306" s="T701">"Uns wurde gesagt, dass unsere Kinder im Winter zum Ausruhen kommen.</ta>
            <ta e="T714" id="Seg_12307" s="T707">Stimmt das?", fragte eine Frau.</ta>
            <ta e="T720" id="Seg_12308" s="T714">Der alte Semjon überlegte (?) auf dem Platz, wo er saß.</ta>
            <ta e="T723" id="Seg_12309" s="T720">"Das ist eine wahre Nachricht.</ta>
            <ta e="T734" id="Seg_12310" s="T723">Im Winter werden sie zum Ausruhen kommen, und im Sommer werden sie kommen und zuhause sein", sagte Anisim.</ta>
            <ta e="T742" id="Seg_12311" s="T734">Der alte Semjon entspannte sich und sagte:</ta>
            <ta e="T745" id="Seg_12312" s="T742">"Was habe ich gesagt?"</ta>
            <ta e="T753" id="Seg_12313" s="T745">Alle schauten wohlwollend (wörtl. mit warmen Augen) und drehten sich zum alten Semjon.</ta>
            <ta e="T756" id="Seg_12314" s="T753">Die Versammlung ging schnell vorbei.</ta>
            <ta e="T761" id="Seg_12315" s="T756">Die Leute gingen alle zu ihren Stangenzelten.</ta>
            <ta e="T770" id="Seg_12316" s="T761">Im nächsten Herbst, nachdem die Herde eingesammelt wurde (?), begann man, Rentiere für den Zug zu fangen.</ta>
            <ta e="T778" id="Seg_12317" s="T770">Die Frauen neben den Zelten mit den Kindern haben keine Ruhe, als sie sich versammelt haben.</ta>
            <ta e="T787" id="Seg_12318" s="T778">Sie sprechen immerzu mit den Kindern, damit sie keine Unruhe veranstalten, damit sie auf die großen Kinder hören.</ta>
            <ta e="T796" id="Seg_12319" s="T787">Nachdem die Rentiere gefangen und angespannt wurden, isst jedes Zelt vor dem Zug.</ta>
            <ta e="T807" id="Seg_12320" s="T796">Danach schossen alle aus den Zelten, um die Kinder, die sich auf die weite Reise machten, zu begleiten.</ta>
            <ta e="T813" id="Seg_12321" s="T807">"Wann werde ich dich [wieder]sehen?", weinte Jevgenija.</ta>
            <ta e="T820" id="Seg_12322" s="T813">"Wie werde ich ohne dich leben?</ta>
            <ta e="T823" id="Seg_12323" s="T820">Wer wird uns helfen?"</ta>
            <ta e="T830" id="Seg_12324" s="T823">Nachdem sie ihr Kind fest gegriffen hat, zieht es zu ihrem Zelt.</ta>
            <ta e="T838" id="Seg_12325" s="T830">"Was ist mit dir?", sagte eine Frau zu ihrer Freundin.</ta>
            <ta e="T843" id="Seg_12326" s="T838">"Beunruhige das Kind nicht vor der Reise.</ta>
            <ta e="T847" id="Seg_12327" s="T843">Es so zu begleiten ist sehr schlecht.</ta>
            <ta e="T850" id="Seg_12328" s="T847">Sünde, begleite es wie ein Mensch."</ta>
            <ta e="T859" id="Seg_12329" s="T850">Alle Kinder, die ein Reitrentier hatten, folgten den alten Leuten und wurden zu einer langen Rentierkarawane.</ta>
            <ta e="T866" id="Seg_12330" s="T859">Die, die stehen oder nicht und die Kinder begleiten, breiteten sich hinter der Karawane aus (?). </ta>
            <ta e="T876" id="Seg_12331" s="T866">Der alte Porfiriy, die Augen weit aufgerissen, segnete [sie] mit aller Kraft.</ta>
            <ta e="T884" id="Seg_12332" s="T876">"Nun, viel Glück, dass nichts auf eurem Weg stört.</ta>
            <ta e="T887" id="Seg_12333" s="T884">Lernt viel.</ta>
            <ta e="T891" id="Seg_12334" s="T887">Kommt schnell zurück in eure Heimat!"</ta>
            <ta e="T906" id="Seg_12335" s="T891">Die zurückgebliebenen Leute schauten lange über die Tundra, bis die lange Karawane hinter einer Anhöhe verschwunden war.</ta>
            <ta e="T914" id="Seg_12336" s="T906">Für die Kinder begann eine neue Zeit: die Zeit, schreiben zu lernen.</ta>
            <ta e="T920" id="Seg_12337" s="T914">Ihr hörtet die Geschichte "Die Fahrt nach Volochanka".</ta>
            <ta e="T922" id="Seg_12338" s="T920">Es las Popov.</ta>
         </annotation>
         <annotation name="fr" tierref="fr" />
         <annotation name="ltr" tierref="ltr">
            <ta e="T3" id="Seg_12339" s="T0">Аргиш в сторону Волочанки.</ta>
            <ta e="T10" id="Seg_12340" s="T3">Августа месяца завершающие дни стояли жаркими очень.</ta>
            <ta e="T13" id="Seg_12341" s="T10">Растения, листья пожелтели.</ta>
            <ta e="T19" id="Seg_12342" s="T13">Тундра то там, то тут красным золотом становилась.</ta>
            <ta e="T28" id="Seg_12343" s="T19">В те дни известного большого озера на берегу большой сбор шёл:</ta>
            <ta e="T34" id="Seg_12344" s="T28">детвору готовят, в Волочанку чтобы увезти на учёбу. </ta>
            <ta e="T42" id="Seg_12345" s="T34">Тундровики (тундровые люди) раньше не знали детей как в школу отправлять.</ta>
            <ta e="T48" id="Seg_12346" s="T42">Поэтому желание не идёт в школу отдавать.</ta>
            <ta e="T53" id="Seg_12347" s="T48">Всякую причину находят, чтобы не отправлять детей своих.</ta>
            <ta e="T60" id="Seg_12348" s="T53">Для родового (главы рода) в те дни очень мучительную работу провели.</ta>
            <ta e="T72" id="Seg_12349" s="T60">По всем чумам пройдя, пытался поговорить, чтобы детей в школу отдали, на учёбу отправили.</ta>
            <ta e="T76" id="Seg_12350" s="T72">Из этого ничего не вышло.</ta>
            <ta e="T91" id="Seg_12351" s="T76">Поэтому было принято решение -- провести собрание по всей тундре, все чтобы собрались лицо к лицу (с глазу на глаз) чтобы разговор состоялся.</ta>
            <ta e="T100" id="Seg_12352" s="T91">В одном большом чуме, еле поместившись, собрались, кто стоя, кто нет.</ta>
            <ta e="T110" id="Seg_12353" s="T100">Мужчины, от того, что ничего им сказат, в пол только глазами уставились. </ta>
            <ta e="T116" id="Seg_12354" s="T110">Женщины платками с глаз слёзы смахивают.</ta>
            <ta e="T123" id="Seg_12355" s="T116">"Друзья, от чего головы в колени опустили? </ta>
            <ta e="T133" id="Seg_12356" s="T123">Что пригорюнились (молчаливыми стали)?"-- промолвила по имени Евгения говорливая бабулька, прокричала.</ta>
            <ta e="T137" id="Seg_12357" s="T133">Она-то прыткая(бесцеременная) очень. </ta>
            <ta e="T140" id="Seg_12358" s="T137">Никого не стесняется.</ta>
            <ta e="T146" id="Seg_12359" s="T140">С места, где сидел, тихонько встал Парфирий старик.</ta>
            <ta e="T152" id="Seg_12360" s="T146">Голову лысую свою поглаживая, не спеша удивился.</ta>
            <ta e="T154" id="Seg_12361" s="T152">"Что сказать! </ta>
            <ta e="T157" id="Seg_12362" s="T154">Это так и происходит. </ta>
            <ta e="T166" id="Seg_12363" s="T157">Дети в эту новую эпову (век) что обучаются всё-таки очень одобрительно.</ta>
            <ta e="T170" id="Seg_12364" s="T166">Они грамотными должны были бы. </ta>
            <ta e="T179" id="Seg_12365" s="T170">Тут я Ивана сына своего с малых лет обучал, чтобы рыбу .. и добычу ловил капканом.</ta>
            <ta e="T182" id="Seg_12366" s="T179">Я сейчас старею. </ta>
            <ta e="T186" id="Seg_12367" s="T182">Работу не осилившим становлюсь. </ta>
            <ta e="T190" id="Seg_12368" s="T186">Кто мою семью кормить будет?</ta>
            <ta e="T194" id="Seg_12369" s="T190">Тундра холодная, с сильными пургами. </ta>
            <ta e="T201" id="Seg_12370" s="T194">Сильный только молодой человек добычу находит.</ta>
            <ta e="T209" id="Seg_12371" s="T201">Ивана в школе .. пасти, капканы снаряжать не будут обучать. </ta>
            <ta e="T213" id="Seg_12372" s="T209">Он забудет тундровую работу.</ta>
            <ta e="T221" id="Seg_12373" s="T213">Как женщина дома только будет сидеть, ничего не делая.</ta>
            <ta e="T229" id="Seg_12374" s="T221">"Ты дед женщин не трогай!", опять Евгении крик раздался.</ta>
            <ta e="T233" id="Seg_12375" s="T229">"Кто тебе еду готовит?</ta>
            <ta e="T239" id="Seg_12376" s="T233">дом твой тепло держит, обувь.. одежду твою реставрирует?"</ta>
            <ta e="T248" id="Seg_12377" s="T239">"Так, так и происходит, -- не очень возбуждённо Парфирий сказал стоя, --</ta>
            <ta e="T257" id="Seg_12378" s="T248">У меня Кыча дочь готовить умеет, обувь, одежду шить умеет. </ta>
            <ta e="T262" id="Seg_12379" s="T257">В школе всё-таки это забудет.</ta>
            <ta e="T269" id="Seg_12380" s="T262">Как мужик, вздёрнув нос, будет ходить вокруг дома. </ta>
            <ta e="T274" id="Seg_12381" s="T269">Как я буду жить, когда дети уедут?"</ta>
            <ta e="T282" id="Seg_12382" s="T274">Родовой Анисим Назарович Попов с усмешкой старика остановил.</ta>
            <ta e="T287" id="Seg_12383" s="T282">"Ну, дедушка, ты устал, кажется. </ta>
            <ta e="T289" id="Seg_12384" s="T287">Посиди. </ta>
            <ta e="T292" id="Seg_12385" s="T289">Кто - нибудь другой пусть выскажется".</ta>
            <ta e="T304" id="Seg_12386" s="T292">Старик молча, тихонечко на место плашмя сел, за колени придерживаясь.</ta>
            <ta e="T311" id="Seg_12387" s="T304">"Кто ещё с каким-то предложением есть?" -- задал вопрос их Родовой.</ta>
            <ta e="T321" id="Seg_12388" s="T311">Кто-то что скажет ожидая, посчитал, какие дети в школу идут. </ta>
            <ta e="T327" id="Seg_12389" s="T321">Кто остаётся в школе, не идёт, взрослых уже.</ta>
            <ta e="T334" id="Seg_12390" s="T327">К тем детям попал Парфирия старика сын Иван.</ta>
            <ta e="T338" id="Seg_12391" s="T334">Кычу тоже оставить думает. </ta>
            <ta e="T346" id="Seg_12392" s="T338">Её годов всё-таки много, попросил Парфирий дед.</ta>
            <ta e="T351" id="Seg_12393" s="T346">"Нет, Кычаа не пойдёт в школу. </ta>
            <ta e="T360" id="Seg_12394" s="T351">Дед, не переживай, если с образованием будет, мужские штаны не наденет.</ta>
            <ta e="T365" id="Seg_12395" s="T360">Вокруг дома, вздёрнув нос, ходить не будет".</ta>
            <ta e="T371" id="Seg_12396" s="T365">На собрании сидевший народ от всей души рассмеялся.</ta>
            <ta e="T377" id="Seg_12397" s="T371">"Детей, которые едут, соберите хорошо. </ta>
            <ta e="T381" id="Seg_12398" s="T377">Оденьте как в праздник будто одеваете.</ta>
            <ta e="T390" id="Seg_12399" s="T381">Детей сам повезу из Волочанки", -- собрание завершая, произнёс советчик (главный).</ta>
            <ta e="T395" id="Seg_12400" s="T390">После собрания сборов шум всё прибавлялся.</ta>
            <ta e="T403" id="Seg_12401" s="T395">Женщины из сумоу вытаскивают деток своих широкие, широкие одежды. </ta>
            <ta e="T406" id="Seg_12402" s="T403">Для надевания в праздник рубашки.</ta>
            <ta e="T414" id="Seg_12403" s="T406">Некоторые в потайные места пришивают, оказывается, маленькие иконки, крестики.</ta>
            <ta e="T424" id="Seg_12404" s="T414">Детей (чествуют?) всей самой вкусной едой. Амахой (измельчённое оленье мясо заливается жиром), (сухим продуктом?), оленьим языком.</ta>
            <ta e="T433" id="Seg_12405" s="T424">К вечеру .. к Родовому в гости зашёл Семён дед.</ta>
            <ta e="T437" id="Seg_12406" s="T433">Зачем пришёл на прямую не говорит. </ta>
            <ta e="T442" id="Seg_12407" s="T437">За чаем только сказал:</ta>
            <ta e="T447" id="Seg_12408" s="T442">"В прошлом наши деды, отцы в бога верили. </ta>
            <ta e="T453" id="Seg_12409" s="T447">Большому шайтану всегда подарки приподноить обычай был</ta>
            <ta e="T458" id="Seg_12410" s="T453">Вся родня хорошо жить хотят. </ta>
            <ta e="T462" id="Seg_12411" s="T458">Не в чём не нуждаясь, не болея.</ta>
            <ta e="T470" id="Seg_12412" s="T462">Большие шаманы, человек по которой ходит дорогу, знали было хорошо. </ta>
            <ta e="T477" id="Seg_12413" s="T470">От нашего места не далеко есть большой шайтан-камень. </ta>
            <ta e="T482" id="Seg_12414" s="T477">Все люди там подарки преподносят.</ta>
            <ta e="T489" id="Seg_12415" s="T482">Скоро дети наши на чужбину едут, к чужим людям".</ta>
            <ta e="T496" id="Seg_12416" s="T489">Родовой гостя своего весть слушав про себя подумал: </ta>
            <ta e="T501" id="Seg_12417" s="T496">"С какой мыслью эту весть рассказывает?"</ta>
            <ta e="T505" id="Seg_12418" s="T501">Гость не спеша разговор продолжил.</ta>
            <ta e="T510" id="Seg_12419" s="T505">Женщины бедные сильно пригорюнились. Плачут.</ta>
            <ta e="T517" id="Seg_12420" s="T510">Маленького ребёнка из дома забирать идея с большим грехо.</ta>
            <ta e="T520" id="Seg_12421" s="T517">Народ сильно переживает. </ta>
            <ta e="T524" id="Seg_12422" s="T520">Наверное, надо шайтану подарок приподнести надо. </ta>
            <ta e="T531" id="Seg_12423" s="T524">"Ну, вот что старик, меня не осуждай. Этого делать не будем".</ta>
            <ta e="T534" id="Seg_12424" s="T531">"Школа шайтана не просит. </ta>
            <ta e="T541" id="Seg_12425" s="T534">Так всем людям расскажи. Пусть не переживают", -- промолвил Родовой. </ta>
            <ta e="T548" id="Seg_12426" s="T541">Старик, ничего не сказав, в чашку свою чаю налил.</ta>
            <ta e="T556" id="Seg_12427" s="T548">"Мы, старики, как будет лучше, думая, совет даём. </ta>
            <ta e="T562" id="Seg_12428" s="T556">И правда, иногда неправильно говорим, наверно".</ta>
            <ta e="T565" id="Seg_12429" s="T562">Женщины рыдают сильно. </ta>
            <ta e="T573" id="Seg_12430" s="T565">Человека поучение (наравоучение).. нравоучение уметь хранить сильное дело.</ta>
            <ta e="T582" id="Seg_12431" s="T573">К очень маленьким детям старшеклассников (больших) пристроить, всегда чтобы присматривали.</ta>
            <ta e="T590" id="Seg_12432" s="T582">Очень маленький ребёнок, как оленёнок, куда идти не знает. </ta>
            <ta e="T593" id="Seg_12433" s="T590">Как следует одеваться не умеет.</ta>
            <ta e="T597" id="Seg_12434" s="T593">"Вот так вот", -- сказал Родовой. </ta>
            <ta e="T603" id="Seg_12435" s="T597">Старик из-за шума на собрании забыл.</ta>
            <ta e="T612" id="Seg_12436" s="T603">Зимой, новый год когда приближаться будет, дети на каникулы приедут домой.</ta>
            <ta e="T623" id="Seg_12437" s="T612">Весной, в мае месяце, по окончанию учебного года до осени дома будут.</ta>
            <ta e="T629" id="Seg_12438" s="T623">Эту новость я всем людям расскажу.</ta>
            <ta e="T640" id="Seg_12439" s="T629">"Люди думают, безвозвратно забирают детей", -- обрадовавшись, старик сказал.</ta>
            <ta e="T648" id="Seg_12440" s="T640">Старик как домой ушёл, Анисим сам с собой разговаривает:</ta>
            <ta e="T661" id="Seg_12441" s="T648">"Всем людям разъяснить надо, как новый путь ведёт к улучшению жизни тундровиков".</ta>
            <ta e="T668" id="Seg_12442" s="T661">Таких стариков весть когда распространяют, правдиво, сильно воспринято будет.</ta>
            <ta e="T678" id="Seg_12443" s="T668">На следующий день снова на собрании люди собрались у Семёна старкик дома.</ta>
            <ta e="T684" id="Seg_12444" s="T678">У народа чем вчера как-то настроение повысилось. </ta>
            <ta e="T689" id="Seg_12445" s="T684">Шум, смех-шутки как утихли Родовой сказал:</ta>
            <ta e="T696" id="Seg_12446" s="T689">"По своему желанию малышей закрепите за старшими детьми.</ta>
            <ta e="T701" id="Seg_12447" s="T696">В пути (в аргише) чтобы присматривали за ними".</ta>
            <ta e="T707" id="Seg_12448" s="T701">"Нам сказали, что дети наши зимой отдыхать приедут. </ta>
            <ta e="T714" id="Seg_12449" s="T707">Это правда? -- спросила одна женщина.</ta>
            <ta e="T720" id="Seg_12450" s="T714">Семён дед из своего места выглядывал.</ta>
            <ta e="T723" id="Seg_12451" s="T720">"Это правдивая весть. </ta>
            <ta e="T734" id="Seg_12452" s="T723">Зимой отдыхать приедут, затем на всё лето дома побыть приедут", -- сказал Анисим.</ta>
            <ta e="T742" id="Seg_12453" s="T734">Семён дед передохнув промолвил:</ta>
            <ta e="T745" id="Seg_12454" s="T742">"Я о чём говорил?"</ta>
            <ta e="T753" id="Seg_12455" s="T745">Все, с одобрением взглянув к Семёну деду, повернулись.</ta>
            <ta e="T756" id="Seg_12456" s="T753">Собрание прошло быстро. </ta>
            <ta e="T761" id="Seg_12457" s="T756">Люди по чумам разошлись.</ta>
            <ta e="T770" id="Seg_12458" s="T761">Следующим днём, стадо собрав, оленей лов начался для аргиша.</ta>
            <ta e="T778" id="Seg_12459" s="T770"> У женщин около дома с детьми времени нет собираясь.</ta>
            <ta e="T787" id="Seg_12460" s="T778">Детям постоянно наказывают, чтобы не баловались, старших детей чтобы слушались.</ta>
            <ta e="T796" id="Seg_12461" s="T787">Отловив оленей, и после как их запрягли, в каждом чуме кушают перед аргишом.</ta>
            <ta e="T807" id="Seg_12462" s="T796">Затем возле чума каждого все высыпали на улицу, чтобы в дальний путь собравшихся детей проводить. </ta>
            <ta e="T813" id="Seg_12463" s="T807">"Когда я тебя увижу? -- Евгения заплакала</ta>
            <ta e="T820" id="Seg_12464" s="T813">-- как без тебя проживу? </ta>
            <ta e="T823" id="Seg_12465" s="T820">Кто нам поможет?"</ta>
            <ta e="T830" id="Seg_12466" s="T823">Ребёнка сильно прижав к себе, в сторону дома тащит.</ta>
            <ta e="T838" id="Seg_12467" s="T830">" Что с тобой происходит? -- одна женщина сказала подружке.</ta>
            <ta e="T843" id="Seg_12468" s="T838">Ребёнка не заставляй периживать перед аргишом. </ta>
            <ta e="T847" id="Seg_12469" s="T843">Так провожать очень плохо. </ta>
            <ta e="T850" id="Seg_12470" s="T847">Грех! Проводи по- человечески".</ta>
            <ta e="T859" id="Seg_12471" s="T850">Все кто на оленях верхом дети, за дедом следуя, длинный аргиш образовали. </ta>
            <ta e="T866" id="Seg_12472" s="T859">Кто стоя, кто сидя, детей провожая за тем аргишом растянулись.</ta>
            <ta e="T876" id="Seg_12473" s="T866">Парфирий дед, глаза широко открыв, изо всех сил благословя стоял.</ta>
            <ta e="T884" id="Seg_12474" s="T876">"Ну вот, удачливы будьте, на вашем пути чтобы никто не мешал. </ta>
            <ta e="T887" id="Seg_12475" s="T884">Большой наукой овладейте. </ta>
            <ta e="T891" id="Seg_12476" s="T887">Быстрее возвращайтесь на родную землю!"</ta>
            <ta e="T906" id="Seg_12477" s="T891">Оставшийся народ долго очень вглядывались вдаль тундры, пока длинный аргиш не исчез за горизонт.</ta>
            <ta e="T914" id="Seg_12478" s="T906">Для детей всех зародилось новое время -- обучающая эпоха письму (грамоте).</ta>
            <ta e="T920" id="Seg_12479" s="T914">Вы слушали рассказ "Аргиш в Волочанку." </ta>
            <ta e="T922" id="Seg_12480" s="T920">Читал Попов.</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T374" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
