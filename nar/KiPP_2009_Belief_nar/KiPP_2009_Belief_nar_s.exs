<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDECDB190A-2530-9C3B-7DC8-A2146274BA0B">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>KiPP_2009_Belief_nar</transcription-name>
         <referenced-file url="KiPP_2009_Belief_nar.wav" />
         <referenced-file url="KiPP_2009_Belief_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\KiPP_2009_Belief_nar\KiPP_2009_Belief_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1047</ud-information>
            <ud-information attribute-name="# HIAT:w">702</ud-information>
            <ud-information attribute-name="# e">707</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">6</ud-information>
            <ud-information attribute-name="# HIAT:u">132</ud-information>
            <ud-information attribute-name="# sc">78</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KiPP">
            <abbreviation>KiPP</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="SE">
            <abbreviation>SE</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="2.4599647514684073" />
         <tli id="T2" time="2.5692323757342037" type="intp" />
         <tli id="T3" time="2.6785" type="appl" />
         <tli id="T4" time="3.8527500000000003" type="appl" />
         <tli id="T0" time="4.079941539020774" />
         <tli id="T5" time="5.939914887692008" />
         <tli id="T6" time="6.2780288724174325" type="intp" />
         <tli id="T7" time="6.616142857142857" type="appl" />
         <tli id="T8" time="7.4107142857142865" type="appl" />
         <tli id="T9" time="8.205285714285715" type="appl" />
         <tli id="T10" time="8.999857142857143" type="appl" />
         <tli id="T11" time="9.794428571428572" type="appl" />
         <tli id="T12" time="10.622333145460669" />
         <tli id="T13" time="11.424800000000001" type="appl" />
         <tli id="T14" time="12.2606" type="appl" />
         <tli id="T15" time="13.096400000000001" type="appl" />
         <tli id="T16" time="13.932200000000002" type="appl" />
         <tli id="T17" time="15.359779911607617" />
         <tli id="T18" time="15.522857142857143" type="appl" />
         <tli id="T19" time="16.277714285714286" type="appl" />
         <tli id="T20" time="17.03257142857143" type="appl" />
         <tli id="T21" time="17.78742857142857" type="appl" />
         <tli id="T22" time="18.542285714285715" type="appl" />
         <tli id="T23" time="19.29714285714286" type="appl" />
         <tli id="T24" time="21.133030520744853" />
         <tli id="T25" time="21.237515260372426" type="intp" />
         <tli id="T26" time="21.342" type="appl" />
         <tli id="T27" time="21.987" type="appl" />
         <tli id="T28" time="22.631999999999998" type="appl" />
         <tli id="T29" time="23.277" type="appl" />
         <tli id="T30" time="23.922" type="appl" />
         <tli id="T31" time="24.567" type="appl" />
         <tli id="T32" time="25.546300616875172" />
         <tli id="T33" time="25.82" type="appl" />
         <tli id="T34" time="26.428" type="appl" />
         <tli id="T35" time="27.036" type="appl" />
         <tli id="T36" time="27.644" type="appl" />
         <tli id="T37" time="28.252" type="appl" />
         <tli id="T38" time="28.866666451220887" />
         <tli id="T39" time="29.727995255179994" />
         <tli id="T40" time="30.002714285714287" type="appl" />
         <tli id="T41" time="30.53742857142857" type="appl" />
         <tli id="T42" time="31.072142857142858" type="appl" />
         <tli id="T43" time="31.60685714285714" type="appl" />
         <tli id="T44" time="32.141571428571424" type="appl" />
         <tli id="T45" time="32.68295291395607" />
         <tli id="T46" time="33.5776601458898" />
         <tli id="T47" time="34.162" type="appl" />
         <tli id="T48" time="35.113" type="appl" />
         <tli id="T49" time="36.06400147253652" />
         <tli id="T50" time="37.672793524520245" />
         <tli id="T51" time="37.67657142857143" type="appl" />
         <tli id="T52" time="38.338142857142856" type="appl" />
         <tli id="T53" time="38.99971428571428" type="appl" />
         <tli id="T54" time="39.66128571428572" type="appl" />
         <tli id="T55" time="40.322857142857146" type="appl" />
         <tli id="T56" time="40.99109558635418" />
         <tli id="T57" time="41.646" type="appl" />
         <tli id="T58" time="42.338499999999996" type="appl" />
         <tli id="T59" time="43.031" type="appl" />
         <tli id="T60" time="43.7235" type="appl" />
         <tli id="T61" time="44.38266808268008" />
         <tli id="T62" time="44.419" type="appl" />
         <tli id="T63" time="44.80025" type="appl" />
         <tli id="T64" time="45.1815" type="appl" />
         <tli id="T65" time="45.56275" type="appl" />
         <tli id="T66" time="45.941" type="appl" />
         <tli id="T67" time="45.944" type="appl" />
         <tli id="T68" time="46.54929411764706" type="appl" />
         <tli id="T69" time="47.15758823529412" type="appl" />
         <tli id="T70" time="47.765882352941176" type="appl" />
         <tli id="T71" time="48.37417647058824" type="appl" />
         <tli id="T72" time="48.982470588235294" type="appl" />
         <tli id="T73" time="49.59076470588235" type="appl" />
         <tli id="T74" time="50.19905882352941" type="appl" />
         <tli id="T75" time="50.80735294117647" type="appl" />
         <tli id="T76" time="51.41564705882353" type="appl" />
         <tli id="T77" time="52.023941176470586" type="appl" />
         <tli id="T78" time="52.63223529411765" type="appl" />
         <tli id="T79" time="53.240529411764705" type="appl" />
         <tli id="T80" time="53.84882352941176" type="appl" />
         <tli id="T81" time="54.45711764705882" type="appl" />
         <tli id="T82" time="55.06541176470588" type="appl" />
         <tli id="T83" time="55.67370746664401" />
         <tli id="T84" time="56.1553346800504" />
         <tli id="T85" time="56.90328571428571" type="appl" />
         <tli id="T86" time="57.52457142857143" type="appl" />
         <tli id="T87" time="58.14585714285714" type="appl" />
         <tli id="T88" time="58.76714285714286" type="appl" />
         <tli id="T89" time="59.38842857142857" type="appl" />
         <tli id="T90" time="59.39045543860323" type="intp" />
         <tli id="T723" />
         <tli id="T91" time="60.35913512139557" />
         <tli id="T92" time="61.6065" type="appl" />
         <tli id="T93" time="64.6857397926774" />
         <tli id="T94" time="64.80072703919583" type="intp" />
         <tli id="T95" time="64.91571428571429" type="appl" />
         <tli id="T96" time="66.08257142857143" type="appl" />
         <tli id="T97" time="67.24942857142857" type="appl" />
         <tli id="T98" time="68.41628571428572" type="appl" />
         <tli id="T99" time="69.58314285714286" type="appl" />
         <tli id="T100" time="70.98564952204771" />
         <tli id="T101" time="71.4985" type="appl" />
         <tli id="T102" time="72.247" type="appl" />
         <tli id="T103" time="72.99549999999999" type="appl" />
         <tli id="T104" time="73.744" type="appl" />
         <tli id="T105" time="73.97227339374919" />
         <tli id="T724" time="74.31226852200093" />
         <tli id="T106" time="75.59225018130155" />
         <tli id="T107" time="75.93433333333333" type="appl" />
         <tli id="T108" time="76.62766666666667" type="appl" />
         <tli id="T109" time="77.321" type="appl" />
         <tli id="T110" time="78.01433333333333" type="appl" />
         <tli id="T111" time="78.70766666666667" type="appl" />
         <tli id="T112" time="79.401" type="appl" />
         <tli id="T113" time="79.79219000088176" />
         <tli id="T114" time="80.0521862754272" />
         <tli id="T725" time="80.50551311309619" />
         <tli id="T115" time="81.71882906097491" />
         <tli id="T116" time="82.587" type="appl" />
         <tli id="T117" time="83.42547127337575" />
         <tli id="T118" time="85.23877862405166" />
         <tli id="T119" time="85.30833333333334" type="appl" />
         <tli id="T120" time="85.81766666666667" type="appl" />
         <tli id="T121" time="86.327" type="appl" />
         <tli id="T122" time="86.83633333333334" type="appl" />
         <tli id="T123" time="87.34566666666667" type="appl" />
         <tli id="T124" time="87.855" type="appl" />
         <tli id="T125" time="88.36433333333333" type="appl" />
         <tli id="T126" time="88.87366666666667" type="appl" />
         <tli id="T127" time="89.38300000000001" type="appl" />
         <tli id="T128" time="89.89233333333334" type="appl" />
         <tli id="T129" time="90.40166666666667" type="appl" />
         <tli id="T130" time="91.05100002894783" />
         <tli id="T131" time="91.685" type="appl" />
         <tli id="T132" time="92.459" type="appl" />
         <tli id="T133" time="93.233" type="appl" />
         <tli id="T134" time="94.30531537743116" />
         <tli id="T135" time="94.48671428571429" type="appl" />
         <tli id="T136" time="94.96642857142858" type="appl" />
         <tli id="T137" time="95.44614285714286" type="appl" />
         <tli id="T138" time="95.92585714285714" type="appl" />
         <tli id="T139" time="96.40557142857142" type="appl" />
         <tli id="T140" time="96.88528571428571" type="appl" />
         <tli id="T141" time="97.53832894781168" />
         <tli id="T142" time="97.73275" type="appl" />
         <tli id="T143" time="98.1005" type="appl" />
         <tli id="T144" time="98.46825" type="appl" />
         <tli id="T145" time="98.836" type="appl" />
         <tli id="T146" time="99.20375" type="appl" />
         <tli id="T147" time="99.5715" type="appl" />
         <tli id="T148" time="99.93925" type="appl" />
         <tli id="T149" time="100.307" type="appl" />
         <tli id="T150" time="100.67475" type="appl" />
         <tli id="T151" time="101.0425" type="appl" />
         <tli id="T152" time="101.41025" type="appl" />
         <tli id="T153" time="102.27853446349135" />
         <tli id="T154" time="103.41851812880599" />
         <tli id="T155" time="104.096" type="appl" />
         <tli id="T156" time="104.588" type="appl" />
         <tli id="T157" time="105.1048" type="appl" />
         <tli id="T158" time="105.6216" type="appl" />
         <tli id="T159" time="106.13839999999999" type="appl" />
         <tli id="T160" time="106.6552" type="appl" />
         <tli id="T161" time="107.95178650549573" />
         <tli id="T162" time="108.037" type="appl" />
         <tli id="T163" time="108.902" type="appl" />
         <tli id="T164" time="109.767" type="appl" />
         <tli id="T165" time="110.75866034864653" />
         <tli id="T166" time="111.28185714285715" type="appl" />
         <tli id="T167" time="111.93171428571429" type="appl" />
         <tli id="T168" time="112.58157142857144" type="appl" />
         <tli id="T169" time="113.23142857142857" type="appl" />
         <tli id="T170" time="113.88128571428571" type="appl" />
         <tli id="T171" time="114.53114285714285" type="appl" />
         <tli id="T172" time="115.00100841473088" />
         <tli id="T173" time="115.59418181818181" type="appl" />
         <tli id="T174" time="116.00736363636364" type="appl" />
         <tli id="T175" time="116.42054545454545" type="appl" />
         <tli id="T176" time="116.83372727272727" type="appl" />
         <tli id="T177" time="117.24690909090909" type="appl" />
         <tli id="T178" time="117.66009090909091" type="appl" />
         <tli id="T179" time="118.07327272727272" type="appl" />
         <tli id="T180" time="118.48645454545455" type="appl" />
         <tli id="T181" time="118.89963636363636" type="appl" />
         <tli id="T182" time="119.31281818181819" type="appl" />
         <tli id="T183" time="119.89267009354721" />
         <tli id="T184" time="120.2282" type="appl" />
         <tli id="T185" time="120.7304" type="appl" />
         <tli id="T186" time="121.2326" type="appl" />
         <tli id="T187" time="121.7348" type="appl" />
         <tli id="T188" time="122.237" type="appl" />
         <tli id="T189" time="122.7392" type="appl" />
         <tli id="T190" time="123.2414" type="appl" />
         <tli id="T191" time="123.7436" type="appl" />
         <tli id="T192" time="124.2458" type="appl" />
         <tli id="T193" time="124.748" type="appl" />
         <tli id="T194" time="125.76333333333334" type="appl" />
         <tli id="T195" time="126.77866666666667" type="appl" />
         <tli id="T196" time="127.794" type="appl" />
         <tli id="T197" time="128.80933333333334" type="appl" />
         <tli id="T198" time="129.82466666666667" type="appl" />
         <tli id="T199" time="130.84" type="appl" />
         <tli id="T200" time="131.85533333333333" type="appl" />
         <tli id="T201" time="132.87066666666666" type="appl" />
         <tli id="T202" time="133.91266451527588" />
         <tli id="T203" time="134.34" type="appl" />
         <tli id="T204" time="134.794" type="appl" />
         <tli id="T205" time="135.248" type="appl" />
         <tli id="T206" time="135.702" type="appl" />
         <tli id="T207" time="136.46471128064582" />
         <tli id="T208" time="137.102" type="appl" />
         <tli id="T209" time="138.048" type="appl" />
         <tli id="T210" time="138.70467918442193" />
         <tli id="T726" time="139.10467345295336" />
         <tli id="T211" time="140.01333230789268" />
         <tli id="T212" time="140.31314285714285" type="appl" />
         <tli id="T213" time="140.6862857142857" type="appl" />
         <tli id="T214" time="141.05942857142855" type="appl" />
         <tli id="T215" time="141.43257142857144" type="appl" />
         <tli id="T216" time="141.8057142857143" type="appl" />
         <tli id="T217" time="142.17885714285714" type="appl" />
         <tli id="T218" time="142.75199098265688" />
         <tli id="T219" time="143.2556" type="appl" />
         <tli id="T220" time="143.95919999999998" type="appl" />
         <tli id="T221" time="144.6628" type="appl" />
         <tli id="T222" time="145.3664" type="appl" />
         <tli id="T223" time="147.54455251896692" />
         <tli id="T224" time="147.58027625948347" type="intp" />
         <tli id="T225" time="147.61599999999999" type="appl" />
         <tli id="T226" time="148.389" type="appl" />
         <tli id="T227" time="149.162" type="appl" />
         <tli id="T228" time="149.935" type="appl" />
         <tli id="T229" time="150.708" type="appl" />
         <tli id="T230" time="151.481" type="appl" />
         <tli id="T231" time="152.254" type="appl" />
         <tli id="T232" time="153.02700000000002" type="appl" />
         <tli id="T233" time="153.27113713010883" />
         <tli id="T727" time="153.86446196176382" />
         <tli id="T234" time="154.573" type="appl" />
         <tli id="T235" time="155.12128571428573" type="appl" />
         <tli id="T236" time="155.66957142857143" type="appl" />
         <tli id="T237" time="156.21785714285716" type="appl" />
         <tli id="T238" time="156.76614285714285" type="appl" />
         <tli id="T239" time="157.31442857142858" type="appl" />
         <tli id="T240" time="157.86271428571428" type="appl" />
         <tli id="T241" time="158.91099121316844" />
         <tli id="T242" time="159.09216666666666" type="appl" />
         <tli id="T243" time="159.77333333333334" type="appl" />
         <tli id="T244" time="160.4545" type="appl" />
         <tli id="T245" time="161.13566666666665" type="appl" />
         <tli id="T246" time="161.81683333333334" type="appl" />
         <tli id="T247" time="162.6179953876294" />
         <tli id="T248" time="163.0236875" type="appl" />
         <tli id="T249" time="163.549375" type="appl" />
         <tli id="T250" time="164.0750625" type="appl" />
         <tli id="T251" time="164.60075" type="appl" />
         <tli id="T252" time="165.12643749999998" type="appl" />
         <tli id="T253" time="165.65212499999998" type="appl" />
         <tli id="T254" time="166.1778125" type="appl" />
         <tli id="T255" time="166.7035" type="appl" />
         <tli id="T256" time="167.2291875" type="appl" />
         <tli id="T257" time="167.754875" type="appl" />
         <tli id="T258" time="168.2805625" type="appl" />
         <tli id="T259" time="168.80624999999998" type="appl" />
         <tli id="T260" time="169.33193749999998" type="appl" />
         <tli id="T261" time="169.85762499999998" type="appl" />
         <tli id="T262" time="170.3833125" type="appl" />
         <tli id="T263" time="170.909" type="appl" />
         <tli id="T264" time="172.1625" type="appl" />
         <tli id="T265" time="174.5308325025553" />
         <tli id="T266" time="174.61387079673221" type="intp" />
         <tli id="T267" time="174.6969090909091" type="appl" />
         <tli id="T268" time="175.33736363636365" type="appl" />
         <tli id="T269" time="175.97781818181818" type="appl" />
         <tli id="T270" time="176.61827272727274" type="appl" />
         <tli id="T271" time="177.25872727272727" type="appl" />
         <tli id="T272" time="177.89918181818183" type="appl" />
         <tli id="T273" time="178.53963636363636" type="appl" />
         <tli id="T274" time="179.18009090909092" type="appl" />
         <tli id="T275" time="179.82054545454545" type="appl" />
         <tli id="T276" time="180.8640750876366" />
         <tli id="T277" time="181.09120000000001" type="appl" />
         <tli id="T278" time="181.72140000000002" type="appl" />
         <tli id="T279" time="182.3516" type="appl" />
         <tli id="T280" time="182.9818" type="appl" />
         <tli id="T281" time="183.612" type="appl" />
         <tli id="T282" time="184.1801818181818" type="appl" />
         <tli id="T283" time="184.74836363636362" type="appl" />
         <tli id="T284" time="185.31654545454546" type="appl" />
         <tli id="T285" time="185.88472727272728" type="appl" />
         <tli id="T286" time="186.4529090909091" type="appl" />
         <tli id="T287" time="187.0210909090909" type="appl" />
         <tli id="T288" time="187.58927272727271" type="appl" />
         <tli id="T289" time="188.15745454545453" type="appl" />
         <tli id="T290" time="188.72563636363637" type="appl" />
         <tli id="T291" time="189.29381818181818" type="appl" />
         <tli id="T292" time="189.92200519350635" />
         <tli id="T293" time="190.29581818181816" type="appl" />
         <tli id="T294" time="190.72963636363636" type="appl" />
         <tli id="T295" time="191.16345454545453" type="appl" />
         <tli id="T296" time="191.59727272727272" type="appl" />
         <tli id="T297" time="192.0310909090909" type="appl" />
         <tli id="T298" time="192.13058031793906" />
         <tli id="T299" time="192.89872727272726" type="appl" />
         <tli id="T300" time="193.33254545454545" type="appl" />
         <tli id="T301" time="193.76636363636362" type="appl" />
         <tli id="T302" time="194.20018181818182" type="appl" />
         <tli id="T303" time="194.92732409790895" />
         <tli id="T304" time="195.2311111111111" type="appl" />
         <tli id="T305" time="195.82822222222222" type="appl" />
         <tli id="T306" time="196.42533333333333" type="appl" />
         <tli id="T307" time="197.02244444444443" type="appl" />
         <tli id="T308" time="197.61955555555556" type="appl" />
         <tli id="T309" time="198.21666666666667" type="appl" />
         <tli id="T310" time="198.81377777777777" type="appl" />
         <tli id="T311" time="199.4108888888889" type="appl" />
         <tli id="T312" time="200.04133936148148" />
         <tli id="T313" time="200.44728571428573" type="appl" />
         <tli id="T314" time="200.88657142857144" type="appl" />
         <tli id="T315" time="201.32585714285716" type="appl" />
         <tli id="T316" time="201.76514285714285" type="appl" />
         <tli id="T317" time="202.20442857142856" type="appl" />
         <tli id="T318" time="202.64371428571428" type="appl" />
         <tli id="T319" time="203.11633436275727" />
         <tli id="T320" time="203.7625" type="appl" />
         <tli id="T321" time="204.62200289238524" />
         <tli id="T322" time="205.0114" type="appl" />
         <tli id="T323" time="205.5808" type="appl" />
         <tli id="T324" time="206.1502" type="appl" />
         <tli id="T325" time="206.7196" type="appl" />
         <tli id="T326" time="207.289" type="appl" />
         <tli id="T327" time="207.8584" type="appl" />
         <tli id="T328" time="208.4278" type="appl" />
         <tli id="T329" time="208.9972" type="appl" />
         <tli id="T330" time="209.5666" type="appl" />
         <tli id="T331" time="210.05600055013622" />
         <tli id="T332" time="212.73695171845085" />
         <tli id="T333" time="212.74866666666665" type="appl" />
         <tli id="T334" time="213.87933333333334" type="appl" />
         <tli id="T335" time="215.14333650849323" />
         <tli id="T336" time="215.67433333333332" type="appl" />
         <tli id="T337" time="216.33866666666665" type="appl" />
         <tli id="T338" time="217.14299535562816" />
         <tli id="T339" time="217.667" type="appl" />
         <tli id="T340" time="218.331" type="appl" />
         <tli id="T341" time="219.03498647468737" />
         <tli id="T342" time="219.46733333333333" type="appl" />
         <tli id="T343" time="219.93966666666668" type="appl" />
         <tli id="T344" time="220.412" type="appl" />
         <tli id="T345" time="220.88433333333333" type="appl" />
         <tli id="T346" time="221.35666666666668" type="appl" />
         <tli id="T347" time="222.0156729314654" />
         <tli id="T348" time="222.47175000000001" type="appl" />
         <tli id="T349" time="223.11450000000002" type="appl" />
         <tli id="T350" time="223.75725" type="appl" />
         <tli id="T351" time="224.32665544268195" />
         <tli id="T352" time="225.01057142857144" type="appl" />
         <tli id="T353" time="225.62114285714287" type="appl" />
         <tli id="T354" time="226.2317142857143" type="appl" />
         <tli id="T355" time="226.8422857142857" type="appl" />
         <tli id="T356" time="227.45285714285714" type="appl" />
         <tli id="T357" time="228.06342857142857" type="appl" />
         <tli id="T358" time="229.05671787453392" />
         <tli id="T359" time="229.31175000000002" type="appl" />
         <tli id="T360" time="229.9495" type="appl" />
         <tli id="T361" time="230.58724999999998" type="appl" />
         <tli id="T728" time="230.93278833318774" type="intp" />
         <tli id="T362" time="232.97666170614212" />
         <tli id="T363" time="234.12331194259892" />
         <tli id="T364" time="235.3166281770511" />
         <tli id="T365" time="235.71642857142857" type="appl" />
         <tli id="T366" time="236.54185714285714" type="appl" />
         <tli id="T367" time="237.3672857142857" type="appl" />
         <tli id="T368" time="238.1927142857143" type="appl" />
         <tli id="T369" time="239.01814285714286" type="appl" />
         <tli id="T370" time="239.84357142857144" type="appl" />
         <tli id="T371" time="240.8156639653787" />
         <tli id="T372" time="241.5685" type="appl" />
         <tli id="T373" time="242.46800000000002" type="appl" />
         <tli id="T374" time="243.3675" type="appl" />
         <tli id="T375" time="244.68698870197724" />
         <tli id="T376" time="244.80349999999999" type="appl" />
         <tli id="T377" time="245.34" type="appl" />
         <tli id="T378" time="245.8765" type="appl" />
         <tli id="T379" time="246.41299999999998" type="appl" />
         <tli id="T380" time="246.9495" type="appl" />
         <tli id="T381" time="247.37267938571327" />
         <tli id="T382" time="249.8639978545848" />
         <tli id="T383" time="250.23014285714285" type="appl" />
         <tli id="T384" time="250.7762857142857" type="appl" />
         <tli id="T385" time="250.837" type="appl" />
         <tli id="T386" time="251.32242857142856" type="appl" />
         <tli id="T387" time="251.4175" type="appl" />
         <tli id="T388" time="251.86857142857144" type="appl" />
         <tli id="T389" time="251.998" type="appl" />
         <tli id="T390" time="252.4147142857143" type="appl" />
         <tli id="T391" time="252.96085714285715" type="appl" />
         <tli id="T392" time="253.9296948060478" />
         <tli id="T393" time="254.45166666666668" type="appl" />
         <tli id="T394" time="255.39633333333333" type="appl" />
         <tli id="T395" time="256.54299069378663" />
         <tli id="T396" time="257.033" type="appl" />
         <tli id="T397" time="257.725" type="appl" />
         <tli id="T398" time="258.417" type="appl" />
         <tli id="T399" time="259.109" type="appl" />
         <tli id="T400" time="260.26293739112907" />
         <tli id="T401" time="260.2823333333333" type="appl" />
         <tli id="T402" time="260.76366666666667" type="appl" />
         <tli id="T403" time="261.245" type="appl" />
         <tli id="T404" time="261.72633333333334" type="appl" />
         <tli id="T405" time="262.2076666666666" type="appl" />
         <tli id="T406" time="262.68899999999996" type="appl" />
         <tli id="T407" time="263.1703333333333" type="appl" />
         <tli id="T408" time="263.65166666666664" type="appl" />
         <tli id="T409" time="264.5562092067" />
         <tli id="T410" time="265.10614285714286" type="appl" />
         <tli id="T411" time="266.0792857142857" type="appl" />
         <tli id="T412" time="267.05242857142855" type="appl" />
         <tli id="T413" time="268.0255714285714" type="appl" />
         <tli id="T414" time="268.9987142857143" type="appl" />
         <tli id="T415" time="269.9718571428571" type="appl" />
         <tli id="T416" time="270.362" type="appl" />
         <tli id="T417" time="270.945" type="appl" />
         <tli id="T418" time="270.958" type="appl" />
         <tli id="T419" time="271.725" type="appl" />
         <tli id="T420" time="272.225" type="appl" />
         <tli id="T421" time="272.725" type="appl" />
         <tli id="T422" time="273.4827479660935" />
         <tli id="T423" time="273.7985" type="appl" />
         <tli id="T424" time="274.42533862649475" />
         <tli id="T425" time="275.1145" type="appl" />
         <tli id="T426" time="275.857" type="appl" />
         <tli id="T427" time="276.466" type="appl" />
         <tli id="T428" time="276.935" type="appl" />
         <tli id="T429" time="277.075" type="appl" />
         <tli id="T430" time="277.49066666666664" type="appl" />
         <tli id="T431" time="278.04633333333334" type="appl" />
         <tli id="T432" time="278.86933744898363" />
         <tli id="T433" time="279.37624999999997" type="appl" />
         <tli id="T434" time="280.15049999999997" type="appl" />
         <tli id="T435" time="280.92475" type="appl" />
         <tli id="T436" time="281.23597020446135" />
         <tli id="T437" time="282.139" type="appl" />
         <tli id="T438" time="282.579" type="appl" />
         <tli id="T439" time="283.1056569556171" />
         <tli id="T440" time="284.1565" type="appl" />
         <tli id="T441" time="285.2492460320602" />
         <tli id="T442" time="285.8845" type="appl" />
         <tli id="T443" time="286.5816748565186" />
         <tli id="T444" time="286.73825" type="appl" />
         <tli id="T445" time="287.0015" type="appl" />
         <tli id="T446" time="287.36473655279076" />
         <tli id="T447" time="287.8679845501456" />
         <tli id="T448" time="288.182" type="appl" />
         <tli id="T449" time="288.836" type="appl" />
         <tli id="T450" time="289.49" type="appl" />
         <tli id="T451" time="290.144" type="appl" />
         <tli id="T452" time="290.7135" type="appl" />
         <tli id="T453" time="291.5491557614305" />
         <tli id="T454" time="291.8525" type="appl" />
         <tli id="T455" time="292.48247572133727" />
         <tli id="T456" time="293.9996675233497" />
         <tli id="T457" time="294.119" type="appl" />
         <tli id="T458" time="294.505" type="appl" />
         <tli id="T459" time="294.891" type="appl" />
         <tli id="T460" time="295.277" type="appl" />
         <tli id="T461" time="295.78966791632143" />
         <tli id="T462" time="296.341" type="appl" />
         <tli id="T463" time="297.019" type="appl" />
         <tli id="T464" time="298.04906262506654" />
         <tli id="T465" time="298.7165" type="appl" />
         <tli id="T466" time="299.736" type="appl" />
         <tli id="T467" time="300.6865" type="appl" />
         <tli id="T468" time="301.7036717169568" />
         <tli id="T469" time="301.9653333333333" type="appl" />
         <tli id="T470" time="302.29366666666664" type="appl" />
         <tli id="T471" time="302.622" type="appl" />
         <tli id="T472" time="302.95033333333333" type="appl" />
         <tli id="T473" time="303.27866666666665" type="appl" />
         <tli id="T474" time="303.60699999999997" type="appl" />
         <tli id="T475" time="303.93533333333335" type="appl" />
         <tli id="T476" time="304.26366666666667" type="appl" />
         <tli id="T477" time="304.592" type="appl" />
         <tli id="T478" time="305.18275" type="appl" />
         <tli id="T479" time="305.7735" type="appl" />
         <tli id="T480" time="306.36424999999997" type="appl" />
         <tli id="T481" time="306.955" type="appl" />
         <tli id="T482" time="307.6673333333333" type="appl" />
         <tli id="T483" time="308.37966666666665" type="appl" />
         <tli id="T484" time="309.41199876528907" />
         <tli id="T485" time="309.7042" type="appl" />
         <tli id="T486" time="310.3164" type="appl" />
         <tli id="T487" time="310.9286" type="appl" />
         <tli id="T488" time="311.5408" type="appl" />
         <tli id="T489" time="312.1863340121814" />
         <tli id="T490" time="314.483" type="appl" />
         <tli id="T491" time="315.821" type="appl" />
         <tli id="T492" time="316.53775" type="appl" />
         <tli id="T493" time="317.2545" type="appl" />
         <tli id="T494" time="317.97125" type="appl" />
         <tli id="T495" time="318.8880088180428" />
         <tli id="T496" time="319.5168" type="appl" />
         <tli id="T497" time="320.3456" type="appl" />
         <tli id="T498" time="321.1744" type="appl" />
         <tli id="T499" time="322.0032" type="appl" />
         <tli id="T500" time="323.0253193266974" />
         <tli id="T501" time="323.4582857142857" type="appl" />
         <tli id="T502" time="324.08457142857145" type="appl" />
         <tli id="T503" time="324.71085714285715" type="appl" />
         <tli id="T504" time="325.33714285714285" type="appl" />
         <tli id="T505" time="325.96342857142855" type="appl" />
         <tli id="T506" time="326.5897142857143" type="appl" />
         <tli id="T507" time="327.3686425084217" />
         <tli id="T508" time="327.7067" type="appl" />
         <tli id="T509" time="328.1974" type="appl" />
         <tli id="T510" time="328.6881" type="appl" />
         <tli id="T511" time="329.1788" type="appl" />
         <tli id="T512" time="329.66949999999997" type="appl" />
         <tli id="T513" time="330.1602" type="appl" />
         <tli id="T514" time="330.6509" type="appl" />
         <tli id="T515" time="331.1416" type="appl" />
         <tli id="T516" time="331.6323" type="appl" />
         <tli id="T517" time="332.7018994221744" />
         <tli id="T518" time="332.93033333333335" type="appl" />
         <tli id="T519" time="333.73766666666666" type="appl" />
         <tli id="T520" time="334.6116637240639" />
         <tli id="T521" time="335.12142857142857" type="appl" />
         <tli id="T522" time="335.6978571428572" type="appl" />
         <tli id="T523" time="336.2742857142857" type="appl" />
         <tli id="T524" time="336.8507142857143" type="appl" />
         <tli id="T525" time="337.4271428571428" type="appl" />
         <tli id="T526" time="338.00357142857143" type="appl" />
         <tli id="T527" time="338.58" type="appl" />
         <tli id="T528" time="340.134" type="appl" />
         <tli id="T529" time="341.688" type="appl" />
         <tli id="T530" time="343.24199999999996" type="appl" />
         <tli id="T531" time="345.1426586600693" />
         <tli id="T532" time="345.3338" type="appl" />
         <tli id="T533" time="345.8716" type="appl" />
         <tli id="T534" time="346.4094" type="appl" />
         <tli id="T535" time="346.9472" type="appl" />
         <tli id="T536" time="347.94834762436477" />
         <tli id="T537" time="348.00964285714286" type="appl" />
         <tli id="T538" time="348.5342857142857" type="appl" />
         <tli id="T539" time="349.05892857142857" type="appl" />
         <tli id="T540" time="349.5835714285714" type="appl" />
         <tli id="T541" time="350.10821428571427" type="appl" />
         <tli id="T542" time="350.6328571428571" type="appl" />
         <tli id="T543" time="351.1575" type="appl" />
         <tli id="T544" time="351.6821428571429" type="appl" />
         <tli id="T545" time="352.20678571428573" type="appl" />
         <tli id="T546" time="352.7314285714286" type="appl" />
         <tli id="T547" time="353.25607142857143" type="appl" />
         <tli id="T548" time="353.7807142857143" type="appl" />
         <tli id="T549" time="354.30535714285713" type="appl" />
         <tli id="T550" time="354.83" type="appl" />
         <tli id="T551" time="355.354" type="appl" />
         <tli id="T552" time="355.435" type="appl" />
         <tli id="T553" time="355.675" type="appl" />
         <tli id="T554" time="355.996" type="appl" />
         <tli id="T555" time="356.317" type="appl" />
         <tli id="T556" time="356.638" type="appl" />
         <tli id="T557" time="356.959" type="appl" />
         <tli id="T558" time="357.28" type="appl" />
         <tli id="T559" time="357.601" type="appl" />
         <tli id="T560" time="357.707" type="appl" />
         <tli id="T561" time="357.92199999999997" type="appl" />
         <tli id="T562" time="358.243" type="appl" />
         <tli id="T563" time="358.344" type="appl" />
         <tli id="T564" time="358.56399999999996" type="appl" />
         <tli id="T565" time="358.885" type="appl" />
         <tli id="T566" time="359.20599999999996" type="appl" />
         <tli id="T567" time="359.36818399093767" />
         <tli id="T568" time="360.1936666666667" type="appl" />
         <tli id="T569" time="360.8603333333333" type="appl" />
         <tli id="T570" time="362.04814559009833" />
         <tli id="T571" time="362.1904" type="appl" />
         <tli id="T572" time="362.8538" type="appl" />
         <tli id="T573" time="363.5172" type="appl" />
         <tli id="T574" time="364.18059999999997" type="appl" />
         <tli id="T575" time="365.5747617243173" />
         <tli id="T576" time="365.77411419549196" type="intp" />
         <tli id="T577" time="365.97346666666664" type="appl" />
         <tli id="T578" time="366.5382" type="appl" />
         <tli id="T579" time="367.10293333333334" type="appl" />
         <tli id="T580" time="367.66766666666666" type="appl" />
         <tli id="T581" time="368.2324" type="appl" />
         <tli id="T582" time="368.7971333333333" type="appl" />
         <tli id="T583" time="369.3618666666667" type="appl" />
         <tli id="T584" time="369.9266" type="appl" />
         <tli id="T585" time="370.49133333333333" type="appl" />
         <tli id="T586" time="371.05606666666665" type="appl" />
         <tli id="T587" time="371.6208" type="appl" />
         <tli id="T588" time="372.18553333333335" type="appl" />
         <tli id="T589" time="372.7502666666667" type="appl" />
         <tli id="T590" time="373.315" type="appl" />
         <tli id="T591" time="373.72984615384615" type="appl" />
         <tli id="T592" time="374.1446923076923" type="appl" />
         <tli id="T593" time="374.55953846153847" type="appl" />
         <tli id="T594" time="374.9743846153846" type="appl" />
         <tli id="T595" time="375.3892307692308" type="appl" />
         <tli id="T596" time="375.80407692307693" type="appl" />
         <tli id="T597" time="376.2189230769231" type="appl" />
         <tli id="T598" time="376.63376923076925" type="appl" />
         <tli id="T599" time="377.0486153846154" type="appl" />
         <tli id="T600" time="377.46346153846156" type="appl" />
         <tli id="T601" time="377.8783076923077" type="appl" />
         <tli id="T602" time="378.29315384615387" type="appl" />
         <tli id="T603" time="378.8146761778823" />
         <tli id="T604" time="379.25980000000004" type="appl" />
         <tli id="T605" time="379.8116" type="appl" />
         <tli id="T606" time="380.3634" type="appl" />
         <tli id="T607" time="380.91519999999997" type="appl" />
         <tli id="T608" time="382.7078495597476" />
         <tli id="T609" time="383.0509247798738" type="intp" />
         <tli id="T610" time="383.394" type="appl" />
         <tli id="T611" time="384.3575" type="appl" />
         <tli id="T612" time="388.4677670266005" />
         <tli id="T613" time="389.21088351330025" type="intp" />
         <tli id="T614" time="389.954" type="appl" />
         <tli id="T615" time="390.45375" type="appl" />
         <tli id="T616" time="390.9535" type="appl" />
         <tli id="T617" time="391.45325" type="appl" />
         <tli id="T618" time="391.953" type="appl" />
         <tli id="T619" time="392.45275" type="appl" />
         <tli id="T620" time="392.9525" type="appl" />
         <tli id="T621" time="393.45225" type="appl" />
         <tli id="T622" time="394.1786747790409" />
         <tli id="T623" time="394.4577857142857" type="appl" />
         <tli id="T624" time="394.9635714285714" type="appl" />
         <tli id="T625" time="395.4693571428571" type="appl" />
         <tli id="T626" time="395.9751428571429" type="appl" />
         <tli id="T627" time="396.4809285714286" type="appl" />
         <tli id="T628" time="396.9867142857143" type="appl" />
         <tli id="T629" time="397.4925" type="appl" />
         <tli id="T630" time="397.9982857142857" type="appl" />
         <tli id="T631" time="398.5040714285714" type="appl" />
         <tli id="T632" time="399.00985714285713" type="appl" />
         <tli id="T633" time="399.5156428571429" type="appl" />
         <tli id="T634" time="400.0214285714286" type="appl" />
         <tli id="T635" time="400.5272142857143" type="appl" />
         <tli id="T636" time="401.8942413069728" />
         <tli id="T637" time="402.0886206534864" type="intp" />
         <tli id="T638" time="402.283" type="appl" />
         <tli id="T639" time="402.908" type="appl" />
         <tli id="T640" time="403.75421465564403" />
         <tli id="T641" time="404.10900000000004" type="appl" />
         <tli id="T642" time="404.685" type="appl" />
         <tli id="T643" time="405.26099999999997" type="appl" />
         <tli id="T644" time="406.1436595842489" />
         <tli id="T645" time="406.38529411764705" type="appl" />
         <tli id="T646" time="406.9335882352941" type="appl" />
         <tli id="T647" time="407.48188235294117" type="appl" />
         <tli id="T648" time="408.03017647058823" type="appl" />
         <tli id="T649" time="408.5784705882353" type="appl" />
         <tli id="T650" time="409.12676470588235" type="appl" />
         <tli id="T651" time="409.6750588235294" type="appl" />
         <tli id="T652" time="410.2233529411765" type="appl" />
         <tli id="T653" time="410.77164705882353" type="appl" />
         <tli id="T654" time="411.3199411764706" type="appl" />
         <tli id="T655" time="411.86823529411765" type="appl" />
         <tli id="T656" time="412.4165294117647" type="appl" />
         <tli id="T657" time="412.9648235294118" type="appl" />
         <tli id="T658" time="413.51311764705883" type="appl" />
         <tli id="T659" time="414.0614117647059" type="appl" />
         <tli id="T660" time="414.60970588235296" type="appl" />
         <tli id="T661" time="415.29800757939194" />
         <tli id="T662" time="415.5602" type="appl" />
         <tli id="T663" time="415.9624" type="appl" />
         <tli id="T664" time="416.3646" type="appl" />
         <tli id="T665" time="416.7668" type="appl" />
         <tli id="T666" time="417.169" type="appl" />
         <tli id="T667" time="417.202" type="appl" />
         <tli id="T668" time="418.29499588591614" />
         <tli id="T669" time="418.58579794295804" type="intp" />
         <tli id="T670" time="418.8766" type="appl" />
         <tli id="T671" time="419.5382" type="appl" />
         <tli id="T672" time="420.1998" type="appl" />
         <tli id="T673" time="420.8614" type="appl" />
         <tli id="T674" time="421.5429962207198" />
         <tli id="T675" time="422.322" type="appl" />
         <tli id="T676" time="423.1472701082771" />
         <tli id="T677" time="425.046125" type="appl" />
         <tli id="T678" time="425.67425000000003" type="appl" />
         <tli id="T679" time="426.302375" type="appl" />
         <tli id="T680" time="426.9305" type="appl" />
         <tli id="T681" time="427.558625" type="appl" />
         <tli id="T682" time="428.18674999999996" type="appl" />
         <tli id="T683" time="428.814875" type="appl" />
         <tli id="T684" time="429.9605058155961" />
         <tli id="T685" time="430.372" type="appl" />
         <tli id="T686" time="431.301" type="appl" />
         <tli id="T687" time="433.51378823438375" />
         <tli id="T688" time="433.65469411719187" type="intp" />
         <tli id="T689" time="433.79560000000004" type="appl" />
         <tli id="T690" time="434.5784" type="appl" />
         <tli id="T691" time="435.3612" type="appl" />
         <tli id="T692" time="436.2640092434727" />
         <tli id="T693" time="436.8548" type="appl" />
         <tli id="T694" time="437.5656" type="appl" />
         <tli id="T695" time="438.27639999999997" type="appl" />
         <tli id="T696" time="438.9872" type="appl" />
         <tli id="T697" time="440.4870216491154" />
         <tli id="T698" time="440.50702136254193" />
         <tli id="T699" time="441.05844444444443" type="appl" />
         <tli id="T700" time="441.73866666666663" type="appl" />
         <tli id="T701" time="442.4188888888889" type="appl" />
         <tli id="T702" time="443.0991111111111" type="appl" />
         <tli id="T703" time="443.77933333333334" type="appl" />
         <tli id="T704" time="444.45955555555554" type="appl" />
         <tli id="T705" time="445.1397777777778" type="appl" />
         <tli id="T706" time="445.82001814993276" />
         <tli id="T707" time="447.231" type="appl" />
         <tli id="T708" time="448.642" type="appl" />
         <tli id="T709" time="449.46975" type="appl" />
         <tli id="T710" time="450.2975" type="appl" />
         <tli id="T711" time="451.12525" type="appl" />
         <tli id="T712" time="451.89967061867554" />
         <tli id="T713" time="456.4867923903733" />
         <tli id="T714" time="457.041" type="appl" />
         <tli id="T715" time="459.1467542761074" />
         <tli id="T716" time="459.58666666666664" type="appl" />
         <tli id="T717" time="460.25533333333334" type="appl" />
         <tli id="T718" time="461.27432795703317" />
         <tli id="T719" time="461.4574973118499" type="intp" />
         <tli id="T720" time="461.6406666666667" type="appl" />
         <tli id="T721" time="462.3603333333333" type="appl" />
         <tli id="T722" time="463.08" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-KiPP"
                      id="tx-KiPP"
                      speaker="KiPP"
                      type="t">
         <timeline-fork end="T351" start="T350">
            <tli id="T350.tx-KiPP.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KiPP">
            <ts e="T0" id="Seg_0" n="sc" s="T1">
               <ts e="T0" id="Seg_2" n="HIAT:u" s="T1">
                  <nts id="Seg_3" n="HIAT:ip">–</nts>
                  <nts id="Seg_4" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_6" n="HIAT:w" s="T1">Tu͡oktan</ts>
                  <nts id="Seg_7" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_9" n="HIAT:w" s="T2">anʼiː</ts>
                  <nts id="Seg_10" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_12" n="HIAT:w" s="T3">diːgin</ts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0" id="Seg_15" n="HIAT:w" s="T4">du͡o</ts>
                  <nts id="Seg_16" n="HIAT:ip">.</nts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T61" id="Seg_18" n="sc" s="T5">
               <ts e="T12" id="Seg_20" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">Anʼiː</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">bu͡ollagɨna</ts>
                  <nts id="Seg_26" n="HIAT:ip">,</nts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_29" n="HIAT:w" s="T7">bɨlɨr</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_32" n="HIAT:w" s="T8">ojuttar</ts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_35" n="HIAT:w" s="T9">baːr</ts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">bu͡olaːččɨlar</ts>
                  <nts id="Seg_39" n="HIAT:ip">,</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_42" n="HIAT:w" s="T11">samaːttar</ts>
                  <nts id="Seg_43" n="HIAT:ip">.</nts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_46" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_48" n="HIAT:w" s="T12">Onnuktar</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_51" n="HIAT:w" s="T13">dʼi͡elerin</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_54" n="HIAT:w" s="T14">ergijerge</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_57" n="HIAT:w" s="T15">hatammat</ts>
                  <nts id="Seg_58" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_60" n="HIAT:w" s="T16">kuhagan</ts>
                  <nts id="Seg_61" n="HIAT:ip">.</nts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T24" id="Seg_64" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_66" n="HIAT:w" s="T17">Ergijbekke</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_69" n="HIAT:w" s="T18">hɨldʼɨ͡akkɨn</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_72" n="HIAT:w" s="T19">anʼiː</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_75" n="HIAT:w" s="T20">bu͡olaːččɨ</ts>
                  <nts id="Seg_76" n="HIAT:ip">,</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_79" n="HIAT:w" s="T21">ol</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_82" n="HIAT:w" s="T22">aːta</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_85" n="HIAT:w" s="T23">anʼiː</ts>
                  <nts id="Seg_86" n="HIAT:ip">.</nts>
                  <nts id="Seg_87" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T32" id="Seg_89" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_91" n="HIAT:w" s="T24">Dʼaktar</ts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_94" n="HIAT:w" s="T25">bu͡ollagɨna</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_97" n="HIAT:w" s="T26">er</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_100" n="HIAT:w" s="T27">kihi</ts>
                  <nts id="Seg_101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_103" n="HIAT:w" s="T28">taŋahɨn</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_106" n="HIAT:w" s="T29">tepser</ts>
                  <nts id="Seg_107" n="HIAT:ip">,</nts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_110" n="HIAT:w" s="T30">emi͡e</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_113" n="HIAT:w" s="T31">anʼiː</ts>
                  <nts id="Seg_114" n="HIAT:ip">.</nts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T39" id="Seg_117" n="HIAT:u" s="T32">
                  <ts e="T33" id="Seg_119" n="HIAT:w" s="T32">Čistej</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_122" n="HIAT:w" s="T33">bu͡olu͡ogun</ts>
                  <nts id="Seg_123" n="HIAT:ip">,</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_126" n="HIAT:w" s="T34">er</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_129" n="HIAT:w" s="T35">kihi</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_132" n="HIAT:w" s="T36">čeberdik</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_135" n="HIAT:w" s="T37">hɨldʼɨ͡agɨn</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_138" n="HIAT:w" s="T38">naːda</ts>
                  <nts id="Seg_139" n="HIAT:ip">.</nts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T46" id="Seg_142" n="HIAT:u" s="T39">
                  <ts e="T40" id="Seg_144" n="HIAT:w" s="T39">Dʼaktar</ts>
                  <nts id="Seg_145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_147" n="HIAT:w" s="T40">ɨrbaːkɨta</ts>
                  <nts id="Seg_148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_150" n="HIAT:w" s="T41">hu͡ok</ts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_153" n="HIAT:w" s="T42">hɨldʼar</ts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_156" n="HIAT:w" s="T43">emi͡e</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_159" n="HIAT:w" s="T44">anʼiː</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_162" n="HIAT:w" s="T45">urut</ts>
                  <nts id="Seg_163" n="HIAT:ip">.</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_166" n="HIAT:u" s="T46">
                  <ts e="T47" id="Seg_168" n="HIAT:w" s="T46">ɨrbaːkɨ</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_171" n="HIAT:w" s="T47">keti͡egin</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_174" n="HIAT:w" s="T48">naːda</ts>
                  <nts id="Seg_175" n="HIAT:ip">,</nts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_178" n="HIAT:w" s="T49">pɨlaːtije</ts>
                  <nts id="Seg_179" n="HIAT:ip">.</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_182" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_184" n="HIAT:w" s="T50">Er</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_187" n="HIAT:w" s="T51">kihi</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_190" n="HIAT:w" s="T52">kördük</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_192" n="HIAT:ip">(</nts>
                  <ts e="T54" id="Seg_194" n="HIAT:w" s="T53">hɨr-</ts>
                  <nts id="Seg_195" n="HIAT:ip">)</nts>
                  <nts id="Seg_196" n="HIAT:ip">,</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_199" n="HIAT:w" s="T54">anɨ</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_202" n="HIAT:w" s="T55">barɨkaːttara</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_205" n="HIAT:w" s="T56">hɨ͡alɨjalaːktar</ts>
                  <nts id="Seg_206" n="HIAT:ip">.</nts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T61" id="Seg_209" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_211" n="HIAT:w" s="T57">Iti</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_214" n="HIAT:w" s="T58">gri͡ex</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_217" n="HIAT:w" s="T59">bu͡o</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_220" n="HIAT:w" s="T60">ontuŋ</ts>
                  <nts id="Seg_221" n="HIAT:ip">.</nts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T723" id="Seg_223" n="sc" s="T66">
               <ts e="T84" id="Seg_225" n="HIAT:u" s="T66">
                  <nts id="Seg_226" n="HIAT:ip">–</nts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_229" n="HIAT:w" s="T66">Eː</ts>
                  <nts id="Seg_230" n="HIAT:ip">,</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_233" n="HIAT:w" s="T68">če</ts>
                  <nts id="Seg_234" n="HIAT:ip">,</nts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_237" n="HIAT:w" s="T69">barɨta</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_240" n="HIAT:w" s="T70">er</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_243" n="HIAT:w" s="T71">kihi͡eke</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_246" n="HIAT:w" s="T72">di͡eri</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_249" n="HIAT:w" s="T73">kuhagan</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_252" n="HIAT:w" s="T74">ol</ts>
                  <nts id="Seg_253" n="HIAT:ip">,</nts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_256" n="HIAT:w" s="T75">dʼaktar</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_259" n="HIAT:w" s="T76">dʼaktar</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_262" n="HIAT:w" s="T77">kördük</ts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_265" n="HIAT:w" s="T78">bu͡olu͡ogun</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_268" n="HIAT:w" s="T79">naːda</ts>
                  <nts id="Seg_269" n="HIAT:ip">,</nts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_272" n="HIAT:w" s="T80">ɨrbaːkɨlaːk</ts>
                  <nts id="Seg_273" n="HIAT:ip">,</nts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_276" n="HIAT:w" s="T81">pɨlaːttaːk</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_279" n="HIAT:w" s="T82">bu͡olu͡ogun</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_282" n="HIAT:w" s="T83">naːda</ts>
                  <nts id="Seg_283" n="HIAT:ip">.</nts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T723" id="Seg_286" n="HIAT:u" s="T84">
                  <ts e="T85" id="Seg_288" n="HIAT:w" s="T84">Bergeheni</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_291" n="HIAT:w" s="T85">baga</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_294" n="HIAT:w" s="T86">ketimne</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_297" n="HIAT:w" s="T87">onto</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_300" n="HIAT:w" s="T88">keti͡ekke</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_303" n="HIAT:w" s="T89">naːda</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_306" n="HIAT:w" s="T90">saːpkɨni</ts>
                  <nts id="Seg_307" n="HIAT:ip">.</nts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T724" id="Seg_309" n="sc" s="T91">
               <ts e="T93" id="Seg_311" n="HIAT:u" s="T91">
                  <ts e="T92" id="Seg_313" n="HIAT:w" s="T91">Iti</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_316" n="HIAT:w" s="T92">onnuktar</ts>
                  <nts id="Seg_317" n="HIAT:ip">.</nts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T100" id="Seg_320" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_322" n="HIAT:w" s="T93">Onton</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_325" n="HIAT:w" s="T94">erge</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_328" n="HIAT:w" s="T95">bararɨŋ</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_331" n="HIAT:w" s="T96">hagɨna</ts>
                  <nts id="Seg_332" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_334" n="HIAT:w" s="T97">iliː</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_337" n="HIAT:w" s="T98">oksohallar</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_340" n="HIAT:w" s="T99">taŋaraga</ts>
                  <nts id="Seg_341" n="HIAT:ip">.</nts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T724" id="Seg_344" n="HIAT:u" s="T100">
                  <ts e="T101" id="Seg_346" n="HIAT:w" s="T100">Itigirdik</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_349" n="HIAT:w" s="T101">bi͡ereller</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_352" n="HIAT:w" s="T102">iliːlerin</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_355" n="HIAT:w" s="T103">erge</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_358" n="HIAT:w" s="T104">barar</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_361" n="HIAT:w" s="T105">kihi</ts>
                  <nts id="Seg_362" n="HIAT:ip">.</nts>
                  <nts id="Seg_363" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T725" id="Seg_364" n="sc" s="T106">
               <ts e="T725" id="Seg_366" n="HIAT:u" s="T106">
                  <ts e="T107" id="Seg_368" n="HIAT:w" s="T106">Iliː</ts>
                  <nts id="Seg_369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_371" n="HIAT:w" s="T107">berseller</ts>
                  <nts id="Seg_372" n="HIAT:ip">,</nts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_375" n="HIAT:w" s="T108">oččogo</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_378" n="HIAT:w" s="T109">iliːlerin</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_381" n="HIAT:w" s="T110">bi͡erdekterine</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_384" n="HIAT:w" s="T111">dʼaktar</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_387" n="HIAT:w" s="T112">bu͡olar</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_390" n="HIAT:w" s="T113">ol</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_393" n="HIAT:w" s="T114">kihi</ts>
                  <nts id="Seg_394" n="HIAT:ip">.</nts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T154" id="Seg_396" n="sc" s="T115">
               <ts e="T118" id="Seg_398" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_400" n="HIAT:w" s="T115">Onnuk</ts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_403" n="HIAT:w" s="T116">abɨčajdar</ts>
                  <nts id="Seg_404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_406" n="HIAT:w" s="T117">onnuk</ts>
                  <nts id="Seg_407" n="HIAT:ip">.</nts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_410" n="HIAT:u" s="T118">
                  <ts e="T119" id="Seg_412" n="HIAT:w" s="T118">Heː</ts>
                  <nts id="Seg_413" n="HIAT:ip">,</nts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_416" n="HIAT:w" s="T119">küːske</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_419" n="HIAT:w" s="T120">bi͡ereller</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_422" n="HIAT:w" s="T121">bu͡o</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_425" n="HIAT:w" s="T122">kihini</ts>
                  <nts id="Seg_426" n="HIAT:ip">,</nts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_429" n="HIAT:w" s="T123">bagar</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_432" n="HIAT:w" s="T124">da</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_435" n="HIAT:w" s="T125">bagarɨma</ts>
                  <nts id="Seg_436" n="HIAT:ip">,</nts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_439" n="HIAT:w" s="T126">da</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_442" n="HIAT:w" s="T127">bi͡ereller</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_445" n="HIAT:w" s="T128">him</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_448" n="HIAT:w" s="T129">biːr</ts>
                  <nts id="Seg_449" n="HIAT:ip">.</nts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T134" id="Seg_452" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_454" n="HIAT:w" s="T130">ɨtɨː-ɨtɨː</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_457" n="HIAT:w" s="T131">bara</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_460" n="HIAT:w" s="T132">turu͡oŋ</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_463" n="HIAT:w" s="T133">onuga</ts>
                  <nts id="Seg_464" n="HIAT:ip">.</nts>
                  <nts id="Seg_465" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T141" id="Seg_467" n="HIAT:u" s="T134">
                  <ts e="T135" id="Seg_469" n="HIAT:w" s="T134">Ol</ts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_472" n="HIAT:w" s="T135">kördük</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_475" n="HIAT:w" s="T136">min</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_478" n="HIAT:w" s="T137">bara</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_481" n="HIAT:w" s="T138">turbutum</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_484" n="HIAT:w" s="T139">erge</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_487" n="HIAT:w" s="T140">ɨtɨː-ɨtɨːbɨn</ts>
                  <nts id="Seg_488" n="HIAT:ip">.</nts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_491" n="HIAT:u" s="T141">
                  <ts e="T142" id="Seg_493" n="HIAT:w" s="T141">U͡on</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_496" n="HIAT:w" s="T142">alta</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_499" n="HIAT:w" s="T143">dʼɨllaːkpar</ts>
                  <nts id="Seg_500" n="HIAT:ip">,</nts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_503" n="HIAT:w" s="T144">ke</ts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_506" n="HIAT:w" s="T145">tu͡ok</ts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_509" n="HIAT:w" s="T146">kihite</ts>
                  <nts id="Seg_510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_512" n="HIAT:w" s="T147">erge</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_515" n="HIAT:w" s="T148">barɨ͡aj</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_518" n="HIAT:w" s="T149">ol</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_521" n="HIAT:w" s="T150">u͡on</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_524" n="HIAT:w" s="T151">alta</ts>
                  <nts id="Seg_525" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_527" n="HIAT:w" s="T152">dʼɨllaːkka</ts>
                  <nts id="Seg_528" n="HIAT:ip">.</nts>
                  <nts id="Seg_529" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T154" id="Seg_531" n="HIAT:u" s="T153">
                  <ts e="T154" id="Seg_533" n="HIAT:w" s="T153">Bi͡erbittere</ts>
                  <nts id="Seg_534" n="HIAT:ip">.</nts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T726" id="Seg_536" n="sc" s="T156">
               <ts e="T161" id="Seg_538" n="HIAT:u" s="T156">
                  <nts id="Seg_539" n="HIAT:ip">–</nts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_542" n="HIAT:w" s="T156">Hapsi͡em</ts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_545" n="HIAT:w" s="T157">ogo</ts>
                  <nts id="Seg_546" n="HIAT:ip">,</nts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_549" n="HIAT:w" s="T158">ɨraːs</ts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_552" n="HIAT:w" s="T159">etim</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_555" n="HIAT:w" s="T160">haːtar</ts>
                  <nts id="Seg_556" n="HIAT:ip">.</nts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T165" id="Seg_559" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_561" n="HIAT:w" s="T161">Mʼesnaja</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_564" n="HIAT:w" s="T162">da</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_567" n="HIAT:w" s="T163">hu͡ok</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_570" n="HIAT:w" s="T164">etim</ts>
                  <nts id="Seg_571" n="HIAT:ip">.</nts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T172" id="Seg_574" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_576" n="HIAT:w" s="T165">Onton</ts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_579" n="HIAT:w" s="T166">iti</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_582" n="HIAT:w" s="T167">tagɨsta</ts>
                  <nts id="Seg_583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_585" n="HIAT:w" s="T168">ulakan</ts>
                  <nts id="Seg_586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_588" n="HIAT:w" s="T169">kɨːhɨm</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_591" n="HIAT:w" s="T170">anɨ</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_594" n="HIAT:w" s="T171">bu͡o</ts>
                  <nts id="Seg_595" n="HIAT:ip">.</nts>
                  <nts id="Seg_596" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T183" id="Seg_598" n="HIAT:u" s="T172">
                  <ts e="T173" id="Seg_600" n="HIAT:w" s="T172">Iti</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_603" n="HIAT:w" s="T173">iti</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_606" n="HIAT:w" s="T174">ogom</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_609" n="HIAT:w" s="T175">u͡on</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_612" n="HIAT:w" s="T176">hette</ts>
                  <nts id="Seg_613" n="HIAT:ip">,</nts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_616" n="HIAT:w" s="T177">u͡on</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_619" n="HIAT:w" s="T178">agɨs</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_622" n="HIAT:w" s="T179">dʼɨllaːkpar</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_625" n="HIAT:w" s="T180">töröːbüt</ts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_628" n="HIAT:w" s="T181">ogo</ts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_631" n="HIAT:w" s="T182">iti</ts>
                  <nts id="Seg_632" n="HIAT:ip">.</nts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T193" id="Seg_635" n="HIAT:u" s="T183">
                  <ts e="T184" id="Seg_637" n="HIAT:w" s="T183">Dʼe</ts>
                  <nts id="Seg_638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_640" n="HIAT:w" s="T184">onton</ts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_643" n="HIAT:w" s="T185">kɨrdʼan</ts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_646" n="HIAT:w" s="T186">kɨrdʼammɨn</ts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_649" n="HIAT:w" s="T187">iti</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_652" n="HIAT:w" s="T188">kojut</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_655" n="HIAT:w" s="T189">kojutun</ts>
                  <nts id="Seg_656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_658" n="HIAT:w" s="T190">onton</ts>
                  <nts id="Seg_659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_661" n="HIAT:w" s="T191">du͡o</ts>
                  <nts id="Seg_662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_663" n="HIAT:ip">(</nts>
                  <nts id="Seg_664" n="HIAT:ip">(</nts>
                  <ats e="T193" id="Seg_665" n="HIAT:non-pho" s="T192">LAUGH</ats>
                  <nts id="Seg_666" n="HIAT:ip">)</nts>
                  <nts id="Seg_667" n="HIAT:ip">)</nts>
                  <nts id="Seg_668" n="HIAT:ip">.</nts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T202" id="Seg_671" n="HIAT:u" s="T193">
                  <ts e="T194" id="Seg_673" n="HIAT:w" s="T193">Ogolonon</ts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_676" n="HIAT:w" s="T194">da</ts>
                  <nts id="Seg_677" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_679" n="HIAT:w" s="T195">ogolonon</ts>
                  <nts id="Seg_680" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_682" n="HIAT:w" s="T196">ihegin</ts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_685" n="HIAT:w" s="T197">bu͡o</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_688" n="HIAT:w" s="T198">lʼubovʼ</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_691" n="HIAT:w" s="T199">tʼomnaja</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_694" n="HIAT:w" s="T200">lʼubovʼ</ts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_696" n="HIAT:ip">(</nts>
                  <nts id="Seg_697" n="HIAT:ip">(</nts>
                  <ats e="T202" id="Seg_698" n="HIAT:non-pho" s="T201">LAUGH</ats>
                  <nts id="Seg_699" n="HIAT:ip">)</nts>
                  <nts id="Seg_700" n="HIAT:ip">)</nts>
                  <nts id="Seg_701" n="HIAT:ip">.</nts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T207" id="Seg_704" n="HIAT:u" s="T202">
                  <ts e="T203" id="Seg_706" n="HIAT:w" s="T202">Hürdeːk</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_709" n="HIAT:w" s="T203">üčügej</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_712" n="HIAT:w" s="T204">kihi</ts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_715" n="HIAT:w" s="T205">ete</ts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_718" n="HIAT:w" s="T206">erim</ts>
                  <nts id="Seg_719" n="HIAT:ip">.</nts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T726" id="Seg_722" n="HIAT:u" s="T207">
                  <ts e="T208" id="Seg_724" n="HIAT:w" s="T207">Bu</ts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_727" n="HIAT:w" s="T208">oloroːčču</ts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_730" n="HIAT:w" s="T209">kihi</ts>
                  <nts id="Seg_731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_733" n="HIAT:w" s="T210">iti</ts>
                  <nts id="Seg_734" n="HIAT:ip">.</nts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T727" id="Seg_736" n="sc" s="T211">
               <ts e="T218" id="Seg_738" n="HIAT:u" s="T211">
                  <ts e="T212" id="Seg_740" n="HIAT:w" s="T211">Iti</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_743" n="HIAT:w" s="T212">min</ts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_746" n="HIAT:w" s="T213">toguh</ts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_749" n="HIAT:w" s="T214">ogoloːk</ts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_751" n="HIAT:ip">(</nts>
                  <ts e="T216" id="Seg_753" n="HIAT:w" s="T215">erdineːgim</ts>
                  <nts id="Seg_754" n="HIAT:ip">)</nts>
                  <nts id="Seg_755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_757" n="HIAT:w" s="T216">bu͡o</ts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_760" n="HIAT:w" s="T217">össü͡ö</ts>
                  <nts id="Seg_761" n="HIAT:ip">.</nts>
                  <nts id="Seg_762" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T223" id="Seg_764" n="HIAT:u" s="T218">
                  <ts e="T219" id="Seg_766" n="HIAT:w" s="T218">Össü͡ö</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_769" n="HIAT:w" s="T219">da</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_772" n="HIAT:w" s="T220">maladoj</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_775" n="HIAT:w" s="T221">bu͡oltakpɨn</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_778" n="HIAT:w" s="T222">itinne</ts>
                  <nts id="Seg_779" n="HIAT:ip">.</nts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T727" id="Seg_782" n="HIAT:u" s="T223">
                  <ts e="T224" id="Seg_784" n="HIAT:w" s="T223">Če</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_787" n="HIAT:w" s="T224">onnuk</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_790" n="HIAT:w" s="T225">abɨčajdaːk</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_793" n="HIAT:w" s="T226">etilere</ts>
                  <nts id="Seg_794" n="HIAT:ip">,</nts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_796" n="HIAT:ip">"</nts>
                  <ts e="T228" id="Seg_798" n="HIAT:w" s="T227">onton</ts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_801" n="HIAT:w" s="T228">hin</ts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_804" n="HIAT:w" s="T229">olok</ts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_807" n="HIAT:w" s="T230">olorogun</ts>
                  <nts id="Seg_808" n="HIAT:ip">,</nts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_811" n="HIAT:w" s="T231">tulaːjaktarɨ</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_814" n="HIAT:w" s="T232">ahaːtɨŋ</ts>
                  <nts id="Seg_815" n="HIAT:ip">"</nts>
                  <nts id="Seg_816" n="HIAT:ip">,</nts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_819" n="HIAT:w" s="T233">di͡eččiler</ts>
                  <nts id="Seg_820" n="HIAT:ip">.</nts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T728" id="Seg_822" n="sc" s="T234">
               <ts e="T241" id="Seg_824" n="HIAT:u" s="T234">
                  <nts id="Seg_825" n="HIAT:ip">"</nts>
                  <ts e="T235" id="Seg_827" n="HIAT:w" s="T234">Tulaːjak</ts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_830" n="HIAT:w" s="T235">ogoloro</ts>
                  <nts id="Seg_831" n="HIAT:ip">,</nts>
                  <nts id="Seg_832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_834" n="HIAT:w" s="T236">anʼiː</ts>
                  <nts id="Seg_835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_837" n="HIAT:w" s="T237">bu͡olu͡o</ts>
                  <nts id="Seg_838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_840" n="HIAT:w" s="T238">ogoloro</ts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_843" n="HIAT:w" s="T239">ahaːtɨŋ</ts>
                  <nts id="Seg_844" n="HIAT:ip">"</nts>
                  <nts id="Seg_845" n="HIAT:ip">,</nts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_848" n="HIAT:w" s="T240">di͡eččiler</ts>
                  <nts id="Seg_849" n="HIAT:ip">.</nts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T247" id="Seg_852" n="HIAT:u" s="T241">
                  <nts id="Seg_853" n="HIAT:ip">"</nts>
                  <ts e="T242" id="Seg_855" n="HIAT:w" s="T241">Kuhagannɨk</ts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_858" n="HIAT:w" s="T242">gɨmmɨt</ts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_861" n="HIAT:w" s="T243">kihini</ts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_864" n="HIAT:w" s="T244">üčügejdik</ts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_867" n="HIAT:w" s="T245">ataːrɨŋ</ts>
                  <nts id="Seg_868" n="HIAT:ip">"</nts>
                  <nts id="Seg_869" n="HIAT:ip">,</nts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_872" n="HIAT:w" s="T246">di͡eččiler</ts>
                  <nts id="Seg_873" n="HIAT:ip">.</nts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_876" n="HIAT:u" s="T247">
                  <nts id="Seg_877" n="HIAT:ip">"</nts>
                  <ts e="T248" id="Seg_879" n="HIAT:w" s="T247">Töttörü</ts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_882" n="HIAT:w" s="T248">togo</ts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_885" n="HIAT:w" s="T249">bu͡olar</ts>
                  <nts id="Seg_886" n="HIAT:ip">"</nts>
                  <nts id="Seg_887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_889" n="HIAT:w" s="T250">di͡etekpine</ts>
                  <nts id="Seg_890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_892" n="HIAT:w" s="T251">anʼiː</ts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_895" n="HIAT:w" s="T252">bu͡olu͡o</ts>
                  <nts id="Seg_896" n="HIAT:ip">,</nts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_898" n="HIAT:ip">"</nts>
                  <ts e="T254" id="Seg_900" n="HIAT:w" s="T253">onton</ts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_903" n="HIAT:w" s="T254">bejeŋ</ts>
                  <nts id="Seg_904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_905" n="HIAT:ip">(</nts>
                  <ts e="T256" id="Seg_907" n="HIAT:w" s="T255">on-</ts>
                  <nts id="Seg_908" n="HIAT:ip">)</nts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_911" n="HIAT:w" s="T256">üčügejdik</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_914" n="HIAT:w" s="T257">olordokkuna</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_917" n="HIAT:w" s="T258">töttörü</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_920" n="HIAT:w" s="T259">bejeger</ts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_923" n="HIAT:w" s="T260">ɨmsɨːrɨ͡aktara</ts>
                  <nts id="Seg_924" n="HIAT:ip">"</nts>
                  <nts id="Seg_925" n="HIAT:ip">,</nts>
                  <nts id="Seg_926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_928" n="HIAT:w" s="T261">di͡eččiler</ts>
                  <nts id="Seg_929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_931" n="HIAT:w" s="T262">bɨlɨrgɨlar</ts>
                  <nts id="Seg_932" n="HIAT:ip">.</nts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T265" id="Seg_935" n="HIAT:u" s="T263">
                  <ts e="T264" id="Seg_937" n="HIAT:w" s="T263">Abɨčajdar</ts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_940" n="HIAT:w" s="T264">onnuk</ts>
                  <nts id="Seg_941" n="HIAT:ip">.</nts>
                  <nts id="Seg_942" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T276" id="Seg_944" n="HIAT:u" s="T265">
                  <nts id="Seg_945" n="HIAT:ip">"</nts>
                  <ts e="T266" id="Seg_947" n="HIAT:w" s="T265">Onton</ts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_950" n="HIAT:w" s="T266">dʼolloːk</ts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_953" n="HIAT:w" s="T267">bu͡olluŋ</ts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_956" n="HIAT:w" s="T268">bejeŋ</ts>
                  <nts id="Seg_957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_959" n="HIAT:w" s="T269">kojut</ts>
                  <nts id="Seg_960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_962" n="HIAT:w" s="T270">dʼolloːk</ts>
                  <nts id="Seg_963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_965" n="HIAT:w" s="T271">bu͡olluŋ</ts>
                  <nts id="Seg_966" n="HIAT:ip">"</nts>
                  <nts id="Seg_967" n="HIAT:ip">,</nts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_970" n="HIAT:w" s="T272">di͡ete</ts>
                  <nts id="Seg_971" n="HIAT:ip">,</nts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_974" n="HIAT:w" s="T273">min</ts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_977" n="HIAT:w" s="T274">anɨ</ts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_980" n="HIAT:w" s="T275">dʼolloːkpun</ts>
                  <nts id="Seg_981" n="HIAT:ip">.</nts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T281" id="Seg_984" n="HIAT:u" s="T276">
                  <ts e="T277" id="Seg_986" n="HIAT:w" s="T276">Ol</ts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_989" n="HIAT:w" s="T277">tulaːjak</ts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_992" n="HIAT:w" s="T278">etim</ts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_995" n="HIAT:w" s="T279">bu͡o</ts>
                  <nts id="Seg_996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_998" n="HIAT:w" s="T280">onton</ts>
                  <nts id="Seg_999" n="HIAT:ip">.</nts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T292" id="Seg_1002" n="HIAT:u" s="T281">
                  <ts e="T282" id="Seg_1004" n="HIAT:w" s="T281">Inʼem</ts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1007" n="HIAT:w" s="T282">ölbütüger</ts>
                  <nts id="Seg_1008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1010" n="HIAT:w" s="T283">erge</ts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_1013" n="HIAT:w" s="T284">bi͡erbitin</ts>
                  <nts id="Seg_1014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1016" n="HIAT:w" s="T285">kenne</ts>
                  <nts id="Seg_1017" n="HIAT:ip">,</nts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1020" n="HIAT:w" s="T286">eː</ts>
                  <nts id="Seg_1021" n="HIAT:ip">,</nts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1024" n="HIAT:w" s="T287">erge</ts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1027" n="HIAT:w" s="T288">bi͡ereligine</ts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1030" n="HIAT:w" s="T289">ɨ͡altan</ts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1033" n="HIAT:w" s="T290">ɨ͡alga</ts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1036" n="HIAT:w" s="T291">hɨldʼaːččɨbɨt</ts>
                  <nts id="Seg_1037" n="HIAT:ip">.</nts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T298" id="Seg_1040" n="HIAT:u" s="T292">
                  <ts e="T293" id="Seg_1042" n="HIAT:w" s="T292">Onno</ts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_1045" n="HIAT:w" s="T293">inʼem</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1048" n="HIAT:w" s="T294">inʼem</ts>
                  <nts id="Seg_1049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1051" n="HIAT:w" s="T295">ölörügör</ts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1054" n="HIAT:w" s="T296">innʼe</ts>
                  <nts id="Seg_1055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1057" n="HIAT:w" s="T297">diːr</ts>
                  <nts id="Seg_1058" n="HIAT:ip">:</nts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T303" id="Seg_1061" n="HIAT:u" s="T298">
                  <nts id="Seg_1062" n="HIAT:ip">"</nts>
                  <ts e="T299" id="Seg_1064" n="HIAT:w" s="T298">Bu͡o</ts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1067" n="HIAT:w" s="T299">kaja</ts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1070" n="HIAT:w" s="T300">tulaːjaktarɨ</ts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1073" n="HIAT:w" s="T301">ahataːr</ts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1076" n="HIAT:w" s="T302">bu͡ol</ts>
                  <nts id="Seg_1077" n="HIAT:ip">.</nts>
                  <nts id="Seg_1078" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T312" id="Seg_1080" n="HIAT:u" s="T303">
                  <ts e="T304" id="Seg_1082" n="HIAT:w" s="T303">Kuhagannɨk</ts>
                  <nts id="Seg_1083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1085" n="HIAT:w" s="T304">gɨmmɨtɨ</ts>
                  <nts id="Seg_1086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1088" n="HIAT:w" s="T305">üčügejdik</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1091" n="HIAT:w" s="T306">gɨnɨ͡akkɨn</ts>
                  <nts id="Seg_1092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1094" n="HIAT:w" s="T307">naːda</ts>
                  <nts id="Seg_1095" n="HIAT:ip">,</nts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1098" n="HIAT:w" s="T308">oččogo</ts>
                  <nts id="Seg_1099" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1101" n="HIAT:w" s="T309">uhun</ts>
                  <nts id="Seg_1102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1104" n="HIAT:w" s="T310">dʼolloːk</ts>
                  <nts id="Seg_1105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1107" n="HIAT:w" s="T311">bu͡olu͡oŋ</ts>
                  <nts id="Seg_1108" n="HIAT:ip">.</nts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T319" id="Seg_1111" n="HIAT:u" s="T312">
                  <ts e="T313" id="Seg_1113" n="HIAT:w" s="T312">Ügüs</ts>
                  <nts id="Seg_1114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1116" n="HIAT:w" s="T313">ogoloːk</ts>
                  <nts id="Seg_1117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1119" n="HIAT:w" s="T314">bu͡olu͡oŋ</ts>
                  <nts id="Seg_1120" n="HIAT:ip">,</nts>
                  <nts id="Seg_1121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1123" n="HIAT:w" s="T315">belem</ts>
                  <nts id="Seg_1124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1126" n="HIAT:w" s="T316">olokko</ts>
                  <nts id="Seg_1127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1129" n="HIAT:w" s="T317">oloru͡oŋ</ts>
                  <nts id="Seg_1130" n="HIAT:ip">"</nts>
                  <nts id="Seg_1131" n="HIAT:ip">,</nts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1134" n="HIAT:w" s="T318">di͡ebite</ts>
                  <nts id="Seg_1135" n="HIAT:ip">.</nts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T321" id="Seg_1138" n="HIAT:u" s="T319">
                  <ts e="T320" id="Seg_1140" n="HIAT:w" s="T319">Ile</ts>
                  <nts id="Seg_1141" n="HIAT:ip">,</nts>
                  <nts id="Seg_1142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1144" n="HIAT:w" s="T320">kör</ts>
                  <nts id="Seg_1145" n="HIAT:ip">.</nts>
                  <nts id="Seg_1146" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T331" id="Seg_1148" n="HIAT:u" s="T321">
                  <ts e="T322" id="Seg_1150" n="HIAT:w" s="T321">Anɨ</ts>
                  <nts id="Seg_1151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1153" n="HIAT:w" s="T322">min</ts>
                  <nts id="Seg_1154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1156" n="HIAT:w" s="T323">bejem</ts>
                  <nts id="Seg_1157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1159" n="HIAT:w" s="T324">bu͡ollagɨna</ts>
                  <nts id="Seg_1160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1162" n="HIAT:w" s="T325">bi͡eh-u͡ontan</ts>
                  <nts id="Seg_1163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1165" n="HIAT:w" s="T326">taksa</ts>
                  <nts id="Seg_1166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1168" n="HIAT:w" s="T327">kihibin</ts>
                  <nts id="Seg_1169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1171" n="HIAT:w" s="T328">bejem</ts>
                  <nts id="Seg_1172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1174" n="HIAT:w" s="T329">ispitten</ts>
                  <nts id="Seg_1175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1177" n="HIAT:w" s="T330">taksɨbɨtɨm</ts>
                  <nts id="Seg_1178" n="HIAT:ip">.</nts>
                  <nts id="Seg_1179" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T332" id="Seg_1181" n="HIAT:u" s="T331">
                  <ts e="T332" id="Seg_1183" n="HIAT:w" s="T331">Vɨnuktardɨːmmɨn</ts>
                  <nts id="Seg_1184" n="HIAT:ip">.</nts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T335" id="Seg_1187" n="HIAT:u" s="T332">
                  <ts e="T333" id="Seg_1189" n="HIAT:w" s="T332">Pʼitdʼesʼat</ts>
                  <nts id="Seg_1190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1192" n="HIAT:w" s="T333">s</ts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1195" n="HIAT:w" s="T334">lʼiska</ts>
                  <nts id="Seg_1196" n="HIAT:ip">.</nts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T338" id="Seg_1199" n="HIAT:u" s="T335">
                  <ts e="T336" id="Seg_1201" n="HIAT:w" s="T335">Naru͡odum</ts>
                  <nts id="Seg_1202" n="HIAT:ip">,</nts>
                  <nts id="Seg_1203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1205" n="HIAT:w" s="T336">bejem</ts>
                  <nts id="Seg_1206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1208" n="HIAT:w" s="T337">naru͡odum</ts>
                  <nts id="Seg_1209" n="HIAT:ip">.</nts>
                  <nts id="Seg_1210" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T341" id="Seg_1212" n="HIAT:u" s="T338">
                  <ts e="T339" id="Seg_1214" n="HIAT:w" s="T338">Hɨndaːska</ts>
                  <nts id="Seg_1215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1217" n="HIAT:w" s="T339">teŋ</ts>
                  <nts id="Seg_1218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1220" n="HIAT:w" s="T340">aŋara</ts>
                  <nts id="Seg_1221" n="HIAT:ip">.</nts>
                  <nts id="Seg_1222" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T347" id="Seg_1224" n="HIAT:u" s="T341">
                  <ts e="T342" id="Seg_1226" n="HIAT:w" s="T341">Aŋara</ts>
                  <nts id="Seg_1227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1229" n="HIAT:w" s="T342">ɨraːk</ts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1232" n="HIAT:w" s="T343">baːllar</ts>
                  <nts id="Seg_1233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1235" n="HIAT:w" s="T344">össü͡ö</ts>
                  <nts id="Seg_1236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1238" n="HIAT:w" s="T345">ol</ts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1241" n="HIAT:w" s="T346">di͡ek</ts>
                  <nts id="Seg_1242" n="HIAT:ip">.</nts>
                  <nts id="Seg_1243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T351" id="Seg_1245" n="HIAT:u" s="T347">
                  <ts e="T348" id="Seg_1247" n="HIAT:w" s="T347">Mannagɨm</ts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1250" n="HIAT:w" s="T348">ere</ts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1253" n="HIAT:w" s="T349">de</ts>
                  <nts id="Seg_1254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350.tx-KiPP.1" id="Seg_1256" n="HIAT:w" s="T350">hɨlaː</ts>
                  <nts id="Seg_1257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1259" n="HIAT:w" s="T350.tx-KiPP.1">bog</ts>
                  <nts id="Seg_1260" n="HIAT:ip">.</nts>
                  <nts id="Seg_1261" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T358" id="Seg_1263" n="HIAT:u" s="T351">
                  <ts e="T352" id="Seg_1265" n="HIAT:w" s="T351">Manna</ts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1268" n="HIAT:w" s="T352">ere</ts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1271" n="HIAT:w" s="T353">baːr</ts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1274" n="HIAT:w" s="T354">bi͡eh-u͡ontan</ts>
                  <nts id="Seg_1275" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1277" n="HIAT:w" s="T355">taksa</ts>
                  <nts id="Seg_1278" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1280" n="HIAT:w" s="T356">kihibit</ts>
                  <nts id="Seg_1281" n="HIAT:ip">,</nts>
                  <nts id="Seg_1282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1284" n="HIAT:w" s="T357">ogolorduːn</ts>
                  <nts id="Seg_1285" n="HIAT:ip">.</nts>
                  <nts id="Seg_1286" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T728" id="Seg_1288" n="HIAT:u" s="T358">
                  <ts e="T359" id="Seg_1290" n="HIAT:w" s="T358">Pačʼtʼi</ts>
                  <nts id="Seg_1291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1293" n="HIAT:w" s="T359">Sɨndasska</ts>
                  <nts id="Seg_1294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1295" n="HIAT:ip">(</nts>
                  <ts e="T361" id="Seg_1297" n="HIAT:w" s="T360">kaltak</ts>
                  <nts id="Seg_1298" n="HIAT:ip">)</nts>
                  <nts id="Seg_1299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_1301" n="HIAT:w" s="T361">kajan</ts>
                  <nts id="Seg_1302" n="HIAT:ip">.</nts>
                  <nts id="Seg_1303" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T419" id="Seg_1304" n="sc" s="T362">
               <ts e="T364" id="Seg_1306" n="HIAT:u" s="T362">
                  <ts e="T363" id="Seg_1308" n="HIAT:w" s="T362">Hɨndaːska</ts>
                  <nts id="Seg_1309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1311" n="HIAT:w" s="T363">aŋaram</ts>
                  <nts id="Seg_1312" n="HIAT:ip">.</nts>
                  <nts id="Seg_1313" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T371" id="Seg_1315" n="HIAT:u" s="T364">
                  <ts e="T365" id="Seg_1317" n="HIAT:w" s="T364">Tak</ts>
                  <nts id="Seg_1318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1320" n="HIAT:w" s="T365">da</ts>
                  <nts id="Seg_1321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1323" n="HIAT:w" s="T366">üčügejdik</ts>
                  <nts id="Seg_1324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1326" n="HIAT:w" s="T367">olorobun</ts>
                  <nts id="Seg_1327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1329" n="HIAT:w" s="T368">bertteːk</ts>
                  <nts id="Seg_1330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1332" n="HIAT:w" s="T369">kihiler</ts>
                  <nts id="Seg_1333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1335" n="HIAT:w" s="T370">algɨstarɨgar</ts>
                  <nts id="Seg_1336" n="HIAT:ip">.</nts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T375" id="Seg_1339" n="HIAT:u" s="T371">
                  <ts e="T372" id="Seg_1341" n="HIAT:w" s="T371">Kihiler</ts>
                  <nts id="Seg_1342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1344" n="HIAT:w" s="T372">algɨstara</ts>
                  <nts id="Seg_1345" n="HIAT:ip">,</nts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1348" n="HIAT:w" s="T373">kɨrdʼagastar</ts>
                  <nts id="Seg_1349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1351" n="HIAT:w" s="T374">algɨstara</ts>
                  <nts id="Seg_1352" n="HIAT:ip">.</nts>
                  <nts id="Seg_1353" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T381" id="Seg_1355" n="HIAT:u" s="T375">
                  <ts e="T376" id="Seg_1357" n="HIAT:w" s="T375">Anɨ</ts>
                  <nts id="Seg_1358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1360" n="HIAT:w" s="T376">bu͡o</ts>
                  <nts id="Seg_1361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1363" n="HIAT:w" s="T377">kɨrdʼammɨn</ts>
                  <nts id="Seg_1364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1366" n="HIAT:w" s="T378">ɨ͡aldʼabɨn</ts>
                  <nts id="Seg_1367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1369" n="HIAT:w" s="T379">ere</ts>
                  <nts id="Seg_1370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1372" n="HIAT:w" s="T380">bu͡o</ts>
                  <nts id="Seg_1373" n="HIAT:ip">.</nts>
                  <nts id="Seg_1374" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T382" id="Seg_1376" n="HIAT:u" s="T381">
                  <ts e="T382" id="Seg_1378" n="HIAT:w" s="T381">Davlʼenʼijalaːppɨn</ts>
                  <nts id="Seg_1379" n="HIAT:ip">.</nts>
                  <nts id="Seg_1380" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T392" id="Seg_1382" n="HIAT:u" s="T382">
                  <ts e="T383" id="Seg_1384" n="HIAT:w" s="T382">Hürdeːk</ts>
                  <nts id="Seg_1385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1387" n="HIAT:w" s="T383">ulakan</ts>
                  <nts id="Seg_1388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1390" n="HIAT:w" s="T384">ikki</ts>
                  <nts id="Seg_1391" n="HIAT:ip">,</nts>
                  <nts id="Seg_1392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1394" n="HIAT:w" s="T386">heː</ts>
                  <nts id="Seg_1395" n="HIAT:ip">,</nts>
                  <nts id="Seg_1396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1398" n="HIAT:w" s="T388">ikki</ts>
                  <nts id="Seg_1399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1401" n="HIAT:w" s="T390">hüːsten</ts>
                  <nts id="Seg_1402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1404" n="HIAT:w" s="T391">taksar</ts>
                  <nts id="Seg_1405" n="HIAT:ip">.</nts>
                  <nts id="Seg_1406" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T395" id="Seg_1408" n="HIAT:u" s="T392">
                  <nts id="Seg_1409" n="HIAT:ip">–</nts>
                  <nts id="Seg_1410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1412" n="HIAT:w" s="T392">Dvacat</ts>
                  <nts id="Seg_1413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1415" n="HIAT:w" s="T393">dva</ts>
                  <nts id="Seg_1416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1418" n="HIAT:w" s="T394">bu͡olar</ts>
                  <nts id="Seg_1419" n="HIAT:ip">.</nts>
                  <nts id="Seg_1420" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T400" id="Seg_1422" n="HIAT:u" s="T395">
                  <ts e="T396" id="Seg_1424" n="HIAT:w" s="T395">Ontubun</ts>
                  <nts id="Seg_1425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1427" n="HIAT:w" s="T396">koppoppun</ts>
                  <nts id="Seg_1428" n="HIAT:ip">,</nts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1431" n="HIAT:w" s="T397">onton</ts>
                  <nts id="Seg_1432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1434" n="HIAT:w" s="T398">ataktarɨm</ts>
                  <nts id="Seg_1435" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1437" n="HIAT:w" s="T399">kaːjallar</ts>
                  <nts id="Seg_1438" n="HIAT:ip">.</nts>
                  <nts id="Seg_1439" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T409" id="Seg_1441" n="HIAT:u" s="T400">
                  <ts e="T401" id="Seg_1443" n="HIAT:w" s="T400">Anɨ</ts>
                  <nts id="Seg_1444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1446" n="HIAT:w" s="T401">biːr</ts>
                  <nts id="Seg_1447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1449" n="HIAT:w" s="T402">da</ts>
                  <nts id="Seg_1450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1452" n="HIAT:w" s="T403">ogogo</ts>
                  <nts id="Seg_1453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1455" n="HIAT:w" s="T404">koton</ts>
                  <nts id="Seg_1456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1458" n="HIAT:w" s="T405">tiːjbeppin</ts>
                  <nts id="Seg_1459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1461" n="HIAT:w" s="T406">bu͡o</ts>
                  <nts id="Seg_1462" n="HIAT:ip">,</nts>
                  <nts id="Seg_1463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1465" n="HIAT:w" s="T407">hɨtabɨn</ts>
                  <nts id="Seg_1466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1468" n="HIAT:w" s="T408">bu͡o</ts>
                  <nts id="Seg_1469" n="HIAT:ip">.</nts>
                  <nts id="Seg_1470" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T417" id="Seg_1472" n="HIAT:u" s="T409">
                  <ts e="T410" id="Seg_1474" n="HIAT:w" s="T409">Skoro</ts>
                  <nts id="Seg_1475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1477" n="HIAT:w" s="T410">umru</ts>
                  <nts id="Seg_1478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1480" n="HIAT:w" s="T411">navʼerna</ts>
                  <nts id="Seg_1481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1482" n="HIAT:ip">(</nts>
                  <nts id="Seg_1483" n="HIAT:ip">(</nts>
                  <ats e="T413" id="Seg_1484" n="HIAT:non-pho" s="T412">LAUGH</ats>
                  <nts id="Seg_1485" n="HIAT:ip">)</nts>
                  <nts id="Seg_1486" n="HIAT:ip">)</nts>
                  <nts id="Seg_1487" n="HIAT:ip">,</nts>
                  <nts id="Seg_1488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1490" n="HIAT:w" s="T413">nʼe</ts>
                  <nts id="Seg_1491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1493" n="HIAT:w" s="T414">bojusʼ</ts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1496" n="HIAT:w" s="T415">ja</ts>
                  <nts id="Seg_1497" n="HIAT:ip">.</nts>
                  <nts id="Seg_1498" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T419" id="Seg_1500" n="HIAT:u" s="T417">
                  <nts id="Seg_1501" n="HIAT:ip">–</nts>
                  <nts id="Seg_1502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1504" n="HIAT:w" s="T417">Nʼet</ts>
                  <nts id="Seg_1505" n="HIAT:ip">.</nts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T424" id="Seg_1507" n="sc" s="T422">
               <ts e="T424" id="Seg_1509" n="HIAT:u" s="T422">
                  <nts id="Seg_1510" n="HIAT:ip">–</nts>
                  <nts id="Seg_1511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1513" n="HIAT:w" s="T422">Arajga</ts>
                  <nts id="Seg_1514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1516" n="HIAT:w" s="T423">barɨ͡am</ts>
                  <nts id="Seg_1517" n="HIAT:ip">.</nts>
                  <nts id="Seg_1518" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T436" id="Seg_1519" n="sc" s="T428">
               <ts e="T432" id="Seg_1521" n="HIAT:u" s="T428">
                  <nts id="Seg_1522" n="HIAT:ip">–</nts>
                  <nts id="Seg_1523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1525" n="HIAT:w" s="T428">E</ts>
                  <nts id="Seg_1526" n="HIAT:ip">,</nts>
                  <nts id="Seg_1527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1529" n="HIAT:w" s="T430">arajga</ts>
                  <nts id="Seg_1530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1532" n="HIAT:w" s="T431">barɨ͡am</ts>
                  <nts id="Seg_1533" n="HIAT:ip">.</nts>
                  <nts id="Seg_1534" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T436" id="Seg_1536" n="HIAT:u" s="T432">
                  <ts e="T433" id="Seg_1538" n="HIAT:w" s="T432">Ogolorum</ts>
                  <nts id="Seg_1539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1541" n="HIAT:w" s="T433">elbekter</ts>
                  <nts id="Seg_1542" n="HIAT:ip">,</nts>
                  <nts id="Seg_1543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1545" n="HIAT:w" s="T434">arajga</ts>
                  <nts id="Seg_1546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1548" n="HIAT:w" s="T435">kötütü͡öktere</ts>
                  <nts id="Seg_1549" n="HIAT:ip">.</nts>
                  <nts id="Seg_1550" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T443" id="Seg_1551" n="sc" s="T439">
               <ts e="T441" id="Seg_1553" n="HIAT:u" s="T439">
                  <nts id="Seg_1554" n="HIAT:ip">–</nts>
                  <nts id="Seg_1555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1557" n="HIAT:w" s="T439">Taŋaraga</ts>
                  <nts id="Seg_1558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1560" n="HIAT:w" s="T440">ke</ts>
                  <nts id="Seg_1561" n="HIAT:ip">.</nts>
                  <nts id="Seg_1562" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T443" id="Seg_1564" n="HIAT:u" s="T441">
                  <ts e="T442" id="Seg_1566" n="HIAT:w" s="T441">Taŋaraga</ts>
                  <nts id="Seg_1567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1569" n="HIAT:w" s="T442">barɨ͡am</ts>
                  <nts id="Seg_1570" n="HIAT:ip">.</nts>
                  <nts id="Seg_1571" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T455" id="Seg_1572" n="sc" s="T447">
               <ts e="T451" id="Seg_1574" n="HIAT:u" s="T447">
                  <nts id="Seg_1575" n="HIAT:ip">–</nts>
                  <nts id="Seg_1576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1577" n="HIAT:ip">(</nts>
                  <nts id="Seg_1578" n="HIAT:ip">(</nts>
                  <ats e="T448" id="Seg_1579" n="HIAT:non-pho" s="T447">LAUGH</ats>
                  <nts id="Seg_1580" n="HIAT:ip">)</nts>
                  <nts id="Seg_1581" n="HIAT:ip">)</nts>
                  <nts id="Seg_1582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1584" n="HIAT:w" s="T448">Ješʼo</ts>
                  <nts id="Seg_1585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1587" n="HIAT:w" s="T449">nʼe</ts>
                  <nts id="Seg_1588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1590" n="HIAT:w" s="T450">znaju</ts>
                  <nts id="Seg_1591" n="HIAT:ip">.</nts>
                  <nts id="Seg_1592" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T453" id="Seg_1594" n="HIAT:u" s="T451">
                  <ts e="T452" id="Seg_1596" n="HIAT:w" s="T451">Di͡eččiler</ts>
                  <nts id="Seg_1597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1599" n="HIAT:w" s="T452">kɨrdʼagastar</ts>
                  <nts id="Seg_1600" n="HIAT:ip">:</nts>
                  <nts id="Seg_1601" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T455" id="Seg_1603" n="HIAT:u" s="T453">
                  <nts id="Seg_1604" n="HIAT:ip">"</nts>
                  <ts e="T454" id="Seg_1606" n="HIAT:w" s="T453">Taŋaraga</ts>
                  <nts id="Seg_1607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1609" n="HIAT:w" s="T454">barɨ͡akkɨt</ts>
                  <nts id="Seg_1610" n="HIAT:ip">.</nts>
                  <nts id="Seg_1611" n="HIAT:ip">"</nts>
                  <nts id="Seg_1612" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T489" id="Seg_1613" n="sc" s="T456">
               <ts e="T461" id="Seg_1615" n="HIAT:u" s="T456">
                  <ts e="T457" id="Seg_1617" n="HIAT:w" s="T456">Ügüs</ts>
                  <nts id="Seg_1618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1620" n="HIAT:w" s="T457">ogoloːk</ts>
                  <nts id="Seg_1621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1623" n="HIAT:w" s="T458">kihi</ts>
                  <nts id="Seg_1624" n="HIAT:ip">,</nts>
                  <nts id="Seg_1625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1627" n="HIAT:w" s="T459">taŋaraga</ts>
                  <nts id="Seg_1628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1630" n="HIAT:w" s="T460">barɨ͡am</ts>
                  <nts id="Seg_1631" n="HIAT:ip">.</nts>
                  <nts id="Seg_1632" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T464" id="Seg_1634" n="HIAT:u" s="T461">
                  <ts e="T462" id="Seg_1636" n="HIAT:w" s="T461">Kihini</ts>
                  <nts id="Seg_1637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1639" n="HIAT:w" s="T462">kuhagannɨk</ts>
                  <nts id="Seg_1640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1642" n="HIAT:w" s="T463">haŋarbappɨn</ts>
                  <nts id="Seg_1643" n="HIAT:ip">.</nts>
                  <nts id="Seg_1644" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T466" id="Seg_1646" n="HIAT:u" s="T464">
                  <ts e="T465" id="Seg_1648" n="HIAT:w" s="T464">Kihini</ts>
                  <nts id="Seg_1649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1651" n="HIAT:w" s="T465">kɨraspappɨn</ts>
                  <nts id="Seg_1652" n="HIAT:ip">.</nts>
                  <nts id="Seg_1653" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T468" id="Seg_1655" n="HIAT:u" s="T466">
                  <ts e="T467" id="Seg_1657" n="HIAT:w" s="T466">Anʼiː</ts>
                  <nts id="Seg_1658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1660" n="HIAT:w" s="T467">bu͡olu͡o</ts>
                  <nts id="Seg_1661" n="HIAT:ip">.</nts>
                  <nts id="Seg_1662" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T477" id="Seg_1664" n="HIAT:u" s="T468">
                  <ts e="T469" id="Seg_1666" n="HIAT:w" s="T468">Ol</ts>
                  <nts id="Seg_1667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1669" n="HIAT:w" s="T469">ihin</ts>
                  <nts id="Seg_1670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1672" n="HIAT:w" s="T470">min</ts>
                  <nts id="Seg_1673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1675" n="HIAT:w" s="T471">kihini</ts>
                  <nts id="Seg_1676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1678" n="HIAT:w" s="T472">kɨraha</ts>
                  <nts id="Seg_1679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1681" n="HIAT:w" s="T473">hataːččɨta</ts>
                  <nts id="Seg_1682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1684" n="HIAT:w" s="T474">hu͡okpun</ts>
                  <nts id="Seg_1685" n="HIAT:ip">,</nts>
                  <nts id="Seg_1686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1688" n="HIAT:w" s="T475">anʼiː</ts>
                  <nts id="Seg_1689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1691" n="HIAT:w" s="T476">bu͡olu͡o</ts>
                  <nts id="Seg_1692" n="HIAT:ip">.</nts>
                  <nts id="Seg_1693" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T481" id="Seg_1695" n="HIAT:u" s="T477">
                  <ts e="T478" id="Seg_1697" n="HIAT:w" s="T477">Kim</ts>
                  <nts id="Seg_1698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1700" n="HIAT:w" s="T478">kiːrbit</ts>
                  <nts id="Seg_1701" n="HIAT:ip">,</nts>
                  <nts id="Seg_1702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1704" n="HIAT:w" s="T479">ahatan</ts>
                  <nts id="Seg_1705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1707" n="HIAT:w" s="T480">ihebin</ts>
                  <nts id="Seg_1708" n="HIAT:ip">.</nts>
                  <nts id="Seg_1709" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T484" id="Seg_1711" n="HIAT:u" s="T481">
                  <ts e="T482" id="Seg_1713" n="HIAT:w" s="T481">Etim</ts>
                  <nts id="Seg_1714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1716" n="HIAT:w" s="T482">kotunar</ts>
                  <nts id="Seg_1717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1719" n="HIAT:w" s="T483">erdekpine</ts>
                  <nts id="Seg_1720" n="HIAT:ip">.</nts>
                  <nts id="Seg_1721" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T489" id="Seg_1723" n="HIAT:u" s="T484">
                  <ts e="T485" id="Seg_1725" n="HIAT:w" s="T484">Anɨ</ts>
                  <nts id="Seg_1726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1728" n="HIAT:w" s="T485">kotummappɨn</ts>
                  <nts id="Seg_1729" n="HIAT:ip">,</nts>
                  <nts id="Seg_1730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1732" n="HIAT:w" s="T486">Dunʼasam</ts>
                  <nts id="Seg_1733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1735" n="HIAT:w" s="T487">ere</ts>
                  <nts id="Seg_1736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1738" n="HIAT:w" s="T488">ebit</ts>
                  <nts id="Seg_1739" n="HIAT:ip">.</nts>
                  <nts id="Seg_1740" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T536" id="Seg_1741" n="sc" s="T490">
               <ts e="T491" id="Seg_1743" n="HIAT:u" s="T490">
                  <nts id="Seg_1744" n="HIAT:ip">–</nts>
                  <nts id="Seg_1745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1747" n="HIAT:w" s="T490">Üčügej</ts>
                  <nts id="Seg_1748" n="HIAT:ip">.</nts>
                  <nts id="Seg_1749" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T495" id="Seg_1751" n="HIAT:u" s="T491">
                  <ts e="T492" id="Seg_1753" n="HIAT:w" s="T491">Kiniːtim</ts>
                  <nts id="Seg_1754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1756" n="HIAT:w" s="T492">hürdeːk</ts>
                  <nts id="Seg_1757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1759" n="HIAT:w" s="T493">üčügej</ts>
                  <nts id="Seg_1760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1762" n="HIAT:w" s="T494">dʼaktar</ts>
                  <nts id="Seg_1763" n="HIAT:ip">.</nts>
                  <nts id="Seg_1764" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T500" id="Seg_1766" n="HIAT:u" s="T495">
                  <ts e="T496" id="Seg_1768" n="HIAT:w" s="T495">Üčügej</ts>
                  <nts id="Seg_1769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1771" n="HIAT:w" s="T496">dʼaktattar</ts>
                  <nts id="Seg_1772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1774" n="HIAT:w" s="T497">kiniːtterim</ts>
                  <nts id="Seg_1775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1777" n="HIAT:w" s="T498">tak</ts>
                  <nts id="Seg_1778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1780" n="HIAT:w" s="T499">da</ts>
                  <nts id="Seg_1781" n="HIAT:ip">.</nts>
                  <nts id="Seg_1782" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T507" id="Seg_1784" n="HIAT:u" s="T500">
                  <ts e="T501" id="Seg_1786" n="HIAT:w" s="T500">Ol</ts>
                  <nts id="Seg_1787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1789" n="HIAT:w" s="T501">kajdak</ts>
                  <nts id="Seg_1790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1792" n="HIAT:w" s="T502">da</ts>
                  <nts id="Seg_1793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1795" n="HIAT:w" s="T503">bu͡ollun</ts>
                  <nts id="Seg_1796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1798" n="HIAT:w" s="T504">min</ts>
                  <nts id="Seg_1799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1801" n="HIAT:w" s="T505">kɨhallɨbappɨn</ts>
                  <nts id="Seg_1802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1804" n="HIAT:w" s="T506">ginilerge</ts>
                  <nts id="Seg_1805" n="HIAT:ip">.</nts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T517" id="Seg_1808" n="HIAT:u" s="T507">
                  <ts e="T508" id="Seg_1810" n="HIAT:w" s="T507">Keristinner</ts>
                  <nts id="Seg_1811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1813" n="HIAT:w" s="T508">da</ts>
                  <nts id="Seg_1814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1816" n="HIAT:w" s="T509">kersibetinner</ts>
                  <nts id="Seg_1817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1819" n="HIAT:w" s="T510">da</ts>
                  <nts id="Seg_1820" n="HIAT:ip">,</nts>
                  <nts id="Seg_1821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1823" n="HIAT:w" s="T511">mini͡eke</ts>
                  <nts id="Seg_1824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1826" n="HIAT:w" s="T512">naːdata</ts>
                  <nts id="Seg_1827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1829" n="HIAT:w" s="T513">hu͡ok</ts>
                  <nts id="Seg_1830" n="HIAT:ip">,</nts>
                  <nts id="Seg_1831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1833" n="HIAT:w" s="T514">tuspa</ts>
                  <nts id="Seg_1834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1836" n="HIAT:w" s="T515">ɨ͡allar</ts>
                  <nts id="Seg_1837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1839" n="HIAT:w" s="T516">bu͡o</ts>
                  <nts id="Seg_1840" n="HIAT:ip">.</nts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T520" id="Seg_1843" n="HIAT:u" s="T517">
                  <ts e="T518" id="Seg_1845" n="HIAT:w" s="T517">Tu͡okka</ts>
                  <nts id="Seg_1846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1848" n="HIAT:w" s="T518">kɨhallɨ͡amɨj</ts>
                  <nts id="Seg_1849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1851" n="HIAT:w" s="T519">onuga</ts>
                  <nts id="Seg_1852" n="HIAT:ip">?</nts>
                  <nts id="Seg_1853" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T527" id="Seg_1855" n="HIAT:u" s="T520">
                  <ts e="T521" id="Seg_1857" n="HIAT:w" s="T520">Min</ts>
                  <nts id="Seg_1858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1860" n="HIAT:w" s="T521">ologum</ts>
                  <nts id="Seg_1861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1863" n="HIAT:w" s="T522">büppüte</ts>
                  <nts id="Seg_1864" n="HIAT:ip">,</nts>
                  <nts id="Seg_1865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1867" n="HIAT:w" s="T523">giniler</ts>
                  <nts id="Seg_1868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1870" n="HIAT:w" s="T524">oloktorun</ts>
                  <nts id="Seg_1871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1873" n="HIAT:w" s="T525">tɨːppappɨn</ts>
                  <nts id="Seg_1874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1876" n="HIAT:w" s="T526">min</ts>
                  <nts id="Seg_1877" n="HIAT:ip">.</nts>
                  <nts id="Seg_1878" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T531" id="Seg_1880" n="HIAT:u" s="T527">
                  <ts e="T528" id="Seg_1882" n="HIAT:w" s="T527">Olordunnar</ts>
                  <nts id="Seg_1883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1885" n="HIAT:w" s="T528">šʼastlʼivɨe</ts>
                  <nts id="Seg_1886" n="HIAT:ip">,</nts>
                  <nts id="Seg_1887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_1889" n="HIAT:w" s="T529">staraːtsa</ts>
                  <nts id="Seg_1890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_1892" n="HIAT:w" s="T530">mʼinʼa</ts>
                  <nts id="Seg_1893" n="HIAT:ip">.</nts>
                  <nts id="Seg_1894" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T536" id="Seg_1896" n="HIAT:u" s="T531">
                  <ts e="T532" id="Seg_1898" n="HIAT:w" s="T531">Elete</ts>
                  <nts id="Seg_1899" n="HIAT:ip">,</nts>
                  <nts id="Seg_1900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_1902" n="HIAT:w" s="T532">onton</ts>
                  <nts id="Seg_1903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1905" n="HIAT:w" s="T533">ke</ts>
                  <nts id="Seg_1906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_1908" n="HIAT:w" s="T534">tuːgu</ts>
                  <nts id="Seg_1909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1911" n="HIAT:w" s="T535">detegin</ts>
                  <nts id="Seg_1912" n="HIAT:ip">?</nts>
                  <nts id="Seg_1913" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T552" id="Seg_1914" n="sc" s="T550">
               <ts e="T552" id="Seg_1916" n="HIAT:u" s="T550">
                  <nts id="Seg_1917" n="HIAT:ip">–</nts>
                  <nts id="Seg_1918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_1920" n="HIAT:w" s="T550">Eː</ts>
                  <nts id="Seg_1921" n="HIAT:ip">.</nts>
                  <nts id="Seg_1922" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T563" id="Seg_1923" n="sc" s="T560">
               <ts e="T563" id="Seg_1925" n="HIAT:u" s="T560">
                  <nts id="Seg_1926" n="HIAT:ip">–</nts>
                  <nts id="Seg_1927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_1929" n="HIAT:w" s="T560">Nʼelzʼa</ts>
                  <nts id="Seg_1930" n="HIAT:ip">.</nts>
                  <nts id="Seg_1931" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T644" id="Seg_1932" n="sc" s="T567">
               <ts e="T570" id="Seg_1934" n="HIAT:u" s="T567">
                  <nts id="Seg_1935" n="HIAT:ip">–</nts>
                  <nts id="Seg_1936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_1938" n="HIAT:w" s="T567">Kanʼešna</ts>
                  <nts id="Seg_1939" n="HIAT:ip">,</nts>
                  <nts id="Seg_1940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_1942" n="HIAT:w" s="T568">kanʼešna</ts>
                  <nts id="Seg_1943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_1945" n="HIAT:w" s="T569">nʼelzʼa</ts>
                  <nts id="Seg_1946" n="HIAT:ip">.</nts>
                  <nts id="Seg_1947" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T575" id="Seg_1949" n="HIAT:u" s="T570">
                  <ts e="T571" id="Seg_1951" n="HIAT:w" s="T570">Istibetter</ts>
                  <nts id="Seg_1952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_1954" n="HIAT:w" s="T571">törüt</ts>
                  <nts id="Seg_1955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_1957" n="HIAT:w" s="T572">iti</ts>
                  <nts id="Seg_1958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_1960" n="HIAT:w" s="T573">baran</ts>
                  <nts id="Seg_1961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_1963" n="HIAT:w" s="T574">kaːlallar</ts>
                  <nts id="Seg_1964" n="HIAT:ip">.</nts>
                  <nts id="Seg_1965" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T590" id="Seg_1967" n="HIAT:u" s="T575">
                  <ts e="T576" id="Seg_1969" n="HIAT:w" s="T575">Iti</ts>
                  <nts id="Seg_1970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_1972" n="HIAT:w" s="T576">bu</ts>
                  <nts id="Seg_1973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_1975" n="HIAT:w" s="T577">erim</ts>
                  <nts id="Seg_1976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_1978" n="HIAT:w" s="T578">edʼiːjin</ts>
                  <nts id="Seg_1979" n="HIAT:ip">,</nts>
                  <nts id="Seg_1980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_1982" n="HIAT:w" s="T579">eː</ts>
                  <nts id="Seg_1983" n="HIAT:ip">,</nts>
                  <nts id="Seg_1984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_1986" n="HIAT:w" s="T580">erim</ts>
                  <nts id="Seg_1987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_1989" n="HIAT:w" s="T581">baltɨtɨn</ts>
                  <nts id="Seg_1990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_1992" n="HIAT:w" s="T582">ogoloro</ts>
                  <nts id="Seg_1993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_1995" n="HIAT:w" s="T583">iti</ts>
                  <nts id="Seg_1996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_1998" n="HIAT:w" s="T584">baraːččɨlar</ts>
                  <nts id="Seg_1999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2000" n="HIAT:ip">(</nts>
                  <ts e="T586" id="Seg_2002" n="HIAT:w" s="T585">oŋo-</ts>
                  <nts id="Seg_2003" n="HIAT:ip">)</nts>
                  <nts id="Seg_2004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2006" n="HIAT:w" s="T586">oŋu͡oktarga</ts>
                  <nts id="Seg_2007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2009" n="HIAT:w" s="T587">min</ts>
                  <nts id="Seg_2010" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2012" n="HIAT:w" s="T588">ogolorbun</ts>
                  <nts id="Seg_2013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2015" n="HIAT:w" s="T589">kɨtta</ts>
                  <nts id="Seg_2016" n="HIAT:ip">.</nts>
                  <nts id="Seg_2017" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T603" id="Seg_2019" n="HIAT:u" s="T590">
                  <ts e="T591" id="Seg_2021" n="HIAT:w" s="T590">Inʼe</ts>
                  <nts id="Seg_2022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2024" n="HIAT:w" s="T591">inʼete</ts>
                  <nts id="Seg_2025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2027" n="HIAT:w" s="T592">ölbütün</ts>
                  <nts id="Seg_2028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2030" n="HIAT:w" s="T593">kenne</ts>
                  <nts id="Seg_2031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2033" n="HIAT:w" s="T594">haŋardɨː</ts>
                  <nts id="Seg_2034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2036" n="HIAT:w" s="T595">keler</ts>
                  <nts id="Seg_2037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2039" n="HIAT:w" s="T596">küččügüj</ts>
                  <nts id="Seg_2040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2042" n="HIAT:w" s="T597">u͡ola</ts>
                  <nts id="Seg_2043" n="HIAT:ip">,</nts>
                  <nts id="Seg_2044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2046" n="HIAT:w" s="T598">ontuŋ</ts>
                  <nts id="Seg_2047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2049" n="HIAT:w" s="T599">barar</ts>
                  <nts id="Seg_2050" n="HIAT:ip">,</nts>
                  <nts id="Seg_2051" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2053" n="HIAT:w" s="T600">onu</ts>
                  <nts id="Seg_2054" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2056" n="HIAT:w" s="T601">ataːrsallar</ts>
                  <nts id="Seg_2057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2059" n="HIAT:w" s="T602">iti</ts>
                  <nts id="Seg_2060" n="HIAT:ip">.</nts>
                  <nts id="Seg_2061" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T608" id="Seg_2063" n="HIAT:u" s="T603">
                  <ts e="T604" id="Seg_2065" n="HIAT:w" s="T603">Onu</ts>
                  <nts id="Seg_2066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2067" n="HIAT:ip">"</nts>
                  <ts e="T605" id="Seg_2069" n="HIAT:w" s="T604">barɨmaŋ</ts>
                  <nts id="Seg_2070" n="HIAT:ip">"</nts>
                  <nts id="Seg_2071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2073" n="HIAT:w" s="T605">diːbin</ts>
                  <nts id="Seg_2074" n="HIAT:ip">,</nts>
                  <nts id="Seg_2075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2077" n="HIAT:w" s="T606">istibetter</ts>
                  <nts id="Seg_2078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2080" n="HIAT:w" s="T607">bu͡o</ts>
                  <nts id="Seg_2081" n="HIAT:ip">.</nts>
                  <nts id="Seg_2082" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T612" id="Seg_2084" n="HIAT:u" s="T608">
                  <ts e="T609" id="Seg_2086" n="HIAT:w" s="T608">Tugu</ts>
                  <nts id="Seg_2087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2089" n="HIAT:w" s="T609">gɨna</ts>
                  <nts id="Seg_2090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2092" n="HIAT:w" s="T610">barallara</ts>
                  <nts id="Seg_2093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2095" n="HIAT:w" s="T611">dʼürü</ts>
                  <nts id="Seg_2096" n="HIAT:ip">?</nts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T614" id="Seg_2099" n="HIAT:u" s="T612">
                  <ts e="T613" id="Seg_2101" n="HIAT:w" s="T612">Gri͡ex</ts>
                  <nts id="Seg_2102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2104" n="HIAT:w" s="T613">bu͡o</ts>
                  <nts id="Seg_2105" n="HIAT:ip">.</nts>
                  <nts id="Seg_2106" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T622" id="Seg_2108" n="HIAT:u" s="T614">
                  <ts e="T615" id="Seg_2110" n="HIAT:w" s="T614">Anɨ</ts>
                  <nts id="Seg_2111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2113" n="HIAT:w" s="T615">kihi</ts>
                  <nts id="Seg_2114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2116" n="HIAT:w" s="T616">hir</ts>
                  <nts id="Seg_2117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2119" n="HIAT:w" s="T617">ajɨ</ts>
                  <nts id="Seg_2120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2122" n="HIAT:w" s="T618">ölö</ts>
                  <nts id="Seg_2123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2125" n="HIAT:w" s="T619">hɨtar</ts>
                  <nts id="Seg_2126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2128" n="HIAT:w" s="T620">kihi</ts>
                  <nts id="Seg_2129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2131" n="HIAT:w" s="T621">kuttanɨ͡ak</ts>
                  <nts id="Seg_2132" n="HIAT:ip">.</nts>
                  <nts id="Seg_2133" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T636" id="Seg_2135" n="HIAT:u" s="T622">
                  <ts e="T623" id="Seg_2137" n="HIAT:w" s="T622">Anɨ</ts>
                  <nts id="Seg_2138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2140" n="HIAT:w" s="T623">itinne</ts>
                  <nts id="Seg_2141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2143" n="HIAT:w" s="T624">biːr</ts>
                  <nts id="Seg_2144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2146" n="HIAT:w" s="T625">emeːksin</ts>
                  <nts id="Seg_2147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2149" n="HIAT:w" s="T626">öllö</ts>
                  <nts id="Seg_2150" n="HIAT:ip">,</nts>
                  <nts id="Seg_2151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2153" n="HIAT:w" s="T627">anɨ</ts>
                  <nts id="Seg_2154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2156" n="HIAT:w" s="T628">biːr</ts>
                  <nts id="Seg_2157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2159" n="HIAT:w" s="T629">ogo</ts>
                  <nts id="Seg_2160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2162" n="HIAT:w" s="T630">kelen</ts>
                  <nts id="Seg_2163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2165" n="HIAT:w" s="T631">ihen</ts>
                  <nts id="Seg_2166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2168" n="HIAT:w" s="T632">ölbüt</ts>
                  <nts id="Seg_2169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2171" n="HIAT:w" s="T633">emi͡e</ts>
                  <nts id="Seg_2172" n="HIAT:ip">,</nts>
                  <nts id="Seg_2173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2175" n="HIAT:w" s="T634">kaːri͡ennaːk</ts>
                  <nts id="Seg_2176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2178" n="HIAT:w" s="T635">ogobut</ts>
                  <nts id="Seg_2179" n="HIAT:ip">.</nts>
                  <nts id="Seg_2180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T640" id="Seg_2182" n="HIAT:u" s="T636">
                  <ts e="T637" id="Seg_2184" n="HIAT:w" s="T636">Hürdeːk</ts>
                  <nts id="Seg_2185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2187" n="HIAT:w" s="T637">üčügej</ts>
                  <nts id="Seg_2188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2190" n="HIAT:w" s="T638">ogo</ts>
                  <nts id="Seg_2191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2193" n="HIAT:w" s="T639">ete</ts>
                  <nts id="Seg_2194" n="HIAT:ip">.</nts>
                  <nts id="Seg_2195" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T644" id="Seg_2197" n="HIAT:u" s="T640">
                  <ts e="T641" id="Seg_2199" n="HIAT:w" s="T640">Buraːnɨnan</ts>
                  <nts id="Seg_2200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2202" n="HIAT:w" s="T641">taskajdana</ts>
                  <nts id="Seg_2203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2205" n="HIAT:w" s="T642">hɨldʼar</ts>
                  <nts id="Seg_2206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2208" n="HIAT:w" s="T643">ete</ts>
                  <nts id="Seg_2209" n="HIAT:ip">.</nts>
                  <nts id="Seg_2210" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T666" id="Seg_2211" n="sc" s="T661">
               <ts e="T666" id="Seg_2213" n="HIAT:u" s="T661">
                  <nts id="Seg_2214" n="HIAT:ip">–</nts>
                  <nts id="Seg_2215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2217" n="HIAT:w" s="T661">Heː</ts>
                  <nts id="Seg_2218" n="HIAT:ip">,</nts>
                  <nts id="Seg_2219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2221" n="HIAT:w" s="T662">bu</ts>
                  <nts id="Seg_2222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2224" n="HIAT:w" s="T663">tabaːkta</ts>
                  <nts id="Seg_2225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2227" n="HIAT:w" s="T664">bɨragɨ͡akkɨn</ts>
                  <nts id="Seg_2228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2230" n="HIAT:w" s="T665">naːda</ts>
                  <nts id="Seg_2231" n="HIAT:ip">.</nts>
                  <nts id="Seg_2232" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T674" id="Seg_2233" n="sc" s="T668">
               <ts e="T674" id="Seg_2235" n="HIAT:u" s="T668">
                  <nts id="Seg_2236" n="HIAT:ip">–</nts>
                  <nts id="Seg_2237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2239" n="HIAT:w" s="T668">Aragiː</ts>
                  <nts id="Seg_2240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2242" n="HIAT:w" s="T670">istekterine</ts>
                  <nts id="Seg_2243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2245" n="HIAT:w" s="T671">aragiːta</ts>
                  <nts id="Seg_2246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2248" n="HIAT:w" s="T672">kutu͡okkun</ts>
                  <nts id="Seg_2249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2251" n="HIAT:w" s="T673">naːda</ts>
                  <nts id="Seg_2252" n="HIAT:ip">.</nts>
                  <nts id="Seg_2253" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T676" id="Seg_2254" n="sc" s="T675">
               <ts e="T676" id="Seg_2256" n="HIAT:u" s="T675">
                  <nts id="Seg_2257" n="HIAT:ip">–</nts>
                  <nts id="Seg_2258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2260" n="HIAT:w" s="T675">Elete</ts>
                  <nts id="Seg_2261" n="HIAT:ip">.</nts>
                  <nts id="Seg_2262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T719" id="Seg_2263" n="sc" s="T687">
               <ts e="T692" id="Seg_2265" n="HIAT:u" s="T687">
                  <nts id="Seg_2266" n="HIAT:ip">–</nts>
                  <nts id="Seg_2267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2269" n="HIAT:w" s="T687">Rʼedka</ts>
                  <nts id="Seg_2270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2272" n="HIAT:w" s="T688">rʼedka</ts>
                  <nts id="Seg_2273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2275" n="HIAT:w" s="T689">barallar</ts>
                  <nts id="Seg_2276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2278" n="HIAT:w" s="T690">teːteleriger</ts>
                  <nts id="Seg_2279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2281" n="HIAT:w" s="T691">oŋu͡oktarga</ts>
                  <nts id="Seg_2282" n="HIAT:ip">.</nts>
                  <nts id="Seg_2283" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T697" id="Seg_2285" n="HIAT:u" s="T692">
                  <ts e="T693" id="Seg_2287" n="HIAT:w" s="T692">Inʼelerim</ts>
                  <nts id="Seg_2288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2290" n="HIAT:w" s="T693">oŋu͡oktarɨgar</ts>
                  <nts id="Seg_2291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2293" n="HIAT:w" s="T694">baraːččɨlar</ts>
                  <nts id="Seg_2294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2296" n="HIAT:w" s="T695">taːk</ts>
                  <nts id="Seg_2297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2299" n="HIAT:w" s="T696">da</ts>
                  <nts id="Seg_2300" n="HIAT:ip">.</nts>
                  <nts id="Seg_2301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T706" id="Seg_2303" n="HIAT:u" s="T697">
                  <ts e="T698" id="Seg_2305" n="HIAT:w" s="T697">Onnuk</ts>
                  <nts id="Seg_2306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2308" n="HIAT:w" s="T698">baːr</ts>
                  <nts id="Seg_2309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2311" n="HIAT:w" s="T699">ojuttar</ts>
                  <nts id="Seg_2312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2314" n="HIAT:w" s="T700">oŋu͡oktara</ts>
                  <nts id="Seg_2315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2317" n="HIAT:w" s="T701">baːllar</ts>
                  <nts id="Seg_2318" n="HIAT:ip">,</nts>
                  <nts id="Seg_2319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2321" n="HIAT:w" s="T702">ol</ts>
                  <nts id="Seg_2322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_2324" n="HIAT:w" s="T703">kuhagan</ts>
                  <nts id="Seg_2325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2327" n="HIAT:w" s="T704">bu͡o</ts>
                  <nts id="Seg_2328" n="HIAT:ip">,</nts>
                  <nts id="Seg_2329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2331" n="HIAT:w" s="T705">samaːttarɨŋ</ts>
                  <nts id="Seg_2332" n="HIAT:ip">.</nts>
                  <nts id="Seg_2333" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T708" id="Seg_2335" n="HIAT:u" s="T706">
                  <ts e="T707" id="Seg_2337" n="HIAT:w" s="T706">Anʼiː</ts>
                  <nts id="Seg_2338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2340" n="HIAT:w" s="T707">bu͡olaːččɨ</ts>
                  <nts id="Seg_2341" n="HIAT:ip">.</nts>
                  <nts id="Seg_2342" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T712" id="Seg_2344" n="HIAT:u" s="T708">
                  <ts e="T709" id="Seg_2346" n="HIAT:w" s="T708">Istibetter</ts>
                  <nts id="Seg_2347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2349" n="HIAT:w" s="T709">anɨ</ts>
                  <nts id="Seg_2350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2352" n="HIAT:w" s="T710">maladʼožtar</ts>
                  <nts id="Seg_2353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2355" n="HIAT:w" s="T711">törüt</ts>
                  <nts id="Seg_2356" n="HIAT:ip">.</nts>
                  <nts id="Seg_2357" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T713" id="Seg_2359" n="HIAT:u" s="T712">
                  <nts id="Seg_2360" n="HIAT:ip">(</nts>
                  <nts id="Seg_2361" n="HIAT:ip">(</nts>
                  <ats e="T713" id="Seg_2362" n="HIAT:non-pho" s="T712">LAUGH</ats>
                  <nts id="Seg_2363" n="HIAT:ip">)</nts>
                  <nts id="Seg_2364" n="HIAT:ip">)</nts>
                  <nts id="Seg_2365" n="HIAT:ip">.</nts>
                  <nts id="Seg_2366" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T715" id="Seg_2368" n="HIAT:u" s="T713">
                  <ts e="T714" id="Seg_2370" n="HIAT:w" s="T713">Durugoj</ts>
                  <nts id="Seg_2371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2373" n="HIAT:w" s="T714">narod</ts>
                  <nts id="Seg_2374" n="HIAT:ip">.</nts>
                  <nts id="Seg_2375" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T719" id="Seg_2377" n="HIAT:u" s="T715">
                  <ts e="T716" id="Seg_2379" n="HIAT:w" s="T715">Havsʼem</ts>
                  <nts id="Seg_2380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2382" n="HIAT:w" s="T716">drugoj</ts>
                  <nts id="Seg_2383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2385" n="HIAT:w" s="T717">narot</ts>
                  <nts id="Seg_2386" n="HIAT:ip">.</nts>
                  <nts id="Seg_2387" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KiPP">
            <ts e="T0" id="Seg_2388" n="sc" s="T1">
               <ts e="T2" id="Seg_2390" n="e" s="T1">– Tu͡oktan </ts>
               <ts e="T3" id="Seg_2392" n="e" s="T2">anʼiː </ts>
               <ts e="T4" id="Seg_2394" n="e" s="T3">diːgin </ts>
               <ts e="T0" id="Seg_2396" n="e" s="T4">du͡o. </ts>
            </ts>
            <ts e="T61" id="Seg_2397" n="sc" s="T5">
               <ts e="T6" id="Seg_2399" n="e" s="T5">Anʼiː </ts>
               <ts e="T7" id="Seg_2401" n="e" s="T6">bu͡ollagɨna, </ts>
               <ts e="T8" id="Seg_2403" n="e" s="T7">bɨlɨr </ts>
               <ts e="T9" id="Seg_2405" n="e" s="T8">ojuttar </ts>
               <ts e="T10" id="Seg_2407" n="e" s="T9">baːr </ts>
               <ts e="T11" id="Seg_2409" n="e" s="T10">bu͡olaːččɨlar, </ts>
               <ts e="T12" id="Seg_2411" n="e" s="T11">samaːttar. </ts>
               <ts e="T13" id="Seg_2413" n="e" s="T12">Onnuktar </ts>
               <ts e="T14" id="Seg_2415" n="e" s="T13">dʼi͡elerin </ts>
               <ts e="T15" id="Seg_2417" n="e" s="T14">ergijerge </ts>
               <ts e="T16" id="Seg_2419" n="e" s="T15">hatammat </ts>
               <ts e="T17" id="Seg_2421" n="e" s="T16">kuhagan. </ts>
               <ts e="T18" id="Seg_2423" n="e" s="T17">Ergijbekke </ts>
               <ts e="T19" id="Seg_2425" n="e" s="T18">hɨldʼɨ͡akkɨn </ts>
               <ts e="T20" id="Seg_2427" n="e" s="T19">anʼiː </ts>
               <ts e="T21" id="Seg_2429" n="e" s="T20">bu͡olaːččɨ, </ts>
               <ts e="T22" id="Seg_2431" n="e" s="T21">ol </ts>
               <ts e="T23" id="Seg_2433" n="e" s="T22">aːta </ts>
               <ts e="T24" id="Seg_2435" n="e" s="T23">anʼiː. </ts>
               <ts e="T25" id="Seg_2437" n="e" s="T24">Dʼaktar </ts>
               <ts e="T26" id="Seg_2439" n="e" s="T25">bu͡ollagɨna </ts>
               <ts e="T27" id="Seg_2441" n="e" s="T26">er </ts>
               <ts e="T28" id="Seg_2443" n="e" s="T27">kihi </ts>
               <ts e="T29" id="Seg_2445" n="e" s="T28">taŋahɨn </ts>
               <ts e="T30" id="Seg_2447" n="e" s="T29">tepser, </ts>
               <ts e="T31" id="Seg_2449" n="e" s="T30">emi͡e </ts>
               <ts e="T32" id="Seg_2451" n="e" s="T31">anʼiː. </ts>
               <ts e="T33" id="Seg_2453" n="e" s="T32">Čistej </ts>
               <ts e="T34" id="Seg_2455" n="e" s="T33">bu͡olu͡ogun, </ts>
               <ts e="T35" id="Seg_2457" n="e" s="T34">er </ts>
               <ts e="T36" id="Seg_2459" n="e" s="T35">kihi </ts>
               <ts e="T37" id="Seg_2461" n="e" s="T36">čeberdik </ts>
               <ts e="T38" id="Seg_2463" n="e" s="T37">hɨldʼɨ͡agɨn </ts>
               <ts e="T39" id="Seg_2465" n="e" s="T38">naːda. </ts>
               <ts e="T40" id="Seg_2467" n="e" s="T39">Dʼaktar </ts>
               <ts e="T41" id="Seg_2469" n="e" s="T40">ɨrbaːkɨta </ts>
               <ts e="T42" id="Seg_2471" n="e" s="T41">hu͡ok </ts>
               <ts e="T43" id="Seg_2473" n="e" s="T42">hɨldʼar </ts>
               <ts e="T44" id="Seg_2475" n="e" s="T43">emi͡e </ts>
               <ts e="T45" id="Seg_2477" n="e" s="T44">anʼiː </ts>
               <ts e="T46" id="Seg_2479" n="e" s="T45">urut. </ts>
               <ts e="T47" id="Seg_2481" n="e" s="T46">ɨrbaːkɨ </ts>
               <ts e="T48" id="Seg_2483" n="e" s="T47">keti͡egin </ts>
               <ts e="T49" id="Seg_2485" n="e" s="T48">naːda, </ts>
               <ts e="T50" id="Seg_2487" n="e" s="T49">pɨlaːtije. </ts>
               <ts e="T51" id="Seg_2489" n="e" s="T50">Er </ts>
               <ts e="T52" id="Seg_2491" n="e" s="T51">kihi </ts>
               <ts e="T53" id="Seg_2493" n="e" s="T52">kördük </ts>
               <ts e="T54" id="Seg_2495" n="e" s="T53">(hɨr-), </ts>
               <ts e="T55" id="Seg_2497" n="e" s="T54">anɨ </ts>
               <ts e="T56" id="Seg_2499" n="e" s="T55">barɨkaːttara </ts>
               <ts e="T57" id="Seg_2501" n="e" s="T56">hɨ͡alɨjalaːktar. </ts>
               <ts e="T58" id="Seg_2503" n="e" s="T57">Iti </ts>
               <ts e="T59" id="Seg_2505" n="e" s="T58">gri͡ex </ts>
               <ts e="T60" id="Seg_2507" n="e" s="T59">bu͡o </ts>
               <ts e="T61" id="Seg_2509" n="e" s="T60">ontuŋ. </ts>
            </ts>
            <ts e="T723" id="Seg_2510" n="sc" s="T66">
               <ts e="T68" id="Seg_2512" n="e" s="T66">– Eː, </ts>
               <ts e="T69" id="Seg_2514" n="e" s="T68">če, </ts>
               <ts e="T70" id="Seg_2516" n="e" s="T69">barɨta </ts>
               <ts e="T71" id="Seg_2518" n="e" s="T70">er </ts>
               <ts e="T72" id="Seg_2520" n="e" s="T71">kihi͡eke </ts>
               <ts e="T73" id="Seg_2522" n="e" s="T72">di͡eri </ts>
               <ts e="T74" id="Seg_2524" n="e" s="T73">kuhagan </ts>
               <ts e="T75" id="Seg_2526" n="e" s="T74">ol, </ts>
               <ts e="T76" id="Seg_2528" n="e" s="T75">dʼaktar </ts>
               <ts e="T77" id="Seg_2530" n="e" s="T76">dʼaktar </ts>
               <ts e="T78" id="Seg_2532" n="e" s="T77">kördük </ts>
               <ts e="T79" id="Seg_2534" n="e" s="T78">bu͡olu͡ogun </ts>
               <ts e="T80" id="Seg_2536" n="e" s="T79">naːda, </ts>
               <ts e="T81" id="Seg_2538" n="e" s="T80">ɨrbaːkɨlaːk, </ts>
               <ts e="T82" id="Seg_2540" n="e" s="T81">pɨlaːttaːk </ts>
               <ts e="T83" id="Seg_2542" n="e" s="T82">bu͡olu͡ogun </ts>
               <ts e="T84" id="Seg_2544" n="e" s="T83">naːda. </ts>
               <ts e="T85" id="Seg_2546" n="e" s="T84">Bergeheni </ts>
               <ts e="T86" id="Seg_2548" n="e" s="T85">baga </ts>
               <ts e="T87" id="Seg_2550" n="e" s="T86">ketimne </ts>
               <ts e="T88" id="Seg_2552" n="e" s="T87">onto </ts>
               <ts e="T89" id="Seg_2554" n="e" s="T88">keti͡ekke </ts>
               <ts e="T90" id="Seg_2556" n="e" s="T89">naːda </ts>
               <ts e="T723" id="Seg_2558" n="e" s="T90">saːpkɨni. </ts>
            </ts>
            <ts e="T724" id="Seg_2559" n="sc" s="T91">
               <ts e="T92" id="Seg_2561" n="e" s="T91">Iti </ts>
               <ts e="T93" id="Seg_2563" n="e" s="T92">onnuktar. </ts>
               <ts e="T94" id="Seg_2565" n="e" s="T93">Onton </ts>
               <ts e="T95" id="Seg_2567" n="e" s="T94">erge </ts>
               <ts e="T96" id="Seg_2569" n="e" s="T95">bararɨŋ </ts>
               <ts e="T97" id="Seg_2571" n="e" s="T96">hagɨna </ts>
               <ts e="T98" id="Seg_2573" n="e" s="T97">iliː </ts>
               <ts e="T99" id="Seg_2575" n="e" s="T98">oksohallar </ts>
               <ts e="T100" id="Seg_2577" n="e" s="T99">taŋaraga. </ts>
               <ts e="T101" id="Seg_2579" n="e" s="T100">Itigirdik </ts>
               <ts e="T102" id="Seg_2581" n="e" s="T101">bi͡ereller </ts>
               <ts e="T103" id="Seg_2583" n="e" s="T102">iliːlerin </ts>
               <ts e="T104" id="Seg_2585" n="e" s="T103">erge </ts>
               <ts e="T105" id="Seg_2587" n="e" s="T104">barar </ts>
               <ts e="T724" id="Seg_2589" n="e" s="T105">kihi. </ts>
            </ts>
            <ts e="T725" id="Seg_2590" n="sc" s="T106">
               <ts e="T107" id="Seg_2592" n="e" s="T106">Iliː </ts>
               <ts e="T108" id="Seg_2594" n="e" s="T107">berseller, </ts>
               <ts e="T109" id="Seg_2596" n="e" s="T108">oččogo </ts>
               <ts e="T110" id="Seg_2598" n="e" s="T109">iliːlerin </ts>
               <ts e="T111" id="Seg_2600" n="e" s="T110">bi͡erdekterine </ts>
               <ts e="T112" id="Seg_2602" n="e" s="T111">dʼaktar </ts>
               <ts e="T113" id="Seg_2604" n="e" s="T112">bu͡olar </ts>
               <ts e="T114" id="Seg_2606" n="e" s="T113">ol </ts>
               <ts e="T725" id="Seg_2608" n="e" s="T114">kihi. </ts>
            </ts>
            <ts e="T154" id="Seg_2609" n="sc" s="T115">
               <ts e="T116" id="Seg_2611" n="e" s="T115">Onnuk </ts>
               <ts e="T117" id="Seg_2613" n="e" s="T116">abɨčajdar </ts>
               <ts e="T118" id="Seg_2615" n="e" s="T117">onnuk. </ts>
               <ts e="T119" id="Seg_2617" n="e" s="T118">Heː, </ts>
               <ts e="T120" id="Seg_2619" n="e" s="T119">küːske </ts>
               <ts e="T121" id="Seg_2621" n="e" s="T120">bi͡ereller </ts>
               <ts e="T122" id="Seg_2623" n="e" s="T121">bu͡o </ts>
               <ts e="T123" id="Seg_2625" n="e" s="T122">kihini, </ts>
               <ts e="T124" id="Seg_2627" n="e" s="T123">bagar </ts>
               <ts e="T125" id="Seg_2629" n="e" s="T124">da </ts>
               <ts e="T126" id="Seg_2631" n="e" s="T125">bagarɨma, </ts>
               <ts e="T127" id="Seg_2633" n="e" s="T126">da </ts>
               <ts e="T128" id="Seg_2635" n="e" s="T127">bi͡ereller </ts>
               <ts e="T129" id="Seg_2637" n="e" s="T128">him </ts>
               <ts e="T130" id="Seg_2639" n="e" s="T129">biːr. </ts>
               <ts e="T131" id="Seg_2641" n="e" s="T130">ɨtɨː-ɨtɨː </ts>
               <ts e="T132" id="Seg_2643" n="e" s="T131">bara </ts>
               <ts e="T133" id="Seg_2645" n="e" s="T132">turu͡oŋ </ts>
               <ts e="T134" id="Seg_2647" n="e" s="T133">onuga. </ts>
               <ts e="T135" id="Seg_2649" n="e" s="T134">Ol </ts>
               <ts e="T136" id="Seg_2651" n="e" s="T135">kördük </ts>
               <ts e="T137" id="Seg_2653" n="e" s="T136">min </ts>
               <ts e="T138" id="Seg_2655" n="e" s="T137">bara </ts>
               <ts e="T139" id="Seg_2657" n="e" s="T138">turbutum </ts>
               <ts e="T140" id="Seg_2659" n="e" s="T139">erge </ts>
               <ts e="T141" id="Seg_2661" n="e" s="T140">ɨtɨː-ɨtɨːbɨn. </ts>
               <ts e="T142" id="Seg_2663" n="e" s="T141">U͡on </ts>
               <ts e="T143" id="Seg_2665" n="e" s="T142">alta </ts>
               <ts e="T144" id="Seg_2667" n="e" s="T143">dʼɨllaːkpar, </ts>
               <ts e="T145" id="Seg_2669" n="e" s="T144">ke </ts>
               <ts e="T146" id="Seg_2671" n="e" s="T145">tu͡ok </ts>
               <ts e="T147" id="Seg_2673" n="e" s="T146">kihite </ts>
               <ts e="T148" id="Seg_2675" n="e" s="T147">erge </ts>
               <ts e="T149" id="Seg_2677" n="e" s="T148">barɨ͡aj </ts>
               <ts e="T150" id="Seg_2679" n="e" s="T149">ol </ts>
               <ts e="T151" id="Seg_2681" n="e" s="T150">u͡on </ts>
               <ts e="T152" id="Seg_2683" n="e" s="T151">alta </ts>
               <ts e="T153" id="Seg_2685" n="e" s="T152">dʼɨllaːkka. </ts>
               <ts e="T154" id="Seg_2687" n="e" s="T153">Bi͡erbittere. </ts>
            </ts>
            <ts e="T726" id="Seg_2688" n="sc" s="T156">
               <ts e="T157" id="Seg_2690" n="e" s="T156">– Hapsi͡em </ts>
               <ts e="T158" id="Seg_2692" n="e" s="T157">ogo, </ts>
               <ts e="T159" id="Seg_2694" n="e" s="T158">ɨraːs </ts>
               <ts e="T160" id="Seg_2696" n="e" s="T159">etim </ts>
               <ts e="T161" id="Seg_2698" n="e" s="T160">haːtar. </ts>
               <ts e="T162" id="Seg_2700" n="e" s="T161">Mʼesnaja </ts>
               <ts e="T163" id="Seg_2702" n="e" s="T162">da </ts>
               <ts e="T164" id="Seg_2704" n="e" s="T163">hu͡ok </ts>
               <ts e="T165" id="Seg_2706" n="e" s="T164">etim. </ts>
               <ts e="T166" id="Seg_2708" n="e" s="T165">Onton </ts>
               <ts e="T167" id="Seg_2710" n="e" s="T166">iti </ts>
               <ts e="T168" id="Seg_2712" n="e" s="T167">tagɨsta </ts>
               <ts e="T169" id="Seg_2714" n="e" s="T168">ulakan </ts>
               <ts e="T170" id="Seg_2716" n="e" s="T169">kɨːhɨm </ts>
               <ts e="T171" id="Seg_2718" n="e" s="T170">anɨ </ts>
               <ts e="T172" id="Seg_2720" n="e" s="T171">bu͡o. </ts>
               <ts e="T173" id="Seg_2722" n="e" s="T172">Iti </ts>
               <ts e="T174" id="Seg_2724" n="e" s="T173">iti </ts>
               <ts e="T175" id="Seg_2726" n="e" s="T174">ogom </ts>
               <ts e="T176" id="Seg_2728" n="e" s="T175">u͡on </ts>
               <ts e="T177" id="Seg_2730" n="e" s="T176">hette, </ts>
               <ts e="T178" id="Seg_2732" n="e" s="T177">u͡on </ts>
               <ts e="T179" id="Seg_2734" n="e" s="T178">agɨs </ts>
               <ts e="T180" id="Seg_2736" n="e" s="T179">dʼɨllaːkpar </ts>
               <ts e="T181" id="Seg_2738" n="e" s="T180">töröːbüt </ts>
               <ts e="T182" id="Seg_2740" n="e" s="T181">ogo </ts>
               <ts e="T183" id="Seg_2742" n="e" s="T182">iti. </ts>
               <ts e="T184" id="Seg_2744" n="e" s="T183">Dʼe </ts>
               <ts e="T185" id="Seg_2746" n="e" s="T184">onton </ts>
               <ts e="T186" id="Seg_2748" n="e" s="T185">kɨrdʼan </ts>
               <ts e="T187" id="Seg_2750" n="e" s="T186">kɨrdʼammɨn </ts>
               <ts e="T188" id="Seg_2752" n="e" s="T187">iti </ts>
               <ts e="T189" id="Seg_2754" n="e" s="T188">kojut </ts>
               <ts e="T190" id="Seg_2756" n="e" s="T189">kojutun </ts>
               <ts e="T191" id="Seg_2758" n="e" s="T190">onton </ts>
               <ts e="T192" id="Seg_2760" n="e" s="T191">du͡o </ts>
               <ts e="T193" id="Seg_2762" n="e" s="T192">((LAUGH)). </ts>
               <ts e="T194" id="Seg_2764" n="e" s="T193">Ogolonon </ts>
               <ts e="T195" id="Seg_2766" n="e" s="T194">da </ts>
               <ts e="T196" id="Seg_2768" n="e" s="T195">ogolonon </ts>
               <ts e="T197" id="Seg_2770" n="e" s="T196">ihegin </ts>
               <ts e="T198" id="Seg_2772" n="e" s="T197">bu͡o </ts>
               <ts e="T199" id="Seg_2774" n="e" s="T198">lʼubovʼ </ts>
               <ts e="T200" id="Seg_2776" n="e" s="T199">tʼomnaja </ts>
               <ts e="T201" id="Seg_2778" n="e" s="T200">lʼubovʼ </ts>
               <ts e="T202" id="Seg_2780" n="e" s="T201">((LAUGH)). </ts>
               <ts e="T203" id="Seg_2782" n="e" s="T202">Hürdeːk </ts>
               <ts e="T204" id="Seg_2784" n="e" s="T203">üčügej </ts>
               <ts e="T205" id="Seg_2786" n="e" s="T204">kihi </ts>
               <ts e="T206" id="Seg_2788" n="e" s="T205">ete </ts>
               <ts e="T207" id="Seg_2790" n="e" s="T206">erim. </ts>
               <ts e="T208" id="Seg_2792" n="e" s="T207">Bu </ts>
               <ts e="T209" id="Seg_2794" n="e" s="T208">oloroːčču </ts>
               <ts e="T210" id="Seg_2796" n="e" s="T209">kihi </ts>
               <ts e="T726" id="Seg_2798" n="e" s="T210">iti. </ts>
            </ts>
            <ts e="T727" id="Seg_2799" n="sc" s="T211">
               <ts e="T212" id="Seg_2801" n="e" s="T211">Iti </ts>
               <ts e="T213" id="Seg_2803" n="e" s="T212">min </ts>
               <ts e="T214" id="Seg_2805" n="e" s="T213">toguh </ts>
               <ts e="T215" id="Seg_2807" n="e" s="T214">ogoloːk </ts>
               <ts e="T216" id="Seg_2809" n="e" s="T215">(erdineːgim) </ts>
               <ts e="T217" id="Seg_2811" n="e" s="T216">bu͡o </ts>
               <ts e="T218" id="Seg_2813" n="e" s="T217">össü͡ö. </ts>
               <ts e="T219" id="Seg_2815" n="e" s="T218">Össü͡ö </ts>
               <ts e="T220" id="Seg_2817" n="e" s="T219">da </ts>
               <ts e="T221" id="Seg_2819" n="e" s="T220">maladoj </ts>
               <ts e="T222" id="Seg_2821" n="e" s="T221">bu͡oltakpɨn </ts>
               <ts e="T223" id="Seg_2823" n="e" s="T222">itinne. </ts>
               <ts e="T224" id="Seg_2825" n="e" s="T223">Če </ts>
               <ts e="T225" id="Seg_2827" n="e" s="T224">onnuk </ts>
               <ts e="T226" id="Seg_2829" n="e" s="T225">abɨčajdaːk </ts>
               <ts e="T227" id="Seg_2831" n="e" s="T226">etilere, </ts>
               <ts e="T228" id="Seg_2833" n="e" s="T227">"onton </ts>
               <ts e="T229" id="Seg_2835" n="e" s="T228">hin </ts>
               <ts e="T230" id="Seg_2837" n="e" s="T229">olok </ts>
               <ts e="T231" id="Seg_2839" n="e" s="T230">olorogun, </ts>
               <ts e="T232" id="Seg_2841" n="e" s="T231">tulaːjaktarɨ </ts>
               <ts e="T233" id="Seg_2843" n="e" s="T232">ahaːtɨŋ", </ts>
               <ts e="T727" id="Seg_2845" n="e" s="T233">di͡eččiler. </ts>
            </ts>
            <ts e="T728" id="Seg_2846" n="sc" s="T234">
               <ts e="T235" id="Seg_2848" n="e" s="T234">"Tulaːjak </ts>
               <ts e="T236" id="Seg_2850" n="e" s="T235">ogoloro, </ts>
               <ts e="T237" id="Seg_2852" n="e" s="T236">anʼiː </ts>
               <ts e="T238" id="Seg_2854" n="e" s="T237">bu͡olu͡o </ts>
               <ts e="T239" id="Seg_2856" n="e" s="T238">ogoloro </ts>
               <ts e="T240" id="Seg_2858" n="e" s="T239">ahaːtɨŋ", </ts>
               <ts e="T241" id="Seg_2860" n="e" s="T240">di͡eččiler. </ts>
               <ts e="T242" id="Seg_2862" n="e" s="T241">"Kuhagannɨk </ts>
               <ts e="T243" id="Seg_2864" n="e" s="T242">gɨmmɨt </ts>
               <ts e="T244" id="Seg_2866" n="e" s="T243">kihini </ts>
               <ts e="T245" id="Seg_2868" n="e" s="T244">üčügejdik </ts>
               <ts e="T246" id="Seg_2870" n="e" s="T245">ataːrɨŋ", </ts>
               <ts e="T247" id="Seg_2872" n="e" s="T246">di͡eččiler. </ts>
               <ts e="T248" id="Seg_2874" n="e" s="T247">"Töttörü </ts>
               <ts e="T249" id="Seg_2876" n="e" s="T248">togo </ts>
               <ts e="T250" id="Seg_2878" n="e" s="T249">bu͡olar" </ts>
               <ts e="T251" id="Seg_2880" n="e" s="T250">di͡etekpine </ts>
               <ts e="T252" id="Seg_2882" n="e" s="T251">anʼiː </ts>
               <ts e="T253" id="Seg_2884" n="e" s="T252">bu͡olu͡o, </ts>
               <ts e="T254" id="Seg_2886" n="e" s="T253">"onton </ts>
               <ts e="T255" id="Seg_2888" n="e" s="T254">bejeŋ </ts>
               <ts e="T256" id="Seg_2890" n="e" s="T255">(on-) </ts>
               <ts e="T257" id="Seg_2892" n="e" s="T256">üčügejdik </ts>
               <ts e="T258" id="Seg_2894" n="e" s="T257">olordokkuna </ts>
               <ts e="T259" id="Seg_2896" n="e" s="T258">töttörü </ts>
               <ts e="T260" id="Seg_2898" n="e" s="T259">bejeger </ts>
               <ts e="T261" id="Seg_2900" n="e" s="T260">ɨmsɨːrɨ͡aktara", </ts>
               <ts e="T262" id="Seg_2902" n="e" s="T261">di͡eččiler </ts>
               <ts e="T263" id="Seg_2904" n="e" s="T262">bɨlɨrgɨlar. </ts>
               <ts e="T264" id="Seg_2906" n="e" s="T263">Abɨčajdar </ts>
               <ts e="T265" id="Seg_2908" n="e" s="T264">onnuk. </ts>
               <ts e="T266" id="Seg_2910" n="e" s="T265">"Onton </ts>
               <ts e="T267" id="Seg_2912" n="e" s="T266">dʼolloːk </ts>
               <ts e="T268" id="Seg_2914" n="e" s="T267">bu͡olluŋ </ts>
               <ts e="T269" id="Seg_2916" n="e" s="T268">bejeŋ </ts>
               <ts e="T270" id="Seg_2918" n="e" s="T269">kojut </ts>
               <ts e="T271" id="Seg_2920" n="e" s="T270">dʼolloːk </ts>
               <ts e="T272" id="Seg_2922" n="e" s="T271">bu͡olluŋ", </ts>
               <ts e="T273" id="Seg_2924" n="e" s="T272">di͡ete, </ts>
               <ts e="T274" id="Seg_2926" n="e" s="T273">min </ts>
               <ts e="T275" id="Seg_2928" n="e" s="T274">anɨ </ts>
               <ts e="T276" id="Seg_2930" n="e" s="T275">dʼolloːkpun. </ts>
               <ts e="T277" id="Seg_2932" n="e" s="T276">Ol </ts>
               <ts e="T278" id="Seg_2934" n="e" s="T277">tulaːjak </ts>
               <ts e="T279" id="Seg_2936" n="e" s="T278">etim </ts>
               <ts e="T280" id="Seg_2938" n="e" s="T279">bu͡o </ts>
               <ts e="T281" id="Seg_2940" n="e" s="T280">onton. </ts>
               <ts e="T282" id="Seg_2942" n="e" s="T281">Inʼem </ts>
               <ts e="T283" id="Seg_2944" n="e" s="T282">ölbütüger </ts>
               <ts e="T284" id="Seg_2946" n="e" s="T283">erge </ts>
               <ts e="T285" id="Seg_2948" n="e" s="T284">bi͡erbitin </ts>
               <ts e="T286" id="Seg_2950" n="e" s="T285">kenne, </ts>
               <ts e="T287" id="Seg_2952" n="e" s="T286">eː, </ts>
               <ts e="T288" id="Seg_2954" n="e" s="T287">erge </ts>
               <ts e="T289" id="Seg_2956" n="e" s="T288">bi͡ereligine </ts>
               <ts e="T290" id="Seg_2958" n="e" s="T289">ɨ͡altan </ts>
               <ts e="T291" id="Seg_2960" n="e" s="T290">ɨ͡alga </ts>
               <ts e="T292" id="Seg_2962" n="e" s="T291">hɨldʼaːččɨbɨt. </ts>
               <ts e="T293" id="Seg_2964" n="e" s="T292">Onno </ts>
               <ts e="T294" id="Seg_2966" n="e" s="T293">inʼem </ts>
               <ts e="T295" id="Seg_2968" n="e" s="T294">inʼem </ts>
               <ts e="T296" id="Seg_2970" n="e" s="T295">ölörügör </ts>
               <ts e="T297" id="Seg_2972" n="e" s="T296">innʼe </ts>
               <ts e="T298" id="Seg_2974" n="e" s="T297">diːr: </ts>
               <ts e="T299" id="Seg_2976" n="e" s="T298">"Bu͡o </ts>
               <ts e="T300" id="Seg_2978" n="e" s="T299">kaja </ts>
               <ts e="T301" id="Seg_2980" n="e" s="T300">tulaːjaktarɨ </ts>
               <ts e="T302" id="Seg_2982" n="e" s="T301">ahataːr </ts>
               <ts e="T303" id="Seg_2984" n="e" s="T302">bu͡ol. </ts>
               <ts e="T304" id="Seg_2986" n="e" s="T303">Kuhagannɨk </ts>
               <ts e="T305" id="Seg_2988" n="e" s="T304">gɨmmɨtɨ </ts>
               <ts e="T306" id="Seg_2990" n="e" s="T305">üčügejdik </ts>
               <ts e="T307" id="Seg_2992" n="e" s="T306">gɨnɨ͡akkɨn </ts>
               <ts e="T308" id="Seg_2994" n="e" s="T307">naːda, </ts>
               <ts e="T309" id="Seg_2996" n="e" s="T308">oččogo </ts>
               <ts e="T310" id="Seg_2998" n="e" s="T309">uhun </ts>
               <ts e="T311" id="Seg_3000" n="e" s="T310">dʼolloːk </ts>
               <ts e="T312" id="Seg_3002" n="e" s="T311">bu͡olu͡oŋ. </ts>
               <ts e="T313" id="Seg_3004" n="e" s="T312">Ügüs </ts>
               <ts e="T314" id="Seg_3006" n="e" s="T313">ogoloːk </ts>
               <ts e="T315" id="Seg_3008" n="e" s="T314">bu͡olu͡oŋ, </ts>
               <ts e="T316" id="Seg_3010" n="e" s="T315">belem </ts>
               <ts e="T317" id="Seg_3012" n="e" s="T316">olokko </ts>
               <ts e="T318" id="Seg_3014" n="e" s="T317">oloru͡oŋ", </ts>
               <ts e="T319" id="Seg_3016" n="e" s="T318">di͡ebite. </ts>
               <ts e="T320" id="Seg_3018" n="e" s="T319">Ile, </ts>
               <ts e="T321" id="Seg_3020" n="e" s="T320">kör. </ts>
               <ts e="T322" id="Seg_3022" n="e" s="T321">Anɨ </ts>
               <ts e="T323" id="Seg_3024" n="e" s="T322">min </ts>
               <ts e="T324" id="Seg_3026" n="e" s="T323">bejem </ts>
               <ts e="T325" id="Seg_3028" n="e" s="T324">bu͡ollagɨna </ts>
               <ts e="T326" id="Seg_3030" n="e" s="T325">bi͡eh-u͡ontan </ts>
               <ts e="T327" id="Seg_3032" n="e" s="T326">taksa </ts>
               <ts e="T328" id="Seg_3034" n="e" s="T327">kihibin </ts>
               <ts e="T329" id="Seg_3036" n="e" s="T328">bejem </ts>
               <ts e="T330" id="Seg_3038" n="e" s="T329">ispitten </ts>
               <ts e="T331" id="Seg_3040" n="e" s="T330">taksɨbɨtɨm. </ts>
               <ts e="T332" id="Seg_3042" n="e" s="T331">Vɨnuktardɨːmmɨn. </ts>
               <ts e="T333" id="Seg_3044" n="e" s="T332">Pʼitdʼesʼat </ts>
               <ts e="T334" id="Seg_3046" n="e" s="T333">s </ts>
               <ts e="T335" id="Seg_3048" n="e" s="T334">lʼiska. </ts>
               <ts e="T336" id="Seg_3050" n="e" s="T335">Naru͡odum, </ts>
               <ts e="T337" id="Seg_3052" n="e" s="T336">bejem </ts>
               <ts e="T338" id="Seg_3054" n="e" s="T337">naru͡odum. </ts>
               <ts e="T339" id="Seg_3056" n="e" s="T338">Hɨndaːska </ts>
               <ts e="T340" id="Seg_3058" n="e" s="T339">teŋ </ts>
               <ts e="T341" id="Seg_3060" n="e" s="T340">aŋara. </ts>
               <ts e="T342" id="Seg_3062" n="e" s="T341">Aŋara </ts>
               <ts e="T343" id="Seg_3064" n="e" s="T342">ɨraːk </ts>
               <ts e="T344" id="Seg_3066" n="e" s="T343">baːllar </ts>
               <ts e="T345" id="Seg_3068" n="e" s="T344">össü͡ö </ts>
               <ts e="T346" id="Seg_3070" n="e" s="T345">ol </ts>
               <ts e="T347" id="Seg_3072" n="e" s="T346">di͡ek. </ts>
               <ts e="T348" id="Seg_3074" n="e" s="T347">Mannagɨm </ts>
               <ts e="T349" id="Seg_3076" n="e" s="T348">ere </ts>
               <ts e="T350" id="Seg_3078" n="e" s="T349">de </ts>
               <ts e="T351" id="Seg_3080" n="e" s="T350">hɨlaː bog. </ts>
               <ts e="T352" id="Seg_3082" n="e" s="T351">Manna </ts>
               <ts e="T353" id="Seg_3084" n="e" s="T352">ere </ts>
               <ts e="T354" id="Seg_3086" n="e" s="T353">baːr </ts>
               <ts e="T355" id="Seg_3088" n="e" s="T354">bi͡eh-u͡ontan </ts>
               <ts e="T356" id="Seg_3090" n="e" s="T355">taksa </ts>
               <ts e="T357" id="Seg_3092" n="e" s="T356">kihibit, </ts>
               <ts e="T358" id="Seg_3094" n="e" s="T357">ogolorduːn. </ts>
               <ts e="T359" id="Seg_3096" n="e" s="T358">Pačʼtʼi </ts>
               <ts e="T360" id="Seg_3098" n="e" s="T359">Sɨndasska </ts>
               <ts e="T361" id="Seg_3100" n="e" s="T360">(kaltak) </ts>
               <ts e="T728" id="Seg_3102" n="e" s="T361">kajan. </ts>
            </ts>
            <ts e="T419" id="Seg_3103" n="sc" s="T362">
               <ts e="T363" id="Seg_3105" n="e" s="T362">Hɨndaːska </ts>
               <ts e="T364" id="Seg_3107" n="e" s="T363">aŋaram. </ts>
               <ts e="T365" id="Seg_3109" n="e" s="T364">Tak </ts>
               <ts e="T366" id="Seg_3111" n="e" s="T365">da </ts>
               <ts e="T367" id="Seg_3113" n="e" s="T366">üčügejdik </ts>
               <ts e="T368" id="Seg_3115" n="e" s="T367">olorobun </ts>
               <ts e="T369" id="Seg_3117" n="e" s="T368">bertteːk </ts>
               <ts e="T370" id="Seg_3119" n="e" s="T369">kihiler </ts>
               <ts e="T371" id="Seg_3121" n="e" s="T370">algɨstarɨgar. </ts>
               <ts e="T372" id="Seg_3123" n="e" s="T371">Kihiler </ts>
               <ts e="T373" id="Seg_3125" n="e" s="T372">algɨstara, </ts>
               <ts e="T374" id="Seg_3127" n="e" s="T373">kɨrdʼagastar </ts>
               <ts e="T375" id="Seg_3129" n="e" s="T374">algɨstara. </ts>
               <ts e="T376" id="Seg_3131" n="e" s="T375">Anɨ </ts>
               <ts e="T377" id="Seg_3133" n="e" s="T376">bu͡o </ts>
               <ts e="T378" id="Seg_3135" n="e" s="T377">kɨrdʼammɨn </ts>
               <ts e="T379" id="Seg_3137" n="e" s="T378">ɨ͡aldʼabɨn </ts>
               <ts e="T380" id="Seg_3139" n="e" s="T379">ere </ts>
               <ts e="T381" id="Seg_3141" n="e" s="T380">bu͡o. </ts>
               <ts e="T382" id="Seg_3143" n="e" s="T381">Davlʼenʼijalaːppɨn. </ts>
               <ts e="T383" id="Seg_3145" n="e" s="T382">Hürdeːk </ts>
               <ts e="T384" id="Seg_3147" n="e" s="T383">ulakan </ts>
               <ts e="T386" id="Seg_3149" n="e" s="T384">ikki, </ts>
               <ts e="T388" id="Seg_3151" n="e" s="T386">heː, </ts>
               <ts e="T390" id="Seg_3153" n="e" s="T388">ikki </ts>
               <ts e="T391" id="Seg_3155" n="e" s="T390">hüːsten </ts>
               <ts e="T392" id="Seg_3157" n="e" s="T391">taksar. </ts>
               <ts e="T393" id="Seg_3159" n="e" s="T392">– Dvacat </ts>
               <ts e="T394" id="Seg_3161" n="e" s="T393">dva </ts>
               <ts e="T395" id="Seg_3163" n="e" s="T394">bu͡olar. </ts>
               <ts e="T396" id="Seg_3165" n="e" s="T395">Ontubun </ts>
               <ts e="T397" id="Seg_3167" n="e" s="T396">koppoppun, </ts>
               <ts e="T398" id="Seg_3169" n="e" s="T397">onton </ts>
               <ts e="T399" id="Seg_3171" n="e" s="T398">ataktarɨm </ts>
               <ts e="T400" id="Seg_3173" n="e" s="T399">kaːjallar. </ts>
               <ts e="T401" id="Seg_3175" n="e" s="T400">Anɨ </ts>
               <ts e="T402" id="Seg_3177" n="e" s="T401">biːr </ts>
               <ts e="T403" id="Seg_3179" n="e" s="T402">da </ts>
               <ts e="T404" id="Seg_3181" n="e" s="T403">ogogo </ts>
               <ts e="T405" id="Seg_3183" n="e" s="T404">koton </ts>
               <ts e="T406" id="Seg_3185" n="e" s="T405">tiːjbeppin </ts>
               <ts e="T407" id="Seg_3187" n="e" s="T406">bu͡o, </ts>
               <ts e="T408" id="Seg_3189" n="e" s="T407">hɨtabɨn </ts>
               <ts e="T409" id="Seg_3191" n="e" s="T408">bu͡o. </ts>
               <ts e="T410" id="Seg_3193" n="e" s="T409">Skoro </ts>
               <ts e="T411" id="Seg_3195" n="e" s="T410">umru </ts>
               <ts e="T412" id="Seg_3197" n="e" s="T411">navʼerna </ts>
               <ts e="T413" id="Seg_3199" n="e" s="T412">((LAUGH)), </ts>
               <ts e="T414" id="Seg_3201" n="e" s="T413">nʼe </ts>
               <ts e="T415" id="Seg_3203" n="e" s="T414">bojusʼ </ts>
               <ts e="T417" id="Seg_3205" n="e" s="T415">ja. </ts>
               <ts e="T419" id="Seg_3207" n="e" s="T417">– Nʼet. </ts>
            </ts>
            <ts e="T424" id="Seg_3208" n="sc" s="T422">
               <ts e="T423" id="Seg_3210" n="e" s="T422">– Arajga </ts>
               <ts e="T424" id="Seg_3212" n="e" s="T423">barɨ͡am. </ts>
            </ts>
            <ts e="T436" id="Seg_3213" n="sc" s="T428">
               <ts e="T430" id="Seg_3215" n="e" s="T428">– E, </ts>
               <ts e="T431" id="Seg_3217" n="e" s="T430">arajga </ts>
               <ts e="T432" id="Seg_3219" n="e" s="T431">barɨ͡am. </ts>
               <ts e="T433" id="Seg_3221" n="e" s="T432">Ogolorum </ts>
               <ts e="T434" id="Seg_3223" n="e" s="T433">elbekter, </ts>
               <ts e="T435" id="Seg_3225" n="e" s="T434">arajga </ts>
               <ts e="T436" id="Seg_3227" n="e" s="T435">kötütü͡öktere. </ts>
            </ts>
            <ts e="T443" id="Seg_3228" n="sc" s="T439">
               <ts e="T440" id="Seg_3230" n="e" s="T439">– Taŋaraga </ts>
               <ts e="T441" id="Seg_3232" n="e" s="T440">ke. </ts>
               <ts e="T442" id="Seg_3234" n="e" s="T441">Taŋaraga </ts>
               <ts e="T443" id="Seg_3236" n="e" s="T442">barɨ͡am. </ts>
            </ts>
            <ts e="T455" id="Seg_3237" n="sc" s="T447">
               <ts e="T448" id="Seg_3239" n="e" s="T447">– ((LAUGH)) </ts>
               <ts e="T449" id="Seg_3241" n="e" s="T448">Ješʼo </ts>
               <ts e="T450" id="Seg_3243" n="e" s="T449">nʼe </ts>
               <ts e="T451" id="Seg_3245" n="e" s="T450">znaju. </ts>
               <ts e="T452" id="Seg_3247" n="e" s="T451">Di͡eččiler </ts>
               <ts e="T453" id="Seg_3249" n="e" s="T452">kɨrdʼagastar: </ts>
               <ts e="T454" id="Seg_3251" n="e" s="T453">"Taŋaraga </ts>
               <ts e="T455" id="Seg_3253" n="e" s="T454">barɨ͡akkɨt." </ts>
            </ts>
            <ts e="T489" id="Seg_3254" n="sc" s="T456">
               <ts e="T457" id="Seg_3256" n="e" s="T456">Ügüs </ts>
               <ts e="T458" id="Seg_3258" n="e" s="T457">ogoloːk </ts>
               <ts e="T459" id="Seg_3260" n="e" s="T458">kihi, </ts>
               <ts e="T460" id="Seg_3262" n="e" s="T459">taŋaraga </ts>
               <ts e="T461" id="Seg_3264" n="e" s="T460">barɨ͡am. </ts>
               <ts e="T462" id="Seg_3266" n="e" s="T461">Kihini </ts>
               <ts e="T463" id="Seg_3268" n="e" s="T462">kuhagannɨk </ts>
               <ts e="T464" id="Seg_3270" n="e" s="T463">haŋarbappɨn. </ts>
               <ts e="T465" id="Seg_3272" n="e" s="T464">Kihini </ts>
               <ts e="T466" id="Seg_3274" n="e" s="T465">kɨraspappɨn. </ts>
               <ts e="T467" id="Seg_3276" n="e" s="T466">Anʼiː </ts>
               <ts e="T468" id="Seg_3278" n="e" s="T467">bu͡olu͡o. </ts>
               <ts e="T469" id="Seg_3280" n="e" s="T468">Ol </ts>
               <ts e="T470" id="Seg_3282" n="e" s="T469">ihin </ts>
               <ts e="T471" id="Seg_3284" n="e" s="T470">min </ts>
               <ts e="T472" id="Seg_3286" n="e" s="T471">kihini </ts>
               <ts e="T473" id="Seg_3288" n="e" s="T472">kɨraha </ts>
               <ts e="T474" id="Seg_3290" n="e" s="T473">hataːččɨta </ts>
               <ts e="T475" id="Seg_3292" n="e" s="T474">hu͡okpun, </ts>
               <ts e="T476" id="Seg_3294" n="e" s="T475">anʼiː </ts>
               <ts e="T477" id="Seg_3296" n="e" s="T476">bu͡olu͡o. </ts>
               <ts e="T478" id="Seg_3298" n="e" s="T477">Kim </ts>
               <ts e="T479" id="Seg_3300" n="e" s="T478">kiːrbit, </ts>
               <ts e="T480" id="Seg_3302" n="e" s="T479">ahatan </ts>
               <ts e="T481" id="Seg_3304" n="e" s="T480">ihebin. </ts>
               <ts e="T482" id="Seg_3306" n="e" s="T481">Etim </ts>
               <ts e="T483" id="Seg_3308" n="e" s="T482">kotunar </ts>
               <ts e="T484" id="Seg_3310" n="e" s="T483">erdekpine. </ts>
               <ts e="T485" id="Seg_3312" n="e" s="T484">Anɨ </ts>
               <ts e="T486" id="Seg_3314" n="e" s="T485">kotummappɨn, </ts>
               <ts e="T487" id="Seg_3316" n="e" s="T486">Dunʼasam </ts>
               <ts e="T488" id="Seg_3318" n="e" s="T487">ere </ts>
               <ts e="T489" id="Seg_3320" n="e" s="T488">ebit. </ts>
            </ts>
            <ts e="T536" id="Seg_3321" n="sc" s="T490">
               <ts e="T491" id="Seg_3323" n="e" s="T490">– Üčügej. </ts>
               <ts e="T492" id="Seg_3325" n="e" s="T491">Kiniːtim </ts>
               <ts e="T493" id="Seg_3327" n="e" s="T492">hürdeːk </ts>
               <ts e="T494" id="Seg_3329" n="e" s="T493">üčügej </ts>
               <ts e="T495" id="Seg_3331" n="e" s="T494">dʼaktar. </ts>
               <ts e="T496" id="Seg_3333" n="e" s="T495">Üčügej </ts>
               <ts e="T497" id="Seg_3335" n="e" s="T496">dʼaktattar </ts>
               <ts e="T498" id="Seg_3337" n="e" s="T497">kiniːtterim </ts>
               <ts e="T499" id="Seg_3339" n="e" s="T498">tak </ts>
               <ts e="T500" id="Seg_3341" n="e" s="T499">da. </ts>
               <ts e="T501" id="Seg_3343" n="e" s="T500">Ol </ts>
               <ts e="T502" id="Seg_3345" n="e" s="T501">kajdak </ts>
               <ts e="T503" id="Seg_3347" n="e" s="T502">da </ts>
               <ts e="T504" id="Seg_3349" n="e" s="T503">bu͡ollun </ts>
               <ts e="T505" id="Seg_3351" n="e" s="T504">min </ts>
               <ts e="T506" id="Seg_3353" n="e" s="T505">kɨhallɨbappɨn </ts>
               <ts e="T507" id="Seg_3355" n="e" s="T506">ginilerge. </ts>
               <ts e="T508" id="Seg_3357" n="e" s="T507">Keristinner </ts>
               <ts e="T509" id="Seg_3359" n="e" s="T508">da </ts>
               <ts e="T510" id="Seg_3361" n="e" s="T509">kersibetinner </ts>
               <ts e="T511" id="Seg_3363" n="e" s="T510">da, </ts>
               <ts e="T512" id="Seg_3365" n="e" s="T511">mini͡eke </ts>
               <ts e="T513" id="Seg_3367" n="e" s="T512">naːdata </ts>
               <ts e="T514" id="Seg_3369" n="e" s="T513">hu͡ok, </ts>
               <ts e="T515" id="Seg_3371" n="e" s="T514">tuspa </ts>
               <ts e="T516" id="Seg_3373" n="e" s="T515">ɨ͡allar </ts>
               <ts e="T517" id="Seg_3375" n="e" s="T516">bu͡o. </ts>
               <ts e="T518" id="Seg_3377" n="e" s="T517">Tu͡okka </ts>
               <ts e="T519" id="Seg_3379" n="e" s="T518">kɨhallɨ͡amɨj </ts>
               <ts e="T520" id="Seg_3381" n="e" s="T519">onuga? </ts>
               <ts e="T521" id="Seg_3383" n="e" s="T520">Min </ts>
               <ts e="T522" id="Seg_3385" n="e" s="T521">ologum </ts>
               <ts e="T523" id="Seg_3387" n="e" s="T522">büppüte, </ts>
               <ts e="T524" id="Seg_3389" n="e" s="T523">giniler </ts>
               <ts e="T525" id="Seg_3391" n="e" s="T524">oloktorun </ts>
               <ts e="T526" id="Seg_3393" n="e" s="T525">tɨːppappɨn </ts>
               <ts e="T527" id="Seg_3395" n="e" s="T526">min. </ts>
               <ts e="T528" id="Seg_3397" n="e" s="T527">Olordunnar </ts>
               <ts e="T529" id="Seg_3399" n="e" s="T528">šʼastlʼivɨe, </ts>
               <ts e="T530" id="Seg_3401" n="e" s="T529">staraːtsa </ts>
               <ts e="T531" id="Seg_3403" n="e" s="T530">mʼinʼa. </ts>
               <ts e="T532" id="Seg_3405" n="e" s="T531">Elete, </ts>
               <ts e="T533" id="Seg_3407" n="e" s="T532">onton </ts>
               <ts e="T534" id="Seg_3409" n="e" s="T533">ke </ts>
               <ts e="T535" id="Seg_3411" n="e" s="T534">tuːgu </ts>
               <ts e="T536" id="Seg_3413" n="e" s="T535">detegin? </ts>
            </ts>
            <ts e="T552" id="Seg_3414" n="sc" s="T550">
               <ts e="T552" id="Seg_3416" n="e" s="T550">– Eː. </ts>
            </ts>
            <ts e="T563" id="Seg_3417" n="sc" s="T560">
               <ts e="T563" id="Seg_3419" n="e" s="T560">– Nʼelzʼa. </ts>
            </ts>
            <ts e="T644" id="Seg_3420" n="sc" s="T567">
               <ts e="T568" id="Seg_3422" n="e" s="T567">– Kanʼešna, </ts>
               <ts e="T569" id="Seg_3424" n="e" s="T568">kanʼešna </ts>
               <ts e="T570" id="Seg_3426" n="e" s="T569">nʼelzʼa. </ts>
               <ts e="T571" id="Seg_3428" n="e" s="T570">Istibetter </ts>
               <ts e="T572" id="Seg_3430" n="e" s="T571">törüt </ts>
               <ts e="T573" id="Seg_3432" n="e" s="T572">iti </ts>
               <ts e="T574" id="Seg_3434" n="e" s="T573">baran </ts>
               <ts e="T575" id="Seg_3436" n="e" s="T574">kaːlallar. </ts>
               <ts e="T576" id="Seg_3438" n="e" s="T575">Iti </ts>
               <ts e="T577" id="Seg_3440" n="e" s="T576">bu </ts>
               <ts e="T578" id="Seg_3442" n="e" s="T577">erim </ts>
               <ts e="T579" id="Seg_3444" n="e" s="T578">edʼiːjin, </ts>
               <ts e="T580" id="Seg_3446" n="e" s="T579">eː, </ts>
               <ts e="T581" id="Seg_3448" n="e" s="T580">erim </ts>
               <ts e="T582" id="Seg_3450" n="e" s="T581">baltɨtɨn </ts>
               <ts e="T583" id="Seg_3452" n="e" s="T582">ogoloro </ts>
               <ts e="T584" id="Seg_3454" n="e" s="T583">iti </ts>
               <ts e="T585" id="Seg_3456" n="e" s="T584">baraːččɨlar </ts>
               <ts e="T586" id="Seg_3458" n="e" s="T585">(oŋo-) </ts>
               <ts e="T587" id="Seg_3460" n="e" s="T586">oŋu͡oktarga </ts>
               <ts e="T588" id="Seg_3462" n="e" s="T587">min </ts>
               <ts e="T589" id="Seg_3464" n="e" s="T588">ogolorbun </ts>
               <ts e="T590" id="Seg_3466" n="e" s="T589">kɨtta. </ts>
               <ts e="T591" id="Seg_3468" n="e" s="T590">Inʼe </ts>
               <ts e="T592" id="Seg_3470" n="e" s="T591">inʼete </ts>
               <ts e="T593" id="Seg_3472" n="e" s="T592">ölbütün </ts>
               <ts e="T594" id="Seg_3474" n="e" s="T593">kenne </ts>
               <ts e="T595" id="Seg_3476" n="e" s="T594">haŋardɨː </ts>
               <ts e="T596" id="Seg_3478" n="e" s="T595">keler </ts>
               <ts e="T597" id="Seg_3480" n="e" s="T596">küččügüj </ts>
               <ts e="T598" id="Seg_3482" n="e" s="T597">u͡ola, </ts>
               <ts e="T599" id="Seg_3484" n="e" s="T598">ontuŋ </ts>
               <ts e="T600" id="Seg_3486" n="e" s="T599">barar, </ts>
               <ts e="T601" id="Seg_3488" n="e" s="T600">onu </ts>
               <ts e="T602" id="Seg_3490" n="e" s="T601">ataːrsallar </ts>
               <ts e="T603" id="Seg_3492" n="e" s="T602">iti. </ts>
               <ts e="T604" id="Seg_3494" n="e" s="T603">Onu </ts>
               <ts e="T605" id="Seg_3496" n="e" s="T604">"barɨmaŋ" </ts>
               <ts e="T606" id="Seg_3498" n="e" s="T605">diːbin, </ts>
               <ts e="T607" id="Seg_3500" n="e" s="T606">istibetter </ts>
               <ts e="T608" id="Seg_3502" n="e" s="T607">bu͡o. </ts>
               <ts e="T609" id="Seg_3504" n="e" s="T608">Tugu </ts>
               <ts e="T610" id="Seg_3506" n="e" s="T609">gɨna </ts>
               <ts e="T611" id="Seg_3508" n="e" s="T610">barallara </ts>
               <ts e="T612" id="Seg_3510" n="e" s="T611">dʼürü? </ts>
               <ts e="T613" id="Seg_3512" n="e" s="T612">Gri͡ex </ts>
               <ts e="T614" id="Seg_3514" n="e" s="T613">bu͡o. </ts>
               <ts e="T615" id="Seg_3516" n="e" s="T614">Anɨ </ts>
               <ts e="T616" id="Seg_3518" n="e" s="T615">kihi </ts>
               <ts e="T617" id="Seg_3520" n="e" s="T616">hir </ts>
               <ts e="T618" id="Seg_3522" n="e" s="T617">ajɨ </ts>
               <ts e="T619" id="Seg_3524" n="e" s="T618">ölö </ts>
               <ts e="T620" id="Seg_3526" n="e" s="T619">hɨtar </ts>
               <ts e="T621" id="Seg_3528" n="e" s="T620">kihi </ts>
               <ts e="T622" id="Seg_3530" n="e" s="T621">kuttanɨ͡ak. </ts>
               <ts e="T623" id="Seg_3532" n="e" s="T622">Anɨ </ts>
               <ts e="T624" id="Seg_3534" n="e" s="T623">itinne </ts>
               <ts e="T625" id="Seg_3536" n="e" s="T624">biːr </ts>
               <ts e="T626" id="Seg_3538" n="e" s="T625">emeːksin </ts>
               <ts e="T627" id="Seg_3540" n="e" s="T626">öllö, </ts>
               <ts e="T628" id="Seg_3542" n="e" s="T627">anɨ </ts>
               <ts e="T629" id="Seg_3544" n="e" s="T628">biːr </ts>
               <ts e="T630" id="Seg_3546" n="e" s="T629">ogo </ts>
               <ts e="T631" id="Seg_3548" n="e" s="T630">kelen </ts>
               <ts e="T632" id="Seg_3550" n="e" s="T631">ihen </ts>
               <ts e="T633" id="Seg_3552" n="e" s="T632">ölbüt </ts>
               <ts e="T634" id="Seg_3554" n="e" s="T633">emi͡e, </ts>
               <ts e="T635" id="Seg_3556" n="e" s="T634">kaːri͡ennaːk </ts>
               <ts e="T636" id="Seg_3558" n="e" s="T635">ogobut. </ts>
               <ts e="T637" id="Seg_3560" n="e" s="T636">Hürdeːk </ts>
               <ts e="T638" id="Seg_3562" n="e" s="T637">üčügej </ts>
               <ts e="T639" id="Seg_3564" n="e" s="T638">ogo </ts>
               <ts e="T640" id="Seg_3566" n="e" s="T639">ete. </ts>
               <ts e="T641" id="Seg_3568" n="e" s="T640">Buraːnɨnan </ts>
               <ts e="T642" id="Seg_3570" n="e" s="T641">taskajdana </ts>
               <ts e="T643" id="Seg_3572" n="e" s="T642">hɨldʼar </ts>
               <ts e="T644" id="Seg_3574" n="e" s="T643">ete. </ts>
            </ts>
            <ts e="T666" id="Seg_3575" n="sc" s="T661">
               <ts e="T662" id="Seg_3577" n="e" s="T661">– Heː, </ts>
               <ts e="T663" id="Seg_3579" n="e" s="T662">bu </ts>
               <ts e="T664" id="Seg_3581" n="e" s="T663">tabaːkta </ts>
               <ts e="T665" id="Seg_3583" n="e" s="T664">bɨragɨ͡akkɨn </ts>
               <ts e="T666" id="Seg_3585" n="e" s="T665">naːda. </ts>
            </ts>
            <ts e="T674" id="Seg_3586" n="sc" s="T668">
               <ts e="T670" id="Seg_3588" n="e" s="T668">– Aragiː </ts>
               <ts e="T671" id="Seg_3590" n="e" s="T670">istekterine </ts>
               <ts e="T672" id="Seg_3592" n="e" s="T671">aragiːta </ts>
               <ts e="T673" id="Seg_3594" n="e" s="T672">kutu͡okkun </ts>
               <ts e="T674" id="Seg_3596" n="e" s="T673">naːda. </ts>
            </ts>
            <ts e="T676" id="Seg_3597" n="sc" s="T675">
               <ts e="T676" id="Seg_3599" n="e" s="T675">– Elete. </ts>
            </ts>
            <ts e="T719" id="Seg_3600" n="sc" s="T687">
               <ts e="T688" id="Seg_3602" n="e" s="T687">– Rʼedka </ts>
               <ts e="T689" id="Seg_3604" n="e" s="T688">rʼedka </ts>
               <ts e="T690" id="Seg_3606" n="e" s="T689">barallar </ts>
               <ts e="T691" id="Seg_3608" n="e" s="T690">teːteleriger </ts>
               <ts e="T692" id="Seg_3610" n="e" s="T691">oŋu͡oktarga. </ts>
               <ts e="T693" id="Seg_3612" n="e" s="T692">Inʼelerim </ts>
               <ts e="T694" id="Seg_3614" n="e" s="T693">oŋu͡oktarɨgar </ts>
               <ts e="T695" id="Seg_3616" n="e" s="T694">baraːččɨlar </ts>
               <ts e="T696" id="Seg_3618" n="e" s="T695">taːk </ts>
               <ts e="T697" id="Seg_3620" n="e" s="T696">da. </ts>
               <ts e="T698" id="Seg_3622" n="e" s="T697">Onnuk </ts>
               <ts e="T699" id="Seg_3624" n="e" s="T698">baːr </ts>
               <ts e="T700" id="Seg_3626" n="e" s="T699">ojuttar </ts>
               <ts e="T701" id="Seg_3628" n="e" s="T700">oŋu͡oktara </ts>
               <ts e="T702" id="Seg_3630" n="e" s="T701">baːllar, </ts>
               <ts e="T703" id="Seg_3632" n="e" s="T702">ol </ts>
               <ts e="T704" id="Seg_3634" n="e" s="T703">kuhagan </ts>
               <ts e="T705" id="Seg_3636" n="e" s="T704">bu͡o, </ts>
               <ts e="T706" id="Seg_3638" n="e" s="T705">samaːttarɨŋ. </ts>
               <ts e="T707" id="Seg_3640" n="e" s="T706">Anʼiː </ts>
               <ts e="T708" id="Seg_3642" n="e" s="T707">bu͡olaːččɨ. </ts>
               <ts e="T709" id="Seg_3644" n="e" s="T708">Istibetter </ts>
               <ts e="T710" id="Seg_3646" n="e" s="T709">anɨ </ts>
               <ts e="T711" id="Seg_3648" n="e" s="T710">maladʼožtar </ts>
               <ts e="T712" id="Seg_3650" n="e" s="T711">törüt. </ts>
               <ts e="T713" id="Seg_3652" n="e" s="T712">((LAUGH)). </ts>
               <ts e="T714" id="Seg_3654" n="e" s="T713">Durugoj </ts>
               <ts e="T715" id="Seg_3656" n="e" s="T714">narod. </ts>
               <ts e="T716" id="Seg_3658" n="e" s="T715">Havsʼem </ts>
               <ts e="T717" id="Seg_3660" n="e" s="T716">drugoj </ts>
               <ts e="T719" id="Seg_3662" n="e" s="T717">narot. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KiPP">
            <ta e="T0" id="Seg_3663" s="T1">KiPP_2009_Belief_nar.KiPP.001 (001.001)</ta>
            <ta e="T12" id="Seg_3664" s="T5">KiPP_2009_Belief_nar.KiPP.002 (001.002)</ta>
            <ta e="T17" id="Seg_3665" s="T12">KiPP_2009_Belief_nar.KiPP.003 (001.003)</ta>
            <ta e="T24" id="Seg_3666" s="T17">KiPP_2009_Belief_nar.KiPP.004 (001.004)</ta>
            <ta e="T32" id="Seg_3667" s="T24">KiPP_2009_Belief_nar.KiPP.005 (001.005)</ta>
            <ta e="T39" id="Seg_3668" s="T32">KiPP_2009_Belief_nar.KiPP.006 (001.006)</ta>
            <ta e="T46" id="Seg_3669" s="T39">KiPP_2009_Belief_nar.KiPP.007 (001.007)</ta>
            <ta e="T50" id="Seg_3670" s="T46">KiPP_2009_Belief_nar.KiPP.008 (001.008)</ta>
            <ta e="T57" id="Seg_3671" s="T50">KiPP_2009_Belief_nar.KiPP.009 (001.009)</ta>
            <ta e="T61" id="Seg_3672" s="T57">KiPP_2009_Belief_nar.KiPP.010 (001.010)</ta>
            <ta e="T84" id="Seg_3673" s="T66">KiPP_2009_Belief_nar.KiPP.011 (001.012)</ta>
            <ta e="T723" id="Seg_3674" s="T84">KiPP_2009_Belief_nar.KiPP.012 (001.013)</ta>
            <ta e="T93" id="Seg_3675" s="T91">KiPP_2009_Belief_nar.KiPP.013 (001.014)</ta>
            <ta e="T100" id="Seg_3676" s="T93">KiPP_2009_Belief_nar.KiPP.014 (001.015)</ta>
            <ta e="T724" id="Seg_3677" s="T100">KiPP_2009_Belief_nar.KiPP.015 (001.016)</ta>
            <ta e="T725" id="Seg_3678" s="T106">KiPP_2009_Belief_nar.KiPP.016 (001.017)</ta>
            <ta e="T118" id="Seg_3679" s="T115">KiPP_2009_Belief_nar.KiPP.017 (001.018)</ta>
            <ta e="T130" id="Seg_3680" s="T118">KiPP_2009_Belief_nar.KiPP.018 (001.019)</ta>
            <ta e="T134" id="Seg_3681" s="T130">KiPP_2009_Belief_nar.KiPP.019 (001.020)</ta>
            <ta e="T141" id="Seg_3682" s="T134">KiPP_2009_Belief_nar.KiPP.020 (001.021)</ta>
            <ta e="T153" id="Seg_3683" s="T141">KiPP_2009_Belief_nar.KiPP.021 (001.022)</ta>
            <ta e="T154" id="Seg_3684" s="T153">KiPP_2009_Belief_nar.KiPP.022 (001.023)</ta>
            <ta e="T161" id="Seg_3685" s="T156">KiPP_2009_Belief_nar.KiPP.023 (001.025)</ta>
            <ta e="T165" id="Seg_3686" s="T161">KiPP_2009_Belief_nar.KiPP.024 (001.026)</ta>
            <ta e="T172" id="Seg_3687" s="T165">KiPP_2009_Belief_nar.KiPP.025 (001.027)</ta>
            <ta e="T183" id="Seg_3688" s="T172">KiPP_2009_Belief_nar.KiPP.026 (001.028)</ta>
            <ta e="T193" id="Seg_3689" s="T183">KiPP_2009_Belief_nar.KiPP.027 (001.029)</ta>
            <ta e="T202" id="Seg_3690" s="T193">KiPP_2009_Belief_nar.KiPP.028 (001.030)</ta>
            <ta e="T207" id="Seg_3691" s="T202">KiPP_2009_Belief_nar.KiPP.029 (001.031)</ta>
            <ta e="T726" id="Seg_3692" s="T207">KiPP_2009_Belief_nar.KiPP.030 (001.032)</ta>
            <ta e="T218" id="Seg_3693" s="T211">KiPP_2009_Belief_nar.KiPP.031 (001.033)</ta>
            <ta e="T223" id="Seg_3694" s="T218">KiPP_2009_Belief_nar.KiPP.032 (001.034)</ta>
            <ta e="T727" id="Seg_3695" s="T223">KiPP_2009_Belief_nar.KiPP.033 (001.035)</ta>
            <ta e="T241" id="Seg_3696" s="T234">KiPP_2009_Belief_nar.KiPP.034 (001.036)</ta>
            <ta e="T247" id="Seg_3697" s="T241">KiPP_2009_Belief_nar.KiPP.035 (001.037)</ta>
            <ta e="T263" id="Seg_3698" s="T247">KiPP_2009_Belief_nar.KiPP.036 (001.038)</ta>
            <ta e="T265" id="Seg_3699" s="T263">KiPP_2009_Belief_nar.KiPP.037 (001.039)</ta>
            <ta e="T276" id="Seg_3700" s="T265">KiPP_2009_Belief_nar.KiPP.038 (001.040)</ta>
            <ta e="T281" id="Seg_3701" s="T276">KiPP_2009_Belief_nar.KiPP.039 (001.041)</ta>
            <ta e="T292" id="Seg_3702" s="T281">KiPP_2009_Belief_nar.KiPP.040 (001.042)</ta>
            <ta e="T298" id="Seg_3703" s="T292">KiPP_2009_Belief_nar.KiPP.041 (001.043)</ta>
            <ta e="T303" id="Seg_3704" s="T298">KiPP_2009_Belief_nar.KiPP.042 (001.043)</ta>
            <ta e="T312" id="Seg_3705" s="T303">KiPP_2009_Belief_nar.KiPP.043 (001.044)</ta>
            <ta e="T319" id="Seg_3706" s="T312">KiPP_2009_Belief_nar.KiPP.044 (001.045)</ta>
            <ta e="T321" id="Seg_3707" s="T319">KiPP_2009_Belief_nar.KiPP.045 (001.046)</ta>
            <ta e="T331" id="Seg_3708" s="T321">KiPP_2009_Belief_nar.KiPP.046 (001.047)</ta>
            <ta e="T332" id="Seg_3709" s="T331">KiPP_2009_Belief_nar.KiPP.047 (001.048)</ta>
            <ta e="T335" id="Seg_3710" s="T332">KiPP_2009_Belief_nar.KiPP.048 (001.049)</ta>
            <ta e="T338" id="Seg_3711" s="T335">KiPP_2009_Belief_nar.KiPP.049 (001.050)</ta>
            <ta e="T341" id="Seg_3712" s="T338">KiPP_2009_Belief_nar.KiPP.050 (001.051)</ta>
            <ta e="T347" id="Seg_3713" s="T341">KiPP_2009_Belief_nar.KiPP.051 (001.052)</ta>
            <ta e="T351" id="Seg_3714" s="T347">KiPP_2009_Belief_nar.KiPP.052 (001.053)</ta>
            <ta e="T358" id="Seg_3715" s="T351">KiPP_2009_Belief_nar.KiPP.053 (001.054)</ta>
            <ta e="T728" id="Seg_3716" s="T358">KiPP_2009_Belief_nar.KiPP.054 (001.055)</ta>
            <ta e="T364" id="Seg_3717" s="T362">KiPP_2009_Belief_nar.KiPP.055 (001.056)</ta>
            <ta e="T371" id="Seg_3718" s="T364">KiPP_2009_Belief_nar.KiPP.056 (001.057)</ta>
            <ta e="T375" id="Seg_3719" s="T371">KiPP_2009_Belief_nar.KiPP.057 (001.058)</ta>
            <ta e="T381" id="Seg_3720" s="T375">KiPP_2009_Belief_nar.KiPP.058 (001.059)</ta>
            <ta e="T382" id="Seg_3721" s="T381">KiPP_2009_Belief_nar.KiPP.059 (001.060)</ta>
            <ta e="T392" id="Seg_3722" s="T382">KiPP_2009_Belief_nar.KiPP.060 (001.061)</ta>
            <ta e="T395" id="Seg_3723" s="T392">KiPP_2009_Belief_nar.KiPP.061 (001.063)</ta>
            <ta e="T400" id="Seg_3724" s="T395">KiPP_2009_Belief_nar.KiPP.062 (001.064)</ta>
            <ta e="T409" id="Seg_3725" s="T400">KiPP_2009_Belief_nar.KiPP.063 (001.065)</ta>
            <ta e="T417" id="Seg_3726" s="T409">KiPP_2009_Belief_nar.KiPP.064 (001.066)</ta>
            <ta e="T419" id="Seg_3727" s="T417">KiPP_2009_Belief_nar.KiPP.065 (001.068)</ta>
            <ta e="T424" id="Seg_3728" s="T422">KiPP_2009_Belief_nar.KiPP.066 (001.070)</ta>
            <ta e="T432" id="Seg_3729" s="T428">KiPP_2009_Belief_nar.KiPP.067 (001.073)</ta>
            <ta e="T436" id="Seg_3730" s="T432">KiPP_2009_Belief_nar.KiPP.068 (001.074)</ta>
            <ta e="T441" id="Seg_3731" s="T439">KiPP_2009_Belief_nar.KiPP.069 (001.076)</ta>
            <ta e="T443" id="Seg_3732" s="T441">KiPP_2009_Belief_nar.KiPP.070 (001.077)</ta>
            <ta e="T451" id="Seg_3733" s="T447">KiPP_2009_Belief_nar.KiPP.071 (001.079)</ta>
            <ta e="T453" id="Seg_3734" s="T451">KiPP_2009_Belief_nar.KiPP.072 (001.080)</ta>
            <ta e="T455" id="Seg_3735" s="T453">KiPP_2009_Belief_nar.KiPP.073 (001.080)</ta>
            <ta e="T461" id="Seg_3736" s="T456">KiPP_2009_Belief_nar.KiPP.074 (001.081)</ta>
            <ta e="T464" id="Seg_3737" s="T461">KiPP_2009_Belief_nar.KiPP.075 (001.082)</ta>
            <ta e="T466" id="Seg_3738" s="T464">KiPP_2009_Belief_nar.KiPP.076 (001.083)</ta>
            <ta e="T468" id="Seg_3739" s="T466">KiPP_2009_Belief_nar.KiPP.077 (001.084)</ta>
            <ta e="T477" id="Seg_3740" s="T468">KiPP_2009_Belief_nar.KiPP.078 (001.085)</ta>
            <ta e="T481" id="Seg_3741" s="T477">KiPP_2009_Belief_nar.KiPP.079 (001.086)</ta>
            <ta e="T484" id="Seg_3742" s="T481">KiPP_2009_Belief_nar.KiPP.080 (001.087)</ta>
            <ta e="T489" id="Seg_3743" s="T484">KiPP_2009_Belief_nar.KiPP.081 (001.088)</ta>
            <ta e="T491" id="Seg_3744" s="T490">KiPP_2009_Belief_nar.KiPP.082 (001.090)</ta>
            <ta e="T495" id="Seg_3745" s="T491">KiPP_2009_Belief_nar.KiPP.083 (001.091)</ta>
            <ta e="T500" id="Seg_3746" s="T495">KiPP_2009_Belief_nar.KiPP.084 (001.092)</ta>
            <ta e="T507" id="Seg_3747" s="T500">KiPP_2009_Belief_nar.KiPP.085 (001.093)</ta>
            <ta e="T517" id="Seg_3748" s="T507">KiPP_2009_Belief_nar.KiPP.086 (001.094)</ta>
            <ta e="T520" id="Seg_3749" s="T517">KiPP_2009_Belief_nar.KiPP.087 (001.095)</ta>
            <ta e="T527" id="Seg_3750" s="T520">KiPP_2009_Belief_nar.KiPP.088 (001.096)</ta>
            <ta e="T531" id="Seg_3751" s="T527">KiPP_2009_Belief_nar.KiPP.089 (001.097)</ta>
            <ta e="T536" id="Seg_3752" s="T531">KiPP_2009_Belief_nar.KiPP.090 (001.098)</ta>
            <ta e="T552" id="Seg_3753" s="T550">KiPP_2009_Belief_nar.KiPP.091 (001.100)</ta>
            <ta e="T563" id="Seg_3754" s="T560">KiPP_2009_Belief_nar.KiPP.092 (001.102)</ta>
            <ta e="T570" id="Seg_3755" s="T567">KiPP_2009_Belief_nar.KiPP.093 (001.103)</ta>
            <ta e="T575" id="Seg_3756" s="T570">KiPP_2009_Belief_nar.KiPP.094 (001.104)</ta>
            <ta e="T590" id="Seg_3757" s="T575">KiPP_2009_Belief_nar.KiPP.095 (001.105)</ta>
            <ta e="T603" id="Seg_3758" s="T590">KiPP_2009_Belief_nar.KiPP.096 (001.106)</ta>
            <ta e="T608" id="Seg_3759" s="T603">KiPP_2009_Belief_nar.KiPP.097 (001.107)</ta>
            <ta e="T612" id="Seg_3760" s="T608">KiPP_2009_Belief_nar.KiPP.098 (001.108)</ta>
            <ta e="T614" id="Seg_3761" s="T612">KiPP_2009_Belief_nar.KiPP.099 (001.109)</ta>
            <ta e="T622" id="Seg_3762" s="T614">KiPP_2009_Belief_nar.KiPP.100 (001.110)</ta>
            <ta e="T636" id="Seg_3763" s="T622">KiPP_2009_Belief_nar.KiPP.101 (001.111)</ta>
            <ta e="T640" id="Seg_3764" s="T636">KiPP_2009_Belief_nar.KiPP.102 (001.112)</ta>
            <ta e="T644" id="Seg_3765" s="T640">KiPP_2009_Belief_nar.KiPP.103 (001.113)</ta>
            <ta e="T666" id="Seg_3766" s="T661">KiPP_2009_Belief_nar.KiPP.104 (001.115)</ta>
            <ta e="T674" id="Seg_3767" s="T668">KiPP_2009_Belief_nar.KiPP.105 (001.117)</ta>
            <ta e="T676" id="Seg_3768" s="T675">KiPP_2009_Belief_nar.KiPP.106 (001.119)</ta>
            <ta e="T692" id="Seg_3769" s="T687">KiPP_2009_Belief_nar.KiPP.107 (001.122)</ta>
            <ta e="T697" id="Seg_3770" s="T692">KiPP_2009_Belief_nar.KiPP.108 (001.123)</ta>
            <ta e="T706" id="Seg_3771" s="T697">KiPP_2009_Belief_nar.KiPP.109 (001.124)</ta>
            <ta e="T708" id="Seg_3772" s="T706">KiPP_2009_Belief_nar.KiPP.110 (001.125)</ta>
            <ta e="T712" id="Seg_3773" s="T708">KiPP_2009_Belief_nar.KiPP.111 (001.126)</ta>
            <ta e="T713" id="Seg_3774" s="T712">KiPP_2009_Belief_nar.KiPP.112 (001.127)</ta>
            <ta e="T715" id="Seg_3775" s="T713">KiPP_2009_Belief_nar.KiPP.113 (001.128)</ta>
            <ta e="T719" id="Seg_3776" s="T715">KiPP_2009_Belief_nar.KiPP.114 (001.129)</ta>
         </annotation>
         <annotation name="st" tierref="st-KiPP" />
         <annotation name="ts" tierref="ts-KiPP">
            <ta e="T0" id="Seg_3777" s="T1">– Tu͡oktan anʼiː diːgin du͡o. </ta>
            <ta e="T12" id="Seg_3778" s="T5">Anʼiː bu͡ollagɨna, bɨlɨr ojuttar baːr bu͡olaːččɨlar, samaːttar. </ta>
            <ta e="T17" id="Seg_3779" s="T12">Onnuktar dʼi͡elerin ergijerge hatammat kuhagan. </ta>
            <ta e="T24" id="Seg_3780" s="T17">Ergijbekke hɨldʼɨ͡akkɨn anʼiː bu͡olaːččɨ, ol aːta anʼiː. </ta>
            <ta e="T32" id="Seg_3781" s="T24">Dʼaktar bu͡ollagɨna er kihi taŋahɨn tepser, emi͡e anʼiː. </ta>
            <ta e="T39" id="Seg_3782" s="T32">Čistej bu͡olu͡ogun, er kihi čeberdik hɨldʼɨ͡agɨn naːda. </ta>
            <ta e="T46" id="Seg_3783" s="T39">Dʼaktar ɨrbaːkɨta hu͡ok hɨldʼar emi͡e anʼiː urut. </ta>
            <ta e="T50" id="Seg_3784" s="T46">ɨrbaːkɨ keti͡egin naːda, pɨlaːtije. </ta>
            <ta e="T57" id="Seg_3785" s="T50">Er kihi kördük (hɨr-), anɨ barɨkaːttara hɨ͡alɨjalaːktar. </ta>
            <ta e="T61" id="Seg_3786" s="T57">Iti gri͡ex bu͡o ontuŋ. </ta>
            <ta e="T84" id="Seg_3787" s="T66">– Eː, če, barɨta er kihi͡eke di͡eri kuhagan ol, dʼaktar dʼaktar kördük bu͡olu͡ogun naːda, ɨrbaːkɨlaːk, pɨlaːttaːk bu͡olu͡ogun naːda. </ta>
            <ta e="T723" id="Seg_3788" s="T84">Bergeheni baga ketimne onto keti͡ekke naːda saːpkɨni. </ta>
            <ta e="T93" id="Seg_3789" s="T91">Iti onnuktar. </ta>
            <ta e="T100" id="Seg_3790" s="T93">Onton erge bararɨŋ hagɨna iliː oksohallar taŋaraga. </ta>
            <ta e="T724" id="Seg_3791" s="T100">((NOISE)) Itigirdik bi͡ereller iliːlerin erge barar kihi. </ta>
            <ta e="T725" id="Seg_3792" s="T106">Iliː berseller, oččogo iliːlerin bi͡erdekterine dʼaktar bu͡olar ol kihi. </ta>
            <ta e="T118" id="Seg_3793" s="T115">Onnuk abɨčajdar onnuk. </ta>
            <ta e="T130" id="Seg_3794" s="T118">Heː, küːske bi͡ereller bu͡o kihini, bagar da bagarɨma, da bi͡ereller him biːr. </ta>
            <ta e="T134" id="Seg_3795" s="T130">ɨtɨː-ɨtɨː bara turu͡oŋ onuga. </ta>
            <ta e="T141" id="Seg_3796" s="T134">Ol kördük min bara turbutum erge ɨtɨː-ɨtɨːbɨn. </ta>
            <ta e="T153" id="Seg_3797" s="T141">U͡on alta dʼɨllaːkpar, ke tu͡ok kihite erge barɨ͡aj ol u͡on alta dʼɨllaːkka. </ta>
            <ta e="T154" id="Seg_3798" s="T153">Bi͡erbittere. </ta>
            <ta e="T161" id="Seg_3799" s="T156">– Hapsi͡em ogo, ɨraːs etim haːtar. </ta>
            <ta e="T165" id="Seg_3800" s="T161">Mʼesnaja da hu͡ok etim. </ta>
            <ta e="T172" id="Seg_3801" s="T165">Onton iti tagɨsta ulakan kɨːhɨm anɨ bu͡o. </ta>
            <ta e="T183" id="Seg_3802" s="T172">Iti iti ogom u͡on hette, u͡on agɨs dʼɨllaːkpar töröːbüt ogo iti. </ta>
            <ta e="T193" id="Seg_3803" s="T183">Dʼe onton kɨrdʼan kɨrdʼammɨn iti kojut kojutun onton du͡o ((LAUGH)). </ta>
            <ta e="T202" id="Seg_3804" s="T193">Ogolonon da ogolonon ihegin bu͡o lʼubovʼ tʼomnaja lʼubovʼ ((LAUGH)). </ta>
            <ta e="T207" id="Seg_3805" s="T202">Hürdeːk üčügej kihi ete erim. </ta>
            <ta e="T726" id="Seg_3806" s="T207">Bu oloroːčču kihi ((NOISE)) iti. </ta>
            <ta e="T218" id="Seg_3807" s="T211">Iti min toguh ogoloːk (erdineːgim) bu͡o össü͡ö. </ta>
            <ta e="T223" id="Seg_3808" s="T218">Össü͡ö da maladoj bu͡oltakpɨn itinne. </ta>
            <ta e="T727" id="Seg_3809" s="T223">Če onnuk abɨčajdaːk etilere, "onton hin olok olorogun, tulaːjaktarɨ ahaːtɨŋ", di͡eččiler. </ta>
            <ta e="T241" id="Seg_3810" s="T234">"Tulaːjak ogoloro, anʼiː bu͡olu͡o ogoloro ahaːtɨŋ", di͡eččiler. </ta>
            <ta e="T247" id="Seg_3811" s="T241">"Kuhagannɨk gɨmmɨt kihini üčügejdik ataːrɨŋ", di͡eččiler. </ta>
            <ta e="T263" id="Seg_3812" s="T247">"Töttörü togo bu͡olar" di͡etekpine anʼiː bu͡olu͡o, "onton bejeŋ (on-) üčügejdik olordokkuna töttörü bejeger ɨmsɨːrɨ͡aktara", di͡eččiler bɨlɨrgɨlar. </ta>
            <ta e="T265" id="Seg_3813" s="T263">Abɨčajdar onnuk. </ta>
            <ta e="T276" id="Seg_3814" s="T265">"Onton dʼolloːk bu͡olluŋ bejeŋ kojut dʼolloːk bu͡olluŋ", di͡ete, min anɨ dʼolloːkpun. </ta>
            <ta e="T281" id="Seg_3815" s="T276">Ol tulaːjak etim bu͡o onton. </ta>
            <ta e="T292" id="Seg_3816" s="T281">Inʼem ölbütüger erge bi͡erbitin kenne, eː, erge bi͡ereligine ɨ͡altan ɨ͡alga hɨldʼaːččɨbɨt. </ta>
            <ta e="T298" id="Seg_3817" s="T292">Onno inʼem inʼem ölörügör innʼe diːr: </ta>
            <ta e="T303" id="Seg_3818" s="T298">"Bu͡o kaja tulaːjaktarɨ ahataːr bu͡ol. </ta>
            <ta e="T312" id="Seg_3819" s="T303">Kuhagannɨk gɨmmɨtɨ üčügejdik gɨnɨ͡akkɨn naːda, oččogo uhun dʼolloːk bu͡olu͡oŋ. </ta>
            <ta e="T319" id="Seg_3820" s="T312">Ügüs ogoloːk bu͡olu͡oŋ, belem olokko oloru͡oŋ", di͡ebite. </ta>
            <ta e="T321" id="Seg_3821" s="T319">Ile, kör. </ta>
            <ta e="T331" id="Seg_3822" s="T321">Anɨ min bejem bu͡ollagɨna bi͡eh-u͡ontan taksa kihibin bejem ispitten taksɨbɨtɨm. </ta>
            <ta e="T332" id="Seg_3823" s="T331">Vɨnuktardɨːmmɨn. </ta>
            <ta e="T335" id="Seg_3824" s="T332">Pʼitdʼesʼat s lʼiska. </ta>
            <ta e="T338" id="Seg_3825" s="T335">Naru͡odum, bejem naru͡odum. </ta>
            <ta e="T341" id="Seg_3826" s="T338">Hɨndaːska teŋ aŋara. </ta>
            <ta e="T347" id="Seg_3827" s="T341">Aŋara ɨraːk baːllar össü͡ö ol di͡ek. </ta>
            <ta e="T351" id="Seg_3828" s="T347">Mannagɨm ere de hɨlaː bog. </ta>
            <ta e="T358" id="Seg_3829" s="T351">Manna ere baːr bi͡eh-u͡ontan taksa kihibit, ((NOISE)) ogolorduːn. </ta>
            <ta e="T728" id="Seg_3830" s="T358">Pačʼtʼi Sɨndasska (kaltak) kajan. </ta>
            <ta e="T364" id="Seg_3831" s="T362">Hɨndaːska aŋaram. </ta>
            <ta e="T371" id="Seg_3832" s="T364">Tak da üčügejdik olorobun bertteːk kihiler algɨstarɨgar. </ta>
            <ta e="T375" id="Seg_3833" s="T371">Kihiler algɨstara, kɨrdʼagastar algɨstara. </ta>
            <ta e="T381" id="Seg_3834" s="T375">Anɨ bu͡o kɨrdʼammɨn ɨ͡aldʼabɨn ere bu͡o. </ta>
            <ta e="T382" id="Seg_3835" s="T381">Davlʼenʼijalaːppɨn. </ta>
            <ta e="T392" id="Seg_3836" s="T382">Hürdeːk ulakan ikki, heː, ikki hüːsten taksar. </ta>
            <ta e="T395" id="Seg_3837" s="T392">– Dvacat dva bu͡olar. </ta>
            <ta e="T400" id="Seg_3838" s="T395">Ontubun koppoppun, onton ataktarɨm kaːjallar. </ta>
            <ta e="T409" id="Seg_3839" s="T400">Anɨ biːr da ogogo koton tiːjbeppin bu͡o, hɨtabɨn bu͡o. </ta>
            <ta e="T417" id="Seg_3840" s="T409">Skoro umru navʼerna ((LAUGH)), nʼe bojusʼ ja. </ta>
            <ta e="T419" id="Seg_3841" s="T417">– Nʼet. </ta>
            <ta e="T424" id="Seg_3842" s="T422">– Arajga barɨ͡am. </ta>
            <ta e="T432" id="Seg_3843" s="T428">– E, arajga barɨ͡am. </ta>
            <ta e="T436" id="Seg_3844" s="T432">Ogolorum elbekter, arajga kötütü͡öktere. </ta>
            <ta e="T441" id="Seg_3845" s="T439">– Taŋaraga ke. </ta>
            <ta e="T443" id="Seg_3846" s="T441">Taŋaraga barɨ͡am. </ta>
            <ta e="T451" id="Seg_3847" s="T447">– ((LAUGH)) Ješʼo nʼe znaju. </ta>
            <ta e="T453" id="Seg_3848" s="T451">Di͡eččiler kɨrdʼagastar: </ta>
            <ta e="T455" id="Seg_3849" s="T453">"Taŋaraga barɨ͡akkɨt." </ta>
            <ta e="T461" id="Seg_3850" s="T456">Ügüs ogoloːk kihi, taŋaraga barɨ͡am. </ta>
            <ta e="T464" id="Seg_3851" s="T461">Kihini kuhagannɨk haŋarbappɨn. </ta>
            <ta e="T466" id="Seg_3852" s="T464">Kihini kɨraspappɨn. </ta>
            <ta e="T468" id="Seg_3853" s="T466">Anʼiː bu͡olu͡o. </ta>
            <ta e="T477" id="Seg_3854" s="T468">Ol ihin min kihini kɨraha hataːččɨta hu͡okpun, anʼiː bu͡olu͡o. </ta>
            <ta e="T481" id="Seg_3855" s="T477">Kim kiːrbit, ahatan ihebin. </ta>
            <ta e="T484" id="Seg_3856" s="T481">Etim kotunar erdekpine. </ta>
            <ta e="T489" id="Seg_3857" s="T484">Anɨ kotummappɨn, Dunʼasam ere ebit. </ta>
            <ta e="T491" id="Seg_3858" s="T490">– Üčügej. </ta>
            <ta e="T495" id="Seg_3859" s="T491">Kiniːtim hürdeːk üčügej dʼaktar. </ta>
            <ta e="T500" id="Seg_3860" s="T495">Üčügej dʼaktattar kiniːtterim tak da. </ta>
            <ta e="T507" id="Seg_3861" s="T500">Ol kajdak da bu͡ollun min kɨhallɨbappɨn ginilerge. </ta>
            <ta e="T517" id="Seg_3862" s="T507">Keristinner da kersibetinner da, mini͡eke naːdata hu͡ok, tuspa ɨ͡allar bu͡o. </ta>
            <ta e="T520" id="Seg_3863" s="T517">Tu͡okka kɨhallɨ͡amɨj onuga? </ta>
            <ta e="T527" id="Seg_3864" s="T520">Min ologum büppüte, giniler oloktorun tɨːppappɨn min. </ta>
            <ta e="T531" id="Seg_3865" s="T527">Olordunnar šʼastlʼivɨe, staraːtsa mʼinʼa. </ta>
            <ta e="T536" id="Seg_3866" s="T531">Elete, onton ke tuːgu detegin? </ta>
            <ta e="T552" id="Seg_3867" s="T550">– Eː. </ta>
            <ta e="T563" id="Seg_3868" s="T560">– Nʼelzʼa. </ta>
            <ta e="T570" id="Seg_3869" s="T567">– Kanʼešna, kanʼešna nʼelzʼa. </ta>
            <ta e="T575" id="Seg_3870" s="T570">Istibetter törüt iti baran kaːlallar. </ta>
            <ta e="T590" id="Seg_3871" s="T575">Iti bu erim edʼiːjin, eː, erim baltɨtɨn ogoloro iti baraːččɨlar (oŋo-) oŋu͡oktarga min ogolorbun kɨtta. </ta>
            <ta e="T603" id="Seg_3872" s="T590">Inʼe inʼete ölbütün kenne haŋardɨː keler küččügüj u͡ola, ontuŋ barar, onu ataːrsallar iti. </ta>
            <ta e="T608" id="Seg_3873" s="T603">Onu "barɨmaŋ" diːbin, istibetter bu͡o. </ta>
            <ta e="T612" id="Seg_3874" s="T608">Tugu gɨna barallara dʼürü? </ta>
            <ta e="T614" id="Seg_3875" s="T612">Gri͡ex bu͡o. </ta>
            <ta e="T622" id="Seg_3876" s="T614">Anɨ kihi hir ajɨ ölö hɨtar kihi kuttanɨ͡ak. </ta>
            <ta e="T636" id="Seg_3877" s="T622">Anɨ itinne biːr emeːksin öllö, anɨ biːr ogo kelen ihen ölbüt emi͡e, kaːri͡ennaːk ogobut. </ta>
            <ta e="T640" id="Seg_3878" s="T636">Hürdeːk üčügej ogo ete. </ta>
            <ta e="T644" id="Seg_3879" s="T640">Buraːnɨnan taskajdana hɨldʼar ete. </ta>
            <ta e="T666" id="Seg_3880" s="T661">– Heː, bu tabaːkta bɨragɨ͡akkɨn naːda. </ta>
            <ta e="T674" id="Seg_3881" s="T668">– Aragiː istekterine aragiːta kutu͡okkun naːda. </ta>
            <ta e="T676" id="Seg_3882" s="T675">– Elete. </ta>
            <ta e="T692" id="Seg_3883" s="T687">– Rʼedka rʼedka barallar teːteleriger oŋu͡oktarga. </ta>
            <ta e="T697" id="Seg_3884" s="T692">Inʼelerim oŋu͡oktarɨgar baraːččɨlar taːk da. </ta>
            <ta e="T706" id="Seg_3885" s="T697">Onnuk baːr ojuttar oŋu͡oktara baːllar, ol kuhagan bu͡o, samaːttarɨŋ. </ta>
            <ta e="T708" id="Seg_3886" s="T706">Anʼiː bu͡olaːččɨ. </ta>
            <ta e="T712" id="Seg_3887" s="T708">Istibetter anɨ maladʼožtar törüt. </ta>
            <ta e="T713" id="Seg_3888" s="T712">((LAUGH)). </ta>
            <ta e="T715" id="Seg_3889" s="T713">Durugoj narod. </ta>
            <ta e="T719" id="Seg_3890" s="T715">Havsʼem drugoj narot. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-KiPP">
            <ta e="T2" id="Seg_3891" s="T1">tu͡ok-tan</ta>
            <ta e="T3" id="Seg_3892" s="T2">anʼiː</ta>
            <ta e="T4" id="Seg_3893" s="T3">d-iː-gin</ta>
            <ta e="T0" id="Seg_3894" s="T4">du͡o</ta>
            <ta e="T6" id="Seg_3895" s="T5">anʼiː</ta>
            <ta e="T7" id="Seg_3896" s="T6">bu͡ollagɨna</ta>
            <ta e="T8" id="Seg_3897" s="T7">bɨlɨr</ta>
            <ta e="T9" id="Seg_3898" s="T8">ojut-tar</ta>
            <ta e="T10" id="Seg_3899" s="T9">baːr</ta>
            <ta e="T11" id="Seg_3900" s="T10">bu͡ol-aːččɨ-lar</ta>
            <ta e="T12" id="Seg_3901" s="T11">samaːt-tar</ta>
            <ta e="T13" id="Seg_3902" s="T12">onnuk-tar</ta>
            <ta e="T14" id="Seg_3903" s="T13">dʼi͡e-leri-n</ta>
            <ta e="T15" id="Seg_3904" s="T14">ergij-er-ge</ta>
            <ta e="T16" id="Seg_3905" s="T15">hatam-mat</ta>
            <ta e="T17" id="Seg_3906" s="T16">kuhagan</ta>
            <ta e="T18" id="Seg_3907" s="T17">ergij-bekke</ta>
            <ta e="T19" id="Seg_3908" s="T18">hɨldʼ-ɨ͡ak-kɨ-n</ta>
            <ta e="T20" id="Seg_3909" s="T19">anʼiː</ta>
            <ta e="T21" id="Seg_3910" s="T20">bu͡ol-aːččɨ</ta>
            <ta e="T22" id="Seg_3911" s="T21">ol</ta>
            <ta e="T23" id="Seg_3912" s="T22">aːt-a</ta>
            <ta e="T24" id="Seg_3913" s="T23">anʼiː</ta>
            <ta e="T25" id="Seg_3914" s="T24">dʼaktar</ta>
            <ta e="T26" id="Seg_3915" s="T25">bu͡ollagɨna</ta>
            <ta e="T27" id="Seg_3916" s="T26">er</ta>
            <ta e="T28" id="Seg_3917" s="T27">kihi</ta>
            <ta e="T29" id="Seg_3918" s="T28">taŋah-ɨ-n</ta>
            <ta e="T30" id="Seg_3919" s="T29">teps-er</ta>
            <ta e="T31" id="Seg_3920" s="T30">emi͡e</ta>
            <ta e="T32" id="Seg_3921" s="T31">anʼiː</ta>
            <ta e="T33" id="Seg_3922" s="T32">čistej</ta>
            <ta e="T34" id="Seg_3923" s="T33">bu͡ol-u͡og-u-n</ta>
            <ta e="T35" id="Seg_3924" s="T34">er</ta>
            <ta e="T36" id="Seg_3925" s="T35">kihi</ta>
            <ta e="T37" id="Seg_3926" s="T36">čeber-dik</ta>
            <ta e="T38" id="Seg_3927" s="T37">hɨldʼ-ɨ͡ag-ɨ-n</ta>
            <ta e="T39" id="Seg_3928" s="T38">naːda</ta>
            <ta e="T40" id="Seg_3929" s="T39">dʼaktar</ta>
            <ta e="T41" id="Seg_3930" s="T40">ɨrbaːkɨ-ta</ta>
            <ta e="T42" id="Seg_3931" s="T41">hu͡ok</ta>
            <ta e="T43" id="Seg_3932" s="T42">hɨldʼ-ar</ta>
            <ta e="T44" id="Seg_3933" s="T43">emi͡e</ta>
            <ta e="T45" id="Seg_3934" s="T44">anʼiː</ta>
            <ta e="T46" id="Seg_3935" s="T45">urut</ta>
            <ta e="T47" id="Seg_3936" s="T46">ɨrbaːkɨ</ta>
            <ta e="T48" id="Seg_3937" s="T47">ket-i͡eg-i-n</ta>
            <ta e="T49" id="Seg_3938" s="T48">naːda</ta>
            <ta e="T50" id="Seg_3939" s="T49">pɨlaːtije</ta>
            <ta e="T51" id="Seg_3940" s="T50">er</ta>
            <ta e="T52" id="Seg_3941" s="T51">kihi</ta>
            <ta e="T53" id="Seg_3942" s="T52">kördük</ta>
            <ta e="T55" id="Seg_3943" s="T54">anɨ</ta>
            <ta e="T56" id="Seg_3944" s="T55">barɨ-kaːt-tara</ta>
            <ta e="T57" id="Seg_3945" s="T56">hɨ͡alɨja-laːk-tar</ta>
            <ta e="T58" id="Seg_3946" s="T57">iti</ta>
            <ta e="T59" id="Seg_3947" s="T58">gri͡ex</ta>
            <ta e="T60" id="Seg_3948" s="T59">bu͡o</ta>
            <ta e="T61" id="Seg_3949" s="T60">on-tu-ŋ</ta>
            <ta e="T68" id="Seg_3950" s="T66">eː</ta>
            <ta e="T69" id="Seg_3951" s="T68">če</ta>
            <ta e="T70" id="Seg_3952" s="T69">barɨ-ta</ta>
            <ta e="T71" id="Seg_3953" s="T70">er</ta>
            <ta e="T72" id="Seg_3954" s="T71">kihi͡e-ke</ta>
            <ta e="T73" id="Seg_3955" s="T72">di͡eri</ta>
            <ta e="T74" id="Seg_3956" s="T73">kuhagan</ta>
            <ta e="T75" id="Seg_3957" s="T74">ol</ta>
            <ta e="T76" id="Seg_3958" s="T75">dʼaktar</ta>
            <ta e="T77" id="Seg_3959" s="T76">dʼaktar</ta>
            <ta e="T78" id="Seg_3960" s="T77">kördük</ta>
            <ta e="T79" id="Seg_3961" s="T78">bu͡ol-u͡og-u-n</ta>
            <ta e="T80" id="Seg_3962" s="T79">naːda</ta>
            <ta e="T81" id="Seg_3963" s="T80">ɨrbaːkɨ-laːk</ta>
            <ta e="T82" id="Seg_3964" s="T81">pɨlaːt-taːk</ta>
            <ta e="T83" id="Seg_3965" s="T82">bu͡ol-u͡og-u-n</ta>
            <ta e="T84" id="Seg_3966" s="T83">naːda</ta>
            <ta e="T85" id="Seg_3967" s="T84">bergehe-ni</ta>
            <ta e="T86" id="Seg_3968" s="T85">baga</ta>
            <ta e="T87" id="Seg_3969" s="T86">ket-i-mne</ta>
            <ta e="T88" id="Seg_3970" s="T87">onto</ta>
            <ta e="T89" id="Seg_3971" s="T88">ket-i͡ek-ke</ta>
            <ta e="T90" id="Seg_3972" s="T89">naːda</ta>
            <ta e="T723" id="Seg_3973" s="T90">saːpkɨ-ni</ta>
            <ta e="T92" id="Seg_3974" s="T91">iti</ta>
            <ta e="T93" id="Seg_3975" s="T92">onnuk-tar</ta>
            <ta e="T94" id="Seg_3976" s="T93">onton</ta>
            <ta e="T95" id="Seg_3977" s="T94">er-ge</ta>
            <ta e="T96" id="Seg_3978" s="T95">bar-ar-ɨ-ŋ</ta>
            <ta e="T97" id="Seg_3979" s="T96">hagɨna</ta>
            <ta e="T98" id="Seg_3980" s="T97">iliː</ta>
            <ta e="T99" id="Seg_3981" s="T98">oks-o-h-al-lar</ta>
            <ta e="T100" id="Seg_3982" s="T99">taŋara-ga</ta>
            <ta e="T101" id="Seg_3983" s="T100">itigirdik</ta>
            <ta e="T102" id="Seg_3984" s="T101">bi͡er-el-ler</ta>
            <ta e="T103" id="Seg_3985" s="T102">iliː-leri-n</ta>
            <ta e="T104" id="Seg_3986" s="T103">er-ge</ta>
            <ta e="T105" id="Seg_3987" s="T104">bar-ar</ta>
            <ta e="T724" id="Seg_3988" s="T105">kihi</ta>
            <ta e="T107" id="Seg_3989" s="T106">iliː</ta>
            <ta e="T108" id="Seg_3990" s="T107">ber-s-el-ler</ta>
            <ta e="T109" id="Seg_3991" s="T108">oččogo</ta>
            <ta e="T110" id="Seg_3992" s="T109">iliː-leri-n</ta>
            <ta e="T111" id="Seg_3993" s="T110">bi͡er-dek-terine</ta>
            <ta e="T112" id="Seg_3994" s="T111">dʼaktar</ta>
            <ta e="T113" id="Seg_3995" s="T112">bu͡ol-ar</ta>
            <ta e="T114" id="Seg_3996" s="T113">ol</ta>
            <ta e="T725" id="Seg_3997" s="T114">kihi</ta>
            <ta e="T116" id="Seg_3998" s="T115">onnuk</ta>
            <ta e="T117" id="Seg_3999" s="T116">abɨčaj-dar</ta>
            <ta e="T118" id="Seg_4000" s="T117">onnuk</ta>
            <ta e="T119" id="Seg_4001" s="T118">heː</ta>
            <ta e="T120" id="Seg_4002" s="T119">küːs-ke</ta>
            <ta e="T121" id="Seg_4003" s="T120">bi͡er-el-ler</ta>
            <ta e="T122" id="Seg_4004" s="T121">bu͡o</ta>
            <ta e="T123" id="Seg_4005" s="T122">kihi-ni</ta>
            <ta e="T124" id="Seg_4006" s="T123">bagar</ta>
            <ta e="T125" id="Seg_4007" s="T124">da</ta>
            <ta e="T126" id="Seg_4008" s="T125">bagar-ɨ-ma</ta>
            <ta e="T127" id="Seg_4009" s="T126">da</ta>
            <ta e="T128" id="Seg_4010" s="T127">bi͡er-el-ler</ta>
            <ta e="T129" id="Seg_4011" s="T128">him</ta>
            <ta e="T130" id="Seg_4012" s="T129">biːr</ta>
            <ta e="T131" id="Seg_4013" s="T130">ɨt-ɨː-ɨt-ɨː</ta>
            <ta e="T132" id="Seg_4014" s="T131">bar-a</ta>
            <ta e="T133" id="Seg_4015" s="T132">tur-u͡o-ŋ</ta>
            <ta e="T134" id="Seg_4016" s="T133">onu-ga</ta>
            <ta e="T135" id="Seg_4017" s="T134">ol</ta>
            <ta e="T136" id="Seg_4018" s="T135">kördük</ta>
            <ta e="T137" id="Seg_4019" s="T136">min</ta>
            <ta e="T138" id="Seg_4020" s="T137">bar-a</ta>
            <ta e="T139" id="Seg_4021" s="T138">tur-but-u-m</ta>
            <ta e="T140" id="Seg_4022" s="T139">er-ge</ta>
            <ta e="T141" id="Seg_4023" s="T140">ɨt-ɨː-ɨt-ɨː-bɨn</ta>
            <ta e="T142" id="Seg_4024" s="T141">u͡on</ta>
            <ta e="T143" id="Seg_4025" s="T142">alta</ta>
            <ta e="T144" id="Seg_4026" s="T143">dʼɨl-laːk-pa-r</ta>
            <ta e="T145" id="Seg_4027" s="T144">ke</ta>
            <ta e="T146" id="Seg_4028" s="T145">tu͡ok</ta>
            <ta e="T147" id="Seg_4029" s="T146">kihi-te</ta>
            <ta e="T148" id="Seg_4030" s="T147">er-ge</ta>
            <ta e="T149" id="Seg_4031" s="T148">bar-ɨ͡a=j</ta>
            <ta e="T150" id="Seg_4032" s="T149">ol</ta>
            <ta e="T151" id="Seg_4033" s="T150">u͡on</ta>
            <ta e="T152" id="Seg_4034" s="T151">alta</ta>
            <ta e="T153" id="Seg_4035" s="T152">dʼɨl-laːk-ka</ta>
            <ta e="T154" id="Seg_4036" s="T153">bi͡er-bit-tere</ta>
            <ta e="T157" id="Seg_4037" s="T156">hapsi͡em</ta>
            <ta e="T158" id="Seg_4038" s="T157">ogo</ta>
            <ta e="T159" id="Seg_4039" s="T158">ɨraːs</ta>
            <ta e="T160" id="Seg_4040" s="T159">e-ti-m</ta>
            <ta e="T161" id="Seg_4041" s="T160">haːtar</ta>
            <ta e="T162" id="Seg_4042" s="T161">mʼesnaj-a</ta>
            <ta e="T163" id="Seg_4043" s="T162">da</ta>
            <ta e="T164" id="Seg_4044" s="T163">hu͡ok</ta>
            <ta e="T165" id="Seg_4045" s="T164">e-ti-m</ta>
            <ta e="T166" id="Seg_4046" s="T165">onton</ta>
            <ta e="T167" id="Seg_4047" s="T166">iti</ta>
            <ta e="T168" id="Seg_4048" s="T167">tagɨs-t-a</ta>
            <ta e="T169" id="Seg_4049" s="T168">ulakan</ta>
            <ta e="T170" id="Seg_4050" s="T169">kɨːh-ɨ-m</ta>
            <ta e="T171" id="Seg_4051" s="T170">anɨ</ta>
            <ta e="T172" id="Seg_4052" s="T171">bu͡o</ta>
            <ta e="T173" id="Seg_4053" s="T172">iti</ta>
            <ta e="T174" id="Seg_4054" s="T173">iti</ta>
            <ta e="T175" id="Seg_4055" s="T174">ogo-m</ta>
            <ta e="T176" id="Seg_4056" s="T175">u͡on</ta>
            <ta e="T177" id="Seg_4057" s="T176">hette</ta>
            <ta e="T178" id="Seg_4058" s="T177">u͡on</ta>
            <ta e="T179" id="Seg_4059" s="T178">agɨs</ta>
            <ta e="T180" id="Seg_4060" s="T179">dʼɨl-laːk-pa-r</ta>
            <ta e="T181" id="Seg_4061" s="T180">töröː-büt</ta>
            <ta e="T182" id="Seg_4062" s="T181">ogo</ta>
            <ta e="T183" id="Seg_4063" s="T182">iti</ta>
            <ta e="T184" id="Seg_4064" s="T183">dʼe</ta>
            <ta e="T185" id="Seg_4065" s="T184">onton</ta>
            <ta e="T186" id="Seg_4066" s="T185">kɨrdʼ-an</ta>
            <ta e="T187" id="Seg_4067" s="T186">kɨrdʼ-am-mɨn</ta>
            <ta e="T188" id="Seg_4068" s="T187">iti</ta>
            <ta e="T189" id="Seg_4069" s="T188">kojut</ta>
            <ta e="T190" id="Seg_4070" s="T189">kojutun</ta>
            <ta e="T191" id="Seg_4071" s="T190">onton</ta>
            <ta e="T192" id="Seg_4072" s="T191">du͡o</ta>
            <ta e="T194" id="Seg_4073" s="T193">ogo-lon-on</ta>
            <ta e="T195" id="Seg_4074" s="T194">da</ta>
            <ta e="T196" id="Seg_4075" s="T195">ogo-lon-on</ta>
            <ta e="T197" id="Seg_4076" s="T196">ih-e-gin</ta>
            <ta e="T198" id="Seg_4077" s="T197">bu͡o</ta>
            <ta e="T203" id="Seg_4078" s="T202">hürdeːk</ta>
            <ta e="T204" id="Seg_4079" s="T203">üčügej</ta>
            <ta e="T205" id="Seg_4080" s="T204">kihi</ta>
            <ta e="T206" id="Seg_4081" s="T205">e-t-e</ta>
            <ta e="T207" id="Seg_4082" s="T206">er-i-m</ta>
            <ta e="T208" id="Seg_4083" s="T207">bu</ta>
            <ta e="T209" id="Seg_4084" s="T208">olor-oːčču</ta>
            <ta e="T210" id="Seg_4085" s="T209">kihi</ta>
            <ta e="T726" id="Seg_4086" s="T210">iti</ta>
            <ta e="T212" id="Seg_4087" s="T211">iti</ta>
            <ta e="T213" id="Seg_4088" s="T212">min</ta>
            <ta e="T214" id="Seg_4089" s="T213">toguh</ta>
            <ta e="T215" id="Seg_4090" s="T214">ogo-loːk</ta>
            <ta e="T216" id="Seg_4091" s="T215">er-di-neːg-i-m</ta>
            <ta e="T217" id="Seg_4092" s="T216">bu͡o</ta>
            <ta e="T218" id="Seg_4093" s="T217">össü͡ö</ta>
            <ta e="T219" id="Seg_4094" s="T218">össü͡ö</ta>
            <ta e="T220" id="Seg_4095" s="T219">da</ta>
            <ta e="T221" id="Seg_4096" s="T220">maladoj</ta>
            <ta e="T222" id="Seg_4097" s="T221">bu͡ol-tak-pɨn</ta>
            <ta e="T223" id="Seg_4098" s="T222">itinne</ta>
            <ta e="T224" id="Seg_4099" s="T223">če</ta>
            <ta e="T225" id="Seg_4100" s="T224">onnuk</ta>
            <ta e="T226" id="Seg_4101" s="T225">abɨčaj-daːk</ta>
            <ta e="T227" id="Seg_4102" s="T226">e-ti-lere</ta>
            <ta e="T228" id="Seg_4103" s="T227">onton</ta>
            <ta e="T229" id="Seg_4104" s="T228">hin</ta>
            <ta e="T230" id="Seg_4105" s="T229">olok</ta>
            <ta e="T231" id="Seg_4106" s="T230">olor-o-gun</ta>
            <ta e="T232" id="Seg_4107" s="T231">tulaːjak-tar-ɨ</ta>
            <ta e="T233" id="Seg_4108" s="T232">ahaː-t-ɨ-ŋ</ta>
            <ta e="T727" id="Seg_4109" s="T233">di͡e-čči-ler</ta>
            <ta e="T235" id="Seg_4110" s="T234">tulaːjak</ta>
            <ta e="T236" id="Seg_4111" s="T235">ogo-loro</ta>
            <ta e="T237" id="Seg_4112" s="T236">anʼiː</ta>
            <ta e="T238" id="Seg_4113" s="T237">bu͡ol-u͡o</ta>
            <ta e="T239" id="Seg_4114" s="T238">ogo-loro</ta>
            <ta e="T240" id="Seg_4115" s="T239">ahaː-t-ɨ-ŋ</ta>
            <ta e="T241" id="Seg_4116" s="T240">di͡e-čči-ler</ta>
            <ta e="T242" id="Seg_4117" s="T241">kuhagan-nɨk</ta>
            <ta e="T243" id="Seg_4118" s="T242">gɨm-mɨt</ta>
            <ta e="T244" id="Seg_4119" s="T243">kihi-ni</ta>
            <ta e="T245" id="Seg_4120" s="T244">üčügej-dik</ta>
            <ta e="T246" id="Seg_4121" s="T245">ataːr-ɨ-ŋ</ta>
            <ta e="T247" id="Seg_4122" s="T246">di͡e-čči-ler</ta>
            <ta e="T248" id="Seg_4123" s="T247">töttörü</ta>
            <ta e="T249" id="Seg_4124" s="T248">togo</ta>
            <ta e="T250" id="Seg_4125" s="T249">bu͡ol-ar</ta>
            <ta e="T251" id="Seg_4126" s="T250">di͡e-tek-pine</ta>
            <ta e="T252" id="Seg_4127" s="T251">anʼiː</ta>
            <ta e="T253" id="Seg_4128" s="T252">bu͡ol-u͡o</ta>
            <ta e="T254" id="Seg_4129" s="T253">onton</ta>
            <ta e="T255" id="Seg_4130" s="T254">beje-ŋ</ta>
            <ta e="T256" id="Seg_4131" s="T255">on</ta>
            <ta e="T257" id="Seg_4132" s="T256">üčügej-dik</ta>
            <ta e="T258" id="Seg_4133" s="T257">olor-dok-kuna</ta>
            <ta e="T259" id="Seg_4134" s="T258">töttörü</ta>
            <ta e="T260" id="Seg_4135" s="T259">beje-ge-r</ta>
            <ta e="T261" id="Seg_4136" s="T260">ɨmsɨːr-ɨ͡ak-tara</ta>
            <ta e="T262" id="Seg_4137" s="T261">di͡e-čči-ler</ta>
            <ta e="T263" id="Seg_4138" s="T262">bɨlɨrgɨ-lar</ta>
            <ta e="T264" id="Seg_4139" s="T263">abɨčaj-dar</ta>
            <ta e="T265" id="Seg_4140" s="T264">onnuk</ta>
            <ta e="T266" id="Seg_4141" s="T265">onton</ta>
            <ta e="T267" id="Seg_4142" s="T266">dʼolloːk</ta>
            <ta e="T268" id="Seg_4143" s="T267">bu͡ol-lu-ŋ</ta>
            <ta e="T269" id="Seg_4144" s="T268">beje-ŋ</ta>
            <ta e="T270" id="Seg_4145" s="T269">kojut</ta>
            <ta e="T271" id="Seg_4146" s="T270">dʼolloːk</ta>
            <ta e="T272" id="Seg_4147" s="T271">bu͡ol-lu-ŋ</ta>
            <ta e="T273" id="Seg_4148" s="T272">di͡e-t-e</ta>
            <ta e="T274" id="Seg_4149" s="T273">min</ta>
            <ta e="T275" id="Seg_4150" s="T274">anɨ</ta>
            <ta e="T276" id="Seg_4151" s="T275">dʼolloːk-pun</ta>
            <ta e="T277" id="Seg_4152" s="T276">ol</ta>
            <ta e="T278" id="Seg_4153" s="T277">tulaːjak</ta>
            <ta e="T279" id="Seg_4154" s="T278">e-ti-m</ta>
            <ta e="T280" id="Seg_4155" s="T279">bu͡o</ta>
            <ta e="T281" id="Seg_4156" s="T280">onton</ta>
            <ta e="T282" id="Seg_4157" s="T281">inʼe-m</ta>
            <ta e="T283" id="Seg_4158" s="T282">öl-büt-ü-ger</ta>
            <ta e="T284" id="Seg_4159" s="T283">er-ge</ta>
            <ta e="T285" id="Seg_4160" s="T284">bi͡er-bit-i-n</ta>
            <ta e="T286" id="Seg_4161" s="T285">kenne</ta>
            <ta e="T287" id="Seg_4162" s="T286">eː</ta>
            <ta e="T288" id="Seg_4163" s="T287">er-ge</ta>
            <ta e="T289" id="Seg_4164" s="T288">bi͡er-e=lig-ine</ta>
            <ta e="T290" id="Seg_4165" s="T289">ɨ͡al-tan</ta>
            <ta e="T291" id="Seg_4166" s="T290">ɨ͡al-ga</ta>
            <ta e="T292" id="Seg_4167" s="T291">hɨldʼ-aːččɨ-bɨt</ta>
            <ta e="T293" id="Seg_4168" s="T292">onno</ta>
            <ta e="T294" id="Seg_4169" s="T293">inʼe-m</ta>
            <ta e="T295" id="Seg_4170" s="T294">inʼe-m</ta>
            <ta e="T296" id="Seg_4171" s="T295">öl-ör-ü-gör</ta>
            <ta e="T297" id="Seg_4172" s="T296">innʼe</ta>
            <ta e="T298" id="Seg_4173" s="T297">diː-r</ta>
            <ta e="T299" id="Seg_4174" s="T298">bu͡o</ta>
            <ta e="T300" id="Seg_4175" s="T299">kaja</ta>
            <ta e="T301" id="Seg_4176" s="T300">tulaːjak-tar-ɨ</ta>
            <ta e="T302" id="Seg_4177" s="T301">ah-a-t-aːr</ta>
            <ta e="T303" id="Seg_4178" s="T302">bu͡ol</ta>
            <ta e="T304" id="Seg_4179" s="T303">kuhagan-nɨk</ta>
            <ta e="T305" id="Seg_4180" s="T304">gɨm-mɨt-ɨ</ta>
            <ta e="T306" id="Seg_4181" s="T305">üčügej-dik</ta>
            <ta e="T307" id="Seg_4182" s="T306">gɨn-ɨ͡ak-kɨ-n</ta>
            <ta e="T308" id="Seg_4183" s="T307">naːda</ta>
            <ta e="T309" id="Seg_4184" s="T308">oččogo</ta>
            <ta e="T310" id="Seg_4185" s="T309">uhun</ta>
            <ta e="T311" id="Seg_4186" s="T310">dʼolloːk</ta>
            <ta e="T312" id="Seg_4187" s="T311">bu͡ol-u͡o-ŋ</ta>
            <ta e="T313" id="Seg_4188" s="T312">ügüs</ta>
            <ta e="T314" id="Seg_4189" s="T313">ogo-loːk</ta>
            <ta e="T315" id="Seg_4190" s="T314">bu͡ol-u͡o-ŋ</ta>
            <ta e="T316" id="Seg_4191" s="T315">belem</ta>
            <ta e="T317" id="Seg_4192" s="T316">olok-ko</ta>
            <ta e="T318" id="Seg_4193" s="T317">olor-u͡o-ŋ</ta>
            <ta e="T319" id="Seg_4194" s="T318">di͡e-bit-e</ta>
            <ta e="T320" id="Seg_4195" s="T319">ile</ta>
            <ta e="T321" id="Seg_4196" s="T320">kör</ta>
            <ta e="T322" id="Seg_4197" s="T321">anɨ</ta>
            <ta e="T323" id="Seg_4198" s="T322">min</ta>
            <ta e="T324" id="Seg_4199" s="T323">beje-m</ta>
            <ta e="T325" id="Seg_4200" s="T324">bu͡ollagɨna</ta>
            <ta e="T326" id="Seg_4201" s="T325">bi͡eh-u͡on-tan</ta>
            <ta e="T327" id="Seg_4202" s="T326">taksa</ta>
            <ta e="T328" id="Seg_4203" s="T327">kihi-bi-n</ta>
            <ta e="T329" id="Seg_4204" s="T328">beje-m</ta>
            <ta e="T330" id="Seg_4205" s="T329">is-pi-tten</ta>
            <ta e="T331" id="Seg_4206" s="T330">taks-ɨ-bɨt-ɨ-m</ta>
            <ta e="T332" id="Seg_4207" s="T331">vɨnuk-tar-dɨːm-mɨn</ta>
            <ta e="T336" id="Seg_4208" s="T335">naru͡od-u-m</ta>
            <ta e="T337" id="Seg_4209" s="T336">beje-m</ta>
            <ta e="T338" id="Seg_4210" s="T337">naru͡od-u-m</ta>
            <ta e="T339" id="Seg_4211" s="T338">Hɨndaːska</ta>
            <ta e="T340" id="Seg_4212" s="T339">teŋ</ta>
            <ta e="T341" id="Seg_4213" s="T340">aŋar-a</ta>
            <ta e="T342" id="Seg_4214" s="T341">aŋar-a</ta>
            <ta e="T343" id="Seg_4215" s="T342">ɨraːk</ta>
            <ta e="T344" id="Seg_4216" s="T343">baːl-lar</ta>
            <ta e="T345" id="Seg_4217" s="T344">össü͡ö</ta>
            <ta e="T346" id="Seg_4218" s="T345">ol</ta>
            <ta e="T347" id="Seg_4219" s="T346">di͡ek</ta>
            <ta e="T348" id="Seg_4220" s="T347">manna-gɨ-m</ta>
            <ta e="T349" id="Seg_4221" s="T348">ere</ta>
            <ta e="T350" id="Seg_4222" s="T349">de</ta>
            <ta e="T351" id="Seg_4223" s="T350">hɨlaː bog</ta>
            <ta e="T352" id="Seg_4224" s="T351">manna</ta>
            <ta e="T353" id="Seg_4225" s="T352">ere</ta>
            <ta e="T354" id="Seg_4226" s="T353">baːr</ta>
            <ta e="T355" id="Seg_4227" s="T354">bi͡eh-u͡on-tan</ta>
            <ta e="T356" id="Seg_4228" s="T355">taksa</ta>
            <ta e="T357" id="Seg_4229" s="T356">kihi-bit</ta>
            <ta e="T358" id="Seg_4230" s="T357">ogo-lor-duːn</ta>
            <ta e="T359" id="Seg_4231" s="T358">pačʼtʼi</ta>
            <ta e="T360" id="Seg_4232" s="T359">Sɨndasska</ta>
            <ta e="T728" id="Seg_4233" s="T361">kaj-an</ta>
            <ta e="T363" id="Seg_4234" s="T362">Hɨndaːska</ta>
            <ta e="T364" id="Seg_4235" s="T363">aŋar-a-m</ta>
            <ta e="T365" id="Seg_4236" s="T364">tak</ta>
            <ta e="T366" id="Seg_4237" s="T365">da</ta>
            <ta e="T367" id="Seg_4238" s="T366">üčügej-dik</ta>
            <ta e="T368" id="Seg_4239" s="T367">olor-o-bun</ta>
            <ta e="T369" id="Seg_4240" s="T368">bert-teːk</ta>
            <ta e="T370" id="Seg_4241" s="T369">kihi-ler</ta>
            <ta e="T371" id="Seg_4242" s="T370">algɨs-tarɨ-gar</ta>
            <ta e="T372" id="Seg_4243" s="T371">kihi-ler</ta>
            <ta e="T373" id="Seg_4244" s="T372">algɨs-tara</ta>
            <ta e="T374" id="Seg_4245" s="T373">kɨrdʼagas-tar</ta>
            <ta e="T375" id="Seg_4246" s="T374">algɨs-tara</ta>
            <ta e="T376" id="Seg_4247" s="T375">anɨ</ta>
            <ta e="T377" id="Seg_4248" s="T376">bu͡o</ta>
            <ta e="T378" id="Seg_4249" s="T377">kɨrdʼ-am-mɨn</ta>
            <ta e="T379" id="Seg_4250" s="T378">ɨ͡aldʼ-a-bɨn</ta>
            <ta e="T380" id="Seg_4251" s="T379">ere</ta>
            <ta e="T381" id="Seg_4252" s="T380">bu͡o</ta>
            <ta e="T382" id="Seg_4253" s="T381">davlʼenʼija-laːp-pɨn</ta>
            <ta e="T383" id="Seg_4254" s="T382">hürdeːk</ta>
            <ta e="T384" id="Seg_4255" s="T383">ulakan</ta>
            <ta e="T386" id="Seg_4256" s="T384">ikki</ta>
            <ta e="T388" id="Seg_4257" s="T386">heː</ta>
            <ta e="T390" id="Seg_4258" s="T388">ikki</ta>
            <ta e="T391" id="Seg_4259" s="T390">hüːs-ten</ta>
            <ta e="T392" id="Seg_4260" s="T391">taks-ar</ta>
            <ta e="T395" id="Seg_4261" s="T394">bu͡ol-ar</ta>
            <ta e="T396" id="Seg_4262" s="T395">on-tu-bu-n</ta>
            <ta e="T397" id="Seg_4263" s="T396">kop-pop-pun</ta>
            <ta e="T398" id="Seg_4264" s="T397">onton</ta>
            <ta e="T399" id="Seg_4265" s="T398">atak-tar-ɨ-m</ta>
            <ta e="T400" id="Seg_4266" s="T399">kaːj-al-lar</ta>
            <ta e="T401" id="Seg_4267" s="T400">anɨ</ta>
            <ta e="T402" id="Seg_4268" s="T401">biːr</ta>
            <ta e="T403" id="Seg_4269" s="T402">da</ta>
            <ta e="T404" id="Seg_4270" s="T403">ogo-go</ta>
            <ta e="T405" id="Seg_4271" s="T404">kot-on</ta>
            <ta e="T406" id="Seg_4272" s="T405">tiːj-bep-pin</ta>
            <ta e="T407" id="Seg_4273" s="T406">bu͡o</ta>
            <ta e="T408" id="Seg_4274" s="T407">hɨt-a-bɨn</ta>
            <ta e="T409" id="Seg_4275" s="T408">bu͡o</ta>
            <ta e="T423" id="Seg_4276" s="T422">araj-ga</ta>
            <ta e="T424" id="Seg_4277" s="T423">bar-ɨ͡a-m</ta>
            <ta e="T430" id="Seg_4278" s="T428">e</ta>
            <ta e="T431" id="Seg_4279" s="T430">araj-ga</ta>
            <ta e="T432" id="Seg_4280" s="T431">bar-ɨ͡a-m</ta>
            <ta e="T433" id="Seg_4281" s="T432">ogo-lor-u-m</ta>
            <ta e="T434" id="Seg_4282" s="T433">elbek-ter</ta>
            <ta e="T435" id="Seg_4283" s="T434">araj-ga</ta>
            <ta e="T436" id="Seg_4284" s="T435">köt-ü-t-ü͡ök-tere</ta>
            <ta e="T440" id="Seg_4285" s="T439">taŋara-ga</ta>
            <ta e="T441" id="Seg_4286" s="T440">ke</ta>
            <ta e="T442" id="Seg_4287" s="T441">taŋara-ga</ta>
            <ta e="T443" id="Seg_4288" s="T442">bar-ɨ͡a-m</ta>
            <ta e="T452" id="Seg_4289" s="T451">di͡e-čči-ler</ta>
            <ta e="T453" id="Seg_4290" s="T452">kɨrdʼagas-tar</ta>
            <ta e="T454" id="Seg_4291" s="T453">taŋara-ga</ta>
            <ta e="T455" id="Seg_4292" s="T454">bar-ɨ͡ak-kɨt</ta>
            <ta e="T457" id="Seg_4293" s="T456">ügüs</ta>
            <ta e="T458" id="Seg_4294" s="T457">ogo-loːk</ta>
            <ta e="T459" id="Seg_4295" s="T458">kihi</ta>
            <ta e="T460" id="Seg_4296" s="T459">taŋara-ga</ta>
            <ta e="T461" id="Seg_4297" s="T460">bar-ɨ͡a-m</ta>
            <ta e="T462" id="Seg_4298" s="T461">kihi-ni</ta>
            <ta e="T463" id="Seg_4299" s="T462">kuhagan-nɨk</ta>
            <ta e="T464" id="Seg_4300" s="T463">haŋar-bap-pɨn</ta>
            <ta e="T465" id="Seg_4301" s="T464">kihi-ni</ta>
            <ta e="T466" id="Seg_4302" s="T465">kɨr-a-s-pap-pɨn</ta>
            <ta e="T467" id="Seg_4303" s="T466">anʼiː</ta>
            <ta e="T468" id="Seg_4304" s="T467">bu͡ol-u͡o</ta>
            <ta e="T469" id="Seg_4305" s="T468">ol</ta>
            <ta e="T470" id="Seg_4306" s="T469">ihin</ta>
            <ta e="T471" id="Seg_4307" s="T470">min</ta>
            <ta e="T472" id="Seg_4308" s="T471">kihi-ni</ta>
            <ta e="T473" id="Seg_4309" s="T472">kɨr-a-h-a</ta>
            <ta e="T474" id="Seg_4310" s="T473">hataː-ččɨ-ta</ta>
            <ta e="T475" id="Seg_4311" s="T474">hu͡ok-pun</ta>
            <ta e="T476" id="Seg_4312" s="T475">anʼiː</ta>
            <ta e="T477" id="Seg_4313" s="T476">bu͡ol-u͡o</ta>
            <ta e="T478" id="Seg_4314" s="T477">kim</ta>
            <ta e="T479" id="Seg_4315" s="T478">kiːr-bit</ta>
            <ta e="T480" id="Seg_4316" s="T479">ah-a-t-an</ta>
            <ta e="T481" id="Seg_4317" s="T480">ih-e-bin</ta>
            <ta e="T482" id="Seg_4318" s="T481">et-i-m</ta>
            <ta e="T483" id="Seg_4319" s="T482">kot-u-n-ar</ta>
            <ta e="T484" id="Seg_4320" s="T483">er-dek-pine</ta>
            <ta e="T485" id="Seg_4321" s="T484">anɨ</ta>
            <ta e="T486" id="Seg_4322" s="T485">kot-u-m-map-pɨn</ta>
            <ta e="T487" id="Seg_4323" s="T486">Dunʼasa-m</ta>
            <ta e="T488" id="Seg_4324" s="T487">ere</ta>
            <ta e="T489" id="Seg_4325" s="T488">e-bit</ta>
            <ta e="T491" id="Seg_4326" s="T490">üčügej</ta>
            <ta e="T492" id="Seg_4327" s="T491">kiniːt-i-m</ta>
            <ta e="T493" id="Seg_4328" s="T492">hürdeːk</ta>
            <ta e="T494" id="Seg_4329" s="T493">üčügej</ta>
            <ta e="T495" id="Seg_4330" s="T494">dʼaktar</ta>
            <ta e="T496" id="Seg_4331" s="T495">üčügej</ta>
            <ta e="T497" id="Seg_4332" s="T496">dʼaktat-tar</ta>
            <ta e="T498" id="Seg_4333" s="T497">kiniːt-ter-i-m</ta>
            <ta e="T499" id="Seg_4334" s="T498">tak</ta>
            <ta e="T500" id="Seg_4335" s="T499">da</ta>
            <ta e="T501" id="Seg_4336" s="T500">ol</ta>
            <ta e="T502" id="Seg_4337" s="T501">kajdak</ta>
            <ta e="T503" id="Seg_4338" s="T502">da</ta>
            <ta e="T504" id="Seg_4339" s="T503">bu͡ol-lun</ta>
            <ta e="T505" id="Seg_4340" s="T504">min</ta>
            <ta e="T506" id="Seg_4341" s="T505">kɨhall-ɨ-bap-pɨn</ta>
            <ta e="T507" id="Seg_4342" s="T506">giniler-ge</ta>
            <ta e="T508" id="Seg_4343" s="T507">keri-s-tinner</ta>
            <ta e="T509" id="Seg_4344" s="T508">da</ta>
            <ta e="T510" id="Seg_4345" s="T509">ker-s-i-be-tinner</ta>
            <ta e="T511" id="Seg_4346" s="T510">da</ta>
            <ta e="T512" id="Seg_4347" s="T511">mini͡e-ke</ta>
            <ta e="T513" id="Seg_4348" s="T512">naːda-ta</ta>
            <ta e="T514" id="Seg_4349" s="T513">hu͡ok</ta>
            <ta e="T515" id="Seg_4350" s="T514">tuspa</ta>
            <ta e="T516" id="Seg_4351" s="T515">ɨ͡al-lar</ta>
            <ta e="T517" id="Seg_4352" s="T516">bu͡o</ta>
            <ta e="T518" id="Seg_4353" s="T517">tu͡ok-ka</ta>
            <ta e="T519" id="Seg_4354" s="T518">kɨhall-ɨ͡a-m=ɨj</ta>
            <ta e="T520" id="Seg_4355" s="T519">onu-ga</ta>
            <ta e="T521" id="Seg_4356" s="T520">min</ta>
            <ta e="T522" id="Seg_4357" s="T521">olog-u-m</ta>
            <ta e="T523" id="Seg_4358" s="T522">büp-püt-e</ta>
            <ta e="T524" id="Seg_4359" s="T523">giniler</ta>
            <ta e="T525" id="Seg_4360" s="T524">olok-toru-n</ta>
            <ta e="T526" id="Seg_4361" s="T525">tɨːp-pap-pɨn</ta>
            <ta e="T527" id="Seg_4362" s="T526">min</ta>
            <ta e="T528" id="Seg_4363" s="T527">olor-dunnar</ta>
            <ta e="T532" id="Seg_4364" s="T531">ele-te</ta>
            <ta e="T533" id="Seg_4365" s="T532">onton</ta>
            <ta e="T534" id="Seg_4366" s="T533">ke</ta>
            <ta e="T535" id="Seg_4367" s="T534">tuːg-u</ta>
            <ta e="T536" id="Seg_4368" s="T535">d-e-t-e-gin</ta>
            <ta e="T552" id="Seg_4369" s="T550">eː</ta>
            <ta e="T571" id="Seg_4370" s="T570">ist-i-bet-ter</ta>
            <ta e="T572" id="Seg_4371" s="T571">törüt</ta>
            <ta e="T573" id="Seg_4372" s="T572">iti</ta>
            <ta e="T574" id="Seg_4373" s="T573">bar-an</ta>
            <ta e="T575" id="Seg_4374" s="T574">kaːl-al-lar</ta>
            <ta e="T576" id="Seg_4375" s="T575">iti</ta>
            <ta e="T577" id="Seg_4376" s="T576">bu</ta>
            <ta e="T578" id="Seg_4377" s="T577">er-i-m</ta>
            <ta e="T579" id="Seg_4378" s="T578">edʼiːj-i-n</ta>
            <ta e="T580" id="Seg_4379" s="T579">eː</ta>
            <ta e="T581" id="Seg_4380" s="T580">er-i-m</ta>
            <ta e="T582" id="Seg_4381" s="T581">balt-ɨ-tɨ-n</ta>
            <ta e="T583" id="Seg_4382" s="T582">ogo-lor-o</ta>
            <ta e="T584" id="Seg_4383" s="T583">iti</ta>
            <ta e="T585" id="Seg_4384" s="T584">bar-aːččɨ-lar</ta>
            <ta e="T587" id="Seg_4385" s="T586">oŋu͡ok-tar-ga</ta>
            <ta e="T588" id="Seg_4386" s="T587">min</ta>
            <ta e="T589" id="Seg_4387" s="T588">ogo-lor-bu-n</ta>
            <ta e="T590" id="Seg_4388" s="T589">kɨtta</ta>
            <ta e="T591" id="Seg_4389" s="T590">inʼe</ta>
            <ta e="T592" id="Seg_4390" s="T591">inʼe-te</ta>
            <ta e="T593" id="Seg_4391" s="T592">öl-büt-ü-n</ta>
            <ta e="T594" id="Seg_4392" s="T593">kenne</ta>
            <ta e="T595" id="Seg_4393" s="T594">haŋardɨː</ta>
            <ta e="T596" id="Seg_4394" s="T595">kel-er</ta>
            <ta e="T597" id="Seg_4395" s="T596">küččügüj</ta>
            <ta e="T598" id="Seg_4396" s="T597">u͡ol-a</ta>
            <ta e="T599" id="Seg_4397" s="T598">on-tu-ŋ</ta>
            <ta e="T600" id="Seg_4398" s="T599">bar-ar</ta>
            <ta e="T601" id="Seg_4399" s="T600">o-nu</ta>
            <ta e="T602" id="Seg_4400" s="T601">ataːr-s-al-lar</ta>
            <ta e="T603" id="Seg_4401" s="T602">iti</ta>
            <ta e="T604" id="Seg_4402" s="T603">o-nu</ta>
            <ta e="T605" id="Seg_4403" s="T604">bar-ɨ-ma-ŋ</ta>
            <ta e="T606" id="Seg_4404" s="T605">d-iː-bin</ta>
            <ta e="T607" id="Seg_4405" s="T606">ist-i-bet-ter</ta>
            <ta e="T608" id="Seg_4406" s="T607">bu͡o</ta>
            <ta e="T609" id="Seg_4407" s="T608">tug-u</ta>
            <ta e="T610" id="Seg_4408" s="T609">gɨn-a</ta>
            <ta e="T611" id="Seg_4409" s="T610">bar-al-lara</ta>
            <ta e="T612" id="Seg_4410" s="T611">dʼürü</ta>
            <ta e="T613" id="Seg_4411" s="T612">gri͡ex</ta>
            <ta e="T614" id="Seg_4412" s="T613">bu͡o</ta>
            <ta e="T615" id="Seg_4413" s="T614">anɨ</ta>
            <ta e="T616" id="Seg_4414" s="T615">kihi</ta>
            <ta e="T617" id="Seg_4415" s="T616">hir</ta>
            <ta e="T618" id="Seg_4416" s="T617">ajɨ</ta>
            <ta e="T619" id="Seg_4417" s="T618">öl-ö</ta>
            <ta e="T620" id="Seg_4418" s="T619">hɨt-ar</ta>
            <ta e="T621" id="Seg_4419" s="T620">kihi</ta>
            <ta e="T622" id="Seg_4420" s="T621">kuttan-ɨ͡ak</ta>
            <ta e="T623" id="Seg_4421" s="T622">anɨ</ta>
            <ta e="T624" id="Seg_4422" s="T623">itinne</ta>
            <ta e="T625" id="Seg_4423" s="T624">biːr</ta>
            <ta e="T626" id="Seg_4424" s="T625">emeːksin</ta>
            <ta e="T627" id="Seg_4425" s="T626">öl-l-ö</ta>
            <ta e="T628" id="Seg_4426" s="T627">anɨ</ta>
            <ta e="T629" id="Seg_4427" s="T628">biːr</ta>
            <ta e="T630" id="Seg_4428" s="T629">ogo</ta>
            <ta e="T631" id="Seg_4429" s="T630">kel-en</ta>
            <ta e="T632" id="Seg_4430" s="T631">ih-en</ta>
            <ta e="T633" id="Seg_4431" s="T632">öl-büt</ta>
            <ta e="T634" id="Seg_4432" s="T633">emi͡e</ta>
            <ta e="T635" id="Seg_4433" s="T634">kaːri͡en-naːk</ta>
            <ta e="T636" id="Seg_4434" s="T635">ogo-but</ta>
            <ta e="T637" id="Seg_4435" s="T636">hürdeːk</ta>
            <ta e="T638" id="Seg_4436" s="T637">üčügej</ta>
            <ta e="T639" id="Seg_4437" s="T638">ogo</ta>
            <ta e="T640" id="Seg_4438" s="T639">e-t-e</ta>
            <ta e="T641" id="Seg_4439" s="T640">buraːn-ɨ-nan</ta>
            <ta e="T642" id="Seg_4440" s="T641">taskaj-dan-a</ta>
            <ta e="T643" id="Seg_4441" s="T642">hɨldʼ-ar</ta>
            <ta e="T644" id="Seg_4442" s="T643">e-t-e</ta>
            <ta e="T662" id="Seg_4443" s="T661">heː</ta>
            <ta e="T663" id="Seg_4444" s="T662">bu</ta>
            <ta e="T664" id="Seg_4445" s="T663">tabaːk-ta</ta>
            <ta e="T665" id="Seg_4446" s="T664">bɨrag-ɨ͡ak-kɨ-n</ta>
            <ta e="T666" id="Seg_4447" s="T665">naːda</ta>
            <ta e="T670" id="Seg_4448" s="T668">aragiː</ta>
            <ta e="T671" id="Seg_4449" s="T670">is-tek-terine</ta>
            <ta e="T672" id="Seg_4450" s="T671">aragiː-ta</ta>
            <ta e="T673" id="Seg_4451" s="T672">kut-u͡ok-ku-n</ta>
            <ta e="T674" id="Seg_4452" s="T673">naːda</ta>
            <ta e="T676" id="Seg_4453" s="T675">ele-te</ta>
            <ta e="T688" id="Seg_4454" s="T687">rʼedka</ta>
            <ta e="T689" id="Seg_4455" s="T688">rʼedka</ta>
            <ta e="T690" id="Seg_4456" s="T689">bar-al-lar</ta>
            <ta e="T691" id="Seg_4457" s="T690">teːte-leri-ger</ta>
            <ta e="T692" id="Seg_4458" s="T691">oŋu͡ok-tar-ga</ta>
            <ta e="T693" id="Seg_4459" s="T692">inʼe-ler-i-m</ta>
            <ta e="T694" id="Seg_4460" s="T693">oŋu͡ok-tarɨ-gar</ta>
            <ta e="T695" id="Seg_4461" s="T694">bar-aːččɨ-lar</ta>
            <ta e="T696" id="Seg_4462" s="T695">taːk</ta>
            <ta e="T697" id="Seg_4463" s="T696">da</ta>
            <ta e="T698" id="Seg_4464" s="T697">onnuk</ta>
            <ta e="T699" id="Seg_4465" s="T698">baːr</ta>
            <ta e="T700" id="Seg_4466" s="T699">ojut-tar</ta>
            <ta e="T701" id="Seg_4467" s="T700">oŋu͡ok-tara</ta>
            <ta e="T702" id="Seg_4468" s="T701">baːl-lar</ta>
            <ta e="T703" id="Seg_4469" s="T702">ol</ta>
            <ta e="T704" id="Seg_4470" s="T703">kuhagan</ta>
            <ta e="T705" id="Seg_4471" s="T704">bu͡o</ta>
            <ta e="T706" id="Seg_4472" s="T705">samaːt-tar-ɨ-ŋ</ta>
            <ta e="T707" id="Seg_4473" s="T706">anʼiː</ta>
            <ta e="T708" id="Seg_4474" s="T707">bu͡ol-aːččɨ</ta>
            <ta e="T709" id="Seg_4475" s="T708">ist-i-bet-ter</ta>
            <ta e="T710" id="Seg_4476" s="T709">anɨ</ta>
            <ta e="T711" id="Seg_4477" s="T710">maladʼož-tar</ta>
            <ta e="T712" id="Seg_4478" s="T711">törüt</ta>
            <ta e="T716" id="Seg_4479" s="T715">havsʼem</ta>
            <ta e="T717" id="Seg_4480" s="T716">drugoj</ta>
            <ta e="T719" id="Seg_4481" s="T717">narot</ta>
         </annotation>
         <annotation name="mp" tierref="mp-KiPP">
            <ta e="T2" id="Seg_4482" s="T1">tu͡ok-ttAn</ta>
            <ta e="T3" id="Seg_4483" s="T2">anʼɨː</ta>
            <ta e="T4" id="Seg_4484" s="T3">di͡e-A-GIn</ta>
            <ta e="T0" id="Seg_4485" s="T4">du͡o</ta>
            <ta e="T6" id="Seg_4486" s="T5">anʼɨː</ta>
            <ta e="T7" id="Seg_4487" s="T6">bu͡ollagɨna</ta>
            <ta e="T8" id="Seg_4488" s="T7">bɨlɨr</ta>
            <ta e="T9" id="Seg_4489" s="T8">ojun-LAr</ta>
            <ta e="T10" id="Seg_4490" s="T9">baːr</ta>
            <ta e="T11" id="Seg_4491" s="T10">bu͡ol-AːččI-LAr</ta>
            <ta e="T12" id="Seg_4492" s="T11">samaːn-LAr</ta>
            <ta e="T13" id="Seg_4493" s="T12">onnuk-LAr</ta>
            <ta e="T14" id="Seg_4494" s="T13">dʼi͡e-LArI-n</ta>
            <ta e="T15" id="Seg_4495" s="T14">ergij-Ar-GA</ta>
            <ta e="T16" id="Seg_4496" s="T15">hatan-BAT</ta>
            <ta e="T17" id="Seg_4497" s="T16">kuhagan</ta>
            <ta e="T18" id="Seg_4498" s="T17">ergij-BAkkA</ta>
            <ta e="T19" id="Seg_4499" s="T18">hɨrɨt-IAK-GI-n</ta>
            <ta e="T20" id="Seg_4500" s="T19">anʼɨː</ta>
            <ta e="T21" id="Seg_4501" s="T20">bu͡ol-AːččI</ta>
            <ta e="T22" id="Seg_4502" s="T21">ol</ta>
            <ta e="T23" id="Seg_4503" s="T22">aːt-tA</ta>
            <ta e="T24" id="Seg_4504" s="T23">anʼɨː</ta>
            <ta e="T25" id="Seg_4505" s="T24">dʼaktar</ta>
            <ta e="T26" id="Seg_4506" s="T25">bu͡ollagɨna</ta>
            <ta e="T27" id="Seg_4507" s="T26">er</ta>
            <ta e="T28" id="Seg_4508" s="T27">kihi</ta>
            <ta e="T29" id="Seg_4509" s="T28">taŋas-tI-n</ta>
            <ta e="T30" id="Seg_4510" s="T29">tebis-Ar</ta>
            <ta e="T31" id="Seg_4511" s="T30">emi͡e</ta>
            <ta e="T32" id="Seg_4512" s="T31">anʼɨː</ta>
            <ta e="T33" id="Seg_4513" s="T32">čistej</ta>
            <ta e="T34" id="Seg_4514" s="T33">bu͡ol-IAK-tI-n</ta>
            <ta e="T35" id="Seg_4515" s="T34">er</ta>
            <ta e="T36" id="Seg_4516" s="T35">kihi</ta>
            <ta e="T37" id="Seg_4517" s="T36">čeber-LIk</ta>
            <ta e="T38" id="Seg_4518" s="T37">hɨrɨt-IAK-tI-n</ta>
            <ta e="T39" id="Seg_4519" s="T38">naːda</ta>
            <ta e="T40" id="Seg_4520" s="T39">dʼaktar</ta>
            <ta e="T41" id="Seg_4521" s="T40">ɨrbaːkɨ-tA</ta>
            <ta e="T42" id="Seg_4522" s="T41">hu͡ok</ta>
            <ta e="T43" id="Seg_4523" s="T42">hɨrɨt-Ar</ta>
            <ta e="T44" id="Seg_4524" s="T43">emi͡e</ta>
            <ta e="T45" id="Seg_4525" s="T44">anʼɨː</ta>
            <ta e="T46" id="Seg_4526" s="T45">urut</ta>
            <ta e="T47" id="Seg_4527" s="T46">ɨrbaːkɨ</ta>
            <ta e="T48" id="Seg_4528" s="T47">ket-IAK-tI-n</ta>
            <ta e="T49" id="Seg_4529" s="T48">naːda</ta>
            <ta e="T50" id="Seg_4530" s="T49">platʼje</ta>
            <ta e="T51" id="Seg_4531" s="T50">er</ta>
            <ta e="T52" id="Seg_4532" s="T51">kihi</ta>
            <ta e="T53" id="Seg_4533" s="T52">kördük</ta>
            <ta e="T55" id="Seg_4534" s="T54">anɨ</ta>
            <ta e="T56" id="Seg_4535" s="T55">barɨ-kAːN-LArA</ta>
            <ta e="T57" id="Seg_4536" s="T56">hɨ͡aldʼa-LAːK-LAr</ta>
            <ta e="T58" id="Seg_4537" s="T57">iti</ta>
            <ta e="T59" id="Seg_4538" s="T58">gri͡ex</ta>
            <ta e="T60" id="Seg_4539" s="T59">bu͡o</ta>
            <ta e="T61" id="Seg_4540" s="T60">ol-tI-ŋ</ta>
            <ta e="T68" id="Seg_4541" s="T66">eː</ta>
            <ta e="T69" id="Seg_4542" s="T68">dʼe</ta>
            <ta e="T70" id="Seg_4543" s="T69">barɨ-tA</ta>
            <ta e="T71" id="Seg_4544" s="T70">er</ta>
            <ta e="T72" id="Seg_4545" s="T71">kihi-GA</ta>
            <ta e="T73" id="Seg_4546" s="T72">di͡eri</ta>
            <ta e="T74" id="Seg_4547" s="T73">kuhagan</ta>
            <ta e="T75" id="Seg_4548" s="T74">ol</ta>
            <ta e="T76" id="Seg_4549" s="T75">dʼaktar</ta>
            <ta e="T77" id="Seg_4550" s="T76">dʼaktar</ta>
            <ta e="T78" id="Seg_4551" s="T77">kördük</ta>
            <ta e="T79" id="Seg_4552" s="T78">bu͡ol-IAK-tI-n</ta>
            <ta e="T80" id="Seg_4553" s="T79">naːda</ta>
            <ta e="T81" id="Seg_4554" s="T80">ɨrbaːkɨ-LAːK</ta>
            <ta e="T82" id="Seg_4555" s="T81">pɨlaːt-LAːK</ta>
            <ta e="T83" id="Seg_4556" s="T82">bu͡ol-IAK-tI-n</ta>
            <ta e="T84" id="Seg_4557" s="T83">naːda</ta>
            <ta e="T85" id="Seg_4558" s="T84">bergehe-nI</ta>
            <ta e="T86" id="Seg_4559" s="T85">bagas</ta>
            <ta e="T87" id="Seg_4560" s="T86">ket-I-mInA</ta>
            <ta e="T88" id="Seg_4561" s="T87">onton</ta>
            <ta e="T89" id="Seg_4562" s="T88">ket-IAK-GA</ta>
            <ta e="T90" id="Seg_4563" s="T89">naːda</ta>
            <ta e="T723" id="Seg_4564" s="T90">saːpkɨ-nI</ta>
            <ta e="T92" id="Seg_4565" s="T91">iti</ta>
            <ta e="T93" id="Seg_4566" s="T92">onnuk-LAr</ta>
            <ta e="T94" id="Seg_4567" s="T93">onton</ta>
            <ta e="T95" id="Seg_4568" s="T94">er-GA</ta>
            <ta e="T96" id="Seg_4569" s="T95">bar-Ar-I-ŋ</ta>
            <ta e="T97" id="Seg_4570" s="T96">hagɨna</ta>
            <ta e="T98" id="Seg_4571" s="T97">iliː</ta>
            <ta e="T99" id="Seg_4572" s="T98">ogus-A-s-Ar-LAr</ta>
            <ta e="T100" id="Seg_4573" s="T99">taŋara-GA</ta>
            <ta e="T101" id="Seg_4574" s="T100">itigirdik</ta>
            <ta e="T102" id="Seg_4575" s="T101">bi͡er-Ar-LAr</ta>
            <ta e="T103" id="Seg_4576" s="T102">iliː-LArI-n</ta>
            <ta e="T104" id="Seg_4577" s="T103">er-GA</ta>
            <ta e="T105" id="Seg_4578" s="T104">bar-Ar</ta>
            <ta e="T724" id="Seg_4579" s="T105">kihi</ta>
            <ta e="T107" id="Seg_4580" s="T106">iliː</ta>
            <ta e="T108" id="Seg_4581" s="T107">bi͡er-s-Ar-LAr</ta>
            <ta e="T109" id="Seg_4582" s="T108">oččogo</ta>
            <ta e="T110" id="Seg_4583" s="T109">iliː-LArI-n</ta>
            <ta e="T111" id="Seg_4584" s="T110">bi͡er-TAK-TArInA</ta>
            <ta e="T112" id="Seg_4585" s="T111">dʼaktar</ta>
            <ta e="T113" id="Seg_4586" s="T112">bu͡ol-Ar</ta>
            <ta e="T114" id="Seg_4587" s="T113">ol</ta>
            <ta e="T725" id="Seg_4588" s="T114">kihi</ta>
            <ta e="T116" id="Seg_4589" s="T115">onnuk</ta>
            <ta e="T117" id="Seg_4590" s="T116">obɨčaj-LAr</ta>
            <ta e="T118" id="Seg_4591" s="T117">onnuk</ta>
            <ta e="T119" id="Seg_4592" s="T118">eː</ta>
            <ta e="T120" id="Seg_4593" s="T119">küːs-GA</ta>
            <ta e="T121" id="Seg_4594" s="T120">bi͡er-Ar-LAr</ta>
            <ta e="T122" id="Seg_4595" s="T121">bu͡o</ta>
            <ta e="T123" id="Seg_4596" s="T122">kihi-nI</ta>
            <ta e="T124" id="Seg_4597" s="T123">bagar</ta>
            <ta e="T125" id="Seg_4598" s="T124">da</ta>
            <ta e="T126" id="Seg_4599" s="T125">bagar-I-m</ta>
            <ta e="T127" id="Seg_4600" s="T126">da</ta>
            <ta e="T128" id="Seg_4601" s="T127">bi͡er-Ar-LAr</ta>
            <ta e="T129" id="Seg_4602" s="T128">hin</ta>
            <ta e="T130" id="Seg_4603" s="T129">biːr</ta>
            <ta e="T131" id="Seg_4604" s="T130">ɨtaː-A-ɨtaː-A</ta>
            <ta e="T132" id="Seg_4605" s="T131">bar-A</ta>
            <ta e="T133" id="Seg_4606" s="T132">tur-IAK-ŋ</ta>
            <ta e="T134" id="Seg_4607" s="T133">ol-GA</ta>
            <ta e="T135" id="Seg_4608" s="T134">ol</ta>
            <ta e="T136" id="Seg_4609" s="T135">kördük</ta>
            <ta e="T137" id="Seg_4610" s="T136">min</ta>
            <ta e="T138" id="Seg_4611" s="T137">bar-A</ta>
            <ta e="T139" id="Seg_4612" s="T138">tur-BIT-I-m</ta>
            <ta e="T140" id="Seg_4613" s="T139">er-GA</ta>
            <ta e="T141" id="Seg_4614" s="T140">ɨtaː-A-ɨtaː-A-BIn</ta>
            <ta e="T142" id="Seg_4615" s="T141">u͡on</ta>
            <ta e="T143" id="Seg_4616" s="T142">alta</ta>
            <ta e="T144" id="Seg_4617" s="T143">dʼɨl-LAːK-BA-r</ta>
            <ta e="T145" id="Seg_4618" s="T144">ka</ta>
            <ta e="T146" id="Seg_4619" s="T145">tu͡ok</ta>
            <ta e="T147" id="Seg_4620" s="T146">kihi-tA</ta>
            <ta e="T148" id="Seg_4621" s="T147">er-GA</ta>
            <ta e="T149" id="Seg_4622" s="T148">bar-IAK.[tA]=Ij</ta>
            <ta e="T150" id="Seg_4623" s="T149">ol</ta>
            <ta e="T151" id="Seg_4624" s="T150">u͡on</ta>
            <ta e="T152" id="Seg_4625" s="T151">alta</ta>
            <ta e="T153" id="Seg_4626" s="T152">dʼɨl-LAːK-GA</ta>
            <ta e="T154" id="Seg_4627" s="T153">bi͡er-BIT-LArA</ta>
            <ta e="T157" id="Seg_4628" s="T156">hapsi͡em</ta>
            <ta e="T158" id="Seg_4629" s="T157">ogo</ta>
            <ta e="T159" id="Seg_4630" s="T158">ɨraːs</ta>
            <ta e="T160" id="Seg_4631" s="T159">e-TI-m</ta>
            <ta e="T161" id="Seg_4632" s="T160">haːtar</ta>
            <ta e="T162" id="Seg_4633" s="T161">mʼesnaj-tA</ta>
            <ta e="T163" id="Seg_4634" s="T162">da</ta>
            <ta e="T164" id="Seg_4635" s="T163">hu͡ok</ta>
            <ta e="T165" id="Seg_4636" s="T164">e-TI-m</ta>
            <ta e="T166" id="Seg_4637" s="T165">onton</ta>
            <ta e="T167" id="Seg_4638" s="T166">iti</ta>
            <ta e="T168" id="Seg_4639" s="T167">tagɨs-TI-tA</ta>
            <ta e="T169" id="Seg_4640" s="T168">ulakan</ta>
            <ta e="T170" id="Seg_4641" s="T169">kɨːs-I-m</ta>
            <ta e="T171" id="Seg_4642" s="T170">anɨ</ta>
            <ta e="T172" id="Seg_4643" s="T171">bu͡o</ta>
            <ta e="T173" id="Seg_4644" s="T172">iti</ta>
            <ta e="T174" id="Seg_4645" s="T173">iti</ta>
            <ta e="T175" id="Seg_4646" s="T174">ogo-m</ta>
            <ta e="T176" id="Seg_4647" s="T175">u͡on</ta>
            <ta e="T177" id="Seg_4648" s="T176">hette</ta>
            <ta e="T178" id="Seg_4649" s="T177">u͡on</ta>
            <ta e="T179" id="Seg_4650" s="T178">agɨs</ta>
            <ta e="T180" id="Seg_4651" s="T179">dʼɨl-LAːK-BA-r</ta>
            <ta e="T181" id="Seg_4652" s="T180">töröː-BIT</ta>
            <ta e="T182" id="Seg_4653" s="T181">ogo</ta>
            <ta e="T183" id="Seg_4654" s="T182">iti</ta>
            <ta e="T184" id="Seg_4655" s="T183">dʼe</ta>
            <ta e="T185" id="Seg_4656" s="T184">onton</ta>
            <ta e="T186" id="Seg_4657" s="T185">kɨrɨj-An</ta>
            <ta e="T187" id="Seg_4658" s="T186">kɨrɨj-An-BIn</ta>
            <ta e="T188" id="Seg_4659" s="T187">iti</ta>
            <ta e="T189" id="Seg_4660" s="T188">kojut</ta>
            <ta e="T190" id="Seg_4661" s="T189">kojut</ta>
            <ta e="T191" id="Seg_4662" s="T190">onton</ta>
            <ta e="T192" id="Seg_4663" s="T191">du͡o</ta>
            <ta e="T194" id="Seg_4664" s="T193">ogo-LAN-An</ta>
            <ta e="T195" id="Seg_4665" s="T194">da</ta>
            <ta e="T196" id="Seg_4666" s="T195">ogo-LAN-An</ta>
            <ta e="T197" id="Seg_4667" s="T196">is-A-GIn</ta>
            <ta e="T198" id="Seg_4668" s="T197">bu͡o</ta>
            <ta e="T203" id="Seg_4669" s="T202">hürdeːk</ta>
            <ta e="T204" id="Seg_4670" s="T203">üčügej</ta>
            <ta e="T205" id="Seg_4671" s="T204">kihi</ta>
            <ta e="T206" id="Seg_4672" s="T205">e-TI-tA</ta>
            <ta e="T207" id="Seg_4673" s="T206">er-I-m</ta>
            <ta e="T208" id="Seg_4674" s="T207">bu</ta>
            <ta e="T209" id="Seg_4675" s="T208">olor-AːččI</ta>
            <ta e="T210" id="Seg_4676" s="T209">kihi</ta>
            <ta e="T726" id="Seg_4677" s="T210">iti</ta>
            <ta e="T212" id="Seg_4678" s="T211">iti</ta>
            <ta e="T213" id="Seg_4679" s="T212">min</ta>
            <ta e="T214" id="Seg_4680" s="T213">togus</ta>
            <ta e="T215" id="Seg_4681" s="T214">ogo-LAːK</ta>
            <ta e="T216" id="Seg_4682" s="T215">er-TI-LAːK-I-m</ta>
            <ta e="T217" id="Seg_4683" s="T216">bu͡o</ta>
            <ta e="T218" id="Seg_4684" s="T217">össü͡ö</ta>
            <ta e="T219" id="Seg_4685" s="T218">össü͡ö</ta>
            <ta e="T220" id="Seg_4686" s="T219">da</ta>
            <ta e="T221" id="Seg_4687" s="T220">maladoj</ta>
            <ta e="T222" id="Seg_4688" s="T221">bu͡ol-BAtAK-BIn</ta>
            <ta e="T223" id="Seg_4689" s="T222">itinne</ta>
            <ta e="T224" id="Seg_4690" s="T223">dʼe</ta>
            <ta e="T225" id="Seg_4691" s="T224">onnuk</ta>
            <ta e="T226" id="Seg_4692" s="T225">obɨčaj-LAːK</ta>
            <ta e="T227" id="Seg_4693" s="T226">e-TI-LArA</ta>
            <ta e="T228" id="Seg_4694" s="T227">onton</ta>
            <ta e="T229" id="Seg_4695" s="T228">hin</ta>
            <ta e="T230" id="Seg_4696" s="T229">olok</ta>
            <ta e="T231" id="Seg_4697" s="T230">olor-A-GIn</ta>
            <ta e="T232" id="Seg_4698" s="T231">tulaːjak-LAr-nI</ta>
            <ta e="T233" id="Seg_4699" s="T232">ahaː-t-I-ŋ</ta>
            <ta e="T727" id="Seg_4700" s="T233">di͡e-AːččI-LAr</ta>
            <ta e="T235" id="Seg_4701" s="T234">tulaːjak</ta>
            <ta e="T236" id="Seg_4702" s="T235">ogo-LArA</ta>
            <ta e="T237" id="Seg_4703" s="T236">anʼɨː</ta>
            <ta e="T238" id="Seg_4704" s="T237">bu͡ol-IAK.[tA]</ta>
            <ta e="T239" id="Seg_4705" s="T238">ogo-LArA</ta>
            <ta e="T240" id="Seg_4706" s="T239">ahaː-t-I-ŋ</ta>
            <ta e="T241" id="Seg_4707" s="T240">di͡e-AːččI-LAr</ta>
            <ta e="T242" id="Seg_4708" s="T241">kuhagan-LIk</ta>
            <ta e="T243" id="Seg_4709" s="T242">gɨn-BIT</ta>
            <ta e="T244" id="Seg_4710" s="T243">kihi-nI</ta>
            <ta e="T245" id="Seg_4711" s="T244">üčügej-LIk</ta>
            <ta e="T246" id="Seg_4712" s="T245">ataːr-I-ŋ</ta>
            <ta e="T247" id="Seg_4713" s="T246">di͡e-AːččI-LAr</ta>
            <ta e="T248" id="Seg_4714" s="T247">töttörü</ta>
            <ta e="T249" id="Seg_4715" s="T248">togo</ta>
            <ta e="T250" id="Seg_4716" s="T249">bu͡ol-Ar</ta>
            <ta e="T251" id="Seg_4717" s="T250">di͡e-TAK-BInA</ta>
            <ta e="T252" id="Seg_4718" s="T251">anʼɨː</ta>
            <ta e="T253" id="Seg_4719" s="T252">bu͡ol-IAK.[tA]</ta>
            <ta e="T254" id="Seg_4720" s="T253">onton</ta>
            <ta e="T255" id="Seg_4721" s="T254">beje-ŋ</ta>
            <ta e="T256" id="Seg_4722" s="T255">ol</ta>
            <ta e="T257" id="Seg_4723" s="T256">üčügej-LIk</ta>
            <ta e="T258" id="Seg_4724" s="T257">olor-TAK-GInA</ta>
            <ta e="T259" id="Seg_4725" s="T258">töttörü</ta>
            <ta e="T260" id="Seg_4726" s="T259">beje-GA-r</ta>
            <ta e="T261" id="Seg_4727" s="T260">ɨmsɨːr-IAK-LArA</ta>
            <ta e="T262" id="Seg_4728" s="T261">di͡e-AːččI-LAr</ta>
            <ta e="T263" id="Seg_4729" s="T262">bɨlɨrgɨ-LAr</ta>
            <ta e="T264" id="Seg_4730" s="T263">obɨčaj-LAr</ta>
            <ta e="T265" id="Seg_4731" s="T264">onnuk</ta>
            <ta e="T266" id="Seg_4732" s="T265">onton</ta>
            <ta e="T267" id="Seg_4733" s="T266">dʼolloːk</ta>
            <ta e="T268" id="Seg_4734" s="T267">bu͡ol-TI-ŋ</ta>
            <ta e="T269" id="Seg_4735" s="T268">beje-ŋ</ta>
            <ta e="T270" id="Seg_4736" s="T269">kojut</ta>
            <ta e="T271" id="Seg_4737" s="T270">dʼolloːk</ta>
            <ta e="T272" id="Seg_4738" s="T271">bu͡ol-TI-ŋ</ta>
            <ta e="T273" id="Seg_4739" s="T272">di͡e-TI-tA</ta>
            <ta e="T274" id="Seg_4740" s="T273">min</ta>
            <ta e="T275" id="Seg_4741" s="T274">anɨ</ta>
            <ta e="T276" id="Seg_4742" s="T275">dʼolloːk-BIn</ta>
            <ta e="T277" id="Seg_4743" s="T276">ol</ta>
            <ta e="T278" id="Seg_4744" s="T277">tulaːjak</ta>
            <ta e="T279" id="Seg_4745" s="T278">e-TI-m</ta>
            <ta e="T280" id="Seg_4746" s="T279">bu͡o</ta>
            <ta e="T281" id="Seg_4747" s="T280">onton</ta>
            <ta e="T282" id="Seg_4748" s="T281">inʼe-m</ta>
            <ta e="T283" id="Seg_4749" s="T282">öl-BIT-tI-GAr</ta>
            <ta e="T284" id="Seg_4750" s="T283">er-GA</ta>
            <ta e="T285" id="Seg_4751" s="T284">bi͡er-BIT-tI-n</ta>
            <ta e="T286" id="Seg_4752" s="T285">genne</ta>
            <ta e="T287" id="Seg_4753" s="T286">eː</ta>
            <ta e="T288" id="Seg_4754" s="T287">er-GA</ta>
            <ta e="T289" id="Seg_4755" s="T288">bi͡er-A=ilik-InA</ta>
            <ta e="T290" id="Seg_4756" s="T289">ɨ͡al-ttAn</ta>
            <ta e="T291" id="Seg_4757" s="T290">ɨ͡al-GA</ta>
            <ta e="T292" id="Seg_4758" s="T291">hɨrɨt-AːččI-BIt</ta>
            <ta e="T293" id="Seg_4759" s="T292">onno</ta>
            <ta e="T294" id="Seg_4760" s="T293">inʼe-m</ta>
            <ta e="T295" id="Seg_4761" s="T294">inʼe-m</ta>
            <ta e="T296" id="Seg_4762" s="T295">öl-Ar-tI-GAr</ta>
            <ta e="T297" id="Seg_4763" s="T296">innʼe</ta>
            <ta e="T298" id="Seg_4764" s="T297">di͡e-Ar</ta>
            <ta e="T299" id="Seg_4765" s="T298">bu͡o</ta>
            <ta e="T300" id="Seg_4766" s="T299">kaja</ta>
            <ta e="T301" id="Seg_4767" s="T300">tulaːjak-LAr-nI</ta>
            <ta e="T302" id="Seg_4768" s="T301">ahaː-A-t-Aːr</ta>
            <ta e="T303" id="Seg_4769" s="T302">bu͡ol</ta>
            <ta e="T304" id="Seg_4770" s="T303">kuhagan-LIk</ta>
            <ta e="T305" id="Seg_4771" s="T304">gɨn-BIT-nI</ta>
            <ta e="T306" id="Seg_4772" s="T305">üčügej-LIk</ta>
            <ta e="T307" id="Seg_4773" s="T306">gɨn-IAK-GI-n</ta>
            <ta e="T308" id="Seg_4774" s="T307">naːda</ta>
            <ta e="T309" id="Seg_4775" s="T308">oččogo</ta>
            <ta e="T310" id="Seg_4776" s="T309">uhun</ta>
            <ta e="T311" id="Seg_4777" s="T310">dʼolloːk</ta>
            <ta e="T312" id="Seg_4778" s="T311">bu͡ol-IAK-ŋ</ta>
            <ta e="T313" id="Seg_4779" s="T312">ügüs</ta>
            <ta e="T314" id="Seg_4780" s="T313">ogo-LAːK</ta>
            <ta e="T315" id="Seg_4781" s="T314">bu͡ol-IAK-ŋ</ta>
            <ta e="T316" id="Seg_4782" s="T315">belem</ta>
            <ta e="T317" id="Seg_4783" s="T316">olok-GA</ta>
            <ta e="T318" id="Seg_4784" s="T317">olor-IAK-ŋ</ta>
            <ta e="T319" id="Seg_4785" s="T318">di͡e-BIT-tA</ta>
            <ta e="T320" id="Seg_4786" s="T319">ile</ta>
            <ta e="T321" id="Seg_4787" s="T320">kör</ta>
            <ta e="T322" id="Seg_4788" s="T321">anɨ</ta>
            <ta e="T323" id="Seg_4789" s="T322">min</ta>
            <ta e="T324" id="Seg_4790" s="T323">beje-m</ta>
            <ta e="T325" id="Seg_4791" s="T324">bu͡ollagɨna</ta>
            <ta e="T326" id="Seg_4792" s="T325">bi͡es-u͡on-ttAn</ta>
            <ta e="T327" id="Seg_4793" s="T326">taksa</ta>
            <ta e="T328" id="Seg_4794" s="T327">kihi-BI-n</ta>
            <ta e="T329" id="Seg_4795" s="T328">beje-m</ta>
            <ta e="T330" id="Seg_4796" s="T329">is-BI-ttAn</ta>
            <ta e="T331" id="Seg_4797" s="T330">tagɨs-I-BIT-I-m</ta>
            <ta e="T332" id="Seg_4798" s="T331">vnuk-LAr-LIːN-BIn</ta>
            <ta e="T336" id="Seg_4799" s="T335">noru͡ot-I-m</ta>
            <ta e="T337" id="Seg_4800" s="T336">beje-m</ta>
            <ta e="T338" id="Seg_4801" s="T337">noru͡ot-I-m</ta>
            <ta e="T339" id="Seg_4802" s="T338">Hɨndaːska</ta>
            <ta e="T340" id="Seg_4803" s="T339">teŋ</ta>
            <ta e="T341" id="Seg_4804" s="T340">aŋar-tA</ta>
            <ta e="T342" id="Seg_4805" s="T341">aŋar-tA</ta>
            <ta e="T343" id="Seg_4806" s="T342">ɨraːk</ta>
            <ta e="T344" id="Seg_4807" s="T343">baːr-LAr</ta>
            <ta e="T345" id="Seg_4808" s="T344">össü͡ö</ta>
            <ta e="T346" id="Seg_4809" s="T345">ol</ta>
            <ta e="T347" id="Seg_4810" s="T346">dek</ta>
            <ta e="T348" id="Seg_4811" s="T347">manna-GI-m</ta>
            <ta e="T349" id="Seg_4812" s="T348">ere</ta>
            <ta e="T350" id="Seg_4813" s="T349">dʼe</ta>
            <ta e="T351" id="Seg_4814" s="T350">slaːbu͡ok</ta>
            <ta e="T352" id="Seg_4815" s="T351">manna</ta>
            <ta e="T353" id="Seg_4816" s="T352">ere</ta>
            <ta e="T354" id="Seg_4817" s="T353">baːr</ta>
            <ta e="T355" id="Seg_4818" s="T354">bi͡es-u͡on-ttAn</ta>
            <ta e="T356" id="Seg_4819" s="T355">taksa</ta>
            <ta e="T357" id="Seg_4820" s="T356">kihi-BIt</ta>
            <ta e="T358" id="Seg_4821" s="T357">ogo-LAr-LIːN</ta>
            <ta e="T359" id="Seg_4822" s="T358">pačtʼi</ta>
            <ta e="T360" id="Seg_4823" s="T359">Hɨndaːska</ta>
            <ta e="T728" id="Seg_4824" s="T361">kaj-An</ta>
            <ta e="T363" id="Seg_4825" s="T362">Hɨndaːska</ta>
            <ta e="T364" id="Seg_4826" s="T363">aŋar-A-m</ta>
            <ta e="T365" id="Seg_4827" s="T364">taːk</ta>
            <ta e="T366" id="Seg_4828" s="T365">da</ta>
            <ta e="T367" id="Seg_4829" s="T366">üčügej-LIk</ta>
            <ta e="T368" id="Seg_4830" s="T367">olor-A-BIn</ta>
            <ta e="T369" id="Seg_4831" s="T368">bert-LAːK</ta>
            <ta e="T370" id="Seg_4832" s="T369">kihi-LAr</ta>
            <ta e="T371" id="Seg_4833" s="T370">algɨs-LArI-GAr</ta>
            <ta e="T372" id="Seg_4834" s="T371">kihi-LAr</ta>
            <ta e="T373" id="Seg_4835" s="T372">algɨs-LArA</ta>
            <ta e="T374" id="Seg_4836" s="T373">kɨrdʼagas-LAr</ta>
            <ta e="T375" id="Seg_4837" s="T374">algɨs-LArA</ta>
            <ta e="T376" id="Seg_4838" s="T375">anɨ</ta>
            <ta e="T377" id="Seg_4839" s="T376">bu͡o</ta>
            <ta e="T378" id="Seg_4840" s="T377">kɨrɨj-An-BIn</ta>
            <ta e="T379" id="Seg_4841" s="T378">ɨ͡arɨj-A-BIn</ta>
            <ta e="T380" id="Seg_4842" s="T379">ere</ta>
            <ta e="T381" id="Seg_4843" s="T380">bu͡o</ta>
            <ta e="T382" id="Seg_4844" s="T381">davlʼenʼija-LAːK-BIn</ta>
            <ta e="T383" id="Seg_4845" s="T382">hürdeːk</ta>
            <ta e="T384" id="Seg_4846" s="T383">ulakan</ta>
            <ta e="T386" id="Seg_4847" s="T384">ikki</ta>
            <ta e="T388" id="Seg_4848" s="T386">eː</ta>
            <ta e="T390" id="Seg_4849" s="T388">ikki</ta>
            <ta e="T391" id="Seg_4850" s="T390">hüːs-ttAn</ta>
            <ta e="T392" id="Seg_4851" s="T391">tagɨs-Ar</ta>
            <ta e="T395" id="Seg_4852" s="T394">bu͡ol-Ar</ta>
            <ta e="T396" id="Seg_4853" s="T395">ol-tI-BI-n</ta>
            <ta e="T397" id="Seg_4854" s="T396">kot-BAT-BIn</ta>
            <ta e="T398" id="Seg_4855" s="T397">onton</ta>
            <ta e="T399" id="Seg_4856" s="T398">atak-LAr-I-m</ta>
            <ta e="T400" id="Seg_4857" s="T399">kaːj-Ar-LAr</ta>
            <ta e="T401" id="Seg_4858" s="T400">anɨ</ta>
            <ta e="T402" id="Seg_4859" s="T401">biːr</ta>
            <ta e="T403" id="Seg_4860" s="T402">da</ta>
            <ta e="T404" id="Seg_4861" s="T403">ogo-GA</ta>
            <ta e="T405" id="Seg_4862" s="T404">kot-An</ta>
            <ta e="T406" id="Seg_4863" s="T405">tij-BAT-BIn</ta>
            <ta e="T407" id="Seg_4864" s="T406">bu͡o</ta>
            <ta e="T408" id="Seg_4865" s="T407">hɨt-A-BIn</ta>
            <ta e="T409" id="Seg_4866" s="T408">bu͡o</ta>
            <ta e="T423" id="Seg_4867" s="T422">araj-GA</ta>
            <ta e="T424" id="Seg_4868" s="T423">bar-IAK-m</ta>
            <ta e="T430" id="Seg_4869" s="T428">eː</ta>
            <ta e="T431" id="Seg_4870" s="T430">araj-GA</ta>
            <ta e="T432" id="Seg_4871" s="T431">bar-IAK-m</ta>
            <ta e="T433" id="Seg_4872" s="T432">ogo-LAr-I-m</ta>
            <ta e="T434" id="Seg_4873" s="T433">elbek-LAr</ta>
            <ta e="T435" id="Seg_4874" s="T434">araj-GA</ta>
            <ta e="T436" id="Seg_4875" s="T435">köt-I-t-IAK-LArA</ta>
            <ta e="T440" id="Seg_4876" s="T439">taŋara-GA</ta>
            <ta e="T441" id="Seg_4877" s="T440">ka</ta>
            <ta e="T442" id="Seg_4878" s="T441">taŋara-GA</ta>
            <ta e="T443" id="Seg_4879" s="T442">bar-IAK-m</ta>
            <ta e="T452" id="Seg_4880" s="T451">di͡e-AːččI-LAr</ta>
            <ta e="T453" id="Seg_4881" s="T452">kɨrdʼagas-LAr</ta>
            <ta e="T454" id="Seg_4882" s="T453">taŋara-GA</ta>
            <ta e="T455" id="Seg_4883" s="T454">bar-IAK-GIt</ta>
            <ta e="T457" id="Seg_4884" s="T456">ügüs</ta>
            <ta e="T458" id="Seg_4885" s="T457">ogo-LAːK</ta>
            <ta e="T459" id="Seg_4886" s="T458">kihi</ta>
            <ta e="T460" id="Seg_4887" s="T459">taŋara-GA</ta>
            <ta e="T461" id="Seg_4888" s="T460">bar-IAK-m</ta>
            <ta e="T462" id="Seg_4889" s="T461">kihi-nI</ta>
            <ta e="T463" id="Seg_4890" s="T462">kuhagan-LIk</ta>
            <ta e="T464" id="Seg_4891" s="T463">haŋar-BAT-BIn</ta>
            <ta e="T465" id="Seg_4892" s="T464">kihi-nI</ta>
            <ta e="T466" id="Seg_4893" s="T465">kɨraː-A-s-BAT-BIn</ta>
            <ta e="T467" id="Seg_4894" s="T466">anʼɨː</ta>
            <ta e="T468" id="Seg_4895" s="T467">bu͡ol-IAK.[tA]</ta>
            <ta e="T469" id="Seg_4896" s="T468">ol</ta>
            <ta e="T470" id="Seg_4897" s="T469">ihin</ta>
            <ta e="T471" id="Seg_4898" s="T470">min</ta>
            <ta e="T472" id="Seg_4899" s="T471">kihi-nI</ta>
            <ta e="T473" id="Seg_4900" s="T472">kɨraː-A-s-A</ta>
            <ta e="T474" id="Seg_4901" s="T473">hataː-AːččI-tA</ta>
            <ta e="T475" id="Seg_4902" s="T474">hu͡ok-BIn</ta>
            <ta e="T476" id="Seg_4903" s="T475">anʼɨː</ta>
            <ta e="T477" id="Seg_4904" s="T476">bu͡ol-IAK.[tA]</ta>
            <ta e="T478" id="Seg_4905" s="T477">kim</ta>
            <ta e="T479" id="Seg_4906" s="T478">kiːr-BIT</ta>
            <ta e="T480" id="Seg_4907" s="T479">ahaː-A-t-An</ta>
            <ta e="T481" id="Seg_4908" s="T480">is-A-BIn</ta>
            <ta e="T482" id="Seg_4909" s="T481">et-I-m</ta>
            <ta e="T483" id="Seg_4910" s="T482">kot-I-n-Ar</ta>
            <ta e="T484" id="Seg_4911" s="T483">er-TAK-BInA</ta>
            <ta e="T485" id="Seg_4912" s="T484">anɨ</ta>
            <ta e="T486" id="Seg_4913" s="T485">kot-I-n-BAT-BIn</ta>
            <ta e="T487" id="Seg_4914" s="T486">Dunʼasa-m</ta>
            <ta e="T488" id="Seg_4915" s="T487">ere</ta>
            <ta e="T489" id="Seg_4916" s="T488">e-BIT</ta>
            <ta e="T491" id="Seg_4917" s="T490">üčügej</ta>
            <ta e="T492" id="Seg_4918" s="T491">kiniːt-I-m</ta>
            <ta e="T493" id="Seg_4919" s="T492">hürdeːk</ta>
            <ta e="T494" id="Seg_4920" s="T493">üčügej</ta>
            <ta e="T495" id="Seg_4921" s="T494">dʼaktar</ta>
            <ta e="T496" id="Seg_4922" s="T495">üčügej</ta>
            <ta e="T497" id="Seg_4923" s="T496">dʼaktar-LAr</ta>
            <ta e="T498" id="Seg_4924" s="T497">kiniːt-LAr-I-m</ta>
            <ta e="T499" id="Seg_4925" s="T498">taːk</ta>
            <ta e="T500" id="Seg_4926" s="T499">da</ta>
            <ta e="T501" id="Seg_4927" s="T500">ol</ta>
            <ta e="T502" id="Seg_4928" s="T501">kajdak</ta>
            <ta e="T503" id="Seg_4929" s="T502">da</ta>
            <ta e="T504" id="Seg_4930" s="T503">bu͡ol-TIn</ta>
            <ta e="T505" id="Seg_4931" s="T504">min</ta>
            <ta e="T506" id="Seg_4932" s="T505">kɨhalɨn-I-BAT-BIn</ta>
            <ta e="T507" id="Seg_4933" s="T506">giniler-GA</ta>
            <ta e="T508" id="Seg_4934" s="T507">kerij-s-TInnAr</ta>
            <ta e="T509" id="Seg_4935" s="T508">da</ta>
            <ta e="T510" id="Seg_4936" s="T509">kerij-s-I-BA-TInnAr</ta>
            <ta e="T511" id="Seg_4937" s="T510">da</ta>
            <ta e="T512" id="Seg_4938" s="T511">min-GA</ta>
            <ta e="T513" id="Seg_4939" s="T512">naːda-tA</ta>
            <ta e="T514" id="Seg_4940" s="T513">hu͡ok</ta>
            <ta e="T515" id="Seg_4941" s="T514">tuspa</ta>
            <ta e="T516" id="Seg_4942" s="T515">ɨ͡al-LAr</ta>
            <ta e="T517" id="Seg_4943" s="T516">bu͡o</ta>
            <ta e="T518" id="Seg_4944" s="T517">tu͡ok-GA</ta>
            <ta e="T519" id="Seg_4945" s="T518">kɨhalɨn-IAK-m=Ij</ta>
            <ta e="T520" id="Seg_4946" s="T519">ol-GA</ta>
            <ta e="T521" id="Seg_4947" s="T520">min</ta>
            <ta e="T522" id="Seg_4948" s="T521">olok-I-m</ta>
            <ta e="T523" id="Seg_4949" s="T522">büt-BIT-tA</ta>
            <ta e="T524" id="Seg_4950" s="T523">giniler</ta>
            <ta e="T525" id="Seg_4951" s="T524">olok-LArI-n</ta>
            <ta e="T526" id="Seg_4952" s="T525">tɨːt-BAT-BIn</ta>
            <ta e="T527" id="Seg_4953" s="T526">min</ta>
            <ta e="T528" id="Seg_4954" s="T527">olor-TInnAr</ta>
            <ta e="T532" id="Seg_4955" s="T531">ele-tA</ta>
            <ta e="T533" id="Seg_4956" s="T532">onton</ta>
            <ta e="T534" id="Seg_4957" s="T533">ka</ta>
            <ta e="T535" id="Seg_4958" s="T534">tu͡ok-nI</ta>
            <ta e="T536" id="Seg_4959" s="T535">di͡e-A-t-A-GIn</ta>
            <ta e="T552" id="Seg_4960" s="T550">eː</ta>
            <ta e="T571" id="Seg_4961" s="T570">ihit-I-BAT-LAr</ta>
            <ta e="T572" id="Seg_4962" s="T571">törüt</ta>
            <ta e="T573" id="Seg_4963" s="T572">iti</ta>
            <ta e="T574" id="Seg_4964" s="T573">bar-An</ta>
            <ta e="T575" id="Seg_4965" s="T574">kaːl-Ar-LAr</ta>
            <ta e="T576" id="Seg_4966" s="T575">iti</ta>
            <ta e="T577" id="Seg_4967" s="T576">bu</ta>
            <ta e="T578" id="Seg_4968" s="T577">er-I-m</ta>
            <ta e="T579" id="Seg_4969" s="T578">edʼij-tI-n</ta>
            <ta e="T580" id="Seg_4970" s="T579">eː</ta>
            <ta e="T581" id="Seg_4971" s="T580">er-I-m</ta>
            <ta e="T582" id="Seg_4972" s="T581">balɨs-I-tI-n</ta>
            <ta e="T583" id="Seg_4973" s="T582">ogo-LAr-tA</ta>
            <ta e="T584" id="Seg_4974" s="T583">iti</ta>
            <ta e="T585" id="Seg_4975" s="T584">bar-AːččI-LAr</ta>
            <ta e="T587" id="Seg_4976" s="T586">oŋu͡ok-LAr-GA</ta>
            <ta e="T588" id="Seg_4977" s="T587">min</ta>
            <ta e="T589" id="Seg_4978" s="T588">ogo-LAr-BI-n</ta>
            <ta e="T590" id="Seg_4979" s="T589">kɨtta</ta>
            <ta e="T591" id="Seg_4980" s="T590">inʼe</ta>
            <ta e="T592" id="Seg_4981" s="T591">inʼe-tA</ta>
            <ta e="T593" id="Seg_4982" s="T592">öl-BIT-tI-n</ta>
            <ta e="T594" id="Seg_4983" s="T593">genne</ta>
            <ta e="T595" id="Seg_4984" s="T594">haŋardɨː</ta>
            <ta e="T596" id="Seg_4985" s="T595">kel-Ar</ta>
            <ta e="T597" id="Seg_4986" s="T596">küččügüj</ta>
            <ta e="T598" id="Seg_4987" s="T597">u͡ol-tA</ta>
            <ta e="T599" id="Seg_4988" s="T598">ol-tI-ŋ</ta>
            <ta e="T600" id="Seg_4989" s="T599">bar-Ar</ta>
            <ta e="T601" id="Seg_4990" s="T600">ol-nI</ta>
            <ta e="T602" id="Seg_4991" s="T601">ataːr-s-Ar-LAr</ta>
            <ta e="T603" id="Seg_4992" s="T602">iti</ta>
            <ta e="T604" id="Seg_4993" s="T603">ol-nI</ta>
            <ta e="T605" id="Seg_4994" s="T604">bar-I-m-ŋ</ta>
            <ta e="T606" id="Seg_4995" s="T605">di͡e-A-BIn</ta>
            <ta e="T607" id="Seg_4996" s="T606">ihit-I-BAT-LAr</ta>
            <ta e="T608" id="Seg_4997" s="T607">bu͡o</ta>
            <ta e="T609" id="Seg_4998" s="T608">tu͡ok-nI</ta>
            <ta e="T610" id="Seg_4999" s="T609">gɨn-A</ta>
            <ta e="T611" id="Seg_5000" s="T610">bar-Ar-LArA</ta>
            <ta e="T612" id="Seg_5001" s="T611">dʼürü</ta>
            <ta e="T613" id="Seg_5002" s="T612">gri͡ex</ta>
            <ta e="T614" id="Seg_5003" s="T613">bu͡o</ta>
            <ta e="T615" id="Seg_5004" s="T614">anɨ</ta>
            <ta e="T616" id="Seg_5005" s="T615">kihi</ta>
            <ta e="T617" id="Seg_5006" s="T616">hir</ta>
            <ta e="T618" id="Seg_5007" s="T617">aːjɨ</ta>
            <ta e="T619" id="Seg_5008" s="T618">öl-A</ta>
            <ta e="T620" id="Seg_5009" s="T619">hɨt-Ar</ta>
            <ta e="T621" id="Seg_5010" s="T620">kihi</ta>
            <ta e="T622" id="Seg_5011" s="T621">kuttan-IAK</ta>
            <ta e="T623" id="Seg_5012" s="T622">anɨ</ta>
            <ta e="T624" id="Seg_5013" s="T623">itinne</ta>
            <ta e="T625" id="Seg_5014" s="T624">biːr</ta>
            <ta e="T626" id="Seg_5015" s="T625">emeːksin</ta>
            <ta e="T627" id="Seg_5016" s="T626">öl-TI-tA</ta>
            <ta e="T628" id="Seg_5017" s="T627">anɨ</ta>
            <ta e="T629" id="Seg_5018" s="T628">biːr</ta>
            <ta e="T630" id="Seg_5019" s="T629">ogo</ta>
            <ta e="T631" id="Seg_5020" s="T630">kel-An</ta>
            <ta e="T632" id="Seg_5021" s="T631">is-An</ta>
            <ta e="T633" id="Seg_5022" s="T632">öl-BIT</ta>
            <ta e="T634" id="Seg_5023" s="T633">emi͡e</ta>
            <ta e="T635" id="Seg_5024" s="T634">kaːrɨ͡an-LAːK</ta>
            <ta e="T636" id="Seg_5025" s="T635">ogo-BIt</ta>
            <ta e="T637" id="Seg_5026" s="T636">hürdeːk</ta>
            <ta e="T638" id="Seg_5027" s="T637">üčügej</ta>
            <ta e="T639" id="Seg_5028" s="T638">ogo</ta>
            <ta e="T640" id="Seg_5029" s="T639">e-TI-tA</ta>
            <ta e="T641" id="Seg_5030" s="T640">buran-I-nAn</ta>
            <ta e="T642" id="Seg_5031" s="T641">taskaj-LAN-A</ta>
            <ta e="T643" id="Seg_5032" s="T642">hɨrɨt-Ar</ta>
            <ta e="T644" id="Seg_5033" s="T643">e-TI-tA</ta>
            <ta e="T662" id="Seg_5034" s="T661">eː</ta>
            <ta e="T663" id="Seg_5035" s="T662">bu</ta>
            <ta e="T664" id="Seg_5036" s="T663">tabaːk-TA</ta>
            <ta e="T665" id="Seg_5037" s="T664">bɨrak-IAK-GI-n</ta>
            <ta e="T666" id="Seg_5038" s="T665">naːda</ta>
            <ta e="T670" id="Seg_5039" s="T668">aragiː</ta>
            <ta e="T671" id="Seg_5040" s="T670">is-TAK-TArInA</ta>
            <ta e="T672" id="Seg_5041" s="T671">aragiː-TA</ta>
            <ta e="T673" id="Seg_5042" s="T672">kut-IAK-GI-n</ta>
            <ta e="T674" id="Seg_5043" s="T673">naːda</ta>
            <ta e="T676" id="Seg_5044" s="T675">ele-tA</ta>
            <ta e="T688" id="Seg_5045" s="T687">rʼedka</ta>
            <ta e="T689" id="Seg_5046" s="T688">rʼedka</ta>
            <ta e="T690" id="Seg_5047" s="T689">bar-Ar-LAr</ta>
            <ta e="T691" id="Seg_5048" s="T690">teːte-LArI-GAr</ta>
            <ta e="T692" id="Seg_5049" s="T691">oŋu͡ok-LAr-GA</ta>
            <ta e="T693" id="Seg_5050" s="T692">inʼe-LAr-I-m</ta>
            <ta e="T694" id="Seg_5051" s="T693">oŋu͡ok-LArI-GAr</ta>
            <ta e="T695" id="Seg_5052" s="T694">bar-AːččI-LAr</ta>
            <ta e="T696" id="Seg_5053" s="T695">taːk</ta>
            <ta e="T697" id="Seg_5054" s="T696">da</ta>
            <ta e="T698" id="Seg_5055" s="T697">onnuk</ta>
            <ta e="T699" id="Seg_5056" s="T698">baːr</ta>
            <ta e="T700" id="Seg_5057" s="T699">ojun-LAr</ta>
            <ta e="T701" id="Seg_5058" s="T700">oŋu͡ok-LArA</ta>
            <ta e="T702" id="Seg_5059" s="T701">baːr-LAr</ta>
            <ta e="T703" id="Seg_5060" s="T702">ol</ta>
            <ta e="T704" id="Seg_5061" s="T703">kuhagan</ta>
            <ta e="T705" id="Seg_5062" s="T704">bu͡o</ta>
            <ta e="T706" id="Seg_5063" s="T705">samaːn-LAr-I-ŋ</ta>
            <ta e="T707" id="Seg_5064" s="T706">anʼɨː</ta>
            <ta e="T708" id="Seg_5065" s="T707">bu͡ol-AːččI</ta>
            <ta e="T709" id="Seg_5066" s="T708">ihit-I-BAT-LAr</ta>
            <ta e="T710" id="Seg_5067" s="T709">anɨ</ta>
            <ta e="T711" id="Seg_5068" s="T710">maladʼož-LAr</ta>
            <ta e="T712" id="Seg_5069" s="T711">törüt</ta>
            <ta e="T716" id="Seg_5070" s="T715">hapsi͡em</ta>
            <ta e="T717" id="Seg_5071" s="T716">drugoj</ta>
            <ta e="T719" id="Seg_5072" s="T717">noru͡ot</ta>
         </annotation>
         <annotation name="ge" tierref="ge-KiPP">
            <ta e="T2" id="Seg_5073" s="T1">what-ABL</ta>
            <ta e="T3" id="Seg_5074" s="T2">sin.[NOM]</ta>
            <ta e="T4" id="Seg_5075" s="T3">say-PRS-2SG</ta>
            <ta e="T0" id="Seg_5076" s="T4">Q</ta>
            <ta e="T6" id="Seg_5077" s="T5">sin.[NOM]</ta>
            <ta e="T7" id="Seg_5078" s="T6">though</ta>
            <ta e="T8" id="Seg_5079" s="T7">long.ago</ta>
            <ta e="T9" id="Seg_5080" s="T8">shaman-PL.[NOM]</ta>
            <ta e="T10" id="Seg_5081" s="T9">there.is</ta>
            <ta e="T11" id="Seg_5082" s="T10">be-HAB-3PL</ta>
            <ta e="T12" id="Seg_5083" s="T11">shaman-PL.[NOM]</ta>
            <ta e="T13" id="Seg_5084" s="T12">such-PL.[NOM]</ta>
            <ta e="T14" id="Seg_5085" s="T13">tent-3PL-ACC</ta>
            <ta e="T15" id="Seg_5086" s="T14">turn-PTCP.PRS-DAT/LOC</ta>
            <ta e="T16" id="Seg_5087" s="T15">be.possible-NEG.[3SG]</ta>
            <ta e="T17" id="Seg_5088" s="T16">bad.[NOM]</ta>
            <ta e="T18" id="Seg_5089" s="T17">turn-NEG.CVB.SIM</ta>
            <ta e="T19" id="Seg_5090" s="T18">go-PTCP.FUT-2SG-ACC</ta>
            <ta e="T20" id="Seg_5091" s="T19">sin.[NOM]</ta>
            <ta e="T21" id="Seg_5092" s="T20">be-HAB.[3SG]</ta>
            <ta e="T22" id="Seg_5093" s="T21">that</ta>
            <ta e="T23" id="Seg_5094" s="T22">name-3SG.[NOM]</ta>
            <ta e="T24" id="Seg_5095" s="T23">sin.[NOM]</ta>
            <ta e="T25" id="Seg_5096" s="T24">woman.[NOM]</ta>
            <ta e="T26" id="Seg_5097" s="T25">though</ta>
            <ta e="T27" id="Seg_5098" s="T26">man.[NOM]</ta>
            <ta e="T28" id="Seg_5099" s="T27">human.being.[NOM]</ta>
            <ta e="T29" id="Seg_5100" s="T28">clothes-3SG-ACC</ta>
            <ta e="T30" id="Seg_5101" s="T29">step.on-PTCP.PRS.[NOM]</ta>
            <ta e="T31" id="Seg_5102" s="T30">also</ta>
            <ta e="T32" id="Seg_5103" s="T31">sin.[NOM]</ta>
            <ta e="T33" id="Seg_5104" s="T32">clean.[NOM]</ta>
            <ta e="T34" id="Seg_5105" s="T33">be-PTCP.FUT-3SG-ACC</ta>
            <ta e="T35" id="Seg_5106" s="T34">man.[NOM]</ta>
            <ta e="T36" id="Seg_5107" s="T35">human.being.[NOM]</ta>
            <ta e="T37" id="Seg_5108" s="T36">clean-ADVZ</ta>
            <ta e="T38" id="Seg_5109" s="T37">live-PTCP.FUT-3SG-ACC</ta>
            <ta e="T39" id="Seg_5110" s="T38">need.to</ta>
            <ta e="T40" id="Seg_5111" s="T39">woman.[NOM]</ta>
            <ta e="T41" id="Seg_5112" s="T40">dress-POSS</ta>
            <ta e="T42" id="Seg_5113" s="T41">NEG</ta>
            <ta e="T43" id="Seg_5114" s="T42">go-PTCP.PRS.[NOM]</ta>
            <ta e="T44" id="Seg_5115" s="T43">also</ta>
            <ta e="T45" id="Seg_5116" s="T44">sin.[NOM]</ta>
            <ta e="T46" id="Seg_5117" s="T45">before</ta>
            <ta e="T47" id="Seg_5118" s="T46">dress.[NOM]</ta>
            <ta e="T48" id="Seg_5119" s="T47">dress-PTCP.FUT-3SG-ACC</ta>
            <ta e="T49" id="Seg_5120" s="T48">need.to</ta>
            <ta e="T50" id="Seg_5121" s="T49">dress.[NOM]</ta>
            <ta e="T51" id="Seg_5122" s="T50">man.[NOM]</ta>
            <ta e="T52" id="Seg_5123" s="T51">human.being.[NOM]</ta>
            <ta e="T53" id="Seg_5124" s="T52">similar</ta>
            <ta e="T55" id="Seg_5125" s="T54">now</ta>
            <ta e="T56" id="Seg_5126" s="T55">every-INTNS-3PL.[NOM]</ta>
            <ta e="T57" id="Seg_5127" s="T56">trousers-PROPR-3PL</ta>
            <ta e="T58" id="Seg_5128" s="T57">that.[NOM]</ta>
            <ta e="T59" id="Seg_5129" s="T58">sin.[NOM]</ta>
            <ta e="T60" id="Seg_5130" s="T59">EMPH</ta>
            <ta e="T61" id="Seg_5131" s="T60">that-3SG-2SG.[NOM]</ta>
            <ta e="T68" id="Seg_5132" s="T66">eh</ta>
            <ta e="T69" id="Seg_5133" s="T68">well</ta>
            <ta e="T70" id="Seg_5134" s="T69">every-3SG.[NOM]</ta>
            <ta e="T71" id="Seg_5135" s="T70">man.[NOM]</ta>
            <ta e="T72" id="Seg_5136" s="T71">human.being-DAT/LOC</ta>
            <ta e="T73" id="Seg_5137" s="T72">like</ta>
            <ta e="T74" id="Seg_5138" s="T73">bad.[NOM]</ta>
            <ta e="T75" id="Seg_5139" s="T74">that</ta>
            <ta e="T76" id="Seg_5140" s="T75">woman.[NOM]</ta>
            <ta e="T77" id="Seg_5141" s="T76">woman.[NOM]</ta>
            <ta e="T78" id="Seg_5142" s="T77">similar</ta>
            <ta e="T79" id="Seg_5143" s="T78">be-PTCP.FUT-3SG-ACC</ta>
            <ta e="T80" id="Seg_5144" s="T79">need.to</ta>
            <ta e="T81" id="Seg_5145" s="T80">dress-PROPR.[NOM]</ta>
            <ta e="T82" id="Seg_5146" s="T81">kerchief-PROPR.[NOM]</ta>
            <ta e="T83" id="Seg_5147" s="T82">be-PTCP.FUT-3SG-ACC</ta>
            <ta e="T84" id="Seg_5148" s="T83">need.to</ta>
            <ta e="T85" id="Seg_5149" s="T84">cap-ACC</ta>
            <ta e="T86" id="Seg_5150" s="T85">EMPH</ta>
            <ta e="T87" id="Seg_5151" s="T86">dress-EP-NEG.CVB</ta>
            <ta e="T88" id="Seg_5152" s="T87">then</ta>
            <ta e="T89" id="Seg_5153" s="T88">dress-PTCP.FUT-DAT/LOC</ta>
            <ta e="T90" id="Seg_5154" s="T89">need.to</ta>
            <ta e="T723" id="Seg_5155" s="T90">cap-ACC</ta>
            <ta e="T92" id="Seg_5156" s="T91">that.[NOM]</ta>
            <ta e="T93" id="Seg_5157" s="T92">such-PL.[NOM]</ta>
            <ta e="T94" id="Seg_5158" s="T93">then</ta>
            <ta e="T95" id="Seg_5159" s="T94">husband-DAT/LOC</ta>
            <ta e="T96" id="Seg_5160" s="T95">go-PTCP.PRS-EP-2SG.[NOM]</ta>
            <ta e="T97" id="Seg_5161" s="T96">when</ta>
            <ta e="T98" id="Seg_5162" s="T97">hand.[NOM]</ta>
            <ta e="T99" id="Seg_5163" s="T98">beat-EP-RECP/COLL-PRS-3PL</ta>
            <ta e="T100" id="Seg_5164" s="T99">icon-DAT/LOC</ta>
            <ta e="T101" id="Seg_5165" s="T100">like.that</ta>
            <ta e="T102" id="Seg_5166" s="T101">give-PRS-3PL</ta>
            <ta e="T103" id="Seg_5167" s="T102">hand-3PL-ACC</ta>
            <ta e="T104" id="Seg_5168" s="T103">husband-DAT/LOC</ta>
            <ta e="T105" id="Seg_5169" s="T104">go-PTCP.PRS</ta>
            <ta e="T724" id="Seg_5170" s="T105">human.being.[NOM]</ta>
            <ta e="T107" id="Seg_5171" s="T106">hand.[NOM]</ta>
            <ta e="T108" id="Seg_5172" s="T107">give-RECP/COLL-PRS-3PL</ta>
            <ta e="T109" id="Seg_5173" s="T108">then</ta>
            <ta e="T110" id="Seg_5174" s="T109">hand-3PL-ACC</ta>
            <ta e="T111" id="Seg_5175" s="T110">give-TEMP-3PL</ta>
            <ta e="T112" id="Seg_5176" s="T111">woman.[NOM]</ta>
            <ta e="T113" id="Seg_5177" s="T112">become-PRS.[3SG]</ta>
            <ta e="T114" id="Seg_5178" s="T113">that</ta>
            <ta e="T725" id="Seg_5179" s="T114">human.being.[NOM]</ta>
            <ta e="T116" id="Seg_5180" s="T115">such</ta>
            <ta e="T117" id="Seg_5181" s="T116">custom-PL.[NOM]</ta>
            <ta e="T118" id="Seg_5182" s="T117">such</ta>
            <ta e="T119" id="Seg_5183" s="T118">AFFIRM</ta>
            <ta e="T120" id="Seg_5184" s="T119">power-DAT/LOC</ta>
            <ta e="T121" id="Seg_5185" s="T120">give-PRS-3PL</ta>
            <ta e="T122" id="Seg_5186" s="T121">EMPH</ta>
            <ta e="T123" id="Seg_5187" s="T122">human.being-ACC</ta>
            <ta e="T124" id="Seg_5188" s="T123">want.[IMP.2SG]</ta>
            <ta e="T125" id="Seg_5189" s="T124">and</ta>
            <ta e="T126" id="Seg_5190" s="T125">want-EP-NEG.[IMP.2SG]</ta>
            <ta e="T127" id="Seg_5191" s="T126">and</ta>
            <ta e="T128" id="Seg_5192" s="T127">give-PRS-3PL</ta>
            <ta e="T129" id="Seg_5193" s="T128">however</ta>
            <ta e="T130" id="Seg_5194" s="T129">one</ta>
            <ta e="T131" id="Seg_5195" s="T130">cry-CVB.SIM-cry-CVB.SIM</ta>
            <ta e="T132" id="Seg_5196" s="T131">go-CVB.SIM</ta>
            <ta e="T133" id="Seg_5197" s="T132">stand-FUT-2SG</ta>
            <ta e="T134" id="Seg_5198" s="T133">that-DAT/LOC</ta>
            <ta e="T135" id="Seg_5199" s="T134">that.[NOM]</ta>
            <ta e="T136" id="Seg_5200" s="T135">similar</ta>
            <ta e="T137" id="Seg_5201" s="T136">1SG.[NOM]</ta>
            <ta e="T138" id="Seg_5202" s="T137">go-CVB.SIM</ta>
            <ta e="T139" id="Seg_5203" s="T138">stand-PST2-EP-1SG</ta>
            <ta e="T140" id="Seg_5204" s="T139">husband-DAT/LOC</ta>
            <ta e="T141" id="Seg_5205" s="T140">cry-CVB.SIM-cry-CVB.SIM-1SG</ta>
            <ta e="T142" id="Seg_5206" s="T141">ten</ta>
            <ta e="T143" id="Seg_5207" s="T142">six</ta>
            <ta e="T144" id="Seg_5208" s="T143">year-PROPR-1SG-DAT/LOC</ta>
            <ta e="T145" id="Seg_5209" s="T144">well</ta>
            <ta e="T146" id="Seg_5210" s="T145">what.[NOM]</ta>
            <ta e="T147" id="Seg_5211" s="T146">human.being-3SG.[NOM]</ta>
            <ta e="T148" id="Seg_5212" s="T147">husband-DAT/LOC</ta>
            <ta e="T149" id="Seg_5213" s="T148">go-FUT.[3SG]=Q</ta>
            <ta e="T150" id="Seg_5214" s="T149">that</ta>
            <ta e="T151" id="Seg_5215" s="T150">ten</ta>
            <ta e="T152" id="Seg_5216" s="T151">six</ta>
            <ta e="T153" id="Seg_5217" s="T152">year-PROPR-DAT/LOC</ta>
            <ta e="T154" id="Seg_5218" s="T153">give-PST2-3PL</ta>
            <ta e="T157" id="Seg_5219" s="T156">at.all</ta>
            <ta e="T158" id="Seg_5220" s="T157">young.[NOM]</ta>
            <ta e="T159" id="Seg_5221" s="T158">clean.[NOM]</ta>
            <ta e="T160" id="Seg_5222" s="T159">be-PST1-1SG</ta>
            <ta e="T161" id="Seg_5223" s="T160">though</ta>
            <ta e="T162" id="Seg_5224" s="T161">period-POSS</ta>
            <ta e="T163" id="Seg_5225" s="T162">NEG</ta>
            <ta e="T164" id="Seg_5226" s="T163">NEG</ta>
            <ta e="T165" id="Seg_5227" s="T164">be-PST1-1SG</ta>
            <ta e="T166" id="Seg_5228" s="T165">then</ta>
            <ta e="T167" id="Seg_5229" s="T166">that</ta>
            <ta e="T168" id="Seg_5230" s="T167">go.out-PST1-3SG</ta>
            <ta e="T169" id="Seg_5231" s="T168">big</ta>
            <ta e="T170" id="Seg_5232" s="T169">daughter-EP-1SG.[NOM]</ta>
            <ta e="T171" id="Seg_5233" s="T170">now</ta>
            <ta e="T172" id="Seg_5234" s="T171">EMPH</ta>
            <ta e="T173" id="Seg_5235" s="T172">that</ta>
            <ta e="T174" id="Seg_5236" s="T173">that</ta>
            <ta e="T175" id="Seg_5237" s="T174">child-1SG.[NOM]</ta>
            <ta e="T176" id="Seg_5238" s="T175">ten</ta>
            <ta e="T177" id="Seg_5239" s="T176">seven</ta>
            <ta e="T178" id="Seg_5240" s="T177">ten</ta>
            <ta e="T179" id="Seg_5241" s="T178">eight</ta>
            <ta e="T180" id="Seg_5242" s="T179">year-PROPR-1SG-DAT/LOC</ta>
            <ta e="T181" id="Seg_5243" s="T180">be.born-PST2.[3SG]</ta>
            <ta e="T182" id="Seg_5244" s="T181">child.[NOM]</ta>
            <ta e="T183" id="Seg_5245" s="T182">that.[NOM]</ta>
            <ta e="T184" id="Seg_5246" s="T183">well</ta>
            <ta e="T185" id="Seg_5247" s="T184">then</ta>
            <ta e="T186" id="Seg_5248" s="T185">age-CVB.SEQ</ta>
            <ta e="T187" id="Seg_5249" s="T186">age-CVB.SEQ-1SG</ta>
            <ta e="T188" id="Seg_5250" s="T187">that.[NOM]</ta>
            <ta e="T189" id="Seg_5251" s="T188">later</ta>
            <ta e="T190" id="Seg_5252" s="T189">later</ta>
            <ta e="T191" id="Seg_5253" s="T190">then</ta>
            <ta e="T192" id="Seg_5254" s="T191">MOD</ta>
            <ta e="T194" id="Seg_5255" s="T193">child-VBZ-CVB.SEQ</ta>
            <ta e="T195" id="Seg_5256" s="T194">and</ta>
            <ta e="T196" id="Seg_5257" s="T195">child-VBZ-CVB.SEQ</ta>
            <ta e="T197" id="Seg_5258" s="T196">go-PRS-2SG</ta>
            <ta e="T198" id="Seg_5259" s="T197">EMPH</ta>
            <ta e="T203" id="Seg_5260" s="T202">very</ta>
            <ta e="T204" id="Seg_5261" s="T203">good</ta>
            <ta e="T205" id="Seg_5262" s="T204">human.being.[NOM]</ta>
            <ta e="T206" id="Seg_5263" s="T205">be-PST1-3SG</ta>
            <ta e="T207" id="Seg_5264" s="T206">husband-EP-1SG.[NOM]</ta>
            <ta e="T208" id="Seg_5265" s="T207">this</ta>
            <ta e="T209" id="Seg_5266" s="T208">sit-PTCP.HAB</ta>
            <ta e="T210" id="Seg_5267" s="T209">human.being.[NOM]</ta>
            <ta e="T726" id="Seg_5268" s="T210">that.[NOM]</ta>
            <ta e="T212" id="Seg_5269" s="T211">that</ta>
            <ta e="T213" id="Seg_5270" s="T212">1SG.[NOM]</ta>
            <ta e="T214" id="Seg_5271" s="T213">nine</ta>
            <ta e="T215" id="Seg_5272" s="T214">child-PROPR.[NOM]</ta>
            <ta e="T216" id="Seg_5273" s="T215">be-PST1-NEC-EP-1SG</ta>
            <ta e="T217" id="Seg_5274" s="T216">EMPH</ta>
            <ta e="T218" id="Seg_5275" s="T217">still</ta>
            <ta e="T219" id="Seg_5276" s="T218">still</ta>
            <ta e="T220" id="Seg_5277" s="T219">and</ta>
            <ta e="T221" id="Seg_5278" s="T220">young.[NOM]</ta>
            <ta e="T222" id="Seg_5279" s="T221">be-PST2.NEG-1SG</ta>
            <ta e="T223" id="Seg_5280" s="T222">there</ta>
            <ta e="T224" id="Seg_5281" s="T223">well</ta>
            <ta e="T225" id="Seg_5282" s="T224">such</ta>
            <ta e="T226" id="Seg_5283" s="T225">custom-PROPR.[NOM]</ta>
            <ta e="T227" id="Seg_5284" s="T226">be-PST1-3PL</ta>
            <ta e="T228" id="Seg_5285" s="T227">then</ta>
            <ta e="T229" id="Seg_5286" s="T228">however</ta>
            <ta e="T230" id="Seg_5287" s="T229">life.[NOM]</ta>
            <ta e="T231" id="Seg_5288" s="T230">live-PRS-2SG</ta>
            <ta e="T232" id="Seg_5289" s="T231">orphan-PL-ACC</ta>
            <ta e="T233" id="Seg_5290" s="T232">eat-CAUS-EP-IMP.2PL</ta>
            <ta e="T727" id="Seg_5291" s="T233">say-HAB-3PL</ta>
            <ta e="T235" id="Seg_5292" s="T234">orphan.[NOM]</ta>
            <ta e="T236" id="Seg_5293" s="T235">child-3PL.[NOM]</ta>
            <ta e="T237" id="Seg_5294" s="T236">sin.[NOM]</ta>
            <ta e="T238" id="Seg_5295" s="T237">be-FUT.[3SG]</ta>
            <ta e="T239" id="Seg_5296" s="T238">child-3PL.[NOM]</ta>
            <ta e="T240" id="Seg_5297" s="T239">eat-CAUS-EP-IMP.2PL</ta>
            <ta e="T241" id="Seg_5298" s="T240">say-HAB-3PL</ta>
            <ta e="T242" id="Seg_5299" s="T241">bad-ADVZ</ta>
            <ta e="T243" id="Seg_5300" s="T242">make-PTCP.PST</ta>
            <ta e="T244" id="Seg_5301" s="T243">human.being-ACC</ta>
            <ta e="T245" id="Seg_5302" s="T244">good-ADVZ</ta>
            <ta e="T246" id="Seg_5303" s="T245">accompany-EP-IMP.2PL</ta>
            <ta e="T247" id="Seg_5304" s="T246">say-HAB-3PL</ta>
            <ta e="T248" id="Seg_5305" s="T247">other.way.round</ta>
            <ta e="T249" id="Seg_5306" s="T248">why</ta>
            <ta e="T250" id="Seg_5307" s="T249">be-PRS.[3SG]</ta>
            <ta e="T251" id="Seg_5308" s="T250">say-TEMP-1SG</ta>
            <ta e="T252" id="Seg_5309" s="T251">sin.[NOM]</ta>
            <ta e="T253" id="Seg_5310" s="T252">be-FUT.[3SG]</ta>
            <ta e="T254" id="Seg_5311" s="T253">then</ta>
            <ta e="T255" id="Seg_5312" s="T254">self-2SG.[NOM]</ta>
            <ta e="T256" id="Seg_5313" s="T255">that</ta>
            <ta e="T257" id="Seg_5314" s="T256">good-ADVZ</ta>
            <ta e="T258" id="Seg_5315" s="T257">live-TEMP-2SG</ta>
            <ta e="T259" id="Seg_5316" s="T258">other.way.round</ta>
            <ta e="T260" id="Seg_5317" s="T259">self-DAT/LOC-DAT/LOC</ta>
            <ta e="T261" id="Seg_5318" s="T260">envy-FUT-3PL</ta>
            <ta e="T262" id="Seg_5319" s="T261">say-HAB-3PL</ta>
            <ta e="T263" id="Seg_5320" s="T262">ancestor-PL.[NOM]</ta>
            <ta e="T264" id="Seg_5321" s="T263">custom-PL.[NOM]</ta>
            <ta e="T265" id="Seg_5322" s="T264">such</ta>
            <ta e="T266" id="Seg_5323" s="T265">then</ta>
            <ta e="T267" id="Seg_5324" s="T266">happy.[NOM]</ta>
            <ta e="T268" id="Seg_5325" s="T267">be-PST1-2SG</ta>
            <ta e="T269" id="Seg_5326" s="T268">self-2SG.[NOM]</ta>
            <ta e="T270" id="Seg_5327" s="T269">later</ta>
            <ta e="T271" id="Seg_5328" s="T270">happy.[NOM]</ta>
            <ta e="T272" id="Seg_5329" s="T271">be-PST1-2SG</ta>
            <ta e="T273" id="Seg_5330" s="T272">say-PST1-3SG</ta>
            <ta e="T274" id="Seg_5331" s="T273">1SG.[NOM]</ta>
            <ta e="T275" id="Seg_5332" s="T274">now</ta>
            <ta e="T276" id="Seg_5333" s="T275">happy-1SG</ta>
            <ta e="T277" id="Seg_5334" s="T276">that</ta>
            <ta e="T278" id="Seg_5335" s="T277">orphan.[NOM]</ta>
            <ta e="T279" id="Seg_5336" s="T278">be-PST1-1SG</ta>
            <ta e="T280" id="Seg_5337" s="T279">EMPH</ta>
            <ta e="T281" id="Seg_5338" s="T280">then</ta>
            <ta e="T282" id="Seg_5339" s="T281">mother-1SG.[NOM]</ta>
            <ta e="T283" id="Seg_5340" s="T282">die-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T284" id="Seg_5341" s="T283">husband-DAT/LOC</ta>
            <ta e="T285" id="Seg_5342" s="T284">give-PTCP.PST-3SG-ACC</ta>
            <ta e="T286" id="Seg_5343" s="T285">after</ta>
            <ta e="T287" id="Seg_5344" s="T286">eh</ta>
            <ta e="T288" id="Seg_5345" s="T287">husband-DAT/LOC</ta>
            <ta e="T289" id="Seg_5346" s="T288">give-CVB.SIM=not.yet-3SG</ta>
            <ta e="T290" id="Seg_5347" s="T289">family-ABL</ta>
            <ta e="T291" id="Seg_5348" s="T290">family-DAT/LOC</ta>
            <ta e="T292" id="Seg_5349" s="T291">go-HAB-1PL</ta>
            <ta e="T293" id="Seg_5350" s="T292">there</ta>
            <ta e="T294" id="Seg_5351" s="T293">mother-1SG.[NOM]</ta>
            <ta e="T295" id="Seg_5352" s="T294">mother-1SG.[NOM]</ta>
            <ta e="T296" id="Seg_5353" s="T295">die-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T297" id="Seg_5354" s="T296">so</ta>
            <ta e="T298" id="Seg_5355" s="T297">say-PRS.[3SG]</ta>
            <ta e="T299" id="Seg_5356" s="T298">EMPH</ta>
            <ta e="T300" id="Seg_5357" s="T299">well</ta>
            <ta e="T301" id="Seg_5358" s="T300">orphan-PL-ACC</ta>
            <ta e="T302" id="Seg_5359" s="T301">eat-EP-CAUS-FUT.[IMP.2SG]</ta>
            <ta e="T303" id="Seg_5360" s="T302">EMPH</ta>
            <ta e="T304" id="Seg_5361" s="T303">bad-ADVZ</ta>
            <ta e="T305" id="Seg_5362" s="T304">make-PTCP.PST-ACC</ta>
            <ta e="T306" id="Seg_5363" s="T305">good-ADVZ</ta>
            <ta e="T307" id="Seg_5364" s="T306">make-PTCP.FUT-2SG-ACC</ta>
            <ta e="T308" id="Seg_5365" s="T307">need.to</ta>
            <ta e="T309" id="Seg_5366" s="T308">then</ta>
            <ta e="T310" id="Seg_5367" s="T309">long</ta>
            <ta e="T311" id="Seg_5368" s="T310">happy</ta>
            <ta e="T312" id="Seg_5369" s="T311">be-FUT-2SG</ta>
            <ta e="T313" id="Seg_5370" s="T312">many</ta>
            <ta e="T314" id="Seg_5371" s="T313">child-PROPR.[NOM]</ta>
            <ta e="T315" id="Seg_5372" s="T314">be-FUT-2SG</ta>
            <ta e="T316" id="Seg_5373" s="T315">ready</ta>
            <ta e="T317" id="Seg_5374" s="T316">life-DAT/LOC</ta>
            <ta e="T318" id="Seg_5375" s="T317">live-FUT-2SG</ta>
            <ta e="T319" id="Seg_5376" s="T318">say-PST2-3SG</ta>
            <ta e="T320" id="Seg_5377" s="T319">indeed</ta>
            <ta e="T321" id="Seg_5378" s="T320">see.[IMP.2SG]</ta>
            <ta e="T322" id="Seg_5379" s="T321">now</ta>
            <ta e="T323" id="Seg_5380" s="T322">1SG.[NOM]</ta>
            <ta e="T324" id="Seg_5381" s="T323">self-1SG.[NOM]</ta>
            <ta e="T325" id="Seg_5382" s="T324">though</ta>
            <ta e="T326" id="Seg_5383" s="T325">five-ten-ABL</ta>
            <ta e="T327" id="Seg_5384" s="T326">over</ta>
            <ta e="T328" id="Seg_5385" s="T327">human.being-1SG-ACC</ta>
            <ta e="T329" id="Seg_5386" s="T328">self-1SG.[NOM]</ta>
            <ta e="T330" id="Seg_5387" s="T329">inside-1SG-ABL</ta>
            <ta e="T331" id="Seg_5388" s="T330">go.out-EP-PST2-EP-1SG</ta>
            <ta e="T332" id="Seg_5389" s="T331">grandchild-PL-COM-1SG</ta>
            <ta e="T336" id="Seg_5390" s="T335">people-EP-1SG.[NOM]</ta>
            <ta e="T337" id="Seg_5391" s="T336">self-1SG.[NOM]</ta>
            <ta e="T338" id="Seg_5392" s="T337">people-EP-1SG.[NOM]</ta>
            <ta e="T339" id="Seg_5393" s="T338">Syndassko.[NOM]</ta>
            <ta e="T340" id="Seg_5394" s="T339">identical</ta>
            <ta e="T341" id="Seg_5395" s="T340">half-3SG.[NOM]</ta>
            <ta e="T342" id="Seg_5396" s="T341">half-3SG.[NOM]</ta>
            <ta e="T343" id="Seg_5397" s="T342">far.away</ta>
            <ta e="T344" id="Seg_5398" s="T343">there.is-3PL</ta>
            <ta e="T345" id="Seg_5399" s="T344">still</ta>
            <ta e="T346" id="Seg_5400" s="T345">that.[NOM]</ta>
            <ta e="T347" id="Seg_5401" s="T346">to</ta>
            <ta e="T348" id="Seg_5402" s="T347">here-ADJZ-1SG.[NOM]</ta>
            <ta e="T349" id="Seg_5403" s="T348">just</ta>
            <ta e="T350" id="Seg_5404" s="T349">well</ta>
            <ta e="T351" id="Seg_5405" s="T350">thank.god</ta>
            <ta e="T352" id="Seg_5406" s="T351">here</ta>
            <ta e="T353" id="Seg_5407" s="T352">just</ta>
            <ta e="T354" id="Seg_5408" s="T353">there.is</ta>
            <ta e="T355" id="Seg_5409" s="T354">five-ten-ABL</ta>
            <ta e="T356" id="Seg_5410" s="T355">over</ta>
            <ta e="T357" id="Seg_5411" s="T356">human.being-1PL.[NOM]</ta>
            <ta e="T358" id="Seg_5412" s="T357">child-PL-COM</ta>
            <ta e="T359" id="Seg_5413" s="T358">almost</ta>
            <ta e="T360" id="Seg_5414" s="T359">Syndassko.[NOM]</ta>
            <ta e="T728" id="Seg_5415" s="T361">split-CVB.SEQ</ta>
            <ta e="T363" id="Seg_5416" s="T362">Syndassko.[NOM]</ta>
            <ta e="T364" id="Seg_5417" s="T363">half-EP-1SG.[NOM]</ta>
            <ta e="T365" id="Seg_5418" s="T364">so</ta>
            <ta e="T366" id="Seg_5419" s="T365">and</ta>
            <ta e="T367" id="Seg_5420" s="T366">good-ADVZ</ta>
            <ta e="T368" id="Seg_5421" s="T367">live-PRS-1SG</ta>
            <ta e="T369" id="Seg_5422" s="T368">power-PROPR</ta>
            <ta e="T370" id="Seg_5423" s="T369">human.being-PL.[NOM]</ta>
            <ta e="T371" id="Seg_5424" s="T370">blessing-3PL-DAT/LOC</ta>
            <ta e="T372" id="Seg_5425" s="T371">human.being-PL.[NOM]</ta>
            <ta e="T373" id="Seg_5426" s="T372">blessing-3PL.[NOM]</ta>
            <ta e="T374" id="Seg_5427" s="T373">old-PL.[NOM]</ta>
            <ta e="T375" id="Seg_5428" s="T374">blessing-3PL.[NOM]</ta>
            <ta e="T376" id="Seg_5429" s="T375">now</ta>
            <ta e="T377" id="Seg_5430" s="T376">EMPH</ta>
            <ta e="T378" id="Seg_5431" s="T377">age-CVB.SEQ-1SG</ta>
            <ta e="T379" id="Seg_5432" s="T378">be.sick-PRS-1SG</ta>
            <ta e="T380" id="Seg_5433" s="T379">EMPH</ta>
            <ta e="T381" id="Seg_5434" s="T380">EMPH</ta>
            <ta e="T382" id="Seg_5435" s="T381">blood.pressure-PROPR-1SG</ta>
            <ta e="T383" id="Seg_5436" s="T382">very</ta>
            <ta e="T384" id="Seg_5437" s="T383">big</ta>
            <ta e="T386" id="Seg_5438" s="T384">two</ta>
            <ta e="T388" id="Seg_5439" s="T386">AFFIRM</ta>
            <ta e="T390" id="Seg_5440" s="T388">two</ta>
            <ta e="T391" id="Seg_5441" s="T390">hundred-ABL</ta>
            <ta e="T392" id="Seg_5442" s="T391">exceed-PRS.[3SG]</ta>
            <ta e="T395" id="Seg_5443" s="T394">be-PRS.[3SG]</ta>
            <ta e="T396" id="Seg_5444" s="T395">that-3SG-1SG-ACC</ta>
            <ta e="T397" id="Seg_5445" s="T396">make.it-NEG-1SG</ta>
            <ta e="T398" id="Seg_5446" s="T397">then</ta>
            <ta e="T399" id="Seg_5447" s="T398">leg-PL-EP-1SG.[NOM]</ta>
            <ta e="T400" id="Seg_5448" s="T399">block-PRS-3PL</ta>
            <ta e="T401" id="Seg_5449" s="T400">now</ta>
            <ta e="T402" id="Seg_5450" s="T401">one</ta>
            <ta e="T403" id="Seg_5451" s="T402">NEG</ta>
            <ta e="T404" id="Seg_5452" s="T403">child-DAT/LOC</ta>
            <ta e="T405" id="Seg_5453" s="T404">make.it-CVB.SEQ</ta>
            <ta e="T406" id="Seg_5454" s="T405">reach-NEG-1SG</ta>
            <ta e="T407" id="Seg_5455" s="T406">EMPH</ta>
            <ta e="T408" id="Seg_5456" s="T407">lie-PRS-1SG</ta>
            <ta e="T409" id="Seg_5457" s="T408">EMPH</ta>
            <ta e="T423" id="Seg_5458" s="T422">paradise-DAT/LOC</ta>
            <ta e="T424" id="Seg_5459" s="T423">go-FUT-1SG</ta>
            <ta e="T430" id="Seg_5460" s="T428">AFFIRM</ta>
            <ta e="T431" id="Seg_5461" s="T430">paradise-DAT/LOC</ta>
            <ta e="T432" id="Seg_5462" s="T431">go-FUT-1SG</ta>
            <ta e="T433" id="Seg_5463" s="T432">child-PL-EP-1SG.[NOM]</ta>
            <ta e="T434" id="Seg_5464" s="T433">many-3PL</ta>
            <ta e="T435" id="Seg_5465" s="T434">paradise-DAT/LOC</ta>
            <ta e="T436" id="Seg_5466" s="T435">fly-EP-CAUS-FUT-3PL</ta>
            <ta e="T440" id="Seg_5467" s="T439">god-DAT/LOC</ta>
            <ta e="T441" id="Seg_5468" s="T440">well</ta>
            <ta e="T442" id="Seg_5469" s="T441">god-DAT/LOC</ta>
            <ta e="T443" id="Seg_5470" s="T442">go-FUT-1SG</ta>
            <ta e="T452" id="Seg_5471" s="T451">say-HAB-3PL</ta>
            <ta e="T453" id="Seg_5472" s="T452">old-PL.[NOM]</ta>
            <ta e="T454" id="Seg_5473" s="T453">god-DAT/LOC</ta>
            <ta e="T455" id="Seg_5474" s="T454">go-FUT-2PL</ta>
            <ta e="T457" id="Seg_5475" s="T456">many</ta>
            <ta e="T458" id="Seg_5476" s="T457">child-PROPR</ta>
            <ta e="T459" id="Seg_5477" s="T458">human.being.[NOM]</ta>
            <ta e="T460" id="Seg_5478" s="T459">god-DAT/LOC</ta>
            <ta e="T461" id="Seg_5479" s="T460">go-FUT-1SG</ta>
            <ta e="T462" id="Seg_5480" s="T461">human.being-ACC</ta>
            <ta e="T463" id="Seg_5481" s="T462">bad-ADVZ</ta>
            <ta e="T464" id="Seg_5482" s="T463">speak-NEG-1SG</ta>
            <ta e="T465" id="Seg_5483" s="T464">human.being-ACC</ta>
            <ta e="T466" id="Seg_5484" s="T465">curse-EP-RECP/COLL-NEG-1SG</ta>
            <ta e="T467" id="Seg_5485" s="T466">sin.[NOM]</ta>
            <ta e="T468" id="Seg_5486" s="T467">be-FUT.[3SG]</ta>
            <ta e="T469" id="Seg_5487" s="T468">that.[NOM]</ta>
            <ta e="T470" id="Seg_5488" s="T469">because.of</ta>
            <ta e="T471" id="Seg_5489" s="T470">1SG.[NOM]</ta>
            <ta e="T472" id="Seg_5490" s="T471">human.being-ACC</ta>
            <ta e="T473" id="Seg_5491" s="T472">curse-EP-RECP/COLL-CVB.SIM</ta>
            <ta e="T474" id="Seg_5492" s="T473">can-PTCP.HAB-3SG</ta>
            <ta e="T475" id="Seg_5493" s="T474">NEG-1SG</ta>
            <ta e="T476" id="Seg_5494" s="T475">sin.[NOM]</ta>
            <ta e="T477" id="Seg_5495" s="T476">be-FUT.[3SG]</ta>
            <ta e="T478" id="Seg_5496" s="T477">who.[NOM]</ta>
            <ta e="T479" id="Seg_5497" s="T478">go.in-PST2.[3SG]</ta>
            <ta e="T480" id="Seg_5498" s="T479">eat-EP-CAUS-CVB.SEQ</ta>
            <ta e="T481" id="Seg_5499" s="T480">go-PRS-1SG</ta>
            <ta e="T482" id="Seg_5500" s="T481">body-EP-1SG.[NOM]</ta>
            <ta e="T483" id="Seg_5501" s="T482">make.it-EP-MED-PTCP.PRS</ta>
            <ta e="T484" id="Seg_5502" s="T483">be-TEMP-1SG</ta>
            <ta e="T485" id="Seg_5503" s="T484">now</ta>
            <ta e="T486" id="Seg_5504" s="T485">make.it-EP-MED-NEG-1SG</ta>
            <ta e="T487" id="Seg_5505" s="T486">Dunyasha-1SG.[NOM]</ta>
            <ta e="T488" id="Seg_5506" s="T487">just</ta>
            <ta e="T489" id="Seg_5507" s="T488">be-PST2.[3SG]</ta>
            <ta e="T491" id="Seg_5508" s="T490">good.[NOM]</ta>
            <ta e="T492" id="Seg_5509" s="T491">daughter_in_law-EP-1SG.[NOM]</ta>
            <ta e="T493" id="Seg_5510" s="T492">very</ta>
            <ta e="T494" id="Seg_5511" s="T493">good.[NOM]</ta>
            <ta e="T495" id="Seg_5512" s="T494">woman.[NOM]</ta>
            <ta e="T496" id="Seg_5513" s="T495">good.[NOM]</ta>
            <ta e="T497" id="Seg_5514" s="T496">woman-PL.[NOM]</ta>
            <ta e="T498" id="Seg_5515" s="T497">daughter_in_law-PL-EP-1SG.[NOM]</ta>
            <ta e="T499" id="Seg_5516" s="T498">so</ta>
            <ta e="T500" id="Seg_5517" s="T499">EMPH</ta>
            <ta e="T501" id="Seg_5518" s="T500">that</ta>
            <ta e="T502" id="Seg_5519" s="T501">how</ta>
            <ta e="T503" id="Seg_5520" s="T502">and</ta>
            <ta e="T504" id="Seg_5521" s="T503">be-IMP.3SG</ta>
            <ta e="T505" id="Seg_5522" s="T504">1SG.[NOM]</ta>
            <ta e="T506" id="Seg_5523" s="T505">worry-EP-NEG-1SG</ta>
            <ta e="T507" id="Seg_5524" s="T506">3PL-DAT/LOC</ta>
            <ta e="T508" id="Seg_5525" s="T507">check-RECP/COLL-IMP.3PL</ta>
            <ta e="T509" id="Seg_5526" s="T508">and</ta>
            <ta e="T510" id="Seg_5527" s="T509">check-RECP/COLL-EP-NEG-IMP.3PL</ta>
            <ta e="T511" id="Seg_5528" s="T510">and</ta>
            <ta e="T512" id="Seg_5529" s="T511">1SG-DAT/LOC</ta>
            <ta e="T513" id="Seg_5530" s="T512">need-POSS</ta>
            <ta e="T514" id="Seg_5531" s="T513">NEG.EX</ta>
            <ta e="T515" id="Seg_5532" s="T514">individually</ta>
            <ta e="T516" id="Seg_5533" s="T515">family-PL.[NOM]</ta>
            <ta e="T517" id="Seg_5534" s="T516">EMPH</ta>
            <ta e="T518" id="Seg_5535" s="T517">what-DAT/LOC</ta>
            <ta e="T519" id="Seg_5536" s="T518">worry-FUT-1SG=Q</ta>
            <ta e="T520" id="Seg_5537" s="T519">that-DAT/LOC</ta>
            <ta e="T521" id="Seg_5538" s="T520">1SG.[NOM]</ta>
            <ta e="T522" id="Seg_5539" s="T521">life-EP-1SG.[NOM]</ta>
            <ta e="T523" id="Seg_5540" s="T522">stop-PST2-3SG</ta>
            <ta e="T524" id="Seg_5541" s="T523">3PL.[NOM]</ta>
            <ta e="T525" id="Seg_5542" s="T524">life-3PL-ACC</ta>
            <ta e="T526" id="Seg_5543" s="T525">touch-NEG-1SG</ta>
            <ta e="T527" id="Seg_5544" s="T526">1SG.[NOM]</ta>
            <ta e="T528" id="Seg_5545" s="T527">live-IMP.3PL</ta>
            <ta e="T532" id="Seg_5546" s="T531">last-3SG.[NOM]</ta>
            <ta e="T533" id="Seg_5547" s="T532">then</ta>
            <ta e="T534" id="Seg_5548" s="T533">well</ta>
            <ta e="T535" id="Seg_5549" s="T534">what-ACC</ta>
            <ta e="T536" id="Seg_5550" s="T535">say-EP-CAUS-PRS-2SG</ta>
            <ta e="T552" id="Seg_5551" s="T550">AFFIRM</ta>
            <ta e="T571" id="Seg_5552" s="T570">hear-EP-NEG-3PL</ta>
            <ta e="T572" id="Seg_5553" s="T571">at.all</ta>
            <ta e="T573" id="Seg_5554" s="T572">that.[NOM]</ta>
            <ta e="T574" id="Seg_5555" s="T573">go-CVB.SEQ</ta>
            <ta e="T575" id="Seg_5556" s="T574">stay-PRS-3PL</ta>
            <ta e="T576" id="Seg_5557" s="T575">that</ta>
            <ta e="T577" id="Seg_5558" s="T576">this</ta>
            <ta e="T578" id="Seg_5559" s="T577">man-EP-1SG.[NOM]</ta>
            <ta e="T579" id="Seg_5560" s="T578">older.sister-3SG-GEN</ta>
            <ta e="T580" id="Seg_5561" s="T579">eh</ta>
            <ta e="T581" id="Seg_5562" s="T580">man-EP-1SG.[NOM]</ta>
            <ta e="T582" id="Seg_5563" s="T581">younger.sister-EP-3SG-GEN</ta>
            <ta e="T583" id="Seg_5564" s="T582">child-PL-3SG.[NOM]</ta>
            <ta e="T584" id="Seg_5565" s="T583">that</ta>
            <ta e="T585" id="Seg_5566" s="T584">go-HAB-3PL</ta>
            <ta e="T587" id="Seg_5567" s="T586">tomb-PL-DAT/LOC</ta>
            <ta e="T588" id="Seg_5568" s="T587">1SG.[NOM]</ta>
            <ta e="T589" id="Seg_5569" s="T588">child-PL-1SG-ACC</ta>
            <ta e="T590" id="Seg_5570" s="T589">with</ta>
            <ta e="T591" id="Seg_5571" s="T590">mother.[NOM]</ta>
            <ta e="T592" id="Seg_5572" s="T591">mother-3SG.[NOM]</ta>
            <ta e="T593" id="Seg_5573" s="T592">die-PTCP.PST-3SG-ACC</ta>
            <ta e="T594" id="Seg_5574" s="T593">after</ta>
            <ta e="T595" id="Seg_5575" s="T594">for.the.first.time</ta>
            <ta e="T596" id="Seg_5576" s="T595">come-PRS.[3SG]</ta>
            <ta e="T597" id="Seg_5577" s="T596">small</ta>
            <ta e="T598" id="Seg_5578" s="T597">son-3SG.[NOM]</ta>
            <ta e="T599" id="Seg_5579" s="T598">that-3SG-2SG.[NOM]</ta>
            <ta e="T600" id="Seg_5580" s="T599">go-PRS.[3SG]</ta>
            <ta e="T601" id="Seg_5581" s="T600">that-ACC</ta>
            <ta e="T602" id="Seg_5582" s="T601">accompany-RECP/COLL-PRS-3PL</ta>
            <ta e="T603" id="Seg_5583" s="T602">that</ta>
            <ta e="T604" id="Seg_5584" s="T603">that-ACC</ta>
            <ta e="T605" id="Seg_5585" s="T604">go-EP-NEG-IMP.2PL</ta>
            <ta e="T606" id="Seg_5586" s="T605">say-PRS-1SG</ta>
            <ta e="T607" id="Seg_5587" s="T606">hear-EP-NEG-3PL</ta>
            <ta e="T608" id="Seg_5588" s="T607">EMPH</ta>
            <ta e="T609" id="Seg_5589" s="T608">what-ACC</ta>
            <ta e="T610" id="Seg_5590" s="T609">make-CVB.SIM</ta>
            <ta e="T611" id="Seg_5591" s="T610">go-PTCP.PRS-3PL.[NOM]</ta>
            <ta e="T612" id="Seg_5592" s="T611">Q</ta>
            <ta e="T613" id="Seg_5593" s="T612">sin.[NOM]</ta>
            <ta e="T614" id="Seg_5594" s="T613">EMPH</ta>
            <ta e="T615" id="Seg_5595" s="T614">now</ta>
            <ta e="T616" id="Seg_5596" s="T615">human.being.[NOM]</ta>
            <ta e="T617" id="Seg_5597" s="T616">place.[NOM]</ta>
            <ta e="T618" id="Seg_5598" s="T617">every</ta>
            <ta e="T619" id="Seg_5599" s="T618">die-CVB.SIM</ta>
            <ta e="T620" id="Seg_5600" s="T619">lie-PRS.[3SG]</ta>
            <ta e="T621" id="Seg_5601" s="T620">human.being.[NOM]</ta>
            <ta e="T622" id="Seg_5602" s="T621">be.afraid-PTCP.FUT.[NOM]</ta>
            <ta e="T623" id="Seg_5603" s="T622">now</ta>
            <ta e="T624" id="Seg_5604" s="T623">there</ta>
            <ta e="T625" id="Seg_5605" s="T624">one</ta>
            <ta e="T626" id="Seg_5606" s="T625">old.woman.[NOM]</ta>
            <ta e="T627" id="Seg_5607" s="T626">die-PST1-3SG</ta>
            <ta e="T628" id="Seg_5608" s="T627">now</ta>
            <ta e="T629" id="Seg_5609" s="T628">one</ta>
            <ta e="T630" id="Seg_5610" s="T629">child.[NOM]</ta>
            <ta e="T631" id="Seg_5611" s="T630">come-CVB.SEQ</ta>
            <ta e="T632" id="Seg_5612" s="T631">go-CVB.SEQ</ta>
            <ta e="T633" id="Seg_5613" s="T632">die-PST2.[3SG]</ta>
            <ta e="T634" id="Seg_5614" s="T633">also</ta>
            <ta e="T635" id="Seg_5615" s="T634">dear-PROPR</ta>
            <ta e="T636" id="Seg_5616" s="T635">child-1PL.[NOM]</ta>
            <ta e="T637" id="Seg_5617" s="T636">very</ta>
            <ta e="T638" id="Seg_5618" s="T637">good.[NOM]</ta>
            <ta e="T639" id="Seg_5619" s="T638">child.[NOM]</ta>
            <ta e="T640" id="Seg_5620" s="T639">be-PST1-3SG</ta>
            <ta e="T641" id="Seg_5621" s="T640">snow.scooter-EP-INSTR</ta>
            <ta e="T642" id="Seg_5622" s="T641">pull-VBZ-CVB.SIM</ta>
            <ta e="T643" id="Seg_5623" s="T642">go-PTCP.PRS</ta>
            <ta e="T644" id="Seg_5624" s="T643">be-PST1-3SG</ta>
            <ta e="T662" id="Seg_5625" s="T661">AFFIRM</ta>
            <ta e="T663" id="Seg_5626" s="T662">this</ta>
            <ta e="T664" id="Seg_5627" s="T663">tobacco-PART</ta>
            <ta e="T665" id="Seg_5628" s="T664">throw-PTCP.FUT-2SG-ACC</ta>
            <ta e="T666" id="Seg_5629" s="T665">need.to</ta>
            <ta e="T670" id="Seg_5630" s="T668">spirit.[NOM]</ta>
            <ta e="T671" id="Seg_5631" s="T670">drink-TEMP-3PL</ta>
            <ta e="T672" id="Seg_5632" s="T671">spirit-PART</ta>
            <ta e="T673" id="Seg_5633" s="T672">pour-PTCP.FUT-2SG-ACC</ta>
            <ta e="T674" id="Seg_5634" s="T673">need.to</ta>
            <ta e="T676" id="Seg_5635" s="T675">last-3SG.[NOM]</ta>
            <ta e="T688" id="Seg_5636" s="T687">seldomly</ta>
            <ta e="T689" id="Seg_5637" s="T688">seldomly</ta>
            <ta e="T690" id="Seg_5638" s="T689">go-PRS-3PL</ta>
            <ta e="T691" id="Seg_5639" s="T690">father-3PL-DAT/LOC</ta>
            <ta e="T692" id="Seg_5640" s="T691">tomb-PL-DAT/LOC</ta>
            <ta e="T693" id="Seg_5641" s="T692">mother-PL-EP-1SG.[NOM]</ta>
            <ta e="T694" id="Seg_5642" s="T693">tomb-3PL-DAT/LOC</ta>
            <ta e="T695" id="Seg_5643" s="T694">go-HAB-3PL</ta>
            <ta e="T696" id="Seg_5644" s="T695">so</ta>
            <ta e="T697" id="Seg_5645" s="T696">EMPH</ta>
            <ta e="T698" id="Seg_5646" s="T697">such</ta>
            <ta e="T699" id="Seg_5647" s="T698">there.is</ta>
            <ta e="T700" id="Seg_5648" s="T699">shaman-PL.[NOM]</ta>
            <ta e="T701" id="Seg_5649" s="T700">tomb-3PL.[NOM]</ta>
            <ta e="T702" id="Seg_5650" s="T701">there.is-3PL</ta>
            <ta e="T703" id="Seg_5651" s="T702">that</ta>
            <ta e="T704" id="Seg_5652" s="T703">bad.[NOM]</ta>
            <ta e="T705" id="Seg_5653" s="T704">EMPH</ta>
            <ta e="T706" id="Seg_5654" s="T705">shaman-PL-EP-2SG.[NOM]</ta>
            <ta e="T707" id="Seg_5655" s="T706">sin.[NOM]</ta>
            <ta e="T708" id="Seg_5656" s="T707">be-HAB.[3SG]</ta>
            <ta e="T709" id="Seg_5657" s="T708">hear-EP-NEG-3PL</ta>
            <ta e="T710" id="Seg_5658" s="T709">now</ta>
            <ta e="T711" id="Seg_5659" s="T710">youth-PL.[NOM]</ta>
            <ta e="T712" id="Seg_5660" s="T711">at.all</ta>
            <ta e="T716" id="Seg_5661" s="T715">at.all</ta>
            <ta e="T717" id="Seg_5662" s="T716">different</ta>
            <ta e="T719" id="Seg_5663" s="T717">people.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg-KiPP">
            <ta e="T2" id="Seg_5664" s="T1">was-ABL</ta>
            <ta e="T3" id="Seg_5665" s="T2">Sünde.[NOM]</ta>
            <ta e="T4" id="Seg_5666" s="T3">sagen-PRS-2SG</ta>
            <ta e="T0" id="Seg_5667" s="T4">Q</ta>
            <ta e="T6" id="Seg_5668" s="T5">Sünde.[NOM]</ta>
            <ta e="T7" id="Seg_5669" s="T6">aber</ta>
            <ta e="T8" id="Seg_5670" s="T7">vor.langer.Zeit</ta>
            <ta e="T9" id="Seg_5671" s="T8">Schamane-PL.[NOM]</ta>
            <ta e="T10" id="Seg_5672" s="T9">es.gibt</ta>
            <ta e="T11" id="Seg_5673" s="T10">sein-HAB-3PL</ta>
            <ta e="T12" id="Seg_5674" s="T11">Schamane-PL.[NOM]</ta>
            <ta e="T13" id="Seg_5675" s="T12">solch-PL.[NOM]</ta>
            <ta e="T14" id="Seg_5676" s="T13">Zelt-3PL-ACC</ta>
            <ta e="T15" id="Seg_5677" s="T14">sich.drehen-PTCP.PRS-DAT/LOC</ta>
            <ta e="T16" id="Seg_5678" s="T15">möglich.sein-NEG.[3SG]</ta>
            <ta e="T17" id="Seg_5679" s="T16">schlecht.[NOM]</ta>
            <ta e="T18" id="Seg_5680" s="T17">sich.drehen-NEG.CVB.SIM</ta>
            <ta e="T19" id="Seg_5681" s="T18">gehen-PTCP.FUT-2SG-ACC</ta>
            <ta e="T20" id="Seg_5682" s="T19">Sünde.[NOM]</ta>
            <ta e="T21" id="Seg_5683" s="T20">sein-HAB.[3SG]</ta>
            <ta e="T22" id="Seg_5684" s="T21">jenes</ta>
            <ta e="T23" id="Seg_5685" s="T22">Name-3SG.[NOM]</ta>
            <ta e="T24" id="Seg_5686" s="T23">Sünde.[NOM]</ta>
            <ta e="T25" id="Seg_5687" s="T24">Frau.[NOM]</ta>
            <ta e="T26" id="Seg_5688" s="T25">aber</ta>
            <ta e="T27" id="Seg_5689" s="T26">Mann.[NOM]</ta>
            <ta e="T28" id="Seg_5690" s="T27">Mensch.[NOM]</ta>
            <ta e="T29" id="Seg_5691" s="T28">Kleidung-3SG-ACC</ta>
            <ta e="T30" id="Seg_5692" s="T29">treten.auf-PTCP.PRS.[NOM]</ta>
            <ta e="T31" id="Seg_5693" s="T30">auch</ta>
            <ta e="T32" id="Seg_5694" s="T31">Sünde.[NOM]</ta>
            <ta e="T33" id="Seg_5695" s="T32">sauber.[NOM]</ta>
            <ta e="T34" id="Seg_5696" s="T33">sein-PTCP.FUT-3SG-ACC</ta>
            <ta e="T35" id="Seg_5697" s="T34">Mann.[NOM]</ta>
            <ta e="T36" id="Seg_5698" s="T35">Mensch.[NOM]</ta>
            <ta e="T37" id="Seg_5699" s="T36">sauber-ADVZ</ta>
            <ta e="T38" id="Seg_5700" s="T37">leben-PTCP.FUT-3SG-ACC</ta>
            <ta e="T39" id="Seg_5701" s="T38">man.muss</ta>
            <ta e="T40" id="Seg_5702" s="T39">Frau.[NOM]</ta>
            <ta e="T41" id="Seg_5703" s="T40">Kleid-POSS</ta>
            <ta e="T42" id="Seg_5704" s="T41">NEG</ta>
            <ta e="T43" id="Seg_5705" s="T42">gehen-PTCP.PRS.[NOM]</ta>
            <ta e="T44" id="Seg_5706" s="T43">auch</ta>
            <ta e="T45" id="Seg_5707" s="T44">Sünde.[NOM]</ta>
            <ta e="T46" id="Seg_5708" s="T45">früher</ta>
            <ta e="T47" id="Seg_5709" s="T46">Kleid.[NOM]</ta>
            <ta e="T48" id="Seg_5710" s="T47">anziehen-PTCP.FUT-3SG-ACC</ta>
            <ta e="T49" id="Seg_5711" s="T48">man.muss</ta>
            <ta e="T50" id="Seg_5712" s="T49">Kleid.[NOM]</ta>
            <ta e="T51" id="Seg_5713" s="T50">Mann.[NOM]</ta>
            <ta e="T52" id="Seg_5714" s="T51">Mensch.[NOM]</ta>
            <ta e="T53" id="Seg_5715" s="T52">ähnlich</ta>
            <ta e="T55" id="Seg_5716" s="T54">jetzt</ta>
            <ta e="T56" id="Seg_5717" s="T55">jeder-INTNS-3PL.[NOM]</ta>
            <ta e="T57" id="Seg_5718" s="T56">Hose-PROPR-3PL</ta>
            <ta e="T58" id="Seg_5719" s="T57">dieses.[NOM]</ta>
            <ta e="T59" id="Seg_5720" s="T58">Sünde.[NOM]</ta>
            <ta e="T60" id="Seg_5721" s="T59">EMPH</ta>
            <ta e="T61" id="Seg_5722" s="T60">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T68" id="Seg_5723" s="T66">äh</ta>
            <ta e="T69" id="Seg_5724" s="T68">doch</ta>
            <ta e="T70" id="Seg_5725" s="T69">jeder-3SG.[NOM]</ta>
            <ta e="T71" id="Seg_5726" s="T70">Mann.[NOM]</ta>
            <ta e="T72" id="Seg_5727" s="T71">Mensch-DAT/LOC</ta>
            <ta e="T73" id="Seg_5728" s="T72">wie</ta>
            <ta e="T74" id="Seg_5729" s="T73">schlecht.[NOM]</ta>
            <ta e="T75" id="Seg_5730" s="T74">jenes</ta>
            <ta e="T76" id="Seg_5731" s="T75">Frau.[NOM]</ta>
            <ta e="T77" id="Seg_5732" s="T76">Frau.[NOM]</ta>
            <ta e="T78" id="Seg_5733" s="T77">ähnlich</ta>
            <ta e="T79" id="Seg_5734" s="T78">sein-PTCP.FUT-3SG-ACC</ta>
            <ta e="T80" id="Seg_5735" s="T79">man.muss</ta>
            <ta e="T81" id="Seg_5736" s="T80">Kleid-PROPR.[NOM]</ta>
            <ta e="T82" id="Seg_5737" s="T81">Tuch-PROPR.[NOM]</ta>
            <ta e="T83" id="Seg_5738" s="T82">sein-PTCP.FUT-3SG-ACC</ta>
            <ta e="T84" id="Seg_5739" s="T83">man.muss</ta>
            <ta e="T85" id="Seg_5740" s="T84">Mütze-ACC</ta>
            <ta e="T86" id="Seg_5741" s="T85">EMPH</ta>
            <ta e="T87" id="Seg_5742" s="T86">anziehen-EP-NEG.CVB</ta>
            <ta e="T88" id="Seg_5743" s="T87">dann</ta>
            <ta e="T89" id="Seg_5744" s="T88">anziehen-PTCP.FUT-DAT/LOC</ta>
            <ta e="T90" id="Seg_5745" s="T89">man.muss</ta>
            <ta e="T723" id="Seg_5746" s="T90">Mütze-ACC</ta>
            <ta e="T92" id="Seg_5747" s="T91">dieses.[NOM]</ta>
            <ta e="T93" id="Seg_5748" s="T92">solch-PL.[NOM]</ta>
            <ta e="T94" id="Seg_5749" s="T93">dann</ta>
            <ta e="T95" id="Seg_5750" s="T94">Ehemann-DAT/LOC</ta>
            <ta e="T96" id="Seg_5751" s="T95">gehen-PTCP.PRS-EP-2SG.[NOM]</ta>
            <ta e="T97" id="Seg_5752" s="T96">als</ta>
            <ta e="T98" id="Seg_5753" s="T97">Hand.[NOM]</ta>
            <ta e="T99" id="Seg_5754" s="T98">schlagen-EP-RECP/COLL-PRS-3PL</ta>
            <ta e="T100" id="Seg_5755" s="T99">Ikone-DAT/LOC</ta>
            <ta e="T101" id="Seg_5756" s="T100">so</ta>
            <ta e="T102" id="Seg_5757" s="T101">geben-PRS-3PL</ta>
            <ta e="T103" id="Seg_5758" s="T102">Hand-3PL-ACC</ta>
            <ta e="T104" id="Seg_5759" s="T103">Ehemann-DAT/LOC</ta>
            <ta e="T105" id="Seg_5760" s="T104">gehen-PTCP.PRS</ta>
            <ta e="T724" id="Seg_5761" s="T105">Mensch.[NOM]</ta>
            <ta e="T107" id="Seg_5762" s="T106">Hand.[NOM]</ta>
            <ta e="T108" id="Seg_5763" s="T107">geben-RECP/COLL-PRS-3PL</ta>
            <ta e="T109" id="Seg_5764" s="T108">dann</ta>
            <ta e="T110" id="Seg_5765" s="T109">Hand-3PL-ACC</ta>
            <ta e="T111" id="Seg_5766" s="T110">geben-TEMP-3PL</ta>
            <ta e="T112" id="Seg_5767" s="T111">Frau.[NOM]</ta>
            <ta e="T113" id="Seg_5768" s="T112">werden-PRS.[3SG]</ta>
            <ta e="T114" id="Seg_5769" s="T113">jenes</ta>
            <ta e="T725" id="Seg_5770" s="T114">Mensch.[NOM]</ta>
            <ta e="T116" id="Seg_5771" s="T115">solch</ta>
            <ta e="T117" id="Seg_5772" s="T116">Gebrauch-PL.[NOM]</ta>
            <ta e="T118" id="Seg_5773" s="T117">solch</ta>
            <ta e="T119" id="Seg_5774" s="T118">AFFIRM</ta>
            <ta e="T120" id="Seg_5775" s="T119">Kraft-DAT/LOC</ta>
            <ta e="T121" id="Seg_5776" s="T120">geben-PRS-3PL</ta>
            <ta e="T122" id="Seg_5777" s="T121">EMPH</ta>
            <ta e="T123" id="Seg_5778" s="T122">Mensch-ACC</ta>
            <ta e="T124" id="Seg_5779" s="T123">wollen.[IMP.2SG]</ta>
            <ta e="T125" id="Seg_5780" s="T124">und</ta>
            <ta e="T126" id="Seg_5781" s="T125">wollen-EP-NEG.[IMP.2SG]</ta>
            <ta e="T127" id="Seg_5782" s="T126">und</ta>
            <ta e="T128" id="Seg_5783" s="T127">geben-PRS-3PL</ta>
            <ta e="T129" id="Seg_5784" s="T128">doch</ta>
            <ta e="T130" id="Seg_5785" s="T129">eins</ta>
            <ta e="T131" id="Seg_5786" s="T130">weinen-CVB.SIM-weinen-CVB.SIM</ta>
            <ta e="T132" id="Seg_5787" s="T131">gehen-CVB.SIM</ta>
            <ta e="T133" id="Seg_5788" s="T132">stehen-FUT-2SG</ta>
            <ta e="T134" id="Seg_5789" s="T133">jenes-DAT/LOC</ta>
            <ta e="T135" id="Seg_5790" s="T134">jenes.[NOM]</ta>
            <ta e="T136" id="Seg_5791" s="T135">ähnlich</ta>
            <ta e="T137" id="Seg_5792" s="T136">1SG.[NOM]</ta>
            <ta e="T138" id="Seg_5793" s="T137">gehen-CVB.SIM</ta>
            <ta e="T139" id="Seg_5794" s="T138">stehen-PST2-EP-1SG</ta>
            <ta e="T140" id="Seg_5795" s="T139">Ehemann-DAT/LOC</ta>
            <ta e="T141" id="Seg_5796" s="T140">weinen-CVB.SIM-weinen-CVB.SIM-1SG</ta>
            <ta e="T142" id="Seg_5797" s="T141">zehn</ta>
            <ta e="T143" id="Seg_5798" s="T142">sechs</ta>
            <ta e="T144" id="Seg_5799" s="T143">Jahr-PROPR-1SG-DAT/LOC</ta>
            <ta e="T145" id="Seg_5800" s="T144">nun</ta>
            <ta e="T146" id="Seg_5801" s="T145">was.[NOM]</ta>
            <ta e="T147" id="Seg_5802" s="T146">Mensch-3SG.[NOM]</ta>
            <ta e="T148" id="Seg_5803" s="T147">Ehemann-DAT/LOC</ta>
            <ta e="T149" id="Seg_5804" s="T148">gehen-FUT.[3SG]=Q</ta>
            <ta e="T150" id="Seg_5805" s="T149">jenes</ta>
            <ta e="T151" id="Seg_5806" s="T150">zehn</ta>
            <ta e="T152" id="Seg_5807" s="T151">sechs</ta>
            <ta e="T153" id="Seg_5808" s="T152">Jahr-PROPR-DAT/LOC</ta>
            <ta e="T154" id="Seg_5809" s="T153">geben-PST2-3PL</ta>
            <ta e="T157" id="Seg_5810" s="T156">ganz</ta>
            <ta e="T158" id="Seg_5811" s="T157">jung.[NOM]</ta>
            <ta e="T159" id="Seg_5812" s="T158">sauber.[NOM]</ta>
            <ta e="T160" id="Seg_5813" s="T159">sein-PST1-1SG</ta>
            <ta e="T161" id="Seg_5814" s="T160">wenn.auch</ta>
            <ta e="T162" id="Seg_5815" s="T161">Regel-POSS</ta>
            <ta e="T163" id="Seg_5816" s="T162">NEG</ta>
            <ta e="T164" id="Seg_5817" s="T163">NEG</ta>
            <ta e="T165" id="Seg_5818" s="T164">sein-PST1-1SG</ta>
            <ta e="T166" id="Seg_5819" s="T165">dann</ta>
            <ta e="T167" id="Seg_5820" s="T166">dieses</ta>
            <ta e="T168" id="Seg_5821" s="T167">hinausgehen-PST1-3SG</ta>
            <ta e="T169" id="Seg_5822" s="T168">groß</ta>
            <ta e="T170" id="Seg_5823" s="T169">Tochter-EP-1SG.[NOM]</ta>
            <ta e="T171" id="Seg_5824" s="T170">jetzt</ta>
            <ta e="T172" id="Seg_5825" s="T171">EMPH</ta>
            <ta e="T173" id="Seg_5826" s="T172">dieses</ta>
            <ta e="T174" id="Seg_5827" s="T173">dieses</ta>
            <ta e="T175" id="Seg_5828" s="T174">Kind-1SG.[NOM]</ta>
            <ta e="T176" id="Seg_5829" s="T175">zehn</ta>
            <ta e="T177" id="Seg_5830" s="T176">sieben</ta>
            <ta e="T178" id="Seg_5831" s="T177">zehn</ta>
            <ta e="T179" id="Seg_5832" s="T178">acht</ta>
            <ta e="T180" id="Seg_5833" s="T179">Jahr-PROPR-1SG-DAT/LOC</ta>
            <ta e="T181" id="Seg_5834" s="T180">geboren.werden-PST2.[3SG]</ta>
            <ta e="T182" id="Seg_5835" s="T181">Kind.[NOM]</ta>
            <ta e="T183" id="Seg_5836" s="T182">dieses.[NOM]</ta>
            <ta e="T184" id="Seg_5837" s="T183">doch</ta>
            <ta e="T185" id="Seg_5838" s="T184">dann</ta>
            <ta e="T186" id="Seg_5839" s="T185">altern-CVB.SEQ</ta>
            <ta e="T187" id="Seg_5840" s="T186">altern-CVB.SEQ-1SG</ta>
            <ta e="T188" id="Seg_5841" s="T187">dieses.[NOM]</ta>
            <ta e="T189" id="Seg_5842" s="T188">später</ta>
            <ta e="T190" id="Seg_5843" s="T189">später</ta>
            <ta e="T191" id="Seg_5844" s="T190">dann</ta>
            <ta e="T192" id="Seg_5845" s="T191">MOD</ta>
            <ta e="T194" id="Seg_5846" s="T193">Kind-VBZ-CVB.SEQ</ta>
            <ta e="T195" id="Seg_5847" s="T194">und</ta>
            <ta e="T196" id="Seg_5848" s="T195">Kind-VBZ-CVB.SEQ</ta>
            <ta e="T197" id="Seg_5849" s="T196">gehen-PRS-2SG</ta>
            <ta e="T198" id="Seg_5850" s="T197">EMPH</ta>
            <ta e="T203" id="Seg_5851" s="T202">sehr</ta>
            <ta e="T204" id="Seg_5852" s="T203">gut</ta>
            <ta e="T205" id="Seg_5853" s="T204">Mensch.[NOM]</ta>
            <ta e="T206" id="Seg_5854" s="T205">sein-PST1-3SG</ta>
            <ta e="T207" id="Seg_5855" s="T206">Ehemann-EP-1SG.[NOM]</ta>
            <ta e="T208" id="Seg_5856" s="T207">dieses</ta>
            <ta e="T209" id="Seg_5857" s="T208">sitzen-PTCP.HAB</ta>
            <ta e="T210" id="Seg_5858" s="T209">Mensch.[NOM]</ta>
            <ta e="T726" id="Seg_5859" s="T210">dieses.[NOM]</ta>
            <ta e="T212" id="Seg_5860" s="T211">dieses</ta>
            <ta e="T213" id="Seg_5861" s="T212">1SG.[NOM]</ta>
            <ta e="T214" id="Seg_5862" s="T213">neun</ta>
            <ta e="T215" id="Seg_5863" s="T214">Kind-PROPR.[NOM]</ta>
            <ta e="T216" id="Seg_5864" s="T215">sein-PST1-NEC-EP-1SG</ta>
            <ta e="T217" id="Seg_5865" s="T216">EMPH</ta>
            <ta e="T218" id="Seg_5866" s="T217">noch</ta>
            <ta e="T219" id="Seg_5867" s="T218">noch</ta>
            <ta e="T220" id="Seg_5868" s="T219">und</ta>
            <ta e="T221" id="Seg_5869" s="T220">jung.[NOM]</ta>
            <ta e="T222" id="Seg_5870" s="T221">sein-PST2.NEG-1SG</ta>
            <ta e="T223" id="Seg_5871" s="T222">dort</ta>
            <ta e="T224" id="Seg_5872" s="T223">doch</ta>
            <ta e="T225" id="Seg_5873" s="T224">solch</ta>
            <ta e="T226" id="Seg_5874" s="T225">Gebrauch-PROPR.[NOM]</ta>
            <ta e="T227" id="Seg_5875" s="T226">sein-PST1-3PL</ta>
            <ta e="T228" id="Seg_5876" s="T227">dann</ta>
            <ta e="T229" id="Seg_5877" s="T228">doch</ta>
            <ta e="T230" id="Seg_5878" s="T229">Leben.[NOM]</ta>
            <ta e="T231" id="Seg_5879" s="T230">leben-PRS-2SG</ta>
            <ta e="T232" id="Seg_5880" s="T231">Waise-PL-ACC</ta>
            <ta e="T233" id="Seg_5881" s="T232">essen-CAUS-EP-IMP.2PL</ta>
            <ta e="T727" id="Seg_5882" s="T233">sagen-HAB-3PL</ta>
            <ta e="T235" id="Seg_5883" s="T234">Waise.[NOM]</ta>
            <ta e="T236" id="Seg_5884" s="T235">Kind-3PL.[NOM]</ta>
            <ta e="T237" id="Seg_5885" s="T236">Sünde.[NOM]</ta>
            <ta e="T238" id="Seg_5886" s="T237">sein-FUT.[3SG]</ta>
            <ta e="T239" id="Seg_5887" s="T238">Kind-3PL.[NOM]</ta>
            <ta e="T240" id="Seg_5888" s="T239">essen-CAUS-EP-IMP.2PL</ta>
            <ta e="T241" id="Seg_5889" s="T240">sagen-HAB-3PL</ta>
            <ta e="T242" id="Seg_5890" s="T241">schlecht-ADVZ</ta>
            <ta e="T243" id="Seg_5891" s="T242">machen-PTCP.PST</ta>
            <ta e="T244" id="Seg_5892" s="T243">Mensch-ACC</ta>
            <ta e="T245" id="Seg_5893" s="T244">gut-ADVZ</ta>
            <ta e="T246" id="Seg_5894" s="T245">begleiten-EP-IMP.2PL</ta>
            <ta e="T247" id="Seg_5895" s="T246">sagen-HAB-3PL</ta>
            <ta e="T248" id="Seg_5896" s="T247">umgekehrt</ta>
            <ta e="T249" id="Seg_5897" s="T248">warum</ta>
            <ta e="T250" id="Seg_5898" s="T249">sein-PRS.[3SG]</ta>
            <ta e="T251" id="Seg_5899" s="T250">sagen-TEMP-1SG</ta>
            <ta e="T252" id="Seg_5900" s="T251">Sünde.[NOM]</ta>
            <ta e="T253" id="Seg_5901" s="T252">sein-FUT.[3SG]</ta>
            <ta e="T254" id="Seg_5902" s="T253">dann</ta>
            <ta e="T255" id="Seg_5903" s="T254">selbst-2SG.[NOM]</ta>
            <ta e="T256" id="Seg_5904" s="T255">jenes</ta>
            <ta e="T257" id="Seg_5905" s="T256">gut-ADVZ</ta>
            <ta e="T258" id="Seg_5906" s="T257">leben-TEMP-2SG</ta>
            <ta e="T259" id="Seg_5907" s="T258">umgekehrt</ta>
            <ta e="T260" id="Seg_5908" s="T259">selbst-DAT/LOC-DAT/LOC</ta>
            <ta e="T261" id="Seg_5909" s="T260">beneiden-FUT-3PL</ta>
            <ta e="T262" id="Seg_5910" s="T261">sagen-HAB-3PL</ta>
            <ta e="T263" id="Seg_5911" s="T262">Vorfahre-PL.[NOM]</ta>
            <ta e="T264" id="Seg_5912" s="T263">Gebrauch-PL.[NOM]</ta>
            <ta e="T265" id="Seg_5913" s="T264">solch</ta>
            <ta e="T266" id="Seg_5914" s="T265">dann</ta>
            <ta e="T267" id="Seg_5915" s="T266">glücklich.[NOM]</ta>
            <ta e="T268" id="Seg_5916" s="T267">sein-PST1-2SG</ta>
            <ta e="T269" id="Seg_5917" s="T268">selbst-2SG.[NOM]</ta>
            <ta e="T270" id="Seg_5918" s="T269">später</ta>
            <ta e="T271" id="Seg_5919" s="T270">glücklich.[NOM]</ta>
            <ta e="T272" id="Seg_5920" s="T271">sein-PST1-2SG</ta>
            <ta e="T273" id="Seg_5921" s="T272">sagen-PST1-3SG</ta>
            <ta e="T274" id="Seg_5922" s="T273">1SG.[NOM]</ta>
            <ta e="T275" id="Seg_5923" s="T274">jetzt</ta>
            <ta e="T276" id="Seg_5924" s="T275">glücklich-1SG</ta>
            <ta e="T277" id="Seg_5925" s="T276">jenes</ta>
            <ta e="T278" id="Seg_5926" s="T277">Waise.[NOM]</ta>
            <ta e="T279" id="Seg_5927" s="T278">sein-PST1-1SG</ta>
            <ta e="T280" id="Seg_5928" s="T279">EMPH</ta>
            <ta e="T281" id="Seg_5929" s="T280">dann</ta>
            <ta e="T282" id="Seg_5930" s="T281">Mutter-1SG.[NOM]</ta>
            <ta e="T283" id="Seg_5931" s="T282">sterben-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T284" id="Seg_5932" s="T283">Ehemann-DAT/LOC</ta>
            <ta e="T285" id="Seg_5933" s="T284">geben-PTCP.PST-3SG-ACC</ta>
            <ta e="T286" id="Seg_5934" s="T285">nachdem</ta>
            <ta e="T287" id="Seg_5935" s="T286">äh</ta>
            <ta e="T288" id="Seg_5936" s="T287">Ehemann-DAT/LOC</ta>
            <ta e="T289" id="Seg_5937" s="T288">geben-CVB.SIM=noch.nicht-3SG</ta>
            <ta e="T290" id="Seg_5938" s="T289">Familie-ABL</ta>
            <ta e="T291" id="Seg_5939" s="T290">Familie-DAT/LOC</ta>
            <ta e="T292" id="Seg_5940" s="T291">gehen-HAB-1PL</ta>
            <ta e="T293" id="Seg_5941" s="T292">dort</ta>
            <ta e="T294" id="Seg_5942" s="T293">Mutter-1SG.[NOM]</ta>
            <ta e="T295" id="Seg_5943" s="T294">Mutter-1SG.[NOM]</ta>
            <ta e="T296" id="Seg_5944" s="T295">sterben-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T297" id="Seg_5945" s="T296">so</ta>
            <ta e="T298" id="Seg_5946" s="T297">sagen-PRS.[3SG]</ta>
            <ta e="T299" id="Seg_5947" s="T298">EMPH</ta>
            <ta e="T300" id="Seg_5948" s="T299">na</ta>
            <ta e="T301" id="Seg_5949" s="T300">Waise-PL-ACC</ta>
            <ta e="T302" id="Seg_5950" s="T301">essen-EP-CAUS-FUT.[IMP.2SG]</ta>
            <ta e="T303" id="Seg_5951" s="T302">EMPH</ta>
            <ta e="T304" id="Seg_5952" s="T303">schlecht-ADVZ</ta>
            <ta e="T305" id="Seg_5953" s="T304">machen-PTCP.PST-ACC</ta>
            <ta e="T306" id="Seg_5954" s="T305">gut-ADVZ</ta>
            <ta e="T307" id="Seg_5955" s="T306">machen-PTCP.FUT-2SG-ACC</ta>
            <ta e="T308" id="Seg_5956" s="T307">man.muss</ta>
            <ta e="T309" id="Seg_5957" s="T308">dann</ta>
            <ta e="T310" id="Seg_5958" s="T309">lang</ta>
            <ta e="T311" id="Seg_5959" s="T310">glücklich</ta>
            <ta e="T312" id="Seg_5960" s="T311">sein-FUT-2SG</ta>
            <ta e="T313" id="Seg_5961" s="T312">viel</ta>
            <ta e="T314" id="Seg_5962" s="T313">Kind-PROPR.[NOM]</ta>
            <ta e="T315" id="Seg_5963" s="T314">sein-FUT-2SG</ta>
            <ta e="T316" id="Seg_5964" s="T315">fertig</ta>
            <ta e="T317" id="Seg_5965" s="T316">Leben-DAT/LOC</ta>
            <ta e="T318" id="Seg_5966" s="T317">leben-FUT-2SG</ta>
            <ta e="T319" id="Seg_5967" s="T318">sagen-PST2-3SG</ta>
            <ta e="T320" id="Seg_5968" s="T319">tatsächlich</ta>
            <ta e="T321" id="Seg_5969" s="T320">sehen.[IMP.2SG]</ta>
            <ta e="T322" id="Seg_5970" s="T321">jetzt</ta>
            <ta e="T323" id="Seg_5971" s="T322">1SG.[NOM]</ta>
            <ta e="T324" id="Seg_5972" s="T323">selbst-1SG.[NOM]</ta>
            <ta e="T325" id="Seg_5973" s="T324">aber</ta>
            <ta e="T326" id="Seg_5974" s="T325">fünf-zehn-ABL</ta>
            <ta e="T327" id="Seg_5975" s="T326">über</ta>
            <ta e="T328" id="Seg_5976" s="T327">Mensch-1SG-ACC</ta>
            <ta e="T329" id="Seg_5977" s="T328">selbst-1SG.[NOM]</ta>
            <ta e="T330" id="Seg_5978" s="T329">Inneres-1SG-ABL</ta>
            <ta e="T331" id="Seg_5979" s="T330">hinausgehen-EP-PST2-EP-1SG</ta>
            <ta e="T332" id="Seg_5980" s="T331">Enkel-PL-COM-1SG</ta>
            <ta e="T336" id="Seg_5981" s="T335">Volk-EP-1SG.[NOM]</ta>
            <ta e="T337" id="Seg_5982" s="T336">selbst-1SG.[NOM]</ta>
            <ta e="T338" id="Seg_5983" s="T337">Volk-EP-1SG.[NOM]</ta>
            <ta e="T339" id="Seg_5984" s="T338">Syndassko.[NOM]</ta>
            <ta e="T340" id="Seg_5985" s="T339">identisch</ta>
            <ta e="T341" id="Seg_5986" s="T340">Hälfte-3SG.[NOM]</ta>
            <ta e="T342" id="Seg_5987" s="T341">Hälfte-3SG.[NOM]</ta>
            <ta e="T343" id="Seg_5988" s="T342">weit.weg</ta>
            <ta e="T344" id="Seg_5989" s="T343">es.gibt-3PL</ta>
            <ta e="T345" id="Seg_5990" s="T344">noch</ta>
            <ta e="T346" id="Seg_5991" s="T345">jenes.[NOM]</ta>
            <ta e="T347" id="Seg_5992" s="T346">zu</ta>
            <ta e="T348" id="Seg_5993" s="T347">hier-ADJZ-1SG.[NOM]</ta>
            <ta e="T349" id="Seg_5994" s="T348">nur</ta>
            <ta e="T350" id="Seg_5995" s="T349">doch</ta>
            <ta e="T351" id="Seg_5996" s="T350">Gott.sei.Dank</ta>
            <ta e="T352" id="Seg_5997" s="T351">hier</ta>
            <ta e="T353" id="Seg_5998" s="T352">nur</ta>
            <ta e="T354" id="Seg_5999" s="T353">es.gibt</ta>
            <ta e="T355" id="Seg_6000" s="T354">fünf-zehn-ABL</ta>
            <ta e="T356" id="Seg_6001" s="T355">über</ta>
            <ta e="T357" id="Seg_6002" s="T356">Mensch-1PL.[NOM]</ta>
            <ta e="T358" id="Seg_6003" s="T357">Kind-PL-COM</ta>
            <ta e="T359" id="Seg_6004" s="T358">fast</ta>
            <ta e="T360" id="Seg_6005" s="T359">Syndassko.[NOM]</ta>
            <ta e="T728" id="Seg_6006" s="T361">spalten-CVB.SEQ</ta>
            <ta e="T363" id="Seg_6007" s="T362">Syndassko.[NOM]</ta>
            <ta e="T364" id="Seg_6008" s="T363">Hälfte-EP-1SG.[NOM]</ta>
            <ta e="T365" id="Seg_6009" s="T364">so</ta>
            <ta e="T366" id="Seg_6010" s="T365">und</ta>
            <ta e="T367" id="Seg_6011" s="T366">gut-ADVZ</ta>
            <ta e="T368" id="Seg_6012" s="T367">leben-PRS-1SG</ta>
            <ta e="T369" id="Seg_6013" s="T368">Kraft-PROPR</ta>
            <ta e="T370" id="Seg_6014" s="T369">Mensch-PL.[NOM]</ta>
            <ta e="T371" id="Seg_6015" s="T370">Segen-3PL-DAT/LOC</ta>
            <ta e="T372" id="Seg_6016" s="T371">Mensch-PL.[NOM]</ta>
            <ta e="T373" id="Seg_6017" s="T372">Segen-3PL.[NOM]</ta>
            <ta e="T374" id="Seg_6018" s="T373">alt-PL.[NOM]</ta>
            <ta e="T375" id="Seg_6019" s="T374">Segen-3PL.[NOM]</ta>
            <ta e="T376" id="Seg_6020" s="T375">jetzt</ta>
            <ta e="T377" id="Seg_6021" s="T376">EMPH</ta>
            <ta e="T378" id="Seg_6022" s="T377">altern-CVB.SEQ-1SG</ta>
            <ta e="T379" id="Seg_6023" s="T378">krank.sein-PRS-1SG</ta>
            <ta e="T380" id="Seg_6024" s="T379">EMPH</ta>
            <ta e="T381" id="Seg_6025" s="T380">EMPH</ta>
            <ta e="T382" id="Seg_6026" s="T381">Blutdruck-PROPR-1SG</ta>
            <ta e="T383" id="Seg_6027" s="T382">sehr</ta>
            <ta e="T384" id="Seg_6028" s="T383">groß</ta>
            <ta e="T386" id="Seg_6029" s="T384">zwei</ta>
            <ta e="T388" id="Seg_6030" s="T386">AFFIRM</ta>
            <ta e="T390" id="Seg_6031" s="T388">zwei</ta>
            <ta e="T391" id="Seg_6032" s="T390">hundert-ABL</ta>
            <ta e="T392" id="Seg_6033" s="T391">übersteigen-PRS.[3SG]</ta>
            <ta e="T395" id="Seg_6034" s="T394">sein-PRS.[3SG]</ta>
            <ta e="T396" id="Seg_6035" s="T395">jenes-3SG-1SG-ACC</ta>
            <ta e="T397" id="Seg_6036" s="T396">schaffen-NEG-1SG</ta>
            <ta e="T398" id="Seg_6037" s="T397">dann</ta>
            <ta e="T399" id="Seg_6038" s="T398">Bein-PL-EP-1SG.[NOM]</ta>
            <ta e="T400" id="Seg_6039" s="T399">versperren-PRS-3PL</ta>
            <ta e="T401" id="Seg_6040" s="T400">jetzt</ta>
            <ta e="T402" id="Seg_6041" s="T401">eins</ta>
            <ta e="T403" id="Seg_6042" s="T402">NEG</ta>
            <ta e="T404" id="Seg_6043" s="T403">Kind-DAT/LOC</ta>
            <ta e="T405" id="Seg_6044" s="T404">schaffen-CVB.SEQ</ta>
            <ta e="T406" id="Seg_6045" s="T405">ankommen-NEG-1SG</ta>
            <ta e="T407" id="Seg_6046" s="T406">EMPH</ta>
            <ta e="T408" id="Seg_6047" s="T407">liegen-PRS-1SG</ta>
            <ta e="T409" id="Seg_6048" s="T408">EMPH</ta>
            <ta e="T423" id="Seg_6049" s="T422">Paradies-DAT/LOC</ta>
            <ta e="T424" id="Seg_6050" s="T423">gehen-FUT-1SG</ta>
            <ta e="T430" id="Seg_6051" s="T428">AFFIRM</ta>
            <ta e="T431" id="Seg_6052" s="T430">Paradies-DAT/LOC</ta>
            <ta e="T432" id="Seg_6053" s="T431">gehen-FUT-1SG</ta>
            <ta e="T433" id="Seg_6054" s="T432">Kind-PL-EP-1SG.[NOM]</ta>
            <ta e="T434" id="Seg_6055" s="T433">viel-3PL</ta>
            <ta e="T435" id="Seg_6056" s="T434">Paradies-DAT/LOC</ta>
            <ta e="T436" id="Seg_6057" s="T435">fliegen-EP-CAUS-FUT-3PL</ta>
            <ta e="T440" id="Seg_6058" s="T439">Gott-DAT/LOC</ta>
            <ta e="T441" id="Seg_6059" s="T440">nun</ta>
            <ta e="T442" id="Seg_6060" s="T441">Gott-DAT/LOC</ta>
            <ta e="T443" id="Seg_6061" s="T442">gehen-FUT-1SG</ta>
            <ta e="T452" id="Seg_6062" s="T451">sagen-HAB-3PL</ta>
            <ta e="T453" id="Seg_6063" s="T452">alt-PL.[NOM]</ta>
            <ta e="T454" id="Seg_6064" s="T453">Gott-DAT/LOC</ta>
            <ta e="T455" id="Seg_6065" s="T454">gehen-FUT-2PL</ta>
            <ta e="T457" id="Seg_6066" s="T456">viel</ta>
            <ta e="T458" id="Seg_6067" s="T457">Kind-PROPR</ta>
            <ta e="T459" id="Seg_6068" s="T458">Mensch.[NOM]</ta>
            <ta e="T460" id="Seg_6069" s="T459">Gott-DAT/LOC</ta>
            <ta e="T461" id="Seg_6070" s="T460">gehen-FUT-1SG</ta>
            <ta e="T462" id="Seg_6071" s="T461">Mensch-ACC</ta>
            <ta e="T463" id="Seg_6072" s="T462">schlecht-ADVZ</ta>
            <ta e="T464" id="Seg_6073" s="T463">sprechen-NEG-1SG</ta>
            <ta e="T465" id="Seg_6074" s="T464">Mensch-ACC</ta>
            <ta e="T466" id="Seg_6075" s="T465">verfluchen-EP-RECP/COLL-NEG-1SG</ta>
            <ta e="T467" id="Seg_6076" s="T466">Sünde.[NOM]</ta>
            <ta e="T468" id="Seg_6077" s="T467">sein-FUT.[3SG]</ta>
            <ta e="T469" id="Seg_6078" s="T468">jenes.[NOM]</ta>
            <ta e="T470" id="Seg_6079" s="T469">wegen</ta>
            <ta e="T471" id="Seg_6080" s="T470">1SG.[NOM]</ta>
            <ta e="T472" id="Seg_6081" s="T471">Mensch-ACC</ta>
            <ta e="T473" id="Seg_6082" s="T472">verfluchen-EP-RECP/COLL-CVB.SIM</ta>
            <ta e="T474" id="Seg_6083" s="T473">können-PTCP.HAB-3SG</ta>
            <ta e="T475" id="Seg_6084" s="T474">NEG-1SG</ta>
            <ta e="T476" id="Seg_6085" s="T475">Sünde.[NOM]</ta>
            <ta e="T477" id="Seg_6086" s="T476">sein-FUT.[3SG]</ta>
            <ta e="T478" id="Seg_6087" s="T477">wer.[NOM]</ta>
            <ta e="T479" id="Seg_6088" s="T478">hineingehen-PST2.[3SG]</ta>
            <ta e="T480" id="Seg_6089" s="T479">essen-EP-CAUS-CVB.SEQ</ta>
            <ta e="T481" id="Seg_6090" s="T480">gehen-PRS-1SG</ta>
            <ta e="T482" id="Seg_6091" s="T481">Körper-EP-1SG.[NOM]</ta>
            <ta e="T483" id="Seg_6092" s="T482">schaffen-EP-MED-PTCP.PRS</ta>
            <ta e="T484" id="Seg_6093" s="T483">sein-TEMP-1SG</ta>
            <ta e="T485" id="Seg_6094" s="T484">jetzt</ta>
            <ta e="T486" id="Seg_6095" s="T485">schaffen-EP-MED-NEG-1SG</ta>
            <ta e="T487" id="Seg_6096" s="T486">Dunjascha-1SG.[NOM]</ta>
            <ta e="T488" id="Seg_6097" s="T487">nur</ta>
            <ta e="T489" id="Seg_6098" s="T488">sein-PST2.[3SG]</ta>
            <ta e="T491" id="Seg_6099" s="T490">gut.[NOM]</ta>
            <ta e="T492" id="Seg_6100" s="T491">Schwiegertochter-EP-1SG.[NOM]</ta>
            <ta e="T493" id="Seg_6101" s="T492">sehr</ta>
            <ta e="T494" id="Seg_6102" s="T493">gut.[NOM]</ta>
            <ta e="T495" id="Seg_6103" s="T494">Frau.[NOM]</ta>
            <ta e="T496" id="Seg_6104" s="T495">gut.[NOM]</ta>
            <ta e="T497" id="Seg_6105" s="T496">Frau-PL.[NOM]</ta>
            <ta e="T498" id="Seg_6106" s="T497">Schwiegertochter-PL-EP-1SG.[NOM]</ta>
            <ta e="T499" id="Seg_6107" s="T498">so</ta>
            <ta e="T500" id="Seg_6108" s="T499">EMPH</ta>
            <ta e="T501" id="Seg_6109" s="T500">jenes</ta>
            <ta e="T502" id="Seg_6110" s="T501">wie</ta>
            <ta e="T503" id="Seg_6111" s="T502">und</ta>
            <ta e="T504" id="Seg_6112" s="T503">sein-IMP.3SG</ta>
            <ta e="T505" id="Seg_6113" s="T504">1SG.[NOM]</ta>
            <ta e="T506" id="Seg_6114" s="T505">beunruhigen-EP-NEG-1SG</ta>
            <ta e="T507" id="Seg_6115" s="T506">3PL-DAT/LOC</ta>
            <ta e="T508" id="Seg_6116" s="T507">prüfen-RECP/COLL-IMP.3PL</ta>
            <ta e="T509" id="Seg_6117" s="T508">und</ta>
            <ta e="T510" id="Seg_6118" s="T509">prüfen-RECP/COLL-EP-NEG-IMP.3PL</ta>
            <ta e="T511" id="Seg_6119" s="T510">und</ta>
            <ta e="T512" id="Seg_6120" s="T511">1SG-DAT/LOC</ta>
            <ta e="T513" id="Seg_6121" s="T512">Bedarf-POSS</ta>
            <ta e="T514" id="Seg_6122" s="T513">NEG.EX</ta>
            <ta e="T515" id="Seg_6123" s="T514">einzeln</ta>
            <ta e="T516" id="Seg_6124" s="T515">Familie-PL.[NOM]</ta>
            <ta e="T517" id="Seg_6125" s="T516">EMPH</ta>
            <ta e="T518" id="Seg_6126" s="T517">was-DAT/LOC</ta>
            <ta e="T519" id="Seg_6127" s="T518">beunruhigen-FUT-1SG=Q</ta>
            <ta e="T520" id="Seg_6128" s="T519">jenes-DAT/LOC</ta>
            <ta e="T521" id="Seg_6129" s="T520">1SG.[NOM]</ta>
            <ta e="T522" id="Seg_6130" s="T521">Leben-EP-1SG.[NOM]</ta>
            <ta e="T523" id="Seg_6131" s="T522">aufhören-PST2-3SG</ta>
            <ta e="T524" id="Seg_6132" s="T523">3PL.[NOM]</ta>
            <ta e="T525" id="Seg_6133" s="T524">Leben-3PL-ACC</ta>
            <ta e="T526" id="Seg_6134" s="T525">berühren-NEG-1SG</ta>
            <ta e="T527" id="Seg_6135" s="T526">1SG.[NOM]</ta>
            <ta e="T528" id="Seg_6136" s="T527">leben-IMP.3PL</ta>
            <ta e="T532" id="Seg_6137" s="T531">letzter-3SG.[NOM]</ta>
            <ta e="T533" id="Seg_6138" s="T532">dann</ta>
            <ta e="T534" id="Seg_6139" s="T533">nun</ta>
            <ta e="T535" id="Seg_6140" s="T534">was-ACC</ta>
            <ta e="T536" id="Seg_6141" s="T535">sagen-EP-CAUS-PRS-2SG</ta>
            <ta e="T552" id="Seg_6142" s="T550">AFFIRM</ta>
            <ta e="T571" id="Seg_6143" s="T570">hören-EP-NEG-3PL</ta>
            <ta e="T572" id="Seg_6144" s="T571">ganz</ta>
            <ta e="T573" id="Seg_6145" s="T572">dieses.[NOM]</ta>
            <ta e="T574" id="Seg_6146" s="T573">gehen-CVB.SEQ</ta>
            <ta e="T575" id="Seg_6147" s="T574">bleiben-PRS-3PL</ta>
            <ta e="T576" id="Seg_6148" s="T575">dieses</ta>
            <ta e="T577" id="Seg_6149" s="T576">dieses</ta>
            <ta e="T578" id="Seg_6150" s="T577">Mann-EP-1SG.[NOM]</ta>
            <ta e="T579" id="Seg_6151" s="T578">ältere.Schwester-3SG-GEN</ta>
            <ta e="T580" id="Seg_6152" s="T579">äh</ta>
            <ta e="T581" id="Seg_6153" s="T580">Mann-EP-1SG.[NOM]</ta>
            <ta e="T582" id="Seg_6154" s="T581">jüngere.Schwester-EP-3SG-GEN</ta>
            <ta e="T583" id="Seg_6155" s="T582">Kind-PL-3SG.[NOM]</ta>
            <ta e="T584" id="Seg_6156" s="T583">dieses</ta>
            <ta e="T585" id="Seg_6157" s="T584">gehen-HAB-3PL</ta>
            <ta e="T587" id="Seg_6158" s="T586">Grab-PL-DAT/LOC</ta>
            <ta e="T588" id="Seg_6159" s="T587">1SG.[NOM]</ta>
            <ta e="T589" id="Seg_6160" s="T588">Kind-PL-1SG-ACC</ta>
            <ta e="T590" id="Seg_6161" s="T589">mit</ta>
            <ta e="T591" id="Seg_6162" s="T590">Mutter.[NOM]</ta>
            <ta e="T592" id="Seg_6163" s="T591">Mutter-3SG.[NOM]</ta>
            <ta e="T593" id="Seg_6164" s="T592">sterben-PTCP.PST-3SG-ACC</ta>
            <ta e="T594" id="Seg_6165" s="T593">nachdem</ta>
            <ta e="T595" id="Seg_6166" s="T594">erstmals</ta>
            <ta e="T596" id="Seg_6167" s="T595">kommen-PRS.[3SG]</ta>
            <ta e="T597" id="Seg_6168" s="T596">klein</ta>
            <ta e="T598" id="Seg_6169" s="T597">Sohn-3SG.[NOM]</ta>
            <ta e="T599" id="Seg_6170" s="T598">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T600" id="Seg_6171" s="T599">gehen-PRS.[3SG]</ta>
            <ta e="T601" id="Seg_6172" s="T600">jenes-ACC</ta>
            <ta e="T602" id="Seg_6173" s="T601">begleiten-RECP/COLL-PRS-3PL</ta>
            <ta e="T603" id="Seg_6174" s="T602">dieses</ta>
            <ta e="T604" id="Seg_6175" s="T603">jenes-ACC</ta>
            <ta e="T605" id="Seg_6176" s="T604">gehen-EP-NEG-IMP.2PL</ta>
            <ta e="T606" id="Seg_6177" s="T605">sagen-PRS-1SG</ta>
            <ta e="T607" id="Seg_6178" s="T606">hören-EP-NEG-3PL</ta>
            <ta e="T608" id="Seg_6179" s="T607">EMPH</ta>
            <ta e="T609" id="Seg_6180" s="T608">was-ACC</ta>
            <ta e="T610" id="Seg_6181" s="T609">machen-CVB.SIM</ta>
            <ta e="T611" id="Seg_6182" s="T610">gehen-PTCP.PRS-3PL.[NOM]</ta>
            <ta e="T612" id="Seg_6183" s="T611">Q</ta>
            <ta e="T613" id="Seg_6184" s="T612">Sünde.[NOM]</ta>
            <ta e="T614" id="Seg_6185" s="T613">EMPH</ta>
            <ta e="T615" id="Seg_6186" s="T614">jetzt</ta>
            <ta e="T616" id="Seg_6187" s="T615">Mensch.[NOM]</ta>
            <ta e="T617" id="Seg_6188" s="T616">Ort.[NOM]</ta>
            <ta e="T618" id="Seg_6189" s="T617">jeder</ta>
            <ta e="T619" id="Seg_6190" s="T618">sterben-CVB.SIM</ta>
            <ta e="T620" id="Seg_6191" s="T619">liegen-PRS.[3SG]</ta>
            <ta e="T621" id="Seg_6192" s="T620">Mensch.[NOM]</ta>
            <ta e="T622" id="Seg_6193" s="T621">Angst.haben-PTCP.FUT.[NOM]</ta>
            <ta e="T623" id="Seg_6194" s="T622">jetzt</ta>
            <ta e="T624" id="Seg_6195" s="T623">dort</ta>
            <ta e="T625" id="Seg_6196" s="T624">eins</ta>
            <ta e="T626" id="Seg_6197" s="T625">Alte.[NOM]</ta>
            <ta e="T627" id="Seg_6198" s="T626">sterben-PST1-3SG</ta>
            <ta e="T628" id="Seg_6199" s="T627">jetzt</ta>
            <ta e="T629" id="Seg_6200" s="T628">eins</ta>
            <ta e="T630" id="Seg_6201" s="T629">Kind.[NOM]</ta>
            <ta e="T631" id="Seg_6202" s="T630">kommen-CVB.SEQ</ta>
            <ta e="T632" id="Seg_6203" s="T631">gehen-CVB.SEQ</ta>
            <ta e="T633" id="Seg_6204" s="T632">sterben-PST2.[3SG]</ta>
            <ta e="T634" id="Seg_6205" s="T633">auch</ta>
            <ta e="T635" id="Seg_6206" s="T634">lieb-PROPR</ta>
            <ta e="T636" id="Seg_6207" s="T635">Kind-1PL.[NOM]</ta>
            <ta e="T637" id="Seg_6208" s="T636">sehr</ta>
            <ta e="T638" id="Seg_6209" s="T637">gut.[NOM]</ta>
            <ta e="T639" id="Seg_6210" s="T638">Kind.[NOM]</ta>
            <ta e="T640" id="Seg_6211" s="T639">sein-PST1-3SG</ta>
            <ta e="T641" id="Seg_6212" s="T640">Schneemobil-EP-INSTR</ta>
            <ta e="T642" id="Seg_6213" s="T641">ziehen-VBZ-CVB.SIM</ta>
            <ta e="T643" id="Seg_6214" s="T642">gehen-PTCP.PRS</ta>
            <ta e="T644" id="Seg_6215" s="T643">sein-PST1-3SG</ta>
            <ta e="T662" id="Seg_6216" s="T661">AFFIRM</ta>
            <ta e="T663" id="Seg_6217" s="T662">dieses</ta>
            <ta e="T664" id="Seg_6218" s="T663">Tabak-PART</ta>
            <ta e="T665" id="Seg_6219" s="T664">werfen-PTCP.FUT-2SG-ACC</ta>
            <ta e="T666" id="Seg_6220" s="T665">man.muss</ta>
            <ta e="T670" id="Seg_6221" s="T668">Schnaps.[NOM]</ta>
            <ta e="T671" id="Seg_6222" s="T670">trinken-TEMP-3PL</ta>
            <ta e="T672" id="Seg_6223" s="T671">Schnaps-PART</ta>
            <ta e="T673" id="Seg_6224" s="T672">gießen-PTCP.FUT-2SG-ACC</ta>
            <ta e="T674" id="Seg_6225" s="T673">man.muss</ta>
            <ta e="T676" id="Seg_6226" s="T675">letzter-3SG.[NOM]</ta>
            <ta e="T688" id="Seg_6227" s="T687">selten</ta>
            <ta e="T689" id="Seg_6228" s="T688">selten</ta>
            <ta e="T690" id="Seg_6229" s="T689">gehen-PRS-3PL</ta>
            <ta e="T691" id="Seg_6230" s="T690">Vater-3PL-DAT/LOC</ta>
            <ta e="T692" id="Seg_6231" s="T691">Grab-PL-DAT/LOC</ta>
            <ta e="T693" id="Seg_6232" s="T692">Mutter-PL-EP-1SG.[NOM]</ta>
            <ta e="T694" id="Seg_6233" s="T693">Grab-3PL-DAT/LOC</ta>
            <ta e="T695" id="Seg_6234" s="T694">gehen-HAB-3PL</ta>
            <ta e="T696" id="Seg_6235" s="T695">so</ta>
            <ta e="T697" id="Seg_6236" s="T696">EMPH</ta>
            <ta e="T698" id="Seg_6237" s="T697">solch</ta>
            <ta e="T699" id="Seg_6238" s="T698">es.gibt</ta>
            <ta e="T700" id="Seg_6239" s="T699">Schamane-PL.[NOM]</ta>
            <ta e="T701" id="Seg_6240" s="T700">Grab-3PL.[NOM]</ta>
            <ta e="T702" id="Seg_6241" s="T701">es.gibt-3PL</ta>
            <ta e="T703" id="Seg_6242" s="T702">jenes</ta>
            <ta e="T704" id="Seg_6243" s="T703">schlecht.[NOM]</ta>
            <ta e="T705" id="Seg_6244" s="T704">EMPH</ta>
            <ta e="T706" id="Seg_6245" s="T705">Schamane-PL-EP-2SG.[NOM]</ta>
            <ta e="T707" id="Seg_6246" s="T706">Sünde.[NOM]</ta>
            <ta e="T708" id="Seg_6247" s="T707">sein-HAB.[3SG]</ta>
            <ta e="T709" id="Seg_6248" s="T708">hören-EP-NEG-3PL</ta>
            <ta e="T710" id="Seg_6249" s="T709">jetzt</ta>
            <ta e="T711" id="Seg_6250" s="T710">Jugend-PL.[NOM]</ta>
            <ta e="T712" id="Seg_6251" s="T711">ganz</ta>
            <ta e="T716" id="Seg_6252" s="T715">ganz</ta>
            <ta e="T717" id="Seg_6253" s="T716">anders</ta>
            <ta e="T719" id="Seg_6254" s="T717">Volk.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr-KiPP">
            <ta e="T2" id="Seg_6255" s="T1">что-ABL</ta>
            <ta e="T3" id="Seg_6256" s="T2">грех.[NOM]</ta>
            <ta e="T4" id="Seg_6257" s="T3">говорить-PRS-2SG</ta>
            <ta e="T0" id="Seg_6258" s="T4">Q</ta>
            <ta e="T6" id="Seg_6259" s="T5">грех.[NOM]</ta>
            <ta e="T7" id="Seg_6260" s="T6">однако</ta>
            <ta e="T8" id="Seg_6261" s="T7">давно</ta>
            <ta e="T9" id="Seg_6262" s="T8">шаман-PL.[NOM]</ta>
            <ta e="T10" id="Seg_6263" s="T9">есть</ta>
            <ta e="T11" id="Seg_6264" s="T10">быть-HAB-3PL</ta>
            <ta e="T12" id="Seg_6265" s="T11">шаман-PL.[NOM]</ta>
            <ta e="T13" id="Seg_6266" s="T12">такой-PL.[NOM]</ta>
            <ta e="T14" id="Seg_6267" s="T13">чум-3PL-ACC</ta>
            <ta e="T15" id="Seg_6268" s="T14">вертеться-PTCP.PRS-DAT/LOC</ta>
            <ta e="T16" id="Seg_6269" s="T15">быть.возможным-NEG.[3SG]</ta>
            <ta e="T17" id="Seg_6270" s="T16">плохой.[NOM]</ta>
            <ta e="T18" id="Seg_6271" s="T17">вертеться-NEG.CVB.SIM</ta>
            <ta e="T19" id="Seg_6272" s="T18">идти-PTCP.FUT-2SG-ACC</ta>
            <ta e="T20" id="Seg_6273" s="T19">грех.[NOM]</ta>
            <ta e="T21" id="Seg_6274" s="T20">быть-HAB.[3SG]</ta>
            <ta e="T22" id="Seg_6275" s="T21">тот</ta>
            <ta e="T23" id="Seg_6276" s="T22">имя-3SG.[NOM]</ta>
            <ta e="T24" id="Seg_6277" s="T23">грех.[NOM]</ta>
            <ta e="T25" id="Seg_6278" s="T24">жена.[NOM]</ta>
            <ta e="T26" id="Seg_6279" s="T25">однако</ta>
            <ta e="T27" id="Seg_6280" s="T26">мужчина.[NOM]</ta>
            <ta e="T28" id="Seg_6281" s="T27">человек.[NOM]</ta>
            <ta e="T29" id="Seg_6282" s="T28">одежда-3SG-ACC</ta>
            <ta e="T30" id="Seg_6283" s="T29">наступать-PTCP.PRS.[NOM]</ta>
            <ta e="T31" id="Seg_6284" s="T30">тоже</ta>
            <ta e="T32" id="Seg_6285" s="T31">грех.[NOM]</ta>
            <ta e="T33" id="Seg_6286" s="T32">чистый.[NOM]</ta>
            <ta e="T34" id="Seg_6287" s="T33">быть-PTCP.FUT-3SG-ACC</ta>
            <ta e="T35" id="Seg_6288" s="T34">мужчина.[NOM]</ta>
            <ta e="T36" id="Seg_6289" s="T35">человек.[NOM]</ta>
            <ta e="T37" id="Seg_6290" s="T36">чистый-ADVZ</ta>
            <ta e="T38" id="Seg_6291" s="T37">жить-PTCP.FUT-3SG-ACC</ta>
            <ta e="T39" id="Seg_6292" s="T38">надо</ta>
            <ta e="T40" id="Seg_6293" s="T39">жена.[NOM]</ta>
            <ta e="T41" id="Seg_6294" s="T40">платье-POSS</ta>
            <ta e="T42" id="Seg_6295" s="T41">NEG</ta>
            <ta e="T43" id="Seg_6296" s="T42">идти-PTCP.PRS.[NOM]</ta>
            <ta e="T44" id="Seg_6297" s="T43">тоже</ta>
            <ta e="T45" id="Seg_6298" s="T44">грех.[NOM]</ta>
            <ta e="T46" id="Seg_6299" s="T45">раньше</ta>
            <ta e="T47" id="Seg_6300" s="T46">платье.[NOM]</ta>
            <ta e="T48" id="Seg_6301" s="T47">одеть-PTCP.FUT-3SG-ACC</ta>
            <ta e="T49" id="Seg_6302" s="T48">надо</ta>
            <ta e="T50" id="Seg_6303" s="T49">платье.[NOM]</ta>
            <ta e="T51" id="Seg_6304" s="T50">мужчина.[NOM]</ta>
            <ta e="T52" id="Seg_6305" s="T51">человек.[NOM]</ta>
            <ta e="T53" id="Seg_6306" s="T52">подобно</ta>
            <ta e="T55" id="Seg_6307" s="T54">теперь</ta>
            <ta e="T56" id="Seg_6308" s="T55">каждый-INTNS-3PL.[NOM]</ta>
            <ta e="T57" id="Seg_6309" s="T56">штаны-PROPR-3PL</ta>
            <ta e="T58" id="Seg_6310" s="T57">тот.[NOM]</ta>
            <ta e="T59" id="Seg_6311" s="T58">грех.[NOM]</ta>
            <ta e="T60" id="Seg_6312" s="T59">EMPH</ta>
            <ta e="T61" id="Seg_6313" s="T60">тот-3SG-2SG.[NOM]</ta>
            <ta e="T68" id="Seg_6314" s="T66">ээ</ta>
            <ta e="T69" id="Seg_6315" s="T68">вот</ta>
            <ta e="T70" id="Seg_6316" s="T69">каждый-3SG.[NOM]</ta>
            <ta e="T71" id="Seg_6317" s="T70">мужчина.[NOM]</ta>
            <ta e="T72" id="Seg_6318" s="T71">человек-DAT/LOC</ta>
            <ta e="T73" id="Seg_6319" s="T72">как</ta>
            <ta e="T74" id="Seg_6320" s="T73">плохой.[NOM]</ta>
            <ta e="T75" id="Seg_6321" s="T74">тот</ta>
            <ta e="T76" id="Seg_6322" s="T75">жена.[NOM]</ta>
            <ta e="T77" id="Seg_6323" s="T76">жена.[NOM]</ta>
            <ta e="T78" id="Seg_6324" s="T77">подобно</ta>
            <ta e="T79" id="Seg_6325" s="T78">быть-PTCP.FUT-3SG-ACC</ta>
            <ta e="T80" id="Seg_6326" s="T79">надо</ta>
            <ta e="T81" id="Seg_6327" s="T80">платье-PROPR.[NOM]</ta>
            <ta e="T82" id="Seg_6328" s="T81">платок-PROPR.[NOM]</ta>
            <ta e="T83" id="Seg_6329" s="T82">быть-PTCP.FUT-3SG-ACC</ta>
            <ta e="T84" id="Seg_6330" s="T83">надо</ta>
            <ta e="T85" id="Seg_6331" s="T84">шапка-ACC</ta>
            <ta e="T86" id="Seg_6332" s="T85">EMPH</ta>
            <ta e="T87" id="Seg_6333" s="T86">одеть-EP-NEG.CVB</ta>
            <ta e="T88" id="Seg_6334" s="T87">потом</ta>
            <ta e="T89" id="Seg_6335" s="T88">одеть-PTCP.FUT-DAT/LOC</ta>
            <ta e="T90" id="Seg_6336" s="T89">надо</ta>
            <ta e="T723" id="Seg_6337" s="T90">шапка-ACC</ta>
            <ta e="T92" id="Seg_6338" s="T91">тот.[NOM]</ta>
            <ta e="T93" id="Seg_6339" s="T92">такой-PL.[NOM]</ta>
            <ta e="T94" id="Seg_6340" s="T93">потом</ta>
            <ta e="T95" id="Seg_6341" s="T94">муж-DAT/LOC</ta>
            <ta e="T96" id="Seg_6342" s="T95">идти-PTCP.PRS-EP-2SG.[NOM]</ta>
            <ta e="T97" id="Seg_6343" s="T96">когда</ta>
            <ta e="T98" id="Seg_6344" s="T97">рука.[NOM]</ta>
            <ta e="T99" id="Seg_6345" s="T98">бить-EP-RECP/COLL-PRS-3PL</ta>
            <ta e="T100" id="Seg_6346" s="T99">икона-DAT/LOC</ta>
            <ta e="T101" id="Seg_6347" s="T100">так</ta>
            <ta e="T102" id="Seg_6348" s="T101">давать-PRS-3PL</ta>
            <ta e="T103" id="Seg_6349" s="T102">рука-3PL-ACC</ta>
            <ta e="T104" id="Seg_6350" s="T103">муж-DAT/LOC</ta>
            <ta e="T105" id="Seg_6351" s="T104">идти-PTCP.PRS</ta>
            <ta e="T724" id="Seg_6352" s="T105">человек.[NOM]</ta>
            <ta e="T107" id="Seg_6353" s="T106">рука.[NOM]</ta>
            <ta e="T108" id="Seg_6354" s="T107">давать-RECP/COLL-PRS-3PL</ta>
            <ta e="T109" id="Seg_6355" s="T108">тогда</ta>
            <ta e="T110" id="Seg_6356" s="T109">рука-3PL-ACC</ta>
            <ta e="T111" id="Seg_6357" s="T110">давать-TEMP-3PL</ta>
            <ta e="T112" id="Seg_6358" s="T111">жена.[NOM]</ta>
            <ta e="T113" id="Seg_6359" s="T112">становиться-PRS.[3SG]</ta>
            <ta e="T114" id="Seg_6360" s="T113">тот</ta>
            <ta e="T725" id="Seg_6361" s="T114">человек.[NOM]</ta>
            <ta e="T116" id="Seg_6362" s="T115">такой</ta>
            <ta e="T117" id="Seg_6363" s="T116">обычай-PL.[NOM]</ta>
            <ta e="T118" id="Seg_6364" s="T117">такой</ta>
            <ta e="T119" id="Seg_6365" s="T118">AFFIRM</ta>
            <ta e="T120" id="Seg_6366" s="T119">сила-DAT/LOC</ta>
            <ta e="T121" id="Seg_6367" s="T120">давать-PRS-3PL</ta>
            <ta e="T122" id="Seg_6368" s="T121">EMPH</ta>
            <ta e="T123" id="Seg_6369" s="T122">человек-ACC</ta>
            <ta e="T124" id="Seg_6370" s="T123">хотеть.[IMP.2SG]</ta>
            <ta e="T125" id="Seg_6371" s="T124">да</ta>
            <ta e="T126" id="Seg_6372" s="T125">хотеть-EP-NEG.[IMP.2SG]</ta>
            <ta e="T127" id="Seg_6373" s="T126">да</ta>
            <ta e="T128" id="Seg_6374" s="T127">давать-PRS-3PL</ta>
            <ta e="T129" id="Seg_6375" s="T128">ведь</ta>
            <ta e="T130" id="Seg_6376" s="T129">один</ta>
            <ta e="T131" id="Seg_6377" s="T130">плакать-CVB.SIM-плакать-CVB.SIM</ta>
            <ta e="T132" id="Seg_6378" s="T131">идти-CVB.SIM</ta>
            <ta e="T133" id="Seg_6379" s="T132">стоять-FUT-2SG</ta>
            <ta e="T134" id="Seg_6380" s="T133">тот-DAT/LOC</ta>
            <ta e="T135" id="Seg_6381" s="T134">тот.[NOM]</ta>
            <ta e="T136" id="Seg_6382" s="T135">подобно</ta>
            <ta e="T137" id="Seg_6383" s="T136">1SG.[NOM]</ta>
            <ta e="T138" id="Seg_6384" s="T137">идти-CVB.SIM</ta>
            <ta e="T139" id="Seg_6385" s="T138">стоять-PST2-EP-1SG</ta>
            <ta e="T140" id="Seg_6386" s="T139">муж-DAT/LOC</ta>
            <ta e="T141" id="Seg_6387" s="T140">плакать-CVB.SIM-плакать-CVB.SIM-1SG</ta>
            <ta e="T142" id="Seg_6388" s="T141">десять</ta>
            <ta e="T143" id="Seg_6389" s="T142">шесть</ta>
            <ta e="T144" id="Seg_6390" s="T143">год-PROPR-1SG-DAT/LOC</ta>
            <ta e="T145" id="Seg_6391" s="T144">вот</ta>
            <ta e="T146" id="Seg_6392" s="T145">что.[NOM]</ta>
            <ta e="T147" id="Seg_6393" s="T146">человек-3SG.[NOM]</ta>
            <ta e="T148" id="Seg_6394" s="T147">муж-DAT/LOC</ta>
            <ta e="T149" id="Seg_6395" s="T148">идти-FUT.[3SG]=Q</ta>
            <ta e="T150" id="Seg_6396" s="T149">тот</ta>
            <ta e="T151" id="Seg_6397" s="T150">десять</ta>
            <ta e="T152" id="Seg_6398" s="T151">шесть</ta>
            <ta e="T153" id="Seg_6399" s="T152">год-PROPR-DAT/LOC</ta>
            <ta e="T154" id="Seg_6400" s="T153">давать-PST2-3PL</ta>
            <ta e="T157" id="Seg_6401" s="T156">совсем</ta>
            <ta e="T158" id="Seg_6402" s="T157">молодой.[NOM]</ta>
            <ta e="T159" id="Seg_6403" s="T158">чистый.[NOM]</ta>
            <ta e="T160" id="Seg_6404" s="T159">быть-PST1-1SG</ta>
            <ta e="T161" id="Seg_6405" s="T160">хоть</ta>
            <ta e="T162" id="Seg_6406" s="T161">месячные-POSS</ta>
            <ta e="T163" id="Seg_6407" s="T162">NEG</ta>
            <ta e="T164" id="Seg_6408" s="T163">NEG</ta>
            <ta e="T165" id="Seg_6409" s="T164">быть-PST1-1SG</ta>
            <ta e="T166" id="Seg_6410" s="T165">потом</ta>
            <ta e="T167" id="Seg_6411" s="T166">тот</ta>
            <ta e="T168" id="Seg_6412" s="T167">выйти-PST1-3SG</ta>
            <ta e="T169" id="Seg_6413" s="T168">большой</ta>
            <ta e="T170" id="Seg_6414" s="T169">дочь-EP-1SG.[NOM]</ta>
            <ta e="T171" id="Seg_6415" s="T170">теперь</ta>
            <ta e="T172" id="Seg_6416" s="T171">EMPH</ta>
            <ta e="T173" id="Seg_6417" s="T172">тот</ta>
            <ta e="T174" id="Seg_6418" s="T173">тот</ta>
            <ta e="T175" id="Seg_6419" s="T174">ребенок-1SG.[NOM]</ta>
            <ta e="T176" id="Seg_6420" s="T175">десять</ta>
            <ta e="T177" id="Seg_6421" s="T176">семь</ta>
            <ta e="T178" id="Seg_6422" s="T177">десять</ta>
            <ta e="T179" id="Seg_6423" s="T178">восемь</ta>
            <ta e="T180" id="Seg_6424" s="T179">год-PROPR-1SG-DAT/LOC</ta>
            <ta e="T181" id="Seg_6425" s="T180">родиться-PST2.[3SG]</ta>
            <ta e="T182" id="Seg_6426" s="T181">ребенок.[NOM]</ta>
            <ta e="T183" id="Seg_6427" s="T182">тот.[NOM]</ta>
            <ta e="T184" id="Seg_6428" s="T183">вот</ta>
            <ta e="T185" id="Seg_6429" s="T184">потом</ta>
            <ta e="T186" id="Seg_6430" s="T185">постареть-CVB.SEQ</ta>
            <ta e="T187" id="Seg_6431" s="T186">постареть-CVB.SEQ-1SG</ta>
            <ta e="T188" id="Seg_6432" s="T187">тот.[NOM]</ta>
            <ta e="T189" id="Seg_6433" s="T188">позже</ta>
            <ta e="T190" id="Seg_6434" s="T189">позже</ta>
            <ta e="T191" id="Seg_6435" s="T190">потом</ta>
            <ta e="T192" id="Seg_6436" s="T191">MOD</ta>
            <ta e="T194" id="Seg_6437" s="T193">ребенок-VBZ-CVB.SEQ</ta>
            <ta e="T195" id="Seg_6438" s="T194">да</ta>
            <ta e="T196" id="Seg_6439" s="T195">ребенок-VBZ-CVB.SEQ</ta>
            <ta e="T197" id="Seg_6440" s="T196">идти-PRS-2SG</ta>
            <ta e="T198" id="Seg_6441" s="T197">EMPH</ta>
            <ta e="T203" id="Seg_6442" s="T202">очень</ta>
            <ta e="T204" id="Seg_6443" s="T203">хороший</ta>
            <ta e="T205" id="Seg_6444" s="T204">человек.[NOM]</ta>
            <ta e="T206" id="Seg_6445" s="T205">быть-PST1-3SG</ta>
            <ta e="T207" id="Seg_6446" s="T206">муж-EP-1SG.[NOM]</ta>
            <ta e="T208" id="Seg_6447" s="T207">этот</ta>
            <ta e="T209" id="Seg_6448" s="T208">сидеть-PTCP.HAB</ta>
            <ta e="T210" id="Seg_6449" s="T209">человек.[NOM]</ta>
            <ta e="T726" id="Seg_6450" s="T210">тот.[NOM]</ta>
            <ta e="T212" id="Seg_6451" s="T211">тот</ta>
            <ta e="T213" id="Seg_6452" s="T212">1SG.[NOM]</ta>
            <ta e="T214" id="Seg_6453" s="T213">девять</ta>
            <ta e="T215" id="Seg_6454" s="T214">ребенок-PROPR.[NOM]</ta>
            <ta e="T216" id="Seg_6455" s="T215">быть-PST1-NEC-EP-1SG</ta>
            <ta e="T217" id="Seg_6456" s="T216">EMPH</ta>
            <ta e="T218" id="Seg_6457" s="T217">еще</ta>
            <ta e="T219" id="Seg_6458" s="T218">еще</ta>
            <ta e="T220" id="Seg_6459" s="T219">да</ta>
            <ta e="T221" id="Seg_6460" s="T220">молодой.[NOM]</ta>
            <ta e="T222" id="Seg_6461" s="T221">быть-PST2.NEG-1SG</ta>
            <ta e="T223" id="Seg_6462" s="T222">там</ta>
            <ta e="T224" id="Seg_6463" s="T223">вот</ta>
            <ta e="T225" id="Seg_6464" s="T224">такой</ta>
            <ta e="T226" id="Seg_6465" s="T225">обычай-PROPR.[NOM]</ta>
            <ta e="T227" id="Seg_6466" s="T226">быть-PST1-3PL</ta>
            <ta e="T228" id="Seg_6467" s="T227">потом</ta>
            <ta e="T229" id="Seg_6468" s="T228">ведь</ta>
            <ta e="T230" id="Seg_6469" s="T229">жизнь.[NOM]</ta>
            <ta e="T231" id="Seg_6470" s="T230">жить-PRS-2SG</ta>
            <ta e="T232" id="Seg_6471" s="T231">сирота-PL-ACC</ta>
            <ta e="T233" id="Seg_6472" s="T232">есть-CAUS-EP-IMP.2PL</ta>
            <ta e="T727" id="Seg_6473" s="T233">говорить-HAB-3PL</ta>
            <ta e="T235" id="Seg_6474" s="T234">сирота.[NOM]</ta>
            <ta e="T236" id="Seg_6475" s="T235">ребенок-3PL.[NOM]</ta>
            <ta e="T237" id="Seg_6476" s="T236">грех.[NOM]</ta>
            <ta e="T238" id="Seg_6477" s="T237">быть-FUT.[3SG]</ta>
            <ta e="T239" id="Seg_6478" s="T238">ребенок-3PL.[NOM]</ta>
            <ta e="T240" id="Seg_6479" s="T239">есть-CAUS-EP-IMP.2PL</ta>
            <ta e="T241" id="Seg_6480" s="T240">говорить-HAB-3PL</ta>
            <ta e="T242" id="Seg_6481" s="T241">плохой-ADVZ</ta>
            <ta e="T243" id="Seg_6482" s="T242">делать-PTCP.PST</ta>
            <ta e="T244" id="Seg_6483" s="T243">человек-ACC</ta>
            <ta e="T245" id="Seg_6484" s="T244">хороший-ADVZ</ta>
            <ta e="T246" id="Seg_6485" s="T245">провожать-EP-IMP.2PL</ta>
            <ta e="T247" id="Seg_6486" s="T246">говорить-HAB-3PL</ta>
            <ta e="T248" id="Seg_6487" s="T247">наоборот</ta>
            <ta e="T249" id="Seg_6488" s="T248">почему</ta>
            <ta e="T250" id="Seg_6489" s="T249">быть-PRS.[3SG]</ta>
            <ta e="T251" id="Seg_6490" s="T250">говорить-TEMP-1SG</ta>
            <ta e="T252" id="Seg_6491" s="T251">грех.[NOM]</ta>
            <ta e="T253" id="Seg_6492" s="T252">быть-FUT.[3SG]</ta>
            <ta e="T254" id="Seg_6493" s="T253">потом</ta>
            <ta e="T255" id="Seg_6494" s="T254">сам-2SG.[NOM]</ta>
            <ta e="T256" id="Seg_6495" s="T255">тот</ta>
            <ta e="T257" id="Seg_6496" s="T256">хороший-ADVZ</ta>
            <ta e="T258" id="Seg_6497" s="T257">жить-TEMP-2SG</ta>
            <ta e="T259" id="Seg_6498" s="T258">наоборот</ta>
            <ta e="T260" id="Seg_6499" s="T259">сам-DAT/LOC-DAT/LOC</ta>
            <ta e="T261" id="Seg_6500" s="T260">завидовать-FUT-3PL</ta>
            <ta e="T262" id="Seg_6501" s="T261">говорить-HAB-3PL</ta>
            <ta e="T263" id="Seg_6502" s="T262">предок-PL.[NOM]</ta>
            <ta e="T264" id="Seg_6503" s="T263">обычай-PL.[NOM]</ta>
            <ta e="T265" id="Seg_6504" s="T264">такой</ta>
            <ta e="T266" id="Seg_6505" s="T265">потом</ta>
            <ta e="T267" id="Seg_6506" s="T266">счастливый.[NOM]</ta>
            <ta e="T268" id="Seg_6507" s="T267">быть-PST1-2SG</ta>
            <ta e="T269" id="Seg_6508" s="T268">сам-2SG.[NOM]</ta>
            <ta e="T270" id="Seg_6509" s="T269">позже</ta>
            <ta e="T271" id="Seg_6510" s="T270">счастливый.[NOM]</ta>
            <ta e="T272" id="Seg_6511" s="T271">быть-PST1-2SG</ta>
            <ta e="T273" id="Seg_6512" s="T272">говорить-PST1-3SG</ta>
            <ta e="T274" id="Seg_6513" s="T273">1SG.[NOM]</ta>
            <ta e="T275" id="Seg_6514" s="T274">теперь</ta>
            <ta e="T276" id="Seg_6515" s="T275">счастливый-1SG</ta>
            <ta e="T277" id="Seg_6516" s="T276">тот</ta>
            <ta e="T278" id="Seg_6517" s="T277">сирота.[NOM]</ta>
            <ta e="T279" id="Seg_6518" s="T278">быть-PST1-1SG</ta>
            <ta e="T280" id="Seg_6519" s="T279">EMPH</ta>
            <ta e="T281" id="Seg_6520" s="T280">потом</ta>
            <ta e="T282" id="Seg_6521" s="T281">мать-1SG.[NOM]</ta>
            <ta e="T283" id="Seg_6522" s="T282">умирать-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T284" id="Seg_6523" s="T283">муж-DAT/LOC</ta>
            <ta e="T285" id="Seg_6524" s="T284">давать-PTCP.PST-3SG-ACC</ta>
            <ta e="T286" id="Seg_6525" s="T285">после.того</ta>
            <ta e="T287" id="Seg_6526" s="T286">ээ</ta>
            <ta e="T288" id="Seg_6527" s="T287">муж-DAT/LOC</ta>
            <ta e="T289" id="Seg_6528" s="T288">давать-CVB.SIM=еще.не-3SG</ta>
            <ta e="T290" id="Seg_6529" s="T289">семья-ABL</ta>
            <ta e="T291" id="Seg_6530" s="T290">семья-DAT/LOC</ta>
            <ta e="T292" id="Seg_6531" s="T291">идти-HAB-1PL</ta>
            <ta e="T293" id="Seg_6532" s="T292">там</ta>
            <ta e="T294" id="Seg_6533" s="T293">мать-1SG.[NOM]</ta>
            <ta e="T295" id="Seg_6534" s="T294">мать-1SG.[NOM]</ta>
            <ta e="T296" id="Seg_6535" s="T295">умирать-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T297" id="Seg_6536" s="T296">так</ta>
            <ta e="T298" id="Seg_6537" s="T297">говорить-PRS.[3SG]</ta>
            <ta e="T299" id="Seg_6538" s="T298">EMPH</ta>
            <ta e="T300" id="Seg_6539" s="T299">эй</ta>
            <ta e="T301" id="Seg_6540" s="T300">сирота-PL-ACC</ta>
            <ta e="T302" id="Seg_6541" s="T301">есть-EP-CAUS-FUT.[IMP.2SG]</ta>
            <ta e="T303" id="Seg_6542" s="T302">EMPH</ta>
            <ta e="T304" id="Seg_6543" s="T303">плохой-ADVZ</ta>
            <ta e="T305" id="Seg_6544" s="T304">делать-PTCP.PST-ACC</ta>
            <ta e="T306" id="Seg_6545" s="T305">хороший-ADVZ</ta>
            <ta e="T307" id="Seg_6546" s="T306">делать-PTCP.FUT-2SG-ACC</ta>
            <ta e="T308" id="Seg_6547" s="T307">надо</ta>
            <ta e="T309" id="Seg_6548" s="T308">тогда</ta>
            <ta e="T310" id="Seg_6549" s="T309">длинный</ta>
            <ta e="T311" id="Seg_6550" s="T310">счастливый</ta>
            <ta e="T312" id="Seg_6551" s="T311">быть-FUT-2SG</ta>
            <ta e="T313" id="Seg_6552" s="T312">много</ta>
            <ta e="T314" id="Seg_6553" s="T313">ребенок-PROPR.[NOM]</ta>
            <ta e="T315" id="Seg_6554" s="T314">быть-FUT-2SG</ta>
            <ta e="T316" id="Seg_6555" s="T315">готов</ta>
            <ta e="T317" id="Seg_6556" s="T316">жизнь-DAT/LOC</ta>
            <ta e="T318" id="Seg_6557" s="T317">жить-FUT-2SG</ta>
            <ta e="T319" id="Seg_6558" s="T318">говорить-PST2-3SG</ta>
            <ta e="T320" id="Seg_6559" s="T319">вправду</ta>
            <ta e="T321" id="Seg_6560" s="T320">видеть.[IMP.2SG]</ta>
            <ta e="T322" id="Seg_6561" s="T321">теперь</ta>
            <ta e="T323" id="Seg_6562" s="T322">1SG.[NOM]</ta>
            <ta e="T324" id="Seg_6563" s="T323">сам-1SG.[NOM]</ta>
            <ta e="T325" id="Seg_6564" s="T324">однако</ta>
            <ta e="T326" id="Seg_6565" s="T325">пять-десять-ABL</ta>
            <ta e="T327" id="Seg_6566" s="T326">больше</ta>
            <ta e="T328" id="Seg_6567" s="T327">человек-1SG-ACC</ta>
            <ta e="T329" id="Seg_6568" s="T328">сам-1SG.[NOM]</ta>
            <ta e="T330" id="Seg_6569" s="T329">нутро-1SG-ABL</ta>
            <ta e="T331" id="Seg_6570" s="T330">выйти-EP-PST2-EP-1SG</ta>
            <ta e="T332" id="Seg_6571" s="T331">внук-PL-COM-1SG</ta>
            <ta e="T336" id="Seg_6572" s="T335">народ-EP-1SG.[NOM]</ta>
            <ta e="T337" id="Seg_6573" s="T336">сам-1SG.[NOM]</ta>
            <ta e="T338" id="Seg_6574" s="T337">народ-EP-1SG.[NOM]</ta>
            <ta e="T339" id="Seg_6575" s="T338">Сындасско.[NOM]</ta>
            <ta e="T340" id="Seg_6576" s="T339">одинаковый</ta>
            <ta e="T341" id="Seg_6577" s="T340">половина-3SG.[NOM]</ta>
            <ta e="T342" id="Seg_6578" s="T341">половина-3SG.[NOM]</ta>
            <ta e="T343" id="Seg_6579" s="T342">далеко</ta>
            <ta e="T344" id="Seg_6580" s="T343">есть-3PL</ta>
            <ta e="T345" id="Seg_6581" s="T344">еще</ta>
            <ta e="T346" id="Seg_6582" s="T345">тот.[NOM]</ta>
            <ta e="T347" id="Seg_6583" s="T346">к</ta>
            <ta e="T348" id="Seg_6584" s="T347">здесь-ADJZ-1SG.[NOM]</ta>
            <ta e="T349" id="Seg_6585" s="T348">только</ta>
            <ta e="T350" id="Seg_6586" s="T349">вот</ta>
            <ta e="T351" id="Seg_6587" s="T350">слава.богу</ta>
            <ta e="T352" id="Seg_6588" s="T351">здесь</ta>
            <ta e="T353" id="Seg_6589" s="T352">только</ta>
            <ta e="T354" id="Seg_6590" s="T353">есть</ta>
            <ta e="T355" id="Seg_6591" s="T354">пять-десять-ABL</ta>
            <ta e="T356" id="Seg_6592" s="T355">больше</ta>
            <ta e="T357" id="Seg_6593" s="T356">человек-1PL.[NOM]</ta>
            <ta e="T358" id="Seg_6594" s="T357">ребенок-PL-COM</ta>
            <ta e="T359" id="Seg_6595" s="T358">почти</ta>
            <ta e="T360" id="Seg_6596" s="T359">Сындасско.[NOM]</ta>
            <ta e="T728" id="Seg_6597" s="T361">колоть-CVB.SEQ</ta>
            <ta e="T363" id="Seg_6598" s="T362">Сындасско.[NOM]</ta>
            <ta e="T364" id="Seg_6599" s="T363">половина-EP-1SG.[NOM]</ta>
            <ta e="T365" id="Seg_6600" s="T364">так</ta>
            <ta e="T366" id="Seg_6601" s="T365">да</ta>
            <ta e="T367" id="Seg_6602" s="T366">хороший-ADVZ</ta>
            <ta e="T368" id="Seg_6603" s="T367">жить-PRS-1SG</ta>
            <ta e="T369" id="Seg_6604" s="T368">сила-PROPR</ta>
            <ta e="T370" id="Seg_6605" s="T369">человек-PL.[NOM]</ta>
            <ta e="T371" id="Seg_6606" s="T370">благословение-3PL-DAT/LOC</ta>
            <ta e="T372" id="Seg_6607" s="T371">человек-PL.[NOM]</ta>
            <ta e="T373" id="Seg_6608" s="T372">благословение-3PL.[NOM]</ta>
            <ta e="T374" id="Seg_6609" s="T373">старый-PL.[NOM]</ta>
            <ta e="T375" id="Seg_6610" s="T374">благословение-3PL.[NOM]</ta>
            <ta e="T376" id="Seg_6611" s="T375">теперь</ta>
            <ta e="T377" id="Seg_6612" s="T376">EMPH</ta>
            <ta e="T378" id="Seg_6613" s="T377">постареть-CVB.SEQ-1SG</ta>
            <ta e="T379" id="Seg_6614" s="T378">быть.больным-PRS-1SG</ta>
            <ta e="T380" id="Seg_6615" s="T379">EMPH</ta>
            <ta e="T381" id="Seg_6616" s="T380">EMPH</ta>
            <ta e="T382" id="Seg_6617" s="T381">давление-PROPR-1SG</ta>
            <ta e="T383" id="Seg_6618" s="T382">очень</ta>
            <ta e="T384" id="Seg_6619" s="T383">большой</ta>
            <ta e="T386" id="Seg_6620" s="T384">два</ta>
            <ta e="T388" id="Seg_6621" s="T386">AFFIRM</ta>
            <ta e="T390" id="Seg_6622" s="T388">два</ta>
            <ta e="T391" id="Seg_6623" s="T390">сто-ABL</ta>
            <ta e="T392" id="Seg_6624" s="T391">превышать-PRS.[3SG]</ta>
            <ta e="T395" id="Seg_6625" s="T394">быть-PRS.[3SG]</ta>
            <ta e="T396" id="Seg_6626" s="T395">тот-3SG-1SG-ACC</ta>
            <ta e="T397" id="Seg_6627" s="T396">мочь-NEG-1SG</ta>
            <ta e="T398" id="Seg_6628" s="T397">потом</ta>
            <ta e="T399" id="Seg_6629" s="T398">нога-PL-EP-1SG.[NOM]</ta>
            <ta e="T400" id="Seg_6630" s="T399">заложить-PRS-3PL</ta>
            <ta e="T401" id="Seg_6631" s="T400">теперь</ta>
            <ta e="T402" id="Seg_6632" s="T401">один</ta>
            <ta e="T403" id="Seg_6633" s="T402">NEG</ta>
            <ta e="T404" id="Seg_6634" s="T403">ребенок-DAT/LOC</ta>
            <ta e="T405" id="Seg_6635" s="T404">мочь-CVB.SEQ</ta>
            <ta e="T406" id="Seg_6636" s="T405">доезжать-NEG-1SG</ta>
            <ta e="T407" id="Seg_6637" s="T406">EMPH</ta>
            <ta e="T408" id="Seg_6638" s="T407">лежать-PRS-1SG</ta>
            <ta e="T409" id="Seg_6639" s="T408">EMPH</ta>
            <ta e="T423" id="Seg_6640" s="T422">рай-DAT/LOC</ta>
            <ta e="T424" id="Seg_6641" s="T423">идти-FUT-1SG</ta>
            <ta e="T430" id="Seg_6642" s="T428">AFFIRM</ta>
            <ta e="T431" id="Seg_6643" s="T430">рай-DAT/LOC</ta>
            <ta e="T432" id="Seg_6644" s="T431">идти-FUT-1SG</ta>
            <ta e="T433" id="Seg_6645" s="T432">ребенок-PL-EP-1SG.[NOM]</ta>
            <ta e="T434" id="Seg_6646" s="T433">много-3PL</ta>
            <ta e="T435" id="Seg_6647" s="T434">рай-DAT/LOC</ta>
            <ta e="T436" id="Seg_6648" s="T435">летать-EP-CAUS-FUT-3PL</ta>
            <ta e="T440" id="Seg_6649" s="T439">Бог-DAT/LOC</ta>
            <ta e="T441" id="Seg_6650" s="T440">вот</ta>
            <ta e="T442" id="Seg_6651" s="T441">Бог-DAT/LOC</ta>
            <ta e="T443" id="Seg_6652" s="T442">идти-FUT-1SG</ta>
            <ta e="T452" id="Seg_6653" s="T451">говорить-HAB-3PL</ta>
            <ta e="T453" id="Seg_6654" s="T452">старый-PL.[NOM]</ta>
            <ta e="T454" id="Seg_6655" s="T453">Бог-DAT/LOC</ta>
            <ta e="T455" id="Seg_6656" s="T454">идти-FUT-2PL</ta>
            <ta e="T457" id="Seg_6657" s="T456">много</ta>
            <ta e="T458" id="Seg_6658" s="T457">ребенок-PROPR</ta>
            <ta e="T459" id="Seg_6659" s="T458">человек.[NOM]</ta>
            <ta e="T460" id="Seg_6660" s="T459">Бог-DAT/LOC</ta>
            <ta e="T461" id="Seg_6661" s="T460">идти-FUT-1SG</ta>
            <ta e="T462" id="Seg_6662" s="T461">человек-ACC</ta>
            <ta e="T463" id="Seg_6663" s="T462">плохой-ADVZ</ta>
            <ta e="T464" id="Seg_6664" s="T463">говорить-NEG-1SG</ta>
            <ta e="T465" id="Seg_6665" s="T464">человек-ACC</ta>
            <ta e="T466" id="Seg_6666" s="T465">проклинать-EP-RECP/COLL-NEG-1SG</ta>
            <ta e="T467" id="Seg_6667" s="T466">грех.[NOM]</ta>
            <ta e="T468" id="Seg_6668" s="T467">быть-FUT.[3SG]</ta>
            <ta e="T469" id="Seg_6669" s="T468">тот.[NOM]</ta>
            <ta e="T470" id="Seg_6670" s="T469">из_за</ta>
            <ta e="T471" id="Seg_6671" s="T470">1SG.[NOM]</ta>
            <ta e="T472" id="Seg_6672" s="T471">человек-ACC</ta>
            <ta e="T473" id="Seg_6673" s="T472">проклинать-EP-RECP/COLL-CVB.SIM</ta>
            <ta e="T474" id="Seg_6674" s="T473">мочь-PTCP.HAB-3SG</ta>
            <ta e="T475" id="Seg_6675" s="T474">NEG-1SG</ta>
            <ta e="T476" id="Seg_6676" s="T475">грех.[NOM]</ta>
            <ta e="T477" id="Seg_6677" s="T476">быть-FUT.[3SG]</ta>
            <ta e="T478" id="Seg_6678" s="T477">кто.[NOM]</ta>
            <ta e="T479" id="Seg_6679" s="T478">входить-PST2.[3SG]</ta>
            <ta e="T480" id="Seg_6680" s="T479">есть-EP-CAUS-CVB.SEQ</ta>
            <ta e="T481" id="Seg_6681" s="T480">идти-PRS-1SG</ta>
            <ta e="T482" id="Seg_6682" s="T481">тело-EP-1SG.[NOM]</ta>
            <ta e="T483" id="Seg_6683" s="T482">мочь-EP-MED-PTCP.PRS</ta>
            <ta e="T484" id="Seg_6684" s="T483">быть-TEMP-1SG</ta>
            <ta e="T485" id="Seg_6685" s="T484">теперь</ta>
            <ta e="T486" id="Seg_6686" s="T485">мочь-EP-MED-NEG-1SG</ta>
            <ta e="T487" id="Seg_6687" s="T486">Дуняша-1SG.[NOM]</ta>
            <ta e="T488" id="Seg_6688" s="T487">только</ta>
            <ta e="T489" id="Seg_6689" s="T488">быть-PST2.[3SG]</ta>
            <ta e="T491" id="Seg_6690" s="T490">хороший.[NOM]</ta>
            <ta e="T492" id="Seg_6691" s="T491">невестка-EP-1SG.[NOM]</ta>
            <ta e="T493" id="Seg_6692" s="T492">очень</ta>
            <ta e="T494" id="Seg_6693" s="T493">хороший.[NOM]</ta>
            <ta e="T495" id="Seg_6694" s="T494">жена.[NOM]</ta>
            <ta e="T496" id="Seg_6695" s="T495">хороший.[NOM]</ta>
            <ta e="T497" id="Seg_6696" s="T496">жена-PL.[NOM]</ta>
            <ta e="T498" id="Seg_6697" s="T497">невестка-PL-EP-1SG.[NOM]</ta>
            <ta e="T499" id="Seg_6698" s="T498">так</ta>
            <ta e="T500" id="Seg_6699" s="T499">EMPH</ta>
            <ta e="T501" id="Seg_6700" s="T500">тот</ta>
            <ta e="T502" id="Seg_6701" s="T501">как</ta>
            <ta e="T503" id="Seg_6702" s="T502">да</ta>
            <ta e="T504" id="Seg_6703" s="T503">быть-IMP.3SG</ta>
            <ta e="T505" id="Seg_6704" s="T504">1SG.[NOM]</ta>
            <ta e="T506" id="Seg_6705" s="T505">заботить-EP-NEG-1SG</ta>
            <ta e="T507" id="Seg_6706" s="T506">3PL-DAT/LOC</ta>
            <ta e="T508" id="Seg_6707" s="T507">проверять-RECP/COLL-IMP.3PL</ta>
            <ta e="T509" id="Seg_6708" s="T508">да</ta>
            <ta e="T510" id="Seg_6709" s="T509">проверять-RECP/COLL-EP-NEG-IMP.3PL</ta>
            <ta e="T511" id="Seg_6710" s="T510">да</ta>
            <ta e="T512" id="Seg_6711" s="T511">1SG-DAT/LOC</ta>
            <ta e="T513" id="Seg_6712" s="T512">потребность-POSS</ta>
            <ta e="T514" id="Seg_6713" s="T513">NEG.EX</ta>
            <ta e="T515" id="Seg_6714" s="T514">отдельно</ta>
            <ta e="T516" id="Seg_6715" s="T515">семья-PL.[NOM]</ta>
            <ta e="T517" id="Seg_6716" s="T516">EMPH</ta>
            <ta e="T518" id="Seg_6717" s="T517">что-DAT/LOC</ta>
            <ta e="T519" id="Seg_6718" s="T518">заботить-FUT-1SG=Q</ta>
            <ta e="T520" id="Seg_6719" s="T519">тот-DAT/LOC</ta>
            <ta e="T521" id="Seg_6720" s="T520">1SG.[NOM]</ta>
            <ta e="T522" id="Seg_6721" s="T521">жизнь-EP-1SG.[NOM]</ta>
            <ta e="T523" id="Seg_6722" s="T522">кончать-PST2-3SG</ta>
            <ta e="T524" id="Seg_6723" s="T523">3PL.[NOM]</ta>
            <ta e="T525" id="Seg_6724" s="T524">жизнь-3PL-ACC</ta>
            <ta e="T526" id="Seg_6725" s="T525">трогать-NEG-1SG</ta>
            <ta e="T527" id="Seg_6726" s="T526">1SG.[NOM]</ta>
            <ta e="T528" id="Seg_6727" s="T527">жить-IMP.3PL</ta>
            <ta e="T532" id="Seg_6728" s="T531">последний-3SG.[NOM]</ta>
            <ta e="T533" id="Seg_6729" s="T532">потом</ta>
            <ta e="T534" id="Seg_6730" s="T533">вот</ta>
            <ta e="T535" id="Seg_6731" s="T534">что-ACC</ta>
            <ta e="T536" id="Seg_6732" s="T535">говорить-EP-CAUS-PRS-2SG</ta>
            <ta e="T552" id="Seg_6733" s="T550">AFFIRM</ta>
            <ta e="T571" id="Seg_6734" s="T570">слышать-EP-NEG-3PL</ta>
            <ta e="T572" id="Seg_6735" s="T571">совсем</ta>
            <ta e="T573" id="Seg_6736" s="T572">тот.[NOM]</ta>
            <ta e="T574" id="Seg_6737" s="T573">идти-CVB.SEQ</ta>
            <ta e="T575" id="Seg_6738" s="T574">оставаться-PRS-3PL</ta>
            <ta e="T576" id="Seg_6739" s="T575">тот</ta>
            <ta e="T577" id="Seg_6740" s="T576">этот</ta>
            <ta e="T578" id="Seg_6741" s="T577">мужчина-EP-1SG.[NOM]</ta>
            <ta e="T579" id="Seg_6742" s="T578">старшая.сестра-3SG-GEN</ta>
            <ta e="T580" id="Seg_6743" s="T579">ээ</ta>
            <ta e="T581" id="Seg_6744" s="T580">мужчина-EP-1SG.[NOM]</ta>
            <ta e="T582" id="Seg_6745" s="T581">младшая.сестра-EP-3SG-GEN</ta>
            <ta e="T583" id="Seg_6746" s="T582">ребенок-PL-3SG.[NOM]</ta>
            <ta e="T584" id="Seg_6747" s="T583">тот</ta>
            <ta e="T585" id="Seg_6748" s="T584">идти-HAB-3PL</ta>
            <ta e="T587" id="Seg_6749" s="T586">могила-PL-DAT/LOC</ta>
            <ta e="T588" id="Seg_6750" s="T587">1SG.[NOM]</ta>
            <ta e="T589" id="Seg_6751" s="T588">ребенок-PL-1SG-ACC</ta>
            <ta e="T590" id="Seg_6752" s="T589">с</ta>
            <ta e="T591" id="Seg_6753" s="T590">мать.[NOM]</ta>
            <ta e="T592" id="Seg_6754" s="T591">мать-3SG.[NOM]</ta>
            <ta e="T593" id="Seg_6755" s="T592">умирать-PTCP.PST-3SG-ACC</ta>
            <ta e="T594" id="Seg_6756" s="T593">после.того</ta>
            <ta e="T595" id="Seg_6757" s="T594">впервые</ta>
            <ta e="T596" id="Seg_6758" s="T595">приходить-PRS.[3SG]</ta>
            <ta e="T597" id="Seg_6759" s="T596">маленький</ta>
            <ta e="T598" id="Seg_6760" s="T597">сын-3SG.[NOM]</ta>
            <ta e="T599" id="Seg_6761" s="T598">тот-3SG-2SG.[NOM]</ta>
            <ta e="T600" id="Seg_6762" s="T599">идти-PRS.[3SG]</ta>
            <ta e="T601" id="Seg_6763" s="T600">тот-ACC</ta>
            <ta e="T602" id="Seg_6764" s="T601">провожать-RECP/COLL-PRS-3PL</ta>
            <ta e="T603" id="Seg_6765" s="T602">тот</ta>
            <ta e="T604" id="Seg_6766" s="T603">тот-ACC</ta>
            <ta e="T605" id="Seg_6767" s="T604">идти-EP-NEG-IMP.2PL</ta>
            <ta e="T606" id="Seg_6768" s="T605">говорить-PRS-1SG</ta>
            <ta e="T607" id="Seg_6769" s="T606">слышать-EP-NEG-3PL</ta>
            <ta e="T608" id="Seg_6770" s="T607">EMPH</ta>
            <ta e="T609" id="Seg_6771" s="T608">что-ACC</ta>
            <ta e="T610" id="Seg_6772" s="T609">делать-CVB.SIM</ta>
            <ta e="T611" id="Seg_6773" s="T610">идти-PTCP.PRS-3PL.[NOM]</ta>
            <ta e="T612" id="Seg_6774" s="T611">Q</ta>
            <ta e="T613" id="Seg_6775" s="T612">грех.[NOM]</ta>
            <ta e="T614" id="Seg_6776" s="T613">EMPH</ta>
            <ta e="T615" id="Seg_6777" s="T614">теперь</ta>
            <ta e="T616" id="Seg_6778" s="T615">человек.[NOM]</ta>
            <ta e="T617" id="Seg_6779" s="T616">место.[NOM]</ta>
            <ta e="T618" id="Seg_6780" s="T617">каждый</ta>
            <ta e="T619" id="Seg_6781" s="T618">умирать-CVB.SIM</ta>
            <ta e="T620" id="Seg_6782" s="T619">лежать-PRS.[3SG]</ta>
            <ta e="T621" id="Seg_6783" s="T620">человек.[NOM]</ta>
            <ta e="T622" id="Seg_6784" s="T621">бояться-PTCP.FUT.[NOM]</ta>
            <ta e="T623" id="Seg_6785" s="T622">теперь</ta>
            <ta e="T624" id="Seg_6786" s="T623">там</ta>
            <ta e="T625" id="Seg_6787" s="T624">один</ta>
            <ta e="T626" id="Seg_6788" s="T625">старуха.[NOM]</ta>
            <ta e="T627" id="Seg_6789" s="T626">умирать-PST1-3SG</ta>
            <ta e="T628" id="Seg_6790" s="T627">теперь</ta>
            <ta e="T629" id="Seg_6791" s="T628">один</ta>
            <ta e="T630" id="Seg_6792" s="T629">ребенок.[NOM]</ta>
            <ta e="T631" id="Seg_6793" s="T630">приходить-CVB.SEQ</ta>
            <ta e="T632" id="Seg_6794" s="T631">идти-CVB.SEQ</ta>
            <ta e="T633" id="Seg_6795" s="T632">умирать-PST2.[3SG]</ta>
            <ta e="T634" id="Seg_6796" s="T633">тоже</ta>
            <ta e="T635" id="Seg_6797" s="T634">дорогой-PROPR</ta>
            <ta e="T636" id="Seg_6798" s="T635">ребенок-1PL.[NOM]</ta>
            <ta e="T637" id="Seg_6799" s="T636">очень</ta>
            <ta e="T638" id="Seg_6800" s="T637">хороший.[NOM]</ta>
            <ta e="T639" id="Seg_6801" s="T638">ребенок.[NOM]</ta>
            <ta e="T640" id="Seg_6802" s="T639">быть-PST1-3SG</ta>
            <ta e="T641" id="Seg_6803" s="T640">буран-EP-INSTR</ta>
            <ta e="T642" id="Seg_6804" s="T641">таскать-VBZ-CVB.SIM</ta>
            <ta e="T643" id="Seg_6805" s="T642">идти-PTCP.PRS</ta>
            <ta e="T644" id="Seg_6806" s="T643">быть-PST1-3SG</ta>
            <ta e="T662" id="Seg_6807" s="T661">AFFIRM</ta>
            <ta e="T663" id="Seg_6808" s="T662">этот</ta>
            <ta e="T664" id="Seg_6809" s="T663">табак-PART</ta>
            <ta e="T665" id="Seg_6810" s="T664">бросать-PTCP.FUT-2SG-ACC</ta>
            <ta e="T666" id="Seg_6811" s="T665">надо</ta>
            <ta e="T670" id="Seg_6812" s="T668">спиртное.[NOM]</ta>
            <ta e="T671" id="Seg_6813" s="T670">пить-TEMP-3PL</ta>
            <ta e="T672" id="Seg_6814" s="T671">спиртное-PART</ta>
            <ta e="T673" id="Seg_6815" s="T672">лить-PTCP.FUT-2SG-ACC</ta>
            <ta e="T674" id="Seg_6816" s="T673">надо</ta>
            <ta e="T676" id="Seg_6817" s="T675">последний-3SG.[NOM]</ta>
            <ta e="T688" id="Seg_6818" s="T687">редко</ta>
            <ta e="T689" id="Seg_6819" s="T688">редко</ta>
            <ta e="T690" id="Seg_6820" s="T689">идти-PRS-3PL</ta>
            <ta e="T691" id="Seg_6821" s="T690">отец-3PL-DAT/LOC</ta>
            <ta e="T692" id="Seg_6822" s="T691">могила-PL-DAT/LOC</ta>
            <ta e="T693" id="Seg_6823" s="T692">мать-PL-EP-1SG.[NOM]</ta>
            <ta e="T694" id="Seg_6824" s="T693">могила-3PL-DAT/LOC</ta>
            <ta e="T695" id="Seg_6825" s="T694">идти-HAB-3PL</ta>
            <ta e="T696" id="Seg_6826" s="T695">так</ta>
            <ta e="T697" id="Seg_6827" s="T696">EMPH</ta>
            <ta e="T698" id="Seg_6828" s="T697">такой</ta>
            <ta e="T699" id="Seg_6829" s="T698">есть</ta>
            <ta e="T700" id="Seg_6830" s="T699">шаман-PL.[NOM]</ta>
            <ta e="T701" id="Seg_6831" s="T700">могила-3PL.[NOM]</ta>
            <ta e="T702" id="Seg_6832" s="T701">есть-3PL</ta>
            <ta e="T703" id="Seg_6833" s="T702">тот</ta>
            <ta e="T704" id="Seg_6834" s="T703">плохой.[NOM]</ta>
            <ta e="T705" id="Seg_6835" s="T704">EMPH</ta>
            <ta e="T706" id="Seg_6836" s="T705">шаман-PL-EP-2SG.[NOM]</ta>
            <ta e="T707" id="Seg_6837" s="T706">грех.[NOM]</ta>
            <ta e="T708" id="Seg_6838" s="T707">быть-HAB.[3SG]</ta>
            <ta e="T709" id="Seg_6839" s="T708">слышать-EP-NEG-3PL</ta>
            <ta e="T710" id="Seg_6840" s="T709">теперь</ta>
            <ta e="T711" id="Seg_6841" s="T710">молодежь-PL.[NOM]</ta>
            <ta e="T712" id="Seg_6842" s="T711">совсем</ta>
            <ta e="T716" id="Seg_6843" s="T715">совсем</ta>
            <ta e="T717" id="Seg_6844" s="T716">другой</ta>
            <ta e="T719" id="Seg_6845" s="T717">народ.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc-KiPP">
            <ta e="T2" id="Seg_6846" s="T1">que-pro:case</ta>
            <ta e="T3" id="Seg_6847" s="T2">n.[n:case]</ta>
            <ta e="T4" id="Seg_6848" s="T3">v-v:tense-v:pred.pn</ta>
            <ta e="T0" id="Seg_6849" s="T4">ptcl</ta>
            <ta e="T6" id="Seg_6850" s="T5">n.[n:case]</ta>
            <ta e="T7" id="Seg_6851" s="T6">ptcl</ta>
            <ta e="T8" id="Seg_6852" s="T7">adv</ta>
            <ta e="T9" id="Seg_6853" s="T8">n-n:(num).[n:case]</ta>
            <ta e="T10" id="Seg_6854" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_6855" s="T10">v-v:mood-v:pred.pn</ta>
            <ta e="T12" id="Seg_6856" s="T11">n-n:(num).[n:case]</ta>
            <ta e="T13" id="Seg_6857" s="T12">dempro-pro:(num).[pro:case]</ta>
            <ta e="T14" id="Seg_6858" s="T13">n-n:poss-n:case</ta>
            <ta e="T15" id="Seg_6859" s="T14">v-v:ptcp-v:(case)</ta>
            <ta e="T16" id="Seg_6860" s="T15">v-v:(neg).[v:pred.pn]</ta>
            <ta e="T17" id="Seg_6861" s="T16">adj.[n:case]</ta>
            <ta e="T18" id="Seg_6862" s="T17">v-v:cvb</ta>
            <ta e="T19" id="Seg_6863" s="T18">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T20" id="Seg_6864" s="T19">n.[n:case]</ta>
            <ta e="T21" id="Seg_6865" s="T20">v-v:mood.[v:pred.pn]</ta>
            <ta e="T22" id="Seg_6866" s="T21">dempro</ta>
            <ta e="T23" id="Seg_6867" s="T22">n-n:(poss).[n:case]</ta>
            <ta e="T24" id="Seg_6868" s="T23">n.[n:case]</ta>
            <ta e="T25" id="Seg_6869" s="T24">n.[n:case]</ta>
            <ta e="T26" id="Seg_6870" s="T25">ptcl</ta>
            <ta e="T27" id="Seg_6871" s="T26">n.[n:case]</ta>
            <ta e="T28" id="Seg_6872" s="T27">n.[n:case]</ta>
            <ta e="T29" id="Seg_6873" s="T28">n-n:poss-n:case</ta>
            <ta e="T30" id="Seg_6874" s="T29">v-v:ptcp.[v:(case)]</ta>
            <ta e="T31" id="Seg_6875" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_6876" s="T31">n.[n:case]</ta>
            <ta e="T33" id="Seg_6877" s="T32">adj.[n:case]</ta>
            <ta e="T34" id="Seg_6878" s="T33">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T35" id="Seg_6879" s="T34">n.[n:case]</ta>
            <ta e="T36" id="Seg_6880" s="T35">n.[n:case]</ta>
            <ta e="T37" id="Seg_6881" s="T36">adj-adj&gt;adv</ta>
            <ta e="T38" id="Seg_6882" s="T37">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T39" id="Seg_6883" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_6884" s="T39">n.[n:case]</ta>
            <ta e="T41" id="Seg_6885" s="T40">n-n:(poss)</ta>
            <ta e="T42" id="Seg_6886" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_6887" s="T42">v-v:ptcp.[v:(case)]</ta>
            <ta e="T44" id="Seg_6888" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_6889" s="T44">n.[n:case]</ta>
            <ta e="T46" id="Seg_6890" s="T45">adv</ta>
            <ta e="T47" id="Seg_6891" s="T46">n.[n:case]</ta>
            <ta e="T48" id="Seg_6892" s="T47">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T49" id="Seg_6893" s="T48">ptcl</ta>
            <ta e="T50" id="Seg_6894" s="T49">n.[n:case]</ta>
            <ta e="T51" id="Seg_6895" s="T50">n.[n:case]</ta>
            <ta e="T52" id="Seg_6896" s="T51">n.[n:case]</ta>
            <ta e="T53" id="Seg_6897" s="T52">post</ta>
            <ta e="T55" id="Seg_6898" s="T54">adv</ta>
            <ta e="T56" id="Seg_6899" s="T55">adj-adj&gt;adj-n:(poss).[n:case]</ta>
            <ta e="T57" id="Seg_6900" s="T56">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T58" id="Seg_6901" s="T57">dempro.[pro:case]</ta>
            <ta e="T59" id="Seg_6902" s="T58">n.[n:case]</ta>
            <ta e="T60" id="Seg_6903" s="T59">ptcl</ta>
            <ta e="T61" id="Seg_6904" s="T60">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T68" id="Seg_6905" s="T66">interj</ta>
            <ta e="T69" id="Seg_6906" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_6907" s="T69">adj-n:(poss).[n:case]</ta>
            <ta e="T71" id="Seg_6908" s="T70">n.[n:case]</ta>
            <ta e="T72" id="Seg_6909" s="T71">n-n:case</ta>
            <ta e="T73" id="Seg_6910" s="T72">post</ta>
            <ta e="T74" id="Seg_6911" s="T73">adj.[n:case]</ta>
            <ta e="T75" id="Seg_6912" s="T74">dempro</ta>
            <ta e="T76" id="Seg_6913" s="T75">n.[n:case]</ta>
            <ta e="T77" id="Seg_6914" s="T76">n.[n:case]</ta>
            <ta e="T78" id="Seg_6915" s="T77">post</ta>
            <ta e="T79" id="Seg_6916" s="T78">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T80" id="Seg_6917" s="T79">ptcl</ta>
            <ta e="T81" id="Seg_6918" s="T80">n-n&gt;adj.[n:case]</ta>
            <ta e="T82" id="Seg_6919" s="T81">n-n&gt;adj.[n:case]</ta>
            <ta e="T83" id="Seg_6920" s="T82">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T84" id="Seg_6921" s="T83">ptcl</ta>
            <ta e="T85" id="Seg_6922" s="T84">n-n:case</ta>
            <ta e="T86" id="Seg_6923" s="T85">ptcl</ta>
            <ta e="T87" id="Seg_6924" s="T86">v-v:(ins)-v:cvb</ta>
            <ta e="T88" id="Seg_6925" s="T87">adv</ta>
            <ta e="T89" id="Seg_6926" s="T88">v-v:ptcp-v:(case)</ta>
            <ta e="T90" id="Seg_6927" s="T89">ptcl</ta>
            <ta e="T723" id="Seg_6928" s="T90">n-n:case</ta>
            <ta e="T92" id="Seg_6929" s="T91">dempro.[pro:case]</ta>
            <ta e="T93" id="Seg_6930" s="T92">dempro-pro:(num).[pro:case]</ta>
            <ta e="T94" id="Seg_6931" s="T93">adv</ta>
            <ta e="T95" id="Seg_6932" s="T94">n-n:case</ta>
            <ta e="T96" id="Seg_6933" s="T95">v-v:ptcp-v:(ins)-v:(poss).[v:(case)]</ta>
            <ta e="T97" id="Seg_6934" s="T96">post</ta>
            <ta e="T98" id="Seg_6935" s="T97">n.[n:case]</ta>
            <ta e="T99" id="Seg_6936" s="T98">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T100" id="Seg_6937" s="T99">n-n:case</ta>
            <ta e="T101" id="Seg_6938" s="T100">adv</ta>
            <ta e="T102" id="Seg_6939" s="T101">v-v:tense-v:pred.pn</ta>
            <ta e="T103" id="Seg_6940" s="T102">n-n:poss-n:case</ta>
            <ta e="T104" id="Seg_6941" s="T103">n-n:case</ta>
            <ta e="T105" id="Seg_6942" s="T104">v-v:ptcp</ta>
            <ta e="T724" id="Seg_6943" s="T105">n.[n:case]</ta>
            <ta e="T107" id="Seg_6944" s="T106">n.[n:case]</ta>
            <ta e="T108" id="Seg_6945" s="T107">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T109" id="Seg_6946" s="T108">adv</ta>
            <ta e="T110" id="Seg_6947" s="T109">n-n:poss-n:case</ta>
            <ta e="T111" id="Seg_6948" s="T110">v-v:mood-v:temp.pn</ta>
            <ta e="T112" id="Seg_6949" s="T111">n.[n:case]</ta>
            <ta e="T113" id="Seg_6950" s="T112">v-v:tense.[v:pred.pn]</ta>
            <ta e="T114" id="Seg_6951" s="T113">dempro</ta>
            <ta e="T725" id="Seg_6952" s="T114">n.[n:case]</ta>
            <ta e="T116" id="Seg_6953" s="T115">dempro</ta>
            <ta e="T117" id="Seg_6954" s="T116">n-n:(num).[n:case]</ta>
            <ta e="T118" id="Seg_6955" s="T117">dempro</ta>
            <ta e="T119" id="Seg_6956" s="T118">ptcl</ta>
            <ta e="T120" id="Seg_6957" s="T119">n-n:case</ta>
            <ta e="T121" id="Seg_6958" s="T120">v-v:tense-v:pred.pn</ta>
            <ta e="T122" id="Seg_6959" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_6960" s="T122">n-n:case</ta>
            <ta e="T124" id="Seg_6961" s="T123">v.[v:mood.pn]</ta>
            <ta e="T125" id="Seg_6962" s="T124">conj</ta>
            <ta e="T126" id="Seg_6963" s="T125">v-v:(ins)-v:(neg).[v:mood.pn]</ta>
            <ta e="T127" id="Seg_6964" s="T126">conj</ta>
            <ta e="T128" id="Seg_6965" s="T127">v-v:tense-v:pred.pn</ta>
            <ta e="T129" id="Seg_6966" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_6967" s="T129">cardnum</ta>
            <ta e="T131" id="Seg_6968" s="T130">v-v:cvb-v-v:cvb</ta>
            <ta e="T132" id="Seg_6969" s="T131">v-v:cvb</ta>
            <ta e="T133" id="Seg_6970" s="T132">v-v:tense-v:poss.pn</ta>
            <ta e="T134" id="Seg_6971" s="T133">dempro-pro:case</ta>
            <ta e="T135" id="Seg_6972" s="T134">dempro.[pro:case]</ta>
            <ta e="T136" id="Seg_6973" s="T135">post</ta>
            <ta e="T137" id="Seg_6974" s="T136">pers.[pro:case]</ta>
            <ta e="T138" id="Seg_6975" s="T137">v-v:cvb</ta>
            <ta e="T139" id="Seg_6976" s="T138">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T140" id="Seg_6977" s="T139">n-n:case</ta>
            <ta e="T141" id="Seg_6978" s="T140">v-v:cvb-v-v:cvb-v:pred.pn</ta>
            <ta e="T142" id="Seg_6979" s="T141">cardnum</ta>
            <ta e="T143" id="Seg_6980" s="T142">cardnum</ta>
            <ta e="T144" id="Seg_6981" s="T143">n-n&gt;adj-n:poss-n:case</ta>
            <ta e="T145" id="Seg_6982" s="T144">ptcl</ta>
            <ta e="T146" id="Seg_6983" s="T145">que.[pro:case]</ta>
            <ta e="T147" id="Seg_6984" s="T146">n-n:(poss).[n:case]</ta>
            <ta e="T148" id="Seg_6985" s="T147">n-n:case</ta>
            <ta e="T149" id="Seg_6986" s="T148">v-v:tense.[v:poss.pn]=ptcl</ta>
            <ta e="T150" id="Seg_6987" s="T149">dempro</ta>
            <ta e="T151" id="Seg_6988" s="T150">cardnum</ta>
            <ta e="T152" id="Seg_6989" s="T151">cardnum</ta>
            <ta e="T153" id="Seg_6990" s="T152">n-n&gt;adj-n:case</ta>
            <ta e="T154" id="Seg_6991" s="T153">v-v:tense-v:poss.pn</ta>
            <ta e="T157" id="Seg_6992" s="T156">adv</ta>
            <ta e="T158" id="Seg_6993" s="T157">adj.[n:case]</ta>
            <ta e="T159" id="Seg_6994" s="T158">adj.[n:case]</ta>
            <ta e="T160" id="Seg_6995" s="T159">v-v:tense-v:poss.pn</ta>
            <ta e="T161" id="Seg_6996" s="T160">ptcl</ta>
            <ta e="T162" id="Seg_6997" s="T161">n-n:(poss)</ta>
            <ta e="T163" id="Seg_6998" s="T162">ptcl</ta>
            <ta e="T164" id="Seg_6999" s="T163">ptcl</ta>
            <ta e="T165" id="Seg_7000" s="T164">v-v:tense-v:poss.pn</ta>
            <ta e="T166" id="Seg_7001" s="T165">adv</ta>
            <ta e="T167" id="Seg_7002" s="T166">dempro</ta>
            <ta e="T168" id="Seg_7003" s="T167">v-v:tense-v:poss.pn</ta>
            <ta e="T169" id="Seg_7004" s="T168">adj</ta>
            <ta e="T170" id="Seg_7005" s="T169">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T171" id="Seg_7006" s="T170">adv</ta>
            <ta e="T172" id="Seg_7007" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_7008" s="T172">dempro</ta>
            <ta e="T174" id="Seg_7009" s="T173">dempro</ta>
            <ta e="T175" id="Seg_7010" s="T174">n-n:(poss).[n:case]</ta>
            <ta e="T176" id="Seg_7011" s="T175">cardnum</ta>
            <ta e="T177" id="Seg_7012" s="T176">cardnum</ta>
            <ta e="T178" id="Seg_7013" s="T177">cardnum</ta>
            <ta e="T179" id="Seg_7014" s="T178">cardnum</ta>
            <ta e="T180" id="Seg_7015" s="T179">n-n&gt;adj-n:poss-n:case</ta>
            <ta e="T181" id="Seg_7016" s="T180">v-v:tense.[v:pred.pn]</ta>
            <ta e="T182" id="Seg_7017" s="T181">n.[n:case]</ta>
            <ta e="T183" id="Seg_7018" s="T182">dempro.[pro:case]</ta>
            <ta e="T184" id="Seg_7019" s="T183">ptcl</ta>
            <ta e="T185" id="Seg_7020" s="T184">adv</ta>
            <ta e="T186" id="Seg_7021" s="T185">v-v:cvb</ta>
            <ta e="T187" id="Seg_7022" s="T186">v-v:cvb-v:pred.pn</ta>
            <ta e="T188" id="Seg_7023" s="T187">dempro.[pro:case]</ta>
            <ta e="T189" id="Seg_7024" s="T188">adv</ta>
            <ta e="T190" id="Seg_7025" s="T189">adv</ta>
            <ta e="T191" id="Seg_7026" s="T190">adv</ta>
            <ta e="T192" id="Seg_7027" s="T191">ptcl</ta>
            <ta e="T194" id="Seg_7028" s="T193">n-n&gt;v-v:cvb</ta>
            <ta e="T195" id="Seg_7029" s="T194">conj</ta>
            <ta e="T196" id="Seg_7030" s="T195">n-n&gt;v-v:cvb</ta>
            <ta e="T197" id="Seg_7031" s="T196">v-v:tense-v:pred.pn</ta>
            <ta e="T198" id="Seg_7032" s="T197">ptcl</ta>
            <ta e="T203" id="Seg_7033" s="T202">adv</ta>
            <ta e="T204" id="Seg_7034" s="T203">adj</ta>
            <ta e="T205" id="Seg_7035" s="T204">n.[n:case]</ta>
            <ta e="T206" id="Seg_7036" s="T205">v-v:tense-v:poss.pn</ta>
            <ta e="T207" id="Seg_7037" s="T206">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T208" id="Seg_7038" s="T207">dempro</ta>
            <ta e="T209" id="Seg_7039" s="T208">v-v:ptcp</ta>
            <ta e="T210" id="Seg_7040" s="T209">n.[n:case]</ta>
            <ta e="T726" id="Seg_7041" s="T210">dempro.[pro:case]</ta>
            <ta e="T212" id="Seg_7042" s="T211">dempro</ta>
            <ta e="T213" id="Seg_7043" s="T212">pers.[pro:case]</ta>
            <ta e="T214" id="Seg_7044" s="T213">cardnum</ta>
            <ta e="T215" id="Seg_7045" s="T214">n-n&gt;adj.[n:case]</ta>
            <ta e="T216" id="Seg_7046" s="T215">v-v:tense-v:mood-v:(ins)-v:poss.pn</ta>
            <ta e="T217" id="Seg_7047" s="T216">ptcl</ta>
            <ta e="T218" id="Seg_7048" s="T217">adv</ta>
            <ta e="T219" id="Seg_7049" s="T218">adv</ta>
            <ta e="T220" id="Seg_7050" s="T219">conj</ta>
            <ta e="T221" id="Seg_7051" s="T220">adj.[pro:case]</ta>
            <ta e="T222" id="Seg_7052" s="T221">v-v:neg-v:pred.pn</ta>
            <ta e="T223" id="Seg_7053" s="T222">adv</ta>
            <ta e="T224" id="Seg_7054" s="T223">ptcl</ta>
            <ta e="T225" id="Seg_7055" s="T224">dempro</ta>
            <ta e="T226" id="Seg_7056" s="T225">n-n&gt;adj.[n:case]</ta>
            <ta e="T227" id="Seg_7057" s="T226">v-v:tense-v:poss.pn</ta>
            <ta e="T228" id="Seg_7058" s="T227">adv</ta>
            <ta e="T229" id="Seg_7059" s="T228">ptcl</ta>
            <ta e="T230" id="Seg_7060" s="T229">n.[n:case]</ta>
            <ta e="T231" id="Seg_7061" s="T230">v-v:tense-v:pred.pn</ta>
            <ta e="T232" id="Seg_7062" s="T231">n-n:(num)-n:case</ta>
            <ta e="T233" id="Seg_7063" s="T232">v-v&gt;v-v:(ins)-v:mood.pn</ta>
            <ta e="T727" id="Seg_7064" s="T233">v-v:mood-v:pred.pn</ta>
            <ta e="T235" id="Seg_7065" s="T234">n.[n:case]</ta>
            <ta e="T236" id="Seg_7066" s="T235">n-n:(poss).[n:case]</ta>
            <ta e="T237" id="Seg_7067" s="T236">n.[n:case]</ta>
            <ta e="T238" id="Seg_7068" s="T237">v-v:tense.[v:poss.pn]</ta>
            <ta e="T239" id="Seg_7069" s="T238">n-n:(poss).[n:case]</ta>
            <ta e="T240" id="Seg_7070" s="T239">v-v&gt;v-v:(ins)-v:mood.pn</ta>
            <ta e="T241" id="Seg_7071" s="T240">v-v:mood-v:pred.pn</ta>
            <ta e="T242" id="Seg_7072" s="T241">adj-adj&gt;adv</ta>
            <ta e="T243" id="Seg_7073" s="T242">v-v:ptcp</ta>
            <ta e="T244" id="Seg_7074" s="T243">n-n:case</ta>
            <ta e="T245" id="Seg_7075" s="T244">adj-adj&gt;adv</ta>
            <ta e="T246" id="Seg_7076" s="T245">v-v:(ins)-v:mood.pn</ta>
            <ta e="T247" id="Seg_7077" s="T246">v-v:mood-v:pred.pn</ta>
            <ta e="T248" id="Seg_7078" s="T247">adv</ta>
            <ta e="T249" id="Seg_7079" s="T248">que</ta>
            <ta e="T250" id="Seg_7080" s="T249">v-v:tense.[v:pred.pn]</ta>
            <ta e="T251" id="Seg_7081" s="T250">v-v:mood-v:temp.pn</ta>
            <ta e="T252" id="Seg_7082" s="T251">n.[n:case]</ta>
            <ta e="T253" id="Seg_7083" s="T252">v-v:tense.[v:poss.pn]</ta>
            <ta e="T254" id="Seg_7084" s="T253">adv</ta>
            <ta e="T255" id="Seg_7085" s="T254">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T256" id="Seg_7086" s="T255">dempro</ta>
            <ta e="T257" id="Seg_7087" s="T256">adj-adj&gt;adv</ta>
            <ta e="T258" id="Seg_7088" s="T257">v-v:mood-v:temp.pn</ta>
            <ta e="T259" id="Seg_7089" s="T258">adv</ta>
            <ta e="T260" id="Seg_7090" s="T259">emphpro-pro:case-pro:case</ta>
            <ta e="T261" id="Seg_7091" s="T260">v-v:tense-v:poss.pn</ta>
            <ta e="T262" id="Seg_7092" s="T261">v-v:mood-v:pred.pn</ta>
            <ta e="T263" id="Seg_7093" s="T262">n-n:(num).[n:case]</ta>
            <ta e="T264" id="Seg_7094" s="T263">n-n:(num).[n:case]</ta>
            <ta e="T265" id="Seg_7095" s="T264">dempro</ta>
            <ta e="T266" id="Seg_7096" s="T265">adv</ta>
            <ta e="T267" id="Seg_7097" s="T266">adj.[n:case]</ta>
            <ta e="T268" id="Seg_7098" s="T267">v-v:tense-v:poss.pn</ta>
            <ta e="T269" id="Seg_7099" s="T268">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T270" id="Seg_7100" s="T269">adv</ta>
            <ta e="T271" id="Seg_7101" s="T270">adj.[n:case]</ta>
            <ta e="T272" id="Seg_7102" s="T271">v-v:tense-v:poss.pn</ta>
            <ta e="T273" id="Seg_7103" s="T272">v-v:tense-v:poss.pn</ta>
            <ta e="T274" id="Seg_7104" s="T273">pers.[pro:case]</ta>
            <ta e="T275" id="Seg_7105" s="T274">adv</ta>
            <ta e="T276" id="Seg_7106" s="T275">adj-n:(pred.pn)</ta>
            <ta e="T277" id="Seg_7107" s="T276">dempro</ta>
            <ta e="T278" id="Seg_7108" s="T277">n.[n:case]</ta>
            <ta e="T279" id="Seg_7109" s="T278">v-v:tense-v:poss.pn</ta>
            <ta e="T280" id="Seg_7110" s="T279">ptcl</ta>
            <ta e="T281" id="Seg_7111" s="T280">adv</ta>
            <ta e="T282" id="Seg_7112" s="T281">n-n:(poss).[n:case]</ta>
            <ta e="T283" id="Seg_7113" s="T282">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T284" id="Seg_7114" s="T283">n-n:case</ta>
            <ta e="T285" id="Seg_7115" s="T284">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T286" id="Seg_7116" s="T285">post</ta>
            <ta e="T287" id="Seg_7117" s="T286">interj</ta>
            <ta e="T288" id="Seg_7118" s="T287">n-n:case</ta>
            <ta e="T289" id="Seg_7119" s="T288">v-v:cvb=ptcl-ptcl:(temp.pn)</ta>
            <ta e="T290" id="Seg_7120" s="T289">n-n:case</ta>
            <ta e="T291" id="Seg_7121" s="T290">n-n:case</ta>
            <ta e="T292" id="Seg_7122" s="T291">v-v:mood-v:pred.pn</ta>
            <ta e="T293" id="Seg_7123" s="T292">adv</ta>
            <ta e="T294" id="Seg_7124" s="T293">n-n:(poss).[n:case]</ta>
            <ta e="T295" id="Seg_7125" s="T294">n-n:(poss).[n:case]</ta>
            <ta e="T296" id="Seg_7126" s="T295">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T297" id="Seg_7127" s="T296">adv</ta>
            <ta e="T298" id="Seg_7128" s="T297">v-v:tense.[v:pred.pn]</ta>
            <ta e="T299" id="Seg_7129" s="T298">ptcl</ta>
            <ta e="T300" id="Seg_7130" s="T299">interj</ta>
            <ta e="T301" id="Seg_7131" s="T300">n-n:(num)-n:case</ta>
            <ta e="T302" id="Seg_7132" s="T301">v-v:(ins)-v&gt;v-v:(tense).[v:mood.pn]</ta>
            <ta e="T303" id="Seg_7133" s="T302">ptcl</ta>
            <ta e="T304" id="Seg_7134" s="T303">adj-adj&gt;adv</ta>
            <ta e="T305" id="Seg_7135" s="T304">v-v:ptcp-v:(case)</ta>
            <ta e="T306" id="Seg_7136" s="T305">adj-adj&gt;adv</ta>
            <ta e="T307" id="Seg_7137" s="T306">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T308" id="Seg_7138" s="T307">ptcl</ta>
            <ta e="T309" id="Seg_7139" s="T308">adv</ta>
            <ta e="T310" id="Seg_7140" s="T309">adj</ta>
            <ta e="T311" id="Seg_7141" s="T310">adj</ta>
            <ta e="T312" id="Seg_7142" s="T311">v-v:tense-v:poss.pn</ta>
            <ta e="T313" id="Seg_7143" s="T312">quant</ta>
            <ta e="T314" id="Seg_7144" s="T313">n-n&gt;adj.[n:case]</ta>
            <ta e="T315" id="Seg_7145" s="T314">v-v:tense-v:poss.pn</ta>
            <ta e="T316" id="Seg_7146" s="T315">adv</ta>
            <ta e="T317" id="Seg_7147" s="T316">n-n:case</ta>
            <ta e="T318" id="Seg_7148" s="T317">v-v:tense-v:poss.pn</ta>
            <ta e="T319" id="Seg_7149" s="T318">v-v:tense-v:poss.pn</ta>
            <ta e="T320" id="Seg_7150" s="T319">adv</ta>
            <ta e="T321" id="Seg_7151" s="T320">v.[v:mood.pn]</ta>
            <ta e="T322" id="Seg_7152" s="T321">adv</ta>
            <ta e="T323" id="Seg_7153" s="T322">pers.[pro:case]</ta>
            <ta e="T324" id="Seg_7154" s="T323">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T325" id="Seg_7155" s="T324">ptcl</ta>
            <ta e="T326" id="Seg_7156" s="T325">cardnum-cardnum-n:case</ta>
            <ta e="T327" id="Seg_7157" s="T326">post</ta>
            <ta e="T328" id="Seg_7158" s="T327">n-n:poss-n:case</ta>
            <ta e="T329" id="Seg_7159" s="T328">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T330" id="Seg_7160" s="T329">n-n:poss-n:case</ta>
            <ta e="T331" id="Seg_7161" s="T330">v-v:(ins)-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T332" id="Seg_7162" s="T331">n-n:(num)-n:case-n:(pred.pn)</ta>
            <ta e="T336" id="Seg_7163" s="T335">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T337" id="Seg_7164" s="T336">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T338" id="Seg_7165" s="T337">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T339" id="Seg_7166" s="T338">propr.[n:case]</ta>
            <ta e="T340" id="Seg_7167" s="T339">adj</ta>
            <ta e="T341" id="Seg_7168" s="T340">n-n:(poss).[n:case]</ta>
            <ta e="T342" id="Seg_7169" s="T341">n-n:(poss).[n:case]</ta>
            <ta e="T343" id="Seg_7170" s="T342">adv</ta>
            <ta e="T344" id="Seg_7171" s="T343">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T345" id="Seg_7172" s="T344">adv</ta>
            <ta e="T346" id="Seg_7173" s="T345">dempro.[pro:case]</ta>
            <ta e="T347" id="Seg_7174" s="T346">post</ta>
            <ta e="T348" id="Seg_7175" s="T347">adv-adv&gt;adj-n:(poss).[n:case]</ta>
            <ta e="T349" id="Seg_7176" s="T348">ptcl</ta>
            <ta e="T350" id="Seg_7177" s="T349">ptcl</ta>
            <ta e="T351" id="Seg_7178" s="T350">interj</ta>
            <ta e="T352" id="Seg_7179" s="T351">adv</ta>
            <ta e="T353" id="Seg_7180" s="T352">ptcl</ta>
            <ta e="T354" id="Seg_7181" s="T353">ptcl</ta>
            <ta e="T355" id="Seg_7182" s="T354">cardnum-cardnum-n:case</ta>
            <ta e="T356" id="Seg_7183" s="T355">post</ta>
            <ta e="T357" id="Seg_7184" s="T356">n-n:(poss).[n:case]</ta>
            <ta e="T358" id="Seg_7185" s="T357">n-n:(num)-n:case</ta>
            <ta e="T359" id="Seg_7186" s="T358">adv</ta>
            <ta e="T360" id="Seg_7187" s="T359">propr.[n:case]</ta>
            <ta e="T728" id="Seg_7188" s="T361">v-v:cvb</ta>
            <ta e="T363" id="Seg_7189" s="T362">propr.[n:case]</ta>
            <ta e="T364" id="Seg_7190" s="T363">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T365" id="Seg_7191" s="T364">ptcl</ta>
            <ta e="T366" id="Seg_7192" s="T365">conj</ta>
            <ta e="T367" id="Seg_7193" s="T366">adj-adj&gt;adv</ta>
            <ta e="T368" id="Seg_7194" s="T367">v-v:tense-v:pred.pn</ta>
            <ta e="T369" id="Seg_7195" s="T368">n-n&gt;adj</ta>
            <ta e="T370" id="Seg_7196" s="T369">n-n:(num).[n:case]</ta>
            <ta e="T371" id="Seg_7197" s="T370">n-n:poss-n:case</ta>
            <ta e="T372" id="Seg_7198" s="T371">n-n:(num).[n:case]</ta>
            <ta e="T373" id="Seg_7199" s="T372">n-n:(poss).[n:case]</ta>
            <ta e="T374" id="Seg_7200" s="T373">adj-n:(num).[n:case]</ta>
            <ta e="T375" id="Seg_7201" s="T374">n-n:(poss).[n:case]</ta>
            <ta e="T376" id="Seg_7202" s="T375">adv</ta>
            <ta e="T377" id="Seg_7203" s="T376">ptcl</ta>
            <ta e="T378" id="Seg_7204" s="T377">v-v:cvb-v:pred.pn</ta>
            <ta e="T379" id="Seg_7205" s="T378">v-v:tense-v:pred.pn</ta>
            <ta e="T380" id="Seg_7206" s="T379">ptcl</ta>
            <ta e="T381" id="Seg_7207" s="T380">ptcl</ta>
            <ta e="T382" id="Seg_7208" s="T381">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T383" id="Seg_7209" s="T382">adv</ta>
            <ta e="T384" id="Seg_7210" s="T383">adj</ta>
            <ta e="T386" id="Seg_7211" s="T384">cardnum</ta>
            <ta e="T388" id="Seg_7212" s="T386">ptcl</ta>
            <ta e="T390" id="Seg_7213" s="T388">cardnum</ta>
            <ta e="T391" id="Seg_7214" s="T390">cardnum-n:case</ta>
            <ta e="T392" id="Seg_7215" s="T391">v-v:tense.[v:pred.pn]</ta>
            <ta e="T395" id="Seg_7216" s="T394">v-v:tense.[v:pred.pn]</ta>
            <ta e="T396" id="Seg_7217" s="T395">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T397" id="Seg_7218" s="T396">v-v:(neg)-v:pred.pn</ta>
            <ta e="T398" id="Seg_7219" s="T397">adv</ta>
            <ta e="T399" id="Seg_7220" s="T398">n-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T400" id="Seg_7221" s="T399">v-v:tense-v:pred.pn</ta>
            <ta e="T401" id="Seg_7222" s="T400">adv</ta>
            <ta e="T402" id="Seg_7223" s="T401">cardnum</ta>
            <ta e="T403" id="Seg_7224" s="T402">ptcl</ta>
            <ta e="T404" id="Seg_7225" s="T403">n-n:case</ta>
            <ta e="T405" id="Seg_7226" s="T404">v-v:cvb</ta>
            <ta e="T406" id="Seg_7227" s="T405">v-v:(neg)-v:pred.pn</ta>
            <ta e="T407" id="Seg_7228" s="T406">ptcl</ta>
            <ta e="T408" id="Seg_7229" s="T407">v-v:tense-v:pred.pn</ta>
            <ta e="T409" id="Seg_7230" s="T408">ptcl</ta>
            <ta e="T423" id="Seg_7231" s="T422">n-n:case</ta>
            <ta e="T424" id="Seg_7232" s="T423">v-v:tense-v:poss.pn</ta>
            <ta e="T430" id="Seg_7233" s="T428">ptcl</ta>
            <ta e="T431" id="Seg_7234" s="T430">n-n:case</ta>
            <ta e="T432" id="Seg_7235" s="T431">v-v:tense-v:poss.pn</ta>
            <ta e="T433" id="Seg_7236" s="T432">n-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T434" id="Seg_7237" s="T433">quant-n:(pred.pn)</ta>
            <ta e="T435" id="Seg_7238" s="T434">n-n:case</ta>
            <ta e="T436" id="Seg_7239" s="T435">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T440" id="Seg_7240" s="T439">n-n:case</ta>
            <ta e="T441" id="Seg_7241" s="T440">ptcl</ta>
            <ta e="T442" id="Seg_7242" s="T441">n-n:case</ta>
            <ta e="T443" id="Seg_7243" s="T442">v-v:tense-v:poss.pn</ta>
            <ta e="T452" id="Seg_7244" s="T451">v-v:mood-v:pred.pn</ta>
            <ta e="T453" id="Seg_7245" s="T452">adj-n:(num).[n:case]</ta>
            <ta e="T454" id="Seg_7246" s="T453">n-n:case</ta>
            <ta e="T455" id="Seg_7247" s="T454">v-v:tense-v:poss.pn</ta>
            <ta e="T457" id="Seg_7248" s="T456">quant</ta>
            <ta e="T458" id="Seg_7249" s="T457">n-n&gt;adj</ta>
            <ta e="T459" id="Seg_7250" s="T458">n.[n:case]</ta>
            <ta e="T460" id="Seg_7251" s="T459">n-n:case</ta>
            <ta e="T461" id="Seg_7252" s="T460">v-v:tense-v:poss.pn</ta>
            <ta e="T462" id="Seg_7253" s="T461">n-n:case</ta>
            <ta e="T463" id="Seg_7254" s="T462">adj-adj&gt;adv</ta>
            <ta e="T464" id="Seg_7255" s="T463">v-v:(neg)-v:pred.pn</ta>
            <ta e="T465" id="Seg_7256" s="T464">n-n:case</ta>
            <ta e="T466" id="Seg_7257" s="T465">v-v:(ins)-v&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T467" id="Seg_7258" s="T466">n.[n:case]</ta>
            <ta e="T468" id="Seg_7259" s="T467">v-v:tense.[v:poss.pn]</ta>
            <ta e="T469" id="Seg_7260" s="T468">dempro.[pro:case]</ta>
            <ta e="T470" id="Seg_7261" s="T469">post</ta>
            <ta e="T471" id="Seg_7262" s="T470">pers.[pro:case]</ta>
            <ta e="T472" id="Seg_7263" s="T471">n-n:case</ta>
            <ta e="T473" id="Seg_7264" s="T472">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T474" id="Seg_7265" s="T473">v-v:ptcp-v:(poss)</ta>
            <ta e="T475" id="Seg_7266" s="T474">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T476" id="Seg_7267" s="T475">n.[n:case]</ta>
            <ta e="T477" id="Seg_7268" s="T476">v-v:tense.[v:poss.pn]</ta>
            <ta e="T478" id="Seg_7269" s="T477">que.[pro:case]</ta>
            <ta e="T479" id="Seg_7270" s="T478">v-v:tense.[v:pred.pn]</ta>
            <ta e="T480" id="Seg_7271" s="T479">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T481" id="Seg_7272" s="T480">v-v:tense-v:pred.pn</ta>
            <ta e="T482" id="Seg_7273" s="T481">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T483" id="Seg_7274" s="T482">v-v:(ins)-v&gt;v-v:ptcp</ta>
            <ta e="T484" id="Seg_7275" s="T483">v-v:mood-v:temp.pn</ta>
            <ta e="T485" id="Seg_7276" s="T484">adv</ta>
            <ta e="T486" id="Seg_7277" s="T485">v-v:(ins)-v&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T487" id="Seg_7278" s="T486">propr-n:(poss).[n:case]</ta>
            <ta e="T488" id="Seg_7279" s="T487">ptcl</ta>
            <ta e="T489" id="Seg_7280" s="T488">v-v:tense.[v:pred.pn]</ta>
            <ta e="T491" id="Seg_7281" s="T490">adj.[n:case]</ta>
            <ta e="T492" id="Seg_7282" s="T491">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T493" id="Seg_7283" s="T492">adv</ta>
            <ta e="T494" id="Seg_7284" s="T493">adj.[n:case]</ta>
            <ta e="T495" id="Seg_7285" s="T494">n.[n:case]</ta>
            <ta e="T496" id="Seg_7286" s="T495">adj.[n:case]</ta>
            <ta e="T497" id="Seg_7287" s="T496">n-n:(num).[n:case]</ta>
            <ta e="T498" id="Seg_7288" s="T497">n-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T499" id="Seg_7289" s="T498">ptcl</ta>
            <ta e="T500" id="Seg_7290" s="T499">ptcl</ta>
            <ta e="T501" id="Seg_7291" s="T500">dempro</ta>
            <ta e="T502" id="Seg_7292" s="T501">que</ta>
            <ta e="T503" id="Seg_7293" s="T502">conj</ta>
            <ta e="T504" id="Seg_7294" s="T503">v-v:mood.pn</ta>
            <ta e="T505" id="Seg_7295" s="T504">pers.[pro:case]</ta>
            <ta e="T506" id="Seg_7296" s="T505">v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T507" id="Seg_7297" s="T506">pers-pro:case</ta>
            <ta e="T508" id="Seg_7298" s="T507">v-v&gt;v-v:mood.pn</ta>
            <ta e="T509" id="Seg_7299" s="T508">conj</ta>
            <ta e="T510" id="Seg_7300" s="T509">v-v&gt;v-v:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T511" id="Seg_7301" s="T510">conj</ta>
            <ta e="T512" id="Seg_7302" s="T511">pers-pro:case</ta>
            <ta e="T513" id="Seg_7303" s="T512">n-n:(poss)</ta>
            <ta e="T514" id="Seg_7304" s="T513">ptcl</ta>
            <ta e="T515" id="Seg_7305" s="T514">adv</ta>
            <ta e="T516" id="Seg_7306" s="T515">n-n:(num).[n:case]</ta>
            <ta e="T517" id="Seg_7307" s="T516">ptcl</ta>
            <ta e="T518" id="Seg_7308" s="T517">que-pro:case</ta>
            <ta e="T519" id="Seg_7309" s="T518">v-v:tense-v:poss.pn=ptcl</ta>
            <ta e="T520" id="Seg_7310" s="T519">dempro-pro:case</ta>
            <ta e="T521" id="Seg_7311" s="T520">pers.[pro:case]</ta>
            <ta e="T522" id="Seg_7312" s="T521">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T523" id="Seg_7313" s="T522">v-v:tense-v:poss.pn</ta>
            <ta e="T524" id="Seg_7314" s="T523">pers.[pro:case]</ta>
            <ta e="T525" id="Seg_7315" s="T524">n-n:poss-n:case</ta>
            <ta e="T526" id="Seg_7316" s="T525">v-v:(neg)-v:pred.pn</ta>
            <ta e="T527" id="Seg_7317" s="T526">pers.[pro:case]</ta>
            <ta e="T528" id="Seg_7318" s="T527">v-v:mood.pn</ta>
            <ta e="T532" id="Seg_7319" s="T531">adj-n:(poss).[n:case]</ta>
            <ta e="T533" id="Seg_7320" s="T532">adv</ta>
            <ta e="T534" id="Seg_7321" s="T533">ptcl</ta>
            <ta e="T535" id="Seg_7322" s="T534">que-pro:case</ta>
            <ta e="T536" id="Seg_7323" s="T535">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T552" id="Seg_7324" s="T550">ptcl</ta>
            <ta e="T571" id="Seg_7325" s="T570">v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T572" id="Seg_7326" s="T571">adv</ta>
            <ta e="T573" id="Seg_7327" s="T572">dempro.[pro:case]</ta>
            <ta e="T574" id="Seg_7328" s="T573">v-v:cvb</ta>
            <ta e="T575" id="Seg_7329" s="T574">v-v:tense-v:pred.pn</ta>
            <ta e="T576" id="Seg_7330" s="T575">dempro</ta>
            <ta e="T577" id="Seg_7331" s="T576">dempro</ta>
            <ta e="T578" id="Seg_7332" s="T577">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T579" id="Seg_7333" s="T578">n-n:poss-n:case</ta>
            <ta e="T580" id="Seg_7334" s="T579">interj</ta>
            <ta e="T581" id="Seg_7335" s="T580">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T582" id="Seg_7336" s="T581">n-n:(ins)-n:poss-n:case</ta>
            <ta e="T583" id="Seg_7337" s="T582">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T584" id="Seg_7338" s="T583">dempro</ta>
            <ta e="T585" id="Seg_7339" s="T584">v-v:mood-v:pred.pn</ta>
            <ta e="T587" id="Seg_7340" s="T586">n-n:(num)-n:case</ta>
            <ta e="T588" id="Seg_7341" s="T587">pers.[pro:case]</ta>
            <ta e="T589" id="Seg_7342" s="T588">n-n:(num)-n:poss-n:case</ta>
            <ta e="T590" id="Seg_7343" s="T589">post</ta>
            <ta e="T591" id="Seg_7344" s="T590">n.[n:case]</ta>
            <ta e="T592" id="Seg_7345" s="T591">n-n:(poss).[n:case]</ta>
            <ta e="T593" id="Seg_7346" s="T592">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T594" id="Seg_7347" s="T593">post</ta>
            <ta e="T595" id="Seg_7348" s="T594">adv</ta>
            <ta e="T596" id="Seg_7349" s="T595">v-v:tense.[v:pred.pn]</ta>
            <ta e="T597" id="Seg_7350" s="T596">adj</ta>
            <ta e="T598" id="Seg_7351" s="T597">n-n:(poss).[n:case]</ta>
            <ta e="T599" id="Seg_7352" s="T598">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T600" id="Seg_7353" s="T599">v-v:tense.[v:pred.pn]</ta>
            <ta e="T601" id="Seg_7354" s="T600">dempro-pro:case</ta>
            <ta e="T602" id="Seg_7355" s="T601">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T603" id="Seg_7356" s="T602">dempro</ta>
            <ta e="T604" id="Seg_7357" s="T603">dempro-pro:case</ta>
            <ta e="T605" id="Seg_7358" s="T604">v-v:(ins)-v:(neg)-v:mood.pn</ta>
            <ta e="T606" id="Seg_7359" s="T605">v-v:tense-v:pred.pn</ta>
            <ta e="T607" id="Seg_7360" s="T606">v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T608" id="Seg_7361" s="T607">ptcl</ta>
            <ta e="T609" id="Seg_7362" s="T608">que-pro:case</ta>
            <ta e="T610" id="Seg_7363" s="T609">v-v:cvb</ta>
            <ta e="T611" id="Seg_7364" s="T610">v-v:ptcp-v:(poss).[v:(case)]</ta>
            <ta e="T612" id="Seg_7365" s="T611">ptcl</ta>
            <ta e="T613" id="Seg_7366" s="T612">n.[n:case]</ta>
            <ta e="T614" id="Seg_7367" s="T613">ptcl</ta>
            <ta e="T615" id="Seg_7368" s="T614">adv</ta>
            <ta e="T616" id="Seg_7369" s="T615">n.[n:case]</ta>
            <ta e="T617" id="Seg_7370" s="T616">n.[n:case]</ta>
            <ta e="T618" id="Seg_7371" s="T617">adj</ta>
            <ta e="T619" id="Seg_7372" s="T618">v-v:cvb</ta>
            <ta e="T620" id="Seg_7373" s="T619">v-v:tense.[v:pred.pn]</ta>
            <ta e="T621" id="Seg_7374" s="T620">n.[n:case]</ta>
            <ta e="T622" id="Seg_7375" s="T621">v-v:ptcp.[v:(case)]</ta>
            <ta e="T623" id="Seg_7376" s="T622">adv</ta>
            <ta e="T624" id="Seg_7377" s="T623">adv</ta>
            <ta e="T625" id="Seg_7378" s="T624">cardnum</ta>
            <ta e="T626" id="Seg_7379" s="T625">n.[n:case]</ta>
            <ta e="T627" id="Seg_7380" s="T626">v-v:tense-v:(poss)</ta>
            <ta e="T628" id="Seg_7381" s="T627">adv</ta>
            <ta e="T629" id="Seg_7382" s="T628">cardnum</ta>
            <ta e="T630" id="Seg_7383" s="T629">n.[n:case]</ta>
            <ta e="T631" id="Seg_7384" s="T630">v-v:cvb</ta>
            <ta e="T632" id="Seg_7385" s="T631">v-v:cvb</ta>
            <ta e="T633" id="Seg_7386" s="T632">v-v:tense.[v:pred.pn]</ta>
            <ta e="T634" id="Seg_7387" s="T633">ptcl</ta>
            <ta e="T635" id="Seg_7388" s="T634">adj-adj&gt;adj</ta>
            <ta e="T636" id="Seg_7389" s="T635">n-n:(poss).[n:case]</ta>
            <ta e="T637" id="Seg_7390" s="T636">adv</ta>
            <ta e="T638" id="Seg_7391" s="T637">adj.[n:case]</ta>
            <ta e="T639" id="Seg_7392" s="T638">n.[n:case]</ta>
            <ta e="T640" id="Seg_7393" s="T639">v-v:tense-v:poss.pn</ta>
            <ta e="T641" id="Seg_7394" s="T640">n-n:(ins)-n:case</ta>
            <ta e="T642" id="Seg_7395" s="T641">v-v&gt;v-v:cvb</ta>
            <ta e="T643" id="Seg_7396" s="T642">v-v:ptcp</ta>
            <ta e="T644" id="Seg_7397" s="T643">v-v:tense-v:poss.pn</ta>
            <ta e="T662" id="Seg_7398" s="T661">ptcl</ta>
            <ta e="T663" id="Seg_7399" s="T662">dempro</ta>
            <ta e="T664" id="Seg_7400" s="T663">n-n:case</ta>
            <ta e="T665" id="Seg_7401" s="T664">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T666" id="Seg_7402" s="T665">ptcl</ta>
            <ta e="T670" id="Seg_7403" s="T668">n.[n:case]</ta>
            <ta e="T671" id="Seg_7404" s="T670">v-v:mood-v:temp.pn</ta>
            <ta e="T672" id="Seg_7405" s="T671">n-n:case</ta>
            <ta e="T673" id="Seg_7406" s="T672">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T674" id="Seg_7407" s="T673">ptcl</ta>
            <ta e="T676" id="Seg_7408" s="T675">adj-n:(poss).[n:case]</ta>
            <ta e="T688" id="Seg_7409" s="T687">adv</ta>
            <ta e="T689" id="Seg_7410" s="T688">adv</ta>
            <ta e="T690" id="Seg_7411" s="T689">v-v:tense-v:pred.pn</ta>
            <ta e="T691" id="Seg_7412" s="T690">n-n:poss-n:case</ta>
            <ta e="T692" id="Seg_7413" s="T691">n-n:(num)-n:case</ta>
            <ta e="T693" id="Seg_7414" s="T692">n-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T694" id="Seg_7415" s="T693">n-n:poss-n:case</ta>
            <ta e="T695" id="Seg_7416" s="T694">v-v:mood-v:pred.pn</ta>
            <ta e="T696" id="Seg_7417" s="T695">ptcl</ta>
            <ta e="T697" id="Seg_7418" s="T696">ptcl</ta>
            <ta e="T698" id="Seg_7419" s="T697">dempro</ta>
            <ta e="T699" id="Seg_7420" s="T698">ptcl</ta>
            <ta e="T700" id="Seg_7421" s="T699">n-n:(num).[n:case]</ta>
            <ta e="T701" id="Seg_7422" s="T700">n-n:(poss).[n:case]</ta>
            <ta e="T702" id="Seg_7423" s="T701">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T703" id="Seg_7424" s="T702">dempro</ta>
            <ta e="T704" id="Seg_7425" s="T703">adj.[n:case]</ta>
            <ta e="T705" id="Seg_7426" s="T704">ptcl</ta>
            <ta e="T706" id="Seg_7427" s="T705">n-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T707" id="Seg_7428" s="T706">n.[n:case]</ta>
            <ta e="T708" id="Seg_7429" s="T707">v-v:mood.[v:pred.pn]</ta>
            <ta e="T709" id="Seg_7430" s="T708">v-v:(ins)-v:(neg)-v:pred.pn</ta>
            <ta e="T710" id="Seg_7431" s="T709">adv</ta>
            <ta e="T711" id="Seg_7432" s="T710">n-n:(num).[n:case]</ta>
            <ta e="T712" id="Seg_7433" s="T711">adv</ta>
            <ta e="T716" id="Seg_7434" s="T715">adv</ta>
            <ta e="T717" id="Seg_7435" s="T716">adj</ta>
            <ta e="T719" id="Seg_7436" s="T717">n.[n:case]</ta>
         </annotation>
         <annotation name="ps" tierref="ps-KiPP">
            <ta e="T2" id="Seg_7437" s="T1">que</ta>
            <ta e="T3" id="Seg_7438" s="T2">n</ta>
            <ta e="T4" id="Seg_7439" s="T3">v</ta>
            <ta e="T0" id="Seg_7440" s="T4">ptcl</ta>
            <ta e="T6" id="Seg_7441" s="T5">n</ta>
            <ta e="T7" id="Seg_7442" s="T6">ptcl</ta>
            <ta e="T8" id="Seg_7443" s="T7">adv</ta>
            <ta e="T9" id="Seg_7444" s="T8">n</ta>
            <ta e="T10" id="Seg_7445" s="T9">ptcl</ta>
            <ta e="T11" id="Seg_7446" s="T10">cop</ta>
            <ta e="T12" id="Seg_7447" s="T11">n</ta>
            <ta e="T13" id="Seg_7448" s="T12">dempro</ta>
            <ta e="T14" id="Seg_7449" s="T13">n</ta>
            <ta e="T15" id="Seg_7450" s="T14">v</ta>
            <ta e="T16" id="Seg_7451" s="T15">v</ta>
            <ta e="T17" id="Seg_7452" s="T16">adj</ta>
            <ta e="T18" id="Seg_7453" s="T17">v</ta>
            <ta e="T19" id="Seg_7454" s="T18">v</ta>
            <ta e="T20" id="Seg_7455" s="T19">n</ta>
            <ta e="T21" id="Seg_7456" s="T20">cop</ta>
            <ta e="T22" id="Seg_7457" s="T21">dempro</ta>
            <ta e="T23" id="Seg_7458" s="T22">n</ta>
            <ta e="T24" id="Seg_7459" s="T23">n</ta>
            <ta e="T25" id="Seg_7460" s="T24">n</ta>
            <ta e="T26" id="Seg_7461" s="T25">ptcl</ta>
            <ta e="T27" id="Seg_7462" s="T26">n</ta>
            <ta e="T28" id="Seg_7463" s="T27">n</ta>
            <ta e="T29" id="Seg_7464" s="T28">n</ta>
            <ta e="T30" id="Seg_7465" s="T29">v</ta>
            <ta e="T31" id="Seg_7466" s="T30">ptcl</ta>
            <ta e="T32" id="Seg_7467" s="T31">n</ta>
            <ta e="T33" id="Seg_7468" s="T32">adj</ta>
            <ta e="T34" id="Seg_7469" s="T33">cop</ta>
            <ta e="T35" id="Seg_7470" s="T34">n</ta>
            <ta e="T36" id="Seg_7471" s="T35">n</ta>
            <ta e="T37" id="Seg_7472" s="T36">adv</ta>
            <ta e="T38" id="Seg_7473" s="T37">v</ta>
            <ta e="T39" id="Seg_7474" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_7475" s="T39">n</ta>
            <ta e="T41" id="Seg_7476" s="T40">n</ta>
            <ta e="T42" id="Seg_7477" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_7478" s="T42">v</ta>
            <ta e="T44" id="Seg_7479" s="T43">ptcl</ta>
            <ta e="T45" id="Seg_7480" s="T44">n</ta>
            <ta e="T46" id="Seg_7481" s="T45">adv</ta>
            <ta e="T47" id="Seg_7482" s="T46">n</ta>
            <ta e="T48" id="Seg_7483" s="T47">v</ta>
            <ta e="T49" id="Seg_7484" s="T48">ptcl</ta>
            <ta e="T50" id="Seg_7485" s="T49">n</ta>
            <ta e="T51" id="Seg_7486" s="T50">n</ta>
            <ta e="T52" id="Seg_7487" s="T51">n</ta>
            <ta e="T53" id="Seg_7488" s="T52">post</ta>
            <ta e="T55" id="Seg_7489" s="T54">adv</ta>
            <ta e="T56" id="Seg_7490" s="T55">n</ta>
            <ta e="T57" id="Seg_7491" s="T56">adj</ta>
            <ta e="T58" id="Seg_7492" s="T57">dempro</ta>
            <ta e="T59" id="Seg_7493" s="T58">n</ta>
            <ta e="T60" id="Seg_7494" s="T59">ptcl</ta>
            <ta e="T61" id="Seg_7495" s="T60">dempro</ta>
            <ta e="T68" id="Seg_7496" s="T66">interj</ta>
            <ta e="T69" id="Seg_7497" s="T68">ptcl</ta>
            <ta e="T70" id="Seg_7498" s="T69">adj</ta>
            <ta e="T71" id="Seg_7499" s="T70">n</ta>
            <ta e="T72" id="Seg_7500" s="T71">n</ta>
            <ta e="T73" id="Seg_7501" s="T72">post</ta>
            <ta e="T74" id="Seg_7502" s="T73">adj</ta>
            <ta e="T75" id="Seg_7503" s="T74">dempro</ta>
            <ta e="T76" id="Seg_7504" s="T75">n</ta>
            <ta e="T77" id="Seg_7505" s="T76">n</ta>
            <ta e="T78" id="Seg_7506" s="T77">post</ta>
            <ta e="T79" id="Seg_7507" s="T78">cop</ta>
            <ta e="T80" id="Seg_7508" s="T79">ptcl</ta>
            <ta e="T81" id="Seg_7509" s="T80">adj</ta>
            <ta e="T82" id="Seg_7510" s="T81">adj</ta>
            <ta e="T83" id="Seg_7511" s="T82">cop</ta>
            <ta e="T84" id="Seg_7512" s="T83">ptcl</ta>
            <ta e="T85" id="Seg_7513" s="T84">n</ta>
            <ta e="T86" id="Seg_7514" s="T85">ptcl</ta>
            <ta e="T87" id="Seg_7515" s="T86">v</ta>
            <ta e="T88" id="Seg_7516" s="T87">adv</ta>
            <ta e="T89" id="Seg_7517" s="T88">v</ta>
            <ta e="T90" id="Seg_7518" s="T89">ptcl</ta>
            <ta e="T723" id="Seg_7519" s="T90">n</ta>
            <ta e="T92" id="Seg_7520" s="T91">dempro</ta>
            <ta e="T93" id="Seg_7521" s="T92">dempro</ta>
            <ta e="T94" id="Seg_7522" s="T93">adv</ta>
            <ta e="T95" id="Seg_7523" s="T94">n</ta>
            <ta e="T96" id="Seg_7524" s="T95">v</ta>
            <ta e="T97" id="Seg_7525" s="T96">post</ta>
            <ta e="T98" id="Seg_7526" s="T97">n</ta>
            <ta e="T99" id="Seg_7527" s="T98">v</ta>
            <ta e="T100" id="Seg_7528" s="T99">n</ta>
            <ta e="T101" id="Seg_7529" s="T100">adv</ta>
            <ta e="T102" id="Seg_7530" s="T101">v</ta>
            <ta e="T103" id="Seg_7531" s="T102">n</ta>
            <ta e="T104" id="Seg_7532" s="T103">n</ta>
            <ta e="T105" id="Seg_7533" s="T104">v</ta>
            <ta e="T724" id="Seg_7534" s="T105">n</ta>
            <ta e="T107" id="Seg_7535" s="T106">n</ta>
            <ta e="T108" id="Seg_7536" s="T107">v</ta>
            <ta e="T109" id="Seg_7537" s="T108">adv</ta>
            <ta e="T110" id="Seg_7538" s="T109">n</ta>
            <ta e="T111" id="Seg_7539" s="T110">v</ta>
            <ta e="T112" id="Seg_7540" s="T111">n</ta>
            <ta e="T113" id="Seg_7541" s="T112">cop</ta>
            <ta e="T114" id="Seg_7542" s="T113">dempro</ta>
            <ta e="T725" id="Seg_7543" s="T114">n</ta>
            <ta e="T116" id="Seg_7544" s="T115">dempro</ta>
            <ta e="T117" id="Seg_7545" s="T116">n</ta>
            <ta e="T118" id="Seg_7546" s="T117">dempro</ta>
            <ta e="T119" id="Seg_7547" s="T118">ptcl</ta>
            <ta e="T120" id="Seg_7548" s="T119">n</ta>
            <ta e="T121" id="Seg_7549" s="T120">v</ta>
            <ta e="T122" id="Seg_7550" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_7551" s="T122">n</ta>
            <ta e="T124" id="Seg_7552" s="T123">v</ta>
            <ta e="T125" id="Seg_7553" s="T124">conj</ta>
            <ta e="T126" id="Seg_7554" s="T125">v</ta>
            <ta e="T127" id="Seg_7555" s="T126">conj</ta>
            <ta e="T128" id="Seg_7556" s="T127">v</ta>
            <ta e="T129" id="Seg_7557" s="T128">ptcl</ta>
            <ta e="T130" id="Seg_7558" s="T129">cardnum</ta>
            <ta e="T131" id="Seg_7559" s="T130">v</ta>
            <ta e="T132" id="Seg_7560" s="T131">v</ta>
            <ta e="T133" id="Seg_7561" s="T132">aux</ta>
            <ta e="T134" id="Seg_7562" s="T133">dempro</ta>
            <ta e="T135" id="Seg_7563" s="T134">dempro</ta>
            <ta e="T136" id="Seg_7564" s="T135">post</ta>
            <ta e="T137" id="Seg_7565" s="T136">pers</ta>
            <ta e="T138" id="Seg_7566" s="T137">v</ta>
            <ta e="T139" id="Seg_7567" s="T138">aux</ta>
            <ta e="T140" id="Seg_7568" s="T139">n</ta>
            <ta e="T141" id="Seg_7569" s="T140">v</ta>
            <ta e="T142" id="Seg_7570" s="T141">cardnum</ta>
            <ta e="T143" id="Seg_7571" s="T142">cardnum</ta>
            <ta e="T144" id="Seg_7572" s="T143">adj</ta>
            <ta e="T145" id="Seg_7573" s="T144">ptcl</ta>
            <ta e="T146" id="Seg_7574" s="T145">que</ta>
            <ta e="T147" id="Seg_7575" s="T146">n</ta>
            <ta e="T148" id="Seg_7576" s="T147">n</ta>
            <ta e="T149" id="Seg_7577" s="T148">v</ta>
            <ta e="T150" id="Seg_7578" s="T149">dempro</ta>
            <ta e="T151" id="Seg_7579" s="T150">cardnum</ta>
            <ta e="T152" id="Seg_7580" s="T151">cardnum</ta>
            <ta e="T153" id="Seg_7581" s="T152">adj</ta>
            <ta e="T154" id="Seg_7582" s="T153">v</ta>
            <ta e="T157" id="Seg_7583" s="T156">adv</ta>
            <ta e="T158" id="Seg_7584" s="T157">adj</ta>
            <ta e="T159" id="Seg_7585" s="T158">adj</ta>
            <ta e="T160" id="Seg_7586" s="T159">cop</ta>
            <ta e="T161" id="Seg_7587" s="T160">ptcl</ta>
            <ta e="T162" id="Seg_7588" s="T161">n</ta>
            <ta e="T163" id="Seg_7589" s="T162">ptcl</ta>
            <ta e="T164" id="Seg_7590" s="T163">ptcl</ta>
            <ta e="T165" id="Seg_7591" s="T164">cop</ta>
            <ta e="T166" id="Seg_7592" s="T165">adv</ta>
            <ta e="T167" id="Seg_7593" s="T166">dempro</ta>
            <ta e="T168" id="Seg_7594" s="T167">v</ta>
            <ta e="T169" id="Seg_7595" s="T168">adj</ta>
            <ta e="T170" id="Seg_7596" s="T169">n</ta>
            <ta e="T171" id="Seg_7597" s="T170">adv</ta>
            <ta e="T172" id="Seg_7598" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_7599" s="T172">dempro</ta>
            <ta e="T174" id="Seg_7600" s="T173">dempro</ta>
            <ta e="T175" id="Seg_7601" s="T174">n</ta>
            <ta e="T176" id="Seg_7602" s="T175">cardnum</ta>
            <ta e="T177" id="Seg_7603" s="T176">cardnum</ta>
            <ta e="T178" id="Seg_7604" s="T177">cardnum</ta>
            <ta e="T179" id="Seg_7605" s="T178">cardnum</ta>
            <ta e="T180" id="Seg_7606" s="T179">adj</ta>
            <ta e="T181" id="Seg_7607" s="T180">v</ta>
            <ta e="T182" id="Seg_7608" s="T181">n</ta>
            <ta e="T183" id="Seg_7609" s="T182">dempro</ta>
            <ta e="T184" id="Seg_7610" s="T183">ptcl</ta>
            <ta e="T185" id="Seg_7611" s="T184">adv</ta>
            <ta e="T186" id="Seg_7612" s="T185">v</ta>
            <ta e="T187" id="Seg_7613" s="T186">v</ta>
            <ta e="T188" id="Seg_7614" s="T187">dempro</ta>
            <ta e="T189" id="Seg_7615" s="T188">adv</ta>
            <ta e="T190" id="Seg_7616" s="T189">adv</ta>
            <ta e="T191" id="Seg_7617" s="T190">adv</ta>
            <ta e="T192" id="Seg_7618" s="T191">ptcl</ta>
            <ta e="T194" id="Seg_7619" s="T193">v</ta>
            <ta e="T195" id="Seg_7620" s="T194">conj</ta>
            <ta e="T196" id="Seg_7621" s="T195">v</ta>
            <ta e="T197" id="Seg_7622" s="T196">aux</ta>
            <ta e="T198" id="Seg_7623" s="T197">ptcl</ta>
            <ta e="T203" id="Seg_7624" s="T202">adv</ta>
            <ta e="T204" id="Seg_7625" s="T203">adj</ta>
            <ta e="T205" id="Seg_7626" s="T204">n</ta>
            <ta e="T206" id="Seg_7627" s="T205">cop</ta>
            <ta e="T207" id="Seg_7628" s="T206">n</ta>
            <ta e="T208" id="Seg_7629" s="T207">dempro</ta>
            <ta e="T209" id="Seg_7630" s="T208">v</ta>
            <ta e="T210" id="Seg_7631" s="T209">n</ta>
            <ta e="T726" id="Seg_7632" s="T210">dempro</ta>
            <ta e="T212" id="Seg_7633" s="T211">dempro</ta>
            <ta e="T213" id="Seg_7634" s="T212">pers</ta>
            <ta e="T214" id="Seg_7635" s="T213">cardnum</ta>
            <ta e="T215" id="Seg_7636" s="T214">adj</ta>
            <ta e="T216" id="Seg_7637" s="T215">v</ta>
            <ta e="T217" id="Seg_7638" s="T216">ptcl</ta>
            <ta e="T218" id="Seg_7639" s="T217">adv</ta>
            <ta e="T219" id="Seg_7640" s="T218">adv</ta>
            <ta e="T220" id="Seg_7641" s="T219">conj</ta>
            <ta e="T221" id="Seg_7642" s="T220">adj</ta>
            <ta e="T222" id="Seg_7643" s="T221">cop</ta>
            <ta e="T223" id="Seg_7644" s="T222">adv</ta>
            <ta e="T224" id="Seg_7645" s="T223">ptcl</ta>
            <ta e="T225" id="Seg_7646" s="T224">dempro</ta>
            <ta e="T226" id="Seg_7647" s="T225">adj</ta>
            <ta e="T227" id="Seg_7648" s="T226">cop</ta>
            <ta e="T228" id="Seg_7649" s="T227">adv</ta>
            <ta e="T229" id="Seg_7650" s="T228">ptcl</ta>
            <ta e="T230" id="Seg_7651" s="T229">n</ta>
            <ta e="T231" id="Seg_7652" s="T230">v</ta>
            <ta e="T232" id="Seg_7653" s="T231">n</ta>
            <ta e="T233" id="Seg_7654" s="T232">v</ta>
            <ta e="T727" id="Seg_7655" s="T233">v</ta>
            <ta e="T235" id="Seg_7656" s="T234">n</ta>
            <ta e="T236" id="Seg_7657" s="T235">n</ta>
            <ta e="T237" id="Seg_7658" s="T236">n</ta>
            <ta e="T238" id="Seg_7659" s="T237">cop</ta>
            <ta e="T239" id="Seg_7660" s="T238">n</ta>
            <ta e="T240" id="Seg_7661" s="T239">v</ta>
            <ta e="T241" id="Seg_7662" s="T240">v</ta>
            <ta e="T242" id="Seg_7663" s="T241">adv</ta>
            <ta e="T243" id="Seg_7664" s="T242">v</ta>
            <ta e="T244" id="Seg_7665" s="T243">n</ta>
            <ta e="T245" id="Seg_7666" s="T244">adv</ta>
            <ta e="T246" id="Seg_7667" s="T245">v</ta>
            <ta e="T247" id="Seg_7668" s="T246">v</ta>
            <ta e="T248" id="Seg_7669" s="T247">adv</ta>
            <ta e="T249" id="Seg_7670" s="T248">que</ta>
            <ta e="T250" id="Seg_7671" s="T249">cop</ta>
            <ta e="T251" id="Seg_7672" s="T250">v</ta>
            <ta e="T252" id="Seg_7673" s="T251">n</ta>
            <ta e="T253" id="Seg_7674" s="T252">cop</ta>
            <ta e="T254" id="Seg_7675" s="T253">adv</ta>
            <ta e="T255" id="Seg_7676" s="T254">emphpro</ta>
            <ta e="T256" id="Seg_7677" s="T255">dempro</ta>
            <ta e="T257" id="Seg_7678" s="T256">adv</ta>
            <ta e="T258" id="Seg_7679" s="T257">v</ta>
            <ta e="T259" id="Seg_7680" s="T258">adv</ta>
            <ta e="T260" id="Seg_7681" s="T259">emphpro</ta>
            <ta e="T261" id="Seg_7682" s="T260">v</ta>
            <ta e="T262" id="Seg_7683" s="T261">v</ta>
            <ta e="T263" id="Seg_7684" s="T262">n</ta>
            <ta e="T264" id="Seg_7685" s="T263">n</ta>
            <ta e="T265" id="Seg_7686" s="T264">dempro</ta>
            <ta e="T266" id="Seg_7687" s="T265">adv</ta>
            <ta e="T267" id="Seg_7688" s="T266">adj</ta>
            <ta e="T268" id="Seg_7689" s="T267">cop</ta>
            <ta e="T269" id="Seg_7690" s="T268">emphpro</ta>
            <ta e="T270" id="Seg_7691" s="T269">adv</ta>
            <ta e="T271" id="Seg_7692" s="T270">adj</ta>
            <ta e="T272" id="Seg_7693" s="T271">cop</ta>
            <ta e="T273" id="Seg_7694" s="T272">v</ta>
            <ta e="T274" id="Seg_7695" s="T273">pers</ta>
            <ta e="T275" id="Seg_7696" s="T274">adv</ta>
            <ta e="T276" id="Seg_7697" s="T275">adj</ta>
            <ta e="T277" id="Seg_7698" s="T276">dempro</ta>
            <ta e="T278" id="Seg_7699" s="T277">n</ta>
            <ta e="T279" id="Seg_7700" s="T278">cop</ta>
            <ta e="T280" id="Seg_7701" s="T279">ptcl</ta>
            <ta e="T281" id="Seg_7702" s="T280">adv</ta>
            <ta e="T282" id="Seg_7703" s="T281">n</ta>
            <ta e="T283" id="Seg_7704" s="T282">v</ta>
            <ta e="T284" id="Seg_7705" s="T283">n</ta>
            <ta e="T285" id="Seg_7706" s="T284">v</ta>
            <ta e="T286" id="Seg_7707" s="T285">post</ta>
            <ta e="T287" id="Seg_7708" s="T286">interj</ta>
            <ta e="T288" id="Seg_7709" s="T287">n</ta>
            <ta e="T289" id="Seg_7710" s="T288">v</ta>
            <ta e="T290" id="Seg_7711" s="T289">n</ta>
            <ta e="T291" id="Seg_7712" s="T290">n</ta>
            <ta e="T292" id="Seg_7713" s="T291">v</ta>
            <ta e="T293" id="Seg_7714" s="T292">adv</ta>
            <ta e="T294" id="Seg_7715" s="T293">n</ta>
            <ta e="T295" id="Seg_7716" s="T294">n</ta>
            <ta e="T296" id="Seg_7717" s="T295">v</ta>
            <ta e="T297" id="Seg_7718" s="T296">adv</ta>
            <ta e="T298" id="Seg_7719" s="T297">v</ta>
            <ta e="T299" id="Seg_7720" s="T298">ptcl</ta>
            <ta e="T300" id="Seg_7721" s="T299">interj</ta>
            <ta e="T301" id="Seg_7722" s="T300">n</ta>
            <ta e="T302" id="Seg_7723" s="T301">v</ta>
            <ta e="T303" id="Seg_7724" s="T302">ptcl</ta>
            <ta e="T304" id="Seg_7725" s="T303">adv</ta>
            <ta e="T305" id="Seg_7726" s="T304">v</ta>
            <ta e="T306" id="Seg_7727" s="T305">adv</ta>
            <ta e="T307" id="Seg_7728" s="T306">v</ta>
            <ta e="T308" id="Seg_7729" s="T307">ptcl</ta>
            <ta e="T309" id="Seg_7730" s="T308">adv</ta>
            <ta e="T310" id="Seg_7731" s="T309">adj</ta>
            <ta e="T311" id="Seg_7732" s="T310">adj</ta>
            <ta e="T312" id="Seg_7733" s="T311">cop</ta>
            <ta e="T313" id="Seg_7734" s="T312">quant</ta>
            <ta e="T314" id="Seg_7735" s="T313">adj</ta>
            <ta e="T315" id="Seg_7736" s="T314">cop</ta>
            <ta e="T316" id="Seg_7737" s="T315">adv</ta>
            <ta e="T317" id="Seg_7738" s="T316">n</ta>
            <ta e="T318" id="Seg_7739" s="T317">v</ta>
            <ta e="T319" id="Seg_7740" s="T318">v</ta>
            <ta e="T320" id="Seg_7741" s="T319">adv</ta>
            <ta e="T321" id="Seg_7742" s="T320">v</ta>
            <ta e="T322" id="Seg_7743" s="T321">adv</ta>
            <ta e="T323" id="Seg_7744" s="T322">pers</ta>
            <ta e="T324" id="Seg_7745" s="T323">emphpro</ta>
            <ta e="T325" id="Seg_7746" s="T324">ptcl</ta>
            <ta e="T326" id="Seg_7747" s="T325">cardnum</ta>
            <ta e="T327" id="Seg_7748" s="T326">post</ta>
            <ta e="T328" id="Seg_7749" s="T327">n</ta>
            <ta e="T329" id="Seg_7750" s="T328">emphpro</ta>
            <ta e="T330" id="Seg_7751" s="T329">n</ta>
            <ta e="T331" id="Seg_7752" s="T330">v</ta>
            <ta e="T332" id="Seg_7753" s="T331">n</ta>
            <ta e="T336" id="Seg_7754" s="T335">n</ta>
            <ta e="T337" id="Seg_7755" s="T336">emphpro</ta>
            <ta e="T338" id="Seg_7756" s="T337">n</ta>
            <ta e="T339" id="Seg_7757" s="T338">propr</ta>
            <ta e="T340" id="Seg_7758" s="T339">adj</ta>
            <ta e="T341" id="Seg_7759" s="T340">n</ta>
            <ta e="T342" id="Seg_7760" s="T341">n</ta>
            <ta e="T343" id="Seg_7761" s="T342">adv</ta>
            <ta e="T344" id="Seg_7762" s="T343">ptcl</ta>
            <ta e="T345" id="Seg_7763" s="T344">adv</ta>
            <ta e="T346" id="Seg_7764" s="T345">dempro</ta>
            <ta e="T347" id="Seg_7765" s="T346">post</ta>
            <ta e="T348" id="Seg_7766" s="T347">n</ta>
            <ta e="T349" id="Seg_7767" s="T348">ptcl</ta>
            <ta e="T350" id="Seg_7768" s="T349">ptcl</ta>
            <ta e="T351" id="Seg_7769" s="T350">interj</ta>
            <ta e="T352" id="Seg_7770" s="T351">adv</ta>
            <ta e="T353" id="Seg_7771" s="T352">ptcl</ta>
            <ta e="T354" id="Seg_7772" s="T353">ptcl</ta>
            <ta e="T355" id="Seg_7773" s="T354">cardnum</ta>
            <ta e="T356" id="Seg_7774" s="T355">post</ta>
            <ta e="T357" id="Seg_7775" s="T356">n</ta>
            <ta e="T358" id="Seg_7776" s="T357">n</ta>
            <ta e="T359" id="Seg_7777" s="T358">adv</ta>
            <ta e="T360" id="Seg_7778" s="T359">propr</ta>
            <ta e="T728" id="Seg_7779" s="T361">v</ta>
            <ta e="T363" id="Seg_7780" s="T362">propr</ta>
            <ta e="T364" id="Seg_7781" s="T363">n</ta>
            <ta e="T365" id="Seg_7782" s="T364">ptcl</ta>
            <ta e="T366" id="Seg_7783" s="T365">conj</ta>
            <ta e="T367" id="Seg_7784" s="T366">adv</ta>
            <ta e="T368" id="Seg_7785" s="T367">v</ta>
            <ta e="T369" id="Seg_7786" s="T368">adj</ta>
            <ta e="T370" id="Seg_7787" s="T369">n</ta>
            <ta e="T371" id="Seg_7788" s="T370">n</ta>
            <ta e="T372" id="Seg_7789" s="T371">n</ta>
            <ta e="T373" id="Seg_7790" s="T372">n</ta>
            <ta e="T374" id="Seg_7791" s="T373">n</ta>
            <ta e="T375" id="Seg_7792" s="T374">n</ta>
            <ta e="T376" id="Seg_7793" s="T375">adv</ta>
            <ta e="T377" id="Seg_7794" s="T376">ptcl</ta>
            <ta e="T378" id="Seg_7795" s="T377">v</ta>
            <ta e="T379" id="Seg_7796" s="T378">v</ta>
            <ta e="T380" id="Seg_7797" s="T379">ptcl</ta>
            <ta e="T381" id="Seg_7798" s="T380">ptcl</ta>
            <ta e="T382" id="Seg_7799" s="T381">adj</ta>
            <ta e="T383" id="Seg_7800" s="T382">adv</ta>
            <ta e="T384" id="Seg_7801" s="T383">adj</ta>
            <ta e="T386" id="Seg_7802" s="T384">cardnum</ta>
            <ta e="T388" id="Seg_7803" s="T386">ptcl</ta>
            <ta e="T390" id="Seg_7804" s="T388">cardnum</ta>
            <ta e="T391" id="Seg_7805" s="T390">cardnum</ta>
            <ta e="T392" id="Seg_7806" s="T391">v</ta>
            <ta e="T395" id="Seg_7807" s="T394">cop</ta>
            <ta e="T396" id="Seg_7808" s="T395">dempro</ta>
            <ta e="T397" id="Seg_7809" s="T396">v</ta>
            <ta e="T398" id="Seg_7810" s="T397">adv</ta>
            <ta e="T399" id="Seg_7811" s="T398">n</ta>
            <ta e="T400" id="Seg_7812" s="T399">v</ta>
            <ta e="T401" id="Seg_7813" s="T400">adv</ta>
            <ta e="T402" id="Seg_7814" s="T401">cardnum</ta>
            <ta e="T403" id="Seg_7815" s="T402">ptcl</ta>
            <ta e="T404" id="Seg_7816" s="T403">n</ta>
            <ta e="T405" id="Seg_7817" s="T404">v</ta>
            <ta e="T406" id="Seg_7818" s="T405">v</ta>
            <ta e="T407" id="Seg_7819" s="T406">ptcl</ta>
            <ta e="T408" id="Seg_7820" s="T407">v</ta>
            <ta e="T409" id="Seg_7821" s="T408">ptcl</ta>
            <ta e="T423" id="Seg_7822" s="T422">n</ta>
            <ta e="T424" id="Seg_7823" s="T423">v</ta>
            <ta e="T430" id="Seg_7824" s="T428">ptcl</ta>
            <ta e="T431" id="Seg_7825" s="T430">n</ta>
            <ta e="T432" id="Seg_7826" s="T431">v</ta>
            <ta e="T433" id="Seg_7827" s="T432">n</ta>
            <ta e="T434" id="Seg_7828" s="T433">quant</ta>
            <ta e="T435" id="Seg_7829" s="T434">n</ta>
            <ta e="T436" id="Seg_7830" s="T435">v</ta>
            <ta e="T440" id="Seg_7831" s="T439">n</ta>
            <ta e="T441" id="Seg_7832" s="T440">ptcl</ta>
            <ta e="T442" id="Seg_7833" s="T441">n</ta>
            <ta e="T443" id="Seg_7834" s="T442">v</ta>
            <ta e="T452" id="Seg_7835" s="T451">v</ta>
            <ta e="T453" id="Seg_7836" s="T452">n</ta>
            <ta e="T454" id="Seg_7837" s="T453">n</ta>
            <ta e="T455" id="Seg_7838" s="T454">v</ta>
            <ta e="T457" id="Seg_7839" s="T456">quant</ta>
            <ta e="T458" id="Seg_7840" s="T457">adj</ta>
            <ta e="T459" id="Seg_7841" s="T458">n</ta>
            <ta e="T460" id="Seg_7842" s="T459">n</ta>
            <ta e="T461" id="Seg_7843" s="T460">v</ta>
            <ta e="T462" id="Seg_7844" s="T461">n</ta>
            <ta e="T463" id="Seg_7845" s="T462">adv</ta>
            <ta e="T464" id="Seg_7846" s="T463">v</ta>
            <ta e="T465" id="Seg_7847" s="T464">n</ta>
            <ta e="T466" id="Seg_7848" s="T465">v</ta>
            <ta e="T467" id="Seg_7849" s="T466">n</ta>
            <ta e="T468" id="Seg_7850" s="T467">cop</ta>
            <ta e="T469" id="Seg_7851" s="T468">dempro</ta>
            <ta e="T470" id="Seg_7852" s="T469">post</ta>
            <ta e="T471" id="Seg_7853" s="T470">pers</ta>
            <ta e="T472" id="Seg_7854" s="T471">n</ta>
            <ta e="T473" id="Seg_7855" s="T472">v</ta>
            <ta e="T474" id="Seg_7856" s="T473">v</ta>
            <ta e="T475" id="Seg_7857" s="T474">ptcl</ta>
            <ta e="T476" id="Seg_7858" s="T475">n</ta>
            <ta e="T477" id="Seg_7859" s="T476">cop</ta>
            <ta e="T478" id="Seg_7860" s="T477">que</ta>
            <ta e="T479" id="Seg_7861" s="T478">v</ta>
            <ta e="T480" id="Seg_7862" s="T479">v</ta>
            <ta e="T481" id="Seg_7863" s="T480">aux</ta>
            <ta e="T482" id="Seg_7864" s="T481">n</ta>
            <ta e="T483" id="Seg_7865" s="T482">v</ta>
            <ta e="T484" id="Seg_7866" s="T483">aux</ta>
            <ta e="T485" id="Seg_7867" s="T484">adv</ta>
            <ta e="T486" id="Seg_7868" s="T485">v</ta>
            <ta e="T487" id="Seg_7869" s="T486">propr</ta>
            <ta e="T488" id="Seg_7870" s="T487">ptcl</ta>
            <ta e="T489" id="Seg_7871" s="T488">cop</ta>
            <ta e="T491" id="Seg_7872" s="T490">adj</ta>
            <ta e="T492" id="Seg_7873" s="T491">n</ta>
            <ta e="T493" id="Seg_7874" s="T492">adv</ta>
            <ta e="T494" id="Seg_7875" s="T493">adj</ta>
            <ta e="T495" id="Seg_7876" s="T494">n</ta>
            <ta e="T496" id="Seg_7877" s="T495">adj</ta>
            <ta e="T497" id="Seg_7878" s="T496">n</ta>
            <ta e="T498" id="Seg_7879" s="T497">n</ta>
            <ta e="T499" id="Seg_7880" s="T498">ptcl</ta>
            <ta e="T500" id="Seg_7881" s="T499">ptcl</ta>
            <ta e="T501" id="Seg_7882" s="T500">dempro</ta>
            <ta e="T502" id="Seg_7883" s="T501">que</ta>
            <ta e="T503" id="Seg_7884" s="T502">conj</ta>
            <ta e="T504" id="Seg_7885" s="T503">cop</ta>
            <ta e="T505" id="Seg_7886" s="T504">pers</ta>
            <ta e="T506" id="Seg_7887" s="T505">v</ta>
            <ta e="T507" id="Seg_7888" s="T506">pers</ta>
            <ta e="T508" id="Seg_7889" s="T507">v</ta>
            <ta e="T509" id="Seg_7890" s="T508">conj</ta>
            <ta e="T510" id="Seg_7891" s="T509">v</ta>
            <ta e="T511" id="Seg_7892" s="T510">conj</ta>
            <ta e="T512" id="Seg_7893" s="T511">pers</ta>
            <ta e="T513" id="Seg_7894" s="T512">n</ta>
            <ta e="T514" id="Seg_7895" s="T513">ptcl</ta>
            <ta e="T515" id="Seg_7896" s="T514">adv</ta>
            <ta e="T516" id="Seg_7897" s="T515">n</ta>
            <ta e="T517" id="Seg_7898" s="T516">ptcl</ta>
            <ta e="T518" id="Seg_7899" s="T517">que</ta>
            <ta e="T519" id="Seg_7900" s="T518">v</ta>
            <ta e="T520" id="Seg_7901" s="T519">dempro</ta>
            <ta e="T521" id="Seg_7902" s="T520">pers</ta>
            <ta e="T522" id="Seg_7903" s="T521">n</ta>
            <ta e="T523" id="Seg_7904" s="T522">v</ta>
            <ta e="T524" id="Seg_7905" s="T523">pers</ta>
            <ta e="T525" id="Seg_7906" s="T524">n</ta>
            <ta e="T526" id="Seg_7907" s="T525">v</ta>
            <ta e="T527" id="Seg_7908" s="T526">pers</ta>
            <ta e="T528" id="Seg_7909" s="T527">v</ta>
            <ta e="T532" id="Seg_7910" s="T531">adj</ta>
            <ta e="T533" id="Seg_7911" s="T532">adv</ta>
            <ta e="T534" id="Seg_7912" s="T533">ptcl</ta>
            <ta e="T535" id="Seg_7913" s="T534">que</ta>
            <ta e="T536" id="Seg_7914" s="T535">v</ta>
            <ta e="T552" id="Seg_7915" s="T550">ptcl</ta>
            <ta e="T571" id="Seg_7916" s="T570">v</ta>
            <ta e="T572" id="Seg_7917" s="T571">adv</ta>
            <ta e="T573" id="Seg_7918" s="T572">dempro</ta>
            <ta e="T574" id="Seg_7919" s="T573">v</ta>
            <ta e="T575" id="Seg_7920" s="T574">aux</ta>
            <ta e="T576" id="Seg_7921" s="T575">dempro</ta>
            <ta e="T577" id="Seg_7922" s="T576">dempro</ta>
            <ta e="T578" id="Seg_7923" s="T577">n</ta>
            <ta e="T579" id="Seg_7924" s="T578">n</ta>
            <ta e="T580" id="Seg_7925" s="T579">interj</ta>
            <ta e="T581" id="Seg_7926" s="T580">n</ta>
            <ta e="T582" id="Seg_7927" s="T581">n</ta>
            <ta e="T583" id="Seg_7928" s="T582">n</ta>
            <ta e="T584" id="Seg_7929" s="T583">dempro</ta>
            <ta e="T585" id="Seg_7930" s="T584">v</ta>
            <ta e="T587" id="Seg_7931" s="T586">n</ta>
            <ta e="T588" id="Seg_7932" s="T587">pers</ta>
            <ta e="T589" id="Seg_7933" s="T588">n</ta>
            <ta e="T590" id="Seg_7934" s="T589">post</ta>
            <ta e="T591" id="Seg_7935" s="T590">n</ta>
            <ta e="T592" id="Seg_7936" s="T591">n</ta>
            <ta e="T593" id="Seg_7937" s="T592">v</ta>
            <ta e="T594" id="Seg_7938" s="T593">post</ta>
            <ta e="T595" id="Seg_7939" s="T594">adv</ta>
            <ta e="T596" id="Seg_7940" s="T595">v</ta>
            <ta e="T597" id="Seg_7941" s="T596">adj</ta>
            <ta e="T598" id="Seg_7942" s="T597">n</ta>
            <ta e="T599" id="Seg_7943" s="T598">dempro</ta>
            <ta e="T600" id="Seg_7944" s="T599">v</ta>
            <ta e="T601" id="Seg_7945" s="T600">dempro</ta>
            <ta e="T602" id="Seg_7946" s="T601">v</ta>
            <ta e="T603" id="Seg_7947" s="T602">dempro</ta>
            <ta e="T604" id="Seg_7948" s="T603">dempro</ta>
            <ta e="T605" id="Seg_7949" s="T604">v</ta>
            <ta e="T606" id="Seg_7950" s="T605">v</ta>
            <ta e="T607" id="Seg_7951" s="T606">v</ta>
            <ta e="T608" id="Seg_7952" s="T607">ptcl</ta>
            <ta e="T609" id="Seg_7953" s="T608">que</ta>
            <ta e="T610" id="Seg_7954" s="T609">v</ta>
            <ta e="T611" id="Seg_7955" s="T610">aux</ta>
            <ta e="T612" id="Seg_7956" s="T611">ptcl</ta>
            <ta e="T613" id="Seg_7957" s="T612">n</ta>
            <ta e="T614" id="Seg_7958" s="T613">ptcl</ta>
            <ta e="T615" id="Seg_7959" s="T614">adv</ta>
            <ta e="T616" id="Seg_7960" s="T615">n</ta>
            <ta e="T617" id="Seg_7961" s="T616">n</ta>
            <ta e="T618" id="Seg_7962" s="T617">adj</ta>
            <ta e="T619" id="Seg_7963" s="T618">v</ta>
            <ta e="T620" id="Seg_7964" s="T619">aux</ta>
            <ta e="T621" id="Seg_7965" s="T620">n</ta>
            <ta e="T622" id="Seg_7966" s="T621">v</ta>
            <ta e="T623" id="Seg_7967" s="T622">adv</ta>
            <ta e="T624" id="Seg_7968" s="T623">adv</ta>
            <ta e="T625" id="Seg_7969" s="T624">cardnum</ta>
            <ta e="T626" id="Seg_7970" s="T625">n</ta>
            <ta e="T627" id="Seg_7971" s="T626">v</ta>
            <ta e="T628" id="Seg_7972" s="T627">adv</ta>
            <ta e="T629" id="Seg_7973" s="T628">cardnum</ta>
            <ta e="T630" id="Seg_7974" s="T629">n</ta>
            <ta e="T631" id="Seg_7975" s="T630">v</ta>
            <ta e="T632" id="Seg_7976" s="T631">aux</ta>
            <ta e="T633" id="Seg_7977" s="T632">v</ta>
            <ta e="T634" id="Seg_7978" s="T633">ptcl</ta>
            <ta e="T635" id="Seg_7979" s="T634">adj</ta>
            <ta e="T636" id="Seg_7980" s="T635">n</ta>
            <ta e="T637" id="Seg_7981" s="T636">adv</ta>
            <ta e="T638" id="Seg_7982" s="T637">adj</ta>
            <ta e="T639" id="Seg_7983" s="T638">n</ta>
            <ta e="T640" id="Seg_7984" s="T639">cop</ta>
            <ta e="T641" id="Seg_7985" s="T640">n</ta>
            <ta e="T642" id="Seg_7986" s="T641">v</ta>
            <ta e="T643" id="Seg_7987" s="T642">aux</ta>
            <ta e="T644" id="Seg_7988" s="T643">aux</ta>
            <ta e="T662" id="Seg_7989" s="T661">ptcl</ta>
            <ta e="T663" id="Seg_7990" s="T662">dempro</ta>
            <ta e="T664" id="Seg_7991" s="T663">n</ta>
            <ta e="T665" id="Seg_7992" s="T664">v</ta>
            <ta e="T666" id="Seg_7993" s="T665">ptcl</ta>
            <ta e="T670" id="Seg_7994" s="T668">n</ta>
            <ta e="T671" id="Seg_7995" s="T670">v</ta>
            <ta e="T672" id="Seg_7996" s="T671">n</ta>
            <ta e="T673" id="Seg_7997" s="T672">v</ta>
            <ta e="T674" id="Seg_7998" s="T673">ptcl</ta>
            <ta e="T676" id="Seg_7999" s="T675">adj</ta>
            <ta e="T688" id="Seg_8000" s="T687">adv</ta>
            <ta e="T689" id="Seg_8001" s="T688">adv</ta>
            <ta e="T690" id="Seg_8002" s="T689">v</ta>
            <ta e="T691" id="Seg_8003" s="T690">n</ta>
            <ta e="T692" id="Seg_8004" s="T691">n</ta>
            <ta e="T693" id="Seg_8005" s="T692">n</ta>
            <ta e="T694" id="Seg_8006" s="T693">n</ta>
            <ta e="T695" id="Seg_8007" s="T694">v</ta>
            <ta e="T696" id="Seg_8008" s="T695">ptcl</ta>
            <ta e="T697" id="Seg_8009" s="T696">ptcl</ta>
            <ta e="T698" id="Seg_8010" s="T697">dempro</ta>
            <ta e="T699" id="Seg_8011" s="T698">ptcl</ta>
            <ta e="T700" id="Seg_8012" s="T699">n</ta>
            <ta e="T701" id="Seg_8013" s="T700">n</ta>
            <ta e="T702" id="Seg_8014" s="T701">ptcl</ta>
            <ta e="T703" id="Seg_8015" s="T702">dempro</ta>
            <ta e="T704" id="Seg_8016" s="T703">adj</ta>
            <ta e="T705" id="Seg_8017" s="T704">ptcl</ta>
            <ta e="T706" id="Seg_8018" s="T705">n</ta>
            <ta e="T707" id="Seg_8019" s="T706">n</ta>
            <ta e="T708" id="Seg_8020" s="T707">cop</ta>
            <ta e="T709" id="Seg_8021" s="T708">v</ta>
            <ta e="T710" id="Seg_8022" s="T709">adv</ta>
            <ta e="T711" id="Seg_8023" s="T710">n</ta>
            <ta e="T712" id="Seg_8024" s="T711">adv</ta>
            <ta e="T716" id="Seg_8025" s="T715">adv</ta>
            <ta e="T717" id="Seg_8026" s="T716">adj</ta>
            <ta e="T719" id="Seg_8027" s="T717">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-KiPP" />
         <annotation name="SyF" tierref="SyF-KiPP" />
         <annotation name="IST" tierref="IST-KiPP" />
         <annotation name="Top" tierref="Top-KiPP" />
         <annotation name="Foc" tierref="Foc-KiPP" />
         <annotation name="BOR" tierref="BOR-KiPP">
            <ta e="T12" id="Seg_8028" s="T11">RUS:cult</ta>
            <ta e="T33" id="Seg_8029" s="T32">RUS:core</ta>
            <ta e="T39" id="Seg_8030" s="T38">RUS:mod</ta>
            <ta e="T41" id="Seg_8031" s="T40">RUS:cult</ta>
            <ta e="T47" id="Seg_8032" s="T46">RUS:cult</ta>
            <ta e="T49" id="Seg_8033" s="T48">RUS:mod</ta>
            <ta e="T50" id="Seg_8034" s="T49">RUS:cult</ta>
            <ta e="T56" id="Seg_8035" s="T55">EV:gram (DIM)</ta>
            <ta e="T59" id="Seg_8036" s="T58">RUS:core</ta>
            <ta e="T80" id="Seg_8037" s="T79">RUS:mod</ta>
            <ta e="T81" id="Seg_8038" s="T80">RUS:cult</ta>
            <ta e="T82" id="Seg_8039" s="T81">RUS:cult</ta>
            <ta e="T84" id="Seg_8040" s="T83">RUS:mod</ta>
            <ta e="T90" id="Seg_8041" s="T89">RUS:mod</ta>
            <ta e="T723" id="Seg_8042" s="T90">RUS:core</ta>
            <ta e="T117" id="Seg_8043" s="T116">RUS:cult</ta>
            <ta e="T125" id="Seg_8044" s="T124">RUS:gram</ta>
            <ta e="T127" id="Seg_8045" s="T126">RUS:gram</ta>
            <ta e="T157" id="Seg_8046" s="T156">RUS:mod</ta>
            <ta e="T162" id="Seg_8047" s="T161">RUS:core</ta>
            <ta e="T195" id="Seg_8048" s="T194">RUS:gram</ta>
            <ta e="T218" id="Seg_8049" s="T217">RUS:mod</ta>
            <ta e="T219" id="Seg_8050" s="T218">RUS:mod</ta>
            <ta e="T220" id="Seg_8051" s="T219">RUS:gram</ta>
            <ta e="T221" id="Seg_8052" s="T220">RUS:core</ta>
            <ta e="T226" id="Seg_8053" s="T225">RUS:cult</ta>
            <ta e="T264" id="Seg_8054" s="T263">RUS:cult</ta>
            <ta e="T308" id="Seg_8055" s="T307">RUS:mod</ta>
            <ta e="T332" id="Seg_8056" s="T331">RUS:cult</ta>
            <ta e="T336" id="Seg_8057" s="T335">RUS:core</ta>
            <ta e="T338" id="Seg_8058" s="T337">RUS:core</ta>
            <ta e="T339" id="Seg_8059" s="T338">RUS:cult</ta>
            <ta e="T345" id="Seg_8060" s="T344">RUS:mod</ta>
            <ta e="T351" id="Seg_8061" s="T350">RUS:disc</ta>
            <ta e="T359" id="Seg_8062" s="T358">RUS:mod</ta>
            <ta e="T360" id="Seg_8063" s="T359">RUS:cult</ta>
            <ta e="T363" id="Seg_8064" s="T362">RUS:cult</ta>
            <ta e="T365" id="Seg_8065" s="T364">RUS:disc</ta>
            <ta e="T366" id="Seg_8066" s="T365">RUS:gram</ta>
            <ta e="T382" id="Seg_8067" s="T381">RUS:cult</ta>
            <ta e="T423" id="Seg_8068" s="T422">RUS:cult</ta>
            <ta e="T431" id="Seg_8069" s="T430">RUS:cult</ta>
            <ta e="T435" id="Seg_8070" s="T434">RUS:cult</ta>
            <ta e="T487" id="Seg_8071" s="T486">RUS:cult</ta>
            <ta e="T499" id="Seg_8072" s="T498">RUS:disc</ta>
            <ta e="T503" id="Seg_8073" s="T502">RUS:gram</ta>
            <ta e="T509" id="Seg_8074" s="T508">RUS:gram</ta>
            <ta e="T511" id="Seg_8075" s="T510">RUS:gram</ta>
            <ta e="T513" id="Seg_8076" s="T512">RUS:cult</ta>
            <ta e="T613" id="Seg_8077" s="T612">RUS:core</ta>
            <ta e="T641" id="Seg_8078" s="T640">RUS:cult</ta>
            <ta e="T642" id="Seg_8079" s="T641">RUS:core</ta>
            <ta e="T664" id="Seg_8080" s="T663">RUS:cult</ta>
            <ta e="T666" id="Seg_8081" s="T665">RUS:mod</ta>
            <ta e="T674" id="Seg_8082" s="T673">RUS:mod</ta>
            <ta e="T688" id="Seg_8083" s="T687">RUS:core</ta>
            <ta e="T689" id="Seg_8084" s="T688">RUS:core</ta>
            <ta e="T696" id="Seg_8085" s="T695">RUS:disc</ta>
            <ta e="T706" id="Seg_8086" s="T705">RUS:cult</ta>
            <ta e="T711" id="Seg_8087" s="T710">RUS:cult</ta>
            <ta e="T716" id="Seg_8088" s="T715">RUS:mod</ta>
            <ta e="T717" id="Seg_8089" s="T716">RUS:core</ta>
            <ta e="T719" id="Seg_8090" s="T717">RUS:core</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-KiPP" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-KiPP" />
         <annotation name="CS" tierref="CS-KiPP" />
         <annotation name="fe" tierref="fe-KiPP">
            <ta e="T0" id="Seg_8091" s="T1">– Why do you say "sin"?</ta>
            <ta e="T12" id="Seg_8092" s="T5">A sin, though, long ago there were shamans, shamans.</ta>
            <ta e="T17" id="Seg_8093" s="T12">One cannot go around their tents, it's bad.</ta>
            <ta e="T24" id="Seg_8094" s="T17">If you go without going aroung (?), it is a sin, that means "sin".</ta>
            <ta e="T32" id="Seg_8095" s="T24">If a woman steps onto male clothes, that's also a sin.</ta>
            <ta e="T39" id="Seg_8096" s="T32">It has to be clean, the man has to live cleanly.</ta>
            <ta e="T46" id="Seg_8097" s="T39">If a woman was going without a dress, it was also a sin earlier.</ta>
            <ta e="T50" id="Seg_8098" s="T46">She has to wear a dress, a dress.</ta>
            <ta e="T57" id="Seg_8099" s="T50">Like men, now they all have trousers.</ta>
            <ta e="T61" id="Seg_8100" s="T57">That is a sin, that.</ta>
            <ta e="T84" id="Seg_8101" s="T66">– Eh, well, everything like a man is bad, a woman shall be like a woman, she shall have a dress and a kerchief.</ta>
            <ta e="T723" id="Seg_8102" s="T84">Of course she can wear a cap, one has to wear a cap.</ta>
            <ta e="T93" id="Seg_8103" s="T91">Well, such.</ta>
            <ta e="T100" id="Seg_8104" s="T93">Then, when you marry, one beats with the hand onto the icon.</ta>
            <ta e="T724" id="Seg_8105" s="T100">Like that the marrying people give each other their hand.</ta>
            <ta e="T725" id="Seg_8106" s="T106">They give each other their hands, then, when they give their hands, this human turns into a woman.</ta>
            <ta e="T118" id="Seg_8107" s="T115">Such customs, such.</ta>
            <ta e="T130" id="Seg_8108" s="T118">Yeah, they marry a person under pressure, if you want or not, they marry [you] in either case.</ta>
            <ta e="T134" id="Seg_8109" s="T130">Crying you go to him then.</ta>
            <ta e="T141" id="Seg_8110" s="T134">Like that I have married, crying.</ta>
            <ta e="T153" id="Seg_8111" s="T141">When I was sixteen, what person is marrying being sixteen?</ta>
            <ta e="T154" id="Seg_8112" s="T153">They gave me.</ta>
            <ta e="T161" id="Seg_8113" s="T156">– Very young, I was still clean.</ta>
            <ta e="T165" id="Seg_8114" s="T161">I didn't have my period.</ta>
            <ta e="T172" id="Seg_8115" s="T165">Then my big daughter came out.</ta>
            <ta e="T183" id="Seg_8116" s="T172">This child was born when I was seventeen, eighteen years old.</ta>
            <ta e="T193" id="Seg_8117" s="T183">Then when I got older, later then.</ta>
            <ta e="T202" id="Seg_8118" s="T193">You have one children after another, love, dark love.</ta>
            <ta e="T207" id="Seg_8119" s="T202">My husband was a very good man.</ta>
            <ta e="T726" id="Seg_8120" s="T207">Well, the person who is sitting there.</ta>
            <ta e="T218" id="Seg_8121" s="T211">That was, when I had nine children (?).</ta>
            <ta e="T223" id="Seg_8122" s="T218">There I wasn't young anymore.</ta>
            <ta e="T727" id="Seg_8123" s="T223">Well, one had such customs, "your are living the life, feed the children" they say.</ta>
            <ta e="T241" id="Seg_8124" s="T234">"Orphan children, that's a sin, feed the orphan children", they say.</ta>
            <ta e="T247" id="Seg_8125" s="T241">"Be good to a person who has done something bad", they say.</ta>
            <ta e="T263" id="Seg_8126" s="T247">If I say "why is it the other way round", it is a sin, "if you yourself are living well, the other people will envy you", the old people say.</ta>
            <ta e="T265" id="Seg_8127" s="T263">That's the custom.</ta>
            <ta e="T276" id="Seg_8128" s="T265">Then you are happy, you yourself will be happy later, it is said, now I am happy.</ta>
            <ta e="T281" id="Seg_8129" s="T276">I was an orphan then.</ta>
            <ta e="T292" id="Seg_8130" s="T281">When my mother died, after I had been married, eh, when I wasn't yet married, we went from family to family.</ta>
            <ta e="T298" id="Seg_8131" s="T292">Then, when my mother died, she said: </ta>
            <ta e="T303" id="Seg_8132" s="T298">"Do always feed orphans".</ta>
            <ta e="T312" id="Seg_8133" s="T303">"You have to be good to that one who has done something bad, then you will be happy for a long time.</ta>
            <ta e="T319" id="Seg_8134" s="T312">You will have many children, you will live good life", she said.</ta>
            <ta e="T321" id="Seg_8135" s="T319">And indeed, look.</ta>
            <ta e="T331" id="Seg_8136" s="T321">Now I myself, though, from me myself more than fifty people have developed .</ta>
            <ta e="T332" id="Seg_8137" s="T331">I am together with my grandchildren.</ta>
            <ta e="T335" id="Seg_8138" s="T332">A bit more than fifty.</ta>
            <ta e="T338" id="Seg_8139" s="T335">My people, my own people.</ta>
            <ta e="T341" id="Seg_8140" s="T338">[In] Syndassko half of them.</ta>
            <ta e="T347" id="Seg_8141" s="T341">The other half is far away there.</ta>
            <ta e="T351" id="Seg_8142" s="T347">Only my people here, thank God…</ta>
            <ta e="T358" id="Seg_8143" s="T351">There are more than fifty of our people only here, with children.</ta>
            <ta e="T728" id="Seg_8144" s="T358">Almost half(?) of Syndassko.</ta>
            <ta e="T364" id="Seg_8145" s="T362">My half of Syndassko.</ta>
            <ta e="T371" id="Seg_8146" s="T364">So I am living well with powerful and good wishes of the people.</ta>
            <ta e="T375" id="Seg_8147" s="T371">The people's good wishes, the old people's good wishes.</ta>
            <ta e="T381" id="Seg_8148" s="T375">Now, getting old, I am sick.</ta>
            <ta e="T382" id="Seg_8149" s="T381">I have [high] blood pressure.</ta>
            <ta e="T392" id="Seg_8150" s="T382">Very high, it is higher than two hundred.</ta>
            <ta e="T395" id="Seg_8151" s="T392">– It is twenty-two.</ta>
            <ta e="T400" id="Seg_8152" s="T395">I don't make it, my legs are failing.</ta>
            <ta e="T409" id="Seg_8153" s="T400">Now I don't reach a single child, I am lying [here].</ta>
            <ta e="T417" id="Seg_8154" s="T409">I will die soon, I'm not scared.</ta>
            <ta e="T419" id="Seg_8155" s="T417">– No.</ta>
            <ta e="T424" id="Seg_8156" s="T422">– I'll go to paradise.</ta>
            <ta e="T432" id="Seg_8157" s="T428">– Yes, I'll go to paradise.</ta>
            <ta e="T436" id="Seg_8158" s="T432">I have many children, they help me flying to paradise.</ta>
            <ta e="T441" id="Seg_8159" s="T439">– Near God.</ta>
            <ta e="T443" id="Seg_8160" s="T441">I'll go to God.</ta>
            <ta e="T451" id="Seg_8161" s="T447">– I don't know yet.</ta>
            <ta e="T453" id="Seg_8162" s="T451">The old people say: </ta>
            <ta e="T455" id="Seg_8163" s="T453">"You'll go to God".</ta>
            <ta e="T461" id="Seg_8164" s="T456">A person with many children, I'll go to God.</ta>
            <ta e="T464" id="Seg_8165" s="T461">I don't speak badly about people.</ta>
            <ta e="T466" id="Seg_8166" s="T464">I don't curse people.</ta>
            <ta e="T468" id="Seg_8167" s="T466">That's a sin.</ta>
            <ta e="T477" id="Seg_8168" s="T468">Therefore I can't curse people, that's a sin.</ta>
            <ta e="T481" id="Seg_8169" s="T477">Who[ever] comes in, I feed him.</ta>
            <ta e="T484" id="Seg_8170" s="T481">When my body makes it.</ta>
            <ta e="T489" id="Seg_8171" s="T484">Now I don't make it, just my Dunyasha is there.</ta>
            <ta e="T491" id="Seg_8172" s="T490">– She is good.</ta>
            <ta e="T495" id="Seg_8173" s="T491">My daughter-in-law is a very good woman.</ta>
            <ta e="T500" id="Seg_8174" s="T495">My daughters-in-law are good women indeed.</ta>
            <ta e="T507" id="Seg_8175" s="T500">However it may be, I don't annoy them.</ta>
            <ta e="T517" id="Seg_8176" s="T507">May they have a look [for me] or not, I don't need it, they are separate families, though.</ta>
            <ta e="T520" id="Seg_8177" s="T517">Why I should harass them?</ta>
            <ta e="T527" id="Seg_8178" s="T520">My life is coming to an end, I don't touch their lives.</ta>
            <ta e="T531" id="Seg_8179" s="T527">Shall they be happy, make an effort for me.</ta>
            <ta e="T536" id="Seg_8180" s="T531">That's all, what else shall I say?</ta>
            <ta e="T552" id="Seg_8181" s="T550">– Yes.</ta>
            <ta e="T563" id="Seg_8182" s="T560">– One mustn't.</ta>
            <ta e="T570" id="Seg_8183" s="T567">– Of course, of course one mustn't.</ta>
            <ta e="T575" id="Seg_8184" s="T570">The don't listen at all, the keep going.</ta>
            <ta e="T590" id="Seg_8185" s="T575">The children of the older sister, eh, of the younger sister of my man are going to the cemetery with my children.</ta>
            <ta e="T603" id="Seg_8186" s="T590">After his mother had died, the small son came for the first time, he goes, they accompany him.</ta>
            <ta e="T608" id="Seg_8187" s="T603">I say to him "don't go", but the don't listen.</ta>
            <ta e="T612" id="Seg_8188" s="T608">What do they have to do there?</ta>
            <ta e="T614" id="Seg_8189" s="T612">It's a sin, though.</ta>
            <ta e="T622" id="Seg_8190" s="T614">Now people are dying everywhere, terrible.</ta>
            <ta e="T636" id="Seg_8191" s="T622">Now one old woman has died there, now one child has come and has also died, poor child. </ta>
            <ta e="T640" id="Seg_8192" s="T636">It was a very good child.</ta>
            <ta e="T644" id="Seg_8193" s="T640">It was driving the snow scooter.</ta>
            <ta e="T666" id="Seg_8194" s="T661">– Yes, you have to throw tobacco there.</ta>
            <ta e="T674" id="Seg_8195" s="T668">– When they drink alcohol, you have to pour alcohol there.</ta>
            <ta e="T676" id="Seg_8196" s="T675">– That's all.</ta>
            <ta e="T692" id="Seg_8197" s="T687">Seldomly they go their father to the cemetery.</ta>
            <ta e="T697" id="Seg_8198" s="T692">They use to go to the graves of my mothers.</ta>
            <ta e="T706" id="Seg_8199" s="T697">And there are such shaman graves, that's bad, shamans.</ta>
            <ta e="T708" id="Seg_8200" s="T706">It's a sin.</ta>
            <ta e="T712" id="Seg_8201" s="T708">The young don't listen at all.</ta>
            <ta e="T715" id="Seg_8202" s="T713">Another people.</ta>
            <ta e="T719" id="Seg_8203" s="T715">At all another people </ta>
         </annotation>
         <annotation name="fg" tierref="fg-KiPP">
            <ta e="T0" id="Seg_8204" s="T1">– Warum sagst du "Sünde"?</ta>
            <ta e="T12" id="Seg_8205" s="T5">Sünde also, vor langer Zeit gab es Schamanen, Schamanen.</ta>
            <ta e="T17" id="Seg_8206" s="T12">Ihre Zelte kann man nicht umgehen, das ist schlecht.</ta>
            <ta e="T24" id="Seg_8207" s="T17">Wenn man geht, ohne drumherum zu gehen (?), ist das eine Sünde, das bedeutet Sünde.</ta>
            <ta e="T32" id="Seg_8208" s="T24">Wenn die Frau auf männliche Kleidung tritt, ist das auch eine Sünde.</ta>
            <ta e="T39" id="Seg_8209" s="T32">Sauber muss sie sein, der Mann muss sauber gehen.</ta>
            <ta e="T46" id="Seg_8210" s="T39">Wenn die Frau ohne Kleid geht, war das früher auch eine Sünde.</ta>
            <ta e="T50" id="Seg_8211" s="T46">Ein Kleid muss sie anziehen, ein Kleid.</ta>
            <ta e="T57" id="Seg_8212" s="T50">Wie die Männer, jetzt haben sie alle Hosen.</ta>
            <ta e="T61" id="Seg_8213" s="T57">Das ist Sünde, das.</ta>
            <ta e="T84" id="Seg_8214" s="T66">– Nun, also, alles wie beim Mann ist schlecht, die Frau soll wie eine Frau sein, sie soll ein Kleid und ein Tuch haben.</ta>
            <ta e="T723" id="Seg_8215" s="T84">Eine Mütze kann man natürlich aufsetzen, man muss eine Mütze aufsetzen.</ta>
            <ta e="T93" id="Seg_8216" s="T91">Nun solche.</ta>
            <ta e="T100" id="Seg_8217" s="T93">Wenn du heiratest, dann schlägt man mit der Hand auf die Ikone.</ta>
            <ta e="T724" id="Seg_8218" s="T100">So geben sich Leute, die heiraten, die Hand.</ta>
            <ta e="T725" id="Seg_8219" s="T106">Sie geben sich die Hand, dann, wenn sie sich die Hand geben, dann wird dieser Mensch zur Frau.</ta>
            <ta e="T118" id="Seg_8220" s="T115">Solche Bräuche, solche.</ta>
            <ta e="T130" id="Seg_8221" s="T118">Ja, unter Zwang verheiraten sie jemanden, wolle oder nicht, sie verheiraten [dich] trotzdem.</ta>
            <ta e="T134" id="Seg_8222" s="T130">Weinend gehst du dann zu ihm.</ta>
            <ta e="T141" id="Seg_8223" s="T134">So habe ich geheiratet, weinend.</ta>
            <ta e="T153" id="Seg_8224" s="T141">Als ich sechzehn war, was für Mensch heiratet mit sechzehn?</ta>
            <ta e="T154" id="Seg_8225" s="T153">Sie haben mich verheiratet.</ta>
            <ta e="T161" id="Seg_8226" s="T156">– Ganz jung, ich war noch sauber.</ta>
            <ta e="T165" id="Seg_8227" s="T161">Meine Regel hatte ich noch nicht.</ta>
            <ta e="T172" id="Seg_8228" s="T165">Dann kam meine große Tochter heraus.</ta>
            <ta e="T183" id="Seg_8229" s="T172">Dieses Kind wurde geboren, als ich siebzehn, achtzehn Jahre alt war.</ta>
            <ta e="T193" id="Seg_8230" s="T183">Dann als ich älter wurde, später dann.</ta>
            <ta e="T202" id="Seg_8231" s="T193">Du kriegst ein Kind nach dem anderen, die Liebe, die dunkle Liebe. </ta>
            <ta e="T207" id="Seg_8232" s="T202">Ein sehr guter Mensch war mein Mann.</ta>
            <ta e="T726" id="Seg_8233" s="T207">Na, der Mensch, der da sitzt.</ta>
            <ta e="T218" id="Seg_8234" s="T211">Das war, als ich neun Kinder hatte (?).</ta>
            <ta e="T223" id="Seg_8235" s="T218">Da war ich auch nicht mehr jung.</ta>
            <ta e="T727" id="Seg_8236" s="T223">Nun, solche Bräuche hatte man, "dann lebst du das Leben, gebt den Waisen zu essen", sagt man.</ta>
            <ta e="T241" id="Seg_8237" s="T234">"Die Waisenkinder, das ist eine Sünde, gebt den Waisenkindern zu essen", sagt man.</ta>
            <ta e="T247" id="Seg_8238" s="T241">"Geht mit einem Menschen, der etwas Schlechtes gemacht hat, gut um", sagt man.</ta>
            <ta e="T263" id="Seg_8239" s="T247">Wenn ich sage "warum ist es umgekehrt", dann ist es eine Sünde, "wenn du selber gut lebst, dann werden die anderen umgekehrt dich selbst beneiden", sagen die Alten.</ta>
            <ta e="T265" id="Seg_8240" s="T263">So ist der Brauch.</ta>
            <ta e="T276" id="Seg_8241" s="T265">Dann bist du glücklich, du selbst bist später glücklich, sagt man, jetzt bin ich glücklich.</ta>
            <ta e="T281" id="Seg_8242" s="T276">Ich war dann Waisin.</ta>
            <ta e="T292" id="Seg_8243" s="T281">Als meine Mutter starb, nachdem ich verheiratet wurde, äh, als ich noch nicht verheiratet war, gingen wir von Familie zu Familie.</ta>
            <ta e="T298" id="Seg_8244" s="T292">Dann, als meine Mutter starb, sagte sie so: </ta>
            <ta e="T303" id="Seg_8245" s="T298">"Gib Waisen immer zu essen."</ta>
            <ta e="T312" id="Seg_8246" s="T303">"Dem, der Schlechtes getan hat, musst du Gutes tun, dann bist du lange glücklich.</ta>
            <ta e="T319" id="Seg_8247" s="T312">Du wirst viele Kinder haben, ein vollkommenes Leben wirst du leben", sagte sie.</ta>
            <ta e="T321" id="Seg_8248" s="T319">Und tatsächlich, schau.</ta>
            <ta e="T331" id="Seg_8249" s="T321">Jetzt sind aus mir selbst über fünfzig Leute hervorgegangen.</ta>
            <ta e="T332" id="Seg_8250" s="T331">Ich bin mit meinen Enkeln zusammen.</ta>
            <ta e="T335" id="Seg_8251" s="T332">Etwas über fünfzig.</ta>
            <ta e="T338" id="Seg_8252" s="T335">Meine Leute, meine eigenen Leute.</ta>
            <ta e="T341" id="Seg_8253" s="T338">[In] Syndassko die Hälfte.</ta>
            <ta e="T347" id="Seg_8254" s="T341">Die [andere] Hälfte ist weit weg dort.</ta>
            <ta e="T351" id="Seg_8255" s="T347">Meine Hiesigen nur, zum Glück…</ta>
            <ta e="T358" id="Seg_8256" s="T351">Hier alleine sind über fünfzig unserer Leute, mit Kindern.</ta>
            <ta e="T728" id="Seg_8257" s="T358">Fast die Hälfte(?) von Syndassko.</ta>
            <ta e="T364" id="Seg_8258" s="T362">Meine Hälfte von Syndassko.</ta>
            <ta e="T371" id="Seg_8259" s="T364">So lebe ich gut mit kräftigen guten Wünschen der Leute.</ta>
            <ta e="T375" id="Seg_8260" s="T371">Die guten Wünsche der Leute, die guten Wünsche der Alten.</ta>
            <ta e="T381" id="Seg_8261" s="T375">Jetzt, wo ich alt bin, bin ich krank.</ta>
            <ta e="T382" id="Seg_8262" s="T381">Ich habe [hohen] Blutdruck. </ta>
            <ta e="T392" id="Seg_8263" s="T382">Sehr hoch, er ist über zweihundetrt. </ta>
            <ta e="T395" id="Seg_8264" s="T392">– Zweiundzwanzig ist es.</ta>
            <ta e="T400" id="Seg_8265" s="T395">Das schaffe ich nicht, meine Beine versagen.</ta>
            <ta e="T409" id="Seg_8266" s="T400">Jetzt komme ich nicht zu einem Kind, ich liege [herum]. </ta>
            <ta e="T417" id="Seg_8267" s="T409">Ich sterbe wahrscheinlich bald, ich habe keine Angst.</ta>
            <ta e="T419" id="Seg_8268" s="T417">– Nein.</ta>
            <ta e="T424" id="Seg_8269" s="T422">– Ich komme ins Paradies.</ta>
            <ta e="T432" id="Seg_8270" s="T428">– Ja, ich komme ins Paradies.</ta>
            <ta e="T436" id="Seg_8271" s="T432">Meine Kinder sind viele, die helfen mir, ins Paradies zu fliegen.</ta>
            <ta e="T441" id="Seg_8272" s="T439">– Bei Gott.</ta>
            <ta e="T443" id="Seg_8273" s="T441">Zu Gott gehe ich.</ta>
            <ta e="T451" id="Seg_8274" s="T447">– Weiß ich noch nicht.</ta>
            <ta e="T453" id="Seg_8275" s="T451">Das sagen die Alten: </ta>
            <ta e="T455" id="Seg_8276" s="T453">"Sie kommen zu Gott."</ta>
            <ta e="T461" id="Seg_8277" s="T456">Ein Mensch mit vielen Kindern, ich gehe zu Gott.</ta>
            <ta e="T464" id="Seg_8278" s="T461">Ich spreche nicht schlecht über Menschen.</ta>
            <ta e="T466" id="Seg_8279" s="T464">Ich verfluche Leute nicht.</ta>
            <ta e="T468" id="Seg_8280" s="T466">Das ist Sünde.</ta>
            <ta e="T477" id="Seg_8281" s="T468">Deshalb kann ich Leute nicht verfluchen, das ist Sünde.</ta>
            <ta e="T481" id="Seg_8282" s="T477">Wer auch hereinkommt, ich gebe [ihm] zu essen.</ta>
            <ta e="T484" id="Seg_8283" s="T481">Wenn mein Körper es schafft.</ta>
            <ta e="T489" id="Seg_8284" s="T484">Jetzt schaffe ich es nicht, nur meine Dunjascha ist. </ta>
            <ta e="T491" id="Seg_8285" s="T490">– Sie ist gut.</ta>
            <ta e="T495" id="Seg_8286" s="T491">Meine Schwiegertochter ist eine sehr gute Frau.</ta>
            <ta e="T500" id="Seg_8287" s="T495">Gute Frauen sind meine Schwiegertöchter in der Tat.</ta>
            <ta e="T507" id="Seg_8288" s="T500">Wie es auch sei, ich belästige sie nicht. </ta>
            <ta e="T517" id="Seg_8289" s="T507">Sollen sie [nach mir] gucken oder nicht, ich brauche es nicht, sie sind eigene Familien.</ta>
            <ta e="T520" id="Seg_8290" s="T517">Wozu sollte ich sie belästigen?</ta>
            <ta e="T527" id="Seg_8291" s="T520">Mein Leben geht zuende, in ihre Leben mische ich mich nicht ein.</ta>
            <ta e="T531" id="Seg_8292" s="T527">Sollen sie glücklich sein, sich um mich bemühen.</ta>
            <ta e="T536" id="Seg_8293" s="T531">Das ist alles, was lässt du mich noch sagen?</ta>
            <ta e="T552" id="Seg_8294" s="T550">– Ja.</ta>
            <ta e="T563" id="Seg_8295" s="T560">– Man darf nicht.</ta>
            <ta e="T570" id="Seg_8296" s="T567">– Natürlich, natürlich darf man nicht.</ta>
            <ta e="T575" id="Seg_8297" s="T570">Sie hören nicht drauf, sie gehen.</ta>
            <ta e="T590" id="Seg_8298" s="T575">Die Kinder der älteren Schwester, äh, der jüngeren Schwester meines Mannes gehen auf den Friedhof mit meinen Kindern.</ta>
            <ta e="T603" id="Seg_8299" s="T590">Nach dem Tod der Mutter kam der kleine Sohn zum ersten Mal, der geht, den begleiten sie.</ta>
            <ta e="T608" id="Seg_8300" s="T603">Ich sage ihm "geht nicht", sie hören nicht.</ta>
            <ta e="T612" id="Seg_8301" s="T608">Was haben sie dort zu tun?</ta>
            <ta e="T614" id="Seg_8302" s="T612">Das ist doch Sünde.</ta>
            <ta e="T622" id="Seg_8303" s="T614">Jetzt sterben überall Menschen, furchtbar.</ta>
            <ta e="T636" id="Seg_8304" s="T622">Jetzt ist eine alte Frau gestorben, jetzt kam ein Kind und starb auch, armes Kind.</ta>
            <ta e="T640" id="Seg_8305" s="T636">Das war ein sehr gutes Kind.</ta>
            <ta e="T644" id="Seg_8306" s="T640">Es fuhr mit dem Schneemobil.</ta>
            <ta e="T666" id="Seg_8307" s="T661">– Ja, Tabak muss man hinwerfen.</ta>
            <ta e="T674" id="Seg_8308" s="T668">– Wenn sie Schnaps trinken, muss man Schnaps draufgießen.</ta>
            <ta e="T676" id="Seg_8309" s="T675">– Das ist alles.</ta>
            <ta e="T692" id="Seg_8310" s="T687">Selten gehen sie zu ihren Vätern auf den Friedhof.</ta>
            <ta e="T697" id="Seg_8311" s="T692">Zu den Gräbern meiner Mütter gehen sie.</ta>
            <ta e="T706" id="Seg_8312" s="T697">Und es gibt solche Schamanengräber, das ist schlecht, Schamanen.</ta>
            <ta e="T708" id="Seg_8313" s="T706">Das ist Sünde.</ta>
            <ta e="T712" id="Seg_8314" s="T708">Die Jugend hört überhaupt nicht.</ta>
            <ta e="T715" id="Seg_8315" s="T713">Ein anderes Volk.</ta>
            <ta e="T719" id="Seg_8316" s="T715">Ein ganz anderes Volk.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KiPP">
            <ta e="T0" id="Seg_8317" s="T1">— Почему ты говоришь "грех"?</ta>
            <ta e="T12" id="Seg_8318" s="T5">Грех это, раньше шаманы были, шаманы.</ta>
            <ta e="T17" id="Seg_8319" s="T12">Их дома обходить не положено, это плохо.</ta>
            <ta e="T24" id="Seg_8320" s="T17">Если (не?) обходишь вокруг, это грех, это называется грех.</ta>
            <ta e="T32" id="Seg_8321" s="T24">Если женщина наступает на мужскую одежду, тоже грех.</ta>
            <ta e="T39" id="Seg_8322" s="T32">Она чистой должна быть, мужчине чистым ходить надо.</ta>
            <ta e="T46" id="Seg_8323" s="T39">Женщине ходить без платья тоже грех раньше был.</ta>
            <ta e="T50" id="Seg_8324" s="T46">Надо платье надевать.</ta>
            <ta e="T57" id="Seg_8325" s="T50">Сейчас на мужчин похожи, все в штанах.</ta>
            <ta e="T61" id="Seg_8326" s="T57">Это грех, вот это.</ta>
            <ta e="T84" id="Seg_8327" s="T66">— Вот когда все на мужчин похожи, это плохо, женщина должна быть как женщина, в платье, в платке быть.</ta>
            <ta e="T723" id="Seg_8328" s="T84">Шапку-то можно носить, конечно, надо шапку.</ta>
            <ta e="T93" id="Seg_8329" s="T91">Вот такие.</ta>
            <ta e="T100" id="Seg_8330" s="T93">Потом, когда выходят замуж, ударяют по рукам перед богом.</ta>
            <ta e="T724" id="Seg_8331" s="T100">Вот так дают руки, когда выходят замуж.</ta>
            <ta e="T725" id="Seg_8332" s="T106">Подают друг другу руки, после этого этот человек становится женой.</ta>
            <ta e="T118" id="Seg_8333" s="T115">Вот такие обычаи.</ta>
            <ta e="T130" id="Seg_8334" s="T118">Да, насильно выдают замуж, хочешь не хочешь, всё равно выдают.</ta>
            <ta e="T134" id="Seg_8335" s="T130">Тогда плачешь, а всё равно идёшь к нему.</ta>
            <ta e="T141" id="Seg_8336" s="T134">Вот так я замуж выходила, плача.</ta>
            <ta e="T153" id="Seg_8337" s="T141">Когда мне было шестнадцать лет, кто замуж выходит в шестнадцать лет?</ta>
            <ta e="T154" id="Seg_8338" s="T153">Выдали.</ta>
            <ta e="T161" id="Seg_8339" s="T156">— Совсем молодая, чистая была к тому же.</ta>
            <ta e="T165" id="Seg_8340" s="T161">Месячных у меня ещё не было.</ta>
            <ta e="T172" id="Seg_8341" s="T165">Потом это вышло, старшая дочка сейчас.</ta>
            <ta e="T183" id="Seg_8342" s="T172">Этот ребёнок родился, когда мне было семнадцать-восемнадцать лет.</ta>
            <ta e="T193" id="Seg_8343" s="T183">Потом, когда я взрослела, позже…</ta>
            <ta e="T202" id="Seg_8344" s="T193">Один ребёнок за другим, ночь, тёмная любовь.</ta>
            <ta e="T207" id="Seg_8345" s="T202">Очень хороший человек был мой муж.</ta>
            <ta e="T726" id="Seg_8346" s="T207">Вот этот, который тут сидит.</ta>
            <ta e="T218" id="Seg_8347" s="T211">Это когда у меня было девять детей.</ta>
            <ta e="T223" id="Seg_8348" s="T218">Тут я уже не молодая.</ta>
            <ta e="T727" id="Seg_8349" s="T223">Вот такие обычаи были, говорят "такой жизнью живёшь, сирот накорми".</ta>
            <ta e="T241" id="Seg_8350" s="T234">"Сироты, это грех будет, накормите сирот," говорят.</ta>
            <ta e="T247" id="Seg_8351" s="T241">"Кто вам плохое сделал, того хорошо проводите," говорят.</ta>
            <ta e="T263" id="Seg_8352" s="T247">"Почему всё наоборот бывает," спрашиваю, грех будет потом, "Когда сама будешь жить хорошо, они, наоборот, будут тебе завидовать," говорят старики.</ta>
            <ta e="T265" id="Seg_8353" s="T263">Обычаи такие.</ta>
            <ta e="T276" id="Seg_8354" s="T265">"Чтобы потом ты была счастлива, после счастлива была," сказали, теперь я счастлива.</ta>
            <ta e="T281" id="Seg_8355" s="T276">Тогда я сиротой была.</ta>
            <ta e="T292" id="Seg_8356" s="T281">Когда мама умерла, после замужества, э-э, до замужества, мы от семьи к семье ходили.</ta>
            <ta e="T298" id="Seg_8357" s="T292">Тогда, когда мама умирала, она сказала:</ta>
            <ta e="T303" id="Seg_8358" s="T298">"Сирот всегда корми".</ta>
            <ta e="T312" id="Seg_8359" s="T303">"Тем, кто делает плохое, надо делать хорошее, тогда долгое счастье будет.</ta>
            <ta e="T319" id="Seg_8360" s="T312">Много детей будет, обустроенная жизнь будет," сказала.</ta>
            <ta e="T321" id="Seg_8361" s="T319">И правда, смотри.</ta>
            <ta e="T331" id="Seg_8362" s="T321">Мне самой сейчас больше пятидесяти, человек из меня вышел.</ta>
            <ta e="T332" id="Seg_8363" s="T331">Вместе с внуками.</ta>
            <ta e="T335" id="Seg_8364" s="T332">Пятьдесят с лишним.</ta>
            <ta e="T338" id="Seg_8365" s="T335">Народ, мой народ.</ta>
            <ta e="T341" id="Seg_8366" s="T338">Половина в Сындасско.</ta>
            <ta e="T347" id="Seg_8367" s="T341">Другая половина ещё там далеко.</ta>
            <ta e="T351" id="Seg_8368" s="T347">Здешних только, слава богу…</ta>
            <ta e="T358" id="Seg_8369" s="T351">Здесь только больше пятидесяти человек с детьми.</ta>
            <ta e="T728" id="Seg_8370" s="T358">Почти половина Сындасско.</ta>
            <ta e="T364" id="Seg_8371" s="T362">Моя половина Сындасско.</ta>
            <ta e="T371" id="Seg_8372" s="T364">Так хорошо живу, люди сильно добра желают.</ta>
            <ta e="T375" id="Seg_8373" s="T371">Пожеланиями людей, пожеланиями стариков.</ta>
            <ta e="T381" id="Seg_8374" s="T375">Сейчас постарела, стала болеть.</ta>
            <ta e="T382" id="Seg_8375" s="T381">Давление у меня.</ta>
            <ta e="T392" id="Seg_8376" s="T382">Очень высокое, выше двухсот.</ta>
            <ta e="T395" id="Seg_8377" s="T392">— Двадцать два.</ta>
            <ta e="T400" id="Seg_8378" s="T395">Не переношу, ноги отказывают.</ta>
            <ta e="T409" id="Seg_8379" s="T400">Сейчас ни до одного ребёнка дойти не могу, лежу вот.</ta>
            <ta e="T417" id="Seg_8380" s="T409">Скоро умру, наверно, я не боюсь.</ta>
            <ta e="T419" id="Seg_8381" s="T417">— Нет.</ta>
            <ta e="T424" id="Seg_8382" s="T422">— Я в рай пойду.</ta>
            <ta e="T432" id="Seg_8383" s="T428">— Да, я в рай пойду.</ta>
            <ta e="T436" id="Seg_8384" s="T432">У меня детей много, они помогут в рай полететь.</ta>
            <ta e="T441" id="Seg_8385" s="T439">— К богу.</ta>
            <ta e="T443" id="Seg_8386" s="T441">К богу пойду.</ta>
            <ta e="T451" id="Seg_8387" s="T447">— Ещё не знаю.</ta>
            <ta e="T453" id="Seg_8388" s="T451">Старики говорят:</ta>
            <ta e="T455" id="Seg_8389" s="T453">"Пойдёте к богу".</ta>
            <ta e="T461" id="Seg_8390" s="T456">У меня детей много, я к богу пойду.</ta>
            <ta e="T464" id="Seg_8391" s="T461">О людях я плохо не говорю.</ta>
            <ta e="T466" id="Seg_8392" s="T464">Людям плохого не желаю.</ta>
            <ta e="T468" id="Seg_8393" s="T466">Грех будет.</ta>
            <ta e="T477" id="Seg_8394" s="T468">Поэтому я людям плохого не умею желать, грех будет.</ta>
            <ta e="T481" id="Seg_8395" s="T477">Кто зайдёт, всех накормлю.</ta>
            <ta e="T484" id="Seg_8396" s="T481">Когда была в состоянии.</ta>
            <ta e="T489" id="Seg_8397" s="T484">Сейчас не могу, Дуняша только может.</ta>
            <ta e="T491" id="Seg_8398" s="T490">— Хорошая.</ta>
            <ta e="T495" id="Seg_8399" s="T491">Невестка моя очень хорошая женщина.</ta>
            <ta e="T500" id="Seg_8400" s="T495">Мои невестки действительно хорошие женщины.</ta>
            <ta e="T507" id="Seg_8401" s="T500">Как бы то ни было, я их не беспокою.</ta>
            <ta e="T517" id="Seg_8402" s="T507">Ругаются, не ругаются, мне не важно, они отдельная семья.</ta>
            <ta e="T520" id="Seg_8403" s="T517">Зачем мне о них беспокоиться?</ta>
            <ta e="T527" id="Seg_8404" s="T520">Моя жизнь прошла, их жизни я не трогаю.</ta>
            <ta e="T531" id="Seg_8405" s="T527">Пусть живут счастливые, (?).</ta>
            <ta e="T536" id="Seg_8406" s="T531">Всё, что ты ещё хочешь, чтобы я сказала?</ta>
            <ta e="T552" id="Seg_8407" s="T550">— Да.</ta>
            <ta e="T563" id="Seg_8408" s="T560">— Нельзя.</ta>
            <ta e="T570" id="Seg_8409" s="T567">— Конечно, конечно нельзя.</ta>
            <ta e="T575" id="Seg_8410" s="T570">Не слушаются совсем, они ходят.</ta>
            <ta e="T590" id="Seg_8411" s="T575">Дети старшей сестры моего мужа, э-э, младшей сестры мужа, ходят на кладбище с моими детьми.</ta>
            <ta e="T603" id="Seg_8412" s="T590">После смерти мамы в первый раз приехал младший сын, он идёт, те его сопровождают.</ta>
            <ta e="T608" id="Seg_8413" s="T603">Я ему говорю, не ходите, но они не слушаются.</ta>
            <ta e="T612" id="Seg_8414" s="T608">Что им там делать?</ta>
            <ta e="T614" id="Seg_8415" s="T612">Грех ведь.</ta>
            <ta e="T622" id="Seg_8416" s="T614">Сейчас во всём мире люди умирают, страшно.</ta>
            <ta e="T636" id="Seg_8417" s="T622">Сейчас одна бабушка умерла, сейчас один ребёнок ехал сюда и умер, жалко ребёнка.</ta>
            <ta e="T640" id="Seg_8418" s="T636">Очень хороший ребёнок был.</ta>
            <ta e="T644" id="Seg_8419" s="T640">На буране возил.</ta>
            <ta e="T666" id="Seg_8420" s="T661">— Да, табак бросить надо.</ta>
            <ta e="T674" id="Seg_8421" s="T668">— Когда пьют, надо наливать спиртное.</ta>
            <ta e="T676" id="Seg_8422" s="T675">— Всё.</ta>
            <ta e="T692" id="Seg_8423" s="T687">— Редко они ходят на могилу к отцам.</ta>
            <ta e="T697" id="Seg_8424" s="T692">А к матерям на могилы так-то ходят.</ta>
            <ta e="T706" id="Seg_8425" s="T697">И есть такие шаманские могилы, это плохо, шаманские.</ta>
            <ta e="T708" id="Seg_8426" s="T706">Это грех.</ta>
            <ta e="T712" id="Seg_8427" s="T708">Молодёжь сейчас совсем не слушается.</ta>
            <ta e="T715" id="Seg_8428" s="T713">Другой народ.</ta>
            <ta e="T719" id="Seg_8429" s="T715">Совсем другой народ.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr-KiPP">
            <ta e="T0" id="Seg_8430" s="T1">ot chego greh govorish tɨ</ta>
            <ta e="T12" id="Seg_8431" s="T5">greh eto shanshe shamanɨ bɨli shamanɨ</ta>
            <ta e="T17" id="Seg_8432" s="T12">ihnie doma obhodit ne polozhenno, ploho</ta>
            <ta e="T24" id="Seg_8433" s="T17">vokrug hodit eto greh eto nazɨvaetsja greh</ta>
            <ta e="T32" id="Seg_8434" s="T24">zhenshine muzhskuju odeshdu perestupat tozhe greh</ta>
            <ta e="T39" id="Seg_8435" s="T32">chistij dolzhno bɨt muzhchine chistim hodit nado</ta>
            <ta e="T46" id="Seg_8436" s="T39">zhenshina hodit bes platja tozne greh ranshe</ta>
            <ta e="T50" id="Seg_8437" s="T46">platje odevat nado, platje</ta>
            <ta e="T57" id="Seg_8438" s="T50">na muzhchin pohozhɨ seichas vse v shtanakh</ta>
            <ta e="T61" id="Seg_8439" s="T57">eto greh, to</ta>
            <ta e="T84" id="Seg_8440" s="T66">vse na mushshin pohozhi ploho eto zhenshina kak zhenshina dolzhna bɨt v plate v platke nado bɨt </ta>
            <ta e="T723" id="Seg_8441" s="T84">shapku to mozhno odevat konechno, nado shapku</ta>
            <ta e="T93" id="Seg_8442" s="T91">vot takie</ta>
            <ta e="T100" id="Seg_8443" s="T93">kogda vɨhodjat zamuzh ruki udarjajut bogu</ta>
            <ta e="T724" id="Seg_8444" s="T100">vot tak dajut ruki zamuzh vzhodjashie ljudi (kogda vɨhodit zamuzh chwlovek</ta>
            <ta e="T725" id="Seg_8445" s="T106">ruki podajut drug drugu, togda posle rukopozhatia zhenoj stanovitsja etot chelovek</ta>
            <ta e="T118" id="Seg_8446" s="T115">takie obɨčia takie</ta>
            <ta e="T130" id="Seg_8447" s="T118">da nasilno otdajut zhenshinu (chelovek) hochesh ne hochesh vse ravno otdajut</ta>
            <ta e="T134" id="Seg_8448" s="T130">plachesh plachesh vse ravno idesh k nemu</ta>
            <ta e="T141" id="Seg_8449" s="T134">vot tak ja vɨhodila zamuzh plachu</ta>
            <ta e="T153" id="Seg_8450" s="T141">v shestnat let, kakoj chelovek zamuzh vɨdet v shestnatsat let</ta>
            <ta e="T154" id="Seg_8451" s="T153">vɨdali</ta>
            <ta e="T161" id="Seg_8452" s="T156">sovsem rebenok chistaja bɨla k tomu zhe</ta>
            <ta e="T165" id="Seg_8453" s="T161">mesjachnɨh nebɨlo</ta>
            <ta e="T172" id="Seg_8454" s="T165">potom eto vɨshlo starshaja dochka seichas</ta>
            <ta e="T183" id="Seg_8455" s="T172">etot etot rebenok semnadsat vosemnadsat let etot rebenok rodilas </ta>
            <ta e="T193" id="Seg_8456" s="T183">potom posle togo kak ja starela, pozzhe pozzhe potom</ta>
            <ta e="T202" id="Seg_8457" s="T193">rebenok za rebenkom idesh noch temjnaja ljubov</ta>
            <ta e="T207" id="Seg_8458" s="T202">ochen horoshim chelovekom bɨl muzh</ta>
            <ta e="T726" id="Seg_8459" s="T207">vot kotorɨ sidit etot</ta>
            <ta e="T218" id="Seg_8460" s="T211">eto kogda u menja bɨlo 9 detej</ta>
            <ta e="T223" id="Seg_8461" s="T218">uzhe ja ne molodaja zdes</ta>
            <ta e="T727" id="Seg_8462" s="T223">vot takie obichie bili potom takoj zhiznju zhivesh, sirot nakormi govorjat</ta>
            <ta e="T241" id="Seg_8463" s="T234">sirot greh budet, nakormite govorili</ta>
            <ta e="T247" id="Seg_8464" s="T241">ktko vam ploho sdelal horosho provodite govorjat</ta>
            <ta e="T263" id="Seg_8465" s="T247">pochemu vse naoborot bɨvaet sprashivaju greh budet potom, kogda sama budesh zhit horosho obratno oni budut tebe zavidovat govorjat stariki</ta>
            <ta e="T265" id="Seg_8466" s="T263">obɨchie takie</ta>
            <ta e="T276" id="Seg_8467" s="T265">chtobz potom bɨla schastlivo potom schastlovo budesh skazali ja seichas schastliva</ta>
            <ta e="T281" id="Seg_8468" s="T276">sirota ja bɨla potom</ta>
            <ta e="T292" id="Seg_8469" s="T281">mama umerla posle zamuzhestv do zamuzhestva ot semi k semje hodili</ta>
            <ta e="T298" id="Seg_8470" s="T292">togda kogda mam umirala govorit, </ta>
            <ta e="T303" id="Seg_8471" s="T298">kaja sirot kormi vsegda</ta>
            <ta e="T312" id="Seg_8472" s="T303">sdelavshimu ploho delaj horosho potom dlinnoe schaste budet</ta>
            <ta e="T319" id="Seg_8473" s="T312">mnogo detej budet, gotovaja zhizn budet, (obustroeːno) ne nuzhdatsja) skazala</ta>
            <ta e="T321" id="Seg_8474" s="T319">pravda smotri</ta>
            <ta e="T331" id="Seg_8475" s="T321">seichas ja sama bolshe pjatidesjati chelovek iz menja vɨshlo</ta>
            <ta e="T332" id="Seg_8476" s="T331">vmeste s vnukami</ta>
            <ta e="T338" id="Seg_8477" s="T335">narod moj narod</ta>
            <ta e="T341" id="Seg_8478" s="T338">sɨndassko polovina</ta>
            <ta e="T347" id="Seg_8479" s="T341">polovina daleko est esho tam</ta>
            <ta e="T351" id="Seg_8480" s="T347">zhedshnih tolko slava bogu</ta>
            <ta e="T358" id="Seg_8481" s="T351">zdes tolko nahoditsja bolshe 50 chelovek vmeste s detmi</ta>
            <ta e="T728" id="Seg_8482" s="T358">pochti kusok polovini sɨndassko</ta>
            <ta e="T364" id="Seg_8483" s="T362">moja polovina sɨndassko</ta>
            <ta e="T371" id="Seg_8484" s="T364">tak horosho zhivu ochen horosho ljudi pozhelajut horosheː</ta>
            <ta e="T375" id="Seg_8485" s="T371">pozhelaniami ljudej pozhelaniami starikov</ta>
            <ta e="T381" id="Seg_8486" s="T375">seichas postarev stalo bolet</ta>
            <ta e="T382" id="Seg_8487" s="T381">u menja davlenie</ta>
            <ta e="T392" id="Seg_8488" s="T382">ochen vɨsokoe bolshe dvesti</ta>
            <ta e="T400" id="Seg_8489" s="T395">ne perenoshu nogi otkazɨvajut</ta>
            <ta e="T409" id="Seg_8490" s="T400">seichas ne no odnogo rebenka doiti ne mogu, lezhu vot</ta>
            <ta e="T424" id="Seg_8491" s="T422">ja va raj poidu</ta>
            <ta e="T436" id="Seg_8492" s="T432">detej mnogo oni pomogut vɨletit v roj</ta>
            <ta e="T441" id="Seg_8493" s="T439">k bogu</ta>
            <ta e="T443" id="Seg_8494" s="T441">k bogu poidu</ta>
            <ta e="T453" id="Seg_8495" s="T451">govorjat stariki </ta>
            <ta e="T455" id="Seg_8496" s="T453">poidete k bogu</ta>
            <ta e="T461" id="Seg_8497" s="T456">u menja mnogo detej k bogu poidu</ta>
            <ta e="T464" id="Seg_8498" s="T461">pro ljudej ploho ne govorju</ta>
            <ta e="T466" id="Seg_8499" s="T464">ljudej ne zhelaju plohoe</ta>
            <ta e="T468" id="Seg_8500" s="T466">greh budet</ta>
            <ta e="T477" id="Seg_8501" s="T468">poetomu ja plohoe ne umeju govorit</ta>
            <ta e="T481" id="Seg_8502" s="T477">kto zajdet vseh nakormlju</ta>
            <ta e="T484" id="Seg_8503" s="T481">kogda sama mogla (bɨla v sostojaniː)</ta>
            <ta e="T489" id="Seg_8504" s="T484">seichas ne mogu Dunjasha tolko (mozhet)</ta>
            <ta e="T495" id="Seg_8505" s="T491">nevestka ochen horoshaja zhenshina</ta>
            <ta e="T500" id="Seg_8506" s="T495">horoshie zhenishɨ moi nevestki tak da</ta>
            <ta e="T507" id="Seg_8507" s="T500">hot kak ja ih ne kosajus (kihillibappin)</ta>
            <ta e="T517" id="Seg_8508" s="T507">rugajutsja ne rugajutsja mne ne nado oni otdelnaja semja</ta>
            <ta e="T520" id="Seg_8509" s="T517">zachem mne ih kosatsja (menja eto ne kosaetsja)</ta>
            <ta e="T527" id="Seg_8510" s="T520">zhizn moja proshla ihnzh zhizn ne trogaju</ta>
            <ta e="T531" id="Seg_8511" s="T527">pust zhivut schastllivze</ta>
            <ta e="T536" id="Seg_8512" s="T531">vse potom chto tɨ hochesh chtobz ja skazala</ta>
            <ta e="T575" id="Seg_8513" s="T570">ne slushajutsja sovsem oni hodjat</ta>
            <ta e="T590" id="Seg_8514" s="T575">moego muzha sestra sestra starshaja, sestrɨ mladshie deti idut vmeste s moimi detmi</ta>
            <ta e="T603" id="Seg_8515" s="T590">posle smerti mamɨ pervɨj raɨ priehal mladshij sɨn, tot idet ego soprovozhdajut</ta>
            <ta e="T608" id="Seg_8516" s="T603">emu govorju ne hodite ne slushajutsja buo</ta>
            <ta e="T612" id="Seg_8517" s="T608">chto im tam delat</ta>
            <ta e="T622" id="Seg_8518" s="T614">seichas vo vsem mire ljudi umirajut strashno</ta>
            <ta e="T636" id="Seg_8519" s="T622">seichas odna babushka umerla, seichas odin rebenok ehal sjuda i umer, zhalka rebenka</ta>
            <ta e="T640" id="Seg_8520" s="T636">ochen horosij rebenok bɨl</ta>
            <ta e="T644" id="Seg_8521" s="T640">vozil na burane</ta>
            <ta e="T666" id="Seg_8522" s="T661">da, tabak brosit nado</ta>
            <ta e="T674" id="Seg_8523" s="T668">kogda pjut nado nalivat spirtnoe</ta>
            <ta e="T676" id="Seg_8524" s="T675">konets</ta>
            <ta e="T692" id="Seg_8525" s="T687">redko oni hodjat na mogilu k otsam</ta>
            <ta e="T697" id="Seg_8526" s="T692">a k maminam mogilam tak to hodjat</ta>
            <ta e="T706" id="Seg_8527" s="T697">eto est shamanskie mogilɨ tam est, eto ploho, shamanɨ</ta>
            <ta e="T712" id="Seg_8528" s="T708">ne slushajutsja seichas moldesh sovsem</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KiPP">
            <ta e="T218" id="Seg_8529" s="T211">[DCh]: Verb form not clearly audible and interpretable.</ta>
            <ta e="T331" id="Seg_8530" s="T321">[DCh]: Valency and person-number-endings do not fit here.</ta>
         </annotation>
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-SE"
                      id="tx-SE"
                      speaker="SE"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-SE">
            <ts e="T67" id="Seg_8531" n="sc" s="T62">
               <ts e="T67" id="Seg_8533" n="HIAT:u" s="T62">
                  <nts id="Seg_8534" n="HIAT:ip">–</nts>
                  <nts id="Seg_8535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_8537" n="HIAT:w" s="T62">A</ts>
                  <nts id="Seg_8538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_8540" n="HIAT:w" s="T63">togo</ts>
                  <nts id="Seg_8541" n="HIAT:ip">,</nts>
                  <nts id="Seg_8542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_8544" n="HIAT:w" s="T64">togo</ts>
                  <nts id="Seg_8545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_8547" n="HIAT:w" s="T65">gri͡ex</ts>
                  <nts id="Seg_8548" n="HIAT:ip">?</nts>
                  <nts id="Seg_8549" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T156" id="Seg_8550" n="sc" s="T154">
               <ts e="T156" id="Seg_8552" n="HIAT:u" s="T154">
                  <nts id="Seg_8553" n="HIAT:ip">–</nts>
                  <nts id="Seg_8554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_8556" n="HIAT:w" s="T154">Savsʼem</ts>
                  <nts id="Seg_8557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_8559" n="HIAT:w" s="T155">ogo</ts>
                  <nts id="Seg_8560" n="HIAT:ip">.</nts>
                  <nts id="Seg_8561" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T389" id="Seg_8562" n="sc" s="T385">
               <ts e="T389" id="Seg_8564" n="HIAT:u" s="T385">
                  <nts id="Seg_8565" n="HIAT:ip">–</nts>
                  <nts id="Seg_8566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_8568" n="HIAT:w" s="T385">Vɨsoːkaje</ts>
                  <nts id="Seg_8569" n="HIAT:ip">,</nts>
                  <nts id="Seg_8570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_8572" n="HIAT:w" s="T387">da</ts>
                  <nts id="Seg_8573" n="HIAT:ip">?</nts>
                  <nts id="Seg_8574" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T418" id="Seg_8575" n="sc" s="T416">
               <ts e="T418" id="Seg_8577" n="HIAT:u" s="T416">
                  <nts id="Seg_8578" n="HIAT:ip">–</nts>
                  <nts id="Seg_8579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_8581" n="HIAT:w" s="T416">Nʼet</ts>
                  <nts id="Seg_8582" n="HIAT:ip">?</nts>
                  <nts id="Seg_8583" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T422" id="Seg_8584" n="sc" s="T419">
               <ts e="T422" id="Seg_8586" n="HIAT:u" s="T419">
                  <nts id="Seg_8587" n="HIAT:ip">–</nts>
                  <nts id="Seg_8588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_8590" n="HIAT:w" s="T419">A</ts>
                  <nts id="Seg_8591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_8593" n="HIAT:w" s="T420">kuda</ts>
                  <nts id="Seg_8594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_8596" n="HIAT:w" s="T421">idʼotʼe</ts>
                  <nts id="Seg_8597" n="HIAT:ip">?</nts>
                  <nts id="Seg_8598" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T429" id="Seg_8599" n="sc" s="T424">
               <ts e="T426" id="Seg_8601" n="HIAT:u" s="T424">
                  <nts id="Seg_8602" n="HIAT:ip">–</nts>
                  <nts id="Seg_8603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_8605" n="HIAT:w" s="T424">Ah</ts>
                  <nts id="Seg_8606" n="HIAT:ip">,</nts>
                  <nts id="Seg_8607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_8608" n="HIAT:ip">(</nts>
                  <nts id="Seg_8609" n="HIAT:ip">(</nts>
                  <ats e="T426" id="Seg_8610" n="HIAT:non-pho" s="T425">LAUGH</ats>
                  <nts id="Seg_8611" n="HIAT:ip">)</nts>
                  <nts id="Seg_8612" n="HIAT:ip">)</nts>
                  <nts id="Seg_8613" n="HIAT:ip">.</nts>
                  <nts id="Seg_8614" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T429" id="Seg_8616" n="HIAT:u" s="T426">
                  <ts e="T427" id="Seg_8618" n="HIAT:w" s="T426">Arajga</ts>
                  <nts id="Seg_8619" n="HIAT:ip">,</nts>
                  <nts id="Seg_8620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_8622" n="HIAT:w" s="T427">da</ts>
                  <nts id="Seg_8623" n="HIAT:ip">?</nts>
                  <nts id="Seg_8624" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T439" id="Seg_8625" n="sc" s="T436">
               <ts e="T439" id="Seg_8627" n="HIAT:u" s="T436">
                  <nts id="Seg_8628" n="HIAT:ip">–</nts>
                  <nts id="Seg_8629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_8631" n="HIAT:w" s="T436">A</ts>
                  <nts id="Seg_8632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_8634" n="HIAT:w" s="T437">kajdak</ts>
                  <nts id="Seg_8635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_8637" n="HIAT:w" s="T438">raj</ts>
                  <nts id="Seg_8638" n="HIAT:ip">?</nts>
                  <nts id="Seg_8639" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T447" id="Seg_8640" n="sc" s="T443">
               <ts e="T447" id="Seg_8642" n="HIAT:u" s="T443">
                  <nts id="Seg_8643" n="HIAT:ip">–</nts>
                  <nts id="Seg_8644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_8646" n="HIAT:w" s="T443">Kak</ts>
                  <nts id="Seg_8647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_8649" n="HIAT:w" s="T444">tam</ts>
                  <nts id="Seg_8650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_8652" n="HIAT:w" s="T445">vɨglʼadʼit-ta</ts>
                  <nts id="Seg_8653" n="HIAT:ip">?</nts>
                  <nts id="Seg_8654" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T490" id="Seg_8655" n="sc" s="T489">
               <ts e="T490" id="Seg_8657" n="HIAT:u" s="T489">
                  <nts id="Seg_8658" n="HIAT:ip">–</nts>
                  <nts id="Seg_8659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_8661" n="HIAT:w" s="T489">Üčügej</ts>
                  <nts id="Seg_8662" n="HIAT:ip">?</nts>
                  <nts id="Seg_8663" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T550" id="Seg_8664" n="sc" s="T536">
               <ts e="T550" id="Seg_8666" n="HIAT:u" s="T536">
                  <nts id="Seg_8667" n="HIAT:ip">–</nts>
                  <nts id="Seg_8668" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_8670" n="HIAT:w" s="T536">Možetʼe</ts>
                  <nts id="Seg_8671" n="HIAT:ip">,</nts>
                  <nts id="Seg_8672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_8674" n="HIAT:w" s="T537">vɨ</ts>
                  <nts id="Seg_8675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_8677" n="HIAT:w" s="T538">pomnʼitʼe</ts>
                  <nts id="Seg_8678" n="HIAT:ip">,</nts>
                  <nts id="Seg_8679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_8681" n="HIAT:w" s="T539">kakʼije</ts>
                  <nts id="Seg_8682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_8684" n="HIAT:w" s="T540">jestʼ</ts>
                  <nts id="Seg_8685" n="HIAT:ip">,</nts>
                  <nts id="Seg_8686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_8688" n="HIAT:w" s="T541">vot</ts>
                  <nts id="Seg_8689" n="HIAT:ip">,</nts>
                  <nts id="Seg_8690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_8692" n="HIAT:w" s="T542">kak</ts>
                  <nts id="Seg_8693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_8695" n="HIAT:w" s="T543">raz</ts>
                  <nts id="Seg_8696" n="HIAT:ip">,</nts>
                  <nts id="Seg_8697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_8699" n="HIAT:w" s="T544">mnogʼije</ts>
                  <nts id="Seg_8700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_8702" n="HIAT:w" s="T545">na</ts>
                  <nts id="Seg_8703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_8705" n="HIAT:w" s="T546">kladbʼišʼe</ts>
                  <nts id="Seg_8706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_8707" n="HIAT:ip">(</nts>
                  <ts e="T548" id="Seg_8709" n="HIAT:w" s="T547">paje-</ts>
                  <nts id="Seg_8710" n="HIAT:ip">)</nts>
                  <nts id="Seg_8711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_8713" n="HIAT:w" s="T548">pajexalʼi</ts>
                  <nts id="Seg_8714" n="HIAT:ip">,</nts>
                  <nts id="Seg_8715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_8717" n="HIAT:w" s="T549">da</ts>
                  <nts id="Seg_8718" n="HIAT:ip">?</nts>
                  <nts id="Seg_8719" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T567" id="Seg_8720" n="sc" s="T551">
               <ts e="T567" id="Seg_8722" n="HIAT:u" s="T551">
                  <nts id="Seg_8723" n="HIAT:ip">–</nts>
                  <nts id="Seg_8724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_8726" n="HIAT:w" s="T551">Vot</ts>
                  <nts id="Seg_8727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_8729" n="HIAT:w" s="T553">tam</ts>
                  <nts id="Seg_8730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_8732" n="HIAT:w" s="T554">že</ts>
                  <nts id="Seg_8733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_8735" n="HIAT:w" s="T555">tože</ts>
                  <nts id="Seg_8736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_8738" n="HIAT:w" s="T556">mnoga</ts>
                  <nts id="Seg_8739" n="HIAT:ip">,</nts>
                  <nts id="Seg_8740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_8742" n="HIAT:w" s="T557">lʼiba</ts>
                  <nts id="Seg_8743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_8745" n="HIAT:w" s="T558">nʼelzʼa</ts>
                  <nts id="Seg_8746" n="HIAT:ip">,</nts>
                  <nts id="Seg_8747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_8749" n="HIAT:w" s="T559">lʼibo</ts>
                  <nts id="Seg_8750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_8752" n="HIAT:w" s="T561">tam</ts>
                  <nts id="Seg_8753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_8755" n="HIAT:w" s="T562">kakʼije-ta</ts>
                  <nts id="Seg_8756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_8758" n="HIAT:w" s="T565">abɨčʼija</ts>
                  <nts id="Seg_8759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_8761" n="HIAT:w" s="T566">jestʼ</ts>
                  <nts id="Seg_8762" n="HIAT:ip">?</nts>
                  <nts id="Seg_8763" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T661" id="Seg_8764" n="sc" s="T644">
               <ts e="T661" id="Seg_8766" n="HIAT:u" s="T644">
                  <nts id="Seg_8767" n="HIAT:ip">–</nts>
                  <nts id="Seg_8768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_8770" n="HIAT:w" s="T644">A</ts>
                  <nts id="Seg_8771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_8773" n="HIAT:w" s="T645">pravda</ts>
                  <nts id="Seg_8774" n="HIAT:ip">,</nts>
                  <nts id="Seg_8775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_8777" n="HIAT:w" s="T646">što</ts>
                  <nts id="Seg_8778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_8780" n="HIAT:w" s="T647">tam</ts>
                  <nts id="Seg_8781" n="HIAT:ip">,</nts>
                  <nts id="Seg_8782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_8784" n="HIAT:w" s="T648">vsʼigda</ts>
                  <nts id="Seg_8785" n="HIAT:ip">,</nts>
                  <nts id="Seg_8786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_8788" n="HIAT:w" s="T649">kagda</ts>
                  <nts id="Seg_8789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_8791" n="HIAT:w" s="T650">posʼešʼaješ</ts>
                  <nts id="Seg_8792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_8794" n="HIAT:w" s="T651">tam</ts>
                  <nts id="Seg_8795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_8797" n="HIAT:w" s="T652">kladbʼiše</ts>
                  <nts id="Seg_8798" n="HIAT:ip">,</nts>
                  <nts id="Seg_8799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_8801" n="HIAT:w" s="T653">što</ts>
                  <nts id="Seg_8802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_8804" n="HIAT:w" s="T654">nada</ts>
                  <nts id="Seg_8805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_8807" n="HIAT:w" s="T655">što-ta</ts>
                  <nts id="Seg_8808" n="HIAT:ip">,</nts>
                  <nts id="Seg_8809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_8811" n="HIAT:w" s="T657">eː</ts>
                  <nts id="Seg_8812" n="HIAT:ip">,</nts>
                  <nts id="Seg_8813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_8815" n="HIAT:w" s="T658">kʼidatʼ</ts>
                  <nts id="Seg_8816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_8818" n="HIAT:w" s="T659">na</ts>
                  <nts id="Seg_8819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_8821" n="HIAT:w" s="T660">magʼilu</ts>
                  <nts id="Seg_8822" n="HIAT:ip">?</nts>
                  <nts id="Seg_8823" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T669" id="Seg_8824" n="sc" s="T667">
               <ts e="T669" id="Seg_8826" n="HIAT:u" s="T667">
                  <nts id="Seg_8827" n="HIAT:ip">–</nts>
                  <nts id="Seg_8828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_8830" n="HIAT:w" s="T667">Heː</ts>
                  <nts id="Seg_8831" n="HIAT:ip">.</nts>
                  <nts id="Seg_8832" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T675" id="Seg_8833" n="sc" s="T674">
               <ts e="T675" id="Seg_8835" n="HIAT:u" s="T674">
                  <nts id="Seg_8836" n="HIAT:ip">–</nts>
                  <nts id="Seg_8837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_8839" n="HIAT:w" s="T674">Heː</ts>
                  <nts id="Seg_8840" n="HIAT:ip">.</nts>
                  <nts id="Seg_8841" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T687" id="Seg_8842" n="sc" s="T676">
               <ts e="T684" id="Seg_8844" n="HIAT:u" s="T676">
                  <nts id="Seg_8845" n="HIAT:ip">–</nts>
                  <nts id="Seg_8846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_8848" n="HIAT:w" s="T676">I</ts>
                  <nts id="Seg_8849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_8851" n="HIAT:w" s="T677">tože</ts>
                  <nts id="Seg_8852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_8854" n="HIAT:w" s="T678">nʼelzʼa</ts>
                  <nts id="Seg_8855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_8857" n="HIAT:w" s="T679">nastupatʼ</ts>
                  <nts id="Seg_8858" n="HIAT:ip">,</nts>
                  <nts id="Seg_8859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_8861" n="HIAT:w" s="T680">da</ts>
                  <nts id="Seg_8862" n="HIAT:ip">,</nts>
                  <nts id="Seg_8863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_8865" n="HIAT:w" s="T681">na</ts>
                  <nts id="Seg_8866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_8868" n="HIAT:w" s="T682">na</ts>
                  <nts id="Seg_8869" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_8871" n="HIAT:w" s="T683">magʼilu</ts>
                  <nts id="Seg_8872" n="HIAT:ip">?</nts>
                  <nts id="Seg_8873" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T687" id="Seg_8875" n="HIAT:u" s="T684">
                  <ts e="T685" id="Seg_8877" n="HIAT:w" s="T684">I</ts>
                  <nts id="Seg_8878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_8880" n="HIAT:w" s="T685">abajtʼi</ts>
                  <nts id="Seg_8881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_8883" n="HIAT:w" s="T686">tože</ts>
                  <nts id="Seg_8884" n="HIAT:ip">?</nts>
                  <nts id="Seg_8885" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T722" id="Seg_8886" n="sc" s="T718">
               <ts e="T722" id="Seg_8888" n="HIAT:u" s="T718">
                  <nts id="Seg_8889" n="HIAT:ip">–</nts>
                  <nts id="Seg_8890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_8892" n="HIAT:w" s="T718">Da</ts>
                  <nts id="Seg_8893" n="HIAT:ip">,</nts>
                  <nts id="Seg_8894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_8896" n="HIAT:w" s="T720">stranna</ts>
                  <nts id="Seg_8897" n="HIAT:ip">,</nts>
                  <nts id="Seg_8898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_8900" n="HIAT:w" s="T721">da</ts>
                  <nts id="Seg_8901" n="HIAT:ip">?</nts>
                  <nts id="Seg_8902" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-SE">
            <ts e="T67" id="Seg_8903" n="sc" s="T62">
               <ts e="T63" id="Seg_8905" n="e" s="T62">– A </ts>
               <ts e="T64" id="Seg_8907" n="e" s="T63">togo, </ts>
               <ts e="T65" id="Seg_8909" n="e" s="T64">togo </ts>
               <ts e="T67" id="Seg_8911" n="e" s="T65">gri͡ex? </ts>
            </ts>
            <ts e="T156" id="Seg_8912" n="sc" s="T154">
               <ts e="T155" id="Seg_8914" n="e" s="T154">– Savsʼem </ts>
               <ts e="T156" id="Seg_8916" n="e" s="T155">ogo. </ts>
            </ts>
            <ts e="T389" id="Seg_8917" n="sc" s="T385">
               <ts e="T387" id="Seg_8919" n="e" s="T385">– Vɨsoːkaje, </ts>
               <ts e="T389" id="Seg_8921" n="e" s="T387">da? </ts>
            </ts>
            <ts e="T418" id="Seg_8922" n="sc" s="T416">
               <ts e="T418" id="Seg_8924" n="e" s="T416">– Nʼet? </ts>
            </ts>
            <ts e="T422" id="Seg_8925" n="sc" s="T419">
               <ts e="T420" id="Seg_8927" n="e" s="T419">– A </ts>
               <ts e="T421" id="Seg_8929" n="e" s="T420">kuda </ts>
               <ts e="T422" id="Seg_8931" n="e" s="T421">idʼotʼe? </ts>
            </ts>
            <ts e="T429" id="Seg_8932" n="sc" s="T424">
               <ts e="T425" id="Seg_8934" n="e" s="T424">– Ah, </ts>
               <ts e="T426" id="Seg_8936" n="e" s="T425">((LAUGH)). </ts>
               <ts e="T427" id="Seg_8938" n="e" s="T426">Arajga, </ts>
               <ts e="T429" id="Seg_8940" n="e" s="T427">da? </ts>
            </ts>
            <ts e="T439" id="Seg_8941" n="sc" s="T436">
               <ts e="T437" id="Seg_8943" n="e" s="T436">– A </ts>
               <ts e="T438" id="Seg_8945" n="e" s="T437">kajdak </ts>
               <ts e="T439" id="Seg_8947" n="e" s="T438">raj? </ts>
            </ts>
            <ts e="T447" id="Seg_8948" n="sc" s="T443">
               <ts e="T444" id="Seg_8950" n="e" s="T443">– Kak </ts>
               <ts e="T445" id="Seg_8952" n="e" s="T444">tam </ts>
               <ts e="T447" id="Seg_8954" n="e" s="T445">vɨglʼadʼit-ta? </ts>
            </ts>
            <ts e="T490" id="Seg_8955" n="sc" s="T489">
               <ts e="T490" id="Seg_8957" n="e" s="T489">– Üčügej? </ts>
            </ts>
            <ts e="T550" id="Seg_8958" n="sc" s="T536">
               <ts e="T537" id="Seg_8960" n="e" s="T536">– Možetʼe, </ts>
               <ts e="T538" id="Seg_8962" n="e" s="T537">vɨ </ts>
               <ts e="T539" id="Seg_8964" n="e" s="T538">pomnʼitʼe, </ts>
               <ts e="T540" id="Seg_8966" n="e" s="T539">kakʼije </ts>
               <ts e="T541" id="Seg_8968" n="e" s="T540">jestʼ, </ts>
               <ts e="T542" id="Seg_8970" n="e" s="T541">vot, </ts>
               <ts e="T543" id="Seg_8972" n="e" s="T542">kak </ts>
               <ts e="T544" id="Seg_8974" n="e" s="T543">raz, </ts>
               <ts e="T545" id="Seg_8976" n="e" s="T544">mnogʼije </ts>
               <ts e="T546" id="Seg_8978" n="e" s="T545">na </ts>
               <ts e="T547" id="Seg_8980" n="e" s="T546">kladbʼišʼe </ts>
               <ts e="T548" id="Seg_8982" n="e" s="T547">(paje-) </ts>
               <ts e="T549" id="Seg_8984" n="e" s="T548">pajexalʼi, </ts>
               <ts e="T550" id="Seg_8986" n="e" s="T549">da? </ts>
            </ts>
            <ts e="T567" id="Seg_8987" n="sc" s="T551">
               <ts e="T553" id="Seg_8989" n="e" s="T551">– Vot </ts>
               <ts e="T554" id="Seg_8991" n="e" s="T553">tam </ts>
               <ts e="T555" id="Seg_8993" n="e" s="T554">že </ts>
               <ts e="T556" id="Seg_8995" n="e" s="T555">tože </ts>
               <ts e="T557" id="Seg_8997" n="e" s="T556">mnoga, </ts>
               <ts e="T558" id="Seg_8999" n="e" s="T557">lʼiba </ts>
               <ts e="T559" id="Seg_9001" n="e" s="T558">nʼelzʼa, </ts>
               <ts e="T561" id="Seg_9003" n="e" s="T559">lʼibo </ts>
               <ts e="T562" id="Seg_9005" n="e" s="T561">tam </ts>
               <ts e="T565" id="Seg_9007" n="e" s="T562">kakʼije-ta </ts>
               <ts e="T566" id="Seg_9009" n="e" s="T565">abɨčʼija </ts>
               <ts e="T567" id="Seg_9011" n="e" s="T566">jestʼ? </ts>
            </ts>
            <ts e="T661" id="Seg_9012" n="sc" s="T644">
               <ts e="T645" id="Seg_9014" n="e" s="T644">– A </ts>
               <ts e="T646" id="Seg_9016" n="e" s="T645">pravda, </ts>
               <ts e="T647" id="Seg_9018" n="e" s="T646">što </ts>
               <ts e="T648" id="Seg_9020" n="e" s="T647">tam, </ts>
               <ts e="T649" id="Seg_9022" n="e" s="T648">vsʼigda, </ts>
               <ts e="T650" id="Seg_9024" n="e" s="T649">kagda </ts>
               <ts e="T651" id="Seg_9026" n="e" s="T650">posʼešʼaješ </ts>
               <ts e="T652" id="Seg_9028" n="e" s="T651">tam </ts>
               <ts e="T653" id="Seg_9030" n="e" s="T652">kladbʼiše, </ts>
               <ts e="T654" id="Seg_9032" n="e" s="T653">što </ts>
               <ts e="T655" id="Seg_9034" n="e" s="T654">nada </ts>
               <ts e="T657" id="Seg_9036" n="e" s="T655">što-ta, </ts>
               <ts e="T658" id="Seg_9038" n="e" s="T657">eː, </ts>
               <ts e="T659" id="Seg_9040" n="e" s="T658">kʼidatʼ </ts>
               <ts e="T660" id="Seg_9042" n="e" s="T659">na </ts>
               <ts e="T661" id="Seg_9044" n="e" s="T660">magʼilu? </ts>
            </ts>
            <ts e="T669" id="Seg_9045" n="sc" s="T667">
               <ts e="T669" id="Seg_9047" n="e" s="T667">– Heː. </ts>
            </ts>
            <ts e="T675" id="Seg_9048" n="sc" s="T674">
               <ts e="T675" id="Seg_9050" n="e" s="T674">– Heː. </ts>
            </ts>
            <ts e="T687" id="Seg_9051" n="sc" s="T676">
               <ts e="T677" id="Seg_9053" n="e" s="T676">– I </ts>
               <ts e="T678" id="Seg_9055" n="e" s="T677">tože </ts>
               <ts e="T679" id="Seg_9057" n="e" s="T678">nʼelzʼa </ts>
               <ts e="T680" id="Seg_9059" n="e" s="T679">nastupatʼ, </ts>
               <ts e="T681" id="Seg_9061" n="e" s="T680">da, </ts>
               <ts e="T682" id="Seg_9063" n="e" s="T681">na </ts>
               <ts e="T683" id="Seg_9065" n="e" s="T682">na </ts>
               <ts e="T684" id="Seg_9067" n="e" s="T683">magʼilu? </ts>
               <ts e="T685" id="Seg_9069" n="e" s="T684">I </ts>
               <ts e="T686" id="Seg_9071" n="e" s="T685">abajtʼi </ts>
               <ts e="T687" id="Seg_9073" n="e" s="T686">tože? </ts>
            </ts>
            <ts e="T722" id="Seg_9074" n="sc" s="T718">
               <ts e="T720" id="Seg_9076" n="e" s="T718">– Da, </ts>
               <ts e="T721" id="Seg_9078" n="e" s="T720">stranna, </ts>
               <ts e="T722" id="Seg_9080" n="e" s="T721">da? </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-SE">
            <ta e="T67" id="Seg_9081" s="T62">KiPP_2009_Belief_nar.ES.001 (001.011)</ta>
            <ta e="T156" id="Seg_9082" s="T154">KiPP_2009_Belief_nar.ES.002 (001.024)</ta>
            <ta e="T389" id="Seg_9083" s="T385">KiPP_2009_Belief_nar.ES.003 (001.062)</ta>
            <ta e="T418" id="Seg_9084" s="T416">KiPP_2009_Belief_nar.ES.004 (001.067)</ta>
            <ta e="T422" id="Seg_9085" s="T419">KiPP_2009_Belief_nar.ES.005 (001.069)</ta>
            <ta e="T426" id="Seg_9086" s="T424">KiPP_2009_Belief_nar.ES.006 (001.071)</ta>
            <ta e="T429" id="Seg_9087" s="T426">KiPP_2009_Belief_nar.ES.007 (001.072)</ta>
            <ta e="T439" id="Seg_9088" s="T436">KiPP_2009_Belief_nar.ES.008 (001.075)</ta>
            <ta e="T447" id="Seg_9089" s="T443">KiPP_2009_Belief_nar.ES.009 (001.078)</ta>
            <ta e="T490" id="Seg_9090" s="T489">KiPP_2009_Belief_nar.ES.010 (001.089)</ta>
            <ta e="T550" id="Seg_9091" s="T536">KiPP_2009_Belief_nar.ES.011 (001.099)</ta>
            <ta e="T567" id="Seg_9092" s="T551">KiPP_2009_Belief_nar.ES.012 (001.101)</ta>
            <ta e="T661" id="Seg_9093" s="T644">KiPP_2009_Belief_nar.ES.013 (001.114)</ta>
            <ta e="T669" id="Seg_9094" s="T667">KiPP_2009_Belief_nar.ES.014 (001.116)</ta>
            <ta e="T675" id="Seg_9095" s="T674">KiPP_2009_Belief_nar.ES.015 (001.118)</ta>
            <ta e="T684" id="Seg_9096" s="T676">KiPP_2009_Belief_nar.ES.016 (001.120)</ta>
            <ta e="T687" id="Seg_9097" s="T684">KiPP_2009_Belief_nar.ES.017 (001.121)</ta>
            <ta e="T722" id="Seg_9098" s="T718">KiPP_2009_Belief_nar.ES.018 (001.130)</ta>
         </annotation>
         <annotation name="st" tierref="st-SE" />
         <annotation name="ts" tierref="ts-SE">
            <ta e="T67" id="Seg_9099" s="T62">– A togo, togo gri͡ex? </ta>
            <ta e="T156" id="Seg_9100" s="T154">– Savsʼem ogo. </ta>
            <ta e="T389" id="Seg_9101" s="T385">– Vɨsoːkaje, da? </ta>
            <ta e="T418" id="Seg_9102" s="T416">– Nʼet? </ta>
            <ta e="T422" id="Seg_9103" s="T419">– A kuda idʼotʼe? </ta>
            <ta e="T426" id="Seg_9104" s="T424">– Ah, ((LAUGH)). </ta>
            <ta e="T429" id="Seg_9105" s="T426">Arajga, da? </ta>
            <ta e="T439" id="Seg_9106" s="T436">– A kajdak raj? </ta>
            <ta e="T447" id="Seg_9107" s="T443">– Kak tam vɨglʼadʼit-ta? </ta>
            <ta e="T490" id="Seg_9108" s="T489">– Üčügej? </ta>
            <ta e="T550" id="Seg_9109" s="T536">– Možetʼe, vɨ pomnʼitʼe, kakʼije jestʼ, vot, kak raz, mnogʼije na kladbʼišʼe (paje-) pajexalʼi, da? </ta>
            <ta e="T567" id="Seg_9110" s="T551">– Vot tam že tože mnoga, lʼiba nʼelzʼa, lʼibo tam kakʼije-ta abɨčʼija jestʼ? </ta>
            <ta e="T661" id="Seg_9111" s="T644">– A pravda, što tam, vsʼigda, kagda posʼešʼaješ tam kladbʼiše, što nada što-ta, eː, kʼidatʼ na magʼilu? </ta>
            <ta e="T669" id="Seg_9112" s="T667">– Heː. </ta>
            <ta e="T675" id="Seg_9113" s="T674">– Heː. </ta>
            <ta e="T684" id="Seg_9114" s="T676">– I tože nʼelzʼa nastupatʼ, da, na na magʼilu? </ta>
            <ta e="T687" id="Seg_9115" s="T684">I abajtʼi tože? </ta>
            <ta e="T722" id="Seg_9116" s="T718">– Da, stranna, da? </ta>
         </annotation>
         <annotation name="mb" tierref="mb-SE">
            <ta e="T63" id="Seg_9117" s="T62">a</ta>
            <ta e="T64" id="Seg_9118" s="T63">togo</ta>
            <ta e="T65" id="Seg_9119" s="T64">togo</ta>
            <ta e="T67" id="Seg_9120" s="T65">gri͡ex</ta>
            <ta e="T155" id="Seg_9121" s="T154">savsʼem</ta>
            <ta e="T156" id="Seg_9122" s="T155">ogo</ta>
            <ta e="T427" id="Seg_9123" s="T426">araj-ga</ta>
            <ta e="T429" id="Seg_9124" s="T427">da</ta>
            <ta e="T437" id="Seg_9125" s="T436">a</ta>
            <ta e="T438" id="Seg_9126" s="T437">kajdak</ta>
            <ta e="T439" id="Seg_9127" s="T438">raj</ta>
            <ta e="T490" id="Seg_9128" s="T489">üčügej</ta>
            <ta e="T669" id="Seg_9129" s="T667">heː</ta>
            <ta e="T675" id="Seg_9130" s="T674">heː</ta>
         </annotation>
         <annotation name="mp" tierref="mp-SE">
            <ta e="T63" id="Seg_9131" s="T62">a</ta>
            <ta e="T64" id="Seg_9132" s="T63">togo</ta>
            <ta e="T65" id="Seg_9133" s="T64">togo</ta>
            <ta e="T67" id="Seg_9134" s="T65">gri͡ex</ta>
            <ta e="T155" id="Seg_9135" s="T154">hapsi͡em</ta>
            <ta e="T156" id="Seg_9136" s="T155">ogo</ta>
            <ta e="T427" id="Seg_9137" s="T426">araj-GA</ta>
            <ta e="T429" id="Seg_9138" s="T427">da</ta>
            <ta e="T437" id="Seg_9139" s="T436">a</ta>
            <ta e="T438" id="Seg_9140" s="T437">kajdak</ta>
            <ta e="T439" id="Seg_9141" s="T438">araj</ta>
            <ta e="T490" id="Seg_9142" s="T489">üčügej</ta>
            <ta e="T669" id="Seg_9143" s="T667">eː</ta>
            <ta e="T675" id="Seg_9144" s="T674">eː</ta>
         </annotation>
         <annotation name="ge" tierref="ge-SE">
            <ta e="T63" id="Seg_9145" s="T62">and</ta>
            <ta e="T64" id="Seg_9146" s="T63">why</ta>
            <ta e="T65" id="Seg_9147" s="T64">why</ta>
            <ta e="T67" id="Seg_9148" s="T65">sin.[NOM]</ta>
            <ta e="T155" id="Seg_9149" s="T154">at.all</ta>
            <ta e="T156" id="Seg_9150" s="T155">young.[NOM]</ta>
            <ta e="T427" id="Seg_9151" s="T426">paradise-DAT/LOC</ta>
            <ta e="T429" id="Seg_9152" s="T427">yes</ta>
            <ta e="T437" id="Seg_9153" s="T436">and</ta>
            <ta e="T438" id="Seg_9154" s="T437">how</ta>
            <ta e="T439" id="Seg_9155" s="T438">paradise.[NOM]</ta>
            <ta e="T490" id="Seg_9156" s="T489">good.[NOM]</ta>
            <ta e="T669" id="Seg_9157" s="T667">AFFIRM</ta>
            <ta e="T675" id="Seg_9158" s="T674">AFFIRM</ta>
         </annotation>
         <annotation name="gg" tierref="gg-SE">
            <ta e="T63" id="Seg_9159" s="T62">und</ta>
            <ta e="T64" id="Seg_9160" s="T63">warum</ta>
            <ta e="T65" id="Seg_9161" s="T64">warum</ta>
            <ta e="T67" id="Seg_9162" s="T65">Sünde.[NOM]</ta>
            <ta e="T155" id="Seg_9163" s="T154">ganz</ta>
            <ta e="T156" id="Seg_9164" s="T155">jung.[NOM]</ta>
            <ta e="T427" id="Seg_9165" s="T426">Paradies-DAT/LOC</ta>
            <ta e="T429" id="Seg_9166" s="T427">ja</ta>
            <ta e="T437" id="Seg_9167" s="T436">und</ta>
            <ta e="T438" id="Seg_9168" s="T437">wie</ta>
            <ta e="T439" id="Seg_9169" s="T438">Paradies.[NOM]</ta>
            <ta e="T490" id="Seg_9170" s="T489">gut.[NOM]</ta>
            <ta e="T669" id="Seg_9171" s="T667">AFFIRM</ta>
            <ta e="T675" id="Seg_9172" s="T674">AFFIRM</ta>
         </annotation>
         <annotation name="gr" tierref="gr-SE">
            <ta e="T63" id="Seg_9173" s="T62">а</ta>
            <ta e="T64" id="Seg_9174" s="T63">почему</ta>
            <ta e="T65" id="Seg_9175" s="T64">почему</ta>
            <ta e="T67" id="Seg_9176" s="T65">грех.[NOM]</ta>
            <ta e="T155" id="Seg_9177" s="T154">совсем</ta>
            <ta e="T156" id="Seg_9178" s="T155">молодой.[NOM]</ta>
            <ta e="T427" id="Seg_9179" s="T426">рай-DAT/LOC</ta>
            <ta e="T429" id="Seg_9180" s="T427">да</ta>
            <ta e="T437" id="Seg_9181" s="T436">а</ta>
            <ta e="T438" id="Seg_9182" s="T437">как</ta>
            <ta e="T439" id="Seg_9183" s="T438">рай.[NOM]</ta>
            <ta e="T490" id="Seg_9184" s="T489">хороший.[NOM]</ta>
            <ta e="T669" id="Seg_9185" s="T667">AFFIRM</ta>
            <ta e="T675" id="Seg_9186" s="T674">AFFIRM</ta>
         </annotation>
         <annotation name="mc" tierref="mc-SE">
            <ta e="T63" id="Seg_9187" s="T62">conj</ta>
            <ta e="T64" id="Seg_9188" s="T63">que</ta>
            <ta e="T65" id="Seg_9189" s="T64">que</ta>
            <ta e="T67" id="Seg_9190" s="T65">n.[n:case]</ta>
            <ta e="T155" id="Seg_9191" s="T154">adv</ta>
            <ta e="T156" id="Seg_9192" s="T155">adj.[n:case]</ta>
            <ta e="T427" id="Seg_9193" s="T426">n-n:case</ta>
            <ta e="T429" id="Seg_9194" s="T427">ptcl</ta>
            <ta e="T437" id="Seg_9195" s="T436">conj</ta>
            <ta e="T438" id="Seg_9196" s="T437">que</ta>
            <ta e="T439" id="Seg_9197" s="T438">n.[n:case]</ta>
            <ta e="T490" id="Seg_9198" s="T489">adj.[n:case]</ta>
            <ta e="T669" id="Seg_9199" s="T667">ptcl</ta>
            <ta e="T675" id="Seg_9200" s="T674">ptcl</ta>
         </annotation>
         <annotation name="ps" tierref="ps-SE">
            <ta e="T63" id="Seg_9201" s="T62">conj</ta>
            <ta e="T64" id="Seg_9202" s="T63">que</ta>
            <ta e="T65" id="Seg_9203" s="T64">que</ta>
            <ta e="T67" id="Seg_9204" s="T65">n</ta>
            <ta e="T155" id="Seg_9205" s="T154">adv</ta>
            <ta e="T156" id="Seg_9206" s="T155">adj</ta>
            <ta e="T427" id="Seg_9207" s="T426">n</ta>
            <ta e="T429" id="Seg_9208" s="T427">ptcl</ta>
            <ta e="T437" id="Seg_9209" s="T436">conj</ta>
            <ta e="T438" id="Seg_9210" s="T437">que</ta>
            <ta e="T439" id="Seg_9211" s="T438">n</ta>
            <ta e="T490" id="Seg_9212" s="T489">adj</ta>
            <ta e="T669" id="Seg_9213" s="T667">ptcl</ta>
            <ta e="T675" id="Seg_9214" s="T674">ptcl</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-SE" />
         <annotation name="SyF" tierref="SyF-SE" />
         <annotation name="IST" tierref="IST-SE" />
         <annotation name="Top" tierref="Top-SE" />
         <annotation name="Foc" tierref="Foc-SE" />
         <annotation name="BOR" tierref="BOR-SE">
            <ta e="T63" id="Seg_9215" s="T62">RUS:gram</ta>
            <ta e="T67" id="Seg_9216" s="T65">RUS:core</ta>
            <ta e="T155" id="Seg_9217" s="T154">RUS:mod</ta>
            <ta e="T427" id="Seg_9218" s="T426">RUS:cult</ta>
            <ta e="T429" id="Seg_9219" s="T427">RUS:disc</ta>
            <ta e="T437" id="Seg_9220" s="T436">RUS:gram</ta>
            <ta e="T439" id="Seg_9221" s="T438">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-SE" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-SE" />
         <annotation name="CS" tierref="CS-SE" />
         <annotation name="fe" tierref="fe-SE">
            <ta e="T67" id="Seg_9222" s="T62">– And why, why is it a sin?</ta>
            <ta e="T156" id="Seg_9223" s="T154">– Very young.</ta>
            <ta e="T389" id="Seg_9224" s="T385">– High, yes?</ta>
            <ta e="T418" id="Seg_9225" s="T416">– No?</ta>
            <ta e="T422" id="Seg_9226" s="T419">– And where do you go?</ta>
            <ta e="T426" id="Seg_9227" s="T424">– Ah.</ta>
            <ta e="T429" id="Seg_9228" s="T426">To paradise, right?</ta>
            <ta e="T439" id="Seg_9229" s="T436">– And how is the paradise?</ta>
            <ta e="T447" id="Seg_9230" s="T443">– What it is like there?</ta>
            <ta e="T490" id="Seg_9231" s="T489">– Is she good?</ta>
            <ta e="T550" id="Seg_9232" s="T536">– Maybe you remember, what kind of, well, many are going to the cemetery, right?</ta>
            <ta e="T567" id="Seg_9233" s="T551">– Well, there are many, one mustn't, or are there some customs?</ta>
            <ta e="T661" id="Seg_9234" s="T644">– And is it true, that, when going to the cemetery, one has always to throw something onto the grave?</ta>
            <ta e="T669" id="Seg_9235" s="T667">– Yes.</ta>
            <ta e="T675" id="Seg_9236" s="T674">– Yes.</ta>
            <ta e="T684" id="Seg_9237" s="T676">– And one also mustn't step onto the grave, right?</ta>
            <ta e="T687" id="Seg_9238" s="T684">And also go around?</ta>
            <ta e="T722" id="Seg_9239" s="T718">– Yes, strange, isn't it?</ta>
         </annotation>
         <annotation name="fg" tierref="fg-SE">
            <ta e="T67" id="Seg_9240" s="T62">– Und warum, warum ist das Sünde?</ta>
            <ta e="T156" id="Seg_9241" s="T154">– Ganz jung.</ta>
            <ta e="T389" id="Seg_9242" s="T385">– Hoher, ja?</ta>
            <ta e="T418" id="Seg_9243" s="T416">– Nein?</ta>
            <ta e="T422" id="Seg_9244" s="T419">– Und wohin kommen Sie?</ta>
            <ta e="T426" id="Seg_9245" s="T424">– Ah. </ta>
            <ta e="T429" id="Seg_9246" s="T426">Ins Paradies, ja?</ta>
            <ta e="T439" id="Seg_9247" s="T436">– Und wie ist das Paradies?</ta>
            <ta e="T447" id="Seg_9248" s="T443">– Und wie sieht es dort aus?</ta>
            <ta e="T490" id="Seg_9249" s="T489">– Ist sie gut?</ta>
            <ta e="T550" id="Seg_9250" s="T536">– Vielleicht erinnern Sie sich, was für, nun, viele gehen auf den Friedhof, ja?</ta>
            <ta e="T567" id="Seg_9251" s="T551">– Nun, da gibt es viele, man darf nicht, oder gibt es irgendwelche Bräuche?</ta>
            <ta e="T661" id="Seg_9252" s="T644">– Und stimmt es, dass man dort, immer, wenn man auf den Friedhof geht, etwas aufs Grab werfen muss?</ta>
            <ta e="T669" id="Seg_9253" s="T667">– Ja.</ta>
            <ta e="T675" id="Seg_9254" s="T674">– Ja.</ta>
            <ta e="T684" id="Seg_9255" s="T676">– Und man darf auf nicht aufs Grab treten, ja?</ta>
            <ta e="T687" id="Seg_9256" s="T684">Und auch drumherum gehen?</ta>
            <ta e="T722" id="Seg_9257" s="T718">– Ja, merkwürdig, oder?</ta>
         </annotation>
         <annotation name="fr" tierref="fr-SE">
            <ta e="T67" id="Seg_9258" s="T62">— А почему, почему это грех?</ta>
            <ta e="T156" id="Seg_9259" s="T154">— Совсем молодая.</ta>
            <ta e="T389" id="Seg_9260" s="T385">— Высокое, да?</ta>
            <ta e="T418" id="Seg_9261" s="T416">— Нет?</ta>
            <ta e="T422" id="Seg_9262" s="T419">— А куда пойдёте?</ta>
            <ta e="T426" id="Seg_9263" s="T424">— А-а.</ta>
            <ta e="T429" id="Seg_9264" s="T426">В рай, да?</ta>
            <ta e="T439" id="Seg_9265" s="T436">— А как в раю?</ta>
            <ta e="T447" id="Seg_9266" s="T443">— Как там всё выглядит-то?</ta>
            <ta e="T490" id="Seg_9267" s="T489">— Она хорошая?</ta>
            <ta e="T550" id="Seg_9268" s="T536">— Может быть, вы помните, какие есть (обычаи), вот как раз, многие на кладбище поехали, да?</ta>
            <ta e="T567" id="Seg_9269" s="T551">— Вот там же тоже много (такого), либо нельзя (чего-то), либо какие-то обычаи есть?</ta>
            <ta e="T661" id="Seg_9270" s="T644">— А правда, что там всегда, когда посещаешь кладбище, что надо что-то кидать на могилу?</ta>
            <ta e="T669" id="Seg_9271" s="T667">— Ага.</ta>
            <ta e="T675" id="Seg_9272" s="T674">— Ага.</ta>
            <ta e="T684" id="Seg_9273" s="T676">— И нельзя наступать, да, на могилу?</ta>
            <ta e="T687" id="Seg_9274" s="T684">И обходить тоже?</ta>
            <ta e="T722" id="Seg_9275" s="T718">— Да, странно, да?</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr-SE">
            <ta e="T67" id="Seg_9276" s="T62">pochemu greh</ta>
         </annotation>
         <annotation name="nt" tierref="nt-SE" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T0" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T723" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T724" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T725" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T726" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T727" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T728" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-KiPP"
                          name="ref"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-KiPP"
                          name="st"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KiPP"
                          name="ts"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KiPP"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KiPP"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-KiPP"
                          name="mb"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-KiPP"
                          name="mp"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-KiPP"
                          name="ge"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-KiPP"
                          name="gg"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-KiPP"
                          name="gr"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-KiPP"
                          name="mc"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-KiPP"
                          name="ps"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-KiPP"
                          name="SeR"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-KiPP"
                          name="SyF"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-KiPP"
                          name="IST"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-KiPP"
                          name="Top"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-KiPP"
                          name="Foc"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-KiPP"
                          name="BOR"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-KiPP"
                          name="BOR-Phon"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-KiPP"
                          name="BOR-Morph"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-KiPP"
                          name="CS"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KiPP"
                          name="fe"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KiPP"
                          name="fg"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KiPP"
                          name="fr"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-KiPP"
                          name="ltr"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KiPP"
                          name="nt"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-SE"
                          name="ref"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-SE"
                          name="st"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-SE"
                          name="ts"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-SE"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-SE"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-SE"
                          name="mb"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-SE"
                          name="mp"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-SE"
                          name="ge"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-SE"
                          name="gg"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-SE"
                          name="gr"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-SE"
                          name="mc"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-SE"
                          name="ps"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-SE"
                          name="SeR"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-SE"
                          name="SyF"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-SE"
                          name="IST"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-SE"
                          name="Top"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-SE"
                          name="Foc"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-SE"
                          name="BOR"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-SE"
                          name="BOR-Phon"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-SE"
                          name="BOR-Morph"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-SE"
                          name="CS"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-SE"
                          name="fe"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-SE"
                          name="fg"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-SE"
                          name="fr"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-SE"
                          name="ltr"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-SE"
                          name="nt"
                          segmented-tier-id="tx-SE"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
