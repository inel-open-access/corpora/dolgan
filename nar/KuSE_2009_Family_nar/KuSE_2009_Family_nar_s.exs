<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID96725289-FB09-D939-9FBD-92CD23D3F284">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>KuSE_2009_Family_nar</transcription-name>
         <referenced-file url="KuSE_2009_Family_nar.wav" />
         <referenced-file url="KuSE_2009_Family_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\KuSE_2009_Family_nar\KuSE_2009_Family_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">152</ud-information>
            <ud-information attribute-name="# HIAT:w">116</ud-information>
            <ud-information attribute-name="# e">114</ud-information>
            <ud-information attribute-name="# HIAT:u">17</ud-information>
            <ud-information attribute-name="# sc">10</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KuSE">
            <abbreviation>KuSE</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="SE">
            <abbreviation>SE</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.0" type="appl" />
         <tli id="T2" time="1.7570000000000001" type="appl" />
         <tli id="T3" time="3.5140000000000002" type="appl" />
         <tli id="T4" time="5.271000000000001" type="appl" />
         <tli id="T5" time="7.0280000000000005" type="appl" />
         <tli id="T6" time="8.785" type="appl" />
         <tli id="T7" time="10.3" type="appl" />
         <tli id="T8" time="11.815" type="appl" />
         <tli id="T9" time="11.83" type="appl" />
         <tli id="T10" time="12.4575" type="appl" />
         <tli id="T11" time="13.085" type="appl" />
         <tli id="T12" time="13.7125" type="appl" />
         <tli id="T13" time="14.34" type="appl" />
         <tli id="T14" time="15.302666666666667" type="appl" />
         <tli id="T15" time="16.265333333333334" type="appl" />
         <tli id="T16" time="17.228" type="appl" />
         <tli id="T17" time="18.492250000000002" type="appl" />
         <tli id="T18" time="19.756500000000003" type="appl" />
         <tli id="T19" time="21.02075" type="appl" />
         <tli id="T20" time="22.285" type="appl" />
         <tli id="T21" time="23.711714285714287" type="appl" />
         <tli id="T22" time="25.13842857142857" type="appl" />
         <tli id="T23" time="26.565142857142856" type="appl" />
         <tli id="T24" time="27.991857142857143" type="appl" />
         <tli id="T25" time="29.418571428571425" type="appl" />
         <tli id="T26" time="30.845285714285712" type="appl" />
         <tli id="T27" time="32.272" type="appl" />
         <tli id="T28" time="33.391999999999996" type="appl" />
         <tli id="T29" time="34.512" type="appl" />
         <tli id="T30" time="36.713" type="appl" />
         <tli id="T31" time="38.914" type="appl" />
         <tli id="T32" time="39.827777777777776" type="appl" />
         <tli id="T33" time="40.74155555555556" type="appl" />
         <tli id="T34" time="41.65533333333333" type="appl" />
         <tli id="T35" time="42.56911111111111" type="appl" />
         <tli id="T36" time="43.48288888888889" type="appl" />
         <tli id="T37" time="44.39666666666667" type="appl" />
         <tli id="T38" time="45.31044444444444" type="appl" />
         <tli id="T39" time="46.224222222222224" type="appl" />
         <tli id="T40" time="47.138" type="appl" />
         <tli id="T41" time="47.754" type="appl" />
         <tli id="T42" time="48.37" type="appl" />
         <tli id="T43" time="48.986" type="appl" />
         <tli id="T44" time="49.602000000000004" type="appl" />
         <tli id="T45" time="50.218" type="appl" />
         <tli id="T46" time="50.834" type="appl" />
         <tli id="T47" time="51.45" type="appl" />
         <tli id="T48" time="52.275" type="appl" />
         <tli id="T49" time="53.1" type="appl" />
         <tli id="T50" time="53.925" type="appl" />
         <tli id="T51" time="54.71125" type="appl" />
         <tli id="T52" time="55.497499999999995" type="appl" />
         <tli id="T53" time="56.28375" type="appl" />
         <tli id="T54" time="57.07" type="appl" />
         <tli id="T55" time="57.856249999999996" type="appl" />
         <tli id="T56" time="58.6425" type="appl" />
         <tli id="T57" time="59.42875" type="appl" />
         <tli id="T58" time="60.214999999999996" type="appl" />
         <tli id="T59" time="61.00125" type="appl" />
         <tli id="T60" time="61.7875" type="appl" />
         <tli id="T61" time="62.573750000000004" type="appl" />
         <tli id="T62" time="63.36" type="appl" />
         <tli id="T63" time="64.1566" type="appl" />
         <tli id="T64" time="64.9532" type="appl" />
         <tli id="T65" time="65.7498" type="appl" />
         <tli id="T66" time="66.5464" type="appl" />
         <tli id="T67" time="67.343" type="appl" />
         <tli id="T68" time="68.1396" type="appl" />
         <tli id="T69" time="68.9362" type="appl" />
         <tli id="T70" time="69.7328" type="appl" />
         <tli id="T71" time="70.5294" type="appl" />
         <tli id="T72" time="71.326" type="appl" />
         <tli id="T73" time="72.1226" type="appl" />
         <tli id="T74" time="72.9192" type="appl" />
         <tli id="T75" time="73.7158" type="appl" />
         <tli id="T76" time="74.5124" type="appl" />
         <tli id="T77" time="75.309" type="appl" />
         <tli id="T78" time="75.95616666666666" type="appl" />
         <tli id="T79" time="76.60333333333332" type="appl" />
         <tli id="T80" time="77.25049999999999" type="appl" />
         <tli id="T81" time="77.89766666666667" type="appl" />
         <tli id="T82" time="78.54483333333333" type="appl" />
         <tli id="T83" time="79.192" type="appl" />
         <tli id="T84" time="80.2686923076923" type="appl" />
         <tli id="T85" time="81.3453846153846" type="appl" />
         <tli id="T86" time="82.42207692307692" type="appl" />
         <tli id="T87" time="83.49876923076923" type="appl" />
         <tli id="T88" time="84.57546153846152" type="appl" />
         <tli id="T89" time="85.65215384615384" type="appl" />
         <tli id="T90" time="86.72884615384615" type="appl" />
         <tli id="T91" time="87.80553846153845" type="appl" />
         <tli id="T92" time="88.88223076923076" type="appl" />
         <tli id="T93" time="89.95892307692307" type="appl" />
         <tli id="T94" time="91.03561538461537" type="appl" />
         <tli id="T95" time="92.11230769230768" type="appl" />
         <tli id="T96" time="93.189" type="appl" />
         <tli id="T97" time="93.64306666666666" type="appl" />
         <tli id="T98" time="94.09713333333333" type="appl" />
         <tli id="T99" time="94.5512" type="appl" />
         <tli id="T100" time="95.00526666666666" type="appl" />
         <tli id="T101" time="95.45933333333333" type="appl" />
         <tli id="T102" time="95.9134" type="appl" />
         <tli id="T103" time="96.36746666666666" type="appl" />
         <tli id="T104" time="96.82153333333333" type="appl" />
         <tli id="T105" time="97.2756" type="appl" />
         <tli id="T106" time="97.72966666666666" type="appl" />
         <tli id="T107" time="98.18373333333334" type="appl" />
         <tli id="T108" time="98.6378" type="appl" />
         <tli id="T109" time="99.09186666666666" type="appl" />
         <tli id="T110" time="99.54593333333334" type="appl" />
         <tli id="T111" time="100.0" type="appl" />
         <tli id="T112" time="102.5044" type="appl" />
         <tli id="T113" time="105.00880000000001" type="appl" />
         <tli id="T114" time="107.5132" type="appl" />
         <tli id="T115" time="110.0176" type="appl" />
         <tli id="T116" time="112.522" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-KuSE"
                      id="tx-KuSE"
                      speaker="KuSE"
                      type="t">
         <timeline-fork end="T54" start="T53">
            <tli id="T53.tx-KuSE.1" />
         </timeline-fork>
         <timeline-fork end="T101" start="T100">
            <tli id="T100.tx-KuSE.1" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KuSE">
            <ts e="T6" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Nas</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_7" n="HIAT:w" s="T2">v</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_10" n="HIAT:w" s="T3">semʼe</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_13" n="HIAT:w" s="T4">semero</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_16" n="HIAT:w" s="T5">detej</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T29" id="Seg_19" n="sc" s="T9">
               <ts e="T13" id="Seg_21" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_23" n="HIAT:w" s="T9">Bihigi</ts>
                  <nts id="Seg_24" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_26" n="HIAT:w" s="T10">semjabɨtɨgar</ts>
                  <nts id="Seg_27" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_29" n="HIAT:w" s="T11">hette</ts>
                  <nts id="Seg_30" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_32" n="HIAT:w" s="T12">kihibit</ts>
                  <nts id="Seg_33" n="HIAT:ip">.</nts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_36" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_38" n="HIAT:w" s="T13">Bihigi</ts>
                  <nts id="Seg_39" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_41" n="HIAT:w" s="T14">kas</ts>
                  <nts id="Seg_42" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_44" n="HIAT:w" s="T15">etej</ts>
                  <nts id="Seg_45" n="HIAT:ip">?</nts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T20" id="Seg_48" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_50" n="HIAT:w" s="T16">Tri</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_53" n="HIAT:w" s="T17">devočki</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_56" n="HIAT:w" s="T18">četɨre</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_59" n="HIAT:w" s="T19">malʼčika</ts>
                  <nts id="Seg_60" n="HIAT:ip">.</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T27" id="Seg_63" n="HIAT:u" s="T20">
                  <ts e="T21" id="Seg_65" n="HIAT:w" s="T20">Vassa</ts>
                  <nts id="Seg_66" n="HIAT:ip">,</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_69" n="HIAT:w" s="T21">Sveta</ts>
                  <nts id="Seg_70" n="HIAT:ip">,</nts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_73" n="HIAT:w" s="T22">Julja</ts>
                  <nts id="Seg_74" n="HIAT:ip">,</nts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_77" n="HIAT:w" s="T23">Vanja</ts>
                  <nts id="Seg_78" n="HIAT:ip">,</nts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_81" n="HIAT:w" s="T24">Senja</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_84" n="HIAT:w" s="T25">i</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_87" n="HIAT:w" s="T26">ja</ts>
                  <nts id="Seg_88" n="HIAT:ip">.</nts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T29" id="Seg_91" n="HIAT:u" s="T27">
                  <ts e="T28" id="Seg_93" n="HIAT:w" s="T27">Odin</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_96" n="HIAT:w" s="T28">prijomnɨj</ts>
                  <nts id="Seg_97" n="HIAT:ip">…</nts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T116" id="Seg_99" n="sc" s="T31">
               <ts e="T40" id="Seg_101" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_103" n="HIAT:w" s="T31">Biːr</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_106" n="HIAT:w" s="T32">ogonu</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_109" n="HIAT:w" s="T33">iːttibippit</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_112" n="HIAT:w" s="T34">aːta</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_115" n="HIAT:w" s="T35">Miša</ts>
                  <nts id="Seg_116" n="HIAT:ip">,</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_119" n="HIAT:w" s="T36">onu</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_122" n="HIAT:w" s="T37">töttörü</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_125" n="HIAT:w" s="T38">bi͡ertibit</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_128" n="HIAT:w" s="T39">maːmatɨgar</ts>
                  <nts id="Seg_129" n="HIAT:ip">.</nts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_132" n="HIAT:u" s="T40">
                  <ts e="T41" id="Seg_134" n="HIAT:w" s="T40">Ontuŋ</ts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_137" n="HIAT:w" s="T41">tolʼko</ts>
                  <nts id="Seg_138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_140" n="HIAT:w" s="T42">bihi͡eke</ts>
                  <nts id="Seg_141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_143" n="HIAT:w" s="T43">pitatʼsja</ts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_146" n="HIAT:w" s="T44">hɨlʼdʼar</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_149" n="HIAT:w" s="T45">ete</ts>
                  <nts id="Seg_150" n="HIAT:ip">,</nts>
                  <nts id="Seg_151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_153" n="HIAT:w" s="T46">urut</ts>
                  <nts id="Seg_154" n="HIAT:ip">.</nts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T50" id="Seg_157" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_159" n="HIAT:w" s="T47">Onton</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_162" n="HIAT:w" s="T48">ke</ts>
                  <nts id="Seg_163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_165" n="HIAT:w" s="T49">tuguj</ts>
                  <nts id="Seg_166" n="HIAT:ip">.</nts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_169" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_171" n="HIAT:w" s="T50">Biːrim</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_174" n="HIAT:w" s="T51">Anabarga</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_177" n="HIAT:w" s="T52">baːr</ts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53.tx-KuSE.1" id="Seg_180" n="HIAT:w" s="T53">Uruŋ</ts>
                  <nts id="Seg_181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_183" n="HIAT:w" s="T53.tx-KuSE.1">Kajaga</ts>
                  <nts id="Seg_184" n="HIAT:ip">,</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_187" n="HIAT:w" s="T54">biːrim</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_190" n="HIAT:w" s="T55">Voločankaga</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_193" n="HIAT:w" s="T56">baːr</ts>
                  <nts id="Seg_194" n="HIAT:ip">,</nts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_197" n="HIAT:w" s="T57">biːrim</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_199" n="HIAT:ip">(</nts>
                  <ts e="T59" id="Seg_201" n="HIAT:w" s="T58">Hɨnda-</ts>
                  <nts id="Seg_202" n="HIAT:ip">)</nts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_205" n="HIAT:w" s="T59">Hɨndaskaga</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_208" n="HIAT:w" s="T60">tɨ͡aga</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_211" n="HIAT:w" s="T61">hɨldʼaːččɨ</ts>
                  <nts id="Seg_212" n="HIAT:ip">.</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_215" n="HIAT:u" s="T62">
                  <ts e="T63" id="Seg_217" n="HIAT:w" s="T62">Biːr</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_220" n="HIAT:w" s="T63">biːr</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_223" n="HIAT:w" s="T64">biːr</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_226" n="HIAT:w" s="T65">edʼijim</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_229" n="HIAT:w" s="T66">Hɨndaːskaga</ts>
                  <nts id="Seg_230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_232" n="HIAT:w" s="T67">baːr</ts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_235" n="HIAT:w" s="T68">ogoloːk</ts>
                  <nts id="Seg_236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_238" n="HIAT:w" s="T69">ikki</ts>
                  <nts id="Seg_239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_241" n="HIAT:w" s="T70">ogoloːk</ts>
                  <nts id="Seg_242" n="HIAT:ip">,</nts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_245" n="HIAT:w" s="T71">tɨ͡aga</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_248" n="HIAT:w" s="T72">hɨlʼdʼaːččɨ</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_251" n="HIAT:w" s="T73">etilere</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_254" n="HIAT:w" s="T74">anɨ</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_257" n="HIAT:w" s="T75">üleliːr</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_260" n="HIAT:w" s="T76">sadikka</ts>
                  <nts id="Seg_261" n="HIAT:ip">.</nts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_264" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_266" n="HIAT:w" s="T77">Ogoto</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_269" n="HIAT:w" s="T78">manna</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_272" n="HIAT:w" s="T79">baːr</ts>
                  <nts id="Seg_273" n="HIAT:ip">,</nts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_276" n="HIAT:w" s="T80">bu</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_279" n="HIAT:w" s="T81">oloror</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_282" n="HIAT:w" s="T82">ogoto</ts>
                  <nts id="Seg_283" n="HIAT:ip">.</nts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_286" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_288" n="HIAT:w" s="T83">Vassa</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_291" n="HIAT:w" s="T84">kim</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_294" n="HIAT:w" s="T85">Turaga</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_297" n="HIAT:w" s="T86">baːr</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_300" n="HIAT:w" s="T87">ete</ts>
                  <nts id="Seg_301" n="HIAT:ip">,</nts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_304" n="HIAT:w" s="T88">onno</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_307" n="HIAT:w" s="T89">ogo</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_310" n="HIAT:w" s="T90">töröːbüte</ts>
                  <nts id="Seg_311" n="HIAT:ip">,</nts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_314" n="HIAT:w" s="T91">aːta</ts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_317" n="HIAT:w" s="T92">Egorka</ts>
                  <nts id="Seg_318" n="HIAT:ip">,</nts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_321" n="HIAT:w" s="T93">bejetin</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_324" n="HIAT:w" s="T94">aːta</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_327" n="HIAT:w" s="T95">Vassa</ts>
                  <nts id="Seg_328" n="HIAT:ip">.</nts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T111" id="Seg_331" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_333" n="HIAT:w" s="T96">Vassa</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_336" n="HIAT:w" s="T97">tu͡okka</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_339" n="HIAT:w" s="T98">da</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_342" n="HIAT:w" s="T99">üleleːbet</ts>
                  <nts id="Seg_343" n="HIAT:ip">,</nts>
                  <nts id="Seg_344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100.tx-KuSE.1" id="Seg_346" n="HIAT:w" s="T100">Uruŋ</ts>
                  <nts id="Seg_347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_349" n="HIAT:w" s="T100.tx-KuSE.1">Kajaga</ts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_352" n="HIAT:w" s="T101">Uppaja</ts>
                  <nts id="Seg_353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_355" n="HIAT:w" s="T102">kimi͡eke</ts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_358" n="HIAT:w" s="T103">üleliːr</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_360" n="HIAT:ip">(</nts>
                  <ts e="T105" id="Seg_362" n="HIAT:w" s="T104">olenevod-</ts>
                  <nts id="Seg_363" n="HIAT:ip">)</nts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_366" n="HIAT:w" s="T105">olenevodka</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_369" n="HIAT:w" s="T106">üleliːr</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_372" n="HIAT:w" s="T107">taba</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_375" n="HIAT:w" s="T108">künüger</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_378" n="HIAT:w" s="T109">hɨrsa</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_381" n="HIAT:w" s="T110">keleːčči</ts>
                  <nts id="Seg_382" n="HIAT:ip">.</nts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T116" id="Seg_385" n="HIAT:u" s="T111">
                  <ts e="T112" id="Seg_387" n="HIAT:w" s="T111">Hibi͡ete</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_390" n="HIAT:w" s="T112">emi͡e</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_393" n="HIAT:w" s="T113">hɨrsaːččɨ</ts>
                  <nts id="Seg_394" n="HIAT:ip">,</nts>
                  <nts id="Seg_395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_397" n="HIAT:w" s="T114">Senja</ts>
                  <nts id="Seg_398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_400" n="HIAT:w" s="T115">hɨrsaːččɨ</ts>
                  <nts id="Seg_401" n="HIAT:ip">.</nts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KuSE">
            <ts e="T6" id="Seg_403" n="sc" s="T1">
               <ts e="T2" id="Seg_405" n="e" s="T1">Nas </ts>
               <ts e="T3" id="Seg_407" n="e" s="T2">v </ts>
               <ts e="T4" id="Seg_409" n="e" s="T3">semʼe </ts>
               <ts e="T5" id="Seg_411" n="e" s="T4">semero </ts>
               <ts e="T6" id="Seg_413" n="e" s="T5">detej. </ts>
            </ts>
            <ts e="T29" id="Seg_414" n="sc" s="T9">
               <ts e="T10" id="Seg_416" n="e" s="T9">Bihigi </ts>
               <ts e="T11" id="Seg_418" n="e" s="T10">semjabɨtɨgar </ts>
               <ts e="T12" id="Seg_420" n="e" s="T11">hette </ts>
               <ts e="T13" id="Seg_422" n="e" s="T12">kihibit. </ts>
               <ts e="T14" id="Seg_424" n="e" s="T13">Bihigi </ts>
               <ts e="T15" id="Seg_426" n="e" s="T14">kas </ts>
               <ts e="T16" id="Seg_428" n="e" s="T15">etej? </ts>
               <ts e="T17" id="Seg_430" n="e" s="T16">Tri </ts>
               <ts e="T18" id="Seg_432" n="e" s="T17">devočki </ts>
               <ts e="T19" id="Seg_434" n="e" s="T18">četɨre </ts>
               <ts e="T20" id="Seg_436" n="e" s="T19">malʼčika. </ts>
               <ts e="T21" id="Seg_438" n="e" s="T20">Vassa, </ts>
               <ts e="T22" id="Seg_440" n="e" s="T21">Sveta, </ts>
               <ts e="T23" id="Seg_442" n="e" s="T22">Julja, </ts>
               <ts e="T24" id="Seg_444" n="e" s="T23">Vanja, </ts>
               <ts e="T25" id="Seg_446" n="e" s="T24">Senja </ts>
               <ts e="T26" id="Seg_448" n="e" s="T25">i </ts>
               <ts e="T27" id="Seg_450" n="e" s="T26">ja. </ts>
               <ts e="T28" id="Seg_452" n="e" s="T27">Odin </ts>
               <ts e="T29" id="Seg_454" n="e" s="T28">prijomnɨj… </ts>
            </ts>
            <ts e="T116" id="Seg_455" n="sc" s="T31">
               <ts e="T32" id="Seg_457" n="e" s="T31">Biːr </ts>
               <ts e="T33" id="Seg_459" n="e" s="T32">ogonu </ts>
               <ts e="T34" id="Seg_461" n="e" s="T33">iːttibippit </ts>
               <ts e="T35" id="Seg_463" n="e" s="T34">aːta </ts>
               <ts e="T36" id="Seg_465" n="e" s="T35">Miša, </ts>
               <ts e="T37" id="Seg_467" n="e" s="T36">onu </ts>
               <ts e="T38" id="Seg_469" n="e" s="T37">töttörü </ts>
               <ts e="T39" id="Seg_471" n="e" s="T38">bi͡ertibit </ts>
               <ts e="T40" id="Seg_473" n="e" s="T39">maːmatɨgar. </ts>
               <ts e="T41" id="Seg_475" n="e" s="T40">Ontuŋ </ts>
               <ts e="T42" id="Seg_477" n="e" s="T41">tolʼko </ts>
               <ts e="T43" id="Seg_479" n="e" s="T42">bihi͡eke </ts>
               <ts e="T44" id="Seg_481" n="e" s="T43">pitatʼsja </ts>
               <ts e="T45" id="Seg_483" n="e" s="T44">hɨlʼdʼar </ts>
               <ts e="T46" id="Seg_485" n="e" s="T45">ete, </ts>
               <ts e="T47" id="Seg_487" n="e" s="T46">urut. </ts>
               <ts e="T48" id="Seg_489" n="e" s="T47">Onton </ts>
               <ts e="T49" id="Seg_491" n="e" s="T48">ke </ts>
               <ts e="T50" id="Seg_493" n="e" s="T49">tuguj. </ts>
               <ts e="T51" id="Seg_495" n="e" s="T50">Biːrim </ts>
               <ts e="T52" id="Seg_497" n="e" s="T51">Anabarga </ts>
               <ts e="T53" id="Seg_499" n="e" s="T52">baːr </ts>
               <ts e="T54" id="Seg_501" n="e" s="T53">Uruŋ Kajaga, </ts>
               <ts e="T55" id="Seg_503" n="e" s="T54">biːrim </ts>
               <ts e="T56" id="Seg_505" n="e" s="T55">Voločankaga </ts>
               <ts e="T57" id="Seg_507" n="e" s="T56">baːr, </ts>
               <ts e="T58" id="Seg_509" n="e" s="T57">biːrim </ts>
               <ts e="T59" id="Seg_511" n="e" s="T58">(Hɨnda-) </ts>
               <ts e="T60" id="Seg_513" n="e" s="T59">Hɨndaskaga </ts>
               <ts e="T61" id="Seg_515" n="e" s="T60">tɨ͡aga </ts>
               <ts e="T62" id="Seg_517" n="e" s="T61">hɨldʼaːččɨ. </ts>
               <ts e="T63" id="Seg_519" n="e" s="T62">Biːr </ts>
               <ts e="T64" id="Seg_521" n="e" s="T63">biːr </ts>
               <ts e="T65" id="Seg_523" n="e" s="T64">biːr </ts>
               <ts e="T66" id="Seg_525" n="e" s="T65">edʼijim </ts>
               <ts e="T67" id="Seg_527" n="e" s="T66">Hɨndaːskaga </ts>
               <ts e="T68" id="Seg_529" n="e" s="T67">baːr </ts>
               <ts e="T69" id="Seg_531" n="e" s="T68">ogoloːk </ts>
               <ts e="T70" id="Seg_533" n="e" s="T69">ikki </ts>
               <ts e="T71" id="Seg_535" n="e" s="T70">ogoloːk, </ts>
               <ts e="T72" id="Seg_537" n="e" s="T71">tɨ͡aga </ts>
               <ts e="T73" id="Seg_539" n="e" s="T72">hɨlʼdʼaːččɨ </ts>
               <ts e="T74" id="Seg_541" n="e" s="T73">etilere </ts>
               <ts e="T75" id="Seg_543" n="e" s="T74">anɨ </ts>
               <ts e="T76" id="Seg_545" n="e" s="T75">üleliːr </ts>
               <ts e="T77" id="Seg_547" n="e" s="T76">sadikka. </ts>
               <ts e="T78" id="Seg_549" n="e" s="T77">Ogoto </ts>
               <ts e="T79" id="Seg_551" n="e" s="T78">manna </ts>
               <ts e="T80" id="Seg_553" n="e" s="T79">baːr, </ts>
               <ts e="T81" id="Seg_555" n="e" s="T80">bu </ts>
               <ts e="T82" id="Seg_557" n="e" s="T81">oloror </ts>
               <ts e="T83" id="Seg_559" n="e" s="T82">ogoto. </ts>
               <ts e="T84" id="Seg_561" n="e" s="T83">Vassa </ts>
               <ts e="T85" id="Seg_563" n="e" s="T84">kim </ts>
               <ts e="T86" id="Seg_565" n="e" s="T85">Turaga </ts>
               <ts e="T87" id="Seg_567" n="e" s="T86">baːr </ts>
               <ts e="T88" id="Seg_569" n="e" s="T87">ete, </ts>
               <ts e="T89" id="Seg_571" n="e" s="T88">onno </ts>
               <ts e="T90" id="Seg_573" n="e" s="T89">ogo </ts>
               <ts e="T91" id="Seg_575" n="e" s="T90">töröːbüte, </ts>
               <ts e="T92" id="Seg_577" n="e" s="T91">aːta </ts>
               <ts e="T93" id="Seg_579" n="e" s="T92">Egorka, </ts>
               <ts e="T94" id="Seg_581" n="e" s="T93">bejetin </ts>
               <ts e="T95" id="Seg_583" n="e" s="T94">aːta </ts>
               <ts e="T96" id="Seg_585" n="e" s="T95">Vassa. </ts>
               <ts e="T97" id="Seg_587" n="e" s="T96">Vassa </ts>
               <ts e="T98" id="Seg_589" n="e" s="T97">tu͡okka </ts>
               <ts e="T99" id="Seg_591" n="e" s="T98">da </ts>
               <ts e="T100" id="Seg_593" n="e" s="T99">üleleːbet, </ts>
               <ts e="T101" id="Seg_595" n="e" s="T100">Uruŋ Kajaga </ts>
               <ts e="T102" id="Seg_597" n="e" s="T101">Uppaja </ts>
               <ts e="T103" id="Seg_599" n="e" s="T102">kimi͡eke </ts>
               <ts e="T104" id="Seg_601" n="e" s="T103">üleliːr </ts>
               <ts e="T105" id="Seg_603" n="e" s="T104">(olenevod-) </ts>
               <ts e="T106" id="Seg_605" n="e" s="T105">olenevodka </ts>
               <ts e="T107" id="Seg_607" n="e" s="T106">üleliːr </ts>
               <ts e="T108" id="Seg_609" n="e" s="T107">taba </ts>
               <ts e="T109" id="Seg_611" n="e" s="T108">künüger </ts>
               <ts e="T110" id="Seg_613" n="e" s="T109">hɨrsa </ts>
               <ts e="T111" id="Seg_615" n="e" s="T110">keleːčči. </ts>
               <ts e="T112" id="Seg_617" n="e" s="T111">Hibi͡ete </ts>
               <ts e="T113" id="Seg_619" n="e" s="T112">emi͡e </ts>
               <ts e="T114" id="Seg_621" n="e" s="T113">hɨrsaːččɨ, </ts>
               <ts e="T115" id="Seg_623" n="e" s="T114">Senja </ts>
               <ts e="T116" id="Seg_625" n="e" s="T115">hɨrsaːččɨ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KuSE">
            <ta e="T6" id="Seg_626" s="T1">KuSE_2009_Family_nar.KuSE.001 (001)</ta>
            <ta e="T13" id="Seg_627" s="T9">KuSE_2009_Family_nar.KuSE.002 (003)</ta>
            <ta e="T16" id="Seg_628" s="T13">KuSE_2009_Family_nar.KuSE.003 (004)</ta>
            <ta e="T20" id="Seg_629" s="T16">KuSE_2009_Family_nar.KuSE.004 (005)</ta>
            <ta e="T27" id="Seg_630" s="T20">KuSE_2009_Family_nar.KuSE.005 (006)</ta>
            <ta e="T29" id="Seg_631" s="T27">KuSE_2009_Family_nar.KuSE.006 (007)</ta>
            <ta e="T40" id="Seg_632" s="T31">KuSE_2009_Family_nar.KuSE.007 (009)</ta>
            <ta e="T47" id="Seg_633" s="T40">KuSE_2009_Family_nar.KuSE.008 (010)</ta>
            <ta e="T50" id="Seg_634" s="T47">KuSE_2009_Family_nar.KuSE.009 (011)</ta>
            <ta e="T62" id="Seg_635" s="T50">KuSE_2009_Family_nar.KuSE.010 (012)</ta>
            <ta e="T77" id="Seg_636" s="T62">KuSE_2009_Family_nar.KuSE.011 (013)</ta>
            <ta e="T83" id="Seg_637" s="T77">KuSE_2009_Family_nar.KuSE.012 (014)</ta>
            <ta e="T96" id="Seg_638" s="T83">KuSE_2009_Family_nar.KuSE.013 (015)</ta>
            <ta e="T111" id="Seg_639" s="T96">KuSE_2009_Family_nar.KuSE.014 (016.001)</ta>
            <ta e="T116" id="Seg_640" s="T111">KuSE_2009_Family_nar.KuSE.015 (016.002)</ta>
         </annotation>
         <annotation name="st" tierref="st-KuSE">
            <ta e="T6" id="Seg_641" s="T1">Нас в семье семеро детей.</ta>
            <ta e="T13" id="Seg_642" s="T9">Биһиги семьябытыгар һэттэ киһибит.</ta>
            <ta e="T16" id="Seg_643" s="T13">Биһиги кас этэй..</ta>
            <ta e="T20" id="Seg_644" s="T16">Три девочки четыре мальчика.</ta>
            <ta e="T27" id="Seg_645" s="T20">Васса, Света, Юля, Ваня, Сеня и я.</ta>
            <ta e="T29" id="Seg_646" s="T27">Один приемный…</ta>
            <ta e="T40" id="Seg_647" s="T31">Биир огону иттибиппит аата Миша, ону төттөрү биэртибит мааматыгар.</ta>
            <ta e="T47" id="Seg_648" s="T40">Онтуӈ только биһиэкэ питаться һылдьар этэ, урут.</ta>
            <ta e="T50" id="Seg_649" s="T47">Онтон кэ тугуй.</ta>
            <ta e="T62" id="Seg_650" s="T50">Биирим Анабарга баар Юрюнг-Хаяга, биирим Волочанкага баар, биирим 3ында… 3ындасскага тыага һылдьааччи.</ta>
            <ta e="T77" id="Seg_651" s="T62">Биир биир биир эдийим 3ындааскага баар оголок икки оголок, тыага һылдьааччи этилэрэ аны үлэлиир садикка.</ta>
            <ta e="T83" id="Seg_652" s="T77">Огото манна баар, бу олорор огото.</ta>
            <ta e="T96" id="Seg_653" s="T83">Васса ким Турага баар этэ, онно ого төрөөбүтэ, аата Егорка, бэйэтин аата Васса.</ta>
            <ta e="T111" id="Seg_654" s="T96">Васса туокка да үлэлээбэт, Юрюнг-Хаяга Уппайа кимиэкэ үлэлиир оленевод оленеводка үлэлиир таба күнүгэр һырса кэлээччи. </ta>
            <ta e="T116" id="Seg_655" s="T111">3ибиэтэ эмиэ һырсааччи, Сеня һырсааччи.</ta>
         </annotation>
         <annotation name="ts" tierref="ts-KuSE">
            <ta e="T6" id="Seg_656" s="T1">Nas v semʼe semero detej. </ta>
            <ta e="T13" id="Seg_657" s="T9">Bihigi semjabɨtɨgar hette kihibit. </ta>
            <ta e="T16" id="Seg_658" s="T13">Bihigi kas etej? </ta>
            <ta e="T20" id="Seg_659" s="T16">Tri devočki četɨre malʼčika. </ta>
            <ta e="T27" id="Seg_660" s="T20">Vassa, Sveta, Julja, Vanja, Senja i ja. </ta>
            <ta e="T29" id="Seg_661" s="T27">Odin prijomnɨj... </ta>
            <ta e="T40" id="Seg_662" s="T31">Biːr ogonu iːttibippit aːta Miša, onu töttörü bi͡ertibit maːmatɨgar. </ta>
            <ta e="T47" id="Seg_663" s="T40">Ontuŋ tolʼko bihi͡eke pitatʼsja hɨlʼdʼar ete, urut. </ta>
            <ta e="T50" id="Seg_664" s="T47">Onton ke tuguj. </ta>
            <ta e="T62" id="Seg_665" s="T50">Biːrim Anabarga baːr Uruŋ Kajaga, biːrim Voločankaga baːr, biːrim (Hɨnda-) Hɨndaskaga tɨ͡aga hɨldʼaːččɨ. </ta>
            <ta e="T77" id="Seg_666" s="T62">Biːr biːr biːr edʼijim Hɨndaːskaga baːr ogoloːk ikki ogoloːk, tɨ͡aga hɨlʼdʼaːččɨ etilere anɨ üleliːr sadikka. </ta>
            <ta e="T83" id="Seg_667" s="T77">Ogoto manna baːr, bu oloror ogoto. </ta>
            <ta e="T96" id="Seg_668" s="T83">Vassa kim Turaga baːr ete, onno ogo töröːbüte, aːta Egorka, bejetin aːta Vassa. </ta>
            <ta e="T111" id="Seg_669" s="T96">Vassa tu͡okka da üleleːbet, Uruŋ Kajaga Uppaja kimi͡eke üleliːr (olenevod-) olenevodka üleliːr taba künüger hɨrsa keleːčči. </ta>
            <ta e="T116" id="Seg_670" s="T111">Hibi͡ete emi͡e hɨrsaːččɨ, Senja hɨrsaːččɨ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-KuSE">
            <ta e="T10" id="Seg_671" s="T9">bihigi</ta>
            <ta e="T11" id="Seg_672" s="T10">semja-bɨtɨ-gar</ta>
            <ta e="T12" id="Seg_673" s="T11">hette</ta>
            <ta e="T13" id="Seg_674" s="T12">kihi-bit</ta>
            <ta e="T14" id="Seg_675" s="T13">bihigi</ta>
            <ta e="T15" id="Seg_676" s="T14">kas</ta>
            <ta e="T16" id="Seg_677" s="T15">e-t-e=j</ta>
            <ta e="T32" id="Seg_678" s="T31">biːr</ta>
            <ta e="T33" id="Seg_679" s="T32">ogo-nu</ta>
            <ta e="T34" id="Seg_680" s="T33">iːt-ti-bip-pit</ta>
            <ta e="T35" id="Seg_681" s="T34">aːt-a</ta>
            <ta e="T37" id="Seg_682" s="T36">o-nu</ta>
            <ta e="T38" id="Seg_683" s="T37">töttörü</ta>
            <ta e="T39" id="Seg_684" s="T38">bi͡er-ti-bit</ta>
            <ta e="T40" id="Seg_685" s="T39">maːma-tɨ-gar</ta>
            <ta e="T41" id="Seg_686" s="T40">on-tu-ŋ</ta>
            <ta e="T42" id="Seg_687" s="T41">tolʼko</ta>
            <ta e="T43" id="Seg_688" s="T42">bihi͡e-ke</ta>
            <ta e="T45" id="Seg_689" s="T44">hɨlʼdʼ-ar</ta>
            <ta e="T46" id="Seg_690" s="T45">e-t-e</ta>
            <ta e="T47" id="Seg_691" s="T46">urut</ta>
            <ta e="T48" id="Seg_692" s="T47">on-ton</ta>
            <ta e="T49" id="Seg_693" s="T48">ke</ta>
            <ta e="T50" id="Seg_694" s="T49">tug=uj</ta>
            <ta e="T51" id="Seg_695" s="T50">biːr-i-m</ta>
            <ta e="T52" id="Seg_696" s="T51">Anabar-ga</ta>
            <ta e="T53" id="Seg_697" s="T52">baːr</ta>
            <ta e="T54" id="Seg_698" s="T53">Uruŋ Kaja-ga</ta>
            <ta e="T55" id="Seg_699" s="T54">biːr-i-m</ta>
            <ta e="T56" id="Seg_700" s="T55">Voločanka-ga</ta>
            <ta e="T57" id="Seg_701" s="T56">baːr</ta>
            <ta e="T58" id="Seg_702" s="T57">biːr-i-m</ta>
            <ta e="T60" id="Seg_703" s="T59">Hɨndaska-ga</ta>
            <ta e="T61" id="Seg_704" s="T60">tɨ͡a-ga</ta>
            <ta e="T62" id="Seg_705" s="T61">hɨldʼ-aːččɨ</ta>
            <ta e="T63" id="Seg_706" s="T62">biːr</ta>
            <ta e="T64" id="Seg_707" s="T63">biːr</ta>
            <ta e="T65" id="Seg_708" s="T64">biːr</ta>
            <ta e="T66" id="Seg_709" s="T65">edʼij-i-m</ta>
            <ta e="T67" id="Seg_710" s="T66">Hɨndaːska-ga</ta>
            <ta e="T68" id="Seg_711" s="T67">baːr</ta>
            <ta e="T69" id="Seg_712" s="T68">ogo-loːk</ta>
            <ta e="T70" id="Seg_713" s="T69">ikki</ta>
            <ta e="T71" id="Seg_714" s="T70">ogo-loːk</ta>
            <ta e="T72" id="Seg_715" s="T71">tɨ͡a-ga</ta>
            <ta e="T73" id="Seg_716" s="T72">hɨlʼdʼ-aːččɨ</ta>
            <ta e="T74" id="Seg_717" s="T73">e-ti-lere</ta>
            <ta e="T75" id="Seg_718" s="T74">anɨ</ta>
            <ta e="T76" id="Seg_719" s="T75">üleliː-r</ta>
            <ta e="T77" id="Seg_720" s="T76">sadik-ka</ta>
            <ta e="T78" id="Seg_721" s="T77">ogo-to</ta>
            <ta e="T79" id="Seg_722" s="T78">manna</ta>
            <ta e="T80" id="Seg_723" s="T79">baːr</ta>
            <ta e="T81" id="Seg_724" s="T80">bu</ta>
            <ta e="T82" id="Seg_725" s="T81">olor-or</ta>
            <ta e="T83" id="Seg_726" s="T82">ogo-to</ta>
            <ta e="T84" id="Seg_727" s="T83">Vassa</ta>
            <ta e="T85" id="Seg_728" s="T84">kim</ta>
            <ta e="T86" id="Seg_729" s="T85">Tura-ga</ta>
            <ta e="T87" id="Seg_730" s="T86">baːr</ta>
            <ta e="T88" id="Seg_731" s="T87">e-t-e</ta>
            <ta e="T89" id="Seg_732" s="T88">onno</ta>
            <ta e="T90" id="Seg_733" s="T89">ogo</ta>
            <ta e="T91" id="Seg_734" s="T90">töröː-büt-e</ta>
            <ta e="T92" id="Seg_735" s="T91">aːt-a</ta>
            <ta e="T93" id="Seg_736" s="T92">Egorka</ta>
            <ta e="T94" id="Seg_737" s="T93">beje-ti-n</ta>
            <ta e="T95" id="Seg_738" s="T94">aːt-a</ta>
            <ta e="T96" id="Seg_739" s="T95">Vassa</ta>
            <ta e="T97" id="Seg_740" s="T96">Vassa</ta>
            <ta e="T98" id="Seg_741" s="T97">tu͡ok-ka</ta>
            <ta e="T99" id="Seg_742" s="T98">da</ta>
            <ta e="T100" id="Seg_743" s="T99">üleleː-bet</ta>
            <ta e="T101" id="Seg_744" s="T100">Uruŋ Kaja-ga</ta>
            <ta e="T102" id="Seg_745" s="T101">Uppaja</ta>
            <ta e="T103" id="Seg_746" s="T102">kimi͡e-ke</ta>
            <ta e="T104" id="Seg_747" s="T103">üleliː-r</ta>
            <ta e="T105" id="Seg_748" s="T104">olenevod</ta>
            <ta e="T106" id="Seg_749" s="T105">olenevod-ka</ta>
            <ta e="T107" id="Seg_750" s="T106">üleliː-r</ta>
            <ta e="T108" id="Seg_751" s="T107">taba</ta>
            <ta e="T109" id="Seg_752" s="T108">kün-ü-ger</ta>
            <ta e="T110" id="Seg_753" s="T109">hɨrs-a</ta>
            <ta e="T111" id="Seg_754" s="T110">kel-eːčči</ta>
            <ta e="T112" id="Seg_755" s="T111">Hibi͡ete</ta>
            <ta e="T113" id="Seg_756" s="T112">emi͡e</ta>
            <ta e="T114" id="Seg_757" s="T113">hɨrs-aːččɨ</ta>
            <ta e="T115" id="Seg_758" s="T114">Senja</ta>
            <ta e="T116" id="Seg_759" s="T115">hɨrs-aːččɨ</ta>
         </annotation>
         <annotation name="mp" tierref="mp-KuSE">
            <ta e="T10" id="Seg_760" s="T9">bihigi</ta>
            <ta e="T11" id="Seg_761" s="T10">semja-BItI-GAr</ta>
            <ta e="T12" id="Seg_762" s="T11">hette</ta>
            <ta e="T13" id="Seg_763" s="T12">kihi-BIt</ta>
            <ta e="T14" id="Seg_764" s="T13">bihigi</ta>
            <ta e="T15" id="Seg_765" s="T14">kas</ta>
            <ta e="T16" id="Seg_766" s="T15">e-TI-tA=Ij</ta>
            <ta e="T32" id="Seg_767" s="T31">biːr</ta>
            <ta e="T33" id="Seg_768" s="T32">ogo-nI</ta>
            <ta e="T34" id="Seg_769" s="T33">iːt-TI-BIT-BIt</ta>
            <ta e="T35" id="Seg_770" s="T34">aːt-tA</ta>
            <ta e="T37" id="Seg_771" s="T36">ol-nI</ta>
            <ta e="T38" id="Seg_772" s="T37">töttörü</ta>
            <ta e="T39" id="Seg_773" s="T38">bi͡er-TI-BIt</ta>
            <ta e="T40" id="Seg_774" s="T39">maːma-tI-GAr</ta>
            <ta e="T41" id="Seg_775" s="T40">ol-tI-ŋ</ta>
            <ta e="T42" id="Seg_776" s="T41">tolʼko</ta>
            <ta e="T43" id="Seg_777" s="T42">bihigi-GA</ta>
            <ta e="T45" id="Seg_778" s="T44">hɨrɨt-Ar</ta>
            <ta e="T46" id="Seg_779" s="T45">e-TI-tA</ta>
            <ta e="T47" id="Seg_780" s="T46">urut</ta>
            <ta e="T48" id="Seg_781" s="T47">ol-ttAn</ta>
            <ta e="T49" id="Seg_782" s="T48">ka</ta>
            <ta e="T50" id="Seg_783" s="T49">tu͡ok=Ij</ta>
            <ta e="T51" id="Seg_784" s="T50">biːr-I-m</ta>
            <ta e="T52" id="Seg_785" s="T51">Anabar-GA</ta>
            <ta e="T53" id="Seg_786" s="T52">baːr</ta>
            <ta e="T54" id="Seg_787" s="T53">Ürüŋ Kaja-GA</ta>
            <ta e="T55" id="Seg_788" s="T54">biːr-I-m</ta>
            <ta e="T56" id="Seg_789" s="T55">Voločanka-GA</ta>
            <ta e="T57" id="Seg_790" s="T56">baːr</ta>
            <ta e="T58" id="Seg_791" s="T57">biːr-I-m</ta>
            <ta e="T60" id="Seg_792" s="T59">Hɨndaːska-GA</ta>
            <ta e="T61" id="Seg_793" s="T60">tɨ͡a-GA</ta>
            <ta e="T62" id="Seg_794" s="T61">hɨrɨt-AːččI</ta>
            <ta e="T63" id="Seg_795" s="T62">biːr</ta>
            <ta e="T64" id="Seg_796" s="T63">biːr</ta>
            <ta e="T65" id="Seg_797" s="T64">biːr</ta>
            <ta e="T66" id="Seg_798" s="T65">edʼij-I-m</ta>
            <ta e="T67" id="Seg_799" s="T66">Hɨndaːska-GA</ta>
            <ta e="T68" id="Seg_800" s="T67">baːr</ta>
            <ta e="T69" id="Seg_801" s="T68">ogo-LAːK</ta>
            <ta e="T70" id="Seg_802" s="T69">ikki</ta>
            <ta e="T71" id="Seg_803" s="T70">ogo-LAːK</ta>
            <ta e="T72" id="Seg_804" s="T71">tɨ͡a-GA</ta>
            <ta e="T73" id="Seg_805" s="T72">hɨrɨt-AːččI</ta>
            <ta e="T74" id="Seg_806" s="T73">e-TI-LArA</ta>
            <ta e="T75" id="Seg_807" s="T74">anɨ</ta>
            <ta e="T76" id="Seg_808" s="T75">üleleː-Ar</ta>
            <ta e="T77" id="Seg_809" s="T76">saːdik-GA</ta>
            <ta e="T78" id="Seg_810" s="T77">ogo-tA</ta>
            <ta e="T79" id="Seg_811" s="T78">manna</ta>
            <ta e="T80" id="Seg_812" s="T79">baːr</ta>
            <ta e="T81" id="Seg_813" s="T80">bu</ta>
            <ta e="T82" id="Seg_814" s="T81">olor-Ar</ta>
            <ta e="T83" id="Seg_815" s="T82">ogo-tA</ta>
            <ta e="T84" id="Seg_816" s="T83">Vasja</ta>
            <ta e="T85" id="Seg_817" s="T84">kim</ta>
            <ta e="T86" id="Seg_818" s="T85">Tura-GA</ta>
            <ta e="T87" id="Seg_819" s="T86">baːr</ta>
            <ta e="T88" id="Seg_820" s="T87">e-TI-tA</ta>
            <ta e="T89" id="Seg_821" s="T88">onno</ta>
            <ta e="T90" id="Seg_822" s="T89">ogo</ta>
            <ta e="T91" id="Seg_823" s="T90">töröː-BIT-tA</ta>
            <ta e="T92" id="Seg_824" s="T91">aːt-tA</ta>
            <ta e="T93" id="Seg_825" s="T92">Egorka</ta>
            <ta e="T94" id="Seg_826" s="T93">beje-tI-n</ta>
            <ta e="T95" id="Seg_827" s="T94">aːt-tA</ta>
            <ta e="T96" id="Seg_828" s="T95">Vasja</ta>
            <ta e="T97" id="Seg_829" s="T96">Vasja</ta>
            <ta e="T98" id="Seg_830" s="T97">tu͡ok-GA</ta>
            <ta e="T99" id="Seg_831" s="T98">da</ta>
            <ta e="T100" id="Seg_832" s="T99">üleleː-BAT</ta>
            <ta e="T101" id="Seg_833" s="T100">Ürüŋ Kaja-GA</ta>
            <ta e="T102" id="Seg_834" s="T101">Uppaja</ta>
            <ta e="T103" id="Seg_835" s="T102">kim-GA</ta>
            <ta e="T104" id="Seg_836" s="T103">üleleː-Ar</ta>
            <ta e="T105" id="Seg_837" s="T104">olenevod</ta>
            <ta e="T106" id="Seg_838" s="T105">olenevod-GA</ta>
            <ta e="T107" id="Seg_839" s="T106">üleleː-Ar</ta>
            <ta e="T108" id="Seg_840" s="T107">taba</ta>
            <ta e="T109" id="Seg_841" s="T108">kün-tI-GAr</ta>
            <ta e="T110" id="Seg_842" s="T109">hɨrɨs-A</ta>
            <ta e="T111" id="Seg_843" s="T110">kel-AːččI</ta>
            <ta e="T112" id="Seg_844" s="T111">Hibi͡ete</ta>
            <ta e="T113" id="Seg_845" s="T112">emi͡e</ta>
            <ta e="T114" id="Seg_846" s="T113">hɨrɨs-AːččI</ta>
            <ta e="T115" id="Seg_847" s="T114">Senja</ta>
            <ta e="T116" id="Seg_848" s="T115">hɨrɨs-AːččI</ta>
         </annotation>
         <annotation name="ge" tierref="ge-KuSE">
            <ta e="T10" id="Seg_849" s="T9">1PL.[NOM]</ta>
            <ta e="T11" id="Seg_850" s="T10">family-1PL-DAT/LOC</ta>
            <ta e="T12" id="Seg_851" s="T11">seven</ta>
            <ta e="T13" id="Seg_852" s="T12">human.being-1PL</ta>
            <ta e="T14" id="Seg_853" s="T13">1PL.[NOM]</ta>
            <ta e="T15" id="Seg_854" s="T14">how.much</ta>
            <ta e="T16" id="Seg_855" s="T15">be-PST1-3SG=Q</ta>
            <ta e="T32" id="Seg_856" s="T31">one</ta>
            <ta e="T33" id="Seg_857" s="T32">child-ACC</ta>
            <ta e="T34" id="Seg_858" s="T33">bring.up-PST1-PST2-1PL</ta>
            <ta e="T35" id="Seg_859" s="T34">name-3SG.[NOM]</ta>
            <ta e="T37" id="Seg_860" s="T36">that -ACC</ta>
            <ta e="T38" id="Seg_861" s="T37">back</ta>
            <ta e="T39" id="Seg_862" s="T38">give-PST1-1PL</ta>
            <ta e="T40" id="Seg_863" s="T39">mum-3SG-DAT/LOC</ta>
            <ta e="T41" id="Seg_864" s="T40">that -3SG-2SG.[NOM]</ta>
            <ta e="T42" id="Seg_865" s="T41">only</ta>
            <ta e="T43" id="Seg_866" s="T42">1PL-DAT/LOC</ta>
            <ta e="T45" id="Seg_867" s="T44">go-PTCP.PRS</ta>
            <ta e="T46" id="Seg_868" s="T45">be-PST1-3SG</ta>
            <ta e="T47" id="Seg_869" s="T46">before</ta>
            <ta e="T48" id="Seg_870" s="T47">that -ABL</ta>
            <ta e="T49" id="Seg_871" s="T48">well</ta>
            <ta e="T50" id="Seg_872" s="T49">what.[NOM]=Q</ta>
            <ta e="T51" id="Seg_873" s="T50">one-EP-1SG.[NOM]</ta>
            <ta e="T52" id="Seg_874" s="T51">Anabar-DAT/LOC</ta>
            <ta e="T53" id="Seg_875" s="T52">there.is</ta>
            <ta e="T54" id="Seg_876" s="T53">Urung.Xaya-DAT/LOC</ta>
            <ta e="T55" id="Seg_877" s="T54">one-EP-1SG.[NOM]</ta>
            <ta e="T56" id="Seg_878" s="T55">Volochanka-DAT/LOC</ta>
            <ta e="T57" id="Seg_879" s="T56">there.is</ta>
            <ta e="T58" id="Seg_880" s="T57">one-EP-1SG.[NOM]</ta>
            <ta e="T60" id="Seg_881" s="T59">Syndassko-DAT/LOC</ta>
            <ta e="T61" id="Seg_882" s="T60">tundra-DAT/LOC</ta>
            <ta e="T62" id="Seg_883" s="T61">go-HAB.[3SG]</ta>
            <ta e="T63" id="Seg_884" s="T62">one</ta>
            <ta e="T64" id="Seg_885" s="T63">one</ta>
            <ta e="T65" id="Seg_886" s="T64">one</ta>
            <ta e="T66" id="Seg_887" s="T65">older.sister-EP-1SG.[NOM]</ta>
            <ta e="T67" id="Seg_888" s="T66">Syndassko-DAT/LOC</ta>
            <ta e="T68" id="Seg_889" s="T67">there.is</ta>
            <ta e="T69" id="Seg_890" s="T68">child-PROPR.[NOM]</ta>
            <ta e="T70" id="Seg_891" s="T69">two</ta>
            <ta e="T71" id="Seg_892" s="T70">child-PROPR.[NOM]</ta>
            <ta e="T72" id="Seg_893" s="T71">tundra-DAT/LOC</ta>
            <ta e="T73" id="Seg_894" s="T72">go-PTCP.HAB</ta>
            <ta e="T74" id="Seg_895" s="T73">be-PST1-3PL</ta>
            <ta e="T75" id="Seg_896" s="T74">now</ta>
            <ta e="T76" id="Seg_897" s="T75">work-PRS.[3SG]</ta>
            <ta e="T77" id="Seg_898" s="T76">kindergarten-DAT/LOC</ta>
            <ta e="T78" id="Seg_899" s="T77">child-3SG.[NOM]</ta>
            <ta e="T79" id="Seg_900" s="T78">here</ta>
            <ta e="T80" id="Seg_901" s="T79">there.is</ta>
            <ta e="T81" id="Seg_902" s="T80">this</ta>
            <ta e="T82" id="Seg_903" s="T81">sit-PRS.[3SG]</ta>
            <ta e="T83" id="Seg_904" s="T82">child-3SG.[NOM]</ta>
            <ta e="T84" id="Seg_905" s="T83">Vasya.[NOM]</ta>
            <ta e="T85" id="Seg_906" s="T84">who.[NOM]</ta>
            <ta e="T86" id="Seg_907" s="T85">Tura-DAT/LOC</ta>
            <ta e="T87" id="Seg_908" s="T86">there.is</ta>
            <ta e="T88" id="Seg_909" s="T87">be-PST1-3SG</ta>
            <ta e="T89" id="Seg_910" s="T88">there</ta>
            <ta e="T90" id="Seg_911" s="T89">child.[NOM]</ta>
            <ta e="T91" id="Seg_912" s="T90">give.birth-PST2-3SG</ta>
            <ta e="T92" id="Seg_913" s="T91">name-3SG.[NOM]</ta>
            <ta e="T93" id="Seg_914" s="T92">Yegorka.[NOM]</ta>
            <ta e="T94" id="Seg_915" s="T93">self-3SG-GEN</ta>
            <ta e="T95" id="Seg_916" s="T94">name-3SG.[NOM]</ta>
            <ta e="T96" id="Seg_917" s="T95">Vasya.[NOM]</ta>
            <ta e="T97" id="Seg_918" s="T96">Vasya.[NOM]</ta>
            <ta e="T98" id="Seg_919" s="T97">what-DAT/LOC</ta>
            <ta e="T99" id="Seg_920" s="T98">NEG</ta>
            <ta e="T100" id="Seg_921" s="T99">work-NEG.[3SG]</ta>
            <ta e="T101" id="Seg_922" s="T100">Urung.Xaya-DAT/LOC</ta>
            <ta e="T102" id="Seg_923" s="T101">Vanya.[NOM]</ta>
            <ta e="T103" id="Seg_924" s="T102">who-DAT/LOC</ta>
            <ta e="T104" id="Seg_925" s="T103">work-PRS.[3SG]</ta>
            <ta e="T105" id="Seg_926" s="T104">reindeer.herder</ta>
            <ta e="T106" id="Seg_927" s="T105">reindeer.herder-DAT/LOC</ta>
            <ta e="T107" id="Seg_928" s="T106">work-PRS.[3SG]</ta>
            <ta e="T108" id="Seg_929" s="T107">reindeer.[NOM]</ta>
            <ta e="T109" id="Seg_930" s="T108">day-3SG-DAT/LOC</ta>
            <ta e="T110" id="Seg_931" s="T109">run.a.race-CVB.SIM</ta>
            <ta e="T111" id="Seg_932" s="T110">come-HAB.[3SG]</ta>
            <ta e="T112" id="Seg_933" s="T111">Sveta.[NOM]</ta>
            <ta e="T113" id="Seg_934" s="T112">also</ta>
            <ta e="T114" id="Seg_935" s="T113">run.a.race-HAB.[3SG]</ta>
            <ta e="T115" id="Seg_936" s="T114">Senya.[NOM]</ta>
            <ta e="T116" id="Seg_937" s="T115">run.a.race-HAB.[3SG]</ta>
         </annotation>
         <annotation name="gg" tierref="gg-KuSE">
            <ta e="T10" id="Seg_938" s="T9">1PL.[NOM]</ta>
            <ta e="T11" id="Seg_939" s="T10">Familie-1PL-DAT/LOC</ta>
            <ta e="T12" id="Seg_940" s="T11">sieben</ta>
            <ta e="T13" id="Seg_941" s="T12">Mensch-1PL</ta>
            <ta e="T14" id="Seg_942" s="T13">1PL.[NOM]</ta>
            <ta e="T15" id="Seg_943" s="T14">wie.viel</ta>
            <ta e="T16" id="Seg_944" s="T15">sein-PST1-3SG=Q</ta>
            <ta e="T32" id="Seg_945" s="T31">eins</ta>
            <ta e="T33" id="Seg_946" s="T32">Kind-ACC</ta>
            <ta e="T34" id="Seg_947" s="T33">aufziehen-PST1-PST2-1PL</ta>
            <ta e="T35" id="Seg_948" s="T34">Name-3SG.[NOM]</ta>
            <ta e="T37" id="Seg_949" s="T36">jenes-ACC</ta>
            <ta e="T38" id="Seg_950" s="T37">zurück</ta>
            <ta e="T39" id="Seg_951" s="T38">geben-PST1-1PL</ta>
            <ta e="T40" id="Seg_952" s="T39">Mama-3SG-DAT/LOC</ta>
            <ta e="T41" id="Seg_953" s="T40">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T42" id="Seg_954" s="T41">nur</ta>
            <ta e="T43" id="Seg_955" s="T42">1PL-DAT/LOC</ta>
            <ta e="T45" id="Seg_956" s="T44">gehen-PTCP.PRS</ta>
            <ta e="T46" id="Seg_957" s="T45">sein-PST1-3SG</ta>
            <ta e="T47" id="Seg_958" s="T46">früher</ta>
            <ta e="T48" id="Seg_959" s="T47">jenes-ABL</ta>
            <ta e="T49" id="Seg_960" s="T48">nun</ta>
            <ta e="T50" id="Seg_961" s="T49">was.[NOM]=Q</ta>
            <ta e="T51" id="Seg_962" s="T50">eins-EP-1SG.[NOM]</ta>
            <ta e="T52" id="Seg_963" s="T51">Anabar-DAT/LOC</ta>
            <ta e="T53" id="Seg_964" s="T52">es.gibt</ta>
            <ta e="T54" id="Seg_965" s="T53">Ürüng.Xaja-DAT/LOC</ta>
            <ta e="T55" id="Seg_966" s="T54">eins-EP-1SG.[NOM]</ta>
            <ta e="T56" id="Seg_967" s="T55">Voločanka-DAT/LOC</ta>
            <ta e="T57" id="Seg_968" s="T56">es.gibt</ta>
            <ta e="T58" id="Seg_969" s="T57">eins-EP-1SG.[NOM]</ta>
            <ta e="T60" id="Seg_970" s="T59">Syndassko-DAT/LOC</ta>
            <ta e="T61" id="Seg_971" s="T60">Tundra-DAT/LOC</ta>
            <ta e="T62" id="Seg_972" s="T61">gehen-HAB.[3SG]</ta>
            <ta e="T63" id="Seg_973" s="T62">eins</ta>
            <ta e="T64" id="Seg_974" s="T63">eins</ta>
            <ta e="T65" id="Seg_975" s="T64">eins</ta>
            <ta e="T66" id="Seg_976" s="T65">ältere.Schwester-EP-1SG.[NOM]</ta>
            <ta e="T67" id="Seg_977" s="T66">Syndassko-DAT/LOC</ta>
            <ta e="T68" id="Seg_978" s="T67">es.gibt</ta>
            <ta e="T69" id="Seg_979" s="T68">Kind-PROPR.[NOM]</ta>
            <ta e="T70" id="Seg_980" s="T69">zwei</ta>
            <ta e="T71" id="Seg_981" s="T70">Kind-PROPR.[NOM]</ta>
            <ta e="T72" id="Seg_982" s="T71">Tundra-DAT/LOC</ta>
            <ta e="T73" id="Seg_983" s="T72">gehen-PTCP.HAB</ta>
            <ta e="T74" id="Seg_984" s="T73">sein-PST1-3PL</ta>
            <ta e="T75" id="Seg_985" s="T74">jetzt</ta>
            <ta e="T76" id="Seg_986" s="T75">arbeiten-PRS.[3SG]</ta>
            <ta e="T77" id="Seg_987" s="T76">Kindergarten-DAT/LOC</ta>
            <ta e="T78" id="Seg_988" s="T77">Kind-3SG.[NOM]</ta>
            <ta e="T79" id="Seg_989" s="T78">hier</ta>
            <ta e="T80" id="Seg_990" s="T79">es.gibt</ta>
            <ta e="T81" id="Seg_991" s="T80">dieses</ta>
            <ta e="T82" id="Seg_992" s="T81">sitzen-PRS.[3SG]</ta>
            <ta e="T83" id="Seg_993" s="T82">Kind-3SG.[NOM]</ta>
            <ta e="T84" id="Seg_994" s="T83">Vasja.[NOM]</ta>
            <ta e="T85" id="Seg_995" s="T84">wer.[NOM]</ta>
            <ta e="T86" id="Seg_996" s="T85">Tura-DAT/LOC</ta>
            <ta e="T87" id="Seg_997" s="T86">es.gibt</ta>
            <ta e="T88" id="Seg_998" s="T87">sein-PST1-3SG</ta>
            <ta e="T89" id="Seg_999" s="T88">dort</ta>
            <ta e="T90" id="Seg_1000" s="T89">Kind.[NOM]</ta>
            <ta e="T91" id="Seg_1001" s="T90">gebären-PST2-3SG</ta>
            <ta e="T92" id="Seg_1002" s="T91">Name-3SG.[NOM]</ta>
            <ta e="T93" id="Seg_1003" s="T92">Jegorka.[NOM]</ta>
            <ta e="T94" id="Seg_1004" s="T93">selbst-3SG-GEN</ta>
            <ta e="T95" id="Seg_1005" s="T94">Name-3SG.[NOM]</ta>
            <ta e="T96" id="Seg_1006" s="T95">Vasja.[NOM]</ta>
            <ta e="T97" id="Seg_1007" s="T96">Vasja.[NOM]</ta>
            <ta e="T98" id="Seg_1008" s="T97">was-DAT/LOC</ta>
            <ta e="T99" id="Seg_1009" s="T98">NEG</ta>
            <ta e="T100" id="Seg_1010" s="T99">arbeiten-NEG.[3SG]</ta>
            <ta e="T101" id="Seg_1011" s="T100">Ürüng.Xaja-DAT/LOC</ta>
            <ta e="T102" id="Seg_1012" s="T101">Vanja.[NOM]</ta>
            <ta e="T103" id="Seg_1013" s="T102">wer-DAT/LOC</ta>
            <ta e="T104" id="Seg_1014" s="T103">arbeiten-PRS.[3SG]</ta>
            <ta e="T105" id="Seg_1015" s="T104">Rentierhirte</ta>
            <ta e="T106" id="Seg_1016" s="T105">Rentierhirte-DAT/LOC</ta>
            <ta e="T107" id="Seg_1017" s="T106">arbeiten-PRS.[3SG]</ta>
            <ta e="T108" id="Seg_1018" s="T107">Rentier.[NOM]</ta>
            <ta e="T109" id="Seg_1019" s="T108">Tag-3SG-DAT/LOC</ta>
            <ta e="T110" id="Seg_1020" s="T109">um.die.Wette.laufen-CVB.SIM</ta>
            <ta e="T111" id="Seg_1021" s="T110">kommen-HAB.[3SG]</ta>
            <ta e="T112" id="Seg_1022" s="T111">Sveta.[NOM]</ta>
            <ta e="T113" id="Seg_1023" s="T112">auch</ta>
            <ta e="T114" id="Seg_1024" s="T113">um.die.Wette.laufen-HAB.[3SG]</ta>
            <ta e="T115" id="Seg_1025" s="T114">Senja.[NOM]</ta>
            <ta e="T116" id="Seg_1026" s="T115">um.die.Wette.laufen-HAB.[3SG]</ta>
         </annotation>
         <annotation name="gr" tierref="gr-KuSE">
            <ta e="T10" id="Seg_1027" s="T9">1PL.[NOM]</ta>
            <ta e="T11" id="Seg_1028" s="T10">семья-1PL-DAT/LOC</ta>
            <ta e="T12" id="Seg_1029" s="T11">семь</ta>
            <ta e="T13" id="Seg_1030" s="T12">человек-1PL</ta>
            <ta e="T14" id="Seg_1031" s="T13">1PL.[NOM]</ta>
            <ta e="T15" id="Seg_1032" s="T14">сколько</ta>
            <ta e="T16" id="Seg_1033" s="T15">быть-PST1-3SG=Q</ta>
            <ta e="T32" id="Seg_1034" s="T31">один</ta>
            <ta e="T33" id="Seg_1035" s="T32">ребенок-ACC</ta>
            <ta e="T34" id="Seg_1036" s="T33">воспитывать-PST1-PST2-1PL</ta>
            <ta e="T35" id="Seg_1037" s="T34">имя-3SG.[NOM]</ta>
            <ta e="T37" id="Seg_1038" s="T36">тот -ACC</ta>
            <ta e="T38" id="Seg_1039" s="T37">назад</ta>
            <ta e="T39" id="Seg_1040" s="T38">давать-PST1-1PL</ta>
            <ta e="T40" id="Seg_1041" s="T39">мама-3SG-DAT/LOC</ta>
            <ta e="T41" id="Seg_1042" s="T40">тот -3SG-2SG.[NOM]</ta>
            <ta e="T42" id="Seg_1043" s="T41">только</ta>
            <ta e="T43" id="Seg_1044" s="T42">1PL-DAT/LOC</ta>
            <ta e="T45" id="Seg_1045" s="T44">идти-PTCP.PRS</ta>
            <ta e="T46" id="Seg_1046" s="T45">быть-PST1-3SG</ta>
            <ta e="T47" id="Seg_1047" s="T46">раньше</ta>
            <ta e="T48" id="Seg_1048" s="T47">тот -ABL</ta>
            <ta e="T49" id="Seg_1049" s="T48">вот</ta>
            <ta e="T50" id="Seg_1050" s="T49">что.[NOM]=Q</ta>
            <ta e="T51" id="Seg_1051" s="T50">один-EP-1SG.[NOM]</ta>
            <ta e="T52" id="Seg_1052" s="T51">Анабар-DAT/LOC</ta>
            <ta e="T53" id="Seg_1053" s="T52">есть</ta>
            <ta e="T54" id="Seg_1054" s="T53">Юрюнг.Хая-DAT/LOC</ta>
            <ta e="T55" id="Seg_1055" s="T54">один-EP-1SG.[NOM]</ta>
            <ta e="T56" id="Seg_1056" s="T55">Волочанка-DAT/LOC</ta>
            <ta e="T57" id="Seg_1057" s="T56">есть</ta>
            <ta e="T58" id="Seg_1058" s="T57">один-EP-1SG.[NOM]</ta>
            <ta e="T60" id="Seg_1059" s="T59">Сындасско-DAT/LOC</ta>
            <ta e="T61" id="Seg_1060" s="T60">тундра-DAT/LOC</ta>
            <ta e="T62" id="Seg_1061" s="T61">идти-HAB.[3SG]</ta>
            <ta e="T63" id="Seg_1062" s="T62">один</ta>
            <ta e="T64" id="Seg_1063" s="T63">один</ta>
            <ta e="T65" id="Seg_1064" s="T64">один</ta>
            <ta e="T66" id="Seg_1065" s="T65">старшая.сестра-EP-1SG.[NOM]</ta>
            <ta e="T67" id="Seg_1066" s="T66">Сындасско-DAT/LOC</ta>
            <ta e="T68" id="Seg_1067" s="T67">есть</ta>
            <ta e="T69" id="Seg_1068" s="T68">ребенок-PROPR.[NOM]</ta>
            <ta e="T70" id="Seg_1069" s="T69">два</ta>
            <ta e="T71" id="Seg_1070" s="T70">ребенок-PROPR.[NOM]</ta>
            <ta e="T72" id="Seg_1071" s="T71">тундра-DAT/LOC</ta>
            <ta e="T73" id="Seg_1072" s="T72">идти-PTCP.HAB</ta>
            <ta e="T74" id="Seg_1073" s="T73">быть-PST1-3PL</ta>
            <ta e="T75" id="Seg_1074" s="T74">теперь</ta>
            <ta e="T76" id="Seg_1075" s="T75">работать-PRS.[3SG]</ta>
            <ta e="T77" id="Seg_1076" s="T76">садик-DAT/LOC</ta>
            <ta e="T78" id="Seg_1077" s="T77">ребенок-3SG.[NOM]</ta>
            <ta e="T79" id="Seg_1078" s="T78">здесь</ta>
            <ta e="T80" id="Seg_1079" s="T79">есть</ta>
            <ta e="T81" id="Seg_1080" s="T80">этот</ta>
            <ta e="T82" id="Seg_1081" s="T81">сидеть-PRS.[3SG]</ta>
            <ta e="T83" id="Seg_1082" s="T82">ребенок-3SG.[NOM]</ta>
            <ta e="T84" id="Seg_1083" s="T83">Вася.[NOM]</ta>
            <ta e="T85" id="Seg_1084" s="T84">кто.[NOM]</ta>
            <ta e="T86" id="Seg_1085" s="T85">Тура-DAT/LOC</ta>
            <ta e="T87" id="Seg_1086" s="T86">есть</ta>
            <ta e="T88" id="Seg_1087" s="T87">быть-PST1-3SG</ta>
            <ta e="T89" id="Seg_1088" s="T88">там</ta>
            <ta e="T90" id="Seg_1089" s="T89">ребенок.[NOM]</ta>
            <ta e="T91" id="Seg_1090" s="T90">родить-PST2-3SG</ta>
            <ta e="T92" id="Seg_1091" s="T91">имя-3SG.[NOM]</ta>
            <ta e="T93" id="Seg_1092" s="T92">Егорка.[NOM]</ta>
            <ta e="T94" id="Seg_1093" s="T93">сам-3SG-GEN</ta>
            <ta e="T95" id="Seg_1094" s="T94">имя-3SG.[NOM]</ta>
            <ta e="T96" id="Seg_1095" s="T95">Вася.[NOM]</ta>
            <ta e="T97" id="Seg_1096" s="T96">Вася.[NOM]</ta>
            <ta e="T98" id="Seg_1097" s="T97">что-DAT/LOC</ta>
            <ta e="T99" id="Seg_1098" s="T98">NEG</ta>
            <ta e="T100" id="Seg_1099" s="T99">работать-NEG.[3SG]</ta>
            <ta e="T101" id="Seg_1100" s="T100">Юрюнг.Хая-DAT/LOC</ta>
            <ta e="T102" id="Seg_1101" s="T101">Ваня.[NOM]</ta>
            <ta e="T103" id="Seg_1102" s="T102">кто-DAT/LOC</ta>
            <ta e="T104" id="Seg_1103" s="T103">работать-PRS.[3SG]</ta>
            <ta e="T105" id="Seg_1104" s="T104">оленевод</ta>
            <ta e="T106" id="Seg_1105" s="T105">оленевод-DAT/LOC</ta>
            <ta e="T107" id="Seg_1106" s="T106">работать-PRS.[3SG]</ta>
            <ta e="T108" id="Seg_1107" s="T107">олень.[NOM]</ta>
            <ta e="T109" id="Seg_1108" s="T108">день-3SG-DAT/LOC</ta>
            <ta e="T110" id="Seg_1109" s="T109">бежить.наперегонки-CVB.SIM</ta>
            <ta e="T111" id="Seg_1110" s="T110">приходить-HAB.[3SG]</ta>
            <ta e="T112" id="Seg_1111" s="T111">Света.[NOM]</ta>
            <ta e="T113" id="Seg_1112" s="T112">тоже</ta>
            <ta e="T114" id="Seg_1113" s="T113">бежить.наперегонки-HAB.[3SG]</ta>
            <ta e="T115" id="Seg_1114" s="T114">Сеня.[NOM]</ta>
            <ta e="T116" id="Seg_1115" s="T115">бежить.наперегонки-HAB.[3SG]</ta>
         </annotation>
         <annotation name="mc" tierref="mc-KuSE">
            <ta e="T10" id="Seg_1116" s="T9">pers.[pro:case]</ta>
            <ta e="T11" id="Seg_1117" s="T10">n-n:poss-n:case</ta>
            <ta e="T12" id="Seg_1118" s="T11">cardnum</ta>
            <ta e="T13" id="Seg_1119" s="T12">n-n:(pred.pn)</ta>
            <ta e="T14" id="Seg_1120" s="T13">pers.[pro:case]</ta>
            <ta e="T15" id="Seg_1121" s="T14">que</ta>
            <ta e="T16" id="Seg_1122" s="T15">v-v:tense-v:poss.pn=ptcl</ta>
            <ta e="T32" id="Seg_1123" s="T31">cardnum</ta>
            <ta e="T33" id="Seg_1124" s="T32">n-n:case</ta>
            <ta e="T34" id="Seg_1125" s="T33">v-v:tense-v:tense-v:pred.pn</ta>
            <ta e="T35" id="Seg_1126" s="T34">n-n:(poss).[n:case]</ta>
            <ta e="T37" id="Seg_1127" s="T36">dempro-pro:case</ta>
            <ta e="T38" id="Seg_1128" s="T37">adv</ta>
            <ta e="T39" id="Seg_1129" s="T38">v-v:tense-v:poss.pn</ta>
            <ta e="T40" id="Seg_1130" s="T39">n-n:poss-n:case</ta>
            <ta e="T41" id="Seg_1131" s="T40">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T42" id="Seg_1132" s="T41">adv</ta>
            <ta e="T43" id="Seg_1133" s="T42">pers-pro:case</ta>
            <ta e="T45" id="Seg_1134" s="T44">v-v:ptcp</ta>
            <ta e="T46" id="Seg_1135" s="T45">v-v:tense-v:poss.pn</ta>
            <ta e="T47" id="Seg_1136" s="T46">adv</ta>
            <ta e="T48" id="Seg_1137" s="T47">dempro-pro:case</ta>
            <ta e="T49" id="Seg_1138" s="T48">ptcl</ta>
            <ta e="T50" id="Seg_1139" s="T49">que.[pro:case]=ptcl</ta>
            <ta e="T51" id="Seg_1140" s="T50">cardnum-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T52" id="Seg_1141" s="T51">propr-n:case</ta>
            <ta e="T53" id="Seg_1142" s="T52">ptcl</ta>
            <ta e="T54" id="Seg_1143" s="T53">propr-n:case</ta>
            <ta e="T55" id="Seg_1144" s="T54">cardnum-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T56" id="Seg_1145" s="T55">propr-n:case</ta>
            <ta e="T57" id="Seg_1146" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_1147" s="T57">cardnum-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T60" id="Seg_1148" s="T59">propr-n:case</ta>
            <ta e="T61" id="Seg_1149" s="T60">n-n:case</ta>
            <ta e="T62" id="Seg_1150" s="T61">v-v:mood.[v:pred.pn]</ta>
            <ta e="T63" id="Seg_1151" s="T62">cardnum</ta>
            <ta e="T64" id="Seg_1152" s="T63">cardnum</ta>
            <ta e="T65" id="Seg_1153" s="T64">cardnum</ta>
            <ta e="T66" id="Seg_1154" s="T65">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T67" id="Seg_1155" s="T66">propr-n:case</ta>
            <ta e="T68" id="Seg_1156" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_1157" s="T68">n-n&gt;adj.[n:case]</ta>
            <ta e="T70" id="Seg_1158" s="T69">cardnum</ta>
            <ta e="T71" id="Seg_1159" s="T70">n-n&gt;adj.[n:case]</ta>
            <ta e="T72" id="Seg_1160" s="T71">n-n:case</ta>
            <ta e="T73" id="Seg_1161" s="T72">v-v:ptcp</ta>
            <ta e="T74" id="Seg_1162" s="T73">v-v:tense-v:poss.pn</ta>
            <ta e="T75" id="Seg_1163" s="T74">adv</ta>
            <ta e="T76" id="Seg_1164" s="T75">v-v:tense.[v:pred.pn]</ta>
            <ta e="T77" id="Seg_1165" s="T76">n-n:case</ta>
            <ta e="T78" id="Seg_1166" s="T77">n-n:(poss).[n:case]</ta>
            <ta e="T79" id="Seg_1167" s="T78">adv</ta>
            <ta e="T80" id="Seg_1168" s="T79">ptcl</ta>
            <ta e="T81" id="Seg_1169" s="T80">dempro</ta>
            <ta e="T82" id="Seg_1170" s="T81">v-v:tense.[v:pred.pn]</ta>
            <ta e="T83" id="Seg_1171" s="T82">n-n:(poss).[n:case]</ta>
            <ta e="T84" id="Seg_1172" s="T83">propr.[n:case]</ta>
            <ta e="T85" id="Seg_1173" s="T84">que.[pro:case]</ta>
            <ta e="T86" id="Seg_1174" s="T85">propr-n:case</ta>
            <ta e="T87" id="Seg_1175" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_1176" s="T87">v-v:tense-v:poss.pn</ta>
            <ta e="T89" id="Seg_1177" s="T88">adv</ta>
            <ta e="T90" id="Seg_1178" s="T89">n.[n:case]</ta>
            <ta e="T91" id="Seg_1179" s="T90">v-v:tense-v:poss.pn</ta>
            <ta e="T92" id="Seg_1180" s="T91">n-n:(poss).[n:case]</ta>
            <ta e="T93" id="Seg_1181" s="T92">propr.[n:case]</ta>
            <ta e="T94" id="Seg_1182" s="T93">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T95" id="Seg_1183" s="T94">n-n:(poss).[n:case]</ta>
            <ta e="T96" id="Seg_1184" s="T95">propr.[n:case]</ta>
            <ta e="T97" id="Seg_1185" s="T96">propr.[n:case]</ta>
            <ta e="T98" id="Seg_1186" s="T97">que-pro:case</ta>
            <ta e="T99" id="Seg_1187" s="T98">ptcl</ta>
            <ta e="T100" id="Seg_1188" s="T99">v-v:(neg).[v:pred.pn]</ta>
            <ta e="T101" id="Seg_1189" s="T100">propr-n:case</ta>
            <ta e="T102" id="Seg_1190" s="T101">propr.[n:case]</ta>
            <ta e="T103" id="Seg_1191" s="T102">que-pro:case</ta>
            <ta e="T104" id="Seg_1192" s="T103">v-v:tense.[v:pred.pn]</ta>
            <ta e="T105" id="Seg_1193" s="T104">n</ta>
            <ta e="T106" id="Seg_1194" s="T105">n-n:case</ta>
            <ta e="T107" id="Seg_1195" s="T106">v-v:tense.[v:pred.pn]</ta>
            <ta e="T108" id="Seg_1196" s="T107">n.[n:case]</ta>
            <ta e="T109" id="Seg_1197" s="T108">n-n:poss-n:case</ta>
            <ta e="T110" id="Seg_1198" s="T109">v-v:cvb</ta>
            <ta e="T111" id="Seg_1199" s="T110">v-v:mood.[v:pred.pn]</ta>
            <ta e="T112" id="Seg_1200" s="T111">propr.[n:case]</ta>
            <ta e="T113" id="Seg_1201" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_1202" s="T113">v-v:mood.[v:pred.pn]</ta>
            <ta e="T115" id="Seg_1203" s="T114">propr.[n:case]</ta>
            <ta e="T116" id="Seg_1204" s="T115">v-v:mood.[v:pred.pn]</ta>
         </annotation>
         <annotation name="ps" tierref="ps-KuSE">
            <ta e="T10" id="Seg_1205" s="T9">pers</ta>
            <ta e="T11" id="Seg_1206" s="T10">n</ta>
            <ta e="T12" id="Seg_1207" s="T11">cardnum</ta>
            <ta e="T13" id="Seg_1208" s="T12">n</ta>
            <ta e="T14" id="Seg_1209" s="T13">pers</ta>
            <ta e="T15" id="Seg_1210" s="T14">que</ta>
            <ta e="T16" id="Seg_1211" s="T15">cop</ta>
            <ta e="T32" id="Seg_1212" s="T31">cardnum</ta>
            <ta e="T33" id="Seg_1213" s="T32">n</ta>
            <ta e="T34" id="Seg_1214" s="T33">v</ta>
            <ta e="T35" id="Seg_1215" s="T34">n</ta>
            <ta e="T37" id="Seg_1216" s="T36">dempro</ta>
            <ta e="T38" id="Seg_1217" s="T37">adv</ta>
            <ta e="T39" id="Seg_1218" s="T38">v</ta>
            <ta e="T40" id="Seg_1219" s="T39">n</ta>
            <ta e="T41" id="Seg_1220" s="T40">dempro</ta>
            <ta e="T42" id="Seg_1221" s="T41">adv</ta>
            <ta e="T43" id="Seg_1222" s="T42">pers</ta>
            <ta e="T45" id="Seg_1223" s="T44">v</ta>
            <ta e="T46" id="Seg_1224" s="T45">aux</ta>
            <ta e="T47" id="Seg_1225" s="T46">adv</ta>
            <ta e="T48" id="Seg_1226" s="T47">dempro</ta>
            <ta e="T49" id="Seg_1227" s="T48">ptcl</ta>
            <ta e="T50" id="Seg_1228" s="T49">que</ta>
            <ta e="T51" id="Seg_1229" s="T50">cardnum</ta>
            <ta e="T52" id="Seg_1230" s="T51">propr</ta>
            <ta e="T53" id="Seg_1231" s="T52">ptcl</ta>
            <ta e="T54" id="Seg_1232" s="T53">propr</ta>
            <ta e="T55" id="Seg_1233" s="T54">cardnum</ta>
            <ta e="T56" id="Seg_1234" s="T55">propr</ta>
            <ta e="T57" id="Seg_1235" s="T56">ptcl</ta>
            <ta e="T58" id="Seg_1236" s="T57">cardnum</ta>
            <ta e="T60" id="Seg_1237" s="T59">propr</ta>
            <ta e="T61" id="Seg_1238" s="T60">n</ta>
            <ta e="T62" id="Seg_1239" s="T61">v</ta>
            <ta e="T63" id="Seg_1240" s="T62">cardnum</ta>
            <ta e="T64" id="Seg_1241" s="T63">cardnum</ta>
            <ta e="T65" id="Seg_1242" s="T64">cardnum</ta>
            <ta e="T66" id="Seg_1243" s="T65">n</ta>
            <ta e="T67" id="Seg_1244" s="T66">propr</ta>
            <ta e="T68" id="Seg_1245" s="T67">ptcl</ta>
            <ta e="T69" id="Seg_1246" s="T68">adj</ta>
            <ta e="T70" id="Seg_1247" s="T69">cardnum</ta>
            <ta e="T71" id="Seg_1248" s="T70">adj</ta>
            <ta e="T72" id="Seg_1249" s="T71">n</ta>
            <ta e="T73" id="Seg_1250" s="T72">v</ta>
            <ta e="T74" id="Seg_1251" s="T73">aux</ta>
            <ta e="T75" id="Seg_1252" s="T74">adv</ta>
            <ta e="T76" id="Seg_1253" s="T75">v</ta>
            <ta e="T77" id="Seg_1254" s="T76">n</ta>
            <ta e="T78" id="Seg_1255" s="T77">n</ta>
            <ta e="T79" id="Seg_1256" s="T78">adv</ta>
            <ta e="T80" id="Seg_1257" s="T79">ptcl</ta>
            <ta e="T81" id="Seg_1258" s="T80">dempro</ta>
            <ta e="T82" id="Seg_1259" s="T81">v</ta>
            <ta e="T83" id="Seg_1260" s="T82">n</ta>
            <ta e="T84" id="Seg_1261" s="T83">propr</ta>
            <ta e="T85" id="Seg_1262" s="T84">que</ta>
            <ta e="T86" id="Seg_1263" s="T85">propr</ta>
            <ta e="T87" id="Seg_1264" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_1265" s="T87">cop</ta>
            <ta e="T89" id="Seg_1266" s="T88">adv</ta>
            <ta e="T90" id="Seg_1267" s="T89">n</ta>
            <ta e="T91" id="Seg_1268" s="T90">v</ta>
            <ta e="T92" id="Seg_1269" s="T91">n</ta>
            <ta e="T93" id="Seg_1270" s="T92">propr</ta>
            <ta e="T94" id="Seg_1271" s="T93">emphpro</ta>
            <ta e="T95" id="Seg_1272" s="T94">n</ta>
            <ta e="T96" id="Seg_1273" s="T95">propr</ta>
            <ta e="T97" id="Seg_1274" s="T96">propr</ta>
            <ta e="T98" id="Seg_1275" s="T97">que</ta>
            <ta e="T99" id="Seg_1276" s="T98">ptcl</ta>
            <ta e="T100" id="Seg_1277" s="T99">v</ta>
            <ta e="T101" id="Seg_1278" s="T100">propr</ta>
            <ta e="T102" id="Seg_1279" s="T101">propr</ta>
            <ta e="T103" id="Seg_1280" s="T102">que</ta>
            <ta e="T104" id="Seg_1281" s="T103">v</ta>
            <ta e="T105" id="Seg_1282" s="T104">n</ta>
            <ta e="T106" id="Seg_1283" s="T105">n</ta>
            <ta e="T107" id="Seg_1284" s="T106">v</ta>
            <ta e="T108" id="Seg_1285" s="T107">n</ta>
            <ta e="T109" id="Seg_1286" s="T108">n</ta>
            <ta e="T110" id="Seg_1287" s="T109">v</ta>
            <ta e="T111" id="Seg_1288" s="T110">v</ta>
            <ta e="T112" id="Seg_1289" s="T111">propr</ta>
            <ta e="T113" id="Seg_1290" s="T112">ptcl</ta>
            <ta e="T114" id="Seg_1291" s="T113">v</ta>
            <ta e="T115" id="Seg_1292" s="T114">propr</ta>
            <ta e="T116" id="Seg_1293" s="T115">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-KuSE" />
         <annotation name="SyF" tierref="SyF-KuSE" />
         <annotation name="IST" tierref="IST-KuSE" />
         <annotation name="Top" tierref="Top-KuSE" />
         <annotation name="Foc" tierref="Foc-KuSE" />
         <annotation name="BOR" tierref="BOR-KuSE">
            <ta e="T11" id="Seg_1294" s="T10">RUS:core</ta>
            <ta e="T40" id="Seg_1295" s="T39">RUS:cult</ta>
            <ta e="T42" id="Seg_1296" s="T41">RUS:mod</ta>
            <ta e="T54" id="Seg_1297" s="T53">YAK:cult</ta>
            <ta e="T56" id="Seg_1298" s="T55">RUS:cult</ta>
            <ta e="T60" id="Seg_1299" s="T59">RUS:cult</ta>
            <ta e="T67" id="Seg_1300" s="T66">RUS:cult</ta>
            <ta e="T77" id="Seg_1301" s="T76">RUS:cult</ta>
            <ta e="T84" id="Seg_1302" s="T83">RUS:cult</ta>
            <ta e="T86" id="Seg_1303" s="T85">RUS:cult</ta>
            <ta e="T96" id="Seg_1304" s="T95">RUS:cult</ta>
            <ta e="T97" id="Seg_1305" s="T96">RUS:cult</ta>
            <ta e="T101" id="Seg_1306" s="T100">YAK:cult</ta>
            <ta e="T102" id="Seg_1307" s="T101">RUS:cult</ta>
            <ta e="T105" id="Seg_1308" s="T104">RUS:cult</ta>
            <ta e="T106" id="Seg_1309" s="T105">RUS:cult</ta>
            <ta e="T115" id="Seg_1310" s="T114">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-KuSE" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-KuSE" />
         <annotation name="CS" tierref="CS-KuSE" />
         <annotation name="fe" tierref="fe-KuSE">
            <ta e="T6" id="Seg_1311" s="T1">We are seven children in our family.</ta>
            <ta e="T13" id="Seg_1312" s="T9">We are seven people in our family.</ta>
            <ta e="T16" id="Seg_1313" s="T13">We are, how many was it?</ta>
            <ta e="T20" id="Seg_1314" s="T16">Three girls, four boys.</ta>
            <ta e="T27" id="Seg_1315" s="T20">Vasya, Sveta, Yulya, Vanya, Senya and I.</ta>
            <ta e="T29" id="Seg_1316" s="T27">One adopted.</ta>
            <ta e="T40" id="Seg_1317" s="T31">One child we brought up (we adopted), his name is Misha, we returned him to his mother.</ta>
            <ta e="T47" id="Seg_1318" s="T40">He only came to us to be fed.</ta>
            <ta e="T50" id="Seg_1319" s="T47">And then what?</ta>
            <ta e="T62" id="Seg_1320" s="T50">One is in Anabar, in Urung Khaya, one is in Volochanka, one is in Syndassko he lives in the tundra.</ta>
            <ta e="T77" id="Seg_1321" s="T62">One sister is in Syndassko, she has children, she has two children, they travelled in the tundra, now she works in the kindergarden.</ta>
            <ta e="T83" id="Seg_1322" s="T77">Her child is here, her child sits here.</ta>
            <ta e="T96" id="Seg_1323" s="T83">Vassa was in Tura, there she gave birth to a child, his name is Egorka, her own name is Vassa.</ta>
            <ta e="T111" id="Seg_1324" s="T96">Vassa does not work, in Urung Khaya Vanya works as a reindeer herder, she will run a race at the day of the reindeer herders.</ta>
            <ta e="T116" id="Seg_1325" s="T111">Sveta also runs a race, Senya runs a race.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KuSE">
            <ta e="T6" id="Seg_1326" s="T1">Wir sind sieben Kinder bei uns in der Familie.</ta>
            <ta e="T13" id="Seg_1327" s="T9">Wir sind sieben Menschen in unserer Familie.</ta>
            <ta e="T16" id="Seg_1328" s="T13">Wie viele waren wir?</ta>
            <ta e="T20" id="Seg_1329" s="T16">Drei Mädchen, vier Jungen.</ta>
            <ta e="T27" id="Seg_1330" s="T20">Vasja, Sveta, Julia, Vanja, Senja und ich.</ta>
            <ta e="T29" id="Seg_1331" s="T27">Ein adoptiertes.</ta>
            <ta e="T40" id="Seg_1332" s="T31">Ein Kind haben wir großgezogen, sein Name ist Mischa, wir haben ihn seiner Mama zurückgegeben.</ta>
            <ta e="T47" id="Seg_1333" s="T40">Der kam nur zu uns, um beköstigt zu werden, früher.</ta>
            <ta e="T50" id="Seg_1334" s="T47">Und was dann?</ta>
            <ta e="T62" id="Seg_1335" s="T50">Einer ist in Anabar in Ürüng Xaja, einer ist in Volochanka, einer fährt in Syndassko in der Tundra.</ta>
            <ta e="T77" id="Seg_1336" s="T62">Eine meiner Schwestern ist in Syndassko, sie hat Kinder, sie hat zwei Kinder, sie sind in der Tundra gefahren, jetzt arbeiten sie im Kindergarten.</ta>
            <ta e="T83" id="Seg_1337" s="T77">Ihr Kind ist hier, es sitzt hier, ihr Kind.</ta>
            <ta e="T96" id="Seg_1338" s="T83">Vasja war in Tura, dort gebar sie ein Kind, sein Name ist Egorka, ihr eigener Name ist Vasja.</ta>
            <ta e="T111" id="Seg_1339" s="T96">Vasja arbeitet nirgendwo, in Ürüng Xaja arbeitet Vanja als Dings, als Rentierheirte, er wird am Rentier(hirten)tag zum Wettrennen kommen.</ta>
            <ta e="T116" id="Seg_1340" s="T111">Sveta läuft auch ein Rennen, Senja läuft ein Rennen.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KuSE" />
         <annotation name="ltr" tierref="ltr-KuSE">
            <ta e="T6" id="Seg_1341" s="T1">Нас в семье семеро детей.</ta>
            <ta e="T13" id="Seg_1342" s="T9">Нас в семье семеро человек.</ta>
            <ta e="T16" id="Seg_1343" s="T13">Нас сколько было то.</ta>
            <ta e="T20" id="Seg_1344" s="T16">Три девочки четыре мальчика.</ta>
            <ta e="T29" id="Seg_1345" s="T27">Один приемный…</ta>
            <ta e="T40" id="Seg_1346" s="T31">Одного ркбенка приютили зовут Миша, его обратно отдали маме.</ta>
            <ta e="T47" id="Seg_1347" s="T40">Он только к нам питаться ходил, раньше.</ta>
            <ta e="T50" id="Seg_1348" s="T47">Ещё что.</ta>
            <ta e="T62" id="Seg_1349" s="T50">Один в Анабаре есть в Юрюнг-Хае, одинв Волочанке есть, один в Сындасско в тундру ездит.</ta>
            <ta e="T77" id="Seg_1350" s="T62">Одна сестра в Сындасско есть с ребенком двое детей, в тундру ездили сейчас работает в детском садике.</ta>
            <ta e="T83" id="Seg_1351" s="T77">Ребенок здесь находится, вот сидит ребенок.</ta>
            <ta e="T96" id="Seg_1352" s="T83">Васса эта в Туре была, там ребенка родила, зовут Егорка, её зовут Васса.</ta>
            <ta e="T111" id="Seg_1353" s="T96">Васса нигде не работает, в Юрюнг-Хае Ваня там работает оленевод оленеводом работает на день оленевода на соревнования преизжает. </ta>
            <ta e="T116" id="Seg_1354" s="T111">Света тоже соревнуется, Сеня соревнуется.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KuSE" />
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-SE"
                      id="tx-SE"
                      speaker="SE"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-SE">
            <ts e="T8" id="Seg_1355" n="sc" s="T6">
               <ts e="T8" id="Seg_1357" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_1359" n="HIAT:w" s="T6">Hakalɨː</ts>
                  <nts id="Seg_1360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_1362" n="HIAT:w" s="T7">nado</ts>
                  <nts id="Seg_1363" n="HIAT:ip">.</nts>
                  <nts id="Seg_1364" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T31" id="Seg_1365" n="sc" s="T29">
               <ts e="T31" id="Seg_1367" n="HIAT:u" s="T29">
                  <ts e="T30" id="Seg_1369" n="HIAT:w" s="T29">Hakalɨː</ts>
                  <nts id="Seg_1370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_1372" n="HIAT:w" s="T30">hakalɨː</ts>
                  <nts id="Seg_1373" n="HIAT:ip">.</nts>
                  <nts id="Seg_1374" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-SE">
            <ts e="T8" id="Seg_1375" n="sc" s="T6">
               <ts e="T7" id="Seg_1377" n="e" s="T6">Hakalɨː </ts>
               <ts e="T8" id="Seg_1379" n="e" s="T7">nado. </ts>
            </ts>
            <ts e="T31" id="Seg_1380" n="sc" s="T29">
               <ts e="T30" id="Seg_1382" n="e" s="T29">Hakalɨː </ts>
               <ts e="T31" id="Seg_1384" n="e" s="T30">hakalɨː. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-SE">
            <ta e="T8" id="Seg_1385" s="T6">KuSE_2009_Family_nar.ES.001 (002)</ta>
            <ta e="T31" id="Seg_1386" s="T29">KuSE_2009_Family_nar.ES.002 (008)</ta>
         </annotation>
         <annotation name="st" tierref="st-SE">
            <ta e="T8" id="Seg_1387" s="T6">Һакалыы надо.</ta>
            <ta e="T31" id="Seg_1388" s="T29">Һакалыы һакалыы.</ta>
         </annotation>
         <annotation name="ts" tierref="ts-SE">
            <ta e="T8" id="Seg_1389" s="T6">Hakalɨː nado. </ta>
            <ta e="T31" id="Seg_1390" s="T29">Hakalɨː hakalɨː. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-SE">
            <ta e="T7" id="Seg_1391" s="T6">haka-lɨː</ta>
            <ta e="T30" id="Seg_1392" s="T29">haka-lɨː</ta>
            <ta e="T31" id="Seg_1393" s="T30">haka-lɨː</ta>
         </annotation>
         <annotation name="mp" tierref="mp-SE">
            <ta e="T7" id="Seg_1394" s="T6">haka-LIː</ta>
            <ta e="T30" id="Seg_1395" s="T29">haka-LIː</ta>
            <ta e="T31" id="Seg_1396" s="T30">haka-LIː</ta>
         </annotation>
         <annotation name="ge" tierref="ge-SE">
            <ta e="T7" id="Seg_1397" s="T6">Dolgan-SIM</ta>
            <ta e="T30" id="Seg_1398" s="T29">Dolgan-SIM</ta>
            <ta e="T31" id="Seg_1399" s="T30">Dolgan-SIM</ta>
         </annotation>
         <annotation name="gg" tierref="gg-SE">
            <ta e="T7" id="Seg_1400" s="T6">dolganisch-SIM</ta>
            <ta e="T30" id="Seg_1401" s="T29">dolganisch-SIM</ta>
            <ta e="T31" id="Seg_1402" s="T30">dolganisch-SIM</ta>
         </annotation>
         <annotation name="gr" tierref="gr-SE">
            <ta e="T7" id="Seg_1403" s="T6">долганский-SIM</ta>
            <ta e="T30" id="Seg_1404" s="T29">долганский-SIM</ta>
            <ta e="T31" id="Seg_1405" s="T30">долганский-SIM</ta>
         </annotation>
         <annotation name="mc" tierref="mc-SE">
            <ta e="T7" id="Seg_1406" s="T6">adj-adj&gt;adv</ta>
            <ta e="T30" id="Seg_1407" s="T29">adj-adj&gt;adv</ta>
            <ta e="T31" id="Seg_1408" s="T30">adj-adj&gt;adv</ta>
         </annotation>
         <annotation name="ps" tierref="ps-SE">
            <ta e="T7" id="Seg_1409" s="T6">adv</ta>
            <ta e="T30" id="Seg_1410" s="T29">adv</ta>
            <ta e="T31" id="Seg_1411" s="T30">adv</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-SE" />
         <annotation name="SyF" tierref="SyF-SE" />
         <annotation name="IST" tierref="IST-SE" />
         <annotation name="Top" tierref="Top-SE" />
         <annotation name="Foc" tierref="Foc-SE" />
         <annotation name="BOR" tierref="BOR-SE" />
         <annotation name="BOR-Phon" tierref="BOR-Phon-SE" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-SE" />
         <annotation name="CS" tierref="CS-SE" />
         <annotation name="fe" tierref="fe-SE">
            <ta e="T8" id="Seg_1412" s="T6">You need to speak in Dolgan.</ta>
            <ta e="T31" id="Seg_1413" s="T29">In Dolgan, in Dolgan!</ta>
         </annotation>
         <annotation name="fg" tierref="fg-SE">
            <ta e="T8" id="Seg_1414" s="T6">[Du] musst auf Dolganisch.</ta>
            <ta e="T31" id="Seg_1415" s="T29">Auf Dolganisch, auf Dolganisch.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-SE" />
         <annotation name="ltr" tierref="ltr-SE">
            <ta e="T8" id="Seg_1416" s="T6">По долгански надо.</ta>
            <ta e="T31" id="Seg_1417" s="T29">По долгански по долгански.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-SE" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-KuSE"
                          name="ref"
                          segmented-tier-id="tx-KuSE"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-KuSE"
                          name="st"
                          segmented-tier-id="tx-KuSE"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KuSE"
                          name="ts"
                          segmented-tier-id="tx-KuSE"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KuSE"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KuSE"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-KuSE"
                          name="mb"
                          segmented-tier-id="tx-KuSE"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-KuSE"
                          name="mp"
                          segmented-tier-id="tx-KuSE"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-KuSE"
                          name="ge"
                          segmented-tier-id="tx-KuSE"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-KuSE"
                          name="gg"
                          segmented-tier-id="tx-KuSE"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-KuSE"
                          name="gr"
                          segmented-tier-id="tx-KuSE"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-KuSE"
                          name="mc"
                          segmented-tier-id="tx-KuSE"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-KuSE"
                          name="ps"
                          segmented-tier-id="tx-KuSE"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-KuSE"
                          name="SeR"
                          segmented-tier-id="tx-KuSE"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-KuSE"
                          name="SyF"
                          segmented-tier-id="tx-KuSE"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-KuSE"
                          name="IST"
                          segmented-tier-id="tx-KuSE"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-KuSE"
                          name="Top"
                          segmented-tier-id="tx-KuSE"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-KuSE"
                          name="Foc"
                          segmented-tier-id="tx-KuSE"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-KuSE"
                          name="BOR"
                          segmented-tier-id="tx-KuSE"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-KuSE"
                          name="BOR-Phon"
                          segmented-tier-id="tx-KuSE"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-KuSE"
                          name="BOR-Morph"
                          segmented-tier-id="tx-KuSE"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-KuSE"
                          name="CS"
                          segmented-tier-id="tx-KuSE"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KuSE"
                          name="fe"
                          segmented-tier-id="tx-KuSE"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KuSE"
                          name="fg"
                          segmented-tier-id="tx-KuSE"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KuSE"
                          name="fr"
                          segmented-tier-id="tx-KuSE"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-KuSE"
                          name="ltr"
                          segmented-tier-id="tx-KuSE"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KuSE"
                          name="nt"
                          segmented-tier-id="tx-KuSE"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-SE"
                          name="ref"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-SE"
                          name="st"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-SE"
                          name="ts"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-SE"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-SE"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-SE"
                          name="mb"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-SE"
                          name="mp"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-SE"
                          name="ge"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-SE"
                          name="gg"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-SE"
                          name="gr"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-SE"
                          name="mc"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-SE"
                          name="ps"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-SE"
                          name="SeR"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-SE"
                          name="SyF"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-SE"
                          name="IST"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-SE"
                          name="Top"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-SE"
                          name="Foc"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-SE"
                          name="BOR"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-SE"
                          name="BOR-Phon"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-SE"
                          name="BOR-Morph"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-SE"
                          name="CS"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-SE"
                          name="fe"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-SE"
                          name="fg"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-SE"
                          name="fr"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-SE"
                          name="ltr"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-SE"
                          name="nt"
                          segmented-tier-id="tx-SE"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
