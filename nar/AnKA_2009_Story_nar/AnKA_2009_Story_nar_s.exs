<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID14289F35-9143-CAE3-5143-67E26912445B">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>AnKA_2009_Story_nar</transcription-name>
         <referenced-file url="AnKA_2009_Story_nar.wav" />
         <referenced-file url="AnKA_2009_Story_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\AnKA_2009_Story_nar\AnKA_2009_Story_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">81</ud-information>
            <ud-information attribute-name="# HIAT:w">60</ud-information>
            <ud-information attribute-name="# e">61</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">1</ud-information>
            <ud-information attribute-name="# HIAT:u">9</ud-information>
            <ud-information attribute-name="# sc">8</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="AnKA">
            <abbreviation>AnKA</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.0" type="appl" />
         <tli id="T2" time="0.4984" type="appl" />
         <tli id="T3" time="0.9968" type="appl" />
         <tli id="T4" time="1.4952" type="appl" />
         <tli id="T5" time="1.9936" type="appl" />
         <tli id="T6" time="2.492" type="appl" />
         <tli id="T7" time="3.8339999999999996" type="appl" />
         <tli id="T8" time="5.176" type="appl" />
         <tli id="T9" time="6.518" type="appl" />
         <tli id="T10" time="7.859999999999999" type="appl" />
         <tli id="T11" time="9.201999999999998" type="appl" />
         <tli id="T12" time="10.544" type="appl" />
         <tli id="T13" time="11.886" type="appl" />
         <tli id="T14" time="12.74275" type="appl" />
         <tli id="T15" time="13.599499999999999" type="appl" />
         <tli id="T16" time="14.45625" type="appl" />
         <tli id="T17" time="15.313" type="appl" />
         <tli id="T18" time="16.368" type="appl" />
         <tli id="T19" time="17.39111111111111" type="appl" />
         <tli id="T20" time="18.41422222222222" type="appl" />
         <tli id="T21" time="19.43733333333333" type="appl" />
         <tli id="T22" time="20.460444444444445" type="appl" />
         <tli id="T23" time="21.483555555555554" type="appl" />
         <tli id="T24" time="22.506666666666668" type="appl" />
         <tli id="T25" time="23.529777777777777" type="appl" />
         <tli id="T26" time="24.552888888888887" type="appl" />
         <tli id="T0" time="25.351982836174244" />
         <tli id="T64" time="25.474000000000004" type="intp" />
         <tli id="T63" time="25.525000000000002" type="intp" />
         <tli id="T27" time="25.576" type="appl" />
         <tli id="T28" time="26.936" type="appl" />
         <tli id="T29" time="27.97111111111111" type="appl" />
         <tli id="T30" time="29.006222222222224" type="appl" />
         <tli id="T31" time="30.041333333333334" type="appl" />
         <tli id="T32" time="31.076444444444444" type="appl" />
         <tli id="T33" time="32.111555555555555" type="appl" />
         <tli id="T34" time="33.14666666666667" type="appl" />
         <tli id="T35" time="34.18177777777778" type="appl" />
         <tli id="T36" time="35.21688888888889" type="appl" />
         <tli id="T37" time="36.252" type="appl" />
         <tli id="T38" time="37.3736" type="appl" />
         <tli id="T39" time="38.495200000000004" type="appl" />
         <tli id="T40" time="39.6168" type="appl" />
         <tli id="T41" time="40.7384" type="appl" />
         <tli id="T42" time="41.86" type="appl" />
         <tli id="T43" time="43.17628571428571" type="appl" />
         <tli id="T44" time="44.49257142857143" type="appl" />
         <tli id="T45" time="45.80885714285714" type="appl" />
         <tli id="T46" time="47.125142857142855" type="appl" />
         <tli id="T47" time="48.44142857142857" type="appl" />
         <tli id="T48" time="49.757714285714286" type="appl" />
         <tli id="T49" time="51.074" type="appl" />
         <tli id="T50" time="56.776" type="appl" />
         <tli id="T51" time="57.582727272727276" type="appl" />
         <tli id="T52" time="58.38945454545455" type="appl" />
         <tli id="T53" time="59.19618181818182" type="appl" />
         <tli id="T54" time="60.00290909090909" type="appl" />
         <tli id="T55" time="60.80963636363637" type="appl" />
         <tli id="T56" time="61.616363636363644" type="appl" />
         <tli id="T57" time="62.42309090909092" type="appl" />
         <tli id="T58" time="63.22981818181819" type="appl" />
         <tli id="T59" time="64.03654545454546" type="appl" />
         <tli id="T60" time="64.84327272727273" type="appl" />
         <tli id="T61" time="65.65" type="appl" />
         <tli id="T62" time="66.876" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="AnKA"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T17" id="Seg_0" n="sc" s="T1">
               <ts e="T6" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Biːr</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6" n="HIAT:ip">(</nts>
                  <ts e="T3" id="Seg_8" n="HIAT:w" s="T2">ha-</ts>
                  <nts id="Seg_9" n="HIAT:ip">)</nts>
                  <nts id="Seg_10" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_12" n="HIAT:w" s="T3">e</ts>
                  <nts id="Seg_13" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_15" n="HIAT:w" s="T4">biːr</ts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_18" n="HIAT:w" s="T5">hajɨn</ts>
                  <nts id="Seg_19" n="HIAT:ip">.</nts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T13" id="Seg_22" n="HIAT:u" s="T6">
                  <ts e="T7" id="Seg_24" n="HIAT:w" s="T6">Bejebit</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_27" n="HIAT:w" s="T7">balokka</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_30" n="HIAT:w" s="T8">hɨldʼaːččɨbɨt</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_33" n="HIAT:w" s="T9">každɨj</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_36" n="HIAT:w" s="T10">denʼ</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_39" n="HIAT:w" s="T11">otto</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_42" n="HIAT:w" s="T12">ke</ts>
                  <nts id="Seg_43" n="HIAT:ip">.</nts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_46" n="HIAT:u" s="T13">
                  <ts e="T14" id="Seg_48" n="HIAT:w" s="T13">Elbek</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_51" n="HIAT:w" s="T14">bagajɨ</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_54" n="HIAT:w" s="T15">ogo</ts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_57" n="HIAT:w" s="T16">bu͡olaːččɨbɨt</ts>
                  <nts id="Seg_58" n="HIAT:ip">.</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T27" id="Seg_60" n="sc" s="T18">
               <ts e="T27" id="Seg_62" n="HIAT:u" s="T18">
                  <ts e="T19" id="Seg_64" n="HIAT:w" s="T18">Onton</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_67" n="HIAT:w" s="T19">klassnɨj</ts>
                  <nts id="Seg_68" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_70" n="HIAT:w" s="T20">bagajɨ</ts>
                  <nts id="Seg_71" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_73" n="HIAT:w" s="T21">bu͡olaːččɨ</ts>
                  <nts id="Seg_74" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_76" n="HIAT:w" s="T22">dʼi͡e</ts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_79" n="HIAT:w" s="T23">stroittammɨt</ts>
                  <nts id="Seg_80" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_82" n="HIAT:w" s="T24">etibit</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_85" n="HIAT:w" s="T25">bu͡o</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T0" id="Seg_88" n="HIAT:w" s="T26">mahɨnan</ts>
                  <nts id="Seg_89" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_90" n="HIAT:ip">(</nts>
                  <nts id="Seg_91" n="HIAT:ip">(</nts>
                  <ats e="T64" id="Seg_92" n="HIAT:non-pho" s="T0">LAUGH</ats>
                  <nts id="Seg_93" n="HIAT:ip">)</nts>
                  <nts id="Seg_94" n="HIAT:ip">)</nts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_97" n="HIAT:w" s="T64">onton</ts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_100" n="HIAT:w" s="T63">ke</ts>
                  <nts id="Seg_101" n="HIAT:ip">.</nts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T49" id="Seg_103" n="sc" s="T28">
               <ts e="T37" id="Seg_105" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_107" n="HIAT:w" s="T28">O</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_110" n="HIAT:w" s="T29">ol</ts>
                  <nts id="Seg_111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_113" n="HIAT:w" s="T30">dʼi͡e</ts>
                  <nts id="Seg_114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_116" n="HIAT:w" s="T31">maspɨtɨn</ts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_119" n="HIAT:w" s="T32">ubatan</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_122" n="HIAT:w" s="T33">keːspippit</ts>
                  <nts id="Seg_123" n="HIAT:ip">,</nts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_126" n="HIAT:w" s="T34">ubatan</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_129" n="HIAT:w" s="T35">keːspittere</ts>
                  <nts id="Seg_130" n="HIAT:ip">,</nts>
                  <nts id="Seg_131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_133" n="HIAT:w" s="T36">onton</ts>
                  <nts id="Seg_134" n="HIAT:ip">.</nts>
                  <nts id="Seg_135" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T42" id="Seg_137" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_139" n="HIAT:w" s="T37">Onton</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_142" n="HIAT:w" s="T38">ke</ts>
                  <nts id="Seg_143" n="HIAT:ip">,</nts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_146" n="HIAT:w" s="T39">rɨbaktartan</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_149" n="HIAT:w" s="T40">balɨk</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_152" n="HIAT:w" s="T41">kördönöːččübüt</ts>
                  <nts id="Seg_153" n="HIAT:ip">.</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T49" id="Seg_156" n="HIAT:u" s="T42">
                  <ts e="T43" id="Seg_158" n="HIAT:w" s="T42">Rɨbaktartan</ts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_161" n="HIAT:w" s="T43">balɨk</ts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_164" n="HIAT:w" s="T44">kördönöːččübüt</ts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_167" n="HIAT:w" s="T45">šašlɨk</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_170" n="HIAT:w" s="T46">oŋostoːččubut</ts>
                  <nts id="Seg_171" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_173" n="HIAT:w" s="T47">kastjordanan</ts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_176" n="HIAT:w" s="T48">baran</ts>
                  <nts id="Seg_177" n="HIAT:ip">.</nts>
                  <nts id="Seg_178" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T62" id="Seg_179" n="sc" s="T50">
               <ts e="T61" id="Seg_181" n="HIAT:u" s="T50">
                  <ts e="T51" id="Seg_183" n="HIAT:w" s="T50">Hɨldʼaːččɨbɨt</ts>
                  <nts id="Seg_184" n="HIAT:ip">,</nts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_187" n="HIAT:w" s="T51">palatka</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_190" n="HIAT:w" s="T52">egelsteččibit</ts>
                  <nts id="Seg_191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_193" n="HIAT:w" s="T53">iti</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_196" n="HIAT:w" s="T54">Diana</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_199" n="HIAT:w" s="T55">palatka</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_202" n="HIAT:w" s="T56">egelsteːčči</ts>
                  <nts id="Seg_203" n="HIAT:ip">,</nts>
                  <nts id="Seg_204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_206" n="HIAT:w" s="T57">onno</ts>
                  <nts id="Seg_207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_209" n="HIAT:w" s="T58">onnʼoːččubut</ts>
                  <nts id="Seg_210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_212" n="HIAT:w" s="T59">palatka</ts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_215" n="HIAT:w" s="T60">ihiger</ts>
                  <nts id="Seg_216" n="HIAT:ip">.</nts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T62" id="Seg_219" n="HIAT:u" s="T61">
                  <ts e="T62" id="Seg_221" n="HIAT:w" s="T61">Elete</ts>
                  <nts id="Seg_222" n="HIAT:ip">.</nts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T17" id="Seg_224" n="sc" s="T1">
               <ts e="T2" id="Seg_226" n="e" s="T1">Biːr </ts>
               <ts e="T3" id="Seg_228" n="e" s="T2">(ha-) </ts>
               <ts e="T4" id="Seg_230" n="e" s="T3">e </ts>
               <ts e="T5" id="Seg_232" n="e" s="T4">biːr </ts>
               <ts e="T6" id="Seg_234" n="e" s="T5">hajɨn. </ts>
               <ts e="T7" id="Seg_236" n="e" s="T6">Bejebit </ts>
               <ts e="T8" id="Seg_238" n="e" s="T7">balokka </ts>
               <ts e="T9" id="Seg_240" n="e" s="T8">hɨldʼaːččɨbɨt </ts>
               <ts e="T10" id="Seg_242" n="e" s="T9">každɨj </ts>
               <ts e="T11" id="Seg_244" n="e" s="T10">denʼ </ts>
               <ts e="T12" id="Seg_246" n="e" s="T11">otto </ts>
               <ts e="T13" id="Seg_248" n="e" s="T12">ke. </ts>
               <ts e="T14" id="Seg_250" n="e" s="T13">Elbek </ts>
               <ts e="T15" id="Seg_252" n="e" s="T14">bagajɨ </ts>
               <ts e="T16" id="Seg_254" n="e" s="T15">ogo </ts>
               <ts e="T17" id="Seg_256" n="e" s="T16">bu͡olaːččɨbɨt. </ts>
            </ts>
            <ts e="T27" id="Seg_257" n="sc" s="T18">
               <ts e="T19" id="Seg_259" n="e" s="T18">Onton </ts>
               <ts e="T20" id="Seg_261" n="e" s="T19">klassnɨj </ts>
               <ts e="T21" id="Seg_263" n="e" s="T20">bagajɨ </ts>
               <ts e="T22" id="Seg_265" n="e" s="T21">bu͡olaːččɨ </ts>
               <ts e="T23" id="Seg_267" n="e" s="T22">dʼi͡e </ts>
               <ts e="T24" id="Seg_269" n="e" s="T23">stroittammɨt </ts>
               <ts e="T25" id="Seg_271" n="e" s="T24">etibit </ts>
               <ts e="T26" id="Seg_273" n="e" s="T25">bu͡o </ts>
               <ts e="T0" id="Seg_275" n="e" s="T26">mahɨnan </ts>
               <ts e="T64" id="Seg_277" n="e" s="T0">((LAUGH)) </ts>
               <ts e="T63" id="Seg_279" n="e" s="T64">onton </ts>
               <ts e="T27" id="Seg_281" n="e" s="T63">ke. </ts>
            </ts>
            <ts e="T49" id="Seg_282" n="sc" s="T28">
               <ts e="T29" id="Seg_284" n="e" s="T28">O </ts>
               <ts e="T30" id="Seg_286" n="e" s="T29">ol </ts>
               <ts e="T31" id="Seg_288" n="e" s="T30">dʼi͡e </ts>
               <ts e="T32" id="Seg_290" n="e" s="T31">maspɨtɨn </ts>
               <ts e="T33" id="Seg_292" n="e" s="T32">ubatan </ts>
               <ts e="T34" id="Seg_294" n="e" s="T33">keːspippit, </ts>
               <ts e="T35" id="Seg_296" n="e" s="T34">ubatan </ts>
               <ts e="T36" id="Seg_298" n="e" s="T35">keːspittere, </ts>
               <ts e="T37" id="Seg_300" n="e" s="T36">onton. </ts>
               <ts e="T38" id="Seg_302" n="e" s="T37">Onton </ts>
               <ts e="T39" id="Seg_304" n="e" s="T38">ke, </ts>
               <ts e="T40" id="Seg_306" n="e" s="T39">rɨbaktartan </ts>
               <ts e="T41" id="Seg_308" n="e" s="T40">balɨk </ts>
               <ts e="T42" id="Seg_310" n="e" s="T41">kördönöːččübüt. </ts>
               <ts e="T43" id="Seg_312" n="e" s="T42">Rɨbaktartan </ts>
               <ts e="T44" id="Seg_314" n="e" s="T43">balɨk </ts>
               <ts e="T45" id="Seg_316" n="e" s="T44">kördönöːččübüt </ts>
               <ts e="T46" id="Seg_318" n="e" s="T45">šašlɨk </ts>
               <ts e="T47" id="Seg_320" n="e" s="T46">oŋostoːččubut </ts>
               <ts e="T48" id="Seg_322" n="e" s="T47">kastjordanan </ts>
               <ts e="T49" id="Seg_324" n="e" s="T48">baran. </ts>
            </ts>
            <ts e="T62" id="Seg_325" n="sc" s="T50">
               <ts e="T51" id="Seg_327" n="e" s="T50">Hɨldʼaːččɨbɨt, </ts>
               <ts e="T52" id="Seg_329" n="e" s="T51">palatka </ts>
               <ts e="T53" id="Seg_331" n="e" s="T52">egelsteččibit </ts>
               <ts e="T54" id="Seg_333" n="e" s="T53">iti </ts>
               <ts e="T55" id="Seg_335" n="e" s="T54">Diana </ts>
               <ts e="T56" id="Seg_337" n="e" s="T55">palatka </ts>
               <ts e="T57" id="Seg_339" n="e" s="T56">egelsteːčči, </ts>
               <ts e="T58" id="Seg_341" n="e" s="T57">onno </ts>
               <ts e="T59" id="Seg_343" n="e" s="T58">onnʼoːččubut </ts>
               <ts e="T60" id="Seg_345" n="e" s="T59">palatka </ts>
               <ts e="T61" id="Seg_347" n="e" s="T60">ihiger. </ts>
               <ts e="T62" id="Seg_349" n="e" s="T61">Elete. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T6" id="Seg_350" s="T1">AnKA_2009_Story_nar.001 (001)</ta>
            <ta e="T13" id="Seg_351" s="T6">AnKA_2009_Story_nar.002 (002)</ta>
            <ta e="T17" id="Seg_352" s="T13">AnKA_2009_Story_nar.003 (003)</ta>
            <ta e="T27" id="Seg_353" s="T18">AnKA_2009_Story_nar.004 (005)</ta>
            <ta e="T37" id="Seg_354" s="T28">AnKA_2009_Story_nar.005 (007)</ta>
            <ta e="T42" id="Seg_355" s="T37">AnKA_2009_Story_nar.006 (008)</ta>
            <ta e="T49" id="Seg_356" s="T42">AnKA_2009_Story_nar.007 (009)</ta>
            <ta e="T61" id="Seg_357" s="T50">AnKA_2009_Story_nar.008 (011)</ta>
            <ta e="T62" id="Seg_358" s="T61">AnKA_2009_Story_nar.009 (012)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T6" id="Seg_359" s="T1">биир һа.. э биир һайын</ta>
            <ta e="T13" id="Seg_360" s="T6">бэйэбит балокка һылдьаччибыт каждый день отто кэ</ta>
            <ta e="T17" id="Seg_361" s="T13">элбэк багайы ого буолаччибыт </ta>
            <ta e="T27" id="Seg_362" s="T18">онтон классный багайы буолаччи дьиэ строиттаммыт этибит буо маһынан</ta>
            <ta e="T37" id="Seg_363" s="T28">о ол дьиэ маспытын убатан кээспиппит, убатан кээспиттэрэ, онтон</ta>
            <ta e="T42" id="Seg_364" s="T37">онтон кэ, рыбактартан балык көрдөнөөчүбүт</ta>
            <ta e="T49" id="Seg_365" s="T42">рыбактартан балык көрдөнөөчүбүт шашлык оӈостооччубут кастёрданан баран</ta>
            <ta e="T61" id="Seg_366" s="T50">һылдьааччибыт, палатка эгэлстэччибит ити Диана палатка эгэлстээччи, онно онньооччубут палатка иһигэр</ta>
            <ta e="T62" id="Seg_367" s="T61">элэтэ</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T6" id="Seg_368" s="T1">Biːr (ha-) e biːr hajɨn. </ta>
            <ta e="T13" id="Seg_369" s="T6">((LAUGH)) Bejebit balokka hɨldʼaːččɨbɨt každɨj denʼ otto ke. </ta>
            <ta e="T17" id="Seg_370" s="T13">Elbek bagajɨ ogo bu͡olaːččɨbɨt. </ta>
            <ta e="T27" id="Seg_371" s="T18">Onton klassnɨj bagajɨ bu͡olaːččɨ dʼi͡e stroittammɨt etibit bu͡o mahɨnan ((LAUGH)) onton ke.</ta>
            <ta e="T37" id="Seg_372" s="T28">O ol dʼi͡e maspɨtɨn ubatan keːspippit, ubatan keːspittere, onton. </ta>
            <ta e="T42" id="Seg_373" s="T37">Onton ke, rɨbaktartan balɨk kördönöːččübüt. </ta>
            <ta e="T49" id="Seg_374" s="T42">Rɨbaktartan balɨk kördönöːččübüt šašlɨk oŋostoːččubut kastjordanan baran. </ta>
            <ta e="T61" id="Seg_375" s="T50">Hɨldʼaːččɨbɨt, palatka egelsteččibit iti Diana palatka egelsteːčči, onno onnʼoːččubut palatka ihiger. </ta>
            <ta e="T62" id="Seg_376" s="T61">Elete. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_377" s="T1">biːr</ta>
            <ta e="T4" id="Seg_378" s="T3">e</ta>
            <ta e="T5" id="Seg_379" s="T4">biːr</ta>
            <ta e="T6" id="Seg_380" s="T5">hajɨn</ta>
            <ta e="T7" id="Seg_381" s="T6">beje-bit</ta>
            <ta e="T8" id="Seg_382" s="T7">balok-ka</ta>
            <ta e="T9" id="Seg_383" s="T8">hɨldʼ-aːččɨ-bɨt</ta>
            <ta e="T12" id="Seg_384" s="T11">otto</ta>
            <ta e="T13" id="Seg_385" s="T12">ke</ta>
            <ta e="T14" id="Seg_386" s="T13">elbek</ta>
            <ta e="T15" id="Seg_387" s="T14">bagajɨ</ta>
            <ta e="T16" id="Seg_388" s="T15">ogo</ta>
            <ta e="T17" id="Seg_389" s="T16">bu͡ol-aːččɨ-bɨt</ta>
            <ta e="T19" id="Seg_390" s="T18">onton</ta>
            <ta e="T21" id="Seg_391" s="T20">bagajɨ</ta>
            <ta e="T22" id="Seg_392" s="T21">bu͡ol-aːččɨ</ta>
            <ta e="T23" id="Seg_393" s="T22">dʼi͡e</ta>
            <ta e="T24" id="Seg_394" s="T23">stroit-tam-mɨt</ta>
            <ta e="T25" id="Seg_395" s="T24">e-ti-bit</ta>
            <ta e="T26" id="Seg_396" s="T25">bu͡o</ta>
            <ta e="T0" id="Seg_397" s="T26">mah-ɨ-nan</ta>
            <ta e="T63" id="Seg_398" s="T64">onton</ta>
            <ta e="T27" id="Seg_399" s="T63">ke</ta>
            <ta e="T29" id="Seg_400" s="T28">o</ta>
            <ta e="T30" id="Seg_401" s="T29">ol</ta>
            <ta e="T31" id="Seg_402" s="T30">dʼi͡e</ta>
            <ta e="T32" id="Seg_403" s="T31">mas-pɨtɨ-n</ta>
            <ta e="T33" id="Seg_404" s="T32">ubat-an</ta>
            <ta e="T34" id="Seg_405" s="T33">keːs-pip-pit</ta>
            <ta e="T35" id="Seg_406" s="T34">ubat-an</ta>
            <ta e="T36" id="Seg_407" s="T35">keːs-pit-tere</ta>
            <ta e="T37" id="Seg_408" s="T36">on-ton</ta>
            <ta e="T38" id="Seg_409" s="T37">onton</ta>
            <ta e="T39" id="Seg_410" s="T38">ke</ta>
            <ta e="T40" id="Seg_411" s="T39">rɨbak-tar-tan</ta>
            <ta e="T41" id="Seg_412" s="T40">balɨk</ta>
            <ta e="T42" id="Seg_413" s="T41">körd-ö-n-öːččü-büt</ta>
            <ta e="T43" id="Seg_414" s="T42">rɨbak-tar-tan</ta>
            <ta e="T44" id="Seg_415" s="T43">balɨk</ta>
            <ta e="T45" id="Seg_416" s="T44">körd-ö-n-öːččü-büt</ta>
            <ta e="T47" id="Seg_417" s="T46">oŋost-oːčču-but</ta>
            <ta e="T48" id="Seg_418" s="T47">kastjor-dan-an</ta>
            <ta e="T49" id="Seg_419" s="T48">baran</ta>
            <ta e="T51" id="Seg_420" s="T50">hɨldʼ-aːččɨ-bɨt</ta>
            <ta e="T53" id="Seg_421" s="T52">egel-s-t-ečči-bit</ta>
            <ta e="T54" id="Seg_422" s="T53">iti</ta>
            <ta e="T55" id="Seg_423" s="T54">Diana</ta>
            <ta e="T57" id="Seg_424" s="T56">egel-s-t-eːčči</ta>
            <ta e="T58" id="Seg_425" s="T57">onno</ta>
            <ta e="T59" id="Seg_426" s="T58">onnʼoː-čču-but</ta>
            <ta e="T61" id="Seg_427" s="T60">ih-i-ger</ta>
            <ta e="T62" id="Seg_428" s="T61">ele-te</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_429" s="T1">biːr</ta>
            <ta e="T4" id="Seg_430" s="T3">e</ta>
            <ta e="T5" id="Seg_431" s="T4">biːr</ta>
            <ta e="T6" id="Seg_432" s="T5">hajɨn</ta>
            <ta e="T7" id="Seg_433" s="T6">beje-BIt</ta>
            <ta e="T8" id="Seg_434" s="T7">balok-GA</ta>
            <ta e="T9" id="Seg_435" s="T8">hɨrɨt-AːččI-BIt</ta>
            <ta e="T12" id="Seg_436" s="T11">otto</ta>
            <ta e="T13" id="Seg_437" s="T12">ka</ta>
            <ta e="T14" id="Seg_438" s="T13">elbek</ta>
            <ta e="T15" id="Seg_439" s="T14">bagajɨ</ta>
            <ta e="T16" id="Seg_440" s="T15">ogo</ta>
            <ta e="T17" id="Seg_441" s="T16">bu͡ol-AːččI-BIt</ta>
            <ta e="T19" id="Seg_442" s="T18">onton</ta>
            <ta e="T21" id="Seg_443" s="T20">bagajɨ</ta>
            <ta e="T22" id="Seg_444" s="T21">bu͡ol-AːččI</ta>
            <ta e="T23" id="Seg_445" s="T22">dʼi͡e</ta>
            <ta e="T24" id="Seg_446" s="T23">stroit-LAN-BIT</ta>
            <ta e="T25" id="Seg_447" s="T24">e-TI-BIt</ta>
            <ta e="T26" id="Seg_448" s="T25">bu͡o</ta>
            <ta e="T0" id="Seg_449" s="T26">mas-I-nAn</ta>
            <ta e="T63" id="Seg_450" s="T64">onton</ta>
            <ta e="T27" id="Seg_451" s="T63">ka</ta>
            <ta e="T29" id="Seg_452" s="T28">o</ta>
            <ta e="T30" id="Seg_453" s="T29">ol</ta>
            <ta e="T31" id="Seg_454" s="T30">dʼi͡e</ta>
            <ta e="T32" id="Seg_455" s="T31">mas-BItI-n</ta>
            <ta e="T33" id="Seg_456" s="T32">ubat-An</ta>
            <ta e="T34" id="Seg_457" s="T33">keːs-BIT-BIt</ta>
            <ta e="T35" id="Seg_458" s="T34">ubat-An</ta>
            <ta e="T36" id="Seg_459" s="T35">keːs-BIT-LArA</ta>
            <ta e="T37" id="Seg_460" s="T36">ol-ttAn</ta>
            <ta e="T38" id="Seg_461" s="T37">onton</ta>
            <ta e="T39" id="Seg_462" s="T38">ka</ta>
            <ta e="T40" id="Seg_463" s="T39">rɨbak-LAr-ttAn</ta>
            <ta e="T41" id="Seg_464" s="T40">balɨk</ta>
            <ta e="T42" id="Seg_465" s="T41">kördöː-A-n-AːččI-BIt</ta>
            <ta e="T43" id="Seg_466" s="T42">rɨbak-LAr-ttAn</ta>
            <ta e="T44" id="Seg_467" s="T43">balɨk</ta>
            <ta e="T45" id="Seg_468" s="T44">kördöː-A-n-AːččI-BIt</ta>
            <ta e="T47" id="Seg_469" s="T46">oŋohun-AːččI-BIt</ta>
            <ta e="T48" id="Seg_470" s="T47">kastjor-LAN-An</ta>
            <ta e="T49" id="Seg_471" s="T48">baran</ta>
            <ta e="T51" id="Seg_472" s="T50">hɨrɨt-AːččI-BIt</ta>
            <ta e="T53" id="Seg_473" s="T52">egel-s-t-AːččI-BIt</ta>
            <ta e="T54" id="Seg_474" s="T53">iti</ta>
            <ta e="T55" id="Seg_475" s="T54">Diana</ta>
            <ta e="T57" id="Seg_476" s="T56">egel-s-t-AːččI</ta>
            <ta e="T58" id="Seg_477" s="T57">onno</ta>
            <ta e="T59" id="Seg_478" s="T58">oːnnʼoː-AːččI-BIt</ta>
            <ta e="T61" id="Seg_479" s="T60">is-tI-GAr</ta>
            <ta e="T62" id="Seg_480" s="T61">ele-tA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_481" s="T1">one</ta>
            <ta e="T4" id="Seg_482" s="T3">eh</ta>
            <ta e="T5" id="Seg_483" s="T4">one</ta>
            <ta e="T6" id="Seg_484" s="T5">summer.[NOM]</ta>
            <ta e="T7" id="Seg_485" s="T6">self-1PL.[NOM]</ta>
            <ta e="T8" id="Seg_486" s="T7">balok-DAT/LOC</ta>
            <ta e="T9" id="Seg_487" s="T8">go-HAB-1PL</ta>
            <ta e="T12" id="Seg_488" s="T11">EMPH</ta>
            <ta e="T13" id="Seg_489" s="T12">well</ta>
            <ta e="T14" id="Seg_490" s="T13">many</ta>
            <ta e="T15" id="Seg_491" s="T14">very</ta>
            <ta e="T16" id="Seg_492" s="T15">child.[NOM]</ta>
            <ta e="T17" id="Seg_493" s="T16">be-HAB-1PL</ta>
            <ta e="T19" id="Seg_494" s="T18">then</ta>
            <ta e="T21" id="Seg_495" s="T20">very</ta>
            <ta e="T22" id="Seg_496" s="T21">be-HAB.[3SG]</ta>
            <ta e="T23" id="Seg_497" s="T22">house.[NOM]</ta>
            <ta e="T24" id="Seg_498" s="T23">build-VBZ-PTCP.PST</ta>
            <ta e="T25" id="Seg_499" s="T24">be-PST1-1PL</ta>
            <ta e="T26" id="Seg_500" s="T25">EMPH</ta>
            <ta e="T0" id="Seg_501" s="T26">wood-EP-INSTR</ta>
            <ta e="T63" id="Seg_502" s="T64">then</ta>
            <ta e="T27" id="Seg_503" s="T63">well</ta>
            <ta e="T29" id="Seg_504" s="T28">oh</ta>
            <ta e="T30" id="Seg_505" s="T29">that</ta>
            <ta e="T31" id="Seg_506" s="T30">house.[NOM]</ta>
            <ta e="T32" id="Seg_507" s="T31">wood-1PL-ACC</ta>
            <ta e="T33" id="Seg_508" s="T32">light-CVB.SEQ</ta>
            <ta e="T34" id="Seg_509" s="T33">throw-PST2-1PL</ta>
            <ta e="T35" id="Seg_510" s="T34">light-CVB.SEQ</ta>
            <ta e="T36" id="Seg_511" s="T35">throw-PST2-3PL</ta>
            <ta e="T37" id="Seg_512" s="T36">that-ABL</ta>
            <ta e="T38" id="Seg_513" s="T37">then</ta>
            <ta e="T39" id="Seg_514" s="T38">well</ta>
            <ta e="T40" id="Seg_515" s="T39">fisherman-PL-ABL</ta>
            <ta e="T41" id="Seg_516" s="T40">fish.[NOM]</ta>
            <ta e="T42" id="Seg_517" s="T41">beg-EP-MED-HAB-1PL</ta>
            <ta e="T43" id="Seg_518" s="T42">fisherman-PL-ABL</ta>
            <ta e="T44" id="Seg_519" s="T43">fish.[NOM]</ta>
            <ta e="T45" id="Seg_520" s="T44">beg-EP-MED-HAB-1PL</ta>
            <ta e="T47" id="Seg_521" s="T46">make-HAB-1PL</ta>
            <ta e="T48" id="Seg_522" s="T47">wood.fire-VBZ-CVB.SEQ</ta>
            <ta e="T49" id="Seg_523" s="T48">after</ta>
            <ta e="T51" id="Seg_524" s="T50">go-HAB-1PL</ta>
            <ta e="T53" id="Seg_525" s="T52">bring-RECP/COLL-CAUS-HAB-1PL</ta>
            <ta e="T54" id="Seg_526" s="T53">that.[NOM]</ta>
            <ta e="T55" id="Seg_527" s="T54">Diana.[NOM]</ta>
            <ta e="T57" id="Seg_528" s="T56">bring-RECP/COLL-CAUS-HAB.[3SG]</ta>
            <ta e="T58" id="Seg_529" s="T57">there</ta>
            <ta e="T59" id="Seg_530" s="T58">play-HAB-1PL</ta>
            <ta e="T61" id="Seg_531" s="T60">inside-3SG-DAT/LOC</ta>
            <ta e="T62" id="Seg_532" s="T61">last-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T2" id="Seg_533" s="T1">eins</ta>
            <ta e="T4" id="Seg_534" s="T3">äh</ta>
            <ta e="T5" id="Seg_535" s="T4">eins</ta>
            <ta e="T6" id="Seg_536" s="T5">Sommer.[NOM]</ta>
            <ta e="T7" id="Seg_537" s="T6">selbst-1PL.[NOM]</ta>
            <ta e="T8" id="Seg_538" s="T7">Balok-DAT/LOC</ta>
            <ta e="T9" id="Seg_539" s="T8">gehen-HAB-1PL</ta>
            <ta e="T12" id="Seg_540" s="T11">EMPH</ta>
            <ta e="T13" id="Seg_541" s="T12">nun</ta>
            <ta e="T14" id="Seg_542" s="T13">viel</ta>
            <ta e="T15" id="Seg_543" s="T14">sehr</ta>
            <ta e="T16" id="Seg_544" s="T15">Kind.[NOM]</ta>
            <ta e="T17" id="Seg_545" s="T16">sein-HAB-1PL</ta>
            <ta e="T19" id="Seg_546" s="T18">dann</ta>
            <ta e="T21" id="Seg_547" s="T20">sehr</ta>
            <ta e="T22" id="Seg_548" s="T21">sein-HAB.[3SG]</ta>
            <ta e="T23" id="Seg_549" s="T22">Haus.[NOM]</ta>
            <ta e="T24" id="Seg_550" s="T23">bauen-VBZ-PTCP.PST</ta>
            <ta e="T25" id="Seg_551" s="T24">sein-PST1-1PL</ta>
            <ta e="T26" id="Seg_552" s="T25">EMPH</ta>
            <ta e="T0" id="Seg_553" s="T26">Holz-EP-INSTR</ta>
            <ta e="T63" id="Seg_554" s="T64">dann</ta>
            <ta e="T27" id="Seg_555" s="T63">nun</ta>
            <ta e="T29" id="Seg_556" s="T28">oh</ta>
            <ta e="T30" id="Seg_557" s="T29">jenes</ta>
            <ta e="T31" id="Seg_558" s="T30">Haus.[NOM]</ta>
            <ta e="T32" id="Seg_559" s="T31">Holz-1PL-ACC</ta>
            <ta e="T33" id="Seg_560" s="T32">anzünden-CVB.SEQ</ta>
            <ta e="T34" id="Seg_561" s="T33">werfen-PST2-1PL</ta>
            <ta e="T35" id="Seg_562" s="T34">anzünden-CVB.SEQ</ta>
            <ta e="T36" id="Seg_563" s="T35">werfen-PST2-3PL</ta>
            <ta e="T37" id="Seg_564" s="T36">jenes-ABL</ta>
            <ta e="T38" id="Seg_565" s="T37">dann</ta>
            <ta e="T39" id="Seg_566" s="T38">nun</ta>
            <ta e="T40" id="Seg_567" s="T39">Fischer-PL-ABL</ta>
            <ta e="T41" id="Seg_568" s="T40">Fisch.[NOM]</ta>
            <ta e="T42" id="Seg_569" s="T41">bitten-EP-MED-HAB-1PL</ta>
            <ta e="T43" id="Seg_570" s="T42">Fischer-PL-ABL</ta>
            <ta e="T44" id="Seg_571" s="T43">Fisch.[NOM]</ta>
            <ta e="T45" id="Seg_572" s="T44">bitten-EP-MED-HAB-1PL</ta>
            <ta e="T47" id="Seg_573" s="T46">machen-HAB-1PL</ta>
            <ta e="T48" id="Seg_574" s="T47">Holzfeuer-VBZ-CVB.SEQ</ta>
            <ta e="T49" id="Seg_575" s="T48">nachdem</ta>
            <ta e="T51" id="Seg_576" s="T50">gehen-HAB-1PL</ta>
            <ta e="T53" id="Seg_577" s="T52">bringen-RECP/COLL-CAUS-HAB-1PL</ta>
            <ta e="T54" id="Seg_578" s="T53">dieses.[NOM]</ta>
            <ta e="T55" id="Seg_579" s="T54">Diana.[NOM]</ta>
            <ta e="T57" id="Seg_580" s="T56">bringen-RECP/COLL-CAUS-HAB.[3SG]</ta>
            <ta e="T58" id="Seg_581" s="T57">dort</ta>
            <ta e="T59" id="Seg_582" s="T58">spielen-HAB-1PL</ta>
            <ta e="T61" id="Seg_583" s="T60">Inneres-3SG-DAT/LOC</ta>
            <ta e="T62" id="Seg_584" s="T61">letzter-3SG.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_585" s="T1">один</ta>
            <ta e="T4" id="Seg_586" s="T3">э</ta>
            <ta e="T5" id="Seg_587" s="T4">один</ta>
            <ta e="T6" id="Seg_588" s="T5">лето.[NOM]</ta>
            <ta e="T7" id="Seg_589" s="T6">сам-1PL.[NOM]</ta>
            <ta e="T8" id="Seg_590" s="T7">балок-DAT/LOC</ta>
            <ta e="T9" id="Seg_591" s="T8">идти-HAB-1PL</ta>
            <ta e="T12" id="Seg_592" s="T11">EMPH</ta>
            <ta e="T13" id="Seg_593" s="T12">вот</ta>
            <ta e="T14" id="Seg_594" s="T13">много</ta>
            <ta e="T15" id="Seg_595" s="T14">очень</ta>
            <ta e="T16" id="Seg_596" s="T15">ребенок.[NOM]</ta>
            <ta e="T17" id="Seg_597" s="T16">быть-HAB-1PL</ta>
            <ta e="T19" id="Seg_598" s="T18">потом</ta>
            <ta e="T21" id="Seg_599" s="T20">очень</ta>
            <ta e="T22" id="Seg_600" s="T21">быть-HAB.[3SG]</ta>
            <ta e="T23" id="Seg_601" s="T22">дом.[NOM]</ta>
            <ta e="T24" id="Seg_602" s="T23">строить-VBZ-PTCP.PST</ta>
            <ta e="T25" id="Seg_603" s="T24">быть-PST1-1PL</ta>
            <ta e="T26" id="Seg_604" s="T25">EMPH</ta>
            <ta e="T0" id="Seg_605" s="T26">дерево-EP-INSTR</ta>
            <ta e="T63" id="Seg_606" s="T64">потом</ta>
            <ta e="T27" id="Seg_607" s="T63">вот</ta>
            <ta e="T29" id="Seg_608" s="T28">о</ta>
            <ta e="T30" id="Seg_609" s="T29">тот</ta>
            <ta e="T31" id="Seg_610" s="T30">дом.[NOM]</ta>
            <ta e="T32" id="Seg_611" s="T31">дерево-1PL-ACC</ta>
            <ta e="T33" id="Seg_612" s="T32">зажигать-CVB.SEQ</ta>
            <ta e="T34" id="Seg_613" s="T33">бросать-PST2-1PL</ta>
            <ta e="T35" id="Seg_614" s="T34">зажигать-CVB.SEQ</ta>
            <ta e="T36" id="Seg_615" s="T35">бросать-PST2-3PL</ta>
            <ta e="T37" id="Seg_616" s="T36">тот-ABL</ta>
            <ta e="T38" id="Seg_617" s="T37">потом</ta>
            <ta e="T39" id="Seg_618" s="T38">вот</ta>
            <ta e="T40" id="Seg_619" s="T39">рыбак-PL-ABL</ta>
            <ta e="T41" id="Seg_620" s="T40">рыба.[NOM]</ta>
            <ta e="T42" id="Seg_621" s="T41">попросить-EP-MED-HAB-1PL</ta>
            <ta e="T43" id="Seg_622" s="T42">рыбак-PL-ABL</ta>
            <ta e="T44" id="Seg_623" s="T43">рыба.[NOM]</ta>
            <ta e="T45" id="Seg_624" s="T44">попросить-EP-MED-HAB-1PL</ta>
            <ta e="T47" id="Seg_625" s="T46">делать-HAB-1PL</ta>
            <ta e="T48" id="Seg_626" s="T47">костёр-VBZ-CVB.SEQ</ta>
            <ta e="T49" id="Seg_627" s="T48">после</ta>
            <ta e="T51" id="Seg_628" s="T50">идти-HAB-1PL</ta>
            <ta e="T53" id="Seg_629" s="T52">принести-RECP/COLL-CAUS-HAB-1PL</ta>
            <ta e="T54" id="Seg_630" s="T53">тот.[NOM]</ta>
            <ta e="T55" id="Seg_631" s="T54">Диана.[NOM]</ta>
            <ta e="T57" id="Seg_632" s="T56">принести-RECP/COLL-CAUS-HAB.[3SG]</ta>
            <ta e="T58" id="Seg_633" s="T57">там</ta>
            <ta e="T59" id="Seg_634" s="T58">играть-HAB-1PL</ta>
            <ta e="T61" id="Seg_635" s="T60">нутро-3SG-DAT/LOC</ta>
            <ta e="T62" id="Seg_636" s="T61">последний-3SG.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_637" s="T1">cardnum</ta>
            <ta e="T4" id="Seg_638" s="T3">interj</ta>
            <ta e="T5" id="Seg_639" s="T4">cardnum</ta>
            <ta e="T6" id="Seg_640" s="T5">n.[n:case]</ta>
            <ta e="T7" id="Seg_641" s="T6">emphpro-pro:(poss).[pro:case]</ta>
            <ta e="T8" id="Seg_642" s="T7">n-n:case</ta>
            <ta e="T9" id="Seg_643" s="T8">v-v:mood-v:pred.pn</ta>
            <ta e="T12" id="Seg_644" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_645" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_646" s="T13">quant</ta>
            <ta e="T15" id="Seg_647" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_648" s="T15">n.[n:case]</ta>
            <ta e="T17" id="Seg_649" s="T16">v-v:mood-v:pred.pn</ta>
            <ta e="T19" id="Seg_650" s="T18">adv</ta>
            <ta e="T21" id="Seg_651" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_652" s="T21">v-v:mood.[v:pred.pn]</ta>
            <ta e="T23" id="Seg_653" s="T22">n.[n:case]</ta>
            <ta e="T24" id="Seg_654" s="T23">v-v&gt;v-v:ptcp</ta>
            <ta e="T25" id="Seg_655" s="T24">v-v:tense-v:poss.pn</ta>
            <ta e="T26" id="Seg_656" s="T25">ptcl</ta>
            <ta e="T0" id="Seg_657" s="T26">n-n:(ins)-n:case</ta>
            <ta e="T63" id="Seg_658" s="T64">adv</ta>
            <ta e="T27" id="Seg_659" s="T63">ptcl</ta>
            <ta e="T29" id="Seg_660" s="T28">interj</ta>
            <ta e="T30" id="Seg_661" s="T29">dempro</ta>
            <ta e="T31" id="Seg_662" s="T30">n.[n:case]</ta>
            <ta e="T32" id="Seg_663" s="T31">n-n:poss-n:case</ta>
            <ta e="T33" id="Seg_664" s="T32">v-v:cvb</ta>
            <ta e="T34" id="Seg_665" s="T33">v-v:tense-v:pred.pn</ta>
            <ta e="T35" id="Seg_666" s="T34">v-v:cvb</ta>
            <ta e="T36" id="Seg_667" s="T35">v-v:tense-v:poss.pn</ta>
            <ta e="T37" id="Seg_668" s="T36">dempro-pro:case</ta>
            <ta e="T38" id="Seg_669" s="T37">adv</ta>
            <ta e="T39" id="Seg_670" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_671" s="T39">n-n:(num)-n:case</ta>
            <ta e="T41" id="Seg_672" s="T40">n.[n:case]</ta>
            <ta e="T42" id="Seg_673" s="T41">v-v:(ins)-v&gt;v-v:mood-v:pred.pn</ta>
            <ta e="T43" id="Seg_674" s="T42">n-n:(num)-n:case</ta>
            <ta e="T44" id="Seg_675" s="T43">n.[n:case]</ta>
            <ta e="T45" id="Seg_676" s="T44">v-v:(ins)-v&gt;v-v:mood-v:pred.pn</ta>
            <ta e="T47" id="Seg_677" s="T46">v-v:mood-v:pred.pn</ta>
            <ta e="T48" id="Seg_678" s="T47">n-n&gt;v-v:cvb</ta>
            <ta e="T49" id="Seg_679" s="T48">post</ta>
            <ta e="T51" id="Seg_680" s="T50">v-v:mood-v:pred.pn</ta>
            <ta e="T53" id="Seg_681" s="T52">v-v&gt;v-v&gt;v-v:mood-v:pred.pn</ta>
            <ta e="T54" id="Seg_682" s="T53">dempro.[pro:case]</ta>
            <ta e="T55" id="Seg_683" s="T54">propr.[n:case]</ta>
            <ta e="T57" id="Seg_684" s="T56">v-v&gt;v-v&gt;v-v:mood.[v:pred.pn]</ta>
            <ta e="T58" id="Seg_685" s="T57">adv</ta>
            <ta e="T59" id="Seg_686" s="T58">v-v:mood-v:pred.pn</ta>
            <ta e="T61" id="Seg_687" s="T60">n-n:poss-n:case</ta>
            <ta e="T62" id="Seg_688" s="T61">adj-n:(poss).[n:case]</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_689" s="T1">cardnum</ta>
            <ta e="T4" id="Seg_690" s="T3">interj</ta>
            <ta e="T5" id="Seg_691" s="T4">cardnum</ta>
            <ta e="T6" id="Seg_692" s="T5">n</ta>
            <ta e="T7" id="Seg_693" s="T6">emphpro</ta>
            <ta e="T8" id="Seg_694" s="T7">n</ta>
            <ta e="T9" id="Seg_695" s="T8">v</ta>
            <ta e="T12" id="Seg_696" s="T11">ptcl</ta>
            <ta e="T13" id="Seg_697" s="T12">ptcl</ta>
            <ta e="T14" id="Seg_698" s="T13">quant</ta>
            <ta e="T15" id="Seg_699" s="T14">ptcl</ta>
            <ta e="T16" id="Seg_700" s="T15">n</ta>
            <ta e="T17" id="Seg_701" s="T16">cop</ta>
            <ta e="T19" id="Seg_702" s="T18">adv</ta>
            <ta e="T21" id="Seg_703" s="T20">ptcl</ta>
            <ta e="T22" id="Seg_704" s="T21">cop</ta>
            <ta e="T23" id="Seg_705" s="T22">n</ta>
            <ta e="T24" id="Seg_706" s="T23">v</ta>
            <ta e="T25" id="Seg_707" s="T24">aux</ta>
            <ta e="T26" id="Seg_708" s="T25">ptcl</ta>
            <ta e="T0" id="Seg_709" s="T26">n</ta>
            <ta e="T63" id="Seg_710" s="T64">adv</ta>
            <ta e="T27" id="Seg_711" s="T63">ptcl</ta>
            <ta e="T29" id="Seg_712" s="T28">interj</ta>
            <ta e="T30" id="Seg_713" s="T29">dempro</ta>
            <ta e="T31" id="Seg_714" s="T30">n</ta>
            <ta e="T32" id="Seg_715" s="T31">n</ta>
            <ta e="T33" id="Seg_716" s="T32">v</ta>
            <ta e="T34" id="Seg_717" s="T33">aux</ta>
            <ta e="T35" id="Seg_718" s="T34">v</ta>
            <ta e="T36" id="Seg_719" s="T35">aux</ta>
            <ta e="T37" id="Seg_720" s="T36">dempro</ta>
            <ta e="T38" id="Seg_721" s="T37">adv</ta>
            <ta e="T39" id="Seg_722" s="T38">ptcl</ta>
            <ta e="T40" id="Seg_723" s="T39">n</ta>
            <ta e="T41" id="Seg_724" s="T40">n</ta>
            <ta e="T42" id="Seg_725" s="T41">v</ta>
            <ta e="T43" id="Seg_726" s="T42">n</ta>
            <ta e="T44" id="Seg_727" s="T43">n</ta>
            <ta e="T45" id="Seg_728" s="T44">v</ta>
            <ta e="T47" id="Seg_729" s="T46">v</ta>
            <ta e="T48" id="Seg_730" s="T47">v</ta>
            <ta e="T49" id="Seg_731" s="T48">post</ta>
            <ta e="T51" id="Seg_732" s="T50">v</ta>
            <ta e="T53" id="Seg_733" s="T52">v</ta>
            <ta e="T54" id="Seg_734" s="T53">dempro</ta>
            <ta e="T55" id="Seg_735" s="T54">propr</ta>
            <ta e="T57" id="Seg_736" s="T56">v</ta>
            <ta e="T58" id="Seg_737" s="T57">adv</ta>
            <ta e="T59" id="Seg_738" s="T58">v</ta>
            <ta e="T61" id="Seg_739" s="T60">n</ta>
            <ta e="T62" id="Seg_740" s="T61">adj</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T7" id="Seg_741" s="T6">pro.h:A</ta>
            <ta e="T8" id="Seg_742" s="T7">np:L</ta>
            <ta e="T17" id="Seg_743" s="T16">0.1.h:Th</ta>
            <ta e="T23" id="Seg_744" s="T22">np:P</ta>
            <ta e="T25" id="Seg_745" s="T23">0.1.h:A</ta>
            <ta e="T32" id="Seg_746" s="T31">np:P</ta>
            <ta e="T34" id="Seg_747" s="T32">0.1.h:A</ta>
            <ta e="T36" id="Seg_748" s="T34">0.3.h:A</ta>
            <ta e="T40" id="Seg_749" s="T39">np.h:R</ta>
            <ta e="T41" id="Seg_750" s="T40">np:Th</ta>
            <ta e="T42" id="Seg_751" s="T41">0.1.h:A</ta>
            <ta e="T43" id="Seg_752" s="T42">np.h:R</ta>
            <ta e="T44" id="Seg_753" s="T43">np:Th</ta>
            <ta e="T45" id="Seg_754" s="T44">0.1.h:A</ta>
            <ta e="T47" id="Seg_755" s="T46">0.1.h:A</ta>
            <ta e="T51" id="Seg_756" s="T50">0.1.h:A</ta>
            <ta e="T53" id="Seg_757" s="T52">0.1.h:A</ta>
            <ta e="T55" id="Seg_758" s="T54">np.h:A</ta>
            <ta e="T58" id="Seg_759" s="T57">adv:L</ta>
            <ta e="T59" id="Seg_760" s="T58">0.1.h:A</ta>
            <ta e="T61" id="Seg_761" s="T60">np:L</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T7" id="Seg_762" s="T6">pro.h:S</ta>
            <ta e="T9" id="Seg_763" s="T8">v:pred</ta>
            <ta e="T16" id="Seg_764" s="T15">n:pred</ta>
            <ta e="T17" id="Seg_765" s="T16">0.1.h:S cop</ta>
            <ta e="T20" id="Seg_766" s="T19">adj:pred</ta>
            <ta e="T22" id="Seg_767" s="T21">cop</ta>
            <ta e="T23" id="Seg_768" s="T22">np:O</ta>
            <ta e="T25" id="Seg_769" s="T23">0.1.h:S v:pred</ta>
            <ta e="T32" id="Seg_770" s="T31">np:O</ta>
            <ta e="T34" id="Seg_771" s="T32">0.1.h:S v:pred</ta>
            <ta e="T36" id="Seg_772" s="T34">0.3.h:S v:pred</ta>
            <ta e="T41" id="Seg_773" s="T40">np:O</ta>
            <ta e="T42" id="Seg_774" s="T41">0.1.h:S v:pred</ta>
            <ta e="T44" id="Seg_775" s="T43">np:O</ta>
            <ta e="T45" id="Seg_776" s="T44">0.1.h:S v:pred</ta>
            <ta e="T47" id="Seg_777" s="T46">0.1.h:S v:pred</ta>
            <ta e="T49" id="Seg_778" s="T47">s:temp</ta>
            <ta e="T51" id="Seg_779" s="T50">0.1.h:S v:pred</ta>
            <ta e="T53" id="Seg_780" s="T52">0.1.h:S v:pred</ta>
            <ta e="T55" id="Seg_781" s="T54">np.h:S</ta>
            <ta e="T57" id="Seg_782" s="T56">v:pred</ta>
            <ta e="T59" id="Seg_783" s="T58">0.1.h:S v:pred</ta>
         </annotation>
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T8" id="Seg_784" s="T7">RUS:cult</ta>
            <ta e="T24" id="Seg_785" s="T23">RUS:core</ta>
            <ta e="T40" id="Seg_786" s="T39">RUS:core</ta>
            <ta e="T43" id="Seg_787" s="T42">RUS:core</ta>
            <ta e="T48" id="Seg_788" s="T47">RUS:cult</ta>
            <ta e="T55" id="Seg_789" s="T54">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T8" id="Seg_790" s="T7">dir:infl</ta>
            <ta e="T24" id="Seg_791" s="T23">indir:infl</ta>
            <ta e="T40" id="Seg_792" s="T39">dir:infl</ta>
            <ta e="T43" id="Seg_793" s="T42">dir:infl</ta>
            <ta e="T48" id="Seg_794" s="T47">indir:infl</ta>
            <ta e="T55" id="Seg_795" s="T54">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS">
            <ta e="T11" id="Seg_796" s="T9">RUS:int.ins</ta>
            <ta e="T20" id="Seg_797" s="T19">RUS:int.ins</ta>
            <ta e="T46" id="Seg_798" s="T45">RUS:int.ins</ta>
            <ta e="T52" id="Seg_799" s="T51">RUS:int.ins</ta>
            <ta e="T56" id="Seg_800" s="T55">RUS:int.ins</ta>
            <ta e="T60" id="Seg_801" s="T59">RUS:int.ins</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T6" id="Seg_802" s="T1">One summer…</ta>
            <ta e="T13" id="Seg_803" s="T6">We used to walk around near the balok every day.</ta>
            <ta e="T17" id="Seg_804" s="T13">We used to be many children.</ta>
            <ta e="T27" id="Seg_805" s="T18">Then it is really cool we built a house from wood, then.</ta>
            <ta e="T37" id="Seg_806" s="T28">That house, our wood we burnt, they burned then.</ta>
            <ta e="T42" id="Seg_807" s="T37">Then we ask fish from the fishermen.</ta>
            <ta e="T49" id="Seg_808" s="T42">We ask for fish from the fishermen we make shashlik after having made a wood fire.</ta>
            <ta e="T61" id="Seg_809" s="T50">We walk around, we bring a tent, well Diana brings a tent, there we play in the tent.</ta>
            <ta e="T62" id="Seg_810" s="T61">That's the end.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T6" id="Seg_811" s="T1">Ein, äh, ein Sommer…</ta>
            <ta e="T13" id="Seg_812" s="T6">Wir liefen jeden Tag um unseren Balok herum.</ta>
            <ta e="T17" id="Seg_813" s="T13">Wir sind sehr viele Kinder.</ta>
            <ta e="T27" id="Seg_814" s="T18">Dann ist es wirklich klasse, wir haben ein Haus aus Holz gebaut, dann. </ta>
            <ta e="T37" id="Seg_815" s="T28">Das Haus, unser Holz zündeten wir an, sie zündeten es an, dann.</ta>
            <ta e="T42" id="Seg_816" s="T37">Dann bitten wir die Fischer um Fisch.</ta>
            <ta e="T49" id="Seg_817" s="T42">Wir bitten die Fischer um Fischer, wir machen Schaschlik, nachdem wir ein Feuer gemacht haben.</ta>
            <ta e="T61" id="Seg_818" s="T50">Wir laufen herum, wir bringen ein Zelt, nun, Diana bringt ein Zelt, dort spielen wir im Zelt.</ta>
            <ta e="T62" id="Seg_819" s="T61">Das ist alles.</ta>
         </annotation>
         <annotation name="fr" tierref="fr" />
         <annotation name="ltr" tierref="ltr">
            <ta e="T6" id="Seg_820" s="T1">однажды летом</ta>
            <ta e="T13" id="Seg_821" s="T6">сами у болка гуляем каждый день потом</ta>
            <ta e="T17" id="Seg_822" s="T13">много очень детей бывает</ta>
            <ta e="T27" id="Seg_823" s="T18">ещё классно очень бывает дом строили когда то из дров</ta>
            <ta e="T37" id="Seg_824" s="T28">этот дом деревянный подожгли, подожгли, потом </ta>
            <ta e="T42" id="Seg_825" s="T37">ещё, у рыбаков рыбу просим</ta>
            <ta e="T49" id="Seg_826" s="T42">у рыбаков рыбу просим шашлык делаем на костре</ta>
            <ta e="T61" id="Seg_827" s="T50">гуляем, палатку приносим эта Диана палатку приносит, там играем в палатке внутри</ta>
            <ta e="T62" id="Seg_828" s="T61">всё</ta>
         </annotation>
         <annotation name="nt" tierref="nt" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T0" />
            <conversion-tli id="T64" />
            <conversion-tli id="T63" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
