<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID6E85EF90-4D40-8F99-A74F-0B3CAF6CEEF7">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>BeES_2010_HidePreparation_nar</transcription-name>
         <referenced-file url="BeES_2010_HidePreparation_nar.wav" />
         <referenced-file url="BeES_2010_HidePreparation_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\BeES_2010_HidePreparation_nar\BeES_2010_HidePreparation_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">991</ud-information>
            <ud-information attribute-name="# HIAT:w">793</ud-information>
            <ud-information attribute-name="# e">793</ud-information>
            <ud-information attribute-name="# HIAT:u">85</ud-information>
            <ud-information attribute-name="# sc">162</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="BeES">
            <abbreviation>BeES</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.0" type="appl" />
         <tli id="T2" time="0.6042857142857143" type="appl" />
         <tli id="T3" time="1.2085714285714286" type="appl" />
         <tli id="T4" time="1.812857142857143" type="appl" />
         <tli id="T5" time="2.4171428571428573" type="appl" />
         <tli id="T6" time="3.0214285714285714" type="appl" />
         <tli id="T7" time="3.625714285714286" type="appl" />
         <tli id="T8" time="4.23" type="appl" />
         <tli id="T9" time="4.8342857142857145" type="appl" />
         <tli id="T10" time="5.438571428571429" type="appl" />
         <tli id="T11" time="6.042857142857143" type="appl" />
         <tli id="T12" time="6.647142857142858" type="appl" />
         <tli id="T13" time="7.251428571428572" type="appl" />
         <tli id="T14" time="7.855714285714286" type="appl" />
         <tli id="T15" time="8.3" />
         <tli id="T16" time="8.76" />
         <tli id="T17" time="9.115" type="appl" />
         <tli id="T18" time="9.7" type="appl" />
         <tli id="T19" time="10.285" type="appl" />
         <tli id="T20" time="10.870000000000001" type="appl" />
         <tli id="T21" time="11.455" type="appl" />
         <tli id="T22" time="12.040000000000001" type="appl" />
         <tli id="T23" time="12.625" type="appl" />
         <tli id="T24" time="13.093333333333332" />
         <tli id="T25" time="13.63" type="appl" />
         <tli id="T26" time="14.391" type="appl" />
         <tli id="T27" time="15.152000000000001" type="appl" />
         <tli id="T28" time="15.913" type="appl" />
         <tli id="T29" time="16.674" type="appl" />
         <tli id="T30" time="17.435" type="appl" />
         <tli id="T31" time="18.195999999999998" type="appl" />
         <tli id="T32" time="18.957" type="appl" />
         <tli id="T33" time="19.718" type="appl" />
         <tli id="T34" time="20.479" type="appl" />
         <tli id="T35" time="21.10666666666667" />
         <tli id="T36" time="21.65" type="appl" />
         <tli id="T37" time="22.389999999999997" type="appl" />
         <tli id="T38" time="23.13" type="appl" />
         <tli id="T39" time="23.869999999999997" type="appl" />
         <tli id="T40" time="24.61" type="appl" />
         <tli id="T41" time="25.349999999999998" type="appl" />
         <tli id="T42" time="26.089999999999996" type="appl" />
         <tli id="T43" time="26.83" type="appl" />
         <tli id="T44" time="27.96" type="appl" />
         <tli id="T45" time="28.988333333333333" type="appl" />
         <tli id="T46" time="30.01666666666667" type="appl" />
         <tli id="T47" time="31.045" type="appl" />
         <tli id="T48" time="32.07333333333334" type="appl" />
         <tli id="T49" time="33.10166666666667" type="appl" />
         <tli id="T50" time="34.13666666666666" />
         <tli id="T51" time="35.18" />
         <tli id="T52" time="35.90833333333333" type="appl" />
         <tli id="T53" time="36.836666666666666" type="appl" />
         <tli id="T54" time="37.765" type="appl" />
         <tli id="T55" time="38.69333333333333" type="appl" />
         <tli id="T56" time="39.62166666666666" type="appl" />
         <tli id="T57" time="40.27666666666667" />
         <tli id="T58" time="41.196666666666665" />
         <tli id="T59" time="41.861111111111114" type="appl" />
         <tli id="T60" time="42.632222222222225" type="appl" />
         <tli id="T61" time="43.403333333333336" type="appl" />
         <tli id="T62" time="44.17444444444445" type="appl" />
         <tli id="T63" time="44.94555555555556" type="appl" />
         <tli id="T64" time="45.71666666666667" type="appl" />
         <tli id="T65" time="46.48777777777778" type="appl" />
         <tli id="T66" time="47.25888888888889" type="appl" />
         <tli id="T67" time="48.03" type="appl" />
         <tli id="T68" time="48.556666666666665" />
         <tli id="T69" time="49.46125" type="appl" />
         <tli id="T70" time="50.5325" type="appl" />
         <tli id="T71" time="51.60375" type="appl" />
         <tli id="T72" time="52.675" type="appl" />
         <tli id="T73" time="53.74625" type="appl" />
         <tli id="T74" time="54.8175" type="appl" />
         <tli id="T75" time="55.88875" type="appl" />
         <tli id="T76" time="56.96" type="appl" />
         <tli id="T77" time="57.586666666666666" />
         <tli id="T78" time="58.421875" type="appl" />
         <tli id="T79" time="59.34375" type="appl" />
         <tli id="T80" time="60.265625" type="appl" />
         <tli id="T81" time="61.1875" type="appl" />
         <tli id="T82" time="62.109375" type="appl" />
         <tli id="T83" time="63.03125" type="appl" />
         <tli id="T84" time="63.953125" type="appl" />
         <tli id="T85" time="64.875" type="appl" />
         <tli id="T86" time="65.796875" type="appl" />
         <tli id="T87" time="66.71875" type="appl" />
         <tli id="T88" time="67.640625" type="appl" />
         <tli id="T89" time="68.5625" type="appl" />
         <tli id="T90" time="69.484375" type="appl" />
         <tli id="T91" time="70.40625" type="appl" />
         <tli id="T92" time="71.328125" type="appl" />
         <tli id="T93" time="72.07666666666667" />
         <tli id="T94" time="72.52333333333334" />
         <tli id="T95" time="73.02875" type="appl" />
         <tli id="T96" time="73.6275" type="appl" />
         <tli id="T97" time="74.22625000000001" type="appl" />
         <tli id="T98" time="74.825" type="appl" />
         <tli id="T99" time="75.42375" type="appl" />
         <tli id="T100" time="76.02250000000001" type="appl" />
         <tli id="T101" time="76.62125" type="appl" />
         <tli id="T102" time="77.14666666666668" />
         <tli id="T103" time="77.5" type="appl" />
         <tli id="T104" time="78.365" type="appl" />
         <tli id="T105" time="79.22999999999999" type="appl" />
         <tli id="T106" time="80.095" type="appl" />
         <tli id="T107" time="80.35333333333332" />
         <tli id="T108" time="81.53" />
         <tli id="T109" time="82.22749999999999" type="appl" />
         <tli id="T110" time="82.82499999999999" type="appl" />
         <tli id="T111" time="83.4225" type="appl" />
         <tli id="T112" time="84.02" type="appl" />
         <tli id="T113" time="84.61749999999999" type="appl" />
         <tli id="T114" time="85.215" type="appl" />
         <tli id="T115" time="85.8125" type="appl" />
         <tli id="T116" time="86.41" type="appl" />
         <tli id="T117" time="86.81" type="appl" />
         <tli id="T118" time="87.54714285714286" type="appl" />
         <tli id="T119" time="88.28428571428572" type="appl" />
         <tli id="T120" time="89.02142857142857" type="appl" />
         <tli id="T121" time="89.75857142857143" type="appl" />
         <tli id="T122" time="90.49571428571429" type="appl" />
         <tli id="T123" time="91.23285714285714" type="appl" />
         <tli id="T124" time="91.76333333333334" />
         <tli id="T125" time="92.09333333333335" />
         <tli id="T126" time="92.70375" type="appl" />
         <tli id="T127" time="93.38749999999999" type="appl" />
         <tli id="T128" time="94.07124999999999" type="appl" />
         <tli id="T129" time="94.755" type="appl" />
         <tli id="T130" time="95.43875" type="appl" />
         <tli id="T131" time="96.1225" type="appl" />
         <tli id="T132" time="96.80624999999999" type="appl" />
         <tli id="T133" time="97.49" type="appl" />
         <tli id="T134" time="98.17375" type="appl" />
         <tli id="T135" time="98.85749999999999" type="appl" />
         <tli id="T136" time="99.54124999999999" type="appl" />
         <tli id="T137" time="100.225" type="appl" />
         <tli id="T138" time="100.90875" type="appl" />
         <tli id="T139" time="101.5925" type="appl" />
         <tli id="T140" time="102.27624999999999" type="appl" />
         <tli id="T141" time="102.79333333333332" />
         <tli id="T142" time="103.38233072916667" />
         <tli id="T143" time="103.8652380952381" type="appl" />
         <tli id="T144" time="104.5014761904762" type="appl" />
         <tli id="T145" time="105.13771428571428" type="appl" />
         <tli id="T146" time="105.77395238095238" type="appl" />
         <tli id="T147" time="106.41019047619048" type="appl" />
         <tli id="T148" time="107.04642857142858" type="appl" />
         <tli id="T149" time="107.68266666666666" type="appl" />
         <tli id="T150" time="108.31890476190476" type="appl" />
         <tli id="T151" time="108.95514285714286" type="appl" />
         <tli id="T152" time="109.59138095238096" type="appl" />
         <tli id="T153" time="110.22761904761904" type="appl" />
         <tli id="T154" time="110.86385714285714" type="appl" />
         <tli id="T155" time="111.50009523809524" type="appl" />
         <tli id="T156" time="112.13633333333334" type="appl" />
         <tli id="T157" time="112.77257142857144" type="appl" />
         <tli id="T158" time="113.40880952380952" type="appl" />
         <tli id="T159" time="114.04504761904762" type="appl" />
         <tli id="T160" time="114.68128571428572" type="appl" />
         <tli id="T161" time="115.3175238095238" type="appl" />
         <tli id="T162" time="115.9537619047619" type="appl" />
         <tli id="T163" time="116.38333333333333" />
         <tli id="T164" time="116.78666666666668" />
         <tli id="T165" time="117.298" type="appl" />
         <tli id="T166" time="117.916" type="appl" />
         <tli id="T167" time="118.534" type="appl" />
         <tli id="T168" time="119.152" type="appl" />
         <tli id="T169" time="119.78333333333333" />
         <tli id="T170" time="119.789" type="appl" />
         <tli id="T171" time="120.42933333333333" type="appl" />
         <tli id="T172" time="121.06966666666666" type="appl" />
         <tli id="T173" time="121.89666666666668" />
         <tli id="T174" time="122.34" />
         <tli id="T175" time="122.71571428571428" type="appl" />
         <tli id="T176" time="123.13142857142857" type="appl" />
         <tli id="T177" time="123.54714285714286" type="appl" />
         <tli id="T178" time="123.96285714285713" type="appl" />
         <tli id="T179" time="124.37857142857142" type="appl" />
         <tli id="T180" time="124.7942857142857" type="appl" />
         <tli id="T181" time="125.35" />
         <tli id="T182" time="125.36" type="appl" />
         <tli id="T183" time="125.854" type="appl" />
         <tli id="T184" time="126.348" type="appl" />
         <tli id="T185" time="126.842" type="appl" />
         <tli id="T186" time="127.336" type="appl" />
         <tli id="T187" time="127.83" type="appl" />
         <tli id="T188" time="128.324" type="appl" />
         <tli id="T189" time="128.818" type="appl" />
         <tli id="T190" time="129.312" type="appl" />
         <tli id="T191" time="129.806" type="appl" />
         <tli id="T192" time="130.3" type="appl" />
         <tli id="T193" time="130.794" type="appl" />
         <tli id="T194" time="131.288" type="appl" />
         <tli id="T195" time="131.782" type="appl" />
         <tli id="T196" time="132.276" type="appl" />
         <tli id="T197" time="132.8166666666667" />
         <tli id="T198" time="133.46666666666667" />
         <tli id="T199" time="133.8788888888889" type="appl" />
         <tli id="T200" time="134.51777777777778" type="appl" />
         <tli id="T201" time="135.15666666666667" type="appl" />
         <tli id="T202" time="135.79555555555555" type="appl" />
         <tli id="T203" time="136.43444444444447" type="appl" />
         <tli id="T204" time="137.07333333333335" type="appl" />
         <tli id="T205" time="137.71222222222224" type="appl" />
         <tli id="T206" time="138.35111111111112" type="appl" />
         <tli id="T207" time="139.0033333333333" />
         <tli id="T208" time="139.01" type="appl" />
         <tli id="T209" time="139.57888888888888" type="appl" />
         <tli id="T210" time="140.14777777777778" type="appl" />
         <tli id="T211" time="140.71666666666667" type="appl" />
         <tli id="T212" time="141.28555555555556" type="appl" />
         <tli id="T213" time="141.85444444444443" type="appl" />
         <tli id="T214" time="142.42333333333332" type="appl" />
         <tli id="T215" time="142.9922222222222" type="appl" />
         <tli id="T216" time="143.5611111111111" type="appl" />
         <tli id="T217" time="143.94333333333336" />
         <tli id="T218" time="144.3733333333333" />
         <tli id="T219" time="145.09166666666667" type="appl" />
         <tli id="T220" time="145.72333333333333" type="appl" />
         <tli id="T221" time="146.35500000000002" type="appl" />
         <tli id="T222" time="146.98666666666668" type="appl" />
         <tli id="T223" time="147.61833333333334" type="appl" />
         <tli id="T224" time="148.53" />
         <tli id="T225" time="149.14" />
         <tli id="T226" time="150.00933333333333" type="appl" />
         <tli id="T227" time="151.19866666666667" type="appl" />
         <tli id="T228" time="152.388" type="appl" />
         <tli id="T229" time="153.5773333333333" type="appl" />
         <tli id="T230" time="154.76666666666665" type="appl" />
         <tli id="T231" time="155.956" type="appl" />
         <tli id="T232" time="157.14533333333333" type="appl" />
         <tli id="T233" time="158.33466666666666" type="appl" />
         <tli id="T234" time="159.524" type="appl" />
         <tli id="T235" time="160.71333333333334" type="appl" />
         <tli id="T236" time="161.90266666666668" type="appl" />
         <tli id="T237" time="163.09199999999998" type="appl" />
         <tli id="T238" time="164.28133333333332" type="appl" />
         <tli id="T239" time="165.47066666666666" type="appl" />
         <tli id="T240" time="166.44" />
         <tli id="T241" time="167.28666666666666" />
         <tli id="T242" time="167.7125" type="appl" />
         <tli id="T243" time="168.385" type="appl" />
         <tli id="T244" time="169.0575" type="appl" />
         <tli id="T245" time="169.73" type="appl" />
         <tli id="T246" time="170.4025" type="appl" />
         <tli id="T247" time="171.075" type="appl" />
         <tli id="T248" time="171.7475" type="appl" />
         <tli id="T249" time="172.42000000000002" type="appl" />
         <tli id="T250" time="173.0925" type="appl" />
         <tli id="T251" time="173.76500000000001" type="appl" />
         <tli id="T252" time="174.4375" type="appl" />
         <tli id="T253" time="175.11" type="appl" />
         <tli id="T254" time="175.7825" type="appl" />
         <tli id="T255" time="176.455" type="appl" />
         <tli id="T256" time="177.1275" type="appl" />
         <tli id="T257" time="177.47333333333333" />
         <tli id="T258" time="177.98" />
         <tli id="T259" time="178.45250000000001" type="appl" />
         <tli id="T260" time="179.005" type="appl" />
         <tli id="T261" time="179.5575" type="appl" />
         <tli id="T262" time="180.11" type="appl" />
         <tli id="T263" time="180.6625" type="appl" />
         <tli id="T264" time="181.215" type="appl" />
         <tli id="T265" time="181.7675" type="appl" />
         <tli id="T266" time="182.32" type="appl" />
         <tli id="T267" time="182.8725" type="appl" />
         <tli id="T268" time="183.425" type="appl" />
         <tli id="T269" time="183.9775" type="appl" />
         <tli id="T270" time="184.26333333333335" />
         <tli id="T271" time="185.05666666666667" />
         <tli id="T272" time="185.71499999999997" type="appl" />
         <tli id="T273" time="186.54" type="appl" />
         <tli id="T274" time="187.365" type="appl" />
         <tli id="T275" time="188.19" type="appl" />
         <tli id="T276" time="189.015" type="appl" />
         <tli id="T277" time="189.84" type="appl" />
         <tli id="T278" time="190.66500000000002" type="appl" />
         <tli id="T279" time="190.91" />
         <tli id="T280" time="191.76666666666668" />
         <tli id="T281" time="192.54333333333332" type="appl" />
         <tli id="T282" time="193.44666666666666" type="appl" />
         <tli id="T283" time="194.35" type="appl" />
         <tli id="T284" time="195.25333333333333" type="appl" />
         <tli id="T285" time="196.15666666666667" type="appl" />
         <tli id="T286" time="197.06" type="appl" />
         <tli id="T287" time="197.9633333333333" type="appl" />
         <tli id="T288" time="198.86666666666665" type="appl" />
         <tli id="T289" time="199.76999999999998" type="appl" />
         <tli id="T290" time="200.67333333333332" type="appl" />
         <tli id="T291" time="201.57666666666665" type="appl" />
         <tli id="T292" time="202.49333333333334" />
         <tli id="T293" time="202.5" />
         <tli id="T294" time="203.27363636363637" type="appl" />
         <tli id="T295" time="203.92727272727274" type="appl" />
         <tli id="T296" time="204.5809090909091" type="appl" />
         <tli id="T297" time="205.23454545454547" type="appl" />
         <tli id="T298" time="205.88818181818183" type="appl" />
         <tli id="T299" time="206.54181818181817" type="appl" />
         <tli id="T300" time="207.19545454545454" type="appl" />
         <tli id="T301" time="207.8490909090909" type="appl" />
         <tli id="T302" time="208.50272727272727" type="appl" />
         <tli id="T303" time="209.15636363636364" type="appl" />
         <tli id="T304" time="209.6566666666667" />
         <tli id="T305" time="210.22666666666666" />
         <tli id="T306" time="210.66316666666665" type="appl" />
         <tli id="T307" time="211.34633333333332" type="appl" />
         <tli id="T308" time="212.02949999999998" type="appl" />
         <tli id="T309" time="212.71266666666668" type="appl" />
         <tli id="T310" time="213.39583333333334" type="appl" />
         <tli id="T311" time="214.079" type="appl" />
         <tli id="T312" time="214.11" type="appl" />
         <tli id="T313" time="214.57428571428574" type="appl" />
         <tli id="T314" time="215.03857142857143" type="appl" />
         <tli id="T315" time="215.50285714285715" type="appl" />
         <tli id="T316" time="215.96714285714287" type="appl" />
         <tli id="T317" time="216.4314285714286" type="appl" />
         <tli id="T318" time="216.8957142857143" type="appl" />
         <tli id="T319" time="217.22" />
         <tli id="T320" time="217.7723307291667" />
         <tli id="T321" time="218.09244444444445" type="appl" />
         <tli id="T322" time="218.6058888888889" type="appl" />
         <tli id="T323" time="219.11933333333334" type="appl" />
         <tli id="T324" time="219.6327777777778" type="appl" />
         <tli id="T325" time="220.1462222222222" type="appl" />
         <tli id="T326" time="220.65966666666665" type="appl" />
         <tli id="T327" time="221.1731111111111" type="appl" />
         <tli id="T328" time="221.68655555555554" type="appl" />
         <tli id="T329" time="222.39333333333332" />
         <tli id="T330" time="223.01666666666668" />
         <tli id="T331" time="223.4623076923077" type="appl" />
         <tli id="T332" time="223.9946153846154" type="appl" />
         <tli id="T333" time="224.52692307692308" type="appl" />
         <tli id="T334" time="225.05923076923077" type="appl" />
         <tli id="T335" time="225.59153846153848" type="appl" />
         <tli id="T336" time="226.12384615384616" type="appl" />
         <tli id="T337" time="226.65615384615384" type="appl" />
         <tli id="T338" time="227.18846153846152" type="appl" />
         <tli id="T339" time="227.72076923076924" type="appl" />
         <tli id="T340" time="228.25307692307692" type="appl" />
         <tli id="T341" time="228.7853846153846" type="appl" />
         <tli id="T342" time="229.3176923076923" type="appl" />
         <tli id="T343" time="229.85" type="appl" />
         <tli id="T344" time="230.52" />
         <tli id="T345" time="231.28444444444446" type="appl" />
         <tli id="T346" time="232.38888888888889" type="appl" />
         <tli id="T347" time="233.49333333333334" type="appl" />
         <tli id="T348" time="234.5977777777778" type="appl" />
         <tli id="T349" time="235.70222222222222" type="appl" />
         <tli id="T350" time="236.80666666666667" type="appl" />
         <tli id="T351" time="237.91111111111113" type="appl" />
         <tli id="T352" time="239.01555555555555" type="appl" />
         <tli id="T353" time="240.12" type="appl" />
         <tli id="T354" time="240.52" type="appl" />
         <tli id="T355" time="241.20454545454547" type="appl" />
         <tli id="T356" time="241.88909090909092" type="appl" />
         <tli id="T357" time="242.57363636363638" type="appl" />
         <tli id="T358" time="243.25818181818184" type="appl" />
         <tli id="T359" time="243.9427272727273" type="appl" />
         <tli id="T360" time="244.62727272727273" type="appl" />
         <tli id="T361" time="245.31181818181818" type="appl" />
         <tli id="T362" time="245.99636363636364" type="appl" />
         <tli id="T363" time="246.6809090909091" type="appl" />
         <tli id="T364" time="247.36545454545455" type="appl" />
         <tli id="T365" time="248.05" type="appl" />
         <tli id="T366" time="248.07" type="appl" />
         <tli id="T367" time="248.555" type="appl" />
         <tli id="T368" time="249.04" type="appl" />
         <tli id="T369" time="249.52499999999998" type="appl" />
         <tli id="T370" time="250.01" type="appl" />
         <tli id="T371" time="250.3433333333333" />
         <tli id="T372" time="250.8577857142857" type="appl" />
         <tli id="T373" time="251.48557142857143" type="appl" />
         <tli id="T374" time="252.11335714285713" type="appl" />
         <tli id="T375" time="252.74114285714285" type="appl" />
         <tli id="T376" time="253.36892857142857" type="appl" />
         <tli id="T377" time="253.9967142857143" type="appl" />
         <tli id="T378" time="254.6245" type="appl" />
         <tli id="T379" time="255.2522857142857" type="appl" />
         <tli id="T380" time="255.88007142857143" type="appl" />
         <tli id="T381" time="256.5078571428571" type="appl" />
         <tli id="T382" time="257.13564285714284" type="appl" />
         <tli id="T383" time="257.76342857142856" type="appl" />
         <tli id="T384" time="258.3912142857143" type="appl" />
         <tli id="T385" time="258.73234375" />
         <tli id="T386" time="259.43333333333334" />
         <tli id="T387" time="259.62" type="appl" />
         <tli id="T388" time="260.0" type="appl" />
         <tli id="T389" time="260.38" type="appl" />
         <tli id="T390" time="260.76" type="appl" />
         <tli id="T391" time="260.779" type="appl" />
         <tli id="T392" time="261.44272727272727" type="appl" />
         <tli id="T393" time="262.10645454545454" type="appl" />
         <tli id="T394" time="262.7701818181818" type="appl" />
         <tli id="T395" time="263.4339090909091" type="appl" />
         <tli id="T396" time="264.09763636363635" type="appl" />
         <tli id="T397" time="264.7613636363636" type="appl" />
         <tli id="T398" time="265.4250909090909" type="appl" />
         <tli id="T399" time="266.08881818181817" type="appl" />
         <tli id="T400" time="266.75254545454544" type="appl" />
         <tli id="T401" time="267.4162727272727" type="appl" />
         <tli id="T402" time="268.32" />
         <tli id="T403" time="268.78444444444443" type="appl" />
         <tli id="T404" time="269.4888888888889" type="appl" />
         <tli id="T405" time="270.1933333333333" type="appl" />
         <tli id="T406" time="270.8977777777778" type="appl" />
         <tli id="T407" time="271.6022222222222" type="appl" />
         <tli id="T408" time="272.3066666666667" type="appl" />
         <tli id="T409" time="273.0111111111111" type="appl" />
         <tli id="T410" time="273.71555555555557" type="appl" />
         <tli id="T411" time="274.36" />
         <tli id="T412" time="274.90000000000003" type="appl" />
         <tli id="T413" time="275.38" type="appl" />
         <tli id="T414" time="275.86" type="appl" />
         <tli id="T415" time="276.34000000000003" type="appl" />
         <tli id="T416" time="276.82" type="appl" />
         <tli id="T417" time="277.06" />
         <tli id="T418" time="278.02" />
         <tli id="T419" time="278.23766666666666" type="appl" />
         <tli id="T420" time="278.9153333333333" type="appl" />
         <tli id="T421" time="279.593" type="appl" />
         <tli id="T422" time="280.27066666666667" type="appl" />
         <tli id="T423" time="280.9483333333333" type="appl" />
         <tli id="T424" time="281.626" type="appl" />
         <tli id="T425" time="282.3036666666667" type="appl" />
         <tli id="T426" time="282.98133333333334" type="appl" />
         <tli id="T427" time="283.659" type="appl" />
         <tli id="T428" time="283.68" type="appl" />
         <tli id="T429" time="284.18" type="appl" />
         <tli id="T430" time="284.68" type="appl" />
         <tli id="T431" time="285.18" type="appl" />
         <tli id="T432" time="285.4266666666666" />
         <tli id="T433" time="286.02" type="appl" />
         <tli id="T434" time="286.74777777777774" type="appl" />
         <tli id="T435" time="287.47555555555556" type="appl" />
         <tli id="T436" time="288.2033333333333" type="appl" />
         <tli id="T437" time="288.9311111111111" type="appl" />
         <tli id="T438" time="289.6588888888889" type="appl" />
         <tli id="T439" time="290.38666666666666" type="appl" />
         <tli id="T440" time="291.1144444444444" type="appl" />
         <tli id="T441" time="291.84222222222223" type="appl" />
         <tli id="T442" time="292.4233333333333" />
         <tli id="T443" time="292.83" type="appl" />
         <tli id="T444" time="293.43333333333334" type="appl" />
         <tli id="T445" time="294.03666666666663" type="appl" />
         <tli id="T446" time="294.64" type="appl" />
         <tli id="T447" time="295.24333333333334" type="appl" />
         <tli id="T448" time="295.84666666666664" type="appl" />
         <tli id="T449" time="296.45" type="appl" />
         <tli id="T450" time="297.05333333333334" type="appl" />
         <tli id="T451" time="297.65666666666664" type="appl" />
         <tli id="T452" time="298.26" type="appl" />
         <tli id="T453" time="298.86333333333334" type="appl" />
         <tli id="T454" time="299.46666666666664" type="appl" />
         <tli id="T455" time="300.07" type="appl" />
         <tli id="T456" time="300.67333333333335" type="appl" />
         <tli id="T457" time="301.27666666666664" type="appl" />
         <tli id="T458" time="301.88" type="appl" />
         <tli id="T459" time="302.48333333333335" type="appl" />
         <tli id="T460" time="303.08666666666664" type="appl" />
         <tli id="T461" time="303.6166666666666" />
         <tli id="T462" time="303.92" />
         <tli id="T463" time="304.6457142857143" type="appl" />
         <tli id="T464" time="305.45142857142855" type="appl" />
         <tli id="T465" time="306.25714285714287" type="appl" />
         <tli id="T466" time="307.0628571428571" type="appl" />
         <tli id="T467" time="307.86857142857144" type="appl" />
         <tli id="T468" time="308.6742857142857" type="appl" />
         <tli id="T469" time="309.28666666666663" />
         <tli id="T470" time="309.7266666666667" />
         <tli id="T471" time="310.21799999999996" type="appl" />
         <tli id="T472" time="310.736" type="appl" />
         <tli id="T473" time="311.254" type="appl" />
         <tli id="T474" time="311.772" type="appl" />
         <tli id="T475" time="312.28999999999996" type="appl" />
         <tli id="T476" time="312.808" type="appl" />
         <tli id="T477" time="313.326" type="appl" />
         <tli id="T478" time="313.844" type="appl" />
         <tli id="T479" time="314.36199999999997" type="appl" />
         <tli id="T480" time="314.88" type="appl" />
         <tli id="T481" time="315.398" type="appl" />
         <tli id="T482" time="315.916" type="appl" />
         <tli id="T483" time="316.43399999999997" type="appl" />
         <tli id="T484" time="316.952" type="appl" />
         <tli id="T485" time="317.47" type="appl" />
         <tli id="T486" time="317.988" type="appl" />
         <tli id="T487" time="318.506" type="appl" />
         <tli id="T488" time="319.024" type="appl" />
         <tli id="T489" time="319.54200000000003" type="appl" />
         <tli id="T490" time="320.0" />
         <tli id="T491" time="320.24333333333334" />
         <tli id="T492" time="320.6383333333333" type="appl" />
         <tli id="T493" time="321.14666666666665" type="appl" />
         <tli id="T494" time="321.655" type="appl" />
         <tli id="T495" time="322.16333333333336" type="appl" />
         <tli id="T496" time="322.6716666666667" type="appl" />
         <tli id="T497" time="323.18" type="appl" />
         <tli id="T498" time="323.68833333333333" type="appl" />
         <tli id="T499" time="324.19666666666666" type="appl" />
         <tli id="T500" time="324.70500000000004" type="appl" />
         <tli id="T501" time="325.21333333333337" type="appl" />
         <tli id="T502" time="325.7216666666667" type="appl" />
         <tli id="T503" time="326.23" type="appl" />
         <tli id="T504" time="326.2366666666667" />
         <tli id="T505" time="326.87199999999996" type="appl" />
         <tli id="T506" time="327.474" type="appl" />
         <tli id="T507" time="328.076" type="appl" />
         <tli id="T508" time="328.678" type="appl" />
         <tli id="T509" time="329.28" type="appl" />
         <tli id="T510" time="329.882" type="appl" />
         <tli id="T511" time="330.48400000000004" type="appl" />
         <tli id="T512" time="331.086" type="appl" />
         <tli id="T513" time="331.688" type="appl" />
         <tli id="T514" time="332.0566666666666" />
         <tli id="T515" time="332.5966666666667" />
         <tli id="T516" time="333.2342857142857" type="appl" />
         <tli id="T517" time="333.8985714285714" type="appl" />
         <tli id="T518" time="334.5628571428571" type="appl" />
         <tli id="T519" time="335.2271428571429" type="appl" />
         <tli id="T520" time="335.8914285714286" type="appl" />
         <tli id="T521" time="336.5557142857143" type="appl" />
         <tli id="T522" time="336.9666666666667" />
         <tli id="T523" time="337.5466666666667" />
         <tli id="T524" time="338.0344444444445" type="appl" />
         <tli id="T525" time="338.5888888888889" type="appl" />
         <tli id="T526" time="339.1433333333334" type="appl" />
         <tli id="T527" time="339.6977777777778" type="appl" />
         <tli id="T528" time="340.25222222222226" type="appl" />
         <tli id="T529" time="340.8066666666667" type="appl" />
         <tli id="T530" time="341.36111111111114" type="appl" />
         <tli id="T531" time="341.91555555555556" type="appl" />
         <tli id="T532" time="342.3966666666667" />
         <tli id="T533" time="342.5233333333333" />
         <tli id="T534" time="343.331" type="appl" />
         <tli id="T535" time="344.092" type="appl" />
         <tli id="T536" time="344.853" type="appl" />
         <tli id="T537" time="345.614" type="appl" />
         <tli id="T538" time="346.375" type="appl" />
         <tli id="T539" time="347.136" type="appl" />
         <tli id="T540" time="347.897" type="appl" />
         <tli id="T541" time="348.658" type="appl" />
         <tli id="T542" time="348.6933333333333" />
         <tli id="T543" time="348.82" />
         <tli id="T544" time="352.67333333333335" />
         <tli id="T545" time="352.78" />
         <tli id="T546" time="352.90333333333336" type="appl" />
         <tli id="T547" time="353.7" type="appl" />
         <tli id="T548" time="354.49666666666667" type="appl" />
         <tli id="T549" time="355.29333333333335" type="appl" />
         <tli id="T550" time="356.09000000000003" type="appl" />
         <tli id="T551" time="356.88666666666666" type="appl" />
         <tli id="T552" time="357.68333333333334" type="appl" />
         <tli id="T553" time="358.1066666666667" />
         <tli id="T554" time="358.6256770833333" />
         <tli id="T555" time="359.3028125" type="appl" />
         <tli id="T556" time="359.946625" type="appl" />
         <tli id="T557" time="360.5904375" type="appl" />
         <tli id="T558" time="361.23425" type="appl" />
         <tli id="T559" time="361.8780625" type="appl" />
         <tli id="T560" time="362.52187499999997" type="appl" />
         <tli id="T561" time="363.1656875" type="appl" />
         <tli id="T562" time="363.80949999999996" type="appl" />
         <tli id="T563" time="364.4533125" type="appl" />
         <tli id="T564" time="365.097125" type="appl" />
         <tli id="T565" time="365.7409375" type="appl" />
         <tli id="T566" time="366.38475" type="appl" />
         <tli id="T567" time="367.02856249999996" type="appl" />
         <tli id="T568" time="367.672375" type="appl" />
         <tli id="T569" time="368.31618749999996" type="appl" />
         <tli id="T570" time="368.94" />
         <tli id="T571" time="369.2333333333333" />
         <tli id="T572" time="369.6983333333333" type="appl" />
         <tli id="T573" time="370.31666666666666" type="appl" />
         <tli id="T574" time="370.935" type="appl" />
         <tli id="T575" time="371.55333333333334" type="appl" />
         <tli id="T576" time="372.1716666666667" type="appl" />
         <tli id="T577" time="372.80333333333334" />
         <tli id="T578" time="373.3" />
         <tli id="T579" time="373.72375" type="appl" />
         <tli id="T580" time="374.3075" type="appl" />
         <tli id="T581" time="374.89125" type="appl" />
         <tli id="T582" time="375.475" type="appl" />
         <tli id="T583" time="376.05875" type="appl" />
         <tli id="T584" time="376.6425" type="appl" />
         <tli id="T585" time="377.22625" type="appl" />
         <tli id="T586" time="377.81" type="appl" />
         <tli id="T587" time="377.81666666666666" />
         <tli id="T588" time="378.41999999999996" type="appl" />
         <tli id="T589" time="379.01" type="appl" />
         <tli id="T590" time="379.59999999999997" type="appl" />
         <tli id="T591" time="380.19" type="appl" />
         <tli id="T592" time="380.59333333333336" />
         <tli id="T593" time="380.88000000000005" />
         <tli id="T594" time="381.4463636363637" type="appl" />
         <tli id="T595" time="382.0427272727273" type="appl" />
         <tli id="T596" time="382.63909090909095" type="appl" />
         <tli id="T597" time="383.23545454545456" type="appl" />
         <tli id="T598" time="383.8318181818182" type="appl" />
         <tli id="T599" time="384.4281818181818" type="appl" />
         <tli id="T600" time="385.0245454545455" type="appl" />
         <tli id="T601" time="385.6209090909091" type="appl" />
         <tli id="T602" time="386.21727272727276" type="appl" />
         <tli id="T603" time="386.81363636363636" type="appl" />
         <tli id="T604" time="387.2966666666667" />
         <tli id="T605" time="387.6566666666667" />
         <tli id="T606" time="388.29272727272723" type="appl" />
         <tli id="T607" time="388.87545454545455" type="appl" />
         <tli id="T608" time="389.4581818181818" type="appl" />
         <tli id="T609" time="390.04090909090905" type="appl" />
         <tli id="T610" time="390.62363636363636" type="appl" />
         <tli id="T611" time="391.2063636363636" type="appl" />
         <tli id="T612" time="391.78909090909093" type="appl" />
         <tli id="T613" time="392.3718181818182" type="appl" />
         <tli id="T614" time="392.95454545454544" type="appl" />
         <tli id="T615" time="393.53727272727275" type="appl" />
         <tli id="T616" time="394.12" />
         <tli id="T617" time="394.1266666666666" />
         <tli id="T618" time="394.71" type="appl" />
         <tli id="T619" time="395.28" type="appl" />
         <tli id="T620" time="395.84999999999997" type="appl" />
         <tli id="T621" time="396.41999999999996" type="appl" />
         <tli id="T622" time="396.99" type="appl" />
         <tli id="T623" time="397.56" type="appl" />
         <tli id="T624" time="398.13" type="appl" />
         <tli id="T625" time="398.7" type="appl" />
         <tli id="T626" time="400.3133333333334" />
         <tli id="T627" time="400.42526315789473" type="appl" />
         <tli id="T628" time="401.04052631578946" type="appl" />
         <tli id="T629" time="401.6557894736842" type="appl" />
         <tli id="T630" time="402.2710526315789" type="appl" />
         <tli id="T631" time="402.8863157894737" type="appl" />
         <tli id="T632" time="403.50157894736844" type="appl" />
         <tli id="T633" time="404.1168421052632" type="appl" />
         <tli id="T634" time="404.7321052631579" type="appl" />
         <tli id="T635" time="405.34736842105264" type="appl" />
         <tli id="T636" time="405.96263157894737" type="appl" />
         <tli id="T637" time="406.5778947368421" type="appl" />
         <tli id="T638" time="407.1931578947368" type="appl" />
         <tli id="T639" time="407.80842105263156" type="appl" />
         <tli id="T640" time="408.4236842105263" type="appl" />
         <tli id="T641" time="409.0389473684211" type="appl" />
         <tli id="T642" time="409.6542105263158" type="appl" />
         <tli id="T643" time="410.26947368421054" type="appl" />
         <tli id="T644" time="410.88473684210527" type="appl" />
         <tli id="T645" time="411.4266666666667" />
         <tli id="T646" time="411.72666666666663" />
         <tli id="T647" time="412.80333333333334" type="appl" />
         <tli id="T648" time="414.02666666666664" type="appl" />
         <tli id="T649" time="415.10333333333335" />
         <tli id="T650" time="415.5733333333334" />
         <tli id="T651" time="416.14375" type="appl" />
         <tli id="T652" time="416.8675" type="appl" />
         <tli id="T653" time="417.59125" type="appl" />
         <tli id="T654" time="418.315" type="appl" />
         <tli id="T655" time="419.03875" type="appl" />
         <tli id="T656" time="419.7625" type="appl" />
         <tli id="T657" time="420.48625" type="appl" />
         <tli id="T658" time="421.0233333333333" />
         <tli id="T659" time="421.3366666666667" />
         <tli id="T660" time="422.16125" type="appl" />
         <tli id="T661" time="423.0325" type="appl" />
         <tli id="T662" time="423.90375" type="appl" />
         <tli id="T663" time="424.775" type="appl" />
         <tli id="T664" time="425.64625" type="appl" />
         <tli id="T665" time="426.5175" type="appl" />
         <tli id="T666" time="427.38875" type="appl" />
         <tli id="T667" time="428.0533333333334" />
         <tli id="T668" time="428.7133333333333" />
         <tli id="T669" time="429.29714285714283" type="appl" />
         <tli id="T670" time="430.01428571428573" type="appl" />
         <tli id="T671" time="430.7314285714286" type="appl" />
         <tli id="T672" time="431.4485714285714" type="appl" />
         <tli id="T673" time="432.1657142857143" type="appl" />
         <tli id="T674" time="432.8828571428572" type="appl" />
         <tli id="T675" time="433.65333333333336" />
         <tli id="T676" time="434.2966666666667" />
         <tli id="T677" time="434.5881818181818" type="appl" />
         <tli id="T678" time="434.98636363636365" type="appl" />
         <tli id="T679" time="435.38454545454545" type="appl" />
         <tli id="T680" time="435.78272727272724" type="appl" />
         <tli id="T681" time="436.1809090909091" type="appl" />
         <tli id="T682" time="436.5790909090909" type="appl" />
         <tli id="T683" time="436.97727272727275" type="appl" />
         <tli id="T684" time="437.37545454545455" type="appl" />
         <tli id="T685" time="437.77363636363634" type="appl" />
         <tli id="T686" time="438.1718181818182" type="appl" />
         <tli id="T687" time="438.47" />
         <tli id="T688" time="438.4766666666667" />
         <tli id="T689" time="439.2633333333333" type="appl" />
         <tli id="T690" time="439.93666666666667" type="appl" />
         <tli id="T691" time="440.61" type="appl" />
         <tli id="T692" time="441.2833333333333" type="appl" />
         <tli id="T693" time="441.95666666666665" type="appl" />
         <tli id="T694" time="442.63" type="appl" />
         <tli id="T695" time="443.06" />
         <tli id="T696" time="443.4783333333333" type="appl" />
         <tli id="T697" time="443.99666666666667" type="appl" />
         <tli id="T698" time="444.515" type="appl" />
         <tli id="T699" time="445.0333333333333" type="appl" />
         <tli id="T700" time="445.5516666666667" type="appl" />
         <tli id="T701" time="446.07" type="appl" />
         <tli id="T702" time="446.6066666666666" />
         <tli id="T703" time="447.0214285714286" type="appl" />
         <tli id="T704" time="447.48285714285714" type="appl" />
         <tli id="T705" time="447.94428571428574" type="appl" />
         <tli id="T706" time="448.4057142857143" type="appl" />
         <tli id="T707" time="448.8671428571429" type="appl" />
         <tli id="T708" time="449.3285714285714" type="appl" />
         <tli id="T709" time="449.7766666666667" />
         <tli id="T710" time="449.95" />
         <tli id="T711" time="450.3414285714286" type="appl" />
         <tli id="T712" time="450.8128571428571" type="appl" />
         <tli id="T713" time="451.2842857142857" type="appl" />
         <tli id="T714" time="451.7557142857143" type="appl" />
         <tli id="T715" time="452.2271428571429" type="appl" />
         <tli id="T716" time="452.6985714285714" type="appl" />
         <tli id="T717" time="453.17" type="appl" />
         <tli id="T718" time="453.6414285714286" type="appl" />
         <tli id="T719" time="454.11285714285714" type="appl" />
         <tli id="T720" time="454.5842857142857" type="appl" />
         <tli id="T721" time="455.0557142857143" type="appl" />
         <tli id="T722" time="455.5271428571429" type="appl" />
         <tli id="T723" time="455.99857142857144" type="appl" />
         <tli id="T724" time="456.47" type="appl" />
         <tli id="T868" time="457.3522916666667" type="intp" />
         <tli id="T869" time="458.058125" type="intp" />
         <tli id="T870" time="459.29333333333335" type="intp" />
         <tli id="T871" time="460.175625" type="intp" />
         <tli id="T872" time="461.234375" type="intp" />
         <tli id="T873" time="461.9402083333333" type="intp" />
         <tli id="T874" time="462.99895833333335" type="intp" />
         <tli id="T725" time="464.94" type="appl" />
         <tli id="T726" time="465.7266666666667" type="appl" />
         <tli id="T727" time="466.5133333333333" type="appl" />
         <tli id="T728" time="467.3" type="appl" />
         <tli id="T729" time="468.08666666666664" type="appl" />
         <tli id="T730" time="468.87333333333333" type="appl" />
         <tli id="T731" time="469.65999999999997" type="appl" />
         <tli id="T732" time="470.44666666666666" type="appl" />
         <tli id="T733" time="471.2333333333333" type="appl" />
         <tli id="T734" time="472.02" type="appl" />
         <tli id="T735" time="472.12" type="appl" />
         <tli id="T736" time="472.59000000000003" type="appl" />
         <tli id="T737" time="473.06" type="appl" />
         <tli id="T738" time="473.53" type="appl" />
         <tli id="T739" time="474.0" type="appl" />
         <tli id="T740" time="474.47" type="appl" />
         <tli id="T741" time="474.94" type="appl" />
         <tli id="T742" time="475.40999999999997" type="appl" />
         <tli id="T743" time="475.88" type="appl" />
         <tli id="T744" time="476.35" type="appl" />
         <tli id="T745" time="476.76" />
         <tli id="T746" time="476.9233333333333" />
         <tli id="T747" time="477.5225" type="appl" />
         <tli id="T748" time="478.155" type="appl" />
         <tli id="T749" time="478.78749999999997" type="appl" />
         <tli id="T750" time="479.41999999999996" type="appl" />
         <tli id="T751" time="480.0525" type="appl" />
         <tli id="T752" time="480.685" type="appl" />
         <tli id="T753" time="481.3175" type="appl" />
         <tli id="T754" time="482.0766666666667" />
         <tli id="T755" time="483.1666666666667" />
         <tli id="T756" time="483.264" type="appl" />
         <tli id="T757" time="484.028" type="appl" />
         <tli id="T758" time="484.792" type="appl" />
         <tli id="T759" time="485.556" type="appl" />
         <tli id="T760" time="486.32" type="appl" />
         <tli id="T761" time="486.44" type="appl" />
         <tli id="T762" time="487.094" type="appl" />
         <tli id="T763" time="487.748" type="appl" />
         <tli id="T764" time="488.402" type="appl" />
         <tli id="T765" time="489.056" type="appl" />
         <tli id="T766" time="489.39" />
         <tli id="T767" time="489.39666666666665" />
         <tli id="T768" time="490.3516666666667" type="appl" />
         <tli id="T769" time="490.97333333333336" type="appl" />
         <tli id="T770" time="491.595" type="appl" />
         <tli id="T771" time="492.2166666666667" type="appl" />
         <tli id="T772" time="492.83833333333337" type="appl" />
         <tli id="T773" time="493.46000000000004" type="appl" />
         <tli id="T774" time="494.08166666666665" type="appl" />
         <tli id="T775" time="494.7033333333333" type="appl" />
         <tli id="T776" time="495.325" type="appl" />
         <tli id="T777" time="495.94666666666666" type="appl" />
         <tli id="T778" time="496.5683333333333" type="appl" />
         <tli id="T779" time="497.19" type="appl" />
         <tli id="T780" time="497.5966666666667" />
         <tli id="T781" time="498.456" type="appl" />
         <tli id="T782" time="499.462" type="appl" />
         <tli id="T783" time="500.468" type="appl" />
         <tli id="T784" time="501.474" type="appl" />
         <tli id="T785" time="502.48" type="appl" />
         <tli id="T786" time="502.5" type="appl" />
         <tli id="T787" time="502.9266666666667" type="appl" />
         <tli id="T788" time="503.35333333333335" type="appl" />
         <tli id="T789" time="503.78" type="appl" />
         <tli id="T790" time="504.20666666666665" type="appl" />
         <tli id="T791" time="504.6333333333333" type="appl" />
         <tli id="T792" time="504.99333333333334" />
         <tli id="T793" time="505.18" />
         <tli id="T794" time="505.772" type="appl" />
         <tli id="T795" time="506.444" type="appl" />
         <tli id="T796" time="507.116" type="appl" />
         <tli id="T797" time="507.788" type="appl" />
         <tli id="T798" time="508.24" />
         <tli id="T799" time="508.55" />
         <tli id="T800" time="509.146" type="appl" />
         <tli id="T801" time="509.782" type="appl" />
         <tli id="T802" time="510.418" type="appl" />
         <tli id="T803" time="511.054" type="appl" />
         <tli id="T804" time="511.61" />
         <tli id="T805" time="511.7033333333333" />
         <tli id="T806" time="512.1116666666667" type="appl" />
         <tli id="T807" time="512.4733333333334" type="appl" />
         <tli id="T808" time="512.835" type="appl" />
         <tli id="T809" time="513.1966666666667" type="appl" />
         <tli id="T810" time="513.5583333333334" type="appl" />
         <tli id="T811" time="513.9200000000001" type="appl" />
         <tli id="T812" time="514.2816666666666" type="appl" />
         <tli id="T813" time="514.6433333333333" type="appl" />
         <tli id="T814" time="515.005" type="appl" />
         <tli id="T815" time="515.3666666666667" type="appl" />
         <tli id="T816" time="515.7283333333334" type="appl" />
         <tli id="T817" time="515.97" />
         <tli id="T818" time="516.27234375" />
         <tli id="T819" time="516.61925" type="appl" />
         <tli id="T820" time="517.0795" type="appl" />
         <tli id="T821" time="517.53975" type="appl" />
         <tli id="T822" time="517.7466666666667" />
         <tli id="T823" time="518.2056770833334" />
         <tli id="T824" time="518.4617307692307" type="appl" />
         <tli id="T825" time="518.7644615384615" type="appl" />
         <tli id="T826" time="519.0671923076923" type="appl" />
         <tli id="T827" time="519.3699230769231" type="appl" />
         <tli id="T828" time="519.6726538461538" type="appl" />
         <tli id="T829" time="519.9753846153847" type="appl" />
         <tli id="T830" time="520.2781153846154" type="appl" />
         <tli id="T831" time="520.5808461538461" type="appl" />
         <tli id="T832" time="520.8835769230769" type="appl" />
         <tli id="T833" time="521.1863076923077" type="appl" />
         <tli id="T834" time="521.4890384615385" type="appl" />
         <tli id="T835" time="521.7917692307692" type="appl" />
         <tli id="T836" time="522.0944999999999" type="appl" />
         <tli id="T837" time="522.3972307692308" type="appl" />
         <tli id="T838" time="522.6999615384615" type="appl" />
         <tli id="T839" time="523.0026923076923" type="appl" />
         <tli id="T840" time="523.305423076923" type="appl" />
         <tli id="T841" time="523.6081538461539" type="appl" />
         <tli id="T842" time="523.9108846153846" type="appl" />
         <tli id="T843" time="524.2136153846153" type="appl" />
         <tli id="T844" time="524.5163461538461" type="appl" />
         <tli id="T845" time="524.8190769230769" type="appl" />
         <tli id="T846" time="525.1218076923077" type="appl" />
         <tli id="T847" time="525.4245384615384" type="appl" />
         <tli id="T848" time="525.7272692307693" type="appl" />
         <tli id="T849" time="533.0933333333334" />
         <tli id="T850" time="533.6733333333333" />
         <tli id="T851" time="533.8283333333334" type="appl" />
         <tli id="T852" time="534.4666666666667" type="appl" />
         <tli id="T853" time="535.105" type="appl" />
         <tli id="T854" time="535.7433333333333" type="appl" />
         <tli id="T855" time="536.3816666666667" type="appl" />
         <tli id="T856" time="537.02" type="appl" />
         <tli id="T857" time="537.1266666666667" />
         <tli id="T858" time="537.547" type="appl" />
         <tli id="T859" time="537.994" type="appl" />
         <tli id="T860" time="538.441" type="appl" />
         <tli id="T861" time="538.888" type="appl" />
         <tli id="T862" time="539.335" type="appl" />
         <tli id="T863" time="539.782" type="appl" />
         <tli id="T864" time="540.229" type="appl" />
         <tli id="T865" time="540.676" type="appl" />
         <tli id="T866" time="541.123" type="appl" />
         <tli id="T867" time="541.57" type="appl" />
         <tli id="T0" time="550.0" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="BeES"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T15" id="Seg_0" n="sc" s="T1">
               <ts e="T15" id="Seg_2" n="HIAT:u" s="T1">
                  <ts e="T2" id="Seg_4" n="HIAT:w" s="T1">Köhö</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_6" n="HIAT:ip">(</nts>
                  <ts e="T3" id="Seg_8" n="HIAT:w" s="T2">hɨl-</ts>
                  <nts id="Seg_9" n="HIAT:ip">)</nts>
                  <nts id="Seg_10" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_12" n="HIAT:w" s="T3">kɨhɨnɨ</ts>
                  <nts id="Seg_13" n="HIAT:ip">,</nts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">kɨhɨnɨ</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">meldʼi</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">köhö</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">hɨldʼan</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">barannar</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">hakalar</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_34" n="HIAT:w" s="T10">haːs</ts>
                  <nts id="Seg_35" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_37" n="HIAT:w" s="T11">toktuːr</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_40" n="HIAT:w" s="T12">hirdere</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_43" n="HIAT:w" s="T13">baːr</ts>
                  <nts id="Seg_44" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_46" n="HIAT:w" s="T14">bu͡olaːččɨ</ts>
                  <nts id="Seg_47" n="HIAT:ip">.</nts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T24" id="Seg_49" n="sc" s="T16">
               <ts e="T24" id="Seg_51" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_53" n="HIAT:w" s="T16">Onno</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_56" n="HIAT:w" s="T17">ügüs</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_59" n="HIAT:w" s="T18">bagajɨ</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_62" n="HIAT:w" s="T19">uraha</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_65" n="HIAT:w" s="T20">dʼi͡eler</ts>
                  <nts id="Seg_66" n="HIAT:ip">,</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_69" n="HIAT:w" s="T21">hɨrga</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_72" n="HIAT:w" s="T22">dʼi͡eler</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_75" n="HIAT:w" s="T23">turaːččɨlar</ts>
                  <nts id="Seg_76" n="HIAT:ip">.</nts>
                  <nts id="Seg_77" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T35" id="Seg_78" n="sc" s="T25">
               <ts e="T35" id="Seg_80" n="HIAT:u" s="T25">
                  <ts e="T26" id="Seg_82" n="HIAT:w" s="T25">Haːskɨ</ts>
                  <nts id="Seg_83" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_85" n="HIAT:w" s="T26">hɨlaːs</ts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_88" n="HIAT:w" s="T27">küttere</ts>
                  <nts id="Seg_89" n="HIAT:ip">,</nts>
                  <nts id="Seg_90" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_92" n="HIAT:w" s="T28">kün</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_95" n="HIAT:w" s="T29">tɨgar</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_98" n="HIAT:w" s="T30">bu͡ollagɨna</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_101" n="HIAT:w" s="T31">de</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_104" n="HIAT:w" s="T32">dʼaktattar</ts>
                  <nts id="Seg_105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_107" n="HIAT:w" s="T33">üleliːllere</ts>
                  <nts id="Seg_108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_110" n="HIAT:w" s="T34">üksüːr</ts>
                  <nts id="Seg_111" n="HIAT:ip">.</nts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T43" id="Seg_113" n="sc" s="T36">
               <ts e="T43" id="Seg_115" n="HIAT:u" s="T36">
                  <ts e="T37" id="Seg_117" n="HIAT:w" s="T36">Tahaːra</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_120" n="HIAT:w" s="T37">ginileriŋ</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_123" n="HIAT:w" s="T38">maŋnaj</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_126" n="HIAT:w" s="T39">iti</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_129" n="HIAT:w" s="T40">tiriː</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_132" n="HIAT:w" s="T41">ületin</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_135" n="HIAT:w" s="T42">üleliːller</ts>
                  <nts id="Seg_136" n="HIAT:ip">.</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T50" id="Seg_138" n="sc" s="T44">
               <ts e="T50" id="Seg_140" n="HIAT:u" s="T44">
                  <ts e="T45" id="Seg_142" n="HIAT:w" s="T44">Tiriː</ts>
                  <nts id="Seg_143" n="HIAT:ip">,</nts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_146" n="HIAT:w" s="T45">tiriːgin</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_149" n="HIAT:w" s="T46">bert</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_152" n="HIAT:w" s="T47">delbi</ts>
                  <nts id="Seg_153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_155" n="HIAT:w" s="T48">ɨraːstɨːllar</ts>
                  <nts id="Seg_156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_158" n="HIAT:w" s="T49">maŋnaj</ts>
                  <nts id="Seg_159" n="HIAT:ip">.</nts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T57" id="Seg_161" n="sc" s="T51">
               <ts e="T57" id="Seg_163" n="HIAT:u" s="T51">
                  <ts e="T52" id="Seg_165" n="HIAT:w" s="T51">Ɨraːstan</ts>
                  <nts id="Seg_166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_168" n="HIAT:w" s="T52">barannar</ts>
                  <nts id="Seg_169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_171" n="HIAT:w" s="T53">kɨptɨjɨnan</ts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_174" n="HIAT:w" s="T54">kɨrɨjallar</ts>
                  <nts id="Seg_175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_176" n="HIAT:ip">(</nts>
                  <ts e="T56" id="Seg_178" n="HIAT:w" s="T55">ütr-</ts>
                  <nts id="Seg_179" n="HIAT:ip">)</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_182" n="HIAT:w" s="T56">tüːtün</ts>
                  <nts id="Seg_183" n="HIAT:ip">.</nts>
                  <nts id="Seg_184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T67" id="Seg_185" n="sc" s="T58">
               <ts e="T67" id="Seg_187" n="HIAT:u" s="T58">
                  <ts e="T59" id="Seg_189" n="HIAT:w" s="T58">Ol</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_192" n="HIAT:w" s="T59">kɨptɨjɨnan</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_195" n="HIAT:w" s="T60">kɨrɨjan</ts>
                  <nts id="Seg_196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_198" n="HIAT:w" s="T61">büttekterine</ts>
                  <nts id="Seg_199" n="HIAT:ip">,</nts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_202" n="HIAT:w" s="T62">onton</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_205" n="HIAT:w" s="T63">kojut</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_208" n="HIAT:w" s="T64">bahagɨnan</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_211" n="HIAT:w" s="T65">emi͡e</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_214" n="HIAT:w" s="T66">kɨrɨjallar</ts>
                  <nts id="Seg_215" n="HIAT:ip">.</nts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T76" id="Seg_217" n="sc" s="T68">
               <ts e="T76" id="Seg_219" n="HIAT:u" s="T68">
                  <ts e="T69" id="Seg_221" n="HIAT:w" s="T68">Öldüːn</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_224" n="HIAT:w" s="T69">hakalar</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_227" n="HIAT:w" s="T70">olus</ts>
                  <nts id="Seg_228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_230" n="HIAT:w" s="T71">köp</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_233" n="HIAT:w" s="T72">kimi</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_236" n="HIAT:w" s="T73">tiriːni</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_239" n="HIAT:w" s="T74">ɨlaːččɨta</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_242" n="HIAT:w" s="T75">hu͡oktar</ts>
                  <nts id="Seg_243" n="HIAT:ip">.</nts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T93" id="Seg_245" n="sc" s="T77">
               <ts e="T93" id="Seg_247" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_249" n="HIAT:w" s="T77">Kɨrɨllɨbɨt</ts>
                  <nts id="Seg_250" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_252" n="HIAT:w" s="T78">iti</ts>
                  <nts id="Seg_253" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_255" n="HIAT:w" s="T79">tüːtün</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_258" n="HIAT:w" s="T80">ɨlɨllɨbɨttan</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_260" n="HIAT:ip">(</nts>
                  <ts e="T82" id="Seg_262" n="HIAT:w" s="T81">kɨrpa-</ts>
                  <nts id="Seg_263" n="HIAT:ip">)</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_266" n="HIAT:w" s="T82">kɨrpalaːbɨt</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_269" n="HIAT:w" s="T83">tüːnü</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_272" n="HIAT:w" s="T84">kimi</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_275" n="HIAT:w" s="T85">tiriːni</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_278" n="HIAT:w" s="T86">ɨlannar</ts>
                  <nts id="Seg_279" n="HIAT:ip">,</nts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_282" n="HIAT:w" s="T87">kim</ts>
                  <nts id="Seg_283" n="HIAT:ip">,</nts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_286" n="HIAT:w" s="T88">dʼi͡e</ts>
                  <nts id="Seg_287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_289" n="HIAT:w" s="T89">habɨːta</ts>
                  <nts id="Seg_290" n="HIAT:ip">,</nts>
                  <nts id="Seg_291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_293" n="HIAT:w" s="T90">iti</ts>
                  <nts id="Seg_294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_296" n="HIAT:w" s="T91">öldüːtteri</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_299" n="HIAT:w" s="T92">tigeːččiler</ts>
                  <nts id="Seg_300" n="HIAT:ip">.</nts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T102" id="Seg_302" n="sc" s="T94">
               <ts e="T102" id="Seg_304" n="HIAT:u" s="T94">
                  <ts e="T95" id="Seg_306" n="HIAT:w" s="T94">Onnuk</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_309" n="HIAT:w" s="T95">bu͡ollagɨna</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_312" n="HIAT:w" s="T96">de</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_315" n="HIAT:w" s="T97">ol</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_318" n="HIAT:w" s="T98">barbak</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_321" n="HIAT:w" s="T99">tüːnü</ts>
                  <nts id="Seg_322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_324" n="HIAT:w" s="T100">keːheller</ts>
                  <nts id="Seg_325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_327" n="HIAT:w" s="T101">bu͡o</ts>
                  <nts id="Seg_328" n="HIAT:ip">.</nts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T107" id="Seg_330" n="sc" s="T103">
               <ts e="T107" id="Seg_332" n="HIAT:u" s="T103">
                  <ts e="T104" id="Seg_334" n="HIAT:w" s="T103">Onton</ts>
                  <nts id="Seg_335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_337" n="HIAT:w" s="T104">ontugunan</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_340" n="HIAT:w" s="T105">öldüːn</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_343" n="HIAT:w" s="T106">tigeller</ts>
                  <nts id="Seg_344" n="HIAT:ip">.</nts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T116" id="Seg_346" n="sc" s="T108">
               <ts e="T116" id="Seg_348" n="HIAT:u" s="T108">
                  <ts e="T109" id="Seg_350" n="HIAT:w" s="T108">Ol</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_352" n="HIAT:ip">(</nts>
                  <ts e="T110" id="Seg_354" n="HIAT:w" s="T109">öl-</ts>
                  <nts id="Seg_355" n="HIAT:ip">)</nts>
                  <nts id="Seg_356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_358" n="HIAT:w" s="T110">horok</ts>
                  <nts id="Seg_359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_361" n="HIAT:w" s="T111">tiriːni</ts>
                  <nts id="Seg_362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_364" n="HIAT:w" s="T112">ginileriŋ</ts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_367" n="HIAT:w" s="T113">iti</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_370" n="HIAT:w" s="T114">harɨː</ts>
                  <nts id="Seg_371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_373" n="HIAT:w" s="T115">oŋoru͡oktaːktar</ts>
                  <nts id="Seg_374" n="HIAT:ip">.</nts>
                  <nts id="Seg_375" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T124" id="Seg_376" n="sc" s="T117">
               <ts e="T124" id="Seg_378" n="HIAT:u" s="T117">
                  <ts e="T118" id="Seg_380" n="HIAT:w" s="T117">Harɨː</ts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_383" n="HIAT:w" s="T118">oŋorollorugar</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_386" n="HIAT:w" s="T119">itinne</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_389" n="HIAT:w" s="T120">de</ts>
                  <nts id="Seg_390" n="HIAT:ip">,</nts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_393" n="HIAT:w" s="T121">hin</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_396" n="HIAT:w" s="T122">ügüs</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_399" n="HIAT:w" s="T123">üle</ts>
                  <nts id="Seg_400" n="HIAT:ip">:</nts>
                  <nts id="Seg_401" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T141" id="Seg_402" n="sc" s="T125">
               <ts e="T141" id="Seg_404" n="HIAT:u" s="T125">
                  <ts e="T126" id="Seg_406" n="HIAT:w" s="T125">Kɨrpalɨːllar</ts>
                  <nts id="Seg_407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_409" n="HIAT:w" s="T126">bu͡o</ts>
                  <nts id="Seg_410" n="HIAT:ip">,</nts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_413" n="HIAT:w" s="T127">iti</ts>
                  <nts id="Seg_414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_416" n="HIAT:w" s="T128">kɨrpalaːbɨttarɨn</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_419" n="HIAT:w" s="T129">kördük</ts>
                  <nts id="Seg_420" n="HIAT:ip">,</nts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_423" n="HIAT:w" s="T130">tüːte</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_426" n="HIAT:w" s="T131">hu͡ok</ts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_429" n="HIAT:w" s="T132">bu͡o</ts>
                  <nts id="Seg_430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_432" n="HIAT:w" s="T133">tüːte</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_435" n="HIAT:w" s="T134">agɨjak</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_438" n="HIAT:w" s="T135">bu͡olu͡on</ts>
                  <nts id="Seg_439" n="HIAT:ip">,</nts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_442" n="HIAT:w" s="T136">hin</ts>
                  <nts id="Seg_443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_445" n="HIAT:w" s="T137">tüːte</ts>
                  <nts id="Seg_446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_448" n="HIAT:w" s="T138">kaːlar</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_451" n="HIAT:w" s="T139">bu͡o</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_454" n="HIAT:w" s="T140">tiriːger</ts>
                  <nts id="Seg_455" n="HIAT:ip">.</nts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T163" id="Seg_457" n="sc" s="T142">
               <ts e="T163" id="Seg_459" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_461" n="HIAT:w" s="T142">Ol</ts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_464" n="HIAT:w" s="T143">tüː</ts>
                  <nts id="Seg_465" n="HIAT:ip">,</nts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_468" n="HIAT:w" s="T144">tüːŋ</ts>
                  <nts id="Seg_469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_471" n="HIAT:w" s="T145">üčügejdik</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_474" n="HIAT:w" s="T146">barɨta</ts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_477" n="HIAT:w" s="T147">tühü͡ön</ts>
                  <nts id="Seg_478" n="HIAT:ip">,</nts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_481" n="HIAT:w" s="T148">onton</ts>
                  <nts id="Seg_482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_484" n="HIAT:w" s="T149">kojut</ts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_487" n="HIAT:w" s="T150">imiterge</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_490" n="HIAT:w" s="T151">da</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_493" n="HIAT:w" s="T152">üčügej</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_496" n="HIAT:w" s="T153">bu͡o</ts>
                  <nts id="Seg_497" n="HIAT:ip">,</nts>
                  <nts id="Seg_498" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_500" n="HIAT:w" s="T154">harɨː</ts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_503" n="HIAT:w" s="T155">bu͡olu͡on</ts>
                  <nts id="Seg_504" n="HIAT:ip">,</nts>
                  <nts id="Seg_505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_507" n="HIAT:w" s="T156">ol</ts>
                  <nts id="Seg_508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_510" n="HIAT:w" s="T157">tüːleːk</ts>
                  <nts id="Seg_511" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_513" n="HIAT:w" s="T158">kim</ts>
                  <nts id="Seg_514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_516" n="HIAT:w" s="T159">tiriːni</ts>
                  <nts id="Seg_517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_519" n="HIAT:w" s="T160">ginileriŋ</ts>
                  <nts id="Seg_520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_522" n="HIAT:w" s="T161">hɨtɨtallar</ts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_525" n="HIAT:w" s="T162">uːga</ts>
                  <nts id="Seg_526" n="HIAT:ip">.</nts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T169" id="Seg_528" n="sc" s="T164">
               <ts e="T169" id="Seg_530" n="HIAT:u" s="T164">
                  <ts e="T165" id="Seg_532" n="HIAT:w" s="T164">Uː</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_535" n="HIAT:w" s="T165">ihiger</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_538" n="HIAT:w" s="T166">mɨlanɨ</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_541" n="HIAT:w" s="T167">kutallar</ts>
                  <nts id="Seg_542" n="HIAT:ip">,</nts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_545" n="HIAT:w" s="T168">uːrallar</ts>
                  <nts id="Seg_546" n="HIAT:ip">.</nts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T173" id="Seg_548" n="sc" s="T170">
               <ts e="T173" id="Seg_550" n="HIAT:u" s="T170">
                  <ts e="T171" id="Seg_552" n="HIAT:w" s="T170">Onton</ts>
                  <nts id="Seg_553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_555" n="HIAT:w" s="T171">eŋin-eŋin</ts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_558" n="HIAT:w" s="T172">ottoru</ts>
                  <nts id="Seg_559" n="HIAT:ip">.</nts>
                  <nts id="Seg_560" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T181" id="Seg_561" n="sc" s="T174">
               <ts e="T181" id="Seg_563" n="HIAT:u" s="T174">
                  <ts e="T175" id="Seg_565" n="HIAT:w" s="T174">Ol</ts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_568" n="HIAT:w" s="T175">gɨna</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_571" n="HIAT:w" s="T176">gɨna</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_574" n="HIAT:w" s="T177">kas</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_577" n="HIAT:w" s="T178">da</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_580" n="HIAT:w" s="T179">künü</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_583" n="HIAT:w" s="T180">hɨtɨ͡aktaːk</ts>
                  <nts id="Seg_584" n="HIAT:ip">.</nts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T197" id="Seg_586" n="sc" s="T182">
               <ts e="T197" id="Seg_588" n="HIAT:u" s="T182">
                  <ts e="T183" id="Seg_590" n="HIAT:w" s="T182">Itiː</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_593" n="HIAT:w" s="T183">da</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_596" n="HIAT:w" s="T184">kün</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_599" n="HIAT:w" s="T185">bu͡ollun</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_602" n="HIAT:w" s="T186">ol</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_605" n="HIAT:w" s="T187">uːŋ</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_608" n="HIAT:w" s="T188">hɨtɨjar</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_611" n="HIAT:w" s="T189">bu͡o</ts>
                  <nts id="Seg_612" n="HIAT:ip">,</nts>
                  <nts id="Seg_613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_615" n="HIAT:w" s="T190">tiriːŋ</ts>
                  <nts id="Seg_616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_618" n="HIAT:w" s="T191">emi͡e</ts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_621" n="HIAT:w" s="T192">hɨtɨjar</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_624" n="HIAT:w" s="T193">bu͡o</ts>
                  <nts id="Seg_625" n="HIAT:ip">,</nts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_628" n="HIAT:w" s="T194">imiges</ts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_631" n="HIAT:w" s="T195">bu͡olar</ts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_634" n="HIAT:w" s="T196">bu͡o</ts>
                  <nts id="Seg_635" n="HIAT:ip">.</nts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T207" id="Seg_637" n="sc" s="T198">
               <ts e="T207" id="Seg_639" n="HIAT:u" s="T198">
                  <ts e="T199" id="Seg_641" n="HIAT:w" s="T198">Tiriːŋ</ts>
                  <nts id="Seg_642" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_644" n="HIAT:w" s="T199">kas</ts>
                  <nts id="Seg_645" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_647" n="HIAT:w" s="T200">da</ts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_650" n="HIAT:w" s="T201">künü</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_653" n="HIAT:w" s="T202">hɨtɨjan</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_656" n="HIAT:w" s="T203">büttegine</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_659" n="HIAT:w" s="T204">ginileriŋ</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_662" n="HIAT:w" s="T205">ol</ts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_665" n="HIAT:w" s="T206">bulkujallar</ts>
                  <nts id="Seg_666" n="HIAT:ip">.</nts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T217" id="Seg_668" n="sc" s="T208">
               <ts e="T217" id="Seg_670" n="HIAT:u" s="T208">
                  <ts e="T209" id="Seg_672" n="HIAT:w" s="T208">Ol</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_675" n="HIAT:w" s="T209">tiriːgin</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_678" n="HIAT:w" s="T210">ɨlan</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_681" n="HIAT:w" s="T211">barannar</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_684" n="HIAT:w" s="T212">iti</ts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_687" n="HIAT:w" s="T213">kim</ts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_690" n="HIAT:w" s="T214">huːnarallar</ts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_693" n="HIAT:w" s="T215">ürekke</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_696" n="HIAT:w" s="T216">barannar</ts>
                  <nts id="Seg_697" n="HIAT:ip">.</nts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T224" id="Seg_699" n="sc" s="T218">
               <ts e="T224" id="Seg_701" n="HIAT:u" s="T218">
                  <ts e="T219" id="Seg_703" n="HIAT:w" s="T218">Delbi</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_705" n="HIAT:ip">(</nts>
                  <ts e="T220" id="Seg_707" n="HIAT:w" s="T219">huːnnaraldɨlar</ts>
                  <nts id="Seg_708" n="HIAT:ip">)</nts>
                  <nts id="Seg_709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_711" n="HIAT:w" s="T220">ol</ts>
                  <nts id="Seg_712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_714" n="HIAT:w" s="T221">gɨnan</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_717" n="HIAT:w" s="T222">baran</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_720" n="HIAT:w" s="T223">kuːrdallar</ts>
                  <nts id="Seg_721" n="HIAT:ip">.</nts>
                  <nts id="Seg_722" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T240" id="Seg_723" n="sc" s="T225">
               <ts e="T240" id="Seg_725" n="HIAT:u" s="T225">
                  <ts e="T226" id="Seg_727" n="HIAT:w" s="T225">Kuːrdan</ts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_730" n="HIAT:w" s="T226">barannara</ts>
                  <nts id="Seg_731" n="HIAT:ip">,</nts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_734" n="HIAT:w" s="T227">iti</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_737" n="HIAT:w" s="T228">tiriːgin</ts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_740" n="HIAT:w" s="T229">kimniːller</ts>
                  <nts id="Seg_741" n="HIAT:ip">,</nts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_744" n="HIAT:w" s="T230">gedereːninen</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_747" n="HIAT:w" s="T231">ol</ts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_750" n="HIAT:w" s="T232">kim</ts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_753" n="HIAT:w" s="T233">kaːlbɨt</ts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_756" n="HIAT:w" s="T234">tüːlerin</ts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_759" n="HIAT:w" s="T235">ɨlallar</ts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_761" n="HIAT:ip">(</nts>
                  <ts e="T237" id="Seg_763" n="HIAT:w" s="T236">gedereː-</ts>
                  <nts id="Seg_764" n="HIAT:ip">)</nts>
                  <nts id="Seg_765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_767" n="HIAT:w" s="T237">gedereːnniːller</ts>
                  <nts id="Seg_768" n="HIAT:ip">,</nts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_771" n="HIAT:w" s="T238">hakalɨː</ts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_774" n="HIAT:w" s="T239">haŋardakka</ts>
                  <nts id="Seg_775" n="HIAT:ip">.</nts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T257" id="Seg_777" n="sc" s="T241">
               <ts e="T257" id="Seg_779" n="HIAT:u" s="T241">
                  <ts e="T242" id="Seg_781" n="HIAT:w" s="T241">Ontuŋ</ts>
                  <nts id="Seg_782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_784" n="HIAT:w" s="T242">emi͡e</ts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_787" n="HIAT:w" s="T243">türgen</ts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_790" n="HIAT:w" s="T244">da</ts>
                  <nts id="Seg_791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_793" n="HIAT:w" s="T245">bu͡olbatak</ts>
                  <nts id="Seg_794" n="HIAT:ip">,</nts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_797" n="HIAT:w" s="T246">dʼogus</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_800" n="HIAT:w" s="T247">da</ts>
                  <nts id="Seg_801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_803" n="HIAT:w" s="T248">bu͡olbatak</ts>
                  <nts id="Seg_804" n="HIAT:ip">,</nts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_807" n="HIAT:w" s="T249">dʼaktattar</ts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_810" n="HIAT:w" s="T250">bert</ts>
                  <nts id="Seg_811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_813" n="HIAT:w" s="T251">kim</ts>
                  <nts id="Seg_814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_816" n="HIAT:w" s="T252">ɨ͡arakan</ts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_819" n="HIAT:w" s="T253">üle</ts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_822" n="HIAT:w" s="T254">iti</ts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_825" n="HIAT:w" s="T255">dʼaktarga</ts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_828" n="HIAT:w" s="T256">da</ts>
                  <nts id="Seg_829" n="HIAT:ip">.</nts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T270" id="Seg_831" n="sc" s="T258">
               <ts e="T270" id="Seg_833" n="HIAT:u" s="T258">
                  <ts e="T259" id="Seg_835" n="HIAT:w" s="T258">Ol</ts>
                  <nts id="Seg_836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_838" n="HIAT:w" s="T259">ihin</ts>
                  <nts id="Seg_839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_841" n="HIAT:w" s="T260">biːrde</ts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_844" n="HIAT:w" s="T261">oloroːt</ts>
                  <nts id="Seg_845" n="HIAT:ip">,</nts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_848" n="HIAT:w" s="T262">onu</ts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_851" n="HIAT:w" s="T263">barɨtɨn</ts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_853" n="HIAT:ip">(</nts>
                  <ts e="T265" id="Seg_855" n="HIAT:w" s="T264">hataːba-</ts>
                  <nts id="Seg_856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_858" n="HIAT:w" s="T265">oloru͡oktaː-</ts>
                  <nts id="Seg_859" n="HIAT:ip">)</nts>
                  <nts id="Seg_860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_862" n="HIAT:w" s="T266">üleli͡ektere</ts>
                  <nts id="Seg_863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_865" n="HIAT:w" s="T267">hu͡oga</ts>
                  <nts id="Seg_866" n="HIAT:ip">,</nts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_869" n="HIAT:w" s="T268">ɨ͡arakan</ts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_872" n="HIAT:w" s="T269">üle</ts>
                  <nts id="Seg_873" n="HIAT:ip">.</nts>
                  <nts id="Seg_874" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T279" id="Seg_875" n="sc" s="T271">
               <ts e="T279" id="Seg_877" n="HIAT:u" s="T271">
                  <ts e="T272" id="Seg_879" n="HIAT:w" s="T271">Onton</ts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_882" n="HIAT:w" s="T272">nöŋü͡ö</ts>
                  <nts id="Seg_883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_885" n="HIAT:w" s="T273">kün</ts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_888" n="HIAT:w" s="T274">emi͡e</ts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_891" n="HIAT:w" s="T275">oŋoru͡oktara</ts>
                  <nts id="Seg_892" n="HIAT:ip">,</nts>
                  <nts id="Seg_893" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_895" n="HIAT:w" s="T276">ol</ts>
                  <nts id="Seg_896" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_898" n="HIAT:w" s="T277">gedereːnni͡ektere</ts>
                  <nts id="Seg_899" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_901" n="HIAT:w" s="T278">emi͡e</ts>
                  <nts id="Seg_902" n="HIAT:ip">.</nts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T292" id="Seg_904" n="sc" s="T280">
               <ts e="T292" id="Seg_906" n="HIAT:u" s="T280">
                  <ts e="T281" id="Seg_908" n="HIAT:w" s="T280">Gedereːnnen</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_911" n="HIAT:w" s="T281">büttekterine</ts>
                  <nts id="Seg_912" n="HIAT:ip">,</nts>
                  <nts id="Seg_913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_915" n="HIAT:w" s="T282">ol</ts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_918" n="HIAT:w" s="T283">harɨːŋ</ts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_921" n="HIAT:w" s="T284">kim</ts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_924" n="HIAT:w" s="T285">kime</ts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_927" n="HIAT:w" s="T286">hu͡ok</ts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_930" n="HIAT:w" s="T287">bu͡olar</ts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_933" n="HIAT:w" s="T288">bu͡o</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_936" n="HIAT:w" s="T289">tüːte</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_939" n="HIAT:w" s="T290">hu͡ok</ts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_942" n="HIAT:w" s="T291">bu͡olar</ts>
                  <nts id="Seg_943" n="HIAT:ip">.</nts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T304" id="Seg_945" n="sc" s="T293">
               <ts e="T304" id="Seg_947" n="HIAT:u" s="T293">
                  <ts e="T294" id="Seg_949" n="HIAT:w" s="T293">Ontuŋ</ts>
                  <nts id="Seg_950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_952" n="HIAT:w" s="T294">deksi</ts>
                  <nts id="Seg_953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_955" n="HIAT:w" s="T295">bu͡olu͡on</ts>
                  <nts id="Seg_956" n="HIAT:ip">,</nts>
                  <nts id="Seg_957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_959" n="HIAT:w" s="T296">iti</ts>
                  <nts id="Seg_960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_962" n="HIAT:w" s="T297">kɨhɨ͡agɨnan</ts>
                  <nts id="Seg_963" n="HIAT:ip">,</nts>
                  <nts id="Seg_964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_966" n="HIAT:w" s="T298">tu͡ogɨnan</ts>
                  <nts id="Seg_967" n="HIAT:ip">,</nts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_970" n="HIAT:w" s="T299">eŋin</ts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_973" n="HIAT:w" s="T300">eŋininen</ts>
                  <nts id="Seg_974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_976" n="HIAT:w" s="T301">tarbɨːllar</ts>
                  <nts id="Seg_977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_979" n="HIAT:w" s="T302">kɨhɨ͡aktɨːllar</ts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_982" n="HIAT:w" s="T303">kanʼɨːllar</ts>
                  <nts id="Seg_983" n="HIAT:ip">.</nts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T311" id="Seg_985" n="sc" s="T305">
               <ts e="T311" id="Seg_987" n="HIAT:u" s="T305">
                  <ts e="T306" id="Seg_989" n="HIAT:w" s="T305">Bahagɨnan</ts>
                  <nts id="Seg_990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_992" n="HIAT:w" s="T306">da</ts>
                  <nts id="Seg_993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_995" n="HIAT:w" s="T307">eŋin</ts>
                  <nts id="Seg_996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_998" n="HIAT:w" s="T308">eŋin</ts>
                  <nts id="Seg_999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1001" n="HIAT:w" s="T309">deksiliːller</ts>
                  <nts id="Seg_1002" n="HIAT:ip">,</nts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1005" n="HIAT:w" s="T310">kanʼɨːllar</ts>
                  <nts id="Seg_1006" n="HIAT:ip">.</nts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T319" id="Seg_1008" n="sc" s="T312">
               <ts e="T319" id="Seg_1010" n="HIAT:u" s="T312">
                  <ts e="T313" id="Seg_1012" n="HIAT:w" s="T312">Tu͡ok</ts>
                  <nts id="Seg_1013" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1015" n="HIAT:w" s="T313">kuhagan</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1018" n="HIAT:w" s="T314">baːrɨn</ts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1021" n="HIAT:w" s="T315">barɨtɨn</ts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1024" n="HIAT:w" s="T316">iti</ts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1027" n="HIAT:w" s="T317">ɨlattan</ts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1030" n="HIAT:w" s="T318">iheller</ts>
                  <nts id="Seg_1031" n="HIAT:ip">.</nts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T329" id="Seg_1033" n="sc" s="T320">
               <ts e="T329" id="Seg_1035" n="HIAT:u" s="T320">
                  <ts e="T321" id="Seg_1037" n="HIAT:w" s="T320">Ol</ts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1040" n="HIAT:w" s="T321">harɨːŋ</ts>
                  <nts id="Seg_1041" n="HIAT:ip">,</nts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1044" n="HIAT:w" s="T322">ol</ts>
                  <nts id="Seg_1045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1047" n="HIAT:w" s="T323">gɨnan</ts>
                  <nts id="Seg_1048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1050" n="HIAT:w" s="T324">baran</ts>
                  <nts id="Seg_1051" n="HIAT:ip">,</nts>
                  <nts id="Seg_1052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1054" n="HIAT:w" s="T325">kördökkö</ts>
                  <nts id="Seg_1055" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1057" n="HIAT:w" s="T326">üčügej</ts>
                  <nts id="Seg_1058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1060" n="HIAT:w" s="T327">bagaj</ts>
                  <nts id="Seg_1061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1063" n="HIAT:w" s="T328">bu͡olar</ts>
                  <nts id="Seg_1064" n="HIAT:ip">.</nts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T343" id="Seg_1066" n="sc" s="T330">
               <ts e="T343" id="Seg_1068" n="HIAT:u" s="T330">
                  <ts e="T331" id="Seg_1070" n="HIAT:w" s="T330">Onton</ts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1073" n="HIAT:w" s="T331">harɨːŋ</ts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1076" n="HIAT:w" s="T332">ile</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1079" n="HIAT:w" s="T333">harɨː</ts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1082" n="HIAT:w" s="T334">bu͡olu͡on</ts>
                  <nts id="Seg_1083" n="HIAT:ip">,</nts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1086" n="HIAT:w" s="T335">ol</ts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1089" n="HIAT:w" s="T336">gɨnan</ts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1092" n="HIAT:w" s="T337">bu͡o</ts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1095" n="HIAT:w" s="T338">ɨːstaːk</ts>
                  <nts id="Seg_1096" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1098" n="HIAT:w" s="T339">bu͡olu͡on</ts>
                  <nts id="Seg_1099" n="HIAT:ip">,</nts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1102" n="HIAT:w" s="T340">ontugun</ts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1105" n="HIAT:w" s="T341">emi͡e</ts>
                  <nts id="Seg_1106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1108" n="HIAT:w" s="T342">imiti͡ekteːkter</ts>
                  <nts id="Seg_1109" n="HIAT:ip">.</nts>
                  <nts id="Seg_1110" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T353" id="Seg_1111" n="sc" s="T344">
               <ts e="T353" id="Seg_1113" n="HIAT:u" s="T344">
                  <ts e="T345" id="Seg_1115" n="HIAT:w" s="T344">Imiteːri</ts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1118" n="HIAT:w" s="T345">gɨnnaktarɨna</ts>
                  <nts id="Seg_1119" n="HIAT:ip">,</nts>
                  <nts id="Seg_1120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1122" n="HIAT:w" s="T346">de</ts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1125" n="HIAT:w" s="T347">kimniːller</ts>
                  <nts id="Seg_1126" n="HIAT:ip">,</nts>
                  <nts id="Seg_1127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1129" n="HIAT:w" s="T348">kɨːh</ts>
                  <nts id="Seg_1130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1132" n="HIAT:w" s="T349">ogolorun</ts>
                  <nts id="Seg_1133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1135" n="HIAT:w" s="T350">ɨgɨraːččɨlar</ts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1138" n="HIAT:w" s="T351">togo</ts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1141" n="HIAT:w" s="T352">ere</ts>
                  <nts id="Seg_1142" n="HIAT:ip">.</nts>
                  <nts id="Seg_1143" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T365" id="Seg_1144" n="sc" s="T354">
               <ts e="T365" id="Seg_1146" n="HIAT:u" s="T354">
                  <ts e="T355" id="Seg_1148" n="HIAT:w" s="T354">Iti</ts>
                  <nts id="Seg_1149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1151" n="HIAT:w" s="T355">kimi͡eke</ts>
                  <nts id="Seg_1152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1154" n="HIAT:w" s="T356">taŋas</ts>
                  <nts id="Seg_1155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1157" n="HIAT:w" s="T357">ɨjaːnar</ts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1160" n="HIAT:w" s="T358">kimnerge</ts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1163" n="HIAT:w" s="T359">ɨjɨːllar</ts>
                  <nts id="Seg_1164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1166" n="HIAT:w" s="T360">bu͡o</ts>
                  <nts id="Seg_1167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1169" n="HIAT:w" s="T361">ol</ts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1172" n="HIAT:w" s="T362">tiriːgin</ts>
                  <nts id="Seg_1173" n="HIAT:ip">,</nts>
                  <nts id="Seg_1174" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1176" n="HIAT:w" s="T363">taŋas</ts>
                  <nts id="Seg_1177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1179" n="HIAT:w" s="T364">ɨjaːnar</ts>
                  <nts id="Seg_1180" n="HIAT:ip">.</nts>
                  <nts id="Seg_1181" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T370" id="Seg_1182" n="sc" s="T366">
               <ts e="T370" id="Seg_1184" n="HIAT:u" s="T366">
                  <ts e="T367" id="Seg_1186" n="HIAT:w" s="T366">Ontugun</ts>
                  <nts id="Seg_1187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1189" n="HIAT:w" s="T367">battan</ts>
                  <nts id="Seg_1190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1192" n="HIAT:w" s="T368">turu͡okkun</ts>
                  <nts id="Seg_1193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1195" n="HIAT:w" s="T369">naːda</ts>
                  <nts id="Seg_1196" n="HIAT:ip">.</nts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T385" id="Seg_1198" n="sc" s="T371">
               <ts e="T385" id="Seg_1200" n="HIAT:u" s="T371">
                  <ts e="T372" id="Seg_1202" n="HIAT:w" s="T371">Hɨrganɨ</ts>
                  <nts id="Seg_1203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1205" n="HIAT:w" s="T372">ti͡ere</ts>
                  <nts id="Seg_1206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1208" n="HIAT:w" s="T373">uːran</ts>
                  <nts id="Seg_1209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1211" n="HIAT:w" s="T374">barannar</ts>
                  <nts id="Seg_1212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1214" n="HIAT:w" s="T375">emi͡e</ts>
                  <nts id="Seg_1215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1217" n="HIAT:w" s="T376">battan</ts>
                  <nts id="Seg_1218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1220" n="HIAT:w" s="T377">turu͡okkun</ts>
                  <nts id="Seg_1221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1223" n="HIAT:w" s="T378">naːda</ts>
                  <nts id="Seg_1224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1226" n="HIAT:w" s="T379">ol</ts>
                  <nts id="Seg_1227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1229" n="HIAT:w" s="T380">tiriːni</ts>
                  <nts id="Seg_1230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1232" n="HIAT:w" s="T381">uːran</ts>
                  <nts id="Seg_1233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1235" n="HIAT:w" s="T382">baraŋŋɨn</ts>
                  <nts id="Seg_1236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1238" n="HIAT:w" s="T383">harɨː</ts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1241" n="HIAT:w" s="T384">bu͡olu͡ogun</ts>
                  <nts id="Seg_1242" n="HIAT:ip">.</nts>
                  <nts id="Seg_1243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T390" id="Seg_1244" n="sc" s="T386">
               <ts e="T390" id="Seg_1246" n="HIAT:u" s="T386">
                  <ts e="T387" id="Seg_1248" n="HIAT:w" s="T386">De</ts>
                  <nts id="Seg_1249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1251" n="HIAT:w" s="T387">ol</ts>
                  <nts id="Seg_1252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1254" n="HIAT:w" s="T388">battan</ts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1257" n="HIAT:w" s="T389">turagɨn</ts>
                  <nts id="Seg_1258" n="HIAT:ip">.</nts>
                  <nts id="Seg_1259" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T417" id="Seg_1260" n="sc" s="T391">
               <ts e="T402" id="Seg_1262" n="HIAT:u" s="T391">
                  <ts e="T392" id="Seg_1264" n="HIAT:w" s="T391">Ol</ts>
                  <nts id="Seg_1265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1267" n="HIAT:w" s="T392">battan</ts>
                  <nts id="Seg_1268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1270" n="HIAT:w" s="T393">turdakkɨna</ts>
                  <nts id="Seg_1271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1273" n="HIAT:w" s="T394">eni͡eke</ts>
                  <nts id="Seg_1274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1276" n="HIAT:w" s="T395">bi͡ereller</ts>
                  <nts id="Seg_1277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1279" n="HIAT:w" s="T396">iti</ts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1282" n="HIAT:w" s="T397">kimi</ts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1285" n="HIAT:w" s="T398">bɨ͡arɨ</ts>
                  <nts id="Seg_1286" n="HIAT:ip">,</nts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1289" n="HIAT:w" s="T399">busput</ts>
                  <nts id="Seg_1290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1292" n="HIAT:w" s="T400">taba</ts>
                  <nts id="Seg_1293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1295" n="HIAT:w" s="T401">bɨ͡arɨn</ts>
                  <nts id="Seg_1296" n="HIAT:ip">.</nts>
                  <nts id="Seg_1297" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T411" id="Seg_1299" n="HIAT:u" s="T402">
                  <ts e="T403" id="Seg_1301" n="HIAT:w" s="T402">Ol</ts>
                  <nts id="Seg_1302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1304" n="HIAT:w" s="T403">bɨ͡arɨ</ts>
                  <nts id="Seg_1305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1307" n="HIAT:w" s="T404">battɨː</ts>
                  <nts id="Seg_1308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1310" n="HIAT:w" s="T405">turan</ts>
                  <nts id="Seg_1311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1313" n="HIAT:w" s="T406">kɨːs</ts>
                  <nts id="Seg_1314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1316" n="HIAT:w" s="T407">kɨːs</ts>
                  <nts id="Seg_1317" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1319" n="HIAT:w" s="T408">ogo</ts>
                  <nts id="Seg_1320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1321" n="HIAT:ip">(</nts>
                  <ts e="T410" id="Seg_1323" n="HIAT:w" s="T409">ɨstɨ͡an-</ts>
                  <nts id="Seg_1324" n="HIAT:ip">)</nts>
                  <nts id="Seg_1325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1327" n="HIAT:w" s="T410">ɨstɨ͡aktaːk</ts>
                  <nts id="Seg_1328" n="HIAT:ip">.</nts>
                  <nts id="Seg_1329" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T417" id="Seg_1331" n="HIAT:u" s="T411">
                  <ts e="T412" id="Seg_1333" n="HIAT:w" s="T411">Ɨstaː</ts>
                  <nts id="Seg_1334" n="HIAT:ip">,</nts>
                  <nts id="Seg_1335" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1337" n="HIAT:w" s="T412">diːller</ts>
                  <nts id="Seg_1338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1340" n="HIAT:w" s="T413">bu͡o</ts>
                  <nts id="Seg_1341" n="HIAT:ip">,</nts>
                  <nts id="Seg_1342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1344" n="HIAT:w" s="T414">kajdi͡ek</ts>
                  <nts id="Seg_1345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1347" n="HIAT:w" s="T415">bu͡olu͡oŋuj</ts>
                  <nts id="Seg_1348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1350" n="HIAT:w" s="T416">ke</ts>
                  <nts id="Seg_1351" n="HIAT:ip">?</nts>
                  <nts id="Seg_1352" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T427" id="Seg_1353" n="sc" s="T418">
               <ts e="T427" id="Seg_1355" n="HIAT:u" s="T418">
                  <ts e="T419" id="Seg_1357" n="HIAT:w" s="T418">Bagarbat</ts>
                  <nts id="Seg_1358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1360" n="HIAT:w" s="T419">bu͡olu͡o</ts>
                  <nts id="Seg_1361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1363" n="HIAT:w" s="T420">bu͡olu͡o</ts>
                  <nts id="Seg_1364" n="HIAT:ip">,</nts>
                  <nts id="Seg_1365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1367" n="HIAT:w" s="T421">ontuŋ</ts>
                  <nts id="Seg_1368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1370" n="HIAT:w" s="T422">amtana</ts>
                  <nts id="Seg_1371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1373" n="HIAT:w" s="T423">kuhagana</ts>
                  <nts id="Seg_1374" n="HIAT:ip">,</nts>
                  <nts id="Seg_1375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1377" n="HIAT:w" s="T424">hin</ts>
                  <nts id="Seg_1378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1380" n="HIAT:w" s="T425">turagɨn</ts>
                  <nts id="Seg_1381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1383" n="HIAT:w" s="T426">bu͡o</ts>
                  <nts id="Seg_1384" n="HIAT:ip">.</nts>
                  <nts id="Seg_1385" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T432" id="Seg_1386" n="sc" s="T428">
               <ts e="T432" id="Seg_1388" n="HIAT:u" s="T428">
                  <ts e="T429" id="Seg_1390" n="HIAT:w" s="T428">Istegin</ts>
                  <nts id="Seg_1391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1393" n="HIAT:w" s="T429">bu͡o</ts>
                  <nts id="Seg_1394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1396" n="HIAT:w" s="T430">inʼeŋ</ts>
                  <nts id="Seg_1397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1399" n="HIAT:w" s="T431">haŋatɨn</ts>
                  <nts id="Seg_1400" n="HIAT:ip">.</nts>
                  <nts id="Seg_1401" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T442" id="Seg_1402" n="sc" s="T433">
               <ts e="T442" id="Seg_1404" n="HIAT:u" s="T433">
                  <ts e="T434" id="Seg_1406" n="HIAT:w" s="T433">Ol</ts>
                  <nts id="Seg_1407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1409" n="HIAT:w" s="T434">tura</ts>
                  <nts id="Seg_1410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1412" n="HIAT:w" s="T435">tura</ts>
                  <nts id="Seg_1413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1415" n="HIAT:w" s="T436">ɨstɨː</ts>
                  <nts id="Seg_1416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1418" n="HIAT:w" s="T437">turagɨn</ts>
                  <nts id="Seg_1419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1421" n="HIAT:w" s="T438">ol</ts>
                  <nts id="Seg_1422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1424" n="HIAT:w" s="T439">kimi</ts>
                  <nts id="Seg_1425" n="HIAT:ip">,</nts>
                  <nts id="Seg_1426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1428" n="HIAT:w" s="T440">bɨ͡arɨ</ts>
                  <nts id="Seg_1429" n="HIAT:ip">,</nts>
                  <nts id="Seg_1430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1432" n="HIAT:w" s="T441">turaŋŋɨn</ts>
                  <nts id="Seg_1433" n="HIAT:ip">.</nts>
                  <nts id="Seg_1434" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T461" id="Seg_1435" n="sc" s="T443">
               <ts e="T461" id="Seg_1437" n="HIAT:u" s="T443">
                  <ts e="T444" id="Seg_1439" n="HIAT:w" s="T443">Ginileriŋ</ts>
                  <nts id="Seg_1440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1442" n="HIAT:w" s="T444">ol</ts>
                  <nts id="Seg_1443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1444" n="HIAT:ip">(</nts>
                  <ts e="T446" id="Seg_1446" n="HIAT:w" s="T445">bɨ͡a-</ts>
                  <nts id="Seg_1447" n="HIAT:ip">)</nts>
                  <nts id="Seg_1448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1450" n="HIAT:w" s="T446">ol</ts>
                  <nts id="Seg_1451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1453" n="HIAT:w" s="T447">ɨstaːbɨt</ts>
                  <nts id="Seg_1454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1456" n="HIAT:w" s="T448">bɨ͡argɨn</ts>
                  <nts id="Seg_1457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1459" n="HIAT:w" s="T449">ɨla</ts>
                  <nts id="Seg_1460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1462" n="HIAT:w" s="T450">ɨlallar</ts>
                  <nts id="Seg_1463" n="HIAT:ip">,</nts>
                  <nts id="Seg_1464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1466" n="HIAT:w" s="T451">ol</ts>
                  <nts id="Seg_1467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1469" n="HIAT:w" s="T452">tiriːgin</ts>
                  <nts id="Seg_1470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1472" n="HIAT:w" s="T453">ol</ts>
                  <nts id="Seg_1473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1475" n="HIAT:w" s="T454">kimneːbit</ts>
                  <nts id="Seg_1476" n="HIAT:ip">,</nts>
                  <nts id="Seg_1477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1479" n="HIAT:w" s="T455">gedereːnneːbit</ts>
                  <nts id="Seg_1480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1482" n="HIAT:w" s="T456">tiriːgin</ts>
                  <nts id="Seg_1483" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1485" n="HIAT:w" s="T457">ɨlan</ts>
                  <nts id="Seg_1486" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1488" n="HIAT:w" s="T458">barannar</ts>
                  <nts id="Seg_1489" n="HIAT:ip">,</nts>
                  <nts id="Seg_1490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1492" n="HIAT:w" s="T459">ol</ts>
                  <nts id="Seg_1493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1495" n="HIAT:w" s="T460">biheller</ts>
                  <nts id="Seg_1496" n="HIAT:ip">.</nts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T469" id="Seg_1498" n="sc" s="T462">
               <ts e="T469" id="Seg_1500" n="HIAT:u" s="T462">
                  <ts e="T463" id="Seg_1502" n="HIAT:w" s="T462">Ol</ts>
                  <nts id="Seg_1503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1505" n="HIAT:w" s="T463">gɨnan</ts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1508" n="HIAT:w" s="T464">baran</ts>
                  <nts id="Seg_1509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1510" n="HIAT:ip">(</nts>
                  <ts e="T466" id="Seg_1512" n="HIAT:w" s="T465">hoŋo-</ts>
                  <nts id="Seg_1513" n="HIAT:ip">)</nts>
                  <nts id="Seg_1514" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1516" n="HIAT:w" s="T466">hoŋohoːnunan</ts>
                  <nts id="Seg_1517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1519" n="HIAT:w" s="T467">hoŋohoːnnuːllar</ts>
                  <nts id="Seg_1520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1522" n="HIAT:w" s="T468">iliːlerinen</ts>
                  <nts id="Seg_1523" n="HIAT:ip">.</nts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T490" id="Seg_1525" n="sc" s="T470">
               <ts e="T490" id="Seg_1527" n="HIAT:u" s="T470">
                  <ts e="T471" id="Seg_1529" n="HIAT:w" s="T470">Biːr</ts>
                  <nts id="Seg_1530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1532" n="HIAT:w" s="T471">iliːnnen</ts>
                  <nts id="Seg_1533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1535" n="HIAT:w" s="T472">tutar</ts>
                  <nts id="Seg_1536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1538" n="HIAT:w" s="T473">ol</ts>
                  <nts id="Seg_1539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1541" n="HIAT:w" s="T474">tiriːgin</ts>
                  <nts id="Seg_1542" n="HIAT:ip">,</nts>
                  <nts id="Seg_1543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1545" n="HIAT:w" s="T475">biːr</ts>
                  <nts id="Seg_1546" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1548" n="HIAT:w" s="T476">iliːnnen</ts>
                  <nts id="Seg_1549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1551" n="HIAT:w" s="T477">hoŋohoːŋŋun</ts>
                  <nts id="Seg_1552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1554" n="HIAT:w" s="T478">tutar</ts>
                  <nts id="Seg_1555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1557" n="HIAT:w" s="T479">bu͡o</ts>
                  <nts id="Seg_1558" n="HIAT:ip">,</nts>
                  <nts id="Seg_1559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1561" n="HIAT:w" s="T480">ol</ts>
                  <nts id="Seg_1562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1564" n="HIAT:w" s="T481">bu͡o</ts>
                  <nts id="Seg_1565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1567" n="HIAT:w" s="T482">üːhetten</ts>
                  <nts id="Seg_1568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1570" n="HIAT:w" s="T483">allaraːga</ts>
                  <nts id="Seg_1571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1573" n="HIAT:w" s="T484">di͡eri</ts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1576" n="HIAT:w" s="T485">innʼe</ts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1579" n="HIAT:w" s="T486">hoŋohoːnnuːr</ts>
                  <nts id="Seg_1580" n="HIAT:ip">,</nts>
                  <nts id="Seg_1581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1583" n="HIAT:w" s="T487">eː</ts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1586" n="HIAT:w" s="T488">bihe</ts>
                  <nts id="Seg_1587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1589" n="HIAT:w" s="T489">bihe</ts>
                  <nts id="Seg_1590" n="HIAT:ip">.</nts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T503" id="Seg_1592" n="sc" s="T491">
               <ts e="T503" id="Seg_1594" n="HIAT:u" s="T491">
                  <ts e="T492" id="Seg_1596" n="HIAT:w" s="T491">De</ts>
                  <nts id="Seg_1597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1599" n="HIAT:w" s="T492">ol</ts>
                  <nts id="Seg_1600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1602" n="HIAT:w" s="T493">gɨnan</ts>
                  <nts id="Seg_1603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1605" n="HIAT:w" s="T494">baran</ts>
                  <nts id="Seg_1606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1608" n="HIAT:w" s="T495">ginileriŋ</ts>
                  <nts id="Seg_1609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1611" n="HIAT:w" s="T496">kɨːh</ts>
                  <nts id="Seg_1612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1614" n="HIAT:w" s="T497">ogolor</ts>
                  <nts id="Seg_1615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1617" n="HIAT:w" s="T498">kahan</ts>
                  <nts id="Seg_1618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1620" n="HIAT:w" s="T499">da</ts>
                  <nts id="Seg_1621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1623" n="HIAT:w" s="T500">turu͡oktarɨn</ts>
                  <nts id="Seg_1624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1626" n="HIAT:w" s="T501">bagaraːččɨta</ts>
                  <nts id="Seg_1627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1629" n="HIAT:w" s="T502">hu͡oktar</ts>
                  <nts id="Seg_1630" n="HIAT:ip">.</nts>
                  <nts id="Seg_1631" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T514" id="Seg_1632" n="sc" s="T504">
               <ts e="T514" id="Seg_1634" n="HIAT:u" s="T504">
                  <ts e="T505" id="Seg_1636" n="HIAT:w" s="T504">Battan</ts>
                  <nts id="Seg_1637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1639" n="HIAT:w" s="T505">turu͡okkun</ts>
                  <nts id="Seg_1640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1642" n="HIAT:w" s="T506">naːda</ts>
                  <nts id="Seg_1643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1645" n="HIAT:w" s="T507">bu͡o</ts>
                  <nts id="Seg_1646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1648" n="HIAT:w" s="T508">ör</ts>
                  <nts id="Seg_1649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1651" n="HIAT:w" s="T509">bagajɨ</ts>
                  <nts id="Seg_1652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1654" n="HIAT:w" s="T510">turu͡okkun</ts>
                  <nts id="Seg_1655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1656" n="HIAT:ip">(</nts>
                  <ts e="T512" id="Seg_1658" n="HIAT:w" s="T511">naː-</ts>
                  <nts id="Seg_1659" n="HIAT:ip">)</nts>
                  <nts id="Seg_1660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1662" n="HIAT:w" s="T512">turu͡oktaːkkɨn</ts>
                  <nts id="Seg_1663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1665" n="HIAT:w" s="T513">onno</ts>
                  <nts id="Seg_1666" n="HIAT:ip">.</nts>
                  <nts id="Seg_1667" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T522" id="Seg_1668" n="sc" s="T515">
               <ts e="T522" id="Seg_1670" n="HIAT:u" s="T515">
                  <ts e="T516" id="Seg_1672" n="HIAT:w" s="T515">A</ts>
                  <nts id="Seg_1673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1675" n="HIAT:w" s="T516">ɨstɨːrɨŋ</ts>
                  <nts id="Seg_1676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1678" n="HIAT:w" s="T517">baːr</ts>
                  <nts id="Seg_1679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1681" n="HIAT:w" s="T518">de</ts>
                  <nts id="Seg_1682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1684" n="HIAT:w" s="T519">kuhagan</ts>
                  <nts id="Seg_1685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1687" n="HIAT:w" s="T520">da</ts>
                  <nts id="Seg_1688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1690" n="HIAT:w" s="T521">kuhagan</ts>
                  <nts id="Seg_1691" n="HIAT:ip">.</nts>
                  <nts id="Seg_1692" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T532" id="Seg_1693" n="sc" s="T523">
               <ts e="T532" id="Seg_1695" n="HIAT:u" s="T523">
                  <ts e="T524" id="Seg_1697" n="HIAT:w" s="T523">Eː</ts>
                  <nts id="Seg_1698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1700" n="HIAT:w" s="T524">ɨstɨ͡aŋ</ts>
                  <nts id="Seg_1701" n="HIAT:ip">,</nts>
                  <nts id="Seg_1702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1704" n="HIAT:w" s="T525">kihi</ts>
                  <nts id="Seg_1705" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1707" n="HIAT:w" s="T526">da</ts>
                  <nts id="Seg_1708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1710" n="HIAT:w" s="T527">hürege</ts>
                  <nts id="Seg_1711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1713" n="HIAT:w" s="T528">loksuju͡ok</ts>
                  <nts id="Seg_1714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_1716" n="HIAT:w" s="T529">horogor</ts>
                  <nts id="Seg_1717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_1719" n="HIAT:w" s="T530">bu͡olar</ts>
                  <nts id="Seg_1720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_1722" n="HIAT:w" s="T531">bu͡o</ts>
                  <nts id="Seg_1723" n="HIAT:ip">.</nts>
                  <nts id="Seg_1724" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T543" id="Seg_1725" n="sc" s="T533">
               <ts e="T543" id="Seg_1727" n="HIAT:u" s="T533">
                  <ts e="T534" id="Seg_1729" n="HIAT:w" s="T533">Ol</ts>
                  <nts id="Seg_1730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_1732" n="HIAT:w" s="T534">ihin</ts>
                  <nts id="Seg_1733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1734" n="HIAT:ip">(</nts>
                  <ts e="T536" id="Seg_1736" n="HIAT:w" s="T535">kɨr-</ts>
                  <nts id="Seg_1737" n="HIAT:ip">)</nts>
                  <nts id="Seg_1738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_1740" n="HIAT:w" s="T536">kɨːh</ts>
                  <nts id="Seg_1741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_1743" n="HIAT:w" s="T537">ogokoːttoruŋ</ts>
                  <nts id="Seg_1744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1746" n="HIAT:w" s="T538">küreneller</ts>
                  <nts id="Seg_1747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1749" n="HIAT:w" s="T539">bu͡o</ts>
                  <nts id="Seg_1750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_1752" n="HIAT:w" s="T540">ɨstaːmaːrɨlar</ts>
                  <nts id="Seg_1753" n="HIAT:ip">,</nts>
                  <nts id="Seg_1754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_1756" n="HIAT:w" s="T541">baran</ts>
                  <nts id="Seg_1757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_1759" n="HIAT:w" s="T542">kaːlallar</ts>
                  <nts id="Seg_1760" n="HIAT:ip">.</nts>
                  <nts id="Seg_1761" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T553" id="Seg_1762" n="sc" s="T544">
               <ts e="T553" id="Seg_1764" n="HIAT:u" s="T544">
                  <ts e="T545" id="Seg_1766" n="HIAT:w" s="T544">Kim</ts>
                  <nts id="Seg_1767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_1769" n="HIAT:w" s="T545">kɨːh</ts>
                  <nts id="Seg_1770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_1772" n="HIAT:w" s="T546">ogoloru</ts>
                  <nts id="Seg_1773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_1775" n="HIAT:w" s="T547">ügüstük</ts>
                  <nts id="Seg_1776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_1778" n="HIAT:w" s="T548">ɨgɨraːččɨlar</ts>
                  <nts id="Seg_1779" n="HIAT:ip">,</nts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_1782" n="HIAT:w" s="T549">u͡ol</ts>
                  <nts id="Seg_1783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_1785" n="HIAT:w" s="T550">ogotoːgor</ts>
                  <nts id="Seg_1786" n="HIAT:ip">,</nts>
                  <nts id="Seg_1787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_1789" n="HIAT:w" s="T551">togo</ts>
                  <nts id="Seg_1790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_1792" n="HIAT:w" s="T552">ere</ts>
                  <nts id="Seg_1793" n="HIAT:ip">.</nts>
                  <nts id="Seg_1794" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T570" id="Seg_1795" n="sc" s="T554">
               <ts e="T570" id="Seg_1797" n="HIAT:u" s="T554">
                  <ts e="T555" id="Seg_1799" n="HIAT:w" s="T554">De</ts>
                  <nts id="Seg_1800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_1802" n="HIAT:w" s="T555">ol</ts>
                  <nts id="Seg_1803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_1805" n="HIAT:w" s="T556">ol</ts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_1808" n="HIAT:w" s="T557">gɨnan</ts>
                  <nts id="Seg_1809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_1811" n="HIAT:w" s="T558">baran</ts>
                  <nts id="Seg_1812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_1814" n="HIAT:w" s="T559">ol</ts>
                  <nts id="Seg_1815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_1817" n="HIAT:w" s="T560">kiminen</ts>
                  <nts id="Seg_1818" n="HIAT:ip">,</nts>
                  <nts id="Seg_1819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_1821" n="HIAT:w" s="T561">delbi</ts>
                  <nts id="Seg_1822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_1824" n="HIAT:w" s="T562">kimne</ts>
                  <nts id="Seg_1825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_1827" n="HIAT:w" s="T563">hoŋohoːnunan</ts>
                  <nts id="Seg_1828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_1830" n="HIAT:w" s="T564">delbi</ts>
                  <nts id="Seg_1831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_1833" n="HIAT:w" s="T565">kimniː</ts>
                  <nts id="Seg_1834" n="HIAT:ip">,</nts>
                  <nts id="Seg_1835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_1837" n="HIAT:w" s="T566">kimneːtiler</ts>
                  <nts id="Seg_1838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_1840" n="HIAT:w" s="T567">bu͡o</ts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_1843" n="HIAT:w" s="T568">ol</ts>
                  <nts id="Seg_1844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_1846" n="HIAT:w" s="T569">tiriːgin</ts>
                  <nts id="Seg_1847" n="HIAT:ip">.</nts>
                  <nts id="Seg_1848" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T577" id="Seg_1849" n="sc" s="T571">
               <ts e="T577" id="Seg_1851" n="HIAT:u" s="T571">
                  <ts e="T572" id="Seg_1853" n="HIAT:w" s="T571">Ol</ts>
                  <nts id="Seg_1854" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_1856" n="HIAT:w" s="T572">hoŋohoːnnuːruŋ</ts>
                  <nts id="Seg_1857" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_1859" n="HIAT:w" s="T573">emi͡e</ts>
                  <nts id="Seg_1860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_1862" n="HIAT:w" s="T574">ör</ts>
                  <nts id="Seg_1863" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_1865" n="HIAT:w" s="T575">bagajɨ</ts>
                  <nts id="Seg_1866" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_1868" n="HIAT:w" s="T576">bu͡olla</ts>
                  <nts id="Seg_1869" n="HIAT:ip">.</nts>
                  <nts id="Seg_1870" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T586" id="Seg_1871" n="sc" s="T578">
               <ts e="T586" id="Seg_1873" n="HIAT:u" s="T578">
                  <ts e="T579" id="Seg_1875" n="HIAT:w" s="T578">Nöŋü͡ö</ts>
                  <nts id="Seg_1876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_1878" n="HIAT:w" s="T579">kün</ts>
                  <nts id="Seg_1879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_1881" n="HIAT:w" s="T580">emi͡e</ts>
                  <nts id="Seg_1882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_1884" n="HIAT:w" s="T581">turu͡oktaːkkɨn</ts>
                  <nts id="Seg_1885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_1887" n="HIAT:w" s="T582">bu͡olla</ts>
                  <nts id="Seg_1888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_1890" n="HIAT:w" s="T583">ol</ts>
                  <nts id="Seg_1891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_1893" n="HIAT:w" s="T584">ɨstɨː</ts>
                  <nts id="Seg_1894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_1896" n="HIAT:w" s="T585">ɨstɨːgɨn</ts>
                  <nts id="Seg_1897" n="HIAT:ip">.</nts>
                  <nts id="Seg_1898" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T592" id="Seg_1899" n="sc" s="T587">
               <ts e="T592" id="Seg_1901" n="HIAT:u" s="T587">
                  <ts e="T588" id="Seg_1903" n="HIAT:w" s="T587">Bagarbat</ts>
                  <nts id="Seg_1904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_1906" n="HIAT:w" s="T588">bu͡olagɨn</ts>
                  <nts id="Seg_1907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_1909" n="HIAT:w" s="T589">hüregiŋ</ts>
                  <nts id="Seg_1910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_1912" n="HIAT:w" s="T590">loksujar</ts>
                  <nts id="Seg_1913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_1915" n="HIAT:w" s="T591">bu͡olla</ts>
                  <nts id="Seg_1916" n="HIAT:ip">.</nts>
                  <nts id="Seg_1917" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T604" id="Seg_1918" n="sc" s="T593">
               <ts e="T604" id="Seg_1920" n="HIAT:u" s="T593">
                  <ts e="T594" id="Seg_1922" n="HIAT:w" s="T593">De</ts>
                  <nts id="Seg_1923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_1925" n="HIAT:w" s="T594">emi͡e</ts>
                  <nts id="Seg_1926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_1928" n="HIAT:w" s="T595">hin</ts>
                  <nts id="Seg_1929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_1931" n="HIAT:w" s="T596">ɨstɨːgɨn</ts>
                  <nts id="Seg_1932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_1934" n="HIAT:w" s="T597">bu͡olla</ts>
                  <nts id="Seg_1935" n="HIAT:ip">,</nts>
                  <nts id="Seg_1936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_1938" n="HIAT:w" s="T598">kajdi͡ek</ts>
                  <nts id="Seg_1939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_1941" n="HIAT:w" s="T599">bu͡olu͡oŋuj</ts>
                  <nts id="Seg_1942" n="HIAT:ip">,</nts>
                  <nts id="Seg_1943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_1945" n="HIAT:w" s="T600">inʼeŋ</ts>
                  <nts id="Seg_1946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_1948" n="HIAT:w" s="T601">haŋatɨn</ts>
                  <nts id="Seg_1949" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_1951" n="HIAT:w" s="T602">istegin</ts>
                  <nts id="Seg_1952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_1954" n="HIAT:w" s="T603">bu͡o</ts>
                  <nts id="Seg_1955" n="HIAT:ip">.</nts>
                  <nts id="Seg_1956" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T616" id="Seg_1957" n="sc" s="T605">
               <ts e="T616" id="Seg_1959" n="HIAT:u" s="T605">
                  <ts e="T606" id="Seg_1961" n="HIAT:w" s="T605">De</ts>
                  <nts id="Seg_1962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_1964" n="HIAT:w" s="T606">ol</ts>
                  <nts id="Seg_1965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_1967" n="HIAT:w" s="T607">turan</ts>
                  <nts id="Seg_1968" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_1970" n="HIAT:w" s="T608">ɨstɨː</ts>
                  <nts id="Seg_1971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_1973" n="HIAT:w" s="T609">ɨstɨːgɨn</ts>
                  <nts id="Seg_1974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_1976" n="HIAT:w" s="T610">de</ts>
                  <nts id="Seg_1977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_1979" n="HIAT:w" s="T611">muŋnanan</ts>
                  <nts id="Seg_1980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_1982" n="HIAT:w" s="T612">muŋnanan</ts>
                  <nts id="Seg_1983" n="HIAT:ip">,</nts>
                  <nts id="Seg_1984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_1986" n="HIAT:w" s="T613">de</ts>
                  <nts id="Seg_1987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_1989" n="HIAT:w" s="T614">bütebit</ts>
                  <nts id="Seg_1990" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_1992" n="HIAT:w" s="T615">bu͡olla</ts>
                  <nts id="Seg_1993" n="HIAT:ip">.</nts>
                  <nts id="Seg_1994" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T625" id="Seg_1995" n="sc" s="T617">
               <ts e="T625" id="Seg_1997" n="HIAT:u" s="T617">
                  <ts e="T618" id="Seg_1999" n="HIAT:w" s="T617">Kahɨs</ts>
                  <nts id="Seg_2000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2002" n="HIAT:w" s="T618">eme</ts>
                  <nts id="Seg_2003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2005" n="HIAT:w" s="T619">künüger</ts>
                  <nts id="Seg_2006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2008" n="HIAT:w" s="T620">de</ts>
                  <nts id="Seg_2009" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2011" n="HIAT:w" s="T621">ol</ts>
                  <nts id="Seg_2012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2014" n="HIAT:w" s="T622">kɨhɨ͡ak</ts>
                  <nts id="Seg_2015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2017" n="HIAT:w" s="T623">kiminen</ts>
                  <nts id="Seg_2018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2020" n="HIAT:w" s="T624">hoŋohoːnnon</ts>
                  <nts id="Seg_2021" n="HIAT:ip">.</nts>
                  <nts id="Seg_2022" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T645" id="Seg_2023" n="sc" s="T626">
               <ts e="T645" id="Seg_2025" n="HIAT:u" s="T626">
                  <ts e="T627" id="Seg_2027" n="HIAT:w" s="T626">Onton</ts>
                  <nts id="Seg_2028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2030" n="HIAT:w" s="T627">kojut</ts>
                  <nts id="Seg_2031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2033" n="HIAT:w" s="T628">iti</ts>
                  <nts id="Seg_2034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2036" n="HIAT:w" s="T629">harɨː</ts>
                  <nts id="Seg_2037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2039" n="HIAT:w" s="T630">bu͡ol</ts>
                  <nts id="Seg_2040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2042" n="HIAT:w" s="T631">bukatɨn</ts>
                  <nts id="Seg_2043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2045" n="HIAT:w" s="T632">harɨː</ts>
                  <nts id="Seg_2046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2048" n="HIAT:w" s="T633">bu͡ol</ts>
                  <nts id="Seg_2049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2051" n="HIAT:w" s="T634">üčügej</ts>
                  <nts id="Seg_2052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2054" n="HIAT:w" s="T635">bu͡olu͡on</ts>
                  <nts id="Seg_2055" n="HIAT:ip">,</nts>
                  <nts id="Seg_2056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2058" n="HIAT:w" s="T636">imiges</ts>
                  <nts id="Seg_2059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2061" n="HIAT:w" s="T637">bu͡olu͡on</ts>
                  <nts id="Seg_2062" n="HIAT:ip">,</nts>
                  <nts id="Seg_2063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2065" n="HIAT:w" s="T638">iti</ts>
                  <nts id="Seg_2066" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2068" n="HIAT:w" s="T639">eŋin</ts>
                  <nts id="Seg_2069" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2071" n="HIAT:w" s="T640">eŋinnik</ts>
                  <nts id="Seg_2072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2074" n="HIAT:w" s="T641">iti</ts>
                  <nts id="Seg_2075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2077" n="HIAT:w" s="T642">kimniː</ts>
                  <nts id="Seg_2078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2080" n="HIAT:w" s="T643">hataːtɨbɨt</ts>
                  <nts id="Seg_2081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2083" n="HIAT:w" s="T644">bu͡o</ts>
                  <nts id="Seg_2084" n="HIAT:ip">.</nts>
                  <nts id="Seg_2085" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T649" id="Seg_2086" n="sc" s="T646">
               <ts e="T649" id="Seg_2088" n="HIAT:u" s="T646">
                  <ts e="T647" id="Seg_2090" n="HIAT:w" s="T646">Kɨhɨ͡aktan</ts>
                  <nts id="Seg_2091" n="HIAT:ip">,</nts>
                  <nts id="Seg_2092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2094" n="HIAT:w" s="T647">gedereːnnen</ts>
                  <nts id="Seg_2095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2097" n="HIAT:w" s="T648">oŋorobut</ts>
                  <nts id="Seg_2098" n="HIAT:ip">.</nts>
                  <nts id="Seg_2099" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T658" id="Seg_2100" n="sc" s="T650">
               <ts e="T658" id="Seg_2102" n="HIAT:u" s="T650">
                  <ts e="T651" id="Seg_2104" n="HIAT:w" s="T650">Ontuŋ</ts>
                  <nts id="Seg_2105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2107" n="HIAT:w" s="T651">harɨː</ts>
                  <nts id="Seg_2108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2110" n="HIAT:w" s="T652">olus</ts>
                  <nts id="Seg_2111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2112" n="HIAT:ip">(</nts>
                  <ts e="T654" id="Seg_2114" n="HIAT:w" s="T653">maŋan</ts>
                  <nts id="Seg_2115" n="HIAT:ip">)</nts>
                  <nts id="Seg_2116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2118" n="HIAT:w" s="T654">bu͡olaːrɨ</ts>
                  <nts id="Seg_2119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2121" n="HIAT:w" s="T655">tože</ts>
                  <nts id="Seg_2122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2124" n="HIAT:w" s="T656">hin</ts>
                  <nts id="Seg_2125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2127" n="HIAT:w" s="T657">kuhagan</ts>
                  <nts id="Seg_2128" n="HIAT:ip">.</nts>
                  <nts id="Seg_2129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T667" id="Seg_2130" n="sc" s="T659">
               <ts e="T667" id="Seg_2132" n="HIAT:u" s="T659">
                  <ts e="T660" id="Seg_2134" n="HIAT:w" s="T659">Ol</ts>
                  <nts id="Seg_2135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2137" n="HIAT:w" s="T660">ihin</ts>
                  <nts id="Seg_2138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2140" n="HIAT:w" s="T661">harɨːgɨn</ts>
                  <nts id="Seg_2141" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2143" n="HIAT:w" s="T662">iti</ts>
                  <nts id="Seg_2144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2146" n="HIAT:w" s="T663">kimi͡eke</ts>
                  <nts id="Seg_2147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2149" n="HIAT:w" s="T664">ɨhaːrallar</ts>
                  <nts id="Seg_2150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2152" n="HIAT:w" s="T665">ɨhaːrallar</ts>
                  <nts id="Seg_2153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2155" n="HIAT:w" s="T666">buru͡oga</ts>
                  <nts id="Seg_2156" n="HIAT:ip">.</nts>
                  <nts id="Seg_2157" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T675" id="Seg_2158" n="sc" s="T668">
               <ts e="T675" id="Seg_2160" n="HIAT:u" s="T668">
                  <ts e="T669" id="Seg_2162" n="HIAT:w" s="T668">Ol</ts>
                  <nts id="Seg_2163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2165" n="HIAT:w" s="T669">harɨːgɨn</ts>
                  <nts id="Seg_2166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2168" n="HIAT:w" s="T670">ɨlan</ts>
                  <nts id="Seg_2169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2171" n="HIAT:w" s="T671">barannar</ts>
                  <nts id="Seg_2172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2174" n="HIAT:w" s="T672">dʼi͡e</ts>
                  <nts id="Seg_2175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2177" n="HIAT:w" s="T673">ihiger</ts>
                  <nts id="Seg_2178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2180" n="HIAT:w" s="T674">ɨjɨːllar</ts>
                  <nts id="Seg_2181" n="HIAT:ip">.</nts>
                  <nts id="Seg_2182" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T687" id="Seg_2183" n="sc" s="T676">
               <ts e="T687" id="Seg_2185" n="HIAT:u" s="T676">
                  <ts e="T677" id="Seg_2187" n="HIAT:w" s="T676">Dʼi͡e</ts>
                  <nts id="Seg_2188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2190" n="HIAT:w" s="T677">da</ts>
                  <nts id="Seg_2191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2193" n="HIAT:w" s="T678">bu͡olu͡oj</ts>
                  <nts id="Seg_2194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2196" n="HIAT:w" s="T679">iti</ts>
                  <nts id="Seg_2197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2199" n="HIAT:w" s="T680">kim</ts>
                  <nts id="Seg_2200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2202" n="HIAT:w" s="T681">uraha</ts>
                  <nts id="Seg_2203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2205" n="HIAT:w" s="T682">ihiger</ts>
                  <nts id="Seg_2206" n="HIAT:ip">,</nts>
                  <nts id="Seg_2207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2209" n="HIAT:w" s="T683">uraha</ts>
                  <nts id="Seg_2210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2212" n="HIAT:w" s="T684">dʼi͡e</ts>
                  <nts id="Seg_2213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2215" n="HIAT:w" s="T685">ihiger</ts>
                  <nts id="Seg_2216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2218" n="HIAT:w" s="T686">ɨjɨːllar</ts>
                  <nts id="Seg_2219" n="HIAT:ip">.</nts>
                  <nts id="Seg_2220" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T694" id="Seg_2221" n="sc" s="T688">
               <ts e="T694" id="Seg_2223" n="HIAT:u" s="T688">
                  <ts e="T689" id="Seg_2225" n="HIAT:w" s="T688">Onno</ts>
                  <nts id="Seg_2226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2228" n="HIAT:w" s="T689">buru͡oga</ts>
                  <nts id="Seg_2229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2231" n="HIAT:w" s="T690">ontuŋ</ts>
                  <nts id="Seg_2232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2234" n="HIAT:w" s="T691">ɨksa</ts>
                  <nts id="Seg_2235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2237" n="HIAT:w" s="T692">turar</ts>
                  <nts id="Seg_2238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2240" n="HIAT:w" s="T693">bu͡o</ts>
                  <nts id="Seg_2241" n="HIAT:ip">.</nts>
                  <nts id="Seg_2242" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T701" id="Seg_2243" n="sc" s="T695">
               <ts e="T701" id="Seg_2245" n="HIAT:u" s="T695">
                  <ts e="T696" id="Seg_2247" n="HIAT:w" s="T695">A</ts>
                  <nts id="Seg_2248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2250" n="HIAT:w" s="T696">horok</ts>
                  <nts id="Seg_2251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2253" n="HIAT:w" s="T697">dʼaktattar</ts>
                  <nts id="Seg_2254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2256" n="HIAT:w" s="T698">innʼe</ts>
                  <nts id="Seg_2257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2259" n="HIAT:w" s="T699">gɨnaːččɨlar</ts>
                  <nts id="Seg_2260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2262" n="HIAT:w" s="T700">bu͡olla</ts>
                  <nts id="Seg_2263" n="HIAT:ip">.</nts>
                  <nts id="Seg_2264" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T709" id="Seg_2265" n="sc" s="T702">
               <ts e="T709" id="Seg_2267" n="HIAT:u" s="T702">
                  <ts e="T703" id="Seg_2269" n="HIAT:w" s="T702">Ol</ts>
                  <nts id="Seg_2270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_2272" n="HIAT:w" s="T703">kördük</ts>
                  <nts id="Seg_2273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2275" n="HIAT:w" s="T704">da</ts>
                  <nts id="Seg_2276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2278" n="HIAT:w" s="T705">ɨhɨ͡ara</ts>
                  <nts id="Seg_2279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2281" n="HIAT:w" s="T706">hɨtɨ͡ak</ts>
                  <nts id="Seg_2282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2284" n="HIAT:w" s="T707">ebitter</ts>
                  <nts id="Seg_2285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2287" n="HIAT:w" s="T708">otto</ts>
                  <nts id="Seg_2288" n="HIAT:ip">.</nts>
                  <nts id="Seg_2289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T734" id="Seg_2290" n="sc" s="T710">
               <ts e="T724" id="Seg_2292" n="HIAT:u" s="T710">
                  <ts e="T711" id="Seg_2294" n="HIAT:w" s="T710">A</ts>
                  <nts id="Seg_2295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2297" n="HIAT:w" s="T711">gini</ts>
                  <nts id="Seg_2298" n="HIAT:ip">,</nts>
                  <nts id="Seg_2299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2301" n="HIAT:w" s="T712">ginileriŋ</ts>
                  <nts id="Seg_2302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2304" n="HIAT:w" s="T713">innʼekeːt</ts>
                  <nts id="Seg_2305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2307" n="HIAT:w" s="T714">diːller</ts>
                  <nts id="Seg_2308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2310" n="HIAT:w" s="T715">bu͡o</ts>
                  <nts id="Seg_2311" n="HIAT:ip">,</nts>
                  <nts id="Seg_2312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2314" n="HIAT:w" s="T716">bihigini</ts>
                  <nts id="Seg_2315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2317" n="HIAT:w" s="T717">turu͡orallar</ts>
                  <nts id="Seg_2318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2320" n="HIAT:w" s="T718">bu͡o</ts>
                  <nts id="Seg_2321" n="HIAT:ip">,</nts>
                  <nts id="Seg_2322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2324" n="HIAT:w" s="T719">üs</ts>
                  <nts id="Seg_2325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2327" n="HIAT:w" s="T720">tü͡ört</ts>
                  <nts id="Seg_2328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_2330" n="HIAT:w" s="T721">kɨːs</ts>
                  <nts id="Seg_2331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_2333" n="HIAT:w" s="T722">kɨːh</ts>
                  <nts id="Seg_2334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2336" n="HIAT:w" s="T723">ogolor</ts>
                  <nts id="Seg_2337" n="HIAT:ip">.</nts>
                  <nts id="Seg_2338" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T725" id="Seg_2340" n="HIAT:u" s="T724">
                  <ts e="T868" id="Seg_2342" n="HIAT:w" s="T724">Kim</ts>
                  <nts id="Seg_2343" n="HIAT:ip">,</nts>
                  <nts id="Seg_2344" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T869" id="Seg_2346" n="HIAT:w" s="T868">kim</ts>
                  <nts id="Seg_2347" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T870" id="Seg_2349" n="HIAT:w" s="T869">u͡ota</ts>
                  <nts id="Seg_2350" n="HIAT:ip">,</nts>
                  <nts id="Seg_2351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T871" id="Seg_2353" n="HIAT:w" s="T870">kim</ts>
                  <nts id="Seg_2354" n="HIAT:ip">,</nts>
                  <nts id="Seg_2355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_2357" n="HIAT:w" s="T871">u͡ota</ts>
                  <nts id="Seg_2358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873" id="Seg_2360" n="HIAT:w" s="T872">iti</ts>
                  <nts id="Seg_2361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874" id="Seg_2363" n="HIAT:w" s="T873">ačaːg</ts>
                  <nts id="Seg_2364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2366" n="HIAT:w" s="T874">oŋorollor</ts>
                  <nts id="Seg_2367" n="HIAT:ip">.</nts>
                  <nts id="Seg_2368" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T734" id="Seg_2370" n="HIAT:u" s="T725">
                  <ts e="T726" id="Seg_2372" n="HIAT:w" s="T725">Mas</ts>
                  <nts id="Seg_2373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2375" n="HIAT:w" s="T726">ubajar</ts>
                  <nts id="Seg_2376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2378" n="HIAT:w" s="T727">ontuŋ</ts>
                  <nts id="Seg_2379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2381" n="HIAT:w" s="T728">ürdütünen</ts>
                  <nts id="Seg_2382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2384" n="HIAT:w" s="T729">ottoru</ts>
                  <nts id="Seg_2385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2387" n="HIAT:w" s="T730">bɨragallar</ts>
                  <nts id="Seg_2388" n="HIAT:ip">,</nts>
                  <nts id="Seg_2389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_2391" n="HIAT:w" s="T731">ulakan</ts>
                  <nts id="Seg_2392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2394" n="HIAT:w" s="T732">buru͡o</ts>
                  <nts id="Seg_2395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2397" n="HIAT:w" s="T733">keli͡en</ts>
                  <nts id="Seg_2398" n="HIAT:ip">.</nts>
                  <nts id="Seg_2399" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T745" id="Seg_2400" n="sc" s="T735">
               <ts e="T745" id="Seg_2402" n="HIAT:u" s="T735">
                  <ts e="T736" id="Seg_2404" n="HIAT:w" s="T735">De</ts>
                  <nts id="Seg_2405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_2407" n="HIAT:w" s="T736">ol</ts>
                  <nts id="Seg_2408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_2410" n="HIAT:w" s="T737">buru͡o</ts>
                  <nts id="Seg_2411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_2413" n="HIAT:w" s="T738">ihiger</ts>
                  <nts id="Seg_2414" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_2416" n="HIAT:w" s="T739">tögürüččü</ts>
                  <nts id="Seg_2417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2419" n="HIAT:w" s="T740">turammɨt</ts>
                  <nts id="Seg_2420" n="HIAT:ip">,</nts>
                  <nts id="Seg_2421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_2423" n="HIAT:w" s="T741">ol</ts>
                  <nts id="Seg_2424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_2426" n="HIAT:w" s="T742">tutabɨt</ts>
                  <nts id="Seg_2427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_2429" n="HIAT:w" s="T743">bu͡olla</ts>
                  <nts id="Seg_2430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_2432" n="HIAT:w" s="T744">kimi</ts>
                  <nts id="Seg_2433" n="HIAT:ip">.</nts>
                  <nts id="Seg_2434" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T754" id="Seg_2435" n="sc" s="T746">
               <ts e="T754" id="Seg_2437" n="HIAT:u" s="T746">
                  <ts e="T747" id="Seg_2439" n="HIAT:w" s="T746">Ol</ts>
                  <nts id="Seg_2440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_2442" n="HIAT:w" s="T747">gɨnan</ts>
                  <nts id="Seg_2443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_2445" n="HIAT:w" s="T748">baran</ts>
                  <nts id="Seg_2446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_2448" n="HIAT:w" s="T749">ergitebit</ts>
                  <nts id="Seg_2449" n="HIAT:ip">,</nts>
                  <nts id="Seg_2450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_2452" n="HIAT:w" s="T750">kün</ts>
                  <nts id="Seg_2453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_2455" n="HIAT:w" s="T751">hu͡olun</ts>
                  <nts id="Seg_2456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_2458" n="HIAT:w" s="T752">kördük</ts>
                  <nts id="Seg_2459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_2461" n="HIAT:w" s="T753">ergitebit</ts>
                  <nts id="Seg_2462" n="HIAT:ip">.</nts>
                  <nts id="Seg_2463" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T760" id="Seg_2464" n="sc" s="T755">
               <ts e="T760" id="Seg_2466" n="HIAT:u" s="T755">
                  <ts e="T756" id="Seg_2468" n="HIAT:w" s="T755">Üčügejdik</ts>
                  <nts id="Seg_2469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_2471" n="HIAT:w" s="T756">tutu͡oktaːkkɨn</ts>
                  <nts id="Seg_2472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_2474" n="HIAT:w" s="T757">ontugun</ts>
                  <nts id="Seg_2475" n="HIAT:ip">,</nts>
                  <nts id="Seg_2476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_2478" n="HIAT:w" s="T758">ɨːppat</ts>
                  <nts id="Seg_2479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_2481" n="HIAT:w" s="T759">kördük</ts>
                  <nts id="Seg_2482" n="HIAT:ip">.</nts>
                  <nts id="Seg_2483" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T766" id="Seg_2484" n="sc" s="T761">
               <ts e="T766" id="Seg_2486" n="HIAT:u" s="T761">
                  <nts id="Seg_2487" n="HIAT:ip">(</nts>
                  <ts e="T762" id="Seg_2489" n="HIAT:w" s="T761">Beje-</ts>
                  <nts id="Seg_2490" n="HIAT:ip">)</nts>
                  <nts id="Seg_2491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_2493" n="HIAT:w" s="T762">bejebitiger</ts>
                  <nts id="Seg_2494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_2496" n="HIAT:w" s="T763">ergitebit</ts>
                  <nts id="Seg_2497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_2499" n="HIAT:w" s="T764">bu͡ol</ts>
                  <nts id="Seg_2500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_2502" n="HIAT:w" s="T765">innʼekeːt</ts>
                  <nts id="Seg_2503" n="HIAT:ip">.</nts>
                  <nts id="Seg_2504" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T779" id="Seg_2505" n="sc" s="T767">
               <ts e="T779" id="Seg_2507" n="HIAT:u" s="T767">
                  <ts e="T768" id="Seg_2509" n="HIAT:w" s="T767">Min</ts>
                  <nts id="Seg_2510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_2512" n="HIAT:w" s="T768">dogorbor</ts>
                  <nts id="Seg_2513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_2515" n="HIAT:w" s="T769">bi͡eri͡em</ts>
                  <nts id="Seg_2516" n="HIAT:ip">,</nts>
                  <nts id="Seg_2517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T771" id="Seg_2519" n="HIAT:w" s="T770">dogorum</ts>
                  <nts id="Seg_2520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_2522" n="HIAT:w" s="T771">nöŋü͡ö</ts>
                  <nts id="Seg_2523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_2525" n="HIAT:w" s="T772">dogorgor</ts>
                  <nts id="Seg_2526" n="HIAT:ip">,</nts>
                  <nts id="Seg_2527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_2529" n="HIAT:w" s="T773">itirdikkeːn</ts>
                  <nts id="Seg_2530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_2532" n="HIAT:w" s="T774">bi͡erer</ts>
                  <nts id="Seg_2533" n="HIAT:ip">,</nts>
                  <nts id="Seg_2534" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_2536" n="HIAT:w" s="T775">eto</ts>
                  <nts id="Seg_2537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_2539" n="HIAT:w" s="T776">tögürütebit</ts>
                  <nts id="Seg_2540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_2542" n="HIAT:w" s="T777">ol</ts>
                  <nts id="Seg_2543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_2545" n="HIAT:w" s="T778">tiriːni</ts>
                  <nts id="Seg_2546" n="HIAT:ip">.</nts>
                  <nts id="Seg_2547" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T785" id="Seg_2548" n="sc" s="T780">
               <ts e="T785" id="Seg_2550" n="HIAT:u" s="T780">
                  <ts e="T781" id="Seg_2552" n="HIAT:w" s="T780">Annɨgɨtɨttan</ts>
                  <nts id="Seg_2553" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_2555" n="HIAT:w" s="T781">ulakan</ts>
                  <nts id="Seg_2556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_2558" n="HIAT:w" s="T782">buru͡o</ts>
                  <nts id="Seg_2559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_2561" n="HIAT:w" s="T783">keler</ts>
                  <nts id="Seg_2562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_2564" n="HIAT:w" s="T784">bu͡o</ts>
                  <nts id="Seg_2565" n="HIAT:ip">.</nts>
                  <nts id="Seg_2566" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T792" id="Seg_2567" n="sc" s="T786">
               <ts e="T792" id="Seg_2569" n="HIAT:u" s="T786">
                  <ts e="T787" id="Seg_2571" n="HIAT:w" s="T786">Bihi͡eke</ts>
                  <nts id="Seg_2572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_2574" n="HIAT:w" s="T787">emi͡e</ts>
                  <nts id="Seg_2575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_2577" n="HIAT:w" s="T788">kuhagan</ts>
                  <nts id="Seg_2578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_2580" n="HIAT:w" s="T789">ol</ts>
                  <nts id="Seg_2581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_2583" n="HIAT:w" s="T790">buru͡o</ts>
                  <nts id="Seg_2584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_2586" n="HIAT:w" s="T791">kelere</ts>
                  <nts id="Seg_2587" n="HIAT:ip">.</nts>
                  <nts id="Seg_2588" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T798" id="Seg_2589" n="sc" s="T793">
               <ts e="T798" id="Seg_2591" n="HIAT:u" s="T793">
                  <ts e="T794" id="Seg_2593" n="HIAT:w" s="T793">Karakpɨt</ts>
                  <nts id="Seg_2594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_2596" n="HIAT:w" s="T794">ahɨjar</ts>
                  <nts id="Seg_2597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_2599" n="HIAT:w" s="T795">bu͡olla</ts>
                  <nts id="Seg_2600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_2602" n="HIAT:w" s="T796">ol</ts>
                  <nts id="Seg_2603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_2605" n="HIAT:w" s="T797">buru͡ottan</ts>
                  <nts id="Seg_2606" n="HIAT:ip">.</nts>
                  <nts id="Seg_2607" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T804" id="Seg_2608" n="sc" s="T799">
               <ts e="T804" id="Seg_2610" n="HIAT:u" s="T799">
                  <ts e="T800" id="Seg_2612" n="HIAT:w" s="T799">Anʼakpɨtɨgar</ts>
                  <nts id="Seg_2613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_2615" n="HIAT:w" s="T800">da</ts>
                  <nts id="Seg_2616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_2618" n="HIAT:w" s="T801">ɨlabɨt</ts>
                  <nts id="Seg_2619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_2621" n="HIAT:w" s="T802">bu͡olla</ts>
                  <nts id="Seg_2622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_2624" n="HIAT:w" s="T803">buru͡onu</ts>
                  <nts id="Seg_2625" n="HIAT:ip">.</nts>
                  <nts id="Seg_2626" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T817" id="Seg_2627" n="sc" s="T805">
               <ts e="T817" id="Seg_2629" n="HIAT:u" s="T805">
                  <ts e="T806" id="Seg_2631" n="HIAT:w" s="T805">De</ts>
                  <nts id="Seg_2632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_2634" n="HIAT:w" s="T806">ol</ts>
                  <nts id="Seg_2635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_2637" n="HIAT:w" s="T807">da</ts>
                  <nts id="Seg_2638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_2640" n="HIAT:w" s="T808">bu͡ollar</ts>
                  <nts id="Seg_2641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_2643" n="HIAT:w" s="T809">de</ts>
                  <nts id="Seg_2644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_2646" n="HIAT:w" s="T810">turunaːktɨːbɨt</ts>
                  <nts id="Seg_2647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_2649" n="HIAT:w" s="T811">bu͡o</ts>
                  <nts id="Seg_2650" n="HIAT:ip">,</nts>
                  <nts id="Seg_2651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T813" id="Seg_2653" n="HIAT:w" s="T812">turuŋ</ts>
                  <nts id="Seg_2654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_2656" n="HIAT:w" s="T813">turuŋ</ts>
                  <nts id="Seg_2657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_2659" n="HIAT:w" s="T814">diːller</ts>
                  <nts id="Seg_2660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_2662" n="HIAT:w" s="T815">bu͡o</ts>
                  <nts id="Seg_2663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_2665" n="HIAT:w" s="T816">bihigini</ts>
                  <nts id="Seg_2666" n="HIAT:ip">.</nts>
                  <nts id="Seg_2667" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T822" id="Seg_2668" n="sc" s="T818">
               <ts e="T822" id="Seg_2670" n="HIAT:u" s="T818">
                  <ts e="T819" id="Seg_2672" n="HIAT:w" s="T818">De</ts>
                  <nts id="Seg_2673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_2675" n="HIAT:w" s="T819">ol</ts>
                  <nts id="Seg_2676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T821" id="Seg_2678" n="HIAT:w" s="T820">kördük</ts>
                  <nts id="Seg_2679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_2681" n="HIAT:w" s="T821">ergitebit</ts>
                  <nts id="Seg_2682" n="HIAT:ip">.</nts>
                  <nts id="Seg_2683" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T849" id="Seg_2684" n="sc" s="T823">
               <ts e="T849" id="Seg_2686" n="HIAT:u" s="T823">
                  <ts e="T824" id="Seg_2688" n="HIAT:w" s="T823">Onton</ts>
                  <nts id="Seg_2689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_2691" n="HIAT:w" s="T824">biːr</ts>
                  <nts id="Seg_2692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_2694" n="HIAT:w" s="T825">emete</ts>
                  <nts id="Seg_2695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_2697" n="HIAT:w" s="T826">kɨːh</ts>
                  <nts id="Seg_2698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T828" id="Seg_2700" n="HIAT:w" s="T827">ogoto</ts>
                  <nts id="Seg_2701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_2703" n="HIAT:w" s="T828">tiriːni</ts>
                  <nts id="Seg_2704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_2706" n="HIAT:w" s="T829">iliːtinen</ts>
                  <nts id="Seg_2707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T831" id="Seg_2709" n="HIAT:w" s="T830">innʼekeːtiŋ</ts>
                  <nts id="Seg_2710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_2712" n="HIAT:w" s="T831">ɨlan</ts>
                  <nts id="Seg_2713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_2715" n="HIAT:w" s="T832">keːhi͡e</ts>
                  <nts id="Seg_2716" n="HIAT:ip">,</nts>
                  <nts id="Seg_2717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_2719" n="HIAT:w" s="T833">hɨːha</ts>
                  <nts id="Seg_2720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_2722" n="HIAT:w" s="T834">tutu͡o</ts>
                  <nts id="Seg_2723" n="HIAT:ip">,</nts>
                  <nts id="Seg_2724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_2726" n="HIAT:w" s="T835">ontutun</ts>
                  <nts id="Seg_2727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T837" id="Seg_2729" n="HIAT:w" s="T836">ol</ts>
                  <nts id="Seg_2730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_2732" n="HIAT:w" s="T837">tutar</ts>
                  <nts id="Seg_2733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_2735" n="HIAT:w" s="T838">hirin</ts>
                  <nts id="Seg_2736" n="HIAT:ip">,</nts>
                  <nts id="Seg_2737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_2739" n="HIAT:w" s="T839">tiriːtin</ts>
                  <nts id="Seg_2740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T841" id="Seg_2742" n="HIAT:w" s="T840">de</ts>
                  <nts id="Seg_2743" n="HIAT:ip">,</nts>
                  <nts id="Seg_2744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_2746" n="HIAT:w" s="T841">oččogo</ts>
                  <nts id="Seg_2747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_2749" n="HIAT:w" s="T842">diːller</ts>
                  <nts id="Seg_2750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T844" id="Seg_2752" n="HIAT:w" s="T843">bu͡o</ts>
                  <nts id="Seg_2753" n="HIAT:ip">,</nts>
                  <nts id="Seg_2754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_2756" n="HIAT:w" s="T844">de</ts>
                  <nts id="Seg_2757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_2759" n="HIAT:w" s="T845">erge</ts>
                  <nts id="Seg_2760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_2762" n="HIAT:w" s="T846">barɨ͡aŋ</ts>
                  <nts id="Seg_2763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T848" id="Seg_2765" n="HIAT:w" s="T847">hu͡oga</ts>
                  <nts id="Seg_2766" n="HIAT:ip">,</nts>
                  <nts id="Seg_2767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T849" id="Seg_2769" n="HIAT:w" s="T848">ulaːttakkɨna</ts>
                  <nts id="Seg_2770" n="HIAT:ip">.</nts>
                  <nts id="Seg_2771" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T856" id="Seg_2772" n="sc" s="T850">
               <ts e="T856" id="Seg_2774" n="HIAT:u" s="T850">
                  <ts e="T851" id="Seg_2776" n="HIAT:w" s="T850">Diː</ts>
                  <nts id="Seg_2777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_2779" n="HIAT:w" s="T851">diːller</ts>
                  <nts id="Seg_2780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_2782" n="HIAT:w" s="T852">itigirdikkeːn</ts>
                  <nts id="Seg_2783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_2785" n="HIAT:w" s="T853">haŋarallar</ts>
                  <nts id="Seg_2786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T855" id="Seg_2788" n="HIAT:w" s="T854">bu͡o</ts>
                  <nts id="Seg_2789" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_2791" n="HIAT:w" s="T855">bihigini</ts>
                  <nts id="Seg_2792" n="HIAT:ip">.</nts>
                  <nts id="Seg_2793" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T867" id="Seg_2794" n="sc" s="T857">
               <ts e="T867" id="Seg_2796" n="HIAT:u" s="T857">
                  <ts e="T858" id="Seg_2798" n="HIAT:w" s="T857">Ol</ts>
                  <nts id="Seg_2799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T859" id="Seg_2801" n="HIAT:w" s="T858">ihin</ts>
                  <nts id="Seg_2802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_2804" n="HIAT:w" s="T859">ol</ts>
                  <nts id="Seg_2805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_2807" n="HIAT:w" s="T860">muŋnana</ts>
                  <nts id="Seg_2808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T862" id="Seg_2810" n="HIAT:w" s="T861">turabɨt</ts>
                  <nts id="Seg_2811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T863" id="Seg_2813" n="HIAT:w" s="T862">bu͡o</ts>
                  <nts id="Seg_2814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864" id="Seg_2816" n="HIAT:w" s="T863">ol</ts>
                  <nts id="Seg_2817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T865" id="Seg_2819" n="HIAT:w" s="T864">tiriːni</ts>
                  <nts id="Seg_2820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_2822" n="HIAT:w" s="T865">ɨːppakka</ts>
                  <nts id="Seg_2823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T867" id="Seg_2825" n="HIAT:w" s="T866">ɨntak</ts>
                  <nts id="Seg_2826" n="HIAT:ip">.</nts>
                  <nts id="Seg_2827" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T15" id="Seg_2828" n="sc" s="T1">
               <ts e="T2" id="Seg_2830" n="e" s="T1">Köhö </ts>
               <ts e="T3" id="Seg_2832" n="e" s="T2">(hɨl-) </ts>
               <ts e="T4" id="Seg_2834" n="e" s="T3">kɨhɨnɨ, </ts>
               <ts e="T5" id="Seg_2836" n="e" s="T4">kɨhɨnɨ </ts>
               <ts e="T6" id="Seg_2838" n="e" s="T5">meldʼi </ts>
               <ts e="T7" id="Seg_2840" n="e" s="T6">köhö </ts>
               <ts e="T8" id="Seg_2842" n="e" s="T7">hɨldʼan </ts>
               <ts e="T9" id="Seg_2844" n="e" s="T8">barannar </ts>
               <ts e="T10" id="Seg_2846" n="e" s="T9">hakalar </ts>
               <ts e="T11" id="Seg_2848" n="e" s="T10">haːs </ts>
               <ts e="T12" id="Seg_2850" n="e" s="T11">toktuːr </ts>
               <ts e="T13" id="Seg_2852" n="e" s="T12">hirdere </ts>
               <ts e="T14" id="Seg_2854" n="e" s="T13">baːr </ts>
               <ts e="T15" id="Seg_2856" n="e" s="T14">bu͡olaːččɨ. </ts>
            </ts>
            <ts e="T24" id="Seg_2857" n="sc" s="T16">
               <ts e="T17" id="Seg_2859" n="e" s="T16">Onno </ts>
               <ts e="T18" id="Seg_2861" n="e" s="T17">ügüs </ts>
               <ts e="T19" id="Seg_2863" n="e" s="T18">bagajɨ </ts>
               <ts e="T20" id="Seg_2865" n="e" s="T19">uraha </ts>
               <ts e="T21" id="Seg_2867" n="e" s="T20">dʼi͡eler, </ts>
               <ts e="T22" id="Seg_2869" n="e" s="T21">hɨrga </ts>
               <ts e="T23" id="Seg_2871" n="e" s="T22">dʼi͡eler </ts>
               <ts e="T24" id="Seg_2873" n="e" s="T23">turaːččɨlar. </ts>
            </ts>
            <ts e="T35" id="Seg_2874" n="sc" s="T25">
               <ts e="T26" id="Seg_2876" n="e" s="T25">Haːskɨ </ts>
               <ts e="T27" id="Seg_2878" n="e" s="T26">hɨlaːs </ts>
               <ts e="T28" id="Seg_2880" n="e" s="T27">küttere, </ts>
               <ts e="T29" id="Seg_2882" n="e" s="T28">kün </ts>
               <ts e="T30" id="Seg_2884" n="e" s="T29">tɨgar </ts>
               <ts e="T31" id="Seg_2886" n="e" s="T30">bu͡ollagɨna </ts>
               <ts e="T32" id="Seg_2888" n="e" s="T31">de </ts>
               <ts e="T33" id="Seg_2890" n="e" s="T32">dʼaktattar </ts>
               <ts e="T34" id="Seg_2892" n="e" s="T33">üleliːllere </ts>
               <ts e="T35" id="Seg_2894" n="e" s="T34">üksüːr. </ts>
            </ts>
            <ts e="T43" id="Seg_2895" n="sc" s="T36">
               <ts e="T37" id="Seg_2897" n="e" s="T36">Tahaːra </ts>
               <ts e="T38" id="Seg_2899" n="e" s="T37">ginileriŋ </ts>
               <ts e="T39" id="Seg_2901" n="e" s="T38">maŋnaj </ts>
               <ts e="T40" id="Seg_2903" n="e" s="T39">iti </ts>
               <ts e="T41" id="Seg_2905" n="e" s="T40">tiriː </ts>
               <ts e="T42" id="Seg_2907" n="e" s="T41">ületin </ts>
               <ts e="T43" id="Seg_2909" n="e" s="T42">üleliːller. </ts>
            </ts>
            <ts e="T50" id="Seg_2910" n="sc" s="T44">
               <ts e="T45" id="Seg_2912" n="e" s="T44">Tiriː, </ts>
               <ts e="T46" id="Seg_2914" n="e" s="T45">tiriːgin </ts>
               <ts e="T47" id="Seg_2916" n="e" s="T46">bert </ts>
               <ts e="T48" id="Seg_2918" n="e" s="T47">delbi </ts>
               <ts e="T49" id="Seg_2920" n="e" s="T48">ɨraːstɨːllar </ts>
               <ts e="T50" id="Seg_2922" n="e" s="T49">maŋnaj. </ts>
            </ts>
            <ts e="T57" id="Seg_2923" n="sc" s="T51">
               <ts e="T52" id="Seg_2925" n="e" s="T51">Ɨraːstan </ts>
               <ts e="T53" id="Seg_2927" n="e" s="T52">barannar </ts>
               <ts e="T54" id="Seg_2929" n="e" s="T53">kɨptɨjɨnan </ts>
               <ts e="T55" id="Seg_2931" n="e" s="T54">kɨrɨjallar </ts>
               <ts e="T56" id="Seg_2933" n="e" s="T55">(ütr-) </ts>
               <ts e="T57" id="Seg_2935" n="e" s="T56">tüːtün. </ts>
            </ts>
            <ts e="T67" id="Seg_2936" n="sc" s="T58">
               <ts e="T59" id="Seg_2938" n="e" s="T58">Ol </ts>
               <ts e="T60" id="Seg_2940" n="e" s="T59">kɨptɨjɨnan </ts>
               <ts e="T61" id="Seg_2942" n="e" s="T60">kɨrɨjan </ts>
               <ts e="T62" id="Seg_2944" n="e" s="T61">büttekterine, </ts>
               <ts e="T63" id="Seg_2946" n="e" s="T62">onton </ts>
               <ts e="T64" id="Seg_2948" n="e" s="T63">kojut </ts>
               <ts e="T65" id="Seg_2950" n="e" s="T64">bahagɨnan </ts>
               <ts e="T66" id="Seg_2952" n="e" s="T65">emi͡e </ts>
               <ts e="T67" id="Seg_2954" n="e" s="T66">kɨrɨjallar. </ts>
            </ts>
            <ts e="T76" id="Seg_2955" n="sc" s="T68">
               <ts e="T69" id="Seg_2957" n="e" s="T68">Öldüːn </ts>
               <ts e="T70" id="Seg_2959" n="e" s="T69">hakalar </ts>
               <ts e="T71" id="Seg_2961" n="e" s="T70">olus </ts>
               <ts e="T72" id="Seg_2963" n="e" s="T71">köp </ts>
               <ts e="T73" id="Seg_2965" n="e" s="T72">kimi </ts>
               <ts e="T74" id="Seg_2967" n="e" s="T73">tiriːni </ts>
               <ts e="T75" id="Seg_2969" n="e" s="T74">ɨlaːččɨta </ts>
               <ts e="T76" id="Seg_2971" n="e" s="T75">hu͡oktar. </ts>
            </ts>
            <ts e="T93" id="Seg_2972" n="sc" s="T77">
               <ts e="T78" id="Seg_2974" n="e" s="T77">Kɨrɨllɨbɨt </ts>
               <ts e="T79" id="Seg_2976" n="e" s="T78">iti </ts>
               <ts e="T80" id="Seg_2978" n="e" s="T79">tüːtün </ts>
               <ts e="T81" id="Seg_2980" n="e" s="T80">ɨlɨllɨbɨttan </ts>
               <ts e="T82" id="Seg_2982" n="e" s="T81">(kɨrpa-) </ts>
               <ts e="T83" id="Seg_2984" n="e" s="T82">kɨrpalaːbɨt </ts>
               <ts e="T84" id="Seg_2986" n="e" s="T83">tüːnü </ts>
               <ts e="T85" id="Seg_2988" n="e" s="T84">kimi </ts>
               <ts e="T86" id="Seg_2990" n="e" s="T85">tiriːni </ts>
               <ts e="T87" id="Seg_2992" n="e" s="T86">ɨlannar, </ts>
               <ts e="T88" id="Seg_2994" n="e" s="T87">kim, </ts>
               <ts e="T89" id="Seg_2996" n="e" s="T88">dʼi͡e </ts>
               <ts e="T90" id="Seg_2998" n="e" s="T89">habɨːta, </ts>
               <ts e="T91" id="Seg_3000" n="e" s="T90">iti </ts>
               <ts e="T92" id="Seg_3002" n="e" s="T91">öldüːtteri </ts>
               <ts e="T93" id="Seg_3004" n="e" s="T92">tigeːččiler. </ts>
            </ts>
            <ts e="T102" id="Seg_3005" n="sc" s="T94">
               <ts e="T95" id="Seg_3007" n="e" s="T94">Onnuk </ts>
               <ts e="T96" id="Seg_3009" n="e" s="T95">bu͡ollagɨna </ts>
               <ts e="T97" id="Seg_3011" n="e" s="T96">de </ts>
               <ts e="T98" id="Seg_3013" n="e" s="T97">ol </ts>
               <ts e="T99" id="Seg_3015" n="e" s="T98">barbak </ts>
               <ts e="T100" id="Seg_3017" n="e" s="T99">tüːnü </ts>
               <ts e="T101" id="Seg_3019" n="e" s="T100">keːheller </ts>
               <ts e="T102" id="Seg_3021" n="e" s="T101">bu͡o. </ts>
            </ts>
            <ts e="T107" id="Seg_3022" n="sc" s="T103">
               <ts e="T104" id="Seg_3024" n="e" s="T103">Onton </ts>
               <ts e="T105" id="Seg_3026" n="e" s="T104">ontugunan </ts>
               <ts e="T106" id="Seg_3028" n="e" s="T105">öldüːn </ts>
               <ts e="T107" id="Seg_3030" n="e" s="T106">tigeller. </ts>
            </ts>
            <ts e="T116" id="Seg_3031" n="sc" s="T108">
               <ts e="T109" id="Seg_3033" n="e" s="T108">Ol </ts>
               <ts e="T110" id="Seg_3035" n="e" s="T109">(öl-) </ts>
               <ts e="T111" id="Seg_3037" n="e" s="T110">horok </ts>
               <ts e="T112" id="Seg_3039" n="e" s="T111">tiriːni </ts>
               <ts e="T113" id="Seg_3041" n="e" s="T112">ginileriŋ </ts>
               <ts e="T114" id="Seg_3043" n="e" s="T113">iti </ts>
               <ts e="T115" id="Seg_3045" n="e" s="T114">harɨː </ts>
               <ts e="T116" id="Seg_3047" n="e" s="T115">oŋoru͡oktaːktar. </ts>
            </ts>
            <ts e="T124" id="Seg_3048" n="sc" s="T117">
               <ts e="T118" id="Seg_3050" n="e" s="T117">Harɨː </ts>
               <ts e="T119" id="Seg_3052" n="e" s="T118">oŋorollorugar </ts>
               <ts e="T120" id="Seg_3054" n="e" s="T119">itinne </ts>
               <ts e="T121" id="Seg_3056" n="e" s="T120">de, </ts>
               <ts e="T122" id="Seg_3058" n="e" s="T121">hin </ts>
               <ts e="T123" id="Seg_3060" n="e" s="T122">ügüs </ts>
               <ts e="T124" id="Seg_3062" n="e" s="T123">üle: </ts>
            </ts>
            <ts e="T141" id="Seg_3063" n="sc" s="T125">
               <ts e="T126" id="Seg_3065" n="e" s="T125">Kɨrpalɨːllar </ts>
               <ts e="T127" id="Seg_3067" n="e" s="T126">bu͡o, </ts>
               <ts e="T128" id="Seg_3069" n="e" s="T127">iti </ts>
               <ts e="T129" id="Seg_3071" n="e" s="T128">kɨrpalaːbɨttarɨn </ts>
               <ts e="T130" id="Seg_3073" n="e" s="T129">kördük, </ts>
               <ts e="T131" id="Seg_3075" n="e" s="T130">tüːte </ts>
               <ts e="T132" id="Seg_3077" n="e" s="T131">hu͡ok </ts>
               <ts e="T133" id="Seg_3079" n="e" s="T132">bu͡o </ts>
               <ts e="T134" id="Seg_3081" n="e" s="T133">tüːte </ts>
               <ts e="T135" id="Seg_3083" n="e" s="T134">agɨjak </ts>
               <ts e="T136" id="Seg_3085" n="e" s="T135">bu͡olu͡on, </ts>
               <ts e="T137" id="Seg_3087" n="e" s="T136">hin </ts>
               <ts e="T138" id="Seg_3089" n="e" s="T137">tüːte </ts>
               <ts e="T139" id="Seg_3091" n="e" s="T138">kaːlar </ts>
               <ts e="T140" id="Seg_3093" n="e" s="T139">bu͡o </ts>
               <ts e="T141" id="Seg_3095" n="e" s="T140">tiriːger. </ts>
            </ts>
            <ts e="T163" id="Seg_3096" n="sc" s="T142">
               <ts e="T143" id="Seg_3098" n="e" s="T142">Ol </ts>
               <ts e="T144" id="Seg_3100" n="e" s="T143">tüː, </ts>
               <ts e="T145" id="Seg_3102" n="e" s="T144">tüːŋ </ts>
               <ts e="T146" id="Seg_3104" n="e" s="T145">üčügejdik </ts>
               <ts e="T147" id="Seg_3106" n="e" s="T146">barɨta </ts>
               <ts e="T148" id="Seg_3108" n="e" s="T147">tühü͡ön, </ts>
               <ts e="T149" id="Seg_3110" n="e" s="T148">onton </ts>
               <ts e="T150" id="Seg_3112" n="e" s="T149">kojut </ts>
               <ts e="T151" id="Seg_3114" n="e" s="T150">imiterge </ts>
               <ts e="T152" id="Seg_3116" n="e" s="T151">da </ts>
               <ts e="T153" id="Seg_3118" n="e" s="T152">üčügej </ts>
               <ts e="T154" id="Seg_3120" n="e" s="T153">bu͡o, </ts>
               <ts e="T155" id="Seg_3122" n="e" s="T154">harɨː </ts>
               <ts e="T156" id="Seg_3124" n="e" s="T155">bu͡olu͡on, </ts>
               <ts e="T157" id="Seg_3126" n="e" s="T156">ol </ts>
               <ts e="T158" id="Seg_3128" n="e" s="T157">tüːleːk </ts>
               <ts e="T159" id="Seg_3130" n="e" s="T158">kim </ts>
               <ts e="T160" id="Seg_3132" n="e" s="T159">tiriːni </ts>
               <ts e="T161" id="Seg_3134" n="e" s="T160">ginileriŋ </ts>
               <ts e="T162" id="Seg_3136" n="e" s="T161">hɨtɨtallar </ts>
               <ts e="T163" id="Seg_3138" n="e" s="T162">uːga. </ts>
            </ts>
            <ts e="T169" id="Seg_3139" n="sc" s="T164">
               <ts e="T165" id="Seg_3141" n="e" s="T164">Uː </ts>
               <ts e="T166" id="Seg_3143" n="e" s="T165">ihiger </ts>
               <ts e="T167" id="Seg_3145" n="e" s="T166">mɨlanɨ </ts>
               <ts e="T168" id="Seg_3147" n="e" s="T167">kutallar, </ts>
               <ts e="T169" id="Seg_3149" n="e" s="T168">uːrallar. </ts>
            </ts>
            <ts e="T173" id="Seg_3150" n="sc" s="T170">
               <ts e="T171" id="Seg_3152" n="e" s="T170">Onton </ts>
               <ts e="T172" id="Seg_3154" n="e" s="T171">eŋin-eŋin </ts>
               <ts e="T173" id="Seg_3156" n="e" s="T172">ottoru. </ts>
            </ts>
            <ts e="T181" id="Seg_3157" n="sc" s="T174">
               <ts e="T175" id="Seg_3159" n="e" s="T174">Ol </ts>
               <ts e="T176" id="Seg_3161" n="e" s="T175">gɨna </ts>
               <ts e="T177" id="Seg_3163" n="e" s="T176">gɨna </ts>
               <ts e="T178" id="Seg_3165" n="e" s="T177">kas </ts>
               <ts e="T179" id="Seg_3167" n="e" s="T178">da </ts>
               <ts e="T180" id="Seg_3169" n="e" s="T179">künü </ts>
               <ts e="T181" id="Seg_3171" n="e" s="T180">hɨtɨ͡aktaːk. </ts>
            </ts>
            <ts e="T197" id="Seg_3172" n="sc" s="T182">
               <ts e="T183" id="Seg_3174" n="e" s="T182">Itiː </ts>
               <ts e="T184" id="Seg_3176" n="e" s="T183">da </ts>
               <ts e="T185" id="Seg_3178" n="e" s="T184">kün </ts>
               <ts e="T186" id="Seg_3180" n="e" s="T185">bu͡ollun </ts>
               <ts e="T187" id="Seg_3182" n="e" s="T186">ol </ts>
               <ts e="T188" id="Seg_3184" n="e" s="T187">uːŋ </ts>
               <ts e="T189" id="Seg_3186" n="e" s="T188">hɨtɨjar </ts>
               <ts e="T190" id="Seg_3188" n="e" s="T189">bu͡o, </ts>
               <ts e="T191" id="Seg_3190" n="e" s="T190">tiriːŋ </ts>
               <ts e="T192" id="Seg_3192" n="e" s="T191">emi͡e </ts>
               <ts e="T193" id="Seg_3194" n="e" s="T192">hɨtɨjar </ts>
               <ts e="T194" id="Seg_3196" n="e" s="T193">bu͡o, </ts>
               <ts e="T195" id="Seg_3198" n="e" s="T194">imiges </ts>
               <ts e="T196" id="Seg_3200" n="e" s="T195">bu͡olar </ts>
               <ts e="T197" id="Seg_3202" n="e" s="T196">bu͡o. </ts>
            </ts>
            <ts e="T207" id="Seg_3203" n="sc" s="T198">
               <ts e="T199" id="Seg_3205" n="e" s="T198">Tiriːŋ </ts>
               <ts e="T200" id="Seg_3207" n="e" s="T199">kas </ts>
               <ts e="T201" id="Seg_3209" n="e" s="T200">da </ts>
               <ts e="T202" id="Seg_3211" n="e" s="T201">künü </ts>
               <ts e="T203" id="Seg_3213" n="e" s="T202">hɨtɨjan </ts>
               <ts e="T204" id="Seg_3215" n="e" s="T203">büttegine </ts>
               <ts e="T205" id="Seg_3217" n="e" s="T204">ginileriŋ </ts>
               <ts e="T206" id="Seg_3219" n="e" s="T205">ol </ts>
               <ts e="T207" id="Seg_3221" n="e" s="T206">bulkujallar. </ts>
            </ts>
            <ts e="T217" id="Seg_3222" n="sc" s="T208">
               <ts e="T209" id="Seg_3224" n="e" s="T208">Ol </ts>
               <ts e="T210" id="Seg_3226" n="e" s="T209">tiriːgin </ts>
               <ts e="T211" id="Seg_3228" n="e" s="T210">ɨlan </ts>
               <ts e="T212" id="Seg_3230" n="e" s="T211">barannar </ts>
               <ts e="T213" id="Seg_3232" n="e" s="T212">iti </ts>
               <ts e="T214" id="Seg_3234" n="e" s="T213">kim </ts>
               <ts e="T215" id="Seg_3236" n="e" s="T214">huːnarallar </ts>
               <ts e="T216" id="Seg_3238" n="e" s="T215">ürekke </ts>
               <ts e="T217" id="Seg_3240" n="e" s="T216">barannar. </ts>
            </ts>
            <ts e="T224" id="Seg_3241" n="sc" s="T218">
               <ts e="T219" id="Seg_3243" n="e" s="T218">Delbi </ts>
               <ts e="T220" id="Seg_3245" n="e" s="T219">(huːnnaraldɨlar) </ts>
               <ts e="T221" id="Seg_3247" n="e" s="T220">ol </ts>
               <ts e="T222" id="Seg_3249" n="e" s="T221">gɨnan </ts>
               <ts e="T223" id="Seg_3251" n="e" s="T222">baran </ts>
               <ts e="T224" id="Seg_3253" n="e" s="T223">kuːrdallar. </ts>
            </ts>
            <ts e="T240" id="Seg_3254" n="sc" s="T225">
               <ts e="T226" id="Seg_3256" n="e" s="T225">Kuːrdan </ts>
               <ts e="T227" id="Seg_3258" n="e" s="T226">barannara, </ts>
               <ts e="T228" id="Seg_3260" n="e" s="T227">iti </ts>
               <ts e="T229" id="Seg_3262" n="e" s="T228">tiriːgin </ts>
               <ts e="T230" id="Seg_3264" n="e" s="T229">kimniːller, </ts>
               <ts e="T231" id="Seg_3266" n="e" s="T230">gedereːninen </ts>
               <ts e="T232" id="Seg_3268" n="e" s="T231">ol </ts>
               <ts e="T233" id="Seg_3270" n="e" s="T232">kim </ts>
               <ts e="T234" id="Seg_3272" n="e" s="T233">kaːlbɨt </ts>
               <ts e="T235" id="Seg_3274" n="e" s="T234">tüːlerin </ts>
               <ts e="T236" id="Seg_3276" n="e" s="T235">ɨlallar </ts>
               <ts e="T237" id="Seg_3278" n="e" s="T236">(gedereː-) </ts>
               <ts e="T238" id="Seg_3280" n="e" s="T237">gedereːnniːller, </ts>
               <ts e="T239" id="Seg_3282" n="e" s="T238">hakalɨː </ts>
               <ts e="T240" id="Seg_3284" n="e" s="T239">haŋardakka. </ts>
            </ts>
            <ts e="T257" id="Seg_3285" n="sc" s="T241">
               <ts e="T242" id="Seg_3287" n="e" s="T241">Ontuŋ </ts>
               <ts e="T243" id="Seg_3289" n="e" s="T242">emi͡e </ts>
               <ts e="T244" id="Seg_3291" n="e" s="T243">türgen </ts>
               <ts e="T245" id="Seg_3293" n="e" s="T244">da </ts>
               <ts e="T246" id="Seg_3295" n="e" s="T245">bu͡olbatak, </ts>
               <ts e="T247" id="Seg_3297" n="e" s="T246">dʼogus </ts>
               <ts e="T248" id="Seg_3299" n="e" s="T247">da </ts>
               <ts e="T249" id="Seg_3301" n="e" s="T248">bu͡olbatak, </ts>
               <ts e="T250" id="Seg_3303" n="e" s="T249">dʼaktattar </ts>
               <ts e="T251" id="Seg_3305" n="e" s="T250">bert </ts>
               <ts e="T252" id="Seg_3307" n="e" s="T251">kim </ts>
               <ts e="T253" id="Seg_3309" n="e" s="T252">ɨ͡arakan </ts>
               <ts e="T254" id="Seg_3311" n="e" s="T253">üle </ts>
               <ts e="T255" id="Seg_3313" n="e" s="T254">iti </ts>
               <ts e="T256" id="Seg_3315" n="e" s="T255">dʼaktarga </ts>
               <ts e="T257" id="Seg_3317" n="e" s="T256">da. </ts>
            </ts>
            <ts e="T270" id="Seg_3318" n="sc" s="T258">
               <ts e="T259" id="Seg_3320" n="e" s="T258">Ol </ts>
               <ts e="T260" id="Seg_3322" n="e" s="T259">ihin </ts>
               <ts e="T261" id="Seg_3324" n="e" s="T260">biːrde </ts>
               <ts e="T262" id="Seg_3326" n="e" s="T261">oloroːt, </ts>
               <ts e="T263" id="Seg_3328" n="e" s="T262">onu </ts>
               <ts e="T264" id="Seg_3330" n="e" s="T263">barɨtɨn </ts>
               <ts e="T265" id="Seg_3332" n="e" s="T264">(hataːba- </ts>
               <ts e="T266" id="Seg_3334" n="e" s="T265">oloru͡oktaː-) </ts>
               <ts e="T267" id="Seg_3336" n="e" s="T266">üleli͡ektere </ts>
               <ts e="T268" id="Seg_3338" n="e" s="T267">hu͡oga, </ts>
               <ts e="T269" id="Seg_3340" n="e" s="T268">ɨ͡arakan </ts>
               <ts e="T270" id="Seg_3342" n="e" s="T269">üle. </ts>
            </ts>
            <ts e="T279" id="Seg_3343" n="sc" s="T271">
               <ts e="T272" id="Seg_3345" n="e" s="T271">Onton </ts>
               <ts e="T273" id="Seg_3347" n="e" s="T272">nöŋü͡ö </ts>
               <ts e="T274" id="Seg_3349" n="e" s="T273">kün </ts>
               <ts e="T275" id="Seg_3351" n="e" s="T274">emi͡e </ts>
               <ts e="T276" id="Seg_3353" n="e" s="T275">oŋoru͡oktara, </ts>
               <ts e="T277" id="Seg_3355" n="e" s="T276">ol </ts>
               <ts e="T278" id="Seg_3357" n="e" s="T277">gedereːnni͡ektere </ts>
               <ts e="T279" id="Seg_3359" n="e" s="T278">emi͡e. </ts>
            </ts>
            <ts e="T292" id="Seg_3360" n="sc" s="T280">
               <ts e="T281" id="Seg_3362" n="e" s="T280">Gedereːnnen </ts>
               <ts e="T282" id="Seg_3364" n="e" s="T281">büttekterine, </ts>
               <ts e="T283" id="Seg_3366" n="e" s="T282">ol </ts>
               <ts e="T284" id="Seg_3368" n="e" s="T283">harɨːŋ </ts>
               <ts e="T285" id="Seg_3370" n="e" s="T284">kim </ts>
               <ts e="T286" id="Seg_3372" n="e" s="T285">kime </ts>
               <ts e="T287" id="Seg_3374" n="e" s="T286">hu͡ok </ts>
               <ts e="T288" id="Seg_3376" n="e" s="T287">bu͡olar </ts>
               <ts e="T289" id="Seg_3378" n="e" s="T288">bu͡o </ts>
               <ts e="T290" id="Seg_3380" n="e" s="T289">tüːte </ts>
               <ts e="T291" id="Seg_3382" n="e" s="T290">hu͡ok </ts>
               <ts e="T292" id="Seg_3384" n="e" s="T291">bu͡olar. </ts>
            </ts>
            <ts e="T304" id="Seg_3385" n="sc" s="T293">
               <ts e="T294" id="Seg_3387" n="e" s="T293">Ontuŋ </ts>
               <ts e="T295" id="Seg_3389" n="e" s="T294">deksi </ts>
               <ts e="T296" id="Seg_3391" n="e" s="T295">bu͡olu͡on, </ts>
               <ts e="T297" id="Seg_3393" n="e" s="T296">iti </ts>
               <ts e="T298" id="Seg_3395" n="e" s="T297">kɨhɨ͡agɨnan, </ts>
               <ts e="T299" id="Seg_3397" n="e" s="T298">tu͡ogɨnan, </ts>
               <ts e="T300" id="Seg_3399" n="e" s="T299">eŋin </ts>
               <ts e="T301" id="Seg_3401" n="e" s="T300">eŋininen </ts>
               <ts e="T302" id="Seg_3403" n="e" s="T301">tarbɨːllar </ts>
               <ts e="T303" id="Seg_3405" n="e" s="T302">kɨhɨ͡aktɨːllar </ts>
               <ts e="T304" id="Seg_3407" n="e" s="T303">kanʼɨːllar. </ts>
            </ts>
            <ts e="T311" id="Seg_3408" n="sc" s="T305">
               <ts e="T306" id="Seg_3410" n="e" s="T305">Bahagɨnan </ts>
               <ts e="T307" id="Seg_3412" n="e" s="T306">da </ts>
               <ts e="T308" id="Seg_3414" n="e" s="T307">eŋin </ts>
               <ts e="T309" id="Seg_3416" n="e" s="T308">eŋin </ts>
               <ts e="T310" id="Seg_3418" n="e" s="T309">deksiliːller, </ts>
               <ts e="T311" id="Seg_3420" n="e" s="T310">kanʼɨːllar. </ts>
            </ts>
            <ts e="T319" id="Seg_3421" n="sc" s="T312">
               <ts e="T313" id="Seg_3423" n="e" s="T312">Tu͡ok </ts>
               <ts e="T314" id="Seg_3425" n="e" s="T313">kuhagan </ts>
               <ts e="T315" id="Seg_3427" n="e" s="T314">baːrɨn </ts>
               <ts e="T316" id="Seg_3429" n="e" s="T315">barɨtɨn </ts>
               <ts e="T317" id="Seg_3431" n="e" s="T316">iti </ts>
               <ts e="T318" id="Seg_3433" n="e" s="T317">ɨlattan </ts>
               <ts e="T319" id="Seg_3435" n="e" s="T318">iheller. </ts>
            </ts>
            <ts e="T329" id="Seg_3436" n="sc" s="T320">
               <ts e="T321" id="Seg_3438" n="e" s="T320">Ol </ts>
               <ts e="T322" id="Seg_3440" n="e" s="T321">harɨːŋ, </ts>
               <ts e="T323" id="Seg_3442" n="e" s="T322">ol </ts>
               <ts e="T324" id="Seg_3444" n="e" s="T323">gɨnan </ts>
               <ts e="T325" id="Seg_3446" n="e" s="T324">baran, </ts>
               <ts e="T326" id="Seg_3448" n="e" s="T325">kördökkö </ts>
               <ts e="T327" id="Seg_3450" n="e" s="T326">üčügej </ts>
               <ts e="T328" id="Seg_3452" n="e" s="T327">bagaj </ts>
               <ts e="T329" id="Seg_3454" n="e" s="T328">bu͡olar. </ts>
            </ts>
            <ts e="T343" id="Seg_3455" n="sc" s="T330">
               <ts e="T331" id="Seg_3457" n="e" s="T330">Onton </ts>
               <ts e="T332" id="Seg_3459" n="e" s="T331">harɨːŋ </ts>
               <ts e="T333" id="Seg_3461" n="e" s="T332">ile </ts>
               <ts e="T334" id="Seg_3463" n="e" s="T333">harɨː </ts>
               <ts e="T335" id="Seg_3465" n="e" s="T334">bu͡olu͡on, </ts>
               <ts e="T336" id="Seg_3467" n="e" s="T335">ol </ts>
               <ts e="T337" id="Seg_3469" n="e" s="T336">gɨnan </ts>
               <ts e="T338" id="Seg_3471" n="e" s="T337">bu͡o </ts>
               <ts e="T339" id="Seg_3473" n="e" s="T338">ɨːstaːk </ts>
               <ts e="T340" id="Seg_3475" n="e" s="T339">bu͡olu͡on, </ts>
               <ts e="T341" id="Seg_3477" n="e" s="T340">ontugun </ts>
               <ts e="T342" id="Seg_3479" n="e" s="T341">emi͡e </ts>
               <ts e="T343" id="Seg_3481" n="e" s="T342">imiti͡ekteːkter. </ts>
            </ts>
            <ts e="T353" id="Seg_3482" n="sc" s="T344">
               <ts e="T345" id="Seg_3484" n="e" s="T344">Imiteːri </ts>
               <ts e="T346" id="Seg_3486" n="e" s="T345">gɨnnaktarɨna, </ts>
               <ts e="T347" id="Seg_3488" n="e" s="T346">de </ts>
               <ts e="T348" id="Seg_3490" n="e" s="T347">kimniːller, </ts>
               <ts e="T349" id="Seg_3492" n="e" s="T348">kɨːh </ts>
               <ts e="T350" id="Seg_3494" n="e" s="T349">ogolorun </ts>
               <ts e="T351" id="Seg_3496" n="e" s="T350">ɨgɨraːččɨlar </ts>
               <ts e="T352" id="Seg_3498" n="e" s="T351">togo </ts>
               <ts e="T353" id="Seg_3500" n="e" s="T352">ere. </ts>
            </ts>
            <ts e="T365" id="Seg_3501" n="sc" s="T354">
               <ts e="T355" id="Seg_3503" n="e" s="T354">Iti </ts>
               <ts e="T356" id="Seg_3505" n="e" s="T355">kimi͡eke </ts>
               <ts e="T357" id="Seg_3507" n="e" s="T356">taŋas </ts>
               <ts e="T358" id="Seg_3509" n="e" s="T357">ɨjaːnar </ts>
               <ts e="T359" id="Seg_3511" n="e" s="T358">kimnerge </ts>
               <ts e="T360" id="Seg_3513" n="e" s="T359">ɨjɨːllar </ts>
               <ts e="T361" id="Seg_3515" n="e" s="T360">bu͡o </ts>
               <ts e="T362" id="Seg_3517" n="e" s="T361">ol </ts>
               <ts e="T363" id="Seg_3519" n="e" s="T362">tiriːgin, </ts>
               <ts e="T364" id="Seg_3521" n="e" s="T363">taŋas </ts>
               <ts e="T365" id="Seg_3523" n="e" s="T364">ɨjaːnar. </ts>
            </ts>
            <ts e="T370" id="Seg_3524" n="sc" s="T366">
               <ts e="T367" id="Seg_3526" n="e" s="T366">Ontugun </ts>
               <ts e="T368" id="Seg_3528" n="e" s="T367">battan </ts>
               <ts e="T369" id="Seg_3530" n="e" s="T368">turu͡okkun </ts>
               <ts e="T370" id="Seg_3532" n="e" s="T369">naːda. </ts>
            </ts>
            <ts e="T385" id="Seg_3533" n="sc" s="T371">
               <ts e="T372" id="Seg_3535" n="e" s="T371">Hɨrganɨ </ts>
               <ts e="T373" id="Seg_3537" n="e" s="T372">ti͡ere </ts>
               <ts e="T374" id="Seg_3539" n="e" s="T373">uːran </ts>
               <ts e="T375" id="Seg_3541" n="e" s="T374">barannar </ts>
               <ts e="T376" id="Seg_3543" n="e" s="T375">emi͡e </ts>
               <ts e="T377" id="Seg_3545" n="e" s="T376">battan </ts>
               <ts e="T378" id="Seg_3547" n="e" s="T377">turu͡okkun </ts>
               <ts e="T379" id="Seg_3549" n="e" s="T378">naːda </ts>
               <ts e="T380" id="Seg_3551" n="e" s="T379">ol </ts>
               <ts e="T381" id="Seg_3553" n="e" s="T380">tiriːni </ts>
               <ts e="T382" id="Seg_3555" n="e" s="T381">uːran </ts>
               <ts e="T383" id="Seg_3557" n="e" s="T382">baraŋŋɨn </ts>
               <ts e="T384" id="Seg_3559" n="e" s="T383">harɨː </ts>
               <ts e="T385" id="Seg_3561" n="e" s="T384">bu͡olu͡ogun. </ts>
            </ts>
            <ts e="T390" id="Seg_3562" n="sc" s="T386">
               <ts e="T387" id="Seg_3564" n="e" s="T386">De </ts>
               <ts e="T388" id="Seg_3566" n="e" s="T387">ol </ts>
               <ts e="T389" id="Seg_3568" n="e" s="T388">battan </ts>
               <ts e="T390" id="Seg_3570" n="e" s="T389">turagɨn. </ts>
            </ts>
            <ts e="T417" id="Seg_3571" n="sc" s="T391">
               <ts e="T392" id="Seg_3573" n="e" s="T391">Ol </ts>
               <ts e="T393" id="Seg_3575" n="e" s="T392">battan </ts>
               <ts e="T394" id="Seg_3577" n="e" s="T393">turdakkɨna </ts>
               <ts e="T395" id="Seg_3579" n="e" s="T394">eni͡eke </ts>
               <ts e="T396" id="Seg_3581" n="e" s="T395">bi͡ereller </ts>
               <ts e="T397" id="Seg_3583" n="e" s="T396">iti </ts>
               <ts e="T398" id="Seg_3585" n="e" s="T397">kimi </ts>
               <ts e="T399" id="Seg_3587" n="e" s="T398">bɨ͡arɨ, </ts>
               <ts e="T400" id="Seg_3589" n="e" s="T399">busput </ts>
               <ts e="T401" id="Seg_3591" n="e" s="T400">taba </ts>
               <ts e="T402" id="Seg_3593" n="e" s="T401">bɨ͡arɨn. </ts>
               <ts e="T403" id="Seg_3595" n="e" s="T402">Ol </ts>
               <ts e="T404" id="Seg_3597" n="e" s="T403">bɨ͡arɨ </ts>
               <ts e="T405" id="Seg_3599" n="e" s="T404">battɨː </ts>
               <ts e="T406" id="Seg_3601" n="e" s="T405">turan </ts>
               <ts e="T407" id="Seg_3603" n="e" s="T406">kɨːs </ts>
               <ts e="T408" id="Seg_3605" n="e" s="T407">kɨːs </ts>
               <ts e="T409" id="Seg_3607" n="e" s="T408">ogo </ts>
               <ts e="T410" id="Seg_3609" n="e" s="T409">(ɨstɨ͡an-) </ts>
               <ts e="T411" id="Seg_3611" n="e" s="T410">ɨstɨ͡aktaːk. </ts>
               <ts e="T412" id="Seg_3613" n="e" s="T411">Ɨstaː, </ts>
               <ts e="T413" id="Seg_3615" n="e" s="T412">diːller </ts>
               <ts e="T414" id="Seg_3617" n="e" s="T413">bu͡o, </ts>
               <ts e="T415" id="Seg_3619" n="e" s="T414">kajdi͡ek </ts>
               <ts e="T416" id="Seg_3621" n="e" s="T415">bu͡olu͡oŋuj </ts>
               <ts e="T417" id="Seg_3623" n="e" s="T416">ke? </ts>
            </ts>
            <ts e="T427" id="Seg_3624" n="sc" s="T418">
               <ts e="T419" id="Seg_3626" n="e" s="T418">Bagarbat </ts>
               <ts e="T420" id="Seg_3628" n="e" s="T419">bu͡olu͡o </ts>
               <ts e="T421" id="Seg_3630" n="e" s="T420">bu͡olu͡o, </ts>
               <ts e="T422" id="Seg_3632" n="e" s="T421">ontuŋ </ts>
               <ts e="T423" id="Seg_3634" n="e" s="T422">amtana </ts>
               <ts e="T424" id="Seg_3636" n="e" s="T423">kuhagana, </ts>
               <ts e="T425" id="Seg_3638" n="e" s="T424">hin </ts>
               <ts e="T426" id="Seg_3640" n="e" s="T425">turagɨn </ts>
               <ts e="T427" id="Seg_3642" n="e" s="T426">bu͡o. </ts>
            </ts>
            <ts e="T432" id="Seg_3643" n="sc" s="T428">
               <ts e="T429" id="Seg_3645" n="e" s="T428">Istegin </ts>
               <ts e="T430" id="Seg_3647" n="e" s="T429">bu͡o </ts>
               <ts e="T431" id="Seg_3649" n="e" s="T430">inʼeŋ </ts>
               <ts e="T432" id="Seg_3651" n="e" s="T431">haŋatɨn. </ts>
            </ts>
            <ts e="T442" id="Seg_3652" n="sc" s="T433">
               <ts e="T434" id="Seg_3654" n="e" s="T433">Ol </ts>
               <ts e="T435" id="Seg_3656" n="e" s="T434">tura </ts>
               <ts e="T436" id="Seg_3658" n="e" s="T435">tura </ts>
               <ts e="T437" id="Seg_3660" n="e" s="T436">ɨstɨː </ts>
               <ts e="T438" id="Seg_3662" n="e" s="T437">turagɨn </ts>
               <ts e="T439" id="Seg_3664" n="e" s="T438">ol </ts>
               <ts e="T440" id="Seg_3666" n="e" s="T439">kimi, </ts>
               <ts e="T441" id="Seg_3668" n="e" s="T440">bɨ͡arɨ, </ts>
               <ts e="T442" id="Seg_3670" n="e" s="T441">turaŋŋɨn. </ts>
            </ts>
            <ts e="T461" id="Seg_3671" n="sc" s="T443">
               <ts e="T444" id="Seg_3673" n="e" s="T443">Ginileriŋ </ts>
               <ts e="T445" id="Seg_3675" n="e" s="T444">ol </ts>
               <ts e="T446" id="Seg_3677" n="e" s="T445">(bɨ͡a-) </ts>
               <ts e="T447" id="Seg_3679" n="e" s="T446">ol </ts>
               <ts e="T448" id="Seg_3681" n="e" s="T447">ɨstaːbɨt </ts>
               <ts e="T449" id="Seg_3683" n="e" s="T448">bɨ͡argɨn </ts>
               <ts e="T450" id="Seg_3685" n="e" s="T449">ɨla </ts>
               <ts e="T451" id="Seg_3687" n="e" s="T450">ɨlallar, </ts>
               <ts e="T452" id="Seg_3689" n="e" s="T451">ol </ts>
               <ts e="T453" id="Seg_3691" n="e" s="T452">tiriːgin </ts>
               <ts e="T454" id="Seg_3693" n="e" s="T453">ol </ts>
               <ts e="T455" id="Seg_3695" n="e" s="T454">kimneːbit, </ts>
               <ts e="T456" id="Seg_3697" n="e" s="T455">gedereːnneːbit </ts>
               <ts e="T457" id="Seg_3699" n="e" s="T456">tiriːgin </ts>
               <ts e="T458" id="Seg_3701" n="e" s="T457">ɨlan </ts>
               <ts e="T459" id="Seg_3703" n="e" s="T458">barannar, </ts>
               <ts e="T460" id="Seg_3705" n="e" s="T459">ol </ts>
               <ts e="T461" id="Seg_3707" n="e" s="T460">biheller. </ts>
            </ts>
            <ts e="T469" id="Seg_3708" n="sc" s="T462">
               <ts e="T463" id="Seg_3710" n="e" s="T462">Ol </ts>
               <ts e="T464" id="Seg_3712" n="e" s="T463">gɨnan </ts>
               <ts e="T465" id="Seg_3714" n="e" s="T464">baran </ts>
               <ts e="T466" id="Seg_3716" n="e" s="T465">(hoŋo-) </ts>
               <ts e="T467" id="Seg_3718" n="e" s="T466">hoŋohoːnunan </ts>
               <ts e="T468" id="Seg_3720" n="e" s="T467">hoŋohoːnnuːllar </ts>
               <ts e="T469" id="Seg_3722" n="e" s="T468">iliːlerinen. </ts>
            </ts>
            <ts e="T490" id="Seg_3723" n="sc" s="T470">
               <ts e="T471" id="Seg_3725" n="e" s="T470">Biːr </ts>
               <ts e="T472" id="Seg_3727" n="e" s="T471">iliːnnen </ts>
               <ts e="T473" id="Seg_3729" n="e" s="T472">tutar </ts>
               <ts e="T474" id="Seg_3731" n="e" s="T473">ol </ts>
               <ts e="T475" id="Seg_3733" n="e" s="T474">tiriːgin, </ts>
               <ts e="T476" id="Seg_3735" n="e" s="T475">biːr </ts>
               <ts e="T477" id="Seg_3737" n="e" s="T476">iliːnnen </ts>
               <ts e="T478" id="Seg_3739" n="e" s="T477">hoŋohoːŋŋun </ts>
               <ts e="T479" id="Seg_3741" n="e" s="T478">tutar </ts>
               <ts e="T480" id="Seg_3743" n="e" s="T479">bu͡o, </ts>
               <ts e="T481" id="Seg_3745" n="e" s="T480">ol </ts>
               <ts e="T482" id="Seg_3747" n="e" s="T481">bu͡o </ts>
               <ts e="T483" id="Seg_3749" n="e" s="T482">üːhetten </ts>
               <ts e="T484" id="Seg_3751" n="e" s="T483">allaraːga </ts>
               <ts e="T485" id="Seg_3753" n="e" s="T484">di͡eri </ts>
               <ts e="T486" id="Seg_3755" n="e" s="T485">innʼe </ts>
               <ts e="T487" id="Seg_3757" n="e" s="T486">hoŋohoːnnuːr, </ts>
               <ts e="T488" id="Seg_3759" n="e" s="T487">eː </ts>
               <ts e="T489" id="Seg_3761" n="e" s="T488">bihe </ts>
               <ts e="T490" id="Seg_3763" n="e" s="T489">bihe. </ts>
            </ts>
            <ts e="T503" id="Seg_3764" n="sc" s="T491">
               <ts e="T492" id="Seg_3766" n="e" s="T491">De </ts>
               <ts e="T493" id="Seg_3768" n="e" s="T492">ol </ts>
               <ts e="T494" id="Seg_3770" n="e" s="T493">gɨnan </ts>
               <ts e="T495" id="Seg_3772" n="e" s="T494">baran </ts>
               <ts e="T496" id="Seg_3774" n="e" s="T495">ginileriŋ </ts>
               <ts e="T497" id="Seg_3776" n="e" s="T496">kɨːh </ts>
               <ts e="T498" id="Seg_3778" n="e" s="T497">ogolor </ts>
               <ts e="T499" id="Seg_3780" n="e" s="T498">kahan </ts>
               <ts e="T500" id="Seg_3782" n="e" s="T499">da </ts>
               <ts e="T501" id="Seg_3784" n="e" s="T500">turu͡oktarɨn </ts>
               <ts e="T502" id="Seg_3786" n="e" s="T501">bagaraːččɨta </ts>
               <ts e="T503" id="Seg_3788" n="e" s="T502">hu͡oktar. </ts>
            </ts>
            <ts e="T514" id="Seg_3789" n="sc" s="T504">
               <ts e="T505" id="Seg_3791" n="e" s="T504">Battan </ts>
               <ts e="T506" id="Seg_3793" n="e" s="T505">turu͡okkun </ts>
               <ts e="T507" id="Seg_3795" n="e" s="T506">naːda </ts>
               <ts e="T508" id="Seg_3797" n="e" s="T507">bu͡o </ts>
               <ts e="T509" id="Seg_3799" n="e" s="T508">ör </ts>
               <ts e="T510" id="Seg_3801" n="e" s="T509">bagajɨ </ts>
               <ts e="T511" id="Seg_3803" n="e" s="T510">turu͡okkun </ts>
               <ts e="T512" id="Seg_3805" n="e" s="T511">(naː-) </ts>
               <ts e="T513" id="Seg_3807" n="e" s="T512">turu͡oktaːkkɨn </ts>
               <ts e="T514" id="Seg_3809" n="e" s="T513">onno. </ts>
            </ts>
            <ts e="T522" id="Seg_3810" n="sc" s="T515">
               <ts e="T516" id="Seg_3812" n="e" s="T515">A </ts>
               <ts e="T517" id="Seg_3814" n="e" s="T516">ɨstɨːrɨŋ </ts>
               <ts e="T518" id="Seg_3816" n="e" s="T517">baːr </ts>
               <ts e="T519" id="Seg_3818" n="e" s="T518">de </ts>
               <ts e="T520" id="Seg_3820" n="e" s="T519">kuhagan </ts>
               <ts e="T521" id="Seg_3822" n="e" s="T520">da </ts>
               <ts e="T522" id="Seg_3824" n="e" s="T521">kuhagan. </ts>
            </ts>
            <ts e="T532" id="Seg_3825" n="sc" s="T523">
               <ts e="T524" id="Seg_3827" n="e" s="T523">Eː </ts>
               <ts e="T525" id="Seg_3829" n="e" s="T524">ɨstɨ͡aŋ, </ts>
               <ts e="T526" id="Seg_3831" n="e" s="T525">kihi </ts>
               <ts e="T527" id="Seg_3833" n="e" s="T526">da </ts>
               <ts e="T528" id="Seg_3835" n="e" s="T527">hürege </ts>
               <ts e="T529" id="Seg_3837" n="e" s="T528">loksuju͡ok </ts>
               <ts e="T530" id="Seg_3839" n="e" s="T529">horogor </ts>
               <ts e="T531" id="Seg_3841" n="e" s="T530">bu͡olar </ts>
               <ts e="T532" id="Seg_3843" n="e" s="T531">bu͡o. </ts>
            </ts>
            <ts e="T543" id="Seg_3844" n="sc" s="T533">
               <ts e="T534" id="Seg_3846" n="e" s="T533">Ol </ts>
               <ts e="T535" id="Seg_3848" n="e" s="T534">ihin </ts>
               <ts e="T536" id="Seg_3850" n="e" s="T535">(kɨr-) </ts>
               <ts e="T537" id="Seg_3852" n="e" s="T536">kɨːh </ts>
               <ts e="T538" id="Seg_3854" n="e" s="T537">ogokoːttoruŋ </ts>
               <ts e="T539" id="Seg_3856" n="e" s="T538">küreneller </ts>
               <ts e="T540" id="Seg_3858" n="e" s="T539">bu͡o </ts>
               <ts e="T541" id="Seg_3860" n="e" s="T540">ɨstaːmaːrɨlar, </ts>
               <ts e="T542" id="Seg_3862" n="e" s="T541">baran </ts>
               <ts e="T543" id="Seg_3864" n="e" s="T542">kaːlallar. </ts>
            </ts>
            <ts e="T553" id="Seg_3865" n="sc" s="T544">
               <ts e="T545" id="Seg_3867" n="e" s="T544">Kim </ts>
               <ts e="T546" id="Seg_3869" n="e" s="T545">kɨːh </ts>
               <ts e="T547" id="Seg_3871" n="e" s="T546">ogoloru </ts>
               <ts e="T548" id="Seg_3873" n="e" s="T547">ügüstük </ts>
               <ts e="T549" id="Seg_3875" n="e" s="T548">ɨgɨraːččɨlar, </ts>
               <ts e="T550" id="Seg_3877" n="e" s="T549">u͡ol </ts>
               <ts e="T551" id="Seg_3879" n="e" s="T550">ogotoːgor, </ts>
               <ts e="T552" id="Seg_3881" n="e" s="T551">togo </ts>
               <ts e="T553" id="Seg_3883" n="e" s="T552">ere. </ts>
            </ts>
            <ts e="T570" id="Seg_3884" n="sc" s="T554">
               <ts e="T555" id="Seg_3886" n="e" s="T554">De </ts>
               <ts e="T556" id="Seg_3888" n="e" s="T555">ol </ts>
               <ts e="T557" id="Seg_3890" n="e" s="T556">ol </ts>
               <ts e="T558" id="Seg_3892" n="e" s="T557">gɨnan </ts>
               <ts e="T559" id="Seg_3894" n="e" s="T558">baran </ts>
               <ts e="T560" id="Seg_3896" n="e" s="T559">ol </ts>
               <ts e="T561" id="Seg_3898" n="e" s="T560">kiminen, </ts>
               <ts e="T562" id="Seg_3900" n="e" s="T561">delbi </ts>
               <ts e="T563" id="Seg_3902" n="e" s="T562">kimne </ts>
               <ts e="T564" id="Seg_3904" n="e" s="T563">hoŋohoːnunan </ts>
               <ts e="T565" id="Seg_3906" n="e" s="T564">delbi </ts>
               <ts e="T566" id="Seg_3908" n="e" s="T565">kimniː, </ts>
               <ts e="T567" id="Seg_3910" n="e" s="T566">kimneːtiler </ts>
               <ts e="T568" id="Seg_3912" n="e" s="T567">bu͡o </ts>
               <ts e="T569" id="Seg_3914" n="e" s="T568">ol </ts>
               <ts e="T570" id="Seg_3916" n="e" s="T569">tiriːgin. </ts>
            </ts>
            <ts e="T577" id="Seg_3917" n="sc" s="T571">
               <ts e="T572" id="Seg_3919" n="e" s="T571">Ol </ts>
               <ts e="T573" id="Seg_3921" n="e" s="T572">hoŋohoːnnuːruŋ </ts>
               <ts e="T574" id="Seg_3923" n="e" s="T573">emi͡e </ts>
               <ts e="T575" id="Seg_3925" n="e" s="T574">ör </ts>
               <ts e="T576" id="Seg_3927" n="e" s="T575">bagajɨ </ts>
               <ts e="T577" id="Seg_3929" n="e" s="T576">bu͡olla. </ts>
            </ts>
            <ts e="T586" id="Seg_3930" n="sc" s="T578">
               <ts e="T579" id="Seg_3932" n="e" s="T578">Nöŋü͡ö </ts>
               <ts e="T580" id="Seg_3934" n="e" s="T579">kün </ts>
               <ts e="T581" id="Seg_3936" n="e" s="T580">emi͡e </ts>
               <ts e="T582" id="Seg_3938" n="e" s="T581">turu͡oktaːkkɨn </ts>
               <ts e="T583" id="Seg_3940" n="e" s="T582">bu͡olla </ts>
               <ts e="T584" id="Seg_3942" n="e" s="T583">ol </ts>
               <ts e="T585" id="Seg_3944" n="e" s="T584">ɨstɨː </ts>
               <ts e="T586" id="Seg_3946" n="e" s="T585">ɨstɨːgɨn. </ts>
            </ts>
            <ts e="T592" id="Seg_3947" n="sc" s="T587">
               <ts e="T588" id="Seg_3949" n="e" s="T587">Bagarbat </ts>
               <ts e="T589" id="Seg_3951" n="e" s="T588">bu͡olagɨn </ts>
               <ts e="T590" id="Seg_3953" n="e" s="T589">hüregiŋ </ts>
               <ts e="T591" id="Seg_3955" n="e" s="T590">loksujar </ts>
               <ts e="T592" id="Seg_3957" n="e" s="T591">bu͡olla. </ts>
            </ts>
            <ts e="T604" id="Seg_3958" n="sc" s="T593">
               <ts e="T594" id="Seg_3960" n="e" s="T593">De </ts>
               <ts e="T595" id="Seg_3962" n="e" s="T594">emi͡e </ts>
               <ts e="T596" id="Seg_3964" n="e" s="T595">hin </ts>
               <ts e="T597" id="Seg_3966" n="e" s="T596">ɨstɨːgɨn </ts>
               <ts e="T598" id="Seg_3968" n="e" s="T597">bu͡olla, </ts>
               <ts e="T599" id="Seg_3970" n="e" s="T598">kajdi͡ek </ts>
               <ts e="T600" id="Seg_3972" n="e" s="T599">bu͡olu͡oŋuj, </ts>
               <ts e="T601" id="Seg_3974" n="e" s="T600">inʼeŋ </ts>
               <ts e="T602" id="Seg_3976" n="e" s="T601">haŋatɨn </ts>
               <ts e="T603" id="Seg_3978" n="e" s="T602">istegin </ts>
               <ts e="T604" id="Seg_3980" n="e" s="T603">bu͡o. </ts>
            </ts>
            <ts e="T616" id="Seg_3981" n="sc" s="T605">
               <ts e="T606" id="Seg_3983" n="e" s="T605">De </ts>
               <ts e="T607" id="Seg_3985" n="e" s="T606">ol </ts>
               <ts e="T608" id="Seg_3987" n="e" s="T607">turan </ts>
               <ts e="T609" id="Seg_3989" n="e" s="T608">ɨstɨː </ts>
               <ts e="T610" id="Seg_3991" n="e" s="T609">ɨstɨːgɨn </ts>
               <ts e="T611" id="Seg_3993" n="e" s="T610">de </ts>
               <ts e="T612" id="Seg_3995" n="e" s="T611">muŋnanan </ts>
               <ts e="T613" id="Seg_3997" n="e" s="T612">muŋnanan, </ts>
               <ts e="T614" id="Seg_3999" n="e" s="T613">de </ts>
               <ts e="T615" id="Seg_4001" n="e" s="T614">bütebit </ts>
               <ts e="T616" id="Seg_4003" n="e" s="T615">bu͡olla. </ts>
            </ts>
            <ts e="T625" id="Seg_4004" n="sc" s="T617">
               <ts e="T618" id="Seg_4006" n="e" s="T617">Kahɨs </ts>
               <ts e="T619" id="Seg_4008" n="e" s="T618">eme </ts>
               <ts e="T620" id="Seg_4010" n="e" s="T619">künüger </ts>
               <ts e="T621" id="Seg_4012" n="e" s="T620">de </ts>
               <ts e="T622" id="Seg_4014" n="e" s="T621">ol </ts>
               <ts e="T623" id="Seg_4016" n="e" s="T622">kɨhɨ͡ak </ts>
               <ts e="T624" id="Seg_4018" n="e" s="T623">kiminen </ts>
               <ts e="T625" id="Seg_4020" n="e" s="T624">hoŋohoːnnon. </ts>
            </ts>
            <ts e="T645" id="Seg_4021" n="sc" s="T626">
               <ts e="T627" id="Seg_4023" n="e" s="T626">Onton </ts>
               <ts e="T628" id="Seg_4025" n="e" s="T627">kojut </ts>
               <ts e="T629" id="Seg_4027" n="e" s="T628">iti </ts>
               <ts e="T630" id="Seg_4029" n="e" s="T629">harɨː </ts>
               <ts e="T631" id="Seg_4031" n="e" s="T630">bu͡ol </ts>
               <ts e="T632" id="Seg_4033" n="e" s="T631">bukatɨn </ts>
               <ts e="T633" id="Seg_4035" n="e" s="T632">harɨː </ts>
               <ts e="T634" id="Seg_4037" n="e" s="T633">bu͡ol </ts>
               <ts e="T635" id="Seg_4039" n="e" s="T634">üčügej </ts>
               <ts e="T636" id="Seg_4041" n="e" s="T635">bu͡olu͡on, </ts>
               <ts e="T637" id="Seg_4043" n="e" s="T636">imiges </ts>
               <ts e="T638" id="Seg_4045" n="e" s="T637">bu͡olu͡on, </ts>
               <ts e="T639" id="Seg_4047" n="e" s="T638">iti </ts>
               <ts e="T640" id="Seg_4049" n="e" s="T639">eŋin </ts>
               <ts e="T641" id="Seg_4051" n="e" s="T640">eŋinnik </ts>
               <ts e="T642" id="Seg_4053" n="e" s="T641">iti </ts>
               <ts e="T643" id="Seg_4055" n="e" s="T642">kimniː </ts>
               <ts e="T644" id="Seg_4057" n="e" s="T643">hataːtɨbɨt </ts>
               <ts e="T645" id="Seg_4059" n="e" s="T644">bu͡o. </ts>
            </ts>
            <ts e="T649" id="Seg_4060" n="sc" s="T646">
               <ts e="T647" id="Seg_4062" n="e" s="T646">Kɨhɨ͡aktan, </ts>
               <ts e="T648" id="Seg_4064" n="e" s="T647">gedereːnnen </ts>
               <ts e="T649" id="Seg_4066" n="e" s="T648">oŋorobut. </ts>
            </ts>
            <ts e="T658" id="Seg_4067" n="sc" s="T650">
               <ts e="T651" id="Seg_4069" n="e" s="T650">Ontuŋ </ts>
               <ts e="T652" id="Seg_4071" n="e" s="T651">harɨː </ts>
               <ts e="T653" id="Seg_4073" n="e" s="T652">olus </ts>
               <ts e="T654" id="Seg_4075" n="e" s="T653">(maŋan) </ts>
               <ts e="T655" id="Seg_4077" n="e" s="T654">bu͡olaːrɨ </ts>
               <ts e="T656" id="Seg_4079" n="e" s="T655">tože </ts>
               <ts e="T657" id="Seg_4081" n="e" s="T656">hin </ts>
               <ts e="T658" id="Seg_4083" n="e" s="T657">kuhagan. </ts>
            </ts>
            <ts e="T667" id="Seg_4084" n="sc" s="T659">
               <ts e="T660" id="Seg_4086" n="e" s="T659">Ol </ts>
               <ts e="T661" id="Seg_4088" n="e" s="T660">ihin </ts>
               <ts e="T662" id="Seg_4090" n="e" s="T661">harɨːgɨn </ts>
               <ts e="T663" id="Seg_4092" n="e" s="T662">iti </ts>
               <ts e="T664" id="Seg_4094" n="e" s="T663">kimi͡eke </ts>
               <ts e="T665" id="Seg_4096" n="e" s="T664">ɨhaːrallar </ts>
               <ts e="T666" id="Seg_4098" n="e" s="T665">ɨhaːrallar </ts>
               <ts e="T667" id="Seg_4100" n="e" s="T666">buru͡oga. </ts>
            </ts>
            <ts e="T675" id="Seg_4101" n="sc" s="T668">
               <ts e="T669" id="Seg_4103" n="e" s="T668">Ol </ts>
               <ts e="T670" id="Seg_4105" n="e" s="T669">harɨːgɨn </ts>
               <ts e="T671" id="Seg_4107" n="e" s="T670">ɨlan </ts>
               <ts e="T672" id="Seg_4109" n="e" s="T671">barannar </ts>
               <ts e="T673" id="Seg_4111" n="e" s="T672">dʼi͡e </ts>
               <ts e="T674" id="Seg_4113" n="e" s="T673">ihiger </ts>
               <ts e="T675" id="Seg_4115" n="e" s="T674">ɨjɨːllar. </ts>
            </ts>
            <ts e="T687" id="Seg_4116" n="sc" s="T676">
               <ts e="T677" id="Seg_4118" n="e" s="T676">Dʼi͡e </ts>
               <ts e="T678" id="Seg_4120" n="e" s="T677">da </ts>
               <ts e="T679" id="Seg_4122" n="e" s="T678">bu͡olu͡oj </ts>
               <ts e="T680" id="Seg_4124" n="e" s="T679">iti </ts>
               <ts e="T681" id="Seg_4126" n="e" s="T680">kim </ts>
               <ts e="T682" id="Seg_4128" n="e" s="T681">uraha </ts>
               <ts e="T683" id="Seg_4130" n="e" s="T682">ihiger, </ts>
               <ts e="T684" id="Seg_4132" n="e" s="T683">uraha </ts>
               <ts e="T685" id="Seg_4134" n="e" s="T684">dʼi͡e </ts>
               <ts e="T686" id="Seg_4136" n="e" s="T685">ihiger </ts>
               <ts e="T687" id="Seg_4138" n="e" s="T686">ɨjɨːllar. </ts>
            </ts>
            <ts e="T694" id="Seg_4139" n="sc" s="T688">
               <ts e="T689" id="Seg_4141" n="e" s="T688">Onno </ts>
               <ts e="T690" id="Seg_4143" n="e" s="T689">buru͡oga </ts>
               <ts e="T691" id="Seg_4145" n="e" s="T690">ontuŋ </ts>
               <ts e="T692" id="Seg_4147" n="e" s="T691">ɨksa </ts>
               <ts e="T693" id="Seg_4149" n="e" s="T692">turar </ts>
               <ts e="T694" id="Seg_4151" n="e" s="T693">bu͡o. </ts>
            </ts>
            <ts e="T701" id="Seg_4152" n="sc" s="T695">
               <ts e="T696" id="Seg_4154" n="e" s="T695">A </ts>
               <ts e="T697" id="Seg_4156" n="e" s="T696">horok </ts>
               <ts e="T698" id="Seg_4158" n="e" s="T697">dʼaktattar </ts>
               <ts e="T699" id="Seg_4160" n="e" s="T698">innʼe </ts>
               <ts e="T700" id="Seg_4162" n="e" s="T699">gɨnaːččɨlar </ts>
               <ts e="T701" id="Seg_4164" n="e" s="T700">bu͡olla. </ts>
            </ts>
            <ts e="T709" id="Seg_4165" n="sc" s="T702">
               <ts e="T703" id="Seg_4167" n="e" s="T702">Ol </ts>
               <ts e="T704" id="Seg_4169" n="e" s="T703">kördük </ts>
               <ts e="T705" id="Seg_4171" n="e" s="T704">da </ts>
               <ts e="T706" id="Seg_4173" n="e" s="T705">ɨhɨ͡ara </ts>
               <ts e="T707" id="Seg_4175" n="e" s="T706">hɨtɨ͡ak </ts>
               <ts e="T708" id="Seg_4177" n="e" s="T707">ebitter </ts>
               <ts e="T709" id="Seg_4179" n="e" s="T708">otto. </ts>
            </ts>
            <ts e="T734" id="Seg_4180" n="sc" s="T710">
               <ts e="T711" id="Seg_4182" n="e" s="T710">A </ts>
               <ts e="T712" id="Seg_4184" n="e" s="T711">gini, </ts>
               <ts e="T713" id="Seg_4186" n="e" s="T712">ginileriŋ </ts>
               <ts e="T714" id="Seg_4188" n="e" s="T713">innʼekeːt </ts>
               <ts e="T715" id="Seg_4190" n="e" s="T714">diːller </ts>
               <ts e="T716" id="Seg_4192" n="e" s="T715">bu͡o, </ts>
               <ts e="T717" id="Seg_4194" n="e" s="T716">bihigini </ts>
               <ts e="T718" id="Seg_4196" n="e" s="T717">turu͡orallar </ts>
               <ts e="T719" id="Seg_4198" n="e" s="T718">bu͡o, </ts>
               <ts e="T720" id="Seg_4200" n="e" s="T719">üs </ts>
               <ts e="T721" id="Seg_4202" n="e" s="T720">tü͡ört </ts>
               <ts e="T722" id="Seg_4204" n="e" s="T721">kɨːs </ts>
               <ts e="T723" id="Seg_4206" n="e" s="T722">kɨːh </ts>
               <ts e="T724" id="Seg_4208" n="e" s="T723">ogolor. </ts>
               <ts e="T868" id="Seg_4210" n="e" s="T724">Kim, </ts>
               <ts e="T869" id="Seg_4212" n="e" s="T868">kim </ts>
               <ts e="T870" id="Seg_4214" n="e" s="T869">u͡ota, </ts>
               <ts e="T871" id="Seg_4216" n="e" s="T870">kim, </ts>
               <ts e="T872" id="Seg_4218" n="e" s="T871">u͡ota </ts>
               <ts e="T873" id="Seg_4220" n="e" s="T872">iti </ts>
               <ts e="T874" id="Seg_4222" n="e" s="T873">ačaːg </ts>
               <ts e="T725" id="Seg_4224" n="e" s="T874">oŋorollor. </ts>
               <ts e="T726" id="Seg_4226" n="e" s="T725">Mas </ts>
               <ts e="T727" id="Seg_4228" n="e" s="T726">ubajar </ts>
               <ts e="T728" id="Seg_4230" n="e" s="T727">ontuŋ </ts>
               <ts e="T729" id="Seg_4232" n="e" s="T728">ürdütünen </ts>
               <ts e="T730" id="Seg_4234" n="e" s="T729">ottoru </ts>
               <ts e="T731" id="Seg_4236" n="e" s="T730">bɨragallar, </ts>
               <ts e="T732" id="Seg_4238" n="e" s="T731">ulakan </ts>
               <ts e="T733" id="Seg_4240" n="e" s="T732">buru͡o </ts>
               <ts e="T734" id="Seg_4242" n="e" s="T733">keli͡en. </ts>
            </ts>
            <ts e="T745" id="Seg_4243" n="sc" s="T735">
               <ts e="T736" id="Seg_4245" n="e" s="T735">De </ts>
               <ts e="T737" id="Seg_4247" n="e" s="T736">ol </ts>
               <ts e="T738" id="Seg_4249" n="e" s="T737">buru͡o </ts>
               <ts e="T739" id="Seg_4251" n="e" s="T738">ihiger </ts>
               <ts e="T740" id="Seg_4253" n="e" s="T739">tögürüččü </ts>
               <ts e="T741" id="Seg_4255" n="e" s="T740">turammɨt, </ts>
               <ts e="T742" id="Seg_4257" n="e" s="T741">ol </ts>
               <ts e="T743" id="Seg_4259" n="e" s="T742">tutabɨt </ts>
               <ts e="T744" id="Seg_4261" n="e" s="T743">bu͡olla </ts>
               <ts e="T745" id="Seg_4263" n="e" s="T744">kimi. </ts>
            </ts>
            <ts e="T754" id="Seg_4264" n="sc" s="T746">
               <ts e="T747" id="Seg_4266" n="e" s="T746">Ol </ts>
               <ts e="T748" id="Seg_4268" n="e" s="T747">gɨnan </ts>
               <ts e="T749" id="Seg_4270" n="e" s="T748">baran </ts>
               <ts e="T750" id="Seg_4272" n="e" s="T749">ergitebit, </ts>
               <ts e="T751" id="Seg_4274" n="e" s="T750">kün </ts>
               <ts e="T752" id="Seg_4276" n="e" s="T751">hu͡olun </ts>
               <ts e="T753" id="Seg_4278" n="e" s="T752">kördük </ts>
               <ts e="T754" id="Seg_4280" n="e" s="T753">ergitebit. </ts>
            </ts>
            <ts e="T760" id="Seg_4281" n="sc" s="T755">
               <ts e="T756" id="Seg_4283" n="e" s="T755">Üčügejdik </ts>
               <ts e="T757" id="Seg_4285" n="e" s="T756">tutu͡oktaːkkɨn </ts>
               <ts e="T758" id="Seg_4287" n="e" s="T757">ontugun, </ts>
               <ts e="T759" id="Seg_4289" n="e" s="T758">ɨːppat </ts>
               <ts e="T760" id="Seg_4291" n="e" s="T759">kördük. </ts>
            </ts>
            <ts e="T766" id="Seg_4292" n="sc" s="T761">
               <ts e="T762" id="Seg_4294" n="e" s="T761">(Beje-) </ts>
               <ts e="T763" id="Seg_4296" n="e" s="T762">bejebitiger </ts>
               <ts e="T764" id="Seg_4298" n="e" s="T763">ergitebit </ts>
               <ts e="T765" id="Seg_4300" n="e" s="T764">bu͡ol </ts>
               <ts e="T766" id="Seg_4302" n="e" s="T765">innʼekeːt. </ts>
            </ts>
            <ts e="T779" id="Seg_4303" n="sc" s="T767">
               <ts e="T768" id="Seg_4305" n="e" s="T767">Min </ts>
               <ts e="T769" id="Seg_4307" n="e" s="T768">dogorbor </ts>
               <ts e="T770" id="Seg_4309" n="e" s="T769">bi͡eri͡em, </ts>
               <ts e="T771" id="Seg_4311" n="e" s="T770">dogorum </ts>
               <ts e="T772" id="Seg_4313" n="e" s="T771">nöŋü͡ö </ts>
               <ts e="T773" id="Seg_4315" n="e" s="T772">dogorgor, </ts>
               <ts e="T774" id="Seg_4317" n="e" s="T773">itirdikkeːn </ts>
               <ts e="T775" id="Seg_4319" n="e" s="T774">bi͡erer, </ts>
               <ts e="T776" id="Seg_4321" n="e" s="T775">eto </ts>
               <ts e="T777" id="Seg_4323" n="e" s="T776">tögürütebit </ts>
               <ts e="T778" id="Seg_4325" n="e" s="T777">ol </ts>
               <ts e="T779" id="Seg_4327" n="e" s="T778">tiriːni. </ts>
            </ts>
            <ts e="T785" id="Seg_4328" n="sc" s="T780">
               <ts e="T781" id="Seg_4330" n="e" s="T780">Annɨgɨtɨttan </ts>
               <ts e="T782" id="Seg_4332" n="e" s="T781">ulakan </ts>
               <ts e="T783" id="Seg_4334" n="e" s="T782">buru͡o </ts>
               <ts e="T784" id="Seg_4336" n="e" s="T783">keler </ts>
               <ts e="T785" id="Seg_4338" n="e" s="T784">bu͡o. </ts>
            </ts>
            <ts e="T792" id="Seg_4339" n="sc" s="T786">
               <ts e="T787" id="Seg_4341" n="e" s="T786">Bihi͡eke </ts>
               <ts e="T788" id="Seg_4343" n="e" s="T787">emi͡e </ts>
               <ts e="T789" id="Seg_4345" n="e" s="T788">kuhagan </ts>
               <ts e="T790" id="Seg_4347" n="e" s="T789">ol </ts>
               <ts e="T791" id="Seg_4349" n="e" s="T790">buru͡o </ts>
               <ts e="T792" id="Seg_4351" n="e" s="T791">kelere. </ts>
            </ts>
            <ts e="T798" id="Seg_4352" n="sc" s="T793">
               <ts e="T794" id="Seg_4354" n="e" s="T793">Karakpɨt </ts>
               <ts e="T795" id="Seg_4356" n="e" s="T794">ahɨjar </ts>
               <ts e="T796" id="Seg_4358" n="e" s="T795">bu͡olla </ts>
               <ts e="T797" id="Seg_4360" n="e" s="T796">ol </ts>
               <ts e="T798" id="Seg_4362" n="e" s="T797">buru͡ottan. </ts>
            </ts>
            <ts e="T804" id="Seg_4363" n="sc" s="T799">
               <ts e="T800" id="Seg_4365" n="e" s="T799">Anʼakpɨtɨgar </ts>
               <ts e="T801" id="Seg_4367" n="e" s="T800">da </ts>
               <ts e="T802" id="Seg_4369" n="e" s="T801">ɨlabɨt </ts>
               <ts e="T803" id="Seg_4371" n="e" s="T802">bu͡olla </ts>
               <ts e="T804" id="Seg_4373" n="e" s="T803">buru͡onu. </ts>
            </ts>
            <ts e="T817" id="Seg_4374" n="sc" s="T805">
               <ts e="T806" id="Seg_4376" n="e" s="T805">De </ts>
               <ts e="T807" id="Seg_4378" n="e" s="T806">ol </ts>
               <ts e="T808" id="Seg_4380" n="e" s="T807">da </ts>
               <ts e="T809" id="Seg_4382" n="e" s="T808">bu͡ollar </ts>
               <ts e="T810" id="Seg_4384" n="e" s="T809">de </ts>
               <ts e="T811" id="Seg_4386" n="e" s="T810">turunaːktɨːbɨt </ts>
               <ts e="T812" id="Seg_4388" n="e" s="T811">bu͡o, </ts>
               <ts e="T813" id="Seg_4390" n="e" s="T812">turuŋ </ts>
               <ts e="T814" id="Seg_4392" n="e" s="T813">turuŋ </ts>
               <ts e="T815" id="Seg_4394" n="e" s="T814">diːller </ts>
               <ts e="T816" id="Seg_4396" n="e" s="T815">bu͡o </ts>
               <ts e="T817" id="Seg_4398" n="e" s="T816">bihigini. </ts>
            </ts>
            <ts e="T822" id="Seg_4399" n="sc" s="T818">
               <ts e="T819" id="Seg_4401" n="e" s="T818">De </ts>
               <ts e="T820" id="Seg_4403" n="e" s="T819">ol </ts>
               <ts e="T821" id="Seg_4405" n="e" s="T820">kördük </ts>
               <ts e="T822" id="Seg_4407" n="e" s="T821">ergitebit. </ts>
            </ts>
            <ts e="T849" id="Seg_4408" n="sc" s="T823">
               <ts e="T824" id="Seg_4410" n="e" s="T823">Onton </ts>
               <ts e="T825" id="Seg_4412" n="e" s="T824">biːr </ts>
               <ts e="T826" id="Seg_4414" n="e" s="T825">emete </ts>
               <ts e="T827" id="Seg_4416" n="e" s="T826">kɨːh </ts>
               <ts e="T828" id="Seg_4418" n="e" s="T827">ogoto </ts>
               <ts e="T829" id="Seg_4420" n="e" s="T828">tiriːni </ts>
               <ts e="T830" id="Seg_4422" n="e" s="T829">iliːtinen </ts>
               <ts e="T831" id="Seg_4424" n="e" s="T830">innʼekeːtiŋ </ts>
               <ts e="T832" id="Seg_4426" n="e" s="T831">ɨlan </ts>
               <ts e="T833" id="Seg_4428" n="e" s="T832">keːhi͡e, </ts>
               <ts e="T834" id="Seg_4430" n="e" s="T833">hɨːha </ts>
               <ts e="T835" id="Seg_4432" n="e" s="T834">tutu͡o, </ts>
               <ts e="T836" id="Seg_4434" n="e" s="T835">ontutun </ts>
               <ts e="T837" id="Seg_4436" n="e" s="T836">ol </ts>
               <ts e="T838" id="Seg_4438" n="e" s="T837">tutar </ts>
               <ts e="T839" id="Seg_4440" n="e" s="T838">hirin, </ts>
               <ts e="T840" id="Seg_4442" n="e" s="T839">tiriːtin </ts>
               <ts e="T841" id="Seg_4444" n="e" s="T840">de, </ts>
               <ts e="T842" id="Seg_4446" n="e" s="T841">oččogo </ts>
               <ts e="T843" id="Seg_4448" n="e" s="T842">diːller </ts>
               <ts e="T844" id="Seg_4450" n="e" s="T843">bu͡o, </ts>
               <ts e="T845" id="Seg_4452" n="e" s="T844">de </ts>
               <ts e="T846" id="Seg_4454" n="e" s="T845">erge </ts>
               <ts e="T847" id="Seg_4456" n="e" s="T846">barɨ͡aŋ </ts>
               <ts e="T848" id="Seg_4458" n="e" s="T847">hu͡oga, </ts>
               <ts e="T849" id="Seg_4460" n="e" s="T848">ulaːttakkɨna. </ts>
            </ts>
            <ts e="T856" id="Seg_4461" n="sc" s="T850">
               <ts e="T851" id="Seg_4463" n="e" s="T850">Diː </ts>
               <ts e="T852" id="Seg_4465" n="e" s="T851">diːller </ts>
               <ts e="T853" id="Seg_4467" n="e" s="T852">itigirdikkeːn </ts>
               <ts e="T854" id="Seg_4469" n="e" s="T853">haŋarallar </ts>
               <ts e="T855" id="Seg_4471" n="e" s="T854">bu͡o </ts>
               <ts e="T856" id="Seg_4473" n="e" s="T855">bihigini. </ts>
            </ts>
            <ts e="T867" id="Seg_4474" n="sc" s="T857">
               <ts e="T858" id="Seg_4476" n="e" s="T857">Ol </ts>
               <ts e="T859" id="Seg_4478" n="e" s="T858">ihin </ts>
               <ts e="T860" id="Seg_4480" n="e" s="T859">ol </ts>
               <ts e="T861" id="Seg_4482" n="e" s="T860">muŋnana </ts>
               <ts e="T862" id="Seg_4484" n="e" s="T861">turabɨt </ts>
               <ts e="T863" id="Seg_4486" n="e" s="T862">bu͡o </ts>
               <ts e="T864" id="Seg_4488" n="e" s="T863">ol </ts>
               <ts e="T865" id="Seg_4490" n="e" s="T864">tiriːni </ts>
               <ts e="T866" id="Seg_4492" n="e" s="T865">ɨːppakka </ts>
               <ts e="T867" id="Seg_4494" n="e" s="T866">ɨntak. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T15" id="Seg_4495" s="T1">BeES_2010_HidePreparation_nar.001 (001)</ta>
            <ta e="T24" id="Seg_4496" s="T16">BeES_2010_HidePreparation_nar.002 (002)</ta>
            <ta e="T35" id="Seg_4497" s="T25">BeES_2010_HidePreparation_nar.003 (003)</ta>
            <ta e="T43" id="Seg_4498" s="T36">BeES_2010_HidePreparation_nar.004 (004)</ta>
            <ta e="T50" id="Seg_4499" s="T44">BeES_2010_HidePreparation_nar.005 (005)</ta>
            <ta e="T57" id="Seg_4500" s="T51">BeES_2010_HidePreparation_nar.006 (006)</ta>
            <ta e="T67" id="Seg_4501" s="T58">BeES_2010_HidePreparation_nar.007 (007)</ta>
            <ta e="T76" id="Seg_4502" s="T68">BeES_2010_HidePreparation_nar.008 (008)</ta>
            <ta e="T93" id="Seg_4503" s="T77">BeES_2010_HidePreparation_nar.009 (009)</ta>
            <ta e="T102" id="Seg_4504" s="T94">BeES_2010_HidePreparation_nar.010 (010)</ta>
            <ta e="T107" id="Seg_4505" s="T103">BeES_2010_HidePreparation_nar.011 (011)</ta>
            <ta e="T116" id="Seg_4506" s="T108">BeES_2010_HidePreparation_nar.012 (012)</ta>
            <ta e="T124" id="Seg_4507" s="T117">BeES_2010_HidePreparation_nar.013 (013)</ta>
            <ta e="T141" id="Seg_4508" s="T125">BeES_2010_HidePreparation_nar.014 (014)</ta>
            <ta e="T163" id="Seg_4509" s="T142">BeES_2010_HidePreparation_nar.015 (015)</ta>
            <ta e="T169" id="Seg_4510" s="T164">BeES_2010_HidePreparation_nar.016 (016)</ta>
            <ta e="T173" id="Seg_4511" s="T170">BeES_2010_HidePreparation_nar.017 (017)</ta>
            <ta e="T181" id="Seg_4512" s="T174">BeES_2010_HidePreparation_nar.018 (018)</ta>
            <ta e="T197" id="Seg_4513" s="T182">BeES_2010_HidePreparation_nar.019 (019)</ta>
            <ta e="T207" id="Seg_4514" s="T198">BeES_2010_HidePreparation_nar.020 (020)</ta>
            <ta e="T217" id="Seg_4515" s="T208">BeES_2010_HidePreparation_nar.021 (021)</ta>
            <ta e="T224" id="Seg_4516" s="T218">BeES_2010_HidePreparation_nar.022 (022)</ta>
            <ta e="T240" id="Seg_4517" s="T225">BeES_2010_HidePreparation_nar.023 (023)</ta>
            <ta e="T257" id="Seg_4518" s="T241">BeES_2010_HidePreparation_nar.024 (024)</ta>
            <ta e="T270" id="Seg_4519" s="T258">BeES_2010_HidePreparation_nar.025 (025)</ta>
            <ta e="T279" id="Seg_4520" s="T271">BeES_2010_HidePreparation_nar.026 (026)</ta>
            <ta e="T292" id="Seg_4521" s="T280">BeES_2010_HidePreparation_nar.027 (027)</ta>
            <ta e="T304" id="Seg_4522" s="T293">BeES_2010_HidePreparation_nar.028 (028)</ta>
            <ta e="T311" id="Seg_4523" s="T305">BeES_2010_HidePreparation_nar.029 (029)</ta>
            <ta e="T319" id="Seg_4524" s="T312">BeES_2010_HidePreparation_nar.030 (030)</ta>
            <ta e="T329" id="Seg_4525" s="T320">BeES_2010_HidePreparation_nar.031 (031)</ta>
            <ta e="T343" id="Seg_4526" s="T330">BeES_2010_HidePreparation_nar.032 (032)</ta>
            <ta e="T353" id="Seg_4527" s="T344">BeES_2010_HidePreparation_nar.033 (033)</ta>
            <ta e="T365" id="Seg_4528" s="T354">BeES_2010_HidePreparation_nar.034 (034)</ta>
            <ta e="T370" id="Seg_4529" s="T366">BeES_2010_HidePreparation_nar.035 (035)</ta>
            <ta e="T385" id="Seg_4530" s="T371">BeES_2010_HidePreparation_nar.036 (036)</ta>
            <ta e="T390" id="Seg_4531" s="T386">BeES_2010_HidePreparation_nar.037 (037)</ta>
            <ta e="T402" id="Seg_4532" s="T391">BeES_2010_HidePreparation_nar.038 (038)</ta>
            <ta e="T411" id="Seg_4533" s="T402">BeES_2010_HidePreparation_nar.039 (039)</ta>
            <ta e="T417" id="Seg_4534" s="T411">BeES_2010_HidePreparation_nar.040 (040)</ta>
            <ta e="T427" id="Seg_4535" s="T418">BeES_2010_HidePreparation_nar.041 (041)</ta>
            <ta e="T432" id="Seg_4536" s="T428">BeES_2010_HidePreparation_nar.042 (042)</ta>
            <ta e="T442" id="Seg_4537" s="T433">BeES_2010_HidePreparation_nar.043 (043)</ta>
            <ta e="T461" id="Seg_4538" s="T443">BeES_2010_HidePreparation_nar.044 (044)</ta>
            <ta e="T469" id="Seg_4539" s="T462">BeES_2010_HidePreparation_nar.045 (045)</ta>
            <ta e="T490" id="Seg_4540" s="T470">BeES_2010_HidePreparation_nar.046 (046)</ta>
            <ta e="T503" id="Seg_4541" s="T491">BeES_2010_HidePreparation_nar.047 (047)</ta>
            <ta e="T514" id="Seg_4542" s="T504">BeES_2010_HidePreparation_nar.048 (048)</ta>
            <ta e="T522" id="Seg_4543" s="T515">BeES_2010_HidePreparation_nar.049 (049)</ta>
            <ta e="T532" id="Seg_4544" s="T523">BeES_2010_HidePreparation_nar.050 (050)</ta>
            <ta e="T543" id="Seg_4545" s="T533">BeES_2010_HidePreparation_nar.051 (051)</ta>
            <ta e="T553" id="Seg_4546" s="T544">BeES_2010_HidePreparation_nar.052 (052)</ta>
            <ta e="T570" id="Seg_4547" s="T554">BeES_2010_HidePreparation_nar.053 (053)</ta>
            <ta e="T577" id="Seg_4548" s="T571">BeES_2010_HidePreparation_nar.054 (054)</ta>
            <ta e="T586" id="Seg_4549" s="T578">BeES_2010_HidePreparation_nar.055 (055)</ta>
            <ta e="T592" id="Seg_4550" s="T587">BeES_2010_HidePreparation_nar.056 (056)</ta>
            <ta e="T604" id="Seg_4551" s="T593">BeES_2010_HidePreparation_nar.057 (057)</ta>
            <ta e="T616" id="Seg_4552" s="T605">BeES_2010_HidePreparation_nar.058 (058)</ta>
            <ta e="T625" id="Seg_4553" s="T617">BeES_2010_HidePreparation_nar.059 (059)</ta>
            <ta e="T645" id="Seg_4554" s="T626">BeES_2010_HidePreparation_nar.060 (060)</ta>
            <ta e="T649" id="Seg_4555" s="T646">BeES_2010_HidePreparation_nar.061 (061)</ta>
            <ta e="T658" id="Seg_4556" s="T650">BeES_2010_HidePreparation_nar.062 (062)</ta>
            <ta e="T667" id="Seg_4557" s="T659">BeES_2010_HidePreparation_nar.063 (063)</ta>
            <ta e="T675" id="Seg_4558" s="T668">BeES_2010_HidePreparation_nar.064 (064)</ta>
            <ta e="T687" id="Seg_4559" s="T676">BeES_2010_HidePreparation_nar.065 (065)</ta>
            <ta e="T694" id="Seg_4560" s="T688">BeES_2010_HidePreparation_nar.066 (066)</ta>
            <ta e="T701" id="Seg_4561" s="T695">BeES_2010_HidePreparation_nar.067 (067)</ta>
            <ta e="T709" id="Seg_4562" s="T702">BeES_2010_HidePreparation_nar.068 (068)</ta>
            <ta e="T724" id="Seg_4563" s="T710">BeES_2010_HidePreparation_nar.069 (069)</ta>
            <ta e="T725" id="Seg_4564" s="T724">BeES_2010_HidePreparation_nar.070 (070)</ta>
            <ta e="T734" id="Seg_4565" s="T725">BeES_2010_HidePreparation_nar.071 (071)</ta>
            <ta e="T745" id="Seg_4566" s="T735">BeES_2010_HidePreparation_nar.072 (072)</ta>
            <ta e="T754" id="Seg_4567" s="T746">BeES_2010_HidePreparation_nar.073 (073)</ta>
            <ta e="T760" id="Seg_4568" s="T755">BeES_2010_HidePreparation_nar.074 (074)</ta>
            <ta e="T766" id="Seg_4569" s="T761">BeES_2010_HidePreparation_nar.075 (075)</ta>
            <ta e="T779" id="Seg_4570" s="T767">BeES_2010_HidePreparation_nar.076 (076)</ta>
            <ta e="T785" id="Seg_4571" s="T780">BeES_2010_HidePreparation_nar.077 (077)</ta>
            <ta e="T792" id="Seg_4572" s="T786">BeES_2010_HidePreparation_nar.078 (078)</ta>
            <ta e="T798" id="Seg_4573" s="T793">BeES_2010_HidePreparation_nar.079 (079)</ta>
            <ta e="T804" id="Seg_4574" s="T799">BeES_2010_HidePreparation_nar.080 (080)</ta>
            <ta e="T817" id="Seg_4575" s="T805">BeES_2010_HidePreparation_nar.081 (081)</ta>
            <ta e="T822" id="Seg_4576" s="T818">BeES_2010_HidePreparation_nar.082 (082)</ta>
            <ta e="T849" id="Seg_4577" s="T823">BeES_2010_HidePreparation_nar.083 (083)</ta>
            <ta e="T856" id="Seg_4578" s="T850">BeES_2010_HidePreparation_nar.084 (084)</ta>
            <ta e="T867" id="Seg_4579" s="T857">BeES_2010_HidePreparation_nar.085 (085)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T15" id="Seg_4580" s="T1">Kös-A kɨhɨn-(n)I kɨhɨn-(n)I meldi kös-A hɨrɨt-An bar-An-LAr Saka-LAr saːs toktoː-Ar sir-LArA baːr bu͡ol-AːččI</ta>
            <ta e="T24" id="Seg_4581" s="T16">Onno ügüs bagajɨ uraha d’i͡e-LAr hɨrga d’i͡e-LAr tur-AːččI-LAr</ta>
            <ta e="T35" id="Seg_4582" s="T25">Saːs-GI hɨlaːs kün-LAr-(t)A kün tɨk-Ar bu͡ol-TAk-InA de d’aktattar üle-LAː-Ar-LArA ükseː-Ar</ta>
            <ta e="T43" id="Seg_4583" s="T36">Tahaːra giniler-(I)ŋ maŋnaj iti tiriː üle-(t)In üle-LAː-A-LAr</ta>
            <ta e="T50" id="Seg_4584" s="T44">Tiriː tiriː-GIn bert delbi ɨraːs-LAː-Ar-LAr maŋnaj</ta>
            <ta e="T57" id="Seg_4585" s="T51">Ɨraːs-(t)tAn bar-An-LAr kɨptɨj-(I)nAn kɨrɨj-Ar-LAr * tüː-(t)In</ta>
            <ta e="T67" id="Seg_4586" s="T58">Ol kɨptɨj-(I)nAn kɨrɨj-An büt-TAk-LArInA onton kojut bahak-(I)nAn emi͡e kɨrɨj-Ar-LAr</ta>
            <ta e="T76" id="Seg_4587" s="T68">Öldün Saka-LAr olus köp kim-(n)I tiriː-(n)I ɨl-AːččI-TA su͡ok-LAr</ta>
            <ta e="T93" id="Seg_4588" s="T77">Kɨrɨj-IlIn-I-BIt iti tüː-(t)In ɨl-IlIn-I-BIt-(t)tAn * kɨrpalaː-BIt tüː-(n)I kim-(n)I tiriː-(n)I ɨl-An-LAr kim d’i͡e habɨː-TA iti öldün-LAr-(n)I tik-AːččI-LAr</ta>
            <ta e="T102" id="Seg_4589" s="T94">Onnuk bu͡ol-TAk-InA de ol barbak tüː-(n)I keːs-Ar-LAr bu͡o</ta>
            <ta e="T107" id="Seg_4590" s="T103">Onton ol-TI-GInAn öldün tik-Ar-LAr</ta>
            <ta e="T116" id="Seg_4591" s="T108">Ol öl horok tiriː-(n)I giniler-(I)ŋ harɨː oŋor-IAk-LAːk-LAr</ta>
            <ta e="T124" id="Seg_4592" s="T117">Harɨː oŋor-Ar-LArIgAr iti-NA de hin ügüs üle</ta>
            <ta e="T141" id="Seg_4593" s="T125">Kɨrpalaː-Ar-LAr bu͡o iti kɨrpalaː-BIt-LArIN kördük tüː-(t)A su͡ok bu͡o tüː-(t)A agɨjak bu͡ol-IAn hin tüː-(t)A kaːl-Ar bu͡o tiriː-GAr</ta>
            <ta e="T163" id="Seg_4594" s="T142">Ol tüː tüː-(I)ŋ üčügej-LIk barɨ-(t)A onton kojut imit-Ar-GA da üčügej bu͡o harɨː ol tüː-LAːk kim tiriː-(n)I giniler-(I)ŋ hɨt-I-T-Ar-LAr uː-GA</ta>
            <ta e="T169" id="Seg_4595" s="T164">Uː ihiger mɨlo.R-(n)I kut-Ar-LAr uːr-Ar-LAr</ta>
            <ta e="T173" id="Seg_4596" s="T170">Onton eŋin-eŋin ot-LAr-(n)I</ta>
            <ta e="T181" id="Seg_4597" s="T174">Ol gɨn-A gɨn-A kas da kün-(n)I hɨt-IAk-LAːk</ta>
            <ta e="T197" id="Seg_4598" s="T182">Itiː da kün bu͡ol-TIn- ol uː-(I)ŋ hɨtɨj-Ar bu͡o tiriː-(I)ŋ emi͡e hɨtɨj-Ar bu͡o imiges bu͡ol-Ar bu͡o</ta>
            <ta e="T207" id="Seg_4599" s="T198">Tiriː-(I)ŋ kas da kün-(n)I hɨtɨj-An bütte-GInA giniler-(I)ŋ ol bulkuj-Ar-LAr</ta>
            <ta e="T217" id="Seg_4600" s="T208">Ol tiriː-GIn ɨl-An bar-An-LAr iti kim huːn-(I)Ar-Ar-LAr ürek-GA bar-An-LAr</ta>
            <ta e="T224" id="Seg_4601" s="T218">Delbi huːn-TAr-Ar ol gɨn-An bar-An kuːr-T-Ar-LAr</ta>
            <ta e="T240" id="Seg_4602" s="T225">Kuːr-T-An bar-An-LArA iti tiriː-GIn kim-LAː-Ar-LAr- gedereːn-(I)nAn ol kim kaːl-BIt tüː-LArIN ɨl-Ar-LAr *gedereː gedereːn-LAː-Ar-LAr Saka-LIː haŋar-TAk-GA</ta>
            <ta e="T257" id="Seg_4603" s="T241">Ol-TI-(I)ŋ emi͡e türgen da bu͡ol-BAtAk d’ogus da bu͡ol-BAtAk d’aktattar bert kim ɨ͡arakan üle iti d’aktar-GA da</ta>
            <ta e="T270" id="Seg_4604" s="T258">Ol ihin biːr-TA olor-Aːt ol-(n)I barɨ-(t)In *hataːb-A olor-IAk-LAː üle-LAː-IAk-LArA su͡ok-(t)A ɨ͡arakan üle</ta>
            <ta e="T279" id="Seg_4605" s="T271">Onton nöŋü͡ö kün emi͡e oŋor-IAk-LArA ol gedereːn-LAː-IAk-LArA emi͡e</ta>
            <ta e="T292" id="Seg_4606" s="T280">Gedereːn-(I)nAn büt-TAk-LArInA ol harɨː-(I)ŋ kim kim-(t)A su͡ok bu͡ol-Ar bu͡o- tüː-(t)A su͡ok bu͡ol-Ar</ta>
            <ta e="T304" id="Seg_4607" s="T293">Ol-TI-(I)ŋ deksi iti kɨhɨ͡ak-(I)nAn tu͡ok-(I)nAn eŋin-eŋin-(I)nAn tarbaː-Ar-LAr kɨhɨ͡ak-LAː-Ar-LAr kann’aː-Ar-LAr</ta>
            <ta e="T311" id="Seg_4608" s="T305">Bahak-(I)nAn da eŋin-eŋin deksi-LAː-Ar-LAr kann’aː-Ar-LAr</ta>
            <ta e="T319" id="Seg_4609" s="T312">Tu͡ok kuhagan baːr-(t)In barɨ-(t)In iti ɨl-AttAː-An is-Ar-LAr</ta>
            <ta e="T329" id="Seg_4610" s="T320">Ol harɨː-(I)ŋ ol gɨn-An bar-An kör-TAk-GA üčügej bagajɨ bu͡ol-Ar</ta>
            <ta e="T343" id="Seg_4611" s="T330">Onton harɨː-(I)ŋ ile harɨː ol gɨn-An bu͡o ɨːs-LAːk ol-TI-GIn emi͡e imit-IAk-LAːk-LAr</ta>
            <ta e="T353" id="Seg_4612" s="T344">Imit-AːrI gɨn-TAk-LArInA de kim-LAː-Ar-LAr- kɨːs-ogo-LArIN ɨgɨr-AːččI-LAr togo ere</ta>
            <ta e="T365" id="Seg_4613" s="T354">Iti kimi͡eke taŋas ɨjaːn-Ar kim-LAr-GA ɨjaː-Ar-LAr bu͡o ol tiriː-GIn taŋas ɨjaːn-Ar</ta>
            <ta e="T370" id="Seg_4614" s="T366">Ol-TI-GIn battaː-An tur-IAk-(t)In naːda</ta>
            <ta e="T385" id="Seg_4615" s="T371">Hɨrga-(n)I ti͡ere uːr-An bar-An-LAr emi͡e battaː-An tur-IAk-(t)In naːda ol tiriː-(n)I uːr-An bar-An-GIn harɨː bu͡ol-IAk-GIn</ta>
            <ta e="T390" id="Seg_4616" s="T386">De ol battaː-An tur-A-GIn</ta>
            <ta e="T402" id="Seg_4617" s="T391">Ol battaː-An tur-TAk-GInA eni͡eke bi͡er-Ar-LAr iti kim-(n)I bɨ͡ar-(n)I bus-BIt taba bɨ͡ar-(t)In</ta>
            <ta e="T411" id="Seg_4618" s="T402">Ol bɨ͡ar-(n)I battaː-A tur-An kɨːs kɨːs ogo ɨstaː-IAn ɨstaː-IAk-LAːk</ta>
            <ta e="T417" id="Seg_4619" s="T411">Ɨstaː di͡e-Ar-LAr bu͡o kajdi͡ek bu͡ol-IAŋ-(n)Ij ke</ta>
            <ta e="T427" id="Seg_4620" s="T418">Bagar-BAt bu͡ol-A bu͡ol-A ol-TI-(I)ŋ amtan-(t)A kuhagan-(t)A- hin tur-A-GIn bu͡o</ta>
            <ta e="T432" id="Seg_4621" s="T428">Ihit-A-GIn bu͡o in’e-(I)ŋ haŋa-(t)In</ta>
            <ta e="T442" id="Seg_4622" s="T433">Ol tur-A tur-A ɨstaː-A tur-A-GIn ol kim-(n)I bɨ͡ar-(n)I tur-An-GIn</ta>
            <ta e="T461" id="Seg_4623" s="T443">Giniler-(I)ŋ ol bɨ͡a ol ɨstaː-BIt bɨ͡ar-GIn ɨl-A- ɨl-Ar-LAr ol tiriː-GIn ol kim-LAː-BIt gedereːn-LAː-BIt tiriː-GIn ɨl-An bar-An-LAr ol bis-Ar-LAr</ta>
            <ta e="T469" id="Seg_4624" s="T462">Ol gɨn-An bar-An hoŋohoːn hoŋohoːn-(I)nAn hoŋohoːn-LAː-Ar-LAr iliː-LArInAn</ta>
            <ta e="T490" id="Seg_4625" s="T470">Biːr iliː-(I)nAn tutun-An ol tiriː-GIn biːr iliː-(I)nAn hoŋohoːn-GIn tut-Ar bu͡o ol bu͡o üːhe-(t)tAn allara-GA di͡eri inn’e hoŋohoːn-LAː-Ar eee bis-A bis-A</ta>
            <ta e="T503" id="Seg_4626" s="T491">De ol gɨn-An bar-An giniler-(I)ŋ kɨːs-ogo-LAr kahan da tur-IAk-LArIN bagar-AːččI-(t)A su͡ok-LAr</ta>
            <ta e="T514" id="Seg_4627" s="T504">Battaː-An tur-IAk-(t)In naːda bu͡o ör bagajɨ tur-IAk-(t)In *-LAː tur-IAk-LAːk-GIn onno</ta>
            <ta e="T522" id="Seg_4628" s="T515">A.R ɨstaː-Ar-(I)ŋ baːr de kuhagan da kuhagan</ta>
            <ta e="T532" id="Seg_4629" s="T523">Eee ɨstaː-IAŋ- kihi da hürek-(t)A *-TAk-(I)s-(n)Ij-IAk horok bu͡ol-Ar bu͡o</ta>
            <ta e="T543" id="Seg_4630" s="T533">Ol ihin kɨːs-ogo-kAːn-LAr-(I)ŋ küreː-(I)n-Ar-LAr bu͡o ɨstaː-ImAːrI-LAr bar-An kaːl-Ar-LAr</ta>
            <ta e="T553" id="Seg_4631" s="T544">Kim kɨːs-ogo-LAr-(n)I ügüs-LIk ɨgɨr-AːččI-LAr u͡ol togo ere</ta>
            <ta e="T570" id="Seg_4632" s="T554">De ol ol gɨn-An bar-An ol kim-InA delbi kim-TA hoŋohoːn-(I)nAn delbi kim-LAː-A kim-LAː-TIlAr bu͡o ol tiriː-GIn</ta>
            <ta e="T577" id="Seg_4633" s="T571">Ol hoŋohoːn-LAː-Ar-(I)ŋ emi͡e ör bagajɨ bu͡ol-TA</ta>
            <ta e="T586" id="Seg_4634" s="T578">Nöŋü͡ö kün emi͡e tur-IAk-LAːk-GIn bu͡ol-TA ol ɨstaː-A ɨstaː-A-GIn</ta>
            <ta e="T592" id="Seg_4635" s="T587">Bagar-BAt bu͡ol-A-GIn hürek-(I)ŋ loksuj-Ar bu͡ol-TA</ta>
            <ta e="T604" id="Seg_4636" s="T593">De emi͡e hin ɨstaː-A-GIn bu͡ol-TA- kajdi͡ek bu͡ol-IAŋ-(n)Ij in’e-(I)ŋ haŋa-(t)In ihit-A-GIn bu͡o</ta>
            <ta e="T616" id="Seg_4637" s="T605">De ol tur-An ɨstaː-A ɨstaː-A-GIn de muŋ-LAː-(I)n-An muŋ-LAː-(I)n-An de büt-A-BIt bu͡ol-TA</ta>
            <ta e="T625" id="Seg_4638" s="T617">Kas-(I)s eme kün-(t)IgAr de ol kɨhɨ͡ak kim-(I)nAn hoŋohoːn-(I)nAn</ta>
            <ta e="T645" id="Seg_4639" s="T626">Onton kojut iti harɨː bu͡ol bukatɨn harɨː bu͡ol üčügej imiges iti eŋin-eŋin-LIk iti kim-LAː-A hataː-TI-BIt bu͡o</ta>
            <ta e="T649" id="Seg_4640" s="T646">Kɨhɨ͡ak-(t)tAn gedereːn-(I)nAn oŋor-A-BIt</ta>
            <ta e="T658" id="Seg_4641" s="T650">Ol-TI-(I)ŋ harɨː olus maŋan tože.R hin kuhagan</ta>
            <ta e="T667" id="Seg_4642" s="T659">Ol ihin harɨː-GIn iti kimi͡eke ɨhaːr-Ar-LAr ɨhaːr-Ar-LAr buru͡o-GA</ta>
            <ta e="T675" id="Seg_4643" s="T668">Ol harɨː-GIn ɨl-An bar-An-LAr d’i͡e ihiger ɨjaː-Ar-LAr</ta>
            <ta e="T687" id="Seg_4644" s="T676">D’i͡e da bu͡ol-IA-(n)Ij iti kim uraha ihiger uraha d’i͡e ihiger ɨjaː-Ar-LAr</ta>
            <ta e="T694" id="Seg_4645" s="T688">Onno buru͡o-GA ol-TI-(I)ŋ ɨksa tur-Ar bu͡o</ta>
            <ta e="T701" id="Seg_4646" s="T695">A.R horok d’aktattar inn’e gɨn-AːččI-LAr bu͡ollaga</ta>
            <ta e="T709" id="Seg_4647" s="T702">Ol kördük da hɨt-IAk e-BIt-LAr otto</ta>
            <ta e="T724" id="Seg_4648" s="T710">A.R gini giniler-(I)ŋ *inn’ek-Aːt di͡e-Ar-LAr bu͡o- bihigi-(n)I tur-(I)Ar-Ar-LAr bu͡o üs tü͡ört kɨːs kɨːs-ogo-LAr</ta>
            <ta e="T734" id="Seg_4649" s="T725">Mas ubaj-Ar ol-TI-(I)ŋ ürdü-(t)InAn ot-LAr-(n)I bɨrak-Ar-LAr ulakan buru͡o kel-IAk-(t)In</ta>
            <ta e="T745" id="Seg_4650" s="T735">De ol buru͡o ihiger tögürüččü tur-An-BIt ol tut-A-BIt bu͡ollaga kim-(n)I</ta>
            <ta e="T754" id="Seg_4651" s="T746">Ol gɨn-An bar-An ergij-T-A-BIt kün su͡ol-(t)In kördük ergij-T-A-BIt</ta>
            <ta e="T760" id="Seg_4652" s="T755">Üčügej-LIk tut-IAk-LAːk-GIn ol-TI-GIn ɨːt-BAt kördük</ta>
            <ta e="T766" id="Seg_4653" s="T761">Beje beje-BItIgAr ergij-T-A-BIt bu͡ol *inn’ek-Aːt</ta>
            <ta e="T779" id="Seg_4654" s="T767">Min dogor-BAr bi͡er-IAm dogor-(I)m nöŋü͡ö dogor-GAr iti kördük-kAːn bi͡er-Ar eto.R tögürüj-T-A-BIt ol tiriː-(n)I</ta>
            <ta e="T785" id="Seg_4655" s="T780">Annɨ-GI-(t)IttAn ulakan buru͡o kel-Ar bu͡o</ta>
            <ta e="T792" id="Seg_4656" s="T786">Bihi͡eke emi͡e kuhagan ol buru͡o kel-Ar-(t)A</ta>
            <ta e="T798" id="Seg_4657" s="T793">Karak-BIt ahɨj-Ar bu͡ollaga ol buru͡o-(t)tAn</ta>
            <ta e="T804" id="Seg_4658" s="T799">Ajak-BItIgAr da ɨl-A-BIt bu͡ollaga buru͡o-(n)I</ta>
            <ta e="T817" id="Seg_4659" s="T805">De ol da bu͡ol-TAr de tur-(I)n-AːktAː-BIt bu͡o tur-(I)ŋ tur-(I)ŋ di͡e-Ar-LAr bu͡o bihigi-(n)I</ta>
            <ta e="T822" id="Seg_4660" s="T818">De ol kördük ergij-T-A-BIt</ta>
            <ta e="T849" id="Seg_4661" s="T823">Onton biːr eme-(t)A kɨːs-ogo-(t)A tiriː-(n)I iliː-(t)InAn *inn’ek-Aːt ɨl-An keːs-IA hɨːha tut-IA ol-TI-(t)In ol tut-Ar sir-(t)In- tiriː-(t)In de Oččogo di͡e-Ar-LAr bu͡o De er-GA bar-IAŋ su͡ok-(t)A ulaːt-TAk-GInA</ta>
            <ta e="T856" id="Seg_4662" s="T850">Di͡e-A di͡e-Ar-LAr iti kördük-kAːn haŋar-Ar-LAr bu͡o bihigi-(n)I</ta>
            <ta e="T867" id="Seg_4663" s="T857">Ol ihin ol muŋ-LAː-(I)n-A tur-A-BIt bu͡o ol tiriː-(n)I ɨːt-BAkkA ɨntak</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T15" id="Seg_4664" s="T1">Köhö (hɨl-) kɨhɨnɨ, kɨhɨnɨ meldʼi köhö hɨldʼan barannar hakalar haːs toktuːr hirdere baːr bu͡olaːččɨ. </ta>
            <ta e="T24" id="Seg_4665" s="T16">Onno ügüs bagajɨ uraha dʼi͡eler, hɨrga dʼi͡eler turaːččɨlar. </ta>
            <ta e="T35" id="Seg_4666" s="T25">Haːskɨ hɨlaːs küttere, kün tɨgar bu͡ollagɨna de dʼaktattar üleliːllere üksüːr. </ta>
            <ta e="T43" id="Seg_4667" s="T36">Tahaːra ginileriŋ maŋnaj iti tiriː ületin üleliːller. </ta>
            <ta e="T50" id="Seg_4668" s="T44">Tiriː, tiriːgin bert delbi ɨraːstɨːllar maŋnaj. </ta>
            <ta e="T57" id="Seg_4669" s="T51">Ɨraːstan barannar kɨptɨjɨnan kɨrɨjallar (ütr-) tüːtün. </ta>
            <ta e="T67" id="Seg_4670" s="T58">Ol kɨptɨjɨnan kɨrɨjan büttekterine, onton kojut bahagɨnan emi͡e kɨrɨjallar. </ta>
            <ta e="T76" id="Seg_4671" s="T68">Öldüːn ((PAUSE)) hakalar ((PAUSE)) olus köp kimi tiriːni ɨlaːččɨta hu͡oktar. </ta>
            <ta e="T93" id="Seg_4672" s="T77">Kɨrɨllɨbɨt ((PAUSE)) iti tüːtün ɨlɨllɨbɨttan (kɨrpa-) kɨrpalaːbɨt tüːnü kimi tiriːni ɨlannar, kim, dʼi͡e habɨːta, iti öldüːtteri tigeːččiler. </ta>
            <ta e="T102" id="Seg_4673" s="T94">Onnuk bu͡ollagɨna de ol barbak tüːnü keːheller bu͡o. </ta>
            <ta e="T107" id="Seg_4674" s="T103">Onton ontugunan öldüːn tigeller. </ta>
            <ta e="T116" id="Seg_4675" s="T108">Ol (öl-) horok tiriːni ginileriŋ iti harɨː oŋoru͡oktaːktar. </ta>
            <ta e="T124" id="Seg_4676" s="T117">Harɨː oŋorollorugar itinne de, hin ügüs üle: </ta>
            <ta e="T141" id="Seg_4677" s="T125">Kɨrpalɨːllar bu͡o, iti kɨrpalaːbɨttarɨn kördük, tüːte hu͡ok bu͡o tüːte agɨjak bu͡olu͡on, hin tüːte kaːlar bu͡o tiriːger. </ta>
            <ta e="T163" id="Seg_4678" s="T142">Ol tüː, tüːŋ üčügejdik barɨta tühü͡ön, onton kojut imiterge da üčügej bu͡o, harɨː bu͡olu͡on, ol tüːleːk kim tiriːni ginileriŋ hɨtɨtallar uːga. </ta>
            <ta e="T169" id="Seg_4679" s="T164">Uː ihiger mɨlanɨ kutallar, uːrallar. </ta>
            <ta e="T173" id="Seg_4680" s="T170">Onton eŋin-eŋin ottoru. </ta>
            <ta e="T181" id="Seg_4681" s="T174">Ol gɨna gɨna kas da künü hɨtɨ͡aktaːk. </ta>
            <ta e="T197" id="Seg_4682" s="T182">Itiː da kün bu͡ollun ol uːŋ hɨtɨjar bu͡o, tiriːŋ emi͡e hɨtɨjar bu͡o, imiges bu͡olar bu͡o. </ta>
            <ta e="T207" id="Seg_4683" s="T198">Tiriːŋ kas da künü hɨtɨjan büttegine ginileriŋ ol bulkujallar. </ta>
            <ta e="T217" id="Seg_4684" s="T208">Ol tiriːgin ɨlan barannar iti kim huːnarallar ürekke barannar. </ta>
            <ta e="T224" id="Seg_4685" s="T218">Delbi (huːnnaraldɨlar) ol gɨnan baran kuːrdallar. </ta>
            <ta e="T240" id="Seg_4686" s="T225">Kuːrdan barannara, iti tiriːgin kimniːller, gedereːninen ol kim kaːlbɨt tüːlerin ɨlallar (gedereː-) gedereːnniːller, hakalɨː haŋardakka. </ta>
            <ta e="T257" id="Seg_4687" s="T241">Ontuŋ emi͡e türgen da bu͡olbatak, dʼogus da bu͡olbatak, dʼaktattar bert kim ɨ͡arakan üle iti dʼaktarga da. </ta>
            <ta e="T270" id="Seg_4688" s="T258">Ol ihin biːrde oloroːt, onu barɨtɨn (hataːba- oloru͡oktaː-) üleli͡ektere hu͡oga, ɨ͡arakan üle. </ta>
            <ta e="T279" id="Seg_4689" s="T271">Onton nöŋü͡ö kün emi͡e oŋoru͡oktara, ol gedereːnni͡ektere emi͡e. </ta>
            <ta e="T292" id="Seg_4690" s="T280">Gedereːnnen büttekterine, ol harɨːŋ ((PAUSE)) kim kime hu͡ok bu͡olar bu͡o tüːte hu͡ok bu͡olar. </ta>
            <ta e="T304" id="Seg_4691" s="T293">Ontuŋ deksi bu͡olu͡on, iti kɨhɨ͡agɨnan, tu͡ogɨnan, eŋin eŋininen tarbɨːllar kɨhɨ͡aktɨːllar kanʼɨːllar. </ta>
            <ta e="T311" id="Seg_4692" s="T305">Bahagɨnan da eŋin eŋin deksiliːller, kanʼɨːllar. </ta>
            <ta e="T319" id="Seg_4693" s="T312">Tu͡ok kuhagan baːrɨn barɨtɨn iti ɨlattan iheller. </ta>
            <ta e="T329" id="Seg_4694" s="T320">Ol harɨːŋ, ol gɨnan baran, kördökkö üčügej bagaj bu͡olar. </ta>
            <ta e="T343" id="Seg_4695" s="T330">Onton harɨːŋ ile harɨː bu͡olu͡on, ol gɨnan bu͡o ɨːstaːk bu͡olu͡on, ontugun emi͡e imiti͡ekteːkter. </ta>
            <ta e="T353" id="Seg_4696" s="T344">Imiteːri gɨnnaktarɨna, de kimniːller, kɨːh ogolorun ɨgɨraːččɨlar togo ere. </ta>
            <ta e="T365" id="Seg_4697" s="T354">Iti kimi͡eke taŋas ɨjaːnar kimnerge ɨjɨːllar bu͡o ol tiriːgin, taŋas ɨjaːnar. </ta>
            <ta e="T370" id="Seg_4698" s="T366">Ontugun battan turu͡okkun naːda. </ta>
            <ta e="T385" id="Seg_4699" s="T371">Hɨrganɨ ti͡ere uːran barannar emi͡e battan turu͡okkun naːda ol tiriːni uːran baraŋŋɨn harɨː bu͡olu͡ogun. </ta>
            <ta e="T390" id="Seg_4700" s="T386">De ol battan turagɨn. </ta>
            <ta e="T402" id="Seg_4701" s="T391">Ol battan turdakkɨna eni͡eke bi͡ereller iti kimi bɨ͡arɨ, busput taba bɨ͡arɨn. </ta>
            <ta e="T411" id="Seg_4702" s="T402">Ol bɨ͡arɨ battɨː turan kɨːs kɨːs ogo (ɨstɨ͡an-) ɨstɨ͡aktaːk. </ta>
            <ta e="T417" id="Seg_4703" s="T411">Ɨstaː, diːller bu͡o, kajdi͡ek bu͡olu͡oŋuj ke? </ta>
            <ta e="T427" id="Seg_4704" s="T418">Bagarbat bu͡olu͡o bu͡olu͡o, ontuŋ amtana kuhagana, hin turagɨn bu͡o. </ta>
            <ta e="T432" id="Seg_4705" s="T428">Istegin bu͡o inʼeŋ haŋatɨn. </ta>
            <ta e="T442" id="Seg_4706" s="T433">Ol tura tura ɨstɨː turagɨn ol kimi, bɨ͡arɨ, turaŋŋɨn. </ta>
            <ta e="T461" id="Seg_4707" s="T443">Ginileriŋ ol (bɨ͡a-) ol ɨstaːbɨt bɨ͡argɨn ɨla ɨlallar, ol tiriːgin ol kimneːbit, gedereːnneːbit tiriːgin ɨlan barannar, ol biheller. </ta>
            <ta e="T469" id="Seg_4708" s="T462">Ol gɨnan baran (hoŋo-) hoŋohoːnunan hoŋohoːnnuːllar iliːlerinen. </ta>
            <ta e="T490" id="Seg_4709" s="T470">Biːr iliːnnen tutar ol tiriːgin, biːr iliːnnen hoŋohoːŋŋun tutar bu͡o, ol bu͡o üːhetten allaraːga di͡eri innʼe hoŋohoːnnuːr, eː bihe bihe. </ta>
            <ta e="T503" id="Seg_4710" s="T491">De ol gɨnan baran ginileriŋ kɨːh ogolor kahan da turu͡oktarɨn bagaraːččɨta hu͡oktar. </ta>
            <ta e="T514" id="Seg_4711" s="T504">Battan turu͡okkun naːda bu͡o ör bagajɨ turu͡okkun (naː-) turu͡oktaːkkɨn onno. </ta>
            <ta e="T522" id="Seg_4712" s="T515">A ɨstɨːrɨŋ baːr de kuhagan da kuhagan. </ta>
            <ta e="T532" id="Seg_4713" s="T523">Eː ɨstɨ͡aŋ, kihi da hürege loksuju͡ok horogor bu͡olar bu͡o. </ta>
            <ta e="T543" id="Seg_4714" s="T533">Ol ihin (kɨr-) kɨːh ogokoːttoruŋ küreneller bu͡o ɨstaːmaːrɨlar, baran kaːlallar. </ta>
            <ta e="T553" id="Seg_4715" s="T544">Kim kɨːh ogoloru ügüstük ɨgɨraːččɨlar, u͡ol ogotoːgor, togo ere. </ta>
            <ta e="T570" id="Seg_4716" s="T554">De ol ol gɨnan baran ol kiminen, delbi kimne hoŋohoːnunan delbi kimniː, kimneːtiler bu͡o ol tiriːgin. </ta>
            <ta e="T577" id="Seg_4717" s="T571">Ol hoŋohoːnnuːruŋ emi͡e ör bagajɨ bu͡olla. </ta>
            <ta e="T586" id="Seg_4718" s="T578">Nöŋü͡ö kün emi͡e turu͡oktaːkkɨn bu͡olla ol ɨstɨː ɨstɨːgɨn. </ta>
            <ta e="T592" id="Seg_4719" s="T587">Bagarbat bu͡olagɨn hüregiŋ loksujar bu͡olla. </ta>
            <ta e="T604" id="Seg_4720" s="T593">De emi͡e hin ɨstɨːgɨn bu͡olla, kajdi͡ek bu͡olu͡oŋuj, inʼeŋ haŋatɨn istegin bu͡o. </ta>
            <ta e="T616" id="Seg_4721" s="T605">De ol turan ɨstɨː ɨstɨːgɨn de muŋnanan muŋnanan, de bütebit bu͡olla. </ta>
            <ta e="T625" id="Seg_4722" s="T617">Kahɨs eme künüger de ol kɨhɨ͡ak kiminen hoŋohoːnnon. </ta>
            <ta e="T645" id="Seg_4723" s="T626">Onton kojut iti harɨː bu͡ol bukatɨn harɨː bu͡ol üčügej bu͡olu͡on, imiges bu͡olu͡on, iti eŋin eŋinnik iti kimniː hataːtɨbɨt bu͡o. </ta>
            <ta e="T649" id="Seg_4724" s="T646">Kɨhɨ͡aktan, gedereːnnen oŋorobut. </ta>
            <ta e="T658" id="Seg_4725" s="T650">Ontuŋ harɨː olus (maŋan) bu͡olaːrɨ tože hin kuhagan. </ta>
            <ta e="T667" id="Seg_4726" s="T659">Ol ihin harɨːgɨn iti kimi͡eke ɨhaːrallar ɨhaːrallar buru͡oga. </ta>
            <ta e="T675" id="Seg_4727" s="T668">Ol harɨːgɨn ɨlan barannar dʼi͡e ihiger ɨjɨːllar. </ta>
            <ta e="T687" id="Seg_4728" s="T676">Dʼi͡e da bu͡olu͡oj iti kim uraha ihiger, uraha dʼi͡e ihiger ɨjɨːllar. </ta>
            <ta e="T694" id="Seg_4729" s="T688">Onno buru͡oga ontuŋ ɨksa turar bu͡o. </ta>
            <ta e="T701" id="Seg_4730" s="T695">A horok dʼaktattar innʼe gɨnaːččɨlar bu͡olla. </ta>
            <ta e="T709" id="Seg_4731" s="T702">Ol kördük da ɨhɨ͡ara hɨtɨ͡ak ebitter otto. </ta>
            <ta e="T724" id="Seg_4732" s="T710">A gini, ginileriŋ innʼekeːt diːller bu͡o, bihigini turu͡orallar bu͡o, üs tü͡ört kɨːs kɨːh ogolor. </ta>
            <ta e="T725" id="Seg_4733" s="T724">Kim, kim u͡ota, kim, u͡ota iti ačaːg oŋorollor. </ta>
            <ta e="T734" id="Seg_4734" s="T725">Mas ubajar ontuŋ ürdütünen ottoru bɨragallar, ulakan buru͡o keli͡en. </ta>
            <ta e="T745" id="Seg_4735" s="T735">De ol buru͡o ihiger tögürüččü turammɨt, ol tutabɨt bu͡olla kimi. </ta>
            <ta e="T754" id="Seg_4736" s="T746">Ol gɨnan baran ergitebit, kün hu͡olun kördük ergitebit. </ta>
            <ta e="T760" id="Seg_4737" s="T755">Üčügejdik tutu͡oktaːkkɨn ontugun, ɨːppat kördük. </ta>
            <ta e="T766" id="Seg_4738" s="T761">(Beje-) bejebitiger ergitebit bu͡ol innʼekeːt. </ta>
            <ta e="T779" id="Seg_4739" s="T767">Min dogorbor bi͡eri͡em, dogorum nöŋü͡ö dogorgor, itirdikkeːn bi͡erer, eto tögürütebit ol tiriːni. </ta>
            <ta e="T785" id="Seg_4740" s="T780">Annɨgɨtɨttan ulakan buru͡o keler bu͡o. </ta>
            <ta e="T792" id="Seg_4741" s="T786">Bihi͡eke emi͡e kuhagan ol buru͡o kelere. </ta>
            <ta e="T798" id="Seg_4742" s="T793">Karakpɨt ahɨjar bu͡olla ol buru͡ottan. </ta>
            <ta e="T804" id="Seg_4743" s="T799">Anʼakpɨtɨgar da ɨlabɨt bu͡olla buru͡onu. </ta>
            <ta e="T817" id="Seg_4744" s="T805">De ol da bu͡ollar de turunaːktɨːbɨt bu͡o, turuŋ turuŋ diːller bu͡o bihigini. </ta>
            <ta e="T822" id="Seg_4745" s="T818">De ol kördük ergitebit. </ta>
            <ta e="T849" id="Seg_4746" s="T823">Onton biːr emete kɨːh ogoto tiriːni iliːtinen innʼekeːtiŋ ɨlan keːhi͡e, hɨːha tutu͡o, ontutun ol tutar hirin, tiriːtin de, oččogo diːller bu͡o, de erge barɨ͡aŋ hu͡oga, ulaːttakkɨna. </ta>
            <ta e="T856" id="Seg_4747" s="T850">Diː diːller itigirdikkeːn haŋarallar bu͡o bihigini. </ta>
            <ta e="T867" id="Seg_4748" s="T857">Ol ihin ol muŋnana turabɨt bu͡o ol tiriːni ɨːppakka ɨntak. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T2" id="Seg_4749" s="T1">köh-ö</ta>
            <ta e="T4" id="Seg_4750" s="T3">kɨhɨn-ɨ</ta>
            <ta e="T5" id="Seg_4751" s="T4">kɨhɨn-ɨ</ta>
            <ta e="T6" id="Seg_4752" s="T5">meldʼi</ta>
            <ta e="T7" id="Seg_4753" s="T6">köh-ö</ta>
            <ta e="T8" id="Seg_4754" s="T7">hɨldʼ-an</ta>
            <ta e="T9" id="Seg_4755" s="T8">bar-an-nar</ta>
            <ta e="T10" id="Seg_4756" s="T9">haka-lar</ta>
            <ta e="T11" id="Seg_4757" s="T10">haːs</ta>
            <ta e="T12" id="Seg_4758" s="T11">toktuː-r</ta>
            <ta e="T13" id="Seg_4759" s="T12">hir-dere</ta>
            <ta e="T14" id="Seg_4760" s="T13">baːr</ta>
            <ta e="T15" id="Seg_4761" s="T14">bu͡ol-aːččɨ</ta>
            <ta e="T17" id="Seg_4762" s="T16">onno</ta>
            <ta e="T18" id="Seg_4763" s="T17">ügüs</ta>
            <ta e="T19" id="Seg_4764" s="T18">bagajɨ</ta>
            <ta e="T20" id="Seg_4765" s="T19">uraha</ta>
            <ta e="T21" id="Seg_4766" s="T20">dʼi͡e-ler</ta>
            <ta e="T22" id="Seg_4767" s="T21">hɨrga</ta>
            <ta e="T23" id="Seg_4768" s="T22">dʼi͡e-ler</ta>
            <ta e="T24" id="Seg_4769" s="T23">tur-aːččɨ-lar</ta>
            <ta e="T26" id="Seg_4770" s="T25">haːs-kɨ</ta>
            <ta e="T27" id="Seg_4771" s="T26">hɨlaːs</ta>
            <ta e="T28" id="Seg_4772" s="T27">küt-ter-e</ta>
            <ta e="T29" id="Seg_4773" s="T28">kün</ta>
            <ta e="T30" id="Seg_4774" s="T29">tɨg-ar</ta>
            <ta e="T31" id="Seg_4775" s="T30">bu͡ol-lag-ɨna</ta>
            <ta e="T32" id="Seg_4776" s="T31">de</ta>
            <ta e="T33" id="Seg_4777" s="T32">dʼaktat-tar</ta>
            <ta e="T34" id="Seg_4778" s="T33">üleliː-l-lere</ta>
            <ta e="T35" id="Seg_4779" s="T34">üksüː-r</ta>
            <ta e="T37" id="Seg_4780" s="T36">tahaːra</ta>
            <ta e="T38" id="Seg_4781" s="T37">giniler-i-ŋ</ta>
            <ta e="T39" id="Seg_4782" s="T38">maŋnaj</ta>
            <ta e="T40" id="Seg_4783" s="T39">iti</ta>
            <ta e="T41" id="Seg_4784" s="T40">tiriː</ta>
            <ta e="T42" id="Seg_4785" s="T41">üle-ti-n</ta>
            <ta e="T43" id="Seg_4786" s="T42">üleliː-l-ler</ta>
            <ta e="T45" id="Seg_4787" s="T44">tiriː</ta>
            <ta e="T46" id="Seg_4788" s="T45">tiriː-gi-n</ta>
            <ta e="T47" id="Seg_4789" s="T46">bert</ta>
            <ta e="T48" id="Seg_4790" s="T47">delbi</ta>
            <ta e="T49" id="Seg_4791" s="T48">ɨraːstɨː-l-lar</ta>
            <ta e="T50" id="Seg_4792" s="T49">maŋnaj</ta>
            <ta e="T52" id="Seg_4793" s="T51">ɨraːst-an</ta>
            <ta e="T53" id="Seg_4794" s="T52">bar-an-nar</ta>
            <ta e="T54" id="Seg_4795" s="T53">kɨptɨj-ɨ-nan</ta>
            <ta e="T55" id="Seg_4796" s="T54">kɨrɨj-al-lar</ta>
            <ta e="T57" id="Seg_4797" s="T56">tüː-tü-n</ta>
            <ta e="T59" id="Seg_4798" s="T58">ol</ta>
            <ta e="T60" id="Seg_4799" s="T59">kɨptɨj-ɨ-nan</ta>
            <ta e="T61" id="Seg_4800" s="T60">kɨrɨj-an</ta>
            <ta e="T62" id="Seg_4801" s="T61">büt-tek-terine</ta>
            <ta e="T63" id="Seg_4802" s="T62">on-ton</ta>
            <ta e="T64" id="Seg_4803" s="T63">kojut</ta>
            <ta e="T65" id="Seg_4804" s="T64">bahag-ɨ-nan</ta>
            <ta e="T66" id="Seg_4805" s="T65">emi͡e</ta>
            <ta e="T67" id="Seg_4806" s="T66">kɨrɨj-al-lar</ta>
            <ta e="T69" id="Seg_4807" s="T68">öldüːn</ta>
            <ta e="T70" id="Seg_4808" s="T69">haka-lar</ta>
            <ta e="T71" id="Seg_4809" s="T70">olus</ta>
            <ta e="T72" id="Seg_4810" s="T71">köp</ta>
            <ta e="T73" id="Seg_4811" s="T72">kim-i</ta>
            <ta e="T74" id="Seg_4812" s="T73">tiriː-ni</ta>
            <ta e="T75" id="Seg_4813" s="T74">ɨl-aːččɨ-ta</ta>
            <ta e="T76" id="Seg_4814" s="T75">hu͡ok-tar</ta>
            <ta e="T78" id="Seg_4815" s="T77">kɨrɨ-ll-ɨ-bɨt</ta>
            <ta e="T79" id="Seg_4816" s="T78">iti</ta>
            <ta e="T80" id="Seg_4817" s="T79">tüː-tü-n</ta>
            <ta e="T81" id="Seg_4818" s="T80">ɨl-ɨ-ll-ɨ-bɨt-tan</ta>
            <ta e="T83" id="Seg_4819" s="T82">kɨrpalaː-bɨt</ta>
            <ta e="T84" id="Seg_4820" s="T83">tüː-nü</ta>
            <ta e="T85" id="Seg_4821" s="T84">kim-i</ta>
            <ta e="T86" id="Seg_4822" s="T85">tiriː-ni</ta>
            <ta e="T87" id="Seg_4823" s="T86">ɨl-an-nar</ta>
            <ta e="T88" id="Seg_4824" s="T87">kim</ta>
            <ta e="T89" id="Seg_4825" s="T88">dʼi͡e</ta>
            <ta e="T90" id="Seg_4826" s="T89">habɨː-ta</ta>
            <ta e="T91" id="Seg_4827" s="T90">iti</ta>
            <ta e="T92" id="Seg_4828" s="T91">öldüːt-ter-i</ta>
            <ta e="T93" id="Seg_4829" s="T92">tig-eːčči-ler</ta>
            <ta e="T95" id="Seg_4830" s="T94">onnuk</ta>
            <ta e="T96" id="Seg_4831" s="T95">bu͡ollagɨna</ta>
            <ta e="T97" id="Seg_4832" s="T96">de</ta>
            <ta e="T98" id="Seg_4833" s="T97">ol</ta>
            <ta e="T99" id="Seg_4834" s="T98">barbak</ta>
            <ta e="T100" id="Seg_4835" s="T99">tüː-nü</ta>
            <ta e="T101" id="Seg_4836" s="T100">keːh-el-ler</ta>
            <ta e="T102" id="Seg_4837" s="T101">bu͡o</ta>
            <ta e="T104" id="Seg_4838" s="T103">on-ton</ta>
            <ta e="T105" id="Seg_4839" s="T104">on-tu-gu-nan</ta>
            <ta e="T106" id="Seg_4840" s="T105">öldüːn</ta>
            <ta e="T107" id="Seg_4841" s="T106">tig-el-ler</ta>
            <ta e="T109" id="Seg_4842" s="T108">ol</ta>
            <ta e="T111" id="Seg_4843" s="T110">horok</ta>
            <ta e="T112" id="Seg_4844" s="T111">tiriː-ni</ta>
            <ta e="T113" id="Seg_4845" s="T112">giniler-i-ŋ</ta>
            <ta e="T114" id="Seg_4846" s="T113">iti</ta>
            <ta e="T115" id="Seg_4847" s="T114">harɨː</ta>
            <ta e="T116" id="Seg_4848" s="T115">oŋor-u͡ok-taːk-tar</ta>
            <ta e="T118" id="Seg_4849" s="T117">harɨː</ta>
            <ta e="T119" id="Seg_4850" s="T118">oŋor-ol-loru-gar</ta>
            <ta e="T120" id="Seg_4851" s="T119">itinne</ta>
            <ta e="T121" id="Seg_4852" s="T120">de</ta>
            <ta e="T122" id="Seg_4853" s="T121">hin</ta>
            <ta e="T123" id="Seg_4854" s="T122">ügüs</ta>
            <ta e="T124" id="Seg_4855" s="T123">üle</ta>
            <ta e="T126" id="Seg_4856" s="T125">kɨrpalɨː-l-lar</ta>
            <ta e="T127" id="Seg_4857" s="T126">bu͡o</ta>
            <ta e="T128" id="Seg_4858" s="T127">iti</ta>
            <ta e="T129" id="Seg_4859" s="T128">kɨrpalaː-bɨt-tarɨ-n</ta>
            <ta e="T130" id="Seg_4860" s="T129">kördük</ta>
            <ta e="T131" id="Seg_4861" s="T130">tüː-te</ta>
            <ta e="T132" id="Seg_4862" s="T131">hu͡ok</ta>
            <ta e="T133" id="Seg_4863" s="T132">bu͡o</ta>
            <ta e="T134" id="Seg_4864" s="T133">tüː-te</ta>
            <ta e="T135" id="Seg_4865" s="T134">agɨjak</ta>
            <ta e="T136" id="Seg_4866" s="T135">bu͡ol-u͡o-n</ta>
            <ta e="T137" id="Seg_4867" s="T136">hin</ta>
            <ta e="T138" id="Seg_4868" s="T137">tüː-te</ta>
            <ta e="T139" id="Seg_4869" s="T138">kaːl-ar</ta>
            <ta e="T140" id="Seg_4870" s="T139">bu͡o</ta>
            <ta e="T141" id="Seg_4871" s="T140">tiriː-ge-r</ta>
            <ta e="T143" id="Seg_4872" s="T142">ol</ta>
            <ta e="T144" id="Seg_4873" s="T143">tüː</ta>
            <ta e="T145" id="Seg_4874" s="T144">tüː-ŋ</ta>
            <ta e="T146" id="Seg_4875" s="T145">üčügej-dik</ta>
            <ta e="T147" id="Seg_4876" s="T146">barɨta</ta>
            <ta e="T148" id="Seg_4877" s="T147">tüh-ü͡ö-n</ta>
            <ta e="T149" id="Seg_4878" s="T148">on-ton</ta>
            <ta e="T150" id="Seg_4879" s="T149">kojut</ta>
            <ta e="T151" id="Seg_4880" s="T150">imit-er-ge</ta>
            <ta e="T152" id="Seg_4881" s="T151">da</ta>
            <ta e="T153" id="Seg_4882" s="T152">üčügej</ta>
            <ta e="T154" id="Seg_4883" s="T153">bu͡o</ta>
            <ta e="T155" id="Seg_4884" s="T154">harɨː</ta>
            <ta e="T156" id="Seg_4885" s="T155">bu͡ol-u͡o-n</ta>
            <ta e="T157" id="Seg_4886" s="T156">ol</ta>
            <ta e="T158" id="Seg_4887" s="T157">tüː-leːk</ta>
            <ta e="T159" id="Seg_4888" s="T158">kim</ta>
            <ta e="T160" id="Seg_4889" s="T159">tiriː-ni</ta>
            <ta e="T161" id="Seg_4890" s="T160">giniler-i-ŋ</ta>
            <ta e="T162" id="Seg_4891" s="T161">hɨt-ɨ-t-al-lar</ta>
            <ta e="T163" id="Seg_4892" s="T162">uː-ga</ta>
            <ta e="T165" id="Seg_4893" s="T164">uː</ta>
            <ta e="T166" id="Seg_4894" s="T165">ih-i-ger</ta>
            <ta e="T167" id="Seg_4895" s="T166">mɨla-nɨ</ta>
            <ta e="T168" id="Seg_4896" s="T167">kut-al-lar</ta>
            <ta e="T169" id="Seg_4897" s="T168">uːr-al-lar</ta>
            <ta e="T171" id="Seg_4898" s="T170">on-ton</ta>
            <ta e="T172" id="Seg_4899" s="T171">eŋin-eŋin</ta>
            <ta e="T173" id="Seg_4900" s="T172">ot-tor-u</ta>
            <ta e="T175" id="Seg_4901" s="T174">ol</ta>
            <ta e="T176" id="Seg_4902" s="T175">gɨn-a</ta>
            <ta e="T177" id="Seg_4903" s="T176">gɨn-a</ta>
            <ta e="T178" id="Seg_4904" s="T177">kas</ta>
            <ta e="T179" id="Seg_4905" s="T178">da</ta>
            <ta e="T180" id="Seg_4906" s="T179">kün-ü</ta>
            <ta e="T181" id="Seg_4907" s="T180">hɨt-ɨ͡ak-taːk</ta>
            <ta e="T183" id="Seg_4908" s="T182">itiː</ta>
            <ta e="T184" id="Seg_4909" s="T183">da</ta>
            <ta e="T185" id="Seg_4910" s="T184">kün</ta>
            <ta e="T186" id="Seg_4911" s="T185">bu͡ol-lun</ta>
            <ta e="T187" id="Seg_4912" s="T186">ol</ta>
            <ta e="T188" id="Seg_4913" s="T187">uː-ŋ</ta>
            <ta e="T189" id="Seg_4914" s="T188">hɨtɨj-ar</ta>
            <ta e="T190" id="Seg_4915" s="T189">bu͡o</ta>
            <ta e="T191" id="Seg_4916" s="T190">tiriː-ŋ</ta>
            <ta e="T192" id="Seg_4917" s="T191">emi͡e</ta>
            <ta e="T193" id="Seg_4918" s="T192">hɨtɨj-ar</ta>
            <ta e="T194" id="Seg_4919" s="T193">bu͡o</ta>
            <ta e="T195" id="Seg_4920" s="T194">imiges</ta>
            <ta e="T196" id="Seg_4921" s="T195">bu͡ol-ar</ta>
            <ta e="T197" id="Seg_4922" s="T196">bu͡o</ta>
            <ta e="T199" id="Seg_4923" s="T198">tiriː-ŋ</ta>
            <ta e="T200" id="Seg_4924" s="T199">kas</ta>
            <ta e="T201" id="Seg_4925" s="T200">da</ta>
            <ta e="T202" id="Seg_4926" s="T201">kün-ü</ta>
            <ta e="T203" id="Seg_4927" s="T202">hɨtɨj-an</ta>
            <ta e="T204" id="Seg_4928" s="T203">büt-teg-ine</ta>
            <ta e="T205" id="Seg_4929" s="T204">giniler-i-ŋ</ta>
            <ta e="T206" id="Seg_4930" s="T205">ol</ta>
            <ta e="T207" id="Seg_4931" s="T206">bulkuj-al-lar</ta>
            <ta e="T209" id="Seg_4932" s="T208">ol</ta>
            <ta e="T210" id="Seg_4933" s="T209">tiriː-gi-n</ta>
            <ta e="T211" id="Seg_4934" s="T210">ɨl-an</ta>
            <ta e="T212" id="Seg_4935" s="T211">bar-an-nar</ta>
            <ta e="T213" id="Seg_4936" s="T212">iti</ta>
            <ta e="T214" id="Seg_4937" s="T213">kim</ta>
            <ta e="T215" id="Seg_4938" s="T214">huːn-ar-al-lar</ta>
            <ta e="T216" id="Seg_4939" s="T215">ürek-ke</ta>
            <ta e="T217" id="Seg_4940" s="T216">bar-an-nar</ta>
            <ta e="T219" id="Seg_4941" s="T218">delbi</ta>
            <ta e="T220" id="Seg_4942" s="T219">huːn-nar-al-dɨ-lar</ta>
            <ta e="T221" id="Seg_4943" s="T220">ol</ta>
            <ta e="T222" id="Seg_4944" s="T221">gɨn-an</ta>
            <ta e="T223" id="Seg_4945" s="T222">baran</ta>
            <ta e="T224" id="Seg_4946" s="T223">kuːr-d-al-lar</ta>
            <ta e="T226" id="Seg_4947" s="T225">kuːr-d-an</ta>
            <ta e="T227" id="Seg_4948" s="T226">bar-an-nara</ta>
            <ta e="T228" id="Seg_4949" s="T227">iti</ta>
            <ta e="T229" id="Seg_4950" s="T228">tiriː-gi-n</ta>
            <ta e="T230" id="Seg_4951" s="T229">kim-niː-l-ler</ta>
            <ta e="T231" id="Seg_4952" s="T230">gedereːn-i-nen</ta>
            <ta e="T232" id="Seg_4953" s="T231">ol</ta>
            <ta e="T233" id="Seg_4954" s="T232">kim</ta>
            <ta e="T234" id="Seg_4955" s="T233">kaːl-bɨt</ta>
            <ta e="T235" id="Seg_4956" s="T234">tüː-leri-n</ta>
            <ta e="T236" id="Seg_4957" s="T235">ɨl-al-lar</ta>
            <ta e="T238" id="Seg_4958" s="T237">gedereːn-niː-l-ler</ta>
            <ta e="T239" id="Seg_4959" s="T238">haka-lɨː</ta>
            <ta e="T240" id="Seg_4960" s="T239">haŋar-dak-ka</ta>
            <ta e="T242" id="Seg_4961" s="T241">on-tu-ŋ</ta>
            <ta e="T243" id="Seg_4962" s="T242">emi͡e</ta>
            <ta e="T244" id="Seg_4963" s="T243">türgen</ta>
            <ta e="T245" id="Seg_4964" s="T244">da</ta>
            <ta e="T246" id="Seg_4965" s="T245">bu͡ol-batak</ta>
            <ta e="T247" id="Seg_4966" s="T246">dʼogus</ta>
            <ta e="T248" id="Seg_4967" s="T247">da</ta>
            <ta e="T249" id="Seg_4968" s="T248">bu͡ol-batak</ta>
            <ta e="T250" id="Seg_4969" s="T249">dʼaktat-tar</ta>
            <ta e="T251" id="Seg_4970" s="T250">bert</ta>
            <ta e="T252" id="Seg_4971" s="T251">kim</ta>
            <ta e="T253" id="Seg_4972" s="T252">ɨ͡arakan</ta>
            <ta e="T254" id="Seg_4973" s="T253">üle</ta>
            <ta e="T255" id="Seg_4974" s="T254">iti</ta>
            <ta e="T256" id="Seg_4975" s="T255">dʼaktar-ga</ta>
            <ta e="T257" id="Seg_4976" s="T256">da</ta>
            <ta e="T259" id="Seg_4977" s="T258">ol</ta>
            <ta e="T260" id="Seg_4978" s="T259">ihin</ta>
            <ta e="T261" id="Seg_4979" s="T260">biːrde</ta>
            <ta e="T262" id="Seg_4980" s="T261">olor-oːt</ta>
            <ta e="T263" id="Seg_4981" s="T262">o-nu</ta>
            <ta e="T264" id="Seg_4982" s="T263">barɨ-tɨ-n</ta>
            <ta e="T267" id="Seg_4983" s="T266">ülel-i͡ek-tere</ta>
            <ta e="T268" id="Seg_4984" s="T267">hu͡og-a</ta>
            <ta e="T269" id="Seg_4985" s="T268">ɨ͡arakan</ta>
            <ta e="T270" id="Seg_4986" s="T269">üle</ta>
            <ta e="T272" id="Seg_4987" s="T271">on-ton</ta>
            <ta e="T273" id="Seg_4988" s="T272">nöŋü͡ö</ta>
            <ta e="T274" id="Seg_4989" s="T273">kün</ta>
            <ta e="T275" id="Seg_4990" s="T274">emi͡e</ta>
            <ta e="T276" id="Seg_4991" s="T275">oŋor-u͡ok-tara</ta>
            <ta e="T277" id="Seg_4992" s="T276">ol</ta>
            <ta e="T278" id="Seg_4993" s="T277">gedereːn-n-i͡ek-tere</ta>
            <ta e="T279" id="Seg_4994" s="T278">emi͡e</ta>
            <ta e="T281" id="Seg_4995" s="T280">gedereːn-n-en</ta>
            <ta e="T282" id="Seg_4996" s="T281">büt-tek-terine</ta>
            <ta e="T283" id="Seg_4997" s="T282">ol</ta>
            <ta e="T284" id="Seg_4998" s="T283">harɨː-ŋ</ta>
            <ta e="T285" id="Seg_4999" s="T284">kim</ta>
            <ta e="T286" id="Seg_5000" s="T285">kim-e</ta>
            <ta e="T287" id="Seg_5001" s="T286">hu͡ok</ta>
            <ta e="T288" id="Seg_5002" s="T287">bu͡ol-ar</ta>
            <ta e="T289" id="Seg_5003" s="T288">bu͡o</ta>
            <ta e="T290" id="Seg_5004" s="T289">tüː-te</ta>
            <ta e="T291" id="Seg_5005" s="T290">hu͡ok</ta>
            <ta e="T292" id="Seg_5006" s="T291">bu͡ol-ar</ta>
            <ta e="T294" id="Seg_5007" s="T293">on-tu-ŋ</ta>
            <ta e="T295" id="Seg_5008" s="T294">deksi</ta>
            <ta e="T296" id="Seg_5009" s="T295">bu͡ol-u͡o-n</ta>
            <ta e="T297" id="Seg_5010" s="T296">iti</ta>
            <ta e="T298" id="Seg_5011" s="T297">kɨhɨ͡ag-ɨ-nan</ta>
            <ta e="T299" id="Seg_5012" s="T298">tu͡og-ɨ-nan</ta>
            <ta e="T300" id="Seg_5013" s="T299">eŋin</ta>
            <ta e="T301" id="Seg_5014" s="T300">eŋin-i-nen</ta>
            <ta e="T302" id="Seg_5015" s="T301">tarbɨː-l-lar</ta>
            <ta e="T303" id="Seg_5016" s="T302">kɨhɨ͡ak-tɨː-l-lar</ta>
            <ta e="T304" id="Seg_5017" s="T303">kanʼɨː-l-lar</ta>
            <ta e="T306" id="Seg_5018" s="T305">bahag-ɨ-nan</ta>
            <ta e="T307" id="Seg_5019" s="T306">da</ta>
            <ta e="T308" id="Seg_5020" s="T307">eŋin</ta>
            <ta e="T309" id="Seg_5021" s="T308">eŋin</ta>
            <ta e="T310" id="Seg_5022" s="T309">deksi-liː-l-ler</ta>
            <ta e="T311" id="Seg_5023" s="T310">kanʼɨː-l-lar</ta>
            <ta e="T313" id="Seg_5024" s="T312">tu͡ok</ta>
            <ta e="T314" id="Seg_5025" s="T313">kuhagan</ta>
            <ta e="T315" id="Seg_5026" s="T314">baːr-ɨ-n</ta>
            <ta e="T316" id="Seg_5027" s="T315">barɨ-tɨ-n</ta>
            <ta e="T317" id="Seg_5028" s="T316">iti</ta>
            <ta e="T318" id="Seg_5029" s="T317">ɨl-atta-n</ta>
            <ta e="T319" id="Seg_5030" s="T318">ih-el-ler</ta>
            <ta e="T321" id="Seg_5031" s="T320">ol</ta>
            <ta e="T322" id="Seg_5032" s="T321">harɨː-ŋ</ta>
            <ta e="T323" id="Seg_5033" s="T322">ol</ta>
            <ta e="T324" id="Seg_5034" s="T323">gɨn-an</ta>
            <ta e="T325" id="Seg_5035" s="T324">baran</ta>
            <ta e="T326" id="Seg_5036" s="T325">kör-dök-kö</ta>
            <ta e="T327" id="Seg_5037" s="T326">üčügej</ta>
            <ta e="T328" id="Seg_5038" s="T327">bagaj</ta>
            <ta e="T329" id="Seg_5039" s="T328">bu͡ol-ar</ta>
            <ta e="T331" id="Seg_5040" s="T330">on-ton</ta>
            <ta e="T332" id="Seg_5041" s="T331">harɨː-ŋ</ta>
            <ta e="T333" id="Seg_5042" s="T332">ile</ta>
            <ta e="T334" id="Seg_5043" s="T333">harɨː</ta>
            <ta e="T335" id="Seg_5044" s="T334">bu͡ol-u͡o-n</ta>
            <ta e="T336" id="Seg_5045" s="T335">ol</ta>
            <ta e="T337" id="Seg_5046" s="T336">gɨn-an</ta>
            <ta e="T338" id="Seg_5047" s="T337">bu͡o</ta>
            <ta e="T339" id="Seg_5048" s="T338">ɨːs-taːk</ta>
            <ta e="T340" id="Seg_5049" s="T339">bu͡ol-u͡o-n</ta>
            <ta e="T341" id="Seg_5050" s="T340">on-tu-gu-n</ta>
            <ta e="T342" id="Seg_5051" s="T341">emi͡e</ta>
            <ta e="T343" id="Seg_5052" s="T342">imit-i͡ek-teːk-ter</ta>
            <ta e="T345" id="Seg_5053" s="T344">imit-eːri</ta>
            <ta e="T346" id="Seg_5054" s="T345">gɨn-nak-tarɨna</ta>
            <ta e="T347" id="Seg_5055" s="T346">de</ta>
            <ta e="T348" id="Seg_5056" s="T347">kim-niː-l-ler</ta>
            <ta e="T349" id="Seg_5057" s="T348">kɨːh</ta>
            <ta e="T350" id="Seg_5058" s="T349">ogo-loru-n</ta>
            <ta e="T351" id="Seg_5059" s="T350">ɨgɨr-aːččɨ-lar</ta>
            <ta e="T352" id="Seg_5060" s="T351">togo</ta>
            <ta e="T353" id="Seg_5061" s="T352">ere</ta>
            <ta e="T355" id="Seg_5062" s="T354">iti</ta>
            <ta e="T356" id="Seg_5063" s="T355">kimi͡e-ke</ta>
            <ta e="T357" id="Seg_5064" s="T356">taŋas</ta>
            <ta e="T358" id="Seg_5065" s="T357">ɨjaː-n-ar</ta>
            <ta e="T359" id="Seg_5066" s="T358">kim-ner-ge</ta>
            <ta e="T360" id="Seg_5067" s="T359">ɨjɨː-l-lar</ta>
            <ta e="T361" id="Seg_5068" s="T360">bu͡o</ta>
            <ta e="T362" id="Seg_5069" s="T361">ol</ta>
            <ta e="T363" id="Seg_5070" s="T362">tiriː-gi-n</ta>
            <ta e="T364" id="Seg_5071" s="T363">taŋas</ta>
            <ta e="T365" id="Seg_5072" s="T364">ɨjaː-n-ar</ta>
            <ta e="T367" id="Seg_5073" s="T366">on-tu-gu-n</ta>
            <ta e="T368" id="Seg_5074" s="T367">batt-an</ta>
            <ta e="T369" id="Seg_5075" s="T368">tur-u͡ok-ku-n</ta>
            <ta e="T370" id="Seg_5076" s="T369">naːda</ta>
            <ta e="T372" id="Seg_5077" s="T371">hɨrga-nɨ</ta>
            <ta e="T373" id="Seg_5078" s="T372">ti͡er-e</ta>
            <ta e="T374" id="Seg_5079" s="T373">uːr-an</ta>
            <ta e="T375" id="Seg_5080" s="T374">bar-an-nar</ta>
            <ta e="T376" id="Seg_5081" s="T375">emi͡e</ta>
            <ta e="T377" id="Seg_5082" s="T376">batt-an</ta>
            <ta e="T378" id="Seg_5083" s="T377">tur-u͡ok-ku-n</ta>
            <ta e="T379" id="Seg_5084" s="T378">naːda</ta>
            <ta e="T380" id="Seg_5085" s="T379">ol</ta>
            <ta e="T381" id="Seg_5086" s="T380">tiriː-ni</ta>
            <ta e="T382" id="Seg_5087" s="T381">uːr-an</ta>
            <ta e="T383" id="Seg_5088" s="T382">bar-aŋ-ŋɨn</ta>
            <ta e="T384" id="Seg_5089" s="T383">harɨː</ta>
            <ta e="T385" id="Seg_5090" s="T384">bu͡ol-u͡og-u-n</ta>
            <ta e="T387" id="Seg_5091" s="T386">de</ta>
            <ta e="T388" id="Seg_5092" s="T387">ol</ta>
            <ta e="T389" id="Seg_5093" s="T388">batt-an</ta>
            <ta e="T390" id="Seg_5094" s="T389">tur-a-gɨn</ta>
            <ta e="T392" id="Seg_5095" s="T391">ol</ta>
            <ta e="T393" id="Seg_5096" s="T392">batt-an</ta>
            <ta e="T394" id="Seg_5097" s="T393">tur-dak-kɨna</ta>
            <ta e="T395" id="Seg_5098" s="T394">eni͡e-ke</ta>
            <ta e="T396" id="Seg_5099" s="T395">bi͡er-el-ler</ta>
            <ta e="T397" id="Seg_5100" s="T396">iti</ta>
            <ta e="T398" id="Seg_5101" s="T397">kim-i</ta>
            <ta e="T399" id="Seg_5102" s="T398">bɨ͡ar-ɨ</ta>
            <ta e="T400" id="Seg_5103" s="T399">bus-put</ta>
            <ta e="T401" id="Seg_5104" s="T400">taba</ta>
            <ta e="T402" id="Seg_5105" s="T401">bɨ͡ar-ɨ-n</ta>
            <ta e="T403" id="Seg_5106" s="T402">ol</ta>
            <ta e="T404" id="Seg_5107" s="T403">bɨ͡ar-ɨ</ta>
            <ta e="T405" id="Seg_5108" s="T404">batt-ɨː</ta>
            <ta e="T406" id="Seg_5109" s="T405">tur-an</ta>
            <ta e="T407" id="Seg_5110" s="T406">kɨːs</ta>
            <ta e="T408" id="Seg_5111" s="T407">kɨːs</ta>
            <ta e="T409" id="Seg_5112" s="T408">ogo</ta>
            <ta e="T411" id="Seg_5113" s="T410">ɨst-ɨ͡ak-taːk</ta>
            <ta e="T412" id="Seg_5114" s="T411">ɨstaː</ta>
            <ta e="T413" id="Seg_5115" s="T412">diː-l-ler</ta>
            <ta e="T414" id="Seg_5116" s="T413">bu͡o</ta>
            <ta e="T415" id="Seg_5117" s="T414">kajdi͡ek</ta>
            <ta e="T416" id="Seg_5118" s="T415">bu͡ol-u͡o-ŋ=uj</ta>
            <ta e="T417" id="Seg_5119" s="T416">ke</ta>
            <ta e="T419" id="Seg_5120" s="T418">bagar-bat</ta>
            <ta e="T420" id="Seg_5121" s="T419">bu͡ol-u͡o</ta>
            <ta e="T421" id="Seg_5122" s="T420">bu͡olu͡o</ta>
            <ta e="T422" id="Seg_5123" s="T421">on-tu-ŋ</ta>
            <ta e="T423" id="Seg_5124" s="T422">amtan-a</ta>
            <ta e="T424" id="Seg_5125" s="T423">kuhagana</ta>
            <ta e="T425" id="Seg_5126" s="T424">hin</ta>
            <ta e="T426" id="Seg_5127" s="T425">tur-a-gɨn</ta>
            <ta e="T427" id="Seg_5128" s="T426">bu͡o</ta>
            <ta e="T429" id="Seg_5129" s="T428">ist-e-gin</ta>
            <ta e="T430" id="Seg_5130" s="T429">bu͡o</ta>
            <ta e="T431" id="Seg_5131" s="T430">inʼe-ŋ</ta>
            <ta e="T432" id="Seg_5132" s="T431">haŋa-tɨ-n</ta>
            <ta e="T434" id="Seg_5133" s="T433">ol</ta>
            <ta e="T435" id="Seg_5134" s="T434">tur-a</ta>
            <ta e="T436" id="Seg_5135" s="T435">tur-a</ta>
            <ta e="T437" id="Seg_5136" s="T436">ɨst-ɨː</ta>
            <ta e="T438" id="Seg_5137" s="T437">tur-a-gɨn</ta>
            <ta e="T439" id="Seg_5138" s="T438">ol</ta>
            <ta e="T440" id="Seg_5139" s="T439">kim-i</ta>
            <ta e="T441" id="Seg_5140" s="T440">bɨ͡ar-ɨ</ta>
            <ta e="T442" id="Seg_5141" s="T441">tur-aŋ-ŋɨn</ta>
            <ta e="T444" id="Seg_5142" s="T443">giniler-i-ŋ</ta>
            <ta e="T445" id="Seg_5143" s="T444">ol</ta>
            <ta e="T447" id="Seg_5144" s="T446">ol</ta>
            <ta e="T448" id="Seg_5145" s="T447">ɨstaː-bɨt</ta>
            <ta e="T449" id="Seg_5146" s="T448">bɨ͡ar-gɨ-n</ta>
            <ta e="T450" id="Seg_5147" s="T449">ɨl-a</ta>
            <ta e="T451" id="Seg_5148" s="T450">ɨl-al-lar</ta>
            <ta e="T452" id="Seg_5149" s="T451">ol</ta>
            <ta e="T453" id="Seg_5150" s="T452">tiriː-gi-n</ta>
            <ta e="T454" id="Seg_5151" s="T453">ol</ta>
            <ta e="T455" id="Seg_5152" s="T454">kim-neː-bit</ta>
            <ta e="T456" id="Seg_5153" s="T455">gedereːn-neː-bit</ta>
            <ta e="T457" id="Seg_5154" s="T456">tiriː-gi-n</ta>
            <ta e="T458" id="Seg_5155" s="T457">ɨl-an</ta>
            <ta e="T459" id="Seg_5156" s="T458">bar-an-nar</ta>
            <ta e="T460" id="Seg_5157" s="T459">ol</ta>
            <ta e="T461" id="Seg_5158" s="T460">bih-el-ler</ta>
            <ta e="T463" id="Seg_5159" s="T462">ol</ta>
            <ta e="T464" id="Seg_5160" s="T463">gɨn-an</ta>
            <ta e="T465" id="Seg_5161" s="T464">baran</ta>
            <ta e="T467" id="Seg_5162" s="T466">hoŋohoːn-u-nan</ta>
            <ta e="T468" id="Seg_5163" s="T467">hoŋohoːn-nuː-l-lar</ta>
            <ta e="T469" id="Seg_5164" s="T468">iliː-leri-nen</ta>
            <ta e="T471" id="Seg_5165" s="T470">biːr</ta>
            <ta e="T472" id="Seg_5166" s="T471">iliː-nnen</ta>
            <ta e="T473" id="Seg_5167" s="T472">tut-ar</ta>
            <ta e="T474" id="Seg_5168" s="T473">ol</ta>
            <ta e="T475" id="Seg_5169" s="T474">tiriː-gi-n</ta>
            <ta e="T476" id="Seg_5170" s="T475">biːr</ta>
            <ta e="T477" id="Seg_5171" s="T476">iliː-nnen</ta>
            <ta e="T478" id="Seg_5172" s="T477">hoŋohoːŋ-ŋu-n</ta>
            <ta e="T479" id="Seg_5173" s="T478">tut-ar</ta>
            <ta e="T480" id="Seg_5174" s="T479">bu͡o</ta>
            <ta e="T481" id="Seg_5175" s="T480">ol</ta>
            <ta e="T482" id="Seg_5176" s="T481">bu͡o</ta>
            <ta e="T483" id="Seg_5177" s="T482">üːhe-tten</ta>
            <ta e="T484" id="Seg_5178" s="T483">allaraː-ga</ta>
            <ta e="T485" id="Seg_5179" s="T484">di͡eri</ta>
            <ta e="T486" id="Seg_5180" s="T485">innʼe</ta>
            <ta e="T487" id="Seg_5181" s="T486">hoŋohoːn-nuː-r</ta>
            <ta e="T488" id="Seg_5182" s="T487">eː</ta>
            <ta e="T489" id="Seg_5183" s="T488">bih-e</ta>
            <ta e="T490" id="Seg_5184" s="T489">bih-e</ta>
            <ta e="T492" id="Seg_5185" s="T491">de</ta>
            <ta e="T493" id="Seg_5186" s="T492">ol</ta>
            <ta e="T494" id="Seg_5187" s="T493">gɨn-an</ta>
            <ta e="T495" id="Seg_5188" s="T494">baran</ta>
            <ta e="T496" id="Seg_5189" s="T495">giniler-i-ŋ</ta>
            <ta e="T497" id="Seg_5190" s="T496">kɨːh</ta>
            <ta e="T498" id="Seg_5191" s="T497">ogo-lor</ta>
            <ta e="T499" id="Seg_5192" s="T498">kahan</ta>
            <ta e="T500" id="Seg_5193" s="T499">da</ta>
            <ta e="T501" id="Seg_5194" s="T500">tur-u͡ok-tarɨ-n</ta>
            <ta e="T502" id="Seg_5195" s="T501">bagar-aːččɨ-ta</ta>
            <ta e="T503" id="Seg_5196" s="T502">hu͡ok-tar</ta>
            <ta e="T505" id="Seg_5197" s="T504">batt-an</ta>
            <ta e="T506" id="Seg_5198" s="T505">tur-u͡ok-ku-n</ta>
            <ta e="T507" id="Seg_5199" s="T506">naːda</ta>
            <ta e="T508" id="Seg_5200" s="T507">bu͡o</ta>
            <ta e="T509" id="Seg_5201" s="T508">ör</ta>
            <ta e="T510" id="Seg_5202" s="T509">bagajɨ</ta>
            <ta e="T511" id="Seg_5203" s="T510">tur-u͡ok-ku-n</ta>
            <ta e="T513" id="Seg_5204" s="T512">tur-u͡ok-taːk-kɨn</ta>
            <ta e="T514" id="Seg_5205" s="T513">onno</ta>
            <ta e="T516" id="Seg_5206" s="T515">a</ta>
            <ta e="T517" id="Seg_5207" s="T516">ɨstɨː-r-ɨ-ŋ</ta>
            <ta e="T518" id="Seg_5208" s="T517">baːr</ta>
            <ta e="T519" id="Seg_5209" s="T518">de</ta>
            <ta e="T520" id="Seg_5210" s="T519">kuhagan</ta>
            <ta e="T521" id="Seg_5211" s="T520">da</ta>
            <ta e="T522" id="Seg_5212" s="T521">kuhagan</ta>
            <ta e="T524" id="Seg_5213" s="T523">eː</ta>
            <ta e="T525" id="Seg_5214" s="T524">ɨst-ɨ͡a-ŋ</ta>
            <ta e="T526" id="Seg_5215" s="T525">kihi</ta>
            <ta e="T527" id="Seg_5216" s="T526">da</ta>
            <ta e="T528" id="Seg_5217" s="T527">hüreg-e</ta>
            <ta e="T529" id="Seg_5218" s="T528">loksuj-u͡ok</ta>
            <ta e="T530" id="Seg_5219" s="T529">horogor</ta>
            <ta e="T531" id="Seg_5220" s="T530">bu͡ol-ar</ta>
            <ta e="T532" id="Seg_5221" s="T531">bu͡o</ta>
            <ta e="T534" id="Seg_5222" s="T533">ol</ta>
            <ta e="T535" id="Seg_5223" s="T534">ihin</ta>
            <ta e="T537" id="Seg_5224" s="T536">kɨːh</ta>
            <ta e="T538" id="Seg_5225" s="T537">ogo-koːt-tor-u-ŋ</ta>
            <ta e="T539" id="Seg_5226" s="T538">küren-el-ler</ta>
            <ta e="T540" id="Seg_5227" s="T539">bu͡o</ta>
            <ta e="T541" id="Seg_5228" s="T540">ɨstaː-m-aːrɨ-lar</ta>
            <ta e="T542" id="Seg_5229" s="T541">bar-an</ta>
            <ta e="T543" id="Seg_5230" s="T542">kaːl-al-lar</ta>
            <ta e="T545" id="Seg_5231" s="T544">kim</ta>
            <ta e="T546" id="Seg_5232" s="T545">kɨːh</ta>
            <ta e="T547" id="Seg_5233" s="T546">ogo-lor-u</ta>
            <ta e="T548" id="Seg_5234" s="T547">ügüs-tük</ta>
            <ta e="T549" id="Seg_5235" s="T548">ɨgɨr-aːččɨ-lar</ta>
            <ta e="T550" id="Seg_5236" s="T549">u͡ol</ta>
            <ta e="T551" id="Seg_5237" s="T550">ogo-toːgor</ta>
            <ta e="T552" id="Seg_5238" s="T551">togo</ta>
            <ta e="T553" id="Seg_5239" s="T552">ere</ta>
            <ta e="T555" id="Seg_5240" s="T554">de</ta>
            <ta e="T556" id="Seg_5241" s="T555">ol</ta>
            <ta e="T557" id="Seg_5242" s="T556">ol</ta>
            <ta e="T558" id="Seg_5243" s="T557">gɨn-an</ta>
            <ta e="T559" id="Seg_5244" s="T558">baran</ta>
            <ta e="T560" id="Seg_5245" s="T559">ol</ta>
            <ta e="T561" id="Seg_5246" s="T560">kim-i-nen</ta>
            <ta e="T562" id="Seg_5247" s="T561">delbi</ta>
            <ta e="T563" id="Seg_5248" s="T562">kim-ne</ta>
            <ta e="T564" id="Seg_5249" s="T563">hoŋohoːn-u-nan</ta>
            <ta e="T565" id="Seg_5250" s="T564">delbi</ta>
            <ta e="T566" id="Seg_5251" s="T565">kim-n-iː</ta>
            <ta e="T567" id="Seg_5252" s="T566">kim-neː-ti-ler</ta>
            <ta e="T568" id="Seg_5253" s="T567">bu͡o</ta>
            <ta e="T569" id="Seg_5254" s="T568">ol</ta>
            <ta e="T570" id="Seg_5255" s="T569">tiriː-gi-n</ta>
            <ta e="T572" id="Seg_5256" s="T571">ol</ta>
            <ta e="T573" id="Seg_5257" s="T572">hoŋohoːn-nuː-r-u-ŋ</ta>
            <ta e="T574" id="Seg_5258" s="T573">emi͡e</ta>
            <ta e="T575" id="Seg_5259" s="T574">ör</ta>
            <ta e="T576" id="Seg_5260" s="T575">bagajɨ</ta>
            <ta e="T577" id="Seg_5261" s="T576">bu͡ol-l-a</ta>
            <ta e="T579" id="Seg_5262" s="T578">nöŋü͡ö</ta>
            <ta e="T580" id="Seg_5263" s="T579">kün</ta>
            <ta e="T581" id="Seg_5264" s="T580">emi͡e</ta>
            <ta e="T582" id="Seg_5265" s="T581">tur-u͡ok-taːk-kɨn</ta>
            <ta e="T583" id="Seg_5266" s="T582">bu͡olla</ta>
            <ta e="T584" id="Seg_5267" s="T583">ol</ta>
            <ta e="T585" id="Seg_5268" s="T584">ɨst-ɨː</ta>
            <ta e="T586" id="Seg_5269" s="T585">ɨst-ɨː-gɨn</ta>
            <ta e="T588" id="Seg_5270" s="T587">bagar-bat</ta>
            <ta e="T589" id="Seg_5271" s="T588">bu͡ol-a-gɨn</ta>
            <ta e="T590" id="Seg_5272" s="T589">hüreg-i-ŋ</ta>
            <ta e="T591" id="Seg_5273" s="T590">loksuj-ar</ta>
            <ta e="T592" id="Seg_5274" s="T591">bu͡olla</ta>
            <ta e="T594" id="Seg_5275" s="T593">de</ta>
            <ta e="T595" id="Seg_5276" s="T594">emi͡e</ta>
            <ta e="T596" id="Seg_5277" s="T595">hin</ta>
            <ta e="T597" id="Seg_5278" s="T596">ɨst-ɨː-gɨn</ta>
            <ta e="T598" id="Seg_5279" s="T597">bu͡olla</ta>
            <ta e="T599" id="Seg_5280" s="T598">kajdi͡ek</ta>
            <ta e="T600" id="Seg_5281" s="T599">bu͡ol-u͡o-ŋ=uj</ta>
            <ta e="T601" id="Seg_5282" s="T600">inʼe-ŋ</ta>
            <ta e="T602" id="Seg_5283" s="T601">haŋa-tɨ-n</ta>
            <ta e="T603" id="Seg_5284" s="T602">ist-e-gin</ta>
            <ta e="T604" id="Seg_5285" s="T603">bu͡o</ta>
            <ta e="T606" id="Seg_5286" s="T605">de</ta>
            <ta e="T607" id="Seg_5287" s="T606">ol</ta>
            <ta e="T608" id="Seg_5288" s="T607">tur-an</ta>
            <ta e="T609" id="Seg_5289" s="T608">ɨst-ɨː</ta>
            <ta e="T610" id="Seg_5290" s="T609">ɨst-ɨː-gɨn</ta>
            <ta e="T611" id="Seg_5291" s="T610">de</ta>
            <ta e="T612" id="Seg_5292" s="T611">muŋ-nan-an</ta>
            <ta e="T613" id="Seg_5293" s="T612">muŋ-nan-an</ta>
            <ta e="T614" id="Seg_5294" s="T613">de</ta>
            <ta e="T615" id="Seg_5295" s="T614">büt-e-bit</ta>
            <ta e="T616" id="Seg_5296" s="T615">bu͡olla</ta>
            <ta e="T618" id="Seg_5297" s="T617">kah-ɨs</ta>
            <ta e="T619" id="Seg_5298" s="T618">eme</ta>
            <ta e="T620" id="Seg_5299" s="T619">kün-ü-ger</ta>
            <ta e="T621" id="Seg_5300" s="T620">de</ta>
            <ta e="T622" id="Seg_5301" s="T621">ol</ta>
            <ta e="T623" id="Seg_5302" s="T622">kɨhɨ͡ak</ta>
            <ta e="T624" id="Seg_5303" s="T623">kim-i-nen</ta>
            <ta e="T625" id="Seg_5304" s="T624">hoŋohoːn-non</ta>
            <ta e="T627" id="Seg_5305" s="T626">on-ton</ta>
            <ta e="T628" id="Seg_5306" s="T627">kojut</ta>
            <ta e="T629" id="Seg_5307" s="T628">iti</ta>
            <ta e="T630" id="Seg_5308" s="T629">harɨː</ta>
            <ta e="T631" id="Seg_5309" s="T630">bu͡ol</ta>
            <ta e="T632" id="Seg_5310" s="T631">bukatɨn</ta>
            <ta e="T633" id="Seg_5311" s="T632">harɨː</ta>
            <ta e="T634" id="Seg_5312" s="T633">bu͡ol</ta>
            <ta e="T635" id="Seg_5313" s="T634">üčügej</ta>
            <ta e="T636" id="Seg_5314" s="T635">bu͡ol-u͡o-n</ta>
            <ta e="T637" id="Seg_5315" s="T636">imiges</ta>
            <ta e="T638" id="Seg_5316" s="T637">bu͡ol-u͡o-n</ta>
            <ta e="T639" id="Seg_5317" s="T638">iti</ta>
            <ta e="T640" id="Seg_5318" s="T639">eŋin</ta>
            <ta e="T641" id="Seg_5319" s="T640">eŋin-nik</ta>
            <ta e="T642" id="Seg_5320" s="T641">iti</ta>
            <ta e="T643" id="Seg_5321" s="T642">kim-n-iː</ta>
            <ta e="T644" id="Seg_5322" s="T643">hataː-tɨ-bɨt</ta>
            <ta e="T645" id="Seg_5323" s="T644">bu͡o</ta>
            <ta e="T647" id="Seg_5324" s="T646">kɨhɨ͡ak-tan</ta>
            <ta e="T648" id="Seg_5325" s="T647">gedereː-nnen</ta>
            <ta e="T649" id="Seg_5326" s="T648">oŋor-o-but</ta>
            <ta e="T651" id="Seg_5327" s="T650">on-tu-ŋ</ta>
            <ta e="T652" id="Seg_5328" s="T651">harɨː</ta>
            <ta e="T653" id="Seg_5329" s="T652">olus</ta>
            <ta e="T654" id="Seg_5330" s="T653">maŋan</ta>
            <ta e="T655" id="Seg_5331" s="T654">bu͡ol-aːrɨ</ta>
            <ta e="T656" id="Seg_5332" s="T655">tože</ta>
            <ta e="T657" id="Seg_5333" s="T656">hin</ta>
            <ta e="T658" id="Seg_5334" s="T657">kuhagan</ta>
            <ta e="T660" id="Seg_5335" s="T659">ol</ta>
            <ta e="T661" id="Seg_5336" s="T660">ihin</ta>
            <ta e="T662" id="Seg_5337" s="T661">harɨː-gɨ-n</ta>
            <ta e="T663" id="Seg_5338" s="T662">iti</ta>
            <ta e="T664" id="Seg_5339" s="T663">kimi͡e-ke</ta>
            <ta e="T665" id="Seg_5340" s="T664">ɨhaːr-al-lar</ta>
            <ta e="T666" id="Seg_5341" s="T665">ɨhaːr-al-lar</ta>
            <ta e="T667" id="Seg_5342" s="T666">buru͡o-ga</ta>
            <ta e="T669" id="Seg_5343" s="T668">ol</ta>
            <ta e="T670" id="Seg_5344" s="T669">harɨː-gɨ-n</ta>
            <ta e="T671" id="Seg_5345" s="T670">ɨl-an</ta>
            <ta e="T672" id="Seg_5346" s="T671">bar-an-nar</ta>
            <ta e="T673" id="Seg_5347" s="T672">dʼi͡e</ta>
            <ta e="T674" id="Seg_5348" s="T673">ih-i-ger</ta>
            <ta e="T675" id="Seg_5349" s="T674">ɨjɨː-l-lar</ta>
            <ta e="T677" id="Seg_5350" s="T676">dʼi͡e</ta>
            <ta e="T678" id="Seg_5351" s="T677">da</ta>
            <ta e="T679" id="Seg_5352" s="T678">bu͡ol-u͡o=j</ta>
            <ta e="T680" id="Seg_5353" s="T679">iti</ta>
            <ta e="T681" id="Seg_5354" s="T680">kim</ta>
            <ta e="T682" id="Seg_5355" s="T681">uraha</ta>
            <ta e="T683" id="Seg_5356" s="T682">ih-i-ger</ta>
            <ta e="T684" id="Seg_5357" s="T683">uraha</ta>
            <ta e="T685" id="Seg_5358" s="T684">dʼi͡e</ta>
            <ta e="T686" id="Seg_5359" s="T685">ih-i-ger</ta>
            <ta e="T687" id="Seg_5360" s="T686">ɨjɨː-l-lar</ta>
            <ta e="T689" id="Seg_5361" s="T688">onno</ta>
            <ta e="T690" id="Seg_5362" s="T689">buru͡o-ga</ta>
            <ta e="T691" id="Seg_5363" s="T690">on-tu-ŋ</ta>
            <ta e="T692" id="Seg_5364" s="T691">ɨksa</ta>
            <ta e="T693" id="Seg_5365" s="T692">tur-ar</ta>
            <ta e="T694" id="Seg_5366" s="T693">bu͡o</ta>
            <ta e="T696" id="Seg_5367" s="T695">a</ta>
            <ta e="T697" id="Seg_5368" s="T696">horok</ta>
            <ta e="T698" id="Seg_5369" s="T697">dʼaktat-tar</ta>
            <ta e="T699" id="Seg_5370" s="T698">innʼe</ta>
            <ta e="T700" id="Seg_5371" s="T699">gɨn-aːččɨ-lar</ta>
            <ta e="T701" id="Seg_5372" s="T700">bu͡olla</ta>
            <ta e="T703" id="Seg_5373" s="T702">ol</ta>
            <ta e="T704" id="Seg_5374" s="T703">kördük</ta>
            <ta e="T705" id="Seg_5375" s="T704">da</ta>
            <ta e="T706" id="Seg_5376" s="T705">ɨhɨ͡ar-a</ta>
            <ta e="T707" id="Seg_5377" s="T706">hɨt-ɨ͡ak</ta>
            <ta e="T708" id="Seg_5378" s="T707">e-bit-ter</ta>
            <ta e="T709" id="Seg_5379" s="T708">otto</ta>
            <ta e="T711" id="Seg_5380" s="T710">a</ta>
            <ta e="T712" id="Seg_5381" s="T711">gini</ta>
            <ta e="T713" id="Seg_5382" s="T712">giniler-i-ŋ</ta>
            <ta e="T714" id="Seg_5383" s="T713">innʼekeːt</ta>
            <ta e="T715" id="Seg_5384" s="T714">diː-l-ler</ta>
            <ta e="T716" id="Seg_5385" s="T715">bu͡o</ta>
            <ta e="T717" id="Seg_5386" s="T716">bihigi-ni</ta>
            <ta e="T718" id="Seg_5387" s="T717">tur-u͡or-al-lar</ta>
            <ta e="T719" id="Seg_5388" s="T718">bu͡o</ta>
            <ta e="T720" id="Seg_5389" s="T719">üs</ta>
            <ta e="T721" id="Seg_5390" s="T720">tü͡ört</ta>
            <ta e="T722" id="Seg_5391" s="T721">kɨːs</ta>
            <ta e="T723" id="Seg_5392" s="T722">kɨːh</ta>
            <ta e="T724" id="Seg_5393" s="T723">ogo-lor</ta>
            <ta e="T868" id="Seg_5394" s="T724">kim</ta>
            <ta e="T869" id="Seg_5395" s="T868">kim</ta>
            <ta e="T870" id="Seg_5396" s="T869">u͡ot-a</ta>
            <ta e="T871" id="Seg_5397" s="T870">kim</ta>
            <ta e="T872" id="Seg_5398" s="T871">u͡ot-a</ta>
            <ta e="T873" id="Seg_5399" s="T872">iti</ta>
            <ta e="T874" id="Seg_5400" s="T873">ačaːg</ta>
            <ta e="T725" id="Seg_5401" s="T874">oŋor-ol-lor</ta>
            <ta e="T726" id="Seg_5402" s="T725">mas</ta>
            <ta e="T727" id="Seg_5403" s="T726">ubaj-ar</ta>
            <ta e="T728" id="Seg_5404" s="T727">on-tu-ŋ</ta>
            <ta e="T729" id="Seg_5405" s="T728">ürdü-tü-nen</ta>
            <ta e="T730" id="Seg_5406" s="T729">ot-tor-u</ta>
            <ta e="T731" id="Seg_5407" s="T730">bɨrag-al-lar</ta>
            <ta e="T732" id="Seg_5408" s="T731">ulakan</ta>
            <ta e="T733" id="Seg_5409" s="T732">buru͡o</ta>
            <ta e="T734" id="Seg_5410" s="T733">kel-i͡e-n</ta>
            <ta e="T736" id="Seg_5411" s="T735">de</ta>
            <ta e="T737" id="Seg_5412" s="T736">ol</ta>
            <ta e="T738" id="Seg_5413" s="T737">buru͡o</ta>
            <ta e="T739" id="Seg_5414" s="T738">ih-i-ger</ta>
            <ta e="T740" id="Seg_5415" s="T739">tögürüččü</ta>
            <ta e="T741" id="Seg_5416" s="T740">tur-am-mɨt</ta>
            <ta e="T742" id="Seg_5417" s="T741">ol</ta>
            <ta e="T743" id="Seg_5418" s="T742">tut-a-bɨt</ta>
            <ta e="T744" id="Seg_5419" s="T743">bu͡olla</ta>
            <ta e="T745" id="Seg_5420" s="T744">kim-i</ta>
            <ta e="T747" id="Seg_5421" s="T746">ol</ta>
            <ta e="T748" id="Seg_5422" s="T747">gɨn-an</ta>
            <ta e="T749" id="Seg_5423" s="T748">baran</ta>
            <ta e="T750" id="Seg_5424" s="T749">ergi-t-e-bit</ta>
            <ta e="T751" id="Seg_5425" s="T750">kün</ta>
            <ta e="T752" id="Seg_5426" s="T751">hu͡ol-u-n</ta>
            <ta e="T753" id="Seg_5427" s="T752">kördük</ta>
            <ta e="T754" id="Seg_5428" s="T753">ergi-t-e-bit</ta>
            <ta e="T756" id="Seg_5429" s="T755">üčügej-dik</ta>
            <ta e="T757" id="Seg_5430" s="T756">tut-u͡ok-taːk-kɨn</ta>
            <ta e="T758" id="Seg_5431" s="T757">on-tu-gu-n</ta>
            <ta e="T759" id="Seg_5432" s="T758">ɨːp-pat</ta>
            <ta e="T760" id="Seg_5433" s="T759">kördük</ta>
            <ta e="T762" id="Seg_5434" s="T761">beje</ta>
            <ta e="T763" id="Seg_5435" s="T762">beje-biti-ger</ta>
            <ta e="T764" id="Seg_5436" s="T763">ergi-t-e-bit</ta>
            <ta e="T765" id="Seg_5437" s="T764">bu͡ol</ta>
            <ta e="T766" id="Seg_5438" s="T765">innʼekeːt</ta>
            <ta e="T768" id="Seg_5439" s="T767">min</ta>
            <ta e="T769" id="Seg_5440" s="T768">dogor-bo-r</ta>
            <ta e="T770" id="Seg_5441" s="T769">bi͡er-i͡e-m</ta>
            <ta e="T771" id="Seg_5442" s="T770">dogor-u-m</ta>
            <ta e="T772" id="Seg_5443" s="T771">nöŋü͡ö</ta>
            <ta e="T773" id="Seg_5444" s="T772">dogor-go-r</ta>
            <ta e="T774" id="Seg_5445" s="T773">itirdik-keːn</ta>
            <ta e="T775" id="Seg_5446" s="T774">bi͡er-er</ta>
            <ta e="T777" id="Seg_5447" s="T776">tögürü-t-e-bit</ta>
            <ta e="T778" id="Seg_5448" s="T777">ol</ta>
            <ta e="T779" id="Seg_5449" s="T778">tiriː-ni</ta>
            <ta e="T781" id="Seg_5450" s="T780">annɨ-gɨtɨ-ttan</ta>
            <ta e="T782" id="Seg_5451" s="T781">ulakan</ta>
            <ta e="T783" id="Seg_5452" s="T782">buru͡o</ta>
            <ta e="T784" id="Seg_5453" s="T783">kel-er</ta>
            <ta e="T785" id="Seg_5454" s="T784">bu͡o</ta>
            <ta e="T787" id="Seg_5455" s="T786">bihi͡e-ke</ta>
            <ta e="T788" id="Seg_5456" s="T787">emi͡e</ta>
            <ta e="T789" id="Seg_5457" s="T788">kuhagan</ta>
            <ta e="T790" id="Seg_5458" s="T789">ol</ta>
            <ta e="T791" id="Seg_5459" s="T790">buru͡o</ta>
            <ta e="T792" id="Seg_5460" s="T791">kel-er-e</ta>
            <ta e="T794" id="Seg_5461" s="T793">karak-pɨt</ta>
            <ta e="T795" id="Seg_5462" s="T794">ahɨj-ar</ta>
            <ta e="T796" id="Seg_5463" s="T795">bu͡olla</ta>
            <ta e="T797" id="Seg_5464" s="T796">ol</ta>
            <ta e="T798" id="Seg_5465" s="T797">buru͡o-ttan</ta>
            <ta e="T800" id="Seg_5466" s="T799">anʼak-pɨtɨ-gar</ta>
            <ta e="T801" id="Seg_5467" s="T800">da</ta>
            <ta e="T802" id="Seg_5468" s="T801">ɨl-a-bɨt</ta>
            <ta e="T803" id="Seg_5469" s="T802">bu͡olla</ta>
            <ta e="T804" id="Seg_5470" s="T803">buru͡o-nu</ta>
            <ta e="T806" id="Seg_5471" s="T805">de</ta>
            <ta e="T807" id="Seg_5472" s="T806">ol</ta>
            <ta e="T808" id="Seg_5473" s="T807">da</ta>
            <ta e="T809" id="Seg_5474" s="T808">bu͡ol-lar</ta>
            <ta e="T810" id="Seg_5475" s="T809">de</ta>
            <ta e="T811" id="Seg_5476" s="T810">tur-u-n-aːkt-ɨː-bɨt</ta>
            <ta e="T812" id="Seg_5477" s="T811">bu͡o</ta>
            <ta e="T813" id="Seg_5478" s="T812">tur-u-ŋ</ta>
            <ta e="T814" id="Seg_5479" s="T813">tur-u-ŋ</ta>
            <ta e="T815" id="Seg_5480" s="T814">diː-l-ler</ta>
            <ta e="T816" id="Seg_5481" s="T815">bu͡o</ta>
            <ta e="T817" id="Seg_5482" s="T816">bihigi-ni</ta>
            <ta e="T819" id="Seg_5483" s="T818">de</ta>
            <ta e="T820" id="Seg_5484" s="T819">ol</ta>
            <ta e="T821" id="Seg_5485" s="T820">kördük</ta>
            <ta e="T822" id="Seg_5486" s="T821">ergi-t-e-bit</ta>
            <ta e="T824" id="Seg_5487" s="T823">on-ton</ta>
            <ta e="T825" id="Seg_5488" s="T824">biːr</ta>
            <ta e="T826" id="Seg_5489" s="T825">emete</ta>
            <ta e="T827" id="Seg_5490" s="T826">kɨːh</ta>
            <ta e="T828" id="Seg_5491" s="T827">ogo-to</ta>
            <ta e="T829" id="Seg_5492" s="T828">tiriː-ni</ta>
            <ta e="T830" id="Seg_5493" s="T829">iliː-ti-nen</ta>
            <ta e="T831" id="Seg_5494" s="T830">innʼekeːt-i-ŋ</ta>
            <ta e="T832" id="Seg_5495" s="T831">ɨl-an</ta>
            <ta e="T833" id="Seg_5496" s="T832">keːh-i͡e</ta>
            <ta e="T834" id="Seg_5497" s="T833">hɨːha</ta>
            <ta e="T835" id="Seg_5498" s="T834">tut-u͡o</ta>
            <ta e="T836" id="Seg_5499" s="T835">on-tu-tu-n</ta>
            <ta e="T837" id="Seg_5500" s="T836">ol</ta>
            <ta e="T838" id="Seg_5501" s="T837">tut-ar</ta>
            <ta e="T839" id="Seg_5502" s="T838">hir-i-n</ta>
            <ta e="T840" id="Seg_5503" s="T839">tiriː-ti-n</ta>
            <ta e="T841" id="Seg_5504" s="T840">de</ta>
            <ta e="T842" id="Seg_5505" s="T841">oččogo</ta>
            <ta e="T843" id="Seg_5506" s="T842">diː-l-ler</ta>
            <ta e="T844" id="Seg_5507" s="T843">bu͡o</ta>
            <ta e="T845" id="Seg_5508" s="T844">de</ta>
            <ta e="T846" id="Seg_5509" s="T845">er-ge</ta>
            <ta e="T847" id="Seg_5510" s="T846">bar-ɨ͡a-ŋ</ta>
            <ta e="T848" id="Seg_5511" s="T847">hu͡og-a</ta>
            <ta e="T849" id="Seg_5512" s="T848">ulaːt-tak-kɨna</ta>
            <ta e="T851" id="Seg_5513" s="T850">d-iː</ta>
            <ta e="T852" id="Seg_5514" s="T851">diː-l-ler</ta>
            <ta e="T853" id="Seg_5515" s="T852">itigirdik-keːn</ta>
            <ta e="T854" id="Seg_5516" s="T853">haŋar-al-lar</ta>
            <ta e="T855" id="Seg_5517" s="T854">bu͡o</ta>
            <ta e="T856" id="Seg_5518" s="T855">bihigi-ni</ta>
            <ta e="T858" id="Seg_5519" s="T857">ol</ta>
            <ta e="T859" id="Seg_5520" s="T858">ihin</ta>
            <ta e="T860" id="Seg_5521" s="T859">ol</ta>
            <ta e="T861" id="Seg_5522" s="T860">muŋ-nan-a</ta>
            <ta e="T862" id="Seg_5523" s="T861">tur-a-bɨt</ta>
            <ta e="T863" id="Seg_5524" s="T862">bu͡o</ta>
            <ta e="T864" id="Seg_5525" s="T863">ol</ta>
            <ta e="T865" id="Seg_5526" s="T864">tiriː-ni</ta>
            <ta e="T866" id="Seg_5527" s="T865">ɨːp-pakka</ta>
            <ta e="T867" id="Seg_5528" s="T866">ɨntak</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T2" id="Seg_5529" s="T1">kös-A</ta>
            <ta e="T4" id="Seg_5530" s="T3">kɨhɨn-nI</ta>
            <ta e="T5" id="Seg_5531" s="T4">kɨhɨn-nI</ta>
            <ta e="T6" id="Seg_5532" s="T5">meldʼi</ta>
            <ta e="T7" id="Seg_5533" s="T6">kös-A</ta>
            <ta e="T8" id="Seg_5534" s="T7">hɨrɨt-An</ta>
            <ta e="T9" id="Seg_5535" s="T8">bar-An-LAr</ta>
            <ta e="T10" id="Seg_5536" s="T9">haka-LAr</ta>
            <ta e="T11" id="Seg_5537" s="T10">haːs</ta>
            <ta e="T12" id="Seg_5538" s="T11">toktoː-Ar</ta>
            <ta e="T13" id="Seg_5539" s="T12">hir-LArA</ta>
            <ta e="T14" id="Seg_5540" s="T13">baːr</ta>
            <ta e="T15" id="Seg_5541" s="T14">bu͡ol-AːččI</ta>
            <ta e="T17" id="Seg_5542" s="T16">onno</ta>
            <ta e="T18" id="Seg_5543" s="T17">ügüs</ta>
            <ta e="T19" id="Seg_5544" s="T18">bagajɨ</ta>
            <ta e="T20" id="Seg_5545" s="T19">uraha</ta>
            <ta e="T21" id="Seg_5546" s="T20">dʼi͡e-LAr</ta>
            <ta e="T22" id="Seg_5547" s="T21">hɨrga</ta>
            <ta e="T23" id="Seg_5548" s="T22">dʼi͡e-LAr</ta>
            <ta e="T24" id="Seg_5549" s="T23">tur-AːččI-LAr</ta>
            <ta e="T26" id="Seg_5550" s="T25">haːs-GI</ta>
            <ta e="T27" id="Seg_5551" s="T26">hɨlaːs</ta>
            <ta e="T28" id="Seg_5552" s="T27">kün-LAr-tA</ta>
            <ta e="T29" id="Seg_5553" s="T28">kün</ta>
            <ta e="T30" id="Seg_5554" s="T29">tɨk-Ar</ta>
            <ta e="T31" id="Seg_5555" s="T30">bu͡ol-TAK-InA</ta>
            <ta e="T32" id="Seg_5556" s="T31">dʼe</ta>
            <ta e="T33" id="Seg_5557" s="T32">dʼaktar-LAr</ta>
            <ta e="T34" id="Seg_5558" s="T33">üleleː-Ar-LArA</ta>
            <ta e="T35" id="Seg_5559" s="T34">ükseː-Ar</ta>
            <ta e="T37" id="Seg_5560" s="T36">tahaːra</ta>
            <ta e="T38" id="Seg_5561" s="T37">giniler-I-ŋ</ta>
            <ta e="T39" id="Seg_5562" s="T38">maŋnaj</ta>
            <ta e="T40" id="Seg_5563" s="T39">iti</ta>
            <ta e="T41" id="Seg_5564" s="T40">tiriː</ta>
            <ta e="T42" id="Seg_5565" s="T41">üle-tI-n</ta>
            <ta e="T43" id="Seg_5566" s="T42">üleleː-Ar-LAr</ta>
            <ta e="T45" id="Seg_5567" s="T44">tiriː</ta>
            <ta e="T46" id="Seg_5568" s="T45">tiriː-GI-n</ta>
            <ta e="T47" id="Seg_5569" s="T46">bert</ta>
            <ta e="T48" id="Seg_5570" s="T47">delbi</ta>
            <ta e="T49" id="Seg_5571" s="T48">ɨraːstaː-Ar-LAr</ta>
            <ta e="T50" id="Seg_5572" s="T49">maŋnaj</ta>
            <ta e="T52" id="Seg_5573" s="T51">ɨraːstaː-An</ta>
            <ta e="T53" id="Seg_5574" s="T52">bar-An-LAr</ta>
            <ta e="T54" id="Seg_5575" s="T53">kɨptɨj-I-nAn</ta>
            <ta e="T55" id="Seg_5576" s="T54">kɨrɨj-Ar-LAr</ta>
            <ta e="T57" id="Seg_5577" s="T56">tüː-tI-n</ta>
            <ta e="T59" id="Seg_5578" s="T58">ol</ta>
            <ta e="T60" id="Seg_5579" s="T59">kɨptɨj-I-nAn</ta>
            <ta e="T61" id="Seg_5580" s="T60">kɨrɨj-An</ta>
            <ta e="T62" id="Seg_5581" s="T61">büt-TAK-TArInA</ta>
            <ta e="T63" id="Seg_5582" s="T62">ol-ttAn</ta>
            <ta e="T64" id="Seg_5583" s="T63">kojut</ta>
            <ta e="T65" id="Seg_5584" s="T64">bahak-tI-nAn</ta>
            <ta e="T66" id="Seg_5585" s="T65">emi͡e</ta>
            <ta e="T67" id="Seg_5586" s="T66">kɨrɨj-Ar-LAr</ta>
            <ta e="T69" id="Seg_5587" s="T68">öldüːn</ta>
            <ta e="T70" id="Seg_5588" s="T69">haka-LAr</ta>
            <ta e="T71" id="Seg_5589" s="T70">olus</ta>
            <ta e="T72" id="Seg_5590" s="T71">köp</ta>
            <ta e="T73" id="Seg_5591" s="T72">kim-nI</ta>
            <ta e="T74" id="Seg_5592" s="T73">tiriː-nI</ta>
            <ta e="T75" id="Seg_5593" s="T74">ɨl-AːččI-tA</ta>
            <ta e="T76" id="Seg_5594" s="T75">hu͡ok-LAr</ta>
            <ta e="T78" id="Seg_5595" s="T77">kɨrɨj-LIN-I-BIT</ta>
            <ta e="T79" id="Seg_5596" s="T78">iti</ta>
            <ta e="T80" id="Seg_5597" s="T79">tüː-tI-n</ta>
            <ta e="T81" id="Seg_5598" s="T80">ɨl-I-LIN-I-BIT-ttAn</ta>
            <ta e="T83" id="Seg_5599" s="T82">kɨrpalaː-BIT</ta>
            <ta e="T84" id="Seg_5600" s="T83">tüː-nI</ta>
            <ta e="T85" id="Seg_5601" s="T84">kim-nI</ta>
            <ta e="T86" id="Seg_5602" s="T85">tiriː-nI</ta>
            <ta e="T87" id="Seg_5603" s="T86">ɨl-An-LAr</ta>
            <ta e="T88" id="Seg_5604" s="T87">kim</ta>
            <ta e="T89" id="Seg_5605" s="T88">dʼi͡e</ta>
            <ta e="T90" id="Seg_5606" s="T89">habɨː-tA</ta>
            <ta e="T91" id="Seg_5607" s="T90">iti</ta>
            <ta e="T92" id="Seg_5608" s="T91">öldüːn-LAr-nI</ta>
            <ta e="T93" id="Seg_5609" s="T92">tik-AːččI-LAr</ta>
            <ta e="T95" id="Seg_5610" s="T94">onnuk</ta>
            <ta e="T96" id="Seg_5611" s="T95">bu͡ollagɨna</ta>
            <ta e="T97" id="Seg_5612" s="T96">dʼe</ta>
            <ta e="T98" id="Seg_5613" s="T97">ol</ta>
            <ta e="T99" id="Seg_5614" s="T98">barbak</ta>
            <ta e="T100" id="Seg_5615" s="T99">tüː-nI</ta>
            <ta e="T101" id="Seg_5616" s="T100">keːs-Ar-LAr</ta>
            <ta e="T102" id="Seg_5617" s="T101">bu͡o</ta>
            <ta e="T104" id="Seg_5618" s="T103">ol-ttAn</ta>
            <ta e="T105" id="Seg_5619" s="T104">ol-tI-GI-nAn</ta>
            <ta e="T106" id="Seg_5620" s="T105">öldüːn</ta>
            <ta e="T107" id="Seg_5621" s="T106">tik-Ar-LAr</ta>
            <ta e="T109" id="Seg_5622" s="T108">ol</ta>
            <ta e="T111" id="Seg_5623" s="T110">horok</ta>
            <ta e="T112" id="Seg_5624" s="T111">tiriː-nI</ta>
            <ta e="T113" id="Seg_5625" s="T112">giniler-I-ŋ</ta>
            <ta e="T114" id="Seg_5626" s="T113">iti</ta>
            <ta e="T115" id="Seg_5627" s="T114">harɨː</ta>
            <ta e="T116" id="Seg_5628" s="T115">oŋor-IAK-LAːK-LAr</ta>
            <ta e="T118" id="Seg_5629" s="T117">harɨː</ta>
            <ta e="T119" id="Seg_5630" s="T118">oŋor-Ar-LArI-GAr</ta>
            <ta e="T120" id="Seg_5631" s="T119">itinne</ta>
            <ta e="T121" id="Seg_5632" s="T120">dʼe</ta>
            <ta e="T122" id="Seg_5633" s="T121">hin</ta>
            <ta e="T123" id="Seg_5634" s="T122">ügüs</ta>
            <ta e="T124" id="Seg_5635" s="T123">üle</ta>
            <ta e="T126" id="Seg_5636" s="T125">kɨrpalaː-Ar-LAr</ta>
            <ta e="T127" id="Seg_5637" s="T126">bu͡o</ta>
            <ta e="T128" id="Seg_5638" s="T127">iti</ta>
            <ta e="T129" id="Seg_5639" s="T128">kɨrpalaː-BIT-LArI-n</ta>
            <ta e="T130" id="Seg_5640" s="T129">kördük</ta>
            <ta e="T131" id="Seg_5641" s="T130">tüː-tA</ta>
            <ta e="T132" id="Seg_5642" s="T131">hu͡ok</ta>
            <ta e="T133" id="Seg_5643" s="T132">bu͡o</ta>
            <ta e="T134" id="Seg_5644" s="T133">tüː-tA</ta>
            <ta e="T135" id="Seg_5645" s="T134">agɨjak</ta>
            <ta e="T136" id="Seg_5646" s="T135">bu͡ol-IAK.[tI]-n</ta>
            <ta e="T137" id="Seg_5647" s="T136">hin</ta>
            <ta e="T138" id="Seg_5648" s="T137">tüː-tA</ta>
            <ta e="T139" id="Seg_5649" s="T138">kaːl-Ar</ta>
            <ta e="T140" id="Seg_5650" s="T139">bu͡o</ta>
            <ta e="T141" id="Seg_5651" s="T140">tiriː-GA-r</ta>
            <ta e="T143" id="Seg_5652" s="T142">ol</ta>
            <ta e="T144" id="Seg_5653" s="T143">tüː</ta>
            <ta e="T145" id="Seg_5654" s="T144">tüː-ŋ</ta>
            <ta e="T146" id="Seg_5655" s="T145">üčügej-LIk</ta>
            <ta e="T147" id="Seg_5656" s="T146">barɨta</ta>
            <ta e="T148" id="Seg_5657" s="T147">tüs-IAK.[tI]-n</ta>
            <ta e="T149" id="Seg_5658" s="T148">ol-ttAn</ta>
            <ta e="T150" id="Seg_5659" s="T149">kojut</ta>
            <ta e="T151" id="Seg_5660" s="T150">imit-Ar-GA</ta>
            <ta e="T152" id="Seg_5661" s="T151">da</ta>
            <ta e="T153" id="Seg_5662" s="T152">üčügej</ta>
            <ta e="T154" id="Seg_5663" s="T153">bu͡o</ta>
            <ta e="T155" id="Seg_5664" s="T154">harɨː</ta>
            <ta e="T156" id="Seg_5665" s="T155">bu͡ol-IAK.[tI]-n</ta>
            <ta e="T157" id="Seg_5666" s="T156">ol</ta>
            <ta e="T158" id="Seg_5667" s="T157">tüː-LAːK</ta>
            <ta e="T159" id="Seg_5668" s="T158">kim</ta>
            <ta e="T160" id="Seg_5669" s="T159">tiriː-nI</ta>
            <ta e="T161" id="Seg_5670" s="T160">giniler-I-ŋ</ta>
            <ta e="T162" id="Seg_5671" s="T161">hɨt-I-t-Ar-LAr</ta>
            <ta e="T163" id="Seg_5672" s="T162">uː-GA</ta>
            <ta e="T165" id="Seg_5673" s="T164">uː</ta>
            <ta e="T166" id="Seg_5674" s="T165">is-tI-GAr</ta>
            <ta e="T167" id="Seg_5675" s="T166">mɨla-nI</ta>
            <ta e="T168" id="Seg_5676" s="T167">kut-Ar-LAr</ta>
            <ta e="T169" id="Seg_5677" s="T168">uːr-Ar-LAr</ta>
            <ta e="T171" id="Seg_5678" s="T170">ol-ttAn</ta>
            <ta e="T172" id="Seg_5679" s="T171">eŋin-eŋin</ta>
            <ta e="T173" id="Seg_5680" s="T172">ot-LAr-nI</ta>
            <ta e="T175" id="Seg_5681" s="T174">ol</ta>
            <ta e="T176" id="Seg_5682" s="T175">gɨn-A</ta>
            <ta e="T177" id="Seg_5683" s="T176">gɨn-A</ta>
            <ta e="T178" id="Seg_5684" s="T177">kas</ta>
            <ta e="T179" id="Seg_5685" s="T178">da</ta>
            <ta e="T180" id="Seg_5686" s="T179">kün-nI</ta>
            <ta e="T181" id="Seg_5687" s="T180">hɨt-IAK-LAːK</ta>
            <ta e="T183" id="Seg_5688" s="T182">itiː</ta>
            <ta e="T184" id="Seg_5689" s="T183">da</ta>
            <ta e="T185" id="Seg_5690" s="T184">kün</ta>
            <ta e="T186" id="Seg_5691" s="T185">bu͡ol-TIn</ta>
            <ta e="T187" id="Seg_5692" s="T186">ol</ta>
            <ta e="T188" id="Seg_5693" s="T187">uː-ŋ</ta>
            <ta e="T189" id="Seg_5694" s="T188">hɨtɨj-Ar</ta>
            <ta e="T190" id="Seg_5695" s="T189">bu͡o</ta>
            <ta e="T191" id="Seg_5696" s="T190">tiriː-ŋ</ta>
            <ta e="T192" id="Seg_5697" s="T191">emi͡e</ta>
            <ta e="T193" id="Seg_5698" s="T192">hɨtɨj-Ar</ta>
            <ta e="T194" id="Seg_5699" s="T193">bu͡o</ta>
            <ta e="T195" id="Seg_5700" s="T194">hɨmnagas</ta>
            <ta e="T196" id="Seg_5701" s="T195">bu͡ol-Ar</ta>
            <ta e="T197" id="Seg_5702" s="T196">bu͡o</ta>
            <ta e="T199" id="Seg_5703" s="T198">tiriː-ŋ</ta>
            <ta e="T200" id="Seg_5704" s="T199">kas</ta>
            <ta e="T201" id="Seg_5705" s="T200">da</ta>
            <ta e="T202" id="Seg_5706" s="T201">kün-nI</ta>
            <ta e="T203" id="Seg_5707" s="T202">hɨtɨj-An</ta>
            <ta e="T204" id="Seg_5708" s="T203">büt-TAK-InA</ta>
            <ta e="T205" id="Seg_5709" s="T204">giniler-I-ŋ</ta>
            <ta e="T206" id="Seg_5710" s="T205">ol</ta>
            <ta e="T207" id="Seg_5711" s="T206">bulkuj-Ar-LAr</ta>
            <ta e="T209" id="Seg_5712" s="T208">ol</ta>
            <ta e="T210" id="Seg_5713" s="T209">tiriː-GI-n</ta>
            <ta e="T211" id="Seg_5714" s="T210">ɨl-An</ta>
            <ta e="T212" id="Seg_5715" s="T211">bar-An-LAr</ta>
            <ta e="T213" id="Seg_5716" s="T212">iti</ta>
            <ta e="T214" id="Seg_5717" s="T213">kim</ta>
            <ta e="T215" id="Seg_5718" s="T214">huːn-IAr-Ar-LAr</ta>
            <ta e="T216" id="Seg_5719" s="T215">ürek-GA</ta>
            <ta e="T217" id="Seg_5720" s="T216">bar-An-LAr</ta>
            <ta e="T219" id="Seg_5721" s="T218">delbi</ta>
            <ta e="T220" id="Seg_5722" s="T219">huːn-TAr-Ar-TI-LAr</ta>
            <ta e="T221" id="Seg_5723" s="T220">ol</ta>
            <ta e="T222" id="Seg_5724" s="T221">gɨn-An</ta>
            <ta e="T223" id="Seg_5725" s="T222">baran</ta>
            <ta e="T224" id="Seg_5726" s="T223">kuːr-t-Ar-LAr</ta>
            <ta e="T226" id="Seg_5727" s="T225">kuːr-t-An</ta>
            <ta e="T227" id="Seg_5728" s="T226">bar-An-LArA</ta>
            <ta e="T228" id="Seg_5729" s="T227">iti</ta>
            <ta e="T229" id="Seg_5730" s="T228">tiriː-GI-n</ta>
            <ta e="T230" id="Seg_5731" s="T229">kim-LAː-Ar-LAr</ta>
            <ta e="T231" id="Seg_5732" s="T230">gedereː-I-nAn</ta>
            <ta e="T232" id="Seg_5733" s="T231">ol</ta>
            <ta e="T233" id="Seg_5734" s="T232">kim</ta>
            <ta e="T234" id="Seg_5735" s="T233">kaːl-BIT</ta>
            <ta e="T235" id="Seg_5736" s="T234">tüː-LArI-n</ta>
            <ta e="T236" id="Seg_5737" s="T235">ɨl-Ar-LAr</ta>
            <ta e="T238" id="Seg_5738" s="T237">gedereː-LAː-Ar-LAr</ta>
            <ta e="T239" id="Seg_5739" s="T238">haka-LIː</ta>
            <ta e="T240" id="Seg_5740" s="T239">haŋar-TAK-GA</ta>
            <ta e="T242" id="Seg_5741" s="T241">ol-tI-ŋ</ta>
            <ta e="T243" id="Seg_5742" s="T242">emi͡e</ta>
            <ta e="T244" id="Seg_5743" s="T243">türgen</ta>
            <ta e="T245" id="Seg_5744" s="T244">da</ta>
            <ta e="T246" id="Seg_5745" s="T245">bu͡ol-BAtAK</ta>
            <ta e="T247" id="Seg_5746" s="T246">dʼogus</ta>
            <ta e="T248" id="Seg_5747" s="T247">da</ta>
            <ta e="T249" id="Seg_5748" s="T248">bu͡ol-BAtAK</ta>
            <ta e="T250" id="Seg_5749" s="T249">dʼaktar-LAr</ta>
            <ta e="T251" id="Seg_5750" s="T250">bert</ta>
            <ta e="T252" id="Seg_5751" s="T251">kim</ta>
            <ta e="T253" id="Seg_5752" s="T252">ɨ͡arakan</ta>
            <ta e="T254" id="Seg_5753" s="T253">üle</ta>
            <ta e="T255" id="Seg_5754" s="T254">iti</ta>
            <ta e="T256" id="Seg_5755" s="T255">dʼaktar-GA</ta>
            <ta e="T257" id="Seg_5756" s="T256">da</ta>
            <ta e="T259" id="Seg_5757" s="T258">ol</ta>
            <ta e="T260" id="Seg_5758" s="T259">ihin</ta>
            <ta e="T261" id="Seg_5759" s="T260">biːrde</ta>
            <ta e="T262" id="Seg_5760" s="T261">olor-AːT</ta>
            <ta e="T263" id="Seg_5761" s="T262">ol-nI</ta>
            <ta e="T264" id="Seg_5762" s="T263">barɨ-tI-n</ta>
            <ta e="T267" id="Seg_5763" s="T266">üleleː-IAK-LArA</ta>
            <ta e="T268" id="Seg_5764" s="T267">hu͡ok-tA</ta>
            <ta e="T269" id="Seg_5765" s="T268">ɨ͡arakan</ta>
            <ta e="T270" id="Seg_5766" s="T269">üle</ta>
            <ta e="T272" id="Seg_5767" s="T271">ol-ttAn</ta>
            <ta e="T273" id="Seg_5768" s="T272">nöŋü͡ö</ta>
            <ta e="T274" id="Seg_5769" s="T273">kün</ta>
            <ta e="T275" id="Seg_5770" s="T274">emi͡e</ta>
            <ta e="T276" id="Seg_5771" s="T275">oŋor-IAK-LArA</ta>
            <ta e="T277" id="Seg_5772" s="T276">ol</ta>
            <ta e="T278" id="Seg_5773" s="T277">gedereː-LAː-IAK-LArA</ta>
            <ta e="T279" id="Seg_5774" s="T278">emi͡e</ta>
            <ta e="T281" id="Seg_5775" s="T280">gedereː-LAː-An</ta>
            <ta e="T282" id="Seg_5776" s="T281">büt-TAK-TArInA</ta>
            <ta e="T283" id="Seg_5777" s="T282">ol</ta>
            <ta e="T284" id="Seg_5778" s="T283">harɨː-ŋ</ta>
            <ta e="T285" id="Seg_5779" s="T284">kim</ta>
            <ta e="T286" id="Seg_5780" s="T285">kim-tA</ta>
            <ta e="T287" id="Seg_5781" s="T286">hu͡ok</ta>
            <ta e="T288" id="Seg_5782" s="T287">bu͡ol-Ar</ta>
            <ta e="T289" id="Seg_5783" s="T288">bu͡o</ta>
            <ta e="T290" id="Seg_5784" s="T289">tüː-tA</ta>
            <ta e="T291" id="Seg_5785" s="T290">hu͡ok</ta>
            <ta e="T292" id="Seg_5786" s="T291">bu͡ol-Ar</ta>
            <ta e="T294" id="Seg_5787" s="T293">ol-tI-ŋ</ta>
            <ta e="T295" id="Seg_5788" s="T294">deksi</ta>
            <ta e="T296" id="Seg_5789" s="T295">bu͡ol-IAK.[tI]-n</ta>
            <ta e="T297" id="Seg_5790" s="T296">iti</ta>
            <ta e="T298" id="Seg_5791" s="T297">kɨhɨ͡ak-I-nAn</ta>
            <ta e="T299" id="Seg_5792" s="T298">tu͡ok-I-nAn</ta>
            <ta e="T300" id="Seg_5793" s="T299">eŋin</ta>
            <ta e="T301" id="Seg_5794" s="T300">eŋin-I-nAn</ta>
            <ta e="T302" id="Seg_5795" s="T301">tarbaː-Ar-LAr</ta>
            <ta e="T303" id="Seg_5796" s="T302">kɨhɨ͡ak-LAː-Ar-LAr</ta>
            <ta e="T304" id="Seg_5797" s="T303">kanʼaː-Ar-LAr</ta>
            <ta e="T306" id="Seg_5798" s="T305">bahak-I-nAn</ta>
            <ta e="T307" id="Seg_5799" s="T306">da</ta>
            <ta e="T308" id="Seg_5800" s="T307">eŋin</ta>
            <ta e="T309" id="Seg_5801" s="T308">eŋin</ta>
            <ta e="T310" id="Seg_5802" s="T309">deksi-LAː-Ar-LAr</ta>
            <ta e="T311" id="Seg_5803" s="T310">kanʼaː-Ar-LAr</ta>
            <ta e="T313" id="Seg_5804" s="T312">tu͡ok</ta>
            <ta e="T314" id="Seg_5805" s="T313">kuhagan</ta>
            <ta e="T315" id="Seg_5806" s="T314">baːr-tI-n</ta>
            <ta e="T316" id="Seg_5807" s="T315">barɨ-tI-n</ta>
            <ta e="T317" id="Seg_5808" s="T316">iti</ta>
            <ta e="T318" id="Seg_5809" s="T317">ɨl-AttAː-An</ta>
            <ta e="T319" id="Seg_5810" s="T318">is-Ar-LAr</ta>
            <ta e="T321" id="Seg_5811" s="T320">ol</ta>
            <ta e="T322" id="Seg_5812" s="T321">harɨː-ŋ</ta>
            <ta e="T323" id="Seg_5813" s="T322">ol</ta>
            <ta e="T324" id="Seg_5814" s="T323">gɨn-An</ta>
            <ta e="T325" id="Seg_5815" s="T324">baran</ta>
            <ta e="T326" id="Seg_5816" s="T325">kör-TAK-GA</ta>
            <ta e="T327" id="Seg_5817" s="T326">üčügej</ta>
            <ta e="T328" id="Seg_5818" s="T327">bagajɨ</ta>
            <ta e="T329" id="Seg_5819" s="T328">bu͡ol-Ar</ta>
            <ta e="T331" id="Seg_5820" s="T330">ol-ttAn</ta>
            <ta e="T332" id="Seg_5821" s="T331">harɨː-ŋ</ta>
            <ta e="T333" id="Seg_5822" s="T332">ile</ta>
            <ta e="T334" id="Seg_5823" s="T333">harɨː</ta>
            <ta e="T335" id="Seg_5824" s="T334">bu͡ol-IAK.[tI]-n</ta>
            <ta e="T336" id="Seg_5825" s="T335">ol</ta>
            <ta e="T337" id="Seg_5826" s="T336">gɨn-An</ta>
            <ta e="T338" id="Seg_5827" s="T337">bu͡o</ta>
            <ta e="T339" id="Seg_5828" s="T338">ɨːs-LAːK</ta>
            <ta e="T340" id="Seg_5829" s="T339">bu͡ol-IAK.[tI]-n</ta>
            <ta e="T341" id="Seg_5830" s="T340">ol-tI-GI-n</ta>
            <ta e="T342" id="Seg_5831" s="T341">emi͡e</ta>
            <ta e="T343" id="Seg_5832" s="T342">imit-IAK-LAːK-LAr</ta>
            <ta e="T345" id="Seg_5833" s="T344">imit-AːrI</ta>
            <ta e="T346" id="Seg_5834" s="T345">gɨn-TAK-TArInA</ta>
            <ta e="T347" id="Seg_5835" s="T346">dʼe</ta>
            <ta e="T348" id="Seg_5836" s="T347">kim-LAː-Ar-LAr</ta>
            <ta e="T349" id="Seg_5837" s="T348">kɨːs</ta>
            <ta e="T350" id="Seg_5838" s="T349">ogo-LArI-n</ta>
            <ta e="T351" id="Seg_5839" s="T350">ɨgɨr-AːččI-LAr</ta>
            <ta e="T352" id="Seg_5840" s="T351">togo</ta>
            <ta e="T353" id="Seg_5841" s="T352">ere</ta>
            <ta e="T355" id="Seg_5842" s="T354">iti</ta>
            <ta e="T356" id="Seg_5843" s="T355">kim-GA</ta>
            <ta e="T357" id="Seg_5844" s="T356">taŋas</ta>
            <ta e="T358" id="Seg_5845" s="T357">ɨjaː-n-Ar</ta>
            <ta e="T359" id="Seg_5846" s="T358">kim-LAr-GA</ta>
            <ta e="T360" id="Seg_5847" s="T359">ɨjaː-Ar-LAr</ta>
            <ta e="T361" id="Seg_5848" s="T360">bu͡o</ta>
            <ta e="T362" id="Seg_5849" s="T361">ol</ta>
            <ta e="T363" id="Seg_5850" s="T362">tiriː-GI-n</ta>
            <ta e="T364" id="Seg_5851" s="T363">taŋas</ta>
            <ta e="T365" id="Seg_5852" s="T364">ɨjaː-n-Ar</ta>
            <ta e="T367" id="Seg_5853" s="T366">ol-tI-GI-n</ta>
            <ta e="T368" id="Seg_5854" s="T367">battaː-An</ta>
            <ta e="T369" id="Seg_5855" s="T368">tur-IAK-GI-n</ta>
            <ta e="T370" id="Seg_5856" s="T369">naːda</ta>
            <ta e="T372" id="Seg_5857" s="T371">hɨrga-nI</ta>
            <ta e="T373" id="Seg_5858" s="T372">ti͡er-A</ta>
            <ta e="T374" id="Seg_5859" s="T373">uːr-An</ta>
            <ta e="T375" id="Seg_5860" s="T374">bar-An-LAr</ta>
            <ta e="T376" id="Seg_5861" s="T375">emi͡e</ta>
            <ta e="T377" id="Seg_5862" s="T376">battaː-An</ta>
            <ta e="T378" id="Seg_5863" s="T377">tur-IAK-GI-n</ta>
            <ta e="T379" id="Seg_5864" s="T378">naːda</ta>
            <ta e="T380" id="Seg_5865" s="T379">ol</ta>
            <ta e="T381" id="Seg_5866" s="T380">tiriː-nI</ta>
            <ta e="T382" id="Seg_5867" s="T381">uːr-An</ta>
            <ta e="T383" id="Seg_5868" s="T382">bar-An-GIn</ta>
            <ta e="T384" id="Seg_5869" s="T383">harɨː</ta>
            <ta e="T385" id="Seg_5870" s="T384">bu͡ol-IAK-tI-n</ta>
            <ta e="T387" id="Seg_5871" s="T386">dʼe</ta>
            <ta e="T388" id="Seg_5872" s="T387">ol</ta>
            <ta e="T389" id="Seg_5873" s="T388">battaː-An</ta>
            <ta e="T390" id="Seg_5874" s="T389">tur-A-GIn</ta>
            <ta e="T392" id="Seg_5875" s="T391">ol</ta>
            <ta e="T393" id="Seg_5876" s="T392">battaː-An</ta>
            <ta e="T394" id="Seg_5877" s="T393">tur-TAK-GInA</ta>
            <ta e="T395" id="Seg_5878" s="T394">en-GA</ta>
            <ta e="T396" id="Seg_5879" s="T395">bi͡er-Ar-LAr</ta>
            <ta e="T397" id="Seg_5880" s="T396">iti</ta>
            <ta e="T398" id="Seg_5881" s="T397">kim-nI</ta>
            <ta e="T399" id="Seg_5882" s="T398">bɨ͡ar-nI</ta>
            <ta e="T400" id="Seg_5883" s="T399">bus-BIT</ta>
            <ta e="T401" id="Seg_5884" s="T400">taba</ta>
            <ta e="T402" id="Seg_5885" s="T401">bɨ͡ar-tI-n</ta>
            <ta e="T403" id="Seg_5886" s="T402">ol</ta>
            <ta e="T404" id="Seg_5887" s="T403">bɨ͡ar-nI</ta>
            <ta e="T405" id="Seg_5888" s="T404">battaː-A</ta>
            <ta e="T406" id="Seg_5889" s="T405">tur-An</ta>
            <ta e="T407" id="Seg_5890" s="T406">kɨːs</ta>
            <ta e="T408" id="Seg_5891" s="T407">kɨːs</ta>
            <ta e="T409" id="Seg_5892" s="T408">ogo</ta>
            <ta e="T411" id="Seg_5893" s="T410">ɨstaː-IAK-LAːK</ta>
            <ta e="T412" id="Seg_5894" s="T411">ɨstaː</ta>
            <ta e="T413" id="Seg_5895" s="T412">di͡e-Ar-LAr</ta>
            <ta e="T414" id="Seg_5896" s="T413">bu͡o</ta>
            <ta e="T415" id="Seg_5897" s="T414">kajdi͡ek</ta>
            <ta e="T416" id="Seg_5898" s="T415">bu͡ol-IAK-ŋ=Ij</ta>
            <ta e="T417" id="Seg_5899" s="T416">ka</ta>
            <ta e="T419" id="Seg_5900" s="T418">bagar-BAT</ta>
            <ta e="T420" id="Seg_5901" s="T419">bu͡ol-IAK.[tA]</ta>
            <ta e="T421" id="Seg_5902" s="T420">bu͡olu͡o</ta>
            <ta e="T422" id="Seg_5903" s="T421">ol-tI-ŋ</ta>
            <ta e="T423" id="Seg_5904" s="T422">amtan-tA</ta>
            <ta e="T424" id="Seg_5905" s="T423">kuhagana</ta>
            <ta e="T425" id="Seg_5906" s="T424">hin</ta>
            <ta e="T426" id="Seg_5907" s="T425">tur-A-GIn</ta>
            <ta e="T427" id="Seg_5908" s="T426">bu͡o</ta>
            <ta e="T429" id="Seg_5909" s="T428">ihit-A-GIn</ta>
            <ta e="T430" id="Seg_5910" s="T429">bu͡o</ta>
            <ta e="T431" id="Seg_5911" s="T430">inʼe-ŋ</ta>
            <ta e="T432" id="Seg_5912" s="T431">haŋa-tI-n</ta>
            <ta e="T434" id="Seg_5913" s="T433">ol</ta>
            <ta e="T435" id="Seg_5914" s="T434">tur-A</ta>
            <ta e="T436" id="Seg_5915" s="T435">tur-A</ta>
            <ta e="T437" id="Seg_5916" s="T436">ɨstaː-A</ta>
            <ta e="T438" id="Seg_5917" s="T437">tur-A-GIn</ta>
            <ta e="T439" id="Seg_5918" s="T438">ol</ta>
            <ta e="T440" id="Seg_5919" s="T439">kim-nI</ta>
            <ta e="T441" id="Seg_5920" s="T440">bɨ͡ar-nI</ta>
            <ta e="T442" id="Seg_5921" s="T441">tur-An-GIn</ta>
            <ta e="T444" id="Seg_5922" s="T443">giniler-I-ŋ</ta>
            <ta e="T445" id="Seg_5923" s="T444">ol</ta>
            <ta e="T447" id="Seg_5924" s="T446">ol</ta>
            <ta e="T448" id="Seg_5925" s="T447">ɨstaː-BIT</ta>
            <ta e="T449" id="Seg_5926" s="T448">bɨ͡ar-GI-n</ta>
            <ta e="T450" id="Seg_5927" s="T449">ɨl-A</ta>
            <ta e="T451" id="Seg_5928" s="T450">ɨl-Ar-LAr</ta>
            <ta e="T452" id="Seg_5929" s="T451">ol</ta>
            <ta e="T453" id="Seg_5930" s="T452">tiriː-GI-n</ta>
            <ta e="T454" id="Seg_5931" s="T453">ol</ta>
            <ta e="T455" id="Seg_5932" s="T454">kim-LAː-BIT</ta>
            <ta e="T456" id="Seg_5933" s="T455">gedereː-LAː-BIT</ta>
            <ta e="T457" id="Seg_5934" s="T456">tiriː-GI-n</ta>
            <ta e="T458" id="Seg_5935" s="T457">ɨl-An</ta>
            <ta e="T459" id="Seg_5936" s="T458">bar-An-LAr</ta>
            <ta e="T460" id="Seg_5937" s="T459">ol</ta>
            <ta e="T461" id="Seg_5938" s="T460">bis-Ar-LAr</ta>
            <ta e="T463" id="Seg_5939" s="T462">ol</ta>
            <ta e="T464" id="Seg_5940" s="T463">gɨn-An</ta>
            <ta e="T465" id="Seg_5941" s="T464">baran</ta>
            <ta e="T467" id="Seg_5942" s="T466">hoŋohoːn-I-nAn</ta>
            <ta e="T468" id="Seg_5943" s="T467">hoŋohoːn-LAː-Ar-LAr</ta>
            <ta e="T469" id="Seg_5944" s="T468">iliː-LArI-nAn</ta>
            <ta e="T471" id="Seg_5945" s="T470">biːr</ta>
            <ta e="T472" id="Seg_5946" s="T471">iliː-nAn</ta>
            <ta e="T473" id="Seg_5947" s="T472">tut-Ar</ta>
            <ta e="T474" id="Seg_5948" s="T473">ol</ta>
            <ta e="T475" id="Seg_5949" s="T474">tiriː-GI-n</ta>
            <ta e="T476" id="Seg_5950" s="T475">biːr</ta>
            <ta e="T477" id="Seg_5951" s="T476">iliː-nAn</ta>
            <ta e="T478" id="Seg_5952" s="T477">hoŋohoːn-GI-n</ta>
            <ta e="T479" id="Seg_5953" s="T478">tut-Ar</ta>
            <ta e="T480" id="Seg_5954" s="T479">bu͡o</ta>
            <ta e="T481" id="Seg_5955" s="T480">ol</ta>
            <ta e="T482" id="Seg_5956" s="T481">bu͡o</ta>
            <ta e="T483" id="Seg_5957" s="T482">üːhe-ttAn</ta>
            <ta e="T484" id="Seg_5958" s="T483">allaraː-GA</ta>
            <ta e="T485" id="Seg_5959" s="T484">di͡eri</ta>
            <ta e="T486" id="Seg_5960" s="T485">innʼe</ta>
            <ta e="T487" id="Seg_5961" s="T486">hoŋohoːn-LAː-Ar</ta>
            <ta e="T488" id="Seg_5962" s="T487">eː</ta>
            <ta e="T489" id="Seg_5963" s="T488">bis-A</ta>
            <ta e="T490" id="Seg_5964" s="T489">bis-A</ta>
            <ta e="T492" id="Seg_5965" s="T491">dʼe</ta>
            <ta e="T493" id="Seg_5966" s="T492">ol</ta>
            <ta e="T494" id="Seg_5967" s="T493">gɨn-An</ta>
            <ta e="T495" id="Seg_5968" s="T494">baran</ta>
            <ta e="T496" id="Seg_5969" s="T495">giniler-I-ŋ</ta>
            <ta e="T497" id="Seg_5970" s="T496">kɨːs</ta>
            <ta e="T498" id="Seg_5971" s="T497">ogo-LAr</ta>
            <ta e="T499" id="Seg_5972" s="T498">kahan</ta>
            <ta e="T500" id="Seg_5973" s="T499">da</ta>
            <ta e="T501" id="Seg_5974" s="T500">tur-IAK-LArI-n</ta>
            <ta e="T502" id="Seg_5975" s="T501">bagar-AːččI-tA</ta>
            <ta e="T503" id="Seg_5976" s="T502">hu͡ok-LAr</ta>
            <ta e="T505" id="Seg_5977" s="T504">battaː-An</ta>
            <ta e="T506" id="Seg_5978" s="T505">tur-IAK-GI-n</ta>
            <ta e="T507" id="Seg_5979" s="T506">naːda</ta>
            <ta e="T508" id="Seg_5980" s="T507">bu͡o</ta>
            <ta e="T509" id="Seg_5981" s="T508">ör</ta>
            <ta e="T510" id="Seg_5982" s="T509">bagajɨ</ta>
            <ta e="T511" id="Seg_5983" s="T510">tur-IAK-GI-n</ta>
            <ta e="T513" id="Seg_5984" s="T512">tur-IAK-LAːK-GIn</ta>
            <ta e="T514" id="Seg_5985" s="T513">onno</ta>
            <ta e="T516" id="Seg_5986" s="T515">a</ta>
            <ta e="T517" id="Seg_5987" s="T516">ɨstaː-Ar-I-ŋ</ta>
            <ta e="T518" id="Seg_5988" s="T517">baːr</ta>
            <ta e="T519" id="Seg_5989" s="T518">dʼe</ta>
            <ta e="T520" id="Seg_5990" s="T519">kuhagan</ta>
            <ta e="T521" id="Seg_5991" s="T520">da</ta>
            <ta e="T522" id="Seg_5992" s="T521">kuhagan</ta>
            <ta e="T524" id="Seg_5993" s="T523">eː</ta>
            <ta e="T525" id="Seg_5994" s="T524">ɨstaː-IAK-ŋ</ta>
            <ta e="T526" id="Seg_5995" s="T525">kihi</ta>
            <ta e="T527" id="Seg_5996" s="T526">da</ta>
            <ta e="T528" id="Seg_5997" s="T527">hürek-tA</ta>
            <ta e="T529" id="Seg_5998" s="T528">loksuj-IAK</ta>
            <ta e="T530" id="Seg_5999" s="T529">horogor</ta>
            <ta e="T531" id="Seg_6000" s="T530">bu͡ol-Ar</ta>
            <ta e="T532" id="Seg_6001" s="T531">bu͡o</ta>
            <ta e="T534" id="Seg_6002" s="T533">ol</ta>
            <ta e="T535" id="Seg_6003" s="T534">ihin</ta>
            <ta e="T537" id="Seg_6004" s="T536">kɨːs</ta>
            <ta e="T538" id="Seg_6005" s="T537">ogo-kAːN-LAr-I-ŋ</ta>
            <ta e="T539" id="Seg_6006" s="T538">küren-Ar-LAr</ta>
            <ta e="T540" id="Seg_6007" s="T539">bu͡o</ta>
            <ta e="T541" id="Seg_6008" s="T540">ɨstaː-m-AːrI-LAr</ta>
            <ta e="T542" id="Seg_6009" s="T541">bar-An</ta>
            <ta e="T543" id="Seg_6010" s="T542">kaːl-Ar-LAr</ta>
            <ta e="T545" id="Seg_6011" s="T544">kim</ta>
            <ta e="T546" id="Seg_6012" s="T545">kɨːs</ta>
            <ta e="T547" id="Seg_6013" s="T546">ogo-LAr-nI</ta>
            <ta e="T548" id="Seg_6014" s="T547">ügüs-LIk</ta>
            <ta e="T549" id="Seg_6015" s="T548">ɨgɨr-AːččI-LAr</ta>
            <ta e="T550" id="Seg_6016" s="T549">u͡ol</ta>
            <ta e="T551" id="Seg_6017" s="T550">ogo-TAːgAr</ta>
            <ta e="T552" id="Seg_6018" s="T551">togo</ta>
            <ta e="T553" id="Seg_6019" s="T552">ere</ta>
            <ta e="T555" id="Seg_6020" s="T554">dʼe</ta>
            <ta e="T556" id="Seg_6021" s="T555">ol</ta>
            <ta e="T557" id="Seg_6022" s="T556">ol</ta>
            <ta e="T558" id="Seg_6023" s="T557">gɨn-An</ta>
            <ta e="T559" id="Seg_6024" s="T558">baran</ta>
            <ta e="T560" id="Seg_6025" s="T559">ol</ta>
            <ta e="T561" id="Seg_6026" s="T560">kim-I-nAn</ta>
            <ta e="T562" id="Seg_6027" s="T561">delbi</ta>
            <ta e="T563" id="Seg_6028" s="T562">kim-TA</ta>
            <ta e="T564" id="Seg_6029" s="T563">hoŋohoːn-I-nAn</ta>
            <ta e="T565" id="Seg_6030" s="T564">delbi</ta>
            <ta e="T566" id="Seg_6031" s="T565">kim-LAː-A</ta>
            <ta e="T567" id="Seg_6032" s="T566">kim-LAː-TI-LAr</ta>
            <ta e="T568" id="Seg_6033" s="T567">bu͡o</ta>
            <ta e="T569" id="Seg_6034" s="T568">ol</ta>
            <ta e="T570" id="Seg_6035" s="T569">tiriː-GI-n</ta>
            <ta e="T572" id="Seg_6036" s="T571">ol</ta>
            <ta e="T573" id="Seg_6037" s="T572">hoŋohoːn-LAː-Ar-I-ŋ</ta>
            <ta e="T574" id="Seg_6038" s="T573">emi͡e</ta>
            <ta e="T575" id="Seg_6039" s="T574">ör</ta>
            <ta e="T576" id="Seg_6040" s="T575">bagajɨ</ta>
            <ta e="T577" id="Seg_6041" s="T576">bu͡ol-TI-tA</ta>
            <ta e="T579" id="Seg_6042" s="T578">nöŋü͡ö</ta>
            <ta e="T580" id="Seg_6043" s="T579">kün</ta>
            <ta e="T581" id="Seg_6044" s="T580">emi͡e</ta>
            <ta e="T582" id="Seg_6045" s="T581">tur-IAK-LAːK-GIn</ta>
            <ta e="T583" id="Seg_6046" s="T582">bu͡olla</ta>
            <ta e="T584" id="Seg_6047" s="T583">ol</ta>
            <ta e="T585" id="Seg_6048" s="T584">ɨstaː-A</ta>
            <ta e="T586" id="Seg_6049" s="T585">ɨstaː-A-GIn</ta>
            <ta e="T588" id="Seg_6050" s="T587">bagar-BAT</ta>
            <ta e="T589" id="Seg_6051" s="T588">bu͡ol-A-GIn</ta>
            <ta e="T590" id="Seg_6052" s="T589">hürek-I-ŋ</ta>
            <ta e="T591" id="Seg_6053" s="T590">loksuj-Ar</ta>
            <ta e="T592" id="Seg_6054" s="T591">bu͡olla</ta>
            <ta e="T594" id="Seg_6055" s="T593">dʼe</ta>
            <ta e="T595" id="Seg_6056" s="T594">emi͡e</ta>
            <ta e="T596" id="Seg_6057" s="T595">hin</ta>
            <ta e="T597" id="Seg_6058" s="T596">ɨstaː-A-GIn</ta>
            <ta e="T598" id="Seg_6059" s="T597">bu͡olla</ta>
            <ta e="T599" id="Seg_6060" s="T598">kajdi͡ek</ta>
            <ta e="T600" id="Seg_6061" s="T599">bu͡ol-IAK-ŋ=Ij</ta>
            <ta e="T601" id="Seg_6062" s="T600">inʼe-ŋ</ta>
            <ta e="T602" id="Seg_6063" s="T601">haŋa-tI-n</ta>
            <ta e="T603" id="Seg_6064" s="T602">ihit-A-GIn</ta>
            <ta e="T604" id="Seg_6065" s="T603">bu͡o</ta>
            <ta e="T606" id="Seg_6066" s="T605">dʼe</ta>
            <ta e="T607" id="Seg_6067" s="T606">ol</ta>
            <ta e="T608" id="Seg_6068" s="T607">tur-An</ta>
            <ta e="T609" id="Seg_6069" s="T608">ɨstaː-A</ta>
            <ta e="T610" id="Seg_6070" s="T609">ɨstaː-A-GIn</ta>
            <ta e="T611" id="Seg_6071" s="T610">dʼe</ta>
            <ta e="T612" id="Seg_6072" s="T611">muŋ-LAN-An</ta>
            <ta e="T613" id="Seg_6073" s="T612">muŋ-LAN-An</ta>
            <ta e="T614" id="Seg_6074" s="T613">dʼe</ta>
            <ta e="T615" id="Seg_6075" s="T614">büt-A-BIt</ta>
            <ta e="T616" id="Seg_6076" s="T615">bu͡olla</ta>
            <ta e="T618" id="Seg_6077" s="T617">kas-Is</ta>
            <ta e="T619" id="Seg_6078" s="T618">eme</ta>
            <ta e="T620" id="Seg_6079" s="T619">kün-tI-GAr</ta>
            <ta e="T621" id="Seg_6080" s="T620">dʼe</ta>
            <ta e="T622" id="Seg_6081" s="T621">ol</ta>
            <ta e="T623" id="Seg_6082" s="T622">kɨhɨ͡ak</ta>
            <ta e="T624" id="Seg_6083" s="T623">kim-I-nAn</ta>
            <ta e="T625" id="Seg_6084" s="T624">hoŋohoːn-nAn</ta>
            <ta e="T627" id="Seg_6085" s="T626">ol-ttAn</ta>
            <ta e="T628" id="Seg_6086" s="T627">kojut</ta>
            <ta e="T629" id="Seg_6087" s="T628">iti</ta>
            <ta e="T630" id="Seg_6088" s="T629">harɨː</ta>
            <ta e="T631" id="Seg_6089" s="T630">bu͡ol</ta>
            <ta e="T632" id="Seg_6090" s="T631">bukatɨn</ta>
            <ta e="T633" id="Seg_6091" s="T632">harɨː</ta>
            <ta e="T634" id="Seg_6092" s="T633">bu͡ol</ta>
            <ta e="T635" id="Seg_6093" s="T634">üčügej</ta>
            <ta e="T636" id="Seg_6094" s="T635">bu͡ol-IAK.[tI]-n</ta>
            <ta e="T637" id="Seg_6095" s="T636">hɨmnagas</ta>
            <ta e="T638" id="Seg_6096" s="T637">bu͡ol-IAK.[tI]-n</ta>
            <ta e="T639" id="Seg_6097" s="T638">iti</ta>
            <ta e="T640" id="Seg_6098" s="T639">eŋin</ta>
            <ta e="T641" id="Seg_6099" s="T640">eŋin-LIk</ta>
            <ta e="T642" id="Seg_6100" s="T641">iti</ta>
            <ta e="T643" id="Seg_6101" s="T642">kim-LAː-A</ta>
            <ta e="T644" id="Seg_6102" s="T643">hataː-TI-BIt</ta>
            <ta e="T645" id="Seg_6103" s="T644">bu͡o</ta>
            <ta e="T647" id="Seg_6104" s="T646">kɨhɨ͡ak-ttAn</ta>
            <ta e="T648" id="Seg_6105" s="T647">gedereː-nAn</ta>
            <ta e="T649" id="Seg_6106" s="T648">oŋor-A-BIt</ta>
            <ta e="T651" id="Seg_6107" s="T650">ol-tI-ŋ</ta>
            <ta e="T652" id="Seg_6108" s="T651">harɨː</ta>
            <ta e="T653" id="Seg_6109" s="T652">olus</ta>
            <ta e="T654" id="Seg_6110" s="T653">maŋan</ta>
            <ta e="T655" id="Seg_6111" s="T654">bu͡ol-AːrI</ta>
            <ta e="T656" id="Seg_6112" s="T655">tože</ta>
            <ta e="T657" id="Seg_6113" s="T656">hin</ta>
            <ta e="T658" id="Seg_6114" s="T657">kuhagan</ta>
            <ta e="T660" id="Seg_6115" s="T659">ol</ta>
            <ta e="T661" id="Seg_6116" s="T660">ihin</ta>
            <ta e="T662" id="Seg_6117" s="T661">harɨː-GI-n</ta>
            <ta e="T663" id="Seg_6118" s="T662">iti</ta>
            <ta e="T664" id="Seg_6119" s="T663">kim-GA</ta>
            <ta e="T665" id="Seg_6120" s="T664">ɨhaːr-Ar-LAr</ta>
            <ta e="T666" id="Seg_6121" s="T665">ɨhaːr-Ar-LAr</ta>
            <ta e="T667" id="Seg_6122" s="T666">buru͡o-GA</ta>
            <ta e="T669" id="Seg_6123" s="T668">ol</ta>
            <ta e="T670" id="Seg_6124" s="T669">harɨː-GI-n</ta>
            <ta e="T671" id="Seg_6125" s="T670">ɨl-An</ta>
            <ta e="T672" id="Seg_6126" s="T671">bar-An-LAr</ta>
            <ta e="T673" id="Seg_6127" s="T672">dʼi͡e</ta>
            <ta e="T674" id="Seg_6128" s="T673">is-tI-GAr</ta>
            <ta e="T675" id="Seg_6129" s="T674">ɨjaː-Ar-LAr</ta>
            <ta e="T677" id="Seg_6130" s="T676">dʼi͡e</ta>
            <ta e="T678" id="Seg_6131" s="T677">da</ta>
            <ta e="T679" id="Seg_6132" s="T678">bu͡ol-IAK.[tA]=Ij</ta>
            <ta e="T680" id="Seg_6133" s="T679">iti</ta>
            <ta e="T681" id="Seg_6134" s="T680">kim</ta>
            <ta e="T682" id="Seg_6135" s="T681">uraha</ta>
            <ta e="T683" id="Seg_6136" s="T682">is-tI-GAr</ta>
            <ta e="T684" id="Seg_6137" s="T683">uraha</ta>
            <ta e="T685" id="Seg_6138" s="T684">dʼi͡e</ta>
            <ta e="T686" id="Seg_6139" s="T685">is-tI-GAr</ta>
            <ta e="T687" id="Seg_6140" s="T686">ɨjaː-Ar-LAr</ta>
            <ta e="T689" id="Seg_6141" s="T688">onno</ta>
            <ta e="T690" id="Seg_6142" s="T689">buru͡o-GA</ta>
            <ta e="T691" id="Seg_6143" s="T690">ol-tI-ŋ</ta>
            <ta e="T692" id="Seg_6144" s="T691">ɨksa</ta>
            <ta e="T693" id="Seg_6145" s="T692">tur-Ar</ta>
            <ta e="T694" id="Seg_6146" s="T693">bu͡o</ta>
            <ta e="T696" id="Seg_6147" s="T695">a</ta>
            <ta e="T697" id="Seg_6148" s="T696">horok</ta>
            <ta e="T698" id="Seg_6149" s="T697">dʼaktar-LAr</ta>
            <ta e="T699" id="Seg_6150" s="T698">innʼe</ta>
            <ta e="T700" id="Seg_6151" s="T699">gɨn-AːččI-LAr</ta>
            <ta e="T701" id="Seg_6152" s="T700">bu͡olla</ta>
            <ta e="T703" id="Seg_6153" s="T702">ol</ta>
            <ta e="T704" id="Seg_6154" s="T703">kördük</ta>
            <ta e="T705" id="Seg_6155" s="T704">da</ta>
            <ta e="T706" id="Seg_6156" s="T705">ɨhaːr-A</ta>
            <ta e="T707" id="Seg_6157" s="T706">hɨt-IAK</ta>
            <ta e="T708" id="Seg_6158" s="T707">e-BIT-LAr</ta>
            <ta e="T709" id="Seg_6159" s="T708">otto</ta>
            <ta e="T711" id="Seg_6160" s="T710">a</ta>
            <ta e="T712" id="Seg_6161" s="T711">gini</ta>
            <ta e="T713" id="Seg_6162" s="T712">giniler-I-ŋ</ta>
            <ta e="T714" id="Seg_6163" s="T713">innʼekeːt</ta>
            <ta e="T715" id="Seg_6164" s="T714">di͡e-Ar-LAr</ta>
            <ta e="T716" id="Seg_6165" s="T715">bu͡o</ta>
            <ta e="T717" id="Seg_6166" s="T716">bihigi-nI</ta>
            <ta e="T718" id="Seg_6167" s="T717">tur-IAr-Ar-LAr</ta>
            <ta e="T719" id="Seg_6168" s="T718">bu͡o</ta>
            <ta e="T720" id="Seg_6169" s="T719">üs</ta>
            <ta e="T721" id="Seg_6170" s="T720">tü͡ört</ta>
            <ta e="T722" id="Seg_6171" s="T721">kɨːs</ta>
            <ta e="T723" id="Seg_6172" s="T722">kɨːs</ta>
            <ta e="T724" id="Seg_6173" s="T723">ogo-LAr</ta>
            <ta e="T868" id="Seg_6174" s="T724">kim</ta>
            <ta e="T869" id="Seg_6175" s="T868">kim</ta>
            <ta e="T870" id="Seg_6176" s="T869">u͡ot-tA</ta>
            <ta e="T871" id="Seg_6177" s="T870">kim</ta>
            <ta e="T872" id="Seg_6178" s="T871">u͡ot-tA</ta>
            <ta e="T873" id="Seg_6179" s="T872">iti</ta>
            <ta e="T874" id="Seg_6180" s="T873">ačaːk</ta>
            <ta e="T725" id="Seg_6181" s="T874">oŋor-Ar-LAr</ta>
            <ta e="T726" id="Seg_6182" s="T725">mas</ta>
            <ta e="T727" id="Seg_6183" s="T726">ubaj-Ar</ta>
            <ta e="T728" id="Seg_6184" s="T727">ol-tI-ŋ</ta>
            <ta e="T729" id="Seg_6185" s="T728">ürüt-tI-nAn</ta>
            <ta e="T730" id="Seg_6186" s="T729">ot-LAr-nI</ta>
            <ta e="T731" id="Seg_6187" s="T730">bɨrak-Ar-LAr</ta>
            <ta e="T732" id="Seg_6188" s="T731">ulakan</ta>
            <ta e="T733" id="Seg_6189" s="T732">buru͡o</ta>
            <ta e="T734" id="Seg_6190" s="T733">kel-IAK.[tI]-n</ta>
            <ta e="T736" id="Seg_6191" s="T735">dʼe</ta>
            <ta e="T737" id="Seg_6192" s="T736">ol</ta>
            <ta e="T738" id="Seg_6193" s="T737">buru͡o</ta>
            <ta e="T739" id="Seg_6194" s="T738">is-tI-GAr</ta>
            <ta e="T740" id="Seg_6195" s="T739">tögürüččü</ta>
            <ta e="T741" id="Seg_6196" s="T740">tur-An-BIt</ta>
            <ta e="T742" id="Seg_6197" s="T741">ol</ta>
            <ta e="T743" id="Seg_6198" s="T742">tut-A-BIt</ta>
            <ta e="T744" id="Seg_6199" s="T743">bu͡olla</ta>
            <ta e="T745" id="Seg_6200" s="T744">kim-nI</ta>
            <ta e="T747" id="Seg_6201" s="T746">ol</ta>
            <ta e="T748" id="Seg_6202" s="T747">gɨn-An</ta>
            <ta e="T749" id="Seg_6203" s="T748">baran</ta>
            <ta e="T750" id="Seg_6204" s="T749">ergij-t-A-BIt</ta>
            <ta e="T751" id="Seg_6205" s="T750">kün</ta>
            <ta e="T752" id="Seg_6206" s="T751">hu͡ol-tI-n</ta>
            <ta e="T753" id="Seg_6207" s="T752">kördük</ta>
            <ta e="T754" id="Seg_6208" s="T753">ergij-t-A-BIt</ta>
            <ta e="T756" id="Seg_6209" s="T755">üčügej-LIk</ta>
            <ta e="T757" id="Seg_6210" s="T756">tut-IAK-LAːK-GIn</ta>
            <ta e="T758" id="Seg_6211" s="T757">ol-tI-GI-n</ta>
            <ta e="T759" id="Seg_6212" s="T758">ɨːt-BAT</ta>
            <ta e="T760" id="Seg_6213" s="T759">kördük</ta>
            <ta e="T762" id="Seg_6214" s="T761">beje</ta>
            <ta e="T763" id="Seg_6215" s="T762">beje-BItI-GAr</ta>
            <ta e="T764" id="Seg_6216" s="T763">ergij-t-A-BIt</ta>
            <ta e="T765" id="Seg_6217" s="T764">bu͡ol</ta>
            <ta e="T766" id="Seg_6218" s="T765">innʼekeːt</ta>
            <ta e="T768" id="Seg_6219" s="T767">min</ta>
            <ta e="T769" id="Seg_6220" s="T768">dogor-BA-r</ta>
            <ta e="T770" id="Seg_6221" s="T769">bi͡er-IAK-m</ta>
            <ta e="T771" id="Seg_6222" s="T770">dogor-I-m</ta>
            <ta e="T772" id="Seg_6223" s="T771">nöŋü͡ö</ta>
            <ta e="T773" id="Seg_6224" s="T772">dogor-GA-r</ta>
            <ta e="T774" id="Seg_6225" s="T773">itigirdik-kAːN</ta>
            <ta e="T775" id="Seg_6226" s="T774">bi͡er-Ar</ta>
            <ta e="T777" id="Seg_6227" s="T776">tögürüj-t-A-BIt</ta>
            <ta e="T778" id="Seg_6228" s="T777">ol</ta>
            <ta e="T779" id="Seg_6229" s="T778">tiriː-nI</ta>
            <ta e="T781" id="Seg_6230" s="T780">alɨn-GItI-ttAn</ta>
            <ta e="T782" id="Seg_6231" s="T781">ulakan</ta>
            <ta e="T783" id="Seg_6232" s="T782">buru͡o</ta>
            <ta e="T784" id="Seg_6233" s="T783">kel-Ar</ta>
            <ta e="T785" id="Seg_6234" s="T784">bu͡o</ta>
            <ta e="T787" id="Seg_6235" s="T786">bihigi-GA</ta>
            <ta e="T788" id="Seg_6236" s="T787">emi͡e</ta>
            <ta e="T789" id="Seg_6237" s="T788">kuhagan</ta>
            <ta e="T790" id="Seg_6238" s="T789">ol</ta>
            <ta e="T791" id="Seg_6239" s="T790">buru͡o</ta>
            <ta e="T792" id="Seg_6240" s="T791">kel-Ar-tA</ta>
            <ta e="T794" id="Seg_6241" s="T793">karak-BIt</ta>
            <ta e="T795" id="Seg_6242" s="T794">ahɨj-Ar</ta>
            <ta e="T796" id="Seg_6243" s="T795">bu͡olla</ta>
            <ta e="T797" id="Seg_6244" s="T796">ol</ta>
            <ta e="T798" id="Seg_6245" s="T797">buru͡o-ttAn</ta>
            <ta e="T800" id="Seg_6246" s="T799">anʼak-BItI-GAr</ta>
            <ta e="T801" id="Seg_6247" s="T800">da</ta>
            <ta e="T802" id="Seg_6248" s="T801">ɨl-A-BIt</ta>
            <ta e="T803" id="Seg_6249" s="T802">bu͡olla</ta>
            <ta e="T804" id="Seg_6250" s="T803">buru͡o-nI</ta>
            <ta e="T806" id="Seg_6251" s="T805">dʼe</ta>
            <ta e="T807" id="Seg_6252" s="T806">ol</ta>
            <ta e="T808" id="Seg_6253" s="T807">da</ta>
            <ta e="T809" id="Seg_6254" s="T808">bu͡ol-TAR</ta>
            <ta e="T810" id="Seg_6255" s="T809">dʼe</ta>
            <ta e="T811" id="Seg_6256" s="T810">tur-I-n-AːktAː-A-BIt</ta>
            <ta e="T812" id="Seg_6257" s="T811">bu͡o</ta>
            <ta e="T813" id="Seg_6258" s="T812">tur-I-ŋ</ta>
            <ta e="T814" id="Seg_6259" s="T813">tur-I-ŋ</ta>
            <ta e="T815" id="Seg_6260" s="T814">di͡e-Ar-LAr</ta>
            <ta e="T816" id="Seg_6261" s="T815">bu͡o</ta>
            <ta e="T817" id="Seg_6262" s="T816">bihigi-nI</ta>
            <ta e="T819" id="Seg_6263" s="T818">dʼe</ta>
            <ta e="T820" id="Seg_6264" s="T819">ol</ta>
            <ta e="T821" id="Seg_6265" s="T820">kördük</ta>
            <ta e="T822" id="Seg_6266" s="T821">ergij-t-A-BIt</ta>
            <ta e="T824" id="Seg_6267" s="T823">ol-ttAn</ta>
            <ta e="T825" id="Seg_6268" s="T824">biːr</ta>
            <ta e="T826" id="Seg_6269" s="T825">eme</ta>
            <ta e="T827" id="Seg_6270" s="T826">kɨːs</ta>
            <ta e="T828" id="Seg_6271" s="T827">ogo-tA</ta>
            <ta e="T829" id="Seg_6272" s="T828">tiriː-nI</ta>
            <ta e="T830" id="Seg_6273" s="T829">iliː-tI-nAn</ta>
            <ta e="T831" id="Seg_6274" s="T830">innʼekeːt-I-ŋ</ta>
            <ta e="T832" id="Seg_6275" s="T831">ɨl-An</ta>
            <ta e="T833" id="Seg_6276" s="T832">keːs-IAK.[tA]</ta>
            <ta e="T834" id="Seg_6277" s="T833">hɨːha</ta>
            <ta e="T835" id="Seg_6278" s="T834">tut-IAK.[tA]</ta>
            <ta e="T836" id="Seg_6279" s="T835">ol-tI-tI-n</ta>
            <ta e="T837" id="Seg_6280" s="T836">ol</ta>
            <ta e="T838" id="Seg_6281" s="T837">tut-Ar</ta>
            <ta e="T839" id="Seg_6282" s="T838">hir-tI-n</ta>
            <ta e="T840" id="Seg_6283" s="T839">tiriː-tI-n</ta>
            <ta e="T841" id="Seg_6284" s="T840">dʼe</ta>
            <ta e="T842" id="Seg_6285" s="T841">oččogo</ta>
            <ta e="T843" id="Seg_6286" s="T842">di͡e-Ar-LAr</ta>
            <ta e="T844" id="Seg_6287" s="T843">bu͡o</ta>
            <ta e="T845" id="Seg_6288" s="T844">dʼe</ta>
            <ta e="T846" id="Seg_6289" s="T845">er-GA</ta>
            <ta e="T847" id="Seg_6290" s="T846">bar-IAK-ŋ</ta>
            <ta e="T848" id="Seg_6291" s="T847">hu͡ok-tA</ta>
            <ta e="T849" id="Seg_6292" s="T848">ulaːt-TAK-GInA</ta>
            <ta e="T851" id="Seg_6293" s="T850">di͡e-A</ta>
            <ta e="T852" id="Seg_6294" s="T851">di͡e-Ar-LAr</ta>
            <ta e="T853" id="Seg_6295" s="T852">itigirdik-kAːN</ta>
            <ta e="T854" id="Seg_6296" s="T853">haŋar-Ar-LAr</ta>
            <ta e="T855" id="Seg_6297" s="T854">bu͡o</ta>
            <ta e="T856" id="Seg_6298" s="T855">bihigi-nI</ta>
            <ta e="T858" id="Seg_6299" s="T857">ol</ta>
            <ta e="T859" id="Seg_6300" s="T858">ihin</ta>
            <ta e="T860" id="Seg_6301" s="T859">ol</ta>
            <ta e="T861" id="Seg_6302" s="T860">muŋ-LAN-A</ta>
            <ta e="T862" id="Seg_6303" s="T861">tur-A-BIt</ta>
            <ta e="T863" id="Seg_6304" s="T862">bu͡o</ta>
            <ta e="T864" id="Seg_6305" s="T863">ol</ta>
            <ta e="T865" id="Seg_6306" s="T864">tiriː-nI</ta>
            <ta e="T866" id="Seg_6307" s="T865">ɨːt-BAkkA</ta>
            <ta e="T867" id="Seg_6308" s="T866">ɨntak</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T2" id="Seg_6309" s="T1">nomadize-CVB.SIM</ta>
            <ta e="T4" id="Seg_6310" s="T3">winter-ACC</ta>
            <ta e="T5" id="Seg_6311" s="T4">winter-ACC</ta>
            <ta e="T6" id="Seg_6312" s="T5">whole</ta>
            <ta e="T7" id="Seg_6313" s="T6">nomadize-CVB.SIM</ta>
            <ta e="T8" id="Seg_6314" s="T7">go-CVB.SEQ</ta>
            <ta e="T9" id="Seg_6315" s="T8">go-CVB.SEQ-3PL</ta>
            <ta e="T10" id="Seg_6316" s="T9">Dolgan-PL.[NOM]</ta>
            <ta e="T11" id="Seg_6317" s="T10">spring.[NOM]</ta>
            <ta e="T12" id="Seg_6318" s="T11">stop-PTCP.PRS</ta>
            <ta e="T13" id="Seg_6319" s="T12">earth-3PL.[NOM]</ta>
            <ta e="T14" id="Seg_6320" s="T13">there.is</ta>
            <ta e="T15" id="Seg_6321" s="T14">be-HAB.[3SG]</ta>
            <ta e="T17" id="Seg_6322" s="T16">there</ta>
            <ta e="T18" id="Seg_6323" s="T17">many</ta>
            <ta e="T19" id="Seg_6324" s="T18">very</ta>
            <ta e="T20" id="Seg_6325" s="T19">pole.tent.[NOM]</ta>
            <ta e="T21" id="Seg_6326" s="T20">house-PL.[NOM]</ta>
            <ta e="T22" id="Seg_6327" s="T21">sledge.[NOM]</ta>
            <ta e="T23" id="Seg_6328" s="T22">house-PL.[NOM]</ta>
            <ta e="T24" id="Seg_6329" s="T23">stand-HAB-3PL</ta>
            <ta e="T26" id="Seg_6330" s="T25">spring-ADJZ</ta>
            <ta e="T27" id="Seg_6331" s="T26">warm</ta>
            <ta e="T28" id="Seg_6332" s="T27">day-PL-3SG.[NOM]</ta>
            <ta e="T29" id="Seg_6333" s="T28">day.[NOM]</ta>
            <ta e="T30" id="Seg_6334" s="T29">shine-PTCP.PRS</ta>
            <ta e="T31" id="Seg_6335" s="T30">be-TEMP-3SG</ta>
            <ta e="T32" id="Seg_6336" s="T31">well</ta>
            <ta e="T33" id="Seg_6337" s="T32">woman-PL.[NOM]</ta>
            <ta e="T34" id="Seg_6338" s="T33">work-PTCP.PRS-3PL.[NOM]</ta>
            <ta e="T35" id="Seg_6339" s="T34">increase-PRS.[3SG]</ta>
            <ta e="T37" id="Seg_6340" s="T36">outside</ta>
            <ta e="T38" id="Seg_6341" s="T37">3PL-EP-2SG.[NOM]</ta>
            <ta e="T39" id="Seg_6342" s="T38">at.first</ta>
            <ta e="T40" id="Seg_6343" s="T39">that.[NOM]</ta>
            <ta e="T41" id="Seg_6344" s="T40">skin.[NOM]</ta>
            <ta e="T42" id="Seg_6345" s="T41">work-3SG-ACC</ta>
            <ta e="T43" id="Seg_6346" s="T42">work-PRS-3PL</ta>
            <ta e="T45" id="Seg_6347" s="T44">skin.[NOM]</ta>
            <ta e="T46" id="Seg_6348" s="T45">skin-2SG-ACC</ta>
            <ta e="T47" id="Seg_6349" s="T46">very</ta>
            <ta e="T48" id="Seg_6350" s="T47">enough</ta>
            <ta e="T49" id="Seg_6351" s="T48">clean-PRS-3PL</ta>
            <ta e="T50" id="Seg_6352" s="T49">at.first</ta>
            <ta e="T52" id="Seg_6353" s="T51">clean-CVB.SEQ</ta>
            <ta e="T53" id="Seg_6354" s="T52">go-CVB.SEQ-3PL</ta>
            <ta e="T54" id="Seg_6355" s="T53">scissors-EP-INSTR</ta>
            <ta e="T55" id="Seg_6356" s="T54">cut-PRS-3PL</ta>
            <ta e="T57" id="Seg_6357" s="T56">animal.hair-3SG-ACC</ta>
            <ta e="T59" id="Seg_6358" s="T58">that</ta>
            <ta e="T60" id="Seg_6359" s="T59">scissors-EP-INSTR</ta>
            <ta e="T61" id="Seg_6360" s="T60">cut-CVB.SEQ</ta>
            <ta e="T62" id="Seg_6361" s="T61">stop-TEMP-3PL</ta>
            <ta e="T63" id="Seg_6362" s="T62">that-ABL</ta>
            <ta e="T64" id="Seg_6363" s="T63">later</ta>
            <ta e="T65" id="Seg_6364" s="T64">knife-EP-INSTR</ta>
            <ta e="T66" id="Seg_6365" s="T65">again</ta>
            <ta e="T67" id="Seg_6366" s="T66">cut-PRS-3PL</ta>
            <ta e="T69" id="Seg_6367" s="T68">leather.cover.for.a.tent.[NOM]</ta>
            <ta e="T70" id="Seg_6368" s="T69">Dolgan-PL.[NOM]</ta>
            <ta e="T71" id="Seg_6369" s="T70">very</ta>
            <ta e="T72" id="Seg_6370" s="T71">furry</ta>
            <ta e="T73" id="Seg_6371" s="T72">who-ACC</ta>
            <ta e="T74" id="Seg_6372" s="T73">skin-ACC</ta>
            <ta e="T75" id="Seg_6373" s="T74">take-PTCP.HAB-3SG</ta>
            <ta e="T76" id="Seg_6374" s="T75">no-3PL</ta>
            <ta e="T78" id="Seg_6375" s="T77">cut-PASS/REFL-EP-PTCP.PST</ta>
            <ta e="T79" id="Seg_6376" s="T78">that.[NOM]</ta>
            <ta e="T80" id="Seg_6377" s="T79">animal.hair-3SG-ACC</ta>
            <ta e="T81" id="Seg_6378" s="T80">take-EP-PASS/REFL-EP-PTCP.PST-ABL</ta>
            <ta e="T83" id="Seg_6379" s="T82">cut-PTCP.PST</ta>
            <ta e="T84" id="Seg_6380" s="T83">animal.hair-ACC</ta>
            <ta e="T85" id="Seg_6381" s="T84">who-ACC</ta>
            <ta e="T86" id="Seg_6382" s="T85">fur-ACC</ta>
            <ta e="T87" id="Seg_6383" s="T86">take-CVB.SEQ-3PL</ta>
            <ta e="T88" id="Seg_6384" s="T87">who.[NOM]</ta>
            <ta e="T89" id="Seg_6385" s="T88">house.[NOM]</ta>
            <ta e="T90" id="Seg_6386" s="T89">lid-3SG.[NOM]</ta>
            <ta e="T91" id="Seg_6387" s="T90">that.[NOM]</ta>
            <ta e="T92" id="Seg_6388" s="T91">leather.cover.for.a.tent-PL-ACC</ta>
            <ta e="T93" id="Seg_6389" s="T92">sew-HAB-3PL</ta>
            <ta e="T95" id="Seg_6390" s="T94">such</ta>
            <ta e="T96" id="Seg_6391" s="T95">though</ta>
            <ta e="T97" id="Seg_6392" s="T96">well</ta>
            <ta e="T98" id="Seg_6393" s="T97">that</ta>
            <ta e="T99" id="Seg_6394" s="T98">a.little</ta>
            <ta e="T100" id="Seg_6395" s="T99">animal.hair-ACC</ta>
            <ta e="T101" id="Seg_6396" s="T100">let-PRS-3PL</ta>
            <ta e="T102" id="Seg_6397" s="T101">EMPH</ta>
            <ta e="T104" id="Seg_6398" s="T103">that-ABL</ta>
            <ta e="T105" id="Seg_6399" s="T104">that-3SG-2SG-INSTR</ta>
            <ta e="T106" id="Seg_6400" s="T105">leather.cover.for.a.tent.[NOM]</ta>
            <ta e="T107" id="Seg_6401" s="T106">sew-PRS-3PL</ta>
            <ta e="T109" id="Seg_6402" s="T108">that</ta>
            <ta e="T111" id="Seg_6403" s="T110">some</ta>
            <ta e="T112" id="Seg_6404" s="T111">fur-ACC</ta>
            <ta e="T113" id="Seg_6405" s="T112">3PL-EP-2SG.[NOM]</ta>
            <ta e="T114" id="Seg_6406" s="T113">that.[NOM]</ta>
            <ta e="T115" id="Seg_6407" s="T114">thin.layer.of.leather.[NOM]</ta>
            <ta e="T116" id="Seg_6408" s="T115">make-FUT-NEC-3PL</ta>
            <ta e="T118" id="Seg_6409" s="T117">thin.layer.of.leather.[NOM]</ta>
            <ta e="T119" id="Seg_6410" s="T118">make-PTCP.PRS-3PL-DAT/LOC</ta>
            <ta e="T120" id="Seg_6411" s="T119">there</ta>
            <ta e="T121" id="Seg_6412" s="T120">well</ta>
            <ta e="T122" id="Seg_6413" s="T121">however</ta>
            <ta e="T123" id="Seg_6414" s="T122">many</ta>
            <ta e="T124" id="Seg_6415" s="T123">work.[NOM]</ta>
            <ta e="T126" id="Seg_6416" s="T125">cut-PRS-3PL</ta>
            <ta e="T127" id="Seg_6417" s="T126">EMPH</ta>
            <ta e="T128" id="Seg_6418" s="T127">that.[NOM]</ta>
            <ta e="T129" id="Seg_6419" s="T128">cut-PTCP.PST-3PL-ACC</ta>
            <ta e="T130" id="Seg_6420" s="T129">similar</ta>
            <ta e="T131" id="Seg_6421" s="T130">animal.hair-3SG.[NOM]</ta>
            <ta e="T132" id="Seg_6422" s="T131">NEG.EX</ta>
            <ta e="T133" id="Seg_6423" s="T132">EMPH</ta>
            <ta e="T134" id="Seg_6424" s="T133">animal.hair-3SG.[NOM]</ta>
            <ta e="T135" id="Seg_6425" s="T134">little</ta>
            <ta e="T136" id="Seg_6426" s="T135">be-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T137" id="Seg_6427" s="T136">however</ta>
            <ta e="T138" id="Seg_6428" s="T137">animal.hair-3SG.[NOM]</ta>
            <ta e="T139" id="Seg_6429" s="T138">stay-PRS.[3SG]</ta>
            <ta e="T140" id="Seg_6430" s="T139">EMPH</ta>
            <ta e="T141" id="Seg_6431" s="T140">skin-2SG-DAT/LOC</ta>
            <ta e="T143" id="Seg_6432" s="T142">that</ta>
            <ta e="T144" id="Seg_6433" s="T143">animal.hair.[NOM]</ta>
            <ta e="T145" id="Seg_6434" s="T144">animal.hair-2SG.[NOM]</ta>
            <ta e="T146" id="Seg_6435" s="T145">good-ADVZ</ta>
            <ta e="T147" id="Seg_6436" s="T146">completely</ta>
            <ta e="T148" id="Seg_6437" s="T147">fall-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T149" id="Seg_6438" s="T148">that-ABL</ta>
            <ta e="T150" id="Seg_6439" s="T149">later</ta>
            <ta e="T151" id="Seg_6440" s="T150">process-PTCP.PRS-DAT/LOC</ta>
            <ta e="T152" id="Seg_6441" s="T151">EMPH</ta>
            <ta e="T153" id="Seg_6442" s="T152">good</ta>
            <ta e="T154" id="Seg_6443" s="T153">EMPH</ta>
            <ta e="T155" id="Seg_6444" s="T154">thin.layer.of.leather.[NOM]</ta>
            <ta e="T156" id="Seg_6445" s="T155">become-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T157" id="Seg_6446" s="T156">that</ta>
            <ta e="T158" id="Seg_6447" s="T157">animal.hair-PROPR</ta>
            <ta e="T159" id="Seg_6448" s="T158">who.[NOM]</ta>
            <ta e="T160" id="Seg_6449" s="T159">fur-ACC</ta>
            <ta e="T161" id="Seg_6450" s="T160">3PL-EP-2SG.[NOM]</ta>
            <ta e="T162" id="Seg_6451" s="T161">lie-EP-CAUS-PRS-3PL</ta>
            <ta e="T163" id="Seg_6452" s="T162">water-DAT/LOC</ta>
            <ta e="T165" id="Seg_6453" s="T164">water.[NOM]</ta>
            <ta e="T166" id="Seg_6454" s="T165">inside-3SG-DAT/LOC</ta>
            <ta e="T167" id="Seg_6455" s="T166">soap-ACC</ta>
            <ta e="T168" id="Seg_6456" s="T167">pour-PRS-3PL</ta>
            <ta e="T169" id="Seg_6457" s="T168">lay-PRS-3PL</ta>
            <ta e="T171" id="Seg_6458" s="T170">that-ABL</ta>
            <ta e="T172" id="Seg_6459" s="T171">different-different</ta>
            <ta e="T173" id="Seg_6460" s="T172">grass-PL-ACC</ta>
            <ta e="T175" id="Seg_6461" s="T174">that</ta>
            <ta e="T176" id="Seg_6462" s="T175">make-CVB.SIM</ta>
            <ta e="T177" id="Seg_6463" s="T176">make-CVB.SIM</ta>
            <ta e="T178" id="Seg_6464" s="T177">how.much</ta>
            <ta e="T179" id="Seg_6465" s="T178">INDEF</ta>
            <ta e="T180" id="Seg_6466" s="T179">day-ACC</ta>
            <ta e="T181" id="Seg_6467" s="T180">lie-FUT-NEC.[3SG]</ta>
            <ta e="T183" id="Seg_6468" s="T182">warm</ta>
            <ta e="T184" id="Seg_6469" s="T183">and</ta>
            <ta e="T185" id="Seg_6470" s="T184">day.[NOM]</ta>
            <ta e="T186" id="Seg_6471" s="T185">be-IMP.3SG</ta>
            <ta e="T187" id="Seg_6472" s="T186">that</ta>
            <ta e="T188" id="Seg_6473" s="T187">water-2SG.[NOM]</ta>
            <ta e="T189" id="Seg_6474" s="T188">rot-PRS.[3SG]</ta>
            <ta e="T190" id="Seg_6475" s="T189">EMPH</ta>
            <ta e="T191" id="Seg_6476" s="T190">skin-2SG.[NOM]</ta>
            <ta e="T192" id="Seg_6477" s="T191">also</ta>
            <ta e="T193" id="Seg_6478" s="T192">rot-PRS.[3SG]</ta>
            <ta e="T194" id="Seg_6479" s="T193">EMPH</ta>
            <ta e="T195" id="Seg_6480" s="T194">soft.[NOM]</ta>
            <ta e="T196" id="Seg_6481" s="T195">be-PRS.[3SG]</ta>
            <ta e="T197" id="Seg_6482" s="T196">EMPH</ta>
            <ta e="T199" id="Seg_6483" s="T198">skin-2SG.[NOM]</ta>
            <ta e="T200" id="Seg_6484" s="T199">how.much</ta>
            <ta e="T201" id="Seg_6485" s="T200">INDEF</ta>
            <ta e="T202" id="Seg_6486" s="T201">day-ACC</ta>
            <ta e="T203" id="Seg_6487" s="T202">rot-CVB.SEQ</ta>
            <ta e="T204" id="Seg_6488" s="T203">stop-TEMP-3SG</ta>
            <ta e="T205" id="Seg_6489" s="T204">3PL-EP-2SG.[NOM]</ta>
            <ta e="T206" id="Seg_6490" s="T205">that</ta>
            <ta e="T207" id="Seg_6491" s="T206">mix-PRS-3PL</ta>
            <ta e="T209" id="Seg_6492" s="T208">that</ta>
            <ta e="T210" id="Seg_6493" s="T209">skin-2SG-ACC</ta>
            <ta e="T211" id="Seg_6494" s="T210">take-CVB.SEQ</ta>
            <ta e="T212" id="Seg_6495" s="T211">go-CVB.SEQ-3PL</ta>
            <ta e="T213" id="Seg_6496" s="T212">that.[NOM]</ta>
            <ta e="T214" id="Seg_6497" s="T213">who.[NOM]</ta>
            <ta e="T215" id="Seg_6498" s="T214">wash.oneself-CAUS-PRS-3PL</ta>
            <ta e="T216" id="Seg_6499" s="T215">river-DAT/LOC</ta>
            <ta e="T217" id="Seg_6500" s="T216">go-CVB.SEQ-3PL</ta>
            <ta e="T219" id="Seg_6501" s="T218">enough</ta>
            <ta e="T220" id="Seg_6502" s="T219">wash.oneself-CAUS-PRS-PST1-3PL</ta>
            <ta e="T221" id="Seg_6503" s="T220">that</ta>
            <ta e="T222" id="Seg_6504" s="T221">make-CVB.SEQ</ta>
            <ta e="T223" id="Seg_6505" s="T222">after</ta>
            <ta e="T224" id="Seg_6506" s="T223">become.dry-CAUS-PRS-3PL</ta>
            <ta e="T226" id="Seg_6507" s="T225">become.dry-CAUS-CVB.SEQ</ta>
            <ta e="T227" id="Seg_6508" s="T226">go-CVB.SEQ-3PL</ta>
            <ta e="T228" id="Seg_6509" s="T227">that.[NOM]</ta>
            <ta e="T229" id="Seg_6510" s="T228">skin-2SG-ACC</ta>
            <ta e="T230" id="Seg_6511" s="T229">who-VBZ-PRS-3PL</ta>
            <ta e="T231" id="Seg_6512" s="T230">scraper.for.leather-EP-INSTR</ta>
            <ta e="T232" id="Seg_6513" s="T231">that</ta>
            <ta e="T233" id="Seg_6514" s="T232">who.[NOM]</ta>
            <ta e="T234" id="Seg_6515" s="T233">stay-PTCP.PST</ta>
            <ta e="T235" id="Seg_6516" s="T234">animal.hair-3PL-ACC</ta>
            <ta e="T236" id="Seg_6517" s="T235">take-PRS-3PL</ta>
            <ta e="T238" id="Seg_6518" s="T237">scraper.for.leather-VBZ-PRS-3PL</ta>
            <ta e="T239" id="Seg_6519" s="T238">Dolgan-SIM</ta>
            <ta e="T240" id="Seg_6520" s="T239">speak-PTCP.COND-DAT/LOC</ta>
            <ta e="T242" id="Seg_6521" s="T241">that-3SG-2SG.[NOM]</ta>
            <ta e="T243" id="Seg_6522" s="T242">also</ta>
            <ta e="T244" id="Seg_6523" s="T243">fast.[NOM]</ta>
            <ta e="T245" id="Seg_6524" s="T244">EMPH</ta>
            <ta e="T246" id="Seg_6525" s="T245">be-PST2.NEG.[3SG]</ta>
            <ta e="T247" id="Seg_6526" s="T246">small.[NOM]</ta>
            <ta e="T248" id="Seg_6527" s="T247">EMPH</ta>
            <ta e="T249" id="Seg_6528" s="T248">be-PST2.NEG.[3SG]</ta>
            <ta e="T250" id="Seg_6529" s="T249">woman-PL.[NOM]</ta>
            <ta e="T251" id="Seg_6530" s="T250">powerful</ta>
            <ta e="T252" id="Seg_6531" s="T251">who.[NOM]</ta>
            <ta e="T253" id="Seg_6532" s="T252">heavy</ta>
            <ta e="T254" id="Seg_6533" s="T253">work.[NOM]</ta>
            <ta e="T255" id="Seg_6534" s="T254">that.[NOM]</ta>
            <ta e="T256" id="Seg_6535" s="T255">woman-DAT/LOC</ta>
            <ta e="T257" id="Seg_6536" s="T256">EMPH</ta>
            <ta e="T259" id="Seg_6537" s="T258">that.[NOM]</ta>
            <ta e="T260" id="Seg_6538" s="T259">because.of</ta>
            <ta e="T261" id="Seg_6539" s="T260">once</ta>
            <ta e="T262" id="Seg_6540" s="T261">sit.down-CVB.ANT</ta>
            <ta e="T263" id="Seg_6541" s="T262">that-ACC</ta>
            <ta e="T264" id="Seg_6542" s="T263">whole-3SG-ACC</ta>
            <ta e="T267" id="Seg_6543" s="T266">work-FUT-3PL</ta>
            <ta e="T268" id="Seg_6544" s="T267">NEG-3SG</ta>
            <ta e="T269" id="Seg_6545" s="T268">heavy.[NOM]</ta>
            <ta e="T270" id="Seg_6546" s="T269">work.[NOM]</ta>
            <ta e="T272" id="Seg_6547" s="T271">that-ABL</ta>
            <ta e="T273" id="Seg_6548" s="T272">next</ta>
            <ta e="T274" id="Seg_6549" s="T273">day.[NOM]</ta>
            <ta e="T275" id="Seg_6550" s="T274">again</ta>
            <ta e="T276" id="Seg_6551" s="T275">make-FUT-3PL</ta>
            <ta e="T277" id="Seg_6552" s="T276">that</ta>
            <ta e="T278" id="Seg_6553" s="T277">scraper.for.leather-VBZ-FUT-3PL</ta>
            <ta e="T279" id="Seg_6554" s="T278">again</ta>
            <ta e="T281" id="Seg_6555" s="T280">scraper.for.leather-VBZ-CVB.SEQ</ta>
            <ta e="T282" id="Seg_6556" s="T281">stop-TEMP-3PL</ta>
            <ta e="T283" id="Seg_6557" s="T282">that</ta>
            <ta e="T284" id="Seg_6558" s="T283">thin.layer.of.leather-2SG.[NOM]</ta>
            <ta e="T285" id="Seg_6559" s="T284">who.[NOM]</ta>
            <ta e="T286" id="Seg_6560" s="T285">who-POSS</ta>
            <ta e="T287" id="Seg_6561" s="T286">NEG</ta>
            <ta e="T288" id="Seg_6562" s="T287">become-PRS.[3SG]</ta>
            <ta e="T289" id="Seg_6563" s="T288">EMPH</ta>
            <ta e="T290" id="Seg_6564" s="T289">animal.hair-POSS</ta>
            <ta e="T291" id="Seg_6565" s="T290">NEG</ta>
            <ta e="T292" id="Seg_6566" s="T291">become-PRS.[3SG]</ta>
            <ta e="T294" id="Seg_6567" s="T293">that-3SG-2SG.[NOM]</ta>
            <ta e="T295" id="Seg_6568" s="T294">flat.[NOM]</ta>
            <ta e="T296" id="Seg_6569" s="T295">be-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T297" id="Seg_6570" s="T296">that.[NOM]</ta>
            <ta e="T298" id="Seg_6571" s="T297">scraper-EP-INSTR</ta>
            <ta e="T299" id="Seg_6572" s="T298">what-EP-INSTR</ta>
            <ta e="T300" id="Seg_6573" s="T299">different</ta>
            <ta e="T301" id="Seg_6574" s="T300">different-EP-INSTR</ta>
            <ta e="T302" id="Seg_6575" s="T301">scrape-PRS-3PL</ta>
            <ta e="T303" id="Seg_6576" s="T302">scraper-VBZ-PRS-3PL</ta>
            <ta e="T304" id="Seg_6577" s="T303">and.so.on-PRS-3PL</ta>
            <ta e="T306" id="Seg_6578" s="T305">knife-EP-INSTR</ta>
            <ta e="T307" id="Seg_6579" s="T306">and</ta>
            <ta e="T308" id="Seg_6580" s="T307">different</ta>
            <ta e="T309" id="Seg_6581" s="T308">different</ta>
            <ta e="T310" id="Seg_6582" s="T309">flat-VBZ-PRS-3PL</ta>
            <ta e="T311" id="Seg_6583" s="T310">and.so.on-PRS-3PL</ta>
            <ta e="T313" id="Seg_6584" s="T312">what.[NOM]</ta>
            <ta e="T314" id="Seg_6585" s="T313">bad</ta>
            <ta e="T315" id="Seg_6586" s="T314">there.is-3SG-ACC</ta>
            <ta e="T316" id="Seg_6587" s="T315">whole-3SG-ACC</ta>
            <ta e="T317" id="Seg_6588" s="T316">that.[NOM]</ta>
            <ta e="T318" id="Seg_6589" s="T317">take-MULT-CVB.SEQ</ta>
            <ta e="T319" id="Seg_6590" s="T318">go-PRS-3PL</ta>
            <ta e="T321" id="Seg_6591" s="T320">that</ta>
            <ta e="T322" id="Seg_6592" s="T321">thin.layer.of.leather-2SG.[NOM]</ta>
            <ta e="T323" id="Seg_6593" s="T322">that</ta>
            <ta e="T324" id="Seg_6594" s="T323">make-CVB.SEQ</ta>
            <ta e="T325" id="Seg_6595" s="T324">after</ta>
            <ta e="T326" id="Seg_6596" s="T325">see-PTCP.COND-DAT/LOC</ta>
            <ta e="T327" id="Seg_6597" s="T326">good.[NOM]</ta>
            <ta e="T328" id="Seg_6598" s="T327">very</ta>
            <ta e="T329" id="Seg_6599" s="T328">become-PRS.[3SG]</ta>
            <ta e="T331" id="Seg_6600" s="T330">that-ABL</ta>
            <ta e="T332" id="Seg_6601" s="T331">thin.layer.of.leather-2SG.[NOM]</ta>
            <ta e="T333" id="Seg_6602" s="T332">real</ta>
            <ta e="T334" id="Seg_6603" s="T333">thin.layer.of.leather.[NOM]</ta>
            <ta e="T335" id="Seg_6604" s="T334">be-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T336" id="Seg_6605" s="T335">that</ta>
            <ta e="T337" id="Seg_6606" s="T336">make-CVB.SEQ</ta>
            <ta e="T338" id="Seg_6607" s="T337">EMPH</ta>
            <ta e="T339" id="Seg_6608" s="T338">smoke-PROPR.[NOM]</ta>
            <ta e="T340" id="Seg_6609" s="T339">be-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T341" id="Seg_6610" s="T340">that-3SG-2SG-ACC</ta>
            <ta e="T342" id="Seg_6611" s="T341">again</ta>
            <ta e="T343" id="Seg_6612" s="T342">soften-FUT-NEC-3PL</ta>
            <ta e="T345" id="Seg_6613" s="T344">soften-CVB.PURP</ta>
            <ta e="T346" id="Seg_6614" s="T345">make-TEMP-3PL</ta>
            <ta e="T347" id="Seg_6615" s="T346">well</ta>
            <ta e="T348" id="Seg_6616" s="T347">who-VBZ-PRS-3PL</ta>
            <ta e="T349" id="Seg_6617" s="T348">girl.[NOM]</ta>
            <ta e="T350" id="Seg_6618" s="T349">child-3PL-ACC</ta>
            <ta e="T351" id="Seg_6619" s="T350">invite-HAB-3PL</ta>
            <ta e="T352" id="Seg_6620" s="T351">why</ta>
            <ta e="T353" id="Seg_6621" s="T352">INDEF</ta>
            <ta e="T355" id="Seg_6622" s="T354">that.[NOM]</ta>
            <ta e="T356" id="Seg_6623" s="T355">who-DAT/LOC</ta>
            <ta e="T357" id="Seg_6624" s="T356">clothes.[NOM]</ta>
            <ta e="T358" id="Seg_6625" s="T357">hang.up-MED-PTCP.PRS</ta>
            <ta e="T359" id="Seg_6626" s="T358">who-PL-DAT/LOC</ta>
            <ta e="T360" id="Seg_6627" s="T359">hang.up-PRS-3PL</ta>
            <ta e="T361" id="Seg_6628" s="T360">EMPH</ta>
            <ta e="T362" id="Seg_6629" s="T361">that</ta>
            <ta e="T363" id="Seg_6630" s="T362">skin-2SG-ACC</ta>
            <ta e="T364" id="Seg_6631" s="T363">clothes.[NOM]</ta>
            <ta e="T365" id="Seg_6632" s="T364">hang.up-MED-PTCP.PRS.[NOM]</ta>
            <ta e="T367" id="Seg_6633" s="T366">that-3SG-2SG-ACC</ta>
            <ta e="T368" id="Seg_6634" s="T367">pull.down-CVB.SEQ</ta>
            <ta e="T369" id="Seg_6635" s="T368">stand-PTCP.FUT-2SG-ACC</ta>
            <ta e="T370" id="Seg_6636" s="T369">need.to</ta>
            <ta e="T372" id="Seg_6637" s="T371">sledge-ACC</ta>
            <ta e="T373" id="Seg_6638" s="T372">turn.inside.out-CVB.SIM</ta>
            <ta e="T374" id="Seg_6639" s="T373">lay-CVB.SEQ</ta>
            <ta e="T375" id="Seg_6640" s="T374">go-CVB.SEQ-3PL</ta>
            <ta e="T376" id="Seg_6641" s="T375">again</ta>
            <ta e="T377" id="Seg_6642" s="T376">pull.down-CVB.SEQ</ta>
            <ta e="T378" id="Seg_6643" s="T377">stand-PTCP.FUT-2SG-ACC</ta>
            <ta e="T379" id="Seg_6644" s="T378">need.to</ta>
            <ta e="T380" id="Seg_6645" s="T379">that</ta>
            <ta e="T381" id="Seg_6646" s="T380">fur-ACC</ta>
            <ta e="T382" id="Seg_6647" s="T381">lay-CVB.SEQ</ta>
            <ta e="T383" id="Seg_6648" s="T382">go-CVB.SEQ-2SG</ta>
            <ta e="T384" id="Seg_6649" s="T383">thin.layer.of.leather.[NOM]</ta>
            <ta e="T385" id="Seg_6650" s="T384">become-PTCP.FUT-3SG-ACC</ta>
            <ta e="T387" id="Seg_6651" s="T386">well</ta>
            <ta e="T388" id="Seg_6652" s="T387">that</ta>
            <ta e="T389" id="Seg_6653" s="T388">pull.down-CVB.SEQ</ta>
            <ta e="T390" id="Seg_6654" s="T389">stand-PRS-2SG</ta>
            <ta e="T392" id="Seg_6655" s="T391">that</ta>
            <ta e="T393" id="Seg_6656" s="T392">pull.down-CVB.SEQ</ta>
            <ta e="T394" id="Seg_6657" s="T393">stand-TEMP-2SG</ta>
            <ta e="T395" id="Seg_6658" s="T394">2SG-DAT/LOC</ta>
            <ta e="T396" id="Seg_6659" s="T395">give-PRS-3PL</ta>
            <ta e="T397" id="Seg_6660" s="T396">that.[NOM]</ta>
            <ta e="T398" id="Seg_6661" s="T397">who-ACC</ta>
            <ta e="T399" id="Seg_6662" s="T398">liver-ACC</ta>
            <ta e="T400" id="Seg_6663" s="T399">boil-PTCP.PST</ta>
            <ta e="T401" id="Seg_6664" s="T400">reindeer.[NOM]</ta>
            <ta e="T402" id="Seg_6665" s="T401">liver-3SG-ACC</ta>
            <ta e="T403" id="Seg_6666" s="T402">that</ta>
            <ta e="T404" id="Seg_6667" s="T403">liver-ACC</ta>
            <ta e="T405" id="Seg_6668" s="T404">weigh-CVB.SIM</ta>
            <ta e="T406" id="Seg_6669" s="T405">stand-CVB.SEQ</ta>
            <ta e="T407" id="Seg_6670" s="T406">girl.[NOM]</ta>
            <ta e="T408" id="Seg_6671" s="T407">girl.[NOM]</ta>
            <ta e="T409" id="Seg_6672" s="T408">child.[NOM]</ta>
            <ta e="T411" id="Seg_6673" s="T410">chew-FUT-NEC.[3SG]</ta>
            <ta e="T412" id="Seg_6674" s="T411">chew.[IMP.2SG]</ta>
            <ta e="T413" id="Seg_6675" s="T412">say-PRS-3PL</ta>
            <ta e="T414" id="Seg_6676" s="T413">EMPH</ta>
            <ta e="T415" id="Seg_6677" s="T414">whereto</ta>
            <ta e="T416" id="Seg_6678" s="T415">be-FUT-2SG=Q</ta>
            <ta e="T417" id="Seg_6679" s="T416">well</ta>
            <ta e="T419" id="Seg_6680" s="T418">want-NEG.PTCP</ta>
            <ta e="T420" id="Seg_6681" s="T419">be-FUT.[3SG]</ta>
            <ta e="T421" id="Seg_6682" s="T420">probably</ta>
            <ta e="T422" id="Seg_6683" s="T421">that-3SG-2SG.[NOM]</ta>
            <ta e="T423" id="Seg_6684" s="T422">taste-3SG.[NOM]</ta>
            <ta e="T424" id="Seg_6685" s="T423">badly</ta>
            <ta e="T425" id="Seg_6686" s="T424">however</ta>
            <ta e="T426" id="Seg_6687" s="T425">stand-PRS-2SG</ta>
            <ta e="T427" id="Seg_6688" s="T426">EMPH</ta>
            <ta e="T429" id="Seg_6689" s="T428">hear-PRS-2SG</ta>
            <ta e="T430" id="Seg_6690" s="T429">EMPH</ta>
            <ta e="T431" id="Seg_6691" s="T430">mother-2SG.[NOM]</ta>
            <ta e="T432" id="Seg_6692" s="T431">word-3SG-ACC</ta>
            <ta e="T434" id="Seg_6693" s="T433">that</ta>
            <ta e="T435" id="Seg_6694" s="T434">stand-CVB.SIM</ta>
            <ta e="T436" id="Seg_6695" s="T435">stand-CVB.SIM</ta>
            <ta e="T437" id="Seg_6696" s="T436">chew-CVB.SIM</ta>
            <ta e="T438" id="Seg_6697" s="T437">stand-PRS-2SG</ta>
            <ta e="T439" id="Seg_6698" s="T438">that</ta>
            <ta e="T440" id="Seg_6699" s="T439">who-ACC</ta>
            <ta e="T441" id="Seg_6700" s="T440">liver-ACC</ta>
            <ta e="T442" id="Seg_6701" s="T441">stand-CVB.SEQ-2SG</ta>
            <ta e="T444" id="Seg_6702" s="T443">3PL-EP-2SG.[NOM]</ta>
            <ta e="T445" id="Seg_6703" s="T444">that</ta>
            <ta e="T447" id="Seg_6704" s="T446">that</ta>
            <ta e="T448" id="Seg_6705" s="T447">chew-PTCP.PST</ta>
            <ta e="T449" id="Seg_6706" s="T448">liver-2SG-ACC</ta>
            <ta e="T450" id="Seg_6707" s="T449">take-CVB.SIM</ta>
            <ta e="T451" id="Seg_6708" s="T450">take-PRS-3PL</ta>
            <ta e="T452" id="Seg_6709" s="T451">that</ta>
            <ta e="T453" id="Seg_6710" s="T452">skin-2SG-ACC</ta>
            <ta e="T454" id="Seg_6711" s="T453">that</ta>
            <ta e="T455" id="Seg_6712" s="T454">who-VBZ-PTCP.PST</ta>
            <ta e="T456" id="Seg_6713" s="T455">scraper.for.leather-VBZ-PTCP.PST</ta>
            <ta e="T457" id="Seg_6714" s="T456">skin-2SG-ACC</ta>
            <ta e="T458" id="Seg_6715" s="T457">take-CVB.SEQ</ta>
            <ta e="T459" id="Seg_6716" s="T458">go-CVB.SEQ-3PL</ta>
            <ta e="T460" id="Seg_6717" s="T459">that</ta>
            <ta e="T461" id="Seg_6718" s="T460">make.dirty-PRS-3PL</ta>
            <ta e="T463" id="Seg_6719" s="T462">that</ta>
            <ta e="T464" id="Seg_6720" s="T463">make-CVB.SEQ</ta>
            <ta e="T465" id="Seg_6721" s="T464">after</ta>
            <ta e="T467" id="Seg_6722" s="T466">scraper.with.toothed.end-EP-INSTR</ta>
            <ta e="T468" id="Seg_6723" s="T467">scraper.with.toothed.end-VBZ-PRS-3PL</ta>
            <ta e="T469" id="Seg_6724" s="T468">hand-3PL-INSTR</ta>
            <ta e="T471" id="Seg_6725" s="T470">one</ta>
            <ta e="T472" id="Seg_6726" s="T471">hand-INSTR</ta>
            <ta e="T473" id="Seg_6727" s="T472">hold-PRS.[3SG]</ta>
            <ta e="T474" id="Seg_6728" s="T473">that</ta>
            <ta e="T475" id="Seg_6729" s="T474">skin-2SG-ACC</ta>
            <ta e="T476" id="Seg_6730" s="T475">one</ta>
            <ta e="T477" id="Seg_6731" s="T476">hand-INSTR</ta>
            <ta e="T478" id="Seg_6732" s="T477">scraper.with.toothed.end-2SG-ACC</ta>
            <ta e="T479" id="Seg_6733" s="T478">hold-PRS.[3SG]</ta>
            <ta e="T480" id="Seg_6734" s="T479">EMPH</ta>
            <ta e="T481" id="Seg_6735" s="T480">that</ta>
            <ta e="T482" id="Seg_6736" s="T481">EMPH</ta>
            <ta e="T483" id="Seg_6737" s="T482">up-ABL</ta>
            <ta e="T484" id="Seg_6738" s="T483">down-DAT/LOC</ta>
            <ta e="T485" id="Seg_6739" s="T484">until</ta>
            <ta e="T486" id="Seg_6740" s="T485">so</ta>
            <ta e="T487" id="Seg_6741" s="T486">scraper.with.toothed.end-VBZ-PRS.[3SG]</ta>
            <ta e="T488" id="Seg_6742" s="T487">eh</ta>
            <ta e="T489" id="Seg_6743" s="T488">make.dirty-CVB.SIM</ta>
            <ta e="T490" id="Seg_6744" s="T489">make.dirty-CVB.SIM</ta>
            <ta e="T492" id="Seg_6745" s="T491">well</ta>
            <ta e="T493" id="Seg_6746" s="T492">that</ta>
            <ta e="T494" id="Seg_6747" s="T493">make-CVB.SEQ</ta>
            <ta e="T495" id="Seg_6748" s="T494">after</ta>
            <ta e="T496" id="Seg_6749" s="T495">3PL-EP-2SG.[NOM]</ta>
            <ta e="T497" id="Seg_6750" s="T496">girl.[NOM]</ta>
            <ta e="T498" id="Seg_6751" s="T497">child-PL.[NOM]</ta>
            <ta e="T499" id="Seg_6752" s="T498">when</ta>
            <ta e="T500" id="Seg_6753" s="T499">NEG</ta>
            <ta e="T501" id="Seg_6754" s="T500">stand-PTCP.FUT-3PL-ACC</ta>
            <ta e="T502" id="Seg_6755" s="T501">want-PTCP.HAB-3SG</ta>
            <ta e="T503" id="Seg_6756" s="T502">no-3PL</ta>
            <ta e="T505" id="Seg_6757" s="T504">pull.down-CVB.SEQ</ta>
            <ta e="T506" id="Seg_6758" s="T505">stand-PTCP.FUT-2SG-ACC</ta>
            <ta e="T507" id="Seg_6759" s="T506">need.to</ta>
            <ta e="T508" id="Seg_6760" s="T507">EMPH</ta>
            <ta e="T509" id="Seg_6761" s="T508">long</ta>
            <ta e="T510" id="Seg_6762" s="T509">very</ta>
            <ta e="T511" id="Seg_6763" s="T510">stand-PTCP.FUT-2SG-ACC</ta>
            <ta e="T513" id="Seg_6764" s="T512">stand-FUT-NEC-2SG</ta>
            <ta e="T514" id="Seg_6765" s="T513">there</ta>
            <ta e="T516" id="Seg_6766" s="T515">and</ta>
            <ta e="T517" id="Seg_6767" s="T516">chew-PTCP.PRS-EP-2SG.[NOM]</ta>
            <ta e="T518" id="Seg_6768" s="T517">there.is</ta>
            <ta e="T519" id="Seg_6769" s="T518">well</ta>
            <ta e="T520" id="Seg_6770" s="T519">bad.[NOM]</ta>
            <ta e="T521" id="Seg_6771" s="T520">and</ta>
            <ta e="T522" id="Seg_6772" s="T521">bad.[NOM]</ta>
            <ta e="T524" id="Seg_6773" s="T523">AFFIRM</ta>
            <ta e="T525" id="Seg_6774" s="T524">chew-FUT-2SG</ta>
            <ta e="T526" id="Seg_6775" s="T525">human.being.[NOM]</ta>
            <ta e="T527" id="Seg_6776" s="T526">EMPH</ta>
            <ta e="T528" id="Seg_6777" s="T527">heart-3SG.[NOM]</ta>
            <ta e="T529" id="Seg_6778" s="T528">be.sick-FUT.[3SG]</ta>
            <ta e="T530" id="Seg_6779" s="T529">sometimes</ta>
            <ta e="T531" id="Seg_6780" s="T530">be-PRS.[3SG]</ta>
            <ta e="T532" id="Seg_6781" s="T531">EMPH</ta>
            <ta e="T534" id="Seg_6782" s="T533">that.[NOM]</ta>
            <ta e="T535" id="Seg_6783" s="T534">because.of</ta>
            <ta e="T537" id="Seg_6784" s="T536">girl.[NOM]</ta>
            <ta e="T538" id="Seg_6785" s="T537">child-DIM-PL-EP-2SG.[NOM]</ta>
            <ta e="T539" id="Seg_6786" s="T538">run.away-PRS-3PL</ta>
            <ta e="T540" id="Seg_6787" s="T539">EMPH</ta>
            <ta e="T541" id="Seg_6788" s="T540">chew-NEG-CVB.PURP-3PL</ta>
            <ta e="T542" id="Seg_6789" s="T541">go-CVB.SEQ</ta>
            <ta e="T543" id="Seg_6790" s="T542">stay-PRS-3PL</ta>
            <ta e="T545" id="Seg_6791" s="T544">who.[NOM]</ta>
            <ta e="T546" id="Seg_6792" s="T545">girl.[NOM]</ta>
            <ta e="T547" id="Seg_6793" s="T546">child-PL-ACC</ta>
            <ta e="T548" id="Seg_6794" s="T547">many-ADVZ</ta>
            <ta e="T549" id="Seg_6795" s="T548">call-HAB-3PL</ta>
            <ta e="T550" id="Seg_6796" s="T549">boy.[NOM]</ta>
            <ta e="T551" id="Seg_6797" s="T550">child-COMP</ta>
            <ta e="T552" id="Seg_6798" s="T551">why</ta>
            <ta e="T553" id="Seg_6799" s="T552">INDEF</ta>
            <ta e="T555" id="Seg_6800" s="T554">well</ta>
            <ta e="T556" id="Seg_6801" s="T555">that</ta>
            <ta e="T557" id="Seg_6802" s="T556">that</ta>
            <ta e="T558" id="Seg_6803" s="T557">make-CVB.SEQ</ta>
            <ta e="T559" id="Seg_6804" s="T558">after</ta>
            <ta e="T560" id="Seg_6805" s="T559">that</ta>
            <ta e="T561" id="Seg_6806" s="T560">who-EP-INSTR</ta>
            <ta e="T562" id="Seg_6807" s="T561">enough</ta>
            <ta e="T563" id="Seg_6808" s="T562">who-PART</ta>
            <ta e="T564" id="Seg_6809" s="T563">scraper.with.toothed.end-EP-INSTR</ta>
            <ta e="T565" id="Seg_6810" s="T564">enough</ta>
            <ta e="T566" id="Seg_6811" s="T565">who-VBZ-CVB.SIM</ta>
            <ta e="T567" id="Seg_6812" s="T566">who-VBZ-PST1-3PL</ta>
            <ta e="T568" id="Seg_6813" s="T567">EMPH</ta>
            <ta e="T569" id="Seg_6814" s="T568">that</ta>
            <ta e="T570" id="Seg_6815" s="T569">skin-2SG-ACC</ta>
            <ta e="T572" id="Seg_6816" s="T571">that</ta>
            <ta e="T573" id="Seg_6817" s="T572">scraper.with.toothed.end-VBZ-PTCP.PRS-EP-2SG.[NOM]</ta>
            <ta e="T574" id="Seg_6818" s="T573">again</ta>
            <ta e="T575" id="Seg_6819" s="T574">long</ta>
            <ta e="T576" id="Seg_6820" s="T575">very</ta>
            <ta e="T577" id="Seg_6821" s="T576">be-PST1-3SG</ta>
            <ta e="T579" id="Seg_6822" s="T578">next</ta>
            <ta e="T580" id="Seg_6823" s="T579">day.[NOM]</ta>
            <ta e="T581" id="Seg_6824" s="T580">again</ta>
            <ta e="T582" id="Seg_6825" s="T581">stand-FUT-NEC-2SG</ta>
            <ta e="T583" id="Seg_6826" s="T582">MOD</ta>
            <ta e="T584" id="Seg_6827" s="T583">that</ta>
            <ta e="T585" id="Seg_6828" s="T584">chew-CVB.SIM</ta>
            <ta e="T586" id="Seg_6829" s="T585">chew-PRS-2SG</ta>
            <ta e="T588" id="Seg_6830" s="T587">want-NEG.PTCP</ta>
            <ta e="T589" id="Seg_6831" s="T588">be-PRS-2SG</ta>
            <ta e="T590" id="Seg_6832" s="T589">heart-EP-2SG.[NOM]</ta>
            <ta e="T591" id="Seg_6833" s="T590">be.sick-PRS.[3SG]</ta>
            <ta e="T592" id="Seg_6834" s="T591">MOD</ta>
            <ta e="T594" id="Seg_6835" s="T593">well</ta>
            <ta e="T595" id="Seg_6836" s="T594">again</ta>
            <ta e="T596" id="Seg_6837" s="T595">however</ta>
            <ta e="T597" id="Seg_6838" s="T596">chew-PRS-2SG</ta>
            <ta e="T598" id="Seg_6839" s="T597">MOD</ta>
            <ta e="T599" id="Seg_6840" s="T598">whereto</ta>
            <ta e="T600" id="Seg_6841" s="T599">be-FUT-2SG=Q</ta>
            <ta e="T601" id="Seg_6842" s="T600">mother-2SG.[NOM]</ta>
            <ta e="T602" id="Seg_6843" s="T601">word-3SG-ACC</ta>
            <ta e="T603" id="Seg_6844" s="T602">hear-PRS-2SG</ta>
            <ta e="T604" id="Seg_6845" s="T603">EMPH</ta>
            <ta e="T606" id="Seg_6846" s="T605">well</ta>
            <ta e="T607" id="Seg_6847" s="T606">that</ta>
            <ta e="T608" id="Seg_6848" s="T607">stand-CVB.SEQ</ta>
            <ta e="T609" id="Seg_6849" s="T608">chew-CVB.SIM</ta>
            <ta e="T610" id="Seg_6850" s="T609">chew-PRS-2SG</ta>
            <ta e="T611" id="Seg_6851" s="T610">well</ta>
            <ta e="T612" id="Seg_6852" s="T611">misery-VBZ-CVB.SEQ</ta>
            <ta e="T613" id="Seg_6853" s="T612">misery-VBZ-CVB.SEQ</ta>
            <ta e="T614" id="Seg_6854" s="T613">well</ta>
            <ta e="T615" id="Seg_6855" s="T614">stop-PRS-1PL</ta>
            <ta e="T616" id="Seg_6856" s="T615">MOD</ta>
            <ta e="T618" id="Seg_6857" s="T617">how.much-ORD</ta>
            <ta e="T619" id="Seg_6858" s="T618">INDEF</ta>
            <ta e="T620" id="Seg_6859" s="T619">day-3SG-DAT/LOC</ta>
            <ta e="T621" id="Seg_6860" s="T620">well</ta>
            <ta e="T622" id="Seg_6861" s="T621">that</ta>
            <ta e="T623" id="Seg_6862" s="T622">scraper.[NOM]</ta>
            <ta e="T624" id="Seg_6863" s="T623">who-EP-INSTR</ta>
            <ta e="T625" id="Seg_6864" s="T624">scraper.with.toothed.end-INSTR</ta>
            <ta e="T627" id="Seg_6865" s="T626">that-ABL</ta>
            <ta e="T628" id="Seg_6866" s="T627">later</ta>
            <ta e="T629" id="Seg_6867" s="T628">that.[NOM]</ta>
            <ta e="T630" id="Seg_6868" s="T629">thin.layer.of.leather.[NOM]</ta>
            <ta e="T631" id="Seg_6869" s="T630">EMPH</ta>
            <ta e="T632" id="Seg_6870" s="T631">completely</ta>
            <ta e="T633" id="Seg_6871" s="T632">thin.layer.of.leather</ta>
            <ta e="T634" id="Seg_6872" s="T633">EMPH</ta>
            <ta e="T635" id="Seg_6873" s="T634">good.[NOM]</ta>
            <ta e="T636" id="Seg_6874" s="T635">be-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T637" id="Seg_6875" s="T636">soft.[NOM]</ta>
            <ta e="T638" id="Seg_6876" s="T637">be-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T639" id="Seg_6877" s="T638">that.[NOM]</ta>
            <ta e="T640" id="Seg_6878" s="T639">different</ta>
            <ta e="T641" id="Seg_6879" s="T640">different-ADVZ</ta>
            <ta e="T642" id="Seg_6880" s="T641">that.[NOM]</ta>
            <ta e="T643" id="Seg_6881" s="T642">who-VBZ-CVB.SIM</ta>
            <ta e="T644" id="Seg_6882" s="T643">can-PST1-1PL</ta>
            <ta e="T645" id="Seg_6883" s="T644">EMPH</ta>
            <ta e="T647" id="Seg_6884" s="T646">scraper-ABL</ta>
            <ta e="T648" id="Seg_6885" s="T647">scraper.for.leather-INSTR</ta>
            <ta e="T649" id="Seg_6886" s="T648">make-PRS-1PL</ta>
            <ta e="T651" id="Seg_6887" s="T650">that-3SG-2SG.[NOM]</ta>
            <ta e="T652" id="Seg_6888" s="T651">thin.layer.of.leather.[NOM]</ta>
            <ta e="T653" id="Seg_6889" s="T652">very</ta>
            <ta e="T654" id="Seg_6890" s="T653">thin</ta>
            <ta e="T655" id="Seg_6891" s="T654">be-CVB.PURP</ta>
            <ta e="T656" id="Seg_6892" s="T655">also</ta>
            <ta e="T657" id="Seg_6893" s="T656">however</ta>
            <ta e="T658" id="Seg_6894" s="T657">bad.[NOM]</ta>
            <ta e="T660" id="Seg_6895" s="T659">that.[NOM]</ta>
            <ta e="T661" id="Seg_6896" s="T660">because.of</ta>
            <ta e="T662" id="Seg_6897" s="T661">thin.layer.of.leather-2SG-ACC</ta>
            <ta e="T663" id="Seg_6898" s="T662">that.[NOM]</ta>
            <ta e="T664" id="Seg_6899" s="T663">who-DAT/LOC</ta>
            <ta e="T665" id="Seg_6900" s="T664">smoke-PRS-3PL</ta>
            <ta e="T666" id="Seg_6901" s="T665">smoke-PRS-3PL</ta>
            <ta e="T667" id="Seg_6902" s="T666">smoke-DAT/LOC</ta>
            <ta e="T669" id="Seg_6903" s="T668">that</ta>
            <ta e="T670" id="Seg_6904" s="T669">thin.layer.of.leather-2SG-ACC</ta>
            <ta e="T671" id="Seg_6905" s="T670">take-CVB.SEQ</ta>
            <ta e="T672" id="Seg_6906" s="T671">go-CVB.SEQ-3PL</ta>
            <ta e="T673" id="Seg_6907" s="T672">house.[NOM]</ta>
            <ta e="T674" id="Seg_6908" s="T673">inside-3SG-DAT/LOC</ta>
            <ta e="T675" id="Seg_6909" s="T674">hang.up-PRS-3PL</ta>
            <ta e="T677" id="Seg_6910" s="T676">house.[NOM]</ta>
            <ta e="T678" id="Seg_6911" s="T677">and</ta>
            <ta e="T679" id="Seg_6912" s="T678">be-FUT.[3SG]=Q</ta>
            <ta e="T680" id="Seg_6913" s="T679">that.[NOM]</ta>
            <ta e="T681" id="Seg_6914" s="T680">who.[NOM]</ta>
            <ta e="T682" id="Seg_6915" s="T681">pole.tent.[NOM]</ta>
            <ta e="T683" id="Seg_6916" s="T682">inside-3SG-DAT/LOC</ta>
            <ta e="T684" id="Seg_6917" s="T683">pole.tent.[NOM]</ta>
            <ta e="T685" id="Seg_6918" s="T684">house.[NOM]</ta>
            <ta e="T686" id="Seg_6919" s="T685">inside-3SG-DAT/LOC</ta>
            <ta e="T687" id="Seg_6920" s="T686">hang.up-PRS-3PL</ta>
            <ta e="T689" id="Seg_6921" s="T688">there</ta>
            <ta e="T690" id="Seg_6922" s="T689">smoke-DAT/LOC</ta>
            <ta e="T691" id="Seg_6923" s="T690">that-3SG-2SG.[NOM]</ta>
            <ta e="T692" id="Seg_6924" s="T691">proximity.[NOM]</ta>
            <ta e="T693" id="Seg_6925" s="T692">stand-PRS.[3SG]</ta>
            <ta e="T694" id="Seg_6926" s="T693">EMPH</ta>
            <ta e="T696" id="Seg_6927" s="T695">and</ta>
            <ta e="T697" id="Seg_6928" s="T696">some</ta>
            <ta e="T698" id="Seg_6929" s="T697">woman-PL.[NOM]</ta>
            <ta e="T699" id="Seg_6930" s="T698">so</ta>
            <ta e="T700" id="Seg_6931" s="T699">make-HAB-3PL</ta>
            <ta e="T701" id="Seg_6932" s="T700">MOD</ta>
            <ta e="T703" id="Seg_6933" s="T702">that.[NOM]</ta>
            <ta e="T704" id="Seg_6934" s="T703">similar</ta>
            <ta e="T705" id="Seg_6935" s="T704">and</ta>
            <ta e="T706" id="Seg_6936" s="T705">smoke-CVB.SIM</ta>
            <ta e="T707" id="Seg_6937" s="T706">lie-PTCP.FUT</ta>
            <ta e="T708" id="Seg_6938" s="T707">be-PST2-3PL</ta>
            <ta e="T709" id="Seg_6939" s="T708">EMPH</ta>
            <ta e="T711" id="Seg_6940" s="T710">and</ta>
            <ta e="T712" id="Seg_6941" s="T711">3SG.[NOM]</ta>
            <ta e="T713" id="Seg_6942" s="T712">3PL-EP-2SG.[NOM]</ta>
            <ta e="T714" id="Seg_6943" s="T713">just.like.this</ta>
            <ta e="T715" id="Seg_6944" s="T714">say-PRS-3PL</ta>
            <ta e="T716" id="Seg_6945" s="T715">EMPH</ta>
            <ta e="T717" id="Seg_6946" s="T716">1PL-ACC</ta>
            <ta e="T718" id="Seg_6947" s="T717">stand-CAUS-PRS-3PL</ta>
            <ta e="T719" id="Seg_6948" s="T718">EMPH</ta>
            <ta e="T720" id="Seg_6949" s="T719">three</ta>
            <ta e="T721" id="Seg_6950" s="T720">four</ta>
            <ta e="T722" id="Seg_6951" s="T721">girl.[NOM]</ta>
            <ta e="T723" id="Seg_6952" s="T722">girl.[NOM]</ta>
            <ta e="T724" id="Seg_6953" s="T723">child-PL.[NOM]</ta>
            <ta e="T868" id="Seg_6954" s="T724">who.[NOM]</ta>
            <ta e="T869" id="Seg_6955" s="T868">who.[NOM]</ta>
            <ta e="T870" id="Seg_6956" s="T869">fire-3SG.[NOM]</ta>
            <ta e="T871" id="Seg_6957" s="T870">who.[NOM]</ta>
            <ta e="T872" id="Seg_6958" s="T871">fire-3SG.[NOM]</ta>
            <ta e="T873" id="Seg_6959" s="T872">that.[NOM]</ta>
            <ta e="T874" id="Seg_6960" s="T873">stove.[NOM]</ta>
            <ta e="T725" id="Seg_6961" s="T874">make-PRS-3PL</ta>
            <ta e="T726" id="Seg_6962" s="T725">wood.[NOM]</ta>
            <ta e="T727" id="Seg_6963" s="T726">flame.up-PRS.[3SG]</ta>
            <ta e="T728" id="Seg_6964" s="T727">that-3SG-2SG.[NOM]</ta>
            <ta e="T729" id="Seg_6965" s="T728">upper.part-3SG-INSTR</ta>
            <ta e="T730" id="Seg_6966" s="T729">grass-PL-ACC</ta>
            <ta e="T731" id="Seg_6967" s="T730">throw-PRS-3PL</ta>
            <ta e="T732" id="Seg_6968" s="T731">big</ta>
            <ta e="T733" id="Seg_6969" s="T732">smoke.[NOM]</ta>
            <ta e="T734" id="Seg_6970" s="T733">come-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T736" id="Seg_6971" s="T735">well</ta>
            <ta e="T737" id="Seg_6972" s="T736">that</ta>
            <ta e="T738" id="Seg_6973" s="T737">smoke.[NOM]</ta>
            <ta e="T739" id="Seg_6974" s="T738">inside-3SG-DAT/LOC</ta>
            <ta e="T740" id="Seg_6975" s="T739">around</ta>
            <ta e="T741" id="Seg_6976" s="T740">stand-CVB.SEQ-1PL</ta>
            <ta e="T742" id="Seg_6977" s="T741">that</ta>
            <ta e="T743" id="Seg_6978" s="T742">hold-PRS-1PL</ta>
            <ta e="T744" id="Seg_6979" s="T743">MOD</ta>
            <ta e="T745" id="Seg_6980" s="T744">who-ACC</ta>
            <ta e="T747" id="Seg_6981" s="T746">that</ta>
            <ta e="T748" id="Seg_6982" s="T747">make-CVB.SEQ</ta>
            <ta e="T749" id="Seg_6983" s="T748">after</ta>
            <ta e="T750" id="Seg_6984" s="T749">turn-CAUS-PRS-1PL</ta>
            <ta e="T751" id="Seg_6985" s="T750">sun.[NOM]</ta>
            <ta e="T752" id="Seg_6986" s="T751">trace-3SG-ACC</ta>
            <ta e="T753" id="Seg_6987" s="T752">similar</ta>
            <ta e="T754" id="Seg_6988" s="T753">turn-CAUS-PRS-1PL</ta>
            <ta e="T756" id="Seg_6989" s="T755">good-ADVZ</ta>
            <ta e="T757" id="Seg_6990" s="T756">hold-FUT-NEC-2SG</ta>
            <ta e="T758" id="Seg_6991" s="T757">that-3SG-2SG-ACC</ta>
            <ta e="T759" id="Seg_6992" s="T758">release-NEG.PTCP.[NOM]</ta>
            <ta e="T760" id="Seg_6993" s="T759">similar</ta>
            <ta e="T762" id="Seg_6994" s="T761">self</ta>
            <ta e="T763" id="Seg_6995" s="T762">self-1PL-DAT/LOC</ta>
            <ta e="T764" id="Seg_6996" s="T763">turn-CAUS-PRS-1PL</ta>
            <ta e="T765" id="Seg_6997" s="T764">maybe</ta>
            <ta e="T766" id="Seg_6998" s="T765">just.like.this</ta>
            <ta e="T768" id="Seg_6999" s="T767">1SG.[NOM]</ta>
            <ta e="T769" id="Seg_7000" s="T768">friend-1SG-DAT/LOC</ta>
            <ta e="T770" id="Seg_7001" s="T769">give-FUT-1SG</ta>
            <ta e="T771" id="Seg_7002" s="T770">friend-EP-1SG.[NOM]</ta>
            <ta e="T772" id="Seg_7003" s="T771">next</ta>
            <ta e="T773" id="Seg_7004" s="T772">friend-2SG-DAT/LOC</ta>
            <ta e="T774" id="Seg_7005" s="T773">like.that-INTNS</ta>
            <ta e="T775" id="Seg_7006" s="T774">give-PRS.[3SG]</ta>
            <ta e="T777" id="Seg_7007" s="T776">go.around-CAUS-PRS-1PL</ta>
            <ta e="T778" id="Seg_7008" s="T777">that</ta>
            <ta e="T779" id="Seg_7009" s="T778">fur-ACC</ta>
            <ta e="T781" id="Seg_7010" s="T780">lower.part-2PL-ABL</ta>
            <ta e="T782" id="Seg_7011" s="T781">big</ta>
            <ta e="T783" id="Seg_7012" s="T782">smoke.[NOM]</ta>
            <ta e="T784" id="Seg_7013" s="T783">come-PRS.[3SG]</ta>
            <ta e="T785" id="Seg_7014" s="T784">EMPH</ta>
            <ta e="T787" id="Seg_7015" s="T786">1PL-DAT/LOC</ta>
            <ta e="T788" id="Seg_7016" s="T787">again</ta>
            <ta e="T789" id="Seg_7017" s="T788">bad</ta>
            <ta e="T790" id="Seg_7018" s="T789">that</ta>
            <ta e="T791" id="Seg_7019" s="T790">smoke.[NOM]</ta>
            <ta e="T792" id="Seg_7020" s="T791">come-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T794" id="Seg_7021" s="T793">eye-1PL.[NOM]</ta>
            <ta e="T795" id="Seg_7022" s="T794">sting-PRS.[3SG]</ta>
            <ta e="T796" id="Seg_7023" s="T795">MOD</ta>
            <ta e="T797" id="Seg_7024" s="T796">that</ta>
            <ta e="T798" id="Seg_7025" s="T797">smoke-ABL</ta>
            <ta e="T800" id="Seg_7026" s="T799">mouth-1PL-DAT/LOC</ta>
            <ta e="T801" id="Seg_7027" s="T800">and</ta>
            <ta e="T802" id="Seg_7028" s="T801">take-PRS-1PL</ta>
            <ta e="T803" id="Seg_7029" s="T802">MOD</ta>
            <ta e="T804" id="Seg_7030" s="T803">smoke-ACC</ta>
            <ta e="T806" id="Seg_7031" s="T805">well</ta>
            <ta e="T807" id="Seg_7032" s="T806">that</ta>
            <ta e="T808" id="Seg_7033" s="T807">and</ta>
            <ta e="T809" id="Seg_7034" s="T808">be-COND.[3SG]</ta>
            <ta e="T810" id="Seg_7035" s="T809">well</ta>
            <ta e="T811" id="Seg_7036" s="T810">stand-EP-REFL-FREQ-PRS-1PL</ta>
            <ta e="T812" id="Seg_7037" s="T811">EMPH</ta>
            <ta e="T813" id="Seg_7038" s="T812">stand-EP-IMP.2PL</ta>
            <ta e="T814" id="Seg_7039" s="T813">stand-EP-IMP.2PL</ta>
            <ta e="T815" id="Seg_7040" s="T814">say-PRS-3PL</ta>
            <ta e="T816" id="Seg_7041" s="T815">EMPH</ta>
            <ta e="T817" id="Seg_7042" s="T816">1PL-ACC</ta>
            <ta e="T819" id="Seg_7043" s="T818">well</ta>
            <ta e="T820" id="Seg_7044" s="T819">that.[NOM]</ta>
            <ta e="T821" id="Seg_7045" s="T820">similar</ta>
            <ta e="T822" id="Seg_7046" s="T821">turn-CAUS-PRS-1PL</ta>
            <ta e="T824" id="Seg_7047" s="T823">that-ABL</ta>
            <ta e="T825" id="Seg_7048" s="T824">one</ta>
            <ta e="T826" id="Seg_7049" s="T825">INDEF</ta>
            <ta e="T827" id="Seg_7050" s="T826">girl.[NOM]</ta>
            <ta e="T828" id="Seg_7051" s="T827">child-3SG.[NOM]</ta>
            <ta e="T829" id="Seg_7052" s="T828">fur-ACC</ta>
            <ta e="T830" id="Seg_7053" s="T829">hand-3SG-INSTR</ta>
            <ta e="T831" id="Seg_7054" s="T830">just.like.this-EP-2SG</ta>
            <ta e="T832" id="Seg_7055" s="T831">take-CVB.SEQ</ta>
            <ta e="T833" id="Seg_7056" s="T832">throw-FUT.[3SG]</ta>
            <ta e="T834" id="Seg_7057" s="T833">mistake</ta>
            <ta e="T835" id="Seg_7058" s="T834">hold-FUT.[3SG]</ta>
            <ta e="T836" id="Seg_7059" s="T835">that-3SG-3SG-ACC</ta>
            <ta e="T837" id="Seg_7060" s="T836">that</ta>
            <ta e="T838" id="Seg_7061" s="T837">hold-PTCP.PRS</ta>
            <ta e="T839" id="Seg_7062" s="T838">place-3SG-ACC</ta>
            <ta e="T840" id="Seg_7063" s="T839">skin-3SG-ACC</ta>
            <ta e="T841" id="Seg_7064" s="T840">well</ta>
            <ta e="T842" id="Seg_7065" s="T841">then</ta>
            <ta e="T843" id="Seg_7066" s="T842">say-PRS-3PL</ta>
            <ta e="T844" id="Seg_7067" s="T843">EMPH</ta>
            <ta e="T845" id="Seg_7068" s="T844">well</ta>
            <ta e="T846" id="Seg_7069" s="T845">husband-DAT/LOC</ta>
            <ta e="T847" id="Seg_7070" s="T846">go-FUT-2SG</ta>
            <ta e="T848" id="Seg_7071" s="T847">NEG-3SG</ta>
            <ta e="T849" id="Seg_7072" s="T848">grow-TEMP-2SG</ta>
            <ta e="T851" id="Seg_7073" s="T850">say-CVB.SIM</ta>
            <ta e="T852" id="Seg_7074" s="T851">say-PRS-3PL</ta>
            <ta e="T853" id="Seg_7075" s="T852">like.that-INTNS</ta>
            <ta e="T854" id="Seg_7076" s="T853">speak-PRS-3PL</ta>
            <ta e="T855" id="Seg_7077" s="T854">EMPH</ta>
            <ta e="T856" id="Seg_7078" s="T855">1PL-ACC</ta>
            <ta e="T858" id="Seg_7079" s="T857">that.[NOM]</ta>
            <ta e="T859" id="Seg_7080" s="T858">because.of</ta>
            <ta e="T860" id="Seg_7081" s="T859">that</ta>
            <ta e="T861" id="Seg_7082" s="T860">misery-VBZ-CVB.SIM</ta>
            <ta e="T862" id="Seg_7083" s="T861">stand-PRS-1PL</ta>
            <ta e="T863" id="Seg_7084" s="T862">EMPH</ta>
            <ta e="T864" id="Seg_7085" s="T863">that</ta>
            <ta e="T865" id="Seg_7086" s="T864">fur-ACC</ta>
            <ta e="T866" id="Seg_7087" s="T865">release-NEG.CVB.SIM</ta>
            <ta e="T867" id="Seg_7088" s="T866">away</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T2" id="Seg_7089" s="T1">nomadisieren-CVB.SIM</ta>
            <ta e="T4" id="Seg_7090" s="T3">Winter-ACC</ta>
            <ta e="T5" id="Seg_7091" s="T4">Winter-ACC</ta>
            <ta e="T6" id="Seg_7092" s="T5">ganz</ta>
            <ta e="T7" id="Seg_7093" s="T6">nomadisieren-CVB.SIM</ta>
            <ta e="T8" id="Seg_7094" s="T7">gehen-CVB.SEQ</ta>
            <ta e="T9" id="Seg_7095" s="T8">gehen-CVB.SEQ-3PL</ta>
            <ta e="T10" id="Seg_7096" s="T9">Dolgane-PL.[NOM]</ta>
            <ta e="T11" id="Seg_7097" s="T10">Frühling.[NOM]</ta>
            <ta e="T12" id="Seg_7098" s="T11">halten-PTCP.PRS</ta>
            <ta e="T13" id="Seg_7099" s="T12">Erde-3PL.[NOM]</ta>
            <ta e="T14" id="Seg_7100" s="T13">es.gibt</ta>
            <ta e="T15" id="Seg_7101" s="T14">sein-HAB.[3SG]</ta>
            <ta e="T17" id="Seg_7102" s="T16">dort</ta>
            <ta e="T18" id="Seg_7103" s="T17">viel</ta>
            <ta e="T19" id="Seg_7104" s="T18">sehr</ta>
            <ta e="T20" id="Seg_7105" s="T19">Stangenzelt.[NOM]</ta>
            <ta e="T21" id="Seg_7106" s="T20">Haus-PL.[NOM]</ta>
            <ta e="T22" id="Seg_7107" s="T21">Schlitten.[NOM]</ta>
            <ta e="T23" id="Seg_7108" s="T22">Haus-PL.[NOM]</ta>
            <ta e="T24" id="Seg_7109" s="T23">stehen-HAB-3PL</ta>
            <ta e="T26" id="Seg_7110" s="T25">Frühling-ADJZ</ta>
            <ta e="T27" id="Seg_7111" s="T26">warm</ta>
            <ta e="T28" id="Seg_7112" s="T27">Tag-PL-3SG.[NOM]</ta>
            <ta e="T29" id="Seg_7113" s="T28">Tag.[NOM]</ta>
            <ta e="T30" id="Seg_7114" s="T29">scheinen-PTCP.PRS</ta>
            <ta e="T31" id="Seg_7115" s="T30">sein-TEMP-3SG</ta>
            <ta e="T32" id="Seg_7116" s="T31">doch</ta>
            <ta e="T33" id="Seg_7117" s="T32">Frau-PL.[NOM]</ta>
            <ta e="T34" id="Seg_7118" s="T33">arbeiten-PTCP.PRS-3PL.[NOM]</ta>
            <ta e="T35" id="Seg_7119" s="T34">zunehmen-PRS.[3SG]</ta>
            <ta e="T37" id="Seg_7120" s="T36">draußen</ta>
            <ta e="T38" id="Seg_7121" s="T37">3PL-EP-2SG.[NOM]</ta>
            <ta e="T39" id="Seg_7122" s="T38">zuerst</ta>
            <ta e="T40" id="Seg_7123" s="T39">dieses.[NOM]</ta>
            <ta e="T41" id="Seg_7124" s="T40">Haut.[NOM]</ta>
            <ta e="T42" id="Seg_7125" s="T41">Arbeit-3SG-ACC</ta>
            <ta e="T43" id="Seg_7126" s="T42">arbeiten-PRS-3PL</ta>
            <ta e="T45" id="Seg_7127" s="T44">Haut.[NOM]</ta>
            <ta e="T46" id="Seg_7128" s="T45">Haut-2SG-ACC</ta>
            <ta e="T47" id="Seg_7129" s="T46">sehr</ta>
            <ta e="T48" id="Seg_7130" s="T47">genug</ta>
            <ta e="T49" id="Seg_7131" s="T48">säubern-PRS-3PL</ta>
            <ta e="T50" id="Seg_7132" s="T49">zuerst</ta>
            <ta e="T52" id="Seg_7133" s="T51">säubern-CVB.SEQ</ta>
            <ta e="T53" id="Seg_7134" s="T52">gehen-CVB.SEQ-3PL</ta>
            <ta e="T54" id="Seg_7135" s="T53">Schere-EP-INSTR</ta>
            <ta e="T55" id="Seg_7136" s="T54">schneiden-PRS-3PL</ta>
            <ta e="T57" id="Seg_7137" s="T56">Tierhaar-3SG-ACC</ta>
            <ta e="T59" id="Seg_7138" s="T58">jenes</ta>
            <ta e="T60" id="Seg_7139" s="T59">Schere-EP-INSTR</ta>
            <ta e="T61" id="Seg_7140" s="T60">schneiden-CVB.SEQ</ta>
            <ta e="T62" id="Seg_7141" s="T61">aufhören-TEMP-3PL</ta>
            <ta e="T63" id="Seg_7142" s="T62">jenes-ABL</ta>
            <ta e="T64" id="Seg_7143" s="T63">später</ta>
            <ta e="T65" id="Seg_7144" s="T64">Messer-EP-INSTR</ta>
            <ta e="T66" id="Seg_7145" s="T65">wieder</ta>
            <ta e="T67" id="Seg_7146" s="T66">schneiden-PRS-3PL</ta>
            <ta e="T69" id="Seg_7147" s="T68">Lederdecke.eines.Zeltes.[NOM]</ta>
            <ta e="T70" id="Seg_7148" s="T69">Dolgane-PL.[NOM]</ta>
            <ta e="T71" id="Seg_7149" s="T70">sehr</ta>
            <ta e="T72" id="Seg_7150" s="T71">pelzig</ta>
            <ta e="T73" id="Seg_7151" s="T72">wer-ACC</ta>
            <ta e="T74" id="Seg_7152" s="T73">Haut-ACC</ta>
            <ta e="T75" id="Seg_7153" s="T74">nehmen-PTCP.HAB-3SG</ta>
            <ta e="T76" id="Seg_7154" s="T75">nein-3PL</ta>
            <ta e="T78" id="Seg_7155" s="T77">schneiden-PASS/REFL-EP-PTCP.PST</ta>
            <ta e="T79" id="Seg_7156" s="T78">dieses.[NOM]</ta>
            <ta e="T80" id="Seg_7157" s="T79">Tierhaar-3SG-ACC</ta>
            <ta e="T81" id="Seg_7158" s="T80">nehmen-EP-PASS/REFL-EP-PTCP.PST-ABL</ta>
            <ta e="T83" id="Seg_7159" s="T82">schneiden-PTCP.PST</ta>
            <ta e="T84" id="Seg_7160" s="T83">Tierhaar-ACC</ta>
            <ta e="T85" id="Seg_7161" s="T84">wer-ACC</ta>
            <ta e="T86" id="Seg_7162" s="T85">Fell-ACC</ta>
            <ta e="T87" id="Seg_7163" s="T86">nehmen-CVB.SEQ-3PL</ta>
            <ta e="T88" id="Seg_7164" s="T87">wer.[NOM]</ta>
            <ta e="T89" id="Seg_7165" s="T88">Haus.[NOM]</ta>
            <ta e="T90" id="Seg_7166" s="T89">Deckel-3SG.[NOM]</ta>
            <ta e="T91" id="Seg_7167" s="T90">dieses.[NOM]</ta>
            <ta e="T92" id="Seg_7168" s="T91">Lederdecke.eines.Zeltes-PL-ACC</ta>
            <ta e="T93" id="Seg_7169" s="T92">nähen-HAB-3PL</ta>
            <ta e="T95" id="Seg_7170" s="T94">solch</ta>
            <ta e="T96" id="Seg_7171" s="T95">aber</ta>
            <ta e="T97" id="Seg_7172" s="T96">doch</ta>
            <ta e="T98" id="Seg_7173" s="T97">jenes</ta>
            <ta e="T99" id="Seg_7174" s="T98">ein.Bisschen</ta>
            <ta e="T100" id="Seg_7175" s="T99">Tierhaar-ACC</ta>
            <ta e="T101" id="Seg_7176" s="T100">lassen-PRS-3PL</ta>
            <ta e="T102" id="Seg_7177" s="T101">EMPH</ta>
            <ta e="T104" id="Seg_7178" s="T103">jenes-ABL</ta>
            <ta e="T105" id="Seg_7179" s="T104">jenes-3SG-2SG-INSTR</ta>
            <ta e="T106" id="Seg_7180" s="T105">Lederdecke.eines.Zeltes.[NOM]</ta>
            <ta e="T107" id="Seg_7181" s="T106">nähen-PRS-3PL</ta>
            <ta e="T109" id="Seg_7182" s="T108">jenes</ta>
            <ta e="T111" id="Seg_7183" s="T110">mancher</ta>
            <ta e="T112" id="Seg_7184" s="T111">Fell-ACC</ta>
            <ta e="T113" id="Seg_7185" s="T112">3PL-EP-2SG.[NOM]</ta>
            <ta e="T114" id="Seg_7186" s="T113">dieses.[NOM]</ta>
            <ta e="T115" id="Seg_7187" s="T114">dünne.Lederschicht.[NOM]</ta>
            <ta e="T116" id="Seg_7188" s="T115">machen-FUT-NEC-3PL</ta>
            <ta e="T118" id="Seg_7189" s="T117">dünne.Lederschicht.[NOM]</ta>
            <ta e="T119" id="Seg_7190" s="T118">machen-PTCP.PRS-3PL-DAT/LOC</ta>
            <ta e="T120" id="Seg_7191" s="T119">dort</ta>
            <ta e="T121" id="Seg_7192" s="T120">doch</ta>
            <ta e="T122" id="Seg_7193" s="T121">doch</ta>
            <ta e="T123" id="Seg_7194" s="T122">viel</ta>
            <ta e="T124" id="Seg_7195" s="T123">Arbeit.[NOM]</ta>
            <ta e="T126" id="Seg_7196" s="T125">schneiden-PRS-3PL</ta>
            <ta e="T127" id="Seg_7197" s="T126">EMPH</ta>
            <ta e="T128" id="Seg_7198" s="T127">dieses.[NOM]</ta>
            <ta e="T129" id="Seg_7199" s="T128">schneiden-PTCP.PST-3PL-ACC</ta>
            <ta e="T130" id="Seg_7200" s="T129">ähnlich</ta>
            <ta e="T131" id="Seg_7201" s="T130">Tierhaar-3SG.[NOM]</ta>
            <ta e="T132" id="Seg_7202" s="T131">NEG.EX</ta>
            <ta e="T133" id="Seg_7203" s="T132">EMPH</ta>
            <ta e="T134" id="Seg_7204" s="T133">Tierhaar-3SG.[NOM]</ta>
            <ta e="T135" id="Seg_7205" s="T134">wenig</ta>
            <ta e="T136" id="Seg_7206" s="T135">sein-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T137" id="Seg_7207" s="T136">doch</ta>
            <ta e="T138" id="Seg_7208" s="T137">Tierhaar-3SG.[NOM]</ta>
            <ta e="T139" id="Seg_7209" s="T138">bleiben-PRS.[3SG]</ta>
            <ta e="T140" id="Seg_7210" s="T139">EMPH</ta>
            <ta e="T141" id="Seg_7211" s="T140">Haut-2SG-DAT/LOC</ta>
            <ta e="T143" id="Seg_7212" s="T142">jenes</ta>
            <ta e="T144" id="Seg_7213" s="T143">Tierhaar.[NOM]</ta>
            <ta e="T145" id="Seg_7214" s="T144">Tierhaar-2SG.[NOM]</ta>
            <ta e="T146" id="Seg_7215" s="T145">gut-ADVZ</ta>
            <ta e="T147" id="Seg_7216" s="T146">ganz</ta>
            <ta e="T148" id="Seg_7217" s="T147">fallen-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T149" id="Seg_7218" s="T148">jenes-ABL</ta>
            <ta e="T150" id="Seg_7219" s="T149">später</ta>
            <ta e="T151" id="Seg_7220" s="T150">verarbeiten-PTCP.PRS-DAT/LOC</ta>
            <ta e="T152" id="Seg_7221" s="T151">EMPH</ta>
            <ta e="T153" id="Seg_7222" s="T152">gut</ta>
            <ta e="T154" id="Seg_7223" s="T153">EMPH</ta>
            <ta e="T155" id="Seg_7224" s="T154">dünne.Lederschicht.[NOM]</ta>
            <ta e="T156" id="Seg_7225" s="T155">werden-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T157" id="Seg_7226" s="T156">jenes</ta>
            <ta e="T158" id="Seg_7227" s="T157">Tierhaar-PROPR</ta>
            <ta e="T159" id="Seg_7228" s="T158">wer.[NOM]</ta>
            <ta e="T160" id="Seg_7229" s="T159">Fell-ACC</ta>
            <ta e="T161" id="Seg_7230" s="T160">3PL-EP-2SG.[NOM]</ta>
            <ta e="T162" id="Seg_7231" s="T161">liegen-EP-CAUS-PRS-3PL</ta>
            <ta e="T163" id="Seg_7232" s="T162">Wasser-DAT/LOC</ta>
            <ta e="T165" id="Seg_7233" s="T164">Wasser.[NOM]</ta>
            <ta e="T166" id="Seg_7234" s="T165">Inneres-3SG-DAT/LOC</ta>
            <ta e="T167" id="Seg_7235" s="T166">Seife-ACC</ta>
            <ta e="T168" id="Seg_7236" s="T167">gießen-PRS-3PL</ta>
            <ta e="T169" id="Seg_7237" s="T168">legen-PRS-3PL</ta>
            <ta e="T171" id="Seg_7238" s="T170">jenes-ABL</ta>
            <ta e="T172" id="Seg_7239" s="T171">unterschiedlich-unterschiedlich</ta>
            <ta e="T173" id="Seg_7240" s="T172">Gras-PL-ACC</ta>
            <ta e="T175" id="Seg_7241" s="T174">jenes</ta>
            <ta e="T176" id="Seg_7242" s="T175">machen-CVB.SIM</ta>
            <ta e="T177" id="Seg_7243" s="T176">machen-CVB.SIM</ta>
            <ta e="T178" id="Seg_7244" s="T177">wie.viel</ta>
            <ta e="T179" id="Seg_7245" s="T178">INDEF</ta>
            <ta e="T180" id="Seg_7246" s="T179">Tag-ACC</ta>
            <ta e="T181" id="Seg_7247" s="T180">liegen-FUT-NEC.[3SG]</ta>
            <ta e="T183" id="Seg_7248" s="T182">warm</ta>
            <ta e="T184" id="Seg_7249" s="T183">und</ta>
            <ta e="T185" id="Seg_7250" s="T184">Tag.[NOM]</ta>
            <ta e="T186" id="Seg_7251" s="T185">sein-IMP.3SG</ta>
            <ta e="T187" id="Seg_7252" s="T186">jenes</ta>
            <ta e="T188" id="Seg_7253" s="T187">Wasser-2SG.[NOM]</ta>
            <ta e="T189" id="Seg_7254" s="T188">faulen-PRS.[3SG]</ta>
            <ta e="T190" id="Seg_7255" s="T189">EMPH</ta>
            <ta e="T191" id="Seg_7256" s="T190">Haut-2SG.[NOM]</ta>
            <ta e="T192" id="Seg_7257" s="T191">auch</ta>
            <ta e="T193" id="Seg_7258" s="T192">faulen-PRS.[3SG]</ta>
            <ta e="T194" id="Seg_7259" s="T193">EMPH</ta>
            <ta e="T195" id="Seg_7260" s="T194">weich.[NOM]</ta>
            <ta e="T196" id="Seg_7261" s="T195">sein-PRS.[3SG]</ta>
            <ta e="T197" id="Seg_7262" s="T196">EMPH</ta>
            <ta e="T199" id="Seg_7263" s="T198">Haut-2SG.[NOM]</ta>
            <ta e="T200" id="Seg_7264" s="T199">wie.viel</ta>
            <ta e="T201" id="Seg_7265" s="T200">INDEF</ta>
            <ta e="T202" id="Seg_7266" s="T201">Tag-ACC</ta>
            <ta e="T203" id="Seg_7267" s="T202">faulen-CVB.SEQ</ta>
            <ta e="T204" id="Seg_7268" s="T203">aufhören-TEMP-3SG</ta>
            <ta e="T205" id="Seg_7269" s="T204">3PL-EP-2SG.[NOM]</ta>
            <ta e="T206" id="Seg_7270" s="T205">jenes</ta>
            <ta e="T207" id="Seg_7271" s="T206">mischen-PRS-3PL</ta>
            <ta e="T209" id="Seg_7272" s="T208">jenes</ta>
            <ta e="T210" id="Seg_7273" s="T209">Haut-2SG-ACC</ta>
            <ta e="T211" id="Seg_7274" s="T210">nehmen-CVB.SEQ</ta>
            <ta e="T212" id="Seg_7275" s="T211">gehen-CVB.SEQ-3PL</ta>
            <ta e="T213" id="Seg_7276" s="T212">dieses.[NOM]</ta>
            <ta e="T214" id="Seg_7277" s="T213">wer.[NOM]</ta>
            <ta e="T215" id="Seg_7278" s="T214">sich.waschen-CAUS-PRS-3PL</ta>
            <ta e="T216" id="Seg_7279" s="T215">Fluss-DAT/LOC</ta>
            <ta e="T217" id="Seg_7280" s="T216">gehen-CVB.SEQ-3PL</ta>
            <ta e="T219" id="Seg_7281" s="T218">genug</ta>
            <ta e="T220" id="Seg_7282" s="T219">sich.waschen-CAUS-PRS-PST1-3PL</ta>
            <ta e="T221" id="Seg_7283" s="T220">jenes</ta>
            <ta e="T222" id="Seg_7284" s="T221">machen-CVB.SEQ</ta>
            <ta e="T223" id="Seg_7285" s="T222">nachdem</ta>
            <ta e="T224" id="Seg_7286" s="T223">trocken.werden-CAUS-PRS-3PL</ta>
            <ta e="T226" id="Seg_7287" s="T225">trocken.werden-CAUS-CVB.SEQ</ta>
            <ta e="T227" id="Seg_7288" s="T226">gehen-CVB.SEQ-3PL</ta>
            <ta e="T228" id="Seg_7289" s="T227">dieses.[NOM]</ta>
            <ta e="T229" id="Seg_7290" s="T228">Haut-2SG-ACC</ta>
            <ta e="T230" id="Seg_7291" s="T229">wer-VBZ-PRS-3PL</ta>
            <ta e="T231" id="Seg_7292" s="T230">Lederschaber-EP-INSTR</ta>
            <ta e="T232" id="Seg_7293" s="T231">jenes</ta>
            <ta e="T233" id="Seg_7294" s="T232">wer.[NOM]</ta>
            <ta e="T234" id="Seg_7295" s="T233">bleiben-PTCP.PST</ta>
            <ta e="T235" id="Seg_7296" s="T234">Tierhaar-3PL-ACC</ta>
            <ta e="T236" id="Seg_7297" s="T235">nehmen-PRS-3PL</ta>
            <ta e="T238" id="Seg_7298" s="T237">Lederschaber-VBZ-PRS-3PL</ta>
            <ta e="T239" id="Seg_7299" s="T238">dolganisch-SIM</ta>
            <ta e="T240" id="Seg_7300" s="T239">sprechen-PTCP.COND-DAT/LOC</ta>
            <ta e="T242" id="Seg_7301" s="T241">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T243" id="Seg_7302" s="T242">auch</ta>
            <ta e="T244" id="Seg_7303" s="T243">schnell.[NOM]</ta>
            <ta e="T245" id="Seg_7304" s="T244">EMPH</ta>
            <ta e="T246" id="Seg_7305" s="T245">sein-PST2.NEG.[3SG]</ta>
            <ta e="T247" id="Seg_7306" s="T246">klein.[NOM]</ta>
            <ta e="T248" id="Seg_7307" s="T247">EMPH</ta>
            <ta e="T249" id="Seg_7308" s="T248">sein-PST2.NEG.[3SG]</ta>
            <ta e="T250" id="Seg_7309" s="T249">Frau-PL.[NOM]</ta>
            <ta e="T251" id="Seg_7310" s="T250">kräftig</ta>
            <ta e="T252" id="Seg_7311" s="T251">wer.[NOM]</ta>
            <ta e="T253" id="Seg_7312" s="T252">schwer</ta>
            <ta e="T254" id="Seg_7313" s="T253">Arbeit.[NOM]</ta>
            <ta e="T255" id="Seg_7314" s="T254">dieses.[NOM]</ta>
            <ta e="T256" id="Seg_7315" s="T255">Frau-DAT/LOC</ta>
            <ta e="T257" id="Seg_7316" s="T256">EMPH</ta>
            <ta e="T259" id="Seg_7317" s="T258">jenes.[NOM]</ta>
            <ta e="T260" id="Seg_7318" s="T259">wegen</ta>
            <ta e="T261" id="Seg_7319" s="T260">einmal</ta>
            <ta e="T262" id="Seg_7320" s="T261">sich.setzen-CVB.ANT</ta>
            <ta e="T263" id="Seg_7321" s="T262">jenes-ACC</ta>
            <ta e="T264" id="Seg_7322" s="T263">ganz-3SG-ACC</ta>
            <ta e="T267" id="Seg_7323" s="T266">arbeiten-FUT-3PL</ta>
            <ta e="T268" id="Seg_7324" s="T267">NEG-3SG</ta>
            <ta e="T269" id="Seg_7325" s="T268">schwer.[NOM]</ta>
            <ta e="T270" id="Seg_7326" s="T269">Arbeit.[NOM]</ta>
            <ta e="T272" id="Seg_7327" s="T271">jenes-ABL</ta>
            <ta e="T273" id="Seg_7328" s="T272">nächster</ta>
            <ta e="T274" id="Seg_7329" s="T273">Tag.[NOM]</ta>
            <ta e="T275" id="Seg_7330" s="T274">wieder</ta>
            <ta e="T276" id="Seg_7331" s="T275">machen-FUT-3PL</ta>
            <ta e="T277" id="Seg_7332" s="T276">jenes</ta>
            <ta e="T278" id="Seg_7333" s="T277">Lederschaber-VBZ-FUT-3PL</ta>
            <ta e="T279" id="Seg_7334" s="T278">wieder</ta>
            <ta e="T281" id="Seg_7335" s="T280">Lederschaber-VBZ-CVB.SEQ</ta>
            <ta e="T282" id="Seg_7336" s="T281">aufhören-TEMP-3PL</ta>
            <ta e="T283" id="Seg_7337" s="T282">jenes</ta>
            <ta e="T284" id="Seg_7338" s="T283">dünne.Lederschicht-2SG.[NOM]</ta>
            <ta e="T285" id="Seg_7339" s="T284">wer.[NOM]</ta>
            <ta e="T286" id="Seg_7340" s="T285">wer-POSS</ta>
            <ta e="T287" id="Seg_7341" s="T286">NEG</ta>
            <ta e="T288" id="Seg_7342" s="T287">werden-PRS.[3SG]</ta>
            <ta e="T289" id="Seg_7343" s="T288">EMPH</ta>
            <ta e="T290" id="Seg_7344" s="T289">Tierhaar-POSS</ta>
            <ta e="T291" id="Seg_7345" s="T290">NEG</ta>
            <ta e="T292" id="Seg_7346" s="T291">werden-PRS.[3SG]</ta>
            <ta e="T294" id="Seg_7347" s="T293">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T295" id="Seg_7348" s="T294">flach.[NOM]</ta>
            <ta e="T296" id="Seg_7349" s="T295">sein-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T297" id="Seg_7350" s="T296">dieses.[NOM]</ta>
            <ta e="T298" id="Seg_7351" s="T297">Schabeisen-EP-INSTR</ta>
            <ta e="T299" id="Seg_7352" s="T298">was-EP-INSTR</ta>
            <ta e="T300" id="Seg_7353" s="T299">unterschiedlich</ta>
            <ta e="T301" id="Seg_7354" s="T300">unterschiedlich-EP-INSTR</ta>
            <ta e="T302" id="Seg_7355" s="T301">kratzen-PRS-3PL</ta>
            <ta e="T303" id="Seg_7356" s="T302">Schabeisen-VBZ-PRS-3PL</ta>
            <ta e="T304" id="Seg_7357" s="T303">und.so.weiter-PRS-3PL</ta>
            <ta e="T306" id="Seg_7358" s="T305">Messer-EP-INSTR</ta>
            <ta e="T307" id="Seg_7359" s="T306">und</ta>
            <ta e="T308" id="Seg_7360" s="T307">unterschiedlich</ta>
            <ta e="T309" id="Seg_7361" s="T308">unterschiedlich</ta>
            <ta e="T310" id="Seg_7362" s="T309">flach-VBZ-PRS-3PL</ta>
            <ta e="T311" id="Seg_7363" s="T310">und.so.weiter-PRS-3PL</ta>
            <ta e="T313" id="Seg_7364" s="T312">was.[NOM]</ta>
            <ta e="T314" id="Seg_7365" s="T313">schlecht</ta>
            <ta e="T315" id="Seg_7366" s="T314">es.gibt-3SG-ACC</ta>
            <ta e="T316" id="Seg_7367" s="T315">ganz-3SG-ACC</ta>
            <ta e="T317" id="Seg_7368" s="T316">dieses.[NOM]</ta>
            <ta e="T318" id="Seg_7369" s="T317">nehmen-MULT-CVB.SEQ</ta>
            <ta e="T319" id="Seg_7370" s="T318">gehen-PRS-3PL</ta>
            <ta e="T321" id="Seg_7371" s="T320">jenes</ta>
            <ta e="T322" id="Seg_7372" s="T321">dünne.Lederschicht-2SG.[NOM]</ta>
            <ta e="T323" id="Seg_7373" s="T322">jenes</ta>
            <ta e="T324" id="Seg_7374" s="T323">machen-CVB.SEQ</ta>
            <ta e="T325" id="Seg_7375" s="T324">nachdem</ta>
            <ta e="T326" id="Seg_7376" s="T325">sehen-PTCP.COND-DAT/LOC</ta>
            <ta e="T327" id="Seg_7377" s="T326">gut.[NOM]</ta>
            <ta e="T328" id="Seg_7378" s="T327">sehr</ta>
            <ta e="T329" id="Seg_7379" s="T328">werden-PRS.[3SG]</ta>
            <ta e="T331" id="Seg_7380" s="T330">jenes-ABL</ta>
            <ta e="T332" id="Seg_7381" s="T331">dünne.Lederschicht-2SG.[NOM]</ta>
            <ta e="T333" id="Seg_7382" s="T332">echt</ta>
            <ta e="T334" id="Seg_7383" s="T333">dünne.Lederschicht.[NOM]</ta>
            <ta e="T335" id="Seg_7384" s="T334">sein-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T336" id="Seg_7385" s="T335">jenes</ta>
            <ta e="T337" id="Seg_7386" s="T336">machen-CVB.SEQ</ta>
            <ta e="T338" id="Seg_7387" s="T337">EMPH</ta>
            <ta e="T339" id="Seg_7388" s="T338">Rauch-PROPR.[NOM]</ta>
            <ta e="T340" id="Seg_7389" s="T339">sein-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T341" id="Seg_7390" s="T340">jenes-3SG-2SG-ACC</ta>
            <ta e="T342" id="Seg_7391" s="T341">wieder</ta>
            <ta e="T343" id="Seg_7392" s="T342">weich.machen-FUT-NEC-3PL</ta>
            <ta e="T345" id="Seg_7393" s="T344">weich.machen-CVB.PURP</ta>
            <ta e="T346" id="Seg_7394" s="T345">machen-TEMP-3PL</ta>
            <ta e="T347" id="Seg_7395" s="T346">doch</ta>
            <ta e="T348" id="Seg_7396" s="T347">wer-VBZ-PRS-3PL</ta>
            <ta e="T349" id="Seg_7397" s="T348">Mädchen.[NOM]</ta>
            <ta e="T350" id="Seg_7398" s="T349">Kind-3PL-ACC</ta>
            <ta e="T351" id="Seg_7399" s="T350">einladen-HAB-3PL</ta>
            <ta e="T352" id="Seg_7400" s="T351">warum</ta>
            <ta e="T353" id="Seg_7401" s="T352">INDEF</ta>
            <ta e="T355" id="Seg_7402" s="T354">dieses.[NOM]</ta>
            <ta e="T356" id="Seg_7403" s="T355">wer-DAT/LOC</ta>
            <ta e="T357" id="Seg_7404" s="T356">Kleidung.[NOM]</ta>
            <ta e="T358" id="Seg_7405" s="T357">aufhängen-MED-PTCP.PRS</ta>
            <ta e="T359" id="Seg_7406" s="T358">wer-PL-DAT/LOC</ta>
            <ta e="T360" id="Seg_7407" s="T359">aufhängen-PRS-3PL</ta>
            <ta e="T361" id="Seg_7408" s="T360">EMPH</ta>
            <ta e="T362" id="Seg_7409" s="T361">jenes</ta>
            <ta e="T363" id="Seg_7410" s="T362">Haut-2SG-ACC</ta>
            <ta e="T364" id="Seg_7411" s="T363">Kleidung.[NOM]</ta>
            <ta e="T365" id="Seg_7412" s="T364">aufhängen-MED-PTCP.PRS.[NOM]</ta>
            <ta e="T367" id="Seg_7413" s="T366">jenes-3SG-2SG-ACC</ta>
            <ta e="T368" id="Seg_7414" s="T367">herunterziehen-CVB.SEQ</ta>
            <ta e="T369" id="Seg_7415" s="T368">stehen-PTCP.FUT-2SG-ACC</ta>
            <ta e="T370" id="Seg_7416" s="T369">man.muss</ta>
            <ta e="T372" id="Seg_7417" s="T371">Schlitten-ACC</ta>
            <ta e="T373" id="Seg_7418" s="T372">auf.links.drehen-CVB.SIM</ta>
            <ta e="T374" id="Seg_7419" s="T373">legen-CVB.SEQ</ta>
            <ta e="T375" id="Seg_7420" s="T374">gehen-CVB.SEQ-3PL</ta>
            <ta e="T376" id="Seg_7421" s="T375">wieder</ta>
            <ta e="T377" id="Seg_7422" s="T376">herunterziehen-CVB.SEQ</ta>
            <ta e="T378" id="Seg_7423" s="T377">stehen-PTCP.FUT-2SG-ACC</ta>
            <ta e="T379" id="Seg_7424" s="T378">man.muss</ta>
            <ta e="T380" id="Seg_7425" s="T379">jenes</ta>
            <ta e="T381" id="Seg_7426" s="T380">Fell-ACC</ta>
            <ta e="T382" id="Seg_7427" s="T381">legen-CVB.SEQ</ta>
            <ta e="T383" id="Seg_7428" s="T382">gehen-CVB.SEQ-2SG</ta>
            <ta e="T384" id="Seg_7429" s="T383">dünne.Lederschicht.[NOM]</ta>
            <ta e="T385" id="Seg_7430" s="T384">werden-PTCP.FUT-3SG-ACC</ta>
            <ta e="T387" id="Seg_7431" s="T386">doch</ta>
            <ta e="T388" id="Seg_7432" s="T387">jenes</ta>
            <ta e="T389" id="Seg_7433" s="T388">herunterziehen-CVB.SEQ</ta>
            <ta e="T390" id="Seg_7434" s="T389">stehen-PRS-2SG</ta>
            <ta e="T392" id="Seg_7435" s="T391">jenes</ta>
            <ta e="T393" id="Seg_7436" s="T392">herunterziehen-CVB.SEQ</ta>
            <ta e="T394" id="Seg_7437" s="T393">stehen-TEMP-2SG</ta>
            <ta e="T395" id="Seg_7438" s="T394">2SG-DAT/LOC</ta>
            <ta e="T396" id="Seg_7439" s="T395">geben-PRS-3PL</ta>
            <ta e="T397" id="Seg_7440" s="T396">dieses.[NOM]</ta>
            <ta e="T398" id="Seg_7441" s="T397">wer-ACC</ta>
            <ta e="T399" id="Seg_7442" s="T398">Leber-ACC</ta>
            <ta e="T400" id="Seg_7443" s="T399">kochen-PTCP.PST</ta>
            <ta e="T401" id="Seg_7444" s="T400">Rentier.[NOM]</ta>
            <ta e="T402" id="Seg_7445" s="T401">Leber-3SG-ACC</ta>
            <ta e="T403" id="Seg_7446" s="T402">jenes</ta>
            <ta e="T404" id="Seg_7447" s="T403">Leber-ACC</ta>
            <ta e="T405" id="Seg_7448" s="T404">lasten-CVB.SIM</ta>
            <ta e="T406" id="Seg_7449" s="T405">stehen-CVB.SEQ</ta>
            <ta e="T407" id="Seg_7450" s="T406">Mädchen.[NOM]</ta>
            <ta e="T408" id="Seg_7451" s="T407">Mädchen.[NOM]</ta>
            <ta e="T409" id="Seg_7452" s="T408">Kind.[NOM]</ta>
            <ta e="T411" id="Seg_7453" s="T410">kauen-FUT-NEC.[3SG]</ta>
            <ta e="T412" id="Seg_7454" s="T411">kauen.[IMP.2SG]</ta>
            <ta e="T413" id="Seg_7455" s="T412">sagen-PRS-3PL</ta>
            <ta e="T414" id="Seg_7456" s="T413">EMPH</ta>
            <ta e="T415" id="Seg_7457" s="T414">wohin</ta>
            <ta e="T416" id="Seg_7458" s="T415">sein-FUT-2SG=Q</ta>
            <ta e="T417" id="Seg_7459" s="T416">nun</ta>
            <ta e="T419" id="Seg_7460" s="T418">wollen-NEG.PTCP</ta>
            <ta e="T420" id="Seg_7461" s="T419">sein-FUT.[3SG]</ta>
            <ta e="T421" id="Seg_7462" s="T420">wahrscheinlich</ta>
            <ta e="T422" id="Seg_7463" s="T421">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T423" id="Seg_7464" s="T422">Geschmack-3SG.[NOM]</ta>
            <ta e="T424" id="Seg_7465" s="T423">schlecht</ta>
            <ta e="T425" id="Seg_7466" s="T424">doch</ta>
            <ta e="T426" id="Seg_7467" s="T425">stehen-PRS-2SG</ta>
            <ta e="T427" id="Seg_7468" s="T426">EMPH</ta>
            <ta e="T429" id="Seg_7469" s="T428">hören-PRS-2SG</ta>
            <ta e="T430" id="Seg_7470" s="T429">EMPH</ta>
            <ta e="T431" id="Seg_7471" s="T430">Mutter-2SG.[NOM]</ta>
            <ta e="T432" id="Seg_7472" s="T431">Wort-3SG-ACC</ta>
            <ta e="T434" id="Seg_7473" s="T433">jenes</ta>
            <ta e="T435" id="Seg_7474" s="T434">stehen-CVB.SIM</ta>
            <ta e="T436" id="Seg_7475" s="T435">stehen-CVB.SIM</ta>
            <ta e="T437" id="Seg_7476" s="T436">kauen-CVB.SIM</ta>
            <ta e="T438" id="Seg_7477" s="T437">stehen-PRS-2SG</ta>
            <ta e="T439" id="Seg_7478" s="T438">jenes</ta>
            <ta e="T440" id="Seg_7479" s="T439">wer-ACC</ta>
            <ta e="T441" id="Seg_7480" s="T440">Leber-ACC</ta>
            <ta e="T442" id="Seg_7481" s="T441">stehen-CVB.SEQ-2SG</ta>
            <ta e="T444" id="Seg_7482" s="T443">3PL-EP-2SG.[NOM]</ta>
            <ta e="T445" id="Seg_7483" s="T444">jenes</ta>
            <ta e="T447" id="Seg_7484" s="T446">jenes</ta>
            <ta e="T448" id="Seg_7485" s="T447">kauen-PTCP.PST</ta>
            <ta e="T449" id="Seg_7486" s="T448">Leber-2SG-ACC</ta>
            <ta e="T450" id="Seg_7487" s="T449">nehmen-CVB.SIM</ta>
            <ta e="T451" id="Seg_7488" s="T450">nehmen-PRS-3PL</ta>
            <ta e="T452" id="Seg_7489" s="T451">jenes</ta>
            <ta e="T453" id="Seg_7490" s="T452">Haut-2SG-ACC</ta>
            <ta e="T454" id="Seg_7491" s="T453">jenes</ta>
            <ta e="T455" id="Seg_7492" s="T454">wer-VBZ-PTCP.PST</ta>
            <ta e="T456" id="Seg_7493" s="T455">Lederschaber-VBZ-PTCP.PST</ta>
            <ta e="T457" id="Seg_7494" s="T456">Haut-2SG-ACC</ta>
            <ta e="T458" id="Seg_7495" s="T457">nehmen-CVB.SEQ</ta>
            <ta e="T459" id="Seg_7496" s="T458">gehen-CVB.SEQ-3PL</ta>
            <ta e="T460" id="Seg_7497" s="T459">jenes</ta>
            <ta e="T461" id="Seg_7498" s="T460">vollschmieren-PRS-3PL</ta>
            <ta e="T463" id="Seg_7499" s="T462">jenes</ta>
            <ta e="T464" id="Seg_7500" s="T463">machen-CVB.SEQ</ta>
            <ta e="T465" id="Seg_7501" s="T464">nachdem</ta>
            <ta e="T467" id="Seg_7502" s="T466">Schaber.mit.Zähnen-EP-INSTR</ta>
            <ta e="T468" id="Seg_7503" s="T467">Schaber.mit.Zähnen-VBZ-PRS-3PL</ta>
            <ta e="T469" id="Seg_7504" s="T468">Hand-3PL-INSTR</ta>
            <ta e="T471" id="Seg_7505" s="T470">eins</ta>
            <ta e="T472" id="Seg_7506" s="T471">Hand-INSTR</ta>
            <ta e="T473" id="Seg_7507" s="T472">halten-PRS.[3SG]</ta>
            <ta e="T474" id="Seg_7508" s="T473">jenes</ta>
            <ta e="T475" id="Seg_7509" s="T474">Haut-2SG-ACC</ta>
            <ta e="T476" id="Seg_7510" s="T475">eins</ta>
            <ta e="T477" id="Seg_7511" s="T476">Hand-INSTR</ta>
            <ta e="T478" id="Seg_7512" s="T477">Schaber.mit.Zähnen-2SG-ACC</ta>
            <ta e="T479" id="Seg_7513" s="T478">halten-PRS.[3SG]</ta>
            <ta e="T480" id="Seg_7514" s="T479">EMPH</ta>
            <ta e="T481" id="Seg_7515" s="T480">jenes</ta>
            <ta e="T482" id="Seg_7516" s="T481">EMPH</ta>
            <ta e="T483" id="Seg_7517" s="T482">oben-ABL</ta>
            <ta e="T484" id="Seg_7518" s="T483">hinunter-DAT/LOC</ta>
            <ta e="T485" id="Seg_7519" s="T484">bis.zu</ta>
            <ta e="T486" id="Seg_7520" s="T485">so</ta>
            <ta e="T487" id="Seg_7521" s="T486">Schaber.mit.Zähnen-VBZ-PRS.[3SG]</ta>
            <ta e="T488" id="Seg_7522" s="T487">äh</ta>
            <ta e="T489" id="Seg_7523" s="T488">vollschmieren-CVB.SIM</ta>
            <ta e="T490" id="Seg_7524" s="T489">vollschmieren-CVB.SIM</ta>
            <ta e="T492" id="Seg_7525" s="T491">doch</ta>
            <ta e="T493" id="Seg_7526" s="T492">jenes</ta>
            <ta e="T494" id="Seg_7527" s="T493">machen-CVB.SEQ</ta>
            <ta e="T495" id="Seg_7528" s="T494">nachdem</ta>
            <ta e="T496" id="Seg_7529" s="T495">3PL-EP-2SG.[NOM]</ta>
            <ta e="T497" id="Seg_7530" s="T496">Mädchen.[NOM]</ta>
            <ta e="T498" id="Seg_7531" s="T497">Kind-PL.[NOM]</ta>
            <ta e="T499" id="Seg_7532" s="T498">wann</ta>
            <ta e="T500" id="Seg_7533" s="T499">NEG</ta>
            <ta e="T501" id="Seg_7534" s="T500">stehen-PTCP.FUT-3PL-ACC</ta>
            <ta e="T502" id="Seg_7535" s="T501">wollen-PTCP.HAB-3SG</ta>
            <ta e="T503" id="Seg_7536" s="T502">nein-3PL</ta>
            <ta e="T505" id="Seg_7537" s="T504">herunterziehen-CVB.SEQ</ta>
            <ta e="T506" id="Seg_7538" s="T505">stehen-PTCP.FUT-2SG-ACC</ta>
            <ta e="T507" id="Seg_7539" s="T506">man.muss</ta>
            <ta e="T508" id="Seg_7540" s="T507">EMPH</ta>
            <ta e="T509" id="Seg_7541" s="T508">lange</ta>
            <ta e="T510" id="Seg_7542" s="T509">sehr</ta>
            <ta e="T511" id="Seg_7543" s="T510">stehen-PTCP.FUT-2SG-ACC</ta>
            <ta e="T513" id="Seg_7544" s="T512">stehen-FUT-NEC-2SG</ta>
            <ta e="T514" id="Seg_7545" s="T513">dort</ta>
            <ta e="T516" id="Seg_7546" s="T515">und</ta>
            <ta e="T517" id="Seg_7547" s="T516">kauen-PTCP.PRS-EP-2SG.[NOM]</ta>
            <ta e="T518" id="Seg_7548" s="T517">es.gibt</ta>
            <ta e="T519" id="Seg_7549" s="T518">doch</ta>
            <ta e="T520" id="Seg_7550" s="T519">schlecht.[NOM]</ta>
            <ta e="T521" id="Seg_7551" s="T520">und</ta>
            <ta e="T522" id="Seg_7552" s="T521">schlecht.[NOM]</ta>
            <ta e="T524" id="Seg_7553" s="T523">AFFIRM</ta>
            <ta e="T525" id="Seg_7554" s="T524">kauen-FUT-2SG</ta>
            <ta e="T526" id="Seg_7555" s="T525">Mensch.[NOM]</ta>
            <ta e="T527" id="Seg_7556" s="T526">EMPH</ta>
            <ta e="T528" id="Seg_7557" s="T527">Herz-3SG.[NOM]</ta>
            <ta e="T529" id="Seg_7558" s="T528">krank.sein-FUT.[3SG]</ta>
            <ta e="T530" id="Seg_7559" s="T529">manchmal</ta>
            <ta e="T531" id="Seg_7560" s="T530">sein-PRS.[3SG]</ta>
            <ta e="T532" id="Seg_7561" s="T531">EMPH</ta>
            <ta e="T534" id="Seg_7562" s="T533">jenes.[NOM]</ta>
            <ta e="T535" id="Seg_7563" s="T534">wegen</ta>
            <ta e="T537" id="Seg_7564" s="T536">Mädchen.[NOM]</ta>
            <ta e="T538" id="Seg_7565" s="T537">Kind-DIM-PL-EP-2SG.[NOM]</ta>
            <ta e="T539" id="Seg_7566" s="T538">weglaufen-PRS-3PL</ta>
            <ta e="T540" id="Seg_7567" s="T539">EMPH</ta>
            <ta e="T541" id="Seg_7568" s="T540">kauen-NEG-CVB.PURP-3PL</ta>
            <ta e="T542" id="Seg_7569" s="T541">gehen-CVB.SEQ</ta>
            <ta e="T543" id="Seg_7570" s="T542">bleiben-PRS-3PL</ta>
            <ta e="T545" id="Seg_7571" s="T544">wer.[NOM]</ta>
            <ta e="T546" id="Seg_7572" s="T545">Mädchen.[NOM]</ta>
            <ta e="T547" id="Seg_7573" s="T546">Kind-PL-ACC</ta>
            <ta e="T548" id="Seg_7574" s="T547">viel-ADVZ</ta>
            <ta e="T549" id="Seg_7575" s="T548">rufen-HAB-3PL</ta>
            <ta e="T550" id="Seg_7576" s="T549">Junge.[NOM]</ta>
            <ta e="T551" id="Seg_7577" s="T550">Kind-COMP</ta>
            <ta e="T552" id="Seg_7578" s="T551">warum</ta>
            <ta e="T553" id="Seg_7579" s="T552">INDEF</ta>
            <ta e="T555" id="Seg_7580" s="T554">doch</ta>
            <ta e="T556" id="Seg_7581" s="T555">jenes</ta>
            <ta e="T557" id="Seg_7582" s="T556">jenes</ta>
            <ta e="T558" id="Seg_7583" s="T557">machen-CVB.SEQ</ta>
            <ta e="T559" id="Seg_7584" s="T558">nachdem</ta>
            <ta e="T560" id="Seg_7585" s="T559">jenes</ta>
            <ta e="T561" id="Seg_7586" s="T560">wer-EP-INSTR</ta>
            <ta e="T562" id="Seg_7587" s="T561">genug</ta>
            <ta e="T563" id="Seg_7588" s="T562">wer-PART</ta>
            <ta e="T564" id="Seg_7589" s="T563">Schaber.mit.Zähnen-EP-INSTR</ta>
            <ta e="T565" id="Seg_7590" s="T564">genug</ta>
            <ta e="T566" id="Seg_7591" s="T565">wer-VBZ-CVB.SIM</ta>
            <ta e="T567" id="Seg_7592" s="T566">wer-VBZ-PST1-3PL</ta>
            <ta e="T568" id="Seg_7593" s="T567">EMPH</ta>
            <ta e="T569" id="Seg_7594" s="T568">jenes</ta>
            <ta e="T570" id="Seg_7595" s="T569">Haut-2SG-ACC</ta>
            <ta e="T572" id="Seg_7596" s="T571">jenes</ta>
            <ta e="T573" id="Seg_7597" s="T572">Schaber.mit.Zähnen-VBZ-PTCP.PRS-EP-2SG.[NOM]</ta>
            <ta e="T574" id="Seg_7598" s="T573">wieder</ta>
            <ta e="T575" id="Seg_7599" s="T574">lange</ta>
            <ta e="T576" id="Seg_7600" s="T575">sehr</ta>
            <ta e="T577" id="Seg_7601" s="T576">sein-PST1-3SG</ta>
            <ta e="T579" id="Seg_7602" s="T578">nächster</ta>
            <ta e="T580" id="Seg_7603" s="T579">Tag.[NOM]</ta>
            <ta e="T581" id="Seg_7604" s="T580">wieder</ta>
            <ta e="T582" id="Seg_7605" s="T581">stehen-FUT-NEC-2SG</ta>
            <ta e="T583" id="Seg_7606" s="T582">MOD</ta>
            <ta e="T584" id="Seg_7607" s="T583">jenes</ta>
            <ta e="T585" id="Seg_7608" s="T584">kauen-CVB.SIM</ta>
            <ta e="T586" id="Seg_7609" s="T585">kauen-PRS-2SG</ta>
            <ta e="T588" id="Seg_7610" s="T587">wollen-NEG.PTCP</ta>
            <ta e="T589" id="Seg_7611" s="T588">sein-PRS-2SG</ta>
            <ta e="T590" id="Seg_7612" s="T589">Herz-EP-2SG.[NOM]</ta>
            <ta e="T591" id="Seg_7613" s="T590">krank.sein-PRS.[3SG]</ta>
            <ta e="T592" id="Seg_7614" s="T591">MOD</ta>
            <ta e="T594" id="Seg_7615" s="T593">doch</ta>
            <ta e="T595" id="Seg_7616" s="T594">wieder</ta>
            <ta e="T596" id="Seg_7617" s="T595">doch</ta>
            <ta e="T597" id="Seg_7618" s="T596">kauen-PRS-2SG</ta>
            <ta e="T598" id="Seg_7619" s="T597">MOD</ta>
            <ta e="T599" id="Seg_7620" s="T598">wohin</ta>
            <ta e="T600" id="Seg_7621" s="T599">sein-FUT-2SG=Q</ta>
            <ta e="T601" id="Seg_7622" s="T600">Mutter-2SG.[NOM]</ta>
            <ta e="T602" id="Seg_7623" s="T601">Wort-3SG-ACC</ta>
            <ta e="T603" id="Seg_7624" s="T602">hören-PRS-2SG</ta>
            <ta e="T604" id="Seg_7625" s="T603">EMPH</ta>
            <ta e="T606" id="Seg_7626" s="T605">doch</ta>
            <ta e="T607" id="Seg_7627" s="T606">jenes</ta>
            <ta e="T608" id="Seg_7628" s="T607">stehen-CVB.SEQ</ta>
            <ta e="T609" id="Seg_7629" s="T608">kauen-CVB.SIM</ta>
            <ta e="T610" id="Seg_7630" s="T609">kauen-PRS-2SG</ta>
            <ta e="T611" id="Seg_7631" s="T610">doch</ta>
            <ta e="T612" id="Seg_7632" s="T611">Unglück-VBZ-CVB.SEQ</ta>
            <ta e="T613" id="Seg_7633" s="T612">Unglück-VBZ-CVB.SEQ</ta>
            <ta e="T614" id="Seg_7634" s="T613">doch</ta>
            <ta e="T615" id="Seg_7635" s="T614">aufhören-PRS-1PL</ta>
            <ta e="T616" id="Seg_7636" s="T615">MOD</ta>
            <ta e="T618" id="Seg_7637" s="T617">wie.viel-ORD</ta>
            <ta e="T619" id="Seg_7638" s="T618">INDEF</ta>
            <ta e="T620" id="Seg_7639" s="T619">Tag-3SG-DAT/LOC</ta>
            <ta e="T621" id="Seg_7640" s="T620">doch</ta>
            <ta e="T622" id="Seg_7641" s="T621">jenes</ta>
            <ta e="T623" id="Seg_7642" s="T622">Schabeisen.[NOM]</ta>
            <ta e="T624" id="Seg_7643" s="T623">wer-EP-INSTR</ta>
            <ta e="T625" id="Seg_7644" s="T624">Schaber.mit.Zähnen-INSTR</ta>
            <ta e="T627" id="Seg_7645" s="T626">jenes-ABL</ta>
            <ta e="T628" id="Seg_7646" s="T627">später</ta>
            <ta e="T629" id="Seg_7647" s="T628">dieses.[NOM]</ta>
            <ta e="T630" id="Seg_7648" s="T629">dünne.Lederschicht.[NOM]</ta>
            <ta e="T631" id="Seg_7649" s="T630">EMPH</ta>
            <ta e="T632" id="Seg_7650" s="T631">ganz</ta>
            <ta e="T633" id="Seg_7651" s="T632">dünne.Lederschicht</ta>
            <ta e="T634" id="Seg_7652" s="T633">EMPH</ta>
            <ta e="T635" id="Seg_7653" s="T634">gut.[NOM]</ta>
            <ta e="T636" id="Seg_7654" s="T635">sein-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T637" id="Seg_7655" s="T636">weich.[NOM]</ta>
            <ta e="T638" id="Seg_7656" s="T637">sein-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T639" id="Seg_7657" s="T638">dieses.[NOM]</ta>
            <ta e="T640" id="Seg_7658" s="T639">unterschiedlich</ta>
            <ta e="T641" id="Seg_7659" s="T640">unterschiedlich-ADVZ</ta>
            <ta e="T642" id="Seg_7660" s="T641">dieses.[NOM]</ta>
            <ta e="T643" id="Seg_7661" s="T642">wer-VBZ-CVB.SIM</ta>
            <ta e="T644" id="Seg_7662" s="T643">können-PST1-1PL</ta>
            <ta e="T645" id="Seg_7663" s="T644">EMPH</ta>
            <ta e="T647" id="Seg_7664" s="T646">Schabeisen-ABL</ta>
            <ta e="T648" id="Seg_7665" s="T647">Lederschaber-INSTR</ta>
            <ta e="T649" id="Seg_7666" s="T648">machen-PRS-1PL</ta>
            <ta e="T651" id="Seg_7667" s="T650">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T652" id="Seg_7668" s="T651">dünne.Lederschicht.[NOM]</ta>
            <ta e="T653" id="Seg_7669" s="T652">sehr</ta>
            <ta e="T654" id="Seg_7670" s="T653">dünn</ta>
            <ta e="T655" id="Seg_7671" s="T654">sein-CVB.PURP</ta>
            <ta e="T656" id="Seg_7672" s="T655">auch</ta>
            <ta e="T657" id="Seg_7673" s="T656">doch</ta>
            <ta e="T658" id="Seg_7674" s="T657">schlecht.[NOM]</ta>
            <ta e="T660" id="Seg_7675" s="T659">jenes.[NOM]</ta>
            <ta e="T661" id="Seg_7676" s="T660">wegen</ta>
            <ta e="T662" id="Seg_7677" s="T661">dünne.Lederschicht-2SG-ACC</ta>
            <ta e="T663" id="Seg_7678" s="T662">dieses.[NOM]</ta>
            <ta e="T664" id="Seg_7679" s="T663">wer-DAT/LOC</ta>
            <ta e="T665" id="Seg_7680" s="T664">räuchern-PRS-3PL</ta>
            <ta e="T666" id="Seg_7681" s="T665">räuchern-PRS-3PL</ta>
            <ta e="T667" id="Seg_7682" s="T666">Rauch-DAT/LOC</ta>
            <ta e="T669" id="Seg_7683" s="T668">jenes</ta>
            <ta e="T670" id="Seg_7684" s="T669">dünne.Lederschicht-2SG-ACC</ta>
            <ta e="T671" id="Seg_7685" s="T670">nehmen-CVB.SEQ</ta>
            <ta e="T672" id="Seg_7686" s="T671">gehen-CVB.SEQ-3PL</ta>
            <ta e="T673" id="Seg_7687" s="T672">Haus.[NOM]</ta>
            <ta e="T674" id="Seg_7688" s="T673">Inneres-3SG-DAT/LOC</ta>
            <ta e="T675" id="Seg_7689" s="T674">aufhängen-PRS-3PL</ta>
            <ta e="T677" id="Seg_7690" s="T676">Haus.[NOM]</ta>
            <ta e="T678" id="Seg_7691" s="T677">und</ta>
            <ta e="T679" id="Seg_7692" s="T678">sein-FUT.[3SG]=Q</ta>
            <ta e="T680" id="Seg_7693" s="T679">dieses.[NOM]</ta>
            <ta e="T681" id="Seg_7694" s="T680">wer.[NOM]</ta>
            <ta e="T682" id="Seg_7695" s="T681">Stangenzelt.[NOM]</ta>
            <ta e="T683" id="Seg_7696" s="T682">Inneres-3SG-DAT/LOC</ta>
            <ta e="T684" id="Seg_7697" s="T683">Stangenzelt.[NOM]</ta>
            <ta e="T685" id="Seg_7698" s="T684">Haus.[NOM]</ta>
            <ta e="T686" id="Seg_7699" s="T685">Inneres-3SG-DAT/LOC</ta>
            <ta e="T687" id="Seg_7700" s="T686">aufhängen-PRS-3PL</ta>
            <ta e="T689" id="Seg_7701" s="T688">dort</ta>
            <ta e="T690" id="Seg_7702" s="T689">Rauch-DAT/LOC</ta>
            <ta e="T691" id="Seg_7703" s="T690">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T692" id="Seg_7704" s="T691">Nähe.[NOM]</ta>
            <ta e="T693" id="Seg_7705" s="T692">stehen-PRS.[3SG]</ta>
            <ta e="T694" id="Seg_7706" s="T693">EMPH</ta>
            <ta e="T696" id="Seg_7707" s="T695">und</ta>
            <ta e="T697" id="Seg_7708" s="T696">mancher</ta>
            <ta e="T698" id="Seg_7709" s="T697">Frau-PL.[NOM]</ta>
            <ta e="T699" id="Seg_7710" s="T698">so</ta>
            <ta e="T700" id="Seg_7711" s="T699">machen-HAB-3PL</ta>
            <ta e="T701" id="Seg_7712" s="T700">MOD</ta>
            <ta e="T703" id="Seg_7713" s="T702">jenes.[NOM]</ta>
            <ta e="T704" id="Seg_7714" s="T703">ähnlich</ta>
            <ta e="T705" id="Seg_7715" s="T704">und</ta>
            <ta e="T706" id="Seg_7716" s="T705">räuchern-CVB.SIM</ta>
            <ta e="T707" id="Seg_7717" s="T706">liegen-PTCP.FUT</ta>
            <ta e="T708" id="Seg_7718" s="T707">sein-PST2-3PL</ta>
            <ta e="T709" id="Seg_7719" s="T708">EMPH</ta>
            <ta e="T711" id="Seg_7720" s="T710">und</ta>
            <ta e="T712" id="Seg_7721" s="T711">3SG.[NOM]</ta>
            <ta e="T713" id="Seg_7722" s="T712">3PL-EP-2SG.[NOM]</ta>
            <ta e="T714" id="Seg_7723" s="T713">genau.so</ta>
            <ta e="T715" id="Seg_7724" s="T714">sagen-PRS-3PL</ta>
            <ta e="T716" id="Seg_7725" s="T715">EMPH</ta>
            <ta e="T717" id="Seg_7726" s="T716">1PL-ACC</ta>
            <ta e="T718" id="Seg_7727" s="T717">stehen-CAUS-PRS-3PL</ta>
            <ta e="T719" id="Seg_7728" s="T718">EMPH</ta>
            <ta e="T720" id="Seg_7729" s="T719">drei</ta>
            <ta e="T721" id="Seg_7730" s="T720">vier</ta>
            <ta e="T722" id="Seg_7731" s="T721">Mädchen.[NOM]</ta>
            <ta e="T723" id="Seg_7732" s="T722">Mädchen.[NOM]</ta>
            <ta e="T724" id="Seg_7733" s="T723">Kind-PL.[NOM]</ta>
            <ta e="T868" id="Seg_7734" s="T724">wer.[NOM]</ta>
            <ta e="T869" id="Seg_7735" s="T868">wer.[NOM]</ta>
            <ta e="T870" id="Seg_7736" s="T869">Feuer-3SG.[NOM]</ta>
            <ta e="T871" id="Seg_7737" s="T870">wer.[NOM]</ta>
            <ta e="T872" id="Seg_7738" s="T871">Feuer-3SG.[NOM]</ta>
            <ta e="T873" id="Seg_7739" s="T872">dieses.[NOM]</ta>
            <ta e="T874" id="Seg_7740" s="T873">Herd.[NOM]</ta>
            <ta e="T725" id="Seg_7741" s="T874">machen-PRS-3PL</ta>
            <ta e="T726" id="Seg_7742" s="T725">Holz.[NOM]</ta>
            <ta e="T727" id="Seg_7743" s="T726">aufflammen-PRS.[3SG]</ta>
            <ta e="T728" id="Seg_7744" s="T727">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T729" id="Seg_7745" s="T728">oberer.Teil-3SG-INSTR</ta>
            <ta e="T730" id="Seg_7746" s="T729">Gras-PL-ACC</ta>
            <ta e="T731" id="Seg_7747" s="T730">werfen-PRS-3PL</ta>
            <ta e="T732" id="Seg_7748" s="T731">groß</ta>
            <ta e="T733" id="Seg_7749" s="T732">Rauch.[NOM]</ta>
            <ta e="T734" id="Seg_7750" s="T733">kommen-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T736" id="Seg_7751" s="T735">doch</ta>
            <ta e="T737" id="Seg_7752" s="T736">jenes</ta>
            <ta e="T738" id="Seg_7753" s="T737">Rauch.[NOM]</ta>
            <ta e="T739" id="Seg_7754" s="T738">Inneres-3SG-DAT/LOC</ta>
            <ta e="T740" id="Seg_7755" s="T739">um.herum</ta>
            <ta e="T741" id="Seg_7756" s="T740">stehen-CVB.SEQ-1PL</ta>
            <ta e="T742" id="Seg_7757" s="T741">jenes</ta>
            <ta e="T743" id="Seg_7758" s="T742">halten-PRS-1PL</ta>
            <ta e="T744" id="Seg_7759" s="T743">MOD</ta>
            <ta e="T745" id="Seg_7760" s="T744">wer-ACC</ta>
            <ta e="T747" id="Seg_7761" s="T746">jenes</ta>
            <ta e="T748" id="Seg_7762" s="T747">machen-CVB.SEQ</ta>
            <ta e="T749" id="Seg_7763" s="T748">nachdem</ta>
            <ta e="T750" id="Seg_7764" s="T749">sich.drehen-CAUS-PRS-1PL</ta>
            <ta e="T751" id="Seg_7765" s="T750">Sonne.[NOM]</ta>
            <ta e="T752" id="Seg_7766" s="T751">Spur-3SG-ACC</ta>
            <ta e="T753" id="Seg_7767" s="T752">ähnlich</ta>
            <ta e="T754" id="Seg_7768" s="T753">sich.drehen-CAUS-PRS-1PL</ta>
            <ta e="T756" id="Seg_7769" s="T755">gut-ADVZ</ta>
            <ta e="T757" id="Seg_7770" s="T756">halten-FUT-NEC-2SG</ta>
            <ta e="T758" id="Seg_7771" s="T757">jenes-3SG-2SG-ACC</ta>
            <ta e="T759" id="Seg_7772" s="T758">lassen-NEG.PTCP.[NOM]</ta>
            <ta e="T760" id="Seg_7773" s="T759">ähnlich</ta>
            <ta e="T762" id="Seg_7774" s="T761">selbst</ta>
            <ta e="T763" id="Seg_7775" s="T762">selbst-1PL-DAT/LOC</ta>
            <ta e="T764" id="Seg_7776" s="T763">sich.drehen-CAUS-PRS-1PL</ta>
            <ta e="T765" id="Seg_7777" s="T764">vielleicht</ta>
            <ta e="T766" id="Seg_7778" s="T765">genau.so</ta>
            <ta e="T768" id="Seg_7779" s="T767">1SG.[NOM]</ta>
            <ta e="T769" id="Seg_7780" s="T768">Freund-1SG-DAT/LOC</ta>
            <ta e="T770" id="Seg_7781" s="T769">geben-FUT-1SG</ta>
            <ta e="T771" id="Seg_7782" s="T770">Freund-EP-1SG.[NOM]</ta>
            <ta e="T772" id="Seg_7783" s="T771">nächster</ta>
            <ta e="T773" id="Seg_7784" s="T772">Freund-2SG-DAT/LOC</ta>
            <ta e="T774" id="Seg_7785" s="T773">so-INTNS</ta>
            <ta e="T775" id="Seg_7786" s="T774">geben-PRS.[3SG]</ta>
            <ta e="T777" id="Seg_7787" s="T776">umgehen-CAUS-PRS-1PL</ta>
            <ta e="T778" id="Seg_7788" s="T777">jenes</ta>
            <ta e="T779" id="Seg_7789" s="T778">Fell-ACC</ta>
            <ta e="T781" id="Seg_7790" s="T780">Unterteil-2PL-ABL</ta>
            <ta e="T782" id="Seg_7791" s="T781">groß</ta>
            <ta e="T783" id="Seg_7792" s="T782">Rauch.[NOM]</ta>
            <ta e="T784" id="Seg_7793" s="T783">kommen-PRS.[3SG]</ta>
            <ta e="T785" id="Seg_7794" s="T784">EMPH</ta>
            <ta e="T787" id="Seg_7795" s="T786">1PL-DAT/LOC</ta>
            <ta e="T788" id="Seg_7796" s="T787">wieder</ta>
            <ta e="T789" id="Seg_7797" s="T788">schlecht</ta>
            <ta e="T790" id="Seg_7798" s="T789">jenes</ta>
            <ta e="T791" id="Seg_7799" s="T790">Rauch.[NOM]</ta>
            <ta e="T792" id="Seg_7800" s="T791">kommen-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T794" id="Seg_7801" s="T793">Auge-1PL.[NOM]</ta>
            <ta e="T795" id="Seg_7802" s="T794">beißen-PRS.[3SG]</ta>
            <ta e="T796" id="Seg_7803" s="T795">MOD</ta>
            <ta e="T797" id="Seg_7804" s="T796">jenes</ta>
            <ta e="T798" id="Seg_7805" s="T797">Rauch-ABL</ta>
            <ta e="T800" id="Seg_7806" s="T799">Mund-1PL-DAT/LOC</ta>
            <ta e="T801" id="Seg_7807" s="T800">und</ta>
            <ta e="T802" id="Seg_7808" s="T801">nehmen-PRS-1PL</ta>
            <ta e="T803" id="Seg_7809" s="T802">MOD</ta>
            <ta e="T804" id="Seg_7810" s="T803">Rauch-ACC</ta>
            <ta e="T806" id="Seg_7811" s="T805">doch</ta>
            <ta e="T807" id="Seg_7812" s="T806">jenes</ta>
            <ta e="T808" id="Seg_7813" s="T807">und</ta>
            <ta e="T809" id="Seg_7814" s="T808">sein-COND.[3SG]</ta>
            <ta e="T810" id="Seg_7815" s="T809">doch</ta>
            <ta e="T811" id="Seg_7816" s="T810">stehen-EP-REFL-FREQ-PRS-1PL</ta>
            <ta e="T812" id="Seg_7817" s="T811">EMPH</ta>
            <ta e="T813" id="Seg_7818" s="T812">stehen-EP-IMP.2PL</ta>
            <ta e="T814" id="Seg_7819" s="T813">stehen-EP-IMP.2PL</ta>
            <ta e="T815" id="Seg_7820" s="T814">sagen-PRS-3PL</ta>
            <ta e="T816" id="Seg_7821" s="T815">EMPH</ta>
            <ta e="T817" id="Seg_7822" s="T816">1PL-ACC</ta>
            <ta e="T819" id="Seg_7823" s="T818">doch</ta>
            <ta e="T820" id="Seg_7824" s="T819">jenes.[NOM]</ta>
            <ta e="T821" id="Seg_7825" s="T820">ähnlich</ta>
            <ta e="T822" id="Seg_7826" s="T821">sich.drehen-CAUS-PRS-1PL</ta>
            <ta e="T824" id="Seg_7827" s="T823">jenes-ABL</ta>
            <ta e="T825" id="Seg_7828" s="T824">eins</ta>
            <ta e="T826" id="Seg_7829" s="T825">INDEF</ta>
            <ta e="T827" id="Seg_7830" s="T826">Mädchen.[NOM]</ta>
            <ta e="T828" id="Seg_7831" s="T827">Kind-3SG.[NOM]</ta>
            <ta e="T829" id="Seg_7832" s="T828">Fell-ACC</ta>
            <ta e="T830" id="Seg_7833" s="T829">Hand-3SG-INSTR</ta>
            <ta e="T831" id="Seg_7834" s="T830">genau.so-EP-2SG</ta>
            <ta e="T832" id="Seg_7835" s="T831">nehmen-CVB.SEQ</ta>
            <ta e="T833" id="Seg_7836" s="T832">werfen-FUT.[3SG]</ta>
            <ta e="T834" id="Seg_7837" s="T833">Fehler</ta>
            <ta e="T835" id="Seg_7838" s="T834">halten-FUT.[3SG]</ta>
            <ta e="T836" id="Seg_7839" s="T835">jenes-3SG-3SG-ACC</ta>
            <ta e="T837" id="Seg_7840" s="T836">jenes</ta>
            <ta e="T838" id="Seg_7841" s="T837">halten-PTCP.PRS</ta>
            <ta e="T839" id="Seg_7842" s="T838">Ort-3SG-ACC</ta>
            <ta e="T840" id="Seg_7843" s="T839">Haut-3SG-ACC</ta>
            <ta e="T841" id="Seg_7844" s="T840">doch</ta>
            <ta e="T842" id="Seg_7845" s="T841">dann</ta>
            <ta e="T843" id="Seg_7846" s="T842">sagen-PRS-3PL</ta>
            <ta e="T844" id="Seg_7847" s="T843">EMPH</ta>
            <ta e="T845" id="Seg_7848" s="T844">doch</ta>
            <ta e="T846" id="Seg_7849" s="T845">Ehemann-DAT/LOC</ta>
            <ta e="T847" id="Seg_7850" s="T846">gehen-FUT-2SG</ta>
            <ta e="T848" id="Seg_7851" s="T847">NEG-3SG</ta>
            <ta e="T849" id="Seg_7852" s="T848">wachsen-TEMP-2SG</ta>
            <ta e="T851" id="Seg_7853" s="T850">sagen-CVB.SIM</ta>
            <ta e="T852" id="Seg_7854" s="T851">sagen-PRS-3PL</ta>
            <ta e="T853" id="Seg_7855" s="T852">so-INTNS</ta>
            <ta e="T854" id="Seg_7856" s="T853">sprechen-PRS-3PL</ta>
            <ta e="T855" id="Seg_7857" s="T854">EMPH</ta>
            <ta e="T856" id="Seg_7858" s="T855">1PL-ACC</ta>
            <ta e="T858" id="Seg_7859" s="T857">jenes.[NOM]</ta>
            <ta e="T859" id="Seg_7860" s="T858">wegen</ta>
            <ta e="T860" id="Seg_7861" s="T859">jenes</ta>
            <ta e="T861" id="Seg_7862" s="T860">Unglück-VBZ-CVB.SIM</ta>
            <ta e="T862" id="Seg_7863" s="T861">stehen-PRS-1PL</ta>
            <ta e="T863" id="Seg_7864" s="T862">EMPH</ta>
            <ta e="T864" id="Seg_7865" s="T863">jenes</ta>
            <ta e="T865" id="Seg_7866" s="T864">Fell-ACC</ta>
            <ta e="T866" id="Seg_7867" s="T865">lassen-NEG.CVB.SIM</ta>
            <ta e="T867" id="Seg_7868" s="T866">weg</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T2" id="Seg_7869" s="T1">кочевать-CVB.SIM</ta>
            <ta e="T4" id="Seg_7870" s="T3">зима-ACC</ta>
            <ta e="T5" id="Seg_7871" s="T4">зима-ACC</ta>
            <ta e="T6" id="Seg_7872" s="T5">целый</ta>
            <ta e="T7" id="Seg_7873" s="T6">кочевать-CVB.SIM</ta>
            <ta e="T8" id="Seg_7874" s="T7">идти-CVB.SEQ</ta>
            <ta e="T9" id="Seg_7875" s="T8">идти-CVB.SEQ-3PL</ta>
            <ta e="T10" id="Seg_7876" s="T9">долганин-PL.[NOM]</ta>
            <ta e="T11" id="Seg_7877" s="T10">весна.[NOM]</ta>
            <ta e="T12" id="Seg_7878" s="T11">останавливаться-PTCP.PRS</ta>
            <ta e="T13" id="Seg_7879" s="T12">земля-3PL.[NOM]</ta>
            <ta e="T14" id="Seg_7880" s="T13">есть</ta>
            <ta e="T15" id="Seg_7881" s="T14">быть-HAB.[3SG]</ta>
            <ta e="T17" id="Seg_7882" s="T16">там</ta>
            <ta e="T18" id="Seg_7883" s="T17">много</ta>
            <ta e="T19" id="Seg_7884" s="T18">очень</ta>
            <ta e="T20" id="Seg_7885" s="T19">чум.[NOM]</ta>
            <ta e="T21" id="Seg_7886" s="T20">дом-PL.[NOM]</ta>
            <ta e="T22" id="Seg_7887" s="T21">сани.[NOM]</ta>
            <ta e="T23" id="Seg_7888" s="T22">дом-PL.[NOM]</ta>
            <ta e="T24" id="Seg_7889" s="T23">стоять-HAB-3PL</ta>
            <ta e="T26" id="Seg_7890" s="T25">весна-ADJZ</ta>
            <ta e="T27" id="Seg_7891" s="T26">теплый</ta>
            <ta e="T28" id="Seg_7892" s="T27">день-PL-3SG.[NOM]</ta>
            <ta e="T29" id="Seg_7893" s="T28">день.[NOM]</ta>
            <ta e="T30" id="Seg_7894" s="T29">светить-PTCP.PRS</ta>
            <ta e="T31" id="Seg_7895" s="T30">быть-TEMP-3SG</ta>
            <ta e="T32" id="Seg_7896" s="T31">вот</ta>
            <ta e="T33" id="Seg_7897" s="T32">жена-PL.[NOM]</ta>
            <ta e="T34" id="Seg_7898" s="T33">работать-PTCP.PRS-3PL.[NOM]</ta>
            <ta e="T35" id="Seg_7899" s="T34">прибавляться-PRS.[3SG]</ta>
            <ta e="T37" id="Seg_7900" s="T36">на.улице</ta>
            <ta e="T38" id="Seg_7901" s="T37">3PL-EP-2SG.[NOM]</ta>
            <ta e="T39" id="Seg_7902" s="T38">сначала</ta>
            <ta e="T40" id="Seg_7903" s="T39">тот.[NOM]</ta>
            <ta e="T41" id="Seg_7904" s="T40">кожа.[NOM]</ta>
            <ta e="T42" id="Seg_7905" s="T41">работа-3SG-ACC</ta>
            <ta e="T43" id="Seg_7906" s="T42">работать-PRS-3PL</ta>
            <ta e="T45" id="Seg_7907" s="T44">кожа.[NOM]</ta>
            <ta e="T46" id="Seg_7908" s="T45">кожа-2SG-ACC</ta>
            <ta e="T47" id="Seg_7909" s="T46">очень</ta>
            <ta e="T48" id="Seg_7910" s="T47">вдоволь</ta>
            <ta e="T49" id="Seg_7911" s="T48">очистить-PRS-3PL</ta>
            <ta e="T50" id="Seg_7912" s="T49">сначала</ta>
            <ta e="T52" id="Seg_7913" s="T51">очистить-CVB.SEQ</ta>
            <ta e="T53" id="Seg_7914" s="T52">идти-CVB.SEQ-3PL</ta>
            <ta e="T54" id="Seg_7915" s="T53">ножницы-EP-INSTR</ta>
            <ta e="T55" id="Seg_7916" s="T54">резать-PRS-3PL</ta>
            <ta e="T57" id="Seg_7917" s="T56">шерсть-3SG-ACC</ta>
            <ta e="T59" id="Seg_7918" s="T58">тот </ta>
            <ta e="T60" id="Seg_7919" s="T59">ножницы-EP-INSTR</ta>
            <ta e="T61" id="Seg_7920" s="T60">резать-CVB.SEQ</ta>
            <ta e="T62" id="Seg_7921" s="T61">кончать-TEMP-3PL</ta>
            <ta e="T63" id="Seg_7922" s="T62">тот -ABL</ta>
            <ta e="T64" id="Seg_7923" s="T63">позже</ta>
            <ta e="T65" id="Seg_7924" s="T64">нож-EP-INSTR</ta>
            <ta e="T66" id="Seg_7925" s="T65">опять</ta>
            <ta e="T67" id="Seg_7926" s="T66">резать-PRS-3PL</ta>
            <ta e="T69" id="Seg_7927" s="T68">кожаная.покрышка.чума.[NOM]</ta>
            <ta e="T70" id="Seg_7928" s="T69">долганин-PL.[NOM]</ta>
            <ta e="T71" id="Seg_7929" s="T70">очень</ta>
            <ta e="T72" id="Seg_7930" s="T71">мохнатый</ta>
            <ta e="T73" id="Seg_7931" s="T72">кто-ACC</ta>
            <ta e="T74" id="Seg_7932" s="T73">кожа-ACC</ta>
            <ta e="T75" id="Seg_7933" s="T74">взять-PTCP.HAB-3SG</ta>
            <ta e="T76" id="Seg_7934" s="T75">нет-3PL</ta>
            <ta e="T78" id="Seg_7935" s="T77">резать-PASS/REFL-EP-PTCP.PST</ta>
            <ta e="T79" id="Seg_7936" s="T78">тот.[NOM]</ta>
            <ta e="T80" id="Seg_7937" s="T79">шерсть-3SG-ACC</ta>
            <ta e="T81" id="Seg_7938" s="T80">взять-EP-PASS/REFL-EP-PTCP.PST-ABL</ta>
            <ta e="T83" id="Seg_7939" s="T82">резать-PTCP.PST</ta>
            <ta e="T84" id="Seg_7940" s="T83">шерсть-ACC</ta>
            <ta e="T85" id="Seg_7941" s="T84">кто-ACC</ta>
            <ta e="T86" id="Seg_7942" s="T85">шкура-ACC</ta>
            <ta e="T87" id="Seg_7943" s="T86">взять-CVB.SEQ-3PL</ta>
            <ta e="T88" id="Seg_7944" s="T87">кто.[NOM]</ta>
            <ta e="T89" id="Seg_7945" s="T88">дом.[NOM]</ta>
            <ta e="T90" id="Seg_7946" s="T89">крышка-3SG.[NOM]</ta>
            <ta e="T91" id="Seg_7947" s="T90">тот.[NOM]</ta>
            <ta e="T92" id="Seg_7948" s="T91">кожаная.покрышка.чума-PL-ACC</ta>
            <ta e="T93" id="Seg_7949" s="T92">шить-HAB-3PL</ta>
            <ta e="T95" id="Seg_7950" s="T94">такой</ta>
            <ta e="T96" id="Seg_7951" s="T95">однако</ta>
            <ta e="T97" id="Seg_7952" s="T96">вот</ta>
            <ta e="T98" id="Seg_7953" s="T97">тот</ta>
            <ta e="T99" id="Seg_7954" s="T98">немного</ta>
            <ta e="T100" id="Seg_7955" s="T99">шерсть-ACC</ta>
            <ta e="T101" id="Seg_7956" s="T100">оставлять-PRS-3PL</ta>
            <ta e="T102" id="Seg_7957" s="T101">EMPH</ta>
            <ta e="T104" id="Seg_7958" s="T103">тот-ABL</ta>
            <ta e="T105" id="Seg_7959" s="T104">тот-3SG-2SG-INSTR</ta>
            <ta e="T106" id="Seg_7960" s="T105">кожаная.покрышка.чума.[NOM]</ta>
            <ta e="T107" id="Seg_7961" s="T106">шить-PRS-3PL</ta>
            <ta e="T109" id="Seg_7962" s="T108">тот</ta>
            <ta e="T111" id="Seg_7963" s="T110">некоторый</ta>
            <ta e="T112" id="Seg_7964" s="T111">шкура-ACC</ta>
            <ta e="T113" id="Seg_7965" s="T112">3PL-EP-2SG.[NOM]</ta>
            <ta e="T114" id="Seg_7966" s="T113">тот.[NOM]</ta>
            <ta e="T115" id="Seg_7967" s="T114">тонкий.слой.кожы.[NOM]</ta>
            <ta e="T116" id="Seg_7968" s="T115">делать-FUT-NEC-3PL</ta>
            <ta e="T118" id="Seg_7969" s="T117">тонкий.слой.кожы.[NOM]</ta>
            <ta e="T119" id="Seg_7970" s="T118">делать-PTCP.PRS-3PL-DAT/LOC</ta>
            <ta e="T120" id="Seg_7971" s="T119">там</ta>
            <ta e="T121" id="Seg_7972" s="T120">вот</ta>
            <ta e="T122" id="Seg_7973" s="T121">ведь</ta>
            <ta e="T123" id="Seg_7974" s="T122">много</ta>
            <ta e="T124" id="Seg_7975" s="T123">работа.[NOM]</ta>
            <ta e="T126" id="Seg_7976" s="T125">резать-PRS-3PL</ta>
            <ta e="T127" id="Seg_7977" s="T126">EMPH</ta>
            <ta e="T128" id="Seg_7978" s="T127">тот.[NOM]</ta>
            <ta e="T129" id="Seg_7979" s="T128">резать-PTCP.PST-3PL-ACC</ta>
            <ta e="T130" id="Seg_7980" s="T129">подобно</ta>
            <ta e="T131" id="Seg_7981" s="T130">шерсть-3SG.[NOM]</ta>
            <ta e="T132" id="Seg_7982" s="T131">NEG.EX</ta>
            <ta e="T133" id="Seg_7983" s="T132">EMPH</ta>
            <ta e="T134" id="Seg_7984" s="T133">шерсть-3SG.[NOM]</ta>
            <ta e="T135" id="Seg_7985" s="T134">мало</ta>
            <ta e="T136" id="Seg_7986" s="T135">быть-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T137" id="Seg_7987" s="T136">ведь</ta>
            <ta e="T138" id="Seg_7988" s="T137">шерсть-3SG.[NOM]</ta>
            <ta e="T139" id="Seg_7989" s="T138">оставаться-PRS.[3SG]</ta>
            <ta e="T140" id="Seg_7990" s="T139">EMPH</ta>
            <ta e="T141" id="Seg_7991" s="T140">кожа-2SG-DAT/LOC</ta>
            <ta e="T143" id="Seg_7992" s="T142">тот</ta>
            <ta e="T144" id="Seg_7993" s="T143">шерсть.[NOM]</ta>
            <ta e="T145" id="Seg_7994" s="T144">шерсть-2SG.[NOM]</ta>
            <ta e="T146" id="Seg_7995" s="T145">хороший-ADVZ</ta>
            <ta e="T147" id="Seg_7996" s="T146">полностью</ta>
            <ta e="T148" id="Seg_7997" s="T147">падать-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T149" id="Seg_7998" s="T148">тот-ABL</ta>
            <ta e="T150" id="Seg_7999" s="T149">позже</ta>
            <ta e="T151" id="Seg_8000" s="T150">обрабатывать-PTCP.PRS-DAT/LOC</ta>
            <ta e="T152" id="Seg_8001" s="T151">EMPH</ta>
            <ta e="T153" id="Seg_8002" s="T152">хороший</ta>
            <ta e="T154" id="Seg_8003" s="T153">EMPH</ta>
            <ta e="T155" id="Seg_8004" s="T154">тонкий.слой.кожы.[NOM]</ta>
            <ta e="T156" id="Seg_8005" s="T155">становиться-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T157" id="Seg_8006" s="T156">тот</ta>
            <ta e="T158" id="Seg_8007" s="T157">шерсть-PROPR</ta>
            <ta e="T159" id="Seg_8008" s="T158">кто.[NOM]</ta>
            <ta e="T160" id="Seg_8009" s="T159">шкура-ACC</ta>
            <ta e="T161" id="Seg_8010" s="T160">3PL-EP-2SG.[NOM]</ta>
            <ta e="T162" id="Seg_8011" s="T161">лежать-EP-CAUS-PRS-3PL</ta>
            <ta e="T163" id="Seg_8012" s="T162">вода-DAT/LOC</ta>
            <ta e="T165" id="Seg_8013" s="T164">вода.[NOM]</ta>
            <ta e="T166" id="Seg_8014" s="T165">нутро-3SG-DAT/LOC</ta>
            <ta e="T167" id="Seg_8015" s="T166">мыло-ACC</ta>
            <ta e="T168" id="Seg_8016" s="T167">лить-PRS-3PL</ta>
            <ta e="T169" id="Seg_8017" s="T168">класть-PRS-3PL</ta>
            <ta e="T171" id="Seg_8018" s="T170">тот-ABL</ta>
            <ta e="T172" id="Seg_8019" s="T171">разный-разный</ta>
            <ta e="T173" id="Seg_8020" s="T172">трава-PL-ACC</ta>
            <ta e="T175" id="Seg_8021" s="T174">тот</ta>
            <ta e="T176" id="Seg_8022" s="T175">делать-CVB.SIM</ta>
            <ta e="T177" id="Seg_8023" s="T176">делать-CVB.SIM</ta>
            <ta e="T178" id="Seg_8024" s="T177">сколько</ta>
            <ta e="T179" id="Seg_8025" s="T178">INDEF</ta>
            <ta e="T180" id="Seg_8026" s="T179">день-ACC</ta>
            <ta e="T181" id="Seg_8027" s="T180">лежать-FUT-NEC.[3SG]</ta>
            <ta e="T183" id="Seg_8028" s="T182">теплый</ta>
            <ta e="T184" id="Seg_8029" s="T183">да</ta>
            <ta e="T185" id="Seg_8030" s="T184">день.[NOM]</ta>
            <ta e="T186" id="Seg_8031" s="T185">быть-IMP.3SG</ta>
            <ta e="T187" id="Seg_8032" s="T186">тот</ta>
            <ta e="T188" id="Seg_8033" s="T187">вода-2SG.[NOM]</ta>
            <ta e="T189" id="Seg_8034" s="T188">гнить-PRS.[3SG]</ta>
            <ta e="T190" id="Seg_8035" s="T189">EMPH</ta>
            <ta e="T191" id="Seg_8036" s="T190">кожа-2SG.[NOM]</ta>
            <ta e="T192" id="Seg_8037" s="T191">тоже</ta>
            <ta e="T193" id="Seg_8038" s="T192">гнить-PRS.[3SG]</ta>
            <ta e="T194" id="Seg_8039" s="T193">EMPH</ta>
            <ta e="T195" id="Seg_8040" s="T194">мягкий.[NOM]</ta>
            <ta e="T196" id="Seg_8041" s="T195">быть-PRS.[3SG]</ta>
            <ta e="T197" id="Seg_8042" s="T196">EMPH</ta>
            <ta e="T199" id="Seg_8043" s="T198">кожа-2SG.[NOM]</ta>
            <ta e="T200" id="Seg_8044" s="T199">сколько</ta>
            <ta e="T201" id="Seg_8045" s="T200">INDEF</ta>
            <ta e="T202" id="Seg_8046" s="T201">день-ACC</ta>
            <ta e="T203" id="Seg_8047" s="T202">гнить-CVB.SEQ</ta>
            <ta e="T204" id="Seg_8048" s="T203">кончать-TEMP-3SG</ta>
            <ta e="T205" id="Seg_8049" s="T204">3PL-EP-2SG.[NOM]</ta>
            <ta e="T206" id="Seg_8050" s="T205">тот</ta>
            <ta e="T207" id="Seg_8051" s="T206">мешать-PRS-3PL</ta>
            <ta e="T209" id="Seg_8052" s="T208">тот</ta>
            <ta e="T210" id="Seg_8053" s="T209">кожа-2SG-ACC</ta>
            <ta e="T211" id="Seg_8054" s="T210">взять-CVB.SEQ</ta>
            <ta e="T212" id="Seg_8055" s="T211">идти-CVB.SEQ-3PL</ta>
            <ta e="T213" id="Seg_8056" s="T212">тот.[NOM]</ta>
            <ta e="T214" id="Seg_8057" s="T213">кто.[NOM]</ta>
            <ta e="T215" id="Seg_8058" s="T214">мыться-CAUS-PRS-3PL</ta>
            <ta e="T216" id="Seg_8059" s="T215">река-DAT/LOC</ta>
            <ta e="T217" id="Seg_8060" s="T216">идти-CVB.SEQ-3PL</ta>
            <ta e="T219" id="Seg_8061" s="T218">вдоволь</ta>
            <ta e="T220" id="Seg_8062" s="T219">мыться-CAUS-PRS-PST1-3PL</ta>
            <ta e="T221" id="Seg_8063" s="T220">тот</ta>
            <ta e="T222" id="Seg_8064" s="T221">делать-CVB.SEQ</ta>
            <ta e="T223" id="Seg_8065" s="T222">после</ta>
            <ta e="T224" id="Seg_8066" s="T223">высыхать-CAUS-PRS-3PL</ta>
            <ta e="T226" id="Seg_8067" s="T225">высыхать-CAUS-CVB.SEQ</ta>
            <ta e="T227" id="Seg_8068" s="T226">идти-CVB.SEQ-3PL</ta>
            <ta e="T228" id="Seg_8069" s="T227">тот.[NOM]</ta>
            <ta e="T229" id="Seg_8070" s="T228">кожа-2SG-ACC</ta>
            <ta e="T230" id="Seg_8071" s="T229">кто-VBZ-PRS-3PL</ta>
            <ta e="T231" id="Seg_8072" s="T230">кожемялка-EP-INSTR</ta>
            <ta e="T232" id="Seg_8073" s="T231">тот</ta>
            <ta e="T233" id="Seg_8074" s="T232">кто.[NOM]</ta>
            <ta e="T234" id="Seg_8075" s="T233">оставаться-PTCP.PST</ta>
            <ta e="T235" id="Seg_8076" s="T234">шерсть-3PL-ACC</ta>
            <ta e="T236" id="Seg_8077" s="T235">взять-PRS-3PL</ta>
            <ta e="T238" id="Seg_8078" s="T237">кожемялка-VBZ-PRS-3PL</ta>
            <ta e="T239" id="Seg_8079" s="T238">долганский-SIM</ta>
            <ta e="T240" id="Seg_8080" s="T239">говорить-PTCP.COND-DAT/LOC</ta>
            <ta e="T242" id="Seg_8081" s="T241">тот-3SG-2SG.[NOM]</ta>
            <ta e="T243" id="Seg_8082" s="T242">тоже</ta>
            <ta e="T244" id="Seg_8083" s="T243">быстрый.[NOM]</ta>
            <ta e="T245" id="Seg_8084" s="T244">EMPH</ta>
            <ta e="T246" id="Seg_8085" s="T245">быть-PST2.NEG.[3SG]</ta>
            <ta e="T247" id="Seg_8086" s="T246">небольшой.[NOM]</ta>
            <ta e="T248" id="Seg_8087" s="T247">EMPH</ta>
            <ta e="T249" id="Seg_8088" s="T248">быть-PST2.NEG.[3SG]</ta>
            <ta e="T250" id="Seg_8089" s="T249">жена-PL.[NOM]</ta>
            <ta e="T251" id="Seg_8090" s="T250">сильный</ta>
            <ta e="T252" id="Seg_8091" s="T251">кто.[NOM]</ta>
            <ta e="T253" id="Seg_8092" s="T252">тяжелый</ta>
            <ta e="T254" id="Seg_8093" s="T253">работа.[NOM]</ta>
            <ta e="T255" id="Seg_8094" s="T254">тот.[NOM]</ta>
            <ta e="T256" id="Seg_8095" s="T255">жена-DAT/LOC</ta>
            <ta e="T257" id="Seg_8096" s="T256">EMPH</ta>
            <ta e="T259" id="Seg_8097" s="T258">тот.[NOM]</ta>
            <ta e="T260" id="Seg_8098" s="T259">из_за</ta>
            <ta e="T261" id="Seg_8099" s="T260">однажды</ta>
            <ta e="T262" id="Seg_8100" s="T261">сесть-CVB.ANT</ta>
            <ta e="T263" id="Seg_8101" s="T262">тот-ACC</ta>
            <ta e="T264" id="Seg_8102" s="T263">целый-3SG-ACC</ta>
            <ta e="T267" id="Seg_8103" s="T266">работать-FUT-3PL</ta>
            <ta e="T268" id="Seg_8104" s="T267">NEG-3SG</ta>
            <ta e="T269" id="Seg_8105" s="T268">тяжелый.[NOM]</ta>
            <ta e="T270" id="Seg_8106" s="T269">работа.[NOM]</ta>
            <ta e="T272" id="Seg_8107" s="T271">тот-ABL</ta>
            <ta e="T273" id="Seg_8108" s="T272">следующий</ta>
            <ta e="T274" id="Seg_8109" s="T273">день.[NOM]</ta>
            <ta e="T275" id="Seg_8110" s="T274">опять</ta>
            <ta e="T276" id="Seg_8111" s="T275">делать-FUT-3PL</ta>
            <ta e="T277" id="Seg_8112" s="T276">тот</ta>
            <ta e="T278" id="Seg_8113" s="T277">кожемялка-VBZ-FUT-3PL</ta>
            <ta e="T279" id="Seg_8114" s="T278">опять</ta>
            <ta e="T281" id="Seg_8115" s="T280">кожемялка-VBZ-CVB.SEQ</ta>
            <ta e="T282" id="Seg_8116" s="T281">кончать-TEMP-3PL</ta>
            <ta e="T283" id="Seg_8117" s="T282">тот</ta>
            <ta e="T284" id="Seg_8118" s="T283">тонкий.слой.кожы-2SG.[NOM]</ta>
            <ta e="T285" id="Seg_8119" s="T284">кто.[NOM]</ta>
            <ta e="T286" id="Seg_8120" s="T285">кто-POSS</ta>
            <ta e="T287" id="Seg_8121" s="T286">NEG</ta>
            <ta e="T288" id="Seg_8122" s="T287">становиться-PRS.[3SG]</ta>
            <ta e="T289" id="Seg_8123" s="T288">EMPH</ta>
            <ta e="T290" id="Seg_8124" s="T289">шерсть-POSS</ta>
            <ta e="T291" id="Seg_8125" s="T290">NEG</ta>
            <ta e="T292" id="Seg_8126" s="T291">становиться-PRS.[3SG]</ta>
            <ta e="T294" id="Seg_8127" s="T293">тот-3SG-2SG.[NOM]</ta>
            <ta e="T295" id="Seg_8128" s="T294">плоский.[NOM]</ta>
            <ta e="T296" id="Seg_8129" s="T295">быть-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T297" id="Seg_8130" s="T296">тот.[NOM]</ta>
            <ta e="T298" id="Seg_8131" s="T297">скребок-EP-INSTR</ta>
            <ta e="T299" id="Seg_8132" s="T298">что-EP-INSTR</ta>
            <ta e="T300" id="Seg_8133" s="T299">разный</ta>
            <ta e="T301" id="Seg_8134" s="T300">разный-EP-INSTR</ta>
            <ta e="T302" id="Seg_8135" s="T301">скрести-PRS-3PL</ta>
            <ta e="T303" id="Seg_8136" s="T302">скребок-VBZ-PRS-3PL</ta>
            <ta e="T304" id="Seg_8137" s="T303">и.так.далее-PRS-3PL</ta>
            <ta e="T306" id="Seg_8138" s="T305">нож-EP-INSTR</ta>
            <ta e="T307" id="Seg_8139" s="T306">да</ta>
            <ta e="T308" id="Seg_8140" s="T307">разный</ta>
            <ta e="T309" id="Seg_8141" s="T308">разный</ta>
            <ta e="T310" id="Seg_8142" s="T309">плоский-VBZ-PRS-3PL</ta>
            <ta e="T311" id="Seg_8143" s="T310">и.так.далее-PRS-3PL</ta>
            <ta e="T313" id="Seg_8144" s="T312">что.[NOM]</ta>
            <ta e="T314" id="Seg_8145" s="T313">плохой</ta>
            <ta e="T315" id="Seg_8146" s="T314">есть-3SG-ACC</ta>
            <ta e="T316" id="Seg_8147" s="T315">целый-3SG-ACC</ta>
            <ta e="T317" id="Seg_8148" s="T316">тот.[NOM]</ta>
            <ta e="T318" id="Seg_8149" s="T317">взять-MULT-CVB.SEQ</ta>
            <ta e="T319" id="Seg_8150" s="T318">идти-PRS-3PL</ta>
            <ta e="T321" id="Seg_8151" s="T320">тот</ta>
            <ta e="T322" id="Seg_8152" s="T321">тонкий.слой.кожы-2SG.[NOM]</ta>
            <ta e="T323" id="Seg_8153" s="T322">тот</ta>
            <ta e="T324" id="Seg_8154" s="T323">делать-CVB.SEQ</ta>
            <ta e="T325" id="Seg_8155" s="T324">после</ta>
            <ta e="T326" id="Seg_8156" s="T325">видеть-PTCP.COND-DAT/LOC</ta>
            <ta e="T327" id="Seg_8157" s="T326">хороший.[NOM]</ta>
            <ta e="T328" id="Seg_8158" s="T327">очень</ta>
            <ta e="T329" id="Seg_8159" s="T328">становиться-PRS.[3SG]</ta>
            <ta e="T331" id="Seg_8160" s="T330">тот-ABL</ta>
            <ta e="T332" id="Seg_8161" s="T331">тонкий.слой.кожы-2SG.[NOM]</ta>
            <ta e="T333" id="Seg_8162" s="T332">настоящий</ta>
            <ta e="T334" id="Seg_8163" s="T333">тонкий.слой.кожы.[NOM]</ta>
            <ta e="T335" id="Seg_8164" s="T334">быть-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T336" id="Seg_8165" s="T335">тот</ta>
            <ta e="T337" id="Seg_8166" s="T336">делать-CVB.SEQ</ta>
            <ta e="T338" id="Seg_8167" s="T337">EMPH</ta>
            <ta e="T339" id="Seg_8168" s="T338">дым-PROPR.[NOM]</ta>
            <ta e="T340" id="Seg_8169" s="T339">быть-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T341" id="Seg_8170" s="T340">тот-3SG-2SG-ACC</ta>
            <ta e="T342" id="Seg_8171" s="T341">опять</ta>
            <ta e="T343" id="Seg_8172" s="T342">смягчать-FUT-NEC-3PL</ta>
            <ta e="T345" id="Seg_8173" s="T344">смягчать-CVB.PURP</ta>
            <ta e="T346" id="Seg_8174" s="T345">делать-TEMP-3PL</ta>
            <ta e="T347" id="Seg_8175" s="T346">вот</ta>
            <ta e="T348" id="Seg_8176" s="T347">кто-VBZ-PRS-3PL</ta>
            <ta e="T349" id="Seg_8177" s="T348">девушка.[NOM]</ta>
            <ta e="T350" id="Seg_8178" s="T349">ребенок-3PL-ACC</ta>
            <ta e="T351" id="Seg_8179" s="T350">зазывать-HAB-3PL</ta>
            <ta e="T352" id="Seg_8180" s="T351">почему</ta>
            <ta e="T353" id="Seg_8181" s="T352">INDEF</ta>
            <ta e="T355" id="Seg_8182" s="T354">тот.[NOM]</ta>
            <ta e="T356" id="Seg_8183" s="T355">кто-DAT/LOC</ta>
            <ta e="T357" id="Seg_8184" s="T356">одежда.[NOM]</ta>
            <ta e="T358" id="Seg_8185" s="T357">повесить-MED-PTCP.PRS</ta>
            <ta e="T359" id="Seg_8186" s="T358">кто-PL-DAT/LOC</ta>
            <ta e="T360" id="Seg_8187" s="T359">повесить-PRS-3PL</ta>
            <ta e="T361" id="Seg_8188" s="T360">EMPH</ta>
            <ta e="T362" id="Seg_8189" s="T361">тот</ta>
            <ta e="T363" id="Seg_8190" s="T362">кожа-2SG-ACC</ta>
            <ta e="T364" id="Seg_8191" s="T363">одежда.[NOM]</ta>
            <ta e="T365" id="Seg_8192" s="T364">повесить-MED-PTCP.PRS.[NOM]</ta>
            <ta e="T367" id="Seg_8193" s="T366">тот-3SG-2SG-ACC</ta>
            <ta e="T368" id="Seg_8194" s="T367">перевешивать-CVB.SEQ</ta>
            <ta e="T369" id="Seg_8195" s="T368">стоять-PTCP.FUT-2SG-ACC</ta>
            <ta e="T370" id="Seg_8196" s="T369">надо</ta>
            <ta e="T372" id="Seg_8197" s="T371">сани-ACC</ta>
            <ta e="T373" id="Seg_8198" s="T372">вертеть.наизнанку-CVB.SIM</ta>
            <ta e="T374" id="Seg_8199" s="T373">класть-CVB.SEQ</ta>
            <ta e="T375" id="Seg_8200" s="T374">идти-CVB.SEQ-3PL</ta>
            <ta e="T376" id="Seg_8201" s="T375">опять</ta>
            <ta e="T377" id="Seg_8202" s="T376">перевешивать-CVB.SEQ</ta>
            <ta e="T378" id="Seg_8203" s="T377">стоять-PTCP.FUT-2SG-ACC</ta>
            <ta e="T379" id="Seg_8204" s="T378">надо</ta>
            <ta e="T380" id="Seg_8205" s="T379">тот</ta>
            <ta e="T381" id="Seg_8206" s="T380">шкура-ACC</ta>
            <ta e="T382" id="Seg_8207" s="T381">класть-CVB.SEQ</ta>
            <ta e="T383" id="Seg_8208" s="T382">идти-CVB.SEQ-2SG</ta>
            <ta e="T384" id="Seg_8209" s="T383">тонкий.слой.кожы.[NOM]</ta>
            <ta e="T385" id="Seg_8210" s="T384">становиться-PTCP.FUT-3SG-ACC</ta>
            <ta e="T387" id="Seg_8211" s="T386">вот</ta>
            <ta e="T388" id="Seg_8212" s="T387">тот</ta>
            <ta e="T389" id="Seg_8213" s="T388">перевешивать-CVB.SEQ</ta>
            <ta e="T390" id="Seg_8214" s="T389">стоять-PRS-2SG</ta>
            <ta e="T392" id="Seg_8215" s="T391">тот</ta>
            <ta e="T393" id="Seg_8216" s="T392">перевешивать-CVB.SEQ</ta>
            <ta e="T394" id="Seg_8217" s="T393">стоять-TEMP-2SG</ta>
            <ta e="T395" id="Seg_8218" s="T394">2SG-DAT/LOC</ta>
            <ta e="T396" id="Seg_8219" s="T395">давать-PRS-3PL</ta>
            <ta e="T397" id="Seg_8220" s="T396">тот.[NOM]</ta>
            <ta e="T398" id="Seg_8221" s="T397">кто-ACC</ta>
            <ta e="T399" id="Seg_8222" s="T398">печень-ACC</ta>
            <ta e="T400" id="Seg_8223" s="T399">вариться-PTCP.PST</ta>
            <ta e="T401" id="Seg_8224" s="T400">олень.[NOM]</ta>
            <ta e="T402" id="Seg_8225" s="T401">печень-3SG-ACC</ta>
            <ta e="T403" id="Seg_8226" s="T402">тот</ta>
            <ta e="T404" id="Seg_8227" s="T403">печень-ACC</ta>
            <ta e="T405" id="Seg_8228" s="T404">давить-CVB.SIM</ta>
            <ta e="T406" id="Seg_8229" s="T405">стоять-CVB.SEQ</ta>
            <ta e="T407" id="Seg_8230" s="T406">девушка.[NOM]</ta>
            <ta e="T408" id="Seg_8231" s="T407">девушка.[NOM]</ta>
            <ta e="T409" id="Seg_8232" s="T408">ребенок.[NOM]</ta>
            <ta e="T411" id="Seg_8233" s="T410">жевать-FUT-NEC.[3SG]</ta>
            <ta e="T412" id="Seg_8234" s="T411">жевать.[IMP.2SG]</ta>
            <ta e="T413" id="Seg_8235" s="T412">говорить-PRS-3PL</ta>
            <ta e="T414" id="Seg_8236" s="T413">EMPH</ta>
            <ta e="T415" id="Seg_8237" s="T414">куда</ta>
            <ta e="T416" id="Seg_8238" s="T415">быть-FUT-2SG=Q</ta>
            <ta e="T417" id="Seg_8239" s="T416">вот</ta>
            <ta e="T419" id="Seg_8240" s="T418">хотеть-NEG.PTCP</ta>
            <ta e="T420" id="Seg_8241" s="T419">быть-FUT.[3SG]</ta>
            <ta e="T421" id="Seg_8242" s="T420">наверное</ta>
            <ta e="T422" id="Seg_8243" s="T421">тот-3SG-2SG.[NOM]</ta>
            <ta e="T423" id="Seg_8244" s="T422">вкус-3SG.[NOM]</ta>
            <ta e="T424" id="Seg_8245" s="T423">плохо</ta>
            <ta e="T425" id="Seg_8246" s="T424">ведь</ta>
            <ta e="T426" id="Seg_8247" s="T425">стоять-PRS-2SG</ta>
            <ta e="T427" id="Seg_8248" s="T426">EMPH</ta>
            <ta e="T429" id="Seg_8249" s="T428">слышать-PRS-2SG</ta>
            <ta e="T430" id="Seg_8250" s="T429">EMPH</ta>
            <ta e="T431" id="Seg_8251" s="T430">мать-2SG.[NOM]</ta>
            <ta e="T432" id="Seg_8252" s="T431">слово-3SG-ACC</ta>
            <ta e="T434" id="Seg_8253" s="T433">тот</ta>
            <ta e="T435" id="Seg_8254" s="T434">стоять-CVB.SIM</ta>
            <ta e="T436" id="Seg_8255" s="T435">стоять-CVB.SIM</ta>
            <ta e="T437" id="Seg_8256" s="T436">жевать-CVB.SIM</ta>
            <ta e="T438" id="Seg_8257" s="T437">стоять-PRS-2SG</ta>
            <ta e="T439" id="Seg_8258" s="T438">тот</ta>
            <ta e="T440" id="Seg_8259" s="T439">кто-ACC</ta>
            <ta e="T441" id="Seg_8260" s="T440">печень-ACC</ta>
            <ta e="T442" id="Seg_8261" s="T441">стоять-CVB.SEQ-2SG</ta>
            <ta e="T444" id="Seg_8262" s="T443">3PL-EP-2SG.[NOM]</ta>
            <ta e="T445" id="Seg_8263" s="T444">тот</ta>
            <ta e="T447" id="Seg_8264" s="T446">тот</ta>
            <ta e="T448" id="Seg_8265" s="T447">жевать-PTCP.PST</ta>
            <ta e="T449" id="Seg_8266" s="T448">печень-2SG-ACC</ta>
            <ta e="T450" id="Seg_8267" s="T449">взять-CVB.SIM</ta>
            <ta e="T451" id="Seg_8268" s="T450">взять-PRS-3PL</ta>
            <ta e="T452" id="Seg_8269" s="T451">тот</ta>
            <ta e="T453" id="Seg_8270" s="T452">кожа-2SG-ACC</ta>
            <ta e="T454" id="Seg_8271" s="T453">тот</ta>
            <ta e="T455" id="Seg_8272" s="T454">кто-VBZ-PTCP.PST</ta>
            <ta e="T456" id="Seg_8273" s="T455">кожемялка-VBZ-PTCP.PST</ta>
            <ta e="T457" id="Seg_8274" s="T456">кожа-2SG-ACC</ta>
            <ta e="T458" id="Seg_8275" s="T457">взять-CVB.SEQ</ta>
            <ta e="T459" id="Seg_8276" s="T458">идти-CVB.SEQ-3PL</ta>
            <ta e="T460" id="Seg_8277" s="T459">тот</ta>
            <ta e="T461" id="Seg_8278" s="T460">замазать-PRS-3PL</ta>
            <ta e="T463" id="Seg_8279" s="T462">тот</ta>
            <ta e="T464" id="Seg_8280" s="T463">делать-CVB.SEQ</ta>
            <ta e="T465" id="Seg_8281" s="T464">после</ta>
            <ta e="T467" id="Seg_8282" s="T466">скребок.зубчатый-EP-INSTR</ta>
            <ta e="T468" id="Seg_8283" s="T467">скребок.зубчатый-VBZ-PRS-3PL</ta>
            <ta e="T469" id="Seg_8284" s="T468">рука-3PL-INSTR</ta>
            <ta e="T471" id="Seg_8285" s="T470">один</ta>
            <ta e="T472" id="Seg_8286" s="T471">рука-INSTR</ta>
            <ta e="T473" id="Seg_8287" s="T472">держать-PRS.[3SG]</ta>
            <ta e="T474" id="Seg_8288" s="T473">тот</ta>
            <ta e="T475" id="Seg_8289" s="T474">кожа-2SG-ACC</ta>
            <ta e="T476" id="Seg_8290" s="T475">один</ta>
            <ta e="T477" id="Seg_8291" s="T476">рука-INSTR</ta>
            <ta e="T478" id="Seg_8292" s="T477">скребок.зубчатый-2SG-ACC</ta>
            <ta e="T479" id="Seg_8293" s="T478">держать-PRS.[3SG]</ta>
            <ta e="T480" id="Seg_8294" s="T479">EMPH</ta>
            <ta e="T481" id="Seg_8295" s="T480">тот</ta>
            <ta e="T482" id="Seg_8296" s="T481">EMPH</ta>
            <ta e="T483" id="Seg_8297" s="T482">наверху-ABL</ta>
            <ta e="T484" id="Seg_8298" s="T483">винз-DAT/LOC</ta>
            <ta e="T485" id="Seg_8299" s="T484">пока</ta>
            <ta e="T486" id="Seg_8300" s="T485">так</ta>
            <ta e="T487" id="Seg_8301" s="T486">скребок.зубчатый-VBZ-PRS.[3SG]</ta>
            <ta e="T488" id="Seg_8302" s="T487">ээ</ta>
            <ta e="T489" id="Seg_8303" s="T488">замазать-CVB.SIM</ta>
            <ta e="T490" id="Seg_8304" s="T489">замазать-CVB.SIM</ta>
            <ta e="T492" id="Seg_8305" s="T491">вот</ta>
            <ta e="T493" id="Seg_8306" s="T492">тот</ta>
            <ta e="T494" id="Seg_8307" s="T493">делать-CVB.SEQ</ta>
            <ta e="T495" id="Seg_8308" s="T494">после</ta>
            <ta e="T496" id="Seg_8309" s="T495">3PL-EP-2SG.[NOM]</ta>
            <ta e="T497" id="Seg_8310" s="T496">девушка.[NOM]</ta>
            <ta e="T498" id="Seg_8311" s="T497">ребенок-PL.[NOM]</ta>
            <ta e="T499" id="Seg_8312" s="T498">когда</ta>
            <ta e="T500" id="Seg_8313" s="T499">NEG</ta>
            <ta e="T501" id="Seg_8314" s="T500">стоять-PTCP.FUT-3PL-ACC</ta>
            <ta e="T502" id="Seg_8315" s="T501">хотеть-PTCP.HAB-3SG</ta>
            <ta e="T503" id="Seg_8316" s="T502">нет-3PL</ta>
            <ta e="T505" id="Seg_8317" s="T504">перевешивать-CVB.SEQ</ta>
            <ta e="T506" id="Seg_8318" s="T505">стоять-PTCP.FUT-2SG-ACC</ta>
            <ta e="T507" id="Seg_8319" s="T506">надо</ta>
            <ta e="T508" id="Seg_8320" s="T507">EMPH</ta>
            <ta e="T509" id="Seg_8321" s="T508">долго</ta>
            <ta e="T510" id="Seg_8322" s="T509">очень</ta>
            <ta e="T511" id="Seg_8323" s="T510">стоять-PTCP.FUT-2SG-ACC</ta>
            <ta e="T513" id="Seg_8324" s="T512">стоять-FUT-NEC-2SG</ta>
            <ta e="T514" id="Seg_8325" s="T513">там</ta>
            <ta e="T516" id="Seg_8326" s="T515">а</ta>
            <ta e="T517" id="Seg_8327" s="T516">жевать-PTCP.PRS-EP-2SG.[NOM]</ta>
            <ta e="T518" id="Seg_8328" s="T517">есть</ta>
            <ta e="T519" id="Seg_8329" s="T518">вот</ta>
            <ta e="T520" id="Seg_8330" s="T519">плохой.[NOM]</ta>
            <ta e="T521" id="Seg_8331" s="T520">да</ta>
            <ta e="T522" id="Seg_8332" s="T521">плохой.[NOM]</ta>
            <ta e="T524" id="Seg_8333" s="T523">AFFIRM</ta>
            <ta e="T525" id="Seg_8334" s="T524">жевать-FUT-2SG</ta>
            <ta e="T526" id="Seg_8335" s="T525">человек.[NOM]</ta>
            <ta e="T527" id="Seg_8336" s="T526">EMPH</ta>
            <ta e="T528" id="Seg_8337" s="T527">сердце-3SG.[NOM]</ta>
            <ta e="T529" id="Seg_8338" s="T528">тошнить-FUT.[3SG]</ta>
            <ta e="T530" id="Seg_8339" s="T529">иногда</ta>
            <ta e="T531" id="Seg_8340" s="T530">быть-PRS.[3SG]</ta>
            <ta e="T532" id="Seg_8341" s="T531">EMPH</ta>
            <ta e="T534" id="Seg_8342" s="T533">тот.[NOM]</ta>
            <ta e="T535" id="Seg_8343" s="T534">из_за</ta>
            <ta e="T537" id="Seg_8344" s="T536">девушка.[NOM]</ta>
            <ta e="T538" id="Seg_8345" s="T537">ребенок-DIM-PL-EP-2SG.[NOM]</ta>
            <ta e="T539" id="Seg_8346" s="T538">убегать-PRS-3PL</ta>
            <ta e="T540" id="Seg_8347" s="T539">EMPH</ta>
            <ta e="T541" id="Seg_8348" s="T540">жевать-NEG-CVB.PURP-3PL</ta>
            <ta e="T542" id="Seg_8349" s="T541">идти-CVB.SEQ</ta>
            <ta e="T543" id="Seg_8350" s="T542">оставаться-PRS-3PL</ta>
            <ta e="T545" id="Seg_8351" s="T544">кто.[NOM]</ta>
            <ta e="T546" id="Seg_8352" s="T545">девушка.[NOM]</ta>
            <ta e="T547" id="Seg_8353" s="T546">ребенок-PL-ACC</ta>
            <ta e="T548" id="Seg_8354" s="T547">много-ADVZ</ta>
            <ta e="T549" id="Seg_8355" s="T548">звать-HAB-3PL</ta>
            <ta e="T550" id="Seg_8356" s="T549">мальчик.[NOM]</ta>
            <ta e="T551" id="Seg_8357" s="T550">ребенок-COMP</ta>
            <ta e="T552" id="Seg_8358" s="T551">почему</ta>
            <ta e="T553" id="Seg_8359" s="T552">INDEF</ta>
            <ta e="T555" id="Seg_8360" s="T554">вот</ta>
            <ta e="T556" id="Seg_8361" s="T555">тот</ta>
            <ta e="T557" id="Seg_8362" s="T556">тот</ta>
            <ta e="T558" id="Seg_8363" s="T557">делать-CVB.SEQ</ta>
            <ta e="T559" id="Seg_8364" s="T558">после</ta>
            <ta e="T560" id="Seg_8365" s="T559">тот</ta>
            <ta e="T561" id="Seg_8366" s="T560">кто-EP-INSTR</ta>
            <ta e="T562" id="Seg_8367" s="T561">вдоволь</ta>
            <ta e="T563" id="Seg_8368" s="T562">кто-PART</ta>
            <ta e="T564" id="Seg_8369" s="T563">скребок.зубчатый-EP-INSTR</ta>
            <ta e="T565" id="Seg_8370" s="T564">вдоволь</ta>
            <ta e="T566" id="Seg_8371" s="T565">кто-VBZ-CVB.SIM</ta>
            <ta e="T567" id="Seg_8372" s="T566">кто-VBZ-PST1-3PL</ta>
            <ta e="T568" id="Seg_8373" s="T567">EMPH</ta>
            <ta e="T569" id="Seg_8374" s="T568">тот</ta>
            <ta e="T570" id="Seg_8375" s="T569">кожа-2SG-ACC</ta>
            <ta e="T572" id="Seg_8376" s="T571">тот</ta>
            <ta e="T573" id="Seg_8377" s="T572">скребок.зубчатый-VBZ-PTCP.PRS-EP-2SG.[NOM]</ta>
            <ta e="T574" id="Seg_8378" s="T573">опять</ta>
            <ta e="T575" id="Seg_8379" s="T574">долго</ta>
            <ta e="T576" id="Seg_8380" s="T575">очень</ta>
            <ta e="T577" id="Seg_8381" s="T576">быть-PST1-3SG</ta>
            <ta e="T579" id="Seg_8382" s="T578">следующий</ta>
            <ta e="T580" id="Seg_8383" s="T579">день.[NOM]</ta>
            <ta e="T581" id="Seg_8384" s="T580">опять</ta>
            <ta e="T582" id="Seg_8385" s="T581">стоять-FUT-NEC-2SG</ta>
            <ta e="T583" id="Seg_8386" s="T582">MOD</ta>
            <ta e="T584" id="Seg_8387" s="T583">тот</ta>
            <ta e="T585" id="Seg_8388" s="T584">жевать-CVB.SIM</ta>
            <ta e="T586" id="Seg_8389" s="T585">жевать-PRS-2SG</ta>
            <ta e="T588" id="Seg_8390" s="T587">хотеть-NEG.PTCP</ta>
            <ta e="T589" id="Seg_8391" s="T588">быть-PRS-2SG</ta>
            <ta e="T590" id="Seg_8392" s="T589">сердце-EP-2SG.[NOM]</ta>
            <ta e="T591" id="Seg_8393" s="T590">тошнить-PRS.[3SG]</ta>
            <ta e="T592" id="Seg_8394" s="T591">MOD</ta>
            <ta e="T594" id="Seg_8395" s="T593">вот</ta>
            <ta e="T595" id="Seg_8396" s="T594">опять</ta>
            <ta e="T596" id="Seg_8397" s="T595">ведь</ta>
            <ta e="T597" id="Seg_8398" s="T596">жевать-PRS-2SG</ta>
            <ta e="T598" id="Seg_8399" s="T597">MOD</ta>
            <ta e="T599" id="Seg_8400" s="T598">куда</ta>
            <ta e="T600" id="Seg_8401" s="T599">быть-FUT-2SG=Q</ta>
            <ta e="T601" id="Seg_8402" s="T600">мать-2SG.[NOM]</ta>
            <ta e="T602" id="Seg_8403" s="T601">слово-3SG-ACC</ta>
            <ta e="T603" id="Seg_8404" s="T602">слышать-PRS-2SG</ta>
            <ta e="T604" id="Seg_8405" s="T603">EMPH</ta>
            <ta e="T606" id="Seg_8406" s="T605">вот</ta>
            <ta e="T607" id="Seg_8407" s="T606">тот</ta>
            <ta e="T608" id="Seg_8408" s="T607">стоять-CVB.SEQ</ta>
            <ta e="T609" id="Seg_8409" s="T608">жевать-CVB.SIM</ta>
            <ta e="T610" id="Seg_8410" s="T609">жевать-PRS-2SG</ta>
            <ta e="T611" id="Seg_8411" s="T610">вот</ta>
            <ta e="T612" id="Seg_8412" s="T611">беда-VBZ-CVB.SEQ</ta>
            <ta e="T613" id="Seg_8413" s="T612">беда-VBZ-CVB.SEQ</ta>
            <ta e="T614" id="Seg_8414" s="T613">вот</ta>
            <ta e="T615" id="Seg_8415" s="T614">кончать-PRS-1PL</ta>
            <ta e="T616" id="Seg_8416" s="T615">MOD</ta>
            <ta e="T618" id="Seg_8417" s="T617">сколько-ORD</ta>
            <ta e="T619" id="Seg_8418" s="T618">INDEF</ta>
            <ta e="T620" id="Seg_8419" s="T619">день-3SG-DAT/LOC</ta>
            <ta e="T621" id="Seg_8420" s="T620">вот</ta>
            <ta e="T622" id="Seg_8421" s="T621">тот</ta>
            <ta e="T623" id="Seg_8422" s="T622">скребок.[NOM]</ta>
            <ta e="T624" id="Seg_8423" s="T623">кто-EP-INSTR</ta>
            <ta e="T625" id="Seg_8424" s="T624">скребок.зубчатый-INSTR</ta>
            <ta e="T627" id="Seg_8425" s="T626">тот-ABL</ta>
            <ta e="T628" id="Seg_8426" s="T627">позже</ta>
            <ta e="T629" id="Seg_8427" s="T628">тот.[NOM]</ta>
            <ta e="T630" id="Seg_8428" s="T629">тонкий.слой.кожы.[NOM]</ta>
            <ta e="T631" id="Seg_8429" s="T630">EMPH</ta>
            <ta e="T632" id="Seg_8430" s="T631">совсем</ta>
            <ta e="T633" id="Seg_8431" s="T632">тонкий.слой.кожы</ta>
            <ta e="T634" id="Seg_8432" s="T633">EMPH</ta>
            <ta e="T635" id="Seg_8433" s="T634">хороший.[NOM]</ta>
            <ta e="T636" id="Seg_8434" s="T635">быть-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T637" id="Seg_8435" s="T636">мягкий.[NOM]</ta>
            <ta e="T638" id="Seg_8436" s="T637">быть-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T639" id="Seg_8437" s="T638">тот.[NOM]</ta>
            <ta e="T640" id="Seg_8438" s="T639">разный</ta>
            <ta e="T641" id="Seg_8439" s="T640">разный-ADVZ</ta>
            <ta e="T642" id="Seg_8440" s="T641">тот.[NOM]</ta>
            <ta e="T643" id="Seg_8441" s="T642">кто-VBZ-CVB.SIM</ta>
            <ta e="T644" id="Seg_8442" s="T643">мочь-PST1-1PL</ta>
            <ta e="T645" id="Seg_8443" s="T644">EMPH</ta>
            <ta e="T647" id="Seg_8444" s="T646">скребок-ABL</ta>
            <ta e="T648" id="Seg_8445" s="T647">кожемялка-INSTR</ta>
            <ta e="T649" id="Seg_8446" s="T648">делать-PRS-1PL</ta>
            <ta e="T651" id="Seg_8447" s="T650">тот-3SG-2SG.[NOM]</ta>
            <ta e="T652" id="Seg_8448" s="T651">тонкий.слой.кожы.[NOM]</ta>
            <ta e="T653" id="Seg_8449" s="T652">очень</ta>
            <ta e="T654" id="Seg_8450" s="T653">тонкий</ta>
            <ta e="T655" id="Seg_8451" s="T654">быть-CVB.PURP</ta>
            <ta e="T656" id="Seg_8452" s="T655">тоже</ta>
            <ta e="T657" id="Seg_8453" s="T656">ведь</ta>
            <ta e="T658" id="Seg_8454" s="T657">плохой.[NOM]</ta>
            <ta e="T660" id="Seg_8455" s="T659">тот.[NOM]</ta>
            <ta e="T661" id="Seg_8456" s="T660">из_за</ta>
            <ta e="T662" id="Seg_8457" s="T661">тонкий.слой.кожы-2SG-ACC</ta>
            <ta e="T663" id="Seg_8458" s="T662">тот.[NOM]</ta>
            <ta e="T664" id="Seg_8459" s="T663">кто-DAT/LOC</ta>
            <ta e="T665" id="Seg_8460" s="T664">коптить-PRS-3PL</ta>
            <ta e="T666" id="Seg_8461" s="T665">коптить-PRS-3PL</ta>
            <ta e="T667" id="Seg_8462" s="T666">дым-DAT/LOC</ta>
            <ta e="T669" id="Seg_8463" s="T668">тот</ta>
            <ta e="T670" id="Seg_8464" s="T669">тонкий.слой.кожы-2SG-ACC</ta>
            <ta e="T671" id="Seg_8465" s="T670">взять-CVB.SEQ</ta>
            <ta e="T672" id="Seg_8466" s="T671">идти-CVB.SEQ-3PL</ta>
            <ta e="T673" id="Seg_8467" s="T672">дом.[NOM]</ta>
            <ta e="T674" id="Seg_8468" s="T673">нутро-3SG-DAT/LOC</ta>
            <ta e="T675" id="Seg_8469" s="T674">повесить-PRS-3PL</ta>
            <ta e="T677" id="Seg_8470" s="T676">дом.[NOM]</ta>
            <ta e="T678" id="Seg_8471" s="T677">да</ta>
            <ta e="T679" id="Seg_8472" s="T678">быть-FUT.[3SG]=Q</ta>
            <ta e="T680" id="Seg_8473" s="T679">тот.[NOM]</ta>
            <ta e="T681" id="Seg_8474" s="T680">кто.[NOM]</ta>
            <ta e="T682" id="Seg_8475" s="T681">чум.[NOM]</ta>
            <ta e="T683" id="Seg_8476" s="T682">нутро-3SG-DAT/LOC</ta>
            <ta e="T684" id="Seg_8477" s="T683">чум.[NOM]</ta>
            <ta e="T685" id="Seg_8478" s="T684">дом.[NOM]</ta>
            <ta e="T686" id="Seg_8479" s="T685">нутро-3SG-DAT/LOC</ta>
            <ta e="T687" id="Seg_8480" s="T686">повесить-PRS-3PL</ta>
            <ta e="T689" id="Seg_8481" s="T688">там</ta>
            <ta e="T690" id="Seg_8482" s="T689">дым-DAT/LOC</ta>
            <ta e="T691" id="Seg_8483" s="T690">тот-3SG-2SG.[NOM]</ta>
            <ta e="T692" id="Seg_8484" s="T691">близость.[NOM]</ta>
            <ta e="T693" id="Seg_8485" s="T692">стоять-PRS.[3SG]</ta>
            <ta e="T694" id="Seg_8486" s="T693">EMPH</ta>
            <ta e="T696" id="Seg_8487" s="T695">а</ta>
            <ta e="T697" id="Seg_8488" s="T696">некоторый</ta>
            <ta e="T698" id="Seg_8489" s="T697">жена-PL.[NOM]</ta>
            <ta e="T699" id="Seg_8490" s="T698">так</ta>
            <ta e="T700" id="Seg_8491" s="T699">делать-HAB-3PL</ta>
            <ta e="T701" id="Seg_8492" s="T700">MOD</ta>
            <ta e="T703" id="Seg_8493" s="T702">тот.[NOM]</ta>
            <ta e="T704" id="Seg_8494" s="T703">подобно</ta>
            <ta e="T705" id="Seg_8495" s="T704">да</ta>
            <ta e="T706" id="Seg_8496" s="T705">коптить-CVB.SIM</ta>
            <ta e="T707" id="Seg_8497" s="T706">лежать-PTCP.FUT</ta>
            <ta e="T708" id="Seg_8498" s="T707">быть-PST2-3PL</ta>
            <ta e="T709" id="Seg_8499" s="T708">EMPH</ta>
            <ta e="T711" id="Seg_8500" s="T710">а</ta>
            <ta e="T712" id="Seg_8501" s="T711">3SG.[NOM]</ta>
            <ta e="T713" id="Seg_8502" s="T712">3PL-EP-2SG.[NOM]</ta>
            <ta e="T714" id="Seg_8503" s="T713">вот.так</ta>
            <ta e="T715" id="Seg_8504" s="T714">говорить-PRS-3PL</ta>
            <ta e="T716" id="Seg_8505" s="T715">EMPH</ta>
            <ta e="T717" id="Seg_8506" s="T716">1PL-ACC</ta>
            <ta e="T718" id="Seg_8507" s="T717">стоять-CAUS-PRS-3PL</ta>
            <ta e="T719" id="Seg_8508" s="T718">EMPH</ta>
            <ta e="T720" id="Seg_8509" s="T719">три</ta>
            <ta e="T721" id="Seg_8510" s="T720">четыре</ta>
            <ta e="T722" id="Seg_8511" s="T721">девушка.[NOM]</ta>
            <ta e="T723" id="Seg_8512" s="T722">девушка.[NOM]</ta>
            <ta e="T724" id="Seg_8513" s="T723">ребенок-PL.[NOM]</ta>
            <ta e="T868" id="Seg_8514" s="T724">кто.[NOM]</ta>
            <ta e="T869" id="Seg_8515" s="T868">кто.[NOM]</ta>
            <ta e="T870" id="Seg_8516" s="T869">огонь-3SG.[NOM]</ta>
            <ta e="T871" id="Seg_8517" s="T870">кто.[NOM]</ta>
            <ta e="T872" id="Seg_8518" s="T871">огонь-3SG.[NOM]</ta>
            <ta e="T873" id="Seg_8519" s="T872">тот.[NOM]</ta>
            <ta e="T874" id="Seg_8520" s="T873">очаг.[NOM]</ta>
            <ta e="T725" id="Seg_8521" s="T874">делать-PRS-3PL</ta>
            <ta e="T726" id="Seg_8522" s="T725">дерево.[NOM]</ta>
            <ta e="T727" id="Seg_8523" s="T726">загораться-PRS.[3SG]</ta>
            <ta e="T728" id="Seg_8524" s="T727">тот-3SG-2SG.[NOM]</ta>
            <ta e="T729" id="Seg_8525" s="T728">верхняя.часть-3SG-INSTR</ta>
            <ta e="T730" id="Seg_8526" s="T729">трава-PL-ACC</ta>
            <ta e="T731" id="Seg_8527" s="T730">бросать-PRS-3PL</ta>
            <ta e="T732" id="Seg_8528" s="T731">большой</ta>
            <ta e="T733" id="Seg_8529" s="T732">дым.[NOM]</ta>
            <ta e="T734" id="Seg_8530" s="T733">приходить-PTCP.FUT.[3SG]-ACC</ta>
            <ta e="T736" id="Seg_8531" s="T735">вот</ta>
            <ta e="T737" id="Seg_8532" s="T736">тот</ta>
            <ta e="T738" id="Seg_8533" s="T737">дым.[NOM]</ta>
            <ta e="T739" id="Seg_8534" s="T738">нутро-3SG-DAT/LOC</ta>
            <ta e="T740" id="Seg_8535" s="T739">вокруг</ta>
            <ta e="T741" id="Seg_8536" s="T740">стоять-CVB.SEQ-1PL</ta>
            <ta e="T742" id="Seg_8537" s="T741">тот</ta>
            <ta e="T743" id="Seg_8538" s="T742">держать-PRS-1PL</ta>
            <ta e="T744" id="Seg_8539" s="T743">MOD</ta>
            <ta e="T745" id="Seg_8540" s="T744">кто-ACC</ta>
            <ta e="T747" id="Seg_8541" s="T746">тот</ta>
            <ta e="T748" id="Seg_8542" s="T747">делать-CVB.SEQ</ta>
            <ta e="T749" id="Seg_8543" s="T748">после</ta>
            <ta e="T750" id="Seg_8544" s="T749">вертеться-CAUS-PRS-1PL</ta>
            <ta e="T751" id="Seg_8545" s="T750">солнце.[NOM]</ta>
            <ta e="T752" id="Seg_8546" s="T751">след-3SG-ACC</ta>
            <ta e="T753" id="Seg_8547" s="T752">подобно</ta>
            <ta e="T754" id="Seg_8548" s="T753">вертеться-CAUS-PRS-1PL</ta>
            <ta e="T756" id="Seg_8549" s="T755">хороший-ADVZ</ta>
            <ta e="T757" id="Seg_8550" s="T756">держать-FUT-NEC-2SG</ta>
            <ta e="T758" id="Seg_8551" s="T757">тот-3SG-2SG-ACC</ta>
            <ta e="T759" id="Seg_8552" s="T758">пустить-NEG.PTCP.[NOM]</ta>
            <ta e="T760" id="Seg_8553" s="T759">подобно</ta>
            <ta e="T762" id="Seg_8554" s="T761">сам</ta>
            <ta e="T763" id="Seg_8555" s="T762">сам-1PL-DAT/LOC</ta>
            <ta e="T764" id="Seg_8556" s="T763">вертеться-CAUS-PRS-1PL</ta>
            <ta e="T765" id="Seg_8557" s="T764">может</ta>
            <ta e="T766" id="Seg_8558" s="T765">вот.так</ta>
            <ta e="T768" id="Seg_8559" s="T767">1SG.[NOM]</ta>
            <ta e="T769" id="Seg_8560" s="T768">друг-1SG-DAT/LOC</ta>
            <ta e="T770" id="Seg_8561" s="T769">давать-FUT-1SG</ta>
            <ta e="T771" id="Seg_8562" s="T770">друг-EP-1SG.[NOM]</ta>
            <ta e="T772" id="Seg_8563" s="T771">следующий</ta>
            <ta e="T773" id="Seg_8564" s="T772">друг-2SG-DAT/LOC</ta>
            <ta e="T774" id="Seg_8565" s="T773">так-INTNS</ta>
            <ta e="T775" id="Seg_8566" s="T774">давать-PRS.[3SG]</ta>
            <ta e="T777" id="Seg_8567" s="T776">обходить-CAUS-PRS-1PL</ta>
            <ta e="T778" id="Seg_8568" s="T777">тот</ta>
            <ta e="T779" id="Seg_8569" s="T778">шкура-ACC</ta>
            <ta e="T781" id="Seg_8570" s="T780">нижняя.часть-2PL-ABL</ta>
            <ta e="T782" id="Seg_8571" s="T781">большой</ta>
            <ta e="T783" id="Seg_8572" s="T782">дым.[NOM]</ta>
            <ta e="T784" id="Seg_8573" s="T783">приходить-PRS.[3SG]</ta>
            <ta e="T785" id="Seg_8574" s="T784">EMPH</ta>
            <ta e="T787" id="Seg_8575" s="T786">1PL-DAT/LOC</ta>
            <ta e="T788" id="Seg_8576" s="T787">опять</ta>
            <ta e="T789" id="Seg_8577" s="T788">плохой</ta>
            <ta e="T790" id="Seg_8578" s="T789">тот</ta>
            <ta e="T791" id="Seg_8579" s="T790">дым.[NOM]</ta>
            <ta e="T792" id="Seg_8580" s="T791">приходить-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T794" id="Seg_8581" s="T793">глаз-1PL.[NOM]</ta>
            <ta e="T795" id="Seg_8582" s="T794">щипать-PRS.[3SG]</ta>
            <ta e="T796" id="Seg_8583" s="T795">MOD</ta>
            <ta e="T797" id="Seg_8584" s="T796">тот</ta>
            <ta e="T798" id="Seg_8585" s="T797">дым-ABL</ta>
            <ta e="T800" id="Seg_8586" s="T799">рот-1PL-DAT/LOC</ta>
            <ta e="T801" id="Seg_8587" s="T800">да</ta>
            <ta e="T802" id="Seg_8588" s="T801">взять-PRS-1PL</ta>
            <ta e="T803" id="Seg_8589" s="T802">MOD</ta>
            <ta e="T804" id="Seg_8590" s="T803">дым-ACC</ta>
            <ta e="T806" id="Seg_8591" s="T805">вот</ta>
            <ta e="T807" id="Seg_8592" s="T806">тот</ta>
            <ta e="T808" id="Seg_8593" s="T807">да</ta>
            <ta e="T809" id="Seg_8594" s="T808">быть-COND.[3SG]</ta>
            <ta e="T810" id="Seg_8595" s="T809">вот</ta>
            <ta e="T811" id="Seg_8596" s="T810">стоять-EP-REFL-FREQ-PRS-1PL</ta>
            <ta e="T812" id="Seg_8597" s="T811">EMPH</ta>
            <ta e="T813" id="Seg_8598" s="T812">стоять-EP-IMP.2PL</ta>
            <ta e="T814" id="Seg_8599" s="T813">стоять-EP-IMP.2PL</ta>
            <ta e="T815" id="Seg_8600" s="T814">говорить-PRS-3PL</ta>
            <ta e="T816" id="Seg_8601" s="T815">EMPH</ta>
            <ta e="T817" id="Seg_8602" s="T816">1PL-ACC</ta>
            <ta e="T819" id="Seg_8603" s="T818">вот</ta>
            <ta e="T820" id="Seg_8604" s="T819">тот.[NOM]</ta>
            <ta e="T821" id="Seg_8605" s="T820">подобно</ta>
            <ta e="T822" id="Seg_8606" s="T821">вертеться-CAUS-PRS-1PL</ta>
            <ta e="T824" id="Seg_8607" s="T823">тот-ABL</ta>
            <ta e="T825" id="Seg_8608" s="T824">один</ta>
            <ta e="T826" id="Seg_8609" s="T825">INDEF</ta>
            <ta e="T827" id="Seg_8610" s="T826">девушка.[NOM]</ta>
            <ta e="T828" id="Seg_8611" s="T827">ребенок-3SG.[NOM]</ta>
            <ta e="T829" id="Seg_8612" s="T828">шкура-ACC</ta>
            <ta e="T830" id="Seg_8613" s="T829">рука-3SG-INSTR</ta>
            <ta e="T831" id="Seg_8614" s="T830">вот.так-EP-2SG</ta>
            <ta e="T832" id="Seg_8615" s="T831">взять-CVB.SEQ</ta>
            <ta e="T833" id="Seg_8616" s="T832">бросать-FUT.[3SG]</ta>
            <ta e="T834" id="Seg_8617" s="T833">ошибка</ta>
            <ta e="T835" id="Seg_8618" s="T834">держать-FUT.[3SG]</ta>
            <ta e="T836" id="Seg_8619" s="T835">тот-3SG-3SG-ACC</ta>
            <ta e="T837" id="Seg_8620" s="T836">тот</ta>
            <ta e="T838" id="Seg_8621" s="T837">держать-PTCP.PRS</ta>
            <ta e="T839" id="Seg_8622" s="T838">место-3SG-ACC</ta>
            <ta e="T840" id="Seg_8623" s="T839">кожа-3SG-ACC</ta>
            <ta e="T841" id="Seg_8624" s="T840">вот</ta>
            <ta e="T842" id="Seg_8625" s="T841">тогда</ta>
            <ta e="T843" id="Seg_8626" s="T842">говорить-PRS-3PL</ta>
            <ta e="T844" id="Seg_8627" s="T843">EMPH</ta>
            <ta e="T845" id="Seg_8628" s="T844">вот</ta>
            <ta e="T846" id="Seg_8629" s="T845">муж-DAT/LOC</ta>
            <ta e="T847" id="Seg_8630" s="T846">идти-FUT-2SG</ta>
            <ta e="T848" id="Seg_8631" s="T847">NEG-3SG</ta>
            <ta e="T849" id="Seg_8632" s="T848">расти-TEMP-2SG</ta>
            <ta e="T851" id="Seg_8633" s="T850">говорить-CVB.SIM</ta>
            <ta e="T852" id="Seg_8634" s="T851">говорить-PRS-3PL</ta>
            <ta e="T853" id="Seg_8635" s="T852">так-INTNS</ta>
            <ta e="T854" id="Seg_8636" s="T853">говорить-PRS-3PL</ta>
            <ta e="T855" id="Seg_8637" s="T854">EMPH</ta>
            <ta e="T856" id="Seg_8638" s="T855">1PL-ACC</ta>
            <ta e="T858" id="Seg_8639" s="T857">тот.[NOM]</ta>
            <ta e="T859" id="Seg_8640" s="T858">из_за</ta>
            <ta e="T860" id="Seg_8641" s="T859">тот</ta>
            <ta e="T861" id="Seg_8642" s="T860">беда-VBZ-CVB.SIM</ta>
            <ta e="T862" id="Seg_8643" s="T861">стоять-PRS-1PL</ta>
            <ta e="T863" id="Seg_8644" s="T862">EMPH</ta>
            <ta e="T864" id="Seg_8645" s="T863">тот</ta>
            <ta e="T865" id="Seg_8646" s="T864">шкура-ACC</ta>
            <ta e="T866" id="Seg_8647" s="T865">пустить-NEG.CVB.SIM</ta>
            <ta e="T867" id="Seg_8648" s="T866">прочь</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T2" id="Seg_8649" s="T1">v-v:cvb</ta>
            <ta e="T4" id="Seg_8650" s="T3">n-n:case</ta>
            <ta e="T5" id="Seg_8651" s="T4">n-n:case</ta>
            <ta e="T6" id="Seg_8652" s="T5">post</ta>
            <ta e="T7" id="Seg_8653" s="T6">v-v:cvb</ta>
            <ta e="T8" id="Seg_8654" s="T7">v-v:cvb</ta>
            <ta e="T9" id="Seg_8655" s="T8">v-v:cvb-v:pred.pn</ta>
            <ta e="T10" id="Seg_8656" s="T9">n-n:(num).[n:case]</ta>
            <ta e="T11" id="Seg_8657" s="T10">n.[n:case]</ta>
            <ta e="T12" id="Seg_8658" s="T11">v-v:ptcp</ta>
            <ta e="T13" id="Seg_8659" s="T12">n-n:(poss).[n:case]</ta>
            <ta e="T14" id="Seg_8660" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_8661" s="T14">v-v:mood.[v:pred.pn]</ta>
            <ta e="T17" id="Seg_8662" s="T16">adv</ta>
            <ta e="T18" id="Seg_8663" s="T17">quant</ta>
            <ta e="T19" id="Seg_8664" s="T18">ptcl</ta>
            <ta e="T20" id="Seg_8665" s="T19">n.[n:case]</ta>
            <ta e="T21" id="Seg_8666" s="T20">n-n:(num).[n:case]</ta>
            <ta e="T22" id="Seg_8667" s="T21">n.[n:case]</ta>
            <ta e="T23" id="Seg_8668" s="T22">n-n:(num).[n:case]</ta>
            <ta e="T24" id="Seg_8669" s="T23">v-v:mood-v:pred.pn</ta>
            <ta e="T26" id="Seg_8670" s="T25">n-n&gt;adj</ta>
            <ta e="T27" id="Seg_8671" s="T26">adj</ta>
            <ta e="T28" id="Seg_8672" s="T27">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T29" id="Seg_8673" s="T28">n.[n:case]</ta>
            <ta e="T30" id="Seg_8674" s="T29">v-v:ptcp</ta>
            <ta e="T31" id="Seg_8675" s="T30">v-v:mood-v:temp.pn</ta>
            <ta e="T32" id="Seg_8676" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_8677" s="T32">n-n:(num).[n:case]</ta>
            <ta e="T34" id="Seg_8678" s="T33">v-v:ptcp-v:(poss).[v:(case)]</ta>
            <ta e="T35" id="Seg_8679" s="T34">v-v:tense.[v:pred.pn]</ta>
            <ta e="T37" id="Seg_8680" s="T36">adv</ta>
            <ta e="T38" id="Seg_8681" s="T37">pers-pro:(ins)-pro:(poss).[pro:case]</ta>
            <ta e="T39" id="Seg_8682" s="T38">adv</ta>
            <ta e="T40" id="Seg_8683" s="T39">dempro.[pro:case]</ta>
            <ta e="T41" id="Seg_8684" s="T40">n.[n:case]</ta>
            <ta e="T42" id="Seg_8685" s="T41">n-n:poss-n:case</ta>
            <ta e="T43" id="Seg_8686" s="T42">v-v:tense-v:pred.pn</ta>
            <ta e="T45" id="Seg_8687" s="T44">n.[n:case]</ta>
            <ta e="T46" id="Seg_8688" s="T45">n-n:poss-n:case</ta>
            <ta e="T47" id="Seg_8689" s="T46">adv</ta>
            <ta e="T48" id="Seg_8690" s="T47">adv</ta>
            <ta e="T49" id="Seg_8691" s="T48">v-v:tense-v:pred.pn</ta>
            <ta e="T50" id="Seg_8692" s="T49">adv</ta>
            <ta e="T52" id="Seg_8693" s="T51">v-v:cvb</ta>
            <ta e="T53" id="Seg_8694" s="T52">v-v:cvb-v:pred.pn</ta>
            <ta e="T54" id="Seg_8695" s="T53">n-n:(ins)-n:case</ta>
            <ta e="T55" id="Seg_8696" s="T54">v-v:tense-v:pred.pn</ta>
            <ta e="T57" id="Seg_8697" s="T56">n-n:poss-n:case</ta>
            <ta e="T59" id="Seg_8698" s="T58">dempro</ta>
            <ta e="T60" id="Seg_8699" s="T59">n-n:(ins)-n:case</ta>
            <ta e="T61" id="Seg_8700" s="T60">v-v:cvb</ta>
            <ta e="T62" id="Seg_8701" s="T61">v-v:mood-v:temp.pn</ta>
            <ta e="T63" id="Seg_8702" s="T62">dempro-pro:case</ta>
            <ta e="T64" id="Seg_8703" s="T63">adv</ta>
            <ta e="T65" id="Seg_8704" s="T64">n-n:(ins)-n:case</ta>
            <ta e="T66" id="Seg_8705" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_8706" s="T66">v-v:tense-v:pred.pn</ta>
            <ta e="T69" id="Seg_8707" s="T68">n.[n:case]</ta>
            <ta e="T70" id="Seg_8708" s="T69">n-n:(num).[n:case]</ta>
            <ta e="T71" id="Seg_8709" s="T70">adv</ta>
            <ta e="T72" id="Seg_8710" s="T71">adj</ta>
            <ta e="T73" id="Seg_8711" s="T72">que-pro:case</ta>
            <ta e="T74" id="Seg_8712" s="T73">n-n:case</ta>
            <ta e="T75" id="Seg_8713" s="T74">v-v:ptcp-v:(poss)</ta>
            <ta e="T76" id="Seg_8714" s="T75">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T78" id="Seg_8715" s="T77">v-v&gt;v-v:(ins)-v:ptcp</ta>
            <ta e="T79" id="Seg_8716" s="T78">dempro.[pro:case]</ta>
            <ta e="T80" id="Seg_8717" s="T79">n-n:poss-n:case</ta>
            <ta e="T81" id="Seg_8718" s="T80">v-v:(ins)-v&gt;v-v:(ins)-v:ptcp-v:(case)</ta>
            <ta e="T83" id="Seg_8719" s="T82">v-v:ptcp</ta>
            <ta e="T84" id="Seg_8720" s="T83">n-n:case</ta>
            <ta e="T85" id="Seg_8721" s="T84">que-pro:case</ta>
            <ta e="T86" id="Seg_8722" s="T85">n-n:case</ta>
            <ta e="T87" id="Seg_8723" s="T86">v-v:cvb-v:pred.pn</ta>
            <ta e="T88" id="Seg_8724" s="T87">que.[pro:case]</ta>
            <ta e="T89" id="Seg_8725" s="T88">n.[n:case]</ta>
            <ta e="T90" id="Seg_8726" s="T89">n-n:(poss).[n:case]</ta>
            <ta e="T91" id="Seg_8727" s="T90">dempro.[pro:case]</ta>
            <ta e="T92" id="Seg_8728" s="T91">n-n:(num)-n:case</ta>
            <ta e="T93" id="Seg_8729" s="T92">v-v:mood-v:pred.pn</ta>
            <ta e="T95" id="Seg_8730" s="T94">dempro</ta>
            <ta e="T96" id="Seg_8731" s="T95">ptcl</ta>
            <ta e="T97" id="Seg_8732" s="T96">ptcl</ta>
            <ta e="T98" id="Seg_8733" s="T97">dempro</ta>
            <ta e="T99" id="Seg_8734" s="T98">adv</ta>
            <ta e="T100" id="Seg_8735" s="T99">n-n:case</ta>
            <ta e="T101" id="Seg_8736" s="T100">v-v:tense-v:pred.pn</ta>
            <ta e="T102" id="Seg_8737" s="T101">ptcl</ta>
            <ta e="T104" id="Seg_8738" s="T103">dempro-pro:case</ta>
            <ta e="T105" id="Seg_8739" s="T104">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T106" id="Seg_8740" s="T105">n.[n:case]</ta>
            <ta e="T107" id="Seg_8741" s="T106">v-v:tense-v:pred.pn</ta>
            <ta e="T109" id="Seg_8742" s="T108">dempro</ta>
            <ta e="T111" id="Seg_8743" s="T110">indfpro</ta>
            <ta e="T112" id="Seg_8744" s="T111">n-n:case</ta>
            <ta e="T113" id="Seg_8745" s="T112">pers-pro:(ins)-pro:(poss).[pro:case]</ta>
            <ta e="T114" id="Seg_8746" s="T113">dempro.[pro:case]</ta>
            <ta e="T115" id="Seg_8747" s="T114">n.[n:case]</ta>
            <ta e="T116" id="Seg_8748" s="T115">v-v:tense-v:mood-v:pred.pn</ta>
            <ta e="T118" id="Seg_8749" s="T117">n.[n:case]</ta>
            <ta e="T119" id="Seg_8750" s="T118">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T120" id="Seg_8751" s="T119">adv</ta>
            <ta e="T121" id="Seg_8752" s="T120">ptcl</ta>
            <ta e="T122" id="Seg_8753" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_8754" s="T122">quant</ta>
            <ta e="T124" id="Seg_8755" s="T123">n.[n:case]</ta>
            <ta e="T126" id="Seg_8756" s="T125">v-v:tense-v:pred.pn</ta>
            <ta e="T127" id="Seg_8757" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_8758" s="T127">dempro.[pro:case]</ta>
            <ta e="T129" id="Seg_8759" s="T128">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T130" id="Seg_8760" s="T129">post</ta>
            <ta e="T131" id="Seg_8761" s="T130">n-n:(poss).[n:case]</ta>
            <ta e="T132" id="Seg_8762" s="T131">ptcl</ta>
            <ta e="T133" id="Seg_8763" s="T132">ptcl</ta>
            <ta e="T134" id="Seg_8764" s="T133">n-n:(poss).[n:case]</ta>
            <ta e="T135" id="Seg_8765" s="T134">quant</ta>
            <ta e="T136" id="Seg_8766" s="T135">v-v:ptcp.[v:(poss)]-v:(case)</ta>
            <ta e="T137" id="Seg_8767" s="T136">ptcl</ta>
            <ta e="T138" id="Seg_8768" s="T137">n-n:(poss).[n:case]</ta>
            <ta e="T139" id="Seg_8769" s="T138">v-v:tense.[v:pred.pn]</ta>
            <ta e="T140" id="Seg_8770" s="T139">ptcl</ta>
            <ta e="T141" id="Seg_8771" s="T140">n-n:poss-n:case</ta>
            <ta e="T143" id="Seg_8772" s="T142">dempro</ta>
            <ta e="T144" id="Seg_8773" s="T143">n.[n:case]</ta>
            <ta e="T145" id="Seg_8774" s="T144">n-n:(poss).[n:case]</ta>
            <ta e="T146" id="Seg_8775" s="T145">adj-adj&gt;adv</ta>
            <ta e="T147" id="Seg_8776" s="T146">adv</ta>
            <ta e="T148" id="Seg_8777" s="T147">v-v:ptcp.[v:(poss)]-v:(case)</ta>
            <ta e="T149" id="Seg_8778" s="T148">dempro-pro:case</ta>
            <ta e="T150" id="Seg_8779" s="T149">adv</ta>
            <ta e="T151" id="Seg_8780" s="T150">v-v:ptcp-v:(case)</ta>
            <ta e="T152" id="Seg_8781" s="T151">ptcl</ta>
            <ta e="T153" id="Seg_8782" s="T152">adj</ta>
            <ta e="T154" id="Seg_8783" s="T153">ptcl</ta>
            <ta e="T155" id="Seg_8784" s="T154">n.[n:case]</ta>
            <ta e="T156" id="Seg_8785" s="T155">v-v:ptcp.[v:(poss)]-v:(case)</ta>
            <ta e="T157" id="Seg_8786" s="T156">dempro</ta>
            <ta e="T158" id="Seg_8787" s="T157">n-n&gt;adj</ta>
            <ta e="T159" id="Seg_8788" s="T158">que.[pro:case]</ta>
            <ta e="T160" id="Seg_8789" s="T159">n-n:case</ta>
            <ta e="T161" id="Seg_8790" s="T160">pers-pro:(ins)-pro:(poss).[pro:case]</ta>
            <ta e="T162" id="Seg_8791" s="T161">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T163" id="Seg_8792" s="T162">n-n:case</ta>
            <ta e="T165" id="Seg_8793" s="T164">n.[n:case]</ta>
            <ta e="T166" id="Seg_8794" s="T165">n-n:poss-n:case</ta>
            <ta e="T167" id="Seg_8795" s="T166">n-n:case</ta>
            <ta e="T168" id="Seg_8796" s="T167">v-v:tense-v:pred.pn</ta>
            <ta e="T169" id="Seg_8797" s="T168">v-v:tense-v:pred.pn</ta>
            <ta e="T171" id="Seg_8798" s="T170">dempro-pro:case</ta>
            <ta e="T172" id="Seg_8799" s="T171">adj-adj</ta>
            <ta e="T173" id="Seg_8800" s="T172">n-n:(num)-n:case</ta>
            <ta e="T175" id="Seg_8801" s="T174">dempro</ta>
            <ta e="T176" id="Seg_8802" s="T175">v-v:cvb</ta>
            <ta e="T177" id="Seg_8803" s="T176">v-v:cvb</ta>
            <ta e="T178" id="Seg_8804" s="T177">que</ta>
            <ta e="T179" id="Seg_8805" s="T178">ptcl</ta>
            <ta e="T180" id="Seg_8806" s="T179">n-n:case</ta>
            <ta e="T181" id="Seg_8807" s="T180">v-v:tense-v:mood.[v:pred.pn]</ta>
            <ta e="T183" id="Seg_8808" s="T182">adj</ta>
            <ta e="T184" id="Seg_8809" s="T183">conj</ta>
            <ta e="T185" id="Seg_8810" s="T184">n.[n:case]</ta>
            <ta e="T186" id="Seg_8811" s="T185">v-v:mood.pn</ta>
            <ta e="T187" id="Seg_8812" s="T186">dempro</ta>
            <ta e="T188" id="Seg_8813" s="T187">n-n:(poss).[n:case]</ta>
            <ta e="T189" id="Seg_8814" s="T188">v-v:tense.[v:pred.pn]</ta>
            <ta e="T190" id="Seg_8815" s="T189">ptcl</ta>
            <ta e="T191" id="Seg_8816" s="T190">n-n:(poss).[n:case]</ta>
            <ta e="T192" id="Seg_8817" s="T191">ptcl</ta>
            <ta e="T193" id="Seg_8818" s="T192">v-v:tense.[v:pred.pn]</ta>
            <ta e="T194" id="Seg_8819" s="T193">ptcl</ta>
            <ta e="T195" id="Seg_8820" s="T194">adj.[n:case]</ta>
            <ta e="T196" id="Seg_8821" s="T195">v-v:tense.[v:pred.pn]</ta>
            <ta e="T197" id="Seg_8822" s="T196">ptcl</ta>
            <ta e="T199" id="Seg_8823" s="T198">n-n:(poss).[n:case]</ta>
            <ta e="T200" id="Seg_8824" s="T199">que</ta>
            <ta e="T201" id="Seg_8825" s="T200">ptcl</ta>
            <ta e="T202" id="Seg_8826" s="T201">n-n:case</ta>
            <ta e="T203" id="Seg_8827" s="T202">v-v:cvb</ta>
            <ta e="T204" id="Seg_8828" s="T203">v-v:mood-v:temp.pn</ta>
            <ta e="T205" id="Seg_8829" s="T204">pers-pro:(ins)-pro:(poss).[pro:case]</ta>
            <ta e="T206" id="Seg_8830" s="T205">dempro</ta>
            <ta e="T207" id="Seg_8831" s="T206">v-v:tense-v:pred.pn</ta>
            <ta e="T209" id="Seg_8832" s="T208">dempro</ta>
            <ta e="T210" id="Seg_8833" s="T209">n-n:poss-n:case</ta>
            <ta e="T211" id="Seg_8834" s="T210">v-v:cvb</ta>
            <ta e="T212" id="Seg_8835" s="T211">v-v:cvb-v:pred.pn</ta>
            <ta e="T213" id="Seg_8836" s="T212">dempro.[pro:case]</ta>
            <ta e="T214" id="Seg_8837" s="T213">que.[pro:case]</ta>
            <ta e="T215" id="Seg_8838" s="T214">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T216" id="Seg_8839" s="T215">n-n:case</ta>
            <ta e="T217" id="Seg_8840" s="T216">v-v:cvb-v:pred.pn</ta>
            <ta e="T219" id="Seg_8841" s="T218">adv</ta>
            <ta e="T220" id="Seg_8842" s="T219">v-v&gt;v-v:tense-v:tense-v:pred.pn</ta>
            <ta e="T221" id="Seg_8843" s="T220">dempro</ta>
            <ta e="T222" id="Seg_8844" s="T221">v-v:cvb</ta>
            <ta e="T223" id="Seg_8845" s="T222">post</ta>
            <ta e="T224" id="Seg_8846" s="T223">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T226" id="Seg_8847" s="T225">v-v&gt;v-v:cvb</ta>
            <ta e="T227" id="Seg_8848" s="T226">v-v:cvb-v:poss.pn</ta>
            <ta e="T228" id="Seg_8849" s="T227">dempro.[pro:case]</ta>
            <ta e="T229" id="Seg_8850" s="T228">n-n:poss-n:case</ta>
            <ta e="T230" id="Seg_8851" s="T229">que-que&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T231" id="Seg_8852" s="T230">n-n:(ins)-n:case</ta>
            <ta e="T232" id="Seg_8853" s="T231">dempro</ta>
            <ta e="T233" id="Seg_8854" s="T232">que.[pro:case]</ta>
            <ta e="T234" id="Seg_8855" s="T233">v-v:ptcp</ta>
            <ta e="T235" id="Seg_8856" s="T234">n-n:poss-n:case</ta>
            <ta e="T236" id="Seg_8857" s="T235">v-v:tense-v:pred.pn</ta>
            <ta e="T238" id="Seg_8858" s="T237">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T239" id="Seg_8859" s="T238">adj-adj&gt;adv</ta>
            <ta e="T240" id="Seg_8860" s="T239">v-v:ptcp-v:(case)</ta>
            <ta e="T242" id="Seg_8861" s="T241">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T243" id="Seg_8862" s="T242">ptcl</ta>
            <ta e="T244" id="Seg_8863" s="T243">adj.[n:case]</ta>
            <ta e="T245" id="Seg_8864" s="T244">ptcl</ta>
            <ta e="T246" id="Seg_8865" s="T245">v-v:neg.[v:pred.pn]</ta>
            <ta e="T247" id="Seg_8866" s="T246">adj.[n:case]</ta>
            <ta e="T248" id="Seg_8867" s="T247">ptcl</ta>
            <ta e="T249" id="Seg_8868" s="T248">v-v:neg.[v:pred.pn]</ta>
            <ta e="T250" id="Seg_8869" s="T249">n-n:(num).[n:case]</ta>
            <ta e="T251" id="Seg_8870" s="T250">adj</ta>
            <ta e="T252" id="Seg_8871" s="T251">que.[pro:case]</ta>
            <ta e="T253" id="Seg_8872" s="T252">adj</ta>
            <ta e="T254" id="Seg_8873" s="T253">n.[n:case]</ta>
            <ta e="T255" id="Seg_8874" s="T254">dempro.[pro:case]</ta>
            <ta e="T256" id="Seg_8875" s="T255">n-n:case</ta>
            <ta e="T257" id="Seg_8876" s="T256">ptcl</ta>
            <ta e="T259" id="Seg_8877" s="T258">dempro.[pro:case]</ta>
            <ta e="T260" id="Seg_8878" s="T259">post</ta>
            <ta e="T261" id="Seg_8879" s="T260">adv</ta>
            <ta e="T262" id="Seg_8880" s="T261">v-v:cvb</ta>
            <ta e="T263" id="Seg_8881" s="T262">dempro-pro:case</ta>
            <ta e="T264" id="Seg_8882" s="T263">adj-n:poss-n:case</ta>
            <ta e="T267" id="Seg_8883" s="T266">v-v:tense-v:poss.pn</ta>
            <ta e="T268" id="Seg_8884" s="T267">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T269" id="Seg_8885" s="T268">adj.[n:case]</ta>
            <ta e="T270" id="Seg_8886" s="T269">n.[n:case]</ta>
            <ta e="T272" id="Seg_8887" s="T271">dempro-pro:case</ta>
            <ta e="T273" id="Seg_8888" s="T272">adj</ta>
            <ta e="T274" id="Seg_8889" s="T273">n.[n:case]</ta>
            <ta e="T275" id="Seg_8890" s="T274">ptcl</ta>
            <ta e="T276" id="Seg_8891" s="T275">v-v:tense-v:poss.pn</ta>
            <ta e="T277" id="Seg_8892" s="T276">dempro</ta>
            <ta e="T278" id="Seg_8893" s="T277">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T279" id="Seg_8894" s="T278">ptcl</ta>
            <ta e="T281" id="Seg_8895" s="T280">n-n&gt;v-v:cvb</ta>
            <ta e="T282" id="Seg_8896" s="T281">v-v:mood-v:temp.pn</ta>
            <ta e="T283" id="Seg_8897" s="T282">dempro</ta>
            <ta e="T284" id="Seg_8898" s="T283">n-n:(poss).[n:case]</ta>
            <ta e="T285" id="Seg_8899" s="T284">que.[pro:case]</ta>
            <ta e="T286" id="Seg_8900" s="T285">que-pro:(poss)</ta>
            <ta e="T287" id="Seg_8901" s="T286">ptcl</ta>
            <ta e="T288" id="Seg_8902" s="T287">v-v:tense.[v:pred.pn]</ta>
            <ta e="T289" id="Seg_8903" s="T288">ptcl</ta>
            <ta e="T290" id="Seg_8904" s="T289">n-n:(poss)</ta>
            <ta e="T291" id="Seg_8905" s="T290">ptcl</ta>
            <ta e="T292" id="Seg_8906" s="T291">v-v:tense.[v:pred.pn]</ta>
            <ta e="T294" id="Seg_8907" s="T293">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T295" id="Seg_8908" s="T294">adj.[n:case]</ta>
            <ta e="T296" id="Seg_8909" s="T295">v-v:ptcp.[v:(poss)]-v:(case)</ta>
            <ta e="T297" id="Seg_8910" s="T296">dempro.[pro:case]</ta>
            <ta e="T298" id="Seg_8911" s="T297">n-n:(ins)-n:case</ta>
            <ta e="T299" id="Seg_8912" s="T298">que-pro:(ins)-pro:case</ta>
            <ta e="T300" id="Seg_8913" s="T299">adj</ta>
            <ta e="T301" id="Seg_8914" s="T300">adj-n:(ins)-n:case</ta>
            <ta e="T302" id="Seg_8915" s="T301">v-v:tense-v:pred.pn</ta>
            <ta e="T303" id="Seg_8916" s="T302">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T304" id="Seg_8917" s="T303">v-v:tense-v:pred.pn</ta>
            <ta e="T306" id="Seg_8918" s="T305">n-n:(ins)-n:case</ta>
            <ta e="T307" id="Seg_8919" s="T306">conj</ta>
            <ta e="T308" id="Seg_8920" s="T307">adj</ta>
            <ta e="T309" id="Seg_8921" s="T308">adj</ta>
            <ta e="T310" id="Seg_8922" s="T309">adj-adj&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T311" id="Seg_8923" s="T310">v-v:tense-v:pred.pn</ta>
            <ta e="T313" id="Seg_8924" s="T312">que.[pro:case]</ta>
            <ta e="T314" id="Seg_8925" s="T313">adj</ta>
            <ta e="T315" id="Seg_8926" s="T314">ptcl-n:poss-n:case</ta>
            <ta e="T316" id="Seg_8927" s="T315">adj-n:poss-n:case</ta>
            <ta e="T317" id="Seg_8928" s="T316">dempro.[pro:case]</ta>
            <ta e="T318" id="Seg_8929" s="T317">v-v&gt;v-v:cvb</ta>
            <ta e="T319" id="Seg_8930" s="T318">v-v:tense-v:pred.pn</ta>
            <ta e="T321" id="Seg_8931" s="T320">dempro</ta>
            <ta e="T322" id="Seg_8932" s="T321">n-n:(poss).[n:case]</ta>
            <ta e="T323" id="Seg_8933" s="T322">dempro</ta>
            <ta e="T324" id="Seg_8934" s="T323">v-v:cvb</ta>
            <ta e="T325" id="Seg_8935" s="T324">post</ta>
            <ta e="T326" id="Seg_8936" s="T325">v-v:ptcp-v:(case)</ta>
            <ta e="T327" id="Seg_8937" s="T326">adj.[n:case]</ta>
            <ta e="T328" id="Seg_8938" s="T327">ptcl</ta>
            <ta e="T329" id="Seg_8939" s="T328">v-v:tense.[v:pred.pn]</ta>
            <ta e="T331" id="Seg_8940" s="T330">dempro-pro:case</ta>
            <ta e="T332" id="Seg_8941" s="T331">n-n:(poss).[n:case]</ta>
            <ta e="T333" id="Seg_8942" s="T332">adj</ta>
            <ta e="T334" id="Seg_8943" s="T333">n.[n:case]</ta>
            <ta e="T335" id="Seg_8944" s="T334">v-v:ptcp.[v:(poss)]-v:(case)</ta>
            <ta e="T336" id="Seg_8945" s="T335">dempro</ta>
            <ta e="T337" id="Seg_8946" s="T336">v-v:cvb</ta>
            <ta e="T338" id="Seg_8947" s="T337">ptcl</ta>
            <ta e="T339" id="Seg_8948" s="T338">n-n&gt;adj.[n:case]</ta>
            <ta e="T340" id="Seg_8949" s="T339">v-v:ptcp.[v:(poss)]-v:(case)</ta>
            <ta e="T341" id="Seg_8950" s="T340">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T342" id="Seg_8951" s="T341">ptcl</ta>
            <ta e="T343" id="Seg_8952" s="T342">v-v:tense-v:mood-v:pred.pn</ta>
            <ta e="T345" id="Seg_8953" s="T344">v-v:cvb</ta>
            <ta e="T346" id="Seg_8954" s="T345">v-v:mood-v:temp.pn</ta>
            <ta e="T347" id="Seg_8955" s="T346">ptcl</ta>
            <ta e="T348" id="Seg_8956" s="T347">que-que&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T349" id="Seg_8957" s="T348">n.[n:case]</ta>
            <ta e="T350" id="Seg_8958" s="T349">n-n:poss-n:case</ta>
            <ta e="T351" id="Seg_8959" s="T350">v-v:mood-v:pred.pn</ta>
            <ta e="T352" id="Seg_8960" s="T351">que</ta>
            <ta e="T353" id="Seg_8961" s="T352">ptcl</ta>
            <ta e="T355" id="Seg_8962" s="T354">dempro.[pro:case]</ta>
            <ta e="T356" id="Seg_8963" s="T355">que-pro:case</ta>
            <ta e="T357" id="Seg_8964" s="T356">n.[n:case]</ta>
            <ta e="T358" id="Seg_8965" s="T357">v-v&gt;v-v:ptcp</ta>
            <ta e="T359" id="Seg_8966" s="T358">que-pro:(num)-pro:case</ta>
            <ta e="T360" id="Seg_8967" s="T359">v-v:tense-v:pred.pn</ta>
            <ta e="T361" id="Seg_8968" s="T360">ptcl</ta>
            <ta e="T362" id="Seg_8969" s="T361">dempro</ta>
            <ta e="T363" id="Seg_8970" s="T362">n-n:poss-n:case</ta>
            <ta e="T364" id="Seg_8971" s="T363">n.[n:case]</ta>
            <ta e="T365" id="Seg_8972" s="T364">v-v&gt;v-v:ptcp.[v:(case)]</ta>
            <ta e="T367" id="Seg_8973" s="T366">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T368" id="Seg_8974" s="T367">v-v:cvb</ta>
            <ta e="T369" id="Seg_8975" s="T368">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T370" id="Seg_8976" s="T369">ptcl</ta>
            <ta e="T372" id="Seg_8977" s="T371">n-n:case</ta>
            <ta e="T373" id="Seg_8978" s="T372">v-v:cvb</ta>
            <ta e="T374" id="Seg_8979" s="T373">v-v:cvb</ta>
            <ta e="T375" id="Seg_8980" s="T374">v-v:cvb-v:pred.pn</ta>
            <ta e="T376" id="Seg_8981" s="T375">ptcl</ta>
            <ta e="T377" id="Seg_8982" s="T376">v-v:cvb</ta>
            <ta e="T378" id="Seg_8983" s="T377">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T379" id="Seg_8984" s="T378">ptcl</ta>
            <ta e="T380" id="Seg_8985" s="T379">dempro</ta>
            <ta e="T381" id="Seg_8986" s="T380">n-n:case</ta>
            <ta e="T382" id="Seg_8987" s="T381">v-v:cvb</ta>
            <ta e="T383" id="Seg_8988" s="T382">v-v:cvb-v:pred.pn</ta>
            <ta e="T384" id="Seg_8989" s="T383">n.[n:case]</ta>
            <ta e="T385" id="Seg_8990" s="T384">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T387" id="Seg_8991" s="T386">ptcl</ta>
            <ta e="T388" id="Seg_8992" s="T387">dempro</ta>
            <ta e="T389" id="Seg_8993" s="T388">v-v:cvb</ta>
            <ta e="T390" id="Seg_8994" s="T389">v-v:tense-v:pred.pn</ta>
            <ta e="T392" id="Seg_8995" s="T391">dempro</ta>
            <ta e="T393" id="Seg_8996" s="T392">v-v:cvb</ta>
            <ta e="T394" id="Seg_8997" s="T393">v-v:mood-v:temp.pn</ta>
            <ta e="T395" id="Seg_8998" s="T394">pers-pro:case</ta>
            <ta e="T396" id="Seg_8999" s="T395">v-v:tense-v:pred.pn</ta>
            <ta e="T397" id="Seg_9000" s="T396">dempro.[pro:case]</ta>
            <ta e="T398" id="Seg_9001" s="T397">que-pro:case</ta>
            <ta e="T399" id="Seg_9002" s="T398">n-n:case</ta>
            <ta e="T400" id="Seg_9003" s="T399">v-v:ptcp</ta>
            <ta e="T401" id="Seg_9004" s="T400">n.[n:case]</ta>
            <ta e="T402" id="Seg_9005" s="T401">n-n:poss-n:case</ta>
            <ta e="T403" id="Seg_9006" s="T402">dempro</ta>
            <ta e="T404" id="Seg_9007" s="T403">n-n:case</ta>
            <ta e="T405" id="Seg_9008" s="T404">v-v:cvb</ta>
            <ta e="T406" id="Seg_9009" s="T405">v-v:cvb</ta>
            <ta e="T407" id="Seg_9010" s="T406">n.[n:case]</ta>
            <ta e="T408" id="Seg_9011" s="T407">n.[n:case]</ta>
            <ta e="T409" id="Seg_9012" s="T408">n.[n:case]</ta>
            <ta e="T411" id="Seg_9013" s="T410">v-v:tense-v:mood.[v:pred.pn]</ta>
            <ta e="T412" id="Seg_9014" s="T411">v.[v:mood.pn]</ta>
            <ta e="T413" id="Seg_9015" s="T412">v-v:tense-v:pred.pn</ta>
            <ta e="T414" id="Seg_9016" s="T413">ptcl</ta>
            <ta e="T415" id="Seg_9017" s="T414">que</ta>
            <ta e="T416" id="Seg_9018" s="T415">v-v:tense-v:poss.pn=ptcl</ta>
            <ta e="T417" id="Seg_9019" s="T416">ptcl</ta>
            <ta e="T419" id="Seg_9020" s="T418">v-v:ptcp</ta>
            <ta e="T420" id="Seg_9021" s="T419">v-v:tense.[v:poss.pn]</ta>
            <ta e="T421" id="Seg_9022" s="T420">adv</ta>
            <ta e="T422" id="Seg_9023" s="T421">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T423" id="Seg_9024" s="T422">n-n:(poss).[n:case]</ta>
            <ta e="T424" id="Seg_9025" s="T423">adv</ta>
            <ta e="T425" id="Seg_9026" s="T424">ptcl</ta>
            <ta e="T426" id="Seg_9027" s="T425">v-v:tense-v:pred.pn</ta>
            <ta e="T427" id="Seg_9028" s="T426">ptcl</ta>
            <ta e="T429" id="Seg_9029" s="T428">v-v:tense-v:pred.pn</ta>
            <ta e="T430" id="Seg_9030" s="T429">ptcl</ta>
            <ta e="T431" id="Seg_9031" s="T430">n-n:(poss).[n:case]</ta>
            <ta e="T432" id="Seg_9032" s="T431">n-n:poss-n:case</ta>
            <ta e="T434" id="Seg_9033" s="T433">dempro</ta>
            <ta e="T435" id="Seg_9034" s="T434">v-v:cvb</ta>
            <ta e="T436" id="Seg_9035" s="T435">v-v:cvb</ta>
            <ta e="T437" id="Seg_9036" s="T436">v-v:cvb</ta>
            <ta e="T438" id="Seg_9037" s="T437">v-v:tense-v:pred.pn</ta>
            <ta e="T439" id="Seg_9038" s="T438">dempro</ta>
            <ta e="T440" id="Seg_9039" s="T439">que-pro:case</ta>
            <ta e="T441" id="Seg_9040" s="T440">n-n:case</ta>
            <ta e="T442" id="Seg_9041" s="T441">v-v:cvb-v:pred.pn</ta>
            <ta e="T444" id="Seg_9042" s="T443">pers-pro:(ins)-pro:(poss).[pro:case]</ta>
            <ta e="T445" id="Seg_9043" s="T444">dempro</ta>
            <ta e="T447" id="Seg_9044" s="T446">dempro</ta>
            <ta e="T448" id="Seg_9045" s="T447">v-v:ptcp</ta>
            <ta e="T449" id="Seg_9046" s="T448">n-n:poss-n:case</ta>
            <ta e="T450" id="Seg_9047" s="T449">v-v:cvb</ta>
            <ta e="T451" id="Seg_9048" s="T450">v-v:tense-v:pred.pn</ta>
            <ta e="T452" id="Seg_9049" s="T451">dempro</ta>
            <ta e="T453" id="Seg_9050" s="T452">n-n:poss-n:case</ta>
            <ta e="T454" id="Seg_9051" s="T453">dempro</ta>
            <ta e="T455" id="Seg_9052" s="T454">que-que&gt;v-v:ptcp</ta>
            <ta e="T456" id="Seg_9053" s="T455">n-n&gt;v-v:ptcp</ta>
            <ta e="T457" id="Seg_9054" s="T456">n-n:poss-n:case</ta>
            <ta e="T458" id="Seg_9055" s="T457">v-v:cvb</ta>
            <ta e="T459" id="Seg_9056" s="T458">v-v:cvb-v:pred.pn</ta>
            <ta e="T460" id="Seg_9057" s="T459">dempro</ta>
            <ta e="T461" id="Seg_9058" s="T460">v-v:tense-v:pred.pn</ta>
            <ta e="T463" id="Seg_9059" s="T462">dempro</ta>
            <ta e="T464" id="Seg_9060" s="T463">v-v:cvb</ta>
            <ta e="T465" id="Seg_9061" s="T464">post</ta>
            <ta e="T467" id="Seg_9062" s="T466">n-n:(ins)-n:case</ta>
            <ta e="T468" id="Seg_9063" s="T467">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T469" id="Seg_9064" s="T468">n-n:poss-n:case</ta>
            <ta e="T471" id="Seg_9065" s="T470">cardnum</ta>
            <ta e="T472" id="Seg_9066" s="T471">n-n:case</ta>
            <ta e="T473" id="Seg_9067" s="T472">v-v:tense.[v:pred.pn]</ta>
            <ta e="T474" id="Seg_9068" s="T473">dempro</ta>
            <ta e="T475" id="Seg_9069" s="T474">n-n:poss-n:case</ta>
            <ta e="T476" id="Seg_9070" s="T475">cardnum</ta>
            <ta e="T477" id="Seg_9071" s="T476">n-n:case</ta>
            <ta e="T478" id="Seg_9072" s="T477">n-n:poss-n:case</ta>
            <ta e="T479" id="Seg_9073" s="T478">v-v:tense.[v:pred.pn]</ta>
            <ta e="T480" id="Seg_9074" s="T479">ptcl</ta>
            <ta e="T481" id="Seg_9075" s="T480">dempro</ta>
            <ta e="T482" id="Seg_9076" s="T481">ptcl</ta>
            <ta e="T483" id="Seg_9077" s="T482">adv-n:case</ta>
            <ta e="T484" id="Seg_9078" s="T483">adv-n:case</ta>
            <ta e="T485" id="Seg_9079" s="T484">post</ta>
            <ta e="T486" id="Seg_9080" s="T485">adv</ta>
            <ta e="T487" id="Seg_9081" s="T486">n-n&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T488" id="Seg_9082" s="T487">interj</ta>
            <ta e="T489" id="Seg_9083" s="T488">v-v:cvb</ta>
            <ta e="T490" id="Seg_9084" s="T489">v-v:cvb</ta>
            <ta e="T492" id="Seg_9085" s="T491">ptcl</ta>
            <ta e="T493" id="Seg_9086" s="T492">dempro</ta>
            <ta e="T494" id="Seg_9087" s="T493">v-v:cvb</ta>
            <ta e="T495" id="Seg_9088" s="T494">post</ta>
            <ta e="T496" id="Seg_9089" s="T495">pers-pro:(ins)-pro:(poss).[pro:case]</ta>
            <ta e="T497" id="Seg_9090" s="T496">n.[n:case]</ta>
            <ta e="T498" id="Seg_9091" s="T497">n-n:(num).[n:case]</ta>
            <ta e="T499" id="Seg_9092" s="T498">que</ta>
            <ta e="T500" id="Seg_9093" s="T499">ptcl</ta>
            <ta e="T501" id="Seg_9094" s="T500">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T502" id="Seg_9095" s="T501">v-v:ptcp-v:(poss)</ta>
            <ta e="T503" id="Seg_9096" s="T502">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T505" id="Seg_9097" s="T504">v-v:cvb</ta>
            <ta e="T506" id="Seg_9098" s="T505">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T507" id="Seg_9099" s="T506">ptcl</ta>
            <ta e="T508" id="Seg_9100" s="T507">ptcl</ta>
            <ta e="T509" id="Seg_9101" s="T508">adv</ta>
            <ta e="T510" id="Seg_9102" s="T509">ptcl</ta>
            <ta e="T511" id="Seg_9103" s="T510">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T513" id="Seg_9104" s="T512">v-v:tense-v:mood-v:pred.pn</ta>
            <ta e="T514" id="Seg_9105" s="T513">adv</ta>
            <ta e="T516" id="Seg_9106" s="T515">conj</ta>
            <ta e="T517" id="Seg_9107" s="T516">v-v:ptcp-v:(ins)-v:(poss).[v:(case)]</ta>
            <ta e="T518" id="Seg_9108" s="T517">ptcl</ta>
            <ta e="T519" id="Seg_9109" s="T518">ptcl</ta>
            <ta e="T520" id="Seg_9110" s="T519">adj.[n:case]</ta>
            <ta e="T521" id="Seg_9111" s="T520">conj</ta>
            <ta e="T522" id="Seg_9112" s="T521">adj.[n:case]</ta>
            <ta e="T524" id="Seg_9113" s="T523">ptcl</ta>
            <ta e="T525" id="Seg_9114" s="T524">v-v:tense-v:poss.pn</ta>
            <ta e="T526" id="Seg_9115" s="T525">n.[n:case]</ta>
            <ta e="T527" id="Seg_9116" s="T526">ptcl</ta>
            <ta e="T528" id="Seg_9117" s="T527">n-n:(poss).[n:case]</ta>
            <ta e="T529" id="Seg_9118" s="T528">v-v:tense.[v:pred.pn]</ta>
            <ta e="T530" id="Seg_9119" s="T529">adv</ta>
            <ta e="T531" id="Seg_9120" s="T530">v-v:tense.[v:pred.pn]</ta>
            <ta e="T532" id="Seg_9121" s="T531">ptcl</ta>
            <ta e="T534" id="Seg_9122" s="T533">dempro.[pro:case]</ta>
            <ta e="T535" id="Seg_9123" s="T534">post</ta>
            <ta e="T537" id="Seg_9124" s="T536">n.[n:case]</ta>
            <ta e="T538" id="Seg_9125" s="T537">n-n&gt;n-n:(num)-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T539" id="Seg_9126" s="T538">v-v:tense-v:pred.pn</ta>
            <ta e="T540" id="Seg_9127" s="T539">ptcl</ta>
            <ta e="T541" id="Seg_9128" s="T540">v-v:(neg)-v:cvb-v:pred.pn</ta>
            <ta e="T542" id="Seg_9129" s="T541">v-v:cvb</ta>
            <ta e="T543" id="Seg_9130" s="T542">v-v:tense-v:pred.pn</ta>
            <ta e="T545" id="Seg_9131" s="T544">que.[pro:case]</ta>
            <ta e="T546" id="Seg_9132" s="T545">n.[n:case]</ta>
            <ta e="T547" id="Seg_9133" s="T546">n-n:(num)-n:case</ta>
            <ta e="T548" id="Seg_9134" s="T547">quant-quant&gt;adv</ta>
            <ta e="T549" id="Seg_9135" s="T548">v-v:mood-v:pred.pn</ta>
            <ta e="T550" id="Seg_9136" s="T549">n.[n:case]</ta>
            <ta e="T551" id="Seg_9137" s="T550">n-n:case</ta>
            <ta e="T552" id="Seg_9138" s="T551">que</ta>
            <ta e="T553" id="Seg_9139" s="T552">ptcl</ta>
            <ta e="T555" id="Seg_9140" s="T554">ptcl</ta>
            <ta e="T556" id="Seg_9141" s="T555">dempro</ta>
            <ta e="T557" id="Seg_9142" s="T556">dempro</ta>
            <ta e="T558" id="Seg_9143" s="T557">v-v:cvb</ta>
            <ta e="T559" id="Seg_9144" s="T558">post</ta>
            <ta e="T560" id="Seg_9145" s="T559">dempro</ta>
            <ta e="T561" id="Seg_9146" s="T560">que-pro:(ins)-pro:case</ta>
            <ta e="T562" id="Seg_9147" s="T561">adv</ta>
            <ta e="T563" id="Seg_9148" s="T562">que-pro:case</ta>
            <ta e="T564" id="Seg_9149" s="T563">n-n:(ins)-n:case</ta>
            <ta e="T565" id="Seg_9150" s="T564">adv</ta>
            <ta e="T566" id="Seg_9151" s="T565">que-que&gt;v-v:cvb</ta>
            <ta e="T567" id="Seg_9152" s="T566">que-que&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T568" id="Seg_9153" s="T567">ptcl</ta>
            <ta e="T569" id="Seg_9154" s="T568">dempro</ta>
            <ta e="T570" id="Seg_9155" s="T569">n-n:poss-n:case</ta>
            <ta e="T572" id="Seg_9156" s="T571">dempro</ta>
            <ta e="T573" id="Seg_9157" s="T572">n-n&gt;v-v:ptcp-v:(ins)-v:(poss).[v:(case)]</ta>
            <ta e="T574" id="Seg_9158" s="T573">ptcl</ta>
            <ta e="T575" id="Seg_9159" s="T574">adv</ta>
            <ta e="T576" id="Seg_9160" s="T575">ptcl</ta>
            <ta e="T577" id="Seg_9161" s="T576">v-v:tense-v:poss.pn</ta>
            <ta e="T579" id="Seg_9162" s="T578">adj</ta>
            <ta e="T580" id="Seg_9163" s="T579">n.[n:case]</ta>
            <ta e="T581" id="Seg_9164" s="T580">ptcl</ta>
            <ta e="T582" id="Seg_9165" s="T581">v-v:tense-v:mood-v:pred.pn</ta>
            <ta e="T583" id="Seg_9166" s="T582">ptcl</ta>
            <ta e="T584" id="Seg_9167" s="T583">dempro</ta>
            <ta e="T585" id="Seg_9168" s="T584">v-v:cvb</ta>
            <ta e="T586" id="Seg_9169" s="T585">v-v:tense-v:pred.pn</ta>
            <ta e="T588" id="Seg_9170" s="T587">v-v:ptcp</ta>
            <ta e="T589" id="Seg_9171" s="T588">v-v:tense-v:pred.pn</ta>
            <ta e="T590" id="Seg_9172" s="T589">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T591" id="Seg_9173" s="T590">v-v:tense.[v:pred.pn]</ta>
            <ta e="T592" id="Seg_9174" s="T591">ptcl</ta>
            <ta e="T594" id="Seg_9175" s="T593">ptcl</ta>
            <ta e="T595" id="Seg_9176" s="T594">ptcl</ta>
            <ta e="T596" id="Seg_9177" s="T595">ptcl</ta>
            <ta e="T597" id="Seg_9178" s="T596">v-v:tense-v:pred.pn</ta>
            <ta e="T598" id="Seg_9179" s="T597">ptcl</ta>
            <ta e="T599" id="Seg_9180" s="T598">que</ta>
            <ta e="T600" id="Seg_9181" s="T599">v-v:tense-v:poss.pn=ptcl</ta>
            <ta e="T601" id="Seg_9182" s="T600">n-n:(poss).[n:case]</ta>
            <ta e="T602" id="Seg_9183" s="T601">n-n:poss-n:case</ta>
            <ta e="T603" id="Seg_9184" s="T602">v-v:tense-v:pred.pn</ta>
            <ta e="T604" id="Seg_9185" s="T603">ptcl</ta>
            <ta e="T606" id="Seg_9186" s="T605">ptcl</ta>
            <ta e="T607" id="Seg_9187" s="T606">dempro</ta>
            <ta e="T608" id="Seg_9188" s="T607">v-v:cvb</ta>
            <ta e="T609" id="Seg_9189" s="T608">v-v:cvb</ta>
            <ta e="T610" id="Seg_9190" s="T609">v-v:tense-v:pred.pn</ta>
            <ta e="T611" id="Seg_9191" s="T610">ptcl</ta>
            <ta e="T612" id="Seg_9192" s="T611">n-n&gt;v-v:cvb</ta>
            <ta e="T613" id="Seg_9193" s="T612">n-n&gt;v-v:cvb</ta>
            <ta e="T614" id="Seg_9194" s="T613">ptcl</ta>
            <ta e="T615" id="Seg_9195" s="T614">v-v:tense-v:pred.pn</ta>
            <ta e="T616" id="Seg_9196" s="T615">ptcl</ta>
            <ta e="T618" id="Seg_9197" s="T617">que-que&gt;ordnum</ta>
            <ta e="T619" id="Seg_9198" s="T618">ptcl</ta>
            <ta e="T620" id="Seg_9199" s="T619">n-n:poss-n:case</ta>
            <ta e="T621" id="Seg_9200" s="T620">ptcl</ta>
            <ta e="T622" id="Seg_9201" s="T621">dempro</ta>
            <ta e="T623" id="Seg_9202" s="T622">n.[n:case]</ta>
            <ta e="T624" id="Seg_9203" s="T623">que-pro:(ins)-pro:case</ta>
            <ta e="T625" id="Seg_9204" s="T624">n-n:case</ta>
            <ta e="T627" id="Seg_9205" s="T626">dempro-pro:case</ta>
            <ta e="T628" id="Seg_9206" s="T627">adv</ta>
            <ta e="T629" id="Seg_9207" s="T628">dempro.[pro:case]</ta>
            <ta e="T630" id="Seg_9208" s="T629">n.[n:case]</ta>
            <ta e="T631" id="Seg_9209" s="T630">ptcl</ta>
            <ta e="T632" id="Seg_9210" s="T631">adv</ta>
            <ta e="T633" id="Seg_9211" s="T632">n</ta>
            <ta e="T634" id="Seg_9212" s="T633">ptcl</ta>
            <ta e="T635" id="Seg_9213" s="T634">adj.[n:case]</ta>
            <ta e="T636" id="Seg_9214" s="T635">v-v:ptcp.[v:(poss)]-v:(case)</ta>
            <ta e="T637" id="Seg_9215" s="T636">adj.[n:case]</ta>
            <ta e="T638" id="Seg_9216" s="T637">v-v:ptcp.[v:(poss)]-v:(case)</ta>
            <ta e="T639" id="Seg_9217" s="T638">dempro.[pro:case]</ta>
            <ta e="T640" id="Seg_9218" s="T639">adj</ta>
            <ta e="T641" id="Seg_9219" s="T640">adj-adj&gt;adv</ta>
            <ta e="T642" id="Seg_9220" s="T641">dempro.[pro:case]</ta>
            <ta e="T643" id="Seg_9221" s="T642">que-que&gt;v-v:cvb</ta>
            <ta e="T644" id="Seg_9222" s="T643">v-v:tense-v:poss.pn</ta>
            <ta e="T645" id="Seg_9223" s="T644">ptcl</ta>
            <ta e="T647" id="Seg_9224" s="T646">n-n:case</ta>
            <ta e="T648" id="Seg_9225" s="T647">n-n:case</ta>
            <ta e="T649" id="Seg_9226" s="T648">v-v:tense-v:pred.pn</ta>
            <ta e="T651" id="Seg_9227" s="T650">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T652" id="Seg_9228" s="T651">n.[n:case]</ta>
            <ta e="T653" id="Seg_9229" s="T652">adv</ta>
            <ta e="T654" id="Seg_9230" s="T653">adj</ta>
            <ta e="T655" id="Seg_9231" s="T654">v-v:cvb</ta>
            <ta e="T656" id="Seg_9232" s="T655">adv</ta>
            <ta e="T657" id="Seg_9233" s="T656">ptcl</ta>
            <ta e="T658" id="Seg_9234" s="T657">adj.[n:case]</ta>
            <ta e="T660" id="Seg_9235" s="T659">dempro.[pro:case]</ta>
            <ta e="T661" id="Seg_9236" s="T660">post</ta>
            <ta e="T662" id="Seg_9237" s="T661">n-n:poss-n:case</ta>
            <ta e="T663" id="Seg_9238" s="T662">dempro.[pro:case]</ta>
            <ta e="T664" id="Seg_9239" s="T663">que-pro:case</ta>
            <ta e="T665" id="Seg_9240" s="T664">v-v:tense-v:pred.pn</ta>
            <ta e="T666" id="Seg_9241" s="T665">v-v:tense-v:pred.pn</ta>
            <ta e="T667" id="Seg_9242" s="T666">n-n:case</ta>
            <ta e="T669" id="Seg_9243" s="T668">dempro</ta>
            <ta e="T670" id="Seg_9244" s="T669">n-n:poss-n:case</ta>
            <ta e="T671" id="Seg_9245" s="T670">v-v:cvb</ta>
            <ta e="T672" id="Seg_9246" s="T671">v-v:cvb-v:pred.pn</ta>
            <ta e="T673" id="Seg_9247" s="T672">n.[n:case]</ta>
            <ta e="T674" id="Seg_9248" s="T673">n-n:poss-n:case</ta>
            <ta e="T675" id="Seg_9249" s="T674">v-v:tense-v:pred.pn</ta>
            <ta e="T677" id="Seg_9250" s="T676">n.[n:case]</ta>
            <ta e="T678" id="Seg_9251" s="T677">conj</ta>
            <ta e="T679" id="Seg_9252" s="T678">v-v:tense.[v:poss.pn]=ptcl</ta>
            <ta e="T680" id="Seg_9253" s="T679">dempro.[pro:case]</ta>
            <ta e="T681" id="Seg_9254" s="T680">que.[pro:case]</ta>
            <ta e="T682" id="Seg_9255" s="T681">n.[n:case]</ta>
            <ta e="T683" id="Seg_9256" s="T682">n-n:poss-n:case</ta>
            <ta e="T684" id="Seg_9257" s="T683">n.[n:case]</ta>
            <ta e="T685" id="Seg_9258" s="T684">n.[n:case]</ta>
            <ta e="T686" id="Seg_9259" s="T685">n-n:poss-n:case</ta>
            <ta e="T687" id="Seg_9260" s="T686">v-v:tense-v:pred.pn</ta>
            <ta e="T689" id="Seg_9261" s="T688">adv</ta>
            <ta e="T690" id="Seg_9262" s="T689">n-n:case</ta>
            <ta e="T691" id="Seg_9263" s="T690">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T692" id="Seg_9264" s="T691">n.[n:case]</ta>
            <ta e="T693" id="Seg_9265" s="T692">v-v:tense.[v:pred.pn]</ta>
            <ta e="T694" id="Seg_9266" s="T693">ptcl</ta>
            <ta e="T696" id="Seg_9267" s="T695">conj</ta>
            <ta e="T697" id="Seg_9268" s="T696">indfpro</ta>
            <ta e="T698" id="Seg_9269" s="T697">n-n:(num).[n:case]</ta>
            <ta e="T699" id="Seg_9270" s="T698">adv</ta>
            <ta e="T700" id="Seg_9271" s="T699">v-v:mood-v:pred.pn</ta>
            <ta e="T701" id="Seg_9272" s="T700">ptcl</ta>
            <ta e="T703" id="Seg_9273" s="T702">dempro.[pro:case]</ta>
            <ta e="T704" id="Seg_9274" s="T703">post</ta>
            <ta e="T705" id="Seg_9275" s="T704">conj</ta>
            <ta e="T706" id="Seg_9276" s="T705">v-v:cvb</ta>
            <ta e="T707" id="Seg_9277" s="T706">v-v:ptcp</ta>
            <ta e="T708" id="Seg_9278" s="T707">v-v:tense-v:pred.pn</ta>
            <ta e="T709" id="Seg_9279" s="T708">ptcl</ta>
            <ta e="T711" id="Seg_9280" s="T710">conj</ta>
            <ta e="T712" id="Seg_9281" s="T711">pers.[pro:case]</ta>
            <ta e="T713" id="Seg_9282" s="T712">pers-pro:(ins)-pro:(poss).[pro:case]</ta>
            <ta e="T714" id="Seg_9283" s="T713">ptcl</ta>
            <ta e="T715" id="Seg_9284" s="T714">v-v:tense-v:pred.pn</ta>
            <ta e="T716" id="Seg_9285" s="T715">ptcl</ta>
            <ta e="T717" id="Seg_9286" s="T716">pers-pro:case</ta>
            <ta e="T718" id="Seg_9287" s="T717">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T719" id="Seg_9288" s="T718">ptcl</ta>
            <ta e="T720" id="Seg_9289" s="T719">cardnum</ta>
            <ta e="T721" id="Seg_9290" s="T720">cardnum</ta>
            <ta e="T722" id="Seg_9291" s="T721">n.[n:case]</ta>
            <ta e="T723" id="Seg_9292" s="T722">n.[n:case]</ta>
            <ta e="T724" id="Seg_9293" s="T723">n-n:(num).[n:case]</ta>
            <ta e="T868" id="Seg_9294" s="T724">que.[pro:case]</ta>
            <ta e="T869" id="Seg_9295" s="T868">que.[pro:case]</ta>
            <ta e="T870" id="Seg_9296" s="T869">n-n:(poss).[n:case]</ta>
            <ta e="T871" id="Seg_9297" s="T870">que.[pro:case]</ta>
            <ta e="T872" id="Seg_9298" s="T871">n-n:(poss).[n:case]</ta>
            <ta e="T873" id="Seg_9299" s="T872">dempro.[pro:case]</ta>
            <ta e="T874" id="Seg_9300" s="T873">n.[n:case]</ta>
            <ta e="T725" id="Seg_9301" s="T874">v-v:tense-v:pred.pn</ta>
            <ta e="T726" id="Seg_9302" s="T725">n.[n:case]</ta>
            <ta e="T727" id="Seg_9303" s="T726">v-v:tense.[v:pred.pn]</ta>
            <ta e="T728" id="Seg_9304" s="T727">dempro-pro:(poss)-pro:(poss).[pro:case]</ta>
            <ta e="T729" id="Seg_9305" s="T728">n-n:poss-n:case</ta>
            <ta e="T730" id="Seg_9306" s="T729">n-n:(num)-n:case</ta>
            <ta e="T731" id="Seg_9307" s="T730">v-v:tense-v:pred.pn</ta>
            <ta e="T732" id="Seg_9308" s="T731">adj</ta>
            <ta e="T733" id="Seg_9309" s="T732">n.[n:case]</ta>
            <ta e="T734" id="Seg_9310" s="T733">v-v:ptcp.[v:(poss)]-v:(case)</ta>
            <ta e="T736" id="Seg_9311" s="T735">ptcl</ta>
            <ta e="T737" id="Seg_9312" s="T736">dempro</ta>
            <ta e="T738" id="Seg_9313" s="T737">n.[n:case]</ta>
            <ta e="T739" id="Seg_9314" s="T738">n-n:poss-n:case</ta>
            <ta e="T740" id="Seg_9315" s="T739">post</ta>
            <ta e="T741" id="Seg_9316" s="T740">v-v:cvb-v:pred.pn</ta>
            <ta e="T742" id="Seg_9317" s="T741">dempro</ta>
            <ta e="T743" id="Seg_9318" s="T742">v-v:tense-v:pred.pn</ta>
            <ta e="T744" id="Seg_9319" s="T743">ptcl</ta>
            <ta e="T745" id="Seg_9320" s="T744">que-pro:case</ta>
            <ta e="T747" id="Seg_9321" s="T746">dempro</ta>
            <ta e="T748" id="Seg_9322" s="T747">v-v:cvb</ta>
            <ta e="T749" id="Seg_9323" s="T748">post</ta>
            <ta e="T750" id="Seg_9324" s="T749">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T751" id="Seg_9325" s="T750">n.[n:case]</ta>
            <ta e="T752" id="Seg_9326" s="T751">n-n:poss-n:case</ta>
            <ta e="T753" id="Seg_9327" s="T752">post</ta>
            <ta e="T754" id="Seg_9328" s="T753">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T756" id="Seg_9329" s="T755">adj-adj&gt;adv</ta>
            <ta e="T757" id="Seg_9330" s="T756">v-v:tense-v:mood-v:pred.pn</ta>
            <ta e="T758" id="Seg_9331" s="T757">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T759" id="Seg_9332" s="T758">v-v:ptcp.[v:(case)]</ta>
            <ta e="T760" id="Seg_9333" s="T759">post</ta>
            <ta e="T762" id="Seg_9334" s="T761">emphpro</ta>
            <ta e="T763" id="Seg_9335" s="T762">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T764" id="Seg_9336" s="T763">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T765" id="Seg_9337" s="T764">ptcl</ta>
            <ta e="T766" id="Seg_9338" s="T765">ptcl</ta>
            <ta e="T768" id="Seg_9339" s="T767">pers.[pro:case]</ta>
            <ta e="T769" id="Seg_9340" s="T768">n-n:poss-n:case</ta>
            <ta e="T770" id="Seg_9341" s="T769">v-v:tense-v:poss.pn</ta>
            <ta e="T771" id="Seg_9342" s="T770">n-n:(ins)-n:(poss).[n:case]</ta>
            <ta e="T772" id="Seg_9343" s="T771">adj</ta>
            <ta e="T773" id="Seg_9344" s="T772">n-n:poss-n:case</ta>
            <ta e="T774" id="Seg_9345" s="T773">adv-adv&gt;adv</ta>
            <ta e="T775" id="Seg_9346" s="T774">v-v:tense.[v:pred.pn]</ta>
            <ta e="T777" id="Seg_9347" s="T776">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T778" id="Seg_9348" s="T777">dempro</ta>
            <ta e="T779" id="Seg_9349" s="T778">n-n:case</ta>
            <ta e="T781" id="Seg_9350" s="T780">n-n:poss-n:case</ta>
            <ta e="T782" id="Seg_9351" s="T781">adj</ta>
            <ta e="T783" id="Seg_9352" s="T782">n.[n:case]</ta>
            <ta e="T784" id="Seg_9353" s="T783">v-v:tense.[v:pred.pn]</ta>
            <ta e="T785" id="Seg_9354" s="T784">ptcl</ta>
            <ta e="T787" id="Seg_9355" s="T786">pers-pro:case</ta>
            <ta e="T788" id="Seg_9356" s="T787">ptcl</ta>
            <ta e="T789" id="Seg_9357" s="T788">adj</ta>
            <ta e="T790" id="Seg_9358" s="T789">dempro</ta>
            <ta e="T791" id="Seg_9359" s="T790">n.[n:case]</ta>
            <ta e="T792" id="Seg_9360" s="T791">v-v:ptcp-v:(poss).[v:(case)]</ta>
            <ta e="T794" id="Seg_9361" s="T793">n-n:(poss).[n:case]</ta>
            <ta e="T795" id="Seg_9362" s="T794">v-v:tense.[v:pred.pn]</ta>
            <ta e="T796" id="Seg_9363" s="T795">ptcl</ta>
            <ta e="T797" id="Seg_9364" s="T796">dempro</ta>
            <ta e="T798" id="Seg_9365" s="T797">n-n:case</ta>
            <ta e="T800" id="Seg_9366" s="T799">n-n:poss-n:case</ta>
            <ta e="T801" id="Seg_9367" s="T800">conj</ta>
            <ta e="T802" id="Seg_9368" s="T801">v-v:tense-v:pred.pn</ta>
            <ta e="T803" id="Seg_9369" s="T802">ptcl</ta>
            <ta e="T804" id="Seg_9370" s="T803">n-n:case</ta>
            <ta e="T806" id="Seg_9371" s="T805">ptcl</ta>
            <ta e="T807" id="Seg_9372" s="T806">dempro</ta>
            <ta e="T808" id="Seg_9373" s="T807">conj</ta>
            <ta e="T809" id="Seg_9374" s="T808">v-v:mood.[v:pred.pn]</ta>
            <ta e="T810" id="Seg_9375" s="T809">ptcl</ta>
            <ta e="T811" id="Seg_9376" s="T810">v-v:(ins)-v&gt;v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T812" id="Seg_9377" s="T811">ptcl</ta>
            <ta e="T813" id="Seg_9378" s="T812">v-v:(ins)-v:mood.pn</ta>
            <ta e="T814" id="Seg_9379" s="T813">v-v:(ins)-v:mood.pn</ta>
            <ta e="T815" id="Seg_9380" s="T814">v-v:tense-v:pred.pn</ta>
            <ta e="T816" id="Seg_9381" s="T815">ptcl</ta>
            <ta e="T817" id="Seg_9382" s="T816">pers-pro:case</ta>
            <ta e="T819" id="Seg_9383" s="T818">ptcl</ta>
            <ta e="T820" id="Seg_9384" s="T819">dempro.[pro:case]</ta>
            <ta e="T821" id="Seg_9385" s="T820">post</ta>
            <ta e="T822" id="Seg_9386" s="T821">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T824" id="Seg_9387" s="T823">dempro-pro:case</ta>
            <ta e="T825" id="Seg_9388" s="T824">cardnum</ta>
            <ta e="T826" id="Seg_9389" s="T825">ptcl</ta>
            <ta e="T827" id="Seg_9390" s="T826">n.[n:case]</ta>
            <ta e="T828" id="Seg_9391" s="T827">n-n:(poss).[n:case]</ta>
            <ta e="T829" id="Seg_9392" s="T828">n-n:case</ta>
            <ta e="T830" id="Seg_9393" s="T829">n-n:poss-n:case</ta>
            <ta e="T831" id="Seg_9394" s="T830">ptcl-n:(ins)-n:(poss)</ta>
            <ta e="T832" id="Seg_9395" s="T831">v-v:cvb</ta>
            <ta e="T833" id="Seg_9396" s="T832">v-v:tense.[v:poss.pn]</ta>
            <ta e="T834" id="Seg_9397" s="T833">n</ta>
            <ta e="T835" id="Seg_9398" s="T834">v-v:tense.[v:poss.pn]</ta>
            <ta e="T836" id="Seg_9399" s="T835">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T837" id="Seg_9400" s="T836">dempro</ta>
            <ta e="T838" id="Seg_9401" s="T837">v-v:ptcp</ta>
            <ta e="T839" id="Seg_9402" s="T838">n-n:poss-n:case</ta>
            <ta e="T840" id="Seg_9403" s="T839">n-n:poss-n:case</ta>
            <ta e="T841" id="Seg_9404" s="T840">ptcl</ta>
            <ta e="T842" id="Seg_9405" s="T841">adv</ta>
            <ta e="T843" id="Seg_9406" s="T842">v-v:tense-v:pred.pn</ta>
            <ta e="T844" id="Seg_9407" s="T843">ptcl</ta>
            <ta e="T845" id="Seg_9408" s="T844">ptcl</ta>
            <ta e="T846" id="Seg_9409" s="T845">n-n:case</ta>
            <ta e="T847" id="Seg_9410" s="T846">v-v:tense-v:poss.pn</ta>
            <ta e="T848" id="Seg_9411" s="T847">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T849" id="Seg_9412" s="T848">v-v:mood-v:temp.pn</ta>
            <ta e="T851" id="Seg_9413" s="T850">v-v:cvb</ta>
            <ta e="T852" id="Seg_9414" s="T851">v-v:tense-v:pred.pn</ta>
            <ta e="T853" id="Seg_9415" s="T852">adv-adv&gt;adv</ta>
            <ta e="T854" id="Seg_9416" s="T853">v-v:tense-v:pred.pn</ta>
            <ta e="T855" id="Seg_9417" s="T854">ptcl</ta>
            <ta e="T856" id="Seg_9418" s="T855">pers-pro:case</ta>
            <ta e="T858" id="Seg_9419" s="T857">dempro.[pro:case]</ta>
            <ta e="T859" id="Seg_9420" s="T858">post</ta>
            <ta e="T860" id="Seg_9421" s="T859">dempro</ta>
            <ta e="T861" id="Seg_9422" s="T860">n-n&gt;v-v:cvb</ta>
            <ta e="T862" id="Seg_9423" s="T861">v-v:tense-v:pred.pn</ta>
            <ta e="T863" id="Seg_9424" s="T862">ptcl</ta>
            <ta e="T864" id="Seg_9425" s="T863">dempro</ta>
            <ta e="T865" id="Seg_9426" s="T864">n-n:case</ta>
            <ta e="T866" id="Seg_9427" s="T865">v-v:cvb</ta>
            <ta e="T867" id="Seg_9428" s="T866">adv</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T2" id="Seg_9429" s="T1">v</ta>
            <ta e="T4" id="Seg_9430" s="T3">n</ta>
            <ta e="T5" id="Seg_9431" s="T4">n</ta>
            <ta e="T6" id="Seg_9432" s="T5">post</ta>
            <ta e="T7" id="Seg_9433" s="T6">v</ta>
            <ta e="T8" id="Seg_9434" s="T7">v</ta>
            <ta e="T9" id="Seg_9435" s="T8">aux</ta>
            <ta e="T10" id="Seg_9436" s="T9">n</ta>
            <ta e="T11" id="Seg_9437" s="T10">n</ta>
            <ta e="T12" id="Seg_9438" s="T11">v</ta>
            <ta e="T13" id="Seg_9439" s="T12">n</ta>
            <ta e="T14" id="Seg_9440" s="T13">ptcl</ta>
            <ta e="T15" id="Seg_9441" s="T14">cop</ta>
            <ta e="T17" id="Seg_9442" s="T16">adv</ta>
            <ta e="T18" id="Seg_9443" s="T17">quant</ta>
            <ta e="T19" id="Seg_9444" s="T18">ptcl</ta>
            <ta e="T20" id="Seg_9445" s="T19">n</ta>
            <ta e="T21" id="Seg_9446" s="T20">n</ta>
            <ta e="T22" id="Seg_9447" s="T21">n</ta>
            <ta e="T23" id="Seg_9448" s="T22">n</ta>
            <ta e="T24" id="Seg_9449" s="T23">v</ta>
            <ta e="T26" id="Seg_9450" s="T25">adj</ta>
            <ta e="T27" id="Seg_9451" s="T26">adj</ta>
            <ta e="T28" id="Seg_9452" s="T27">n</ta>
            <ta e="T29" id="Seg_9453" s="T28">n</ta>
            <ta e="T30" id="Seg_9454" s="T29">v</ta>
            <ta e="T31" id="Seg_9455" s="T30">aux</ta>
            <ta e="T32" id="Seg_9456" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_9457" s="T32">n</ta>
            <ta e="T34" id="Seg_9458" s="T33">v</ta>
            <ta e="T35" id="Seg_9459" s="T34">v</ta>
            <ta e="T37" id="Seg_9460" s="T36">adv</ta>
            <ta e="T38" id="Seg_9461" s="T37">pers</ta>
            <ta e="T39" id="Seg_9462" s="T38">adv</ta>
            <ta e="T40" id="Seg_9463" s="T39">dempro</ta>
            <ta e="T41" id="Seg_9464" s="T40">n</ta>
            <ta e="T42" id="Seg_9465" s="T41">n</ta>
            <ta e="T43" id="Seg_9466" s="T42">v</ta>
            <ta e="T45" id="Seg_9467" s="T44">n</ta>
            <ta e="T46" id="Seg_9468" s="T45">n</ta>
            <ta e="T47" id="Seg_9469" s="T46">adv</ta>
            <ta e="T48" id="Seg_9470" s="T47">adv</ta>
            <ta e="T49" id="Seg_9471" s="T48">v</ta>
            <ta e="T50" id="Seg_9472" s="T49">adv</ta>
            <ta e="T52" id="Seg_9473" s="T51">v</ta>
            <ta e="T53" id="Seg_9474" s="T52">aux</ta>
            <ta e="T54" id="Seg_9475" s="T53">n</ta>
            <ta e="T55" id="Seg_9476" s="T54">v</ta>
            <ta e="T57" id="Seg_9477" s="T56">n</ta>
            <ta e="T59" id="Seg_9478" s="T58">dempro</ta>
            <ta e="T60" id="Seg_9479" s="T59">n</ta>
            <ta e="T61" id="Seg_9480" s="T60">v</ta>
            <ta e="T62" id="Seg_9481" s="T61">v</ta>
            <ta e="T63" id="Seg_9482" s="T62">dempro</ta>
            <ta e="T64" id="Seg_9483" s="T63">adv</ta>
            <ta e="T65" id="Seg_9484" s="T64">n</ta>
            <ta e="T66" id="Seg_9485" s="T65">ptcl</ta>
            <ta e="T67" id="Seg_9486" s="T66">v</ta>
            <ta e="T69" id="Seg_9487" s="T68">n</ta>
            <ta e="T70" id="Seg_9488" s="T69">n</ta>
            <ta e="T71" id="Seg_9489" s="T70">adv</ta>
            <ta e="T72" id="Seg_9490" s="T71">adj</ta>
            <ta e="T73" id="Seg_9491" s="T72">que</ta>
            <ta e="T74" id="Seg_9492" s="T73">n</ta>
            <ta e="T75" id="Seg_9493" s="T74">v</ta>
            <ta e="T76" id="Seg_9494" s="T75">ptcl</ta>
            <ta e="T78" id="Seg_9495" s="T77">v</ta>
            <ta e="T79" id="Seg_9496" s="T78">dempro</ta>
            <ta e="T80" id="Seg_9497" s="T79">n</ta>
            <ta e="T81" id="Seg_9498" s="T80">v</ta>
            <ta e="T83" id="Seg_9499" s="T82">v</ta>
            <ta e="T84" id="Seg_9500" s="T83">n</ta>
            <ta e="T85" id="Seg_9501" s="T84">que</ta>
            <ta e="T86" id="Seg_9502" s="T85">n</ta>
            <ta e="T87" id="Seg_9503" s="T86">v</ta>
            <ta e="T88" id="Seg_9504" s="T87">que</ta>
            <ta e="T89" id="Seg_9505" s="T88">n</ta>
            <ta e="T90" id="Seg_9506" s="T89">n</ta>
            <ta e="T91" id="Seg_9507" s="T90">dempro</ta>
            <ta e="T92" id="Seg_9508" s="T91">n</ta>
            <ta e="T93" id="Seg_9509" s="T92">v</ta>
            <ta e="T95" id="Seg_9510" s="T94">dempro</ta>
            <ta e="T96" id="Seg_9511" s="T95">ptcl</ta>
            <ta e="T97" id="Seg_9512" s="T96">ptcl</ta>
            <ta e="T98" id="Seg_9513" s="T97">dempro</ta>
            <ta e="T99" id="Seg_9514" s="T98">adv</ta>
            <ta e="T100" id="Seg_9515" s="T99">n</ta>
            <ta e="T101" id="Seg_9516" s="T100">v</ta>
            <ta e="T102" id="Seg_9517" s="T101">ptcl</ta>
            <ta e="T104" id="Seg_9518" s="T103">dempro</ta>
            <ta e="T105" id="Seg_9519" s="T104">dempro</ta>
            <ta e="T106" id="Seg_9520" s="T105">n</ta>
            <ta e="T107" id="Seg_9521" s="T106">v</ta>
            <ta e="T109" id="Seg_9522" s="T108">dempro</ta>
            <ta e="T111" id="Seg_9523" s="T110">indfpro</ta>
            <ta e="T112" id="Seg_9524" s="T111">n</ta>
            <ta e="T113" id="Seg_9525" s="T112">pers</ta>
            <ta e="T114" id="Seg_9526" s="T113">dempro</ta>
            <ta e="T115" id="Seg_9527" s="T114">n</ta>
            <ta e="T116" id="Seg_9528" s="T115">v</ta>
            <ta e="T118" id="Seg_9529" s="T117">n</ta>
            <ta e="T119" id="Seg_9530" s="T118">v</ta>
            <ta e="T120" id="Seg_9531" s="T119">adv</ta>
            <ta e="T121" id="Seg_9532" s="T120">ptcl</ta>
            <ta e="T122" id="Seg_9533" s="T121">ptcl</ta>
            <ta e="T123" id="Seg_9534" s="T122">quant</ta>
            <ta e="T124" id="Seg_9535" s="T123">n</ta>
            <ta e="T126" id="Seg_9536" s="T125">v</ta>
            <ta e="T127" id="Seg_9537" s="T126">ptcl</ta>
            <ta e="T128" id="Seg_9538" s="T127">dempro</ta>
            <ta e="T129" id="Seg_9539" s="T128">v</ta>
            <ta e="T130" id="Seg_9540" s="T129">post</ta>
            <ta e="T131" id="Seg_9541" s="T130">n</ta>
            <ta e="T132" id="Seg_9542" s="T131">ptcl</ta>
            <ta e="T133" id="Seg_9543" s="T132">ptcl</ta>
            <ta e="T134" id="Seg_9544" s="T133">n</ta>
            <ta e="T135" id="Seg_9545" s="T134">quant</ta>
            <ta e="T136" id="Seg_9546" s="T135">cop</ta>
            <ta e="T137" id="Seg_9547" s="T136">ptcl</ta>
            <ta e="T138" id="Seg_9548" s="T137">n</ta>
            <ta e="T139" id="Seg_9549" s="T138">v</ta>
            <ta e="T140" id="Seg_9550" s="T139">ptcl</ta>
            <ta e="T141" id="Seg_9551" s="T140">n</ta>
            <ta e="T143" id="Seg_9552" s="T142">dempro</ta>
            <ta e="T144" id="Seg_9553" s="T143">n</ta>
            <ta e="T145" id="Seg_9554" s="T144">n</ta>
            <ta e="T146" id="Seg_9555" s="T145">adv</ta>
            <ta e="T147" id="Seg_9556" s="T146">adv</ta>
            <ta e="T148" id="Seg_9557" s="T147">v</ta>
            <ta e="T149" id="Seg_9558" s="T148">dempro</ta>
            <ta e="T150" id="Seg_9559" s="T149">adv</ta>
            <ta e="T151" id="Seg_9560" s="T150">v</ta>
            <ta e="T152" id="Seg_9561" s="T151">ptcl</ta>
            <ta e="T153" id="Seg_9562" s="T152">adj</ta>
            <ta e="T154" id="Seg_9563" s="T153">ptcl</ta>
            <ta e="T155" id="Seg_9564" s="T154">n</ta>
            <ta e="T156" id="Seg_9565" s="T155">cop</ta>
            <ta e="T157" id="Seg_9566" s="T156">dempro</ta>
            <ta e="T158" id="Seg_9567" s="T157">adj</ta>
            <ta e="T159" id="Seg_9568" s="T158">que</ta>
            <ta e="T160" id="Seg_9569" s="T159">n</ta>
            <ta e="T161" id="Seg_9570" s="T160">pers</ta>
            <ta e="T162" id="Seg_9571" s="T161">v</ta>
            <ta e="T163" id="Seg_9572" s="T162">n</ta>
            <ta e="T165" id="Seg_9573" s="T164">n</ta>
            <ta e="T166" id="Seg_9574" s="T165">n</ta>
            <ta e="T167" id="Seg_9575" s="T166">n</ta>
            <ta e="T168" id="Seg_9576" s="T167">v</ta>
            <ta e="T169" id="Seg_9577" s="T168">v</ta>
            <ta e="T171" id="Seg_9578" s="T170">dempro</ta>
            <ta e="T172" id="Seg_9579" s="T171">adj</ta>
            <ta e="T173" id="Seg_9580" s="T172">n</ta>
            <ta e="T175" id="Seg_9581" s="T174">dempro</ta>
            <ta e="T176" id="Seg_9582" s="T175">v</ta>
            <ta e="T177" id="Seg_9583" s="T176">v</ta>
            <ta e="T178" id="Seg_9584" s="T177">que</ta>
            <ta e="T179" id="Seg_9585" s="T178">ptcl</ta>
            <ta e="T180" id="Seg_9586" s="T179">n</ta>
            <ta e="T181" id="Seg_9587" s="T180">v</ta>
            <ta e="T183" id="Seg_9588" s="T182">adj</ta>
            <ta e="T184" id="Seg_9589" s="T183">conj</ta>
            <ta e="T185" id="Seg_9590" s="T184">n</ta>
            <ta e="T186" id="Seg_9591" s="T185">cop</ta>
            <ta e="T187" id="Seg_9592" s="T186">dempro</ta>
            <ta e="T188" id="Seg_9593" s="T187">n</ta>
            <ta e="T189" id="Seg_9594" s="T188">v</ta>
            <ta e="T190" id="Seg_9595" s="T189">ptcl</ta>
            <ta e="T191" id="Seg_9596" s="T190">n</ta>
            <ta e="T192" id="Seg_9597" s="T191">ptcl</ta>
            <ta e="T193" id="Seg_9598" s="T192">v</ta>
            <ta e="T194" id="Seg_9599" s="T193">ptcl</ta>
            <ta e="T195" id="Seg_9600" s="T194">adj</ta>
            <ta e="T196" id="Seg_9601" s="T195">cop</ta>
            <ta e="T197" id="Seg_9602" s="T196">ptcl</ta>
            <ta e="T199" id="Seg_9603" s="T198">n</ta>
            <ta e="T200" id="Seg_9604" s="T199">que</ta>
            <ta e="T201" id="Seg_9605" s="T200">ptcl</ta>
            <ta e="T202" id="Seg_9606" s="T201">n</ta>
            <ta e="T203" id="Seg_9607" s="T202">v</ta>
            <ta e="T204" id="Seg_9608" s="T203">aux</ta>
            <ta e="T205" id="Seg_9609" s="T204">pers</ta>
            <ta e="T206" id="Seg_9610" s="T205">dempro</ta>
            <ta e="T207" id="Seg_9611" s="T206">v</ta>
            <ta e="T209" id="Seg_9612" s="T208">dempro</ta>
            <ta e="T210" id="Seg_9613" s="T209">n</ta>
            <ta e="T211" id="Seg_9614" s="T210">v</ta>
            <ta e="T212" id="Seg_9615" s="T211">aux</ta>
            <ta e="T213" id="Seg_9616" s="T212">dempro</ta>
            <ta e="T214" id="Seg_9617" s="T213">que</ta>
            <ta e="T215" id="Seg_9618" s="T214">v</ta>
            <ta e="T216" id="Seg_9619" s="T215">n</ta>
            <ta e="T217" id="Seg_9620" s="T216">v</ta>
            <ta e="T219" id="Seg_9621" s="T218">adv</ta>
            <ta e="T220" id="Seg_9622" s="T219">v</ta>
            <ta e="T221" id="Seg_9623" s="T220">dempro</ta>
            <ta e="T222" id="Seg_9624" s="T221">v</ta>
            <ta e="T223" id="Seg_9625" s="T222">post</ta>
            <ta e="T224" id="Seg_9626" s="T223">v</ta>
            <ta e="T226" id="Seg_9627" s="T225">v</ta>
            <ta e="T227" id="Seg_9628" s="T226">aux</ta>
            <ta e="T228" id="Seg_9629" s="T227">dempro</ta>
            <ta e="T229" id="Seg_9630" s="T228">n</ta>
            <ta e="T230" id="Seg_9631" s="T229">v</ta>
            <ta e="T231" id="Seg_9632" s="T230">n</ta>
            <ta e="T232" id="Seg_9633" s="T231">dempro</ta>
            <ta e="T233" id="Seg_9634" s="T232">que</ta>
            <ta e="T234" id="Seg_9635" s="T233">v</ta>
            <ta e="T235" id="Seg_9636" s="T234">n</ta>
            <ta e="T236" id="Seg_9637" s="T235">v</ta>
            <ta e="T238" id="Seg_9638" s="T237">v</ta>
            <ta e="T239" id="Seg_9639" s="T238">adv</ta>
            <ta e="T240" id="Seg_9640" s="T239">v</ta>
            <ta e="T242" id="Seg_9641" s="T241">dempro</ta>
            <ta e="T243" id="Seg_9642" s="T242">ptcl</ta>
            <ta e="T244" id="Seg_9643" s="T243">adj</ta>
            <ta e="T245" id="Seg_9644" s="T244">ptcl</ta>
            <ta e="T246" id="Seg_9645" s="T245">cop</ta>
            <ta e="T247" id="Seg_9646" s="T246">adj</ta>
            <ta e="T248" id="Seg_9647" s="T247">ptcl</ta>
            <ta e="T249" id="Seg_9648" s="T248">cop</ta>
            <ta e="T250" id="Seg_9649" s="T249">n</ta>
            <ta e="T251" id="Seg_9650" s="T250">adj</ta>
            <ta e="T252" id="Seg_9651" s="T251">que</ta>
            <ta e="T253" id="Seg_9652" s="T252">adj</ta>
            <ta e="T254" id="Seg_9653" s="T253">n</ta>
            <ta e="T255" id="Seg_9654" s="T254">dempro</ta>
            <ta e="T256" id="Seg_9655" s="T255">n</ta>
            <ta e="T257" id="Seg_9656" s="T256">ptcl</ta>
            <ta e="T259" id="Seg_9657" s="T258">dempro</ta>
            <ta e="T260" id="Seg_9658" s="T259">post</ta>
            <ta e="T261" id="Seg_9659" s="T260">adv</ta>
            <ta e="T262" id="Seg_9660" s="T261">v</ta>
            <ta e="T263" id="Seg_9661" s="T262">dempro</ta>
            <ta e="T264" id="Seg_9662" s="T263">adj</ta>
            <ta e="T267" id="Seg_9663" s="T266">v</ta>
            <ta e="T268" id="Seg_9664" s="T267">ptcl</ta>
            <ta e="T269" id="Seg_9665" s="T268">adj</ta>
            <ta e="T270" id="Seg_9666" s="T269">n</ta>
            <ta e="T272" id="Seg_9667" s="T271">dempro</ta>
            <ta e="T273" id="Seg_9668" s="T272">adj</ta>
            <ta e="T274" id="Seg_9669" s="T273">n</ta>
            <ta e="T275" id="Seg_9670" s="T274">ptcl</ta>
            <ta e="T276" id="Seg_9671" s="T275">v</ta>
            <ta e="T277" id="Seg_9672" s="T276">dempro</ta>
            <ta e="T278" id="Seg_9673" s="T277">v</ta>
            <ta e="T279" id="Seg_9674" s="T278">ptcl</ta>
            <ta e="T281" id="Seg_9675" s="T280">v</ta>
            <ta e="T282" id="Seg_9676" s="T281">aux</ta>
            <ta e="T283" id="Seg_9677" s="T282">dempro</ta>
            <ta e="T284" id="Seg_9678" s="T283">n</ta>
            <ta e="T285" id="Seg_9679" s="T284">que</ta>
            <ta e="T286" id="Seg_9680" s="T285">que</ta>
            <ta e="T287" id="Seg_9681" s="T286">ptcl</ta>
            <ta e="T288" id="Seg_9682" s="T287">cop</ta>
            <ta e="T289" id="Seg_9683" s="T288">ptcl</ta>
            <ta e="T290" id="Seg_9684" s="T289">n</ta>
            <ta e="T291" id="Seg_9685" s="T290">ptcl</ta>
            <ta e="T292" id="Seg_9686" s="T291">cop</ta>
            <ta e="T294" id="Seg_9687" s="T293">dempro</ta>
            <ta e="T295" id="Seg_9688" s="T294">adj</ta>
            <ta e="T296" id="Seg_9689" s="T295">v</ta>
            <ta e="T297" id="Seg_9690" s="T296">dempro</ta>
            <ta e="T298" id="Seg_9691" s="T297">n</ta>
            <ta e="T299" id="Seg_9692" s="T298">que</ta>
            <ta e="T300" id="Seg_9693" s="T299">adj</ta>
            <ta e="T301" id="Seg_9694" s="T300">adj</ta>
            <ta e="T302" id="Seg_9695" s="T301">v</ta>
            <ta e="T303" id="Seg_9696" s="T302">v</ta>
            <ta e="T304" id="Seg_9697" s="T303">v</ta>
            <ta e="T306" id="Seg_9698" s="T305">n</ta>
            <ta e="T307" id="Seg_9699" s="T306">conj</ta>
            <ta e="T308" id="Seg_9700" s="T307">adj</ta>
            <ta e="T309" id="Seg_9701" s="T308">adj</ta>
            <ta e="T310" id="Seg_9702" s="T309">v</ta>
            <ta e="T311" id="Seg_9703" s="T310">v</ta>
            <ta e="T313" id="Seg_9704" s="T312">que</ta>
            <ta e="T314" id="Seg_9705" s="T313">adj</ta>
            <ta e="T315" id="Seg_9706" s="T314">ptcl</ta>
            <ta e="T316" id="Seg_9707" s="T315">adj</ta>
            <ta e="T317" id="Seg_9708" s="T316">dempro</ta>
            <ta e="T318" id="Seg_9709" s="T317">v</ta>
            <ta e="T319" id="Seg_9710" s="T318">aux</ta>
            <ta e="T321" id="Seg_9711" s="T320">dempro</ta>
            <ta e="T322" id="Seg_9712" s="T321">n</ta>
            <ta e="T323" id="Seg_9713" s="T322">dempro</ta>
            <ta e="T324" id="Seg_9714" s="T323">v</ta>
            <ta e="T325" id="Seg_9715" s="T324">post</ta>
            <ta e="T326" id="Seg_9716" s="T325">v</ta>
            <ta e="T327" id="Seg_9717" s="T326">adj</ta>
            <ta e="T328" id="Seg_9718" s="T327">ptcl</ta>
            <ta e="T329" id="Seg_9719" s="T328">cop</ta>
            <ta e="T331" id="Seg_9720" s="T330">dempro</ta>
            <ta e="T332" id="Seg_9721" s="T331">n</ta>
            <ta e="T333" id="Seg_9722" s="T332">adv</ta>
            <ta e="T334" id="Seg_9723" s="T333">n</ta>
            <ta e="T335" id="Seg_9724" s="T334">cop</ta>
            <ta e="T336" id="Seg_9725" s="T335">dempro</ta>
            <ta e="T337" id="Seg_9726" s="T336">v</ta>
            <ta e="T338" id="Seg_9727" s="T337">ptcl</ta>
            <ta e="T339" id="Seg_9728" s="T338">adj</ta>
            <ta e="T340" id="Seg_9729" s="T339">cop</ta>
            <ta e="T341" id="Seg_9730" s="T340">dempro</ta>
            <ta e="T342" id="Seg_9731" s="T341">ptcl</ta>
            <ta e="T343" id="Seg_9732" s="T342">v</ta>
            <ta e="T345" id="Seg_9733" s="T344">v</ta>
            <ta e="T346" id="Seg_9734" s="T345">v</ta>
            <ta e="T347" id="Seg_9735" s="T346">ptcl</ta>
            <ta e="T348" id="Seg_9736" s="T347">v</ta>
            <ta e="T349" id="Seg_9737" s="T348">n</ta>
            <ta e="T350" id="Seg_9738" s="T349">n</ta>
            <ta e="T351" id="Seg_9739" s="T350">v</ta>
            <ta e="T352" id="Seg_9740" s="T351">que</ta>
            <ta e="T353" id="Seg_9741" s="T352">ptcl</ta>
            <ta e="T355" id="Seg_9742" s="T354">dempro</ta>
            <ta e="T356" id="Seg_9743" s="T355">que</ta>
            <ta e="T357" id="Seg_9744" s="T356">n</ta>
            <ta e="T358" id="Seg_9745" s="T357">v</ta>
            <ta e="T359" id="Seg_9746" s="T358">que</ta>
            <ta e="T360" id="Seg_9747" s="T359">v</ta>
            <ta e="T361" id="Seg_9748" s="T360">ptcl</ta>
            <ta e="T362" id="Seg_9749" s="T361">dempro</ta>
            <ta e="T363" id="Seg_9750" s="T362">n</ta>
            <ta e="T364" id="Seg_9751" s="T363">n</ta>
            <ta e="T365" id="Seg_9752" s="T364">v</ta>
            <ta e="T367" id="Seg_9753" s="T366">dempro</ta>
            <ta e="T368" id="Seg_9754" s="T367">v</ta>
            <ta e="T369" id="Seg_9755" s="T368">v</ta>
            <ta e="T370" id="Seg_9756" s="T369">ptcl</ta>
            <ta e="T372" id="Seg_9757" s="T371">n</ta>
            <ta e="T373" id="Seg_9758" s="T372">v</ta>
            <ta e="T374" id="Seg_9759" s="T373">v</ta>
            <ta e="T375" id="Seg_9760" s="T374">aux</ta>
            <ta e="T376" id="Seg_9761" s="T375">ptcl</ta>
            <ta e="T377" id="Seg_9762" s="T376">v</ta>
            <ta e="T378" id="Seg_9763" s="T377">aux</ta>
            <ta e="T379" id="Seg_9764" s="T378">ptcl</ta>
            <ta e="T380" id="Seg_9765" s="T379">dempro</ta>
            <ta e="T381" id="Seg_9766" s="T380">n</ta>
            <ta e="T382" id="Seg_9767" s="T381">v</ta>
            <ta e="T383" id="Seg_9768" s="T382">aux</ta>
            <ta e="T384" id="Seg_9769" s="T383">n</ta>
            <ta e="T385" id="Seg_9770" s="T384">cop</ta>
            <ta e="T387" id="Seg_9771" s="T386">ptcl</ta>
            <ta e="T388" id="Seg_9772" s="T387">dempro</ta>
            <ta e="T389" id="Seg_9773" s="T388">v</ta>
            <ta e="T390" id="Seg_9774" s="T389">aux</ta>
            <ta e="T392" id="Seg_9775" s="T391">dempro</ta>
            <ta e="T393" id="Seg_9776" s="T392">v</ta>
            <ta e="T394" id="Seg_9777" s="T393">aux</ta>
            <ta e="T395" id="Seg_9778" s="T394">pers</ta>
            <ta e="T396" id="Seg_9779" s="T395">v</ta>
            <ta e="T397" id="Seg_9780" s="T396">dempro</ta>
            <ta e="T398" id="Seg_9781" s="T397">que</ta>
            <ta e="T399" id="Seg_9782" s="T398">n</ta>
            <ta e="T400" id="Seg_9783" s="T399">v</ta>
            <ta e="T401" id="Seg_9784" s="T400">n</ta>
            <ta e="T402" id="Seg_9785" s="T401">n</ta>
            <ta e="T403" id="Seg_9786" s="T402">dempro</ta>
            <ta e="T404" id="Seg_9787" s="T403">n</ta>
            <ta e="T405" id="Seg_9788" s="T404">v</ta>
            <ta e="T406" id="Seg_9789" s="T405">aux</ta>
            <ta e="T407" id="Seg_9790" s="T406">n</ta>
            <ta e="T408" id="Seg_9791" s="T407">n</ta>
            <ta e="T409" id="Seg_9792" s="T408">n</ta>
            <ta e="T411" id="Seg_9793" s="T410">v</ta>
            <ta e="T412" id="Seg_9794" s="T411">v</ta>
            <ta e="T413" id="Seg_9795" s="T412">v</ta>
            <ta e="T414" id="Seg_9796" s="T413">ptcl</ta>
            <ta e="T415" id="Seg_9797" s="T414">que</ta>
            <ta e="T416" id="Seg_9798" s="T415">cop</ta>
            <ta e="T417" id="Seg_9799" s="T416">ptcl</ta>
            <ta e="T419" id="Seg_9800" s="T418">v</ta>
            <ta e="T420" id="Seg_9801" s="T419">aux</ta>
            <ta e="T421" id="Seg_9802" s="T420">adv</ta>
            <ta e="T422" id="Seg_9803" s="T421">dempro</ta>
            <ta e="T423" id="Seg_9804" s="T422">n</ta>
            <ta e="T424" id="Seg_9805" s="T423">adv</ta>
            <ta e="T425" id="Seg_9806" s="T424">ptcl</ta>
            <ta e="T426" id="Seg_9807" s="T425">v</ta>
            <ta e="T427" id="Seg_9808" s="T426">ptcl</ta>
            <ta e="T429" id="Seg_9809" s="T428">v</ta>
            <ta e="T430" id="Seg_9810" s="T429">ptcl</ta>
            <ta e="T431" id="Seg_9811" s="T430">n</ta>
            <ta e="T432" id="Seg_9812" s="T431">n</ta>
            <ta e="T434" id="Seg_9813" s="T433">dempro</ta>
            <ta e="T435" id="Seg_9814" s="T434">v</ta>
            <ta e="T436" id="Seg_9815" s="T435">v</ta>
            <ta e="T437" id="Seg_9816" s="T436">v</ta>
            <ta e="T438" id="Seg_9817" s="T437">v</ta>
            <ta e="T439" id="Seg_9818" s="T438">dempro</ta>
            <ta e="T440" id="Seg_9819" s="T439">que</ta>
            <ta e="T441" id="Seg_9820" s="T440">n</ta>
            <ta e="T442" id="Seg_9821" s="T441">v</ta>
            <ta e="T444" id="Seg_9822" s="T443">pers</ta>
            <ta e="T445" id="Seg_9823" s="T444">dempro</ta>
            <ta e="T447" id="Seg_9824" s="T446">dempro</ta>
            <ta e="T448" id="Seg_9825" s="T447">v</ta>
            <ta e="T449" id="Seg_9826" s="T448">n</ta>
            <ta e="T450" id="Seg_9827" s="T449">v</ta>
            <ta e="T451" id="Seg_9828" s="T450">aux</ta>
            <ta e="T452" id="Seg_9829" s="T451">dempro</ta>
            <ta e="T453" id="Seg_9830" s="T452">n</ta>
            <ta e="T454" id="Seg_9831" s="T453">dempro</ta>
            <ta e="T455" id="Seg_9832" s="T454">v</ta>
            <ta e="T456" id="Seg_9833" s="T455">v</ta>
            <ta e="T457" id="Seg_9834" s="T456">n</ta>
            <ta e="T458" id="Seg_9835" s="T457">v</ta>
            <ta e="T459" id="Seg_9836" s="T458">aux</ta>
            <ta e="T460" id="Seg_9837" s="T459">dempro</ta>
            <ta e="T461" id="Seg_9838" s="T460">v</ta>
            <ta e="T463" id="Seg_9839" s="T462">dempro</ta>
            <ta e="T464" id="Seg_9840" s="T463">v</ta>
            <ta e="T465" id="Seg_9841" s="T464">post</ta>
            <ta e="T467" id="Seg_9842" s="T466">n</ta>
            <ta e="T468" id="Seg_9843" s="T467">v</ta>
            <ta e="T469" id="Seg_9844" s="T468">n</ta>
            <ta e="T471" id="Seg_9845" s="T470">cardnum</ta>
            <ta e="T472" id="Seg_9846" s="T471">n</ta>
            <ta e="T473" id="Seg_9847" s="T472">v</ta>
            <ta e="T474" id="Seg_9848" s="T473">dempro</ta>
            <ta e="T475" id="Seg_9849" s="T474">n</ta>
            <ta e="T476" id="Seg_9850" s="T475">cardnum</ta>
            <ta e="T477" id="Seg_9851" s="T476">n</ta>
            <ta e="T478" id="Seg_9852" s="T477">n</ta>
            <ta e="T479" id="Seg_9853" s="T478">v</ta>
            <ta e="T480" id="Seg_9854" s="T479">ptcl</ta>
            <ta e="T481" id="Seg_9855" s="T480">dempro</ta>
            <ta e="T482" id="Seg_9856" s="T481">ptcl</ta>
            <ta e="T483" id="Seg_9857" s="T482">adv</ta>
            <ta e="T484" id="Seg_9858" s="T483">adv</ta>
            <ta e="T485" id="Seg_9859" s="T484">post</ta>
            <ta e="T486" id="Seg_9860" s="T485">adv</ta>
            <ta e="T487" id="Seg_9861" s="T486">v</ta>
            <ta e="T488" id="Seg_9862" s="T487">interj</ta>
            <ta e="T489" id="Seg_9863" s="T488">v</ta>
            <ta e="T490" id="Seg_9864" s="T489">v</ta>
            <ta e="T492" id="Seg_9865" s="T491">ptcl</ta>
            <ta e="T493" id="Seg_9866" s="T492">dempro</ta>
            <ta e="T494" id="Seg_9867" s="T493">v</ta>
            <ta e="T495" id="Seg_9868" s="T494">post</ta>
            <ta e="T496" id="Seg_9869" s="T495">pers</ta>
            <ta e="T497" id="Seg_9870" s="T496">n</ta>
            <ta e="T498" id="Seg_9871" s="T497">n</ta>
            <ta e="T499" id="Seg_9872" s="T498">que</ta>
            <ta e="T500" id="Seg_9873" s="T499">ptcl</ta>
            <ta e="T501" id="Seg_9874" s="T500">v</ta>
            <ta e="T502" id="Seg_9875" s="T501">v</ta>
            <ta e="T503" id="Seg_9876" s="T502">ptcl</ta>
            <ta e="T505" id="Seg_9877" s="T504">v</ta>
            <ta e="T506" id="Seg_9878" s="T505">v</ta>
            <ta e="T507" id="Seg_9879" s="T506">ptcl</ta>
            <ta e="T508" id="Seg_9880" s="T507">ptcl</ta>
            <ta e="T509" id="Seg_9881" s="T508">adv</ta>
            <ta e="T510" id="Seg_9882" s="T509">ptcl</ta>
            <ta e="T511" id="Seg_9883" s="T510">v</ta>
            <ta e="T513" id="Seg_9884" s="T512">v</ta>
            <ta e="T514" id="Seg_9885" s="T513">adv</ta>
            <ta e="T516" id="Seg_9886" s="T515">conj</ta>
            <ta e="T517" id="Seg_9887" s="T516">v</ta>
            <ta e="T518" id="Seg_9888" s="T517">ptcl</ta>
            <ta e="T519" id="Seg_9889" s="T518">ptcl</ta>
            <ta e="T520" id="Seg_9890" s="T519">adj</ta>
            <ta e="T521" id="Seg_9891" s="T520">conj</ta>
            <ta e="T522" id="Seg_9892" s="T521">adj</ta>
            <ta e="T524" id="Seg_9893" s="T523">ptcl</ta>
            <ta e="T525" id="Seg_9894" s="T524">v</ta>
            <ta e="T526" id="Seg_9895" s="T525">n</ta>
            <ta e="T527" id="Seg_9896" s="T526">ptcl</ta>
            <ta e="T528" id="Seg_9897" s="T527">n</ta>
            <ta e="T529" id="Seg_9898" s="T528">v</ta>
            <ta e="T530" id="Seg_9899" s="T529">adv</ta>
            <ta e="T531" id="Seg_9900" s="T530">cop</ta>
            <ta e="T532" id="Seg_9901" s="T531">ptcl</ta>
            <ta e="T534" id="Seg_9902" s="T533">dempro</ta>
            <ta e="T535" id="Seg_9903" s="T534">post</ta>
            <ta e="T537" id="Seg_9904" s="T536">n</ta>
            <ta e="T538" id="Seg_9905" s="T537">n</ta>
            <ta e="T539" id="Seg_9906" s="T538">v</ta>
            <ta e="T540" id="Seg_9907" s="T539">ptcl</ta>
            <ta e="T541" id="Seg_9908" s="T540">v</ta>
            <ta e="T542" id="Seg_9909" s="T541">v</ta>
            <ta e="T543" id="Seg_9910" s="T542">aux</ta>
            <ta e="T545" id="Seg_9911" s="T544">que</ta>
            <ta e="T546" id="Seg_9912" s="T545">n</ta>
            <ta e="T547" id="Seg_9913" s="T546">n</ta>
            <ta e="T548" id="Seg_9914" s="T547">adv</ta>
            <ta e="T549" id="Seg_9915" s="T548">v</ta>
            <ta e="T550" id="Seg_9916" s="T549">n</ta>
            <ta e="T551" id="Seg_9917" s="T550">n</ta>
            <ta e="T552" id="Seg_9918" s="T551">que</ta>
            <ta e="T553" id="Seg_9919" s="T552">ptcl</ta>
            <ta e="T555" id="Seg_9920" s="T554">ptcl</ta>
            <ta e="T556" id="Seg_9921" s="T555">dempro</ta>
            <ta e="T557" id="Seg_9922" s="T556">dempro</ta>
            <ta e="T558" id="Seg_9923" s="T557">v</ta>
            <ta e="T559" id="Seg_9924" s="T558">post</ta>
            <ta e="T560" id="Seg_9925" s="T559">dempro</ta>
            <ta e="T561" id="Seg_9926" s="T560">que</ta>
            <ta e="T562" id="Seg_9927" s="T561">adv</ta>
            <ta e="T563" id="Seg_9928" s="T562">que</ta>
            <ta e="T564" id="Seg_9929" s="T563">n</ta>
            <ta e="T565" id="Seg_9930" s="T564">adv</ta>
            <ta e="T566" id="Seg_9931" s="T565">v</ta>
            <ta e="T567" id="Seg_9932" s="T566">v</ta>
            <ta e="T568" id="Seg_9933" s="T567">ptcl</ta>
            <ta e="T569" id="Seg_9934" s="T568">dempro</ta>
            <ta e="T570" id="Seg_9935" s="T569">n</ta>
            <ta e="T572" id="Seg_9936" s="T571">dempro</ta>
            <ta e="T573" id="Seg_9937" s="T572">v</ta>
            <ta e="T574" id="Seg_9938" s="T573">ptcl</ta>
            <ta e="T575" id="Seg_9939" s="T574">adv</ta>
            <ta e="T576" id="Seg_9940" s="T575">ptcl</ta>
            <ta e="T577" id="Seg_9941" s="T576">cop</ta>
            <ta e="T579" id="Seg_9942" s="T578">adj</ta>
            <ta e="T580" id="Seg_9943" s="T579">n</ta>
            <ta e="T581" id="Seg_9944" s="T580">ptcl</ta>
            <ta e="T582" id="Seg_9945" s="T581">v</ta>
            <ta e="T583" id="Seg_9946" s="T582">ptcl</ta>
            <ta e="T584" id="Seg_9947" s="T583">dempro</ta>
            <ta e="T585" id="Seg_9948" s="T584">v</ta>
            <ta e="T586" id="Seg_9949" s="T585">v</ta>
            <ta e="T588" id="Seg_9950" s="T587">v</ta>
            <ta e="T589" id="Seg_9951" s="T588">aux</ta>
            <ta e="T590" id="Seg_9952" s="T589">n</ta>
            <ta e="T591" id="Seg_9953" s="T590">v</ta>
            <ta e="T592" id="Seg_9954" s="T591">ptcl</ta>
            <ta e="T594" id="Seg_9955" s="T593">ptcl</ta>
            <ta e="T595" id="Seg_9956" s="T594">ptcl</ta>
            <ta e="T596" id="Seg_9957" s="T595">ptcl</ta>
            <ta e="T597" id="Seg_9958" s="T596">v</ta>
            <ta e="T598" id="Seg_9959" s="T597">ptcl</ta>
            <ta e="T599" id="Seg_9960" s="T598">que</ta>
            <ta e="T600" id="Seg_9961" s="T599">cop</ta>
            <ta e="T601" id="Seg_9962" s="T600">n</ta>
            <ta e="T602" id="Seg_9963" s="T601">n</ta>
            <ta e="T603" id="Seg_9964" s="T602">v</ta>
            <ta e="T604" id="Seg_9965" s="T603">ptcl</ta>
            <ta e="T606" id="Seg_9966" s="T605">ptcl</ta>
            <ta e="T607" id="Seg_9967" s="T606">dempro</ta>
            <ta e="T608" id="Seg_9968" s="T607">v</ta>
            <ta e="T609" id="Seg_9969" s="T608">v</ta>
            <ta e="T610" id="Seg_9970" s="T609">v</ta>
            <ta e="T611" id="Seg_9971" s="T610">ptcl</ta>
            <ta e="T612" id="Seg_9972" s="T611">v</ta>
            <ta e="T613" id="Seg_9973" s="T612">v</ta>
            <ta e="T614" id="Seg_9974" s="T613">ptcl</ta>
            <ta e="T615" id="Seg_9975" s="T614">v</ta>
            <ta e="T616" id="Seg_9976" s="T615">ptcl</ta>
            <ta e="T618" id="Seg_9977" s="T617">ordnum</ta>
            <ta e="T619" id="Seg_9978" s="T618">ptcl</ta>
            <ta e="T620" id="Seg_9979" s="T619">n</ta>
            <ta e="T621" id="Seg_9980" s="T620">ptcl</ta>
            <ta e="T622" id="Seg_9981" s="T621">dempro</ta>
            <ta e="T623" id="Seg_9982" s="T622">n</ta>
            <ta e="T624" id="Seg_9983" s="T623">que</ta>
            <ta e="T625" id="Seg_9984" s="T624">n</ta>
            <ta e="T627" id="Seg_9985" s="T626">dempro</ta>
            <ta e="T628" id="Seg_9986" s="T627">adv</ta>
            <ta e="T629" id="Seg_9987" s="T628">dempro</ta>
            <ta e="T630" id="Seg_9988" s="T629">n</ta>
            <ta e="T631" id="Seg_9989" s="T630">ptcl</ta>
            <ta e="T632" id="Seg_9990" s="T631">adv</ta>
            <ta e="T633" id="Seg_9991" s="T632">n</ta>
            <ta e="T634" id="Seg_9992" s="T633">ptcl</ta>
            <ta e="T635" id="Seg_9993" s="T634">adj</ta>
            <ta e="T636" id="Seg_9994" s="T635">cop</ta>
            <ta e="T637" id="Seg_9995" s="T636">adj</ta>
            <ta e="T638" id="Seg_9996" s="T637">cop</ta>
            <ta e="T639" id="Seg_9997" s="T638">dempro</ta>
            <ta e="T640" id="Seg_9998" s="T639">adj</ta>
            <ta e="T641" id="Seg_9999" s="T640">adv</ta>
            <ta e="T642" id="Seg_10000" s="T641">dempro</ta>
            <ta e="T643" id="Seg_10001" s="T642">v</ta>
            <ta e="T644" id="Seg_10002" s="T643">v</ta>
            <ta e="T645" id="Seg_10003" s="T644">ptcl</ta>
            <ta e="T647" id="Seg_10004" s="T646">n</ta>
            <ta e="T648" id="Seg_10005" s="T647">n</ta>
            <ta e="T649" id="Seg_10006" s="T648">v</ta>
            <ta e="T651" id="Seg_10007" s="T650">dempro</ta>
            <ta e="T652" id="Seg_10008" s="T651">n</ta>
            <ta e="T653" id="Seg_10009" s="T652">adv</ta>
            <ta e="T654" id="Seg_10010" s="T653">adj</ta>
            <ta e="T655" id="Seg_10011" s="T654">cop</ta>
            <ta e="T656" id="Seg_10012" s="T655">adv</ta>
            <ta e="T657" id="Seg_10013" s="T656">ptcl</ta>
            <ta e="T658" id="Seg_10014" s="T657">adj</ta>
            <ta e="T660" id="Seg_10015" s="T659">dempro</ta>
            <ta e="T661" id="Seg_10016" s="T660">post</ta>
            <ta e="T662" id="Seg_10017" s="T661">n</ta>
            <ta e="T663" id="Seg_10018" s="T662">dempro</ta>
            <ta e="T664" id="Seg_10019" s="T663">que</ta>
            <ta e="T665" id="Seg_10020" s="T664">v</ta>
            <ta e="T666" id="Seg_10021" s="T665">v</ta>
            <ta e="T667" id="Seg_10022" s="T666">n</ta>
            <ta e="T669" id="Seg_10023" s="T668">dempro</ta>
            <ta e="T670" id="Seg_10024" s="T669">n</ta>
            <ta e="T671" id="Seg_10025" s="T670">v</ta>
            <ta e="T672" id="Seg_10026" s="T671">aux</ta>
            <ta e="T673" id="Seg_10027" s="T672">n</ta>
            <ta e="T674" id="Seg_10028" s="T673">n</ta>
            <ta e="T675" id="Seg_10029" s="T674">v</ta>
            <ta e="T677" id="Seg_10030" s="T676">n</ta>
            <ta e="T678" id="Seg_10031" s="T677">ptcl</ta>
            <ta e="T679" id="Seg_10032" s="T678">cop</ta>
            <ta e="T680" id="Seg_10033" s="T679">dempro</ta>
            <ta e="T681" id="Seg_10034" s="T680">que</ta>
            <ta e="T682" id="Seg_10035" s="T681">n</ta>
            <ta e="T683" id="Seg_10036" s="T682">n</ta>
            <ta e="T684" id="Seg_10037" s="T683">n</ta>
            <ta e="T685" id="Seg_10038" s="T684">n</ta>
            <ta e="T686" id="Seg_10039" s="T685">n</ta>
            <ta e="T687" id="Seg_10040" s="T686">v</ta>
            <ta e="T689" id="Seg_10041" s="T688">adv</ta>
            <ta e="T690" id="Seg_10042" s="T689">n</ta>
            <ta e="T691" id="Seg_10043" s="T690">dempro</ta>
            <ta e="T692" id="Seg_10044" s="T691">n</ta>
            <ta e="T693" id="Seg_10045" s="T692">v</ta>
            <ta e="T694" id="Seg_10046" s="T693">ptcl</ta>
            <ta e="T696" id="Seg_10047" s="T695">conj</ta>
            <ta e="T697" id="Seg_10048" s="T696">indfpro</ta>
            <ta e="T698" id="Seg_10049" s="T697">n</ta>
            <ta e="T699" id="Seg_10050" s="T698">adv</ta>
            <ta e="T700" id="Seg_10051" s="T699">v</ta>
            <ta e="T701" id="Seg_10052" s="T700">ptcl</ta>
            <ta e="T703" id="Seg_10053" s="T702">dempro</ta>
            <ta e="T704" id="Seg_10054" s="T703">post</ta>
            <ta e="T705" id="Seg_10055" s="T704">conj</ta>
            <ta e="T706" id="Seg_10056" s="T705">v</ta>
            <ta e="T707" id="Seg_10057" s="T706">aux</ta>
            <ta e="T708" id="Seg_10058" s="T707">aux</ta>
            <ta e="T709" id="Seg_10059" s="T708">ptcl</ta>
            <ta e="T711" id="Seg_10060" s="T710">conj</ta>
            <ta e="T712" id="Seg_10061" s="T711">pers</ta>
            <ta e="T713" id="Seg_10062" s="T712">pers</ta>
            <ta e="T714" id="Seg_10063" s="T713">ptcl</ta>
            <ta e="T715" id="Seg_10064" s="T714">v</ta>
            <ta e="T716" id="Seg_10065" s="T715">ptcl</ta>
            <ta e="T717" id="Seg_10066" s="T716">pers</ta>
            <ta e="T718" id="Seg_10067" s="T717">v</ta>
            <ta e="T719" id="Seg_10068" s="T718">ptcl</ta>
            <ta e="T720" id="Seg_10069" s="T719">cardnum</ta>
            <ta e="T721" id="Seg_10070" s="T720">cardnum</ta>
            <ta e="T722" id="Seg_10071" s="T721">n</ta>
            <ta e="T723" id="Seg_10072" s="T722">n</ta>
            <ta e="T724" id="Seg_10073" s="T723">n</ta>
            <ta e="T868" id="Seg_10074" s="T724">que</ta>
            <ta e="T869" id="Seg_10075" s="T868">que</ta>
            <ta e="T870" id="Seg_10076" s="T869">n</ta>
            <ta e="T871" id="Seg_10077" s="T870">que</ta>
            <ta e="T872" id="Seg_10078" s="T871">n</ta>
            <ta e="T873" id="Seg_10079" s="T872">dempro</ta>
            <ta e="T874" id="Seg_10080" s="T873">n</ta>
            <ta e="T725" id="Seg_10081" s="T874">v</ta>
            <ta e="T726" id="Seg_10082" s="T725">n</ta>
            <ta e="T727" id="Seg_10083" s="T726">v</ta>
            <ta e="T728" id="Seg_10084" s="T727">dempro</ta>
            <ta e="T729" id="Seg_10085" s="T728">n</ta>
            <ta e="T730" id="Seg_10086" s="T729">n</ta>
            <ta e="T731" id="Seg_10087" s="T730">v</ta>
            <ta e="T732" id="Seg_10088" s="T731">adj</ta>
            <ta e="T733" id="Seg_10089" s="T732">n</ta>
            <ta e="T734" id="Seg_10090" s="T733">v</ta>
            <ta e="T736" id="Seg_10091" s="T735">ptcl</ta>
            <ta e="T737" id="Seg_10092" s="T736">dempro</ta>
            <ta e="T738" id="Seg_10093" s="T737">n</ta>
            <ta e="T739" id="Seg_10094" s="T738">n</ta>
            <ta e="T740" id="Seg_10095" s="T739">post</ta>
            <ta e="T741" id="Seg_10096" s="T740">v</ta>
            <ta e="T742" id="Seg_10097" s="T741">dempro</ta>
            <ta e="T743" id="Seg_10098" s="T742">v</ta>
            <ta e="T744" id="Seg_10099" s="T743">ptcl</ta>
            <ta e="T745" id="Seg_10100" s="T744">que</ta>
            <ta e="T747" id="Seg_10101" s="T746">dempro</ta>
            <ta e="T748" id="Seg_10102" s="T747">v</ta>
            <ta e="T749" id="Seg_10103" s="T748">post</ta>
            <ta e="T750" id="Seg_10104" s="T749">v</ta>
            <ta e="T751" id="Seg_10105" s="T750">n</ta>
            <ta e="T752" id="Seg_10106" s="T751">n</ta>
            <ta e="T753" id="Seg_10107" s="T752">post</ta>
            <ta e="T754" id="Seg_10108" s="T753">v</ta>
            <ta e="T756" id="Seg_10109" s="T755">adv</ta>
            <ta e="T757" id="Seg_10110" s="T756">v</ta>
            <ta e="T758" id="Seg_10111" s="T757">dempro</ta>
            <ta e="T759" id="Seg_10112" s="T758">v</ta>
            <ta e="T760" id="Seg_10113" s="T759">post</ta>
            <ta e="T762" id="Seg_10114" s="T761">emphpro</ta>
            <ta e="T763" id="Seg_10115" s="T762">emphpro</ta>
            <ta e="T764" id="Seg_10116" s="T763">v</ta>
            <ta e="T765" id="Seg_10117" s="T764">ptcl</ta>
            <ta e="T766" id="Seg_10118" s="T765">ptcl</ta>
            <ta e="T768" id="Seg_10119" s="T767">pers</ta>
            <ta e="T769" id="Seg_10120" s="T768">n</ta>
            <ta e="T770" id="Seg_10121" s="T769">v</ta>
            <ta e="T771" id="Seg_10122" s="T770">n</ta>
            <ta e="T772" id="Seg_10123" s="T771">adj</ta>
            <ta e="T773" id="Seg_10124" s="T772">n</ta>
            <ta e="T774" id="Seg_10125" s="T773">adv</ta>
            <ta e="T775" id="Seg_10126" s="T774">v</ta>
            <ta e="T777" id="Seg_10127" s="T776">v</ta>
            <ta e="T778" id="Seg_10128" s="T777">dempro</ta>
            <ta e="T779" id="Seg_10129" s="T778">n</ta>
            <ta e="T781" id="Seg_10130" s="T780">n</ta>
            <ta e="T782" id="Seg_10131" s="T781">adj</ta>
            <ta e="T783" id="Seg_10132" s="T782">n</ta>
            <ta e="T784" id="Seg_10133" s="T783">v</ta>
            <ta e="T785" id="Seg_10134" s="T784">ptcl</ta>
            <ta e="T787" id="Seg_10135" s="T786">pers</ta>
            <ta e="T788" id="Seg_10136" s="T787">ptcl</ta>
            <ta e="T789" id="Seg_10137" s="T788">adj</ta>
            <ta e="T790" id="Seg_10138" s="T789">dempro</ta>
            <ta e="T791" id="Seg_10139" s="T790">n</ta>
            <ta e="T792" id="Seg_10140" s="T791">v</ta>
            <ta e="T794" id="Seg_10141" s="T793">n</ta>
            <ta e="T795" id="Seg_10142" s="T794">v</ta>
            <ta e="T796" id="Seg_10143" s="T795">ptcl</ta>
            <ta e="T797" id="Seg_10144" s="T796">dempro</ta>
            <ta e="T798" id="Seg_10145" s="T797">n</ta>
            <ta e="T800" id="Seg_10146" s="T799">n</ta>
            <ta e="T801" id="Seg_10147" s="T800">conj</ta>
            <ta e="T802" id="Seg_10148" s="T801">v</ta>
            <ta e="T803" id="Seg_10149" s="T802">ptcl</ta>
            <ta e="T804" id="Seg_10150" s="T803">n</ta>
            <ta e="T806" id="Seg_10151" s="T805">ptcl</ta>
            <ta e="T807" id="Seg_10152" s="T806">dempro</ta>
            <ta e="T808" id="Seg_10153" s="T807">conj</ta>
            <ta e="T809" id="Seg_10154" s="T808">cop</ta>
            <ta e="T810" id="Seg_10155" s="T809">ptcl</ta>
            <ta e="T811" id="Seg_10156" s="T810">v</ta>
            <ta e="T812" id="Seg_10157" s="T811">ptcl</ta>
            <ta e="T813" id="Seg_10158" s="T812">v</ta>
            <ta e="T814" id="Seg_10159" s="T813">v</ta>
            <ta e="T815" id="Seg_10160" s="T814">v</ta>
            <ta e="T816" id="Seg_10161" s="T815">ptcl</ta>
            <ta e="T817" id="Seg_10162" s="T816">pers</ta>
            <ta e="T819" id="Seg_10163" s="T818">ptcl</ta>
            <ta e="T820" id="Seg_10164" s="T819">dempro</ta>
            <ta e="T821" id="Seg_10165" s="T820">post</ta>
            <ta e="T822" id="Seg_10166" s="T821">v</ta>
            <ta e="T824" id="Seg_10167" s="T823">dempro</ta>
            <ta e="T825" id="Seg_10168" s="T824">cardnum</ta>
            <ta e="T826" id="Seg_10169" s="T825">ptcl</ta>
            <ta e="T827" id="Seg_10170" s="T826">n</ta>
            <ta e="T828" id="Seg_10171" s="T827">n</ta>
            <ta e="T829" id="Seg_10172" s="T828">n</ta>
            <ta e="T830" id="Seg_10173" s="T829">n</ta>
            <ta e="T831" id="Seg_10174" s="T830">ptcl</ta>
            <ta e="T832" id="Seg_10175" s="T831">v</ta>
            <ta e="T833" id="Seg_10176" s="T832">aux</ta>
            <ta e="T834" id="Seg_10177" s="T833">n</ta>
            <ta e="T835" id="Seg_10178" s="T834">v</ta>
            <ta e="T836" id="Seg_10179" s="T835">dempro</ta>
            <ta e="T837" id="Seg_10180" s="T836">dempro</ta>
            <ta e="T838" id="Seg_10181" s="T837">v</ta>
            <ta e="T839" id="Seg_10182" s="T838">n</ta>
            <ta e="T840" id="Seg_10183" s="T839">n</ta>
            <ta e="T841" id="Seg_10184" s="T840">ptcl</ta>
            <ta e="T842" id="Seg_10185" s="T841">adv</ta>
            <ta e="T843" id="Seg_10186" s="T842">v</ta>
            <ta e="T844" id="Seg_10187" s="T843">ptcl</ta>
            <ta e="T845" id="Seg_10188" s="T844">ptcl</ta>
            <ta e="T846" id="Seg_10189" s="T845">n</ta>
            <ta e="T847" id="Seg_10190" s="T846">v</ta>
            <ta e="T848" id="Seg_10191" s="T847">ptcl</ta>
            <ta e="T849" id="Seg_10192" s="T848">v</ta>
            <ta e="T851" id="Seg_10193" s="T850">v</ta>
            <ta e="T852" id="Seg_10194" s="T851">v</ta>
            <ta e="T853" id="Seg_10195" s="T852">adv</ta>
            <ta e="T854" id="Seg_10196" s="T853">v</ta>
            <ta e="T855" id="Seg_10197" s="T854">ptcl</ta>
            <ta e="T856" id="Seg_10198" s="T855">pers</ta>
            <ta e="T858" id="Seg_10199" s="T857">dempro</ta>
            <ta e="T859" id="Seg_10200" s="T858">post</ta>
            <ta e="T860" id="Seg_10201" s="T859">dempro</ta>
            <ta e="T861" id="Seg_10202" s="T860">v</ta>
            <ta e="T862" id="Seg_10203" s="T861">v</ta>
            <ta e="T863" id="Seg_10204" s="T862">ptcl</ta>
            <ta e="T864" id="Seg_10205" s="T863">dempro</ta>
            <ta e="T865" id="Seg_10206" s="T864">n</ta>
            <ta e="T866" id="Seg_10207" s="T865">v</ta>
            <ta e="T867" id="Seg_10208" s="T866">adv</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T69" id="Seg_10209" s="T68">EV:cult</ta>
            <ta e="T92" id="Seg_10210" s="T91">EV:cult</ta>
            <ta e="T106" id="Seg_10211" s="T105">EV:cult</ta>
            <ta e="T167" id="Seg_10212" s="T166">RUS:cult</ta>
            <ta e="T184" id="Seg_10213" s="T183">RUS:gram</ta>
            <ta e="T231" id="Seg_10214" s="T230">EV:cult</ta>
            <ta e="T238" id="Seg_10215" s="T237">EV:cult</ta>
            <ta e="T278" id="Seg_10216" s="T277">EV:cult</ta>
            <ta e="T281" id="Seg_10217" s="T280">EV:cult</ta>
            <ta e="T307" id="Seg_10218" s="T306">RUS:gram</ta>
            <ta e="T370" id="Seg_10219" s="T369">RUS:mod</ta>
            <ta e="T379" id="Seg_10220" s="T378">RUS:mod</ta>
            <ta e="T456" id="Seg_10221" s="T455">EV:cult</ta>
            <ta e="T507" id="Seg_10222" s="T506">RUS:mod</ta>
            <ta e="T516" id="Seg_10223" s="T515">RUS:gram</ta>
            <ta e="T521" id="Seg_10224" s="T520">RUS:gram</ta>
            <ta e="T538" id="Seg_10225" s="T537">EV:gram (DIM)</ta>
            <ta e="T648" id="Seg_10226" s="T647">EV:cult</ta>
            <ta e="T656" id="Seg_10227" s="T655">RUS:mod</ta>
            <ta e="T678" id="Seg_10228" s="T677">RUS:gram</ta>
            <ta e="T696" id="Seg_10229" s="T695">RUS:gram</ta>
            <ta e="T705" id="Seg_10230" s="T704">RUS:gram</ta>
            <ta e="T711" id="Seg_10231" s="T710">RUS:gram</ta>
            <ta e="T769" id="Seg_10232" s="T768">EV:core</ta>
            <ta e="T771" id="Seg_10233" s="T770">EV:core</ta>
            <ta e="T773" id="Seg_10234" s="T772">EV:core</ta>
            <ta e="T774" id="Seg_10235" s="T773">EV:gram (DIM)</ta>
            <ta e="T801" id="Seg_10236" s="T800">RUS:gram</ta>
            <ta e="T808" id="Seg_10237" s="T807">RUS:gram</ta>
            <ta e="T853" id="Seg_10238" s="T852">EV:gram (DIM)</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T69" id="Seg_10239" s="T68">Vsub Vsub</ta>
            <ta e="T92" id="Seg_10240" s="T91">Vsub Vsub</ta>
            <ta e="T106" id="Seg_10241" s="T105">Vsub Vsub</ta>
            <ta e="T231" id="Seg_10242" s="T230">fortition Vsub Vsub Vsub finCins</ta>
            <ta e="T238" id="Seg_10243" s="T237">fortition Vsub Vsub Vsub finCins</ta>
            <ta e="T278" id="Seg_10244" s="T277">fortition Vsub Vsub Vsub finCins</ta>
            <ta e="T281" id="Seg_10245" s="T280">fortition Vsub Vsub Vsub finCins</ta>
            <ta e="T456" id="Seg_10246" s="T455">fortition Vsub Vsub Vsub finCins</ta>
            <ta e="T648" id="Seg_10247" s="T647">fortition Vsub Vsub Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T69" id="Seg_10248" s="T68">dir:bare</ta>
            <ta e="T92" id="Seg_10249" s="T91">dir:infl</ta>
            <ta e="T106" id="Seg_10250" s="T105">dir:bare</ta>
            <ta e="T167" id="Seg_10251" s="T166">dir:infl</ta>
            <ta e="T184" id="Seg_10252" s="T183">dir:bare</ta>
            <ta e="T231" id="Seg_10253" s="T230">dir:infl</ta>
            <ta e="T238" id="Seg_10254" s="T237">dir:infl</ta>
            <ta e="T278" id="Seg_10255" s="T277">dir:infl</ta>
            <ta e="T281" id="Seg_10256" s="T280">dir:infl</ta>
            <ta e="T307" id="Seg_10257" s="T306">dir:bare</ta>
            <ta e="T370" id="Seg_10258" s="T369">dir:bare</ta>
            <ta e="T379" id="Seg_10259" s="T378">dir:bare</ta>
            <ta e="T456" id="Seg_10260" s="T455">dir:infl</ta>
            <ta e="T507" id="Seg_10261" s="T506">dir:bare</ta>
            <ta e="T516" id="Seg_10262" s="T515">dir:bare</ta>
            <ta e="T521" id="Seg_10263" s="T520">dir:bare</ta>
            <ta e="T648" id="Seg_10264" s="T647">dir:infl</ta>
            <ta e="T656" id="Seg_10265" s="T655">dir:bare</ta>
            <ta e="T678" id="Seg_10266" s="T677">dir:bare</ta>
            <ta e="T696" id="Seg_10267" s="T695">dir:bare</ta>
            <ta e="T705" id="Seg_10268" s="T704">dir:bare</ta>
            <ta e="T711" id="Seg_10269" s="T710">dir:bare</ta>
            <ta e="T769" id="Seg_10270" s="T768">dir:infl</ta>
            <ta e="T771" id="Seg_10271" s="T770">dir:infl</ta>
            <ta e="T773" id="Seg_10272" s="T772">dir:infl</ta>
            <ta e="T801" id="Seg_10273" s="T800">dir:bare</ta>
            <ta e="T808" id="Seg_10274" s="T807">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T15" id="Seg_10275" s="T1">Migrating… after having migrated all winter, there is a place where the Dolgans stop for spring.</ta>
            <ta e="T24" id="Seg_10276" s="T16">There stand very many pole tents, and sleigh houses.</ta>
            <ta e="T35" id="Seg_10277" s="T25">The spring days with a light cool, when the sun shines, well, the work of the women increases.</ta>
            <ta e="T43" id="Seg_10278" s="T36">Outside they first do this work with skin.</ta>
            <ta e="T50" id="Seg_10279" s="T44">First they clean the skin very well.</ta>
            <ta e="T57" id="Seg_10280" s="T51">After having cleaned, they cut the fur with scissors.</ta>
            <ta e="T67" id="Seg_10281" s="T58">When they finish cutting with the scissors, then later they also cut with a knife. </ta>
            <ta e="T76" id="Seg_10282" s="T68">The tent cover… the Dolgans do not take very furry skins.</ta>
            <ta e="T93" id="Seg_10283" s="T77">From the sheared skin they take the skin of which they took the fur, the cover of the house, they sew leather covers.</ta>
            <ta e="T102" id="Seg_10284" s="T94">There they leave a little bit of fur.</ta>
            <ta e="T107" id="Seg_10285" s="T103">Then with this they sew a leather cover.</ta>
            <ta e="T116" id="Seg_10286" s="T108">From several skins they have to make a thin layer of skin.</ta>
            <ta e="T124" id="Seg_10287" s="T117">For the production of the thin leather there is here also a lot ot work.</ta>
            <ta e="T141" id="Seg_10288" s="T125">They cut, as they used to cut, so there is no fur, so there is little fur, there will remain some hair on the skin anyway.</ta>
            <ta e="T163" id="Seg_10289" s="T142">The fur, so the fur comes off easily, and then it's good for the processing, so it becomes a thin layer of skin, they put the skin into the water.</ta>
            <ta e="T169" id="Seg_10290" s="T164">Into the water they pour soap, they put it in.</ta>
            <ta e="T173" id="Seg_10291" s="T170">Then various herbs.</ta>
            <ta e="T181" id="Seg_10292" s="T174">Doing that, it has to lie for several days.</ta>
            <ta e="T197" id="Seg_10293" s="T182">Let it be a warm day, the water ferments, the skin also ferments, becomes soft.</ta>
            <ta e="T207" id="Seg_10294" s="T198">After the skin has fermented for a few days, they stir it.</ta>
            <ta e="T217" id="Seg_10295" s="T208">After they took the skin they wash it after having gone to the river.</ta>
            <ta e="T224" id="Seg_10296" s="T218">They wash it well, and after that they dry it.</ta>
            <ta e="T240" id="Seg_10297" s="T225">After they have dried it they whatchamacallit with a leather scraper, they take the remaining fur and scrape it off, when saying it in Dolgan.</ta>
            <ta e="T257" id="Seg_10298" s="T241">This is also not fast, not easy, the women… it is a very hard job for the women.</ta>
            <ta e="T270" id="Seg_10299" s="T258">Therefore in one go they don't finish all the work, it is hard work.</ta>
            <ta e="T279" id="Seg_10300" s="T271">Then the next day they will do it again: they will take off the fur with a leather scraper.</ta>
            <ta e="T292" id="Seg_10301" s="T280">After finishing scraping off the fur, the skin becomes… it becomes leather without what, without fur.</ta>
            <ta e="T304" id="Seg_10302" s="T293">And then so it will be even, they scrape it with a smaller scraper, whatever, they scrape in various ways they scrape with a scraper and so on.</ta>
            <ta e="T311" id="Seg_10303" s="T305">Even with a knife they even it and so on.</ta>
            <ta e="T319" id="Seg_10304" s="T312">They take away everything bad.</ta>
            <ta e="T329" id="Seg_10305" s="T320">That thin layer of skin, when you look, becomes good.</ta>
            <ta e="T343" id="Seg_10306" s="T330">Then so it becomes real (good) leather, and darkly smoked, they also have to soften it.</ta>
            <ta e="T353" id="Seg_10307" s="T344">Before they soften it, they whatchammacallit, they call the girls for some reason.</ta>
            <ta e="T365" id="Seg_10308" s="T354">They hang up the skin onto whatchamacallits, a clothes hanger.</ta>
            <ta e="T370" id="Seg_10309" s="T366">It's necessary to press it down.</ta>
            <ta e="T385" id="Seg_10310" s="T371">It is also necessary to press it down with the upside down side of a sleigh, you put down the skin in order to get thin leather.</ta>
            <ta e="T390" id="Seg_10311" s="T386">You stand and press it down.</ta>
            <ta e="T402" id="Seg_10312" s="T391">When you press it down, they give you what… liver, a boiled reindeer liver.</ta>
            <ta e="T411" id="Seg_10313" s="T402">This liver the girl who is pressing (the leather) has to chew.</ta>
            <ta e="T417" id="Seg_10314" s="T411">"Chew!" they say, where can you go?</ta>
            <ta e="T427" id="Seg_10315" s="T418">Against your will, the taste is bad, you stand anyway.</ta>
            <ta e="T432" id="Seg_10316" s="T428">You listen to the word of your mother.</ta>
            <ta e="T442" id="Seg_10317" s="T433">You stand and keep chewing the liver, while you stand.</ta>
            <ta e="T461" id="Seg_10318" s="T443">They take the chewed liver, and after they take the scraped skin they smear it.</ta>
            <ta e="T469" id="Seg_10319" s="T462">Then they scrape it with a toothed scraper, with their hands.</ta>
            <ta e="T490" id="Seg_10320" s="T470">With one hand she holds the skin, with one hand she holds the toothed scraper, and she scrapes from up to down, smearing (the liver).</ta>
            <ta e="T503" id="Seg_10321" s="T491">The girls never want to stand there.</ta>
            <ta e="T514" id="Seg_10322" s="T504">You need to stand and press, you need to stand for a very long time.</ta>
            <ta e="T522" id="Seg_10323" s="T515">And the chewing is very very bad.</ta>
            <ta e="T532" id="Seg_10324" s="T523">When you chew you feel sick sometimes.</ta>
            <ta e="T543" id="Seg_10325" s="T533">Therefore the girls ran away, in order not to have to chew, they ran away.</ta>
            <ta e="T553" id="Seg_10326" s="T544">For some reason they called the girls more often than the boys.</ta>
            <ta e="T570" id="Seg_10327" s="T554">Well, after scraping the skin with a toothed scraper a lot, they whatevered the skin.</ta>
            <ta e="T577" id="Seg_10328" s="T571">You scrape with a toothed scraper again for very long.</ta>
            <ta e="T586" id="Seg_10329" s="T578">The next day again you have to stand chewing.</ta>
            <ta e="T592" id="Seg_10330" s="T587">You don't want to and you feel sick.</ta>
            <ta e="T604" id="Seg_10331" s="T593">And again you chew after all, where can you go, you listen to the word of your mother.</ta>
            <ta e="T616" id="Seg_10332" s="T605">You chew and chew, being very unhappy, and then you finish.</ta>
            <ta e="T625" id="Seg_10333" s="T617">On some days they scrape with a (small) scraper.</ta>
            <ta e="T645" id="Seg_10334" s="T626">Then later, so it beomes good and soft leather we can do various things.</ta>
            <ta e="T649" id="Seg_10335" s="T646">We do it with a small scraper, with a larger scraper.</ta>
            <ta e="T658" id="Seg_10336" s="T650">Then the leather when the leather is too (thin?) it is bad after all.</ta>
            <ta e="T667" id="Seg_10337" s="T659">Therefore they smoke the skin in smoke.</ta>
            <ta e="T675" id="Seg_10338" s="T668">After taking the skin they hang it up in the tent.</ta>
            <ta e="T687" id="Seg_10339" s="T676">The house, in the pole tent in the pole tent they hang it up.</ta>
            <ta e="T694" id="Seg_10340" s="T688">There it sits close to the smoke (?).</ta>
            <ta e="T701" id="Seg_10341" s="T695">And some women do it differently.</ta>
            <ta e="T709" id="Seg_10342" s="T702">They could also drench it in smoke.</ta>
            <ta e="T724" id="Seg_10343" s="T710">But s/he, they say like that, they make us stand, three, four girls.</ta>
            <ta e="T725" id="Seg_10344" s="T724">Whatchamacallit, the fire, whatchamacallit, the fire, they heat [lit. heat] the stove.</ta>
            <ta e="T734" id="Seg_10345" s="T725">The wood burns and then on top they throw grass, so thick smoke appears.</ta>
            <ta e="T745" id="Seg_10346" s="T735">Well standing around the smoke we hold it.</ta>
            <ta e="T754" id="Seg_10347" s="T746">Then we turn it around, in the direction of the sun we turn it around.</ta>
            <ta e="T760" id="Seg_10348" s="T755">You have to hold it well, so you don't let it slip.</ta>
            <ta e="T766" id="Seg_10349" s="T761">We turn it around to each other like this.</ta>
            <ta e="T779" id="Seg_10350" s="T767">I give it to my friend, my friend to the next friend, giving like this we turn the skin around.</ta>
            <ta e="T785" id="Seg_10351" s="T780">From below a thick smoke comes.</ta>
            <ta e="T792" id="Seg_10352" s="T786">We also feel sick of the coming smoke.</ta>
            <ta e="T798" id="Seg_10353" s="T793">Our eyes sting from the smoke.</ta>
            <ta e="T804" id="Seg_10354" s="T799">The smoke gets into our mouths.</ta>
            <ta e="T817" id="Seg_10355" s="T805">Nonetheless we have to stand: stand stand they say to us.</ta>
            <ta e="T822" id="Seg_10356" s="T818">That's how we turn it around.</ta>
            <ta e="T849" id="Seg_10357" s="T823">Then one girl lets the skin go like that from her hand, she grabs past, (past) the place place she was holding, the skin, then they say: 'Well, you won't be getting married, when you grow up.'</ta>
            <ta e="T856" id="Seg_10358" s="T850">Like that they talk to us.</ta>
            <ta e="T867" id="Seg_10359" s="T857">Therefore we stand there being unhappy not letting go that skin, so.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T15" id="Seg_10360" s="T1">Nomadisieren…nachdem man den ganzen Winter nomadisiert ist, gibt es einen Platz, wo die Dolganen für den Frühling anhalten.</ta>
            <ta e="T24" id="Seg_10361" s="T16">Dort stehen sehr viele Stangenzelte und Schlittenhäuser. </ta>
            <ta e="T35" id="Seg_10362" s="T25">Die Frühlingstage mit einer leichten Kühle, wenn die Sonne scheint, nun, die Arbeit der Frauen wird mehr.</ta>
            <ta e="T43" id="Seg_10363" s="T36">Draußen machen sie zuerst diese Arbeit mit Häuten.</ta>
            <ta e="T50" id="Seg_10364" s="T44">Zuerst säubern sie die Haut sehr gut.</ta>
            <ta e="T57" id="Seg_10365" s="T51">Nachdem sie sie gesäubert haben, schneiden sie das Fell mit einer Schere.</ta>
            <ta e="T67" id="Seg_10366" s="T58">Wenn sie fertig sind mit dem Schneiden mit der Schere, dann schneiden sie später auch mit einem Messer.</ta>
            <ta e="T76" id="Seg_10367" s="T68">Die Abdeckung des Zeltes… die Dolganen nehmen nicht sehr pelzige Häute.</ta>
            <ta e="T93" id="Seg_10368" s="T77">Von der geschorenen Haut nehmen sie die Haut, von der sie das Fell abgeschnitten haben, die Abdeckung des Zeltes, sie nähen Lederabdeckungen.</ta>
            <ta e="T102" id="Seg_10369" s="T94">Dort lassen sie etwas Fell übrig.</ta>
            <ta e="T107" id="Seg_10370" s="T103">Dann nähen sie damit eine Lederabdeckung.</ta>
            <ta e="T116" id="Seg_10371" s="T108">Aus einigen Häuten müssen sie eine dünne Lederschicht machen. </ta>
            <ta e="T124" id="Seg_10372" s="T117">Zur Produktion von dünnem Leder gibt es auch viel Arbeit.</ta>
            <ta e="T141" id="Seg_10373" s="T125">Sie schneiden, wie sie [früher] schnitten, damit kein Fell da ist, damit nur wenig Fell da ist, einige Haare werden ohnehin auf der Haut bleiben.</ta>
            <ta e="T163" id="Seg_10374" s="T142">Das Fell, damit das Fell einfach abgeht, und dann ist es gut zum Verarbeiten, damit es eine dünne Lederschicht wird, sie legen die Haut in Wasser.</ta>
            <ta e="T169" id="Seg_10375" s="T164">Ins Wasser gießen sie Seife, sie tun sie hinein.</ta>
            <ta e="T173" id="Seg_10376" s="T170">Dann unterschiedliche Kräuter.</ta>
            <ta e="T181" id="Seg_10377" s="T174">Wenn man das macht, muss es ein paar Tage liegen.</ta>
            <ta e="T197" id="Seg_10378" s="T182">Soll es ein warmer Tag sein, [dann] fault das Wasser, die Haut fault auch, sie wird weich.</ta>
            <ta e="T207" id="Seg_10379" s="T198">Nachdem die Haut ein paar Tage lang gefault ist, vermischen sie es.</ta>
            <ta e="T217" id="Seg_10380" s="T208">Nachdem sie die Haut genommen haben, waschen sie sie, nachdem sie zum Fluss gegangen sind.</ta>
            <ta e="T224" id="Seg_10381" s="T218">Sie waschen sie gut und danach trocknen sie sie.</ta>
            <ta e="T240" id="Seg_10382" s="T225">Nachdem sie sie getrocknet haben, dingsen sie sie mit einem Lederschaber, sie nehmen das restliche Fell und kratzen es ab, wenn man es auf Dolganisch sagt.</ta>
            <ta e="T257" id="Seg_10383" s="T241">Das ist auch nicht, nicht einfach, die Frauen… es ist sehr harte Arbeit für die Frauen.</ta>
            <ta e="T270" id="Seg_10384" s="T258">Deshalb schaffen sie nicht alle Arbeit auf einmal, es ist harte Arbeit.</ta>
            <ta e="T279" id="Seg_10385" s="T271">Dann am nächsten Tag werden sie es wieder machen: sie werden das Fell mit einem Lederschaber abmachen.</ta>
            <ta e="T292" id="Seg_10386" s="T280">Wenn sie fertig sind mit dem Schaben, dann wird die Haut… sie wird Leder ohne dings, ohne Fell.</ta>
            <ta e="T304" id="Seg_10387" s="T293">Und dann, damit es flach wird, schaben sie es mit einem kleineren Schaber, wie auch immer, sie schaben auf unterschiedliche Weise, sie schaben mit einem Schaber und so.</ta>
            <ta e="T311" id="Seg_10388" s="T305">Sogar mit einem Messer glätten sie es und so weiter.</ta>
            <ta e="T319" id="Seg_10389" s="T312">Sie nehme alles Schlechte weg.</ta>
            <ta e="T329" id="Seg_10390" s="T320">Die dünne Lederschicht, wenn man guckt, wird gut.</ta>
            <ta e="T343" id="Seg_10391" s="T330">Dann, damit es wirklich [gutes] Leder wird, und damit es dunkel geräuchert ist, müssen sie es auch weich machen.</ta>
            <ta e="T353" id="Seg_10392" s="T344">Bevor sie es weich machen, dingsen sie, sie rufen aus irgendeinem Grund die Mädchen.</ta>
            <ta e="T365" id="Seg_10393" s="T354">Sie hängen die Haut auf Dingse auf, ein Kleideraufhänger.</ta>
            <ta e="T370" id="Seg_10394" s="T366">Man muss es runterdrücken.</ta>
            <ta e="T385" id="Seg_10395" s="T371">Man muss es auch mit der Unterseite eines Schlittens runterdrücken, man legt die Haut hin, damit es dünnes Leder wird.</ta>
            <ta e="T390" id="Seg_10396" s="T386">Du stehst und drückst es runter.</ta>
            <ta e="T402" id="Seg_10397" s="T391">Wenn du es runterdrückt, dann geben sie die Dings, Leber, eine gekochte Rentierleber.</ta>
            <ta e="T411" id="Seg_10398" s="T402">Diese Leber muss das Mädchen kauen, das drückt.</ta>
            <ta e="T417" id="Seg_10399" s="T411">"Kau!", sagen sie, wo soll man hin?</ta>
            <ta e="T427" id="Seg_10400" s="T418">Gegen deinen Willen, der Geschmack ist schlecht, doch du stehst.</ta>
            <ta e="T432" id="Seg_10401" s="T428">Du hörst auf die Worte deiner Mutter.</ta>
            <ta e="T442" id="Seg_10402" s="T433">Du stehst und kaust die Leber, während du stehst.</ta>
            <ta e="T461" id="Seg_10403" s="T443">Sie nehmen die gekaute Leber, und danach nehmen sie die abgeschabte Haut, sie beschmieren sie.</ta>
            <ta e="T469" id="Seg_10404" s="T462">Dann schaben sie sie mit einem Schaber mit Zähnen, mit den Händen.</ta>
            <ta e="T490" id="Seg_10405" s="T470">Mit einer Hand hält sie die Haut, mit der anderen Hand hält sie den Schaber mit Zähnen, und sie schabt von oben nach unten, schmierend.</ta>
            <ta e="T503" id="Seg_10406" s="T491">Nun, die Mädchen wollen nie dort stehen.</ta>
            <ta e="T514" id="Seg_10407" s="T504">Man muss stehen und drücken, man muss sehr lange stehen.</ta>
            <ta e="T522" id="Seg_10408" s="T515">Und das Kauen ist sehr schlecht.</ta>
            <ta e="T532" id="Seg_10409" s="T523">Wenn man kaut, wird einem manchmal schlecht.</ta>
            <ta e="T543" id="Seg_10410" s="T533">Deshalb rennen die Mädchen weg, damit sie nicht kauen, sie laufen weg.</ta>
            <ta e="T553" id="Seg_10411" s="T544">Aus irgendeinem Grund wurden Mädchen öfter gerufen als Jungen.</ta>
            <ta e="T570" id="Seg_10412" s="T554">Nun, nachdem die Haut mit einem Schaber mit Zähnen abgeschabt wurde, dingsen sie die Haut.</ta>
            <ta e="T577" id="Seg_10413" s="T571">Man schabt wieder sehr lange mit einem Schaber mit Zähnen.</ta>
            <ta e="T586" id="Seg_10414" s="T578">Am nächsten Tag muss man wieder stehen und kauen.</ta>
            <ta e="T592" id="Seg_10415" s="T587">Man will nicht und einem ist schlecht.</ta>
            <ta e="T604" id="Seg_10416" s="T593">Und man kaut dennoch wieder, wo soll man hin, man hört auf die Worte der Mutter.</ta>
            <ta e="T616" id="Seg_10417" s="T605">Man kaut und kaut und ist sehr unglücklich, und dann ist man fertig.</ta>
            <ta e="T625" id="Seg_10418" s="T617">An manchen Tagen kratzen sie mit einem (kleinen) Schaber.</ta>
            <ta e="T645" id="Seg_10419" s="T626">Dann später, damit es gut wird und weiches Leder, wir können unterschiedliche Sachen machen.</ta>
            <ta e="T649" id="Seg_10420" s="T646">Wir machen es mit einem kleinen Schaber, mit einem größeren Schaber.</ta>
            <ta e="T658" id="Seg_10421" s="T650">Dann das Leder, wenn das Leder zu (dünn?) ist, ist es schlecht.</ta>
            <ta e="T667" id="Seg_10422" s="T659">Deshalb räuchern sie die Haut in Rauch.</ta>
            <ta e="T675" id="Seg_10423" s="T668">Nachdem sie die Haut genommen haben, hängen sie sie im Zelt auf.</ta>
            <ta e="T687" id="Seg_10424" s="T676">Das Zelt, im Stangenzelt, sie hängen es im Stangenzelt auf.</ta>
            <ta e="T694" id="Seg_10425" s="T688">Dort steht es in der Nähe des Rauches (?).</ta>
            <ta e="T701" id="Seg_10426" s="T695">Und einige Frauen machen es so anders.</ta>
            <ta e="T709" id="Seg_10427" s="T702">So können sie es auch räuchern.</ta>
            <ta e="T724" id="Seg_10428" s="T710">Aber er/sie, so sagen sie, sie lassen uns stehen, drei, vier Mädchen.</ta>
            <ta e="T725" id="Seg_10429" s="T724">Dings, das Feuer, dings, das Feuer, sie heizen [wörtl. machen] den Ofen.</ta>
            <ta e="T734" id="Seg_10430" s="T725">Das Holz brennt und obendrauf werfen sie Gras, damit dicker Rauch entsteht.</ta>
            <ta e="T745" id="Seg_10431" s="T735">Nun, wir stehen um den Rauch herum und halten sie [= die Haut].</ta>
            <ta e="T754" id="Seg_10432" s="T746">Dann drehen wir sie um, wir drehen sie im Uhrzeigersinn um.</ta>
            <ta e="T760" id="Seg_10433" s="T755">Man muss sie gut festhalten, damit man sie nicht loslässt.</ta>
            <ta e="T766" id="Seg_10434" s="T761">Wir reichen sie genauso einander herum.</ta>
            <ta e="T779" id="Seg_10435" s="T767">Ich gebe sie meiner Freundin, meine Freundin zur nächsten Freundin, so gebend reichen wir die Haut herum.</ta>
            <ta e="T785" id="Seg_10436" s="T780">Von unten kommt dicker Rauch.</ta>
            <ta e="T792" id="Seg_10437" s="T786">Uns ist auch vom kommenden Rauch schlecht.</ta>
            <ta e="T798" id="Seg_10438" s="T793">Unsere Augen brennen vom Rauch.</ta>
            <ta e="T804" id="Seg_10439" s="T799">Wir nehmen den Rauch in den Mund.</ta>
            <ta e="T817" id="Seg_10440" s="T805">Dennoch müssen wir stehen: "steht, steht", sagen sie zu uns.</ta>
            <ta e="T822" id="Seg_10441" s="T818">So geben wir sie herum.</ta>
            <ta e="T849" id="Seg_10442" s="T823">Dann lässt ein Mädchen die Haut aus ihrer Hand rutschen, sie greift vorbei, die Stelle, wo sie gehalten hatte, die Haut, dann sagen sie: "Nun, du wirst nicht heiraten, wenn du erwachsen bist."</ta>
            <ta e="T856" id="Seg_10443" s="T850">So sprechen sie mit uns.</ta>
            <ta e="T867" id="Seg_10444" s="T857">Deshalb stehen wir unglücklich dort, ohne die Haut loszulassen, so.</ta>
         </annotation>
         <annotation name="fr" tierref="fr" />
         <annotation name="ltr" tierref="ltr">
            <ta e="T15" id="Seg_10445" s="T1">Кочуя, всю зиму прокочевав, у долган бывает место, на котором они весной останавливаются</ta>
            <ta e="T24" id="Seg_10446" s="T16">Там много чумов, балков стоят.</ta>
            <ta e="T35" id="Seg_10447" s="T25">Весенние, с лёгкой прохладой дни, когда светит солнце, ну, у женщин прибавляется работы.</ta>
            <ta e="T43" id="Seg_10448" s="T36">На улице они вначале вот работу со шкурой делают</ta>
            <ta e="T50" id="Seg_10449" s="T44">Шкура, шкуру тщательно очищают в начале</ta>
            <ta e="T57" id="Seg_10450" s="T51">Очистив, ножницами стригут ворс</ta>
            <ta e="T67" id="Seg_10451" s="T58">После того, как закончат стричь ножницами, потом после - ножиком тоже режут.</ta>
            <ta e="T76" id="Seg_10452" s="T68">Нюки … долганы … сильно толстую шкуру не берут.</ta>
            <ta e="T93" id="Seg_10453" s="T77">У обстриженного, у которого ворс убрали, берут обстриженную шкуру, что чум накрывают, это нюки шьют</ta>
            <ta e="T102" id="Seg_10454" s="T94">Там, значит, немного ворс оставляют</ta>
            <ta e="T107" id="Seg_10455" s="T103">Этим самым шьют нюк</ta>
            <ta e="T116" id="Seg_10456" s="T108">Это …, из некоторых шкур они должны сделать ровдуги.</ta>
            <ta e="T124" id="Seg_10457" s="T117">Для изготовления ровдуги здесь тоже много работы:</ta>
            <ta e="T141" id="Seg_10458" s="T125">Стригут, как тогда стригли, чтобы шерсти (ворса) не было, чтобы шерсти мало было, всё равно ведь шерсть остаётся на шкуре</ta>
            <ta e="T163" id="Seg_10459" s="T142">Эта шерсть чтобы хорошо слезла, потом для обработки даже хорошо – чтобы замшей стала, эту ворсистую шкуру они оставляют киснуть в воде.</ta>
            <ta e="T169" id="Seg_10460" s="T164">В воду кладут мыло,</ta>
            <ta e="T173" id="Seg_10461" s="T170">Потом добавляют разные-разные травы.</ta>
            <ta e="T181" id="Seg_10462" s="T174">Делая это, несколько дней должна лежать.</ta>
            <ta e="T197" id="Seg_10463" s="T182">Пусть и жаркий день будет – вода же прокиснет …, шкура тоже киснет, размякнет.</ta>
            <ta e="T207" id="Seg_10464" s="T198">После того, как шкура за несколько дней прокиснет, они размешивают</ta>
            <ta e="T217" id="Seg_10465" s="T208">Затем вытащив эту шкуру, это промывают, пройдя к реке.</ta>
            <ta e="T224" id="Seg_10466" s="T218">Хорошенько прополоскали, затем сушат</ta>
            <ta e="T240" id="Seg_10467" s="T225">После того, как просушили эту шкуру – с помощью скребка (гэдэрээ) выскабливают оставшийся ворс</ta>
            <ta e="T257" id="Seg_10468" s="T241">Это тоже не быстро делается, нелегкий процесс. Это очень трудная работа даже для женщины.</ta>
            <ta e="T270" id="Seg_10469" s="T258">Поэтому за один раз они всю эту работу не завершат – трудная работа</ta>
            <ta e="T279" id="Seg_10470" s="T271">На следующий день то же самое будут проделывать: скоблить шкуру с помощью гэрэрээ</ta>
            <ta e="T292" id="Seg_10471" s="T280">После того, как закончат скоблить, остается безворсная замша (кожа)</ta>
            <ta e="T304" id="Seg_10472" s="T293">Чтобы замша (т.е.ровдуга) была ровной, скоблят скребком поменьше (с помощью кы4ыак),</ta>
            <ta e="T311" id="Seg_10473" s="T305">Даже с помощью ножа разравнивают.</ta>
            <ta e="T319" id="Seg_10474" s="T312">Всё плохое убирают</ta>
            <ta e="T329" id="Seg_10475" s="T320">И эта ровдуга, если посмотреть, становится очень хорошей</ta>
            <ta e="T343" id="Seg_10476" s="T330">И чтобы замша была качественной и темноватой за счёт дыма, её тоже размягчают</ta>
            <ta e="T353" id="Seg_10477" s="T344">Прежде чем как размягчить, почему-то звали девочек.</ta>
            <ta e="T365" id="Seg_10478" s="T354">Накидывают замшу на вешала</ta>
            <ta e="T370" id="Seg_10479" s="T366">Её надо придавить</ta>
            <ta e="T385" id="Seg_10480" s="T371">Её надо придавить санками(обратной стороной), например, и надо держать, придавливая</ta>
            <ta e="T390" id="Seg_10481" s="T386">Когда стоишь</ta>
            <ta e="T402" id="Seg_10482" s="T391">Когда стоишь, придавливая, тебе дают варёную оленью печёнку.</ta>
            <ta e="T411" id="Seg_10483" s="T402">Эту печень девочка, которая стоит и придавливает кожу, должна разжевать.</ta>
            <ta e="T417" id="Seg_10484" s="T411">«Жуй», - говорят. Ну, куда тут денешься?!</ta>
            <ta e="T427" id="Seg_10485" s="T418">Нехотя, потому что у варёной печени вкус нехороший – всё равно стоишь,</ta>
            <ta e="T432" id="Seg_10486" s="T428">Маму свою слушаешься</ta>
            <ta e="T442" id="Seg_10487" s="T433">Стоишь и жуёшь печень</ta>
            <ta e="T461" id="Seg_10488" s="T443">Они, взяв эту разжёванную печень, эту соскобленную замшу обмазывают.</ta>
            <ta e="T469" id="Seg_10489" s="T462">Затем размазывают это по шкуре с помощью 4о8о4оо или рук</ta>
            <ta e="T490" id="Seg_10490" s="T470">Одной рукой придерживают кожу, другой держат 4о8о4оо, с помощью которого сверху вниз обрабатывают шкуру.</ta>
            <ta e="T503" id="Seg_10491" s="T491">А девочки не очень-то хотя стоять и держать,</ta>
            <ta e="T514" id="Seg_10492" s="T504">Потому что долго надо стоять</ta>
            <ta e="T522" id="Seg_10493" s="T515">А жевать печень – это очень плохо.</ta>
            <ta e="T532" id="Seg_10494" s="T523">Когда жуёшь, аж тошнит иногда</ta>
            <ta e="T543" id="Seg_10495" s="T533">Поэтому девочки убегали, чтобы не жевать, уходили и всё…</ta>
            <ta e="T553" id="Seg_10496" s="T544">В основном девочек для этого привлекали, больше чем мальчиков почему-то.</ta>
            <ta e="T570" id="Seg_10497" s="T554">Ну, после того, как сделали это, хорошенько обрабатывают с помощью 4о8о4оо шкуру</ta>
            <ta e="T586" id="Seg_10498" s="T578">На следующий день ты снова должен стоять и жевать.</ta>
            <ta e="T592" id="Seg_10499" s="T587">Не хочется - тошнит</ta>
            <ta e="T604" id="Seg_10500" s="T593">Ну, всё равно жуёшь – что поделаешь, маму слушаешься.</ta>
            <ta e="T616" id="Seg_10501" s="T605">Ну, стоишь и жуёшь, мучаешься, затем заканчиваешь.</ta>
            <ta e="T625" id="Seg_10502" s="T617">Ну, в какой-нибудь из дней скребками скребут</ta>
            <ta e="T645" id="Seg_10503" s="T626">Потом, чтобы получилась качественная, мягкая замша, по-всякому и с помощью гэдэрээн выделываем</ta>
            <ta e="T649" id="Seg_10504" s="T646">По-всякому и с помощью гэдэрээн выделываем</ta>
            <ta e="T658" id="Seg_10505" s="T650">Замша ведь слишком ?(ма8ан)тонкой тоже не должна быть, это плохо</ta>
            <ta e="T667" id="Seg_10506" s="T659">Поэтому замшу натягивают над дымом</ta>
            <ta e="T675" id="Seg_10507" s="T668">Берут замшу и вешают внутри дома, то есть чума.</ta>
            <ta e="T687" id="Seg_10508" s="T676">Берут замшу и вешают внутри дома, то есть чума.</ta>
            <ta e="T694" id="Seg_10509" s="T688">Она должна пропитаться дымом</ta>
            <ta e="T701" id="Seg_10510" s="T695">А некоторые женщины делают по-другому.</ta>
            <ta e="T709" id="Seg_10511" s="T702">Могли бы также пропитать дымом</ta>
            <ta e="T724" id="Seg_10512" s="T710">А они нас расставляют, троих-четверых девочек.</ta>
            <ta e="T725" id="Seg_10513" s="T724">Это… огонь, очаг разводят. </ta>
            <ta e="T734" id="Seg_10514" s="T725">Как дрова прогорят, на них накидывают ветки, чтобы сильный дым шёл.</ta>
            <ta e="T745" id="Seg_10515" s="T735">Ну, стоя вокруг этого дыма, держим это</ta>
            <ta e="T754" id="Seg_10516" s="T746">Потом поворачиваем по ходу солнца</ta>
            <ta e="T760" id="Seg_10517" s="T755">Ты должен хорошо держать это, чтобы не выпустить из рук</ta>
            <ta e="T766" id="Seg_10518" s="T761">Друг к другу поворачиваем:</ta>
            <ta e="T779" id="Seg_10519" s="T767">Я подруге даю, подруга – другой девочкеб Таким образом, передавая друг другу, подхватываем по кругу эту замшу</ta>
            <ta e="T785" id="Seg_10520" s="T780">А снизу ещё больше дыма поступает</ta>
            <ta e="T792" id="Seg_10521" s="T786">А нам тоже плохо от этого дыма</ta>
            <ta e="T798" id="Seg_10522" s="T793">Глаза щиплет от этого дыма,</ta>
            <ta e="T804" id="Seg_10523" s="T799">В рот проникает этот дым</ta>
            <ta e="T817" id="Seg_10524" s="T805">Тем не менее, стоим. «Стойте, стойте», - нам говорят</ta>
            <ta e="T822" id="Seg_10525" s="T818">Ну, так поворачиваем</ta>
            <ta e="T849" id="Seg_10526" s="T823">И потом, если какая-нибудь девочка, схватив мимо, выпустит край шкуры, за который она держала, ну, Тогда говорят ей: «Ну, замуж не выйдешь, когда вырастешь». …</ta>
            <ta e="T856" id="Seg_10527" s="T850">Так ругают нас.</ta>
            <ta e="T867" id="Seg_10528" s="T857">Поэтому стоим и мучаемся, не отпускаем замшу туда дальше. Ну.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T694" id="Seg_10529" s="T688">[DCh]: I'm not sure whether "ɨksa" is a noun here, and not a verb in converb form.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T0" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
