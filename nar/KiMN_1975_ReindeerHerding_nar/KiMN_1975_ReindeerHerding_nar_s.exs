<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDE4CF8FC0-245C-CF5D-2CCC-22E041440065">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>KiMN_1975_ReindeerBirth_nar</transcription-name>
         <referenced-file url="KiMN_1975_ReindeerHerding_nar.wav" />
         <referenced-file url="KiMN_1975_ReindeerHerding_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\KiMN_1975_ReindeerHerding_nar\KiMN_1975_ReindeerHerding_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1172</ud-information>
            <ud-information attribute-name="# HIAT:w">923</ud-information>
            <ud-information attribute-name="# e">926</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">3</ud-information>
            <ud-information attribute-name="# HIAT:u">111</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KiMN">
            <abbreviation>KiMN</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="0.586" type="appl" />
         <tli id="T2" time="1.172" type="appl" />
         <tli id="T3" time="1.758" type="appl" />
         <tli id="T4" time="2.344" type="appl" />
         <tli id="T5" time="2.93" type="appl" />
         <tli id="T6" time="3.413" type="appl" />
         <tli id="T7" time="3.896" type="appl" />
         <tli id="T8" time="4.379" type="appl" />
         <tli id="T9" time="4.862" type="appl" />
         <tli id="T10" time="5.345" type="appl" />
         <tli id="T11" time="6.766631048195773" />
         <tli id="T12" time="6.875815524097886" type="intp" />
         <tli id="T13" time="6.985" type="appl" />
         <tli id="T14" time="7.564" type="appl" />
         <tli id="T15" time="8.143" type="appl" />
         <tli id="T16" time="8.721" type="appl" />
         <tli id="T17" time="9.446665769043749" />
         <tli id="T18" time="9.79" type="appl" />
         <tli id="T19" time="10.28" type="appl" />
         <tli id="T20" time="10.77" type="appl" />
         <tli id="T21" time="11.26" type="appl" />
         <tli id="T22" time="11.75" type="appl" />
         <tli id="T23" time="12.24" type="appl" />
         <tli id="T24" time="12.73" type="appl" />
         <tli id="T25" time="13.22" type="appl" />
         <tli id="T26" time="13.71" type="appl" />
         <tli id="T27" time="14.2" type="appl" />
         <tli id="T28" time="14.683334167594863" />
         <tli id="T29" time="15.339" type="appl" />
         <tli id="T30" time="15.989" type="appl" />
         <tli id="T31" time="16.638" type="appl" />
         <tli id="T32" time="17.288" type="appl" />
         <tli id="T33" time="17.937" type="appl" />
         <tli id="T34" time="18.587" type="appl" />
         <tli id="T35" time="19.176000948598126" />
         <tli id="T36" time="20.348" type="appl" />
         <tli id="T37" time="21.49333250053297" />
         <tli id="T38" time="22.123" type="appl" />
         <tli id="T39" time="22.785" type="appl" />
         <tli id="T40" time="23.447" type="appl" />
         <tli id="T41" time="23.943334252550454" />
         <tli id="T42" time="24.599" type="appl" />
         <tli id="T43" time="25.088" type="appl" />
         <tli id="T44" time="25.577" type="appl" />
         <tli id="T45" time="26.066" type="appl" />
         <tli id="T46" time="26.737" type="appl" />
         <tli id="T47" time="27.408" type="appl" />
         <tli id="T48" time="28.079" type="appl" />
         <tli id="T49" time="28.75" type="appl" />
         <tli id="T50" time="29.422" type="appl" />
         <tli id="T51" time="30.093" type="appl" />
         <tli id="T52" time="30.764" type="appl" />
         <tli id="T53" time="31.435" type="appl" />
         <tli id="T54" time="32.106" type="appl" />
         <tli id="T55" time="32.7369989015967" />
         <tli id="T56" time="33.297" type="appl" />
         <tli id="T57" time="33.818" type="appl" />
         <tli id="T58" time="34.338" type="appl" />
         <tli id="T59" time="34.858" type="appl" />
         <tli id="T60" time="35.379" type="appl" />
         <tli id="T61" time="35.899" type="appl" />
         <tli id="T62" time="36.42" type="appl" />
         <tli id="T63" time="36.813334866286745" />
         <tli id="T64" time="37.492" type="appl" />
         <tli id="T65" time="38.044" type="appl" />
         <tli id="T66" time="38.596" type="appl" />
         <tli id="T67" time="39.148" type="appl" />
         <tli id="T68" time="39.7" type="appl" />
         <tli id="T69" time="40.252" type="appl" />
         <tli id="T70" time="41.57978113063747" />
         <tli id="T71" time="41.652" type="appl" />
         <tli id="T72" time="42.5" type="appl" />
         <tli id="T73" time="43.349" type="appl" />
         <tli id="T74" time="44.197" type="appl" />
         <tli id="T75" time="45.045" type="appl" />
         <tli id="T76" time="45.893" type="appl" />
         <tli id="T77" time="46.742" type="appl" />
         <tli id="T78" time="47.59" type="appl" />
         <tli id="T79" time="48.438" type="appl" />
         <tli id="T80" time="48.947" type="appl" />
         <tli id="T81" time="49.456" type="appl" />
         <tli id="T82" time="49.965" type="appl" />
         <tli id="T83" time="50.474" type="appl" />
         <tli id="T84" time="50.984" type="appl" />
         <tli id="T85" time="51.493" type="appl" />
         <tli id="T86" time="52.002" type="appl" />
         <tli id="T87" time="52.511" type="appl" />
         <tli id="T88" time="53.199999911568455" />
         <tli id="T89" time="53.685" type="appl" />
         <tli id="T90" time="54.349" type="appl" />
         <tli id="T91" time="55.014" type="appl" />
         <tli id="T92" time="55.678" type="appl" />
         <tli id="T93" time="56.343" type="appl" />
         <tli id="T94" time="56.945" type="appl" />
         <tli id="T95" time="57.547" type="appl" />
         <tli id="T96" time="58.15" type="appl" />
         <tli id="T97" time="58.752" type="appl" />
         <tli id="T98" time="59.354" type="appl" />
         <tli id="T99" time="59.956" type="appl" />
         <tli id="T100" time="60.558" type="appl" />
         <tli id="T101" time="61.16" type="appl" />
         <tli id="T102" time="61.763" type="appl" />
         <tli id="T103" time="62.365" type="appl" />
         <tli id="T104" time="62.98699917560909" />
         <tli id="T105" time="63.619" type="appl" />
         <tli id="T106" time="64.271" type="appl" />
         <tli id="T107" time="64.923" type="appl" />
         <tli id="T108" time="65.576" type="appl" />
         <tli id="T109" time="66.228" type="appl" />
         <tli id="T110" time="66.88" type="appl" />
         <tli id="T111" time="67.374" type="appl" />
         <tli id="T112" time="67.868" type="appl" />
         <tli id="T113" time="68.362" type="appl" />
         <tli id="T114" time="68.856" type="appl" />
         <tli id="T115" time="69.35" type="appl" />
         <tli id="T116" time="69.985" type="appl" />
         <tli id="T117" time="70.621" type="appl" />
         <tli id="T118" time="71.256" type="appl" />
         <tli id="T119" time="71.891" type="appl" />
         <tli id="T120" time="72.526" type="appl" />
         <tli id="T121" time="73.162" type="appl" />
         <tli id="T122" time="73.797" type="appl" />
         <tli id="T123" time="74.432" type="appl" />
         <tli id="T124" time="75.067" type="appl" />
         <tli id="T125" time="75.703" type="appl" />
         <tli id="T126" time="76.338" type="appl" />
         <tli id="T127" time="76.77" type="appl" />
         <tli id="T128" time="77.203" type="appl" />
         <tli id="T129" time="77.635" type="appl" />
         <tli id="T130" time="78.067" type="appl" />
         <tli id="T131" time="78.5" type="appl" />
         <tli id="T132" time="78.932" type="appl" />
         <tli id="T133" time="79.364" type="appl" />
         <tli id="T134" time="79.797" type="appl" />
         <tli id="T135" time="80.229" type="appl" />
         <tli id="T136" time="80.754" type="appl" />
         <tli id="T137" time="81.28" type="appl" />
         <tli id="T138" time="81.805" type="appl" />
         <tli id="T139" time="82.54623215641384" />
         <tli id="T140" time="82.962" type="appl" />
         <tli id="T141" time="83.594" type="appl" />
         <tli id="T142" time="84.226" type="appl" />
         <tli id="T143" time="84.858" type="appl" />
         <tli id="T144" time="85.489" type="appl" />
         <tli id="T145" time="86.121" type="appl" />
         <tli id="T146" time="86.753" type="appl" />
         <tli id="T147" time="87.53166815345406" />
         <tli id="T148" time="88.143" type="appl" />
         <tli id="T149" time="88.9" type="appl" />
         <tli id="T150" time="89.658" type="appl" />
         <tli id="T151" time="90.50266813958213" />
         <tli id="T152" time="91.096" type="appl" />
         <tli id="T153" time="91.775" type="appl" />
         <tli id="T154" time="92.454" type="appl" />
         <tli id="T155" time="93.43950814927285" />
         <tli id="T156" time="93.877" type="appl" />
         <tli id="T157" time="94.619" type="appl" />
         <tli id="T158" time="95.362" type="appl" />
         <tli id="T159" time="96.105" type="appl" />
         <tli id="T160" time="96.847" type="appl" />
         <tli id="T161" time="97.41000157214287" />
         <tli id="T162" time="97.955" type="appl" />
         <tli id="T163" time="98.32" type="appl" />
         <tli id="T164" time="98.685" type="appl" />
         <tli id="T165" time="99.02333292357297" />
         <tli id="T166" time="99.61" type="appl" />
         <tli id="T167" time="100.169" type="appl" />
         <tli id="T168" time="100.729" type="appl" />
         <tli id="T169" time="101.288" type="appl" />
         <tli id="T170" time="101.848" type="appl" />
         <tli id="T171" time="102.408" type="appl" />
         <tli id="T172" time="102.967" type="appl" />
         <tli id="T173" time="103.527" type="appl" />
         <tli id="T174" time="104.087" type="appl" />
         <tli id="T175" time="104.646" type="appl" />
         <tli id="T176" time="105.206" type="appl" />
         <tli id="T177" time="105.765" type="appl" />
         <tli id="T178" time="106.2383340074534" />
         <tli id="T179" time="106.765" type="appl" />
         <tli id="T180" time="107.205" type="appl" />
         <tli id="T181" time="107.645" type="appl" />
         <tli id="T182" time="108.086" type="appl" />
         <tli id="T183" time="108.526" type="appl" />
         <tli id="T184" time="108.966" type="appl" />
         <tli id="T185" time="109.406" type="appl" />
         <tli id="T186" time="109.846" type="appl" />
         <tli id="T187" time="110.286" type="appl" />
         <tli id="T188" time="110.727" type="appl" />
         <tli id="T189" time="111.167" type="appl" />
         <tli id="T190" time="111.607" type="appl" />
         <tli id="T191" time="112.047" type="appl" />
         <tli id="T192" time="112.487" type="appl" />
         <tli id="T193" time="112.927" type="appl" />
         <tli id="T194" time="113.368" type="appl" />
         <tli id="T195" time="113.808" type="appl" />
         <tli id="T196" time="114.248" type="appl" />
         <tli id="T197" time="115.29272645073664" />
         <tli id="T198" time="115.558" type="appl" />
         <tli id="T199" time="116.427" type="appl" />
         <tli id="T200" time="117.297" type="appl" />
         <tli id="T201" time="118.166" type="appl" />
         <tli id="T202" time="119.036" type="appl" />
         <tli id="T203" time="119.905" type="appl" />
         <tli id="T204" time="120.88166838560082" />
         <tli id="T205" time="121.418" type="appl" />
         <tli id="T206" time="122.061" type="appl" />
         <tli id="T207" time="122.704" type="appl" />
         <tli id="T208" time="123.347" type="appl" />
         <tli id="T209" time="123.99" type="appl" />
         <tli id="T210" time="124.633" type="appl" />
         <tli id="T211" time="125.276" type="appl" />
         <tli id="T212" time="125.919" type="appl" />
         <tli id="T213" time="126.562" type="appl" />
         <tli id="T214" time="127.205" type="appl" />
         <tli id="T215" time="127.848" type="appl" />
         <tli id="T216" time="128.491" type="appl" />
         <tli id="T217" time="129.134" type="appl" />
         <tli id="T218" time="129.777" type="appl" />
         <tli id="T219" time="130.42" type="appl" />
         <tli id="T220" time="131.063" type="appl" />
         <tli id="T221" time="131.674" type="appl" />
         <tli id="T222" time="132.285" type="appl" />
         <tli id="T223" time="132.896" type="appl" />
         <tli id="T224" time="133.508" type="appl" />
         <tli id="T225" time="134.119" type="appl" />
         <tli id="T226" time="134.60999455937272" />
         <tli id="T227" time="135.138" type="appl" />
         <tli id="T228" time="135.546" type="appl" />
         <tli id="T229" time="135.955" type="appl" />
         <tli id="T230" time="136.363" type="appl" />
         <tli id="T231" time="136.871" type="appl" />
         <tli id="T232" time="137.378" type="appl" />
         <tli id="T233" time="137.886" type="appl" />
         <tli id="T234" time="138.394" type="appl" />
         <tli id="T235" time="138.901" type="appl" />
         <tli id="T236" time="139.409" type="appl" />
         <tli id="T237" time="139.917" type="appl" />
         <tli id="T238" time="140.425" type="appl" />
         <tli id="T239" time="140.932" type="appl" />
         <tli id="T240" time="141.44" type="appl" />
         <tli id="T241" time="141.948" type="appl" />
         <tli id="T242" time="142.455" type="appl" />
         <tli id="T243" time="143.00299725493983" />
         <tli id="T244" time="144.993" type="appl" />
         <tli id="T245" time="146.9963356096411" />
         <tli id="T246" time="147.613" type="appl" />
         <tli id="T247" time="148.203" type="appl" />
         <tli id="T248" time="148.794" type="appl" />
         <tli id="T249" time="149.384" type="appl" />
         <tli id="T250" time="149.974" type="appl" />
         <tli id="T251" time="150.564" type="appl" />
         <tli id="T252" time="151.154" type="appl" />
         <tli id="T253" time="151.745" type="appl" />
         <tli id="T254" time="152.335" type="appl" />
         <tli id="T255" time="152.925" type="appl" />
         <tli id="T256" time="153.45" type="appl" />
         <tli id="T257" time="153.975" type="appl" />
         <tli id="T258" time="154.5" type="appl" />
         <tli id="T259" time="155.025" type="appl" />
         <tli id="T260" time="155.55" type="appl" />
         <tli id="T261" time="156.075" type="appl" />
         <tli id="T262" time="156.6" type="appl" />
         <tli id="T263" time="157.125" type="appl" />
         <tli id="T264" time="157.65" type="appl" />
         <tli id="T265" time="158.175" type="appl" />
         <tli id="T266" time="158.65333154073303" />
         <tli id="T267" time="159.167" type="appl" />
         <tli id="T268" time="159.633" type="appl" />
         <tli id="T269" time="160.1" type="appl" />
         <tli id="T270" time="160.567" type="appl" />
         <tli id="T271" time="161.033" type="appl" />
         <tli id="T272" time="161.5" type="appl" />
         <tli id="T273" time="161.967" type="appl" />
         <tli id="T274" time="162.433" type="appl" />
         <tli id="T275" time="162.9" type="appl" />
         <tli id="T276" time="163.367" type="appl" />
         <tli id="T277" time="163.833" type="appl" />
         <tli id="T278" time="164.3" type="appl" />
         <tli id="T279" time="164.767" type="appl" />
         <tli id="T280" time="165.233" type="appl" />
         <tli id="T281" time="165.7" type="appl" />
         <tli id="T282" time="166.167" type="appl" />
         <tli id="T283" time="166.633" type="appl" />
         <tli id="T284" time="167.0933391972068" />
         <tli id="T285" time="167.677" type="appl" />
         <tli id="T286" time="168.254" type="appl" />
         <tli id="T287" time="168.831" type="appl" />
         <tli id="T288" time="169.408" type="appl" />
         <tli id="T289" time="169.986" type="appl" />
         <tli id="T290" time="170.563" type="appl" />
         <tli id="T291" time="171.14" type="appl" />
         <tli id="T292" time="171.717" type="appl" />
         <tli id="T293" time="172.3340017153585" />
         <tli id="T294" time="172.755" type="appl" />
         <tli id="T295" time="173.216" type="appl" />
         <tli id="T296" time="173.677" type="appl" />
         <tli id="T297" time="174.137" type="appl" />
         <tli id="T298" time="174.598" type="appl" />
         <tli id="T299" time="175.059" type="appl" />
         <tli id="T300" time="175.46000088559552" />
         <tli id="T301" time="176.315" type="appl" />
         <tli id="T302" time="177.11" type="appl" />
         <tli id="T303" time="177.905" type="appl" />
         <tli id="T304" time="178.7" type="appl" />
         <tli id="T305" time="179.495" type="appl" />
         <tli id="T306" time="180.29" type="appl" />
         <tli id="T307" time="181.085" type="appl" />
         <tli id="T308" time="181.88" type="appl" />
         <tli id="T309" time="182.675" type="appl" />
         <tli id="T310" time="183.47" type="appl" />
         <tli id="T311" time="184.265" type="appl" />
         <tli id="T312" time="185.06" type="appl" />
         <tli id="T313" time="185.855" type="appl" />
         <tli id="T314" time="187.1366581715265" />
         <tli id="T315" time="187.21813636363638" type="intp" />
         <tli id="T316" time="187.78627272727275" type="intp" />
         <tli id="T317" time="188.35440909090912" type="intp" />
         <tli id="T318" time="188.92254545454549" type="intp" />
         <tli id="T319" time="189.49068181818186" type="intp" />
         <tli id="T320" time="190.05881818181822" type="intp" />
         <tli id="T321" time="190.6269545454546" type="intp" />
         <tli id="T322" time="191.19509090909096" type="intp" />
         <tli id="T323" time="192.20322004361634" />
         <tli id="T324" time="192.3313636363637" type="intp" />
         <tli id="T325" time="192.89950000000007" type="intp" />
         <tli id="T326" time="193.46763636363644" type="intp" />
         <tli id="T327" time="194.03577272727279" type="intp" />
         <tli id="T328" time="194.60390909090916" type="intp" />
         <tli id="T329" time="195.17204545454553" type="intp" />
         <tli id="T330" time="195.74018181818187" type="intp" />
         <tli id="T331" time="196.3083181818182" type="intp" />
         <tli id="T332" time="196.87645454545458" type="intp" />
         <tli id="T333" time="197.44459090909095" type="intp" />
         <tli id="T334" time="198.0127272727273" type="intp" />
         <tli id="T335" time="198.5875353959279" />
         <tli id="T336" time="199.149" type="appl" />
         <tli id="T337" time="199.809" type="appl" />
         <tli id="T338" time="200.47" type="appl" />
         <tli id="T339" time="201.131" type="appl" />
         <tli id="T340" time="201.792" type="appl" />
         <tli id="T341" time="202.452" type="appl" />
         <tli id="T342" time="202.9196740505715" />
         <tli id="T343" time="203.766" type="appl" />
         <tli id="T344" time="204.419" type="appl" />
         <tli id="T345" time="205.072" type="appl" />
         <tli id="T346" time="205.725" type="appl" />
         <tli id="T347" time="206.378" type="appl" />
         <tli id="T348" time="207.031" type="appl" />
         <tli id="T349" time="207.684" type="appl" />
         <tli id="T350" time="208.337" type="appl" />
         <tli id="T351" time="208.99" type="appl" />
         <tli id="T352" time="209.643" type="appl" />
         <tli id="T353" time="210.296" type="appl" />
         <tli id="T354" time="210.949" type="appl" />
         <tli id="T355" time="211.602" type="appl" />
         <tli id="T356" time="212.254" type="appl" />
         <tli id="T357" time="212.907" type="appl" />
         <tli id="T358" time="213.56" type="appl" />
         <tli id="T359" time="214.213" type="appl" />
         <tli id="T360" time="214.866" type="appl" />
         <tli id="T361" time="215.519" type="appl" />
         <tli id="T362" time="216.172" type="appl" />
         <tli id="T363" time="216.825" type="appl" />
         <tli id="T364" time="217.478" type="appl" />
         <tli id="T365" time="218.131" type="appl" />
         <tli id="T366" time="218.784" type="appl" />
         <tli id="T367" time="219.437" type="appl" />
         <tli id="T368" time="219.83884280325495" />
         <tli id="T369" time="220.567" type="appl" />
         <tli id="T370" time="221.044" type="appl" />
         <tli id="T371" time="221.52" type="appl" />
         <tli id="T372" time="221.997" type="appl" />
         <tli id="T373" time="222.474" type="appl" />
         <tli id="T374" time="223.08432051124254" />
         <tli id="T375" time="223.816" type="appl" />
         <tli id="T376" time="224.681" type="appl" />
         <tli id="T377" time="225.547" type="appl" />
         <tli id="T378" time="226.412" type="appl" />
         <tli id="T379" time="227.277" type="appl" />
         <tli id="T380" time="228.142" type="appl" />
         <tli id="T381" time="229.008" type="appl" />
         <tli id="T382" time="229.873" type="appl" />
         <tli id="T383" time="230.83133702384498" />
         <tli id="T384" time="231.732" type="appl" />
         <tli id="T385" time="232.726" type="appl" />
         <tli id="T386" time="233.719" type="appl" />
         <tli id="T387" time="234.713" type="appl" />
         <tli id="T388" time="235.707" type="appl" />
         <tli id="T389" time="236.7" type="appl" />
         <tli id="T390" time="237.694" type="appl" />
         <tli id="T391" time="238.688" type="appl" />
         <tli id="T392" time="239.682" type="appl" />
         <tli id="T393" time="240.676" type="appl" />
         <tli id="T394" time="241.669" type="appl" />
         <tli id="T395" time="242.61632707286284" />
         <tli id="T396" time="243.52" type="appl" />
         <tli id="T397" time="244.377" type="appl" />
         <tli id="T398" time="245.233" type="appl" />
         <tli id="T399" time="246.023340388886" />
         <tli id="T400" time="246.754" type="appl" />
         <tli id="T401" time="247.418" type="appl" />
         <tli id="T402" time="248.082" type="appl" />
         <tli id="T403" time="248.746" type="appl" />
         <tli id="T404" time="249.41" type="appl" />
         <tli id="T405" time="250.074" type="appl" />
         <tli id="T406" time="250.739" type="appl" />
         <tli id="T407" time="251.403" type="appl" />
         <tli id="T408" time="252.067" type="appl" />
         <tli id="T409" time="252.731" type="appl" />
         <tli id="T410" time="253.395" type="appl" />
         <tli id="T411" time="254.51233216251518" />
         <tli id="T412" time="254.69" type="appl" />
         <tli id="T413" time="255.322" type="appl" />
         <tli id="T414" time="255.954" type="appl" />
         <tli id="T415" time="256.9986471999478" />
         <tli id="T416" time="257.023" type="appl" />
         <tli id="T417" time="257.461" type="appl" />
         <tli id="T418" time="257.9" type="appl" />
         <tli id="T419" time="258.338" type="appl" />
         <tli id="T420" time="258.776" type="appl" />
         <tli id="T421" time="259.214" type="appl" />
         <tli id="T422" time="259.652" type="appl" />
         <tli id="T423" time="260.09" type="appl" />
         <tli id="T424" time="260.528" type="appl" />
         <tli id="T425" time="260.967" type="appl" />
         <tli id="T426" time="261.405" type="appl" />
         <tli id="T427" time="261.843" type="appl" />
         <tli id="T428" time="262.281" type="appl" />
         <tli id="T429" time="262.719" type="appl" />
         <tli id="T430" time="263.157" type="appl" />
         <tli id="T431" time="263.596" type="appl" />
         <tli id="T432" time="264.034" type="appl" />
         <tli id="T433" time="264.5719927517266" />
         <tli id="T434" time="265.651" type="appl" />
         <tli id="T435" time="267.3652592984152" />
         <tli id="T436" time="267.393" type="appl" />
         <tli id="T437" time="267.955" type="appl" />
         <tli id="T438" time="268.518" type="appl" />
         <tli id="T439" time="269.08" type="appl" />
         <tli id="T440" time="269.6763408832489" />
         <tli id="T441" time="270.106" type="appl" />
         <tli id="T442" time="270.569" type="appl" />
         <tli id="T443" time="271.033" type="appl" />
         <tli id="T444" time="271.496" type="appl" />
         <tli id="T445" time="271.959" type="appl" />
         <tli id="T446" time="272.422" type="appl" />
         <tli id="T447" time="272.886" type="appl" />
         <tli id="T448" time="273.349" type="appl" />
         <tli id="T449" time="273.812" type="appl" />
         <tli id="T450" time="274.275" type="appl" />
         <tli id="T451" time="274.739" type="appl" />
         <tli id="T452" time="275.202" type="appl" />
         <tli id="T453" time="275.665" type="appl" />
         <tli id="T454" time="276.128" type="appl" />
         <tli id="T455" time="276.592" type="appl" />
         <tli id="T456" time="277.055" type="appl" />
         <tli id="T457" time="282.54517939372727" />
         <tli id="T458" time="283.248" type="appl" />
         <tli id="T459" time="283.926" type="appl" />
         <tli id="T460" time="284.605" type="appl" />
         <tli id="T461" time="285.283" type="appl" />
         <tli id="T462" time="285.962" type="appl" />
         <tli id="T463" time="286.5133355893168" />
         <tli id="T464" time="287.087" type="appl" />
         <tli id="T465" time="287.535" type="appl" />
         <tli id="T466" time="287.982" type="appl" />
         <tli id="T467" time="288.429" type="appl" />
         <tli id="T468" time="288.876" type="appl" />
         <tli id="T469" time="289.324" type="appl" />
         <tli id="T470" time="289.771" type="appl" />
         <tli id="T471" time="290.218" type="appl" />
         <tli id="T472" time="290.665" type="appl" />
         <tli id="T473" time="291.113" type="appl" />
         <tli id="T474" time="291.3800026802877" />
         <tli id="T475" time="292.251" type="appl" />
         <tli id="T476" time="292.942" type="appl" />
         <tli id="T477" time="293.633" type="appl" />
         <tli id="T478" time="294.324" type="appl" />
         <tli id="T479" time="295.015" type="appl" />
         <tli id="T480" time="295.706" type="appl" />
         <tli id="T481" time="296.397" type="appl" />
         <tli id="T482" time="297.4146584148186" />
         <tli id="T483" time="297.954" type="appl" />
         <tli id="T484" time="298.97899913871083" />
         <tli id="T485" time="299.309" type="appl" />
         <tli id="T486" time="299.798" type="appl" />
         <tli id="T487" time="300.288" type="appl" />
         <tli id="T488" time="300.777" type="appl" />
         <tli id="T489" time="301.267" type="appl" />
         <tli id="T490" time="301.756" type="appl" />
         <tli id="T491" time="302.246" type="appl" />
         <tli id="T492" time="302.735" type="appl" />
         <tli id="T493" time="303.35832504496835" />
         <tli id="T494" time="303.955" type="appl" />
         <tli id="T495" time="304.684" type="appl" />
         <tli id="T496" time="305.414" type="appl" />
         <tli id="T497" time="306.144" type="appl" />
         <tli id="T498" time="306.874" type="appl" />
         <tli id="T499" time="307.604" type="appl" />
         <tli id="T500" time="308.333" type="appl" />
         <tli id="T501" time="309.063" type="appl" />
         <tli id="T502" time="309.793" type="appl" />
         <tli id="T503" time="310.523" type="appl" />
         <tli id="T504" time="311.252" type="appl" />
         <tli id="T505" time="312.0220033990751" />
         <tli id="T506" time="312.427" type="appl" />
         <tli id="T507" time="312.873" type="appl" />
         <tli id="T508" time="313.318" type="appl" />
         <tli id="T509" time="313.763" type="appl" />
         <tli id="T510" time="314.209" type="appl" />
         <tli id="T511" time="314.654" type="appl" />
         <tli id="T512" time="315.099" type="appl" />
         <tli id="T513" time="315.545" type="appl" />
         <tli id="T514" time="315.99" type="appl" />
         <tli id="T515" time="316.435" type="appl" />
         <tli id="T516" time="316.881" type="appl" />
         <tli id="T517" time="317.326" type="appl" />
         <tli id="T518" time="317.771" type="appl" />
         <tli id="T519" time="318.217" type="appl" />
         <tli id="T520" time="318.80199375194957" />
         <tli id="T521" time="319.8633846153846" type="intp" />
         <tli id="T522" time="321.0647692307692" type="intp" />
         <tli id="T523" time="322.26615384615377" type="intp" />
         <tli id="T524" time="323.46753846153837" type="intp" />
         <tli id="T525" time="324.66892307692297" type="intp" />
         <tli id="T526" time="325.8703076923076" type="intp" />
         <tli id="T527" time="327.0716923076922" type="intp" />
         <tli id="T528" time="328.2730769230768" type="intp" />
         <tli id="T529" time="329.47446153846147" type="intp" />
         <tli id="T530" time="330.6758461538461" type="intp" />
         <tli id="T531" time="331.8772307692307" type="intp" />
         <tli id="T532" time="333.784909675927" />
         <tli id="T533" time="334.28" type="appl" />
         <tli id="T534" time="334.999" type="appl" />
         <tli id="T535" time="335.719" type="appl" />
         <tli id="T536" time="336.439" type="appl" />
         <tli id="T537" time="337.158" type="appl" />
         <tli id="T538" time="337.878" type="appl" />
         <tli id="T539" time="338.598" type="appl" />
         <tli id="T540" time="339.317" type="appl" />
         <tli id="T541" time="340.037" type="appl" />
         <tli id="T542" time="340.65" type="appl" />
         <tli id="T543" time="341.262" type="appl" />
         <tli id="T544" time="341.875" type="appl" />
         <tli id="T545" time="342.487" type="appl" />
         <tli id="T546" time="343.1" type="appl" />
         <tli id="T547" time="343.712" type="appl" />
         <tli id="T548" time="344.325" type="appl" />
         <tli id="T549" time="344.937" type="appl" />
         <tli id="T550" time="345.55" type="appl" />
         <tli id="T551" time="346.162" type="appl" />
         <tli id="T552" time="346.775" type="appl" />
         <tli id="T553" time="347.387" type="appl" />
         <tli id="T554" time="348.0" type="appl" />
         <tli id="T555" time="348.612" type="appl" />
         <tli id="T556" time="349.331676798548" />
         <tli id="T557" time="349.722" type="appl" />
         <tli id="T558" time="350.219" type="appl" />
         <tli id="T559" time="350.717" type="appl" />
         <tli id="T560" time="351.214" type="appl" />
         <tli id="T561" time="351.711" type="appl" />
         <tli id="T562" time="352.208" type="appl" />
         <tli id="T563" time="352.706" type="appl" />
         <tli id="T564" time="353.203" type="appl" />
         <tli id="T565" time="353.7466535588117" />
         <tli id="T566" time="354.4" type="appl" />
         <tli id="T567" time="355.1" type="appl" />
         <tli id="T568" time="355.8" type="appl" />
         <tli id="T569" time="356.5" type="appl" />
         <tli id="T570" time="357.2" type="appl" />
         <tli id="T571" time="357.9" type="appl" />
         <tli id="T572" time="358.552" type="appl" />
         <tli id="T573" time="359.203" type="appl" />
         <tli id="T574" time="359.855" type="appl" />
         <tli id="T575" time="360.507" type="appl" />
         <tli id="T576" time="361.158" type="appl" />
         <tli id="T577" time="361.64999737353196" />
         <tli id="T578" time="362.33" type="appl" />
         <tli id="T579" time="362.85" type="appl" />
         <tli id="T580" time="363.37" type="appl" />
         <tli id="T581" time="363.89" type="appl" />
         <tli id="T582" time="364.41" type="appl" />
         <tli id="T583" time="364.63667436047217" />
         <tli id="T584" time="365.548" type="appl" />
         <tli id="T585" time="366.166" type="appl" />
         <tli id="T586" time="366.784" type="appl" />
         <tli id="T587" time="367.402" type="appl" />
         <tli id="T588" time="367.9333236741005" />
         <tli id="T589" time="368.435" type="appl" />
         <tli id="T590" time="368.851" type="appl" />
         <tli id="T591" time="369.266" type="appl" />
         <tli id="T592" time="369.681" type="appl" />
         <tli id="T593" time="370.097" type="appl" />
         <tli id="T594" time="370.48534149068416" />
         <tli id="T595" time="371.007" type="appl" />
         <tli id="T596" time="371.501" type="appl" />
         <tli id="T597" time="371.996" type="appl" />
         <tli id="T598" time="372.491" type="appl" />
         <tli id="T599" time="372.985" type="appl" />
         <tli id="T600" time="373.48" type="appl" />
         <tli id="T601" time="373.975" type="appl" />
         <tli id="T602" time="374.469" type="appl" />
         <tli id="T603" time="374.964" type="appl" />
         <tli id="T604" time="375.458" type="appl" />
         <tli id="T605" time="375.953" type="appl" />
         <tli id="T606" time="376.448" type="appl" />
         <tli id="T607" time="376.942" type="appl" />
         <tli id="T608" time="377.46366413290235" />
         <tli id="T609" time="378.249" type="appl" />
         <tli id="T610" time="379.061" type="appl" />
         <tli id="T611" time="379.872" type="appl" />
         <tli id="T612" time="380.684" type="appl" />
         <tli id="T613" time="381.70267827773864" />
         <tli id="T614" time="382.371" type="appl" />
         <tli id="T615" time="383.247" type="appl" />
         <tli id="T616" time="384.122" type="appl" />
         <tli id="T617" time="384.998" type="appl" />
         <tli id="T618" time="385.873" type="appl" />
         <tli id="T619" time="386.749" type="appl" />
         <tli id="T620" time="387.624" type="appl" />
         <tli id="T621" time="388.5" type="appl" />
         <tli id="T622" time="389.3283412624601" />
         <tli id="T623" time="390.046" type="appl" />
         <tli id="T624" time="390.717" type="appl" />
         <tli id="T625" time="391.388" type="appl" />
         <tli id="T626" time="392.058" type="appl" />
         <tli id="T627" time="392.729" type="appl" />
         <tli id="T628" time="393.4200124328864" />
         <tli id="T629" time="393.835" type="appl" />
         <tli id="T630" time="394.271" type="appl" />
         <tli id="T631" time="394.706" type="appl" />
         <tli id="T632" time="395.141" type="appl" />
         <tli id="T633" time="395.576" type="appl" />
         <tli id="T634" time="396.012" type="appl" />
         <tli id="T635" time="396.447" type="appl" />
         <tli id="T636" time="396.882" type="appl" />
         <tli id="T637" time="397.318" type="appl" />
         <tli id="T638" time="397.753" type="appl" />
         <tli id="T639" time="398.188" type="appl" />
         <tli id="T640" time="398.624" type="appl" />
         <tli id="T641" time="399.059" type="appl" />
         <tli id="T642" time="399.494" type="appl" />
         <tli id="T643" time="399.929" type="appl" />
         <tli id="T644" time="400.365" type="appl" />
         <tli id="T645" time="400.76666646959995" />
         <tli id="T646" time="401.509" type="appl" />
         <tli id="T647" time="402.219" type="appl" />
         <tli id="T648" time="402.928" type="appl" />
         <tli id="T649" time="403.637" type="appl" />
         <tli id="T650" time="404.347" type="appl" />
         <tli id="T651" time="405.056" type="appl" />
         <tli id="T652" time="405.765" type="appl" />
         <tli id="T653" time="406.475" type="appl" />
         <tli id="T654" time="407.184" type="appl" />
         <tli id="T655" time="407.893" type="appl" />
         <tli id="T656" time="408.603" type="appl" />
         <tli id="T657" time="409.35201190270544" />
         <tli id="T658" time="409.668" type="appl" />
         <tli id="T659" time="410.024" type="appl" />
         <tli id="T660" time="410.38" type="appl" />
         <tli id="T661" time="410.736" type="appl" />
         <tli id="T662" time="411.092" type="appl" />
         <tli id="T663" time="411.448" type="appl" />
         <tli id="T664" time="411.804" type="appl" />
         <tli id="T665" time="412.16" type="appl" />
         <tli id="T666" time="412.516" type="appl" />
         <tli id="T667" time="412.872" type="appl" />
         <tli id="T668" time="413.7111556235321" />
         <tli id="T669" time="414.309" type="appl" />
         <tli id="T670" time="415.389" type="appl" />
         <tli id="T671" time="416.43666211023225" />
         <tli id="T672" time="417.121" type="appl" />
         <tli id="T673" time="417.772" type="appl" />
         <tli id="T674" time="418.423" type="appl" />
         <tli id="T675" time="419.074" type="appl" />
         <tli id="T676" time="419.7516707022203" />
         <tli id="T677" time="420.216" type="appl" />
         <tli id="T678" time="420.707" type="appl" />
         <tli id="T679" time="421.198" type="appl" />
         <tli id="T680" time="421.689" type="appl" />
         <tli id="T681" time="422.18" type="appl" />
         <tli id="T682" time="422.671" type="appl" />
         <tli id="T683" time="423.162" type="appl" />
         <tli id="T684" time="423.654" type="appl" />
         <tli id="T685" time="424.145" type="appl" />
         <tli id="T686" time="424.636" type="appl" />
         <tli id="T687" time="425.127" type="appl" />
         <tli id="T688" time="425.618" type="appl" />
         <tli id="T689" time="426.109" type="appl" />
         <tli id="T690" time="426.63999381818167" />
         <tli id="T691" time="427.537" type="appl" />
         <tli id="T692" time="428.473" type="appl" />
         <tli id="T693" time="429.2500061211582" />
         <tli id="T694" time="429.812" type="appl" />
         <tli id="T695" time="430.215" type="appl" />
         <tli id="T696" time="430.618" type="appl" />
         <tli id="T697" time="431.02" type="appl" />
         <tli id="T698" time="431.422" type="appl" />
         <tli id="T699" time="431.76499288266746" />
         <tli id="T700" time="432.385" type="appl" />
         <tli id="T701" time="432.945" type="appl" />
         <tli id="T702" time="433.505" type="appl" />
         <tli id="T703" time="434.065" type="appl" />
         <tli id="T704" time="434.625" type="appl" />
         <tli id="T705" time="435.185" type="appl" />
         <tli id="T706" time="435.745" type="appl" />
         <tli id="T707" time="436.305" type="appl" />
         <tli id="T708" time="436.865" type="appl" />
         <tli id="T709" time="437.425" type="appl" />
         <tli id="T710" time="438.31165113043176" />
         <tli id="T711" time="438.417" type="appl" />
         <tli id="T712" time="438.849" type="appl" />
         <tli id="T713" time="440.92434571095583" />
         <tli id="T714" time="442.53767055200353" />
         <tli id="T715" />
         <tli id="T716" />
         <tli id="T717" time="444.17766191934953" />
         <tli id="T718" time="444.239" type="appl" />
         <tli id="T719" time="444.691" type="appl" />
         <tli id="T720" time="445.144" type="appl" />
         <tli id="T721" time="445.596" type="appl" />
         <tli id="T722" time="446.049" type="appl" />
         <tli id="T723" time="446.501" type="appl" />
         <tli id="T724" time="446.954" type="appl" />
         <tli id="T725" time="447.407" type="appl" />
         <tli id="T726" time="447.859" type="appl" />
         <tli id="T727" time="448.312" type="appl" />
         <tli id="T728" time="448.764" type="appl" />
         <tli id="T729" time="449.217" type="appl" />
         <tli id="T730" time="449.669" type="appl" />
         <tli id="T731" time="450.27533815548384" />
         <tli id="T732" time="450.468" type="appl" />
         <tli id="T733" time="450.814" type="appl" />
         <tli id="T734" time="451.159" type="appl" />
         <tli id="T735" time="451.505" type="appl" />
         <tli id="T736" time="451.851" type="appl" />
         <tli id="T737" time="452.197" type="appl" />
         <tli id="T738" time="452.542" type="appl" />
         <tli id="T739" time="452.888" type="appl" />
         <tli id="T740" time="453.234" type="appl" />
         <tli id="T741" time="453.58" type="appl" />
         <tli id="T742" time="453.925" type="appl" />
         <tli id="T743" time="454.271" type="appl" />
         <tli id="T744" time="454.617" type="appl" />
         <tli id="T745" time="454.963" type="appl" />
         <tli id="T746" time="455.308" type="appl" />
         <tli id="T747" time="455.654" type="appl" />
         <tli id="T748" time="456.0" type="appl" />
         <tli id="T749" time="456.604" type="appl" />
         <tli id="T750" time="457.208" type="appl" />
         <tli id="T751" time="457.812" type="appl" />
         <tli id="T752" time="458.416" type="appl" />
         <tli id="T753" time="459.021" type="appl" />
         <tli id="T754" time="459.625" type="appl" />
         <tli id="T755" time="460.229" type="appl" />
         <tli id="T756" time="460.833" type="appl" />
         <tli id="T757" time="461.437" type="appl" />
         <tli id="T758" time="461.981" type="appl" />
         <tli id="T759" time="462.526" type="appl" />
         <tli id="T760" time="463.07" type="appl" />
         <tli id="T761" time="463.614" type="appl" />
         <tli id="T762" time="464.158" type="appl" />
         <tli id="T763" time="464.703" type="appl" />
         <tli id="T764" time="465.247" type="appl" />
         <tli id="T765" time="465.791" type="appl" />
         <tli id="T766" time="466.336" type="appl" />
         <tli id="T767" time="466.71332454520143" />
         <tli id="T768" time="467.235" type="appl" />
         <tli id="T769" time="467.59" type="appl" />
         <tli id="T770" time="467.945" type="appl" />
         <tli id="T771" time="468.1933167547575" />
         <tli id="T772" time="468.778" type="appl" />
         <tli id="T773" time="469.256" type="appl" />
         <tli id="T774" time="469.734" type="appl" />
         <tli id="T775" time="470.212" type="appl" />
         <tli id="T776" time="470.7633553098162" />
         <tli id="T777" time="471.151" type="appl" />
         <tli id="T778" time="471.612" type="appl" />
         <tli id="T779" time="472.072" type="appl" />
         <tli id="T780" time="472.533" type="appl" />
         <tli id="T781" time="472.994" type="appl" />
         <tli id="T782" time="473.455" type="appl" />
         <tli id="T783" time="473.915" type="appl" />
         <tli id="T784" time="474.376" type="appl" />
         <tli id="T785" time="474.837" type="appl" />
         <tli id="T786" time="475.835" type="appl" />
         <tli id="T787" time="476.833" type="appl" />
         <tli id="T788" time="477.832" type="appl" />
         <tli id="T789" time="478.83" type="appl" />
         <tli id="T790" time="479.646" type="appl" />
         <tli id="T791" time="480.4086691217499" />
         <tli id="T792" time="481.203" type="appl" />
         <tli id="T793" time="481.943" type="appl" />
         <tli id="T794" time="482.684" type="appl" />
         <tli id="T795" time="483.424" type="appl" />
         <tli id="T796" time="484.165" type="appl" />
         <tli id="T797" time="484.906" type="appl" />
         <tli id="T798" time="485.646" type="appl" />
         <tli id="T799" time="486.84702064795687" />
         <tli id="T800" time="487.586" type="appl" />
         <tli id="T801" time="488.785" type="appl" />
         <tli id="T802" time="489.426" type="appl" />
         <tli id="T803" time="490.068" type="appl" />
         <tli id="T804" time="490.709" type="appl" />
         <tli id="T805" time="491.2233517783117" />
         <tli id="T806" time="491.761" type="appl" />
         <tli id="T807" time="492.172" type="appl" />
         <tli id="T808" time="492.583" type="appl" />
         <tli id="T809" time="492.994" type="appl" />
         <tli id="T810" time="493.8116714871412" />
         <tli id="T811" time="493.853" type="appl" />
         <tli id="T812" time="494.301" type="appl" />
         <tli id="T813" time="494.75" type="appl" />
         <tli id="T814" time="495.198" type="appl" />
         <tli id="T815" time="495.646" type="appl" />
         <tli id="T816" time="496.094" type="appl" />
         <tli id="T817" time="496.542" type="appl" />
         <tli id="T818" time="496.991" type="appl" />
         <tli id="T819" time="497.439" type="appl" />
         <tli id="T820" time="498.05368040788017" />
         <tli id="T821" time="498.875" type="appl" />
         <tli id="T822" time="499.862" type="appl" />
         <tli id="T823" time="500.85" type="appl" />
         <tli id="T824" time="501.837" type="appl" />
         <tli id="T825" time="502.825" type="appl" />
         <tli id="T826" time="503.812" type="appl" />
         <tli id="T827" time="505.3573398792437" />
         <tli id="T828" time="505.705" type="appl" />
         <tli id="T930" time="506.0063333333333" type="intp" />
         <tli id="T929" time="506.30766666666665" type="intp" />
         <tli id="T829" time="506.609" type="appl" />
         <tli id="T830" time="507.54732835149224" />
         <tli id="T831" time="507.675" type="appl" />
         <tli id="T832" time="507.836" type="appl" />
         <tli id="T833" time="507.997" type="appl" />
         <tli id="T834" time="508.158" type="appl" />
         <tli id="T835" time="508.319" type="appl" />
         <tli id="T836" time="508.48" type="appl" />
         <tli id="T837" time="508.641" type="appl" />
         <tli id="T838" time="508.802" type="appl" />
         <tli id="T839" time="508.963" type="appl" />
         <tli id="T840" time="509.124" type="appl" />
         <tli id="T841" time="509.285" type="appl" />
         <tli id="T842" time="509.445" type="appl" />
         <tli id="T843" time="509.606" type="appl" />
         <tli id="T844" time="509.767" type="appl" />
         <tli id="T845" time="509.928" type="appl" />
         <tli id="T846" time="510.089" type="appl" />
         <tli id="T847" time="510.25" type="appl" />
         <tli id="T848" time="510.411" type="appl" />
         <tli id="T849" time="510.572" type="appl" />
         <tli id="T850" time="510.733" type="appl" />
         <tli id="T851" time="510.894" type="appl" />
         <tli id="T852" time="511.055" type="appl" />
         <tli id="T853" time="518.2506053444758" />
         <tli id="T854" time="518.4" type="appl" />
         <tli id="T855" time="519.094" type="appl" />
         <tli id="T856" time="519.788" type="appl" />
         <tli id="T857" time="520.481" type="appl" />
         <tli id="T858" time="521.174" type="appl" />
         <tli id="T859" time="521.868" type="appl" />
         <tli id="T860" time="522.562" type="appl" />
         <tli id="T861" time="523.255" type="appl" />
         <tli id="T862" time="523.948" type="appl" />
         <tli id="T863" time="524.642" type="appl" />
         <tli id="T864" time="525.336" type="appl" />
         <tli id="T865" time="526.029" type="appl" />
         <tli id="T866" time="526.74" type="appl" />
         <tli id="T867" time="527.451" type="appl" />
         <tli id="T868" time="528.0086789795264" />
         <tli id="T869" time="528.703" type="appl" />
         <tli id="T870" time="529.244" type="appl" />
         <tli id="T871" time="529.785" type="appl" />
         <tli id="T872" time="530.326" type="appl" />
         <tli id="T873" time="530.867" type="appl" />
         <tli id="T874" time="531.408" type="appl" />
         <tli id="T875" time="531.949" type="appl" />
         <tli id="T876" time="532.49" type="appl" />
         <tli id="T877" time="533.031" type="appl" />
         <tli id="T878" time="533.572" type="appl" />
         <tli id="T879" time="534.113" type="appl" />
         <tli id="T880" time="534.654" type="appl" />
         <tli id="T881" time="535.195" type="appl" />
         <tli id="T882" time="535.736" type="appl" />
         <tli id="T883" time="536.277" type="appl" />
         <tli id="T884" time="536.818" type="appl" />
         <tli id="T885" time="537.359" type="appl" />
         <tli id="T886" time="537.9" type="appl" />
         <tli id="T887" time="538.441" type="appl" />
         <tli id="T888" time="539.3286714762174" />
         <tli id="T889" time="539.396" type="appl" />
         <tli id="T890" time="539.811" type="appl" />
         <tli id="T891" time="540.225" type="appl" />
         <tli id="T892" time="540.64" type="appl" />
         <tli id="T893" time="541.054" type="appl" />
         <tli id="T894" time="541.468" type="appl" />
         <tli id="T895" time="541.883" type="appl" />
         <tli id="T896" time="542.3236557110962" />
         <tli id="T897" time="542.8870000000001" type="intp" />
         <tli id="T898" time="543.4770000000001" type="intp" />
         <tli id="T899" time="544.0670000000001" type="intp" />
         <tli id="T900" time="544.6570000000002" type="intp" />
         <tli id="T901" time="545.2470000000002" type="intp" />
         <tli id="T902" time="545.8370000000002" type="intp" />
         <tli id="T903" time="546.9171211151573" />
         <tli id="T904" time="547.0170000000002" type="intp" />
         <tli id="T905" time="547.6070000000002" type="intp" />
         <tli id="T906" time="548.1970000000001" type="intp" />
         <tli id="T907" time="548.787" type="intp" />
         <tli id="T908" time="549.3770000000001" type="intp" />
         <tli id="T909" time="549.8003351050437" />
         <tli id="T910" time="550.557" type="appl" />
         <tli id="T911" time="551.305" type="appl" />
         <tli id="T912" time="552.0653231825063" />
         <tli id="T913" time="552.78" type="appl" />
         <tli id="T914" time="553.507" type="appl" />
         <tli id="T915" time="554.235" type="appl" />
         <tli id="T916" time="554.962" type="appl" />
         <tli id="T917" time="555.5033259187393" />
         <tli id="T918" time="556.044" type="appl" />
         <tli id="T919" time="556.398" type="appl" />
         <tli id="T920" time="556.752" type="appl" />
         <tli id="T921" time="557.106" type="appl" />
         <tli id="T922" time="557.6933143909879" />
         <tli id="T923" time="558.221" type="appl" />
         <tli id="T924" time="558.981" type="appl" />
         <tli id="T925" time="559.742" type="appl" />
         <tli id="T926" time="560.503" type="appl" />
         <tli id="T927" time="561.263" type="appl" />
         <tli id="T928" time="562.024" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="KiMN"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T928" id="Seg_0" n="sc" s="T0">
               <ts e="T5" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Dʼe</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">min</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">tabahɨttarga</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">ihilleteːri</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">haŋarabɨn</ts>
                  <nts id="Seg_17" n="HIAT:ip">.</nts>
                  <nts id="Seg_18" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T11" id="Seg_20" n="HIAT:u" s="T5">
                  <ts e="T6" id="Seg_22" n="HIAT:w" s="T5">Min</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_25" n="HIAT:w" s="T6">ügüstük</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_28" n="HIAT:w" s="T7">üleleːbit</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">kihibin</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_34" n="HIAT:w" s="T9">tabaga</ts>
                  <nts id="Seg_35" n="HIAT:ip">,</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_38" n="HIAT:w" s="T10">urut</ts>
                  <nts id="Seg_39" n="HIAT:ip">.</nts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T17" id="Seg_42" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_44" n="HIAT:w" s="T11">Bu</ts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_47" n="HIAT:w" s="T12">taba</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_50" n="HIAT:w" s="T13">törüːrün</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_53" n="HIAT:w" s="T14">tuhunan</ts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_56" n="HIAT:w" s="T15">haŋarɨ͡am</ts>
                  <nts id="Seg_57" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">anɨ</ts>
                  <nts id="Seg_60" n="HIAT:ip">.</nts>
                  <nts id="Seg_61" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T28" id="Seg_63" n="HIAT:u" s="T17">
                  <ts e="T18" id="Seg_65" n="HIAT:w" s="T17">Kannɨk</ts>
                  <nts id="Seg_66" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_68" n="HIAT:w" s="T18">da</ts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_71" n="HIAT:w" s="T19">otto</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_74" n="HIAT:w" s="T20">törüːrütten</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_77" n="HIAT:w" s="T21">bu͡olar</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_80" n="HIAT:w" s="T22">bu͡o</ts>
                  <nts id="Seg_81" n="HIAT:ip">,</nts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_84" n="HIAT:w" s="T23">taba</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_87" n="HIAT:w" s="T24">da</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_90" n="HIAT:w" s="T25">ü͡ösküːre</ts>
                  <nts id="Seg_91" n="HIAT:ip">,</nts>
                  <nts id="Seg_92" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_94" n="HIAT:w" s="T26">kihi</ts>
                  <nts id="Seg_95" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_97" n="HIAT:w" s="T27">de</ts>
                  <nts id="Seg_98" n="HIAT:ip">.</nts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_101" n="HIAT:u" s="T28">
                  <ts e="T29" id="Seg_103" n="HIAT:w" s="T28">Onon</ts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_106" n="HIAT:w" s="T29">min</ts>
                  <nts id="Seg_107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_109" n="HIAT:w" s="T30">haŋarabɨn</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_112" n="HIAT:w" s="T31">dʼe</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_115" n="HIAT:w" s="T32">bugurduk</ts>
                  <nts id="Seg_116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_118" n="HIAT:w" s="T33">taba</ts>
                  <nts id="Seg_119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_121" n="HIAT:w" s="T34">törüːrüger</ts>
                  <nts id="Seg_122" n="HIAT:ip">.</nts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T37" id="Seg_125" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_127" n="HIAT:w" s="T35">Pastuːxtarga</ts>
                  <nts id="Seg_128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_130" n="HIAT:w" s="T36">ihilleteːri</ts>
                  <nts id="Seg_131" n="HIAT:ip">.</nts>
                  <nts id="Seg_132" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T41" id="Seg_134" n="HIAT:u" s="T37">
                  <ts e="T38" id="Seg_136" n="HIAT:w" s="T37">Min</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_139" n="HIAT:w" s="T38">haŋararɨm</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_142" n="HIAT:w" s="T39">dʼe</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_145" n="HIAT:w" s="T40">bugurdukkaːn</ts>
                  <nts id="Seg_146" n="HIAT:ip">.</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T45" id="Seg_149" n="HIAT:u" s="T41">
                  <ts e="T42" id="Seg_151" n="HIAT:w" s="T41">Anɨ</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_154" n="HIAT:w" s="T42">bu</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_157" n="HIAT:w" s="T43">tabanɨ</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_160" n="HIAT:w" s="T44">karajallar</ts>
                  <nts id="Seg_161" n="HIAT:ip">.</nts>
                  <nts id="Seg_162" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T55" id="Seg_164" n="HIAT:u" s="T45">
                  <ts e="T46" id="Seg_166" n="HIAT:w" s="T45">Min</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_169" n="HIAT:w" s="T46">urut</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_172" n="HIAT:w" s="T47">karaja</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_175" n="HIAT:w" s="T48">hɨldʼar</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_178" n="HIAT:w" s="T49">erdekpine</ts>
                  <nts id="Seg_179" n="HIAT:ip">,</nts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_182" n="HIAT:w" s="T50">taba</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_185" n="HIAT:w" s="T51">töröːtögüne</ts>
                  <nts id="Seg_186" n="HIAT:ip">,</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_189" n="HIAT:w" s="T52">ügüs</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_192" n="HIAT:w" s="T53">prʼičʼineleːk</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_195" n="HIAT:w" s="T54">bu͡olar</ts>
                  <nts id="Seg_196" n="HIAT:ip">.</nts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T63" id="Seg_199" n="HIAT:u" s="T55">
                  <ts e="T56" id="Seg_201" n="HIAT:w" s="T55">Taba</ts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_204" n="HIAT:w" s="T56">törüːrün</ts>
                  <nts id="Seg_205" n="HIAT:ip">,</nts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_208" n="HIAT:w" s="T57">pastuːx</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_211" n="HIAT:w" s="T58">erdekpine</ts>
                  <nts id="Seg_212" n="HIAT:ip">,</nts>
                  <nts id="Seg_213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_215" n="HIAT:w" s="T59">ketiː</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_218" n="HIAT:w" s="T60">hɨldʼammɨn</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_221" n="HIAT:w" s="T61">kerijeːčči</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_224" n="HIAT:w" s="T62">etim</ts>
                  <nts id="Seg_225" n="HIAT:ip">.</nts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T70" id="Seg_228" n="HIAT:u" s="T63">
                  <ts e="T64" id="Seg_230" n="HIAT:w" s="T63">Taba</ts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_233" n="HIAT:w" s="T64">töröːn</ts>
                  <nts id="Seg_234" n="HIAT:ip">,</nts>
                  <nts id="Seg_235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_237" n="HIAT:w" s="T65">ogotun</ts>
                  <nts id="Seg_238" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_240" n="HIAT:w" s="T66">tüherdegine</ts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_243" n="HIAT:w" s="T67">eŋin-eŋinnik</ts>
                  <nts id="Seg_244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_246" n="HIAT:w" s="T68">törüːr</ts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_249" n="HIAT:w" s="T69">taba</ts>
                  <nts id="Seg_250" n="HIAT:ip">.</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T79" id="Seg_253" n="HIAT:u" s="T70">
                  <ts e="T71" id="Seg_255" n="HIAT:w" s="T70">Horok</ts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_258" n="HIAT:w" s="T71">taba</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_261" n="HIAT:w" s="T72">tuguta</ts>
                  <nts id="Seg_262" n="HIAT:ip">,</nts>
                  <nts id="Seg_263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_265" n="HIAT:w" s="T73">töröːn</ts>
                  <nts id="Seg_266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_268" n="HIAT:w" s="T74">baraːn</ts>
                  <nts id="Seg_269" n="HIAT:ip">,</nts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_272" n="HIAT:w" s="T75">tebi͡elene</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_275" n="HIAT:w" s="T76">hɨtaːččɨ</ts>
                  <nts id="Seg_276" n="HIAT:ip">,</nts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_279" n="HIAT:w" s="T77">koton</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_282" n="HIAT:w" s="T78">turbakka</ts>
                  <nts id="Seg_283" n="HIAT:ip">.</nts>
                  <nts id="Seg_284" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T88" id="Seg_286" n="HIAT:u" s="T79">
                  <ts e="T80" id="Seg_288" n="HIAT:w" s="T79">Inʼetin</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_291" n="HIAT:w" s="T80">kördökkö</ts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_294" n="HIAT:w" s="T81">emis</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_297" n="HIAT:w" s="T82">bagaj</ts>
                  <nts id="Seg_298" n="HIAT:ip">,</nts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_301" n="HIAT:w" s="T83">tugutun</ts>
                  <nts id="Seg_302" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_304" n="HIAT:w" s="T84">kördökkö</ts>
                  <nts id="Seg_305" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_307" n="HIAT:w" s="T85">ulakan</ts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_310" n="HIAT:w" s="T86">bagaj</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_313" n="HIAT:w" s="T87">bu͡olar</ts>
                  <nts id="Seg_314" n="HIAT:ip">.</nts>
                  <nts id="Seg_315" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T93" id="Seg_317" n="HIAT:u" s="T88">
                  <ts e="T89" id="Seg_319" n="HIAT:w" s="T88">Bu</ts>
                  <nts id="Seg_320" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_322" n="HIAT:w" s="T89">tebi͡elene</ts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_325" n="HIAT:w" s="T90">hɨtarɨ</ts>
                  <nts id="Seg_326" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_328" n="HIAT:w" s="T91">ergičiti͡ekke</ts>
                  <nts id="Seg_329" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_331" n="HIAT:w" s="T92">naːda</ts>
                  <nts id="Seg_332" n="HIAT:ip">.</nts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T104" id="Seg_335" n="HIAT:u" s="T93">
                  <ts e="T94" id="Seg_337" n="HIAT:w" s="T93">Ergičiten</ts>
                  <nts id="Seg_338" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_340" n="HIAT:w" s="T94">kördökkö</ts>
                  <nts id="Seg_341" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_342" n="HIAT:ip">–</nts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_345" n="HIAT:w" s="T95">tuːgu</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T97" id="Seg_348" n="HIAT:w" s="T96">gɨnan</ts>
                  <nts id="Seg_349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_351" n="HIAT:w" s="T97">togo</ts>
                  <nts id="Seg_352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_354" n="HIAT:w" s="T98">turbat</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_357" n="HIAT:w" s="T99">di͡en</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_360" n="HIAT:w" s="T100">manɨ</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_363" n="HIAT:w" s="T101">ɨbaktɨ͡akka</ts>
                  <nts id="Seg_364" n="HIAT:ip">,</nts>
                  <nts id="Seg_365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_367" n="HIAT:w" s="T102">bɨ͡arɨn</ts>
                  <nts id="Seg_368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_370" n="HIAT:w" s="T103">ɨbaktɨ͡akka</ts>
                  <nts id="Seg_371" n="HIAT:ip">.</nts>
                  <nts id="Seg_372" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T110" id="Seg_374" n="HIAT:u" s="T104">
                  <ts e="T105" id="Seg_376" n="HIAT:w" s="T104">Taba</ts>
                  <nts id="Seg_377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_379" n="HIAT:w" s="T105">töröːtögüne</ts>
                  <nts id="Seg_380" n="HIAT:ip">,</nts>
                  <nts id="Seg_381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_383" n="HIAT:w" s="T106">tugut</ts>
                  <nts id="Seg_384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_386" n="HIAT:w" s="T107">gi͡ene</ts>
                  <nts id="Seg_387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_389" n="HIAT:w" s="T108">kiːnneːk</ts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_392" n="HIAT:w" s="T109">bu͡olaːččɨ</ts>
                  <nts id="Seg_393" n="HIAT:ip">.</nts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T115" id="Seg_396" n="HIAT:u" s="T110">
                  <ts e="T111" id="Seg_398" n="HIAT:w" s="T110">Bu</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_401" n="HIAT:w" s="T111">kiːnin</ts>
                  <nts id="Seg_402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_404" n="HIAT:w" s="T112">horok</ts>
                  <nts id="Seg_405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_407" n="HIAT:w" s="T113">taba</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_410" n="HIAT:w" s="T114">hubujaːččɨ</ts>
                  <nts id="Seg_411" n="HIAT:ip">.</nts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T126" id="Seg_414" n="HIAT:u" s="T115">
                  <ts e="T116" id="Seg_416" n="HIAT:w" s="T115">Bu</ts>
                  <nts id="Seg_417" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_419" n="HIAT:w" s="T116">kiːne</ts>
                  <nts id="Seg_420" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_422" n="HIAT:w" s="T117">hubulunnagɨna</ts>
                  <nts id="Seg_423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_425" n="HIAT:w" s="T118">bunan</ts>
                  <nts id="Seg_426" n="HIAT:ip">,</nts>
                  <nts id="Seg_427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_429" n="HIAT:w" s="T119">ihitten</ts>
                  <nts id="Seg_430" n="HIAT:ip">,</nts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_433" n="HIAT:w" s="T120">huptu</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_436" n="HIAT:w" s="T121">üːt</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_439" n="HIAT:w" s="T122">bu͡olar</ts>
                  <nts id="Seg_440" n="HIAT:ip">,</nts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_443" n="HIAT:w" s="T123">onon</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_446" n="HIAT:w" s="T124">tɨːna</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_449" n="HIAT:w" s="T125">taksar</ts>
                  <nts id="Seg_450" n="HIAT:ip">.</nts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_453" n="HIAT:u" s="T126">
                  <ts e="T127" id="Seg_455" n="HIAT:w" s="T126">Mu͡otunan</ts>
                  <nts id="Seg_456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_458" n="HIAT:w" s="T127">kam</ts>
                  <nts id="Seg_459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_461" n="HIAT:w" s="T128">baːjdakka</ts>
                  <nts id="Seg_462" n="HIAT:ip">,</nts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_465" n="HIAT:w" s="T129">bu</ts>
                  <nts id="Seg_466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_468" n="HIAT:w" s="T130">taba</ts>
                  <nts id="Seg_469" n="HIAT:ip">,</nts>
                  <nts id="Seg_470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_472" n="HIAT:w" s="T131">bu</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_475" n="HIAT:w" s="T132">tugut</ts>
                  <nts id="Seg_476" n="HIAT:ip">,</nts>
                  <nts id="Seg_477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_479" n="HIAT:w" s="T133">turan</ts>
                  <nts id="Seg_480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_482" n="HIAT:w" s="T134">keleːčči</ts>
                  <nts id="Seg_483" n="HIAT:ip">.</nts>
                  <nts id="Seg_484" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T139" id="Seg_486" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_488" n="HIAT:w" s="T135">Ol</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_491" n="HIAT:w" s="T136">aːta</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_493" n="HIAT:ip">"</nts>
                  <ts e="T138" id="Seg_495" n="HIAT:w" s="T137">kiːnin</ts>
                  <nts id="Seg_496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_498" n="HIAT:w" s="T138">hubujar</ts>
                  <nts id="Seg_499" n="HIAT:ip">"</nts>
                  <nts id="Seg_500" n="HIAT:ip">.</nts>
                  <nts id="Seg_501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T147" id="Seg_503" n="HIAT:u" s="T139">
                  <ts e="T140" id="Seg_505" n="HIAT:w" s="T139">Iti</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_508" n="HIAT:w" s="T140">bu͡olar</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_511" n="HIAT:w" s="T141">taba</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T143" id="Seg_514" n="HIAT:w" s="T142">gi͡ene</ts>
                  <nts id="Seg_515" n="HIAT:ip">,</nts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_518" n="HIAT:w" s="T143">tugutun</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_521" n="HIAT:w" s="T144">gi͡ene</ts>
                  <nts id="Seg_522" n="HIAT:ip">,</nts>
                  <nts id="Seg_523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_525" n="HIAT:w" s="T145">prʼičʼinete</ts>
                  <nts id="Seg_526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_528" n="HIAT:w" s="T146">töröːtögüne</ts>
                  <nts id="Seg_529" n="HIAT:ip">.</nts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T151" id="Seg_532" n="HIAT:u" s="T147">
                  <ts e="T148" id="Seg_534" n="HIAT:w" s="T147">Onton</ts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_537" n="HIAT:w" s="T148">biːr</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_540" n="HIAT:w" s="T149">prʼičʼineleːk</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_542" n="HIAT:ip">–</nts>
                  <nts id="Seg_543" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_545" n="HIAT:w" s="T150">slabaj</ts>
                  <nts id="Seg_546" n="HIAT:ip">.</nts>
                  <nts id="Seg_547" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T155" id="Seg_549" n="HIAT:u" s="T151">
                  <ts e="T152" id="Seg_551" n="HIAT:w" s="T151">Taba</ts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_554" n="HIAT:w" s="T152">törüːr</ts>
                  <nts id="Seg_555" n="HIAT:ip">,</nts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_558" n="HIAT:w" s="T153">torugan</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_561" n="HIAT:w" s="T154">inʼete</ts>
                  <nts id="Seg_562" n="HIAT:ip">.</nts>
                  <nts id="Seg_563" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T161" id="Seg_565" n="HIAT:u" s="T155">
                  <ts e="T156" id="Seg_567" n="HIAT:w" s="T155">Bu</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_570" n="HIAT:w" s="T156">tuguta</ts>
                  <nts id="Seg_571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_573" n="HIAT:w" s="T157">töröːtögüne</ts>
                  <nts id="Seg_574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_576" n="HIAT:w" s="T158">dʼapču</ts>
                  <nts id="Seg_577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_579" n="HIAT:w" s="T159">di͡en</ts>
                  <nts id="Seg_580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_582" n="HIAT:w" s="T160">aːttanaːččɨ</ts>
                  <nts id="Seg_583" n="HIAT:ip">.</nts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T165" id="Seg_586" n="HIAT:u" s="T161">
                  <ts e="T162" id="Seg_588" n="HIAT:w" s="T161">Emi͡e</ts>
                  <nts id="Seg_589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_591" n="HIAT:w" s="T162">tuguta</ts>
                  <nts id="Seg_592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_594" n="HIAT:w" s="T163">turaːččɨta</ts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_597" n="HIAT:w" s="T164">hu͡ok</ts>
                  <nts id="Seg_598" n="HIAT:ip">.</nts>
                  <nts id="Seg_599" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T178" id="Seg_601" n="HIAT:u" s="T165">
                  <ts e="T166" id="Seg_603" n="HIAT:w" s="T165">Bu</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_606" n="HIAT:w" s="T166">tugutu</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_609" n="HIAT:w" s="T167">ergičiten</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_612" n="HIAT:w" s="T168">kördökkö</ts>
                  <nts id="Seg_613" n="HIAT:ip">,</nts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_616" n="HIAT:w" s="T169">amattan</ts>
                  <nts id="Seg_617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_619" n="HIAT:w" s="T170">slabaj</ts>
                  <nts id="Seg_620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_622" n="HIAT:w" s="T171">bagaj</ts>
                  <nts id="Seg_623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_625" n="HIAT:w" s="T172">bu͡olar</ts>
                  <nts id="Seg_626" n="HIAT:ip">,</nts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_629" n="HIAT:w" s="T173">bu</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_632" n="HIAT:w" s="T174">tugut</ts>
                  <nts id="Seg_633" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_635" n="HIAT:w" s="T175">tebi͡elene</ts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_638" n="HIAT:w" s="T176">agaj</ts>
                  <nts id="Seg_639" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_641" n="HIAT:w" s="T177">hɨtaːččɨ</ts>
                  <nts id="Seg_642" n="HIAT:ip">.</nts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T197" id="Seg_645" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_647" n="HIAT:w" s="T178">Bu</ts>
                  <nts id="Seg_648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_650" n="HIAT:w" s="T179">tugutu</ts>
                  <nts id="Seg_651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_653" n="HIAT:w" s="T180">ittenne</ts>
                  <nts id="Seg_654" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_656" n="HIAT:w" s="T181">tutan</ts>
                  <nts id="Seg_657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_659" n="HIAT:w" s="T182">baraːn</ts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_662" n="HIAT:w" s="T183">tü͡öhün</ts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_665" n="HIAT:w" s="T184">imerijdekke</ts>
                  <nts id="Seg_666" n="HIAT:ip">,</nts>
                  <nts id="Seg_667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_669" n="HIAT:w" s="T185">oččogo</ts>
                  <nts id="Seg_670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_672" n="HIAT:w" s="T186">bu</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_675" n="HIAT:w" s="T187">tü͡öhe</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_678" n="HIAT:w" s="T188">Dʼapču</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_681" n="HIAT:w" s="T189">gi͡ene</ts>
                  <nts id="Seg_682" n="HIAT:ip">,</nts>
                  <nts id="Seg_683" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_685" n="HIAT:w" s="T190">kahan</ts>
                  <nts id="Seg_686" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_688" n="HIAT:w" s="T191">da</ts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_691" n="HIAT:w" s="T192">tugut</ts>
                  <nts id="Seg_692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_694" n="HIAT:w" s="T193">töröːtögüne</ts>
                  <nts id="Seg_695" n="HIAT:ip">,</nts>
                  <nts id="Seg_696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_698" n="HIAT:w" s="T194">oŋu͡oga</ts>
                  <nts id="Seg_699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_701" n="HIAT:w" s="T195">oŋurgas</ts>
                  <nts id="Seg_702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_704" n="HIAT:w" s="T196">bu͡olar</ts>
                  <nts id="Seg_705" n="HIAT:ip">.</nts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T204" id="Seg_708" n="HIAT:u" s="T197">
                  <ts e="T198" id="Seg_710" n="HIAT:w" s="T197">Bunɨ</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_713" n="HIAT:w" s="T198">tü͡öhün</ts>
                  <nts id="Seg_714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_716" n="HIAT:w" s="T199">imerijdekke</ts>
                  <nts id="Seg_717" n="HIAT:ip">,</nts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_720" n="HIAT:w" s="T200">tura-tura</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_723" n="HIAT:w" s="T201">tühe</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_726" n="HIAT:w" s="T202">hɨldʼaːččɨ</ts>
                  <nts id="Seg_727" n="HIAT:ip">,</nts>
                  <nts id="Seg_728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_730" n="HIAT:w" s="T203">tɨːnnagɨna</ts>
                  <nts id="Seg_731" n="HIAT:ip">.</nts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T220" id="Seg_734" n="HIAT:u" s="T204">
                  <ts e="T205" id="Seg_736" n="HIAT:w" s="T204">Manɨ</ts>
                  <nts id="Seg_737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_739" n="HIAT:w" s="T205">inʼetin</ts>
                  <nts id="Seg_740" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_742" n="HIAT:w" s="T206">tutan</ts>
                  <nts id="Seg_743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_745" n="HIAT:w" s="T207">baraːn</ts>
                  <nts id="Seg_746" n="HIAT:ip">,</nts>
                  <nts id="Seg_747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_749" n="HIAT:w" s="T208">inʼetin</ts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_752" n="HIAT:w" s="T209">emneri͡ekke</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_755" n="HIAT:w" s="T210">naːda</ts>
                  <nts id="Seg_756" n="HIAT:ip">,</nts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_759" n="HIAT:w" s="T211">bu</ts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_762" n="HIAT:w" s="T212">tugut</ts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_765" n="HIAT:w" s="T213">tü͡öhün</ts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_768" n="HIAT:w" s="T214">kimi</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_771" n="HIAT:w" s="T215">ilbijbitin</ts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_774" n="HIAT:w" s="T216">kenne</ts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_777" n="HIAT:w" s="T217">koton</ts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_780" n="HIAT:w" s="T218">emeːččite</ts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_783" n="HIAT:w" s="T219">hu͡ok</ts>
                  <nts id="Seg_784" n="HIAT:ip">.</nts>
                  <nts id="Seg_785" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T226" id="Seg_787" n="HIAT:u" s="T220">
                  <ts e="T221" id="Seg_789" n="HIAT:w" s="T220">Manɨ</ts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_792" n="HIAT:w" s="T221">inʼete</ts>
                  <nts id="Seg_793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_795" n="HIAT:w" s="T222">emije</ts>
                  <nts id="Seg_796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_798" n="HIAT:w" s="T223">ɨ͡astaːk</ts>
                  <nts id="Seg_799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_801" n="HIAT:w" s="T224">bu͡olla</ts>
                  <nts id="Seg_802" n="HIAT:ip">,</nts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_805" n="HIAT:w" s="T225">lampagar</ts>
                  <nts id="Seg_806" n="HIAT:ip">.</nts>
                  <nts id="Seg_807" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T230" id="Seg_809" n="HIAT:u" s="T226">
                  <ts e="T227" id="Seg_811" n="HIAT:w" s="T226">Bu</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_814" n="HIAT:w" s="T227">ɨ͡ahɨ</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_817" n="HIAT:w" s="T228">ɨgaːččɨ</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_820" n="HIAT:w" s="T229">kihi</ts>
                  <nts id="Seg_821" n="HIAT:ip">.</nts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T243" id="Seg_824" n="HIAT:u" s="T230">
                  <ts e="T231" id="Seg_826" n="HIAT:w" s="T230">ɨgan</ts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_829" n="HIAT:w" s="T231">baraːn</ts>
                  <nts id="Seg_830" n="HIAT:ip">,</nts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_833" n="HIAT:w" s="T232">ɨbagas</ts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_836" n="HIAT:w" s="T233">emije</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_839" n="HIAT:w" s="T234">kellegine</ts>
                  <nts id="Seg_840" n="HIAT:ip">,</nts>
                  <nts id="Seg_841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_843" n="HIAT:w" s="T235">oččogo</ts>
                  <nts id="Seg_844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_846" n="HIAT:w" s="T236">emnereːčči</ts>
                  <nts id="Seg_847" n="HIAT:ip">,</nts>
                  <nts id="Seg_848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_850" n="HIAT:w" s="T237">oččogo</ts>
                  <nts id="Seg_851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_853" n="HIAT:w" s="T238">emnerdegine</ts>
                  <nts id="Seg_854" n="HIAT:ip">,</nts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_857" n="HIAT:w" s="T239">bu</ts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_860" n="HIAT:w" s="T240">tugut</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_863" n="HIAT:w" s="T241">kihi</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_866" n="HIAT:w" s="T242">bu͡olar</ts>
                  <nts id="Seg_867" n="HIAT:ip">.</nts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T245" id="Seg_870" n="HIAT:u" s="T243">
                  <ts e="T244" id="Seg_872" n="HIAT:w" s="T243">Pastuːktar</ts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_875" n="HIAT:w" s="T244">köhöllör</ts>
                  <nts id="Seg_876" n="HIAT:ip">.</nts>
                  <nts id="Seg_877" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T255" id="Seg_879" n="HIAT:u" s="T245">
                  <ts e="T246" id="Seg_881" n="HIAT:w" s="T245">Ügüs</ts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_884" n="HIAT:w" s="T246">taba</ts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_887" n="HIAT:w" s="T247">töröːtögüne</ts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_890" n="HIAT:w" s="T248">kahan</ts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_893" n="HIAT:w" s="T249">da</ts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_896" n="HIAT:w" s="T250">törüːlik</ts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_899" n="HIAT:w" s="T251">tabanɨ</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_902" n="HIAT:w" s="T252">inni</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_905" n="HIAT:w" s="T253">di͡ek</ts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_908" n="HIAT:w" s="T254">illeːččiler</ts>
                  <nts id="Seg_909" n="HIAT:ip">.</nts>
                  <nts id="Seg_910" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T266" id="Seg_912" n="HIAT:u" s="T255">
                  <ts e="T256" id="Seg_914" n="HIAT:w" s="T255">Töröːbüt</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_917" n="HIAT:w" s="T256">tabanɨ</ts>
                  <nts id="Seg_918" n="HIAT:ip">,</nts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_921" n="HIAT:w" s="T257">tuguta</ts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_924" n="HIAT:w" s="T258">ulaːta</ts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_927" n="HIAT:w" s="T259">tühü͡ör</ts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_930" n="HIAT:w" s="T260">di͡eri</ts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_933" n="HIAT:w" s="T261">kaːmpatɨn</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_936" n="HIAT:w" s="T262">ihin</ts>
                  <nts id="Seg_937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_939" n="HIAT:w" s="T263">kenni</ts>
                  <nts id="Seg_940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_942" n="HIAT:w" s="T264">di͡ek</ts>
                  <nts id="Seg_943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_945" n="HIAT:w" s="T265">keːheller</ts>
                  <nts id="Seg_946" n="HIAT:ip">.</nts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T284" id="Seg_949" n="HIAT:u" s="T266">
                  <ts e="T267" id="Seg_951" n="HIAT:w" s="T266">Bu</ts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_954" n="HIAT:w" s="T267">keːhiːleriger</ts>
                  <nts id="Seg_955" n="HIAT:ip">,</nts>
                  <nts id="Seg_956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_958" n="HIAT:w" s="T268">bu</ts>
                  <nts id="Seg_959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_961" n="HIAT:w" s="T269">te</ts>
                  <nts id="Seg_962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_964" n="HIAT:w" s="T270">u͡ontan</ts>
                  <nts id="Seg_965" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_967" n="HIAT:w" s="T271">taksa</ts>
                  <nts id="Seg_968" n="HIAT:ip">,</nts>
                  <nts id="Seg_969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_971" n="HIAT:w" s="T272">hüːrbečče</ts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_974" n="HIAT:w" s="T273">taba</ts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_977" n="HIAT:w" s="T274">töröːbütün</ts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_980" n="HIAT:w" s="T275">kenne</ts>
                  <nts id="Seg_981" n="HIAT:ip">,</nts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_984" n="HIAT:w" s="T276">oččogo</ts>
                  <nts id="Seg_985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_987" n="HIAT:w" s="T277">bu</ts>
                  <nts id="Seg_988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_990" n="HIAT:w" s="T278">törüːlik</ts>
                  <nts id="Seg_991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_993" n="HIAT:w" s="T279">tabanɨ</ts>
                  <nts id="Seg_994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_996" n="HIAT:w" s="T280">inni</ts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_999" n="HIAT:w" s="T281">di͡ek</ts>
                  <nts id="Seg_1000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_1002" n="HIAT:w" s="T282">illi͡ekke</ts>
                  <nts id="Seg_1003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_1005" n="HIAT:w" s="T283">naːda</ts>
                  <nts id="Seg_1006" n="HIAT:ip">.</nts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T293" id="Seg_1009" n="HIAT:u" s="T284">
                  <ts e="T285" id="Seg_1011" n="HIAT:w" s="T284">Bu</ts>
                  <nts id="Seg_1012" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_1014" n="HIAT:w" s="T285">tabanɨ</ts>
                  <nts id="Seg_1015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_1017" n="HIAT:w" s="T286">illerge</ts>
                  <nts id="Seg_1018" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_1020" n="HIAT:w" s="T287">horok</ts>
                  <nts id="Seg_1021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_1023" n="HIAT:w" s="T288">pastuːk</ts>
                  <nts id="Seg_1024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1026" n="HIAT:w" s="T289">bu</ts>
                  <nts id="Seg_1027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1029" n="HIAT:w" s="T290">töröːbüt</ts>
                  <nts id="Seg_1030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1032" n="HIAT:w" s="T291">tuguttaːkka</ts>
                  <nts id="Seg_1033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_1035" n="HIAT:w" s="T292">kaːlar</ts>
                  <nts id="Seg_1036" n="HIAT:ip">.</nts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T300" id="Seg_1039" n="HIAT:u" s="T293">
                  <ts e="T294" id="Seg_1041" n="HIAT:w" s="T293">Horok</ts>
                  <nts id="Seg_1042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1044" n="HIAT:w" s="T294">pastuːk</ts>
                  <nts id="Seg_1045" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1047" n="HIAT:w" s="T295">bu</ts>
                  <nts id="Seg_1048" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1049" n="HIAT:ip">(</nts>
                  <ts e="T297" id="Seg_1051" n="HIAT:w" s="T296">töröː-</ts>
                  <nts id="Seg_1052" n="HIAT:ip">)</nts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1055" n="HIAT:w" s="T297">törüːlik</ts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1058" n="HIAT:w" s="T298">tabanɨ</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1061" n="HIAT:w" s="T299">iller</ts>
                  <nts id="Seg_1062" n="HIAT:ip">.</nts>
                  <nts id="Seg_1063" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T314" id="Seg_1065" n="HIAT:u" s="T300">
                  <ts e="T301" id="Seg_1067" n="HIAT:w" s="T300">Bu</ts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1070" n="HIAT:w" s="T301">illeriger</ts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1073" n="HIAT:w" s="T302">ahatan</ts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1076" n="HIAT:w" s="T303">albɨnnaːn</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1079" n="HIAT:w" s="T304">kalka</ts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1082" n="HIAT:w" s="T305">hirge</ts>
                  <nts id="Seg_1083" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1085" n="HIAT:w" s="T306">kiːlleren</ts>
                  <nts id="Seg_1086" n="HIAT:ip">,</nts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1089" n="HIAT:w" s="T307">ü͡öre</ts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1092" n="HIAT:w" s="T308">bararɨn</ts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1095" n="HIAT:w" s="T309">köllörbökkö</ts>
                  <nts id="Seg_1096" n="HIAT:ip">,</nts>
                  <nts id="Seg_1097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1099" n="HIAT:w" s="T310">bu</ts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1102" n="HIAT:w" s="T311">tuguttaːk</ts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1105" n="HIAT:w" s="T312">tɨhɨnɨ</ts>
                  <nts id="Seg_1106" n="HIAT:ip">…</nts>
                  <nts id="Seg_1107" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T323" id="Seg_1109" n="HIAT:u" s="T314">
                  <ts e="T315" id="Seg_1111" n="HIAT:w" s="T314">Kahan</ts>
                  <nts id="Seg_1112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1114" n="HIAT:w" s="T315">da</ts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1117" n="HIAT:w" s="T316">taba</ts>
                  <nts id="Seg_1118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1120" n="HIAT:w" s="T317">hannʼɨhan</ts>
                  <nts id="Seg_1121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1123" n="HIAT:w" s="T318">tugutun</ts>
                  <nts id="Seg_1124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1126" n="HIAT:w" s="T319">keːheːčči</ts>
                  <nts id="Seg_1127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1129" n="HIAT:w" s="T320">batɨhan</ts>
                  <nts id="Seg_1130" n="HIAT:ip">,</nts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1133" n="HIAT:w" s="T321">ü͡örüger</ts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1136" n="HIAT:w" s="T322">talahan</ts>
                  <nts id="Seg_1137" n="HIAT:ip">.</nts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T335" id="Seg_1140" n="HIAT:u" s="T323">
                  <ts e="T324" id="Seg_1142" n="HIAT:w" s="T323">Onu</ts>
                  <nts id="Seg_1143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1145" n="HIAT:w" s="T324">ahata</ts>
                  <nts id="Seg_1146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1148" n="HIAT:w" s="T325">hɨldʼan</ts>
                  <nts id="Seg_1149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1151" n="HIAT:w" s="T326">topputun</ts>
                  <nts id="Seg_1152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1154" n="HIAT:w" s="T327">kemnene</ts>
                  <nts id="Seg_1155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1157" n="HIAT:w" s="T328">hɨtarɨgar</ts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1160" n="HIAT:w" s="T329">törüːlik</ts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1163" n="HIAT:w" s="T330">tabatɨn</ts>
                  <nts id="Seg_1164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1166" n="HIAT:w" s="T331">inni</ts>
                  <nts id="Seg_1167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1169" n="HIAT:w" s="T332">di͡ek</ts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1172" n="HIAT:w" s="T333">köhördön</ts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1175" n="HIAT:w" s="T334">iller</ts>
                  <nts id="Seg_1176" n="HIAT:ip">.</nts>
                  <nts id="Seg_1177" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T342" id="Seg_1179" n="HIAT:u" s="T335">
                  <ts e="T336" id="Seg_1181" n="HIAT:w" s="T335">Ol</ts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1184" n="HIAT:w" s="T336">köhörtöktörüne</ts>
                  <nts id="Seg_1185" n="HIAT:ip">,</nts>
                  <nts id="Seg_1186" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1188" n="HIAT:w" s="T337">horok</ts>
                  <nts id="Seg_1189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1191" n="HIAT:w" s="T338">pastuːktar</ts>
                  <nts id="Seg_1192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1194" n="HIAT:w" s="T339">hürdeːk</ts>
                  <nts id="Seg_1195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1197" n="HIAT:w" s="T340">ɨraːk</ts>
                  <nts id="Seg_1198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1200" n="HIAT:w" s="T341">baraːččɨlar</ts>
                  <nts id="Seg_1201" n="HIAT:ip">.</nts>
                  <nts id="Seg_1202" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T368" id="Seg_1204" n="HIAT:u" s="T342">
                  <ts e="T343" id="Seg_1206" n="HIAT:w" s="T342">Bu</ts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1209" n="HIAT:w" s="T343">ɨraːk</ts>
                  <nts id="Seg_1210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1212" n="HIAT:w" s="T344">bardaktarɨna</ts>
                  <nts id="Seg_1213" n="HIAT:ip">,</nts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1216" n="HIAT:w" s="T345">bu</ts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1219" n="HIAT:w" s="T346">tugut</ts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1222" n="HIAT:w" s="T347">harsɨŋŋɨtɨn</ts>
                  <nts id="Seg_1223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1225" n="HIAT:w" s="T348">ol</ts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1228" n="HIAT:w" s="T349">törüːlik</ts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1231" n="HIAT:w" s="T350">tabalaːkka</ts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1234" n="HIAT:w" s="T351">kennikiːtin</ts>
                  <nts id="Seg_1235" n="HIAT:ip">,</nts>
                  <nts id="Seg_1236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1238" n="HIAT:w" s="T352">pastuːktarɨn</ts>
                  <nts id="Seg_1239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1241" n="HIAT:w" s="T353">ol</ts>
                  <nts id="Seg_1242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1244" n="HIAT:w" s="T354">manɨ</ts>
                  <nts id="Seg_1245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1247" n="HIAT:w" s="T355">töröːbüt</ts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1250" n="HIAT:w" s="T356">tɨhɨlaːktarɨn</ts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1253" n="HIAT:w" s="T357">pastuːkta</ts>
                  <nts id="Seg_1254" n="HIAT:ip">,</nts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1257" n="HIAT:w" s="T358">inni</ts>
                  <nts id="Seg_1258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1260" n="HIAT:w" s="T359">di͡ek</ts>
                  <nts id="Seg_1261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1263" n="HIAT:w" s="T360">pastuːktarga</ts>
                  <nts id="Seg_1264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1266" n="HIAT:w" s="T361">illeːriler</ts>
                  <nts id="Seg_1267" n="HIAT:ip">,</nts>
                  <nts id="Seg_1268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1270" n="HIAT:w" s="T362">köhöllörüger</ts>
                  <nts id="Seg_1271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1273" n="HIAT:w" s="T363">taba</ts>
                  <nts id="Seg_1274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1276" n="HIAT:w" s="T364">koton</ts>
                  <nts id="Seg_1277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1279" n="HIAT:w" s="T365">tijeːččite</ts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1282" n="HIAT:w" s="T366">hu͡ok</ts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1285" n="HIAT:w" s="T367">bo</ts>
                  <nts id="Seg_1286" n="HIAT:ip">.</nts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T374" id="Seg_1289" n="HIAT:u" s="T368">
                  <ts e="T369" id="Seg_1291" n="HIAT:w" s="T368">Ol</ts>
                  <nts id="Seg_1292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1294" n="HIAT:w" s="T369">ihin</ts>
                  <nts id="Seg_1295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1297" n="HIAT:w" s="T370">čugas</ts>
                  <nts id="Seg_1298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1300" n="HIAT:w" s="T371">tühü͡ökke</ts>
                  <nts id="Seg_1301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1303" n="HIAT:w" s="T372">körön</ts>
                  <nts id="Seg_1304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1306" n="HIAT:w" s="T373">baraːn</ts>
                  <nts id="Seg_1307" n="HIAT:ip">.</nts>
                  <nts id="Seg_1308" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T383" id="Seg_1310" n="HIAT:u" s="T374">
                  <ts e="T375" id="Seg_1312" n="HIAT:w" s="T374">Onton</ts>
                  <nts id="Seg_1313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1315" n="HIAT:w" s="T375">bu͡ollagɨna</ts>
                  <nts id="Seg_1316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1318" n="HIAT:w" s="T376">bu</ts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1321" n="HIAT:w" s="T377">pastuːktar</ts>
                  <nts id="Seg_1322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1324" n="HIAT:w" s="T378">kahan</ts>
                  <nts id="Seg_1325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1327" n="HIAT:w" s="T379">da</ts>
                  <nts id="Seg_1328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1330" n="HIAT:w" s="T380">kalkalaːk</ts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1333" n="HIAT:w" s="T381">hirinen</ts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1336" n="HIAT:w" s="T382">hɨldʼallar</ts>
                  <nts id="Seg_1337" n="HIAT:ip">.</nts>
                  <nts id="Seg_1338" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T395" id="Seg_1340" n="HIAT:u" s="T383">
                  <ts e="T384" id="Seg_1342" n="HIAT:w" s="T383">Maršrut</ts>
                  <nts id="Seg_1343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1345" n="HIAT:w" s="T384">köröllör</ts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1348" n="HIAT:w" s="T385">taba</ts>
                  <nts id="Seg_1349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1351" n="HIAT:w" s="T386">törötörüger</ts>
                  <nts id="Seg_1352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1354" n="HIAT:w" s="T387">kalkalaːk</ts>
                  <nts id="Seg_1355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1357" n="HIAT:w" s="T388">hirinen</ts>
                  <nts id="Seg_1358" n="HIAT:ip">,</nts>
                  <nts id="Seg_1359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1361" n="HIAT:w" s="T389">ohuːrdaːk</ts>
                  <nts id="Seg_1362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1364" n="HIAT:w" s="T390">hirinen</ts>
                  <nts id="Seg_1365" n="HIAT:ip">,</nts>
                  <nts id="Seg_1366" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1368" n="HIAT:w" s="T391">kajalardaːk</ts>
                  <nts id="Seg_1369" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1371" n="HIAT:w" s="T392">hirderinen</ts>
                  <nts id="Seg_1372" n="HIAT:ip">,</nts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1375" n="HIAT:w" s="T393">ürekterdeːk</ts>
                  <nts id="Seg_1376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1378" n="HIAT:w" s="T394">hirderinen</ts>
                  <nts id="Seg_1379" n="HIAT:ip">.</nts>
                  <nts id="Seg_1380" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T399" id="Seg_1382" n="HIAT:u" s="T395">
                  <ts e="T396" id="Seg_1384" n="HIAT:w" s="T395">Bu</ts>
                  <nts id="Seg_1385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1387" n="HIAT:w" s="T396">ulakan</ts>
                  <nts id="Seg_1388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1390" n="HIAT:w" s="T397">hobu͡oj</ts>
                  <nts id="Seg_1391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1393" n="HIAT:w" s="T398">bɨstaːččɨ</ts>
                  <nts id="Seg_1394" n="HIAT:ip">.</nts>
                  <nts id="Seg_1395" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T411" id="Seg_1397" n="HIAT:u" s="T399">
                  <ts e="T400" id="Seg_1399" n="HIAT:w" s="T399">Bu</ts>
                  <nts id="Seg_1400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1402" n="HIAT:w" s="T400">bɨhɨnnagɨna</ts>
                  <nts id="Seg_1403" n="HIAT:ip">,</nts>
                  <nts id="Seg_1404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1406" n="HIAT:w" s="T401">haːs</ts>
                  <nts id="Seg_1407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1409" n="HIAT:w" s="T402">taba</ts>
                  <nts id="Seg_1410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1412" n="HIAT:w" s="T403">törüːrün</ts>
                  <nts id="Seg_1413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1415" n="HIAT:w" s="T404">hagɨna</ts>
                  <nts id="Seg_1416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1418" n="HIAT:w" s="T405">kün</ts>
                  <nts id="Seg_1419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1421" n="HIAT:w" s="T406">hɨrdaːtagɨna</ts>
                  <nts id="Seg_1422" n="HIAT:ip">,</nts>
                  <nts id="Seg_1423" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1425" n="HIAT:w" s="T407">kɨhɨn</ts>
                  <nts id="Seg_1426" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1428" n="HIAT:w" s="T408">bɨstɨbɨt</ts>
                  <nts id="Seg_1429" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1431" n="HIAT:w" s="T409">hobu͡oj</ts>
                  <nts id="Seg_1432" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1434" n="HIAT:w" s="T410">ataːččɨ</ts>
                  <nts id="Seg_1435" n="HIAT:ip">.</nts>
                  <nts id="Seg_1436" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T415" id="Seg_1438" n="HIAT:u" s="T411">
                  <ts e="T412" id="Seg_1440" n="HIAT:w" s="T411">Ol</ts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1443" n="HIAT:w" s="T412">aːta</ts>
                  <nts id="Seg_1444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1446" n="HIAT:w" s="T413">köŋüs</ts>
                  <nts id="Seg_1447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1449" n="HIAT:w" s="T414">bu͡olar</ts>
                  <nts id="Seg_1450" n="HIAT:ip">.</nts>
                  <nts id="Seg_1451" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T433" id="Seg_1453" n="HIAT:u" s="T415">
                  <ts e="T416" id="Seg_1455" n="HIAT:w" s="T415">Taba</ts>
                  <nts id="Seg_1456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1458" n="HIAT:w" s="T416">huraga</ts>
                  <nts id="Seg_1459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1461" n="HIAT:w" s="T417">hu͡ok</ts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1464" n="HIAT:w" s="T418">hütere</ts>
                  <nts id="Seg_1465" n="HIAT:ip">,</nts>
                  <nts id="Seg_1466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1468" n="HIAT:w" s="T419">ol</ts>
                  <nts id="Seg_1469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1471" n="HIAT:w" s="T420">köŋüs</ts>
                  <nts id="Seg_1472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1474" n="HIAT:w" s="T421">ihiger</ts>
                  <nts id="Seg_1475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1477" n="HIAT:w" s="T422">tüspüt</ts>
                  <nts id="Seg_1478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1480" n="HIAT:w" s="T423">tabanɨ</ts>
                  <nts id="Seg_1481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1483" n="HIAT:w" s="T424">bu</ts>
                  <nts id="Seg_1484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1486" n="HIAT:w" s="T425">pastuːk</ts>
                  <nts id="Seg_1487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1489" n="HIAT:w" s="T426">ketiː</ts>
                  <nts id="Seg_1490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1492" n="HIAT:w" s="T427">hɨldʼan</ts>
                  <nts id="Seg_1493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1495" n="HIAT:w" s="T428">bilbet</ts>
                  <nts id="Seg_1496" n="HIAT:ip">,</nts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1499" n="HIAT:w" s="T429">kas</ts>
                  <nts id="Seg_1500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1502" n="HIAT:w" s="T430">da</ts>
                  <nts id="Seg_1503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1505" n="HIAT:w" s="T431">taba</ts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1508" n="HIAT:w" s="T432">tüspütün</ts>
                  <nts id="Seg_1509" n="HIAT:ip">.</nts>
                  <nts id="Seg_1510" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T435" id="Seg_1512" n="HIAT:u" s="T433">
                  <ts e="T434" id="Seg_1514" n="HIAT:w" s="T433">Honon</ts>
                  <nts id="Seg_1515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1517" n="HIAT:w" s="T434">hu͡oktaːbat</ts>
                  <nts id="Seg_1518" n="HIAT:ip">.</nts>
                  <nts id="Seg_1519" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T440" id="Seg_1521" n="HIAT:u" s="T435">
                  <ts e="T436" id="Seg_1523" n="HIAT:w" s="T435">Taba</ts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1526" n="HIAT:w" s="T436">huraga</ts>
                  <nts id="Seg_1527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1529" n="HIAT:w" s="T437">hu͡ok</ts>
                  <nts id="Seg_1530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1532" n="HIAT:w" s="T438">hütere</ts>
                  <nts id="Seg_1533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1535" n="HIAT:w" s="T439">ol</ts>
                  <nts id="Seg_1536" n="HIAT:ip">.</nts>
                  <nts id="Seg_1537" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T457" id="Seg_1539" n="HIAT:u" s="T440">
                  <ts e="T441" id="Seg_1541" n="HIAT:w" s="T440">Onon</ts>
                  <nts id="Seg_1542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1544" n="HIAT:w" s="T441">brʼigadʼiːrder</ts>
                  <nts id="Seg_1545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1547" n="HIAT:w" s="T442">bu͡ol</ts>
                  <nts id="Seg_1548" n="HIAT:ip">,</nts>
                  <nts id="Seg_1549" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1551" n="HIAT:w" s="T443">pastuːktar</ts>
                  <nts id="Seg_1552" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1554" n="HIAT:w" s="T444">bu͡ol</ts>
                  <nts id="Seg_1555" n="HIAT:ip">,</nts>
                  <nts id="Seg_1556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1558" n="HIAT:w" s="T445">haːs</ts>
                  <nts id="Seg_1559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1561" n="HIAT:w" s="T446">taba</ts>
                  <nts id="Seg_1562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1564" n="HIAT:w" s="T447">törüːrün</ts>
                  <nts id="Seg_1565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1567" n="HIAT:w" s="T448">hagɨna</ts>
                  <nts id="Seg_1568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1569" n="HIAT:ip">(</nts>
                  <nts id="Seg_1570" n="HIAT:ip">(</nts>
                  <ats e="T450" id="Seg_1571" n="HIAT:non-pho" s="T449">PAUSE</ats>
                  <nts id="Seg_1572" n="HIAT:ip">)</nts>
                  <nts id="Seg_1573" n="HIAT:ip">)</nts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1576" n="HIAT:w" s="T450">kaːr</ts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1579" n="HIAT:w" s="T451">irerin</ts>
                  <nts id="Seg_1580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1582" n="HIAT:w" s="T452">hagɨna</ts>
                  <nts id="Seg_1583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1585" n="HIAT:w" s="T453">üčügejdik</ts>
                  <nts id="Seg_1586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1588" n="HIAT:w" s="T454">hirderin</ts>
                  <nts id="Seg_1589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1591" n="HIAT:w" s="T455">körü͡ökterin</ts>
                  <nts id="Seg_1592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1594" n="HIAT:w" s="T456">naːda</ts>
                  <nts id="Seg_1595" n="HIAT:ip">.</nts>
                  <nts id="Seg_1596" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T463" id="Seg_1598" n="HIAT:u" s="T457">
                  <ts e="T458" id="Seg_1600" n="HIAT:w" s="T457">Hobu͡oj</ts>
                  <nts id="Seg_1601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1603" n="HIAT:w" s="T458">kajdɨbɨta</ts>
                  <nts id="Seg_1604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1606" n="HIAT:w" s="T459">purga</ts>
                  <nts id="Seg_1607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1609" n="HIAT:w" s="T460">bu͡ollagɨna</ts>
                  <nts id="Seg_1610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1612" n="HIAT:w" s="T461">tibillen</ts>
                  <nts id="Seg_1613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1615" n="HIAT:w" s="T462">kaːlaːččɨ</ts>
                  <nts id="Seg_1616" n="HIAT:ip">.</nts>
                  <nts id="Seg_1617" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T474" id="Seg_1619" n="HIAT:u" s="T463">
                  <ts e="T464" id="Seg_1621" n="HIAT:w" s="T463">Ol</ts>
                  <nts id="Seg_1622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1624" n="HIAT:w" s="T464">gɨnan</ts>
                  <nts id="Seg_1625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1627" n="HIAT:w" s="T465">baraːn</ts>
                  <nts id="Seg_1628" n="HIAT:ip">,</nts>
                  <nts id="Seg_1629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1631" n="HIAT:w" s="T466">ol</ts>
                  <nts id="Seg_1632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1634" n="HIAT:w" s="T467">tibillibitin</ts>
                  <nts id="Seg_1635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1637" n="HIAT:w" s="T468">kenne</ts>
                  <nts id="Seg_1638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1640" n="HIAT:w" s="T469">onu</ts>
                  <nts id="Seg_1641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1643" n="HIAT:w" s="T470">taba</ts>
                  <nts id="Seg_1644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1646" n="HIAT:w" s="T471">körböt</ts>
                  <nts id="Seg_1647" n="HIAT:ip">,</nts>
                  <nts id="Seg_1648" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1650" n="HIAT:w" s="T472">honon</ts>
                  <nts id="Seg_1651" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1653" n="HIAT:w" s="T473">tüher</ts>
                  <nts id="Seg_1654" n="HIAT:ip">.</nts>
                  <nts id="Seg_1655" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T482" id="Seg_1657" n="HIAT:u" s="T474">
                  <ts e="T475" id="Seg_1659" n="HIAT:w" s="T474">Ol</ts>
                  <nts id="Seg_1660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1662" n="HIAT:w" s="T475">tüstegine</ts>
                  <nts id="Seg_1663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1665" n="HIAT:w" s="T476">bi͡es-tü͡ört</ts>
                  <nts id="Seg_1666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1668" n="HIAT:w" s="T477">mʼetʼirdeːk</ts>
                  <nts id="Seg_1669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1671" n="HIAT:w" s="T478">diriŋneːk</ts>
                  <nts id="Seg_1672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1674" n="HIAT:w" s="T479">bu͡olaːččɨ</ts>
                  <nts id="Seg_1675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1677" n="HIAT:w" s="T480">bu</ts>
                  <nts id="Seg_1678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1680" n="HIAT:w" s="T481">köŋüs</ts>
                  <nts id="Seg_1681" n="HIAT:ip">.</nts>
                  <nts id="Seg_1682" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T484" id="Seg_1684" n="HIAT:u" s="T482">
                  <ts e="T483" id="Seg_1686" n="HIAT:w" s="T482">Kaːr</ts>
                  <nts id="Seg_1687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1689" n="HIAT:w" s="T483">bɨstɨbɨt</ts>
                  <nts id="Seg_1690" n="HIAT:ip">.</nts>
                  <nts id="Seg_1691" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T493" id="Seg_1693" n="HIAT:u" s="T484">
                  <ts e="T485" id="Seg_1695" n="HIAT:w" s="T484">Ol</ts>
                  <nts id="Seg_1696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1698" n="HIAT:w" s="T485">ihin</ts>
                  <nts id="Seg_1699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1701" n="HIAT:w" s="T486">onnugu</ts>
                  <nts id="Seg_1702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1704" n="HIAT:w" s="T487">bulbakka</ts>
                  <nts id="Seg_1705" n="HIAT:ip">,</nts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1708" n="HIAT:w" s="T488">taba</ts>
                  <nts id="Seg_1709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1711" n="HIAT:w" s="T489">huraga</ts>
                  <nts id="Seg_1712" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1714" n="HIAT:w" s="T490">hu͡ok</ts>
                  <nts id="Seg_1715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1717" n="HIAT:w" s="T491">hüter</ts>
                  <nts id="Seg_1718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1720" n="HIAT:w" s="T492">iti</ts>
                  <nts id="Seg_1721" n="HIAT:ip">.</nts>
                  <nts id="Seg_1722" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T505" id="Seg_1724" n="HIAT:u" s="T493">
                  <ts e="T494" id="Seg_1726" n="HIAT:w" s="T493">Onton</ts>
                  <nts id="Seg_1727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1729" n="HIAT:w" s="T494">ürekteːk</ts>
                  <nts id="Seg_1730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1732" n="HIAT:w" s="T495">hirderge</ts>
                  <nts id="Seg_1733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1735" n="HIAT:w" s="T496">bu͡ollagɨna</ts>
                  <nts id="Seg_1736" n="HIAT:ip">,</nts>
                  <nts id="Seg_1737" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1739" n="HIAT:w" s="T497">karadʼɨktaːtagɨna</ts>
                  <nts id="Seg_1740" n="HIAT:ip">,</nts>
                  <nts id="Seg_1741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1743" n="HIAT:w" s="T498">kahan</ts>
                  <nts id="Seg_1744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1746" n="HIAT:w" s="T499">da</ts>
                  <nts id="Seg_1747" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1749" n="HIAT:w" s="T500">ürekteːk</ts>
                  <nts id="Seg_1750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1752" n="HIAT:w" s="T501">hirinen</ts>
                  <nts id="Seg_1753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1755" n="HIAT:w" s="T502">bu͡olar</ts>
                  <nts id="Seg_1756" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1758" n="HIAT:w" s="T503">maršrut</ts>
                  <nts id="Seg_1759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1761" n="HIAT:w" s="T504">bu</ts>
                  <nts id="Seg_1762" n="HIAT:ip">.</nts>
                  <nts id="Seg_1763" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T520" id="Seg_1765" n="HIAT:u" s="T505">
                  <ts e="T506" id="Seg_1767" n="HIAT:w" s="T505">Kaːrɨ</ts>
                  <nts id="Seg_1768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1770" n="HIAT:w" s="T506">gɨtta</ts>
                  <nts id="Seg_1771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1773" n="HIAT:w" s="T507">kaja</ts>
                  <nts id="Seg_1774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1776" n="HIAT:w" s="T508">hir</ts>
                  <nts id="Seg_1777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1779" n="HIAT:w" s="T509">ɨkkardɨta</ts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1782" n="HIAT:w" s="T510">ataːččɨ</ts>
                  <nts id="Seg_1783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1785" n="HIAT:w" s="T511">emi͡e</ts>
                  <nts id="Seg_1786" n="HIAT:ip">,</nts>
                  <nts id="Seg_1787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1789" n="HIAT:w" s="T512">onon</ts>
                  <nts id="Seg_1790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1792" n="HIAT:w" s="T513">emi͡e</ts>
                  <nts id="Seg_1793" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1795" n="HIAT:w" s="T514">tüheːčči</ts>
                  <nts id="Seg_1796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1798" n="HIAT:w" s="T515">tugut</ts>
                  <nts id="Seg_1799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1801" n="HIAT:w" s="T516">bu͡ol</ts>
                  <nts id="Seg_1802" n="HIAT:ip">,</nts>
                  <nts id="Seg_1803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1805" n="HIAT:w" s="T517">ulakan</ts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1808" n="HIAT:w" s="T518">taba</ts>
                  <nts id="Seg_1809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1811" n="HIAT:w" s="T519">bu͡ol</ts>
                  <nts id="Seg_1812" n="HIAT:ip">.</nts>
                  <nts id="Seg_1813" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T532" id="Seg_1815" n="HIAT:u" s="T520">
                  <ts e="T521" id="Seg_1817" n="HIAT:w" s="T520">Onton</ts>
                  <nts id="Seg_1818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1820" n="HIAT:w" s="T521">bu</ts>
                  <nts id="Seg_1821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1823" n="HIAT:w" s="T522">pastuːktar</ts>
                  <nts id="Seg_1824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1825" n="HIAT:ip">(</nts>
                  <ts e="T524" id="Seg_1827" n="HIAT:w" s="T523">haːs-</ts>
                  <nts id="Seg_1828" n="HIAT:ip">)</nts>
                  <nts id="Seg_1829" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1831" n="HIAT:w" s="T524">hajɨnnara</ts>
                  <nts id="Seg_1832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1834" n="HIAT:w" s="T525">čugahaːtagɨna</ts>
                  <nts id="Seg_1835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1837" n="HIAT:w" s="T526">haːskɨlarɨgar</ts>
                  <nts id="Seg_1838" n="HIAT:ip">,</nts>
                  <nts id="Seg_1839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1841" n="HIAT:w" s="T527">uːčaktarɨn</ts>
                  <nts id="Seg_1842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1844" n="HIAT:w" s="T528">hataːn</ts>
                  <nts id="Seg_1845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_1847" n="HIAT:w" s="T529">karammattar</ts>
                  <nts id="Seg_1848" n="HIAT:ip">,</nts>
                  <nts id="Seg_1849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_1851" n="HIAT:w" s="T530">künüktetin</ts>
                  <nts id="Seg_1852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_1854" n="HIAT:w" s="T531">tonoːbottor</ts>
                  <nts id="Seg_1855" n="HIAT:ip">.</nts>
                  <nts id="Seg_1856" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T541" id="Seg_1858" n="HIAT:u" s="T532">
                  <ts e="T533" id="Seg_1860" n="HIAT:w" s="T532">Haːs</ts>
                  <nts id="Seg_1861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1863" n="HIAT:w" s="T533">künüktetin</ts>
                  <nts id="Seg_1864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_1866" n="HIAT:w" s="T534">tonoːtoktoruna</ts>
                  <nts id="Seg_1867" n="HIAT:ip">,</nts>
                  <nts id="Seg_1868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1870" n="HIAT:w" s="T535">ol</ts>
                  <nts id="Seg_1871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_1873" n="HIAT:w" s="T536">künüktetin</ts>
                  <nts id="Seg_1874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_1876" n="HIAT:w" s="T537">tonoːtoktoruna</ts>
                  <nts id="Seg_1877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1879" n="HIAT:w" s="T538">bu</ts>
                  <nts id="Seg_1880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1882" n="HIAT:w" s="T539">künükte</ts>
                  <nts id="Seg_1883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_1885" n="HIAT:w" s="T540">ohor</ts>
                  <nts id="Seg_1886" n="HIAT:ip">.</nts>
                  <nts id="Seg_1887" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T556" id="Seg_1889" n="HIAT:u" s="T541">
                  <ts e="T542" id="Seg_1891" n="HIAT:w" s="T541">Bejete</ts>
                  <nts id="Seg_1892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_1894" n="HIAT:w" s="T542">künükte</ts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_1897" n="HIAT:w" s="T543">haːs</ts>
                  <nts id="Seg_1898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_1900" n="HIAT:w" s="T544">ulaːtan</ts>
                  <nts id="Seg_1901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_1903" n="HIAT:w" s="T545">tagɨstagɨna</ts>
                  <nts id="Seg_1904" n="HIAT:ip">,</nts>
                  <nts id="Seg_1905" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_1907" n="HIAT:w" s="T546">tiriːtin</ts>
                  <nts id="Seg_1908" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_1910" n="HIAT:w" s="T547">ihitten</ts>
                  <nts id="Seg_1911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_1913" n="HIAT:w" s="T548">bejete</ts>
                  <nts id="Seg_1914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_1916" n="HIAT:w" s="T549">tullan</ts>
                  <nts id="Seg_1917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_1919" n="HIAT:w" s="T550">tühüːr</ts>
                  <nts id="Seg_1920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_1922" n="HIAT:w" s="T551">di͡eri</ts>
                  <nts id="Seg_1923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_1925" n="HIAT:w" s="T552">hɨrɨttagɨna</ts>
                  <nts id="Seg_1926" n="HIAT:ip">,</nts>
                  <nts id="Seg_1927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_1929" n="HIAT:w" s="T553">tabaga</ts>
                  <nts id="Seg_1930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_1932" n="HIAT:w" s="T554">da</ts>
                  <nts id="Seg_1933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_1935" n="HIAT:w" s="T555">kuhagan</ts>
                  <nts id="Seg_1936" n="HIAT:ip">.</nts>
                  <nts id="Seg_1937" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T565" id="Seg_1939" n="HIAT:u" s="T556">
                  <ts e="T557" id="Seg_1941" n="HIAT:w" s="T556">Horok</ts>
                  <nts id="Seg_1942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_1944" n="HIAT:w" s="T557">künükteni</ts>
                  <nts id="Seg_1945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_1947" n="HIAT:w" s="T558">bütejdiː</ts>
                  <nts id="Seg_1948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_1950" n="HIAT:w" s="T559">miːne-miːneler</ts>
                  <nts id="Seg_1951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_1953" n="HIAT:w" s="T560">taba</ts>
                  <nts id="Seg_1954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_1956" n="HIAT:w" s="T561">gi͡ene</ts>
                  <nts id="Seg_1957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_1959" n="HIAT:w" s="T562">ölör</ts>
                  <nts id="Seg_1960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_1962" n="HIAT:w" s="T563">ünere</ts>
                  <nts id="Seg_1963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_1965" n="HIAT:w" s="T564">ol</ts>
                  <nts id="Seg_1966" n="HIAT:ip">.</nts>
                  <nts id="Seg_1967" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T571" id="Seg_1969" n="HIAT:u" s="T565">
                  <ts e="T566" id="Seg_1971" n="HIAT:w" s="T565">Horok</ts>
                  <nts id="Seg_1972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_1974" n="HIAT:w" s="T566">pastuːk</ts>
                  <nts id="Seg_1975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_1977" n="HIAT:w" s="T567">tabanɨ</ts>
                  <nts id="Seg_1978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_1980" n="HIAT:w" s="T568">da</ts>
                  <nts id="Seg_1981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_1983" n="HIAT:w" s="T569">kölüne</ts>
                  <nts id="Seg_1984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_1986" n="HIAT:w" s="T570">hataːbat</ts>
                  <nts id="Seg_1987" n="HIAT:ip">.</nts>
                  <nts id="Seg_1988" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T577" id="Seg_1990" n="HIAT:u" s="T571">
                  <ts e="T572" id="Seg_1992" n="HIAT:w" s="T571">Pabudu͡oktara</ts>
                  <nts id="Seg_1993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_1995" n="HIAT:w" s="T572">bɨhɨnna</ts>
                  <nts id="Seg_1996" n="HIAT:ip">,</nts>
                  <nts id="Seg_1997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_1999" n="HIAT:w" s="T573">taba</ts>
                  <nts id="Seg_2000" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_2002" n="HIAT:w" s="T574">gi͡enen</ts>
                  <nts id="Seg_2003" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2005" n="HIAT:w" s="T575">hamagɨn</ts>
                  <nts id="Seg_2006" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_2008" n="HIAT:w" s="T576">aːllarallar</ts>
                  <nts id="Seg_2009" n="HIAT:ip">.</nts>
                  <nts id="Seg_2010" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T583" id="Seg_2012" n="HIAT:u" s="T577">
                  <ts e="T578" id="Seg_2014" n="HIAT:w" s="T577">Hamagɨn</ts>
                  <nts id="Seg_2015" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2017" n="HIAT:w" s="T578">aːllardaktarɨna</ts>
                  <nts id="Seg_2018" n="HIAT:ip">,</nts>
                  <nts id="Seg_2019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2021" n="HIAT:w" s="T579">bu</ts>
                  <nts id="Seg_2022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2024" n="HIAT:w" s="T580">hajɨn</ts>
                  <nts id="Seg_2025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2027" n="HIAT:w" s="T581">baːs</ts>
                  <nts id="Seg_2028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2030" n="HIAT:w" s="T582">bu͡olaːččɨ</ts>
                  <nts id="Seg_2031" n="HIAT:ip">.</nts>
                  <nts id="Seg_2032" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T588" id="Seg_2034" n="HIAT:u" s="T583">
                  <ts e="T584" id="Seg_2036" n="HIAT:w" s="T583">Manan</ts>
                  <nts id="Seg_2037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2039" n="HIAT:w" s="T584">taba</ts>
                  <nts id="Seg_2040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2041" n="HIAT:ip">(</nts>
                  <nts id="Seg_2042" n="HIAT:ip">(</nts>
                  <ats e="T586" id="Seg_2043" n="HIAT:non-pho" s="T585">PAUSE</ats>
                  <nts id="Seg_2044" n="HIAT:ip">)</nts>
                  <nts id="Seg_2045" n="HIAT:ip">)</nts>
                  <nts id="Seg_2046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2048" n="HIAT:w" s="T586">һutuːra</ts>
                  <nts id="Seg_2049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2051" n="HIAT:w" s="T587">iti</ts>
                  <nts id="Seg_2052" n="HIAT:ip">.</nts>
                  <nts id="Seg_2053" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T594" id="Seg_2055" n="HIAT:u" s="T588">
                  <ts e="T589" id="Seg_2057" n="HIAT:w" s="T588">Onton</ts>
                  <nts id="Seg_2058" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2060" n="HIAT:w" s="T589">kuturugun</ts>
                  <nts id="Seg_2061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_2063" n="HIAT:w" s="T590">taba</ts>
                  <nts id="Seg_2064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2066" n="HIAT:w" s="T591">gi͡enin</ts>
                  <nts id="Seg_2067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2069" n="HIAT:w" s="T592">kaja</ts>
                  <nts id="Seg_2070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2072" n="HIAT:w" s="T593">annʼallar</ts>
                  <nts id="Seg_2073" n="HIAT:ip">.</nts>
                  <nts id="Seg_2074" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T608" id="Seg_2076" n="HIAT:u" s="T594">
                  <ts e="T595" id="Seg_2078" n="HIAT:w" s="T594">Bu</ts>
                  <nts id="Seg_2079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2081" n="HIAT:w" s="T595">kuturugun</ts>
                  <nts id="Seg_2082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2084" n="HIAT:w" s="T596">kaja</ts>
                  <nts id="Seg_2085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2087" n="HIAT:w" s="T597">astaktarɨna</ts>
                  <nts id="Seg_2088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2090" n="HIAT:w" s="T598">kɨhɨn</ts>
                  <nts id="Seg_2091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2093" n="HIAT:w" s="T599">tɨmnɨːga</ts>
                  <nts id="Seg_2094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2096" n="HIAT:w" s="T600">bu</ts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2099" n="HIAT:w" s="T601">taba</ts>
                  <nts id="Seg_2100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2102" n="HIAT:w" s="T602">gi͡ene</ts>
                  <nts id="Seg_2103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2105" n="HIAT:w" s="T603">ete</ts>
                  <nts id="Seg_2106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2108" n="HIAT:w" s="T604">kaččaga</ts>
                  <nts id="Seg_2109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2111" n="HIAT:w" s="T605">da</ts>
                  <nts id="Seg_2112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2114" n="HIAT:w" s="T606">emi͡e</ts>
                  <nts id="Seg_2115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2117" n="HIAT:w" s="T607">köppöt</ts>
                  <nts id="Seg_2118" n="HIAT:ip">.</nts>
                  <nts id="Seg_2119" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T613" id="Seg_2121" n="HIAT:u" s="T608">
                  <ts e="T609" id="Seg_2123" n="HIAT:w" s="T608">Onton</ts>
                  <nts id="Seg_2124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2126" n="HIAT:w" s="T609">hajɨn</ts>
                  <nts id="Seg_2127" n="HIAT:ip">,</nts>
                  <nts id="Seg_2128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2130" n="HIAT:w" s="T610">itiː</ts>
                  <nts id="Seg_2131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2133" n="HIAT:w" s="T611">hagɨna</ts>
                  <nts id="Seg_2134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2136" n="HIAT:w" s="T612">köhöllör</ts>
                  <nts id="Seg_2137" n="HIAT:ip">.</nts>
                  <nts id="Seg_2138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T622" id="Seg_2140" n="HIAT:u" s="T613">
                  <ts e="T614" id="Seg_2142" n="HIAT:w" s="T613">Bu</ts>
                  <nts id="Seg_2143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2145" n="HIAT:w" s="T614">köstöktörüne</ts>
                  <nts id="Seg_2146" n="HIAT:ip">,</nts>
                  <nts id="Seg_2147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2149" n="HIAT:w" s="T615">itiːge</ts>
                  <nts id="Seg_2150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2152" n="HIAT:w" s="T616">köstöktörüne</ts>
                  <nts id="Seg_2153" n="HIAT:ip">,</nts>
                  <nts id="Seg_2154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2156" n="HIAT:w" s="T617">köhön</ts>
                  <nts id="Seg_2157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2159" n="HIAT:w" s="T618">ihenner</ts>
                  <nts id="Seg_2160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2162" n="HIAT:w" s="T619">ürekteːk</ts>
                  <nts id="Seg_2163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2165" n="HIAT:w" s="T620">hirderinen</ts>
                  <nts id="Seg_2166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2168" n="HIAT:w" s="T621">barallar</ts>
                  <nts id="Seg_2169" n="HIAT:ip">.</nts>
                  <nts id="Seg_2170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T628" id="Seg_2172" n="HIAT:u" s="T622">
                  <ts e="T623" id="Seg_2174" n="HIAT:w" s="T622">Bu</ts>
                  <nts id="Seg_2175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2177" n="HIAT:w" s="T623">ürekteːk</ts>
                  <nts id="Seg_2178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2180" n="HIAT:w" s="T624">ulakan</ts>
                  <nts id="Seg_2181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2183" n="HIAT:w" s="T625">kajalarga</ts>
                  <nts id="Seg_2184" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2186" n="HIAT:w" s="T626">tu͡ojdaːk</ts>
                  <nts id="Seg_2187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2189" n="HIAT:w" s="T627">bu͡olaːččɨ</ts>
                  <nts id="Seg_2190" n="HIAT:ip">.</nts>
                  <nts id="Seg_2191" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T645" id="Seg_2193" n="HIAT:u" s="T628">
                  <ts e="T629" id="Seg_2195" n="HIAT:w" s="T628">Töhö</ts>
                  <nts id="Seg_2196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2198" n="HIAT:w" s="T629">da</ts>
                  <nts id="Seg_2199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2201" n="HIAT:w" s="T630">itiːge</ts>
                  <nts id="Seg_2202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2204" n="HIAT:w" s="T631">bu</ts>
                  <nts id="Seg_2205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2207" n="HIAT:w" s="T632">tu͡ojɨ</ts>
                  <nts id="Seg_2208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2210" n="HIAT:w" s="T633">herejde</ts>
                  <nts id="Seg_2211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2213" n="HIAT:w" s="T634">da</ts>
                  <nts id="Seg_2214" n="HIAT:ip">,</nts>
                  <nts id="Seg_2215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2217" n="HIAT:w" s="T635">taba</ts>
                  <nts id="Seg_2218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2220" n="HIAT:w" s="T636">bu</ts>
                  <nts id="Seg_2221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2223" n="HIAT:w" s="T637">kajaga</ts>
                  <nts id="Seg_2224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2226" n="HIAT:w" s="T638">baraːččɨ</ts>
                  <nts id="Seg_2227" n="HIAT:ip">,</nts>
                  <nts id="Seg_2228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2230" n="HIAT:w" s="T639">köhön</ts>
                  <nts id="Seg_2231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2233" n="HIAT:w" s="T640">istekke</ts>
                  <nts id="Seg_2234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2236" n="HIAT:w" s="T641">da</ts>
                  <nts id="Seg_2237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2239" n="HIAT:w" s="T642">üːren</ts>
                  <nts id="Seg_2240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2242" n="HIAT:w" s="T643">da</ts>
                  <nts id="Seg_2243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2245" n="HIAT:w" s="T644">istekterinen</ts>
                  <nts id="Seg_2246" n="HIAT:ip">.</nts>
                  <nts id="Seg_2247" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T657" id="Seg_2249" n="HIAT:u" s="T645">
                  <ts e="T646" id="Seg_2251" n="HIAT:w" s="T645">Manɨ</ts>
                  <nts id="Seg_2252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2254" n="HIAT:w" s="T646">pastuːktar</ts>
                  <nts id="Seg_2255" n="HIAT:ip">,</nts>
                  <nts id="Seg_2256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2258" n="HIAT:w" s="T647">bu</ts>
                  <nts id="Seg_2259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2261" n="HIAT:w" s="T648">kajanɨ</ts>
                  <nts id="Seg_2262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2264" n="HIAT:w" s="T649">körbökkö</ts>
                  <nts id="Seg_2265" n="HIAT:ip">,</nts>
                  <nts id="Seg_2266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2268" n="HIAT:w" s="T650">ɨttaːn</ts>
                  <nts id="Seg_2269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2271" n="HIAT:w" s="T651">ɨllɨlar</ts>
                  <nts id="Seg_2272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2274" n="HIAT:w" s="T652">daː</ts>
                  <nts id="Seg_2275" n="HIAT:ip">,</nts>
                  <nts id="Seg_2276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2278" n="HIAT:w" s="T653">honon</ts>
                  <nts id="Seg_2279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2281" n="HIAT:w" s="T654">külse-külse</ts>
                  <nts id="Seg_2282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2284" n="HIAT:w" s="T655">aːhan</ts>
                  <nts id="Seg_2285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2287" n="HIAT:w" s="T656">kaːlallar</ts>
                  <nts id="Seg_2288" n="HIAT:ip">.</nts>
                  <nts id="Seg_2289" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T668" id="Seg_2291" n="HIAT:u" s="T657">
                  <ts e="T658" id="Seg_2293" n="HIAT:w" s="T657">Manna</ts>
                  <nts id="Seg_2294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2296" n="HIAT:w" s="T658">u͡on</ts>
                  <nts id="Seg_2297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2299" n="HIAT:w" s="T659">orduga</ts>
                  <nts id="Seg_2300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2302" n="HIAT:w" s="T660">bi͡es</ts>
                  <nts id="Seg_2303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2305" n="HIAT:w" s="T661">duː</ts>
                  <nts id="Seg_2306" n="HIAT:ip">,</nts>
                  <nts id="Seg_2307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2309" n="HIAT:w" s="T662">hüːrbe</ts>
                  <nts id="Seg_2310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2312" n="HIAT:w" s="T663">orduga</ts>
                  <nts id="Seg_2313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2315" n="HIAT:w" s="T664">bi͡es</ts>
                  <nts id="Seg_2316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2318" n="HIAT:w" s="T665">duː</ts>
                  <nts id="Seg_2319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2321" n="HIAT:w" s="T666">taba</ts>
                  <nts id="Seg_2322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2324" n="HIAT:w" s="T667">kaːlar</ts>
                  <nts id="Seg_2325" n="HIAT:ip">.</nts>
                  <nts id="Seg_2326" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T671" id="Seg_2328" n="HIAT:u" s="T668">
                  <ts e="T669" id="Seg_2330" n="HIAT:w" s="T668">Bu</ts>
                  <nts id="Seg_2331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2333" n="HIAT:w" s="T669">kaːlbɨtɨn</ts>
                  <nts id="Seg_2334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2336" n="HIAT:w" s="T670">öjdöːböttör</ts>
                  <nts id="Seg_2337" n="HIAT:ip">.</nts>
                  <nts id="Seg_2338" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T676" id="Seg_2340" n="HIAT:u" s="T671">
                  <ts e="T672" id="Seg_2342" n="HIAT:w" s="T671">Huːrka</ts>
                  <nts id="Seg_2343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2345" n="HIAT:w" s="T672">tühenner</ts>
                  <nts id="Seg_2346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2348" n="HIAT:w" s="T673">nöŋü͡ö</ts>
                  <nts id="Seg_2349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2351" n="HIAT:w" s="T674">kihi</ts>
                  <nts id="Seg_2352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2354" n="HIAT:w" s="T675">ketiːr</ts>
                  <nts id="Seg_2355" n="HIAT:ip">.</nts>
                  <nts id="Seg_2356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T690" id="Seg_2358" n="HIAT:u" s="T676">
                  <ts e="T677" id="Seg_2360" n="HIAT:w" s="T676">Maːdin</ts>
                  <nts id="Seg_2361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2363" n="HIAT:w" s="T677">bu</ts>
                  <nts id="Seg_2364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2366" n="HIAT:w" s="T678">köhölleriger</ts>
                  <nts id="Seg_2367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2369" n="HIAT:w" s="T679">keteːbit</ts>
                  <nts id="Seg_2370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2372" n="HIAT:w" s="T680">kihi</ts>
                  <nts id="Seg_2373" n="HIAT:ip">,</nts>
                  <nts id="Seg_2374" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2376" n="HIAT:w" s="T681">araj</ts>
                  <nts id="Seg_2377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2379" n="HIAT:w" s="T682">ugučaːga</ts>
                  <nts id="Seg_2380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2382" n="HIAT:w" s="T683">ol</ts>
                  <nts id="Seg_2383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2385" n="HIAT:w" s="T684">tabalarga</ts>
                  <nts id="Seg_2386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2388" n="HIAT:w" s="T685">onno</ts>
                  <nts id="Seg_2389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2391" n="HIAT:w" s="T686">kajaga</ts>
                  <nts id="Seg_2392" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2394" n="HIAT:w" s="T687">kaːlbɨt</ts>
                  <nts id="Seg_2395" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T689" id="Seg_2397" n="HIAT:w" s="T688">biːr</ts>
                  <nts id="Seg_2398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2400" n="HIAT:w" s="T689">ugučaga</ts>
                  <nts id="Seg_2401" n="HIAT:ip">.</nts>
                  <nts id="Seg_2402" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T693" id="Seg_2404" n="HIAT:u" s="T690">
                  <ts e="T691" id="Seg_2406" n="HIAT:w" s="T690">Harsɨŋŋɨtɨn</ts>
                  <nts id="Seg_2407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2409" n="HIAT:w" s="T691">köhöllör</ts>
                  <nts id="Seg_2410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2412" n="HIAT:w" s="T692">emi͡e</ts>
                  <nts id="Seg_2413" n="HIAT:ip">.</nts>
                  <nts id="Seg_2414" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T699" id="Seg_2416" n="HIAT:u" s="T693">
                  <ts e="T694" id="Seg_2418" n="HIAT:w" s="T693">Köhön</ts>
                  <nts id="Seg_2419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2421" n="HIAT:w" s="T694">tühen</ts>
                  <nts id="Seg_2422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2424" n="HIAT:w" s="T695">baraːn</ts>
                  <nts id="Seg_2425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2427" n="HIAT:w" s="T696">nöŋü͡ö</ts>
                  <nts id="Seg_2428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2430" n="HIAT:w" s="T697">kihiler</ts>
                  <nts id="Seg_2431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2433" n="HIAT:w" s="T698">ketiːller</ts>
                  <nts id="Seg_2434" n="HIAT:ip">.</nts>
                  <nts id="Seg_2435" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T710" id="Seg_2437" n="HIAT:u" s="T699">
                  <ts e="T700" id="Seg_2439" n="HIAT:w" s="T699">Maː</ts>
                  <nts id="Seg_2440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2442" n="HIAT:w" s="T700">ɨnaraː</ts>
                  <nts id="Seg_2443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2445" n="HIAT:w" s="T701">huːrka</ts>
                  <nts id="Seg_2446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T703" id="Seg_2448" n="HIAT:w" s="T702">keteːbit</ts>
                  <nts id="Seg_2449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_2451" n="HIAT:w" s="T703">artɨnnaːk</ts>
                  <nts id="Seg_2452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2454" n="HIAT:w" s="T704">kihi</ts>
                  <nts id="Seg_2455" n="HIAT:ip">,</nts>
                  <nts id="Seg_2456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2458" n="HIAT:w" s="T705">ketiː</ts>
                  <nts id="Seg_2459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2461" n="HIAT:w" s="T706">taksan</ts>
                  <nts id="Seg_2462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2464" n="HIAT:w" s="T707">uːčagɨn</ts>
                  <nts id="Seg_2465" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2467" n="HIAT:w" s="T708">oččogo</ts>
                  <nts id="Seg_2468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2470" n="HIAT:w" s="T709">hu͡oktuːr</ts>
                  <nts id="Seg_2471" n="HIAT:ip">.</nts>
                  <nts id="Seg_2472" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T713" id="Seg_2474" n="HIAT:u" s="T710">
                  <ts e="T711" id="Seg_2476" n="HIAT:w" s="T710">Harsi͡erde</ts>
                  <nts id="Seg_2477" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2479" n="HIAT:w" s="T711">kelen</ts>
                  <nts id="Seg_2480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2482" n="HIAT:w" s="T712">diːr</ts>
                  <nts id="Seg_2483" n="HIAT:ip">:</nts>
                  <nts id="Seg_2484" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T717" id="Seg_2486" n="HIAT:u" s="T713">
                  <nts id="Seg_2487" n="HIAT:ip">"</nts>
                  <ts e="T714" id="Seg_2489" n="HIAT:w" s="T713">Kaja</ts>
                  <nts id="Seg_2490" n="HIAT:ip">,</nts>
                  <nts id="Seg_2491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2493" n="HIAT:w" s="T714">mini͡ene</ts>
                  <nts id="Seg_2494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T716" id="Seg_2496" n="HIAT:w" s="T715">uːčagɨm</ts>
                  <nts id="Seg_2497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2499" n="HIAT:w" s="T716">hu͡ok</ts>
                  <nts id="Seg_2500" n="HIAT:ip">.</nts>
                  <nts id="Seg_2501" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T731" id="Seg_2503" n="HIAT:u" s="T717">
                  <ts e="T718" id="Seg_2505" n="HIAT:w" s="T717">Onno</ts>
                  <nts id="Seg_2506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2508" n="HIAT:w" s="T718">huːrka</ts>
                  <nts id="Seg_2509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2511" n="HIAT:w" s="T719">da</ts>
                  <nts id="Seg_2512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2514" n="HIAT:w" s="T720">körbötögüm</ts>
                  <nts id="Seg_2515" n="HIAT:ip">,</nts>
                  <nts id="Seg_2516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_2518" n="HIAT:w" s="T721">maːdin</ts>
                  <nts id="Seg_2519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_2521" n="HIAT:w" s="T722">köspüt</ts>
                  <nts id="Seg_2522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2524" n="HIAT:w" s="T723">huːrpar</ts>
                  <nts id="Seg_2525" n="HIAT:ip">,</nts>
                  <nts id="Seg_2526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2528" n="HIAT:w" s="T724">ɨnaraː</ts>
                  <nts id="Seg_2529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_2531" n="HIAT:w" s="T725">huːrka</ts>
                  <nts id="Seg_2532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2534" n="HIAT:w" s="T726">ketiːrber</ts>
                  <nts id="Seg_2535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T728" id="Seg_2537" n="HIAT:w" s="T727">miːne</ts>
                  <nts id="Seg_2538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2540" n="HIAT:w" s="T728">hɨldʼɨbɨt</ts>
                  <nts id="Seg_2541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2543" n="HIAT:w" s="T729">tabam</ts>
                  <nts id="Seg_2544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2546" n="HIAT:w" s="T730">ete</ts>
                  <nts id="Seg_2547" n="HIAT:ip">.</nts>
                  <nts id="Seg_2548" n="HIAT:ip">"</nts>
                  <nts id="Seg_2549" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T748" id="Seg_2551" n="HIAT:u" s="T731">
                  <nts id="Seg_2552" n="HIAT:ip">"</nts>
                  <ts e="T732" id="Seg_2554" n="HIAT:w" s="T731">Kirdik</ts>
                  <nts id="Seg_2555" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2557" n="HIAT:w" s="T732">da</ts>
                  <nts id="Seg_2558" n="HIAT:ip">,</nts>
                  <nts id="Seg_2559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2561" n="HIAT:w" s="T733">onno</ts>
                  <nts id="Seg_2562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_2564" n="HIAT:w" s="T734">huːrka</ts>
                  <nts id="Seg_2565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T736" id="Seg_2567" n="HIAT:w" s="T735">kaːlbɨt</ts>
                  <nts id="Seg_2568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_2570" n="HIAT:w" s="T736">bu͡olla</ts>
                  <nts id="Seg_2571" n="HIAT:ip">"</nts>
                  <nts id="Seg_2572" n="HIAT:ip">,</nts>
                  <nts id="Seg_2573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_2575" n="HIAT:w" s="T737">diː</ts>
                  <nts id="Seg_2576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_2578" n="HIAT:w" s="T738">diːller</ts>
                  <nts id="Seg_2579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_2581" n="HIAT:w" s="T739">ol</ts>
                  <nts id="Seg_2582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2584" n="HIAT:w" s="T740">tabanɨ</ts>
                  <nts id="Seg_2585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_2587" n="HIAT:w" s="T741">bu</ts>
                  <nts id="Seg_2588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_2590" n="HIAT:w" s="T742">huːrtarɨgar</ts>
                  <nts id="Seg_2591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_2593" n="HIAT:w" s="T743">kördüː</ts>
                  <nts id="Seg_2594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_2596" n="HIAT:w" s="T744">keleller</ts>
                  <nts id="Seg_2597" n="HIAT:ip">,</nts>
                  <nts id="Seg_2598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_2600" n="HIAT:w" s="T745">bügün</ts>
                  <nts id="Seg_2601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_2603" n="HIAT:w" s="T746">köspüt</ts>
                  <nts id="Seg_2604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_2606" n="HIAT:w" s="T747">huːrdarɨgar</ts>
                  <nts id="Seg_2607" n="HIAT:ip">.</nts>
                  <nts id="Seg_2608" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T757" id="Seg_2610" n="HIAT:u" s="T748">
                  <ts e="T749" id="Seg_2612" n="HIAT:w" s="T748">Ol</ts>
                  <nts id="Seg_2613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T750" id="Seg_2615" n="HIAT:w" s="T749">tabalarɨ</ts>
                  <nts id="Seg_2616" n="HIAT:ip">,</nts>
                  <nts id="Seg_2617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_2619" n="HIAT:w" s="T750">ɨnaraː</ts>
                  <nts id="Seg_2620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_2622" n="HIAT:w" s="T751">köskö</ts>
                  <nts id="Seg_2623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_2625" n="HIAT:w" s="T752">kaːlbɨt</ts>
                  <nts id="Seg_2626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_2628" n="HIAT:w" s="T753">tabalarɨ</ts>
                  <nts id="Seg_2629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_2631" n="HIAT:w" s="T754">betereː</ts>
                  <nts id="Seg_2632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_2634" n="HIAT:w" s="T755">huːrka</ts>
                  <nts id="Seg_2635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_2637" n="HIAT:w" s="T756">kördüːller</ts>
                  <nts id="Seg_2638" n="HIAT:ip">.</nts>
                  <nts id="Seg_2639" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T767" id="Seg_2641" n="HIAT:u" s="T757">
                  <ts e="T758" id="Seg_2643" n="HIAT:w" s="T757">Ol</ts>
                  <nts id="Seg_2644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_2646" n="HIAT:w" s="T758">ɨnaraː</ts>
                  <nts id="Seg_2647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T760" id="Seg_2649" n="HIAT:w" s="T759">huːrka</ts>
                  <nts id="Seg_2650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_2652" n="HIAT:w" s="T760">keteːbit</ts>
                  <nts id="Seg_2653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_2655" n="HIAT:w" s="T761">pastuːk</ts>
                  <nts id="Seg_2656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_2658" n="HIAT:w" s="T762">ugučaga</ts>
                  <nts id="Seg_2659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_2661" n="HIAT:w" s="T763">onno</ts>
                  <nts id="Seg_2662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_2664" n="HIAT:w" s="T764">kaːlbɨt</ts>
                  <nts id="Seg_2665" n="HIAT:ip">,</nts>
                  <nts id="Seg_2666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_2668" n="HIAT:w" s="T765">ol</ts>
                  <nts id="Seg_2669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_2671" n="HIAT:w" s="T766">kajaga</ts>
                  <nts id="Seg_2672" n="HIAT:ip">.</nts>
                  <nts id="Seg_2673" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T771" id="Seg_2675" n="HIAT:u" s="T767">
                  <ts e="T768" id="Seg_2677" n="HIAT:w" s="T767">Ol</ts>
                  <nts id="Seg_2678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_2680" n="HIAT:w" s="T768">hogotok</ts>
                  <nts id="Seg_2681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_2683" n="HIAT:w" s="T769">tabanɨ</ts>
                  <nts id="Seg_2684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T771" id="Seg_2686" n="HIAT:w" s="T770">öjdüːller</ts>
                  <nts id="Seg_2687" n="HIAT:ip">.</nts>
                  <nts id="Seg_2688" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T776" id="Seg_2690" n="HIAT:u" s="T771">
                  <ts e="T772" id="Seg_2692" n="HIAT:w" s="T771">Onno</ts>
                  <nts id="Seg_2693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_2695" n="HIAT:w" s="T772">kaːlbɨta</ts>
                  <nts id="Seg_2696" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_2698" n="HIAT:w" s="T773">u͡ontan</ts>
                  <nts id="Seg_2699" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_2701" n="HIAT:w" s="T774">taksa</ts>
                  <nts id="Seg_2702" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_2704" n="HIAT:w" s="T775">taba</ts>
                  <nts id="Seg_2705" n="HIAT:ip">.</nts>
                  <nts id="Seg_2706" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T785" id="Seg_2708" n="HIAT:u" s="T776">
                  <ts e="T777" id="Seg_2710" n="HIAT:w" s="T776">Ol</ts>
                  <nts id="Seg_2711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_2713" n="HIAT:w" s="T777">ihin</ts>
                  <nts id="Seg_2714" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_2716" n="HIAT:w" s="T778">bʼez</ts>
                  <nts id="Seg_2717" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_2719" n="HIAT:w" s="T779">vʼestʼi</ts>
                  <nts id="Seg_2720" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_2722" n="HIAT:w" s="T780">taba</ts>
                  <nts id="Seg_2723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_2725" n="HIAT:w" s="T781">huraga</ts>
                  <nts id="Seg_2726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_2728" n="HIAT:w" s="T782">hu͡ok</ts>
                  <nts id="Seg_2729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_2731" n="HIAT:w" s="T783">hütere</ts>
                  <nts id="Seg_2732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2733" n="HIAT:ip">–</nts>
                  <nts id="Seg_2734" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_2736" n="HIAT:w" s="T784">iti</ts>
                  <nts id="Seg_2737" n="HIAT:ip">.</nts>
                  <nts id="Seg_2738" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T789" id="Seg_2740" n="HIAT:u" s="T785">
                  <ts e="T786" id="Seg_2742" n="HIAT:w" s="T785">Onton</ts>
                  <nts id="Seg_2743" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_2745" n="HIAT:w" s="T786">dʼɨl</ts>
                  <nts id="Seg_2746" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_2748" n="HIAT:w" s="T787">eŋin-eŋin</ts>
                  <nts id="Seg_2749" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_2751" n="HIAT:w" s="T788">bu͡olaːččɨ</ts>
                  <nts id="Seg_2752" n="HIAT:ip">.</nts>
                  <nts id="Seg_2753" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T791" id="Seg_2755" n="HIAT:u" s="T789">
                  <ts e="T790" id="Seg_2757" n="HIAT:w" s="T789">Itiːli͡ek</ts>
                  <nts id="Seg_2758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_2760" n="HIAT:w" s="T790">bu͡olaːččɨ</ts>
                  <nts id="Seg_2761" n="HIAT:ip">.</nts>
                  <nts id="Seg_2762" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T799" id="Seg_2764" n="HIAT:u" s="T791">
                  <ts e="T792" id="Seg_2766" n="HIAT:w" s="T791">Bu</ts>
                  <nts id="Seg_2767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_2769" n="HIAT:w" s="T792">itiːge</ts>
                  <nts id="Seg_2770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_2772" n="HIAT:w" s="T793">emi͡e</ts>
                  <nts id="Seg_2773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_2775" n="HIAT:w" s="T794">anɨgɨ</ts>
                  <nts id="Seg_2776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_2778" n="HIAT:w" s="T795">pastuːktar</ts>
                  <nts id="Seg_2779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_2781" n="HIAT:w" s="T796">tabanɨ</ts>
                  <nts id="Seg_2782" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_2784" n="HIAT:w" s="T797">hataːn</ts>
                  <nts id="Seg_2785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_2787" n="HIAT:w" s="T798">karajbattar</ts>
                  <nts id="Seg_2788" n="HIAT:ip">.</nts>
                  <nts id="Seg_2789" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T801" id="Seg_2791" n="HIAT:u" s="T799">
                  <ts e="T800" id="Seg_2793" n="HIAT:w" s="T799">Karajbattara</ts>
                  <nts id="Seg_2794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T801" id="Seg_2796" n="HIAT:w" s="T800">tuːguj</ts>
                  <nts id="Seg_2797" n="HIAT:ip">?</nts>
                  <nts id="Seg_2798" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T805" id="Seg_2800" n="HIAT:u" s="T801">
                  <ts e="T802" id="Seg_2802" n="HIAT:w" s="T801">Itiːge</ts>
                  <nts id="Seg_2803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_2805" n="HIAT:w" s="T802">halgɨndʼɨt</ts>
                  <nts id="Seg_2806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_2808" n="HIAT:w" s="T803">taba</ts>
                  <nts id="Seg_2809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_2811" n="HIAT:w" s="T804">bu͡olaːččɨ</ts>
                  <nts id="Seg_2812" n="HIAT:ip">.</nts>
                  <nts id="Seg_2813" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T810" id="Seg_2815" n="HIAT:u" s="T805">
                  <ts e="T806" id="Seg_2817" n="HIAT:w" s="T805">Halgɨndʼɨt</ts>
                  <nts id="Seg_2818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_2820" n="HIAT:w" s="T806">taba</ts>
                  <nts id="Seg_2821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T808" id="Seg_2823" n="HIAT:w" s="T807">aːta</ts>
                  <nts id="Seg_2824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_2826" n="HIAT:w" s="T808">tɨ͡alɨ</ts>
                  <nts id="Seg_2827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_2829" n="HIAT:w" s="T809">batar</ts>
                  <nts id="Seg_2830" n="HIAT:ip">.</nts>
                  <nts id="Seg_2831" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T820" id="Seg_2833" n="HIAT:u" s="T810">
                  <ts e="T811" id="Seg_2835" n="HIAT:w" s="T810">Bu</ts>
                  <nts id="Seg_2836" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_2838" n="HIAT:w" s="T811">tɨ͡alɨ</ts>
                  <nts id="Seg_2839" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T813" id="Seg_2841" n="HIAT:w" s="T812">batar</ts>
                  <nts id="Seg_2842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T814" id="Seg_2844" n="HIAT:w" s="T813">tabanɨ</ts>
                  <nts id="Seg_2845" n="HIAT:ip">,</nts>
                  <nts id="Seg_2846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_2848" n="HIAT:w" s="T814">itiː</ts>
                  <nts id="Seg_2849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_2851" n="HIAT:w" s="T815">ahɨ͡ar</ts>
                  <nts id="Seg_2852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_2854" n="HIAT:w" s="T816">di͡eri</ts>
                  <nts id="Seg_2855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_2857" n="HIAT:w" s="T817">kihi</ts>
                  <nts id="Seg_2858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_2860" n="HIAT:w" s="T818">tabattan</ts>
                  <nts id="Seg_2861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_2863" n="HIAT:w" s="T819">baːjaːččɨ</ts>
                  <nts id="Seg_2864" n="HIAT:ip">.</nts>
                  <nts id="Seg_2865" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T827" id="Seg_2867" n="HIAT:u" s="T820">
                  <ts e="T821" id="Seg_2869" n="HIAT:w" s="T820">Taba</ts>
                  <nts id="Seg_2870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_2872" n="HIAT:w" s="T821">keteːtekterine</ts>
                  <nts id="Seg_2873" n="HIAT:ip">,</nts>
                  <nts id="Seg_2874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_2876" n="HIAT:w" s="T822">kihi</ts>
                  <nts id="Seg_2877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T824" id="Seg_2879" n="HIAT:w" s="T823">küːhe</ts>
                  <nts id="Seg_2880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_2882" n="HIAT:w" s="T824">tijeːččite</ts>
                  <nts id="Seg_2883" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_2885" n="HIAT:w" s="T825">hu͡ok</ts>
                  <nts id="Seg_2886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_2888" n="HIAT:w" s="T826">itiːge</ts>
                  <nts id="Seg_2889" n="HIAT:ip">.</nts>
                  <nts id="Seg_2890" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T830" id="Seg_2892" n="HIAT:u" s="T827">
                  <ts e="T828" id="Seg_2894" n="HIAT:w" s="T827">Tɨ͡alɨ</ts>
                  <nts id="Seg_2895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2896" n="HIAT:ip">(</nts>
                  <nts id="Seg_2897" n="HIAT:ip">(</nts>
                  <ats e="T929" id="Seg_2898" n="HIAT:non-pho" s="T828">…</ats>
                  <nts id="Seg_2899" n="HIAT:ip">)</nts>
                  <nts id="Seg_2900" n="HIAT:ip">)</nts>
                  <nts id="Seg_2901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_2903" n="HIAT:w" s="T929">gɨmmattar</ts>
                  <nts id="Seg_2904" n="HIAT:ip">.</nts>
                  <nts id="Seg_2905" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T853" id="Seg_2907" n="HIAT:u" s="T830">
                  <ts e="T831" id="Seg_2909" n="HIAT:w" s="T830">Harsi͡erde</ts>
                  <nts id="Seg_2910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_2912" n="HIAT:w" s="T831">egelelleriger</ts>
                  <nts id="Seg_2913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T833" id="Seg_2915" n="HIAT:w" s="T832">nöŋü͡ö</ts>
                  <nts id="Seg_2916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_2918" n="HIAT:w" s="T833">pastuːkka</ts>
                  <nts id="Seg_2919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_2921" n="HIAT:w" s="T834">tuttarallarɨgar</ts>
                  <nts id="Seg_2922" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2923" n="HIAT:ip">"</nts>
                  <ts e="T836" id="Seg_2925" n="HIAT:w" s="T835">bihigi</ts>
                  <nts id="Seg_2926" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T837" id="Seg_2928" n="HIAT:w" s="T836">kɨ͡ajɨ͡akpɨt</ts>
                  <nts id="Seg_2929" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_2931" n="HIAT:w" s="T837">hu͡oga</ts>
                  <nts id="Seg_2932" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_2934" n="HIAT:w" s="T838">et</ts>
                  <nts id="Seg_2935" n="HIAT:ip">"</nts>
                  <nts id="Seg_2936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_2938" n="HIAT:w" s="T839">di͡en</ts>
                  <nts id="Seg_2939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T841" id="Seg_2941" n="HIAT:w" s="T840">kahan</ts>
                  <nts id="Seg_2942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_2944" n="HIAT:w" s="T841">da</ts>
                  <nts id="Seg_2945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_2947" n="HIAT:w" s="T842">tɨ͡al</ts>
                  <nts id="Seg_2948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T844" id="Seg_2950" n="HIAT:w" s="T843">annɨ</ts>
                  <nts id="Seg_2951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_2953" n="HIAT:w" s="T844">di͡ek</ts>
                  <nts id="Seg_2954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_2956" n="HIAT:w" s="T845">darabiː</ts>
                  <nts id="Seg_2957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_2959" n="HIAT:w" s="T846">gɨnɨ͡aktarɨn</ts>
                  <nts id="Seg_2960" n="HIAT:ip">,</nts>
                  <nts id="Seg_2961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T848" id="Seg_2963" n="HIAT:w" s="T847">dʼi͡elere</ts>
                  <nts id="Seg_2964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T849" id="Seg_2966" n="HIAT:w" s="T848">tɨ͡al</ts>
                  <nts id="Seg_2967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_2969" n="HIAT:w" s="T849">ürdü</ts>
                  <nts id="Seg_2970" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T851" id="Seg_2972" n="HIAT:w" s="T850">bü</ts>
                  <nts id="Seg_2973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_2975" n="HIAT:w" s="T851">bu͡olar</ts>
                  <nts id="Seg_2976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_2978" n="HIAT:w" s="T852">kördük</ts>
                  <nts id="Seg_2979" n="HIAT:ip">.</nts>
                  <nts id="Seg_2980" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T865" id="Seg_2982" n="HIAT:u" s="T853">
                  <ts e="T854" id="Seg_2984" n="HIAT:w" s="T853">Taba</ts>
                  <nts id="Seg_2985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T855" id="Seg_2987" n="HIAT:w" s="T854">oččogo</ts>
                  <nts id="Seg_2988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_2990" n="HIAT:w" s="T855">kɨ͡ajbakka</ts>
                  <nts id="Seg_2991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T857" id="Seg_2993" n="HIAT:w" s="T856">tɨ͡al</ts>
                  <nts id="Seg_2994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T858" id="Seg_2996" n="HIAT:w" s="T857">utara</ts>
                  <nts id="Seg_2997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T859" id="Seg_2999" n="HIAT:w" s="T858">kellegine</ts>
                  <nts id="Seg_3000" n="HIAT:ip">,</nts>
                  <nts id="Seg_3001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_3003" n="HIAT:w" s="T859">dʼi͡etiger</ts>
                  <nts id="Seg_3004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_3006" n="HIAT:w" s="T860">kepterdegine</ts>
                  <nts id="Seg_3007" n="HIAT:ip">,</nts>
                  <nts id="Seg_3008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T862" id="Seg_3010" n="HIAT:w" s="T861">dʼi͡eteːgilere</ts>
                  <nts id="Seg_3011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T863" id="Seg_3013" n="HIAT:w" s="T862">turannar</ts>
                  <nts id="Seg_3014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864" id="Seg_3016" n="HIAT:w" s="T863">manna</ts>
                  <nts id="Seg_3017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T865" id="Seg_3019" n="HIAT:w" s="T864">kömölöhönnör</ts>
                  <nts id="Seg_3020" n="HIAT:ip">.</nts>
                  <nts id="Seg_3021" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T868" id="Seg_3023" n="HIAT:u" s="T865">
                  <ts e="T866" id="Seg_3025" n="HIAT:w" s="T865">Oččogo</ts>
                  <nts id="Seg_3026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T867" id="Seg_3028" n="HIAT:w" s="T866">kɨ͡ajallar</ts>
                  <nts id="Seg_3029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T868" id="Seg_3031" n="HIAT:w" s="T867">tabanɨ</ts>
                  <nts id="Seg_3032" n="HIAT:ip">.</nts>
                  <nts id="Seg_3033" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T888" id="Seg_3035" n="HIAT:u" s="T868">
                  <ts e="T869" id="Seg_3037" n="HIAT:w" s="T868">Ol</ts>
                  <nts id="Seg_3038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T870" id="Seg_3040" n="HIAT:w" s="T869">kennine</ts>
                  <nts id="Seg_3041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T871" id="Seg_3043" n="HIAT:w" s="T870">giniler</ts>
                  <nts id="Seg_3044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_3046" n="HIAT:w" s="T871">haːskɨttan</ts>
                  <nts id="Seg_3047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873" id="Seg_3049" n="HIAT:w" s="T872">itiː</ts>
                  <nts id="Seg_3050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874" id="Seg_3052" n="HIAT:w" s="T873">tühe</ts>
                  <nts id="Seg_3053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T875" id="Seg_3055" n="HIAT:w" s="T874">iligine</ts>
                  <nts id="Seg_3056" n="HIAT:ip">,</nts>
                  <nts id="Seg_3057" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_3059" n="HIAT:w" s="T875">haŋardɨː</ts>
                  <nts id="Seg_3060" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T877" id="Seg_3062" n="HIAT:w" s="T876">itiː</ts>
                  <nts id="Seg_3063" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878" id="Seg_3065" n="HIAT:w" s="T877">tüheriger</ts>
                  <nts id="Seg_3066" n="HIAT:ip">,</nts>
                  <nts id="Seg_3067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T879" id="Seg_3069" n="HIAT:w" s="T878">taba</ts>
                  <nts id="Seg_3070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T880" id="Seg_3072" n="HIAT:w" s="T879">itiːrgiːriger</ts>
                  <nts id="Seg_3073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T881" id="Seg_3075" n="HIAT:w" s="T880">buru͡oga</ts>
                  <nts id="Seg_3076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T882" id="Seg_3078" n="HIAT:w" s="T881">tabanɨ</ts>
                  <nts id="Seg_3079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T883" id="Seg_3081" n="HIAT:w" s="T882">ü͡öreti͡ekterin</ts>
                  <nts id="Seg_3082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T884" id="Seg_3084" n="HIAT:w" s="T883">nada</ts>
                  <nts id="Seg_3085" n="HIAT:ip">,</nts>
                  <nts id="Seg_3086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885" id="Seg_3088" n="HIAT:w" s="T884">ulakan</ts>
                  <nts id="Seg_3089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T886" id="Seg_3091" n="HIAT:w" s="T885">itiː</ts>
                  <nts id="Seg_3092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T887" id="Seg_3094" n="HIAT:w" s="T886">tühe</ts>
                  <nts id="Seg_3095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T888" id="Seg_3097" n="HIAT:w" s="T887">iligine</ts>
                  <nts id="Seg_3098" n="HIAT:ip">.</nts>
                  <nts id="Seg_3099" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T896" id="Seg_3101" n="HIAT:u" s="T888">
                  <ts e="T889" id="Seg_3103" n="HIAT:w" s="T888">Buru͡onu</ts>
                  <nts id="Seg_3104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890" id="Seg_3106" n="HIAT:w" s="T889">kördö</ts>
                  <nts id="Seg_3107" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T891" id="Seg_3109" n="HIAT:w" s="T890">da</ts>
                  <nts id="Seg_3110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T892" id="Seg_3112" n="HIAT:w" s="T891">töhö</ts>
                  <nts id="Seg_3113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T893" id="Seg_3115" n="HIAT:w" s="T892">daː</ts>
                  <nts id="Seg_3116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T894" id="Seg_3118" n="HIAT:w" s="T893">itiːge</ts>
                  <nts id="Seg_3119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T896" id="Seg_3121" n="HIAT:w" s="T894">taba</ts>
                  <nts id="Seg_3122" n="HIAT:ip">…</nts>
                  <nts id="Seg_3123" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T903" id="Seg_3125" n="HIAT:u" s="T896">
                  <ts e="T897" id="Seg_3127" n="HIAT:w" s="T896">Dʼi͡eteːgilere</ts>
                  <nts id="Seg_3128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898" id="Seg_3130" n="HIAT:w" s="T897">itiːge</ts>
                  <nts id="Seg_3131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T899" id="Seg_3133" n="HIAT:w" s="T898">pastuːktar</ts>
                  <nts id="Seg_3134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T900" id="Seg_3136" n="HIAT:w" s="T899">ketiː</ts>
                  <nts id="Seg_3137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T901" id="Seg_3139" n="HIAT:w" s="T900">baraːrɨlar</ts>
                  <nts id="Seg_3140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T902" id="Seg_3142" n="HIAT:w" s="T901">haŋarɨ͡ak</ts>
                  <nts id="Seg_3143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T903" id="Seg_3145" n="HIAT:w" s="T902">tustaːktar</ts>
                  <nts id="Seg_3146" n="HIAT:ip">:</nts>
                  <nts id="Seg_3147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T909" id="Seg_3149" n="HIAT:u" s="T903">
                  <nts id="Seg_3150" n="HIAT:ip">"</nts>
                  <ts e="T904" id="Seg_3152" n="HIAT:w" s="T903">Kaja</ts>
                  <nts id="Seg_3153" n="HIAT:ip">,</nts>
                  <nts id="Seg_3154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T905" id="Seg_3156" n="HIAT:w" s="T904">itiː</ts>
                  <nts id="Seg_3157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T906" id="Seg_3159" n="HIAT:w" s="T905">bu͡ollagɨna</ts>
                  <nts id="Seg_3160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T907" id="Seg_3162" n="HIAT:w" s="T906">dʼe</ts>
                  <nts id="Seg_3163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T908" id="Seg_3165" n="HIAT:w" s="T907">kečehen</ts>
                  <nts id="Seg_3166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909" id="Seg_3168" n="HIAT:w" s="T908">utujuŋ</ts>
                  <nts id="Seg_3169" n="HIAT:ip">.</nts>
                  <nts id="Seg_3170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T912" id="Seg_3172" n="HIAT:u" s="T909">
                  <ts e="T910" id="Seg_3174" n="HIAT:w" s="T909">Buru͡olataːrɨŋ</ts>
                  <nts id="Seg_3175" n="HIAT:ip">,</nts>
                  <nts id="Seg_3176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T911" id="Seg_3178" n="HIAT:w" s="T910">otuː</ts>
                  <nts id="Seg_3179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T912" id="Seg_3181" n="HIAT:w" s="T911">ottoːruŋ</ts>
                  <nts id="Seg_3182" n="HIAT:ip">.</nts>
                  <nts id="Seg_3183" n="HIAT:ip">"</nts>
                  <nts id="Seg_3184" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T917" id="Seg_3186" n="HIAT:u" s="T912">
                  <ts e="T913" id="Seg_3188" n="HIAT:w" s="T912">Itiː</ts>
                  <nts id="Seg_3189" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T914" id="Seg_3191" n="HIAT:w" s="T913">bu͡ollagɨna</ts>
                  <nts id="Seg_3192" n="HIAT:ip">,</nts>
                  <nts id="Seg_3193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T915" id="Seg_3195" n="HIAT:w" s="T914">bubaːt</ts>
                  <nts id="Seg_3196" n="HIAT:ip">,</nts>
                  <nts id="Seg_3197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T916" id="Seg_3199" n="HIAT:w" s="T915">kɨ͡ajɨ͡akpɨt</ts>
                  <nts id="Seg_3200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T917" id="Seg_3202" n="HIAT:w" s="T916">hu͡oga</ts>
                  <nts id="Seg_3203" n="HIAT:ip">.</nts>
                  <nts id="Seg_3204" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T922" id="Seg_3206" n="HIAT:u" s="T917">
                  <ts e="T918" id="Seg_3208" n="HIAT:w" s="T917">Kajtak</ts>
                  <nts id="Seg_3209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T919" id="Seg_3211" n="HIAT:w" s="T918">ere</ts>
                  <nts id="Seg_3212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T920" id="Seg_3214" n="HIAT:w" s="T919">itiː</ts>
                  <nts id="Seg_3215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T921" id="Seg_3217" n="HIAT:w" s="T920">bu͡olan</ts>
                  <nts id="Seg_3218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T922" id="Seg_3220" n="HIAT:w" s="T921">erer</ts>
                  <nts id="Seg_3221" n="HIAT:ip">.</nts>
                  <nts id="Seg_3222" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T928" id="Seg_3224" n="HIAT:u" s="T922">
                  <ts e="T923" id="Seg_3226" n="HIAT:w" s="T922">Ol</ts>
                  <nts id="Seg_3227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T924" id="Seg_3229" n="HIAT:w" s="T923">kördük</ts>
                  <nts id="Seg_3230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T925" id="Seg_3232" n="HIAT:w" s="T924">karajaːččɨ</ts>
                  <nts id="Seg_3233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T926" id="Seg_3235" n="HIAT:w" s="T925">etibit</ts>
                  <nts id="Seg_3236" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T927" id="Seg_3238" n="HIAT:w" s="T926">bihigi</ts>
                  <nts id="Seg_3239" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T928" id="Seg_3241" n="HIAT:w" s="T927">tabanɨ</ts>
                  <nts id="Seg_3242" n="HIAT:ip">.</nts>
                  <nts id="Seg_3243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T928" id="Seg_3244" n="sc" s="T0">
               <ts e="T1" id="Seg_3246" n="e" s="T0">Dʼe </ts>
               <ts e="T2" id="Seg_3248" n="e" s="T1">min </ts>
               <ts e="T3" id="Seg_3250" n="e" s="T2">tabahɨttarga </ts>
               <ts e="T4" id="Seg_3252" n="e" s="T3">ihilleteːri </ts>
               <ts e="T5" id="Seg_3254" n="e" s="T4">haŋarabɨn. </ts>
               <ts e="T6" id="Seg_3256" n="e" s="T5">Min </ts>
               <ts e="T7" id="Seg_3258" n="e" s="T6">ügüstük </ts>
               <ts e="T8" id="Seg_3260" n="e" s="T7">üleleːbit </ts>
               <ts e="T9" id="Seg_3262" n="e" s="T8">kihibin </ts>
               <ts e="T10" id="Seg_3264" n="e" s="T9">tabaga, </ts>
               <ts e="T11" id="Seg_3266" n="e" s="T10">urut. </ts>
               <ts e="T12" id="Seg_3268" n="e" s="T11">Bu </ts>
               <ts e="T13" id="Seg_3270" n="e" s="T12">taba </ts>
               <ts e="T14" id="Seg_3272" n="e" s="T13">törüːrün </ts>
               <ts e="T15" id="Seg_3274" n="e" s="T14">tuhunan </ts>
               <ts e="T16" id="Seg_3276" n="e" s="T15">haŋarɨ͡am </ts>
               <ts e="T17" id="Seg_3278" n="e" s="T16">anɨ. </ts>
               <ts e="T18" id="Seg_3280" n="e" s="T17">Kannɨk </ts>
               <ts e="T19" id="Seg_3282" n="e" s="T18">da </ts>
               <ts e="T20" id="Seg_3284" n="e" s="T19">otto </ts>
               <ts e="T21" id="Seg_3286" n="e" s="T20">törüːrütten </ts>
               <ts e="T22" id="Seg_3288" n="e" s="T21">bu͡olar </ts>
               <ts e="T23" id="Seg_3290" n="e" s="T22">bu͡o, </ts>
               <ts e="T24" id="Seg_3292" n="e" s="T23">taba </ts>
               <ts e="T25" id="Seg_3294" n="e" s="T24">da </ts>
               <ts e="T26" id="Seg_3296" n="e" s="T25">ü͡ösküːre, </ts>
               <ts e="T27" id="Seg_3298" n="e" s="T26">kihi </ts>
               <ts e="T28" id="Seg_3300" n="e" s="T27">de. </ts>
               <ts e="T29" id="Seg_3302" n="e" s="T28">Onon </ts>
               <ts e="T30" id="Seg_3304" n="e" s="T29">min </ts>
               <ts e="T31" id="Seg_3306" n="e" s="T30">haŋarabɨn </ts>
               <ts e="T32" id="Seg_3308" n="e" s="T31">dʼe </ts>
               <ts e="T33" id="Seg_3310" n="e" s="T32">bugurduk </ts>
               <ts e="T34" id="Seg_3312" n="e" s="T33">taba </ts>
               <ts e="T35" id="Seg_3314" n="e" s="T34">törüːrüger. </ts>
               <ts e="T36" id="Seg_3316" n="e" s="T35">Pastuːxtarga </ts>
               <ts e="T37" id="Seg_3318" n="e" s="T36">ihilleteːri. </ts>
               <ts e="T38" id="Seg_3320" n="e" s="T37">Min </ts>
               <ts e="T39" id="Seg_3322" n="e" s="T38">haŋararɨm </ts>
               <ts e="T40" id="Seg_3324" n="e" s="T39">dʼe </ts>
               <ts e="T41" id="Seg_3326" n="e" s="T40">bugurdukkaːn. </ts>
               <ts e="T42" id="Seg_3328" n="e" s="T41">Anɨ </ts>
               <ts e="T43" id="Seg_3330" n="e" s="T42">bu </ts>
               <ts e="T44" id="Seg_3332" n="e" s="T43">tabanɨ </ts>
               <ts e="T45" id="Seg_3334" n="e" s="T44">karajallar. </ts>
               <ts e="T46" id="Seg_3336" n="e" s="T45">Min </ts>
               <ts e="T47" id="Seg_3338" n="e" s="T46">urut </ts>
               <ts e="T48" id="Seg_3340" n="e" s="T47">karaja </ts>
               <ts e="T49" id="Seg_3342" n="e" s="T48">hɨldʼar </ts>
               <ts e="T50" id="Seg_3344" n="e" s="T49">erdekpine, </ts>
               <ts e="T51" id="Seg_3346" n="e" s="T50">taba </ts>
               <ts e="T52" id="Seg_3348" n="e" s="T51">töröːtögüne, </ts>
               <ts e="T53" id="Seg_3350" n="e" s="T52">ügüs </ts>
               <ts e="T54" id="Seg_3352" n="e" s="T53">prʼičʼineleːk </ts>
               <ts e="T55" id="Seg_3354" n="e" s="T54">bu͡olar. </ts>
               <ts e="T56" id="Seg_3356" n="e" s="T55">Taba </ts>
               <ts e="T57" id="Seg_3358" n="e" s="T56">törüːrün, </ts>
               <ts e="T58" id="Seg_3360" n="e" s="T57">pastuːx </ts>
               <ts e="T59" id="Seg_3362" n="e" s="T58">erdekpine, </ts>
               <ts e="T60" id="Seg_3364" n="e" s="T59">ketiː </ts>
               <ts e="T61" id="Seg_3366" n="e" s="T60">hɨldʼammɨn </ts>
               <ts e="T62" id="Seg_3368" n="e" s="T61">kerijeːčči </ts>
               <ts e="T63" id="Seg_3370" n="e" s="T62">etim. </ts>
               <ts e="T64" id="Seg_3372" n="e" s="T63">Taba </ts>
               <ts e="T65" id="Seg_3374" n="e" s="T64">töröːn, </ts>
               <ts e="T66" id="Seg_3376" n="e" s="T65">ogotun </ts>
               <ts e="T67" id="Seg_3378" n="e" s="T66">tüherdegine </ts>
               <ts e="T68" id="Seg_3380" n="e" s="T67">eŋin-eŋinnik </ts>
               <ts e="T69" id="Seg_3382" n="e" s="T68">törüːr </ts>
               <ts e="T70" id="Seg_3384" n="e" s="T69">taba. </ts>
               <ts e="T71" id="Seg_3386" n="e" s="T70">Horok </ts>
               <ts e="T72" id="Seg_3388" n="e" s="T71">taba </ts>
               <ts e="T73" id="Seg_3390" n="e" s="T72">tuguta, </ts>
               <ts e="T74" id="Seg_3392" n="e" s="T73">töröːn </ts>
               <ts e="T75" id="Seg_3394" n="e" s="T74">baraːn, </ts>
               <ts e="T76" id="Seg_3396" n="e" s="T75">tebi͡elene </ts>
               <ts e="T77" id="Seg_3398" n="e" s="T76">hɨtaːččɨ, </ts>
               <ts e="T78" id="Seg_3400" n="e" s="T77">koton </ts>
               <ts e="T79" id="Seg_3402" n="e" s="T78">turbakka. </ts>
               <ts e="T80" id="Seg_3404" n="e" s="T79">Inʼetin </ts>
               <ts e="T81" id="Seg_3406" n="e" s="T80">kördökkö </ts>
               <ts e="T82" id="Seg_3408" n="e" s="T81">emis </ts>
               <ts e="T83" id="Seg_3410" n="e" s="T82">bagaj, </ts>
               <ts e="T84" id="Seg_3412" n="e" s="T83">tugutun </ts>
               <ts e="T85" id="Seg_3414" n="e" s="T84">kördökkö </ts>
               <ts e="T86" id="Seg_3416" n="e" s="T85">ulakan </ts>
               <ts e="T87" id="Seg_3418" n="e" s="T86">bagaj </ts>
               <ts e="T88" id="Seg_3420" n="e" s="T87">bu͡olar. </ts>
               <ts e="T89" id="Seg_3422" n="e" s="T88">Bu </ts>
               <ts e="T90" id="Seg_3424" n="e" s="T89">tebi͡elene </ts>
               <ts e="T91" id="Seg_3426" n="e" s="T90">hɨtarɨ </ts>
               <ts e="T92" id="Seg_3428" n="e" s="T91">ergičiti͡ekke </ts>
               <ts e="T93" id="Seg_3430" n="e" s="T92">naːda. </ts>
               <ts e="T94" id="Seg_3432" n="e" s="T93">Ergičiten </ts>
               <ts e="T95" id="Seg_3434" n="e" s="T94">kördökkö – </ts>
               <ts e="T96" id="Seg_3436" n="e" s="T95">tuːgu </ts>
               <ts e="T97" id="Seg_3438" n="e" s="T96">gɨnan </ts>
               <ts e="T98" id="Seg_3440" n="e" s="T97">togo </ts>
               <ts e="T99" id="Seg_3442" n="e" s="T98">turbat </ts>
               <ts e="T100" id="Seg_3444" n="e" s="T99">di͡en </ts>
               <ts e="T101" id="Seg_3446" n="e" s="T100">manɨ </ts>
               <ts e="T102" id="Seg_3448" n="e" s="T101">ɨbaktɨ͡akka, </ts>
               <ts e="T103" id="Seg_3450" n="e" s="T102">bɨ͡arɨn </ts>
               <ts e="T104" id="Seg_3452" n="e" s="T103">ɨbaktɨ͡akka. </ts>
               <ts e="T105" id="Seg_3454" n="e" s="T104">Taba </ts>
               <ts e="T106" id="Seg_3456" n="e" s="T105">töröːtögüne, </ts>
               <ts e="T107" id="Seg_3458" n="e" s="T106">tugut </ts>
               <ts e="T108" id="Seg_3460" n="e" s="T107">gi͡ene </ts>
               <ts e="T109" id="Seg_3462" n="e" s="T108">kiːnneːk </ts>
               <ts e="T110" id="Seg_3464" n="e" s="T109">bu͡olaːččɨ. </ts>
               <ts e="T111" id="Seg_3466" n="e" s="T110">Bu </ts>
               <ts e="T112" id="Seg_3468" n="e" s="T111">kiːnin </ts>
               <ts e="T113" id="Seg_3470" n="e" s="T112">horok </ts>
               <ts e="T114" id="Seg_3472" n="e" s="T113">taba </ts>
               <ts e="T115" id="Seg_3474" n="e" s="T114">hubujaːččɨ. </ts>
               <ts e="T116" id="Seg_3476" n="e" s="T115">Bu </ts>
               <ts e="T117" id="Seg_3478" n="e" s="T116">kiːne </ts>
               <ts e="T118" id="Seg_3480" n="e" s="T117">hubulunnagɨna </ts>
               <ts e="T119" id="Seg_3482" n="e" s="T118">bunan, </ts>
               <ts e="T120" id="Seg_3484" n="e" s="T119">ihitten, </ts>
               <ts e="T121" id="Seg_3486" n="e" s="T120">huptu </ts>
               <ts e="T122" id="Seg_3488" n="e" s="T121">üːt </ts>
               <ts e="T123" id="Seg_3490" n="e" s="T122">bu͡olar, </ts>
               <ts e="T124" id="Seg_3492" n="e" s="T123">onon </ts>
               <ts e="T125" id="Seg_3494" n="e" s="T124">tɨːna </ts>
               <ts e="T126" id="Seg_3496" n="e" s="T125">taksar. </ts>
               <ts e="T127" id="Seg_3498" n="e" s="T126">Mu͡otunan </ts>
               <ts e="T128" id="Seg_3500" n="e" s="T127">kam </ts>
               <ts e="T129" id="Seg_3502" n="e" s="T128">baːjdakka, </ts>
               <ts e="T130" id="Seg_3504" n="e" s="T129">bu </ts>
               <ts e="T131" id="Seg_3506" n="e" s="T130">taba, </ts>
               <ts e="T132" id="Seg_3508" n="e" s="T131">bu </ts>
               <ts e="T133" id="Seg_3510" n="e" s="T132">tugut, </ts>
               <ts e="T134" id="Seg_3512" n="e" s="T133">turan </ts>
               <ts e="T135" id="Seg_3514" n="e" s="T134">keleːčči. </ts>
               <ts e="T136" id="Seg_3516" n="e" s="T135">Ol </ts>
               <ts e="T137" id="Seg_3518" n="e" s="T136">aːta </ts>
               <ts e="T138" id="Seg_3520" n="e" s="T137">"kiːnin </ts>
               <ts e="T139" id="Seg_3522" n="e" s="T138">hubujar". </ts>
               <ts e="T140" id="Seg_3524" n="e" s="T139">Iti </ts>
               <ts e="T141" id="Seg_3526" n="e" s="T140">bu͡olar </ts>
               <ts e="T142" id="Seg_3528" n="e" s="T141">taba </ts>
               <ts e="T143" id="Seg_3530" n="e" s="T142">gi͡ene, </ts>
               <ts e="T144" id="Seg_3532" n="e" s="T143">tugutun </ts>
               <ts e="T145" id="Seg_3534" n="e" s="T144">gi͡ene, </ts>
               <ts e="T146" id="Seg_3536" n="e" s="T145">prʼičʼinete </ts>
               <ts e="T147" id="Seg_3538" n="e" s="T146">töröːtögüne. </ts>
               <ts e="T148" id="Seg_3540" n="e" s="T147">Onton </ts>
               <ts e="T149" id="Seg_3542" n="e" s="T148">biːr </ts>
               <ts e="T150" id="Seg_3544" n="e" s="T149">prʼičʼineleːk – </ts>
               <ts e="T151" id="Seg_3546" n="e" s="T150">slabaj. </ts>
               <ts e="T152" id="Seg_3548" n="e" s="T151">Taba </ts>
               <ts e="T153" id="Seg_3550" n="e" s="T152">törüːr, </ts>
               <ts e="T154" id="Seg_3552" n="e" s="T153">torugan </ts>
               <ts e="T155" id="Seg_3554" n="e" s="T154">inʼete. </ts>
               <ts e="T156" id="Seg_3556" n="e" s="T155">Bu </ts>
               <ts e="T157" id="Seg_3558" n="e" s="T156">tuguta </ts>
               <ts e="T158" id="Seg_3560" n="e" s="T157">töröːtögüne </ts>
               <ts e="T159" id="Seg_3562" n="e" s="T158">dʼapču </ts>
               <ts e="T160" id="Seg_3564" n="e" s="T159">di͡en </ts>
               <ts e="T161" id="Seg_3566" n="e" s="T160">aːttanaːččɨ. </ts>
               <ts e="T162" id="Seg_3568" n="e" s="T161">Emi͡e </ts>
               <ts e="T163" id="Seg_3570" n="e" s="T162">tuguta </ts>
               <ts e="T164" id="Seg_3572" n="e" s="T163">turaːččɨta </ts>
               <ts e="T165" id="Seg_3574" n="e" s="T164">hu͡ok. </ts>
               <ts e="T166" id="Seg_3576" n="e" s="T165">Bu </ts>
               <ts e="T167" id="Seg_3578" n="e" s="T166">tugutu </ts>
               <ts e="T168" id="Seg_3580" n="e" s="T167">ergičiten </ts>
               <ts e="T169" id="Seg_3582" n="e" s="T168">kördökkö, </ts>
               <ts e="T170" id="Seg_3584" n="e" s="T169">amattan </ts>
               <ts e="T171" id="Seg_3586" n="e" s="T170">slabaj </ts>
               <ts e="T172" id="Seg_3588" n="e" s="T171">bagaj </ts>
               <ts e="T173" id="Seg_3590" n="e" s="T172">bu͡olar, </ts>
               <ts e="T174" id="Seg_3592" n="e" s="T173">bu </ts>
               <ts e="T175" id="Seg_3594" n="e" s="T174">tugut </ts>
               <ts e="T176" id="Seg_3596" n="e" s="T175">tebi͡elene </ts>
               <ts e="T177" id="Seg_3598" n="e" s="T176">agaj </ts>
               <ts e="T178" id="Seg_3600" n="e" s="T177">hɨtaːččɨ. </ts>
               <ts e="T179" id="Seg_3602" n="e" s="T178">Bu </ts>
               <ts e="T180" id="Seg_3604" n="e" s="T179">tugutu </ts>
               <ts e="T181" id="Seg_3606" n="e" s="T180">ittenne </ts>
               <ts e="T182" id="Seg_3608" n="e" s="T181">tutan </ts>
               <ts e="T183" id="Seg_3610" n="e" s="T182">baraːn </ts>
               <ts e="T184" id="Seg_3612" n="e" s="T183">tü͡öhün </ts>
               <ts e="T185" id="Seg_3614" n="e" s="T184">imerijdekke, </ts>
               <ts e="T186" id="Seg_3616" n="e" s="T185">oččogo </ts>
               <ts e="T187" id="Seg_3618" n="e" s="T186">bu </ts>
               <ts e="T188" id="Seg_3620" n="e" s="T187">tü͡öhe </ts>
               <ts e="T189" id="Seg_3622" n="e" s="T188">Dʼapču </ts>
               <ts e="T190" id="Seg_3624" n="e" s="T189">gi͡ene, </ts>
               <ts e="T191" id="Seg_3626" n="e" s="T190">kahan </ts>
               <ts e="T192" id="Seg_3628" n="e" s="T191">da </ts>
               <ts e="T193" id="Seg_3630" n="e" s="T192">tugut </ts>
               <ts e="T194" id="Seg_3632" n="e" s="T193">töröːtögüne, </ts>
               <ts e="T195" id="Seg_3634" n="e" s="T194">oŋu͡oga </ts>
               <ts e="T196" id="Seg_3636" n="e" s="T195">oŋurgas </ts>
               <ts e="T197" id="Seg_3638" n="e" s="T196">bu͡olar. </ts>
               <ts e="T198" id="Seg_3640" n="e" s="T197">Bunɨ </ts>
               <ts e="T199" id="Seg_3642" n="e" s="T198">tü͡öhün </ts>
               <ts e="T200" id="Seg_3644" n="e" s="T199">imerijdekke, </ts>
               <ts e="T201" id="Seg_3646" n="e" s="T200">tura-tura </ts>
               <ts e="T202" id="Seg_3648" n="e" s="T201">tühe </ts>
               <ts e="T203" id="Seg_3650" n="e" s="T202">hɨldʼaːččɨ, </ts>
               <ts e="T204" id="Seg_3652" n="e" s="T203">tɨːnnagɨna. </ts>
               <ts e="T205" id="Seg_3654" n="e" s="T204">Manɨ </ts>
               <ts e="T206" id="Seg_3656" n="e" s="T205">inʼetin </ts>
               <ts e="T207" id="Seg_3658" n="e" s="T206">tutan </ts>
               <ts e="T208" id="Seg_3660" n="e" s="T207">baraːn, </ts>
               <ts e="T209" id="Seg_3662" n="e" s="T208">inʼetin </ts>
               <ts e="T210" id="Seg_3664" n="e" s="T209">emneri͡ekke </ts>
               <ts e="T211" id="Seg_3666" n="e" s="T210">naːda, </ts>
               <ts e="T212" id="Seg_3668" n="e" s="T211">bu </ts>
               <ts e="T213" id="Seg_3670" n="e" s="T212">tugut </ts>
               <ts e="T214" id="Seg_3672" n="e" s="T213">tü͡öhün </ts>
               <ts e="T215" id="Seg_3674" n="e" s="T214">kimi </ts>
               <ts e="T216" id="Seg_3676" n="e" s="T215">ilbijbitin </ts>
               <ts e="T217" id="Seg_3678" n="e" s="T216">kenne </ts>
               <ts e="T218" id="Seg_3680" n="e" s="T217">koton </ts>
               <ts e="T219" id="Seg_3682" n="e" s="T218">emeːččite </ts>
               <ts e="T220" id="Seg_3684" n="e" s="T219">hu͡ok. </ts>
               <ts e="T221" id="Seg_3686" n="e" s="T220">Manɨ </ts>
               <ts e="T222" id="Seg_3688" n="e" s="T221">inʼete </ts>
               <ts e="T223" id="Seg_3690" n="e" s="T222">emije </ts>
               <ts e="T224" id="Seg_3692" n="e" s="T223">ɨ͡astaːk </ts>
               <ts e="T225" id="Seg_3694" n="e" s="T224">bu͡olla, </ts>
               <ts e="T226" id="Seg_3696" n="e" s="T225">lampagar. </ts>
               <ts e="T227" id="Seg_3698" n="e" s="T226">Bu </ts>
               <ts e="T228" id="Seg_3700" n="e" s="T227">ɨ͡ahɨ </ts>
               <ts e="T229" id="Seg_3702" n="e" s="T228">ɨgaːččɨ </ts>
               <ts e="T230" id="Seg_3704" n="e" s="T229">kihi. </ts>
               <ts e="T231" id="Seg_3706" n="e" s="T230">ɨgan </ts>
               <ts e="T232" id="Seg_3708" n="e" s="T231">baraːn, </ts>
               <ts e="T233" id="Seg_3710" n="e" s="T232">ɨbagas </ts>
               <ts e="T234" id="Seg_3712" n="e" s="T233">emije </ts>
               <ts e="T235" id="Seg_3714" n="e" s="T234">kellegine, </ts>
               <ts e="T236" id="Seg_3716" n="e" s="T235">oččogo </ts>
               <ts e="T237" id="Seg_3718" n="e" s="T236">emnereːčči, </ts>
               <ts e="T238" id="Seg_3720" n="e" s="T237">oččogo </ts>
               <ts e="T239" id="Seg_3722" n="e" s="T238">emnerdegine, </ts>
               <ts e="T240" id="Seg_3724" n="e" s="T239">bu </ts>
               <ts e="T241" id="Seg_3726" n="e" s="T240">tugut </ts>
               <ts e="T242" id="Seg_3728" n="e" s="T241">kihi </ts>
               <ts e="T243" id="Seg_3730" n="e" s="T242">bu͡olar. </ts>
               <ts e="T244" id="Seg_3732" n="e" s="T243">Pastuːktar </ts>
               <ts e="T245" id="Seg_3734" n="e" s="T244">köhöllör. </ts>
               <ts e="T246" id="Seg_3736" n="e" s="T245">Ügüs </ts>
               <ts e="T247" id="Seg_3738" n="e" s="T246">taba </ts>
               <ts e="T248" id="Seg_3740" n="e" s="T247">töröːtögüne </ts>
               <ts e="T249" id="Seg_3742" n="e" s="T248">kahan </ts>
               <ts e="T250" id="Seg_3744" n="e" s="T249">da </ts>
               <ts e="T251" id="Seg_3746" n="e" s="T250">törüːlik </ts>
               <ts e="T252" id="Seg_3748" n="e" s="T251">tabanɨ </ts>
               <ts e="T253" id="Seg_3750" n="e" s="T252">inni </ts>
               <ts e="T254" id="Seg_3752" n="e" s="T253">di͡ek </ts>
               <ts e="T255" id="Seg_3754" n="e" s="T254">illeːččiler. </ts>
               <ts e="T256" id="Seg_3756" n="e" s="T255">Töröːbüt </ts>
               <ts e="T257" id="Seg_3758" n="e" s="T256">tabanɨ, </ts>
               <ts e="T258" id="Seg_3760" n="e" s="T257">tuguta </ts>
               <ts e="T259" id="Seg_3762" n="e" s="T258">ulaːta </ts>
               <ts e="T260" id="Seg_3764" n="e" s="T259">tühü͡ör </ts>
               <ts e="T261" id="Seg_3766" n="e" s="T260">di͡eri </ts>
               <ts e="T262" id="Seg_3768" n="e" s="T261">kaːmpatɨn </ts>
               <ts e="T263" id="Seg_3770" n="e" s="T262">ihin </ts>
               <ts e="T264" id="Seg_3772" n="e" s="T263">kenni </ts>
               <ts e="T265" id="Seg_3774" n="e" s="T264">di͡ek </ts>
               <ts e="T266" id="Seg_3776" n="e" s="T265">keːheller. </ts>
               <ts e="T267" id="Seg_3778" n="e" s="T266">Bu </ts>
               <ts e="T268" id="Seg_3780" n="e" s="T267">keːhiːleriger, </ts>
               <ts e="T269" id="Seg_3782" n="e" s="T268">bu </ts>
               <ts e="T270" id="Seg_3784" n="e" s="T269">te </ts>
               <ts e="T271" id="Seg_3786" n="e" s="T270">u͡ontan </ts>
               <ts e="T272" id="Seg_3788" n="e" s="T271">taksa, </ts>
               <ts e="T273" id="Seg_3790" n="e" s="T272">hüːrbečče </ts>
               <ts e="T274" id="Seg_3792" n="e" s="T273">taba </ts>
               <ts e="T275" id="Seg_3794" n="e" s="T274">töröːbütün </ts>
               <ts e="T276" id="Seg_3796" n="e" s="T275">kenne, </ts>
               <ts e="T277" id="Seg_3798" n="e" s="T276">oččogo </ts>
               <ts e="T278" id="Seg_3800" n="e" s="T277">bu </ts>
               <ts e="T279" id="Seg_3802" n="e" s="T278">törüːlik </ts>
               <ts e="T280" id="Seg_3804" n="e" s="T279">tabanɨ </ts>
               <ts e="T281" id="Seg_3806" n="e" s="T280">inni </ts>
               <ts e="T282" id="Seg_3808" n="e" s="T281">di͡ek </ts>
               <ts e="T283" id="Seg_3810" n="e" s="T282">illi͡ekke </ts>
               <ts e="T284" id="Seg_3812" n="e" s="T283">naːda. </ts>
               <ts e="T285" id="Seg_3814" n="e" s="T284">Bu </ts>
               <ts e="T286" id="Seg_3816" n="e" s="T285">tabanɨ </ts>
               <ts e="T287" id="Seg_3818" n="e" s="T286">illerge </ts>
               <ts e="T288" id="Seg_3820" n="e" s="T287">horok </ts>
               <ts e="T289" id="Seg_3822" n="e" s="T288">pastuːk </ts>
               <ts e="T290" id="Seg_3824" n="e" s="T289">bu </ts>
               <ts e="T291" id="Seg_3826" n="e" s="T290">töröːbüt </ts>
               <ts e="T292" id="Seg_3828" n="e" s="T291">tuguttaːkka </ts>
               <ts e="T293" id="Seg_3830" n="e" s="T292">kaːlar. </ts>
               <ts e="T294" id="Seg_3832" n="e" s="T293">Horok </ts>
               <ts e="T295" id="Seg_3834" n="e" s="T294">pastuːk </ts>
               <ts e="T296" id="Seg_3836" n="e" s="T295">bu </ts>
               <ts e="T297" id="Seg_3838" n="e" s="T296">(töröː-) </ts>
               <ts e="T298" id="Seg_3840" n="e" s="T297">törüːlik </ts>
               <ts e="T299" id="Seg_3842" n="e" s="T298">tabanɨ </ts>
               <ts e="T300" id="Seg_3844" n="e" s="T299">iller. </ts>
               <ts e="T301" id="Seg_3846" n="e" s="T300">Bu </ts>
               <ts e="T302" id="Seg_3848" n="e" s="T301">illeriger </ts>
               <ts e="T303" id="Seg_3850" n="e" s="T302">ahatan </ts>
               <ts e="T304" id="Seg_3852" n="e" s="T303">albɨnnaːn </ts>
               <ts e="T305" id="Seg_3854" n="e" s="T304">kalka </ts>
               <ts e="T306" id="Seg_3856" n="e" s="T305">hirge </ts>
               <ts e="T307" id="Seg_3858" n="e" s="T306">kiːlleren, </ts>
               <ts e="T308" id="Seg_3860" n="e" s="T307">ü͡öre </ts>
               <ts e="T309" id="Seg_3862" n="e" s="T308">bararɨn </ts>
               <ts e="T310" id="Seg_3864" n="e" s="T309">köllörbökkö, </ts>
               <ts e="T311" id="Seg_3866" n="e" s="T310">bu </ts>
               <ts e="T312" id="Seg_3868" n="e" s="T311">tuguttaːk </ts>
               <ts e="T314" id="Seg_3870" n="e" s="T312">tɨhɨnɨ… </ts>
               <ts e="T315" id="Seg_3872" n="e" s="T314">Kahan </ts>
               <ts e="T316" id="Seg_3874" n="e" s="T315">da </ts>
               <ts e="T317" id="Seg_3876" n="e" s="T316">taba </ts>
               <ts e="T318" id="Seg_3878" n="e" s="T317">hannʼɨhan </ts>
               <ts e="T319" id="Seg_3880" n="e" s="T318">tugutun </ts>
               <ts e="T320" id="Seg_3882" n="e" s="T319">keːheːčči </ts>
               <ts e="T321" id="Seg_3884" n="e" s="T320">batɨhan, </ts>
               <ts e="T322" id="Seg_3886" n="e" s="T321">ü͡örüger </ts>
               <ts e="T323" id="Seg_3888" n="e" s="T322">talahan. </ts>
               <ts e="T324" id="Seg_3890" n="e" s="T323">Onu </ts>
               <ts e="T325" id="Seg_3892" n="e" s="T324">ahata </ts>
               <ts e="T326" id="Seg_3894" n="e" s="T325">hɨldʼan </ts>
               <ts e="T327" id="Seg_3896" n="e" s="T326">topputun </ts>
               <ts e="T328" id="Seg_3898" n="e" s="T327">kemnene </ts>
               <ts e="T329" id="Seg_3900" n="e" s="T328">hɨtarɨgar </ts>
               <ts e="T330" id="Seg_3902" n="e" s="T329">törüːlik </ts>
               <ts e="T331" id="Seg_3904" n="e" s="T330">tabatɨn </ts>
               <ts e="T332" id="Seg_3906" n="e" s="T331">inni </ts>
               <ts e="T333" id="Seg_3908" n="e" s="T332">di͡ek </ts>
               <ts e="T334" id="Seg_3910" n="e" s="T333">köhördön </ts>
               <ts e="T335" id="Seg_3912" n="e" s="T334">iller. </ts>
               <ts e="T336" id="Seg_3914" n="e" s="T335">Ol </ts>
               <ts e="T337" id="Seg_3916" n="e" s="T336">köhörtöktörüne, </ts>
               <ts e="T338" id="Seg_3918" n="e" s="T337">horok </ts>
               <ts e="T339" id="Seg_3920" n="e" s="T338">pastuːktar </ts>
               <ts e="T340" id="Seg_3922" n="e" s="T339">hürdeːk </ts>
               <ts e="T341" id="Seg_3924" n="e" s="T340">ɨraːk </ts>
               <ts e="T342" id="Seg_3926" n="e" s="T341">baraːččɨlar. </ts>
               <ts e="T343" id="Seg_3928" n="e" s="T342">Bu </ts>
               <ts e="T344" id="Seg_3930" n="e" s="T343">ɨraːk </ts>
               <ts e="T345" id="Seg_3932" n="e" s="T344">bardaktarɨna, </ts>
               <ts e="T346" id="Seg_3934" n="e" s="T345">bu </ts>
               <ts e="T347" id="Seg_3936" n="e" s="T346">tugut </ts>
               <ts e="T348" id="Seg_3938" n="e" s="T347">harsɨŋŋɨtɨn </ts>
               <ts e="T349" id="Seg_3940" n="e" s="T348">ol </ts>
               <ts e="T350" id="Seg_3942" n="e" s="T349">törüːlik </ts>
               <ts e="T351" id="Seg_3944" n="e" s="T350">tabalaːkka </ts>
               <ts e="T352" id="Seg_3946" n="e" s="T351">kennikiːtin, </ts>
               <ts e="T353" id="Seg_3948" n="e" s="T352">pastuːktarɨn </ts>
               <ts e="T354" id="Seg_3950" n="e" s="T353">ol </ts>
               <ts e="T355" id="Seg_3952" n="e" s="T354">manɨ </ts>
               <ts e="T356" id="Seg_3954" n="e" s="T355">töröːbüt </ts>
               <ts e="T357" id="Seg_3956" n="e" s="T356">tɨhɨlaːktarɨn </ts>
               <ts e="T358" id="Seg_3958" n="e" s="T357">pastuːkta, </ts>
               <ts e="T359" id="Seg_3960" n="e" s="T358">inni </ts>
               <ts e="T360" id="Seg_3962" n="e" s="T359">di͡ek </ts>
               <ts e="T361" id="Seg_3964" n="e" s="T360">pastuːktarga </ts>
               <ts e="T362" id="Seg_3966" n="e" s="T361">illeːriler, </ts>
               <ts e="T363" id="Seg_3968" n="e" s="T362">köhöllörüger </ts>
               <ts e="T364" id="Seg_3970" n="e" s="T363">taba </ts>
               <ts e="T365" id="Seg_3972" n="e" s="T364">koton </ts>
               <ts e="T366" id="Seg_3974" n="e" s="T365">tijeːččite </ts>
               <ts e="T367" id="Seg_3976" n="e" s="T366">hu͡ok </ts>
               <ts e="T368" id="Seg_3978" n="e" s="T367">bo. </ts>
               <ts e="T369" id="Seg_3980" n="e" s="T368">Ol </ts>
               <ts e="T370" id="Seg_3982" n="e" s="T369">ihin </ts>
               <ts e="T371" id="Seg_3984" n="e" s="T370">čugas </ts>
               <ts e="T372" id="Seg_3986" n="e" s="T371">tühü͡ökke </ts>
               <ts e="T373" id="Seg_3988" n="e" s="T372">körön </ts>
               <ts e="T374" id="Seg_3990" n="e" s="T373">baraːn. </ts>
               <ts e="T375" id="Seg_3992" n="e" s="T374">Onton </ts>
               <ts e="T376" id="Seg_3994" n="e" s="T375">bu͡ollagɨna </ts>
               <ts e="T377" id="Seg_3996" n="e" s="T376">bu </ts>
               <ts e="T378" id="Seg_3998" n="e" s="T377">pastuːktar </ts>
               <ts e="T379" id="Seg_4000" n="e" s="T378">kahan </ts>
               <ts e="T380" id="Seg_4002" n="e" s="T379">da </ts>
               <ts e="T381" id="Seg_4004" n="e" s="T380">kalkalaːk </ts>
               <ts e="T382" id="Seg_4006" n="e" s="T381">hirinen </ts>
               <ts e="T383" id="Seg_4008" n="e" s="T382">hɨldʼallar. </ts>
               <ts e="T384" id="Seg_4010" n="e" s="T383">Maršrut </ts>
               <ts e="T385" id="Seg_4012" n="e" s="T384">köröllör </ts>
               <ts e="T386" id="Seg_4014" n="e" s="T385">taba </ts>
               <ts e="T387" id="Seg_4016" n="e" s="T386">törötörüger </ts>
               <ts e="T388" id="Seg_4018" n="e" s="T387">kalkalaːk </ts>
               <ts e="T389" id="Seg_4020" n="e" s="T388">hirinen, </ts>
               <ts e="T390" id="Seg_4022" n="e" s="T389">ohuːrdaːk </ts>
               <ts e="T391" id="Seg_4024" n="e" s="T390">hirinen, </ts>
               <ts e="T392" id="Seg_4026" n="e" s="T391">kajalardaːk </ts>
               <ts e="T393" id="Seg_4028" n="e" s="T392">hirderinen, </ts>
               <ts e="T394" id="Seg_4030" n="e" s="T393">ürekterdeːk </ts>
               <ts e="T395" id="Seg_4032" n="e" s="T394">hirderinen. </ts>
               <ts e="T396" id="Seg_4034" n="e" s="T395">Bu </ts>
               <ts e="T397" id="Seg_4036" n="e" s="T396">ulakan </ts>
               <ts e="T398" id="Seg_4038" n="e" s="T397">hobu͡oj </ts>
               <ts e="T399" id="Seg_4040" n="e" s="T398">bɨstaːččɨ. </ts>
               <ts e="T400" id="Seg_4042" n="e" s="T399">Bu </ts>
               <ts e="T401" id="Seg_4044" n="e" s="T400">bɨhɨnnagɨna, </ts>
               <ts e="T402" id="Seg_4046" n="e" s="T401">haːs </ts>
               <ts e="T403" id="Seg_4048" n="e" s="T402">taba </ts>
               <ts e="T404" id="Seg_4050" n="e" s="T403">törüːrün </ts>
               <ts e="T405" id="Seg_4052" n="e" s="T404">hagɨna </ts>
               <ts e="T406" id="Seg_4054" n="e" s="T405">kün </ts>
               <ts e="T407" id="Seg_4056" n="e" s="T406">hɨrdaːtagɨna, </ts>
               <ts e="T408" id="Seg_4058" n="e" s="T407">kɨhɨn </ts>
               <ts e="T409" id="Seg_4060" n="e" s="T408">bɨstɨbɨt </ts>
               <ts e="T410" id="Seg_4062" n="e" s="T409">hobu͡oj </ts>
               <ts e="T411" id="Seg_4064" n="e" s="T410">ataːččɨ. </ts>
               <ts e="T412" id="Seg_4066" n="e" s="T411">Ol </ts>
               <ts e="T413" id="Seg_4068" n="e" s="T412">aːta </ts>
               <ts e="T414" id="Seg_4070" n="e" s="T413">köŋüs </ts>
               <ts e="T415" id="Seg_4072" n="e" s="T414">bu͡olar. </ts>
               <ts e="T416" id="Seg_4074" n="e" s="T415">Taba </ts>
               <ts e="T417" id="Seg_4076" n="e" s="T416">huraga </ts>
               <ts e="T418" id="Seg_4078" n="e" s="T417">hu͡ok </ts>
               <ts e="T419" id="Seg_4080" n="e" s="T418">hütere, </ts>
               <ts e="T420" id="Seg_4082" n="e" s="T419">ol </ts>
               <ts e="T421" id="Seg_4084" n="e" s="T420">köŋüs </ts>
               <ts e="T422" id="Seg_4086" n="e" s="T421">ihiger </ts>
               <ts e="T423" id="Seg_4088" n="e" s="T422">tüspüt </ts>
               <ts e="T424" id="Seg_4090" n="e" s="T423">tabanɨ </ts>
               <ts e="T425" id="Seg_4092" n="e" s="T424">bu </ts>
               <ts e="T426" id="Seg_4094" n="e" s="T425">pastuːk </ts>
               <ts e="T427" id="Seg_4096" n="e" s="T426">ketiː </ts>
               <ts e="T428" id="Seg_4098" n="e" s="T427">hɨldʼan </ts>
               <ts e="T429" id="Seg_4100" n="e" s="T428">bilbet, </ts>
               <ts e="T430" id="Seg_4102" n="e" s="T429">kas </ts>
               <ts e="T431" id="Seg_4104" n="e" s="T430">da </ts>
               <ts e="T432" id="Seg_4106" n="e" s="T431">taba </ts>
               <ts e="T433" id="Seg_4108" n="e" s="T432">tüspütün. </ts>
               <ts e="T434" id="Seg_4110" n="e" s="T433">Honon </ts>
               <ts e="T435" id="Seg_4112" n="e" s="T434">hu͡oktaːbat. </ts>
               <ts e="T436" id="Seg_4114" n="e" s="T435">Taba </ts>
               <ts e="T437" id="Seg_4116" n="e" s="T436">huraga </ts>
               <ts e="T438" id="Seg_4118" n="e" s="T437">hu͡ok </ts>
               <ts e="T439" id="Seg_4120" n="e" s="T438">hütere </ts>
               <ts e="T440" id="Seg_4122" n="e" s="T439">ol. </ts>
               <ts e="T441" id="Seg_4124" n="e" s="T440">Onon </ts>
               <ts e="T442" id="Seg_4126" n="e" s="T441">brʼigadʼiːrder </ts>
               <ts e="T443" id="Seg_4128" n="e" s="T442">bu͡ol, </ts>
               <ts e="T444" id="Seg_4130" n="e" s="T443">pastuːktar </ts>
               <ts e="T445" id="Seg_4132" n="e" s="T444">bu͡ol, </ts>
               <ts e="T446" id="Seg_4134" n="e" s="T445">haːs </ts>
               <ts e="T447" id="Seg_4136" n="e" s="T446">taba </ts>
               <ts e="T448" id="Seg_4138" n="e" s="T447">törüːrün </ts>
               <ts e="T449" id="Seg_4140" n="e" s="T448">hagɨna </ts>
               <ts e="T450" id="Seg_4142" n="e" s="T449">((PAUSE)) </ts>
               <ts e="T451" id="Seg_4144" n="e" s="T450">kaːr </ts>
               <ts e="T452" id="Seg_4146" n="e" s="T451">irerin </ts>
               <ts e="T453" id="Seg_4148" n="e" s="T452">hagɨna </ts>
               <ts e="T454" id="Seg_4150" n="e" s="T453">üčügejdik </ts>
               <ts e="T455" id="Seg_4152" n="e" s="T454">hirderin </ts>
               <ts e="T456" id="Seg_4154" n="e" s="T455">körü͡ökterin </ts>
               <ts e="T457" id="Seg_4156" n="e" s="T456">naːda. </ts>
               <ts e="T458" id="Seg_4158" n="e" s="T457">Hobu͡oj </ts>
               <ts e="T459" id="Seg_4160" n="e" s="T458">kajdɨbɨta </ts>
               <ts e="T460" id="Seg_4162" n="e" s="T459">purga </ts>
               <ts e="T461" id="Seg_4164" n="e" s="T460">bu͡ollagɨna </ts>
               <ts e="T462" id="Seg_4166" n="e" s="T461">tibillen </ts>
               <ts e="T463" id="Seg_4168" n="e" s="T462">kaːlaːččɨ. </ts>
               <ts e="T464" id="Seg_4170" n="e" s="T463">Ol </ts>
               <ts e="T465" id="Seg_4172" n="e" s="T464">gɨnan </ts>
               <ts e="T466" id="Seg_4174" n="e" s="T465">baraːn, </ts>
               <ts e="T467" id="Seg_4176" n="e" s="T466">ol </ts>
               <ts e="T468" id="Seg_4178" n="e" s="T467">tibillibitin </ts>
               <ts e="T469" id="Seg_4180" n="e" s="T468">kenne </ts>
               <ts e="T470" id="Seg_4182" n="e" s="T469">onu </ts>
               <ts e="T471" id="Seg_4184" n="e" s="T470">taba </ts>
               <ts e="T472" id="Seg_4186" n="e" s="T471">körböt, </ts>
               <ts e="T473" id="Seg_4188" n="e" s="T472">honon </ts>
               <ts e="T474" id="Seg_4190" n="e" s="T473">tüher. </ts>
               <ts e="T475" id="Seg_4192" n="e" s="T474">Ol </ts>
               <ts e="T476" id="Seg_4194" n="e" s="T475">tüstegine </ts>
               <ts e="T477" id="Seg_4196" n="e" s="T476">bi͡es-tü͡ört </ts>
               <ts e="T478" id="Seg_4198" n="e" s="T477">mʼetʼirdeːk </ts>
               <ts e="T479" id="Seg_4200" n="e" s="T478">diriŋneːk </ts>
               <ts e="T480" id="Seg_4202" n="e" s="T479">bu͡olaːččɨ </ts>
               <ts e="T481" id="Seg_4204" n="e" s="T480">bu </ts>
               <ts e="T482" id="Seg_4206" n="e" s="T481">köŋüs. </ts>
               <ts e="T483" id="Seg_4208" n="e" s="T482">Kaːr </ts>
               <ts e="T484" id="Seg_4210" n="e" s="T483">bɨstɨbɨt. </ts>
               <ts e="T485" id="Seg_4212" n="e" s="T484">Ol </ts>
               <ts e="T486" id="Seg_4214" n="e" s="T485">ihin </ts>
               <ts e="T487" id="Seg_4216" n="e" s="T486">onnugu </ts>
               <ts e="T488" id="Seg_4218" n="e" s="T487">bulbakka, </ts>
               <ts e="T489" id="Seg_4220" n="e" s="T488">taba </ts>
               <ts e="T490" id="Seg_4222" n="e" s="T489">huraga </ts>
               <ts e="T491" id="Seg_4224" n="e" s="T490">hu͡ok </ts>
               <ts e="T492" id="Seg_4226" n="e" s="T491">hüter </ts>
               <ts e="T493" id="Seg_4228" n="e" s="T492">iti. </ts>
               <ts e="T494" id="Seg_4230" n="e" s="T493">Onton </ts>
               <ts e="T495" id="Seg_4232" n="e" s="T494">ürekteːk </ts>
               <ts e="T496" id="Seg_4234" n="e" s="T495">hirderge </ts>
               <ts e="T497" id="Seg_4236" n="e" s="T496">bu͡ollagɨna, </ts>
               <ts e="T498" id="Seg_4238" n="e" s="T497">karadʼɨktaːtagɨna, </ts>
               <ts e="T499" id="Seg_4240" n="e" s="T498">kahan </ts>
               <ts e="T500" id="Seg_4242" n="e" s="T499">da </ts>
               <ts e="T501" id="Seg_4244" n="e" s="T500">ürekteːk </ts>
               <ts e="T502" id="Seg_4246" n="e" s="T501">hirinen </ts>
               <ts e="T503" id="Seg_4248" n="e" s="T502">bu͡olar </ts>
               <ts e="T504" id="Seg_4250" n="e" s="T503">maršrut </ts>
               <ts e="T505" id="Seg_4252" n="e" s="T504">bu. </ts>
               <ts e="T506" id="Seg_4254" n="e" s="T505">Kaːrɨ </ts>
               <ts e="T507" id="Seg_4256" n="e" s="T506">gɨtta </ts>
               <ts e="T508" id="Seg_4258" n="e" s="T507">kaja </ts>
               <ts e="T509" id="Seg_4260" n="e" s="T508">hir </ts>
               <ts e="T510" id="Seg_4262" n="e" s="T509">ɨkkardɨta </ts>
               <ts e="T511" id="Seg_4264" n="e" s="T510">ataːččɨ </ts>
               <ts e="T512" id="Seg_4266" n="e" s="T511">emi͡e, </ts>
               <ts e="T513" id="Seg_4268" n="e" s="T512">onon </ts>
               <ts e="T514" id="Seg_4270" n="e" s="T513">emi͡e </ts>
               <ts e="T515" id="Seg_4272" n="e" s="T514">tüheːčči </ts>
               <ts e="T516" id="Seg_4274" n="e" s="T515">tugut </ts>
               <ts e="T517" id="Seg_4276" n="e" s="T516">bu͡ol, </ts>
               <ts e="T518" id="Seg_4278" n="e" s="T517">ulakan </ts>
               <ts e="T519" id="Seg_4280" n="e" s="T518">taba </ts>
               <ts e="T520" id="Seg_4282" n="e" s="T519">bu͡ol. </ts>
               <ts e="T521" id="Seg_4284" n="e" s="T520">Onton </ts>
               <ts e="T522" id="Seg_4286" n="e" s="T521">bu </ts>
               <ts e="T523" id="Seg_4288" n="e" s="T522">pastuːktar </ts>
               <ts e="T524" id="Seg_4290" n="e" s="T523">(haːs-) </ts>
               <ts e="T525" id="Seg_4292" n="e" s="T524">hajɨnnara </ts>
               <ts e="T526" id="Seg_4294" n="e" s="T525">čugahaːtagɨna </ts>
               <ts e="T527" id="Seg_4296" n="e" s="T526">haːskɨlarɨgar, </ts>
               <ts e="T528" id="Seg_4298" n="e" s="T527">uːčaktarɨn </ts>
               <ts e="T529" id="Seg_4300" n="e" s="T528">hataːn </ts>
               <ts e="T530" id="Seg_4302" n="e" s="T529">karammattar, </ts>
               <ts e="T531" id="Seg_4304" n="e" s="T530">künüktetin </ts>
               <ts e="T532" id="Seg_4306" n="e" s="T531">tonoːbottor. </ts>
               <ts e="T533" id="Seg_4308" n="e" s="T532">Haːs </ts>
               <ts e="T534" id="Seg_4310" n="e" s="T533">künüktetin </ts>
               <ts e="T535" id="Seg_4312" n="e" s="T534">tonoːtoktoruna, </ts>
               <ts e="T536" id="Seg_4314" n="e" s="T535">ol </ts>
               <ts e="T537" id="Seg_4316" n="e" s="T536">künüktetin </ts>
               <ts e="T538" id="Seg_4318" n="e" s="T537">tonoːtoktoruna </ts>
               <ts e="T539" id="Seg_4320" n="e" s="T538">bu </ts>
               <ts e="T540" id="Seg_4322" n="e" s="T539">künükte </ts>
               <ts e="T541" id="Seg_4324" n="e" s="T540">ohor. </ts>
               <ts e="T542" id="Seg_4326" n="e" s="T541">Bejete </ts>
               <ts e="T543" id="Seg_4328" n="e" s="T542">künükte </ts>
               <ts e="T544" id="Seg_4330" n="e" s="T543">haːs </ts>
               <ts e="T545" id="Seg_4332" n="e" s="T544">ulaːtan </ts>
               <ts e="T546" id="Seg_4334" n="e" s="T545">tagɨstagɨna, </ts>
               <ts e="T547" id="Seg_4336" n="e" s="T546">tiriːtin </ts>
               <ts e="T548" id="Seg_4338" n="e" s="T547">ihitten </ts>
               <ts e="T549" id="Seg_4340" n="e" s="T548">bejete </ts>
               <ts e="T550" id="Seg_4342" n="e" s="T549">tullan </ts>
               <ts e="T551" id="Seg_4344" n="e" s="T550">tühüːr </ts>
               <ts e="T552" id="Seg_4346" n="e" s="T551">di͡eri </ts>
               <ts e="T553" id="Seg_4348" n="e" s="T552">hɨrɨttagɨna, </ts>
               <ts e="T554" id="Seg_4350" n="e" s="T553">tabaga </ts>
               <ts e="T555" id="Seg_4352" n="e" s="T554">da </ts>
               <ts e="T556" id="Seg_4354" n="e" s="T555">kuhagan. </ts>
               <ts e="T557" id="Seg_4356" n="e" s="T556">Horok </ts>
               <ts e="T558" id="Seg_4358" n="e" s="T557">künükteni </ts>
               <ts e="T559" id="Seg_4360" n="e" s="T558">bütejdiː </ts>
               <ts e="T560" id="Seg_4362" n="e" s="T559">miːne-miːneler </ts>
               <ts e="T561" id="Seg_4364" n="e" s="T560">taba </ts>
               <ts e="T562" id="Seg_4366" n="e" s="T561">gi͡ene </ts>
               <ts e="T563" id="Seg_4368" n="e" s="T562">ölör </ts>
               <ts e="T564" id="Seg_4370" n="e" s="T563">ünere </ts>
               <ts e="T565" id="Seg_4372" n="e" s="T564">ol. </ts>
               <ts e="T566" id="Seg_4374" n="e" s="T565">Horok </ts>
               <ts e="T567" id="Seg_4376" n="e" s="T566">pastuːk </ts>
               <ts e="T568" id="Seg_4378" n="e" s="T567">tabanɨ </ts>
               <ts e="T569" id="Seg_4380" n="e" s="T568">da </ts>
               <ts e="T570" id="Seg_4382" n="e" s="T569">kölüne </ts>
               <ts e="T571" id="Seg_4384" n="e" s="T570">hataːbat. </ts>
               <ts e="T572" id="Seg_4386" n="e" s="T571">Pabudu͡oktara </ts>
               <ts e="T573" id="Seg_4388" n="e" s="T572">bɨhɨnna, </ts>
               <ts e="T574" id="Seg_4390" n="e" s="T573">taba </ts>
               <ts e="T575" id="Seg_4392" n="e" s="T574">gi͡enen </ts>
               <ts e="T576" id="Seg_4394" n="e" s="T575">hamagɨn </ts>
               <ts e="T577" id="Seg_4396" n="e" s="T576">aːllarallar. </ts>
               <ts e="T578" id="Seg_4398" n="e" s="T577">Hamagɨn </ts>
               <ts e="T579" id="Seg_4400" n="e" s="T578">aːllardaktarɨna, </ts>
               <ts e="T580" id="Seg_4402" n="e" s="T579">bu </ts>
               <ts e="T581" id="Seg_4404" n="e" s="T580">hajɨn </ts>
               <ts e="T582" id="Seg_4406" n="e" s="T581">baːs </ts>
               <ts e="T583" id="Seg_4408" n="e" s="T582">bu͡olaːččɨ. </ts>
               <ts e="T584" id="Seg_4410" n="e" s="T583">Manan </ts>
               <ts e="T585" id="Seg_4412" n="e" s="T584">taba </ts>
               <ts e="T586" id="Seg_4414" n="e" s="T585">((PAUSE)) </ts>
               <ts e="T587" id="Seg_4416" n="e" s="T586">һutuːra </ts>
               <ts e="T588" id="Seg_4418" n="e" s="T587">iti. </ts>
               <ts e="T589" id="Seg_4420" n="e" s="T588">Onton </ts>
               <ts e="T590" id="Seg_4422" n="e" s="T589">kuturugun </ts>
               <ts e="T591" id="Seg_4424" n="e" s="T590">taba </ts>
               <ts e="T592" id="Seg_4426" n="e" s="T591">gi͡enin </ts>
               <ts e="T593" id="Seg_4428" n="e" s="T592">kaja </ts>
               <ts e="T594" id="Seg_4430" n="e" s="T593">annʼallar. </ts>
               <ts e="T595" id="Seg_4432" n="e" s="T594">Bu </ts>
               <ts e="T596" id="Seg_4434" n="e" s="T595">kuturugun </ts>
               <ts e="T597" id="Seg_4436" n="e" s="T596">kaja </ts>
               <ts e="T598" id="Seg_4438" n="e" s="T597">astaktarɨna </ts>
               <ts e="T599" id="Seg_4440" n="e" s="T598">kɨhɨn </ts>
               <ts e="T600" id="Seg_4442" n="e" s="T599">tɨmnɨːga </ts>
               <ts e="T601" id="Seg_4444" n="e" s="T600">bu </ts>
               <ts e="T602" id="Seg_4446" n="e" s="T601">taba </ts>
               <ts e="T603" id="Seg_4448" n="e" s="T602">gi͡ene </ts>
               <ts e="T604" id="Seg_4450" n="e" s="T603">ete </ts>
               <ts e="T605" id="Seg_4452" n="e" s="T604">kaččaga </ts>
               <ts e="T606" id="Seg_4454" n="e" s="T605">da </ts>
               <ts e="T607" id="Seg_4456" n="e" s="T606">emi͡e </ts>
               <ts e="T608" id="Seg_4458" n="e" s="T607">köppöt. </ts>
               <ts e="T609" id="Seg_4460" n="e" s="T608">Onton </ts>
               <ts e="T610" id="Seg_4462" n="e" s="T609">hajɨn, </ts>
               <ts e="T611" id="Seg_4464" n="e" s="T610">itiː </ts>
               <ts e="T612" id="Seg_4466" n="e" s="T611">hagɨna </ts>
               <ts e="T613" id="Seg_4468" n="e" s="T612">köhöllör. </ts>
               <ts e="T614" id="Seg_4470" n="e" s="T613">Bu </ts>
               <ts e="T615" id="Seg_4472" n="e" s="T614">köstöktörüne, </ts>
               <ts e="T616" id="Seg_4474" n="e" s="T615">itiːge </ts>
               <ts e="T617" id="Seg_4476" n="e" s="T616">köstöktörüne, </ts>
               <ts e="T618" id="Seg_4478" n="e" s="T617">köhön </ts>
               <ts e="T619" id="Seg_4480" n="e" s="T618">ihenner </ts>
               <ts e="T620" id="Seg_4482" n="e" s="T619">ürekteːk </ts>
               <ts e="T621" id="Seg_4484" n="e" s="T620">hirderinen </ts>
               <ts e="T622" id="Seg_4486" n="e" s="T621">barallar. </ts>
               <ts e="T623" id="Seg_4488" n="e" s="T622">Bu </ts>
               <ts e="T624" id="Seg_4490" n="e" s="T623">ürekteːk </ts>
               <ts e="T625" id="Seg_4492" n="e" s="T624">ulakan </ts>
               <ts e="T626" id="Seg_4494" n="e" s="T625">kajalarga </ts>
               <ts e="T627" id="Seg_4496" n="e" s="T626">tu͡ojdaːk </ts>
               <ts e="T628" id="Seg_4498" n="e" s="T627">bu͡olaːččɨ. </ts>
               <ts e="T629" id="Seg_4500" n="e" s="T628">Töhö </ts>
               <ts e="T630" id="Seg_4502" n="e" s="T629">da </ts>
               <ts e="T631" id="Seg_4504" n="e" s="T630">itiːge </ts>
               <ts e="T632" id="Seg_4506" n="e" s="T631">bu </ts>
               <ts e="T633" id="Seg_4508" n="e" s="T632">tu͡ojɨ </ts>
               <ts e="T634" id="Seg_4510" n="e" s="T633">herejde </ts>
               <ts e="T635" id="Seg_4512" n="e" s="T634">da, </ts>
               <ts e="T636" id="Seg_4514" n="e" s="T635">taba </ts>
               <ts e="T637" id="Seg_4516" n="e" s="T636">bu </ts>
               <ts e="T638" id="Seg_4518" n="e" s="T637">kajaga </ts>
               <ts e="T639" id="Seg_4520" n="e" s="T638">baraːččɨ, </ts>
               <ts e="T640" id="Seg_4522" n="e" s="T639">köhön </ts>
               <ts e="T641" id="Seg_4524" n="e" s="T640">istekke </ts>
               <ts e="T642" id="Seg_4526" n="e" s="T641">da </ts>
               <ts e="T643" id="Seg_4528" n="e" s="T642">üːren </ts>
               <ts e="T644" id="Seg_4530" n="e" s="T643">da </ts>
               <ts e="T645" id="Seg_4532" n="e" s="T644">istekterinen. </ts>
               <ts e="T646" id="Seg_4534" n="e" s="T645">Manɨ </ts>
               <ts e="T647" id="Seg_4536" n="e" s="T646">pastuːktar, </ts>
               <ts e="T648" id="Seg_4538" n="e" s="T647">bu </ts>
               <ts e="T649" id="Seg_4540" n="e" s="T648">kajanɨ </ts>
               <ts e="T650" id="Seg_4542" n="e" s="T649">körbökkö, </ts>
               <ts e="T651" id="Seg_4544" n="e" s="T650">ɨttaːn </ts>
               <ts e="T652" id="Seg_4546" n="e" s="T651">ɨllɨlar </ts>
               <ts e="T653" id="Seg_4548" n="e" s="T652">daː, </ts>
               <ts e="T654" id="Seg_4550" n="e" s="T653">honon </ts>
               <ts e="T655" id="Seg_4552" n="e" s="T654">külse-külse </ts>
               <ts e="T656" id="Seg_4554" n="e" s="T655">aːhan </ts>
               <ts e="T657" id="Seg_4556" n="e" s="T656">kaːlallar. </ts>
               <ts e="T658" id="Seg_4558" n="e" s="T657">Manna </ts>
               <ts e="T659" id="Seg_4560" n="e" s="T658">u͡on </ts>
               <ts e="T660" id="Seg_4562" n="e" s="T659">orduga </ts>
               <ts e="T661" id="Seg_4564" n="e" s="T660">bi͡es </ts>
               <ts e="T662" id="Seg_4566" n="e" s="T661">duː, </ts>
               <ts e="T663" id="Seg_4568" n="e" s="T662">hüːrbe </ts>
               <ts e="T664" id="Seg_4570" n="e" s="T663">orduga </ts>
               <ts e="T665" id="Seg_4572" n="e" s="T664">bi͡es </ts>
               <ts e="T666" id="Seg_4574" n="e" s="T665">duː </ts>
               <ts e="T667" id="Seg_4576" n="e" s="T666">taba </ts>
               <ts e="T668" id="Seg_4578" n="e" s="T667">kaːlar. </ts>
               <ts e="T669" id="Seg_4580" n="e" s="T668">Bu </ts>
               <ts e="T670" id="Seg_4582" n="e" s="T669">kaːlbɨtɨn </ts>
               <ts e="T671" id="Seg_4584" n="e" s="T670">öjdöːböttör. </ts>
               <ts e="T672" id="Seg_4586" n="e" s="T671">Huːrka </ts>
               <ts e="T673" id="Seg_4588" n="e" s="T672">tühenner </ts>
               <ts e="T674" id="Seg_4590" n="e" s="T673">nöŋü͡ö </ts>
               <ts e="T675" id="Seg_4592" n="e" s="T674">kihi </ts>
               <ts e="T676" id="Seg_4594" n="e" s="T675">ketiːr. </ts>
               <ts e="T677" id="Seg_4596" n="e" s="T676">Maːdin </ts>
               <ts e="T678" id="Seg_4598" n="e" s="T677">bu </ts>
               <ts e="T679" id="Seg_4600" n="e" s="T678">köhölleriger </ts>
               <ts e="T680" id="Seg_4602" n="e" s="T679">keteːbit </ts>
               <ts e="T681" id="Seg_4604" n="e" s="T680">kihi, </ts>
               <ts e="T682" id="Seg_4606" n="e" s="T681">araj </ts>
               <ts e="T683" id="Seg_4608" n="e" s="T682">ugučaːga </ts>
               <ts e="T684" id="Seg_4610" n="e" s="T683">ol </ts>
               <ts e="T685" id="Seg_4612" n="e" s="T684">tabalarga </ts>
               <ts e="T686" id="Seg_4614" n="e" s="T685">onno </ts>
               <ts e="T687" id="Seg_4616" n="e" s="T686">kajaga </ts>
               <ts e="T688" id="Seg_4618" n="e" s="T687">kaːlbɨt </ts>
               <ts e="T689" id="Seg_4620" n="e" s="T688">biːr </ts>
               <ts e="T690" id="Seg_4622" n="e" s="T689">ugučaga. </ts>
               <ts e="T691" id="Seg_4624" n="e" s="T690">Harsɨŋŋɨtɨn </ts>
               <ts e="T692" id="Seg_4626" n="e" s="T691">köhöllör </ts>
               <ts e="T693" id="Seg_4628" n="e" s="T692">emi͡e. </ts>
               <ts e="T694" id="Seg_4630" n="e" s="T693">Köhön </ts>
               <ts e="T695" id="Seg_4632" n="e" s="T694">tühen </ts>
               <ts e="T696" id="Seg_4634" n="e" s="T695">baraːn </ts>
               <ts e="T697" id="Seg_4636" n="e" s="T696">nöŋü͡ö </ts>
               <ts e="T698" id="Seg_4638" n="e" s="T697">kihiler </ts>
               <ts e="T699" id="Seg_4640" n="e" s="T698">ketiːller. </ts>
               <ts e="T700" id="Seg_4642" n="e" s="T699">Maː </ts>
               <ts e="T701" id="Seg_4644" n="e" s="T700">ɨnaraː </ts>
               <ts e="T702" id="Seg_4646" n="e" s="T701">huːrka </ts>
               <ts e="T703" id="Seg_4648" n="e" s="T702">keteːbit </ts>
               <ts e="T704" id="Seg_4650" n="e" s="T703">artɨnnaːk </ts>
               <ts e="T705" id="Seg_4652" n="e" s="T704">kihi, </ts>
               <ts e="T706" id="Seg_4654" n="e" s="T705">ketiː </ts>
               <ts e="T707" id="Seg_4656" n="e" s="T706">taksan </ts>
               <ts e="T708" id="Seg_4658" n="e" s="T707">uːčagɨn </ts>
               <ts e="T709" id="Seg_4660" n="e" s="T708">oččogo </ts>
               <ts e="T710" id="Seg_4662" n="e" s="T709">hu͡oktuːr. </ts>
               <ts e="T711" id="Seg_4664" n="e" s="T710">Harsi͡erde </ts>
               <ts e="T712" id="Seg_4666" n="e" s="T711">kelen </ts>
               <ts e="T713" id="Seg_4668" n="e" s="T712">diːr: </ts>
               <ts e="T714" id="Seg_4670" n="e" s="T713">"Kaja, </ts>
               <ts e="T715" id="Seg_4672" n="e" s="T714">mini͡ene </ts>
               <ts e="T716" id="Seg_4674" n="e" s="T715">uːčagɨm </ts>
               <ts e="T717" id="Seg_4676" n="e" s="T716">hu͡ok. </ts>
               <ts e="T718" id="Seg_4678" n="e" s="T717">Onno </ts>
               <ts e="T719" id="Seg_4680" n="e" s="T718">huːrka </ts>
               <ts e="T720" id="Seg_4682" n="e" s="T719">da </ts>
               <ts e="T721" id="Seg_4684" n="e" s="T720">körbötögüm, </ts>
               <ts e="T722" id="Seg_4686" n="e" s="T721">maːdin </ts>
               <ts e="T723" id="Seg_4688" n="e" s="T722">köspüt </ts>
               <ts e="T724" id="Seg_4690" n="e" s="T723">huːrpar, </ts>
               <ts e="T725" id="Seg_4692" n="e" s="T724">ɨnaraː </ts>
               <ts e="T726" id="Seg_4694" n="e" s="T725">huːrka </ts>
               <ts e="T727" id="Seg_4696" n="e" s="T726">ketiːrber </ts>
               <ts e="T728" id="Seg_4698" n="e" s="T727">miːne </ts>
               <ts e="T729" id="Seg_4700" n="e" s="T728">hɨldʼɨbɨt </ts>
               <ts e="T730" id="Seg_4702" n="e" s="T729">tabam </ts>
               <ts e="T731" id="Seg_4704" n="e" s="T730">ete." </ts>
               <ts e="T732" id="Seg_4706" n="e" s="T731">"Kirdik </ts>
               <ts e="T733" id="Seg_4708" n="e" s="T732">da, </ts>
               <ts e="T734" id="Seg_4710" n="e" s="T733">onno </ts>
               <ts e="T735" id="Seg_4712" n="e" s="T734">huːrka </ts>
               <ts e="T736" id="Seg_4714" n="e" s="T735">kaːlbɨt </ts>
               <ts e="T737" id="Seg_4716" n="e" s="T736">bu͡olla", </ts>
               <ts e="T738" id="Seg_4718" n="e" s="T737">diː </ts>
               <ts e="T739" id="Seg_4720" n="e" s="T738">diːller </ts>
               <ts e="T740" id="Seg_4722" n="e" s="T739">ol </ts>
               <ts e="T741" id="Seg_4724" n="e" s="T740">tabanɨ </ts>
               <ts e="T742" id="Seg_4726" n="e" s="T741">bu </ts>
               <ts e="T743" id="Seg_4728" n="e" s="T742">huːrtarɨgar </ts>
               <ts e="T744" id="Seg_4730" n="e" s="T743">kördüː </ts>
               <ts e="T745" id="Seg_4732" n="e" s="T744">keleller, </ts>
               <ts e="T746" id="Seg_4734" n="e" s="T745">bügün </ts>
               <ts e="T747" id="Seg_4736" n="e" s="T746">köspüt </ts>
               <ts e="T748" id="Seg_4738" n="e" s="T747">huːrdarɨgar. </ts>
               <ts e="T749" id="Seg_4740" n="e" s="T748">Ol </ts>
               <ts e="T750" id="Seg_4742" n="e" s="T749">tabalarɨ, </ts>
               <ts e="T751" id="Seg_4744" n="e" s="T750">ɨnaraː </ts>
               <ts e="T752" id="Seg_4746" n="e" s="T751">köskö </ts>
               <ts e="T753" id="Seg_4748" n="e" s="T752">kaːlbɨt </ts>
               <ts e="T754" id="Seg_4750" n="e" s="T753">tabalarɨ </ts>
               <ts e="T755" id="Seg_4752" n="e" s="T754">betereː </ts>
               <ts e="T756" id="Seg_4754" n="e" s="T755">huːrka </ts>
               <ts e="T757" id="Seg_4756" n="e" s="T756">kördüːller. </ts>
               <ts e="T758" id="Seg_4758" n="e" s="T757">Ol </ts>
               <ts e="T759" id="Seg_4760" n="e" s="T758">ɨnaraː </ts>
               <ts e="T760" id="Seg_4762" n="e" s="T759">huːrka </ts>
               <ts e="T761" id="Seg_4764" n="e" s="T760">keteːbit </ts>
               <ts e="T762" id="Seg_4766" n="e" s="T761">pastuːk </ts>
               <ts e="T763" id="Seg_4768" n="e" s="T762">ugučaga </ts>
               <ts e="T764" id="Seg_4770" n="e" s="T763">onno </ts>
               <ts e="T765" id="Seg_4772" n="e" s="T764">kaːlbɨt, </ts>
               <ts e="T766" id="Seg_4774" n="e" s="T765">ol </ts>
               <ts e="T767" id="Seg_4776" n="e" s="T766">kajaga. </ts>
               <ts e="T768" id="Seg_4778" n="e" s="T767">Ol </ts>
               <ts e="T769" id="Seg_4780" n="e" s="T768">hogotok </ts>
               <ts e="T770" id="Seg_4782" n="e" s="T769">tabanɨ </ts>
               <ts e="T771" id="Seg_4784" n="e" s="T770">öjdüːller. </ts>
               <ts e="T772" id="Seg_4786" n="e" s="T771">Onno </ts>
               <ts e="T773" id="Seg_4788" n="e" s="T772">kaːlbɨta </ts>
               <ts e="T774" id="Seg_4790" n="e" s="T773">u͡ontan </ts>
               <ts e="T775" id="Seg_4792" n="e" s="T774">taksa </ts>
               <ts e="T776" id="Seg_4794" n="e" s="T775">taba. </ts>
               <ts e="T777" id="Seg_4796" n="e" s="T776">Ol </ts>
               <ts e="T778" id="Seg_4798" n="e" s="T777">ihin </ts>
               <ts e="T779" id="Seg_4800" n="e" s="T778">bʼez </ts>
               <ts e="T780" id="Seg_4802" n="e" s="T779">vʼestʼi </ts>
               <ts e="T781" id="Seg_4804" n="e" s="T780">taba </ts>
               <ts e="T782" id="Seg_4806" n="e" s="T781">huraga </ts>
               <ts e="T783" id="Seg_4808" n="e" s="T782">hu͡ok </ts>
               <ts e="T784" id="Seg_4810" n="e" s="T783">hütere – </ts>
               <ts e="T785" id="Seg_4812" n="e" s="T784">iti. </ts>
               <ts e="T786" id="Seg_4814" n="e" s="T785">Onton </ts>
               <ts e="T787" id="Seg_4816" n="e" s="T786">dʼɨl </ts>
               <ts e="T788" id="Seg_4818" n="e" s="T787">eŋin-eŋin </ts>
               <ts e="T789" id="Seg_4820" n="e" s="T788">bu͡olaːččɨ. </ts>
               <ts e="T790" id="Seg_4822" n="e" s="T789">Itiːli͡ek </ts>
               <ts e="T791" id="Seg_4824" n="e" s="T790">bu͡olaːččɨ. </ts>
               <ts e="T792" id="Seg_4826" n="e" s="T791">Bu </ts>
               <ts e="T793" id="Seg_4828" n="e" s="T792">itiːge </ts>
               <ts e="T794" id="Seg_4830" n="e" s="T793">emi͡e </ts>
               <ts e="T795" id="Seg_4832" n="e" s="T794">anɨgɨ </ts>
               <ts e="T796" id="Seg_4834" n="e" s="T795">pastuːktar </ts>
               <ts e="T797" id="Seg_4836" n="e" s="T796">tabanɨ </ts>
               <ts e="T798" id="Seg_4838" n="e" s="T797">hataːn </ts>
               <ts e="T799" id="Seg_4840" n="e" s="T798">karajbattar. </ts>
               <ts e="T800" id="Seg_4842" n="e" s="T799">Karajbattara </ts>
               <ts e="T801" id="Seg_4844" n="e" s="T800">tuːguj? </ts>
               <ts e="T802" id="Seg_4846" n="e" s="T801">Itiːge </ts>
               <ts e="T803" id="Seg_4848" n="e" s="T802">halgɨndʼɨt </ts>
               <ts e="T804" id="Seg_4850" n="e" s="T803">taba </ts>
               <ts e="T805" id="Seg_4852" n="e" s="T804">bu͡olaːččɨ. </ts>
               <ts e="T806" id="Seg_4854" n="e" s="T805">Halgɨndʼɨt </ts>
               <ts e="T807" id="Seg_4856" n="e" s="T806">taba </ts>
               <ts e="T808" id="Seg_4858" n="e" s="T807">aːta </ts>
               <ts e="T809" id="Seg_4860" n="e" s="T808">tɨ͡alɨ </ts>
               <ts e="T810" id="Seg_4862" n="e" s="T809">batar. </ts>
               <ts e="T811" id="Seg_4864" n="e" s="T810">Bu </ts>
               <ts e="T812" id="Seg_4866" n="e" s="T811">tɨ͡alɨ </ts>
               <ts e="T813" id="Seg_4868" n="e" s="T812">batar </ts>
               <ts e="T814" id="Seg_4870" n="e" s="T813">tabanɨ, </ts>
               <ts e="T815" id="Seg_4872" n="e" s="T814">itiː </ts>
               <ts e="T816" id="Seg_4874" n="e" s="T815">ahɨ͡ar </ts>
               <ts e="T817" id="Seg_4876" n="e" s="T816">di͡eri </ts>
               <ts e="T818" id="Seg_4878" n="e" s="T817">kihi </ts>
               <ts e="T819" id="Seg_4880" n="e" s="T818">tabattan </ts>
               <ts e="T820" id="Seg_4882" n="e" s="T819">baːjaːččɨ. </ts>
               <ts e="T821" id="Seg_4884" n="e" s="T820">Taba </ts>
               <ts e="T822" id="Seg_4886" n="e" s="T821">keteːtekterine, </ts>
               <ts e="T823" id="Seg_4888" n="e" s="T822">kihi </ts>
               <ts e="T824" id="Seg_4890" n="e" s="T823">küːhe </ts>
               <ts e="T825" id="Seg_4892" n="e" s="T824">tijeːččite </ts>
               <ts e="T826" id="Seg_4894" n="e" s="T825">hu͡ok </ts>
               <ts e="T827" id="Seg_4896" n="e" s="T826">itiːge. </ts>
               <ts e="T828" id="Seg_4898" n="e" s="T827">Tɨ͡alɨ </ts>
               <ts e="T929" id="Seg_4900" n="e" s="T828">((…)) </ts>
               <ts e="T830" id="Seg_4902" n="e" s="T929">gɨmmattar. </ts>
               <ts e="T831" id="Seg_4904" n="e" s="T830">Harsi͡erde </ts>
               <ts e="T832" id="Seg_4906" n="e" s="T831">egelelleriger </ts>
               <ts e="T833" id="Seg_4908" n="e" s="T832">nöŋü͡ö </ts>
               <ts e="T834" id="Seg_4910" n="e" s="T833">pastuːkka </ts>
               <ts e="T835" id="Seg_4912" n="e" s="T834">tuttarallarɨgar </ts>
               <ts e="T836" id="Seg_4914" n="e" s="T835">"bihigi </ts>
               <ts e="T837" id="Seg_4916" n="e" s="T836">kɨ͡ajɨ͡akpɨt </ts>
               <ts e="T838" id="Seg_4918" n="e" s="T837">hu͡oga </ts>
               <ts e="T839" id="Seg_4920" n="e" s="T838">et" </ts>
               <ts e="T840" id="Seg_4922" n="e" s="T839">di͡en </ts>
               <ts e="T841" id="Seg_4924" n="e" s="T840">kahan </ts>
               <ts e="T842" id="Seg_4926" n="e" s="T841">da </ts>
               <ts e="T843" id="Seg_4928" n="e" s="T842">tɨ͡al </ts>
               <ts e="T844" id="Seg_4930" n="e" s="T843">annɨ </ts>
               <ts e="T845" id="Seg_4932" n="e" s="T844">di͡ek </ts>
               <ts e="T846" id="Seg_4934" n="e" s="T845">darabiː </ts>
               <ts e="T847" id="Seg_4936" n="e" s="T846">gɨnɨ͡aktarɨn, </ts>
               <ts e="T848" id="Seg_4938" n="e" s="T847">dʼi͡elere </ts>
               <ts e="T849" id="Seg_4940" n="e" s="T848">tɨ͡al </ts>
               <ts e="T850" id="Seg_4942" n="e" s="T849">ürdü </ts>
               <ts e="T851" id="Seg_4944" n="e" s="T850">bü </ts>
               <ts e="T852" id="Seg_4946" n="e" s="T851">bu͡olar </ts>
               <ts e="T853" id="Seg_4948" n="e" s="T852">kördük. </ts>
               <ts e="T854" id="Seg_4950" n="e" s="T853">Taba </ts>
               <ts e="T855" id="Seg_4952" n="e" s="T854">oččogo </ts>
               <ts e="T856" id="Seg_4954" n="e" s="T855">kɨ͡ajbakka </ts>
               <ts e="T857" id="Seg_4956" n="e" s="T856">tɨ͡al </ts>
               <ts e="T858" id="Seg_4958" n="e" s="T857">utara </ts>
               <ts e="T859" id="Seg_4960" n="e" s="T858">kellegine, </ts>
               <ts e="T860" id="Seg_4962" n="e" s="T859">dʼi͡etiger </ts>
               <ts e="T861" id="Seg_4964" n="e" s="T860">kepterdegine, </ts>
               <ts e="T862" id="Seg_4966" n="e" s="T861">dʼi͡eteːgilere </ts>
               <ts e="T863" id="Seg_4968" n="e" s="T862">turannar </ts>
               <ts e="T864" id="Seg_4970" n="e" s="T863">manna </ts>
               <ts e="T865" id="Seg_4972" n="e" s="T864">kömölöhönnör. </ts>
               <ts e="T866" id="Seg_4974" n="e" s="T865">Oččogo </ts>
               <ts e="T867" id="Seg_4976" n="e" s="T866">kɨ͡ajallar </ts>
               <ts e="T868" id="Seg_4978" n="e" s="T867">tabanɨ. </ts>
               <ts e="T869" id="Seg_4980" n="e" s="T868">Ol </ts>
               <ts e="T870" id="Seg_4982" n="e" s="T869">kennine </ts>
               <ts e="T871" id="Seg_4984" n="e" s="T870">giniler </ts>
               <ts e="T872" id="Seg_4986" n="e" s="T871">haːskɨttan </ts>
               <ts e="T873" id="Seg_4988" n="e" s="T872">itiː </ts>
               <ts e="T874" id="Seg_4990" n="e" s="T873">tühe </ts>
               <ts e="T875" id="Seg_4992" n="e" s="T874">iligine, </ts>
               <ts e="T876" id="Seg_4994" n="e" s="T875">haŋardɨː </ts>
               <ts e="T877" id="Seg_4996" n="e" s="T876">itiː </ts>
               <ts e="T878" id="Seg_4998" n="e" s="T877">tüheriger, </ts>
               <ts e="T879" id="Seg_5000" n="e" s="T878">taba </ts>
               <ts e="T880" id="Seg_5002" n="e" s="T879">itiːrgiːriger </ts>
               <ts e="T881" id="Seg_5004" n="e" s="T880">buru͡oga </ts>
               <ts e="T882" id="Seg_5006" n="e" s="T881">tabanɨ </ts>
               <ts e="T883" id="Seg_5008" n="e" s="T882">ü͡öreti͡ekterin </ts>
               <ts e="T884" id="Seg_5010" n="e" s="T883">nada, </ts>
               <ts e="T885" id="Seg_5012" n="e" s="T884">ulakan </ts>
               <ts e="T886" id="Seg_5014" n="e" s="T885">itiː </ts>
               <ts e="T887" id="Seg_5016" n="e" s="T886">tühe </ts>
               <ts e="T888" id="Seg_5018" n="e" s="T887">iligine. </ts>
               <ts e="T889" id="Seg_5020" n="e" s="T888">Buru͡onu </ts>
               <ts e="T890" id="Seg_5022" n="e" s="T889">kördö </ts>
               <ts e="T891" id="Seg_5024" n="e" s="T890">da </ts>
               <ts e="T892" id="Seg_5026" n="e" s="T891">töhö </ts>
               <ts e="T893" id="Seg_5028" n="e" s="T892">daː </ts>
               <ts e="T894" id="Seg_5030" n="e" s="T893">itiːge </ts>
               <ts e="T896" id="Seg_5032" n="e" s="T894">taba… </ts>
               <ts e="T897" id="Seg_5034" n="e" s="T896">Dʼi͡eteːgilere </ts>
               <ts e="T898" id="Seg_5036" n="e" s="T897">itiːge </ts>
               <ts e="T899" id="Seg_5038" n="e" s="T898">pastuːktar </ts>
               <ts e="T900" id="Seg_5040" n="e" s="T899">ketiː </ts>
               <ts e="T901" id="Seg_5042" n="e" s="T900">baraːrɨlar </ts>
               <ts e="T902" id="Seg_5044" n="e" s="T901">haŋarɨ͡ak </ts>
               <ts e="T903" id="Seg_5046" n="e" s="T902">tustaːktar: </ts>
               <ts e="T904" id="Seg_5048" n="e" s="T903">"Kaja, </ts>
               <ts e="T905" id="Seg_5050" n="e" s="T904">itiː </ts>
               <ts e="T906" id="Seg_5052" n="e" s="T905">bu͡ollagɨna </ts>
               <ts e="T907" id="Seg_5054" n="e" s="T906">dʼe </ts>
               <ts e="T908" id="Seg_5056" n="e" s="T907">kečehen </ts>
               <ts e="T909" id="Seg_5058" n="e" s="T908">utujuŋ. </ts>
               <ts e="T910" id="Seg_5060" n="e" s="T909">Buru͡olataːrɨŋ, </ts>
               <ts e="T911" id="Seg_5062" n="e" s="T910">otuː </ts>
               <ts e="T912" id="Seg_5064" n="e" s="T911">ottoːruŋ." </ts>
               <ts e="T913" id="Seg_5066" n="e" s="T912">Itiː </ts>
               <ts e="T914" id="Seg_5068" n="e" s="T913">bu͡ollagɨna, </ts>
               <ts e="T915" id="Seg_5070" n="e" s="T914">bubaːt, </ts>
               <ts e="T916" id="Seg_5072" n="e" s="T915">kɨ͡ajɨ͡akpɨt </ts>
               <ts e="T917" id="Seg_5074" n="e" s="T916">hu͡oga. </ts>
               <ts e="T918" id="Seg_5076" n="e" s="T917">Kajtak </ts>
               <ts e="T919" id="Seg_5078" n="e" s="T918">ere </ts>
               <ts e="T920" id="Seg_5080" n="e" s="T919">itiː </ts>
               <ts e="T921" id="Seg_5082" n="e" s="T920">bu͡olan </ts>
               <ts e="T922" id="Seg_5084" n="e" s="T921">erer. </ts>
               <ts e="T923" id="Seg_5086" n="e" s="T922">Ol </ts>
               <ts e="T924" id="Seg_5088" n="e" s="T923">kördük </ts>
               <ts e="T925" id="Seg_5090" n="e" s="T924">karajaːččɨ </ts>
               <ts e="T926" id="Seg_5092" n="e" s="T925">etibit </ts>
               <ts e="T927" id="Seg_5094" n="e" s="T926">bihigi </ts>
               <ts e="T928" id="Seg_5096" n="e" s="T927">tabanɨ. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T5" id="Seg_5097" s="T0">KiMN_1975_ReindeerHerding_nar.001 (001.001)</ta>
            <ta e="T11" id="Seg_5098" s="T5">KiMN_1975_ReindeerHerding_nar.002 (001.002)</ta>
            <ta e="T17" id="Seg_5099" s="T11">KiMN_1975_ReindeerHerding_nar.003 (001.003)</ta>
            <ta e="T28" id="Seg_5100" s="T17">KiMN_1975_ReindeerHerding_nar.004 (001.004)</ta>
            <ta e="T35" id="Seg_5101" s="T28">KiMN_1975_ReindeerHerding_nar.005 (001.005)</ta>
            <ta e="T37" id="Seg_5102" s="T35">KiMN_1975_ReindeerHerding_nar.006 (001.006)</ta>
            <ta e="T41" id="Seg_5103" s="T37">KiMN_1975_ReindeerHerding_nar.007 (001.007)</ta>
            <ta e="T45" id="Seg_5104" s="T41">KiMN_1975_ReindeerHerding_nar.008 (001.008)</ta>
            <ta e="T55" id="Seg_5105" s="T45">KiMN_1975_ReindeerHerding_nar.009 (001.009)</ta>
            <ta e="T63" id="Seg_5106" s="T55">KiMN_1975_ReindeerHerding_nar.010 (001.010)</ta>
            <ta e="T70" id="Seg_5107" s="T63">KiMN_1975_ReindeerHerding_nar.011 (001.011)</ta>
            <ta e="T79" id="Seg_5108" s="T70">KiMN_1975_ReindeerHerding_nar.012 (001.012)</ta>
            <ta e="T88" id="Seg_5109" s="T79">KiMN_1975_ReindeerHerding_nar.013 (001.013)</ta>
            <ta e="T93" id="Seg_5110" s="T88">KiMN_1975_ReindeerHerding_nar.014 (001.014)</ta>
            <ta e="T104" id="Seg_5111" s="T93">KiMN_1975_ReindeerHerding_nar.015 (001.015)</ta>
            <ta e="T110" id="Seg_5112" s="T104">KiMN_1975_ReindeerHerding_nar.016 (001.016)</ta>
            <ta e="T115" id="Seg_5113" s="T110">KiMN_1975_ReindeerHerding_nar.017 (001.017)</ta>
            <ta e="T126" id="Seg_5114" s="T115">KiMN_1975_ReindeerHerding_nar.018 (001.018)</ta>
            <ta e="T135" id="Seg_5115" s="T126">KiMN_1975_ReindeerHerding_nar.019 (001.019)</ta>
            <ta e="T139" id="Seg_5116" s="T135">KiMN_1975_ReindeerHerding_nar.020 (001.020)</ta>
            <ta e="T147" id="Seg_5117" s="T139">KiMN_1975_ReindeerHerding_nar.021 (001.021)</ta>
            <ta e="T151" id="Seg_5118" s="T147">KiMN_1975_ReindeerHerding_nar.022 (001.022)</ta>
            <ta e="T155" id="Seg_5119" s="T151">KiMN_1975_ReindeerHerding_nar.023 (001.023)</ta>
            <ta e="T161" id="Seg_5120" s="T155">KiMN_1975_ReindeerHerding_nar.024 (001.024)</ta>
            <ta e="T165" id="Seg_5121" s="T161">KiMN_1975_ReindeerHerding_nar.025 (001.025)</ta>
            <ta e="T178" id="Seg_5122" s="T165">KiMN_1975_ReindeerHerding_nar.026 (001.026)</ta>
            <ta e="T197" id="Seg_5123" s="T178">KiMN_1975_ReindeerHerding_nar.027 (001.027)</ta>
            <ta e="T204" id="Seg_5124" s="T197">KiMN_1975_ReindeerHerding_nar.028 (001.028)</ta>
            <ta e="T220" id="Seg_5125" s="T204">KiMN_1975_ReindeerHerding_nar.029 (001.029)</ta>
            <ta e="T226" id="Seg_5126" s="T220">KiMN_1975_ReindeerHerding_nar.030 (001.030)</ta>
            <ta e="T230" id="Seg_5127" s="T226">KiMN_1975_ReindeerHerding_nar.031 (001.031)</ta>
            <ta e="T243" id="Seg_5128" s="T230">KiMN_1975_ReindeerHerding_nar.032 (001.032)</ta>
            <ta e="T245" id="Seg_5129" s="T243">KiMN_1975_ReindeerHerding_nar.033 (001.033)</ta>
            <ta e="T255" id="Seg_5130" s="T245">KiMN_1975_ReindeerHerding_nar.034 (001.034)</ta>
            <ta e="T266" id="Seg_5131" s="T255">KiMN_1975_ReindeerHerding_nar.035 (001.035)</ta>
            <ta e="T284" id="Seg_5132" s="T266">KiMN_1975_ReindeerHerding_nar.036 (001.036)</ta>
            <ta e="T293" id="Seg_5133" s="T284">KiMN_1975_ReindeerHerding_nar.037 (001.037)</ta>
            <ta e="T300" id="Seg_5134" s="T293">KiMN_1975_ReindeerHerding_nar.038 (001.038)</ta>
            <ta e="T314" id="Seg_5135" s="T300">KiMN_1975_ReindeerHerding_nar.039 (001.039)</ta>
            <ta e="T323" id="Seg_5136" s="T314">KiMN_1975_ReindeerHerding_nar.040 (001.040)</ta>
            <ta e="T335" id="Seg_5137" s="T323">KiMN_1975_ReindeerHerding_nar.041 (001.041)</ta>
            <ta e="T342" id="Seg_5138" s="T335">KiMN_1975_ReindeerHerding_nar.042 (001.042)</ta>
            <ta e="T368" id="Seg_5139" s="T342">KiMN_1975_ReindeerHerding_nar.043 (001.043)</ta>
            <ta e="T374" id="Seg_5140" s="T368">KiMN_1975_ReindeerHerding_nar.044 (001.044)</ta>
            <ta e="T383" id="Seg_5141" s="T374">KiMN_1975_ReindeerHerding_nar.045 (001.045)</ta>
            <ta e="T395" id="Seg_5142" s="T383">KiMN_1975_ReindeerHerding_nar.046 (001.046)</ta>
            <ta e="T399" id="Seg_5143" s="T395">KiMN_1975_ReindeerHerding_nar.047 (001.047)</ta>
            <ta e="T411" id="Seg_5144" s="T399">KiMN_1975_ReindeerHerding_nar.048 (001.048)</ta>
            <ta e="T415" id="Seg_5145" s="T411">KiMN_1975_ReindeerHerding_nar.049 (001.049)</ta>
            <ta e="T433" id="Seg_5146" s="T415">KiMN_1975_ReindeerHerding_nar.050 (001.050)</ta>
            <ta e="T435" id="Seg_5147" s="T433">KiMN_1975_ReindeerHerding_nar.051 (001.051)</ta>
            <ta e="T440" id="Seg_5148" s="T435">KiMN_1975_ReindeerHerding_nar.052 (001.052)</ta>
            <ta e="T457" id="Seg_5149" s="T440">KiMN_1975_ReindeerHerding_nar.053 (001.053)</ta>
            <ta e="T463" id="Seg_5150" s="T457">KiMN_1975_ReindeerHerding_nar.054 (001.054)</ta>
            <ta e="T474" id="Seg_5151" s="T463">KiMN_1975_ReindeerHerding_nar.055 (001.055)</ta>
            <ta e="T482" id="Seg_5152" s="T474">KiMN_1975_ReindeerHerding_nar.056 (001.056)</ta>
            <ta e="T484" id="Seg_5153" s="T482">KiMN_1975_ReindeerHerding_nar.057 (001.057)</ta>
            <ta e="T493" id="Seg_5154" s="T484">KiMN_1975_ReindeerHerding_nar.058 (001.058)</ta>
            <ta e="T505" id="Seg_5155" s="T493">KiMN_1975_ReindeerHerding_nar.059 (001.059)</ta>
            <ta e="T520" id="Seg_5156" s="T505">KiMN_1975_ReindeerHerding_nar.060 (001.060)</ta>
            <ta e="T532" id="Seg_5157" s="T520">KiMN_1975_ReindeerHerding_nar.061 (001.061)</ta>
            <ta e="T541" id="Seg_5158" s="T532">KiMN_1975_ReindeerHerding_nar.062 (001.062)</ta>
            <ta e="T556" id="Seg_5159" s="T541">KiMN_1975_ReindeerHerding_nar.063 (001.063)</ta>
            <ta e="T565" id="Seg_5160" s="T556">KiMN_1975_ReindeerHerding_nar.064 (001.064)</ta>
            <ta e="T571" id="Seg_5161" s="T565">KiMN_1975_ReindeerHerding_nar.065 (001.065)</ta>
            <ta e="T577" id="Seg_5162" s="T571">KiMN_1975_ReindeerHerding_nar.066 (001.066)</ta>
            <ta e="T583" id="Seg_5163" s="T577">KiMN_1975_ReindeerHerding_nar.067 (001.067)</ta>
            <ta e="T588" id="Seg_5164" s="T583">KiMN_1975_ReindeerHerding_nar.068 (001.068)</ta>
            <ta e="T594" id="Seg_5165" s="T588">KiMN_1975_ReindeerHerding_nar.069 (001.069)</ta>
            <ta e="T608" id="Seg_5166" s="T594">KiMN_1975_ReindeerHerding_nar.070 (001.070)</ta>
            <ta e="T613" id="Seg_5167" s="T608">KiMN_1975_ReindeerHerding_nar.071 (001.071)</ta>
            <ta e="T622" id="Seg_5168" s="T613">KiMN_1975_ReindeerHerding_nar.072 (001.072)</ta>
            <ta e="T628" id="Seg_5169" s="T622">KiMN_1975_ReindeerHerding_nar.073 (001.073)</ta>
            <ta e="T645" id="Seg_5170" s="T628">KiMN_1975_ReindeerHerding_nar.074 (001.074)</ta>
            <ta e="T657" id="Seg_5171" s="T645">KiMN_1975_ReindeerHerding_nar.075 (001.075)</ta>
            <ta e="T668" id="Seg_5172" s="T657">KiMN_1975_ReindeerHerding_nar.076 (001.076)</ta>
            <ta e="T671" id="Seg_5173" s="T668">KiMN_1975_ReindeerHerding_nar.077 (001.077)</ta>
            <ta e="T676" id="Seg_5174" s="T671">KiMN_1975_ReindeerHerding_nar.078 (001.078)</ta>
            <ta e="T690" id="Seg_5175" s="T676">KiMN_1975_ReindeerHerding_nar.079 (001.079)</ta>
            <ta e="T693" id="Seg_5176" s="T690">KiMN_1975_ReindeerHerding_nar.080 (001.080)</ta>
            <ta e="T699" id="Seg_5177" s="T693">KiMN_1975_ReindeerHerding_nar.081 (001.081)</ta>
            <ta e="T710" id="Seg_5178" s="T699">KiMN_1975_ReindeerHerding_nar.082 (001.082)</ta>
            <ta e="T713" id="Seg_5179" s="T710">KiMN_1975_ReindeerHerding_nar.083 (001.083)</ta>
            <ta e="T717" id="Seg_5180" s="T713">KiMN_1975_ReindeerHerding_nar.084 (001.083)</ta>
            <ta e="T731" id="Seg_5181" s="T717">KiMN_1975_ReindeerHerding_nar.085 (001.084)</ta>
            <ta e="T748" id="Seg_5182" s="T731">KiMN_1975_ReindeerHerding_nar.086 (001.085)</ta>
            <ta e="T757" id="Seg_5183" s="T748">KiMN_1975_ReindeerHerding_nar.087 (001.086)</ta>
            <ta e="T767" id="Seg_5184" s="T757">KiMN_1975_ReindeerHerding_nar.088 (001.087)</ta>
            <ta e="T771" id="Seg_5185" s="T767">KiMN_1975_ReindeerHerding_nar.089 (001.088)</ta>
            <ta e="T776" id="Seg_5186" s="T771">KiMN_1975_ReindeerHerding_nar.090 (001.089)</ta>
            <ta e="T785" id="Seg_5187" s="T776">KiMN_1975_ReindeerHerding_nar.091 (001.090)</ta>
            <ta e="T789" id="Seg_5188" s="T785">KiMN_1975_ReindeerHerding_nar.092 (001.091)</ta>
            <ta e="T791" id="Seg_5189" s="T789">KiMN_1975_ReindeerHerding_nar.093 (001.092)</ta>
            <ta e="T799" id="Seg_5190" s="T791">KiMN_1975_ReindeerHerding_nar.094 (001.093)</ta>
            <ta e="T801" id="Seg_5191" s="T799">KiMN_1975_ReindeerHerding_nar.095 (001.094)</ta>
            <ta e="T805" id="Seg_5192" s="T801">KiMN_1975_ReindeerHerding_nar.096 (001.095)</ta>
            <ta e="T810" id="Seg_5193" s="T805">KiMN_1975_ReindeerHerding_nar.097 (001.096)</ta>
            <ta e="T820" id="Seg_5194" s="T810">KiMN_1975_ReindeerHerding_nar.098 (001.097)</ta>
            <ta e="T827" id="Seg_5195" s="T820">KiMN_1975_ReindeerHerding_nar.099 (001.098)</ta>
            <ta e="T830" id="Seg_5196" s="T827">KiMN_1975_ReindeerHerding_nar.100 (001.099)</ta>
            <ta e="T853" id="Seg_5197" s="T830">KiMN_1975_ReindeerHerding_nar.101 (001.100)</ta>
            <ta e="T865" id="Seg_5198" s="T853">KiMN_1975_ReindeerHerding_nar.102 (001.101)</ta>
            <ta e="T868" id="Seg_5199" s="T865">KiMN_1975_ReindeerHerding_nar.103 (001.102)</ta>
            <ta e="T888" id="Seg_5200" s="T868">KiMN_1975_ReindeerHerding_nar.104 (001.103)</ta>
            <ta e="T896" id="Seg_5201" s="T888">KiMN_1975_ReindeerHerding_nar.105 (001.104)</ta>
            <ta e="T903" id="Seg_5202" s="T896">KiMN_1975_ReindeerHerding_nar.106 (001.105)</ta>
            <ta e="T909" id="Seg_5203" s="T903">KiMN_1975_ReindeerHerding_nar.107 (001.106)</ta>
            <ta e="T912" id="Seg_5204" s="T909">KiMN_1975_ReindeerHerding_nar.108 (001.107)</ta>
            <ta e="T917" id="Seg_5205" s="T912">KiMN_1975_ReindeerHerding_nar.109 (001.108)</ta>
            <ta e="T922" id="Seg_5206" s="T917">KiMN_1975_ReindeerHerding_nar.110 (001.109)</ta>
            <ta e="T928" id="Seg_5207" s="T922">KiMN_1975_ReindeerHerding_nar.111 (001.110)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T5" id="Seg_5208" s="T0">Дьэ мин табаһыттарга иһиллэтээри һаӈарабын.</ta>
            <ta e="T11" id="Seg_5209" s="T5">Мин үгүстүк үлэлээбит киһибин табага, урут.</ta>
            <ta e="T17" id="Seg_5210" s="T11">Бу таба төрүүрүн туһунан һаӈарыам аны.</ta>
            <ta e="T28" id="Seg_5211" s="T17">Каннык да отто төрүүрүттэн буолар буо, таба да үөскүүрэ, киһи дэ (да).</ta>
            <ta e="T35" id="Seg_5212" s="T28">Онон мин һаӈарабын дьэ бугурдук таба төрүүрүгэр.</ta>
            <ta e="T37" id="Seg_5213" s="T35">Пастууктарга иһиллэтээри.</ta>
            <ta e="T41" id="Seg_5214" s="T37">Мин һаӈарарым дьэ бугурдуккаан.</ta>
            <ta e="T45" id="Seg_5215" s="T41">Аны бу табаны карайаллар.</ta>
            <ta e="T55" id="Seg_5216" s="T45">Мин урут карайа һылдьар эрдэкпинэ, таба төрөөтөгүнэ, үгүс причинэлээк буолар. </ta>
            <ta e="T63" id="Seg_5217" s="T55">Таба төрүүрүн, пастух эрдэкпинэ, кэтии һылдьаммын кэрийээччи этим.</ta>
            <ta e="T70" id="Seg_5218" s="T63">Таба төрөөн, оготун түһэрдэгинэ эӈин-эӈинник төрүүр таба.</ta>
            <ta e="T79" id="Seg_5219" s="T70">Һорок таба тугута, төрөөн бараан, тэбиэлэнэ һытааччы, котон турбакка.</ta>
            <ta e="T88" id="Seg_5220" s="T79">Иньэтин көрдөккө эмис багай, тугутун көрдөккө улакан багай буолар.</ta>
            <ta e="T93" id="Seg_5221" s="T88">Бу тэбиэлэнэ һытары эргичитиэккэ наада.</ta>
            <ta e="T104" id="Seg_5222" s="T93">Эргичитэн көрдөккө – туугу гынан того турбат диэн маны ыбактыакка (убактыакка), быарын ыбактыакка (убактыакка).</ta>
            <ta e="T110" id="Seg_5223" s="T104">Таба төрөөтөгүнэ, тугут гиэнэ кииннээк буолааччы.</ta>
            <ta e="T115" id="Seg_5224" s="T110">Бу киинин һорок таба һубуйаaччы.</ta>
            <ta e="T126" id="Seg_5225" s="T115">Бу киинэ һубулуннагына бунан, иһиттэн, һупту үүт буолар, онон тыына таксар.</ta>
            <ta e="T135" id="Seg_5226" s="T126">Муотунан кам баайдакка, бу таба, бу тугут, туран кэлээччи.</ta>
            <ta e="T139" id="Seg_5227" s="T135">Ол аата "киинин һубуйар".</ta>
            <ta e="T147" id="Seg_5228" s="T139">Ити буолар таба гиэнэ, тугутун гиэнэ, причинэтэ төрөөтөгүнэ.</ta>
            <ta e="T151" id="Seg_5229" s="T147">Онтон биир причинэлээк – слабай.</ta>
            <ta e="T155" id="Seg_5230" s="T151">Таба төрүүр, торуган иньэтэ.</ta>
            <ta e="T161" id="Seg_5231" s="T155">Бу тугута төрөөтөгүнэ Дьапчу диэн аaттанааччы.</ta>
            <ta e="T165" id="Seg_5232" s="T161">Эмиэ тугута турааччыта һуок.</ta>
            <ta e="T178" id="Seg_5233" s="T165">Бу тугуту эргичитэн көрдөккө, аматтан слабай багай буолар, бу тугут тэбиэлэнэ агай һытааччы.</ta>
            <ta e="T197" id="Seg_5234" s="T178">Бу тугуту иттэннэ тутан бараан түөһүн имэрийдэккэ, оччого бу түөһэ Дьапчу гиэнэ, каһан да тугут төрөөтөгүнэ, оӈуога оӈургас буолар.</ta>
            <ta e="T204" id="Seg_5235" s="T197">Буны түөһүн имэрийдэккэ, тура-тура түһэ һылдьааччы, тыыннагына.</ta>
            <ta e="T220" id="Seg_5236" s="T204">Маны иньэтин тутан бараан, иньэтин эмнэриэккэ наада, бу тугут түөһүн кими илбийбитин кэннэ котон эмээччитэ һуок.</ta>
            <ta e="T226" id="Seg_5237" s="T220">Маны иньэтэ эмийэ ыастаак буола, лампагар.</ta>
            <ta e="T230" id="Seg_5238" s="T226">Бу ыаһын ыгааччы киһи. </ta>
            <ta e="T243" id="Seg_5239" s="T230">Ыган бараан, ыбагас (убагас) эмийэ кэллэгинэ, оччого эмнэрээччи, оччого эмнэрдэгинэ, бу тугут киһи буолар.</ta>
            <ta e="T245" id="Seg_5240" s="T243">Пастууктар көһөллөр.</ta>
            <ta e="T255" id="Seg_5241" s="T245">Үгүс таба төрөөтөгүнэ каһан да төрүүлик (төрүү илик) табаны инни диэк иллээччилэр.</ta>
            <ta e="T266" id="Seg_5242" s="T255">Төрөөбүт табаны, тугута улаата түһүөр диэри каампатын иһин кэнни диэк кээһэллэр.</ta>
            <ta e="T284" id="Seg_5243" s="T266">Бу кээһиилэригэр, бу тэ уонтан такса, һүүрбэччэ таба төрөөбүтүн кэннэ, оччого бу төрүүлик (төрүү илик) табаны инни диэк иллиэккэ наада.</ta>
            <ta e="T293" id="Seg_5244" s="T284">Бу табаны иллэргэ һорок пастуук бу төрөөбүт тугуттаакка каалар.</ta>
            <ta e="T300" id="Seg_5245" s="T293">Һорок пастуук бу (төрөө..) төрүүлик (төрүү илик) табаны иллэр.</ta>
            <ta e="T314" id="Seg_5246" s="T300">Бу иллэригэр аһатан албыннаан калка һиргэ кииллэрэн, үөрэ барарын көллөрбөккө, бу тугуттаак тыһыны… </ta>
            <ta e="T323" id="Seg_5247" s="T314">каһан да таба һаньыһан тугутун кэһээччи батыһан, үөрүгэр талаһан…</ta>
            <ta e="T335" id="Seg_5248" s="T323">ону аһата һылдьан топпутун кэмнэнэ һытарыгар төрүүлик (төрүү илик) табатын инни диэк көһөрдөн иллэр.</ta>
            <ta e="T342" id="Seg_5249" s="T335">Ол көһөртөктөрүнэ, һорок пастууктар һүрдээк ыраак барааччылар.</ta>
            <ta e="T368" id="Seg_5250" s="T342">Бу ыраак бардактарына, бу тугут һарсынӈытын ол төрүүлик (төрүү илик) табалаакка кэнникиитин, пастууктарын ол манин (баадьыгы) төрөөбүт тыһылаактарын пастуукта, инни диэк пастууктарга иллээрилэр, көһөллөрүгэр таба котон тийэччитэ һуок бо.</ta>
            <ta e="T374" id="Seg_5251" s="T368">Ол иһин чугас түһүөккэ көрөн бараан.</ta>
            <ta e="T383" id="Seg_5252" s="T374">Онтон буоллагына бу пастууктар каһан да калкалаак һиринэн һылдьаллар.</ta>
            <ta e="T395" id="Seg_5253" s="T383">Маршрут көрөллөр таба төрөөтөрүгэр калкалаак һиринэн, оһурдаак һиринэн, кайалардаак һирдэринэн, үрэктэрдээк һирдэринэн.</ta>
            <ta e="T399" id="Seg_5254" s="T395">Бу улакан һобуой быстааччы.</ta>
            <ta e="T411" id="Seg_5255" s="T399">Бу быһыннагына, һаас таба төрүүрүн һагына күн һырдаатагына, кыһын быстыбыт һобуой атааччы.</ta>
            <ta e="T415" id="Seg_5256" s="T411">Ол аата көӈүс буолар.</ta>
            <ta e="T433" id="Seg_5257" s="T415">Таба һурага һуок һүтэрэ, ол көӈүс иһигэр түспүт табаны бу пастуук кэтии һылдьан билбэт, кас да таба түспүтүн.</ta>
            <ta e="T435" id="Seg_5258" s="T433">Һонон һуоктаабат.</ta>
            <ta e="T440" id="Seg_5259" s="T435">Таба һурага һуок һүтэрэ ол.</ta>
            <ta e="T457" id="Seg_5260" s="T440">Онон бригадиирдэр буол, пастууктар буол, һаас таба төрүүрүн һагына… каар ирэрин һагына үчүгэйдик һирдэрин көрүөктэрин наада.</ta>
            <ta e="T463" id="Seg_5261" s="T457">Һобуой кайдыбыта пурга буоллагына тибиллэн каалааччы.</ta>
            <ta e="T474" id="Seg_5262" s="T463">Ол гынан бараан, ол тибиллибитин кэннэ ону таба көрбөт, һонон түһэр.</ta>
            <ta e="T482" id="Seg_5263" s="T474">Ол түстэгинэ биэс-түөрт метирдээк дириӈнээк буолааччы бу көӈүс.</ta>
            <ta e="T484" id="Seg_5264" s="T482">Каар быстыбыт.</ta>
            <ta e="T493" id="Seg_5265" s="T484">Ол иһин оннугу булбакка, таба һурага һуок һүтэрэ ити.</ta>
            <ta e="T505" id="Seg_5266" s="T493">Онтон үрэктээк һирдэргэ буоллагына, карадьыктаатагына, каһан да үрэктээк һиринэн буолар маршрут бу.</ta>
            <ta e="T520" id="Seg_5267" s="T505">Каары гытта кайа һир ыккардыта атааччы эмиэ, онон эмиэ түһээччи тугут буол, улакан таба буол.</ta>
            <ta e="T532" id="Seg_5268" s="T520">Онтон бу пастууктар (һаас..) һайыннара чугаһаатагына һааскыларыгар, уучактарын һатаан карамматтар, күнүктэтин тонооботтор.</ta>
            <ta e="T541" id="Seg_5269" s="T532">Һаас күнүктэтин тоноотокторуна, ол күнүктэтин тоноотокторуна бу күнүктэ оһор.</ta>
            <ta e="T556" id="Seg_5270" s="T541">Бэйэтэ күнүктэ һаас улаатан тагыстагына, тириитин иһиттэн бэйэтэ туллан түһүүр диэри һырыттагына, табага да куһаган.</ta>
            <ta e="T565" id="Seg_5271" s="T556">Һорок күнүктэни бүтэйдии миинэ-миинэлэр таба гиэнэ өлөр үнэрэ ол.</ta>
            <ta e="T571" id="Seg_5272" s="T565">Һорок пастуук табаны да көлүнэ һатаабат.</ta>
            <ta e="T577" id="Seg_5273" s="T571">Побудуоктара быһынна, таба гиэнэн һамагын ааллараллар.</ta>
            <ta e="T583" id="Seg_5274" s="T577">Һамагын ааллардактарына, бу һайын баас буолааччы.</ta>
            <ta e="T588" id="Seg_5275" s="T583">Манан таба… һутуура ити.</ta>
            <ta e="T594" id="Seg_5276" s="T588">Онтон кутуругун таба гиэнин кайа анньаллар.</ta>
            <ta e="T608" id="Seg_5277" s="T594">Бу кутуругун кайа астактарына кыһын тымныыга бу таба гиэнэ этэ каччага да эмиэ көппөт.</ta>
            <ta e="T613" id="Seg_5278" s="T608">Онтон һайын, итии һагына көһөллөр.</ta>
            <ta e="T622" id="Seg_5279" s="T613">Бу көстөктөрүнэ, итиигэ көстөктөрүнэ, көһөн иһэннэр үрэктээк һирдэринэн бараллар.</ta>
            <ta e="T628" id="Seg_5280" s="T622">Бу үрэктээк улакан кайаларга туойдаак буолааччы.</ta>
            <ta e="T645" id="Seg_5281" s="T628">Төһө да итиигэ бу туойы һэрэйдэ да, таба бу кайага барааччи, көһөн истэккэ да үүрэн да истэктэринэн.</ta>
            <ta e="T657" id="Seg_5282" s="T645">Маны пастууктар, бу кайаны көрбөккө, ыттаан ыллылар даа, һонон күлсэ-кулсэ ааһан каалаллар. </ta>
            <ta e="T668" id="Seg_5283" s="T657">Манна уон ордуга биэс дуу, һүрбэ ордуга биэс дуу таба каалар.</ta>
            <ta e="T671" id="Seg_5284" s="T668">Бу каалбытын өйдөөбөттөр.</ta>
            <ta e="T676" id="Seg_5285" s="T671">Һурка түһэннэр нөӈүө киһи кэтиир.</ta>
            <ta e="T690" id="Seg_5286" s="T676">Маадин (бадьыгы) бу көһөллэригэр кэтээбит киһи, арай угучаага ол табаларга онно кайага каалбыт биир угучага.</ta>
            <ta e="T693" id="Seg_5287" s="T690">Һарсынӈытын көһөллөр эмиэ.</ta>
            <ta e="T699" id="Seg_5288" s="T693">Көһөн түһэн бараан нөӈүө киһилэр кэтииллэр. </ta>
            <ta e="T710" id="Seg_5289" s="T699">Маа ынараа һурка кэтээбит артыыннаак киһи, кэтии таксан уучагын оччого һуоктуур.</ta>
            <ta e="T713" id="Seg_5290" s="T710">Һарсиэрдэ кэлэн диир: </ta>
            <ta e="T717" id="Seg_5291" s="T713">"Кайа, миниэнэ уучагым һуок.</ta>
            <ta e="T731" id="Seg_5292" s="T717">Онно һуурка да көрбөтөгүм, маадин(байдьыгы) көспүт һуурпар, ынараа һуурка кэтиирбэр миинэ һылдьыбыт табам этэ."</ta>
            <ta e="T748" id="Seg_5293" s="T731">"Кирдик да, онно һуурка каалбыт буолла", – дии дииллэр ол табаны бу һууртарыгар көрдүү кэлэллэр, бүгүн көспүт һуурдарыгар.</ta>
            <ta e="T757" id="Seg_5294" s="T748">Ол табалары, ынараа көскө каалбыт табалары бэтэрээ һуурка көрдүүллэр.</ta>
            <ta e="T767" id="Seg_5295" s="T757">Ол ынараа һуурка кэтээбит пастуук угучага онно каалбыт, ол кайага.</ta>
            <ta e="T771" id="Seg_5296" s="T767">Ол һоготок табаны өйдүүллэр.</ta>
            <ta e="T776" id="Seg_5297" s="T771">Онно каалбыта уонтан такса таба.</ta>
            <ta e="T785" id="Seg_5298" s="T776">Ол иһин без вести таба һурага һуок һүтэрэ – ити.</ta>
            <ta e="T789" id="Seg_5299" s="T785">Онтон дьыл эӈин-эӈин буолааччы.</ta>
            <ta e="T791" id="Seg_5300" s="T789">Итиилиэк буолааччы.</ta>
            <ta e="T799" id="Seg_5301" s="T791">Бу итиигэ эмиэ аныгы пастууктар табаны һатаан карайбаттар.</ta>
            <ta e="T801" id="Seg_5302" s="T799">Карайбаттара туугуй!?</ta>
            <ta e="T805" id="Seg_5303" s="T801">Итиигэ һалгындьыт таба буолааччы.</ta>
            <ta e="T810" id="Seg_5304" s="T805">Һалгындьыт таба аата тыалы батар.</ta>
            <ta e="T820" id="Seg_5305" s="T810">Бу тыалы батар табаны, итии аһыыр диэри киһи табаттан баайааччы.</ta>
            <ta e="T827" id="Seg_5306" s="T820">Таба кэтээтэктэринэ, киһи күүһэ тийээччитэ һуок итиигэ.</ta>
            <ta e="T830" id="Seg_5307" s="T827">Тыалы ((…)) гымматтар.</ta>
            <ta e="T853" id="Seg_5308" s="T830">Һарсиэрдэ эгэлэллэригэр нөӈүө пастуукка туттаралларыгар: "Биһиги кыайыакпыт һуога эт," – диэн каһан да тыал анны диэк дарабии гыныактарын, дьиэлэрэ тыал үрдү бү буолар көрдүк.</ta>
            <ta e="T865" id="Seg_5309" s="T853">Таба оччого кыайбакка тыал утара (утары) кэллэгинэ, дьиэтигэр кэптэрдэгинэ, дьиэтээгилэрэ тураннар манна көмөлөһөннөр.</ta>
            <ta e="T868" id="Seg_5310" s="T865">Оччого кыайаллар табаны.</ta>
            <ta e="T888" id="Seg_5311" s="T868">Ол кэннинэ гинилэр һааскыттан итии түһэ илигинэ, һаӈардыы иттии түһэригэр, таба итииргииригэр буруога табаны үөрэтиэктэрин нада, улакан итии түһэ илигинэ.</ta>
            <ta e="T896" id="Seg_5312" s="T888">Буруону көрдө да төһө даа итиигэ таба… </ta>
            <ta e="T903" id="Seg_5313" s="T896">Дьиэтээгилэрэ итиигэ пастууктар кэтии бараарылар һаӈарыак тустаактар: </ta>
            <ta e="T909" id="Seg_5314" s="T903">"Кайа, итии буоллагына дьэ кэчэһэн утуйуӈ.</ta>
            <ta e="T912" id="Seg_5315" s="T909">Буруолатаарыӈ, отуу оттооруӈ."</ta>
            <ta e="T917" id="Seg_5316" s="T912">Итии буоллагына, бубаат, кыайыакпыт һуога.</ta>
            <ta e="T922" id="Seg_5317" s="T917">Кайтак эрэ итии буолан эрэр.</ta>
            <ta e="T928" id="Seg_5318" s="T922">Ол көрдүк карайаaччы этибит биһиги табаны. </ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T5" id="Seg_5319" s="T0">Dʼe min tabahɨttarga ihilleteːri haŋarabɨn. </ta>
            <ta e="T11" id="Seg_5320" s="T5">Min ügüstük üleleːbit kihibin tabaga, urut. </ta>
            <ta e="T17" id="Seg_5321" s="T11">Bu taba törüːrün tuhunan haŋarɨ͡am anɨ. </ta>
            <ta e="T28" id="Seg_5322" s="T17">Kannɨk da otto törüːrütten bu͡olar bu͡o, taba da ü͡ösküːre, kihi de. </ta>
            <ta e="T35" id="Seg_5323" s="T28">Onon min haŋarabɨn dʼe bugurduk taba törüːrüger. </ta>
            <ta e="T37" id="Seg_5324" s="T35">Pastuːxtarga ihilleteːri. </ta>
            <ta e="T41" id="Seg_5325" s="T37">Min haŋararɨm dʼe bugurdukkaːn. </ta>
            <ta e="T45" id="Seg_5326" s="T41">Anɨ bu tabanɨ karajallar. </ta>
            <ta e="T55" id="Seg_5327" s="T45">Min urut karaja hɨldʼar erdekpine, taba töröːtögüne, ügüs prʼičʼineleːk bu͡olar. </ta>
            <ta e="T63" id="Seg_5328" s="T55">Taba törüːrün, pastuːx erdekpine, ketiː hɨldʼammɨn kerijeːčči etim. </ta>
            <ta e="T70" id="Seg_5329" s="T63">Taba töröːn, ogotun tüherdegine eŋin-eŋinnik törüːr taba. </ta>
            <ta e="T79" id="Seg_5330" s="T70">Horok taba tuguta, töröːn baraːn, tebi͡elene hɨtaːččɨ, koton turbakka. </ta>
            <ta e="T88" id="Seg_5331" s="T79">Inʼetin kördökkö emis bagaj, tugutun kördökkö ulakan bagaj bu͡olar. </ta>
            <ta e="T93" id="Seg_5332" s="T88">Bu tebi͡elene hɨtarɨ ergičiti͡ekke naːda. </ta>
            <ta e="T104" id="Seg_5333" s="T93">Ergičiten kördökkö – tuːgu gɨnan togo turbat di͡en manɨ ɨbaktɨ͡akka, bɨ͡arɨn ɨbaktɨ͡akka. </ta>
            <ta e="T110" id="Seg_5334" s="T104">Taba töröːtögüne, tugut gi͡ene kiːnneːk bu͡olaːččɨ. </ta>
            <ta e="T115" id="Seg_5335" s="T110">Bu kiːnin horok taba hubujaːččɨ. </ta>
            <ta e="T126" id="Seg_5336" s="T115">Bu kiːne hubulunnagɨna bunan, ihitten, huptu üːt bu͡olar, onon tɨːna taksar. </ta>
            <ta e="T135" id="Seg_5337" s="T126">Mu͡otunan kam baːjdakka, bu taba, bu tugut, turan keleːčči. </ta>
            <ta e="T139" id="Seg_5338" s="T135">Ol aːta "kiːnin hubujar". </ta>
            <ta e="T147" id="Seg_5339" s="T139">Iti bu͡olar taba gi͡ene, tugutun gi͡ene, prʼičʼinete töröːtögüne. </ta>
            <ta e="T151" id="Seg_5340" s="T147">Onton biːr prʼičʼineleːk – slabaj. </ta>
            <ta e="T155" id="Seg_5341" s="T151">Taba törüːr, torugan inʼete. </ta>
            <ta e="T161" id="Seg_5342" s="T155">Bu tuguta töröːtögüne dʼapču di͡en aːttanaːččɨ. </ta>
            <ta e="T165" id="Seg_5343" s="T161">Emi͡e tuguta turaːččɨta hu͡ok. </ta>
            <ta e="T178" id="Seg_5344" s="T165">Bu tugutu ergičiten kördökkö, amattan slabaj bagaj bu͡olar, bu tugut tebi͡elene agaj hɨtaːččɨ. </ta>
            <ta e="T197" id="Seg_5345" s="T178">Bu tugutu ittenne tutan baraːn tü͡öhün imerijdekke, oččogo bu tü͡öhe Dʼapču gi͡ene, kahan da tugut töröːtögüne, oŋu͡oga oŋurgas bu͡olar. </ta>
            <ta e="T204" id="Seg_5346" s="T197">Bunɨ tü͡öhün imerijdekke, tura-tura tühe hɨldʼaːččɨ, tɨːnnagɨna. </ta>
            <ta e="T220" id="Seg_5347" s="T204">Manɨ inʼetin tutan baraːn, inʼetin emneri͡ekke naːda, bu tugut tü͡öhün kimi ilbijbitin kenne koton emeːččite hu͡ok. </ta>
            <ta e="T226" id="Seg_5348" s="T220">Manɨ inʼete emije ɨ͡astaːk bu͡olla, lampagar. </ta>
            <ta e="T230" id="Seg_5349" s="T226">Bu ɨ͡ahɨ ɨgaːččɨ kihi. </ta>
            <ta e="T243" id="Seg_5350" s="T230">ɨgan baraːn, ɨbagas emije kellegine, oččogo emnereːčči, oččogo emnerdegine, bu tugut kihi bu͡olar. </ta>
            <ta e="T245" id="Seg_5351" s="T243">Pastuːktar köhöllör. </ta>
            <ta e="T255" id="Seg_5352" s="T245">Ügüs taba töröːtögüne kahan da törüːlik tabanɨ inni di͡ek illeːččiler. </ta>
            <ta e="T266" id="Seg_5353" s="T255">Töröːbüt tabanɨ, tuguta ulaːta tühü͡ör di͡eri kaːmpatɨn ihin kenni di͡ek keːheller. </ta>
            <ta e="T284" id="Seg_5354" s="T266">Bu keːhiːleriger, bu te u͡ontan taksa, hüːrbečče taba töröːbütün kenne, oččogo bu törüːlik tabanɨ inni di͡ek illi͡ekke naːda. </ta>
            <ta e="T293" id="Seg_5355" s="T284">Bu tabanɨ illerge horok pastuːk bu töröːbüt tuguttaːkka kaːlar. </ta>
            <ta e="T300" id="Seg_5356" s="T293">Horok pastuːk bu (töröː-) törüːlik tabanɨ iller. </ta>
            <ta e="T314" id="Seg_5357" s="T300">Bu illeriger ahatan albɨnnaːn kalka hirge kiːlleren, ü͡öre bararɨn köllörbökkö, bu tuguttaːk tɨhɨnɨ… </ta>
            <ta e="T323" id="Seg_5358" s="T314">Kahan da taba hannʼɨhan tugutun keːheːčči batɨhan, ü͡örüger talahan. </ta>
            <ta e="T335" id="Seg_5359" s="T323">Onu ahata hɨldʼan topputun kemnene hɨtarɨgar törüːlik tabatɨn inni di͡ek köhördön iller. </ta>
            <ta e="T342" id="Seg_5360" s="T335">Ol köhörtöktörüne, horok pastuːktar hürdeːk ɨraːk baraːččɨlar. </ta>
            <ta e="T368" id="Seg_5361" s="T342">Bu ɨraːk bardaktarɨna, bu tugut harsɨŋŋɨtɨn ol törüːlik tabalaːkka kennikiːtin, pastuːktarɨn ol manɨ töröːbüt tɨhɨlaːktarɨn pastuːkta, inni di͡ek pastuːktarga illeːriler, köhöllörüger taba koton tijeːččite hu͡ok bo. </ta>
            <ta e="T374" id="Seg_5362" s="T368">Ol ihin čugas tühü͡ökke körön baraːn. </ta>
            <ta e="T383" id="Seg_5363" s="T374">Onton bu͡ollagɨna bu pastuːktar kahan da kalkalaːk hirinen hɨldʼallar. </ta>
            <ta e="T395" id="Seg_5364" s="T383">Maršrut köröllör taba törötörüger kalkalaːk hirinen, ohuːrdaːk hirinen, kajalardaːk hirderinen, ürekterdeːk hirderinen. </ta>
            <ta e="T399" id="Seg_5365" s="T395">Bu ulakan hobu͡oj bɨstaːččɨ. </ta>
            <ta e="T411" id="Seg_5366" s="T399">Bu bɨhɨnnagɨna, haːs taba törüːrün hagɨna kün hɨrdaːtagɨna, kɨhɨn bɨstɨbɨt hobu͡oj ataːččɨ. </ta>
            <ta e="T415" id="Seg_5367" s="T411">Ol aːta köŋüs bu͡olar. </ta>
            <ta e="T433" id="Seg_5368" s="T415">Taba huraga hu͡ok hütere, ol köŋüs ihiger tüspüt tabanɨ bu pastuːk ketiː hɨldʼan bilbet, kas da taba tüspütün. </ta>
            <ta e="T435" id="Seg_5369" s="T433">Honon hu͡oktaːbat. </ta>
            <ta e="T440" id="Seg_5370" s="T435">Taba huraga hu͡ok hütere ol. </ta>
            <ta e="T457" id="Seg_5371" s="T440">Onon brʼigadʼiːrder bu͡ol, pastuːktar bu͡ol, haːs taba törüːrün hagɨna ((PAUSE)) kaːr irerin hagɨna üčügejdik hirderin körü͡ökterin naːda. </ta>
            <ta e="T463" id="Seg_5372" s="T457">Hobu͡oj kajdɨbɨta purga bu͡ollagɨna tibillen kaːlaːččɨ. </ta>
            <ta e="T474" id="Seg_5373" s="T463">Ol gɨnan baraːn, ol tibillibitin kenne onu taba körböt, honon tüher. </ta>
            <ta e="T482" id="Seg_5374" s="T474">Ol tüstegine bi͡es-tü͡ört mʼetʼirdeːk diriŋneːk bu͡olaːččɨ bu köŋüs. </ta>
            <ta e="T484" id="Seg_5375" s="T482">Kaːr bɨstɨbɨt. </ta>
            <ta e="T493" id="Seg_5376" s="T484">Ol ihin onnugu bulbakka, taba huraga hu͡ok hüter iti. </ta>
            <ta e="T505" id="Seg_5377" s="T493">Onton ürekteːk hirderge bu͡ollagɨna, karadʼɨktaːtagɨna, kahan da ürekteːk hirinen bu͡olar maršrut bu. </ta>
            <ta e="T520" id="Seg_5378" s="T505">Kaːrɨ gɨtta kaja hir ɨkkardɨta ataːččɨ emi͡e, onon emi͡e tüheːčči tugut bu͡ol, ulakan taba bu͡ol. </ta>
            <ta e="T532" id="Seg_5379" s="T520">Onton bu pastuːktar (haːs-) hajɨnnara čugahaːtagɨna haːskɨlarɨgar, uːčaktarɨn hataːn karammattar, künüktetin tonoːbottor. </ta>
            <ta e="T541" id="Seg_5380" s="T532">Haːs künüktetin tonoːtoktoruna, ol künüktetin tonoːtoktoruna bu künükte ohor. </ta>
            <ta e="T556" id="Seg_5381" s="T541">Bejete künükte haːs ulaːtan tagɨstagɨna, tiriːtin ihitten bejete tullan tühüːr di͡eri hɨrɨttagɨna, tabaga da kuhagan. </ta>
            <ta e="T565" id="Seg_5382" s="T556">Horok künükteni bütejdiː miːne-miːneler taba gi͡ene ölör ünere ol. </ta>
            <ta e="T571" id="Seg_5383" s="T565">Horok pastuːk tabanɨ da kölüne hataːbat. </ta>
            <ta e="T577" id="Seg_5384" s="T571">Pabudu͡oktara bɨhɨnna, taba gi͡enen hamagɨn aːllarallar. </ta>
            <ta e="T583" id="Seg_5385" s="T577">Hamagɨn aːllardaktarɨna, bu hajɨn baːs bu͡olaːččɨ. </ta>
            <ta e="T588" id="Seg_5386" s="T583">Manan taba ((PAUSE)) һutuːra iti. </ta>
            <ta e="T594" id="Seg_5387" s="T588">Onton kuturugun taba gi͡enin kaja annʼallar. </ta>
            <ta e="T608" id="Seg_5388" s="T594">Bu kuturugun kaja astaktarɨna kɨhɨn tɨmnɨːga bu taba gi͡ene ete kaččaga da emi͡e köppöt. </ta>
            <ta e="T613" id="Seg_5389" s="T608">Onton hajɨn, itiː hagɨna köhöllör. </ta>
            <ta e="T622" id="Seg_5390" s="T613">Bu köstöktörüne, itiːge köstöktörüne, köhön ihenner ürekteːk hirderinen barallar. </ta>
            <ta e="T628" id="Seg_5391" s="T622">Bu ürekteːk ulakan kajalarga tu͡ojdaːk bu͡olaːččɨ. </ta>
            <ta e="T645" id="Seg_5392" s="T628">Töhö da itiːge bu tu͡ojɨ herejde da, taba bu kajaga baraːččɨ, köhön istekke da üːren da istekterinen. </ta>
            <ta e="T657" id="Seg_5393" s="T645">Manɨ pastuːktar, bu kajanɨ körbökkö, ɨttaːn ɨllɨlar daː, honon külse-külse aːhan kaːlallar. </ta>
            <ta e="T668" id="Seg_5394" s="T657">Manna u͡on orduga bi͡es duː, hüːrbe orduga bi͡es duː taba kaːlar. </ta>
            <ta e="T671" id="Seg_5395" s="T668">Bu kaːlbɨtɨn öjdöːböttör. </ta>
            <ta e="T676" id="Seg_5396" s="T671">Huːrka tühenner nöŋü͡ö kihi ketiːr. </ta>
            <ta e="T690" id="Seg_5397" s="T676">Maːdin bu köhölleriger keteːbit kihi, araj ugučaːga ol tabalarga onno kajaga kaːlbɨt biːr ugučaga. </ta>
            <ta e="T693" id="Seg_5398" s="T690">Harsɨŋŋɨtɨn köhöllör emi͡e. </ta>
            <ta e="T699" id="Seg_5399" s="T693">Köhön tühen baraːn nöŋü͡ö kihiler ketiːller. </ta>
            <ta e="T710" id="Seg_5400" s="T699">Maː ɨnaraː huːrka keteːbit artɨnnaːk kihi, ketiː taksan uːčagɨn oččogo hu͡oktuːr. </ta>
            <ta e="T713" id="Seg_5401" s="T710">Harsi͡erde kelen diːr: </ta>
            <ta e="T717" id="Seg_5402" s="T713">"Kaja, mini͡ene uːčagɨm hu͡ok. </ta>
            <ta e="T731" id="Seg_5403" s="T717">Onno huːrka da körbötögüm, maːdin köspüt huːrpar, ɨnaraː huːrka ketiːrber miːne hɨldʼɨbɨt tabam ete." </ta>
            <ta e="T748" id="Seg_5404" s="T731">"Kirdik da, onno huːrka kaːlbɨt bu͡olla", diː diːller ol tabanɨ bu huːrtarɨgar kördüː keleller, bügün köspüt huːrdarɨgar. </ta>
            <ta e="T757" id="Seg_5405" s="T748">Ol tabalarɨ, ɨnaraː köskö kaːlbɨt tabalarɨ betereː huːrka kördüːller. </ta>
            <ta e="T767" id="Seg_5406" s="T757">Ol ɨnaraː huːrka keteːbit pastuːk ugučaga onno kaːlbɨt, ol kajaga. </ta>
            <ta e="T771" id="Seg_5407" s="T767">Ol hogotok tabanɨ öjdüːller. </ta>
            <ta e="T776" id="Seg_5408" s="T771">Onno kaːlbɨta u͡ontan taksa taba. </ta>
            <ta e="T785" id="Seg_5409" s="T776">Ol ihin bʼez vʼestʼi taba huraga hu͡ok hütere – iti. </ta>
            <ta e="T789" id="Seg_5410" s="T785">Onton dʼɨl eŋin-eŋin bu͡olaːččɨ. </ta>
            <ta e="T791" id="Seg_5411" s="T789">Itiːli͡ek bu͡olaːččɨ. </ta>
            <ta e="T799" id="Seg_5412" s="T791">Bu itiːge emi͡e anɨgɨ pastuːktar tabanɨ hataːn karajbattar. </ta>
            <ta e="T801" id="Seg_5413" s="T799">Karajbattara tuːguj? </ta>
            <ta e="T805" id="Seg_5414" s="T801">Itiːge halgɨndʼɨt taba bu͡olaːččɨ. </ta>
            <ta e="T810" id="Seg_5415" s="T805">Halgɨndʼɨt taba aːta tɨ͡alɨ batar. </ta>
            <ta e="T820" id="Seg_5416" s="T810">Bu tɨ͡alɨ batar tabanɨ, itiː ahɨ͡ar di͡eri kihi tabattan baːjaːččɨ. </ta>
            <ta e="T827" id="Seg_5417" s="T820">Taba keteːtekterine, kihi küːhe tijeːččite hu͡ok itiːge. </ta>
            <ta e="T830" id="Seg_5418" s="T827">Tɨ͡alɨ ((…)) gɨmmattar. </ta>
            <ta e="T853" id="Seg_5419" s="T830">Harsi͡erde egelelleriger nöŋü͡ö pastuːkka tuttarallarɨgar "bihigi kɨ͡ajɨ͡akpɨt hu͡oga et" di͡en kahan da tɨ͡al annɨ di͡ek darabiː gɨnɨ͡aktarɨn, dʼi͡elere tɨ͡al ürdü bü bu͡olar kördük. </ta>
            <ta e="T865" id="Seg_5420" s="T853">Taba oččogo kɨ͡ajbakka tɨ͡al utara kellegine, dʼi͡etiger kepterdegine, dʼi͡eteːgilere turannar manna kömölöhönnör. </ta>
            <ta e="T868" id="Seg_5421" s="T865">Oččogo kɨ͡ajallar tabanɨ. </ta>
            <ta e="T888" id="Seg_5422" s="T868">Ol kennine giniler haːskɨttan itiː tühe iligine, haŋardɨː itiː tüheriger, taba itiːrgiːriger buru͡oga tabanɨ ü͡öreti͡ekterin nada, ulakan itiː tühe iligine. </ta>
            <ta e="T896" id="Seg_5423" s="T888">Buru͡onu kördö da töhö daː itiːge taba… </ta>
            <ta e="T903" id="Seg_5424" s="T896">Dʼi͡eteːgilere itiːge pastuːktar ketiː baraːrɨlar haŋarɨ͡ak tustaːktar:</ta>
            <ta e="T909" id="Seg_5425" s="T903">"Kaja, itiː bu͡ollagɨna dʼe kečehen utujuŋ. </ta>
            <ta e="T912" id="Seg_5426" s="T909">Buru͡olataːrɨŋ, otuː ottoːruŋ." </ta>
            <ta e="T917" id="Seg_5427" s="T912">Itiː bu͡ollagɨna, bubaːt, kɨ͡ajɨ͡akpɨt hu͡oga. </ta>
            <ta e="T922" id="Seg_5428" s="T917">Kajtak ere itiː bu͡olan erer. </ta>
            <ta e="T928" id="Seg_5429" s="T922">Ol kördük karajaːččɨ etibit bihigi tabanɨ. </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_5430" s="T0">dʼe</ta>
            <ta e="T2" id="Seg_5431" s="T1">min</ta>
            <ta e="T3" id="Seg_5432" s="T2">taba-hɨt-tar-ga</ta>
            <ta e="T4" id="Seg_5433" s="T3">ihill-e-t-eːri</ta>
            <ta e="T5" id="Seg_5434" s="T4">haŋar-a-bɨn</ta>
            <ta e="T6" id="Seg_5435" s="T5">min</ta>
            <ta e="T7" id="Seg_5436" s="T6">ügüs-tük</ta>
            <ta e="T8" id="Seg_5437" s="T7">üleleː-bit</ta>
            <ta e="T9" id="Seg_5438" s="T8">kihi-bin</ta>
            <ta e="T10" id="Seg_5439" s="T9">taba-ga</ta>
            <ta e="T11" id="Seg_5440" s="T10">urut</ta>
            <ta e="T12" id="Seg_5441" s="T11">bu</ta>
            <ta e="T13" id="Seg_5442" s="T12">taba</ta>
            <ta e="T14" id="Seg_5443" s="T13">törüː-r-ü-n</ta>
            <ta e="T15" id="Seg_5444" s="T14">tuh-u-nan</ta>
            <ta e="T16" id="Seg_5445" s="T15">haŋar-ɨ͡a-m</ta>
            <ta e="T17" id="Seg_5446" s="T16">anɨ</ta>
            <ta e="T18" id="Seg_5447" s="T17">kannɨk</ta>
            <ta e="T19" id="Seg_5448" s="T18">da</ta>
            <ta e="T20" id="Seg_5449" s="T19">otto</ta>
            <ta e="T21" id="Seg_5450" s="T20">törüː-r-ü-tten</ta>
            <ta e="T22" id="Seg_5451" s="T21">bu͡ol-ar</ta>
            <ta e="T23" id="Seg_5452" s="T22">bu͡o</ta>
            <ta e="T24" id="Seg_5453" s="T23">taba</ta>
            <ta e="T25" id="Seg_5454" s="T24">da</ta>
            <ta e="T26" id="Seg_5455" s="T25">ü͡ösküː-r-e</ta>
            <ta e="T27" id="Seg_5456" s="T26">kihi</ta>
            <ta e="T28" id="Seg_5457" s="T27">de</ta>
            <ta e="T29" id="Seg_5458" s="T28">onon</ta>
            <ta e="T30" id="Seg_5459" s="T29">min</ta>
            <ta e="T31" id="Seg_5460" s="T30">haŋar-a-bɨn</ta>
            <ta e="T32" id="Seg_5461" s="T31">dʼe</ta>
            <ta e="T33" id="Seg_5462" s="T32">bugurduk</ta>
            <ta e="T34" id="Seg_5463" s="T33">taba</ta>
            <ta e="T35" id="Seg_5464" s="T34">törüː-r-ü-ger</ta>
            <ta e="T36" id="Seg_5465" s="T35">pastuːx-tar-ga</ta>
            <ta e="T37" id="Seg_5466" s="T36">ihill-e-t-eːri</ta>
            <ta e="T38" id="Seg_5467" s="T37">min</ta>
            <ta e="T39" id="Seg_5468" s="T38">haŋar-ar-ɨ-m</ta>
            <ta e="T40" id="Seg_5469" s="T39">dʼe</ta>
            <ta e="T41" id="Seg_5470" s="T40">bugurduk-kaːn</ta>
            <ta e="T42" id="Seg_5471" s="T41">anɨ</ta>
            <ta e="T43" id="Seg_5472" s="T42">bu</ta>
            <ta e="T44" id="Seg_5473" s="T43">taba-nɨ</ta>
            <ta e="T45" id="Seg_5474" s="T44">karaj-al-lar</ta>
            <ta e="T46" id="Seg_5475" s="T45">min</ta>
            <ta e="T47" id="Seg_5476" s="T46">urut</ta>
            <ta e="T48" id="Seg_5477" s="T47">karaj-a</ta>
            <ta e="T49" id="Seg_5478" s="T48">hɨldʼ-ar</ta>
            <ta e="T50" id="Seg_5479" s="T49">er-dek-pine</ta>
            <ta e="T51" id="Seg_5480" s="T50">taba</ta>
            <ta e="T52" id="Seg_5481" s="T51">töröː-tög-üne</ta>
            <ta e="T53" id="Seg_5482" s="T52">ügüs</ta>
            <ta e="T54" id="Seg_5483" s="T53">prʼičʼine-leːk</ta>
            <ta e="T55" id="Seg_5484" s="T54">bu͡ol-ar</ta>
            <ta e="T56" id="Seg_5485" s="T55">taba</ta>
            <ta e="T57" id="Seg_5486" s="T56">törüː-r-ü-n</ta>
            <ta e="T58" id="Seg_5487" s="T57">pastuːx</ta>
            <ta e="T59" id="Seg_5488" s="T58">er-dek-pine</ta>
            <ta e="T60" id="Seg_5489" s="T59">ket-iː</ta>
            <ta e="T61" id="Seg_5490" s="T60">hɨldʼ-am-mɨn</ta>
            <ta e="T62" id="Seg_5491" s="T61">kerij-eːčči</ta>
            <ta e="T63" id="Seg_5492" s="T62">e-ti-m</ta>
            <ta e="T64" id="Seg_5493" s="T63">taba</ta>
            <ta e="T65" id="Seg_5494" s="T64">töröː-n</ta>
            <ta e="T66" id="Seg_5495" s="T65">ogo-tu-n</ta>
            <ta e="T67" id="Seg_5496" s="T66">tüher-deg-ine</ta>
            <ta e="T68" id="Seg_5497" s="T67">eŋin-eŋin-nik</ta>
            <ta e="T69" id="Seg_5498" s="T68">törüː-r</ta>
            <ta e="T70" id="Seg_5499" s="T69">taba</ta>
            <ta e="T71" id="Seg_5500" s="T70">horok</ta>
            <ta e="T72" id="Seg_5501" s="T71">taba</ta>
            <ta e="T73" id="Seg_5502" s="T72">tugut-a</ta>
            <ta e="T74" id="Seg_5503" s="T73">töröː-n</ta>
            <ta e="T75" id="Seg_5504" s="T74">baraːn</ta>
            <ta e="T76" id="Seg_5505" s="T75">tebi͡elen-e</ta>
            <ta e="T77" id="Seg_5506" s="T76">hɨt-aːččɨ</ta>
            <ta e="T78" id="Seg_5507" s="T77">kot-on</ta>
            <ta e="T79" id="Seg_5508" s="T78">tur-bakka</ta>
            <ta e="T80" id="Seg_5509" s="T79">inʼe-ti-n</ta>
            <ta e="T81" id="Seg_5510" s="T80">kör-dök-kö</ta>
            <ta e="T82" id="Seg_5511" s="T81">emis</ta>
            <ta e="T83" id="Seg_5512" s="T82">bagaj</ta>
            <ta e="T84" id="Seg_5513" s="T83">tugut-u-n</ta>
            <ta e="T85" id="Seg_5514" s="T84">kör-dök-kö</ta>
            <ta e="T86" id="Seg_5515" s="T85">ulakan</ta>
            <ta e="T87" id="Seg_5516" s="T86">bagaj</ta>
            <ta e="T88" id="Seg_5517" s="T87">bu͡ol-ar</ta>
            <ta e="T89" id="Seg_5518" s="T88">bu</ta>
            <ta e="T90" id="Seg_5519" s="T89">tebi͡elen-e</ta>
            <ta e="T91" id="Seg_5520" s="T90">hɨt-ar-ɨ</ta>
            <ta e="T92" id="Seg_5521" s="T91">ergiči-t-i͡ek-ke</ta>
            <ta e="T93" id="Seg_5522" s="T92">naːda</ta>
            <ta e="T94" id="Seg_5523" s="T93">ergiči-t-en</ta>
            <ta e="T95" id="Seg_5524" s="T94">kör-dök-kö</ta>
            <ta e="T96" id="Seg_5525" s="T95">tuːg-u</ta>
            <ta e="T97" id="Seg_5526" s="T96">gɨn-an</ta>
            <ta e="T98" id="Seg_5527" s="T97">togo</ta>
            <ta e="T99" id="Seg_5528" s="T98">tur-bat</ta>
            <ta e="T100" id="Seg_5529" s="T99">di͡e-n</ta>
            <ta e="T101" id="Seg_5530" s="T100">ma-nɨ</ta>
            <ta e="T102" id="Seg_5531" s="T101">ɨbakt-ɨ͡ak-ka</ta>
            <ta e="T103" id="Seg_5532" s="T102">bɨ͡ar-ɨ-n</ta>
            <ta e="T104" id="Seg_5533" s="T103">ɨbakt-ɨ͡ak-ka</ta>
            <ta e="T105" id="Seg_5534" s="T104">taba</ta>
            <ta e="T106" id="Seg_5535" s="T105">töröː-tög-üne</ta>
            <ta e="T107" id="Seg_5536" s="T106">tugut</ta>
            <ta e="T108" id="Seg_5537" s="T107">gi͡en-e</ta>
            <ta e="T109" id="Seg_5538" s="T108">kiːn-neːk</ta>
            <ta e="T110" id="Seg_5539" s="T109">bu͡ol-aːččɨ</ta>
            <ta e="T111" id="Seg_5540" s="T110">bu</ta>
            <ta e="T112" id="Seg_5541" s="T111">kiːn-i-n</ta>
            <ta e="T113" id="Seg_5542" s="T112">horok</ta>
            <ta e="T114" id="Seg_5543" s="T113">taba</ta>
            <ta e="T115" id="Seg_5544" s="T114">hubuj-aːččɨ</ta>
            <ta e="T116" id="Seg_5545" s="T115">bu</ta>
            <ta e="T117" id="Seg_5546" s="T116">kiːn-e</ta>
            <ta e="T118" id="Seg_5547" s="T117">hubu-lun-nag-ɨna</ta>
            <ta e="T119" id="Seg_5548" s="T118">bu-nan</ta>
            <ta e="T120" id="Seg_5549" s="T119">ih-i-tten</ta>
            <ta e="T121" id="Seg_5550" s="T120">huptu</ta>
            <ta e="T122" id="Seg_5551" s="T121">üːt</ta>
            <ta e="T123" id="Seg_5552" s="T122">bu͡ol-ar</ta>
            <ta e="T124" id="Seg_5553" s="T123">o-non</ta>
            <ta e="T125" id="Seg_5554" s="T124">tɨːn-a</ta>
            <ta e="T126" id="Seg_5555" s="T125">taks-ar</ta>
            <ta e="T127" id="Seg_5556" s="T126">mu͡ot-u-nan</ta>
            <ta e="T128" id="Seg_5557" s="T127">kam</ta>
            <ta e="T129" id="Seg_5558" s="T128">baːj-dak-ka</ta>
            <ta e="T130" id="Seg_5559" s="T129">bu</ta>
            <ta e="T131" id="Seg_5560" s="T130">taba</ta>
            <ta e="T132" id="Seg_5561" s="T131">bu</ta>
            <ta e="T133" id="Seg_5562" s="T132">tugut</ta>
            <ta e="T134" id="Seg_5563" s="T133">tur-an</ta>
            <ta e="T135" id="Seg_5564" s="T134">kel-eːčči</ta>
            <ta e="T136" id="Seg_5565" s="T135">ol</ta>
            <ta e="T137" id="Seg_5566" s="T136">aːt-a</ta>
            <ta e="T138" id="Seg_5567" s="T137">kiːn-i-n</ta>
            <ta e="T139" id="Seg_5568" s="T138">hubuj-ar</ta>
            <ta e="T140" id="Seg_5569" s="T139">iti</ta>
            <ta e="T141" id="Seg_5570" s="T140">bu͡ol-ar</ta>
            <ta e="T142" id="Seg_5571" s="T141">taba</ta>
            <ta e="T143" id="Seg_5572" s="T142">gi͡en-e</ta>
            <ta e="T144" id="Seg_5573" s="T143">tugut-u-n</ta>
            <ta e="T145" id="Seg_5574" s="T144">gi͡en-e</ta>
            <ta e="T146" id="Seg_5575" s="T145">prʼičʼine-te</ta>
            <ta e="T147" id="Seg_5576" s="T146">töröː-tög-üne</ta>
            <ta e="T148" id="Seg_5577" s="T147">onton</ta>
            <ta e="T149" id="Seg_5578" s="T148">biːr</ta>
            <ta e="T150" id="Seg_5579" s="T149">prʼičʼine-leːk</ta>
            <ta e="T151" id="Seg_5580" s="T150">slabaj</ta>
            <ta e="T152" id="Seg_5581" s="T151">taba</ta>
            <ta e="T153" id="Seg_5582" s="T152">törüː-r</ta>
            <ta e="T154" id="Seg_5583" s="T153">torugan</ta>
            <ta e="T155" id="Seg_5584" s="T154">inʼe-te</ta>
            <ta e="T156" id="Seg_5585" s="T155">bu</ta>
            <ta e="T157" id="Seg_5586" s="T156">tugut-a</ta>
            <ta e="T158" id="Seg_5587" s="T157">töröː-tög-üne</ta>
            <ta e="T159" id="Seg_5588" s="T158">dʼapču</ta>
            <ta e="T160" id="Seg_5589" s="T159">di͡e-n</ta>
            <ta e="T161" id="Seg_5590" s="T160">aːt-tan-aːččɨ</ta>
            <ta e="T162" id="Seg_5591" s="T161">emi͡e</ta>
            <ta e="T163" id="Seg_5592" s="T162">tugut-a</ta>
            <ta e="T164" id="Seg_5593" s="T163">tur-aːččɨ-ta</ta>
            <ta e="T165" id="Seg_5594" s="T164">hu͡ok</ta>
            <ta e="T166" id="Seg_5595" s="T165">bu</ta>
            <ta e="T167" id="Seg_5596" s="T166">tugut-u</ta>
            <ta e="T168" id="Seg_5597" s="T167">ergiči-t-en</ta>
            <ta e="T169" id="Seg_5598" s="T168">kör-dök-kö</ta>
            <ta e="T170" id="Seg_5599" s="T169">amattan</ta>
            <ta e="T171" id="Seg_5600" s="T170">slabaj</ta>
            <ta e="T172" id="Seg_5601" s="T171">bagaj</ta>
            <ta e="T173" id="Seg_5602" s="T172">bu͡ol-ar</ta>
            <ta e="T174" id="Seg_5603" s="T173">bu</ta>
            <ta e="T175" id="Seg_5604" s="T174">tugut</ta>
            <ta e="T176" id="Seg_5605" s="T175">tebi͡elen-e</ta>
            <ta e="T177" id="Seg_5606" s="T176">agaj</ta>
            <ta e="T178" id="Seg_5607" s="T177">hɨt-aːččɨ</ta>
            <ta e="T179" id="Seg_5608" s="T178">bu</ta>
            <ta e="T180" id="Seg_5609" s="T179">tugut-u</ta>
            <ta e="T181" id="Seg_5610" s="T180">ittenne</ta>
            <ta e="T182" id="Seg_5611" s="T181">tut-an</ta>
            <ta e="T183" id="Seg_5612" s="T182">baraːn</ta>
            <ta e="T184" id="Seg_5613" s="T183">tü͡öh-ü-n</ta>
            <ta e="T185" id="Seg_5614" s="T184">imerij-dek-ke</ta>
            <ta e="T186" id="Seg_5615" s="T185">oččogo</ta>
            <ta e="T187" id="Seg_5616" s="T186">bu</ta>
            <ta e="T188" id="Seg_5617" s="T187">tü͡öh-e</ta>
            <ta e="T189" id="Seg_5618" s="T188">dʼapču</ta>
            <ta e="T190" id="Seg_5619" s="T189">gi͡en-e</ta>
            <ta e="T191" id="Seg_5620" s="T190">kahan</ta>
            <ta e="T192" id="Seg_5621" s="T191">da</ta>
            <ta e="T193" id="Seg_5622" s="T192">tugut</ta>
            <ta e="T194" id="Seg_5623" s="T193">töröː-tög-üne</ta>
            <ta e="T195" id="Seg_5624" s="T194">oŋu͡og-a</ta>
            <ta e="T196" id="Seg_5625" s="T195">oŋurgas</ta>
            <ta e="T197" id="Seg_5626" s="T196">bu͡ol-ar</ta>
            <ta e="T198" id="Seg_5627" s="T197">bu-nɨ</ta>
            <ta e="T199" id="Seg_5628" s="T198">tü͡öh-ü-n</ta>
            <ta e="T200" id="Seg_5629" s="T199">imerij-dek-ke</ta>
            <ta e="T201" id="Seg_5630" s="T200">tur-a-tur-a</ta>
            <ta e="T202" id="Seg_5631" s="T201">tüh-e</ta>
            <ta e="T203" id="Seg_5632" s="T202">hɨldʼ-aːččɨ</ta>
            <ta e="T204" id="Seg_5633" s="T203">tɨːn-nag-ɨna</ta>
            <ta e="T205" id="Seg_5634" s="T204">ma-nɨ</ta>
            <ta e="T206" id="Seg_5635" s="T205">inʼe-ti-n</ta>
            <ta e="T207" id="Seg_5636" s="T206">tut-an</ta>
            <ta e="T208" id="Seg_5637" s="T207">baraːn</ta>
            <ta e="T209" id="Seg_5638" s="T208">inʼe-ti-n</ta>
            <ta e="T210" id="Seg_5639" s="T209">em-ner-i͡ek-ke</ta>
            <ta e="T211" id="Seg_5640" s="T210">naːda</ta>
            <ta e="T212" id="Seg_5641" s="T211">bu</ta>
            <ta e="T213" id="Seg_5642" s="T212">tugut</ta>
            <ta e="T214" id="Seg_5643" s="T213">tü͡öh-ü-n</ta>
            <ta e="T215" id="Seg_5644" s="T214">kim-i</ta>
            <ta e="T216" id="Seg_5645" s="T215">ilbij-bit-i-n</ta>
            <ta e="T217" id="Seg_5646" s="T216">kenne</ta>
            <ta e="T218" id="Seg_5647" s="T217">kot-on</ta>
            <ta e="T219" id="Seg_5648" s="T218">em-eːčči-te</ta>
            <ta e="T220" id="Seg_5649" s="T219">hu͡ok</ta>
            <ta e="T221" id="Seg_5650" s="T220">ma-nɨ</ta>
            <ta e="T222" id="Seg_5651" s="T221">inʼe-te</ta>
            <ta e="T223" id="Seg_5652" s="T222">emij-e</ta>
            <ta e="T224" id="Seg_5653" s="T223">ɨ͡as-taːk</ta>
            <ta e="T225" id="Seg_5654" s="T224">bu͡ol-l-a</ta>
            <ta e="T226" id="Seg_5655" s="T225">lampagar</ta>
            <ta e="T227" id="Seg_5656" s="T226">bu</ta>
            <ta e="T228" id="Seg_5657" s="T227">ɨ͡ah-ɨ</ta>
            <ta e="T229" id="Seg_5658" s="T228">ɨg-aːččɨ</ta>
            <ta e="T230" id="Seg_5659" s="T229">kihi</ta>
            <ta e="T231" id="Seg_5660" s="T230">ɨg-an</ta>
            <ta e="T232" id="Seg_5661" s="T231">baraːn</ta>
            <ta e="T233" id="Seg_5662" s="T232">ɨbagas</ta>
            <ta e="T234" id="Seg_5663" s="T233">emij-e</ta>
            <ta e="T235" id="Seg_5664" s="T234">kel-leg-ine</ta>
            <ta e="T236" id="Seg_5665" s="T235">oččogo</ta>
            <ta e="T237" id="Seg_5666" s="T236">em-ner-eːčči</ta>
            <ta e="T238" id="Seg_5667" s="T237">oččogo</ta>
            <ta e="T239" id="Seg_5668" s="T238">em-ner-deg-ine</ta>
            <ta e="T240" id="Seg_5669" s="T239">bu</ta>
            <ta e="T241" id="Seg_5670" s="T240">tugut</ta>
            <ta e="T242" id="Seg_5671" s="T241">kihi</ta>
            <ta e="T243" id="Seg_5672" s="T242">bu͡ol-ar</ta>
            <ta e="T244" id="Seg_5673" s="T243">pastuːk-tar</ta>
            <ta e="T245" id="Seg_5674" s="T244">köh-öl-lör</ta>
            <ta e="T246" id="Seg_5675" s="T245">ügüs</ta>
            <ta e="T247" id="Seg_5676" s="T246">taba</ta>
            <ta e="T248" id="Seg_5677" s="T247">töröː-tög-üne</ta>
            <ta e="T249" id="Seg_5678" s="T248">kahan</ta>
            <ta e="T250" id="Seg_5679" s="T249">da</ta>
            <ta e="T251" id="Seg_5680" s="T250">tör-üː=lik</ta>
            <ta e="T252" id="Seg_5681" s="T251">taba-nɨ</ta>
            <ta e="T253" id="Seg_5682" s="T252">inni</ta>
            <ta e="T254" id="Seg_5683" s="T253">di͡ek</ta>
            <ta e="T255" id="Seg_5684" s="T254">ill-eːčči-ler</ta>
            <ta e="T256" id="Seg_5685" s="T255">töröː-büt</ta>
            <ta e="T257" id="Seg_5686" s="T256">taba-nɨ</ta>
            <ta e="T258" id="Seg_5687" s="T257">tugut-a</ta>
            <ta e="T259" id="Seg_5688" s="T258">ulaːt-a</ta>
            <ta e="T260" id="Seg_5689" s="T259">tüh-ü͡ö-r</ta>
            <ta e="T261" id="Seg_5690" s="T260">di͡eri</ta>
            <ta e="T262" id="Seg_5691" s="T261">kaːm-pat-ɨ-n</ta>
            <ta e="T263" id="Seg_5692" s="T262">ihin</ta>
            <ta e="T264" id="Seg_5693" s="T263">kenni</ta>
            <ta e="T265" id="Seg_5694" s="T264">di͡ek</ta>
            <ta e="T266" id="Seg_5695" s="T265">keːh-el-ler</ta>
            <ta e="T267" id="Seg_5696" s="T266">bu</ta>
            <ta e="T268" id="Seg_5697" s="T267">keːh-iː-leri-ger</ta>
            <ta e="T269" id="Seg_5698" s="T268">bu</ta>
            <ta e="T270" id="Seg_5699" s="T269">te</ta>
            <ta e="T271" id="Seg_5700" s="T270">u͡on-tan</ta>
            <ta e="T272" id="Seg_5701" s="T271">taksa</ta>
            <ta e="T273" id="Seg_5702" s="T272">hüːrbe-čče</ta>
            <ta e="T274" id="Seg_5703" s="T273">taba</ta>
            <ta e="T275" id="Seg_5704" s="T274">töröː-büt-ü-n</ta>
            <ta e="T276" id="Seg_5705" s="T275">kenne</ta>
            <ta e="T277" id="Seg_5706" s="T276">oččogo</ta>
            <ta e="T278" id="Seg_5707" s="T277">bu</ta>
            <ta e="T279" id="Seg_5708" s="T278">tör-üː=lik</ta>
            <ta e="T280" id="Seg_5709" s="T279">taba-nɨ</ta>
            <ta e="T281" id="Seg_5710" s="T280">inni</ta>
            <ta e="T282" id="Seg_5711" s="T281">di͡ek</ta>
            <ta e="T283" id="Seg_5712" s="T282">ill-i͡ek-ke</ta>
            <ta e="T284" id="Seg_5713" s="T283">naːda</ta>
            <ta e="T285" id="Seg_5714" s="T284">bu</ta>
            <ta e="T286" id="Seg_5715" s="T285">taba-nɨ</ta>
            <ta e="T287" id="Seg_5716" s="T286">ill-er-ge</ta>
            <ta e="T288" id="Seg_5717" s="T287">horok</ta>
            <ta e="T289" id="Seg_5718" s="T288">pastuːk</ta>
            <ta e="T290" id="Seg_5719" s="T289">bu</ta>
            <ta e="T291" id="Seg_5720" s="T290">töröː-büt</ta>
            <ta e="T292" id="Seg_5721" s="T291">tugut-taːk-ka</ta>
            <ta e="T293" id="Seg_5722" s="T292">kaːl-ar</ta>
            <ta e="T294" id="Seg_5723" s="T293">horok</ta>
            <ta e="T295" id="Seg_5724" s="T294">pastuːk</ta>
            <ta e="T296" id="Seg_5725" s="T295">bu</ta>
            <ta e="T297" id="Seg_5726" s="T296">töröː</ta>
            <ta e="T298" id="Seg_5727" s="T297">tör-üː=lik</ta>
            <ta e="T299" id="Seg_5728" s="T298">taba-nɨ</ta>
            <ta e="T300" id="Seg_5729" s="T299">ill-er</ta>
            <ta e="T301" id="Seg_5730" s="T300">bu</ta>
            <ta e="T302" id="Seg_5731" s="T301">ill-er-i-ger</ta>
            <ta e="T303" id="Seg_5732" s="T302">ah-a-t-an</ta>
            <ta e="T304" id="Seg_5733" s="T303">albɨn-naː-n</ta>
            <ta e="T305" id="Seg_5734" s="T304">kalka</ta>
            <ta e="T306" id="Seg_5735" s="T305">hir-ge</ta>
            <ta e="T307" id="Seg_5736" s="T306">kiːl-ler-en</ta>
            <ta e="T308" id="Seg_5737" s="T307">ü͡ör-e</ta>
            <ta e="T309" id="Seg_5738" s="T308">bar-ar-ɨ-n</ta>
            <ta e="T310" id="Seg_5739" s="T309">köllör-bökkö</ta>
            <ta e="T311" id="Seg_5740" s="T310">bu</ta>
            <ta e="T312" id="Seg_5741" s="T311">tugut-taːk</ta>
            <ta e="T314" id="Seg_5742" s="T312">tɨhɨ-nɨ</ta>
            <ta e="T315" id="Seg_5743" s="T314">kahan</ta>
            <ta e="T316" id="Seg_5744" s="T315">da</ta>
            <ta e="T317" id="Seg_5745" s="T316">taba</ta>
            <ta e="T318" id="Seg_5746" s="T317">hannʼɨ-h-an</ta>
            <ta e="T319" id="Seg_5747" s="T318">tugut-u-n</ta>
            <ta e="T320" id="Seg_5748" s="T319">keːh-eːčči</ta>
            <ta e="T321" id="Seg_5749" s="T320">batɨh-an</ta>
            <ta e="T322" id="Seg_5750" s="T321">ü͡ör-ü-ger</ta>
            <ta e="T323" id="Seg_5751" s="T322">tal-a-h-an</ta>
            <ta e="T324" id="Seg_5752" s="T323">o-nu</ta>
            <ta e="T325" id="Seg_5753" s="T324">ah-a-t-a</ta>
            <ta e="T326" id="Seg_5754" s="T325">hɨldʼ-an</ta>
            <ta e="T327" id="Seg_5755" s="T326">top-put-u-n</ta>
            <ta e="T328" id="Seg_5756" s="T327">kemnen-e</ta>
            <ta e="T329" id="Seg_5757" s="T328">hɨt-ar-ɨ-gar</ta>
            <ta e="T330" id="Seg_5758" s="T329">tör-üː=lik</ta>
            <ta e="T331" id="Seg_5759" s="T330">taba-tɨ-n</ta>
            <ta e="T332" id="Seg_5760" s="T331">inni</ta>
            <ta e="T333" id="Seg_5761" s="T332">di͡ek</ta>
            <ta e="T334" id="Seg_5762" s="T333">köhörd-ön</ta>
            <ta e="T335" id="Seg_5763" s="T334">ill-er</ta>
            <ta e="T336" id="Seg_5764" s="T335">ol</ta>
            <ta e="T337" id="Seg_5765" s="T336">köhör-tök-törüne</ta>
            <ta e="T338" id="Seg_5766" s="T337">horok</ta>
            <ta e="T339" id="Seg_5767" s="T338">pastuːk-tar</ta>
            <ta e="T340" id="Seg_5768" s="T339">hürdeːk</ta>
            <ta e="T341" id="Seg_5769" s="T340">ɨraːk</ta>
            <ta e="T342" id="Seg_5770" s="T341">bar-aːččɨ-lar</ta>
            <ta e="T343" id="Seg_5771" s="T342">bu</ta>
            <ta e="T344" id="Seg_5772" s="T343">ɨraːk</ta>
            <ta e="T345" id="Seg_5773" s="T344">bar-dak-tarɨna</ta>
            <ta e="T346" id="Seg_5774" s="T345">bu</ta>
            <ta e="T347" id="Seg_5775" s="T346">tugut</ta>
            <ta e="T348" id="Seg_5776" s="T347">harsɨŋŋɨ-tɨ-n</ta>
            <ta e="T349" id="Seg_5777" s="T348">ol</ta>
            <ta e="T350" id="Seg_5778" s="T349">tör-üː=lik</ta>
            <ta e="T351" id="Seg_5779" s="T350">taba-laːk-ka</ta>
            <ta e="T352" id="Seg_5780" s="T351">kenni-kiː-ti-n</ta>
            <ta e="T353" id="Seg_5781" s="T352">pastuːk-tarɨ-n</ta>
            <ta e="T354" id="Seg_5782" s="T353">ol</ta>
            <ta e="T355" id="Seg_5783" s="T354">ma-nɨ</ta>
            <ta e="T356" id="Seg_5784" s="T355">töröː-büt</ta>
            <ta e="T357" id="Seg_5785" s="T356">tɨhɨ-laːk-tarɨ-n</ta>
            <ta e="T358" id="Seg_5786" s="T357">pastuːk-ta</ta>
            <ta e="T359" id="Seg_5787" s="T358">inni</ta>
            <ta e="T360" id="Seg_5788" s="T359">di͡ek</ta>
            <ta e="T361" id="Seg_5789" s="T360">pastuːk-tar-ga</ta>
            <ta e="T362" id="Seg_5790" s="T361">ill-eːri-ler</ta>
            <ta e="T363" id="Seg_5791" s="T362">köh-öl-lörü-ger</ta>
            <ta e="T364" id="Seg_5792" s="T363">taba</ta>
            <ta e="T365" id="Seg_5793" s="T364">kot-on</ta>
            <ta e="T366" id="Seg_5794" s="T365">tij-eːčči-te</ta>
            <ta e="T367" id="Seg_5795" s="T366">hu͡ok</ta>
            <ta e="T368" id="Seg_5796" s="T367">bo</ta>
            <ta e="T369" id="Seg_5797" s="T368">ol</ta>
            <ta e="T370" id="Seg_5798" s="T369">ihin</ta>
            <ta e="T371" id="Seg_5799" s="T370">čugas</ta>
            <ta e="T372" id="Seg_5800" s="T371">tüh-ü͡ök-ke</ta>
            <ta e="T373" id="Seg_5801" s="T372">kör-ön</ta>
            <ta e="T374" id="Seg_5802" s="T373">baraːn</ta>
            <ta e="T375" id="Seg_5803" s="T374">onton</ta>
            <ta e="T376" id="Seg_5804" s="T375">bu͡ollagɨna</ta>
            <ta e="T377" id="Seg_5805" s="T376">bu</ta>
            <ta e="T378" id="Seg_5806" s="T377">pastuːk-tar</ta>
            <ta e="T379" id="Seg_5807" s="T378">kahan</ta>
            <ta e="T380" id="Seg_5808" s="T379">da</ta>
            <ta e="T381" id="Seg_5809" s="T380">kalka-laːk</ta>
            <ta e="T382" id="Seg_5810" s="T381">hir-i-nen</ta>
            <ta e="T383" id="Seg_5811" s="T382">hɨldʼ-al-lar</ta>
            <ta e="T384" id="Seg_5812" s="T383">maršrut</ta>
            <ta e="T385" id="Seg_5813" s="T384">kör-öl-lör</ta>
            <ta e="T386" id="Seg_5814" s="T385">taba</ta>
            <ta e="T387" id="Seg_5815" s="T386">tör-ö-t-ör-ü-ger</ta>
            <ta e="T388" id="Seg_5816" s="T387">kalka-laːk</ta>
            <ta e="T389" id="Seg_5817" s="T388">hir-i-nen</ta>
            <ta e="T390" id="Seg_5818" s="T389">ohuːr-daːk</ta>
            <ta e="T391" id="Seg_5819" s="T390">hir-i-nen</ta>
            <ta e="T392" id="Seg_5820" s="T391">kaja-lar-daːk</ta>
            <ta e="T393" id="Seg_5821" s="T392">hir-der-i-nen</ta>
            <ta e="T394" id="Seg_5822" s="T393">ürek-ter-deːk</ta>
            <ta e="T395" id="Seg_5823" s="T394">hir-der-i-nen</ta>
            <ta e="T396" id="Seg_5824" s="T395">bu</ta>
            <ta e="T397" id="Seg_5825" s="T396">ulakan</ta>
            <ta e="T398" id="Seg_5826" s="T397">hobu͡oj</ta>
            <ta e="T399" id="Seg_5827" s="T398">bɨst-aːččɨ</ta>
            <ta e="T400" id="Seg_5828" s="T399">bu</ta>
            <ta e="T401" id="Seg_5829" s="T400">bɨhɨn-nag-ɨna</ta>
            <ta e="T402" id="Seg_5830" s="T401">haːs</ta>
            <ta e="T403" id="Seg_5831" s="T402">taba</ta>
            <ta e="T404" id="Seg_5832" s="T403">törüː-r-ü-n</ta>
            <ta e="T405" id="Seg_5833" s="T404">hagɨna</ta>
            <ta e="T406" id="Seg_5834" s="T405">kün</ta>
            <ta e="T407" id="Seg_5835" s="T406">hɨrdaː-tag-ɨna</ta>
            <ta e="T408" id="Seg_5836" s="T407">kɨhɨn</ta>
            <ta e="T409" id="Seg_5837" s="T408">bɨst-ɨ-bɨt</ta>
            <ta e="T410" id="Seg_5838" s="T409">hobu͡oj</ta>
            <ta e="T411" id="Seg_5839" s="T410">at-aːččɨ</ta>
            <ta e="T412" id="Seg_5840" s="T411">ol</ta>
            <ta e="T413" id="Seg_5841" s="T412">aːt-a</ta>
            <ta e="T414" id="Seg_5842" s="T413">köŋüs</ta>
            <ta e="T415" id="Seg_5843" s="T414">bu͡ol-ar</ta>
            <ta e="T416" id="Seg_5844" s="T415">taba</ta>
            <ta e="T417" id="Seg_5845" s="T416">hurag-a</ta>
            <ta e="T418" id="Seg_5846" s="T417">hu͡ok</ta>
            <ta e="T419" id="Seg_5847" s="T418">hüter-e</ta>
            <ta e="T420" id="Seg_5848" s="T419">ol</ta>
            <ta e="T421" id="Seg_5849" s="T420">köŋüs</ta>
            <ta e="T422" id="Seg_5850" s="T421">ih-i-ger</ta>
            <ta e="T423" id="Seg_5851" s="T422">tüs-püt</ta>
            <ta e="T424" id="Seg_5852" s="T423">taba-nɨ</ta>
            <ta e="T425" id="Seg_5853" s="T424">bu</ta>
            <ta e="T426" id="Seg_5854" s="T425">pastuːk</ta>
            <ta e="T427" id="Seg_5855" s="T426">ket-iː</ta>
            <ta e="T428" id="Seg_5856" s="T427">hɨldʼ-an</ta>
            <ta e="T429" id="Seg_5857" s="T428">bil-bet</ta>
            <ta e="T430" id="Seg_5858" s="T429">kas</ta>
            <ta e="T431" id="Seg_5859" s="T430">da</ta>
            <ta e="T432" id="Seg_5860" s="T431">taba</ta>
            <ta e="T433" id="Seg_5861" s="T432">tüs-püt-ü-n</ta>
            <ta e="T434" id="Seg_5862" s="T433">h-onon</ta>
            <ta e="T435" id="Seg_5863" s="T434">hu͡oktaː-bat</ta>
            <ta e="T436" id="Seg_5864" s="T435">taba</ta>
            <ta e="T437" id="Seg_5865" s="T436">hurag-a</ta>
            <ta e="T438" id="Seg_5866" s="T437">hu͡ok</ta>
            <ta e="T439" id="Seg_5867" s="T438">hüt-er-e</ta>
            <ta e="T440" id="Seg_5868" s="T439">ol</ta>
            <ta e="T441" id="Seg_5869" s="T440">onon</ta>
            <ta e="T442" id="Seg_5870" s="T441">brʼigadʼiːr-der</ta>
            <ta e="T443" id="Seg_5871" s="T442">bu͡ol</ta>
            <ta e="T444" id="Seg_5872" s="T443">pastuːk-tar</ta>
            <ta e="T445" id="Seg_5873" s="T444">bu͡ol</ta>
            <ta e="T446" id="Seg_5874" s="T445">haːs</ta>
            <ta e="T447" id="Seg_5875" s="T446">taba</ta>
            <ta e="T448" id="Seg_5876" s="T447">törüː-r-ü-n</ta>
            <ta e="T449" id="Seg_5877" s="T448">hagɨna</ta>
            <ta e="T451" id="Seg_5878" s="T450">kaːr</ta>
            <ta e="T452" id="Seg_5879" s="T451">ir-er-i-n</ta>
            <ta e="T453" id="Seg_5880" s="T452">hagɨna</ta>
            <ta e="T454" id="Seg_5881" s="T453">üčügej-dik</ta>
            <ta e="T455" id="Seg_5882" s="T454">hir-deri-n</ta>
            <ta e="T456" id="Seg_5883" s="T455">kör-ü͡ök-teri-n</ta>
            <ta e="T457" id="Seg_5884" s="T456">naːda</ta>
            <ta e="T458" id="Seg_5885" s="T457">hobu͡oj</ta>
            <ta e="T459" id="Seg_5886" s="T458">kajd-ɨ-bɨt-a</ta>
            <ta e="T460" id="Seg_5887" s="T459">purga</ta>
            <ta e="T461" id="Seg_5888" s="T460">bu͡ol-lag-ɨna</ta>
            <ta e="T462" id="Seg_5889" s="T461">tib-i-ll-en</ta>
            <ta e="T463" id="Seg_5890" s="T462">kaːl-aːččɨ</ta>
            <ta e="T464" id="Seg_5891" s="T463">ol</ta>
            <ta e="T465" id="Seg_5892" s="T464">gɨn-an</ta>
            <ta e="T466" id="Seg_5893" s="T465">baraːn</ta>
            <ta e="T467" id="Seg_5894" s="T466">ol</ta>
            <ta e="T468" id="Seg_5895" s="T467">tib-i-ll-i-bit-i-n</ta>
            <ta e="T469" id="Seg_5896" s="T468">kenne</ta>
            <ta e="T470" id="Seg_5897" s="T469">o-nu</ta>
            <ta e="T471" id="Seg_5898" s="T470">taba</ta>
            <ta e="T472" id="Seg_5899" s="T471">kör-böt</ta>
            <ta e="T473" id="Seg_5900" s="T472">h-onon</ta>
            <ta e="T474" id="Seg_5901" s="T473">tüh-er</ta>
            <ta e="T475" id="Seg_5902" s="T474">ol</ta>
            <ta e="T476" id="Seg_5903" s="T475">tüs-teg-ine</ta>
            <ta e="T477" id="Seg_5904" s="T476">bi͡es-tü͡ört</ta>
            <ta e="T478" id="Seg_5905" s="T477">mʼetʼir-deːk</ta>
            <ta e="T479" id="Seg_5906" s="T478">diriŋ-neːk</ta>
            <ta e="T480" id="Seg_5907" s="T479">bu͡ol-aːččɨ</ta>
            <ta e="T481" id="Seg_5908" s="T480">bu</ta>
            <ta e="T482" id="Seg_5909" s="T481">köŋüs</ta>
            <ta e="T483" id="Seg_5910" s="T482">kaːr</ta>
            <ta e="T484" id="Seg_5911" s="T483">bɨst-ɨ-bɨt</ta>
            <ta e="T485" id="Seg_5912" s="T484">ol</ta>
            <ta e="T486" id="Seg_5913" s="T485">ihin</ta>
            <ta e="T487" id="Seg_5914" s="T486">onnug-u</ta>
            <ta e="T488" id="Seg_5915" s="T487">bul-bakka</ta>
            <ta e="T489" id="Seg_5916" s="T488">taba</ta>
            <ta e="T490" id="Seg_5917" s="T489">hurag-a</ta>
            <ta e="T491" id="Seg_5918" s="T490">hu͡ok</ta>
            <ta e="T492" id="Seg_5919" s="T491">hüt-er</ta>
            <ta e="T493" id="Seg_5920" s="T492">iti</ta>
            <ta e="T494" id="Seg_5921" s="T493">onton</ta>
            <ta e="T495" id="Seg_5922" s="T494">ürek-teːk</ta>
            <ta e="T496" id="Seg_5923" s="T495">hir-der-ge</ta>
            <ta e="T497" id="Seg_5924" s="T496">bu͡ollagɨna</ta>
            <ta e="T498" id="Seg_5925" s="T497">karadʼɨk-taː-tag-ɨna</ta>
            <ta e="T499" id="Seg_5926" s="T498">kahan</ta>
            <ta e="T500" id="Seg_5927" s="T499">da</ta>
            <ta e="T501" id="Seg_5928" s="T500">ürek-teːk</ta>
            <ta e="T502" id="Seg_5929" s="T501">hir-i-nen</ta>
            <ta e="T503" id="Seg_5930" s="T502">bu͡ol-ar</ta>
            <ta e="T504" id="Seg_5931" s="T503">maršrut</ta>
            <ta e="T505" id="Seg_5932" s="T504">bu</ta>
            <ta e="T506" id="Seg_5933" s="T505">kaːr-ɨ</ta>
            <ta e="T507" id="Seg_5934" s="T506">gɨtta</ta>
            <ta e="T508" id="Seg_5935" s="T507">kaja</ta>
            <ta e="T509" id="Seg_5936" s="T508">hir</ta>
            <ta e="T510" id="Seg_5937" s="T509">ɨkkardɨ-ta</ta>
            <ta e="T511" id="Seg_5938" s="T510">at-aːččɨ</ta>
            <ta e="T512" id="Seg_5939" s="T511">emi͡e</ta>
            <ta e="T513" id="Seg_5940" s="T512">onon</ta>
            <ta e="T514" id="Seg_5941" s="T513">emi͡e</ta>
            <ta e="T515" id="Seg_5942" s="T514">tüh-eːčči</ta>
            <ta e="T516" id="Seg_5943" s="T515">tugut</ta>
            <ta e="T517" id="Seg_5944" s="T516">bu͡ol</ta>
            <ta e="T518" id="Seg_5945" s="T517">ulakan</ta>
            <ta e="T519" id="Seg_5946" s="T518">taba</ta>
            <ta e="T520" id="Seg_5947" s="T519">bu͡ol</ta>
            <ta e="T521" id="Seg_5948" s="T520">onton</ta>
            <ta e="T522" id="Seg_5949" s="T521">bu</ta>
            <ta e="T523" id="Seg_5950" s="T522">pastuːk-tar</ta>
            <ta e="T524" id="Seg_5951" s="T523">haːs</ta>
            <ta e="T525" id="Seg_5952" s="T524">hajɨn-nara</ta>
            <ta e="T526" id="Seg_5953" s="T525">čugahaː-tag-ɨna</ta>
            <ta e="T527" id="Seg_5954" s="T526">haːs-kɨ-larɨ-gar</ta>
            <ta e="T528" id="Seg_5955" s="T527">uːčak-tarɨ-n</ta>
            <ta e="T529" id="Seg_5956" s="T528">hataː-n</ta>
            <ta e="T530" id="Seg_5957" s="T529">kara-m-mat-tar</ta>
            <ta e="T531" id="Seg_5958" s="T530">künükte-ti-n</ta>
            <ta e="T532" id="Seg_5959" s="T531">tonoː-bot-tor</ta>
            <ta e="T533" id="Seg_5960" s="T532">haːs</ta>
            <ta e="T534" id="Seg_5961" s="T533">künükte-ti-n</ta>
            <ta e="T535" id="Seg_5962" s="T534">tonoː-tok-toruna</ta>
            <ta e="T536" id="Seg_5963" s="T535">ol</ta>
            <ta e="T537" id="Seg_5964" s="T536">künükte-ti-n</ta>
            <ta e="T538" id="Seg_5965" s="T537">tonoː-tok-toruna</ta>
            <ta e="T539" id="Seg_5966" s="T538">bu</ta>
            <ta e="T540" id="Seg_5967" s="T539">künükte</ta>
            <ta e="T541" id="Seg_5968" s="T540">oh-or</ta>
            <ta e="T542" id="Seg_5969" s="T541">beje-te</ta>
            <ta e="T543" id="Seg_5970" s="T542">künükte</ta>
            <ta e="T544" id="Seg_5971" s="T543">haːs</ta>
            <ta e="T545" id="Seg_5972" s="T544">ulaːt-an</ta>
            <ta e="T546" id="Seg_5973" s="T545">tagɨs-tag-ɨna</ta>
            <ta e="T547" id="Seg_5974" s="T546">tiriː-ti-n</ta>
            <ta e="T548" id="Seg_5975" s="T547">ih-i-tten</ta>
            <ta e="T549" id="Seg_5976" s="T548">beje-te</ta>
            <ta e="T550" id="Seg_5977" s="T549">tull-an</ta>
            <ta e="T551" id="Seg_5978" s="T550">tüh-üː-r</ta>
            <ta e="T552" id="Seg_5979" s="T551">di͡eri</ta>
            <ta e="T553" id="Seg_5980" s="T552">hɨrɨt-tag-ɨna</ta>
            <ta e="T554" id="Seg_5981" s="T553">taba-ga</ta>
            <ta e="T555" id="Seg_5982" s="T554">da</ta>
            <ta e="T556" id="Seg_5983" s="T555">kuhagan</ta>
            <ta e="T557" id="Seg_5984" s="T556">horok</ta>
            <ta e="T558" id="Seg_5985" s="T557">künükte-ni</ta>
            <ta e="T559" id="Seg_5986" s="T558">bütej-diː</ta>
            <ta e="T560" id="Seg_5987" s="T559">miːn-e-miːn-e-ler</ta>
            <ta e="T561" id="Seg_5988" s="T560">taba</ta>
            <ta e="T562" id="Seg_5989" s="T561">gi͡en-e</ta>
            <ta e="T563" id="Seg_5990" s="T562">öl-ör</ta>
            <ta e="T564" id="Seg_5991" s="T563">ün-er-e</ta>
            <ta e="T565" id="Seg_5992" s="T564">ol</ta>
            <ta e="T566" id="Seg_5993" s="T565">horok</ta>
            <ta e="T567" id="Seg_5994" s="T566">pastuːk</ta>
            <ta e="T568" id="Seg_5995" s="T567">taba-nɨ</ta>
            <ta e="T569" id="Seg_5996" s="T568">da</ta>
            <ta e="T570" id="Seg_5997" s="T569">kölün-e</ta>
            <ta e="T571" id="Seg_5998" s="T570">hataː-bat</ta>
            <ta e="T572" id="Seg_5999" s="T571">pabudu͡ok-tara</ta>
            <ta e="T573" id="Seg_6000" s="T572">bɨhɨn-n-a</ta>
            <ta e="T574" id="Seg_6001" s="T573">taba</ta>
            <ta e="T575" id="Seg_6002" s="T574">gi͡en-e-n</ta>
            <ta e="T576" id="Seg_6003" s="T575">hamag-ɨ-n</ta>
            <ta e="T577" id="Seg_6004" s="T576">aːllar-al-lar</ta>
            <ta e="T578" id="Seg_6005" s="T577">hamag-ɨ-n</ta>
            <ta e="T579" id="Seg_6006" s="T578">aːllar-dak-tarɨna</ta>
            <ta e="T580" id="Seg_6007" s="T579">bu</ta>
            <ta e="T581" id="Seg_6008" s="T580">hajɨn</ta>
            <ta e="T582" id="Seg_6009" s="T581">baːs</ta>
            <ta e="T583" id="Seg_6010" s="T582">bu͡ol-aːččɨ</ta>
            <ta e="T584" id="Seg_6011" s="T583">ma-nan</ta>
            <ta e="T585" id="Seg_6012" s="T584">taba</ta>
            <ta e="T587" id="Seg_6013" s="T586">һutuː-r-a</ta>
            <ta e="T588" id="Seg_6014" s="T587">iti</ta>
            <ta e="T589" id="Seg_6015" s="T588">onton</ta>
            <ta e="T590" id="Seg_6016" s="T589">kuturug-u-n</ta>
            <ta e="T591" id="Seg_6017" s="T590">taba</ta>
            <ta e="T592" id="Seg_6018" s="T591">gi͡en-i-n</ta>
            <ta e="T593" id="Seg_6019" s="T592">kaj-a</ta>
            <ta e="T594" id="Seg_6020" s="T593">annʼ-al-lar</ta>
            <ta e="T595" id="Seg_6021" s="T594">bu</ta>
            <ta e="T596" id="Seg_6022" s="T595">kuturug-u-n</ta>
            <ta e="T597" id="Seg_6023" s="T596">kaj-a</ta>
            <ta e="T598" id="Seg_6024" s="T597">as-tak-tarɨna</ta>
            <ta e="T599" id="Seg_6025" s="T598">kɨhɨn</ta>
            <ta e="T600" id="Seg_6026" s="T599">tɨmnɨː-ga</ta>
            <ta e="T601" id="Seg_6027" s="T600">bu</ta>
            <ta e="T602" id="Seg_6028" s="T601">taba</ta>
            <ta e="T603" id="Seg_6029" s="T602">gi͡en-e</ta>
            <ta e="T604" id="Seg_6030" s="T603">et-e</ta>
            <ta e="T605" id="Seg_6031" s="T604">kaččaga</ta>
            <ta e="T606" id="Seg_6032" s="T605">da</ta>
            <ta e="T607" id="Seg_6033" s="T606">emi͡e</ta>
            <ta e="T608" id="Seg_6034" s="T607">köp-pöt</ta>
            <ta e="T609" id="Seg_6035" s="T608">onton</ta>
            <ta e="T610" id="Seg_6036" s="T609">hajɨn</ta>
            <ta e="T611" id="Seg_6037" s="T610">itiː</ta>
            <ta e="T612" id="Seg_6038" s="T611">hagɨna</ta>
            <ta e="T613" id="Seg_6039" s="T612">köh-öl-lör</ta>
            <ta e="T614" id="Seg_6040" s="T613">bu</ta>
            <ta e="T615" id="Seg_6041" s="T614">kös-tök-törüne</ta>
            <ta e="T616" id="Seg_6042" s="T615">itiː-ge</ta>
            <ta e="T617" id="Seg_6043" s="T616">kös-tök-törüne</ta>
            <ta e="T618" id="Seg_6044" s="T617">köh-ön</ta>
            <ta e="T619" id="Seg_6045" s="T618">ih-en-ner</ta>
            <ta e="T620" id="Seg_6046" s="T619">ürek-teːk</ta>
            <ta e="T621" id="Seg_6047" s="T620">hir-der-i-nen</ta>
            <ta e="T622" id="Seg_6048" s="T621">bar-al-lar</ta>
            <ta e="T623" id="Seg_6049" s="T622">bu</ta>
            <ta e="T624" id="Seg_6050" s="T623">ürek-teːk</ta>
            <ta e="T625" id="Seg_6051" s="T624">ulakan</ta>
            <ta e="T626" id="Seg_6052" s="T625">kaja-lar-ga</ta>
            <ta e="T627" id="Seg_6053" s="T626">tu͡oj-daːk</ta>
            <ta e="T628" id="Seg_6054" s="T627">bu͡ol-aːččɨ</ta>
            <ta e="T629" id="Seg_6055" s="T628">töhö</ta>
            <ta e="T630" id="Seg_6056" s="T629">da</ta>
            <ta e="T631" id="Seg_6057" s="T630">itiː-ge</ta>
            <ta e="T632" id="Seg_6058" s="T631">bu</ta>
            <ta e="T633" id="Seg_6059" s="T632">tu͡oj-ɨ</ta>
            <ta e="T634" id="Seg_6060" s="T633">herej-d-e</ta>
            <ta e="T635" id="Seg_6061" s="T634">da</ta>
            <ta e="T636" id="Seg_6062" s="T635">taba</ta>
            <ta e="T637" id="Seg_6063" s="T636">bu</ta>
            <ta e="T638" id="Seg_6064" s="T637">kaja-ga</ta>
            <ta e="T639" id="Seg_6065" s="T638">bar-aːččɨ</ta>
            <ta e="T640" id="Seg_6066" s="T639">köh-ön</ta>
            <ta e="T641" id="Seg_6067" s="T640">is-tek-ke</ta>
            <ta e="T642" id="Seg_6068" s="T641">da</ta>
            <ta e="T643" id="Seg_6069" s="T642">üːr-en</ta>
            <ta e="T644" id="Seg_6070" s="T643">da</ta>
            <ta e="T645" id="Seg_6071" s="T644">is-tek-terinen</ta>
            <ta e="T646" id="Seg_6072" s="T645">ma-nɨ</ta>
            <ta e="T647" id="Seg_6073" s="T646">pastuːk-tar</ta>
            <ta e="T648" id="Seg_6074" s="T647">bu</ta>
            <ta e="T649" id="Seg_6075" s="T648">kaja-nɨ</ta>
            <ta e="T650" id="Seg_6076" s="T649">kör-bökkö</ta>
            <ta e="T651" id="Seg_6077" s="T650">ɨt-taː-n</ta>
            <ta e="T652" id="Seg_6078" s="T651">ɨl-lɨ-lar</ta>
            <ta e="T653" id="Seg_6079" s="T652">daː</ta>
            <ta e="T654" id="Seg_6080" s="T653">h-onon</ta>
            <ta e="T655" id="Seg_6081" s="T654">kül-s-e-kül-s-e</ta>
            <ta e="T656" id="Seg_6082" s="T655">aːh-an</ta>
            <ta e="T657" id="Seg_6083" s="T656">kaːl-al-lar</ta>
            <ta e="T658" id="Seg_6084" s="T657">manna</ta>
            <ta e="T659" id="Seg_6085" s="T658">u͡on</ta>
            <ta e="T660" id="Seg_6086" s="T659">ordug-a</ta>
            <ta e="T661" id="Seg_6087" s="T660">bi͡es</ta>
            <ta e="T662" id="Seg_6088" s="T661">duː</ta>
            <ta e="T663" id="Seg_6089" s="T662">hüːrbe</ta>
            <ta e="T664" id="Seg_6090" s="T663">ordug-a</ta>
            <ta e="T665" id="Seg_6091" s="T664">bi͡es</ta>
            <ta e="T666" id="Seg_6092" s="T665">duː</ta>
            <ta e="T667" id="Seg_6093" s="T666">taba</ta>
            <ta e="T668" id="Seg_6094" s="T667">kaːl-ar</ta>
            <ta e="T669" id="Seg_6095" s="T668">bu</ta>
            <ta e="T670" id="Seg_6096" s="T669">kaːl-bɨt-ɨ-n</ta>
            <ta e="T671" id="Seg_6097" s="T670">öjdöː-böt-tör</ta>
            <ta e="T672" id="Seg_6098" s="T671">huːr-ka</ta>
            <ta e="T673" id="Seg_6099" s="T672">tüh-en-ner</ta>
            <ta e="T674" id="Seg_6100" s="T673">nöŋü͡ö</ta>
            <ta e="T675" id="Seg_6101" s="T674">kihi</ta>
            <ta e="T676" id="Seg_6102" s="T675">ketiː-r</ta>
            <ta e="T677" id="Seg_6103" s="T676">maːdin</ta>
            <ta e="T678" id="Seg_6104" s="T677">bu</ta>
            <ta e="T679" id="Seg_6105" s="T678">köh-öl-leri-ger</ta>
            <ta e="T680" id="Seg_6106" s="T679">keteː-bit</ta>
            <ta e="T681" id="Seg_6107" s="T680">kihi</ta>
            <ta e="T682" id="Seg_6108" s="T681">araj</ta>
            <ta e="T683" id="Seg_6109" s="T682">ugučaːg-a</ta>
            <ta e="T684" id="Seg_6110" s="T683">ol</ta>
            <ta e="T685" id="Seg_6111" s="T684">taba-lar-ga</ta>
            <ta e="T686" id="Seg_6112" s="T685">onno</ta>
            <ta e="T687" id="Seg_6113" s="T686">kaja-ga</ta>
            <ta e="T688" id="Seg_6114" s="T687">kaːl-bɨt</ta>
            <ta e="T689" id="Seg_6115" s="T688">biːr</ta>
            <ta e="T690" id="Seg_6116" s="T689">ugučag-a</ta>
            <ta e="T691" id="Seg_6117" s="T690">harsɨŋŋɨ-tɨ-n</ta>
            <ta e="T692" id="Seg_6118" s="T691">köh-öl-lör</ta>
            <ta e="T693" id="Seg_6119" s="T692">emi͡e</ta>
            <ta e="T694" id="Seg_6120" s="T693">köh-ön</ta>
            <ta e="T695" id="Seg_6121" s="T694">tüh-en</ta>
            <ta e="T696" id="Seg_6122" s="T695">baraːn</ta>
            <ta e="T697" id="Seg_6123" s="T696">nöŋü͡ö</ta>
            <ta e="T698" id="Seg_6124" s="T697">kihi-ler</ta>
            <ta e="T699" id="Seg_6125" s="T698">ketiː-l-ler</ta>
            <ta e="T700" id="Seg_6126" s="T699">maː</ta>
            <ta e="T701" id="Seg_6127" s="T700">ɨnaraː</ta>
            <ta e="T702" id="Seg_6128" s="T701">huːr-ka</ta>
            <ta e="T703" id="Seg_6129" s="T702">keteː-bit</ta>
            <ta e="T704" id="Seg_6130" s="T703">artɨnnaːk</ta>
            <ta e="T705" id="Seg_6131" s="T704">kihi</ta>
            <ta e="T706" id="Seg_6132" s="T705">ket-iː</ta>
            <ta e="T707" id="Seg_6133" s="T706">taks-an</ta>
            <ta e="T708" id="Seg_6134" s="T707">uːčag-ɨ-n</ta>
            <ta e="T709" id="Seg_6135" s="T708">oččogo</ta>
            <ta e="T710" id="Seg_6136" s="T709">hu͡oktuː-r</ta>
            <ta e="T711" id="Seg_6137" s="T710">harsi͡erde</ta>
            <ta e="T712" id="Seg_6138" s="T711">kel-en</ta>
            <ta e="T713" id="Seg_6139" s="T712">diː-r</ta>
            <ta e="T714" id="Seg_6140" s="T713">kaja</ta>
            <ta e="T715" id="Seg_6141" s="T714">mini͡ene</ta>
            <ta e="T716" id="Seg_6142" s="T715">uːčag-ɨ-m</ta>
            <ta e="T717" id="Seg_6143" s="T716">hu͡ok</ta>
            <ta e="T718" id="Seg_6144" s="T717">onno</ta>
            <ta e="T719" id="Seg_6145" s="T718">huːr-ka</ta>
            <ta e="T720" id="Seg_6146" s="T719">da</ta>
            <ta e="T721" id="Seg_6147" s="T720">kör-bötög-ü-m</ta>
            <ta e="T722" id="Seg_6148" s="T721">maːdin</ta>
            <ta e="T723" id="Seg_6149" s="T722">kös-püt</ta>
            <ta e="T724" id="Seg_6150" s="T723">huːr-pa-r</ta>
            <ta e="T725" id="Seg_6151" s="T724">ɨnaraː</ta>
            <ta e="T726" id="Seg_6152" s="T725">huːr-ka</ta>
            <ta e="T727" id="Seg_6153" s="T726">ketiː-r-be-r</ta>
            <ta e="T728" id="Seg_6154" s="T727">miːn-e</ta>
            <ta e="T729" id="Seg_6155" s="T728">hɨldʼ-ɨ-bɨt</ta>
            <ta e="T730" id="Seg_6156" s="T729">taba-m</ta>
            <ta e="T731" id="Seg_6157" s="T730">e-t-e</ta>
            <ta e="T732" id="Seg_6158" s="T731">kirdik</ta>
            <ta e="T733" id="Seg_6159" s="T732">da</ta>
            <ta e="T734" id="Seg_6160" s="T733">onno</ta>
            <ta e="T735" id="Seg_6161" s="T734">huːr-ka</ta>
            <ta e="T736" id="Seg_6162" s="T735">kaːl-bɨt</ta>
            <ta e="T737" id="Seg_6163" s="T736">bu͡olla</ta>
            <ta e="T738" id="Seg_6164" s="T737">d-iː</ta>
            <ta e="T739" id="Seg_6165" s="T738">diː-l-ler</ta>
            <ta e="T740" id="Seg_6166" s="T739">ol</ta>
            <ta e="T741" id="Seg_6167" s="T740">taba-nɨ</ta>
            <ta e="T742" id="Seg_6168" s="T741">bu</ta>
            <ta e="T743" id="Seg_6169" s="T742">huːr-tarɨ-gar</ta>
            <ta e="T744" id="Seg_6170" s="T743">körd-üː</ta>
            <ta e="T745" id="Seg_6171" s="T744">kel-el-ler</ta>
            <ta e="T746" id="Seg_6172" s="T745">bügün</ta>
            <ta e="T747" id="Seg_6173" s="T746">kös-püt</ta>
            <ta e="T748" id="Seg_6174" s="T747">huːr-darɨ-gar</ta>
            <ta e="T749" id="Seg_6175" s="T748">ol</ta>
            <ta e="T750" id="Seg_6176" s="T749">taba-lar-ɨ</ta>
            <ta e="T751" id="Seg_6177" s="T750">ɨnaraː</ta>
            <ta e="T752" id="Seg_6178" s="T751">kös-kö</ta>
            <ta e="T753" id="Seg_6179" s="T752">kaːl-bɨt</ta>
            <ta e="T754" id="Seg_6180" s="T753">taba-lar-ɨ</ta>
            <ta e="T755" id="Seg_6181" s="T754">betereː</ta>
            <ta e="T756" id="Seg_6182" s="T755">huːr-ka</ta>
            <ta e="T757" id="Seg_6183" s="T756">kördüː-l-ler</ta>
            <ta e="T758" id="Seg_6184" s="T757">ol</ta>
            <ta e="T759" id="Seg_6185" s="T758">ɨnaraː</ta>
            <ta e="T760" id="Seg_6186" s="T759">huːr-ka</ta>
            <ta e="T761" id="Seg_6187" s="T760">keteː-bit</ta>
            <ta e="T762" id="Seg_6188" s="T761">pastuːk</ta>
            <ta e="T763" id="Seg_6189" s="T762">ugučag-a</ta>
            <ta e="T764" id="Seg_6190" s="T763">onno</ta>
            <ta e="T765" id="Seg_6191" s="T764">kaːl-bɨt</ta>
            <ta e="T766" id="Seg_6192" s="T765">ol</ta>
            <ta e="T767" id="Seg_6193" s="T766">kaja-ga</ta>
            <ta e="T768" id="Seg_6194" s="T767">ol</ta>
            <ta e="T769" id="Seg_6195" s="T768">hogotok</ta>
            <ta e="T770" id="Seg_6196" s="T769">taba-nɨ</ta>
            <ta e="T771" id="Seg_6197" s="T770">öjdüː-l-ler</ta>
            <ta e="T772" id="Seg_6198" s="T771">onno</ta>
            <ta e="T773" id="Seg_6199" s="T772">kaːl-bɨt-a</ta>
            <ta e="T774" id="Seg_6200" s="T773">u͡on-tan</ta>
            <ta e="T775" id="Seg_6201" s="T774">taksa</ta>
            <ta e="T776" id="Seg_6202" s="T775">taba</ta>
            <ta e="T777" id="Seg_6203" s="T776">ol</ta>
            <ta e="T778" id="Seg_6204" s="T777">ihin</ta>
            <ta e="T781" id="Seg_6205" s="T780">taba</ta>
            <ta e="T782" id="Seg_6206" s="T781">hurag-a</ta>
            <ta e="T783" id="Seg_6207" s="T782">hu͡ok</ta>
            <ta e="T784" id="Seg_6208" s="T783">hüt-er-e</ta>
            <ta e="T785" id="Seg_6209" s="T784">iti</ta>
            <ta e="T786" id="Seg_6210" s="T785">onton</ta>
            <ta e="T787" id="Seg_6211" s="T786">dʼɨl</ta>
            <ta e="T788" id="Seg_6212" s="T787">eŋin-eŋin</ta>
            <ta e="T789" id="Seg_6213" s="T788">bu͡ol-aːččɨ</ta>
            <ta e="T790" id="Seg_6214" s="T789">itiː-l-i͡ek</ta>
            <ta e="T791" id="Seg_6215" s="T790">bu͡ol-aːččɨ</ta>
            <ta e="T792" id="Seg_6216" s="T791">bu</ta>
            <ta e="T793" id="Seg_6217" s="T792">itiː-ge</ta>
            <ta e="T794" id="Seg_6218" s="T793">emi͡e</ta>
            <ta e="T795" id="Seg_6219" s="T794">anɨ-gɨ</ta>
            <ta e="T796" id="Seg_6220" s="T795">pastuːk-tar</ta>
            <ta e="T797" id="Seg_6221" s="T796">taba-nɨ</ta>
            <ta e="T798" id="Seg_6222" s="T797">hataː-n</ta>
            <ta e="T799" id="Seg_6223" s="T798">karaj-bat-tar</ta>
            <ta e="T800" id="Seg_6224" s="T799">karaj-bat-tara</ta>
            <ta e="T801" id="Seg_6225" s="T800">tuːg-u=j</ta>
            <ta e="T802" id="Seg_6226" s="T801">itiː-ge</ta>
            <ta e="T803" id="Seg_6227" s="T802">halgɨn-dʼɨt</ta>
            <ta e="T804" id="Seg_6228" s="T803">taba</ta>
            <ta e="T805" id="Seg_6229" s="T804">bu͡ol-aːččɨ</ta>
            <ta e="T806" id="Seg_6230" s="T805">halgɨn-dʼɨt</ta>
            <ta e="T807" id="Seg_6231" s="T806">taba</ta>
            <ta e="T808" id="Seg_6232" s="T807">aːt-a</ta>
            <ta e="T809" id="Seg_6233" s="T808">tɨ͡al-ɨ</ta>
            <ta e="T810" id="Seg_6234" s="T809">bat-ar</ta>
            <ta e="T811" id="Seg_6235" s="T810">bu</ta>
            <ta e="T812" id="Seg_6236" s="T811">tɨ͡al-ɨ</ta>
            <ta e="T813" id="Seg_6237" s="T812">bat-ar</ta>
            <ta e="T814" id="Seg_6238" s="T813">taba-nɨ</ta>
            <ta e="T815" id="Seg_6239" s="T814">itiː</ta>
            <ta e="T816" id="Seg_6240" s="T815">ah-ɨ͡a-r</ta>
            <ta e="T817" id="Seg_6241" s="T816">di͡eri</ta>
            <ta e="T818" id="Seg_6242" s="T817">kihi</ta>
            <ta e="T819" id="Seg_6243" s="T818">taba-ttan</ta>
            <ta e="T820" id="Seg_6244" s="T819">baːj-aːččɨ</ta>
            <ta e="T821" id="Seg_6245" s="T820">taba</ta>
            <ta e="T822" id="Seg_6246" s="T821">keteː-tek-terine</ta>
            <ta e="T823" id="Seg_6247" s="T822">kihi</ta>
            <ta e="T824" id="Seg_6248" s="T823">küːh-e</ta>
            <ta e="T825" id="Seg_6249" s="T824">tij-eːčči-te</ta>
            <ta e="T826" id="Seg_6250" s="T825">hu͡ok</ta>
            <ta e="T827" id="Seg_6251" s="T826">itiː-ge</ta>
            <ta e="T828" id="Seg_6252" s="T827">tɨ͡al-ɨ</ta>
            <ta e="T830" id="Seg_6253" s="T929">gɨm-mat-tar</ta>
            <ta e="T831" id="Seg_6254" s="T830">harsi͡erde</ta>
            <ta e="T832" id="Seg_6255" s="T831">egel-el-leri-ger</ta>
            <ta e="T833" id="Seg_6256" s="T832">nöŋü͡ö</ta>
            <ta e="T834" id="Seg_6257" s="T833">pastuːk-ka</ta>
            <ta e="T835" id="Seg_6258" s="T834">tuttar-al-larɨ-gar</ta>
            <ta e="T836" id="Seg_6259" s="T835">bihigi</ta>
            <ta e="T837" id="Seg_6260" s="T836">kɨ͡aj-ɨ͡ak-pɨt</ta>
            <ta e="T838" id="Seg_6261" s="T837">hu͡og-a</ta>
            <ta e="T839" id="Seg_6262" s="T838">et</ta>
            <ta e="T840" id="Seg_6263" s="T839">di͡e-n</ta>
            <ta e="T841" id="Seg_6264" s="T840">kahan</ta>
            <ta e="T842" id="Seg_6265" s="T841">da</ta>
            <ta e="T843" id="Seg_6266" s="T842">tɨ͡al</ta>
            <ta e="T844" id="Seg_6267" s="T843">annɨ</ta>
            <ta e="T845" id="Seg_6268" s="T844">di͡ek</ta>
            <ta e="T846" id="Seg_6269" s="T845">darab-iː</ta>
            <ta e="T847" id="Seg_6270" s="T846">gɨn-ɨ͡ak-tarɨ-n</ta>
            <ta e="T848" id="Seg_6271" s="T847">dʼi͡e-lere</ta>
            <ta e="T849" id="Seg_6272" s="T848">tɨ͡al</ta>
            <ta e="T850" id="Seg_6273" s="T849">ürdü</ta>
            <ta e="T851" id="Seg_6274" s="T850">bü</ta>
            <ta e="T852" id="Seg_6275" s="T851">bu͡ol-ar</ta>
            <ta e="T853" id="Seg_6276" s="T852">kördük</ta>
            <ta e="T854" id="Seg_6277" s="T853">taba</ta>
            <ta e="T855" id="Seg_6278" s="T854">oččogo</ta>
            <ta e="T856" id="Seg_6279" s="T855">kɨ͡aj-bakka</ta>
            <ta e="T857" id="Seg_6280" s="T856">tɨ͡al</ta>
            <ta e="T858" id="Seg_6281" s="T857">utara</ta>
            <ta e="T859" id="Seg_6282" s="T858">kel-leg-ine</ta>
            <ta e="T860" id="Seg_6283" s="T859">dʼi͡e-ti-ger</ta>
            <ta e="T861" id="Seg_6284" s="T860">kep-ter-deg-ine</ta>
            <ta e="T862" id="Seg_6285" s="T861">dʼi͡e-teːgi-lere</ta>
            <ta e="T863" id="Seg_6286" s="T862">tur-an-nar</ta>
            <ta e="T864" id="Seg_6287" s="T863">manna</ta>
            <ta e="T865" id="Seg_6288" s="T864">kömölöh-ön-nör</ta>
            <ta e="T866" id="Seg_6289" s="T865">oččogo</ta>
            <ta e="T867" id="Seg_6290" s="T866">kɨ͡aj-al-lar</ta>
            <ta e="T868" id="Seg_6291" s="T867">taba-nɨ</ta>
            <ta e="T869" id="Seg_6292" s="T868">ol</ta>
            <ta e="T870" id="Seg_6293" s="T869">kennine</ta>
            <ta e="T871" id="Seg_6294" s="T870">giniler</ta>
            <ta e="T872" id="Seg_6295" s="T871">haːs-kɨ-ttan</ta>
            <ta e="T873" id="Seg_6296" s="T872">itiː</ta>
            <ta e="T874" id="Seg_6297" s="T873">tüh-e</ta>
            <ta e="T875" id="Seg_6298" s="T874">ilig-ine</ta>
            <ta e="T876" id="Seg_6299" s="T875">haŋardɨː</ta>
            <ta e="T877" id="Seg_6300" s="T876">itiː</ta>
            <ta e="T878" id="Seg_6301" s="T877">tüh-er-i-ger</ta>
            <ta e="T879" id="Seg_6302" s="T878">taba</ta>
            <ta e="T880" id="Seg_6303" s="T879">itiːrgiː-r-i-ger</ta>
            <ta e="T881" id="Seg_6304" s="T880">buru͡o-ga</ta>
            <ta e="T882" id="Seg_6305" s="T881">taba-nɨ</ta>
            <ta e="T883" id="Seg_6306" s="T882">ü͡öret-i͡ek-teri-n</ta>
            <ta e="T884" id="Seg_6307" s="T883">nada</ta>
            <ta e="T885" id="Seg_6308" s="T884">ulakan</ta>
            <ta e="T886" id="Seg_6309" s="T885">itiː</ta>
            <ta e="T887" id="Seg_6310" s="T886">tüh-e</ta>
            <ta e="T888" id="Seg_6311" s="T887">ilig-ine</ta>
            <ta e="T889" id="Seg_6312" s="T888">buru͡o-nu</ta>
            <ta e="T890" id="Seg_6313" s="T889">kör-d-ö</ta>
            <ta e="T891" id="Seg_6314" s="T890">da</ta>
            <ta e="T892" id="Seg_6315" s="T891">töhö</ta>
            <ta e="T893" id="Seg_6316" s="T892">daː</ta>
            <ta e="T894" id="Seg_6317" s="T893">itiː-ge</ta>
            <ta e="T896" id="Seg_6318" s="T894">taba</ta>
            <ta e="T897" id="Seg_6319" s="T896">dʼi͡e-teːgi-lere</ta>
            <ta e="T898" id="Seg_6320" s="T897">itiː-ge</ta>
            <ta e="T899" id="Seg_6321" s="T898">pastuːk-tar</ta>
            <ta e="T900" id="Seg_6322" s="T899">ket-iː</ta>
            <ta e="T901" id="Seg_6323" s="T900">bar-aːrɨ-lar</ta>
            <ta e="T902" id="Seg_6324" s="T901">haŋar-ɨ͡ak</ta>
            <ta e="T903" id="Seg_6325" s="T902">tus-taːk-tar</ta>
            <ta e="T904" id="Seg_6326" s="T903">kaja</ta>
            <ta e="T905" id="Seg_6327" s="T904">itiː</ta>
            <ta e="T906" id="Seg_6328" s="T905">bu͡ol-lag-ɨna</ta>
            <ta e="T907" id="Seg_6329" s="T906">dʼe</ta>
            <ta e="T908" id="Seg_6330" s="T907">kečeh-en</ta>
            <ta e="T909" id="Seg_6331" s="T908">utuj-u-ŋ</ta>
            <ta e="T910" id="Seg_6332" s="T909">buru͡o-l-a-t-aːr-ɨ-ŋ</ta>
            <ta e="T911" id="Seg_6333" s="T910">otuː</ta>
            <ta e="T912" id="Seg_6334" s="T911">ott-oːr-u-ŋ</ta>
            <ta e="T913" id="Seg_6335" s="T912">itiː</ta>
            <ta e="T914" id="Seg_6336" s="T913">bu͡ol-lag-ɨna</ta>
            <ta e="T915" id="Seg_6337" s="T914">bubaːt</ta>
            <ta e="T916" id="Seg_6338" s="T915">kɨ͡aj-ɨ͡ak-pɨt</ta>
            <ta e="T917" id="Seg_6339" s="T916">hu͡og-a</ta>
            <ta e="T918" id="Seg_6340" s="T917">kajtak</ta>
            <ta e="T919" id="Seg_6341" s="T918">ere</ta>
            <ta e="T920" id="Seg_6342" s="T919">itiː</ta>
            <ta e="T921" id="Seg_6343" s="T920">bu͡ol-an</ta>
            <ta e="T922" id="Seg_6344" s="T921">er-er</ta>
            <ta e="T923" id="Seg_6345" s="T922">ol</ta>
            <ta e="T924" id="Seg_6346" s="T923">kördük</ta>
            <ta e="T925" id="Seg_6347" s="T924">karaj-aːččɨ</ta>
            <ta e="T926" id="Seg_6348" s="T925">e-ti-bit</ta>
            <ta e="T927" id="Seg_6349" s="T926">bihigi</ta>
            <ta e="T928" id="Seg_6350" s="T927">taba-nɨ</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_6351" s="T0">dʼe</ta>
            <ta e="T2" id="Seg_6352" s="T1">min</ta>
            <ta e="T3" id="Seg_6353" s="T2">taba-ČIt-LAr-GA</ta>
            <ta e="T4" id="Seg_6354" s="T3">ihilin-A-t-AːrI</ta>
            <ta e="T5" id="Seg_6355" s="T4">haŋar-A-BIn</ta>
            <ta e="T6" id="Seg_6356" s="T5">min</ta>
            <ta e="T7" id="Seg_6357" s="T6">ügüs-LIk</ta>
            <ta e="T8" id="Seg_6358" s="T7">üleleː-BIT</ta>
            <ta e="T9" id="Seg_6359" s="T8">kihi-BIn</ta>
            <ta e="T10" id="Seg_6360" s="T9">taba-GA</ta>
            <ta e="T11" id="Seg_6361" s="T10">urut</ta>
            <ta e="T12" id="Seg_6362" s="T11">bu</ta>
            <ta e="T13" id="Seg_6363" s="T12">taba</ta>
            <ta e="T14" id="Seg_6364" s="T13">töröː-Ar-tI-n</ta>
            <ta e="T15" id="Seg_6365" s="T14">tus-tI-nAn</ta>
            <ta e="T16" id="Seg_6366" s="T15">haŋar-IAK-m</ta>
            <ta e="T17" id="Seg_6367" s="T16">anɨ</ta>
            <ta e="T18" id="Seg_6368" s="T17">kannɨk</ta>
            <ta e="T19" id="Seg_6369" s="T18">da</ta>
            <ta e="T20" id="Seg_6370" s="T19">otto</ta>
            <ta e="T21" id="Seg_6371" s="T20">töröː-Ar-I-ttAn</ta>
            <ta e="T22" id="Seg_6372" s="T21">bu͡ol-Ar</ta>
            <ta e="T23" id="Seg_6373" s="T22">bu͡o</ta>
            <ta e="T24" id="Seg_6374" s="T23">taba</ta>
            <ta e="T25" id="Seg_6375" s="T24">da</ta>
            <ta e="T26" id="Seg_6376" s="T25">ü͡öskeː-Ar-tA</ta>
            <ta e="T27" id="Seg_6377" s="T26">kihi</ta>
            <ta e="T28" id="Seg_6378" s="T27">dʼe</ta>
            <ta e="T29" id="Seg_6379" s="T28">onon</ta>
            <ta e="T30" id="Seg_6380" s="T29">min</ta>
            <ta e="T31" id="Seg_6381" s="T30">haŋar-A-BIn</ta>
            <ta e="T32" id="Seg_6382" s="T31">dʼe</ta>
            <ta e="T33" id="Seg_6383" s="T32">bugurduk</ta>
            <ta e="T34" id="Seg_6384" s="T33">taba</ta>
            <ta e="T35" id="Seg_6385" s="T34">töröː-Ar-tI-GAr</ta>
            <ta e="T36" id="Seg_6386" s="T35">pastux-LAr-GA</ta>
            <ta e="T37" id="Seg_6387" s="T36">ihilin-A-t-AːrI</ta>
            <ta e="T38" id="Seg_6388" s="T37">min</ta>
            <ta e="T39" id="Seg_6389" s="T38">haŋar-Ar-I-m</ta>
            <ta e="T40" id="Seg_6390" s="T39">dʼe</ta>
            <ta e="T41" id="Seg_6391" s="T40">bugurduk-kAːN</ta>
            <ta e="T42" id="Seg_6392" s="T41">anɨ</ta>
            <ta e="T43" id="Seg_6393" s="T42">bu</ta>
            <ta e="T44" id="Seg_6394" s="T43">taba-nI</ta>
            <ta e="T45" id="Seg_6395" s="T44">karaj-Ar-LAr</ta>
            <ta e="T46" id="Seg_6396" s="T45">min</ta>
            <ta e="T47" id="Seg_6397" s="T46">urut</ta>
            <ta e="T48" id="Seg_6398" s="T47">karaj-A</ta>
            <ta e="T49" id="Seg_6399" s="T48">hɨrɨt-Ar</ta>
            <ta e="T50" id="Seg_6400" s="T49">er-TAK-BInA</ta>
            <ta e="T51" id="Seg_6401" s="T50">taba</ta>
            <ta e="T52" id="Seg_6402" s="T51">töröː-TAK-InA</ta>
            <ta e="T53" id="Seg_6403" s="T52">ügüs</ta>
            <ta e="T54" id="Seg_6404" s="T53">prʼičʼine-LAːK</ta>
            <ta e="T55" id="Seg_6405" s="T54">bu͡ol-Ar</ta>
            <ta e="T56" id="Seg_6406" s="T55">taba</ta>
            <ta e="T57" id="Seg_6407" s="T56">töröː-Ar-tI-n</ta>
            <ta e="T58" id="Seg_6408" s="T57">pastux</ta>
            <ta e="T59" id="Seg_6409" s="T58">er-TAK-BInA</ta>
            <ta e="T60" id="Seg_6410" s="T59">keteː-A</ta>
            <ta e="T61" id="Seg_6411" s="T60">hɨrɨt-An-BIn</ta>
            <ta e="T62" id="Seg_6412" s="T61">kerij-AːččI</ta>
            <ta e="T63" id="Seg_6413" s="T62">e-TI-m</ta>
            <ta e="T64" id="Seg_6414" s="T63">taba</ta>
            <ta e="T65" id="Seg_6415" s="T64">töröː-An</ta>
            <ta e="T66" id="Seg_6416" s="T65">ogo-tI-n</ta>
            <ta e="T67" id="Seg_6417" s="T66">tüher-TAK-InA</ta>
            <ta e="T68" id="Seg_6418" s="T67">eŋin-eŋin-LIk</ta>
            <ta e="T69" id="Seg_6419" s="T68">töröː-Ar</ta>
            <ta e="T70" id="Seg_6420" s="T69">taba</ta>
            <ta e="T71" id="Seg_6421" s="T70">horok</ta>
            <ta e="T72" id="Seg_6422" s="T71">taba</ta>
            <ta e="T73" id="Seg_6423" s="T72">tugut-tA</ta>
            <ta e="T74" id="Seg_6424" s="T73">töröː-An</ta>
            <ta e="T75" id="Seg_6425" s="T74">baran</ta>
            <ta e="T76" id="Seg_6426" s="T75">tebi͡elen-A</ta>
            <ta e="T77" id="Seg_6427" s="T76">hɨt-AːččI</ta>
            <ta e="T78" id="Seg_6428" s="T77">kot-An</ta>
            <ta e="T79" id="Seg_6429" s="T78">tur-BAkkA</ta>
            <ta e="T80" id="Seg_6430" s="T79">inʼe-tI-n</ta>
            <ta e="T81" id="Seg_6431" s="T80">kör-TAK-GA</ta>
            <ta e="T82" id="Seg_6432" s="T81">emis</ta>
            <ta e="T83" id="Seg_6433" s="T82">bagajɨ</ta>
            <ta e="T84" id="Seg_6434" s="T83">tugut-tI-n</ta>
            <ta e="T85" id="Seg_6435" s="T84">kör-TAK-GA</ta>
            <ta e="T86" id="Seg_6436" s="T85">ulakan</ta>
            <ta e="T87" id="Seg_6437" s="T86">bagajɨ</ta>
            <ta e="T88" id="Seg_6438" s="T87">bu͡ol-Ar</ta>
            <ta e="T89" id="Seg_6439" s="T88">bu</ta>
            <ta e="T90" id="Seg_6440" s="T89">tebi͡elen-A</ta>
            <ta e="T91" id="Seg_6441" s="T90">hɨt-Ar-nI</ta>
            <ta e="T92" id="Seg_6442" s="T91">ergičij-t-IAK-GA</ta>
            <ta e="T93" id="Seg_6443" s="T92">naːda</ta>
            <ta e="T94" id="Seg_6444" s="T93">ergičij-t-An</ta>
            <ta e="T95" id="Seg_6445" s="T94">kör-TAK-GA</ta>
            <ta e="T96" id="Seg_6446" s="T95">tu͡ok-nI</ta>
            <ta e="T97" id="Seg_6447" s="T96">gɨn-An</ta>
            <ta e="T98" id="Seg_6448" s="T97">togo</ta>
            <ta e="T99" id="Seg_6449" s="T98">tur-BAT</ta>
            <ta e="T100" id="Seg_6450" s="T99">di͡e-An</ta>
            <ta e="T101" id="Seg_6451" s="T100">bu-nI</ta>
            <ta e="T102" id="Seg_6452" s="T101">ɨbaktaː-IAK-GA</ta>
            <ta e="T103" id="Seg_6453" s="T102">bɨ͡ar-tI-n</ta>
            <ta e="T104" id="Seg_6454" s="T103">ɨbaktaː-IAK-GA</ta>
            <ta e="T105" id="Seg_6455" s="T104">taba</ta>
            <ta e="T106" id="Seg_6456" s="T105">töröː-TAK-InA</ta>
            <ta e="T107" id="Seg_6457" s="T106">tugut</ta>
            <ta e="T108" id="Seg_6458" s="T107">gi͡en-tA</ta>
            <ta e="T109" id="Seg_6459" s="T108">kiːn-LAːK</ta>
            <ta e="T110" id="Seg_6460" s="T109">bu͡ol-AːččI</ta>
            <ta e="T111" id="Seg_6461" s="T110">bu</ta>
            <ta e="T112" id="Seg_6462" s="T111">kiːn-tI-n</ta>
            <ta e="T113" id="Seg_6463" s="T112">horok</ta>
            <ta e="T114" id="Seg_6464" s="T113">taba</ta>
            <ta e="T115" id="Seg_6465" s="T114">hubuj-AːččI</ta>
            <ta e="T116" id="Seg_6466" s="T115">bu</ta>
            <ta e="T117" id="Seg_6467" s="T116">kiːn-tA</ta>
            <ta e="T118" id="Seg_6468" s="T117">hubuj-LIN-TAK-InA</ta>
            <ta e="T119" id="Seg_6469" s="T118">bu-nAn</ta>
            <ta e="T120" id="Seg_6470" s="T119">is-I-ttAn</ta>
            <ta e="T121" id="Seg_6471" s="T120">huptu</ta>
            <ta e="T122" id="Seg_6472" s="T121">üːt</ta>
            <ta e="T123" id="Seg_6473" s="T122">bu͡ol-Ar</ta>
            <ta e="T124" id="Seg_6474" s="T123">ol-nAn</ta>
            <ta e="T125" id="Seg_6475" s="T124">tɨːn-tA</ta>
            <ta e="T126" id="Seg_6476" s="T125">tagɨs-Ar</ta>
            <ta e="T127" id="Seg_6477" s="T126">mu͡ot-I-nAn</ta>
            <ta e="T128" id="Seg_6478" s="T127">kam</ta>
            <ta e="T129" id="Seg_6479" s="T128">baːj-TAK-GA</ta>
            <ta e="T130" id="Seg_6480" s="T129">bu</ta>
            <ta e="T131" id="Seg_6481" s="T130">taba</ta>
            <ta e="T132" id="Seg_6482" s="T131">bu</ta>
            <ta e="T133" id="Seg_6483" s="T132">tugut</ta>
            <ta e="T134" id="Seg_6484" s="T133">tur-An</ta>
            <ta e="T135" id="Seg_6485" s="T134">kel-AːččI</ta>
            <ta e="T136" id="Seg_6486" s="T135">ol</ta>
            <ta e="T137" id="Seg_6487" s="T136">aːt-tA</ta>
            <ta e="T138" id="Seg_6488" s="T137">kiːn-tI-n</ta>
            <ta e="T139" id="Seg_6489" s="T138">hubuj-Ar</ta>
            <ta e="T140" id="Seg_6490" s="T139">iti</ta>
            <ta e="T141" id="Seg_6491" s="T140">bu͡ol-Ar</ta>
            <ta e="T142" id="Seg_6492" s="T141">taba</ta>
            <ta e="T143" id="Seg_6493" s="T142">gi͡en-tA</ta>
            <ta e="T144" id="Seg_6494" s="T143">tugut-tI-n</ta>
            <ta e="T145" id="Seg_6495" s="T144">gi͡en-tA</ta>
            <ta e="T146" id="Seg_6496" s="T145">prʼičʼine-tA</ta>
            <ta e="T147" id="Seg_6497" s="T146">töröː-TAK-InA</ta>
            <ta e="T148" id="Seg_6498" s="T147">onton</ta>
            <ta e="T149" id="Seg_6499" s="T148">biːr</ta>
            <ta e="T150" id="Seg_6500" s="T149">prʼičʼine-LAːK</ta>
            <ta e="T151" id="Seg_6501" s="T150">slabaj</ta>
            <ta e="T152" id="Seg_6502" s="T151">taba</ta>
            <ta e="T153" id="Seg_6503" s="T152">töröː-Ar</ta>
            <ta e="T154" id="Seg_6504" s="T153">torugan</ta>
            <ta e="T155" id="Seg_6505" s="T154">inʼe-tA</ta>
            <ta e="T156" id="Seg_6506" s="T155">bu</ta>
            <ta e="T157" id="Seg_6507" s="T156">tugut-tA</ta>
            <ta e="T158" id="Seg_6508" s="T157">töröː-TAK-InA</ta>
            <ta e="T159" id="Seg_6509" s="T158">dʼapču</ta>
            <ta e="T160" id="Seg_6510" s="T159">di͡e-An</ta>
            <ta e="T161" id="Seg_6511" s="T160">aːt-LAN-AːččI</ta>
            <ta e="T162" id="Seg_6512" s="T161">emi͡e</ta>
            <ta e="T163" id="Seg_6513" s="T162">tugut-tA</ta>
            <ta e="T164" id="Seg_6514" s="T163">tur-AːččI-tA</ta>
            <ta e="T165" id="Seg_6515" s="T164">hu͡ok</ta>
            <ta e="T166" id="Seg_6516" s="T165">bu</ta>
            <ta e="T167" id="Seg_6517" s="T166">tugut-nI</ta>
            <ta e="T168" id="Seg_6518" s="T167">ergičij-t-An</ta>
            <ta e="T169" id="Seg_6519" s="T168">kör-TAK-GA</ta>
            <ta e="T170" id="Seg_6520" s="T169">amattan</ta>
            <ta e="T171" id="Seg_6521" s="T170">slabaj</ta>
            <ta e="T172" id="Seg_6522" s="T171">bagajɨ</ta>
            <ta e="T173" id="Seg_6523" s="T172">bu͡ol-Ar</ta>
            <ta e="T174" id="Seg_6524" s="T173">bu</ta>
            <ta e="T175" id="Seg_6525" s="T174">tugut</ta>
            <ta e="T176" id="Seg_6526" s="T175">tebi͡elen-A</ta>
            <ta e="T177" id="Seg_6527" s="T176">agaj</ta>
            <ta e="T178" id="Seg_6528" s="T177">hɨt-AːččI</ta>
            <ta e="T179" id="Seg_6529" s="T178">bu</ta>
            <ta e="T180" id="Seg_6530" s="T179">tugut-nI</ta>
            <ta e="T181" id="Seg_6531" s="T180">ittenne</ta>
            <ta e="T182" id="Seg_6532" s="T181">tut-An</ta>
            <ta e="T183" id="Seg_6533" s="T182">baran</ta>
            <ta e="T184" id="Seg_6534" s="T183">tü͡ös-tI-n</ta>
            <ta e="T185" id="Seg_6535" s="T184">imerij-TAK-GA</ta>
            <ta e="T186" id="Seg_6536" s="T185">oččogo</ta>
            <ta e="T187" id="Seg_6537" s="T186">bu</ta>
            <ta e="T188" id="Seg_6538" s="T187">tü͡ös-tA</ta>
            <ta e="T189" id="Seg_6539" s="T188">dʼapču</ta>
            <ta e="T190" id="Seg_6540" s="T189">gi͡en-tA</ta>
            <ta e="T191" id="Seg_6541" s="T190">kahan</ta>
            <ta e="T192" id="Seg_6542" s="T191">da</ta>
            <ta e="T193" id="Seg_6543" s="T192">tugut</ta>
            <ta e="T194" id="Seg_6544" s="T193">töröː-TAK-InA</ta>
            <ta e="T195" id="Seg_6545" s="T194">oŋu͡ok-tA</ta>
            <ta e="T196" id="Seg_6546" s="T195">oŋurgas</ta>
            <ta e="T197" id="Seg_6547" s="T196">bu͡ol-Ar</ta>
            <ta e="T198" id="Seg_6548" s="T197">bu-nI</ta>
            <ta e="T199" id="Seg_6549" s="T198">tü͡ös-tI-n</ta>
            <ta e="T200" id="Seg_6550" s="T199">imerij-TAK-GA</ta>
            <ta e="T201" id="Seg_6551" s="T200">tur-A-tur-A</ta>
            <ta e="T202" id="Seg_6552" s="T201">tüs-A</ta>
            <ta e="T203" id="Seg_6553" s="T202">hɨrɨt-AːččI</ta>
            <ta e="T204" id="Seg_6554" s="T203">tɨːn-TAK-InA</ta>
            <ta e="T205" id="Seg_6555" s="T204">bu-nI</ta>
            <ta e="T206" id="Seg_6556" s="T205">inʼe-tI-n</ta>
            <ta e="T207" id="Seg_6557" s="T206">tut-An</ta>
            <ta e="T208" id="Seg_6558" s="T207">baran</ta>
            <ta e="T209" id="Seg_6559" s="T208">inʼe-tI-n</ta>
            <ta e="T210" id="Seg_6560" s="T209">em-TAr-IAK-GA</ta>
            <ta e="T211" id="Seg_6561" s="T210">naːda</ta>
            <ta e="T212" id="Seg_6562" s="T211">bu</ta>
            <ta e="T213" id="Seg_6563" s="T212">tugut</ta>
            <ta e="T214" id="Seg_6564" s="T213">tü͡ös-tI-n</ta>
            <ta e="T215" id="Seg_6565" s="T214">kim-nI</ta>
            <ta e="T216" id="Seg_6566" s="T215">ilbij-BIT-tI-n</ta>
            <ta e="T217" id="Seg_6567" s="T216">genne</ta>
            <ta e="T218" id="Seg_6568" s="T217">kot-An</ta>
            <ta e="T219" id="Seg_6569" s="T218">em-AːččI-tA</ta>
            <ta e="T220" id="Seg_6570" s="T219">hu͡ok</ta>
            <ta e="T221" id="Seg_6571" s="T220">bu-nI</ta>
            <ta e="T222" id="Seg_6572" s="T221">inʼe-tA</ta>
            <ta e="T223" id="Seg_6573" s="T222">emij-tA</ta>
            <ta e="T224" id="Seg_6574" s="T223">ɨ͡as-LAːK</ta>
            <ta e="T225" id="Seg_6575" s="T224">bu͡ol-TI-tA</ta>
            <ta e="T226" id="Seg_6576" s="T225">lappagar</ta>
            <ta e="T227" id="Seg_6577" s="T226">bu</ta>
            <ta e="T228" id="Seg_6578" s="T227">ɨ͡as-nI</ta>
            <ta e="T229" id="Seg_6579" s="T228">ɨk-AːččI</ta>
            <ta e="T230" id="Seg_6580" s="T229">kihi</ta>
            <ta e="T231" id="Seg_6581" s="T230">ɨk-An</ta>
            <ta e="T232" id="Seg_6582" s="T231">baran</ta>
            <ta e="T233" id="Seg_6583" s="T232">ɨbagas</ta>
            <ta e="T234" id="Seg_6584" s="T233">emij-tA</ta>
            <ta e="T235" id="Seg_6585" s="T234">kel-TAK-InA</ta>
            <ta e="T236" id="Seg_6586" s="T235">oččogo</ta>
            <ta e="T237" id="Seg_6587" s="T236">em-TAr-AːččI</ta>
            <ta e="T238" id="Seg_6588" s="T237">oččogo</ta>
            <ta e="T239" id="Seg_6589" s="T238">em-TAr-TAK-InA</ta>
            <ta e="T240" id="Seg_6590" s="T239">bu</ta>
            <ta e="T241" id="Seg_6591" s="T240">tugut</ta>
            <ta e="T242" id="Seg_6592" s="T241">kihi</ta>
            <ta e="T243" id="Seg_6593" s="T242">bu͡ol-Ar</ta>
            <ta e="T244" id="Seg_6594" s="T243">pastux-LAr</ta>
            <ta e="T245" id="Seg_6595" s="T244">kös-Ar-LAr</ta>
            <ta e="T246" id="Seg_6596" s="T245">ügüs</ta>
            <ta e="T247" id="Seg_6597" s="T246">taba</ta>
            <ta e="T248" id="Seg_6598" s="T247">töröː-TAK-InA</ta>
            <ta e="T249" id="Seg_6599" s="T248">kahan</ta>
            <ta e="T250" id="Seg_6600" s="T249">da</ta>
            <ta e="T251" id="Seg_6601" s="T250">töröː-A=ilik</ta>
            <ta e="T252" id="Seg_6602" s="T251">taba-nI</ta>
            <ta e="T253" id="Seg_6603" s="T252">ilin</ta>
            <ta e="T254" id="Seg_6604" s="T253">dek</ta>
            <ta e="T255" id="Seg_6605" s="T254">ilin-AːččI-LAr</ta>
            <ta e="T256" id="Seg_6606" s="T255">töröː-BIT</ta>
            <ta e="T257" id="Seg_6607" s="T256">taba-nI</ta>
            <ta e="T258" id="Seg_6608" s="T257">tugut-tA</ta>
            <ta e="T259" id="Seg_6609" s="T258">ulaːt-A</ta>
            <ta e="T260" id="Seg_6610" s="T259">tüs-IAK.[tI]-r</ta>
            <ta e="T261" id="Seg_6611" s="T260">di͡eri</ta>
            <ta e="T262" id="Seg_6612" s="T261">kaːm-BAT-tI-n</ta>
            <ta e="T263" id="Seg_6613" s="T262">ihin</ta>
            <ta e="T264" id="Seg_6614" s="T263">kelin</ta>
            <ta e="T265" id="Seg_6615" s="T264">dek</ta>
            <ta e="T266" id="Seg_6616" s="T265">keːs-Ar-LAr</ta>
            <ta e="T267" id="Seg_6617" s="T266">bu</ta>
            <ta e="T268" id="Seg_6618" s="T267">keːs-Iː-LArI-GAr</ta>
            <ta e="T269" id="Seg_6619" s="T268">bu</ta>
            <ta e="T270" id="Seg_6620" s="T269">dʼe</ta>
            <ta e="T271" id="Seg_6621" s="T270">u͡on-ttAn</ta>
            <ta e="T272" id="Seg_6622" s="T271">taksa</ta>
            <ta e="T273" id="Seg_6623" s="T272">hüːrbe-ččA</ta>
            <ta e="T274" id="Seg_6624" s="T273">taba</ta>
            <ta e="T275" id="Seg_6625" s="T274">töröː-BIT-tI-n</ta>
            <ta e="T276" id="Seg_6626" s="T275">genne</ta>
            <ta e="T277" id="Seg_6627" s="T276">oččogo</ta>
            <ta e="T278" id="Seg_6628" s="T277">bu</ta>
            <ta e="T279" id="Seg_6629" s="T278">töröː-A=ilik</ta>
            <ta e="T280" id="Seg_6630" s="T279">taba-nI</ta>
            <ta e="T281" id="Seg_6631" s="T280">ilin</ta>
            <ta e="T282" id="Seg_6632" s="T281">dek</ta>
            <ta e="T283" id="Seg_6633" s="T282">ilin-IAK-GA</ta>
            <ta e="T284" id="Seg_6634" s="T283">naːda</ta>
            <ta e="T285" id="Seg_6635" s="T284">bu</ta>
            <ta e="T286" id="Seg_6636" s="T285">taba-nI</ta>
            <ta e="T287" id="Seg_6637" s="T286">ilin-Ar-GA</ta>
            <ta e="T288" id="Seg_6638" s="T287">horok</ta>
            <ta e="T289" id="Seg_6639" s="T288">pastux</ta>
            <ta e="T290" id="Seg_6640" s="T289">bu</ta>
            <ta e="T291" id="Seg_6641" s="T290">töröː-BIT</ta>
            <ta e="T292" id="Seg_6642" s="T291">tugut-LAːK-GA</ta>
            <ta e="T293" id="Seg_6643" s="T292">kaːl-Ar</ta>
            <ta e="T294" id="Seg_6644" s="T293">horok</ta>
            <ta e="T295" id="Seg_6645" s="T294">pastux</ta>
            <ta e="T296" id="Seg_6646" s="T295">bu</ta>
            <ta e="T297" id="Seg_6647" s="T296">töröː</ta>
            <ta e="T298" id="Seg_6648" s="T297">töröː-A=ilik</ta>
            <ta e="T299" id="Seg_6649" s="T298">taba-nI</ta>
            <ta e="T300" id="Seg_6650" s="T299">ilin-Ar</ta>
            <ta e="T301" id="Seg_6651" s="T300">bu</ta>
            <ta e="T302" id="Seg_6652" s="T301">ilin-Ar-tI-GAr</ta>
            <ta e="T303" id="Seg_6653" s="T302">ahaː-A-t-An</ta>
            <ta e="T304" id="Seg_6654" s="T303">albun-LAː-An</ta>
            <ta e="T305" id="Seg_6655" s="T304">kalka</ta>
            <ta e="T306" id="Seg_6656" s="T305">hir-GA</ta>
            <ta e="T307" id="Seg_6657" s="T306">kiːr-TAr-An</ta>
            <ta e="T308" id="Seg_6658" s="T307">ü͡ör-tA</ta>
            <ta e="T309" id="Seg_6659" s="T308">bar-Ar-tI-n</ta>
            <ta e="T310" id="Seg_6660" s="T309">köllör-BAkkA</ta>
            <ta e="T311" id="Seg_6661" s="T310">bu</ta>
            <ta e="T312" id="Seg_6662" s="T311">tugut-LAːK</ta>
            <ta e="T314" id="Seg_6663" s="T312">tɨhɨ-nI</ta>
            <ta e="T315" id="Seg_6664" s="T314">kahan</ta>
            <ta e="T316" id="Seg_6665" s="T315">da</ta>
            <ta e="T317" id="Seg_6666" s="T316">taba</ta>
            <ta e="T318" id="Seg_6667" s="T317">hannʼɨ-s-An</ta>
            <ta e="T319" id="Seg_6668" s="T318">tugut-tI-n</ta>
            <ta e="T320" id="Seg_6669" s="T319">keːs-AːččI</ta>
            <ta e="T321" id="Seg_6670" s="T320">batɨs-An</ta>
            <ta e="T322" id="Seg_6671" s="T321">ü͡ör-tI-GAr</ta>
            <ta e="T323" id="Seg_6672" s="T322">tal-A-s-An</ta>
            <ta e="T324" id="Seg_6673" s="T323">ol-nI</ta>
            <ta e="T325" id="Seg_6674" s="T324">ahaː-A-t-A</ta>
            <ta e="T326" id="Seg_6675" s="T325">hɨrɨt-An</ta>
            <ta e="T327" id="Seg_6676" s="T326">tot-BIT-tI-n</ta>
            <ta e="T328" id="Seg_6677" s="T327">kemnen-A</ta>
            <ta e="T329" id="Seg_6678" s="T328">hɨt-Ar-tI-GAr</ta>
            <ta e="T330" id="Seg_6679" s="T329">töröː-A=ilik</ta>
            <ta e="T331" id="Seg_6680" s="T330">taba-tI-n</ta>
            <ta e="T332" id="Seg_6681" s="T331">ilin</ta>
            <ta e="T333" id="Seg_6682" s="T332">dek</ta>
            <ta e="T334" id="Seg_6683" s="T333">köhört-An</ta>
            <ta e="T335" id="Seg_6684" s="T334">ilin-Ar</ta>
            <ta e="T336" id="Seg_6685" s="T335">ol</ta>
            <ta e="T337" id="Seg_6686" s="T336">köhört-TAK-TArInA</ta>
            <ta e="T338" id="Seg_6687" s="T337">horok</ta>
            <ta e="T339" id="Seg_6688" s="T338">pastux-LAr</ta>
            <ta e="T340" id="Seg_6689" s="T339">hürdeːk</ta>
            <ta e="T341" id="Seg_6690" s="T340">ɨraːk</ta>
            <ta e="T342" id="Seg_6691" s="T341">bar-AːččI-LAr</ta>
            <ta e="T343" id="Seg_6692" s="T342">bu</ta>
            <ta e="T344" id="Seg_6693" s="T343">ɨraːk</ta>
            <ta e="T345" id="Seg_6694" s="T344">bar-TAK-TArInA</ta>
            <ta e="T346" id="Seg_6695" s="T345">bu</ta>
            <ta e="T347" id="Seg_6696" s="T346">tugut</ta>
            <ta e="T348" id="Seg_6697" s="T347">harsɨŋŋɨ-tI-n</ta>
            <ta e="T349" id="Seg_6698" s="T348">ol</ta>
            <ta e="T350" id="Seg_6699" s="T349">töröː-A=ilik</ta>
            <ta e="T351" id="Seg_6700" s="T350">taba-LAːK-GA</ta>
            <ta e="T352" id="Seg_6701" s="T351">kelin-GI-tI-n</ta>
            <ta e="T353" id="Seg_6702" s="T352">pastux-LArI-n</ta>
            <ta e="T354" id="Seg_6703" s="T353">ol</ta>
            <ta e="T355" id="Seg_6704" s="T354">bu-nI</ta>
            <ta e="T356" id="Seg_6705" s="T355">töröː-BIT</ta>
            <ta e="T357" id="Seg_6706" s="T356">tɨhɨ-LAːK-LArI-n</ta>
            <ta e="T358" id="Seg_6707" s="T357">pastux-tA</ta>
            <ta e="T359" id="Seg_6708" s="T358">ilin</ta>
            <ta e="T360" id="Seg_6709" s="T359">dek</ta>
            <ta e="T361" id="Seg_6710" s="T360">pastux-LAr-GA</ta>
            <ta e="T362" id="Seg_6711" s="T361">ilin-AːrI-LAr</ta>
            <ta e="T363" id="Seg_6712" s="T362">kös-Ar-LArI-GAr</ta>
            <ta e="T364" id="Seg_6713" s="T363">taba</ta>
            <ta e="T365" id="Seg_6714" s="T364">kot-An</ta>
            <ta e="T366" id="Seg_6715" s="T365">tij-AːččI-tA</ta>
            <ta e="T367" id="Seg_6716" s="T366">hu͡ok</ta>
            <ta e="T368" id="Seg_6717" s="T367">bu</ta>
            <ta e="T369" id="Seg_6718" s="T368">ol</ta>
            <ta e="T370" id="Seg_6719" s="T369">ihin</ta>
            <ta e="T371" id="Seg_6720" s="T370">hugas</ta>
            <ta e="T372" id="Seg_6721" s="T371">tüs-IAK-GA</ta>
            <ta e="T373" id="Seg_6722" s="T372">kör-An</ta>
            <ta e="T374" id="Seg_6723" s="T373">baran</ta>
            <ta e="T375" id="Seg_6724" s="T374">onton</ta>
            <ta e="T376" id="Seg_6725" s="T375">bu͡ollagɨna</ta>
            <ta e="T377" id="Seg_6726" s="T376">bu</ta>
            <ta e="T378" id="Seg_6727" s="T377">pastux-LAr</ta>
            <ta e="T379" id="Seg_6728" s="T378">kahan</ta>
            <ta e="T380" id="Seg_6729" s="T379">da</ta>
            <ta e="T381" id="Seg_6730" s="T380">kalka-LAːK</ta>
            <ta e="T382" id="Seg_6731" s="T381">hir-I-nAn</ta>
            <ta e="T383" id="Seg_6732" s="T382">hɨrɨt-Ar-LAr</ta>
            <ta e="T384" id="Seg_6733" s="T383">maršrut</ta>
            <ta e="T385" id="Seg_6734" s="T384">kör-Ar-LAr</ta>
            <ta e="T386" id="Seg_6735" s="T385">taba</ta>
            <ta e="T387" id="Seg_6736" s="T386">töröː-A-t-Ar-tI-GAr</ta>
            <ta e="T388" id="Seg_6737" s="T387">kalka-LAːK</ta>
            <ta e="T389" id="Seg_6738" s="T388">hir-I-nAn</ta>
            <ta e="T390" id="Seg_6739" s="T389">ojuːr-LAːK</ta>
            <ta e="T391" id="Seg_6740" s="T390">hir-I-nAn</ta>
            <ta e="T392" id="Seg_6741" s="T391">kaja-LAr-LAːK</ta>
            <ta e="T393" id="Seg_6742" s="T392">hir-LAr-I-nAn</ta>
            <ta e="T394" id="Seg_6743" s="T393">ürek-LAr-LAːK</ta>
            <ta e="T395" id="Seg_6744" s="T394">hir-LAr-I-nAn</ta>
            <ta e="T396" id="Seg_6745" s="T395">bu</ta>
            <ta e="T397" id="Seg_6746" s="T396">ulakan</ta>
            <ta e="T398" id="Seg_6747" s="T397">hobu͡oj</ta>
            <ta e="T399" id="Seg_6748" s="T398">bɨhɨn-AːččI</ta>
            <ta e="T400" id="Seg_6749" s="T399">bu</ta>
            <ta e="T401" id="Seg_6750" s="T400">bɨhɨn-TAK-InA</ta>
            <ta e="T402" id="Seg_6751" s="T401">haːs</ta>
            <ta e="T403" id="Seg_6752" s="T402">taba</ta>
            <ta e="T404" id="Seg_6753" s="T403">töröː-Ar-tI-n</ta>
            <ta e="T405" id="Seg_6754" s="T404">hagɨna</ta>
            <ta e="T406" id="Seg_6755" s="T405">kün</ta>
            <ta e="T407" id="Seg_6756" s="T406">hɨrdaː-TAK-InA</ta>
            <ta e="T408" id="Seg_6757" s="T407">kɨhɨn</ta>
            <ta e="T409" id="Seg_6758" s="T408">bɨhɨn-I-BIT</ta>
            <ta e="T410" id="Seg_6759" s="T409">hobu͡oj</ta>
            <ta e="T411" id="Seg_6760" s="T410">at-AːččI</ta>
            <ta e="T412" id="Seg_6761" s="T411">ol</ta>
            <ta e="T413" id="Seg_6762" s="T412">aːt-tA</ta>
            <ta e="T414" id="Seg_6763" s="T413">köŋüs</ta>
            <ta e="T415" id="Seg_6764" s="T414">bu͡ol-Ar</ta>
            <ta e="T416" id="Seg_6765" s="T415">taba</ta>
            <ta e="T417" id="Seg_6766" s="T416">hurak-tA</ta>
            <ta e="T418" id="Seg_6767" s="T417">hu͡ok</ta>
            <ta e="T419" id="Seg_6768" s="T418">hüter-A</ta>
            <ta e="T420" id="Seg_6769" s="T419">ol</ta>
            <ta e="T421" id="Seg_6770" s="T420">köŋüs</ta>
            <ta e="T422" id="Seg_6771" s="T421">is-tI-GAr</ta>
            <ta e="T423" id="Seg_6772" s="T422">tüs-BIT</ta>
            <ta e="T424" id="Seg_6773" s="T423">taba-nI</ta>
            <ta e="T425" id="Seg_6774" s="T424">bu</ta>
            <ta e="T426" id="Seg_6775" s="T425">pastux</ta>
            <ta e="T427" id="Seg_6776" s="T426">keteː-A</ta>
            <ta e="T428" id="Seg_6777" s="T427">hɨrɨt-An</ta>
            <ta e="T429" id="Seg_6778" s="T428">bil-BAT</ta>
            <ta e="T430" id="Seg_6779" s="T429">kas</ta>
            <ta e="T431" id="Seg_6780" s="T430">da</ta>
            <ta e="T432" id="Seg_6781" s="T431">taba</ta>
            <ta e="T433" id="Seg_6782" s="T432">tüs-BIT-tI-n</ta>
            <ta e="T434" id="Seg_6783" s="T433">h-onon</ta>
            <ta e="T435" id="Seg_6784" s="T434">hu͡oktaː-BAT</ta>
            <ta e="T436" id="Seg_6785" s="T435">taba</ta>
            <ta e="T437" id="Seg_6786" s="T436">hurak-tA</ta>
            <ta e="T438" id="Seg_6787" s="T437">hu͡ok</ta>
            <ta e="T439" id="Seg_6788" s="T438">hüt-Ar-tA</ta>
            <ta e="T440" id="Seg_6789" s="T439">ol</ta>
            <ta e="T441" id="Seg_6790" s="T440">onon</ta>
            <ta e="T442" id="Seg_6791" s="T441">brʼigadʼiːr-LAr</ta>
            <ta e="T443" id="Seg_6792" s="T442">bu͡ol</ta>
            <ta e="T444" id="Seg_6793" s="T443">pastux-LAr</ta>
            <ta e="T445" id="Seg_6794" s="T444">bu͡ol</ta>
            <ta e="T446" id="Seg_6795" s="T445">haːs</ta>
            <ta e="T447" id="Seg_6796" s="T446">taba</ta>
            <ta e="T448" id="Seg_6797" s="T447">töröː-Ar-tI-n</ta>
            <ta e="T449" id="Seg_6798" s="T448">hagɨna</ta>
            <ta e="T451" id="Seg_6799" s="T450">kaːr</ta>
            <ta e="T452" id="Seg_6800" s="T451">ir-Ar-tI-n</ta>
            <ta e="T453" id="Seg_6801" s="T452">hagɨna</ta>
            <ta e="T454" id="Seg_6802" s="T453">üčügej-LIk</ta>
            <ta e="T455" id="Seg_6803" s="T454">hir-LArI-n</ta>
            <ta e="T456" id="Seg_6804" s="T455">kör-IAK-LArI-n</ta>
            <ta e="T457" id="Seg_6805" s="T456">naːda</ta>
            <ta e="T458" id="Seg_6806" s="T457">hobu͡oj</ta>
            <ta e="T459" id="Seg_6807" s="T458">kajɨt-I-BIT-tA</ta>
            <ta e="T460" id="Seg_6808" s="T459">purgaː</ta>
            <ta e="T461" id="Seg_6809" s="T460">bu͡ol-TAK-InA</ta>
            <ta e="T462" id="Seg_6810" s="T461">tip-I-LIN-An</ta>
            <ta e="T463" id="Seg_6811" s="T462">kaːl-AːččI</ta>
            <ta e="T464" id="Seg_6812" s="T463">ol</ta>
            <ta e="T465" id="Seg_6813" s="T464">gɨn-An</ta>
            <ta e="T466" id="Seg_6814" s="T465">baran</ta>
            <ta e="T467" id="Seg_6815" s="T466">ol</ta>
            <ta e="T468" id="Seg_6816" s="T467">tip-I-LIN-I-BIT-tI-n</ta>
            <ta e="T469" id="Seg_6817" s="T468">genne</ta>
            <ta e="T470" id="Seg_6818" s="T469">ol-nI</ta>
            <ta e="T471" id="Seg_6819" s="T470">taba</ta>
            <ta e="T472" id="Seg_6820" s="T471">kör-BAT</ta>
            <ta e="T473" id="Seg_6821" s="T472">h-onon</ta>
            <ta e="T474" id="Seg_6822" s="T473">tüs-Ar</ta>
            <ta e="T475" id="Seg_6823" s="T474">ol</ta>
            <ta e="T476" id="Seg_6824" s="T475">tüs-TAK-InA</ta>
            <ta e="T477" id="Seg_6825" s="T476">bi͡es-tü͡ört</ta>
            <ta e="T478" id="Seg_6826" s="T477">mi͡eter-LAːK</ta>
            <ta e="T479" id="Seg_6827" s="T478">diriŋ-LAːK</ta>
            <ta e="T480" id="Seg_6828" s="T479">bu͡ol-AːččI</ta>
            <ta e="T481" id="Seg_6829" s="T480">bu</ta>
            <ta e="T482" id="Seg_6830" s="T481">köŋüs</ta>
            <ta e="T483" id="Seg_6831" s="T482">kaːr</ta>
            <ta e="T484" id="Seg_6832" s="T483">bɨhɨn-I-BIT</ta>
            <ta e="T485" id="Seg_6833" s="T484">ol</ta>
            <ta e="T486" id="Seg_6834" s="T485">ihin</ta>
            <ta e="T487" id="Seg_6835" s="T486">onnuk-nI</ta>
            <ta e="T488" id="Seg_6836" s="T487">bul-BAkkA</ta>
            <ta e="T489" id="Seg_6837" s="T488">taba</ta>
            <ta e="T490" id="Seg_6838" s="T489">hurak-tA</ta>
            <ta e="T491" id="Seg_6839" s="T490">hu͡ok</ta>
            <ta e="T492" id="Seg_6840" s="T491">hüt-Ar</ta>
            <ta e="T493" id="Seg_6841" s="T492">iti</ta>
            <ta e="T494" id="Seg_6842" s="T493">onton</ta>
            <ta e="T495" id="Seg_6843" s="T494">ürek-LAːK</ta>
            <ta e="T496" id="Seg_6844" s="T495">hir-LAr-GA</ta>
            <ta e="T497" id="Seg_6845" s="T496">bu͡ollagɨna</ta>
            <ta e="T498" id="Seg_6846" s="T497">kadʼɨrɨk-LAː-TAK-InA</ta>
            <ta e="T499" id="Seg_6847" s="T498">kahan</ta>
            <ta e="T500" id="Seg_6848" s="T499">da</ta>
            <ta e="T501" id="Seg_6849" s="T500">ürek-LAːK</ta>
            <ta e="T502" id="Seg_6850" s="T501">hir-I-nAn</ta>
            <ta e="T503" id="Seg_6851" s="T502">bu͡ol-Ar</ta>
            <ta e="T504" id="Seg_6852" s="T503">maršrut</ta>
            <ta e="T505" id="Seg_6853" s="T504">bu</ta>
            <ta e="T506" id="Seg_6854" s="T505">kaːr-nI</ta>
            <ta e="T507" id="Seg_6855" s="T506">kɨtta</ta>
            <ta e="T508" id="Seg_6856" s="T507">kaja</ta>
            <ta e="T509" id="Seg_6857" s="T508">hir</ta>
            <ta e="T510" id="Seg_6858" s="T509">ɨkkarda-tA</ta>
            <ta e="T511" id="Seg_6859" s="T510">at-AːččI</ta>
            <ta e="T512" id="Seg_6860" s="T511">emi͡e</ta>
            <ta e="T513" id="Seg_6861" s="T512">onon</ta>
            <ta e="T514" id="Seg_6862" s="T513">emi͡e</ta>
            <ta e="T515" id="Seg_6863" s="T514">tüs-AːččI</ta>
            <ta e="T516" id="Seg_6864" s="T515">tugut</ta>
            <ta e="T517" id="Seg_6865" s="T516">bu͡ol</ta>
            <ta e="T518" id="Seg_6866" s="T517">ulakan</ta>
            <ta e="T519" id="Seg_6867" s="T518">taba</ta>
            <ta e="T520" id="Seg_6868" s="T519">bu͡ol</ta>
            <ta e="T521" id="Seg_6869" s="T520">onton</ta>
            <ta e="T522" id="Seg_6870" s="T521">bu</ta>
            <ta e="T523" id="Seg_6871" s="T522">pastux-LAr</ta>
            <ta e="T524" id="Seg_6872" s="T523">haːs</ta>
            <ta e="T525" id="Seg_6873" s="T524">hajɨn-LArA</ta>
            <ta e="T526" id="Seg_6874" s="T525">čugahaː-TAK-InA</ta>
            <ta e="T527" id="Seg_6875" s="T526">haːs-GI-LArI-GAr</ta>
            <ta e="T528" id="Seg_6876" s="T527">uːčak-LArI-n</ta>
            <ta e="T529" id="Seg_6877" s="T528">hataː-An</ta>
            <ta e="T530" id="Seg_6878" s="T529">karaj-n-BAT-LAr</ta>
            <ta e="T531" id="Seg_6879" s="T530">künükte-tI-n</ta>
            <ta e="T532" id="Seg_6880" s="T531">tonoː-BAT-LAr</ta>
            <ta e="T533" id="Seg_6881" s="T532">haːs</ta>
            <ta e="T534" id="Seg_6882" s="T533">künükte-tI-n</ta>
            <ta e="T535" id="Seg_6883" s="T534">tonoː-TAK-TArInA</ta>
            <ta e="T536" id="Seg_6884" s="T535">ol</ta>
            <ta e="T537" id="Seg_6885" s="T536">künükte-tI-n</ta>
            <ta e="T538" id="Seg_6886" s="T537">tonoː-TAK-TArInA</ta>
            <ta e="T539" id="Seg_6887" s="T538">bu</ta>
            <ta e="T540" id="Seg_6888" s="T539">künükte</ta>
            <ta e="T541" id="Seg_6889" s="T540">os-Ar</ta>
            <ta e="T542" id="Seg_6890" s="T541">beje-tA</ta>
            <ta e="T543" id="Seg_6891" s="T542">künükte</ta>
            <ta e="T544" id="Seg_6892" s="T543">haːs</ta>
            <ta e="T545" id="Seg_6893" s="T544">ulaːt-An</ta>
            <ta e="T546" id="Seg_6894" s="T545">tagɨs-TAK-InA</ta>
            <ta e="T547" id="Seg_6895" s="T546">tiriː-tI-n</ta>
            <ta e="T548" id="Seg_6896" s="T547">is-tI-ttAn</ta>
            <ta e="T549" id="Seg_6897" s="T548">beje-tA</ta>
            <ta e="T550" id="Seg_6898" s="T549">tulun-An</ta>
            <ta e="T551" id="Seg_6899" s="T550">tüs-IAK.[tI]-r</ta>
            <ta e="T552" id="Seg_6900" s="T551">di͡eri</ta>
            <ta e="T553" id="Seg_6901" s="T552">hɨrɨt-TAK-InA</ta>
            <ta e="T554" id="Seg_6902" s="T553">taba-GA</ta>
            <ta e="T555" id="Seg_6903" s="T554">da</ta>
            <ta e="T556" id="Seg_6904" s="T555">kuhagan</ta>
            <ta e="T557" id="Seg_6905" s="T556">horok</ta>
            <ta e="T558" id="Seg_6906" s="T557">künükte-nI</ta>
            <ta e="T559" id="Seg_6907" s="T558">bütej-LIː</ta>
            <ta e="T560" id="Seg_6908" s="T559">miːn-A-miːn-A-LAr</ta>
            <ta e="T561" id="Seg_6909" s="T560">taba</ta>
            <ta e="T562" id="Seg_6910" s="T561">gi͡en-tA</ta>
            <ta e="T563" id="Seg_6911" s="T562">öl-Ar</ta>
            <ta e="T564" id="Seg_6912" s="T563">ün-Ar-tA</ta>
            <ta e="T565" id="Seg_6913" s="T564">ol</ta>
            <ta e="T566" id="Seg_6914" s="T565">horok</ta>
            <ta e="T567" id="Seg_6915" s="T566">pastux</ta>
            <ta e="T568" id="Seg_6916" s="T567">taba-nI</ta>
            <ta e="T569" id="Seg_6917" s="T568">da</ta>
            <ta e="T570" id="Seg_6918" s="T569">kölün-A</ta>
            <ta e="T571" id="Seg_6919" s="T570">hataː-BAT</ta>
            <ta e="T572" id="Seg_6920" s="T571">pabadok-LArA</ta>
            <ta e="T573" id="Seg_6921" s="T572">bɨhɨn-TI-tA</ta>
            <ta e="T574" id="Seg_6922" s="T573">taba</ta>
            <ta e="T575" id="Seg_6923" s="T574">gi͡en-tA-n</ta>
            <ta e="T576" id="Seg_6924" s="T575">hamak-tI-n</ta>
            <ta e="T577" id="Seg_6925" s="T576">allar-Ar-LAr</ta>
            <ta e="T578" id="Seg_6926" s="T577">hamak-tI-n</ta>
            <ta e="T579" id="Seg_6927" s="T578">allar-TAK-TArInA</ta>
            <ta e="T580" id="Seg_6928" s="T579">bu</ta>
            <ta e="T581" id="Seg_6929" s="T580">hajɨn</ta>
            <ta e="T582" id="Seg_6930" s="T581">baːs</ta>
            <ta e="T583" id="Seg_6931" s="T582">bu͡ol-AːččI</ta>
            <ta e="T584" id="Seg_6932" s="T583">bu-nAn</ta>
            <ta e="T585" id="Seg_6933" s="T584">taba</ta>
            <ta e="T587" id="Seg_6934" s="T586">һutaː-Ar-tA</ta>
            <ta e="T588" id="Seg_6935" s="T587">iti</ta>
            <ta e="T589" id="Seg_6936" s="T588">onton</ta>
            <ta e="T590" id="Seg_6937" s="T589">kuturuk-tI-n</ta>
            <ta e="T591" id="Seg_6938" s="T590">taba</ta>
            <ta e="T592" id="Seg_6939" s="T591">gi͡en-tI-n</ta>
            <ta e="T593" id="Seg_6940" s="T592">kaj-A</ta>
            <ta e="T594" id="Seg_6941" s="T593">as-Ar-LAr</ta>
            <ta e="T595" id="Seg_6942" s="T594">bu</ta>
            <ta e="T596" id="Seg_6943" s="T595">kuturuk-tI-n</ta>
            <ta e="T597" id="Seg_6944" s="T596">kaj-A</ta>
            <ta e="T598" id="Seg_6945" s="T597">as-TAK-TArInA</ta>
            <ta e="T599" id="Seg_6946" s="T598">kɨhɨn</ta>
            <ta e="T600" id="Seg_6947" s="T599">tɨmnɨː-GA</ta>
            <ta e="T601" id="Seg_6948" s="T600">bu</ta>
            <ta e="T602" id="Seg_6949" s="T601">taba</ta>
            <ta e="T603" id="Seg_6950" s="T602">gi͡en-tA</ta>
            <ta e="T604" id="Seg_6951" s="T603">et-tA</ta>
            <ta e="T605" id="Seg_6952" s="T604">kaččaga</ta>
            <ta e="T606" id="Seg_6953" s="T605">da</ta>
            <ta e="T607" id="Seg_6954" s="T606">emi͡e</ta>
            <ta e="T608" id="Seg_6955" s="T607">köt-BAT</ta>
            <ta e="T609" id="Seg_6956" s="T608">onton</ta>
            <ta e="T610" id="Seg_6957" s="T609">hajɨn</ta>
            <ta e="T611" id="Seg_6958" s="T610">itiː</ta>
            <ta e="T612" id="Seg_6959" s="T611">hagɨna</ta>
            <ta e="T613" id="Seg_6960" s="T612">kös-Ar-LAr</ta>
            <ta e="T614" id="Seg_6961" s="T613">bu</ta>
            <ta e="T615" id="Seg_6962" s="T614">kös-TAK-TArInA</ta>
            <ta e="T616" id="Seg_6963" s="T615">itiː-GA</ta>
            <ta e="T617" id="Seg_6964" s="T616">kös-TAK-TArInA</ta>
            <ta e="T618" id="Seg_6965" s="T617">kös-An</ta>
            <ta e="T619" id="Seg_6966" s="T618">is-An-LAr</ta>
            <ta e="T620" id="Seg_6967" s="T619">ürek-LAːK</ta>
            <ta e="T621" id="Seg_6968" s="T620">hir-LAr-I-nAn</ta>
            <ta e="T622" id="Seg_6969" s="T621">bar-Ar-LAr</ta>
            <ta e="T623" id="Seg_6970" s="T622">bu</ta>
            <ta e="T624" id="Seg_6971" s="T623">ürek-LAːK</ta>
            <ta e="T625" id="Seg_6972" s="T624">ulakan</ta>
            <ta e="T626" id="Seg_6973" s="T625">kaja-LAr-GA</ta>
            <ta e="T627" id="Seg_6974" s="T626">tu͡oj-LAːK</ta>
            <ta e="T628" id="Seg_6975" s="T627">bu͡ol-AːččI</ta>
            <ta e="T629" id="Seg_6976" s="T628">töhö</ta>
            <ta e="T630" id="Seg_6977" s="T629">da</ta>
            <ta e="T631" id="Seg_6978" s="T630">itiː-GA</ta>
            <ta e="T632" id="Seg_6979" s="T631">bu</ta>
            <ta e="T633" id="Seg_6980" s="T632">tu͡oj-nI</ta>
            <ta e="T634" id="Seg_6981" s="T633">herej-TI-tA</ta>
            <ta e="T635" id="Seg_6982" s="T634">da</ta>
            <ta e="T636" id="Seg_6983" s="T635">taba</ta>
            <ta e="T637" id="Seg_6984" s="T636">bu</ta>
            <ta e="T638" id="Seg_6985" s="T637">kaja-GA</ta>
            <ta e="T639" id="Seg_6986" s="T638">bar-AːččI</ta>
            <ta e="T640" id="Seg_6987" s="T639">kös-An</ta>
            <ta e="T641" id="Seg_6988" s="T640">is-TAK-GA</ta>
            <ta e="T642" id="Seg_6989" s="T641">da</ta>
            <ta e="T643" id="Seg_6990" s="T642">üːr-An</ta>
            <ta e="T644" id="Seg_6991" s="T643">da</ta>
            <ta e="T645" id="Seg_6992" s="T644">is-TAK-TArInA</ta>
            <ta e="T646" id="Seg_6993" s="T645">bu-nI</ta>
            <ta e="T647" id="Seg_6994" s="T646">pastux-LAr</ta>
            <ta e="T648" id="Seg_6995" s="T647">bu</ta>
            <ta e="T649" id="Seg_6996" s="T648">kaja-nI</ta>
            <ta e="T650" id="Seg_6997" s="T649">kör-BAkkA</ta>
            <ta e="T651" id="Seg_6998" s="T650">ɨt-LAː-An</ta>
            <ta e="T652" id="Seg_6999" s="T651">ɨl-TI-LAr</ta>
            <ta e="T653" id="Seg_7000" s="T652">da</ta>
            <ta e="T654" id="Seg_7001" s="T653">h-onon</ta>
            <ta e="T655" id="Seg_7002" s="T654">kül-s-A-kül-s-A</ta>
            <ta e="T656" id="Seg_7003" s="T655">aːs-An</ta>
            <ta e="T657" id="Seg_7004" s="T656">kaːl-Ar-LAr</ta>
            <ta e="T658" id="Seg_7005" s="T657">manna</ta>
            <ta e="T659" id="Seg_7006" s="T658">u͡on</ta>
            <ta e="T660" id="Seg_7007" s="T659">orduk-tA</ta>
            <ta e="T661" id="Seg_7008" s="T660">bi͡es</ta>
            <ta e="T662" id="Seg_7009" s="T661">du͡o</ta>
            <ta e="T663" id="Seg_7010" s="T662">hüːrbe</ta>
            <ta e="T664" id="Seg_7011" s="T663">orduk-tA</ta>
            <ta e="T665" id="Seg_7012" s="T664">bi͡es</ta>
            <ta e="T666" id="Seg_7013" s="T665">du͡o</ta>
            <ta e="T667" id="Seg_7014" s="T666">taba</ta>
            <ta e="T668" id="Seg_7015" s="T667">kaːl-Ar</ta>
            <ta e="T669" id="Seg_7016" s="T668">bu</ta>
            <ta e="T670" id="Seg_7017" s="T669">kaːl-BIT-tI-n</ta>
            <ta e="T671" id="Seg_7018" s="T670">öjdöː-BAT-LAr</ta>
            <ta e="T672" id="Seg_7019" s="T671">huːrt-GA</ta>
            <ta e="T673" id="Seg_7020" s="T672">tüs-An-LAr</ta>
            <ta e="T674" id="Seg_7021" s="T673">nöŋü͡ö</ta>
            <ta e="T675" id="Seg_7022" s="T674">kihi</ta>
            <ta e="T676" id="Seg_7023" s="T675">keteː-Ar</ta>
            <ta e="T677" id="Seg_7024" s="T676">maːjdiːn</ta>
            <ta e="T678" id="Seg_7025" s="T677">bu</ta>
            <ta e="T679" id="Seg_7026" s="T678">kös-Ar-LArI-GAr</ta>
            <ta e="T680" id="Seg_7027" s="T679">keteː-BIT</ta>
            <ta e="T681" id="Seg_7028" s="T680">kihi</ta>
            <ta e="T682" id="Seg_7029" s="T681">agaj</ta>
            <ta e="T683" id="Seg_7030" s="T682">ugučak-tA</ta>
            <ta e="T684" id="Seg_7031" s="T683">ol</ta>
            <ta e="T685" id="Seg_7032" s="T684">taba-LAr-GA</ta>
            <ta e="T686" id="Seg_7033" s="T685">onno</ta>
            <ta e="T687" id="Seg_7034" s="T686">kaja-GA</ta>
            <ta e="T688" id="Seg_7035" s="T687">kaːl-BIT</ta>
            <ta e="T689" id="Seg_7036" s="T688">biːr</ta>
            <ta e="T690" id="Seg_7037" s="T689">ugučak-tA</ta>
            <ta e="T691" id="Seg_7038" s="T690">harsɨŋŋɨ-tI-n</ta>
            <ta e="T692" id="Seg_7039" s="T691">kös-Ar-LAr</ta>
            <ta e="T693" id="Seg_7040" s="T692">emi͡e</ta>
            <ta e="T694" id="Seg_7041" s="T693">kös-An</ta>
            <ta e="T695" id="Seg_7042" s="T694">tüs-An</ta>
            <ta e="T696" id="Seg_7043" s="T695">baran</ta>
            <ta e="T697" id="Seg_7044" s="T696">nöŋü͡ö</ta>
            <ta e="T698" id="Seg_7045" s="T697">kihi-LAr</ta>
            <ta e="T699" id="Seg_7046" s="T698">keteː-Ar-LAr</ta>
            <ta e="T700" id="Seg_7047" s="T699">bu</ta>
            <ta e="T701" id="Seg_7048" s="T700">ɨnaraː</ta>
            <ta e="T702" id="Seg_7049" s="T701">huːrt-GA</ta>
            <ta e="T703" id="Seg_7050" s="T702">keteː-BIT</ta>
            <ta e="T704" id="Seg_7051" s="T703">artɨnnaːk</ta>
            <ta e="T705" id="Seg_7052" s="T704">kihi</ta>
            <ta e="T706" id="Seg_7053" s="T705">keteː-A</ta>
            <ta e="T707" id="Seg_7054" s="T706">tagɨs-An</ta>
            <ta e="T708" id="Seg_7055" s="T707">uːčak-tI-n</ta>
            <ta e="T709" id="Seg_7056" s="T708">oččogo</ta>
            <ta e="T710" id="Seg_7057" s="T709">hu͡oktaː-Ar</ta>
            <ta e="T711" id="Seg_7058" s="T710">harsi͡erda</ta>
            <ta e="T712" id="Seg_7059" s="T711">kel-An</ta>
            <ta e="T713" id="Seg_7060" s="T712">di͡e-Ar</ta>
            <ta e="T714" id="Seg_7061" s="T713">kaja</ta>
            <ta e="T715" id="Seg_7062" s="T714">mini͡ene</ta>
            <ta e="T716" id="Seg_7063" s="T715">uːčak-I-m</ta>
            <ta e="T717" id="Seg_7064" s="T716">hu͡ok</ta>
            <ta e="T718" id="Seg_7065" s="T717">onno</ta>
            <ta e="T719" id="Seg_7066" s="T718">huːrt-GA</ta>
            <ta e="T720" id="Seg_7067" s="T719">da</ta>
            <ta e="T721" id="Seg_7068" s="T720">kör-BAtAK-I-m</ta>
            <ta e="T722" id="Seg_7069" s="T721">maːjdiːn</ta>
            <ta e="T723" id="Seg_7070" s="T722">kös-BIT</ta>
            <ta e="T724" id="Seg_7071" s="T723">huːrt-BA-r</ta>
            <ta e="T725" id="Seg_7072" s="T724">ɨnaraː</ta>
            <ta e="T726" id="Seg_7073" s="T725">huːrt-GA</ta>
            <ta e="T727" id="Seg_7074" s="T726">keteː-Ar-BA-r</ta>
            <ta e="T728" id="Seg_7075" s="T727">miːn-A</ta>
            <ta e="T729" id="Seg_7076" s="T728">hɨrɨt-I-BIT</ta>
            <ta e="T730" id="Seg_7077" s="T729">taba-m</ta>
            <ta e="T731" id="Seg_7078" s="T730">e-TI-tA</ta>
            <ta e="T732" id="Seg_7079" s="T731">kirdik</ta>
            <ta e="T733" id="Seg_7080" s="T732">da</ta>
            <ta e="T734" id="Seg_7081" s="T733">onno</ta>
            <ta e="T735" id="Seg_7082" s="T734">huːrt-GA</ta>
            <ta e="T736" id="Seg_7083" s="T735">kaːl-BIT</ta>
            <ta e="T737" id="Seg_7084" s="T736">bu͡olla</ta>
            <ta e="T738" id="Seg_7085" s="T737">di͡e-A</ta>
            <ta e="T739" id="Seg_7086" s="T738">di͡e-Ar-LAr</ta>
            <ta e="T740" id="Seg_7087" s="T739">ol</ta>
            <ta e="T741" id="Seg_7088" s="T740">taba-nI</ta>
            <ta e="T742" id="Seg_7089" s="T741">bu</ta>
            <ta e="T743" id="Seg_7090" s="T742">huːrt-LArI-GAr</ta>
            <ta e="T744" id="Seg_7091" s="T743">kördöː-A</ta>
            <ta e="T745" id="Seg_7092" s="T744">kel-Ar-LAr</ta>
            <ta e="T746" id="Seg_7093" s="T745">bügün</ta>
            <ta e="T747" id="Seg_7094" s="T746">kös-BIT</ta>
            <ta e="T748" id="Seg_7095" s="T747">huːrt-LArI-GAr</ta>
            <ta e="T749" id="Seg_7096" s="T748">ol</ta>
            <ta e="T750" id="Seg_7097" s="T749">taba-LAr-nI</ta>
            <ta e="T751" id="Seg_7098" s="T750">ɨnaraː</ta>
            <ta e="T752" id="Seg_7099" s="T751">kös-GA</ta>
            <ta e="T753" id="Seg_7100" s="T752">kaːl-BIT</ta>
            <ta e="T754" id="Seg_7101" s="T753">taba-LAr-nI</ta>
            <ta e="T755" id="Seg_7102" s="T754">betereː</ta>
            <ta e="T756" id="Seg_7103" s="T755">huːrt-GA</ta>
            <ta e="T757" id="Seg_7104" s="T756">kördöː-Ar-LAr</ta>
            <ta e="T758" id="Seg_7105" s="T757">ol</ta>
            <ta e="T759" id="Seg_7106" s="T758">ɨnaraː</ta>
            <ta e="T760" id="Seg_7107" s="T759">huːrt-GA</ta>
            <ta e="T761" id="Seg_7108" s="T760">keteː-BIT</ta>
            <ta e="T762" id="Seg_7109" s="T761">pastux</ta>
            <ta e="T763" id="Seg_7110" s="T762">ugučak-tA</ta>
            <ta e="T764" id="Seg_7111" s="T763">onno</ta>
            <ta e="T765" id="Seg_7112" s="T764">kaːl-BIT</ta>
            <ta e="T766" id="Seg_7113" s="T765">ol</ta>
            <ta e="T767" id="Seg_7114" s="T766">kaja-GA</ta>
            <ta e="T768" id="Seg_7115" s="T767">ol</ta>
            <ta e="T769" id="Seg_7116" s="T768">čogotok</ta>
            <ta e="T770" id="Seg_7117" s="T769">taba-nI</ta>
            <ta e="T771" id="Seg_7118" s="T770">öjdöː-Ar-LAr</ta>
            <ta e="T772" id="Seg_7119" s="T771">onno</ta>
            <ta e="T773" id="Seg_7120" s="T772">kaːl-BIT-tA</ta>
            <ta e="T774" id="Seg_7121" s="T773">u͡on-ttAn</ta>
            <ta e="T775" id="Seg_7122" s="T774">taksa</ta>
            <ta e="T776" id="Seg_7123" s="T775">taba</ta>
            <ta e="T777" id="Seg_7124" s="T776">ol</ta>
            <ta e="T778" id="Seg_7125" s="T777">ihin</ta>
            <ta e="T781" id="Seg_7126" s="T780">taba</ta>
            <ta e="T782" id="Seg_7127" s="T781">hurak-tA</ta>
            <ta e="T783" id="Seg_7128" s="T782">hu͡ok</ta>
            <ta e="T784" id="Seg_7129" s="T783">hüt-Ar-tA</ta>
            <ta e="T785" id="Seg_7130" s="T784">iti</ta>
            <ta e="T786" id="Seg_7131" s="T785">onton</ta>
            <ta e="T787" id="Seg_7132" s="T786">dʼɨl</ta>
            <ta e="T788" id="Seg_7133" s="T787">eŋin-eŋin</ta>
            <ta e="T789" id="Seg_7134" s="T788">bu͡ol-AːččI</ta>
            <ta e="T790" id="Seg_7135" s="T789">itiː-LAː-IAK</ta>
            <ta e="T791" id="Seg_7136" s="T790">bu͡ol-AːččI</ta>
            <ta e="T792" id="Seg_7137" s="T791">bu</ta>
            <ta e="T793" id="Seg_7138" s="T792">itiː-GA</ta>
            <ta e="T794" id="Seg_7139" s="T793">emi͡e</ta>
            <ta e="T795" id="Seg_7140" s="T794">anɨ-GI</ta>
            <ta e="T796" id="Seg_7141" s="T795">pastux-LAr</ta>
            <ta e="T797" id="Seg_7142" s="T796">taba-nI</ta>
            <ta e="T798" id="Seg_7143" s="T797">hataː-An</ta>
            <ta e="T799" id="Seg_7144" s="T798">karaj-BAT-LAr</ta>
            <ta e="T800" id="Seg_7145" s="T799">karaj-BAT-LArA</ta>
            <ta e="T801" id="Seg_7146" s="T800">tu͡ok-nI=Ij</ta>
            <ta e="T802" id="Seg_7147" s="T801">itiː-GA</ta>
            <ta e="T803" id="Seg_7148" s="T802">halgɨn-ČIt</ta>
            <ta e="T804" id="Seg_7149" s="T803">taba</ta>
            <ta e="T805" id="Seg_7150" s="T804">bu͡ol-AːččI</ta>
            <ta e="T806" id="Seg_7151" s="T805">halgɨn-ČIt</ta>
            <ta e="T807" id="Seg_7152" s="T806">taba</ta>
            <ta e="T808" id="Seg_7153" s="T807">aːt-tA</ta>
            <ta e="T809" id="Seg_7154" s="T808">tɨ͡al-nI</ta>
            <ta e="T810" id="Seg_7155" s="T809">bat-Ar</ta>
            <ta e="T811" id="Seg_7156" s="T810">bu</ta>
            <ta e="T812" id="Seg_7157" s="T811">tɨ͡al-nI</ta>
            <ta e="T813" id="Seg_7158" s="T812">bat-Ar</ta>
            <ta e="T814" id="Seg_7159" s="T813">taba-nI</ta>
            <ta e="T815" id="Seg_7160" s="T814">itiː</ta>
            <ta e="T816" id="Seg_7161" s="T815">aːs-IAK.[tI]-r</ta>
            <ta e="T817" id="Seg_7162" s="T816">di͡eri</ta>
            <ta e="T818" id="Seg_7163" s="T817">kihi</ta>
            <ta e="T819" id="Seg_7164" s="T818">taba-ttAn</ta>
            <ta e="T820" id="Seg_7165" s="T819">baːj-AːččI</ta>
            <ta e="T821" id="Seg_7166" s="T820">taba</ta>
            <ta e="T822" id="Seg_7167" s="T821">keteː-TAK-TArInA</ta>
            <ta e="T823" id="Seg_7168" s="T822">kihi</ta>
            <ta e="T824" id="Seg_7169" s="T823">küːs-tA</ta>
            <ta e="T825" id="Seg_7170" s="T824">tij-AːččI-tA</ta>
            <ta e="T826" id="Seg_7171" s="T825">hu͡ok</ta>
            <ta e="T827" id="Seg_7172" s="T826">itiː-GA</ta>
            <ta e="T828" id="Seg_7173" s="T827">tɨ͡al-tI</ta>
            <ta e="T830" id="Seg_7174" s="T929">gɨn-BAT-LAr</ta>
            <ta e="T831" id="Seg_7175" s="T830">harsi͡erda</ta>
            <ta e="T832" id="Seg_7176" s="T831">egel-Ar-LArI-GAr</ta>
            <ta e="T833" id="Seg_7177" s="T832">nöŋü͡ö</ta>
            <ta e="T834" id="Seg_7178" s="T833">pastux-GA</ta>
            <ta e="T835" id="Seg_7179" s="T834">tuttar-Ar-LArI-GAr</ta>
            <ta e="T836" id="Seg_7180" s="T835">bihigi</ta>
            <ta e="T837" id="Seg_7181" s="T836">kɨ͡aj-IAK-BIt</ta>
            <ta e="T838" id="Seg_7182" s="T837">hu͡ok-tA</ta>
            <ta e="T839" id="Seg_7183" s="T838">eːt</ta>
            <ta e="T840" id="Seg_7184" s="T839">di͡e-An</ta>
            <ta e="T841" id="Seg_7185" s="T840">kahan</ta>
            <ta e="T842" id="Seg_7186" s="T841">da</ta>
            <ta e="T843" id="Seg_7187" s="T842">tɨ͡al</ta>
            <ta e="T844" id="Seg_7188" s="T843">alɨn</ta>
            <ta e="T845" id="Seg_7189" s="T844">dek</ta>
            <ta e="T846" id="Seg_7190" s="T845">darap-Iː</ta>
            <ta e="T847" id="Seg_7191" s="T846">gɨn-IAK-LArI-n</ta>
            <ta e="T848" id="Seg_7192" s="T847">dʼi͡e-LArA</ta>
            <ta e="T849" id="Seg_7193" s="T848">tɨ͡al</ta>
            <ta e="T850" id="Seg_7194" s="T849">ürüt</ta>
            <ta e="T851" id="Seg_7195" s="T850">bu</ta>
            <ta e="T852" id="Seg_7196" s="T851">bu͡ol-Ar</ta>
            <ta e="T853" id="Seg_7197" s="T852">kördük</ta>
            <ta e="T854" id="Seg_7198" s="T853">taba</ta>
            <ta e="T855" id="Seg_7199" s="T854">oččogo</ta>
            <ta e="T856" id="Seg_7200" s="T855">kɨ͡aj-BAkkA</ta>
            <ta e="T857" id="Seg_7201" s="T856">tɨ͡al</ta>
            <ta e="T858" id="Seg_7202" s="T857">utarɨ</ta>
            <ta e="T859" id="Seg_7203" s="T858">kel-TAK-InA</ta>
            <ta e="T860" id="Seg_7204" s="T859">dʼi͡e-tI-GAr</ta>
            <ta e="T861" id="Seg_7205" s="T860">kep-TAr-TAK-InA</ta>
            <ta e="T862" id="Seg_7206" s="T861">dʼi͡e-LAːgI-LArA</ta>
            <ta e="T863" id="Seg_7207" s="T862">tur-An-LAr</ta>
            <ta e="T864" id="Seg_7208" s="T863">manna</ta>
            <ta e="T865" id="Seg_7209" s="T864">kömölös-An-LAr</ta>
            <ta e="T866" id="Seg_7210" s="T865">oččogo</ta>
            <ta e="T867" id="Seg_7211" s="T866">kɨ͡aj-Ar-LAr</ta>
            <ta e="T868" id="Seg_7212" s="T867">taba-nI</ta>
            <ta e="T869" id="Seg_7213" s="T868">ol</ta>
            <ta e="T870" id="Seg_7214" s="T869">kennine</ta>
            <ta e="T871" id="Seg_7215" s="T870">giniler</ta>
            <ta e="T872" id="Seg_7216" s="T871">haːs-GI-ttAn</ta>
            <ta e="T873" id="Seg_7217" s="T872">itiː</ta>
            <ta e="T874" id="Seg_7218" s="T873">tüs-A</ta>
            <ta e="T875" id="Seg_7219" s="T874">ilik-InA</ta>
            <ta e="T876" id="Seg_7220" s="T875">haŋardɨː</ta>
            <ta e="T877" id="Seg_7221" s="T876">itiː</ta>
            <ta e="T878" id="Seg_7222" s="T877">tüs-Ar-tI-GAr</ta>
            <ta e="T879" id="Seg_7223" s="T878">taba</ta>
            <ta e="T880" id="Seg_7224" s="T879">itiːrgeː-Ar-tI-GAr</ta>
            <ta e="T881" id="Seg_7225" s="T880">buru͡o-GA</ta>
            <ta e="T882" id="Seg_7226" s="T881">taba-nI</ta>
            <ta e="T883" id="Seg_7227" s="T882">ü͡öret-IAK-LArI-n</ta>
            <ta e="T884" id="Seg_7228" s="T883">naːda</ta>
            <ta e="T885" id="Seg_7229" s="T884">ulakan</ta>
            <ta e="T886" id="Seg_7230" s="T885">itiː</ta>
            <ta e="T887" id="Seg_7231" s="T886">tüs-A</ta>
            <ta e="T888" id="Seg_7232" s="T887">ilik-InA</ta>
            <ta e="T889" id="Seg_7233" s="T888">buru͡o-nI</ta>
            <ta e="T890" id="Seg_7234" s="T889">kör-TI-tA</ta>
            <ta e="T891" id="Seg_7235" s="T890">da</ta>
            <ta e="T892" id="Seg_7236" s="T891">töhö</ta>
            <ta e="T893" id="Seg_7237" s="T892">da</ta>
            <ta e="T894" id="Seg_7238" s="T893">itiː-GA</ta>
            <ta e="T896" id="Seg_7239" s="T894">taba</ta>
            <ta e="T897" id="Seg_7240" s="T896">dʼi͡e-LAːgI-LArA</ta>
            <ta e="T898" id="Seg_7241" s="T897">itiː-GA</ta>
            <ta e="T899" id="Seg_7242" s="T898">pastux-LAr</ta>
            <ta e="T900" id="Seg_7243" s="T899">keteː-A</ta>
            <ta e="T901" id="Seg_7244" s="T900">bar-AːrI-LAr</ta>
            <ta e="T902" id="Seg_7245" s="T901">haŋar-IAK</ta>
            <ta e="T903" id="Seg_7246" s="T902">tüs-LAːK-LAr</ta>
            <ta e="T904" id="Seg_7247" s="T903">kaja</ta>
            <ta e="T905" id="Seg_7248" s="T904">itiː</ta>
            <ta e="T906" id="Seg_7249" s="T905">bu͡ol-TAK-InA</ta>
            <ta e="T907" id="Seg_7250" s="T906">dʼe</ta>
            <ta e="T908" id="Seg_7251" s="T907">kečes-An</ta>
            <ta e="T909" id="Seg_7252" s="T908">utuj-I-ŋ</ta>
            <ta e="T910" id="Seg_7253" s="T909">buru͡o-LAː-A-t-Aːr-I-ŋ</ta>
            <ta e="T911" id="Seg_7254" s="T910">otuː</ta>
            <ta e="T912" id="Seg_7255" s="T911">otut-Aːr-I-ŋ</ta>
            <ta e="T913" id="Seg_7256" s="T912">itiː</ta>
            <ta e="T914" id="Seg_7257" s="T913">bu͡ol-TAK-InA</ta>
            <ta e="T915" id="Seg_7258" s="T914">bɨbaːt</ta>
            <ta e="T916" id="Seg_7259" s="T915">kɨ͡aj-IAK-BIt</ta>
            <ta e="T917" id="Seg_7260" s="T916">hu͡ok-tA</ta>
            <ta e="T918" id="Seg_7261" s="T917">kajdak</ta>
            <ta e="T919" id="Seg_7262" s="T918">ere</ta>
            <ta e="T920" id="Seg_7263" s="T919">itiː</ta>
            <ta e="T921" id="Seg_7264" s="T920">bu͡ol-An</ta>
            <ta e="T922" id="Seg_7265" s="T921">er-Ar</ta>
            <ta e="T923" id="Seg_7266" s="T922">ol</ta>
            <ta e="T924" id="Seg_7267" s="T923">kördük</ta>
            <ta e="T925" id="Seg_7268" s="T924">karaj-AːččI</ta>
            <ta e="T926" id="Seg_7269" s="T925">e-TI-BIt</ta>
            <ta e="T927" id="Seg_7270" s="T926">bihigi</ta>
            <ta e="T928" id="Seg_7271" s="T927">taba-nI</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_7272" s="T0">well</ta>
            <ta e="T2" id="Seg_7273" s="T1">1SG.[NOM]</ta>
            <ta e="T3" id="Seg_7274" s="T2">reindeer-AG-PL-DAT/LOC</ta>
            <ta e="T4" id="Seg_7275" s="T3">be.heard-EP-CAUS-CVB.PURP</ta>
            <ta e="T5" id="Seg_7276" s="T4">speak-PRS-1SG</ta>
            <ta e="T6" id="Seg_7277" s="T5">1SG.[NOM]</ta>
            <ta e="T7" id="Seg_7278" s="T6">many-ADVZ</ta>
            <ta e="T8" id="Seg_7279" s="T7">work-PTCP.PST</ta>
            <ta e="T9" id="Seg_7280" s="T8">human.being-1SG</ta>
            <ta e="T10" id="Seg_7281" s="T9">reindeer-DAT/LOC</ta>
            <ta e="T11" id="Seg_7282" s="T10">before</ta>
            <ta e="T12" id="Seg_7283" s="T11">this</ta>
            <ta e="T13" id="Seg_7284" s="T12">reindeer.[NOM]</ta>
            <ta e="T14" id="Seg_7285" s="T13">be.born-PTCP.PRS-3SG-GEN</ta>
            <ta e="T15" id="Seg_7286" s="T14">side-3SG-INSTR</ta>
            <ta e="T16" id="Seg_7287" s="T15">speak-FUT-1SG</ta>
            <ta e="T17" id="Seg_7288" s="T16">now</ta>
            <ta e="T18" id="Seg_7289" s="T17">what.kind.of</ta>
            <ta e="T19" id="Seg_7290" s="T18">INDEF</ta>
            <ta e="T20" id="Seg_7291" s="T19">EMPH</ta>
            <ta e="T21" id="Seg_7292" s="T20">give.birth-PTCP.PRS-3SG-ABL</ta>
            <ta e="T22" id="Seg_7293" s="T21">be-PRS.[3SG]</ta>
            <ta e="T23" id="Seg_7294" s="T22">EMPH</ta>
            <ta e="T24" id="Seg_7295" s="T23">reindeer.[NOM]</ta>
            <ta e="T25" id="Seg_7296" s="T24">and</ta>
            <ta e="T26" id="Seg_7297" s="T25">arise-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T27" id="Seg_7298" s="T26">human.being.[NOM]</ta>
            <ta e="T28" id="Seg_7299" s="T27">well</ta>
            <ta e="T29" id="Seg_7300" s="T28">so</ta>
            <ta e="T30" id="Seg_7301" s="T29">1SG.[NOM]</ta>
            <ta e="T31" id="Seg_7302" s="T30">speak-PRS-1SG</ta>
            <ta e="T32" id="Seg_7303" s="T31">well</ta>
            <ta e="T33" id="Seg_7304" s="T32">like.this</ta>
            <ta e="T34" id="Seg_7305" s="T33">reindeer.[NOM]</ta>
            <ta e="T35" id="Seg_7306" s="T34">give.birth-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T36" id="Seg_7307" s="T35">shepherd-PL-DAT/LOC</ta>
            <ta e="T37" id="Seg_7308" s="T36">be.heard-EP-CAUS-CVB.PURP</ta>
            <ta e="T38" id="Seg_7309" s="T37">1SG.[NOM]</ta>
            <ta e="T39" id="Seg_7310" s="T38">speak-PTCP.PRS-EP-1SG.[NOM]</ta>
            <ta e="T40" id="Seg_7311" s="T39">well</ta>
            <ta e="T41" id="Seg_7312" s="T40">like.this-INTNS</ta>
            <ta e="T42" id="Seg_7313" s="T41">now</ta>
            <ta e="T43" id="Seg_7314" s="T42">this</ta>
            <ta e="T44" id="Seg_7315" s="T43">reindeer-ACC</ta>
            <ta e="T45" id="Seg_7316" s="T44">care.about-PRS-3PL</ta>
            <ta e="T46" id="Seg_7317" s="T45">1SG.[NOM]</ta>
            <ta e="T47" id="Seg_7318" s="T46">before</ta>
            <ta e="T48" id="Seg_7319" s="T47">care.about-CVB.SIM</ta>
            <ta e="T49" id="Seg_7320" s="T48">go-PTCP.PRS</ta>
            <ta e="T50" id="Seg_7321" s="T49">be-TEMP-1SG</ta>
            <ta e="T51" id="Seg_7322" s="T50">reindeer.[NOM]</ta>
            <ta e="T52" id="Seg_7323" s="T51">give.birth-TEMP-3SG</ta>
            <ta e="T53" id="Seg_7324" s="T52">many</ta>
            <ta e="T54" id="Seg_7325" s="T53">reason-PROPR.[NOM]</ta>
            <ta e="T55" id="Seg_7326" s="T54">be-PRS.[3SG]</ta>
            <ta e="T56" id="Seg_7327" s="T55">reindeer.[NOM]</ta>
            <ta e="T57" id="Seg_7328" s="T56">be.born-PTCP.PRS-3SG-ACC</ta>
            <ta e="T58" id="Seg_7329" s="T57">shepherd.[NOM]</ta>
            <ta e="T59" id="Seg_7330" s="T58">be-TEMP-1SG</ta>
            <ta e="T60" id="Seg_7331" s="T59">observe-CVB.SIM</ta>
            <ta e="T61" id="Seg_7332" s="T60">go-CVB.SEQ-1SG</ta>
            <ta e="T62" id="Seg_7333" s="T61">go.around-PTCP.HAB</ta>
            <ta e="T63" id="Seg_7334" s="T62">be-PST1-1SG</ta>
            <ta e="T64" id="Seg_7335" s="T63">reindeer.[NOM]</ta>
            <ta e="T65" id="Seg_7336" s="T64">give.birth-CVB.SEQ</ta>
            <ta e="T66" id="Seg_7337" s="T65">child-3SG-ACC</ta>
            <ta e="T67" id="Seg_7338" s="T66">drop-TEMP-3SG</ta>
            <ta e="T68" id="Seg_7339" s="T67">different-different-ADVZ</ta>
            <ta e="T69" id="Seg_7340" s="T68">be.born-PTCP.PRS</ta>
            <ta e="T70" id="Seg_7341" s="T69">reindeer.[NOM]</ta>
            <ta e="T71" id="Seg_7342" s="T70">some</ta>
            <ta e="T72" id="Seg_7343" s="T71">reindeer.[NOM]</ta>
            <ta e="T73" id="Seg_7344" s="T72">reindeer.calf-3SG.[NOM]</ta>
            <ta e="T74" id="Seg_7345" s="T73">be.born-CVB.SEQ</ta>
            <ta e="T75" id="Seg_7346" s="T74">after</ta>
            <ta e="T76" id="Seg_7347" s="T75">fidget.with.legs-CVB.SIM</ta>
            <ta e="T77" id="Seg_7348" s="T76">lie-HAB.[3SG]</ta>
            <ta e="T78" id="Seg_7349" s="T77">make.it-CVB.SEQ</ta>
            <ta e="T79" id="Seg_7350" s="T78">stand.up-NEG.CVB.SIM</ta>
            <ta e="T80" id="Seg_7351" s="T79">mother-3SG-ACC</ta>
            <ta e="T81" id="Seg_7352" s="T80">see-PTCP.COND-DAT/LOC</ta>
            <ta e="T82" id="Seg_7353" s="T81">fat.[NOM]</ta>
            <ta e="T83" id="Seg_7354" s="T82">very</ta>
            <ta e="T84" id="Seg_7355" s="T83">reindeer.calf-3SG-ACC</ta>
            <ta e="T85" id="Seg_7356" s="T84">see-PTCP.COND-DAT/LOC</ta>
            <ta e="T86" id="Seg_7357" s="T85">big.[NOM]</ta>
            <ta e="T87" id="Seg_7358" s="T86">very</ta>
            <ta e="T88" id="Seg_7359" s="T87">be-PRS.[3SG]</ta>
            <ta e="T89" id="Seg_7360" s="T88">this</ta>
            <ta e="T90" id="Seg_7361" s="T89">fidget.with.legs-CVB.SIM</ta>
            <ta e="T91" id="Seg_7362" s="T90">lie-PTCP.PRS-ACC</ta>
            <ta e="T92" id="Seg_7363" s="T91">turn-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T93" id="Seg_7364" s="T92">need.to</ta>
            <ta e="T94" id="Seg_7365" s="T93">turn-CAUS-CVB.SEQ</ta>
            <ta e="T95" id="Seg_7366" s="T94">try-PTCP.COND-DAT/LOC</ta>
            <ta e="T96" id="Seg_7367" s="T95">what-ACC</ta>
            <ta e="T97" id="Seg_7368" s="T96">make-CVB.SEQ</ta>
            <ta e="T98" id="Seg_7369" s="T97">why</ta>
            <ta e="T99" id="Seg_7370" s="T98">stand.up-NEG.[3SG]</ta>
            <ta e="T100" id="Seg_7371" s="T99">think-CVB.SEQ</ta>
            <ta e="T101" id="Seg_7372" s="T100">this-ACC</ta>
            <ta e="T102" id="Seg_7373" s="T101">massage-PTCP.FUT-DAT/LOC</ta>
            <ta e="T103" id="Seg_7374" s="T102">belly-3SG-ACC</ta>
            <ta e="T104" id="Seg_7375" s="T103">massage-PTCP.FUT-DAT/LOC</ta>
            <ta e="T105" id="Seg_7376" s="T104">reindeer.[NOM]</ta>
            <ta e="T106" id="Seg_7377" s="T105">give.birth-TEMP-3SG</ta>
            <ta e="T107" id="Seg_7378" s="T106">reindeer.calf.[NOM]</ta>
            <ta e="T108" id="Seg_7379" s="T107">own-3SG</ta>
            <ta e="T109" id="Seg_7380" s="T108">navel-PROPR.[NOM]</ta>
            <ta e="T110" id="Seg_7381" s="T109">be-HAB.[3SG]</ta>
            <ta e="T111" id="Seg_7382" s="T110">this</ta>
            <ta e="T112" id="Seg_7383" s="T111">navel-3SG-ACC</ta>
            <ta e="T113" id="Seg_7384" s="T112">some</ta>
            <ta e="T114" id="Seg_7385" s="T113">reindeer.[NOM]</ta>
            <ta e="T115" id="Seg_7386" s="T114">pull-HAB.[3SG]</ta>
            <ta e="T116" id="Seg_7387" s="T115">this</ta>
            <ta e="T117" id="Seg_7388" s="T116">navel-3SG.[NOM]</ta>
            <ta e="T118" id="Seg_7389" s="T117">pull-PASS/REFL-TEMP-3SG</ta>
            <ta e="T119" id="Seg_7390" s="T118">this-INSTR</ta>
            <ta e="T120" id="Seg_7391" s="T119">inside-EP-ABL</ta>
            <ta e="T121" id="Seg_7392" s="T120">straight</ta>
            <ta e="T122" id="Seg_7393" s="T121">hole.[NOM]</ta>
            <ta e="T123" id="Seg_7394" s="T122">become-PRS.[3SG]</ta>
            <ta e="T124" id="Seg_7395" s="T123">that-INSTR</ta>
            <ta e="T125" id="Seg_7396" s="T124">breath-3SG.[NOM]</ta>
            <ta e="T126" id="Seg_7397" s="T125">go.out-PRS.[3SG]</ta>
            <ta e="T127" id="Seg_7398" s="T126">thread-EP-INSTR</ta>
            <ta e="T128" id="Seg_7399" s="T127">strongly</ta>
            <ta e="T129" id="Seg_7400" s="T128">tie-PTCP.COND-DAT/LOC</ta>
            <ta e="T130" id="Seg_7401" s="T129">this</ta>
            <ta e="T131" id="Seg_7402" s="T130">reindeer.[NOM]</ta>
            <ta e="T132" id="Seg_7403" s="T131">this</ta>
            <ta e="T133" id="Seg_7404" s="T132">reindeer.calf.[NOM]</ta>
            <ta e="T134" id="Seg_7405" s="T133">stand.up-CVB.SEQ</ta>
            <ta e="T135" id="Seg_7406" s="T134">come-HAB.[3SG]</ta>
            <ta e="T136" id="Seg_7407" s="T135">that</ta>
            <ta e="T137" id="Seg_7408" s="T136">name-3SG.[NOM]</ta>
            <ta e="T138" id="Seg_7409" s="T137">navel-3SG-ACC</ta>
            <ta e="T139" id="Seg_7410" s="T138">pull-PRS.[3SG]</ta>
            <ta e="T140" id="Seg_7411" s="T139">that.[NOM]</ta>
            <ta e="T141" id="Seg_7412" s="T140">be-PRS.[3SG]</ta>
            <ta e="T142" id="Seg_7413" s="T141">reindeer.[NOM]</ta>
            <ta e="T143" id="Seg_7414" s="T142">own-3SG.[NOM]</ta>
            <ta e="T144" id="Seg_7415" s="T143">reindeer.calf-3SG-GEN</ta>
            <ta e="T145" id="Seg_7416" s="T144">own-3SG.[NOM]</ta>
            <ta e="T146" id="Seg_7417" s="T145">reason-3SG.[NOM]</ta>
            <ta e="T147" id="Seg_7418" s="T146">give.birth-TEMP-3SG</ta>
            <ta e="T148" id="Seg_7419" s="T147">then</ta>
            <ta e="T149" id="Seg_7420" s="T148">one</ta>
            <ta e="T150" id="Seg_7421" s="T149">reason-PROPR.[NOM]</ta>
            <ta e="T151" id="Seg_7422" s="T150">weak.[NOM]</ta>
            <ta e="T152" id="Seg_7423" s="T151">reindeer.[NOM]</ta>
            <ta e="T153" id="Seg_7424" s="T152">be.born-PRS.[3SG]</ta>
            <ta e="T154" id="Seg_7425" s="T153">agile</ta>
            <ta e="T155" id="Seg_7426" s="T154">mother-3SG.[NOM]</ta>
            <ta e="T156" id="Seg_7427" s="T155">this</ta>
            <ta e="T157" id="Seg_7428" s="T156">reindeer.calf-3SG.[NOM]</ta>
            <ta e="T158" id="Seg_7429" s="T157">be.born-TEMP-3SG</ta>
            <ta e="T159" id="Seg_7430" s="T158">dyapchu.[NOM]</ta>
            <ta e="T160" id="Seg_7431" s="T159">say-CVB.SEQ</ta>
            <ta e="T161" id="Seg_7432" s="T160">name-VBZ-HAB.[3SG]</ta>
            <ta e="T162" id="Seg_7433" s="T161">also</ta>
            <ta e="T163" id="Seg_7434" s="T162">reindeer.calf-3SG.[NOM]</ta>
            <ta e="T164" id="Seg_7435" s="T163">stand-PTCP.HAB-3SG</ta>
            <ta e="T165" id="Seg_7436" s="T164">NEG.[3SG]</ta>
            <ta e="T166" id="Seg_7437" s="T165">this</ta>
            <ta e="T167" id="Seg_7438" s="T166">reindeer.calf-ACC</ta>
            <ta e="T168" id="Seg_7439" s="T167">turn-CAUS-CVB.SEQ</ta>
            <ta e="T169" id="Seg_7440" s="T168">try-PTCP.COND-DAT/LOC</ta>
            <ta e="T170" id="Seg_7441" s="T169">at.all</ta>
            <ta e="T171" id="Seg_7442" s="T170">weak.[NOM]</ta>
            <ta e="T172" id="Seg_7443" s="T171">very</ta>
            <ta e="T173" id="Seg_7444" s="T172">be-PRS.[3SG]</ta>
            <ta e="T174" id="Seg_7445" s="T173">this</ta>
            <ta e="T175" id="Seg_7446" s="T174">reindeer.calf.[NOM]</ta>
            <ta e="T176" id="Seg_7447" s="T175">fidget.with.legs-CVB.SIM</ta>
            <ta e="T177" id="Seg_7448" s="T176">only</ta>
            <ta e="T178" id="Seg_7449" s="T177">lie-HAB.[3SG]</ta>
            <ta e="T179" id="Seg_7450" s="T178">this</ta>
            <ta e="T180" id="Seg_7451" s="T179">reindeer.calf-ACC</ta>
            <ta e="T181" id="Seg_7452" s="T180">on.the.back</ta>
            <ta e="T182" id="Seg_7453" s="T181">hold-CVB.SEQ</ta>
            <ta e="T183" id="Seg_7454" s="T182">after</ta>
            <ta e="T184" id="Seg_7455" s="T183">breast-3SG-ACC</ta>
            <ta e="T185" id="Seg_7456" s="T184">stroke-PTCP.COND-DAT/LOC</ta>
            <ta e="T186" id="Seg_7457" s="T185">then</ta>
            <ta e="T187" id="Seg_7458" s="T186">this</ta>
            <ta e="T188" id="Seg_7459" s="T187">breast-3SG.[NOM]</ta>
            <ta e="T189" id="Seg_7460" s="T188">dyapchu.[NOM]</ta>
            <ta e="T190" id="Seg_7461" s="T189">own-3SG.[NOM]</ta>
            <ta e="T191" id="Seg_7462" s="T190">when</ta>
            <ta e="T192" id="Seg_7463" s="T191">INDEF</ta>
            <ta e="T193" id="Seg_7464" s="T192">reindeer.calf.[NOM]</ta>
            <ta e="T194" id="Seg_7465" s="T193">be.born-TEMP-3SG</ta>
            <ta e="T195" id="Seg_7466" s="T194">bone-3SG.[NOM]</ta>
            <ta e="T196" id="Seg_7467" s="T195">soft.[NOM]</ta>
            <ta e="T197" id="Seg_7468" s="T196">be-PRS.[3SG]</ta>
            <ta e="T198" id="Seg_7469" s="T197">this-ACC</ta>
            <ta e="T199" id="Seg_7470" s="T198">breast-3SG-ACC</ta>
            <ta e="T200" id="Seg_7471" s="T199">stroke-PTCP.COND-DAT/LOC</ta>
            <ta e="T201" id="Seg_7472" s="T200">stand.up-CVB.SIM-stand.up-CVB.SIM</ta>
            <ta e="T202" id="Seg_7473" s="T201">fall-CVB.SIM</ta>
            <ta e="T203" id="Seg_7474" s="T202">go-HAB.[3SG]</ta>
            <ta e="T204" id="Seg_7475" s="T203">breath-TEMP-3SG</ta>
            <ta e="T205" id="Seg_7476" s="T204">this-ACC</ta>
            <ta e="T206" id="Seg_7477" s="T205">mother-3SG-ACC</ta>
            <ta e="T207" id="Seg_7478" s="T206">grab-CVB.SEQ</ta>
            <ta e="T208" id="Seg_7479" s="T207">after</ta>
            <ta e="T209" id="Seg_7480" s="T208">mother-3SG-ACC</ta>
            <ta e="T210" id="Seg_7481" s="T209">suck-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T211" id="Seg_7482" s="T210">need.to</ta>
            <ta e="T212" id="Seg_7483" s="T211">this</ta>
            <ta e="T213" id="Seg_7484" s="T212">reindeer.calf.[NOM]</ta>
            <ta e="T214" id="Seg_7485" s="T213">breast-3SG-ACC</ta>
            <ta e="T215" id="Seg_7486" s="T214">who-ACC</ta>
            <ta e="T216" id="Seg_7487" s="T215">sweep-PTCP.PST-3SG-ACC</ta>
            <ta e="T217" id="Seg_7488" s="T216">after</ta>
            <ta e="T218" id="Seg_7489" s="T217">make.it-CVB.SEQ</ta>
            <ta e="T219" id="Seg_7490" s="T218">suck-PTCP.HAB-3SG</ta>
            <ta e="T220" id="Seg_7491" s="T219">NEG.[3SG]</ta>
            <ta e="T221" id="Seg_7492" s="T220">this-ACC</ta>
            <ta e="T222" id="Seg_7493" s="T221">mother-3SG.[NOM]</ta>
            <ta e="T223" id="Seg_7494" s="T222">bosoms-3SG.[NOM]</ta>
            <ta e="T224" id="Seg_7495" s="T223">tree.resin-PROPR.[NOM]</ta>
            <ta e="T225" id="Seg_7496" s="T224">be-PST1-3SG</ta>
            <ta e="T226" id="Seg_7497" s="T225">thick.and.short.[NOM]</ta>
            <ta e="T227" id="Seg_7498" s="T226">this</ta>
            <ta e="T228" id="Seg_7499" s="T227">tree.resin-ACC</ta>
            <ta e="T229" id="Seg_7500" s="T228">press-HAB.[3SG]</ta>
            <ta e="T230" id="Seg_7501" s="T229">human.being.[NOM]</ta>
            <ta e="T231" id="Seg_7502" s="T230">press-CVB.SEQ</ta>
            <ta e="T232" id="Seg_7503" s="T231">after</ta>
            <ta e="T233" id="Seg_7504" s="T232">liquid</ta>
            <ta e="T234" id="Seg_7505" s="T233">milk-3SG.[NOM]</ta>
            <ta e="T235" id="Seg_7506" s="T234">come-TEMP-3SG</ta>
            <ta e="T236" id="Seg_7507" s="T235">then</ta>
            <ta e="T237" id="Seg_7508" s="T236">suck-CAUS-HAB.[3SG]</ta>
            <ta e="T238" id="Seg_7509" s="T237">then</ta>
            <ta e="T239" id="Seg_7510" s="T238">suck-CAUS-TEMP-3SG</ta>
            <ta e="T240" id="Seg_7511" s="T239">this</ta>
            <ta e="T241" id="Seg_7512" s="T240">reindeer.calf.[NOM]</ta>
            <ta e="T242" id="Seg_7513" s="T241">human.being.[NOM]</ta>
            <ta e="T243" id="Seg_7514" s="T242">become-PRS.[3SG]</ta>
            <ta e="T244" id="Seg_7515" s="T243">shepherd-PL.[NOM]</ta>
            <ta e="T245" id="Seg_7516" s="T244">nomadize-PRS-3PL</ta>
            <ta e="T246" id="Seg_7517" s="T245">many</ta>
            <ta e="T247" id="Seg_7518" s="T246">reindeer.[NOM]</ta>
            <ta e="T248" id="Seg_7519" s="T247">give.birth-TEMP-3SG</ta>
            <ta e="T249" id="Seg_7520" s="T248">when</ta>
            <ta e="T250" id="Seg_7521" s="T249">NEG</ta>
            <ta e="T251" id="Seg_7522" s="T250">give.birth-CVB.SIM=not.yet</ta>
            <ta e="T252" id="Seg_7523" s="T251">reindeer-ACC</ta>
            <ta e="T253" id="Seg_7524" s="T252">front.[NOM]</ta>
            <ta e="T254" id="Seg_7525" s="T253">to</ta>
            <ta e="T255" id="Seg_7526" s="T254">take.away-HAB-3PL</ta>
            <ta e="T256" id="Seg_7527" s="T255">give.birth-PTCP.PST</ta>
            <ta e="T257" id="Seg_7528" s="T256">reindeer-ACC</ta>
            <ta e="T258" id="Seg_7529" s="T257">reindeer.calf-3SG.[NOM]</ta>
            <ta e="T259" id="Seg_7530" s="T258">grow-CVB.SIM</ta>
            <ta e="T260" id="Seg_7531" s="T259">fall-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T261" id="Seg_7532" s="T260">until</ta>
            <ta e="T262" id="Seg_7533" s="T261">walk-NEG.PTCP-3SG-ACC</ta>
            <ta e="T263" id="Seg_7534" s="T262">because.of</ta>
            <ta e="T264" id="Seg_7535" s="T263">back.[NOM]</ta>
            <ta e="T265" id="Seg_7536" s="T264">to</ta>
            <ta e="T266" id="Seg_7537" s="T265">let-PRS-3PL</ta>
            <ta e="T267" id="Seg_7538" s="T266">this</ta>
            <ta e="T268" id="Seg_7539" s="T267">let-NMNZ-3PL-DAT/LOC</ta>
            <ta e="T269" id="Seg_7540" s="T268">this</ta>
            <ta e="T270" id="Seg_7541" s="T269">well</ta>
            <ta e="T271" id="Seg_7542" s="T270">ten-ABL</ta>
            <ta e="T272" id="Seg_7543" s="T271">over</ta>
            <ta e="T273" id="Seg_7544" s="T272">twenty-APRX</ta>
            <ta e="T274" id="Seg_7545" s="T273">reindeer.[NOM]</ta>
            <ta e="T275" id="Seg_7546" s="T274">give.birth-PTCP.PST-3SG-ACC</ta>
            <ta e="T276" id="Seg_7547" s="T275">after</ta>
            <ta e="T277" id="Seg_7548" s="T276">then</ta>
            <ta e="T278" id="Seg_7549" s="T277">this</ta>
            <ta e="T279" id="Seg_7550" s="T278">give.birth-CVB.SIM=not.yet</ta>
            <ta e="T280" id="Seg_7551" s="T279">reindeer-ACC</ta>
            <ta e="T281" id="Seg_7552" s="T280">front.[NOM]</ta>
            <ta e="T282" id="Seg_7553" s="T281">to</ta>
            <ta e="T283" id="Seg_7554" s="T282">take.away-PTCP.FUT-DAT/LOC</ta>
            <ta e="T284" id="Seg_7555" s="T283">need.to</ta>
            <ta e="T285" id="Seg_7556" s="T284">this</ta>
            <ta e="T286" id="Seg_7557" s="T285">reindeer-ACC</ta>
            <ta e="T287" id="Seg_7558" s="T286">take.away-PTCP.PRS-DAT/LOC</ta>
            <ta e="T288" id="Seg_7559" s="T287">some</ta>
            <ta e="T289" id="Seg_7560" s="T288">shepherd.[NOM]</ta>
            <ta e="T290" id="Seg_7561" s="T289">this</ta>
            <ta e="T291" id="Seg_7562" s="T290">give.birth-PTCP.PST</ta>
            <ta e="T292" id="Seg_7563" s="T291">calf-PROPR-DAT/LOC</ta>
            <ta e="T293" id="Seg_7564" s="T292">stay-PRS.[3SG]</ta>
            <ta e="T294" id="Seg_7565" s="T293">some</ta>
            <ta e="T295" id="Seg_7566" s="T294">shepherd.[NOM]</ta>
            <ta e="T296" id="Seg_7567" s="T295">this</ta>
            <ta e="T297" id="Seg_7568" s="T296">give.birth</ta>
            <ta e="T298" id="Seg_7569" s="T297">give.birth-CVB.SIM=not.yet</ta>
            <ta e="T299" id="Seg_7570" s="T298">reindeer-ACC</ta>
            <ta e="T300" id="Seg_7571" s="T299">take.away-PRS.[3SG]</ta>
            <ta e="T301" id="Seg_7572" s="T300">this</ta>
            <ta e="T302" id="Seg_7573" s="T301">take.away-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T303" id="Seg_7574" s="T302">eat-EP-CAUS-CVB.SEQ</ta>
            <ta e="T304" id="Seg_7575" s="T303">deception-VBZ-CVB.SEQ</ta>
            <ta e="T305" id="Seg_7576" s="T304">protected</ta>
            <ta e="T306" id="Seg_7577" s="T305">place-DAT/LOC</ta>
            <ta e="T307" id="Seg_7578" s="T306">go.in-CAUS-CVB.SEQ</ta>
            <ta e="T308" id="Seg_7579" s="T307">herd-3SG.[NOM]</ta>
            <ta e="T309" id="Seg_7580" s="T308">go-PTCP.PRS-3SG-ACC</ta>
            <ta e="T310" id="Seg_7581" s="T309">show-NEG.CVB.SIM</ta>
            <ta e="T311" id="Seg_7582" s="T310">this</ta>
            <ta e="T312" id="Seg_7583" s="T311">calf-PROPR</ta>
            <ta e="T314" id="Seg_7584" s="T312">female.reindeer-ACC</ta>
            <ta e="T315" id="Seg_7585" s="T314">when</ta>
            <ta e="T316" id="Seg_7586" s="T315">INDEF</ta>
            <ta e="T317" id="Seg_7587" s="T316">reindeer.[NOM]</ta>
            <ta e="T318" id="Seg_7588" s="T317">regret-RECP/COLL-CVB.SEQ</ta>
            <ta e="T319" id="Seg_7589" s="T318">reindeer.calf-3SG-ACC</ta>
            <ta e="T320" id="Seg_7590" s="T319">let-HAB.[3SG]</ta>
            <ta e="T321" id="Seg_7591" s="T320">follow-CVB.SEQ</ta>
            <ta e="T322" id="Seg_7592" s="T321">herd-3SG-DAT/LOC</ta>
            <ta e="T323" id="Seg_7593" s="T322">choose-EP-RECP/COLL-CVB.SEQ</ta>
            <ta e="T324" id="Seg_7594" s="T323">that-ACC</ta>
            <ta e="T325" id="Seg_7595" s="T324">eat-EP-CAUS-CVB.SIM</ta>
            <ta e="T326" id="Seg_7596" s="T325">go-CVB.SEQ</ta>
            <ta e="T327" id="Seg_7597" s="T326">eat.ones.fill-PTCP.PST-3SG-ACC</ta>
            <ta e="T328" id="Seg_7598" s="T327">ruminate-CVB.SIM</ta>
            <ta e="T329" id="Seg_7599" s="T328">lie-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T330" id="Seg_7600" s="T329">give.birth-CVB.SIM=not.yet</ta>
            <ta e="T331" id="Seg_7601" s="T330">reindeer-3SG-ACC</ta>
            <ta e="T332" id="Seg_7602" s="T331">front.[NOM]</ta>
            <ta e="T333" id="Seg_7603" s="T332">to</ta>
            <ta e="T334" id="Seg_7604" s="T333">convey-CVB.SEQ</ta>
            <ta e="T335" id="Seg_7605" s="T334">take.away-PRS.[3SG]</ta>
            <ta e="T336" id="Seg_7606" s="T335">that</ta>
            <ta e="T337" id="Seg_7607" s="T336">convey-TEMP-3PL</ta>
            <ta e="T338" id="Seg_7608" s="T337">some</ta>
            <ta e="T339" id="Seg_7609" s="T338">shepherd-PL.[NOM]</ta>
            <ta e="T340" id="Seg_7610" s="T339">very</ta>
            <ta e="T341" id="Seg_7611" s="T340">distant.[NOM]</ta>
            <ta e="T342" id="Seg_7612" s="T341">go-HAB-3PL</ta>
            <ta e="T343" id="Seg_7613" s="T342">this</ta>
            <ta e="T344" id="Seg_7614" s="T343">distant.[NOM]</ta>
            <ta e="T345" id="Seg_7615" s="T344">go-TEMP-3PL</ta>
            <ta e="T346" id="Seg_7616" s="T345">this</ta>
            <ta e="T347" id="Seg_7617" s="T346">reindeer.calf.[NOM]</ta>
            <ta e="T348" id="Seg_7618" s="T347">next.morning-3SG-ACC</ta>
            <ta e="T349" id="Seg_7619" s="T348">that</ta>
            <ta e="T350" id="Seg_7620" s="T349">give.birth-CVB.SIM=not.yet</ta>
            <ta e="T351" id="Seg_7621" s="T350">reindeer-PROPR-DAT/LOC</ta>
            <ta e="T352" id="Seg_7622" s="T351">back-ADJZ-3SG-ACC</ta>
            <ta e="T353" id="Seg_7623" s="T352">shepherd-3PL-ACC</ta>
            <ta e="T354" id="Seg_7624" s="T353">that</ta>
            <ta e="T355" id="Seg_7625" s="T354">this-ACC</ta>
            <ta e="T356" id="Seg_7626" s="T355">give.birth-PTCP.PST</ta>
            <ta e="T357" id="Seg_7627" s="T356">female.reindeer-PROPR-3PL-ACC</ta>
            <ta e="T358" id="Seg_7628" s="T357">shepherd-3SG.[NOM]</ta>
            <ta e="T359" id="Seg_7629" s="T358">front.[NOM]</ta>
            <ta e="T360" id="Seg_7630" s="T359">to</ta>
            <ta e="T361" id="Seg_7631" s="T360">shepherd-PL-DAT/LOC</ta>
            <ta e="T362" id="Seg_7632" s="T361">take.away-CVB.PURP-3PL</ta>
            <ta e="T363" id="Seg_7633" s="T362">nomadize-PTCP.PRS-3PL-DAT/LOC</ta>
            <ta e="T364" id="Seg_7634" s="T363">reindeer.[NOM]</ta>
            <ta e="T365" id="Seg_7635" s="T364">make.it-CVB.SEQ</ta>
            <ta e="T366" id="Seg_7636" s="T365">reach-PTCP.HAB-3SG</ta>
            <ta e="T367" id="Seg_7637" s="T366">NEG.[3SG]</ta>
            <ta e="T368" id="Seg_7638" s="T367">this.[NOM]</ta>
            <ta e="T369" id="Seg_7639" s="T368">that.[NOM]</ta>
            <ta e="T370" id="Seg_7640" s="T369">because.of</ta>
            <ta e="T371" id="Seg_7641" s="T370">close.[NOM]</ta>
            <ta e="T372" id="Seg_7642" s="T371">fall-PTCP.FUT-DAT/LOC</ta>
            <ta e="T373" id="Seg_7643" s="T372">see-CVB.SEQ</ta>
            <ta e="T374" id="Seg_7644" s="T373">after</ta>
            <ta e="T375" id="Seg_7645" s="T374">then</ta>
            <ta e="T376" id="Seg_7646" s="T375">though</ta>
            <ta e="T377" id="Seg_7647" s="T376">this</ta>
            <ta e="T378" id="Seg_7648" s="T377">shepherd-PL.[NOM]</ta>
            <ta e="T379" id="Seg_7649" s="T378">when</ta>
            <ta e="T380" id="Seg_7650" s="T379">and</ta>
            <ta e="T381" id="Seg_7651" s="T380">protection-PROPR</ta>
            <ta e="T382" id="Seg_7652" s="T381">place-EP-INSTR</ta>
            <ta e="T383" id="Seg_7653" s="T382">drive-PRS-3PL</ta>
            <ta e="T384" id="Seg_7654" s="T383">route.[NOM]</ta>
            <ta e="T385" id="Seg_7655" s="T384">see-PRS-3PL</ta>
            <ta e="T386" id="Seg_7656" s="T385">reindeer.[NOM]</ta>
            <ta e="T387" id="Seg_7657" s="T386">give.birth-EP-CAUS-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T388" id="Seg_7658" s="T387">protection-PROPR</ta>
            <ta e="T389" id="Seg_7659" s="T388">earth-EP-INSTR</ta>
            <ta e="T390" id="Seg_7660" s="T389">forest-PROPR</ta>
            <ta e="T391" id="Seg_7661" s="T390">place-EP-INSTR</ta>
            <ta e="T392" id="Seg_7662" s="T391">mountain-PL-PROPR</ta>
            <ta e="T393" id="Seg_7663" s="T392">place-PL-EP-INSTR</ta>
            <ta e="T394" id="Seg_7664" s="T393">river-PL-PROPR</ta>
            <ta e="T395" id="Seg_7665" s="T394">place-PL-EP-INSTR</ta>
            <ta e="T396" id="Seg_7666" s="T395">this</ta>
            <ta e="T397" id="Seg_7667" s="T396">big</ta>
            <ta e="T398" id="Seg_7668" s="T397">snowdrift.[NOM]</ta>
            <ta e="T399" id="Seg_7669" s="T398">come.to.an.end-HAB.[3SG]</ta>
            <ta e="T400" id="Seg_7670" s="T399">this</ta>
            <ta e="T401" id="Seg_7671" s="T400">being.blown.away-TEMP-3SG</ta>
            <ta e="T402" id="Seg_7672" s="T401">in.spring</ta>
            <ta e="T403" id="Seg_7673" s="T402">reindeer.[NOM]</ta>
            <ta e="T404" id="Seg_7674" s="T403">give.birth-PTCP.PRS-3SG-ACC</ta>
            <ta e="T405" id="Seg_7675" s="T404">when</ta>
            <ta e="T406" id="Seg_7676" s="T405">day.[NOM]</ta>
            <ta e="T407" id="Seg_7677" s="T406">get.light-TEMP-3SG</ta>
            <ta e="T408" id="Seg_7678" s="T407">in.winter</ta>
            <ta e="T409" id="Seg_7679" s="T408">being.blown.away-EP-PTCP.PST</ta>
            <ta e="T410" id="Seg_7680" s="T409">snowdrift.[NOM]</ta>
            <ta e="T411" id="Seg_7681" s="T410">open-HAB.[3SG]</ta>
            <ta e="T412" id="Seg_7682" s="T411">that</ta>
            <ta e="T413" id="Seg_7683" s="T412">name-3SG.[NOM]</ta>
            <ta e="T414" id="Seg_7684" s="T413">precipice.[NOM]</ta>
            <ta e="T415" id="Seg_7685" s="T414">be-PRS.[3SG]</ta>
            <ta e="T416" id="Seg_7686" s="T415">reindeer.[NOM]</ta>
            <ta e="T417" id="Seg_7687" s="T416">message-POSS</ta>
            <ta e="T418" id="Seg_7688" s="T417">NEG</ta>
            <ta e="T419" id="Seg_7689" s="T418">lose-CVB.SIM</ta>
            <ta e="T420" id="Seg_7690" s="T419">that</ta>
            <ta e="T421" id="Seg_7691" s="T420">precipice.[NOM]</ta>
            <ta e="T422" id="Seg_7692" s="T421">inside-3SG-DAT/LOC</ta>
            <ta e="T423" id="Seg_7693" s="T422">fall-PTCP.PST</ta>
            <ta e="T424" id="Seg_7694" s="T423">reindeer-ACC</ta>
            <ta e="T425" id="Seg_7695" s="T424">this</ta>
            <ta e="T426" id="Seg_7696" s="T425">shepherd.[NOM]</ta>
            <ta e="T427" id="Seg_7697" s="T426">guard-CVB.SIM</ta>
            <ta e="T428" id="Seg_7698" s="T427">go-CVB.SEQ</ta>
            <ta e="T429" id="Seg_7699" s="T428">notice-NEG.[3SG]</ta>
            <ta e="T430" id="Seg_7700" s="T429">how.much</ta>
            <ta e="T431" id="Seg_7701" s="T430">INDEF</ta>
            <ta e="T432" id="Seg_7702" s="T431">reindeer.[NOM]</ta>
            <ta e="T433" id="Seg_7703" s="T432">fall-PTCP.PST-3SG-ACC</ta>
            <ta e="T434" id="Seg_7704" s="T433">EMPH-then</ta>
            <ta e="T435" id="Seg_7705" s="T434">miss-NEG.[3SG]</ta>
            <ta e="T436" id="Seg_7706" s="T435">reindeer.[NOM]</ta>
            <ta e="T437" id="Seg_7707" s="T436">message-POSS</ta>
            <ta e="T438" id="Seg_7708" s="T437">NEG</ta>
            <ta e="T439" id="Seg_7709" s="T438">get.lost-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T440" id="Seg_7710" s="T439">that</ta>
            <ta e="T441" id="Seg_7711" s="T440">so</ta>
            <ta e="T442" id="Seg_7712" s="T441">brigade.leader-PL.[NOM]</ta>
            <ta e="T443" id="Seg_7713" s="T442">maybe</ta>
            <ta e="T444" id="Seg_7714" s="T443">shepherd-PL.[NOM]</ta>
            <ta e="T445" id="Seg_7715" s="T444">maybe</ta>
            <ta e="T446" id="Seg_7716" s="T445">in.spring</ta>
            <ta e="T447" id="Seg_7717" s="T446">reindeer.[NOM]</ta>
            <ta e="T448" id="Seg_7718" s="T447">give.birth-PTCP.PRS-3SG-ACC</ta>
            <ta e="T449" id="Seg_7719" s="T448">when</ta>
            <ta e="T451" id="Seg_7720" s="T450">snow.[NOM]</ta>
            <ta e="T452" id="Seg_7721" s="T451">melt-PTCP.PRS-3SG-ACC</ta>
            <ta e="T453" id="Seg_7722" s="T452">when</ta>
            <ta e="T454" id="Seg_7723" s="T453">good-ADVZ</ta>
            <ta e="T455" id="Seg_7724" s="T454">place-3PL-ACC</ta>
            <ta e="T456" id="Seg_7725" s="T455">see-PTCP.FUT-3PL-ACC</ta>
            <ta e="T457" id="Seg_7726" s="T456">need.to</ta>
            <ta e="T458" id="Seg_7727" s="T457">snowdrift.[NOM]</ta>
            <ta e="T459" id="Seg_7728" s="T458">tear-EP-PTCP.PST-3SG.[NOM]</ta>
            <ta e="T460" id="Seg_7729" s="T459">snowstorm.[NOM]</ta>
            <ta e="T461" id="Seg_7730" s="T460">be-TEMP-3SG</ta>
            <ta e="T462" id="Seg_7731" s="T461">flurry-EP-PASS/REFL-CVB.SEQ</ta>
            <ta e="T463" id="Seg_7732" s="T462">stay-HAB.[3SG]</ta>
            <ta e="T464" id="Seg_7733" s="T463">that</ta>
            <ta e="T465" id="Seg_7734" s="T464">make-CVB.SEQ</ta>
            <ta e="T466" id="Seg_7735" s="T465">after</ta>
            <ta e="T467" id="Seg_7736" s="T466">that</ta>
            <ta e="T468" id="Seg_7737" s="T467">flurry-EP-PASS/REFL-EP-PTCP.PST-3SG-ACC</ta>
            <ta e="T469" id="Seg_7738" s="T468">after</ta>
            <ta e="T470" id="Seg_7739" s="T469">that-ACC</ta>
            <ta e="T471" id="Seg_7740" s="T470">reindeer.[NOM]</ta>
            <ta e="T472" id="Seg_7741" s="T471">see-NEG.[3SG]</ta>
            <ta e="T473" id="Seg_7742" s="T472">EMPH-then</ta>
            <ta e="T474" id="Seg_7743" s="T473">fall-PRS.[3SG]</ta>
            <ta e="T475" id="Seg_7744" s="T474">that</ta>
            <ta e="T476" id="Seg_7745" s="T475">fall-TEMP-3SG</ta>
            <ta e="T477" id="Seg_7746" s="T476">five-four</ta>
            <ta e="T478" id="Seg_7747" s="T477">meter-PROPR</ta>
            <ta e="T479" id="Seg_7748" s="T478">depth-PROPR.[NOM]</ta>
            <ta e="T480" id="Seg_7749" s="T479">be-HAB.[3SG]</ta>
            <ta e="T481" id="Seg_7750" s="T480">this</ta>
            <ta e="T482" id="Seg_7751" s="T481">precipice.[NOM]</ta>
            <ta e="T483" id="Seg_7752" s="T482">snow.[NOM]</ta>
            <ta e="T484" id="Seg_7753" s="T483">being.blown.away-EP-PST2.[3SG]</ta>
            <ta e="T485" id="Seg_7754" s="T484">that.[NOM]</ta>
            <ta e="T486" id="Seg_7755" s="T485">because.of</ta>
            <ta e="T487" id="Seg_7756" s="T486">such-ACC.[NOM]</ta>
            <ta e="T488" id="Seg_7757" s="T487">find-NEG.CVB.SIM</ta>
            <ta e="T489" id="Seg_7758" s="T488">reindeer.[NOM]</ta>
            <ta e="T490" id="Seg_7759" s="T489">message-POSS</ta>
            <ta e="T491" id="Seg_7760" s="T490">NEG</ta>
            <ta e="T492" id="Seg_7761" s="T491">get.lost-PRS.[3SG]</ta>
            <ta e="T493" id="Seg_7762" s="T492">that.[NOM]</ta>
            <ta e="T494" id="Seg_7763" s="T493">then</ta>
            <ta e="T495" id="Seg_7764" s="T494">river-PROPR</ta>
            <ta e="T496" id="Seg_7765" s="T495">place-PL-DAT/LOC</ta>
            <ta e="T497" id="Seg_7766" s="T496">though</ta>
            <ta e="T498" id="Seg_7767" s="T497">thawed.patch-VBZ-TEMP-3SG</ta>
            <ta e="T499" id="Seg_7768" s="T498">when</ta>
            <ta e="T500" id="Seg_7769" s="T499">NEG</ta>
            <ta e="T501" id="Seg_7770" s="T500">river-PROPR</ta>
            <ta e="T502" id="Seg_7771" s="T501">earth-EP-INSTR</ta>
            <ta e="T503" id="Seg_7772" s="T502">be-PRS.[3SG]</ta>
            <ta e="T504" id="Seg_7773" s="T503">route.[NOM]</ta>
            <ta e="T505" id="Seg_7774" s="T504">this</ta>
            <ta e="T506" id="Seg_7775" s="T505">snow-ACC</ta>
            <ta e="T507" id="Seg_7776" s="T506">with</ta>
            <ta e="T508" id="Seg_7777" s="T507">mountain.[NOM]</ta>
            <ta e="T509" id="Seg_7778" s="T508">earth.[NOM]</ta>
            <ta e="T510" id="Seg_7779" s="T509">space.in.between-3SG.[NOM]</ta>
            <ta e="T511" id="Seg_7780" s="T510">open-HAB.[3SG]</ta>
            <ta e="T512" id="Seg_7781" s="T511">also</ta>
            <ta e="T513" id="Seg_7782" s="T512">so</ta>
            <ta e="T514" id="Seg_7783" s="T513">also</ta>
            <ta e="T515" id="Seg_7784" s="T514">fall-HAB.[3SG]</ta>
            <ta e="T516" id="Seg_7785" s="T515">reindeer.calf.[NOM]</ta>
            <ta e="T517" id="Seg_7786" s="T516">maybe</ta>
            <ta e="T518" id="Seg_7787" s="T517">big</ta>
            <ta e="T519" id="Seg_7788" s="T518">reindeer.[NOM]</ta>
            <ta e="T520" id="Seg_7789" s="T519">maybe</ta>
            <ta e="T521" id="Seg_7790" s="T520">then</ta>
            <ta e="T522" id="Seg_7791" s="T521">this</ta>
            <ta e="T523" id="Seg_7792" s="T522">shepherd-PL.[NOM]</ta>
            <ta e="T524" id="Seg_7793" s="T523">spring</ta>
            <ta e="T525" id="Seg_7794" s="T524">summer-3PL.[NOM]</ta>
            <ta e="T526" id="Seg_7795" s="T525">come.closer-TEMP-3SG</ta>
            <ta e="T527" id="Seg_7796" s="T526">spring-ADJZ-3PL-DAT/LOC</ta>
            <ta e="T528" id="Seg_7797" s="T527">riding.reindeer-3PL-ACC</ta>
            <ta e="T529" id="Seg_7798" s="T528">can-CVB.SEQ</ta>
            <ta e="T530" id="Seg_7799" s="T529">care.about-MED-NEG-3PL</ta>
            <ta e="T531" id="Seg_7800" s="T530">horse.fly-3SG-ACC</ta>
            <ta e="T532" id="Seg_7801" s="T531">tear.off-NEG-3PL</ta>
            <ta e="T533" id="Seg_7802" s="T532">spring.[NOM]</ta>
            <ta e="T534" id="Seg_7803" s="T533">horse.fly-3SG-ACC</ta>
            <ta e="T535" id="Seg_7804" s="T534">tear.off-TEMP-3PL</ta>
            <ta e="T536" id="Seg_7805" s="T535">that</ta>
            <ta e="T537" id="Seg_7806" s="T536">horse.fly-3SG-ACC</ta>
            <ta e="T538" id="Seg_7807" s="T537">tear.off-TEMP-3PL</ta>
            <ta e="T539" id="Seg_7808" s="T538">this</ta>
            <ta e="T540" id="Seg_7809" s="T539">horse.fly.[NOM]</ta>
            <ta e="T541" id="Seg_7810" s="T540">get.better-PRS.[3SG]</ta>
            <ta e="T542" id="Seg_7811" s="T541">self-3SG.[NOM]</ta>
            <ta e="T543" id="Seg_7812" s="T542">horse.fly.[NOM]</ta>
            <ta e="T544" id="Seg_7813" s="T543">in.spring</ta>
            <ta e="T545" id="Seg_7814" s="T544">grow-CVB.SEQ</ta>
            <ta e="T546" id="Seg_7815" s="T545">go.out-TEMP-3SG</ta>
            <ta e="T547" id="Seg_7816" s="T546">skin-3SG-GEN</ta>
            <ta e="T548" id="Seg_7817" s="T547">inside-3SG-ABL</ta>
            <ta e="T549" id="Seg_7818" s="T548">self-3SG.[NOM]</ta>
            <ta e="T550" id="Seg_7819" s="T549">separate-CVB.SEQ</ta>
            <ta e="T551" id="Seg_7820" s="T550">fall-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T552" id="Seg_7821" s="T551">until</ta>
            <ta e="T553" id="Seg_7822" s="T552">go-TEMP-3SG</ta>
            <ta e="T554" id="Seg_7823" s="T553">reindeer-DAT/LOC</ta>
            <ta e="T555" id="Seg_7824" s="T554">and</ta>
            <ta e="T556" id="Seg_7825" s="T555">bad.[NOM]</ta>
            <ta e="T557" id="Seg_7826" s="T556">some</ta>
            <ta e="T558" id="Seg_7827" s="T557">horse.fly-ACC</ta>
            <ta e="T559" id="Seg_7828" s="T558">complete-SIM</ta>
            <ta e="T560" id="Seg_7829" s="T559">mount-CVB.SIM-mount-CVB.SIM-3PL</ta>
            <ta e="T561" id="Seg_7830" s="T560">reindeer.[NOM]</ta>
            <ta e="T562" id="Seg_7831" s="T561">own-3SG</ta>
            <ta e="T563" id="Seg_7832" s="T562">die-PTCP.PRS</ta>
            <ta e="T564" id="Seg_7833" s="T563">crawl-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T565" id="Seg_7834" s="T564">that</ta>
            <ta e="T566" id="Seg_7835" s="T565">some</ta>
            <ta e="T567" id="Seg_7836" s="T566">shepherd.[NOM]</ta>
            <ta e="T568" id="Seg_7837" s="T567">reindeer-ACC</ta>
            <ta e="T569" id="Seg_7838" s="T568">and</ta>
            <ta e="T570" id="Seg_7839" s="T569">harness-CVB.SIM</ta>
            <ta e="T571" id="Seg_7840" s="T570">can-NEG.[3SG]</ta>
            <ta e="T572" id="Seg_7841" s="T571">rein-3PL.[NOM]</ta>
            <ta e="T573" id="Seg_7842" s="T572">being.blown.away-PST1-3SG</ta>
            <ta e="T574" id="Seg_7843" s="T573">reindeer.[NOM]</ta>
            <ta e="T575" id="Seg_7844" s="T574">own-3SG-GEN</ta>
            <ta e="T576" id="Seg_7845" s="T575">groin-3SG-ACC</ta>
            <ta e="T577" id="Seg_7846" s="T576">cut.through-PRS-3PL</ta>
            <ta e="T578" id="Seg_7847" s="T577">groin-3SG-ACC</ta>
            <ta e="T579" id="Seg_7848" s="T578">cut.through-TEMP-3PL</ta>
            <ta e="T580" id="Seg_7849" s="T579">this</ta>
            <ta e="T581" id="Seg_7850" s="T580">in.summer</ta>
            <ta e="T582" id="Seg_7851" s="T581">wound.[NOM]</ta>
            <ta e="T583" id="Seg_7852" s="T582">be-HAB.[3SG]</ta>
            <ta e="T584" id="Seg_7853" s="T583">this-INSTR</ta>
            <ta e="T585" id="Seg_7854" s="T584">reindeer.[NOM]</ta>
            <ta e="T587" id="Seg_7855" s="T586">become.sick-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T588" id="Seg_7856" s="T587">that.[NOM]</ta>
            <ta e="T589" id="Seg_7857" s="T588">then</ta>
            <ta e="T590" id="Seg_7858" s="T589">tail-3SG-ACC</ta>
            <ta e="T591" id="Seg_7859" s="T590">reindeer.[NOM]</ta>
            <ta e="T592" id="Seg_7860" s="T591">own-3SG-ACC</ta>
            <ta e="T593" id="Seg_7861" s="T592">split-CVB.SIM</ta>
            <ta e="T594" id="Seg_7862" s="T593">push-PRS-3PL</ta>
            <ta e="T595" id="Seg_7863" s="T594">this</ta>
            <ta e="T596" id="Seg_7864" s="T595">tail-3SG-ACC</ta>
            <ta e="T597" id="Seg_7865" s="T596">split-CVB.SIM</ta>
            <ta e="T598" id="Seg_7866" s="T597">push-TEMP-3PL</ta>
            <ta e="T599" id="Seg_7867" s="T598">in.winter</ta>
            <ta e="T600" id="Seg_7868" s="T599">cold-DAT/LOC</ta>
            <ta e="T601" id="Seg_7869" s="T600">this</ta>
            <ta e="T602" id="Seg_7870" s="T601">reindeer.[NOM]</ta>
            <ta e="T603" id="Seg_7871" s="T602">own-3SG.[NOM]</ta>
            <ta e="T604" id="Seg_7872" s="T603">meat-3SG.[NOM]</ta>
            <ta e="T605" id="Seg_7873" s="T604">when</ta>
            <ta e="T606" id="Seg_7874" s="T605">NEG</ta>
            <ta e="T607" id="Seg_7875" s="T606">again</ta>
            <ta e="T608" id="Seg_7876" s="T607">rise-NEG.[3SG]</ta>
            <ta e="T609" id="Seg_7877" s="T608">then</ta>
            <ta e="T610" id="Seg_7878" s="T609">in.summer</ta>
            <ta e="T611" id="Seg_7879" s="T610">warm.[NOM]</ta>
            <ta e="T612" id="Seg_7880" s="T611">when</ta>
            <ta e="T613" id="Seg_7881" s="T612">nomadize-PRS-3PL</ta>
            <ta e="T614" id="Seg_7882" s="T613">this</ta>
            <ta e="T615" id="Seg_7883" s="T614">nomadize-TEMP-3PL</ta>
            <ta e="T616" id="Seg_7884" s="T615">heat-DAT/LOC</ta>
            <ta e="T617" id="Seg_7885" s="T616">nomadize-TEMP-3PL</ta>
            <ta e="T618" id="Seg_7886" s="T617">nomadize-CVB.SEQ</ta>
            <ta e="T619" id="Seg_7887" s="T618">go-CVB.SEQ-3PL</ta>
            <ta e="T620" id="Seg_7888" s="T619">river-PROPR</ta>
            <ta e="T621" id="Seg_7889" s="T620">place-PL-EP-INSTR</ta>
            <ta e="T622" id="Seg_7890" s="T621">go-PRS-3PL</ta>
            <ta e="T623" id="Seg_7891" s="T622">this</ta>
            <ta e="T624" id="Seg_7892" s="T623">river-PROPR</ta>
            <ta e="T625" id="Seg_7893" s="T624">big</ta>
            <ta e="T626" id="Seg_7894" s="T625">mountain-PL-DAT/LOC</ta>
            <ta e="T627" id="Seg_7895" s="T626">clay-PROPR.[NOM]</ta>
            <ta e="T628" id="Seg_7896" s="T627">be-HAB.[3SG]</ta>
            <ta e="T629" id="Seg_7897" s="T628">how.much</ta>
            <ta e="T630" id="Seg_7898" s="T629">and</ta>
            <ta e="T631" id="Seg_7899" s="T630">heat-DAT/LOC</ta>
            <ta e="T632" id="Seg_7900" s="T631">this</ta>
            <ta e="T633" id="Seg_7901" s="T632">clay-ACC</ta>
            <ta e="T634" id="Seg_7902" s="T633">recognise-PST1-3SG</ta>
            <ta e="T635" id="Seg_7903" s="T634">and</ta>
            <ta e="T636" id="Seg_7904" s="T635">reindeer.[NOM]</ta>
            <ta e="T637" id="Seg_7905" s="T636">this</ta>
            <ta e="T638" id="Seg_7906" s="T637">mountain-DAT/LOC</ta>
            <ta e="T639" id="Seg_7907" s="T638">go-HAB.[3SG]</ta>
            <ta e="T640" id="Seg_7908" s="T639">nomadize-CVB.SEQ</ta>
            <ta e="T641" id="Seg_7909" s="T640">go-PTCP.COND-DAT/LOC</ta>
            <ta e="T642" id="Seg_7910" s="T641">and</ta>
            <ta e="T643" id="Seg_7911" s="T642">hunt-CVB.SEQ</ta>
            <ta e="T644" id="Seg_7912" s="T643">and</ta>
            <ta e="T645" id="Seg_7913" s="T644">go-TEMP-3PL</ta>
            <ta e="T646" id="Seg_7914" s="T645">this-ACC</ta>
            <ta e="T647" id="Seg_7915" s="T646">shepherd-PL.[NOM]</ta>
            <ta e="T648" id="Seg_7916" s="T647">this</ta>
            <ta e="T649" id="Seg_7917" s="T648">mountain-ACC</ta>
            <ta e="T650" id="Seg_7918" s="T649">see-NEG.CVB.SIM</ta>
            <ta e="T651" id="Seg_7919" s="T650">dog-VBZ-CVB.SEQ</ta>
            <ta e="T652" id="Seg_7920" s="T651">take-PST1-3PL</ta>
            <ta e="T653" id="Seg_7921" s="T652">and</ta>
            <ta e="T654" id="Seg_7922" s="T653">EMPH-then</ta>
            <ta e="T655" id="Seg_7923" s="T654">laugh-RECP/COLL-CVB.SIM-laugh-RECP/COLL-CVB.SIM</ta>
            <ta e="T656" id="Seg_7924" s="T655">pass.by-CVB.SEQ</ta>
            <ta e="T657" id="Seg_7925" s="T656">stay-PRS-3PL</ta>
            <ta e="T658" id="Seg_7926" s="T657">here</ta>
            <ta e="T659" id="Seg_7927" s="T658">ten</ta>
            <ta e="T660" id="Seg_7928" s="T659">rest-3SG.[NOM]</ta>
            <ta e="T661" id="Seg_7929" s="T660">five</ta>
            <ta e="T662" id="Seg_7930" s="T661">MOD</ta>
            <ta e="T663" id="Seg_7931" s="T662">twenty</ta>
            <ta e="T664" id="Seg_7932" s="T663">rest-3SG.[NOM]</ta>
            <ta e="T665" id="Seg_7933" s="T664">five</ta>
            <ta e="T666" id="Seg_7934" s="T665">MOD</ta>
            <ta e="T667" id="Seg_7935" s="T666">reindeer.[NOM]</ta>
            <ta e="T668" id="Seg_7936" s="T667">stay-PRS.[3SG]</ta>
            <ta e="T669" id="Seg_7937" s="T668">this</ta>
            <ta e="T670" id="Seg_7938" s="T669">stay-PTCP.PST-3SG-ACC</ta>
            <ta e="T671" id="Seg_7939" s="T670">notice-NEG-3PL</ta>
            <ta e="T672" id="Seg_7940" s="T671">temporary.settlement-DAT/LOC</ta>
            <ta e="T673" id="Seg_7941" s="T672">fall-CVB.SEQ-3PL</ta>
            <ta e="T674" id="Seg_7942" s="T673">next</ta>
            <ta e="T675" id="Seg_7943" s="T674">human.being.[NOM]</ta>
            <ta e="T676" id="Seg_7944" s="T675">guard-PRS.[3SG]</ta>
            <ta e="T677" id="Seg_7945" s="T676">not.long.ago</ta>
            <ta e="T678" id="Seg_7946" s="T677">this</ta>
            <ta e="T679" id="Seg_7947" s="T678">nomadize-PRS-3PL-DAT/LOC</ta>
            <ta e="T680" id="Seg_7948" s="T679">guard-PTCP.PST</ta>
            <ta e="T681" id="Seg_7949" s="T680">human.being.[NOM]</ta>
            <ta e="T682" id="Seg_7950" s="T681">only</ta>
            <ta e="T683" id="Seg_7951" s="T682">riding.reindeer-3SG.[NOM]</ta>
            <ta e="T684" id="Seg_7952" s="T683">that</ta>
            <ta e="T685" id="Seg_7953" s="T684">reindeer-PL-DAT/LOC</ta>
            <ta e="T686" id="Seg_7954" s="T685">there</ta>
            <ta e="T687" id="Seg_7955" s="T686">mountain-DAT/LOC</ta>
            <ta e="T688" id="Seg_7956" s="T687">stay-PST2.[3SG]</ta>
            <ta e="T689" id="Seg_7957" s="T688">one</ta>
            <ta e="T690" id="Seg_7958" s="T689">riding.reindeer-3SG.[NOM]</ta>
            <ta e="T691" id="Seg_7959" s="T690">next.morning-3SG-ACC</ta>
            <ta e="T692" id="Seg_7960" s="T691">nomadize-PRS-3PL</ta>
            <ta e="T693" id="Seg_7961" s="T692">again</ta>
            <ta e="T694" id="Seg_7962" s="T693">nomadize-CVB.SEQ</ta>
            <ta e="T695" id="Seg_7963" s="T694">fall-CVB.SEQ</ta>
            <ta e="T696" id="Seg_7964" s="T695">after</ta>
            <ta e="T697" id="Seg_7965" s="T696">next</ta>
            <ta e="T698" id="Seg_7966" s="T697">human.being-PL.[NOM]</ta>
            <ta e="T699" id="Seg_7967" s="T698">guard-PRS-3PL</ta>
            <ta e="T700" id="Seg_7968" s="T699">this</ta>
            <ta e="T701" id="Seg_7969" s="T700">remote</ta>
            <ta e="T702" id="Seg_7970" s="T701">temporary.settlement-DAT/LOC</ta>
            <ta e="T703" id="Seg_7971" s="T702">guard-PTCP.PST</ta>
            <ta e="T704" id="Seg_7972" s="T703">in.turn</ta>
            <ta e="T705" id="Seg_7973" s="T704">human.being.[NOM]</ta>
            <ta e="T706" id="Seg_7974" s="T705">guard-CVB.SIM</ta>
            <ta e="T707" id="Seg_7975" s="T706">go.out-CVB.SEQ</ta>
            <ta e="T708" id="Seg_7976" s="T707">riding.reindeer-3SG-ACC</ta>
            <ta e="T709" id="Seg_7977" s="T708">then</ta>
            <ta e="T710" id="Seg_7978" s="T709">miss-PRS.[3SG]</ta>
            <ta e="T711" id="Seg_7979" s="T710">morning</ta>
            <ta e="T712" id="Seg_7980" s="T711">come-CVB.SEQ</ta>
            <ta e="T713" id="Seg_7981" s="T712">say-PRS.[3SG]</ta>
            <ta e="T714" id="Seg_7982" s="T713">well</ta>
            <ta e="T715" id="Seg_7983" s="T714">my</ta>
            <ta e="T716" id="Seg_7984" s="T715">riding.reindeer-EP-1SG.[NOM]</ta>
            <ta e="T717" id="Seg_7985" s="T716">NEG.EX</ta>
            <ta e="T718" id="Seg_7986" s="T717">there</ta>
            <ta e="T719" id="Seg_7987" s="T718">temporary.settlement-DAT/LOC</ta>
            <ta e="T720" id="Seg_7988" s="T719">NEG</ta>
            <ta e="T721" id="Seg_7989" s="T720">see-PST2.NEG-EP-1SG</ta>
            <ta e="T722" id="Seg_7990" s="T721">recent</ta>
            <ta e="T723" id="Seg_7991" s="T722">nomadize-PTCP.PST</ta>
            <ta e="T724" id="Seg_7992" s="T723">temporary.settlement-1SG-DAT/LOC</ta>
            <ta e="T725" id="Seg_7993" s="T724">remote</ta>
            <ta e="T726" id="Seg_7994" s="T725">temporary.settlement-DAT/LOC</ta>
            <ta e="T727" id="Seg_7995" s="T726">guard-PTCP.PRS-1SG-DAT/LOC</ta>
            <ta e="T728" id="Seg_7996" s="T727">mount-CVB.SIM</ta>
            <ta e="T729" id="Seg_7997" s="T728">go-EP-PTCP.PST</ta>
            <ta e="T730" id="Seg_7998" s="T729">reindeer-1SG.[NOM]</ta>
            <ta e="T731" id="Seg_7999" s="T730">be-PST1-3SG</ta>
            <ta e="T732" id="Seg_8000" s="T731">truth.[NOM]</ta>
            <ta e="T733" id="Seg_8001" s="T732">and</ta>
            <ta e="T734" id="Seg_8002" s="T733">there</ta>
            <ta e="T735" id="Seg_8003" s="T734">temporary.settlement-DAT/LOC</ta>
            <ta e="T736" id="Seg_8004" s="T735">stay-PST2.[3SG]</ta>
            <ta e="T737" id="Seg_8005" s="T736">MOD</ta>
            <ta e="T738" id="Seg_8006" s="T737">say-CVB.SIM</ta>
            <ta e="T739" id="Seg_8007" s="T738">say-PRS-3PL</ta>
            <ta e="T740" id="Seg_8008" s="T739">that</ta>
            <ta e="T741" id="Seg_8009" s="T740">reindeer-ACC</ta>
            <ta e="T742" id="Seg_8010" s="T741">this</ta>
            <ta e="T743" id="Seg_8011" s="T742">temporary.settlement-3PL-DAT/LOC</ta>
            <ta e="T744" id="Seg_8012" s="T743">search-CVB.SIM</ta>
            <ta e="T745" id="Seg_8013" s="T744">come-PRS-3PL</ta>
            <ta e="T746" id="Seg_8014" s="T745">today</ta>
            <ta e="T747" id="Seg_8015" s="T746">nomadize-PTCP.PST</ta>
            <ta e="T748" id="Seg_8016" s="T747">temporary.settlement-3PL-DAT/LOC</ta>
            <ta e="T749" id="Seg_8017" s="T748">that</ta>
            <ta e="T750" id="Seg_8018" s="T749">reindeer-PL-ACC</ta>
            <ta e="T751" id="Seg_8019" s="T750">remote</ta>
            <ta e="T752" id="Seg_8020" s="T751">reindeer.caravan-DAT/LOC</ta>
            <ta e="T753" id="Seg_8021" s="T752">stay-PTCP.PST</ta>
            <ta e="T754" id="Seg_8022" s="T753">reindeer-PL-ACC</ta>
            <ta e="T755" id="Seg_8023" s="T754">that.side.[NOM]</ta>
            <ta e="T756" id="Seg_8024" s="T755">temporary.settlement-DAT/LOC</ta>
            <ta e="T757" id="Seg_8025" s="T756">search-PRS-3PL</ta>
            <ta e="T758" id="Seg_8026" s="T757">that</ta>
            <ta e="T759" id="Seg_8027" s="T758">remote</ta>
            <ta e="T760" id="Seg_8028" s="T759">temporary.settlement-DAT/LOC</ta>
            <ta e="T761" id="Seg_8029" s="T760">guard-PTCP.PST</ta>
            <ta e="T762" id="Seg_8030" s="T761">shepherd.[NOM]</ta>
            <ta e="T763" id="Seg_8031" s="T762">riding.reindeer-3SG.[NOM]</ta>
            <ta e="T764" id="Seg_8032" s="T763">there</ta>
            <ta e="T765" id="Seg_8033" s="T764">stay-PST2.[3SG]</ta>
            <ta e="T766" id="Seg_8034" s="T765">that</ta>
            <ta e="T767" id="Seg_8035" s="T766">mountain-DAT/LOC</ta>
            <ta e="T768" id="Seg_8036" s="T767">that</ta>
            <ta e="T769" id="Seg_8037" s="T768">lonely</ta>
            <ta e="T770" id="Seg_8038" s="T769">reindeer-ACC</ta>
            <ta e="T771" id="Seg_8039" s="T770">remember-PRS-3PL</ta>
            <ta e="T772" id="Seg_8040" s="T771">there</ta>
            <ta e="T773" id="Seg_8041" s="T772">stay-PST2-3SG</ta>
            <ta e="T774" id="Seg_8042" s="T773">ten-ABL</ta>
            <ta e="T775" id="Seg_8043" s="T774">over</ta>
            <ta e="T776" id="Seg_8044" s="T775">reindeer.[NOM]</ta>
            <ta e="T777" id="Seg_8045" s="T776">that.[NOM]</ta>
            <ta e="T778" id="Seg_8046" s="T777">because.of</ta>
            <ta e="T781" id="Seg_8047" s="T780">reindeer.[NOM]</ta>
            <ta e="T782" id="Seg_8048" s="T781">message-POSS</ta>
            <ta e="T783" id="Seg_8049" s="T782">NEG</ta>
            <ta e="T784" id="Seg_8050" s="T783">get.lost-PRS-3SG.[NOM]</ta>
            <ta e="T785" id="Seg_8051" s="T784">that.[NOM]</ta>
            <ta e="T786" id="Seg_8052" s="T785">then</ta>
            <ta e="T787" id="Seg_8053" s="T786">year.[NOM]</ta>
            <ta e="T788" id="Seg_8054" s="T787">different-different.[NOM]</ta>
            <ta e="T789" id="Seg_8055" s="T788">be-HAB.[3SG]</ta>
            <ta e="T790" id="Seg_8056" s="T789">warm-VBZ-PTCP.FUT</ta>
            <ta e="T791" id="Seg_8057" s="T790">be-HAB.[3SG]</ta>
            <ta e="T792" id="Seg_8058" s="T791">this</ta>
            <ta e="T793" id="Seg_8059" s="T792">heat-DAT/LOC</ta>
            <ta e="T794" id="Seg_8060" s="T793">again</ta>
            <ta e="T795" id="Seg_8061" s="T794">now-ADJZ</ta>
            <ta e="T796" id="Seg_8062" s="T795">shepherd-PL.[NOM]</ta>
            <ta e="T797" id="Seg_8063" s="T796">reindeer-ACC</ta>
            <ta e="T798" id="Seg_8064" s="T797">can-CVB.SEQ</ta>
            <ta e="T799" id="Seg_8065" s="T798">care.about-NEG-3PL</ta>
            <ta e="T800" id="Seg_8066" s="T799">care.about-NEG-3PL</ta>
            <ta e="T801" id="Seg_8067" s="T800">what-ACC=Q</ta>
            <ta e="T802" id="Seg_8068" s="T801">heat-DAT/LOC</ta>
            <ta e="T803" id="Seg_8069" s="T802">wind-AG.[NOM]</ta>
            <ta e="T804" id="Seg_8070" s="T803">reindeer.[NOM]</ta>
            <ta e="T805" id="Seg_8071" s="T804">be-HAB.[3SG]</ta>
            <ta e="T806" id="Seg_8072" s="T805">wind-AG.[NOM]</ta>
            <ta e="T807" id="Seg_8073" s="T806">reindeer.[NOM]</ta>
            <ta e="T808" id="Seg_8074" s="T807">name-3SG.[NOM]</ta>
            <ta e="T809" id="Seg_8075" s="T808">wind-ACC</ta>
            <ta e="T810" id="Seg_8076" s="T809">follow-PRS.[3SG]</ta>
            <ta e="T811" id="Seg_8077" s="T810">this</ta>
            <ta e="T812" id="Seg_8078" s="T811">wind-ACC</ta>
            <ta e="T813" id="Seg_8079" s="T812">follow-PTCP.PRS</ta>
            <ta e="T814" id="Seg_8080" s="T813">reindeer-ACC</ta>
            <ta e="T815" id="Seg_8081" s="T814">heat.[NOM]</ta>
            <ta e="T816" id="Seg_8082" s="T815">pass.by-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T817" id="Seg_8083" s="T816">until</ta>
            <ta e="T818" id="Seg_8084" s="T817">human.being.[NOM]</ta>
            <ta e="T819" id="Seg_8085" s="T818">reindeer-ABL</ta>
            <ta e="T820" id="Seg_8086" s="T819">tie-HAB.[3SG]</ta>
            <ta e="T821" id="Seg_8087" s="T820">reindeer.[NOM]</ta>
            <ta e="T822" id="Seg_8088" s="T821">guard-TEMP-3PL</ta>
            <ta e="T823" id="Seg_8089" s="T822">human.being.[NOM]</ta>
            <ta e="T824" id="Seg_8090" s="T823">power-3SG.[NOM]</ta>
            <ta e="T825" id="Seg_8091" s="T824">be.enough-PTCP.HAB-3SG</ta>
            <ta e="T826" id="Seg_8092" s="T825">NEG.[3SG]</ta>
            <ta e="T827" id="Seg_8093" s="T826">heat-DAT/LOC</ta>
            <ta e="T828" id="Seg_8094" s="T827">wind-ACC</ta>
            <ta e="T830" id="Seg_8095" s="T929">make-NEG-3PL</ta>
            <ta e="T831" id="Seg_8096" s="T830">morning</ta>
            <ta e="T832" id="Seg_8097" s="T831">bring-PTCP.PRS-3PL-DAT/LOC</ta>
            <ta e="T833" id="Seg_8098" s="T832">next</ta>
            <ta e="T834" id="Seg_8099" s="T833">shepherd-DAT/LOC</ta>
            <ta e="T835" id="Seg_8100" s="T834">hand-PTCP.PRS-3PL-DAT/LOC</ta>
            <ta e="T836" id="Seg_8101" s="T835">1PL.[NOM]</ta>
            <ta e="T837" id="Seg_8102" s="T836">can-FUT-1PL</ta>
            <ta e="T838" id="Seg_8103" s="T837">NEG-3SG</ta>
            <ta e="T839" id="Seg_8104" s="T838">EVID</ta>
            <ta e="T840" id="Seg_8105" s="T839">say-CVB.SEQ</ta>
            <ta e="T841" id="Seg_8106" s="T840">when</ta>
            <ta e="T842" id="Seg_8107" s="T841">NEG</ta>
            <ta e="T843" id="Seg_8108" s="T842">wind.[NOM]</ta>
            <ta e="T844" id="Seg_8109" s="T843">lower.part.[NOM]</ta>
            <ta e="T845" id="Seg_8110" s="T844">to</ta>
            <ta e="T846" id="Seg_8111" s="T845">send-NMNZ.[NOM]</ta>
            <ta e="T847" id="Seg_8112" s="T846">make-PTCP.FUT-3PL-ACC</ta>
            <ta e="T848" id="Seg_8113" s="T847">house-3PL.[NOM]</ta>
            <ta e="T849" id="Seg_8114" s="T848">wind.[NOM]</ta>
            <ta e="T850" id="Seg_8115" s="T849">upper.part.[NOM]</ta>
            <ta e="T851" id="Seg_8116" s="T850">this</ta>
            <ta e="T852" id="Seg_8117" s="T851">be-PRS.[3SG]</ta>
            <ta e="T853" id="Seg_8118" s="T852">similar</ta>
            <ta e="T854" id="Seg_8119" s="T853">reindeer.[NOM]</ta>
            <ta e="T855" id="Seg_8120" s="T854">then</ta>
            <ta e="T856" id="Seg_8121" s="T855">can-NEG.CVB.SIM</ta>
            <ta e="T857" id="Seg_8122" s="T856">wind.[NOM]</ta>
            <ta e="T858" id="Seg_8123" s="T857">towards</ta>
            <ta e="T859" id="Seg_8124" s="T858">come-TEMP-3SG</ta>
            <ta e="T860" id="Seg_8125" s="T859">house-3SG-DAT/LOC</ta>
            <ta e="T861" id="Seg_8126" s="T860">push-CAUS-TEMP-3SG</ta>
            <ta e="T862" id="Seg_8127" s="T861">house-ADJZ-3PL.[NOM]</ta>
            <ta e="T863" id="Seg_8128" s="T862">stand.up-CVB.SEQ-3PL</ta>
            <ta e="T864" id="Seg_8129" s="T863">here</ta>
            <ta e="T865" id="Seg_8130" s="T864">help-CVB.SEQ-3PL</ta>
            <ta e="T866" id="Seg_8131" s="T865">then</ta>
            <ta e="T867" id="Seg_8132" s="T866">win-PRS-3PL</ta>
            <ta e="T868" id="Seg_8133" s="T867">reindeer-ACC</ta>
            <ta e="T869" id="Seg_8134" s="T868">that.[NOM]</ta>
            <ta e="T870" id="Seg_8135" s="T869">after</ta>
            <ta e="T871" id="Seg_8136" s="T870">3PL.[NOM]</ta>
            <ta e="T872" id="Seg_8137" s="T871">spring-ADJZ-ABL</ta>
            <ta e="T873" id="Seg_8138" s="T872">heat.[NOM]</ta>
            <ta e="T874" id="Seg_8139" s="T873">fall-CVB.SIM</ta>
            <ta e="T875" id="Seg_8140" s="T874">not.yet-3SG</ta>
            <ta e="T876" id="Seg_8141" s="T875">just</ta>
            <ta e="T877" id="Seg_8142" s="T876">heat.[NOM]</ta>
            <ta e="T878" id="Seg_8143" s="T877">fall-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T879" id="Seg_8144" s="T878">reindeer.[NOM]</ta>
            <ta e="T880" id="Seg_8145" s="T879">feel-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T881" id="Seg_8146" s="T880">smoke-DAT/LOC</ta>
            <ta e="T882" id="Seg_8147" s="T881">reindeer-ACC</ta>
            <ta e="T883" id="Seg_8148" s="T882">teach-PTCP.FUT-3PL-ACC</ta>
            <ta e="T884" id="Seg_8149" s="T883">need.to</ta>
            <ta e="T885" id="Seg_8150" s="T884">big</ta>
            <ta e="T886" id="Seg_8151" s="T885">heat.[NOM]</ta>
            <ta e="T887" id="Seg_8152" s="T886">fall-CVB.SIM</ta>
            <ta e="T888" id="Seg_8153" s="T887">not.yet-3SG</ta>
            <ta e="T889" id="Seg_8154" s="T888">smoke-ACC</ta>
            <ta e="T890" id="Seg_8155" s="T889">see-PST1-3SG</ta>
            <ta e="T891" id="Seg_8156" s="T890">and</ta>
            <ta e="T892" id="Seg_8157" s="T891">how.much</ta>
            <ta e="T893" id="Seg_8158" s="T892">and</ta>
            <ta e="T894" id="Seg_8159" s="T893">heat-DAT/LOC</ta>
            <ta e="T896" id="Seg_8160" s="T894">reindeer.[NOM]</ta>
            <ta e="T897" id="Seg_8161" s="T896">house-ADJZ-3PL.[NOM]</ta>
            <ta e="T898" id="Seg_8162" s="T897">heat-DAT/LOC</ta>
            <ta e="T899" id="Seg_8163" s="T898">shepherd-PL.[NOM]</ta>
            <ta e="T900" id="Seg_8164" s="T899">guard-CVB.SIM</ta>
            <ta e="T901" id="Seg_8165" s="T900">go-CVB.PURP-3PL</ta>
            <ta e="T902" id="Seg_8166" s="T901">speak-PTCP.FUT</ta>
            <ta e="T903" id="Seg_8167" s="T902">fall-NEC-3PL</ta>
            <ta e="T904" id="Seg_8168" s="T903">well</ta>
            <ta e="T905" id="Seg_8169" s="T904">heat.[NOM]</ta>
            <ta e="T906" id="Seg_8170" s="T905">be-TEMP-3SG</ta>
            <ta e="T907" id="Seg_8171" s="T906">well</ta>
            <ta e="T908" id="Seg_8172" s="T907">fear-CVB.SEQ</ta>
            <ta e="T909" id="Seg_8173" s="T908">sleep-EP-IMP.2PL</ta>
            <ta e="T910" id="Seg_8174" s="T909">smoke-VBZ-EP-CAUS-FUT-EP-IMP.2PL</ta>
            <ta e="T911" id="Seg_8175" s="T910">campfire.[NOM]</ta>
            <ta e="T912" id="Seg_8176" s="T911">heat-FUT-EP-IMP.2PL</ta>
            <ta e="T913" id="Seg_8177" s="T912">warm</ta>
            <ta e="T914" id="Seg_8178" s="T913">be-TEMP-3SG</ta>
            <ta e="T915" id="Seg_8179" s="T914">maybe</ta>
            <ta e="T916" id="Seg_8180" s="T915">can-FUT-1PL</ta>
            <ta e="T917" id="Seg_8181" s="T916">NEG-3SG</ta>
            <ta e="T918" id="Seg_8182" s="T917">how</ta>
            <ta e="T919" id="Seg_8183" s="T918">INDEF</ta>
            <ta e="T920" id="Seg_8184" s="T919">warm.[NOM]</ta>
            <ta e="T921" id="Seg_8185" s="T920">be-CVB.SEQ</ta>
            <ta e="T922" id="Seg_8186" s="T921">be-PRS.[3SG]</ta>
            <ta e="T923" id="Seg_8187" s="T922">that.[NOM]</ta>
            <ta e="T924" id="Seg_8188" s="T923">similar</ta>
            <ta e="T925" id="Seg_8189" s="T924">care.about-PTCP.HAB</ta>
            <ta e="T926" id="Seg_8190" s="T925">be-PST1-1PL</ta>
            <ta e="T927" id="Seg_8191" s="T926">1PL.[NOM]</ta>
            <ta e="T928" id="Seg_8192" s="T927">reindeer-ACC</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_8193" s="T0">doch</ta>
            <ta e="T2" id="Seg_8194" s="T1">1SG.[NOM]</ta>
            <ta e="T3" id="Seg_8195" s="T2">Rentier-AG-PL-DAT/LOC</ta>
            <ta e="T4" id="Seg_8196" s="T3">gehört.werden-EP-CAUS-CVB.PURP</ta>
            <ta e="T5" id="Seg_8197" s="T4">sprechen-PRS-1SG</ta>
            <ta e="T6" id="Seg_8198" s="T5">1SG.[NOM]</ta>
            <ta e="T7" id="Seg_8199" s="T6">viel-ADVZ</ta>
            <ta e="T8" id="Seg_8200" s="T7">arbeiten-PTCP.PST</ta>
            <ta e="T9" id="Seg_8201" s="T8">Mensch-1SG</ta>
            <ta e="T10" id="Seg_8202" s="T9">Rentier-DAT/LOC</ta>
            <ta e="T11" id="Seg_8203" s="T10">früher</ta>
            <ta e="T12" id="Seg_8204" s="T11">dieses</ta>
            <ta e="T13" id="Seg_8205" s="T12">Rentier.[NOM]</ta>
            <ta e="T14" id="Seg_8206" s="T13">geboren.werden-PTCP.PRS-3SG-GEN</ta>
            <ta e="T15" id="Seg_8207" s="T14">Seite-3SG-INSTR</ta>
            <ta e="T16" id="Seg_8208" s="T15">sprechen-FUT-1SG</ta>
            <ta e="T17" id="Seg_8209" s="T16">jetzt</ta>
            <ta e="T18" id="Seg_8210" s="T17">was.für.ein</ta>
            <ta e="T19" id="Seg_8211" s="T18">INDEF</ta>
            <ta e="T20" id="Seg_8212" s="T19">EMPH</ta>
            <ta e="T21" id="Seg_8213" s="T20">gebären-PTCP.PRS-3SG-ABL</ta>
            <ta e="T22" id="Seg_8214" s="T21">sein-PRS.[3SG]</ta>
            <ta e="T23" id="Seg_8215" s="T22">EMPH</ta>
            <ta e="T24" id="Seg_8216" s="T23">Rentier.[NOM]</ta>
            <ta e="T25" id="Seg_8217" s="T24">und</ta>
            <ta e="T26" id="Seg_8218" s="T25">entstehen-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T27" id="Seg_8219" s="T26">Mensch.[NOM]</ta>
            <ta e="T28" id="Seg_8220" s="T27">doch</ta>
            <ta e="T29" id="Seg_8221" s="T28">so</ta>
            <ta e="T30" id="Seg_8222" s="T29">1SG.[NOM]</ta>
            <ta e="T31" id="Seg_8223" s="T30">sprechen-PRS-1SG</ta>
            <ta e="T32" id="Seg_8224" s="T31">doch</ta>
            <ta e="T33" id="Seg_8225" s="T32">so</ta>
            <ta e="T34" id="Seg_8226" s="T33">Rentier.[NOM]</ta>
            <ta e="T35" id="Seg_8227" s="T34">gebären-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T36" id="Seg_8228" s="T35">Hirte-PL-DAT/LOC</ta>
            <ta e="T37" id="Seg_8229" s="T36">gehört.werden-EP-CAUS-CVB.PURP</ta>
            <ta e="T38" id="Seg_8230" s="T37">1SG.[NOM]</ta>
            <ta e="T39" id="Seg_8231" s="T38">sprechen-PTCP.PRS-EP-1SG.[NOM]</ta>
            <ta e="T40" id="Seg_8232" s="T39">doch</ta>
            <ta e="T41" id="Seg_8233" s="T40">so-INTNS</ta>
            <ta e="T42" id="Seg_8234" s="T41">jetzt</ta>
            <ta e="T43" id="Seg_8235" s="T42">dieses</ta>
            <ta e="T44" id="Seg_8236" s="T43">Rentier-ACC</ta>
            <ta e="T45" id="Seg_8237" s="T44">sich.kümmern-PRS-3PL</ta>
            <ta e="T46" id="Seg_8238" s="T45">1SG.[NOM]</ta>
            <ta e="T47" id="Seg_8239" s="T46">früher</ta>
            <ta e="T48" id="Seg_8240" s="T47">sich.kümmern-CVB.SIM</ta>
            <ta e="T49" id="Seg_8241" s="T48">gehen-PTCP.PRS</ta>
            <ta e="T50" id="Seg_8242" s="T49">sein-TEMP-1SG</ta>
            <ta e="T51" id="Seg_8243" s="T50">Rentier.[NOM]</ta>
            <ta e="T52" id="Seg_8244" s="T51">gebären-TEMP-3SG</ta>
            <ta e="T53" id="Seg_8245" s="T52">viel</ta>
            <ta e="T54" id="Seg_8246" s="T53">Grund-PROPR.[NOM]</ta>
            <ta e="T55" id="Seg_8247" s="T54">sein-PRS.[3SG]</ta>
            <ta e="T56" id="Seg_8248" s="T55">Rentier.[NOM]</ta>
            <ta e="T57" id="Seg_8249" s="T56">geboren.werden-PTCP.PRS-3SG-ACC</ta>
            <ta e="T58" id="Seg_8250" s="T57">Hirte.[NOM]</ta>
            <ta e="T59" id="Seg_8251" s="T58">sein-TEMP-1SG</ta>
            <ta e="T60" id="Seg_8252" s="T59">beobachten-CVB.SIM</ta>
            <ta e="T61" id="Seg_8253" s="T60">gehen-CVB.SEQ-1SG</ta>
            <ta e="T62" id="Seg_8254" s="T61">herumgehen-PTCP.HAB</ta>
            <ta e="T63" id="Seg_8255" s="T62">sein-PST1-1SG</ta>
            <ta e="T64" id="Seg_8256" s="T63">Rentier.[NOM]</ta>
            <ta e="T65" id="Seg_8257" s="T64">gebären-CVB.SEQ</ta>
            <ta e="T66" id="Seg_8258" s="T65">Kind-3SG-ACC</ta>
            <ta e="T67" id="Seg_8259" s="T66">fallen.lassen-TEMP-3SG</ta>
            <ta e="T68" id="Seg_8260" s="T67">unterschiedlich-unterschiedlich-ADVZ</ta>
            <ta e="T69" id="Seg_8261" s="T68">geboren.werden-PTCP.PRS</ta>
            <ta e="T70" id="Seg_8262" s="T69">Rentier.[NOM]</ta>
            <ta e="T71" id="Seg_8263" s="T70">mancher</ta>
            <ta e="T72" id="Seg_8264" s="T71">Rentier.[NOM]</ta>
            <ta e="T73" id="Seg_8265" s="T72">Rentierkalb-3SG.[NOM]</ta>
            <ta e="T74" id="Seg_8266" s="T73">geboren.werden-CVB.SEQ</ta>
            <ta e="T75" id="Seg_8267" s="T74">nachdem</ta>
            <ta e="T76" id="Seg_8268" s="T75">mit.den.Beinen.zappeln-CVB.SIM</ta>
            <ta e="T77" id="Seg_8269" s="T76">liegen-HAB.[3SG]</ta>
            <ta e="T78" id="Seg_8270" s="T77">schaffen-CVB.SEQ</ta>
            <ta e="T79" id="Seg_8271" s="T78">aufstehen-NEG.CVB.SIM</ta>
            <ta e="T80" id="Seg_8272" s="T79">Mutter-3SG-ACC</ta>
            <ta e="T81" id="Seg_8273" s="T80">sehen-PTCP.COND-DAT/LOC</ta>
            <ta e="T82" id="Seg_8274" s="T81">fett.[NOM]</ta>
            <ta e="T83" id="Seg_8275" s="T82">sehr</ta>
            <ta e="T84" id="Seg_8276" s="T83">Rentierkalb-3SG-ACC</ta>
            <ta e="T85" id="Seg_8277" s="T84">sehen-PTCP.COND-DAT/LOC</ta>
            <ta e="T86" id="Seg_8278" s="T85">groß.[NOM]</ta>
            <ta e="T87" id="Seg_8279" s="T86">sehr</ta>
            <ta e="T88" id="Seg_8280" s="T87">sein-PRS.[3SG]</ta>
            <ta e="T89" id="Seg_8281" s="T88">dieses</ta>
            <ta e="T90" id="Seg_8282" s="T89">mit.den.Beinen.zappeln-CVB.SIM</ta>
            <ta e="T91" id="Seg_8283" s="T90">liegen-PTCP.PRS-ACC</ta>
            <ta e="T92" id="Seg_8284" s="T91">sich.drehen-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T93" id="Seg_8285" s="T92">man.muss</ta>
            <ta e="T94" id="Seg_8286" s="T93">sich.drehen-CAUS-CVB.SEQ</ta>
            <ta e="T95" id="Seg_8287" s="T94">versuchen-PTCP.COND-DAT/LOC</ta>
            <ta e="T96" id="Seg_8288" s="T95">was-ACC</ta>
            <ta e="T97" id="Seg_8289" s="T96">machen-CVB.SEQ</ta>
            <ta e="T98" id="Seg_8290" s="T97">warum</ta>
            <ta e="T99" id="Seg_8291" s="T98">aufstehen-NEG.[3SG]</ta>
            <ta e="T100" id="Seg_8292" s="T99">denken-CVB.SEQ</ta>
            <ta e="T101" id="Seg_8293" s="T100">dieses-ACC</ta>
            <ta e="T102" id="Seg_8294" s="T101">massieren-PTCP.FUT-DAT/LOC</ta>
            <ta e="T103" id="Seg_8295" s="T102">Bauch-3SG-ACC</ta>
            <ta e="T104" id="Seg_8296" s="T103">massieren-PTCP.FUT-DAT/LOC</ta>
            <ta e="T105" id="Seg_8297" s="T104">Rentier.[NOM]</ta>
            <ta e="T106" id="Seg_8298" s="T105">gebären-TEMP-3SG</ta>
            <ta e="T107" id="Seg_8299" s="T106">Rentierkalb.[NOM]</ta>
            <ta e="T108" id="Seg_8300" s="T107">eigen-3SG</ta>
            <ta e="T109" id="Seg_8301" s="T108">Nabel-PROPR.[NOM]</ta>
            <ta e="T110" id="Seg_8302" s="T109">sein-HAB.[3SG]</ta>
            <ta e="T111" id="Seg_8303" s="T110">dieses</ta>
            <ta e="T112" id="Seg_8304" s="T111">Nabel-3SG-ACC</ta>
            <ta e="T113" id="Seg_8305" s="T112">mancher</ta>
            <ta e="T114" id="Seg_8306" s="T113">Rentier.[NOM]</ta>
            <ta e="T115" id="Seg_8307" s="T114">ziehen-HAB.[3SG]</ta>
            <ta e="T116" id="Seg_8308" s="T115">dieses</ta>
            <ta e="T117" id="Seg_8309" s="T116">Nabel-3SG.[NOM]</ta>
            <ta e="T118" id="Seg_8310" s="T117">ziehen-PASS/REFL-TEMP-3SG</ta>
            <ta e="T119" id="Seg_8311" s="T118">dieses-INSTR</ta>
            <ta e="T120" id="Seg_8312" s="T119">Inneres-EP-ABL</ta>
            <ta e="T121" id="Seg_8313" s="T120">direkt</ta>
            <ta e="T122" id="Seg_8314" s="T121">Loch.[NOM]</ta>
            <ta e="T123" id="Seg_8315" s="T122">werden-PRS.[3SG]</ta>
            <ta e="T124" id="Seg_8316" s="T123">jenes-INSTR</ta>
            <ta e="T125" id="Seg_8317" s="T124">Atem-3SG.[NOM]</ta>
            <ta e="T126" id="Seg_8318" s="T125">hinausgehen-PRS.[3SG]</ta>
            <ta e="T127" id="Seg_8319" s="T126">Faden-EP-INSTR</ta>
            <ta e="T128" id="Seg_8320" s="T127">heftig</ta>
            <ta e="T129" id="Seg_8321" s="T128">binden-PTCP.COND-DAT/LOC</ta>
            <ta e="T130" id="Seg_8322" s="T129">dieses</ta>
            <ta e="T131" id="Seg_8323" s="T130">Rentier.[NOM]</ta>
            <ta e="T132" id="Seg_8324" s="T131">dieses</ta>
            <ta e="T133" id="Seg_8325" s="T132">Rentierkalb.[NOM]</ta>
            <ta e="T134" id="Seg_8326" s="T133">aufstehen-CVB.SEQ</ta>
            <ta e="T135" id="Seg_8327" s="T134">kommen-HAB.[3SG]</ta>
            <ta e="T136" id="Seg_8328" s="T135">jenes</ta>
            <ta e="T137" id="Seg_8329" s="T136">Name-3SG.[NOM]</ta>
            <ta e="T138" id="Seg_8330" s="T137">Nabel-3SG-ACC</ta>
            <ta e="T139" id="Seg_8331" s="T138">ziehen-PRS.[3SG]</ta>
            <ta e="T140" id="Seg_8332" s="T139">dieses.[NOM]</ta>
            <ta e="T141" id="Seg_8333" s="T140">sein-PRS.[3SG]</ta>
            <ta e="T142" id="Seg_8334" s="T141">Rentier.[NOM]</ta>
            <ta e="T143" id="Seg_8335" s="T142">eigen-3SG.[NOM]</ta>
            <ta e="T144" id="Seg_8336" s="T143">Rentierkalb-3SG-GEN</ta>
            <ta e="T145" id="Seg_8337" s="T144">eigen-3SG.[NOM]</ta>
            <ta e="T146" id="Seg_8338" s="T145">Grund-3SG.[NOM]</ta>
            <ta e="T147" id="Seg_8339" s="T146">gebären-TEMP-3SG</ta>
            <ta e="T148" id="Seg_8340" s="T147">dann</ta>
            <ta e="T149" id="Seg_8341" s="T148">eins</ta>
            <ta e="T150" id="Seg_8342" s="T149">Grund-PROPR.[NOM]</ta>
            <ta e="T151" id="Seg_8343" s="T150">schwach.[NOM]</ta>
            <ta e="T152" id="Seg_8344" s="T151">Rentier.[NOM]</ta>
            <ta e="T153" id="Seg_8345" s="T152">geboren.werden-PRS.[3SG]</ta>
            <ta e="T154" id="Seg_8346" s="T153">flink</ta>
            <ta e="T155" id="Seg_8347" s="T154">Mutter-3SG.[NOM]</ta>
            <ta e="T156" id="Seg_8348" s="T155">dieses</ta>
            <ta e="T157" id="Seg_8349" s="T156">Rentierkalb-3SG.[NOM]</ta>
            <ta e="T158" id="Seg_8350" s="T157">geboren.werden-TEMP-3SG</ta>
            <ta e="T159" id="Seg_8351" s="T158">Djaptschu.[NOM]</ta>
            <ta e="T160" id="Seg_8352" s="T159">sagen-CVB.SEQ</ta>
            <ta e="T161" id="Seg_8353" s="T160">Name-VBZ-HAB.[3SG]</ta>
            <ta e="T162" id="Seg_8354" s="T161">auch</ta>
            <ta e="T163" id="Seg_8355" s="T162">Rentierkalb-3SG.[NOM]</ta>
            <ta e="T164" id="Seg_8356" s="T163">stehen-PTCP.HAB-3SG</ta>
            <ta e="T165" id="Seg_8357" s="T164">NEG.[3SG]</ta>
            <ta e="T166" id="Seg_8358" s="T165">dieses</ta>
            <ta e="T167" id="Seg_8359" s="T166">Rentierkalb-ACC</ta>
            <ta e="T168" id="Seg_8360" s="T167">sich.drehen-CAUS-CVB.SEQ</ta>
            <ta e="T169" id="Seg_8361" s="T168">versuchen-PTCP.COND-DAT/LOC</ta>
            <ta e="T170" id="Seg_8362" s="T169">völlig</ta>
            <ta e="T171" id="Seg_8363" s="T170">schwach.[NOM]</ta>
            <ta e="T172" id="Seg_8364" s="T171">sehr</ta>
            <ta e="T173" id="Seg_8365" s="T172">sein-PRS.[3SG]</ta>
            <ta e="T174" id="Seg_8366" s="T173">dieses</ta>
            <ta e="T175" id="Seg_8367" s="T174">Rentierkalb.[NOM]</ta>
            <ta e="T176" id="Seg_8368" s="T175">mit.den.Beinen.zappeln-CVB.SIM</ta>
            <ta e="T177" id="Seg_8369" s="T176">nur</ta>
            <ta e="T178" id="Seg_8370" s="T177">liegen-HAB.[3SG]</ta>
            <ta e="T179" id="Seg_8371" s="T178">dieses</ta>
            <ta e="T180" id="Seg_8372" s="T179">Rentierkalb-ACC</ta>
            <ta e="T181" id="Seg_8373" s="T180">auf.dem.Rücken</ta>
            <ta e="T182" id="Seg_8374" s="T181">halten-CVB.SEQ</ta>
            <ta e="T183" id="Seg_8375" s="T182">nachdem</ta>
            <ta e="T184" id="Seg_8376" s="T183">Brust-3SG-ACC</ta>
            <ta e="T185" id="Seg_8377" s="T184">streichen.über-PTCP.COND-DAT/LOC</ta>
            <ta e="T186" id="Seg_8378" s="T185">dann</ta>
            <ta e="T187" id="Seg_8379" s="T186">dieses</ta>
            <ta e="T188" id="Seg_8380" s="T187">Brust-3SG.[NOM]</ta>
            <ta e="T189" id="Seg_8381" s="T188">Djaptschu.[NOM]</ta>
            <ta e="T190" id="Seg_8382" s="T189">eigen-3SG.[NOM]</ta>
            <ta e="T191" id="Seg_8383" s="T190">wann</ta>
            <ta e="T192" id="Seg_8384" s="T191">INDEF</ta>
            <ta e="T193" id="Seg_8385" s="T192">Rentierkalb.[NOM]</ta>
            <ta e="T194" id="Seg_8386" s="T193">geboren.werden-TEMP-3SG</ta>
            <ta e="T195" id="Seg_8387" s="T194">Knochen-3SG.[NOM]</ta>
            <ta e="T196" id="Seg_8388" s="T195">weich.[NOM]</ta>
            <ta e="T197" id="Seg_8389" s="T196">sein-PRS.[3SG]</ta>
            <ta e="T198" id="Seg_8390" s="T197">dieses-ACC</ta>
            <ta e="T199" id="Seg_8391" s="T198">Brust-3SG-ACC</ta>
            <ta e="T200" id="Seg_8392" s="T199">streichen.über-PTCP.COND-DAT/LOC</ta>
            <ta e="T201" id="Seg_8393" s="T200">aufstehen-CVB.SIM-aufstehen-CVB.SIM</ta>
            <ta e="T202" id="Seg_8394" s="T201">fallen-CVB.SIM</ta>
            <ta e="T203" id="Seg_8395" s="T202">gehen-HAB.[3SG]</ta>
            <ta e="T204" id="Seg_8396" s="T203">atmen-TEMP-3SG</ta>
            <ta e="T205" id="Seg_8397" s="T204">dieses-ACC</ta>
            <ta e="T206" id="Seg_8398" s="T205">Mutter-3SG-ACC</ta>
            <ta e="T207" id="Seg_8399" s="T206">greifen-CVB.SEQ</ta>
            <ta e="T208" id="Seg_8400" s="T207">nachdem</ta>
            <ta e="T209" id="Seg_8401" s="T208">Mutter-3SG-ACC</ta>
            <ta e="T210" id="Seg_8402" s="T209">saugen-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T211" id="Seg_8403" s="T210">man.muss</ta>
            <ta e="T212" id="Seg_8404" s="T211">dieses</ta>
            <ta e="T213" id="Seg_8405" s="T212">Rentierkalb.[NOM]</ta>
            <ta e="T214" id="Seg_8406" s="T213">Brust-3SG-ACC</ta>
            <ta e="T215" id="Seg_8407" s="T214">wer-ACC</ta>
            <ta e="T216" id="Seg_8408" s="T215">fegen-PTCP.PST-3SG-ACC</ta>
            <ta e="T217" id="Seg_8409" s="T216">nachdem</ta>
            <ta e="T218" id="Seg_8410" s="T217">schaffen-CVB.SEQ</ta>
            <ta e="T219" id="Seg_8411" s="T218">saugen-PTCP.HAB-3SG</ta>
            <ta e="T220" id="Seg_8412" s="T219">NEG.[3SG]</ta>
            <ta e="T221" id="Seg_8413" s="T220">dieses-ACC</ta>
            <ta e="T222" id="Seg_8414" s="T221">Mutter-3SG.[NOM]</ta>
            <ta e="T223" id="Seg_8415" s="T222">Busen-3SG.[NOM]</ta>
            <ta e="T224" id="Seg_8416" s="T223">Baumharz-PROPR.[NOM]</ta>
            <ta e="T225" id="Seg_8417" s="T224">sein-PST1-3SG</ta>
            <ta e="T226" id="Seg_8418" s="T225">dick.und.kurz.[NOM]</ta>
            <ta e="T227" id="Seg_8419" s="T226">dieses</ta>
            <ta e="T228" id="Seg_8420" s="T227">Baumharz-ACC</ta>
            <ta e="T229" id="Seg_8421" s="T228">pressen-HAB.[3SG]</ta>
            <ta e="T230" id="Seg_8422" s="T229">Mensch.[NOM]</ta>
            <ta e="T231" id="Seg_8423" s="T230">pressen-CVB.SEQ</ta>
            <ta e="T232" id="Seg_8424" s="T231">nachdem</ta>
            <ta e="T233" id="Seg_8425" s="T232">flüssig</ta>
            <ta e="T234" id="Seg_8426" s="T233">Milch-3SG.[NOM]</ta>
            <ta e="T235" id="Seg_8427" s="T234">kommen-TEMP-3SG</ta>
            <ta e="T236" id="Seg_8428" s="T235">dann</ta>
            <ta e="T237" id="Seg_8429" s="T236">saugen-CAUS-HAB.[3SG]</ta>
            <ta e="T238" id="Seg_8430" s="T237">dann</ta>
            <ta e="T239" id="Seg_8431" s="T238">saugen-CAUS-TEMP-3SG</ta>
            <ta e="T240" id="Seg_8432" s="T239">dieses</ta>
            <ta e="T241" id="Seg_8433" s="T240">Rentierkalb.[NOM]</ta>
            <ta e="T242" id="Seg_8434" s="T241">Mensch.[NOM]</ta>
            <ta e="T243" id="Seg_8435" s="T242">werden-PRS.[3SG]</ta>
            <ta e="T244" id="Seg_8436" s="T243">Hirte-PL.[NOM]</ta>
            <ta e="T245" id="Seg_8437" s="T244">nomadisieren-PRS-3PL</ta>
            <ta e="T246" id="Seg_8438" s="T245">viel</ta>
            <ta e="T247" id="Seg_8439" s="T246">Rentier.[NOM]</ta>
            <ta e="T248" id="Seg_8440" s="T247">gebären-TEMP-3SG</ta>
            <ta e="T249" id="Seg_8441" s="T248">wann</ta>
            <ta e="T250" id="Seg_8442" s="T249">NEG</ta>
            <ta e="T251" id="Seg_8443" s="T250">gebären-CVB.SIM=noch.nicht</ta>
            <ta e="T252" id="Seg_8444" s="T251">Rentier-ACC</ta>
            <ta e="T253" id="Seg_8445" s="T252">Vorderteil.[NOM]</ta>
            <ta e="T254" id="Seg_8446" s="T253">zu</ta>
            <ta e="T255" id="Seg_8447" s="T254">mitnehmen-HAB-3PL</ta>
            <ta e="T256" id="Seg_8448" s="T255">gebären-PTCP.PST</ta>
            <ta e="T257" id="Seg_8449" s="T256">Rentier-ACC</ta>
            <ta e="T258" id="Seg_8450" s="T257">Rentierkalb-3SG.[NOM]</ta>
            <ta e="T259" id="Seg_8451" s="T258">wachsen-CVB.SIM</ta>
            <ta e="T260" id="Seg_8452" s="T259">fallen-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T261" id="Seg_8453" s="T260">bis.zu</ta>
            <ta e="T262" id="Seg_8454" s="T261">go-NEG.PTCP-3SG-ACC</ta>
            <ta e="T263" id="Seg_8455" s="T262">wegen</ta>
            <ta e="T264" id="Seg_8456" s="T263">Hinterteil.[NOM]</ta>
            <ta e="T265" id="Seg_8457" s="T264">zu</ta>
            <ta e="T266" id="Seg_8458" s="T265">lassen-PRS-3PL</ta>
            <ta e="T267" id="Seg_8459" s="T266">dieses</ta>
            <ta e="T268" id="Seg_8460" s="T267">lassen-NMNZ-3PL-DAT/LOC</ta>
            <ta e="T269" id="Seg_8461" s="T268">dieses</ta>
            <ta e="T270" id="Seg_8462" s="T269">doch</ta>
            <ta e="T271" id="Seg_8463" s="T270">zehn-ABL</ta>
            <ta e="T272" id="Seg_8464" s="T271">über</ta>
            <ta e="T273" id="Seg_8465" s="T272">zwanzig-APRX</ta>
            <ta e="T274" id="Seg_8466" s="T273">Rentier.[NOM]</ta>
            <ta e="T275" id="Seg_8467" s="T274">gebären-PTCP.PST-3SG-ACC</ta>
            <ta e="T276" id="Seg_8468" s="T275">nachdem</ta>
            <ta e="T277" id="Seg_8469" s="T276">dann</ta>
            <ta e="T278" id="Seg_8470" s="T277">dieses</ta>
            <ta e="T279" id="Seg_8471" s="T278">gebären-CVB.SIM=noch.nicht</ta>
            <ta e="T280" id="Seg_8472" s="T279">Rentier-ACC</ta>
            <ta e="T281" id="Seg_8473" s="T280">Vorderteil.[NOM]</ta>
            <ta e="T282" id="Seg_8474" s="T281">zu</ta>
            <ta e="T283" id="Seg_8475" s="T282">mitnehmen-PTCP.FUT-DAT/LOC</ta>
            <ta e="T284" id="Seg_8476" s="T283">man.muss</ta>
            <ta e="T285" id="Seg_8477" s="T284">dieses</ta>
            <ta e="T286" id="Seg_8478" s="T285">Rentier-ACC</ta>
            <ta e="T287" id="Seg_8479" s="T286">mitnehmen-PTCP.PRS-DAT/LOC</ta>
            <ta e="T288" id="Seg_8480" s="T287">mancher</ta>
            <ta e="T289" id="Seg_8481" s="T288">Hirte.[NOM]</ta>
            <ta e="T290" id="Seg_8482" s="T289">dieses</ta>
            <ta e="T291" id="Seg_8483" s="T290">gebären-PTCP.PST</ta>
            <ta e="T292" id="Seg_8484" s="T291">Kalb-PROPR-DAT/LOC</ta>
            <ta e="T293" id="Seg_8485" s="T292">bleiben-PRS.[3SG]</ta>
            <ta e="T294" id="Seg_8486" s="T293">mancher</ta>
            <ta e="T295" id="Seg_8487" s="T294">Hirte.[NOM]</ta>
            <ta e="T296" id="Seg_8488" s="T295">dieses</ta>
            <ta e="T297" id="Seg_8489" s="T296">gebären</ta>
            <ta e="T298" id="Seg_8490" s="T297">gebären-CVB.SIM=noch.nicht</ta>
            <ta e="T299" id="Seg_8491" s="T298">Rentier-ACC</ta>
            <ta e="T300" id="Seg_8492" s="T299">mitnehmen-PRS.[3SG]</ta>
            <ta e="T301" id="Seg_8493" s="T300">dieses</ta>
            <ta e="T302" id="Seg_8494" s="T301">mitnehmen-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T303" id="Seg_8495" s="T302">essen-EP-CAUS-CVB.SEQ</ta>
            <ta e="T304" id="Seg_8496" s="T303">Betrug-VBZ-CVB.SEQ</ta>
            <ta e="T305" id="Seg_8497" s="T304">geschützt</ta>
            <ta e="T306" id="Seg_8498" s="T305">Ort-DAT/LOC</ta>
            <ta e="T307" id="Seg_8499" s="T306">hineingehen-CAUS-CVB.SEQ</ta>
            <ta e="T308" id="Seg_8500" s="T307">Herde-3SG.[NOM]</ta>
            <ta e="T309" id="Seg_8501" s="T308">gehen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T310" id="Seg_8502" s="T309">zeigen-NEG.CVB.SIM</ta>
            <ta e="T311" id="Seg_8503" s="T310">dieses</ta>
            <ta e="T312" id="Seg_8504" s="T311">Kalb-PROPR</ta>
            <ta e="T314" id="Seg_8505" s="T312">Rentierkuh-ACC</ta>
            <ta e="T315" id="Seg_8506" s="T314">wann</ta>
            <ta e="T316" id="Seg_8507" s="T315">INDEF</ta>
            <ta e="T317" id="Seg_8508" s="T316">Rentier.[NOM]</ta>
            <ta e="T318" id="Seg_8509" s="T317">bedauern-RECP/COLL-CVB.SEQ</ta>
            <ta e="T319" id="Seg_8510" s="T318">Rentierkalb-3SG-ACC</ta>
            <ta e="T320" id="Seg_8511" s="T319">lassen-HAB.[3SG]</ta>
            <ta e="T321" id="Seg_8512" s="T320">folgen-CVB.SEQ</ta>
            <ta e="T322" id="Seg_8513" s="T321">Herde-3SG-DAT/LOC</ta>
            <ta e="T323" id="Seg_8514" s="T322">wählen-EP-RECP/COLL-CVB.SEQ</ta>
            <ta e="T324" id="Seg_8515" s="T323">jenes-ACC</ta>
            <ta e="T325" id="Seg_8516" s="T324">essen-EP-CAUS-CVB.SIM</ta>
            <ta e="T326" id="Seg_8517" s="T325">gehen-CVB.SEQ</ta>
            <ta e="T327" id="Seg_8518" s="T326">sich.satt.essen-PTCP.PST-3SG-ACC</ta>
            <ta e="T328" id="Seg_8519" s="T327">wiederkäuen-CVB.SIM</ta>
            <ta e="T329" id="Seg_8520" s="T328">liegen-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T330" id="Seg_8521" s="T329">gebären-CVB.SIM=noch.nicht</ta>
            <ta e="T331" id="Seg_8522" s="T330">Rentier-3SG-ACC</ta>
            <ta e="T332" id="Seg_8523" s="T331">Vorderteil.[NOM]</ta>
            <ta e="T333" id="Seg_8524" s="T332">zu</ta>
            <ta e="T334" id="Seg_8525" s="T333">überführen-CVB.SEQ</ta>
            <ta e="T335" id="Seg_8526" s="T334">mitnehmen-PRS.[3SG]</ta>
            <ta e="T336" id="Seg_8527" s="T335">jenes</ta>
            <ta e="T337" id="Seg_8528" s="T336">überführen-TEMP-3PL</ta>
            <ta e="T338" id="Seg_8529" s="T337">mancher</ta>
            <ta e="T339" id="Seg_8530" s="T338">Hirte-PL.[NOM]</ta>
            <ta e="T340" id="Seg_8531" s="T339">sehr</ta>
            <ta e="T341" id="Seg_8532" s="T340">fern.[NOM]</ta>
            <ta e="T342" id="Seg_8533" s="T341">gehen-HAB-3PL</ta>
            <ta e="T343" id="Seg_8534" s="T342">dieses</ta>
            <ta e="T344" id="Seg_8535" s="T343">fern.[NOM]</ta>
            <ta e="T345" id="Seg_8536" s="T344">gehen-TEMP-3PL</ta>
            <ta e="T346" id="Seg_8537" s="T345">dieses</ta>
            <ta e="T347" id="Seg_8538" s="T346">Rentierkalb.[NOM]</ta>
            <ta e="T348" id="Seg_8539" s="T347">nächster.Morgen-3SG-ACC</ta>
            <ta e="T349" id="Seg_8540" s="T348">jenes</ta>
            <ta e="T350" id="Seg_8541" s="T349">gebären-CVB.SIM=noch.nicht</ta>
            <ta e="T351" id="Seg_8542" s="T350">Rentier-PROPR-DAT/LOC</ta>
            <ta e="T352" id="Seg_8543" s="T351">Hinterteil-ADJZ-3SG-ACC</ta>
            <ta e="T353" id="Seg_8544" s="T352">Hirte-3PL-ACC</ta>
            <ta e="T354" id="Seg_8545" s="T353">jenes</ta>
            <ta e="T355" id="Seg_8546" s="T354">dieses-ACC</ta>
            <ta e="T356" id="Seg_8547" s="T355">gebären-PTCP.PST</ta>
            <ta e="T357" id="Seg_8548" s="T356">Rentierkuh-PROPR-3PL-ACC</ta>
            <ta e="T358" id="Seg_8549" s="T357">Hirte-3SG.[NOM]</ta>
            <ta e="T359" id="Seg_8550" s="T358">Vorderteil.[NOM]</ta>
            <ta e="T360" id="Seg_8551" s="T359">zu</ta>
            <ta e="T361" id="Seg_8552" s="T360">Hirte-PL-DAT/LOC</ta>
            <ta e="T362" id="Seg_8553" s="T361">mitnehmen-CVB.PURP-3PL</ta>
            <ta e="T363" id="Seg_8554" s="T362">nomadisieren-PTCP.PRS-3PL-DAT/LOC</ta>
            <ta e="T364" id="Seg_8555" s="T363">Rentier.[NOM]</ta>
            <ta e="T365" id="Seg_8556" s="T364">schaffen-CVB.SEQ</ta>
            <ta e="T366" id="Seg_8557" s="T365">ankommen-PTCP.HAB-3SG</ta>
            <ta e="T367" id="Seg_8558" s="T366">NEG.[3SG]</ta>
            <ta e="T368" id="Seg_8559" s="T367">dieses.[NOM]</ta>
            <ta e="T369" id="Seg_8560" s="T368">jenes.[NOM]</ta>
            <ta e="T370" id="Seg_8561" s="T369">wegen</ta>
            <ta e="T371" id="Seg_8562" s="T370">nah.[NOM]</ta>
            <ta e="T372" id="Seg_8563" s="T371">fallen-PTCP.FUT-DAT/LOC</ta>
            <ta e="T373" id="Seg_8564" s="T372">sehen-CVB.SEQ</ta>
            <ta e="T374" id="Seg_8565" s="T373">nachdem</ta>
            <ta e="T375" id="Seg_8566" s="T374">dann</ta>
            <ta e="T376" id="Seg_8567" s="T375">aber</ta>
            <ta e="T377" id="Seg_8568" s="T376">dieses</ta>
            <ta e="T378" id="Seg_8569" s="T377">Hirte-PL.[NOM]</ta>
            <ta e="T379" id="Seg_8570" s="T378">wann</ta>
            <ta e="T380" id="Seg_8571" s="T379">und</ta>
            <ta e="T381" id="Seg_8572" s="T380">Schutz-PROPR</ta>
            <ta e="T382" id="Seg_8573" s="T381">Ort-EP-INSTR</ta>
            <ta e="T383" id="Seg_8574" s="T382">fahren-PRS-3PL</ta>
            <ta e="T384" id="Seg_8575" s="T383">Strecke.[NOM]</ta>
            <ta e="T385" id="Seg_8576" s="T384">sehen-PRS-3PL</ta>
            <ta e="T386" id="Seg_8577" s="T385">Rentier.[NOM]</ta>
            <ta e="T387" id="Seg_8578" s="T386">gebären-EP-CAUS-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T388" id="Seg_8579" s="T387">Schutz-PROPR</ta>
            <ta e="T389" id="Seg_8580" s="T388">Erde-EP-INSTR</ta>
            <ta e="T390" id="Seg_8581" s="T389">Wald-PROPR</ta>
            <ta e="T391" id="Seg_8582" s="T390">Ort-EP-INSTR</ta>
            <ta e="T392" id="Seg_8583" s="T391">Berg-PL-PROPR</ta>
            <ta e="T393" id="Seg_8584" s="T392">Ort-PL-EP-INSTR</ta>
            <ta e="T394" id="Seg_8585" s="T393">Fluss-PL-PROPR</ta>
            <ta e="T395" id="Seg_8586" s="T394">Ort-PL-EP-INSTR</ta>
            <ta e="T396" id="Seg_8587" s="T395">dieses</ta>
            <ta e="T397" id="Seg_8588" s="T396">groß</ta>
            <ta e="T398" id="Seg_8589" s="T397">Schneewehe.[NOM]</ta>
            <ta e="T399" id="Seg_8590" s="T398">zu.Ende.gehen-HAB.[3SG]</ta>
            <ta e="T400" id="Seg_8591" s="T399">dieses</ta>
            <ta e="T401" id="Seg_8592" s="T400">abreißen-TEMP-3SG</ta>
            <ta e="T402" id="Seg_8593" s="T401">im.Frühling</ta>
            <ta e="T403" id="Seg_8594" s="T402">Rentier.[NOM]</ta>
            <ta e="T404" id="Seg_8595" s="T403">gebären-PTCP.PRS-3SG-ACC</ta>
            <ta e="T405" id="Seg_8596" s="T404">als</ta>
            <ta e="T406" id="Seg_8597" s="T405">Tag.[NOM]</ta>
            <ta e="T407" id="Seg_8598" s="T406">hell.werden-TEMP-3SG</ta>
            <ta e="T408" id="Seg_8599" s="T407">im.Winter</ta>
            <ta e="T409" id="Seg_8600" s="T408">abreißen-EP-PTCP.PST</ta>
            <ta e="T410" id="Seg_8601" s="T409">Schneewehe.[NOM]</ta>
            <ta e="T411" id="Seg_8602" s="T410">öffnen-HAB.[3SG]</ta>
            <ta e="T412" id="Seg_8603" s="T411">jenes</ta>
            <ta e="T413" id="Seg_8604" s="T412">Name-3SG.[NOM]</ta>
            <ta e="T414" id="Seg_8605" s="T413">Abgrund.[NOM]</ta>
            <ta e="T415" id="Seg_8606" s="T414">sein-PRS.[3SG]</ta>
            <ta e="T416" id="Seg_8607" s="T415">Rentier.[NOM]</ta>
            <ta e="T417" id="Seg_8608" s="T416">Nachricht-POSS</ta>
            <ta e="T418" id="Seg_8609" s="T417">NEG</ta>
            <ta e="T419" id="Seg_8610" s="T418">verlieren-CVB.SIM</ta>
            <ta e="T420" id="Seg_8611" s="T419">jenes</ta>
            <ta e="T421" id="Seg_8612" s="T420">Abgrund.[NOM]</ta>
            <ta e="T422" id="Seg_8613" s="T421">Inneres-3SG-DAT/LOC</ta>
            <ta e="T423" id="Seg_8614" s="T422">fallen-PTCP.PST</ta>
            <ta e="T424" id="Seg_8615" s="T423">Rentier-ACC</ta>
            <ta e="T425" id="Seg_8616" s="T424">dieses</ta>
            <ta e="T426" id="Seg_8617" s="T425">Hirte.[NOM]</ta>
            <ta e="T427" id="Seg_8618" s="T426">hüten-CVB.SIM</ta>
            <ta e="T428" id="Seg_8619" s="T427">gehen-CVB.SEQ</ta>
            <ta e="T429" id="Seg_8620" s="T428">bemerken-NEG.[3SG]</ta>
            <ta e="T430" id="Seg_8621" s="T429">wie.viel</ta>
            <ta e="T431" id="Seg_8622" s="T430">INDEF</ta>
            <ta e="T432" id="Seg_8623" s="T431">Rentier.[NOM]</ta>
            <ta e="T433" id="Seg_8624" s="T432">fallen-PTCP.PST-3SG-ACC</ta>
            <ta e="T434" id="Seg_8625" s="T433">EMPH-dann</ta>
            <ta e="T435" id="Seg_8626" s="T434">vermissen-NEG.[3SG]</ta>
            <ta e="T436" id="Seg_8627" s="T435">Rentier.[NOM]</ta>
            <ta e="T437" id="Seg_8628" s="T436">Nachricht-POSS</ta>
            <ta e="T438" id="Seg_8629" s="T437">NEG</ta>
            <ta e="T439" id="Seg_8630" s="T438">verlorengehen-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T440" id="Seg_8631" s="T439">jenes</ta>
            <ta e="T441" id="Seg_8632" s="T440">so</ta>
            <ta e="T442" id="Seg_8633" s="T441">Brigadeleiter-PL.[NOM]</ta>
            <ta e="T443" id="Seg_8634" s="T442">vielleicht</ta>
            <ta e="T444" id="Seg_8635" s="T443">Hirte-PL.[NOM]</ta>
            <ta e="T445" id="Seg_8636" s="T444">vielleicht</ta>
            <ta e="T446" id="Seg_8637" s="T445">im.Frühling</ta>
            <ta e="T447" id="Seg_8638" s="T446">Rentier.[NOM]</ta>
            <ta e="T448" id="Seg_8639" s="T447">gebären-PTCP.PRS-3SG-ACC</ta>
            <ta e="T449" id="Seg_8640" s="T448">als</ta>
            <ta e="T451" id="Seg_8641" s="T450">Schnee.[NOM]</ta>
            <ta e="T452" id="Seg_8642" s="T451">schmelzen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T453" id="Seg_8643" s="T452">als</ta>
            <ta e="T454" id="Seg_8644" s="T453">gut-ADVZ</ta>
            <ta e="T455" id="Seg_8645" s="T454">Ort-3PL-ACC</ta>
            <ta e="T456" id="Seg_8646" s="T455">sehen-PTCP.FUT-3PL-ACC</ta>
            <ta e="T457" id="Seg_8647" s="T456">man.muss</ta>
            <ta e="T458" id="Seg_8648" s="T457">Schneewehe.[NOM]</ta>
            <ta e="T459" id="Seg_8649" s="T458">zerreißen-EP-PTCP.PST-3SG.[NOM]</ta>
            <ta e="T460" id="Seg_8650" s="T459">Schneesturm.[NOM]</ta>
            <ta e="T461" id="Seg_8651" s="T460">sein-TEMP-3SG</ta>
            <ta e="T462" id="Seg_8652" s="T461">stöbern-EP-PASS/REFL-CVB.SEQ</ta>
            <ta e="T463" id="Seg_8653" s="T462">bleiben-HAB.[3SG]</ta>
            <ta e="T464" id="Seg_8654" s="T463">jenes</ta>
            <ta e="T465" id="Seg_8655" s="T464">machen-CVB.SEQ</ta>
            <ta e="T466" id="Seg_8656" s="T465">nachdem</ta>
            <ta e="T467" id="Seg_8657" s="T466">jenes</ta>
            <ta e="T468" id="Seg_8658" s="T467">stöbern-EP-PASS/REFL-EP-PTCP.PST-3SG-ACC</ta>
            <ta e="T469" id="Seg_8659" s="T468">nachdem</ta>
            <ta e="T470" id="Seg_8660" s="T469">jenes-ACC</ta>
            <ta e="T471" id="Seg_8661" s="T470">Rentier.[NOM]</ta>
            <ta e="T472" id="Seg_8662" s="T471">sehen-NEG.[3SG]</ta>
            <ta e="T473" id="Seg_8663" s="T472">EMPH-dann</ta>
            <ta e="T474" id="Seg_8664" s="T473">fallen-PRS.[3SG]</ta>
            <ta e="T475" id="Seg_8665" s="T474">jenes</ta>
            <ta e="T476" id="Seg_8666" s="T475">fallen-TEMP-3SG</ta>
            <ta e="T477" id="Seg_8667" s="T476">fünf-vier</ta>
            <ta e="T478" id="Seg_8668" s="T477">Meter-PROPR</ta>
            <ta e="T479" id="Seg_8669" s="T478">Tiefe-PROPR.[NOM]</ta>
            <ta e="T480" id="Seg_8670" s="T479">sein-HAB.[3SG]</ta>
            <ta e="T481" id="Seg_8671" s="T480">dieses</ta>
            <ta e="T482" id="Seg_8672" s="T481">Abgrund.[NOM]</ta>
            <ta e="T483" id="Seg_8673" s="T482">Schnee.[NOM]</ta>
            <ta e="T484" id="Seg_8674" s="T483">abreißen-EP-PST2.[3SG]</ta>
            <ta e="T485" id="Seg_8675" s="T484">jenes.[NOM]</ta>
            <ta e="T486" id="Seg_8676" s="T485">wegen</ta>
            <ta e="T487" id="Seg_8677" s="T486">solch-ACC.[NOM]</ta>
            <ta e="T488" id="Seg_8678" s="T487">finden-NEG.CVB.SIM</ta>
            <ta e="T489" id="Seg_8679" s="T488">Rentier.[NOM]</ta>
            <ta e="T490" id="Seg_8680" s="T489">Nachricht-POSS</ta>
            <ta e="T491" id="Seg_8681" s="T490">NEG</ta>
            <ta e="T492" id="Seg_8682" s="T491">verlorengehen-PRS.[3SG]</ta>
            <ta e="T493" id="Seg_8683" s="T492">dieses.[NOM]</ta>
            <ta e="T494" id="Seg_8684" s="T493">dann</ta>
            <ta e="T495" id="Seg_8685" s="T494">Fluss-PROPR</ta>
            <ta e="T496" id="Seg_8686" s="T495">Ort-PL-DAT/LOC</ta>
            <ta e="T497" id="Seg_8687" s="T496">aber</ta>
            <ta e="T498" id="Seg_8688" s="T497">Loch.in.der.Schneedecke-VBZ-TEMP-3SG</ta>
            <ta e="T499" id="Seg_8689" s="T498">wann</ta>
            <ta e="T500" id="Seg_8690" s="T499">NEG</ta>
            <ta e="T501" id="Seg_8691" s="T500">Fluss-PROPR</ta>
            <ta e="T502" id="Seg_8692" s="T501">Erde-EP-INSTR</ta>
            <ta e="T503" id="Seg_8693" s="T502">sein-PRS.[3SG]</ta>
            <ta e="T504" id="Seg_8694" s="T503">Strecke.[NOM]</ta>
            <ta e="T505" id="Seg_8695" s="T504">dieses</ta>
            <ta e="T506" id="Seg_8696" s="T505">Schnee-ACC</ta>
            <ta e="T507" id="Seg_8697" s="T506">mit</ta>
            <ta e="T508" id="Seg_8698" s="T507">Berg.[NOM]</ta>
            <ta e="T509" id="Seg_8699" s="T508">Erde.[NOM]</ta>
            <ta e="T510" id="Seg_8700" s="T509">Zwischenraum-3SG.[NOM]</ta>
            <ta e="T511" id="Seg_8701" s="T510">öffnen-HAB.[3SG]</ta>
            <ta e="T512" id="Seg_8702" s="T511">auch</ta>
            <ta e="T513" id="Seg_8703" s="T512">so</ta>
            <ta e="T514" id="Seg_8704" s="T513">auch</ta>
            <ta e="T515" id="Seg_8705" s="T514">fallen-HAB.[3SG]</ta>
            <ta e="T516" id="Seg_8706" s="T515">Rentierkalb.[NOM]</ta>
            <ta e="T517" id="Seg_8707" s="T516">vielleicht</ta>
            <ta e="T518" id="Seg_8708" s="T517">groß</ta>
            <ta e="T519" id="Seg_8709" s="T518">Rentier.[NOM]</ta>
            <ta e="T520" id="Seg_8710" s="T519">vielleicht</ta>
            <ta e="T521" id="Seg_8711" s="T520">dann</ta>
            <ta e="T522" id="Seg_8712" s="T521">dieses</ta>
            <ta e="T523" id="Seg_8713" s="T522">Hirte-PL.[NOM]</ta>
            <ta e="T524" id="Seg_8714" s="T523">Frühling</ta>
            <ta e="T525" id="Seg_8715" s="T524">Sommer-3PL.[NOM]</ta>
            <ta e="T526" id="Seg_8716" s="T525">sich.nähern-TEMP-3SG</ta>
            <ta e="T527" id="Seg_8717" s="T526">Frühling-ADJZ-3PL-DAT/LOC</ta>
            <ta e="T528" id="Seg_8718" s="T527">Reitrentier-3PL-ACC</ta>
            <ta e="T529" id="Seg_8719" s="T528">können-CVB.SEQ</ta>
            <ta e="T530" id="Seg_8720" s="T529">sich.kümmern-MED-NEG-3PL</ta>
            <ta e="T531" id="Seg_8721" s="T530">Pferdebremse-3SG-ACC</ta>
            <ta e="T532" id="Seg_8722" s="T531">abreißen-NEG-3PL</ta>
            <ta e="T533" id="Seg_8723" s="T532">Frühling.[NOM]</ta>
            <ta e="T534" id="Seg_8724" s="T533">Pferdebremse-3SG-ACC</ta>
            <ta e="T535" id="Seg_8725" s="T534">abreißen-TEMP-3PL</ta>
            <ta e="T536" id="Seg_8726" s="T535">jenes</ta>
            <ta e="T537" id="Seg_8727" s="T536">Pferdebremse-3SG-ACC</ta>
            <ta e="T538" id="Seg_8728" s="T537">abreißen-TEMP-3PL</ta>
            <ta e="T539" id="Seg_8729" s="T538">dieses</ta>
            <ta e="T540" id="Seg_8730" s="T539">Pferdebremse.[NOM]</ta>
            <ta e="T541" id="Seg_8731" s="T540">gesund.werden-PRS.[3SG]</ta>
            <ta e="T542" id="Seg_8732" s="T541">selbst-3SG.[NOM]</ta>
            <ta e="T543" id="Seg_8733" s="T542">Pferdebremse.[NOM]</ta>
            <ta e="T544" id="Seg_8734" s="T543">im.Frühling</ta>
            <ta e="T545" id="Seg_8735" s="T544">wachsen-CVB.SEQ</ta>
            <ta e="T546" id="Seg_8736" s="T545">hinausgehen-TEMP-3SG</ta>
            <ta e="T547" id="Seg_8737" s="T546">Haut-3SG-GEN</ta>
            <ta e="T548" id="Seg_8738" s="T547">Inneres-3SG-ABL</ta>
            <ta e="T549" id="Seg_8739" s="T548">selbst-3SG.[NOM]</ta>
            <ta e="T550" id="Seg_8740" s="T549">sich.abtrennen-CVB.SEQ</ta>
            <ta e="T551" id="Seg_8741" s="T550">fallen-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T552" id="Seg_8742" s="T551">bis.zu</ta>
            <ta e="T553" id="Seg_8743" s="T552">gehen-TEMP-3SG</ta>
            <ta e="T554" id="Seg_8744" s="T553">Rentier-DAT/LOC</ta>
            <ta e="T555" id="Seg_8745" s="T554">und</ta>
            <ta e="T556" id="Seg_8746" s="T555">schlecht.[NOM]</ta>
            <ta e="T557" id="Seg_8747" s="T556">mancher</ta>
            <ta e="T558" id="Seg_8748" s="T557">Pferdebremse-ACC</ta>
            <ta e="T559" id="Seg_8749" s="T558">vollständig-SIM</ta>
            <ta e="T560" id="Seg_8750" s="T559">besteigen-CVB.SIM-besteigen-CVB.SIM-3PL</ta>
            <ta e="T561" id="Seg_8751" s="T560">Rentier.[NOM]</ta>
            <ta e="T562" id="Seg_8752" s="T561">eigen-3SG</ta>
            <ta e="T563" id="Seg_8753" s="T562">sterben-PTCP.PRS</ta>
            <ta e="T564" id="Seg_8754" s="T563">kriechen-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T565" id="Seg_8755" s="T564">jenes</ta>
            <ta e="T566" id="Seg_8756" s="T565">mancher</ta>
            <ta e="T567" id="Seg_8757" s="T566">Hirte.[NOM]</ta>
            <ta e="T568" id="Seg_8758" s="T567">Rentier-ACC</ta>
            <ta e="T569" id="Seg_8759" s="T568">und</ta>
            <ta e="T570" id="Seg_8760" s="T569">einspannen-CVB.SIM</ta>
            <ta e="T571" id="Seg_8761" s="T570">können-NEG.[3SG]</ta>
            <ta e="T572" id="Seg_8762" s="T571">Zügel-3PL.[NOM]</ta>
            <ta e="T573" id="Seg_8763" s="T572">abreißen-PST1-3SG</ta>
            <ta e="T574" id="Seg_8764" s="T573">Rentier.[NOM]</ta>
            <ta e="T575" id="Seg_8765" s="T574">eigen-3SG-GEN</ta>
            <ta e="T576" id="Seg_8766" s="T575">Leiste-3SG-ACC</ta>
            <ta e="T577" id="Seg_8767" s="T576">zerschlagen-PRS-3PL</ta>
            <ta e="T578" id="Seg_8768" s="T577">Leiste-3SG-ACC</ta>
            <ta e="T579" id="Seg_8769" s="T578">zerschlagen-TEMP-3PL</ta>
            <ta e="T580" id="Seg_8770" s="T579">dieses</ta>
            <ta e="T581" id="Seg_8771" s="T580">im.Sommer</ta>
            <ta e="T582" id="Seg_8772" s="T581">Wunde.[NOM]</ta>
            <ta e="T583" id="Seg_8773" s="T582">sein-HAB.[3SG]</ta>
            <ta e="T584" id="Seg_8774" s="T583">dieses-INSTR</ta>
            <ta e="T585" id="Seg_8775" s="T584">Rentier.[NOM]</ta>
            <ta e="T587" id="Seg_8776" s="T586">krank.werden-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T588" id="Seg_8777" s="T587">dieses.[NOM]</ta>
            <ta e="T589" id="Seg_8778" s="T588">dann</ta>
            <ta e="T590" id="Seg_8779" s="T589">Schwanz-3SG-ACC</ta>
            <ta e="T591" id="Seg_8780" s="T590">Rentier.[NOM]</ta>
            <ta e="T592" id="Seg_8781" s="T591">eigen-3SG-ACC</ta>
            <ta e="T593" id="Seg_8782" s="T592">spalten-CVB.SIM</ta>
            <ta e="T594" id="Seg_8783" s="T593">stoßen-PRS-3PL</ta>
            <ta e="T595" id="Seg_8784" s="T594">dieses</ta>
            <ta e="T596" id="Seg_8785" s="T595">Schwanz-3SG-ACC</ta>
            <ta e="T597" id="Seg_8786" s="T596">spalten-CVB.SIM</ta>
            <ta e="T598" id="Seg_8787" s="T597">stoßen-TEMP-3PL</ta>
            <ta e="T599" id="Seg_8788" s="T598">im.Winter</ta>
            <ta e="T600" id="Seg_8789" s="T599">Kälte-DAT/LOC</ta>
            <ta e="T601" id="Seg_8790" s="T600">dieses</ta>
            <ta e="T602" id="Seg_8791" s="T601">Rentier.[NOM]</ta>
            <ta e="T603" id="Seg_8792" s="T602">eigen-3SG.[NOM]</ta>
            <ta e="T604" id="Seg_8793" s="T603">Fleisch-3SG.[NOM]</ta>
            <ta e="T605" id="Seg_8794" s="T604">wann</ta>
            <ta e="T606" id="Seg_8795" s="T605">NEG</ta>
            <ta e="T607" id="Seg_8796" s="T606">wieder</ta>
            <ta e="T608" id="Seg_8797" s="T607">sich.erheben-NEG.[3SG]</ta>
            <ta e="T609" id="Seg_8798" s="T608">dann</ta>
            <ta e="T610" id="Seg_8799" s="T609">im.Sommer</ta>
            <ta e="T611" id="Seg_8800" s="T610">warm.[NOM]</ta>
            <ta e="T612" id="Seg_8801" s="T611">als</ta>
            <ta e="T613" id="Seg_8802" s="T612">nomadisieren-PRS-3PL</ta>
            <ta e="T614" id="Seg_8803" s="T613">dieses</ta>
            <ta e="T615" id="Seg_8804" s="T614">nomadisieren-TEMP-3PL</ta>
            <ta e="T616" id="Seg_8805" s="T615">Wärme-DAT/LOC</ta>
            <ta e="T617" id="Seg_8806" s="T616">nomadisieren-TEMP-3PL</ta>
            <ta e="T618" id="Seg_8807" s="T617">nomadisieren-CVB.SEQ</ta>
            <ta e="T619" id="Seg_8808" s="T618">gehen-CVB.SEQ-3PL</ta>
            <ta e="T620" id="Seg_8809" s="T619">Fluss-PROPR</ta>
            <ta e="T621" id="Seg_8810" s="T620">Ort-PL-EP-INSTR</ta>
            <ta e="T622" id="Seg_8811" s="T621">gehen-PRS-3PL</ta>
            <ta e="T623" id="Seg_8812" s="T622">dieses</ta>
            <ta e="T624" id="Seg_8813" s="T623">Fluss-PROPR</ta>
            <ta e="T625" id="Seg_8814" s="T624">groß</ta>
            <ta e="T626" id="Seg_8815" s="T625">Berg-PL-DAT/LOC</ta>
            <ta e="T627" id="Seg_8816" s="T626">Lehm-PROPR.[NOM]</ta>
            <ta e="T628" id="Seg_8817" s="T627">sein-HAB.[3SG]</ta>
            <ta e="T629" id="Seg_8818" s="T628">wie.viel</ta>
            <ta e="T630" id="Seg_8819" s="T629">und</ta>
            <ta e="T631" id="Seg_8820" s="T630">Wärme-DAT/LOC</ta>
            <ta e="T632" id="Seg_8821" s="T631">dieses</ta>
            <ta e="T633" id="Seg_8822" s="T632">Lehm-ACC</ta>
            <ta e="T634" id="Seg_8823" s="T633">erkennen-PST1-3SG</ta>
            <ta e="T635" id="Seg_8824" s="T634">und</ta>
            <ta e="T636" id="Seg_8825" s="T635">Rentier.[NOM]</ta>
            <ta e="T637" id="Seg_8826" s="T636">dieses</ta>
            <ta e="T638" id="Seg_8827" s="T637">Berg-DAT/LOC</ta>
            <ta e="T639" id="Seg_8828" s="T638">gehen-HAB.[3SG]</ta>
            <ta e="T640" id="Seg_8829" s="T639">nomadisieren-CVB.SEQ</ta>
            <ta e="T641" id="Seg_8830" s="T640">gehen-PTCP.COND-DAT/LOC</ta>
            <ta e="T642" id="Seg_8831" s="T641">und</ta>
            <ta e="T643" id="Seg_8832" s="T642">jagen-CVB.SEQ</ta>
            <ta e="T644" id="Seg_8833" s="T643">und</ta>
            <ta e="T645" id="Seg_8834" s="T644">gehen-TEMP-3PL</ta>
            <ta e="T646" id="Seg_8835" s="T645">dieses-ACC</ta>
            <ta e="T647" id="Seg_8836" s="T646">Hirte-PL.[NOM]</ta>
            <ta e="T648" id="Seg_8837" s="T647">dieses</ta>
            <ta e="T649" id="Seg_8838" s="T648">Berg-ACC</ta>
            <ta e="T650" id="Seg_8839" s="T649">sehen-NEG.CVB.SIM</ta>
            <ta e="T651" id="Seg_8840" s="T650">Hund-VBZ-CVB.SEQ</ta>
            <ta e="T652" id="Seg_8841" s="T651">nehmen-PST1-3PL</ta>
            <ta e="T653" id="Seg_8842" s="T652">und</ta>
            <ta e="T654" id="Seg_8843" s="T653">EMPH-dann</ta>
            <ta e="T655" id="Seg_8844" s="T654">lachen-RECP/COLL-CVB.SIM-lachen-RECP/COLL-CVB.SIM</ta>
            <ta e="T656" id="Seg_8845" s="T655">vorbeigehen-CVB.SEQ</ta>
            <ta e="T657" id="Seg_8846" s="T656">bleiben-PRS-3PL</ta>
            <ta e="T658" id="Seg_8847" s="T657">hier</ta>
            <ta e="T659" id="Seg_8848" s="T658">zehn</ta>
            <ta e="T660" id="Seg_8849" s="T659">Rest-3SG.[NOM]</ta>
            <ta e="T661" id="Seg_8850" s="T660">fünf</ta>
            <ta e="T662" id="Seg_8851" s="T661">MOD</ta>
            <ta e="T663" id="Seg_8852" s="T662">zwanzig</ta>
            <ta e="T664" id="Seg_8853" s="T663">Rest-3SG.[NOM]</ta>
            <ta e="T665" id="Seg_8854" s="T664">fünf</ta>
            <ta e="T666" id="Seg_8855" s="T665">MOD</ta>
            <ta e="T667" id="Seg_8856" s="T666">Rentier.[NOM]</ta>
            <ta e="T668" id="Seg_8857" s="T667">bleiben-PRS.[3SG]</ta>
            <ta e="T669" id="Seg_8858" s="T668">dieses</ta>
            <ta e="T670" id="Seg_8859" s="T669">bleiben-PTCP.PST-3SG-ACC</ta>
            <ta e="T671" id="Seg_8860" s="T670">bemerken-NEG-3PL</ta>
            <ta e="T672" id="Seg_8861" s="T671">vorübergehende.Siedlung-DAT/LOC</ta>
            <ta e="T673" id="Seg_8862" s="T672">fallen-CVB.SEQ-3PL</ta>
            <ta e="T674" id="Seg_8863" s="T673">nächster</ta>
            <ta e="T675" id="Seg_8864" s="T674">Mensch.[NOM]</ta>
            <ta e="T676" id="Seg_8865" s="T675">hüten-PRS.[3SG]</ta>
            <ta e="T677" id="Seg_8866" s="T676">vor.Kurzem</ta>
            <ta e="T678" id="Seg_8867" s="T677">dieses</ta>
            <ta e="T679" id="Seg_8868" s="T678">nomadisieren-PRS-3PL-DAT/LOC</ta>
            <ta e="T680" id="Seg_8869" s="T679">hüten-PTCP.PST</ta>
            <ta e="T681" id="Seg_8870" s="T680">Mensch.[NOM]</ta>
            <ta e="T682" id="Seg_8871" s="T681">nur</ta>
            <ta e="T683" id="Seg_8872" s="T682">Reitrentier-3SG.[NOM]</ta>
            <ta e="T684" id="Seg_8873" s="T683">jenes</ta>
            <ta e="T685" id="Seg_8874" s="T684">Rentier-PL-DAT/LOC</ta>
            <ta e="T686" id="Seg_8875" s="T685">dort</ta>
            <ta e="T687" id="Seg_8876" s="T686">Berg-DAT/LOC</ta>
            <ta e="T688" id="Seg_8877" s="T687">bleiben-PST2.[3SG]</ta>
            <ta e="T689" id="Seg_8878" s="T688">eins</ta>
            <ta e="T690" id="Seg_8879" s="T689">Reitrentier-3SG.[NOM]</ta>
            <ta e="T691" id="Seg_8880" s="T690">nächster.Morgen-3SG-ACC</ta>
            <ta e="T692" id="Seg_8881" s="T691">nomadisieren-PRS-3PL</ta>
            <ta e="T693" id="Seg_8882" s="T692">wieder</ta>
            <ta e="T694" id="Seg_8883" s="T693">nomadisieren-CVB.SEQ</ta>
            <ta e="T695" id="Seg_8884" s="T694">fallen-CVB.SEQ</ta>
            <ta e="T696" id="Seg_8885" s="T695">nachdem</ta>
            <ta e="T697" id="Seg_8886" s="T696">nächster</ta>
            <ta e="T698" id="Seg_8887" s="T697">Mensch-PL.[NOM]</ta>
            <ta e="T699" id="Seg_8888" s="T698">hüten-PRS-3PL</ta>
            <ta e="T700" id="Seg_8889" s="T699">dieses</ta>
            <ta e="T701" id="Seg_8890" s="T700">entfernt</ta>
            <ta e="T702" id="Seg_8891" s="T701">vorübergehende.Siedlung-DAT/LOC</ta>
            <ta e="T703" id="Seg_8892" s="T702">hüten-PTCP.PST</ta>
            <ta e="T704" id="Seg_8893" s="T703">der.Reihe.nach</ta>
            <ta e="T705" id="Seg_8894" s="T704">Mensch.[NOM]</ta>
            <ta e="T706" id="Seg_8895" s="T705">hüten-CVB.SIM</ta>
            <ta e="T707" id="Seg_8896" s="T706">hinausgehen-CVB.SEQ</ta>
            <ta e="T708" id="Seg_8897" s="T707">Reitrentier-3SG-ACC</ta>
            <ta e="T709" id="Seg_8898" s="T708">dann</ta>
            <ta e="T710" id="Seg_8899" s="T709">vermissen-PRS.[3SG]</ta>
            <ta e="T711" id="Seg_8900" s="T710">Morgen</ta>
            <ta e="T712" id="Seg_8901" s="T711">kommen-CVB.SEQ</ta>
            <ta e="T713" id="Seg_8902" s="T712">sagen-PRS.[3SG]</ta>
            <ta e="T714" id="Seg_8903" s="T713">na</ta>
            <ta e="T715" id="Seg_8904" s="T714">mein</ta>
            <ta e="T716" id="Seg_8905" s="T715">Reitrentier-EP-1SG.[NOM]</ta>
            <ta e="T717" id="Seg_8906" s="T716">NEG.EX</ta>
            <ta e="T718" id="Seg_8907" s="T717">dort</ta>
            <ta e="T719" id="Seg_8908" s="T718">vorübergehende.Siedlung-DAT/LOC</ta>
            <ta e="T720" id="Seg_8909" s="T719">NEG</ta>
            <ta e="T721" id="Seg_8910" s="T720">sehen-PST2.NEG-EP-1SG</ta>
            <ta e="T722" id="Seg_8911" s="T721">kürzlich</ta>
            <ta e="T723" id="Seg_8912" s="T722">nomadisieren-PTCP.PST</ta>
            <ta e="T724" id="Seg_8913" s="T723">vorübergehende.Siedlung-1SG-DAT/LOC</ta>
            <ta e="T725" id="Seg_8914" s="T724">entfernt</ta>
            <ta e="T726" id="Seg_8915" s="T725">vorübergehende.Siedlung-DAT/LOC</ta>
            <ta e="T727" id="Seg_8916" s="T726">hüten-PTCP.PRS-1SG-DAT/LOC</ta>
            <ta e="T728" id="Seg_8917" s="T727">besteigen-CVB.SIM</ta>
            <ta e="T729" id="Seg_8918" s="T728">gehen-EP-PTCP.PST</ta>
            <ta e="T730" id="Seg_8919" s="T729">Rentier-1SG.[NOM]</ta>
            <ta e="T731" id="Seg_8920" s="T730">sein-PST1-3SG</ta>
            <ta e="T732" id="Seg_8921" s="T731">Wahrheit.[NOM]</ta>
            <ta e="T733" id="Seg_8922" s="T732">und</ta>
            <ta e="T734" id="Seg_8923" s="T733">dort</ta>
            <ta e="T735" id="Seg_8924" s="T734">vorübergehende.Siedlung-DAT/LOC</ta>
            <ta e="T736" id="Seg_8925" s="T735">bleiben-PST2.[3SG]</ta>
            <ta e="T737" id="Seg_8926" s="T736">MOD</ta>
            <ta e="T738" id="Seg_8927" s="T737">sagen-CVB.SIM</ta>
            <ta e="T739" id="Seg_8928" s="T738">sagen-PRS-3PL</ta>
            <ta e="T740" id="Seg_8929" s="T739">jenes</ta>
            <ta e="T741" id="Seg_8930" s="T740">Rentier-ACC</ta>
            <ta e="T742" id="Seg_8931" s="T741">dieses</ta>
            <ta e="T743" id="Seg_8932" s="T742">vorübergehende.Siedlung-3PL-DAT/LOC</ta>
            <ta e="T744" id="Seg_8933" s="T743">suchen-CVB.SIM</ta>
            <ta e="T745" id="Seg_8934" s="T744">kommen-PRS-3PL</ta>
            <ta e="T746" id="Seg_8935" s="T745">heute</ta>
            <ta e="T747" id="Seg_8936" s="T746">nomadisieren-PTCP.PST</ta>
            <ta e="T748" id="Seg_8937" s="T747">vorübergehende.Siedlung-3PL-DAT/LOC</ta>
            <ta e="T749" id="Seg_8938" s="T748">jenes</ta>
            <ta e="T750" id="Seg_8939" s="T749">Rentier-PL-ACC</ta>
            <ta e="T751" id="Seg_8940" s="T750">entfernt</ta>
            <ta e="T752" id="Seg_8941" s="T751">Rentierkarawane-DAT/LOC</ta>
            <ta e="T753" id="Seg_8942" s="T752">bleiben-PTCP.PST</ta>
            <ta e="T754" id="Seg_8943" s="T753">Rentier-PL-ACC</ta>
            <ta e="T755" id="Seg_8944" s="T754">jene.Seite.[NOM]</ta>
            <ta e="T756" id="Seg_8945" s="T755">vorübergehende.Siedlung-DAT/LOC</ta>
            <ta e="T757" id="Seg_8946" s="T756">suchen-PRS-3PL</ta>
            <ta e="T758" id="Seg_8947" s="T757">jenes</ta>
            <ta e="T759" id="Seg_8948" s="T758">entfernt</ta>
            <ta e="T760" id="Seg_8949" s="T759">vorübergehende.Siedlung-DAT/LOC</ta>
            <ta e="T761" id="Seg_8950" s="T760">hüten-PTCP.PST</ta>
            <ta e="T762" id="Seg_8951" s="T761">Hirte.[NOM]</ta>
            <ta e="T763" id="Seg_8952" s="T762">Reitrentier-3SG.[NOM]</ta>
            <ta e="T764" id="Seg_8953" s="T763">dort</ta>
            <ta e="T765" id="Seg_8954" s="T764">bleiben-PST2.[3SG]</ta>
            <ta e="T766" id="Seg_8955" s="T765">jenes</ta>
            <ta e="T767" id="Seg_8956" s="T766">Berg-DAT/LOC</ta>
            <ta e="T768" id="Seg_8957" s="T767">jenes</ta>
            <ta e="T769" id="Seg_8958" s="T768">einsam</ta>
            <ta e="T770" id="Seg_8959" s="T769">Rentier-ACC</ta>
            <ta e="T771" id="Seg_8960" s="T770">erinnern-PRS-3PL</ta>
            <ta e="T772" id="Seg_8961" s="T771">dort</ta>
            <ta e="T773" id="Seg_8962" s="T772">bleiben-PST2-3SG</ta>
            <ta e="T774" id="Seg_8963" s="T773">zehn-ABL</ta>
            <ta e="T775" id="Seg_8964" s="T774">über</ta>
            <ta e="T776" id="Seg_8965" s="T775">Rentier.[NOM]</ta>
            <ta e="T777" id="Seg_8966" s="T776">jenes.[NOM]</ta>
            <ta e="T778" id="Seg_8967" s="T777">wegen</ta>
            <ta e="T781" id="Seg_8968" s="T780">Rentier.[NOM]</ta>
            <ta e="T782" id="Seg_8969" s="T781">Nachricht-POSS</ta>
            <ta e="T783" id="Seg_8970" s="T782">NEG</ta>
            <ta e="T784" id="Seg_8971" s="T783">verlorengehen-PRS-3SG.[NOM]</ta>
            <ta e="T785" id="Seg_8972" s="T784">dieses.[NOM]</ta>
            <ta e="T786" id="Seg_8973" s="T785">dann</ta>
            <ta e="T787" id="Seg_8974" s="T786">Jahr.[NOM]</ta>
            <ta e="T788" id="Seg_8975" s="T787">unterschiedlich-unterschiedlich.[NOM]</ta>
            <ta e="T789" id="Seg_8976" s="T788">sein-HAB.[3SG]</ta>
            <ta e="T790" id="Seg_8977" s="T789">warm-VBZ-PTCP.FUT</ta>
            <ta e="T791" id="Seg_8978" s="T790">sein-HAB.[3SG]</ta>
            <ta e="T792" id="Seg_8979" s="T791">dieses</ta>
            <ta e="T793" id="Seg_8980" s="T792">Wärme-DAT/LOC</ta>
            <ta e="T794" id="Seg_8981" s="T793">wieder</ta>
            <ta e="T795" id="Seg_8982" s="T794">jetzt-ADJZ</ta>
            <ta e="T796" id="Seg_8983" s="T795">Hirte-PL.[NOM]</ta>
            <ta e="T797" id="Seg_8984" s="T796">Rentier-ACC</ta>
            <ta e="T798" id="Seg_8985" s="T797">können-CVB.SEQ</ta>
            <ta e="T799" id="Seg_8986" s="T798">sich.kümmern-NEG-3PL</ta>
            <ta e="T800" id="Seg_8987" s="T799">sich.kümmern-NEG-3PL</ta>
            <ta e="T801" id="Seg_8988" s="T800">was-ACC=Q</ta>
            <ta e="T802" id="Seg_8989" s="T801">Wärme-DAT/LOC</ta>
            <ta e="T803" id="Seg_8990" s="T802">Wind-AG.[NOM]</ta>
            <ta e="T804" id="Seg_8991" s="T803">Rentier.[NOM]</ta>
            <ta e="T805" id="Seg_8992" s="T804">sein-HAB.[3SG]</ta>
            <ta e="T806" id="Seg_8993" s="T805">Wind-AG.[NOM]</ta>
            <ta e="T807" id="Seg_8994" s="T806">Rentier.[NOM]</ta>
            <ta e="T808" id="Seg_8995" s="T807">Name-3SG.[NOM]</ta>
            <ta e="T809" id="Seg_8996" s="T808">Wind-ACC</ta>
            <ta e="T810" id="Seg_8997" s="T809">folgen-PRS.[3SG]</ta>
            <ta e="T811" id="Seg_8998" s="T810">dieses</ta>
            <ta e="T812" id="Seg_8999" s="T811">Wind-ACC</ta>
            <ta e="T813" id="Seg_9000" s="T812">folgen-PTCP.PRS</ta>
            <ta e="T814" id="Seg_9001" s="T813">Rentier-ACC</ta>
            <ta e="T815" id="Seg_9002" s="T814">Wärme.[NOM]</ta>
            <ta e="T816" id="Seg_9003" s="T815">vorbeigehen-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T817" id="Seg_9004" s="T816">bis.zu</ta>
            <ta e="T818" id="Seg_9005" s="T817">Mensch.[NOM]</ta>
            <ta e="T819" id="Seg_9006" s="T818">Rentier-ABL</ta>
            <ta e="T820" id="Seg_9007" s="T819">binden-HAB.[3SG]</ta>
            <ta e="T821" id="Seg_9008" s="T820">Rentier.[NOM]</ta>
            <ta e="T822" id="Seg_9009" s="T821">hüten-TEMP-3PL</ta>
            <ta e="T823" id="Seg_9010" s="T822">Mensch.[NOM]</ta>
            <ta e="T824" id="Seg_9011" s="T823">Kraft-3SG.[NOM]</ta>
            <ta e="T825" id="Seg_9012" s="T824">reichen-PTCP.HAB-3SG</ta>
            <ta e="T826" id="Seg_9013" s="T825">NEG.[3SG]</ta>
            <ta e="T827" id="Seg_9014" s="T826">Wärme-DAT/LOC</ta>
            <ta e="T828" id="Seg_9015" s="T827">Wind-ACC</ta>
            <ta e="T830" id="Seg_9016" s="T929">machen-NEG-3PL</ta>
            <ta e="T831" id="Seg_9017" s="T830">Morgen</ta>
            <ta e="T832" id="Seg_9018" s="T831">bringen-PTCP.PRS-3PL-DAT/LOC</ta>
            <ta e="T833" id="Seg_9019" s="T832">nächster</ta>
            <ta e="T834" id="Seg_9020" s="T833">Hirte-DAT/LOC</ta>
            <ta e="T835" id="Seg_9021" s="T834">überreichen-PTCP.PRS-3PL-DAT/LOC</ta>
            <ta e="T836" id="Seg_9022" s="T835">1PL.[NOM]</ta>
            <ta e="T837" id="Seg_9023" s="T836">können-FUT-1PL</ta>
            <ta e="T838" id="Seg_9024" s="T837">NEG-3SG</ta>
            <ta e="T839" id="Seg_9025" s="T838">EVID</ta>
            <ta e="T840" id="Seg_9026" s="T839">sagen-CVB.SEQ</ta>
            <ta e="T841" id="Seg_9027" s="T840">wann</ta>
            <ta e="T842" id="Seg_9028" s="T841">NEG</ta>
            <ta e="T843" id="Seg_9029" s="T842">Wind.[NOM]</ta>
            <ta e="T844" id="Seg_9030" s="T843">Unterteil.[NOM]</ta>
            <ta e="T845" id="Seg_9031" s="T844">zu</ta>
            <ta e="T846" id="Seg_9032" s="T845">schicken-NMNZ.[NOM]</ta>
            <ta e="T847" id="Seg_9033" s="T846">machen-PTCP.FUT-3PL-ACC</ta>
            <ta e="T848" id="Seg_9034" s="T847">Haus-3PL.[NOM]</ta>
            <ta e="T849" id="Seg_9035" s="T848">Wind.[NOM]</ta>
            <ta e="T850" id="Seg_9036" s="T849">oberer.Teil.[NOM]</ta>
            <ta e="T851" id="Seg_9037" s="T850">dieses</ta>
            <ta e="T852" id="Seg_9038" s="T851">sein-PRS.[3SG]</ta>
            <ta e="T853" id="Seg_9039" s="T852">ähnlich</ta>
            <ta e="T854" id="Seg_9040" s="T853">Rentier.[NOM]</ta>
            <ta e="T855" id="Seg_9041" s="T854">dann</ta>
            <ta e="T856" id="Seg_9042" s="T855">können-NEG.CVB.SIM</ta>
            <ta e="T857" id="Seg_9043" s="T856">Wind.[NOM]</ta>
            <ta e="T858" id="Seg_9044" s="T857">entgegen</ta>
            <ta e="T859" id="Seg_9045" s="T858">kommen-TEMP-3SG</ta>
            <ta e="T860" id="Seg_9046" s="T859">Haus-3SG-DAT/LOC</ta>
            <ta e="T861" id="Seg_9047" s="T860">stoßen-CAUS-TEMP-3SG</ta>
            <ta e="T862" id="Seg_9048" s="T861">Haus-ADJZ-3PL.[NOM]</ta>
            <ta e="T863" id="Seg_9049" s="T862">aufstehen-CVB.SEQ-3PL</ta>
            <ta e="T864" id="Seg_9050" s="T863">hier</ta>
            <ta e="T865" id="Seg_9051" s="T864">helfen-CVB.SEQ-3PL</ta>
            <ta e="T866" id="Seg_9052" s="T865">dann</ta>
            <ta e="T867" id="Seg_9053" s="T866">siegen-PRS-3PL</ta>
            <ta e="T868" id="Seg_9054" s="T867">Rentier-ACC</ta>
            <ta e="T869" id="Seg_9055" s="T868">jenes.[NOM]</ta>
            <ta e="T870" id="Seg_9056" s="T869">nach</ta>
            <ta e="T871" id="Seg_9057" s="T870">3PL.[NOM]</ta>
            <ta e="T872" id="Seg_9058" s="T871">Frühling-ADJZ-ABL</ta>
            <ta e="T873" id="Seg_9059" s="T872">Wärme.[NOM]</ta>
            <ta e="T874" id="Seg_9060" s="T873">fallen-CVB.SIM</ta>
            <ta e="T875" id="Seg_9061" s="T874">noch.nicht-3SG</ta>
            <ta e="T876" id="Seg_9062" s="T875">soeben</ta>
            <ta e="T877" id="Seg_9063" s="T876">Wärme.[NOM]</ta>
            <ta e="T878" id="Seg_9064" s="T877">fallen-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T879" id="Seg_9065" s="T878">Rentier.[NOM]</ta>
            <ta e="T880" id="Seg_9066" s="T879">fühlen-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T881" id="Seg_9067" s="T880">Rauch-DAT/LOC</ta>
            <ta e="T882" id="Seg_9068" s="T881">Rentier-ACC</ta>
            <ta e="T883" id="Seg_9069" s="T882">lehren-PTCP.FUT-3PL-ACC</ta>
            <ta e="T884" id="Seg_9070" s="T883">man.muss</ta>
            <ta e="T885" id="Seg_9071" s="T884">groß</ta>
            <ta e="T886" id="Seg_9072" s="T885">Wärme.[NOM]</ta>
            <ta e="T887" id="Seg_9073" s="T886">fallen-CVB.SIM</ta>
            <ta e="T888" id="Seg_9074" s="T887">noch.nicht-3SG</ta>
            <ta e="T889" id="Seg_9075" s="T888">Rauch-ACC</ta>
            <ta e="T890" id="Seg_9076" s="T889">sehen-PST1-3SG</ta>
            <ta e="T891" id="Seg_9077" s="T890">und</ta>
            <ta e="T892" id="Seg_9078" s="T891">wie.viel</ta>
            <ta e="T893" id="Seg_9079" s="T892">und</ta>
            <ta e="T894" id="Seg_9080" s="T893">Wärme-DAT/LOC</ta>
            <ta e="T896" id="Seg_9081" s="T894">Rentier.[NOM]</ta>
            <ta e="T897" id="Seg_9082" s="T896">Haus-ADJZ-3PL.[NOM]</ta>
            <ta e="T898" id="Seg_9083" s="T897">Wärme-DAT/LOC</ta>
            <ta e="T899" id="Seg_9084" s="T898">Hirte-PL.[NOM]</ta>
            <ta e="T900" id="Seg_9085" s="T899">hüten-CVB.SIM</ta>
            <ta e="T901" id="Seg_9086" s="T900">gehen-CVB.PURP-3PL</ta>
            <ta e="T902" id="Seg_9087" s="T901">sprechen-PTCP.FUT</ta>
            <ta e="T903" id="Seg_9088" s="T902">fallen-NEC-3PL</ta>
            <ta e="T904" id="Seg_9089" s="T903">na</ta>
            <ta e="T905" id="Seg_9090" s="T904">Wärme.[NOM]</ta>
            <ta e="T906" id="Seg_9091" s="T905">sein-TEMP-3SG</ta>
            <ta e="T907" id="Seg_9092" s="T906">doch</ta>
            <ta e="T908" id="Seg_9093" s="T907">fürchten-CVB.SEQ</ta>
            <ta e="T909" id="Seg_9094" s="T908">schlafen-EP-IMP.2PL</ta>
            <ta e="T910" id="Seg_9095" s="T909">Rauch-VBZ-EP-CAUS-FUT-EP-IMP.2PL</ta>
            <ta e="T911" id="Seg_9096" s="T910">Lagerfeuer.[NOM]</ta>
            <ta e="T912" id="Seg_9097" s="T911">heizen-FUT-EP-IMP.2PL</ta>
            <ta e="T913" id="Seg_9098" s="T912">warm</ta>
            <ta e="T914" id="Seg_9099" s="T913">sein-TEMP-3SG</ta>
            <ta e="T915" id="Seg_9100" s="T914">vielleicht</ta>
            <ta e="T916" id="Seg_9101" s="T915">können-FUT-1PL</ta>
            <ta e="T917" id="Seg_9102" s="T916">NEG-3SG</ta>
            <ta e="T918" id="Seg_9103" s="T917">wie</ta>
            <ta e="T919" id="Seg_9104" s="T918">INDEF</ta>
            <ta e="T920" id="Seg_9105" s="T919">warm.[NOM]</ta>
            <ta e="T921" id="Seg_9106" s="T920">sein-CVB.SEQ</ta>
            <ta e="T922" id="Seg_9107" s="T921">sein-PRS.[3SG]</ta>
            <ta e="T923" id="Seg_9108" s="T922">jenes.[NOM]</ta>
            <ta e="T924" id="Seg_9109" s="T923">ähnlich</ta>
            <ta e="T925" id="Seg_9110" s="T924">sich.kümmern-PTCP.HAB</ta>
            <ta e="T926" id="Seg_9111" s="T925">sein-PST1-1PL</ta>
            <ta e="T927" id="Seg_9112" s="T926">1PL.[NOM]</ta>
            <ta e="T928" id="Seg_9113" s="T927">Rentier-ACC</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_9114" s="T0">вот</ta>
            <ta e="T2" id="Seg_9115" s="T1">1SG.[NOM]</ta>
            <ta e="T3" id="Seg_9116" s="T2">олень-AG-PL-DAT/LOC</ta>
            <ta e="T4" id="Seg_9117" s="T3">слышаться-EP-CAUS-CVB.PURP</ta>
            <ta e="T5" id="Seg_9118" s="T4">говорить-PRS-1SG</ta>
            <ta e="T6" id="Seg_9119" s="T5">1SG.[NOM]</ta>
            <ta e="T7" id="Seg_9120" s="T6">много-ADVZ</ta>
            <ta e="T8" id="Seg_9121" s="T7">работать-PTCP.PST</ta>
            <ta e="T9" id="Seg_9122" s="T8">человек-1SG</ta>
            <ta e="T10" id="Seg_9123" s="T9">олень-DAT/LOC</ta>
            <ta e="T11" id="Seg_9124" s="T10">раньше</ta>
            <ta e="T12" id="Seg_9125" s="T11">этот</ta>
            <ta e="T13" id="Seg_9126" s="T12">олень.[NOM]</ta>
            <ta e="T14" id="Seg_9127" s="T13">родиться-PTCP.PRS-3SG-GEN</ta>
            <ta e="T15" id="Seg_9128" s="T14">сторона-3SG-INSTR</ta>
            <ta e="T16" id="Seg_9129" s="T15">говорить-FUT-1SG</ta>
            <ta e="T17" id="Seg_9130" s="T16">теперь</ta>
            <ta e="T18" id="Seg_9131" s="T17">какой</ta>
            <ta e="T19" id="Seg_9132" s="T18">INDEF</ta>
            <ta e="T20" id="Seg_9133" s="T19">EMPH</ta>
            <ta e="T21" id="Seg_9134" s="T20">родить-PTCP.PRS-3SG-ABL</ta>
            <ta e="T22" id="Seg_9135" s="T21">быть-PRS.[3SG]</ta>
            <ta e="T23" id="Seg_9136" s="T22">EMPH</ta>
            <ta e="T24" id="Seg_9137" s="T23">олень.[NOM]</ta>
            <ta e="T25" id="Seg_9138" s="T24">да</ta>
            <ta e="T26" id="Seg_9139" s="T25">создаваться-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T27" id="Seg_9140" s="T26">человек.[NOM]</ta>
            <ta e="T28" id="Seg_9141" s="T27">вот</ta>
            <ta e="T29" id="Seg_9142" s="T28">так</ta>
            <ta e="T30" id="Seg_9143" s="T29">1SG.[NOM]</ta>
            <ta e="T31" id="Seg_9144" s="T30">говорить-PRS-1SG</ta>
            <ta e="T32" id="Seg_9145" s="T31">вот</ta>
            <ta e="T33" id="Seg_9146" s="T32">так</ta>
            <ta e="T34" id="Seg_9147" s="T33">олень.[NOM]</ta>
            <ta e="T35" id="Seg_9148" s="T34">родить-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T36" id="Seg_9149" s="T35">пастух-PL-DAT/LOC</ta>
            <ta e="T37" id="Seg_9150" s="T36">слышаться-EP-CAUS-CVB.PURP</ta>
            <ta e="T38" id="Seg_9151" s="T37">1SG.[NOM]</ta>
            <ta e="T39" id="Seg_9152" s="T38">говорить-PTCP.PRS-EP-1SG.[NOM]</ta>
            <ta e="T40" id="Seg_9153" s="T39">вот</ta>
            <ta e="T41" id="Seg_9154" s="T40">так-INTNS</ta>
            <ta e="T42" id="Seg_9155" s="T41">теперь</ta>
            <ta e="T43" id="Seg_9156" s="T42">этот</ta>
            <ta e="T44" id="Seg_9157" s="T43">олень-ACC</ta>
            <ta e="T45" id="Seg_9158" s="T44">заботиться-PRS-3PL</ta>
            <ta e="T46" id="Seg_9159" s="T45">1SG.[NOM]</ta>
            <ta e="T47" id="Seg_9160" s="T46">раньше</ta>
            <ta e="T48" id="Seg_9161" s="T47">заботиться-CVB.SIM</ta>
            <ta e="T49" id="Seg_9162" s="T48">идти-PTCP.PRS</ta>
            <ta e="T50" id="Seg_9163" s="T49">быть-TEMP-1SG</ta>
            <ta e="T51" id="Seg_9164" s="T50">олень.[NOM]</ta>
            <ta e="T52" id="Seg_9165" s="T51">родить-TEMP-3SG</ta>
            <ta e="T53" id="Seg_9166" s="T52">много</ta>
            <ta e="T54" id="Seg_9167" s="T53">причина-PROPR.[NOM]</ta>
            <ta e="T55" id="Seg_9168" s="T54">быть-PRS.[3SG]</ta>
            <ta e="T56" id="Seg_9169" s="T55">олень.[NOM]</ta>
            <ta e="T57" id="Seg_9170" s="T56">родиться-PTCP.PRS-3SG-ACC</ta>
            <ta e="T58" id="Seg_9171" s="T57">пастух.[NOM]</ta>
            <ta e="T59" id="Seg_9172" s="T58">быть-TEMP-1SG</ta>
            <ta e="T60" id="Seg_9173" s="T59">наблюдать-CVB.SIM</ta>
            <ta e="T61" id="Seg_9174" s="T60">идти-CVB.SEQ-1SG</ta>
            <ta e="T62" id="Seg_9175" s="T61">обходить-PTCP.HAB</ta>
            <ta e="T63" id="Seg_9176" s="T62">быть-PST1-1SG</ta>
            <ta e="T64" id="Seg_9177" s="T63">олень.[NOM]</ta>
            <ta e="T65" id="Seg_9178" s="T64">родить-CVB.SEQ</ta>
            <ta e="T66" id="Seg_9179" s="T65">ребенок-3SG-ACC</ta>
            <ta e="T67" id="Seg_9180" s="T66">спустить-TEMP-3SG</ta>
            <ta e="T68" id="Seg_9181" s="T67">разный-разный-ADVZ</ta>
            <ta e="T69" id="Seg_9182" s="T68">родиться-PTCP.PRS</ta>
            <ta e="T70" id="Seg_9183" s="T69">олень.[NOM]</ta>
            <ta e="T71" id="Seg_9184" s="T70">некоторый</ta>
            <ta e="T72" id="Seg_9185" s="T71">олень.[NOM]</ta>
            <ta e="T73" id="Seg_9186" s="T72">олененок-3SG.[NOM]</ta>
            <ta e="T74" id="Seg_9187" s="T73">родиться-CVB.SEQ</ta>
            <ta e="T75" id="Seg_9188" s="T74">после</ta>
            <ta e="T76" id="Seg_9189" s="T75">дернуть.ногами-CVB.SIM</ta>
            <ta e="T77" id="Seg_9190" s="T76">лежать-HAB.[3SG]</ta>
            <ta e="T78" id="Seg_9191" s="T77">мочь-CVB.SEQ</ta>
            <ta e="T79" id="Seg_9192" s="T78">вставать-NEG.CVB.SIM</ta>
            <ta e="T80" id="Seg_9193" s="T79">мать-3SG-ACC</ta>
            <ta e="T81" id="Seg_9194" s="T80">видеть-PTCP.COND-DAT/LOC</ta>
            <ta e="T82" id="Seg_9195" s="T81">жирный.[NOM]</ta>
            <ta e="T83" id="Seg_9196" s="T82">очень</ta>
            <ta e="T84" id="Seg_9197" s="T83">олененок-3SG-ACC</ta>
            <ta e="T85" id="Seg_9198" s="T84">видеть-PTCP.COND-DAT/LOC</ta>
            <ta e="T86" id="Seg_9199" s="T85">большой.[NOM]</ta>
            <ta e="T87" id="Seg_9200" s="T86">очень</ta>
            <ta e="T88" id="Seg_9201" s="T87">быть-PRS.[3SG]</ta>
            <ta e="T89" id="Seg_9202" s="T88">этот</ta>
            <ta e="T90" id="Seg_9203" s="T89">дернуть.ногами-CVB.SIM</ta>
            <ta e="T91" id="Seg_9204" s="T90">лежать-PTCP.PRS-ACC</ta>
            <ta e="T92" id="Seg_9205" s="T91">вращаться-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T93" id="Seg_9206" s="T92">надо</ta>
            <ta e="T94" id="Seg_9207" s="T93">вращаться-CAUS-CVB.SEQ</ta>
            <ta e="T95" id="Seg_9208" s="T94">попробовать-PTCP.COND-DAT/LOC</ta>
            <ta e="T96" id="Seg_9209" s="T95">что-ACC</ta>
            <ta e="T97" id="Seg_9210" s="T96">делать-CVB.SEQ</ta>
            <ta e="T98" id="Seg_9211" s="T97">почему</ta>
            <ta e="T99" id="Seg_9212" s="T98">вставать-NEG.[3SG]</ta>
            <ta e="T100" id="Seg_9213" s="T99">думать-CVB.SEQ</ta>
            <ta e="T101" id="Seg_9214" s="T100">этот-ACC</ta>
            <ta e="T102" id="Seg_9215" s="T101">массировать-PTCP.FUT-DAT/LOC</ta>
            <ta e="T103" id="Seg_9216" s="T102">живот-3SG-ACC</ta>
            <ta e="T104" id="Seg_9217" s="T103">массировать-PTCP.FUT-DAT/LOC</ta>
            <ta e="T105" id="Seg_9218" s="T104">олень.[NOM]</ta>
            <ta e="T106" id="Seg_9219" s="T105">родить-TEMP-3SG</ta>
            <ta e="T107" id="Seg_9220" s="T106">олененок.[NOM]</ta>
            <ta e="T108" id="Seg_9221" s="T107">собственный-3SG</ta>
            <ta e="T109" id="Seg_9222" s="T108">пуп-PROPR.[NOM]</ta>
            <ta e="T110" id="Seg_9223" s="T109">быть-HAB.[3SG]</ta>
            <ta e="T111" id="Seg_9224" s="T110">этот</ta>
            <ta e="T112" id="Seg_9225" s="T111">пуп-3SG-ACC</ta>
            <ta e="T113" id="Seg_9226" s="T112">некоторый</ta>
            <ta e="T114" id="Seg_9227" s="T113">олень.[NOM]</ta>
            <ta e="T115" id="Seg_9228" s="T114">тянуть-HAB.[3SG]</ta>
            <ta e="T116" id="Seg_9229" s="T115">этот</ta>
            <ta e="T117" id="Seg_9230" s="T116">пуп-3SG.[NOM]</ta>
            <ta e="T118" id="Seg_9231" s="T117">тянуть-PASS/REFL-TEMP-3SG</ta>
            <ta e="T119" id="Seg_9232" s="T118">этот-INSTR</ta>
            <ta e="T120" id="Seg_9233" s="T119">нутро-EP-ABL</ta>
            <ta e="T121" id="Seg_9234" s="T120">прямо</ta>
            <ta e="T122" id="Seg_9235" s="T121">дыра.[NOM]</ta>
            <ta e="T123" id="Seg_9236" s="T122">становиться-PRS.[3SG]</ta>
            <ta e="T124" id="Seg_9237" s="T123">тот-INSTR</ta>
            <ta e="T125" id="Seg_9238" s="T124">дыхание-3SG.[NOM]</ta>
            <ta e="T126" id="Seg_9239" s="T125">выйти-PRS.[3SG]</ta>
            <ta e="T127" id="Seg_9240" s="T126">нить-EP-INSTR</ta>
            <ta e="T128" id="Seg_9241" s="T127">крепко</ta>
            <ta e="T129" id="Seg_9242" s="T128">связывать-PTCP.COND-DAT/LOC</ta>
            <ta e="T130" id="Seg_9243" s="T129">этот</ta>
            <ta e="T131" id="Seg_9244" s="T130">олень.[NOM]</ta>
            <ta e="T132" id="Seg_9245" s="T131">этот</ta>
            <ta e="T133" id="Seg_9246" s="T132">олененок.[NOM]</ta>
            <ta e="T134" id="Seg_9247" s="T133">вставать-CVB.SEQ</ta>
            <ta e="T135" id="Seg_9248" s="T134">приходить-HAB.[3SG]</ta>
            <ta e="T136" id="Seg_9249" s="T135">тот</ta>
            <ta e="T137" id="Seg_9250" s="T136">имя-3SG.[NOM]</ta>
            <ta e="T138" id="Seg_9251" s="T137">пуп-3SG-ACC</ta>
            <ta e="T139" id="Seg_9252" s="T138">тянуть-PRS.[3SG]</ta>
            <ta e="T140" id="Seg_9253" s="T139">тот.[NOM]</ta>
            <ta e="T141" id="Seg_9254" s="T140">быть-PRS.[3SG]</ta>
            <ta e="T142" id="Seg_9255" s="T141">олень.[NOM]</ta>
            <ta e="T143" id="Seg_9256" s="T142">собственный-3SG.[NOM]</ta>
            <ta e="T144" id="Seg_9257" s="T143">олененок-3SG-GEN</ta>
            <ta e="T145" id="Seg_9258" s="T144">собственный-3SG.[NOM]</ta>
            <ta e="T146" id="Seg_9259" s="T145">причина-3SG.[NOM]</ta>
            <ta e="T147" id="Seg_9260" s="T146">родить-TEMP-3SG</ta>
            <ta e="T148" id="Seg_9261" s="T147">потом</ta>
            <ta e="T149" id="Seg_9262" s="T148">один</ta>
            <ta e="T150" id="Seg_9263" s="T149">причина-PROPR.[NOM]</ta>
            <ta e="T151" id="Seg_9264" s="T150">слабый.[NOM]</ta>
            <ta e="T152" id="Seg_9265" s="T151">олень.[NOM]</ta>
            <ta e="T153" id="Seg_9266" s="T152">родиться-PRS.[3SG]</ta>
            <ta e="T154" id="Seg_9267" s="T153">шустрый</ta>
            <ta e="T155" id="Seg_9268" s="T154">мать-3SG.[NOM]</ta>
            <ta e="T156" id="Seg_9269" s="T155">этот</ta>
            <ta e="T157" id="Seg_9270" s="T156">олененок-3SG.[NOM]</ta>
            <ta e="T158" id="Seg_9271" s="T157">родиться-TEMP-3SG</ta>
            <ta e="T159" id="Seg_9272" s="T158">дяпчу.[NOM]</ta>
            <ta e="T160" id="Seg_9273" s="T159">говорить-CVB.SEQ</ta>
            <ta e="T161" id="Seg_9274" s="T160">имя-VBZ-HAB.[3SG]</ta>
            <ta e="T162" id="Seg_9275" s="T161">тоже</ta>
            <ta e="T163" id="Seg_9276" s="T162">олененок-3SG.[NOM]</ta>
            <ta e="T164" id="Seg_9277" s="T163">стоять-PTCP.HAB-3SG</ta>
            <ta e="T165" id="Seg_9278" s="T164">NEG.[3SG]</ta>
            <ta e="T166" id="Seg_9279" s="T165">этот</ta>
            <ta e="T167" id="Seg_9280" s="T166">олененок-ACC</ta>
            <ta e="T168" id="Seg_9281" s="T167">вращаться-CAUS-CVB.SEQ</ta>
            <ta e="T169" id="Seg_9282" s="T168">попробовать-PTCP.COND-DAT/LOC</ta>
            <ta e="T170" id="Seg_9283" s="T169">вообще</ta>
            <ta e="T171" id="Seg_9284" s="T170">слабый.[NOM]</ta>
            <ta e="T172" id="Seg_9285" s="T171">очень</ta>
            <ta e="T173" id="Seg_9286" s="T172">быть-PRS.[3SG]</ta>
            <ta e="T174" id="Seg_9287" s="T173">этот</ta>
            <ta e="T175" id="Seg_9288" s="T174">олененок.[NOM]</ta>
            <ta e="T176" id="Seg_9289" s="T175">дернуть.ногами-CVB.SIM</ta>
            <ta e="T177" id="Seg_9290" s="T176">только</ta>
            <ta e="T178" id="Seg_9291" s="T177">лежать-HAB.[3SG]</ta>
            <ta e="T179" id="Seg_9292" s="T178">этот</ta>
            <ta e="T180" id="Seg_9293" s="T179">олененок-ACC</ta>
            <ta e="T181" id="Seg_9294" s="T180">навзничь</ta>
            <ta e="T182" id="Seg_9295" s="T181">держать-CVB.SEQ</ta>
            <ta e="T183" id="Seg_9296" s="T182">после</ta>
            <ta e="T184" id="Seg_9297" s="T183">грудь-3SG-ACC</ta>
            <ta e="T185" id="Seg_9298" s="T184">гладить-PTCP.COND-DAT/LOC</ta>
            <ta e="T186" id="Seg_9299" s="T185">тогда</ta>
            <ta e="T187" id="Seg_9300" s="T186">этот</ta>
            <ta e="T188" id="Seg_9301" s="T187">грудь-3SG.[NOM]</ta>
            <ta e="T189" id="Seg_9302" s="T188">дяпчу.[NOM]</ta>
            <ta e="T190" id="Seg_9303" s="T189">собственный-3SG.[NOM]</ta>
            <ta e="T191" id="Seg_9304" s="T190">когда</ta>
            <ta e="T192" id="Seg_9305" s="T191">INDEF</ta>
            <ta e="T193" id="Seg_9306" s="T192">олененок.[NOM]</ta>
            <ta e="T194" id="Seg_9307" s="T193">родиться-TEMP-3SG</ta>
            <ta e="T195" id="Seg_9308" s="T194">кость-3SG.[NOM]</ta>
            <ta e="T196" id="Seg_9309" s="T195">мягкий.[NOM]</ta>
            <ta e="T197" id="Seg_9310" s="T196">быть-PRS.[3SG]</ta>
            <ta e="T198" id="Seg_9311" s="T197">этот-ACC</ta>
            <ta e="T199" id="Seg_9312" s="T198">грудь-3SG-ACC</ta>
            <ta e="T200" id="Seg_9313" s="T199">гладить-PTCP.COND-DAT/LOC</ta>
            <ta e="T201" id="Seg_9314" s="T200">вставать-CVB.SIM-вставать-CVB.SIM</ta>
            <ta e="T202" id="Seg_9315" s="T201">падать-CVB.SIM</ta>
            <ta e="T203" id="Seg_9316" s="T202">идти-HAB.[3SG]</ta>
            <ta e="T204" id="Seg_9317" s="T203">дышать-TEMP-3SG</ta>
            <ta e="T205" id="Seg_9318" s="T204">этот-ACC</ta>
            <ta e="T206" id="Seg_9319" s="T205">мать-3SG-ACC</ta>
            <ta e="T207" id="Seg_9320" s="T206">хватать-CVB.SEQ</ta>
            <ta e="T208" id="Seg_9321" s="T207">после</ta>
            <ta e="T209" id="Seg_9322" s="T208">мать-3SG-ACC</ta>
            <ta e="T210" id="Seg_9323" s="T209">сосать-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T211" id="Seg_9324" s="T210">надо</ta>
            <ta e="T212" id="Seg_9325" s="T211">этот</ta>
            <ta e="T213" id="Seg_9326" s="T212">олененок.[NOM]</ta>
            <ta e="T214" id="Seg_9327" s="T213">грудь-3SG-ACC</ta>
            <ta e="T215" id="Seg_9328" s="T214">кто-ACC</ta>
            <ta e="T216" id="Seg_9329" s="T215">мести-PTCP.PST-3SG-ACC</ta>
            <ta e="T217" id="Seg_9330" s="T216">после.того</ta>
            <ta e="T218" id="Seg_9331" s="T217">мочь-CVB.SEQ</ta>
            <ta e="T219" id="Seg_9332" s="T218">сосать-PTCP.HAB-3SG</ta>
            <ta e="T220" id="Seg_9333" s="T219">NEG.[3SG]</ta>
            <ta e="T221" id="Seg_9334" s="T220">этот-ACC</ta>
            <ta e="T222" id="Seg_9335" s="T221">мать-3SG.[NOM]</ta>
            <ta e="T223" id="Seg_9336" s="T222">женская.грудь-3SG.[NOM]</ta>
            <ta e="T224" id="Seg_9337" s="T223">древесная.смола-PROPR.[NOM]</ta>
            <ta e="T225" id="Seg_9338" s="T224">быть-PST1-3SG</ta>
            <ta e="T226" id="Seg_9339" s="T225">толстый.и.короткий.[NOM]</ta>
            <ta e="T227" id="Seg_9340" s="T226">этот</ta>
            <ta e="T228" id="Seg_9341" s="T227">древесная.смола-ACC</ta>
            <ta e="T229" id="Seg_9342" s="T228">теснить-HAB.[3SG]</ta>
            <ta e="T230" id="Seg_9343" s="T229">человек.[NOM]</ta>
            <ta e="T231" id="Seg_9344" s="T230">теснить-CVB.SEQ</ta>
            <ta e="T232" id="Seg_9345" s="T231">после</ta>
            <ta e="T233" id="Seg_9346" s="T232">жидкий</ta>
            <ta e="T234" id="Seg_9347" s="T233">молоко-3SG.[NOM]</ta>
            <ta e="T235" id="Seg_9348" s="T234">приходить-TEMP-3SG</ta>
            <ta e="T236" id="Seg_9349" s="T235">тогда</ta>
            <ta e="T237" id="Seg_9350" s="T236">сосать-CAUS-HAB.[3SG]</ta>
            <ta e="T238" id="Seg_9351" s="T237">тогда</ta>
            <ta e="T239" id="Seg_9352" s="T238">сосать-CAUS-TEMP-3SG</ta>
            <ta e="T240" id="Seg_9353" s="T239">этот</ta>
            <ta e="T241" id="Seg_9354" s="T240">олененок.[NOM]</ta>
            <ta e="T242" id="Seg_9355" s="T241">человек.[NOM]</ta>
            <ta e="T243" id="Seg_9356" s="T242">становиться-PRS.[3SG]</ta>
            <ta e="T244" id="Seg_9357" s="T243">пастух-PL.[NOM]</ta>
            <ta e="T245" id="Seg_9358" s="T244">кочевать-PRS-3PL</ta>
            <ta e="T246" id="Seg_9359" s="T245">много</ta>
            <ta e="T247" id="Seg_9360" s="T246">олень.[NOM]</ta>
            <ta e="T248" id="Seg_9361" s="T247">родить-TEMP-3SG</ta>
            <ta e="T249" id="Seg_9362" s="T248">когда</ta>
            <ta e="T250" id="Seg_9363" s="T249">NEG</ta>
            <ta e="T251" id="Seg_9364" s="T250">родить-CVB.SIM=еще.не</ta>
            <ta e="T252" id="Seg_9365" s="T251">олень-ACC</ta>
            <ta e="T253" id="Seg_9366" s="T252">передняя.часть.[NOM]</ta>
            <ta e="T254" id="Seg_9367" s="T253">к</ta>
            <ta e="T255" id="Seg_9368" s="T254">уносить-HAB-3PL</ta>
            <ta e="T256" id="Seg_9369" s="T255">родить-PTCP.PST</ta>
            <ta e="T257" id="Seg_9370" s="T256">олень-ACC</ta>
            <ta e="T258" id="Seg_9371" s="T257">олененок-3SG.[NOM]</ta>
            <ta e="T259" id="Seg_9372" s="T258">расти-CVB.SIM</ta>
            <ta e="T260" id="Seg_9373" s="T259">падать-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T261" id="Seg_9374" s="T260">пока</ta>
            <ta e="T262" id="Seg_9375" s="T261">идти-NEG.PTCP-3SG-ACC</ta>
            <ta e="T263" id="Seg_9376" s="T262">из_за</ta>
            <ta e="T264" id="Seg_9377" s="T263">задняя.часть.[NOM]</ta>
            <ta e="T265" id="Seg_9378" s="T264">к</ta>
            <ta e="T266" id="Seg_9379" s="T265">оставлять-PRS-3PL</ta>
            <ta e="T267" id="Seg_9380" s="T266">этот</ta>
            <ta e="T268" id="Seg_9381" s="T267">оставлять-NMNZ-3PL-DAT/LOC</ta>
            <ta e="T269" id="Seg_9382" s="T268">этот</ta>
            <ta e="T270" id="Seg_9383" s="T269">вот</ta>
            <ta e="T271" id="Seg_9384" s="T270">десять-ABL</ta>
            <ta e="T272" id="Seg_9385" s="T271">больше</ta>
            <ta e="T273" id="Seg_9386" s="T272">двадцать-APRX</ta>
            <ta e="T274" id="Seg_9387" s="T273">олень.[NOM]</ta>
            <ta e="T275" id="Seg_9388" s="T274">родить-PTCP.PST-3SG-ACC</ta>
            <ta e="T276" id="Seg_9389" s="T275">после.того</ta>
            <ta e="T277" id="Seg_9390" s="T276">тогда</ta>
            <ta e="T278" id="Seg_9391" s="T277">этот</ta>
            <ta e="T279" id="Seg_9392" s="T278">родить-CVB.SIM=еще.не</ta>
            <ta e="T280" id="Seg_9393" s="T279">олень-ACC</ta>
            <ta e="T281" id="Seg_9394" s="T280">передняя.часть.[NOM]</ta>
            <ta e="T282" id="Seg_9395" s="T281">к</ta>
            <ta e="T283" id="Seg_9396" s="T282">уносить-PTCP.FUT-DAT/LOC</ta>
            <ta e="T284" id="Seg_9397" s="T283">надо</ta>
            <ta e="T285" id="Seg_9398" s="T284">этот</ta>
            <ta e="T286" id="Seg_9399" s="T285">олень-ACC</ta>
            <ta e="T287" id="Seg_9400" s="T286">уносить-PTCP.PRS-DAT/LOC</ta>
            <ta e="T288" id="Seg_9401" s="T287">некоторый</ta>
            <ta e="T289" id="Seg_9402" s="T288">пастух.[NOM]</ta>
            <ta e="T290" id="Seg_9403" s="T289">этот</ta>
            <ta e="T291" id="Seg_9404" s="T290">родить-PTCP.PST</ta>
            <ta e="T292" id="Seg_9405" s="T291">теленок-PROPR-DAT/LOC</ta>
            <ta e="T293" id="Seg_9406" s="T292">оставаться-PRS.[3SG]</ta>
            <ta e="T294" id="Seg_9407" s="T293">некоторый</ta>
            <ta e="T295" id="Seg_9408" s="T294">пастух.[NOM]</ta>
            <ta e="T296" id="Seg_9409" s="T295">этот</ta>
            <ta e="T297" id="Seg_9410" s="T296">родить</ta>
            <ta e="T298" id="Seg_9411" s="T297">родить-CVB.SIM=еще.не</ta>
            <ta e="T299" id="Seg_9412" s="T298">олень-ACC</ta>
            <ta e="T300" id="Seg_9413" s="T299">уносить-PRS.[3SG]</ta>
            <ta e="T301" id="Seg_9414" s="T300">этот</ta>
            <ta e="T302" id="Seg_9415" s="T301">уносить-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T303" id="Seg_9416" s="T302">есть-EP-CAUS-CVB.SEQ</ta>
            <ta e="T304" id="Seg_9417" s="T303">обман-VBZ-CVB.SEQ</ta>
            <ta e="T305" id="Seg_9418" s="T304">защитый</ta>
            <ta e="T306" id="Seg_9419" s="T305">место-DAT/LOC</ta>
            <ta e="T307" id="Seg_9420" s="T306">входить-CAUS-CVB.SEQ</ta>
            <ta e="T308" id="Seg_9421" s="T307">стадо-3SG.[NOM]</ta>
            <ta e="T309" id="Seg_9422" s="T308">идти-PTCP.PRS-3SG-ACC</ta>
            <ta e="T310" id="Seg_9423" s="T309">показывать-NEG.CVB.SIM</ta>
            <ta e="T311" id="Seg_9424" s="T310">этот</ta>
            <ta e="T312" id="Seg_9425" s="T311">теленок-PROPR</ta>
            <ta e="T314" id="Seg_9426" s="T312">важенка-ACC</ta>
            <ta e="T315" id="Seg_9427" s="T314">когда</ta>
            <ta e="T316" id="Seg_9428" s="T315">INDEF</ta>
            <ta e="T317" id="Seg_9429" s="T316">олень.[NOM]</ta>
            <ta e="T318" id="Seg_9430" s="T317">жалеть-RECP/COLL-CVB.SEQ</ta>
            <ta e="T319" id="Seg_9431" s="T318">олененок-3SG-ACC</ta>
            <ta e="T320" id="Seg_9432" s="T319">оставлять-HAB.[3SG]</ta>
            <ta e="T321" id="Seg_9433" s="T320">следовать-CVB.SEQ</ta>
            <ta e="T322" id="Seg_9434" s="T321">стадо-3SG-DAT/LOC</ta>
            <ta e="T323" id="Seg_9435" s="T322">выбирать-EP-RECP/COLL-CVB.SEQ</ta>
            <ta e="T324" id="Seg_9436" s="T323">тот-ACC</ta>
            <ta e="T325" id="Seg_9437" s="T324">есть-EP-CAUS-CVB.SIM</ta>
            <ta e="T326" id="Seg_9438" s="T325">идти-CVB.SEQ</ta>
            <ta e="T327" id="Seg_9439" s="T326">наесться-PTCP.PST-3SG-ACC</ta>
            <ta e="T328" id="Seg_9440" s="T327">постоянно.жевать-CVB.SIM</ta>
            <ta e="T329" id="Seg_9441" s="T328">лежать-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T330" id="Seg_9442" s="T329">родить-CVB.SIM=еще.не</ta>
            <ta e="T331" id="Seg_9443" s="T330">олень-3SG-ACC</ta>
            <ta e="T332" id="Seg_9444" s="T331">передняя.часть.[NOM]</ta>
            <ta e="T333" id="Seg_9445" s="T332">к</ta>
            <ta e="T334" id="Seg_9446" s="T333">перевозить-CVB.SEQ</ta>
            <ta e="T335" id="Seg_9447" s="T334">уносить-PRS.[3SG]</ta>
            <ta e="T336" id="Seg_9448" s="T335">тот</ta>
            <ta e="T337" id="Seg_9449" s="T336">перевозить-TEMP-3PL</ta>
            <ta e="T338" id="Seg_9450" s="T337">некоторый</ta>
            <ta e="T339" id="Seg_9451" s="T338">пастух-PL.[NOM]</ta>
            <ta e="T340" id="Seg_9452" s="T339">очень</ta>
            <ta e="T341" id="Seg_9453" s="T340">далекий.[NOM]</ta>
            <ta e="T342" id="Seg_9454" s="T341">идти-HAB-3PL</ta>
            <ta e="T343" id="Seg_9455" s="T342">этот</ta>
            <ta e="T344" id="Seg_9456" s="T343">далекий.[NOM]</ta>
            <ta e="T345" id="Seg_9457" s="T344">идти-TEMP-3PL</ta>
            <ta e="T346" id="Seg_9458" s="T345">этот</ta>
            <ta e="T347" id="Seg_9459" s="T346">олененок.[NOM]</ta>
            <ta e="T348" id="Seg_9460" s="T347">следующее.утро-3SG-ACC</ta>
            <ta e="T349" id="Seg_9461" s="T348">тот</ta>
            <ta e="T350" id="Seg_9462" s="T349">родить-CVB.SIM=еще.не</ta>
            <ta e="T351" id="Seg_9463" s="T350">олень-PROPR-DAT/LOC</ta>
            <ta e="T352" id="Seg_9464" s="T351">задняя.часть-ADJZ-3SG-ACC</ta>
            <ta e="T353" id="Seg_9465" s="T352">пастух-3PL-ACC</ta>
            <ta e="T354" id="Seg_9466" s="T353">тот</ta>
            <ta e="T355" id="Seg_9467" s="T354">этот-ACC</ta>
            <ta e="T356" id="Seg_9468" s="T355">родить-PTCP.PST</ta>
            <ta e="T357" id="Seg_9469" s="T356">важенка-PROPR-3PL-ACC</ta>
            <ta e="T358" id="Seg_9470" s="T357">пастух-3SG.[NOM]</ta>
            <ta e="T359" id="Seg_9471" s="T358">передняя.часть.[NOM]</ta>
            <ta e="T360" id="Seg_9472" s="T359">к</ta>
            <ta e="T361" id="Seg_9473" s="T360">пастух-PL-DAT/LOC</ta>
            <ta e="T362" id="Seg_9474" s="T361">уносить-CVB.PURP-3PL</ta>
            <ta e="T363" id="Seg_9475" s="T362">кочевать-PTCP.PRS-3PL-DAT/LOC</ta>
            <ta e="T364" id="Seg_9476" s="T363">олень.[NOM]</ta>
            <ta e="T365" id="Seg_9477" s="T364">мочь-CVB.SEQ</ta>
            <ta e="T366" id="Seg_9478" s="T365">доезжать-PTCP.HAB-3SG</ta>
            <ta e="T367" id="Seg_9479" s="T366">NEG.[3SG]</ta>
            <ta e="T368" id="Seg_9480" s="T367">этот.[NOM]</ta>
            <ta e="T369" id="Seg_9481" s="T368">тот.[NOM]</ta>
            <ta e="T370" id="Seg_9482" s="T369">из_за</ta>
            <ta e="T371" id="Seg_9483" s="T370">близкий.[NOM]</ta>
            <ta e="T372" id="Seg_9484" s="T371">падать-PTCP.FUT-DAT/LOC</ta>
            <ta e="T373" id="Seg_9485" s="T372">видеть-CVB.SEQ</ta>
            <ta e="T374" id="Seg_9486" s="T373">после</ta>
            <ta e="T375" id="Seg_9487" s="T374">потом</ta>
            <ta e="T376" id="Seg_9488" s="T375">однако</ta>
            <ta e="T377" id="Seg_9489" s="T376">этот</ta>
            <ta e="T378" id="Seg_9490" s="T377">пастух-PL.[NOM]</ta>
            <ta e="T379" id="Seg_9491" s="T378">когда</ta>
            <ta e="T380" id="Seg_9492" s="T379">да</ta>
            <ta e="T381" id="Seg_9493" s="T380">защита-PROPR</ta>
            <ta e="T382" id="Seg_9494" s="T381">место-EP-INSTR</ta>
            <ta e="T383" id="Seg_9495" s="T382">ехать-PRS-3PL</ta>
            <ta e="T384" id="Seg_9496" s="T383">маршрут.[NOM]</ta>
            <ta e="T385" id="Seg_9497" s="T384">видеть-PRS-3PL</ta>
            <ta e="T386" id="Seg_9498" s="T385">олень.[NOM]</ta>
            <ta e="T387" id="Seg_9499" s="T386">родить-EP-CAUS-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T388" id="Seg_9500" s="T387">защита-PROPR</ta>
            <ta e="T389" id="Seg_9501" s="T388">земля-EP-INSTR</ta>
            <ta e="T390" id="Seg_9502" s="T389">лес-PROPR</ta>
            <ta e="T391" id="Seg_9503" s="T390">место-EP-INSTR</ta>
            <ta e="T392" id="Seg_9504" s="T391">гора-PL-PROPR</ta>
            <ta e="T393" id="Seg_9505" s="T392">место-PL-EP-INSTR</ta>
            <ta e="T394" id="Seg_9506" s="T393">река-PL-PROPR</ta>
            <ta e="T395" id="Seg_9507" s="T394">место-PL-EP-INSTR</ta>
            <ta e="T396" id="Seg_9508" s="T395">этот</ta>
            <ta e="T397" id="Seg_9509" s="T396">большой</ta>
            <ta e="T398" id="Seg_9510" s="T397">сугроб.[NOM]</ta>
            <ta e="T399" id="Seg_9511" s="T398">кончаться-HAB.[3SG]</ta>
            <ta e="T400" id="Seg_9512" s="T399">этот</ta>
            <ta e="T401" id="Seg_9513" s="T400">обрываться-TEMP-3SG</ta>
            <ta e="T402" id="Seg_9514" s="T401">весной</ta>
            <ta e="T403" id="Seg_9515" s="T402">олень.[NOM]</ta>
            <ta e="T404" id="Seg_9516" s="T403">родить-PTCP.PRS-3SG-ACC</ta>
            <ta e="T405" id="Seg_9517" s="T404">когда</ta>
            <ta e="T406" id="Seg_9518" s="T405">день.[NOM]</ta>
            <ta e="T407" id="Seg_9519" s="T406">посветлеть-TEMP-3SG</ta>
            <ta e="T408" id="Seg_9520" s="T407">зимой</ta>
            <ta e="T409" id="Seg_9521" s="T408">обрываться-EP-PTCP.PST</ta>
            <ta e="T410" id="Seg_9522" s="T409">сугроб.[NOM]</ta>
            <ta e="T411" id="Seg_9523" s="T410">открывать-HAB.[3SG]</ta>
            <ta e="T412" id="Seg_9524" s="T411">тот</ta>
            <ta e="T413" id="Seg_9525" s="T412">имя-3SG.[NOM]</ta>
            <ta e="T414" id="Seg_9526" s="T413">обрыв.[NOM]</ta>
            <ta e="T415" id="Seg_9527" s="T414">быть-PRS.[3SG]</ta>
            <ta e="T416" id="Seg_9528" s="T415">олень.[NOM]</ta>
            <ta e="T417" id="Seg_9529" s="T416">весть-POSS</ta>
            <ta e="T418" id="Seg_9530" s="T417">NEG</ta>
            <ta e="T419" id="Seg_9531" s="T418">потерять-CVB.SIM</ta>
            <ta e="T420" id="Seg_9532" s="T419">тот</ta>
            <ta e="T421" id="Seg_9533" s="T420">обрыв.[NOM]</ta>
            <ta e="T422" id="Seg_9534" s="T421">нутро-3SG-DAT/LOC</ta>
            <ta e="T423" id="Seg_9535" s="T422">падать-PTCP.PST</ta>
            <ta e="T424" id="Seg_9536" s="T423">олень-ACC</ta>
            <ta e="T425" id="Seg_9537" s="T424">этот</ta>
            <ta e="T426" id="Seg_9538" s="T425">пастух.[NOM]</ta>
            <ta e="T427" id="Seg_9539" s="T426">охранять-CVB.SIM</ta>
            <ta e="T428" id="Seg_9540" s="T427">идти-CVB.SEQ</ta>
            <ta e="T429" id="Seg_9541" s="T428">замечать-NEG.[3SG]</ta>
            <ta e="T430" id="Seg_9542" s="T429">сколько</ta>
            <ta e="T431" id="Seg_9543" s="T430">INDEF</ta>
            <ta e="T432" id="Seg_9544" s="T431">олень.[NOM]</ta>
            <ta e="T433" id="Seg_9545" s="T432">падать-PTCP.PST-3SG-ACC</ta>
            <ta e="T434" id="Seg_9546" s="T433">EMPH-вот</ta>
            <ta e="T435" id="Seg_9547" s="T434">скучать-NEG.[3SG]</ta>
            <ta e="T436" id="Seg_9548" s="T435">олень.[NOM]</ta>
            <ta e="T437" id="Seg_9549" s="T436">весть-POSS</ta>
            <ta e="T438" id="Seg_9550" s="T437">NEG</ta>
            <ta e="T439" id="Seg_9551" s="T438">пропадать-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T440" id="Seg_9552" s="T439">тот</ta>
            <ta e="T441" id="Seg_9553" s="T440">так</ta>
            <ta e="T442" id="Seg_9554" s="T441">бригадир-PL.[NOM]</ta>
            <ta e="T443" id="Seg_9555" s="T442">может</ta>
            <ta e="T444" id="Seg_9556" s="T443">пастух-PL.[NOM]</ta>
            <ta e="T445" id="Seg_9557" s="T444">может</ta>
            <ta e="T446" id="Seg_9558" s="T445">весной</ta>
            <ta e="T447" id="Seg_9559" s="T446">олень.[NOM]</ta>
            <ta e="T448" id="Seg_9560" s="T447">родить-PTCP.PRS-3SG-ACC</ta>
            <ta e="T449" id="Seg_9561" s="T448">когда</ta>
            <ta e="T451" id="Seg_9562" s="T450">снег.[NOM]</ta>
            <ta e="T452" id="Seg_9563" s="T451">таять-PTCP.PRS-3SG-ACC</ta>
            <ta e="T453" id="Seg_9564" s="T452">когда</ta>
            <ta e="T454" id="Seg_9565" s="T453">хороший-ADVZ</ta>
            <ta e="T455" id="Seg_9566" s="T454">место-3PL-ACC</ta>
            <ta e="T456" id="Seg_9567" s="T455">видеть-PTCP.FUT-3PL-ACC</ta>
            <ta e="T457" id="Seg_9568" s="T456">надо</ta>
            <ta e="T458" id="Seg_9569" s="T457">сугроб.[NOM]</ta>
            <ta e="T459" id="Seg_9570" s="T458">разорвать-EP-PTCP.PST-3SG.[NOM]</ta>
            <ta e="T460" id="Seg_9571" s="T459">пурга.[NOM]</ta>
            <ta e="T461" id="Seg_9572" s="T460">быть-TEMP-3SG</ta>
            <ta e="T462" id="Seg_9573" s="T461">замести-EP-PASS/REFL-CVB.SEQ</ta>
            <ta e="T463" id="Seg_9574" s="T462">оставаться-HAB.[3SG]</ta>
            <ta e="T464" id="Seg_9575" s="T463">тот</ta>
            <ta e="T465" id="Seg_9576" s="T464">делать-CVB.SEQ</ta>
            <ta e="T466" id="Seg_9577" s="T465">после</ta>
            <ta e="T467" id="Seg_9578" s="T466">тот</ta>
            <ta e="T468" id="Seg_9579" s="T467">замести-EP-PASS/REFL-EP-PTCP.PST-3SG-ACC</ta>
            <ta e="T469" id="Seg_9580" s="T468">после.того</ta>
            <ta e="T470" id="Seg_9581" s="T469">тот-ACC</ta>
            <ta e="T471" id="Seg_9582" s="T470">олень.[NOM]</ta>
            <ta e="T472" id="Seg_9583" s="T471">видеть-NEG.[3SG]</ta>
            <ta e="T473" id="Seg_9584" s="T472">EMPH-вот</ta>
            <ta e="T474" id="Seg_9585" s="T473">падать-PRS.[3SG]</ta>
            <ta e="T475" id="Seg_9586" s="T474">тот</ta>
            <ta e="T476" id="Seg_9587" s="T475">падать-TEMP-3SG</ta>
            <ta e="T477" id="Seg_9588" s="T476">пять-четыре</ta>
            <ta e="T478" id="Seg_9589" s="T477">метр-PROPR</ta>
            <ta e="T479" id="Seg_9590" s="T478">глубина-PROPR.[NOM]</ta>
            <ta e="T480" id="Seg_9591" s="T479">быть-HAB.[3SG]</ta>
            <ta e="T481" id="Seg_9592" s="T480">этот</ta>
            <ta e="T482" id="Seg_9593" s="T481">обрыв.[NOM]</ta>
            <ta e="T483" id="Seg_9594" s="T482">снег.[NOM]</ta>
            <ta e="T484" id="Seg_9595" s="T483">обрываться-EP-PST2.[3SG]</ta>
            <ta e="T485" id="Seg_9596" s="T484">тот.[NOM]</ta>
            <ta e="T486" id="Seg_9597" s="T485">из_за</ta>
            <ta e="T487" id="Seg_9598" s="T486">такой-ACC.[NOM]</ta>
            <ta e="T488" id="Seg_9599" s="T487">найти-NEG.CVB.SIM</ta>
            <ta e="T489" id="Seg_9600" s="T488">олень.[NOM]</ta>
            <ta e="T490" id="Seg_9601" s="T489">весть-POSS</ta>
            <ta e="T491" id="Seg_9602" s="T490">NEG</ta>
            <ta e="T492" id="Seg_9603" s="T491">пропадать-PRS.[3SG]</ta>
            <ta e="T493" id="Seg_9604" s="T492">тот.[NOM]</ta>
            <ta e="T494" id="Seg_9605" s="T493">потом</ta>
            <ta e="T495" id="Seg_9606" s="T494">река-PROPR</ta>
            <ta e="T496" id="Seg_9607" s="T495">место-PL-DAT/LOC</ta>
            <ta e="T497" id="Seg_9608" s="T496">однако</ta>
            <ta e="T498" id="Seg_9609" s="T497">проталина-VBZ-TEMP-3SG</ta>
            <ta e="T499" id="Seg_9610" s="T498">когда</ta>
            <ta e="T500" id="Seg_9611" s="T499">NEG</ta>
            <ta e="T501" id="Seg_9612" s="T500">река-PROPR</ta>
            <ta e="T502" id="Seg_9613" s="T501">земля-EP-INSTR</ta>
            <ta e="T503" id="Seg_9614" s="T502">быть-PRS.[3SG]</ta>
            <ta e="T504" id="Seg_9615" s="T503">маршрут.[NOM]</ta>
            <ta e="T505" id="Seg_9616" s="T504">этот</ta>
            <ta e="T506" id="Seg_9617" s="T505">снег-ACC</ta>
            <ta e="T507" id="Seg_9618" s="T506">с</ta>
            <ta e="T508" id="Seg_9619" s="T507">гора.[NOM]</ta>
            <ta e="T509" id="Seg_9620" s="T508">земля.[NOM]</ta>
            <ta e="T510" id="Seg_9621" s="T509">промежуток-3SG.[NOM]</ta>
            <ta e="T511" id="Seg_9622" s="T510">открывать-HAB.[3SG]</ta>
            <ta e="T512" id="Seg_9623" s="T511">тоже</ta>
            <ta e="T513" id="Seg_9624" s="T512">так</ta>
            <ta e="T514" id="Seg_9625" s="T513">тоже</ta>
            <ta e="T515" id="Seg_9626" s="T514">падать-HAB.[3SG]</ta>
            <ta e="T516" id="Seg_9627" s="T515">олененок.[NOM]</ta>
            <ta e="T517" id="Seg_9628" s="T516">может</ta>
            <ta e="T518" id="Seg_9629" s="T517">большой</ta>
            <ta e="T519" id="Seg_9630" s="T518">олень.[NOM]</ta>
            <ta e="T520" id="Seg_9631" s="T519">может</ta>
            <ta e="T521" id="Seg_9632" s="T520">потом</ta>
            <ta e="T522" id="Seg_9633" s="T521">этот</ta>
            <ta e="T523" id="Seg_9634" s="T522">пастух-PL.[NOM]</ta>
            <ta e="T524" id="Seg_9635" s="T523">весна</ta>
            <ta e="T525" id="Seg_9636" s="T524">лето-3PL.[NOM]</ta>
            <ta e="T526" id="Seg_9637" s="T525">приближаться-TEMP-3SG</ta>
            <ta e="T527" id="Seg_9638" s="T526">весна-ADJZ-3PL-DAT/LOC</ta>
            <ta e="T528" id="Seg_9639" s="T527">верховой.олень-3PL-ACC</ta>
            <ta e="T529" id="Seg_9640" s="T528">мочь-CVB.SEQ</ta>
            <ta e="T530" id="Seg_9641" s="T529">заботиться-MED-NEG-3PL</ta>
            <ta e="T531" id="Seg_9642" s="T530">овод-3SG-ACC</ta>
            <ta e="T532" id="Seg_9643" s="T531">обрывать-NEG-3PL</ta>
            <ta e="T533" id="Seg_9644" s="T532">весна.[NOM]</ta>
            <ta e="T534" id="Seg_9645" s="T533">овод-3SG-ACC</ta>
            <ta e="T535" id="Seg_9646" s="T534">обрывать-TEMP-3PL</ta>
            <ta e="T536" id="Seg_9647" s="T535">тот</ta>
            <ta e="T537" id="Seg_9648" s="T536">овод-3SG-ACC</ta>
            <ta e="T538" id="Seg_9649" s="T537">обрывать-TEMP-3PL</ta>
            <ta e="T539" id="Seg_9650" s="T538">этот</ta>
            <ta e="T540" id="Seg_9651" s="T539">овод.[NOM]</ta>
            <ta e="T541" id="Seg_9652" s="T540">поправляться-PRS.[3SG]</ta>
            <ta e="T542" id="Seg_9653" s="T541">сам-3SG.[NOM]</ta>
            <ta e="T543" id="Seg_9654" s="T542">овод.[NOM]</ta>
            <ta e="T544" id="Seg_9655" s="T543">весной</ta>
            <ta e="T545" id="Seg_9656" s="T544">расти-CVB.SEQ</ta>
            <ta e="T546" id="Seg_9657" s="T545">выйти-TEMP-3SG</ta>
            <ta e="T547" id="Seg_9658" s="T546">кожа-3SG-GEN</ta>
            <ta e="T548" id="Seg_9659" s="T547">нутро-3SG-ABL</ta>
            <ta e="T549" id="Seg_9660" s="T548">сам-3SG.[NOM]</ta>
            <ta e="T550" id="Seg_9661" s="T549">отделяться-CVB.SEQ</ta>
            <ta e="T551" id="Seg_9662" s="T550">падать-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T552" id="Seg_9663" s="T551">пока</ta>
            <ta e="T553" id="Seg_9664" s="T552">идти-TEMP-3SG</ta>
            <ta e="T554" id="Seg_9665" s="T553">олень-DAT/LOC</ta>
            <ta e="T555" id="Seg_9666" s="T554">да</ta>
            <ta e="T556" id="Seg_9667" s="T555">плохой.[NOM]</ta>
            <ta e="T557" id="Seg_9668" s="T556">некоторый</ta>
            <ta e="T558" id="Seg_9669" s="T557">овод-ACC</ta>
            <ta e="T559" id="Seg_9670" s="T558">полный-SIM</ta>
            <ta e="T560" id="Seg_9671" s="T559">садиться-CVB.SIM-садиться-CVB.SIM-3PL</ta>
            <ta e="T561" id="Seg_9672" s="T560">олень.[NOM]</ta>
            <ta e="T562" id="Seg_9673" s="T561">собственный-3SG</ta>
            <ta e="T563" id="Seg_9674" s="T562">умирать-PTCP.PRS</ta>
            <ta e="T564" id="Seg_9675" s="T563">ползать-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T565" id="Seg_9676" s="T564">тот</ta>
            <ta e="T566" id="Seg_9677" s="T565">некоторый</ta>
            <ta e="T567" id="Seg_9678" s="T566">пастух.[NOM]</ta>
            <ta e="T568" id="Seg_9679" s="T567">олень-ACC</ta>
            <ta e="T569" id="Seg_9680" s="T568">да</ta>
            <ta e="T570" id="Seg_9681" s="T569">запрячь-CVB.SIM</ta>
            <ta e="T571" id="Seg_9682" s="T570">мочь-NEG.[3SG]</ta>
            <ta e="T572" id="Seg_9683" s="T571">поводок-3PL.[NOM]</ta>
            <ta e="T573" id="Seg_9684" s="T572">обрываться-PST1-3SG</ta>
            <ta e="T574" id="Seg_9685" s="T573">олень.[NOM]</ta>
            <ta e="T575" id="Seg_9686" s="T574">собственный-3SG-GEN</ta>
            <ta e="T576" id="Seg_9687" s="T575">пах-3SG-ACC</ta>
            <ta e="T577" id="Seg_9688" s="T576">прорубить-PRS-3PL</ta>
            <ta e="T578" id="Seg_9689" s="T577">пах-3SG-ACC</ta>
            <ta e="T579" id="Seg_9690" s="T578">прорубить-TEMP-3PL</ta>
            <ta e="T580" id="Seg_9691" s="T579">этот</ta>
            <ta e="T581" id="Seg_9692" s="T580">летом</ta>
            <ta e="T582" id="Seg_9693" s="T581">рана.[NOM]</ta>
            <ta e="T583" id="Seg_9694" s="T582">быть-HAB.[3SG]</ta>
            <ta e="T584" id="Seg_9695" s="T583">этот-INSTR</ta>
            <ta e="T585" id="Seg_9696" s="T584">олень.[NOM]</ta>
            <ta e="T587" id="Seg_9697" s="T586">заболеть-PTCP.PRS-3SG.[NOM]</ta>
            <ta e="T588" id="Seg_9698" s="T587">тот.[NOM]</ta>
            <ta e="T589" id="Seg_9699" s="T588">потом</ta>
            <ta e="T590" id="Seg_9700" s="T589">хвост-3SG-ACC</ta>
            <ta e="T591" id="Seg_9701" s="T590">олень.[NOM]</ta>
            <ta e="T592" id="Seg_9702" s="T591">собственный-3SG-ACC</ta>
            <ta e="T593" id="Seg_9703" s="T592">колоть-CVB.SIM</ta>
            <ta e="T594" id="Seg_9704" s="T593">толкать-PRS-3PL</ta>
            <ta e="T595" id="Seg_9705" s="T594">этот</ta>
            <ta e="T596" id="Seg_9706" s="T595">хвост-3SG-ACC</ta>
            <ta e="T597" id="Seg_9707" s="T596">колоть-CVB.SIM</ta>
            <ta e="T598" id="Seg_9708" s="T597">толкать-TEMP-3PL</ta>
            <ta e="T599" id="Seg_9709" s="T598">зимой</ta>
            <ta e="T600" id="Seg_9710" s="T599">холод-DAT/LOC</ta>
            <ta e="T601" id="Seg_9711" s="T600">этот</ta>
            <ta e="T602" id="Seg_9712" s="T601">олень.[NOM]</ta>
            <ta e="T603" id="Seg_9713" s="T602">собственный-3SG.[NOM]</ta>
            <ta e="T604" id="Seg_9714" s="T603">мясо-3SG.[NOM]</ta>
            <ta e="T605" id="Seg_9715" s="T604">когда</ta>
            <ta e="T606" id="Seg_9716" s="T605">NEG</ta>
            <ta e="T607" id="Seg_9717" s="T606">опять</ta>
            <ta e="T608" id="Seg_9718" s="T607">подниматься-NEG.[3SG]</ta>
            <ta e="T609" id="Seg_9719" s="T608">потом</ta>
            <ta e="T610" id="Seg_9720" s="T609">летом</ta>
            <ta e="T611" id="Seg_9721" s="T610">теплый.[NOM]</ta>
            <ta e="T612" id="Seg_9722" s="T611">когда</ta>
            <ta e="T613" id="Seg_9723" s="T612">кочевать-PRS-3PL</ta>
            <ta e="T614" id="Seg_9724" s="T613">этот</ta>
            <ta e="T615" id="Seg_9725" s="T614">кочевать-TEMP-3PL</ta>
            <ta e="T616" id="Seg_9726" s="T615">тепло-DAT/LOC</ta>
            <ta e="T617" id="Seg_9727" s="T616">кочевать-TEMP-3PL</ta>
            <ta e="T618" id="Seg_9728" s="T617">кочевать-CVB.SEQ</ta>
            <ta e="T619" id="Seg_9729" s="T618">идти-CVB.SEQ-3PL</ta>
            <ta e="T620" id="Seg_9730" s="T619">река-PROPR</ta>
            <ta e="T621" id="Seg_9731" s="T620">место-PL-EP-INSTR</ta>
            <ta e="T622" id="Seg_9732" s="T621">идти-PRS-3PL</ta>
            <ta e="T623" id="Seg_9733" s="T622">этот</ta>
            <ta e="T624" id="Seg_9734" s="T623">река-PROPR</ta>
            <ta e="T625" id="Seg_9735" s="T624">большой</ta>
            <ta e="T626" id="Seg_9736" s="T625">гора-PL-DAT/LOC</ta>
            <ta e="T627" id="Seg_9737" s="T626">глина-PROPR.[NOM]</ta>
            <ta e="T628" id="Seg_9738" s="T627">быть-HAB.[3SG]</ta>
            <ta e="T629" id="Seg_9739" s="T628">сколько</ta>
            <ta e="T630" id="Seg_9740" s="T629">да</ta>
            <ta e="T631" id="Seg_9741" s="T630">тепло-DAT/LOC</ta>
            <ta e="T632" id="Seg_9742" s="T631">этот</ta>
            <ta e="T633" id="Seg_9743" s="T632">глина-ACC</ta>
            <ta e="T634" id="Seg_9744" s="T633">узнавать-PST1-3SG</ta>
            <ta e="T635" id="Seg_9745" s="T634">да</ta>
            <ta e="T636" id="Seg_9746" s="T635">олень.[NOM]</ta>
            <ta e="T637" id="Seg_9747" s="T636">этот</ta>
            <ta e="T638" id="Seg_9748" s="T637">гора-DAT/LOC</ta>
            <ta e="T639" id="Seg_9749" s="T638">идти-HAB.[3SG]</ta>
            <ta e="T640" id="Seg_9750" s="T639">кочевать-CVB.SEQ</ta>
            <ta e="T641" id="Seg_9751" s="T640">идти-PTCP.COND-DAT/LOC</ta>
            <ta e="T642" id="Seg_9752" s="T641">да</ta>
            <ta e="T643" id="Seg_9753" s="T642">гнать-CVB.SEQ</ta>
            <ta e="T644" id="Seg_9754" s="T643">да</ta>
            <ta e="T645" id="Seg_9755" s="T644">идти-TEMP-3PL</ta>
            <ta e="T646" id="Seg_9756" s="T645">этот-ACC</ta>
            <ta e="T647" id="Seg_9757" s="T646">пастух-PL.[NOM]</ta>
            <ta e="T648" id="Seg_9758" s="T647">этот</ta>
            <ta e="T649" id="Seg_9759" s="T648">гора-ACC</ta>
            <ta e="T650" id="Seg_9760" s="T649">видеть-NEG.CVB.SIM</ta>
            <ta e="T651" id="Seg_9761" s="T650">собака-VBZ-CVB.SEQ</ta>
            <ta e="T652" id="Seg_9762" s="T651">взять-PST1-3PL</ta>
            <ta e="T653" id="Seg_9763" s="T652">да</ta>
            <ta e="T654" id="Seg_9764" s="T653">EMPH-вот</ta>
            <ta e="T655" id="Seg_9765" s="T654">смеяться-RECP/COLL-CVB.SIM-смеяться-RECP/COLL-CVB.SIM</ta>
            <ta e="T656" id="Seg_9766" s="T655">проехать-CVB.SEQ</ta>
            <ta e="T657" id="Seg_9767" s="T656">оставаться-PRS-3PL</ta>
            <ta e="T658" id="Seg_9768" s="T657">здесь</ta>
            <ta e="T659" id="Seg_9769" s="T658">десять</ta>
            <ta e="T660" id="Seg_9770" s="T659">остаток-3SG.[NOM]</ta>
            <ta e="T661" id="Seg_9771" s="T660">пять</ta>
            <ta e="T662" id="Seg_9772" s="T661">MOD</ta>
            <ta e="T663" id="Seg_9773" s="T662">двадцать</ta>
            <ta e="T664" id="Seg_9774" s="T663">остаток-3SG.[NOM]</ta>
            <ta e="T665" id="Seg_9775" s="T664">пять</ta>
            <ta e="T666" id="Seg_9776" s="T665">MOD</ta>
            <ta e="T667" id="Seg_9777" s="T666">олень.[NOM]</ta>
            <ta e="T668" id="Seg_9778" s="T667">оставаться-PRS.[3SG]</ta>
            <ta e="T669" id="Seg_9779" s="T668">этот</ta>
            <ta e="T670" id="Seg_9780" s="T669">оставаться-PTCP.PST-3SG-ACC</ta>
            <ta e="T671" id="Seg_9781" s="T670">замечать-NEG-3PL</ta>
            <ta e="T672" id="Seg_9782" s="T671">стоянка-DAT/LOC</ta>
            <ta e="T673" id="Seg_9783" s="T672">падать-CVB.SEQ-3PL</ta>
            <ta e="T674" id="Seg_9784" s="T673">следующий</ta>
            <ta e="T675" id="Seg_9785" s="T674">человек.[NOM]</ta>
            <ta e="T676" id="Seg_9786" s="T675">охранять-PRS.[3SG]</ta>
            <ta e="T677" id="Seg_9787" s="T676">недавно</ta>
            <ta e="T678" id="Seg_9788" s="T677">этот</ta>
            <ta e="T679" id="Seg_9789" s="T678">кочевать-PRS-3PL-DAT/LOC</ta>
            <ta e="T680" id="Seg_9790" s="T679">охранять-PTCP.PST</ta>
            <ta e="T681" id="Seg_9791" s="T680">человек.[NOM]</ta>
            <ta e="T682" id="Seg_9792" s="T681">только</ta>
            <ta e="T683" id="Seg_9793" s="T682">верховой.олень-3SG.[NOM]</ta>
            <ta e="T684" id="Seg_9794" s="T683">тот</ta>
            <ta e="T685" id="Seg_9795" s="T684">олень-PL-DAT/LOC</ta>
            <ta e="T686" id="Seg_9796" s="T685">там</ta>
            <ta e="T687" id="Seg_9797" s="T686">гора-DAT/LOC</ta>
            <ta e="T688" id="Seg_9798" s="T687">оставаться-PST2.[3SG]</ta>
            <ta e="T689" id="Seg_9799" s="T688">один</ta>
            <ta e="T690" id="Seg_9800" s="T689">верховой.олень-3SG.[NOM]</ta>
            <ta e="T691" id="Seg_9801" s="T690">следующее.утро-3SG-ACC</ta>
            <ta e="T692" id="Seg_9802" s="T691">кочевать-PRS-3PL</ta>
            <ta e="T693" id="Seg_9803" s="T692">опять</ta>
            <ta e="T694" id="Seg_9804" s="T693">кочевать-CVB.SEQ</ta>
            <ta e="T695" id="Seg_9805" s="T694">падать-CVB.SEQ</ta>
            <ta e="T696" id="Seg_9806" s="T695">после</ta>
            <ta e="T697" id="Seg_9807" s="T696">следующий</ta>
            <ta e="T698" id="Seg_9808" s="T697">человек-PL.[NOM]</ta>
            <ta e="T699" id="Seg_9809" s="T698">охранять-PRS-3PL</ta>
            <ta e="T700" id="Seg_9810" s="T699">этот</ta>
            <ta e="T701" id="Seg_9811" s="T700">далекий</ta>
            <ta e="T702" id="Seg_9812" s="T701">стоянка-DAT/LOC</ta>
            <ta e="T703" id="Seg_9813" s="T702">охранять-PTCP.PST</ta>
            <ta e="T704" id="Seg_9814" s="T703">по.очереди</ta>
            <ta e="T705" id="Seg_9815" s="T704">человек.[NOM]</ta>
            <ta e="T706" id="Seg_9816" s="T705">охранять-CVB.SIM</ta>
            <ta e="T707" id="Seg_9817" s="T706">выйти-CVB.SEQ</ta>
            <ta e="T708" id="Seg_9818" s="T707">верховой.олень-3SG-ACC</ta>
            <ta e="T709" id="Seg_9819" s="T708">тогда</ta>
            <ta e="T710" id="Seg_9820" s="T709">скучать-PRS.[3SG]</ta>
            <ta e="T711" id="Seg_9821" s="T710">утро</ta>
            <ta e="T712" id="Seg_9822" s="T711">приходить-CVB.SEQ</ta>
            <ta e="T713" id="Seg_9823" s="T712">говорить-PRS.[3SG]</ta>
            <ta e="T714" id="Seg_9824" s="T713">эй</ta>
            <ta e="T715" id="Seg_9825" s="T714">мой</ta>
            <ta e="T716" id="Seg_9826" s="T715">верховой.олень-EP-1SG.[NOM]</ta>
            <ta e="T717" id="Seg_9827" s="T716">NEG.EX</ta>
            <ta e="T718" id="Seg_9828" s="T717">там</ta>
            <ta e="T719" id="Seg_9829" s="T718">стоянка-DAT/LOC</ta>
            <ta e="T720" id="Seg_9830" s="T719">NEG</ta>
            <ta e="T721" id="Seg_9831" s="T720">видеть-PST2.NEG-EP-1SG</ta>
            <ta e="T722" id="Seg_9832" s="T721">давешний</ta>
            <ta e="T723" id="Seg_9833" s="T722">кочевать-PTCP.PST</ta>
            <ta e="T724" id="Seg_9834" s="T723">стоянка-1SG-DAT/LOC</ta>
            <ta e="T725" id="Seg_9835" s="T724">далекий</ta>
            <ta e="T726" id="Seg_9836" s="T725">стоянка-DAT/LOC</ta>
            <ta e="T727" id="Seg_9837" s="T726">охранять-PTCP.PRS-1SG-DAT/LOC</ta>
            <ta e="T728" id="Seg_9838" s="T727">садиться-CVB.SIM</ta>
            <ta e="T729" id="Seg_9839" s="T728">идти-EP-PTCP.PST</ta>
            <ta e="T730" id="Seg_9840" s="T729">олень-1SG.[NOM]</ta>
            <ta e="T731" id="Seg_9841" s="T730">быть-PST1-3SG</ta>
            <ta e="T732" id="Seg_9842" s="T731">правда.[NOM]</ta>
            <ta e="T733" id="Seg_9843" s="T732">да</ta>
            <ta e="T734" id="Seg_9844" s="T733">там</ta>
            <ta e="T735" id="Seg_9845" s="T734">стоянка-DAT/LOC</ta>
            <ta e="T736" id="Seg_9846" s="T735">оставаться-PST2.[3SG]</ta>
            <ta e="T737" id="Seg_9847" s="T736">MOD</ta>
            <ta e="T738" id="Seg_9848" s="T737">говорить-CVB.SIM</ta>
            <ta e="T739" id="Seg_9849" s="T738">говорить-PRS-3PL</ta>
            <ta e="T740" id="Seg_9850" s="T739">тот</ta>
            <ta e="T741" id="Seg_9851" s="T740">олень-ACC</ta>
            <ta e="T742" id="Seg_9852" s="T741">этот</ta>
            <ta e="T743" id="Seg_9853" s="T742">стоянка-3PL-DAT/LOC</ta>
            <ta e="T744" id="Seg_9854" s="T743">искать-CVB.SIM</ta>
            <ta e="T745" id="Seg_9855" s="T744">приходить-PRS-3PL</ta>
            <ta e="T746" id="Seg_9856" s="T745">сегодня</ta>
            <ta e="T747" id="Seg_9857" s="T746">кочевать-PTCP.PST</ta>
            <ta e="T748" id="Seg_9858" s="T747">стоянка-3PL-DAT/LOC</ta>
            <ta e="T749" id="Seg_9859" s="T748">тот</ta>
            <ta e="T750" id="Seg_9860" s="T749">олень-PL-ACC</ta>
            <ta e="T751" id="Seg_9861" s="T750">далекий</ta>
            <ta e="T752" id="Seg_9862" s="T751">аргиш-DAT/LOC</ta>
            <ta e="T753" id="Seg_9863" s="T752">оставаться-PTCP.PST</ta>
            <ta e="T754" id="Seg_9864" s="T753">олень-PL-ACC</ta>
            <ta e="T755" id="Seg_9865" s="T754">та.сторона.[NOM]</ta>
            <ta e="T756" id="Seg_9866" s="T755">стоянка-DAT/LOC</ta>
            <ta e="T757" id="Seg_9867" s="T756">искать-PRS-3PL</ta>
            <ta e="T758" id="Seg_9868" s="T757">тот</ta>
            <ta e="T759" id="Seg_9869" s="T758">далекий</ta>
            <ta e="T760" id="Seg_9870" s="T759">стоянка-DAT/LOC</ta>
            <ta e="T761" id="Seg_9871" s="T760">охранять-PTCP.PST</ta>
            <ta e="T762" id="Seg_9872" s="T761">пастух.[NOM]</ta>
            <ta e="T763" id="Seg_9873" s="T762">верховой.олень-3SG.[NOM]</ta>
            <ta e="T764" id="Seg_9874" s="T763">там</ta>
            <ta e="T765" id="Seg_9875" s="T764">оставаться-PST2.[3SG]</ta>
            <ta e="T766" id="Seg_9876" s="T765">тот</ta>
            <ta e="T767" id="Seg_9877" s="T766">гора-DAT/LOC</ta>
            <ta e="T768" id="Seg_9878" s="T767">тот</ta>
            <ta e="T769" id="Seg_9879" s="T768">одинокий</ta>
            <ta e="T770" id="Seg_9880" s="T769">олень-ACC</ta>
            <ta e="T771" id="Seg_9881" s="T770">помнить-PRS-3PL</ta>
            <ta e="T772" id="Seg_9882" s="T771">там</ta>
            <ta e="T773" id="Seg_9883" s="T772">оставаться-PST2-3SG</ta>
            <ta e="T774" id="Seg_9884" s="T773">десять-ABL</ta>
            <ta e="T775" id="Seg_9885" s="T774">больше</ta>
            <ta e="T776" id="Seg_9886" s="T775">олень.[NOM]</ta>
            <ta e="T777" id="Seg_9887" s="T776">тот.[NOM]</ta>
            <ta e="T778" id="Seg_9888" s="T777">из_за</ta>
            <ta e="T781" id="Seg_9889" s="T780">олень.[NOM]</ta>
            <ta e="T782" id="Seg_9890" s="T781">весть-POSS</ta>
            <ta e="T783" id="Seg_9891" s="T782">NEG</ta>
            <ta e="T784" id="Seg_9892" s="T783">пропадать-PRS-3SG.[NOM]</ta>
            <ta e="T785" id="Seg_9893" s="T784">тот.[NOM]</ta>
            <ta e="T786" id="Seg_9894" s="T785">потом</ta>
            <ta e="T787" id="Seg_9895" s="T786">год.[NOM]</ta>
            <ta e="T788" id="Seg_9896" s="T787">разный-разный.[NOM]</ta>
            <ta e="T789" id="Seg_9897" s="T788">быть-HAB.[3SG]</ta>
            <ta e="T790" id="Seg_9898" s="T789">теплый-VBZ-PTCP.FUT</ta>
            <ta e="T791" id="Seg_9899" s="T790">быть-HAB.[3SG]</ta>
            <ta e="T792" id="Seg_9900" s="T791">этот</ta>
            <ta e="T793" id="Seg_9901" s="T792">тепло-DAT/LOC</ta>
            <ta e="T794" id="Seg_9902" s="T793">опять</ta>
            <ta e="T795" id="Seg_9903" s="T794">теперь-ADJZ</ta>
            <ta e="T796" id="Seg_9904" s="T795">пастух-PL.[NOM]</ta>
            <ta e="T797" id="Seg_9905" s="T796">олень-ACC</ta>
            <ta e="T798" id="Seg_9906" s="T797">мочь-CVB.SEQ</ta>
            <ta e="T799" id="Seg_9907" s="T798">заботиться-NEG-3PL</ta>
            <ta e="T800" id="Seg_9908" s="T799">заботиться-NEG-3PL</ta>
            <ta e="T801" id="Seg_9909" s="T800">что-ACC=Q</ta>
            <ta e="T802" id="Seg_9910" s="T801">тепло-DAT/LOC</ta>
            <ta e="T803" id="Seg_9911" s="T802">ветер-AG.[NOM]</ta>
            <ta e="T804" id="Seg_9912" s="T803">олень.[NOM]</ta>
            <ta e="T805" id="Seg_9913" s="T804">быть-HAB.[3SG]</ta>
            <ta e="T806" id="Seg_9914" s="T805">ветер-AG.[NOM]</ta>
            <ta e="T807" id="Seg_9915" s="T806">олень.[NOM]</ta>
            <ta e="T808" id="Seg_9916" s="T807">имя-3SG.[NOM]</ta>
            <ta e="T809" id="Seg_9917" s="T808">ветер-ACC</ta>
            <ta e="T810" id="Seg_9918" s="T809">следовать-PRS.[3SG]</ta>
            <ta e="T811" id="Seg_9919" s="T810">этот</ta>
            <ta e="T812" id="Seg_9920" s="T811">ветер-ACC</ta>
            <ta e="T813" id="Seg_9921" s="T812">следовать-PTCP.PRS</ta>
            <ta e="T814" id="Seg_9922" s="T813">олень-ACC</ta>
            <ta e="T815" id="Seg_9923" s="T814">тепло.[NOM]</ta>
            <ta e="T816" id="Seg_9924" s="T815">проехать-PTCP.FUT.[3SG]-DAT/LOC</ta>
            <ta e="T817" id="Seg_9925" s="T816">пока</ta>
            <ta e="T818" id="Seg_9926" s="T817">человек.[NOM]</ta>
            <ta e="T819" id="Seg_9927" s="T818">олень-ABL</ta>
            <ta e="T820" id="Seg_9928" s="T819">связывать-HAB.[3SG]</ta>
            <ta e="T821" id="Seg_9929" s="T820">олень.[NOM]</ta>
            <ta e="T822" id="Seg_9930" s="T821">охранять-TEMP-3PL</ta>
            <ta e="T823" id="Seg_9931" s="T822">человек.[NOM]</ta>
            <ta e="T824" id="Seg_9932" s="T823">сила-3SG.[NOM]</ta>
            <ta e="T825" id="Seg_9933" s="T824">хватать-PTCP.HAB-3SG</ta>
            <ta e="T826" id="Seg_9934" s="T825">NEG.[3SG]</ta>
            <ta e="T827" id="Seg_9935" s="T826">тепло-DAT/LOC</ta>
            <ta e="T828" id="Seg_9936" s="T827">ветер-ACC</ta>
            <ta e="T830" id="Seg_9937" s="T929">делать-NEG-3PL</ta>
            <ta e="T831" id="Seg_9938" s="T830">утро</ta>
            <ta e="T832" id="Seg_9939" s="T831">принести-PTCP.PRS-3PL-DAT/LOC</ta>
            <ta e="T833" id="Seg_9940" s="T832">следующий</ta>
            <ta e="T834" id="Seg_9941" s="T833">пастух-DAT/LOC</ta>
            <ta e="T835" id="Seg_9942" s="T834">подать-PTCP.PRS-3PL-DAT/LOC</ta>
            <ta e="T836" id="Seg_9943" s="T835">1PL.[NOM]</ta>
            <ta e="T837" id="Seg_9944" s="T836">мочь-FUT-1PL</ta>
            <ta e="T838" id="Seg_9945" s="T837">NEG-3SG</ta>
            <ta e="T839" id="Seg_9946" s="T838">EVID</ta>
            <ta e="T840" id="Seg_9947" s="T839">говорить-CVB.SEQ</ta>
            <ta e="T841" id="Seg_9948" s="T840">когда</ta>
            <ta e="T842" id="Seg_9949" s="T841">NEG</ta>
            <ta e="T843" id="Seg_9950" s="T842">ветер.[NOM]</ta>
            <ta e="T844" id="Seg_9951" s="T843">нижняя.часть.[NOM]</ta>
            <ta e="T845" id="Seg_9952" s="T844">к</ta>
            <ta e="T846" id="Seg_9953" s="T845">направлять-NMNZ.[NOM]</ta>
            <ta e="T847" id="Seg_9954" s="T846">делать-PTCP.FUT-3PL-ACC</ta>
            <ta e="T848" id="Seg_9955" s="T847">дом-3PL.[NOM]</ta>
            <ta e="T849" id="Seg_9956" s="T848">ветер.[NOM]</ta>
            <ta e="T850" id="Seg_9957" s="T849">верхняя.часть.[NOM]</ta>
            <ta e="T851" id="Seg_9958" s="T850">этот</ta>
            <ta e="T852" id="Seg_9959" s="T851">быть-PRS.[3SG]</ta>
            <ta e="T853" id="Seg_9960" s="T852">подобно</ta>
            <ta e="T854" id="Seg_9961" s="T853">олень.[NOM]</ta>
            <ta e="T855" id="Seg_9962" s="T854">тогда</ta>
            <ta e="T856" id="Seg_9963" s="T855">мочь-NEG.CVB.SIM</ta>
            <ta e="T857" id="Seg_9964" s="T856">ветер.[NOM]</ta>
            <ta e="T858" id="Seg_9965" s="T857">навстречу</ta>
            <ta e="T859" id="Seg_9966" s="T858">приходить-TEMP-3SG</ta>
            <ta e="T860" id="Seg_9967" s="T859">дом-3SG-DAT/LOC</ta>
            <ta e="T861" id="Seg_9968" s="T860">толкать-CAUS-TEMP-3SG</ta>
            <ta e="T862" id="Seg_9969" s="T861">дом-ADJZ-3PL.[NOM]</ta>
            <ta e="T863" id="Seg_9970" s="T862">вставать-CVB.SEQ-3PL</ta>
            <ta e="T864" id="Seg_9971" s="T863">здесь</ta>
            <ta e="T865" id="Seg_9972" s="T864">помогать-CVB.SEQ-3PL</ta>
            <ta e="T866" id="Seg_9973" s="T865">тогда</ta>
            <ta e="T867" id="Seg_9974" s="T866">побеждать-PRS-3PL</ta>
            <ta e="T868" id="Seg_9975" s="T867">олень-ACC</ta>
            <ta e="T869" id="Seg_9976" s="T868">тот.[NOM]</ta>
            <ta e="T870" id="Seg_9977" s="T869">после</ta>
            <ta e="T871" id="Seg_9978" s="T870">3PL.[NOM]</ta>
            <ta e="T872" id="Seg_9979" s="T871">весна-ADJZ-ABL</ta>
            <ta e="T873" id="Seg_9980" s="T872">тепло.[NOM]</ta>
            <ta e="T874" id="Seg_9981" s="T873">падать-CVB.SIM</ta>
            <ta e="T875" id="Seg_9982" s="T874">еще.не-3SG</ta>
            <ta e="T876" id="Seg_9983" s="T875">только.что</ta>
            <ta e="T877" id="Seg_9984" s="T876">тепло.[NOM]</ta>
            <ta e="T878" id="Seg_9985" s="T877">падать-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T879" id="Seg_9986" s="T878">олень.[NOM]</ta>
            <ta e="T880" id="Seg_9987" s="T879">чувствовать-PTCP.PRS-3SG-DAT/LOC</ta>
            <ta e="T881" id="Seg_9988" s="T880">дым-DAT/LOC</ta>
            <ta e="T882" id="Seg_9989" s="T881">олень-ACC</ta>
            <ta e="T883" id="Seg_9990" s="T882">учить-PTCP.FUT-3PL-ACC</ta>
            <ta e="T884" id="Seg_9991" s="T883">надо</ta>
            <ta e="T885" id="Seg_9992" s="T884">большой</ta>
            <ta e="T886" id="Seg_9993" s="T885">тепло.[NOM]</ta>
            <ta e="T887" id="Seg_9994" s="T886">падать-CVB.SIM</ta>
            <ta e="T888" id="Seg_9995" s="T887">еще.не-3SG</ta>
            <ta e="T889" id="Seg_9996" s="T888">дым-ACC</ta>
            <ta e="T890" id="Seg_9997" s="T889">видеть-PST1-3SG</ta>
            <ta e="T891" id="Seg_9998" s="T890">да</ta>
            <ta e="T892" id="Seg_9999" s="T891">сколько</ta>
            <ta e="T893" id="Seg_10000" s="T892">да</ta>
            <ta e="T894" id="Seg_10001" s="T893">тепло-DAT/LOC</ta>
            <ta e="T896" id="Seg_10002" s="T894">олень.[NOM]</ta>
            <ta e="T897" id="Seg_10003" s="T896">дом-ADJZ-3PL.[NOM]</ta>
            <ta e="T898" id="Seg_10004" s="T897">тепло-DAT/LOC</ta>
            <ta e="T899" id="Seg_10005" s="T898">пастух-PL.[NOM]</ta>
            <ta e="T900" id="Seg_10006" s="T899">охранять-CVB.SIM</ta>
            <ta e="T901" id="Seg_10007" s="T900">идти-CVB.PURP-3PL</ta>
            <ta e="T902" id="Seg_10008" s="T901">говорить-PTCP.FUT</ta>
            <ta e="T903" id="Seg_10009" s="T902">падать-NEC-3PL</ta>
            <ta e="T904" id="Seg_10010" s="T903">эй</ta>
            <ta e="T905" id="Seg_10011" s="T904">тепло.[NOM]</ta>
            <ta e="T906" id="Seg_10012" s="T905">быть-TEMP-3SG</ta>
            <ta e="T907" id="Seg_10013" s="T906">вот</ta>
            <ta e="T908" id="Seg_10014" s="T907">опасаться-CVB.SEQ</ta>
            <ta e="T909" id="Seg_10015" s="T908">спать-EP-IMP.2PL</ta>
            <ta e="T910" id="Seg_10016" s="T909">дым-VBZ-EP-CAUS-FUT-EP-IMP.2PL</ta>
            <ta e="T911" id="Seg_10017" s="T910">костер.[NOM]</ta>
            <ta e="T912" id="Seg_10018" s="T911">отапливать-FUT-EP-IMP.2PL</ta>
            <ta e="T913" id="Seg_10019" s="T912">теплый</ta>
            <ta e="T914" id="Seg_10020" s="T913">быть-TEMP-3SG</ta>
            <ta e="T915" id="Seg_10021" s="T914">может</ta>
            <ta e="T916" id="Seg_10022" s="T915">мочь-FUT-1PL</ta>
            <ta e="T917" id="Seg_10023" s="T916">NEG-3SG</ta>
            <ta e="T918" id="Seg_10024" s="T917">как</ta>
            <ta e="T919" id="Seg_10025" s="T918">INDEF</ta>
            <ta e="T920" id="Seg_10026" s="T919">теплый.[NOM]</ta>
            <ta e="T921" id="Seg_10027" s="T920">быть-CVB.SEQ</ta>
            <ta e="T922" id="Seg_10028" s="T921">быть-PRS.[3SG]</ta>
            <ta e="T923" id="Seg_10029" s="T922">тот.[NOM]</ta>
            <ta e="T924" id="Seg_10030" s="T923">подобно</ta>
            <ta e="T925" id="Seg_10031" s="T924">заботиться-PTCP.HAB</ta>
            <ta e="T926" id="Seg_10032" s="T925">быть-PST1-1PL</ta>
            <ta e="T927" id="Seg_10033" s="T926">1PL.[NOM]</ta>
            <ta e="T928" id="Seg_10034" s="T927">олень-ACC</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_10035" s="T0">ptcl</ta>
            <ta e="T2" id="Seg_10036" s="T1">pers-pro:case</ta>
            <ta e="T3" id="Seg_10037" s="T2">n-n&gt;n-n:(num)-n:case</ta>
            <ta e="T4" id="Seg_10038" s="T3">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T5" id="Seg_10039" s="T4">v-v:tense-v:pred.pn</ta>
            <ta e="T6" id="Seg_10040" s="T5">pers-pro:case</ta>
            <ta e="T7" id="Seg_10041" s="T6">quant-quant&gt;adv</ta>
            <ta e="T8" id="Seg_10042" s="T7">v-v:ptcp</ta>
            <ta e="T9" id="Seg_10043" s="T8">n-n:(pred.pn)</ta>
            <ta e="T10" id="Seg_10044" s="T9">n-n:case</ta>
            <ta e="T11" id="Seg_10045" s="T10">adv</ta>
            <ta e="T12" id="Seg_10046" s="T11">dempro</ta>
            <ta e="T13" id="Seg_10047" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_10048" s="T13">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T15" id="Seg_10049" s="T14">n-n:poss-n:case</ta>
            <ta e="T16" id="Seg_10050" s="T15">v-v:tense-v:poss.pn</ta>
            <ta e="T17" id="Seg_10051" s="T16">adv</ta>
            <ta e="T18" id="Seg_10052" s="T17">que</ta>
            <ta e="T19" id="Seg_10053" s="T18">ptcl</ta>
            <ta e="T20" id="Seg_10054" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_10055" s="T20">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T22" id="Seg_10056" s="T21">v-v:tense-v:pred.pn</ta>
            <ta e="T23" id="Seg_10057" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_10058" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_10059" s="T24">conj</ta>
            <ta e="T26" id="Seg_10060" s="T25">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T27" id="Seg_10061" s="T26">n-n:case</ta>
            <ta e="T28" id="Seg_10062" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_10063" s="T28">adv</ta>
            <ta e="T30" id="Seg_10064" s="T29">pers-pro:case</ta>
            <ta e="T31" id="Seg_10065" s="T30">v-v:tense-v:pred.pn</ta>
            <ta e="T32" id="Seg_10066" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_10067" s="T32">adv</ta>
            <ta e="T34" id="Seg_10068" s="T33">n-n:case</ta>
            <ta e="T35" id="Seg_10069" s="T34">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T36" id="Seg_10070" s="T35">n-n:(num)-n:case</ta>
            <ta e="T37" id="Seg_10071" s="T36">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T38" id="Seg_10072" s="T37">pers-pro:case</ta>
            <ta e="T39" id="Seg_10073" s="T38">v-v:ptcp-v:(ins)-v:(poss)-v:(case)</ta>
            <ta e="T40" id="Seg_10074" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_10075" s="T40">adv-adv&gt;adv</ta>
            <ta e="T42" id="Seg_10076" s="T41">adv</ta>
            <ta e="T43" id="Seg_10077" s="T42">dempro</ta>
            <ta e="T44" id="Seg_10078" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_10079" s="T44">v-v:tense-v:pred.pn</ta>
            <ta e="T46" id="Seg_10080" s="T45">pers-pro:case</ta>
            <ta e="T47" id="Seg_10081" s="T46">adv</ta>
            <ta e="T48" id="Seg_10082" s="T47">v-v:cvb</ta>
            <ta e="T49" id="Seg_10083" s="T48">v-v:ptcp</ta>
            <ta e="T50" id="Seg_10084" s="T49">v-v:mood-v:temp.pn</ta>
            <ta e="T51" id="Seg_10085" s="T50">n-n:case</ta>
            <ta e="T52" id="Seg_10086" s="T51">v-v:mood-v:temp.pn</ta>
            <ta e="T53" id="Seg_10087" s="T52">quant</ta>
            <ta e="T54" id="Seg_10088" s="T53">n-n&gt;adj-n:case</ta>
            <ta e="T55" id="Seg_10089" s="T54">v-v:tense-v:pred.pn</ta>
            <ta e="T56" id="Seg_10090" s="T55">n-n:case</ta>
            <ta e="T57" id="Seg_10091" s="T56">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T58" id="Seg_10092" s="T57">n-n:case</ta>
            <ta e="T59" id="Seg_10093" s="T58">v-v:mood-v:temp.pn</ta>
            <ta e="T60" id="Seg_10094" s="T59">v-v:cvb</ta>
            <ta e="T61" id="Seg_10095" s="T60">v-v:cvb-v:pred.pn</ta>
            <ta e="T62" id="Seg_10096" s="T61">v-v:ptcp</ta>
            <ta e="T63" id="Seg_10097" s="T62">v-v:tense-v:poss.pn</ta>
            <ta e="T64" id="Seg_10098" s="T63">n-n:case</ta>
            <ta e="T65" id="Seg_10099" s="T64">v-v:cvb</ta>
            <ta e="T66" id="Seg_10100" s="T65">n-n:poss-n:case</ta>
            <ta e="T67" id="Seg_10101" s="T66">v-v:mood-v:temp.pn</ta>
            <ta e="T68" id="Seg_10102" s="T67">adj-adj-adj&gt;adv</ta>
            <ta e="T69" id="Seg_10103" s="T68">v-v:ptcp</ta>
            <ta e="T70" id="Seg_10104" s="T69">n-n:case</ta>
            <ta e="T71" id="Seg_10105" s="T70">indfpro</ta>
            <ta e="T72" id="Seg_10106" s="T71">n-n:case</ta>
            <ta e="T73" id="Seg_10107" s="T72">n-n:(poss)-n:case</ta>
            <ta e="T74" id="Seg_10108" s="T73">v-v:cvb</ta>
            <ta e="T75" id="Seg_10109" s="T74">post</ta>
            <ta e="T76" id="Seg_10110" s="T75">v-v:cvb</ta>
            <ta e="T77" id="Seg_10111" s="T76">v-v:mood-v:pred.pn</ta>
            <ta e="T78" id="Seg_10112" s="T77">v-v:cvb</ta>
            <ta e="T79" id="Seg_10113" s="T78">v-v:cvb</ta>
            <ta e="T80" id="Seg_10114" s="T79">n-n:poss-n:case</ta>
            <ta e="T81" id="Seg_10115" s="T80">v-v:ptcp-v:(case)</ta>
            <ta e="T82" id="Seg_10116" s="T81">adj-n:case</ta>
            <ta e="T83" id="Seg_10117" s="T82">ptcl</ta>
            <ta e="T84" id="Seg_10118" s="T83">n-n:poss-n:case</ta>
            <ta e="T85" id="Seg_10119" s="T84">v-v:ptcp-v:(case)</ta>
            <ta e="T86" id="Seg_10120" s="T85">adj-n:case</ta>
            <ta e="T87" id="Seg_10121" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_10122" s="T87">v-v:tense-v:pred.pn</ta>
            <ta e="T89" id="Seg_10123" s="T88">dempro</ta>
            <ta e="T90" id="Seg_10124" s="T89">v-v:cvb</ta>
            <ta e="T91" id="Seg_10125" s="T90">v-v:ptcp-v:(case)</ta>
            <ta e="T92" id="Seg_10126" s="T91">v-v&gt;v-v:ptcp-v:(case)</ta>
            <ta e="T93" id="Seg_10127" s="T92">ptcl</ta>
            <ta e="T94" id="Seg_10128" s="T93">v-v&gt;v-v:cvb</ta>
            <ta e="T95" id="Seg_10129" s="T94">v-v:ptcp-v:(case)</ta>
            <ta e="T96" id="Seg_10130" s="T95">que-pro:case</ta>
            <ta e="T97" id="Seg_10131" s="T96">v-v:cvb</ta>
            <ta e="T98" id="Seg_10132" s="T97">que</ta>
            <ta e="T99" id="Seg_10133" s="T98">v-v:(neg)-v:pred.pn</ta>
            <ta e="T100" id="Seg_10134" s="T99">v-v:cvb</ta>
            <ta e="T101" id="Seg_10135" s="T100">dempro-pro:case</ta>
            <ta e="T102" id="Seg_10136" s="T101">v-v:ptcp-v:(case)</ta>
            <ta e="T103" id="Seg_10137" s="T102">n-n:poss-n:case</ta>
            <ta e="T104" id="Seg_10138" s="T103">v-v:ptcp-v:(case)</ta>
            <ta e="T105" id="Seg_10139" s="T104">n-n:case</ta>
            <ta e="T106" id="Seg_10140" s="T105">v-v:mood-v:temp.pn</ta>
            <ta e="T107" id="Seg_10141" s="T106">n-n:case</ta>
            <ta e="T108" id="Seg_10142" s="T107">adj-n:(poss)</ta>
            <ta e="T109" id="Seg_10143" s="T108">n-n&gt;adj-n:case</ta>
            <ta e="T110" id="Seg_10144" s="T109">v-v:mood-v:pred.pn</ta>
            <ta e="T111" id="Seg_10145" s="T110">dempro</ta>
            <ta e="T112" id="Seg_10146" s="T111">n-n:poss-n:case</ta>
            <ta e="T113" id="Seg_10147" s="T112">indfpro</ta>
            <ta e="T114" id="Seg_10148" s="T113">n-n:case</ta>
            <ta e="T115" id="Seg_10149" s="T114">v-v:mood-v:pred.pn</ta>
            <ta e="T116" id="Seg_10150" s="T115">dempro</ta>
            <ta e="T117" id="Seg_10151" s="T116">n-n:(poss)-n:case</ta>
            <ta e="T118" id="Seg_10152" s="T117">v-v&gt;v-v:mood-v:temp.pn</ta>
            <ta e="T119" id="Seg_10153" s="T118">dempro-pro:case</ta>
            <ta e="T120" id="Seg_10154" s="T119">n-n:(ins)-n:case</ta>
            <ta e="T121" id="Seg_10155" s="T120">adv</ta>
            <ta e="T122" id="Seg_10156" s="T121">n-n:case</ta>
            <ta e="T123" id="Seg_10157" s="T122">v-v:tense-v:pred.pn</ta>
            <ta e="T124" id="Seg_10158" s="T123">dempro-pro:case</ta>
            <ta e="T125" id="Seg_10159" s="T124">n-n:(poss)-n:case</ta>
            <ta e="T126" id="Seg_10160" s="T125">v-v:tense-v:pred.pn</ta>
            <ta e="T127" id="Seg_10161" s="T126">n-n:(ins)-n:case</ta>
            <ta e="T128" id="Seg_10162" s="T127">adv</ta>
            <ta e="T129" id="Seg_10163" s="T128">v-v:ptcp-v:(case)</ta>
            <ta e="T130" id="Seg_10164" s="T129">dempro</ta>
            <ta e="T131" id="Seg_10165" s="T130">n-n:case</ta>
            <ta e="T132" id="Seg_10166" s="T131">dempro</ta>
            <ta e="T133" id="Seg_10167" s="T132">n-n:case</ta>
            <ta e="T134" id="Seg_10168" s="T133">v-v:cvb</ta>
            <ta e="T135" id="Seg_10169" s="T134">v-v:mood-v:pred.pn</ta>
            <ta e="T136" id="Seg_10170" s="T135">dempro</ta>
            <ta e="T137" id="Seg_10171" s="T136">n-n:(poss)-n:case</ta>
            <ta e="T138" id="Seg_10172" s="T137">n-n:poss-n:case</ta>
            <ta e="T139" id="Seg_10173" s="T138">v-v:tense-v:pred.pn</ta>
            <ta e="T140" id="Seg_10174" s="T139">dempro-pro:case</ta>
            <ta e="T141" id="Seg_10175" s="T140">v-v:tense-v:pred.pn</ta>
            <ta e="T142" id="Seg_10176" s="T141">n-n:case</ta>
            <ta e="T143" id="Seg_10177" s="T142">adj-n:(poss)-n:case</ta>
            <ta e="T144" id="Seg_10178" s="T143">n-n:poss-n:case</ta>
            <ta e="T145" id="Seg_10179" s="T144">adj-n:(poss)-n:case</ta>
            <ta e="T146" id="Seg_10180" s="T145">n-n:(poss)-n:case</ta>
            <ta e="T147" id="Seg_10181" s="T146">v-v:mood-v:temp.pn</ta>
            <ta e="T148" id="Seg_10182" s="T147">adv</ta>
            <ta e="T149" id="Seg_10183" s="T148">cardnum</ta>
            <ta e="T150" id="Seg_10184" s="T149">n-n&gt;adj-n:case</ta>
            <ta e="T151" id="Seg_10185" s="T150">adj-n:case</ta>
            <ta e="T152" id="Seg_10186" s="T151">n-n:case</ta>
            <ta e="T153" id="Seg_10187" s="T152">v-v:tense-v:pred.pn</ta>
            <ta e="T154" id="Seg_10188" s="T153">adj</ta>
            <ta e="T155" id="Seg_10189" s="T154">n-n:(poss)-n:case</ta>
            <ta e="T156" id="Seg_10190" s="T155">dempro</ta>
            <ta e="T157" id="Seg_10191" s="T156">n-n:(poss)-n:case</ta>
            <ta e="T158" id="Seg_10192" s="T157">v-v:mood-v:temp.pn</ta>
            <ta e="T159" id="Seg_10193" s="T158">n-n:case</ta>
            <ta e="T160" id="Seg_10194" s="T159">v-v:cvb</ta>
            <ta e="T161" id="Seg_10195" s="T160">n-n&gt;v-v:mood-v:pred.pn</ta>
            <ta e="T162" id="Seg_10196" s="T161">ptcl</ta>
            <ta e="T163" id="Seg_10197" s="T162">n-n:(poss)-n:case</ta>
            <ta e="T164" id="Seg_10198" s="T163">v-v:ptcp-v:(poss)</ta>
            <ta e="T165" id="Seg_10199" s="T164">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T166" id="Seg_10200" s="T165">dempro</ta>
            <ta e="T167" id="Seg_10201" s="T166">n-n:case</ta>
            <ta e="T168" id="Seg_10202" s="T167">v-v&gt;v-v:cvb</ta>
            <ta e="T169" id="Seg_10203" s="T168">v-v:ptcp-v:(case)</ta>
            <ta e="T170" id="Seg_10204" s="T169">adv</ta>
            <ta e="T171" id="Seg_10205" s="T170">adj-n:case</ta>
            <ta e="T172" id="Seg_10206" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_10207" s="T172">v-v:tense-v:pred.pn</ta>
            <ta e="T174" id="Seg_10208" s="T173">dempro</ta>
            <ta e="T175" id="Seg_10209" s="T174">n-n:case</ta>
            <ta e="T176" id="Seg_10210" s="T175">v-v:cvb</ta>
            <ta e="T177" id="Seg_10211" s="T176">ptcl</ta>
            <ta e="T178" id="Seg_10212" s="T177">v-v:mood-v:pred.pn</ta>
            <ta e="T179" id="Seg_10213" s="T178">dempro</ta>
            <ta e="T180" id="Seg_10214" s="T179">n-n:case</ta>
            <ta e="T181" id="Seg_10215" s="T180">adv</ta>
            <ta e="T182" id="Seg_10216" s="T181">v-v:cvb</ta>
            <ta e="T183" id="Seg_10217" s="T182">post</ta>
            <ta e="T184" id="Seg_10218" s="T183">n-n:poss-n:case</ta>
            <ta e="T185" id="Seg_10219" s="T184">v-v:ptcp-v:(case)</ta>
            <ta e="T186" id="Seg_10220" s="T185">adv</ta>
            <ta e="T187" id="Seg_10221" s="T186">dempro</ta>
            <ta e="T188" id="Seg_10222" s="T187">n-n:(poss)-n:case</ta>
            <ta e="T189" id="Seg_10223" s="T188">n-n:case</ta>
            <ta e="T190" id="Seg_10224" s="T189">adj-n:(poss)-n:case</ta>
            <ta e="T191" id="Seg_10225" s="T190">que</ta>
            <ta e="T192" id="Seg_10226" s="T191">ptcl</ta>
            <ta e="T193" id="Seg_10227" s="T192">n-n:case</ta>
            <ta e="T194" id="Seg_10228" s="T193">v-v:mood-v:temp.pn</ta>
            <ta e="T195" id="Seg_10229" s="T194">n-n:(poss)-n:case</ta>
            <ta e="T196" id="Seg_10230" s="T195">adj-n:case</ta>
            <ta e="T197" id="Seg_10231" s="T196">v-v:tense-v:pred.pn</ta>
            <ta e="T198" id="Seg_10232" s="T197">dempro-pro:case</ta>
            <ta e="T199" id="Seg_10233" s="T198">n-n:poss-n:case</ta>
            <ta e="T200" id="Seg_10234" s="T199">v-v:ptcp-v:(case)</ta>
            <ta e="T201" id="Seg_10235" s="T200">v-v:cvb-v-v:cvb</ta>
            <ta e="T202" id="Seg_10236" s="T201">v-v:cvb</ta>
            <ta e="T203" id="Seg_10237" s="T202">v-v:mood-v:pred.pn</ta>
            <ta e="T204" id="Seg_10238" s="T203">v-v:mood-v:temp.pn</ta>
            <ta e="T205" id="Seg_10239" s="T204">dempro-pro:case</ta>
            <ta e="T206" id="Seg_10240" s="T205">n-n:poss-n:case</ta>
            <ta e="T207" id="Seg_10241" s="T206">v-v:cvb</ta>
            <ta e="T208" id="Seg_10242" s="T207">post</ta>
            <ta e="T209" id="Seg_10243" s="T208">n-n:poss-n:case</ta>
            <ta e="T210" id="Seg_10244" s="T209">v-v&gt;v-v:ptcp-v:(case)</ta>
            <ta e="T211" id="Seg_10245" s="T210">ptcl</ta>
            <ta e="T212" id="Seg_10246" s="T211">dempro</ta>
            <ta e="T213" id="Seg_10247" s="T212">n-n:case</ta>
            <ta e="T214" id="Seg_10248" s="T213">n-n:poss-n:case</ta>
            <ta e="T215" id="Seg_10249" s="T214">que-pro:case</ta>
            <ta e="T216" id="Seg_10250" s="T215">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T217" id="Seg_10251" s="T216">post</ta>
            <ta e="T218" id="Seg_10252" s="T217">v-v:cvb</ta>
            <ta e="T219" id="Seg_10253" s="T218">v-v:ptcp-v:(poss)</ta>
            <ta e="T220" id="Seg_10254" s="T219">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T221" id="Seg_10255" s="T220">dempro-pro:case</ta>
            <ta e="T222" id="Seg_10256" s="T221">n-n:(poss)-n:case</ta>
            <ta e="T223" id="Seg_10257" s="T222">n-n:(poss)-n:case</ta>
            <ta e="T224" id="Seg_10258" s="T223">n-n&gt;adj-n:case</ta>
            <ta e="T225" id="Seg_10259" s="T224">v-v:tense-v:poss.pn</ta>
            <ta e="T226" id="Seg_10260" s="T225">adj-n:case</ta>
            <ta e="T227" id="Seg_10261" s="T226">dempro</ta>
            <ta e="T228" id="Seg_10262" s="T227">n-n:case</ta>
            <ta e="T229" id="Seg_10263" s="T228">v-v:mood-v:pred.pn</ta>
            <ta e="T230" id="Seg_10264" s="T229">n-n:case</ta>
            <ta e="T231" id="Seg_10265" s="T230">v-v:cvb</ta>
            <ta e="T232" id="Seg_10266" s="T231">post</ta>
            <ta e="T233" id="Seg_10267" s="T232">adj</ta>
            <ta e="T234" id="Seg_10268" s="T233">n-n:(poss)-n:case</ta>
            <ta e="T235" id="Seg_10269" s="T234">v-v:mood-v:temp.pn</ta>
            <ta e="T236" id="Seg_10270" s="T235">adv</ta>
            <ta e="T237" id="Seg_10271" s="T236">v-v&gt;v-v:mood-v:pred.pn</ta>
            <ta e="T238" id="Seg_10272" s="T237">adv</ta>
            <ta e="T239" id="Seg_10273" s="T238">v-v&gt;v-v:mood-v:temp.pn</ta>
            <ta e="T240" id="Seg_10274" s="T239">dempro</ta>
            <ta e="T241" id="Seg_10275" s="T240">n-n:case</ta>
            <ta e="T242" id="Seg_10276" s="T241">n-n:case</ta>
            <ta e="T243" id="Seg_10277" s="T242">v-v:tense-v:pred.pn</ta>
            <ta e="T244" id="Seg_10278" s="T243">n-n:(num)-n:case</ta>
            <ta e="T245" id="Seg_10279" s="T244">v-v:tense-v:pred.pn</ta>
            <ta e="T246" id="Seg_10280" s="T245">quant</ta>
            <ta e="T247" id="Seg_10281" s="T246">n-n:case</ta>
            <ta e="T248" id="Seg_10282" s="T247">v-v:mood-v:temp.pn</ta>
            <ta e="T249" id="Seg_10283" s="T248">que</ta>
            <ta e="T250" id="Seg_10284" s="T249">ptcl</ta>
            <ta e="T251" id="Seg_10285" s="T250">v-v:cvb-ptcl</ta>
            <ta e="T252" id="Seg_10286" s="T251">n-n:case</ta>
            <ta e="T253" id="Seg_10287" s="T252">n-n:case</ta>
            <ta e="T254" id="Seg_10288" s="T253">post</ta>
            <ta e="T255" id="Seg_10289" s="T254">v-v:mood-v:pred.pn</ta>
            <ta e="T256" id="Seg_10290" s="T255">v-v:ptcp</ta>
            <ta e="T257" id="Seg_10291" s="T256">n-n:case</ta>
            <ta e="T258" id="Seg_10292" s="T257">n-n:(poss)-n:case</ta>
            <ta e="T259" id="Seg_10293" s="T258">v-v:cvb</ta>
            <ta e="T260" id="Seg_10294" s="T259">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T261" id="Seg_10295" s="T260">post</ta>
            <ta e="T262" id="Seg_10296" s="T261">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T263" id="Seg_10297" s="T262">post</ta>
            <ta e="T264" id="Seg_10298" s="T263">n-n:case</ta>
            <ta e="T265" id="Seg_10299" s="T264">post</ta>
            <ta e="T266" id="Seg_10300" s="T265">v-v:tense-v:pred.pn</ta>
            <ta e="T267" id="Seg_10301" s="T266">dempro</ta>
            <ta e="T268" id="Seg_10302" s="T267">v-v&gt;n-n:poss-n:case</ta>
            <ta e="T269" id="Seg_10303" s="T268">dempro</ta>
            <ta e="T270" id="Seg_10304" s="T269">ptcl</ta>
            <ta e="T271" id="Seg_10305" s="T270">cardnum-cardnum:case</ta>
            <ta e="T272" id="Seg_10306" s="T271">post</ta>
            <ta e="T273" id="Seg_10307" s="T272">cardnum-cardnum&gt;cardnum</ta>
            <ta e="T274" id="Seg_10308" s="T273">n-n:case</ta>
            <ta e="T275" id="Seg_10309" s="T274">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T276" id="Seg_10310" s="T275">post</ta>
            <ta e="T277" id="Seg_10311" s="T276">adv</ta>
            <ta e="T278" id="Seg_10312" s="T277">dempro</ta>
            <ta e="T279" id="Seg_10313" s="T278">v-v:cvb-ptcl</ta>
            <ta e="T280" id="Seg_10314" s="T279">n-n:case</ta>
            <ta e="T281" id="Seg_10315" s="T280">n-n:case</ta>
            <ta e="T282" id="Seg_10316" s="T281">post</ta>
            <ta e="T283" id="Seg_10317" s="T282">v-v:ptcp-v:(case)</ta>
            <ta e="T284" id="Seg_10318" s="T283">ptcl</ta>
            <ta e="T285" id="Seg_10319" s="T284">dempro</ta>
            <ta e="T286" id="Seg_10320" s="T285">n-n:case</ta>
            <ta e="T287" id="Seg_10321" s="T286">v-v:ptcp-v:(case)</ta>
            <ta e="T288" id="Seg_10322" s="T287">indfpro</ta>
            <ta e="T289" id="Seg_10323" s="T288">n-n:case</ta>
            <ta e="T290" id="Seg_10324" s="T289">dempro</ta>
            <ta e="T291" id="Seg_10325" s="T290">v-v:ptcp</ta>
            <ta e="T292" id="Seg_10326" s="T291">n-n&gt;adj-n:case</ta>
            <ta e="T293" id="Seg_10327" s="T292">v-v:tense-v:pred.pn</ta>
            <ta e="T294" id="Seg_10328" s="T293">indfpro</ta>
            <ta e="T295" id="Seg_10329" s="T294">n-n:case</ta>
            <ta e="T296" id="Seg_10330" s="T295">dempro</ta>
            <ta e="T297" id="Seg_10331" s="T296">v</ta>
            <ta e="T298" id="Seg_10332" s="T297">v-v:cvb-ptcl</ta>
            <ta e="T299" id="Seg_10333" s="T298">n-n:case</ta>
            <ta e="T300" id="Seg_10334" s="T299">v-v:tense-v:pred.pn</ta>
            <ta e="T301" id="Seg_10335" s="T300">dempro</ta>
            <ta e="T302" id="Seg_10336" s="T301">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T303" id="Seg_10337" s="T302">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T304" id="Seg_10338" s="T303">n-n&gt;v-v:cvb</ta>
            <ta e="T305" id="Seg_10339" s="T304">adj</ta>
            <ta e="T306" id="Seg_10340" s="T305">n-n:case</ta>
            <ta e="T307" id="Seg_10341" s="T306">v-v&gt;v-v:cvb</ta>
            <ta e="T308" id="Seg_10342" s="T307">n-n:(poss)-n:case</ta>
            <ta e="T309" id="Seg_10343" s="T308">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T310" id="Seg_10344" s="T309">v-v:cvb</ta>
            <ta e="T311" id="Seg_10345" s="T310">dempro</ta>
            <ta e="T312" id="Seg_10346" s="T311">n-n&gt;adj</ta>
            <ta e="T314" id="Seg_10347" s="T312">n-n:case</ta>
            <ta e="T315" id="Seg_10348" s="T314">que</ta>
            <ta e="T316" id="Seg_10349" s="T315">ptcl</ta>
            <ta e="T317" id="Seg_10350" s="T316">n-n:case</ta>
            <ta e="T318" id="Seg_10351" s="T317">v-v&gt;v-v:cvb</ta>
            <ta e="T319" id="Seg_10352" s="T318">n-n:poss-n:case</ta>
            <ta e="T320" id="Seg_10353" s="T319">v-v:mood-v:pred.pn</ta>
            <ta e="T321" id="Seg_10354" s="T320">v-v:cvb</ta>
            <ta e="T322" id="Seg_10355" s="T321">n-n:poss-n:case</ta>
            <ta e="T323" id="Seg_10356" s="T322">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T324" id="Seg_10357" s="T323">dempro-pro:case</ta>
            <ta e="T325" id="Seg_10358" s="T324">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T326" id="Seg_10359" s="T325">v-v:cvb</ta>
            <ta e="T327" id="Seg_10360" s="T326">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T328" id="Seg_10361" s="T327">v-v:cvb</ta>
            <ta e="T329" id="Seg_10362" s="T328">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T330" id="Seg_10363" s="T329">v-v:cvb-ptcl</ta>
            <ta e="T331" id="Seg_10364" s="T330">n-n:poss-n:case</ta>
            <ta e="T332" id="Seg_10365" s="T331">n-n:case</ta>
            <ta e="T333" id="Seg_10366" s="T332">post</ta>
            <ta e="T334" id="Seg_10367" s="T333">v-v:cvb</ta>
            <ta e="T335" id="Seg_10368" s="T334">v-v:tense-v:pred.pn</ta>
            <ta e="T336" id="Seg_10369" s="T335">dempro</ta>
            <ta e="T337" id="Seg_10370" s="T336">v-v:mood-v:temp.pn</ta>
            <ta e="T338" id="Seg_10371" s="T337">indfpro</ta>
            <ta e="T339" id="Seg_10372" s="T338">n-n:(num)-n:case</ta>
            <ta e="T340" id="Seg_10373" s="T339">adv</ta>
            <ta e="T341" id="Seg_10374" s="T340">adj-n:case</ta>
            <ta e="T342" id="Seg_10375" s="T341">v-v:mood-v:pred.pn</ta>
            <ta e="T343" id="Seg_10376" s="T342">dempro</ta>
            <ta e="T344" id="Seg_10377" s="T343">adj-n:case</ta>
            <ta e="T345" id="Seg_10378" s="T344">v-v:mood-v:temp.pn</ta>
            <ta e="T346" id="Seg_10379" s="T345">dempro</ta>
            <ta e="T347" id="Seg_10380" s="T346">n-n:case</ta>
            <ta e="T348" id="Seg_10381" s="T347">n-n:poss-n:case</ta>
            <ta e="T349" id="Seg_10382" s="T348">dempro</ta>
            <ta e="T350" id="Seg_10383" s="T349">v-v:cvb-ptcl</ta>
            <ta e="T351" id="Seg_10384" s="T350">n-n&gt;adj-n:case</ta>
            <ta e="T352" id="Seg_10385" s="T351">n-n&gt;adj-n:poss-n:case</ta>
            <ta e="T353" id="Seg_10386" s="T352">n-n:poss-n:case</ta>
            <ta e="T354" id="Seg_10387" s="T353">dempro</ta>
            <ta e="T355" id="Seg_10388" s="T354">dempro-pro:case</ta>
            <ta e="T356" id="Seg_10389" s="T355">v-v:ptcp</ta>
            <ta e="T357" id="Seg_10390" s="T356">n-n&gt;adj-n:poss-n:case</ta>
            <ta e="T358" id="Seg_10391" s="T357">n-n:(poss)-n:case</ta>
            <ta e="T359" id="Seg_10392" s="T358">n-n:case</ta>
            <ta e="T360" id="Seg_10393" s="T359">post</ta>
            <ta e="T361" id="Seg_10394" s="T360">n-n:(num)-n:case</ta>
            <ta e="T362" id="Seg_10395" s="T361">v-v:cvb-v:pred.pn</ta>
            <ta e="T363" id="Seg_10396" s="T362">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T364" id="Seg_10397" s="T363">n-n:case</ta>
            <ta e="T365" id="Seg_10398" s="T364">v-v:cvb</ta>
            <ta e="T366" id="Seg_10399" s="T365">v-v:ptcp-v:(poss)</ta>
            <ta e="T367" id="Seg_10400" s="T366">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T368" id="Seg_10401" s="T367">dempro-pro:case</ta>
            <ta e="T369" id="Seg_10402" s="T368">dempro-n:case</ta>
            <ta e="T370" id="Seg_10403" s="T369">post</ta>
            <ta e="T371" id="Seg_10404" s="T370">adj-n:case</ta>
            <ta e="T372" id="Seg_10405" s="T371">v-v:ptcp-v:(case)</ta>
            <ta e="T373" id="Seg_10406" s="T372">v-v:cvb</ta>
            <ta e="T374" id="Seg_10407" s="T373">post</ta>
            <ta e="T375" id="Seg_10408" s="T374">adv</ta>
            <ta e="T376" id="Seg_10409" s="T375">ptcl</ta>
            <ta e="T377" id="Seg_10410" s="T376">dempro</ta>
            <ta e="T378" id="Seg_10411" s="T377">n-n:(num)-n:case</ta>
            <ta e="T379" id="Seg_10412" s="T378">que</ta>
            <ta e="T380" id="Seg_10413" s="T379">conj</ta>
            <ta e="T381" id="Seg_10414" s="T380">n-n&gt;adj</ta>
            <ta e="T382" id="Seg_10415" s="T381">n-n:(ins)-n:case</ta>
            <ta e="T383" id="Seg_10416" s="T382">v-v:tense-v:pred.pn</ta>
            <ta e="T384" id="Seg_10417" s="T383">n-n:case</ta>
            <ta e="T385" id="Seg_10418" s="T384">v-v:tense-v:pred.pn</ta>
            <ta e="T386" id="Seg_10419" s="T385">n-n:case</ta>
            <ta e="T387" id="Seg_10420" s="T386">v-v:(ins)-v&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T388" id="Seg_10421" s="T387">n-n&gt;adj</ta>
            <ta e="T389" id="Seg_10422" s="T388">n-n:(ins)-n:case</ta>
            <ta e="T390" id="Seg_10423" s="T389">n-n&gt;adj</ta>
            <ta e="T391" id="Seg_10424" s="T390">n-n:(ins)-n:case</ta>
            <ta e="T392" id="Seg_10425" s="T391">n-n:(num)-n&gt;adj</ta>
            <ta e="T393" id="Seg_10426" s="T392">n-n:(num)-n:(ins)-n:case</ta>
            <ta e="T394" id="Seg_10427" s="T393">n-n:(num)-n&gt;adj</ta>
            <ta e="T395" id="Seg_10428" s="T394">n-n:(num)-n:(ins)-n:case</ta>
            <ta e="T396" id="Seg_10429" s="T395">dempro</ta>
            <ta e="T397" id="Seg_10430" s="T396">adj</ta>
            <ta e="T398" id="Seg_10431" s="T397">n-n:case</ta>
            <ta e="T399" id="Seg_10432" s="T398">v-v:mood-v:pred.pn</ta>
            <ta e="T400" id="Seg_10433" s="T399">dempro</ta>
            <ta e="T401" id="Seg_10434" s="T400">v-v:mood-v:temp.pn</ta>
            <ta e="T402" id="Seg_10435" s="T401">adv</ta>
            <ta e="T403" id="Seg_10436" s="T402">n-n:case</ta>
            <ta e="T404" id="Seg_10437" s="T403">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T405" id="Seg_10438" s="T404">post</ta>
            <ta e="T406" id="Seg_10439" s="T405">n-n:case</ta>
            <ta e="T407" id="Seg_10440" s="T406">v-v:mood-v:temp.pn</ta>
            <ta e="T408" id="Seg_10441" s="T407">adv</ta>
            <ta e="T409" id="Seg_10442" s="T408">v-v:(ins)-v:ptcp</ta>
            <ta e="T410" id="Seg_10443" s="T409">n-n:case</ta>
            <ta e="T411" id="Seg_10444" s="T410">v-v:mood-v:pred.pn</ta>
            <ta e="T412" id="Seg_10445" s="T411">dempro</ta>
            <ta e="T413" id="Seg_10446" s="T412">n-n:(poss)-n:case</ta>
            <ta e="T414" id="Seg_10447" s="T413">n-n:case</ta>
            <ta e="T415" id="Seg_10448" s="T414">v-v:tense-v:pred.pn</ta>
            <ta e="T416" id="Seg_10449" s="T415">n-n:case</ta>
            <ta e="T417" id="Seg_10450" s="T416">n-n:(poss)</ta>
            <ta e="T418" id="Seg_10451" s="T417">ptcl</ta>
            <ta e="T419" id="Seg_10452" s="T418">v-v:cvb</ta>
            <ta e="T420" id="Seg_10453" s="T419">dempro</ta>
            <ta e="T421" id="Seg_10454" s="T420">n-n:case</ta>
            <ta e="T422" id="Seg_10455" s="T421">n-n:poss-n:case</ta>
            <ta e="T423" id="Seg_10456" s="T422">v-v:ptcp</ta>
            <ta e="T424" id="Seg_10457" s="T423">n-cardnum:case</ta>
            <ta e="T425" id="Seg_10458" s="T424">dempro</ta>
            <ta e="T426" id="Seg_10459" s="T425">n-n:case</ta>
            <ta e="T427" id="Seg_10460" s="T426">v-v:cvb</ta>
            <ta e="T428" id="Seg_10461" s="T427">v-v:cvb</ta>
            <ta e="T429" id="Seg_10462" s="T428">v-v:(neg)-v:pred.pn</ta>
            <ta e="T430" id="Seg_10463" s="T429">que</ta>
            <ta e="T431" id="Seg_10464" s="T430">ptcl</ta>
            <ta e="T432" id="Seg_10465" s="T431">n-n:case</ta>
            <ta e="T433" id="Seg_10466" s="T432">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T434" id="Seg_10467" s="T433">adv&gt;adv-adv</ta>
            <ta e="T435" id="Seg_10468" s="T434">v-v:(neg)-v:pred.pn</ta>
            <ta e="T436" id="Seg_10469" s="T435">n-n:case</ta>
            <ta e="T437" id="Seg_10470" s="T436">n-n:(poss)</ta>
            <ta e="T438" id="Seg_10471" s="T437">ptcl</ta>
            <ta e="T439" id="Seg_10472" s="T438">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T440" id="Seg_10473" s="T439">dempro</ta>
            <ta e="T441" id="Seg_10474" s="T440">adv</ta>
            <ta e="T442" id="Seg_10475" s="T441">n-n:(num)-n:case</ta>
            <ta e="T443" id="Seg_10476" s="T442">ptcl</ta>
            <ta e="T444" id="Seg_10477" s="T443">n-n:(num)-n:case</ta>
            <ta e="T445" id="Seg_10478" s="T444">ptcl</ta>
            <ta e="T446" id="Seg_10479" s="T445">adv</ta>
            <ta e="T447" id="Seg_10480" s="T446">n-n:case</ta>
            <ta e="T448" id="Seg_10481" s="T447">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T449" id="Seg_10482" s="T448">post</ta>
            <ta e="T451" id="Seg_10483" s="T450">n-n:case</ta>
            <ta e="T452" id="Seg_10484" s="T451">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T453" id="Seg_10485" s="T452">post</ta>
            <ta e="T454" id="Seg_10486" s="T453">adj-adj&gt;adv</ta>
            <ta e="T455" id="Seg_10487" s="T454">n-n:poss-n:case</ta>
            <ta e="T456" id="Seg_10488" s="T455">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T457" id="Seg_10489" s="T456">ptcl</ta>
            <ta e="T458" id="Seg_10490" s="T457">n-n:case</ta>
            <ta e="T459" id="Seg_10491" s="T458">v-v:(ins)-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T460" id="Seg_10492" s="T459">n-n:case</ta>
            <ta e="T461" id="Seg_10493" s="T460">v-v:mood-v:temp.pn</ta>
            <ta e="T462" id="Seg_10494" s="T461">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T463" id="Seg_10495" s="T462">v-v:mood-v:pred.pn</ta>
            <ta e="T464" id="Seg_10496" s="T463">dempro</ta>
            <ta e="T465" id="Seg_10497" s="T464">v-v:cvb</ta>
            <ta e="T466" id="Seg_10498" s="T465">post</ta>
            <ta e="T467" id="Seg_10499" s="T466">dempro</ta>
            <ta e="T468" id="Seg_10500" s="T467">v-v:(ins)-v&gt;v-v:(ins)-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T469" id="Seg_10501" s="T468">post</ta>
            <ta e="T470" id="Seg_10502" s="T469">dempro-pro:case</ta>
            <ta e="T471" id="Seg_10503" s="T470">n-n:case</ta>
            <ta e="T472" id="Seg_10504" s="T471">v-v:(neg)-v:pred.pn</ta>
            <ta e="T473" id="Seg_10505" s="T472">adv&gt;adv-adv</ta>
            <ta e="T474" id="Seg_10506" s="T473">v-v:tense-v:pred.pn</ta>
            <ta e="T475" id="Seg_10507" s="T474">dempro</ta>
            <ta e="T476" id="Seg_10508" s="T475">v-v:mood-v:temp.pn</ta>
            <ta e="T477" id="Seg_10509" s="T476">cardnum-cardnum</ta>
            <ta e="T478" id="Seg_10510" s="T477">n-n&gt;adj</ta>
            <ta e="T479" id="Seg_10511" s="T478">n-n&gt;adj-n:case</ta>
            <ta e="T480" id="Seg_10512" s="T479">v-v:mood-v:pred.pn</ta>
            <ta e="T481" id="Seg_10513" s="T480">dempro</ta>
            <ta e="T482" id="Seg_10514" s="T481">n-n:case</ta>
            <ta e="T483" id="Seg_10515" s="T482">n-n:case</ta>
            <ta e="T484" id="Seg_10516" s="T483">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T485" id="Seg_10517" s="T484">dempro-pro:case</ta>
            <ta e="T486" id="Seg_10518" s="T485">post</ta>
            <ta e="T487" id="Seg_10519" s="T486">dempro-pro:case-pro:case</ta>
            <ta e="T488" id="Seg_10520" s="T487">v-v:cvb</ta>
            <ta e="T489" id="Seg_10521" s="T488">n-n:case</ta>
            <ta e="T490" id="Seg_10522" s="T489">n-n:(poss)</ta>
            <ta e="T491" id="Seg_10523" s="T490">ptcl</ta>
            <ta e="T492" id="Seg_10524" s="T491">v-v:tense-v:pred.pn</ta>
            <ta e="T493" id="Seg_10525" s="T492">dempro-pro:case</ta>
            <ta e="T494" id="Seg_10526" s="T493">adv</ta>
            <ta e="T495" id="Seg_10527" s="T494">n-n&gt;adj</ta>
            <ta e="T496" id="Seg_10528" s="T495">n-n:(num)-n:case</ta>
            <ta e="T497" id="Seg_10529" s="T496">ptcl</ta>
            <ta e="T498" id="Seg_10530" s="T497">n-n&gt;v-v:mood-v:temp.pn</ta>
            <ta e="T499" id="Seg_10531" s="T498">que</ta>
            <ta e="T500" id="Seg_10532" s="T499">ptcl</ta>
            <ta e="T501" id="Seg_10533" s="T500">n-n&gt;adj</ta>
            <ta e="T502" id="Seg_10534" s="T501">n-n:(ins)-n:case</ta>
            <ta e="T503" id="Seg_10535" s="T502">v-v:tense-v:pred.pn</ta>
            <ta e="T504" id="Seg_10536" s="T503">n-n:case</ta>
            <ta e="T505" id="Seg_10537" s="T504">dempro</ta>
            <ta e="T506" id="Seg_10538" s="T505">n-n:case</ta>
            <ta e="T507" id="Seg_10539" s="T506">post</ta>
            <ta e="T508" id="Seg_10540" s="T507">n-n:case</ta>
            <ta e="T509" id="Seg_10541" s="T508">n-n:case</ta>
            <ta e="T510" id="Seg_10542" s="T509">n-n:(poss)-n:case</ta>
            <ta e="T511" id="Seg_10543" s="T510">v-v:mood-v:pred.pn</ta>
            <ta e="T512" id="Seg_10544" s="T511">ptcl</ta>
            <ta e="T513" id="Seg_10545" s="T512">adv</ta>
            <ta e="T514" id="Seg_10546" s="T513">ptcl</ta>
            <ta e="T515" id="Seg_10547" s="T514">v-v:mood-v:pred.pn</ta>
            <ta e="T516" id="Seg_10548" s="T515">n-n:case</ta>
            <ta e="T517" id="Seg_10549" s="T516">ptcl</ta>
            <ta e="T518" id="Seg_10550" s="T517">adj</ta>
            <ta e="T519" id="Seg_10551" s="T518">n-n:case</ta>
            <ta e="T520" id="Seg_10552" s="T519">ptcl</ta>
            <ta e="T521" id="Seg_10553" s="T520">adv</ta>
            <ta e="T522" id="Seg_10554" s="T521">dempro</ta>
            <ta e="T523" id="Seg_10555" s="T522">n-n:(num)-n:case</ta>
            <ta e="T524" id="Seg_10556" s="T523">n</ta>
            <ta e="T525" id="Seg_10557" s="T524">n-n:(poss)-n:case</ta>
            <ta e="T526" id="Seg_10558" s="T525">v-v:mood-v:temp.pn</ta>
            <ta e="T527" id="Seg_10559" s="T526">n-n&gt;adj-n:poss-n:case</ta>
            <ta e="T528" id="Seg_10560" s="T527">n-n:poss-n:case</ta>
            <ta e="T529" id="Seg_10561" s="T528">v-v:cvb</ta>
            <ta e="T530" id="Seg_10562" s="T529">v-v&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T531" id="Seg_10563" s="T530">n-n:poss-n:case</ta>
            <ta e="T532" id="Seg_10564" s="T531">v-v:(neg)-v:pred.pn</ta>
            <ta e="T533" id="Seg_10565" s="T532">n-n:case</ta>
            <ta e="T534" id="Seg_10566" s="T533">n-n:poss-n:case</ta>
            <ta e="T535" id="Seg_10567" s="T534">v-v:mood-v:temp.pn</ta>
            <ta e="T536" id="Seg_10568" s="T535">dempro</ta>
            <ta e="T537" id="Seg_10569" s="T536">n-n:poss-n:case</ta>
            <ta e="T538" id="Seg_10570" s="T537">v-v:mood-v:temp.pn</ta>
            <ta e="T539" id="Seg_10571" s="T538">dempro</ta>
            <ta e="T540" id="Seg_10572" s="T539">n-n:case</ta>
            <ta e="T541" id="Seg_10573" s="T540">v-v:tense-v:pred.pn</ta>
            <ta e="T542" id="Seg_10574" s="T541">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T543" id="Seg_10575" s="T542">n-n:case</ta>
            <ta e="T544" id="Seg_10576" s="T543">adv</ta>
            <ta e="T545" id="Seg_10577" s="T544">v-v:cvb</ta>
            <ta e="T546" id="Seg_10578" s="T545">v-v:mood-v:temp.pn</ta>
            <ta e="T547" id="Seg_10579" s="T546">n-n:poss-n:case</ta>
            <ta e="T548" id="Seg_10580" s="T547">n-n:poss-n:case</ta>
            <ta e="T549" id="Seg_10581" s="T548">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T550" id="Seg_10582" s="T549">v-v:cvb</ta>
            <ta e="T551" id="Seg_10583" s="T550">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T552" id="Seg_10584" s="T551">post</ta>
            <ta e="T553" id="Seg_10585" s="T552">v-v:mood-v:temp.pn</ta>
            <ta e="T554" id="Seg_10586" s="T553">n-n:case</ta>
            <ta e="T555" id="Seg_10587" s="T554">conj</ta>
            <ta e="T556" id="Seg_10588" s="T555">adj-n:case</ta>
            <ta e="T557" id="Seg_10589" s="T556">indfpro</ta>
            <ta e="T558" id="Seg_10590" s="T557">n-n:case</ta>
            <ta e="T559" id="Seg_10591" s="T558">adj-adj&gt;adv</ta>
            <ta e="T560" id="Seg_10592" s="T559">v-v:cvb-v-v:cvb-v:pred.pn</ta>
            <ta e="T561" id="Seg_10593" s="T560">n-n:case</ta>
            <ta e="T562" id="Seg_10594" s="T561">adj-n:(poss)</ta>
            <ta e="T563" id="Seg_10595" s="T562">v-v:ptcp</ta>
            <ta e="T564" id="Seg_10596" s="T563">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T565" id="Seg_10597" s="T564">dempro</ta>
            <ta e="T566" id="Seg_10598" s="T565">indfpro</ta>
            <ta e="T567" id="Seg_10599" s="T566">n-n:case</ta>
            <ta e="T568" id="Seg_10600" s="T567">n-n:case</ta>
            <ta e="T569" id="Seg_10601" s="T568">conj</ta>
            <ta e="T570" id="Seg_10602" s="T569">v-v:cvb</ta>
            <ta e="T571" id="Seg_10603" s="T570">v-v:(neg)-v:pred.pn</ta>
            <ta e="T572" id="Seg_10604" s="T571">n-n:(poss)-n:case</ta>
            <ta e="T573" id="Seg_10605" s="T572">v-v:tense-v:poss.pn</ta>
            <ta e="T574" id="Seg_10606" s="T573">n-n:case</ta>
            <ta e="T575" id="Seg_10607" s="T574">adj-n:(poss)-n:case</ta>
            <ta e="T576" id="Seg_10608" s="T575">n-n:poss-n:case</ta>
            <ta e="T577" id="Seg_10609" s="T576">v-v:tense-v:pred.pn</ta>
            <ta e="T578" id="Seg_10610" s="T577">n-n:poss-n:case</ta>
            <ta e="T579" id="Seg_10611" s="T578">v-v:mood-v:temp.pn</ta>
            <ta e="T580" id="Seg_10612" s="T579">dempro</ta>
            <ta e="T581" id="Seg_10613" s="T580">adv</ta>
            <ta e="T582" id="Seg_10614" s="T581">n-n:case</ta>
            <ta e="T583" id="Seg_10615" s="T582">v-v:mood-v:pred.pn</ta>
            <ta e="T584" id="Seg_10616" s="T583">dempro-pro:case</ta>
            <ta e="T585" id="Seg_10617" s="T584">n-n:case</ta>
            <ta e="T587" id="Seg_10618" s="T586">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T588" id="Seg_10619" s="T587">dempro-pro:case</ta>
            <ta e="T589" id="Seg_10620" s="T588">adv</ta>
            <ta e="T590" id="Seg_10621" s="T589">n-n:poss-n:case</ta>
            <ta e="T591" id="Seg_10622" s="T590">n-n:case</ta>
            <ta e="T592" id="Seg_10623" s="T591">adj-n:poss-n:case</ta>
            <ta e="T593" id="Seg_10624" s="T592">v-v:cvb</ta>
            <ta e="T594" id="Seg_10625" s="T593">v-v:tense-v:pred.pn</ta>
            <ta e="T595" id="Seg_10626" s="T594">dempro</ta>
            <ta e="T596" id="Seg_10627" s="T595">n-n:poss-n:case</ta>
            <ta e="T597" id="Seg_10628" s="T596">v-v:cvb</ta>
            <ta e="T598" id="Seg_10629" s="T597">v-v:mood-v:temp.pn</ta>
            <ta e="T599" id="Seg_10630" s="T598">adv</ta>
            <ta e="T600" id="Seg_10631" s="T599">n-n:case</ta>
            <ta e="T601" id="Seg_10632" s="T600">dempro</ta>
            <ta e="T602" id="Seg_10633" s="T601">n-n:case</ta>
            <ta e="T603" id="Seg_10634" s="T602">adj-n:(poss)-n:case</ta>
            <ta e="T604" id="Seg_10635" s="T603">n-n:(poss)-n:case</ta>
            <ta e="T605" id="Seg_10636" s="T604">que</ta>
            <ta e="T606" id="Seg_10637" s="T605">ptcl</ta>
            <ta e="T607" id="Seg_10638" s="T606">ptcl</ta>
            <ta e="T608" id="Seg_10639" s="T607">v-v:(neg)-v:pred.pn</ta>
            <ta e="T609" id="Seg_10640" s="T608">adv</ta>
            <ta e="T610" id="Seg_10641" s="T609">adv</ta>
            <ta e="T611" id="Seg_10642" s="T610">adj-n:case</ta>
            <ta e="T612" id="Seg_10643" s="T611">post</ta>
            <ta e="T613" id="Seg_10644" s="T612">v-v:tense-v:pred.pn</ta>
            <ta e="T614" id="Seg_10645" s="T613">dempro</ta>
            <ta e="T615" id="Seg_10646" s="T614">v-v:mood-v:temp.pn</ta>
            <ta e="T616" id="Seg_10647" s="T615">n-n:case</ta>
            <ta e="T617" id="Seg_10648" s="T616">v-v:mood-v:temp.pn</ta>
            <ta e="T618" id="Seg_10649" s="T617">v-v:cvb</ta>
            <ta e="T619" id="Seg_10650" s="T618">v-v:cvb-v:pred.pn</ta>
            <ta e="T620" id="Seg_10651" s="T619">n-n&gt;adj</ta>
            <ta e="T621" id="Seg_10652" s="T620">n-n:(num)-n:(ins)-n:case</ta>
            <ta e="T622" id="Seg_10653" s="T621">v-v:tense-v:pred.pn</ta>
            <ta e="T623" id="Seg_10654" s="T622">dempro</ta>
            <ta e="T624" id="Seg_10655" s="T623">n-n&gt;adj</ta>
            <ta e="T625" id="Seg_10656" s="T624">adj</ta>
            <ta e="T626" id="Seg_10657" s="T625">n-n:(num)-n:case</ta>
            <ta e="T627" id="Seg_10658" s="T626">n-n&gt;adj-n:case</ta>
            <ta e="T628" id="Seg_10659" s="T627">v-v:mood-v:pred.pn</ta>
            <ta e="T629" id="Seg_10660" s="T628">que</ta>
            <ta e="T630" id="Seg_10661" s="T629">conj</ta>
            <ta e="T631" id="Seg_10662" s="T630">n-n:case</ta>
            <ta e="T632" id="Seg_10663" s="T631">dempro</ta>
            <ta e="T633" id="Seg_10664" s="T632">n-n:case</ta>
            <ta e="T634" id="Seg_10665" s="T633">v-v:tense-v:poss.pn</ta>
            <ta e="T635" id="Seg_10666" s="T634">conj</ta>
            <ta e="T636" id="Seg_10667" s="T635">n-n:case</ta>
            <ta e="T637" id="Seg_10668" s="T636">dempro</ta>
            <ta e="T638" id="Seg_10669" s="T637">n-n:case</ta>
            <ta e="T639" id="Seg_10670" s="T638">v-v:mood-v:pred.pn</ta>
            <ta e="T640" id="Seg_10671" s="T639">v-v:cvb</ta>
            <ta e="T641" id="Seg_10672" s="T640">v-v:ptcp-v:(case)</ta>
            <ta e="T642" id="Seg_10673" s="T641">conj</ta>
            <ta e="T643" id="Seg_10674" s="T642">v-v:cvb</ta>
            <ta e="T644" id="Seg_10675" s="T643">conj</ta>
            <ta e="T645" id="Seg_10676" s="T644">v-v:mood-v:temp.pn</ta>
            <ta e="T646" id="Seg_10677" s="T645">dempro-pro:case</ta>
            <ta e="T647" id="Seg_10678" s="T646">n-n:(num)-n:case</ta>
            <ta e="T648" id="Seg_10679" s="T647">dempro</ta>
            <ta e="T649" id="Seg_10680" s="T648">n-n:case</ta>
            <ta e="T650" id="Seg_10681" s="T649">v-v:cvb</ta>
            <ta e="T651" id="Seg_10682" s="T650">n-n&gt;v-v:cvb</ta>
            <ta e="T652" id="Seg_10683" s="T651">v-v:tense-v:pred.pn</ta>
            <ta e="T653" id="Seg_10684" s="T652">conj</ta>
            <ta e="T654" id="Seg_10685" s="T653">adv&gt;adv-adv</ta>
            <ta e="T655" id="Seg_10686" s="T654">v-v&gt;v-v:cvb-v-v&gt;v-v:cvb</ta>
            <ta e="T656" id="Seg_10687" s="T655">v-v:cvb</ta>
            <ta e="T657" id="Seg_10688" s="T656">v-v:tense-v:pred.pn</ta>
            <ta e="T658" id="Seg_10689" s="T657">adv</ta>
            <ta e="T659" id="Seg_10690" s="T658">cardnum</ta>
            <ta e="T660" id="Seg_10691" s="T659">n-n:(poss)-n:case</ta>
            <ta e="T661" id="Seg_10692" s="T660">cardnum</ta>
            <ta e="T662" id="Seg_10693" s="T661">ptcl</ta>
            <ta e="T663" id="Seg_10694" s="T662">cardnum</ta>
            <ta e="T664" id="Seg_10695" s="T663">n-n:(poss)-n:case</ta>
            <ta e="T665" id="Seg_10696" s="T664">cardnum</ta>
            <ta e="T666" id="Seg_10697" s="T665">ptcl</ta>
            <ta e="T667" id="Seg_10698" s="T666">n-n:case</ta>
            <ta e="T668" id="Seg_10699" s="T667">v-v:tense-v:pred.pn</ta>
            <ta e="T669" id="Seg_10700" s="T668">dempro</ta>
            <ta e="T670" id="Seg_10701" s="T669">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T671" id="Seg_10702" s="T670">v-v:(neg)-v:pred.pn</ta>
            <ta e="T672" id="Seg_10703" s="T671">n-n:case</ta>
            <ta e="T673" id="Seg_10704" s="T672">v-v:cvb-v:pred.pn</ta>
            <ta e="T674" id="Seg_10705" s="T673">adj</ta>
            <ta e="T675" id="Seg_10706" s="T674">n-n:case</ta>
            <ta e="T676" id="Seg_10707" s="T675">v-v:tense-v:pred.pn</ta>
            <ta e="T677" id="Seg_10708" s="T676">adv</ta>
            <ta e="T678" id="Seg_10709" s="T677">dempro</ta>
            <ta e="T679" id="Seg_10710" s="T678">v-v:tense-v:(poss)-v:(case)</ta>
            <ta e="T680" id="Seg_10711" s="T679">v-v:ptcp</ta>
            <ta e="T681" id="Seg_10712" s="T680">n-n:case</ta>
            <ta e="T682" id="Seg_10713" s="T681">ptcl</ta>
            <ta e="T683" id="Seg_10714" s="T682">n-n:(poss)-n:case</ta>
            <ta e="T684" id="Seg_10715" s="T683">dempro</ta>
            <ta e="T685" id="Seg_10716" s="T684">n-n:(num)-n:case</ta>
            <ta e="T686" id="Seg_10717" s="T685">adv</ta>
            <ta e="T687" id="Seg_10718" s="T686">n-n:case</ta>
            <ta e="T688" id="Seg_10719" s="T687">v-v:tense-v:pred.pn</ta>
            <ta e="T689" id="Seg_10720" s="T688">cardnum</ta>
            <ta e="T690" id="Seg_10721" s="T689">n-n:(poss)-n:case</ta>
            <ta e="T691" id="Seg_10722" s="T690">n-n:poss-n:case</ta>
            <ta e="T692" id="Seg_10723" s="T691">v-v:tense-v:pred.pn</ta>
            <ta e="T693" id="Seg_10724" s="T692">ptcl</ta>
            <ta e="T694" id="Seg_10725" s="T693">v-v:cvb</ta>
            <ta e="T695" id="Seg_10726" s="T694">v-v:cvb</ta>
            <ta e="T696" id="Seg_10727" s="T695">post</ta>
            <ta e="T697" id="Seg_10728" s="T696">adj</ta>
            <ta e="T698" id="Seg_10729" s="T697">n-n:(num)-n:case</ta>
            <ta e="T699" id="Seg_10730" s="T698">v-v:tense-v:pred.pn</ta>
            <ta e="T700" id="Seg_10731" s="T699">dempro</ta>
            <ta e="T701" id="Seg_10732" s="T700">adj</ta>
            <ta e="T702" id="Seg_10733" s="T701">n-n:case</ta>
            <ta e="T703" id="Seg_10734" s="T702">v-v:ptcp</ta>
            <ta e="T704" id="Seg_10735" s="T703">adv</ta>
            <ta e="T705" id="Seg_10736" s="T704">n-n:case</ta>
            <ta e="T706" id="Seg_10737" s="T705">v-v:cvb</ta>
            <ta e="T707" id="Seg_10738" s="T706">v-v:cvb</ta>
            <ta e="T708" id="Seg_10739" s="T707">n-n:poss-n:case</ta>
            <ta e="T709" id="Seg_10740" s="T708">adv</ta>
            <ta e="T710" id="Seg_10741" s="T709">v-v:tense-v:pred.pn</ta>
            <ta e="T711" id="Seg_10742" s="T710">n</ta>
            <ta e="T712" id="Seg_10743" s="T711">v-v:cvb</ta>
            <ta e="T713" id="Seg_10744" s="T712">v-v:tense-v:pred.pn</ta>
            <ta e="T714" id="Seg_10745" s="T713">interj</ta>
            <ta e="T715" id="Seg_10746" s="T714">posspr</ta>
            <ta e="T716" id="Seg_10747" s="T715">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T717" id="Seg_10748" s="T716">ptcl</ta>
            <ta e="T718" id="Seg_10749" s="T717">adv</ta>
            <ta e="T719" id="Seg_10750" s="T718">n-n:case</ta>
            <ta e="T720" id="Seg_10751" s="T719">ptcl</ta>
            <ta e="T721" id="Seg_10752" s="T720">v-v:neg-v:(ins)-v:poss.pn</ta>
            <ta e="T722" id="Seg_10753" s="T721">adj</ta>
            <ta e="T723" id="Seg_10754" s="T722">v-v:ptcp</ta>
            <ta e="T724" id="Seg_10755" s="T723">n-n:poss-n:case</ta>
            <ta e="T725" id="Seg_10756" s="T724">adj</ta>
            <ta e="T726" id="Seg_10757" s="T725">n-n:case</ta>
            <ta e="T727" id="Seg_10758" s="T726">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T728" id="Seg_10759" s="T727">v-v:cvb</ta>
            <ta e="T729" id="Seg_10760" s="T728">v-v:(ins)-v:ptcp</ta>
            <ta e="T730" id="Seg_10761" s="T729">n-n:(poss)-n:case</ta>
            <ta e="T731" id="Seg_10762" s="T730">v-v:tense-v:poss.pn</ta>
            <ta e="T732" id="Seg_10763" s="T731">n-n:case</ta>
            <ta e="T733" id="Seg_10764" s="T732">conj</ta>
            <ta e="T734" id="Seg_10765" s="T733">adv</ta>
            <ta e="T735" id="Seg_10766" s="T734">n-n:case</ta>
            <ta e="T736" id="Seg_10767" s="T735">v-v:tense-v:pred.pn</ta>
            <ta e="T737" id="Seg_10768" s="T736">ptcl</ta>
            <ta e="T738" id="Seg_10769" s="T737">v-v:cvb</ta>
            <ta e="T739" id="Seg_10770" s="T738">v-v:tense-v:pred.pn</ta>
            <ta e="T740" id="Seg_10771" s="T739">dempro</ta>
            <ta e="T741" id="Seg_10772" s="T740">n-n:case</ta>
            <ta e="T742" id="Seg_10773" s="T741">dempro</ta>
            <ta e="T743" id="Seg_10774" s="T742">n-n:poss-n:case</ta>
            <ta e="T744" id="Seg_10775" s="T743">v-v:cvb</ta>
            <ta e="T745" id="Seg_10776" s="T744">v-v:tense-v:pred.pn</ta>
            <ta e="T746" id="Seg_10777" s="T745">adv</ta>
            <ta e="T747" id="Seg_10778" s="T746">v-v:ptcp</ta>
            <ta e="T748" id="Seg_10779" s="T747">n-n:poss-n:case</ta>
            <ta e="T749" id="Seg_10780" s="T748">dempro</ta>
            <ta e="T750" id="Seg_10781" s="T749">n-n:(num)-n:case</ta>
            <ta e="T751" id="Seg_10782" s="T750">adj</ta>
            <ta e="T752" id="Seg_10783" s="T751">n-n:case</ta>
            <ta e="T753" id="Seg_10784" s="T752">v-v:ptcp</ta>
            <ta e="T754" id="Seg_10785" s="T753">n-n:(num)-n:case</ta>
            <ta e="T755" id="Seg_10786" s="T754">n-n:case</ta>
            <ta e="T756" id="Seg_10787" s="T755">n-n:case</ta>
            <ta e="T757" id="Seg_10788" s="T756">v-v:tense-v:pred.pn</ta>
            <ta e="T758" id="Seg_10789" s="T757">dempro</ta>
            <ta e="T759" id="Seg_10790" s="T758">adj</ta>
            <ta e="T760" id="Seg_10791" s="T759">n-n:case</ta>
            <ta e="T761" id="Seg_10792" s="T760">v-v:ptcp</ta>
            <ta e="T762" id="Seg_10793" s="T761">n-n:case</ta>
            <ta e="T763" id="Seg_10794" s="T762">n-n:(poss)-n:case</ta>
            <ta e="T764" id="Seg_10795" s="T763">adv</ta>
            <ta e="T765" id="Seg_10796" s="T764">v-v:tense-v:pred.pn</ta>
            <ta e="T766" id="Seg_10797" s="T765">dempro</ta>
            <ta e="T767" id="Seg_10798" s="T766">n-n:case</ta>
            <ta e="T768" id="Seg_10799" s="T767">dempro</ta>
            <ta e="T769" id="Seg_10800" s="T768">adj</ta>
            <ta e="T770" id="Seg_10801" s="T769">n-n:case</ta>
            <ta e="T771" id="Seg_10802" s="T770">v-v:tense-v:pred.pn</ta>
            <ta e="T772" id="Seg_10803" s="T771">adv</ta>
            <ta e="T773" id="Seg_10804" s="T772">v-v:tense-v:poss.pn</ta>
            <ta e="T774" id="Seg_10805" s="T773">cardnum-cardnum:case</ta>
            <ta e="T775" id="Seg_10806" s="T774">post</ta>
            <ta e="T776" id="Seg_10807" s="T775">n-n:case</ta>
            <ta e="T777" id="Seg_10808" s="T776">dempro-n:case</ta>
            <ta e="T778" id="Seg_10809" s="T777">post</ta>
            <ta e="T781" id="Seg_10810" s="T780">n-n:case</ta>
            <ta e="T782" id="Seg_10811" s="T781">n-n:(poss)</ta>
            <ta e="T783" id="Seg_10812" s="T782">ptcl</ta>
            <ta e="T784" id="Seg_10813" s="T783">v-v:tense-v:(poss)-v:(case)</ta>
            <ta e="T785" id="Seg_10814" s="T784">dempro-pro:case</ta>
            <ta e="T786" id="Seg_10815" s="T785">adv</ta>
            <ta e="T787" id="Seg_10816" s="T786">n-n:case</ta>
            <ta e="T788" id="Seg_10817" s="T787">adj-adj-n:case</ta>
            <ta e="T789" id="Seg_10818" s="T788">v-v:mood-v:pred.pn</ta>
            <ta e="T790" id="Seg_10819" s="T789">adj-adj&gt;v-v:ptcp</ta>
            <ta e="T791" id="Seg_10820" s="T790">v-v:mood-v:pred.pn</ta>
            <ta e="T792" id="Seg_10821" s="T791">dempro</ta>
            <ta e="T793" id="Seg_10822" s="T792">n-n:case</ta>
            <ta e="T794" id="Seg_10823" s="T793">ptcl</ta>
            <ta e="T795" id="Seg_10824" s="T794">adv-adv&gt;adj</ta>
            <ta e="T796" id="Seg_10825" s="T795">n-n:(num)-n:case</ta>
            <ta e="T797" id="Seg_10826" s="T796">n-n:case</ta>
            <ta e="T798" id="Seg_10827" s="T797">v-v:cvb</ta>
            <ta e="T799" id="Seg_10828" s="T798">v-v:(neg)-v:pred.pn</ta>
            <ta e="T800" id="Seg_10829" s="T799">v-v:(neg)-v:poss.pn</ta>
            <ta e="T801" id="Seg_10830" s="T800">que-pro:case-ptcl</ta>
            <ta e="T802" id="Seg_10831" s="T801">n-n:case</ta>
            <ta e="T803" id="Seg_10832" s="T802">n-n&gt;n-n:case</ta>
            <ta e="T804" id="Seg_10833" s="T803">n-n:case</ta>
            <ta e="T805" id="Seg_10834" s="T804">v-v:mood-v:pred.pn</ta>
            <ta e="T806" id="Seg_10835" s="T805">n-n&gt;n-n:case</ta>
            <ta e="T807" id="Seg_10836" s="T806">n-n:case</ta>
            <ta e="T808" id="Seg_10837" s="T807">n-n:(poss)-n:case</ta>
            <ta e="T809" id="Seg_10838" s="T808">n-n:case</ta>
            <ta e="T810" id="Seg_10839" s="T809">v-v:tense-v:pred.pn</ta>
            <ta e="T811" id="Seg_10840" s="T810">dempro</ta>
            <ta e="T812" id="Seg_10841" s="T811">n-n:case</ta>
            <ta e="T813" id="Seg_10842" s="T812">v-v:ptcp</ta>
            <ta e="T814" id="Seg_10843" s="T813">n-n:case</ta>
            <ta e="T815" id="Seg_10844" s="T814">n-n:case</ta>
            <ta e="T816" id="Seg_10845" s="T815">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T817" id="Seg_10846" s="T816">post</ta>
            <ta e="T818" id="Seg_10847" s="T817">n-n:case</ta>
            <ta e="T819" id="Seg_10848" s="T818">n-n:case</ta>
            <ta e="T820" id="Seg_10849" s="T819">v-v:mood-v:pred.pn</ta>
            <ta e="T821" id="Seg_10850" s="T820">n-n:case</ta>
            <ta e="T822" id="Seg_10851" s="T821">v-v:mood-v:temp.pn</ta>
            <ta e="T823" id="Seg_10852" s="T822">n-n:case</ta>
            <ta e="T824" id="Seg_10853" s="T823">n-n:(poss)-n:case</ta>
            <ta e="T825" id="Seg_10854" s="T824">v-v:ptcp-v:(poss)</ta>
            <ta e="T826" id="Seg_10855" s="T825">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T827" id="Seg_10856" s="T826">n-n:case</ta>
            <ta e="T828" id="Seg_10857" s="T827">n-n:case</ta>
            <ta e="T830" id="Seg_10858" s="T929">v-v:(neg)-v:pred.pn</ta>
            <ta e="T831" id="Seg_10859" s="T830">n</ta>
            <ta e="T832" id="Seg_10860" s="T831">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T833" id="Seg_10861" s="T832">adj</ta>
            <ta e="T834" id="Seg_10862" s="T833">n-n:case</ta>
            <ta e="T835" id="Seg_10863" s="T834">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T836" id="Seg_10864" s="T835">pers-pro:case</ta>
            <ta e="T837" id="Seg_10865" s="T836">v-v:tense-v:poss.pn</ta>
            <ta e="T838" id="Seg_10866" s="T837">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T839" id="Seg_10867" s="T838">ptcl</ta>
            <ta e="T840" id="Seg_10868" s="T839">v-v:cvb</ta>
            <ta e="T841" id="Seg_10869" s="T840">que</ta>
            <ta e="T842" id="Seg_10870" s="T841">ptcl</ta>
            <ta e="T843" id="Seg_10871" s="T842">n-n:case</ta>
            <ta e="T844" id="Seg_10872" s="T843">n-n:case</ta>
            <ta e="T845" id="Seg_10873" s="T844">post</ta>
            <ta e="T846" id="Seg_10874" s="T845">v-v&gt;n-n:case</ta>
            <ta e="T847" id="Seg_10875" s="T846">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T848" id="Seg_10876" s="T847">n-n:(poss)-n:case</ta>
            <ta e="T849" id="Seg_10877" s="T848">n-n:case</ta>
            <ta e="T850" id="Seg_10878" s="T849">n-n:case</ta>
            <ta e="T851" id="Seg_10879" s="T850">dempro</ta>
            <ta e="T852" id="Seg_10880" s="T851">v-v:tense-v:pred.pn</ta>
            <ta e="T853" id="Seg_10881" s="T852">post</ta>
            <ta e="T854" id="Seg_10882" s="T853">n-n:case</ta>
            <ta e="T855" id="Seg_10883" s="T854">adv</ta>
            <ta e="T856" id="Seg_10884" s="T855">v-v:cvb</ta>
            <ta e="T857" id="Seg_10885" s="T856">n-n:case</ta>
            <ta e="T858" id="Seg_10886" s="T857">post</ta>
            <ta e="T859" id="Seg_10887" s="T858">v-v:mood-v:temp.pn</ta>
            <ta e="T860" id="Seg_10888" s="T859">n-n:poss-n:case</ta>
            <ta e="T861" id="Seg_10889" s="T860">v-v&gt;v-v:mood-v:temp.pn</ta>
            <ta e="T862" id="Seg_10890" s="T861">n-n&gt;adj-n:(poss)-n:case</ta>
            <ta e="T863" id="Seg_10891" s="T862">v-v:cvb-v:pred.pn</ta>
            <ta e="T864" id="Seg_10892" s="T863">adv</ta>
            <ta e="T865" id="Seg_10893" s="T864">v-v:cvb-v:pred.pn</ta>
            <ta e="T866" id="Seg_10894" s="T865">adv</ta>
            <ta e="T867" id="Seg_10895" s="T866">v-v:tense-v:pred.pn</ta>
            <ta e="T868" id="Seg_10896" s="T867">n-n:case</ta>
            <ta e="T869" id="Seg_10897" s="T868">dempro-pro:case</ta>
            <ta e="T870" id="Seg_10898" s="T869">post</ta>
            <ta e="T871" id="Seg_10899" s="T870">pers-pro:case</ta>
            <ta e="T872" id="Seg_10900" s="T871">n-n&gt;adj-n:case</ta>
            <ta e="T873" id="Seg_10901" s="T872">n-n:case</ta>
            <ta e="T874" id="Seg_10902" s="T873">v-v:cvb</ta>
            <ta e="T875" id="Seg_10903" s="T874">ptcl-ptcl:(temp.pn)</ta>
            <ta e="T876" id="Seg_10904" s="T875">adv</ta>
            <ta e="T877" id="Seg_10905" s="T876">n-n:case</ta>
            <ta e="T878" id="Seg_10906" s="T877">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T879" id="Seg_10907" s="T878">n-n:case</ta>
            <ta e="T880" id="Seg_10908" s="T879">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T881" id="Seg_10909" s="T880">n-n:case</ta>
            <ta e="T882" id="Seg_10910" s="T881">n-n:case</ta>
            <ta e="T883" id="Seg_10911" s="T882">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T884" id="Seg_10912" s="T883">ptcl</ta>
            <ta e="T885" id="Seg_10913" s="T884">adj</ta>
            <ta e="T886" id="Seg_10914" s="T885">n-n:case</ta>
            <ta e="T887" id="Seg_10915" s="T886">v-v:cvb</ta>
            <ta e="T888" id="Seg_10916" s="T887">ptcl-ptcl:(temp.pn)</ta>
            <ta e="T889" id="Seg_10917" s="T888">n-n:case</ta>
            <ta e="T890" id="Seg_10918" s="T889">v-v:tense-v:poss.pn</ta>
            <ta e="T891" id="Seg_10919" s="T890">conj</ta>
            <ta e="T892" id="Seg_10920" s="T891">que</ta>
            <ta e="T893" id="Seg_10921" s="T892">conj</ta>
            <ta e="T894" id="Seg_10922" s="T893">n-n:case</ta>
            <ta e="T896" id="Seg_10923" s="T894">n-n:case</ta>
            <ta e="T897" id="Seg_10924" s="T896">n-n&gt;adj-n:(poss)-n:case</ta>
            <ta e="T898" id="Seg_10925" s="T897">n-n:case</ta>
            <ta e="T899" id="Seg_10926" s="T898">n-n:(num)-n:case</ta>
            <ta e="T900" id="Seg_10927" s="T899">v-v:cvb</ta>
            <ta e="T901" id="Seg_10928" s="T900">v-v:cvb-v:pred.pn</ta>
            <ta e="T902" id="Seg_10929" s="T901">v-v:ptcp</ta>
            <ta e="T903" id="Seg_10930" s="T902">v-v:mood-v:pred.pn</ta>
            <ta e="T904" id="Seg_10931" s="T903">interj</ta>
            <ta e="T905" id="Seg_10932" s="T904">n-n:case</ta>
            <ta e="T906" id="Seg_10933" s="T905">v-v:mood-v:temp.pn</ta>
            <ta e="T907" id="Seg_10934" s="T906">ptcl</ta>
            <ta e="T908" id="Seg_10935" s="T907">v-v:cvb</ta>
            <ta e="T909" id="Seg_10936" s="T908">v-v:(ins)-v:mood.pn</ta>
            <ta e="T910" id="Seg_10937" s="T909">n-n&gt;v-v:(ins)-v&gt;v-v:(tense)-v:(ins)-v:mood.pn</ta>
            <ta e="T911" id="Seg_10938" s="T910">n-n:case</ta>
            <ta e="T912" id="Seg_10939" s="T911">v-v:(tense)-v:(ins)-v:mood.pn</ta>
            <ta e="T913" id="Seg_10940" s="T912">adj</ta>
            <ta e="T914" id="Seg_10941" s="T913">v-v:mood-v:temp.pn</ta>
            <ta e="T915" id="Seg_10942" s="T914">ptcl</ta>
            <ta e="T916" id="Seg_10943" s="T915">v-v:tense-v:poss.pn</ta>
            <ta e="T917" id="Seg_10944" s="T916">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T918" id="Seg_10945" s="T917">que</ta>
            <ta e="T919" id="Seg_10946" s="T918">ptcl</ta>
            <ta e="T920" id="Seg_10947" s="T919">adj-n:case</ta>
            <ta e="T921" id="Seg_10948" s="T920">v-v:cvb</ta>
            <ta e="T922" id="Seg_10949" s="T921">v-v:tense-v:pred.pn</ta>
            <ta e="T923" id="Seg_10950" s="T922">dempro-pro:case</ta>
            <ta e="T924" id="Seg_10951" s="T923">post</ta>
            <ta e="T925" id="Seg_10952" s="T924">v-v:ptcp</ta>
            <ta e="T926" id="Seg_10953" s="T925">v-v:tense-v:poss.pn</ta>
            <ta e="T927" id="Seg_10954" s="T926">pers-pro:case</ta>
            <ta e="T928" id="Seg_10955" s="T927">n-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_10956" s="T0">ptcl</ta>
            <ta e="T2" id="Seg_10957" s="T1">pers</ta>
            <ta e="T3" id="Seg_10958" s="T2">n</ta>
            <ta e="T4" id="Seg_10959" s="T3">v</ta>
            <ta e="T5" id="Seg_10960" s="T4">v</ta>
            <ta e="T6" id="Seg_10961" s="T5">pers</ta>
            <ta e="T7" id="Seg_10962" s="T6">adv</ta>
            <ta e="T8" id="Seg_10963" s="T7">v</ta>
            <ta e="T9" id="Seg_10964" s="T8">n</ta>
            <ta e="T10" id="Seg_10965" s="T9">n</ta>
            <ta e="T11" id="Seg_10966" s="T10">adv</ta>
            <ta e="T12" id="Seg_10967" s="T11">dempro</ta>
            <ta e="T13" id="Seg_10968" s="T12">n</ta>
            <ta e="T14" id="Seg_10969" s="T13">v</ta>
            <ta e="T15" id="Seg_10970" s="T14">n</ta>
            <ta e="T16" id="Seg_10971" s="T15">v</ta>
            <ta e="T17" id="Seg_10972" s="T16">adv</ta>
            <ta e="T18" id="Seg_10973" s="T17">que</ta>
            <ta e="T19" id="Seg_10974" s="T18">ptcl</ta>
            <ta e="T20" id="Seg_10975" s="T19">ptcl</ta>
            <ta e="T21" id="Seg_10976" s="T20">v</ta>
            <ta e="T22" id="Seg_10977" s="T21">cop</ta>
            <ta e="T23" id="Seg_10978" s="T22">ptcl</ta>
            <ta e="T24" id="Seg_10979" s="T23">n</ta>
            <ta e="T25" id="Seg_10980" s="T24">conj</ta>
            <ta e="T26" id="Seg_10981" s="T25">v</ta>
            <ta e="T27" id="Seg_10982" s="T26">n</ta>
            <ta e="T28" id="Seg_10983" s="T27">ptcl</ta>
            <ta e="T29" id="Seg_10984" s="T28">adv</ta>
            <ta e="T30" id="Seg_10985" s="T29">pers</ta>
            <ta e="T31" id="Seg_10986" s="T30">v</ta>
            <ta e="T32" id="Seg_10987" s="T31">ptcl</ta>
            <ta e="T33" id="Seg_10988" s="T32">adv</ta>
            <ta e="T34" id="Seg_10989" s="T33">n</ta>
            <ta e="T35" id="Seg_10990" s="T34">v</ta>
            <ta e="T36" id="Seg_10991" s="T35">n</ta>
            <ta e="T37" id="Seg_10992" s="T36">v</ta>
            <ta e="T38" id="Seg_10993" s="T37">pers</ta>
            <ta e="T39" id="Seg_10994" s="T38">v</ta>
            <ta e="T40" id="Seg_10995" s="T39">ptcl</ta>
            <ta e="T41" id="Seg_10996" s="T40">adv</ta>
            <ta e="T42" id="Seg_10997" s="T41">adv</ta>
            <ta e="T43" id="Seg_10998" s="T42">dempro</ta>
            <ta e="T44" id="Seg_10999" s="T43">n</ta>
            <ta e="T45" id="Seg_11000" s="T44">v</ta>
            <ta e="T46" id="Seg_11001" s="T45">pers</ta>
            <ta e="T47" id="Seg_11002" s="T46">adv</ta>
            <ta e="T48" id="Seg_11003" s="T47">v</ta>
            <ta e="T49" id="Seg_11004" s="T48">aux</ta>
            <ta e="T50" id="Seg_11005" s="T49">aux</ta>
            <ta e="T51" id="Seg_11006" s="T50">n</ta>
            <ta e="T52" id="Seg_11007" s="T51">v</ta>
            <ta e="T53" id="Seg_11008" s="T52">quant</ta>
            <ta e="T54" id="Seg_11009" s="T53">adj</ta>
            <ta e="T55" id="Seg_11010" s="T54">cop</ta>
            <ta e="T56" id="Seg_11011" s="T55">n</ta>
            <ta e="T57" id="Seg_11012" s="T56">v</ta>
            <ta e="T58" id="Seg_11013" s="T57">n</ta>
            <ta e="T59" id="Seg_11014" s="T58">cop</ta>
            <ta e="T60" id="Seg_11015" s="T59">v</ta>
            <ta e="T61" id="Seg_11016" s="T60">aux</ta>
            <ta e="T62" id="Seg_11017" s="T61">v</ta>
            <ta e="T63" id="Seg_11018" s="T62">aux</ta>
            <ta e="T64" id="Seg_11019" s="T63">n</ta>
            <ta e="T65" id="Seg_11020" s="T64">v</ta>
            <ta e="T66" id="Seg_11021" s="T65">n</ta>
            <ta e="T67" id="Seg_11022" s="T66">v</ta>
            <ta e="T68" id="Seg_11023" s="T67">adv</ta>
            <ta e="T69" id="Seg_11024" s="T68">v</ta>
            <ta e="T70" id="Seg_11025" s="T69">n</ta>
            <ta e="T71" id="Seg_11026" s="T70">indfpro</ta>
            <ta e="T72" id="Seg_11027" s="T71">n</ta>
            <ta e="T73" id="Seg_11028" s="T72">n</ta>
            <ta e="T74" id="Seg_11029" s="T73">v</ta>
            <ta e="T75" id="Seg_11030" s="T74">post</ta>
            <ta e="T76" id="Seg_11031" s="T75">v</ta>
            <ta e="T77" id="Seg_11032" s="T76">v</ta>
            <ta e="T78" id="Seg_11033" s="T77">v</ta>
            <ta e="T79" id="Seg_11034" s="T78">v</ta>
            <ta e="T80" id="Seg_11035" s="T79">n</ta>
            <ta e="T81" id="Seg_11036" s="T80">v</ta>
            <ta e="T82" id="Seg_11037" s="T81">adj</ta>
            <ta e="T83" id="Seg_11038" s="T82">ptcl</ta>
            <ta e="T84" id="Seg_11039" s="T83">n</ta>
            <ta e="T85" id="Seg_11040" s="T84">v</ta>
            <ta e="T86" id="Seg_11041" s="T85">adj</ta>
            <ta e="T87" id="Seg_11042" s="T86">ptcl</ta>
            <ta e="T88" id="Seg_11043" s="T87">cop</ta>
            <ta e="T89" id="Seg_11044" s="T88">dempro</ta>
            <ta e="T90" id="Seg_11045" s="T89">v</ta>
            <ta e="T91" id="Seg_11046" s="T90">n</ta>
            <ta e="T92" id="Seg_11047" s="T91">v</ta>
            <ta e="T93" id="Seg_11048" s="T92">ptcl</ta>
            <ta e="T94" id="Seg_11049" s="T93">v</ta>
            <ta e="T95" id="Seg_11050" s="T94">v</ta>
            <ta e="T96" id="Seg_11051" s="T95">que</ta>
            <ta e="T97" id="Seg_11052" s="T96">v</ta>
            <ta e="T98" id="Seg_11053" s="T97">que</ta>
            <ta e="T99" id="Seg_11054" s="T98">v</ta>
            <ta e="T100" id="Seg_11055" s="T99">v</ta>
            <ta e="T101" id="Seg_11056" s="T100">dempro</ta>
            <ta e="T102" id="Seg_11057" s="T101">v</ta>
            <ta e="T103" id="Seg_11058" s="T102">n</ta>
            <ta e="T104" id="Seg_11059" s="T103">v</ta>
            <ta e="T105" id="Seg_11060" s="T104">n</ta>
            <ta e="T106" id="Seg_11061" s="T105">v</ta>
            <ta e="T107" id="Seg_11062" s="T106">n</ta>
            <ta e="T108" id="Seg_11063" s="T107">adj</ta>
            <ta e="T109" id="Seg_11064" s="T108">adj</ta>
            <ta e="T110" id="Seg_11065" s="T109">cop</ta>
            <ta e="T111" id="Seg_11066" s="T110">dempro</ta>
            <ta e="T112" id="Seg_11067" s="T111">n</ta>
            <ta e="T113" id="Seg_11068" s="T112">indfpro</ta>
            <ta e="T114" id="Seg_11069" s="T113">n</ta>
            <ta e="T115" id="Seg_11070" s="T114">v</ta>
            <ta e="T116" id="Seg_11071" s="T115">dempro</ta>
            <ta e="T117" id="Seg_11072" s="T116">n</ta>
            <ta e="T118" id="Seg_11073" s="T117">v</ta>
            <ta e="T119" id="Seg_11074" s="T118">dempro</ta>
            <ta e="T120" id="Seg_11075" s="T119">n</ta>
            <ta e="T121" id="Seg_11076" s="T120">adv</ta>
            <ta e="T122" id="Seg_11077" s="T121">n</ta>
            <ta e="T123" id="Seg_11078" s="T122">cop</ta>
            <ta e="T124" id="Seg_11079" s="T123">dempro</ta>
            <ta e="T125" id="Seg_11080" s="T124">n</ta>
            <ta e="T126" id="Seg_11081" s="T125">v</ta>
            <ta e="T127" id="Seg_11082" s="T126">n</ta>
            <ta e="T128" id="Seg_11083" s="T127">adv</ta>
            <ta e="T129" id="Seg_11084" s="T128">v</ta>
            <ta e="T130" id="Seg_11085" s="T129">dempro</ta>
            <ta e="T131" id="Seg_11086" s="T130">n</ta>
            <ta e="T132" id="Seg_11087" s="T131">dempro</ta>
            <ta e="T133" id="Seg_11088" s="T132">n</ta>
            <ta e="T134" id="Seg_11089" s="T133">v</ta>
            <ta e="T135" id="Seg_11090" s="T134">aux</ta>
            <ta e="T136" id="Seg_11091" s="T135">dempro</ta>
            <ta e="T137" id="Seg_11092" s="T136">n</ta>
            <ta e="T138" id="Seg_11093" s="T137">n</ta>
            <ta e="T139" id="Seg_11094" s="T138">v</ta>
            <ta e="T140" id="Seg_11095" s="T139">dempro</ta>
            <ta e="T141" id="Seg_11096" s="T140">cop</ta>
            <ta e="T142" id="Seg_11097" s="T141">n</ta>
            <ta e="T143" id="Seg_11098" s="T142">adj</ta>
            <ta e="T144" id="Seg_11099" s="T143">n</ta>
            <ta e="T145" id="Seg_11100" s="T144">adj</ta>
            <ta e="T146" id="Seg_11101" s="T145">n</ta>
            <ta e="T147" id="Seg_11102" s="T146">v</ta>
            <ta e="T148" id="Seg_11103" s="T147">adv</ta>
            <ta e="T149" id="Seg_11104" s="T148">cardnum</ta>
            <ta e="T150" id="Seg_11105" s="T149">adj</ta>
            <ta e="T151" id="Seg_11106" s="T150">adj</ta>
            <ta e="T152" id="Seg_11107" s="T151">n</ta>
            <ta e="T153" id="Seg_11108" s="T152">v</ta>
            <ta e="T154" id="Seg_11109" s="T153">adj</ta>
            <ta e="T155" id="Seg_11110" s="T154">n</ta>
            <ta e="T156" id="Seg_11111" s="T155">dempro</ta>
            <ta e="T157" id="Seg_11112" s="T156">n</ta>
            <ta e="T158" id="Seg_11113" s="T157">v</ta>
            <ta e="T159" id="Seg_11114" s="T158">n</ta>
            <ta e="T160" id="Seg_11115" s="T159">v</ta>
            <ta e="T161" id="Seg_11116" s="T160">v</ta>
            <ta e="T162" id="Seg_11117" s="T161">ptcl</ta>
            <ta e="T163" id="Seg_11118" s="T162">n</ta>
            <ta e="T164" id="Seg_11119" s="T163">v</ta>
            <ta e="T165" id="Seg_11120" s="T164">ptcl</ta>
            <ta e="T166" id="Seg_11121" s="T165">dempro</ta>
            <ta e="T167" id="Seg_11122" s="T166">n</ta>
            <ta e="T168" id="Seg_11123" s="T167">v</ta>
            <ta e="T169" id="Seg_11124" s="T168">v</ta>
            <ta e="T170" id="Seg_11125" s="T169">adv</ta>
            <ta e="T171" id="Seg_11126" s="T170">adj</ta>
            <ta e="T172" id="Seg_11127" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_11128" s="T172">cop</ta>
            <ta e="T174" id="Seg_11129" s="T173">dempro</ta>
            <ta e="T175" id="Seg_11130" s="T174">n</ta>
            <ta e="T176" id="Seg_11131" s="T175">v</ta>
            <ta e="T177" id="Seg_11132" s="T176">ptcl</ta>
            <ta e="T178" id="Seg_11133" s="T177">v</ta>
            <ta e="T179" id="Seg_11134" s="T178">dempro</ta>
            <ta e="T180" id="Seg_11135" s="T179">n</ta>
            <ta e="T181" id="Seg_11136" s="T180">adv</ta>
            <ta e="T182" id="Seg_11137" s="T181">v</ta>
            <ta e="T183" id="Seg_11138" s="T182">post</ta>
            <ta e="T184" id="Seg_11139" s="T183">n</ta>
            <ta e="T185" id="Seg_11140" s="T184">v</ta>
            <ta e="T186" id="Seg_11141" s="T185">adv</ta>
            <ta e="T187" id="Seg_11142" s="T186">dempro</ta>
            <ta e="T188" id="Seg_11143" s="T187">n</ta>
            <ta e="T189" id="Seg_11144" s="T188">n</ta>
            <ta e="T190" id="Seg_11145" s="T189">adj</ta>
            <ta e="T191" id="Seg_11146" s="T190">que</ta>
            <ta e="T192" id="Seg_11147" s="T191">ptcl</ta>
            <ta e="T193" id="Seg_11148" s="T192">n</ta>
            <ta e="T194" id="Seg_11149" s="T193">v</ta>
            <ta e="T195" id="Seg_11150" s="T194">n</ta>
            <ta e="T196" id="Seg_11151" s="T195">adj</ta>
            <ta e="T197" id="Seg_11152" s="T196">cop</ta>
            <ta e="T198" id="Seg_11153" s="T197">dempro</ta>
            <ta e="T199" id="Seg_11154" s="T198">n</ta>
            <ta e="T200" id="Seg_11155" s="T199">v</ta>
            <ta e="T201" id="Seg_11156" s="T200">v</ta>
            <ta e="T202" id="Seg_11157" s="T201">v</ta>
            <ta e="T203" id="Seg_11158" s="T202">aux</ta>
            <ta e="T204" id="Seg_11159" s="T203">v</ta>
            <ta e="T205" id="Seg_11160" s="T204">dempro</ta>
            <ta e="T206" id="Seg_11161" s="T205">n</ta>
            <ta e="T207" id="Seg_11162" s="T206">v</ta>
            <ta e="T208" id="Seg_11163" s="T207">post</ta>
            <ta e="T209" id="Seg_11164" s="T208">n</ta>
            <ta e="T210" id="Seg_11165" s="T209">v</ta>
            <ta e="T211" id="Seg_11166" s="T210">ptcl</ta>
            <ta e="T212" id="Seg_11167" s="T211">dempro</ta>
            <ta e="T213" id="Seg_11168" s="T212">n</ta>
            <ta e="T214" id="Seg_11169" s="T213">n</ta>
            <ta e="T215" id="Seg_11170" s="T214">que</ta>
            <ta e="T216" id="Seg_11171" s="T215">v</ta>
            <ta e="T217" id="Seg_11172" s="T216">post</ta>
            <ta e="T218" id="Seg_11173" s="T217">v</ta>
            <ta e="T219" id="Seg_11174" s="T218">v</ta>
            <ta e="T220" id="Seg_11175" s="T219">ptcl</ta>
            <ta e="T221" id="Seg_11176" s="T220">dempro</ta>
            <ta e="T222" id="Seg_11177" s="T221">n</ta>
            <ta e="T223" id="Seg_11178" s="T222">n</ta>
            <ta e="T224" id="Seg_11179" s="T223">adj</ta>
            <ta e="T225" id="Seg_11180" s="T224">cop</ta>
            <ta e="T226" id="Seg_11181" s="T225">adj</ta>
            <ta e="T227" id="Seg_11182" s="T226">dempro</ta>
            <ta e="T228" id="Seg_11183" s="T227">n</ta>
            <ta e="T229" id="Seg_11184" s="T228">v</ta>
            <ta e="T230" id="Seg_11185" s="T229">n</ta>
            <ta e="T231" id="Seg_11186" s="T230">v</ta>
            <ta e="T232" id="Seg_11187" s="T231">post</ta>
            <ta e="T233" id="Seg_11188" s="T232">adj</ta>
            <ta e="T234" id="Seg_11189" s="T233">n</ta>
            <ta e="T235" id="Seg_11190" s="T234">v</ta>
            <ta e="T236" id="Seg_11191" s="T235">adv</ta>
            <ta e="T237" id="Seg_11192" s="T236">v</ta>
            <ta e="T238" id="Seg_11193" s="T237">adv</ta>
            <ta e="T239" id="Seg_11194" s="T238">v</ta>
            <ta e="T240" id="Seg_11195" s="T239">dempro</ta>
            <ta e="T241" id="Seg_11196" s="T240">n</ta>
            <ta e="T242" id="Seg_11197" s="T241">n</ta>
            <ta e="T243" id="Seg_11198" s="T242">cop</ta>
            <ta e="T244" id="Seg_11199" s="T243">n</ta>
            <ta e="T245" id="Seg_11200" s="T244">v</ta>
            <ta e="T246" id="Seg_11201" s="T245">quant</ta>
            <ta e="T247" id="Seg_11202" s="T246">n</ta>
            <ta e="T248" id="Seg_11203" s="T247">v</ta>
            <ta e="T249" id="Seg_11204" s="T248">que</ta>
            <ta e="T250" id="Seg_11205" s="T249">ptcl</ta>
            <ta e="T251" id="Seg_11206" s="T250">v</ta>
            <ta e="T252" id="Seg_11207" s="T251">n</ta>
            <ta e="T253" id="Seg_11208" s="T252">n</ta>
            <ta e="T254" id="Seg_11209" s="T253">post</ta>
            <ta e="T255" id="Seg_11210" s="T254">v</ta>
            <ta e="T256" id="Seg_11211" s="T255">v</ta>
            <ta e="T257" id="Seg_11212" s="T256">n</ta>
            <ta e="T258" id="Seg_11213" s="T257">n</ta>
            <ta e="T259" id="Seg_11214" s="T258">v</ta>
            <ta e="T260" id="Seg_11215" s="T259">aux</ta>
            <ta e="T261" id="Seg_11216" s="T260">post</ta>
            <ta e="T262" id="Seg_11217" s="T261">v</ta>
            <ta e="T263" id="Seg_11218" s="T262">post</ta>
            <ta e="T264" id="Seg_11219" s="T263">n</ta>
            <ta e="T265" id="Seg_11220" s="T264">post</ta>
            <ta e="T266" id="Seg_11221" s="T265">v</ta>
            <ta e="T267" id="Seg_11222" s="T266">dempro</ta>
            <ta e="T268" id="Seg_11223" s="T267">n</ta>
            <ta e="T269" id="Seg_11224" s="T268">dempro</ta>
            <ta e="T270" id="Seg_11225" s="T269">ptcl</ta>
            <ta e="T271" id="Seg_11226" s="T270">cardnum</ta>
            <ta e="T272" id="Seg_11227" s="T271">post</ta>
            <ta e="T273" id="Seg_11228" s="T272">cardnum</ta>
            <ta e="T274" id="Seg_11229" s="T273">n</ta>
            <ta e="T275" id="Seg_11230" s="T274">v</ta>
            <ta e="T276" id="Seg_11231" s="T275">post</ta>
            <ta e="T277" id="Seg_11232" s="T276">adv</ta>
            <ta e="T278" id="Seg_11233" s="T277">dempro</ta>
            <ta e="T279" id="Seg_11234" s="T278">v</ta>
            <ta e="T280" id="Seg_11235" s="T279">n</ta>
            <ta e="T281" id="Seg_11236" s="T280">n</ta>
            <ta e="T282" id="Seg_11237" s="T281">post</ta>
            <ta e="T283" id="Seg_11238" s="T282">v</ta>
            <ta e="T284" id="Seg_11239" s="T283">ptcl</ta>
            <ta e="T285" id="Seg_11240" s="T284">dempro</ta>
            <ta e="T286" id="Seg_11241" s="T285">n</ta>
            <ta e="T287" id="Seg_11242" s="T286">v</ta>
            <ta e="T288" id="Seg_11243" s="T287">indfpro</ta>
            <ta e="T289" id="Seg_11244" s="T288">n</ta>
            <ta e="T290" id="Seg_11245" s="T289">dempro</ta>
            <ta e="T291" id="Seg_11246" s="T290">v</ta>
            <ta e="T292" id="Seg_11247" s="T291">n</ta>
            <ta e="T293" id="Seg_11248" s="T292">v</ta>
            <ta e="T294" id="Seg_11249" s="T293">indfpro</ta>
            <ta e="T295" id="Seg_11250" s="T294">n</ta>
            <ta e="T296" id="Seg_11251" s="T295">dempro</ta>
            <ta e="T297" id="Seg_11252" s="T296">v</ta>
            <ta e="T298" id="Seg_11253" s="T297">v</ta>
            <ta e="T299" id="Seg_11254" s="T298">n</ta>
            <ta e="T300" id="Seg_11255" s="T299">v</ta>
            <ta e="T301" id="Seg_11256" s="T300">dempro</ta>
            <ta e="T302" id="Seg_11257" s="T301">v</ta>
            <ta e="T303" id="Seg_11258" s="T302">v</ta>
            <ta e="T304" id="Seg_11259" s="T303">v</ta>
            <ta e="T305" id="Seg_11260" s="T304">adj</ta>
            <ta e="T306" id="Seg_11261" s="T305">n</ta>
            <ta e="T307" id="Seg_11262" s="T306">v</ta>
            <ta e="T308" id="Seg_11263" s="T307">n</ta>
            <ta e="T309" id="Seg_11264" s="T308">v</ta>
            <ta e="T310" id="Seg_11265" s="T309">v</ta>
            <ta e="T311" id="Seg_11266" s="T310">dempro</ta>
            <ta e="T312" id="Seg_11267" s="T311">adj</ta>
            <ta e="T314" id="Seg_11268" s="T312">n</ta>
            <ta e="T315" id="Seg_11269" s="T314">que</ta>
            <ta e="T316" id="Seg_11270" s="T315">ptcl</ta>
            <ta e="T317" id="Seg_11271" s="T316">n</ta>
            <ta e="T318" id="Seg_11272" s="T317">v</ta>
            <ta e="T319" id="Seg_11273" s="T318">n</ta>
            <ta e="T320" id="Seg_11274" s="T319">v</ta>
            <ta e="T321" id="Seg_11275" s="T320">v</ta>
            <ta e="T322" id="Seg_11276" s="T321">n</ta>
            <ta e="T323" id="Seg_11277" s="T322">v</ta>
            <ta e="T324" id="Seg_11278" s="T323">dempro</ta>
            <ta e="T325" id="Seg_11279" s="T324">v</ta>
            <ta e="T326" id="Seg_11280" s="T325">aux</ta>
            <ta e="T327" id="Seg_11281" s="T326">v</ta>
            <ta e="T328" id="Seg_11282" s="T327">v</ta>
            <ta e="T329" id="Seg_11283" s="T328">aux</ta>
            <ta e="T330" id="Seg_11284" s="T329">v</ta>
            <ta e="T331" id="Seg_11285" s="T330">n</ta>
            <ta e="T332" id="Seg_11286" s="T331">n</ta>
            <ta e="T333" id="Seg_11287" s="T332">post</ta>
            <ta e="T334" id="Seg_11288" s="T333">v</ta>
            <ta e="T335" id="Seg_11289" s="T334">v</ta>
            <ta e="T336" id="Seg_11290" s="T335">dempro</ta>
            <ta e="T337" id="Seg_11291" s="T336">v</ta>
            <ta e="T338" id="Seg_11292" s="T337">indfpro</ta>
            <ta e="T339" id="Seg_11293" s="T338">n</ta>
            <ta e="T340" id="Seg_11294" s="T339">adv</ta>
            <ta e="T341" id="Seg_11295" s="T340">adj</ta>
            <ta e="T342" id="Seg_11296" s="T341">v</ta>
            <ta e="T343" id="Seg_11297" s="T342">dempro</ta>
            <ta e="T344" id="Seg_11298" s="T343">adj</ta>
            <ta e="T345" id="Seg_11299" s="T344">v</ta>
            <ta e="T346" id="Seg_11300" s="T345">dempro</ta>
            <ta e="T347" id="Seg_11301" s="T346">n</ta>
            <ta e="T348" id="Seg_11302" s="T347">n</ta>
            <ta e="T349" id="Seg_11303" s="T348">dempro</ta>
            <ta e="T350" id="Seg_11304" s="T349">v</ta>
            <ta e="T351" id="Seg_11305" s="T350">n</ta>
            <ta e="T352" id="Seg_11306" s="T351">adj</ta>
            <ta e="T353" id="Seg_11307" s="T352">n</ta>
            <ta e="T354" id="Seg_11308" s="T353">dempro</ta>
            <ta e="T355" id="Seg_11309" s="T354">dempro</ta>
            <ta e="T356" id="Seg_11310" s="T355">v</ta>
            <ta e="T357" id="Seg_11311" s="T356">n</ta>
            <ta e="T358" id="Seg_11312" s="T357">n</ta>
            <ta e="T359" id="Seg_11313" s="T358">n</ta>
            <ta e="T360" id="Seg_11314" s="T359">post</ta>
            <ta e="T361" id="Seg_11315" s="T360">n</ta>
            <ta e="T362" id="Seg_11316" s="T361">v</ta>
            <ta e="T363" id="Seg_11317" s="T362">v</ta>
            <ta e="T364" id="Seg_11318" s="T363">n</ta>
            <ta e="T365" id="Seg_11319" s="T364">v</ta>
            <ta e="T366" id="Seg_11320" s="T365">v</ta>
            <ta e="T367" id="Seg_11321" s="T366">ptcl</ta>
            <ta e="T368" id="Seg_11322" s="T367">dempro</ta>
            <ta e="T369" id="Seg_11323" s="T368">dempro</ta>
            <ta e="T370" id="Seg_11324" s="T369">post</ta>
            <ta e="T371" id="Seg_11325" s="T370">adj</ta>
            <ta e="T372" id="Seg_11326" s="T371">v</ta>
            <ta e="T373" id="Seg_11327" s="T372">v</ta>
            <ta e="T374" id="Seg_11328" s="T373">post</ta>
            <ta e="T375" id="Seg_11329" s="T374">adv</ta>
            <ta e="T376" id="Seg_11330" s="T375">ptcl</ta>
            <ta e="T377" id="Seg_11331" s="T376">dempro</ta>
            <ta e="T378" id="Seg_11332" s="T377">n</ta>
            <ta e="T379" id="Seg_11333" s="T378">que</ta>
            <ta e="T380" id="Seg_11334" s="T379">conj</ta>
            <ta e="T381" id="Seg_11335" s="T380">adj</ta>
            <ta e="T382" id="Seg_11336" s="T381">n</ta>
            <ta e="T383" id="Seg_11337" s="T382">v</ta>
            <ta e="T384" id="Seg_11338" s="T383">n</ta>
            <ta e="T385" id="Seg_11339" s="T384">v</ta>
            <ta e="T386" id="Seg_11340" s="T385">n</ta>
            <ta e="T387" id="Seg_11341" s="T386">v</ta>
            <ta e="T388" id="Seg_11342" s="T387">adj</ta>
            <ta e="T389" id="Seg_11343" s="T388">n</ta>
            <ta e="T390" id="Seg_11344" s="T389">adj</ta>
            <ta e="T391" id="Seg_11345" s="T390">n</ta>
            <ta e="T392" id="Seg_11346" s="T391">adj</ta>
            <ta e="T393" id="Seg_11347" s="T392">n</ta>
            <ta e="T394" id="Seg_11348" s="T393">adj</ta>
            <ta e="T395" id="Seg_11349" s="T394">n</ta>
            <ta e="T396" id="Seg_11350" s="T395">dempro</ta>
            <ta e="T397" id="Seg_11351" s="T396">adj</ta>
            <ta e="T398" id="Seg_11352" s="T397">n</ta>
            <ta e="T399" id="Seg_11353" s="T398">v</ta>
            <ta e="T400" id="Seg_11354" s="T399">dempro</ta>
            <ta e="T401" id="Seg_11355" s="T400">v</ta>
            <ta e="T402" id="Seg_11356" s="T401">adv</ta>
            <ta e="T403" id="Seg_11357" s="T402">n</ta>
            <ta e="T404" id="Seg_11358" s="T403">v</ta>
            <ta e="T405" id="Seg_11359" s="T404">post</ta>
            <ta e="T406" id="Seg_11360" s="T405">n</ta>
            <ta e="T407" id="Seg_11361" s="T406">v</ta>
            <ta e="T408" id="Seg_11362" s="T407">adv</ta>
            <ta e="T409" id="Seg_11363" s="T408">v</ta>
            <ta e="T410" id="Seg_11364" s="T409">n</ta>
            <ta e="T411" id="Seg_11365" s="T410">v</ta>
            <ta e="T412" id="Seg_11366" s="T411">dempro</ta>
            <ta e="T413" id="Seg_11367" s="T412">n</ta>
            <ta e="T414" id="Seg_11368" s="T413">n</ta>
            <ta e="T415" id="Seg_11369" s="T414">cop</ta>
            <ta e="T416" id="Seg_11370" s="T415">n</ta>
            <ta e="T417" id="Seg_11371" s="T416">n</ta>
            <ta e="T418" id="Seg_11372" s="T417">ptcl</ta>
            <ta e="T419" id="Seg_11373" s="T418">v</ta>
            <ta e="T420" id="Seg_11374" s="T419">dempro</ta>
            <ta e="T421" id="Seg_11375" s="T420">n</ta>
            <ta e="T422" id="Seg_11376" s="T421">n</ta>
            <ta e="T423" id="Seg_11377" s="T422">v</ta>
            <ta e="T424" id="Seg_11378" s="T423">n</ta>
            <ta e="T425" id="Seg_11379" s="T424">dempro</ta>
            <ta e="T426" id="Seg_11380" s="T425">n</ta>
            <ta e="T427" id="Seg_11381" s="T426">v</ta>
            <ta e="T428" id="Seg_11382" s="T427">aux</ta>
            <ta e="T429" id="Seg_11383" s="T428">v</ta>
            <ta e="T430" id="Seg_11384" s="T429">que</ta>
            <ta e="T431" id="Seg_11385" s="T430">ptcl</ta>
            <ta e="T432" id="Seg_11386" s="T431">n</ta>
            <ta e="T433" id="Seg_11387" s="T432">v</ta>
            <ta e="T434" id="Seg_11388" s="T433">adv</ta>
            <ta e="T435" id="Seg_11389" s="T434">v</ta>
            <ta e="T436" id="Seg_11390" s="T435">n</ta>
            <ta e="T437" id="Seg_11391" s="T436">n</ta>
            <ta e="T438" id="Seg_11392" s="T437">ptcl</ta>
            <ta e="T439" id="Seg_11393" s="T438">v</ta>
            <ta e="T440" id="Seg_11394" s="T439">dempro</ta>
            <ta e="T441" id="Seg_11395" s="T440">adv</ta>
            <ta e="T442" id="Seg_11396" s="T441">n</ta>
            <ta e="T443" id="Seg_11397" s="T442">ptcl</ta>
            <ta e="T444" id="Seg_11398" s="T443">n</ta>
            <ta e="T445" id="Seg_11399" s="T444">ptcl</ta>
            <ta e="T446" id="Seg_11400" s="T445">adv</ta>
            <ta e="T447" id="Seg_11401" s="T446">n</ta>
            <ta e="T448" id="Seg_11402" s="T447">v</ta>
            <ta e="T449" id="Seg_11403" s="T448">post</ta>
            <ta e="T451" id="Seg_11404" s="T450">n</ta>
            <ta e="T452" id="Seg_11405" s="T451">v</ta>
            <ta e="T453" id="Seg_11406" s="T452">post</ta>
            <ta e="T454" id="Seg_11407" s="T453">adv</ta>
            <ta e="T455" id="Seg_11408" s="T454">n</ta>
            <ta e="T456" id="Seg_11409" s="T455">v</ta>
            <ta e="T457" id="Seg_11410" s="T456">ptcl</ta>
            <ta e="T458" id="Seg_11411" s="T457">n</ta>
            <ta e="T459" id="Seg_11412" s="T458">v</ta>
            <ta e="T460" id="Seg_11413" s="T459">n</ta>
            <ta e="T461" id="Seg_11414" s="T460">cop</ta>
            <ta e="T462" id="Seg_11415" s="T461">v</ta>
            <ta e="T463" id="Seg_11416" s="T462">aux</ta>
            <ta e="T464" id="Seg_11417" s="T463">dempro</ta>
            <ta e="T465" id="Seg_11418" s="T464">v</ta>
            <ta e="T466" id="Seg_11419" s="T465">post</ta>
            <ta e="T467" id="Seg_11420" s="T466">dempro</ta>
            <ta e="T468" id="Seg_11421" s="T467">v</ta>
            <ta e="T469" id="Seg_11422" s="T468">post</ta>
            <ta e="T470" id="Seg_11423" s="T469">dempro</ta>
            <ta e="T471" id="Seg_11424" s="T470">n</ta>
            <ta e="T472" id="Seg_11425" s="T471">v</ta>
            <ta e="T473" id="Seg_11426" s="T472">adv</ta>
            <ta e="T474" id="Seg_11427" s="T473">v</ta>
            <ta e="T475" id="Seg_11428" s="T474">dempro</ta>
            <ta e="T476" id="Seg_11429" s="T475">v</ta>
            <ta e="T477" id="Seg_11430" s="T476">cardnum</ta>
            <ta e="T478" id="Seg_11431" s="T477">adj</ta>
            <ta e="T479" id="Seg_11432" s="T478">adj</ta>
            <ta e="T480" id="Seg_11433" s="T479">cop</ta>
            <ta e="T481" id="Seg_11434" s="T480">dempro</ta>
            <ta e="T482" id="Seg_11435" s="T481">n</ta>
            <ta e="T483" id="Seg_11436" s="T482">n</ta>
            <ta e="T484" id="Seg_11437" s="T483">v</ta>
            <ta e="T485" id="Seg_11438" s="T484">dempro</ta>
            <ta e="T486" id="Seg_11439" s="T485">post</ta>
            <ta e="T487" id="Seg_11440" s="T486">dempro</ta>
            <ta e="T488" id="Seg_11441" s="T487">v</ta>
            <ta e="T489" id="Seg_11442" s="T488">n</ta>
            <ta e="T490" id="Seg_11443" s="T489">n</ta>
            <ta e="T491" id="Seg_11444" s="T490">ptcl</ta>
            <ta e="T492" id="Seg_11445" s="T491">v</ta>
            <ta e="T493" id="Seg_11446" s="T492">dempro</ta>
            <ta e="T494" id="Seg_11447" s="T493">adv</ta>
            <ta e="T495" id="Seg_11448" s="T494">adj</ta>
            <ta e="T496" id="Seg_11449" s="T495">n</ta>
            <ta e="T497" id="Seg_11450" s="T496">ptcl</ta>
            <ta e="T498" id="Seg_11451" s="T497">v</ta>
            <ta e="T499" id="Seg_11452" s="T498">que</ta>
            <ta e="T500" id="Seg_11453" s="T499">ptcl</ta>
            <ta e="T501" id="Seg_11454" s="T500">adj</ta>
            <ta e="T502" id="Seg_11455" s="T501">n</ta>
            <ta e="T503" id="Seg_11456" s="T502">cop</ta>
            <ta e="T504" id="Seg_11457" s="T503">n</ta>
            <ta e="T505" id="Seg_11458" s="T504">dempro</ta>
            <ta e="T506" id="Seg_11459" s="T505">n</ta>
            <ta e="T507" id="Seg_11460" s="T506">post</ta>
            <ta e="T508" id="Seg_11461" s="T507">n</ta>
            <ta e="T509" id="Seg_11462" s="T508">n</ta>
            <ta e="T510" id="Seg_11463" s="T509">n</ta>
            <ta e="T511" id="Seg_11464" s="T510">v</ta>
            <ta e="T512" id="Seg_11465" s="T511">ptcl</ta>
            <ta e="T513" id="Seg_11466" s="T512">adv</ta>
            <ta e="T514" id="Seg_11467" s="T513">ptcl</ta>
            <ta e="T515" id="Seg_11468" s="T514">v</ta>
            <ta e="T516" id="Seg_11469" s="T515">n</ta>
            <ta e="T517" id="Seg_11470" s="T516">ptcl</ta>
            <ta e="T518" id="Seg_11471" s="T517">adj</ta>
            <ta e="T519" id="Seg_11472" s="T518">n</ta>
            <ta e="T520" id="Seg_11473" s="T519">ptcl</ta>
            <ta e="T521" id="Seg_11474" s="T520">adv</ta>
            <ta e="T522" id="Seg_11475" s="T521">dempro</ta>
            <ta e="T523" id="Seg_11476" s="T522">n</ta>
            <ta e="T524" id="Seg_11477" s="T523">n</ta>
            <ta e="T525" id="Seg_11478" s="T524">n</ta>
            <ta e="T526" id="Seg_11479" s="T525">v</ta>
            <ta e="T527" id="Seg_11480" s="T526">adj</ta>
            <ta e="T528" id="Seg_11481" s="T527">n</ta>
            <ta e="T529" id="Seg_11482" s="T528">v</ta>
            <ta e="T530" id="Seg_11483" s="T529">v</ta>
            <ta e="T531" id="Seg_11484" s="T530">n</ta>
            <ta e="T532" id="Seg_11485" s="T531">v</ta>
            <ta e="T533" id="Seg_11486" s="T532">n</ta>
            <ta e="T534" id="Seg_11487" s="T533">n</ta>
            <ta e="T535" id="Seg_11488" s="T534">v</ta>
            <ta e="T536" id="Seg_11489" s="T535">dempro</ta>
            <ta e="T537" id="Seg_11490" s="T536">n</ta>
            <ta e="T538" id="Seg_11491" s="T537">v</ta>
            <ta e="T539" id="Seg_11492" s="T538">dempro</ta>
            <ta e="T540" id="Seg_11493" s="T539">n</ta>
            <ta e="T541" id="Seg_11494" s="T540">v</ta>
            <ta e="T542" id="Seg_11495" s="T541">emphpro</ta>
            <ta e="T543" id="Seg_11496" s="T542">n</ta>
            <ta e="T544" id="Seg_11497" s="T543">adv</ta>
            <ta e="T545" id="Seg_11498" s="T544">v</ta>
            <ta e="T546" id="Seg_11499" s="T545">v</ta>
            <ta e="T547" id="Seg_11500" s="T546">n</ta>
            <ta e="T548" id="Seg_11501" s="T547">n</ta>
            <ta e="T549" id="Seg_11502" s="T548">emphpro</ta>
            <ta e="T550" id="Seg_11503" s="T549">v</ta>
            <ta e="T551" id="Seg_11504" s="T550">v</ta>
            <ta e="T552" id="Seg_11505" s="T551">post</ta>
            <ta e="T553" id="Seg_11506" s="T552">aux</ta>
            <ta e="T554" id="Seg_11507" s="T553">n</ta>
            <ta e="T555" id="Seg_11508" s="T554">conj</ta>
            <ta e="T556" id="Seg_11509" s="T555">adj</ta>
            <ta e="T557" id="Seg_11510" s="T556">indfpro</ta>
            <ta e="T558" id="Seg_11511" s="T557">n</ta>
            <ta e="T559" id="Seg_11512" s="T558">adv</ta>
            <ta e="T560" id="Seg_11513" s="T559">v</ta>
            <ta e="T561" id="Seg_11514" s="T560">n</ta>
            <ta e="T562" id="Seg_11515" s="T561">adj</ta>
            <ta e="T563" id="Seg_11516" s="T562">v</ta>
            <ta e="T564" id="Seg_11517" s="T563">v</ta>
            <ta e="T565" id="Seg_11518" s="T564">dempro</ta>
            <ta e="T566" id="Seg_11519" s="T565">indfpro</ta>
            <ta e="T567" id="Seg_11520" s="T566">n</ta>
            <ta e="T568" id="Seg_11521" s="T567">n</ta>
            <ta e="T569" id="Seg_11522" s="T568">conj</ta>
            <ta e="T570" id="Seg_11523" s="T569">v</ta>
            <ta e="T571" id="Seg_11524" s="T570">v</ta>
            <ta e="T572" id="Seg_11525" s="T571">n</ta>
            <ta e="T573" id="Seg_11526" s="T572">v</ta>
            <ta e="T574" id="Seg_11527" s="T573">n</ta>
            <ta e="T575" id="Seg_11528" s="T574">adj</ta>
            <ta e="T576" id="Seg_11529" s="T575">n</ta>
            <ta e="T577" id="Seg_11530" s="T576">v</ta>
            <ta e="T578" id="Seg_11531" s="T577">n</ta>
            <ta e="T579" id="Seg_11532" s="T578">v</ta>
            <ta e="T580" id="Seg_11533" s="T579">dempro</ta>
            <ta e="T581" id="Seg_11534" s="T580">adv</ta>
            <ta e="T582" id="Seg_11535" s="T581">n</ta>
            <ta e="T583" id="Seg_11536" s="T582">cop</ta>
            <ta e="T584" id="Seg_11537" s="T583">dempro</ta>
            <ta e="T585" id="Seg_11538" s="T584">n</ta>
            <ta e="T587" id="Seg_11539" s="T586">v</ta>
            <ta e="T588" id="Seg_11540" s="T587">dempro</ta>
            <ta e="T589" id="Seg_11541" s="T588">adv</ta>
            <ta e="T590" id="Seg_11542" s="T589">n</ta>
            <ta e="T591" id="Seg_11543" s="T590">n</ta>
            <ta e="T592" id="Seg_11544" s="T591">adj</ta>
            <ta e="T593" id="Seg_11545" s="T592">v</ta>
            <ta e="T594" id="Seg_11546" s="T593">v</ta>
            <ta e="T595" id="Seg_11547" s="T594">dempro</ta>
            <ta e="T596" id="Seg_11548" s="T595">n</ta>
            <ta e="T597" id="Seg_11549" s="T596">v</ta>
            <ta e="T598" id="Seg_11550" s="T597">v</ta>
            <ta e="T599" id="Seg_11551" s="T598">adv</ta>
            <ta e="T600" id="Seg_11552" s="T599">n</ta>
            <ta e="T601" id="Seg_11553" s="T600">dempro</ta>
            <ta e="T602" id="Seg_11554" s="T601">n</ta>
            <ta e="T603" id="Seg_11555" s="T602">adj</ta>
            <ta e="T604" id="Seg_11556" s="T603">n</ta>
            <ta e="T605" id="Seg_11557" s="T604">que</ta>
            <ta e="T606" id="Seg_11558" s="T605">ptcl</ta>
            <ta e="T607" id="Seg_11559" s="T606">ptcl</ta>
            <ta e="T608" id="Seg_11560" s="T607">v</ta>
            <ta e="T609" id="Seg_11561" s="T608">adv</ta>
            <ta e="T610" id="Seg_11562" s="T609">adv</ta>
            <ta e="T611" id="Seg_11563" s="T610">adj</ta>
            <ta e="T612" id="Seg_11564" s="T611">post</ta>
            <ta e="T613" id="Seg_11565" s="T612">v</ta>
            <ta e="T614" id="Seg_11566" s="T613">dempro</ta>
            <ta e="T615" id="Seg_11567" s="T614">v</ta>
            <ta e="T616" id="Seg_11568" s="T615">n</ta>
            <ta e="T617" id="Seg_11569" s="T616">v</ta>
            <ta e="T618" id="Seg_11570" s="T617">v</ta>
            <ta e="T619" id="Seg_11571" s="T618">aux</ta>
            <ta e="T620" id="Seg_11572" s="T619">adj</ta>
            <ta e="T621" id="Seg_11573" s="T620">n</ta>
            <ta e="T622" id="Seg_11574" s="T621">v</ta>
            <ta e="T623" id="Seg_11575" s="T622">dempro</ta>
            <ta e="T624" id="Seg_11576" s="T623">adj</ta>
            <ta e="T625" id="Seg_11577" s="T624">adj</ta>
            <ta e="T626" id="Seg_11578" s="T625">n</ta>
            <ta e="T627" id="Seg_11579" s="T626">adj</ta>
            <ta e="T628" id="Seg_11580" s="T627">cop</ta>
            <ta e="T629" id="Seg_11581" s="T628">que</ta>
            <ta e="T630" id="Seg_11582" s="T629">conj</ta>
            <ta e="T631" id="Seg_11583" s="T630">n</ta>
            <ta e="T632" id="Seg_11584" s="T631">dempro</ta>
            <ta e="T633" id="Seg_11585" s="T632">n</ta>
            <ta e="T634" id="Seg_11586" s="T633">v</ta>
            <ta e="T635" id="Seg_11587" s="T634">conj</ta>
            <ta e="T636" id="Seg_11588" s="T635">n</ta>
            <ta e="T637" id="Seg_11589" s="T636">dempro</ta>
            <ta e="T638" id="Seg_11590" s="T637">n</ta>
            <ta e="T639" id="Seg_11591" s="T638">v</ta>
            <ta e="T640" id="Seg_11592" s="T639">v</ta>
            <ta e="T641" id="Seg_11593" s="T640">aux</ta>
            <ta e="T642" id="Seg_11594" s="T641">conj</ta>
            <ta e="T643" id="Seg_11595" s="T642">v</ta>
            <ta e="T644" id="Seg_11596" s="T643">conj</ta>
            <ta e="T645" id="Seg_11597" s="T644">aux</ta>
            <ta e="T646" id="Seg_11598" s="T645">dempro</ta>
            <ta e="T647" id="Seg_11599" s="T646">n</ta>
            <ta e="T648" id="Seg_11600" s="T647">dempro</ta>
            <ta e="T649" id="Seg_11601" s="T648">n</ta>
            <ta e="T650" id="Seg_11602" s="T649">v</ta>
            <ta e="T651" id="Seg_11603" s="T650">v</ta>
            <ta e="T652" id="Seg_11604" s="T651">aux</ta>
            <ta e="T653" id="Seg_11605" s="T652">conj</ta>
            <ta e="T654" id="Seg_11606" s="T653">adv</ta>
            <ta e="T655" id="Seg_11607" s="T654">v</ta>
            <ta e="T656" id="Seg_11608" s="T655">v</ta>
            <ta e="T657" id="Seg_11609" s="T656">aux</ta>
            <ta e="T658" id="Seg_11610" s="T657">adv</ta>
            <ta e="T659" id="Seg_11611" s="T658">cardnum</ta>
            <ta e="T660" id="Seg_11612" s="T659">n</ta>
            <ta e="T661" id="Seg_11613" s="T660">cardnum</ta>
            <ta e="T662" id="Seg_11614" s="T661">ptcl</ta>
            <ta e="T663" id="Seg_11615" s="T662">cardnum</ta>
            <ta e="T664" id="Seg_11616" s="T663">n</ta>
            <ta e="T665" id="Seg_11617" s="T664">cardnum</ta>
            <ta e="T666" id="Seg_11618" s="T665">ptcl</ta>
            <ta e="T667" id="Seg_11619" s="T666">n</ta>
            <ta e="T668" id="Seg_11620" s="T667">v</ta>
            <ta e="T669" id="Seg_11621" s="T668">dempro</ta>
            <ta e="T670" id="Seg_11622" s="T669">v</ta>
            <ta e="T671" id="Seg_11623" s="T670">v</ta>
            <ta e="T672" id="Seg_11624" s="T671">n</ta>
            <ta e="T673" id="Seg_11625" s="T672">v</ta>
            <ta e="T674" id="Seg_11626" s="T673">adj</ta>
            <ta e="T675" id="Seg_11627" s="T674">n</ta>
            <ta e="T676" id="Seg_11628" s="T675">v</ta>
            <ta e="T677" id="Seg_11629" s="T676">adv</ta>
            <ta e="T678" id="Seg_11630" s="T677">dempro</ta>
            <ta e="T679" id="Seg_11631" s="T678">v</ta>
            <ta e="T680" id="Seg_11632" s="T679">v</ta>
            <ta e="T681" id="Seg_11633" s="T680">n</ta>
            <ta e="T682" id="Seg_11634" s="T681">ptcl</ta>
            <ta e="T683" id="Seg_11635" s="T682">n</ta>
            <ta e="T684" id="Seg_11636" s="T683">dempro</ta>
            <ta e="T685" id="Seg_11637" s="T684">n</ta>
            <ta e="T686" id="Seg_11638" s="T685">adv</ta>
            <ta e="T687" id="Seg_11639" s="T686">n</ta>
            <ta e="T688" id="Seg_11640" s="T687">v</ta>
            <ta e="T689" id="Seg_11641" s="T688">cardnum</ta>
            <ta e="T690" id="Seg_11642" s="T689">n</ta>
            <ta e="T691" id="Seg_11643" s="T690">n</ta>
            <ta e="T692" id="Seg_11644" s="T691">v</ta>
            <ta e="T693" id="Seg_11645" s="T692">ptcl</ta>
            <ta e="T694" id="Seg_11646" s="T693">v</ta>
            <ta e="T695" id="Seg_11647" s="T694">aux</ta>
            <ta e="T696" id="Seg_11648" s="T695">post</ta>
            <ta e="T697" id="Seg_11649" s="T696">adj</ta>
            <ta e="T698" id="Seg_11650" s="T697">n</ta>
            <ta e="T699" id="Seg_11651" s="T698">v</ta>
            <ta e="T700" id="Seg_11652" s="T699">dempro</ta>
            <ta e="T701" id="Seg_11653" s="T700">adj</ta>
            <ta e="T702" id="Seg_11654" s="T701">n</ta>
            <ta e="T703" id="Seg_11655" s="T702">v</ta>
            <ta e="T704" id="Seg_11656" s="T703">adv</ta>
            <ta e="T705" id="Seg_11657" s="T704">n</ta>
            <ta e="T706" id="Seg_11658" s="T705">v</ta>
            <ta e="T707" id="Seg_11659" s="T706">v</ta>
            <ta e="T708" id="Seg_11660" s="T707">n</ta>
            <ta e="T709" id="Seg_11661" s="T708">adv</ta>
            <ta e="T710" id="Seg_11662" s="T709">v</ta>
            <ta e="T711" id="Seg_11663" s="T710">n</ta>
            <ta e="T712" id="Seg_11664" s="T711">v</ta>
            <ta e="T713" id="Seg_11665" s="T712">v</ta>
            <ta e="T714" id="Seg_11666" s="T713">interj</ta>
            <ta e="T715" id="Seg_11667" s="T714">posspr</ta>
            <ta e="T716" id="Seg_11668" s="T715">n</ta>
            <ta e="T717" id="Seg_11669" s="T716">ptcl</ta>
            <ta e="T718" id="Seg_11670" s="T717">adv</ta>
            <ta e="T719" id="Seg_11671" s="T718">n</ta>
            <ta e="T720" id="Seg_11672" s="T719">ptcl</ta>
            <ta e="T721" id="Seg_11673" s="T720">v</ta>
            <ta e="T722" id="Seg_11674" s="T721">adj</ta>
            <ta e="T723" id="Seg_11675" s="T722">v</ta>
            <ta e="T724" id="Seg_11676" s="T723">n</ta>
            <ta e="T725" id="Seg_11677" s="T724">adj</ta>
            <ta e="T726" id="Seg_11678" s="T725">n</ta>
            <ta e="T727" id="Seg_11679" s="T726">v</ta>
            <ta e="T728" id="Seg_11680" s="T727">v</ta>
            <ta e="T729" id="Seg_11681" s="T728">aux</ta>
            <ta e="T730" id="Seg_11682" s="T729">n</ta>
            <ta e="T731" id="Seg_11683" s="T730">cop</ta>
            <ta e="T732" id="Seg_11684" s="T731">n</ta>
            <ta e="T733" id="Seg_11685" s="T732">conj</ta>
            <ta e="T734" id="Seg_11686" s="T733">adv</ta>
            <ta e="T735" id="Seg_11687" s="T734">n</ta>
            <ta e="T736" id="Seg_11688" s="T735">v</ta>
            <ta e="T737" id="Seg_11689" s="T736">ptcl</ta>
            <ta e="T738" id="Seg_11690" s="T737">v</ta>
            <ta e="T739" id="Seg_11691" s="T738">v</ta>
            <ta e="T740" id="Seg_11692" s="T739">dempro</ta>
            <ta e="T741" id="Seg_11693" s="T740">n</ta>
            <ta e="T742" id="Seg_11694" s="T741">dempro</ta>
            <ta e="T743" id="Seg_11695" s="T742">n</ta>
            <ta e="T744" id="Seg_11696" s="T743">v</ta>
            <ta e="T745" id="Seg_11697" s="T744">v</ta>
            <ta e="T746" id="Seg_11698" s="T745">adv</ta>
            <ta e="T747" id="Seg_11699" s="T746">v</ta>
            <ta e="T748" id="Seg_11700" s="T747">n</ta>
            <ta e="T749" id="Seg_11701" s="T748">dempro</ta>
            <ta e="T750" id="Seg_11702" s="T749">n</ta>
            <ta e="T751" id="Seg_11703" s="T750">adj</ta>
            <ta e="T752" id="Seg_11704" s="T751">n</ta>
            <ta e="T753" id="Seg_11705" s="T752">v</ta>
            <ta e="T754" id="Seg_11706" s="T753">n</ta>
            <ta e="T755" id="Seg_11707" s="T754">n</ta>
            <ta e="T756" id="Seg_11708" s="T755">n</ta>
            <ta e="T757" id="Seg_11709" s="T756">v</ta>
            <ta e="T758" id="Seg_11710" s="T757">dempro</ta>
            <ta e="T759" id="Seg_11711" s="T758">adj</ta>
            <ta e="T760" id="Seg_11712" s="T759">n</ta>
            <ta e="T761" id="Seg_11713" s="T760">v</ta>
            <ta e="T762" id="Seg_11714" s="T761">n</ta>
            <ta e="T763" id="Seg_11715" s="T762">n</ta>
            <ta e="T764" id="Seg_11716" s="T763">adv</ta>
            <ta e="T765" id="Seg_11717" s="T764">v</ta>
            <ta e="T766" id="Seg_11718" s="T765">dempro</ta>
            <ta e="T767" id="Seg_11719" s="T766">n</ta>
            <ta e="T768" id="Seg_11720" s="T767">dempro</ta>
            <ta e="T769" id="Seg_11721" s="T768">adj</ta>
            <ta e="T770" id="Seg_11722" s="T769">n</ta>
            <ta e="T771" id="Seg_11723" s="T770">v</ta>
            <ta e="T772" id="Seg_11724" s="T771">adv</ta>
            <ta e="T773" id="Seg_11725" s="T772">v</ta>
            <ta e="T774" id="Seg_11726" s="T773">cardnum</ta>
            <ta e="T775" id="Seg_11727" s="T774">post</ta>
            <ta e="T776" id="Seg_11728" s="T775">n</ta>
            <ta e="T777" id="Seg_11729" s="T776">dempro</ta>
            <ta e="T778" id="Seg_11730" s="T777">post</ta>
            <ta e="T781" id="Seg_11731" s="T780">n</ta>
            <ta e="T782" id="Seg_11732" s="T781">n</ta>
            <ta e="T783" id="Seg_11733" s="T782">ptcl</ta>
            <ta e="T784" id="Seg_11734" s="T783">v</ta>
            <ta e="T785" id="Seg_11735" s="T784">dempro</ta>
            <ta e="T786" id="Seg_11736" s="T785">adv</ta>
            <ta e="T787" id="Seg_11737" s="T786">n</ta>
            <ta e="T788" id="Seg_11738" s="T787">adj</ta>
            <ta e="T789" id="Seg_11739" s="T788">cop</ta>
            <ta e="T790" id="Seg_11740" s="T789">v</ta>
            <ta e="T791" id="Seg_11741" s="T790">aux</ta>
            <ta e="T792" id="Seg_11742" s="T791">dempro</ta>
            <ta e="T793" id="Seg_11743" s="T792">n</ta>
            <ta e="T794" id="Seg_11744" s="T793">ptcl</ta>
            <ta e="T795" id="Seg_11745" s="T794">adj</ta>
            <ta e="T796" id="Seg_11746" s="T795">n</ta>
            <ta e="T797" id="Seg_11747" s="T796">n</ta>
            <ta e="T798" id="Seg_11748" s="T797">v</ta>
            <ta e="T799" id="Seg_11749" s="T798">v</ta>
            <ta e="T800" id="Seg_11750" s="T799">v</ta>
            <ta e="T801" id="Seg_11751" s="T800">que</ta>
            <ta e="T802" id="Seg_11752" s="T801">n</ta>
            <ta e="T803" id="Seg_11753" s="T802">n</ta>
            <ta e="T804" id="Seg_11754" s="T803">n</ta>
            <ta e="T805" id="Seg_11755" s="T804">cop</ta>
            <ta e="T806" id="Seg_11756" s="T805">n</ta>
            <ta e="T807" id="Seg_11757" s="T806">n</ta>
            <ta e="T808" id="Seg_11758" s="T807">n</ta>
            <ta e="T809" id="Seg_11759" s="T808">n</ta>
            <ta e="T810" id="Seg_11760" s="T809">v</ta>
            <ta e="T811" id="Seg_11761" s="T810">dempro</ta>
            <ta e="T812" id="Seg_11762" s="T811">n</ta>
            <ta e="T813" id="Seg_11763" s="T812">v</ta>
            <ta e="T814" id="Seg_11764" s="T813">n</ta>
            <ta e="T815" id="Seg_11765" s="T814">n</ta>
            <ta e="T816" id="Seg_11766" s="T815">v</ta>
            <ta e="T817" id="Seg_11767" s="T816">post</ta>
            <ta e="T818" id="Seg_11768" s="T817">n</ta>
            <ta e="T819" id="Seg_11769" s="T818">n</ta>
            <ta e="T820" id="Seg_11770" s="T819">v</ta>
            <ta e="T821" id="Seg_11771" s="T820">n</ta>
            <ta e="T822" id="Seg_11772" s="T821">v</ta>
            <ta e="T823" id="Seg_11773" s="T822">n</ta>
            <ta e="T824" id="Seg_11774" s="T823">n</ta>
            <ta e="T825" id="Seg_11775" s="T824">v</ta>
            <ta e="T826" id="Seg_11776" s="T825">ptcl</ta>
            <ta e="T827" id="Seg_11777" s="T826">n</ta>
            <ta e="T828" id="Seg_11778" s="T827">n</ta>
            <ta e="T830" id="Seg_11779" s="T929">v</ta>
            <ta e="T831" id="Seg_11780" s="T830">n</ta>
            <ta e="T832" id="Seg_11781" s="T831">v</ta>
            <ta e="T833" id="Seg_11782" s="T832">adj</ta>
            <ta e="T834" id="Seg_11783" s="T833">n</ta>
            <ta e="T835" id="Seg_11784" s="T834">v</ta>
            <ta e="T836" id="Seg_11785" s="T835">pers</ta>
            <ta e="T837" id="Seg_11786" s="T836">v</ta>
            <ta e="T838" id="Seg_11787" s="T837">ptcl</ta>
            <ta e="T839" id="Seg_11788" s="T838">ptcl</ta>
            <ta e="T840" id="Seg_11789" s="T839">v</ta>
            <ta e="T841" id="Seg_11790" s="T840">que</ta>
            <ta e="T842" id="Seg_11791" s="T841">ptcl</ta>
            <ta e="T843" id="Seg_11792" s="T842">n</ta>
            <ta e="T844" id="Seg_11793" s="T843">n</ta>
            <ta e="T845" id="Seg_11794" s="T844">post</ta>
            <ta e="T846" id="Seg_11795" s="T845">n</ta>
            <ta e="T847" id="Seg_11796" s="T846">v</ta>
            <ta e="T848" id="Seg_11797" s="T847">n</ta>
            <ta e="T849" id="Seg_11798" s="T848">n</ta>
            <ta e="T850" id="Seg_11799" s="T849">n</ta>
            <ta e="T851" id="Seg_11800" s="T850">dempro</ta>
            <ta e="T852" id="Seg_11801" s="T851">cop</ta>
            <ta e="T853" id="Seg_11802" s="T852">post</ta>
            <ta e="T854" id="Seg_11803" s="T853">n</ta>
            <ta e="T855" id="Seg_11804" s="T854">adv</ta>
            <ta e="T856" id="Seg_11805" s="T855">v</ta>
            <ta e="T857" id="Seg_11806" s="T856">n</ta>
            <ta e="T858" id="Seg_11807" s="T857">post</ta>
            <ta e="T859" id="Seg_11808" s="T858">v</ta>
            <ta e="T860" id="Seg_11809" s="T859">n</ta>
            <ta e="T861" id="Seg_11810" s="T860">v</ta>
            <ta e="T862" id="Seg_11811" s="T861">n</ta>
            <ta e="T863" id="Seg_11812" s="T862">v</ta>
            <ta e="T864" id="Seg_11813" s="T863">adv</ta>
            <ta e="T865" id="Seg_11814" s="T864">v</ta>
            <ta e="T866" id="Seg_11815" s="T865">adv</ta>
            <ta e="T867" id="Seg_11816" s="T866">v</ta>
            <ta e="T868" id="Seg_11817" s="T867">n</ta>
            <ta e="T869" id="Seg_11818" s="T868">dempro</ta>
            <ta e="T870" id="Seg_11819" s="T869">post</ta>
            <ta e="T871" id="Seg_11820" s="T870">pers</ta>
            <ta e="T872" id="Seg_11821" s="T871">adj</ta>
            <ta e="T873" id="Seg_11822" s="T872">n</ta>
            <ta e="T874" id="Seg_11823" s="T873">v</ta>
            <ta e="T875" id="Seg_11824" s="T874">ptcl</ta>
            <ta e="T876" id="Seg_11825" s="T875">adv</ta>
            <ta e="T877" id="Seg_11826" s="T876">n</ta>
            <ta e="T878" id="Seg_11827" s="T877">v</ta>
            <ta e="T879" id="Seg_11828" s="T878">n</ta>
            <ta e="T880" id="Seg_11829" s="T879">v</ta>
            <ta e="T881" id="Seg_11830" s="T880">n</ta>
            <ta e="T882" id="Seg_11831" s="T881">n</ta>
            <ta e="T883" id="Seg_11832" s="T882">v</ta>
            <ta e="T884" id="Seg_11833" s="T883">ptcl</ta>
            <ta e="T885" id="Seg_11834" s="T884">adj</ta>
            <ta e="T886" id="Seg_11835" s="T885">n</ta>
            <ta e="T887" id="Seg_11836" s="T886">v</ta>
            <ta e="T888" id="Seg_11837" s="T887">ptcl</ta>
            <ta e="T889" id="Seg_11838" s="T888">n</ta>
            <ta e="T890" id="Seg_11839" s="T889">v</ta>
            <ta e="T891" id="Seg_11840" s="T890">conj</ta>
            <ta e="T892" id="Seg_11841" s="T891">que</ta>
            <ta e="T893" id="Seg_11842" s="T892">conj</ta>
            <ta e="T894" id="Seg_11843" s="T893">n</ta>
            <ta e="T896" id="Seg_11844" s="T894">n</ta>
            <ta e="T897" id="Seg_11845" s="T896">n</ta>
            <ta e="T898" id="Seg_11846" s="T897">n</ta>
            <ta e="T899" id="Seg_11847" s="T898">n</ta>
            <ta e="T900" id="Seg_11848" s="T899">v</ta>
            <ta e="T901" id="Seg_11849" s="T900">v</ta>
            <ta e="T902" id="Seg_11850" s="T901">v</ta>
            <ta e="T903" id="Seg_11851" s="T902">aux</ta>
            <ta e="T904" id="Seg_11852" s="T903">interj</ta>
            <ta e="T905" id="Seg_11853" s="T904">n</ta>
            <ta e="T906" id="Seg_11854" s="T905">cop</ta>
            <ta e="T907" id="Seg_11855" s="T906">ptcl</ta>
            <ta e="T908" id="Seg_11856" s="T907">v</ta>
            <ta e="T909" id="Seg_11857" s="T908">v</ta>
            <ta e="T910" id="Seg_11858" s="T909">v</ta>
            <ta e="T911" id="Seg_11859" s="T910">n</ta>
            <ta e="T912" id="Seg_11860" s="T911">v</ta>
            <ta e="T913" id="Seg_11861" s="T912">adj</ta>
            <ta e="T914" id="Seg_11862" s="T913">cop</ta>
            <ta e="T915" id="Seg_11863" s="T914">ptcl</ta>
            <ta e="T916" id="Seg_11864" s="T915">v</ta>
            <ta e="T917" id="Seg_11865" s="T916">ptcl</ta>
            <ta e="T918" id="Seg_11866" s="T917">que</ta>
            <ta e="T919" id="Seg_11867" s="T918">ptcl</ta>
            <ta e="T920" id="Seg_11868" s="T919">adj</ta>
            <ta e="T921" id="Seg_11869" s="T920">cop</ta>
            <ta e="T922" id="Seg_11870" s="T921">aux</ta>
            <ta e="T923" id="Seg_11871" s="T922">dempro</ta>
            <ta e="T924" id="Seg_11872" s="T923">post</ta>
            <ta e="T925" id="Seg_11873" s="T924">v</ta>
            <ta e="T926" id="Seg_11874" s="T925">aux</ta>
            <ta e="T927" id="Seg_11875" s="T926">pers</ta>
            <ta e="T928" id="Seg_11876" s="T927">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR" />
         <annotation name="SyF" tierref="SyF" />
         <annotation name="IST" tierref="IST" />
         <annotation name="Top" tierref="Top" />
         <annotation name="Foc" tierref="Foc" />
         <annotation name="BOR" tierref="BOR">
            <ta e="T25" id="Seg_11877" s="T24">RUS:gram</ta>
            <ta e="T36" id="Seg_11878" s="T35">RUS:cult</ta>
            <ta e="T54" id="Seg_11879" s="T53">RUS:cult</ta>
            <ta e="T58" id="Seg_11880" s="T57">RUS:cult</ta>
            <ta e="T93" id="Seg_11881" s="T92">RUS:mod</ta>
            <ta e="T146" id="Seg_11882" s="T145">RUS:cult</ta>
            <ta e="T150" id="Seg_11883" s="T149">RUS:cult</ta>
            <ta e="T151" id="Seg_11884" s="T150">RUS:core</ta>
            <ta e="T171" id="Seg_11885" s="T170">RUS:core</ta>
            <ta e="T211" id="Seg_11886" s="T210">RUS:mod</ta>
            <ta e="T244" id="Seg_11887" s="T243">RUS:cult</ta>
            <ta e="T284" id="Seg_11888" s="T283">RUS:mod</ta>
            <ta e="T289" id="Seg_11889" s="T288">RUS:cult</ta>
            <ta e="T295" id="Seg_11890" s="T294">RUS:cult</ta>
            <ta e="T339" id="Seg_11891" s="T338">RUS:cult</ta>
            <ta e="T353" id="Seg_11892" s="T352">RUS:cult</ta>
            <ta e="T358" id="Seg_11893" s="T357">RUS:cult</ta>
            <ta e="T361" id="Seg_11894" s="T360">RUS:cult</ta>
            <ta e="T378" id="Seg_11895" s="T377">RUS:cult</ta>
            <ta e="T380" id="Seg_11896" s="T379">RUS:gram</ta>
            <ta e="T384" id="Seg_11897" s="T383">RUS:cult</ta>
            <ta e="T398" id="Seg_11898" s="T397">RUS:cult</ta>
            <ta e="T410" id="Seg_11899" s="T409">RUS:cult</ta>
            <ta e="T426" id="Seg_11900" s="T425">RUS:cult</ta>
            <ta e="T442" id="Seg_11901" s="T441">RUS:cult</ta>
            <ta e="T444" id="Seg_11902" s="T443">RUS:cult</ta>
            <ta e="T457" id="Seg_11903" s="T456">RUS:mod</ta>
            <ta e="T458" id="Seg_11904" s="T457">RUS:cult</ta>
            <ta e="T460" id="Seg_11905" s="T459">RUS:core</ta>
            <ta e="T478" id="Seg_11906" s="T477">RUS:cult</ta>
            <ta e="T504" id="Seg_11907" s="T503">RUS:cult</ta>
            <ta e="T523" id="Seg_11908" s="T522">RUS:cult</ta>
            <ta e="T528" id="Seg_11909" s="T527">EV:cult</ta>
            <ta e="T555" id="Seg_11910" s="T554">RUS:gram</ta>
            <ta e="T567" id="Seg_11911" s="T566">RUS:cult</ta>
            <ta e="T569" id="Seg_11912" s="T568">RUS:gram</ta>
            <ta e="T630" id="Seg_11913" s="T629">RUS:gram</ta>
            <ta e="T635" id="Seg_11914" s="T634">RUS:gram</ta>
            <ta e="T642" id="Seg_11915" s="T641">RUS:gram</ta>
            <ta e="T644" id="Seg_11916" s="T643">RUS:gram</ta>
            <ta e="T647" id="Seg_11917" s="T646">RUS:cult</ta>
            <ta e="T653" id="Seg_11918" s="T652">RUS:gram</ta>
            <ta e="T683" id="Seg_11919" s="T682">EV:cult</ta>
            <ta e="T690" id="Seg_11920" s="T689">EV:cult</ta>
            <ta e="T708" id="Seg_11921" s="T707">EV:cult</ta>
            <ta e="T716" id="Seg_11922" s="T715">EV:cult</ta>
            <ta e="T733" id="Seg_11923" s="T732">RUS:gram</ta>
            <ta e="T762" id="Seg_11924" s="T761">RUS:cult</ta>
            <ta e="T763" id="Seg_11925" s="T762">EV:cult</ta>
            <ta e="T796" id="Seg_11926" s="T795">RUS:cult</ta>
            <ta e="T834" id="Seg_11927" s="T833">RUS:cult</ta>
            <ta e="T884" id="Seg_11928" s="T883">RUS:mod</ta>
            <ta e="T891" id="Seg_11929" s="T890">RUS:gram</ta>
            <ta e="T893" id="Seg_11930" s="T892">RUS:gram</ta>
            <ta e="T899" id="Seg_11931" s="T898">RUS:cult</ta>
            <ta e="T915" id="Seg_11932" s="T914">RUS:mod</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon" />
         <annotation name="BOR-Morph" tierref="BOR-Morph" />
         <annotation name="CS" tierref="CS" />
         <annotation name="fe" tierref="fe">
            <ta e="T5" id="Seg_11933" s="T0">Well, I am speaking, for that the reindeer herders can listen.</ta>
            <ta e="T11" id="Seg_11934" s="T5">I am a man who has worked a lot with reindeers earlier.</ta>
            <ta e="T17" id="Seg_11935" s="T11">And about the birth of a reindeer I will talk now.</ta>
            <ta e="T28" id="Seg_11936" s="T17">Everything comes from the birth, the emergence of a reindeer, also of a human.</ta>
            <ta e="T35" id="Seg_11937" s="T28">And so I talk about that when a reindeer is born.</ta>
            <ta e="T37" id="Seg_11938" s="T35">For that the shepherds can listen.</ta>
            <ta e="T41" id="Seg_11939" s="T37">I say so.</ta>
            <ta e="T45" id="Seg_11940" s="T41">Now they are caring about the reindeer.</ta>
            <ta e="T55" id="Seg_11941" s="T45">When I was caring about earlier, when a reindeer was born, there were many reasons.</ta>
            <ta e="T63" id="Seg_11942" s="T55">The reindeer births, when I was a shepherd, I watched [them] and went around.</ta>
            <ta e="T70" id="Seg_11943" s="T63">When a reindeer gives birth and drops the calf, then reindeer are born in different ways.</ta>
            <ta e="T79" id="Seg_11944" s="T70">Some reindeer lie after being born and fidget with their legs, not being able to stand up.</ta>
            <ta e="T88" id="Seg_11945" s="T79">When looking at the mother and it is fat, then also the calf is very big, when looking at it.</ta>
            <ta e="T93" id="Seg_11946" s="T88">Such [a calf], which lies and fidgets with its legs, has to be turned.</ta>
            <ta e="T104" id="Seg_11947" s="T93">When trying to turn it and thinking "why doesn't it stand up", one has to massage it, one has to massage its belly.</ta>
            <ta e="T110" id="Seg_11948" s="T104">When a reindeer gives birth, then the calf has its own navel.</ta>
            <ta e="T115" id="Seg_11949" s="T110">Something reindeer pull this navel.</ta>
            <ta e="T126" id="Seg_11950" s="T115">If the navel is pulled out there, out of the inside, there emerges a hole, air comes out.</ta>
            <ta e="T135" id="Seg_11951" s="T126">If one ties it up strongly with a rope, this reindeer, this calf, then it stands up.</ta>
            <ta e="T139" id="Seg_11952" s="T135">That means that is pulls out the navel.</ta>
            <ta e="T147" id="Seg_11953" s="T139">That happens when the reindeer, the calf is born for some reason.</ta>
            <ta e="T151" id="Seg_11954" s="T147">Then there is still a reason – weakness.</ta>
            <ta e="T155" id="Seg_11955" s="T151">The reindeer is born, its mother is agile.</ta>
            <ta e="T161" id="Seg_11956" s="T155">When a reindeer calf is born like that, then it is called "dyapchu".</ta>
            <ta e="T165" id="Seg_11957" s="T161">The reindeer calf also doesn't stand up.</ta>
            <ta e="T178" id="Seg_11958" s="T165">Why trying to turn this reindeer calf, it is very weak, it just lies and fidgets with its legs.</ta>
            <ta e="T197" id="Seg_11959" s="T178">When one keeps this calf on the back and strokes its breast, it's the breast of a "dyapchu", when a reindeer calf is born, then the bones are soft.</ta>
            <ta e="T204" id="Seg_11960" s="T197">When one strokes over its breast, then it goes up and down, when it is breathing.</ta>
            <ta e="T220" id="Seg_11961" s="T204">Having caught the mother, one has to make her suckle, after having stroked the breast of the calf, it can't suck.</ta>
            <ta e="T226" id="Seg_11962" s="T220">Then the mother's breast is like with tree resin, thick and short.</ta>
            <ta e="T230" id="Seg_11963" s="T226">This tree resin is pressed out by the human.</ta>
            <ta e="T243" id="Seg_11964" s="T230">After having pressed it out, when liquid milk is coming, then it [the mother] lets it suck, when it is suckled, then this calf survives.</ta>
            <ta e="T245" id="Seg_11965" s="T243">The shepherds nomadize.</ta>
            <ta e="T255" id="Seg_11966" s="T245">When many reindeer calve, then one takes those reindeer away, which never have calved.</ta>
            <ta e="T266" id="Seg_11967" s="T255">The reindeer, which have calved, and the calves, as long as they haven't grown up, are left behind, because they don't run yet.</ta>
            <ta e="T284" id="Seg_11968" s="T266">If one leaves them behind, after more then ten, approx. twenty reindeer have calved, one has to take away the reindeer, which have not yet calved.</ta>
            <ta e="T293" id="Seg_11969" s="T284">When one takes away these reindeer, some shepheds stay with the reindeer, which have calved, and with the calves.</ta>
            <ta e="T300" id="Seg_11970" s="T293">Some shepherds bring the reindeer, which have not calved yet, away.</ta>
            <ta e="T314" id="Seg_11971" s="T300">When they are brought away, one feeds and betrays them, one brings them to a protected place, not showing them, that the herd is going, to the reindeer cows with calves…</ta>
            <ta e="T323" id="Seg_11972" s="T314">Sometimes a reindeer leaves behind its calf in order to not stay behind, and follows the herd.</ta>
            <ta e="T335" id="Seg_11973" s="T323">When one is feeding them and they are ruminating, then one drives the reindeer away, which have not yet calved.</ta>
            <ta e="T342" id="Seg_11974" s="T335">When driving them away, some shepherds go far away.</ta>
            <ta e="T368" id="Seg_11975" s="T342">If they go far away, in the next morning the calves and the reindeer which have not yet calved, the shepherds, to take those to the shepherds, who have gone away, when they are nomadizing, the reindeer don't reach them.</ta>
            <ta e="T374" id="Seg_11976" s="T368">Therefore one stops very closely, after having looked around.</ta>
            <ta e="T383" id="Seg_11977" s="T374">And then these shepherds always drive along protected places.</ta>
            <ta e="T395" id="Seg_11978" s="T383">The route for the calving of the reindeer is chosen along protected places, forested places, mountainous places and places with rivers.</ta>
            <ta e="T399" id="Seg_11979" s="T395">This big snowdrift breaks off.</ta>
            <ta e="T411" id="Seg_11980" s="T399">When it breaks off, in spring, when the reindeer are calving, when the days are getting light, in winter the broken off snowdrift opens.</ta>
            <ta e="T415" id="Seg_11981" s="T411">That means that there is a precipice.</ta>
            <ta e="T433" id="Seg_11982" s="T415">The reindeer get lost without a trace, a shepherd doesn't notice a reindeer which has fallen into the precipice, when he is guarding, that some reindeer have fallen in.</ta>
            <ta e="T435" id="Seg_11983" s="T433">So he is not missing it either.</ta>
            <ta e="T440" id="Seg_11984" s="T435">Like that a reindeer gets lost without a trace.</ta>
            <ta e="T457" id="Seg_11985" s="T440">Therefore the brigade leaders or the shepherds, when the reindeer are calving in spring… when the snow is melting, they have to watch the surrounding well.</ta>
            <ta e="T463" id="Seg_11986" s="T457">[A place], where a snowdrift is broken off, is drifted over in a snowstorm.</ta>
            <ta e="T474" id="Seg_11987" s="T463">After that, being drifted over, the reindeer don't see it and fall [in].</ta>
            <ta e="T482" id="Seg_11988" s="T474">When it falls in, then the precipice is four, five meters deep.</ta>
            <ta e="T484" id="Seg_11989" s="T482">The snow is blown away.</ta>
            <ta e="T493" id="Seg_11990" s="T484">Therefore one doesn't find such and the reindeer gets lost without a trace.</ta>
            <ta e="T505" id="Seg_11991" s="T493">Then at places with rivers, when thawed patches are emerging, then the route is never along rivers.</ta>
            <ta e="T520" id="Seg_11992" s="T505">The space between snow and mountains also opens, so also [there] calves or big reindeer fall [in].</ta>
            <ta e="T532" id="Seg_11993" s="T520">Und these shepherds, when summer is coming in spring, then they can't care for the riding reindeer, they don't remove the horse flies.</ta>
            <ta e="T541" id="Seg_11994" s="T532">When one removes the horse flies in spring, when one removes those horse flies, then these horse flies [i.e. the wounds] will heal.</ta>
            <ta e="T556" id="Seg_11995" s="T541">When the horse fly grows in spring and comes out of the fur itself, when it tears off itself, until it falls down, then it is also bad for the reindeer.</ta>
            <ta e="T565" id="Seg_11996" s="T556">Some horse flies completely, when mounting it, it is bad for the reindeer.</ta>
            <ta e="T571" id="Seg_11997" s="T565">Some shepherds even can't harness reindeer.</ta>
            <ta e="T577" id="Seg_11998" s="T571">The reins break off, then it chafes the reindeer's groin.</ta>
            <ta e="T583" id="Seg_11999" s="T577">If it chafes at the groin, then it becomes a wound in summer.</ta>
            <ta e="T588" id="Seg_12000" s="T583">So the reindeer becomes sick.</ta>
            <ta e="T594" id="Seg_12001" s="T588">Then they are piercing the reindeer's tail.</ta>
            <ta e="T608" id="Seg_12002" s="T594">If the tail is pierced, then in winter in the cold no meat grows at this reindeer [i.e. the wound remains open].</ta>
            <ta e="T613" id="Seg_12003" s="T608">Then in summer, when it is warm, they nomadize.</ta>
            <ta e="T622" id="Seg_12004" s="T613">And nomadizing, nomadizing in the warmth, one goes along places with rivers.</ta>
            <ta e="T628" id="Seg_12005" s="T622">In the high mountains with rivers it is clayey.</ta>
            <ta e="T645" id="Seg_12006" s="T628">Regardless of the heat, a reindeer notices this clay and goes to the mountains, while nomadizing, even when driving them.</ta>
            <ta e="T657" id="Seg_12007" s="T645">And then shepherds, without noticing the mountain, they take their dogs and drive around laughing.</ta>
            <ta e="T668" id="Seg_12008" s="T657">Here more than fifteen or twenty-five reindeer stay.</ta>
            <ta e="T671" id="Seg_12009" s="T668">They don't notice that they have stayed there.</ta>
            <ta e="T676" id="Seg_12010" s="T671">When they come to the station, the next man is guarding.</ta>
            <ta e="T690" id="Seg_12011" s="T676">Then, when nomadizing, the man who just was guarding, his riding reindeer stayed with those reindeer at the mountain, a riding reindeer.</ta>
            <ta e="T693" id="Seg_12012" s="T690">In the next morning they nomadize again.</ta>
            <ta e="T699" id="Seg_12013" s="T693">After nomadizing the next people guard.</ta>
            <ta e="T710" id="Seg_12014" s="T699">There, when the man, who has guarded at the last station, goes out for guarding, he misses his riding reindeer.</ta>
            <ta e="T713" id="Seg_12015" s="T710">In the morning, when he comes, he says: </ta>
            <ta e="T717" id="Seg_12016" s="T713">"Oh, my riding reindeer is not there.</ta>
            <ta e="T731" id="Seg_12017" s="T717">Already there at the station I haven't seen it, at the recent station, at the distant station, when I was guarding, the reindeer which I was riding was there."</ta>
            <ta e="T748" id="Seg_12018" s="T731">"Indeed it seems to have stayed at the station", they say, when they come from the search at this station, the station, from where they have started today.</ta>
            <ta e="T757" id="Seg_12019" s="T748">The reindeer, who stayed behind at the last caravan, they are searching at this station.</ta>
            <ta e="T767" id="Seg_12020" s="T757">There at the distant station the riding reindeer of the guarding shepherd has stayed, on that mountain.</ta>
            <ta e="T771" id="Seg_12021" s="T767">They do remember a single reindeer.</ta>
            <ta e="T776" id="Seg_12022" s="T771">They have stayed more than ten reindeer.</ta>
            <ta e="T785" id="Seg_12023" s="T776">Therefore the reindeer got lost without a trace – like that.</ta>
            <ta e="T789" id="Seg_12024" s="T785">Then the years are different.</ta>
            <ta e="T791" id="Seg_12025" s="T789">It can be hot.</ta>
            <ta e="T799" id="Seg_12026" s="T791">In the heat today's shepherds also can't care about the reindeer.</ta>
            <ta e="T801" id="Seg_12027" s="T799">Do they care anything?!</ta>
            <ta e="T805" id="Seg_12028" s="T801">In the heat the reindeer is following the wind.</ta>
            <ta e="T810" id="Seg_12029" s="T805">A reindeer which follows the wind is called "halgɨndʼɨt".</ta>
            <ta e="T820" id="Seg_12030" s="T810">This reindeer following the wind, until the heat is over, a man is tied to the reindeer.</ta>
            <ta e="T827" id="Seg_12031" s="T820">When guarding reindeer, the human strength is not enough in the heat.</ta>
            <ta e="T830" id="Seg_12032" s="T827">The wind ((…)) they don't do.</ta>
            <ta e="T853" id="Seg_12033" s="T830">When getting them in the morning and giving them to the next shepherd, thinking "we don't make it", one never sends against the wind, for that the houses are leeward.</ta>
            <ta e="T865" id="Seg_12034" s="T853">If the reindeer comes against the wind and doesn't make it, then it runs into its house, then the inhabitants stand up and help. </ta>
            <ta e="T868" id="Seg_12035" s="T865">They one wins against the reindeer.</ta>
            <ta e="T888" id="Seg_12036" s="T868">Then, when the warmth of the spring hasn't come yet, when the warmth is just coming and the reindeer is feeling it, one has to accustom the reindeer to smoke, as long as the big heat hasn't come.</ta>
            <ta e="T896" id="Seg_12037" s="T888">It sees smoke and in any heat the reindeer…</ta>
            <ta e="T903" id="Seg_12038" s="T896">The inhabitants, for that the shepherds will go in the heat, have to say:</ta>
            <ta e="T909" id="Seg_12039" s="T903">"Well, when the heat comes, be careful and sleep.</ta>
            <ta e="T912" id="Seg_12040" s="T909">Make smoke, light a fire."</ta>
            <ta e="T917" id="Seg_12041" s="T912">When it gets warm, we maybe won't make it.</ta>
            <ta e="T922" id="Seg_12042" s="T917">Somehow it is getting warmer.</ta>
            <ta e="T928" id="Seg_12043" s="T922">Like that we were caring about reindeer.</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T5" id="Seg_12044" s="T0">Nun, ich spreche, damit die Rentierhirten es hören können.</ta>
            <ta e="T11" id="Seg_12045" s="T5">Ich bin ein Mensch, der viel mit Rentieren gearbeitet hat, früher.</ta>
            <ta e="T17" id="Seg_12046" s="T11">Und über die Rentiergeburt spreche ich jetzt.</ta>
            <ta e="T28" id="Seg_12047" s="T17">Alles kommt von der Geburt, das Entstehen eines Rentieres, eines Menschens auch.</ta>
            <ta e="T35" id="Seg_12048" s="T28">Und so spreche ich darüber, wenn ein Rentier geboren wird.</ta>
            <ta e="T37" id="Seg_12049" s="T35">Damit die Hirten es hören können.</ta>
            <ta e="T41" id="Seg_12050" s="T37">Ich sage so.</ta>
            <ta e="T45" id="Seg_12051" s="T41">Jetzt kümmert man sich um die Rentiere.</ta>
            <ta e="T55" id="Seg_12052" s="T45">Als ich früher mich gekümmert habe, wenn ein Rentier geboren wurde, gab es viele Gründe.</ta>
            <ta e="T63" id="Seg_12053" s="T55">Die Rentiergeburten, als ich Hirte war, beobachtete ich und ging herum [d.h. ging die Herde ab].</ta>
            <ta e="T70" id="Seg_12054" s="T63">Wenn ein Rentier gebärt und es das Kalb fallen lässt, dann sind Rentiere auf unterschiedliche Art und Weise geboren.</ta>
            <ta e="T79" id="Seg_12055" s="T70">Einige Rentierkälber liegen, nachdem sie geboren wurden, und zappeln mit den Beinen, ohne aufstehen zu können.</ta>
            <ta e="T88" id="Seg_12056" s="T79">Wenn man die Mutter anguckt und die fett ist, dann ist auch das Kalb, wenn man es anguckt, sehr groß.</ta>
            <ta e="T93" id="Seg_12057" s="T88">So eines, das liegt und mit den Beinen zappelt, muss man umdrehen.</ta>
            <ta e="T104" id="Seg_12058" s="T93">Wenn man versucht es umzudrehen und denkt "warum steht es nich auf", dann muss man es massieren, man muss seinen Bauch massieren.</ta>
            <ta e="T110" id="Seg_12059" s="T104">Wenn ein Rentier gebärt, dann hat das Kalb einen eigenen Nabel.</ta>
            <ta e="T115" id="Seg_12060" s="T110">An diesem Nabel ziehen einige Rentiere.</ta>
            <ta e="T126" id="Seg_12061" s="T115">Wenn der Nabel dort herausgezogen wird, aus dem Inneren, dann entsteht sofort ein Loch, dort kommt Luft heraus.</ta>
            <ta e="T135" id="Seg_12062" s="T126">Wenn man es mit einem Band fest anbindet, dieses Rentier, dieses Kalb, dann steht es auf.</ta>
            <ta e="T139" id="Seg_12063" s="T135">Das bedeutet, dass es den Nabel herauszieht.</ta>
            <ta e="T147" id="Seg_12064" s="T139">Das passiert, wenn das Rentier, das Kalb mit einem Grund geboren wird.</ta>
            <ta e="T151" id="Seg_12065" s="T147">Dann gibt es noch einen Grund – Schwäche.</ta>
            <ta e="T155" id="Seg_12066" s="T151">Das Rentier wird geboren, die Mutter ist flink.</ta>
            <ta e="T161" id="Seg_12067" s="T155">Wenn ein Rentierkalb so geboren wird, dann nennt man es "Djaptschu".</ta>
            <ta e="T165" id="Seg_12068" s="T161">Das Rentierkalb steht auch nicht auf.</ta>
            <ta e="T178" id="Seg_12069" s="T165">Wenn man versucht, dieses Rentierkalb umzudrehen, dann ist es sehr schwach, es liegt nur und zappelt mit den Beinen.</ta>
            <ta e="T197" id="Seg_12070" s="T178">Wenn man dieses Kalb auf dem Rücken hält und über die Brust streicht, das ist die Brust eines Djaptschu, wenn ein Rentierkalb geboren wird, dann sind die Knochen weich.</ta>
            <ta e="T204" id="Seg_12071" s="T197">Wenn man dem über die Brust streicht, dann geht sie hoch und runter, wenn es atmet.</ta>
            <ta e="T220" id="Seg_12072" s="T204">Nachdem man die Mutter gefangen hat, muss man sie zum Säugen bringen, nachdem man dem Kalb die Brust gestreichelt hat, kann es nicht saugen.</ta>
            <ta e="T226" id="Seg_12073" s="T220">Da ist die Brust der Mutter wie mit Baumharz, dick und kurz.</ta>
            <ta e="T230" id="Seg_12074" s="T226">Dieses Harz presst der Mensch raus.</ta>
            <ta e="T243" id="Seg_12075" s="T230">Nachdem man das rauspresst hat, wenn dann flüssige Milch kommt, dann lässt sie es saugen, wenn es gesäugt wird, dann überlebt dieses Kalb.</ta>
            <ta e="T245" id="Seg_12076" s="T243">Die Hirten nomadisieren.</ta>
            <ta e="T255" id="Seg_12077" s="T245">Wenn viele Rentiere kalben, dann nimmt man die Rentiere, die noch nie gekalbt haben, weg.</ta>
            <ta e="T266" id="Seg_12078" s="T255">Die gekalbt habenden Rentiere und die Kälber, solange sie noch nicht gewachsen sind, lässt man zurück, weil sie noch nicht laufen.</ta>
            <ta e="T284" id="Seg_12079" s="T266">Wenn man die zurücklässt, nachdem mehr als zehn, ungefähr zwanzig Rentiere gekalbt haben, dann muss man die Rentiere, die noch nicht gekalbt haben, wegnehmen.</ta>
            <ta e="T293" id="Seg_12080" s="T284">Wenn man diese Rentiere wegnimmt, dann bleiben einige Hirten bei den Rentieren, die gekalbt haben, und bei den Kälbern.</ta>
            <ta e="T300" id="Seg_12081" s="T293">Einige Hirten bringen die Rentiere, die noch nicht gekalbt haben, weg.</ta>
            <ta e="T314" id="Seg_12082" s="T300">Wenn die weggebracht werden, gibt man Futter und betrügt sie, man bringt sie an einen geschützten Ort, ohne zu zeigen, dass die Herde geht, den Rentierkühen mit Kälbern… </ta>
            <ta e="T323" id="Seg_12083" s="T314">Manchmal lässt ein Rentier, um nicht zurückzubleiben, sein Kalb zurück und folgt der Herde.</ta>
            <ta e="T335" id="Seg_12084" s="T323">Wenn man die füttert und die wiederkäuen, dann führt man die nicht gekalbt habenden Rentiere weg.</ta>
            <ta e="T342" id="Seg_12085" s="T335">Wenn man die wegführt, gehen einige Hirten sehr weit.</ta>
            <ta e="T368" id="Seg_12086" s="T342">Wenn sie weit gehen, am nächsten Morgen die Kälber und die noch nicht gekalbt habenden Kühe, die Hirten, um diese zu den Hirten, die vorausgegangen sind, mitzunehmen, wenn sie nomadisieren, kommen die Rentiere nicht dort an.</ta>
            <ta e="T374" id="Seg_12087" s="T368">Deshalb hält man nah an, nachdem man sich umgesehen hat.</ta>
            <ta e="T383" id="Seg_12088" s="T374">Dann aber fahren diese Hirten immer an geschützten Plätzen entlang.</ta>
            <ta e="T395" id="Seg_12089" s="T383">Die Strecke für das Kalben der Rentiere guckt man sich entlang geschützter Stellen, waldiger Stellen, bergiger Stellen, Stellen mit Flüssen aus.</ta>
            <ta e="T399" id="Seg_12090" s="T395">Diese große Schneewehe reißt ab.</ta>
            <ta e="T411" id="Seg_12091" s="T399">Wenn sie abreißt, im Frühling, wenn die Rentiere kalben, wenn die Tage hell werden, im Winter öffnet sich die abgerissene Schneewehe.</ta>
            <ta e="T415" id="Seg_12092" s="T411">Das bedeutet, dass dort an Abgrund ist.</ta>
            <ta e="T433" id="Seg_12093" s="T415">Das Rentier geht spurlos verschwunden, ein in den Abgrund gefallenes Rentier bemerkt der Hirte nicht, wenn er Wache hält, dass einige Rentiere hineingefallen sind.</ta>
            <ta e="T435" id="Seg_12094" s="T433">So vermisst er es auch nicht.</ta>
            <ta e="T440" id="Seg_12095" s="T435">So geht ein Rentier spurlos verloren.</ta>
            <ta e="T457" id="Seg_12096" s="T440">Daher die Brigadeleiter oder die Hirten, wenn im Frühjahr die Rentiere kalben… wenn der Schnee schmilzt, müssen sie gut auf die Umgebung schauen.</ta>
            <ta e="T463" id="Seg_12097" s="T457">[Ein Platz], wo eine Schneewehe zerrissen wurde, wird, wenn Schneesturm ist, zugeweht.</ta>
            <ta e="T474" id="Seg_12098" s="T463">Danach, nachdem es zugeweht ist, sehen die Rentiere es nicht und fallen [hinein].</ta>
            <ta e="T482" id="Seg_12099" s="T474">Wenn es hineinfällt, dann ist der Abgrund vier, fünf Meter tief.</ta>
            <ta e="T484" id="Seg_12100" s="T482">Der Schnee bricht ab.</ta>
            <ta e="T493" id="Seg_12101" s="T484">Deshalb findet man so eins nicht und das Rentier geht spurlos verloren.</ta>
            <ta e="T505" id="Seg_12102" s="T493">Dann an Stellen mit Flüssen, wenn Löcher in der Schneedecke entstehen, geht die Strecke nie an Flüssen entlang.</ta>
            <ta e="T520" id="Seg_12103" s="T505">Der Zwischenraum von Schnee und Bergen öffnet sich auch, so fallen [dort] auch Kälber oder große Rentiere.</ta>
            <ta e="T532" id="Seg_12104" s="T520">Und diese Hirten, wenn der Sommer kommt im Frühjahr, dann können sie sich nicht um die Reitrentiere kümmern, sie entfernen nicht die Pferdebremsen.</ta>
            <ta e="T541" id="Seg_12105" s="T532">Wenn man im Frühling die Pferdebremsen entfernt, wenn man diese Pferdebremsen entfernt, dann werden diese Pferdebremsen [d.i. die Wunden] gesund.</ta>
            <ta e="T556" id="Seg_12106" s="T541">Wenn die Pferdebremse im Frühling wächst und selbst hinausgeht aus dem Inneren des Falls, wenn sie sich selbst abtrennt, bis sie fällt, dann ist es auch schlecht für das Rentier.</ta>
            <ta e="T565" id="Seg_12107" s="T556">Einige Pferdebremsen vollständig, wenn man es besteigt, dann ist das schlecht für das Rentier.</ta>
            <ta e="T571" id="Seg_12108" s="T565">Einige Hirten können Rentiere nicht einmal anspannen.</ta>
            <ta e="T577" id="Seg_12109" s="T571">Das Geschirr reißt ab, dann scheuert es an der Leiste des Rentiers.</ta>
            <ta e="T583" id="Seg_12110" s="T577">Wenn es an der Leiste scheuert, dann wird das im Sommer eine Wunde.</ta>
            <ta e="T588" id="Seg_12111" s="T583">So wird das Rentier krank.</ta>
            <ta e="T594" id="Seg_12112" s="T588">Dann durchstechen sie den Schwanz des Rentiers.</ta>
            <ta e="T608" id="Seg_12113" s="T594">Wenn der Schwanz durchstochen wird, dann im Winter in der Kälte wächst bei dem Rentier kein Fleisch mehr [d.i. die Wunde bleibt offen].</ta>
            <ta e="T613" id="Seg_12114" s="T608">Dann im Sommer, wenn es warm ist, nomadisiert man.</ta>
            <ta e="T622" id="Seg_12115" s="T613">Und wenn man nomadisiert, wenn man in der Wärme nomadisiert, dann geht man an Stellen mit Flüssen entlang.</ta>
            <ta e="T628" id="Seg_12116" s="T622">Bei den hohen Bergen mit Flüssen ist es lehmig.</ta>
            <ta e="T645" id="Seg_12117" s="T628">Egal in was für einer Hitze, ein Rentier erkennt diesen Lehm und geht in die Berge, während des Nomadisierens, sogar während man sie treibt.</ta>
            <ta e="T657" id="Seg_12118" s="T645">Und die Hirten, ohne den Berg zu merken, nehmen ihre Hunde und fahren lachend vorbei.</ta>
            <ta e="T668" id="Seg_12119" s="T657">Hier bleiben über fünfzehn oder über fünfundzwanzig Rentiere.</ta>
            <ta e="T671" id="Seg_12120" s="T668">Sie bemerken nicht, dass sie da geblieben sind.</ta>
            <ta e="T676" id="Seg_12121" s="T671">Wenn sie an die Station kommen, dann passt der nächste Mensch auf.</ta>
            <ta e="T690" id="Seg_12122" s="T676">Dann, wenn sie nomadisieren, der Mensch, der gerade gehütet hat, sein Reitrentier ist bei jenen Rentieren am Berg geblieben, ein Reitrentier.</ta>
            <ta e="T693" id="Seg_12123" s="T690">Am nächsten Morgen nomadisiert man wieder.</ta>
            <ta e="T699" id="Seg_12124" s="T693">Nach dem Nomadisieren passen die nächsten Leute auf.</ta>
            <ta e="T710" id="Seg_12125" s="T699">Da, wenn der auf der letzten Station gehütet habende Mensch zum Hüten hinausgeht, vermisst er sein Reitrentier.</ta>
            <ta e="T713" id="Seg_12126" s="T710">Am Morgen, wenn er kommt, sagt er: </ta>
            <ta e="T717" id="Seg_12127" s="T713">"Oh, mein Reitrentier ist nicht da.</ta>
            <ta e="T731" id="Seg_12128" s="T717">Schon dort an der Station habe ich es nicht gesehen, an der gerade gewesenen Station, an der entfernten Station, als ich gehütet habe, war das Rentier, auf dem ich ritt, da."</ta>
            <ta e="T748" id="Seg_12129" s="T731">"Tatsächlich, da an der Station ist es wohl geblieben", sagt man, als man von der Suche an dieser Station kommt, die Station, wo man heute losgegangen ist.</ta>
            <ta e="T757" id="Seg_12130" s="T748">Die Rentiere, die beim letzten Zug zurückgeblieben sind, suchen sie auf der jetztigen Station.</ta>
            <ta e="T767" id="Seg_12131" s="T757">Dort an der entfernten Station ist das Reitrentier des aufpassenden Hirten geblieben, auf jenem Berg.</ta>
            <ta e="T771" id="Seg_12132" s="T767">Sie erinnern sich an ein einziges Rentier.</ta>
            <ta e="T776" id="Seg_12133" s="T771">Dort sind mehr als zehn Rentiere geblieben.</ta>
            <ta e="T785" id="Seg_12134" s="T776">Deshalb sind die Rentiere spurlos verschwunden – so.</ta>
            <ta e="T789" id="Seg_12135" s="T785">Dann sind die Jahre unterschiedlich.</ta>
            <ta e="T791" id="Seg_12136" s="T789">Es kann heiß sein.</ta>
            <ta e="T799" id="Seg_12137" s="T791">In der Hitze können die heutigen Hirten wieder nicht auf die Rentiere aufpassen.</ta>
            <ta e="T801" id="Seg_12138" s="T799">Kümmern sie sich überhaupt um etwas?!</ta>
            <ta e="T805" id="Seg_12139" s="T801">In der Hitze folgt das Rentier dem Wind.</ta>
            <ta e="T810" id="Seg_12140" s="T805">Ein dem Wind folgendes Rentier heißt "halgɨndʼɨt".</ta>
            <ta e="T820" id="Seg_12141" s="T810">Dieses dem Wind folgende Rentier, bis die Hitze vorbei ist, bindet man einen Menschen ans Rentier.</ta>
            <ta e="T827" id="Seg_12142" s="T820">Wenn man Rentiere hütet, dann reichen die menschlichen Kräfte nicht in der Hitze.</ta>
            <ta e="T830" id="Seg_12143" s="T827">Den Wind ((…)) macht man nicht.</ta>
            <ta e="T853" id="Seg_12144" s="T830">Wenn man sie am Morgen holt und dem nächsten Hirten übergibt: "Wir schaffen es nicht", denkend, schickt man nie gegen dem Wind, damit die Häuser auf der windabgewandten Seite sind. </ta>
            <ta e="T865" id="Seg_12145" s="T853">Wenn das Rentier gegen den Wind kommt und es nicht schafft, dann stößt es an sein Haus, dann stehen die Bewohner auf und helfen.</ta>
            <ta e="T868" id="Seg_12146" s="T865">Dann besiegt man die Rentiere.</ta>
            <ta e="T888" id="Seg_12147" s="T868">Danach, wenn die Frühlingswärme noch nicht gekommen ist, gerade wenn die Wärme kommt und das Rentier es fühlt, dann muss man die Rentiere an Rauch gewöhnen, solange die große Hitze noch nicht gekommen ist.</ta>
            <ta e="T896" id="Seg_12148" s="T888">Es sieht Rauch und egal in welche Wärme das Rentier… </ta>
            <ta e="T903" id="Seg_12149" s="T896">Die Hausangehörigen, damit die Hirten in die Hitze gehen, müssen sagen: </ta>
            <ta e="T909" id="Seg_12150" s="T903">"Nun, wenn die Hitze kommt, dann seid wachsam und schlaft.</ta>
            <ta e="T912" id="Seg_12151" s="T909">Macht Rauch, zündet ein Feuer an."</ta>
            <ta e="T917" id="Seg_12152" s="T912">Wenn es warm wird, schaffen wir es vielleicht nicht.</ta>
            <ta e="T922" id="Seg_12153" s="T917">Irgendwie wird es wärmer.</ta>
            <ta e="T928" id="Seg_12154" s="T922">So haben wir auf Rentiere aufgepasst.</ta>
         </annotation>
         <annotation name="fr" tierref="fr" />
         <annotation name="ltr" tierref="ltr">
            <ta e="T5" id="Seg_12155" s="T0">Вот я оленеводам дать им послушать говорю.</ta>
            <ta e="T11" id="Seg_12156" s="T5">Я много раз проработавший человек в олене (оленеводстве) раньше.</ta>
            <ta e="T17" id="Seg_12157" s="T11">Вот об оленьем отёле расскажу сейчас.</ta>
            <ta e="T28" id="Seg_12158" s="T17">Любое от рождения идёт, конечно, оленя хоть появление или человека.</ta>
            <ta e="T35" id="Seg_12159" s="T28">Таким образом я говорю, вот так олень рождается. </ta>
            <ta e="T37" id="Seg_12160" s="T35">Пастухам чтобы дать послушать.</ta>
            <ta e="T41" id="Seg_12161" s="T37">Я говорю вот так.</ta>
            <ta e="T45" id="Seg_12162" s="T41">Сейчас вот за оленами ухаживают.</ta>
            <ta e="T55" id="Seg_12163" s="T45">Я раньше когда ухаживал, олень когда рождался, много причины бывает.</ta>
            <ta e="T63" id="Seg_12164" s="T55">Оленя отёл, пастухом когда был, ожидая, обходил тогда.</ta>
            <ta e="T70" id="Seg_12165" s="T63">Олень отелившись, дитёныша когда скидывает, по-разному телится олень.</ta>
            <ta e="T79" id="Seg_12166" s="T70">Некоторого оленя дитёныш после отёла брыкается, бывает, лежит, от того что не может встать.</ta>
            <ta e="T88" id="Seg_12167" s="T79">На мать если помотреть жирная такая, на дитёныша посмотришь – большой такой.</ta>
            <ta e="T93" id="Seg_12168" s="T88">Этого брыкающегося лёжа перевернуть надо.</ta>
            <ta e="T104" id="Seg_12169" s="T93">Перевернув попробовав – почему не встаёт, подумавши, тут помять (помассировать), живот помять.</ta>
            <ta e="T110" id="Seg_12170" s="T104">Олень если рождается, у оленёнка пупок бывает.</ta>
            <ta e="T115" id="Seg_12171" s="T110">Этот пупок некоторый олень вытягивает.</ta>
            <ta e="T126" id="Seg_12172" s="T115">Этот пупок если протягивается отсюда, из нутри, насквозь дыра получается, оттуда дыхание выходит.</ta>
            <ta e="T135" id="Seg_12173" s="T126">Бичёвкой сильно если завязать, этот олень, этот оленёнок вскакивает.</ta>
            <ta e="T139" id="Seg_12174" s="T135">Это называется "пупок вытягивает".</ta>
            <ta e="T147" id="Seg_12175" s="T139">Это получается у оленя, у оленёнка причина при рождении.</ta>
            <ta e="T151" id="Seg_12176" s="T147">Потом одна причина есть – слабый.</ta>
            <ta e="T155" id="Seg_12177" s="T151">Олень рожает, мать его.</ta>
            <ta e="T161" id="Seg_12178" s="T155">Оленёнок если так рождается Дьапчу именуется.</ta>
            <ta e="T165" id="Seg_12179" s="T161">Тоже оленёнок не встаёт.</ta>
            <ta e="T178" id="Seg_12180" s="T165">Этого оленёнка перевернуть если попробовать, слабый очень бывает, этот оленёнок пинается только лежит.</ta>
            <ta e="T197" id="Seg_12181" s="T178">Этого оленёнка перевернули потереть (помассажировать), тогда у этой Дьапчу всегда грудь хращевидная.</ta>
            <ta e="T204" id="Seg_12182" s="T197">Этого если грудь помассажировать, вставая несколько раз, падает, когда дышит.</ta>
            <ta e="T220" id="Seg_12183" s="T204">Здесь маму придержав, маму его чтобы пососать надо, этот оленёнок грудь его, когда помассажировали, никак сосать не может.</ta>
            <ta e="T226" id="Seg_12184" s="T220">Тут мамина грудь будто с древесной смолой бывает, большеватая и грубая.</ta>
            <ta e="T230" id="Seg_12185" s="T226">Эту смоляную жидкость выдавливает человек.</ta>
            <ta e="T243" id="Seg_12186" s="T230">После как выдавит и жидкое вымя (молоко) когда подойдёт, тогда даёт пососать, тогда после как покормит, этот оленёнок остает жить.</ta>
            <ta e="T245" id="Seg_12187" s="T243">Пастухи аргишат.</ta>
            <ta e="T255" id="Seg_12188" s="T245">Если много оленей отелятся, ни разу не рожавших оленей вперёд ведут.</ta>
            <ta e="T266" id="Seg_12189" s="T255">Отеливших оленей, оленёнок, пока не подрастёт, из-за того, что не ходит, позади оставляют.</ta>
            <ta e="T284" id="Seg_12190" s="T266">Вот при оставлении, эти больше десяти, двадцати оленей родивших, тогда не рожавших оленей вперёд провести надо.</ta>
            <ta e="T293" id="Seg_12191" s="T284">Оленей чтобы провести некоторые пастухи с новорожденными оленятами остаётся.</ta>
            <ta e="T300" id="Seg_12192" s="T293">Некоторые пастухи, не рожавших оленей уводит.</ta>
            <ta e="T314" id="Seg_12193" s="T300">Постепенно дав подкормиться обманным образом уводят, в укромное место заводя, как стадо уходит не показывая, самок у с дитёнышем… </ta>
            <ta e="T323" id="Seg_12194" s="T314">в некоторых случаях олень, чтобы не отстать, телят оставляет следуя, к стаду следуя…</ta>
            <ta e="T335" id="Seg_12195" s="T323">его подкармливая и насытился ли ожидая, не отелившнх оленей вперёд перегоняет. </ta>
            <ta e="T342" id="Seg_12196" s="T335">Когда аргишат, некоторые пастухи очень далеко уходят.</ta>
            <ta e="T368" id="Seg_12197" s="T342">Тут если далеко уйдут, эти теляты на завтра у неотеливших оленей отставшего, пастухи ту, отелившую важенку к пастухам, вперёд ушедим пастухам, чтобы увести при аргише оленей не хватает вот.</ta>
            <ta e="T374" id="Seg_12198" s="T368">Поэтому близко остановиться, осмотревшись.</ta>
            <ta e="T383" id="Seg_12199" s="T374">И вообще эти пастухи, как правило, по укромным местам (незаметно/не беспокоят стадо) приезжают.</ta>
            <ta e="T395" id="Seg_12200" s="T383">Маршрут высматривают для оленьего отёла по укромной местности, по ( ? ) местности, по гористым местам, по речным местам.</ta>
            <ta e="T399" id="Seg_12201" s="T395">Этот большой сугроб обрывается.</ta>
            <ta e="T411" id="Seg_12202" s="T399">Это если обрывается, весной, оленя отёла время, солнце когда ярче начинает светить, зимой обрывавшийся сугроб расходится (раскрывается).</ta>
            <ta e="T415" id="Seg_12203" s="T411">То называется обрыв бывает.</ta>
            <ta e="T433" id="Seg_12204" s="T415">Олени без вести пропавший, вот внутрь обрыва упавшего оленя, тут пастух, охраняя, не знает сколько оленей провалилось.</ta>
            <ta e="T435" id="Seg_12205" s="T433">Так и не теряет.</ta>
            <ta e="T440" id="Seg_12206" s="T435">Оленя без вести пропажа вот.</ta>
            <ta e="T457" id="Seg_12207" s="T440">Поэтому бригадиры хоть, пастухи хоть, весной в оленя отёл время… снег когда тает, хорошо местность осмотреть должны.</ta>
            <ta e="T463" id="Seg_12208" s="T457">Место, где обрыв, в пургу заносит снегом.</ta>
            <ta e="T474" id="Seg_12209" s="T463">После этого, как заметёт его олень не видит, так и проваливается.</ta>
            <ta e="T482" id="Seg_12210" s="T474">И если провалится где-то пять-четыре метра глубиной бывает этот обрыв.</ta>
            <ta e="T484" id="Seg_12211" s="T482">Снег оборвался.</ta>
            <ta e="T493" id="Seg_12212" s="T484">Поэтому не найдя такого, олень без вести пропадает вот так.</ta>
            <ta e="T505" id="Seg_12213" s="T493">Потом в речных местах вот, как на земле проталинки появятся, всегда с речкой местности идёт маршрут этот.</ta>
            <ta e="T520" id="Seg_12214" s="T505">Между снегом и горным местом расходится (раскрываетсяя) тоже, там тоже проваливается или телёнок, или взрослый олень.</ta>
            <ta e="T532" id="Seg_12215" s="T520">Ещё эти пастухи … лето их когда приближается к весенней поре, за верховыми оленями не могут ухаживать, личинки овода на шкуре не очищают. </ta>
            <ta e="T541" id="Seg_12216" s="T532">Весной, когда лечинки очитсят, те личинки очистят, эти лечинки оживают (как рана оживает, т.е засыхают).</ta>
            <ta e="T556" id="Seg_12217" s="T541">Сама лечинка весной увеличившись когда выйдет из внутренней стороны шкуры, сама отделившись пока упадёт, и оленю плохо.</ta>
            <ta e="T565" id="Seg_12218" s="T556">Некоторые лечинки пока внутри (в закрытом виде), человек, садясь верхом, оленя погибель эта.</ta>
            <ta e="T571" id="Seg_12219" s="T565">Иной пастух оленя даже запрягать не умеет.</ta>
            <ta e="T577" id="Seg_12220" s="T571">Поводок оборвался, тогда оленя пах натирают.</ta>
            <ta e="T583" id="Seg_12221" s="T577">Пах если натрёт, вот летом болячка образовывается.</ta>
            <ta e="T588" id="Seg_12222" s="T583">Так олень заболевает.</ta>
            <ta e="T594" id="Seg_12223" s="T588">Потом хвост оленя насквозь прокалывают.</ta>
            <ta e="T608" id="Seg_12224" s="T594">Тут хвост если наскозь проколят, в зимнюю стужу у этого оленя никогда мясо не зарастает.</ta>
            <ta e="T613" id="Seg_12225" s="T608">Затем летом, в жаркое время аргишат.</ta>
            <ta e="T622" id="Seg_12226" s="T613">И вот когда аргишат, в жару если аргишат, аргишая по речным местам проходят.</ta>
            <ta e="T628" id="Seg_12227" s="T622">Здесь у речистых высоких гор глина бывает.</ta>
            <ta e="T645" id="Seg_12228" s="T628">Хоть какая бы жара не была эту глину как почувствует, олень вот в гору уходит, во время аргиша даже и во время загона стада даже.</ta>
            <ta e="T657" id="Seg_12229" s="T645">Это пастухи, эту гору не замечая, погоняя собаками, так и смеясь проходят.</ta>
            <ta e="T668" id="Seg_12230" s="T657">Здесь больше пятнадцати или больше дватцати пяти оленей остаётся.</ta>
            <ta e="T671" id="Seg_12231" s="T668">Здесь отставшего не замечают.</ta>
            <ta e="T676" id="Seg_12232" s="T671">На стоянку если прибудут, следующий человек сторожит.</ta>
            <ta e="T690" id="Seg_12233" s="T676">Тогда, когда аргишили, охранявший пастух, вот его верховой олень с теми оленями, в горе остался один верховой олень.</ta>
            <ta e="T693" id="Seg_12234" s="T690">На утро аргишат опять.</ta>
            <ta e="T699" id="Seg_12235" s="T693">После аргиша следующие люди сторожат.</ta>
            <ta e="T710" id="Seg_12236" s="T699">Тогда на прошлом месте стороживший по очереди человек, сторожить выходя, верхового оленя только тогда теряет.</ta>
            <ta e="T713" id="Seg_12237" s="T710">Утром, приходя, говорит: </ta>
            <ta e="T717" id="Seg_12238" s="T713">"Ой, моего верхового оленя нет.</ta>
            <ta e="T731" id="Seg_12239" s="T717">Там даже на прежнем месте не видел, на том аргишившем месте моём, и на дальнем стойбище, когда сторожил, ездил верхом на олене этом.</ta>
            <ta e="T748" id="Seg_12240" s="T731">"И вправду, там на том месте остался наверно" – говоря, этого оленя на этом (ближнем) месте искать возвращаются, сегодня аргишили с которого места.</ta>
            <ta e="T757" id="Seg_12241" s="T748">Этих оленей, на прошлом аргише оставшихся оленей, на ближайшем стойбище ищут. </ta>
            <ta e="T767" id="Seg_12242" s="T757">Там на прошлом (дальнем) месте пастуха верховой олень там остался, на той горе.</ta>
            <ta e="T771" id="Seg_12243" s="T767">То единственного оленя вспоминают.</ta>
            <ta e="T776" id="Seg_12244" s="T771">Там осталось десяти больше оленей.</ta>
            <ta e="T785" id="Seg_12245" s="T776">Поэтому без вести пропавшего оленя вестей нет – вот.</ta>
            <ta e="T789" id="Seg_12246" s="T785">Затем год разным бывает.</ta>
            <ta e="T791" id="Seg_12247" s="T789">С жарой бывает.</ta>
            <ta e="T799" id="Seg_12248" s="T791">В эту жару опять нынешние пастухи оленя умело не ухаживают.</ta>
            <ta e="T801" id="Seg_12249" s="T799">Не ухаживают это что!?</ta>
            <ta e="T805" id="Seg_12250" s="T801">В жару олень следует за дуновением ветра.</ta>
            <ta e="T810" id="Seg_12251" s="T805">Следующий за ветром олень – это ветер догоняющий.</ta>
            <ta e="T820" id="Seg_12252" s="T810">Этого ветер догоняющего оленя, пока жара не спадёт, человек за оленя привязывает.</ta>
            <ta e="T827" id="Seg_12253" s="T820">Оленя когда сторожат, у человека сил не хватает, в жару.</ta>
            <ta e="T830" id="Seg_12254" s="T827">(?).. не делают.</ta>
            <ta e="T853" id="Seg_12255" s="T830">Утром когда пригоняют, следующему пастуху когда передают: "Мы не справимся", – думая, всегда по ветру направляют, дома с подветрянной страны чтобы были.</ta>
            <ta e="T865" id="Seg_12256" s="T853">Олень тогда, если не справившись, против ветра если придёт, в дом свой если упрётся, домашние его, поднимаясь, тут помогают.</ta>
            <ta e="T868" id="Seg_12257" s="T865">Тогда справляют оленей.</ta>
            <ta e="T888" id="Seg_12258" s="T868">После этого они, с весны жара пока не спадёт, только жара когда спадает, оленю когда жарко, к дыму оленя приучить должны, большая жара пока не настала.</ta>
            <ta e="T896" id="Seg_12259" s="T888">Дым как увидит, в любую жару олень.. </ta>
            <ta e="T903" id="Seg_12260" s="T896">Домашние в жару, когда пастухи сторожить уходят, должны сказать: </ta>
            <ta e="T909" id="Seg_12261" s="T903">"Смотрите, жара когда будет, беспокоясь спите.</ta>
            <ta e="T912" id="Seg_12262" s="T909">Пустите дым, костёр разожгите."</ta>
            <ta e="T917" id="Seg_12263" s="T912">Жара когда будет, возможно, не справимся.</ta>
            <ta e="T922" id="Seg_12264" s="T917">Как-то жарче становится.</ta>
            <ta e="T928" id="Seg_12265" s="T922">Так мы ухаживали за оленями.</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T55" id="Seg_12266" s="T45">[DCh]: Last part of the sentence not clear, even for native speakers: There were reasons for what?</ta>
            <ta e="T147" id="Seg_12267" s="T139">[DCh]: Absolutely unclear, what is meant with 'reason' here.</ta>
            <ta e="T255" id="Seg_12268" s="T245">[DCh]: The calving reindeer cows are separated from the herd, so that they can calve at rest.</ta>
            <ta e="T368" id="Seg_12269" s="T342">[DCh]: Sense not really clear; according to native speaker: If the first sheperds go far away, then the reindeers who stayed can not reach them.</ta>
            <ta e="T395" id="Seg_12270" s="T383">[DCh]: "ohuːr" is not found in dictionaries, neither known by native speakers; so the interpretation as a variant of ojuːr is far from certain.</ta>
            <ta e="T565" id="Seg_12271" s="T556">[DCh] "ölör ün-" is lexicalised and means 'being bad, hurt, being sick'.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T930" />
            <conversion-tli id="T929" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
