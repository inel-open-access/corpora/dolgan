<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDIDE266E15C-8DA0-C57A-EA98-EEE6BB6B2BEA">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>MiXS_1967_SoldierInSecondWorldWar_nar</transcription-name>
         <referenced-file url="MiXS_1967_SoldierInSecondWorldWar_nar.wav" />
         <referenced-file url="MiXS_1967_SoldierInSecondWorldWar_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\MiXS_1967_SoldierInSecondWorldWar_nar\MiXS_1967_SoldierInSecondWorldWar_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">1120</ud-information>
            <ud-information attribute-name="# HIAT:w">951</ud-information>
            <ud-information attribute-name="# e">945</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">1</ud-information>
            <ud-information attribute-name="# HIAT:u">82</ud-information>
            <ud-information attribute-name="# sc">2</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="MiXS">
            <abbreviation>MiXS</abbreviation>
            <sex value="m" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="2.035" type="appl" />
         <tli id="T1" time="2.558" type="appl" />
         <tli id="T2" time="3.081" type="appl" />
         <tli id="T3" time="3.604" type="appl" />
         <tli id="T4" time="4.127" type="appl" />
         <tli id="T5" time="4.65" type="appl" />
         <tli id="T6" time="5.173" type="appl" />
         <tli id="T7" time="5.696" type="appl" />
         <tli id="T8" time="6.219" type="appl" />
         <tli id="T9" time="6.742" type="appl" />
         <tli id="T10" time="7.265" type="appl" />
         <tli id="T11" time="8.16133166357812" />
         <tli id="T12" time="8.546" type="appl" />
         <tli id="T13" time="9.304" type="appl" />
         <tli id="T14" time="10.061" type="appl" />
         <tli id="T15" time="10.819" type="appl" />
         <tli id="T16" time="12.766613643310917" />
         <tli id="T17" time="12.923409095540611" type="intp" />
         <tli id="T18" time="13.080204547770306" type="intp" />
         <tli id="T19" time="13.237" type="appl" />
         <tli id="T20" time="13.79" type="appl" />
         <tli id="T21" time="14.343" type="appl" />
         <tli id="T22" time="14.897" type="appl" />
         <tli id="T23" time="15.45" type="appl" />
         <tli id="T24" time="16.003" type="appl" />
         <tli id="T25" time="16.556" type="appl" />
         <tli id="T26" time="17.11" type="appl" />
         <tli id="T27" time="17.663" type="appl" />
         <tli id="T28" time="18.216" type="appl" />
         <tli id="T29" time="18.769" type="appl" />
         <tli id="T30" time="19.323" type="appl" />
         <tli id="T31" time="20.853246723904203" />
         <tli id="T32" time="21.008623361952104" type="intp" />
         <tli id="T33" time="21.164" type="appl" />
         <tli id="T34" time="21.808" type="appl" />
         <tli id="T35" time="22.452" type="appl" />
         <tli id="T36" time="23.097" type="appl" />
         <tli id="T37" time="23.741" type="appl" />
         <tli id="T38" time="24.385" type="appl" />
         <tli id="T39" time="25.029" type="appl" />
         <tli id="T40" time="25.673" type="appl" />
         <tli id="T41" time="26.317" type="appl" />
         <tli id="T42" time="26.961" type="appl" />
         <tli id="T43" time="27.605" type="appl" />
         <tli id="T44" time="28.25" type="appl" />
         <tli id="T45" time="28.894" type="appl" />
         <tli id="T46" time="29.538" type="appl" />
         <tli id="T47" time="30.182" type="appl" />
         <tli id="T48" time="30.826" type="appl" />
         <tli id="T49" time="31.597" type="appl" />
         <tli id="T50" time="32.368" type="appl" />
         <tli id="T51" time="33.139" type="appl" />
         <tli id="T52" time="33.91" type="appl" />
         <tli id="T53" time="34.681" type="appl" />
         <tli id="T54" time="35.452" type="appl" />
         <tli id="T55" time="36.223" type="appl" />
         <tli id="T56" time="36.994" type="appl" />
         <tli id="T57" time="37.765" type="appl" />
         <tli id="T58" time="38.5" type="appl" />
         <tli id="T59" time="39.234" type="appl" />
         <tli id="T60" time="39.969" type="appl" />
         <tli id="T61" time="40.704" type="appl" />
         <tli id="T62" time="41.439" type="appl" />
         <tli id="T63" time="42.174" type="appl" />
         <tli id="T64" time="42.908" type="appl" />
         <tli id="T65" time="43.643" type="appl" />
         <tli id="T66" time="44.378" type="appl" />
         <tli id="T67" time="45.112" type="appl" />
         <tli id="T68" time="45.847" type="appl" />
         <tli id="T69" time="46.582" type="appl" />
         <tli id="T70" time="47.317" type="appl" />
         <tli id="T71" time="48.052" type="appl" />
         <tli id="T72" time="48.786" type="appl" />
         <tli id="T73" time="49.521" type="appl" />
         <tli id="T74" time="50.256" type="appl" />
         <tli id="T75" time="50.99" type="appl" />
         <tli id="T76" time="51.725" type="appl" />
         <tli id="T77" time="52.46" type="appl" />
         <tli id="T78" time="53.195" type="appl" />
         <tli id="T79" time="53.93" type="appl" />
         <tli id="T80" time="54.664" type="appl" />
         <tli id="T81" time="55.993100778155814" />
         <tli id="T82" time="56.091" type="appl" />
         <tli id="T83" time="56.782" type="appl" />
         <tli id="T84" time="57.474" type="appl" />
         <tli id="T85" time="58.166" type="appl" />
         <tli id="T86" time="58.858" type="appl" />
         <tli id="T87" time="59.55" type="appl" />
         <tli id="T88" time="60.241" type="appl" />
         <tli id="T89" time="61.54641104702162" />
         <tli id="T90" time="61.8" type="appl" />
         <tli id="T91" time="62.667" type="appl" />
         <tli id="T92" time="63.534" type="appl" />
         <tli id="T93" time="64.4" type="appl" />
         <tli id="T94" time="65.267" type="appl" />
         <tli id="T95" time="66.134" type="appl" />
         <tli id="T96" time="67.20766357590601" />
         <tli id="T97" time="67.767" type="appl" />
         <tli id="T98" time="68.533" type="appl" />
         <tli id="T99" time="69.298" type="appl" />
         <tli id="T100" time="70.064" type="appl" />
         <tli id="T101" time="70.83" type="appl" />
         <tli id="T102" time="71.66933124325548" />
         <tli id="T103" time="72.207" type="appl" />
         <tli id="T104" time="72.818" type="appl" />
         <tli id="T105" time="73.43" type="appl" />
         <tli id="T106" time="74.041" type="appl" />
         <tli id="T107" time="74.652" type="appl" />
         <tli id="T108" time="75.263" type="appl" />
         <tli id="T109" time="75.874" type="appl" />
         <tli id="T110" time="76.486" type="appl" />
         <tli id="T111" time="77.097" type="appl" />
         <tli id="T112" time="77.708" type="appl" />
         <tli id="T113" time="78.319" type="appl" />
         <tli id="T114" time="78.93" type="appl" />
         <tli id="T115" time="79.542" type="appl" />
         <tli id="T116" time="80.153" type="appl" />
         <tli id="T117" time="81.52632806477766" />
         <tli id="T118" time="81.549" type="appl" />
         <tli id="T119" time="82.335" type="appl" />
         <tli id="T120" time="83.12" type="appl" />
         <tli id="T121" time="83.906" type="appl" />
         <tli id="T122" time="84.691" type="appl" />
         <tli id="T123" time="85.476" type="appl" />
         <tli id="T124" time="86.262" type="appl" />
         <tli id="T125" time="87.047" type="appl" />
         <tli id="T126" time="87.832" type="appl" />
         <tli id="T127" time="88.618" type="appl" />
         <tli id="T128" time="89.403" type="appl" />
         <tli id="T129" time="90.189" type="appl" />
         <tli id="T130" time="90.974" type="appl" />
         <tli id="T131" time="91.759" type="appl" />
         <tli id="T132" time="92.545" type="appl" />
         <tli id="T133" time="93.33" type="appl" />
         <tli id="T134" time="94.116" type="appl" />
         <tli id="T135" time="95.83960195103799" />
         <tli id="T136" time="95.876" type="appl" />
         <tli id="T137" time="96.85" type="appl" />
         <tli id="T138" time="97.825" type="appl" />
         <tli id="T139" time="98.799" type="appl" />
         <tli id="T140" time="99.774" type="appl" />
         <tli id="T141" time="100.748" type="appl" />
         <tli id="T142" time="101.98300091493194" />
         <tli id="T143" time="102.635" type="appl" />
         <tli id="T144" time="103.547" type="appl" />
         <tli id="T145" time="104.458" type="appl" />
         <tli id="T146" time="105.52999790200066" />
         <tli id="T147" time="106.049" type="appl" />
         <tli id="T148" time="106.728" type="appl" />
         <tli id="T149" time="107.406" type="appl" />
         <tli id="T150" time="108.085" type="appl" />
         <tli id="T151" time="108.764" type="appl" />
         <tli id="T152" time="109.72299481021577" />
         <tli id="T153" time="109.837" type="appl" />
         <tli id="T154" time="110.232" type="appl" />
         <tli id="T155" time="110.626" type="appl" />
         <tli id="T156" time="111.02" type="appl" />
         <tli id="T157" time="111.414" type="appl" />
         <tli id="T158" time="111.809" type="appl" />
         <tli id="T159" time="112.203" type="appl" />
         <tli id="T160" time="112.597" type="appl" />
         <tli id="T161" time="112.992" type="appl" />
         <tli id="T162" time="113.386" type="appl" />
         <tli id="T163" time="113.78" type="appl" />
         <tli id="T164" time="114.174" type="appl" />
         <tli id="T165" time="114.569" type="appl" />
         <tli id="T166" time="114.963" type="appl" />
         <tli id="T167" time="115.357" type="appl" />
         <tli id="T168" time="115.752" type="appl" />
         <tli id="T169" time="116.146" type="appl" />
         <tli id="T170" time="116.54" type="appl" />
         <tli id="T171" time="116.934" type="appl" />
         <tli id="T172" time="117.329" type="appl" />
         <tli id="T173" time="117.723" type="appl" />
         <tli id="T174" time="118.117" type="appl" />
         <tli id="T175" time="118.511" type="appl" />
         <tli id="T176" time="118.906" type="appl" />
         <tli id="T177" time="119.3" type="appl" />
         <tli id="T178" time="119.694" type="appl" />
         <tli id="T179" time="120.089" type="appl" />
         <tli id="T180" time="120.483" type="appl" />
         <tli id="T181" time="120.877" type="appl" />
         <tli id="T182" time="121.271" type="appl" />
         <tli id="T183" time="121.666" type="appl" />
         <tli id="T184" time="135.20610511748757" />
         <tli id="T185" time="135.271" type="appl" />
         <tli id="T186" time="136.214" type="appl" />
         <tli id="T187" time="137.158" type="appl" />
         <tli id="T188" time="138.101" type="appl" />
         <tli id="T189" time="139.044" type="appl" />
         <tli id="T190" time="139.988" type="appl" />
         <tli id="T191" time="140.931" type="appl" />
         <tli id="T192" time="141.874" type="appl" />
         <tli id="T193" time="142.817" type="appl" />
         <tli id="T194" time="143.76" type="appl" />
         <tli id="T195" time="144.704" type="appl" />
         <tli id="T196" time="145.56033294678545" />
         <tli id="T197" time="146.22217119914148" type="intp" />
         <tli id="T198" time="146.88400945149752" type="intp" />
         <tli id="T199" time="147.54584770385355" type="intp" />
         <tli id="T200" time="148.2076859562096" type="intp" />
         <tli id="T201" time="148.86952420856562" type="intp" />
         <tli id="T202" time="149.53136246092166" type="intp" />
         <tli id="T203" time="150.1932007132777" type="intp" />
         <tli id="T204" time="150.85503896563372" type="intp" />
         <tli id="T205" time="151.51687721798976" type="intp" />
         <tli id="T206" time="152.1787154703458" type="intp" />
         <tli id="T207" time="152.84055372270183" type="intp" />
         <tli id="T208" time="153.50239197505786" type="intp" />
         <tli id="T209" time="154.1642302274139" type="intp" />
         <tli id="T210" time="154.82606847976993" type="intp" />
         <tli id="T211" time="155.48790673212596" type="intp" />
         <tli id="T212" time="156.149744984482" type="intp" />
         <tli id="T213" time="156.81158323683803" type="intp" />
         <tli id="T214" time="157.4734214891941" />
         <tli id="T215" time="157.666" type="appl" />
         <tli id="T216" time="158.43" type="appl" />
         <tli id="T217" time="159.193" type="appl" />
         <tli id="T218" time="159.957" type="appl" />
         <tli id="T219" time="160.72" type="appl" />
         <tli id="T220" time="161.484" type="appl" />
         <tli id="T221" time="162.247" type="appl" />
         <tli id="T222" time="163.01" type="appl" />
         <tli id="T223" time="163.774" type="appl" />
         <tli id="T224" time="164.537" type="appl" />
         <tli id="T225" time="165.301" type="appl" />
         <tli id="T226" time="166.1973305688333" />
         <tli id="T227" time="166.839" type="appl" />
         <tli id="T228" time="167.614" type="appl" />
         <tli id="T229" time="168.389" type="appl" />
         <tli id="T230" time="169.164" type="appl" />
         <tli id="T231" time="169.939" type="appl" />
         <tli id="T232" time="170.714" type="appl" />
         <tli id="T233" time="171.49" type="appl" />
         <tli id="T234" time="172.265" type="appl" />
         <tli id="T235" time="173.04" type="appl" />
         <tli id="T236" time="173.815" type="appl" />
         <tli id="T237" time="174.59" type="appl" />
         <tli id="T238" time="175.365" type="appl" />
         <tli id="T239" time="176.8925986483613" />
         <tli id="T240" time="176.963" type="appl" />
         <tli id="T241" time="177.786" type="appl" />
         <tli id="T242" time="178.609" type="appl" />
         <tli id="T243" time="179.432" type="appl" />
         <tli id="T244" time="180.254" type="appl" />
         <tli id="T245" time="181.077" type="appl" />
         <tli id="T246" time="181.9" type="appl" />
         <tli id="T247" time="182.723" type="appl" />
         <tli id="T248" time="183.546" type="appl" />
         <tli id="T249" time="184.369" type="appl" />
         <tli id="T250" time="185.192" type="appl" />
         <tli id="T251" time="186.015" type="appl" />
         <tli id="T252" time="186.838" type="appl" />
         <tli id="T253" time="187.66" type="appl" />
         <tli id="T254" time="188.483" type="appl" />
         <tli id="T255" time="189.306" type="appl" />
         <tli id="T256" time="190.129" type="appl" />
         <tli id="T257" time="191.18587261768693" />
         <tli id="T258" time="191.745" type="appl" />
         <tli id="T259" time="192.539" type="appl" />
         <tli id="T260" time="193.332" type="appl" />
         <tli id="T261" time="194.126" type="appl" />
         <tli id="T262" time="194.919" type="appl" />
         <tli id="T263" time="195.713" type="appl" />
         <tli id="T264" time="196.506" type="appl" />
         <tli id="T265" time="197.3" type="appl" />
         <tli id="T266" time="198.73250794104356" />
         <tli id="T267" time="198.831" type="appl" />
         <tli id="T268" time="199.57" type="appl" />
         <tli id="T269" time="200.308" type="appl" />
         <tli id="T270" time="201.046" type="appl" />
         <tli id="T271" time="201.785" type="appl" />
         <tli id="T272" time="202.523" type="appl" />
         <tli id="T273" time="203.261" type="appl" />
         <tli id="T274" time="203.999" type="appl" />
         <tli id="T275" time="204.738" type="appl" />
         <tli id="T276" time="205.476" type="appl" />
         <tli id="T277" time="206.214" type="appl" />
         <tli id="T278" time="206.953" type="appl" />
         <tli id="T279" time="207.93913636997954" />
         <tli id="T280" time="208.664" type="appl" />
         <tli id="T281" time="209.638" type="appl" />
         <tli id="T282" time="210.611" type="appl" />
         <tli id="T283" time="211.585" type="appl" />
         <tli id="T284" time="212.558" type="appl" />
         <tli id="T285" time="213.532" type="appl" />
         <tli id="T286" time="214.505" type="appl" />
         <tli id="T287" time="215.479" type="appl" />
         <tli id="T288" time="216.2386722121608" />
         <tli id="T289" time="217.229" type="appl" />
         <tli id="T290" time="218.006" type="appl" />
         <tli id="T291" time="218.783" type="appl" />
         <tli id="T292" time="219.559" type="appl" />
         <tli id="T293" time="220.336" type="appl" />
         <tli id="T294" time="221.113" type="appl" />
         <tli id="T295" time="221.89" type="appl" />
         <tli id="T296" time="222.667" type="appl" />
         <tli id="T297" time="223.444" type="appl" />
         <tli id="T298" time="224.221" type="appl" />
         <tli id="T299" time="224.998" type="appl" />
         <tli id="T300" time="225.774" type="appl" />
         <tli id="T301" time="226.551" type="appl" />
         <tli id="T302" time="227.328" type="appl" />
         <tli id="T303" time="228.105" type="appl" />
         <tli id="T304" time="228.732" type="appl" />
         <tli id="T305" time="229.36" type="appl" />
         <tli id="T306" time="229.988" type="appl" />
         <tli id="T307" time="230.615" type="appl" />
         <tli id="T308" time="231.242" type="appl" />
         <tli id="T309" time="231.87" type="appl" />
         <tli id="T310" time="232.567" type="appl" />
         <tli id="T311" time="233.265" type="appl" />
         <tli id="T312" time="233.962" type="appl" />
         <tli id="T313" time="234.66" type="appl" />
         <tli id="T314" time="235.357" type="appl" />
         <tli id="T315" time="236.055" type="appl" />
         <tli id="T316" time="237.61901310106055" />
         <tli id="T317" time="237.677" type="appl" />
         <tli id="T318" time="238.602" type="appl" />
         <tli id="T319" time="239.527" type="appl" />
         <tli id="T320" time="240.452" type="appl" />
         <tli id="T321" time="241.377" type="appl" />
         <tli id="T322" time="242.302" type="appl" />
         <tli id="T323" time="243.227" type="appl" />
         <tli id="T324" time="244.152" type="appl" />
         <tli id="T325" time="245.49898037332872" />
         <tli id="T326" time="245.911" type="appl" />
         <tli id="T327" time="246.745" type="appl" />
         <tli id="T328" time="247.579" type="appl" />
         <tli id="T329" time="248.413" type="appl" />
         <tli id="T330" time="249.247" type="appl" />
         <tli id="T331" time="250.081" type="appl" />
         <tli id="T332" time="250.915" type="appl" />
         <tli id="T333" time="251.527" type="appl" />
         <tli id="T334" time="252.138" type="appl" />
         <tli id="T335" time="252.75" type="appl" />
         <tli id="T336" time="253.361" type="appl" />
         <tli id="T337" time="253.973" type="appl" />
         <tli id="T338" time="254.584" type="appl" />
         <tli id="T339" time="255.196" type="appl" />
         <tli id="T340" time="255.807" type="appl" />
         <tli id="T341" time="256.419" type="appl" />
         <tli id="T342" time="257.331" type="appl" />
         <tli id="T343" time="258.243" type="appl" />
         <tli id="T344" time="259.154" type="appl" />
         <tli id="T345" time="260.066" type="appl" />
         <tli id="T346" time="261.7389129242976" />
         <tli id="T347" time="261.845" type="appl" />
         <tli id="T348" time="262.712" type="appl" />
         <tli id="T349" time="263.579" type="appl" />
         <tli id="T350" time="264.446" type="appl" />
         <tli id="T351" time="265.312" type="appl" />
         <tli id="T352" time="266.179" type="appl" />
         <tli id="T353" time="267.046" type="appl" />
         <tli id="T354" time="267.913" type="appl" />
         <tli id="T355" time="268.78" type="appl" />
         <tli id="T356" time="269.647" type="appl" />
         <tli id="T357" time="270.514" type="appl" />
         <tli id="T358" time="271.381" type="appl" />
         <tli id="T359" time="272.247" type="appl" />
         <tli id="T360" time="273.114" type="appl" />
         <tli id="T361" time="273.981" type="appl" />
         <tli id="T362" time="274.848" type="appl" />
         <tli id="T363" time="276.09218664442733" />
         <tli id="T364" time="276.677" type="appl" />
         <tli id="T365" time="277.638" type="appl" />
         <tli id="T366" time="278.6" type="appl" />
         <tli id="T367" time="279.561" type="appl" />
         <tli id="T368" time="280.523" type="appl" />
         <tli id="T369" time="281.484" type="appl" />
         <tli id="T370" time="282.446" type="appl" />
         <tli id="T371" time="283.407" type="appl" />
         <tli id="T372" time="284.369" type="appl" />
         <tli id="T373" time="285.33" type="appl" />
         <tli id="T374" time="286.292" type="appl" />
         <tli id="T375" time="287.253" type="appl" />
         <tli id="T376" time="288.6988009522607" />
         <tli id="T377" time="289.154" type="appl" />
         <tli id="T378" time="290.093" type="appl" />
         <tli id="T379" time="291.033" type="appl" />
         <tli id="T380" time="291.972" type="appl" />
         <tli id="T381" time="292.911" type="appl" />
         <tli id="T382" time="293.85" type="appl" />
         <tli id="T383" time="294.789" type="appl" />
         <tli id="T384" time="295.728" type="appl" />
         <tli id="T952" time="296.198" type="intp" />
         <tli id="T385" time="296.668" type="appl" />
         <tli id="T386" time="297.607" type="appl" />
         <tli id="T387" time="298.81933183550143" />
         <tli id="T388" time="299.088" type="appl" />
         <tli id="T389" time="299.63" type="appl" />
         <tli id="T390" time="300.172" type="appl" />
         <tli id="T391" time="300.713" type="appl" />
         <tli id="T392" time="301.255" type="appl" />
         <tli id="T393" time="301.797" type="appl" />
         <tli id="T394" time="302.339" type="appl" />
         <tli id="T395" time="302.881" type="appl" />
         <tli id="T396" time="303.423" type="appl" />
         <tli id="T397" time="303.964" type="appl" />
         <tli id="T398" time="304.506" type="appl" />
         <tli id="T399" time="305.048" type="appl" />
         <tli id="T400" time="309.67871381675127" />
         <tli id="T401" time="310.29328512789925" type="intp" />
         <tli id="T402" time="310.90785643904724" type="intp" />
         <tli id="T403" time="311.5224277501952" type="intp" />
         <tli id="T404" time="312.1369990613432" type="intp" />
         <tli id="T405" time="312.75157037249113" type="intp" />
         <tli id="T406" time="313.36614168363906" type="intp" />
         <tli id="T407" time="313.98071299478704" type="intp" />
         <tli id="T408" time="314.595284305935" type="intp" />
         <tli id="T409" time="315.20985561708295" type="intp" />
         <tli id="T410" time="315.8244269282309" type="intp" />
         <tli id="T411" time="316.43899823937886" />
         <tli id="T412" time="316.816" type="appl" />
         <tli id="T413" time="317.679" type="appl" />
         <tli id="T414" time="318.542" type="appl" />
         <tli id="T415" time="319.405" type="appl" />
         <tli id="T416" time="320.268" type="appl" />
         <tli id="T417" time="321.131" type="appl" />
         <tli id="T418" time="321.994" type="appl" />
         <tli id="T419" time="322.858" type="appl" />
         <tli id="T420" time="323.721" type="appl" />
         <tli id="T421" time="324.584" type="appl" />
         <tli id="T422" time="325.447" type="appl" />
         <tli id="T423" time="326.31" type="appl" />
         <tli id="T424" time="327.173" type="appl" />
         <tli id="T425" time="328.036" type="appl" />
         <tli id="T426" time="329.45232960800126" />
         <tli id="T427" time="329.628" type="appl" />
         <tli id="T428" time="330.357" type="appl" />
         <tli id="T429" time="331.086" type="appl" />
         <tli id="T430" time="331.816" type="appl" />
         <tli id="T431" time="332.545" type="appl" />
         <tli id="T432" time="333.9719462534223" />
         <tli id="T433" time="334.021" type="appl" />
         <tli id="T434" time="334.768" type="appl" />
         <tli id="T435" time="335.515" type="appl" />
         <tli id="T436" time="336.262" type="appl" />
         <tli id="T437" time="337.01" type="appl" />
         <tli id="T438" time="337.757" type="appl" />
         <tli id="T439" time="338.504" type="appl" />
         <tli id="T440" time="339.251" type="appl" />
         <tli id="T441" time="339.998" type="appl" />
         <tli id="T442" time="340.745" type="appl" />
         <tli id="T443" time="341.492" type="appl" />
         <tli id="T444" time="342.239" type="appl" />
         <tli id="T445" time="342.986" type="appl" />
         <tli id="T446" time="343.733" type="appl" />
         <tli id="T447" time="344.48" type="appl" />
         <tli id="T448" time="345.228" type="appl" />
         <tli id="T449" time="345.975" type="appl" />
         <tli id="T450" time="346.722" type="appl" />
         <tli id="T451" time="347.469" type="appl" />
         <tli id="T452" time="348.6226666548385" />
         <tli id="T453" time="348.695" type="appl" />
         <tli id="T454" time="349.174" type="appl" />
         <tli id="T455" time="349.653" type="appl" />
         <tli id="T456" time="350.132" type="appl" />
         <tli id="T457" time="350.61" type="appl" />
         <tli id="T458" time="351.089" type="appl" />
         <tli id="T459" time="351.568" type="appl" />
         <tli id="T460" time="352.047" type="appl" />
         <tli id="T461" time="352.526" type="appl" />
         <tli id="T462" time="353.50519845963686" />
         <tli id="T463" time="353.546" type="appl" />
         <tli id="T464" time="354.087" type="appl" />
         <tli id="T465" time="354.628" type="appl" />
         <tli id="T466" time="355.169" type="appl" />
         <tli id="T467" time="355.711" type="appl" />
         <tli id="T468" time="356.252" type="appl" />
         <tli id="T469" time="356.793" type="appl" />
         <tli id="T470" time="357.334" type="appl" />
         <tli id="T471" time="357.875" type="appl" />
         <tli id="T472" time="358.416" type="appl" />
         <tli id="T473" time="358.957" type="appl" />
         <tli id="T474" time="359.499" type="appl" />
         <tli id="T475" time="360.04" type="appl" />
         <tli id="T476" time="360.581" type="appl" />
         <tli id="T477" time="361.122" type="appl" />
         <tli id="T478" time="365.85181384731936" />
         <tli id="T479" time="366.671" type="appl" />
         <tli id="T480" time="367.511" type="appl" />
         <tli id="T481" time="368.352" type="appl" />
         <tli id="T482" time="369.192" type="appl" />
         <tli id="T483" time="370.033" type="appl" />
         <tli id="T484" time="370.874" type="appl" />
         <tli id="T485" time="371.714" type="appl" />
         <tli id="T486" time="372.555" type="appl" />
         <tli id="T487" time="373.395" type="appl" />
         <tli id="T488" time="374.236" type="appl" />
         <tli id="T489" time="375.076" type="appl" />
         <tli id="T490" time="375.917" type="appl" />
         <tli id="T491" time="376.598" type="appl" />
         <tli id="T492" time="377.278" type="appl" />
         <tli id="T493" time="377.959" type="appl" />
         <tli id="T494" time="378.64" type="appl" />
         <tli id="T495" time="379.32" type="appl" />
         <tli id="T496" time="380.001" type="appl" />
         <tli id="T497" time="380.682" type="appl" />
         <tli id="T498" time="381.362" type="appl" />
         <tli id="T499" time="382.043" type="appl" />
         <tli id="T500" time="382.723" type="appl" />
         <tli id="T501" time="383.404" type="appl" />
         <tli id="T502" time="384.085" type="appl" />
         <tli id="T503" time="384.765" type="appl" />
         <tli id="T504" time="385.446" type="appl" />
         <tli id="T505" time="386.127" type="appl" />
         <tli id="T506" time="386.807" type="appl" />
         <tli id="T507" time="387.488" type="appl" />
         <tli id="T508" time="388.169" type="appl" />
         <tli id="T509" time="388.849" type="appl" />
         <tli id="T510" time="390.4183784820977" />
         <tli id="T511" time="390.44125232139845" type="intp" />
         <tli id="T512" time="390.46412616069927" type="intp" />
         <tli id="T513" time="390.487" type="appl" />
         <tli id="T514" time="390.806" type="appl" />
         <tli id="T515" time="391.124" type="appl" />
         <tli id="T516" time="391.443" type="appl" />
         <tli id="T517" time="391.762" type="appl" />
         <tli id="T518" time="392.081" type="appl" />
         <tli id="T519" time="392.4" type="appl" />
         <tli id="T520" time="392.719" type="appl" />
         <tli id="T521" time="393.038" type="appl" />
         <tli id="T522" time="393.357" type="appl" />
         <tli id="T523" time="393.676" type="appl" />
         <tli id="T524" time="393.994" type="appl" />
         <tli id="T525" time="394.313" type="appl" />
         <tli id="T526" time="394.632" type="appl" />
         <tli id="T527" time="394.951" type="appl" />
         <tli id="T528" time="395.27" type="appl" />
         <tli id="T529" time="395.589" type="appl" />
         <tli id="T530" time="395.908" type="appl" />
         <tli id="T531" time="396.227" type="appl" />
         <tli id="T532" time="396.546" type="appl" />
         <tli id="T533" time="396.865" type="appl" />
         <tli id="T534" time="397.183" type="appl" />
         <tli id="T535" time="397.502" type="appl" />
         <tli id="T536" time="397.821" type="appl" />
         <tli id="T537" time="398.14" type="appl" />
         <tli id="T538" time="413.2049505096888" />
         <tli id="T539" time="414.001" type="appl" />
         <tli id="T540" time="414.892" type="appl" />
         <tli id="T541" time="415.782" type="appl" />
         <tli id="T542" time="416.672" type="appl" />
         <tli id="T543" time="417.562" type="appl" />
         <tli id="T544" time="418.453" type="appl" />
         <tli id="T545" time="419.343" type="appl" />
         <tli id="T546" time="420.233" type="appl" />
         <tli id="T547" time="421.123" type="appl" />
         <tli id="T548" time="422.014" type="appl" />
         <tli id="T549" time="423.67157370551024" />
         <tli id="T550" time="423.672" type="appl" />
         <tli id="T551" time="424.44" type="appl" />
         <tli id="T552" time="425.208" type="appl" />
         <tli id="T553" time="425.976" type="appl" />
         <tli id="T554" time="426.745" type="appl" />
         <tli id="T555" time="427.513" type="appl" />
         <tli id="T556" time="428.281" type="appl" />
         <tli id="T557" time="429.049" type="appl" />
         <tli id="T558" time="429.8503240850797" />
         <tli id="T559" time="430.526" type="appl" />
         <tli id="T560" time="431.235" type="appl" />
         <tli id="T561" time="431.944" type="appl" />
         <tli id="T562" time="432.654" type="appl" />
         <tli id="T563" time="433.363" type="appl" />
         <tli id="T564" time="435.41152494617364" />
         <tli id="T565" time="435.60226247308685" type="intp" />
         <tli id="T566" time="435.793" type="appl" />
         <tli id="T567" time="436.653" type="appl" />
         <tli id="T568" time="437.513" type="appl" />
         <tli id="T569" time="438.374" type="appl" />
         <tli id="T570" time="439.234" type="appl" />
         <tli id="T571" time="440.095" type="appl" />
         <tli id="T572" time="440.955" type="appl" />
         <tli id="T573" time="441.815" type="appl" />
         <tli id="T574" time="442.676" type="appl" />
         <tli id="T575" time="443.536" type="appl" />
         <tli id="T576" time="444.412" type="appl" />
         <tli id="T577" time="445.287" type="appl" />
         <tli id="T578" time="446.163" type="appl" />
         <tli id="T579" time="447.039" type="appl" />
         <tli id="T580" time="447.914" type="appl" />
         <tli id="T581" time="448.79" type="appl" />
         <tli id="T582" time="449.666" type="appl" />
         <tli id="T583" time="450.541" type="appl" />
         <tli id="T584" time="451.417" type="appl" />
         <tli id="T585" time="452.292" type="appl" />
         <tli id="T586" time="453.168" type="appl" />
         <tli id="T587" time="454.044" type="appl" />
         <tli id="T588" time="454.919" type="appl" />
         <tli id="T589" time="456.5083123252513" />
         <tli id="T590" time="456.75" type="appl" />
         <tli id="T591" time="457.706" type="appl" />
         <tli id="T592" time="458.661" type="appl" />
         <tli id="T593" time="459.616" type="appl" />
         <tli id="T594" time="460.572" type="appl" />
         <tli id="T595" time="461.527" type="appl" />
         <tli id="T596" time="462.483" type="appl" />
         <tli id="T597" time="463.438" type="appl" />
         <tli id="T598" time="464.393" type="appl" />
         <tli id="T599" time="465.349" type="appl" />
         <tli id="T600" time="466.304" type="appl" />
         <tli id="T601" time="467.011" type="appl" />
         <tli id="T602" time="467.718" type="appl" />
         <tli id="T603" time="468.425" type="appl" />
         <tli id="T604" time="469.132" type="appl" />
         <tli id="T605" time="469.839" type="appl" />
         <tli id="T606" time="470.546" type="appl" />
         <tli id="T607" time="471.253" type="appl" />
         <tli id="T608" time="471.96" type="appl" />
         <tli id="T609" time="472.667" type="appl" />
         <tli id="T610" time="473.374" type="appl" />
         <tli id="T611" time="474.081" type="appl" />
         <tli id="T612" time="474.788" type="appl" />
         <tli id="T613" time="475.495" type="appl" />
         <tli id="T614" time="476.202" type="appl" />
         <tli id="T615" time="476.909" type="appl" />
         <tli id="T616" time="478.77801149956144" />
         <tli id="T617" time="478.9520057497807" type="intp" />
         <tli id="T618" time="479.126" type="appl" />
         <tli id="T619" time="479.881" type="appl" />
         <tli id="T620" time="480.636" type="appl" />
         <tli id="T621" time="481.391" type="appl" />
         <tli id="T622" time="482.147" type="appl" />
         <tli id="T623" time="482.902" type="appl" />
         <tli id="T624" time="483.657" type="appl" />
         <tli id="T625" time="484.412" type="appl" />
         <tli id="T626" time="485.6179830912256" />
         <tli id="T627" time="485.97" type="appl" />
         <tli id="T628" time="486.774" type="appl" />
         <tli id="T629" time="487.577" type="appl" />
         <tli id="T630" time="488.38" type="appl" />
         <tli id="T631" time="489.184" type="appl" />
         <tli id="T632" time="489.987" type="appl" />
         <tli id="T633" time="490.791" type="appl" />
         <tli id="T634" time="491.98066499854224" />
         <tli id="T635" time="492.385" type="appl" />
         <tli id="T636" time="493.176" type="appl" />
         <tli id="T637" time="493.967" type="appl" />
         <tli id="T638" time="494.758" type="appl" />
         <tli id="T639" time="495.549" type="appl" />
         <tli id="T640" time="496.48002131144335" />
         <tli id="T641" time="497.0" type="appl" />
         <tli id="T642" time="497.66" type="appl" />
         <tli id="T643" time="498.32" type="appl" />
         <tli id="T644" time="498.98" type="appl" />
         <tli id="T645" time="499.64" type="appl" />
         <tli id="T646" time="500.4266715865557" />
         <tli id="T647" time="501.101" type="appl" />
         <tli id="T648" time="501.903" type="appl" />
         <tli id="T649" time="502.704" type="appl" />
         <tli id="T650" time="503.505" type="appl" />
         <tli id="T651" time="504.306" type="appl" />
         <tli id="T652" time="505.107" type="appl" />
         <tli id="T653" time="505.909" type="appl" />
         <tli id="T654" time="507.3978926331039" />
         <tli id="T655" time="507.572" type="appl" />
         <tli id="T656" time="508.434" type="appl" />
         <tli id="T657" time="509.296" type="appl" />
         <tli id="T658" time="510.158" type="appl" />
         <tli id="T659" time="511.019" type="appl" />
         <tli id="T660" time="511.881" type="appl" />
         <tli id="T661" time="512.743" type="appl" />
         <tli id="T662" time="513.605" type="appl" />
         <tli id="T663" time="514.8736428341997" />
         <tli id="T664" time="515.477" type="appl" />
         <tli id="T665" time="516.488" type="appl" />
         <tli id="T666" time="517.498" type="appl" />
         <tli id="T667" time="518.509" type="appl" />
         <tli id="T668" time="519.519" type="appl" />
         <tli id="T669" time="520.53" type="appl" />
         <tli id="T670" time="522.2778308325137" />
         <tli id="T671" time="522.351" type="appl" />
         <tli id="T672" time="523.162" type="appl" />
         <tli id="T673" time="523.973" type="appl" />
         <tli id="T674" time="524.784" type="appl" />
         <tli id="T675" time="525.595" type="appl" />
         <tli id="T676" time="526.406" type="appl" />
         <tli id="T677" time="527.217" type="appl" />
         <tli id="T678" time="528.028" type="appl" />
         <tli id="T679" time="528.838" type="appl" />
         <tli id="T680" time="529.649" type="appl" />
         <tli id="T681" time="530.46" type="appl" />
         <tli id="T682" time="531.271" type="appl" />
         <tli id="T683" time="532.082" type="appl" />
         <tli id="T684" time="532.893" type="appl" />
         <tli id="T685" time="533.704" type="appl" />
         <tli id="T686" time="534.515" type="appl" />
         <tli id="T687" time="535.326" type="appl" />
         <tli id="T688" time="537.0311028913372" />
         <tli id="T689" time="537.036" type="appl" />
         <tli id="T690" time="537.936" type="appl" />
         <tli id="T691" time="538.835" type="appl" />
         <tli id="T692" time="539.735" type="appl" />
         <tli id="T693" time="540.634" type="appl" />
         <tli id="T694" time="541.534" type="appl" />
         <tli id="T695" time="542.434" type="appl" />
         <tli id="T696" time="543.333" type="appl" />
         <tli id="T697" time="544.232" type="appl" />
         <tli id="T698" time="545.132" type="appl" />
         <tli id="T699" time="546.032" type="appl" />
         <tli id="T700" time="546.931" type="appl" />
         <tli id="T701" time="547.83" type="appl" />
         <tli id="T702" time="549.2843853333245" />
         <tli id="T703" time="549.567" type="appl" />
         <tli id="T704" time="550.404" type="appl" />
         <tli id="T705" time="551.241" type="appl" />
         <tli id="T706" time="552.078" type="appl" />
         <tli id="T707" time="552.915" type="appl" />
         <tli id="T708" time="553.752" type="appl" />
         <tli id="T709" time="554.59" type="appl" />
         <tli id="T710" time="555.427" type="appl" />
         <tli id="T711" time="556.264" type="appl" />
         <tli id="T712" time="557.101" type="appl" />
         <tli id="T713" time="557.938" type="appl" />
         <tli id="T714" time="558.775" type="appl" />
         <tli id="T715" time="560.3110062033173" />
         <tli id="T716" time="560.372" type="appl" />
         <tli id="T717" time="561.131" type="appl" />
         <tli id="T718" time="561.891" type="appl" />
         <tli id="T719" time="562.651" type="appl" />
         <tli id="T720" time="563.41" type="appl" />
         <tli id="T721" time="564.17" type="appl" />
         <tli id="T722" time="564.93" type="appl" />
         <tli id="T723" time="565.689" type="appl" />
         <tli id="T724" time="566.449" type="appl" />
         <tli id="T725" time="567.209" type="appl" />
         <tli id="T726" time="567.968" type="appl" />
         <tli id="T727" time="569.0509699037772" />
         <tli id="T728" time="569.405" type="appl" />
         <tli id="T729" time="570.082" type="appl" />
         <tli id="T730" time="570.76" type="appl" />
         <tli id="T731" time="571.437" type="appl" />
         <tli id="T732" time="572.114" type="appl" />
         <tli id="T733" time="572.791" type="appl" />
         <tli id="T734" time="573.469" type="appl" />
         <tli id="T735" time="574.8509458148375" />
         <tli id="T736" time="574.853" type="appl" />
         <tli id="T737" time="575.56" type="appl" />
         <tli id="T738" time="576.267" type="appl" />
         <tli id="T739" time="576.975" type="appl" />
         <tli id="T740" time="577.682" type="appl" />
         <tli id="T741" time="578.389" type="appl" />
         <tli id="T742" time="579.096" type="appl" />
         <tli id="T743" time="579.803" type="appl" />
         <tli id="T744" time="580.51" type="appl" />
         <tli id="T745" time="581.217" type="appl" />
         <tli id="T746" time="581.925" type="appl" />
         <tli id="T747" time="582.632" type="appl" />
         <tli id="T748" time="583.339" type="appl" />
         <tli id="T749" time="585.030903534595" />
         <tli id="T750" time="585.034" type="appl" />
         <tli id="T751" time="586.022" type="appl" />
         <tli id="T752" time="587.011" type="appl" />
         <tli id="T753" time="587.999" type="appl" />
         <tli id="T754" time="588.987" type="appl" />
         <tli id="T755" time="589.975" type="appl" />
         <tli id="T756" time="590.963" type="appl" />
         <tli id="T757" time="591.952" type="appl" />
         <tli id="T758" time="592.94" type="appl" />
         <tli id="T759" time="594.3346669767685" />
         <tli id="T760" time="594.463" type="appl" />
         <tli id="T761" time="594.999" type="appl" />
         <tli id="T762" time="595.534" type="appl" />
         <tli id="T763" time="596.07" type="appl" />
         <tli id="T764" time="596.605" type="appl" />
         <tli id="T765" time="597.141" type="appl" />
         <tli id="T766" time="597.676" type="appl" />
         <tli id="T767" time="598.212" type="appl" />
         <tli id="T768" time="598.747" type="appl" />
         <tli id="T769" time="599.283" type="appl" />
         <tli id="T770" time="599.818" type="appl" />
         <tli id="T771" time="600.354" type="appl" />
         <tli id="T772" time="600.889" type="appl" />
         <tli id="T773" time="601.424" type="appl" />
         <tli id="T774" time="601.96" type="appl" />
         <tli id="T775" time="602.495" type="appl" />
         <tli id="T776" time="603.031" type="appl" />
         <tli id="T777" time="603.566" type="appl" />
         <tli id="T778" time="604.102" type="appl" />
         <tli id="T779" time="604.637" type="appl" />
         <tli id="T780" time="605.173" type="appl" />
         <tli id="T781" time="605.708" type="appl" />
         <tli id="T782" time="606.244" type="appl" />
         <tli id="T783" time="606.779" type="appl" />
         <tli id="T784" time="607.315" type="appl" />
         <tli id="T785" time="614.7374468215891" />
         <tli id="T786" time="615.103" type="appl" />
         <tli id="T787" time="615.709" type="appl" />
         <tli id="T788" time="616.314" type="appl" />
         <tli id="T789" time="616.92" type="appl" />
         <tli id="T790" time="617.525" type="appl" />
         <tli id="T791" time="618.131" type="appl" />
         <tli id="T792" time="618.736" type="appl" />
         <tli id="T793" time="619.342" type="appl" />
         <tli id="T794" time="619.947" type="appl" />
         <tli id="T795" time="620.553" type="appl" />
         <tli id="T796" time="621.158" type="appl" />
         <tli id="T797" time="621.764" type="appl" />
         <tli id="T798" time="622.369" type="appl" />
         <tli id="T799" time="622.975" type="appl" />
         <tli id="T800" time="623.9133462114847" />
         <tli id="T801" time="624.659" type="appl" />
         <tli id="T802" time="625.737" type="appl" />
         <tli id="T803" time="626.816" type="appl" />
         <tli id="T804" time="627.894" type="appl" />
         <tli id="T805" time="628.973" type="appl" />
         <tli id="T806" time="630.051" type="appl" />
         <tli id="T807" time="630.7107138134287" />
         <tli id="T808" time="631.7762649434492" type="intp" />
         <tli id="T809" time="632.8418160734697" type="intp" />
         <tli id="T810" time="633.9073672034901" type="intp" />
         <tli id="T811" time="634.9729183335106" type="intp" />
         <tli id="T812" time="636.0384694635311" type="intp" />
         <tli id="T813" time="637.1040205935515" />
         <tli id="T814" time="637.105" type="appl" />
         <tli id="T815" time="638.067" type="appl" />
         <tli id="T816" time="639.03" type="appl" />
         <tli id="T817" time="639.993" type="appl" />
         <tli id="T818" time="640.955" type="appl" />
         <tli id="T819" time="641.918" type="appl" />
         <tli id="T820" time="642.88" type="appl" />
         <tli id="T821" time="643.843" type="appl" />
         <tli id="T822" time="644.806" type="appl" />
         <tli id="T823" time="645.768" type="appl" />
         <tli id="T824" time="646.731" type="appl" />
         <tli id="T825" time="647.694" type="appl" />
         <tli id="T826" time="648.656" type="appl" />
         <tli id="T827" time="649.8439676809495" />
         <tli id="T828" time="650.393" type="appl" />
         <tli id="T829" time="651.167" type="appl" />
         <tli id="T830" time="651.942" type="appl" />
         <tli id="T831" time="652.716" type="appl" />
         <tli id="T832" time="654.2239494896467" />
         <tli id="T833" time="654.459" type="appl" />
         <tli id="T834" time="655.429" type="appl" />
         <tli id="T835" time="656.398" type="appl" />
         <tli id="T836" time="657.368" type="appl" />
         <tli id="T837" time="658.337" type="appl" />
         <tli id="T838" time="659.307" type="appl" />
         <tli id="T839" time="660.276" type="appl" />
         <tli id="T840" time="661.246" type="appl" />
         <tli id="T841" time="662.215" type="appl" />
         <tli id="T842" time="663.185" type="appl" />
         <tli id="T843" time="664.154" type="appl" />
         <tli id="T844" time="665.123" type="appl" />
         <tli id="T845" time="666.093" type="appl" />
         <tli id="T846" time="667.062" type="appl" />
         <tli id="T847" time="668.032" type="appl" />
         <tli id="T848" time="669.001" type="appl" />
         <tli id="T849" time="669.971" type="appl" />
         <tli id="T850" time="670.94" type="appl" />
         <tli id="T851" time="671.91" type="appl" />
         <tli id="T852" time="672.879" type="appl" />
         <tli id="T853" time="673.849" type="appl" />
         <tli id="T854" time="674.818" type="appl" />
         <tli id="T855" time="675.67" type="appl" />
         <tli id="T856" time="676.523" type="appl" />
         <tli id="T857" time="677.375" type="appl" />
         <tli id="T858" time="678.227" type="appl" />
         <tli id="T859" time="679.079" type="appl" />
         <tli id="T860" time="679.932" type="appl" />
         <tli id="T861" time="680.784" type="appl" />
         <tli id="T862" time="681.609" type="appl" />
         <tli id="T863" time="682.434" type="appl" />
         <tli id="T864" time="683.259" type="appl" />
         <tli id="T865" time="684.084" type="appl" />
         <tli id="T866" time="684.909" type="appl" />
         <tli id="T867" time="685.734" type="appl" />
         <tli id="T868" time="686.559" type="appl" />
         <tli id="T869" time="687.384" type="appl" />
         <tli id="T870" time="688.209" type="appl" />
         <tli id="T871" time="689.034" type="appl" />
         <tli id="T872" time="689.858" type="appl" />
         <tli id="T873" time="690.683" type="appl" />
         <tli id="T874" time="691.508" type="appl" />
         <tli id="T875" time="692.333" type="appl" />
         <tli id="T876" time="693.158" type="appl" />
         <tli id="T877" time="693.983" type="appl" />
         <tli id="T878" time="694.808" type="appl" />
         <tli id="T879" time="695.633" type="appl" />
         <tli id="T880" time="696.458" type="appl" />
         <tli id="T881" time="697.283" type="appl" />
         <tli id="T882" time="698.108" type="appl" />
         <tli id="T883" time="698.675" type="appl" />
         <tli id="T884" time="699.243" type="appl" />
         <tli id="T885" time="699.81" type="appl" />
         <tli id="T886" time="700.378" type="appl" />
         <tli id="T887" time="700.946" type="appl" />
         <tli id="T888" time="701.513" type="appl" />
         <tli id="T889" time="702.08" type="appl" />
         <tli id="T890" time="704.383741161851" />
         <tli id="T891" time="704.5839384231324" type="intp" />
         <tli id="T892" time="704.7374692115661" type="intp" />
         <tli id="T893" time="704.891" type="appl" />
         <tli id="T894" time="705.638" type="appl" />
         <tli id="T895" time="706.386" type="appl" />
         <tli id="T896" time="707.133" type="appl" />
         <tli id="T897" time="707.881" type="appl" />
         <tli id="T898" time="708.629" type="appl" />
         <tli id="T899" time="709.376" type="appl" />
         <tli id="T900" time="710.124" type="appl" />
         <tli id="T901" time="710.871" type="appl" />
         <tli id="T902" time="711.619" type="appl" />
         <tli id="T903" time="712.366" type="appl" />
         <tli id="T904" time="713.8837017058293" />
         <tli id="T905" time="713.89" type="appl" />
         <tli id="T906" time="714.665" type="appl" />
         <tli id="T907" time="715.441" type="appl" />
         <tli id="T908" time="716.216" type="appl" />
         <tli id="T909" time="716.992" type="appl" />
         <tli id="T910" time="717.768" type="appl" />
         <tli id="T911" time="718.543" type="appl" />
         <tli id="T912" time="719.9170099811275" />
         <tli id="T913" time="720.348" type="appl" />
         <tli id="T914" time="721.377" type="appl" />
         <tli id="T915" time="722.406" type="appl" />
         <tli id="T916" time="723.435" type="appl" />
         <tli id="T917" time="724.465" type="appl" />
         <tli id="T918" time="725.494" type="appl" />
         <tli id="T919" time="726.523" type="appl" />
         <tli id="T920" time="727.552" type="appl" />
         <tli id="T921" time="728.9503057966296" />
         <tli id="T922" time="729.404" type="appl" />
         <tli id="T923" time="730.227" type="appl" />
         <tli id="T924" time="731.049" type="appl" />
         <tli id="T925" time="731.872" type="appl" />
         <tli id="T926" time="732.695" type="appl" />
         <tli id="T927" time="733.518" type="appl" />
         <tli id="T928" time="734.34" type="appl" />
         <tli id="T929" time="735.163" type="appl" />
         <tli id="T930" time="735.986" type="appl" />
         <tli id="T931" time="736.809" type="appl" />
         <tli id="T932" time="737.632" type="appl" />
         <tli id="T933" time="738.454" type="appl" />
         <tli id="T934" time="739.277" type="appl" />
         <tli id="T935" time="740.4035915612291" />
         <tli id="T936" time="740.41" type="appl" />
         <tli id="T937" time="740.72" type="appl" />
         <tli id="T938" time="741.03" type="appl" />
         <tli id="T939" time="742.9635809288695" />
         <tli id="T940" time="745.2769046543154" />
         <tli id="T941" />
         <tli id="T942" />
         <tli id="T943" />
         <tli id="T944" time="745.8635688843997" />
         <tli id="T945" time="746.653" type="appl" />
         <tli id="T946" time="747.616" type="appl" />
         <tli id="T947" time="748.579" type="appl" />
         <tli id="T948" time="749.542" type="appl" />
         <tli id="T949" time="750.505" type="appl" />
         <tli id="T950" time="751.468" type="appl" />
         <tli id="T951" time="752.431" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx"
                      id="tx"
                      speaker="MiXS"
                      type="t">
         <timeline-fork end="T29" start="T27">
            <tli id="T27.tx.1" />
         </timeline-fork>
         <timeline-fork end="T554" start="T553">
            <tli id="T553.tx.1" />
         </timeline-fork>
         <timeline-fork end="T881" start="T878">
            <tli id="T878.tx.1" />
            <tli id="T878.tx.2" />
         </timeline-fork>
         <timeline-fork end="T889" start="T885">
            <tli id="T885.tx.1" />
            <tli id="T885.tx.2" />
            <tli id="T885.tx.3" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx">
            <ts e="T951" id="Seg_0" n="sc" s="T0">
               <ts e="T11" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Min</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">seriːge</ts>
                  <nts id="Seg_8" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_10" n="HIAT:w" s="T2">barbɨtɨm</ts>
                  <nts id="Seg_11" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_13" n="HIAT:w" s="T3">bejem</ts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_16" n="HIAT:w" s="T4">sanaːbɨnan</ts>
                  <nts id="Seg_17" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_19" n="HIAT:w" s="T5">tɨːhɨčča</ts>
                  <nts id="Seg_20" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_22" n="HIAT:w" s="T6">togus</ts>
                  <nts id="Seg_23" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_25" n="HIAT:w" s="T7">süːs</ts>
                  <nts id="Seg_26" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_28" n="HIAT:w" s="T8">tü͡örd-u͡on</ts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_31" n="HIAT:w" s="T9">biːr</ts>
                  <nts id="Seg_32" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_34" n="HIAT:w" s="T10">dʼɨllaːkka</ts>
                  <nts id="Seg_35" n="HIAT:ip">.</nts>
                  <nts id="Seg_36" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T16" id="Seg_38" n="HIAT:u" s="T11">
                  <ts e="T12" id="Seg_40" n="HIAT:w" s="T11">Kühüŋŋü</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_43" n="HIAT:w" s="T12">sʼentʼabrʼ</ts>
                  <nts id="Seg_44" n="HIAT:ip">,</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_47" n="HIAT:w" s="T13">tɨmnɨː</ts>
                  <nts id="Seg_48" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_50" n="HIAT:w" s="T14">tüher</ts>
                  <nts id="Seg_51" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_53" n="HIAT:w" s="T15">ɨjɨgar</ts>
                  <nts id="Seg_54" n="HIAT:ip">.</nts>
                  <nts id="Seg_55" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T31" id="Seg_57" n="HIAT:u" s="T16">
                  <ts e="T17" id="Seg_59" n="HIAT:w" s="T16">Maŋnaj</ts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_62" n="HIAT:w" s="T17">barammɨn</ts>
                  <nts id="Seg_63" n="HIAT:ip">,</nts>
                  <nts id="Seg_64" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_66" n="HIAT:w" s="T18">biːr</ts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_69" n="HIAT:w" s="T19">dʼɨlɨ</ts>
                  <nts id="Seg_70" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_72" n="HIAT:w" s="T20">gɨtta</ts>
                  <nts id="Seg_73" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_75" n="HIAT:w" s="T21">togus</ts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_78" n="HIAT:w" s="T22">ɨjɨ</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_81" n="HIAT:w" s="T23">seriː</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_84" n="HIAT:w" s="T24">ü͡öregiger</ts>
                  <nts id="Seg_85" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_87" n="HIAT:w" s="T25">ü͡örene</ts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_90" n="HIAT:w" s="T26">sɨldʼɨbɨtɨm</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27.tx.1" id="Seg_93" n="HIAT:w" s="T27">Dalʼnʼij</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_96" n="HIAT:w" s="T27.tx.1">Vastok</ts>
                  <nts id="Seg_97" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_99" n="HIAT:w" s="T29">di͡en</ts>
                  <nts id="Seg_100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_102" n="HIAT:w" s="T30">hirge</ts>
                  <nts id="Seg_103" n="HIAT:ip">.</nts>
                  <nts id="Seg_104" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_106" n="HIAT:u" s="T31">
                  <ts e="T32" id="Seg_108" n="HIAT:w" s="T31">Onton</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_111" n="HIAT:w" s="T32">sajɨn</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_114" n="HIAT:w" s="T33">tɨːhɨčča</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_117" n="HIAT:w" s="T34">togus</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T36" id="Seg_120" n="HIAT:w" s="T35">süːs</ts>
                  <nts id="Seg_121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_123" n="HIAT:w" s="T36">tü͡örd-u͡on</ts>
                  <nts id="Seg_124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_126" n="HIAT:w" s="T37">üs</ts>
                  <nts id="Seg_127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_129" n="HIAT:w" s="T38">dʼɨllaːkka</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_132" n="HIAT:w" s="T39">bihigini</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_135" n="HIAT:w" s="T40">ɨːppɨttara</ts>
                  <nts id="Seg_136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_138" n="HIAT:w" s="T41">zaːpad</ts>
                  <nts id="Seg_139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_141" n="HIAT:w" s="T42">di͡ek</ts>
                  <nts id="Seg_142" n="HIAT:ip">,</nts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_145" n="HIAT:w" s="T43">seriː</ts>
                  <nts id="Seg_146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_148" n="HIAT:w" s="T44">kersiːte</ts>
                  <nts id="Seg_149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_151" n="HIAT:w" s="T45">bara</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_154" n="HIAT:w" s="T46">turar</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_157" n="HIAT:w" s="T47">siriger</ts>
                  <nts id="Seg_158" n="HIAT:ip">.</nts>
                  <nts id="Seg_159" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_161" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_163" n="HIAT:w" s="T48">Bihigi</ts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_166" n="HIAT:w" s="T49">seriːni</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_169" n="HIAT:w" s="T50">körsübüppüt</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_172" n="HIAT:w" s="T51">Proxorovka</ts>
                  <nts id="Seg_173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_175" n="HIAT:w" s="T52">di͡en</ts>
                  <nts id="Seg_176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_178" n="HIAT:w" s="T53">aːttaːk</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_181" n="HIAT:w" s="T54">dʼerʼevnʼaga</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_184" n="HIAT:w" s="T55">Kurskaj</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_187" n="HIAT:w" s="T56">oblahɨgar</ts>
                  <nts id="Seg_188" n="HIAT:ip">.</nts>
                  <nts id="Seg_189" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T81" id="Seg_191" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_193" n="HIAT:w" s="T57">Onno</ts>
                  <nts id="Seg_194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_196" n="HIAT:w" s="T58">nʼemʼes</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_199" n="HIAT:w" s="T59">seriːtin</ts>
                  <nts id="Seg_200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_202" n="HIAT:w" s="T60">kihite</ts>
                  <nts id="Seg_203" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_205" n="HIAT:w" s="T61">bert</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_208" n="HIAT:w" s="T62">ügüs</ts>
                  <nts id="Seg_209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_211" n="HIAT:w" s="T63">seriː</ts>
                  <nts id="Seg_212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_214" n="HIAT:w" s="T64">saːtɨn</ts>
                  <nts id="Seg_215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_217" n="HIAT:w" s="T65">eŋin</ts>
                  <nts id="Seg_218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_220" n="HIAT:w" s="T66">atɨn</ts>
                  <nts id="Seg_221" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_223" n="HIAT:w" s="T67">teriltetin</ts>
                  <nts id="Seg_224" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_226" n="HIAT:w" s="T68">munnʼan</ts>
                  <nts id="Seg_227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_229" n="HIAT:w" s="T69">baraːn</ts>
                  <nts id="Seg_230" n="HIAT:ip">,</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_233" n="HIAT:w" s="T70">bihigi</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_236" n="HIAT:w" s="T71">armʼijabɨtɨn</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_239" n="HIAT:w" s="T72">berke</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_242" n="HIAT:w" s="T73">töttörü</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_245" n="HIAT:w" s="T74">di͡ek</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_248" n="HIAT:w" s="T75">otut</ts>
                  <nts id="Seg_249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_251" n="HIAT:w" s="T76">bi͡es</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_254" n="HIAT:w" s="T77">biːrsteleːk</ts>
                  <nts id="Seg_255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_257" n="HIAT:w" s="T78">sirge</ts>
                  <nts id="Seg_258" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_260" n="HIAT:w" s="T79">ɨgajbɨt</ts>
                  <nts id="Seg_261" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_263" n="HIAT:w" s="T80">ete</ts>
                  <nts id="Seg_264" n="HIAT:ip">.</nts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T89" id="Seg_267" n="HIAT:u" s="T81">
                  <ts e="T82" id="Seg_269" n="HIAT:w" s="T81">Onton</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_272" n="HIAT:w" s="T82">kojut</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_275" n="HIAT:w" s="T83">iti</ts>
                  <nts id="Seg_276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_278" n="HIAT:w" s="T84">siri</ts>
                  <nts id="Seg_279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_281" n="HIAT:w" s="T85">Kurskaj</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_284" n="HIAT:w" s="T86">duga</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_287" n="HIAT:w" s="T87">di͡en</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_290" n="HIAT:w" s="T88">aːttaːbɨttara</ts>
                  <nts id="Seg_291" n="HIAT:ip">.</nts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_294" n="HIAT:u" s="T89">
                  <ts e="T90" id="Seg_296" n="HIAT:w" s="T89">Sakalɨː</ts>
                  <nts id="Seg_297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_299" n="HIAT:w" s="T90">saŋardakka</ts>
                  <nts id="Seg_300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_302" n="HIAT:w" s="T91">Kurskaj</ts>
                  <nts id="Seg_303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_305" n="HIAT:w" s="T92">tokura</ts>
                  <nts id="Seg_306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_308" n="HIAT:w" s="T93">di͡ekke</ts>
                  <nts id="Seg_309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_311" n="HIAT:w" s="T94">höp</ts>
                  <nts id="Seg_312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_314" n="HIAT:w" s="T95">bu͡olu͡oga</ts>
                  <nts id="Seg_315" n="HIAT:ip">.</nts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T102" id="Seg_318" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_320" n="HIAT:w" s="T96">Nʼemʼes</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_323" n="HIAT:w" s="T97">armʼijatɨn</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_326" n="HIAT:w" s="T98">tojonun</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_329" n="HIAT:w" s="T99">sanaːta</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_332" n="HIAT:w" s="T100">baːr</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_335" n="HIAT:w" s="T101">ebit</ts>
                  <nts id="Seg_336" n="HIAT:ip">.</nts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T117" id="Seg_339" n="HIAT:u" s="T102">
                  <ts e="T103" id="Seg_341" n="HIAT:w" s="T102">Iti</ts>
                  <nts id="Seg_342" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_344" n="HIAT:w" s="T103">sirinen</ts>
                  <nts id="Seg_345" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_347" n="HIAT:w" s="T104">bɨha</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_350" n="HIAT:w" s="T105">mökküjen</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_353" n="HIAT:w" s="T106">baraːn</ts>
                  <nts id="Seg_354" n="HIAT:ip">,</nts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_357" n="HIAT:w" s="T107">bert</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_360" n="HIAT:w" s="T108">türgennik</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_363" n="HIAT:w" s="T109">bihigi</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_366" n="HIAT:w" s="T110">kiːn</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_369" n="HIAT:w" s="T111">sirbitin</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_372" n="HIAT:w" s="T112">Maskva</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_375" n="HIAT:w" s="T113">di͡en</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_378" n="HIAT:w" s="T114">gu͡oratɨ</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_381" n="HIAT:w" s="T115">ɨlar</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_384" n="HIAT:w" s="T116">kördük</ts>
                  <nts id="Seg_385" n="HIAT:ip">.</nts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T135" id="Seg_388" n="HIAT:u" s="T117">
                  <ts e="T118" id="Seg_390" n="HIAT:w" s="T117">Bihi͡ene</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_393" n="HIAT:w" s="T118">čaːspɨt</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_396" n="HIAT:w" s="T119">Kurskaj</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_399" n="HIAT:w" s="T120">tokurugar</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_402" n="HIAT:w" s="T121">tu͡oktan</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_405" n="HIAT:w" s="T122">da</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_408" n="HIAT:w" s="T123">ulakan</ts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_411" n="HIAT:w" s="T124">seriː</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_414" n="HIAT:w" s="T125">u͡otugar</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_417" n="HIAT:w" s="T126">kiːren</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_420" n="HIAT:w" s="T127">baraːn</ts>
                  <nts id="Seg_421" n="HIAT:ip">,</nts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_424" n="HIAT:w" s="T128">u͡on</ts>
                  <nts id="Seg_425" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T130" id="Seg_427" n="HIAT:w" s="T129">kün</ts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_430" n="HIAT:w" s="T130">turkarɨ</ts>
                  <nts id="Seg_431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_433" n="HIAT:w" s="T131">nʼemʼes</ts>
                  <nts id="Seg_434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_436" n="HIAT:w" s="T132">armʼijatɨn</ts>
                  <nts id="Seg_437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_439" n="HIAT:w" s="T133">bata</ts>
                  <nts id="Seg_440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_442" n="HIAT:w" s="T134">hataːbɨta</ts>
                  <nts id="Seg_443" n="HIAT:ip">.</nts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T142" id="Seg_446" n="HIAT:u" s="T135">
                  <ts e="T136" id="Seg_448" n="HIAT:w" s="T135">Iti</ts>
                  <nts id="Seg_449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_451" n="HIAT:w" s="T136">turkarɨ</ts>
                  <nts id="Seg_452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_454" n="HIAT:w" s="T137">bihigi</ts>
                  <nts id="Seg_455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_457" n="HIAT:w" s="T138">seriːlespippit</ts>
                  <nts id="Seg_458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_460" n="HIAT:w" s="T139">künü</ts>
                  <nts id="Seg_461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_463" n="HIAT:w" s="T140">tüːnü</ts>
                  <nts id="Seg_464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_466" n="HIAT:w" s="T141">bilbekke</ts>
                  <nts id="Seg_467" n="HIAT:ip">.</nts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T146" id="Seg_470" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_472" n="HIAT:w" s="T142">Utujbakka</ts>
                  <nts id="Seg_473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_475" n="HIAT:w" s="T143">da</ts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_478" n="HIAT:w" s="T144">sɨnnʼammakka</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_481" n="HIAT:w" s="T145">da</ts>
                  <nts id="Seg_482" n="HIAT:ip">.</nts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_485" n="HIAT:u" s="T146">
                  <ts e="T147" id="Seg_487" n="HIAT:w" s="T146">Kolobura</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_490" n="HIAT:w" s="T147">tugu</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_493" n="HIAT:w" s="T148">da</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_496" n="HIAT:w" s="T149">bilbet</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_499" n="HIAT:w" s="T150">istibet</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_502" n="HIAT:w" s="T151">kördük</ts>
                  <nts id="Seg_503" n="HIAT:ip">.</nts>
                  <nts id="Seg_504" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T184" id="Seg_506" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_508" n="HIAT:w" s="T152">Ikki</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_511" n="HIAT:w" s="T153">di͡egi</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_514" n="HIAT:w" s="T154">seriːleheːčči</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_517" n="HIAT:w" s="T155">öttütten</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_520" n="HIAT:w" s="T156">töhölü͡ök</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_523" n="HIAT:w" s="T157">tɨːhɨčča</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_526" n="HIAT:w" s="T158">kihi</ts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_529" n="HIAT:w" s="T159">süppütün</ts>
                  <nts id="Seg_530" n="HIAT:ip">,</nts>
                  <nts id="Seg_531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_533" n="HIAT:w" s="T160">ölbütün</ts>
                  <nts id="Seg_534" n="HIAT:ip">,</nts>
                  <nts id="Seg_535" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_537" n="HIAT:w" s="T161">töhölöːk</ts>
                  <nts id="Seg_538" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_540" n="HIAT:w" s="T162">kihi</ts>
                  <nts id="Seg_541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_543" n="HIAT:w" s="T163">gojobuːn</ts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_546" n="HIAT:w" s="T164">bu͡olbutun</ts>
                  <nts id="Seg_547" n="HIAT:ip">,</nts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_550" n="HIAT:w" s="T165">töhölöːk</ts>
                  <nts id="Seg_551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_553" n="HIAT:w" s="T166">seriː</ts>
                  <nts id="Seg_554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_556" n="HIAT:w" s="T167">saːta</ts>
                  <nts id="Seg_557" n="HIAT:ip">,</nts>
                  <nts id="Seg_558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_560" n="HIAT:w" s="T168">samalʼottar</ts>
                  <nts id="Seg_561" n="HIAT:ip">,</nts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_564" n="HIAT:w" s="T169">tankalar</ts>
                  <nts id="Seg_565" n="HIAT:ip">,</nts>
                  <nts id="Seg_566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_568" n="HIAT:w" s="T170">arudʼijalar</ts>
                  <nts id="Seg_569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_571" n="HIAT:w" s="T171">onton</ts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_574" n="HIAT:w" s="T172">kɨra</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_577" n="HIAT:w" s="T173">eŋin</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_580" n="HIAT:w" s="T174">saːlar</ts>
                  <nts id="Seg_581" n="HIAT:ip">,</nts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_584" n="HIAT:w" s="T175">seriː</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_587" n="HIAT:w" s="T176">teriltete</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_590" n="HIAT:w" s="T177">aldʼammɨtɨn</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_593" n="HIAT:w" s="T178">kim</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_596" n="HIAT:w" s="T179">daːgɨnɨ</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_599" n="HIAT:w" s="T180">čakčɨ</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_602" n="HIAT:w" s="T181">bilbet</ts>
                  <nts id="Seg_603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_605" n="HIAT:w" s="T182">ete</ts>
                  <nts id="Seg_606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_608" n="HIAT:w" s="T183">oččogo</ts>
                  <nts id="Seg_609" n="HIAT:ip">.</nts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T196" id="Seg_612" n="HIAT:u" s="T184">
                  <ts e="T185" id="Seg_614" n="HIAT:w" s="T184">Kajtak</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_617" n="HIAT:w" s="T185">ulakan</ts>
                  <nts id="Seg_618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_620" n="HIAT:w" s="T186">küːsteːk</ts>
                  <nts id="Seg_621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_623" n="HIAT:w" s="T187">seriː</ts>
                  <nts id="Seg_624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_626" n="HIAT:w" s="T188">barbɨtɨn</ts>
                  <nts id="Seg_627" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_629" n="HIAT:w" s="T189">iti</ts>
                  <nts id="Seg_630" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_632" n="HIAT:w" s="T190">hirge</ts>
                  <nts id="Seg_633" n="HIAT:ip">,</nts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_636" n="HIAT:w" s="T191">kihi</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_639" n="HIAT:w" s="T192">tu͡olkulu͡oga</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_642" n="HIAT:w" s="T193">biːr</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_645" n="HIAT:w" s="T194">munnuk</ts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_648" n="HIAT:w" s="T195">koloburtan</ts>
                  <nts id="Seg_649" n="HIAT:ip">.</nts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T214" id="Seg_652" n="HIAT:u" s="T196">
                  <ts e="T197" id="Seg_654" n="HIAT:w" s="T196">Biːr</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_656" n="HIAT:ip">(</nts>
                  <ts e="T198" id="Seg_658" n="HIAT:w" s="T197">buːstaːk</ts>
                  <nts id="Seg_659" n="HIAT:ip">)</nts>
                  <nts id="Seg_660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T199" id="Seg_662" n="HIAT:w" s="T198">sirkeːŋŋe</ts>
                  <nts id="Seg_663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_665" n="HIAT:w" s="T199">bihigi</ts>
                  <nts id="Seg_666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_668" n="HIAT:w" s="T200">baːr</ts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_671" n="HIAT:w" s="T201">sirbitiger</ts>
                  <nts id="Seg_672" n="HIAT:ip">,</nts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_675" n="HIAT:w" s="T202">küŋŋe</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_678" n="HIAT:w" s="T203">biːrde</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_681" n="HIAT:w" s="T204">kötön</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_684" n="HIAT:w" s="T205">keleːčči</ts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_687" n="HIAT:w" s="T206">ete</ts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_690" n="HIAT:w" s="T207">nʼemʼes</ts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_693" n="HIAT:w" s="T208">di͡egi</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_696" n="HIAT:w" s="T209">öttütten</ts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_699" n="HIAT:w" s="T210">ikki</ts>
                  <nts id="Seg_700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_702" n="HIAT:w" s="T211">hüːs</ts>
                  <nts id="Seg_703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_705" n="HIAT:w" s="T212">kuraŋa</ts>
                  <nts id="Seg_706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T214" id="Seg_708" n="HIAT:w" s="T213">samalʼottar</ts>
                  <nts id="Seg_709" n="HIAT:ip">.</nts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T226" id="Seg_712" n="HIAT:u" s="T214">
                  <ts e="T215" id="Seg_714" n="HIAT:w" s="T214">Ontuŋ</ts>
                  <nts id="Seg_715" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_717" n="HIAT:w" s="T215">barɨta</ts>
                  <nts id="Seg_718" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_720" n="HIAT:w" s="T216">bihigi</ts>
                  <nts id="Seg_721" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_723" n="HIAT:w" s="T217">ürdübütünen</ts>
                  <nts id="Seg_724" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_726" n="HIAT:w" s="T218">samɨːr</ts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_729" n="HIAT:w" s="T219">kördük</ts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_732" n="HIAT:w" s="T220">tühen</ts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_735" n="HIAT:w" s="T221">e</ts>
                  <nts id="Seg_736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_738" n="HIAT:w" s="T222">hüːs</ts>
                  <nts id="Seg_739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_741" n="HIAT:w" s="T223">bombalarɨ</ts>
                  <nts id="Seg_742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_744" n="HIAT:w" s="T224">keːheːčči</ts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_747" n="HIAT:w" s="T225">ete</ts>
                  <nts id="Seg_748" n="HIAT:ip">.</nts>
                  <nts id="Seg_749" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T239" id="Seg_751" n="HIAT:u" s="T226">
                  <ts e="T227" id="Seg_753" n="HIAT:w" s="T226">Onton</ts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_756" n="HIAT:w" s="T227">tuspa</ts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_759" n="HIAT:w" s="T228">össü͡ö</ts>
                  <nts id="Seg_760" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_762" n="HIAT:w" s="T229">ɨtɨ͡alɨːr</ts>
                  <nts id="Seg_763" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_765" n="HIAT:w" s="T230">ete</ts>
                  <nts id="Seg_766" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_768" n="HIAT:w" s="T231">töhö</ts>
                  <nts id="Seg_769" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_771" n="HIAT:w" s="T232">emete</ts>
                  <nts id="Seg_772" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_774" n="HIAT:w" s="T233">sarpa</ts>
                  <nts id="Seg_775" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_777" n="HIAT:w" s="T234">kördük</ts>
                  <nts id="Seg_778" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_780" n="HIAT:w" s="T235">iːtiːleːk</ts>
                  <nts id="Seg_781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_783" n="HIAT:w" s="T236">seriː</ts>
                  <nts id="Seg_784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_786" n="HIAT:w" s="T237">saːtɨnan</ts>
                  <nts id="Seg_787" n="HIAT:ip">–</nts>
                  <nts id="Seg_788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_790" n="HIAT:w" s="T238">pulʼemʼotɨnan</ts>
                  <nts id="Seg_791" n="HIAT:ip">.</nts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T257" id="Seg_794" n="HIAT:u" s="T239">
                  <ts e="T240" id="Seg_796" n="HIAT:w" s="T239">Itiččeleːk</ts>
                  <nts id="Seg_797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_799" n="HIAT:w" s="T240">bomba</ts>
                  <nts id="Seg_800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_802" n="HIAT:w" s="T241">tüspütün</ts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_805" n="HIAT:w" s="T242">kenne</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_808" n="HIAT:w" s="T243">itiččeleːk</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_811" n="HIAT:w" s="T244">saː</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_814" n="HIAT:w" s="T245">ɨtɨ͡alaːbɨtɨn</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_817" n="HIAT:w" s="T246">kenne</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_820" n="HIAT:w" s="T247">sir</ts>
                  <nts id="Seg_821" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_823" n="HIAT:w" s="T248">barɨta</ts>
                  <nts id="Seg_824" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_826" n="HIAT:w" s="T249">tu͡okkaːn</ts>
                  <nts id="Seg_827" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_829" n="HIAT:w" s="T250">da</ts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_832" n="HIAT:w" s="T251">köstübet</ts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_835" n="HIAT:w" s="T252">kördük</ts>
                  <nts id="Seg_836" n="HIAT:ip">,</nts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_839" n="HIAT:w" s="T253">kara</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_842" n="HIAT:w" s="T254">tuman</ts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_845" n="HIAT:w" s="T255">bu͡olar</ts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_848" n="HIAT:w" s="T256">ete</ts>
                  <nts id="Seg_849" n="HIAT:ip">.</nts>
                  <nts id="Seg_850" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T266" id="Seg_852" n="HIAT:u" s="T257">
                  <ts e="T258" id="Seg_854" n="HIAT:w" s="T257">Iti</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_857" n="HIAT:w" s="T258">tumaŋŋa</ts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_860" n="HIAT:w" s="T259">kihi</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_863" n="HIAT:w" s="T260">attɨgar</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_866" n="HIAT:w" s="T261">sɨtar</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_869" n="HIAT:w" s="T262">kihitin</ts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_872" n="HIAT:w" s="T263">koton</ts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_875" n="HIAT:w" s="T264">körböt</ts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_878" n="HIAT:w" s="T265">ete</ts>
                  <nts id="Seg_879" n="HIAT:ip">.</nts>
                  <nts id="Seg_880" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T279" id="Seg_882" n="HIAT:u" s="T266">
                  <ts e="T267" id="Seg_884" n="HIAT:w" s="T266">Sorok</ts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_887" n="HIAT:w" s="T267">kenne</ts>
                  <nts id="Seg_888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_890" n="HIAT:w" s="T268">kihi</ts>
                  <nts id="Seg_891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_893" n="HIAT:w" s="T269">seriː</ts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_896" n="HIAT:w" s="T270">saːtɨn</ts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_899" n="HIAT:w" s="T271">tɨ͡ahɨttan</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_902" n="HIAT:w" s="T272">tu͡okkaːnɨ</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_905" n="HIAT:w" s="T273">da</ts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_908" n="HIAT:w" s="T274">istibet</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_911" n="HIAT:w" s="T275">kördük</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_914" n="HIAT:w" s="T276">dülej</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_917" n="HIAT:w" s="T277">bu͡olar</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_920" n="HIAT:w" s="T278">ete</ts>
                  <nts id="Seg_921" n="HIAT:ip">.</nts>
                  <nts id="Seg_922" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T288" id="Seg_924" n="HIAT:u" s="T279">
                  <ts e="T280" id="Seg_926" n="HIAT:w" s="T279">Onus</ts>
                  <nts id="Seg_927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_929" n="HIAT:w" s="T280">kümmütüger</ts>
                  <nts id="Seg_930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_932" n="HIAT:w" s="T281">bihigi</ts>
                  <nts id="Seg_933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_935" n="HIAT:w" s="T282">di͡egi</ts>
                  <nts id="Seg_936" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T284" id="Seg_938" n="HIAT:w" s="T283">öttünnen</ts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_941" n="HIAT:w" s="T284">seriːni</ts>
                  <nts id="Seg_942" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_944" n="HIAT:w" s="T285">olus</ts>
                  <nts id="Seg_945" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_947" n="HIAT:w" s="T286">küːsteːktik</ts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_950" n="HIAT:w" s="T287">barda</ts>
                  <nts id="Seg_951" n="HIAT:ip">.</nts>
                  <nts id="Seg_952" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T303" id="Seg_954" n="HIAT:u" s="T288">
                  <ts e="T289" id="Seg_956" n="HIAT:w" s="T288">Kallaːntan</ts>
                  <nts id="Seg_957" n="HIAT:ip">,</nts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_960" n="HIAT:w" s="T289">sirten</ts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_963" n="HIAT:w" s="T290">bert</ts>
                  <nts id="Seg_964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_966" n="HIAT:w" s="T291">elbek</ts>
                  <nts id="Seg_967" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_969" n="HIAT:w" s="T292">samalʼottar</ts>
                  <nts id="Seg_970" n="HIAT:ip">,</nts>
                  <nts id="Seg_971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_973" n="HIAT:w" s="T293">tankalar</ts>
                  <nts id="Seg_974" n="HIAT:ip">,</nts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_977" n="HIAT:w" s="T294">anaːn</ts>
                  <nts id="Seg_978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_980" n="HIAT:w" s="T295">seriːge</ts>
                  <nts id="Seg_981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_983" n="HIAT:w" s="T296">oŋohullubut</ts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_986" n="HIAT:w" s="T297">u͡ottaːk</ts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_989" n="HIAT:w" s="T298">saːlar</ts>
                  <nts id="Seg_990" n="HIAT:ip">,</nts>
                  <nts id="Seg_991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_992" n="HIAT:ip">"</nts>
                  <ts e="T300" id="Seg_994" n="HIAT:w" s="T299">Katʼuša</ts>
                  <nts id="Seg_995" n="HIAT:ip">"</nts>
                  <nts id="Seg_996" n="HIAT:ip">,</nts>
                  <nts id="Seg_997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_998" n="HIAT:ip">"</nts>
                  <ts e="T301" id="Seg_1000" n="HIAT:w" s="T300">Andrʼuša</ts>
                  <nts id="Seg_1001" n="HIAT:ip">"</nts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1004" n="HIAT:w" s="T301">di͡en</ts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1007" n="HIAT:w" s="T302">aːttaːktar</ts>
                  <nts id="Seg_1008" n="HIAT:ip">.</nts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T309" id="Seg_1011" n="HIAT:u" s="T303">
                  <ts e="T304" id="Seg_1013" n="HIAT:w" s="T303">Onton</ts>
                  <nts id="Seg_1014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1016" n="HIAT:w" s="T304">tuspa</ts>
                  <nts id="Seg_1017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1019" n="HIAT:w" s="T305">töhö</ts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1022" n="HIAT:w" s="T306">eme</ts>
                  <nts id="Seg_1023" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1025" n="HIAT:w" s="T307">kɨra</ts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1028" n="HIAT:w" s="T308">saːlar</ts>
                  <nts id="Seg_1029" n="HIAT:ip">.</nts>
                  <nts id="Seg_1030" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T316" id="Seg_1032" n="HIAT:u" s="T309">
                  <ts e="T310" id="Seg_1034" n="HIAT:w" s="T309">Ol</ts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1037" n="HIAT:w" s="T310">barɨta</ts>
                  <nts id="Seg_1038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1040" n="HIAT:w" s="T311">biːrge</ts>
                  <nts id="Seg_1041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1043" n="HIAT:w" s="T312">ogustular</ts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1046" n="HIAT:w" s="T313">nʼemʼester</ts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1049" n="HIAT:w" s="T314">baːr</ts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1052" n="HIAT:w" s="T315">hirderin</ts>
                  <nts id="Seg_1053" n="HIAT:ip">.</nts>
                  <nts id="Seg_1054" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T325" id="Seg_1056" n="HIAT:u" s="T316">
                  <ts e="T317" id="Seg_1058" n="HIAT:w" s="T316">Nʼemʼes</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1061" n="HIAT:w" s="T317">armʼijata</ts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1064" n="HIAT:w" s="T318">itini</ts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1067" n="HIAT:w" s="T319">dolgunnaːk</ts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1070" n="HIAT:w" s="T320">ulakan</ts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1073" n="HIAT:w" s="T321">oksuːnu</ts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1076" n="HIAT:w" s="T322">tulujbakka</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1079" n="HIAT:w" s="T323">töttörü</ts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1082" n="HIAT:w" s="T324">supturujbuta</ts>
                  <nts id="Seg_1083" n="HIAT:ip">.</nts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T332" id="Seg_1086" n="HIAT:u" s="T325">
                  <ts e="T326" id="Seg_1088" n="HIAT:w" s="T325">Sonnokoːn</ts>
                  <nts id="Seg_1089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1091" n="HIAT:w" s="T326">u͡on</ts>
                  <nts id="Seg_1092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1094" n="HIAT:w" s="T327">bi͡es</ts>
                  <nts id="Seg_1095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1097" n="HIAT:w" s="T328">beristeleːk</ts>
                  <nts id="Seg_1098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1100" n="HIAT:w" s="T329">siri</ts>
                  <nts id="Seg_1101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1103" n="HIAT:w" s="T330">töttörü</ts>
                  <nts id="Seg_1104" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1106" n="HIAT:w" s="T331">ɨstammɨta</ts>
                  <nts id="Seg_1107" n="HIAT:ip">.</nts>
                  <nts id="Seg_1108" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T341" id="Seg_1110" n="HIAT:u" s="T332">
                  <ts e="T333" id="Seg_1112" n="HIAT:w" s="T332">Iti</ts>
                  <nts id="Seg_1113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1115" n="HIAT:w" s="T333">ulakan</ts>
                  <nts id="Seg_1116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1118" n="HIAT:w" s="T334">oksuː</ts>
                  <nts id="Seg_1119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1121" n="HIAT:w" s="T335">kennitten</ts>
                  <nts id="Seg_1122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1124" n="HIAT:w" s="T336">Kurskaj</ts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1127" n="HIAT:w" s="T337">di͡en</ts>
                  <nts id="Seg_1128" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1130" n="HIAT:w" s="T338">tokura</ts>
                  <nts id="Seg_1131" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1133" n="HIAT:w" s="T339">berke</ts>
                  <nts id="Seg_1134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1136" n="HIAT:w" s="T340">kömmüte</ts>
                  <nts id="Seg_1137" n="HIAT:ip">.</nts>
                  <nts id="Seg_1138" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T346" id="Seg_1140" n="HIAT:u" s="T341">
                  <ts e="T342" id="Seg_1142" n="HIAT:w" s="T341">Nʼemʼes</ts>
                  <nts id="Seg_1143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1145" n="HIAT:w" s="T342">armʼijatɨn</ts>
                  <nts id="Seg_1146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1148" n="HIAT:w" s="T343">töttörü</ts>
                  <nts id="Seg_1149" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1151" n="HIAT:w" s="T344">bihigi</ts>
                  <nts id="Seg_1152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1154" n="HIAT:w" s="T345">bappɨppɨt</ts>
                  <nts id="Seg_1155" n="HIAT:ip">.</nts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T363" id="Seg_1158" n="HIAT:u" s="T346">
                  <ts e="T347" id="Seg_1160" n="HIAT:w" s="T346">Iti</ts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1163" n="HIAT:w" s="T347">kennitten</ts>
                  <nts id="Seg_1164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1166" n="HIAT:w" s="T348">nʼemʼester</ts>
                  <nts id="Seg_1167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1169" n="HIAT:w" s="T349">hereges</ts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1172" n="HIAT:w" s="T350">saldaːttara</ts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1175" n="HIAT:w" s="T351">innilerin</ts>
                  <nts id="Seg_1176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T353" id="Seg_1178" n="HIAT:w" s="T352">bihi͡eke</ts>
                  <nts id="Seg_1179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1181" n="HIAT:w" s="T353">bi͡eren</ts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1184" n="HIAT:w" s="T354">baraːn</ts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1187" n="HIAT:w" s="T355">suptu</ts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1190" n="HIAT:w" s="T356">seriː</ts>
                  <nts id="Seg_1191" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1193" n="HIAT:w" s="T357">bütü͡ögüger</ts>
                  <nts id="Seg_1194" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1196" n="HIAT:w" s="T358">di͡eri</ts>
                  <nts id="Seg_1197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1199" n="HIAT:w" s="T359">öjdörün-puːttarɨn</ts>
                  <nts id="Seg_1200" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1202" n="HIAT:w" s="T360">bulbataktara</ts>
                  <nts id="Seg_1203" n="HIAT:ip">,</nts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1206" n="HIAT:w" s="T361">ku͡otan</ts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1209" n="HIAT:w" s="T362">ispittere</ts>
                  <nts id="Seg_1210" n="HIAT:ip">.</nts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T376" id="Seg_1213" n="HIAT:u" s="T363">
                  <ts e="T364" id="Seg_1215" n="HIAT:w" s="T363">Bihigi</ts>
                  <nts id="Seg_1216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1218" n="HIAT:w" s="T364">armʼijabɨt</ts>
                  <nts id="Seg_1219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1221" n="HIAT:w" s="T365">bert</ts>
                  <nts id="Seg_1222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1224" n="HIAT:w" s="T366">üčügejdik</ts>
                  <nts id="Seg_1225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1227" n="HIAT:w" s="T367">küːhün</ts>
                  <nts id="Seg_1228" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1230" n="HIAT:w" s="T368">tutan</ts>
                  <nts id="Seg_1231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1233" n="HIAT:w" s="T369">baraːn</ts>
                  <nts id="Seg_1234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1236" n="HIAT:w" s="T370">nʼemʼesteri</ts>
                  <nts id="Seg_1237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1239" n="HIAT:w" s="T371">kenni</ts>
                  <nts id="Seg_1240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1242" n="HIAT:w" s="T372">di͡ek</ts>
                  <nts id="Seg_1243" n="HIAT:ip">,</nts>
                  <nts id="Seg_1244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1246" n="HIAT:w" s="T373">köllörbökkö</ts>
                  <nts id="Seg_1247" n="HIAT:ip">,</nts>
                  <nts id="Seg_1248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1250" n="HIAT:w" s="T374">delbi</ts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1253" n="HIAT:w" s="T375">bappɨta</ts>
                  <nts id="Seg_1254" n="HIAT:ip">.</nts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T387" id="Seg_1257" n="HIAT:u" s="T376">
                  <ts e="T377" id="Seg_1259" n="HIAT:w" s="T376">Min</ts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1262" n="HIAT:w" s="T377">kojukku</ts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1265" n="HIAT:w" s="T378">künnerge</ts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1268" n="HIAT:w" s="T379">seriːge</ts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1271" n="HIAT:w" s="T380">hɨldʼammɨn</ts>
                  <nts id="Seg_1272" n="HIAT:ip">,</nts>
                  <nts id="Seg_1273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1275" n="HIAT:w" s="T381">itinnik</ts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1278" n="HIAT:w" s="T382">sürdeːk</ts>
                  <nts id="Seg_1279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1281" n="HIAT:w" s="T383">kihi</ts>
                  <nts id="Seg_1282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1283" n="HIAT:ip">(</nts>
                  <nts id="Seg_1284" n="HIAT:ip">(</nts>
                  <ats e="T952" id="Seg_1285" n="HIAT:non-pho" s="T384">…</ats>
                  <nts id="Seg_1286" n="HIAT:ip">)</nts>
                  <nts id="Seg_1287" n="HIAT:ip">)</nts>
                  <nts id="Seg_1288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1290" n="HIAT:w" s="T952">kördük</ts>
                  <nts id="Seg_1291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1293" n="HIAT:w" s="T385">körbötögüm</ts>
                  <nts id="Seg_1294" n="HIAT:ip">.</nts>
                  <nts id="Seg_1295" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T400" id="Seg_1297" n="HIAT:u" s="T387">
                  <ts e="T388" id="Seg_1299" n="HIAT:w" s="T387">Min</ts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1302" n="HIAT:w" s="T388">iti</ts>
                  <nts id="Seg_1303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1305" n="HIAT:w" s="T389">hirge</ts>
                  <nts id="Seg_1306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1308" n="HIAT:w" s="T390">saŋardɨː</ts>
                  <nts id="Seg_1309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1311" n="HIAT:w" s="T391">körbütüm</ts>
                  <nts id="Seg_1312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1314" n="HIAT:w" s="T392">hu͡ostaːk</ts>
                  <nts id="Seg_1315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1317" n="HIAT:w" s="T393">kersiːni</ts>
                  <nts id="Seg_1318" n="HIAT:ip">,</nts>
                  <nts id="Seg_1319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1321" n="HIAT:w" s="T394">kajtak</ts>
                  <nts id="Seg_1322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1324" n="HIAT:w" s="T395">seriː</ts>
                  <nts id="Seg_1325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1327" n="HIAT:w" s="T396">kihitin</ts>
                  <nts id="Seg_1328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1330" n="HIAT:w" s="T397">kaːna</ts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1333" n="HIAT:w" s="T398">ürektiː</ts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1336" n="HIAT:w" s="T399">toktorun</ts>
                  <nts id="Seg_1337" n="HIAT:ip">.</nts>
                  <nts id="Seg_1338" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T405" id="Seg_1340" n="HIAT:u" s="T400">
                  <ts e="T401" id="Seg_1342" n="HIAT:w" s="T400">Töhölöːk</ts>
                  <nts id="Seg_1343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1345" n="HIAT:w" s="T401">tɨːhɨčča</ts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1348" n="HIAT:w" s="T402">kihi</ts>
                  <nts id="Seg_1349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1351" n="HIAT:w" s="T403">tɨːna</ts>
                  <nts id="Seg_1352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1354" n="HIAT:w" s="T404">bɨstarɨn</ts>
                  <nts id="Seg_1355" n="HIAT:ip">.</nts>
                  <nts id="Seg_1356" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T411" id="Seg_1358" n="HIAT:u" s="T405">
                  <ts e="T406" id="Seg_1360" n="HIAT:w" s="T405">töhölöːk</ts>
                  <nts id="Seg_1361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1363" n="HIAT:w" s="T406">gojobuːn</ts>
                  <nts id="Seg_1364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1366" n="HIAT:w" s="T407">bu͡olbut</ts>
                  <nts id="Seg_1367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1369" n="HIAT:w" s="T408">kihi</ts>
                  <nts id="Seg_1370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1372" n="HIAT:w" s="T409">ulakannɨk</ts>
                  <nts id="Seg_1373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1375" n="HIAT:w" s="T410">erejdenerin</ts>
                  <nts id="Seg_1376" n="HIAT:ip">.</nts>
                  <nts id="Seg_1377" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T426" id="Seg_1379" n="HIAT:u" s="T411">
                  <ts e="T412" id="Seg_1381" n="HIAT:w" s="T411">Min</ts>
                  <nts id="Seg_1382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1384" n="HIAT:w" s="T412">emi͡e</ts>
                  <nts id="Seg_1385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1387" n="HIAT:w" s="T413">iti</ts>
                  <nts id="Seg_1388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1390" n="HIAT:w" s="T414">oksohuːga</ts>
                  <nts id="Seg_1391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1393" n="HIAT:w" s="T415">Kurskaj</ts>
                  <nts id="Seg_1394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1396" n="HIAT:w" s="T416">tumuhugar</ts>
                  <nts id="Seg_1397" n="HIAT:ip">,</nts>
                  <nts id="Seg_1398" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1400" n="HIAT:w" s="T417">atdʼeleʼnʼija</ts>
                  <nts id="Seg_1401" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1403" n="HIAT:w" s="T418">kamandʼira</ts>
                  <nts id="Seg_1404" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1406" n="HIAT:w" s="T419">bu͡ola</ts>
                  <nts id="Seg_1407" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1409" n="HIAT:w" s="T420">hɨldʼammɨn</ts>
                  <nts id="Seg_1410" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1412" n="HIAT:w" s="T421">dʼogustuk</ts>
                  <nts id="Seg_1413" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1415" n="HIAT:w" s="T422">kaŋas</ts>
                  <nts id="Seg_1416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1418" n="HIAT:w" s="T423">atakpɨn</ts>
                  <nts id="Seg_1419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1421" n="HIAT:w" s="T424">bɨlčɨŋɨ</ts>
                  <nts id="Seg_1422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1424" n="HIAT:w" s="T425">gojobuːnnappɨtɨm</ts>
                  <nts id="Seg_1425" n="HIAT:ip">.</nts>
                  <nts id="Seg_1426" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T432" id="Seg_1428" n="HIAT:u" s="T426">
                  <ts e="T427" id="Seg_1430" n="HIAT:w" s="T426">Ol</ts>
                  <nts id="Seg_1431" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1433" n="HIAT:w" s="T427">bu͡olan</ts>
                  <nts id="Seg_1434" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1436" n="HIAT:w" s="T428">baraːn</ts>
                  <nts id="Seg_1437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1439" n="HIAT:w" s="T429">seriːleher</ts>
                  <nts id="Seg_1440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1442" n="HIAT:w" s="T430">sirten</ts>
                  <nts id="Seg_1443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1445" n="HIAT:w" s="T431">taksɨbatagɨm</ts>
                  <nts id="Seg_1446" n="HIAT:ip">.</nts>
                  <nts id="Seg_1447" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T452" id="Seg_1449" n="HIAT:u" s="T432">
                  <ts e="T433" id="Seg_1451" n="HIAT:w" s="T432">Iti</ts>
                  <nts id="Seg_1452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1454" n="HIAT:w" s="T433">sirdeːk</ts>
                  <nts id="Seg_1455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1457" n="HIAT:w" s="T434">oksohuːga</ts>
                  <nts id="Seg_1458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1460" n="HIAT:w" s="T435">min</ts>
                  <nts id="Seg_1461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1463" n="HIAT:w" s="T436">baːr</ts>
                  <nts id="Seg_1464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1466" n="HIAT:w" s="T437">agaj</ts>
                  <nts id="Seg_1467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1469" n="HIAT:w" s="T438">čaːhɨm</ts>
                  <nts id="Seg_1470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1472" n="HIAT:w" s="T439">tɨːhɨččattan</ts>
                  <nts id="Seg_1473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1475" n="HIAT:w" s="T440">taksa</ts>
                  <nts id="Seg_1476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1478" n="HIAT:w" s="T441">seriː</ts>
                  <nts id="Seg_1479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1481" n="HIAT:w" s="T442">kihititten</ts>
                  <nts id="Seg_1482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1484" n="HIAT:w" s="T443">iti</ts>
                  <nts id="Seg_1485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1487" n="HIAT:w" s="T444">u͡on</ts>
                  <nts id="Seg_1488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1490" n="HIAT:w" s="T445">kuŋŋe</ts>
                  <nts id="Seg_1491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1493" n="HIAT:w" s="T446">oksohuːga</ts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1496" n="HIAT:w" s="T447">orputa</ts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1499" n="HIAT:w" s="T448">u͡on</ts>
                  <nts id="Seg_1500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1502" n="HIAT:w" s="T449">biːr</ts>
                  <nts id="Seg_1503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1505" n="HIAT:w" s="T450">agaj</ts>
                  <nts id="Seg_1506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1508" n="HIAT:w" s="T451">kihi</ts>
                  <nts id="Seg_1509" n="HIAT:ip">.</nts>
                  <nts id="Seg_1510" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T462" id="Seg_1512" n="HIAT:u" s="T452">
                  <ts e="T453" id="Seg_1514" n="HIAT:w" s="T452">Iti</ts>
                  <nts id="Seg_1515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1517" n="HIAT:w" s="T453">ihiger</ts>
                  <nts id="Seg_1518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1520" n="HIAT:w" s="T454">össü͡ö</ts>
                  <nts id="Seg_1521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1523" n="HIAT:w" s="T455">biːr</ts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1526" n="HIAT:w" s="T456">min</ts>
                  <nts id="Seg_1527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T458" id="Seg_1529" n="HIAT:w" s="T457">gojobuːn</ts>
                  <nts id="Seg_1530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1532" n="HIAT:w" s="T458">bu͡olbut</ts>
                  <nts id="Seg_1533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1535" n="HIAT:w" s="T459">kihi</ts>
                  <nts id="Seg_1536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1538" n="HIAT:w" s="T460">baːr</ts>
                  <nts id="Seg_1539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1541" n="HIAT:w" s="T461">etim</ts>
                  <nts id="Seg_1542" n="HIAT:ip">.</nts>
                  <nts id="Seg_1543" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T478" id="Seg_1545" n="HIAT:u" s="T462">
                  <ts e="T463" id="Seg_1547" n="HIAT:w" s="T462">Itinten</ts>
                  <nts id="Seg_1548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1550" n="HIAT:w" s="T463">söp</ts>
                  <nts id="Seg_1551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1553" n="HIAT:w" s="T464">bu͡olu͡oga</ts>
                  <nts id="Seg_1554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1556" n="HIAT:w" s="T465">kolu͡okka</ts>
                  <nts id="Seg_1557" n="HIAT:ip">,</nts>
                  <nts id="Seg_1558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1560" n="HIAT:w" s="T466">kajtak</ts>
                  <nts id="Seg_1561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1563" n="HIAT:w" s="T467">aldʼarkajdaːk</ts>
                  <nts id="Seg_1564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1566" n="HIAT:w" s="T468">seriː</ts>
                  <nts id="Seg_1567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1569" n="HIAT:w" s="T469">etin</ts>
                  <nts id="Seg_1570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1572" n="HIAT:w" s="T470">Kurskaj</ts>
                  <nts id="Seg_1573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1575" n="HIAT:w" s="T471">togurugor</ts>
                  <nts id="Seg_1576" n="HIAT:ip">,</nts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1579" n="HIAT:w" s="T472">töhö</ts>
                  <nts id="Seg_1580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1582" n="HIAT:w" s="T473">kaːn</ts>
                  <nts id="Seg_1583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1585" n="HIAT:w" s="T474">toktubutun</ts>
                  <nts id="Seg_1586" n="HIAT:ip">,</nts>
                  <nts id="Seg_1587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1589" n="HIAT:w" s="T475">töhö</ts>
                  <nts id="Seg_1590" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1592" n="HIAT:w" s="T476">kihi</ts>
                  <nts id="Seg_1593" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T478" id="Seg_1595" n="HIAT:w" s="T477">ölbütün</ts>
                  <nts id="Seg_1596" n="HIAT:ip">.</nts>
                  <nts id="Seg_1597" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T490" id="Seg_1599" n="HIAT:u" s="T478">
                  <ts e="T479" id="Seg_1601" n="HIAT:w" s="T478">Kurskajga</ts>
                  <nts id="Seg_1602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1604" n="HIAT:w" s="T479">büten</ts>
                  <nts id="Seg_1605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1607" n="HIAT:w" s="T480">baraːn</ts>
                  <nts id="Seg_1608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1610" n="HIAT:w" s="T481">bihigi</ts>
                  <nts id="Seg_1611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1613" n="HIAT:w" s="T482">armʼijabɨt</ts>
                  <nts id="Seg_1614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1616" n="HIAT:w" s="T483">töttörü</ts>
                  <nts id="Seg_1617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1619" n="HIAT:w" s="T484">kürüːr</ts>
                  <nts id="Seg_1620" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1622" n="HIAT:w" s="T485">nʼemʼehi</ts>
                  <nts id="Seg_1623" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1625" n="HIAT:w" s="T486">zaːpad</ts>
                  <nts id="Seg_1626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1628" n="HIAT:w" s="T487">di͡ek</ts>
                  <nts id="Seg_1629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1631" n="HIAT:w" s="T488">batan</ts>
                  <nts id="Seg_1632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1634" n="HIAT:w" s="T489">barbɨta</ts>
                  <nts id="Seg_1635" n="HIAT:ip">.</nts>
                  <nts id="Seg_1636" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T510" id="Seg_1638" n="HIAT:u" s="T490">
                  <ts e="T491" id="Seg_1640" n="HIAT:w" s="T490">Iti</ts>
                  <nts id="Seg_1641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1643" n="HIAT:w" s="T491">Kurskaj</ts>
                  <nts id="Seg_1644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1646" n="HIAT:w" s="T492">kennitten</ts>
                  <nts id="Seg_1647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1649" n="HIAT:w" s="T493">min</ts>
                  <nts id="Seg_1650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1652" n="HIAT:w" s="T494">saːmaj</ts>
                  <nts id="Seg_1653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1655" n="HIAT:w" s="T495">seriː</ts>
                  <nts id="Seg_1656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1657" n="HIAT:ip">(</nts>
                  <ts e="T497" id="Seg_1659" n="HIAT:w" s="T496">kerse</ts>
                  <nts id="Seg_1660" n="HIAT:ip">)</nts>
                  <nts id="Seg_1661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1663" n="HIAT:w" s="T497">siriger</ts>
                  <nts id="Seg_1664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1666" n="HIAT:w" s="T498">baːr</ts>
                  <nts id="Seg_1667" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1669" n="HIAT:w" s="T499">etim</ts>
                  <nts id="Seg_1670" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1672" n="HIAT:w" s="T500">tɨːhɨčča</ts>
                  <nts id="Seg_1673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1675" n="HIAT:w" s="T501">togus</ts>
                  <nts id="Seg_1676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1678" n="HIAT:w" s="T502">süːs</ts>
                  <nts id="Seg_1679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1681" n="HIAT:w" s="T503">tü͡örd-u͡on</ts>
                  <nts id="Seg_1682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1684" n="HIAT:w" s="T504">tü͡ört</ts>
                  <nts id="Seg_1685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1687" n="HIAT:w" s="T505">dʼɨllaːkka</ts>
                  <nts id="Seg_1688" n="HIAT:ip">,</nts>
                  <nts id="Seg_1689" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1691" n="HIAT:w" s="T506">kɨhɨŋŋɨ</ts>
                  <nts id="Seg_1692" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1694" n="HIAT:w" s="T507">janvarʼ</ts>
                  <nts id="Seg_1695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1697" n="HIAT:w" s="T508">ɨjga</ts>
                  <nts id="Seg_1698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1700" n="HIAT:w" s="T509">di͡eri</ts>
                  <nts id="Seg_1701" n="HIAT:ip">.</nts>
                  <nts id="Seg_1702" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T538" id="Seg_1704" n="HIAT:u" s="T510">
                  <ts e="T511" id="Seg_1706" n="HIAT:w" s="T510">Iti</ts>
                  <nts id="Seg_1707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1709" n="HIAT:w" s="T511">kemŋe</ts>
                  <nts id="Seg_1710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1712" n="HIAT:w" s="T512">nʼemʼes</ts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1715" n="HIAT:w" s="T513">saldaːttarɨn</ts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1718" n="HIAT:w" s="T514">gɨtta</ts>
                  <nts id="Seg_1719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1721" n="HIAT:w" s="T515">utara</ts>
                  <nts id="Seg_1722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1724" n="HIAT:w" s="T516">turan</ts>
                  <nts id="Seg_1725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1727" n="HIAT:w" s="T517">seriːlespitim</ts>
                  <nts id="Seg_1728" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1730" n="HIAT:w" s="T518">munnuk</ts>
                  <nts id="Seg_1731" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1733" n="HIAT:w" s="T519">sirderge</ts>
                  <nts id="Seg_1734" n="HIAT:ip">,</nts>
                  <nts id="Seg_1735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1737" n="HIAT:w" s="T520">Bʼelgarad</ts>
                  <nts id="Seg_1738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1740" n="HIAT:w" s="T521">onton</ts>
                  <nts id="Seg_1741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1743" n="HIAT:w" s="T522">Xarʼkav</ts>
                  <nts id="Seg_1744" n="HIAT:ip">,</nts>
                  <nts id="Seg_1745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1747" n="HIAT:w" s="T523">onton</ts>
                  <nts id="Seg_1748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1750" n="HIAT:w" s="T524">Poltava</ts>
                  <nts id="Seg_1751" n="HIAT:ip">,</nts>
                  <nts id="Seg_1752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1754" n="HIAT:w" s="T525">onton</ts>
                  <nts id="Seg_1755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1757" n="HIAT:w" s="T526">Krʼemʼenčʼug</ts>
                  <nts id="Seg_1758" n="HIAT:ip">,</nts>
                  <nts id="Seg_1759" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1761" n="HIAT:w" s="T527">onton</ts>
                  <nts id="Seg_1762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1764" n="HIAT:w" s="T528">Krʼukov</ts>
                  <nts id="Seg_1765" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_1767" n="HIAT:w" s="T529">di͡en</ts>
                  <nts id="Seg_1768" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_1770" n="HIAT:w" s="T530">aːttaːk</ts>
                  <nts id="Seg_1771" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_1773" n="HIAT:w" s="T531">ulakan</ts>
                  <nts id="Seg_1774" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_1776" n="HIAT:w" s="T532">gu͡orattarɨ</ts>
                  <nts id="Seg_1777" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1779" n="HIAT:w" s="T533">ɨlarga</ts>
                  <nts id="Seg_1780" n="HIAT:ip">,</nts>
                  <nts id="Seg_1781" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_1783" n="HIAT:w" s="T534">ukrainʼes</ts>
                  <nts id="Seg_1784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1786" n="HIAT:w" s="T535">di͡en</ts>
                  <nts id="Seg_1787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_1789" n="HIAT:w" s="T536">omuk</ts>
                  <nts id="Seg_1790" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_1792" n="HIAT:w" s="T537">siriger</ts>
                  <nts id="Seg_1793" n="HIAT:ip">.</nts>
                  <nts id="Seg_1794" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T549" id="Seg_1796" n="HIAT:u" s="T538">
                  <ts e="T539" id="Seg_1798" n="HIAT:w" s="T538">Iti</ts>
                  <nts id="Seg_1799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1801" n="HIAT:w" s="T539">gu͡orattartan</ts>
                  <nts id="Seg_1802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_1804" n="HIAT:w" s="T540">olus</ts>
                  <nts id="Seg_1805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_1807" n="HIAT:w" s="T541">kɨtaːnaktɨk</ts>
                  <nts id="Seg_1808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_1810" n="HIAT:w" s="T542">bihigi</ts>
                  <nts id="Seg_1811" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_1813" n="HIAT:w" s="T543">seriːlespippit</ts>
                  <nts id="Seg_1814" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_1816" n="HIAT:w" s="T544">nʼemʼehi</ts>
                  <nts id="Seg_1817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_1819" n="HIAT:w" s="T545">gɨtta</ts>
                  <nts id="Seg_1820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_1822" n="HIAT:w" s="T546">Xarʼkav</ts>
                  <nts id="Seg_1823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_1825" n="HIAT:w" s="T547">gu͡orat</ts>
                  <nts id="Seg_1826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_1828" n="HIAT:w" s="T548">bɨldʼahɨːga</ts>
                  <nts id="Seg_1829" n="HIAT:ip">.</nts>
                  <nts id="Seg_1830" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T558" id="Seg_1832" n="HIAT:u" s="T549">
                  <ts e="T550" id="Seg_1834" n="HIAT:w" s="T549">Iti</ts>
                  <nts id="Seg_1835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_1837" n="HIAT:w" s="T550">gu͡orat</ts>
                  <nts id="Seg_1838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_1840" n="HIAT:w" s="T551">üske</ts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_1843" n="HIAT:w" s="T552">di͡eri</ts>
                  <nts id="Seg_1844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553.tx.1" id="Seg_1846" n="HIAT:w" s="T553">töttörü</ts>
                  <nts id="Seg_1847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_1849" n="HIAT:w" s="T553.tx.1">taːrɨ</ts>
                  <nts id="Seg_1850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_1852" n="HIAT:w" s="T554">iliːtten</ts>
                  <nts id="Seg_1853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_1855" n="HIAT:w" s="T555">iliːge</ts>
                  <nts id="Seg_1856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T557" id="Seg_1858" n="HIAT:w" s="T556">kele</ts>
                  <nts id="Seg_1859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_1861" n="HIAT:w" s="T557">hɨldʼɨbɨta</ts>
                  <nts id="Seg_1862" n="HIAT:ip">.</nts>
                  <nts id="Seg_1863" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T564" id="Seg_1865" n="HIAT:u" s="T558">
                  <ts e="T559" id="Seg_1867" n="HIAT:w" s="T558">Onton</ts>
                  <nts id="Seg_1868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_1870" n="HIAT:w" s="T559">ühüs</ts>
                  <nts id="Seg_1871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_1873" n="HIAT:w" s="T560">keliːtiger</ts>
                  <nts id="Seg_1874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_1876" n="HIAT:w" s="T561">bihigi</ts>
                  <nts id="Seg_1877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_1879" n="HIAT:w" s="T562">iliːbitten</ts>
                  <nts id="Seg_1880" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_1882" n="HIAT:w" s="T563">ɨːppatakpɨt</ts>
                  <nts id="Seg_1883" n="HIAT:ip">.</nts>
                  <nts id="Seg_1884" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T575" id="Seg_1886" n="HIAT:u" s="T564">
                  <ts e="T565" id="Seg_1888" n="HIAT:w" s="T564">Onton</ts>
                  <nts id="Seg_1889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_1891" n="HIAT:w" s="T565">ulakan</ts>
                  <nts id="Seg_1892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_1894" n="HIAT:w" s="T566">oksohuː</ts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_1897" n="HIAT:w" s="T567">össü͡ö</ts>
                  <nts id="Seg_1898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_1900" n="HIAT:w" s="T568">baːr</ts>
                  <nts id="Seg_1901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_1903" n="HIAT:w" s="T569">ete</ts>
                  <nts id="Seg_1904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_1906" n="HIAT:w" s="T570">Dnʼepr</ts>
                  <nts id="Seg_1907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_1909" n="HIAT:w" s="T571">di͡en</ts>
                  <nts id="Seg_1910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_1912" n="HIAT:w" s="T572">aːttaːk</ts>
                  <nts id="Seg_1913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_1915" n="HIAT:w" s="T573">ebe</ts>
                  <nts id="Seg_1916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_1918" n="HIAT:w" s="T574">kehiːtiger</ts>
                  <nts id="Seg_1919" n="HIAT:ip">.</nts>
                  <nts id="Seg_1920" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T589" id="Seg_1922" n="HIAT:u" s="T575">
                  <ts e="T576" id="Seg_1924" n="HIAT:w" s="T575">Iti</ts>
                  <nts id="Seg_1925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T577" id="Seg_1927" n="HIAT:w" s="T576">ebe</ts>
                  <nts id="Seg_1928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_1930" n="HIAT:w" s="T577">oŋu͡orgu</ts>
                  <nts id="Seg_1931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_1933" n="HIAT:w" s="T578">öttütüger</ts>
                  <nts id="Seg_1934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_1936" n="HIAT:w" s="T579">oloron</ts>
                  <nts id="Seg_1937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_1939" n="HIAT:w" s="T580">nʼemʼester</ts>
                  <nts id="Seg_1940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_1942" n="HIAT:w" s="T581">künüsteri-tüːnneri</ts>
                  <nts id="Seg_1943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_1945" n="HIAT:w" s="T582">ketiː</ts>
                  <nts id="Seg_1946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_1948" n="HIAT:w" s="T583">hɨldʼannar</ts>
                  <nts id="Seg_1949" n="HIAT:ip">,</nts>
                  <nts id="Seg_1950" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_1952" n="HIAT:w" s="T584">bihigini</ts>
                  <nts id="Seg_1953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_1955" n="HIAT:w" s="T585">amattan</ts>
                  <nts id="Seg_1956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_1958" n="HIAT:w" s="T586">sataːn</ts>
                  <nts id="Seg_1959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_1961" n="HIAT:w" s="T587">keherpet</ts>
                  <nts id="Seg_1962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_1964" n="HIAT:w" s="T588">etilere</ts>
                  <nts id="Seg_1965" n="HIAT:ip">.</nts>
                  <nts id="Seg_1966" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T600" id="Seg_1968" n="HIAT:u" s="T589">
                  <ts e="T590" id="Seg_1970" n="HIAT:w" s="T589">Ol</ts>
                  <nts id="Seg_1971" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T591" id="Seg_1973" n="HIAT:w" s="T590">ihin</ts>
                  <nts id="Seg_1974" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_1976" n="HIAT:w" s="T591">bihigi</ts>
                  <nts id="Seg_1977" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_1979" n="HIAT:w" s="T592">kas</ts>
                  <nts id="Seg_1980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_1982" n="HIAT:w" s="T593">da</ts>
                  <nts id="Seg_1983" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_1985" n="HIAT:w" s="T594">künü</ts>
                  <nts id="Seg_1986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_1988" n="HIAT:w" s="T595">koton</ts>
                  <nts id="Seg_1989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_1991" n="HIAT:w" s="T596">taksɨbatakpɨt</ts>
                  <nts id="Seg_1992" n="HIAT:ip">,</nts>
                  <nts id="Seg_1993" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_1995" n="HIAT:w" s="T597">betereːgi</ts>
                  <nts id="Seg_1996" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_1998" n="HIAT:w" s="T598">kɨtɨlga</ts>
                  <nts id="Seg_1999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2001" n="HIAT:w" s="T599">olorbupput</ts>
                  <nts id="Seg_2002" n="HIAT:ip">.</nts>
                  <nts id="Seg_2003" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T616" id="Seg_2005" n="HIAT:u" s="T600">
                  <ts e="T601" id="Seg_2007" n="HIAT:w" s="T600">Ebeni</ts>
                  <nts id="Seg_2008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2010" n="HIAT:w" s="T601">taksar</ts>
                  <nts id="Seg_2011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2013" n="HIAT:w" s="T602">mostɨlarɨ</ts>
                  <nts id="Seg_2014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T604" id="Seg_2016" n="HIAT:w" s="T603">künüs</ts>
                  <nts id="Seg_2017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2019" n="HIAT:w" s="T604">oŋororbutun</ts>
                  <nts id="Seg_2020" n="HIAT:ip">,</nts>
                  <nts id="Seg_2021" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2023" n="HIAT:w" s="T605">nʼemʼester</ts>
                  <nts id="Seg_2024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2026" n="HIAT:w" s="T606">samalʼottara</ts>
                  <nts id="Seg_2027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2029" n="HIAT:w" s="T607">kötön</ts>
                  <nts id="Seg_2030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2032" n="HIAT:w" s="T608">kele-kele</ts>
                  <nts id="Seg_2033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2035" n="HIAT:w" s="T609">töhö</ts>
                  <nts id="Seg_2036" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2038" n="HIAT:w" s="T610">eme</ts>
                  <nts id="Seg_2039" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T612" id="Seg_2041" n="HIAT:w" s="T611">bombalarɨ</ts>
                  <nts id="Seg_2042" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2044" n="HIAT:w" s="T612">bɨragattaːn</ts>
                  <nts id="Seg_2045" n="HIAT:ip">,</nts>
                  <nts id="Seg_2046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2048" n="HIAT:w" s="T613">delbi</ts>
                  <nts id="Seg_2049" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2051" n="HIAT:w" s="T614">aldʼatar</ts>
                  <nts id="Seg_2052" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2054" n="HIAT:w" s="T615">ete</ts>
                  <nts id="Seg_2055" n="HIAT:ip">.</nts>
                  <nts id="Seg_2056" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T626" id="Seg_2058" n="HIAT:u" s="T616">
                  <ts e="T617" id="Seg_2060" n="HIAT:w" s="T616">Ol</ts>
                  <nts id="Seg_2061" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2063" n="HIAT:w" s="T617">ihin</ts>
                  <nts id="Seg_2064" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2066" n="HIAT:w" s="T618">kojut</ts>
                  <nts id="Seg_2067" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2069" n="HIAT:w" s="T619">bihigi</ts>
                  <nts id="Seg_2070" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2072" n="HIAT:w" s="T620">tüːn</ts>
                  <nts id="Seg_2073" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2075" n="HIAT:w" s="T621">kahu͡on</ts>
                  <nts id="Seg_2076" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2078" n="HIAT:w" s="T622">sirinen</ts>
                  <nts id="Seg_2079" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T624" id="Seg_2081" n="HIAT:w" s="T623">mostɨlarɨ</ts>
                  <nts id="Seg_2082" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2084" n="HIAT:w" s="T624">oŋortuːr</ts>
                  <nts id="Seg_2085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2087" n="HIAT:w" s="T625">etibit</ts>
                  <nts id="Seg_2088" n="HIAT:ip">.</nts>
                  <nts id="Seg_2089" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T634" id="Seg_2091" n="HIAT:u" s="T626">
                  <ts e="T627" id="Seg_2093" n="HIAT:w" s="T626">Abaːhɨ</ts>
                  <nts id="Seg_2094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2096" n="HIAT:w" s="T627">nʼemʼis</ts>
                  <nts id="Seg_2097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T629" id="Seg_2099" n="HIAT:w" s="T628">tüːn</ts>
                  <nts id="Seg_2100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T630" id="Seg_2102" n="HIAT:w" s="T629">daː</ts>
                  <nts id="Seg_2103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2105" n="HIAT:w" s="T630">oŋorbut</ts>
                  <nts id="Seg_2106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2108" n="HIAT:w" s="T631">mostɨlarbɨtɨn</ts>
                  <nts id="Seg_2109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2111" n="HIAT:w" s="T632">turu͡orbat</ts>
                  <nts id="Seg_2112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2114" n="HIAT:w" s="T633">ete</ts>
                  <nts id="Seg_2115" n="HIAT:ip">.</nts>
                  <nts id="Seg_2116" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T640" id="Seg_2118" n="HIAT:u" s="T634">
                  <ts e="T635" id="Seg_2120" n="HIAT:w" s="T634">Oččogo</ts>
                  <nts id="Seg_2121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2123" n="HIAT:w" s="T635">bihigi</ts>
                  <nts id="Seg_2124" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2126" n="HIAT:w" s="T636">di͡egiler</ts>
                  <nts id="Seg_2127" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2129" n="HIAT:w" s="T637">delbi</ts>
                  <nts id="Seg_2130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2132" n="HIAT:w" s="T638">kɨjŋanan</ts>
                  <nts id="Seg_2133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T640" id="Seg_2135" n="HIAT:w" s="T639">baraːn</ts>
                  <nts id="Seg_2136" n="HIAT:ip">.</nts>
                  <nts id="Seg_2137" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T646" id="Seg_2139" n="HIAT:u" s="T640">
                  <ts e="T641" id="Seg_2141" n="HIAT:w" s="T640">Barɨ</ts>
                  <nts id="Seg_2142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2144" n="HIAT:w" s="T641">seriː</ts>
                  <nts id="Seg_2145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2147" n="HIAT:w" s="T642">saːtɨnan</ts>
                  <nts id="Seg_2148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T644" id="Seg_2150" n="HIAT:w" s="T643">delbi</ts>
                  <nts id="Seg_2151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2153" n="HIAT:w" s="T644">ɨtɨ͡alaːn</ts>
                  <nts id="Seg_2154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2156" n="HIAT:w" s="T645">baraːn</ts>
                  <nts id="Seg_2157" n="HIAT:ip">.</nts>
                  <nts id="Seg_2158" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T654" id="Seg_2160" n="HIAT:u" s="T646">
                  <ts e="T647" id="Seg_2162" n="HIAT:w" s="T646">Töhö</ts>
                  <nts id="Seg_2163" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2165" n="HIAT:w" s="T647">eme</ts>
                  <nts id="Seg_2166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T649" id="Seg_2168" n="HIAT:w" s="T648">u͡onnuː</ts>
                  <nts id="Seg_2169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_2171" n="HIAT:w" s="T649">samalʼottarɨ</ts>
                  <nts id="Seg_2172" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_2174" n="HIAT:w" s="T650">oŋu͡orgu</ts>
                  <nts id="Seg_2175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T652" id="Seg_2177" n="HIAT:w" s="T651">öttüge</ts>
                  <nts id="Seg_2178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2180" n="HIAT:w" s="T652">ɨːtan</ts>
                  <nts id="Seg_2181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2183" n="HIAT:w" s="T653">baraːn</ts>
                  <nts id="Seg_2184" n="HIAT:ip">.</nts>
                  <nts id="Seg_2185" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T663" id="Seg_2187" n="HIAT:u" s="T654">
                  <ts e="T655" id="Seg_2189" n="HIAT:w" s="T654">Itini</ts>
                  <nts id="Seg_2190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2192" n="HIAT:w" s="T655">barɨtɨn</ts>
                  <nts id="Seg_2193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2195" n="HIAT:w" s="T656">bürünen</ts>
                  <nts id="Seg_2196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2198" n="HIAT:w" s="T657">turan</ts>
                  <nts id="Seg_2199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2201" n="HIAT:w" s="T658">oŋu͡orgu</ts>
                  <nts id="Seg_2202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2204" n="HIAT:w" s="T659">di͡ek</ts>
                  <nts id="Seg_2205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2207" n="HIAT:w" s="T660">bɨha</ts>
                  <nts id="Seg_2208" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2210" n="HIAT:w" s="T661">mökkühe</ts>
                  <nts id="Seg_2211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2213" n="HIAT:w" s="T662">taksɨbɨttara</ts>
                  <nts id="Seg_2214" n="HIAT:ip">.</nts>
                  <nts id="Seg_2215" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T670" id="Seg_2217" n="HIAT:u" s="T663">
                  <ts e="T664" id="Seg_2219" n="HIAT:w" s="T663">Bihigi</ts>
                  <nts id="Seg_2220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T665" id="Seg_2222" n="HIAT:w" s="T664">saldaːttarbɨt</ts>
                  <nts id="Seg_2223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2225" n="HIAT:w" s="T665">ol</ts>
                  <nts id="Seg_2226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2228" n="HIAT:w" s="T666">mökkü͡ön</ts>
                  <nts id="Seg_2229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2231" n="HIAT:w" s="T667">ajdaːnɨgar</ts>
                  <nts id="Seg_2232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2234" n="HIAT:w" s="T668">mostaga</ts>
                  <nts id="Seg_2235" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2237" n="HIAT:w" s="T669">bappakka</ts>
                  <nts id="Seg_2238" n="HIAT:ip">.</nts>
                  <nts id="Seg_2239" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T688" id="Seg_2241" n="HIAT:u" s="T670">
                  <ts e="T671" id="Seg_2243" n="HIAT:w" s="T670">Aŋar</ts>
                  <nts id="Seg_2244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2246" n="HIAT:w" s="T671">kihi</ts>
                  <nts id="Seg_2247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2249" n="HIAT:w" s="T672">lu͡otkannan</ts>
                  <nts id="Seg_2250" n="HIAT:ip">,</nts>
                  <nts id="Seg_2251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T674" id="Seg_2253" n="HIAT:w" s="T673">soroktor</ts>
                  <nts id="Seg_2254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T675" id="Seg_2256" n="HIAT:w" s="T674">kastɨː</ts>
                  <nts id="Seg_2257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T676" id="Seg_2259" n="HIAT:w" s="T675">emi͡e</ts>
                  <nts id="Seg_2260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T677" id="Seg_2262" n="HIAT:w" s="T676">bremnu͡olarɨ</ts>
                  <nts id="Seg_2263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T678" id="Seg_2265" n="HIAT:w" s="T677">biːrge</ts>
                  <nts id="Seg_2266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T679" id="Seg_2268" n="HIAT:w" s="T678">kolbuː</ts>
                  <nts id="Seg_2269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T680" id="Seg_2271" n="HIAT:w" s="T679">baːjan</ts>
                  <nts id="Seg_2272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T681" id="Seg_2274" n="HIAT:w" s="T680">baraːn</ts>
                  <nts id="Seg_2275" n="HIAT:ip">,</nts>
                  <nts id="Seg_2276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T682" id="Seg_2278" n="HIAT:w" s="T681">aŋardar</ts>
                  <nts id="Seg_2279" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T683" id="Seg_2281" n="HIAT:w" s="T682">köŋül</ts>
                  <nts id="Seg_2282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T684" id="Seg_2284" n="HIAT:w" s="T683">karbaːn</ts>
                  <nts id="Seg_2285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T685" id="Seg_2287" n="HIAT:w" s="T684">taksɨbɨttara</ts>
                  <nts id="Seg_2288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T686" id="Seg_2290" n="HIAT:w" s="T685">Dnʼepr</ts>
                  <nts id="Seg_2291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T687" id="Seg_2293" n="HIAT:w" s="T686">di͡en</ts>
                  <nts id="Seg_2294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T688" id="Seg_2296" n="HIAT:w" s="T687">ebeni</ts>
                  <nts id="Seg_2297" n="HIAT:ip">.</nts>
                  <nts id="Seg_2298" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T702" id="Seg_2300" n="HIAT:u" s="T688">
                  <ts e="T689" id="Seg_2302" n="HIAT:w" s="T688">Onno</ts>
                  <nts id="Seg_2303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T690" id="Seg_2305" n="HIAT:w" s="T689">ajdaːn</ts>
                  <nts id="Seg_2306" n="HIAT:ip">,</nts>
                  <nts id="Seg_2307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T691" id="Seg_2309" n="HIAT:w" s="T690">ulakan</ts>
                  <nts id="Seg_2310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T692" id="Seg_2312" n="HIAT:w" s="T691">ü͡ögüː</ts>
                  <nts id="Seg_2313" n="HIAT:ip">,</nts>
                  <nts id="Seg_2314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T693" id="Seg_2316" n="HIAT:w" s="T692">kahɨː</ts>
                  <nts id="Seg_2317" n="HIAT:ip">,</nts>
                  <nts id="Seg_2318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T694" id="Seg_2320" n="HIAT:w" s="T693">kersiː</ts>
                  <nts id="Seg_2321" n="HIAT:ip">,</nts>
                  <nts id="Seg_2322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T695" id="Seg_2324" n="HIAT:w" s="T694">tu͡oktan</ts>
                  <nts id="Seg_2325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T696" id="Seg_2327" n="HIAT:w" s="T695">da</ts>
                  <nts id="Seg_2328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T697" id="Seg_2330" n="HIAT:w" s="T696">atɨn</ts>
                  <nts id="Seg_2331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T698" id="Seg_2333" n="HIAT:w" s="T697">seriː</ts>
                  <nts id="Seg_2334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T699" id="Seg_2336" n="HIAT:w" s="T698">saːtɨn</ts>
                  <nts id="Seg_2337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T700" id="Seg_2339" n="HIAT:w" s="T699">tɨ͡aha</ts>
                  <nts id="Seg_2340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T701" id="Seg_2342" n="HIAT:w" s="T700">sürdeːk</ts>
                  <nts id="Seg_2343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T702" id="Seg_2345" n="HIAT:w" s="T701">ete</ts>
                  <nts id="Seg_2346" n="HIAT:ip">.</nts>
                  <nts id="Seg_2347" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T715" id="Seg_2349" n="HIAT:u" s="T702">
                  <ts e="T703" id="Seg_2351" n="HIAT:w" s="T702">Nʼemʼes</ts>
                  <nts id="Seg_2352" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T704" id="Seg_2354" n="HIAT:w" s="T703">di͡egi</ts>
                  <nts id="Seg_2355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T705" id="Seg_2357" n="HIAT:w" s="T704">öttü</ts>
                  <nts id="Seg_2358" n="HIAT:ip">,</nts>
                  <nts id="Seg_2359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T706" id="Seg_2361" n="HIAT:w" s="T705">iti</ts>
                  <nts id="Seg_2362" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T707" id="Seg_2364" n="HIAT:w" s="T706">bɨha</ts>
                  <nts id="Seg_2365" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T708" id="Seg_2367" n="HIAT:w" s="T707">mökküjüːnü</ts>
                  <nts id="Seg_2368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T709" id="Seg_2370" n="HIAT:w" s="T708">koton</ts>
                  <nts id="Seg_2371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T710" id="Seg_2373" n="HIAT:w" s="T709">tuppakka</ts>
                  <nts id="Seg_2374" n="HIAT:ip">,</nts>
                  <nts id="Seg_2375" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T711" id="Seg_2377" n="HIAT:w" s="T710">meːne</ts>
                  <nts id="Seg_2378" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T712" id="Seg_2380" n="HIAT:w" s="T711">ebe</ts>
                  <nts id="Seg_2381" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T713" id="Seg_2383" n="HIAT:w" s="T712">ürdünen</ts>
                  <nts id="Seg_2384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T714" id="Seg_2386" n="HIAT:w" s="T713">ɨtɨ͡alɨːr</ts>
                  <nts id="Seg_2387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T715" id="Seg_2389" n="HIAT:w" s="T714">ete</ts>
                  <nts id="Seg_2390" n="HIAT:ip">.</nts>
                  <nts id="Seg_2391" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T727" id="Seg_2393" n="HIAT:u" s="T715">
                  <ts e="T716" id="Seg_2395" n="HIAT:w" s="T715">Kɨtɨltan</ts>
                  <nts id="Seg_2396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T717" id="Seg_2398" n="HIAT:w" s="T716">kördökkö</ts>
                  <nts id="Seg_2399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T718" id="Seg_2401" n="HIAT:w" s="T717">ebe</ts>
                  <nts id="Seg_2402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T719" id="Seg_2404" n="HIAT:w" s="T718">di͡ek</ts>
                  <nts id="Seg_2405" n="HIAT:ip">,</nts>
                  <nts id="Seg_2406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T720" id="Seg_2408" n="HIAT:w" s="T719">uːga</ts>
                  <nts id="Seg_2409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T721" id="Seg_2411" n="HIAT:w" s="T720">bukatɨn</ts>
                  <nts id="Seg_2412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T722" id="Seg_2414" n="HIAT:w" s="T721">kihi</ts>
                  <nts id="Seg_2415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T723" id="Seg_2417" n="HIAT:w" s="T722">tugu</ts>
                  <nts id="Seg_2418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T724" id="Seg_2420" n="HIAT:w" s="T723">da</ts>
                  <nts id="Seg_2421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T725" id="Seg_2423" n="HIAT:w" s="T724">tu͡olkulu͡ok</ts>
                  <nts id="Seg_2424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T726" id="Seg_2426" n="HIAT:w" s="T725">bu͡olbatak</ts>
                  <nts id="Seg_2427" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T727" id="Seg_2429" n="HIAT:w" s="T726">ete</ts>
                  <nts id="Seg_2430" n="HIAT:ip">.</nts>
                  <nts id="Seg_2431" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T735" id="Seg_2433" n="HIAT:u" s="T727">
                  <ts e="T728" id="Seg_2435" n="HIAT:w" s="T727">Kolobura</ts>
                  <nts id="Seg_2436" n="HIAT:ip">,</nts>
                  <nts id="Seg_2437" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T729" id="Seg_2439" n="HIAT:w" s="T728">ürgübüt</ts>
                  <nts id="Seg_2440" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T730" id="Seg_2442" n="HIAT:w" s="T729">ulakan</ts>
                  <nts id="Seg_2443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T731" id="Seg_2445" n="HIAT:w" s="T730">kɨːl</ts>
                  <nts id="Seg_2446" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T732" id="Seg_2448" n="HIAT:w" s="T731">ü͡öre</ts>
                  <nts id="Seg_2449" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T733" id="Seg_2451" n="HIAT:w" s="T732">karbɨːrɨn</ts>
                  <nts id="Seg_2452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T734" id="Seg_2454" n="HIAT:w" s="T733">kördük</ts>
                  <nts id="Seg_2455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T735" id="Seg_2457" n="HIAT:w" s="T734">ete</ts>
                  <nts id="Seg_2458" n="HIAT:ip">.</nts>
                  <nts id="Seg_2459" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T749" id="Seg_2461" n="HIAT:u" s="T735">
                  <ts e="T736" id="Seg_2463" n="HIAT:w" s="T735">Iti</ts>
                  <nts id="Seg_2464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T737" id="Seg_2466" n="HIAT:w" s="T736">ebe</ts>
                  <nts id="Seg_2467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T738" id="Seg_2469" n="HIAT:w" s="T737">kehiːtiger</ts>
                  <nts id="Seg_2470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T739" id="Seg_2472" n="HIAT:w" s="T738">töhölöːk</ts>
                  <nts id="Seg_2473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T740" id="Seg_2475" n="HIAT:w" s="T739">kihi</ts>
                  <nts id="Seg_2476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T741" id="Seg_2478" n="HIAT:w" s="T740">tumnastan</ts>
                  <nts id="Seg_2479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T742" id="Seg_2481" n="HIAT:w" s="T741">seriː</ts>
                  <nts id="Seg_2482" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T743" id="Seg_2484" n="HIAT:w" s="T742">saːtɨttan</ts>
                  <nts id="Seg_2485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T744" id="Seg_2487" n="HIAT:w" s="T743">ölbütün</ts>
                  <nts id="Seg_2488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T745" id="Seg_2490" n="HIAT:w" s="T744">kim</ts>
                  <nts id="Seg_2491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T746" id="Seg_2493" n="HIAT:w" s="T745">daːgɨnɨ</ts>
                  <nts id="Seg_2494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T747" id="Seg_2496" n="HIAT:w" s="T746">bilbet</ts>
                  <nts id="Seg_2497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T748" id="Seg_2499" n="HIAT:w" s="T747">oččogo</ts>
                  <nts id="Seg_2500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T749" id="Seg_2502" n="HIAT:w" s="T748">ete</ts>
                  <nts id="Seg_2503" n="HIAT:ip">.</nts>
                  <nts id="Seg_2504" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T759" id="Seg_2506" n="HIAT:u" s="T749">
                  <ts e="T750" id="Seg_2508" n="HIAT:w" s="T749">Ebeni</ts>
                  <nts id="Seg_2509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T751" id="Seg_2511" n="HIAT:w" s="T750">taksan</ts>
                  <nts id="Seg_2512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T752" id="Seg_2514" n="HIAT:w" s="T751">baraːn</ts>
                  <nts id="Seg_2515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T753" id="Seg_2517" n="HIAT:w" s="T752">bihi͡ettere</ts>
                  <nts id="Seg_2518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T754" id="Seg_2520" n="HIAT:w" s="T753">emi͡e</ts>
                  <nts id="Seg_2521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T755" id="Seg_2523" n="HIAT:w" s="T754">nʼemʼesteri</ts>
                  <nts id="Seg_2524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T756" id="Seg_2526" n="HIAT:w" s="T755">solo</ts>
                  <nts id="Seg_2527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T757" id="Seg_2529" n="HIAT:w" s="T756">bullarbakka</ts>
                  <nts id="Seg_2530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T758" id="Seg_2532" n="HIAT:w" s="T757">delbi</ts>
                  <nts id="Seg_2533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T759" id="Seg_2535" n="HIAT:w" s="T758">bappɨttara</ts>
                  <nts id="Seg_2536" n="HIAT:ip">.</nts>
                  <nts id="Seg_2537" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T785" id="Seg_2539" n="HIAT:u" s="T759">
                  <ts e="T760" id="Seg_2541" n="HIAT:w" s="T759">Onton</ts>
                  <nts id="Seg_2542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T761" id="Seg_2544" n="HIAT:w" s="T760">min</ts>
                  <nts id="Seg_2545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T762" id="Seg_2547" n="HIAT:w" s="T761">töhö</ts>
                  <nts id="Seg_2548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T763" id="Seg_2550" n="HIAT:w" s="T762">emete</ts>
                  <nts id="Seg_2551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T764" id="Seg_2553" n="HIAT:w" s="T763">seriːge</ts>
                  <nts id="Seg_2554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T765" id="Seg_2556" n="HIAT:w" s="T764">barammɨn</ts>
                  <nts id="Seg_2557" n="HIAT:ip">,</nts>
                  <nts id="Seg_2558" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T766" id="Seg_2560" n="HIAT:w" s="T765">tördüsteːk</ts>
                  <nts id="Seg_2561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T767" id="Seg_2563" n="HIAT:w" s="T766">künüger</ts>
                  <nts id="Seg_2564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T768" id="Seg_2566" n="HIAT:w" s="T767">janvarʼ</ts>
                  <nts id="Seg_2567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T769" id="Seg_2569" n="HIAT:w" s="T768">ɨjga</ts>
                  <nts id="Seg_2570" n="HIAT:ip">,</nts>
                  <nts id="Seg_2571" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T770" id="Seg_2573" n="HIAT:w" s="T769">tɨːhɨčča</ts>
                  <nts id="Seg_2574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T771" id="Seg_2576" n="HIAT:w" s="T770">togus</ts>
                  <nts id="Seg_2577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T772" id="Seg_2579" n="HIAT:w" s="T771">süːs</ts>
                  <nts id="Seg_2580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T773" id="Seg_2582" n="HIAT:w" s="T772">tü͡örd-u͡on</ts>
                  <nts id="Seg_2583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T774" id="Seg_2585" n="HIAT:w" s="T773">üs</ts>
                  <nts id="Seg_2586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T775" id="Seg_2588" n="HIAT:w" s="T774">dʼɨllaːkka</ts>
                  <nts id="Seg_2589" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T776" id="Seg_2591" n="HIAT:w" s="T775">bütügespin</ts>
                  <nts id="Seg_2592" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T777" id="Seg_2594" n="HIAT:w" s="T776">gojobuːnnappɨtɨm</ts>
                  <nts id="Seg_2595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T778" id="Seg_2597" n="HIAT:w" s="T777">bert</ts>
                  <nts id="Seg_2598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T779" id="Seg_2600" n="HIAT:w" s="T778">ɨ͡arakannɨk</ts>
                  <nts id="Seg_2601" n="HIAT:ip">,</nts>
                  <nts id="Seg_2602" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T780" id="Seg_2604" n="HIAT:w" s="T779">kaŋas</ts>
                  <nts id="Seg_2605" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T781" id="Seg_2607" n="HIAT:w" s="T780">di͡eki</ts>
                  <nts id="Seg_2608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T782" id="Seg_2610" n="HIAT:w" s="T781">buːtum</ts>
                  <nts id="Seg_2611" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T783" id="Seg_2613" n="HIAT:w" s="T782">oŋu͡ogun</ts>
                  <nts id="Seg_2614" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T784" id="Seg_2616" n="HIAT:w" s="T783">delbi</ts>
                  <nts id="Seg_2617" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T785" id="Seg_2619" n="HIAT:w" s="T784">tohuttarbɨtɨm</ts>
                  <nts id="Seg_2620" n="HIAT:ip">.</nts>
                  <nts id="Seg_2621" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T800" id="Seg_2623" n="HIAT:u" s="T785">
                  <ts e="T786" id="Seg_2625" n="HIAT:w" s="T785">Ihini</ts>
                  <nts id="Seg_2626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T787" id="Seg_2628" n="HIAT:w" s="T786">emtetemmin</ts>
                  <nts id="Seg_2629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T788" id="Seg_2631" n="HIAT:w" s="T787">ohu͡okpar</ts>
                  <nts id="Seg_2632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T789" id="Seg_2634" n="HIAT:w" s="T788">di͡eri</ts>
                  <nts id="Seg_2635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T790" id="Seg_2637" n="HIAT:w" s="T789">alta</ts>
                  <nts id="Seg_2638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T791" id="Seg_2640" n="HIAT:w" s="T790">ɨj</ts>
                  <nts id="Seg_2641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T792" id="Seg_2643" n="HIAT:w" s="T791">turkarɨ</ts>
                  <nts id="Seg_2644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T793" id="Seg_2646" n="HIAT:w" s="T792">sɨppɨtɨm</ts>
                  <nts id="Seg_2647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T794" id="Seg_2649" n="HIAT:w" s="T793">Essentuki</ts>
                  <nts id="Seg_2650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T795" id="Seg_2652" n="HIAT:w" s="T794">di͡en</ts>
                  <nts id="Seg_2653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T796" id="Seg_2655" n="HIAT:w" s="T795">gu͡orakka</ts>
                  <nts id="Seg_2656" n="HIAT:ip">,</nts>
                  <nts id="Seg_2657" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T797" id="Seg_2659" n="HIAT:w" s="T796">Kavkaːz</ts>
                  <nts id="Seg_2660" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T798" id="Seg_2662" n="HIAT:w" s="T797">di͡en</ts>
                  <nts id="Seg_2663" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T799" id="Seg_2665" n="HIAT:w" s="T798">itiː</ts>
                  <nts id="Seg_2666" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T800" id="Seg_2668" n="HIAT:w" s="T799">hirge</ts>
                  <nts id="Seg_2669" n="HIAT:ip">.</nts>
                  <nts id="Seg_2670" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T807" id="Seg_2672" n="HIAT:u" s="T800">
                  <ts e="T801" id="Seg_2674" n="HIAT:w" s="T800">Onton</ts>
                  <nts id="Seg_2675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T802" id="Seg_2677" n="HIAT:w" s="T801">emteten</ts>
                  <nts id="Seg_2678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T803" id="Seg_2680" n="HIAT:w" s="T802">baraːn</ts>
                  <nts id="Seg_2681" n="HIAT:ip">,</nts>
                  <nts id="Seg_2682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T804" id="Seg_2684" n="HIAT:w" s="T803">mas</ts>
                  <nts id="Seg_2685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T805" id="Seg_2687" n="HIAT:w" s="T804">tanʼaktaːk</ts>
                  <nts id="Seg_2688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T806" id="Seg_2690" n="HIAT:w" s="T805">taksan</ts>
                  <nts id="Seg_2691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T807" id="Seg_2693" n="HIAT:w" s="T806">baraːmmɨn</ts>
                  <nts id="Seg_2694" n="HIAT:ip">.</nts>
                  <nts id="Seg_2695" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T813" id="Seg_2697" n="HIAT:u" s="T807">
                  <ts e="T808" id="Seg_2699" n="HIAT:w" s="T807">Seriːtten</ts>
                  <nts id="Seg_2700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T809" id="Seg_2702" n="HIAT:w" s="T808">adʼas</ts>
                  <nts id="Seg_2703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T810" id="Seg_2705" n="HIAT:w" s="T809">boskolommutum</ts>
                  <nts id="Seg_2706" n="HIAT:ip">,</nts>
                  <nts id="Seg_2707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T811" id="Seg_2709" n="HIAT:w" s="T810">armʼijattan</ts>
                  <nts id="Seg_2710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T812" id="Seg_2712" n="HIAT:w" s="T811">sаvsʼem</ts>
                  <nts id="Seg_2713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T813" id="Seg_2715" n="HIAT:w" s="T812">taksɨbɨtɨm</ts>
                  <nts id="Seg_2716" n="HIAT:ip">.</nts>
                  <nts id="Seg_2717" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T827" id="Seg_2719" n="HIAT:u" s="T813">
                  <ts e="T814" id="Seg_2721" n="HIAT:w" s="T813">Maŋnaj</ts>
                  <nts id="Seg_2722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T815" id="Seg_2724" n="HIAT:w" s="T814">seriːlespit</ts>
                  <nts id="Seg_2725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T816" id="Seg_2727" n="HIAT:w" s="T815">kümmütten</ts>
                  <nts id="Seg_2728" n="HIAT:ip">,</nts>
                  <nts id="Seg_2729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T817" id="Seg_2731" n="HIAT:w" s="T816">biːr</ts>
                  <nts id="Seg_2732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T818" id="Seg_2734" n="HIAT:w" s="T817">dʼɨl</ts>
                  <nts id="Seg_2735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T819" id="Seg_2737" n="HIAT:w" s="T818">aŋarɨn</ts>
                  <nts id="Seg_2738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T820" id="Seg_2740" n="HIAT:w" s="T819">turkarɨ</ts>
                  <nts id="Seg_2741" n="HIAT:ip">,</nts>
                  <nts id="Seg_2742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T821" id="Seg_2744" n="HIAT:w" s="T820">seriː</ts>
                  <nts id="Seg_2745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T822" id="Seg_2747" n="HIAT:w" s="T821">kersiːtiger</ts>
                  <nts id="Seg_2748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T823" id="Seg_2750" n="HIAT:w" s="T822">sɨldʼammɨn</ts>
                  <nts id="Seg_2751" n="HIAT:ip">,</nts>
                  <nts id="Seg_2752" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T824" id="Seg_2754" n="HIAT:w" s="T823">barɨta</ts>
                  <nts id="Seg_2755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T825" id="Seg_2757" n="HIAT:w" s="T824">tü͡örka</ts>
                  <nts id="Seg_2758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T826" id="Seg_2760" n="HIAT:w" s="T825">kat</ts>
                  <nts id="Seg_2761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T827" id="Seg_2763" n="HIAT:w" s="T826">gojobuːnnappɨtɨm</ts>
                  <nts id="Seg_2764" n="HIAT:ip">.</nts>
                  <nts id="Seg_2765" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T832" id="Seg_2767" n="HIAT:u" s="T827">
                  <ts e="T828" id="Seg_2769" n="HIAT:w" s="T827">Iti</ts>
                  <nts id="Seg_2770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T829" id="Seg_2772" n="HIAT:w" s="T828">tuhunan</ts>
                  <nts id="Seg_2773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T830" id="Seg_2775" n="HIAT:w" s="T829">üste</ts>
                  <nts id="Seg_2776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T831" id="Seg_2778" n="HIAT:w" s="T830">emtete</ts>
                  <nts id="Seg_2779" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T832" id="Seg_2781" n="HIAT:w" s="T831">hɨldʼɨbɨtɨm</ts>
                  <nts id="Seg_2782" n="HIAT:ip">.</nts>
                  <nts id="Seg_2783" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T854" id="Seg_2785" n="HIAT:u" s="T832">
                  <ts e="T833" id="Seg_2787" n="HIAT:w" s="T832">Kɨlgastɨk</ts>
                  <nts id="Seg_2788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T834" id="Seg_2790" n="HIAT:w" s="T833">kepseːtekke</ts>
                  <nts id="Seg_2791" n="HIAT:ip">,</nts>
                  <nts id="Seg_2792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T835" id="Seg_2794" n="HIAT:w" s="T834">itigirdik</ts>
                  <nts id="Seg_2795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T836" id="Seg_2797" n="HIAT:w" s="T835">min</ts>
                  <nts id="Seg_2798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T837" id="Seg_2800" n="HIAT:w" s="T836">kihi</ts>
                  <nts id="Seg_2801" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T838" id="Seg_2803" n="HIAT:w" s="T837">aːjmak</ts>
                  <nts id="Seg_2804" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T839" id="Seg_2806" n="HIAT:w" s="T838">üjetiger</ts>
                  <nts id="Seg_2807" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T840" id="Seg_2809" n="HIAT:w" s="T839">kaččaga</ts>
                  <nts id="Seg_2810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T841" id="Seg_2812" n="HIAT:w" s="T840">da</ts>
                  <nts id="Seg_2813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T842" id="Seg_2815" n="HIAT:w" s="T841">billibetek</ts>
                  <nts id="Seg_2816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T843" id="Seg_2818" n="HIAT:w" s="T842">aga</ts>
                  <nts id="Seg_2819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T844" id="Seg_2821" n="HIAT:w" s="T843">da</ts>
                  <nts id="Seg_2822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T845" id="Seg_2824" n="HIAT:w" s="T844">iti</ts>
                  <nts id="Seg_2825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T846" id="Seg_2827" n="HIAT:w" s="T845">ulakan</ts>
                  <nts id="Seg_2828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T847" id="Seg_2830" n="HIAT:w" s="T846">seriːtiger</ts>
                  <nts id="Seg_2831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T848" id="Seg_2833" n="HIAT:w" s="T847">bejem</ts>
                  <nts id="Seg_2834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T849" id="Seg_2836" n="HIAT:w" s="T848">dʼommun</ts>
                  <nts id="Seg_2837" n="HIAT:ip">,</nts>
                  <nts id="Seg_2838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T850" id="Seg_2840" n="HIAT:w" s="T849">töröːbüt</ts>
                  <nts id="Seg_2841" n="HIAT:ip">,</nts>
                  <nts id="Seg_2842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T851" id="Seg_2844" n="HIAT:w" s="T850">ü͡öskeːbit</ts>
                  <nts id="Seg_2845" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T852" id="Seg_2847" n="HIAT:w" s="T851">sirbin</ts>
                  <nts id="Seg_2848" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T853" id="Seg_2850" n="HIAT:w" s="T852">kömüsküː</ts>
                  <nts id="Seg_2851" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T854" id="Seg_2853" n="HIAT:w" s="T853">hɨldʼɨbɨtɨm</ts>
                  <nts id="Seg_2854" n="HIAT:ip">.</nts>
                  <nts id="Seg_2855" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T861" id="Seg_2857" n="HIAT:u" s="T854">
                  <ts e="T855" id="Seg_2859" n="HIAT:w" s="T854">Mini͡ene</ts>
                  <nts id="Seg_2860" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T856" id="Seg_2862" n="HIAT:w" s="T855">kaːnɨm</ts>
                  <nts id="Seg_2863" n="HIAT:ip">,</nts>
                  <nts id="Seg_2864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T857" id="Seg_2866" n="HIAT:w" s="T856">kölöhünüm</ts>
                  <nts id="Seg_2867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T858" id="Seg_2869" n="HIAT:w" s="T857">iti</ts>
                  <nts id="Seg_2870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T859" id="Seg_2872" n="HIAT:w" s="T858">seriːge</ts>
                  <nts id="Seg_2873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T860" id="Seg_2875" n="HIAT:w" s="T859">köŋül</ts>
                  <nts id="Seg_2876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T861" id="Seg_2878" n="HIAT:w" s="T860">toktubataga</ts>
                  <nts id="Seg_2879" n="HIAT:ip">.</nts>
                  <nts id="Seg_2880" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T882" id="Seg_2882" n="HIAT:u" s="T861">
                  <ts e="T862" id="Seg_2884" n="HIAT:w" s="T861">Bihi͡ene</ts>
                  <nts id="Seg_2885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T863" id="Seg_2887" n="HIAT:w" s="T862">kammunʼistʼičʼeskaj</ts>
                  <nts id="Seg_2888" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T864" id="Seg_2890" n="HIAT:w" s="T863">partʼijabɨt</ts>
                  <nts id="Seg_2891" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T865" id="Seg_2893" n="HIAT:w" s="T864">kiːn</ts>
                  <nts id="Seg_2894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T866" id="Seg_2896" n="HIAT:w" s="T865">kаmitʼeta</ts>
                  <nts id="Seg_2897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T867" id="Seg_2899" n="HIAT:w" s="T866">onton</ts>
                  <nts id="Seg_2900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T868" id="Seg_2902" n="HIAT:w" s="T867">bihi͡ene</ts>
                  <nts id="Seg_2903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T869" id="Seg_2905" n="HIAT:w" s="T868">gasudarstvabɨtɨn</ts>
                  <nts id="Seg_2906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T870" id="Seg_2908" n="HIAT:w" s="T869">ologun</ts>
                  <nts id="Seg_2909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T871" id="Seg_2911" n="HIAT:w" s="T870">könnöröːččü</ts>
                  <nts id="Seg_2912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T872" id="Seg_2914" n="HIAT:w" s="T871">pravʼitʼelʼstvabɨt</ts>
                  <nts id="Seg_2915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T873" id="Seg_2917" n="HIAT:w" s="T872">bi͡erbittere</ts>
                  <nts id="Seg_2918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T874" id="Seg_2920" n="HIAT:w" s="T873">mini͡eke</ts>
                  <nts id="Seg_2921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T875" id="Seg_2923" n="HIAT:w" s="T874">gasudarstva</ts>
                  <nts id="Seg_2924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T876" id="Seg_2926" n="HIAT:w" s="T875">ürdük</ts>
                  <nts id="Seg_2927" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T877" id="Seg_2929" n="HIAT:w" s="T876">seriːtin</ts>
                  <nts id="Seg_2930" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878" id="Seg_2932" n="HIAT:w" s="T877">ordʼenɨn</ts>
                  <nts id="Seg_2933" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2934" n="HIAT:ip">"</nts>
                  <ts e="T878.tx.1" id="Seg_2936" n="HIAT:w" s="T878">Bajevova</ts>
                  <nts id="Seg_2937" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T878.tx.2" id="Seg_2939" n="HIAT:w" s="T878.tx.1">Kraːsnava</ts>
                  <nts id="Seg_2940" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T881" id="Seg_2942" n="HIAT:w" s="T878.tx.2">Znamʼenʼi</ts>
                  <nts id="Seg_2943" n="HIAT:ip">"</nts>
                  <nts id="Seg_2944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T882" id="Seg_2946" n="HIAT:w" s="T881">di͡en</ts>
                  <nts id="Seg_2947" n="HIAT:ip">.</nts>
                  <nts id="Seg_2948" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T890" id="Seg_2950" n="HIAT:u" s="T882">
                  <ts e="T883" id="Seg_2952" n="HIAT:w" s="T882">Onton</ts>
                  <nts id="Seg_2953" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T884" id="Seg_2955" n="HIAT:w" s="T883">össü͡ö</ts>
                  <nts id="Seg_2956" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885" id="Seg_2958" n="HIAT:w" s="T884">mʼedalʼi</ts>
                  <nts id="Seg_2959" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2960" n="HIAT:ip">"</nts>
                  <ts e="T885.tx.1" id="Seg_2962" n="HIAT:w" s="T885">Za</ts>
                  <nts id="Seg_2963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885.tx.2" id="Seg_2965" n="HIAT:w" s="T885.tx.1">pabʼedu</ts>
                  <nts id="Seg_2966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T885.tx.3" id="Seg_2968" n="HIAT:w" s="T885.tx.2">nad</ts>
                  <nts id="Seg_2969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T889" id="Seg_2971" n="HIAT:w" s="T885.tx.3">Gʼermanʼijej</ts>
                  <nts id="Seg_2972" n="HIAT:ip">"</nts>
                  <nts id="Seg_2973" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T890" id="Seg_2975" n="HIAT:w" s="T889">di͡en</ts>
                  <nts id="Seg_2976" n="HIAT:ip">.</nts>
                  <nts id="Seg_2977" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T904" id="Seg_2979" n="HIAT:u" s="T890">
                  <ts e="T891" id="Seg_2981" n="HIAT:w" s="T890">Anɨ</ts>
                  <nts id="Seg_2982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T892" id="Seg_2984" n="HIAT:w" s="T891">bihigi</ts>
                  <nts id="Seg_2985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T893" id="Seg_2987" n="HIAT:w" s="T892">Savʼetskaj</ts>
                  <nts id="Seg_2988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T894" id="Seg_2990" n="HIAT:w" s="T893">ologur</ts>
                  <nts id="Seg_2991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T895" id="Seg_2993" n="HIAT:w" s="T894">oloror</ts>
                  <nts id="Seg_2994" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T896" id="Seg_2996" n="HIAT:w" s="T895">kihiler</ts>
                  <nts id="Seg_2997" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2998" n="HIAT:ip">–</nts>
                  <nts id="Seg_2999" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T897" id="Seg_3001" n="HIAT:w" s="T896">olokkut</ts>
                  <nts id="Seg_3002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T898" id="Seg_3004" n="HIAT:w" s="T897">berke</ts>
                  <nts id="Seg_3005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T899" id="Seg_3007" n="HIAT:w" s="T898">tuksan</ts>
                  <nts id="Seg_3008" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T900" id="Seg_3010" n="HIAT:w" s="T899">turar</ts>
                  <nts id="Seg_3011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T901" id="Seg_3013" n="HIAT:w" s="T900">inni</ts>
                  <nts id="Seg_3014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T902" id="Seg_3016" n="HIAT:w" s="T901">di͡ek</ts>
                  <nts id="Seg_3017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T903" id="Seg_3019" n="HIAT:w" s="T902">barar</ts>
                  <nts id="Seg_3020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T904" id="Seg_3022" n="HIAT:w" s="T903">küːske</ts>
                  <nts id="Seg_3023" n="HIAT:ip">.</nts>
                  <nts id="Seg_3024" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T912" id="Seg_3026" n="HIAT:u" s="T904">
                  <ts e="T905" id="Seg_3028" n="HIAT:w" s="T904">Ol</ts>
                  <nts id="Seg_3029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T906" id="Seg_3031" n="HIAT:w" s="T905">ihin</ts>
                  <nts id="Seg_3032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T907" id="Seg_3034" n="HIAT:w" s="T906">kim</ts>
                  <nts id="Seg_3035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T908" id="Seg_3037" n="HIAT:w" s="T907">daːganɨ</ts>
                  <nts id="Seg_3038" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T909" id="Seg_3040" n="HIAT:w" s="T908">anɨ</ts>
                  <nts id="Seg_3041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T910" id="Seg_3043" n="HIAT:w" s="T909">seriːgin</ts>
                  <nts id="Seg_3044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T911" id="Seg_3046" n="HIAT:w" s="T910">bu͡olu͡ogun</ts>
                  <nts id="Seg_3047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T912" id="Seg_3049" n="HIAT:w" s="T911">bagarbat</ts>
                  <nts id="Seg_3050" n="HIAT:ip">.</nts>
                  <nts id="Seg_3051" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T921" id="Seg_3053" n="HIAT:u" s="T912">
                  <ts e="T913" id="Seg_3055" n="HIAT:w" s="T912">Bihigi</ts>
                  <nts id="Seg_3056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T914" id="Seg_3058" n="HIAT:w" s="T913">sirbit</ts>
                  <nts id="Seg_3059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T915" id="Seg_3061" n="HIAT:w" s="T914">kihite</ts>
                  <nts id="Seg_3062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T916" id="Seg_3064" n="HIAT:w" s="T915">urukkuttan</ts>
                  <nts id="Seg_3065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T917" id="Seg_3067" n="HIAT:w" s="T916">daːganɨ</ts>
                  <nts id="Seg_3068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T918" id="Seg_3070" n="HIAT:w" s="T917">seriːge</ts>
                  <nts id="Seg_3071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T919" id="Seg_3073" n="HIAT:w" s="T918">bagaraːččɨta</ts>
                  <nts id="Seg_3074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T920" id="Seg_3076" n="HIAT:w" s="T919">hu͡ok</ts>
                  <nts id="Seg_3077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T921" id="Seg_3079" n="HIAT:w" s="T920">ete</ts>
                  <nts id="Seg_3080" n="HIAT:ip">.</nts>
                  <nts id="Seg_3081" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T935" id="Seg_3083" n="HIAT:u" s="T921">
                  <ts e="T922" id="Seg_3085" n="HIAT:w" s="T921">Bihigi</ts>
                  <nts id="Seg_3086" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T923" id="Seg_3088" n="HIAT:w" s="T922">dʼommut</ts>
                  <nts id="Seg_3089" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T924" id="Seg_3091" n="HIAT:w" s="T923">anɨ</ts>
                  <nts id="Seg_3092" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T925" id="Seg_3094" n="HIAT:w" s="T924">bejetin</ts>
                  <nts id="Seg_3095" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T926" id="Seg_3097" n="HIAT:w" s="T925">sanaːta</ts>
                  <nts id="Seg_3098" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T927" id="Seg_3100" n="HIAT:w" s="T926">söbülüːrün</ts>
                  <nts id="Seg_3101" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T928" id="Seg_3103" n="HIAT:w" s="T927">sɨrdɨk</ts>
                  <nts id="Seg_3104" n="HIAT:ip">,</nts>
                  <nts id="Seg_3105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T929" id="Seg_3107" n="HIAT:w" s="T928">dʼolloːk</ts>
                  <nts id="Seg_3108" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T930" id="Seg_3110" n="HIAT:w" s="T929">Lʼenʼin</ts>
                  <nts id="Seg_3111" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T931" id="Seg_3113" n="HIAT:w" s="T930">ɨjbɨt</ts>
                  <nts id="Seg_3114" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T932" id="Seg_3116" n="HIAT:w" s="T931">kommunʼist</ts>
                  <nts id="Seg_3117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T933" id="Seg_3119" n="HIAT:w" s="T932">aːttaːk</ts>
                  <nts id="Seg_3120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T934" id="Seg_3122" n="HIAT:w" s="T933">ologun</ts>
                  <nts id="Seg_3123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T935" id="Seg_3125" n="HIAT:w" s="T934">oŋostor</ts>
                  <nts id="Seg_3126" n="HIAT:ip">.</nts>
                  <nts id="Seg_3127" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T939" id="Seg_3129" n="HIAT:u" s="T935">
                  <ts e="T936" id="Seg_3131" n="HIAT:w" s="T935">Bihigi</ts>
                  <nts id="Seg_3132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T937" id="Seg_3134" n="HIAT:w" s="T936">barɨbɨt</ts>
                  <nts id="Seg_3135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T938" id="Seg_3137" n="HIAT:w" s="T937">diːbit</ts>
                  <nts id="Seg_3138" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T939" id="Seg_3140" n="HIAT:w" s="T938">anɨ</ts>
                  <nts id="Seg_3141" n="HIAT:ip">:</nts>
                  <nts id="Seg_3142" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T944" id="Seg_3144" n="HIAT:u" s="T939">
                  <nts id="Seg_3145" n="HIAT:ip">"</nts>
                  <ts e="T940" id="Seg_3147" n="HIAT:w" s="T939">Seriː</ts>
                  <nts id="Seg_3148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T941" id="Seg_3150" n="HIAT:w" s="T940">su͡ok</ts>
                  <nts id="Seg_3151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T942" id="Seg_3153" n="HIAT:w" s="T941">bu͡ollun</ts>
                  <nts id="Seg_3154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T943" id="Seg_3156" n="HIAT:w" s="T942">sir</ts>
                  <nts id="Seg_3157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T944" id="Seg_3159" n="HIAT:w" s="T943">ürdünen</ts>
                  <nts id="Seg_3160" n="HIAT:ip">.</nts>
                  <nts id="Seg_3161" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T951" id="Seg_3163" n="HIAT:u" s="T944">
                  <ts e="T945" id="Seg_3165" n="HIAT:w" s="T944">Baːr</ts>
                  <nts id="Seg_3166" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T946" id="Seg_3168" n="HIAT:w" s="T945">bu͡ollun</ts>
                  <nts id="Seg_3169" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T947" id="Seg_3171" n="HIAT:w" s="T946">sɨrdɨk</ts>
                  <nts id="Seg_3172" n="HIAT:ip">,</nts>
                  <nts id="Seg_3173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T948" id="Seg_3175" n="HIAT:w" s="T947">künneːk</ts>
                  <nts id="Seg_3176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T949" id="Seg_3178" n="HIAT:w" s="T948">olok</ts>
                  <nts id="Seg_3179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T950" id="Seg_3181" n="HIAT:w" s="T949">barɨ</ts>
                  <nts id="Seg_3182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T951" id="Seg_3184" n="HIAT:w" s="T950">joŋŋo</ts>
                  <nts id="Seg_3185" n="HIAT:ip">"</nts>
                  <nts id="Seg_3186" n="HIAT:ip">.</nts>
                  <nts id="Seg_3187" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx">
            <ts e="T951" id="Seg_3188" n="sc" s="T0">
               <ts e="T1" id="Seg_3190" n="e" s="T0">Min </ts>
               <ts e="T2" id="Seg_3192" n="e" s="T1">seriːge </ts>
               <ts e="T3" id="Seg_3194" n="e" s="T2">barbɨtɨm </ts>
               <ts e="T4" id="Seg_3196" n="e" s="T3">bejem </ts>
               <ts e="T5" id="Seg_3198" n="e" s="T4">sanaːbɨnan </ts>
               <ts e="T6" id="Seg_3200" n="e" s="T5">tɨːhɨčča </ts>
               <ts e="T7" id="Seg_3202" n="e" s="T6">togus </ts>
               <ts e="T8" id="Seg_3204" n="e" s="T7">süːs </ts>
               <ts e="T9" id="Seg_3206" n="e" s="T8">tü͡örd-u͡on </ts>
               <ts e="T10" id="Seg_3208" n="e" s="T9">biːr </ts>
               <ts e="T11" id="Seg_3210" n="e" s="T10">dʼɨllaːkka. </ts>
               <ts e="T12" id="Seg_3212" n="e" s="T11">Kühüŋŋü </ts>
               <ts e="T13" id="Seg_3214" n="e" s="T12">sʼentʼabrʼ, </ts>
               <ts e="T14" id="Seg_3216" n="e" s="T13">tɨmnɨː </ts>
               <ts e="T15" id="Seg_3218" n="e" s="T14">tüher </ts>
               <ts e="T16" id="Seg_3220" n="e" s="T15">ɨjɨgar. </ts>
               <ts e="T17" id="Seg_3222" n="e" s="T16">Maŋnaj </ts>
               <ts e="T18" id="Seg_3224" n="e" s="T17">barammɨn, </ts>
               <ts e="T19" id="Seg_3226" n="e" s="T18">biːr </ts>
               <ts e="T20" id="Seg_3228" n="e" s="T19">dʼɨlɨ </ts>
               <ts e="T21" id="Seg_3230" n="e" s="T20">gɨtta </ts>
               <ts e="T22" id="Seg_3232" n="e" s="T21">togus </ts>
               <ts e="T23" id="Seg_3234" n="e" s="T22">ɨjɨ </ts>
               <ts e="T24" id="Seg_3236" n="e" s="T23">seriː </ts>
               <ts e="T25" id="Seg_3238" n="e" s="T24">ü͡öregiger </ts>
               <ts e="T26" id="Seg_3240" n="e" s="T25">ü͡örene </ts>
               <ts e="T27" id="Seg_3242" n="e" s="T26">sɨldʼɨbɨtɨm </ts>
               <ts e="T29" id="Seg_3244" n="e" s="T27">Dalʼnʼij Vastok </ts>
               <ts e="T30" id="Seg_3246" n="e" s="T29">di͡en </ts>
               <ts e="T31" id="Seg_3248" n="e" s="T30">hirge. </ts>
               <ts e="T32" id="Seg_3250" n="e" s="T31">Onton </ts>
               <ts e="T33" id="Seg_3252" n="e" s="T32">sajɨn </ts>
               <ts e="T34" id="Seg_3254" n="e" s="T33">tɨːhɨčča </ts>
               <ts e="T35" id="Seg_3256" n="e" s="T34">togus </ts>
               <ts e="T36" id="Seg_3258" n="e" s="T35">süːs </ts>
               <ts e="T37" id="Seg_3260" n="e" s="T36">tü͡örd-u͡on </ts>
               <ts e="T38" id="Seg_3262" n="e" s="T37">üs </ts>
               <ts e="T39" id="Seg_3264" n="e" s="T38">dʼɨllaːkka </ts>
               <ts e="T40" id="Seg_3266" n="e" s="T39">bihigini </ts>
               <ts e="T41" id="Seg_3268" n="e" s="T40">ɨːppɨttara </ts>
               <ts e="T42" id="Seg_3270" n="e" s="T41">zaːpad </ts>
               <ts e="T43" id="Seg_3272" n="e" s="T42">di͡ek, </ts>
               <ts e="T44" id="Seg_3274" n="e" s="T43">seriː </ts>
               <ts e="T45" id="Seg_3276" n="e" s="T44">kersiːte </ts>
               <ts e="T46" id="Seg_3278" n="e" s="T45">bara </ts>
               <ts e="T47" id="Seg_3280" n="e" s="T46">turar </ts>
               <ts e="T48" id="Seg_3282" n="e" s="T47">siriger. </ts>
               <ts e="T49" id="Seg_3284" n="e" s="T48">Bihigi </ts>
               <ts e="T50" id="Seg_3286" n="e" s="T49">seriːni </ts>
               <ts e="T51" id="Seg_3288" n="e" s="T50">körsübüppüt </ts>
               <ts e="T52" id="Seg_3290" n="e" s="T51">Proxorovka </ts>
               <ts e="T53" id="Seg_3292" n="e" s="T52">di͡en </ts>
               <ts e="T54" id="Seg_3294" n="e" s="T53">aːttaːk </ts>
               <ts e="T55" id="Seg_3296" n="e" s="T54">dʼerʼevnʼaga </ts>
               <ts e="T56" id="Seg_3298" n="e" s="T55">Kurskaj </ts>
               <ts e="T57" id="Seg_3300" n="e" s="T56">oblahɨgar. </ts>
               <ts e="T58" id="Seg_3302" n="e" s="T57">Onno </ts>
               <ts e="T59" id="Seg_3304" n="e" s="T58">nʼemʼes </ts>
               <ts e="T60" id="Seg_3306" n="e" s="T59">seriːtin </ts>
               <ts e="T61" id="Seg_3308" n="e" s="T60">kihite </ts>
               <ts e="T62" id="Seg_3310" n="e" s="T61">bert </ts>
               <ts e="T63" id="Seg_3312" n="e" s="T62">ügüs </ts>
               <ts e="T64" id="Seg_3314" n="e" s="T63">seriː </ts>
               <ts e="T65" id="Seg_3316" n="e" s="T64">saːtɨn </ts>
               <ts e="T66" id="Seg_3318" n="e" s="T65">eŋin </ts>
               <ts e="T67" id="Seg_3320" n="e" s="T66">atɨn </ts>
               <ts e="T68" id="Seg_3322" n="e" s="T67">teriltetin </ts>
               <ts e="T69" id="Seg_3324" n="e" s="T68">munnʼan </ts>
               <ts e="T70" id="Seg_3326" n="e" s="T69">baraːn, </ts>
               <ts e="T71" id="Seg_3328" n="e" s="T70">bihigi </ts>
               <ts e="T72" id="Seg_3330" n="e" s="T71">armʼijabɨtɨn </ts>
               <ts e="T73" id="Seg_3332" n="e" s="T72">berke </ts>
               <ts e="T74" id="Seg_3334" n="e" s="T73">töttörü </ts>
               <ts e="T75" id="Seg_3336" n="e" s="T74">di͡ek </ts>
               <ts e="T76" id="Seg_3338" n="e" s="T75">otut </ts>
               <ts e="T77" id="Seg_3340" n="e" s="T76">bi͡es </ts>
               <ts e="T78" id="Seg_3342" n="e" s="T77">biːrsteleːk </ts>
               <ts e="T79" id="Seg_3344" n="e" s="T78">sirge </ts>
               <ts e="T80" id="Seg_3346" n="e" s="T79">ɨgajbɨt </ts>
               <ts e="T81" id="Seg_3348" n="e" s="T80">ete. </ts>
               <ts e="T82" id="Seg_3350" n="e" s="T81">Onton </ts>
               <ts e="T83" id="Seg_3352" n="e" s="T82">kojut </ts>
               <ts e="T84" id="Seg_3354" n="e" s="T83">iti </ts>
               <ts e="T85" id="Seg_3356" n="e" s="T84">siri </ts>
               <ts e="T86" id="Seg_3358" n="e" s="T85">Kurskaj </ts>
               <ts e="T87" id="Seg_3360" n="e" s="T86">duga </ts>
               <ts e="T88" id="Seg_3362" n="e" s="T87">di͡en </ts>
               <ts e="T89" id="Seg_3364" n="e" s="T88">aːttaːbɨttara. </ts>
               <ts e="T90" id="Seg_3366" n="e" s="T89">Sakalɨː </ts>
               <ts e="T91" id="Seg_3368" n="e" s="T90">saŋardakka </ts>
               <ts e="T92" id="Seg_3370" n="e" s="T91">Kurskaj </ts>
               <ts e="T93" id="Seg_3372" n="e" s="T92">tokura </ts>
               <ts e="T94" id="Seg_3374" n="e" s="T93">di͡ekke </ts>
               <ts e="T95" id="Seg_3376" n="e" s="T94">höp </ts>
               <ts e="T96" id="Seg_3378" n="e" s="T95">bu͡olu͡oga. </ts>
               <ts e="T97" id="Seg_3380" n="e" s="T96">Nʼemʼes </ts>
               <ts e="T98" id="Seg_3382" n="e" s="T97">armʼijatɨn </ts>
               <ts e="T99" id="Seg_3384" n="e" s="T98">tojonun </ts>
               <ts e="T100" id="Seg_3386" n="e" s="T99">sanaːta </ts>
               <ts e="T101" id="Seg_3388" n="e" s="T100">baːr </ts>
               <ts e="T102" id="Seg_3390" n="e" s="T101">ebit. </ts>
               <ts e="T103" id="Seg_3392" n="e" s="T102">Iti </ts>
               <ts e="T104" id="Seg_3394" n="e" s="T103">sirinen </ts>
               <ts e="T105" id="Seg_3396" n="e" s="T104">bɨha </ts>
               <ts e="T106" id="Seg_3398" n="e" s="T105">mökküjen </ts>
               <ts e="T107" id="Seg_3400" n="e" s="T106">baraːn, </ts>
               <ts e="T108" id="Seg_3402" n="e" s="T107">bert </ts>
               <ts e="T109" id="Seg_3404" n="e" s="T108">türgennik </ts>
               <ts e="T110" id="Seg_3406" n="e" s="T109">bihigi </ts>
               <ts e="T111" id="Seg_3408" n="e" s="T110">kiːn </ts>
               <ts e="T112" id="Seg_3410" n="e" s="T111">sirbitin </ts>
               <ts e="T113" id="Seg_3412" n="e" s="T112">Maskva </ts>
               <ts e="T114" id="Seg_3414" n="e" s="T113">di͡en </ts>
               <ts e="T115" id="Seg_3416" n="e" s="T114">gu͡oratɨ </ts>
               <ts e="T116" id="Seg_3418" n="e" s="T115">ɨlar </ts>
               <ts e="T117" id="Seg_3420" n="e" s="T116">kördük. </ts>
               <ts e="T118" id="Seg_3422" n="e" s="T117">Bihi͡ene </ts>
               <ts e="T119" id="Seg_3424" n="e" s="T118">čaːspɨt </ts>
               <ts e="T120" id="Seg_3426" n="e" s="T119">Kurskaj </ts>
               <ts e="T121" id="Seg_3428" n="e" s="T120">tokurugar </ts>
               <ts e="T122" id="Seg_3430" n="e" s="T121">tu͡oktan </ts>
               <ts e="T123" id="Seg_3432" n="e" s="T122">da </ts>
               <ts e="T124" id="Seg_3434" n="e" s="T123">ulakan </ts>
               <ts e="T125" id="Seg_3436" n="e" s="T124">seriː </ts>
               <ts e="T126" id="Seg_3438" n="e" s="T125">u͡otugar </ts>
               <ts e="T127" id="Seg_3440" n="e" s="T126">kiːren </ts>
               <ts e="T128" id="Seg_3442" n="e" s="T127">baraːn, </ts>
               <ts e="T129" id="Seg_3444" n="e" s="T128">u͡on </ts>
               <ts e="T130" id="Seg_3446" n="e" s="T129">kün </ts>
               <ts e="T131" id="Seg_3448" n="e" s="T130">turkarɨ </ts>
               <ts e="T132" id="Seg_3450" n="e" s="T131">nʼemʼes </ts>
               <ts e="T133" id="Seg_3452" n="e" s="T132">armʼijatɨn </ts>
               <ts e="T134" id="Seg_3454" n="e" s="T133">bata </ts>
               <ts e="T135" id="Seg_3456" n="e" s="T134">hataːbɨta. </ts>
               <ts e="T136" id="Seg_3458" n="e" s="T135">Iti </ts>
               <ts e="T137" id="Seg_3460" n="e" s="T136">turkarɨ </ts>
               <ts e="T138" id="Seg_3462" n="e" s="T137">bihigi </ts>
               <ts e="T139" id="Seg_3464" n="e" s="T138">seriːlespippit </ts>
               <ts e="T140" id="Seg_3466" n="e" s="T139">künü </ts>
               <ts e="T141" id="Seg_3468" n="e" s="T140">tüːnü </ts>
               <ts e="T142" id="Seg_3470" n="e" s="T141">bilbekke. </ts>
               <ts e="T143" id="Seg_3472" n="e" s="T142">Utujbakka </ts>
               <ts e="T144" id="Seg_3474" n="e" s="T143">da </ts>
               <ts e="T145" id="Seg_3476" n="e" s="T144">sɨnnʼammakka </ts>
               <ts e="T146" id="Seg_3478" n="e" s="T145">da. </ts>
               <ts e="T147" id="Seg_3480" n="e" s="T146">Kolobura </ts>
               <ts e="T148" id="Seg_3482" n="e" s="T147">tugu </ts>
               <ts e="T149" id="Seg_3484" n="e" s="T148">da </ts>
               <ts e="T150" id="Seg_3486" n="e" s="T149">bilbet </ts>
               <ts e="T151" id="Seg_3488" n="e" s="T150">istibet </ts>
               <ts e="T152" id="Seg_3490" n="e" s="T151">kördük. </ts>
               <ts e="T153" id="Seg_3492" n="e" s="T152">Ikki </ts>
               <ts e="T154" id="Seg_3494" n="e" s="T153">di͡egi </ts>
               <ts e="T155" id="Seg_3496" n="e" s="T154">seriːleheːčči </ts>
               <ts e="T156" id="Seg_3498" n="e" s="T155">öttütten </ts>
               <ts e="T157" id="Seg_3500" n="e" s="T156">töhölü͡ök </ts>
               <ts e="T158" id="Seg_3502" n="e" s="T157">tɨːhɨčča </ts>
               <ts e="T159" id="Seg_3504" n="e" s="T158">kihi </ts>
               <ts e="T160" id="Seg_3506" n="e" s="T159">süppütün, </ts>
               <ts e="T161" id="Seg_3508" n="e" s="T160">ölbütün, </ts>
               <ts e="T162" id="Seg_3510" n="e" s="T161">töhölöːk </ts>
               <ts e="T163" id="Seg_3512" n="e" s="T162">kihi </ts>
               <ts e="T164" id="Seg_3514" n="e" s="T163">gojobuːn </ts>
               <ts e="T165" id="Seg_3516" n="e" s="T164">bu͡olbutun, </ts>
               <ts e="T166" id="Seg_3518" n="e" s="T165">töhölöːk </ts>
               <ts e="T167" id="Seg_3520" n="e" s="T166">seriː </ts>
               <ts e="T168" id="Seg_3522" n="e" s="T167">saːta, </ts>
               <ts e="T169" id="Seg_3524" n="e" s="T168">samalʼottar, </ts>
               <ts e="T170" id="Seg_3526" n="e" s="T169">tankalar, </ts>
               <ts e="T171" id="Seg_3528" n="e" s="T170">arudʼijalar </ts>
               <ts e="T172" id="Seg_3530" n="e" s="T171">onton </ts>
               <ts e="T173" id="Seg_3532" n="e" s="T172">kɨra </ts>
               <ts e="T174" id="Seg_3534" n="e" s="T173">eŋin </ts>
               <ts e="T175" id="Seg_3536" n="e" s="T174">saːlar, </ts>
               <ts e="T176" id="Seg_3538" n="e" s="T175">seriː </ts>
               <ts e="T177" id="Seg_3540" n="e" s="T176">teriltete </ts>
               <ts e="T178" id="Seg_3542" n="e" s="T177">aldʼammɨtɨn </ts>
               <ts e="T179" id="Seg_3544" n="e" s="T178">kim </ts>
               <ts e="T180" id="Seg_3546" n="e" s="T179">daːgɨnɨ </ts>
               <ts e="T181" id="Seg_3548" n="e" s="T180">čakčɨ </ts>
               <ts e="T182" id="Seg_3550" n="e" s="T181">bilbet </ts>
               <ts e="T183" id="Seg_3552" n="e" s="T182">ete </ts>
               <ts e="T184" id="Seg_3554" n="e" s="T183">oččogo. </ts>
               <ts e="T185" id="Seg_3556" n="e" s="T184">Kajtak </ts>
               <ts e="T186" id="Seg_3558" n="e" s="T185">ulakan </ts>
               <ts e="T187" id="Seg_3560" n="e" s="T186">küːsteːk </ts>
               <ts e="T188" id="Seg_3562" n="e" s="T187">seriː </ts>
               <ts e="T189" id="Seg_3564" n="e" s="T188">barbɨtɨn </ts>
               <ts e="T190" id="Seg_3566" n="e" s="T189">iti </ts>
               <ts e="T191" id="Seg_3568" n="e" s="T190">hirge, </ts>
               <ts e="T192" id="Seg_3570" n="e" s="T191">kihi </ts>
               <ts e="T193" id="Seg_3572" n="e" s="T192">tu͡olkulu͡oga </ts>
               <ts e="T194" id="Seg_3574" n="e" s="T193">biːr </ts>
               <ts e="T195" id="Seg_3576" n="e" s="T194">munnuk </ts>
               <ts e="T196" id="Seg_3578" n="e" s="T195">koloburtan. </ts>
               <ts e="T197" id="Seg_3580" n="e" s="T196">Biːr </ts>
               <ts e="T198" id="Seg_3582" n="e" s="T197">(buːstaːk) </ts>
               <ts e="T199" id="Seg_3584" n="e" s="T198">sirkeːŋŋe </ts>
               <ts e="T200" id="Seg_3586" n="e" s="T199">bihigi </ts>
               <ts e="T201" id="Seg_3588" n="e" s="T200">baːr </ts>
               <ts e="T202" id="Seg_3590" n="e" s="T201">sirbitiger, </ts>
               <ts e="T203" id="Seg_3592" n="e" s="T202">küŋŋe </ts>
               <ts e="T204" id="Seg_3594" n="e" s="T203">biːrde </ts>
               <ts e="T205" id="Seg_3596" n="e" s="T204">kötön </ts>
               <ts e="T206" id="Seg_3598" n="e" s="T205">keleːčči </ts>
               <ts e="T207" id="Seg_3600" n="e" s="T206">ete </ts>
               <ts e="T208" id="Seg_3602" n="e" s="T207">nʼemʼes </ts>
               <ts e="T209" id="Seg_3604" n="e" s="T208">di͡egi </ts>
               <ts e="T210" id="Seg_3606" n="e" s="T209">öttütten </ts>
               <ts e="T211" id="Seg_3608" n="e" s="T210">ikki </ts>
               <ts e="T212" id="Seg_3610" n="e" s="T211">hüːs </ts>
               <ts e="T213" id="Seg_3612" n="e" s="T212">kuraŋa </ts>
               <ts e="T214" id="Seg_3614" n="e" s="T213">samalʼottar. </ts>
               <ts e="T215" id="Seg_3616" n="e" s="T214">Ontuŋ </ts>
               <ts e="T216" id="Seg_3618" n="e" s="T215">barɨta </ts>
               <ts e="T217" id="Seg_3620" n="e" s="T216">bihigi </ts>
               <ts e="T218" id="Seg_3622" n="e" s="T217">ürdübütünen </ts>
               <ts e="T219" id="Seg_3624" n="e" s="T218">samɨːr </ts>
               <ts e="T220" id="Seg_3626" n="e" s="T219">kördük </ts>
               <ts e="T221" id="Seg_3628" n="e" s="T220">tühen </ts>
               <ts e="T222" id="Seg_3630" n="e" s="T221">e </ts>
               <ts e="T223" id="Seg_3632" n="e" s="T222">hüːs </ts>
               <ts e="T224" id="Seg_3634" n="e" s="T223">bombalarɨ </ts>
               <ts e="T225" id="Seg_3636" n="e" s="T224">keːheːčči </ts>
               <ts e="T226" id="Seg_3638" n="e" s="T225">ete. </ts>
               <ts e="T227" id="Seg_3640" n="e" s="T226">Onton </ts>
               <ts e="T228" id="Seg_3642" n="e" s="T227">tuspa </ts>
               <ts e="T229" id="Seg_3644" n="e" s="T228">össü͡ö </ts>
               <ts e="T230" id="Seg_3646" n="e" s="T229">ɨtɨ͡alɨːr </ts>
               <ts e="T231" id="Seg_3648" n="e" s="T230">ete </ts>
               <ts e="T232" id="Seg_3650" n="e" s="T231">töhö </ts>
               <ts e="T233" id="Seg_3652" n="e" s="T232">emete </ts>
               <ts e="T234" id="Seg_3654" n="e" s="T233">sarpa </ts>
               <ts e="T235" id="Seg_3656" n="e" s="T234">kördük </ts>
               <ts e="T236" id="Seg_3658" n="e" s="T235">iːtiːleːk </ts>
               <ts e="T237" id="Seg_3660" n="e" s="T236">seriː </ts>
               <ts e="T238" id="Seg_3662" n="e" s="T237">saːtɨnan– </ts>
               <ts e="T239" id="Seg_3664" n="e" s="T238">pulʼemʼotɨnan. </ts>
               <ts e="T240" id="Seg_3666" n="e" s="T239">Itiččeleːk </ts>
               <ts e="T241" id="Seg_3668" n="e" s="T240">bomba </ts>
               <ts e="T242" id="Seg_3670" n="e" s="T241">tüspütün </ts>
               <ts e="T243" id="Seg_3672" n="e" s="T242">kenne </ts>
               <ts e="T244" id="Seg_3674" n="e" s="T243">itiččeleːk </ts>
               <ts e="T245" id="Seg_3676" n="e" s="T244">saː </ts>
               <ts e="T246" id="Seg_3678" n="e" s="T245">ɨtɨ͡alaːbɨtɨn </ts>
               <ts e="T247" id="Seg_3680" n="e" s="T246">kenne </ts>
               <ts e="T248" id="Seg_3682" n="e" s="T247">sir </ts>
               <ts e="T249" id="Seg_3684" n="e" s="T248">barɨta </ts>
               <ts e="T250" id="Seg_3686" n="e" s="T249">tu͡okkaːn </ts>
               <ts e="T251" id="Seg_3688" n="e" s="T250">da </ts>
               <ts e="T252" id="Seg_3690" n="e" s="T251">köstübet </ts>
               <ts e="T253" id="Seg_3692" n="e" s="T252">kördük, </ts>
               <ts e="T254" id="Seg_3694" n="e" s="T253">kara </ts>
               <ts e="T255" id="Seg_3696" n="e" s="T254">tuman </ts>
               <ts e="T256" id="Seg_3698" n="e" s="T255">bu͡olar </ts>
               <ts e="T257" id="Seg_3700" n="e" s="T256">ete. </ts>
               <ts e="T258" id="Seg_3702" n="e" s="T257">Iti </ts>
               <ts e="T259" id="Seg_3704" n="e" s="T258">tumaŋŋa </ts>
               <ts e="T260" id="Seg_3706" n="e" s="T259">kihi </ts>
               <ts e="T261" id="Seg_3708" n="e" s="T260">attɨgar </ts>
               <ts e="T262" id="Seg_3710" n="e" s="T261">sɨtar </ts>
               <ts e="T263" id="Seg_3712" n="e" s="T262">kihitin </ts>
               <ts e="T264" id="Seg_3714" n="e" s="T263">koton </ts>
               <ts e="T265" id="Seg_3716" n="e" s="T264">körböt </ts>
               <ts e="T266" id="Seg_3718" n="e" s="T265">ete. </ts>
               <ts e="T267" id="Seg_3720" n="e" s="T266">Sorok </ts>
               <ts e="T268" id="Seg_3722" n="e" s="T267">kenne </ts>
               <ts e="T269" id="Seg_3724" n="e" s="T268">kihi </ts>
               <ts e="T270" id="Seg_3726" n="e" s="T269">seriː </ts>
               <ts e="T271" id="Seg_3728" n="e" s="T270">saːtɨn </ts>
               <ts e="T272" id="Seg_3730" n="e" s="T271">tɨ͡ahɨttan </ts>
               <ts e="T273" id="Seg_3732" n="e" s="T272">tu͡okkaːnɨ </ts>
               <ts e="T274" id="Seg_3734" n="e" s="T273">da </ts>
               <ts e="T275" id="Seg_3736" n="e" s="T274">istibet </ts>
               <ts e="T276" id="Seg_3738" n="e" s="T275">kördük </ts>
               <ts e="T277" id="Seg_3740" n="e" s="T276">dülej </ts>
               <ts e="T278" id="Seg_3742" n="e" s="T277">bu͡olar </ts>
               <ts e="T279" id="Seg_3744" n="e" s="T278">ete. </ts>
               <ts e="T280" id="Seg_3746" n="e" s="T279">Onus </ts>
               <ts e="T281" id="Seg_3748" n="e" s="T280">kümmütüger </ts>
               <ts e="T282" id="Seg_3750" n="e" s="T281">bihigi </ts>
               <ts e="T283" id="Seg_3752" n="e" s="T282">di͡egi </ts>
               <ts e="T284" id="Seg_3754" n="e" s="T283">öttünnen </ts>
               <ts e="T285" id="Seg_3756" n="e" s="T284">seriːni </ts>
               <ts e="T286" id="Seg_3758" n="e" s="T285">olus </ts>
               <ts e="T287" id="Seg_3760" n="e" s="T286">küːsteːktik </ts>
               <ts e="T288" id="Seg_3762" n="e" s="T287">barda. </ts>
               <ts e="T289" id="Seg_3764" n="e" s="T288">Kallaːntan, </ts>
               <ts e="T290" id="Seg_3766" n="e" s="T289">sirten </ts>
               <ts e="T291" id="Seg_3768" n="e" s="T290">bert </ts>
               <ts e="T292" id="Seg_3770" n="e" s="T291">elbek </ts>
               <ts e="T293" id="Seg_3772" n="e" s="T292">samalʼottar, </ts>
               <ts e="T294" id="Seg_3774" n="e" s="T293">tankalar, </ts>
               <ts e="T295" id="Seg_3776" n="e" s="T294">anaːn </ts>
               <ts e="T296" id="Seg_3778" n="e" s="T295">seriːge </ts>
               <ts e="T297" id="Seg_3780" n="e" s="T296">oŋohullubut </ts>
               <ts e="T298" id="Seg_3782" n="e" s="T297">u͡ottaːk </ts>
               <ts e="T299" id="Seg_3784" n="e" s="T298">saːlar, </ts>
               <ts e="T300" id="Seg_3786" n="e" s="T299">"Katʼuša", </ts>
               <ts e="T301" id="Seg_3788" n="e" s="T300">"Andrʼuša" </ts>
               <ts e="T302" id="Seg_3790" n="e" s="T301">di͡en </ts>
               <ts e="T303" id="Seg_3792" n="e" s="T302">aːttaːktar. </ts>
               <ts e="T304" id="Seg_3794" n="e" s="T303">Onton </ts>
               <ts e="T305" id="Seg_3796" n="e" s="T304">tuspa </ts>
               <ts e="T306" id="Seg_3798" n="e" s="T305">töhö </ts>
               <ts e="T307" id="Seg_3800" n="e" s="T306">eme </ts>
               <ts e="T308" id="Seg_3802" n="e" s="T307">kɨra </ts>
               <ts e="T309" id="Seg_3804" n="e" s="T308">saːlar. </ts>
               <ts e="T310" id="Seg_3806" n="e" s="T309">Ol </ts>
               <ts e="T311" id="Seg_3808" n="e" s="T310">barɨta </ts>
               <ts e="T312" id="Seg_3810" n="e" s="T311">biːrge </ts>
               <ts e="T313" id="Seg_3812" n="e" s="T312">ogustular </ts>
               <ts e="T314" id="Seg_3814" n="e" s="T313">nʼemʼester </ts>
               <ts e="T315" id="Seg_3816" n="e" s="T314">baːr </ts>
               <ts e="T316" id="Seg_3818" n="e" s="T315">hirderin. </ts>
               <ts e="T317" id="Seg_3820" n="e" s="T316">Nʼemʼes </ts>
               <ts e="T318" id="Seg_3822" n="e" s="T317">armʼijata </ts>
               <ts e="T319" id="Seg_3824" n="e" s="T318">itini </ts>
               <ts e="T320" id="Seg_3826" n="e" s="T319">dolgunnaːk </ts>
               <ts e="T321" id="Seg_3828" n="e" s="T320">ulakan </ts>
               <ts e="T322" id="Seg_3830" n="e" s="T321">oksuːnu </ts>
               <ts e="T323" id="Seg_3832" n="e" s="T322">tulujbakka </ts>
               <ts e="T324" id="Seg_3834" n="e" s="T323">töttörü </ts>
               <ts e="T325" id="Seg_3836" n="e" s="T324">supturujbuta. </ts>
               <ts e="T326" id="Seg_3838" n="e" s="T325">Sonnokoːn </ts>
               <ts e="T327" id="Seg_3840" n="e" s="T326">u͡on </ts>
               <ts e="T328" id="Seg_3842" n="e" s="T327">bi͡es </ts>
               <ts e="T329" id="Seg_3844" n="e" s="T328">beristeleːk </ts>
               <ts e="T330" id="Seg_3846" n="e" s="T329">siri </ts>
               <ts e="T331" id="Seg_3848" n="e" s="T330">töttörü </ts>
               <ts e="T332" id="Seg_3850" n="e" s="T331">ɨstammɨta. </ts>
               <ts e="T333" id="Seg_3852" n="e" s="T332">Iti </ts>
               <ts e="T334" id="Seg_3854" n="e" s="T333">ulakan </ts>
               <ts e="T335" id="Seg_3856" n="e" s="T334">oksuː </ts>
               <ts e="T336" id="Seg_3858" n="e" s="T335">kennitten </ts>
               <ts e="T337" id="Seg_3860" n="e" s="T336">Kurskaj </ts>
               <ts e="T338" id="Seg_3862" n="e" s="T337">di͡en </ts>
               <ts e="T339" id="Seg_3864" n="e" s="T338">tokura </ts>
               <ts e="T340" id="Seg_3866" n="e" s="T339">berke </ts>
               <ts e="T341" id="Seg_3868" n="e" s="T340">kömmüte. </ts>
               <ts e="T342" id="Seg_3870" n="e" s="T341">Nʼemʼes </ts>
               <ts e="T343" id="Seg_3872" n="e" s="T342">armʼijatɨn </ts>
               <ts e="T344" id="Seg_3874" n="e" s="T343">töttörü </ts>
               <ts e="T345" id="Seg_3876" n="e" s="T344">bihigi </ts>
               <ts e="T346" id="Seg_3878" n="e" s="T345">bappɨppɨt. </ts>
               <ts e="T347" id="Seg_3880" n="e" s="T346">Iti </ts>
               <ts e="T348" id="Seg_3882" n="e" s="T347">kennitten </ts>
               <ts e="T349" id="Seg_3884" n="e" s="T348">nʼemʼester </ts>
               <ts e="T350" id="Seg_3886" n="e" s="T349">hereges </ts>
               <ts e="T351" id="Seg_3888" n="e" s="T350">saldaːttara </ts>
               <ts e="T352" id="Seg_3890" n="e" s="T351">innilerin </ts>
               <ts e="T353" id="Seg_3892" n="e" s="T352">bihi͡eke </ts>
               <ts e="T354" id="Seg_3894" n="e" s="T353">bi͡eren </ts>
               <ts e="T355" id="Seg_3896" n="e" s="T354">baraːn </ts>
               <ts e="T356" id="Seg_3898" n="e" s="T355">suptu </ts>
               <ts e="T357" id="Seg_3900" n="e" s="T356">seriː </ts>
               <ts e="T358" id="Seg_3902" n="e" s="T357">bütü͡ögüger </ts>
               <ts e="T359" id="Seg_3904" n="e" s="T358">di͡eri </ts>
               <ts e="T360" id="Seg_3906" n="e" s="T359">öjdörün-puːttarɨn </ts>
               <ts e="T361" id="Seg_3908" n="e" s="T360">bulbataktara, </ts>
               <ts e="T362" id="Seg_3910" n="e" s="T361">ku͡otan </ts>
               <ts e="T363" id="Seg_3912" n="e" s="T362">ispittere. </ts>
               <ts e="T364" id="Seg_3914" n="e" s="T363">Bihigi </ts>
               <ts e="T365" id="Seg_3916" n="e" s="T364">armʼijabɨt </ts>
               <ts e="T366" id="Seg_3918" n="e" s="T365">bert </ts>
               <ts e="T367" id="Seg_3920" n="e" s="T366">üčügejdik </ts>
               <ts e="T368" id="Seg_3922" n="e" s="T367">küːhün </ts>
               <ts e="T369" id="Seg_3924" n="e" s="T368">tutan </ts>
               <ts e="T370" id="Seg_3926" n="e" s="T369">baraːn </ts>
               <ts e="T371" id="Seg_3928" n="e" s="T370">nʼemʼesteri </ts>
               <ts e="T372" id="Seg_3930" n="e" s="T371">kenni </ts>
               <ts e="T373" id="Seg_3932" n="e" s="T372">di͡ek, </ts>
               <ts e="T374" id="Seg_3934" n="e" s="T373">köllörbökkö, </ts>
               <ts e="T375" id="Seg_3936" n="e" s="T374">delbi </ts>
               <ts e="T376" id="Seg_3938" n="e" s="T375">bappɨta. </ts>
               <ts e="T377" id="Seg_3940" n="e" s="T376">Min </ts>
               <ts e="T378" id="Seg_3942" n="e" s="T377">kojukku </ts>
               <ts e="T379" id="Seg_3944" n="e" s="T378">künnerge </ts>
               <ts e="T380" id="Seg_3946" n="e" s="T379">seriːge </ts>
               <ts e="T381" id="Seg_3948" n="e" s="T380">hɨldʼammɨn, </ts>
               <ts e="T382" id="Seg_3950" n="e" s="T381">itinnik </ts>
               <ts e="T383" id="Seg_3952" n="e" s="T382">sürdeːk </ts>
               <ts e="T384" id="Seg_3954" n="e" s="T383">kihi </ts>
               <ts e="T952" id="Seg_3956" n="e" s="T384">((…)) </ts>
               <ts e="T385" id="Seg_3958" n="e" s="T952">kördük </ts>
               <ts e="T387" id="Seg_3960" n="e" s="T385">körbötögüm. </ts>
               <ts e="T388" id="Seg_3962" n="e" s="T387">Min </ts>
               <ts e="T389" id="Seg_3964" n="e" s="T388">iti </ts>
               <ts e="T390" id="Seg_3966" n="e" s="T389">hirge </ts>
               <ts e="T391" id="Seg_3968" n="e" s="T390">saŋardɨː </ts>
               <ts e="T392" id="Seg_3970" n="e" s="T391">körbütüm </ts>
               <ts e="T393" id="Seg_3972" n="e" s="T392">hu͡ostaːk </ts>
               <ts e="T394" id="Seg_3974" n="e" s="T393">kersiːni, </ts>
               <ts e="T395" id="Seg_3976" n="e" s="T394">kajtak </ts>
               <ts e="T396" id="Seg_3978" n="e" s="T395">seriː </ts>
               <ts e="T397" id="Seg_3980" n="e" s="T396">kihitin </ts>
               <ts e="T398" id="Seg_3982" n="e" s="T397">kaːna </ts>
               <ts e="T399" id="Seg_3984" n="e" s="T398">ürektiː </ts>
               <ts e="T400" id="Seg_3986" n="e" s="T399">toktorun. </ts>
               <ts e="T401" id="Seg_3988" n="e" s="T400">Töhölöːk </ts>
               <ts e="T402" id="Seg_3990" n="e" s="T401">tɨːhɨčča </ts>
               <ts e="T403" id="Seg_3992" n="e" s="T402">kihi </ts>
               <ts e="T404" id="Seg_3994" n="e" s="T403">tɨːna </ts>
               <ts e="T405" id="Seg_3996" n="e" s="T404">bɨstarɨn. </ts>
               <ts e="T406" id="Seg_3998" n="e" s="T405">töhölöːk </ts>
               <ts e="T407" id="Seg_4000" n="e" s="T406">gojobuːn </ts>
               <ts e="T408" id="Seg_4002" n="e" s="T407">bu͡olbut </ts>
               <ts e="T409" id="Seg_4004" n="e" s="T408">kihi </ts>
               <ts e="T410" id="Seg_4006" n="e" s="T409">ulakannɨk </ts>
               <ts e="T411" id="Seg_4008" n="e" s="T410">erejdenerin. </ts>
               <ts e="T412" id="Seg_4010" n="e" s="T411">Min </ts>
               <ts e="T413" id="Seg_4012" n="e" s="T412">emi͡e </ts>
               <ts e="T414" id="Seg_4014" n="e" s="T413">iti </ts>
               <ts e="T415" id="Seg_4016" n="e" s="T414">oksohuːga </ts>
               <ts e="T416" id="Seg_4018" n="e" s="T415">Kurskaj </ts>
               <ts e="T417" id="Seg_4020" n="e" s="T416">tumuhugar, </ts>
               <ts e="T418" id="Seg_4022" n="e" s="T417">atdʼeleʼnʼija </ts>
               <ts e="T419" id="Seg_4024" n="e" s="T418">kamandʼira </ts>
               <ts e="T420" id="Seg_4026" n="e" s="T419">bu͡ola </ts>
               <ts e="T421" id="Seg_4028" n="e" s="T420">hɨldʼammɨn </ts>
               <ts e="T422" id="Seg_4030" n="e" s="T421">dʼogustuk </ts>
               <ts e="T423" id="Seg_4032" n="e" s="T422">kaŋas </ts>
               <ts e="T424" id="Seg_4034" n="e" s="T423">atakpɨn </ts>
               <ts e="T425" id="Seg_4036" n="e" s="T424">bɨlčɨŋɨ </ts>
               <ts e="T426" id="Seg_4038" n="e" s="T425">gojobuːnnappɨtɨm. </ts>
               <ts e="T427" id="Seg_4040" n="e" s="T426">Ol </ts>
               <ts e="T428" id="Seg_4042" n="e" s="T427">bu͡olan </ts>
               <ts e="T429" id="Seg_4044" n="e" s="T428">baraːn </ts>
               <ts e="T430" id="Seg_4046" n="e" s="T429">seriːleher </ts>
               <ts e="T431" id="Seg_4048" n="e" s="T430">sirten </ts>
               <ts e="T432" id="Seg_4050" n="e" s="T431">taksɨbatagɨm. </ts>
               <ts e="T433" id="Seg_4052" n="e" s="T432">Iti </ts>
               <ts e="T434" id="Seg_4054" n="e" s="T433">sirdeːk </ts>
               <ts e="T435" id="Seg_4056" n="e" s="T434">oksohuːga </ts>
               <ts e="T436" id="Seg_4058" n="e" s="T435">min </ts>
               <ts e="T437" id="Seg_4060" n="e" s="T436">baːr </ts>
               <ts e="T438" id="Seg_4062" n="e" s="T437">agaj </ts>
               <ts e="T439" id="Seg_4064" n="e" s="T438">čaːhɨm </ts>
               <ts e="T440" id="Seg_4066" n="e" s="T439">tɨːhɨččattan </ts>
               <ts e="T441" id="Seg_4068" n="e" s="T440">taksa </ts>
               <ts e="T442" id="Seg_4070" n="e" s="T441">seriː </ts>
               <ts e="T443" id="Seg_4072" n="e" s="T442">kihititten </ts>
               <ts e="T444" id="Seg_4074" n="e" s="T443">iti </ts>
               <ts e="T445" id="Seg_4076" n="e" s="T444">u͡on </ts>
               <ts e="T446" id="Seg_4078" n="e" s="T445">kuŋŋe </ts>
               <ts e="T447" id="Seg_4080" n="e" s="T446">oksohuːga </ts>
               <ts e="T448" id="Seg_4082" n="e" s="T447">orputa </ts>
               <ts e="T449" id="Seg_4084" n="e" s="T448">u͡on </ts>
               <ts e="T450" id="Seg_4086" n="e" s="T449">biːr </ts>
               <ts e="T451" id="Seg_4088" n="e" s="T450">agaj </ts>
               <ts e="T452" id="Seg_4090" n="e" s="T451">kihi. </ts>
               <ts e="T453" id="Seg_4092" n="e" s="T452">Iti </ts>
               <ts e="T454" id="Seg_4094" n="e" s="T453">ihiger </ts>
               <ts e="T455" id="Seg_4096" n="e" s="T454">össü͡ö </ts>
               <ts e="T456" id="Seg_4098" n="e" s="T455">biːr </ts>
               <ts e="T457" id="Seg_4100" n="e" s="T456">min </ts>
               <ts e="T458" id="Seg_4102" n="e" s="T457">gojobuːn </ts>
               <ts e="T459" id="Seg_4104" n="e" s="T458">bu͡olbut </ts>
               <ts e="T460" id="Seg_4106" n="e" s="T459">kihi </ts>
               <ts e="T461" id="Seg_4108" n="e" s="T460">baːr </ts>
               <ts e="T462" id="Seg_4110" n="e" s="T461">etim. </ts>
               <ts e="T463" id="Seg_4112" n="e" s="T462">Itinten </ts>
               <ts e="T464" id="Seg_4114" n="e" s="T463">söp </ts>
               <ts e="T465" id="Seg_4116" n="e" s="T464">bu͡olu͡oga </ts>
               <ts e="T466" id="Seg_4118" n="e" s="T465">kolu͡okka, </ts>
               <ts e="T467" id="Seg_4120" n="e" s="T466">kajtak </ts>
               <ts e="T468" id="Seg_4122" n="e" s="T467">aldʼarkajdaːk </ts>
               <ts e="T469" id="Seg_4124" n="e" s="T468">seriː </ts>
               <ts e="T470" id="Seg_4126" n="e" s="T469">etin </ts>
               <ts e="T471" id="Seg_4128" n="e" s="T470">Kurskaj </ts>
               <ts e="T472" id="Seg_4130" n="e" s="T471">togurugor, </ts>
               <ts e="T473" id="Seg_4132" n="e" s="T472">töhö </ts>
               <ts e="T474" id="Seg_4134" n="e" s="T473">kaːn </ts>
               <ts e="T475" id="Seg_4136" n="e" s="T474">toktubutun, </ts>
               <ts e="T476" id="Seg_4138" n="e" s="T475">töhö </ts>
               <ts e="T477" id="Seg_4140" n="e" s="T476">kihi </ts>
               <ts e="T478" id="Seg_4142" n="e" s="T477">ölbütün. </ts>
               <ts e="T479" id="Seg_4144" n="e" s="T478">Kurskajga </ts>
               <ts e="T480" id="Seg_4146" n="e" s="T479">büten </ts>
               <ts e="T481" id="Seg_4148" n="e" s="T480">baraːn </ts>
               <ts e="T482" id="Seg_4150" n="e" s="T481">bihigi </ts>
               <ts e="T483" id="Seg_4152" n="e" s="T482">armʼijabɨt </ts>
               <ts e="T484" id="Seg_4154" n="e" s="T483">töttörü </ts>
               <ts e="T485" id="Seg_4156" n="e" s="T484">kürüːr </ts>
               <ts e="T486" id="Seg_4158" n="e" s="T485">nʼemʼehi </ts>
               <ts e="T487" id="Seg_4160" n="e" s="T486">zaːpad </ts>
               <ts e="T488" id="Seg_4162" n="e" s="T487">di͡ek </ts>
               <ts e="T489" id="Seg_4164" n="e" s="T488">batan </ts>
               <ts e="T490" id="Seg_4166" n="e" s="T489">barbɨta. </ts>
               <ts e="T491" id="Seg_4168" n="e" s="T490">Iti </ts>
               <ts e="T492" id="Seg_4170" n="e" s="T491">Kurskaj </ts>
               <ts e="T493" id="Seg_4172" n="e" s="T492">kennitten </ts>
               <ts e="T494" id="Seg_4174" n="e" s="T493">min </ts>
               <ts e="T495" id="Seg_4176" n="e" s="T494">saːmaj </ts>
               <ts e="T496" id="Seg_4178" n="e" s="T495">seriː </ts>
               <ts e="T497" id="Seg_4180" n="e" s="T496">(kerse) </ts>
               <ts e="T498" id="Seg_4182" n="e" s="T497">siriger </ts>
               <ts e="T499" id="Seg_4184" n="e" s="T498">baːr </ts>
               <ts e="T500" id="Seg_4186" n="e" s="T499">etim </ts>
               <ts e="T501" id="Seg_4188" n="e" s="T500">tɨːhɨčča </ts>
               <ts e="T502" id="Seg_4190" n="e" s="T501">togus </ts>
               <ts e="T503" id="Seg_4192" n="e" s="T502">süːs </ts>
               <ts e="T504" id="Seg_4194" n="e" s="T503">tü͡örd-u͡on </ts>
               <ts e="T505" id="Seg_4196" n="e" s="T504">tü͡ört </ts>
               <ts e="T506" id="Seg_4198" n="e" s="T505">dʼɨllaːkka, </ts>
               <ts e="T507" id="Seg_4200" n="e" s="T506">kɨhɨŋŋɨ </ts>
               <ts e="T508" id="Seg_4202" n="e" s="T507">janvarʼ </ts>
               <ts e="T509" id="Seg_4204" n="e" s="T508">ɨjga </ts>
               <ts e="T510" id="Seg_4206" n="e" s="T509">di͡eri. </ts>
               <ts e="T511" id="Seg_4208" n="e" s="T510">Iti </ts>
               <ts e="T512" id="Seg_4210" n="e" s="T511">kemŋe </ts>
               <ts e="T513" id="Seg_4212" n="e" s="T512">nʼemʼes </ts>
               <ts e="T514" id="Seg_4214" n="e" s="T513">saldaːttarɨn </ts>
               <ts e="T515" id="Seg_4216" n="e" s="T514">gɨtta </ts>
               <ts e="T516" id="Seg_4218" n="e" s="T515">utara </ts>
               <ts e="T517" id="Seg_4220" n="e" s="T516">turan </ts>
               <ts e="T518" id="Seg_4222" n="e" s="T517">seriːlespitim </ts>
               <ts e="T519" id="Seg_4224" n="e" s="T518">munnuk </ts>
               <ts e="T520" id="Seg_4226" n="e" s="T519">sirderge, </ts>
               <ts e="T521" id="Seg_4228" n="e" s="T520">Bʼelgarad </ts>
               <ts e="T522" id="Seg_4230" n="e" s="T521">onton </ts>
               <ts e="T523" id="Seg_4232" n="e" s="T522">Xarʼkav, </ts>
               <ts e="T524" id="Seg_4234" n="e" s="T523">onton </ts>
               <ts e="T525" id="Seg_4236" n="e" s="T524">Poltava, </ts>
               <ts e="T526" id="Seg_4238" n="e" s="T525">onton </ts>
               <ts e="T527" id="Seg_4240" n="e" s="T526">Krʼemʼenčʼug, </ts>
               <ts e="T528" id="Seg_4242" n="e" s="T527">onton </ts>
               <ts e="T529" id="Seg_4244" n="e" s="T528">Krʼukov </ts>
               <ts e="T530" id="Seg_4246" n="e" s="T529">di͡en </ts>
               <ts e="T531" id="Seg_4248" n="e" s="T530">aːttaːk </ts>
               <ts e="T532" id="Seg_4250" n="e" s="T531">ulakan </ts>
               <ts e="T533" id="Seg_4252" n="e" s="T532">gu͡orattarɨ </ts>
               <ts e="T534" id="Seg_4254" n="e" s="T533">ɨlarga, </ts>
               <ts e="T535" id="Seg_4256" n="e" s="T534">ukrainʼes </ts>
               <ts e="T536" id="Seg_4258" n="e" s="T535">di͡en </ts>
               <ts e="T537" id="Seg_4260" n="e" s="T536">omuk </ts>
               <ts e="T538" id="Seg_4262" n="e" s="T537">siriger. </ts>
               <ts e="T539" id="Seg_4264" n="e" s="T538">Iti </ts>
               <ts e="T540" id="Seg_4266" n="e" s="T539">gu͡orattartan </ts>
               <ts e="T541" id="Seg_4268" n="e" s="T540">olus </ts>
               <ts e="T542" id="Seg_4270" n="e" s="T541">kɨtaːnaktɨk </ts>
               <ts e="T543" id="Seg_4272" n="e" s="T542">bihigi </ts>
               <ts e="T544" id="Seg_4274" n="e" s="T543">seriːlespippit </ts>
               <ts e="T545" id="Seg_4276" n="e" s="T544">nʼemʼehi </ts>
               <ts e="T546" id="Seg_4278" n="e" s="T545">gɨtta </ts>
               <ts e="T547" id="Seg_4280" n="e" s="T546">Xarʼkav </ts>
               <ts e="T548" id="Seg_4282" n="e" s="T547">gu͡orat </ts>
               <ts e="T549" id="Seg_4284" n="e" s="T548">bɨldʼahɨːga. </ts>
               <ts e="T550" id="Seg_4286" n="e" s="T549">Iti </ts>
               <ts e="T551" id="Seg_4288" n="e" s="T550">gu͡orat </ts>
               <ts e="T552" id="Seg_4290" n="e" s="T551">üske </ts>
               <ts e="T553" id="Seg_4292" n="e" s="T552">di͡eri </ts>
               <ts e="T554" id="Seg_4294" n="e" s="T553">töttörü taːrɨ </ts>
               <ts e="T555" id="Seg_4296" n="e" s="T554">iliːtten </ts>
               <ts e="T556" id="Seg_4298" n="e" s="T555">iliːge </ts>
               <ts e="T557" id="Seg_4300" n="e" s="T556">kele </ts>
               <ts e="T558" id="Seg_4302" n="e" s="T557">hɨldʼɨbɨta. </ts>
               <ts e="T559" id="Seg_4304" n="e" s="T558">Onton </ts>
               <ts e="T560" id="Seg_4306" n="e" s="T559">ühüs </ts>
               <ts e="T561" id="Seg_4308" n="e" s="T560">keliːtiger </ts>
               <ts e="T562" id="Seg_4310" n="e" s="T561">bihigi </ts>
               <ts e="T563" id="Seg_4312" n="e" s="T562">iliːbitten </ts>
               <ts e="T564" id="Seg_4314" n="e" s="T563">ɨːppatakpɨt. </ts>
               <ts e="T565" id="Seg_4316" n="e" s="T564">Onton </ts>
               <ts e="T566" id="Seg_4318" n="e" s="T565">ulakan </ts>
               <ts e="T567" id="Seg_4320" n="e" s="T566">oksohuː </ts>
               <ts e="T568" id="Seg_4322" n="e" s="T567">össü͡ö </ts>
               <ts e="T569" id="Seg_4324" n="e" s="T568">baːr </ts>
               <ts e="T570" id="Seg_4326" n="e" s="T569">ete </ts>
               <ts e="T571" id="Seg_4328" n="e" s="T570">Dnʼepr </ts>
               <ts e="T572" id="Seg_4330" n="e" s="T571">di͡en </ts>
               <ts e="T573" id="Seg_4332" n="e" s="T572">aːttaːk </ts>
               <ts e="T574" id="Seg_4334" n="e" s="T573">ebe </ts>
               <ts e="T575" id="Seg_4336" n="e" s="T574">kehiːtiger. </ts>
               <ts e="T576" id="Seg_4338" n="e" s="T575">Iti </ts>
               <ts e="T577" id="Seg_4340" n="e" s="T576">ebe </ts>
               <ts e="T578" id="Seg_4342" n="e" s="T577">oŋu͡orgu </ts>
               <ts e="T579" id="Seg_4344" n="e" s="T578">öttütüger </ts>
               <ts e="T580" id="Seg_4346" n="e" s="T579">oloron </ts>
               <ts e="T581" id="Seg_4348" n="e" s="T580">nʼemʼester </ts>
               <ts e="T582" id="Seg_4350" n="e" s="T581">künüsteri-tüːnneri </ts>
               <ts e="T583" id="Seg_4352" n="e" s="T582">ketiː </ts>
               <ts e="T584" id="Seg_4354" n="e" s="T583">hɨldʼannar, </ts>
               <ts e="T585" id="Seg_4356" n="e" s="T584">bihigini </ts>
               <ts e="T586" id="Seg_4358" n="e" s="T585">amattan </ts>
               <ts e="T587" id="Seg_4360" n="e" s="T586">sataːn </ts>
               <ts e="T588" id="Seg_4362" n="e" s="T587">keherpet </ts>
               <ts e="T589" id="Seg_4364" n="e" s="T588">etilere. </ts>
               <ts e="T590" id="Seg_4366" n="e" s="T589">Ol </ts>
               <ts e="T591" id="Seg_4368" n="e" s="T590">ihin </ts>
               <ts e="T592" id="Seg_4370" n="e" s="T591">bihigi </ts>
               <ts e="T593" id="Seg_4372" n="e" s="T592">kas </ts>
               <ts e="T594" id="Seg_4374" n="e" s="T593">da </ts>
               <ts e="T595" id="Seg_4376" n="e" s="T594">künü </ts>
               <ts e="T596" id="Seg_4378" n="e" s="T595">koton </ts>
               <ts e="T597" id="Seg_4380" n="e" s="T596">taksɨbatakpɨt, </ts>
               <ts e="T598" id="Seg_4382" n="e" s="T597">betereːgi </ts>
               <ts e="T599" id="Seg_4384" n="e" s="T598">kɨtɨlga </ts>
               <ts e="T600" id="Seg_4386" n="e" s="T599">olorbupput. </ts>
               <ts e="T601" id="Seg_4388" n="e" s="T600">Ebeni </ts>
               <ts e="T602" id="Seg_4390" n="e" s="T601">taksar </ts>
               <ts e="T603" id="Seg_4392" n="e" s="T602">mostɨlarɨ </ts>
               <ts e="T604" id="Seg_4394" n="e" s="T603">künüs </ts>
               <ts e="T605" id="Seg_4396" n="e" s="T604">oŋororbutun, </ts>
               <ts e="T606" id="Seg_4398" n="e" s="T605">nʼemʼester </ts>
               <ts e="T607" id="Seg_4400" n="e" s="T606">samalʼottara </ts>
               <ts e="T608" id="Seg_4402" n="e" s="T607">kötön </ts>
               <ts e="T609" id="Seg_4404" n="e" s="T608">kele-kele </ts>
               <ts e="T610" id="Seg_4406" n="e" s="T609">töhö </ts>
               <ts e="T611" id="Seg_4408" n="e" s="T610">eme </ts>
               <ts e="T612" id="Seg_4410" n="e" s="T611">bombalarɨ </ts>
               <ts e="T613" id="Seg_4412" n="e" s="T612">bɨragattaːn, </ts>
               <ts e="T614" id="Seg_4414" n="e" s="T613">delbi </ts>
               <ts e="T615" id="Seg_4416" n="e" s="T614">aldʼatar </ts>
               <ts e="T616" id="Seg_4418" n="e" s="T615">ete. </ts>
               <ts e="T617" id="Seg_4420" n="e" s="T616">Ol </ts>
               <ts e="T618" id="Seg_4422" n="e" s="T617">ihin </ts>
               <ts e="T619" id="Seg_4424" n="e" s="T618">kojut </ts>
               <ts e="T620" id="Seg_4426" n="e" s="T619">bihigi </ts>
               <ts e="T621" id="Seg_4428" n="e" s="T620">tüːn </ts>
               <ts e="T622" id="Seg_4430" n="e" s="T621">kahu͡on </ts>
               <ts e="T623" id="Seg_4432" n="e" s="T622">sirinen </ts>
               <ts e="T624" id="Seg_4434" n="e" s="T623">mostɨlarɨ </ts>
               <ts e="T625" id="Seg_4436" n="e" s="T624">oŋortuːr </ts>
               <ts e="T626" id="Seg_4438" n="e" s="T625">etibit. </ts>
               <ts e="T627" id="Seg_4440" n="e" s="T626">Abaːhɨ </ts>
               <ts e="T628" id="Seg_4442" n="e" s="T627">nʼemʼis </ts>
               <ts e="T629" id="Seg_4444" n="e" s="T628">tüːn </ts>
               <ts e="T630" id="Seg_4446" n="e" s="T629">daː </ts>
               <ts e="T631" id="Seg_4448" n="e" s="T630">oŋorbut </ts>
               <ts e="T632" id="Seg_4450" n="e" s="T631">mostɨlarbɨtɨn </ts>
               <ts e="T633" id="Seg_4452" n="e" s="T632">turu͡orbat </ts>
               <ts e="T634" id="Seg_4454" n="e" s="T633">ete. </ts>
               <ts e="T635" id="Seg_4456" n="e" s="T634">Oččogo </ts>
               <ts e="T636" id="Seg_4458" n="e" s="T635">bihigi </ts>
               <ts e="T637" id="Seg_4460" n="e" s="T636">di͡egiler </ts>
               <ts e="T638" id="Seg_4462" n="e" s="T637">delbi </ts>
               <ts e="T639" id="Seg_4464" n="e" s="T638">kɨjŋanan </ts>
               <ts e="T640" id="Seg_4466" n="e" s="T639">baraːn. </ts>
               <ts e="T641" id="Seg_4468" n="e" s="T640">Barɨ </ts>
               <ts e="T642" id="Seg_4470" n="e" s="T641">seriː </ts>
               <ts e="T643" id="Seg_4472" n="e" s="T642">saːtɨnan </ts>
               <ts e="T644" id="Seg_4474" n="e" s="T643">delbi </ts>
               <ts e="T645" id="Seg_4476" n="e" s="T644">ɨtɨ͡alaːn </ts>
               <ts e="T646" id="Seg_4478" n="e" s="T645">baraːn. </ts>
               <ts e="T647" id="Seg_4480" n="e" s="T646">Töhö </ts>
               <ts e="T648" id="Seg_4482" n="e" s="T647">eme </ts>
               <ts e="T649" id="Seg_4484" n="e" s="T648">u͡onnuː </ts>
               <ts e="T650" id="Seg_4486" n="e" s="T649">samalʼottarɨ </ts>
               <ts e="T651" id="Seg_4488" n="e" s="T650">oŋu͡orgu </ts>
               <ts e="T652" id="Seg_4490" n="e" s="T651">öttüge </ts>
               <ts e="T653" id="Seg_4492" n="e" s="T652">ɨːtan </ts>
               <ts e="T654" id="Seg_4494" n="e" s="T653">baraːn. </ts>
               <ts e="T655" id="Seg_4496" n="e" s="T654">Itini </ts>
               <ts e="T656" id="Seg_4498" n="e" s="T655">barɨtɨn </ts>
               <ts e="T657" id="Seg_4500" n="e" s="T656">bürünen </ts>
               <ts e="T658" id="Seg_4502" n="e" s="T657">turan </ts>
               <ts e="T659" id="Seg_4504" n="e" s="T658">oŋu͡orgu </ts>
               <ts e="T660" id="Seg_4506" n="e" s="T659">di͡ek </ts>
               <ts e="T661" id="Seg_4508" n="e" s="T660">bɨha </ts>
               <ts e="T662" id="Seg_4510" n="e" s="T661">mökkühe </ts>
               <ts e="T663" id="Seg_4512" n="e" s="T662">taksɨbɨttara. </ts>
               <ts e="T664" id="Seg_4514" n="e" s="T663">Bihigi </ts>
               <ts e="T665" id="Seg_4516" n="e" s="T664">saldaːttarbɨt </ts>
               <ts e="T666" id="Seg_4518" n="e" s="T665">ol </ts>
               <ts e="T667" id="Seg_4520" n="e" s="T666">mökkü͡ön </ts>
               <ts e="T668" id="Seg_4522" n="e" s="T667">ajdaːnɨgar </ts>
               <ts e="T669" id="Seg_4524" n="e" s="T668">mostaga </ts>
               <ts e="T670" id="Seg_4526" n="e" s="T669">bappakka. </ts>
               <ts e="T671" id="Seg_4528" n="e" s="T670">Aŋar </ts>
               <ts e="T672" id="Seg_4530" n="e" s="T671">kihi </ts>
               <ts e="T673" id="Seg_4532" n="e" s="T672">lu͡otkannan, </ts>
               <ts e="T674" id="Seg_4534" n="e" s="T673">soroktor </ts>
               <ts e="T675" id="Seg_4536" n="e" s="T674">kastɨː </ts>
               <ts e="T676" id="Seg_4538" n="e" s="T675">emi͡e </ts>
               <ts e="T677" id="Seg_4540" n="e" s="T676">bremnu͡olarɨ </ts>
               <ts e="T678" id="Seg_4542" n="e" s="T677">biːrge </ts>
               <ts e="T679" id="Seg_4544" n="e" s="T678">kolbuː </ts>
               <ts e="T680" id="Seg_4546" n="e" s="T679">baːjan </ts>
               <ts e="T681" id="Seg_4548" n="e" s="T680">baraːn, </ts>
               <ts e="T682" id="Seg_4550" n="e" s="T681">aŋardar </ts>
               <ts e="T683" id="Seg_4552" n="e" s="T682">köŋül </ts>
               <ts e="T684" id="Seg_4554" n="e" s="T683">karbaːn </ts>
               <ts e="T685" id="Seg_4556" n="e" s="T684">taksɨbɨttara </ts>
               <ts e="T686" id="Seg_4558" n="e" s="T685">Dnʼepr </ts>
               <ts e="T687" id="Seg_4560" n="e" s="T686">di͡en </ts>
               <ts e="T688" id="Seg_4562" n="e" s="T687">ebeni. </ts>
               <ts e="T689" id="Seg_4564" n="e" s="T688">Onno </ts>
               <ts e="T690" id="Seg_4566" n="e" s="T689">ajdaːn, </ts>
               <ts e="T691" id="Seg_4568" n="e" s="T690">ulakan </ts>
               <ts e="T692" id="Seg_4570" n="e" s="T691">ü͡ögüː, </ts>
               <ts e="T693" id="Seg_4572" n="e" s="T692">kahɨː, </ts>
               <ts e="T694" id="Seg_4574" n="e" s="T693">kersiː, </ts>
               <ts e="T695" id="Seg_4576" n="e" s="T694">tu͡oktan </ts>
               <ts e="T696" id="Seg_4578" n="e" s="T695">da </ts>
               <ts e="T697" id="Seg_4580" n="e" s="T696">atɨn </ts>
               <ts e="T698" id="Seg_4582" n="e" s="T697">seriː </ts>
               <ts e="T699" id="Seg_4584" n="e" s="T698">saːtɨn </ts>
               <ts e="T700" id="Seg_4586" n="e" s="T699">tɨ͡aha </ts>
               <ts e="T701" id="Seg_4588" n="e" s="T700">sürdeːk </ts>
               <ts e="T702" id="Seg_4590" n="e" s="T701">ete. </ts>
               <ts e="T703" id="Seg_4592" n="e" s="T702">Nʼemʼes </ts>
               <ts e="T704" id="Seg_4594" n="e" s="T703">di͡egi </ts>
               <ts e="T705" id="Seg_4596" n="e" s="T704">öttü, </ts>
               <ts e="T706" id="Seg_4598" n="e" s="T705">iti </ts>
               <ts e="T707" id="Seg_4600" n="e" s="T706">bɨha </ts>
               <ts e="T708" id="Seg_4602" n="e" s="T707">mökküjüːnü </ts>
               <ts e="T709" id="Seg_4604" n="e" s="T708">koton </ts>
               <ts e="T710" id="Seg_4606" n="e" s="T709">tuppakka, </ts>
               <ts e="T711" id="Seg_4608" n="e" s="T710">meːne </ts>
               <ts e="T712" id="Seg_4610" n="e" s="T711">ebe </ts>
               <ts e="T713" id="Seg_4612" n="e" s="T712">ürdünen </ts>
               <ts e="T714" id="Seg_4614" n="e" s="T713">ɨtɨ͡alɨːr </ts>
               <ts e="T715" id="Seg_4616" n="e" s="T714">ete. </ts>
               <ts e="T716" id="Seg_4618" n="e" s="T715">Kɨtɨltan </ts>
               <ts e="T717" id="Seg_4620" n="e" s="T716">kördökkö </ts>
               <ts e="T718" id="Seg_4622" n="e" s="T717">ebe </ts>
               <ts e="T719" id="Seg_4624" n="e" s="T718">di͡ek, </ts>
               <ts e="T720" id="Seg_4626" n="e" s="T719">uːga </ts>
               <ts e="T721" id="Seg_4628" n="e" s="T720">bukatɨn </ts>
               <ts e="T722" id="Seg_4630" n="e" s="T721">kihi </ts>
               <ts e="T723" id="Seg_4632" n="e" s="T722">tugu </ts>
               <ts e="T724" id="Seg_4634" n="e" s="T723">da </ts>
               <ts e="T725" id="Seg_4636" n="e" s="T724">tu͡olkulu͡ok </ts>
               <ts e="T726" id="Seg_4638" n="e" s="T725">bu͡olbatak </ts>
               <ts e="T727" id="Seg_4640" n="e" s="T726">ete. </ts>
               <ts e="T728" id="Seg_4642" n="e" s="T727">Kolobura, </ts>
               <ts e="T729" id="Seg_4644" n="e" s="T728">ürgübüt </ts>
               <ts e="T730" id="Seg_4646" n="e" s="T729">ulakan </ts>
               <ts e="T731" id="Seg_4648" n="e" s="T730">kɨːl </ts>
               <ts e="T732" id="Seg_4650" n="e" s="T731">ü͡öre </ts>
               <ts e="T733" id="Seg_4652" n="e" s="T732">karbɨːrɨn </ts>
               <ts e="T734" id="Seg_4654" n="e" s="T733">kördük </ts>
               <ts e="T735" id="Seg_4656" n="e" s="T734">ete. </ts>
               <ts e="T736" id="Seg_4658" n="e" s="T735">Iti </ts>
               <ts e="T737" id="Seg_4660" n="e" s="T736">ebe </ts>
               <ts e="T738" id="Seg_4662" n="e" s="T737">kehiːtiger </ts>
               <ts e="T739" id="Seg_4664" n="e" s="T738">töhölöːk </ts>
               <ts e="T740" id="Seg_4666" n="e" s="T739">kihi </ts>
               <ts e="T741" id="Seg_4668" n="e" s="T740">tumnastan </ts>
               <ts e="T742" id="Seg_4670" n="e" s="T741">seriː </ts>
               <ts e="T743" id="Seg_4672" n="e" s="T742">saːtɨttan </ts>
               <ts e="T744" id="Seg_4674" n="e" s="T743">ölbütün </ts>
               <ts e="T745" id="Seg_4676" n="e" s="T744">kim </ts>
               <ts e="T746" id="Seg_4678" n="e" s="T745">daːgɨnɨ </ts>
               <ts e="T747" id="Seg_4680" n="e" s="T746">bilbet </ts>
               <ts e="T748" id="Seg_4682" n="e" s="T747">oččogo </ts>
               <ts e="T749" id="Seg_4684" n="e" s="T748">ete. </ts>
               <ts e="T750" id="Seg_4686" n="e" s="T749">Ebeni </ts>
               <ts e="T751" id="Seg_4688" n="e" s="T750">taksan </ts>
               <ts e="T752" id="Seg_4690" n="e" s="T751">baraːn </ts>
               <ts e="T753" id="Seg_4692" n="e" s="T752">bihi͡ettere </ts>
               <ts e="T754" id="Seg_4694" n="e" s="T753">emi͡e </ts>
               <ts e="T755" id="Seg_4696" n="e" s="T754">nʼemʼesteri </ts>
               <ts e="T756" id="Seg_4698" n="e" s="T755">solo </ts>
               <ts e="T757" id="Seg_4700" n="e" s="T756">bullarbakka </ts>
               <ts e="T758" id="Seg_4702" n="e" s="T757">delbi </ts>
               <ts e="T759" id="Seg_4704" n="e" s="T758">bappɨttara. </ts>
               <ts e="T760" id="Seg_4706" n="e" s="T759">Onton </ts>
               <ts e="T761" id="Seg_4708" n="e" s="T760">min </ts>
               <ts e="T762" id="Seg_4710" n="e" s="T761">töhö </ts>
               <ts e="T763" id="Seg_4712" n="e" s="T762">emete </ts>
               <ts e="T764" id="Seg_4714" n="e" s="T763">seriːge </ts>
               <ts e="T765" id="Seg_4716" n="e" s="T764">barammɨn, </ts>
               <ts e="T766" id="Seg_4718" n="e" s="T765">tördüsteːk </ts>
               <ts e="T767" id="Seg_4720" n="e" s="T766">künüger </ts>
               <ts e="T768" id="Seg_4722" n="e" s="T767">janvarʼ </ts>
               <ts e="T769" id="Seg_4724" n="e" s="T768">ɨjga, </ts>
               <ts e="T770" id="Seg_4726" n="e" s="T769">tɨːhɨčča </ts>
               <ts e="T771" id="Seg_4728" n="e" s="T770">togus </ts>
               <ts e="T772" id="Seg_4730" n="e" s="T771">süːs </ts>
               <ts e="T773" id="Seg_4732" n="e" s="T772">tü͡örd-u͡on </ts>
               <ts e="T774" id="Seg_4734" n="e" s="T773">üs </ts>
               <ts e="T775" id="Seg_4736" n="e" s="T774">dʼɨllaːkka </ts>
               <ts e="T776" id="Seg_4738" n="e" s="T775">bütügespin </ts>
               <ts e="T777" id="Seg_4740" n="e" s="T776">gojobuːnnappɨtɨm </ts>
               <ts e="T778" id="Seg_4742" n="e" s="T777">bert </ts>
               <ts e="T779" id="Seg_4744" n="e" s="T778">ɨ͡arakannɨk, </ts>
               <ts e="T780" id="Seg_4746" n="e" s="T779">kaŋas </ts>
               <ts e="T781" id="Seg_4748" n="e" s="T780">di͡eki </ts>
               <ts e="T782" id="Seg_4750" n="e" s="T781">buːtum </ts>
               <ts e="T783" id="Seg_4752" n="e" s="T782">oŋu͡ogun </ts>
               <ts e="T784" id="Seg_4754" n="e" s="T783">delbi </ts>
               <ts e="T785" id="Seg_4756" n="e" s="T784">tohuttarbɨtɨm. </ts>
               <ts e="T786" id="Seg_4758" n="e" s="T785">Ihini </ts>
               <ts e="T787" id="Seg_4760" n="e" s="T786">emtetemmin </ts>
               <ts e="T788" id="Seg_4762" n="e" s="T787">ohu͡okpar </ts>
               <ts e="T789" id="Seg_4764" n="e" s="T788">di͡eri </ts>
               <ts e="T790" id="Seg_4766" n="e" s="T789">alta </ts>
               <ts e="T791" id="Seg_4768" n="e" s="T790">ɨj </ts>
               <ts e="T792" id="Seg_4770" n="e" s="T791">turkarɨ </ts>
               <ts e="T793" id="Seg_4772" n="e" s="T792">sɨppɨtɨm </ts>
               <ts e="T794" id="Seg_4774" n="e" s="T793">Essentuki </ts>
               <ts e="T795" id="Seg_4776" n="e" s="T794">di͡en </ts>
               <ts e="T796" id="Seg_4778" n="e" s="T795">gu͡orakka, </ts>
               <ts e="T797" id="Seg_4780" n="e" s="T796">Kavkaːz </ts>
               <ts e="T798" id="Seg_4782" n="e" s="T797">di͡en </ts>
               <ts e="T799" id="Seg_4784" n="e" s="T798">itiː </ts>
               <ts e="T800" id="Seg_4786" n="e" s="T799">hirge. </ts>
               <ts e="T801" id="Seg_4788" n="e" s="T800">Onton </ts>
               <ts e="T802" id="Seg_4790" n="e" s="T801">emteten </ts>
               <ts e="T803" id="Seg_4792" n="e" s="T802">baraːn, </ts>
               <ts e="T804" id="Seg_4794" n="e" s="T803">mas </ts>
               <ts e="T805" id="Seg_4796" n="e" s="T804">tanʼaktaːk </ts>
               <ts e="T806" id="Seg_4798" n="e" s="T805">taksan </ts>
               <ts e="T807" id="Seg_4800" n="e" s="T806">baraːmmɨn. </ts>
               <ts e="T808" id="Seg_4802" n="e" s="T807">Seriːtten </ts>
               <ts e="T809" id="Seg_4804" n="e" s="T808">adʼas </ts>
               <ts e="T810" id="Seg_4806" n="e" s="T809">boskolommutum, </ts>
               <ts e="T811" id="Seg_4808" n="e" s="T810">armʼijattan </ts>
               <ts e="T812" id="Seg_4810" n="e" s="T811">sаvsʼem </ts>
               <ts e="T813" id="Seg_4812" n="e" s="T812">taksɨbɨtɨm. </ts>
               <ts e="T814" id="Seg_4814" n="e" s="T813">Maŋnaj </ts>
               <ts e="T815" id="Seg_4816" n="e" s="T814">seriːlespit </ts>
               <ts e="T816" id="Seg_4818" n="e" s="T815">kümmütten, </ts>
               <ts e="T817" id="Seg_4820" n="e" s="T816">biːr </ts>
               <ts e="T818" id="Seg_4822" n="e" s="T817">dʼɨl </ts>
               <ts e="T819" id="Seg_4824" n="e" s="T818">aŋarɨn </ts>
               <ts e="T820" id="Seg_4826" n="e" s="T819">turkarɨ, </ts>
               <ts e="T821" id="Seg_4828" n="e" s="T820">seriː </ts>
               <ts e="T822" id="Seg_4830" n="e" s="T821">kersiːtiger </ts>
               <ts e="T823" id="Seg_4832" n="e" s="T822">sɨldʼammɨn, </ts>
               <ts e="T824" id="Seg_4834" n="e" s="T823">barɨta </ts>
               <ts e="T825" id="Seg_4836" n="e" s="T824">tü͡örka </ts>
               <ts e="T826" id="Seg_4838" n="e" s="T825">kat </ts>
               <ts e="T827" id="Seg_4840" n="e" s="T826">gojobuːnnappɨtɨm. </ts>
               <ts e="T828" id="Seg_4842" n="e" s="T827">Iti </ts>
               <ts e="T829" id="Seg_4844" n="e" s="T828">tuhunan </ts>
               <ts e="T830" id="Seg_4846" n="e" s="T829">üste </ts>
               <ts e="T831" id="Seg_4848" n="e" s="T830">emtete </ts>
               <ts e="T832" id="Seg_4850" n="e" s="T831">hɨldʼɨbɨtɨm. </ts>
               <ts e="T833" id="Seg_4852" n="e" s="T832">Kɨlgastɨk </ts>
               <ts e="T834" id="Seg_4854" n="e" s="T833">kepseːtekke, </ts>
               <ts e="T835" id="Seg_4856" n="e" s="T834">itigirdik </ts>
               <ts e="T836" id="Seg_4858" n="e" s="T835">min </ts>
               <ts e="T837" id="Seg_4860" n="e" s="T836">kihi </ts>
               <ts e="T838" id="Seg_4862" n="e" s="T837">aːjmak </ts>
               <ts e="T839" id="Seg_4864" n="e" s="T838">üjetiger </ts>
               <ts e="T840" id="Seg_4866" n="e" s="T839">kaččaga </ts>
               <ts e="T841" id="Seg_4868" n="e" s="T840">da </ts>
               <ts e="T842" id="Seg_4870" n="e" s="T841">billibetek </ts>
               <ts e="T843" id="Seg_4872" n="e" s="T842">aga </ts>
               <ts e="T844" id="Seg_4874" n="e" s="T843">da </ts>
               <ts e="T845" id="Seg_4876" n="e" s="T844">iti </ts>
               <ts e="T846" id="Seg_4878" n="e" s="T845">ulakan </ts>
               <ts e="T847" id="Seg_4880" n="e" s="T846">seriːtiger </ts>
               <ts e="T848" id="Seg_4882" n="e" s="T847">bejem </ts>
               <ts e="T849" id="Seg_4884" n="e" s="T848">dʼommun, </ts>
               <ts e="T850" id="Seg_4886" n="e" s="T849">töröːbüt, </ts>
               <ts e="T851" id="Seg_4888" n="e" s="T850">ü͡öskeːbit </ts>
               <ts e="T852" id="Seg_4890" n="e" s="T851">sirbin </ts>
               <ts e="T853" id="Seg_4892" n="e" s="T852">kömüsküː </ts>
               <ts e="T854" id="Seg_4894" n="e" s="T853">hɨldʼɨbɨtɨm. </ts>
               <ts e="T855" id="Seg_4896" n="e" s="T854">Mini͡ene </ts>
               <ts e="T856" id="Seg_4898" n="e" s="T855">kaːnɨm, </ts>
               <ts e="T857" id="Seg_4900" n="e" s="T856">kölöhünüm </ts>
               <ts e="T858" id="Seg_4902" n="e" s="T857">iti </ts>
               <ts e="T859" id="Seg_4904" n="e" s="T858">seriːge </ts>
               <ts e="T860" id="Seg_4906" n="e" s="T859">köŋül </ts>
               <ts e="T861" id="Seg_4908" n="e" s="T860">toktubataga. </ts>
               <ts e="T862" id="Seg_4910" n="e" s="T861">Bihi͡ene </ts>
               <ts e="T863" id="Seg_4912" n="e" s="T862">kammunʼistʼičʼeskaj </ts>
               <ts e="T864" id="Seg_4914" n="e" s="T863">partʼijabɨt </ts>
               <ts e="T865" id="Seg_4916" n="e" s="T864">kiːn </ts>
               <ts e="T866" id="Seg_4918" n="e" s="T865">kаmitʼeta </ts>
               <ts e="T867" id="Seg_4920" n="e" s="T866">onton </ts>
               <ts e="T868" id="Seg_4922" n="e" s="T867">bihi͡ene </ts>
               <ts e="T869" id="Seg_4924" n="e" s="T868">gasudarstvabɨtɨn </ts>
               <ts e="T870" id="Seg_4926" n="e" s="T869">ologun </ts>
               <ts e="T871" id="Seg_4928" n="e" s="T870">könnöröːččü </ts>
               <ts e="T872" id="Seg_4930" n="e" s="T871">pravʼitʼelʼstvabɨt </ts>
               <ts e="T873" id="Seg_4932" n="e" s="T872">bi͡erbittere </ts>
               <ts e="T874" id="Seg_4934" n="e" s="T873">mini͡eke </ts>
               <ts e="T875" id="Seg_4936" n="e" s="T874">gasudarstva </ts>
               <ts e="T876" id="Seg_4938" n="e" s="T875">ürdük </ts>
               <ts e="T877" id="Seg_4940" n="e" s="T876">seriːtin </ts>
               <ts e="T878" id="Seg_4942" n="e" s="T877">ordʼenɨn </ts>
               <ts e="T881" id="Seg_4944" n="e" s="T878">"Bajevova Kraːsnava Znamʼenʼi" </ts>
               <ts e="T882" id="Seg_4946" n="e" s="T881">di͡en. </ts>
               <ts e="T883" id="Seg_4948" n="e" s="T882">Onton </ts>
               <ts e="T884" id="Seg_4950" n="e" s="T883">össü͡ö </ts>
               <ts e="T885" id="Seg_4952" n="e" s="T884">mʼedalʼi </ts>
               <ts e="T889" id="Seg_4954" n="e" s="T885">"Za pabʼedu nad Gʼermanʼijej" </ts>
               <ts e="T890" id="Seg_4956" n="e" s="T889">di͡en. </ts>
               <ts e="T891" id="Seg_4958" n="e" s="T890">Anɨ </ts>
               <ts e="T892" id="Seg_4960" n="e" s="T891">bihigi </ts>
               <ts e="T893" id="Seg_4962" n="e" s="T892">Savʼetskaj </ts>
               <ts e="T894" id="Seg_4964" n="e" s="T893">ologur </ts>
               <ts e="T895" id="Seg_4966" n="e" s="T894">oloror </ts>
               <ts e="T896" id="Seg_4968" n="e" s="T895">kihiler – </ts>
               <ts e="T897" id="Seg_4970" n="e" s="T896">olokkut </ts>
               <ts e="T898" id="Seg_4972" n="e" s="T897">berke </ts>
               <ts e="T899" id="Seg_4974" n="e" s="T898">tuksan </ts>
               <ts e="T900" id="Seg_4976" n="e" s="T899">turar </ts>
               <ts e="T901" id="Seg_4978" n="e" s="T900">inni </ts>
               <ts e="T902" id="Seg_4980" n="e" s="T901">di͡ek </ts>
               <ts e="T903" id="Seg_4982" n="e" s="T902">barar </ts>
               <ts e="T904" id="Seg_4984" n="e" s="T903">küːske. </ts>
               <ts e="T905" id="Seg_4986" n="e" s="T904">Ol </ts>
               <ts e="T906" id="Seg_4988" n="e" s="T905">ihin </ts>
               <ts e="T907" id="Seg_4990" n="e" s="T906">kim </ts>
               <ts e="T908" id="Seg_4992" n="e" s="T907">daːganɨ </ts>
               <ts e="T909" id="Seg_4994" n="e" s="T908">anɨ </ts>
               <ts e="T910" id="Seg_4996" n="e" s="T909">seriːgin </ts>
               <ts e="T911" id="Seg_4998" n="e" s="T910">bu͡olu͡ogun </ts>
               <ts e="T912" id="Seg_5000" n="e" s="T911">bagarbat. </ts>
               <ts e="T913" id="Seg_5002" n="e" s="T912">Bihigi </ts>
               <ts e="T914" id="Seg_5004" n="e" s="T913">sirbit </ts>
               <ts e="T915" id="Seg_5006" n="e" s="T914">kihite </ts>
               <ts e="T916" id="Seg_5008" n="e" s="T915">urukkuttan </ts>
               <ts e="T917" id="Seg_5010" n="e" s="T916">daːganɨ </ts>
               <ts e="T918" id="Seg_5012" n="e" s="T917">seriːge </ts>
               <ts e="T919" id="Seg_5014" n="e" s="T918">bagaraːččɨta </ts>
               <ts e="T920" id="Seg_5016" n="e" s="T919">hu͡ok </ts>
               <ts e="T921" id="Seg_5018" n="e" s="T920">ete. </ts>
               <ts e="T922" id="Seg_5020" n="e" s="T921">Bihigi </ts>
               <ts e="T923" id="Seg_5022" n="e" s="T922">dʼommut </ts>
               <ts e="T924" id="Seg_5024" n="e" s="T923">anɨ </ts>
               <ts e="T925" id="Seg_5026" n="e" s="T924">bejetin </ts>
               <ts e="T926" id="Seg_5028" n="e" s="T925">sanaːta </ts>
               <ts e="T927" id="Seg_5030" n="e" s="T926">söbülüːrün </ts>
               <ts e="T928" id="Seg_5032" n="e" s="T927">sɨrdɨk, </ts>
               <ts e="T929" id="Seg_5034" n="e" s="T928">dʼolloːk </ts>
               <ts e="T930" id="Seg_5036" n="e" s="T929">Lʼenʼin </ts>
               <ts e="T931" id="Seg_5038" n="e" s="T930">ɨjbɨt </ts>
               <ts e="T932" id="Seg_5040" n="e" s="T931">kommunʼist </ts>
               <ts e="T933" id="Seg_5042" n="e" s="T932">aːttaːk </ts>
               <ts e="T934" id="Seg_5044" n="e" s="T933">ologun </ts>
               <ts e="T935" id="Seg_5046" n="e" s="T934">oŋostor. </ts>
               <ts e="T936" id="Seg_5048" n="e" s="T935">Bihigi </ts>
               <ts e="T937" id="Seg_5050" n="e" s="T936">barɨbɨt </ts>
               <ts e="T938" id="Seg_5052" n="e" s="T937">diːbit </ts>
               <ts e="T939" id="Seg_5054" n="e" s="T938">anɨ: </ts>
               <ts e="T940" id="Seg_5056" n="e" s="T939">"Seriː </ts>
               <ts e="T941" id="Seg_5058" n="e" s="T940">su͡ok </ts>
               <ts e="T942" id="Seg_5060" n="e" s="T941">bu͡ollun </ts>
               <ts e="T943" id="Seg_5062" n="e" s="T942">sir </ts>
               <ts e="T944" id="Seg_5064" n="e" s="T943">ürdünen. </ts>
               <ts e="T945" id="Seg_5066" n="e" s="T944">Baːr </ts>
               <ts e="T946" id="Seg_5068" n="e" s="T945">bu͡ollun </ts>
               <ts e="T947" id="Seg_5070" n="e" s="T946">sɨrdɨk, </ts>
               <ts e="T948" id="Seg_5072" n="e" s="T947">künneːk </ts>
               <ts e="T949" id="Seg_5074" n="e" s="T948">olok </ts>
               <ts e="T950" id="Seg_5076" n="e" s="T949">barɨ </ts>
               <ts e="T951" id="Seg_5078" n="e" s="T950">joŋŋo". </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref">
            <ta e="T11" id="Seg_5079" s="T0">MiXS_1967_SoldierInSecondWorldWar_nar.001 (001.001)</ta>
            <ta e="T16" id="Seg_5080" s="T11">MiXS_1967_SoldierInSecondWorldWar_nar.002 (001.002)</ta>
            <ta e="T31" id="Seg_5081" s="T16">MiXS_1967_SoldierInSecondWorldWar_nar.003 (001.003)</ta>
            <ta e="T48" id="Seg_5082" s="T31">MiXS_1967_SoldierInSecondWorldWar_nar.004 (001.004)</ta>
            <ta e="T57" id="Seg_5083" s="T48">MiXS_1967_SoldierInSecondWorldWar_nar.005 (001.005)</ta>
            <ta e="T81" id="Seg_5084" s="T57">MiXS_1967_SoldierInSecondWorldWar_nar.006 (001.006)</ta>
            <ta e="T89" id="Seg_5085" s="T81">MiXS_1967_SoldierInSecondWorldWar_nar.007 (001.007)</ta>
            <ta e="T96" id="Seg_5086" s="T89">MiXS_1967_SoldierInSecondWorldWar_nar.008 (001.008)</ta>
            <ta e="T102" id="Seg_5087" s="T96">MiXS_1967_SoldierInSecondWorldWar_nar.009 (001.009)</ta>
            <ta e="T117" id="Seg_5088" s="T102">MiXS_1967_SoldierInSecondWorldWar_nar.010 (001.010)</ta>
            <ta e="T135" id="Seg_5089" s="T117">MiXS_1967_SoldierInSecondWorldWar_nar.011 (001.011)</ta>
            <ta e="T142" id="Seg_5090" s="T135">MiXS_1967_SoldierInSecondWorldWar_nar.012 (001.012)</ta>
            <ta e="T146" id="Seg_5091" s="T142">MiXS_1967_SoldierInSecondWorldWar_nar.013 (001.013)</ta>
            <ta e="T152" id="Seg_5092" s="T146">MiXS_1967_SoldierInSecondWorldWar_nar.014 (001.014)</ta>
            <ta e="T184" id="Seg_5093" s="T152">MiXS_1967_SoldierInSecondWorldWar_nar.015 (001.015)</ta>
            <ta e="T196" id="Seg_5094" s="T184">MiXS_1967_SoldierInSecondWorldWar_nar.016 (001.016)</ta>
            <ta e="T214" id="Seg_5095" s="T196">MiXS_1967_SoldierInSecondWorldWar_nar.017 (001.017)</ta>
            <ta e="T226" id="Seg_5096" s="T214">MiXS_1967_SoldierInSecondWorldWar_nar.018 (001.018)</ta>
            <ta e="T239" id="Seg_5097" s="T226">MiXS_1967_SoldierInSecondWorldWar_nar.019 (001.019)</ta>
            <ta e="T257" id="Seg_5098" s="T239">MiXS_1967_SoldierInSecondWorldWar_nar.020 (001.020)</ta>
            <ta e="T266" id="Seg_5099" s="T257">MiXS_1967_SoldierInSecondWorldWar_nar.021 (001.021)</ta>
            <ta e="T279" id="Seg_5100" s="T266">MiXS_1967_SoldierInSecondWorldWar_nar.022 (001.022)</ta>
            <ta e="T288" id="Seg_5101" s="T279">MiXS_1967_SoldierInSecondWorldWar_nar.023 (001.023)</ta>
            <ta e="T303" id="Seg_5102" s="T288">MiXS_1967_SoldierInSecondWorldWar_nar.024 (001.024)</ta>
            <ta e="T309" id="Seg_5103" s="T303">MiXS_1967_SoldierInSecondWorldWar_nar.025 (001.025)</ta>
            <ta e="T316" id="Seg_5104" s="T309">MiXS_1967_SoldierInSecondWorldWar_nar.026 (001.026)</ta>
            <ta e="T325" id="Seg_5105" s="T316">MiXS_1967_SoldierInSecondWorldWar_nar.027 (001.027)</ta>
            <ta e="T332" id="Seg_5106" s="T325">MiXS_1967_SoldierInSecondWorldWar_nar.028 (001.028)</ta>
            <ta e="T341" id="Seg_5107" s="T332">MiXS_1967_SoldierInSecondWorldWar_nar.029 (001.029)</ta>
            <ta e="T346" id="Seg_5108" s="T341">MiXS_1967_SoldierInSecondWorldWar_nar.030 (001.030)</ta>
            <ta e="T363" id="Seg_5109" s="T346">MiXS_1967_SoldierInSecondWorldWar_nar.031 (001.031)</ta>
            <ta e="T376" id="Seg_5110" s="T363">MiXS_1967_SoldierInSecondWorldWar_nar.032 (001.032)</ta>
            <ta e="T387" id="Seg_5111" s="T376">MiXS_1967_SoldierInSecondWorldWar_nar.033 (001.033)</ta>
            <ta e="T400" id="Seg_5112" s="T387">MiXS_1967_SoldierInSecondWorldWar_nar.034 (001.034)</ta>
            <ta e="T405" id="Seg_5113" s="T400">MiXS_1967_SoldierInSecondWorldWar_nar.035 (001.035)</ta>
            <ta e="T411" id="Seg_5114" s="T405">MiXS_1967_SoldierInSecondWorldWar_nar.036 (001.036)</ta>
            <ta e="T426" id="Seg_5115" s="T411">MiXS_1967_SoldierInSecondWorldWar_nar.037 (001.037)</ta>
            <ta e="T432" id="Seg_5116" s="T426">MiXS_1967_SoldierInSecondWorldWar_nar.038 (001.038)</ta>
            <ta e="T452" id="Seg_5117" s="T432">MiXS_1967_SoldierInSecondWorldWar_nar.039 (001.039)</ta>
            <ta e="T462" id="Seg_5118" s="T452">MiXS_1967_SoldierInSecondWorldWar_nar.040 (001.040)</ta>
            <ta e="T478" id="Seg_5119" s="T462">MiXS_1967_SoldierInSecondWorldWar_nar.041 (001.041)</ta>
            <ta e="T490" id="Seg_5120" s="T478">MiXS_1967_SoldierInSecondWorldWar_nar.042 (001.042)</ta>
            <ta e="T510" id="Seg_5121" s="T490">MiXS_1967_SoldierInSecondWorldWar_nar.043 (001.043)</ta>
            <ta e="T538" id="Seg_5122" s="T510">MiXS_1967_SoldierInSecondWorldWar_nar.044 (001.044)</ta>
            <ta e="T549" id="Seg_5123" s="T538">MiXS_1967_SoldierInSecondWorldWar_nar.045 (001.045)</ta>
            <ta e="T558" id="Seg_5124" s="T549">MiXS_1967_SoldierInSecondWorldWar_nar.046 (001.046)</ta>
            <ta e="T564" id="Seg_5125" s="T558">MiXS_1967_SoldierInSecondWorldWar_nar.047 (001.047)</ta>
            <ta e="T575" id="Seg_5126" s="T564">MiXS_1967_SoldierInSecondWorldWar_nar.048 (001.048)</ta>
            <ta e="T589" id="Seg_5127" s="T575">MiXS_1967_SoldierInSecondWorldWar_nar.049 (001.049)</ta>
            <ta e="T600" id="Seg_5128" s="T589">MiXS_1967_SoldierInSecondWorldWar_nar.050 (001.050)</ta>
            <ta e="T616" id="Seg_5129" s="T600">MiXS_1967_SoldierInSecondWorldWar_nar.051 (001.051)</ta>
            <ta e="T626" id="Seg_5130" s="T616">MiXS_1967_SoldierInSecondWorldWar_nar.052 (001.052)</ta>
            <ta e="T634" id="Seg_5131" s="T626">MiXS_1967_SoldierInSecondWorldWar_nar.053 (001.053)</ta>
            <ta e="T640" id="Seg_5132" s="T634">MiXS_1967_SoldierInSecondWorldWar_nar.054 (001.054)</ta>
            <ta e="T646" id="Seg_5133" s="T640">MiXS_1967_SoldierInSecondWorldWar_nar.055 (001.055)</ta>
            <ta e="T654" id="Seg_5134" s="T646">MiXS_1967_SoldierInSecondWorldWar_nar.056 (001.056)</ta>
            <ta e="T663" id="Seg_5135" s="T654">MiXS_1967_SoldierInSecondWorldWar_nar.057 (001.057)</ta>
            <ta e="T670" id="Seg_5136" s="T663">MiXS_1967_SoldierInSecondWorldWar_nar.058 (001.058)</ta>
            <ta e="T688" id="Seg_5137" s="T670">MiXS_1967_SoldierInSecondWorldWar_nar.059 (001.059)</ta>
            <ta e="T702" id="Seg_5138" s="T688">MiXS_1967_SoldierInSecondWorldWar_nar.060 (001.060)</ta>
            <ta e="T715" id="Seg_5139" s="T702">MiXS_1967_SoldierInSecondWorldWar_nar.061 (001.061)</ta>
            <ta e="T727" id="Seg_5140" s="T715">MiXS_1967_SoldierInSecondWorldWar_nar.062 (001.062)</ta>
            <ta e="T735" id="Seg_5141" s="T727">MiXS_1967_SoldierInSecondWorldWar_nar.063 (001.063)</ta>
            <ta e="T749" id="Seg_5142" s="T735">MiXS_1967_SoldierInSecondWorldWar_nar.064 (001.064)</ta>
            <ta e="T759" id="Seg_5143" s="T749">MiXS_1967_SoldierInSecondWorldWar_nar.065 (001.065)</ta>
            <ta e="T785" id="Seg_5144" s="T759">MiXS_1967_SoldierInSecondWorldWar_nar.066 (001.066)</ta>
            <ta e="T800" id="Seg_5145" s="T785">MiXS_1967_SoldierInSecondWorldWar_nar.067 (001.067)</ta>
            <ta e="T807" id="Seg_5146" s="T800">MiXS_1967_SoldierInSecondWorldWar_nar.068 (001.068)</ta>
            <ta e="T813" id="Seg_5147" s="T807">MiXS_1967_SoldierInSecondWorldWar_nar.069 (001.069)</ta>
            <ta e="T827" id="Seg_5148" s="T813">MiXS_1967_SoldierInSecondWorldWar_nar.070 (001.070)</ta>
            <ta e="T832" id="Seg_5149" s="T827">MiXS_1967_SoldierInSecondWorldWar_nar.071 (001.071)</ta>
            <ta e="T854" id="Seg_5150" s="T832">MiXS_1967_SoldierInSecondWorldWar_nar.072 (001.072)</ta>
            <ta e="T861" id="Seg_5151" s="T854">MiXS_1967_SoldierInSecondWorldWar_nar.073 (001.073)</ta>
            <ta e="T882" id="Seg_5152" s="T861">MiXS_1967_SoldierInSecondWorldWar_nar.074 (001.074)</ta>
            <ta e="T890" id="Seg_5153" s="T882">MiXS_1967_SoldierInSecondWorldWar_nar.075 (001.075)</ta>
            <ta e="T904" id="Seg_5154" s="T890">MiXS_1967_SoldierInSecondWorldWar_nar.076 (001.076)</ta>
            <ta e="T912" id="Seg_5155" s="T904">MiXS_1967_SoldierInSecondWorldWar_nar.077 (001.077)</ta>
            <ta e="T921" id="Seg_5156" s="T912">MiXS_1967_SoldierInSecondWorldWar_nar.078 (001.078)</ta>
            <ta e="T935" id="Seg_5157" s="T921">MiXS_1967_SoldierInSecondWorldWar_nar.079 (001.079)</ta>
            <ta e="T939" id="Seg_5158" s="T935">MiXS_1967_SoldierInSecondWorldWar_nar.080 (001.080)</ta>
            <ta e="T944" id="Seg_5159" s="T939">MiXS_1967_SoldierInSecondWorldWar_nar.081 (001.080)</ta>
            <ta e="T951" id="Seg_5160" s="T944">MiXS_1967_SoldierInSecondWorldWar_nar.082 (001.081)</ta>
         </annotation>
         <annotation name="st" tierref="st">
            <ta e="T11" id="Seg_5161" s="T0">Мин сэриигэ (һэриигэ) барбытым бэйэм санаабынан (һанаабынан) тыыһычча тогус сүүс (һүүс) түөрдуон биир дьыллаакка.</ta>
            <ta e="T16" id="Seg_5162" s="T11">Күһүӈӈү сентябрь, тымныы түһэр ыйыгар. </ta>
            <ta e="T31" id="Seg_5163" s="T16">Маӈнай бараммын, биир дьылы гытта тогус ыйы сэрии (һэрии) үөрэгигэр үөрэнэ сылдьыбытым (һылдьыбытым) Дальний Восток диэн һиргэ.</ta>
            <ta e="T48" id="Seg_5164" s="T31">Онтон сайын (һайын) тыыһычча тогус сүс түөрдуон үс дьыллаакка биһигини ыыппыттара запад диэк сэрии (һэрии) кэрсиитэ бара турар сиригэр (һиригэр).</ta>
            <ta e="T57" id="Seg_5165" s="T48">Биһиги сэриини (һэриини) көрсүбүппүт Прохоровка диэн ааттаак деревняга Курской облаһыгар.</ta>
            <ta e="T81" id="Seg_5166" s="T57">Онно немец сэриитин (һэриитин) киһитэ бэрт үгүс сэрии (һэрии) саатын (һаатын) эӈин атын тэрилтэтин мунньан бараан, биһиги армиябытын бэркэ төттөрү диэк отут биэс биирстэлээк сиргэ (һиргэ) ыгайбыт этэ.</ta>
            <ta e="T89" id="Seg_5167" s="T81">Онтон койут ити сири (һири) Курскайа дуга диэн ааттаабыттара. </ta>
            <ta e="T96" id="Seg_5168" s="T89">Сакалыы (һакалыы) саӈардакка (һаӈардакка) Курскай токура диэккэ һөп буолуога.</ta>
            <ta e="T102" id="Seg_5169" s="T96">Немэс армиятын тойонун санаата (һанаата) баар эбит.</ta>
            <ta e="T117" id="Seg_5170" s="T102">Ити сиринэн (һиринэн) быһа мөккүйэн бараан, бэрт түргэнник биһиги киин сирбитин (һирбитин) Москва диэн гуораты ылар көрдүк.</ta>
            <ta e="T135" id="Seg_5171" s="T117">Биһиэнэ частьпыт Курской токуругар туоктан да улакан сэрии (һэрии) уотугар киирэн бараан, уон күн туркары немэс армиятын бата һатаабыта.</ta>
            <ta e="T142" id="Seg_5172" s="T135">Ити туркары биһиги сэриилэспиппит (һэриилэспиппит) күнү түнү билбэккэ. </ta>
            <ta e="T146" id="Seg_5173" s="T142">Утуйбакка да сынньаммакка (һынньаммакка) да.</ta>
            <ta e="T152" id="Seg_5174" s="T146">Колобура тугу да билбэт истибэт көрдүк. </ta>
            <ta e="T184" id="Seg_5175" s="T152">Икки диэги сэриилэһээччи (һэриилэһээччи) өттүттэн төһөлүөк тыыһычча киһи сүппүтүн (һүппүтүн), өлбүтүн, төһөлөөк киһи гойобуун буолбутун, төһөлөөк сэрии (һэрии) саата (һаата): самолёттар, танкалар, орудиялар онтон кыра эӈин кыра саалар (һаалар), сэрии (һэрии) тэрилтэтэ алдьаммытын ким даагыны чакчы билбэт этэ оччого.</ta>
            <ta e="T196" id="Seg_5176" s="T184">Кайтак (кайдак) улакан күүстээк сэрии (һэрии) барбытын ити һиргэ, киһи туолкулуога биир муннук колобуртан.</ta>
            <ta e="T214" id="Seg_5177" s="T196">Биир (абустаак?) сиркээӈӈэ (һирбитигэр) биһиги баар сирбитигэр, күӈӈэ биирдэ көтөн кэлэээччи этэ немэс диэги өттүттэн икки һүүс кураӈа самолёттар.</ta>
            <ta e="T226" id="Seg_5178" s="T214">Онтуӈ барыта биһиги үрдүбүтүнэн самыыр (һамыыр) көрдүк түһэн э һүүс бомбалары кээһээччи этэ.</ta>
            <ta e="T239" id="Seg_5179" s="T226">Онтон туспа өссүө ытыалыыр этэ төһө эмэтэ (сарпа ?) көрдүк иитиилээк сэрии (һэрии) саатынан (һаатынан) – пулемётынан.</ta>
            <ta e="T257" id="Seg_5180" s="T239">Итиччэлээк бомба түспүтүн кэннэ итиччэлээк саа (һаа) ытыалаабытын кэннэ сир барыта туоккаан да көстүбэт көрдүк, кара туман буолар этэ.</ta>
            <ta e="T266" id="Seg_5181" s="T257">Ити тумаӈӈа киһи аттыга сытар(һытар) киһитин котон көрбөт этэ.</ta>
            <ta e="T279" id="Seg_5182" s="T266">Сорок (һорок) кэннэ киһи сэрии (һэрии) саатын (һаатын) тыаһыттан туоккааны да истибэт көрдүк дүлэй буолар этэ.</ta>
            <ta e="T288" id="Seg_5183" s="T279">Онус күммүтүгэр биһиги диэги өттүннэн сэриини олус күстээктик барда.</ta>
            <ta e="T303" id="Seg_5184" s="T288">Каллаантан, сиртэн бэрт элбэк самолёттар, танкалар, анаан сэриигэ оӈоһуллубут уоттаак саалар, "Катюша", "Андрюша" диэн аттаактар.</ta>
            <ta e="T309" id="Seg_5185" s="T303">Онтон туспа төһө эмэ кыра саалар.</ta>
            <ta e="T316" id="Seg_5186" s="T309">Ол барыта бииргэ огустулар немэстэр баар һирдэрин.</ta>
            <ta e="T325" id="Seg_5187" s="T316">Немэс армията итини долгуннаак улакан оксууну тулуйбакка төттөрү суптуруйбута.</ta>
            <ta e="T332" id="Seg_5188" s="T325">Соннокоон (һоннокоон) уон биэс бэристэлээк сири (һири) төттөрү ыстаммыта.</ta>
            <ta e="T341" id="Seg_5189" s="T332">Ити улакан оксуу кэнниттэн Курскай диэн токура бэрткэ көммүтэ.</ta>
            <ta e="T346" id="Seg_5190" s="T341">Немэс армиятын төттөрү биһиги баппыппыт.</ta>
            <ta e="T363" id="Seg_5191" s="T346">Ити кэнниттэн немэстэр һэрэгэс солдаттара иннилэрин биһиэкэ биэрэн бараан супту(һупту) сэрии( һэрии) бүтүөгүгэр диэри өйдөрүн-пууттарын булбатактара, куотан испиттэрэ.</ta>
            <ta e="T376" id="Seg_5192" s="T363">Биһиги армиябыт бэрт үчүгэйдик күүһүн тутан бараан немэстэри кэнни диэк, көллөрбөккө, дэлби баппыта.</ta>
            <ta e="T387" id="Seg_5193" s="T376">Мин койукку күннэргэ сэриигэ һылдьаммын, итинник сүрдээк киһи ынныта ыстанарын көрдүк көрбөтөгүм.</ta>
            <ta e="T400" id="Seg_5194" s="T387">Мин ити һиргэ саӈардыы(һаӈардыы) көрбүтүм һуостаак кэрсиини: кайтак сэрии киһитин каана үрэктии токторун, </ta>
            <ta e="T405" id="Seg_5195" s="T400">төһөлөөк тыыһычча киһи тыына быстарын, </ta>
            <ta e="T411" id="Seg_5196" s="T405">төһөлөөк гойобуун буолбут киһи улаканнык эрэйдэнэрин.</ta>
            <ta e="T426" id="Seg_5197" s="T411">Мин эмиэ ити оксоһууга Курскай тумуһугар, отделения командира буола һылдьаммын дьогустук каӈас атакпын былчыӈы гойобууннаппытым. </ta>
            <ta e="T432" id="Seg_5198" s="T426">Ол буолан бараан сэриилэһэр сиртэн (һиртэн) таксыбатагым.</ta>
            <ta e="T452" id="Seg_5199" s="T432">Ити сирдээк (һирдэк) оксоһууга мин баар агай чааһым тыыһыччаттан такса сэрии киһититтэн ити уон күӈӈэ оксоһууга орпута уон биир агай киһи.</ta>
            <ta e="T462" id="Seg_5200" s="T452">Ити иһигэр өссүө биир мин гойобуун буолбут киһи баар этим.</ta>
            <ta e="T478" id="Seg_5201" s="T462">Итинтэн сөп буолуога колуокка, кайтак алдьаркайдаак сэрии (һэрии) этин Курскай тогуруугугар: төһө каан токтубутун, төһө киһи өлбүтүн.</ta>
            <ta e="T490" id="Seg_5202" s="T478">Курскайга бүтэн бараан биһиги армиябыт төттөрү күрүүр немэһи запад диэк батан барбыта.</ta>
            <ta e="T510" id="Seg_5203" s="T490">Ити Курскай кэнниттэн мин саамай сэрии кэрсэ сиригэр баар этим тыыһычча тогус сүүс (һүүс) түөрдуон түөрт дьыллаакка, кыһыӈӈы январь ыйга диэри.</ta>
            <ta e="T538" id="Seg_5204" s="T510">Ити кэмӈэ немэс солдаттарын гытта утара туран сэриилэспитим (һэрилэспитим) муннук сирдэргэ (һирдэргэ): Белгород онтон Харьков, онтон Полтава, онтон Кременчуг, онтон Крюков диэн ааттаак улакан гуораттары ыларга, укранинец диэн омук сиригэр.</ta>
            <ta e="T549" id="Seg_5205" s="T538">Ити гуораттартан олус кытаанактык биһиги сэриилэспиппит (һэрилэспиппит) немэһи гытта Харьков гуорат былдьаһыыга.</ta>
            <ta e="T558" id="Seg_5206" s="T549">Ити гуорат үскэ диэри төттөрү-таары илииттэн илиигэ кэлэ һылдьыбыта.</ta>
            <ta e="T564" id="Seg_5207" s="T558">Онтон үһүс кэлиитигэр биһиги илиибиттэн ыыппатакпыт.</ta>
            <ta e="T575" id="Seg_5208" s="T564">Онтон улакан оксoһуу өссүө баар этэ Днепр диэн ааттаак эбэ кэһиитигэр.</ta>
            <ta e="T589" id="Seg_5209" s="T575">Ити эбэ оӈуоргу өттүтүгэр олорон немэстэр күнүстэри-түүннэри кэтии һылдьаннар, биһигини аматтан сатаан кэһэрпэт этилэрэ.</ta>
            <ta e="T600" id="Seg_5210" s="T589">Ол иһин биһиги кас да күнү котон таксыбатакпыт, бэтэрээги кытыылга олорбуппут.</ta>
            <ta e="T616" id="Seg_5211" s="T600">Эбэни таксар мостылары күнүс оӈорорбутун, немэстэр самолёттара көтөн кэлэ-кэлэ төһө эмэ бомбалары бырагаттаан, дэлби алдьатар этэ.</ta>
            <ta e="T626" id="Seg_5212" s="T616">Ол иһин койут биһиги түүн каһуон сиринэн мостылары оӈортуур этибит.</ta>
            <ta e="T634" id="Seg_5213" s="T626">Абааһы немис түүн даа оӈуорбут мостыларбытын туруорбат этэ.</ta>
            <ta e="T640" id="Seg_5214" s="T634">Оччого биһиги диэгилэр дэлби кыйӈанан бараан,</ta>
            <ta e="T646" id="Seg_5215" s="T640">бары сэрии саатынан дэлби ытыалаан бараан,</ta>
            <ta e="T654" id="Seg_5216" s="T646">төһө эмиэ уоннуу самолёттары оӈуоргу өттүгэ ыытан бараан,</ta>
            <ta e="T663" id="Seg_5217" s="T654">итини барытын бүрүнэн туран оӈуоргу диэк быһа мөккүһэ таксыбыттара.</ta>
            <ta e="T670" id="Seg_5218" s="T663">Биһиги солдаттарбыт ол мөккүөн айдааныгар мостага баппакка,</ta>
            <ta e="T688" id="Seg_5219" s="T670">аӈар киһи луотканнан, сороктор кастыы эмиэ брэмнуолары бииргэ колбуу бaайан бараан, аӈардар көӈүл карбаан таксыбыттара Днепр диэн эбэни.</ta>
            <ta e="T702" id="Seg_5220" s="T688">Онно айдаан, улакан үөгүү, каһыы, кэрсии, туоктан да атын сэрии (һэрии) саатын (һаатын) тыаһа сүрдээк этэ.</ta>
            <ta e="T715" id="Seg_5221" s="T702">Немэс диэги өттү, ити быһа мөккүйүүнү котон туппакка, мээни (мээнэ) эбэ үрдүнэн ытыалыыр этэ.</ta>
            <ta e="T727" id="Seg_5222" s="T715">Кытылтан көрдөккө эбэ диэк, ууга букатын киһи тугу да туолкулуок буолбатак этэ.</ta>
            <ta e="T735" id="Seg_5223" s="T727">Колобура, үргүбүт улакан кыыл үөрэ карбыырын көрдүк этэ.</ta>
            <ta e="T749" id="Seg_5224" s="T735">Ити эбэ кэһиитигэр төһөлөөк киһи тумнастан сэрии саатыттан өлбүтүн ким даагыны билбэт оччого этэ.</ta>
            <ta e="T759" id="Seg_5225" s="T749">Эбэни таксан бараан биһиэттэрэ эмиэ немэстэри соло (һоло) булларбакка дэлби баппыттара.</ta>
            <ta e="T785" id="Seg_5226" s="T759">Онтон мин төһө эмэтэ сэриигэ (һэриигэ) бараммын, төрдүстээк күнүгэр январь ыйга, тыыһычча тогус сүүс (һүүс) түөрдуон үс дьыллаакка бүтүгэспин гойобууннаппытым бэрт ыараканнык: каӈас диэки буутум оӈуогун дэлби тоһуттарбытым.</ta>
            <ta e="T800" id="Seg_5227" s="T785">Иһини (итини) эмтэтэммин оһуокпар диэри алта ый туркары сыппытым (һыппытым) Ессентуки диэн гуоракка, Кавказ диэн итии һиргэ.</ta>
            <ta e="T807" id="Seg_5228" s="T800">Онтон эмтэтэн бараан, мас таньактаак таксан барааммын, </ta>
            <ta e="T813" id="Seg_5229" s="T807">сэрииттэн (һэрииттэн) адьас босколоммутум, армияттан совсем таксыбытым. </ta>
            <ta e="T827" id="Seg_5230" s="T813">Маӈнай сэриилэспит (һэриилэспит) күммүттэн, биир дьыл аӈарын туркары, сэрии (һэрии) кэрсиитигэр сылдьаммын (һылдьаммын), барыта түөрткэ кат гойобууннаппытым.</ta>
            <ta e="T832" id="Seg_5231" s="T827">Ити туһунан үстэ эмтэтэ һылдьыбытым.</ta>
            <ta e="T854" id="Seg_5232" s="T832">Кылгастык кэпсээтэккэ, итигирдик мин киһи ааймак үйэтүгэр каччага да биллибэтээк ага да ити улакан сэриитигэр (һэриитигэр) бэйэм дьоммун, төрөөбүт, үөскээбит сирбин (һирбин) көмүскүү һылдьыбытым.</ta>
            <ta e="T861" id="Seg_5233" s="T854">Миниэнэ кааным, көлөһүнүм ити сэриигэ (һэриигэ) көӈүл токтубатага.</ta>
            <ta e="T882" id="Seg_5234" s="T861">Биһиэнэ коммунистическай партиябыт киин комитета онтон биһиэнэ государствабытын ологун көннөрүөччү правительствобыт биэрбиттэрэ миниэкэ государство үрдүк сэриитин орденын "Боевого Красного Знамени" диэн.</ta>
            <ta e="T890" id="Seg_5235" s="T882">Онтон өссүө медали "За победу над Германией" диэн.</ta>
            <ta e="T904" id="Seg_5236" s="T890">Аны биһиги Советскай ологугар олорор киһилэр – олоккут бэрткэ туксан турар инни диэк барар күүскэ.</ta>
            <ta e="T912" id="Seg_5237" s="T904">Ол иһин ким даагыны аны сэриигин буолуогун багарбат.</ta>
            <ta e="T921" id="Seg_5238" s="T912">Биһиги сирбит киһитэ уруккуттан даагыны сэриигэ багараччыта һуок этэ.</ta>
            <ta e="T935" id="Seg_5239" s="T921">Биһиги дьоммут аны бэйэтин санаата (һаната) сөбүлүүрүн (һөбүлүрүн) сырдык (һырдык), дьоллоок Ленин ыйбыт коммунист ааттаак ологун оӈостор. </ta>
            <ta e="T939" id="Seg_5240" s="T935">Биһиги барыбыт диибит аны: </ta>
            <ta e="T944" id="Seg_5241" s="T939">"Сэрии суок буоллун сир үрдүнэн.</ta>
            <ta e="T951" id="Seg_5242" s="T944">Баар буоллун сырдык, күннээк олок бары йоӈӈо".</ta>
         </annotation>
         <annotation name="ts" tierref="ts">
            <ta e="T11" id="Seg_5243" s="T0">Min seriːge barbɨtɨm bejem sanaːbɨnan tɨːhɨčča togus süːs tü͡örd-u͡on biːr dʼɨllaːkka. </ta>
            <ta e="T16" id="Seg_5244" s="T11">Kühüŋŋü sʼentʼabrʼ, tɨmnɨː tüher ɨjɨgar. </ta>
            <ta e="T31" id="Seg_5245" s="T16">Maŋnaj barammɨn, biːr dʼɨlɨ gɨtta togus ɨjɨ seriː ü͡öregiger ü͡örene sɨldʼɨbɨtɨm Dalʼnʼij Vastok di͡en hirge. </ta>
            <ta e="T48" id="Seg_5246" s="T31">Onton sajɨn tɨːhɨčča togus süːs tü͡örd-u͡on üs dʼɨllaːkka bihigini ɨːppɨttara zaːpad di͡ek, seriː kersiːte bara turar siriger. </ta>
            <ta e="T57" id="Seg_5247" s="T48">Bihigi seriːni körsübüppüt Proxorovka di͡en aːttaːk dʼerʼevnʼaga Kurskaj oblahɨgar. </ta>
            <ta e="T81" id="Seg_5248" s="T57">Onno nʼemʼes seriːtin kihite bert ügüs seriː saːtɨn eŋin atɨn teriltetin munnʼan baraːn, bihigi armʼijabɨtɨn berke töttörü di͡ek otut bi͡es biːrsteleːk sirge ɨgajbɨt ete. </ta>
            <ta e="T89" id="Seg_5249" s="T81">Onton kojut iti siri Kurskaj duga di͡en aːttaːbɨttara. </ta>
            <ta e="T96" id="Seg_5250" s="T89">Sakalɨː saŋardakka Kurskaj tokura di͡ekke höp bu͡olu͡oga. </ta>
            <ta e="T102" id="Seg_5251" s="T96">Nʼemʼes armʼijatɨn tojonun sanaːta baːr ebit. </ta>
            <ta e="T117" id="Seg_5252" s="T102">Iti sirinen bɨha mökküjen baraːn, bert türgennik bihigi kiːn sirbitin Maskva di͡en gu͡oratɨ ɨlar kördük. </ta>
            <ta e="T135" id="Seg_5253" s="T117">Bihi͡ene čaːspɨt Kurskaj tokurugar tu͡oktan da ulakan seriː u͡otugar kiːren baraːn, u͡on kün turkarɨ nʼemʼes armʼijatɨn bata hataːbɨta. </ta>
            <ta e="T142" id="Seg_5254" s="T135">Iti turkarɨ bihigi seriːlespippit künü tüːnü bilbekke. </ta>
            <ta e="T146" id="Seg_5255" s="T142">Utujbakka da sɨnnʼammakka da. </ta>
            <ta e="T152" id="Seg_5256" s="T146">Kolobura tugu da bilbet istibet kördük. </ta>
            <ta e="T184" id="Seg_5257" s="T152">Ikki di͡egi seriːleheːčči öttütten töhölü͡ök tɨːhɨčča kihi süppütün, ölbütün, töhölöːk kihi gojobuːn bu͡olbutun, töhölöːk seriː saːta, samalʼottar, tankalar, arudʼijalar onton kɨra eŋin saːlar, seriː teriltete aldʼammɨtɨn kim daːgɨnɨ čakčɨ bilbet ete oččogo. </ta>
            <ta e="T196" id="Seg_5258" s="T184">Kajtak ulakan küːsteːk seriː barbɨtɨn iti hirge, kihi tu͡olkulu͡oga biːr munnuk koloburtan. </ta>
            <ta e="T214" id="Seg_5259" s="T196">Biːr (buːstaːk) sirkeːŋŋe bihigi baːr sirbitiger, küŋŋe biːrde kötön keleːčči ete nʼemʼes di͡egi öttütten ikki hüːs kuraŋa samalʼottar. </ta>
            <ta e="T226" id="Seg_5260" s="T214">Ontuŋ barɨta bihigi ürdübütünen samɨːr kördük tühen e hüːs bombalarɨ keːheːčči ete. </ta>
            <ta e="T239" id="Seg_5261" s="T226">Onton tuspa össü͡ö ɨtɨ͡alɨːr ete töhö emete sarpa kördük iːtiːleːk seriː saːtɨnan– pulʼemʼotɨnan. </ta>
            <ta e="T257" id="Seg_5262" s="T239">Itiččeleːk bomba tüspütün kenne itiččeleːk saː ɨtɨ͡alaːbɨtɨn kenne sir barɨta tu͡okkaːn da köstübet kördük, kara tuman bu͡olar ete. </ta>
            <ta e="T266" id="Seg_5263" s="T257">Iti tumaŋŋa kihi attɨgar sɨtar kihitin koton körböt ete. </ta>
            <ta e="T279" id="Seg_5264" s="T266">Sorok kenne kihi seriː saːtɨn tɨ͡ahɨttan tu͡okkaːnɨ da istibet kördük dülej bu͡olar ete. </ta>
            <ta e="T288" id="Seg_5265" s="T279">Onus kümmütüger bihigi di͡egi öttünnen seriːni olus küːsteːktik barda. </ta>
            <ta e="T303" id="Seg_5266" s="T288">Kallaːntan, sirten bert elbek samalʼottar, tankalar, anaːn seriːge oŋohullubut u͡ottaːk saːlar, "Katʼuša", "Andrʼuša" di͡en aːttaːktar. </ta>
            <ta e="T309" id="Seg_5267" s="T303">Onton tuspa töhö eme kɨra saːlar. </ta>
            <ta e="T316" id="Seg_5268" s="T309">Ol barɨta biːrge ogustular nʼemʼester baːr hirderin. </ta>
            <ta e="T325" id="Seg_5269" s="T316">Nʼemʼes armʼijata itini dolgunnaːk ulakan oksuːnu tulujbakka töttörü supturujbuta. </ta>
            <ta e="T332" id="Seg_5270" s="T325">Sonnokoːn u͡on bi͡es beristeleːk siri töttörü ɨstammɨta. </ta>
            <ta e="T341" id="Seg_5271" s="T332">Iti ulakan oksuː kennitten Kurskaj di͡en tokura berke kömmüte. </ta>
            <ta e="T346" id="Seg_5272" s="T341">Nʼemʼes armʼijatɨn töttörü bihigi bappɨppɨt. </ta>
            <ta e="T363" id="Seg_5273" s="T346">Iti kennitten nʼemʼester hereges saldaːttara innilerin bihi͡eke bi͡eren baraːn suptu seriː bütü͡ögüger di͡eri öjdörün-puːttarɨn bulbataktara, ku͡otan ispittere. </ta>
            <ta e="T376" id="Seg_5274" s="T363">Bihigi armʼijabɨt bert üčügejdik küːhün tutan baraːn nʼemʼesteri kenni di͡ek, köllörbökkö, delbi bappɨta. </ta>
            <ta e="T387" id="Seg_5275" s="T376">Min kojukku künnerge seriːge hɨldʼammɨn, itinnik sürdeːk kihi ((…)) kördük körbötögüm. </ta>
            <ta e="T400" id="Seg_5276" s="T387">Min iti hirge saŋardɨː körbütüm hu͡ostaːk kersiːni, kajtak seriː kihitin kaːna ürektiː toktorun. </ta>
            <ta e="T405" id="Seg_5277" s="T400">Töhölöːk tɨːhɨčča kihi tɨːna bɨstarɨn. </ta>
            <ta e="T411" id="Seg_5278" s="T405">töhölöːk gojobuːn bu͡olbut kihi ulakannɨk erejdenerin. </ta>
            <ta e="T426" id="Seg_5279" s="T411">Min emi͡e iti oksohuːga Kurskaj tumuhugar, atdʼeleʼnʼija kamandʼira bu͡ola hɨldʼammɨn dʼogustuk kaŋas atakpɨn bɨlčɨŋɨ gojobuːnnappɨtɨm. </ta>
            <ta e="T432" id="Seg_5280" s="T426">Ol bu͡olan baraːn seriːleher sirten taksɨbatagɨm. </ta>
            <ta e="T452" id="Seg_5281" s="T432">Iti sirdeːk oksohuːga min baːr agaj čaːhɨm tɨːhɨččattan taksa seriː kihititten iti u͡on kuŋŋe oksohuːga orputa u͡on biːr agaj kihi. </ta>
            <ta e="T462" id="Seg_5282" s="T452">Iti ihiger össü͡ö biːr min gojobuːn bu͡olbut kihi baːr etim. </ta>
            <ta e="T478" id="Seg_5283" s="T462">Itinten söp bu͡olu͡oga kolu͡okka, kajtak aldʼarkajdaːk seriː etin Kurskaj togurugor, töhö kaːn toktubutun, töhö kihi ölbütün. </ta>
            <ta e="T490" id="Seg_5284" s="T478">Kurskajga büten baraːn bihigi armʼijabɨt töttörü kürüːr nʼemʼehi zaːpad di͡ek batan barbɨta. </ta>
            <ta e="T510" id="Seg_5285" s="T490">Iti Kurskaj kennitten min saːmaj seriː (kerse) siriger baːr etim tɨːhɨčča togus süːs tü͡örd-u͡on tü͡ört dʼɨllaːkka, kɨhɨŋŋɨ janvarʼ ɨjga di͡eri. </ta>
            <ta e="T538" id="Seg_5286" s="T510">Iti kemŋe nʼemʼes saldaːttarɨn gɨtta utara turan seriːlespitim munnuk sirderge, Bʼelgarad onton Xarʼkav, onton Poltava, onton Krʼemʼenčʼug, onton Krʼukov di͡en aːttaːk ulakan gu͡orattarɨ ɨlarga, ukrainʼes di͡en omuk siriger. </ta>
            <ta e="T549" id="Seg_5287" s="T538">Iti gu͡orattartan olus kɨtaːnaktɨk bihigi seriːlespippit nʼemʼehi gɨtta Xarʼkav gu͡orat bɨldʼahɨːga. </ta>
            <ta e="T558" id="Seg_5288" s="T549">Iti gu͡orat üske di͡eri töttörü taːrɨ iliːtten iliːge kele hɨldʼɨbɨta. </ta>
            <ta e="T564" id="Seg_5289" s="T558">Onton ühüs keliːtiger bihigi iliːbitten ɨːppatakpɨt. </ta>
            <ta e="T575" id="Seg_5290" s="T564">Onton ulakan oksohuː össü͡ö baːr ete Dnʼepr di͡en aːttaːk ebe kehiːtiger. </ta>
            <ta e="T589" id="Seg_5291" s="T575">Iti ebe oŋu͡orgu öttütüger oloron nʼemʼester künüsteri-tüːnneri ketiː hɨldʼannar, bihigini amattan sataːn keherpet etilere. </ta>
            <ta e="T600" id="Seg_5292" s="T589">Ol ihin bihigi kas da künü koton taksɨbatakpɨt, betereːgi kɨtɨlga olorbupput. </ta>
            <ta e="T616" id="Seg_5293" s="T600">Ebeni taksar mostɨlarɨ künüs oŋororbutun, nʼemʼester samalʼottara kötön kele-kele töhö eme bombalarɨ bɨragattaːn, delbi aldʼatar ete. </ta>
            <ta e="T626" id="Seg_5294" s="T616">Ol ihin kojut bihigi tüːn kahu͡on sirinen mostɨlarɨ oŋortuːr etibit. </ta>
            <ta e="T634" id="Seg_5295" s="T626">Abaːhɨ nʼemʼis tüːn daː oŋorbut mostɨlarbɨtɨn turu͡orbat ete. </ta>
            <ta e="T640" id="Seg_5296" s="T634">Oččogo bihigi di͡egiler delbi kɨjŋanan baraːn. </ta>
            <ta e="T646" id="Seg_5297" s="T640">Barɨ seriː saːtɨnan delbi ɨtɨ͡alaːn baraːn. </ta>
            <ta e="T654" id="Seg_5298" s="T646">Töhö eme u͡onnuː samalʼottarɨ oŋu͡orgu öttüge ɨːtan baraːn. </ta>
            <ta e="T663" id="Seg_5299" s="T654">Itini barɨtɨn bürünen turan oŋu͡orgu di͡ek bɨha mökkühe taksɨbɨttara. </ta>
            <ta e="T670" id="Seg_5300" s="T663">Bihigi saldaːttarbɨt ol mökkü͡ön ajdaːnɨgar mostaga bappakka. </ta>
            <ta e="T688" id="Seg_5301" s="T670">Aŋar kihi lu͡otkannan, soroktor kastɨː emi͡e bremnu͡olarɨ biːrge kolbuː baːjan baraːn, aŋardar köŋül karbaːn taksɨbɨttara Dnʼepr di͡en ebeni. </ta>
            <ta e="T702" id="Seg_5302" s="T688">Onno ajdaːn, ulakan ü͡ögüː, kahɨː, kersiː, tu͡oktan da atɨn seriː saːtɨn tɨ͡aha sürdeːk ete. </ta>
            <ta e="T715" id="Seg_5303" s="T702">Nʼemʼes di͡egi öttü, iti bɨha mökküjüːnü koton tuppakka, meːne ebe ürdünen ɨtɨ͡alɨːr ete. </ta>
            <ta e="T727" id="Seg_5304" s="T715">Kɨtɨltan kördökkö ebe di͡ek, uːga bukatɨn kihi tugu da tu͡olkulu͡ok bu͡olbatak ete. </ta>
            <ta e="T735" id="Seg_5305" s="T727">Kolobura, ürgübüt ulakan kɨːl ü͡öre karbɨːrɨn kördük ete. </ta>
            <ta e="T749" id="Seg_5306" s="T735">Iti ebe kehiːtiger töhölöːk kihi tumnastan seriː saːtɨttan ölbütün kim daːgɨnɨ bilbet oččogo ete. </ta>
            <ta e="T759" id="Seg_5307" s="T749">Ebeni taksan baraːn bihi͡ettere emi͡e nʼemʼesteri solo bullarbakka delbi bappɨttara. </ta>
            <ta e="T785" id="Seg_5308" s="T759">Onton min töhö emete seriːge barammɨn, tördüsteːk künüger janvarʼ ɨjga, tɨːhɨčča togus süːs tü͡örd-u͡on üs dʼɨllaːkka bütügespin gojobuːnnappɨtɨm bert ɨ͡arakannɨk, kaŋas di͡eki buːtum oŋu͡ogun delbi tohuttarbɨtɨm. </ta>
            <ta e="T800" id="Seg_5309" s="T785">Ihini emtetemmin ohu͡okpar di͡eri alta ɨj turkarɨ sɨppɨtɨm Essentuki di͡en gu͡orakka, Kavkaːz di͡en itiː hirge. </ta>
            <ta e="T807" id="Seg_5310" s="T800">Onton emteten baraːn, mas tanʼaktaːk taksan baraːmmɨn. </ta>
            <ta e="T813" id="Seg_5311" s="T807">Seriːtten adʼas boskolommutum, armʼijattan sаvsʼem taksɨbɨtɨm. </ta>
            <ta e="T827" id="Seg_5312" s="T813">Maŋnaj seriːlespit kümmütten, biːr dʼɨl aŋarɨn turkarɨ, seriː kersiːtiger sɨldʼammɨn, barɨta tü͡örka kat gojobuːnnappɨtɨm. </ta>
            <ta e="T832" id="Seg_5313" s="T827">Iti tuhunan üste emtete hɨldʼɨbɨtɨm. </ta>
            <ta e="T854" id="Seg_5314" s="T832">Kɨlgastɨk kepseːtekke, itigirdik min kihi aːjmak üjetiger kaččaga da billibetek aga da iti ulakan seriːtiger bejem dʼommun, töröːbüt, ü͡öskeːbit sirbin kömüsküː hɨldʼɨbɨtɨm. </ta>
            <ta e="T861" id="Seg_5315" s="T854">Mini͡ene kaːnɨm, kölöhünüm iti seriːge köŋül toktubataga. </ta>
            <ta e="T882" id="Seg_5316" s="T861">Bihi͡ene kammunʼistʼičʼeskaj partʼijabɨt kiːn kаmitʼeta onton bihi͡ene gasudarstvabɨtɨn ologun könnöröːččü pravʼitʼelʼstvabɨt bi͡erbittere mini͡eke gasudarstva ürdük seriːtin ordʼenɨn "Bajevova Kraːsnava Znamʼenʼi" di͡en. </ta>
            <ta e="T890" id="Seg_5317" s="T882">Onton össü͡ö mʼedalʼi "Za pabʼedu nad Gʼermanʼijej" di͡en. </ta>
            <ta e="T904" id="Seg_5318" s="T890">Anɨ bihigi Savʼetskaj ologur oloror kihiler – olokkut berke tuksan turar inni di͡ek barar küːske. </ta>
            <ta e="T912" id="Seg_5319" s="T904">Ol ihin kim daːganɨ anɨ seriːgin bu͡olu͡ogun bagarbat. </ta>
            <ta e="T921" id="Seg_5320" s="T912">Bihigi sirbit kihite urukkuttan daːganɨ seriːge bagaraːččɨta hu͡ok ete. </ta>
            <ta e="T935" id="Seg_5321" s="T921">Bihigi dʼommut anɨ bejetin sanaːta söbülüːrün sɨrdɨk, dʼolloːk Lʼenʼin ɨjbɨt kommunʼist aːttaːk ologun oŋostor. </ta>
            <ta e="T939" id="Seg_5322" s="T935">Bihigi barɨbɨt diːbit anɨ: </ta>
            <ta e="T944" id="Seg_5323" s="T939">"Seriː su͡ok bu͡ollun sir ürdünen. </ta>
            <ta e="T951" id="Seg_5324" s="T944">Baːr bu͡ollun sɨrdɨk, künneːk olok barɨ joŋŋo". </ta>
         </annotation>
         <annotation name="mb" tierref="mb">
            <ta e="T1" id="Seg_5325" s="T0">min</ta>
            <ta e="T2" id="Seg_5326" s="T1">seriː-ge</ta>
            <ta e="T3" id="Seg_5327" s="T2">bar-bɨt-ɨ-m</ta>
            <ta e="T4" id="Seg_5328" s="T3">beje-m</ta>
            <ta e="T5" id="Seg_5329" s="T4">sanaː-bɨ-nan</ta>
            <ta e="T6" id="Seg_5330" s="T5">tɨːhɨčča</ta>
            <ta e="T7" id="Seg_5331" s="T6">togus</ta>
            <ta e="T8" id="Seg_5332" s="T7">süːs</ta>
            <ta e="T9" id="Seg_5333" s="T8">tü͡örd-u͡on</ta>
            <ta e="T10" id="Seg_5334" s="T9">biːr</ta>
            <ta e="T11" id="Seg_5335" s="T10">dʼɨl-laːk-ka</ta>
            <ta e="T12" id="Seg_5336" s="T11">kühüŋŋü</ta>
            <ta e="T13" id="Seg_5337" s="T12">sʼentʼabrʼ</ta>
            <ta e="T14" id="Seg_5338" s="T13">tɨmnɨː</ta>
            <ta e="T15" id="Seg_5339" s="T14">tüh-er</ta>
            <ta e="T16" id="Seg_5340" s="T15">ɨj-ɨ-gar</ta>
            <ta e="T17" id="Seg_5341" s="T16">maŋnaj</ta>
            <ta e="T18" id="Seg_5342" s="T17">bar-am-mɨn</ta>
            <ta e="T19" id="Seg_5343" s="T18">biːr</ta>
            <ta e="T20" id="Seg_5344" s="T19">dʼɨl-ɨ</ta>
            <ta e="T21" id="Seg_5345" s="T20">gɨtta</ta>
            <ta e="T22" id="Seg_5346" s="T21">togus</ta>
            <ta e="T23" id="Seg_5347" s="T22">ɨj-ɨ</ta>
            <ta e="T24" id="Seg_5348" s="T23">seriː</ta>
            <ta e="T25" id="Seg_5349" s="T24">ü͡öreg-i-ger</ta>
            <ta e="T26" id="Seg_5350" s="T25">ü͡ören-e</ta>
            <ta e="T27" id="Seg_5351" s="T26">sɨldʼ-ɨ-bɨt-ɨ-m</ta>
            <ta e="T29" id="Seg_5352" s="T27">Dalʼnʼij Vastok</ta>
            <ta e="T30" id="Seg_5353" s="T29">di͡e-n</ta>
            <ta e="T31" id="Seg_5354" s="T30">hir-ge</ta>
            <ta e="T32" id="Seg_5355" s="T31">onton</ta>
            <ta e="T33" id="Seg_5356" s="T32">sajɨn</ta>
            <ta e="T34" id="Seg_5357" s="T33">tɨːhɨčča</ta>
            <ta e="T35" id="Seg_5358" s="T34">togus</ta>
            <ta e="T36" id="Seg_5359" s="T35">süːs</ta>
            <ta e="T37" id="Seg_5360" s="T36">tü͡örd-u͡on</ta>
            <ta e="T38" id="Seg_5361" s="T37">üs</ta>
            <ta e="T39" id="Seg_5362" s="T38">dʼɨl-laːk-ka</ta>
            <ta e="T40" id="Seg_5363" s="T39">bihigi-ni</ta>
            <ta e="T41" id="Seg_5364" s="T40">ɨːp-pɨt-tara</ta>
            <ta e="T42" id="Seg_5365" s="T41">zaːpad</ta>
            <ta e="T43" id="Seg_5366" s="T42">di͡ek</ta>
            <ta e="T44" id="Seg_5367" s="T43">seriː</ta>
            <ta e="T45" id="Seg_5368" s="T44">kersiː-te</ta>
            <ta e="T46" id="Seg_5369" s="T45">bar-a</ta>
            <ta e="T47" id="Seg_5370" s="T46">tur-ar</ta>
            <ta e="T48" id="Seg_5371" s="T47">sir-i-ger</ta>
            <ta e="T49" id="Seg_5372" s="T48">bihigi</ta>
            <ta e="T50" id="Seg_5373" s="T49">seriː-ni</ta>
            <ta e="T51" id="Seg_5374" s="T50">körs-ü-büp-püt</ta>
            <ta e="T52" id="Seg_5375" s="T51">Proxorovka</ta>
            <ta e="T53" id="Seg_5376" s="T52">di͡e-n</ta>
            <ta e="T54" id="Seg_5377" s="T53">aːt-taːk</ta>
            <ta e="T55" id="Seg_5378" s="T54">dʼerʼevnʼa-ga</ta>
            <ta e="T56" id="Seg_5379" s="T55">Kurskaj</ta>
            <ta e="T57" id="Seg_5380" s="T56">oblah-ɨ-gar</ta>
            <ta e="T58" id="Seg_5381" s="T57">onno</ta>
            <ta e="T59" id="Seg_5382" s="T58">nʼemʼes</ta>
            <ta e="T60" id="Seg_5383" s="T59">seriː-ti-n</ta>
            <ta e="T61" id="Seg_5384" s="T60">kihi-te</ta>
            <ta e="T62" id="Seg_5385" s="T61">bert</ta>
            <ta e="T63" id="Seg_5386" s="T62">ügüs</ta>
            <ta e="T64" id="Seg_5387" s="T63">seriː</ta>
            <ta e="T65" id="Seg_5388" s="T64">saː-tɨ-n</ta>
            <ta e="T66" id="Seg_5389" s="T65">eŋin</ta>
            <ta e="T67" id="Seg_5390" s="T66">atɨn</ta>
            <ta e="T68" id="Seg_5391" s="T67">terilte-ti-n</ta>
            <ta e="T69" id="Seg_5392" s="T68">munnʼ-an</ta>
            <ta e="T70" id="Seg_5393" s="T69">baraːn</ta>
            <ta e="T71" id="Seg_5394" s="T70">bihigi</ta>
            <ta e="T72" id="Seg_5395" s="T71">armʼija-bɨtɨ-n</ta>
            <ta e="T73" id="Seg_5396" s="T72">berke</ta>
            <ta e="T74" id="Seg_5397" s="T73">töttörü</ta>
            <ta e="T75" id="Seg_5398" s="T74">di͡ek</ta>
            <ta e="T76" id="Seg_5399" s="T75">otut</ta>
            <ta e="T77" id="Seg_5400" s="T76">bi͡es</ta>
            <ta e="T78" id="Seg_5401" s="T77">biːrste-leːk</ta>
            <ta e="T79" id="Seg_5402" s="T78">sir-ge</ta>
            <ta e="T80" id="Seg_5403" s="T79">ɨgaj-bɨt</ta>
            <ta e="T81" id="Seg_5404" s="T80">e-t-e</ta>
            <ta e="T82" id="Seg_5405" s="T81">onton</ta>
            <ta e="T83" id="Seg_5406" s="T82">kojut</ta>
            <ta e="T84" id="Seg_5407" s="T83">iti</ta>
            <ta e="T85" id="Seg_5408" s="T84">sir-i</ta>
            <ta e="T86" id="Seg_5409" s="T85">Kurskaj</ta>
            <ta e="T87" id="Seg_5410" s="T86">duga</ta>
            <ta e="T88" id="Seg_5411" s="T87">di͡e-n</ta>
            <ta e="T89" id="Seg_5412" s="T88">aːt-taː-bɨt-tara</ta>
            <ta e="T90" id="Seg_5413" s="T89">saka-lɨː</ta>
            <ta e="T91" id="Seg_5414" s="T90">saŋar-dak-ka</ta>
            <ta e="T92" id="Seg_5415" s="T91">Kurskaj</ta>
            <ta e="T93" id="Seg_5416" s="T92">tokur-a</ta>
            <ta e="T94" id="Seg_5417" s="T93">d-i͡ek-ke</ta>
            <ta e="T95" id="Seg_5418" s="T94">höp</ta>
            <ta e="T96" id="Seg_5419" s="T95">bu͡ol-u͡og-a</ta>
            <ta e="T97" id="Seg_5420" s="T96">nʼemʼes</ta>
            <ta e="T98" id="Seg_5421" s="T97">armʼija-tɨ-n</ta>
            <ta e="T99" id="Seg_5422" s="T98">tojon-u-n</ta>
            <ta e="T100" id="Seg_5423" s="T99">sanaː-ta</ta>
            <ta e="T101" id="Seg_5424" s="T100">baːr</ta>
            <ta e="T102" id="Seg_5425" s="T101">e-bit</ta>
            <ta e="T103" id="Seg_5426" s="T102">iti</ta>
            <ta e="T104" id="Seg_5427" s="T103">sir-i-nen</ta>
            <ta e="T105" id="Seg_5428" s="T104">bɨha</ta>
            <ta e="T106" id="Seg_5429" s="T105">mökküj-en</ta>
            <ta e="T107" id="Seg_5430" s="T106">baraːn</ta>
            <ta e="T108" id="Seg_5431" s="T107">bert</ta>
            <ta e="T109" id="Seg_5432" s="T108">türgen-nik</ta>
            <ta e="T110" id="Seg_5433" s="T109">bihigi</ta>
            <ta e="T111" id="Seg_5434" s="T110">kiːn</ta>
            <ta e="T112" id="Seg_5435" s="T111">sir-biti-n</ta>
            <ta e="T113" id="Seg_5436" s="T112">Maskva</ta>
            <ta e="T114" id="Seg_5437" s="T113">di͡e-n</ta>
            <ta e="T115" id="Seg_5438" s="T114">gu͡orat-ɨ</ta>
            <ta e="T116" id="Seg_5439" s="T115">ɨl-ar</ta>
            <ta e="T117" id="Seg_5440" s="T116">kördük</ta>
            <ta e="T118" id="Seg_5441" s="T117">bihi͡ene</ta>
            <ta e="T119" id="Seg_5442" s="T118">čaːs-pɨt</ta>
            <ta e="T120" id="Seg_5443" s="T119">Kurskaj</ta>
            <ta e="T121" id="Seg_5444" s="T120">tokur-u-gar</ta>
            <ta e="T122" id="Seg_5445" s="T121">tu͡ok-tan</ta>
            <ta e="T123" id="Seg_5446" s="T122">da</ta>
            <ta e="T124" id="Seg_5447" s="T123">ulakan</ta>
            <ta e="T125" id="Seg_5448" s="T124">seriː</ta>
            <ta e="T126" id="Seg_5449" s="T125">u͡ot-u-gar</ta>
            <ta e="T127" id="Seg_5450" s="T126">kiːr-en</ta>
            <ta e="T128" id="Seg_5451" s="T127">baraːn</ta>
            <ta e="T129" id="Seg_5452" s="T128">u͡on</ta>
            <ta e="T130" id="Seg_5453" s="T129">kün</ta>
            <ta e="T131" id="Seg_5454" s="T130">turkarɨ</ta>
            <ta e="T132" id="Seg_5455" s="T131">nʼemʼes</ta>
            <ta e="T133" id="Seg_5456" s="T132">armʼija-tɨ-n</ta>
            <ta e="T134" id="Seg_5457" s="T133">bat-a</ta>
            <ta e="T135" id="Seg_5458" s="T134">hataː-bɨt-a</ta>
            <ta e="T136" id="Seg_5459" s="T135">iti</ta>
            <ta e="T137" id="Seg_5460" s="T136">turkarɨ</ta>
            <ta e="T138" id="Seg_5461" s="T137">bihigi</ta>
            <ta e="T139" id="Seg_5462" s="T138">seriːles-pip-pit</ta>
            <ta e="T140" id="Seg_5463" s="T139">kün-ü</ta>
            <ta e="T141" id="Seg_5464" s="T140">tüːn-ü</ta>
            <ta e="T142" id="Seg_5465" s="T141">bil-bekke</ta>
            <ta e="T143" id="Seg_5466" s="T142">utuj-bakka</ta>
            <ta e="T144" id="Seg_5467" s="T143">da</ta>
            <ta e="T145" id="Seg_5468" s="T144">sɨnnʼam-makka</ta>
            <ta e="T146" id="Seg_5469" s="T145">da</ta>
            <ta e="T147" id="Seg_5470" s="T146">kolobur-a</ta>
            <ta e="T148" id="Seg_5471" s="T147">tug-u</ta>
            <ta e="T149" id="Seg_5472" s="T148">da</ta>
            <ta e="T150" id="Seg_5473" s="T149">bil-bet</ta>
            <ta e="T151" id="Seg_5474" s="T150">ist-i-bet</ta>
            <ta e="T152" id="Seg_5475" s="T151">kördük</ta>
            <ta e="T153" id="Seg_5476" s="T152">ikki</ta>
            <ta e="T154" id="Seg_5477" s="T153">di͡egi</ta>
            <ta e="T155" id="Seg_5478" s="T154">seriːleh-eːčči</ta>
            <ta e="T156" id="Seg_5479" s="T155">öttü-tten</ta>
            <ta e="T157" id="Seg_5480" s="T156">töhölü͡ök</ta>
            <ta e="T158" id="Seg_5481" s="T157">tɨːhɨčča</ta>
            <ta e="T159" id="Seg_5482" s="T158">kihi</ta>
            <ta e="T160" id="Seg_5483" s="T159">süp-püt-ü-n</ta>
            <ta e="T161" id="Seg_5484" s="T160">öl-büt-ü-n</ta>
            <ta e="T162" id="Seg_5485" s="T161">töhölöːk</ta>
            <ta e="T163" id="Seg_5486" s="T162">kihi</ta>
            <ta e="T164" id="Seg_5487" s="T163">gojobuːn</ta>
            <ta e="T165" id="Seg_5488" s="T164">bu͡ol-but-u-n</ta>
            <ta e="T166" id="Seg_5489" s="T165">töhölöːk</ta>
            <ta e="T167" id="Seg_5490" s="T166">seriː</ta>
            <ta e="T168" id="Seg_5491" s="T167">saː-ta</ta>
            <ta e="T169" id="Seg_5492" s="T168">samalʼot-tar</ta>
            <ta e="T170" id="Seg_5493" s="T169">tanka-lar</ta>
            <ta e="T171" id="Seg_5494" s="T170">arudʼija-lar</ta>
            <ta e="T172" id="Seg_5495" s="T171">onton</ta>
            <ta e="T173" id="Seg_5496" s="T172">kɨra</ta>
            <ta e="T174" id="Seg_5497" s="T173">eŋin</ta>
            <ta e="T175" id="Seg_5498" s="T174">saː-lar</ta>
            <ta e="T176" id="Seg_5499" s="T175">seriː</ta>
            <ta e="T177" id="Seg_5500" s="T176">terilte-te</ta>
            <ta e="T178" id="Seg_5501" s="T177">aldʼam-mɨt-ɨ-n</ta>
            <ta e="T179" id="Seg_5502" s="T178">kim</ta>
            <ta e="T180" id="Seg_5503" s="T179">daːgɨnɨ</ta>
            <ta e="T181" id="Seg_5504" s="T180">čakčɨ</ta>
            <ta e="T182" id="Seg_5505" s="T181">bil-bet</ta>
            <ta e="T183" id="Seg_5506" s="T182">e-t-e</ta>
            <ta e="T184" id="Seg_5507" s="T183">oččogo</ta>
            <ta e="T185" id="Seg_5508" s="T184">kajtak</ta>
            <ta e="T186" id="Seg_5509" s="T185">ulakan</ta>
            <ta e="T187" id="Seg_5510" s="T186">küːsteːk</ta>
            <ta e="T188" id="Seg_5511" s="T187">seriː</ta>
            <ta e="T189" id="Seg_5512" s="T188">bar-bɨt-ɨ-n</ta>
            <ta e="T190" id="Seg_5513" s="T189">iti</ta>
            <ta e="T191" id="Seg_5514" s="T190">hir-ge</ta>
            <ta e="T192" id="Seg_5515" s="T191">kihi</ta>
            <ta e="T193" id="Seg_5516" s="T192">tu͡olkul-u͡og-a</ta>
            <ta e="T194" id="Seg_5517" s="T193">biːr</ta>
            <ta e="T195" id="Seg_5518" s="T194">munnuk</ta>
            <ta e="T196" id="Seg_5519" s="T195">kolobur-tan</ta>
            <ta e="T197" id="Seg_5520" s="T196">biːr</ta>
            <ta e="T198" id="Seg_5521" s="T197">buːs-taːk</ta>
            <ta e="T199" id="Seg_5522" s="T198">sir-keːŋ-ŋe</ta>
            <ta e="T200" id="Seg_5523" s="T199">bihigi</ta>
            <ta e="T201" id="Seg_5524" s="T200">baːr</ta>
            <ta e="T202" id="Seg_5525" s="T201">sir-biti-ger</ta>
            <ta e="T203" id="Seg_5526" s="T202">küŋ-ŋe</ta>
            <ta e="T204" id="Seg_5527" s="T203">biːrde</ta>
            <ta e="T205" id="Seg_5528" s="T204">köt-ön</ta>
            <ta e="T206" id="Seg_5529" s="T205">kel-eːčči</ta>
            <ta e="T207" id="Seg_5530" s="T206">e-t-e</ta>
            <ta e="T208" id="Seg_5531" s="T207">nʼemʼes</ta>
            <ta e="T209" id="Seg_5532" s="T208">di͡egi</ta>
            <ta e="T210" id="Seg_5533" s="T209">öttü-tten</ta>
            <ta e="T211" id="Seg_5534" s="T210">ikki</ta>
            <ta e="T212" id="Seg_5535" s="T211">hüːs</ta>
            <ta e="T213" id="Seg_5536" s="T212">kuraŋ-a</ta>
            <ta e="T214" id="Seg_5537" s="T213">samalʼot-tar</ta>
            <ta e="T215" id="Seg_5538" s="T214">on-tu-ŋ</ta>
            <ta e="T216" id="Seg_5539" s="T215">barɨ-ta</ta>
            <ta e="T217" id="Seg_5540" s="T216">bihigi</ta>
            <ta e="T218" id="Seg_5541" s="T217">ürdü-bütü-nen</ta>
            <ta e="T219" id="Seg_5542" s="T218">samɨːr</ta>
            <ta e="T220" id="Seg_5543" s="T219">kördük</ta>
            <ta e="T221" id="Seg_5544" s="T220">tüh-en</ta>
            <ta e="T222" id="Seg_5545" s="T221">e</ta>
            <ta e="T223" id="Seg_5546" s="T222">hüːs</ta>
            <ta e="T224" id="Seg_5547" s="T223">bomba-lar-ɨ</ta>
            <ta e="T225" id="Seg_5548" s="T224">keːh-eːčči</ta>
            <ta e="T226" id="Seg_5549" s="T225">e-t-e</ta>
            <ta e="T227" id="Seg_5550" s="T226">onton</ta>
            <ta e="T228" id="Seg_5551" s="T227">tuspa</ta>
            <ta e="T229" id="Seg_5552" s="T228">össü͡ö</ta>
            <ta e="T230" id="Seg_5553" s="T229">ɨt-ɨ͡alɨː-r</ta>
            <ta e="T231" id="Seg_5554" s="T230">e-t-e</ta>
            <ta e="T232" id="Seg_5555" s="T231">töhö</ta>
            <ta e="T233" id="Seg_5556" s="T232">emete</ta>
            <ta e="T234" id="Seg_5557" s="T233">sarpa</ta>
            <ta e="T235" id="Seg_5558" s="T234">kördük</ta>
            <ta e="T236" id="Seg_5559" s="T235">iːt-iː-leːk</ta>
            <ta e="T237" id="Seg_5560" s="T236">seriː</ta>
            <ta e="T238" id="Seg_5561" s="T237">saː-tɨ-nan</ta>
            <ta e="T239" id="Seg_5562" s="T238">pulʼemʼot-ɨ-nan</ta>
            <ta e="T240" id="Seg_5563" s="T239">itičče-leːk</ta>
            <ta e="T241" id="Seg_5564" s="T240">bomba</ta>
            <ta e="T242" id="Seg_5565" s="T241">tüs-püt-ü-n</ta>
            <ta e="T243" id="Seg_5566" s="T242">kenne</ta>
            <ta e="T244" id="Seg_5567" s="T243">itičče-leːk</ta>
            <ta e="T245" id="Seg_5568" s="T244">saː</ta>
            <ta e="T246" id="Seg_5569" s="T245">ɨt-ɨ͡alaː-bɨt-ɨ-n</ta>
            <ta e="T247" id="Seg_5570" s="T246">kenne</ta>
            <ta e="T248" id="Seg_5571" s="T247">sir</ta>
            <ta e="T249" id="Seg_5572" s="T248">barɨta</ta>
            <ta e="T250" id="Seg_5573" s="T249">tu͡ok-kaːn</ta>
            <ta e="T251" id="Seg_5574" s="T250">da</ta>
            <ta e="T252" id="Seg_5575" s="T251">köst-ü-bet</ta>
            <ta e="T253" id="Seg_5576" s="T252">kördük</ta>
            <ta e="T254" id="Seg_5577" s="T253">kara</ta>
            <ta e="T255" id="Seg_5578" s="T254">tuman</ta>
            <ta e="T256" id="Seg_5579" s="T255">bu͡ol-ar</ta>
            <ta e="T257" id="Seg_5580" s="T256">e-t-e</ta>
            <ta e="T258" id="Seg_5581" s="T257">iti</ta>
            <ta e="T259" id="Seg_5582" s="T258">tumaŋ-ŋa</ta>
            <ta e="T260" id="Seg_5583" s="T259">kihi</ta>
            <ta e="T261" id="Seg_5584" s="T260">attɨ-gar</ta>
            <ta e="T262" id="Seg_5585" s="T261">sɨt-ar</ta>
            <ta e="T263" id="Seg_5586" s="T262">kihi-ti-n</ta>
            <ta e="T264" id="Seg_5587" s="T263">kot-on</ta>
            <ta e="T265" id="Seg_5588" s="T264">kör-böt</ta>
            <ta e="T266" id="Seg_5589" s="T265">e-t-e</ta>
            <ta e="T267" id="Seg_5590" s="T266">sorok</ta>
            <ta e="T268" id="Seg_5591" s="T267">kenne</ta>
            <ta e="T269" id="Seg_5592" s="T268">kihi</ta>
            <ta e="T270" id="Seg_5593" s="T269">seriː</ta>
            <ta e="T271" id="Seg_5594" s="T270">saː-tɨ-n</ta>
            <ta e="T272" id="Seg_5595" s="T271">tɨ͡ah-ɨ-ttan</ta>
            <ta e="T273" id="Seg_5596" s="T272">tu͡ok-kaːn-ɨ</ta>
            <ta e="T274" id="Seg_5597" s="T273">da</ta>
            <ta e="T275" id="Seg_5598" s="T274">ist-i-bet</ta>
            <ta e="T276" id="Seg_5599" s="T275">kördük</ta>
            <ta e="T277" id="Seg_5600" s="T276">dülej</ta>
            <ta e="T278" id="Seg_5601" s="T277">bu͡ol-ar</ta>
            <ta e="T279" id="Seg_5602" s="T278">e-t-e</ta>
            <ta e="T280" id="Seg_5603" s="T279">on-us</ta>
            <ta e="T281" id="Seg_5604" s="T280">küm-mütü-ger</ta>
            <ta e="T282" id="Seg_5605" s="T281">bihigi</ta>
            <ta e="T283" id="Seg_5606" s="T282">di͡egi</ta>
            <ta e="T284" id="Seg_5607" s="T283">öttü-nnen</ta>
            <ta e="T285" id="Seg_5608" s="T284">seriː-ni</ta>
            <ta e="T286" id="Seg_5609" s="T285">olus</ta>
            <ta e="T287" id="Seg_5610" s="T286">küːs-teːk-tik</ta>
            <ta e="T288" id="Seg_5611" s="T287">bar-d-a</ta>
            <ta e="T289" id="Seg_5612" s="T288">kallaːn-tan</ta>
            <ta e="T290" id="Seg_5613" s="T289">sir-ten</ta>
            <ta e="T291" id="Seg_5614" s="T290">bert</ta>
            <ta e="T292" id="Seg_5615" s="T291">elbek</ta>
            <ta e="T293" id="Seg_5616" s="T292">samalʼot-tar</ta>
            <ta e="T294" id="Seg_5617" s="T293">tanka-lar</ta>
            <ta e="T295" id="Seg_5618" s="T294">anaːn</ta>
            <ta e="T296" id="Seg_5619" s="T295">seriː-ge</ta>
            <ta e="T297" id="Seg_5620" s="T296">oŋohull-u-but</ta>
            <ta e="T298" id="Seg_5621" s="T297">u͡ot-taːk</ta>
            <ta e="T299" id="Seg_5622" s="T298">saː-lar</ta>
            <ta e="T300" id="Seg_5623" s="T299">Katʼuša</ta>
            <ta e="T301" id="Seg_5624" s="T300">Andrʼuša</ta>
            <ta e="T302" id="Seg_5625" s="T301">di͡e-n</ta>
            <ta e="T303" id="Seg_5626" s="T302">aːt-taːk-tar</ta>
            <ta e="T304" id="Seg_5627" s="T303">onton</ta>
            <ta e="T305" id="Seg_5628" s="T304">tuspa</ta>
            <ta e="T306" id="Seg_5629" s="T305">töhö</ta>
            <ta e="T307" id="Seg_5630" s="T306">eme</ta>
            <ta e="T308" id="Seg_5631" s="T307">kɨra</ta>
            <ta e="T309" id="Seg_5632" s="T308">saː-lar</ta>
            <ta e="T310" id="Seg_5633" s="T309">ol</ta>
            <ta e="T311" id="Seg_5634" s="T310">barɨ-ta</ta>
            <ta e="T312" id="Seg_5635" s="T311">biːrge</ta>
            <ta e="T313" id="Seg_5636" s="T312">ogus-tu-lar</ta>
            <ta e="T314" id="Seg_5637" s="T313">nʼemʼes-ter</ta>
            <ta e="T315" id="Seg_5638" s="T314">baːr</ta>
            <ta e="T316" id="Seg_5639" s="T315">hir-deri-n</ta>
            <ta e="T317" id="Seg_5640" s="T316">nʼemʼes</ta>
            <ta e="T318" id="Seg_5641" s="T317">armʼija-ta</ta>
            <ta e="T319" id="Seg_5642" s="T318">iti-ni</ta>
            <ta e="T320" id="Seg_5643" s="T319">dolgun-naːk</ta>
            <ta e="T321" id="Seg_5644" s="T320">ulakan</ta>
            <ta e="T322" id="Seg_5645" s="T321">oks-uː-nu</ta>
            <ta e="T323" id="Seg_5646" s="T322">tuluj-bakka</ta>
            <ta e="T324" id="Seg_5647" s="T323">töttörü</ta>
            <ta e="T325" id="Seg_5648" s="T324">supturuj-but-a</ta>
            <ta e="T326" id="Seg_5649" s="T325">sonno-koːn</ta>
            <ta e="T327" id="Seg_5650" s="T326">u͡on</ta>
            <ta e="T328" id="Seg_5651" s="T327">bi͡es</ta>
            <ta e="T329" id="Seg_5652" s="T328">beriste-leːk</ta>
            <ta e="T330" id="Seg_5653" s="T329">sir-i</ta>
            <ta e="T331" id="Seg_5654" s="T330">töttörü</ta>
            <ta e="T332" id="Seg_5655" s="T331">ɨstam-mɨt-a</ta>
            <ta e="T333" id="Seg_5656" s="T332">iti</ta>
            <ta e="T334" id="Seg_5657" s="T333">ulakan</ta>
            <ta e="T335" id="Seg_5658" s="T334">oks-uː</ta>
            <ta e="T336" id="Seg_5659" s="T335">kenn-i-tten</ta>
            <ta e="T337" id="Seg_5660" s="T336">Kurskaj</ta>
            <ta e="T338" id="Seg_5661" s="T337">di͡e-n</ta>
            <ta e="T339" id="Seg_5662" s="T338">tokur-a</ta>
            <ta e="T340" id="Seg_5663" s="T339">berke</ta>
            <ta e="T341" id="Seg_5664" s="T340">köm-müt-e</ta>
            <ta e="T342" id="Seg_5665" s="T341">nʼemʼes</ta>
            <ta e="T343" id="Seg_5666" s="T342">armʼija-tɨ-n</ta>
            <ta e="T344" id="Seg_5667" s="T343">töttörü</ta>
            <ta e="T345" id="Seg_5668" s="T344">bihigi</ta>
            <ta e="T346" id="Seg_5669" s="T345">bap-pɨp-pɨt</ta>
            <ta e="T347" id="Seg_5670" s="T346">iti</ta>
            <ta e="T348" id="Seg_5671" s="T347">kenn-i-tten</ta>
            <ta e="T349" id="Seg_5672" s="T348">nʼemʼes-ter</ta>
            <ta e="T350" id="Seg_5673" s="T349">hereges</ta>
            <ta e="T351" id="Seg_5674" s="T350">saldaːt-tara</ta>
            <ta e="T352" id="Seg_5675" s="T351">inni-leri-n</ta>
            <ta e="T353" id="Seg_5676" s="T352">bihi͡e-ke</ta>
            <ta e="T354" id="Seg_5677" s="T353">bi͡er-en</ta>
            <ta e="T355" id="Seg_5678" s="T354">baraːn</ta>
            <ta e="T356" id="Seg_5679" s="T355">suptu</ta>
            <ta e="T357" id="Seg_5680" s="T356">seriː</ta>
            <ta e="T358" id="Seg_5681" s="T357">büt-ü͡ög-ü-ger</ta>
            <ta e="T359" id="Seg_5682" s="T358">di͡eri</ta>
            <ta e="T360" id="Seg_5683" s="T359">öj-dörü-n-puːt-tarɨ-n</ta>
            <ta e="T361" id="Seg_5684" s="T360">bul-batak-tara</ta>
            <ta e="T362" id="Seg_5685" s="T361">ku͡ot-an</ta>
            <ta e="T363" id="Seg_5686" s="T362">is-pit-tere</ta>
            <ta e="T364" id="Seg_5687" s="T363">bihigi</ta>
            <ta e="T365" id="Seg_5688" s="T364">armʼija-bɨt</ta>
            <ta e="T366" id="Seg_5689" s="T365">bert</ta>
            <ta e="T367" id="Seg_5690" s="T366">üčügej-dik</ta>
            <ta e="T368" id="Seg_5691" s="T367">küːh-ü-n</ta>
            <ta e="T369" id="Seg_5692" s="T368">tut-an</ta>
            <ta e="T370" id="Seg_5693" s="T369">baraːn</ta>
            <ta e="T371" id="Seg_5694" s="T370">nʼemʼes-ter-i</ta>
            <ta e="T372" id="Seg_5695" s="T371">kenni</ta>
            <ta e="T373" id="Seg_5696" s="T372">di͡ek</ta>
            <ta e="T374" id="Seg_5697" s="T373">köllör-bökkö</ta>
            <ta e="T375" id="Seg_5698" s="T374">delbi</ta>
            <ta e="T376" id="Seg_5699" s="T375">bap-pɨt-a</ta>
            <ta e="T377" id="Seg_5700" s="T376">min</ta>
            <ta e="T378" id="Seg_5701" s="T377">kojuk-ku</ta>
            <ta e="T379" id="Seg_5702" s="T378">kün-ner-ge</ta>
            <ta e="T380" id="Seg_5703" s="T379">seriː-ge</ta>
            <ta e="T381" id="Seg_5704" s="T380">hɨldʼ-am-mɨn</ta>
            <ta e="T382" id="Seg_5705" s="T381">itinnik</ta>
            <ta e="T383" id="Seg_5706" s="T382">sür-deːk</ta>
            <ta e="T384" id="Seg_5707" s="T383">kihi</ta>
            <ta e="T385" id="Seg_5708" s="T952">kördük</ta>
            <ta e="T387" id="Seg_5709" s="T385">kör-bötög-ü-m</ta>
            <ta e="T388" id="Seg_5710" s="T387">min</ta>
            <ta e="T389" id="Seg_5711" s="T388">iti</ta>
            <ta e="T390" id="Seg_5712" s="T389">hir-ge</ta>
            <ta e="T391" id="Seg_5713" s="T390">saŋardɨː</ta>
            <ta e="T392" id="Seg_5714" s="T391">kör-büt-ü-m</ta>
            <ta e="T393" id="Seg_5715" s="T392">hu͡os-taːk</ta>
            <ta e="T394" id="Seg_5716" s="T393">kersiː-ni</ta>
            <ta e="T395" id="Seg_5717" s="T394">kajtak</ta>
            <ta e="T396" id="Seg_5718" s="T395">seriː</ta>
            <ta e="T397" id="Seg_5719" s="T396">kihi-ti-n</ta>
            <ta e="T398" id="Seg_5720" s="T397">kaːn-a</ta>
            <ta e="T399" id="Seg_5721" s="T398">ürek-tiː</ta>
            <ta e="T400" id="Seg_5722" s="T399">tok-t-or-u-n</ta>
            <ta e="T401" id="Seg_5723" s="T400">töhölöːk</ta>
            <ta e="T402" id="Seg_5724" s="T401">tɨːhɨčča</ta>
            <ta e="T403" id="Seg_5725" s="T402">kihi</ta>
            <ta e="T404" id="Seg_5726" s="T403">tɨːn-a</ta>
            <ta e="T405" id="Seg_5727" s="T404">bɨst-ar-ɨ-n</ta>
            <ta e="T406" id="Seg_5728" s="T405">töhölöːk</ta>
            <ta e="T407" id="Seg_5729" s="T406">gojobuːn</ta>
            <ta e="T408" id="Seg_5730" s="T407">bu͡ol-but</ta>
            <ta e="T409" id="Seg_5731" s="T408">kihi</ta>
            <ta e="T410" id="Seg_5732" s="T409">ulakan-nɨk</ta>
            <ta e="T411" id="Seg_5733" s="T410">erej-den-er-i-n</ta>
            <ta e="T412" id="Seg_5734" s="T411">min</ta>
            <ta e="T413" id="Seg_5735" s="T412">emi͡e</ta>
            <ta e="T414" id="Seg_5736" s="T413">iti</ta>
            <ta e="T415" id="Seg_5737" s="T414">oks-o-h-uː-ga</ta>
            <ta e="T416" id="Seg_5738" s="T415">Kurskaj</ta>
            <ta e="T417" id="Seg_5739" s="T416">tumuh-u-gar</ta>
            <ta e="T418" id="Seg_5740" s="T417">atdʼeleʼnʼija</ta>
            <ta e="T419" id="Seg_5741" s="T418">kamandʼir-a</ta>
            <ta e="T420" id="Seg_5742" s="T419">bu͡ol-a</ta>
            <ta e="T421" id="Seg_5743" s="T420">hɨldʼ-am-mɨn</ta>
            <ta e="T422" id="Seg_5744" s="T421">dʼogus-tuk</ta>
            <ta e="T423" id="Seg_5745" s="T422">kaŋas</ta>
            <ta e="T424" id="Seg_5746" s="T423">atak-pɨ-n</ta>
            <ta e="T425" id="Seg_5747" s="T424">bɨlčɨŋ-ɨ</ta>
            <ta e="T426" id="Seg_5748" s="T425">gojobuːn-n-a-p-pɨt-ɨ-m</ta>
            <ta e="T427" id="Seg_5749" s="T426">ol</ta>
            <ta e="T428" id="Seg_5750" s="T427">bu͡ol-an</ta>
            <ta e="T429" id="Seg_5751" s="T428">baraːn</ta>
            <ta e="T430" id="Seg_5752" s="T429">seriːleh-er</ta>
            <ta e="T431" id="Seg_5753" s="T430">sir-ten</ta>
            <ta e="T432" id="Seg_5754" s="T431">taks-ɨ-batag-ɨ-m</ta>
            <ta e="T433" id="Seg_5755" s="T432">iti</ta>
            <ta e="T434" id="Seg_5756" s="T433">sir-deːk</ta>
            <ta e="T435" id="Seg_5757" s="T434">oks-o-h-uː-ga</ta>
            <ta e="T436" id="Seg_5758" s="T435">min</ta>
            <ta e="T437" id="Seg_5759" s="T436">baːr</ta>
            <ta e="T438" id="Seg_5760" s="T437">agaj</ta>
            <ta e="T439" id="Seg_5761" s="T438">čaːh-ɨ-m</ta>
            <ta e="T440" id="Seg_5762" s="T439">tɨːhɨčča-ttan</ta>
            <ta e="T441" id="Seg_5763" s="T440">taksa</ta>
            <ta e="T442" id="Seg_5764" s="T441">seriː</ta>
            <ta e="T443" id="Seg_5765" s="T442">kihi-ti-tten</ta>
            <ta e="T444" id="Seg_5766" s="T443">iti</ta>
            <ta e="T445" id="Seg_5767" s="T444">u͡on</ta>
            <ta e="T446" id="Seg_5768" s="T445">kuŋ-ŋe</ta>
            <ta e="T447" id="Seg_5769" s="T446">oks-o-h-uː-ga</ta>
            <ta e="T448" id="Seg_5770" s="T447">or-put-a</ta>
            <ta e="T449" id="Seg_5771" s="T448">u͡on</ta>
            <ta e="T450" id="Seg_5772" s="T449">biːr</ta>
            <ta e="T451" id="Seg_5773" s="T450">agaj</ta>
            <ta e="T452" id="Seg_5774" s="T451">kihi</ta>
            <ta e="T453" id="Seg_5775" s="T452">iti</ta>
            <ta e="T454" id="Seg_5776" s="T453">ih-i-ger</ta>
            <ta e="T455" id="Seg_5777" s="T454">össü͡ö</ta>
            <ta e="T456" id="Seg_5778" s="T455">biːr</ta>
            <ta e="T457" id="Seg_5779" s="T456">min</ta>
            <ta e="T458" id="Seg_5780" s="T457">gojobuːn</ta>
            <ta e="T459" id="Seg_5781" s="T458">bu͡ol-but</ta>
            <ta e="T460" id="Seg_5782" s="T459">kihi</ta>
            <ta e="T461" id="Seg_5783" s="T460">baːr</ta>
            <ta e="T462" id="Seg_5784" s="T461">e-ti-m</ta>
            <ta e="T463" id="Seg_5785" s="T462">itin-ten</ta>
            <ta e="T464" id="Seg_5786" s="T463">söp</ta>
            <ta e="T465" id="Seg_5787" s="T464">bu͡ol-u͡og-a</ta>
            <ta e="T466" id="Seg_5788" s="T465">kol-u͡ok-ka</ta>
            <ta e="T467" id="Seg_5789" s="T466">kajtak</ta>
            <ta e="T468" id="Seg_5790" s="T467">aldʼarkaj-daːk</ta>
            <ta e="T469" id="Seg_5791" s="T468">seriː</ta>
            <ta e="T470" id="Seg_5792" s="T469">e-t-i-n</ta>
            <ta e="T471" id="Seg_5793" s="T470">Kurskaj</ta>
            <ta e="T472" id="Seg_5794" s="T471">togur-u-gor</ta>
            <ta e="T473" id="Seg_5795" s="T472">töhö</ta>
            <ta e="T474" id="Seg_5796" s="T473">kaːn</ta>
            <ta e="T475" id="Seg_5797" s="T474">tok-t-u-but-u-n</ta>
            <ta e="T476" id="Seg_5798" s="T475">töhö</ta>
            <ta e="T477" id="Seg_5799" s="T476">kihi</ta>
            <ta e="T478" id="Seg_5800" s="T477">öl-büt-ü-n</ta>
            <ta e="T479" id="Seg_5801" s="T478">Kurskaj-ga</ta>
            <ta e="T480" id="Seg_5802" s="T479">büt-en</ta>
            <ta e="T481" id="Seg_5803" s="T480">baraːn</ta>
            <ta e="T482" id="Seg_5804" s="T481">bihigi</ta>
            <ta e="T483" id="Seg_5805" s="T482">armʼija-bɨt</ta>
            <ta e="T484" id="Seg_5806" s="T483">töttörü</ta>
            <ta e="T485" id="Seg_5807" s="T484">kürüː-r</ta>
            <ta e="T486" id="Seg_5808" s="T485">nʼemʼeh-i</ta>
            <ta e="T487" id="Seg_5809" s="T486">zaːpad</ta>
            <ta e="T488" id="Seg_5810" s="T487">di͡ek</ta>
            <ta e="T489" id="Seg_5811" s="T488">bat-an</ta>
            <ta e="T490" id="Seg_5812" s="T489">bar-bɨt-a</ta>
            <ta e="T491" id="Seg_5813" s="T490">iti</ta>
            <ta e="T492" id="Seg_5814" s="T491">Kurskaj</ta>
            <ta e="T493" id="Seg_5815" s="T492">kenn-i-tten</ta>
            <ta e="T494" id="Seg_5816" s="T493">min</ta>
            <ta e="T495" id="Seg_5817" s="T494">saːmaj</ta>
            <ta e="T496" id="Seg_5818" s="T495">seriː</ta>
            <ta e="T497" id="Seg_5819" s="T496">kerse</ta>
            <ta e="T498" id="Seg_5820" s="T497">sir-i-ger</ta>
            <ta e="T499" id="Seg_5821" s="T498">baːr</ta>
            <ta e="T500" id="Seg_5822" s="T499">e-ti-m</ta>
            <ta e="T501" id="Seg_5823" s="T500">tɨːhɨčča</ta>
            <ta e="T502" id="Seg_5824" s="T501">togus</ta>
            <ta e="T503" id="Seg_5825" s="T502">süːs</ta>
            <ta e="T504" id="Seg_5826" s="T503">tü͡örd-u͡on</ta>
            <ta e="T505" id="Seg_5827" s="T504">tü͡ört</ta>
            <ta e="T506" id="Seg_5828" s="T505">dʼɨl-laːk-ka</ta>
            <ta e="T507" id="Seg_5829" s="T506">kɨhɨŋ-ŋɨ</ta>
            <ta e="T508" id="Seg_5830" s="T507">janvarʼ</ta>
            <ta e="T509" id="Seg_5831" s="T508">ɨj-ga</ta>
            <ta e="T510" id="Seg_5832" s="T509">di͡eri</ta>
            <ta e="T511" id="Seg_5833" s="T510">iti</ta>
            <ta e="T512" id="Seg_5834" s="T511">kem-ŋe</ta>
            <ta e="T513" id="Seg_5835" s="T512">nʼemʼes</ta>
            <ta e="T514" id="Seg_5836" s="T513">saldaːt-tarɨ-n</ta>
            <ta e="T515" id="Seg_5837" s="T514">gɨtta</ta>
            <ta e="T516" id="Seg_5838" s="T515">utara</ta>
            <ta e="T517" id="Seg_5839" s="T516">tur-an</ta>
            <ta e="T518" id="Seg_5840" s="T517">seriːles-pit-i-m</ta>
            <ta e="T519" id="Seg_5841" s="T518">munnuk</ta>
            <ta e="T520" id="Seg_5842" s="T519">sir-der-ge</ta>
            <ta e="T521" id="Seg_5843" s="T520">Bʼelgarad</ta>
            <ta e="T522" id="Seg_5844" s="T521">onton</ta>
            <ta e="T523" id="Seg_5845" s="T522">Xarʼkav</ta>
            <ta e="T524" id="Seg_5846" s="T523">onton</ta>
            <ta e="T525" id="Seg_5847" s="T524">Poltava</ta>
            <ta e="T526" id="Seg_5848" s="T525">onton</ta>
            <ta e="T527" id="Seg_5849" s="T526">Krʼemʼenčʼug</ta>
            <ta e="T528" id="Seg_5850" s="T527">onton</ta>
            <ta e="T529" id="Seg_5851" s="T528">Krʼukov</ta>
            <ta e="T530" id="Seg_5852" s="T529">di͡e-n</ta>
            <ta e="T531" id="Seg_5853" s="T530">aːt-taːk</ta>
            <ta e="T532" id="Seg_5854" s="T531">ulakan</ta>
            <ta e="T533" id="Seg_5855" s="T532">gu͡orat-tar-ɨ</ta>
            <ta e="T534" id="Seg_5856" s="T533">ɨl-ar-ga</ta>
            <ta e="T535" id="Seg_5857" s="T534">ukrainʼes</ta>
            <ta e="T536" id="Seg_5858" s="T535">di͡e-n</ta>
            <ta e="T537" id="Seg_5859" s="T536">omuk</ta>
            <ta e="T538" id="Seg_5860" s="T537">sir-i-ger</ta>
            <ta e="T539" id="Seg_5861" s="T538">iti</ta>
            <ta e="T540" id="Seg_5862" s="T539">gu͡orat-tar-tan</ta>
            <ta e="T541" id="Seg_5863" s="T540">olus</ta>
            <ta e="T542" id="Seg_5864" s="T541">kɨtaːnak-tɨk</ta>
            <ta e="T543" id="Seg_5865" s="T542">bihigi</ta>
            <ta e="T544" id="Seg_5866" s="T543">seriːles-pip-pit</ta>
            <ta e="T545" id="Seg_5867" s="T544">nʼemʼeh-i</ta>
            <ta e="T546" id="Seg_5868" s="T545">gɨtta</ta>
            <ta e="T547" id="Seg_5869" s="T546">Xarʼkav</ta>
            <ta e="T548" id="Seg_5870" s="T547">gu͡orat</ta>
            <ta e="T549" id="Seg_5871" s="T548">bɨldʼ-a-h-ɨː-ga</ta>
            <ta e="T550" id="Seg_5872" s="T549">iti</ta>
            <ta e="T551" id="Seg_5873" s="T550">gu͡orat</ta>
            <ta e="T552" id="Seg_5874" s="T551">üs-ke</ta>
            <ta e="T553" id="Seg_5875" s="T552">di͡eri</ta>
            <ta e="T554" id="Seg_5876" s="T553">töttörü taːrɨ</ta>
            <ta e="T555" id="Seg_5877" s="T554">iliː-tten</ta>
            <ta e="T556" id="Seg_5878" s="T555">iliː-ge</ta>
            <ta e="T557" id="Seg_5879" s="T556">kel-e</ta>
            <ta e="T558" id="Seg_5880" s="T557">hɨldʼ-ɨ-bɨt-a</ta>
            <ta e="T559" id="Seg_5881" s="T558">onton</ta>
            <ta e="T560" id="Seg_5882" s="T559">üh-üs</ta>
            <ta e="T561" id="Seg_5883" s="T560">kel-iː-ti-ger</ta>
            <ta e="T562" id="Seg_5884" s="T561">bihigi</ta>
            <ta e="T563" id="Seg_5885" s="T562">iliː-bit-ten</ta>
            <ta e="T564" id="Seg_5886" s="T563">ɨːp-patak-pɨt</ta>
            <ta e="T565" id="Seg_5887" s="T564">onton</ta>
            <ta e="T566" id="Seg_5888" s="T565">ulakan</ta>
            <ta e="T567" id="Seg_5889" s="T566">oks-o-h-uː</ta>
            <ta e="T568" id="Seg_5890" s="T567">össü͡ö</ta>
            <ta e="T569" id="Seg_5891" s="T568">baːr</ta>
            <ta e="T570" id="Seg_5892" s="T569">e-t-e</ta>
            <ta e="T571" id="Seg_5893" s="T570">Dnʼepr </ta>
            <ta e="T572" id="Seg_5894" s="T571">di͡e-n</ta>
            <ta e="T573" id="Seg_5895" s="T572">aːt-taːk</ta>
            <ta e="T574" id="Seg_5896" s="T573">ebe</ta>
            <ta e="T575" id="Seg_5897" s="T574">keh-iː-ti-ger</ta>
            <ta e="T576" id="Seg_5898" s="T575">iti</ta>
            <ta e="T577" id="Seg_5899" s="T576">ebe</ta>
            <ta e="T578" id="Seg_5900" s="T577">oŋu͡or-gu</ta>
            <ta e="T579" id="Seg_5901" s="T578">öttü-tü-ger</ta>
            <ta e="T580" id="Seg_5902" s="T579">olor-on</ta>
            <ta e="T581" id="Seg_5903" s="T580">nʼemʼes-ter</ta>
            <ta e="T582" id="Seg_5904" s="T581">künüs-ter-i-tüːn-ner-i</ta>
            <ta e="T583" id="Seg_5905" s="T582">ket-iː</ta>
            <ta e="T584" id="Seg_5906" s="T583">hɨldʼ-an-nar</ta>
            <ta e="T585" id="Seg_5907" s="T584">bihigi-ni</ta>
            <ta e="T586" id="Seg_5908" s="T585">amattan</ta>
            <ta e="T587" id="Seg_5909" s="T586">sataː-n</ta>
            <ta e="T588" id="Seg_5910" s="T587">keh-e-r-pet</ta>
            <ta e="T589" id="Seg_5911" s="T588">e-ti-lere</ta>
            <ta e="T590" id="Seg_5912" s="T589">ol</ta>
            <ta e="T591" id="Seg_5913" s="T590">ihin</ta>
            <ta e="T592" id="Seg_5914" s="T591">bihigi</ta>
            <ta e="T593" id="Seg_5915" s="T592">kas</ta>
            <ta e="T594" id="Seg_5916" s="T593">da</ta>
            <ta e="T595" id="Seg_5917" s="T594">kün-ü</ta>
            <ta e="T596" id="Seg_5918" s="T595">kot-on</ta>
            <ta e="T597" id="Seg_5919" s="T596">taks-ɨ-batak-pɨt</ta>
            <ta e="T598" id="Seg_5920" s="T597">betereː-gi</ta>
            <ta e="T599" id="Seg_5921" s="T598">kɨtɨl-ga</ta>
            <ta e="T600" id="Seg_5922" s="T599">olor-bup-put</ta>
            <ta e="T601" id="Seg_5923" s="T600">ebe-ni</ta>
            <ta e="T602" id="Seg_5924" s="T601">taks-ar</ta>
            <ta e="T603" id="Seg_5925" s="T602">mostɨ-lar-ɨ</ta>
            <ta e="T604" id="Seg_5926" s="T603">künüs</ta>
            <ta e="T605" id="Seg_5927" s="T604">oŋor-or-butu-n</ta>
            <ta e="T606" id="Seg_5928" s="T605">nʼemʼes-ter</ta>
            <ta e="T607" id="Seg_5929" s="T606">samalʼot-tara</ta>
            <ta e="T608" id="Seg_5930" s="T607">köt-ön</ta>
            <ta e="T609" id="Seg_5931" s="T608">kel-e-kel-e</ta>
            <ta e="T610" id="Seg_5932" s="T609">töhö</ta>
            <ta e="T611" id="Seg_5933" s="T610">eme</ta>
            <ta e="T612" id="Seg_5934" s="T611">bomba-lar-ɨ</ta>
            <ta e="T613" id="Seg_5935" s="T612">bɨrag-attaː-n</ta>
            <ta e="T614" id="Seg_5936" s="T613">delbi</ta>
            <ta e="T615" id="Seg_5937" s="T614">aldʼat-ar</ta>
            <ta e="T616" id="Seg_5938" s="T615">e-t-e</ta>
            <ta e="T617" id="Seg_5939" s="T616">ol</ta>
            <ta e="T618" id="Seg_5940" s="T617">ihin</ta>
            <ta e="T619" id="Seg_5941" s="T618">kojut</ta>
            <ta e="T620" id="Seg_5942" s="T619">bihigi</ta>
            <ta e="T621" id="Seg_5943" s="T620">tüːn</ta>
            <ta e="T622" id="Seg_5944" s="T621">kahu͡on</ta>
            <ta e="T623" id="Seg_5945" s="T622">sir-i-nen</ta>
            <ta e="T624" id="Seg_5946" s="T623">mostɨ-lar-ɨ</ta>
            <ta e="T625" id="Seg_5947" s="T624">oŋor-tuː-r</ta>
            <ta e="T626" id="Seg_5948" s="T625">e-ti-bit</ta>
            <ta e="T627" id="Seg_5949" s="T626">abaːhɨ</ta>
            <ta e="T628" id="Seg_5950" s="T627">nʼemʼis</ta>
            <ta e="T629" id="Seg_5951" s="T628">tüːn</ta>
            <ta e="T630" id="Seg_5952" s="T629">daː</ta>
            <ta e="T631" id="Seg_5953" s="T630">oŋor-but</ta>
            <ta e="T632" id="Seg_5954" s="T631">mostɨ-lar-bɨtɨ-n</ta>
            <ta e="T633" id="Seg_5955" s="T632">tur-u͡or-bat</ta>
            <ta e="T634" id="Seg_5956" s="T633">e-t-e</ta>
            <ta e="T635" id="Seg_5957" s="T634">oččogo</ta>
            <ta e="T636" id="Seg_5958" s="T635">bihigi</ta>
            <ta e="T637" id="Seg_5959" s="T636">di͡egi-ler</ta>
            <ta e="T638" id="Seg_5960" s="T637">delbi</ta>
            <ta e="T639" id="Seg_5961" s="T638">kɨjŋan-an</ta>
            <ta e="T640" id="Seg_5962" s="T639">baraːn</ta>
            <ta e="T641" id="Seg_5963" s="T640">barɨ</ta>
            <ta e="T642" id="Seg_5964" s="T641">seriː</ta>
            <ta e="T643" id="Seg_5965" s="T642">saː-tɨ-nan</ta>
            <ta e="T644" id="Seg_5966" s="T643">delbi</ta>
            <ta e="T645" id="Seg_5967" s="T644">ɨt-ɨ͡alaː-n</ta>
            <ta e="T646" id="Seg_5968" s="T645">baraːn</ta>
            <ta e="T647" id="Seg_5969" s="T646">töhö</ta>
            <ta e="T648" id="Seg_5970" s="T647">eme</ta>
            <ta e="T649" id="Seg_5971" s="T648">u͡on-nuː</ta>
            <ta e="T650" id="Seg_5972" s="T649">samalʼot-tar-ɨ</ta>
            <ta e="T651" id="Seg_5973" s="T650">oŋu͡or-gu</ta>
            <ta e="T652" id="Seg_5974" s="T651">ött-ü-ge</ta>
            <ta e="T653" id="Seg_5975" s="T652">ɨːt-an</ta>
            <ta e="T654" id="Seg_5976" s="T653">baraːn</ta>
            <ta e="T655" id="Seg_5977" s="T654">iti-ni</ta>
            <ta e="T656" id="Seg_5978" s="T655">barɨ-tɨ-n</ta>
            <ta e="T657" id="Seg_5979" s="T656">bürü-n-en</ta>
            <ta e="T658" id="Seg_5980" s="T657">tur-an</ta>
            <ta e="T659" id="Seg_5981" s="T658">oŋu͡or-gu</ta>
            <ta e="T660" id="Seg_5982" s="T659">di͡ek</ta>
            <ta e="T661" id="Seg_5983" s="T660">bɨh-a</ta>
            <ta e="T662" id="Seg_5984" s="T661">mökkü-h-e</ta>
            <ta e="T663" id="Seg_5985" s="T662">taks-ɨ-bɨt-tara</ta>
            <ta e="T664" id="Seg_5986" s="T663">bihigi</ta>
            <ta e="T665" id="Seg_5987" s="T664">saldaːt-tar-bɨt</ta>
            <ta e="T666" id="Seg_5988" s="T665">ol</ta>
            <ta e="T667" id="Seg_5989" s="T666">mökkü͡ön</ta>
            <ta e="T668" id="Seg_5990" s="T667">ajdaːn-ɨ-gar</ta>
            <ta e="T669" id="Seg_5991" s="T668">mosta-ga</ta>
            <ta e="T670" id="Seg_5992" s="T669">bap-pakka</ta>
            <ta e="T671" id="Seg_5993" s="T670">aŋar</ta>
            <ta e="T672" id="Seg_5994" s="T671">kihi</ta>
            <ta e="T673" id="Seg_5995" s="T672">lu͡otka-nnan</ta>
            <ta e="T674" id="Seg_5996" s="T673">sorok-tor</ta>
            <ta e="T675" id="Seg_5997" s="T674">kas-tɨː</ta>
            <ta e="T676" id="Seg_5998" s="T675">emi͡e</ta>
            <ta e="T677" id="Seg_5999" s="T676">bremnu͡o-lar-ɨ</ta>
            <ta e="T678" id="Seg_6000" s="T677">biːrge</ta>
            <ta e="T679" id="Seg_6001" s="T678">kolb-uː</ta>
            <ta e="T680" id="Seg_6002" s="T679">baːj-an</ta>
            <ta e="T681" id="Seg_6003" s="T680">baraːn</ta>
            <ta e="T682" id="Seg_6004" s="T681">aŋar-dar</ta>
            <ta e="T683" id="Seg_6005" s="T682">köŋül</ta>
            <ta e="T684" id="Seg_6006" s="T683">karbaː-n</ta>
            <ta e="T685" id="Seg_6007" s="T684">taks-ɨ-bɨt-tara</ta>
            <ta e="T686" id="Seg_6008" s="T685">Dnʼepr</ta>
            <ta e="T687" id="Seg_6009" s="T686">di͡e-n</ta>
            <ta e="T688" id="Seg_6010" s="T687">ebe-ni</ta>
            <ta e="T689" id="Seg_6011" s="T688">onno</ta>
            <ta e="T690" id="Seg_6012" s="T689">ajdaːn</ta>
            <ta e="T691" id="Seg_6013" s="T690">ulakan</ta>
            <ta e="T692" id="Seg_6014" s="T691">ü͡ögüː</ta>
            <ta e="T693" id="Seg_6015" s="T692">kahɨː</ta>
            <ta e="T694" id="Seg_6016" s="T693">kersiː</ta>
            <ta e="T695" id="Seg_6017" s="T694">tu͡ok-tan</ta>
            <ta e="T696" id="Seg_6018" s="T695">da</ta>
            <ta e="T697" id="Seg_6019" s="T696">atɨn</ta>
            <ta e="T698" id="Seg_6020" s="T697">seriː</ta>
            <ta e="T699" id="Seg_6021" s="T698">saː-tɨ-n</ta>
            <ta e="T700" id="Seg_6022" s="T699">tɨ͡ah-a</ta>
            <ta e="T701" id="Seg_6023" s="T700">sür-deːk</ta>
            <ta e="T702" id="Seg_6024" s="T701">e-t-e</ta>
            <ta e="T703" id="Seg_6025" s="T702">nʼemʼes</ta>
            <ta e="T704" id="Seg_6026" s="T703">di͡egi</ta>
            <ta e="T705" id="Seg_6027" s="T704">öttü</ta>
            <ta e="T706" id="Seg_6028" s="T705">iti</ta>
            <ta e="T707" id="Seg_6029" s="T706">bɨh-a</ta>
            <ta e="T708" id="Seg_6030" s="T707">mökküj-üː-nü</ta>
            <ta e="T709" id="Seg_6031" s="T708">kot-on</ta>
            <ta e="T710" id="Seg_6032" s="T709">tup-pakka</ta>
            <ta e="T711" id="Seg_6033" s="T710">meːne</ta>
            <ta e="T712" id="Seg_6034" s="T711">ebe</ta>
            <ta e="T713" id="Seg_6035" s="T712">ürd-ü-nen</ta>
            <ta e="T714" id="Seg_6036" s="T713">ɨt-ɨ͡alɨː-r</ta>
            <ta e="T715" id="Seg_6037" s="T714">e-t-e</ta>
            <ta e="T716" id="Seg_6038" s="T715">kɨtɨl-tan</ta>
            <ta e="T717" id="Seg_6039" s="T716">kör-dök-kö</ta>
            <ta e="T718" id="Seg_6040" s="T717">ebe</ta>
            <ta e="T719" id="Seg_6041" s="T718">di͡ek</ta>
            <ta e="T720" id="Seg_6042" s="T719">uː-ga</ta>
            <ta e="T721" id="Seg_6043" s="T720">bukatɨn</ta>
            <ta e="T722" id="Seg_6044" s="T721">kihi</ta>
            <ta e="T723" id="Seg_6045" s="T722">tug-u</ta>
            <ta e="T724" id="Seg_6046" s="T723">da</ta>
            <ta e="T725" id="Seg_6047" s="T724">tu͡olkul-u͡ok</ta>
            <ta e="T726" id="Seg_6048" s="T725">bu͡ol-batak</ta>
            <ta e="T727" id="Seg_6049" s="T726">e-t-e</ta>
            <ta e="T728" id="Seg_6050" s="T727">kolobur-a</ta>
            <ta e="T729" id="Seg_6051" s="T728">ürg-ü-büt</ta>
            <ta e="T730" id="Seg_6052" s="T729">ulakan</ta>
            <ta e="T731" id="Seg_6053" s="T730">kɨːl</ta>
            <ta e="T732" id="Seg_6054" s="T731">ü͡ör-e</ta>
            <ta e="T733" id="Seg_6055" s="T732">karbɨː-r-ɨ-n</ta>
            <ta e="T734" id="Seg_6056" s="T733">kördük</ta>
            <ta e="T735" id="Seg_6057" s="T734">e-t-e</ta>
            <ta e="T736" id="Seg_6058" s="T735">iti</ta>
            <ta e="T737" id="Seg_6059" s="T736">ebe</ta>
            <ta e="T738" id="Seg_6060" s="T737">keh-iː-ti-ger</ta>
            <ta e="T739" id="Seg_6061" s="T738">töhölöːk</ta>
            <ta e="T740" id="Seg_6062" s="T739">kihi</ta>
            <ta e="T741" id="Seg_6063" s="T740">tumnast-an</ta>
            <ta e="T742" id="Seg_6064" s="T741">seriː</ta>
            <ta e="T743" id="Seg_6065" s="T742">saː-tɨ-ttan</ta>
            <ta e="T744" id="Seg_6066" s="T743">öl-büt-ü-n</ta>
            <ta e="T745" id="Seg_6067" s="T744">kim</ta>
            <ta e="T746" id="Seg_6068" s="T745">daːgɨnɨ</ta>
            <ta e="T747" id="Seg_6069" s="T746">bil-bet</ta>
            <ta e="T748" id="Seg_6070" s="T747">oččogo</ta>
            <ta e="T749" id="Seg_6071" s="T748">e-t-e</ta>
            <ta e="T750" id="Seg_6072" s="T749">ebe-ni</ta>
            <ta e="T751" id="Seg_6073" s="T750">taks-an</ta>
            <ta e="T752" id="Seg_6074" s="T751">baraːn</ta>
            <ta e="T753" id="Seg_6075" s="T752">bihi͡ettere</ta>
            <ta e="T754" id="Seg_6076" s="T753">emi͡e</ta>
            <ta e="T755" id="Seg_6077" s="T754">nʼemʼes-ter-i</ta>
            <ta e="T756" id="Seg_6078" s="T755">solo</ta>
            <ta e="T757" id="Seg_6079" s="T756">bul-lar-bakka</ta>
            <ta e="T758" id="Seg_6080" s="T757">delbi</ta>
            <ta e="T759" id="Seg_6081" s="T758">bap-pɨt-tara</ta>
            <ta e="T760" id="Seg_6082" s="T759">onton</ta>
            <ta e="T761" id="Seg_6083" s="T760">min</ta>
            <ta e="T762" id="Seg_6084" s="T761">töhö</ta>
            <ta e="T763" id="Seg_6085" s="T762">emete</ta>
            <ta e="T764" id="Seg_6086" s="T763">seriː-ge</ta>
            <ta e="T765" id="Seg_6087" s="T764">bar-am-mɨn</ta>
            <ta e="T766" id="Seg_6088" s="T765">törd-üs-teːk</ta>
            <ta e="T767" id="Seg_6089" s="T766">kün-ü-ger</ta>
            <ta e="T768" id="Seg_6090" s="T767">janvarʼ</ta>
            <ta e="T769" id="Seg_6091" s="T768">ɨj-ga</ta>
            <ta e="T770" id="Seg_6092" s="T769">tɨːhɨčča</ta>
            <ta e="T771" id="Seg_6093" s="T770">togus</ta>
            <ta e="T772" id="Seg_6094" s="T771">süːs</ta>
            <ta e="T773" id="Seg_6095" s="T772">tü͡örd-u͡on</ta>
            <ta e="T774" id="Seg_6096" s="T773">üs</ta>
            <ta e="T775" id="Seg_6097" s="T774">dʼɨl-laːk-ka</ta>
            <ta e="T776" id="Seg_6098" s="T775">bütüges-pi-n</ta>
            <ta e="T777" id="Seg_6099" s="T776">gojobuːn-n-a-p-pɨt-ɨ-m</ta>
            <ta e="T778" id="Seg_6100" s="T777">bert</ta>
            <ta e="T779" id="Seg_6101" s="T778">ɨ͡arakan-nɨk</ta>
            <ta e="T780" id="Seg_6102" s="T779">kaŋas</ta>
            <ta e="T781" id="Seg_6103" s="T780">di͡eki</ta>
            <ta e="T782" id="Seg_6104" s="T781">buːt-u-m</ta>
            <ta e="T783" id="Seg_6105" s="T782">oŋu͡og-u-n</ta>
            <ta e="T784" id="Seg_6106" s="T783">delbi</ta>
            <ta e="T785" id="Seg_6107" s="T784">tohut-tar-bɨt-ɨ-m</ta>
            <ta e="T786" id="Seg_6108" s="T785">ihi-ni</ta>
            <ta e="T787" id="Seg_6109" s="T786">emt-e-t-em-min</ta>
            <ta e="T788" id="Seg_6110" s="T787">oh-u͡ok-pa-r</ta>
            <ta e="T789" id="Seg_6111" s="T788">di͡eri</ta>
            <ta e="T790" id="Seg_6112" s="T789">alta</ta>
            <ta e="T791" id="Seg_6113" s="T790">ɨj</ta>
            <ta e="T792" id="Seg_6114" s="T791">turkarɨ</ta>
            <ta e="T793" id="Seg_6115" s="T792">sɨp-pɨt-ɨ-m</ta>
            <ta e="T794" id="Seg_6116" s="T793">Essentuki</ta>
            <ta e="T795" id="Seg_6117" s="T794">di͡e-n</ta>
            <ta e="T796" id="Seg_6118" s="T795">gu͡orak-ka</ta>
            <ta e="T797" id="Seg_6119" s="T796">Kavkaːz</ta>
            <ta e="T798" id="Seg_6120" s="T797">di͡e-n</ta>
            <ta e="T799" id="Seg_6121" s="T798">itiː</ta>
            <ta e="T800" id="Seg_6122" s="T799">hir-ge</ta>
            <ta e="T801" id="Seg_6123" s="T800">onton</ta>
            <ta e="T802" id="Seg_6124" s="T801">emt-e-t-en</ta>
            <ta e="T803" id="Seg_6125" s="T802">baraːn</ta>
            <ta e="T804" id="Seg_6126" s="T803">mas</ta>
            <ta e="T805" id="Seg_6127" s="T804">tanʼak-taːk</ta>
            <ta e="T806" id="Seg_6128" s="T805">taks-an</ta>
            <ta e="T807" id="Seg_6129" s="T806">bar-aːm-mɨn</ta>
            <ta e="T808" id="Seg_6130" s="T807">seriː-tten</ta>
            <ta e="T809" id="Seg_6131" s="T808">adʼas</ta>
            <ta e="T810" id="Seg_6132" s="T809">boskol-o-m-mut-u-m</ta>
            <ta e="T811" id="Seg_6133" s="T810">armʼija-ttan</ta>
            <ta e="T813" id="Seg_6134" s="T812">taks-ɨ-bɨt-ɨ-m</ta>
            <ta e="T814" id="Seg_6135" s="T813">maŋnaj</ta>
            <ta e="T815" id="Seg_6136" s="T814">seriːles-pit</ta>
            <ta e="T816" id="Seg_6137" s="T815">küm-mü-tten</ta>
            <ta e="T817" id="Seg_6138" s="T816">biːr</ta>
            <ta e="T818" id="Seg_6139" s="T817">dʼɨl</ta>
            <ta e="T819" id="Seg_6140" s="T818">aŋar-ɨ-n</ta>
            <ta e="T820" id="Seg_6141" s="T819">turkarɨ</ta>
            <ta e="T821" id="Seg_6142" s="T820">seriː</ta>
            <ta e="T822" id="Seg_6143" s="T821">kersiː-ti-ger</ta>
            <ta e="T823" id="Seg_6144" s="T822">sɨldʼ-am-mɨn</ta>
            <ta e="T824" id="Seg_6145" s="T823">barɨta</ta>
            <ta e="T825" id="Seg_6146" s="T824">tü͡ör-ka</ta>
            <ta e="T826" id="Seg_6147" s="T825">kat</ta>
            <ta e="T827" id="Seg_6148" s="T826">gojobuːn-n-a-p-pɨt-ɨ-m</ta>
            <ta e="T828" id="Seg_6149" s="T827">iti</ta>
            <ta e="T829" id="Seg_6150" s="T828">tuh-u-nan</ta>
            <ta e="T830" id="Seg_6151" s="T829">üs-te</ta>
            <ta e="T831" id="Seg_6152" s="T830">emt-e-t-e</ta>
            <ta e="T832" id="Seg_6153" s="T831">hɨldʼ-ɨ-bɨt-ɨ-m</ta>
            <ta e="T833" id="Seg_6154" s="T832">kɨlgas-tɨk</ta>
            <ta e="T834" id="Seg_6155" s="T833">kepseː-tek-ke</ta>
            <ta e="T835" id="Seg_6156" s="T834">itigirdik</ta>
            <ta e="T836" id="Seg_6157" s="T835">min</ta>
            <ta e="T837" id="Seg_6158" s="T836">kihi</ta>
            <ta e="T838" id="Seg_6159" s="T837">aːjmak</ta>
            <ta e="T839" id="Seg_6160" s="T838">üje-ti-ger</ta>
            <ta e="T840" id="Seg_6161" s="T839">kaččaga</ta>
            <ta e="T841" id="Seg_6162" s="T840">da</ta>
            <ta e="T842" id="Seg_6163" s="T841">bil-l-i-betek</ta>
            <ta e="T843" id="Seg_6164" s="T842">aga</ta>
            <ta e="T844" id="Seg_6165" s="T843">da</ta>
            <ta e="T845" id="Seg_6166" s="T844">iti</ta>
            <ta e="T846" id="Seg_6167" s="T845">ulakan</ta>
            <ta e="T847" id="Seg_6168" s="T846">seriː-ti-ger</ta>
            <ta e="T848" id="Seg_6169" s="T847">beje-m</ta>
            <ta e="T849" id="Seg_6170" s="T848">dʼom-mu-n</ta>
            <ta e="T850" id="Seg_6171" s="T849">töröː-büt</ta>
            <ta e="T851" id="Seg_6172" s="T850">ü͡öskeː-bit</ta>
            <ta e="T852" id="Seg_6173" s="T851">sir-bi-n</ta>
            <ta e="T853" id="Seg_6174" s="T852">kömüsk-üː</ta>
            <ta e="T854" id="Seg_6175" s="T853">hɨldʼ-ɨ-bɨt-ɨ-m</ta>
            <ta e="T855" id="Seg_6176" s="T854">mini͡ene</ta>
            <ta e="T856" id="Seg_6177" s="T855">kaːn-ɨ-m</ta>
            <ta e="T857" id="Seg_6178" s="T856">kölöhün-ü-m</ta>
            <ta e="T858" id="Seg_6179" s="T857">iti</ta>
            <ta e="T859" id="Seg_6180" s="T858">seriː-ge</ta>
            <ta e="T860" id="Seg_6181" s="T859">köŋül</ta>
            <ta e="T861" id="Seg_6182" s="T860">tok-t-u-batag-a</ta>
            <ta e="T862" id="Seg_6183" s="T861">bihi͡ene</ta>
            <ta e="T863" id="Seg_6184" s="T862">kammunʼistʼičʼeskaj</ta>
            <ta e="T864" id="Seg_6185" s="T863">partʼija-bɨt</ta>
            <ta e="T865" id="Seg_6186" s="T864">kiːn</ta>
            <ta e="T866" id="Seg_6187" s="T865">kаmitʼet-a</ta>
            <ta e="T867" id="Seg_6188" s="T866">onton</ta>
            <ta e="T868" id="Seg_6189" s="T867">bihi͡ene</ta>
            <ta e="T869" id="Seg_6190" s="T868">gasudarstva-bɨtɨ-n</ta>
            <ta e="T870" id="Seg_6191" s="T869">olog-u-n</ta>
            <ta e="T871" id="Seg_6192" s="T870">könnör-öːččü</ta>
            <ta e="T872" id="Seg_6193" s="T871">pravʼitʼelʼstva-bɨt</ta>
            <ta e="T873" id="Seg_6194" s="T872">bi͡er-bit-tere</ta>
            <ta e="T874" id="Seg_6195" s="T873">mini͡e-ke</ta>
            <ta e="T875" id="Seg_6196" s="T874">gasudarstva</ta>
            <ta e="T876" id="Seg_6197" s="T875">ürdük</ta>
            <ta e="T877" id="Seg_6198" s="T876">seriː-ti-n</ta>
            <ta e="T878" id="Seg_6199" s="T877">ordʼen-ɨ-n</ta>
            <ta e="T882" id="Seg_6200" s="T881">di͡e-n</ta>
            <ta e="T883" id="Seg_6201" s="T882">onton</ta>
            <ta e="T884" id="Seg_6202" s="T883">össü͡ö</ta>
            <ta e="T885" id="Seg_6203" s="T884">mʼedalʼ-i</ta>
            <ta e="T890" id="Seg_6204" s="T889">di͡e-n</ta>
            <ta e="T891" id="Seg_6205" s="T890">anɨ</ta>
            <ta e="T892" id="Seg_6206" s="T891">bihigi</ta>
            <ta e="T893" id="Seg_6207" s="T892">savʼetskaj</ta>
            <ta e="T894" id="Seg_6208" s="T893">olog-u-r</ta>
            <ta e="T895" id="Seg_6209" s="T894">olor-or</ta>
            <ta e="T896" id="Seg_6210" s="T895">kihi-ler</ta>
            <ta e="T897" id="Seg_6211" s="T896">olok-kut</ta>
            <ta e="T898" id="Seg_6212" s="T897">berke</ta>
            <ta e="T899" id="Seg_6213" s="T898">tuks-an</ta>
            <ta e="T900" id="Seg_6214" s="T899">tur-ar</ta>
            <ta e="T901" id="Seg_6215" s="T900">inni</ta>
            <ta e="T902" id="Seg_6216" s="T901">di͡ek</ta>
            <ta e="T903" id="Seg_6217" s="T902">bar-ar</ta>
            <ta e="T904" id="Seg_6218" s="T903">küːs-ke</ta>
            <ta e="T905" id="Seg_6219" s="T904">ol</ta>
            <ta e="T906" id="Seg_6220" s="T905">ihin</ta>
            <ta e="T907" id="Seg_6221" s="T906">kim</ta>
            <ta e="T908" id="Seg_6222" s="T907">daːganɨ</ta>
            <ta e="T909" id="Seg_6223" s="T908">anɨ</ta>
            <ta e="T910" id="Seg_6224" s="T909">seriː-gi-n</ta>
            <ta e="T911" id="Seg_6225" s="T910">bu͡ol-u͡og-u-n</ta>
            <ta e="T912" id="Seg_6226" s="T911">bagar-bat</ta>
            <ta e="T913" id="Seg_6227" s="T912">bihigi</ta>
            <ta e="T914" id="Seg_6228" s="T913">sir-bit</ta>
            <ta e="T915" id="Seg_6229" s="T914">kihi-te</ta>
            <ta e="T916" id="Seg_6230" s="T915">urukku-ttan</ta>
            <ta e="T917" id="Seg_6231" s="T916">daːganɨ</ta>
            <ta e="T918" id="Seg_6232" s="T917">seriː-ge</ta>
            <ta e="T919" id="Seg_6233" s="T918">bagar-aːččɨ-ta</ta>
            <ta e="T920" id="Seg_6234" s="T919">hu͡ok</ta>
            <ta e="T921" id="Seg_6235" s="T920">e-t-e</ta>
            <ta e="T922" id="Seg_6236" s="T921">bihigi</ta>
            <ta e="T923" id="Seg_6237" s="T922">dʼom-mut</ta>
            <ta e="T924" id="Seg_6238" s="T923">anɨ</ta>
            <ta e="T925" id="Seg_6239" s="T924">beje-ti-n</ta>
            <ta e="T926" id="Seg_6240" s="T925">sanaː-ta</ta>
            <ta e="T927" id="Seg_6241" s="T926">söbülüː-r-ü-n</ta>
            <ta e="T928" id="Seg_6242" s="T927">sɨrdɨk</ta>
            <ta e="T929" id="Seg_6243" s="T928">dʼolloːk</ta>
            <ta e="T930" id="Seg_6244" s="T929">Lʼenʼin</ta>
            <ta e="T931" id="Seg_6245" s="T930">ɨj-bɨt</ta>
            <ta e="T932" id="Seg_6246" s="T931">kommunʼist</ta>
            <ta e="T933" id="Seg_6247" s="T932">aːt-taːk</ta>
            <ta e="T934" id="Seg_6248" s="T933">olog-u-n</ta>
            <ta e="T935" id="Seg_6249" s="T934">oŋost-or</ta>
            <ta e="T936" id="Seg_6250" s="T935">bihigi</ta>
            <ta e="T937" id="Seg_6251" s="T936">barɨ-bɨt</ta>
            <ta e="T938" id="Seg_6252" s="T937">d-iː-bit</ta>
            <ta e="T939" id="Seg_6253" s="T938">anɨ</ta>
            <ta e="T940" id="Seg_6254" s="T939">seriː</ta>
            <ta e="T941" id="Seg_6255" s="T940">su͡ok</ta>
            <ta e="T942" id="Seg_6256" s="T941">bu͡ol-lun</ta>
            <ta e="T943" id="Seg_6257" s="T942">sir</ta>
            <ta e="T944" id="Seg_6258" s="T943">ürd-ü-nen</ta>
            <ta e="T945" id="Seg_6259" s="T944">baːr</ta>
            <ta e="T946" id="Seg_6260" s="T945">bu͡ol-lun</ta>
            <ta e="T947" id="Seg_6261" s="T946">sɨrdɨk</ta>
            <ta e="T948" id="Seg_6262" s="T947">kün-neːk</ta>
            <ta e="T949" id="Seg_6263" s="T948">olok</ta>
            <ta e="T950" id="Seg_6264" s="T949">barɨ</ta>
            <ta e="T951" id="Seg_6265" s="T950">joŋ-ŋo</ta>
         </annotation>
         <annotation name="mp" tierref="mp">
            <ta e="T1" id="Seg_6266" s="T0">min</ta>
            <ta e="T2" id="Seg_6267" s="T1">heriː-GA</ta>
            <ta e="T3" id="Seg_6268" s="T2">bar-BIT-I-m</ta>
            <ta e="T4" id="Seg_6269" s="T3">beje-m</ta>
            <ta e="T5" id="Seg_6270" s="T4">hanaː-BI-nAn</ta>
            <ta e="T6" id="Seg_6271" s="T5">tɨːhɨčča</ta>
            <ta e="T7" id="Seg_6272" s="T6">togus</ta>
            <ta e="T8" id="Seg_6273" s="T7">hüːs</ta>
            <ta e="T9" id="Seg_6274" s="T8">tü͡ört-u͡on</ta>
            <ta e="T10" id="Seg_6275" s="T9">biːr</ta>
            <ta e="T11" id="Seg_6276" s="T10">dʼɨl-LAːK-GA</ta>
            <ta e="T12" id="Seg_6277" s="T11">kühüŋŋü</ta>
            <ta e="T13" id="Seg_6278" s="T12">sʼentʼabrʼ</ta>
            <ta e="T14" id="Seg_6279" s="T13">tɨmnɨː</ta>
            <ta e="T15" id="Seg_6280" s="T14">tüs-Ar</ta>
            <ta e="T16" id="Seg_6281" s="T15">ɨj-tI-GAr</ta>
            <ta e="T17" id="Seg_6282" s="T16">maŋnaj</ta>
            <ta e="T18" id="Seg_6283" s="T17">bar-An-BIn</ta>
            <ta e="T19" id="Seg_6284" s="T18">biːr</ta>
            <ta e="T20" id="Seg_6285" s="T19">dʼɨl-nI</ta>
            <ta e="T21" id="Seg_6286" s="T20">kɨtta</ta>
            <ta e="T22" id="Seg_6287" s="T21">togus</ta>
            <ta e="T23" id="Seg_6288" s="T22">ɨj-nI</ta>
            <ta e="T24" id="Seg_6289" s="T23">heriː</ta>
            <ta e="T25" id="Seg_6290" s="T24">ü͡örek-tI-GAr</ta>
            <ta e="T26" id="Seg_6291" s="T25">ü͡ören-A</ta>
            <ta e="T27" id="Seg_6292" s="T26">hɨrɨt-I-BIT-I-m</ta>
            <ta e="T29" id="Seg_6293" s="T27">Dalʼnʼij Vastok</ta>
            <ta e="T30" id="Seg_6294" s="T29">di͡e-An</ta>
            <ta e="T31" id="Seg_6295" s="T30">hir-GA</ta>
            <ta e="T32" id="Seg_6296" s="T31">onton</ta>
            <ta e="T33" id="Seg_6297" s="T32">hajɨn</ta>
            <ta e="T34" id="Seg_6298" s="T33">tɨːhɨčča</ta>
            <ta e="T35" id="Seg_6299" s="T34">togus</ta>
            <ta e="T36" id="Seg_6300" s="T35">hüːs</ta>
            <ta e="T37" id="Seg_6301" s="T36">tü͡ört-u͡on</ta>
            <ta e="T38" id="Seg_6302" s="T37">üs</ta>
            <ta e="T39" id="Seg_6303" s="T38">dʼɨl-LAːK-GA</ta>
            <ta e="T40" id="Seg_6304" s="T39">bihigi-nI</ta>
            <ta e="T41" id="Seg_6305" s="T40">ɨːt-BIT-LArA</ta>
            <ta e="T42" id="Seg_6306" s="T41">haːpat</ta>
            <ta e="T43" id="Seg_6307" s="T42">dek</ta>
            <ta e="T44" id="Seg_6308" s="T43">heriː</ta>
            <ta e="T45" id="Seg_6309" s="T44">kersiː-tA</ta>
            <ta e="T46" id="Seg_6310" s="T45">bar-A</ta>
            <ta e="T47" id="Seg_6311" s="T46">tur-Ar</ta>
            <ta e="T48" id="Seg_6312" s="T47">hir-tI-GAr</ta>
            <ta e="T49" id="Seg_6313" s="T48">bihigi</ta>
            <ta e="T50" id="Seg_6314" s="T49">heriː-nI</ta>
            <ta e="T51" id="Seg_6315" s="T50">körüs-I-BIT-BIt</ta>
            <ta e="T52" id="Seg_6316" s="T51">Proxorovka</ta>
            <ta e="T53" id="Seg_6317" s="T52">di͡e-An</ta>
            <ta e="T54" id="Seg_6318" s="T53">aːt-LAːK</ta>
            <ta e="T55" id="Seg_6319" s="T54">deri͡ebine-GA</ta>
            <ta e="T56" id="Seg_6320" s="T55">Kurskaj</ta>
            <ta e="T57" id="Seg_6321" s="T56">oblast-tI-GAr</ta>
            <ta e="T58" id="Seg_6322" s="T57">onno</ta>
            <ta e="T59" id="Seg_6323" s="T58">nʼemʼec</ta>
            <ta e="T60" id="Seg_6324" s="T59">heriː-tI-n</ta>
            <ta e="T61" id="Seg_6325" s="T60">kihi-tA</ta>
            <ta e="T62" id="Seg_6326" s="T61">bert</ta>
            <ta e="T63" id="Seg_6327" s="T62">ügüs</ta>
            <ta e="T64" id="Seg_6328" s="T63">heriː</ta>
            <ta e="T65" id="Seg_6329" s="T64">haː-tI-n</ta>
            <ta e="T66" id="Seg_6330" s="T65">eŋin</ta>
            <ta e="T67" id="Seg_6331" s="T66">atɨn</ta>
            <ta e="T68" id="Seg_6332" s="T67">terilte-tI-n</ta>
            <ta e="T69" id="Seg_6333" s="T68">mus-An</ta>
            <ta e="T70" id="Seg_6334" s="T69">baran</ta>
            <ta e="T71" id="Seg_6335" s="T70">bihigi</ta>
            <ta e="T72" id="Seg_6336" s="T71">armʼija-BItI-n</ta>
            <ta e="T73" id="Seg_6337" s="T72">berke</ta>
            <ta e="T74" id="Seg_6338" s="T73">töttörü</ta>
            <ta e="T75" id="Seg_6339" s="T74">dek</ta>
            <ta e="T76" id="Seg_6340" s="T75">otut</ta>
            <ta e="T77" id="Seg_6341" s="T76">bi͡es</ta>
            <ta e="T78" id="Seg_6342" s="T77">biːrste-LAːK</ta>
            <ta e="T79" id="Seg_6343" s="T78">hir-GA</ta>
            <ta e="T80" id="Seg_6344" s="T79">ɨgaj-BIT</ta>
            <ta e="T81" id="Seg_6345" s="T80">e-TI-tA</ta>
            <ta e="T82" id="Seg_6346" s="T81">onton</ta>
            <ta e="T83" id="Seg_6347" s="T82">kojut</ta>
            <ta e="T84" id="Seg_6348" s="T83">iti</ta>
            <ta e="T85" id="Seg_6349" s="T84">hir-nI</ta>
            <ta e="T86" id="Seg_6350" s="T85">Kurskaj</ta>
            <ta e="T87" id="Seg_6351" s="T86">duga</ta>
            <ta e="T88" id="Seg_6352" s="T87">di͡e-An</ta>
            <ta e="T89" id="Seg_6353" s="T88">aːt-LAː-BIT-LArA</ta>
            <ta e="T90" id="Seg_6354" s="T89">haka-LIː</ta>
            <ta e="T91" id="Seg_6355" s="T90">haŋar-TAK-GA</ta>
            <ta e="T92" id="Seg_6356" s="T91">Kurskaj</ta>
            <ta e="T93" id="Seg_6357" s="T92">tokur-tA</ta>
            <ta e="T94" id="Seg_6358" s="T93">di͡e-IAK-GA</ta>
            <ta e="T95" id="Seg_6359" s="T94">höp</ta>
            <ta e="T96" id="Seg_6360" s="T95">bu͡ol-IAK-tA</ta>
            <ta e="T97" id="Seg_6361" s="T96">nʼemʼec</ta>
            <ta e="T98" id="Seg_6362" s="T97">armʼija-tI-n</ta>
            <ta e="T99" id="Seg_6363" s="T98">tojon-tI-n</ta>
            <ta e="T100" id="Seg_6364" s="T99">hanaː-tA</ta>
            <ta e="T101" id="Seg_6365" s="T100">baːr</ta>
            <ta e="T102" id="Seg_6366" s="T101">e-BIT</ta>
            <ta e="T103" id="Seg_6367" s="T102">iti</ta>
            <ta e="T104" id="Seg_6368" s="T103">hir-tI-nAn</ta>
            <ta e="T105" id="Seg_6369" s="T104">bɨha</ta>
            <ta e="T106" id="Seg_6370" s="T105">mökküj-An</ta>
            <ta e="T107" id="Seg_6371" s="T106">baran</ta>
            <ta e="T108" id="Seg_6372" s="T107">bert</ta>
            <ta e="T109" id="Seg_6373" s="T108">türgen-LIk</ta>
            <ta e="T110" id="Seg_6374" s="T109">bihigi</ta>
            <ta e="T111" id="Seg_6375" s="T110">kiːn</ta>
            <ta e="T112" id="Seg_6376" s="T111">hir-BItI-n</ta>
            <ta e="T113" id="Seg_6377" s="T112">Maskva</ta>
            <ta e="T114" id="Seg_6378" s="T113">di͡e-An</ta>
            <ta e="T115" id="Seg_6379" s="T114">gu͡orat-nI</ta>
            <ta e="T116" id="Seg_6380" s="T115">ɨl-Ar</ta>
            <ta e="T117" id="Seg_6381" s="T116">kördük</ta>
            <ta e="T118" id="Seg_6382" s="T117">bihi͡ene</ta>
            <ta e="T119" id="Seg_6383" s="T118">čaːst-BIt</ta>
            <ta e="T120" id="Seg_6384" s="T119">Kurskaj</ta>
            <ta e="T121" id="Seg_6385" s="T120">tokur-tI-GAr</ta>
            <ta e="T122" id="Seg_6386" s="T121">tu͡ok-ttAn</ta>
            <ta e="T123" id="Seg_6387" s="T122">da</ta>
            <ta e="T124" id="Seg_6388" s="T123">ulakan</ta>
            <ta e="T125" id="Seg_6389" s="T124">heriː</ta>
            <ta e="T126" id="Seg_6390" s="T125">u͡ot-tI-GAr</ta>
            <ta e="T127" id="Seg_6391" s="T126">kiːr-An</ta>
            <ta e="T128" id="Seg_6392" s="T127">baran</ta>
            <ta e="T129" id="Seg_6393" s="T128">u͡on</ta>
            <ta e="T130" id="Seg_6394" s="T129">kün</ta>
            <ta e="T131" id="Seg_6395" s="T130">turkarɨ</ta>
            <ta e="T132" id="Seg_6396" s="T131">nʼemʼec</ta>
            <ta e="T133" id="Seg_6397" s="T132">armʼija-tI-n</ta>
            <ta e="T134" id="Seg_6398" s="T133">bat-A</ta>
            <ta e="T135" id="Seg_6399" s="T134">hataː-BIT-tA</ta>
            <ta e="T136" id="Seg_6400" s="T135">iti</ta>
            <ta e="T137" id="Seg_6401" s="T136">turkarɨ</ta>
            <ta e="T138" id="Seg_6402" s="T137">bihigi</ta>
            <ta e="T139" id="Seg_6403" s="T138">heriːles-BIT-BIt</ta>
            <ta e="T140" id="Seg_6404" s="T139">kün-nI</ta>
            <ta e="T141" id="Seg_6405" s="T140">tüːn-nI</ta>
            <ta e="T142" id="Seg_6406" s="T141">bil-BAkkA</ta>
            <ta e="T143" id="Seg_6407" s="T142">utuj-BAkkA</ta>
            <ta e="T144" id="Seg_6408" s="T143">da</ta>
            <ta e="T145" id="Seg_6409" s="T144">hɨnnʼan-BAkkA</ta>
            <ta e="T146" id="Seg_6410" s="T145">da</ta>
            <ta e="T147" id="Seg_6411" s="T146">kolobur-tA</ta>
            <ta e="T148" id="Seg_6412" s="T147">tu͡ok-nI</ta>
            <ta e="T149" id="Seg_6413" s="T148">da</ta>
            <ta e="T150" id="Seg_6414" s="T149">bil-BAT</ta>
            <ta e="T151" id="Seg_6415" s="T150">ihit-I-BAT</ta>
            <ta e="T152" id="Seg_6416" s="T151">kördük</ta>
            <ta e="T153" id="Seg_6417" s="T152">ikki</ta>
            <ta e="T154" id="Seg_6418" s="T153">di͡egi</ta>
            <ta e="T155" id="Seg_6419" s="T154">heriːles-AːččI</ta>
            <ta e="T156" id="Seg_6420" s="T155">öttü-ttAn</ta>
            <ta e="T157" id="Seg_6421" s="T156">töhölöːk</ta>
            <ta e="T158" id="Seg_6422" s="T157">tɨːhɨčča</ta>
            <ta e="T159" id="Seg_6423" s="T158">kihi</ta>
            <ta e="T160" id="Seg_6424" s="T159">hüt-BIT-tI-n</ta>
            <ta e="T161" id="Seg_6425" s="T160">öl-BIT-tI-n</ta>
            <ta e="T162" id="Seg_6426" s="T161">töhölöːk</ta>
            <ta e="T163" id="Seg_6427" s="T162">kihi</ta>
            <ta e="T164" id="Seg_6428" s="T163">gojobuːn</ta>
            <ta e="T165" id="Seg_6429" s="T164">bu͡ol-BIT-tI-n</ta>
            <ta e="T166" id="Seg_6430" s="T165">töhölöːk</ta>
            <ta e="T167" id="Seg_6431" s="T166">heriː</ta>
            <ta e="T168" id="Seg_6432" s="T167">haː-tA</ta>
            <ta e="T169" id="Seg_6433" s="T168">samalʼot-LAr</ta>
            <ta e="T170" id="Seg_6434" s="T169">tanka-LAr</ta>
            <ta e="T171" id="Seg_6435" s="T170">arudʼija-LAr</ta>
            <ta e="T172" id="Seg_6436" s="T171">onton</ta>
            <ta e="T173" id="Seg_6437" s="T172">kɨra</ta>
            <ta e="T174" id="Seg_6438" s="T173">eŋin</ta>
            <ta e="T175" id="Seg_6439" s="T174">haː-LAr</ta>
            <ta e="T176" id="Seg_6440" s="T175">heriː</ta>
            <ta e="T177" id="Seg_6441" s="T176">terilte-tA</ta>
            <ta e="T178" id="Seg_6442" s="T177">aldʼat-BIT-tI-n</ta>
            <ta e="T179" id="Seg_6443" s="T178">kim</ta>
            <ta e="T180" id="Seg_6444" s="T179">daːganɨ</ta>
            <ta e="T181" id="Seg_6445" s="T180">čakčɨ</ta>
            <ta e="T182" id="Seg_6446" s="T181">bil-BAT</ta>
            <ta e="T183" id="Seg_6447" s="T182">e-TI-tA</ta>
            <ta e="T184" id="Seg_6448" s="T183">oččogo</ta>
            <ta e="T185" id="Seg_6449" s="T184">kajdak</ta>
            <ta e="T186" id="Seg_6450" s="T185">ulakan</ta>
            <ta e="T187" id="Seg_6451" s="T186">küːsteːk</ta>
            <ta e="T188" id="Seg_6452" s="T187">heriː</ta>
            <ta e="T189" id="Seg_6453" s="T188">bar-BIT-tI-n</ta>
            <ta e="T190" id="Seg_6454" s="T189">iti</ta>
            <ta e="T191" id="Seg_6455" s="T190">hir-GA</ta>
            <ta e="T192" id="Seg_6456" s="T191">kihi</ta>
            <ta e="T193" id="Seg_6457" s="T192">tu͡olkulaː-IAK-tA</ta>
            <ta e="T194" id="Seg_6458" s="T193">biːr</ta>
            <ta e="T195" id="Seg_6459" s="T194">mannɨk</ta>
            <ta e="T196" id="Seg_6460" s="T195">kolobur-ttAn</ta>
            <ta e="T197" id="Seg_6461" s="T196">biːr</ta>
            <ta e="T198" id="Seg_6462" s="T197">buːs-LAːK</ta>
            <ta e="T199" id="Seg_6463" s="T198">hir-kAːN-GA</ta>
            <ta e="T200" id="Seg_6464" s="T199">bihigi</ta>
            <ta e="T201" id="Seg_6465" s="T200">baːr</ta>
            <ta e="T202" id="Seg_6466" s="T201">hir-BItI-GAr</ta>
            <ta e="T203" id="Seg_6467" s="T202">kün-GA</ta>
            <ta e="T204" id="Seg_6468" s="T203">biːrde</ta>
            <ta e="T205" id="Seg_6469" s="T204">köt-An</ta>
            <ta e="T206" id="Seg_6470" s="T205">kel-AːččI</ta>
            <ta e="T207" id="Seg_6471" s="T206">e-TI-tA</ta>
            <ta e="T208" id="Seg_6472" s="T207">nʼemʼec</ta>
            <ta e="T209" id="Seg_6473" s="T208">di͡ekki</ta>
            <ta e="T210" id="Seg_6474" s="T209">öttü-ttAn</ta>
            <ta e="T211" id="Seg_6475" s="T210">ikki</ta>
            <ta e="T212" id="Seg_6476" s="T211">hüːs</ta>
            <ta e="T213" id="Seg_6477" s="T212">kuraŋ-tA</ta>
            <ta e="T214" id="Seg_6478" s="T213">samalʼot-LAr</ta>
            <ta e="T215" id="Seg_6479" s="T214">ol-tI-ŋ</ta>
            <ta e="T216" id="Seg_6480" s="T215">barɨ-tA</ta>
            <ta e="T217" id="Seg_6481" s="T216">bihigi</ta>
            <ta e="T218" id="Seg_6482" s="T217">ürüt-BItI-nAn</ta>
            <ta e="T219" id="Seg_6483" s="T218">hamɨːr</ta>
            <ta e="T220" id="Seg_6484" s="T219">kördük</ta>
            <ta e="T221" id="Seg_6485" s="T220">tüs-An</ta>
            <ta e="T222" id="Seg_6486" s="T221">e</ta>
            <ta e="T223" id="Seg_6487" s="T222">hüːs</ta>
            <ta e="T224" id="Seg_6488" s="T223">bomba-LAr-nI</ta>
            <ta e="T225" id="Seg_6489" s="T224">keːs-AːččI</ta>
            <ta e="T226" id="Seg_6490" s="T225">e-TI-tA</ta>
            <ta e="T227" id="Seg_6491" s="T226">onton</ta>
            <ta e="T228" id="Seg_6492" s="T227">tuspa</ta>
            <ta e="T229" id="Seg_6493" s="T228">össü͡ö</ta>
            <ta e="T230" id="Seg_6494" s="T229">ɨt-IAlAː-Ar</ta>
            <ta e="T231" id="Seg_6495" s="T230">e-TI-tA</ta>
            <ta e="T232" id="Seg_6496" s="T231">töhö</ta>
            <ta e="T233" id="Seg_6497" s="T232">eme</ta>
            <ta e="T234" id="Seg_6498" s="T233">sarpa</ta>
            <ta e="T235" id="Seg_6499" s="T234">kördük</ta>
            <ta e="T236" id="Seg_6500" s="T235">iːt-Iː-LAːK</ta>
            <ta e="T237" id="Seg_6501" s="T236">heriː</ta>
            <ta e="T238" id="Seg_6502" s="T237">haː-tI-nAn</ta>
            <ta e="T239" id="Seg_6503" s="T238">pulemʼot-I-nAn</ta>
            <ta e="T240" id="Seg_6504" s="T239">itičče-LAːK</ta>
            <ta e="T241" id="Seg_6505" s="T240">bomba</ta>
            <ta e="T242" id="Seg_6506" s="T241">tüs-BIT-tI-n</ta>
            <ta e="T243" id="Seg_6507" s="T242">genne</ta>
            <ta e="T244" id="Seg_6508" s="T243">itičče-LAːK</ta>
            <ta e="T245" id="Seg_6509" s="T244">haː</ta>
            <ta e="T246" id="Seg_6510" s="T245">ɨt-IAlAː-BIT-tI-n</ta>
            <ta e="T247" id="Seg_6511" s="T246">genne</ta>
            <ta e="T248" id="Seg_6512" s="T247">hir</ta>
            <ta e="T249" id="Seg_6513" s="T248">barɨta</ta>
            <ta e="T250" id="Seg_6514" s="T249">tu͡ok-kAːN</ta>
            <ta e="T251" id="Seg_6515" s="T250">da</ta>
            <ta e="T252" id="Seg_6516" s="T251">köhün-I-BAT</ta>
            <ta e="T253" id="Seg_6517" s="T252">kördük</ta>
            <ta e="T254" id="Seg_6518" s="T253">kara</ta>
            <ta e="T255" id="Seg_6519" s="T254">tuman</ta>
            <ta e="T256" id="Seg_6520" s="T255">bu͡ol-Ar</ta>
            <ta e="T257" id="Seg_6521" s="T256">e-TI-tA</ta>
            <ta e="T258" id="Seg_6522" s="T257">iti</ta>
            <ta e="T259" id="Seg_6523" s="T258">tuman-GA</ta>
            <ta e="T260" id="Seg_6524" s="T259">kihi</ta>
            <ta e="T261" id="Seg_6525" s="T260">attɨ-GAr</ta>
            <ta e="T262" id="Seg_6526" s="T261">hɨt-Ar</ta>
            <ta e="T263" id="Seg_6527" s="T262">kihi-tI-n</ta>
            <ta e="T264" id="Seg_6528" s="T263">kot-An</ta>
            <ta e="T265" id="Seg_6529" s="T264">kör-BAT</ta>
            <ta e="T266" id="Seg_6530" s="T265">e-TI-tA</ta>
            <ta e="T267" id="Seg_6531" s="T266">horok</ta>
            <ta e="T268" id="Seg_6532" s="T267">genne</ta>
            <ta e="T269" id="Seg_6533" s="T268">kihi</ta>
            <ta e="T270" id="Seg_6534" s="T269">heriː</ta>
            <ta e="T271" id="Seg_6535" s="T270">haː-tI-n</ta>
            <ta e="T272" id="Seg_6536" s="T271">tɨ͡as-tI-ttAn</ta>
            <ta e="T273" id="Seg_6537" s="T272">tu͡ok-kAːN-nI</ta>
            <ta e="T274" id="Seg_6538" s="T273">da</ta>
            <ta e="T275" id="Seg_6539" s="T274">ihit-I-BAT</ta>
            <ta e="T276" id="Seg_6540" s="T275">kördük</ta>
            <ta e="T277" id="Seg_6541" s="T276">dülej</ta>
            <ta e="T278" id="Seg_6542" s="T277">bu͡ol-Ar</ta>
            <ta e="T279" id="Seg_6543" s="T278">e-TI-tA</ta>
            <ta e="T280" id="Seg_6544" s="T279">u͡on-Is</ta>
            <ta e="T281" id="Seg_6545" s="T280">kün-BItI-GAr</ta>
            <ta e="T282" id="Seg_6546" s="T281">bihigi</ta>
            <ta e="T283" id="Seg_6547" s="T282">di͡ekki</ta>
            <ta e="T284" id="Seg_6548" s="T283">öttü-nAn</ta>
            <ta e="T285" id="Seg_6549" s="T284">heriː-nI</ta>
            <ta e="T286" id="Seg_6550" s="T285">olus</ta>
            <ta e="T287" id="Seg_6551" s="T286">küːs-LAːK-LIk</ta>
            <ta e="T288" id="Seg_6552" s="T287">bar-TI-tA</ta>
            <ta e="T289" id="Seg_6553" s="T288">kallaːn-ttAn</ta>
            <ta e="T290" id="Seg_6554" s="T289">hir-ttAn</ta>
            <ta e="T291" id="Seg_6555" s="T290">bert</ta>
            <ta e="T292" id="Seg_6556" s="T291">elbek</ta>
            <ta e="T293" id="Seg_6557" s="T292">samalʼot-LAr</ta>
            <ta e="T294" id="Seg_6558" s="T293">tanka-LAr</ta>
            <ta e="T295" id="Seg_6559" s="T294">anaːn</ta>
            <ta e="T296" id="Seg_6560" s="T295">heriː-GA</ta>
            <ta e="T297" id="Seg_6561" s="T296">oŋohulun-I-BIT</ta>
            <ta e="T298" id="Seg_6562" s="T297">u͡ot-LAːK</ta>
            <ta e="T299" id="Seg_6563" s="T298">haː-LAr</ta>
            <ta e="T300" id="Seg_6564" s="T299">Katʼuša</ta>
            <ta e="T301" id="Seg_6565" s="T300">Andrʼuša</ta>
            <ta e="T302" id="Seg_6566" s="T301">di͡e-An</ta>
            <ta e="T303" id="Seg_6567" s="T302">aːt-LAːK-LAr</ta>
            <ta e="T304" id="Seg_6568" s="T303">onton</ta>
            <ta e="T305" id="Seg_6569" s="T304">tuspa</ta>
            <ta e="T306" id="Seg_6570" s="T305">töhö</ta>
            <ta e="T307" id="Seg_6571" s="T306">eme</ta>
            <ta e="T308" id="Seg_6572" s="T307">kɨra</ta>
            <ta e="T309" id="Seg_6573" s="T308">haː-LAr</ta>
            <ta e="T310" id="Seg_6574" s="T309">ol</ta>
            <ta e="T311" id="Seg_6575" s="T310">barɨ-tA</ta>
            <ta e="T312" id="Seg_6576" s="T311">biːrge</ta>
            <ta e="T313" id="Seg_6577" s="T312">ogus-TI-LAr</ta>
            <ta e="T314" id="Seg_6578" s="T313">nʼemʼec-LAr</ta>
            <ta e="T315" id="Seg_6579" s="T314">baːr</ta>
            <ta e="T316" id="Seg_6580" s="T315">hir-LArI-n</ta>
            <ta e="T317" id="Seg_6581" s="T316">nʼemʼec</ta>
            <ta e="T318" id="Seg_6582" s="T317">armʼija-tA</ta>
            <ta e="T319" id="Seg_6583" s="T318">iti-nI</ta>
            <ta e="T320" id="Seg_6584" s="T319">dolgun-LAːK</ta>
            <ta e="T321" id="Seg_6585" s="T320">ulakan</ta>
            <ta e="T322" id="Seg_6586" s="T321">ogus-Iː-nI</ta>
            <ta e="T323" id="Seg_6587" s="T322">tuluj-BAkkA</ta>
            <ta e="T324" id="Seg_6588" s="T323">töttörü</ta>
            <ta e="T325" id="Seg_6589" s="T324">hupturuj-BIT-tA</ta>
            <ta e="T326" id="Seg_6590" s="T325">honno-kAːN</ta>
            <ta e="T327" id="Seg_6591" s="T326">u͡on</ta>
            <ta e="T328" id="Seg_6592" s="T327">bi͡es</ta>
            <ta e="T329" id="Seg_6593" s="T328">biːrste-LAːK</ta>
            <ta e="T330" id="Seg_6594" s="T329">hir-nI</ta>
            <ta e="T331" id="Seg_6595" s="T330">töttörü</ta>
            <ta e="T332" id="Seg_6596" s="T331">ɨstan-BIT-tA</ta>
            <ta e="T333" id="Seg_6597" s="T332">iti</ta>
            <ta e="T334" id="Seg_6598" s="T333">ulakan</ta>
            <ta e="T335" id="Seg_6599" s="T334">ogus-Iː</ta>
            <ta e="T336" id="Seg_6600" s="T335">kelin-tI-ttAn</ta>
            <ta e="T337" id="Seg_6601" s="T336">Kurskaj</ta>
            <ta e="T338" id="Seg_6602" s="T337">di͡e-An</ta>
            <ta e="T339" id="Seg_6603" s="T338">tokur-tA</ta>
            <ta e="T340" id="Seg_6604" s="T339">berke</ta>
            <ta e="T341" id="Seg_6605" s="T340">kön-BIT-tA</ta>
            <ta e="T342" id="Seg_6606" s="T341">nʼemʼec</ta>
            <ta e="T343" id="Seg_6607" s="T342">armʼija-tI-n</ta>
            <ta e="T344" id="Seg_6608" s="T343">töttörü</ta>
            <ta e="T345" id="Seg_6609" s="T344">bihigi</ta>
            <ta e="T346" id="Seg_6610" s="T345">bat-BIT-BIt</ta>
            <ta e="T347" id="Seg_6611" s="T346">iti</ta>
            <ta e="T348" id="Seg_6612" s="T347">kelin-tI-ttAn</ta>
            <ta e="T349" id="Seg_6613" s="T348">nʼemʼec-LAr</ta>
            <ta e="T350" id="Seg_6614" s="T349">hereges</ta>
            <ta e="T351" id="Seg_6615" s="T350">haldaːt-LArA</ta>
            <ta e="T352" id="Seg_6616" s="T351">ilin-LArI-n</ta>
            <ta e="T353" id="Seg_6617" s="T352">bihigi-GA</ta>
            <ta e="T354" id="Seg_6618" s="T353">bi͡er-An</ta>
            <ta e="T355" id="Seg_6619" s="T354">baran</ta>
            <ta e="T356" id="Seg_6620" s="T355">huptu</ta>
            <ta e="T357" id="Seg_6621" s="T356">heriː</ta>
            <ta e="T358" id="Seg_6622" s="T357">büt-IAK-tI-GAr</ta>
            <ta e="T359" id="Seg_6623" s="T358">di͡eri</ta>
            <ta e="T360" id="Seg_6624" s="T359">öj-LArI-n-puːt-LArI-n</ta>
            <ta e="T361" id="Seg_6625" s="T360">bul-BAtAK-LArA</ta>
            <ta e="T362" id="Seg_6626" s="T361">ku͡ot-An</ta>
            <ta e="T363" id="Seg_6627" s="T362">is-BIT-LArA</ta>
            <ta e="T364" id="Seg_6628" s="T363">bihigi</ta>
            <ta e="T365" id="Seg_6629" s="T364">armʼija-BIt</ta>
            <ta e="T366" id="Seg_6630" s="T365">bert</ta>
            <ta e="T367" id="Seg_6631" s="T366">üčügej-LIk</ta>
            <ta e="T368" id="Seg_6632" s="T367">küːs-tI-n</ta>
            <ta e="T369" id="Seg_6633" s="T368">tut-An</ta>
            <ta e="T370" id="Seg_6634" s="T369">baran</ta>
            <ta e="T371" id="Seg_6635" s="T370">nʼemʼec-LAr-nI</ta>
            <ta e="T372" id="Seg_6636" s="T371">kelin</ta>
            <ta e="T373" id="Seg_6637" s="T372">dek</ta>
            <ta e="T374" id="Seg_6638" s="T373">köllör-BAkkA</ta>
            <ta e="T375" id="Seg_6639" s="T374">delbi</ta>
            <ta e="T376" id="Seg_6640" s="T375">bat-BIT-tA</ta>
            <ta e="T377" id="Seg_6641" s="T376">min</ta>
            <ta e="T378" id="Seg_6642" s="T377">kojut-GI</ta>
            <ta e="T379" id="Seg_6643" s="T378">kün-LAr-GA</ta>
            <ta e="T380" id="Seg_6644" s="T379">heriː-GA</ta>
            <ta e="T381" id="Seg_6645" s="T380">hɨrɨt-An-BIn</ta>
            <ta e="T382" id="Seg_6646" s="T381">itinnik</ta>
            <ta e="T383" id="Seg_6647" s="T382">hür-LAːK</ta>
            <ta e="T384" id="Seg_6648" s="T383">kihi</ta>
            <ta e="T385" id="Seg_6649" s="T952">kördük</ta>
            <ta e="T387" id="Seg_6650" s="T385">kör-BAtAK-I-m</ta>
            <ta e="T388" id="Seg_6651" s="T387">min</ta>
            <ta e="T389" id="Seg_6652" s="T388">iti</ta>
            <ta e="T390" id="Seg_6653" s="T389">hir-GA</ta>
            <ta e="T391" id="Seg_6654" s="T390">haŋardɨː</ta>
            <ta e="T392" id="Seg_6655" s="T391">kör-BIT-I-m</ta>
            <ta e="T393" id="Seg_6656" s="T392">hu͡os-LAːK</ta>
            <ta e="T394" id="Seg_6657" s="T393">kersiː-nI</ta>
            <ta e="T395" id="Seg_6658" s="T394">kajdak</ta>
            <ta e="T396" id="Seg_6659" s="T395">heriː</ta>
            <ta e="T397" id="Seg_6660" s="T396">kihi-tI-n</ta>
            <ta e="T398" id="Seg_6661" s="T397">kaːn-tA</ta>
            <ta e="T399" id="Seg_6662" s="T398">ürek-LIː</ta>
            <ta e="T400" id="Seg_6663" s="T399">tok-t-Ar-tI-n</ta>
            <ta e="T401" id="Seg_6664" s="T400">töhölöːk</ta>
            <ta e="T402" id="Seg_6665" s="T401">tɨːhɨčča</ta>
            <ta e="T403" id="Seg_6666" s="T402">kihi</ta>
            <ta e="T404" id="Seg_6667" s="T403">tɨːn-tA</ta>
            <ta e="T405" id="Seg_6668" s="T404">bɨhɨn-Ar-tI-n</ta>
            <ta e="T406" id="Seg_6669" s="T405">töhölöːk</ta>
            <ta e="T407" id="Seg_6670" s="T406">gojobuːn</ta>
            <ta e="T408" id="Seg_6671" s="T407">bu͡ol-BIT</ta>
            <ta e="T409" id="Seg_6672" s="T408">kihi</ta>
            <ta e="T410" id="Seg_6673" s="T409">ulakan-LIk</ta>
            <ta e="T411" id="Seg_6674" s="T410">erej-LAN-Ar-tI-n</ta>
            <ta e="T412" id="Seg_6675" s="T411">min</ta>
            <ta e="T413" id="Seg_6676" s="T412">emi͡e</ta>
            <ta e="T414" id="Seg_6677" s="T413">iti</ta>
            <ta e="T415" id="Seg_6678" s="T414">ogus-A-s-Iː-GA</ta>
            <ta e="T416" id="Seg_6679" s="T415">Kurskaj</ta>
            <ta e="T417" id="Seg_6680" s="T416">tumus-tI-GAr</ta>
            <ta e="T418" id="Seg_6681" s="T417">atdʼeleʼnʼija</ta>
            <ta e="T419" id="Seg_6682" s="T418">kamandʼir-tA</ta>
            <ta e="T420" id="Seg_6683" s="T419">bu͡ol-A</ta>
            <ta e="T421" id="Seg_6684" s="T420">hɨrɨt-An-BIn</ta>
            <ta e="T422" id="Seg_6685" s="T421">dʼogus-LIk</ta>
            <ta e="T423" id="Seg_6686" s="T422">kaŋas</ta>
            <ta e="T424" id="Seg_6687" s="T423">atak-BI-n</ta>
            <ta e="T425" id="Seg_6688" s="T424">bɨlčɨŋ-nI</ta>
            <ta e="T426" id="Seg_6689" s="T425">gojobuːn-LAː-A-t-BIT-I-m</ta>
            <ta e="T427" id="Seg_6690" s="T426">ol</ta>
            <ta e="T428" id="Seg_6691" s="T427">bu͡ol-An</ta>
            <ta e="T429" id="Seg_6692" s="T428">baran</ta>
            <ta e="T430" id="Seg_6693" s="T429">heriːles-Ar</ta>
            <ta e="T431" id="Seg_6694" s="T430">hir-ttAn</ta>
            <ta e="T432" id="Seg_6695" s="T431">tagɨs-I-BAtAK-I-m</ta>
            <ta e="T433" id="Seg_6696" s="T432">iti</ta>
            <ta e="T434" id="Seg_6697" s="T433">hir-LAːK</ta>
            <ta e="T435" id="Seg_6698" s="T434">ogus-A-s-Iː-GA</ta>
            <ta e="T436" id="Seg_6699" s="T435">min</ta>
            <ta e="T437" id="Seg_6700" s="T436">baːr</ta>
            <ta e="T438" id="Seg_6701" s="T437">agaj</ta>
            <ta e="T439" id="Seg_6702" s="T438">čaːst-I-m</ta>
            <ta e="T440" id="Seg_6703" s="T439">tɨːhɨčča-ttAn</ta>
            <ta e="T441" id="Seg_6704" s="T440">taksa</ta>
            <ta e="T442" id="Seg_6705" s="T441">heriː</ta>
            <ta e="T443" id="Seg_6706" s="T442">kihi-tI-ttAn</ta>
            <ta e="T444" id="Seg_6707" s="T443">iti</ta>
            <ta e="T445" id="Seg_6708" s="T444">u͡on</ta>
            <ta e="T446" id="Seg_6709" s="T445">kün-GA</ta>
            <ta e="T447" id="Seg_6710" s="T446">ogus-A-s-Iː-GA</ta>
            <ta e="T448" id="Seg_6711" s="T447">ort-BIT-tA</ta>
            <ta e="T449" id="Seg_6712" s="T448">u͡on</ta>
            <ta e="T450" id="Seg_6713" s="T449">biːr</ta>
            <ta e="T451" id="Seg_6714" s="T450">agaj</ta>
            <ta e="T452" id="Seg_6715" s="T451">kihi</ta>
            <ta e="T453" id="Seg_6716" s="T452">iti</ta>
            <ta e="T454" id="Seg_6717" s="T453">is-tI-GAr</ta>
            <ta e="T455" id="Seg_6718" s="T454">össü͡ö</ta>
            <ta e="T456" id="Seg_6719" s="T455">biːr</ta>
            <ta e="T457" id="Seg_6720" s="T456">min</ta>
            <ta e="T458" id="Seg_6721" s="T457">gojobuːn</ta>
            <ta e="T459" id="Seg_6722" s="T458">bu͡ol-BIT</ta>
            <ta e="T460" id="Seg_6723" s="T459">kihi</ta>
            <ta e="T461" id="Seg_6724" s="T460">baːr</ta>
            <ta e="T462" id="Seg_6725" s="T461">e-TI-m</ta>
            <ta e="T463" id="Seg_6726" s="T462">iti-ttAn</ta>
            <ta e="T464" id="Seg_6727" s="T463">höp</ta>
            <ta e="T465" id="Seg_6728" s="T464">bu͡ol-IAK-tA</ta>
            <ta e="T466" id="Seg_6729" s="T465">koloː-IAK-GA</ta>
            <ta e="T467" id="Seg_6730" s="T466">kajdak</ta>
            <ta e="T468" id="Seg_6731" s="T467">aldʼarkaj-LAːK</ta>
            <ta e="T469" id="Seg_6732" s="T468">heriː</ta>
            <ta e="T470" id="Seg_6733" s="T469">e-BIT-tI-n</ta>
            <ta e="T471" id="Seg_6734" s="T470">Kurskaj</ta>
            <ta e="T472" id="Seg_6735" s="T471">tokur-tI-GAr</ta>
            <ta e="T473" id="Seg_6736" s="T472">töhö</ta>
            <ta e="T474" id="Seg_6737" s="T473">kaːn</ta>
            <ta e="T475" id="Seg_6738" s="T474">tok-n-I-BIT-tI-n</ta>
            <ta e="T476" id="Seg_6739" s="T475">töhö</ta>
            <ta e="T477" id="Seg_6740" s="T476">kihi</ta>
            <ta e="T478" id="Seg_6741" s="T477">öl-BIT-tI-n</ta>
            <ta e="T479" id="Seg_6742" s="T478">Kurskaj-GA</ta>
            <ta e="T480" id="Seg_6743" s="T479">büt-An</ta>
            <ta e="T481" id="Seg_6744" s="T480">baran</ta>
            <ta e="T482" id="Seg_6745" s="T481">bihigi</ta>
            <ta e="T483" id="Seg_6746" s="T482">armʼija-BIt</ta>
            <ta e="T484" id="Seg_6747" s="T483">töttörü</ta>
            <ta e="T485" id="Seg_6748" s="T484">küreː-Ar</ta>
            <ta e="T486" id="Seg_6749" s="T485">nʼemʼec-nI</ta>
            <ta e="T487" id="Seg_6750" s="T486">haːpat</ta>
            <ta e="T488" id="Seg_6751" s="T487">dek</ta>
            <ta e="T489" id="Seg_6752" s="T488">bat-An</ta>
            <ta e="T490" id="Seg_6753" s="T489">bar-BIT-tA</ta>
            <ta e="T491" id="Seg_6754" s="T490">iti</ta>
            <ta e="T492" id="Seg_6755" s="T491">Kurskaj</ta>
            <ta e="T493" id="Seg_6756" s="T492">kelin-tI-ttAn</ta>
            <ta e="T494" id="Seg_6757" s="T493">min</ta>
            <ta e="T495" id="Seg_6758" s="T494">haːmaj</ta>
            <ta e="T496" id="Seg_6759" s="T495">heriː</ta>
            <ta e="T497" id="Seg_6760" s="T496">kersiː</ta>
            <ta e="T498" id="Seg_6761" s="T497">hir-tI-GAr</ta>
            <ta e="T499" id="Seg_6762" s="T498">baːr</ta>
            <ta e="T500" id="Seg_6763" s="T499">e-TI-m</ta>
            <ta e="T501" id="Seg_6764" s="T500">tɨːhɨčča</ta>
            <ta e="T502" id="Seg_6765" s="T501">togus</ta>
            <ta e="T503" id="Seg_6766" s="T502">hüːs</ta>
            <ta e="T504" id="Seg_6767" s="T503">tü͡ört-u͡on</ta>
            <ta e="T505" id="Seg_6768" s="T504">tü͡ört</ta>
            <ta e="T506" id="Seg_6769" s="T505">dʼɨl-LAːK-GA</ta>
            <ta e="T507" id="Seg_6770" s="T506">kɨhɨn-GI</ta>
            <ta e="T508" id="Seg_6771" s="T507">janvarʼ</ta>
            <ta e="T509" id="Seg_6772" s="T508">ɨj-GA</ta>
            <ta e="T510" id="Seg_6773" s="T509">di͡eri</ta>
            <ta e="T511" id="Seg_6774" s="T510">iti</ta>
            <ta e="T512" id="Seg_6775" s="T511">kem-GA</ta>
            <ta e="T513" id="Seg_6776" s="T512">nʼemʼec</ta>
            <ta e="T514" id="Seg_6777" s="T513">haldaːt-LArI-n</ta>
            <ta e="T515" id="Seg_6778" s="T514">kɨtta</ta>
            <ta e="T516" id="Seg_6779" s="T515">utarɨ</ta>
            <ta e="T517" id="Seg_6780" s="T516">tur-An</ta>
            <ta e="T518" id="Seg_6781" s="T517">heriːles-BIT-I-m</ta>
            <ta e="T519" id="Seg_6782" s="T518">mannɨk</ta>
            <ta e="T520" id="Seg_6783" s="T519">hir-LAr-GA</ta>
            <ta e="T521" id="Seg_6784" s="T520">Bʼelgarad</ta>
            <ta e="T522" id="Seg_6785" s="T521">onton</ta>
            <ta e="T523" id="Seg_6786" s="T522">Xarʼkav</ta>
            <ta e="T524" id="Seg_6787" s="T523">onton</ta>
            <ta e="T525" id="Seg_6788" s="T524">Poltava</ta>
            <ta e="T526" id="Seg_6789" s="T525">onton</ta>
            <ta e="T527" id="Seg_6790" s="T526">Krʼemʼenčʼug</ta>
            <ta e="T528" id="Seg_6791" s="T527">onton</ta>
            <ta e="T529" id="Seg_6792" s="T528">Krʼukov</ta>
            <ta e="T530" id="Seg_6793" s="T529">di͡e-An</ta>
            <ta e="T531" id="Seg_6794" s="T530">aːt-LAːK</ta>
            <ta e="T532" id="Seg_6795" s="T531">ulakan</ta>
            <ta e="T533" id="Seg_6796" s="T532">gu͡orat-LAr-nI</ta>
            <ta e="T534" id="Seg_6797" s="T533">ɨl-Ar-GA</ta>
            <ta e="T535" id="Seg_6798" s="T534">ukrainʼes</ta>
            <ta e="T536" id="Seg_6799" s="T535">di͡e-An</ta>
            <ta e="T537" id="Seg_6800" s="T536">omuk</ta>
            <ta e="T538" id="Seg_6801" s="T537">hir-tI-GAr</ta>
            <ta e="T539" id="Seg_6802" s="T538">iti</ta>
            <ta e="T540" id="Seg_6803" s="T539">gu͡orat-LAr-ttAn</ta>
            <ta e="T541" id="Seg_6804" s="T540">olus</ta>
            <ta e="T542" id="Seg_6805" s="T541">kɨtaːnak-LIk</ta>
            <ta e="T543" id="Seg_6806" s="T542">bihigi</ta>
            <ta e="T544" id="Seg_6807" s="T543">heriːles-BIT-BIt</ta>
            <ta e="T545" id="Seg_6808" s="T544">nʼemʼec-nI</ta>
            <ta e="T546" id="Seg_6809" s="T545">kɨtta</ta>
            <ta e="T547" id="Seg_6810" s="T546">Xarʼkav</ta>
            <ta e="T548" id="Seg_6811" s="T547">gu͡orat</ta>
            <ta e="T549" id="Seg_6812" s="T548">bɨldʼaː-A-s-Iː-GA</ta>
            <ta e="T550" id="Seg_6813" s="T549">iti</ta>
            <ta e="T551" id="Seg_6814" s="T550">gu͡orat</ta>
            <ta e="T552" id="Seg_6815" s="T551">üs-GA</ta>
            <ta e="T553" id="Seg_6816" s="T552">di͡eri</ta>
            <ta e="T554" id="Seg_6817" s="T553">töttörü taːrɨ</ta>
            <ta e="T555" id="Seg_6818" s="T554">iliː-ttAn</ta>
            <ta e="T556" id="Seg_6819" s="T555">iliː-GA</ta>
            <ta e="T557" id="Seg_6820" s="T556">kel-A</ta>
            <ta e="T558" id="Seg_6821" s="T557">hɨrɨt-I-BIT-tA</ta>
            <ta e="T559" id="Seg_6822" s="T558">onton</ta>
            <ta e="T560" id="Seg_6823" s="T559">üs-Is</ta>
            <ta e="T561" id="Seg_6824" s="T560">kel-Iː-tI-GAr</ta>
            <ta e="T562" id="Seg_6825" s="T561">bihigi</ta>
            <ta e="T563" id="Seg_6826" s="T562">iliː-BIt-ttAn</ta>
            <ta e="T564" id="Seg_6827" s="T563">ɨːt-BAtAK-BIt</ta>
            <ta e="T565" id="Seg_6828" s="T564">onton</ta>
            <ta e="T566" id="Seg_6829" s="T565">ulakan</ta>
            <ta e="T567" id="Seg_6830" s="T566">ogus-A-s-Iː</ta>
            <ta e="T568" id="Seg_6831" s="T567">össü͡ö</ta>
            <ta e="T569" id="Seg_6832" s="T568">baːr</ta>
            <ta e="T570" id="Seg_6833" s="T569">e-TI-tA</ta>
            <ta e="T571" id="Seg_6834" s="T570">Dnʼepr </ta>
            <ta e="T572" id="Seg_6835" s="T571">di͡e-An</ta>
            <ta e="T573" id="Seg_6836" s="T572">aːt-LAːK</ta>
            <ta e="T574" id="Seg_6837" s="T573">ebe</ta>
            <ta e="T575" id="Seg_6838" s="T574">kes-Iː-tI-GAr</ta>
            <ta e="T576" id="Seg_6839" s="T575">iti</ta>
            <ta e="T577" id="Seg_6840" s="T576">ebe</ta>
            <ta e="T578" id="Seg_6841" s="T577">onu͡or-GI</ta>
            <ta e="T579" id="Seg_6842" s="T578">öttü-tI-GAr</ta>
            <ta e="T580" id="Seg_6843" s="T579">olor-An</ta>
            <ta e="T581" id="Seg_6844" s="T580">nʼemʼec-LAr</ta>
            <ta e="T582" id="Seg_6845" s="T581">künüs-LAr-nI-tüːn-LAr-nI</ta>
            <ta e="T583" id="Seg_6846" s="T582">keteː-A</ta>
            <ta e="T584" id="Seg_6847" s="T583">hɨrɨt-An-LAr</ta>
            <ta e="T585" id="Seg_6848" s="T584">bihigi-nI</ta>
            <ta e="T586" id="Seg_6849" s="T585">amattan</ta>
            <ta e="T587" id="Seg_6850" s="T586">hataː-An</ta>
            <ta e="T588" id="Seg_6851" s="T587">kes-A-r.[t]-BAT</ta>
            <ta e="T589" id="Seg_6852" s="T588">e-TI-LArA</ta>
            <ta e="T590" id="Seg_6853" s="T589">ol</ta>
            <ta e="T591" id="Seg_6854" s="T590">ihin</ta>
            <ta e="T592" id="Seg_6855" s="T591">bihigi</ta>
            <ta e="T593" id="Seg_6856" s="T592">kas</ta>
            <ta e="T594" id="Seg_6857" s="T593">da</ta>
            <ta e="T595" id="Seg_6858" s="T594">kün-nI</ta>
            <ta e="T596" id="Seg_6859" s="T595">kot-An</ta>
            <ta e="T597" id="Seg_6860" s="T596">tagɨs-I-BAtAK-BIt</ta>
            <ta e="T598" id="Seg_6861" s="T597">betereː-GI</ta>
            <ta e="T599" id="Seg_6862" s="T598">kɨtɨl-GA</ta>
            <ta e="T600" id="Seg_6863" s="T599">olor-BIT-BIt</ta>
            <ta e="T601" id="Seg_6864" s="T600">ebe-nI</ta>
            <ta e="T602" id="Seg_6865" s="T601">tagɨs-Ar</ta>
            <ta e="T603" id="Seg_6866" s="T602">mostɨ-LAr-nI</ta>
            <ta e="T604" id="Seg_6867" s="T603">künüs</ta>
            <ta e="T605" id="Seg_6868" s="T604">oŋor-Ar-BItI-n</ta>
            <ta e="T606" id="Seg_6869" s="T605">nʼemʼec-LAr</ta>
            <ta e="T607" id="Seg_6870" s="T606">samalʼot-LArA</ta>
            <ta e="T608" id="Seg_6871" s="T607">köt-An</ta>
            <ta e="T609" id="Seg_6872" s="T608">kel-A-kel-A</ta>
            <ta e="T610" id="Seg_6873" s="T609">töhö</ta>
            <ta e="T611" id="Seg_6874" s="T610">eme</ta>
            <ta e="T612" id="Seg_6875" s="T611">bomba-LAr-nI</ta>
            <ta e="T613" id="Seg_6876" s="T612">bɨrak-AːktAː-An</ta>
            <ta e="T614" id="Seg_6877" s="T613">delbi</ta>
            <ta e="T615" id="Seg_6878" s="T614">aldʼat-Ar</ta>
            <ta e="T616" id="Seg_6879" s="T615">e-TI-tA</ta>
            <ta e="T617" id="Seg_6880" s="T616">ol</ta>
            <ta e="T618" id="Seg_6881" s="T617">ihin</ta>
            <ta e="T619" id="Seg_6882" s="T618">kojut</ta>
            <ta e="T620" id="Seg_6883" s="T619">bihigi</ta>
            <ta e="T621" id="Seg_6884" s="T620">tüːn</ta>
            <ta e="T622" id="Seg_6885" s="T621">kahu͡on</ta>
            <ta e="T623" id="Seg_6886" s="T622">hir-I-nAn</ta>
            <ta e="T624" id="Seg_6887" s="T623">mostɨ-LAr-nI</ta>
            <ta e="T625" id="Seg_6888" s="T624">oŋor-TAː-Ar</ta>
            <ta e="T626" id="Seg_6889" s="T625">e-TI-BIt</ta>
            <ta e="T627" id="Seg_6890" s="T626">abaːhɨ</ta>
            <ta e="T628" id="Seg_6891" s="T627">nʼemʼec</ta>
            <ta e="T629" id="Seg_6892" s="T628">tüːn</ta>
            <ta e="T630" id="Seg_6893" s="T629">da</ta>
            <ta e="T631" id="Seg_6894" s="T630">oŋor-BIT</ta>
            <ta e="T632" id="Seg_6895" s="T631">mostɨ-LAr-BItI-n</ta>
            <ta e="T633" id="Seg_6896" s="T632">tur-IAr-BAT</ta>
            <ta e="T634" id="Seg_6897" s="T633">e-TI-tA</ta>
            <ta e="T635" id="Seg_6898" s="T634">oččogo</ta>
            <ta e="T636" id="Seg_6899" s="T635">bihigi</ta>
            <ta e="T637" id="Seg_6900" s="T636">di͡egi-LAr</ta>
            <ta e="T638" id="Seg_6901" s="T637">delbi</ta>
            <ta e="T639" id="Seg_6902" s="T638">kɨjgan-An</ta>
            <ta e="T640" id="Seg_6903" s="T639">baran</ta>
            <ta e="T641" id="Seg_6904" s="T640">barɨ</ta>
            <ta e="T642" id="Seg_6905" s="T641">heriː</ta>
            <ta e="T643" id="Seg_6906" s="T642">haː-tI-nAn</ta>
            <ta e="T644" id="Seg_6907" s="T643">delbi</ta>
            <ta e="T645" id="Seg_6908" s="T644">ɨt-IAlAː-An</ta>
            <ta e="T646" id="Seg_6909" s="T645">baran</ta>
            <ta e="T647" id="Seg_6910" s="T646">töhö</ta>
            <ta e="T648" id="Seg_6911" s="T647">eme</ta>
            <ta e="T649" id="Seg_6912" s="T648">u͡on-LIː</ta>
            <ta e="T650" id="Seg_6913" s="T649">samalʼot-LAr-nI</ta>
            <ta e="T651" id="Seg_6914" s="T650">onu͡or-GI</ta>
            <ta e="T652" id="Seg_6915" s="T651">örüt-tI-GA</ta>
            <ta e="T653" id="Seg_6916" s="T652">ɨːt-An</ta>
            <ta e="T654" id="Seg_6917" s="T653">baran</ta>
            <ta e="T655" id="Seg_6918" s="T654">iti-nI</ta>
            <ta e="T656" id="Seg_6919" s="T655">barɨ-tI-n</ta>
            <ta e="T657" id="Seg_6920" s="T656">bürüj-n-An</ta>
            <ta e="T658" id="Seg_6921" s="T657">tur-An</ta>
            <ta e="T659" id="Seg_6922" s="T658">onu͡or-GI</ta>
            <ta e="T660" id="Seg_6923" s="T659">dek</ta>
            <ta e="T661" id="Seg_6924" s="T660">bɨs-A</ta>
            <ta e="T662" id="Seg_6925" s="T661">mökküj-s-A</ta>
            <ta e="T663" id="Seg_6926" s="T662">tagɨs-I-BIT-LArA</ta>
            <ta e="T664" id="Seg_6927" s="T663">bihigi</ta>
            <ta e="T665" id="Seg_6928" s="T664">haldaːt-LAr-BIt</ta>
            <ta e="T666" id="Seg_6929" s="T665">ol</ta>
            <ta e="T667" id="Seg_6930" s="T666">mökkü͡ön</ta>
            <ta e="T668" id="Seg_6931" s="T667">ajdaːn-tI-GAr</ta>
            <ta e="T669" id="Seg_6932" s="T668">mostɨ-GA</ta>
            <ta e="T670" id="Seg_6933" s="T669">bat-BAkkA</ta>
            <ta e="T671" id="Seg_6934" s="T670">aŋar</ta>
            <ta e="T672" id="Seg_6935" s="T671">kihi</ta>
            <ta e="T673" id="Seg_6936" s="T672">lu͡otka-nAn</ta>
            <ta e="T674" id="Seg_6937" s="T673">horok-LAr</ta>
            <ta e="T675" id="Seg_6938" s="T674">kas-LIː</ta>
            <ta e="T676" id="Seg_6939" s="T675">emi͡e</ta>
            <ta e="T677" id="Seg_6940" s="T676">brevnoː-LAr-nI</ta>
            <ta e="T678" id="Seg_6941" s="T677">biːrge</ta>
            <ta e="T679" id="Seg_6942" s="T678">kolboː-A</ta>
            <ta e="T680" id="Seg_6943" s="T679">baːj-An</ta>
            <ta e="T681" id="Seg_6944" s="T680">baran</ta>
            <ta e="T682" id="Seg_6945" s="T681">aŋar-LAr</ta>
            <ta e="T683" id="Seg_6946" s="T682">köŋül</ta>
            <ta e="T684" id="Seg_6947" s="T683">karbaː-An</ta>
            <ta e="T685" id="Seg_6948" s="T684">tagɨs-I-BIT-LArA</ta>
            <ta e="T686" id="Seg_6949" s="T685">Dnʼepr</ta>
            <ta e="T687" id="Seg_6950" s="T686">di͡e-An</ta>
            <ta e="T688" id="Seg_6951" s="T687">ebe-nI</ta>
            <ta e="T689" id="Seg_6952" s="T688">onno</ta>
            <ta e="T690" id="Seg_6953" s="T689">ajdaːn</ta>
            <ta e="T691" id="Seg_6954" s="T690">ulakan</ta>
            <ta e="T692" id="Seg_6955" s="T691">üːgü</ta>
            <ta e="T693" id="Seg_6956" s="T692">kahɨː</ta>
            <ta e="T694" id="Seg_6957" s="T693">kersiː</ta>
            <ta e="T695" id="Seg_6958" s="T694">tu͡ok-ttAn</ta>
            <ta e="T696" id="Seg_6959" s="T695">da</ta>
            <ta e="T697" id="Seg_6960" s="T696">atɨn</ta>
            <ta e="T698" id="Seg_6961" s="T697">heriː</ta>
            <ta e="T699" id="Seg_6962" s="T698">haː-tI-n</ta>
            <ta e="T700" id="Seg_6963" s="T699">tɨ͡as-tA</ta>
            <ta e="T701" id="Seg_6964" s="T700">hür-LAːK</ta>
            <ta e="T702" id="Seg_6965" s="T701">e-TI-tA</ta>
            <ta e="T703" id="Seg_6966" s="T702">nʼemʼec</ta>
            <ta e="T704" id="Seg_6967" s="T703">di͡ekki</ta>
            <ta e="T705" id="Seg_6968" s="T704">öttü</ta>
            <ta e="T706" id="Seg_6969" s="T705">iti</ta>
            <ta e="T707" id="Seg_6970" s="T706">bɨs-A</ta>
            <ta e="T708" id="Seg_6971" s="T707">mökküj-Iː-nI</ta>
            <ta e="T709" id="Seg_6972" s="T708">kot-An</ta>
            <ta e="T710" id="Seg_6973" s="T709">tut-BAkkA</ta>
            <ta e="T711" id="Seg_6974" s="T710">meːne</ta>
            <ta e="T712" id="Seg_6975" s="T711">ebe</ta>
            <ta e="T713" id="Seg_6976" s="T712">ürüt-tI-nAn</ta>
            <ta e="T714" id="Seg_6977" s="T713">ɨt-IAlAː-Ar</ta>
            <ta e="T715" id="Seg_6978" s="T714">e-TI-tA</ta>
            <ta e="T716" id="Seg_6979" s="T715">kɨtɨl-ttAn</ta>
            <ta e="T717" id="Seg_6980" s="T716">kör-TAK-GA</ta>
            <ta e="T718" id="Seg_6981" s="T717">ebe</ta>
            <ta e="T719" id="Seg_6982" s="T718">dek</ta>
            <ta e="T720" id="Seg_6983" s="T719">uː-GA</ta>
            <ta e="T721" id="Seg_6984" s="T720">bukatɨn</ta>
            <ta e="T722" id="Seg_6985" s="T721">kihi</ta>
            <ta e="T723" id="Seg_6986" s="T722">tu͡ok-nI</ta>
            <ta e="T724" id="Seg_6987" s="T723">da</ta>
            <ta e="T725" id="Seg_6988" s="T724">tu͡olkulaː-IAK</ta>
            <ta e="T726" id="Seg_6989" s="T725">bu͡ol-BAtAK</ta>
            <ta e="T727" id="Seg_6990" s="T726">e-TI-tA</ta>
            <ta e="T728" id="Seg_6991" s="T727">kolobur-tA</ta>
            <ta e="T729" id="Seg_6992" s="T728">ürt-I-BIT</ta>
            <ta e="T730" id="Seg_6993" s="T729">ulakan</ta>
            <ta e="T731" id="Seg_6994" s="T730">kɨːl</ta>
            <ta e="T732" id="Seg_6995" s="T731">ü͡ör-tA</ta>
            <ta e="T733" id="Seg_6996" s="T732">karbaː-Ar-tI-n</ta>
            <ta e="T734" id="Seg_6997" s="T733">kördük</ta>
            <ta e="T735" id="Seg_6998" s="T734">e-TI-tA</ta>
            <ta e="T736" id="Seg_6999" s="T735">iti</ta>
            <ta e="T737" id="Seg_7000" s="T736">ebe</ta>
            <ta e="T738" id="Seg_7001" s="T737">kes-Iː-tI-GAr</ta>
            <ta e="T739" id="Seg_7002" s="T738">töhölöːk</ta>
            <ta e="T740" id="Seg_7003" s="T739">kihi</ta>
            <ta e="T741" id="Seg_7004" s="T740">tumnahɨn-An</ta>
            <ta e="T742" id="Seg_7005" s="T741">heriː</ta>
            <ta e="T743" id="Seg_7006" s="T742">haː-tI-ttAn</ta>
            <ta e="T744" id="Seg_7007" s="T743">öl-BIT-tI-n</ta>
            <ta e="T745" id="Seg_7008" s="T744">kim</ta>
            <ta e="T746" id="Seg_7009" s="T745">daːganɨ</ta>
            <ta e="T747" id="Seg_7010" s="T746">bil-BAT</ta>
            <ta e="T748" id="Seg_7011" s="T747">oččogo</ta>
            <ta e="T749" id="Seg_7012" s="T748">e-TI-tA</ta>
            <ta e="T750" id="Seg_7013" s="T749">ebe-nI</ta>
            <ta e="T751" id="Seg_7014" s="T750">tagɨs-An</ta>
            <ta e="T752" id="Seg_7015" s="T751">baran</ta>
            <ta e="T753" id="Seg_7016" s="T752">bihi͡ettere</ta>
            <ta e="T754" id="Seg_7017" s="T753">emi͡e</ta>
            <ta e="T755" id="Seg_7018" s="T754">nʼemʼec-LAr-nI</ta>
            <ta e="T756" id="Seg_7019" s="T755">holo</ta>
            <ta e="T757" id="Seg_7020" s="T756">bul-TAr-BAkkA</ta>
            <ta e="T758" id="Seg_7021" s="T757">delbi</ta>
            <ta e="T759" id="Seg_7022" s="T758">bat-BIT-LArA</ta>
            <ta e="T760" id="Seg_7023" s="T759">onton</ta>
            <ta e="T761" id="Seg_7024" s="T760">min</ta>
            <ta e="T762" id="Seg_7025" s="T761">töhö</ta>
            <ta e="T763" id="Seg_7026" s="T762">eme</ta>
            <ta e="T764" id="Seg_7027" s="T763">heriː-GA</ta>
            <ta e="T765" id="Seg_7028" s="T764">bar-An-BIn</ta>
            <ta e="T766" id="Seg_7029" s="T765">tü͡ört-Is-LAːK</ta>
            <ta e="T767" id="Seg_7030" s="T766">kün-tI-GAr</ta>
            <ta e="T768" id="Seg_7031" s="T767">janvarʼ</ta>
            <ta e="T769" id="Seg_7032" s="T768">ɨj-GA</ta>
            <ta e="T770" id="Seg_7033" s="T769">tɨːhɨčča</ta>
            <ta e="T771" id="Seg_7034" s="T770">togus</ta>
            <ta e="T772" id="Seg_7035" s="T771">hüːs</ta>
            <ta e="T773" id="Seg_7036" s="T772">tü͡ört-u͡on</ta>
            <ta e="T774" id="Seg_7037" s="T773">üs</ta>
            <ta e="T775" id="Seg_7038" s="T774">dʼɨl-LAːK-GA</ta>
            <ta e="T776" id="Seg_7039" s="T775">bütüges-BI-n</ta>
            <ta e="T777" id="Seg_7040" s="T776">gojobuːn-LAː-A-t-BIT-I-m</ta>
            <ta e="T778" id="Seg_7041" s="T777">bert</ta>
            <ta e="T779" id="Seg_7042" s="T778">ɨ͡arakan-LIk</ta>
            <ta e="T780" id="Seg_7043" s="T779">kaŋas</ta>
            <ta e="T781" id="Seg_7044" s="T780">di͡ekki</ta>
            <ta e="T782" id="Seg_7045" s="T781">buːt-I-m</ta>
            <ta e="T783" id="Seg_7046" s="T782">oŋu͡ok-tI-n</ta>
            <ta e="T784" id="Seg_7047" s="T783">delbi</ta>
            <ta e="T785" id="Seg_7048" s="T784">tohut-TAr-BIT-I-m</ta>
            <ta e="T786" id="Seg_7049" s="T785">iti-nI</ta>
            <ta e="T787" id="Seg_7050" s="T786">emteː-A-t-An-BIn</ta>
            <ta e="T788" id="Seg_7051" s="T787">os-IAK-BA-r</ta>
            <ta e="T789" id="Seg_7052" s="T788">di͡eri</ta>
            <ta e="T790" id="Seg_7053" s="T789">alta</ta>
            <ta e="T791" id="Seg_7054" s="T790">ɨj</ta>
            <ta e="T792" id="Seg_7055" s="T791">turkarɨ</ta>
            <ta e="T793" id="Seg_7056" s="T792">hɨt-BIT-I-m</ta>
            <ta e="T794" id="Seg_7057" s="T793">Essentuki</ta>
            <ta e="T795" id="Seg_7058" s="T794">di͡e-An</ta>
            <ta e="T796" id="Seg_7059" s="T795">gu͡orat-GA</ta>
            <ta e="T797" id="Seg_7060" s="T796">Kavkaːz</ta>
            <ta e="T798" id="Seg_7061" s="T797">di͡e-An</ta>
            <ta e="T799" id="Seg_7062" s="T798">itiː</ta>
            <ta e="T800" id="Seg_7063" s="T799">hir-GA</ta>
            <ta e="T801" id="Seg_7064" s="T800">onton</ta>
            <ta e="T802" id="Seg_7065" s="T801">emteː-A-t-An</ta>
            <ta e="T803" id="Seg_7066" s="T802">baran</ta>
            <ta e="T804" id="Seg_7067" s="T803">mas</ta>
            <ta e="T805" id="Seg_7068" s="T804">tanʼak-LAːK</ta>
            <ta e="T806" id="Seg_7069" s="T805">tagɨs-An</ta>
            <ta e="T807" id="Seg_7070" s="T806">bar-An-BIn</ta>
            <ta e="T808" id="Seg_7071" s="T807">heriː-ttAn</ta>
            <ta e="T809" id="Seg_7072" s="T808">adʼas</ta>
            <ta e="T810" id="Seg_7073" s="T809">boskoloː-A-n-BIT-I-m</ta>
            <ta e="T811" id="Seg_7074" s="T810">armʼija-ttAn</ta>
            <ta e="T813" id="Seg_7075" s="T812">tagɨs-I-BIT-I-m</ta>
            <ta e="T814" id="Seg_7076" s="T813">maŋnaj</ta>
            <ta e="T815" id="Seg_7077" s="T814">heriːles-BIT</ta>
            <ta e="T816" id="Seg_7078" s="T815">kün-BI-ttAn</ta>
            <ta e="T817" id="Seg_7079" s="T816">biːr</ta>
            <ta e="T818" id="Seg_7080" s="T817">dʼɨl</ta>
            <ta e="T819" id="Seg_7081" s="T818">aŋar-tI-n</ta>
            <ta e="T820" id="Seg_7082" s="T819">turkarɨ</ta>
            <ta e="T821" id="Seg_7083" s="T820">heriː</ta>
            <ta e="T822" id="Seg_7084" s="T821">kersiː-tI-GAr</ta>
            <ta e="T823" id="Seg_7085" s="T822">hɨrɨt-An-BIn</ta>
            <ta e="T824" id="Seg_7086" s="T823">barɨta</ta>
            <ta e="T825" id="Seg_7087" s="T824">tü͡ört-GA</ta>
            <ta e="T826" id="Seg_7088" s="T825">kat</ta>
            <ta e="T827" id="Seg_7089" s="T826">gojobuːn-LAː-A-t-BIT-I-m</ta>
            <ta e="T828" id="Seg_7090" s="T827">iti</ta>
            <ta e="T829" id="Seg_7091" s="T828">tus-tI-nAn</ta>
            <ta e="T830" id="Seg_7092" s="T829">üs-TA</ta>
            <ta e="T831" id="Seg_7093" s="T830">emteː-A-t-A</ta>
            <ta e="T832" id="Seg_7094" s="T831">hɨrɨt-I-BIT-I-m</ta>
            <ta e="T833" id="Seg_7095" s="T832">kɨlgas-LIk</ta>
            <ta e="T834" id="Seg_7096" s="T833">kepseː-TAK-GA</ta>
            <ta e="T835" id="Seg_7097" s="T834">itigirdik</ta>
            <ta e="T836" id="Seg_7098" s="T835">min</ta>
            <ta e="T837" id="Seg_7099" s="T836">kihi</ta>
            <ta e="T838" id="Seg_7100" s="T837">ajmak</ta>
            <ta e="T839" id="Seg_7101" s="T838">üje-tI-GAr</ta>
            <ta e="T840" id="Seg_7102" s="T839">kaččaga</ta>
            <ta e="T841" id="Seg_7103" s="T840">da</ta>
            <ta e="T842" id="Seg_7104" s="T841">bil-LIN-I-BAtAK</ta>
            <ta e="T843" id="Seg_7105" s="T842">aga</ta>
            <ta e="T844" id="Seg_7106" s="T843">da</ta>
            <ta e="T845" id="Seg_7107" s="T844">iti</ta>
            <ta e="T846" id="Seg_7108" s="T845">ulakan</ta>
            <ta e="T847" id="Seg_7109" s="T846">heriː-tI-GAr</ta>
            <ta e="T848" id="Seg_7110" s="T847">beje-m</ta>
            <ta e="T849" id="Seg_7111" s="T848">dʼon-BI-n</ta>
            <ta e="T850" id="Seg_7112" s="T849">töröː-BIT</ta>
            <ta e="T851" id="Seg_7113" s="T850">ü͡öskeː-BIT</ta>
            <ta e="T852" id="Seg_7114" s="T851">hir-BI-n</ta>
            <ta e="T853" id="Seg_7115" s="T852">kömüskeː-A</ta>
            <ta e="T854" id="Seg_7116" s="T853">hɨrɨt-I-BIT-I-m</ta>
            <ta e="T855" id="Seg_7117" s="T854">mini͡ene</ta>
            <ta e="T856" id="Seg_7118" s="T855">kaːn-I-m</ta>
            <ta e="T857" id="Seg_7119" s="T856">kölöhün-I-m</ta>
            <ta e="T858" id="Seg_7120" s="T857">iti</ta>
            <ta e="T859" id="Seg_7121" s="T858">heriː-GA</ta>
            <ta e="T860" id="Seg_7122" s="T859">köŋül</ta>
            <ta e="T861" id="Seg_7123" s="T860">tok-n-I-BAtAK-tA</ta>
            <ta e="T862" id="Seg_7124" s="T861">bihi͡ene</ta>
            <ta e="T863" id="Seg_7125" s="T862">kammunʼistʼičʼeskaj</ta>
            <ta e="T864" id="Seg_7126" s="T863">partʼija-BIt</ta>
            <ta e="T865" id="Seg_7127" s="T864">kiːn</ta>
            <ta e="T866" id="Seg_7128" s="T865">kаmitʼet-tA</ta>
            <ta e="T867" id="Seg_7129" s="T866">onton</ta>
            <ta e="T868" id="Seg_7130" s="T867">bihi͡ene</ta>
            <ta e="T869" id="Seg_7131" s="T868">gasudarstva-BItI-n</ta>
            <ta e="T870" id="Seg_7132" s="T869">olok-tI-n</ta>
            <ta e="T871" id="Seg_7133" s="T870">könnör-AːččI</ta>
            <ta e="T872" id="Seg_7134" s="T871">pravʼitʼelʼstva-BIt</ta>
            <ta e="T873" id="Seg_7135" s="T872">bi͡er-BIT-LArA</ta>
            <ta e="T874" id="Seg_7136" s="T873">min-GA</ta>
            <ta e="T875" id="Seg_7137" s="T874">gasudarstva</ta>
            <ta e="T876" id="Seg_7138" s="T875">ürdük</ta>
            <ta e="T877" id="Seg_7139" s="T876">heriː-tI-n</ta>
            <ta e="T878" id="Seg_7140" s="T877">ordʼen-tI-n</ta>
            <ta e="T882" id="Seg_7141" s="T881">di͡e-An</ta>
            <ta e="T883" id="Seg_7142" s="T882">onton</ta>
            <ta e="T884" id="Seg_7143" s="T883">össü͡ö</ta>
            <ta e="T885" id="Seg_7144" s="T884">mʼedalʼ-nI</ta>
            <ta e="T890" id="Seg_7145" s="T889">di͡e-An</ta>
            <ta e="T891" id="Seg_7146" s="T890">anɨ</ta>
            <ta e="T892" id="Seg_7147" s="T891">bihigi</ta>
            <ta e="T893" id="Seg_7148" s="T892">habi͡eskaj</ta>
            <ta e="T894" id="Seg_7149" s="T893">olok-tI-r</ta>
            <ta e="T895" id="Seg_7150" s="T894">olor-Ar</ta>
            <ta e="T896" id="Seg_7151" s="T895">kihi-LAr</ta>
            <ta e="T897" id="Seg_7152" s="T896">olok-GIt</ta>
            <ta e="T898" id="Seg_7153" s="T897">berke</ta>
            <ta e="T899" id="Seg_7154" s="T898">tugus-An</ta>
            <ta e="T900" id="Seg_7155" s="T899">tur-Ar</ta>
            <ta e="T901" id="Seg_7156" s="T900">ilin</ta>
            <ta e="T902" id="Seg_7157" s="T901">dek</ta>
            <ta e="T903" id="Seg_7158" s="T902">bar-Ar</ta>
            <ta e="T904" id="Seg_7159" s="T903">küːs-GA</ta>
            <ta e="T905" id="Seg_7160" s="T904">ol</ta>
            <ta e="T906" id="Seg_7161" s="T905">ihin</ta>
            <ta e="T907" id="Seg_7162" s="T906">kim</ta>
            <ta e="T908" id="Seg_7163" s="T907">daːganɨ</ta>
            <ta e="T909" id="Seg_7164" s="T908">anɨ</ta>
            <ta e="T910" id="Seg_7165" s="T909">heriː-GI-n</ta>
            <ta e="T911" id="Seg_7166" s="T910">bu͡ol-IAK-tI-n</ta>
            <ta e="T912" id="Seg_7167" s="T911">bagar-BAT</ta>
            <ta e="T913" id="Seg_7168" s="T912">bihigi</ta>
            <ta e="T914" id="Seg_7169" s="T913">hir-BIt</ta>
            <ta e="T915" id="Seg_7170" s="T914">kihi-tA</ta>
            <ta e="T916" id="Seg_7171" s="T915">urukku-ttAn</ta>
            <ta e="T917" id="Seg_7172" s="T916">daːganɨ</ta>
            <ta e="T918" id="Seg_7173" s="T917">heriː-GA</ta>
            <ta e="T919" id="Seg_7174" s="T918">bagar-AːččI-tA</ta>
            <ta e="T920" id="Seg_7175" s="T919">hu͡ok</ta>
            <ta e="T921" id="Seg_7176" s="T920">e-TI-tA</ta>
            <ta e="T922" id="Seg_7177" s="T921">bihigi</ta>
            <ta e="T923" id="Seg_7178" s="T922">dʼon-BIt</ta>
            <ta e="T924" id="Seg_7179" s="T923">anɨ</ta>
            <ta e="T925" id="Seg_7180" s="T924">beje-tI-n</ta>
            <ta e="T926" id="Seg_7181" s="T925">hanaː-tA</ta>
            <ta e="T927" id="Seg_7182" s="T926">höbüleː-Ar-tI-n</ta>
            <ta e="T928" id="Seg_7183" s="T927">hɨrdɨk</ta>
            <ta e="T929" id="Seg_7184" s="T928">dʼolloːk</ta>
            <ta e="T930" id="Seg_7185" s="T929">Lʼenʼin</ta>
            <ta e="T931" id="Seg_7186" s="T930">ɨj-BIT</ta>
            <ta e="T932" id="Seg_7187" s="T931">kommunʼist</ta>
            <ta e="T933" id="Seg_7188" s="T932">aːt-LAːK</ta>
            <ta e="T934" id="Seg_7189" s="T933">olok-tI-n</ta>
            <ta e="T935" id="Seg_7190" s="T934">oŋohun-Ar</ta>
            <ta e="T936" id="Seg_7191" s="T935">bihigi</ta>
            <ta e="T937" id="Seg_7192" s="T936">barɨ-BIt</ta>
            <ta e="T938" id="Seg_7193" s="T937">di͡e-A-BIt</ta>
            <ta e="T939" id="Seg_7194" s="T938">anɨ</ta>
            <ta e="T940" id="Seg_7195" s="T939">heriː</ta>
            <ta e="T941" id="Seg_7196" s="T940">hu͡ok</ta>
            <ta e="T942" id="Seg_7197" s="T941">bu͡ol-TIn</ta>
            <ta e="T943" id="Seg_7198" s="T942">hir</ta>
            <ta e="T944" id="Seg_7199" s="T943">ürüt-tI-nAn</ta>
            <ta e="T945" id="Seg_7200" s="T944">baːr</ta>
            <ta e="T946" id="Seg_7201" s="T945">bu͡ol-TIn</ta>
            <ta e="T947" id="Seg_7202" s="T946">hɨrdɨk</ta>
            <ta e="T948" id="Seg_7203" s="T947">kün-LAːK</ta>
            <ta e="T949" id="Seg_7204" s="T948">olok</ta>
            <ta e="T950" id="Seg_7205" s="T949">barɨ</ta>
            <ta e="T951" id="Seg_7206" s="T950">dʼon-GA</ta>
         </annotation>
         <annotation name="ge" tierref="ge">
            <ta e="T1" id="Seg_7207" s="T0">1SG.[NOM]</ta>
            <ta e="T2" id="Seg_7208" s="T1">war-DAT/LOC</ta>
            <ta e="T3" id="Seg_7209" s="T2">go-PST2-EP-1SG</ta>
            <ta e="T4" id="Seg_7210" s="T3">self-1SG.[NOM]</ta>
            <ta e="T5" id="Seg_7211" s="T4">wish-1SG-INSTR</ta>
            <ta e="T6" id="Seg_7212" s="T5">thousand</ta>
            <ta e="T7" id="Seg_7213" s="T6">nine</ta>
            <ta e="T8" id="Seg_7214" s="T7">hundred</ta>
            <ta e="T9" id="Seg_7215" s="T8">four-ten</ta>
            <ta e="T10" id="Seg_7216" s="T9">one</ta>
            <ta e="T11" id="Seg_7217" s="T10">year-PROPR-DAT/LOC</ta>
            <ta e="T12" id="Seg_7218" s="T11">autumnal</ta>
            <ta e="T13" id="Seg_7219" s="T12">september.[NOM]</ta>
            <ta e="T14" id="Seg_7220" s="T13">cold.[NOM]</ta>
            <ta e="T15" id="Seg_7221" s="T14">fall-PTCP.PRS</ta>
            <ta e="T16" id="Seg_7222" s="T15">month-3SG-DAT/LOC</ta>
            <ta e="T17" id="Seg_7223" s="T16">at.first</ta>
            <ta e="T18" id="Seg_7224" s="T17">go-CVB.SEQ-1SG</ta>
            <ta e="T19" id="Seg_7225" s="T18">one</ta>
            <ta e="T20" id="Seg_7226" s="T19">year-ACC</ta>
            <ta e="T21" id="Seg_7227" s="T20">with</ta>
            <ta e="T22" id="Seg_7228" s="T21">nine</ta>
            <ta e="T23" id="Seg_7229" s="T22">month-ACC</ta>
            <ta e="T24" id="Seg_7230" s="T23">war.[NOM]</ta>
            <ta e="T25" id="Seg_7231" s="T24">education-3SG-DAT/LOC</ta>
            <ta e="T26" id="Seg_7232" s="T25">learn-CVB.SIM</ta>
            <ta e="T27" id="Seg_7233" s="T26">go-EP-PST2-EP-1SG</ta>
            <ta e="T29" id="Seg_7234" s="T27">Far.East.[NOM]</ta>
            <ta e="T30" id="Seg_7235" s="T29">say-CVB.SEQ</ta>
            <ta e="T31" id="Seg_7236" s="T30">place-DAT/LOC</ta>
            <ta e="T32" id="Seg_7237" s="T31">then</ta>
            <ta e="T33" id="Seg_7238" s="T32">in.summer</ta>
            <ta e="T34" id="Seg_7239" s="T33">thousand</ta>
            <ta e="T35" id="Seg_7240" s="T34">nine</ta>
            <ta e="T36" id="Seg_7241" s="T35">hundred</ta>
            <ta e="T37" id="Seg_7242" s="T36">four-ten</ta>
            <ta e="T38" id="Seg_7243" s="T37">three</ta>
            <ta e="T39" id="Seg_7244" s="T38">year-PROPR-DAT/LOC</ta>
            <ta e="T40" id="Seg_7245" s="T39">1PL-ACC</ta>
            <ta e="T41" id="Seg_7246" s="T40">send-PST2-3PL</ta>
            <ta e="T42" id="Seg_7247" s="T41">west.[NOM]</ta>
            <ta e="T43" id="Seg_7248" s="T42">to</ta>
            <ta e="T44" id="Seg_7249" s="T43">war.[NOM]</ta>
            <ta e="T45" id="Seg_7250" s="T44">fight-3SG.[NOM]</ta>
            <ta e="T46" id="Seg_7251" s="T45">go-CVB.SIM</ta>
            <ta e="T47" id="Seg_7252" s="T46">stand-PTCP.PRS</ta>
            <ta e="T48" id="Seg_7253" s="T47">place-3SG-DAT/LOC</ta>
            <ta e="T49" id="Seg_7254" s="T48">1PL.[NOM]</ta>
            <ta e="T50" id="Seg_7255" s="T49">war-ACC</ta>
            <ta e="T51" id="Seg_7256" s="T50">meet-EP-PST2-1PL</ta>
            <ta e="T52" id="Seg_7257" s="T51">Prokhorovka.[NOM]</ta>
            <ta e="T53" id="Seg_7258" s="T52">say-CVB.SEQ</ta>
            <ta e="T54" id="Seg_7259" s="T53">name-PROPR</ta>
            <ta e="T55" id="Seg_7260" s="T54">village-DAT/LOC</ta>
            <ta e="T56" id="Seg_7261" s="T55">Kursk.[NOM]</ta>
            <ta e="T57" id="Seg_7262" s="T56">oblast-3SG-DAT/LOC</ta>
            <ta e="T58" id="Seg_7263" s="T57">there</ta>
            <ta e="T59" id="Seg_7264" s="T58">german</ta>
            <ta e="T60" id="Seg_7265" s="T59">war-3SG-GEN</ta>
            <ta e="T61" id="Seg_7266" s="T60">human.being-3SG.[NOM]</ta>
            <ta e="T62" id="Seg_7267" s="T61">very</ta>
            <ta e="T63" id="Seg_7268" s="T62">many</ta>
            <ta e="T64" id="Seg_7269" s="T63">war.[NOM]</ta>
            <ta e="T65" id="Seg_7270" s="T64">weapon-3SG-ACC</ta>
            <ta e="T66" id="Seg_7271" s="T65">different</ta>
            <ta e="T67" id="Seg_7272" s="T66">different</ta>
            <ta e="T68" id="Seg_7273" s="T67">ammunition-3SG-ACC</ta>
            <ta e="T69" id="Seg_7274" s="T68">gather-CVB.SEQ</ta>
            <ta e="T70" id="Seg_7275" s="T69">after</ta>
            <ta e="T71" id="Seg_7276" s="T70">1PL.[NOM]</ta>
            <ta e="T72" id="Seg_7277" s="T71">army-1PL-ACC</ta>
            <ta e="T73" id="Seg_7278" s="T72">very</ta>
            <ta e="T74" id="Seg_7279" s="T73">back</ta>
            <ta e="T75" id="Seg_7280" s="T74">to</ta>
            <ta e="T76" id="Seg_7281" s="T75">thirty</ta>
            <ta e="T77" id="Seg_7282" s="T76">five</ta>
            <ta e="T78" id="Seg_7283" s="T77">verst-PROPR</ta>
            <ta e="T79" id="Seg_7284" s="T78">place-DAT/LOC</ta>
            <ta e="T80" id="Seg_7285" s="T79">push.back-PTCP.PST</ta>
            <ta e="T81" id="Seg_7286" s="T80">be-PST1-3SG</ta>
            <ta e="T82" id="Seg_7287" s="T81">then</ta>
            <ta e="T83" id="Seg_7288" s="T82">later</ta>
            <ta e="T84" id="Seg_7289" s="T83">that</ta>
            <ta e="T85" id="Seg_7290" s="T84">place-ACC</ta>
            <ta e="T86" id="Seg_7291" s="T85">Kursk.[NOM]</ta>
            <ta e="T87" id="Seg_7292" s="T86">curve.[NOM]</ta>
            <ta e="T88" id="Seg_7293" s="T87">say-CVB.SEQ</ta>
            <ta e="T89" id="Seg_7294" s="T88">name-VBZ-PST2-3PL</ta>
            <ta e="T90" id="Seg_7295" s="T89">Dolgan-SIM</ta>
            <ta e="T91" id="Seg_7296" s="T90">speak-PTCP.COND-DAT/LOC</ta>
            <ta e="T92" id="Seg_7297" s="T91">Kursk.[NOM]</ta>
            <ta e="T93" id="Seg_7298" s="T92">curved-3SG</ta>
            <ta e="T94" id="Seg_7299" s="T93">say-PTCP.FUT-DAT/LOC</ta>
            <ta e="T95" id="Seg_7300" s="T94">right.[NOM]</ta>
            <ta e="T96" id="Seg_7301" s="T95">be-FUT-3SG</ta>
            <ta e="T97" id="Seg_7302" s="T96">german</ta>
            <ta e="T98" id="Seg_7303" s="T97">army-3SG-GEN</ta>
            <ta e="T99" id="Seg_7304" s="T98">lord-3SG-GEN</ta>
            <ta e="T100" id="Seg_7305" s="T99">thought-3SG.[NOM]</ta>
            <ta e="T101" id="Seg_7306" s="T100">there.is</ta>
            <ta e="T102" id="Seg_7307" s="T101">be-PST2.[3SG]</ta>
            <ta e="T103" id="Seg_7308" s="T102">that.[NOM]</ta>
            <ta e="T104" id="Seg_7309" s="T103">earth-3SG-INSTR</ta>
            <ta e="T105" id="Seg_7310" s="T104">through</ta>
            <ta e="T106" id="Seg_7311" s="T105">struggle-CVB.SEQ</ta>
            <ta e="T107" id="Seg_7312" s="T106">after</ta>
            <ta e="T108" id="Seg_7313" s="T107">very</ta>
            <ta e="T109" id="Seg_7314" s="T108">fast-ADVZ</ta>
            <ta e="T110" id="Seg_7315" s="T109">1PL.[NOM]</ta>
            <ta e="T111" id="Seg_7316" s="T110">centre.[NOM]</ta>
            <ta e="T112" id="Seg_7317" s="T111">place-1PL-ACC</ta>
            <ta e="T113" id="Seg_7318" s="T112">Moscow.[NOM]</ta>
            <ta e="T114" id="Seg_7319" s="T113">say-CVB.SEQ</ta>
            <ta e="T115" id="Seg_7320" s="T114">city-ACC</ta>
            <ta e="T116" id="Seg_7321" s="T115">take-PTCP.PRS.[NOM]</ta>
            <ta e="T117" id="Seg_7322" s="T116">similar</ta>
            <ta e="T118" id="Seg_7323" s="T117">our</ta>
            <ta e="T119" id="Seg_7324" s="T118">part-1PL.[NOM]</ta>
            <ta e="T120" id="Seg_7325" s="T119">Kursk.[NOM]</ta>
            <ta e="T121" id="Seg_7326" s="T120">curved-3SG-DAT/LOC</ta>
            <ta e="T122" id="Seg_7327" s="T121">what-ABL</ta>
            <ta e="T123" id="Seg_7328" s="T122">NEG</ta>
            <ta e="T124" id="Seg_7329" s="T123">big</ta>
            <ta e="T125" id="Seg_7330" s="T124">war.[NOM]</ta>
            <ta e="T126" id="Seg_7331" s="T125">fire-3SG-DAT/LOC</ta>
            <ta e="T127" id="Seg_7332" s="T126">go.in-CVB.SEQ</ta>
            <ta e="T128" id="Seg_7333" s="T127">after</ta>
            <ta e="T129" id="Seg_7334" s="T128">ten</ta>
            <ta e="T130" id="Seg_7335" s="T129">day.[NOM]</ta>
            <ta e="T131" id="Seg_7336" s="T130">as.long.as</ta>
            <ta e="T132" id="Seg_7337" s="T131">german</ta>
            <ta e="T133" id="Seg_7338" s="T132">army-3SG-ACC</ta>
            <ta e="T134" id="Seg_7339" s="T133">chase-CVB.SIM</ta>
            <ta e="T135" id="Seg_7340" s="T134">try-PST2-3SG</ta>
            <ta e="T136" id="Seg_7341" s="T135">that.[NOM]</ta>
            <ta e="T137" id="Seg_7342" s="T136">as.long.as</ta>
            <ta e="T138" id="Seg_7343" s="T137">1PL.[NOM]</ta>
            <ta e="T139" id="Seg_7344" s="T138">fight-PST2-1PL</ta>
            <ta e="T140" id="Seg_7345" s="T139">day-ACC</ta>
            <ta e="T141" id="Seg_7346" s="T140">night-ACC</ta>
            <ta e="T142" id="Seg_7347" s="T141">notice-NEG.CVB.SIM</ta>
            <ta e="T143" id="Seg_7348" s="T142">sleep-NEG.CVB.SIM</ta>
            <ta e="T144" id="Seg_7349" s="T143">NEG</ta>
            <ta e="T145" id="Seg_7350" s="T144">relax-NEG.CVB.SIM</ta>
            <ta e="T146" id="Seg_7351" s="T145">NEG</ta>
            <ta e="T147" id="Seg_7352" s="T146">example-3SG.[NOM]</ta>
            <ta e="T148" id="Seg_7353" s="T147">what-ACC</ta>
            <ta e="T149" id="Seg_7354" s="T148">NEG</ta>
            <ta e="T150" id="Seg_7355" s="T149">know-NEG.PTCP.[NOM]</ta>
            <ta e="T151" id="Seg_7356" s="T150">hear-EP-NEG.PTCP.[NOM]</ta>
            <ta e="T152" id="Seg_7357" s="T151">similar</ta>
            <ta e="T153" id="Seg_7358" s="T152">two</ta>
            <ta e="T154" id="Seg_7359" s="T153">side</ta>
            <ta e="T155" id="Seg_7360" s="T154">fight-PTCP.HAB</ta>
            <ta e="T156" id="Seg_7361" s="T155">side-ABL</ta>
            <ta e="T157" id="Seg_7362" s="T156">so.much</ta>
            <ta e="T158" id="Seg_7363" s="T157">thousand</ta>
            <ta e="T159" id="Seg_7364" s="T158">human.being.[NOM]</ta>
            <ta e="T160" id="Seg_7365" s="T159">get.lost-PTCP.PST-3SG-ACC</ta>
            <ta e="T161" id="Seg_7366" s="T160">die-PTCP.PST-3SG-ACC</ta>
            <ta e="T162" id="Seg_7367" s="T161">so.much</ta>
            <ta e="T163" id="Seg_7368" s="T162">human.being.[NOM]</ta>
            <ta e="T164" id="Seg_7369" s="T163">wound.[NOM]</ta>
            <ta e="T165" id="Seg_7370" s="T164">be-PTCP.PST-3SG-ACC</ta>
            <ta e="T166" id="Seg_7371" s="T165">so.much</ta>
            <ta e="T167" id="Seg_7372" s="T166">war.[NOM]</ta>
            <ta e="T168" id="Seg_7373" s="T167">weapon-3SG.[NOM]</ta>
            <ta e="T169" id="Seg_7374" s="T168">airplane-PL.[NOM]</ta>
            <ta e="T170" id="Seg_7375" s="T169">tank-PL.[NOM]</ta>
            <ta e="T171" id="Seg_7376" s="T170">gun-PL.[NOM]</ta>
            <ta e="T172" id="Seg_7377" s="T171">then</ta>
            <ta e="T173" id="Seg_7378" s="T172">small</ta>
            <ta e="T174" id="Seg_7379" s="T173">different</ta>
            <ta e="T175" id="Seg_7380" s="T174">rifle-PL.[NOM]</ta>
            <ta e="T176" id="Seg_7381" s="T175">war.[NOM]</ta>
            <ta e="T177" id="Seg_7382" s="T176">ammunition-3SG.[NOM]</ta>
            <ta e="T178" id="Seg_7383" s="T177">break-PTCP.PST-3SG-ACC</ta>
            <ta e="T179" id="Seg_7384" s="T178">who.[NOM]</ta>
            <ta e="T180" id="Seg_7385" s="T179">EMPH</ta>
            <ta e="T181" id="Seg_7386" s="T180">especially</ta>
            <ta e="T182" id="Seg_7387" s="T181">know-NEG.PTCP</ta>
            <ta e="T183" id="Seg_7388" s="T182">be-PST1-3SG</ta>
            <ta e="T184" id="Seg_7389" s="T183">then</ta>
            <ta e="T185" id="Seg_7390" s="T184">how</ta>
            <ta e="T186" id="Seg_7391" s="T185">big</ta>
            <ta e="T187" id="Seg_7392" s="T186">strong</ta>
            <ta e="T188" id="Seg_7393" s="T187">war.[NOM]</ta>
            <ta e="T189" id="Seg_7394" s="T188">go-PTCP.PST-3SG-ACC</ta>
            <ta e="T190" id="Seg_7395" s="T189">that.[NOM]</ta>
            <ta e="T191" id="Seg_7396" s="T190">place-DAT/LOC</ta>
            <ta e="T192" id="Seg_7397" s="T191">human.being.[NOM]</ta>
            <ta e="T193" id="Seg_7398" s="T192">understand-FUT-3SG</ta>
            <ta e="T194" id="Seg_7399" s="T193">one</ta>
            <ta e="T195" id="Seg_7400" s="T194">such</ta>
            <ta e="T196" id="Seg_7401" s="T195">example-ABL</ta>
            <ta e="T197" id="Seg_7402" s="T196">one</ta>
            <ta e="T198" id="Seg_7403" s="T197">ice-PROPR</ta>
            <ta e="T199" id="Seg_7404" s="T198">place-DIM-DAT/LOC</ta>
            <ta e="T200" id="Seg_7405" s="T199">1PL.[NOM]</ta>
            <ta e="T201" id="Seg_7406" s="T200">there.is</ta>
            <ta e="T202" id="Seg_7407" s="T201">earth-1PL-DAT/LOC</ta>
            <ta e="T203" id="Seg_7408" s="T202">day-DAT/LOC</ta>
            <ta e="T204" id="Seg_7409" s="T203">once</ta>
            <ta e="T205" id="Seg_7410" s="T204">fly-CVB.SEQ</ta>
            <ta e="T206" id="Seg_7411" s="T205">come-PTCP.HAB</ta>
            <ta e="T207" id="Seg_7412" s="T206">be-PST1-3SG</ta>
            <ta e="T208" id="Seg_7413" s="T207">German.[NOM]</ta>
            <ta e="T209" id="Seg_7414" s="T208">in.the.direction</ta>
            <ta e="T210" id="Seg_7415" s="T209">side-ABL</ta>
            <ta e="T211" id="Seg_7416" s="T210">two</ta>
            <ta e="T212" id="Seg_7417" s="T211">hundred</ta>
            <ta e="T213" id="Seg_7418" s="T212">approximately-3SG.[NOM]</ta>
            <ta e="T214" id="Seg_7419" s="T213">airplane-PL.[NOM]</ta>
            <ta e="T215" id="Seg_7420" s="T214">that-3SG-2SG.[NOM]</ta>
            <ta e="T216" id="Seg_7421" s="T215">every-3SG.[NOM]</ta>
            <ta e="T217" id="Seg_7422" s="T216">1PL.[NOM]</ta>
            <ta e="T218" id="Seg_7423" s="T217">upper.part-1PL-INSTR</ta>
            <ta e="T219" id="Seg_7424" s="T218">rain.[NOM]</ta>
            <ta e="T220" id="Seg_7425" s="T219">similar</ta>
            <ta e="T221" id="Seg_7426" s="T220">fall-CVB.SEQ</ta>
            <ta e="T222" id="Seg_7427" s="T221">eh</ta>
            <ta e="T223" id="Seg_7428" s="T222">hundred</ta>
            <ta e="T224" id="Seg_7429" s="T223">bomb-PL-ACC</ta>
            <ta e="T225" id="Seg_7430" s="T224">throw-PTCP.HAB</ta>
            <ta e="T226" id="Seg_7431" s="T225">be-PST1-3SG</ta>
            <ta e="T227" id="Seg_7432" s="T226">then</ta>
            <ta e="T228" id="Seg_7433" s="T227">individually</ta>
            <ta e="T229" id="Seg_7434" s="T228">still</ta>
            <ta e="T230" id="Seg_7435" s="T229">shoot-FREQ-PTCP.PRS</ta>
            <ta e="T231" id="Seg_7436" s="T230">be-PST1-3SG</ta>
            <ta e="T232" id="Seg_7437" s="T231">how.much</ta>
            <ta e="T233" id="Seg_7438" s="T232">INDEF</ta>
            <ta e="T234" id="Seg_7439" s="T233">small.patron.[NOM]</ta>
            <ta e="T235" id="Seg_7440" s="T234">similar</ta>
            <ta e="T236" id="Seg_7441" s="T235">load-NMNZ-PROPR</ta>
            <ta e="T237" id="Seg_7442" s="T236">war.[NOM]</ta>
            <ta e="T238" id="Seg_7443" s="T237">weapon-3SG-INSTR</ta>
            <ta e="T239" id="Seg_7444" s="T238">machine.gun-EP-INSTR</ta>
            <ta e="T240" id="Seg_7445" s="T239">that.much-PROPR</ta>
            <ta e="T241" id="Seg_7446" s="T240">bomb.[NOM]</ta>
            <ta e="T242" id="Seg_7447" s="T241">fall-PTCP.PST-3SG-ACC</ta>
            <ta e="T243" id="Seg_7448" s="T242">after</ta>
            <ta e="T244" id="Seg_7449" s="T243">that.much-PROPR</ta>
            <ta e="T245" id="Seg_7450" s="T244">weapon.[NOM]</ta>
            <ta e="T246" id="Seg_7451" s="T245">shoot-FREQ-PTCP.PST-3SG-ACC</ta>
            <ta e="T247" id="Seg_7452" s="T246">after</ta>
            <ta e="T248" id="Seg_7453" s="T247">earth.[NOM]</ta>
            <ta e="T249" id="Seg_7454" s="T248">completely</ta>
            <ta e="T250" id="Seg_7455" s="T249">what-INTNS.[NOM]</ta>
            <ta e="T251" id="Seg_7456" s="T250">NEG</ta>
            <ta e="T252" id="Seg_7457" s="T251">to.be.on.view-EP-NEG.PTCP.[NOM]</ta>
            <ta e="T253" id="Seg_7458" s="T252">similar</ta>
            <ta e="T254" id="Seg_7459" s="T253">black</ta>
            <ta e="T255" id="Seg_7460" s="T254">fog.[NOM]</ta>
            <ta e="T256" id="Seg_7461" s="T255">become-PTCP.PRS</ta>
            <ta e="T257" id="Seg_7462" s="T256">be-PST1-3SG</ta>
            <ta e="T258" id="Seg_7463" s="T257">that</ta>
            <ta e="T259" id="Seg_7464" s="T258">fog-DAT/LOC</ta>
            <ta e="T260" id="Seg_7465" s="T259">human.being.[NOM]</ta>
            <ta e="T261" id="Seg_7466" s="T260">place.beneath-DAT/LOC</ta>
            <ta e="T262" id="Seg_7467" s="T261">lie-PTCP.PRS</ta>
            <ta e="T263" id="Seg_7468" s="T262">human.being-3SG-ACC</ta>
            <ta e="T264" id="Seg_7469" s="T263">make.it-CVB.SEQ</ta>
            <ta e="T265" id="Seg_7470" s="T264">see-NEG.PTCP</ta>
            <ta e="T266" id="Seg_7471" s="T265">be-PST1-3SG</ta>
            <ta e="T267" id="Seg_7472" s="T266">some.[NOM]</ta>
            <ta e="T268" id="Seg_7473" s="T267">after</ta>
            <ta e="T269" id="Seg_7474" s="T268">human.being.[NOM]</ta>
            <ta e="T270" id="Seg_7475" s="T269">war.[NOM]</ta>
            <ta e="T271" id="Seg_7476" s="T270">weapon-3SG-GEN</ta>
            <ta e="T272" id="Seg_7477" s="T271">noise-3SG-ABL</ta>
            <ta e="T273" id="Seg_7478" s="T272">what-INTNS-ACC</ta>
            <ta e="T274" id="Seg_7479" s="T273">NEG</ta>
            <ta e="T275" id="Seg_7480" s="T274">hear-EP-NEG.PTCP.[NOM]</ta>
            <ta e="T276" id="Seg_7481" s="T275">similar</ta>
            <ta e="T277" id="Seg_7482" s="T276">deaf.[NOM]</ta>
            <ta e="T278" id="Seg_7483" s="T277">become-PTCP.PRS</ta>
            <ta e="T279" id="Seg_7484" s="T278">be-PST1-3SG</ta>
            <ta e="T280" id="Seg_7485" s="T279">ten-ORD</ta>
            <ta e="T281" id="Seg_7486" s="T280">day-1PL-DAT/LOC</ta>
            <ta e="T282" id="Seg_7487" s="T281">1PL.[NOM]</ta>
            <ta e="T283" id="Seg_7488" s="T282">in.the.direction</ta>
            <ta e="T284" id="Seg_7489" s="T283">side-INSTR</ta>
            <ta e="T285" id="Seg_7490" s="T284">war-ACC</ta>
            <ta e="T286" id="Seg_7491" s="T285">very</ta>
            <ta e="T287" id="Seg_7492" s="T286">power-PROPR-ADVZ</ta>
            <ta e="T288" id="Seg_7493" s="T287">go-PST1-3SG</ta>
            <ta e="T289" id="Seg_7494" s="T288">sky-ABL</ta>
            <ta e="T290" id="Seg_7495" s="T289">earth-ABL</ta>
            <ta e="T291" id="Seg_7496" s="T290">very</ta>
            <ta e="T292" id="Seg_7497" s="T291">many</ta>
            <ta e="T293" id="Seg_7498" s="T292">airplane-PL.[NOM]</ta>
            <ta e="T294" id="Seg_7499" s="T293">tank-PL.[NOM]</ta>
            <ta e="T295" id="Seg_7500" s="T294">especially</ta>
            <ta e="T296" id="Seg_7501" s="T295">war-DAT/LOC</ta>
            <ta e="T297" id="Seg_7502" s="T296">be.made-EP-PTCP.PST</ta>
            <ta e="T298" id="Seg_7503" s="T297">fire-PROPR</ta>
            <ta e="T299" id="Seg_7504" s="T298">weapon-PL.[NOM]</ta>
            <ta e="T300" id="Seg_7505" s="T299">Katyusha.[NOM]</ta>
            <ta e="T301" id="Seg_7506" s="T300">Andryusha.[NOM]</ta>
            <ta e="T302" id="Seg_7507" s="T301">say-CVB.SEQ</ta>
            <ta e="T303" id="Seg_7508" s="T302">name-PROPR-PL.[NOM]</ta>
            <ta e="T304" id="Seg_7509" s="T303">then</ta>
            <ta e="T305" id="Seg_7510" s="T304">individually</ta>
            <ta e="T306" id="Seg_7511" s="T305">how.much</ta>
            <ta e="T307" id="Seg_7512" s="T306">INDEF</ta>
            <ta e="T308" id="Seg_7513" s="T307">small</ta>
            <ta e="T309" id="Seg_7514" s="T308">rifle-PL.[NOM]</ta>
            <ta e="T310" id="Seg_7515" s="T309">that.[NOM]</ta>
            <ta e="T311" id="Seg_7516" s="T310">every-3SG.[NOM]</ta>
            <ta e="T312" id="Seg_7517" s="T311">together</ta>
            <ta e="T313" id="Seg_7518" s="T312">beat-PST1-3PL</ta>
            <ta e="T314" id="Seg_7519" s="T313">German-PL.[NOM]</ta>
            <ta e="T315" id="Seg_7520" s="T314">there.is</ta>
            <ta e="T316" id="Seg_7521" s="T315">place-3PL-ACC</ta>
            <ta e="T317" id="Seg_7522" s="T316">german</ta>
            <ta e="T318" id="Seg_7523" s="T317">army-3SG.[NOM]</ta>
            <ta e="T319" id="Seg_7524" s="T318">that-ACC</ta>
            <ta e="T320" id="Seg_7525" s="T319">wave-PROPR</ta>
            <ta e="T321" id="Seg_7526" s="T320">big</ta>
            <ta e="T322" id="Seg_7527" s="T321">beat-NMNZ-ACC</ta>
            <ta e="T323" id="Seg_7528" s="T322">bear-NEG.CVB.SIM</ta>
            <ta e="T324" id="Seg_7529" s="T323">back</ta>
            <ta e="T325" id="Seg_7530" s="T324">break.through-PST2-3SG</ta>
            <ta e="T326" id="Seg_7531" s="T325">immediately-INTNS</ta>
            <ta e="T327" id="Seg_7532" s="T326">ten</ta>
            <ta e="T328" id="Seg_7533" s="T327">five</ta>
            <ta e="T329" id="Seg_7534" s="T328">verst-PROPR</ta>
            <ta e="T330" id="Seg_7535" s="T329">place-ACC</ta>
            <ta e="T331" id="Seg_7536" s="T330">back</ta>
            <ta e="T332" id="Seg_7537" s="T331">jump-PST2-3SG</ta>
            <ta e="T333" id="Seg_7538" s="T332">that.[NOM]</ta>
            <ta e="T334" id="Seg_7539" s="T333">big</ta>
            <ta e="T335" id="Seg_7540" s="T334">beat-NMNZ.[NOM]</ta>
            <ta e="T336" id="Seg_7541" s="T335">back-3SG-ABL</ta>
            <ta e="T337" id="Seg_7542" s="T336">Kursk.[NOM]</ta>
            <ta e="T338" id="Seg_7543" s="T337">say-CVB.SEQ</ta>
            <ta e="T339" id="Seg_7544" s="T338">curved-3SG.[NOM]</ta>
            <ta e="T340" id="Seg_7545" s="T339">very</ta>
            <ta e="T341" id="Seg_7546" s="T340">get.better-PST2-3SG</ta>
            <ta e="T342" id="Seg_7547" s="T341">german</ta>
            <ta e="T343" id="Seg_7548" s="T342">army-3SG-ACC</ta>
            <ta e="T344" id="Seg_7549" s="T343">back</ta>
            <ta e="T345" id="Seg_7550" s="T344">1PL.[NOM]</ta>
            <ta e="T346" id="Seg_7551" s="T345">chase-PST2-1PL</ta>
            <ta e="T347" id="Seg_7552" s="T346">that.[NOM]</ta>
            <ta e="T348" id="Seg_7553" s="T347">back-3SG-ABL</ta>
            <ta e="T349" id="Seg_7554" s="T348">German-PL.[NOM]</ta>
            <ta e="T350" id="Seg_7555" s="T349">not.good</ta>
            <ta e="T351" id="Seg_7556" s="T350">soldier-3PL.[NOM]</ta>
            <ta e="T352" id="Seg_7557" s="T351">front-3PL-ACC</ta>
            <ta e="T353" id="Seg_7558" s="T352">1PL-DAT/LOC</ta>
            <ta e="T354" id="Seg_7559" s="T353">give-CVB.SEQ</ta>
            <ta e="T355" id="Seg_7560" s="T354">after</ta>
            <ta e="T356" id="Seg_7561" s="T355">straight</ta>
            <ta e="T357" id="Seg_7562" s="T356">war.[NOM]</ta>
            <ta e="T358" id="Seg_7563" s="T357">stop-PTCP.FUT-3SG-DAT/LOC</ta>
            <ta e="T359" id="Seg_7564" s="T358">until</ta>
            <ta e="T360" id="Seg_7565" s="T359">mind-3PL-ACC-reason-3PL-ACC</ta>
            <ta e="T361" id="Seg_7566" s="T360">find-PST2.NEG-3PL</ta>
            <ta e="T362" id="Seg_7567" s="T361">flee-CVB.SEQ</ta>
            <ta e="T363" id="Seg_7568" s="T362">go-PST2-3PL</ta>
            <ta e="T364" id="Seg_7569" s="T363">1PL.[NOM]</ta>
            <ta e="T365" id="Seg_7570" s="T364">army-1PL.[NOM]</ta>
            <ta e="T366" id="Seg_7571" s="T365">very</ta>
            <ta e="T367" id="Seg_7572" s="T366">good-ADVZ</ta>
            <ta e="T368" id="Seg_7573" s="T367">power-3SG-ACC</ta>
            <ta e="T369" id="Seg_7574" s="T368">grab-CVB.SEQ</ta>
            <ta e="T370" id="Seg_7575" s="T369">after</ta>
            <ta e="T371" id="Seg_7576" s="T370">German-PL-ACC</ta>
            <ta e="T372" id="Seg_7577" s="T371">back.[NOM]</ta>
            <ta e="T373" id="Seg_7578" s="T372">to</ta>
            <ta e="T374" id="Seg_7579" s="T373">show-NEG.CVB.SIM</ta>
            <ta e="T375" id="Seg_7580" s="T374">enough</ta>
            <ta e="T376" id="Seg_7581" s="T375">chase-PST2-3SG</ta>
            <ta e="T377" id="Seg_7582" s="T376">1SG.[NOM]</ta>
            <ta e="T378" id="Seg_7583" s="T377">late-ADJZ</ta>
            <ta e="T379" id="Seg_7584" s="T378">day-PL-DAT/LOC</ta>
            <ta e="T380" id="Seg_7585" s="T379">war-DAT/LOC</ta>
            <ta e="T381" id="Seg_7586" s="T380">go-CVB.SEQ-1SG</ta>
            <ta e="T382" id="Seg_7587" s="T381">such</ta>
            <ta e="T383" id="Seg_7588" s="T382">horror-PROPR</ta>
            <ta e="T384" id="Seg_7589" s="T383">human.being.[NOM]</ta>
            <ta e="T385" id="Seg_7590" s="T952">similar</ta>
            <ta e="T387" id="Seg_7591" s="T385">see-PST2.NEG-EP-1SG</ta>
            <ta e="T388" id="Seg_7592" s="T387">1SG.[NOM]</ta>
            <ta e="T389" id="Seg_7593" s="T388">that</ta>
            <ta e="T390" id="Seg_7594" s="T389">place-DAT/LOC</ta>
            <ta e="T391" id="Seg_7595" s="T390">for.the.first.time</ta>
            <ta e="T392" id="Seg_7596" s="T391">see-PST2-EP-1SG</ta>
            <ta e="T393" id="Seg_7597" s="T392">heat-PROPR</ta>
            <ta e="T394" id="Seg_7598" s="T393">fight-ACC</ta>
            <ta e="T395" id="Seg_7599" s="T394">how</ta>
            <ta e="T396" id="Seg_7600" s="T395">war.[NOM]</ta>
            <ta e="T397" id="Seg_7601" s="T396">human.being-3SG-GEN</ta>
            <ta e="T398" id="Seg_7602" s="T397">blood-3SG.[NOM]</ta>
            <ta e="T399" id="Seg_7603" s="T398">river-SIM</ta>
            <ta e="T400" id="Seg_7604" s="T399">pour.out-MED-PTCP.PRS-3SG-ACC</ta>
            <ta e="T401" id="Seg_7605" s="T400">so.much</ta>
            <ta e="T402" id="Seg_7606" s="T401">thousand</ta>
            <ta e="T403" id="Seg_7607" s="T402">human.being.[NOM]</ta>
            <ta e="T404" id="Seg_7608" s="T403">breath-3SG.[NOM]</ta>
            <ta e="T405" id="Seg_7609" s="T404">come.to.an.end-PTCP.PRS-3SG-ACC</ta>
            <ta e="T406" id="Seg_7610" s="T405">so.much</ta>
            <ta e="T407" id="Seg_7611" s="T406">wound.[NOM]</ta>
            <ta e="T408" id="Seg_7612" s="T407">become-PTCP.PST</ta>
            <ta e="T409" id="Seg_7613" s="T408">human.being.[NOM]</ta>
            <ta e="T410" id="Seg_7614" s="T409">big-ADVZ</ta>
            <ta e="T411" id="Seg_7615" s="T410">pain-VBZ-PTCP.PRS-3SG-ACC</ta>
            <ta e="T412" id="Seg_7616" s="T411">1SG.[NOM]</ta>
            <ta e="T413" id="Seg_7617" s="T412">also</ta>
            <ta e="T414" id="Seg_7618" s="T413">that</ta>
            <ta e="T415" id="Seg_7619" s="T414">beat-EP-RECP/COLL-NMNZ-DAT/LOC</ta>
            <ta e="T416" id="Seg_7620" s="T415">Kursk.[NOM]</ta>
            <ta e="T417" id="Seg_7621" s="T416">promontory-3SG-DAT/LOC</ta>
            <ta e="T418" id="Seg_7622" s="T417">unit.[NOM]</ta>
            <ta e="T419" id="Seg_7623" s="T418">commander-3SG.[NOM]</ta>
            <ta e="T420" id="Seg_7624" s="T419">be-CVB.SIM</ta>
            <ta e="T421" id="Seg_7625" s="T420">go-CVB.SEQ-1SG</ta>
            <ta e="T422" id="Seg_7626" s="T421">easy-ADVZ</ta>
            <ta e="T423" id="Seg_7627" s="T422">left</ta>
            <ta e="T424" id="Seg_7628" s="T423">leg-1SG-GEN</ta>
            <ta e="T425" id="Seg_7629" s="T424">muscle-ACC</ta>
            <ta e="T426" id="Seg_7630" s="T425">wound-VBZ-EP-CAUS-PST2-EP-1SG</ta>
            <ta e="T427" id="Seg_7631" s="T426">that.[NOM]</ta>
            <ta e="T428" id="Seg_7632" s="T427">be-CVB.SEQ</ta>
            <ta e="T429" id="Seg_7633" s="T428">after</ta>
            <ta e="T430" id="Seg_7634" s="T429">fight-PTCP.PRS</ta>
            <ta e="T431" id="Seg_7635" s="T430">place-ABL</ta>
            <ta e="T432" id="Seg_7636" s="T431">go.out-EP-PST2.NEG-EP-1SG</ta>
            <ta e="T433" id="Seg_7637" s="T432">that</ta>
            <ta e="T434" id="Seg_7638" s="T433">earth-PROPR</ta>
            <ta e="T435" id="Seg_7639" s="T434">beat-EP-RECP/COLL-NMNZ-DAT/LOC</ta>
            <ta e="T436" id="Seg_7640" s="T435">1SG.[NOM]</ta>
            <ta e="T437" id="Seg_7641" s="T436">there.is</ta>
            <ta e="T438" id="Seg_7642" s="T437">only</ta>
            <ta e="T439" id="Seg_7643" s="T438">part-EP-1SG.[NOM]</ta>
            <ta e="T440" id="Seg_7644" s="T439">thousand-ABL</ta>
            <ta e="T441" id="Seg_7645" s="T440">over</ta>
            <ta e="T442" id="Seg_7646" s="T441">war.[NOM]</ta>
            <ta e="T443" id="Seg_7647" s="T442">human.being-3SG-ABL</ta>
            <ta e="T444" id="Seg_7648" s="T443">that</ta>
            <ta e="T445" id="Seg_7649" s="T444">ten</ta>
            <ta e="T446" id="Seg_7650" s="T445">day-DAT/LOC</ta>
            <ta e="T447" id="Seg_7651" s="T446">beat-EP-RECP/COLL-NMNZ-DAT/LOC</ta>
            <ta e="T448" id="Seg_7652" s="T447">remain-PST2-3SG</ta>
            <ta e="T449" id="Seg_7653" s="T448">ten</ta>
            <ta e="T450" id="Seg_7654" s="T449">one</ta>
            <ta e="T451" id="Seg_7655" s="T450">only</ta>
            <ta e="T452" id="Seg_7656" s="T451">human.being.[NOM]</ta>
            <ta e="T453" id="Seg_7657" s="T452">that.[NOM]</ta>
            <ta e="T454" id="Seg_7658" s="T453">inside-3SG-DAT/LOC</ta>
            <ta e="T455" id="Seg_7659" s="T454">still</ta>
            <ta e="T456" id="Seg_7660" s="T455">one</ta>
            <ta e="T457" id="Seg_7661" s="T456">1SG.[NOM]</ta>
            <ta e="T458" id="Seg_7662" s="T457">wound.[NOM]</ta>
            <ta e="T459" id="Seg_7663" s="T458">become-PTCP.PST</ta>
            <ta e="T460" id="Seg_7664" s="T459">human.being.[NOM]</ta>
            <ta e="T461" id="Seg_7665" s="T460">there.is</ta>
            <ta e="T462" id="Seg_7666" s="T461">be-PST1-1SG</ta>
            <ta e="T463" id="Seg_7667" s="T462">that-ABL</ta>
            <ta e="T464" id="Seg_7668" s="T463">right.[NOM]</ta>
            <ta e="T465" id="Seg_7669" s="T464">be-FUT-3SG</ta>
            <ta e="T466" id="Seg_7670" s="T465">ascertain-PTCP.FUT-DAT/LOC</ta>
            <ta e="T467" id="Seg_7671" s="T466">how</ta>
            <ta e="T468" id="Seg_7672" s="T467">misfortune-PROPR.[NOM]</ta>
            <ta e="T469" id="Seg_7673" s="T468">war.[NOM]</ta>
            <ta e="T470" id="Seg_7674" s="T469">be-PTCP.PST-3SG-ACC</ta>
            <ta e="T471" id="Seg_7675" s="T470">Kursk.[NOM]</ta>
            <ta e="T472" id="Seg_7676" s="T471">curved-3SG-DAT/LOC</ta>
            <ta e="T473" id="Seg_7677" s="T472">how.much</ta>
            <ta e="T474" id="Seg_7678" s="T473">blood.[NOM]</ta>
            <ta e="T475" id="Seg_7679" s="T474">pour.out-MED-EP-PTCP.PST-3SG-ACC</ta>
            <ta e="T476" id="Seg_7680" s="T475">how.much</ta>
            <ta e="T477" id="Seg_7681" s="T476">human.being.[NOM]</ta>
            <ta e="T478" id="Seg_7682" s="T477">die-PTCP.PST-3SG-ACC</ta>
            <ta e="T479" id="Seg_7683" s="T478">Kursk-DAT/LOC</ta>
            <ta e="T480" id="Seg_7684" s="T479">stop-CVB.SEQ</ta>
            <ta e="T481" id="Seg_7685" s="T480">after</ta>
            <ta e="T482" id="Seg_7686" s="T481">1PL.[NOM]</ta>
            <ta e="T483" id="Seg_7687" s="T482">army-1PL.[NOM]</ta>
            <ta e="T484" id="Seg_7688" s="T483">back</ta>
            <ta e="T485" id="Seg_7689" s="T484">escape-PTCP.PRS</ta>
            <ta e="T486" id="Seg_7690" s="T485">German-ACC</ta>
            <ta e="T487" id="Seg_7691" s="T486">west.[NOM]</ta>
            <ta e="T488" id="Seg_7692" s="T487">to</ta>
            <ta e="T489" id="Seg_7693" s="T488">chase-CVB.SEQ</ta>
            <ta e="T490" id="Seg_7694" s="T489">go-PST2-3SG</ta>
            <ta e="T491" id="Seg_7695" s="T490">that</ta>
            <ta e="T492" id="Seg_7696" s="T491">Kursk.[NOM]</ta>
            <ta e="T493" id="Seg_7697" s="T492">back-3SG-ABL</ta>
            <ta e="T494" id="Seg_7698" s="T493">1SG.[NOM]</ta>
            <ta e="T495" id="Seg_7699" s="T494">most</ta>
            <ta e="T496" id="Seg_7700" s="T495">war.[NOM]</ta>
            <ta e="T497" id="Seg_7701" s="T496">fight.[NOM]</ta>
            <ta e="T498" id="Seg_7702" s="T497">place-3SG-DAT/LOC</ta>
            <ta e="T499" id="Seg_7703" s="T498">there.is</ta>
            <ta e="T500" id="Seg_7704" s="T499">be-PST1-1SG</ta>
            <ta e="T501" id="Seg_7705" s="T500">thousand</ta>
            <ta e="T502" id="Seg_7706" s="T501">nine</ta>
            <ta e="T503" id="Seg_7707" s="T502">hundred</ta>
            <ta e="T504" id="Seg_7708" s="T503">four-ten</ta>
            <ta e="T505" id="Seg_7709" s="T504">four</ta>
            <ta e="T506" id="Seg_7710" s="T505">year-PROPR-DAT/LOC</ta>
            <ta e="T507" id="Seg_7711" s="T506">winter-ADJZ</ta>
            <ta e="T508" id="Seg_7712" s="T507">January.[NOM]</ta>
            <ta e="T509" id="Seg_7713" s="T508">month-DAT/LOC</ta>
            <ta e="T510" id="Seg_7714" s="T509">until</ta>
            <ta e="T511" id="Seg_7715" s="T510">that</ta>
            <ta e="T512" id="Seg_7716" s="T511">time-DAT/LOC</ta>
            <ta e="T513" id="Seg_7717" s="T512">german</ta>
            <ta e="T514" id="Seg_7718" s="T513">soldier-3PL-ACC</ta>
            <ta e="T515" id="Seg_7719" s="T514">with</ta>
            <ta e="T516" id="Seg_7720" s="T515">towards</ta>
            <ta e="T517" id="Seg_7721" s="T516">stand-CVB.SEQ</ta>
            <ta e="T518" id="Seg_7722" s="T517">fight-PST2-EP-1SG</ta>
            <ta e="T519" id="Seg_7723" s="T518">such</ta>
            <ta e="T520" id="Seg_7724" s="T519">place-PL-DAT/LOC</ta>
            <ta e="T521" id="Seg_7725" s="T520">Belgorod.[NOM]</ta>
            <ta e="T522" id="Seg_7726" s="T521">then</ta>
            <ta e="T523" id="Seg_7727" s="T522">Kharkov.[NOM]</ta>
            <ta e="T524" id="Seg_7728" s="T523">then</ta>
            <ta e="T525" id="Seg_7729" s="T524">Poltava.[NOM]</ta>
            <ta e="T526" id="Seg_7730" s="T525">then</ta>
            <ta e="T527" id="Seg_7731" s="T526">Kremenchug.[NOM]</ta>
            <ta e="T528" id="Seg_7732" s="T527">then</ta>
            <ta e="T529" id="Seg_7733" s="T528">Kryukov.[NOM]</ta>
            <ta e="T530" id="Seg_7734" s="T529">say-CVB.SEQ</ta>
            <ta e="T531" id="Seg_7735" s="T530">name-PROPR</ta>
            <ta e="T532" id="Seg_7736" s="T531">big</ta>
            <ta e="T533" id="Seg_7737" s="T532">city-PL-ACC</ta>
            <ta e="T534" id="Seg_7738" s="T533">take-PTCP.PRS-DAT/LOC</ta>
            <ta e="T535" id="Seg_7739" s="T534">Ukrainian</ta>
            <ta e="T536" id="Seg_7740" s="T535">say-CVB.SEQ</ta>
            <ta e="T537" id="Seg_7741" s="T536">people.[NOM]</ta>
            <ta e="T538" id="Seg_7742" s="T537">earth-3SG-DAT/LOC</ta>
            <ta e="T539" id="Seg_7743" s="T538">that</ta>
            <ta e="T540" id="Seg_7744" s="T539">city-PL-ABL</ta>
            <ta e="T541" id="Seg_7745" s="T540">very</ta>
            <ta e="T542" id="Seg_7746" s="T541">difficult-ADVZ</ta>
            <ta e="T543" id="Seg_7747" s="T542">1PL.[NOM]</ta>
            <ta e="T544" id="Seg_7748" s="T543">fight-PST2-1PL</ta>
            <ta e="T545" id="Seg_7749" s="T544">German-ACC</ta>
            <ta e="T546" id="Seg_7750" s="T545">with</ta>
            <ta e="T547" id="Seg_7751" s="T546">Kharkov.[NOM]</ta>
            <ta e="T548" id="Seg_7752" s="T547">city.[NOM]</ta>
            <ta e="T549" id="Seg_7753" s="T548">take.away-EP-RECP/COLL-NMNZ-DAT/LOC</ta>
            <ta e="T550" id="Seg_7754" s="T549">that</ta>
            <ta e="T551" id="Seg_7755" s="T550">city.[NOM]</ta>
            <ta e="T552" id="Seg_7756" s="T551">three-DAT/LOC</ta>
            <ta e="T553" id="Seg_7757" s="T552">until</ta>
            <ta e="T554" id="Seg_7758" s="T553">forth.and.back</ta>
            <ta e="T555" id="Seg_7759" s="T554">hand-ABL</ta>
            <ta e="T556" id="Seg_7760" s="T555">hand-DAT/LOC</ta>
            <ta e="T557" id="Seg_7761" s="T556">come-CVB.SIM</ta>
            <ta e="T558" id="Seg_7762" s="T557">go-EP-PST2-3SG</ta>
            <ta e="T559" id="Seg_7763" s="T558">then</ta>
            <ta e="T560" id="Seg_7764" s="T559">three-ORD</ta>
            <ta e="T561" id="Seg_7765" s="T560">come-NMNZ-3SG-DAT/LOC</ta>
            <ta e="T562" id="Seg_7766" s="T561">1PL.[NOM]</ta>
            <ta e="T563" id="Seg_7767" s="T562">hand-1PL-ABL</ta>
            <ta e="T564" id="Seg_7768" s="T563">release-PST2.NEG-1PL</ta>
            <ta e="T565" id="Seg_7769" s="T564">then</ta>
            <ta e="T566" id="Seg_7770" s="T565">big</ta>
            <ta e="T567" id="Seg_7771" s="T566">beat-EP-RECP/COLL-NMNZ.[NOM]</ta>
            <ta e="T568" id="Seg_7772" s="T567">still</ta>
            <ta e="T569" id="Seg_7773" s="T568">there.is</ta>
            <ta e="T570" id="Seg_7774" s="T569">be-PST1-3SG</ta>
            <ta e="T571" id="Seg_7775" s="T570">Dnyepr.[NOM]</ta>
            <ta e="T572" id="Seg_7776" s="T571">say-CVB.SEQ</ta>
            <ta e="T573" id="Seg_7777" s="T572">name-PROPR</ta>
            <ta e="T574" id="Seg_7778" s="T573">river.[NOM]</ta>
            <ta e="T575" id="Seg_7779" s="T574">move.on.water-NMNZ-3SG-DAT/LOC</ta>
            <ta e="T576" id="Seg_7780" s="T575">that</ta>
            <ta e="T577" id="Seg_7781" s="T576">river.[NOM]</ta>
            <ta e="T578" id="Seg_7782" s="T577">on.the.other.shore-ADJZ</ta>
            <ta e="T579" id="Seg_7783" s="T578">side-3SG-DAT/LOC</ta>
            <ta e="T580" id="Seg_7784" s="T579">sit-CVB.SEQ</ta>
            <ta e="T581" id="Seg_7785" s="T580">German-PL.[NOM]</ta>
            <ta e="T582" id="Seg_7786" s="T581">day-PL-ACC-night-PL-ACC</ta>
            <ta e="T583" id="Seg_7787" s="T582">guard-CVB.SIM</ta>
            <ta e="T584" id="Seg_7788" s="T583">go-CVB.SEQ-3PL</ta>
            <ta e="T585" id="Seg_7789" s="T584">1PL-ACC</ta>
            <ta e="T586" id="Seg_7790" s="T585">at.all</ta>
            <ta e="T587" id="Seg_7791" s="T586">can-CVB.SEQ</ta>
            <ta e="T588" id="Seg_7792" s="T587">move.on.water-EP-CAUS.[CAUS]-NEG.PTCP</ta>
            <ta e="T589" id="Seg_7793" s="T588">be-PST1-3PL</ta>
            <ta e="T590" id="Seg_7794" s="T589">that.[NOM]</ta>
            <ta e="T591" id="Seg_7795" s="T590">because.of</ta>
            <ta e="T592" id="Seg_7796" s="T591">1PL.[NOM]</ta>
            <ta e="T593" id="Seg_7797" s="T592">how.much</ta>
            <ta e="T594" id="Seg_7798" s="T593">INDEF</ta>
            <ta e="T595" id="Seg_7799" s="T594">day-ACC</ta>
            <ta e="T596" id="Seg_7800" s="T595">make.it-CVB.SEQ</ta>
            <ta e="T597" id="Seg_7801" s="T596">go.out-EP-PST2.NEG-1PL</ta>
            <ta e="T598" id="Seg_7802" s="T597">that.side-ADJZ</ta>
            <ta e="T599" id="Seg_7803" s="T598">shore-DAT/LOC</ta>
            <ta e="T600" id="Seg_7804" s="T599">sit-PST2-1PL</ta>
            <ta e="T601" id="Seg_7805" s="T600">river-ACC</ta>
            <ta e="T602" id="Seg_7806" s="T601">go.out-PTCP.PRS</ta>
            <ta e="T603" id="Seg_7807" s="T602">bridge-PL-ACC</ta>
            <ta e="T604" id="Seg_7808" s="T603">by.day</ta>
            <ta e="T605" id="Seg_7809" s="T604">make-PTCP.PRS-1PL-ACC</ta>
            <ta e="T606" id="Seg_7810" s="T605">German-PL.[NOM]</ta>
            <ta e="T607" id="Seg_7811" s="T606">airplane-3PL.[NOM]</ta>
            <ta e="T608" id="Seg_7812" s="T607">fly-CVB.SEQ</ta>
            <ta e="T609" id="Seg_7813" s="T608">come-CVB.SIM-come-CVB.SIM</ta>
            <ta e="T610" id="Seg_7814" s="T609">how.much</ta>
            <ta e="T611" id="Seg_7815" s="T610">INDEF</ta>
            <ta e="T612" id="Seg_7816" s="T611">bomb-PL-ACC</ta>
            <ta e="T613" id="Seg_7817" s="T612">throw-FREQ-CVB.SEQ</ta>
            <ta e="T614" id="Seg_7818" s="T613">enough</ta>
            <ta e="T615" id="Seg_7819" s="T614">break-PTCP.PRS</ta>
            <ta e="T616" id="Seg_7820" s="T615">be-PST1-3SG</ta>
            <ta e="T617" id="Seg_7821" s="T616">that.[NOM]</ta>
            <ta e="T618" id="Seg_7822" s="T617">because.of</ta>
            <ta e="T619" id="Seg_7823" s="T618">later</ta>
            <ta e="T620" id="Seg_7824" s="T619">1PL.[NOM]</ta>
            <ta e="T621" id="Seg_7825" s="T620">at.night</ta>
            <ta e="T622" id="Seg_7826" s="T621">some</ta>
            <ta e="T623" id="Seg_7827" s="T622">place-EP-INSTR</ta>
            <ta e="T624" id="Seg_7828" s="T623">bridge-PL-ACC</ta>
            <ta e="T625" id="Seg_7829" s="T624">make-ITER-PTCP.PRS</ta>
            <ta e="T626" id="Seg_7830" s="T625">be-PST1-1PL</ta>
            <ta e="T627" id="Seg_7831" s="T626">evil.spirit.[NOM]</ta>
            <ta e="T628" id="Seg_7832" s="T627">German.[NOM]</ta>
            <ta e="T629" id="Seg_7833" s="T628">at.night</ta>
            <ta e="T630" id="Seg_7834" s="T629">and</ta>
            <ta e="T631" id="Seg_7835" s="T630">make-PTCP.PST</ta>
            <ta e="T632" id="Seg_7836" s="T631">bridge-PL-1PL-ACC</ta>
            <ta e="T633" id="Seg_7837" s="T632">stand-CAUS-NEG.PTCP</ta>
            <ta e="T634" id="Seg_7838" s="T633">be-PST1-3SG</ta>
            <ta e="T635" id="Seg_7839" s="T634">then</ta>
            <ta e="T636" id="Seg_7840" s="T635">1PL.[NOM]</ta>
            <ta e="T637" id="Seg_7841" s="T636">side-PL.[NOM]</ta>
            <ta e="T638" id="Seg_7842" s="T637">enough</ta>
            <ta e="T639" id="Seg_7843" s="T638">get.angry-CVB.SEQ</ta>
            <ta e="T640" id="Seg_7844" s="T639">after</ta>
            <ta e="T641" id="Seg_7845" s="T640">every</ta>
            <ta e="T642" id="Seg_7846" s="T641">war.[NOM]</ta>
            <ta e="T643" id="Seg_7847" s="T642">weapon-3SG-INSTR</ta>
            <ta e="T644" id="Seg_7848" s="T643">enough</ta>
            <ta e="T645" id="Seg_7849" s="T644">shoot-FREQ-CVB.SEQ</ta>
            <ta e="T646" id="Seg_7850" s="T645">after</ta>
            <ta e="T647" id="Seg_7851" s="T646">how.much</ta>
            <ta e="T648" id="Seg_7852" s="T647">INDEF</ta>
            <ta e="T649" id="Seg_7853" s="T648">ten-DISTR</ta>
            <ta e="T650" id="Seg_7854" s="T649">airplane-PL-ACC</ta>
            <ta e="T651" id="Seg_7855" s="T650">on.the.other.shore-ADJZ</ta>
            <ta e="T652" id="Seg_7856" s="T651">side-3SG-DAT/LOC</ta>
            <ta e="T653" id="Seg_7857" s="T652">send-CVB.SEQ</ta>
            <ta e="T654" id="Seg_7858" s="T653">after</ta>
            <ta e="T655" id="Seg_7859" s="T654">that-ACC</ta>
            <ta e="T656" id="Seg_7860" s="T655">whole-3SG-ACC</ta>
            <ta e="T657" id="Seg_7861" s="T656">cover-REFL-CVB.SEQ</ta>
            <ta e="T658" id="Seg_7862" s="T657">stand-CVB.SEQ</ta>
            <ta e="T659" id="Seg_7863" s="T658">on.the.other.shore-ADJZ.[NOM]</ta>
            <ta e="T660" id="Seg_7864" s="T659">to</ta>
            <ta e="T661" id="Seg_7865" s="T660">cross-CVB.SIM</ta>
            <ta e="T662" id="Seg_7866" s="T661">struggle-RECP/COLL-CVB.SIM</ta>
            <ta e="T663" id="Seg_7867" s="T662">go.out-EP-PST2-3PL</ta>
            <ta e="T664" id="Seg_7868" s="T663">1PL.[NOM]</ta>
            <ta e="T665" id="Seg_7869" s="T664">soldier-PL-1PL.[NOM]</ta>
            <ta e="T666" id="Seg_7870" s="T665">that</ta>
            <ta e="T667" id="Seg_7871" s="T666">dispute.[NOM]</ta>
            <ta e="T668" id="Seg_7872" s="T667">noise-3SG-DAT/LOC</ta>
            <ta e="T669" id="Seg_7873" s="T668">bridge-DAT/LOC</ta>
            <ta e="T670" id="Seg_7874" s="T669">fit-NEG.CVB.SIM</ta>
            <ta e="T671" id="Seg_7875" s="T670">other.of.two</ta>
            <ta e="T672" id="Seg_7876" s="T671">human.being.[NOM]</ta>
            <ta e="T673" id="Seg_7877" s="T672">boat-INSTR</ta>
            <ta e="T674" id="Seg_7878" s="T673">some-PL.[NOM]</ta>
            <ta e="T675" id="Seg_7879" s="T674">how.much-DISTR</ta>
            <ta e="T676" id="Seg_7880" s="T675">again</ta>
            <ta e="T677" id="Seg_7881" s="T676">log-PL-ACC</ta>
            <ta e="T678" id="Seg_7882" s="T677">together</ta>
            <ta e="T679" id="Seg_7883" s="T678">connect-CVB.SIM</ta>
            <ta e="T680" id="Seg_7884" s="T679">tie-CVB.SEQ</ta>
            <ta e="T681" id="Seg_7885" s="T680">after</ta>
            <ta e="T682" id="Seg_7886" s="T681">other.of.two-PL.[NOM]</ta>
            <ta e="T683" id="Seg_7887" s="T682">freely</ta>
            <ta e="T684" id="Seg_7888" s="T683">swim-CVB.SEQ</ta>
            <ta e="T685" id="Seg_7889" s="T684">go.out-EP-PST2-3PL</ta>
            <ta e="T686" id="Seg_7890" s="T685">Dnyepr.[NOM]</ta>
            <ta e="T687" id="Seg_7891" s="T686">say-CVB.SEQ</ta>
            <ta e="T688" id="Seg_7892" s="T687">river-ACC</ta>
            <ta e="T689" id="Seg_7893" s="T688">there</ta>
            <ta e="T690" id="Seg_7894" s="T689">noise.[NOM]</ta>
            <ta e="T691" id="Seg_7895" s="T690">big</ta>
            <ta e="T692" id="Seg_7896" s="T691">shout.[NOM]</ta>
            <ta e="T693" id="Seg_7897" s="T692">scream.[NOM]</ta>
            <ta e="T694" id="Seg_7898" s="T693">fight.[NOM]</ta>
            <ta e="T695" id="Seg_7899" s="T694">what-ABL</ta>
            <ta e="T696" id="Seg_7900" s="T695">NEG</ta>
            <ta e="T697" id="Seg_7901" s="T696">different</ta>
            <ta e="T698" id="Seg_7902" s="T697">war.[NOM]</ta>
            <ta e="T699" id="Seg_7903" s="T698">weapon-3SG-GEN</ta>
            <ta e="T700" id="Seg_7904" s="T699">noise-3SG.[NOM]</ta>
            <ta e="T701" id="Seg_7905" s="T700">horror-PROPR.[NOM]</ta>
            <ta e="T702" id="Seg_7906" s="T701">be-PST1-3SG</ta>
            <ta e="T703" id="Seg_7907" s="T702">German.[NOM]</ta>
            <ta e="T704" id="Seg_7908" s="T703">in.the.direction</ta>
            <ta e="T705" id="Seg_7909" s="T704">side.[NOM]</ta>
            <ta e="T706" id="Seg_7910" s="T705">that</ta>
            <ta e="T707" id="Seg_7911" s="T706">defeat-CVB.SIM</ta>
            <ta e="T708" id="Seg_7912" s="T707">struggle-NMNZ-ACC</ta>
            <ta e="T709" id="Seg_7913" s="T708">make.it-CVB.SEQ</ta>
            <ta e="T710" id="Seg_7914" s="T709">hold-NEG.CVB.SIM</ta>
            <ta e="T711" id="Seg_7915" s="T710">in.vain</ta>
            <ta e="T712" id="Seg_7916" s="T711">river.[NOM]</ta>
            <ta e="T713" id="Seg_7917" s="T712">upper.part-3SG-INSTR</ta>
            <ta e="T714" id="Seg_7918" s="T713">shoot-FREQ-PTCP.PRS</ta>
            <ta e="T715" id="Seg_7919" s="T714">be-PST1-3SG</ta>
            <ta e="T716" id="Seg_7920" s="T715">shore-ABL</ta>
            <ta e="T717" id="Seg_7921" s="T716">see-PTCP.COND-DAT/LOC</ta>
            <ta e="T718" id="Seg_7922" s="T717">river.[NOM]</ta>
            <ta e="T719" id="Seg_7923" s="T718">to</ta>
            <ta e="T720" id="Seg_7924" s="T719">water-DAT/LOC</ta>
            <ta e="T721" id="Seg_7925" s="T720">completely</ta>
            <ta e="T722" id="Seg_7926" s="T721">human.being.[NOM]</ta>
            <ta e="T723" id="Seg_7927" s="T722">what-ACC</ta>
            <ta e="T724" id="Seg_7928" s="T723">NEG</ta>
            <ta e="T725" id="Seg_7929" s="T724">understand-PTCP.FUT</ta>
            <ta e="T726" id="Seg_7930" s="T725">be-NEG.PTCP.PST</ta>
            <ta e="T727" id="Seg_7931" s="T726">be-PST1-3SG</ta>
            <ta e="T728" id="Seg_7932" s="T727">example-3SG.[NOM]</ta>
            <ta e="T729" id="Seg_7933" s="T728">be.afraid-EP-PTCP.PST</ta>
            <ta e="T730" id="Seg_7934" s="T729">big</ta>
            <ta e="T731" id="Seg_7935" s="T730">wild.reindeer.[NOM]</ta>
            <ta e="T732" id="Seg_7936" s="T731">herd-3SG.[NOM]</ta>
            <ta e="T733" id="Seg_7937" s="T732">swim-PTCP.PRS-3SG-ACC</ta>
            <ta e="T734" id="Seg_7938" s="T733">similar</ta>
            <ta e="T735" id="Seg_7939" s="T734">be-PST1-3SG</ta>
            <ta e="T736" id="Seg_7940" s="T735">that</ta>
            <ta e="T737" id="Seg_7941" s="T736">river.[NOM]</ta>
            <ta e="T738" id="Seg_7942" s="T737">move.on.water-NMNZ-3SG-DAT/LOC</ta>
            <ta e="T739" id="Seg_7943" s="T738">so.much</ta>
            <ta e="T740" id="Seg_7944" s="T739">human.being.[NOM]</ta>
            <ta e="T741" id="Seg_7945" s="T740">choke-CVB.SEQ</ta>
            <ta e="T742" id="Seg_7946" s="T741">war.[NOM]</ta>
            <ta e="T743" id="Seg_7947" s="T742">rifle-3SG-ABL</ta>
            <ta e="T744" id="Seg_7948" s="T743">die-PTCP.PST-3SG-ACC</ta>
            <ta e="T745" id="Seg_7949" s="T744">who.[NOM]</ta>
            <ta e="T746" id="Seg_7950" s="T745">EMPH</ta>
            <ta e="T747" id="Seg_7951" s="T746">know-NEG.PTCP</ta>
            <ta e="T748" id="Seg_7952" s="T747">then</ta>
            <ta e="T749" id="Seg_7953" s="T748">be-PST1-3SG</ta>
            <ta e="T750" id="Seg_7954" s="T749">river-ACC</ta>
            <ta e="T751" id="Seg_7955" s="T750">go.out-CVB.SEQ</ta>
            <ta e="T752" id="Seg_7956" s="T751">after</ta>
            <ta e="T753" id="Seg_7957" s="T752">our.[NOM]</ta>
            <ta e="T754" id="Seg_7958" s="T753">again</ta>
            <ta e="T755" id="Seg_7959" s="T754">German-PL-ACC</ta>
            <ta e="T756" id="Seg_7960" s="T755">free.time.[NOM]</ta>
            <ta e="T757" id="Seg_7961" s="T756">find-CAUS-NEG.CVB.SIM</ta>
            <ta e="T758" id="Seg_7962" s="T757">enough</ta>
            <ta e="T759" id="Seg_7963" s="T758">chase-PST2-3PL</ta>
            <ta e="T760" id="Seg_7964" s="T759">then</ta>
            <ta e="T761" id="Seg_7965" s="T760">1SG.[NOM]</ta>
            <ta e="T762" id="Seg_7966" s="T761">how.much</ta>
            <ta e="T763" id="Seg_7967" s="T762">INDEF</ta>
            <ta e="T764" id="Seg_7968" s="T763">war-DAT/LOC</ta>
            <ta e="T765" id="Seg_7969" s="T764">go-CVB.SEQ-1SG</ta>
            <ta e="T766" id="Seg_7970" s="T765">four-ORD-PROPR</ta>
            <ta e="T767" id="Seg_7971" s="T766">day-3SG-DAT/LOC</ta>
            <ta e="T768" id="Seg_7972" s="T767">January.[NOM]</ta>
            <ta e="T769" id="Seg_7973" s="T768">month-DAT/LOC</ta>
            <ta e="T770" id="Seg_7974" s="T769">thousand</ta>
            <ta e="T771" id="Seg_7975" s="T770">nine</ta>
            <ta e="T772" id="Seg_7976" s="T771">hundred</ta>
            <ta e="T773" id="Seg_7977" s="T772">four-ten</ta>
            <ta e="T774" id="Seg_7978" s="T773">three</ta>
            <ta e="T775" id="Seg_7979" s="T774">year-PROPR-DAT/LOC</ta>
            <ta e="T776" id="Seg_7980" s="T775">last-1SG-ACC</ta>
            <ta e="T777" id="Seg_7981" s="T776">wound-VBZ-EP-CAUS-PST2-EP-1SG</ta>
            <ta e="T778" id="Seg_7982" s="T777">very</ta>
            <ta e="T779" id="Seg_7983" s="T778">heavy-ADVZ</ta>
            <ta e="T780" id="Seg_7984" s="T779">left</ta>
            <ta e="T781" id="Seg_7985" s="T780">in.the.direction</ta>
            <ta e="T782" id="Seg_7986" s="T781">hind.leg-EP-1SG.[NOM]</ta>
            <ta e="T783" id="Seg_7987" s="T782">bone-3SG-ACC</ta>
            <ta e="T784" id="Seg_7988" s="T783">enough</ta>
            <ta e="T785" id="Seg_7989" s="T784">break-CAUS-PST2-EP-1SG</ta>
            <ta e="T786" id="Seg_7990" s="T785">that-ACC</ta>
            <ta e="T787" id="Seg_7991" s="T786">heal-EP-CAUS-CVB.SEQ-1SG</ta>
            <ta e="T788" id="Seg_7992" s="T787">get.better-PTCP.FUT-1SG-DAT/LOC</ta>
            <ta e="T789" id="Seg_7993" s="T788">until</ta>
            <ta e="T790" id="Seg_7994" s="T789">six</ta>
            <ta e="T791" id="Seg_7995" s="T790">month.[NOM]</ta>
            <ta e="T792" id="Seg_7996" s="T791">as.long.as</ta>
            <ta e="T793" id="Seg_7997" s="T792">lie-PST2-EP-1SG</ta>
            <ta e="T794" id="Seg_7998" s="T793">Essentuki.[NOM]</ta>
            <ta e="T795" id="Seg_7999" s="T794">say-CVB.SEQ</ta>
            <ta e="T796" id="Seg_8000" s="T795">city-DAT/LOC</ta>
            <ta e="T797" id="Seg_8001" s="T796">Caucasus.[NOM]</ta>
            <ta e="T798" id="Seg_8002" s="T797">say-CVB.SEQ</ta>
            <ta e="T799" id="Seg_8003" s="T798">warm</ta>
            <ta e="T800" id="Seg_8004" s="T799">place-DAT/LOC</ta>
            <ta e="T801" id="Seg_8005" s="T800">then</ta>
            <ta e="T802" id="Seg_8006" s="T801">heal-EP-CAUS-CVB.SEQ</ta>
            <ta e="T803" id="Seg_8007" s="T802">after</ta>
            <ta e="T804" id="Seg_8008" s="T803">wood.[NOM]</ta>
            <ta e="T805" id="Seg_8009" s="T804">stick-PROPR.[NOM]</ta>
            <ta e="T806" id="Seg_8010" s="T805">go.out-CVB.SEQ</ta>
            <ta e="T807" id="Seg_8011" s="T806">go-CVB.SEQ-1SG</ta>
            <ta e="T808" id="Seg_8012" s="T807">war-ABL</ta>
            <ta e="T809" id="Seg_8013" s="T808">completely</ta>
            <ta e="T810" id="Seg_8014" s="T809">free-EP-MED-PST2-EP-1SG</ta>
            <ta e="T811" id="Seg_8015" s="T810">army-ABL</ta>
            <ta e="T813" id="Seg_8016" s="T812">go.out-EP-PST2-EP-1SG</ta>
            <ta e="T814" id="Seg_8017" s="T813">at.first</ta>
            <ta e="T815" id="Seg_8018" s="T814">fight-PTCP.PST</ta>
            <ta e="T816" id="Seg_8019" s="T815">day-1SG-ABL</ta>
            <ta e="T817" id="Seg_8020" s="T816">one</ta>
            <ta e="T818" id="Seg_8021" s="T817">year.[NOM]</ta>
            <ta e="T819" id="Seg_8022" s="T818">half-3SG-ACC</ta>
            <ta e="T820" id="Seg_8023" s="T819">as.long.as</ta>
            <ta e="T821" id="Seg_8024" s="T820">war.[NOM]</ta>
            <ta e="T822" id="Seg_8025" s="T821">fight-3SG-DAT/LOC</ta>
            <ta e="T823" id="Seg_8026" s="T822">go-CVB.SEQ-1SG</ta>
            <ta e="T824" id="Seg_8027" s="T823">completely</ta>
            <ta e="T825" id="Seg_8028" s="T824">four-DAT/LOC</ta>
            <ta e="T826" id="Seg_8029" s="T825">time.[NOM]</ta>
            <ta e="T827" id="Seg_8030" s="T826">wound-VBZ-EP-CAUS-PST2-EP-1SG</ta>
            <ta e="T828" id="Seg_8031" s="T827">that.[NOM]</ta>
            <ta e="T829" id="Seg_8032" s="T828">side-3SG-INSTR</ta>
            <ta e="T830" id="Seg_8033" s="T829">three-MLTP</ta>
            <ta e="T831" id="Seg_8034" s="T830">heal-EP-CAUS-CVB.SIM</ta>
            <ta e="T832" id="Seg_8035" s="T831">go-EP-PST2-EP-1SG</ta>
            <ta e="T833" id="Seg_8036" s="T832">short-ADVZ</ta>
            <ta e="T834" id="Seg_8037" s="T833">tell-PTCP.COND-DAT/LOC</ta>
            <ta e="T835" id="Seg_8038" s="T834">such</ta>
            <ta e="T836" id="Seg_8039" s="T835">1SG.[NOM]</ta>
            <ta e="T837" id="Seg_8040" s="T836">human.being.[NOM]</ta>
            <ta e="T838" id="Seg_8041" s="T837">relative.[NOM]</ta>
            <ta e="T839" id="Seg_8042" s="T838">life-3SG-DAT/LOC</ta>
            <ta e="T840" id="Seg_8043" s="T839">when</ta>
            <ta e="T841" id="Seg_8044" s="T840">NEG</ta>
            <ta e="T842" id="Seg_8045" s="T841">know-PASS/REFL-EP-NEG.PTCP.PST</ta>
            <ta e="T843" id="Seg_8046" s="T842">father.[NOM]</ta>
            <ta e="T844" id="Seg_8047" s="T843">and</ta>
            <ta e="T845" id="Seg_8048" s="T844">that</ta>
            <ta e="T846" id="Seg_8049" s="T845">big</ta>
            <ta e="T847" id="Seg_8050" s="T846">war-3SG-DAT/LOC</ta>
            <ta e="T848" id="Seg_8051" s="T847">self-1SG.[NOM]</ta>
            <ta e="T849" id="Seg_8052" s="T848">people-1SG-ACC</ta>
            <ta e="T850" id="Seg_8053" s="T849">be.born-PTCP.PST</ta>
            <ta e="T851" id="Seg_8054" s="T850">arise-PTCP.PST</ta>
            <ta e="T852" id="Seg_8055" s="T851">earth-1SG-ACC</ta>
            <ta e="T853" id="Seg_8056" s="T852">protect-CVB.SIM</ta>
            <ta e="T854" id="Seg_8057" s="T853">go-EP-PST2-EP-1SG</ta>
            <ta e="T855" id="Seg_8058" s="T854">my</ta>
            <ta e="T856" id="Seg_8059" s="T855">blood-EP-1SG.[NOM]</ta>
            <ta e="T857" id="Seg_8060" s="T856">sweat-EP-1SG.[NOM]</ta>
            <ta e="T858" id="Seg_8061" s="T857">that</ta>
            <ta e="T859" id="Seg_8062" s="T858">war-DAT/LOC</ta>
            <ta e="T860" id="Seg_8063" s="T859">in.vain</ta>
            <ta e="T861" id="Seg_8064" s="T860">pour.out-REFL-EP-PST2.NEG-3SG</ta>
            <ta e="T862" id="Seg_8065" s="T861">our</ta>
            <ta e="T863" id="Seg_8066" s="T862">communist</ta>
            <ta e="T864" id="Seg_8067" s="T863">party-1PL.[NOM]</ta>
            <ta e="T865" id="Seg_8068" s="T864">centre.[NOM]</ta>
            <ta e="T866" id="Seg_8069" s="T865">committee-3SG.[NOM]</ta>
            <ta e="T867" id="Seg_8070" s="T866">then</ta>
            <ta e="T868" id="Seg_8071" s="T867">our</ta>
            <ta e="T869" id="Seg_8072" s="T868">state-1PL-GEN</ta>
            <ta e="T870" id="Seg_8073" s="T869">life-3SG-ACC</ta>
            <ta e="T871" id="Seg_8074" s="T870">improve-PTCP.HAB</ta>
            <ta e="T872" id="Seg_8075" s="T871">government-1PL.[NOM]</ta>
            <ta e="T873" id="Seg_8076" s="T872">give-PST2-3PL</ta>
            <ta e="T874" id="Seg_8077" s="T873">1SG-DAT/LOC</ta>
            <ta e="T875" id="Seg_8078" s="T874">state.[NOM]</ta>
            <ta e="T876" id="Seg_8079" s="T875">high</ta>
            <ta e="T877" id="Seg_8080" s="T876">war-3SG-GEN</ta>
            <ta e="T878" id="Seg_8081" s="T877">medal-3SG-ACC</ta>
            <ta e="T882" id="Seg_8082" s="T881">say-CVB.SEQ</ta>
            <ta e="T883" id="Seg_8083" s="T882">then</ta>
            <ta e="T884" id="Seg_8084" s="T883">still</ta>
            <ta e="T885" id="Seg_8085" s="T884">medal-ACC</ta>
            <ta e="T890" id="Seg_8086" s="T889">say-CVB.SEQ</ta>
            <ta e="T891" id="Seg_8087" s="T890">now</ta>
            <ta e="T892" id="Seg_8088" s="T891">1PL.[NOM]</ta>
            <ta e="T893" id="Seg_8089" s="T892">Soviet</ta>
            <ta e="T894" id="Seg_8090" s="T893">life-3SG-DAT/LOC</ta>
            <ta e="T895" id="Seg_8091" s="T894">live-PTCP.PRS</ta>
            <ta e="T896" id="Seg_8092" s="T895">human.being-PL.[NOM]</ta>
            <ta e="T897" id="Seg_8093" s="T896">life-2PL.[NOM]</ta>
            <ta e="T898" id="Seg_8094" s="T897">very</ta>
            <ta e="T899" id="Seg_8095" s="T898">work-CVB.SEQ</ta>
            <ta e="T900" id="Seg_8096" s="T899">stand-PRS.[3SG]</ta>
            <ta e="T901" id="Seg_8097" s="T900">front.[NOM]</ta>
            <ta e="T902" id="Seg_8098" s="T901">to</ta>
            <ta e="T903" id="Seg_8099" s="T902">go-PTCP.PRS</ta>
            <ta e="T904" id="Seg_8100" s="T903">power-DAT/LOC</ta>
            <ta e="T905" id="Seg_8101" s="T904">that.[NOM]</ta>
            <ta e="T906" id="Seg_8102" s="T905">because.of</ta>
            <ta e="T907" id="Seg_8103" s="T906">who.[NOM]</ta>
            <ta e="T908" id="Seg_8104" s="T907">EMPH</ta>
            <ta e="T909" id="Seg_8105" s="T908">now</ta>
            <ta e="T910" id="Seg_8106" s="T909">war-2SG-ACC</ta>
            <ta e="T911" id="Seg_8107" s="T910">become-PTCP.FUT-3SG-ACC</ta>
            <ta e="T912" id="Seg_8108" s="T911">want-NEG.[3SG]</ta>
            <ta e="T913" id="Seg_8109" s="T912">1PL.[NOM]</ta>
            <ta e="T914" id="Seg_8110" s="T913">earth-1PL.[NOM]</ta>
            <ta e="T915" id="Seg_8111" s="T914">human.being-3SG.[NOM]</ta>
            <ta e="T916" id="Seg_8112" s="T915">former-ABL</ta>
            <ta e="T917" id="Seg_8113" s="T916">EMPH</ta>
            <ta e="T918" id="Seg_8114" s="T917">war-DAT/LOC</ta>
            <ta e="T919" id="Seg_8115" s="T918">want-PTCP.HAB-3SG</ta>
            <ta e="T920" id="Seg_8116" s="T919">NEG</ta>
            <ta e="T921" id="Seg_8117" s="T920">be-PST1-3SG</ta>
            <ta e="T922" id="Seg_8118" s="T921">1PL.[NOM]</ta>
            <ta e="T923" id="Seg_8119" s="T922">people-1PL.[NOM]</ta>
            <ta e="T924" id="Seg_8120" s="T923">now</ta>
            <ta e="T925" id="Seg_8121" s="T924">self-3SG-GEN</ta>
            <ta e="T926" id="Seg_8122" s="T925">wish-3SG.[NOM]</ta>
            <ta e="T927" id="Seg_8123" s="T926">agree-PTCP.PRS-3SG-ACC</ta>
            <ta e="T928" id="Seg_8124" s="T927">bright</ta>
            <ta e="T929" id="Seg_8125" s="T928">happy</ta>
            <ta e="T930" id="Seg_8126" s="T929">Lenin.[NOM]</ta>
            <ta e="T931" id="Seg_8127" s="T930">show-PTCP.PST</ta>
            <ta e="T932" id="Seg_8128" s="T931">communism</ta>
            <ta e="T933" id="Seg_8129" s="T932">name-PROPR</ta>
            <ta e="T934" id="Seg_8130" s="T933">life-3SG-ACC</ta>
            <ta e="T935" id="Seg_8131" s="T934">make-PRS.[3SG]</ta>
            <ta e="T936" id="Seg_8132" s="T935">1PL.[NOM]</ta>
            <ta e="T937" id="Seg_8133" s="T936">every-1PL.[NOM]</ta>
            <ta e="T938" id="Seg_8134" s="T937">say-PRS-1PL</ta>
            <ta e="T939" id="Seg_8135" s="T938">now</ta>
            <ta e="T940" id="Seg_8136" s="T939">war.[NOM]</ta>
            <ta e="T941" id="Seg_8137" s="T940">NEG.EX</ta>
            <ta e="T942" id="Seg_8138" s="T941">be-IMP.3SG</ta>
            <ta e="T943" id="Seg_8139" s="T942">earth.[NOM]</ta>
            <ta e="T944" id="Seg_8140" s="T943">upper.part-3SG-INSTR</ta>
            <ta e="T945" id="Seg_8141" s="T944">there.is</ta>
            <ta e="T946" id="Seg_8142" s="T945">be-IMP.3SG</ta>
            <ta e="T947" id="Seg_8143" s="T946">bright</ta>
            <ta e="T948" id="Seg_8144" s="T947">sun-PROPR</ta>
            <ta e="T949" id="Seg_8145" s="T948">life.[NOM]</ta>
            <ta e="T950" id="Seg_8146" s="T949">every</ta>
            <ta e="T951" id="Seg_8147" s="T950">people-DAT/LOC</ta>
         </annotation>
         <annotation name="gg" tierref="gg">
            <ta e="T1" id="Seg_8148" s="T0">1SG.[NOM]</ta>
            <ta e="T2" id="Seg_8149" s="T1">Krieg-DAT/LOC</ta>
            <ta e="T3" id="Seg_8150" s="T2">gehen-PST2-EP-1SG</ta>
            <ta e="T4" id="Seg_8151" s="T3">selbst-1SG.[NOM]</ta>
            <ta e="T5" id="Seg_8152" s="T4">Wunsch-1SG-INSTR</ta>
            <ta e="T6" id="Seg_8153" s="T5">tausend</ta>
            <ta e="T7" id="Seg_8154" s="T6">neun</ta>
            <ta e="T8" id="Seg_8155" s="T7">hundert</ta>
            <ta e="T9" id="Seg_8156" s="T8">vier-zehn</ta>
            <ta e="T10" id="Seg_8157" s="T9">eins</ta>
            <ta e="T11" id="Seg_8158" s="T10">Jahr-PROPR-DAT/LOC</ta>
            <ta e="T12" id="Seg_8159" s="T11">herbstlich</ta>
            <ta e="T13" id="Seg_8160" s="T12">September.[NOM]</ta>
            <ta e="T14" id="Seg_8161" s="T13">Kälte.[NOM]</ta>
            <ta e="T15" id="Seg_8162" s="T14">fallen-PTCP.PRS</ta>
            <ta e="T16" id="Seg_8163" s="T15">Monat-3SG-DAT/LOC</ta>
            <ta e="T17" id="Seg_8164" s="T16">zuerst</ta>
            <ta e="T18" id="Seg_8165" s="T17">gehen-CVB.SEQ-1SG</ta>
            <ta e="T19" id="Seg_8166" s="T18">eins</ta>
            <ta e="T20" id="Seg_8167" s="T19">Jahr-ACC</ta>
            <ta e="T21" id="Seg_8168" s="T20">mit</ta>
            <ta e="T22" id="Seg_8169" s="T21">neun</ta>
            <ta e="T23" id="Seg_8170" s="T22">Monat-ACC</ta>
            <ta e="T24" id="Seg_8171" s="T23">Krieg.[NOM]</ta>
            <ta e="T25" id="Seg_8172" s="T24">Ausbildung-3SG-DAT/LOC</ta>
            <ta e="T26" id="Seg_8173" s="T25">lernen-CVB.SIM</ta>
            <ta e="T27" id="Seg_8174" s="T26">gehen-EP-PST2-EP-1SG</ta>
            <ta e="T29" id="Seg_8175" s="T27">Ferner.Osten.[NOM]</ta>
            <ta e="T30" id="Seg_8176" s="T29">sagen-CVB.SEQ</ta>
            <ta e="T31" id="Seg_8177" s="T30">Ort-DAT/LOC</ta>
            <ta e="T32" id="Seg_8178" s="T31">dann</ta>
            <ta e="T33" id="Seg_8179" s="T32">im.Sommer</ta>
            <ta e="T34" id="Seg_8180" s="T33">tausend</ta>
            <ta e="T35" id="Seg_8181" s="T34">neun</ta>
            <ta e="T36" id="Seg_8182" s="T35">hundert</ta>
            <ta e="T37" id="Seg_8183" s="T36">vier-zehn</ta>
            <ta e="T38" id="Seg_8184" s="T37">drei</ta>
            <ta e="T39" id="Seg_8185" s="T38">Jahr-PROPR-DAT/LOC</ta>
            <ta e="T40" id="Seg_8186" s="T39">1PL-ACC</ta>
            <ta e="T41" id="Seg_8187" s="T40">schicken-PST2-3PL</ta>
            <ta e="T42" id="Seg_8188" s="T41">Westen.[NOM]</ta>
            <ta e="T43" id="Seg_8189" s="T42">zu</ta>
            <ta e="T44" id="Seg_8190" s="T43">Krieg.[NOM]</ta>
            <ta e="T45" id="Seg_8191" s="T44">Kampf-3SG.[NOM]</ta>
            <ta e="T46" id="Seg_8192" s="T45">gehen-CVB.SIM</ta>
            <ta e="T47" id="Seg_8193" s="T46">stehen-PTCP.PRS</ta>
            <ta e="T48" id="Seg_8194" s="T47">Ort-3SG-DAT/LOC</ta>
            <ta e="T49" id="Seg_8195" s="T48">1PL.[NOM]</ta>
            <ta e="T50" id="Seg_8196" s="T49">Krieg-ACC</ta>
            <ta e="T51" id="Seg_8197" s="T50">treffen-EP-PST2-1PL</ta>
            <ta e="T52" id="Seg_8198" s="T51">Prochorovka.[NOM]</ta>
            <ta e="T53" id="Seg_8199" s="T52">sagen-CVB.SEQ</ta>
            <ta e="T54" id="Seg_8200" s="T53">Name-PROPR</ta>
            <ta e="T55" id="Seg_8201" s="T54">Dorf-DAT/LOC</ta>
            <ta e="T56" id="Seg_8202" s="T55">Kursk.[NOM]</ta>
            <ta e="T57" id="Seg_8203" s="T56">Oblast-3SG-DAT/LOC</ta>
            <ta e="T58" id="Seg_8204" s="T57">dort</ta>
            <ta e="T59" id="Seg_8205" s="T58">deutsch</ta>
            <ta e="T60" id="Seg_8206" s="T59">Krieg-3SG-GEN</ta>
            <ta e="T61" id="Seg_8207" s="T60">Mensch-3SG.[NOM]</ta>
            <ta e="T62" id="Seg_8208" s="T61">sehr</ta>
            <ta e="T63" id="Seg_8209" s="T62">viel</ta>
            <ta e="T64" id="Seg_8210" s="T63">Krieg.[NOM]</ta>
            <ta e="T65" id="Seg_8211" s="T64">Waffe-3SG-ACC</ta>
            <ta e="T66" id="Seg_8212" s="T65">unterschiedlich</ta>
            <ta e="T67" id="Seg_8213" s="T66">anders</ta>
            <ta e="T68" id="Seg_8214" s="T67">Munition-3SG-ACC</ta>
            <ta e="T69" id="Seg_8215" s="T68">sammeln-CVB.SEQ</ta>
            <ta e="T70" id="Seg_8216" s="T69">nachdem</ta>
            <ta e="T71" id="Seg_8217" s="T70">1PL.[NOM]</ta>
            <ta e="T72" id="Seg_8218" s="T71">Armee-1PL-ACC</ta>
            <ta e="T73" id="Seg_8219" s="T72">sehr</ta>
            <ta e="T74" id="Seg_8220" s="T73">zurück</ta>
            <ta e="T75" id="Seg_8221" s="T74">zu</ta>
            <ta e="T76" id="Seg_8222" s="T75">dreißig</ta>
            <ta e="T77" id="Seg_8223" s="T76">fünf</ta>
            <ta e="T78" id="Seg_8224" s="T77">Werst-PROPR</ta>
            <ta e="T79" id="Seg_8225" s="T78">Ort-DAT/LOC</ta>
            <ta e="T80" id="Seg_8226" s="T79">zurückdrängen-PTCP.PST</ta>
            <ta e="T81" id="Seg_8227" s="T80">sein-PST1-3SG</ta>
            <ta e="T82" id="Seg_8228" s="T81">dann</ta>
            <ta e="T83" id="Seg_8229" s="T82">später</ta>
            <ta e="T84" id="Seg_8230" s="T83">dieses</ta>
            <ta e="T85" id="Seg_8231" s="T84">Ort-ACC</ta>
            <ta e="T86" id="Seg_8232" s="T85">Kursk.[NOM]</ta>
            <ta e="T87" id="Seg_8233" s="T86">Bogen.[NOM]</ta>
            <ta e="T88" id="Seg_8234" s="T87">sagen-CVB.SEQ</ta>
            <ta e="T89" id="Seg_8235" s="T88">Name-VBZ-PST2-3PL</ta>
            <ta e="T90" id="Seg_8236" s="T89">dolganisch-SIM</ta>
            <ta e="T91" id="Seg_8237" s="T90">sprechen-PTCP.COND-DAT/LOC</ta>
            <ta e="T92" id="Seg_8238" s="T91">Kursk.[NOM]</ta>
            <ta e="T93" id="Seg_8239" s="T92">krumm-3SG</ta>
            <ta e="T94" id="Seg_8240" s="T93">sagen-PTCP.FUT-DAT/LOC</ta>
            <ta e="T95" id="Seg_8241" s="T94">richtig.[NOM]</ta>
            <ta e="T96" id="Seg_8242" s="T95">sein-FUT-3SG</ta>
            <ta e="T97" id="Seg_8243" s="T96">deutsch</ta>
            <ta e="T98" id="Seg_8244" s="T97">Armee-3SG-GEN</ta>
            <ta e="T99" id="Seg_8245" s="T98">Herr-3SG-GEN</ta>
            <ta e="T100" id="Seg_8246" s="T99">Gedanke-3SG.[NOM]</ta>
            <ta e="T101" id="Seg_8247" s="T100">es.gibt</ta>
            <ta e="T102" id="Seg_8248" s="T101">sein-PST2.[3SG]</ta>
            <ta e="T103" id="Seg_8249" s="T102">dieses.[NOM]</ta>
            <ta e="T104" id="Seg_8250" s="T103">Erde-3SG-INSTR</ta>
            <ta e="T105" id="Seg_8251" s="T104">durch</ta>
            <ta e="T106" id="Seg_8252" s="T105">streiten-CVB.SEQ</ta>
            <ta e="T107" id="Seg_8253" s="T106">nachdem</ta>
            <ta e="T108" id="Seg_8254" s="T107">sehr</ta>
            <ta e="T109" id="Seg_8255" s="T108">schnell-ADVZ</ta>
            <ta e="T110" id="Seg_8256" s="T109">1PL.[NOM]</ta>
            <ta e="T111" id="Seg_8257" s="T110">Zentrum.[NOM]</ta>
            <ta e="T112" id="Seg_8258" s="T111">Ort-1PL-ACC</ta>
            <ta e="T113" id="Seg_8259" s="T112">Moskau.[NOM]</ta>
            <ta e="T114" id="Seg_8260" s="T113">sagen-CVB.SEQ</ta>
            <ta e="T115" id="Seg_8261" s="T114">Stadt-ACC</ta>
            <ta e="T116" id="Seg_8262" s="T115">nehmen-PTCP.PRS.[NOM]</ta>
            <ta e="T117" id="Seg_8263" s="T116">ähnlich</ta>
            <ta e="T118" id="Seg_8264" s="T117">unser</ta>
            <ta e="T119" id="Seg_8265" s="T118">Teil-1PL.[NOM]</ta>
            <ta e="T120" id="Seg_8266" s="T119">Kursk.[NOM]</ta>
            <ta e="T121" id="Seg_8267" s="T120">krumm-3SG-DAT/LOC</ta>
            <ta e="T122" id="Seg_8268" s="T121">was-ABL</ta>
            <ta e="T123" id="Seg_8269" s="T122">NEG</ta>
            <ta e="T124" id="Seg_8270" s="T123">groß</ta>
            <ta e="T125" id="Seg_8271" s="T124">Krieg.[NOM]</ta>
            <ta e="T126" id="Seg_8272" s="T125">Feuer-3SG-DAT/LOC</ta>
            <ta e="T127" id="Seg_8273" s="T126">hineingehen-CVB.SEQ</ta>
            <ta e="T128" id="Seg_8274" s="T127">nachdem</ta>
            <ta e="T129" id="Seg_8275" s="T128">zehn</ta>
            <ta e="T130" id="Seg_8276" s="T129">Tag.[NOM]</ta>
            <ta e="T131" id="Seg_8277" s="T130">solange</ta>
            <ta e="T132" id="Seg_8278" s="T131">deutsch</ta>
            <ta e="T133" id="Seg_8279" s="T132">Armee-3SG-ACC</ta>
            <ta e="T134" id="Seg_8280" s="T133">jagen-CVB.SIM</ta>
            <ta e="T135" id="Seg_8281" s="T134">versuchen-PST2-3SG</ta>
            <ta e="T136" id="Seg_8282" s="T135">dieses.[NOM]</ta>
            <ta e="T137" id="Seg_8283" s="T136">solange</ta>
            <ta e="T138" id="Seg_8284" s="T137">1PL.[NOM]</ta>
            <ta e="T139" id="Seg_8285" s="T138">kämpfen-PST2-1PL</ta>
            <ta e="T140" id="Seg_8286" s="T139">Tag-ACC</ta>
            <ta e="T141" id="Seg_8287" s="T140">Nacht-ACC</ta>
            <ta e="T142" id="Seg_8288" s="T141">bemerken-NEG.CVB.SIM</ta>
            <ta e="T143" id="Seg_8289" s="T142">schlafen-NEG.CVB.SIM</ta>
            <ta e="T144" id="Seg_8290" s="T143">NEG</ta>
            <ta e="T145" id="Seg_8291" s="T144">sich.ausruhen-NEG.CVB.SIM</ta>
            <ta e="T146" id="Seg_8292" s="T145">NEG</ta>
            <ta e="T147" id="Seg_8293" s="T146">Beispiel-3SG.[NOM]</ta>
            <ta e="T148" id="Seg_8294" s="T147">was-ACC</ta>
            <ta e="T149" id="Seg_8295" s="T148">NEG</ta>
            <ta e="T150" id="Seg_8296" s="T149">wissen-NEG.PTCP.[NOM]</ta>
            <ta e="T151" id="Seg_8297" s="T150">hören-EP-NEG.PTCP.[NOM]</ta>
            <ta e="T152" id="Seg_8298" s="T151">ähnlich</ta>
            <ta e="T153" id="Seg_8299" s="T152">zwei</ta>
            <ta e="T154" id="Seg_8300" s="T153">Seite</ta>
            <ta e="T155" id="Seg_8301" s="T154">kämpfen-PTCP.HAB</ta>
            <ta e="T156" id="Seg_8302" s="T155">Seite-ABL</ta>
            <ta e="T157" id="Seg_8303" s="T156">so.viel</ta>
            <ta e="T158" id="Seg_8304" s="T157">tausend</ta>
            <ta e="T159" id="Seg_8305" s="T158">Mensch.[NOM]</ta>
            <ta e="T160" id="Seg_8306" s="T159">verlorengehen-PTCP.PST-3SG-ACC</ta>
            <ta e="T161" id="Seg_8307" s="T160">sterben-PTCP.PST-3SG-ACC</ta>
            <ta e="T162" id="Seg_8308" s="T161">so.viel</ta>
            <ta e="T163" id="Seg_8309" s="T162">Mensch.[NOM]</ta>
            <ta e="T164" id="Seg_8310" s="T163">Wunde.[NOM]</ta>
            <ta e="T165" id="Seg_8311" s="T164">sein-PTCP.PST-3SG-ACC</ta>
            <ta e="T166" id="Seg_8312" s="T165">so.viel</ta>
            <ta e="T167" id="Seg_8313" s="T166">Krieg.[NOM]</ta>
            <ta e="T168" id="Seg_8314" s="T167">Waffe-3SG.[NOM]</ta>
            <ta e="T169" id="Seg_8315" s="T168">Flugzeug-PL.[NOM]</ta>
            <ta e="T170" id="Seg_8316" s="T169">Panzer-PL.[NOM]</ta>
            <ta e="T171" id="Seg_8317" s="T170">Geschütz-PL.[NOM]</ta>
            <ta e="T172" id="Seg_8318" s="T171">dann</ta>
            <ta e="T173" id="Seg_8319" s="T172">klein</ta>
            <ta e="T174" id="Seg_8320" s="T173">unterschiedlich</ta>
            <ta e="T175" id="Seg_8321" s="T174">Gewehr-PL.[NOM]</ta>
            <ta e="T176" id="Seg_8322" s="T175">Krieg.[NOM]</ta>
            <ta e="T177" id="Seg_8323" s="T176">Munition-3SG.[NOM]</ta>
            <ta e="T178" id="Seg_8324" s="T177">zerbrechen-PTCP.PST-3SG-ACC</ta>
            <ta e="T179" id="Seg_8325" s="T178">wer.[NOM]</ta>
            <ta e="T180" id="Seg_8326" s="T179">EMPH</ta>
            <ta e="T181" id="Seg_8327" s="T180">besonders</ta>
            <ta e="T182" id="Seg_8328" s="T181">wissen-NEG.PTCP</ta>
            <ta e="T183" id="Seg_8329" s="T182">sein-PST1-3SG</ta>
            <ta e="T184" id="Seg_8330" s="T183">dann</ta>
            <ta e="T185" id="Seg_8331" s="T184">wie</ta>
            <ta e="T186" id="Seg_8332" s="T185">groß</ta>
            <ta e="T187" id="Seg_8333" s="T186">stark</ta>
            <ta e="T188" id="Seg_8334" s="T187">Krieg.[NOM]</ta>
            <ta e="T189" id="Seg_8335" s="T188">gehen-PTCP.PST-3SG-ACC</ta>
            <ta e="T190" id="Seg_8336" s="T189">dieses.[NOM]</ta>
            <ta e="T191" id="Seg_8337" s="T190">Ort-DAT/LOC</ta>
            <ta e="T192" id="Seg_8338" s="T191">Mensch.[NOM]</ta>
            <ta e="T193" id="Seg_8339" s="T192">verstehen-FUT-3SG</ta>
            <ta e="T194" id="Seg_8340" s="T193">eins</ta>
            <ta e="T195" id="Seg_8341" s="T194">solch</ta>
            <ta e="T196" id="Seg_8342" s="T195">Beispiel-ABL</ta>
            <ta e="T197" id="Seg_8343" s="T196">eins</ta>
            <ta e="T198" id="Seg_8344" s="T197">Eis-PROPR</ta>
            <ta e="T199" id="Seg_8345" s="T198">Ort-DIM-DAT/LOC</ta>
            <ta e="T200" id="Seg_8346" s="T199">1PL.[NOM]</ta>
            <ta e="T201" id="Seg_8347" s="T200">es.gibt</ta>
            <ta e="T202" id="Seg_8348" s="T201">Erde-1PL-DAT/LOC</ta>
            <ta e="T203" id="Seg_8349" s="T202">Tag-DAT/LOC</ta>
            <ta e="T204" id="Seg_8350" s="T203">einmal</ta>
            <ta e="T205" id="Seg_8351" s="T204">fliegen-CVB.SEQ</ta>
            <ta e="T206" id="Seg_8352" s="T205">kommen-PTCP.HAB</ta>
            <ta e="T207" id="Seg_8353" s="T206">sein-PST1-3SG</ta>
            <ta e="T208" id="Seg_8354" s="T207">Deutscher.[NOM]</ta>
            <ta e="T209" id="Seg_8355" s="T208">in.Richtung</ta>
            <ta e="T210" id="Seg_8356" s="T209">Seite-ABL</ta>
            <ta e="T211" id="Seg_8357" s="T210">zwei</ta>
            <ta e="T212" id="Seg_8358" s="T211">hundert</ta>
            <ta e="T213" id="Seg_8359" s="T212">ungefähr-3SG.[NOM]</ta>
            <ta e="T214" id="Seg_8360" s="T213">Flugzeug-PL.[NOM]</ta>
            <ta e="T215" id="Seg_8361" s="T214">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T216" id="Seg_8362" s="T215">jeder-3SG.[NOM]</ta>
            <ta e="T217" id="Seg_8363" s="T216">1PL.[NOM]</ta>
            <ta e="T218" id="Seg_8364" s="T217">oberer.Teil-1PL-INSTR</ta>
            <ta e="T219" id="Seg_8365" s="T218">Regen.[NOM]</ta>
            <ta e="T220" id="Seg_8366" s="T219">ähnlich</ta>
            <ta e="T221" id="Seg_8367" s="T220">fallen-CVB.SEQ</ta>
            <ta e="T222" id="Seg_8368" s="T221">äh</ta>
            <ta e="T223" id="Seg_8369" s="T222">hundert</ta>
            <ta e="T224" id="Seg_8370" s="T223">Bombe-PL-ACC</ta>
            <ta e="T225" id="Seg_8371" s="T224">werfen-PTCP.HAB</ta>
            <ta e="T226" id="Seg_8372" s="T225">sein-PST1-3SG</ta>
            <ta e="T227" id="Seg_8373" s="T226">dann</ta>
            <ta e="T228" id="Seg_8374" s="T227">einzeln</ta>
            <ta e="T229" id="Seg_8375" s="T228">noch</ta>
            <ta e="T230" id="Seg_8376" s="T229">schießen-FREQ-PTCP.PRS</ta>
            <ta e="T231" id="Seg_8377" s="T230">sein-PST1-3SG</ta>
            <ta e="T232" id="Seg_8378" s="T231">wie.viel</ta>
            <ta e="T233" id="Seg_8379" s="T232">INDEF</ta>
            <ta e="T234" id="Seg_8380" s="T233">kleine.Patrone.[NOM]</ta>
            <ta e="T235" id="Seg_8381" s="T234">ähnlich</ta>
            <ta e="T236" id="Seg_8382" s="T235">laden-NMNZ-PROPR</ta>
            <ta e="T237" id="Seg_8383" s="T236">Krieg.[NOM]</ta>
            <ta e="T238" id="Seg_8384" s="T237">Waffe-3SG-INSTR</ta>
            <ta e="T239" id="Seg_8385" s="T238">Maschinengewehr-EP-INSTR</ta>
            <ta e="T240" id="Seg_8386" s="T239">so.viel-PROPR</ta>
            <ta e="T241" id="Seg_8387" s="T240">Bombe.[NOM]</ta>
            <ta e="T242" id="Seg_8388" s="T241">fallen-PTCP.PST-3SG-ACC</ta>
            <ta e="T243" id="Seg_8389" s="T242">nachdem</ta>
            <ta e="T244" id="Seg_8390" s="T243">so.viel-PROPR</ta>
            <ta e="T245" id="Seg_8391" s="T244">Waffe.[NOM]</ta>
            <ta e="T246" id="Seg_8392" s="T245">schießen-FREQ-PTCP.PST-3SG-ACC</ta>
            <ta e="T247" id="Seg_8393" s="T246">nachdem</ta>
            <ta e="T248" id="Seg_8394" s="T247">Erde.[NOM]</ta>
            <ta e="T249" id="Seg_8395" s="T248">ganz</ta>
            <ta e="T250" id="Seg_8396" s="T249">was-INTNS.[NOM]</ta>
            <ta e="T251" id="Seg_8397" s="T250">NEG</ta>
            <ta e="T252" id="Seg_8398" s="T251">zu.sehen.sein-EP-NEG.PTCP.[NOM]</ta>
            <ta e="T253" id="Seg_8399" s="T252">ähnlich</ta>
            <ta e="T254" id="Seg_8400" s="T253">schwarz</ta>
            <ta e="T255" id="Seg_8401" s="T254">Nebel.[NOM]</ta>
            <ta e="T256" id="Seg_8402" s="T255">werden-PTCP.PRS</ta>
            <ta e="T257" id="Seg_8403" s="T256">sein-PST1-3SG</ta>
            <ta e="T258" id="Seg_8404" s="T257">dieses</ta>
            <ta e="T259" id="Seg_8405" s="T258">Nebel-DAT/LOC</ta>
            <ta e="T260" id="Seg_8406" s="T259">Mensch.[NOM]</ta>
            <ta e="T261" id="Seg_8407" s="T260">Platz.neben-DAT/LOC</ta>
            <ta e="T262" id="Seg_8408" s="T261">liegen-PTCP.PRS</ta>
            <ta e="T263" id="Seg_8409" s="T262">Mensch-3SG-ACC</ta>
            <ta e="T264" id="Seg_8410" s="T263">schaffen-CVB.SEQ</ta>
            <ta e="T265" id="Seg_8411" s="T264">sehen-NEG.PTCP</ta>
            <ta e="T266" id="Seg_8412" s="T265">sein-PST1-3SG</ta>
            <ta e="T267" id="Seg_8413" s="T266">mancher.[NOM]</ta>
            <ta e="T268" id="Seg_8414" s="T267">nachdem</ta>
            <ta e="T269" id="Seg_8415" s="T268">Mensch.[NOM]</ta>
            <ta e="T270" id="Seg_8416" s="T269">Krieg.[NOM]</ta>
            <ta e="T271" id="Seg_8417" s="T270">Waffe-3SG-GEN</ta>
            <ta e="T272" id="Seg_8418" s="T271">Lärm-3SG-ABL</ta>
            <ta e="T273" id="Seg_8419" s="T272">was-INTNS-ACC</ta>
            <ta e="T274" id="Seg_8420" s="T273">NEG</ta>
            <ta e="T275" id="Seg_8421" s="T274">hören-EP-NEG.PTCP.[NOM]</ta>
            <ta e="T276" id="Seg_8422" s="T275">ähnlich</ta>
            <ta e="T277" id="Seg_8423" s="T276">taub.[NOM]</ta>
            <ta e="T278" id="Seg_8424" s="T277">werden-PTCP.PRS</ta>
            <ta e="T279" id="Seg_8425" s="T278">sein-PST1-3SG</ta>
            <ta e="T280" id="Seg_8426" s="T279">zehn-ORD</ta>
            <ta e="T281" id="Seg_8427" s="T280">Tag-1PL-DAT/LOC</ta>
            <ta e="T282" id="Seg_8428" s="T281">1PL.[NOM]</ta>
            <ta e="T283" id="Seg_8429" s="T282">in.Richtung</ta>
            <ta e="T284" id="Seg_8430" s="T283">Seite-INSTR</ta>
            <ta e="T285" id="Seg_8431" s="T284">Krieg-ACC</ta>
            <ta e="T286" id="Seg_8432" s="T285">sehr</ta>
            <ta e="T287" id="Seg_8433" s="T286">Kraft-PROPR-ADVZ</ta>
            <ta e="T288" id="Seg_8434" s="T287">gehen-PST1-3SG</ta>
            <ta e="T289" id="Seg_8435" s="T288">Himmel-ABL</ta>
            <ta e="T290" id="Seg_8436" s="T289">Erde-ABL</ta>
            <ta e="T291" id="Seg_8437" s="T290">sehr</ta>
            <ta e="T292" id="Seg_8438" s="T291">viel</ta>
            <ta e="T293" id="Seg_8439" s="T292">Flugzeug-PL.[NOM]</ta>
            <ta e="T294" id="Seg_8440" s="T293">Panzer-PL.[NOM]</ta>
            <ta e="T295" id="Seg_8441" s="T294">besonders</ta>
            <ta e="T296" id="Seg_8442" s="T295">Krieg-DAT/LOC</ta>
            <ta e="T297" id="Seg_8443" s="T296">gemacht.werden-EP-PTCP.PST</ta>
            <ta e="T298" id="Seg_8444" s="T297">Feuer-PROPR</ta>
            <ta e="T299" id="Seg_8445" s="T298">Waffe-PL.[NOM]</ta>
            <ta e="T300" id="Seg_8446" s="T299">Katjuscha.[NOM]</ta>
            <ta e="T301" id="Seg_8447" s="T300">Andrjuscha.[NOM]</ta>
            <ta e="T302" id="Seg_8448" s="T301">sagen-CVB.SEQ</ta>
            <ta e="T303" id="Seg_8449" s="T302">Name-PROPR-PL.[NOM]</ta>
            <ta e="T304" id="Seg_8450" s="T303">dann</ta>
            <ta e="T305" id="Seg_8451" s="T304">einzeln</ta>
            <ta e="T306" id="Seg_8452" s="T305">wie.viel</ta>
            <ta e="T307" id="Seg_8453" s="T306">INDEF</ta>
            <ta e="T308" id="Seg_8454" s="T307">klein</ta>
            <ta e="T309" id="Seg_8455" s="T308">Gewehr-PL.[NOM]</ta>
            <ta e="T310" id="Seg_8456" s="T309">jenes.[NOM]</ta>
            <ta e="T311" id="Seg_8457" s="T310">jeder-3SG.[NOM]</ta>
            <ta e="T312" id="Seg_8458" s="T311">zusammen</ta>
            <ta e="T313" id="Seg_8459" s="T312">schlagen-PST1-3PL</ta>
            <ta e="T314" id="Seg_8460" s="T313">Deutscher-PL.[NOM]</ta>
            <ta e="T315" id="Seg_8461" s="T314">es.gibt</ta>
            <ta e="T316" id="Seg_8462" s="T315">Ort-3PL-ACC</ta>
            <ta e="T317" id="Seg_8463" s="T316">deutsch</ta>
            <ta e="T318" id="Seg_8464" s="T317">Armee-3SG.[NOM]</ta>
            <ta e="T319" id="Seg_8465" s="T318">dieses-ACC</ta>
            <ta e="T320" id="Seg_8466" s="T319">Welle-PROPR</ta>
            <ta e="T321" id="Seg_8467" s="T320">groß</ta>
            <ta e="T322" id="Seg_8468" s="T321">schlagen-NMNZ-ACC</ta>
            <ta e="T323" id="Seg_8469" s="T322">ertragen-NEG.CVB.SIM</ta>
            <ta e="T324" id="Seg_8470" s="T323">zurück</ta>
            <ta e="T325" id="Seg_8471" s="T324">durchbrechen-PST2-3SG</ta>
            <ta e="T326" id="Seg_8472" s="T325">sofort-INTNS</ta>
            <ta e="T327" id="Seg_8473" s="T326">zehn</ta>
            <ta e="T328" id="Seg_8474" s="T327">fünf</ta>
            <ta e="T329" id="Seg_8475" s="T328">Werst-PROPR</ta>
            <ta e="T330" id="Seg_8476" s="T329">Ort-ACC</ta>
            <ta e="T331" id="Seg_8477" s="T330">zurück</ta>
            <ta e="T332" id="Seg_8478" s="T331">springen-PST2-3SG</ta>
            <ta e="T333" id="Seg_8479" s="T332">dieses.[NOM]</ta>
            <ta e="T334" id="Seg_8480" s="T333">groß</ta>
            <ta e="T335" id="Seg_8481" s="T334">schlagen-NMNZ.[NOM]</ta>
            <ta e="T336" id="Seg_8482" s="T335">Hinterteil-3SG-ABL</ta>
            <ta e="T337" id="Seg_8483" s="T336">Kursk.[NOM]</ta>
            <ta e="T338" id="Seg_8484" s="T337">sagen-CVB.SEQ</ta>
            <ta e="T339" id="Seg_8485" s="T338">krumm-3SG.[NOM]</ta>
            <ta e="T340" id="Seg_8486" s="T339">sehr</ta>
            <ta e="T341" id="Seg_8487" s="T340">besser.werden-PST2-3SG</ta>
            <ta e="T342" id="Seg_8488" s="T341">deutsch</ta>
            <ta e="T343" id="Seg_8489" s="T342">Armee-3SG-ACC</ta>
            <ta e="T344" id="Seg_8490" s="T343">zurück</ta>
            <ta e="T345" id="Seg_8491" s="T344">1PL.[NOM]</ta>
            <ta e="T346" id="Seg_8492" s="T345">jagen-PST2-1PL</ta>
            <ta e="T347" id="Seg_8493" s="T346">dieses.[NOM]</ta>
            <ta e="T348" id="Seg_8494" s="T347">Hinterteil-3SG-ABL</ta>
            <ta e="T349" id="Seg_8495" s="T348">Deutscher-PL.[NOM]</ta>
            <ta e="T350" id="Seg_8496" s="T349">nicht.gut</ta>
            <ta e="T351" id="Seg_8497" s="T350">Soldat-3PL.[NOM]</ta>
            <ta e="T352" id="Seg_8498" s="T351">Vorderteil-3PL-ACC</ta>
            <ta e="T353" id="Seg_8499" s="T352">1PL-DAT/LOC</ta>
            <ta e="T354" id="Seg_8500" s="T353">geben-CVB.SEQ</ta>
            <ta e="T355" id="Seg_8501" s="T354">nachdem</ta>
            <ta e="T356" id="Seg_8502" s="T355">direkt</ta>
            <ta e="T357" id="Seg_8503" s="T356">Krieg.[NOM]</ta>
            <ta e="T358" id="Seg_8504" s="T357">aufhören-PTCP.FUT-3SG-DAT/LOC</ta>
            <ta e="T359" id="Seg_8505" s="T358">bis.zu</ta>
            <ta e="T360" id="Seg_8506" s="T359">Verstand-3PL-ACC-Vernunft-3PL-ACC</ta>
            <ta e="T361" id="Seg_8507" s="T360">finden-PST2.NEG-3PL</ta>
            <ta e="T362" id="Seg_8508" s="T361">fliehen-CVB.SEQ</ta>
            <ta e="T363" id="Seg_8509" s="T362">gehen-PST2-3PL</ta>
            <ta e="T364" id="Seg_8510" s="T363">1PL.[NOM]</ta>
            <ta e="T365" id="Seg_8511" s="T364">Armee-1PL.[NOM]</ta>
            <ta e="T366" id="Seg_8512" s="T365">sehr</ta>
            <ta e="T367" id="Seg_8513" s="T366">gut-ADVZ</ta>
            <ta e="T368" id="Seg_8514" s="T367">Kraft-3SG-ACC</ta>
            <ta e="T369" id="Seg_8515" s="T368">greifen-CVB.SEQ</ta>
            <ta e="T370" id="Seg_8516" s="T369">nachdem</ta>
            <ta e="T371" id="Seg_8517" s="T370">Deutscher-PL-ACC</ta>
            <ta e="T372" id="Seg_8518" s="T371">Hinterteil.[NOM]</ta>
            <ta e="T373" id="Seg_8519" s="T372">zu</ta>
            <ta e="T374" id="Seg_8520" s="T373">zeigen-NEG.CVB.SIM</ta>
            <ta e="T375" id="Seg_8521" s="T374">genug</ta>
            <ta e="T376" id="Seg_8522" s="T375">jagen-PST2-3SG</ta>
            <ta e="T377" id="Seg_8523" s="T376">1SG.[NOM]</ta>
            <ta e="T378" id="Seg_8524" s="T377">spät-ADJZ</ta>
            <ta e="T379" id="Seg_8525" s="T378">Tag-PL-DAT/LOC</ta>
            <ta e="T380" id="Seg_8526" s="T379">Krieg-DAT/LOC</ta>
            <ta e="T381" id="Seg_8527" s="T380">gehen-CVB.SEQ-1SG</ta>
            <ta e="T382" id="Seg_8528" s="T381">solch</ta>
            <ta e="T383" id="Seg_8529" s="T382">Grauen-PROPR</ta>
            <ta e="T384" id="Seg_8530" s="T383">Mensch.[NOM]</ta>
            <ta e="T385" id="Seg_8531" s="T952">ähnlich</ta>
            <ta e="T387" id="Seg_8532" s="T385">sehen-PST2.NEG-EP-1SG</ta>
            <ta e="T388" id="Seg_8533" s="T387">1SG.[NOM]</ta>
            <ta e="T389" id="Seg_8534" s="T388">dieses</ta>
            <ta e="T390" id="Seg_8535" s="T389">Ort-DAT/LOC</ta>
            <ta e="T391" id="Seg_8536" s="T390">erstmals</ta>
            <ta e="T392" id="Seg_8537" s="T391">sehen-PST2-EP-1SG</ta>
            <ta e="T393" id="Seg_8538" s="T392">Hitze-PROPR</ta>
            <ta e="T394" id="Seg_8539" s="T393">Kampf-ACC</ta>
            <ta e="T395" id="Seg_8540" s="T394">wie</ta>
            <ta e="T396" id="Seg_8541" s="T395">Krieg.[NOM]</ta>
            <ta e="T397" id="Seg_8542" s="T396">Mensch-3SG-GEN</ta>
            <ta e="T398" id="Seg_8543" s="T397">Blut-3SG.[NOM]</ta>
            <ta e="T399" id="Seg_8544" s="T398">Fluss-SIM</ta>
            <ta e="T400" id="Seg_8545" s="T399">ausgießen-MED-PTCP.PRS-3SG-ACC</ta>
            <ta e="T401" id="Seg_8546" s="T400">so.viel</ta>
            <ta e="T402" id="Seg_8547" s="T401">tausend</ta>
            <ta e="T403" id="Seg_8548" s="T402">Mensch.[NOM]</ta>
            <ta e="T404" id="Seg_8549" s="T403">Atem-3SG.[NOM]</ta>
            <ta e="T405" id="Seg_8550" s="T404">zu.Ende.gehen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T406" id="Seg_8551" s="T405">so.viel</ta>
            <ta e="T407" id="Seg_8552" s="T406">Wunde.[NOM]</ta>
            <ta e="T408" id="Seg_8553" s="T407">werden-PTCP.PST</ta>
            <ta e="T409" id="Seg_8554" s="T408">Mensch.[NOM]</ta>
            <ta e="T410" id="Seg_8555" s="T409">groß-ADVZ</ta>
            <ta e="T411" id="Seg_8556" s="T410">Qual-VBZ-PTCP.PRS-3SG-ACC</ta>
            <ta e="T412" id="Seg_8557" s="T411">1SG.[NOM]</ta>
            <ta e="T413" id="Seg_8558" s="T412">auch</ta>
            <ta e="T414" id="Seg_8559" s="T413">dieses</ta>
            <ta e="T415" id="Seg_8560" s="T414">schlagen-EP-RECP/COLL-NMNZ-DAT/LOC</ta>
            <ta e="T416" id="Seg_8561" s="T415">Kursk.[NOM]</ta>
            <ta e="T417" id="Seg_8562" s="T416">Felsvorsprung-3SG-DAT/LOC</ta>
            <ta e="T418" id="Seg_8563" s="T417">Einheit.[NOM]</ta>
            <ta e="T419" id="Seg_8564" s="T418">Befehlshaber-3SG.[NOM]</ta>
            <ta e="T420" id="Seg_8565" s="T419">sein-CVB.SIM</ta>
            <ta e="T421" id="Seg_8566" s="T420">gehen-CVB.SEQ-1SG</ta>
            <ta e="T422" id="Seg_8567" s="T421">leicht-ADVZ</ta>
            <ta e="T423" id="Seg_8568" s="T422">linker</ta>
            <ta e="T424" id="Seg_8569" s="T423">Bein-1SG-GEN</ta>
            <ta e="T425" id="Seg_8570" s="T424">Muskel-ACC</ta>
            <ta e="T426" id="Seg_8571" s="T425">Wunde-VBZ-EP-CAUS-PST2-EP-1SG</ta>
            <ta e="T427" id="Seg_8572" s="T426">jenes.[NOM]</ta>
            <ta e="T428" id="Seg_8573" s="T427">sein-CVB.SEQ</ta>
            <ta e="T429" id="Seg_8574" s="T428">nachdem</ta>
            <ta e="T430" id="Seg_8575" s="T429">kämpfen-PTCP.PRS</ta>
            <ta e="T431" id="Seg_8576" s="T430">Ort-ABL</ta>
            <ta e="T432" id="Seg_8577" s="T431">hinausgehen-EP-PST2.NEG-EP-1SG</ta>
            <ta e="T433" id="Seg_8578" s="T432">dieses</ta>
            <ta e="T434" id="Seg_8579" s="T433">Erde-PROPR</ta>
            <ta e="T435" id="Seg_8580" s="T434">schlagen-EP-RECP/COLL-NMNZ-DAT/LOC</ta>
            <ta e="T436" id="Seg_8581" s="T435">1SG.[NOM]</ta>
            <ta e="T437" id="Seg_8582" s="T436">es.gibt</ta>
            <ta e="T438" id="Seg_8583" s="T437">nur</ta>
            <ta e="T439" id="Seg_8584" s="T438">Teil-EP-1SG.[NOM]</ta>
            <ta e="T440" id="Seg_8585" s="T439">tausend-ABL</ta>
            <ta e="T441" id="Seg_8586" s="T440">über</ta>
            <ta e="T442" id="Seg_8587" s="T441">Krieg.[NOM]</ta>
            <ta e="T443" id="Seg_8588" s="T442">Mensch-3SG-ABL</ta>
            <ta e="T444" id="Seg_8589" s="T443">dieses</ta>
            <ta e="T445" id="Seg_8590" s="T444">zehn</ta>
            <ta e="T446" id="Seg_8591" s="T445">Tag-DAT/LOC</ta>
            <ta e="T447" id="Seg_8592" s="T446">schlagen-EP-RECP/COLL-NMNZ-DAT/LOC</ta>
            <ta e="T448" id="Seg_8593" s="T447">übrig.bleiben-PST2-3SG</ta>
            <ta e="T449" id="Seg_8594" s="T448">zehn</ta>
            <ta e="T450" id="Seg_8595" s="T449">eins</ta>
            <ta e="T451" id="Seg_8596" s="T450">nur</ta>
            <ta e="T452" id="Seg_8597" s="T451">Mensch.[NOM]</ta>
            <ta e="T453" id="Seg_8598" s="T452">dieses.[NOM]</ta>
            <ta e="T454" id="Seg_8599" s="T453">Inneres-3SG-DAT/LOC</ta>
            <ta e="T455" id="Seg_8600" s="T454">noch</ta>
            <ta e="T456" id="Seg_8601" s="T455">eins</ta>
            <ta e="T457" id="Seg_8602" s="T456">1SG.[NOM]</ta>
            <ta e="T458" id="Seg_8603" s="T457">Wunde.[NOM]</ta>
            <ta e="T459" id="Seg_8604" s="T458">werden-PTCP.PST</ta>
            <ta e="T460" id="Seg_8605" s="T459">Mensch.[NOM]</ta>
            <ta e="T461" id="Seg_8606" s="T460">es.gibt</ta>
            <ta e="T462" id="Seg_8607" s="T461">sein-PST1-1SG</ta>
            <ta e="T463" id="Seg_8608" s="T462">dieses-ABL</ta>
            <ta e="T464" id="Seg_8609" s="T463">richtig.[NOM]</ta>
            <ta e="T465" id="Seg_8610" s="T464">sein-FUT-3SG</ta>
            <ta e="T466" id="Seg_8611" s="T465">feststellen-PTCP.FUT-DAT/LOC</ta>
            <ta e="T467" id="Seg_8612" s="T466">wie</ta>
            <ta e="T468" id="Seg_8613" s="T467">Unglück-PROPR.[NOM]</ta>
            <ta e="T469" id="Seg_8614" s="T468">Krieg.[NOM]</ta>
            <ta e="T470" id="Seg_8615" s="T469">sein-PTCP.PST-3SG-ACC</ta>
            <ta e="T471" id="Seg_8616" s="T470">Kursk.[NOM]</ta>
            <ta e="T472" id="Seg_8617" s="T471">krumm-3SG-DAT/LOC</ta>
            <ta e="T473" id="Seg_8618" s="T472">wie.viel</ta>
            <ta e="T474" id="Seg_8619" s="T473">Blut.[NOM]</ta>
            <ta e="T475" id="Seg_8620" s="T474">ausgießen-MED-EP-PTCP.PST-3SG-ACC</ta>
            <ta e="T476" id="Seg_8621" s="T475">wie.viel</ta>
            <ta e="T477" id="Seg_8622" s="T476">Mensch.[NOM]</ta>
            <ta e="T478" id="Seg_8623" s="T477">sterben-PTCP.PST-3SG-ACC</ta>
            <ta e="T479" id="Seg_8624" s="T478">Kursk-DAT/LOC</ta>
            <ta e="T480" id="Seg_8625" s="T479">aufhören-CVB.SEQ</ta>
            <ta e="T481" id="Seg_8626" s="T480">nachdem</ta>
            <ta e="T482" id="Seg_8627" s="T481">1PL.[NOM]</ta>
            <ta e="T483" id="Seg_8628" s="T482">Armee-1PL.[NOM]</ta>
            <ta e="T484" id="Seg_8629" s="T483">zurück</ta>
            <ta e="T485" id="Seg_8630" s="T484">entfliehen-PTCP.PRS</ta>
            <ta e="T486" id="Seg_8631" s="T485">Deutscher-ACC</ta>
            <ta e="T487" id="Seg_8632" s="T486">Westen.[NOM]</ta>
            <ta e="T488" id="Seg_8633" s="T487">zu</ta>
            <ta e="T489" id="Seg_8634" s="T488">jagen-CVB.SEQ</ta>
            <ta e="T490" id="Seg_8635" s="T489">gehen-PST2-3SG</ta>
            <ta e="T491" id="Seg_8636" s="T490">dieses</ta>
            <ta e="T492" id="Seg_8637" s="T491">Kursk.[NOM]</ta>
            <ta e="T493" id="Seg_8638" s="T492">Hinterteil-3SG-ABL</ta>
            <ta e="T494" id="Seg_8639" s="T493">1SG.[NOM]</ta>
            <ta e="T495" id="Seg_8640" s="T494">meist</ta>
            <ta e="T496" id="Seg_8641" s="T495">Krieg.[NOM]</ta>
            <ta e="T497" id="Seg_8642" s="T496">Kampf.[NOM]</ta>
            <ta e="T498" id="Seg_8643" s="T497">Ort-3SG-DAT/LOC</ta>
            <ta e="T499" id="Seg_8644" s="T498">es.gibt</ta>
            <ta e="T500" id="Seg_8645" s="T499">sein-PST1-1SG</ta>
            <ta e="T501" id="Seg_8646" s="T500">tausend</ta>
            <ta e="T502" id="Seg_8647" s="T501">neun</ta>
            <ta e="T503" id="Seg_8648" s="T502">hundert</ta>
            <ta e="T504" id="Seg_8649" s="T503">vier-zehn</ta>
            <ta e="T505" id="Seg_8650" s="T504">vier</ta>
            <ta e="T506" id="Seg_8651" s="T505">Jahr-PROPR-DAT/LOC</ta>
            <ta e="T507" id="Seg_8652" s="T506">Winter-ADJZ</ta>
            <ta e="T508" id="Seg_8653" s="T507">Januar.[NOM]</ta>
            <ta e="T509" id="Seg_8654" s="T508">Monat-DAT/LOC</ta>
            <ta e="T510" id="Seg_8655" s="T509">bis.zu</ta>
            <ta e="T511" id="Seg_8656" s="T510">dieses</ta>
            <ta e="T512" id="Seg_8657" s="T511">Zeit-DAT/LOC</ta>
            <ta e="T513" id="Seg_8658" s="T512">deutsch</ta>
            <ta e="T514" id="Seg_8659" s="T513">Soldat-3PL-ACC</ta>
            <ta e="T515" id="Seg_8660" s="T514">mit</ta>
            <ta e="T516" id="Seg_8661" s="T515">entgegen</ta>
            <ta e="T517" id="Seg_8662" s="T516">stehen-CVB.SEQ</ta>
            <ta e="T518" id="Seg_8663" s="T517">kämpfen-PST2-EP-1SG</ta>
            <ta e="T519" id="Seg_8664" s="T518">solch</ta>
            <ta e="T520" id="Seg_8665" s="T519">Ort-PL-DAT/LOC</ta>
            <ta e="T521" id="Seg_8666" s="T520">Belgorod.[NOM]</ta>
            <ta e="T522" id="Seg_8667" s="T521">dann</ta>
            <ta e="T523" id="Seg_8668" s="T522">Charkov.[NOM]</ta>
            <ta e="T524" id="Seg_8669" s="T523">dann</ta>
            <ta e="T525" id="Seg_8670" s="T524">Poltava.[NOM]</ta>
            <ta e="T526" id="Seg_8671" s="T525">dann</ta>
            <ta e="T527" id="Seg_8672" s="T526">Krementschug.[NOM]</ta>
            <ta e="T528" id="Seg_8673" s="T527">dann</ta>
            <ta e="T529" id="Seg_8674" s="T528">Krjukov.[NOM]</ta>
            <ta e="T530" id="Seg_8675" s="T529">sagen-CVB.SEQ</ta>
            <ta e="T531" id="Seg_8676" s="T530">Name-PROPR</ta>
            <ta e="T532" id="Seg_8677" s="T531">groß</ta>
            <ta e="T533" id="Seg_8678" s="T532">Stadt-PL-ACC</ta>
            <ta e="T534" id="Seg_8679" s="T533">nehmen-PTCP.PRS-DAT/LOC</ta>
            <ta e="T535" id="Seg_8680" s="T534">ukrainisch</ta>
            <ta e="T536" id="Seg_8681" s="T535">sagen-CVB.SEQ</ta>
            <ta e="T537" id="Seg_8682" s="T536">Volk.[NOM]</ta>
            <ta e="T538" id="Seg_8683" s="T537">Erde-3SG-DAT/LOC</ta>
            <ta e="T539" id="Seg_8684" s="T538">dieses</ta>
            <ta e="T540" id="Seg_8685" s="T539">Stadt-PL-ABL</ta>
            <ta e="T541" id="Seg_8686" s="T540">sehr</ta>
            <ta e="T542" id="Seg_8687" s="T541">schwer-ADVZ</ta>
            <ta e="T543" id="Seg_8688" s="T542">1PL.[NOM]</ta>
            <ta e="T544" id="Seg_8689" s="T543">kämpfen-PST2-1PL</ta>
            <ta e="T545" id="Seg_8690" s="T544">Deutscher-ACC</ta>
            <ta e="T546" id="Seg_8691" s="T545">mit</ta>
            <ta e="T547" id="Seg_8692" s="T546">Charkov.[NOM]</ta>
            <ta e="T548" id="Seg_8693" s="T547">Stadt.[NOM]</ta>
            <ta e="T549" id="Seg_8694" s="T548">wegnehmen-EP-RECP/COLL-NMNZ-DAT/LOC</ta>
            <ta e="T550" id="Seg_8695" s="T549">dieses</ta>
            <ta e="T551" id="Seg_8696" s="T550">Stadt.[NOM]</ta>
            <ta e="T552" id="Seg_8697" s="T551">drei-DAT/LOC</ta>
            <ta e="T553" id="Seg_8698" s="T552">bis.zu</ta>
            <ta e="T554" id="Seg_8699" s="T553">hin.und.her</ta>
            <ta e="T555" id="Seg_8700" s="T554">Hand-ABL</ta>
            <ta e="T556" id="Seg_8701" s="T555">Hand-DAT/LOC</ta>
            <ta e="T557" id="Seg_8702" s="T556">kommen-CVB.SIM</ta>
            <ta e="T558" id="Seg_8703" s="T557">gehen-EP-PST2-3SG</ta>
            <ta e="T559" id="Seg_8704" s="T558">dann</ta>
            <ta e="T560" id="Seg_8705" s="T559">drei-ORD</ta>
            <ta e="T561" id="Seg_8706" s="T560">kommen-NMNZ-3SG-DAT/LOC</ta>
            <ta e="T562" id="Seg_8707" s="T561">1PL.[NOM]</ta>
            <ta e="T563" id="Seg_8708" s="T562">Hand-1PL-ABL</ta>
            <ta e="T564" id="Seg_8709" s="T563">lassen-PST2.NEG-1PL</ta>
            <ta e="T565" id="Seg_8710" s="T564">dann</ta>
            <ta e="T566" id="Seg_8711" s="T565">groß</ta>
            <ta e="T567" id="Seg_8712" s="T566">schlagen-EP-RECP/COLL-NMNZ.[NOM]</ta>
            <ta e="T568" id="Seg_8713" s="T567">noch</ta>
            <ta e="T569" id="Seg_8714" s="T568">es.gibt</ta>
            <ta e="T570" id="Seg_8715" s="T569">sein-PST1-3SG</ta>
            <ta e="T571" id="Seg_8716" s="T570">Dnjepr.[NOM]</ta>
            <ta e="T572" id="Seg_8717" s="T571">sagen-CVB.SEQ</ta>
            <ta e="T573" id="Seg_8718" s="T572">Name-PROPR</ta>
            <ta e="T574" id="Seg_8719" s="T573">Fluss.[NOM]</ta>
            <ta e="T575" id="Seg_8720" s="T574">auf.Wasser.bewegen-NMNZ-3SG-DAT/LOC</ta>
            <ta e="T576" id="Seg_8721" s="T575">dieses</ta>
            <ta e="T577" id="Seg_8722" s="T576">Fluss.[NOM]</ta>
            <ta e="T578" id="Seg_8723" s="T577">am.anderen.Ufer-ADJZ</ta>
            <ta e="T579" id="Seg_8724" s="T578">Seite-3SG-DAT/LOC</ta>
            <ta e="T580" id="Seg_8725" s="T579">sitzen-CVB.SEQ</ta>
            <ta e="T581" id="Seg_8726" s="T580">Deutscher-PL.[NOM]</ta>
            <ta e="T582" id="Seg_8727" s="T581">Tag-PL-ACC-Nacht-PL-ACC</ta>
            <ta e="T583" id="Seg_8728" s="T582">hüten-CVB.SIM</ta>
            <ta e="T584" id="Seg_8729" s="T583">gehen-CVB.SEQ-3PL</ta>
            <ta e="T585" id="Seg_8730" s="T584">1PL-ACC</ta>
            <ta e="T586" id="Seg_8731" s="T585">völlig</ta>
            <ta e="T587" id="Seg_8732" s="T586">können-CVB.SEQ</ta>
            <ta e="T588" id="Seg_8733" s="T587">auf.Wasser.bewegen-EP-CAUS.[CAUS]-NEG.PTCP</ta>
            <ta e="T589" id="Seg_8734" s="T588">sein-PST1-3PL</ta>
            <ta e="T590" id="Seg_8735" s="T589">jenes.[NOM]</ta>
            <ta e="T591" id="Seg_8736" s="T590">wegen</ta>
            <ta e="T592" id="Seg_8737" s="T591">1PL.[NOM]</ta>
            <ta e="T593" id="Seg_8738" s="T592">wie.viel</ta>
            <ta e="T594" id="Seg_8739" s="T593">INDEF</ta>
            <ta e="T595" id="Seg_8740" s="T594">Tag-ACC</ta>
            <ta e="T596" id="Seg_8741" s="T595">schaffen-CVB.SEQ</ta>
            <ta e="T597" id="Seg_8742" s="T596">hinausgehen-EP-PST2.NEG-1PL</ta>
            <ta e="T598" id="Seg_8743" s="T597">jene.Seite-ADJZ</ta>
            <ta e="T599" id="Seg_8744" s="T598">Ufer-DAT/LOC</ta>
            <ta e="T600" id="Seg_8745" s="T599">sitzen-PST2-1PL</ta>
            <ta e="T601" id="Seg_8746" s="T600">Fluss-ACC</ta>
            <ta e="T602" id="Seg_8747" s="T601">hinausgehen-PTCP.PRS</ta>
            <ta e="T603" id="Seg_8748" s="T602">Brücke-PL-ACC</ta>
            <ta e="T604" id="Seg_8749" s="T603">am.Tag</ta>
            <ta e="T605" id="Seg_8750" s="T604">machen-PTCP.PRS-1PL-ACC</ta>
            <ta e="T606" id="Seg_8751" s="T605">Deutscher-PL.[NOM]</ta>
            <ta e="T607" id="Seg_8752" s="T606">Flugzeug-3PL.[NOM]</ta>
            <ta e="T608" id="Seg_8753" s="T607">fliegen-CVB.SEQ</ta>
            <ta e="T609" id="Seg_8754" s="T608">kommen-CVB.SIM-kommen-CVB.SIM</ta>
            <ta e="T610" id="Seg_8755" s="T609">wie.viel</ta>
            <ta e="T611" id="Seg_8756" s="T610">INDEF</ta>
            <ta e="T612" id="Seg_8757" s="T611">Bombe-PL-ACC</ta>
            <ta e="T613" id="Seg_8758" s="T612">werfen-FREQ-CVB.SEQ</ta>
            <ta e="T614" id="Seg_8759" s="T613">genug</ta>
            <ta e="T615" id="Seg_8760" s="T614">zerbrechen-PTCP.PRS</ta>
            <ta e="T616" id="Seg_8761" s="T615">sein-PST1-3SG</ta>
            <ta e="T617" id="Seg_8762" s="T616">jenes.[NOM]</ta>
            <ta e="T618" id="Seg_8763" s="T617">wegen</ta>
            <ta e="T619" id="Seg_8764" s="T618">später</ta>
            <ta e="T620" id="Seg_8765" s="T619">1PL.[NOM]</ta>
            <ta e="T621" id="Seg_8766" s="T620">nachts</ta>
            <ta e="T622" id="Seg_8767" s="T621">einige</ta>
            <ta e="T623" id="Seg_8768" s="T622">Ort-EP-INSTR</ta>
            <ta e="T624" id="Seg_8769" s="T623">Brücke-PL-ACC</ta>
            <ta e="T625" id="Seg_8770" s="T624">machen-ITER-PTCP.PRS</ta>
            <ta e="T626" id="Seg_8771" s="T625">sein-PST1-1PL</ta>
            <ta e="T627" id="Seg_8772" s="T626">böser.Geist.[NOM]</ta>
            <ta e="T628" id="Seg_8773" s="T627">Deutscher.[NOM]</ta>
            <ta e="T629" id="Seg_8774" s="T628">nachts</ta>
            <ta e="T630" id="Seg_8775" s="T629">und</ta>
            <ta e="T631" id="Seg_8776" s="T630">machen-PTCP.PST</ta>
            <ta e="T632" id="Seg_8777" s="T631">Brücke-PL-1PL-ACC</ta>
            <ta e="T633" id="Seg_8778" s="T632">stehen-CAUS-NEG.PTCP</ta>
            <ta e="T634" id="Seg_8779" s="T633">sein-PST1-3SG</ta>
            <ta e="T635" id="Seg_8780" s="T634">dann</ta>
            <ta e="T636" id="Seg_8781" s="T635">1PL.[NOM]</ta>
            <ta e="T637" id="Seg_8782" s="T636">Seite-PL.[NOM]</ta>
            <ta e="T638" id="Seg_8783" s="T637">genug</ta>
            <ta e="T639" id="Seg_8784" s="T638">böse.werden-CVB.SEQ</ta>
            <ta e="T640" id="Seg_8785" s="T639">nachdem</ta>
            <ta e="T641" id="Seg_8786" s="T640">jeder</ta>
            <ta e="T642" id="Seg_8787" s="T641">Krieg.[NOM]</ta>
            <ta e="T643" id="Seg_8788" s="T642">Waffe-3SG-INSTR</ta>
            <ta e="T644" id="Seg_8789" s="T643">genug</ta>
            <ta e="T645" id="Seg_8790" s="T644">schießen-FREQ-CVB.SEQ</ta>
            <ta e="T646" id="Seg_8791" s="T645">nachdem</ta>
            <ta e="T647" id="Seg_8792" s="T646">wie.viel</ta>
            <ta e="T648" id="Seg_8793" s="T647">INDEF</ta>
            <ta e="T649" id="Seg_8794" s="T648">zehn-DISTR</ta>
            <ta e="T650" id="Seg_8795" s="T649">Flugzeug-PL-ACC</ta>
            <ta e="T651" id="Seg_8796" s="T650">am.anderen.Ufer-ADJZ</ta>
            <ta e="T652" id="Seg_8797" s="T651">Seite-3SG-DAT/LOC</ta>
            <ta e="T653" id="Seg_8798" s="T652">schicken-CVB.SEQ</ta>
            <ta e="T654" id="Seg_8799" s="T653">nachdem</ta>
            <ta e="T655" id="Seg_8800" s="T654">dieses-ACC</ta>
            <ta e="T656" id="Seg_8801" s="T655">ganz-3SG-ACC</ta>
            <ta e="T657" id="Seg_8802" s="T656">bedecken-REFL-CVB.SEQ</ta>
            <ta e="T658" id="Seg_8803" s="T657">stehen-CVB.SEQ</ta>
            <ta e="T659" id="Seg_8804" s="T658">am.anderen.Ufer-ADJZ.[NOM]</ta>
            <ta e="T660" id="Seg_8805" s="T659">zu</ta>
            <ta e="T661" id="Seg_8806" s="T660">überqueren-CVB.SIM</ta>
            <ta e="T662" id="Seg_8807" s="T661">streiten-RECP/COLL-CVB.SIM</ta>
            <ta e="T663" id="Seg_8808" s="T662">hinausgehen-EP-PST2-3PL</ta>
            <ta e="T664" id="Seg_8809" s="T663">1PL.[NOM]</ta>
            <ta e="T665" id="Seg_8810" s="T664">Soldat-PL-1PL.[NOM]</ta>
            <ta e="T666" id="Seg_8811" s="T665">jenes</ta>
            <ta e="T667" id="Seg_8812" s="T666">Streit.[NOM]</ta>
            <ta e="T668" id="Seg_8813" s="T667">Lärm-3SG-DAT/LOC</ta>
            <ta e="T669" id="Seg_8814" s="T668">Brücke-DAT/LOC</ta>
            <ta e="T670" id="Seg_8815" s="T669">passen-NEG.CVB.SIM</ta>
            <ta e="T671" id="Seg_8816" s="T670">anderer.von.zwei</ta>
            <ta e="T672" id="Seg_8817" s="T671">Mensch.[NOM]</ta>
            <ta e="T673" id="Seg_8818" s="T672">Boot-INSTR</ta>
            <ta e="T674" id="Seg_8819" s="T673">mancher-PL.[NOM]</ta>
            <ta e="T675" id="Seg_8820" s="T674">wie.viel-DISTR</ta>
            <ta e="T676" id="Seg_8821" s="T675">wieder</ta>
            <ta e="T677" id="Seg_8822" s="T676">Holzklotz-PL-ACC</ta>
            <ta e="T678" id="Seg_8823" s="T677">zusammen</ta>
            <ta e="T679" id="Seg_8824" s="T678">verbinden-CVB.SIM</ta>
            <ta e="T680" id="Seg_8825" s="T679">binden-CVB.SEQ</ta>
            <ta e="T681" id="Seg_8826" s="T680">nachdem</ta>
            <ta e="T682" id="Seg_8827" s="T681">anderer.von.zwei-PL.[NOM]</ta>
            <ta e="T683" id="Seg_8828" s="T682">frei</ta>
            <ta e="T684" id="Seg_8829" s="T683">schwimmen-CVB.SEQ</ta>
            <ta e="T685" id="Seg_8830" s="T684">hinausgehen-EP-PST2-3PL</ta>
            <ta e="T686" id="Seg_8831" s="T685">Dnjepr.[NOM]</ta>
            <ta e="T687" id="Seg_8832" s="T686">sagen-CVB.SEQ</ta>
            <ta e="T688" id="Seg_8833" s="T687">Fluss-ACC</ta>
            <ta e="T689" id="Seg_8834" s="T688">dort</ta>
            <ta e="T690" id="Seg_8835" s="T689">Lärm.[NOM]</ta>
            <ta e="T691" id="Seg_8836" s="T690">groß</ta>
            <ta e="T692" id="Seg_8837" s="T691">Schrei.[NOM]</ta>
            <ta e="T693" id="Seg_8838" s="T692">Schrei.[NOM]</ta>
            <ta e="T694" id="Seg_8839" s="T693">Kampf.[NOM]</ta>
            <ta e="T695" id="Seg_8840" s="T694">was-ABL</ta>
            <ta e="T696" id="Seg_8841" s="T695">NEG</ta>
            <ta e="T697" id="Seg_8842" s="T696">anders</ta>
            <ta e="T698" id="Seg_8843" s="T697">Krieg.[NOM]</ta>
            <ta e="T699" id="Seg_8844" s="T698">Waffe-3SG-GEN</ta>
            <ta e="T700" id="Seg_8845" s="T699">Lärm-3SG.[NOM]</ta>
            <ta e="T701" id="Seg_8846" s="T700">Grauen-PROPR.[NOM]</ta>
            <ta e="T702" id="Seg_8847" s="T701">sein-PST1-3SG</ta>
            <ta e="T703" id="Seg_8848" s="T702">Deutscher.[NOM]</ta>
            <ta e="T704" id="Seg_8849" s="T703">in.Richtung</ta>
            <ta e="T705" id="Seg_8850" s="T704">Seite.[NOM]</ta>
            <ta e="T706" id="Seg_8851" s="T705">dieses</ta>
            <ta e="T707" id="Seg_8852" s="T706">vernichten-CVB.SIM</ta>
            <ta e="T708" id="Seg_8853" s="T707">streiten-NMNZ-ACC</ta>
            <ta e="T709" id="Seg_8854" s="T708">schaffen-CVB.SEQ</ta>
            <ta e="T710" id="Seg_8855" s="T709">halten-NEG.CVB.SIM</ta>
            <ta e="T711" id="Seg_8856" s="T710">vergeblich</ta>
            <ta e="T712" id="Seg_8857" s="T711">Fluss.[NOM]</ta>
            <ta e="T713" id="Seg_8858" s="T712">oberer.Teil-3SG-INSTR</ta>
            <ta e="T714" id="Seg_8859" s="T713">schießen-FREQ-PTCP.PRS</ta>
            <ta e="T715" id="Seg_8860" s="T714">sein-PST1-3SG</ta>
            <ta e="T716" id="Seg_8861" s="T715">Ufer-ABL</ta>
            <ta e="T717" id="Seg_8862" s="T716">sehen-PTCP.COND-DAT/LOC</ta>
            <ta e="T718" id="Seg_8863" s="T717">Fluss.[NOM]</ta>
            <ta e="T719" id="Seg_8864" s="T718">zu</ta>
            <ta e="T720" id="Seg_8865" s="T719">Wasser-DAT/LOC</ta>
            <ta e="T721" id="Seg_8866" s="T720">ganz</ta>
            <ta e="T722" id="Seg_8867" s="T721">Mensch.[NOM]</ta>
            <ta e="T723" id="Seg_8868" s="T722">was-ACC</ta>
            <ta e="T724" id="Seg_8869" s="T723">NEG</ta>
            <ta e="T725" id="Seg_8870" s="T724">verstehen-PTCP.FUT</ta>
            <ta e="T726" id="Seg_8871" s="T725">sein-NEG.PTCP.PST</ta>
            <ta e="T727" id="Seg_8872" s="T726">sein-PST1-3SG</ta>
            <ta e="T728" id="Seg_8873" s="T727">Beispiel-3SG.[NOM]</ta>
            <ta e="T729" id="Seg_8874" s="T728">sich.ängstigen-EP-PTCP.PST</ta>
            <ta e="T730" id="Seg_8875" s="T729">groß</ta>
            <ta e="T731" id="Seg_8876" s="T730">wildes.Rentier.[NOM]</ta>
            <ta e="T732" id="Seg_8877" s="T731">Herde-3SG.[NOM]</ta>
            <ta e="T733" id="Seg_8878" s="T732">schwimmen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T734" id="Seg_8879" s="T733">ähnlich</ta>
            <ta e="T735" id="Seg_8880" s="T734">sein-PST1-3SG</ta>
            <ta e="T736" id="Seg_8881" s="T735">dieses</ta>
            <ta e="T737" id="Seg_8882" s="T736">Fluss.[NOM]</ta>
            <ta e="T738" id="Seg_8883" s="T737">auf.Wasser.bewegen-NMNZ-3SG-DAT/LOC</ta>
            <ta e="T739" id="Seg_8884" s="T738">so.viel</ta>
            <ta e="T740" id="Seg_8885" s="T739">Mensch.[NOM]</ta>
            <ta e="T741" id="Seg_8886" s="T740">ersticken-CVB.SEQ</ta>
            <ta e="T742" id="Seg_8887" s="T741">Krieg.[NOM]</ta>
            <ta e="T743" id="Seg_8888" s="T742">Gewehr-3SG-ABL</ta>
            <ta e="T744" id="Seg_8889" s="T743">sterben-PTCP.PST-3SG-ACC</ta>
            <ta e="T745" id="Seg_8890" s="T744">wer.[NOM]</ta>
            <ta e="T746" id="Seg_8891" s="T745">EMPH</ta>
            <ta e="T747" id="Seg_8892" s="T746">wissen-NEG.PTCP</ta>
            <ta e="T748" id="Seg_8893" s="T747">dann</ta>
            <ta e="T749" id="Seg_8894" s="T748">sein-PST1-3SG</ta>
            <ta e="T750" id="Seg_8895" s="T749">Fluss-ACC</ta>
            <ta e="T751" id="Seg_8896" s="T750">hinausgehen-CVB.SEQ</ta>
            <ta e="T752" id="Seg_8897" s="T751">nachdem</ta>
            <ta e="T753" id="Seg_8898" s="T752">unsere.[NOM]</ta>
            <ta e="T754" id="Seg_8899" s="T753">wieder</ta>
            <ta e="T755" id="Seg_8900" s="T754">Deutscher-PL-ACC</ta>
            <ta e="T756" id="Seg_8901" s="T755">Freizeit.[NOM]</ta>
            <ta e="T757" id="Seg_8902" s="T756">finden-CAUS-NEG.CVB.SIM</ta>
            <ta e="T758" id="Seg_8903" s="T757">genug</ta>
            <ta e="T759" id="Seg_8904" s="T758">jagen-PST2-3PL</ta>
            <ta e="T760" id="Seg_8905" s="T759">dann</ta>
            <ta e="T761" id="Seg_8906" s="T760">1SG.[NOM]</ta>
            <ta e="T762" id="Seg_8907" s="T761">wie.viel</ta>
            <ta e="T763" id="Seg_8908" s="T762">INDEF</ta>
            <ta e="T764" id="Seg_8909" s="T763">Krieg-DAT/LOC</ta>
            <ta e="T765" id="Seg_8910" s="T764">gehen-CVB.SEQ-1SG</ta>
            <ta e="T766" id="Seg_8911" s="T765">vier-ORD-PROPR</ta>
            <ta e="T767" id="Seg_8912" s="T766">Tag-3SG-DAT/LOC</ta>
            <ta e="T768" id="Seg_8913" s="T767">Januar.[NOM]</ta>
            <ta e="T769" id="Seg_8914" s="T768">Monat-DAT/LOC</ta>
            <ta e="T770" id="Seg_8915" s="T769">tausend</ta>
            <ta e="T771" id="Seg_8916" s="T770">neun</ta>
            <ta e="T772" id="Seg_8917" s="T771">hundert</ta>
            <ta e="T773" id="Seg_8918" s="T772">vier-zehn</ta>
            <ta e="T774" id="Seg_8919" s="T773">drei</ta>
            <ta e="T775" id="Seg_8920" s="T774">Jahr-PROPR-DAT/LOC</ta>
            <ta e="T776" id="Seg_8921" s="T775">letzter-1SG-ACC</ta>
            <ta e="T777" id="Seg_8922" s="T776">Wunde-VBZ-EP-CAUS-PST2-EP-1SG</ta>
            <ta e="T778" id="Seg_8923" s="T777">sehr</ta>
            <ta e="T779" id="Seg_8924" s="T778">schwer-ADVZ</ta>
            <ta e="T780" id="Seg_8925" s="T779">linker</ta>
            <ta e="T781" id="Seg_8926" s="T780">in.Richtung</ta>
            <ta e="T782" id="Seg_8927" s="T781">Hinterbein-EP-1SG.[NOM]</ta>
            <ta e="T783" id="Seg_8928" s="T782">Knochen-3SG-ACC</ta>
            <ta e="T784" id="Seg_8929" s="T783">genug</ta>
            <ta e="T785" id="Seg_8930" s="T784">brechen-CAUS-PST2-EP-1SG</ta>
            <ta e="T786" id="Seg_8931" s="T785">dieses-ACC</ta>
            <ta e="T787" id="Seg_8932" s="T786">heilen-EP-CAUS-CVB.SEQ-1SG</ta>
            <ta e="T788" id="Seg_8933" s="T787">gesund.werden-PTCP.FUT-1SG-DAT/LOC</ta>
            <ta e="T789" id="Seg_8934" s="T788">bis.zu</ta>
            <ta e="T790" id="Seg_8935" s="T789">sechs</ta>
            <ta e="T791" id="Seg_8936" s="T790">Monat.[NOM]</ta>
            <ta e="T792" id="Seg_8937" s="T791">solange</ta>
            <ta e="T793" id="Seg_8938" s="T792">liegen-PST2-EP-1SG</ta>
            <ta e="T794" id="Seg_8939" s="T793">Essentuki.[NOM]</ta>
            <ta e="T795" id="Seg_8940" s="T794">sagen-CVB.SEQ</ta>
            <ta e="T796" id="Seg_8941" s="T795">Stadt-DAT/LOC</ta>
            <ta e="T797" id="Seg_8942" s="T796">Kaukasus.[NOM]</ta>
            <ta e="T798" id="Seg_8943" s="T797">sagen-CVB.SEQ</ta>
            <ta e="T799" id="Seg_8944" s="T798">warm</ta>
            <ta e="T800" id="Seg_8945" s="T799">Ort-DAT/LOC</ta>
            <ta e="T801" id="Seg_8946" s="T800">dann</ta>
            <ta e="T802" id="Seg_8947" s="T801">heilen-EP-CAUS-CVB.SEQ</ta>
            <ta e="T803" id="Seg_8948" s="T802">nachdem</ta>
            <ta e="T804" id="Seg_8949" s="T803">Holz.[NOM]</ta>
            <ta e="T805" id="Seg_8950" s="T804">Stab-PROPR.[NOM]</ta>
            <ta e="T806" id="Seg_8951" s="T805">hinausgehen-CVB.SEQ</ta>
            <ta e="T807" id="Seg_8952" s="T806">gehen-CVB.SEQ-1SG</ta>
            <ta e="T808" id="Seg_8953" s="T807">Krieg-ABL</ta>
            <ta e="T809" id="Seg_8954" s="T808">ganz</ta>
            <ta e="T810" id="Seg_8955" s="T809">befreien-EP-MED-PST2-EP-1SG</ta>
            <ta e="T811" id="Seg_8956" s="T810">Armee-ABL</ta>
            <ta e="T813" id="Seg_8957" s="T812">hinausgehen-EP-PST2-EP-1SG</ta>
            <ta e="T814" id="Seg_8958" s="T813">zuerst</ta>
            <ta e="T815" id="Seg_8959" s="T814">kämpfen-PTCP.PST</ta>
            <ta e="T816" id="Seg_8960" s="T815">Tag-1SG-ABL</ta>
            <ta e="T817" id="Seg_8961" s="T816">eins</ta>
            <ta e="T818" id="Seg_8962" s="T817">Jahr.[NOM]</ta>
            <ta e="T819" id="Seg_8963" s="T818">Hälfte-3SG-ACC</ta>
            <ta e="T820" id="Seg_8964" s="T819">solange</ta>
            <ta e="T821" id="Seg_8965" s="T820">Krieg.[NOM]</ta>
            <ta e="T822" id="Seg_8966" s="T821">Kampf-3SG-DAT/LOC</ta>
            <ta e="T823" id="Seg_8967" s="T822">gehen-CVB.SEQ-1SG</ta>
            <ta e="T824" id="Seg_8968" s="T823">ganz</ta>
            <ta e="T825" id="Seg_8969" s="T824">vier-DAT/LOC</ta>
            <ta e="T826" id="Seg_8970" s="T825">Mal.[NOM]</ta>
            <ta e="T827" id="Seg_8971" s="T826">Wunde-VBZ-EP-CAUS-PST2-EP-1SG</ta>
            <ta e="T828" id="Seg_8972" s="T827">dieses.[NOM]</ta>
            <ta e="T829" id="Seg_8973" s="T828">Seite-3SG-INSTR</ta>
            <ta e="T830" id="Seg_8974" s="T829">drei-MLTP</ta>
            <ta e="T831" id="Seg_8975" s="T830">heilen-EP-CAUS-CVB.SIM</ta>
            <ta e="T832" id="Seg_8976" s="T831">gehen-EP-PST2-EP-1SG</ta>
            <ta e="T833" id="Seg_8977" s="T832">kurz-ADVZ</ta>
            <ta e="T834" id="Seg_8978" s="T833">erzählen-PTCP.COND-DAT/LOC</ta>
            <ta e="T835" id="Seg_8979" s="T834">solch</ta>
            <ta e="T836" id="Seg_8980" s="T835">1SG.[NOM]</ta>
            <ta e="T837" id="Seg_8981" s="T836">Mensch.[NOM]</ta>
            <ta e="T838" id="Seg_8982" s="T837">Verwandter.[NOM]</ta>
            <ta e="T839" id="Seg_8983" s="T838">Leben-3SG-DAT/LOC</ta>
            <ta e="T840" id="Seg_8984" s="T839">wann</ta>
            <ta e="T841" id="Seg_8985" s="T840">NEG</ta>
            <ta e="T842" id="Seg_8986" s="T841">wissen-PASS/REFL-EP-NEG.PTCP.PST</ta>
            <ta e="T843" id="Seg_8987" s="T842">Vater.[NOM]</ta>
            <ta e="T844" id="Seg_8988" s="T843">und</ta>
            <ta e="T845" id="Seg_8989" s="T844">dieses</ta>
            <ta e="T846" id="Seg_8990" s="T845">groß</ta>
            <ta e="T847" id="Seg_8991" s="T846">Krieg-3SG-DAT/LOC</ta>
            <ta e="T848" id="Seg_8992" s="T847">selbst-1SG.[NOM]</ta>
            <ta e="T849" id="Seg_8993" s="T848">Leute-1SG-ACC</ta>
            <ta e="T850" id="Seg_8994" s="T849">geboren.werden-PTCP.PST</ta>
            <ta e="T851" id="Seg_8995" s="T850">entstehen-PTCP.PST</ta>
            <ta e="T852" id="Seg_8996" s="T851">Erde-1SG-ACC</ta>
            <ta e="T853" id="Seg_8997" s="T852">schützen-CVB.SIM</ta>
            <ta e="T854" id="Seg_8998" s="T853">gehen-EP-PST2-EP-1SG</ta>
            <ta e="T855" id="Seg_8999" s="T854">mein</ta>
            <ta e="T856" id="Seg_9000" s="T855">Blut-EP-1SG.[NOM]</ta>
            <ta e="T857" id="Seg_9001" s="T856">Schweiß-EP-1SG.[NOM]</ta>
            <ta e="T858" id="Seg_9002" s="T857">dieses</ta>
            <ta e="T859" id="Seg_9003" s="T858">Krieg-DAT/LOC</ta>
            <ta e="T860" id="Seg_9004" s="T859">umsonst</ta>
            <ta e="T861" id="Seg_9005" s="T860">ausgießen-REFL-EP-PST2.NEG-3SG</ta>
            <ta e="T862" id="Seg_9006" s="T861">unser</ta>
            <ta e="T863" id="Seg_9007" s="T862">kommunistisch</ta>
            <ta e="T864" id="Seg_9008" s="T863">Partei-1PL.[NOM]</ta>
            <ta e="T865" id="Seg_9009" s="T864">Zentrum.[NOM]</ta>
            <ta e="T866" id="Seg_9010" s="T865">Komitee-3SG.[NOM]</ta>
            <ta e="T867" id="Seg_9011" s="T866">dann</ta>
            <ta e="T868" id="Seg_9012" s="T867">unser</ta>
            <ta e="T869" id="Seg_9013" s="T868">Staat-1PL-GEN</ta>
            <ta e="T870" id="Seg_9014" s="T869">Leben-3SG-ACC</ta>
            <ta e="T871" id="Seg_9015" s="T870">verbessern-PTCP.HAB</ta>
            <ta e="T872" id="Seg_9016" s="T871">Regierung-1PL.[NOM]</ta>
            <ta e="T873" id="Seg_9017" s="T872">geben-PST2-3PL</ta>
            <ta e="T874" id="Seg_9018" s="T873">1SG-DAT/LOC</ta>
            <ta e="T875" id="Seg_9019" s="T874">Staat.[NOM]</ta>
            <ta e="T876" id="Seg_9020" s="T875">hoch</ta>
            <ta e="T877" id="Seg_9021" s="T876">Krieg-3SG-GEN</ta>
            <ta e="T878" id="Seg_9022" s="T877">Orden-3SG-ACC</ta>
            <ta e="T882" id="Seg_9023" s="T881">sagen-CVB.SEQ</ta>
            <ta e="T883" id="Seg_9024" s="T882">dann</ta>
            <ta e="T884" id="Seg_9025" s="T883">noch</ta>
            <ta e="T885" id="Seg_9026" s="T884">Medaille-ACC</ta>
            <ta e="T890" id="Seg_9027" s="T889">sagen-CVB.SEQ</ta>
            <ta e="T891" id="Seg_9028" s="T890">jetzt</ta>
            <ta e="T892" id="Seg_9029" s="T891">1PL.[NOM]</ta>
            <ta e="T893" id="Seg_9030" s="T892">sowjetisch</ta>
            <ta e="T894" id="Seg_9031" s="T893">Leben-3SG-DAT/LOC</ta>
            <ta e="T895" id="Seg_9032" s="T894">leben-PTCP.PRS</ta>
            <ta e="T896" id="Seg_9033" s="T895">Mensch-PL.[NOM]</ta>
            <ta e="T897" id="Seg_9034" s="T896">Leben-2PL.[NOM]</ta>
            <ta e="T898" id="Seg_9035" s="T897">sehr</ta>
            <ta e="T899" id="Seg_9036" s="T898">klappen-CVB.SEQ</ta>
            <ta e="T900" id="Seg_9037" s="T899">stehen-PRS.[3SG]</ta>
            <ta e="T901" id="Seg_9038" s="T900">Vorderteil.[NOM]</ta>
            <ta e="T902" id="Seg_9039" s="T901">zu</ta>
            <ta e="T903" id="Seg_9040" s="T902">gehen-PTCP.PRS</ta>
            <ta e="T904" id="Seg_9041" s="T903">Kraft-DAT/LOC</ta>
            <ta e="T905" id="Seg_9042" s="T904">jenes.[NOM]</ta>
            <ta e="T906" id="Seg_9043" s="T905">wegen</ta>
            <ta e="T907" id="Seg_9044" s="T906">wer.[NOM]</ta>
            <ta e="T908" id="Seg_9045" s="T907">EMPH</ta>
            <ta e="T909" id="Seg_9046" s="T908">jetzt</ta>
            <ta e="T910" id="Seg_9047" s="T909">Krieg-2SG-ACC</ta>
            <ta e="T911" id="Seg_9048" s="T910">werden-PTCP.FUT-3SG-ACC</ta>
            <ta e="T912" id="Seg_9049" s="T911">wollen-NEG.[3SG]</ta>
            <ta e="T913" id="Seg_9050" s="T912">1PL.[NOM]</ta>
            <ta e="T914" id="Seg_9051" s="T913">Erde-1PL.[NOM]</ta>
            <ta e="T915" id="Seg_9052" s="T914">Mensch-3SG.[NOM]</ta>
            <ta e="T916" id="Seg_9053" s="T915">früher-ABL</ta>
            <ta e="T917" id="Seg_9054" s="T916">EMPH</ta>
            <ta e="T918" id="Seg_9055" s="T917">Krieg-DAT/LOC</ta>
            <ta e="T919" id="Seg_9056" s="T918">wollen-PTCP.HAB-3SG</ta>
            <ta e="T920" id="Seg_9057" s="T919">NEG</ta>
            <ta e="T921" id="Seg_9058" s="T920">sein-PST1-3SG</ta>
            <ta e="T922" id="Seg_9059" s="T921">1PL.[NOM]</ta>
            <ta e="T923" id="Seg_9060" s="T922">Volk-1PL.[NOM]</ta>
            <ta e="T924" id="Seg_9061" s="T923">jetzt</ta>
            <ta e="T925" id="Seg_9062" s="T924">selbst-3SG-GEN</ta>
            <ta e="T926" id="Seg_9063" s="T925">Wunsch-3SG.[NOM]</ta>
            <ta e="T927" id="Seg_9064" s="T926">einverstanden.sein-PTCP.PRS-3SG-ACC</ta>
            <ta e="T928" id="Seg_9065" s="T927">strahlend</ta>
            <ta e="T929" id="Seg_9066" s="T928">glücklich</ta>
            <ta e="T930" id="Seg_9067" s="T929">Lenin.[NOM]</ta>
            <ta e="T931" id="Seg_9068" s="T930">zeigen-PTCP.PST</ta>
            <ta e="T932" id="Seg_9069" s="T931">Kommunismus</ta>
            <ta e="T933" id="Seg_9070" s="T932">Name-PROPR</ta>
            <ta e="T934" id="Seg_9071" s="T933">Leben-3SG-ACC</ta>
            <ta e="T935" id="Seg_9072" s="T934">machen-PRS.[3SG]</ta>
            <ta e="T936" id="Seg_9073" s="T935">1PL.[NOM]</ta>
            <ta e="T937" id="Seg_9074" s="T936">jeder-1PL.[NOM]</ta>
            <ta e="T938" id="Seg_9075" s="T937">sagen-PRS-1PL</ta>
            <ta e="T939" id="Seg_9076" s="T938">jetzt</ta>
            <ta e="T940" id="Seg_9077" s="T939">Krieg.[NOM]</ta>
            <ta e="T941" id="Seg_9078" s="T940">NEG.EX</ta>
            <ta e="T942" id="Seg_9079" s="T941">sein-IMP.3SG</ta>
            <ta e="T943" id="Seg_9080" s="T942">Erde.[NOM]</ta>
            <ta e="T944" id="Seg_9081" s="T943">oberer.Teil-3SG-INSTR</ta>
            <ta e="T945" id="Seg_9082" s="T944">es.gibt</ta>
            <ta e="T946" id="Seg_9083" s="T945">sein-IMP.3SG</ta>
            <ta e="T947" id="Seg_9084" s="T946">strahlend</ta>
            <ta e="T948" id="Seg_9085" s="T947">Sonne-PROPR</ta>
            <ta e="T949" id="Seg_9086" s="T948">Leben.[NOM]</ta>
            <ta e="T950" id="Seg_9087" s="T949">jeder</ta>
            <ta e="T951" id="Seg_9088" s="T950">Volk-DAT/LOC</ta>
         </annotation>
         <annotation name="gr" tierref="gr">
            <ta e="T1" id="Seg_9089" s="T0">1SG.[NOM]</ta>
            <ta e="T2" id="Seg_9090" s="T1">война-DAT/LOC</ta>
            <ta e="T3" id="Seg_9091" s="T2">идти-PST2-EP-1SG</ta>
            <ta e="T4" id="Seg_9092" s="T3">сам-1SG.[NOM]</ta>
            <ta e="T5" id="Seg_9093" s="T4">желание-1SG-INSTR</ta>
            <ta e="T6" id="Seg_9094" s="T5">тысяча</ta>
            <ta e="T7" id="Seg_9095" s="T6">девять</ta>
            <ta e="T8" id="Seg_9096" s="T7">сто</ta>
            <ta e="T9" id="Seg_9097" s="T8">четыре-десять</ta>
            <ta e="T10" id="Seg_9098" s="T9">один</ta>
            <ta e="T11" id="Seg_9099" s="T10">год-PROPR-DAT/LOC</ta>
            <ta e="T12" id="Seg_9100" s="T11">осенний</ta>
            <ta e="T13" id="Seg_9101" s="T12">сентябрь.[NOM]</ta>
            <ta e="T14" id="Seg_9102" s="T13">холод.[NOM]</ta>
            <ta e="T15" id="Seg_9103" s="T14">падать-PTCP.PRS</ta>
            <ta e="T16" id="Seg_9104" s="T15">месяц-3SG-DAT/LOC</ta>
            <ta e="T17" id="Seg_9105" s="T16">сначала</ta>
            <ta e="T18" id="Seg_9106" s="T17">идти-CVB.SEQ-1SG</ta>
            <ta e="T19" id="Seg_9107" s="T18">один</ta>
            <ta e="T20" id="Seg_9108" s="T19">год-ACC</ta>
            <ta e="T21" id="Seg_9109" s="T20">с</ta>
            <ta e="T22" id="Seg_9110" s="T21">девять</ta>
            <ta e="T23" id="Seg_9111" s="T22">месяц-ACC</ta>
            <ta e="T24" id="Seg_9112" s="T23">война.[NOM]</ta>
            <ta e="T25" id="Seg_9113" s="T24">учеба-3SG-DAT/LOC</ta>
            <ta e="T26" id="Seg_9114" s="T25">учиться-CVB.SIM</ta>
            <ta e="T27" id="Seg_9115" s="T26">идти-EP-PST2-EP-1SG</ta>
            <ta e="T29" id="Seg_9116" s="T27">Дальний.Восток.[NOM]</ta>
            <ta e="T30" id="Seg_9117" s="T29">говорить-CVB.SEQ</ta>
            <ta e="T31" id="Seg_9118" s="T30">место-DAT/LOC</ta>
            <ta e="T32" id="Seg_9119" s="T31">потом</ta>
            <ta e="T33" id="Seg_9120" s="T32">летом</ta>
            <ta e="T34" id="Seg_9121" s="T33">тысяча</ta>
            <ta e="T35" id="Seg_9122" s="T34">девять</ta>
            <ta e="T36" id="Seg_9123" s="T35">сто</ta>
            <ta e="T37" id="Seg_9124" s="T36">четыре-десять</ta>
            <ta e="T38" id="Seg_9125" s="T37">три</ta>
            <ta e="T39" id="Seg_9126" s="T38">год-PROPR-DAT/LOC</ta>
            <ta e="T40" id="Seg_9127" s="T39">1PL-ACC</ta>
            <ta e="T41" id="Seg_9128" s="T40">послать-PST2-3PL</ta>
            <ta e="T42" id="Seg_9129" s="T41">запад.[NOM]</ta>
            <ta e="T43" id="Seg_9130" s="T42">к</ta>
            <ta e="T44" id="Seg_9131" s="T43">война.[NOM]</ta>
            <ta e="T45" id="Seg_9132" s="T44">борьба-3SG.[NOM]</ta>
            <ta e="T46" id="Seg_9133" s="T45">идти-CVB.SIM</ta>
            <ta e="T47" id="Seg_9134" s="T46">стоять-PTCP.PRS</ta>
            <ta e="T48" id="Seg_9135" s="T47">место-3SG-DAT/LOC</ta>
            <ta e="T49" id="Seg_9136" s="T48">1PL.[NOM]</ta>
            <ta e="T50" id="Seg_9137" s="T49">война-ACC</ta>
            <ta e="T51" id="Seg_9138" s="T50">встречать-EP-PST2-1PL</ta>
            <ta e="T52" id="Seg_9139" s="T51">Прохоровка.[NOM]</ta>
            <ta e="T53" id="Seg_9140" s="T52">говорить-CVB.SEQ</ta>
            <ta e="T54" id="Seg_9141" s="T53">имя-PROPR</ta>
            <ta e="T55" id="Seg_9142" s="T54">деревня-DAT/LOC</ta>
            <ta e="T56" id="Seg_9143" s="T55">Курск.[NOM]</ta>
            <ta e="T57" id="Seg_9144" s="T56">область-3SG-DAT/LOC</ta>
            <ta e="T58" id="Seg_9145" s="T57">там</ta>
            <ta e="T59" id="Seg_9146" s="T58">немецкий</ta>
            <ta e="T60" id="Seg_9147" s="T59">война-3SG-GEN</ta>
            <ta e="T61" id="Seg_9148" s="T60">человек-3SG.[NOM]</ta>
            <ta e="T62" id="Seg_9149" s="T61">очень</ta>
            <ta e="T63" id="Seg_9150" s="T62">много</ta>
            <ta e="T64" id="Seg_9151" s="T63">война.[NOM]</ta>
            <ta e="T65" id="Seg_9152" s="T64">оружие-3SG-ACC</ta>
            <ta e="T66" id="Seg_9153" s="T65">разный</ta>
            <ta e="T67" id="Seg_9154" s="T66">другой</ta>
            <ta e="T68" id="Seg_9155" s="T67">аммуниция-3SG-ACC</ta>
            <ta e="T69" id="Seg_9156" s="T68">собирать-CVB.SEQ</ta>
            <ta e="T70" id="Seg_9157" s="T69">после</ta>
            <ta e="T71" id="Seg_9158" s="T70">1PL.[NOM]</ta>
            <ta e="T72" id="Seg_9159" s="T71">армия-1PL-ACC</ta>
            <ta e="T73" id="Seg_9160" s="T72">очень</ta>
            <ta e="T74" id="Seg_9161" s="T73">назад</ta>
            <ta e="T75" id="Seg_9162" s="T74">к</ta>
            <ta e="T76" id="Seg_9163" s="T75">тридцать</ta>
            <ta e="T77" id="Seg_9164" s="T76">пять</ta>
            <ta e="T78" id="Seg_9165" s="T77">верста-PROPR</ta>
            <ta e="T79" id="Seg_9166" s="T78">место-DAT/LOC</ta>
            <ta e="T80" id="Seg_9167" s="T79">отодвинуть-PTCP.PST</ta>
            <ta e="T81" id="Seg_9168" s="T80">быть-PST1-3SG</ta>
            <ta e="T82" id="Seg_9169" s="T81">потом</ta>
            <ta e="T83" id="Seg_9170" s="T82">позже</ta>
            <ta e="T84" id="Seg_9171" s="T83">тот</ta>
            <ta e="T85" id="Seg_9172" s="T84">место-ACC</ta>
            <ta e="T86" id="Seg_9173" s="T85">Курск.[NOM]</ta>
            <ta e="T87" id="Seg_9174" s="T86">дуга.[NOM]</ta>
            <ta e="T88" id="Seg_9175" s="T87">говорить-CVB.SEQ</ta>
            <ta e="T89" id="Seg_9176" s="T88">имя-VBZ-PST2-3PL</ta>
            <ta e="T90" id="Seg_9177" s="T89">долганский-SIM</ta>
            <ta e="T91" id="Seg_9178" s="T90">говорить-PTCP.COND-DAT/LOC</ta>
            <ta e="T92" id="Seg_9179" s="T91">Курск.[NOM]</ta>
            <ta e="T93" id="Seg_9180" s="T92">кривой-3SG</ta>
            <ta e="T94" id="Seg_9181" s="T93">говорить-PTCP.FUT-DAT/LOC</ta>
            <ta e="T95" id="Seg_9182" s="T94">правильный.[NOM]</ta>
            <ta e="T96" id="Seg_9183" s="T95">быть-FUT-3SG</ta>
            <ta e="T97" id="Seg_9184" s="T96">немецкий</ta>
            <ta e="T98" id="Seg_9185" s="T97">армия-3SG-GEN</ta>
            <ta e="T99" id="Seg_9186" s="T98">господин-3SG-GEN</ta>
            <ta e="T100" id="Seg_9187" s="T99">мысль-3SG.[NOM]</ta>
            <ta e="T101" id="Seg_9188" s="T100">есть</ta>
            <ta e="T102" id="Seg_9189" s="T101">быть-PST2.[3SG]</ta>
            <ta e="T103" id="Seg_9190" s="T102">тот.[NOM]</ta>
            <ta e="T104" id="Seg_9191" s="T103">земля-3SG-INSTR</ta>
            <ta e="T105" id="Seg_9192" s="T104">сквозь</ta>
            <ta e="T106" id="Seg_9193" s="T105">спорить-CVB.SEQ</ta>
            <ta e="T107" id="Seg_9194" s="T106">после</ta>
            <ta e="T108" id="Seg_9195" s="T107">очень</ta>
            <ta e="T109" id="Seg_9196" s="T108">быстрый-ADVZ</ta>
            <ta e="T110" id="Seg_9197" s="T109">1PL.[NOM]</ta>
            <ta e="T111" id="Seg_9198" s="T110">центр.[NOM]</ta>
            <ta e="T112" id="Seg_9199" s="T111">место-1PL-ACC</ta>
            <ta e="T113" id="Seg_9200" s="T112">Москва.[NOM]</ta>
            <ta e="T114" id="Seg_9201" s="T113">говорить-CVB.SEQ</ta>
            <ta e="T115" id="Seg_9202" s="T114">город-ACC</ta>
            <ta e="T116" id="Seg_9203" s="T115">взять-PTCP.PRS.[NOM]</ta>
            <ta e="T117" id="Seg_9204" s="T116">подобно</ta>
            <ta e="T118" id="Seg_9205" s="T117">наш</ta>
            <ta e="T119" id="Seg_9206" s="T118">часть-1PL.[NOM]</ta>
            <ta e="T120" id="Seg_9207" s="T119">Курск.[NOM]</ta>
            <ta e="T121" id="Seg_9208" s="T120">кривой-3SG-DAT/LOC</ta>
            <ta e="T122" id="Seg_9209" s="T121">что-ABL</ta>
            <ta e="T123" id="Seg_9210" s="T122">NEG</ta>
            <ta e="T124" id="Seg_9211" s="T123">большой</ta>
            <ta e="T125" id="Seg_9212" s="T124">война.[NOM]</ta>
            <ta e="T126" id="Seg_9213" s="T125">огонь-3SG-DAT/LOC</ta>
            <ta e="T127" id="Seg_9214" s="T126">входить-CVB.SEQ</ta>
            <ta e="T128" id="Seg_9215" s="T127">после</ta>
            <ta e="T129" id="Seg_9216" s="T128">десять</ta>
            <ta e="T130" id="Seg_9217" s="T129">день.[NOM]</ta>
            <ta e="T131" id="Seg_9218" s="T130">так.долго</ta>
            <ta e="T132" id="Seg_9219" s="T131">немецкий</ta>
            <ta e="T133" id="Seg_9220" s="T132">армия-3SG-ACC</ta>
            <ta e="T134" id="Seg_9221" s="T133">гнать-CVB.SIM</ta>
            <ta e="T135" id="Seg_9222" s="T134">пытаться-PST2-3SG</ta>
            <ta e="T136" id="Seg_9223" s="T135">тот.[NOM]</ta>
            <ta e="T137" id="Seg_9224" s="T136">так.долго</ta>
            <ta e="T138" id="Seg_9225" s="T137">1PL.[NOM]</ta>
            <ta e="T139" id="Seg_9226" s="T138">воевать-PST2-1PL</ta>
            <ta e="T140" id="Seg_9227" s="T139">день-ACC</ta>
            <ta e="T141" id="Seg_9228" s="T140">ночь-ACC</ta>
            <ta e="T142" id="Seg_9229" s="T141">замечать-NEG.CVB.SIM</ta>
            <ta e="T143" id="Seg_9230" s="T142">спать-NEG.CVB.SIM</ta>
            <ta e="T144" id="Seg_9231" s="T143">NEG</ta>
            <ta e="T145" id="Seg_9232" s="T144">отдыхать-NEG.CVB.SIM</ta>
            <ta e="T146" id="Seg_9233" s="T145">NEG</ta>
            <ta e="T147" id="Seg_9234" s="T146">пример-3SG.[NOM]</ta>
            <ta e="T148" id="Seg_9235" s="T147">что-ACC</ta>
            <ta e="T149" id="Seg_9236" s="T148">NEG</ta>
            <ta e="T150" id="Seg_9237" s="T149">знать-NEG.PTCP.[NOM]</ta>
            <ta e="T151" id="Seg_9238" s="T150">слышать-EP-NEG.PTCP.[NOM]</ta>
            <ta e="T152" id="Seg_9239" s="T151">подобно</ta>
            <ta e="T153" id="Seg_9240" s="T152">два</ta>
            <ta e="T154" id="Seg_9241" s="T153">сторона</ta>
            <ta e="T155" id="Seg_9242" s="T154">воевать-PTCP.HAB</ta>
            <ta e="T156" id="Seg_9243" s="T155">сторона-ABL</ta>
            <ta e="T157" id="Seg_9244" s="T156">столько</ta>
            <ta e="T158" id="Seg_9245" s="T157">тысяча</ta>
            <ta e="T159" id="Seg_9246" s="T158">человек.[NOM]</ta>
            <ta e="T160" id="Seg_9247" s="T159">пропадать-PTCP.PST-3SG-ACC</ta>
            <ta e="T161" id="Seg_9248" s="T160">умирать-PTCP.PST-3SG-ACC</ta>
            <ta e="T162" id="Seg_9249" s="T161">столько</ta>
            <ta e="T163" id="Seg_9250" s="T162">человек.[NOM]</ta>
            <ta e="T164" id="Seg_9251" s="T163">рана.[NOM]</ta>
            <ta e="T165" id="Seg_9252" s="T164">быть-PTCP.PST-3SG-ACC</ta>
            <ta e="T166" id="Seg_9253" s="T165">столько</ta>
            <ta e="T167" id="Seg_9254" s="T166">война.[NOM]</ta>
            <ta e="T168" id="Seg_9255" s="T167">оружие-3SG.[NOM]</ta>
            <ta e="T169" id="Seg_9256" s="T168">самолет-PL.[NOM]</ta>
            <ta e="T170" id="Seg_9257" s="T169">танк-PL.[NOM]</ta>
            <ta e="T171" id="Seg_9258" s="T170">орудие-PL.[NOM]</ta>
            <ta e="T172" id="Seg_9259" s="T171">потом</ta>
            <ta e="T173" id="Seg_9260" s="T172">маленький</ta>
            <ta e="T174" id="Seg_9261" s="T173">разный</ta>
            <ta e="T175" id="Seg_9262" s="T174">ружье-PL.[NOM]</ta>
            <ta e="T176" id="Seg_9263" s="T175">война.[NOM]</ta>
            <ta e="T177" id="Seg_9264" s="T176">аммуниция-3SG.[NOM]</ta>
            <ta e="T178" id="Seg_9265" s="T177">разбивать-PTCP.PST-3SG-ACC</ta>
            <ta e="T179" id="Seg_9266" s="T178">кто.[NOM]</ta>
            <ta e="T180" id="Seg_9267" s="T179">EMPH</ta>
            <ta e="T181" id="Seg_9268" s="T180">особо</ta>
            <ta e="T182" id="Seg_9269" s="T181">знать-NEG.PTCP</ta>
            <ta e="T183" id="Seg_9270" s="T182">быть-PST1-3SG</ta>
            <ta e="T184" id="Seg_9271" s="T183">тогда</ta>
            <ta e="T185" id="Seg_9272" s="T184">как</ta>
            <ta e="T186" id="Seg_9273" s="T185">большой</ta>
            <ta e="T187" id="Seg_9274" s="T186">сильный</ta>
            <ta e="T188" id="Seg_9275" s="T187">война.[NOM]</ta>
            <ta e="T189" id="Seg_9276" s="T188">идти-PTCP.PST-3SG-ACC</ta>
            <ta e="T190" id="Seg_9277" s="T189">тот.[NOM]</ta>
            <ta e="T191" id="Seg_9278" s="T190">место-DAT/LOC</ta>
            <ta e="T192" id="Seg_9279" s="T191">человек.[NOM]</ta>
            <ta e="T193" id="Seg_9280" s="T192">понимать-FUT-3SG</ta>
            <ta e="T194" id="Seg_9281" s="T193">один</ta>
            <ta e="T195" id="Seg_9282" s="T194">такой</ta>
            <ta e="T196" id="Seg_9283" s="T195">пример-ABL</ta>
            <ta e="T197" id="Seg_9284" s="T196">один</ta>
            <ta e="T198" id="Seg_9285" s="T197">лед-PROPR</ta>
            <ta e="T199" id="Seg_9286" s="T198">место-DIM-DAT/LOC</ta>
            <ta e="T200" id="Seg_9287" s="T199">1PL.[NOM]</ta>
            <ta e="T201" id="Seg_9288" s="T200">есть</ta>
            <ta e="T202" id="Seg_9289" s="T201">земля-1PL-DAT/LOC</ta>
            <ta e="T203" id="Seg_9290" s="T202">день-DAT/LOC</ta>
            <ta e="T204" id="Seg_9291" s="T203">однажды</ta>
            <ta e="T205" id="Seg_9292" s="T204">летать-CVB.SEQ</ta>
            <ta e="T206" id="Seg_9293" s="T205">приходить-PTCP.HAB</ta>
            <ta e="T207" id="Seg_9294" s="T206">быть-PST1-3SG</ta>
            <ta e="T208" id="Seg_9295" s="T207">немец.[NOM]</ta>
            <ta e="T209" id="Seg_9296" s="T208">в.сторону</ta>
            <ta e="T210" id="Seg_9297" s="T209">сторона-ABL</ta>
            <ta e="T211" id="Seg_9298" s="T210">два</ta>
            <ta e="T212" id="Seg_9299" s="T211">сто</ta>
            <ta e="T213" id="Seg_9300" s="T212">около-3SG.[NOM]</ta>
            <ta e="T214" id="Seg_9301" s="T213">самолет-PL.[NOM]</ta>
            <ta e="T215" id="Seg_9302" s="T214">тот-3SG-2SG.[NOM]</ta>
            <ta e="T216" id="Seg_9303" s="T215">каждый-3SG.[NOM]</ta>
            <ta e="T217" id="Seg_9304" s="T216">1PL.[NOM]</ta>
            <ta e="T218" id="Seg_9305" s="T217">верхняя.часть-1PL-INSTR</ta>
            <ta e="T219" id="Seg_9306" s="T218">дождь.[NOM]</ta>
            <ta e="T220" id="Seg_9307" s="T219">подобно</ta>
            <ta e="T221" id="Seg_9308" s="T220">падать-CVB.SEQ</ta>
            <ta e="T222" id="Seg_9309" s="T221">э</ta>
            <ta e="T223" id="Seg_9310" s="T222">сто</ta>
            <ta e="T224" id="Seg_9311" s="T223">бомба-PL-ACC</ta>
            <ta e="T225" id="Seg_9312" s="T224">бросать-PTCP.HAB</ta>
            <ta e="T226" id="Seg_9313" s="T225">быть-PST1-3SG</ta>
            <ta e="T227" id="Seg_9314" s="T226">потом</ta>
            <ta e="T228" id="Seg_9315" s="T227">отдельно</ta>
            <ta e="T229" id="Seg_9316" s="T228">еще</ta>
            <ta e="T230" id="Seg_9317" s="T229">стрелять-FREQ-PTCP.PRS</ta>
            <ta e="T231" id="Seg_9318" s="T230">быть-PST1-3SG</ta>
            <ta e="T232" id="Seg_9319" s="T231">сколько</ta>
            <ta e="T233" id="Seg_9320" s="T232">INDEF</ta>
            <ta e="T234" id="Seg_9321" s="T233">сарпа.[NOM]</ta>
            <ta e="T235" id="Seg_9322" s="T234">подобно</ta>
            <ta e="T236" id="Seg_9323" s="T235">зарядить-NMNZ-PROPR</ta>
            <ta e="T237" id="Seg_9324" s="T236">война.[NOM]</ta>
            <ta e="T238" id="Seg_9325" s="T237">оружие-3SG-INSTR</ta>
            <ta e="T239" id="Seg_9326" s="T238">пулемет-EP-INSTR</ta>
            <ta e="T240" id="Seg_9327" s="T239">столько-PROPR</ta>
            <ta e="T241" id="Seg_9328" s="T240">бомба.[NOM]</ta>
            <ta e="T242" id="Seg_9329" s="T241">падать-PTCP.PST-3SG-ACC</ta>
            <ta e="T243" id="Seg_9330" s="T242">после.того</ta>
            <ta e="T244" id="Seg_9331" s="T243">столько-PROPR</ta>
            <ta e="T245" id="Seg_9332" s="T244">оружие.[NOM]</ta>
            <ta e="T246" id="Seg_9333" s="T245">стрелять-FREQ-PTCP.PST-3SG-ACC</ta>
            <ta e="T247" id="Seg_9334" s="T246">после.того</ta>
            <ta e="T248" id="Seg_9335" s="T247">земля.[NOM]</ta>
            <ta e="T249" id="Seg_9336" s="T248">полностью</ta>
            <ta e="T250" id="Seg_9337" s="T249">что-INTNS.[NOM]</ta>
            <ta e="T251" id="Seg_9338" s="T250">NEG</ta>
            <ta e="T252" id="Seg_9339" s="T251">быть.видно-EP-NEG.PTCP.[NOM]</ta>
            <ta e="T253" id="Seg_9340" s="T252">подобно</ta>
            <ta e="T254" id="Seg_9341" s="T253">черный</ta>
            <ta e="T255" id="Seg_9342" s="T254">туман.[NOM]</ta>
            <ta e="T256" id="Seg_9343" s="T255">становиться-PTCP.PRS</ta>
            <ta e="T257" id="Seg_9344" s="T256">быть-PST1-3SG</ta>
            <ta e="T258" id="Seg_9345" s="T257">тот</ta>
            <ta e="T259" id="Seg_9346" s="T258">туман-DAT/LOC</ta>
            <ta e="T260" id="Seg_9347" s="T259">человек.[NOM]</ta>
            <ta e="T261" id="Seg_9348" s="T260">место.около-DAT/LOC</ta>
            <ta e="T262" id="Seg_9349" s="T261">лежать-PTCP.PRS</ta>
            <ta e="T263" id="Seg_9350" s="T262">человек-3SG-ACC</ta>
            <ta e="T264" id="Seg_9351" s="T263">мочь-CVB.SEQ</ta>
            <ta e="T265" id="Seg_9352" s="T264">видеть-NEG.PTCP</ta>
            <ta e="T266" id="Seg_9353" s="T265">быть-PST1-3SG</ta>
            <ta e="T267" id="Seg_9354" s="T266">некоторый.[NOM]</ta>
            <ta e="T268" id="Seg_9355" s="T267">после.того</ta>
            <ta e="T269" id="Seg_9356" s="T268">человек.[NOM]</ta>
            <ta e="T270" id="Seg_9357" s="T269">война.[NOM]</ta>
            <ta e="T271" id="Seg_9358" s="T270">оружие-3SG-GEN</ta>
            <ta e="T272" id="Seg_9359" s="T271">шум-3SG-ABL</ta>
            <ta e="T273" id="Seg_9360" s="T272">что-INTNS-ACC</ta>
            <ta e="T274" id="Seg_9361" s="T273">NEG</ta>
            <ta e="T275" id="Seg_9362" s="T274">слышать-EP-NEG.PTCP.[NOM]</ta>
            <ta e="T276" id="Seg_9363" s="T275">подобно</ta>
            <ta e="T277" id="Seg_9364" s="T276">глухой.[NOM]</ta>
            <ta e="T278" id="Seg_9365" s="T277">становиться-PTCP.PRS</ta>
            <ta e="T279" id="Seg_9366" s="T278">быть-PST1-3SG</ta>
            <ta e="T280" id="Seg_9367" s="T279">десять-ORD</ta>
            <ta e="T281" id="Seg_9368" s="T280">день-1PL-DAT/LOC</ta>
            <ta e="T282" id="Seg_9369" s="T281">1PL.[NOM]</ta>
            <ta e="T283" id="Seg_9370" s="T282">в.сторону</ta>
            <ta e="T284" id="Seg_9371" s="T283">сторона-INSTR</ta>
            <ta e="T285" id="Seg_9372" s="T284">война-ACC</ta>
            <ta e="T286" id="Seg_9373" s="T285">очень</ta>
            <ta e="T287" id="Seg_9374" s="T286">сила-PROPR-ADVZ</ta>
            <ta e="T288" id="Seg_9375" s="T287">идти-PST1-3SG</ta>
            <ta e="T289" id="Seg_9376" s="T288">небо-ABL</ta>
            <ta e="T290" id="Seg_9377" s="T289">земля-ABL</ta>
            <ta e="T291" id="Seg_9378" s="T290">очень</ta>
            <ta e="T292" id="Seg_9379" s="T291">много</ta>
            <ta e="T293" id="Seg_9380" s="T292">самолет-PL.[NOM]</ta>
            <ta e="T294" id="Seg_9381" s="T293">танк-PL.[NOM]</ta>
            <ta e="T295" id="Seg_9382" s="T294">специально</ta>
            <ta e="T296" id="Seg_9383" s="T295">война-DAT/LOC</ta>
            <ta e="T297" id="Seg_9384" s="T296">делаться-EP-PTCP.PST</ta>
            <ta e="T298" id="Seg_9385" s="T297">огонь-PROPR</ta>
            <ta e="T299" id="Seg_9386" s="T298">оружие-PL.[NOM]</ta>
            <ta e="T300" id="Seg_9387" s="T299">Катюша.[NOM]</ta>
            <ta e="T301" id="Seg_9388" s="T300">Андрюша.[NOM]</ta>
            <ta e="T302" id="Seg_9389" s="T301">говорить-CVB.SEQ</ta>
            <ta e="T303" id="Seg_9390" s="T302">имя-PROPR-PL.[NOM]</ta>
            <ta e="T304" id="Seg_9391" s="T303">потом</ta>
            <ta e="T305" id="Seg_9392" s="T304">отдельно</ta>
            <ta e="T306" id="Seg_9393" s="T305">сколько</ta>
            <ta e="T307" id="Seg_9394" s="T306">INDEF</ta>
            <ta e="T308" id="Seg_9395" s="T307">маленький</ta>
            <ta e="T309" id="Seg_9396" s="T308">ружье-PL.[NOM]</ta>
            <ta e="T310" id="Seg_9397" s="T309">тот.[NOM]</ta>
            <ta e="T311" id="Seg_9398" s="T310">каждый-3SG.[NOM]</ta>
            <ta e="T312" id="Seg_9399" s="T311">вместе</ta>
            <ta e="T313" id="Seg_9400" s="T312">бить-PST1-3PL</ta>
            <ta e="T314" id="Seg_9401" s="T313">немец-PL.[NOM]</ta>
            <ta e="T315" id="Seg_9402" s="T314">есть</ta>
            <ta e="T316" id="Seg_9403" s="T315">место-3PL-ACC</ta>
            <ta e="T317" id="Seg_9404" s="T316">немецкий</ta>
            <ta e="T318" id="Seg_9405" s="T317">армия-3SG.[NOM]</ta>
            <ta e="T319" id="Seg_9406" s="T318">тот-ACC</ta>
            <ta e="T320" id="Seg_9407" s="T319">волна-PROPR</ta>
            <ta e="T321" id="Seg_9408" s="T320">большой</ta>
            <ta e="T322" id="Seg_9409" s="T321">бить-NMNZ-ACC</ta>
            <ta e="T323" id="Seg_9410" s="T322">выносить-NEG.CVB.SIM</ta>
            <ta e="T324" id="Seg_9411" s="T323">назад</ta>
            <ta e="T325" id="Seg_9412" s="T324">проваливать-PST2-3SG</ta>
            <ta e="T326" id="Seg_9413" s="T325">сразу-INTNS</ta>
            <ta e="T327" id="Seg_9414" s="T326">десять</ta>
            <ta e="T328" id="Seg_9415" s="T327">пять</ta>
            <ta e="T329" id="Seg_9416" s="T328">верста-PROPR</ta>
            <ta e="T330" id="Seg_9417" s="T329">место-ACC</ta>
            <ta e="T331" id="Seg_9418" s="T330">назад</ta>
            <ta e="T332" id="Seg_9419" s="T331">прыгать-PST2-3SG</ta>
            <ta e="T333" id="Seg_9420" s="T332">тот.[NOM]</ta>
            <ta e="T334" id="Seg_9421" s="T333">большой</ta>
            <ta e="T335" id="Seg_9422" s="T334">бить-NMNZ.[NOM]</ta>
            <ta e="T336" id="Seg_9423" s="T335">задняя.часть-3SG-ABL</ta>
            <ta e="T337" id="Seg_9424" s="T336">Курск.[NOM]</ta>
            <ta e="T338" id="Seg_9425" s="T337">говорить-CVB.SEQ</ta>
            <ta e="T339" id="Seg_9426" s="T338">кривой-3SG.[NOM]</ta>
            <ta e="T340" id="Seg_9427" s="T339">очень</ta>
            <ta e="T341" id="Seg_9428" s="T340">становиться.лучше-PST2-3SG</ta>
            <ta e="T342" id="Seg_9429" s="T341">немецкий</ta>
            <ta e="T343" id="Seg_9430" s="T342">армия-3SG-ACC</ta>
            <ta e="T344" id="Seg_9431" s="T343">назад</ta>
            <ta e="T345" id="Seg_9432" s="T344">1PL.[NOM]</ta>
            <ta e="T346" id="Seg_9433" s="T345">гнать-PST2-1PL</ta>
            <ta e="T347" id="Seg_9434" s="T346">тот.[NOM]</ta>
            <ta e="T348" id="Seg_9435" s="T347">задняя.часть-3SG-ABL</ta>
            <ta e="T349" id="Seg_9436" s="T348">немец-PL.[NOM]</ta>
            <ta e="T350" id="Seg_9437" s="T349">нехороший</ta>
            <ta e="T351" id="Seg_9438" s="T350">солдат-3PL.[NOM]</ta>
            <ta e="T352" id="Seg_9439" s="T351">передняя.часть-3PL-ACC</ta>
            <ta e="T353" id="Seg_9440" s="T352">1PL-DAT/LOC</ta>
            <ta e="T354" id="Seg_9441" s="T353">давать-CVB.SEQ</ta>
            <ta e="T355" id="Seg_9442" s="T354">после</ta>
            <ta e="T356" id="Seg_9443" s="T355">прямо</ta>
            <ta e="T357" id="Seg_9444" s="T356">война.[NOM]</ta>
            <ta e="T358" id="Seg_9445" s="T357">кончать-PTCP.FUT-3SG-DAT/LOC</ta>
            <ta e="T359" id="Seg_9446" s="T358">пока</ta>
            <ta e="T360" id="Seg_9447" s="T359">ум-3PL-ACC-разум-3PL-ACC</ta>
            <ta e="T361" id="Seg_9448" s="T360">найти-PST2.NEG-3PL</ta>
            <ta e="T362" id="Seg_9449" s="T361">убежать-CVB.SEQ</ta>
            <ta e="T363" id="Seg_9450" s="T362">идти-PST2-3PL</ta>
            <ta e="T364" id="Seg_9451" s="T363">1PL.[NOM]</ta>
            <ta e="T365" id="Seg_9452" s="T364">армия-1PL.[NOM]</ta>
            <ta e="T366" id="Seg_9453" s="T365">очень</ta>
            <ta e="T367" id="Seg_9454" s="T366">хороший-ADVZ</ta>
            <ta e="T368" id="Seg_9455" s="T367">сила-3SG-ACC</ta>
            <ta e="T369" id="Seg_9456" s="T368">хватать-CVB.SEQ</ta>
            <ta e="T370" id="Seg_9457" s="T369">после</ta>
            <ta e="T371" id="Seg_9458" s="T370">немец-PL-ACC</ta>
            <ta e="T372" id="Seg_9459" s="T371">задняя.часть.[NOM]</ta>
            <ta e="T373" id="Seg_9460" s="T372">к</ta>
            <ta e="T374" id="Seg_9461" s="T373">показывать-NEG.CVB.SIM</ta>
            <ta e="T375" id="Seg_9462" s="T374">вдоволь</ta>
            <ta e="T376" id="Seg_9463" s="T375">гнать-PST2-3SG</ta>
            <ta e="T377" id="Seg_9464" s="T376">1SG.[NOM]</ta>
            <ta e="T378" id="Seg_9465" s="T377">поздний-ADJZ</ta>
            <ta e="T379" id="Seg_9466" s="T378">день-PL-DAT/LOC</ta>
            <ta e="T380" id="Seg_9467" s="T379">война-DAT/LOC</ta>
            <ta e="T381" id="Seg_9468" s="T380">идти-CVB.SEQ-1SG</ta>
            <ta e="T382" id="Seg_9469" s="T381">такой</ta>
            <ta e="T383" id="Seg_9470" s="T382">ужас-PROPR</ta>
            <ta e="T384" id="Seg_9471" s="T383">человек.[NOM]</ta>
            <ta e="T385" id="Seg_9472" s="T952">подобно</ta>
            <ta e="T387" id="Seg_9473" s="T385">видеть-PST2.NEG-EP-1SG</ta>
            <ta e="T388" id="Seg_9474" s="T387">1SG.[NOM]</ta>
            <ta e="T389" id="Seg_9475" s="T388">тот</ta>
            <ta e="T390" id="Seg_9476" s="T389">место-DAT/LOC</ta>
            <ta e="T391" id="Seg_9477" s="T390">впервые</ta>
            <ta e="T392" id="Seg_9478" s="T391">видеть-PST2-EP-1SG</ta>
            <ta e="T393" id="Seg_9479" s="T392">жара-PROPR</ta>
            <ta e="T394" id="Seg_9480" s="T393">борьба-ACC</ta>
            <ta e="T395" id="Seg_9481" s="T394">как</ta>
            <ta e="T396" id="Seg_9482" s="T395">война.[NOM]</ta>
            <ta e="T397" id="Seg_9483" s="T396">человек-3SG-GEN</ta>
            <ta e="T398" id="Seg_9484" s="T397">кровь-3SG.[NOM]</ta>
            <ta e="T399" id="Seg_9485" s="T398">река-SIM</ta>
            <ta e="T400" id="Seg_9486" s="T399">выливать-MED-PTCP.PRS-3SG-ACC</ta>
            <ta e="T401" id="Seg_9487" s="T400">столько</ta>
            <ta e="T402" id="Seg_9488" s="T401">тысяча</ta>
            <ta e="T403" id="Seg_9489" s="T402">человек.[NOM]</ta>
            <ta e="T404" id="Seg_9490" s="T403">дыхание-3SG.[NOM]</ta>
            <ta e="T405" id="Seg_9491" s="T404">кончаться-PTCP.PRS-3SG-ACC</ta>
            <ta e="T406" id="Seg_9492" s="T405">столько</ta>
            <ta e="T407" id="Seg_9493" s="T406">рана.[NOM]</ta>
            <ta e="T408" id="Seg_9494" s="T407">становиться-PTCP.PST</ta>
            <ta e="T409" id="Seg_9495" s="T408">человек.[NOM]</ta>
            <ta e="T410" id="Seg_9496" s="T409">большой-ADVZ</ta>
            <ta e="T411" id="Seg_9497" s="T410">мука-VBZ-PTCP.PRS-3SG-ACC</ta>
            <ta e="T412" id="Seg_9498" s="T411">1SG.[NOM]</ta>
            <ta e="T413" id="Seg_9499" s="T412">тоже</ta>
            <ta e="T414" id="Seg_9500" s="T413">тот</ta>
            <ta e="T415" id="Seg_9501" s="T414">бить-EP-RECP/COLL-NMNZ-DAT/LOC</ta>
            <ta e="T416" id="Seg_9502" s="T415">Курск.[NOM]</ta>
            <ta e="T417" id="Seg_9503" s="T416">мыс-3SG-DAT/LOC</ta>
            <ta e="T418" id="Seg_9504" s="T417">отделение.[NOM]</ta>
            <ta e="T419" id="Seg_9505" s="T418">командир-3SG.[NOM]</ta>
            <ta e="T420" id="Seg_9506" s="T419">быть-CVB.SIM</ta>
            <ta e="T421" id="Seg_9507" s="T420">идти-CVB.SEQ-1SG</ta>
            <ta e="T422" id="Seg_9508" s="T421">легкий-ADVZ</ta>
            <ta e="T423" id="Seg_9509" s="T422">левый</ta>
            <ta e="T424" id="Seg_9510" s="T423">нога-1SG-GEN</ta>
            <ta e="T425" id="Seg_9511" s="T424">мышца-ACC</ta>
            <ta e="T426" id="Seg_9512" s="T425">рана-VBZ-EP-CAUS-PST2-EP-1SG</ta>
            <ta e="T427" id="Seg_9513" s="T426">тот.[NOM]</ta>
            <ta e="T428" id="Seg_9514" s="T427">быть-CVB.SEQ</ta>
            <ta e="T429" id="Seg_9515" s="T428">после</ta>
            <ta e="T430" id="Seg_9516" s="T429">воевать-PTCP.PRS</ta>
            <ta e="T431" id="Seg_9517" s="T430">место-ABL</ta>
            <ta e="T432" id="Seg_9518" s="T431">выйти-EP-PST2.NEG-EP-1SG</ta>
            <ta e="T433" id="Seg_9519" s="T432">тот</ta>
            <ta e="T434" id="Seg_9520" s="T433">земля-PROPR</ta>
            <ta e="T435" id="Seg_9521" s="T434">бить-EP-RECP/COLL-NMNZ-DAT/LOC</ta>
            <ta e="T436" id="Seg_9522" s="T435">1SG.[NOM]</ta>
            <ta e="T437" id="Seg_9523" s="T436">есть</ta>
            <ta e="T438" id="Seg_9524" s="T437">только</ta>
            <ta e="T439" id="Seg_9525" s="T438">часть-EP-1SG.[NOM]</ta>
            <ta e="T440" id="Seg_9526" s="T439">тысяча-ABL</ta>
            <ta e="T441" id="Seg_9527" s="T440">больше</ta>
            <ta e="T442" id="Seg_9528" s="T441">война.[NOM]</ta>
            <ta e="T443" id="Seg_9529" s="T442">человек-3SG-ABL</ta>
            <ta e="T444" id="Seg_9530" s="T443">тот</ta>
            <ta e="T445" id="Seg_9531" s="T444">десять</ta>
            <ta e="T446" id="Seg_9532" s="T445">день-DAT/LOC</ta>
            <ta e="T447" id="Seg_9533" s="T446">бить-EP-RECP/COLL-NMNZ-DAT/LOC</ta>
            <ta e="T448" id="Seg_9534" s="T447">быть.лишным-PST2-3SG</ta>
            <ta e="T449" id="Seg_9535" s="T448">десять</ta>
            <ta e="T450" id="Seg_9536" s="T449">один</ta>
            <ta e="T451" id="Seg_9537" s="T450">только</ta>
            <ta e="T452" id="Seg_9538" s="T451">человек.[NOM]</ta>
            <ta e="T453" id="Seg_9539" s="T452">тот.[NOM]</ta>
            <ta e="T454" id="Seg_9540" s="T453">нутро-3SG-DAT/LOC</ta>
            <ta e="T455" id="Seg_9541" s="T454">еще</ta>
            <ta e="T456" id="Seg_9542" s="T455">один</ta>
            <ta e="T457" id="Seg_9543" s="T456">1SG.[NOM]</ta>
            <ta e="T458" id="Seg_9544" s="T457">рана.[NOM]</ta>
            <ta e="T459" id="Seg_9545" s="T458">становиться-PTCP.PST</ta>
            <ta e="T460" id="Seg_9546" s="T459">человек.[NOM]</ta>
            <ta e="T461" id="Seg_9547" s="T460">есть</ta>
            <ta e="T462" id="Seg_9548" s="T461">быть-PST1-1SG</ta>
            <ta e="T463" id="Seg_9549" s="T462">тот-ABL</ta>
            <ta e="T464" id="Seg_9550" s="T463">правильный.[NOM]</ta>
            <ta e="T465" id="Seg_9551" s="T464">быть-FUT-3SG</ta>
            <ta e="T466" id="Seg_9552" s="T465">устанавливать-PTCP.FUT-DAT/LOC</ta>
            <ta e="T467" id="Seg_9553" s="T466">как</ta>
            <ta e="T468" id="Seg_9554" s="T467">беда-PROPR.[NOM]</ta>
            <ta e="T469" id="Seg_9555" s="T468">война.[NOM]</ta>
            <ta e="T470" id="Seg_9556" s="T469">быть-PTCP.PST-3SG-ACC</ta>
            <ta e="T471" id="Seg_9557" s="T470">Курск.[NOM]</ta>
            <ta e="T472" id="Seg_9558" s="T471">кривой-3SG-DAT/LOC</ta>
            <ta e="T473" id="Seg_9559" s="T472">сколько</ta>
            <ta e="T474" id="Seg_9560" s="T473">кровь.[NOM]</ta>
            <ta e="T475" id="Seg_9561" s="T474">выливать-MED-EP-PTCP.PST-3SG-ACC</ta>
            <ta e="T476" id="Seg_9562" s="T475">сколько</ta>
            <ta e="T477" id="Seg_9563" s="T476">человек.[NOM]</ta>
            <ta e="T478" id="Seg_9564" s="T477">умирать-PTCP.PST-3SG-ACC</ta>
            <ta e="T479" id="Seg_9565" s="T478">Курск-DAT/LOC</ta>
            <ta e="T480" id="Seg_9566" s="T479">кончать-CVB.SEQ</ta>
            <ta e="T481" id="Seg_9567" s="T480">после</ta>
            <ta e="T482" id="Seg_9568" s="T481">1PL.[NOM]</ta>
            <ta e="T483" id="Seg_9569" s="T482">армия-1PL.[NOM]</ta>
            <ta e="T484" id="Seg_9570" s="T483">назад</ta>
            <ta e="T485" id="Seg_9571" s="T484">спасаться-PTCP.PRS</ta>
            <ta e="T486" id="Seg_9572" s="T485">немец-ACC</ta>
            <ta e="T487" id="Seg_9573" s="T486">запад.[NOM]</ta>
            <ta e="T488" id="Seg_9574" s="T487">к</ta>
            <ta e="T489" id="Seg_9575" s="T488">гнать-CVB.SEQ</ta>
            <ta e="T490" id="Seg_9576" s="T489">идти-PST2-3SG</ta>
            <ta e="T491" id="Seg_9577" s="T490">тот</ta>
            <ta e="T492" id="Seg_9578" s="T491">Курск.[NOM]</ta>
            <ta e="T493" id="Seg_9579" s="T492">задняя.часть-3SG-ABL</ta>
            <ta e="T494" id="Seg_9580" s="T493">1SG.[NOM]</ta>
            <ta e="T495" id="Seg_9581" s="T494">самый</ta>
            <ta e="T496" id="Seg_9582" s="T495">война.[NOM]</ta>
            <ta e="T497" id="Seg_9583" s="T496">борьба.[NOM]</ta>
            <ta e="T498" id="Seg_9584" s="T497">место-3SG-DAT/LOC</ta>
            <ta e="T499" id="Seg_9585" s="T498">есть</ta>
            <ta e="T500" id="Seg_9586" s="T499">быть-PST1-1SG</ta>
            <ta e="T501" id="Seg_9587" s="T500">тысяча</ta>
            <ta e="T502" id="Seg_9588" s="T501">девять</ta>
            <ta e="T503" id="Seg_9589" s="T502">сто</ta>
            <ta e="T504" id="Seg_9590" s="T503">четыре-десять</ta>
            <ta e="T505" id="Seg_9591" s="T504">четыре</ta>
            <ta e="T506" id="Seg_9592" s="T505">год-PROPR-DAT/LOC</ta>
            <ta e="T507" id="Seg_9593" s="T506">зима-ADJZ</ta>
            <ta e="T508" id="Seg_9594" s="T507">январь.[NOM]</ta>
            <ta e="T509" id="Seg_9595" s="T508">месяц-DAT/LOC</ta>
            <ta e="T510" id="Seg_9596" s="T509">пока</ta>
            <ta e="T511" id="Seg_9597" s="T510">тот</ta>
            <ta e="T512" id="Seg_9598" s="T511">час-DAT/LOC</ta>
            <ta e="T513" id="Seg_9599" s="T512">немецкий</ta>
            <ta e="T514" id="Seg_9600" s="T513">солдат-3PL-ACC</ta>
            <ta e="T515" id="Seg_9601" s="T514">с</ta>
            <ta e="T516" id="Seg_9602" s="T515">навстречу</ta>
            <ta e="T517" id="Seg_9603" s="T516">стоять-CVB.SEQ</ta>
            <ta e="T518" id="Seg_9604" s="T517">воевать-PST2-EP-1SG</ta>
            <ta e="T519" id="Seg_9605" s="T518">такой</ta>
            <ta e="T520" id="Seg_9606" s="T519">место-PL-DAT/LOC</ta>
            <ta e="T521" id="Seg_9607" s="T520">Белгород.[NOM]</ta>
            <ta e="T522" id="Seg_9608" s="T521">потом</ta>
            <ta e="T523" id="Seg_9609" s="T522">Харьков.[NOM]</ta>
            <ta e="T524" id="Seg_9610" s="T523">потом</ta>
            <ta e="T525" id="Seg_9611" s="T524">Полтава.[NOM]</ta>
            <ta e="T526" id="Seg_9612" s="T525">потом</ta>
            <ta e="T527" id="Seg_9613" s="T526">Кременчуг.[NOM]</ta>
            <ta e="T528" id="Seg_9614" s="T527">потом</ta>
            <ta e="T529" id="Seg_9615" s="T528">Крюков.[NOM]</ta>
            <ta e="T530" id="Seg_9616" s="T529">говорить-CVB.SEQ</ta>
            <ta e="T531" id="Seg_9617" s="T530">имя-PROPR</ta>
            <ta e="T532" id="Seg_9618" s="T531">большой</ta>
            <ta e="T533" id="Seg_9619" s="T532">город-PL-ACC</ta>
            <ta e="T534" id="Seg_9620" s="T533">взять-PTCP.PRS-DAT/LOC</ta>
            <ta e="T535" id="Seg_9621" s="T534">украинский</ta>
            <ta e="T536" id="Seg_9622" s="T535">говорить-CVB.SEQ</ta>
            <ta e="T537" id="Seg_9623" s="T536">народ.[NOM]</ta>
            <ta e="T538" id="Seg_9624" s="T537">земля-3SG-DAT/LOC</ta>
            <ta e="T539" id="Seg_9625" s="T538">тот</ta>
            <ta e="T540" id="Seg_9626" s="T539">город-PL-ABL</ta>
            <ta e="T541" id="Seg_9627" s="T540">очень</ta>
            <ta e="T542" id="Seg_9628" s="T541">трудный-ADVZ</ta>
            <ta e="T543" id="Seg_9629" s="T542">1PL.[NOM]</ta>
            <ta e="T544" id="Seg_9630" s="T543">воевать-PST2-1PL</ta>
            <ta e="T545" id="Seg_9631" s="T544">немец-ACC</ta>
            <ta e="T546" id="Seg_9632" s="T545">с</ta>
            <ta e="T547" id="Seg_9633" s="T546">Харьков.[NOM]</ta>
            <ta e="T548" id="Seg_9634" s="T547">город.[NOM]</ta>
            <ta e="T549" id="Seg_9635" s="T548">отнимать-EP-RECP/COLL-NMNZ-DAT/LOC</ta>
            <ta e="T550" id="Seg_9636" s="T549">тот</ta>
            <ta e="T551" id="Seg_9637" s="T550">город.[NOM]</ta>
            <ta e="T552" id="Seg_9638" s="T551">три-DAT/LOC</ta>
            <ta e="T553" id="Seg_9639" s="T552">пока</ta>
            <ta e="T554" id="Seg_9640" s="T553">бзад.и.вперед</ta>
            <ta e="T555" id="Seg_9641" s="T554">рука-ABL</ta>
            <ta e="T556" id="Seg_9642" s="T555">рука-DAT/LOC</ta>
            <ta e="T557" id="Seg_9643" s="T556">приходить-CVB.SIM</ta>
            <ta e="T558" id="Seg_9644" s="T557">идти-EP-PST2-3SG</ta>
            <ta e="T559" id="Seg_9645" s="T558">потом</ta>
            <ta e="T560" id="Seg_9646" s="T559">три-ORD</ta>
            <ta e="T561" id="Seg_9647" s="T560">приходить-NMNZ-3SG-DAT/LOC</ta>
            <ta e="T562" id="Seg_9648" s="T561">1PL.[NOM]</ta>
            <ta e="T563" id="Seg_9649" s="T562">рука-1PL-ABL</ta>
            <ta e="T564" id="Seg_9650" s="T563">пустить-PST2.NEG-1PL</ta>
            <ta e="T565" id="Seg_9651" s="T564">потом</ta>
            <ta e="T566" id="Seg_9652" s="T565">большой</ta>
            <ta e="T567" id="Seg_9653" s="T566">бить-EP-RECP/COLL-NMNZ.[NOM]</ta>
            <ta e="T568" id="Seg_9654" s="T567">еще</ta>
            <ta e="T569" id="Seg_9655" s="T568">есть</ta>
            <ta e="T570" id="Seg_9656" s="T569">быть-PST1-3SG</ta>
            <ta e="T571" id="Seg_9657" s="T570">Днепр.[NOM]</ta>
            <ta e="T572" id="Seg_9658" s="T571">говорить-CVB.SEQ</ta>
            <ta e="T573" id="Seg_9659" s="T572">имя-PROPR</ta>
            <ta e="T574" id="Seg_9660" s="T573">река.[NOM]</ta>
            <ta e="T575" id="Seg_9661" s="T574">идти.по.воде-NMNZ-3SG-DAT/LOC</ta>
            <ta e="T576" id="Seg_9662" s="T575">тот</ta>
            <ta e="T577" id="Seg_9663" s="T576">река.[NOM]</ta>
            <ta e="T578" id="Seg_9664" s="T577">на.том.берегу-ADJZ</ta>
            <ta e="T579" id="Seg_9665" s="T578">сторона-3SG-DAT/LOC</ta>
            <ta e="T580" id="Seg_9666" s="T579">сидеть-CVB.SEQ</ta>
            <ta e="T581" id="Seg_9667" s="T580">немец-PL.[NOM]</ta>
            <ta e="T582" id="Seg_9668" s="T581">день-PL-ACC-ночь-PL-ACC</ta>
            <ta e="T583" id="Seg_9669" s="T582">охранять-CVB.SIM</ta>
            <ta e="T584" id="Seg_9670" s="T583">идти-CVB.SEQ-3PL</ta>
            <ta e="T585" id="Seg_9671" s="T584">1PL-ACC</ta>
            <ta e="T586" id="Seg_9672" s="T585">вообще</ta>
            <ta e="T587" id="Seg_9673" s="T586">мочь-CVB.SEQ</ta>
            <ta e="T588" id="Seg_9674" s="T587">идти.по.воде-EP-CAUS.[CAUS]-NEG.PTCP</ta>
            <ta e="T589" id="Seg_9675" s="T588">быть-PST1-3PL</ta>
            <ta e="T590" id="Seg_9676" s="T589">тот.[NOM]</ta>
            <ta e="T591" id="Seg_9677" s="T590">из_за</ta>
            <ta e="T592" id="Seg_9678" s="T591">1PL.[NOM]</ta>
            <ta e="T593" id="Seg_9679" s="T592">сколько</ta>
            <ta e="T594" id="Seg_9680" s="T593">INDEF</ta>
            <ta e="T595" id="Seg_9681" s="T594">день-ACC</ta>
            <ta e="T596" id="Seg_9682" s="T595">мочь-CVB.SEQ</ta>
            <ta e="T597" id="Seg_9683" s="T596">выйти-EP-PST2.NEG-1PL</ta>
            <ta e="T598" id="Seg_9684" s="T597">та.сторона-ADJZ</ta>
            <ta e="T599" id="Seg_9685" s="T598">берег-DAT/LOC</ta>
            <ta e="T600" id="Seg_9686" s="T599">сидеть-PST2-1PL</ta>
            <ta e="T601" id="Seg_9687" s="T600">река-ACC</ta>
            <ta e="T602" id="Seg_9688" s="T601">выйти-PTCP.PRS</ta>
            <ta e="T603" id="Seg_9689" s="T602">мост-PL-ACC</ta>
            <ta e="T604" id="Seg_9690" s="T603">днем</ta>
            <ta e="T605" id="Seg_9691" s="T604">делать-PTCP.PRS-1PL-ACC</ta>
            <ta e="T606" id="Seg_9692" s="T605">немец-PL.[NOM]</ta>
            <ta e="T607" id="Seg_9693" s="T606">самолет-3PL.[NOM]</ta>
            <ta e="T608" id="Seg_9694" s="T607">летать-CVB.SEQ</ta>
            <ta e="T609" id="Seg_9695" s="T608">приходить-CVB.SIM-приходить-CVB.SIM</ta>
            <ta e="T610" id="Seg_9696" s="T609">сколько</ta>
            <ta e="T611" id="Seg_9697" s="T610">INDEF</ta>
            <ta e="T612" id="Seg_9698" s="T611">бомба-PL-ACC</ta>
            <ta e="T613" id="Seg_9699" s="T612">бросать-FREQ-CVB.SEQ</ta>
            <ta e="T614" id="Seg_9700" s="T613">вдоволь</ta>
            <ta e="T615" id="Seg_9701" s="T614">разбивать-PTCP.PRS</ta>
            <ta e="T616" id="Seg_9702" s="T615">быть-PST1-3SG</ta>
            <ta e="T617" id="Seg_9703" s="T616">тот.[NOM]</ta>
            <ta e="T618" id="Seg_9704" s="T617">из_за</ta>
            <ta e="T619" id="Seg_9705" s="T618">позже</ta>
            <ta e="T620" id="Seg_9706" s="T619">1PL.[NOM]</ta>
            <ta e="T621" id="Seg_9707" s="T620">ночью</ta>
            <ta e="T622" id="Seg_9708" s="T621">несколько</ta>
            <ta e="T623" id="Seg_9709" s="T622">место-EP-INSTR</ta>
            <ta e="T624" id="Seg_9710" s="T623">мост-PL-ACC</ta>
            <ta e="T625" id="Seg_9711" s="T624">делать-ITER-PTCP.PRS</ta>
            <ta e="T626" id="Seg_9712" s="T625">быть-PST1-1PL</ta>
            <ta e="T627" id="Seg_9713" s="T626">злой.дух.[NOM]</ta>
            <ta e="T628" id="Seg_9714" s="T627">немец.[NOM]</ta>
            <ta e="T629" id="Seg_9715" s="T628">ночью</ta>
            <ta e="T630" id="Seg_9716" s="T629">да</ta>
            <ta e="T631" id="Seg_9717" s="T630">делать-PTCP.PST</ta>
            <ta e="T632" id="Seg_9718" s="T631">мост-PL-1PL-ACC</ta>
            <ta e="T633" id="Seg_9719" s="T632">стоять-CAUS-NEG.PTCP</ta>
            <ta e="T634" id="Seg_9720" s="T633">быть-PST1-3SG</ta>
            <ta e="T635" id="Seg_9721" s="T634">тогда</ta>
            <ta e="T636" id="Seg_9722" s="T635">1PL.[NOM]</ta>
            <ta e="T637" id="Seg_9723" s="T636">сторона-PL.[NOM]</ta>
            <ta e="T638" id="Seg_9724" s="T637">вдоволь</ta>
            <ta e="T639" id="Seg_9725" s="T638">рассердиться-CVB.SEQ</ta>
            <ta e="T640" id="Seg_9726" s="T639">после</ta>
            <ta e="T641" id="Seg_9727" s="T640">каждый</ta>
            <ta e="T642" id="Seg_9728" s="T641">война.[NOM]</ta>
            <ta e="T643" id="Seg_9729" s="T642">оружие-3SG-INSTR</ta>
            <ta e="T644" id="Seg_9730" s="T643">вдоволь</ta>
            <ta e="T645" id="Seg_9731" s="T644">стрелять-FREQ-CVB.SEQ</ta>
            <ta e="T646" id="Seg_9732" s="T645">после</ta>
            <ta e="T647" id="Seg_9733" s="T646">сколько</ta>
            <ta e="T648" id="Seg_9734" s="T647">INDEF</ta>
            <ta e="T649" id="Seg_9735" s="T648">десять-DISTR</ta>
            <ta e="T650" id="Seg_9736" s="T649">самолет-PL-ACC</ta>
            <ta e="T651" id="Seg_9737" s="T650">на.том.берегу-ADJZ</ta>
            <ta e="T652" id="Seg_9738" s="T651">сторона-3SG-DAT/LOC</ta>
            <ta e="T653" id="Seg_9739" s="T652">послать-CVB.SEQ</ta>
            <ta e="T654" id="Seg_9740" s="T653">после</ta>
            <ta e="T655" id="Seg_9741" s="T654">тот-ACC</ta>
            <ta e="T656" id="Seg_9742" s="T655">целый-3SG-ACC</ta>
            <ta e="T657" id="Seg_9743" s="T656">накрывать-REFL-CVB.SEQ</ta>
            <ta e="T658" id="Seg_9744" s="T657">стоять-CVB.SEQ</ta>
            <ta e="T659" id="Seg_9745" s="T658">на.том.берегу-ADJZ.[NOM]</ta>
            <ta e="T660" id="Seg_9746" s="T659">к</ta>
            <ta e="T661" id="Seg_9747" s="T660">переходить-CVB.SIM</ta>
            <ta e="T662" id="Seg_9748" s="T661">спорить-RECP/COLL-CVB.SIM</ta>
            <ta e="T663" id="Seg_9749" s="T662">выйти-EP-PST2-3PL</ta>
            <ta e="T664" id="Seg_9750" s="T663">1PL.[NOM]</ta>
            <ta e="T665" id="Seg_9751" s="T664">солдат-PL-1PL.[NOM]</ta>
            <ta e="T666" id="Seg_9752" s="T665">тот</ta>
            <ta e="T667" id="Seg_9753" s="T666">спор.[NOM]</ta>
            <ta e="T668" id="Seg_9754" s="T667">шум-3SG-DAT/LOC</ta>
            <ta e="T669" id="Seg_9755" s="T668">мост-DAT/LOC</ta>
            <ta e="T670" id="Seg_9756" s="T669">подходить-NEG.CVB.SIM</ta>
            <ta e="T671" id="Seg_9757" s="T670">другой.из.двух</ta>
            <ta e="T672" id="Seg_9758" s="T671">человек.[NOM]</ta>
            <ta e="T673" id="Seg_9759" s="T672">лодка-INSTR</ta>
            <ta e="T674" id="Seg_9760" s="T673">некоторый-PL.[NOM]</ta>
            <ta e="T675" id="Seg_9761" s="T674">сколько-DISTR</ta>
            <ta e="T676" id="Seg_9762" s="T675">опять</ta>
            <ta e="T677" id="Seg_9763" s="T676">бревно-PL-ACC</ta>
            <ta e="T678" id="Seg_9764" s="T677">вместе</ta>
            <ta e="T679" id="Seg_9765" s="T678">связывать-CVB.SIM</ta>
            <ta e="T680" id="Seg_9766" s="T679">связывать-CVB.SEQ</ta>
            <ta e="T681" id="Seg_9767" s="T680">после</ta>
            <ta e="T682" id="Seg_9768" s="T681">другой.из.двух-PL.[NOM]</ta>
            <ta e="T683" id="Seg_9769" s="T682">свободно</ta>
            <ta e="T684" id="Seg_9770" s="T683">плавать-CVB.SEQ</ta>
            <ta e="T685" id="Seg_9771" s="T684">выйти-EP-PST2-3PL</ta>
            <ta e="T686" id="Seg_9772" s="T685">Днепр.[NOM]</ta>
            <ta e="T687" id="Seg_9773" s="T686">говорить-CVB.SEQ</ta>
            <ta e="T688" id="Seg_9774" s="T687">река-ACC</ta>
            <ta e="T689" id="Seg_9775" s="T688">там</ta>
            <ta e="T690" id="Seg_9776" s="T689">шум.[NOM]</ta>
            <ta e="T691" id="Seg_9777" s="T690">большой</ta>
            <ta e="T692" id="Seg_9778" s="T691">крик.[NOM]</ta>
            <ta e="T693" id="Seg_9779" s="T692">вопль.[NOM]</ta>
            <ta e="T694" id="Seg_9780" s="T693">борьба.[NOM]</ta>
            <ta e="T695" id="Seg_9781" s="T694">что-ABL</ta>
            <ta e="T696" id="Seg_9782" s="T695">NEG</ta>
            <ta e="T697" id="Seg_9783" s="T696">другой</ta>
            <ta e="T698" id="Seg_9784" s="T697">война.[NOM]</ta>
            <ta e="T699" id="Seg_9785" s="T698">оружие-3SG-GEN</ta>
            <ta e="T700" id="Seg_9786" s="T699">шум-3SG.[NOM]</ta>
            <ta e="T701" id="Seg_9787" s="T700">ужас-PROPR.[NOM]</ta>
            <ta e="T702" id="Seg_9788" s="T701">быть-PST1-3SG</ta>
            <ta e="T703" id="Seg_9789" s="T702">немец.[NOM]</ta>
            <ta e="T704" id="Seg_9790" s="T703">в.сторону</ta>
            <ta e="T705" id="Seg_9791" s="T704">сторона.[NOM]</ta>
            <ta e="T706" id="Seg_9792" s="T705">тот</ta>
            <ta e="T707" id="Seg_9793" s="T706">уничтожать-CVB.SIM</ta>
            <ta e="T708" id="Seg_9794" s="T707">спорить-NMNZ-ACC</ta>
            <ta e="T709" id="Seg_9795" s="T708">мочь-CVB.SEQ</ta>
            <ta e="T710" id="Seg_9796" s="T709">держать-NEG.CVB.SIM</ta>
            <ta e="T711" id="Seg_9797" s="T710">вхолостую</ta>
            <ta e="T712" id="Seg_9798" s="T711">река.[NOM]</ta>
            <ta e="T713" id="Seg_9799" s="T712">верхняя.часть-3SG-INSTR</ta>
            <ta e="T714" id="Seg_9800" s="T713">стрелять-FREQ-PTCP.PRS</ta>
            <ta e="T715" id="Seg_9801" s="T714">быть-PST1-3SG</ta>
            <ta e="T716" id="Seg_9802" s="T715">берег-ABL</ta>
            <ta e="T717" id="Seg_9803" s="T716">видеть-PTCP.COND-DAT/LOC</ta>
            <ta e="T718" id="Seg_9804" s="T717">река.[NOM]</ta>
            <ta e="T719" id="Seg_9805" s="T718">к</ta>
            <ta e="T720" id="Seg_9806" s="T719">вода-DAT/LOC</ta>
            <ta e="T721" id="Seg_9807" s="T720">совсем</ta>
            <ta e="T722" id="Seg_9808" s="T721">человек.[NOM]</ta>
            <ta e="T723" id="Seg_9809" s="T722">что-ACC</ta>
            <ta e="T724" id="Seg_9810" s="T723">NEG</ta>
            <ta e="T725" id="Seg_9811" s="T724">понимать-PTCP.FUT</ta>
            <ta e="T726" id="Seg_9812" s="T725">быть-NEG.PTCP.PST</ta>
            <ta e="T727" id="Seg_9813" s="T726">быть-PST1-3SG</ta>
            <ta e="T728" id="Seg_9814" s="T727">пример-3SG.[NOM]</ta>
            <ta e="T729" id="Seg_9815" s="T728">бояться-EP-PTCP.PST</ta>
            <ta e="T730" id="Seg_9816" s="T729">большой</ta>
            <ta e="T731" id="Seg_9817" s="T730">дикий.олень.[NOM]</ta>
            <ta e="T732" id="Seg_9818" s="T731">стадо-3SG.[NOM]</ta>
            <ta e="T733" id="Seg_9819" s="T732">плавать-PTCP.PRS-3SG-ACC</ta>
            <ta e="T734" id="Seg_9820" s="T733">подобно</ta>
            <ta e="T735" id="Seg_9821" s="T734">быть-PST1-3SG</ta>
            <ta e="T736" id="Seg_9822" s="T735">тот</ta>
            <ta e="T737" id="Seg_9823" s="T736">река.[NOM]</ta>
            <ta e="T738" id="Seg_9824" s="T737">идти.по.воде-NMNZ-3SG-DAT/LOC</ta>
            <ta e="T739" id="Seg_9825" s="T738">столько</ta>
            <ta e="T740" id="Seg_9826" s="T739">человек.[NOM]</ta>
            <ta e="T741" id="Seg_9827" s="T740">задыхаться-CVB.SEQ</ta>
            <ta e="T742" id="Seg_9828" s="T741">война.[NOM]</ta>
            <ta e="T743" id="Seg_9829" s="T742">ружье-3SG-ABL</ta>
            <ta e="T744" id="Seg_9830" s="T743">умирать-PTCP.PST-3SG-ACC</ta>
            <ta e="T745" id="Seg_9831" s="T744">кто.[NOM]</ta>
            <ta e="T746" id="Seg_9832" s="T745">EMPH</ta>
            <ta e="T747" id="Seg_9833" s="T746">знать-NEG.PTCP</ta>
            <ta e="T748" id="Seg_9834" s="T747">тогда</ta>
            <ta e="T749" id="Seg_9835" s="T748">быть-PST1-3SG</ta>
            <ta e="T750" id="Seg_9836" s="T749">река-ACC</ta>
            <ta e="T751" id="Seg_9837" s="T750">выйти-CVB.SEQ</ta>
            <ta e="T752" id="Seg_9838" s="T751">после</ta>
            <ta e="T753" id="Seg_9839" s="T752">наши.[NOM]</ta>
            <ta e="T754" id="Seg_9840" s="T753">опять</ta>
            <ta e="T755" id="Seg_9841" s="T754">немец-PL-ACC</ta>
            <ta e="T756" id="Seg_9842" s="T755">досуг.[NOM]</ta>
            <ta e="T757" id="Seg_9843" s="T756">найти-CAUS-NEG.CVB.SIM</ta>
            <ta e="T758" id="Seg_9844" s="T757">вдоволь</ta>
            <ta e="T759" id="Seg_9845" s="T758">гнать-PST2-3PL</ta>
            <ta e="T760" id="Seg_9846" s="T759">потом</ta>
            <ta e="T761" id="Seg_9847" s="T760">1SG.[NOM]</ta>
            <ta e="T762" id="Seg_9848" s="T761">сколько</ta>
            <ta e="T763" id="Seg_9849" s="T762">INDEF</ta>
            <ta e="T764" id="Seg_9850" s="T763">война-DAT/LOC</ta>
            <ta e="T765" id="Seg_9851" s="T764">идти-CVB.SEQ-1SG</ta>
            <ta e="T766" id="Seg_9852" s="T765">четыре-ORD-PROPR</ta>
            <ta e="T767" id="Seg_9853" s="T766">день-3SG-DAT/LOC</ta>
            <ta e="T768" id="Seg_9854" s="T767">январь.[NOM]</ta>
            <ta e="T769" id="Seg_9855" s="T768">месяц-DAT/LOC</ta>
            <ta e="T770" id="Seg_9856" s="T769">тысяча</ta>
            <ta e="T771" id="Seg_9857" s="T770">девять</ta>
            <ta e="T772" id="Seg_9858" s="T771">сто</ta>
            <ta e="T773" id="Seg_9859" s="T772">четыре-десять</ta>
            <ta e="T774" id="Seg_9860" s="T773">три</ta>
            <ta e="T775" id="Seg_9861" s="T774">год-PROPR-DAT/LOC</ta>
            <ta e="T776" id="Seg_9862" s="T775">последний-1SG-ACC</ta>
            <ta e="T777" id="Seg_9863" s="T776">рана-VBZ-EP-CAUS-PST2-EP-1SG</ta>
            <ta e="T778" id="Seg_9864" s="T777">очень</ta>
            <ta e="T779" id="Seg_9865" s="T778">тяжелый-ADVZ</ta>
            <ta e="T780" id="Seg_9866" s="T779">левый</ta>
            <ta e="T781" id="Seg_9867" s="T780">в.сторону</ta>
            <ta e="T782" id="Seg_9868" s="T781">задняя.нога-EP-1SG.[NOM]</ta>
            <ta e="T783" id="Seg_9869" s="T782">кость-3SG-ACC</ta>
            <ta e="T784" id="Seg_9870" s="T783">вдоволь</ta>
            <ta e="T785" id="Seg_9871" s="T784">ломать-CAUS-PST2-EP-1SG</ta>
            <ta e="T786" id="Seg_9872" s="T785">тот-ACC</ta>
            <ta e="T787" id="Seg_9873" s="T786">лечить-EP-CAUS-CVB.SEQ-1SG</ta>
            <ta e="T788" id="Seg_9874" s="T787">поправляться-PTCP.FUT-1SG-DAT/LOC</ta>
            <ta e="T789" id="Seg_9875" s="T788">пока</ta>
            <ta e="T790" id="Seg_9876" s="T789">шесть</ta>
            <ta e="T791" id="Seg_9877" s="T790">месяц.[NOM]</ta>
            <ta e="T792" id="Seg_9878" s="T791">так.долго</ta>
            <ta e="T793" id="Seg_9879" s="T792">лежать-PST2-EP-1SG</ta>
            <ta e="T794" id="Seg_9880" s="T793">Ессентуки.[NOM]</ta>
            <ta e="T795" id="Seg_9881" s="T794">говорить-CVB.SEQ</ta>
            <ta e="T796" id="Seg_9882" s="T795">город-DAT/LOC</ta>
            <ta e="T797" id="Seg_9883" s="T796">Кавказ.[NOM]</ta>
            <ta e="T798" id="Seg_9884" s="T797">говорить-CVB.SEQ</ta>
            <ta e="T799" id="Seg_9885" s="T798">теплый</ta>
            <ta e="T800" id="Seg_9886" s="T799">место-DAT/LOC</ta>
            <ta e="T801" id="Seg_9887" s="T800">потом</ta>
            <ta e="T802" id="Seg_9888" s="T801">лечить-EP-CAUS-CVB.SEQ</ta>
            <ta e="T803" id="Seg_9889" s="T802">после</ta>
            <ta e="T804" id="Seg_9890" s="T803">дерево.[NOM]</ta>
            <ta e="T805" id="Seg_9891" s="T804">палка-PROPR.[NOM]</ta>
            <ta e="T806" id="Seg_9892" s="T805">выйти-CVB.SEQ</ta>
            <ta e="T807" id="Seg_9893" s="T806">идти-CVB.SEQ-1SG</ta>
            <ta e="T808" id="Seg_9894" s="T807">война-ABL</ta>
            <ta e="T809" id="Seg_9895" s="T808">совсем</ta>
            <ta e="T810" id="Seg_9896" s="T809">освободить-EP-MED-PST2-EP-1SG</ta>
            <ta e="T811" id="Seg_9897" s="T810">армия-ABL</ta>
            <ta e="T813" id="Seg_9898" s="T812">выйти-EP-PST2-EP-1SG</ta>
            <ta e="T814" id="Seg_9899" s="T813">сначала</ta>
            <ta e="T815" id="Seg_9900" s="T814">воевать-PTCP.PST</ta>
            <ta e="T816" id="Seg_9901" s="T815">день-1SG-ABL</ta>
            <ta e="T817" id="Seg_9902" s="T816">один</ta>
            <ta e="T818" id="Seg_9903" s="T817">год.[NOM]</ta>
            <ta e="T819" id="Seg_9904" s="T818">половина-3SG-ACC</ta>
            <ta e="T820" id="Seg_9905" s="T819">так.долго</ta>
            <ta e="T821" id="Seg_9906" s="T820">война.[NOM]</ta>
            <ta e="T822" id="Seg_9907" s="T821">борьба-3SG-DAT/LOC</ta>
            <ta e="T823" id="Seg_9908" s="T822">идти-CVB.SEQ-1SG</ta>
            <ta e="T824" id="Seg_9909" s="T823">полностью</ta>
            <ta e="T825" id="Seg_9910" s="T824">четыре-DAT/LOC</ta>
            <ta e="T826" id="Seg_9911" s="T825">раз.[NOM]</ta>
            <ta e="T827" id="Seg_9912" s="T826">рана-VBZ-EP-CAUS-PST2-EP-1SG</ta>
            <ta e="T828" id="Seg_9913" s="T827">тот.[NOM]</ta>
            <ta e="T829" id="Seg_9914" s="T828">сторона-3SG-INSTR</ta>
            <ta e="T830" id="Seg_9915" s="T829">три-MLTP</ta>
            <ta e="T831" id="Seg_9916" s="T830">лечить-EP-CAUS-CVB.SIM</ta>
            <ta e="T832" id="Seg_9917" s="T831">идти-EP-PST2-EP-1SG</ta>
            <ta e="T833" id="Seg_9918" s="T832">короткий-ADVZ</ta>
            <ta e="T834" id="Seg_9919" s="T833">рассказывать-PTCP.COND-DAT/LOC</ta>
            <ta e="T835" id="Seg_9920" s="T834">такой</ta>
            <ta e="T836" id="Seg_9921" s="T835">1SG.[NOM]</ta>
            <ta e="T837" id="Seg_9922" s="T836">человек.[NOM]</ta>
            <ta e="T838" id="Seg_9923" s="T837">родственник.[NOM]</ta>
            <ta e="T839" id="Seg_9924" s="T838">жизнь-3SG-DAT/LOC</ta>
            <ta e="T840" id="Seg_9925" s="T839">когда</ta>
            <ta e="T841" id="Seg_9926" s="T840">NEG</ta>
            <ta e="T842" id="Seg_9927" s="T841">знать-PASS/REFL-EP-NEG.PTCP.PST</ta>
            <ta e="T843" id="Seg_9928" s="T842">отец.[NOM]</ta>
            <ta e="T844" id="Seg_9929" s="T843">да</ta>
            <ta e="T845" id="Seg_9930" s="T844">тот</ta>
            <ta e="T846" id="Seg_9931" s="T845">большой</ta>
            <ta e="T847" id="Seg_9932" s="T846">война-3SG-DAT/LOC</ta>
            <ta e="T848" id="Seg_9933" s="T847">сам-1SG.[NOM]</ta>
            <ta e="T849" id="Seg_9934" s="T848">люди-1SG-ACC</ta>
            <ta e="T850" id="Seg_9935" s="T849">родиться-PTCP.PST</ta>
            <ta e="T851" id="Seg_9936" s="T850">создаваться-PTCP.PST</ta>
            <ta e="T852" id="Seg_9937" s="T851">земля-1SG-ACC</ta>
            <ta e="T853" id="Seg_9938" s="T852">защищать-CVB.SIM</ta>
            <ta e="T854" id="Seg_9939" s="T853">идти-EP-PST2-EP-1SG</ta>
            <ta e="T855" id="Seg_9940" s="T854">мой</ta>
            <ta e="T856" id="Seg_9941" s="T855">кровь-EP-1SG.[NOM]</ta>
            <ta e="T857" id="Seg_9942" s="T856">пот-EP-1SG.[NOM]</ta>
            <ta e="T858" id="Seg_9943" s="T857">тот</ta>
            <ta e="T859" id="Seg_9944" s="T858">война-DAT/LOC</ta>
            <ta e="T860" id="Seg_9945" s="T859">зря</ta>
            <ta e="T861" id="Seg_9946" s="T860">выливать-REFL-EP-PST2.NEG-3SG</ta>
            <ta e="T862" id="Seg_9947" s="T861">наш</ta>
            <ta e="T863" id="Seg_9948" s="T862">коммунистический</ta>
            <ta e="T864" id="Seg_9949" s="T863">партия-1PL.[NOM]</ta>
            <ta e="T865" id="Seg_9950" s="T864">центр.[NOM]</ta>
            <ta e="T866" id="Seg_9951" s="T865">комитет-3SG.[NOM]</ta>
            <ta e="T867" id="Seg_9952" s="T866">потом</ta>
            <ta e="T868" id="Seg_9953" s="T867">наш</ta>
            <ta e="T869" id="Seg_9954" s="T868">государство-1PL-GEN</ta>
            <ta e="T870" id="Seg_9955" s="T869">жизнь-3SG-ACC</ta>
            <ta e="T871" id="Seg_9956" s="T870">исправить-PTCP.HAB</ta>
            <ta e="T872" id="Seg_9957" s="T871">правительство-1PL.[NOM]</ta>
            <ta e="T873" id="Seg_9958" s="T872">давать-PST2-3PL</ta>
            <ta e="T874" id="Seg_9959" s="T873">1SG-DAT/LOC</ta>
            <ta e="T875" id="Seg_9960" s="T874">государство.[NOM]</ta>
            <ta e="T876" id="Seg_9961" s="T875">высокий</ta>
            <ta e="T877" id="Seg_9962" s="T876">война-3SG-GEN</ta>
            <ta e="T878" id="Seg_9963" s="T877">орден-3SG-ACC</ta>
            <ta e="T882" id="Seg_9964" s="T881">говорить-CVB.SEQ</ta>
            <ta e="T883" id="Seg_9965" s="T882">потом</ta>
            <ta e="T884" id="Seg_9966" s="T883">еще</ta>
            <ta e="T885" id="Seg_9967" s="T884">медаль-ACC</ta>
            <ta e="T890" id="Seg_9968" s="T889">говорить-CVB.SEQ</ta>
            <ta e="T891" id="Seg_9969" s="T890">теперь</ta>
            <ta e="T892" id="Seg_9970" s="T891">1PL.[NOM]</ta>
            <ta e="T893" id="Seg_9971" s="T892">советский</ta>
            <ta e="T894" id="Seg_9972" s="T893">жизнь-3SG-DAT/LOC</ta>
            <ta e="T895" id="Seg_9973" s="T894">жить-PTCP.PRS</ta>
            <ta e="T896" id="Seg_9974" s="T895">человек-PL.[NOM]</ta>
            <ta e="T897" id="Seg_9975" s="T896">жизнь-2PL.[NOM]</ta>
            <ta e="T898" id="Seg_9976" s="T897">очень</ta>
            <ta e="T899" id="Seg_9977" s="T898">получаться-CVB.SEQ</ta>
            <ta e="T900" id="Seg_9978" s="T899">стоять-PRS.[3SG]</ta>
            <ta e="T901" id="Seg_9979" s="T900">передняя.часть.[NOM]</ta>
            <ta e="T902" id="Seg_9980" s="T901">к</ta>
            <ta e="T903" id="Seg_9981" s="T902">идти-PTCP.PRS</ta>
            <ta e="T904" id="Seg_9982" s="T903">сила-DAT/LOC</ta>
            <ta e="T905" id="Seg_9983" s="T904">тот.[NOM]</ta>
            <ta e="T906" id="Seg_9984" s="T905">из_за</ta>
            <ta e="T907" id="Seg_9985" s="T906">кто.[NOM]</ta>
            <ta e="T908" id="Seg_9986" s="T907">EMPH</ta>
            <ta e="T909" id="Seg_9987" s="T908">теперь</ta>
            <ta e="T910" id="Seg_9988" s="T909">война-2SG-ACC</ta>
            <ta e="T911" id="Seg_9989" s="T910">становиться-PTCP.FUT-3SG-ACC</ta>
            <ta e="T912" id="Seg_9990" s="T911">хотеть-NEG.[3SG]</ta>
            <ta e="T913" id="Seg_9991" s="T912">1PL.[NOM]</ta>
            <ta e="T914" id="Seg_9992" s="T913">земля-1PL.[NOM]</ta>
            <ta e="T915" id="Seg_9993" s="T914">человек-3SG.[NOM]</ta>
            <ta e="T916" id="Seg_9994" s="T915">прежний-ABL</ta>
            <ta e="T917" id="Seg_9995" s="T916">EMPH</ta>
            <ta e="T918" id="Seg_9996" s="T917">война-DAT/LOC</ta>
            <ta e="T919" id="Seg_9997" s="T918">хотеть-PTCP.HAB-3SG</ta>
            <ta e="T920" id="Seg_9998" s="T919">NEG</ta>
            <ta e="T921" id="Seg_9999" s="T920">быть-PST1-3SG</ta>
            <ta e="T922" id="Seg_10000" s="T921">1PL.[NOM]</ta>
            <ta e="T923" id="Seg_10001" s="T922">народ-1PL.[NOM]</ta>
            <ta e="T924" id="Seg_10002" s="T923">теперь</ta>
            <ta e="T925" id="Seg_10003" s="T924">сам-3SG-GEN</ta>
            <ta e="T926" id="Seg_10004" s="T925">желание-3SG.[NOM]</ta>
            <ta e="T927" id="Seg_10005" s="T926">согласить-PTCP.PRS-3SG-ACC</ta>
            <ta e="T928" id="Seg_10006" s="T927">яркий</ta>
            <ta e="T929" id="Seg_10007" s="T928">счастливый</ta>
            <ta e="T930" id="Seg_10008" s="T929">Ленин.[NOM]</ta>
            <ta e="T931" id="Seg_10009" s="T930">показывать-PTCP.PST</ta>
            <ta e="T932" id="Seg_10010" s="T931">коммунизм</ta>
            <ta e="T933" id="Seg_10011" s="T932">имя-PROPR</ta>
            <ta e="T934" id="Seg_10012" s="T933">жизнь-3SG-ACC</ta>
            <ta e="T935" id="Seg_10013" s="T934">делать-PRS.[3SG]</ta>
            <ta e="T936" id="Seg_10014" s="T935">1PL.[NOM]</ta>
            <ta e="T937" id="Seg_10015" s="T936">каждый-1PL.[NOM]</ta>
            <ta e="T938" id="Seg_10016" s="T937">говорить-PRS-1PL</ta>
            <ta e="T939" id="Seg_10017" s="T938">теперь</ta>
            <ta e="T940" id="Seg_10018" s="T939">война.[NOM]</ta>
            <ta e="T941" id="Seg_10019" s="T940">NEG.EX</ta>
            <ta e="T942" id="Seg_10020" s="T941">быть-IMP.3SG</ta>
            <ta e="T943" id="Seg_10021" s="T942">земля.[NOM]</ta>
            <ta e="T944" id="Seg_10022" s="T943">верхняя.часть-3SG-INSTR</ta>
            <ta e="T945" id="Seg_10023" s="T944">есть</ta>
            <ta e="T946" id="Seg_10024" s="T945">быть-IMP.3SG</ta>
            <ta e="T947" id="Seg_10025" s="T946">яркий</ta>
            <ta e="T948" id="Seg_10026" s="T947">солнце-PROPR</ta>
            <ta e="T949" id="Seg_10027" s="T948">жизнь.[NOM]</ta>
            <ta e="T950" id="Seg_10028" s="T949">каждый</ta>
            <ta e="T951" id="Seg_10029" s="T950">народ-DAT/LOC</ta>
         </annotation>
         <annotation name="mc" tierref="mc">
            <ta e="T1" id="Seg_10030" s="T0">pers-pro:case</ta>
            <ta e="T2" id="Seg_10031" s="T1">n-n:case</ta>
            <ta e="T3" id="Seg_10032" s="T2">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T4" id="Seg_10033" s="T3">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T5" id="Seg_10034" s="T4">n-n:poss-n:case</ta>
            <ta e="T6" id="Seg_10035" s="T5">cardnum</ta>
            <ta e="T7" id="Seg_10036" s="T6">cardnum</ta>
            <ta e="T8" id="Seg_10037" s="T7">cardnum</ta>
            <ta e="T9" id="Seg_10038" s="T8">cardnum-cardnum</ta>
            <ta e="T10" id="Seg_10039" s="T9">cardnum</ta>
            <ta e="T11" id="Seg_10040" s="T10">n-n&gt;adj-n:case</ta>
            <ta e="T12" id="Seg_10041" s="T11">adj</ta>
            <ta e="T13" id="Seg_10042" s="T12">n-n:case</ta>
            <ta e="T14" id="Seg_10043" s="T13">n-n:case</ta>
            <ta e="T15" id="Seg_10044" s="T14">v-v:ptcp</ta>
            <ta e="T16" id="Seg_10045" s="T15">n-n:poss-n:case</ta>
            <ta e="T17" id="Seg_10046" s="T16">adv</ta>
            <ta e="T18" id="Seg_10047" s="T17">v-v:cvb-v:pred.pn</ta>
            <ta e="T19" id="Seg_10048" s="T18">cardnum</ta>
            <ta e="T20" id="Seg_10049" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_10050" s="T20">post</ta>
            <ta e="T22" id="Seg_10051" s="T21">cardnum</ta>
            <ta e="T23" id="Seg_10052" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_10053" s="T23">n-n:case</ta>
            <ta e="T25" id="Seg_10054" s="T24">n-n:poss-n:case</ta>
            <ta e="T26" id="Seg_10055" s="T25">v-v:cvb</ta>
            <ta e="T27" id="Seg_10056" s="T26">v-v:(ins)-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T29" id="Seg_10057" s="T27">propr-n:case</ta>
            <ta e="T30" id="Seg_10058" s="T29">v-v:cvb</ta>
            <ta e="T31" id="Seg_10059" s="T30">n-n:case</ta>
            <ta e="T32" id="Seg_10060" s="T31">adv</ta>
            <ta e="T33" id="Seg_10061" s="T32">adv</ta>
            <ta e="T34" id="Seg_10062" s="T33">cardnum</ta>
            <ta e="T35" id="Seg_10063" s="T34">cardnum</ta>
            <ta e="T36" id="Seg_10064" s="T35">cardnum</ta>
            <ta e="T37" id="Seg_10065" s="T36">cardnum-cardnum</ta>
            <ta e="T38" id="Seg_10066" s="T37">cardnum</ta>
            <ta e="T39" id="Seg_10067" s="T38">n-n&gt;adj-n:case</ta>
            <ta e="T40" id="Seg_10068" s="T39">pers-pro:case</ta>
            <ta e="T41" id="Seg_10069" s="T40">v-v:tense-v:poss.pn</ta>
            <ta e="T42" id="Seg_10070" s="T41">n-n:case</ta>
            <ta e="T43" id="Seg_10071" s="T42">post</ta>
            <ta e="T44" id="Seg_10072" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_10073" s="T44">n-n:(poss)-n:case</ta>
            <ta e="T46" id="Seg_10074" s="T45">v-v:cvb</ta>
            <ta e="T47" id="Seg_10075" s="T46">v-v:ptcp</ta>
            <ta e="T48" id="Seg_10076" s="T47">n-n:poss-n:case</ta>
            <ta e="T49" id="Seg_10077" s="T48">pers-pro:case</ta>
            <ta e="T50" id="Seg_10078" s="T49">n-n:case</ta>
            <ta e="T51" id="Seg_10079" s="T50">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T52" id="Seg_10080" s="T51">propr-n:case</ta>
            <ta e="T53" id="Seg_10081" s="T52">v-v:cvb</ta>
            <ta e="T54" id="Seg_10082" s="T53">n-n&gt;adj</ta>
            <ta e="T55" id="Seg_10083" s="T54">n-n:case</ta>
            <ta e="T56" id="Seg_10084" s="T55">propr-n:case</ta>
            <ta e="T57" id="Seg_10085" s="T56">n-n:poss-n:case</ta>
            <ta e="T58" id="Seg_10086" s="T57">adv</ta>
            <ta e="T59" id="Seg_10087" s="T58">adj</ta>
            <ta e="T60" id="Seg_10088" s="T59">n-n:poss-n:case</ta>
            <ta e="T61" id="Seg_10089" s="T60">n-n:(poss)-n:case</ta>
            <ta e="T62" id="Seg_10090" s="T61">adv</ta>
            <ta e="T63" id="Seg_10091" s="T62">quant</ta>
            <ta e="T64" id="Seg_10092" s="T63">n-n:case</ta>
            <ta e="T65" id="Seg_10093" s="T64">n-n:poss-n:case</ta>
            <ta e="T66" id="Seg_10094" s="T65">adj</ta>
            <ta e="T67" id="Seg_10095" s="T66">adj</ta>
            <ta e="T68" id="Seg_10096" s="T67">n-n:poss-n:case</ta>
            <ta e="T69" id="Seg_10097" s="T68">v-v:cvb</ta>
            <ta e="T70" id="Seg_10098" s="T69">post</ta>
            <ta e="T71" id="Seg_10099" s="T70">pers-pro:case</ta>
            <ta e="T72" id="Seg_10100" s="T71">n-n:poss-n:case</ta>
            <ta e="T73" id="Seg_10101" s="T72">adv</ta>
            <ta e="T74" id="Seg_10102" s="T73">adv</ta>
            <ta e="T75" id="Seg_10103" s="T74">post</ta>
            <ta e="T76" id="Seg_10104" s="T75">cardnum</ta>
            <ta e="T77" id="Seg_10105" s="T76">cardnum</ta>
            <ta e="T78" id="Seg_10106" s="T77">n-n&gt;adj</ta>
            <ta e="T79" id="Seg_10107" s="T78">n-n:case</ta>
            <ta e="T80" id="Seg_10108" s="T79">v-v:ptcp</ta>
            <ta e="T81" id="Seg_10109" s="T80">v-v:tense-v:poss.pn</ta>
            <ta e="T82" id="Seg_10110" s="T81">adv</ta>
            <ta e="T83" id="Seg_10111" s="T82">adv</ta>
            <ta e="T84" id="Seg_10112" s="T83">dempro</ta>
            <ta e="T85" id="Seg_10113" s="T84">n-n:case</ta>
            <ta e="T86" id="Seg_10114" s="T85">propr-n:case</ta>
            <ta e="T87" id="Seg_10115" s="T86">n-n:case</ta>
            <ta e="T88" id="Seg_10116" s="T87">v-v:cvb</ta>
            <ta e="T89" id="Seg_10117" s="T88">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T90" id="Seg_10118" s="T89">adj-adj&gt;adv</ta>
            <ta e="T91" id="Seg_10119" s="T90">v-v:ptcp-v:(case)</ta>
            <ta e="T92" id="Seg_10120" s="T91">propr-n:case</ta>
            <ta e="T93" id="Seg_10121" s="T92">adj-n:(poss)</ta>
            <ta e="T94" id="Seg_10122" s="T93">v-v:ptcp-v:(case)</ta>
            <ta e="T95" id="Seg_10123" s="T94">adj-n:case</ta>
            <ta e="T96" id="Seg_10124" s="T95">v-v:tense-v:poss.pn</ta>
            <ta e="T97" id="Seg_10125" s="T96">adj</ta>
            <ta e="T98" id="Seg_10126" s="T97">n-n:poss-n:case</ta>
            <ta e="T99" id="Seg_10127" s="T98">n-n:poss-n:case</ta>
            <ta e="T100" id="Seg_10128" s="T99">n-n:(poss)-n:case</ta>
            <ta e="T101" id="Seg_10129" s="T100">ptcl</ta>
            <ta e="T102" id="Seg_10130" s="T101">v-v:tense-v:pred.pn</ta>
            <ta e="T103" id="Seg_10131" s="T102">dempro-pro:case</ta>
            <ta e="T104" id="Seg_10132" s="T103">n-n:poss-n:case</ta>
            <ta e="T105" id="Seg_10133" s="T104">postp</ta>
            <ta e="T106" id="Seg_10134" s="T105">v-v:cvb</ta>
            <ta e="T107" id="Seg_10135" s="T106">post</ta>
            <ta e="T108" id="Seg_10136" s="T107">adv</ta>
            <ta e="T109" id="Seg_10137" s="T108">adj-adj&gt;adv</ta>
            <ta e="T110" id="Seg_10138" s="T109">pers-pro:case</ta>
            <ta e="T111" id="Seg_10139" s="T110">n-n:case</ta>
            <ta e="T112" id="Seg_10140" s="T111">n-n:poss-n:case</ta>
            <ta e="T113" id="Seg_10141" s="T112">propr-n:case</ta>
            <ta e="T114" id="Seg_10142" s="T113">v-v:cvb</ta>
            <ta e="T115" id="Seg_10143" s="T114">n-n:case</ta>
            <ta e="T116" id="Seg_10144" s="T115">v-v:ptcp-v:(case)</ta>
            <ta e="T117" id="Seg_10145" s="T116">post</ta>
            <ta e="T118" id="Seg_10146" s="T117">posspr</ta>
            <ta e="T119" id="Seg_10147" s="T118">n-n:(poss)-n:case</ta>
            <ta e="T120" id="Seg_10148" s="T119">propr-n:case</ta>
            <ta e="T121" id="Seg_10149" s="T120">adj-n:poss-n:case</ta>
            <ta e="T122" id="Seg_10150" s="T121">que-pro:case</ta>
            <ta e="T123" id="Seg_10151" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_10152" s="T123">adj</ta>
            <ta e="T125" id="Seg_10153" s="T124">n-n:case</ta>
            <ta e="T126" id="Seg_10154" s="T125">n-n:poss-n:case</ta>
            <ta e="T127" id="Seg_10155" s="T126">v-v:cvb</ta>
            <ta e="T128" id="Seg_10156" s="T127">post</ta>
            <ta e="T129" id="Seg_10157" s="T128">cardnum</ta>
            <ta e="T130" id="Seg_10158" s="T129">n-n:case</ta>
            <ta e="T131" id="Seg_10159" s="T130">post</ta>
            <ta e="T132" id="Seg_10160" s="T131">adj</ta>
            <ta e="T133" id="Seg_10161" s="T132">n-n:poss-n:case</ta>
            <ta e="T134" id="Seg_10162" s="T133">v-v:cvb</ta>
            <ta e="T135" id="Seg_10163" s="T134">v-v:tense-v:poss.pn</ta>
            <ta e="T136" id="Seg_10164" s="T135">dempro-pro:case</ta>
            <ta e="T137" id="Seg_10165" s="T136">post</ta>
            <ta e="T138" id="Seg_10166" s="T137">pers-pro:case</ta>
            <ta e="T139" id="Seg_10167" s="T138">v-v:tense-v:pred.pn</ta>
            <ta e="T140" id="Seg_10168" s="T139">n-n:case</ta>
            <ta e="T141" id="Seg_10169" s="T140">n-n:case</ta>
            <ta e="T142" id="Seg_10170" s="T141">v-v:cvb</ta>
            <ta e="T143" id="Seg_10171" s="T142">v-v:cvb</ta>
            <ta e="T144" id="Seg_10172" s="T143">ptcl</ta>
            <ta e="T145" id="Seg_10173" s="T144">v-v:cvb</ta>
            <ta e="T146" id="Seg_10174" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_10175" s="T146">n-n:(poss)-n:case</ta>
            <ta e="T148" id="Seg_10176" s="T147">que-pro:case</ta>
            <ta e="T149" id="Seg_10177" s="T148">ptcl</ta>
            <ta e="T150" id="Seg_10178" s="T149">v-v:ptcp-v:(case)</ta>
            <ta e="T151" id="Seg_10179" s="T150">v-v:(ins)-v:ptcp-v:(case)</ta>
            <ta e="T152" id="Seg_10180" s="T151">post</ta>
            <ta e="T153" id="Seg_10181" s="T152">cardnum</ta>
            <ta e="T154" id="Seg_10182" s="T153">n</ta>
            <ta e="T155" id="Seg_10183" s="T154">v-v:ptcp</ta>
            <ta e="T156" id="Seg_10184" s="T155">n-n:case</ta>
            <ta e="T157" id="Seg_10185" s="T156">quant</ta>
            <ta e="T158" id="Seg_10186" s="T157">cardnum</ta>
            <ta e="T159" id="Seg_10187" s="T158">n-n:case</ta>
            <ta e="T160" id="Seg_10188" s="T159">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T161" id="Seg_10189" s="T160">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T162" id="Seg_10190" s="T161">quant</ta>
            <ta e="T163" id="Seg_10191" s="T162">n-n:case</ta>
            <ta e="T164" id="Seg_10192" s="T163">n-n:case</ta>
            <ta e="T165" id="Seg_10193" s="T164">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T166" id="Seg_10194" s="T165">quant</ta>
            <ta e="T167" id="Seg_10195" s="T166">n-n:case</ta>
            <ta e="T168" id="Seg_10196" s="T167">n-n:(poss)-n:case</ta>
            <ta e="T169" id="Seg_10197" s="T168">n-n:(num)-n:case</ta>
            <ta e="T170" id="Seg_10198" s="T169">n-n:(num)-n:case</ta>
            <ta e="T171" id="Seg_10199" s="T170">n-n:(num)-n:case</ta>
            <ta e="T172" id="Seg_10200" s="T171">adv</ta>
            <ta e="T173" id="Seg_10201" s="T172">adj</ta>
            <ta e="T174" id="Seg_10202" s="T173">adj</ta>
            <ta e="T175" id="Seg_10203" s="T174">n-n:(num)-n:case</ta>
            <ta e="T176" id="Seg_10204" s="T175">n-n:case</ta>
            <ta e="T177" id="Seg_10205" s="T176">n-n:(poss)-n:case</ta>
            <ta e="T178" id="Seg_10206" s="T177">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T179" id="Seg_10207" s="T178">que-pro:case</ta>
            <ta e="T180" id="Seg_10208" s="T179">ptcl</ta>
            <ta e="T181" id="Seg_10209" s="T180">adv</ta>
            <ta e="T182" id="Seg_10210" s="T181">v-v:ptcp</ta>
            <ta e="T183" id="Seg_10211" s="T182">v-v:tense-v:poss.pn</ta>
            <ta e="T184" id="Seg_10212" s="T183">adv</ta>
            <ta e="T185" id="Seg_10213" s="T184">que</ta>
            <ta e="T186" id="Seg_10214" s="T185">adj</ta>
            <ta e="T187" id="Seg_10215" s="T186">adj</ta>
            <ta e="T188" id="Seg_10216" s="T187">n-n:case</ta>
            <ta e="T189" id="Seg_10217" s="T188">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T190" id="Seg_10218" s="T189">dempro-pro:case</ta>
            <ta e="T191" id="Seg_10219" s="T190">n-n:case</ta>
            <ta e="T192" id="Seg_10220" s="T191">n-n:case</ta>
            <ta e="T193" id="Seg_10221" s="T192">v-v:tense-v:poss.pn</ta>
            <ta e="T194" id="Seg_10222" s="T193">cardnum</ta>
            <ta e="T195" id="Seg_10223" s="T194">dempro</ta>
            <ta e="T196" id="Seg_10224" s="T195">n-n:case</ta>
            <ta e="T197" id="Seg_10225" s="T196">cardnum</ta>
            <ta e="T198" id="Seg_10226" s="T197">n-n&gt;adj</ta>
            <ta e="T199" id="Seg_10227" s="T198">n-n&gt;n-n:case</ta>
            <ta e="T200" id="Seg_10228" s="T199">pers-pro:case</ta>
            <ta e="T201" id="Seg_10229" s="T200">ptcl</ta>
            <ta e="T202" id="Seg_10230" s="T201">n-n:poss-n:case</ta>
            <ta e="T203" id="Seg_10231" s="T202">n-n:case</ta>
            <ta e="T204" id="Seg_10232" s="T203">adv</ta>
            <ta e="T205" id="Seg_10233" s="T204">v-v:cvb</ta>
            <ta e="T206" id="Seg_10234" s="T205">v-v:ptcp</ta>
            <ta e="T207" id="Seg_10235" s="T206">v-v:tense-v:poss.pn</ta>
            <ta e="T208" id="Seg_10236" s="T207">n-n:case</ta>
            <ta e="T209" id="Seg_10237" s="T208">post</ta>
            <ta e="T210" id="Seg_10238" s="T209">n-n:case</ta>
            <ta e="T211" id="Seg_10239" s="T210">cardnum</ta>
            <ta e="T212" id="Seg_10240" s="T211">cardnum</ta>
            <ta e="T213" id="Seg_10241" s="T212">post-n:(poss)-n:case</ta>
            <ta e="T214" id="Seg_10242" s="T213">n-n:(num)-n:case</ta>
            <ta e="T215" id="Seg_10243" s="T214">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T216" id="Seg_10244" s="T215">adj-n:(poss)-n:case</ta>
            <ta e="T217" id="Seg_10245" s="T216">pers-pro:case</ta>
            <ta e="T218" id="Seg_10246" s="T217">n-n:poss-n:case</ta>
            <ta e="T219" id="Seg_10247" s="T218">n-n:case</ta>
            <ta e="T220" id="Seg_10248" s="T219">post</ta>
            <ta e="T221" id="Seg_10249" s="T220">v-v:cvb</ta>
            <ta e="T222" id="Seg_10250" s="T221">interj</ta>
            <ta e="T223" id="Seg_10251" s="T222">cardnum</ta>
            <ta e="T224" id="Seg_10252" s="T223">n-n:(num)-n:case</ta>
            <ta e="T225" id="Seg_10253" s="T224">v-v:ptcp</ta>
            <ta e="T226" id="Seg_10254" s="T225">v-v:tense-v:poss.pn</ta>
            <ta e="T227" id="Seg_10255" s="T226">adv</ta>
            <ta e="T228" id="Seg_10256" s="T227">adv</ta>
            <ta e="T229" id="Seg_10257" s="T228">adv</ta>
            <ta e="T230" id="Seg_10258" s="T229">v-v&gt;v-v:ptcp</ta>
            <ta e="T231" id="Seg_10259" s="T230">v-v:tense-v:poss.pn</ta>
            <ta e="T232" id="Seg_10260" s="T231">que</ta>
            <ta e="T233" id="Seg_10261" s="T232">ptcl</ta>
            <ta e="T234" id="Seg_10262" s="T233">n-n:case</ta>
            <ta e="T235" id="Seg_10263" s="T234">post</ta>
            <ta e="T236" id="Seg_10264" s="T235">v-v&gt;n-n&gt;adj</ta>
            <ta e="T237" id="Seg_10265" s="T236">n-n:case</ta>
            <ta e="T238" id="Seg_10266" s="T237">n-n:poss-n:case</ta>
            <ta e="T239" id="Seg_10267" s="T238">n-n:(ins)-n:case</ta>
            <ta e="T240" id="Seg_10268" s="T239">adv-adv&gt;adj</ta>
            <ta e="T241" id="Seg_10269" s="T240">n-n:case</ta>
            <ta e="T242" id="Seg_10270" s="T241">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T243" id="Seg_10271" s="T242">post</ta>
            <ta e="T244" id="Seg_10272" s="T243">adv-adv&gt;adj</ta>
            <ta e="T245" id="Seg_10273" s="T244">n-n:case</ta>
            <ta e="T246" id="Seg_10274" s="T245">v-v&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T247" id="Seg_10275" s="T246">post</ta>
            <ta e="T248" id="Seg_10276" s="T247">n-n:case</ta>
            <ta e="T249" id="Seg_10277" s="T248">adv</ta>
            <ta e="T250" id="Seg_10278" s="T249">que-pro&gt;pro-pro:case</ta>
            <ta e="T251" id="Seg_10279" s="T250">ptcl</ta>
            <ta e="T252" id="Seg_10280" s="T251">v-v:(ins)-v:ptcp-v:(case)</ta>
            <ta e="T253" id="Seg_10281" s="T252">post</ta>
            <ta e="T254" id="Seg_10282" s="T253">adj</ta>
            <ta e="T255" id="Seg_10283" s="T254">n-n:case</ta>
            <ta e="T256" id="Seg_10284" s="T255">v-v:ptcp</ta>
            <ta e="T257" id="Seg_10285" s="T256">v-v:tense-v:poss.pn</ta>
            <ta e="T258" id="Seg_10286" s="T257">dempro</ta>
            <ta e="T259" id="Seg_10287" s="T258">n-n:case</ta>
            <ta e="T260" id="Seg_10288" s="T259">n-n:case</ta>
            <ta e="T261" id="Seg_10289" s="T260">n-n:case</ta>
            <ta e="T262" id="Seg_10290" s="T261">v-v:ptcp</ta>
            <ta e="T263" id="Seg_10291" s="T262">n-n:poss-n:case</ta>
            <ta e="T264" id="Seg_10292" s="T263">v-v:cvb</ta>
            <ta e="T265" id="Seg_10293" s="T264">v-v:ptcp</ta>
            <ta e="T266" id="Seg_10294" s="T265">v-v:tense-v:poss.pn</ta>
            <ta e="T267" id="Seg_10295" s="T266">indfpro-pro:case</ta>
            <ta e="T268" id="Seg_10296" s="T267">post</ta>
            <ta e="T269" id="Seg_10297" s="T268">n-n:case</ta>
            <ta e="T270" id="Seg_10298" s="T269">n-n:case</ta>
            <ta e="T271" id="Seg_10299" s="T270">n-n:poss-n:case</ta>
            <ta e="T272" id="Seg_10300" s="T271">n-n:poss-n:case</ta>
            <ta e="T273" id="Seg_10301" s="T272">que-pro&gt;pro-pro:case</ta>
            <ta e="T274" id="Seg_10302" s="T273">ptcl</ta>
            <ta e="T275" id="Seg_10303" s="T274">v-v:(ins)-v:ptcp-v:(case)</ta>
            <ta e="T276" id="Seg_10304" s="T275">post</ta>
            <ta e="T277" id="Seg_10305" s="T276">adj-n:case</ta>
            <ta e="T278" id="Seg_10306" s="T277">v-v:ptcp</ta>
            <ta e="T279" id="Seg_10307" s="T278">v-v:tense-v:poss.pn</ta>
            <ta e="T280" id="Seg_10308" s="T279">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T281" id="Seg_10309" s="T280">n-n:poss-n:case</ta>
            <ta e="T282" id="Seg_10310" s="T281">pers-pro:case</ta>
            <ta e="T283" id="Seg_10311" s="T282">post</ta>
            <ta e="T284" id="Seg_10312" s="T283">n-n:case</ta>
            <ta e="T285" id="Seg_10313" s="T284">n-n:case</ta>
            <ta e="T286" id="Seg_10314" s="T285">adv</ta>
            <ta e="T287" id="Seg_10315" s="T286">n-n&gt;adj-adj&gt;adv</ta>
            <ta e="T288" id="Seg_10316" s="T287">v-v:tense-v:poss.pn</ta>
            <ta e="T289" id="Seg_10317" s="T288">n-n:case</ta>
            <ta e="T290" id="Seg_10318" s="T289">n-n:case</ta>
            <ta e="T291" id="Seg_10319" s="T290">adv</ta>
            <ta e="T292" id="Seg_10320" s="T291">quant</ta>
            <ta e="T293" id="Seg_10321" s="T292">n-n:(num)-n:case</ta>
            <ta e="T294" id="Seg_10322" s="T293">n-n:(num)-n:case</ta>
            <ta e="T295" id="Seg_10323" s="T294">adv</ta>
            <ta e="T296" id="Seg_10324" s="T295">n-n:case</ta>
            <ta e="T297" id="Seg_10325" s="T296">v-v:(ins)-v:ptcp</ta>
            <ta e="T298" id="Seg_10326" s="T297">n-n&gt;adj</ta>
            <ta e="T299" id="Seg_10327" s="T298">n-n:(num)-n:case</ta>
            <ta e="T300" id="Seg_10328" s="T299">propr-n:case</ta>
            <ta e="T301" id="Seg_10329" s="T300">propr-n:case</ta>
            <ta e="T302" id="Seg_10330" s="T301">v-v:cvb</ta>
            <ta e="T303" id="Seg_10331" s="T302">n-n&gt;adj-n:(num)-n:case</ta>
            <ta e="T304" id="Seg_10332" s="T303">adv</ta>
            <ta e="T305" id="Seg_10333" s="T304">adv</ta>
            <ta e="T306" id="Seg_10334" s="T305">que</ta>
            <ta e="T307" id="Seg_10335" s="T306">ptcl</ta>
            <ta e="T308" id="Seg_10336" s="T307">adj</ta>
            <ta e="T309" id="Seg_10337" s="T308">n-n:(num)-n:case</ta>
            <ta e="T310" id="Seg_10338" s="T309">dempro-pro:case</ta>
            <ta e="T311" id="Seg_10339" s="T310">adj-n:(poss)-n:case</ta>
            <ta e="T312" id="Seg_10340" s="T311">adv</ta>
            <ta e="T313" id="Seg_10341" s="T312">v-v:tense-v:pred.pn</ta>
            <ta e="T314" id="Seg_10342" s="T313">n-n:(num)-n:case</ta>
            <ta e="T315" id="Seg_10343" s="T314">ptcl</ta>
            <ta e="T316" id="Seg_10344" s="T315">n-n:poss-n:case</ta>
            <ta e="T317" id="Seg_10345" s="T316">adj</ta>
            <ta e="T318" id="Seg_10346" s="T317">n-n:(poss)-n:case</ta>
            <ta e="T319" id="Seg_10347" s="T318">dempro-pro:case</ta>
            <ta e="T320" id="Seg_10348" s="T319">n-n&gt;adj</ta>
            <ta e="T321" id="Seg_10349" s="T320">adj</ta>
            <ta e="T322" id="Seg_10350" s="T321">v-v&gt;n-n:case</ta>
            <ta e="T323" id="Seg_10351" s="T322">v-v:cvb</ta>
            <ta e="T324" id="Seg_10352" s="T323">adv</ta>
            <ta e="T325" id="Seg_10353" s="T324">v-v:tense-v:poss.pn</ta>
            <ta e="T326" id="Seg_10354" s="T325">adv-adv&gt;adv</ta>
            <ta e="T327" id="Seg_10355" s="T326">cardnum</ta>
            <ta e="T328" id="Seg_10356" s="T327">cardnum</ta>
            <ta e="T329" id="Seg_10357" s="T328">n-n&gt;adj</ta>
            <ta e="T330" id="Seg_10358" s="T329">n-n:case</ta>
            <ta e="T331" id="Seg_10359" s="T330">adv</ta>
            <ta e="T332" id="Seg_10360" s="T331">v-v:tense-v:poss.pn</ta>
            <ta e="T333" id="Seg_10361" s="T332">dempro-pro:case</ta>
            <ta e="T334" id="Seg_10362" s="T333">adj</ta>
            <ta e="T335" id="Seg_10363" s="T334">v-v&gt;n-n:case</ta>
            <ta e="T336" id="Seg_10364" s="T335">n-n:poss-n:case</ta>
            <ta e="T337" id="Seg_10365" s="T336">propr-n:case</ta>
            <ta e="T338" id="Seg_10366" s="T337">v-v:cvb</ta>
            <ta e="T339" id="Seg_10367" s="T338">adj-n:(poss)-n:case</ta>
            <ta e="T340" id="Seg_10368" s="T339">adv</ta>
            <ta e="T341" id="Seg_10369" s="T340">v-v:tense-v:poss.pn</ta>
            <ta e="T342" id="Seg_10370" s="T341">adj</ta>
            <ta e="T343" id="Seg_10371" s="T342">n-n:poss-n:case</ta>
            <ta e="T344" id="Seg_10372" s="T343">adv</ta>
            <ta e="T345" id="Seg_10373" s="T344">pers-pro:case</ta>
            <ta e="T346" id="Seg_10374" s="T345">v-v:tense-v:pred.pn</ta>
            <ta e="T347" id="Seg_10375" s="T346">dempro-pro:case</ta>
            <ta e="T348" id="Seg_10376" s="T347">n-n:poss-n:case</ta>
            <ta e="T349" id="Seg_10377" s="T348">n-n:(num)-n:case</ta>
            <ta e="T350" id="Seg_10378" s="T349">adj</ta>
            <ta e="T351" id="Seg_10379" s="T350">n-n:(poss)-n:case</ta>
            <ta e="T352" id="Seg_10380" s="T351">n-n:poss-n:case</ta>
            <ta e="T353" id="Seg_10381" s="T352">pers-pro:case</ta>
            <ta e="T354" id="Seg_10382" s="T353">v-v:cvb</ta>
            <ta e="T355" id="Seg_10383" s="T354">post</ta>
            <ta e="T356" id="Seg_10384" s="T355">adv</ta>
            <ta e="T357" id="Seg_10385" s="T356">n-n:case</ta>
            <ta e="T358" id="Seg_10386" s="T357">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T359" id="Seg_10387" s="T358">post</ta>
            <ta e="T360" id="Seg_10388" s="T359">n-n:poss-n:case-n-n:poss-n:case</ta>
            <ta e="T361" id="Seg_10389" s="T360">v-v:neg-v:poss.pn</ta>
            <ta e="T362" id="Seg_10390" s="T361">v-v:cvb</ta>
            <ta e="T363" id="Seg_10391" s="T362">v-v:tense-v:poss.pn</ta>
            <ta e="T364" id="Seg_10392" s="T363">pers-pro:case</ta>
            <ta e="T365" id="Seg_10393" s="T364">n-n:(poss)-n:case</ta>
            <ta e="T366" id="Seg_10394" s="T365">adv</ta>
            <ta e="T367" id="Seg_10395" s="T366">adj-adj&gt;adv</ta>
            <ta e="T368" id="Seg_10396" s="T367">n-n:poss-n:case</ta>
            <ta e="T369" id="Seg_10397" s="T368">v-v:cvb</ta>
            <ta e="T370" id="Seg_10398" s="T369">post</ta>
            <ta e="T371" id="Seg_10399" s="T370">n-n:(num)-n:case</ta>
            <ta e="T372" id="Seg_10400" s="T371">n-n:case</ta>
            <ta e="T373" id="Seg_10401" s="T372">post</ta>
            <ta e="T374" id="Seg_10402" s="T373">v-v:cvb</ta>
            <ta e="T375" id="Seg_10403" s="T374">adv</ta>
            <ta e="T376" id="Seg_10404" s="T375">v-v:tense-v:poss.pn</ta>
            <ta e="T377" id="Seg_10405" s="T376">pers-pro:case</ta>
            <ta e="T378" id="Seg_10406" s="T377">adj-adj&gt;adj</ta>
            <ta e="T379" id="Seg_10407" s="T378">n-n:(num)-n:case</ta>
            <ta e="T380" id="Seg_10408" s="T379">n-n:case</ta>
            <ta e="T381" id="Seg_10409" s="T380">v-v:cvb-v:pred.pn</ta>
            <ta e="T382" id="Seg_10410" s="T381">dempro</ta>
            <ta e="T383" id="Seg_10411" s="T382">n-n&gt;adj</ta>
            <ta e="T384" id="Seg_10412" s="T383">n-n:case</ta>
            <ta e="T385" id="Seg_10413" s="T952">post</ta>
            <ta e="T387" id="Seg_10414" s="T385">v-v:neg-v:(ins)-v:poss.pn</ta>
            <ta e="T388" id="Seg_10415" s="T387">pers-pro:case</ta>
            <ta e="T389" id="Seg_10416" s="T388">dempro</ta>
            <ta e="T390" id="Seg_10417" s="T389">n-n:case</ta>
            <ta e="T391" id="Seg_10418" s="T390">adv</ta>
            <ta e="T392" id="Seg_10419" s="T391">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T393" id="Seg_10420" s="T392">n-n&gt;adj</ta>
            <ta e="T394" id="Seg_10421" s="T393">n-n:case</ta>
            <ta e="T395" id="Seg_10422" s="T394">que</ta>
            <ta e="T396" id="Seg_10423" s="T395">n-n:case</ta>
            <ta e="T397" id="Seg_10424" s="T396">n-n:poss-n:case</ta>
            <ta e="T398" id="Seg_10425" s="T397">n-n:(poss)-n:case</ta>
            <ta e="T399" id="Seg_10426" s="T398">n-n&gt;adv</ta>
            <ta e="T400" id="Seg_10427" s="T399">v-v&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T401" id="Seg_10428" s="T400">quant</ta>
            <ta e="T402" id="Seg_10429" s="T401">cardnum</ta>
            <ta e="T403" id="Seg_10430" s="T402">n-n:case</ta>
            <ta e="T404" id="Seg_10431" s="T403">n-n:(poss)-n:case</ta>
            <ta e="T405" id="Seg_10432" s="T404">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T406" id="Seg_10433" s="T405">quant</ta>
            <ta e="T407" id="Seg_10434" s="T406">n-n:case</ta>
            <ta e="T408" id="Seg_10435" s="T407">v-v:ptcp</ta>
            <ta e="T409" id="Seg_10436" s="T408">n-n:case</ta>
            <ta e="T410" id="Seg_10437" s="T409">adj-adj&gt;adv</ta>
            <ta e="T411" id="Seg_10438" s="T410">n-n&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T412" id="Seg_10439" s="T411">pers-pro:case</ta>
            <ta e="T413" id="Seg_10440" s="T412">ptcl</ta>
            <ta e="T414" id="Seg_10441" s="T413">dempro</ta>
            <ta e="T415" id="Seg_10442" s="T414">v-v:(ins)-v&gt;v-v&gt;n-n:case</ta>
            <ta e="T416" id="Seg_10443" s="T415">propr-n:case</ta>
            <ta e="T417" id="Seg_10444" s="T416">n-n:poss-n:case</ta>
            <ta e="T418" id="Seg_10445" s="T417">n-n:case</ta>
            <ta e="T419" id="Seg_10446" s="T418">n-n:(poss)-n:case</ta>
            <ta e="T420" id="Seg_10447" s="T419">v-v:cvb</ta>
            <ta e="T421" id="Seg_10448" s="T420">v-v:cvb-v:pred.pn</ta>
            <ta e="T422" id="Seg_10449" s="T421">adj-adj&gt;adv</ta>
            <ta e="T423" id="Seg_10450" s="T422">adj</ta>
            <ta e="T424" id="Seg_10451" s="T423">n-n:poss-n:case</ta>
            <ta e="T425" id="Seg_10452" s="T424">n-n:case</ta>
            <ta e="T426" id="Seg_10453" s="T425">n-n&gt;v-v:(ins)-v&gt;v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T427" id="Seg_10454" s="T426">dempro-pro:case</ta>
            <ta e="T428" id="Seg_10455" s="T427">v-v:cvb</ta>
            <ta e="T429" id="Seg_10456" s="T428">post</ta>
            <ta e="T430" id="Seg_10457" s="T429">v-v:ptcp</ta>
            <ta e="T431" id="Seg_10458" s="T430">n-n:case</ta>
            <ta e="T432" id="Seg_10459" s="T431">v-v:(ins)-v:neg-v:(ins)-v:poss.pn</ta>
            <ta e="T433" id="Seg_10460" s="T432">dempro</ta>
            <ta e="T434" id="Seg_10461" s="T433">n-n&gt;adj</ta>
            <ta e="T435" id="Seg_10462" s="T434">v-v:(ins)-v&gt;v-v&gt;n-n:case</ta>
            <ta e="T436" id="Seg_10463" s="T435">pers-pro:case</ta>
            <ta e="T437" id="Seg_10464" s="T436">ptcl</ta>
            <ta e="T438" id="Seg_10465" s="T437">ptcl</ta>
            <ta e="T439" id="Seg_10466" s="T438">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T440" id="Seg_10467" s="T439">cardnum-n:case</ta>
            <ta e="T441" id="Seg_10468" s="T440">postp</ta>
            <ta e="T442" id="Seg_10469" s="T441">n-n:case</ta>
            <ta e="T443" id="Seg_10470" s="T442">n-n:poss-n:case</ta>
            <ta e="T444" id="Seg_10471" s="T443">dempro</ta>
            <ta e="T445" id="Seg_10472" s="T444">cardnum</ta>
            <ta e="T446" id="Seg_10473" s="T445">n-n:case</ta>
            <ta e="T447" id="Seg_10474" s="T446">v-v:(ins)-v&gt;v-v&gt;n-n:case</ta>
            <ta e="T448" id="Seg_10475" s="T447">v-v:tense-v:poss.pn</ta>
            <ta e="T449" id="Seg_10476" s="T448">cardnum</ta>
            <ta e="T450" id="Seg_10477" s="T449">cardnum</ta>
            <ta e="T451" id="Seg_10478" s="T450">ptcl</ta>
            <ta e="T452" id="Seg_10479" s="T451">n-n:case</ta>
            <ta e="T453" id="Seg_10480" s="T452">dempro-pro:case</ta>
            <ta e="T454" id="Seg_10481" s="T453">n-n:poss-n:case</ta>
            <ta e="T455" id="Seg_10482" s="T454">adv</ta>
            <ta e="T456" id="Seg_10483" s="T455">cardnum</ta>
            <ta e="T457" id="Seg_10484" s="T456">pers-pro:case</ta>
            <ta e="T458" id="Seg_10485" s="T457">n-n:case</ta>
            <ta e="T459" id="Seg_10486" s="T458">v-v:ptcp</ta>
            <ta e="T460" id="Seg_10487" s="T459">n-n:case</ta>
            <ta e="T461" id="Seg_10488" s="T460">ptcl</ta>
            <ta e="T462" id="Seg_10489" s="T461">v-v:tense-v:poss.pn</ta>
            <ta e="T463" id="Seg_10490" s="T462">dempro-pro:case</ta>
            <ta e="T464" id="Seg_10491" s="T463">adj-n:case</ta>
            <ta e="T465" id="Seg_10492" s="T464">v-v:tense-v:poss.pn</ta>
            <ta e="T466" id="Seg_10493" s="T465">v-v:ptcp-v:(case)</ta>
            <ta e="T467" id="Seg_10494" s="T466">que</ta>
            <ta e="T468" id="Seg_10495" s="T467">n-n&gt;adj-n:case</ta>
            <ta e="T469" id="Seg_10496" s="T468">n-n:case</ta>
            <ta e="T470" id="Seg_10497" s="T469">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T471" id="Seg_10498" s="T470">propr-n:case</ta>
            <ta e="T472" id="Seg_10499" s="T471">adj-n:poss-n:case</ta>
            <ta e="T473" id="Seg_10500" s="T472">que</ta>
            <ta e="T474" id="Seg_10501" s="T473">n-n:case</ta>
            <ta e="T475" id="Seg_10502" s="T474">v-v&gt;v-v:(ins)-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T476" id="Seg_10503" s="T475">que</ta>
            <ta e="T477" id="Seg_10504" s="T476">n-n:case</ta>
            <ta e="T478" id="Seg_10505" s="T477">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T479" id="Seg_10506" s="T478">propr-n:case</ta>
            <ta e="T480" id="Seg_10507" s="T479">v-v:cvb</ta>
            <ta e="T481" id="Seg_10508" s="T480">post</ta>
            <ta e="T482" id="Seg_10509" s="T481">pers-pro:case</ta>
            <ta e="T483" id="Seg_10510" s="T482">n-n:(poss)-n:case</ta>
            <ta e="T484" id="Seg_10511" s="T483">adv</ta>
            <ta e="T485" id="Seg_10512" s="T484">v-v:ptcp</ta>
            <ta e="T486" id="Seg_10513" s="T485">n-n:case</ta>
            <ta e="T487" id="Seg_10514" s="T486">n-n:case</ta>
            <ta e="T488" id="Seg_10515" s="T487">post</ta>
            <ta e="T489" id="Seg_10516" s="T488">v-v:cvb</ta>
            <ta e="T490" id="Seg_10517" s="T489">v-v:tense-v:poss.pn</ta>
            <ta e="T491" id="Seg_10518" s="T490">dempro</ta>
            <ta e="T492" id="Seg_10519" s="T491">propr-n:case</ta>
            <ta e="T493" id="Seg_10520" s="T492">n-n:poss-n:case</ta>
            <ta e="T494" id="Seg_10521" s="T493">pers-pro:case</ta>
            <ta e="T495" id="Seg_10522" s="T494">ptcl</ta>
            <ta e="T496" id="Seg_10523" s="T495">n-n:case</ta>
            <ta e="T497" id="Seg_10524" s="T496">n-n:case</ta>
            <ta e="T498" id="Seg_10525" s="T497">n-n:poss-n:case</ta>
            <ta e="T499" id="Seg_10526" s="T498">ptcl</ta>
            <ta e="T500" id="Seg_10527" s="T499">v-v:tense-v:poss.pn</ta>
            <ta e="T501" id="Seg_10528" s="T500">cardnum</ta>
            <ta e="T502" id="Seg_10529" s="T501">cardnum</ta>
            <ta e="T503" id="Seg_10530" s="T502">cardnum</ta>
            <ta e="T504" id="Seg_10531" s="T503">cardnum-cardnum</ta>
            <ta e="T505" id="Seg_10532" s="T504">cardnum</ta>
            <ta e="T506" id="Seg_10533" s="T505">n-n&gt;adj-n:case</ta>
            <ta e="T507" id="Seg_10534" s="T506">n-n&gt;adj</ta>
            <ta e="T508" id="Seg_10535" s="T507">n-n:case</ta>
            <ta e="T509" id="Seg_10536" s="T508">n-n:case</ta>
            <ta e="T510" id="Seg_10537" s="T509">post</ta>
            <ta e="T511" id="Seg_10538" s="T510">dempro</ta>
            <ta e="T512" id="Seg_10539" s="T511">n-n:case</ta>
            <ta e="T513" id="Seg_10540" s="T512">adj</ta>
            <ta e="T514" id="Seg_10541" s="T513">n-n:poss-n:case</ta>
            <ta e="T515" id="Seg_10542" s="T514">post</ta>
            <ta e="T516" id="Seg_10543" s="T515">adv</ta>
            <ta e="T517" id="Seg_10544" s="T516">v-v:cvb</ta>
            <ta e="T518" id="Seg_10545" s="T517">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T519" id="Seg_10546" s="T518">dempro</ta>
            <ta e="T520" id="Seg_10547" s="T519">n-n:(num)-n:case</ta>
            <ta e="T521" id="Seg_10548" s="T520">propr-n:case</ta>
            <ta e="T522" id="Seg_10549" s="T521">adv</ta>
            <ta e="T523" id="Seg_10550" s="T522">propr-n:case</ta>
            <ta e="T524" id="Seg_10551" s="T523">adv</ta>
            <ta e="T525" id="Seg_10552" s="T524">propr-n:case</ta>
            <ta e="T526" id="Seg_10553" s="T525">adv</ta>
            <ta e="T527" id="Seg_10554" s="T526">propr-n:case</ta>
            <ta e="T528" id="Seg_10555" s="T527">adv</ta>
            <ta e="T529" id="Seg_10556" s="T528">propr-n:case</ta>
            <ta e="T530" id="Seg_10557" s="T529">v-v:cvb</ta>
            <ta e="T531" id="Seg_10558" s="T530">n-n&gt;adj</ta>
            <ta e="T532" id="Seg_10559" s="T531">adj</ta>
            <ta e="T533" id="Seg_10560" s="T532">n-n:(num)-n:case</ta>
            <ta e="T534" id="Seg_10561" s="T533">v-v:ptcp-v:(case)</ta>
            <ta e="T535" id="Seg_10562" s="T534">adj</ta>
            <ta e="T536" id="Seg_10563" s="T535">v-v:cvb</ta>
            <ta e="T537" id="Seg_10564" s="T536">n-n:case</ta>
            <ta e="T538" id="Seg_10565" s="T537">n-n:poss-n:case</ta>
            <ta e="T539" id="Seg_10566" s="T538">dempro</ta>
            <ta e="T540" id="Seg_10567" s="T539">n-n:(num)-n:case</ta>
            <ta e="T541" id="Seg_10568" s="T540">adv</ta>
            <ta e="T542" id="Seg_10569" s="T541">adj-adj&gt;adv</ta>
            <ta e="T543" id="Seg_10570" s="T542">pers-pro:case</ta>
            <ta e="T544" id="Seg_10571" s="T543">v-v:tense-v:pred.pn</ta>
            <ta e="T545" id="Seg_10572" s="T544">n-n:case</ta>
            <ta e="T546" id="Seg_10573" s="T545">post</ta>
            <ta e="T547" id="Seg_10574" s="T546">propr-n:case</ta>
            <ta e="T548" id="Seg_10575" s="T547">n-n:case</ta>
            <ta e="T549" id="Seg_10576" s="T548">v-v:(ins)-v&gt;v-v&gt;n-n:case</ta>
            <ta e="T550" id="Seg_10577" s="T549">dempro</ta>
            <ta e="T551" id="Seg_10578" s="T550">n-n:case</ta>
            <ta e="T552" id="Seg_10579" s="T551">cardnum-n:case</ta>
            <ta e="T553" id="Seg_10580" s="T552">post</ta>
            <ta e="T554" id="Seg_10581" s="T553">adv</ta>
            <ta e="T555" id="Seg_10582" s="T554">n-n:case</ta>
            <ta e="T556" id="Seg_10583" s="T555">n-n:case</ta>
            <ta e="T557" id="Seg_10584" s="T556">v-v:cvb</ta>
            <ta e="T558" id="Seg_10585" s="T557">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T559" id="Seg_10586" s="T558">adv</ta>
            <ta e="T560" id="Seg_10587" s="T559">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T561" id="Seg_10588" s="T560">v-v&gt;n-n:poss-n:case</ta>
            <ta e="T562" id="Seg_10589" s="T561">pers-pro:case</ta>
            <ta e="T563" id="Seg_10590" s="T562">n-n:(poss)-n:case</ta>
            <ta e="T564" id="Seg_10591" s="T563">v-v:neg-v:pred.pn</ta>
            <ta e="T565" id="Seg_10592" s="T564">adv</ta>
            <ta e="T566" id="Seg_10593" s="T565">adj</ta>
            <ta e="T567" id="Seg_10594" s="T566">v-v:(ins)-v&gt;v-v&gt;n-n:case</ta>
            <ta e="T568" id="Seg_10595" s="T567">adv</ta>
            <ta e="T569" id="Seg_10596" s="T568">ptcl</ta>
            <ta e="T570" id="Seg_10597" s="T569">v-v:tense-v:poss.pn</ta>
            <ta e="T571" id="Seg_10598" s="T570">propr-n:case</ta>
            <ta e="T572" id="Seg_10599" s="T571">v-v:cvb</ta>
            <ta e="T573" id="Seg_10600" s="T572">n-n&gt;adj</ta>
            <ta e="T574" id="Seg_10601" s="T573">n-n:case</ta>
            <ta e="T575" id="Seg_10602" s="T574">v-v&gt;n-n:poss-n:case</ta>
            <ta e="T576" id="Seg_10603" s="T575">dempro</ta>
            <ta e="T577" id="Seg_10604" s="T576">n-n:case</ta>
            <ta e="T578" id="Seg_10605" s="T577">adv-adv&gt;adj</ta>
            <ta e="T579" id="Seg_10606" s="T578">n-n:poss-n:case</ta>
            <ta e="T580" id="Seg_10607" s="T579">v-v:cvb</ta>
            <ta e="T581" id="Seg_10608" s="T580">n-n:(num)-n:case</ta>
            <ta e="T582" id="Seg_10609" s="T581">n-n:(num)-n:case-n-n:(num)-n:case</ta>
            <ta e="T583" id="Seg_10610" s="T582">v-v:cvb</ta>
            <ta e="T584" id="Seg_10611" s="T583">v-v:cvb-v:pred.pn</ta>
            <ta e="T585" id="Seg_10612" s="T584">pers-pro:case</ta>
            <ta e="T586" id="Seg_10613" s="T585">adv</ta>
            <ta e="T587" id="Seg_10614" s="T586">v-v:cvb</ta>
            <ta e="T588" id="Seg_10615" s="T587">v-v:(ins)-v&gt;v-v&gt;v-v:ptcp</ta>
            <ta e="T589" id="Seg_10616" s="T588">v-v:tense-v:poss.pn</ta>
            <ta e="T590" id="Seg_10617" s="T589">dempro-pro:case</ta>
            <ta e="T591" id="Seg_10618" s="T590">post</ta>
            <ta e="T592" id="Seg_10619" s="T591">pers-pro:case</ta>
            <ta e="T593" id="Seg_10620" s="T592">que</ta>
            <ta e="T594" id="Seg_10621" s="T593">ptcl</ta>
            <ta e="T595" id="Seg_10622" s="T594">n-n:case</ta>
            <ta e="T596" id="Seg_10623" s="T595">v-v:cvb</ta>
            <ta e="T597" id="Seg_10624" s="T596">v-v:(ins)-v:neg-v:pred.pn</ta>
            <ta e="T598" id="Seg_10625" s="T597">n-n&gt;adj</ta>
            <ta e="T599" id="Seg_10626" s="T598">n-n:case</ta>
            <ta e="T600" id="Seg_10627" s="T599">v-v:tense-v:pred.pn</ta>
            <ta e="T601" id="Seg_10628" s="T600">n-n:case</ta>
            <ta e="T602" id="Seg_10629" s="T601">v-v:ptcp</ta>
            <ta e="T603" id="Seg_10630" s="T602">n-n:(num)-n:case</ta>
            <ta e="T604" id="Seg_10631" s="T603">adv</ta>
            <ta e="T605" id="Seg_10632" s="T604">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T606" id="Seg_10633" s="T605">n-n:(num)-n:case</ta>
            <ta e="T607" id="Seg_10634" s="T606">n-n:(poss)-n:case</ta>
            <ta e="T608" id="Seg_10635" s="T607">v-v:cvb</ta>
            <ta e="T609" id="Seg_10636" s="T608">v-v:cvb-v-v:cvb</ta>
            <ta e="T610" id="Seg_10637" s="T609">que</ta>
            <ta e="T611" id="Seg_10638" s="T610">ptcl</ta>
            <ta e="T612" id="Seg_10639" s="T611">n-n:(num)-n:case</ta>
            <ta e="T613" id="Seg_10640" s="T612">v-v&gt;v-v:cvb</ta>
            <ta e="T614" id="Seg_10641" s="T613">adv</ta>
            <ta e="T615" id="Seg_10642" s="T614">v-v:ptcp</ta>
            <ta e="T616" id="Seg_10643" s="T615">v-v:tense-v:poss.pn</ta>
            <ta e="T617" id="Seg_10644" s="T616">dempro-pro:case</ta>
            <ta e="T618" id="Seg_10645" s="T617">post</ta>
            <ta e="T619" id="Seg_10646" s="T618">adv</ta>
            <ta e="T620" id="Seg_10647" s="T619">pers-pro:case</ta>
            <ta e="T621" id="Seg_10648" s="T620">adv</ta>
            <ta e="T622" id="Seg_10649" s="T621">quant</ta>
            <ta e="T623" id="Seg_10650" s="T622">n-n:(ins)-n:case</ta>
            <ta e="T624" id="Seg_10651" s="T623">n-n:(num)-n:case</ta>
            <ta e="T625" id="Seg_10652" s="T624">v-v&gt;v-v:ptcp</ta>
            <ta e="T626" id="Seg_10653" s="T625">v-v:tense-v:poss.pn</ta>
            <ta e="T627" id="Seg_10654" s="T626">n-n:case</ta>
            <ta e="T628" id="Seg_10655" s="T627">n-n:case</ta>
            <ta e="T629" id="Seg_10656" s="T628">adv</ta>
            <ta e="T630" id="Seg_10657" s="T629">conj</ta>
            <ta e="T631" id="Seg_10658" s="T630">v-v:ptcp</ta>
            <ta e="T632" id="Seg_10659" s="T631">n-n:(num)-n:poss-n:case</ta>
            <ta e="T633" id="Seg_10660" s="T632">v-v&gt;v-v:ptcp</ta>
            <ta e="T634" id="Seg_10661" s="T633">v-v:tense-v:poss.pn</ta>
            <ta e="T635" id="Seg_10662" s="T634">adv</ta>
            <ta e="T636" id="Seg_10663" s="T635">pers-pro:case</ta>
            <ta e="T637" id="Seg_10664" s="T636">n-n:(num)-n:case</ta>
            <ta e="T638" id="Seg_10665" s="T637">adv</ta>
            <ta e="T639" id="Seg_10666" s="T638">v-v:cvb</ta>
            <ta e="T640" id="Seg_10667" s="T639">post</ta>
            <ta e="T641" id="Seg_10668" s="T640">adj</ta>
            <ta e="T642" id="Seg_10669" s="T641">n-n:case</ta>
            <ta e="T643" id="Seg_10670" s="T642">n-n:poss-n:case</ta>
            <ta e="T644" id="Seg_10671" s="T643">adv</ta>
            <ta e="T645" id="Seg_10672" s="T644">v-v&gt;v-v:cvb</ta>
            <ta e="T646" id="Seg_10673" s="T645">post</ta>
            <ta e="T647" id="Seg_10674" s="T646">que</ta>
            <ta e="T648" id="Seg_10675" s="T647">ptcl</ta>
            <ta e="T649" id="Seg_10676" s="T648">cardnum-cardnum&gt;distrnum</ta>
            <ta e="T650" id="Seg_10677" s="T649">n-n:(num)-n:case</ta>
            <ta e="T651" id="Seg_10678" s="T650">adv-adv&gt;adj</ta>
            <ta e="T652" id="Seg_10679" s="T651">n-n:poss-n:case</ta>
            <ta e="T653" id="Seg_10680" s="T652">v-v:cvb</ta>
            <ta e="T654" id="Seg_10681" s="T653">post</ta>
            <ta e="T655" id="Seg_10682" s="T654">dempro-pro:case</ta>
            <ta e="T656" id="Seg_10683" s="T655">adj-n:poss-n:case</ta>
            <ta e="T657" id="Seg_10684" s="T656">v-v&gt;v-v:cvb</ta>
            <ta e="T658" id="Seg_10685" s="T657">v-v:cvb</ta>
            <ta e="T659" id="Seg_10686" s="T658">adv-adv&gt;adj-n:case</ta>
            <ta e="T660" id="Seg_10687" s="T659">post</ta>
            <ta e="T661" id="Seg_10688" s="T660">v-v:cvb</ta>
            <ta e="T662" id="Seg_10689" s="T661">v-v&gt;v-v:cvb</ta>
            <ta e="T663" id="Seg_10690" s="T662">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T664" id="Seg_10691" s="T663">pers-pro:case</ta>
            <ta e="T665" id="Seg_10692" s="T664">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T666" id="Seg_10693" s="T665">dempro</ta>
            <ta e="T667" id="Seg_10694" s="T666">n-n:case</ta>
            <ta e="T668" id="Seg_10695" s="T667">n-n:poss-n:case</ta>
            <ta e="T669" id="Seg_10696" s="T668">n-n:case</ta>
            <ta e="T670" id="Seg_10697" s="T669">v-v:cvb</ta>
            <ta e="T671" id="Seg_10698" s="T670">adj</ta>
            <ta e="T672" id="Seg_10699" s="T671">n-n:case</ta>
            <ta e="T673" id="Seg_10700" s="T672">n-n:case</ta>
            <ta e="T674" id="Seg_10701" s="T673">indfpro-pro:(num)-pro:case</ta>
            <ta e="T675" id="Seg_10702" s="T674">que-que&gt;distrnum</ta>
            <ta e="T676" id="Seg_10703" s="T675">ptcl</ta>
            <ta e="T677" id="Seg_10704" s="T676">n-n:(num)-n:case</ta>
            <ta e="T678" id="Seg_10705" s="T677">adv</ta>
            <ta e="T679" id="Seg_10706" s="T678">v-v:cvb</ta>
            <ta e="T680" id="Seg_10707" s="T679">v-v:cvb</ta>
            <ta e="T681" id="Seg_10708" s="T680">post</ta>
            <ta e="T682" id="Seg_10709" s="T681">adj-n:(num)-n:case</ta>
            <ta e="T683" id="Seg_10710" s="T682">adv</ta>
            <ta e="T684" id="Seg_10711" s="T683">v-v:cvb</ta>
            <ta e="T685" id="Seg_10712" s="T684">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T686" id="Seg_10713" s="T685">propr-n:case</ta>
            <ta e="T687" id="Seg_10714" s="T686">v-v:cvb</ta>
            <ta e="T688" id="Seg_10715" s="T687">n-n:case</ta>
            <ta e="T689" id="Seg_10716" s="T688">adv</ta>
            <ta e="T690" id="Seg_10717" s="T689">n-n:case</ta>
            <ta e="T691" id="Seg_10718" s="T690">adj</ta>
            <ta e="T692" id="Seg_10719" s="T691">n-n:case</ta>
            <ta e="T693" id="Seg_10720" s="T692">n-n:case</ta>
            <ta e="T694" id="Seg_10721" s="T693">n-n:case</ta>
            <ta e="T695" id="Seg_10722" s="T694">que-pro:case</ta>
            <ta e="T696" id="Seg_10723" s="T695">ptcl</ta>
            <ta e="T697" id="Seg_10724" s="T696">adj</ta>
            <ta e="T698" id="Seg_10725" s="T697">n-n:case</ta>
            <ta e="T699" id="Seg_10726" s="T698">n-n:poss-n:case</ta>
            <ta e="T700" id="Seg_10727" s="T699">n-n:(poss)-n:case</ta>
            <ta e="T701" id="Seg_10728" s="T700">n-n&gt;adj-n:case</ta>
            <ta e="T702" id="Seg_10729" s="T701">v-v:tense-v:poss.pn</ta>
            <ta e="T703" id="Seg_10730" s="T702">n-n:case</ta>
            <ta e="T704" id="Seg_10731" s="T703">post</ta>
            <ta e="T705" id="Seg_10732" s="T704">n-n:case</ta>
            <ta e="T706" id="Seg_10733" s="T705">dempro</ta>
            <ta e="T707" id="Seg_10734" s="T706">v-v:cvb</ta>
            <ta e="T708" id="Seg_10735" s="T707">v-v&gt;n-n:case</ta>
            <ta e="T709" id="Seg_10736" s="T708">v-v:cvb</ta>
            <ta e="T710" id="Seg_10737" s="T709">v-v:cvb</ta>
            <ta e="T711" id="Seg_10738" s="T710">adv</ta>
            <ta e="T712" id="Seg_10739" s="T711">n-n:case</ta>
            <ta e="T713" id="Seg_10740" s="T712">n-n:poss-n:case</ta>
            <ta e="T714" id="Seg_10741" s="T713">v-v&gt;v-v:ptcp</ta>
            <ta e="T715" id="Seg_10742" s="T714">v-v:tense-v:poss.pn</ta>
            <ta e="T716" id="Seg_10743" s="T715">n-n:case</ta>
            <ta e="T717" id="Seg_10744" s="T716">v-v:ptcp-v:(case)</ta>
            <ta e="T718" id="Seg_10745" s="T717">n-n:case</ta>
            <ta e="T719" id="Seg_10746" s="T718">post</ta>
            <ta e="T720" id="Seg_10747" s="T719">n-n:case</ta>
            <ta e="T721" id="Seg_10748" s="T720">adv</ta>
            <ta e="T722" id="Seg_10749" s="T721">n-n:case</ta>
            <ta e="T723" id="Seg_10750" s="T722">que-pro:case</ta>
            <ta e="T724" id="Seg_10751" s="T723">ptcl</ta>
            <ta e="T725" id="Seg_10752" s="T724">v-v:ptcp</ta>
            <ta e="T726" id="Seg_10753" s="T725">v-v:ptcp</ta>
            <ta e="T727" id="Seg_10754" s="T726">v-v:tense-v:poss.pn</ta>
            <ta e="T728" id="Seg_10755" s="T727">n-n:(poss)-n:case</ta>
            <ta e="T729" id="Seg_10756" s="T728">v-v:(ins)-v:ptcp</ta>
            <ta e="T730" id="Seg_10757" s="T729">adj</ta>
            <ta e="T731" id="Seg_10758" s="T730">n-n:case</ta>
            <ta e="T732" id="Seg_10759" s="T731">n-n:(poss)-n:case</ta>
            <ta e="T733" id="Seg_10760" s="T732">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T734" id="Seg_10761" s="T733">post</ta>
            <ta e="T735" id="Seg_10762" s="T734">v-v:tense-v:poss.pn</ta>
            <ta e="T736" id="Seg_10763" s="T735">dempro</ta>
            <ta e="T737" id="Seg_10764" s="T736">n-n:case</ta>
            <ta e="T738" id="Seg_10765" s="T737">v-v&gt;n-n:poss-n:case</ta>
            <ta e="T739" id="Seg_10766" s="T738">quant</ta>
            <ta e="T740" id="Seg_10767" s="T739">n-n:case</ta>
            <ta e="T741" id="Seg_10768" s="T740">v-v:cvb</ta>
            <ta e="T742" id="Seg_10769" s="T741">n-n:case</ta>
            <ta e="T743" id="Seg_10770" s="T742">n-n:poss-n:case</ta>
            <ta e="T744" id="Seg_10771" s="T743">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T745" id="Seg_10772" s="T744">que-pro:case</ta>
            <ta e="T746" id="Seg_10773" s="T745">ptcl</ta>
            <ta e="T747" id="Seg_10774" s="T746">v-v:ptcp</ta>
            <ta e="T748" id="Seg_10775" s="T747">adv</ta>
            <ta e="T749" id="Seg_10776" s="T748">v-v:tense-v:poss.pn</ta>
            <ta e="T750" id="Seg_10777" s="T749">n-n:case</ta>
            <ta e="T751" id="Seg_10778" s="T750">v-v:cvb</ta>
            <ta e="T752" id="Seg_10779" s="T751">post</ta>
            <ta e="T753" id="Seg_10780" s="T752">posspr-pro:case</ta>
            <ta e="T754" id="Seg_10781" s="T753">ptcl</ta>
            <ta e="T755" id="Seg_10782" s="T754">n-n:(num)-n:case</ta>
            <ta e="T756" id="Seg_10783" s="T755">n-n:case</ta>
            <ta e="T757" id="Seg_10784" s="T756">v-v&gt;v-v:cvb</ta>
            <ta e="T758" id="Seg_10785" s="T757">adv</ta>
            <ta e="T759" id="Seg_10786" s="T758">v-v:tense-v:poss.pn</ta>
            <ta e="T760" id="Seg_10787" s="T759">adv</ta>
            <ta e="T761" id="Seg_10788" s="T760">pers-pro:case</ta>
            <ta e="T762" id="Seg_10789" s="T761">que</ta>
            <ta e="T763" id="Seg_10790" s="T762">ptcl</ta>
            <ta e="T764" id="Seg_10791" s="T763">n-n:case</ta>
            <ta e="T765" id="Seg_10792" s="T764">v-v:cvb-v:pred.pn</ta>
            <ta e="T766" id="Seg_10793" s="T765">cardnum-cardnum&gt;ordnum-ordnum&gt;adj</ta>
            <ta e="T767" id="Seg_10794" s="T766">n-n:poss-n:case</ta>
            <ta e="T768" id="Seg_10795" s="T767">n-n:case</ta>
            <ta e="T769" id="Seg_10796" s="T768">n-n:case</ta>
            <ta e="T770" id="Seg_10797" s="T769">cardnum</ta>
            <ta e="T771" id="Seg_10798" s="T770">cardnum</ta>
            <ta e="T772" id="Seg_10799" s="T771">cardnum</ta>
            <ta e="T773" id="Seg_10800" s="T772">cardnum-cardnum</ta>
            <ta e="T774" id="Seg_10801" s="T773">cardnum</ta>
            <ta e="T775" id="Seg_10802" s="T774">n-n&gt;adj-n:case</ta>
            <ta e="T776" id="Seg_10803" s="T775">adj-n:poss-n:case</ta>
            <ta e="T777" id="Seg_10804" s="T776">n-n&gt;v-v:(ins)-v&gt;v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T778" id="Seg_10805" s="T777">adv</ta>
            <ta e="T779" id="Seg_10806" s="T778">adj-adj&gt;adv</ta>
            <ta e="T780" id="Seg_10807" s="T779">adj</ta>
            <ta e="T781" id="Seg_10808" s="T780">post</ta>
            <ta e="T782" id="Seg_10809" s="T781">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T783" id="Seg_10810" s="T782">n-n:poss-n:case</ta>
            <ta e="T784" id="Seg_10811" s="T783">adv</ta>
            <ta e="T785" id="Seg_10812" s="T784">v-v&gt;v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T786" id="Seg_10813" s="T785">dempro-pro:case</ta>
            <ta e="T787" id="Seg_10814" s="T786">v-v:(ins)-v&gt;v-v:cvb-v:pred.pn</ta>
            <ta e="T788" id="Seg_10815" s="T787">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T789" id="Seg_10816" s="T788">post</ta>
            <ta e="T790" id="Seg_10817" s="T789">cardnum</ta>
            <ta e="T791" id="Seg_10818" s="T790">n-n:case</ta>
            <ta e="T792" id="Seg_10819" s="T791">post</ta>
            <ta e="T793" id="Seg_10820" s="T792">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T794" id="Seg_10821" s="T793">propr-n:case</ta>
            <ta e="T795" id="Seg_10822" s="T794">v-v:cvb</ta>
            <ta e="T796" id="Seg_10823" s="T795">n-n:case</ta>
            <ta e="T797" id="Seg_10824" s="T796">propr-n:case</ta>
            <ta e="T798" id="Seg_10825" s="T797">v-v:cvb</ta>
            <ta e="T799" id="Seg_10826" s="T798">adj</ta>
            <ta e="T800" id="Seg_10827" s="T799">n-n:case</ta>
            <ta e="T801" id="Seg_10828" s="T800">adv</ta>
            <ta e="T802" id="Seg_10829" s="T801">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T803" id="Seg_10830" s="T802">post</ta>
            <ta e="T804" id="Seg_10831" s="T803">n-n:case</ta>
            <ta e="T805" id="Seg_10832" s="T804">n-n&gt;adj-n:case</ta>
            <ta e="T806" id="Seg_10833" s="T805">v-v:cvb</ta>
            <ta e="T807" id="Seg_10834" s="T806">v-v:cvb-v:pred.pn</ta>
            <ta e="T808" id="Seg_10835" s="T807">n-n:case</ta>
            <ta e="T809" id="Seg_10836" s="T808">adv</ta>
            <ta e="T810" id="Seg_10837" s="T809">v-v:(ins)-v&gt;v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T811" id="Seg_10838" s="T810">n-n:case</ta>
            <ta e="T813" id="Seg_10839" s="T812">v-v:(ins)-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T814" id="Seg_10840" s="T813">adv</ta>
            <ta e="T815" id="Seg_10841" s="T814">v-v:ptcp</ta>
            <ta e="T816" id="Seg_10842" s="T815">n-n:poss-n:case</ta>
            <ta e="T817" id="Seg_10843" s="T816">cardnum</ta>
            <ta e="T818" id="Seg_10844" s="T817">n-n:case</ta>
            <ta e="T819" id="Seg_10845" s="T818">n-n:poss-n:case</ta>
            <ta e="T820" id="Seg_10846" s="T819">post</ta>
            <ta e="T821" id="Seg_10847" s="T820">n-n:case</ta>
            <ta e="T822" id="Seg_10848" s="T821">n-n:poss-n:case</ta>
            <ta e="T823" id="Seg_10849" s="T822">v-v:cvb-v:pred.pn</ta>
            <ta e="T824" id="Seg_10850" s="T823">adv</ta>
            <ta e="T825" id="Seg_10851" s="T824">cardnum-n:case</ta>
            <ta e="T826" id="Seg_10852" s="T825">n-n:case</ta>
            <ta e="T827" id="Seg_10853" s="T826">n-n&gt;v-v:(ins)-v&gt;v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T828" id="Seg_10854" s="T827">dempro-pro:case</ta>
            <ta e="T829" id="Seg_10855" s="T828">n-n:poss-n:case</ta>
            <ta e="T830" id="Seg_10856" s="T829">cardnum-cardnum&gt;adv</ta>
            <ta e="T831" id="Seg_10857" s="T830">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T832" id="Seg_10858" s="T831">v-v:(ins)-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T833" id="Seg_10859" s="T832">adj-adj&gt;adv</ta>
            <ta e="T834" id="Seg_10860" s="T833">v-v:ptcp-v:(case)</ta>
            <ta e="T835" id="Seg_10861" s="T834">dempro</ta>
            <ta e="T836" id="Seg_10862" s="T835">pers-pro:case</ta>
            <ta e="T837" id="Seg_10863" s="T836">n-n:case</ta>
            <ta e="T838" id="Seg_10864" s="T837">n-n:case</ta>
            <ta e="T839" id="Seg_10865" s="T838">n-n:poss-n:case</ta>
            <ta e="T840" id="Seg_10866" s="T839">que</ta>
            <ta e="T841" id="Seg_10867" s="T840">ptcl</ta>
            <ta e="T842" id="Seg_10868" s="T841">v-v&gt;v-v:(ins)-v:ptcp</ta>
            <ta e="T843" id="Seg_10869" s="T842">n-n:case</ta>
            <ta e="T844" id="Seg_10870" s="T843">conj</ta>
            <ta e="T845" id="Seg_10871" s="T844">dempro</ta>
            <ta e="T846" id="Seg_10872" s="T845">adj</ta>
            <ta e="T847" id="Seg_10873" s="T846">n-n:poss-n:case</ta>
            <ta e="T848" id="Seg_10874" s="T847">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T849" id="Seg_10875" s="T848">n-n:poss-n:case</ta>
            <ta e="T850" id="Seg_10876" s="T849">v-v:ptcp</ta>
            <ta e="T851" id="Seg_10877" s="T850">v-v:ptcp</ta>
            <ta e="T852" id="Seg_10878" s="T851">n-n:poss-n:case</ta>
            <ta e="T853" id="Seg_10879" s="T852">v-v:cvb</ta>
            <ta e="T854" id="Seg_10880" s="T853">v-v:(ins)-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T855" id="Seg_10881" s="T854">posspr</ta>
            <ta e="T856" id="Seg_10882" s="T855">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T857" id="Seg_10883" s="T856">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T858" id="Seg_10884" s="T857">dempro</ta>
            <ta e="T859" id="Seg_10885" s="T858">n-n:case</ta>
            <ta e="T860" id="Seg_10886" s="T859">adv</ta>
            <ta e="T861" id="Seg_10887" s="T860">v-v&gt;v-v:(ins)-v:neg-v:poss.pn</ta>
            <ta e="T862" id="Seg_10888" s="T861">posspr</ta>
            <ta e="T863" id="Seg_10889" s="T862">adj</ta>
            <ta e="T864" id="Seg_10890" s="T863">n-n:(poss)-n:case</ta>
            <ta e="T865" id="Seg_10891" s="T864">n-n:case</ta>
            <ta e="T866" id="Seg_10892" s="T865">n-n:(poss)-n:case</ta>
            <ta e="T867" id="Seg_10893" s="T866">adv</ta>
            <ta e="T868" id="Seg_10894" s="T867">posspr</ta>
            <ta e="T869" id="Seg_10895" s="T868">n-n:poss-n:case</ta>
            <ta e="T870" id="Seg_10896" s="T869">n-n:poss-n:case</ta>
            <ta e="T871" id="Seg_10897" s="T870">v-v:ptcp</ta>
            <ta e="T872" id="Seg_10898" s="T871">n-n:(poss)-n:case</ta>
            <ta e="T873" id="Seg_10899" s="T872">v-v:tense-v:(poss)</ta>
            <ta e="T874" id="Seg_10900" s="T873">pers-pro:case</ta>
            <ta e="T875" id="Seg_10901" s="T874">n-n:case</ta>
            <ta e="T876" id="Seg_10902" s="T875">adj</ta>
            <ta e="T877" id="Seg_10903" s="T876">n-n:poss-n:case</ta>
            <ta e="T878" id="Seg_10904" s="T877">n-n:poss-n:case</ta>
            <ta e="T882" id="Seg_10905" s="T881">v-v:cvb</ta>
            <ta e="T883" id="Seg_10906" s="T882">adv</ta>
            <ta e="T884" id="Seg_10907" s="T883">adv</ta>
            <ta e="T885" id="Seg_10908" s="T884">n-n:case</ta>
            <ta e="T890" id="Seg_10909" s="T889">v-v:cvb</ta>
            <ta e="T891" id="Seg_10910" s="T890">adv</ta>
            <ta e="T892" id="Seg_10911" s="T891">pers-pro:case</ta>
            <ta e="T893" id="Seg_10912" s="T892">adj</ta>
            <ta e="T894" id="Seg_10913" s="T893">n-n:poss-n:case</ta>
            <ta e="T895" id="Seg_10914" s="T894">v-v:ptcp</ta>
            <ta e="T896" id="Seg_10915" s="T895">n-n:(num)-n:case</ta>
            <ta e="T897" id="Seg_10916" s="T896">n-n:(poss)-n:case</ta>
            <ta e="T898" id="Seg_10917" s="T897">adv</ta>
            <ta e="T899" id="Seg_10918" s="T898">v-v:cvb</ta>
            <ta e="T900" id="Seg_10919" s="T899">v-v:tense-v:pred.pn</ta>
            <ta e="T901" id="Seg_10920" s="T900">n-n:case</ta>
            <ta e="T902" id="Seg_10921" s="T901">post</ta>
            <ta e="T903" id="Seg_10922" s="T902">v-v:ptcp</ta>
            <ta e="T904" id="Seg_10923" s="T903">n-n:case</ta>
            <ta e="T905" id="Seg_10924" s="T904">dempro-pro:case</ta>
            <ta e="T906" id="Seg_10925" s="T905">post</ta>
            <ta e="T907" id="Seg_10926" s="T906">que-pro:case</ta>
            <ta e="T908" id="Seg_10927" s="T907">ptcl</ta>
            <ta e="T909" id="Seg_10928" s="T908">adv</ta>
            <ta e="T910" id="Seg_10929" s="T909">n-n:poss-n:case</ta>
            <ta e="T911" id="Seg_10930" s="T910">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T912" id="Seg_10931" s="T911">v-v:(neg)-v:pred.pn</ta>
            <ta e="T913" id="Seg_10932" s="T912">pers-pro:case</ta>
            <ta e="T914" id="Seg_10933" s="T913">n-n:(poss)-n:case</ta>
            <ta e="T915" id="Seg_10934" s="T914">n-n:(poss)-n:case</ta>
            <ta e="T916" id="Seg_10935" s="T915">adj-n:case</ta>
            <ta e="T917" id="Seg_10936" s="T916">ptcl</ta>
            <ta e="T918" id="Seg_10937" s="T917">n-n:case</ta>
            <ta e="T919" id="Seg_10938" s="T918">v-v:ptcp-v:(poss)</ta>
            <ta e="T920" id="Seg_10939" s="T919">ptcl</ta>
            <ta e="T921" id="Seg_10940" s="T920">v-v:tense-v:poss.pn</ta>
            <ta e="T922" id="Seg_10941" s="T921">pers-pro:case</ta>
            <ta e="T923" id="Seg_10942" s="T922">n-n:(poss)-n:case</ta>
            <ta e="T924" id="Seg_10943" s="T923">adv</ta>
            <ta e="T925" id="Seg_10944" s="T924">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T926" id="Seg_10945" s="T925">n-n:(poss)-n:case</ta>
            <ta e="T927" id="Seg_10946" s="T926">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T928" id="Seg_10947" s="T927">adj</ta>
            <ta e="T929" id="Seg_10948" s="T928">adj</ta>
            <ta e="T930" id="Seg_10949" s="T929">propr-n:case</ta>
            <ta e="T931" id="Seg_10950" s="T930">v-v:ptcp</ta>
            <ta e="T932" id="Seg_10951" s="T931">n</ta>
            <ta e="T933" id="Seg_10952" s="T932">n-n&gt;adj</ta>
            <ta e="T934" id="Seg_10953" s="T933">n-n:poss-n:case</ta>
            <ta e="T935" id="Seg_10954" s="T934">v-v:tense-v:pred.pn</ta>
            <ta e="T936" id="Seg_10955" s="T935">pers-pro:case</ta>
            <ta e="T937" id="Seg_10956" s="T936">adj-n:(poss)-n:case</ta>
            <ta e="T938" id="Seg_10957" s="T937">v-v:tense-v:pred.pn</ta>
            <ta e="T939" id="Seg_10958" s="T938">adv</ta>
            <ta e="T940" id="Seg_10959" s="T939">n-n:case</ta>
            <ta e="T941" id="Seg_10960" s="T940">ptcl</ta>
            <ta e="T942" id="Seg_10961" s="T941">v-v:mood.pn</ta>
            <ta e="T943" id="Seg_10962" s="T942">n-n:case</ta>
            <ta e="T944" id="Seg_10963" s="T943">n-n:poss-n:case</ta>
            <ta e="T945" id="Seg_10964" s="T944">ptcl</ta>
            <ta e="T946" id="Seg_10965" s="T945">v-v:mood.pn</ta>
            <ta e="T947" id="Seg_10966" s="T946">adj</ta>
            <ta e="T948" id="Seg_10967" s="T947">n-n&gt;adj</ta>
            <ta e="T949" id="Seg_10968" s="T948">n-n:case</ta>
            <ta e="T950" id="Seg_10969" s="T949">adj</ta>
            <ta e="T951" id="Seg_10970" s="T950">n-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps">
            <ta e="T1" id="Seg_10971" s="T0">pers</ta>
            <ta e="T2" id="Seg_10972" s="T1">n</ta>
            <ta e="T3" id="Seg_10973" s="T2">v</ta>
            <ta e="T4" id="Seg_10974" s="T3">emphpro</ta>
            <ta e="T5" id="Seg_10975" s="T4">n</ta>
            <ta e="T6" id="Seg_10976" s="T5">cardnum</ta>
            <ta e="T7" id="Seg_10977" s="T6">cardnum</ta>
            <ta e="T8" id="Seg_10978" s="T7">cardnum</ta>
            <ta e="T9" id="Seg_10979" s="T8">cardnum</ta>
            <ta e="T10" id="Seg_10980" s="T9">cardnum</ta>
            <ta e="T11" id="Seg_10981" s="T10">adj</ta>
            <ta e="T12" id="Seg_10982" s="T11">adj</ta>
            <ta e="T13" id="Seg_10983" s="T12">n</ta>
            <ta e="T14" id="Seg_10984" s="T13">n</ta>
            <ta e="T15" id="Seg_10985" s="T14">v</ta>
            <ta e="T16" id="Seg_10986" s="T15">n</ta>
            <ta e="T17" id="Seg_10987" s="T16">adv</ta>
            <ta e="T18" id="Seg_10988" s="T17">v</ta>
            <ta e="T19" id="Seg_10989" s="T18">cardnum</ta>
            <ta e="T20" id="Seg_10990" s="T19">n</ta>
            <ta e="T21" id="Seg_10991" s="T20">post</ta>
            <ta e="T22" id="Seg_10992" s="T21">cardnum</ta>
            <ta e="T23" id="Seg_10993" s="T22">n</ta>
            <ta e="T24" id="Seg_10994" s="T23">n</ta>
            <ta e="T25" id="Seg_10995" s="T24">n</ta>
            <ta e="T26" id="Seg_10996" s="T25">v</ta>
            <ta e="T27" id="Seg_10997" s="T26">aux</ta>
            <ta e="T29" id="Seg_10998" s="T27">propr</ta>
            <ta e="T30" id="Seg_10999" s="T29">v</ta>
            <ta e="T31" id="Seg_11000" s="T30">n</ta>
            <ta e="T32" id="Seg_11001" s="T31">adv</ta>
            <ta e="T33" id="Seg_11002" s="T32">adv</ta>
            <ta e="T34" id="Seg_11003" s="T33">cardnum</ta>
            <ta e="T35" id="Seg_11004" s="T34">cardnum</ta>
            <ta e="T36" id="Seg_11005" s="T35">cardnum</ta>
            <ta e="T37" id="Seg_11006" s="T36">cardnum</ta>
            <ta e="T38" id="Seg_11007" s="T37">cardnum</ta>
            <ta e="T39" id="Seg_11008" s="T38">adj</ta>
            <ta e="T40" id="Seg_11009" s="T39">pers</ta>
            <ta e="T41" id="Seg_11010" s="T40">v</ta>
            <ta e="T42" id="Seg_11011" s="T41">n</ta>
            <ta e="T43" id="Seg_11012" s="T42">post</ta>
            <ta e="T44" id="Seg_11013" s="T43">n</ta>
            <ta e="T45" id="Seg_11014" s="T44">n</ta>
            <ta e="T46" id="Seg_11015" s="T45">v</ta>
            <ta e="T47" id="Seg_11016" s="T46">aux</ta>
            <ta e="T48" id="Seg_11017" s="T47">n</ta>
            <ta e="T49" id="Seg_11018" s="T48">pers</ta>
            <ta e="T50" id="Seg_11019" s="T49">n</ta>
            <ta e="T51" id="Seg_11020" s="T50">v</ta>
            <ta e="T52" id="Seg_11021" s="T51">propr</ta>
            <ta e="T53" id="Seg_11022" s="T52">v</ta>
            <ta e="T54" id="Seg_11023" s="T53">adj</ta>
            <ta e="T55" id="Seg_11024" s="T54">n</ta>
            <ta e="T56" id="Seg_11025" s="T55">propr</ta>
            <ta e="T57" id="Seg_11026" s="T56">n</ta>
            <ta e="T58" id="Seg_11027" s="T57">adv</ta>
            <ta e="T59" id="Seg_11028" s="T58">adj</ta>
            <ta e="T60" id="Seg_11029" s="T59">n</ta>
            <ta e="T61" id="Seg_11030" s="T60">n</ta>
            <ta e="T62" id="Seg_11031" s="T61">adv</ta>
            <ta e="T63" id="Seg_11032" s="T62">quant</ta>
            <ta e="T64" id="Seg_11033" s="T63">n</ta>
            <ta e="T65" id="Seg_11034" s="T64">n</ta>
            <ta e="T66" id="Seg_11035" s="T65">adj</ta>
            <ta e="T67" id="Seg_11036" s="T66">adj</ta>
            <ta e="T68" id="Seg_11037" s="T67">n</ta>
            <ta e="T69" id="Seg_11038" s="T68">v</ta>
            <ta e="T70" id="Seg_11039" s="T69">post</ta>
            <ta e="T71" id="Seg_11040" s="T70">pers</ta>
            <ta e="T72" id="Seg_11041" s="T71">n</ta>
            <ta e="T73" id="Seg_11042" s="T72">adv</ta>
            <ta e="T74" id="Seg_11043" s="T73">adv</ta>
            <ta e="T75" id="Seg_11044" s="T74">post</ta>
            <ta e="T76" id="Seg_11045" s="T75">cardnum</ta>
            <ta e="T77" id="Seg_11046" s="T76">cardnum</ta>
            <ta e="T78" id="Seg_11047" s="T77">adj</ta>
            <ta e="T79" id="Seg_11048" s="T78">n</ta>
            <ta e="T80" id="Seg_11049" s="T79">v</ta>
            <ta e="T81" id="Seg_11050" s="T80">aux</ta>
            <ta e="T82" id="Seg_11051" s="T81">adv</ta>
            <ta e="T83" id="Seg_11052" s="T82">adv</ta>
            <ta e="T84" id="Seg_11053" s="T83">dempro</ta>
            <ta e="T85" id="Seg_11054" s="T84">n</ta>
            <ta e="T86" id="Seg_11055" s="T85">propr</ta>
            <ta e="T87" id="Seg_11056" s="T86">n</ta>
            <ta e="T88" id="Seg_11057" s="T87">v</ta>
            <ta e="T89" id="Seg_11058" s="T88">v</ta>
            <ta e="T90" id="Seg_11059" s="T89">adv</ta>
            <ta e="T91" id="Seg_11060" s="T90">v</ta>
            <ta e="T92" id="Seg_11061" s="T91">propr</ta>
            <ta e="T93" id="Seg_11062" s="T92">n</ta>
            <ta e="T94" id="Seg_11063" s="T93">v</ta>
            <ta e="T95" id="Seg_11064" s="T94">adj</ta>
            <ta e="T96" id="Seg_11065" s="T95">cop</ta>
            <ta e="T97" id="Seg_11066" s="T96">adj</ta>
            <ta e="T98" id="Seg_11067" s="T97">n</ta>
            <ta e="T99" id="Seg_11068" s="T98">n</ta>
            <ta e="T100" id="Seg_11069" s="T99">n</ta>
            <ta e="T101" id="Seg_11070" s="T100">ptcl</ta>
            <ta e="T102" id="Seg_11071" s="T101">cop</ta>
            <ta e="T103" id="Seg_11072" s="T102">dempro</ta>
            <ta e="T104" id="Seg_11073" s="T103">n</ta>
            <ta e="T105" id="Seg_11074" s="T104">postp</ta>
            <ta e="T106" id="Seg_11075" s="T105">v</ta>
            <ta e="T107" id="Seg_11076" s="T106">post</ta>
            <ta e="T108" id="Seg_11077" s="T107">adv</ta>
            <ta e="T109" id="Seg_11078" s="T108">adv</ta>
            <ta e="T110" id="Seg_11079" s="T109">pers</ta>
            <ta e="T111" id="Seg_11080" s="T110">n</ta>
            <ta e="T112" id="Seg_11081" s="T111">n</ta>
            <ta e="T113" id="Seg_11082" s="T112">propr</ta>
            <ta e="T114" id="Seg_11083" s="T113">v</ta>
            <ta e="T115" id="Seg_11084" s="T114">n</ta>
            <ta e="T116" id="Seg_11085" s="T115">v</ta>
            <ta e="T117" id="Seg_11086" s="T116">post</ta>
            <ta e="T118" id="Seg_11087" s="T117">posspr</ta>
            <ta e="T119" id="Seg_11088" s="T118">n</ta>
            <ta e="T120" id="Seg_11089" s="T119">propr</ta>
            <ta e="T121" id="Seg_11090" s="T120">n</ta>
            <ta e="T122" id="Seg_11091" s="T121">que</ta>
            <ta e="T123" id="Seg_11092" s="T122">ptcl</ta>
            <ta e="T124" id="Seg_11093" s="T123">adj</ta>
            <ta e="T125" id="Seg_11094" s="T124">n</ta>
            <ta e="T126" id="Seg_11095" s="T125">n</ta>
            <ta e="T127" id="Seg_11096" s="T126">v</ta>
            <ta e="T128" id="Seg_11097" s="T127">post</ta>
            <ta e="T129" id="Seg_11098" s="T128">cardnum</ta>
            <ta e="T130" id="Seg_11099" s="T129">n</ta>
            <ta e="T131" id="Seg_11100" s="T130">post</ta>
            <ta e="T132" id="Seg_11101" s="T131">adj</ta>
            <ta e="T133" id="Seg_11102" s="T132">n</ta>
            <ta e="T134" id="Seg_11103" s="T133">v</ta>
            <ta e="T135" id="Seg_11104" s="T134">v</ta>
            <ta e="T136" id="Seg_11105" s="T135">dempro</ta>
            <ta e="T137" id="Seg_11106" s="T136">post</ta>
            <ta e="T138" id="Seg_11107" s="T137">pers</ta>
            <ta e="T139" id="Seg_11108" s="T138">v</ta>
            <ta e="T140" id="Seg_11109" s="T139">n</ta>
            <ta e="T141" id="Seg_11110" s="T140">n</ta>
            <ta e="T142" id="Seg_11111" s="T141">v</ta>
            <ta e="T143" id="Seg_11112" s="T142">v</ta>
            <ta e="T144" id="Seg_11113" s="T143">ptcl</ta>
            <ta e="T145" id="Seg_11114" s="T144">v</ta>
            <ta e="T146" id="Seg_11115" s="T145">ptcl</ta>
            <ta e="T147" id="Seg_11116" s="T146">n</ta>
            <ta e="T148" id="Seg_11117" s="T147">que</ta>
            <ta e="T149" id="Seg_11118" s="T148">ptcl</ta>
            <ta e="T150" id="Seg_11119" s="T149">v</ta>
            <ta e="T151" id="Seg_11120" s="T150">v</ta>
            <ta e="T152" id="Seg_11121" s="T151">post</ta>
            <ta e="T153" id="Seg_11122" s="T152">cardnum</ta>
            <ta e="T154" id="Seg_11123" s="T153">n</ta>
            <ta e="T155" id="Seg_11124" s="T154">v</ta>
            <ta e="T156" id="Seg_11125" s="T155">n</ta>
            <ta e="T157" id="Seg_11126" s="T156">quant</ta>
            <ta e="T158" id="Seg_11127" s="T157">cardnum</ta>
            <ta e="T159" id="Seg_11128" s="T158">n</ta>
            <ta e="T160" id="Seg_11129" s="T159">v</ta>
            <ta e="T161" id="Seg_11130" s="T160">v</ta>
            <ta e="T162" id="Seg_11131" s="T161">quant</ta>
            <ta e="T163" id="Seg_11132" s="T162">n</ta>
            <ta e="T164" id="Seg_11133" s="T163">n</ta>
            <ta e="T165" id="Seg_11134" s="T164">cop</ta>
            <ta e="T166" id="Seg_11135" s="T165">quant</ta>
            <ta e="T167" id="Seg_11136" s="T166">n</ta>
            <ta e="T168" id="Seg_11137" s="T167">n</ta>
            <ta e="T169" id="Seg_11138" s="T168">n</ta>
            <ta e="T170" id="Seg_11139" s="T169">n</ta>
            <ta e="T171" id="Seg_11140" s="T170">n</ta>
            <ta e="T172" id="Seg_11141" s="T171">adv</ta>
            <ta e="T173" id="Seg_11142" s="T172">adj</ta>
            <ta e="T174" id="Seg_11143" s="T173">adj</ta>
            <ta e="T175" id="Seg_11144" s="T174">n</ta>
            <ta e="T176" id="Seg_11145" s="T175">n</ta>
            <ta e="T177" id="Seg_11146" s="T176">n</ta>
            <ta e="T178" id="Seg_11147" s="T177">v</ta>
            <ta e="T179" id="Seg_11148" s="T178">que</ta>
            <ta e="T180" id="Seg_11149" s="T179">ptcl</ta>
            <ta e="T181" id="Seg_11150" s="T180">adv</ta>
            <ta e="T182" id="Seg_11151" s="T181">v</ta>
            <ta e="T183" id="Seg_11152" s="T182">aux</ta>
            <ta e="T184" id="Seg_11153" s="T183">adv</ta>
            <ta e="T185" id="Seg_11154" s="T184">que</ta>
            <ta e="T186" id="Seg_11155" s="T185">adj</ta>
            <ta e="T187" id="Seg_11156" s="T186">adj</ta>
            <ta e="T188" id="Seg_11157" s="T187">n</ta>
            <ta e="T189" id="Seg_11158" s="T188">v</ta>
            <ta e="T190" id="Seg_11159" s="T189">dempro</ta>
            <ta e="T191" id="Seg_11160" s="T190">n</ta>
            <ta e="T192" id="Seg_11161" s="T191">n</ta>
            <ta e="T193" id="Seg_11162" s="T192">v</ta>
            <ta e="T194" id="Seg_11163" s="T193">cardnum</ta>
            <ta e="T195" id="Seg_11164" s="T194">dempro</ta>
            <ta e="T196" id="Seg_11165" s="T195">n</ta>
            <ta e="T197" id="Seg_11166" s="T196">cardnum</ta>
            <ta e="T198" id="Seg_11167" s="T197">adj</ta>
            <ta e="T199" id="Seg_11168" s="T198">n</ta>
            <ta e="T200" id="Seg_11169" s="T199">pers</ta>
            <ta e="T201" id="Seg_11170" s="T200">ptcl</ta>
            <ta e="T202" id="Seg_11171" s="T201">n</ta>
            <ta e="T203" id="Seg_11172" s="T202">n</ta>
            <ta e="T204" id="Seg_11173" s="T203">adv</ta>
            <ta e="T205" id="Seg_11174" s="T204">v</ta>
            <ta e="T206" id="Seg_11175" s="T205">v</ta>
            <ta e="T207" id="Seg_11176" s="T206">aux</ta>
            <ta e="T208" id="Seg_11177" s="T207">n</ta>
            <ta e="T209" id="Seg_11178" s="T208">post</ta>
            <ta e="T210" id="Seg_11179" s="T209">n</ta>
            <ta e="T211" id="Seg_11180" s="T210">cardnum</ta>
            <ta e="T212" id="Seg_11181" s="T211">cardnum</ta>
            <ta e="T213" id="Seg_11182" s="T212">post</ta>
            <ta e="T214" id="Seg_11183" s="T213">n</ta>
            <ta e="T215" id="Seg_11184" s="T214">dempro</ta>
            <ta e="T216" id="Seg_11185" s="T215">adj</ta>
            <ta e="T217" id="Seg_11186" s="T216">pers</ta>
            <ta e="T218" id="Seg_11187" s="T217">n</ta>
            <ta e="T219" id="Seg_11188" s="T218">n</ta>
            <ta e="T220" id="Seg_11189" s="T219">post</ta>
            <ta e="T221" id="Seg_11190" s="T220">v</ta>
            <ta e="T222" id="Seg_11191" s="T221">interj</ta>
            <ta e="T223" id="Seg_11192" s="T222">cardnum</ta>
            <ta e="T224" id="Seg_11193" s="T223">n</ta>
            <ta e="T225" id="Seg_11194" s="T224">v</ta>
            <ta e="T226" id="Seg_11195" s="T225">aux</ta>
            <ta e="T227" id="Seg_11196" s="T226">adv</ta>
            <ta e="T228" id="Seg_11197" s="T227">adv</ta>
            <ta e="T229" id="Seg_11198" s="T228">adv</ta>
            <ta e="T230" id="Seg_11199" s="T229">v</ta>
            <ta e="T231" id="Seg_11200" s="T230">aux</ta>
            <ta e="T232" id="Seg_11201" s="T231">que</ta>
            <ta e="T233" id="Seg_11202" s="T232">ptcl</ta>
            <ta e="T234" id="Seg_11203" s="T233">n</ta>
            <ta e="T235" id="Seg_11204" s="T234">post</ta>
            <ta e="T236" id="Seg_11205" s="T235">adj</ta>
            <ta e="T237" id="Seg_11206" s="T236">n</ta>
            <ta e="T238" id="Seg_11207" s="T237">n</ta>
            <ta e="T239" id="Seg_11208" s="T238">n</ta>
            <ta e="T240" id="Seg_11209" s="T239">adj</ta>
            <ta e="T241" id="Seg_11210" s="T240">n</ta>
            <ta e="T242" id="Seg_11211" s="T241">v</ta>
            <ta e="T243" id="Seg_11212" s="T242">post</ta>
            <ta e="T244" id="Seg_11213" s="T243">adj</ta>
            <ta e="T245" id="Seg_11214" s="T244">n</ta>
            <ta e="T246" id="Seg_11215" s="T245">v</ta>
            <ta e="T247" id="Seg_11216" s="T246">post</ta>
            <ta e="T248" id="Seg_11217" s="T247">n</ta>
            <ta e="T249" id="Seg_11218" s="T248">adv</ta>
            <ta e="T250" id="Seg_11219" s="T249">que</ta>
            <ta e="T251" id="Seg_11220" s="T250">ptcl</ta>
            <ta e="T252" id="Seg_11221" s="T251">v</ta>
            <ta e="T253" id="Seg_11222" s="T252">post</ta>
            <ta e="T254" id="Seg_11223" s="T253">adj</ta>
            <ta e="T255" id="Seg_11224" s="T254">n</ta>
            <ta e="T256" id="Seg_11225" s="T255">cop</ta>
            <ta e="T257" id="Seg_11226" s="T256">aux</ta>
            <ta e="T258" id="Seg_11227" s="T257">dempro</ta>
            <ta e="T259" id="Seg_11228" s="T258">n</ta>
            <ta e="T260" id="Seg_11229" s="T259">n</ta>
            <ta e="T261" id="Seg_11230" s="T260">n</ta>
            <ta e="T262" id="Seg_11231" s="T261">v</ta>
            <ta e="T263" id="Seg_11232" s="T262">n</ta>
            <ta e="T264" id="Seg_11233" s="T263">v</ta>
            <ta e="T265" id="Seg_11234" s="T264">v</ta>
            <ta e="T266" id="Seg_11235" s="T265">aux</ta>
            <ta e="T267" id="Seg_11236" s="T266">indfpro</ta>
            <ta e="T268" id="Seg_11237" s="T267">post</ta>
            <ta e="T269" id="Seg_11238" s="T268">n</ta>
            <ta e="T270" id="Seg_11239" s="T269">n</ta>
            <ta e="T271" id="Seg_11240" s="T270">n</ta>
            <ta e="T272" id="Seg_11241" s="T271">n</ta>
            <ta e="T273" id="Seg_11242" s="T272">pro</ta>
            <ta e="T274" id="Seg_11243" s="T273">ptcl</ta>
            <ta e="T275" id="Seg_11244" s="T274">v</ta>
            <ta e="T276" id="Seg_11245" s="T275">post</ta>
            <ta e="T277" id="Seg_11246" s="T276">adj</ta>
            <ta e="T278" id="Seg_11247" s="T277">cop</ta>
            <ta e="T279" id="Seg_11248" s="T278">aux</ta>
            <ta e="T280" id="Seg_11249" s="T279">ordnum</ta>
            <ta e="T281" id="Seg_11250" s="T280">n</ta>
            <ta e="T282" id="Seg_11251" s="T281">pers</ta>
            <ta e="T283" id="Seg_11252" s="T282">post</ta>
            <ta e="T284" id="Seg_11253" s="T283">n</ta>
            <ta e="T285" id="Seg_11254" s="T284">n</ta>
            <ta e="T286" id="Seg_11255" s="T285">adv</ta>
            <ta e="T287" id="Seg_11256" s="T286">adv</ta>
            <ta e="T288" id="Seg_11257" s="T287">v</ta>
            <ta e="T289" id="Seg_11258" s="T288">n</ta>
            <ta e="T290" id="Seg_11259" s="T289">n</ta>
            <ta e="T291" id="Seg_11260" s="T290">adv</ta>
            <ta e="T292" id="Seg_11261" s="T291">quant</ta>
            <ta e="T293" id="Seg_11262" s="T292">n</ta>
            <ta e="T294" id="Seg_11263" s="T293">n</ta>
            <ta e="T295" id="Seg_11264" s="T294">adv</ta>
            <ta e="T296" id="Seg_11265" s="T295">n</ta>
            <ta e="T297" id="Seg_11266" s="T296">v</ta>
            <ta e="T298" id="Seg_11267" s="T297">adj</ta>
            <ta e="T299" id="Seg_11268" s="T298">n</ta>
            <ta e="T300" id="Seg_11269" s="T299">propr</ta>
            <ta e="T301" id="Seg_11270" s="T300">propr</ta>
            <ta e="T302" id="Seg_11271" s="T301">v</ta>
            <ta e="T303" id="Seg_11272" s="T302">adj</ta>
            <ta e="T304" id="Seg_11273" s="T303">adv</ta>
            <ta e="T305" id="Seg_11274" s="T304">adv</ta>
            <ta e="T306" id="Seg_11275" s="T305">que</ta>
            <ta e="T307" id="Seg_11276" s="T306">ptcl</ta>
            <ta e="T308" id="Seg_11277" s="T307">adj</ta>
            <ta e="T309" id="Seg_11278" s="T308">n</ta>
            <ta e="T310" id="Seg_11279" s="T309">dempro</ta>
            <ta e="T311" id="Seg_11280" s="T310">adj</ta>
            <ta e="T312" id="Seg_11281" s="T311">adv</ta>
            <ta e="T313" id="Seg_11282" s="T312">v</ta>
            <ta e="T314" id="Seg_11283" s="T313">n</ta>
            <ta e="T315" id="Seg_11284" s="T314">ptcl</ta>
            <ta e="T316" id="Seg_11285" s="T315">n</ta>
            <ta e="T317" id="Seg_11286" s="T316">adj</ta>
            <ta e="T318" id="Seg_11287" s="T317">n</ta>
            <ta e="T319" id="Seg_11288" s="T318">dempro</ta>
            <ta e="T320" id="Seg_11289" s="T319">adj</ta>
            <ta e="T321" id="Seg_11290" s="T320">adj</ta>
            <ta e="T322" id="Seg_11291" s="T321">n</ta>
            <ta e="T323" id="Seg_11292" s="T322">v</ta>
            <ta e="T324" id="Seg_11293" s="T323">adv</ta>
            <ta e="T325" id="Seg_11294" s="T324">v</ta>
            <ta e="T326" id="Seg_11295" s="T325">adv</ta>
            <ta e="T327" id="Seg_11296" s="T326">cardnum</ta>
            <ta e="T328" id="Seg_11297" s="T327">cardnum</ta>
            <ta e="T329" id="Seg_11298" s="T328">adj</ta>
            <ta e="T330" id="Seg_11299" s="T329">n</ta>
            <ta e="T331" id="Seg_11300" s="T330">adv</ta>
            <ta e="T332" id="Seg_11301" s="T331">v</ta>
            <ta e="T333" id="Seg_11302" s="T332">dempro</ta>
            <ta e="T334" id="Seg_11303" s="T333">adj</ta>
            <ta e="T335" id="Seg_11304" s="T334">n</ta>
            <ta e="T336" id="Seg_11305" s="T335">n</ta>
            <ta e="T337" id="Seg_11306" s="T336">propr</ta>
            <ta e="T338" id="Seg_11307" s="T337">v</ta>
            <ta e="T339" id="Seg_11308" s="T338">n</ta>
            <ta e="T340" id="Seg_11309" s="T339">adv</ta>
            <ta e="T341" id="Seg_11310" s="T340">v</ta>
            <ta e="T342" id="Seg_11311" s="T341">adj</ta>
            <ta e="T343" id="Seg_11312" s="T342">n</ta>
            <ta e="T344" id="Seg_11313" s="T343">adv</ta>
            <ta e="T345" id="Seg_11314" s="T344">pers</ta>
            <ta e="T346" id="Seg_11315" s="T345">v</ta>
            <ta e="T347" id="Seg_11316" s="T346">dempro</ta>
            <ta e="T348" id="Seg_11317" s="T347">n</ta>
            <ta e="T349" id="Seg_11318" s="T348">n</ta>
            <ta e="T350" id="Seg_11319" s="T349">adj</ta>
            <ta e="T351" id="Seg_11320" s="T350">n</ta>
            <ta e="T352" id="Seg_11321" s="T351">n</ta>
            <ta e="T353" id="Seg_11322" s="T352">pers</ta>
            <ta e="T354" id="Seg_11323" s="T353">v</ta>
            <ta e="T355" id="Seg_11324" s="T354">post</ta>
            <ta e="T356" id="Seg_11325" s="T355">adv</ta>
            <ta e="T357" id="Seg_11326" s="T356">n</ta>
            <ta e="T358" id="Seg_11327" s="T357">v</ta>
            <ta e="T359" id="Seg_11328" s="T358">post</ta>
            <ta e="T360" id="Seg_11329" s="T359">n</ta>
            <ta e="T361" id="Seg_11330" s="T360">v</ta>
            <ta e="T362" id="Seg_11331" s="T361">v</ta>
            <ta e="T363" id="Seg_11332" s="T362">aux</ta>
            <ta e="T364" id="Seg_11333" s="T363">pers</ta>
            <ta e="T365" id="Seg_11334" s="T364">n</ta>
            <ta e="T366" id="Seg_11335" s="T365">adv</ta>
            <ta e="T367" id="Seg_11336" s="T366">adv</ta>
            <ta e="T368" id="Seg_11337" s="T367">n</ta>
            <ta e="T369" id="Seg_11338" s="T368">v</ta>
            <ta e="T370" id="Seg_11339" s="T369">post</ta>
            <ta e="T371" id="Seg_11340" s="T370">n</ta>
            <ta e="T372" id="Seg_11341" s="T371">n</ta>
            <ta e="T373" id="Seg_11342" s="T372">post</ta>
            <ta e="T374" id="Seg_11343" s="T373">v</ta>
            <ta e="T375" id="Seg_11344" s="T374">adv</ta>
            <ta e="T376" id="Seg_11345" s="T375">v</ta>
            <ta e="T377" id="Seg_11346" s="T376">pers</ta>
            <ta e="T378" id="Seg_11347" s="T377">adj</ta>
            <ta e="T379" id="Seg_11348" s="T378">n</ta>
            <ta e="T380" id="Seg_11349" s="T379">n</ta>
            <ta e="T381" id="Seg_11350" s="T380">v</ta>
            <ta e="T382" id="Seg_11351" s="T381">dempro</ta>
            <ta e="T383" id="Seg_11352" s="T382">adj</ta>
            <ta e="T384" id="Seg_11353" s="T383">n</ta>
            <ta e="T385" id="Seg_11354" s="T952">post</ta>
            <ta e="T387" id="Seg_11355" s="T385">v</ta>
            <ta e="T388" id="Seg_11356" s="T387">pers</ta>
            <ta e="T389" id="Seg_11357" s="T388">dempro</ta>
            <ta e="T390" id="Seg_11358" s="T389">n</ta>
            <ta e="T391" id="Seg_11359" s="T390">adv</ta>
            <ta e="T392" id="Seg_11360" s="T391">v</ta>
            <ta e="T393" id="Seg_11361" s="T392">adj</ta>
            <ta e="T394" id="Seg_11362" s="T393">n</ta>
            <ta e="T395" id="Seg_11363" s="T394">que</ta>
            <ta e="T396" id="Seg_11364" s="T395">n</ta>
            <ta e="T397" id="Seg_11365" s="T396">n</ta>
            <ta e="T398" id="Seg_11366" s="T397">n</ta>
            <ta e="T399" id="Seg_11367" s="T398">adv</ta>
            <ta e="T400" id="Seg_11368" s="T399">v</ta>
            <ta e="T401" id="Seg_11369" s="T400">quant</ta>
            <ta e="T402" id="Seg_11370" s="T401">cardnum</ta>
            <ta e="T403" id="Seg_11371" s="T402">n</ta>
            <ta e="T404" id="Seg_11372" s="T403">n</ta>
            <ta e="T405" id="Seg_11373" s="T404">v</ta>
            <ta e="T406" id="Seg_11374" s="T405">quant</ta>
            <ta e="T407" id="Seg_11375" s="T406">n</ta>
            <ta e="T408" id="Seg_11376" s="T407">cop</ta>
            <ta e="T409" id="Seg_11377" s="T408">n</ta>
            <ta e="T410" id="Seg_11378" s="T409">adv</ta>
            <ta e="T411" id="Seg_11379" s="T410">v</ta>
            <ta e="T412" id="Seg_11380" s="T411">pers</ta>
            <ta e="T413" id="Seg_11381" s="T412">ptcl</ta>
            <ta e="T414" id="Seg_11382" s="T413">dempro</ta>
            <ta e="T415" id="Seg_11383" s="T414">n</ta>
            <ta e="T416" id="Seg_11384" s="T415">propr</ta>
            <ta e="T417" id="Seg_11385" s="T416">n</ta>
            <ta e="T418" id="Seg_11386" s="T417">n</ta>
            <ta e="T419" id="Seg_11387" s="T418">n</ta>
            <ta e="T420" id="Seg_11388" s="T419">cop</ta>
            <ta e="T421" id="Seg_11389" s="T420">aux</ta>
            <ta e="T422" id="Seg_11390" s="T421">adv</ta>
            <ta e="T423" id="Seg_11391" s="T422">adj</ta>
            <ta e="T424" id="Seg_11392" s="T423">n</ta>
            <ta e="T425" id="Seg_11393" s="T424">n</ta>
            <ta e="T426" id="Seg_11394" s="T425">v</ta>
            <ta e="T427" id="Seg_11395" s="T426">dempro</ta>
            <ta e="T428" id="Seg_11396" s="T427">cop</ta>
            <ta e="T429" id="Seg_11397" s="T428">post</ta>
            <ta e="T430" id="Seg_11398" s="T429">v</ta>
            <ta e="T431" id="Seg_11399" s="T430">n</ta>
            <ta e="T432" id="Seg_11400" s="T431">v</ta>
            <ta e="T433" id="Seg_11401" s="T432">dempro</ta>
            <ta e="T434" id="Seg_11402" s="T433">adj</ta>
            <ta e="T435" id="Seg_11403" s="T434">n</ta>
            <ta e="T436" id="Seg_11404" s="T435">pers</ta>
            <ta e="T437" id="Seg_11405" s="T436">ptcl</ta>
            <ta e="T438" id="Seg_11406" s="T437">ptcl</ta>
            <ta e="T439" id="Seg_11407" s="T438">n</ta>
            <ta e="T440" id="Seg_11408" s="T439">cardnum</ta>
            <ta e="T441" id="Seg_11409" s="T440">postp</ta>
            <ta e="T442" id="Seg_11410" s="T441">n</ta>
            <ta e="T443" id="Seg_11411" s="T442">n</ta>
            <ta e="T444" id="Seg_11412" s="T443">dempro</ta>
            <ta e="T445" id="Seg_11413" s="T444">cardnum</ta>
            <ta e="T446" id="Seg_11414" s="T445">n</ta>
            <ta e="T447" id="Seg_11415" s="T446">n</ta>
            <ta e="T448" id="Seg_11416" s="T447">v</ta>
            <ta e="T449" id="Seg_11417" s="T448">cardnum</ta>
            <ta e="T450" id="Seg_11418" s="T449">cardnum</ta>
            <ta e="T451" id="Seg_11419" s="T450">ptcl</ta>
            <ta e="T452" id="Seg_11420" s="T451">n</ta>
            <ta e="T453" id="Seg_11421" s="T452">dempro</ta>
            <ta e="T454" id="Seg_11422" s="T453">n</ta>
            <ta e="T455" id="Seg_11423" s="T454">adv</ta>
            <ta e="T456" id="Seg_11424" s="T455">cardnum</ta>
            <ta e="T457" id="Seg_11425" s="T456">pers</ta>
            <ta e="T458" id="Seg_11426" s="T457">n</ta>
            <ta e="T459" id="Seg_11427" s="T458">cop</ta>
            <ta e="T460" id="Seg_11428" s="T459">n</ta>
            <ta e="T461" id="Seg_11429" s="T460">ptcl</ta>
            <ta e="T462" id="Seg_11430" s="T461">cop</ta>
            <ta e="T463" id="Seg_11431" s="T462">dempro</ta>
            <ta e="T464" id="Seg_11432" s="T463">adj</ta>
            <ta e="T465" id="Seg_11433" s="T464">cop</ta>
            <ta e="T466" id="Seg_11434" s="T465">v</ta>
            <ta e="T467" id="Seg_11435" s="T466">que</ta>
            <ta e="T468" id="Seg_11436" s="T467">adj</ta>
            <ta e="T469" id="Seg_11437" s="T468">n</ta>
            <ta e="T470" id="Seg_11438" s="T469">cop</ta>
            <ta e="T471" id="Seg_11439" s="T470">propr</ta>
            <ta e="T472" id="Seg_11440" s="T471">n</ta>
            <ta e="T473" id="Seg_11441" s="T472">que</ta>
            <ta e="T474" id="Seg_11442" s="T473">n</ta>
            <ta e="T475" id="Seg_11443" s="T474">v</ta>
            <ta e="T476" id="Seg_11444" s="T475">que</ta>
            <ta e="T477" id="Seg_11445" s="T476">n</ta>
            <ta e="T478" id="Seg_11446" s="T477">v</ta>
            <ta e="T479" id="Seg_11447" s="T478">propr</ta>
            <ta e="T480" id="Seg_11448" s="T479">v</ta>
            <ta e="T481" id="Seg_11449" s="T480">post</ta>
            <ta e="T482" id="Seg_11450" s="T481">pers</ta>
            <ta e="T483" id="Seg_11451" s="T482">n</ta>
            <ta e="T484" id="Seg_11452" s="T483">adv</ta>
            <ta e="T485" id="Seg_11453" s="T484">v</ta>
            <ta e="T486" id="Seg_11454" s="T485">n</ta>
            <ta e="T487" id="Seg_11455" s="T486">n</ta>
            <ta e="T488" id="Seg_11456" s="T487">post</ta>
            <ta e="T489" id="Seg_11457" s="T488">v</ta>
            <ta e="T490" id="Seg_11458" s="T489">aux</ta>
            <ta e="T491" id="Seg_11459" s="T490">dempro</ta>
            <ta e="T492" id="Seg_11460" s="T491">propr</ta>
            <ta e="T493" id="Seg_11461" s="T492">n</ta>
            <ta e="T494" id="Seg_11462" s="T493">pers</ta>
            <ta e="T495" id="Seg_11463" s="T494">ptcl</ta>
            <ta e="T496" id="Seg_11464" s="T495">n</ta>
            <ta e="T497" id="Seg_11465" s="T496">n</ta>
            <ta e="T498" id="Seg_11466" s="T497">n</ta>
            <ta e="T499" id="Seg_11467" s="T498">ptcl</ta>
            <ta e="T500" id="Seg_11468" s="T499">cop</ta>
            <ta e="T501" id="Seg_11469" s="T500">cardnum</ta>
            <ta e="T502" id="Seg_11470" s="T501">cardnum</ta>
            <ta e="T503" id="Seg_11471" s="T502">cardnum</ta>
            <ta e="T504" id="Seg_11472" s="T503">cardnum</ta>
            <ta e="T505" id="Seg_11473" s="T504">cardnum</ta>
            <ta e="T506" id="Seg_11474" s="T505">adj</ta>
            <ta e="T507" id="Seg_11475" s="T506">adj</ta>
            <ta e="T508" id="Seg_11476" s="T507">n</ta>
            <ta e="T509" id="Seg_11477" s="T508">n</ta>
            <ta e="T510" id="Seg_11478" s="T509">post</ta>
            <ta e="T511" id="Seg_11479" s="T510">dempro</ta>
            <ta e="T512" id="Seg_11480" s="T511">n</ta>
            <ta e="T513" id="Seg_11481" s="T512">adj</ta>
            <ta e="T514" id="Seg_11482" s="T513">n</ta>
            <ta e="T515" id="Seg_11483" s="T514">post</ta>
            <ta e="T516" id="Seg_11484" s="T515">adv</ta>
            <ta e="T517" id="Seg_11485" s="T516">v</ta>
            <ta e="T518" id="Seg_11486" s="T517">v</ta>
            <ta e="T519" id="Seg_11487" s="T518">dempro</ta>
            <ta e="T520" id="Seg_11488" s="T519">n</ta>
            <ta e="T521" id="Seg_11489" s="T520">propr</ta>
            <ta e="T522" id="Seg_11490" s="T521">adv</ta>
            <ta e="T523" id="Seg_11491" s="T522">propr</ta>
            <ta e="T524" id="Seg_11492" s="T523">adv</ta>
            <ta e="T525" id="Seg_11493" s="T524">propr</ta>
            <ta e="T526" id="Seg_11494" s="T525">adv</ta>
            <ta e="T527" id="Seg_11495" s="T526">propr</ta>
            <ta e="T528" id="Seg_11496" s="T527">adv</ta>
            <ta e="T529" id="Seg_11497" s="T528">propr</ta>
            <ta e="T530" id="Seg_11498" s="T529">v</ta>
            <ta e="T531" id="Seg_11499" s="T530">adj</ta>
            <ta e="T532" id="Seg_11500" s="T531">adj</ta>
            <ta e="T533" id="Seg_11501" s="T532">n</ta>
            <ta e="T534" id="Seg_11502" s="T533">v</ta>
            <ta e="T535" id="Seg_11503" s="T534">adj</ta>
            <ta e="T536" id="Seg_11504" s="T535">v</ta>
            <ta e="T537" id="Seg_11505" s="T536">n</ta>
            <ta e="T538" id="Seg_11506" s="T537">n</ta>
            <ta e="T539" id="Seg_11507" s="T538">dempro</ta>
            <ta e="T540" id="Seg_11508" s="T539">n</ta>
            <ta e="T541" id="Seg_11509" s="T540">adv</ta>
            <ta e="T542" id="Seg_11510" s="T541">adv</ta>
            <ta e="T543" id="Seg_11511" s="T542">pers</ta>
            <ta e="T544" id="Seg_11512" s="T543">v</ta>
            <ta e="T545" id="Seg_11513" s="T544">n</ta>
            <ta e="T546" id="Seg_11514" s="T545">post</ta>
            <ta e="T547" id="Seg_11515" s="T546">propr</ta>
            <ta e="T548" id="Seg_11516" s="T547">n</ta>
            <ta e="T549" id="Seg_11517" s="T548">n</ta>
            <ta e="T550" id="Seg_11518" s="T549">dempro</ta>
            <ta e="T551" id="Seg_11519" s="T550">n</ta>
            <ta e="T552" id="Seg_11520" s="T551">cardnum</ta>
            <ta e="T553" id="Seg_11521" s="T552">post</ta>
            <ta e="T554" id="Seg_11522" s="T553">adv</ta>
            <ta e="T555" id="Seg_11523" s="T554">n</ta>
            <ta e="T556" id="Seg_11524" s="T555">n</ta>
            <ta e="T557" id="Seg_11525" s="T556">v</ta>
            <ta e="T558" id="Seg_11526" s="T557">aux</ta>
            <ta e="T559" id="Seg_11527" s="T558">adv</ta>
            <ta e="T560" id="Seg_11528" s="T559">ordnum</ta>
            <ta e="T561" id="Seg_11529" s="T560">n</ta>
            <ta e="T562" id="Seg_11530" s="T561">pers</ta>
            <ta e="T563" id="Seg_11531" s="T562">n</ta>
            <ta e="T564" id="Seg_11532" s="T563">v</ta>
            <ta e="T565" id="Seg_11533" s="T564">adv</ta>
            <ta e="T566" id="Seg_11534" s="T565">adj</ta>
            <ta e="T567" id="Seg_11535" s="T566">n</ta>
            <ta e="T568" id="Seg_11536" s="T567">adv</ta>
            <ta e="T569" id="Seg_11537" s="T568">ptcl</ta>
            <ta e="T570" id="Seg_11538" s="T569">cop</ta>
            <ta e="T571" id="Seg_11539" s="T570">propr</ta>
            <ta e="T572" id="Seg_11540" s="T571">v</ta>
            <ta e="T573" id="Seg_11541" s="T572">adj</ta>
            <ta e="T574" id="Seg_11542" s="T573">n</ta>
            <ta e="T575" id="Seg_11543" s="T574">n</ta>
            <ta e="T576" id="Seg_11544" s="T575">dempro</ta>
            <ta e="T577" id="Seg_11545" s="T576">n</ta>
            <ta e="T578" id="Seg_11546" s="T577">adj</ta>
            <ta e="T579" id="Seg_11547" s="T578">n</ta>
            <ta e="T580" id="Seg_11548" s="T579">v</ta>
            <ta e="T581" id="Seg_11549" s="T580">n</ta>
            <ta e="T582" id="Seg_11550" s="T581">n</ta>
            <ta e="T583" id="Seg_11551" s="T582">v</ta>
            <ta e="T584" id="Seg_11552" s="T583">aux</ta>
            <ta e="T585" id="Seg_11553" s="T584">pers</ta>
            <ta e="T586" id="Seg_11554" s="T585">adv</ta>
            <ta e="T587" id="Seg_11555" s="T586">v</ta>
            <ta e="T588" id="Seg_11556" s="T587">v</ta>
            <ta e="T589" id="Seg_11557" s="T588">aux</ta>
            <ta e="T590" id="Seg_11558" s="T589">dempro</ta>
            <ta e="T591" id="Seg_11559" s="T590">post</ta>
            <ta e="T592" id="Seg_11560" s="T591">pers</ta>
            <ta e="T593" id="Seg_11561" s="T592">que</ta>
            <ta e="T594" id="Seg_11562" s="T593">ptcl</ta>
            <ta e="T595" id="Seg_11563" s="T594">n</ta>
            <ta e="T596" id="Seg_11564" s="T595">v</ta>
            <ta e="T597" id="Seg_11565" s="T596">v</ta>
            <ta e="T598" id="Seg_11566" s="T597">adj</ta>
            <ta e="T599" id="Seg_11567" s="T598">n</ta>
            <ta e="T600" id="Seg_11568" s="T599">v</ta>
            <ta e="T601" id="Seg_11569" s="T600">n</ta>
            <ta e="T602" id="Seg_11570" s="T601">v</ta>
            <ta e="T603" id="Seg_11571" s="T602">n</ta>
            <ta e="T604" id="Seg_11572" s="T603">adv</ta>
            <ta e="T605" id="Seg_11573" s="T604">v</ta>
            <ta e="T606" id="Seg_11574" s="T605">n</ta>
            <ta e="T607" id="Seg_11575" s="T606">n</ta>
            <ta e="T608" id="Seg_11576" s="T607">v</ta>
            <ta e="T609" id="Seg_11577" s="T608">v</ta>
            <ta e="T610" id="Seg_11578" s="T609">que</ta>
            <ta e="T611" id="Seg_11579" s="T610">ptcl</ta>
            <ta e="T612" id="Seg_11580" s="T611">n</ta>
            <ta e="T613" id="Seg_11581" s="T612">v</ta>
            <ta e="T614" id="Seg_11582" s="T613">adv</ta>
            <ta e="T615" id="Seg_11583" s="T614">v</ta>
            <ta e="T616" id="Seg_11584" s="T615">aux</ta>
            <ta e="T617" id="Seg_11585" s="T616">dempro</ta>
            <ta e="T618" id="Seg_11586" s="T617">post</ta>
            <ta e="T619" id="Seg_11587" s="T618">adv</ta>
            <ta e="T620" id="Seg_11588" s="T619">pers</ta>
            <ta e="T621" id="Seg_11589" s="T620">adv</ta>
            <ta e="T622" id="Seg_11590" s="T621">quant</ta>
            <ta e="T623" id="Seg_11591" s="T622">n</ta>
            <ta e="T624" id="Seg_11592" s="T623">n</ta>
            <ta e="T625" id="Seg_11593" s="T624">v</ta>
            <ta e="T626" id="Seg_11594" s="T625">aux</ta>
            <ta e="T627" id="Seg_11595" s="T626">n</ta>
            <ta e="T628" id="Seg_11596" s="T627">n</ta>
            <ta e="T629" id="Seg_11597" s="T628">adv</ta>
            <ta e="T630" id="Seg_11598" s="T629">conj</ta>
            <ta e="T631" id="Seg_11599" s="T630">v</ta>
            <ta e="T632" id="Seg_11600" s="T631">n</ta>
            <ta e="T633" id="Seg_11601" s="T632">v</ta>
            <ta e="T634" id="Seg_11602" s="T633">aux</ta>
            <ta e="T635" id="Seg_11603" s="T634">adv</ta>
            <ta e="T636" id="Seg_11604" s="T635">pers</ta>
            <ta e="T637" id="Seg_11605" s="T636">n</ta>
            <ta e="T638" id="Seg_11606" s="T637">adv</ta>
            <ta e="T639" id="Seg_11607" s="T638">v</ta>
            <ta e="T640" id="Seg_11608" s="T639">post</ta>
            <ta e="T641" id="Seg_11609" s="T640">adj</ta>
            <ta e="T642" id="Seg_11610" s="T641">n</ta>
            <ta e="T643" id="Seg_11611" s="T642">n</ta>
            <ta e="T644" id="Seg_11612" s="T643">adv</ta>
            <ta e="T645" id="Seg_11613" s="T644">v</ta>
            <ta e="T646" id="Seg_11614" s="T645">post</ta>
            <ta e="T647" id="Seg_11615" s="T646">que</ta>
            <ta e="T648" id="Seg_11616" s="T647">ptcl</ta>
            <ta e="T649" id="Seg_11617" s="T648">distrnum</ta>
            <ta e="T650" id="Seg_11618" s="T649">n</ta>
            <ta e="T651" id="Seg_11619" s="T650">adj</ta>
            <ta e="T652" id="Seg_11620" s="T651">n</ta>
            <ta e="T653" id="Seg_11621" s="T652">v</ta>
            <ta e="T654" id="Seg_11622" s="T653">post</ta>
            <ta e="T655" id="Seg_11623" s="T654">dempro</ta>
            <ta e="T656" id="Seg_11624" s="T655">adj</ta>
            <ta e="T657" id="Seg_11625" s="T656">v</ta>
            <ta e="T658" id="Seg_11626" s="T657">aux</ta>
            <ta e="T659" id="Seg_11627" s="T658">n</ta>
            <ta e="T660" id="Seg_11628" s="T659">post</ta>
            <ta e="T661" id="Seg_11629" s="T660">v</ta>
            <ta e="T662" id="Seg_11630" s="T661">v</ta>
            <ta e="T663" id="Seg_11631" s="T662">v</ta>
            <ta e="T664" id="Seg_11632" s="T663">pers</ta>
            <ta e="T665" id="Seg_11633" s="T664">n</ta>
            <ta e="T666" id="Seg_11634" s="T665">dempro</ta>
            <ta e="T667" id="Seg_11635" s="T666">n</ta>
            <ta e="T668" id="Seg_11636" s="T667">n</ta>
            <ta e="T669" id="Seg_11637" s="T668">n</ta>
            <ta e="T670" id="Seg_11638" s="T669">v</ta>
            <ta e="T671" id="Seg_11639" s="T670">adj</ta>
            <ta e="T672" id="Seg_11640" s="T671">n</ta>
            <ta e="T673" id="Seg_11641" s="T672">n</ta>
            <ta e="T674" id="Seg_11642" s="T673">indfpro</ta>
            <ta e="T675" id="Seg_11643" s="T674">distrnum</ta>
            <ta e="T676" id="Seg_11644" s="T675">ptcl</ta>
            <ta e="T677" id="Seg_11645" s="T676">n</ta>
            <ta e="T678" id="Seg_11646" s="T677">adv</ta>
            <ta e="T679" id="Seg_11647" s="T678">v</ta>
            <ta e="T680" id="Seg_11648" s="T679">v</ta>
            <ta e="T681" id="Seg_11649" s="T680">post</ta>
            <ta e="T682" id="Seg_11650" s="T681">adj</ta>
            <ta e="T683" id="Seg_11651" s="T682">adv</ta>
            <ta e="T684" id="Seg_11652" s="T683">v</ta>
            <ta e="T685" id="Seg_11653" s="T684">v</ta>
            <ta e="T686" id="Seg_11654" s="T685">propr</ta>
            <ta e="T687" id="Seg_11655" s="T686">v</ta>
            <ta e="T688" id="Seg_11656" s="T687">n</ta>
            <ta e="T689" id="Seg_11657" s="T688">adv</ta>
            <ta e="T690" id="Seg_11658" s="T689">n</ta>
            <ta e="T691" id="Seg_11659" s="T690">adj</ta>
            <ta e="T692" id="Seg_11660" s="T691">n</ta>
            <ta e="T693" id="Seg_11661" s="T692">n</ta>
            <ta e="T694" id="Seg_11662" s="T693">n</ta>
            <ta e="T695" id="Seg_11663" s="T694">que</ta>
            <ta e="T696" id="Seg_11664" s="T695">ptcl</ta>
            <ta e="T697" id="Seg_11665" s="T696">adj</ta>
            <ta e="T698" id="Seg_11666" s="T697">n</ta>
            <ta e="T699" id="Seg_11667" s="T698">n</ta>
            <ta e="T700" id="Seg_11668" s="T699">n</ta>
            <ta e="T701" id="Seg_11669" s="T700">adj</ta>
            <ta e="T702" id="Seg_11670" s="T701">cop</ta>
            <ta e="T703" id="Seg_11671" s="T702">n</ta>
            <ta e="T704" id="Seg_11672" s="T703">post</ta>
            <ta e="T705" id="Seg_11673" s="T704">n</ta>
            <ta e="T706" id="Seg_11674" s="T705">dempro</ta>
            <ta e="T707" id="Seg_11675" s="T706">v</ta>
            <ta e="T708" id="Seg_11676" s="T707">n</ta>
            <ta e="T709" id="Seg_11677" s="T708">v</ta>
            <ta e="T710" id="Seg_11678" s="T709">v</ta>
            <ta e="T711" id="Seg_11679" s="T710">adv</ta>
            <ta e="T712" id="Seg_11680" s="T711">n</ta>
            <ta e="T713" id="Seg_11681" s="T712">n</ta>
            <ta e="T714" id="Seg_11682" s="T713">v</ta>
            <ta e="T715" id="Seg_11683" s="T714">aux</ta>
            <ta e="T716" id="Seg_11684" s="T715">n</ta>
            <ta e="T717" id="Seg_11685" s="T716">v</ta>
            <ta e="T718" id="Seg_11686" s="T717">n</ta>
            <ta e="T719" id="Seg_11687" s="T718">post</ta>
            <ta e="T720" id="Seg_11688" s="T719">n</ta>
            <ta e="T721" id="Seg_11689" s="T720">adv</ta>
            <ta e="T722" id="Seg_11690" s="T721">n</ta>
            <ta e="T723" id="Seg_11691" s="T722">que</ta>
            <ta e="T724" id="Seg_11692" s="T723">ptcl</ta>
            <ta e="T725" id="Seg_11693" s="T724">v</ta>
            <ta e="T726" id="Seg_11694" s="T725">aux</ta>
            <ta e="T727" id="Seg_11695" s="T726">aux</ta>
            <ta e="T728" id="Seg_11696" s="T727">n</ta>
            <ta e="T729" id="Seg_11697" s="T728">v</ta>
            <ta e="T730" id="Seg_11698" s="T729">adj</ta>
            <ta e="T731" id="Seg_11699" s="T730">n</ta>
            <ta e="T732" id="Seg_11700" s="T731">n</ta>
            <ta e="T733" id="Seg_11701" s="T732">v</ta>
            <ta e="T734" id="Seg_11702" s="T733">post</ta>
            <ta e="T735" id="Seg_11703" s="T734">cop</ta>
            <ta e="T736" id="Seg_11704" s="T735">dempro</ta>
            <ta e="T737" id="Seg_11705" s="T736">n</ta>
            <ta e="T738" id="Seg_11706" s="T737">n</ta>
            <ta e="T739" id="Seg_11707" s="T738">quant</ta>
            <ta e="T740" id="Seg_11708" s="T739">n</ta>
            <ta e="T741" id="Seg_11709" s="T740">v</ta>
            <ta e="T742" id="Seg_11710" s="T741">n</ta>
            <ta e="T743" id="Seg_11711" s="T742">n</ta>
            <ta e="T744" id="Seg_11712" s="T743">v</ta>
            <ta e="T745" id="Seg_11713" s="T744">que</ta>
            <ta e="T746" id="Seg_11714" s="T745">ptcl</ta>
            <ta e="T747" id="Seg_11715" s="T746">v</ta>
            <ta e="T748" id="Seg_11716" s="T747">adv</ta>
            <ta e="T749" id="Seg_11717" s="T748">aux</ta>
            <ta e="T750" id="Seg_11718" s="T749">n</ta>
            <ta e="T751" id="Seg_11719" s="T750">v</ta>
            <ta e="T752" id="Seg_11720" s="T751">post</ta>
            <ta e="T753" id="Seg_11721" s="T752">posspr</ta>
            <ta e="T754" id="Seg_11722" s="T753">ptcl</ta>
            <ta e="T755" id="Seg_11723" s="T754">n</ta>
            <ta e="T756" id="Seg_11724" s="T755">n</ta>
            <ta e="T757" id="Seg_11725" s="T756">v</ta>
            <ta e="T758" id="Seg_11726" s="T757">adv</ta>
            <ta e="T759" id="Seg_11727" s="T758">v</ta>
            <ta e="T760" id="Seg_11728" s="T759">adv</ta>
            <ta e="T761" id="Seg_11729" s="T760">pers</ta>
            <ta e="T762" id="Seg_11730" s="T761">que</ta>
            <ta e="T763" id="Seg_11731" s="T762">ptcl</ta>
            <ta e="T764" id="Seg_11732" s="T763">n</ta>
            <ta e="T765" id="Seg_11733" s="T764">v</ta>
            <ta e="T766" id="Seg_11734" s="T765">adj</ta>
            <ta e="T767" id="Seg_11735" s="T766">n</ta>
            <ta e="T768" id="Seg_11736" s="T767">n</ta>
            <ta e="T769" id="Seg_11737" s="T768">n</ta>
            <ta e="T770" id="Seg_11738" s="T769">cardnum</ta>
            <ta e="T771" id="Seg_11739" s="T770">cardnum</ta>
            <ta e="T772" id="Seg_11740" s="T771">cardnum</ta>
            <ta e="T773" id="Seg_11741" s="T772">cardnum</ta>
            <ta e="T774" id="Seg_11742" s="T773">cardnum</ta>
            <ta e="T775" id="Seg_11743" s="T774">adj</ta>
            <ta e="T776" id="Seg_11744" s="T775">n</ta>
            <ta e="T777" id="Seg_11745" s="T776">v</ta>
            <ta e="T778" id="Seg_11746" s="T777">adv</ta>
            <ta e="T779" id="Seg_11747" s="T778">adv</ta>
            <ta e="T780" id="Seg_11748" s="T779">adj</ta>
            <ta e="T781" id="Seg_11749" s="T780">post</ta>
            <ta e="T782" id="Seg_11750" s="T781">n</ta>
            <ta e="T783" id="Seg_11751" s="T782">n</ta>
            <ta e="T784" id="Seg_11752" s="T783">adv</ta>
            <ta e="T785" id="Seg_11753" s="T784">v</ta>
            <ta e="T786" id="Seg_11754" s="T785">dempro</ta>
            <ta e="T787" id="Seg_11755" s="T786">v</ta>
            <ta e="T788" id="Seg_11756" s="T787">v</ta>
            <ta e="T789" id="Seg_11757" s="T788">post</ta>
            <ta e="T790" id="Seg_11758" s="T789">cardnum</ta>
            <ta e="T791" id="Seg_11759" s="T790">n</ta>
            <ta e="T792" id="Seg_11760" s="T791">post</ta>
            <ta e="T793" id="Seg_11761" s="T792">v</ta>
            <ta e="T794" id="Seg_11762" s="T793">propr</ta>
            <ta e="T795" id="Seg_11763" s="T794">v</ta>
            <ta e="T796" id="Seg_11764" s="T795">n</ta>
            <ta e="T797" id="Seg_11765" s="T796">propr</ta>
            <ta e="T798" id="Seg_11766" s="T797">v</ta>
            <ta e="T799" id="Seg_11767" s="T798">adj</ta>
            <ta e="T800" id="Seg_11768" s="T799">n</ta>
            <ta e="T801" id="Seg_11769" s="T800">adv</ta>
            <ta e="T802" id="Seg_11770" s="T801">v</ta>
            <ta e="T803" id="Seg_11771" s="T802">post</ta>
            <ta e="T804" id="Seg_11772" s="T803">n</ta>
            <ta e="T805" id="Seg_11773" s="T804">adj</ta>
            <ta e="T806" id="Seg_11774" s="T805">v</ta>
            <ta e="T807" id="Seg_11775" s="T806">aux</ta>
            <ta e="T808" id="Seg_11776" s="T807">n</ta>
            <ta e="T809" id="Seg_11777" s="T808">adv</ta>
            <ta e="T810" id="Seg_11778" s="T809">v</ta>
            <ta e="T811" id="Seg_11779" s="T810">n</ta>
            <ta e="T813" id="Seg_11780" s="T812">v</ta>
            <ta e="T814" id="Seg_11781" s="T813">adv</ta>
            <ta e="T815" id="Seg_11782" s="T814">v</ta>
            <ta e="T816" id="Seg_11783" s="T815">n</ta>
            <ta e="T817" id="Seg_11784" s="T816">cardnum</ta>
            <ta e="T818" id="Seg_11785" s="T817">n</ta>
            <ta e="T819" id="Seg_11786" s="T818">n</ta>
            <ta e="T820" id="Seg_11787" s="T819">post</ta>
            <ta e="T821" id="Seg_11788" s="T820">n</ta>
            <ta e="T822" id="Seg_11789" s="T821">n</ta>
            <ta e="T823" id="Seg_11790" s="T822">v</ta>
            <ta e="T824" id="Seg_11791" s="T823">adv</ta>
            <ta e="T825" id="Seg_11792" s="T824">cardnum</ta>
            <ta e="T826" id="Seg_11793" s="T825">n</ta>
            <ta e="T827" id="Seg_11794" s="T826">v</ta>
            <ta e="T828" id="Seg_11795" s="T827">dempro</ta>
            <ta e="T829" id="Seg_11796" s="T828">n</ta>
            <ta e="T830" id="Seg_11797" s="T829">adv</ta>
            <ta e="T831" id="Seg_11798" s="T830">v</ta>
            <ta e="T832" id="Seg_11799" s="T831">v</ta>
            <ta e="T833" id="Seg_11800" s="T832">adv</ta>
            <ta e="T834" id="Seg_11801" s="T833">v</ta>
            <ta e="T835" id="Seg_11802" s="T834">dempro</ta>
            <ta e="T836" id="Seg_11803" s="T835">pers</ta>
            <ta e="T837" id="Seg_11804" s="T836">n</ta>
            <ta e="T838" id="Seg_11805" s="T837">n</ta>
            <ta e="T839" id="Seg_11806" s="T838">n</ta>
            <ta e="T840" id="Seg_11807" s="T839">que</ta>
            <ta e="T841" id="Seg_11808" s="T840">ptcl</ta>
            <ta e="T842" id="Seg_11809" s="T841">v</ta>
            <ta e="T843" id="Seg_11810" s="T842">n</ta>
            <ta e="T844" id="Seg_11811" s="T843">conj</ta>
            <ta e="T845" id="Seg_11812" s="T844">dempro</ta>
            <ta e="T846" id="Seg_11813" s="T845">adj</ta>
            <ta e="T847" id="Seg_11814" s="T846">n</ta>
            <ta e="T848" id="Seg_11815" s="T847">emphpro</ta>
            <ta e="T849" id="Seg_11816" s="T848">n</ta>
            <ta e="T850" id="Seg_11817" s="T849">v</ta>
            <ta e="T851" id="Seg_11818" s="T850">v</ta>
            <ta e="T852" id="Seg_11819" s="T851">n</ta>
            <ta e="T853" id="Seg_11820" s="T852">v</ta>
            <ta e="T854" id="Seg_11821" s="T853">v</ta>
            <ta e="T855" id="Seg_11822" s="T854">posspr</ta>
            <ta e="T856" id="Seg_11823" s="T855">n</ta>
            <ta e="T857" id="Seg_11824" s="T856">n</ta>
            <ta e="T858" id="Seg_11825" s="T857">dempro</ta>
            <ta e="T859" id="Seg_11826" s="T858">n</ta>
            <ta e="T860" id="Seg_11827" s="T859">adv</ta>
            <ta e="T861" id="Seg_11828" s="T860">v</ta>
            <ta e="T862" id="Seg_11829" s="T861">posspr</ta>
            <ta e="T863" id="Seg_11830" s="T862">adj</ta>
            <ta e="T864" id="Seg_11831" s="T863">n</ta>
            <ta e="T865" id="Seg_11832" s="T864">n</ta>
            <ta e="T866" id="Seg_11833" s="T865">n</ta>
            <ta e="T867" id="Seg_11834" s="T866">adv</ta>
            <ta e="T868" id="Seg_11835" s="T867">posspr</ta>
            <ta e="T869" id="Seg_11836" s="T868">n</ta>
            <ta e="T870" id="Seg_11837" s="T869">n</ta>
            <ta e="T871" id="Seg_11838" s="T870">v</ta>
            <ta e="T872" id="Seg_11839" s="T871">n</ta>
            <ta e="T873" id="Seg_11840" s="T872">v</ta>
            <ta e="T874" id="Seg_11841" s="T873">pers</ta>
            <ta e="T875" id="Seg_11842" s="T874">n</ta>
            <ta e="T876" id="Seg_11843" s="T875">adj</ta>
            <ta e="T877" id="Seg_11844" s="T876">n</ta>
            <ta e="T878" id="Seg_11845" s="T877">n</ta>
            <ta e="T882" id="Seg_11846" s="T881">v</ta>
            <ta e="T883" id="Seg_11847" s="T882">adv</ta>
            <ta e="T884" id="Seg_11848" s="T883">adv</ta>
            <ta e="T885" id="Seg_11849" s="T884">n</ta>
            <ta e="T890" id="Seg_11850" s="T889">v</ta>
            <ta e="T891" id="Seg_11851" s="T890">adv</ta>
            <ta e="T892" id="Seg_11852" s="T891">pers</ta>
            <ta e="T893" id="Seg_11853" s="T892">adj</ta>
            <ta e="T894" id="Seg_11854" s="T893">n</ta>
            <ta e="T895" id="Seg_11855" s="T894">v</ta>
            <ta e="T896" id="Seg_11856" s="T895">n</ta>
            <ta e="T897" id="Seg_11857" s="T896">n</ta>
            <ta e="T898" id="Seg_11858" s="T897">adv</ta>
            <ta e="T899" id="Seg_11859" s="T898">v</ta>
            <ta e="T900" id="Seg_11860" s="T899">aux</ta>
            <ta e="T901" id="Seg_11861" s="T900">n</ta>
            <ta e="T902" id="Seg_11862" s="T901">post</ta>
            <ta e="T903" id="Seg_11863" s="T902">v</ta>
            <ta e="T904" id="Seg_11864" s="T903">n</ta>
            <ta e="T905" id="Seg_11865" s="T904">dempro</ta>
            <ta e="T906" id="Seg_11866" s="T905">post</ta>
            <ta e="T907" id="Seg_11867" s="T906">que</ta>
            <ta e="T908" id="Seg_11868" s="T907">ptcl</ta>
            <ta e="T909" id="Seg_11869" s="T908">adv</ta>
            <ta e="T910" id="Seg_11870" s="T909">n</ta>
            <ta e="T911" id="Seg_11871" s="T910">cop</ta>
            <ta e="T912" id="Seg_11872" s="T911">v</ta>
            <ta e="T913" id="Seg_11873" s="T912">pers</ta>
            <ta e="T914" id="Seg_11874" s="T913">n</ta>
            <ta e="T915" id="Seg_11875" s="T914">n</ta>
            <ta e="T916" id="Seg_11876" s="T915">adj</ta>
            <ta e="T917" id="Seg_11877" s="T916">ptcl</ta>
            <ta e="T918" id="Seg_11878" s="T917">n</ta>
            <ta e="T919" id="Seg_11879" s="T918">v</ta>
            <ta e="T920" id="Seg_11880" s="T919">ptcl</ta>
            <ta e="T921" id="Seg_11881" s="T920">aux</ta>
            <ta e="T922" id="Seg_11882" s="T921">pers</ta>
            <ta e="T923" id="Seg_11883" s="T922">n</ta>
            <ta e="T924" id="Seg_11884" s="T923">adv</ta>
            <ta e="T925" id="Seg_11885" s="T924">emphpro</ta>
            <ta e="T926" id="Seg_11886" s="T925">n</ta>
            <ta e="T927" id="Seg_11887" s="T926">v</ta>
            <ta e="T928" id="Seg_11888" s="T927">adj</ta>
            <ta e="T929" id="Seg_11889" s="T928">adj</ta>
            <ta e="T930" id="Seg_11890" s="T929">propr</ta>
            <ta e="T931" id="Seg_11891" s="T930">v</ta>
            <ta e="T932" id="Seg_11892" s="T931">n</ta>
            <ta e="T933" id="Seg_11893" s="T932">adj</ta>
            <ta e="T934" id="Seg_11894" s="T933">n</ta>
            <ta e="T935" id="Seg_11895" s="T934">v</ta>
            <ta e="T936" id="Seg_11896" s="T935">pers</ta>
            <ta e="T937" id="Seg_11897" s="T936">adj</ta>
            <ta e="T938" id="Seg_11898" s="T937">v</ta>
            <ta e="T939" id="Seg_11899" s="T938">adv</ta>
            <ta e="T940" id="Seg_11900" s="T939">n</ta>
            <ta e="T941" id="Seg_11901" s="T940">ptcl</ta>
            <ta e="T942" id="Seg_11902" s="T941">cop</ta>
            <ta e="T943" id="Seg_11903" s="T942">n</ta>
            <ta e="T944" id="Seg_11904" s="T943">n</ta>
            <ta e="T945" id="Seg_11905" s="T944">ptcl</ta>
            <ta e="T946" id="Seg_11906" s="T945">cop</ta>
            <ta e="T947" id="Seg_11907" s="T946">adj</ta>
            <ta e="T948" id="Seg_11908" s="T947">adj</ta>
            <ta e="T949" id="Seg_11909" s="T948">n</ta>
            <ta e="T950" id="Seg_11910" s="T949">adj</ta>
            <ta e="T951" id="Seg_11911" s="T950">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR">
            <ta e="T1" id="Seg_11912" s="T0">pro.h:A</ta>
            <ta e="T2" id="Seg_11913" s="T1">np:G</ta>
            <ta e="T11" id="Seg_11914" s="T10">n:Time</ta>
            <ta e="T16" id="Seg_11915" s="T15">n:Time</ta>
            <ta e="T18" id="Seg_11916" s="T17">0.1.h:A</ta>
            <ta e="T23" id="Seg_11917" s="T22">n:Time</ta>
            <ta e="T25" id="Seg_11918" s="T24">np:L</ta>
            <ta e="T27" id="Seg_11919" s="T26">0.1.h:A</ta>
            <ta e="T31" id="Seg_11920" s="T30">np:L</ta>
            <ta e="T32" id="Seg_11921" s="T31">adv:Time</ta>
            <ta e="T39" id="Seg_11922" s="T38">n:Time</ta>
            <ta e="T40" id="Seg_11923" s="T39">pro.h:Th</ta>
            <ta e="T41" id="Seg_11924" s="T40">0.3.h:A</ta>
            <ta e="T43" id="Seg_11925" s="T41">pp:G</ta>
            <ta e="T45" id="Seg_11926" s="T44">np:Th</ta>
            <ta e="T48" id="Seg_11927" s="T47">np:G</ta>
            <ta e="T49" id="Seg_11928" s="T48">pro.h:A</ta>
            <ta e="T50" id="Seg_11929" s="T49">np:Th</ta>
            <ta e="T55" id="Seg_11930" s="T54">np:L</ta>
            <ta e="T56" id="Seg_11931" s="T55">np:Poss</ta>
            <ta e="T57" id="Seg_11932" s="T56">np:L</ta>
            <ta e="T58" id="Seg_11933" s="T57">adv:L</ta>
            <ta e="T61" id="Seg_11934" s="T60">np.h:A</ta>
            <ta e="T65" id="Seg_11935" s="T64">np:Th</ta>
            <ta e="T68" id="Seg_11936" s="T67">np:Th</ta>
            <ta e="T71" id="Seg_11937" s="T70">pro.h:Poss</ta>
            <ta e="T72" id="Seg_11938" s="T71">np:Th</ta>
            <ta e="T75" id="Seg_11939" s="T73">pp:G</ta>
            <ta e="T79" id="Seg_11940" s="T78">np:G</ta>
            <ta e="T83" id="Seg_11941" s="T82">adv:Time</ta>
            <ta e="T85" id="Seg_11942" s="T84">np:Th</ta>
            <ta e="T89" id="Seg_11943" s="T88">0.3.h:A</ta>
            <ta e="T98" id="Seg_11944" s="T97">np:Poss</ta>
            <ta e="T99" id="Seg_11945" s="T98">np.h:Poss</ta>
            <ta e="T100" id="Seg_11946" s="T99">np:Th</ta>
            <ta e="T105" id="Seg_11947" s="T103">pp:Path</ta>
            <ta e="T110" id="Seg_11948" s="T109">pro.h:Poss</ta>
            <ta e="T112" id="Seg_11949" s="T111">np:Th</ta>
            <ta e="T115" id="Seg_11950" s="T114">np:Th</ta>
            <ta e="T118" id="Seg_11951" s="T117">pro.h:Poss</ta>
            <ta e="T119" id="Seg_11952" s="T118">np.h:A</ta>
            <ta e="T121" id="Seg_11953" s="T120">np:L</ta>
            <ta e="T126" id="Seg_11954" s="T125">np:G</ta>
            <ta e="T131" id="Seg_11955" s="T129">pp:Time</ta>
            <ta e="T133" id="Seg_11956" s="T132">np:Th</ta>
            <ta e="T137" id="Seg_11957" s="T135">pp:Time</ta>
            <ta e="T138" id="Seg_11958" s="T137">pro.h:A</ta>
            <ta e="T140" id="Seg_11959" s="T139">np:St</ta>
            <ta e="T141" id="Seg_11960" s="T140">np:St</ta>
            <ta e="T148" id="Seg_11961" s="T147">pro:St</ta>
            <ta e="T159" id="Seg_11962" s="T158">np.h:P</ta>
            <ta e="T163" id="Seg_11963" s="T162">np.h:P</ta>
            <ta e="T168" id="Seg_11964" s="T167">np:P</ta>
            <ta e="T169" id="Seg_11965" s="T168">np:P</ta>
            <ta e="T170" id="Seg_11966" s="T169">np:P</ta>
            <ta e="T171" id="Seg_11967" s="T170">np:P</ta>
            <ta e="T175" id="Seg_11968" s="T174">np:P</ta>
            <ta e="T177" id="Seg_11969" s="T176">np:P</ta>
            <ta e="T179" id="Seg_11970" s="T178">pro.h:E</ta>
            <ta e="T184" id="Seg_11971" s="T183">adv:Time</ta>
            <ta e="T188" id="Seg_11972" s="T187">np:Th</ta>
            <ta e="T191" id="Seg_11973" s="T190">np:L</ta>
            <ta e="T192" id="Seg_11974" s="T191">np.h:E</ta>
            <ta e="T196" id="Seg_11975" s="T195">np:Cau</ta>
            <ta e="T199" id="Seg_11976" s="T198">np:L</ta>
            <ta e="T200" id="Seg_11977" s="T199">pro.h:Th</ta>
            <ta e="T202" id="Seg_11978" s="T201">np:L</ta>
            <ta e="T203" id="Seg_11979" s="T202">n:Time</ta>
            <ta e="T210" id="Seg_11980" s="T209">np:So</ta>
            <ta e="T214" id="Seg_11981" s="T213">np:A</ta>
            <ta e="T215" id="Seg_11982" s="T214">pro:A</ta>
            <ta e="T218" id="Seg_11983" s="T217">np:L</ta>
            <ta e="T224" id="Seg_11984" s="T223">np:Th</ta>
            <ta e="T231" id="Seg_11985" s="T230">0.3.h:A</ta>
            <ta e="T238" id="Seg_11986" s="T237">np:Ins</ta>
            <ta e="T239" id="Seg_11987" s="T238">np:Ins</ta>
            <ta e="T241" id="Seg_11988" s="T240">np:P</ta>
            <ta e="T245" id="Seg_11989" s="T244">np:A</ta>
            <ta e="T248" id="Seg_11990" s="T247">np:St</ta>
            <ta e="T255" id="Seg_11991" s="T254">np:Th</ta>
            <ta e="T259" id="Seg_11992" s="T258">np:L</ta>
            <ta e="T260" id="Seg_11993" s="T259">np.h:E</ta>
            <ta e="T261" id="Seg_11994" s="T260">np:L</ta>
            <ta e="T263" id="Seg_11995" s="T262">np:St</ta>
            <ta e="T269" id="Seg_11996" s="T268">np.h:E</ta>
            <ta e="T271" id="Seg_11997" s="T270">np:Poss</ta>
            <ta e="T272" id="Seg_11998" s="T271">np:Cau</ta>
            <ta e="T273" id="Seg_11999" s="T272">pro:St</ta>
            <ta e="T281" id="Seg_12000" s="T280">n:Time</ta>
            <ta e="T284" id="Seg_12001" s="T283">np:L</ta>
            <ta e="T285" id="Seg_12002" s="T284">np:Th</ta>
            <ta e="T288" id="Seg_12003" s="T287">0.3.h:A</ta>
            <ta e="T289" id="Seg_12004" s="T288">np:So</ta>
            <ta e="T290" id="Seg_12005" s="T289">np:So</ta>
            <ta e="T293" id="Seg_12006" s="T292">np:Th</ta>
            <ta e="T294" id="Seg_12007" s="T293">np:Th</ta>
            <ta e="T299" id="Seg_12008" s="T298">np:Th</ta>
            <ta e="T310" id="Seg_12009" s="T309">pro:A</ta>
            <ta e="T314" id="Seg_12010" s="T313">np.h:Th</ta>
            <ta e="T316" id="Seg_12011" s="T315">np:Th</ta>
            <ta e="T318" id="Seg_12012" s="T317">np.h:A</ta>
            <ta e="T322" id="Seg_12013" s="T321">np:Th</ta>
            <ta e="T324" id="Seg_12014" s="T323">adv:G</ta>
            <ta e="T330" id="Seg_12015" s="T329">np:G</ta>
            <ta e="T332" id="Seg_12016" s="T331">0.3.h:A</ta>
            <ta e="T336" id="Seg_12017" s="T335">n:Time</ta>
            <ta e="T339" id="Seg_12018" s="T338">np:Th</ta>
            <ta e="T343" id="Seg_12019" s="T342">np:Th</ta>
            <ta e="T345" id="Seg_12020" s="T344">pro.h:A</ta>
            <ta e="T348" id="Seg_12021" s="T347">n:Time</ta>
            <ta e="T351" id="Seg_12022" s="T350">np.h:A</ta>
            <ta e="T357" id="Seg_12023" s="T356">np:Th</ta>
            <ta e="T360" id="Seg_12024" s="T359">0.3.h:Poss np:Th</ta>
            <ta e="T361" id="Seg_12025" s="T360">0.3.h:E</ta>
            <ta e="T363" id="Seg_12026" s="T362">0.3.h:A</ta>
            <ta e="T364" id="Seg_12027" s="T363">pro.h:Poss</ta>
            <ta e="T365" id="Seg_12028" s="T364">np.h:A</ta>
            <ta e="T368" id="Seg_12029" s="T367">0.3.h:Poss np:Th</ta>
            <ta e="T371" id="Seg_12030" s="T370">np.h:Th</ta>
            <ta e="T373" id="Seg_12031" s="T371">pp:G</ta>
            <ta e="T377" id="Seg_12032" s="T376">pro.h:E</ta>
            <ta e="T379" id="Seg_12033" s="T378">n:Time</ta>
            <ta e="T380" id="Seg_12034" s="T379">np:L</ta>
            <ta e="T381" id="Seg_12035" s="T380">0.1.h:A</ta>
            <ta e="T388" id="Seg_12036" s="T387">pro.h:E</ta>
            <ta e="T390" id="Seg_12037" s="T389">np:L</ta>
            <ta e="T394" id="Seg_12038" s="T393">np:St</ta>
            <ta e="T397" id="Seg_12039" s="T396">np.h:Poss</ta>
            <ta e="T398" id="Seg_12040" s="T397">np:Th</ta>
            <ta e="T403" id="Seg_12041" s="T402">np.h:Poss</ta>
            <ta e="T404" id="Seg_12042" s="T403">np:Th</ta>
            <ta e="T409" id="Seg_12043" s="T408">np.h:Th</ta>
            <ta e="T412" id="Seg_12044" s="T411">pro.h:Th</ta>
            <ta e="T415" id="Seg_12045" s="T414">np:L</ta>
            <ta e="T416" id="Seg_12046" s="T415">np:Poss</ta>
            <ta e="T417" id="Seg_12047" s="T416">np:L</ta>
            <ta e="T421" id="Seg_12048" s="T420">0.1.h:Th</ta>
            <ta e="T424" id="Seg_12049" s="T423">0.1.h:Poss np:Poss</ta>
            <ta e="T425" id="Seg_12050" s="T424">np:P</ta>
            <ta e="T431" id="Seg_12051" s="T430">np:So</ta>
            <ta e="T432" id="Seg_12052" s="T431">0.1.h:A</ta>
            <ta e="T435" id="Seg_12053" s="T434">np:L</ta>
            <ta e="T436" id="Seg_12054" s="T435">pro.h:Th</ta>
            <ta e="T443" id="Seg_12055" s="T442">np:So</ta>
            <ta e="T446" id="Seg_12056" s="T445">n:Time</ta>
            <ta e="T447" id="Seg_12057" s="T446">np:L</ta>
            <ta e="T452" id="Seg_12058" s="T451">np.h:Th</ta>
            <ta e="T457" id="Seg_12059" s="T456">pro.h:Th</ta>
            <ta e="T463" id="Seg_12060" s="T462">pro:Cau</ta>
            <ta e="T469" id="Seg_12061" s="T468">np:Th</ta>
            <ta e="T472" id="Seg_12062" s="T471">np:L</ta>
            <ta e="T474" id="Seg_12063" s="T473">np:Th</ta>
            <ta e="T477" id="Seg_12064" s="T476">np.h:P</ta>
            <ta e="T479" id="Seg_12065" s="T478">np:L</ta>
            <ta e="T482" id="Seg_12066" s="T481">pro.h:Poss</ta>
            <ta e="T483" id="Seg_12067" s="T482">np.h:A</ta>
            <ta e="T486" id="Seg_12068" s="T485">np.h:Th</ta>
            <ta e="T488" id="Seg_12069" s="T486">pp:G</ta>
            <ta e="T493" id="Seg_12070" s="T492">n:Time</ta>
            <ta e="T494" id="Seg_12071" s="T493">pro.h:Th</ta>
            <ta e="T498" id="Seg_12072" s="T497">np:L</ta>
            <ta e="T506" id="Seg_12073" s="T505">n:Time</ta>
            <ta e="T510" id="Seg_12074" s="T508">pp:Time</ta>
            <ta e="T512" id="Seg_12075" s="T511">n:Time</ta>
            <ta e="T515" id="Seg_12076" s="T513">pp:Com</ta>
            <ta e="T518" id="Seg_12077" s="T517">0.1.h:A</ta>
            <ta e="T520" id="Seg_12078" s="T519">np:L</ta>
            <ta e="T533" id="Seg_12079" s="T532">np:Th</ta>
            <ta e="T537" id="Seg_12080" s="T536">np.h:Poss</ta>
            <ta e="T538" id="Seg_12081" s="T537">np:L</ta>
            <ta e="T540" id="Seg_12082" s="T539">np:So</ta>
            <ta e="T543" id="Seg_12083" s="T542">pro.h:A</ta>
            <ta e="T546" id="Seg_12084" s="T544">pp:Com</ta>
            <ta e="T548" id="Seg_12085" s="T547">np:Poss</ta>
            <ta e="T549" id="Seg_12086" s="T548">n:Time</ta>
            <ta e="T551" id="Seg_12087" s="T550">np:Th</ta>
            <ta e="T555" id="Seg_12088" s="T554">np:So</ta>
            <ta e="T556" id="Seg_12089" s="T555">np:G</ta>
            <ta e="T561" id="Seg_12090" s="T560">n:Time</ta>
            <ta e="T562" id="Seg_12091" s="T561">pro.h:A</ta>
            <ta e="T563" id="Seg_12092" s="T562">np:So</ta>
            <ta e="T567" id="Seg_12093" s="T566">np:Th</ta>
            <ta e="T575" id="Seg_12094" s="T574">n:Time</ta>
            <ta e="T579" id="Seg_12095" s="T578">np:L</ta>
            <ta e="T581" id="Seg_12096" s="T580">np.h:A</ta>
            <ta e="T582" id="Seg_12097" s="T581">n:Time</ta>
            <ta e="T585" id="Seg_12098" s="T584">pro.h:Th</ta>
            <ta e="T591" id="Seg_12099" s="T589">pp:Cau</ta>
            <ta e="T592" id="Seg_12100" s="T591">pro.h:E</ta>
            <ta e="T595" id="Seg_12101" s="T594">n:Time</ta>
            <ta e="T599" id="Seg_12102" s="T598">np:L</ta>
            <ta e="T600" id="Seg_12103" s="T599">0.1.h:Th</ta>
            <ta e="T603" id="Seg_12104" s="T602">np:P</ta>
            <ta e="T604" id="Seg_12105" s="T603">adv:Time</ta>
            <ta e="T605" id="Seg_12106" s="T604">0.1.h:A</ta>
            <ta e="T606" id="Seg_12107" s="T605">np.h:Poss</ta>
            <ta e="T607" id="Seg_12108" s="T606">np:A</ta>
            <ta e="T612" id="Seg_12109" s="T611">np:Th</ta>
            <ta e="T618" id="Seg_12110" s="T616">pp:Cau</ta>
            <ta e="T619" id="Seg_12111" s="T618">adv:Time</ta>
            <ta e="T620" id="Seg_12112" s="T619">pro.h:A</ta>
            <ta e="T621" id="Seg_12113" s="T620">adv:Time</ta>
            <ta e="T623" id="Seg_12114" s="T622">np:L</ta>
            <ta e="T624" id="Seg_12115" s="T623">np:P</ta>
            <ta e="T628" id="Seg_12116" s="T627">np.h:A</ta>
            <ta e="T632" id="Seg_12117" s="T631">0.1.h:Poss np:P</ta>
            <ta e="T635" id="Seg_12118" s="T634">adv:Time</ta>
            <ta e="T637" id="Seg_12119" s="T636">np.h:E</ta>
            <ta e="T643" id="Seg_12120" s="T642">np:Ins</ta>
            <ta e="T650" id="Seg_12121" s="T649">np:Th</ta>
            <ta e="T652" id="Seg_12122" s="T651">np:G</ta>
            <ta e="T660" id="Seg_12123" s="T658">pp:G</ta>
            <ta e="T663" id="Seg_12124" s="T662">0.3.h:A</ta>
            <ta e="T664" id="Seg_12125" s="T663">pro.h:Poss</ta>
            <ta e="T665" id="Seg_12126" s="T664">np.h:A</ta>
            <ta e="T672" id="Seg_12127" s="T671">np.h:A</ta>
            <ta e="T673" id="Seg_12128" s="T672">np:Ins</ta>
            <ta e="T674" id="Seg_12129" s="T673">pro.h:A</ta>
            <ta e="T677" id="Seg_12130" s="T676">np:Th</ta>
            <ta e="T682" id="Seg_12131" s="T681">np.h:A</ta>
            <ta e="T690" id="Seg_12132" s="T689">np:Th</ta>
            <ta e="T692" id="Seg_12133" s="T691">np:Th</ta>
            <ta e="T693" id="Seg_12134" s="T692">np:Th</ta>
            <ta e="T694" id="Seg_12135" s="T693">np:Th</ta>
            <ta e="T705" id="Seg_12136" s="T704">np.h:A</ta>
            <ta e="T708" id="Seg_12137" s="T707">np:Th</ta>
            <ta e="T713" id="Seg_12138" s="T712">np:Path</ta>
            <ta e="T716" id="Seg_12139" s="T715">np:So</ta>
            <ta e="T719" id="Seg_12140" s="T717">pp:G</ta>
            <ta e="T720" id="Seg_12141" s="T719">np:L</ta>
            <ta e="T722" id="Seg_12142" s="T721">np.h:E</ta>
            <ta e="T723" id="Seg_12143" s="T722">pro.h:Th</ta>
            <ta e="T732" id="Seg_12144" s="T731">np:A</ta>
            <ta e="T738" id="Seg_12145" s="T737">n:Time</ta>
            <ta e="T740" id="Seg_12146" s="T739">np.h:P</ta>
            <ta e="T743" id="Seg_12147" s="T742">np:Ins</ta>
            <ta e="T745" id="Seg_12148" s="T744">pro.h:E</ta>
            <ta e="T748" id="Seg_12149" s="T747">adv:Time</ta>
            <ta e="T750" id="Seg_12150" s="T749">np:Th</ta>
            <ta e="T753" id="Seg_12151" s="T752">pro.h:Poss pro.h:A</ta>
            <ta e="T755" id="Seg_12152" s="T754">np.h:Th</ta>
            <ta e="T761" id="Seg_12153" s="T760">pro.h:Th</ta>
            <ta e="T764" id="Seg_12154" s="T763">np:G</ta>
            <ta e="T767" id="Seg_12155" s="T766">n:Time</ta>
            <ta e="T769" id="Seg_12156" s="T768">n:Time</ta>
            <ta e="T775" id="Seg_12157" s="T774">n:Time</ta>
            <ta e="T782" id="Seg_12158" s="T781">0.1.h:Poss np:Poss</ta>
            <ta e="T783" id="Seg_12159" s="T782">np:P</ta>
            <ta e="T785" id="Seg_12160" s="T784">0.1.h:A</ta>
            <ta e="T787" id="Seg_12161" s="T786">0.1.h:A</ta>
            <ta e="T788" id="Seg_12162" s="T787">0.1.h:Th</ta>
            <ta e="T792" id="Seg_12163" s="T790">pp:Time</ta>
            <ta e="T793" id="Seg_12164" s="T792">0.1.h:Th</ta>
            <ta e="T796" id="Seg_12165" s="T795">np:L</ta>
            <ta e="T800" id="Seg_12166" s="T799">np:L</ta>
            <ta e="T807" id="Seg_12167" s="T806">0.1.h:A</ta>
            <ta e="T808" id="Seg_12168" s="T807">np:So</ta>
            <ta e="T810" id="Seg_12169" s="T809">0.1.h:Th</ta>
            <ta e="T811" id="Seg_12170" s="T810">np:So</ta>
            <ta e="T813" id="Seg_12171" s="T812">0.1.h:A</ta>
            <ta e="T816" id="Seg_12172" s="T815">n:Time</ta>
            <ta e="T820" id="Seg_12173" s="T818">pp:Time</ta>
            <ta e="T822" id="Seg_12174" s="T821">np:L</ta>
            <ta e="T823" id="Seg_12175" s="T822">0.1.h:Th</ta>
            <ta e="T827" id="Seg_12176" s="T826">0.1.h:Th</ta>
            <ta e="T829" id="Seg_12177" s="T828">np:Cau</ta>
            <ta e="T832" id="Seg_12178" s="T831">0.1.h:A</ta>
            <ta e="T836" id="Seg_12179" s="T835">pro.h:A</ta>
            <ta e="T847" id="Seg_12180" s="T846">np:G</ta>
            <ta e="T848" id="Seg_12181" s="T847">pro.h:Poss</ta>
            <ta e="T849" id="Seg_12182" s="T848">np.h:P</ta>
            <ta e="T852" id="Seg_12183" s="T851">0.1.h:Poss np:P</ta>
            <ta e="T855" id="Seg_12184" s="T854">pro.h:Poss</ta>
            <ta e="T856" id="Seg_12185" s="T855">np:Th</ta>
            <ta e="T857" id="Seg_12186" s="T856">np:Th</ta>
            <ta e="T859" id="Seg_12187" s="T858">np:L</ta>
            <ta e="T862" id="Seg_12188" s="T861">pro.h:Poss</ta>
            <ta e="T864" id="Seg_12189" s="T863">np:Poss</ta>
            <ta e="T866" id="Seg_12190" s="T865">np.h:A</ta>
            <ta e="T868" id="Seg_12191" s="T867">pro.h:Poss</ta>
            <ta e="T869" id="Seg_12192" s="T868">np:Poss</ta>
            <ta e="T870" id="Seg_12193" s="T869">np:Th</ta>
            <ta e="T872" id="Seg_12194" s="T871">0.1.h:Poss np.h:A</ta>
            <ta e="T874" id="Seg_12195" s="T873">pro.h:R</ta>
            <ta e="T875" id="Seg_12196" s="T874">np:Poss</ta>
            <ta e="T878" id="Seg_12197" s="T877">np:Th</ta>
            <ta e="T885" id="Seg_12198" s="T884">np:Th</ta>
            <ta e="T892" id="Seg_12199" s="T891">pro.h:Poss</ta>
            <ta e="T894" id="Seg_12200" s="T893">np:L</ta>
            <ta e="T897" id="Seg_12201" s="T896">0.2.h:Poss np:Th</ta>
            <ta e="T902" id="Seg_12202" s="T900">pp:G</ta>
            <ta e="T904" id="Seg_12203" s="T903">np:G</ta>
            <ta e="T906" id="Seg_12204" s="T904">pp:Cau</ta>
            <ta e="T907" id="Seg_12205" s="T906">pro.h:E</ta>
            <ta e="T910" id="Seg_12206" s="T909">np:Th</ta>
            <ta e="T913" id="Seg_12207" s="T912">pro.h:Poss</ta>
            <ta e="T914" id="Seg_12208" s="T913">np:Poss</ta>
            <ta e="T915" id="Seg_12209" s="T914">np.h:E</ta>
            <ta e="T916" id="Seg_12210" s="T915">adv:Time</ta>
            <ta e="T922" id="Seg_12211" s="T921">pro.h:Poss</ta>
            <ta e="T923" id="Seg_12212" s="T922">np.h:A</ta>
            <ta e="T925" id="Seg_12213" s="T924">pro.h:Poss</ta>
            <ta e="T930" id="Seg_12214" s="T929">np.h:A</ta>
            <ta e="T934" id="Seg_12215" s="T933">np:Th</ta>
            <ta e="T936" id="Seg_12216" s="T935">pro.h:A</ta>
            <ta e="T939" id="Seg_12217" s="T938">adv:Time</ta>
            <ta e="T940" id="Seg_12218" s="T939">np:Th</ta>
            <ta e="T944" id="Seg_12219" s="T943">np:L</ta>
            <ta e="T949" id="Seg_12220" s="T948">np:Th</ta>
            <ta e="T951" id="Seg_12221" s="T950">np.h:B</ta>
         </annotation>
         <annotation name="SyF" tierref="SyF">
            <ta e="T1" id="Seg_12222" s="T0">pro.h:S</ta>
            <ta e="T3" id="Seg_12223" s="T2">v:pred</ta>
            <ta e="T15" id="Seg_12224" s="T13">s:rel</ta>
            <ta e="T18" id="Seg_12225" s="T16">s:temp</ta>
            <ta e="T27" id="Seg_12226" s="T26">0.1.h:S v:pred</ta>
            <ta e="T30" id="Seg_12227" s="T27">s:adv</ta>
            <ta e="T40" id="Seg_12228" s="T39">pro.h:O</ta>
            <ta e="T41" id="Seg_12229" s="T40">0.3.h:S v:pred</ta>
            <ta e="T47" id="Seg_12230" s="T43">s:rel</ta>
            <ta e="T49" id="Seg_12231" s="T48">pro.h:S</ta>
            <ta e="T50" id="Seg_12232" s="T49">np:O</ta>
            <ta e="T51" id="Seg_12233" s="T50">v:pred</ta>
            <ta e="T53" id="Seg_12234" s="T51">s:adv</ta>
            <ta e="T61" id="Seg_12235" s="T60">np.h:S</ta>
            <ta e="T70" id="Seg_12236" s="T61">s:temp</ta>
            <ta e="T72" id="Seg_12237" s="T71">np:O</ta>
            <ta e="T81" id="Seg_12238" s="T80">v:pred</ta>
            <ta e="T85" id="Seg_12239" s="T84">np:O</ta>
            <ta e="T88" id="Seg_12240" s="T85">s:adv</ta>
            <ta e="T89" id="Seg_12241" s="T88">0.3.h:S v:pred</ta>
            <ta e="T91" id="Seg_12242" s="T89">s:cond</ta>
            <ta e="T95" id="Seg_12243" s="T94">adj:pred</ta>
            <ta e="T96" id="Seg_12244" s="T95">0.3:S cop</ta>
            <ta e="T100" id="Seg_12245" s="T99">np:S</ta>
            <ta e="T101" id="Seg_12246" s="T100">ptcl:pred</ta>
            <ta e="T102" id="Seg_12247" s="T101">cop</ta>
            <ta e="T119" id="Seg_12248" s="T118">np.h:S</ta>
            <ta e="T128" id="Seg_12249" s="T119">s:temp</ta>
            <ta e="T133" id="Seg_12250" s="T132">np:O</ta>
            <ta e="T135" id="Seg_12251" s="T134">v:pred</ta>
            <ta e="T138" id="Seg_12252" s="T137">pro.h:S</ta>
            <ta e="T139" id="Seg_12253" s="T138">v:pred</ta>
            <ta e="T142" id="Seg_12254" s="T139">s:adv</ta>
            <ta e="T144" id="Seg_12255" s="T142">s:adv</ta>
            <ta e="T146" id="Seg_12256" s="T144">s:adv</ta>
            <ta e="T152" id="Seg_12257" s="T146">s:adv</ta>
            <ta e="T161" id="Seg_12258" s="T152">s:comp</ta>
            <ta e="T165" id="Seg_12259" s="T161">s:comp</ta>
            <ta e="T178" id="Seg_12260" s="T165">s:comp</ta>
            <ta e="T179" id="Seg_12261" s="T178">pro.h:S</ta>
            <ta e="T183" id="Seg_12262" s="T182">v:pred</ta>
            <ta e="T191" id="Seg_12263" s="T184">s:comp</ta>
            <ta e="T192" id="Seg_12264" s="T191">np.h:S</ta>
            <ta e="T193" id="Seg_12265" s="T192">v:pred</ta>
            <ta e="T201" id="Seg_12266" s="T199">s:rel</ta>
            <ta e="T207" id="Seg_12267" s="T206">v:pred</ta>
            <ta e="T214" id="Seg_12268" s="T213">np:S</ta>
            <ta e="T215" id="Seg_12269" s="T214">pro:S</ta>
            <ta e="T221" id="Seg_12270" s="T218">s:adv</ta>
            <ta e="T224" id="Seg_12271" s="T223">np:O</ta>
            <ta e="T226" id="Seg_12272" s="T225">v:pred</ta>
            <ta e="T231" id="Seg_12273" s="T230">0.3.h:S v:pred</ta>
            <ta e="T243" id="Seg_12274" s="T239">s:temp</ta>
            <ta e="T247" id="Seg_12275" s="T243">s:temp</ta>
            <ta e="T253" id="Seg_12276" s="T247">s:adv</ta>
            <ta e="T255" id="Seg_12277" s="T254">np:S</ta>
            <ta e="T257" id="Seg_12278" s="T256">v:pred</ta>
            <ta e="T260" id="Seg_12279" s="T259">np.h:S</ta>
            <ta e="T262" id="Seg_12280" s="T260">s:rel</ta>
            <ta e="T263" id="Seg_12281" s="T262">np.h:O</ta>
            <ta e="T266" id="Seg_12282" s="T265">v:pred</ta>
            <ta e="T269" id="Seg_12283" s="T268">np.h:S</ta>
            <ta e="T276" id="Seg_12284" s="T272">s:adv</ta>
            <ta e="T277" id="Seg_12285" s="T276">adj:pred</ta>
            <ta e="T279" id="Seg_12286" s="T278">cop</ta>
            <ta e="T285" id="Seg_12287" s="T284">np:O</ta>
            <ta e="T288" id="Seg_12288" s="T287">0.3.h:S v:pred</ta>
            <ta e="T297" id="Seg_12289" s="T294">s:rel</ta>
            <ta e="T302" id="Seg_12290" s="T299">s:adv</ta>
            <ta e="T310" id="Seg_12291" s="T309">pro:S</ta>
            <ta e="T313" id="Seg_12292" s="T312">v:pred</ta>
            <ta e="T315" id="Seg_12293" s="T313">s:rel</ta>
            <ta e="T316" id="Seg_12294" s="T315">np:O</ta>
            <ta e="T318" id="Seg_12295" s="T317">np.h:S</ta>
            <ta e="T323" id="Seg_12296" s="T318">s:adv</ta>
            <ta e="T325" id="Seg_12297" s="T324">v:pred</ta>
            <ta e="T332" id="Seg_12298" s="T331">0.3.h:S v:pred</ta>
            <ta e="T338" id="Seg_12299" s="T336">s:adv</ta>
            <ta e="T339" id="Seg_12300" s="T338">np:S</ta>
            <ta e="T341" id="Seg_12301" s="T340">v:pred</ta>
            <ta e="T343" id="Seg_12302" s="T342">np:O</ta>
            <ta e="T345" id="Seg_12303" s="T344">pro.h:S</ta>
            <ta e="T346" id="Seg_12304" s="T345">v:pred</ta>
            <ta e="T355" id="Seg_12305" s="T346">s:temp</ta>
            <ta e="T359" id="Seg_12306" s="T356">s:temp</ta>
            <ta e="T360" id="Seg_12307" s="T359">np:O</ta>
            <ta e="T361" id="Seg_12308" s="T360">0.3.h:S v:pred</ta>
            <ta e="T363" id="Seg_12309" s="T362">0.3.h:S v:pred</ta>
            <ta e="T365" id="Seg_12310" s="T364">np.h:S</ta>
            <ta e="T370" id="Seg_12311" s="T365">s:temp</ta>
            <ta e="T371" id="Seg_12312" s="T370">np.h:O</ta>
            <ta e="T374" id="Seg_12313" s="T373">s:adv</ta>
            <ta e="T376" id="Seg_12314" s="T375">v:pred</ta>
            <ta e="T377" id="Seg_12315" s="T376">pro.h:S</ta>
            <ta e="T381" id="Seg_12316" s="T377">s:temp</ta>
            <ta e="T387" id="Seg_12317" s="T385">v:pred</ta>
            <ta e="T388" id="Seg_12318" s="T387">pro.h:S</ta>
            <ta e="T392" id="Seg_12319" s="T391">v:pred</ta>
            <ta e="T394" id="Seg_12320" s="T393">np:O</ta>
            <ta e="T400" id="Seg_12321" s="T394">s:adv</ta>
            <ta e="T405" id="Seg_12322" s="T400">s:adv</ta>
            <ta e="T411" id="Seg_12323" s="T405">s:adv</ta>
            <ta e="T412" id="Seg_12324" s="T411">pro.h:S</ta>
            <ta e="T421" id="Seg_12325" s="T417">s:temp</ta>
            <ta e="T425" id="Seg_12326" s="T424">np:O</ta>
            <ta e="T426" id="Seg_12327" s="T425">v:pred</ta>
            <ta e="T429" id="Seg_12328" s="T426">s:temp</ta>
            <ta e="T432" id="Seg_12329" s="T431">0.1.h:S v:pred</ta>
            <ta e="T437" id="Seg_12330" s="T435">s:rel</ta>
            <ta e="T448" id="Seg_12331" s="T447">v:pred</ta>
            <ta e="T452" id="Seg_12332" s="T451">np.h:S</ta>
            <ta e="T457" id="Seg_12333" s="T456">pro.h:S</ta>
            <ta e="T459" id="Seg_12334" s="T457">s:rel</ta>
            <ta e="T460" id="Seg_12335" s="T459">n:pred</ta>
            <ta e="T462" id="Seg_12336" s="T461">cop</ta>
            <ta e="T464" id="Seg_12337" s="T463">adj:pred</ta>
            <ta e="T465" id="Seg_12338" s="T464">cop</ta>
            <ta e="T466" id="Seg_12339" s="T465">s:comp</ta>
            <ta e="T472" id="Seg_12340" s="T466">s:comp</ta>
            <ta e="T475" id="Seg_12341" s="T472">s:comp</ta>
            <ta e="T478" id="Seg_12342" s="T475">s:comp</ta>
            <ta e="T481" id="Seg_12343" s="T478">s:temp</ta>
            <ta e="T483" id="Seg_12344" s="T482">np.h:S</ta>
            <ta e="T486" id="Seg_12345" s="T485">np.h:O</ta>
            <ta e="T490" id="Seg_12346" s="T489">v:pred</ta>
            <ta e="T494" id="Seg_12347" s="T493">pro.h:S</ta>
            <ta e="T499" id="Seg_12348" s="T498">ptcl:pred</ta>
            <ta e="T500" id="Seg_12349" s="T499">cop</ta>
            <ta e="T518" id="Seg_12350" s="T517">0.1.h:S v:pred</ta>
            <ta e="T534" id="Seg_12351" s="T520">s:temp</ta>
            <ta e="T536" id="Seg_12352" s="T534">s:adv</ta>
            <ta e="T543" id="Seg_12353" s="T542">pro.h:S</ta>
            <ta e="T544" id="Seg_12354" s="T543">v:pred</ta>
            <ta e="T551" id="Seg_12355" s="T550">np:S</ta>
            <ta e="T558" id="Seg_12356" s="T557">v:pred</ta>
            <ta e="T562" id="Seg_12357" s="T561">pro.h:S</ta>
            <ta e="T564" id="Seg_12358" s="T563">v:pred</ta>
            <ta e="T567" id="Seg_12359" s="T566">np:S</ta>
            <ta e="T569" id="Seg_12360" s="T568">ptcl:pred</ta>
            <ta e="T570" id="Seg_12361" s="T569">cop</ta>
            <ta e="T572" id="Seg_12362" s="T570">s:adv</ta>
            <ta e="T580" id="Seg_12363" s="T575">s:adv</ta>
            <ta e="T581" id="Seg_12364" s="T580">np.h:S</ta>
            <ta e="T584" id="Seg_12365" s="T581">s:adv</ta>
            <ta e="T585" id="Seg_12366" s="T584">pro.h:O</ta>
            <ta e="T589" id="Seg_12367" s="T588">v:pred</ta>
            <ta e="T592" id="Seg_12368" s="T591">pro.h:S</ta>
            <ta e="T597" id="Seg_12369" s="T596">v:pred</ta>
            <ta e="T600" id="Seg_12370" s="T599">0.1.h:S v:pred</ta>
            <ta e="T602" id="Seg_12371" s="T600">s:rel</ta>
            <ta e="T603" id="Seg_12372" s="T602">np:O</ta>
            <ta e="T605" id="Seg_12373" s="T603">s:rel</ta>
            <ta e="T607" id="Seg_12374" s="T606">np:S</ta>
            <ta e="T609" id="Seg_12375" s="T607">s:temp</ta>
            <ta e="T613" id="Seg_12376" s="T609">s:adv</ta>
            <ta e="T616" id="Seg_12377" s="T615">v:pred</ta>
            <ta e="T620" id="Seg_12378" s="T619">pro.h:S</ta>
            <ta e="T624" id="Seg_12379" s="T623">np:O</ta>
            <ta e="T626" id="Seg_12380" s="T625">v:pred</ta>
            <ta e="T628" id="Seg_12381" s="T627">np.h:S</ta>
            <ta e="T631" id="Seg_12382" s="T628">s:rel</ta>
            <ta e="T632" id="Seg_12383" s="T631">np:O</ta>
            <ta e="T634" id="Seg_12384" s="T633">v:pred</ta>
            <ta e="T640" id="Seg_12385" s="T634">s:temp</ta>
            <ta e="T646" id="Seg_12386" s="T640">s:temp</ta>
            <ta e="T654" id="Seg_12387" s="T646">s:temp</ta>
            <ta e="T658" id="Seg_12388" s="T654">s:temp</ta>
            <ta e="T663" id="Seg_12389" s="T662">0.3.h:S v:pred</ta>
            <ta e="T665" id="Seg_12390" s="T664">np.h:S</ta>
            <ta e="T670" id="Seg_12391" s="T665">s:adv</ta>
            <ta e="T672" id="Seg_12392" s="T671">np.h:S</ta>
            <ta e="T674" id="Seg_12393" s="T673">pro.h:S</ta>
            <ta e="T681" id="Seg_12394" s="T674">s:temp</ta>
            <ta e="T682" id="Seg_12395" s="T681">np.h:S</ta>
            <ta e="T684" id="Seg_12396" s="T682">s:adv</ta>
            <ta e="T685" id="Seg_12397" s="T684">v:pred</ta>
            <ta e="T687" id="Seg_12398" s="T685">s:adv</ta>
            <ta e="T688" id="Seg_12399" s="T687">np:O</ta>
            <ta e="T690" id="Seg_12400" s="T689">np:S</ta>
            <ta e="T692" id="Seg_12401" s="T691">np:S</ta>
            <ta e="T693" id="Seg_12402" s="T692">np:S</ta>
            <ta e="T694" id="Seg_12403" s="T693">np:S</ta>
            <ta e="T701" id="Seg_12404" s="T700">adj:pred</ta>
            <ta e="T702" id="Seg_12405" s="T701">cop</ta>
            <ta e="T705" id="Seg_12406" s="T704">np.h:S</ta>
            <ta e="T710" id="Seg_12407" s="T705">s:adv</ta>
            <ta e="T715" id="Seg_12408" s="T714">v:pred</ta>
            <ta e="T719" id="Seg_12409" s="T715">s:temp</ta>
            <ta e="T722" id="Seg_12410" s="T721">np.h:S</ta>
            <ta e="T723" id="Seg_12411" s="T722">pro.h:O</ta>
            <ta e="T727" id="Seg_12412" s="T726">v:pred</ta>
            <ta e="T734" id="Seg_12413" s="T728">s:adv</ta>
            <ta e="T735" id="Seg_12414" s="T734">cop</ta>
            <ta e="T744" id="Seg_12415" s="T735">s:comp</ta>
            <ta e="T745" id="Seg_12416" s="T744">pro.h:S</ta>
            <ta e="T749" id="Seg_12417" s="T748">v:pred</ta>
            <ta e="T752" id="Seg_12418" s="T749">s:temp</ta>
            <ta e="T753" id="Seg_12419" s="T752">pro.h:S</ta>
            <ta e="T755" id="Seg_12420" s="T754">np.h:O</ta>
            <ta e="T757" id="Seg_12421" s="T755">s:adv</ta>
            <ta e="T759" id="Seg_12422" s="T758">v:pred</ta>
            <ta e="T761" id="Seg_12423" s="T760">pro.h:S</ta>
            <ta e="T765" id="Seg_12424" s="T761">s:temp</ta>
            <ta e="T777" id="Seg_12425" s="T776">v:pred</ta>
            <ta e="T783" id="Seg_12426" s="T782">np:O</ta>
            <ta e="T785" id="Seg_12427" s="T784">0.1.h:S v:pred</ta>
            <ta e="T787" id="Seg_12428" s="T785">s:purp</ta>
            <ta e="T789" id="Seg_12429" s="T787">s:temp</ta>
            <ta e="T793" id="Seg_12430" s="T792">0.1.h:S v:pred</ta>
            <ta e="T795" id="Seg_12431" s="T793">s:adv</ta>
            <ta e="T798" id="Seg_12432" s="T796">s:adv</ta>
            <ta e="T803" id="Seg_12433" s="T800">s:temp</ta>
            <ta e="T807" id="Seg_12434" s="T803">s:temp</ta>
            <ta e="T810" id="Seg_12435" s="T807">s:temp</ta>
            <ta e="T813" id="Seg_12436" s="T812">0.1.h:S v:pred</ta>
            <ta e="T815" id="Seg_12437" s="T813">s:rel</ta>
            <ta e="T823" id="Seg_12438" s="T816">s:temp</ta>
            <ta e="T827" id="Seg_12439" s="T826">0.1.h:S v:pred</ta>
            <ta e="T832" id="Seg_12440" s="T831">0.1.h:S v:pred</ta>
            <ta e="T836" id="Seg_12441" s="T835">pro.h:S</ta>
            <ta e="T853" id="Seg_12442" s="T847">s:purp</ta>
            <ta e="T854" id="Seg_12443" s="T853">v:pred</ta>
            <ta e="T856" id="Seg_12444" s="T855">np:S</ta>
            <ta e="T857" id="Seg_12445" s="T856">np:S</ta>
            <ta e="T861" id="Seg_12446" s="T860">v:pred</ta>
            <ta e="T866" id="Seg_12447" s="T865">np.h:S</ta>
            <ta e="T871" id="Seg_12448" s="T867">s:rel</ta>
            <ta e="T872" id="Seg_12449" s="T871">np.h:S</ta>
            <ta e="T873" id="Seg_12450" s="T872">v:pred</ta>
            <ta e="T878" id="Seg_12451" s="T877">np:O</ta>
            <ta e="T882" id="Seg_12452" s="T878">s:adv</ta>
            <ta e="T890" id="Seg_12453" s="T885">s:adv</ta>
            <ta e="T895" id="Seg_12454" s="T890">s:rel</ta>
            <ta e="T897" id="Seg_12455" s="T896">np:S</ta>
            <ta e="T903" id="Seg_12456" s="T900">s:rel</ta>
            <ta e="T907" id="Seg_12457" s="T906">pro.h:S</ta>
            <ta e="T911" id="Seg_12458" s="T908">s:comp</ta>
            <ta e="T912" id="Seg_12459" s="T911">v:pred</ta>
            <ta e="T915" id="Seg_12460" s="T914">np.h:S</ta>
            <ta e="T921" id="Seg_12461" s="T920">v:pred</ta>
            <ta e="T923" id="Seg_12462" s="T922">np.h:S</ta>
            <ta e="T927" id="Seg_12463" s="T924">s:rel</ta>
            <ta e="T931" id="Seg_12464" s="T929">s:rel</ta>
            <ta e="T934" id="Seg_12465" s="T933">np:O</ta>
            <ta e="T935" id="Seg_12466" s="T934">v:pred</ta>
            <ta e="T936" id="Seg_12467" s="T935">pro.h:S</ta>
            <ta e="T938" id="Seg_12468" s="T937">v:pred</ta>
            <ta e="T940" id="Seg_12469" s="T939">np:S</ta>
            <ta e="T941" id="Seg_12470" s="T940">ptcl:pred</ta>
            <ta e="T942" id="Seg_12471" s="T941">cop</ta>
            <ta e="T945" id="Seg_12472" s="T944">ptcl:pred</ta>
            <ta e="T946" id="Seg_12473" s="T945">cop</ta>
            <ta e="T949" id="Seg_12474" s="T948">np:S</ta>
         </annotation>
         <annotation name="IST" tierref="IST">
            <ta e="T1" id="Seg_12475" s="T0">accs-sit</ta>
            <ta e="T2" id="Seg_12476" s="T1">new</ta>
            <ta e="T4" id="Seg_12477" s="T3">giv-active</ta>
            <ta e="T5" id="Seg_12478" s="T4">accs-inf</ta>
            <ta e="T11" id="Seg_12479" s="T10">new</ta>
            <ta e="T16" id="Seg_12480" s="T15">accs-inf</ta>
            <ta e="T18" id="Seg_12481" s="T17">0.giv-inactive</ta>
            <ta e="T25" id="Seg_12482" s="T24">new</ta>
            <ta e="T27" id="Seg_12483" s="T26">0.giv-active</ta>
            <ta e="T29" id="Seg_12484" s="T27">accs-gen</ta>
            <ta e="T39" id="Seg_12485" s="T38">new</ta>
            <ta e="T40" id="Seg_12486" s="T39">accs-inf</ta>
            <ta e="T42" id="Seg_12487" s="T41">accs-gen</ta>
            <ta e="T44" id="Seg_12488" s="T43">giv-inactive</ta>
            <ta e="T45" id="Seg_12489" s="T44">accs-inf</ta>
            <ta e="T48" id="Seg_12490" s="T47">accs-inf</ta>
            <ta e="T49" id="Seg_12491" s="T48">giv-active</ta>
            <ta e="T50" id="Seg_12492" s="T49">giv-active</ta>
            <ta e="T52" id="Seg_12493" s="T51">new</ta>
            <ta e="T56" id="Seg_12494" s="T55">new</ta>
            <ta e="T57" id="Seg_12495" s="T56">accs-inf</ta>
            <ta e="T60" id="Seg_12496" s="T59">accs-gen</ta>
            <ta e="T61" id="Seg_12497" s="T60">accs-inf</ta>
            <ta e="T65" id="Seg_12498" s="T64">accs-inf</ta>
            <ta e="T68" id="Seg_12499" s="T67">accs-inf</ta>
            <ta e="T71" id="Seg_12500" s="T70">giv-inactive</ta>
            <ta e="T72" id="Seg_12501" s="T71">accs-inf</ta>
            <ta e="T79" id="Seg_12502" s="T78">new</ta>
            <ta e="T85" id="Seg_12503" s="T84">giv-active</ta>
            <ta e="T86" id="Seg_12504" s="T85">giv-inactive</ta>
            <ta e="T92" id="Seg_12505" s="T91">giv-active</ta>
            <ta e="T98" id="Seg_12506" s="T97">giv-inactive</ta>
            <ta e="T99" id="Seg_12507" s="T98">accs-inf</ta>
            <ta e="T100" id="Seg_12508" s="T99">new</ta>
            <ta e="T104" id="Seg_12509" s="T103">giv-inactive</ta>
            <ta e="T110" id="Seg_12510" s="T109">giv-inactive</ta>
            <ta e="T113" id="Seg_12511" s="T112">accs-gen</ta>
            <ta e="T118" id="Seg_12512" s="T117">accs-inf</ta>
            <ta e="T119" id="Seg_12513" s="T118">accs-inf</ta>
            <ta e="T120" id="Seg_12514" s="T119">giv-inactive</ta>
            <ta e="T121" id="Seg_12515" s="T120">giv-inactive</ta>
            <ta e="T126" id="Seg_12516" s="T125">new</ta>
            <ta e="T133" id="Seg_12517" s="T132">giv-inactive</ta>
            <ta e="T138" id="Seg_12518" s="T137">giv-active</ta>
            <ta e="T156" id="Seg_12519" s="T155">accs-inf</ta>
            <ta e="T159" id="Seg_12520" s="T158">accs-inf</ta>
            <ta e="T163" id="Seg_12521" s="T162">accs-inf</ta>
            <ta e="T168" id="Seg_12522" s="T167">accs-inf</ta>
            <ta e="T169" id="Seg_12523" s="T168">accs-inf</ta>
            <ta e="T170" id="Seg_12524" s="T169">accs-inf</ta>
            <ta e="T171" id="Seg_12525" s="T170">accs-inf</ta>
            <ta e="T175" id="Seg_12526" s="T174">accs-inf</ta>
            <ta e="T177" id="Seg_12527" s="T176">accs-inf</ta>
            <ta e="T188" id="Seg_12528" s="T187">giv-inactive</ta>
            <ta e="T191" id="Seg_12529" s="T190">giv-inactive</ta>
            <ta e="T196" id="Seg_12530" s="T195">accs-sit</ta>
            <ta e="T199" id="Seg_12531" s="T198">new</ta>
            <ta e="T200" id="Seg_12532" s="T199">giv-inactive</ta>
            <ta e="T202" id="Seg_12533" s="T201">giv-active</ta>
            <ta e="T209" id="Seg_12534" s="T208">accs-inf</ta>
            <ta e="T214" id="Seg_12535" s="T213">new</ta>
            <ta e="T215" id="Seg_12536" s="T214">giv-active</ta>
            <ta e="T217" id="Seg_12537" s="T216">giv-active</ta>
            <ta e="T224" id="Seg_12538" s="T223">accs-inf</ta>
            <ta e="T231" id="Seg_12539" s="T230">0.giv-active</ta>
            <ta e="T238" id="Seg_12540" s="T237">accs-inf</ta>
            <ta e="T239" id="Seg_12541" s="T238">accs-inf</ta>
            <ta e="T241" id="Seg_12542" s="T240">giv-inactive</ta>
            <ta e="T245" id="Seg_12543" s="T244">giv-active</ta>
            <ta e="T248" id="Seg_12544" s="T247">accs-sit</ta>
            <ta e="T255" id="Seg_12545" s="T254">new</ta>
            <ta e="T259" id="Seg_12546" s="T258">giv-active</ta>
            <ta e="T260" id="Seg_12547" s="T259">accs-sit</ta>
            <ta e="T261" id="Seg_12548" s="T260">accs-inf</ta>
            <ta e="T263" id="Seg_12549" s="T262">accs-sit</ta>
            <ta e="T269" id="Seg_12550" s="T268">accs-inf</ta>
            <ta e="T271" id="Seg_12551" s="T270">giv-inactive</ta>
            <ta e="T272" id="Seg_12552" s="T271">accs-inf</ta>
            <ta e="T282" id="Seg_12553" s="T281">giv-inactive</ta>
            <ta e="T285" id="Seg_12554" s="T284">giv-inactive</ta>
            <ta e="T289" id="Seg_12555" s="T288">accs-gen</ta>
            <ta e="T290" id="Seg_12556" s="T289">accs-gen</ta>
            <ta e="T293" id="Seg_12557" s="T292">new</ta>
            <ta e="T294" id="Seg_12558" s="T293">new</ta>
            <ta e="T296" id="Seg_12559" s="T295">giv-inactive</ta>
            <ta e="T299" id="Seg_12560" s="T298">new</ta>
            <ta e="T309" id="Seg_12561" s="T308">new</ta>
            <ta e="T310" id="Seg_12562" s="T309">accs-aggr</ta>
            <ta e="T314" id="Seg_12563" s="T313">giv-inactive</ta>
            <ta e="T316" id="Seg_12564" s="T315">accs-inf</ta>
            <ta e="T318" id="Seg_12565" s="T317">giv-inactive</ta>
            <ta e="T322" id="Seg_12566" s="T321">accs-sit</ta>
            <ta e="T332" id="Seg_12567" s="T331">0.giv-active</ta>
            <ta e="T335" id="Seg_12568" s="T334">giv-inactive</ta>
            <ta e="T337" id="Seg_12569" s="T336">giv-inactive</ta>
            <ta e="T339" id="Seg_12570" s="T338">giv-inactive</ta>
            <ta e="T343" id="Seg_12571" s="T342">giv-inactive</ta>
            <ta e="T345" id="Seg_12572" s="T344">giv-inactive</ta>
            <ta e="T349" id="Seg_12573" s="T348">giv-inactive</ta>
            <ta e="T351" id="Seg_12574" s="T350">accs-inf</ta>
            <ta e="T353" id="Seg_12575" s="T352">giv-active</ta>
            <ta e="T357" id="Seg_12576" s="T356">giv-inactive</ta>
            <ta e="T360" id="Seg_12577" s="T359">accs-inf</ta>
            <ta e="T363" id="Seg_12578" s="T362">0.giv-active</ta>
            <ta e="T364" id="Seg_12579" s="T363">giv-inactive</ta>
            <ta e="T365" id="Seg_12580" s="T364">giv-inactive</ta>
            <ta e="T368" id="Seg_12581" s="T367">accs-inf</ta>
            <ta e="T371" id="Seg_12582" s="T370">giv-inactive</ta>
            <ta e="T377" id="Seg_12583" s="T376">giv-active</ta>
            <ta e="T380" id="Seg_12584" s="T379">giv-inactive</ta>
            <ta e="T388" id="Seg_12585" s="T387">giv-active</ta>
            <ta e="T390" id="Seg_12586" s="T389">giv-inactive</ta>
            <ta e="T394" id="Seg_12587" s="T393">giv-inactive</ta>
            <ta e="T397" id="Seg_12588" s="T396">accs-inf</ta>
            <ta e="T398" id="Seg_12589" s="T397">accs-inf</ta>
            <ta e="T403" id="Seg_12590" s="T402">accs-inf</ta>
            <ta e="T404" id="Seg_12591" s="T403">accs-inf</ta>
            <ta e="T409" id="Seg_12592" s="T408">accs-inf</ta>
            <ta e="T412" id="Seg_12593" s="T411">giv-inactive</ta>
            <ta e="T415" id="Seg_12594" s="T414">giv-inactive</ta>
            <ta e="T416" id="Seg_12595" s="T415">giv-inactive</ta>
            <ta e="T417" id="Seg_12596" s="T416">accs-inf</ta>
            <ta e="T421" id="Seg_12597" s="T420">0.giv-active</ta>
            <ta e="T424" id="Seg_12598" s="T423">accs-inf</ta>
            <ta e="T425" id="Seg_12599" s="T424">accs-inf</ta>
            <ta e="T426" id="Seg_12600" s="T425">0.giv-active</ta>
            <ta e="T431" id="Seg_12601" s="T430">giv-inactive</ta>
            <ta e="T432" id="Seg_12602" s="T431">0.giv-active</ta>
            <ta e="T435" id="Seg_12603" s="T434">giv-inactive</ta>
            <ta e="T436" id="Seg_12604" s="T435">giv-active</ta>
            <ta e="T439" id="Seg_12605" s="T438">giv-inactive</ta>
            <ta e="T447" id="Seg_12606" s="T446">giv-active</ta>
            <ta e="T452" id="Seg_12607" s="T451">accs-inf</ta>
            <ta e="T457" id="Seg_12608" s="T456">giv-active</ta>
            <ta e="T469" id="Seg_12609" s="T468">giv-inactive</ta>
            <ta e="T471" id="Seg_12610" s="T470">giv-inactive</ta>
            <ta e="T472" id="Seg_12611" s="T471">giv-inactive</ta>
            <ta e="T474" id="Seg_12612" s="T473">giv-inactive</ta>
            <ta e="T477" id="Seg_12613" s="T476">giv-inactive</ta>
            <ta e="T479" id="Seg_12614" s="T478">giv-active</ta>
            <ta e="T482" id="Seg_12615" s="T481">giv-inactive</ta>
            <ta e="T483" id="Seg_12616" s="T482">giv-inactive</ta>
            <ta e="T486" id="Seg_12617" s="T485">giv-inactive</ta>
            <ta e="T487" id="Seg_12618" s="T486">giv-inactive</ta>
            <ta e="T492" id="Seg_12619" s="T491">giv-active</ta>
            <ta e="T494" id="Seg_12620" s="T493">giv-inactive</ta>
            <ta e="T496" id="Seg_12621" s="T495">giv-inactive</ta>
            <ta e="T498" id="Seg_12622" s="T497">accs-inf</ta>
            <ta e="T506" id="Seg_12623" s="T505">new</ta>
            <ta e="T509" id="Seg_12624" s="T508">accs-inf</ta>
            <ta e="T514" id="Seg_12625" s="T513">giv-inactive</ta>
            <ta e="T518" id="Seg_12626" s="T517">0.giv-active</ta>
            <ta e="T520" id="Seg_12627" s="T519">new</ta>
            <ta e="T521" id="Seg_12628" s="T520">new</ta>
            <ta e="T523" id="Seg_12629" s="T522">new</ta>
            <ta e="T525" id="Seg_12630" s="T524">new</ta>
            <ta e="T527" id="Seg_12631" s="T526">new</ta>
            <ta e="T529" id="Seg_12632" s="T528">new</ta>
            <ta e="T533" id="Seg_12633" s="T532">accs-aggr</ta>
            <ta e="T537" id="Seg_12634" s="T536">accs-gen</ta>
            <ta e="T538" id="Seg_12635" s="T537">accs-inf</ta>
            <ta e="T540" id="Seg_12636" s="T539">giv-active</ta>
            <ta e="T543" id="Seg_12637" s="T542">giv-inactive</ta>
            <ta e="T545" id="Seg_12638" s="T544">giv-inactive</ta>
            <ta e="T547" id="Seg_12639" s="T546">giv-active</ta>
            <ta e="T551" id="Seg_12640" s="T550">giv-active</ta>
            <ta e="T562" id="Seg_12641" s="T561">giv-inactive</ta>
            <ta e="T567" id="Seg_12642" s="T566">new</ta>
            <ta e="T571" id="Seg_12643" s="T570">new</ta>
            <ta e="T577" id="Seg_12644" s="T576">giv-active</ta>
            <ta e="T579" id="Seg_12645" s="T578">accs-inf</ta>
            <ta e="T581" id="Seg_12646" s="T580">giv-inactive</ta>
            <ta e="T585" id="Seg_12647" s="T584">giv-inactive</ta>
            <ta e="T592" id="Seg_12648" s="T591">giv-active</ta>
            <ta e="T599" id="Seg_12649" s="T598">accs-inf</ta>
            <ta e="T600" id="Seg_12650" s="T599">0.giv-active</ta>
            <ta e="T601" id="Seg_12651" s="T600">giv-active</ta>
            <ta e="T603" id="Seg_12652" s="T602">new</ta>
            <ta e="T606" id="Seg_12653" s="T605">giv-inactive</ta>
            <ta e="T607" id="Seg_12654" s="T606">accs-inf</ta>
            <ta e="T612" id="Seg_12655" s="T611">accs-inf</ta>
            <ta e="T620" id="Seg_12656" s="T619">giv-inactive</ta>
            <ta e="T623" id="Seg_12657" s="T622">accs-inf</ta>
            <ta e="T624" id="Seg_12658" s="T623">new</ta>
            <ta e="T628" id="Seg_12659" s="T627">giv-inactive</ta>
            <ta e="T632" id="Seg_12660" s="T631">giv-active</ta>
            <ta e="T636" id="Seg_12661" s="T635">giv-inactive</ta>
            <ta e="T637" id="Seg_12662" s="T636">accs-inf</ta>
            <ta e="T643" id="Seg_12663" s="T642">accs-inf</ta>
            <ta e="T650" id="Seg_12664" s="T649">accs-inf</ta>
            <ta e="T652" id="Seg_12665" s="T651">giv-inactive</ta>
            <ta e="T659" id="Seg_12666" s="T658">giv-active</ta>
            <ta e="T663" id="Seg_12667" s="T662">0.giv-inactive</ta>
            <ta e="T664" id="Seg_12668" s="T663">giv-inactive</ta>
            <ta e="T665" id="Seg_12669" s="T664">giv-active</ta>
            <ta e="T667" id="Seg_12670" s="T666">accs-sit</ta>
            <ta e="T668" id="Seg_12671" s="T667">accs-inf</ta>
            <ta e="T669" id="Seg_12672" s="T668">giv-inactive</ta>
            <ta e="T672" id="Seg_12673" s="T671">accs-inf</ta>
            <ta e="T673" id="Seg_12674" s="T672">new</ta>
            <ta e="T674" id="Seg_12675" s="T673">accs-inf</ta>
            <ta e="T677" id="Seg_12676" s="T676">new</ta>
            <ta e="T682" id="Seg_12677" s="T681">accs-inf</ta>
            <ta e="T686" id="Seg_12678" s="T685">giv-inactive</ta>
            <ta e="T690" id="Seg_12679" s="T689">accs-sit</ta>
            <ta e="T692" id="Seg_12680" s="T691">accs-sit</ta>
            <ta e="T693" id="Seg_12681" s="T692">accs-sit</ta>
            <ta e="T694" id="Seg_12682" s="T693">accs-sit</ta>
            <ta e="T699" id="Seg_12683" s="T698">giv-inactive</ta>
            <ta e="T700" id="Seg_12684" s="T699">accs-inf</ta>
            <ta e="T705" id="Seg_12685" s="T704">giv-inactive</ta>
            <ta e="T708" id="Seg_12686" s="T707">giv-inactive</ta>
            <ta e="T712" id="Seg_12687" s="T711">giv-inactive</ta>
            <ta e="T716" id="Seg_12688" s="T715">accs-inf</ta>
            <ta e="T718" id="Seg_12689" s="T717">giv-active</ta>
            <ta e="T720" id="Seg_12690" s="T719">accs-inf</ta>
            <ta e="T737" id="Seg_12691" s="T736">giv-inactive</ta>
            <ta e="T740" id="Seg_12692" s="T739">accs-inf</ta>
            <ta e="T742" id="Seg_12693" s="T741">giv-inactive</ta>
            <ta e="T743" id="Seg_12694" s="T742">giv-inactive</ta>
            <ta e="T750" id="Seg_12695" s="T749">giv-inactive</ta>
            <ta e="T753" id="Seg_12696" s="T752">giv-inactive</ta>
            <ta e="T755" id="Seg_12697" s="T754">giv-inactive</ta>
            <ta e="T761" id="Seg_12698" s="T760">giv-inactive</ta>
            <ta e="T764" id="Seg_12699" s="T763">accs-inf</ta>
            <ta e="T767" id="Seg_12700" s="T766">accs-inf</ta>
            <ta e="T769" id="Seg_12701" s="T768">accs-inf</ta>
            <ta e="T775" id="Seg_12702" s="T774">giv-inactive</ta>
            <ta e="T782" id="Seg_12703" s="T781">accs-inf</ta>
            <ta e="T783" id="Seg_12704" s="T782">accs-inf</ta>
            <ta e="T785" id="Seg_12705" s="T784">0.giv-active</ta>
            <ta e="T786" id="Seg_12706" s="T785">giv-active</ta>
            <ta e="T787" id="Seg_12707" s="T786">0.giv-active</ta>
            <ta e="T788" id="Seg_12708" s="T787">0.giv-active</ta>
            <ta e="T793" id="Seg_12709" s="T792">0.giv-active</ta>
            <ta e="T794" id="Seg_12710" s="T793">new</ta>
            <ta e="T797" id="Seg_12711" s="T796">accs-gen</ta>
            <ta e="T807" id="Seg_12712" s="T806">0.giv-active</ta>
            <ta e="T808" id="Seg_12713" s="T807">giv-inactive</ta>
            <ta e="T810" id="Seg_12714" s="T809">0.giv-active</ta>
            <ta e="T811" id="Seg_12715" s="T810">giv-inactive</ta>
            <ta e="T813" id="Seg_12716" s="T812">0.giv-active</ta>
            <ta e="T816" id="Seg_12717" s="T815">accs-inf</ta>
            <ta e="T821" id="Seg_12718" s="T820">giv-inactive</ta>
            <ta e="T822" id="Seg_12719" s="T821">giv-inactive</ta>
            <ta e="T823" id="Seg_12720" s="T822">0.giv-active</ta>
            <ta e="T827" id="Seg_12721" s="T826">0.giv-active</ta>
            <ta e="T832" id="Seg_12722" s="T831">0.giv-active</ta>
            <ta e="T836" id="Seg_12723" s="T835">giv-active</ta>
            <ta e="T847" id="Seg_12724" s="T846">giv-inactive</ta>
            <ta e="T848" id="Seg_12725" s="T847">giv-active</ta>
            <ta e="T849" id="Seg_12726" s="T848">accs-inf</ta>
            <ta e="T852" id="Seg_12727" s="T851">accs-inf</ta>
            <ta e="T855" id="Seg_12728" s="T854">giv-active</ta>
            <ta e="T856" id="Seg_12729" s="T855">accs-inf</ta>
            <ta e="T857" id="Seg_12730" s="T856">accs-inf</ta>
            <ta e="T859" id="Seg_12731" s="T858">giv-active</ta>
            <ta e="T862" id="Seg_12732" s="T861">giv-inactive</ta>
            <ta e="T864" id="Seg_12733" s="T863">accs-gen</ta>
            <ta e="T866" id="Seg_12734" s="T865">accs-inf</ta>
            <ta e="T868" id="Seg_12735" s="T867">giv-active</ta>
            <ta e="T869" id="Seg_12736" s="T868">accs-gen</ta>
            <ta e="T870" id="Seg_12737" s="T869">accs-inf</ta>
            <ta e="T872" id="Seg_12738" s="T871">accs-gen</ta>
            <ta e="T874" id="Seg_12739" s="T873">giv-active</ta>
            <ta e="T875" id="Seg_12740" s="T874">giv-active</ta>
            <ta e="T878" id="Seg_12741" s="T877">new</ta>
            <ta e="T885" id="Seg_12742" s="T884">new</ta>
            <ta e="T892" id="Seg_12743" s="T891">giv-inactive</ta>
            <ta e="T894" id="Seg_12744" s="T893">giv-inactive</ta>
            <ta e="T896" id="Seg_12745" s="T895">accs-sit</ta>
            <ta e="T897" id="Seg_12746" s="T896">accs-inf</ta>
            <ta e="T910" id="Seg_12747" s="T909">giv-inactive</ta>
            <ta e="T913" id="Seg_12748" s="T912">giv-inactive</ta>
            <ta e="T914" id="Seg_12749" s="T913">giv-inactive</ta>
            <ta e="T915" id="Seg_12750" s="T914">accs-inf</ta>
            <ta e="T918" id="Seg_12751" s="T917">giv-active</ta>
            <ta e="T922" id="Seg_12752" s="T921">giv-active</ta>
            <ta e="T923" id="Seg_12753" s="T922">giv-active</ta>
            <ta e="T925" id="Seg_12754" s="T924">giv-active</ta>
            <ta e="T926" id="Seg_12755" s="T925">accs-inf</ta>
            <ta e="T930" id="Seg_12756" s="T929">accs-gen</ta>
            <ta e="T934" id="Seg_12757" s="T933">giv-inactive</ta>
            <ta e="T936" id="Seg_12758" s="T935">giv-inactive</ta>
            <ta e="T938" id="Seg_12759" s="T937">quot-sp</ta>
            <ta e="T940" id="Seg_12760" s="T939">giv-inactive-Q</ta>
            <ta e="T943" id="Seg_12761" s="T942">accs-gen-Q</ta>
            <ta e="T949" id="Seg_12762" s="T948">new-Q</ta>
            <ta e="T951" id="Seg_12763" s="T950">accs-inf-Q</ta>
         </annotation>
         <annotation name="Top" tierref="Top">
            <ta e="T1" id="Seg_12764" s="T0">top.int.concr</ta>
            <ta e="T27" id="Seg_12765" s="T26">0.top.int.concr</ta>
            <ta e="T39" id="Seg_12766" s="T31">top.int.concr</ta>
            <ta e="T49" id="Seg_12767" s="T48">top.int.concr</ta>
            <ta e="T58" id="Seg_12768" s="T57">top.int.concr</ta>
            <ta e="T85" id="Seg_12769" s="T83">top.int.concr</ta>
            <ta e="T119" id="Seg_12770" s="T117">top.int.concr</ta>
            <ta e="T137" id="Seg_12771" s="T135">top.int.concr</ta>
            <ta e="T161" id="Seg_12772" s="T152">top.int.concr</ta>
            <ta e="T165" id="Seg_12773" s="T161">top.int.concr</ta>
            <ta e="T178" id="Seg_12774" s="T165">top.int.concr</ta>
            <ta e="T192" id="Seg_12775" s="T191">top.int.concr</ta>
            <ta e="T199" id="Seg_12776" s="T196">top.int.concr</ta>
            <ta e="T202" id="Seg_12777" s="T199">top.int.concr</ta>
            <ta e="T215" id="Seg_12778" s="T214">top.int.concr</ta>
            <ta e="T231" id="Seg_12779" s="T230">0.top.int.concr</ta>
            <ta e="T243" id="Seg_12780" s="T239">top.int.concr</ta>
            <ta e="T247" id="Seg_12781" s="T243">top.int.concr</ta>
            <ta e="T259" id="Seg_12782" s="T257">top.int.concr</ta>
            <ta e="T269" id="Seg_12783" s="T268">top.int.concr</ta>
            <ta e="T281" id="Seg_12784" s="T279">top.int.concr</ta>
            <ta e="T311" id="Seg_12785" s="T309">top.int.concr</ta>
            <ta e="T318" id="Seg_12786" s="T316">top.int.concr</ta>
            <ta e="T332" id="Seg_12787" s="T331">0.top.int.concr</ta>
            <ta e="T336" id="Seg_12788" s="T332">top.int.concr</ta>
            <ta e="T343" id="Seg_12789" s="T341">top.int.concr</ta>
            <ta e="T365" id="Seg_12790" s="T363">top.int.concr</ta>
            <ta e="T377" id="Seg_12791" s="T376">top.int.concr</ta>
            <ta e="T388" id="Seg_12792" s="T387">top.int.concr</ta>
            <ta e="T412" id="Seg_12793" s="T411">top.int.concr</ta>
            <ta e="T429" id="Seg_12794" s="T426">top.int.concr</ta>
            <ta e="T435" id="Seg_12795" s="T432">top.int.concr</ta>
            <ta e="T481" id="Seg_12796" s="T478">top.int.concr</ta>
            <ta e="T493" id="Seg_12797" s="T490">top.int.concr</ta>
            <ta e="T512" id="Seg_12798" s="T510">top.int.concr</ta>
            <ta e="T540" id="Seg_12799" s="T538">top.int.concr</ta>
            <ta e="T551" id="Seg_12800" s="T549">top.int.concr</ta>
            <ta e="T561" id="Seg_12801" s="T558">top.int.concr</ta>
            <ta e="T569" id="Seg_12802" s="T568">0.top.int.abstr</ta>
            <ta e="T581" id="Seg_12803" s="T575">top.int.concr</ta>
            <ta e="T591" id="Seg_12804" s="T589">top.int.concr</ta>
            <ta e="T600" id="Seg_12805" s="T599">0.top.int.concr</ta>
            <ta e="T603" id="Seg_12806" s="T600">top.int.concr</ta>
            <ta e="T618" id="Seg_12807" s="T616">top.int.concr</ta>
            <ta e="T628" id="Seg_12808" s="T626">top.int.concr</ta>
            <ta e="T690" id="Seg_12809" s="T689">top.int.concr</ta>
            <ta e="T692" id="Seg_12810" s="T690">top.int.concr</ta>
            <ta e="T693" id="Seg_12811" s="T692">top.int.concr</ta>
            <ta e="T694" id="Seg_12812" s="T693">top.int.concr</ta>
            <ta e="T700" id="Seg_12813" s="T694">top.int.concr</ta>
            <ta e="T705" id="Seg_12814" s="T702">top.int.concr</ta>
            <ta e="T752" id="Seg_12815" s="T749">top.int.concr</ta>
            <ta e="T761" id="Seg_12816" s="T760">top.int.concr</ta>
            <ta e="T775" id="Seg_12817" s="T765">top.int.concr</ta>
            <ta e="T785" id="Seg_12818" s="T784">0.top.int.concr</ta>
            <ta e="T803" id="Seg_12819" s="T800">top.int.concr</ta>
            <ta e="T810" id="Seg_12820" s="T809">0.top.int.concr</ta>
            <ta e="T813" id="Seg_12821" s="T812">0.top.int.concr</ta>
            <ta e="T816" id="Seg_12822" s="T813">top.int.concr</ta>
            <ta e="T827" id="Seg_12823" s="T826">0.top.int.concr</ta>
            <ta e="T832" id="Seg_12824" s="T831">0.top.int.concr</ta>
            <ta e="T856" id="Seg_12825" s="T854">top.int.concr</ta>
            <ta e="T857" id="Seg_12826" s="T856">top.int.concr</ta>
            <ta e="T872" id="Seg_12827" s="T861">top.int.concr</ta>
            <ta e="T897" id="Seg_12828" s="T896">top.int.concr</ta>
            <ta e="T906" id="Seg_12829" s="T904">top.int.concr</ta>
            <ta e="T915" id="Seg_12830" s="T912">top.int.concr</ta>
            <ta e="T923" id="Seg_12831" s="T921">top.int.concr</ta>
            <ta e="T936" id="Seg_12832" s="T935">top.int.concr</ta>
            <ta e="T940" id="Seg_12833" s="T939">top.int.concr</ta>
         </annotation>
         <annotation name="Foc" tierref="Foc">
            <ta e="T11" id="Seg_12834" s="T1">foc.int</ta>
            <ta e="T31" id="Seg_12835" s="T18">foc.int</ta>
            <ta e="T43" id="Seg_12836" s="T39">foc.int</ta>
            <ta e="T57" id="Seg_12837" s="T51">foc.nar</ta>
            <ta e="T81" id="Seg_12838" s="T57">foc.wid</ta>
            <ta e="T89" id="Seg_12839" s="T85">foc.int</ta>
            <ta e="T102" id="Seg_12840" s="T96">foc.wid</ta>
            <ta e="T135" id="Seg_12841" s="T119">foc.int</ta>
            <ta e="T142" id="Seg_12842" s="T139">foc.nar</ta>
            <ta e="T144" id="Seg_12843" s="T142">foc.nar</ta>
            <ta e="T146" id="Seg_12844" s="T144">foc.nar</ta>
            <ta e="T183" id="Seg_12845" s="T181">foc.int</ta>
            <ta e="T196" id="Seg_12846" s="T192">foc.int</ta>
            <ta e="T214" id="Seg_12847" s="T197">foc.wid</ta>
            <ta e="T226" id="Seg_12848" s="T215">foc.int</ta>
            <ta e="T231" id="Seg_12849" s="T227">foc.int</ta>
            <ta e="T257" id="Seg_12850" s="T253">foc.wid</ta>
            <ta e="T266" id="Seg_12851" s="T260">foc.int</ta>
            <ta e="T279" id="Seg_12852" s="T269">foc.int</ta>
            <ta e="T288" id="Seg_12853" s="T281">foc.int</ta>
            <ta e="T316" id="Seg_12854" s="T311">foc.int</ta>
            <ta e="T325" id="Seg_12855" s="T318">foc.int</ta>
            <ta e="T332" id="Seg_12856" s="T325">foc.int</ta>
            <ta e="T341" id="Seg_12857" s="T339">foc.int</ta>
            <ta e="T346" id="Seg_12858" s="T345">foc.int</ta>
            <ta e="T376" id="Seg_12859" s="T365">foc.int</ta>
            <ta e="T387" id="Seg_12860" s="T381">foc.int</ta>
            <ta e="T394" id="Seg_12861" s="T392">foc.nar</ta>
            <ta e="T426" id="Seg_12862" s="T421">foc.int</ta>
            <ta e="T432" id="Seg_12863" s="T429">foc.int</ta>
            <ta e="T452" id="Seg_12864" s="T448">foc.nar</ta>
            <ta e="T490" id="Seg_12865" s="T483">foc.int</ta>
            <ta e="T510" id="Seg_12866" s="T494">foc.int</ta>
            <ta e="T520" id="Seg_12867" s="T518">foc.nar</ta>
            <ta e="T549" id="Seg_12868" s="T546">foc.nar</ta>
            <ta e="T558" id="Seg_12869" s="T551">foc.int</ta>
            <ta e="T564" id="Seg_12870" s="T562">foc.int</ta>
            <ta e="T575" id="Seg_12871" s="T564">foc.wid</ta>
            <ta e="T589" id="Seg_12872" s="T581">foc.int</ta>
            <ta e="T597" id="Seg_12873" s="T592">foc.int</ta>
            <ta e="T600" id="Seg_12874" s="T597">foc.int</ta>
            <ta e="T616" id="Seg_12875" s="T613">foc.int</ta>
            <ta e="T626" id="Seg_12876" s="T620">foc.int</ta>
            <ta e="T634" id="Seg_12877" s="T628">foc.int</ta>
            <ta e="T702" id="Seg_12878" s="T700">foc.int</ta>
            <ta e="T715" id="Seg_12879" s="T710">foc.int</ta>
            <ta e="T759" id="Seg_12880" s="T753">foc.int</ta>
            <ta e="T765" id="Seg_12881" s="T761">foc.int</ta>
            <ta e="T779" id="Seg_12882" s="T775">foc.int</ta>
            <ta e="T785" id="Seg_12883" s="T779">foc.int</ta>
            <ta e="T807" id="Seg_12884" s="T803">foc.int</ta>
            <ta e="T810" id="Seg_12885" s="T807">foc.int</ta>
            <ta e="T813" id="Seg_12886" s="T810">foc.int</ta>
            <ta e="T823" id="Seg_12887" s="T816">foc.int</ta>
            <ta e="T827" id="Seg_12888" s="T823">foc.int</ta>
            <ta e="T832" id="Seg_12889" s="T827">foc.int</ta>
            <ta e="T861" id="Seg_12890" s="T857">foc.int</ta>
            <ta e="T882" id="Seg_12891" s="T872">foc.int</ta>
            <ta e="T890" id="Seg_12892" s="T884">foc.nar</ta>
            <ta e="T904" id="Seg_12893" s="T897">foc.int</ta>
            <ta e="T912" id="Seg_12894" s="T908">foc.int</ta>
            <ta e="T921" id="Seg_12895" s="T915">foc.int</ta>
            <ta e="T935" id="Seg_12896" s="T923">foc.int</ta>
            <ta e="T939" id="Seg_12897" s="T936">foc.int</ta>
            <ta e="T944" id="Seg_12898" s="T940">foc.int</ta>
            <ta e="T951" id="Seg_12899" s="T944">foc.wid</ta>
         </annotation>
         <annotation name="BOR" tierref="BOR">
            <ta e="T6" id="Seg_12900" s="T5">RUS:cult</ta>
            <ta e="T13" id="Seg_12901" s="T12">RUS:cult</ta>
            <ta e="T29" id="Seg_12902" s="T27">RUS:cult</ta>
            <ta e="T34" id="Seg_12903" s="T33">RUS:cult</ta>
            <ta e="T42" id="Seg_12904" s="T41">RUS:cult</ta>
            <ta e="T52" id="Seg_12905" s="T51">RUS:cult</ta>
            <ta e="T55" id="Seg_12906" s="T54">RUS:cult</ta>
            <ta e="T56" id="Seg_12907" s="T55">RUS:cult</ta>
            <ta e="T57" id="Seg_12908" s="T56">RUS:cult</ta>
            <ta e="T59" id="Seg_12909" s="T58">RUS:cult</ta>
            <ta e="T72" id="Seg_12910" s="T71">RUS:cult</ta>
            <ta e="T78" id="Seg_12911" s="T77">RUS:cult</ta>
            <ta e="T86" id="Seg_12912" s="T85">RUS:cult</ta>
            <ta e="T87" id="Seg_12913" s="T86">RUS:cult</ta>
            <ta e="T92" id="Seg_12914" s="T91">RUS:cult</ta>
            <ta e="T97" id="Seg_12915" s="T96">RUS:cult</ta>
            <ta e="T98" id="Seg_12916" s="T97">RUS:cult</ta>
            <ta e="T113" id="Seg_12917" s="T112">RUS:cult</ta>
            <ta e="T115" id="Seg_12918" s="T114">RUS:cult</ta>
            <ta e="T119" id="Seg_12919" s="T118">RUS:cult</ta>
            <ta e="T120" id="Seg_12920" s="T119">RUS:cult</ta>
            <ta e="T132" id="Seg_12921" s="T131">RUS:cult</ta>
            <ta e="T133" id="Seg_12922" s="T132">RUS:cult</ta>
            <ta e="T158" id="Seg_12923" s="T157">RUS:cult</ta>
            <ta e="T164" id="Seg_12924" s="T163">EV:core</ta>
            <ta e="T169" id="Seg_12925" s="T168">RUS:cult</ta>
            <ta e="T170" id="Seg_12926" s="T169">RUS:cult</ta>
            <ta e="T171" id="Seg_12927" s="T170">RUS:cult</ta>
            <ta e="T193" id="Seg_12928" s="T192">RUS:cult</ta>
            <ta e="T199" id="Seg_12929" s="T198">EV:gram (DIM)</ta>
            <ta e="T208" id="Seg_12930" s="T207">RUS:cult</ta>
            <ta e="T214" id="Seg_12931" s="T213">RUS:cult</ta>
            <ta e="T224" id="Seg_12932" s="T223">RUS:cult</ta>
            <ta e="T229" id="Seg_12933" s="T228">RUS:mod</ta>
            <ta e="T234" id="Seg_12934" s="T233">RUS:cult</ta>
            <ta e="T239" id="Seg_12935" s="T238">RUS:cult</ta>
            <ta e="T241" id="Seg_12936" s="T240">RUS:cult</ta>
            <ta e="T250" id="Seg_12937" s="T249">EV:gram (INTNS)</ta>
            <ta e="T255" id="Seg_12938" s="T254">RUS:core</ta>
            <ta e="T259" id="Seg_12939" s="T258">RUS:core</ta>
            <ta e="T273" id="Seg_12940" s="T272">EV:gram (INTNS)</ta>
            <ta e="T293" id="Seg_12941" s="T292">RUS:cult</ta>
            <ta e="T294" id="Seg_12942" s="T293">RUS:cult</ta>
            <ta e="T300" id="Seg_12943" s="T299">RUS:cult</ta>
            <ta e="T301" id="Seg_12944" s="T300">RUS:cult</ta>
            <ta e="T314" id="Seg_12945" s="T313">RUS:cult</ta>
            <ta e="T317" id="Seg_12946" s="T316">RUS:cult</ta>
            <ta e="T318" id="Seg_12947" s="T317">RUS:cult</ta>
            <ta e="T326" id="Seg_12948" s="T325">EV:gram (INTNS)</ta>
            <ta e="T329" id="Seg_12949" s="T328">RUS:cult</ta>
            <ta e="T337" id="Seg_12950" s="T336">RUS:cult</ta>
            <ta e="T342" id="Seg_12951" s="T341">RUS:cult</ta>
            <ta e="T343" id="Seg_12952" s="T342">RUS:cult</ta>
            <ta e="T349" id="Seg_12953" s="T348">RUS:cult</ta>
            <ta e="T351" id="Seg_12954" s="T350">RUS:cult</ta>
            <ta e="T365" id="Seg_12955" s="T364">RUS:cult</ta>
            <ta e="T371" id="Seg_12956" s="T370">RUS:cult</ta>
            <ta e="T402" id="Seg_12957" s="T401">RUS:cult</ta>
            <ta e="T407" id="Seg_12958" s="T406">EV:core</ta>
            <ta e="T416" id="Seg_12959" s="T415">RUS:cult</ta>
            <ta e="T418" id="Seg_12960" s="T417">RUS:cult</ta>
            <ta e="T419" id="Seg_12961" s="T418">RUS:cult</ta>
            <ta e="T426" id="Seg_12962" s="T425">EV:core</ta>
            <ta e="T439" id="Seg_12963" s="T438">RUS:cult</ta>
            <ta e="T440" id="Seg_12964" s="T439">RUS:cult</ta>
            <ta e="T455" id="Seg_12965" s="T454">RUS:mod</ta>
            <ta e="T458" id="Seg_12966" s="T457">EV:core</ta>
            <ta e="T471" id="Seg_12967" s="T470">RUS:cult</ta>
            <ta e="T479" id="Seg_12968" s="T478">RUS:cult</ta>
            <ta e="T483" id="Seg_12969" s="T482">RUS:cult</ta>
            <ta e="T486" id="Seg_12970" s="T485">RUS:cult</ta>
            <ta e="T487" id="Seg_12971" s="T486">RUS:cult</ta>
            <ta e="T492" id="Seg_12972" s="T491">RUS:cult</ta>
            <ta e="T495" id="Seg_12973" s="T494">RUS:gram</ta>
            <ta e="T501" id="Seg_12974" s="T500">RUS:cult</ta>
            <ta e="T508" id="Seg_12975" s="T507">RUS:cult</ta>
            <ta e="T513" id="Seg_12976" s="T512">RUS:cult</ta>
            <ta e="T514" id="Seg_12977" s="T513">RUS:cult</ta>
            <ta e="T521" id="Seg_12978" s="T520">RUS:cult</ta>
            <ta e="T523" id="Seg_12979" s="T522">RUS:cult</ta>
            <ta e="T525" id="Seg_12980" s="T524">RUS:cult</ta>
            <ta e="T527" id="Seg_12981" s="T526">RUS:cult</ta>
            <ta e="T529" id="Seg_12982" s="T528">RUS:cult</ta>
            <ta e="T533" id="Seg_12983" s="T532">RUS:cult</ta>
            <ta e="T535" id="Seg_12984" s="T534">RUS:cult</ta>
            <ta e="T540" id="Seg_12985" s="T539">RUS:cult</ta>
            <ta e="T545" id="Seg_12986" s="T544">RUS:cult</ta>
            <ta e="T547" id="Seg_12987" s="T546">RUS:cult</ta>
            <ta e="T548" id="Seg_12988" s="T547">RUS:cult</ta>
            <ta e="T551" id="Seg_12989" s="T550">RUS:cult</ta>
            <ta e="T568" id="Seg_12990" s="T567">RUS:mod</ta>
            <ta e="T571" id="Seg_12991" s="T570">RUS:cult</ta>
            <ta e="T581" id="Seg_12992" s="T580">RUS:cult</ta>
            <ta e="T603" id="Seg_12993" s="T602">RUS:cult</ta>
            <ta e="T606" id="Seg_12994" s="T605">RUS:cult</ta>
            <ta e="T607" id="Seg_12995" s="T606">RUS:cult</ta>
            <ta e="T612" id="Seg_12996" s="T611">RUS:cult</ta>
            <ta e="T624" id="Seg_12997" s="T623">RUS:cult</ta>
            <ta e="T628" id="Seg_12998" s="T627">RUS:cult</ta>
            <ta e="T630" id="Seg_12999" s="T629">RUS:gram</ta>
            <ta e="T632" id="Seg_13000" s="T631">RUS:cult</ta>
            <ta e="T650" id="Seg_13001" s="T649">RUS:cult</ta>
            <ta e="T665" id="Seg_13002" s="T664">RUS:cult</ta>
            <ta e="T669" id="Seg_13003" s="T668">RUS:cult</ta>
            <ta e="T673" id="Seg_13004" s="T672">RUS:cult</ta>
            <ta e="T677" id="Seg_13005" s="T676">RUS:cult</ta>
            <ta e="T686" id="Seg_13006" s="T685">RUS:cult</ta>
            <ta e="T703" id="Seg_13007" s="T702">RUS:cult</ta>
            <ta e="T725" id="Seg_13008" s="T724">RUS:cult</ta>
            <ta e="T755" id="Seg_13009" s="T754">RUS:cult</ta>
            <ta e="T768" id="Seg_13010" s="T767">RUS:cult</ta>
            <ta e="T770" id="Seg_13011" s="T769">RUS:cult</ta>
            <ta e="T777" id="Seg_13012" s="T776">EV:core</ta>
            <ta e="T794" id="Seg_13013" s="T793">RUS:cult</ta>
            <ta e="T796" id="Seg_13014" s="T795">RUS:cult</ta>
            <ta e="T797" id="Seg_13015" s="T796">RUS:cult</ta>
            <ta e="T811" id="Seg_13016" s="T810">RUS:cult</ta>
            <ta e="T812" id="Seg_13017" s="T811">RUS:mod</ta>
            <ta e="T827" id="Seg_13018" s="T826">EV:core</ta>
            <ta e="T844" id="Seg_13019" s="T843">RUS:gram</ta>
            <ta e="T863" id="Seg_13020" s="T862">RUS:cult</ta>
            <ta e="T864" id="Seg_13021" s="T863">RUS:cult</ta>
            <ta e="T866" id="Seg_13022" s="T865">RUS:cult</ta>
            <ta e="T869" id="Seg_13023" s="T868">RUS:cult</ta>
            <ta e="T872" id="Seg_13024" s="T871">RUS:cult</ta>
            <ta e="T875" id="Seg_13025" s="T874">RUS:cult</ta>
            <ta e="T878" id="Seg_13026" s="T877">RUS:cult</ta>
            <ta e="T884" id="Seg_13027" s="T883">RUS:mod</ta>
            <ta e="T885" id="Seg_13028" s="T884">RUS:cult</ta>
            <ta e="T893" id="Seg_13029" s="T892">RUS:cult</ta>
            <ta e="T930" id="Seg_13030" s="T929">RUS:cult</ta>
            <ta e="T932" id="Seg_13031" s="T931">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon">
            <ta e="T6" id="Seg_13032" s="T5">Csub Vsub</ta>
            <ta e="T34" id="Seg_13033" s="T33">Csub Vsub</ta>
            <ta e="T57" id="Seg_13034" s="T56">finCdel</ta>
            <ta e="T59" id="Seg_13035" s="T58">lenition</ta>
            <ta e="T78" id="Seg_13036" s="T77">fortition Vsub Vsub</ta>
            <ta e="T97" id="Seg_13037" s="T96">lenition</ta>
            <ta e="T115" id="Seg_13038" s="T114">Vsub fortition</ta>
            <ta e="T119" id="Seg_13039" s="T118">finCdel</ta>
            <ta e="T132" id="Seg_13040" s="T131">lenition</ta>
            <ta e="T158" id="Seg_13041" s="T157">Csub Vsub</ta>
            <ta e="T164" id="Seg_13042" s="T163">fortition</ta>
            <ta e="T170" id="Seg_13043" s="T169">finVins</ta>
            <ta e="T171" id="Seg_13044" s="T170">Vsub</ta>
            <ta e="T193" id="Seg_13045" s="T192">Vsub</ta>
            <ta e="T208" id="Seg_13046" s="T207">lenition</ta>
            <ta e="T229" id="Seg_13047" s="T228">Vsub Csub Vsub</ta>
            <ta e="T294" id="Seg_13048" s="T293">finVins</ta>
            <ta e="T314" id="Seg_13049" s="T313">lenition</ta>
            <ta e="T317" id="Seg_13050" s="T316">lenition</ta>
            <ta e="T329" id="Seg_13051" s="T328">fortition medVins Vsub</ta>
            <ta e="T342" id="Seg_13052" s="T341">lenition</ta>
            <ta e="T349" id="Seg_13053" s="T348">lenition</ta>
            <ta e="T371" id="Seg_13054" s="T370">lenition</ta>
            <ta e="T402" id="Seg_13055" s="T401">Csub Vsub</ta>
            <ta e="T407" id="Seg_13056" s="T406">fortition</ta>
            <ta e="T426" id="Seg_13057" s="T425">fortition</ta>
            <ta e="T439" id="Seg_13058" s="T438">finCdel</ta>
            <ta e="T440" id="Seg_13059" s="T439">Csub Vsub</ta>
            <ta e="T455" id="Seg_13060" s="T454">Vsub Csub Vsub</ta>
            <ta e="T458" id="Seg_13061" s="T457">fortition</ta>
            <ta e="T486" id="Seg_13062" s="T485">lenition</ta>
            <ta e="T495" id="Seg_13063" s="T494">Vsub</ta>
            <ta e="T501" id="Seg_13064" s="T500">Csub Vsub</ta>
            <ta e="T513" id="Seg_13065" s="T512">lenition</ta>
            <ta e="T533" id="Seg_13066" s="T532">Vsub fortition</ta>
            <ta e="T535" id="Seg_13067" s="T534">lenition</ta>
            <ta e="T540" id="Seg_13068" s="T539">Vsub fortition</ta>
            <ta e="T545" id="Seg_13069" s="T544">lenition</ta>
            <ta e="T548" id="Seg_13070" s="T547">Vsub fortition</ta>
            <ta e="T551" id="Seg_13071" s="T550">Vsub fortition</ta>
            <ta e="T568" id="Seg_13072" s="T567">Vsub Csub Vsub</ta>
            <ta e="T581" id="Seg_13073" s="T580">lenition</ta>
            <ta e="T603" id="Seg_13074" s="T602">finVins</ta>
            <ta e="T606" id="Seg_13075" s="T605">lenition</ta>
            <ta e="T624" id="Seg_13076" s="T623">finVins</ta>
            <ta e="T628" id="Seg_13077" s="T627">Vsub lenition</ta>
            <ta e="T632" id="Seg_13078" s="T631">finVins</ta>
            <ta e="T669" id="Seg_13079" s="T668">finVins</ta>
            <ta e="T673" id="Seg_13080" s="T672">Vsub fortition</ta>
            <ta e="T677" id="Seg_13081" s="T676">Csub Vsub</ta>
            <ta e="T703" id="Seg_13082" s="T702">lenition</ta>
            <ta e="T725" id="Seg_13083" s="T724">Vsub</ta>
            <ta e="T755" id="Seg_13084" s="T754">fortition</ta>
            <ta e="T770" id="Seg_13085" s="T769">Csub Vsub</ta>
            <ta e="T777" id="Seg_13086" s="T776">fortition</ta>
            <ta e="T796" id="Seg_13087" s="T795">Vsub fortition</ta>
            <ta e="T827" id="Seg_13088" s="T826">fortition</ta>
            <ta e="T884" id="Seg_13089" s="T883">Vsub Csub Vsub</ta>
         </annotation>
         <annotation name="BOR-Morph" tierref="BOR-Morph">
            <ta e="T6" id="Seg_13090" s="T5">dir:bare</ta>
            <ta e="T13" id="Seg_13091" s="T12">dir:bare</ta>
            <ta e="T29" id="Seg_13092" s="T27">dir:bare</ta>
            <ta e="T34" id="Seg_13093" s="T33">dir:bare</ta>
            <ta e="T42" id="Seg_13094" s="T41">dir:bare</ta>
            <ta e="T52" id="Seg_13095" s="T51">dir:bare</ta>
            <ta e="T55" id="Seg_13096" s="T54">dir:infl</ta>
            <ta e="T56" id="Seg_13097" s="T55">indir:bare</ta>
            <ta e="T57" id="Seg_13098" s="T56">dir:infl</ta>
            <ta e="T59" id="Seg_13099" s="T58">dir:bare</ta>
            <ta e="T72" id="Seg_13100" s="T71">dir:infl</ta>
            <ta e="T78" id="Seg_13101" s="T77">dir:infl</ta>
            <ta e="T86" id="Seg_13102" s="T85">indir:bare</ta>
            <ta e="T87" id="Seg_13103" s="T86">dir:bare</ta>
            <ta e="T92" id="Seg_13104" s="T91">indir:bare</ta>
            <ta e="T97" id="Seg_13105" s="T96">dir:bare</ta>
            <ta e="T98" id="Seg_13106" s="T97">dir:infl</ta>
            <ta e="T113" id="Seg_13107" s="T112">dir:bare</ta>
            <ta e="T115" id="Seg_13108" s="T114">dir:infl</ta>
            <ta e="T119" id="Seg_13109" s="T118">dir:infl</ta>
            <ta e="T120" id="Seg_13110" s="T119">indir:bare</ta>
            <ta e="T132" id="Seg_13111" s="T131">dir:bare</ta>
            <ta e="T133" id="Seg_13112" s="T132">dir:infl</ta>
            <ta e="T158" id="Seg_13113" s="T157">dir:bare</ta>
            <ta e="T164" id="Seg_13114" s="T163">dir:bare</ta>
            <ta e="T169" id="Seg_13115" s="T168">dir:infl</ta>
            <ta e="T170" id="Seg_13116" s="T169">dir:infl</ta>
            <ta e="T171" id="Seg_13117" s="T170">dir:infl</ta>
            <ta e="T193" id="Seg_13118" s="T192">indir:infl</ta>
            <ta e="T208" id="Seg_13119" s="T207">dir:bare</ta>
            <ta e="T214" id="Seg_13120" s="T213">dir:infl</ta>
            <ta e="T224" id="Seg_13121" s="T223">dir:infl</ta>
            <ta e="T229" id="Seg_13122" s="T228">dir:bare</ta>
            <ta e="T234" id="Seg_13123" s="T233">dir:bare</ta>
            <ta e="T239" id="Seg_13124" s="T238">dir:infl</ta>
            <ta e="T241" id="Seg_13125" s="T240">dir:bare</ta>
            <ta e="T255" id="Seg_13126" s="T254">dir:bare</ta>
            <ta e="T259" id="Seg_13127" s="T258">dir:infl</ta>
            <ta e="T293" id="Seg_13128" s="T292">dir:infl</ta>
            <ta e="T294" id="Seg_13129" s="T293">dir:infl</ta>
            <ta e="T300" id="Seg_13130" s="T299">dir:bare</ta>
            <ta e="T301" id="Seg_13131" s="T300">dir:bare</ta>
            <ta e="T314" id="Seg_13132" s="T313">dir:infl</ta>
            <ta e="T317" id="Seg_13133" s="T316">dir:bare</ta>
            <ta e="T318" id="Seg_13134" s="T317">dir:infl</ta>
            <ta e="T329" id="Seg_13135" s="T328">dir:infl</ta>
            <ta e="T337" id="Seg_13136" s="T336">indir:bare</ta>
            <ta e="T342" id="Seg_13137" s="T341">dir:bare</ta>
            <ta e="T343" id="Seg_13138" s="T342">dir:infl</ta>
            <ta e="T349" id="Seg_13139" s="T348">dir:infl</ta>
            <ta e="T351" id="Seg_13140" s="T350">dir:infl</ta>
            <ta e="T365" id="Seg_13141" s="T364">dir:infl</ta>
            <ta e="T371" id="Seg_13142" s="T370">dir:infl</ta>
            <ta e="T402" id="Seg_13143" s="T401">dir:bare</ta>
            <ta e="T407" id="Seg_13144" s="T406">dir:bare</ta>
            <ta e="T416" id="Seg_13145" s="T415">indir:bare</ta>
            <ta e="T418" id="Seg_13146" s="T417">parad:bare</ta>
            <ta e="T419" id="Seg_13147" s="T418">dir:infl</ta>
            <ta e="T426" id="Seg_13148" s="T425">dir:infl</ta>
            <ta e="T439" id="Seg_13149" s="T438">dir:infl</ta>
            <ta e="T440" id="Seg_13150" s="T439">dir:infl</ta>
            <ta e="T455" id="Seg_13151" s="T454">dir:bare</ta>
            <ta e="T458" id="Seg_13152" s="T457">dir:bare</ta>
            <ta e="T471" id="Seg_13153" s="T470">indir:bare</ta>
            <ta e="T479" id="Seg_13154" s="T478">indir:infl</ta>
            <ta e="T483" id="Seg_13155" s="T482">dir:infl</ta>
            <ta e="T486" id="Seg_13156" s="T485">dir:infl</ta>
            <ta e="T487" id="Seg_13157" s="T486">dir:bare</ta>
            <ta e="T492" id="Seg_13158" s="T491">indir:bare</ta>
            <ta e="T495" id="Seg_13159" s="T494">dir:bare</ta>
            <ta e="T501" id="Seg_13160" s="T500">dir:bare</ta>
            <ta e="T508" id="Seg_13161" s="T507">dir:bare</ta>
            <ta e="T513" id="Seg_13162" s="T512">dir:bare</ta>
            <ta e="T514" id="Seg_13163" s="T513">dir:infl</ta>
            <ta e="T521" id="Seg_13164" s="T520">dir:bare</ta>
            <ta e="T523" id="Seg_13165" s="T522">dir:bare</ta>
            <ta e="T525" id="Seg_13166" s="T524">dir:bare</ta>
            <ta e="T527" id="Seg_13167" s="T526">dir:bare</ta>
            <ta e="T529" id="Seg_13168" s="T528">dir:bare</ta>
            <ta e="T533" id="Seg_13169" s="T532">dir:infl</ta>
            <ta e="T535" id="Seg_13170" s="T534">dir:bare</ta>
            <ta e="T540" id="Seg_13171" s="T539">dir:infl</ta>
            <ta e="T545" id="Seg_13172" s="T544">dir:infl</ta>
            <ta e="T547" id="Seg_13173" s="T546">dir:bare</ta>
            <ta e="T548" id="Seg_13174" s="T547">dir:bare</ta>
            <ta e="T551" id="Seg_13175" s="T550">dir:bare</ta>
            <ta e="T568" id="Seg_13176" s="T567">dir:bare</ta>
            <ta e="T571" id="Seg_13177" s="T570">dir:bare</ta>
            <ta e="T581" id="Seg_13178" s="T580">dir:infl</ta>
            <ta e="T603" id="Seg_13179" s="T602">dir:infl</ta>
            <ta e="T606" id="Seg_13180" s="T605">dir:infl</ta>
            <ta e="T607" id="Seg_13181" s="T606">dir:infl</ta>
            <ta e="T612" id="Seg_13182" s="T611">dir:infl</ta>
            <ta e="T624" id="Seg_13183" s="T623">dir:infl</ta>
            <ta e="T628" id="Seg_13184" s="T627">dir:bare</ta>
            <ta e="T630" id="Seg_13185" s="T629">dir:bare</ta>
            <ta e="T632" id="Seg_13186" s="T631">dir:infl</ta>
            <ta e="T650" id="Seg_13187" s="T649">dir:infl</ta>
            <ta e="T665" id="Seg_13188" s="T664">dir:infl</ta>
            <ta e="T669" id="Seg_13189" s="T668">dir:infl</ta>
            <ta e="T673" id="Seg_13190" s="T672">dir:infl</ta>
            <ta e="T677" id="Seg_13191" s="T676">dir:infl</ta>
            <ta e="T686" id="Seg_13192" s="T685">dir:bare</ta>
            <ta e="T703" id="Seg_13193" s="T702">dir:bare</ta>
            <ta e="T725" id="Seg_13194" s="T724">indir:infl</ta>
            <ta e="T755" id="Seg_13195" s="T754">dir:infl</ta>
            <ta e="T768" id="Seg_13196" s="T767">dir:bare</ta>
            <ta e="T770" id="Seg_13197" s="T769">dir:bare</ta>
            <ta e="T777" id="Seg_13198" s="T776">dir:infl</ta>
            <ta e="T794" id="Seg_13199" s="T793">dir:bare</ta>
            <ta e="T796" id="Seg_13200" s="T795">dir:infl</ta>
            <ta e="T797" id="Seg_13201" s="T796">dir:bare</ta>
            <ta e="T811" id="Seg_13202" s="T810">dir:infl</ta>
            <ta e="T812" id="Seg_13203" s="T811">dir:bare</ta>
            <ta e="T827" id="Seg_13204" s="T826">dir:infl</ta>
            <ta e="T844" id="Seg_13205" s="T843">dir:bare</ta>
            <ta e="T863" id="Seg_13206" s="T862">indir:bare</ta>
            <ta e="T864" id="Seg_13207" s="T863">dir:infl</ta>
            <ta e="T866" id="Seg_13208" s="T865">dir:infl</ta>
            <ta e="T869" id="Seg_13209" s="T868">dir:infl</ta>
            <ta e="T872" id="Seg_13210" s="T871">dir:infl</ta>
            <ta e="T875" id="Seg_13211" s="T874">dir:bare</ta>
            <ta e="T878" id="Seg_13212" s="T877">dir:infl</ta>
            <ta e="T884" id="Seg_13213" s="T883">dir:bare</ta>
            <ta e="T885" id="Seg_13214" s="T884">dir:infl</ta>
            <ta e="T893" id="Seg_13215" s="T892">indir:bare</ta>
            <ta e="T930" id="Seg_13216" s="T929">dir:bare</ta>
            <ta e="T932" id="Seg_13217" s="T931">dir:bare</ta>
         </annotation>
         <annotation name="CS" tierref="CS">
            <ta e="T881" id="Seg_13218" s="T878">RUS:int.ins</ta>
            <ta e="T889" id="Seg_13219" s="T885">RUS:int.ins</ta>
         </annotation>
         <annotation name="fe" tierref="fe">
            <ta e="T11" id="Seg_13220" s="T0">I went to war voluntarily, in the year 1941.</ta>
            <ta e="T16" id="Seg_13221" s="T11">In September, the month when the cold comes.</ta>
            <ta e="T31" id="Seg_13222" s="T16">Having gone in the beginning, I was at first trained for one year and nine months as a soldier, at a place which is called "Far East".</ta>
            <ta e="T48" id="Seg_13223" s="T31">Then in summer 1943 we were sent west, to the place where the acts of war were taking place.</ta>
            <ta e="T57" id="Seg_13224" s="T48">We met the war in a village named Prokhorovoka in the oblast of Kursk.</ta>
            <ta e="T81" id="Seg_13225" s="T57">There the German soldiers had accumulated very many weapons and ammunition, and they had pushed our army to a place located thirty-five versts back.</ta>
            <ta e="T89" id="Seg_13226" s="T81">This place was later called "Curve of Kursk".</ta>
            <ta e="T96" id="Seg_13227" s="T89">Saying it in Dolgan, it would be correct to say "Bending of Kursk".</ta>
            <ta e="T102" id="Seg_13228" s="T96">The commander of the Germany army had apparently an idea.</ta>
            <ta e="T117" id="Seg_13229" s="T102">That is, having fought through this place, to take our capital named Moscow very quickly.</ta>
            <ta e="T135" id="Seg_13230" s="T117">Our entity, having gone into the biggest fire of the war at the "Curve of Kursk", tried for ten days to chase away the German army.</ta>
            <ta e="T142" id="Seg_13231" s="T135">During this time we fought without noticing day and night.</ta>
            <ta e="T146" id="Seg_13232" s="T142">Without sleeping and relaxing.</ta>
            <ta e="T152" id="Seg_13233" s="T146">For example without knowing and hearing anything.</ta>
            <ta e="T184" id="Seg_13234" s="T152">On both fighting sides so many thousands of people got lost, died, so many people were wounded, so many war weapons: airplanes, tanks, guns and other small rifles, the war armory was destroyed, nobody did know especially at that time.</ta>
            <ta e="T196" id="Seg_13235" s="T184">One can understand by means of an example, what huge and strong war was taking place at this place.</ta>
            <ta e="T214" id="Seg_13236" s="T196">At an (icy?) place, at the place where we were, one day around 200 airplanes came flying from the German side.</ta>
            <ta e="T226" id="Seg_13237" s="T214">They released hundreds of bombs over us, as if rain was falling.</ta>
            <ta e="T239" id="Seg_13238" s="T226">Then they additionally shot with such war weapons loaded with small patrons, with machine guns.</ta>
            <ta e="T257" id="Seg_13239" s="T239">After so many bombs had fallen, after so many weapons had shot, the earth wasn't visible at all, black fog arose.</ta>
            <ta e="T266" id="Seg_13240" s="T257">In this fog on could not see the person lying next to oneself.</ta>
            <ta e="T279" id="Seg_13241" s="T266">Sometimes, however, somebody became deaf beacuase of the noise of the war weapons, as if he wouldn't hear anything.</ta>
            <ta e="T288" id="Seg_13242" s="T279">On the tenth day the war was forced in our surrounding.</ta>
            <ta e="T303" id="Seg_13243" s="T288">From the sky, from the earth very many airplanes, tanks [were coming], fire weapons made especially for the war, called "Katyusha" and "Andryusha".</ta>
            <ta e="T309" id="Seg_13244" s="T303">Then individually some small rifles.</ta>
            <ta e="T316" id="Seg_13245" s="T309">Together they all rushed into the places where the Germans were.</ta>
            <ta e="T325" id="Seg_13246" s="T316">The German army, not withstanding such a big attack in waves, went back.</ta>
            <ta e="T332" id="Seg_13247" s="T325">Immediately it rushed back to a place located ten, fifteen verst backwards.</ta>
            <ta e="T341" id="Seg_13248" s="T332">After this big attack the Curve of Kursk was straightened.</ta>
            <ta e="T346" id="Seg_13249" s="T341">We chased back the German army.</ta>
            <ta e="T363" id="Seg_13250" s="T346">After some bad soldiers of the Germans had surrendered to us, they didn't find their mind and reason until the end of the war, they fled.</ta>
            <ta e="T376" id="Seg_13251" s="T363">After our army had assembled its forces very well, it chased back the Germans.</ta>
            <ta e="T387" id="Seg_13252" s="T376">On later days, while I was in the war, such terrible humans (…) I didn't see.</ta>
            <ta e="T400" id="Seg_13253" s="T387">At this place I saw a hot battle for the first time: how the blood of the soldier's was poured out like a river,</ta>
            <ta e="T405" id="Seg_13254" s="T400">how so many thousands of people stopped breathing,</ta>
            <ta e="T411" id="Seg_13255" s="T405">how so many wounded people suffered a lot.</ta>
            <ta e="T426" id="Seg_13256" s="T411">Serving as a commander of an entity, I was also slightly wounded in this battle at the promontory of Kursk, in a muscle of my left leg.</ta>
            <ta e="T432" id="Seg_13257" s="T426">After that I didn't leave the place where they fought.</ta>
            <ta e="T452" id="Seg_13258" s="T432">Only eleven out of over thousand soldiers of the entity, which I was part of, stayed alive in those ten days.</ta>
            <ta e="T462" id="Seg_13259" s="T452">I was among those as a wounded person.</ta>
            <ta e="T478" id="Seg_13260" s="T462">Therefore it is correct to state how desastrous the war was at the Curve of Kursk: how much blood flowed, how many people died.</ta>
            <ta e="T490" id="Seg_13261" s="T478">Being done in Kursk, our army followed the fleeing Germans westwards.</ta>
            <ta e="T510" id="Seg_13262" s="T490">After Kursk I was at places, where the actual (fights?) of the war took place, until the winter month January in the year 1944.</ta>
            <ta e="T538" id="Seg_13263" s="T510">At this time I stood and fought against the German soldiers at the following places: conquering the big cities named Belgorod, Kharkov, Poltava, Kremenchug and Kryukov, on the land of the Ukrainian people.</ta>
            <ta e="T549" id="Seg_13264" s="T538">Out of these cities we fought very heavily with the Germans conquering Kharkov.</ta>
            <ta e="T558" id="Seg_13265" s="T549">This city went up to three times from the one to the other hand [i.e. from one to the other side].</ta>
            <ta e="T564" id="Seg_13266" s="T558">Then on the third attempt we didn't release it anymore.</ta>
            <ta e="T575" id="Seg_13267" s="T564">Then there was a big battle while crossing a river named Dnyepr.</ta>
            <ta e="T589" id="Seg_13268" s="T575">The Germans, sitting on the other side of the river, were awake day and night and didn't let us cross at all.</ta>
            <ta e="T600" id="Seg_13269" s="T589">Therefore we couldn't go out for some days, we sat on the shore at this side.</ta>
            <ta e="T616" id="Seg_13270" s="T600">The bridges crossing the river, which we had repared by day, were destroyed, when airplanes of the Germans came and released some bombs.</ta>
            <ta e="T626" id="Seg_13271" s="T616">Therefore we later in the night built bridges at some places.</ta>
            <ta e="T634" id="Seg_13272" s="T626">The bloody German also didn't let the bridges stand, which we had built by night.</ta>
            <ta e="T640" id="Seg_13273" s="T634">Then, after the people being at our side had become very angry,</ta>
            <ta e="T646" id="Seg_13274" s="T640">after they had shot with all war weapons,</ta>
            <ta e="T654" id="Seg_13275" s="T646">after they had sent some dozens of airplanes to the other side,</ta>
            <ta e="T663" id="Seg_13276" s="T654">they covered themselves completely and went to the other shore, in order to struggle [with the Germans].</ta>
            <ta e="T670" id="Seg_13277" s="T663">Our soldiers, in the noise of the battle not fitting onto the bridge,</ta>
            <ta e="T688" id="Seg_13278" s="T670">some people with a boat, some, having tied together some logs, others swam freely out to the river named Dnyepr.</ta>
            <ta e="T702" id="Seg_13279" s="T688">The noise there, the loud screams, the battle and the not comparable clamor of the weapons were terrible.</ta>
            <ta e="T715" id="Seg_13280" s="T702">The German side, which didn't withstand this defeating battle, shot over the water in vain.</ta>
            <ta e="T727" id="Seg_13281" s="T715">When looking from the shore onto the river, in the water the human didn't understand anything.</ta>
            <ta e="T735" id="Seg_13282" s="T727">For example it was, as if a big frightened herd of wild reindeers was swimming.</ta>
            <ta e="T749" id="Seg_13283" s="T735">How many people choked or were killed by the weapons while crossing the river, nobody knew then.</ta>
            <ta e="T759" id="Seg_13284" s="T749">Having crossed the river, our people chased the Germans again not letting them take a rest.</ta>
            <ta e="T785" id="Seg_13285" s="T759">I went then to some battles, on 4th January 1943 I was finally wounded: my left femur broke.</ta>
            <ta e="T800" id="Seg_13286" s="T785">For healing it, until I got better, I was lying for six months in a city named Essentuki, at a warm place named Caucasus.</ta>
            <ta e="T807" id="Seg_13287" s="T800">Then after the healing I went out with a wooden stick,</ta>
            <ta e="T813" id="Seg_13288" s="T807">I was completely freed from the war, I left the army completely.</ta>
            <ta e="T827" id="Seg_13289" s="T813">Since the day, when I fought for the first time, I was in the war for one and a half year, I was wounded in total four times.</ta>
            <ta e="T832" id="Seg_13290" s="T827">Therefore I went three times in order to let it heal.</ta>
            <ta e="T854" id="Seg_13291" s="T832">Shortly spoken, (…) went to the Great Patriotic War to protect my people and my homeland.</ta>
            <ta e="T861" id="Seg_13292" s="T854">My blood and my sweat were not poured out in vain in this war.</ta>
            <ta e="T882" id="Seg_13293" s="T861">The Central Committee of our Communist Party and the government, which improves the life of our state, awarded me the high war medal "Order of the Red Banner".</ta>
            <ta e="T890" id="Seg_13294" s="T882">Then also a medal with the name "For the victory over Germany".</ta>
            <ta e="T904" id="Seg_13295" s="T890">People, living in our Soviet system: Your life is moving well, to the power going forward.</ta>
            <ta e="T912" id="Seg_13296" s="T904">Therefore nobody wants now that war would come.</ta>
            <ta e="T921" id="Seg_13297" s="T912">The people of our country didn't like war since olden times.</ta>
            <ta e="T935" id="Seg_13298" s="T921">Our people now builds up a life, which corresponds to its wishes, a bright and happy life shown by Lenin and named "communism".</ta>
            <ta e="T939" id="Seg_13299" s="T935">We all say now: </ta>
            <ta e="T944" id="Seg_13300" s="T939">"May there be no war on the earth.</ta>
            <ta e="T951" id="Seg_13301" s="T944">May there be a bright and sunny life for all peoples."</ta>
         </annotation>
         <annotation name="fg" tierref="fg">
            <ta e="T11" id="Seg_13302" s="T0">Ich bin freiwillig in den Krieg gegangen, im Jahr 1941.</ta>
            <ta e="T16" id="Seg_13303" s="T11">Im September, dem Monat, wo die Kälte kommt.</ta>
            <ta e="T31" id="Seg_13304" s="T16">Als ich am Anfang ging, durchlief ich zunächst ein Jahr und neun Monate eine Kriegsausbildung, an einem Ort, der "Ferner Osten" heißt.</ta>
            <ta e="T48" id="Seg_13305" s="T31">Dann im Sommer 1943 schickte man uns nach Westen, an den Ort, wo die Kriegshandlungen stattfanden.</ta>
            <ta e="T57" id="Seg_13306" s="T48">Wir trafen den Krieg in einem Dorf namens Prochorovka in der Oblast Kursk.</ta>
            <ta e="T81" id="Seg_13307" s="T57">Dort hatten die deutschen Soldaten sehr viele Waffen und Munition gesammelt und unsere Armee an einen Ort fünfdreißig Werst weiter zurück gedrängt.</ta>
            <ta e="T89" id="Seg_13308" s="T81">Diesen Ort nannte man dann später Kursker Bogen.</ta>
            <ta e="T96" id="Seg_13309" s="T89">Wenn man es auf Dolganisch sagt, wäre es richtig "Kursker Krümmung" zu sagen.</ta>
            <ta e="T102" id="Seg_13310" s="T96">Der Befehlshaber der deutschen Armee hatte offenbar einen Gedanken.</ta>
            <ta e="T117" id="Seg_13311" s="T102">Nämlich, nachdem sie sich durch diesen Ort gekämpft hätten, sehr schnell unsere Hauptstadt mit dem Namen Moskau zu nehmen.</ta>
            <ta e="T135" id="Seg_13312" s="T117">Unsere Einheit, nachdem sie am Kursker Bogen in das größte Feuer des Krieges hineingegangen war, versuchte zehn Tage lang die deutsche Armee zu verjagen.</ta>
            <ta e="T142" id="Seg_13313" s="T135">Während dieser Zeit kämpften wir ohne Tag und Nacht zu bemerken.</ta>
            <ta e="T146" id="Seg_13314" s="T142">Ohne zu schlafen und sich auszuruhen.</ta>
            <ta e="T152" id="Seg_13315" s="T146">Zum Beispiel ohne etwas zu wissen und zu hören.</ta>
            <ta e="T184" id="Seg_13316" s="T152">Auf beiden kämpfenden Seiten gingen so viele tausend Menschen verloren, starben, so viele Menschen wurden verwundet, so viele Kriegswaffen: Flugzeuge, Panzer, Geschütze und andere kleine Gewehre, das Kriegsarsenal wurde zerstört, niemand wusste es besonders damals.</ta>
            <ta e="T196" id="Seg_13317" s="T184">Was für ein riesiger Krieg sich an diesem Ort abspielte, kann man durch ein Beispiel verstehen.</ta>
            <ta e="T214" id="Seg_13318" s="T196">An einem (eisigen?) Ort, am Ort, wo wir waren, kamen an einem Tag einmal von der deutschen Seite ungefähr 200 Flugzeuge geflogen.</ta>
            <ta e="T226" id="Seg_13319" s="T214">Sie warfen über uns hunderte Bomben ab, als ob Regen fiele.</ta>
            <ta e="T239" id="Seg_13320" s="T226">Dann schossen sie auch noch zusätzlich mit so mit kleinen Patronen geladenen Kriegswaffen, mit Maschinengewehren.</ta>
            <ta e="T257" id="Seg_13321" s="T239">Nachdem so viele Bomben gefallen waren, nachdem so viele Waffen geschossen hatten, war die Erde überhaupt nicht mehr zu sehen, schwarzer Nebel legte sich.</ta>
            <ta e="T266" id="Seg_13322" s="T257">In diesem Nebel konnte man den neben sich liegenden Mensch nicht sehen.</ta>
            <ta e="T279" id="Seg_13323" s="T266">Manchmal wurde jemand von dem Lärm der Kriegswaffen so, als ob er nicht hören würde, taub.</ta>
            <ta e="T288" id="Seg_13324" s="T279">Am zehnten Tag wurde bei uns in der Nähe der Krieg sehr stark gemacht.</ta>
            <ta e="T303" id="Seg_13325" s="T288">Vom Himmel, von der Erde [kamen] sehr viele Flugzeuge, Panzer, speziell für den Krieg gemachte Feuerwaffen, die man "Katjuscha" und "Andrjuscha" nannte.</ta>
            <ta e="T309" id="Seg_13326" s="T303">Dann einzeln einige kleine Gewehre.</ta>
            <ta e="T316" id="Seg_13327" s="T309">Die alle zusammen stürmten auf die Orte, wo die Deutschen waren.</ta>
            <ta e="T325" id="Seg_13328" s="T316">Die deutsche Armee, einen solchen großen Schlag in Wellen nicht aushaltend, wich zurück.</ta>
            <ta e="T332" id="Seg_13329" s="T325">Sofort stürzte sie an einen Ort, zehn, fünfzehn Werst weiter hinten, zurück.</ta>
            <ta e="T341" id="Seg_13330" s="T332">Nach diesem großen Schlag wurde die Kursker Krümmung begradigt.</ta>
            <ta e="T346" id="Seg_13331" s="T341">Die deutsche Armee jagten wir zurück.</ta>
            <ta e="T363" id="Seg_13332" s="T346">Nachdem sich anschließend einige schlechte Soldaten der Deutschen uns ergeben hatten, fanden sie bis zum Ende des Krieges ihren Verstand und ihre Vernunft nicht, sie flohen.</ta>
            <ta e="T376" id="Seg_13333" s="T363">Nachdem unsere Armee ihre Kräfte sehr gut gesammelt hatte, jagte sie die Deutschen zurück.</ta>
            <ta e="T387" id="Seg_13334" s="T376">An späteren Tagen, während ich im Krieg war, habe ich solch schreckliche Menschen (…) nicht gesehen.</ta>
            <ta e="T400" id="Seg_13335" s="T387">Ich habe an diesem Ort zum ersten Mal eine heiße Schlacht gesehen: wie das Blut von Soldaten wie ein Fluss vergossen wurde,</ta>
            <ta e="T405" id="Seg_13336" s="T400">wie so viele tausend Menschen aufhörten zu atmen,</ta>
            <ta e="T411" id="Seg_13337" s="T405">wie so viele verwundete Menschen sehr litten.</ta>
            <ta e="T426" id="Seg_13338" s="T411">Auch ich wurde in diesem Gefecht, am Kursker Felsvorsprung, als Befehlshaber einer Einheit dienend, leicht am Muskel meines linken Beins verwundet.</ta>
            <ta e="T432" id="Seg_13339" s="T426">Danach verließ ich den Ort, wo gekämpft wurde, nicht.</ta>
            <ta e="T452" id="Seg_13340" s="T432">In dem Gefecht an diesem Ort blieben von den über tausend Soldaten der Einheit, in der ich war, in diesen zehn Tagen nur elf übrig.</ta>
            <ta e="T462" id="Seg_13341" s="T452">Darunter war ich als verwundeter Mensch.</ta>
            <ta e="T478" id="Seg_13342" s="T462">Daher ist es richtig festzustellen, wie desaströs der Krieg an der Kursker Krümmung war: wie viel Blut floss, wie viele Menschen starben.</ta>
            <ta e="T490" id="Seg_13343" s="T478">Nachdem sie in Kursk fertig war, verfolgte unsere Armee den fliehenden Deutschen Richtung Westen.</ta>
            <ta e="T510" id="Seg_13344" s="T490">Nach Kursk war ich an Orten, wo die eigentlichen (Kämpfe?) des Krieges waren, bis zum Wintermonat Januar im Jahr 1944.</ta>
            <ta e="T538" id="Seg_13345" s="T510">Zu dieser Zeit stand und kämpfte ich gegen die deutschen Soldaten an solchen Orten: beim Einnehmen der großen Städte mit den Namen Belgorod, Charkov, Poltava, Krementschug und Krjukov, auf dem Boden des ukrainischen Volkes.</ta>
            <ta e="T549" id="Seg_13346" s="T538">Von diesen Städten kämpften wir sehr heftig mir den Deutschen bei der Einnahme von Charkov.</ta>
            <ta e="T558" id="Seg_13347" s="T549">Diese Stadt wechselte bis zu drei Male aus der einen in die andere Hand.</ta>
            <ta e="T564" id="Seg_13348" s="T558">Dann beim dritten Anlauf gaben wir sie nicht mehr aus der Hand.</ta>
            <ta e="T575" id="Seg_13349" s="T564">Dann war eine große Schlacht beim Überqueren eines Flusses mit dem Namen Dnjepr.</ta>
            <ta e="T589" id="Seg_13350" s="T575">Die Deutschen, die auf der anderen Seite dieses Flusses saßen, wachten Tag und Nacht und ließen uns überhaupt nicht hinübergehen.</ta>
            <ta e="T600" id="Seg_13351" s="T589">Deshalb konnten wir einige Tage lang nicht hinausgehen, wir saßen auf dem diesseitigen Ufer.</ta>
            <ta e="T616" id="Seg_13352" s="T600">Die den Fluss überquerenden Brücken, die wir bei Tag repariert hatten, wurden zerstört, indem die Flugzeuge der Deutschen kamen und einige Bomben warfen.</ta>
            <ta e="T626" id="Seg_13353" s="T616">Deshalb bauten wir später in der Nacht an einigen Stellen Brücken.</ta>
            <ta e="T634" id="Seg_13354" s="T626">Der verfluchte Deutsche ließ auch die Brücken, die wir in der Nacht gebaut hatten, nicht stehen.</ta>
            <ta e="T640" id="Seg_13355" s="T634">Dann, nachdem die auf unserer Seite Befindlichen sehr böse geworden waren,</ta>
            <ta e="T646" id="Seg_13356" s="T640">nachdem sie mit allen Kriegswaffen geschossen hatten,</ta>
            <ta e="T654" id="Seg_13357" s="T646">nachdem sie einige Dutzend Flugzeuge auf die andere Seite losgeschickt hatten,</ta>
            <ta e="T663" id="Seg_13358" s="T654">bedeckten sie sich ganz und gingen ans andere Ufer, um sich dem entgegen zu stellen.</ta>
            <ta e="T670" id="Seg_13359" s="T663">Unsere Soldaten, im Lärm des Kampfes nicht auf die Brücke passend,</ta>
            <ta e="T688" id="Seg_13360" s="T670">einige Leute mit einem Boot, einige, nachdem sie ein paar Holzklötze zusammengebunden hatten, andere schwammen frei auf den Fluss mit dem Namen Dnjepr hinaus.</ta>
            <ta e="T702" id="Seg_13361" s="T688">Der Lärm dort, die lauten Schreie, der Kampf und das mit nichts zu vergleichende Lärmen der Waffen waren furchtbar.</ta>
            <ta e="T715" id="Seg_13362" s="T702">Die deutsche Seite, die dieses vernichtende Gefecht nicht aushielt, schoss vergeblich über das Wasser.</ta>
            <ta e="T727" id="Seg_13363" s="T715">Wenn man vom Ufer auf den Fluss schaut, im Wasser hat der Mensch nichts verstanden.</ta>
            <ta e="T735" id="Seg_13364" s="T727">Zum Beispiel war es so, als ob eine große verängstigte Herde wilder Rentiere schwimmen würde.</ta>
            <ta e="T749" id="Seg_13365" s="T735">Wie viele Menschen beim Überqueren dieses Flusses erstickten oder durch die Kriegswaffen gestorben sind, wusste damals niemand.</ta>
            <ta e="T759" id="Seg_13366" s="T749">Nachdem sie den Fluss überquert hatten, jagten die Unseren die Deutschen wieder, ohne ihne eine Pause zu gönnen.</ta>
            <ta e="T785" id="Seg_13367" s="T759">Ich ging dann in einige Gefechte, am 4. Januar 1943 wurde ich endgültig verwundet: ich brach mir meinen linken Oberschenkelknochen.</ta>
            <ta e="T800" id="Seg_13368" s="T785">Um das heilen zu lassen, bis ich wieder gesund würde, lag ich sechs Monate in einer Stadt namens Essentuki, an einem warmen Ort namens Kaukasus.</ta>
            <ta e="T807" id="Seg_13369" s="T800">Dann nach der Heilung ging ich mit einem Holzstock heraus, </ta>
            <ta e="T813" id="Seg_13370" s="T807">ich wurde ganz vom Krieg befreit, ich verließ die Armee komplett.</ta>
            <ta e="T827" id="Seg_13371" s="T813">Seit dem Tag, an dem ich das erste Mal kämpfte, war ich anderthalb Jahre lang im Krieg, ich wurde insgesamt vier Mal verwundet.</ta>
            <ta e="T832" id="Seg_13372" s="T827">Daher fuhr ich drei mal, um das heilen zu lassen.</ta>
            <ta e="T854" id="Seg_13373" s="T832">Kurz gesagt, (…) ging ich in den Großen Vaterländischen Krieg, um mein Volk und mein Heimatland zu schützen.</ta>
            <ta e="T861" id="Seg_13374" s="T854">Mein Blut und mein Schweiß wurden in diesem Krieg nicht umsonst vergossen.</ta>
            <ta e="T882" id="Seg_13375" s="T861">Das Zentralkomitee unserer kommunistischen Partei und unsere das Leben unseres Staates verbessernde Regierung haben mir den hohen Kriegsorden "Rotbannerorden" verliehen.</ta>
            <ta e="T890" id="Seg_13376" s="T882">Dann noch eine Medaille mit dem Namen "Für den Sieg über Deutschland".</ta>
            <ta e="T904" id="Seg_13377" s="T890">Menschen, die jetzt in unserer Sowjetordnung lebt: Euer Leben läuft gut zur nach vorne gehenden Kraft.</ta>
            <ta e="T912" id="Seg_13378" s="T904">Deshalb möchte niemand, dass jetzt Krieg kommt.</ta>
            <ta e="T921" id="Seg_13379" s="T912">Die Menschen unseres Landes mochten seit Urzeiten keinen Krieg.</ta>
            <ta e="T935" id="Seg_13380" s="T921">Unser Volk baut jetzt ein mit seinem Wunsch übereinstimmendes, strahlendes, glückliches, von Lenin gezeigtes Leben mit dem Namen "Kommunismus" auf.</ta>
            <ta e="T939" id="Seg_13381" s="T935">Wir alle sagen jetzt: </ta>
            <ta e="T944" id="Seg_13382" s="T939">"Möge kein Krieg auf der Erde sein.</ta>
            <ta e="T951" id="Seg_13383" s="T944">Möge es für jedes Volk ein strahlendes, sonniges Leben geben."</ta>
         </annotation>
         <annotation name="fr" tierref="fr" />
         <annotation name="ltr" tierref="ltr">
            <ta e="T11" id="Seg_13384" s="T0">Я на войну ушёл по своей воле в тысяча девятьсот сорок первом году.</ta>
            <ta e="T16" id="Seg_13385" s="T11">В осенний сентябрь, холод когда наступает в месяц. </ta>
            <ta e="T31" id="Seg_13386" s="T16">В начале когда поехал, один год и девять месяцев военной науке обучался Дальний Восток называемом месте.</ta>
            <ta e="T48" id="Seg_13387" s="T31">Потом летом в тысяча девятьсот сорок третьем году нас направили на запад, где военные действия шли в этом месте.</ta>
            <ta e="T57" id="Seg_13388" s="T48">Мы войну встретили в Прохоровке называемой деревне Курской области.</ta>
            <ta e="T81" id="Seg_13389" s="T57">Там немецкие солдаты очень много военного оружия и остальную аммуницию сложив, нашу армию сильно назад, в тридцать пятую версту имеющую землю отодвинул было.</ta>
            <ta e="T89" id="Seg_13390" s="T81">Затем позже эту землю Курской дугой назвали.</ta>
            <ta e="T96" id="Seg_13391" s="T89">По-долгански если сказать, "Курская кривая" назвать правильнее будет.</ta>
            <ta e="T102" id="Seg_13392" s="T96">У немецкой армии предводителей желание было, оказывается.</ta>
            <ta e="T117" id="Seg_13393" s="T102">По этому месту вероломно, вмиг нашей родины столицу город Москву захватить мог. </ta>
            <ta e="T135" id="Seg_13394" s="T117">Наша часть на Курской дуге, в самое пекло войны прорвавшись, в течении десяти дней подряд немецкую армию прогоняла.</ta>
            <ta e="T142" id="Seg_13395" s="T135">В это время непрерывно воевали, сутки дни не зная.</ta>
            <ta e="T146" id="Seg_13396" s="T142">Без сна без отдыха.</ta>
            <ta e="T152" id="Seg_13397" s="T146">Иной раз ничего не зная и не слыша.</ta>
            <ta e="T184" id="Seg_13398" s="T152">С двух воююших сторон сколько тысяч людей пропало, погибло, сколько человек ранено было, сколько военного оружия: самолёты, танки, орудия затем маленькие разлинные ружья, военного арсенала сломаного никто особо не знал тогда.</ta>
            <ta e="T196" id="Seg_13399" s="T184">Какая огромная великая война прошла на этой земле человек поймёт из одного такого сравнения. </ta>
            <ta e="T214" id="Seg_13400" s="T196">В ( ? ) земле, мы где находились, в день один раз прилетал с немецкой стороны порядка духсот самолётов.</ta>
            <ta e="T226" id="Seg_13401" s="T214">Они все над нами, словно дождь проливаясь, сотни бом скидывали.</ta>
            <ta e="T239" id="Seg_13402" s="T226">Затем отдельно ещё стреляли несколько раз как (сарпа?) заряженным военным орудием, из пулемёта.</ta>
            <ta e="T257" id="Seg_13403" s="T239">Столько бомб как попадало, из такого количества орудия как постреляло, земля вся ничегошеньки не видно будто, чёрным туманом становилась.</ta>
            <ta e="T266" id="Seg_13404" s="T257">В этом тумане человек рядом лежащего человека никак не видел.</ta>
            <ta e="T279" id="Seg_13405" s="T266">Иной раз человек от оружейного звука ничего слышал будто, глухим становился.</ta>
            <ta e="T288" id="Seg_13406" s="T279">На десятый день около нас мимо военные дествия с большей силой прошли.</ta>
            <ta e="T303" id="Seg_13407" s="T288">С неба, с земли очень много самолётов, танки, специально для войны сооруженные с огнём ружья, "Катюша", "Андрюша" их называли. </ta>
            <ta e="T309" id="Seg_13408" s="T303">Затем отдельно несколько маленькие ружья.</ta>
            <ta e="T316" id="Seg_13409" s="T309">Всё это в один миг ударили по немецким позициям.</ta>
            <ta e="T325" id="Seg_13410" s="T316">Немецкая армия, такого с волнами сильного удара не выдержав, назад отступила (попятилась).</ta>
            <ta e="T332" id="Seg_13411" s="T325">И тут же на пятнадцать вёрст по земле обратно отсупило (отскочила).</ta>
            <ta e="T341" id="Seg_13412" s="T332">Этого большого удара после Курской называемая дуга хорошо выпрямилась. </ta>
            <ta e="T346" id="Seg_13413" s="T341">Немецкую армию обратно мы прогнали.</ta>
            <ta e="T363" id="Seg_13414" s="T346">После этого немецкие (некоторые?) солдаты после как сдались, до самого окончания войны свой ум-разум не нашли, убегали.</ta>
            <ta e="T376" id="Seg_13415" s="T363">Наша армия, очень хорошо силы все собрав, немцев, назад не показывая, вовсе погнала.</ta>
            <ta e="T387" id="Seg_13416" s="T376">Я последующие дни, бывая на войне, такого, чтоб у человека ужас был, не видел.</ta>
            <ta e="T400" id="Seg_13417" s="T387">Я на этой земле впервые увидел жестокое сражение: как солдатская кровь лилась рекой, </ta>
            <ta e="T405" id="Seg_13418" s="T400">скольких тысяч людей жизнь обрывалась. </ta>
            <ta e="T411" id="Seg_13419" s="T405">сколько раненых людей сильно мучалось.</ta>
            <ta e="T426" id="Seg_13420" s="T411">Я тоже здесь в этом сражении Курском мысе, в отделении командиром служив, легонько в левую ногу ранение получил.</ta>
            <ta e="T432" id="Seg_13421" s="T426">И после этого с воюющего места не выходил.</ta>
            <ta e="T452" id="Seg_13422" s="T432">На этом месте имеющемся сражении только, я когда находился время, из тысячии с лишним солдатов осталось за эти десять дней битвы осталось одиннадать всего людей.</ta>
            <ta e="T462" id="Seg_13423" s="T452">Среди них ещё один я раненый был.</ta>
            <ta e="T478" id="Seg_13424" s="T462">Отсюда правильно будет оценить, насколько пагубной битвой война была на Курской дуге: сколько крови пролилось, сколько людей погибло.</ta>
            <ta e="T490" id="Seg_13425" s="T478">На Курской завершив, наша армия назад убегающего немца к западу погналась за ней.</ta>
            <ta e="T510" id="Seg_13426" s="T490">Этой Курской после я на самой войны разгаре был тысяча девятьсот сорок четвёртом году до зимнего января месяца.</ta>
            <ta e="T538" id="Seg_13427" s="T510">В это время с немецкими солдатами, стоя напротив, сражался в таких местах: Белгород потом Харьков, затем Полтава, потом Кременчуг, потом Крюков с такими названиями большие города когда брали, на украинской нации земле.</ta>
            <ta e="T549" id="Seg_13428" s="T538">Из этих городов очень жёстко мы сражались с немцем Харьков города при захвате. </ta>
            <ta e="T558" id="Seg_13429" s="T549">Этот город три раза из рук в руки переходил.</ta>
            <ta e="T564" id="Seg_13430" s="T558">Потом с третьего захода мы из рук не выпустили.</ta>
            <ta e="T575" id="Seg_13431" s="T564">Потом большая битва ещё была на Днепре называемой реке переправе.</ta>
            <ta e="T589" id="Seg_13432" s="T575">Этой реки по той стороне сидевшие немцы, дни и ночи окарауливая, нам никак не давали перейти через реку. </ta>
            <ta e="T600" id="Seg_13433" s="T589">Поэтому мы несколько дней не могли выйти, на ближнем берегу сидели.</ta>
            <ta e="T616" id="Seg_13434" s="T600">Реку переходившие мосты, днём когда ремонтировали, немецкие самолёты, прилетая, несколько бомб сбрасывая, сильно разрушал.</ta>
            <ta e="T626" id="Seg_13435" s="T616">Поэтому мы ночью на нескольких местах мосты налаживали.</ta>
            <ta e="T634" id="Seg_13436" s="T626">Проклятый немец, ночью даже отремонтированные нами мосты, не оставлял в покое.</ta>
            <ta e="T640" id="Seg_13437" s="T634">Затем с нашей стороны находившиеся, очень рассердившись, </ta>
            <ta e="T646" id="Seg_13438" s="T640">из всех военных орудий выстрелив,</ta>
            <ta e="T654" id="Seg_13439" s="T646">по несколько десятков самолетов на противоположную сторону направив,</ta>
            <ta e="T663" id="Seg_13440" s="T654">это всё, накрывшись, на тот берег, сопротивляясь, вышли. </ta>
            <ta e="T670" id="Seg_13441" s="T663">Наши солдаты в этом сражении, на мосту не поместившись,</ta>
            <ta e="T688" id="Seg_13442" s="T670">некоторые люди на лодках, некоторые по несколько брёвен соединив, некоторые в брод вышли на другую сторону Днепр назуваемую реку.</ta>
            <ta e="T702" id="Seg_13443" s="T688">Там шум, сильные крики, вопли, битва ни с чем не сравнимого военного оружия грохот невыносим был.</ta>
            <ta e="T715" id="Seg_13444" s="T702">С немецкой стороны такой отпор не выдержав, в холостую над рекой палили.</ta>
            <ta e="T727" id="Seg_13445" s="T715">Если с берега посмотреть, в стороне реки, на воде человек ничего не понимал.</ta>
            <ta e="T735" id="Seg_13446" s="T727">Допустим, испугавших сильно диких оленей стадо как будто переплывало было.</ta>
            <ta e="T749" id="Seg_13447" s="T735">Эту реку во время переправы сколько человек, захлебнувшись ли, от военного оружия ли погибло, никто не знал тогда.</ta>
            <ta e="T759" id="Seg_13448" s="T749">Реку переплыв, наши опять не дав немцам опомниться, прогнали.</ta>
            <ta e="T785" id="Seg_13449" s="T759">Потом я в несколько сражений отправляясь, четвёртого дня в январе месяце, тысяча девятьсот сорок третьем году окончательное ранение получил очень тяжёло: с левой стороны бедренную кость сильно дал сломать.</ta>
            <ta e="T800" id="Seg_13450" s="T785">Это пролечив, до выздоровления шесть месяцев подряд пролежал в Ессентуки называемом городе, Кавказ называемом жарком месте.</ta>
            <ta e="T807" id="Seg_13451" s="T800">Потом после лечения с деревянной тростью как вышел, </ta>
            <ta e="T813" id="Seg_13452" s="T807">от войны вовсе освободился, из армия совсем вышел (демобилизировался). </ta>
            <ta e="T827" id="Seg_13453" s="T813">С первого военного моего дня, один год подряд, на военных дейсвиях побывав, всего четыре раза был ранен.</ta>
            <ta e="T832" id="Seg_13454" s="T827">По этому поводу три раза лечиться ездил.</ta>
            <ta e="T854" id="Seg_13455" s="T832">Таким образом я среди можества людей в никто не узнаваемой в отечественной этой великой войне свой народ, родимую, родную землю свою защищал ходил.</ta>
            <ta e="T861" id="Seg_13456" s="T854">Моя кровь, пот в этой войне зря не проливались.</ta>
            <ta e="T882" id="Seg_13457" s="T861">Наша коммунистическая партия, центральный комитета и нашего государства жизнь улучшающее правительство дали мне от государства высший военный орден "Боевого Красного Знамени" называемый.</ta>
            <ta e="T890" id="Seg_13458" s="T882">Затем ещё медаль "За победу над Германией".</ta>
            <ta e="T904" id="Seg_13459" s="T890">Сейчас в нашей в Советское время живущие люде – ваша жизнь хорошо налажена, к перёд идущей силе.</ta>
            <ta e="T912" id="Seg_13460" s="T904">Поэтому никто сейчас, чтобы война была, не хочет.</ta>
            <ta e="T921" id="Seg_13461" s="T912">Нашей земли люди с давних времён войну не любил.</ta>
            <ta e="T935" id="Seg_13462" s="T921">Нашего народа сейчас своё желание – светлую, счастливую, Лениным указанный коммунизм назваемую жизнь строит.</ta>
            <ta e="T939" id="Seg_13463" s="T935">Мы все говорим сейчас: </ta>
            <ta e="T944" id="Seg_13464" s="T939">"Пусть войны не будет над землёй.</ta>
            <ta e="T951" id="Seg_13465" s="T944">Будет пусть ясная, солнечная (светлая) жизнь для всего народа".</ta>
         </annotation>
         <annotation name="nt" tierref="nt">
            <ta e="T376" id="Seg_13466" s="T363">[DCh]: Not clear what is meant with the clause "köllörbökkö".</ta>
            <ta e="T854" id="Seg_13467" s="T832">[DCh]: Meaning of the middle part of the sentence not clear.</ta>
         </annotation>
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T952" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T629" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T673" />
            <conversion-tli id="T674" />
            <conversion-tli id="T675" />
            <conversion-tli id="T676" />
            <conversion-tli id="T677" />
            <conversion-tli id="T678" />
            <conversion-tli id="T679" />
            <conversion-tli id="T680" />
            <conversion-tli id="T681" />
            <conversion-tli id="T682" />
            <conversion-tli id="T683" />
            <conversion-tli id="T684" />
            <conversion-tli id="T685" />
            <conversion-tli id="T686" />
            <conversion-tli id="T687" />
            <conversion-tli id="T688" />
            <conversion-tli id="T689" />
            <conversion-tli id="T690" />
            <conversion-tli id="T691" />
            <conversion-tli id="T692" />
            <conversion-tli id="T693" />
            <conversion-tli id="T694" />
            <conversion-tli id="T695" />
            <conversion-tli id="T696" />
            <conversion-tli id="T697" />
            <conversion-tli id="T698" />
            <conversion-tli id="T699" />
            <conversion-tli id="T700" />
            <conversion-tli id="T701" />
            <conversion-tli id="T702" />
            <conversion-tli id="T703" />
            <conversion-tli id="T704" />
            <conversion-tli id="T705" />
            <conversion-tli id="T706" />
            <conversion-tli id="T707" />
            <conversion-tli id="T708" />
            <conversion-tli id="T709" />
            <conversion-tli id="T710" />
            <conversion-tli id="T711" />
            <conversion-tli id="T712" />
            <conversion-tli id="T713" />
            <conversion-tli id="T714" />
            <conversion-tli id="T715" />
            <conversion-tli id="T716" />
            <conversion-tli id="T717" />
            <conversion-tli id="T718" />
            <conversion-tli id="T719" />
            <conversion-tli id="T720" />
            <conversion-tli id="T721" />
            <conversion-tli id="T722" />
            <conversion-tli id="T723" />
            <conversion-tli id="T724" />
            <conversion-tli id="T725" />
            <conversion-tli id="T726" />
            <conversion-tli id="T727" />
            <conversion-tli id="T728" />
            <conversion-tli id="T729" />
            <conversion-tli id="T730" />
            <conversion-tli id="T731" />
            <conversion-tli id="T732" />
            <conversion-tli id="T733" />
            <conversion-tli id="T734" />
            <conversion-tli id="T735" />
            <conversion-tli id="T736" />
            <conversion-tli id="T737" />
            <conversion-tli id="T738" />
            <conversion-tli id="T739" />
            <conversion-tli id="T740" />
            <conversion-tli id="T741" />
            <conversion-tli id="T742" />
            <conversion-tli id="T743" />
            <conversion-tli id="T744" />
            <conversion-tli id="T745" />
            <conversion-tli id="T746" />
            <conversion-tli id="T747" />
            <conversion-tli id="T748" />
            <conversion-tli id="T749" />
            <conversion-tli id="T750" />
            <conversion-tli id="T751" />
            <conversion-tli id="T752" />
            <conversion-tli id="T753" />
            <conversion-tli id="T754" />
            <conversion-tli id="T755" />
            <conversion-tli id="T756" />
            <conversion-tli id="T757" />
            <conversion-tli id="T758" />
            <conversion-tli id="T759" />
            <conversion-tli id="T760" />
            <conversion-tli id="T761" />
            <conversion-tli id="T762" />
            <conversion-tli id="T763" />
            <conversion-tli id="T764" />
            <conversion-tli id="T765" />
            <conversion-tli id="T766" />
            <conversion-tli id="T767" />
            <conversion-tli id="T768" />
            <conversion-tli id="T769" />
            <conversion-tli id="T770" />
            <conversion-tli id="T771" />
            <conversion-tli id="T772" />
            <conversion-tli id="T773" />
            <conversion-tli id="T774" />
            <conversion-tli id="T775" />
            <conversion-tli id="T776" />
            <conversion-tli id="T777" />
            <conversion-tli id="T778" />
            <conversion-tli id="T779" />
            <conversion-tli id="T780" />
            <conversion-tli id="T781" />
            <conversion-tli id="T782" />
            <conversion-tli id="T783" />
            <conversion-tli id="T784" />
            <conversion-tli id="T785" />
            <conversion-tli id="T786" />
            <conversion-tli id="T787" />
            <conversion-tli id="T788" />
            <conversion-tli id="T789" />
            <conversion-tli id="T790" />
            <conversion-tli id="T791" />
            <conversion-tli id="T792" />
            <conversion-tli id="T793" />
            <conversion-tli id="T794" />
            <conversion-tli id="T795" />
            <conversion-tli id="T796" />
            <conversion-tli id="T797" />
            <conversion-tli id="T798" />
            <conversion-tli id="T799" />
            <conversion-tli id="T800" />
            <conversion-tli id="T801" />
            <conversion-tli id="T802" />
            <conversion-tli id="T803" />
            <conversion-tli id="T804" />
            <conversion-tli id="T805" />
            <conversion-tli id="T806" />
            <conversion-tli id="T807" />
            <conversion-tli id="T808" />
            <conversion-tli id="T809" />
            <conversion-tli id="T810" />
            <conversion-tli id="T811" />
            <conversion-tli id="T812" />
            <conversion-tli id="T813" />
            <conversion-tli id="T814" />
            <conversion-tli id="T815" />
            <conversion-tli id="T816" />
            <conversion-tli id="T817" />
            <conversion-tli id="T818" />
            <conversion-tli id="T819" />
            <conversion-tli id="T820" />
            <conversion-tli id="T821" />
            <conversion-tli id="T822" />
            <conversion-tli id="T823" />
            <conversion-tli id="T824" />
            <conversion-tli id="T825" />
            <conversion-tli id="T826" />
            <conversion-tli id="T827" />
            <conversion-tli id="T828" />
            <conversion-tli id="T829" />
            <conversion-tli id="T830" />
            <conversion-tli id="T831" />
            <conversion-tli id="T832" />
            <conversion-tli id="T833" />
            <conversion-tli id="T834" />
            <conversion-tli id="T835" />
            <conversion-tli id="T836" />
            <conversion-tli id="T837" />
            <conversion-tli id="T838" />
            <conversion-tli id="T839" />
            <conversion-tli id="T840" />
            <conversion-tli id="T841" />
            <conversion-tli id="T842" />
            <conversion-tli id="T843" />
            <conversion-tli id="T844" />
            <conversion-tli id="T845" />
            <conversion-tli id="T846" />
            <conversion-tli id="T847" />
            <conversion-tli id="T848" />
            <conversion-tli id="T849" />
            <conversion-tli id="T850" />
            <conversion-tli id="T851" />
            <conversion-tli id="T852" />
            <conversion-tli id="T853" />
            <conversion-tli id="T854" />
            <conversion-tli id="T855" />
            <conversion-tli id="T856" />
            <conversion-tli id="T857" />
            <conversion-tli id="T858" />
            <conversion-tli id="T859" />
            <conversion-tli id="T860" />
            <conversion-tli id="T861" />
            <conversion-tli id="T862" />
            <conversion-tli id="T863" />
            <conversion-tli id="T864" />
            <conversion-tli id="T865" />
            <conversion-tli id="T866" />
            <conversion-tli id="T867" />
            <conversion-tli id="T868" />
            <conversion-tli id="T869" />
            <conversion-tli id="T870" />
            <conversion-tli id="T871" />
            <conversion-tli id="T872" />
            <conversion-tli id="T873" />
            <conversion-tli id="T874" />
            <conversion-tli id="T875" />
            <conversion-tli id="T876" />
            <conversion-tli id="T877" />
            <conversion-tli id="T878" />
            <conversion-tli id="T879" />
            <conversion-tli id="T880" />
            <conversion-tli id="T881" />
            <conversion-tli id="T882" />
            <conversion-tli id="T883" />
            <conversion-tli id="T884" />
            <conversion-tli id="T885" />
            <conversion-tli id="T886" />
            <conversion-tli id="T887" />
            <conversion-tli id="T888" />
            <conversion-tli id="T889" />
            <conversion-tli id="T890" />
            <conversion-tli id="T891" />
            <conversion-tli id="T892" />
            <conversion-tli id="T893" />
            <conversion-tli id="T894" />
            <conversion-tli id="T895" />
            <conversion-tli id="T896" />
            <conversion-tli id="T897" />
            <conversion-tli id="T898" />
            <conversion-tli id="T899" />
            <conversion-tli id="T900" />
            <conversion-tli id="T901" />
            <conversion-tli id="T902" />
            <conversion-tli id="T903" />
            <conversion-tli id="T904" />
            <conversion-tli id="T905" />
            <conversion-tli id="T906" />
            <conversion-tli id="T907" />
            <conversion-tli id="T908" />
            <conversion-tli id="T909" />
            <conversion-tli id="T910" />
            <conversion-tli id="T911" />
            <conversion-tli id="T912" />
            <conversion-tli id="T913" />
            <conversion-tli id="T914" />
            <conversion-tli id="T915" />
            <conversion-tli id="T916" />
            <conversion-tli id="T917" />
            <conversion-tli id="T918" />
            <conversion-tli id="T919" />
            <conversion-tli id="T920" />
            <conversion-tli id="T921" />
            <conversion-tli id="T922" />
            <conversion-tli id="T923" />
            <conversion-tli id="T924" />
            <conversion-tli id="T925" />
            <conversion-tli id="T926" />
            <conversion-tli id="T927" />
            <conversion-tli id="T928" />
            <conversion-tli id="T929" />
            <conversion-tli id="T930" />
            <conversion-tli id="T931" />
            <conversion-tli id="T932" />
            <conversion-tli id="T933" />
            <conversion-tli id="T934" />
            <conversion-tli id="T935" />
            <conversion-tli id="T936" />
            <conversion-tli id="T937" />
            <conversion-tli id="T938" />
            <conversion-tli id="T939" />
            <conversion-tli id="T940" />
            <conversion-tli id="T941" />
            <conversion-tli id="T942" />
            <conversion-tli id="T943" />
            <conversion-tli id="T944" />
            <conversion-tli id="T945" />
            <conversion-tli id="T946" />
            <conversion-tli id="T947" />
            <conversion-tli id="T948" />
            <conversion-tli id="T949" />
            <conversion-tli id="T950" />
            <conversion-tli id="T951" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref"
                          name="ref"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st"
                          name="st"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts"
                          name="ts"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb"
                          name="mb"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp"
                          name="mp"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge"
                          name="ge"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg"
                          name="gg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr"
                          name="gr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc"
                          name="mc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps"
                          name="ps"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR"
                          name="SeR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF"
                          name="SyF"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST"
                          name="IST"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top"
                          name="Top"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc"
                          name="Foc"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR"
                          name="BOR"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon"
                          name="BOR-Phon"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph"
                          name="BOR-Morph"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS"
                          name="CS"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe"
                          name="fe"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg"
                          name="fg"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr"
                          name="fr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr"
                          name="ltr"
                          segmented-tier-id="tx"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt"
                          name="nt"
                          segmented-tier-id="tx"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
