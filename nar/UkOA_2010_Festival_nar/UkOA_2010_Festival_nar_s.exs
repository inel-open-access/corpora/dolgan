<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID03243C8D-0A48-3348-E4AF-DD3B157DB29D">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>UkOA_2010_Festival_nar</transcription-name>
         <referenced-file url="UkOA_2010_Festival_nar.wav" />
         <referenced-file url="UkOA_2010_Festival_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\UkOA_2010_Festival_nar\UkOA_2010_Festival_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">704</ud-information>
            <ud-information attribute-name="# HIAT:w">573</ud-information>
            <ud-information attribute-name="# e">566</ud-information>
            <ud-information attribute-name="# HIAT:u">45</ud-information>
            <ud-information attribute-name="# sc">8</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="UkOA">
            <abbreviation>UkOA</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="SE">
            <abbreviation>SE</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T1" time="0.0" type="appl" />
         <tli id="T2" time="0.6533333333333333" type="appl" />
         <tli id="T3" time="1.3066666666666666" type="appl" />
         <tli id="T4" time="1.9933333057788531" />
         <tli id="T5" time="2.426660441165594" />
         <tli id="T6" time="2.9476672018741525" />
         <tli id="T7" time="3.306658183566304" />
         <tli id="T8" time="3.339991431384513" />
         <tli id="T9" time="7.999741907341666" type="intp" />
         <tli id="T10" time="8.019504287004166" type="intp" />
         <tli id="T11" time="8.039266666666666" type="appl" />
         <tli id="T12" time="9.3324" type="appl" />
         <tli id="T13" time="10.625533333333333" type="appl" />
         <tli id="T14" time="11.918666666666667" type="appl" />
         <tli id="T15" time="13.2118" type="appl" />
         <tli id="T16" time="14.504933333333334" type="appl" />
         <tli id="T17" time="15.798066666666667" type="appl" />
         <tli id="T18" time="17.0912" type="appl" />
         <tli id="T19" time="18.384333333333334" type="appl" />
         <tli id="T20" time="19.677466666666668" type="appl" />
         <tli id="T21" time="20.9706" type="appl" />
         <tli id="T22" time="22.263733333333334" type="appl" />
         <tli id="T23" time="23.556866666666668" type="appl" />
         <tli id="T24" time="25.699934067838917" />
         <tli id="T25" time="25.758666666666667" type="appl" />
         <tli id="T26" time="26.667333333333335" type="appl" />
         <tli id="T27" time="27.576" type="appl" />
         <tli id="T28" time="28.484666666666666" type="appl" />
         <tli id="T29" time="29.393333333333334" type="appl" />
         <tli id="T30" time="30.699921240570223" />
         <tli id="T31" time="31.171176470588236" type="appl" />
         <tli id="T32" time="32.04035294117647" type="appl" />
         <tli id="T33" time="32.90952941176471" type="appl" />
         <tli id="T34" time="33.77870588235294" type="appl" />
         <tli id="T35" time="34.647882352941174" type="appl" />
         <tli id="T36" time="35.51705882352941" type="appl" />
         <tli id="T37" time="36.38623529411765" type="appl" />
         <tli id="T38" time="37.25541176470588" type="appl" />
         <tli id="T39" time="38.12458823529412" type="appl" />
         <tli id="T40" time="38.993764705882356" type="appl" />
         <tli id="T41" time="39.862941176470585" type="appl" />
         <tli id="T42" time="40.73211764705883" type="appl" />
         <tli id="T43" time="41.60129411764706" type="appl" />
         <tli id="T44" time="42.470470588235294" type="appl" />
         <tli id="T45" time="43.33964705882353" type="appl" />
         <tli id="T46" time="44.20882352941177" type="appl" />
         <tli id="T47" time="48.3598759346572" />
         <tli id="T48" time="48.45134586120004" type="intp" />
         <tli id="T49" time="48.54281578774288" type="intp" />
         <tli id="T50" time="48.63428571428572" type="appl" />
         <tli id="T51" time="49.819714285714284" type="appl" />
         <tli id="T52" time="51.00514285714286" type="appl" />
         <tli id="T53" time="52.190571428571424" type="appl" />
         <tli id="T54" time="55.326524728662825" />
         <tli id="T55" time="55.818362364331406" type="intp" />
         <tli id="T56" time="56.310199999999995" type="appl" />
         <tli id="T57" time="57.7773" type="appl" />
         <tli id="T58" time="59.2444" type="appl" />
         <tli id="T59" time="60.7115" type="appl" />
         <tli id="T60" time="62.178599999999996" type="appl" />
         <tli id="T61" time="63.6457" type="appl" />
         <tli id="T62" time="65.1128" type="appl" />
         <tli id="T63" time="66.5799" type="appl" />
         <tli id="T64" time="68.047" type="appl" />
         <tli id="T65" time="69.4063" type="appl" />
         <tli id="T66" time="70.76559999999999" type="appl" />
         <tli id="T67" time="72.1249" type="appl" />
         <tli id="T68" time="73.4842" type="appl" />
         <tli id="T69" time="74.8435" type="appl" />
         <tli id="T70" time="76.2028" type="appl" />
         <tli id="T71" time="77.5621" type="appl" />
         <tli id="T72" time="78.9214" type="appl" />
         <tli id="T73" time="80.2807" type="appl" />
         <tli id="T74" time="82.9531205203942" />
         <tli id="T75" time="83.5165602601971" type="intp" />
         <tli id="T76" time="84.08" type="appl" />
         <tli id="T77" time="85.3" type="appl" />
         <tli id="T78" time="86.52" type="appl" />
         <tli id="T79" time="87.74" type="appl" />
         <tli id="T80" time="89.07333268381608" />
         <tli id="T81" time="89.37285714285714" type="appl" />
         <tli id="T82" time="89.78571428571428" type="appl" />
         <tli id="T83" time="90.19857142857143" type="appl" />
         <tli id="T84" time="90.61142857142856" type="appl" />
         <tli id="T85" time="91.02428571428571" type="appl" />
         <tli id="T86" time="91.43714285714285" type="appl" />
         <tli id="T87" time="91.86333203660011" />
         <tli id="T88" time="92.9011111111111" type="appl" />
         <tli id="T89" time="93.95222222222222" type="appl" />
         <tli id="T90" time="95.00333333333333" type="appl" />
         <tli id="T91" time="96.05444444444444" type="appl" />
         <tli id="T92" time="97.10555555555555" type="appl" />
         <tli id="T93" time="98.15666666666667" type="appl" />
         <tli id="T94" time="99.20777777777778" type="appl" />
         <tli id="T95" time="100.25888888888889" type="appl" />
         <tli id="T96" time="102.01973827240958" />
         <tli id="T97" time="102.18125" type="appl" />
         <tli id="T98" time="103.05250000000001" type="appl" />
         <tli id="T99" time="103.92375" type="appl" />
         <tli id="T100" time="104.795" type="appl" />
         <tli id="T101" time="105.66625" type="appl" />
         <tli id="T102" time="106.5375" type="appl" />
         <tli id="T103" time="107.40875" type="appl" />
         <tli id="T104" time="108.37333525375863" />
         <tli id="T105" time="109.17121739130435" type="appl" />
         <tli id="T106" time="110.06243478260869" type="appl" />
         <tli id="T107" time="110.95365217391304" type="appl" />
         <tli id="T108" time="111.8448695652174" type="appl" />
         <tli id="T109" time="112.73608695652175" type="appl" />
         <tli id="T110" time="113.62730434782608" type="appl" />
         <tli id="T111" time="114.51852173913043" type="appl" />
         <tli id="T112" time="115.40973913043479" type="appl" />
         <tli id="T113" time="116.30095652173912" type="appl" />
         <tli id="T114" time="117.19217391304348" type="appl" />
         <tli id="T115" time="118.08339130434783" type="appl" />
         <tli id="T116" time="118.97460869565217" type="appl" />
         <tli id="T117" time="119.86582608695652" type="appl" />
         <tli id="T118" time="120.75704347826087" type="appl" />
         <tli id="T119" time="121.6482608695652" type="appl" />
         <tli id="T120" time="122.53947826086956" type="appl" />
         <tli id="T121" time="123.43069565217391" type="appl" />
         <tli id="T122" time="124.32191304347825" type="appl" />
         <tli id="T123" time="125.2131304347826" type="appl" />
         <tli id="T124" time="126.10434782608695" type="appl" />
         <tli id="T125" time="126.99556521739129" type="appl" />
         <tli id="T126" time="127.88678260869564" type="appl" />
         <tli id="T127" time="129.27966833814065" />
         <tli id="T128" time="129.58536363636364" type="appl" />
         <tli id="T129" time="130.39272727272726" type="appl" />
         <tli id="T130" time="131.2000909090909" type="appl" />
         <tli id="T131" time="132.00745454545455" type="appl" />
         <tli id="T132" time="132.81481818181817" type="appl" />
         <tli id="T133" time="133.62218181818182" type="appl" />
         <tli id="T134" time="134.42954545454543" type="appl" />
         <tli id="T135" time="135.23690909090908" type="appl" />
         <tli id="T136" time="136.04427272727273" type="appl" />
         <tli id="T137" time="136.85163636363635" type="appl" />
         <tli id="T138" time="137.659" type="appl" />
         <tli id="T139" time="139.10174999999998" type="appl" />
         <tli id="T140" time="140.5445" type="appl" />
         <tli id="T141" time="141.98725000000002" type="appl" />
         <tli id="T142" time="143.48999646544104" />
         <tli id="T143" time="145.2055" type="appl" />
         <tli id="T144" time="146.981" type="appl" />
         <tli id="T145" time="148.75650000000002" type="appl" />
         <tli id="T146" time="150.532" type="appl" />
         <tli id="T147" time="152.3075" type="appl" />
         <tli id="T148" time="154.083" type="appl" />
         <tli id="T149" time="155.8585" type="appl" />
         <tli id="T150" time="157.63400000000001" type="appl" />
         <tli id="T151" time="159.4095" type="appl" />
         <tli id="T152" time="161.185" type="appl" />
         <tli id="T153" time="162.9605" type="appl" />
         <tli id="T154" time="164.736" type="appl" />
         <tli id="T155" time="166.5115" type="appl" />
         <tli id="T156" time="168.287" type="appl" />
         <tli id="T157" time="169.07685714285714" type="appl" />
         <tli id="T158" time="169.8667142857143" type="appl" />
         <tli id="T159" time="170.65657142857143" type="appl" />
         <tli id="T160" time="171.44642857142858" type="appl" />
         <tli id="T161" time="172.2362857142857" type="appl" />
         <tli id="T162" time="173.02614285714287" type="appl" />
         <tli id="T163" time="173.816" type="appl" />
         <tli id="T164" time="174.80594444444444" type="appl" />
         <tli id="T165" time="175.7958888888889" type="appl" />
         <tli id="T166" time="176.78583333333333" type="appl" />
         <tli id="T167" time="177.7757777777778" type="appl" />
         <tli id="T168" time="178.76572222222222" type="appl" />
         <tli id="T169" time="179.75566666666666" type="appl" />
         <tli id="T170" time="180.74561111111112" type="appl" />
         <tli id="T171" time="181.73555555555555" type="appl" />
         <tli id="T172" time="182.7255" type="appl" />
         <tli id="T173" time="183.71544444444444" type="appl" />
         <tli id="T174" time="184.70538888888888" type="appl" />
         <tli id="T175" time="185.69533333333334" type="appl" />
         <tli id="T176" time="186.68527777777777" type="appl" />
         <tli id="T177" time="187.6752222222222" type="appl" />
         <tli id="T178" time="188.66516666666666" type="appl" />
         <tli id="T179" time="189.6551111111111" type="appl" />
         <tli id="T180" time="190.64505555555556" type="appl" />
         <tli id="T181" time="192.53283939797353" />
         <tli id="T182" time="192.85329411764704" type="appl" />
         <tli id="T183" time="194.07158823529412" type="appl" />
         <tli id="T184" time="195.28988235294116" type="appl" />
         <tli id="T185" time="196.50817647058824" type="appl" />
         <tli id="T186" time="197.7264705882353" type="appl" />
         <tli id="T187" time="198.94476470588233" type="appl" />
         <tli id="T188" time="200.1630588235294" type="appl" />
         <tli id="T189" time="201.38135294117646" type="appl" />
         <tli id="T190" time="202.59964705882354" type="appl" />
         <tli id="T191" time="203.81794117647058" type="appl" />
         <tli id="T192" time="205.03623529411766" type="appl" />
         <tli id="T193" time="206.2545294117647" type="appl" />
         <tli id="T194" time="207.47282352941176" type="appl" />
         <tli id="T195" time="208.69111764705883" type="appl" />
         <tli id="T196" time="209.90941176470588" type="appl" />
         <tli id="T197" time="211.12770588235296" type="appl" />
         <tli id="T198" time="212.99278690879004" />
         <tli id="T199" time="213.0126" type="appl" />
         <tli id="T200" time="213.6792" type="appl" />
         <tli id="T201" time="214.3458" type="appl" />
         <tli id="T202" time="215.0124" type="appl" />
         <tli id="T203" time="215.679" type="appl" />
         <tli id="T204" time="216.3456" type="appl" />
         <tli id="T205" time="217.0122" type="appl" />
         <tli id="T206" time="217.6788" type="appl" />
         <tli id="T207" time="218.3454" type="appl" />
         <tli id="T208" time="219.012" type="appl" />
         <tli id="T209" time="219.6786" type="appl" />
         <tli id="T210" time="220.3452" type="appl" />
         <tli id="T211" time="221.0118" type="appl" />
         <tli id="T212" time="221.6784" type="appl" />
         <tli id="T213" time="223.1927607411619" />
         <tli id="T214" time="223.19809523809525" type="appl" />
         <tli id="T215" time="224.05119047619047" type="appl" />
         <tli id="T216" time="224.90428571428572" type="appl" />
         <tli id="T217" time="225.75738095238094" type="appl" />
         <tli id="T218" time="226.6104761904762" type="appl" />
         <tli id="T219" time="227.4635714285714" type="appl" />
         <tli id="T220" time="228.31666666666666" type="appl" />
         <tli id="T221" time="229.1697619047619" type="appl" />
         <tli id="T222" time="230.02285714285713" type="appl" />
         <tli id="T223" time="230.87595238095238" type="appl" />
         <tli id="T224" time="231.7290476190476" type="appl" />
         <tli id="T225" time="232.58214285714286" type="appl" />
         <tli id="T226" time="233.43523809523808" type="appl" />
         <tli id="T227" time="234.28833333333333" type="appl" />
         <tli id="T228" time="235.14142857142858" type="appl" />
         <tli id="T229" time="235.9945238095238" type="appl" />
         <tli id="T230" time="236.84761904761905" type="appl" />
         <tli id="T231" time="237.70071428571427" type="appl" />
         <tli id="T232" time="238.55380952380952" type="appl" />
         <tli id="T233" time="239.40690476190474" type="appl" />
         <tli id="T234" time="240.44667481026332" />
         <tli id="T235" time="240.79235294117646" type="appl" />
         <tli id="T236" time="241.32470588235293" type="appl" />
         <tli id="T237" time="241.8570588235294" type="appl" />
         <tli id="T238" time="242.38941176470587" type="appl" />
         <tli id="T239" time="242.92176470588234" type="appl" />
         <tli id="T240" time="243.4541176470588" type="appl" />
         <tli id="T241" time="243.98647058823528" type="appl" />
         <tli id="T242" time="244.51882352941175" type="appl" />
         <tli id="T243" time="245.05117647058825" type="appl" />
         <tli id="T244" time="245.58352941176472" type="appl" />
         <tli id="T245" time="246.11588235294118" type="appl" />
         <tli id="T246" time="246.64823529411765" type="appl" />
         <tli id="T247" time="247.18058823529412" type="appl" />
         <tli id="T248" time="247.7129411764706" type="appl" />
         <tli id="T249" time="248.24529411764706" type="appl" />
         <tli id="T250" time="248.77764705882353" type="appl" />
         <tli id="T251" time="249.3900112414886" />
         <tli id="T252" time="249.78676923076924" type="appl" />
         <tli id="T253" time="250.26353846153847" type="appl" />
         <tli id="T254" time="250.7403076923077" type="appl" />
         <tli id="T255" time="251.21707692307692" type="appl" />
         <tli id="T256" time="251.69384615384615" type="appl" />
         <tli id="T257" time="252.1706153846154" type="appl" />
         <tli id="T258" time="252.64738461538462" type="appl" />
         <tli id="T259" time="253.12415384615386" type="appl" />
         <tli id="T260" time="253.6009230769231" type="appl" />
         <tli id="T261" time="254.0776923076923" type="appl" />
         <tli id="T262" time="254.55446153846154" type="appl" />
         <tli id="T263" time="255.03123076923077" type="appl" />
         <tli id="T264" time="255.56132353423178" />
         <tli id="T265" time="255.81547368421053" type="appl" />
         <tli id="T266" time="256.1229473684211" type="appl" />
         <tli id="T267" time="256.4304210526316" type="appl" />
         <tli id="T268" time="256.7378947368421" type="appl" />
         <tli id="T269" time="257.0453684210527" type="appl" />
         <tli id="T270" time="257.35284210526316" type="appl" />
         <tli id="T271" time="257.6603157894737" type="appl" />
         <tli id="T272" time="257.9677894736842" type="appl" />
         <tli id="T273" time="258.27526315789476" type="appl" />
         <tli id="T274" time="258.5827368421053" type="appl" />
         <tli id="T275" time="258.8902105263158" type="appl" />
         <tli id="T276" time="259.19768421052635" type="appl" />
         <tli id="T277" time="259.50515789473684" type="appl" />
         <tli id="T278" time="259.8126315789474" type="appl" />
         <tli id="T279" time="260.12010526315794" type="appl" />
         <tli id="T280" time="260.42757894736843" type="appl" />
         <tli id="T281" time="260.735052631579" type="appl" />
         <tli id="T282" time="261.0425263157895" type="appl" />
         <tli id="T283" time="261.2966734037526" />
         <tli id="T284" time="261.45667299328" />
         <tli id="T285" time="262.23675000000003" type="appl" />
         <tli id="T286" time="263.0635" type="appl" />
         <tli id="T287" time="263.89025" type="appl" />
         <tli id="T288" time="264.7503364185172" />
         <tli id="T289" time="265.45099999999996" type="appl" />
         <tli id="T290" time="266.185" type="appl" />
         <tli id="T291" time="266.919" type="appl" />
         <tli id="T292" time="267.653" type="appl" />
         <tli id="T293" time="268.387" type="appl" />
         <tli id="T294" time="269.121" type="appl" />
         <tli id="T295" time="269.855" type="appl" />
         <tli id="T296" time="270.589" type="appl" />
         <tli id="T297" time="271.323" type="appl" />
         <tli id="T298" time="272.057" type="appl" />
         <tli id="T299" time="272.791" type="appl" />
         <tli id="T300" time="273.52500000000003" type="appl" />
         <tli id="T301" time="274.53900922273215" />
         <tli id="T302" time="275.2845" type="appl" />
         <tli id="T303" time="276.48332715132835" />
         <tli id="T304" time="276.9433333333333" type="appl" />
         <tli id="T305" time="277.57666666666665" type="appl" />
         <tli id="T306" time="278.21" type="appl" />
         <tli id="T307" time="278.8433333333333" type="appl" />
         <tli id="T308" time="279.4766666666667" type="appl" />
         <tli id="T309" time="280.11" type="appl" />
         <tli id="T310" time="280.74333333333334" type="appl" />
         <tli id="T311" time="281.37666666666667" type="appl" />
         <tli id="T312" time="282.1366720228964" />
         <tli id="T313" time="282.9073913043478" type="appl" />
         <tli id="T314" time="283.80478260869563" type="appl" />
         <tli id="T315" time="284.70217391304345" type="appl" />
         <tli id="T316" time="285.5995652173913" type="appl" />
         <tli id="T317" time="286.4969565217391" type="appl" />
         <tli id="T318" time="287.39434782608697" type="appl" />
         <tli id="T319" time="288.2917391304348" type="appl" />
         <tli id="T320" time="289.1891304347826" type="appl" />
         <tli id="T321" time="290.08652173913043" type="appl" />
         <tli id="T322" time="290.98391304347825" type="appl" />
         <tli id="T323" time="291.8813043478261" type="appl" />
         <tli id="T324" time="292.7786956521739" type="appl" />
         <tli id="T325" time="293.6760869565217" type="appl" />
         <tli id="T326" time="294.57347826086954" type="appl" />
         <tli id="T327" time="295.47086956521736" type="appl" />
         <tli id="T328" time="296.3682608695652" type="appl" />
         <tli id="T329" time="297.26565217391305" type="appl" />
         <tli id="T330" time="298.1630434782609" type="appl" />
         <tli id="T331" time="299.0604347826087" type="appl" />
         <tli id="T332" time="299.9578260869565" type="appl" />
         <tli id="T333" time="300.85521739130434" type="appl" />
         <tli id="T334" time="301.75260869565216" type="appl" />
         <tli id="T335" time="302.65" type="appl" />
         <tli id="T336" time="303.35982352941176" type="appl" />
         <tli id="T337" time="304.06964705882353" type="appl" />
         <tli id="T338" time="304.77947058823526" type="appl" />
         <tli id="T339" time="305.48929411764703" type="appl" />
         <tli id="T340" time="306.1991176470588" type="appl" />
         <tli id="T341" time="306.9089411764706" type="appl" />
         <tli id="T342" time="307.6187647058823" type="appl" />
         <tli id="T343" time="308.3285882352941" type="appl" />
         <tli id="T344" time="309.03841176470587" type="appl" />
         <tli id="T345" time="309.74823529411765" type="appl" />
         <tli id="T346" time="310.45805882352937" type="appl" />
         <tli id="T347" time="311.16788235294115" type="appl" />
         <tli id="T348" time="311.8777058823529" type="appl" />
         <tli id="T349" time="312.5875294117647" type="appl" />
         <tli id="T350" time="313.2973529411764" type="appl" />
         <tli id="T351" time="314.0071764705882" type="appl" />
         <tli id="T352" time="315.87918962447304" />
         <tli id="T353" time="315.9680588235294" type="appl" />
         <tli id="T354" time="317.2191176470588" type="appl" />
         <tli id="T355" time="318.4701764705882" type="appl" />
         <tli id="T356" time="319.72123529411766" type="appl" />
         <tli id="T357" time="320.97229411764704" type="appl" />
         <tli id="T358" time="322.2233529411765" type="appl" />
         <tli id="T359" time="323.4744117647059" type="appl" />
         <tli id="T360" time="324.7254705882353" type="appl" />
         <tli id="T361" time="325.9765294117647" type="appl" />
         <tli id="T362" time="327.2275882352941" type="appl" />
         <tli id="T363" time="328.4786470588235" type="appl" />
         <tli id="T364" time="329.72970588235296" type="appl" />
         <tli id="T365" time="330.98076470588234" type="appl" />
         <tli id="T366" time="332.23182352941177" type="appl" />
         <tli id="T367" time="333.4828823529412" type="appl" />
         <tli id="T368" time="334.7339411764706" type="appl" />
         <tli id="T369" time="337.4124677150359" />
         <tli id="T370" time="337.5081668824877" type="intp" />
         <tli id="T371" time="337.6038660499395" type="intp" />
         <tli id="T372" time="337.6995652173913" type="appl" />
         <tli id="T373" time="338.27108695652174" type="appl" />
         <tli id="T374" time="338.8426086956522" type="appl" />
         <tli id="T375" time="339.41413043478263" type="appl" />
         <tli id="T376" time="339.985652173913" type="appl" />
         <tli id="T377" time="340.55717391304347" type="appl" />
         <tli id="T378" time="341.1286956521739" type="appl" />
         <tli id="T379" time="341.70021739130436" type="appl" />
         <tli id="T380" time="342.2717391304348" type="appl" />
         <tli id="T381" time="342.8432608695652" type="appl" />
         <tli id="T382" time="343.41478260869565" type="appl" />
         <tli id="T383" time="343.9863043478261" type="appl" />
         <tli id="T384" time="344.55782608695654" type="appl" />
         <tli id="T385" time="345.129347826087" type="appl" />
         <tli id="T386" time="345.7008695652174" type="appl" />
         <tli id="T387" time="346.2723913043478" type="appl" />
         <tli id="T388" time="346.84391304347827" type="appl" />
         <tli id="T389" time="347.4154347826087" type="appl" />
         <tli id="T390" time="347.9869565217391" type="appl" />
         <tli id="T391" time="348.55847826086955" type="appl" />
         <tli id="T392" time="349.3499891731316" />
         <tli id="T393" time="349.70545454545453" type="appl" />
         <tli id="T394" time="350.28090909090906" type="appl" />
         <tli id="T395" time="350.85636363636365" type="appl" />
         <tli id="T396" time="351.4318181818182" type="appl" />
         <tli id="T397" time="352.0072727272727" type="appl" />
         <tli id="T398" time="352.58272727272725" type="appl" />
         <tli id="T399" time="353.1581818181818" type="appl" />
         <tli id="T400" time="353.7336363636363" type="appl" />
         <tli id="T401" time="354.3090909090909" type="appl" />
         <tli id="T402" time="354.88454545454545" type="appl" />
         <tli id="T403" time="355.61333247977285" />
         <tli id="T404" time="356.26884615384614" type="appl" />
         <tli id="T405" time="357.0776923076923" type="appl" />
         <tli id="T406" time="357.88653846153846" type="appl" />
         <tli id="T407" time="358.6953846153846" type="appl" />
         <tli id="T408" time="359.5042307692308" type="appl" />
         <tli id="T409" time="360.31307692307695" type="appl" />
         <tli id="T410" time="361.12192307692305" type="appl" />
         <tli id="T411" time="361.9307692307692" type="appl" />
         <tli id="T412" time="362.7396153846154" type="appl" />
         <tli id="T413" time="363.54846153846154" type="appl" />
         <tli id="T414" time="364.3573076923077" type="appl" />
         <tli id="T415" time="365.16615384615386" type="appl" />
         <tli id="T416" time="367.3857241531691" />
         <tli id="T417" time="367.4209620765846" type="intp" />
         <tli id="T418" time="367.4562" type="appl" />
         <tli id="T419" time="368.1968" type="appl" />
         <tli id="T420" time="368.9374" type="appl" />
         <tli id="T421" time="369.678" type="appl" />
         <tli id="T422" time="370.7253333333333" type="appl" />
         <tli id="T423" time="371.7726666666667" type="appl" />
         <tli id="T424" time="372.82" type="appl" />
         <tli id="T425" time="373.8673333333333" type="appl" />
         <tli id="T426" time="374.9146666666667" type="appl" />
         <tli id="T427" time="376.2153369178146" />
         <tli id="T428" time="376.5659333333333" type="appl" />
         <tli id="T429" time="377.1698666666667" type="appl" />
         <tli id="T430" time="377.7738" type="appl" />
         <tli id="T431" time="378.3777333333333" type="appl" />
         <tli id="T432" time="378.9816666666667" type="appl" />
         <tli id="T433" time="379.5856" type="appl" />
         <tli id="T434" time="380.1895333333333" type="appl" />
         <tli id="T435" time="380.79346666666663" type="appl" />
         <tli id="T436" time="381.3974" type="appl" />
         <tli id="T437" time="382.0013333333333" type="appl" />
         <tli id="T438" time="382.60526666666664" type="appl" />
         <tli id="T439" time="383.2092" type="appl" />
         <tli id="T440" time="383.8131333333333" type="appl" />
         <tli id="T441" time="384.41706666666664" type="appl" />
         <tli id="T442" time="385.02099999999996" type="appl" />
         <tli id="T443" time="385.62493333333333" type="appl" />
         <tli id="T444" time="386.22886666666665" type="appl" />
         <tli id="T445" time="386.83279999999996" type="appl" />
         <tli id="T446" time="387.43673333333334" type="appl" />
         <tli id="T447" time="388.04066666666665" type="appl" />
         <tli id="T448" time="388.64459999999997" type="appl" />
         <tli id="T449" time="389.24853333333334" type="appl" />
         <tli id="T450" time="389.85246666666666" type="appl" />
         <tli id="T451" time="390.4564" type="appl" />
         <tli id="T452" time="391.06033333333335" type="appl" />
         <tli id="T453" time="391.66426666666666" type="appl" />
         <tli id="T454" time="392.2682" type="appl" />
         <tli id="T455" time="392.8721333333333" type="appl" />
         <tli id="T456" time="393.47606666666667" type="appl" />
         <tli id="T457" time="394.193337670967" />
         <tli id="T458" time="394.60066666666665" type="appl" />
         <tli id="T459" time="395.1213333333333" type="appl" />
         <tli id="T460" time="395.642" type="appl" />
         <tli id="T461" time="396.16266666666667" type="appl" />
         <tli id="T462" time="396.68333333333334" type="appl" />
         <tli id="T463" time="397.20399999999995" type="appl" />
         <tli id="T464" time="397.7246666666666" type="appl" />
         <tli id="T465" time="398.2453333333333" type="appl" />
         <tli id="T466" time="398.76599999999996" type="appl" />
         <tli id="T467" time="399.28666666666663" type="appl" />
         <tli id="T468" time="399.8073333333333" type="appl" />
         <tli id="T469" time="400.46134242398284" />
         <tli id="T470" time="400.89937499999996" type="appl" />
         <tli id="T471" time="401.47074999999995" type="appl" />
         <tli id="T472" time="402.042125" type="appl" />
         <tli id="T473" time="402.6135" type="appl" />
         <tli id="T474" time="403.184875" type="appl" />
         <tli id="T475" time="403.75625" type="appl" />
         <tli id="T476" time="404.327625" type="appl" />
         <tli id="T477" time="404.899" type="appl" />
         <tli id="T478" time="405.6052" type="appl" />
         <tli id="T479" time="406.3114" type="appl" />
         <tli id="T480" time="407.0176" type="appl" />
         <tli id="T481" time="407.7238" type="appl" />
         <tli id="T482" time="408.43" type="appl" />
         <tli id="T483" time="409.13620000000003" type="appl" />
         <tli id="T484" time="409.8424" type="appl" />
         <tli id="T485" time="410.5486" type="appl" />
         <tli id="T486" time="411.2548" type="appl" />
         <tli id="T487" time="411.961" type="appl" />
         <tli id="T488" time="412.4581111111111" type="appl" />
         <tli id="T489" time="412.95522222222223" type="appl" />
         <tli id="T490" time="413.45233333333334" type="appl" />
         <tli id="T491" time="413.94944444444445" type="appl" />
         <tli id="T492" time="414.44655555555556" type="appl" />
         <tli id="T493" time="414.9436666666667" type="appl" />
         <tli id="T494" time="415.4407777777778" type="appl" />
         <tli id="T495" time="415.9378888888889" type="appl" />
         <tli id="T496" time="416.435" type="appl" />
         <tli id="T497" time="416.9321111111111" type="appl" />
         <tli id="T498" time="417.4292222222222" type="appl" />
         <tli id="T499" time="417.92633333333333" type="appl" />
         <tli id="T500" time="418.42344444444444" type="appl" />
         <tli id="T501" time="418.92055555555555" type="appl" />
         <tli id="T502" time="419.41766666666666" type="appl" />
         <tli id="T503" time="419.9147777777778" type="appl" />
         <tli id="T504" time="420.4118888888889" type="appl" />
         <tli id="T505" time="421.0689978891939" />
         <tli id="T506" time="421.27205" type="appl" />
         <tli id="T507" time="421.63509999999997" type="appl" />
         <tli id="T508" time="421.99815" type="appl" />
         <tli id="T509" time="422.3612" type="appl" />
         <tli id="T510" time="422.72425" type="appl" />
         <tli id="T511" time="423.0873" type="appl" />
         <tli id="T512" time="423.45035" type="appl" />
         <tli id="T513" time="423.8134" type="appl" />
         <tli id="T514" time="424.17645" type="appl" />
         <tli id="T515" time="424.5395" type="appl" />
         <tli id="T516" time="424.90255" type="appl" />
         <tli id="T517" time="425.2656" type="appl" />
         <tli id="T518" time="425.62865" type="appl" />
         <tli id="T519" time="425.99170000000004" type="appl" />
         <tli id="T520" time="426.35475" type="appl" />
         <tli id="T521" time="426.7178" type="appl" />
         <tli id="T522" time="427.08085" type="appl" />
         <tli id="T523" time="427.4439" type="appl" />
         <tli id="T524" time="427.80695000000003" type="appl" />
         <tli id="T525" time="428.17" type="appl" />
         <tli id="T526" time="428.4947619047619" type="appl" />
         <tli id="T527" time="428.81952380952384" type="appl" />
         <tli id="T528" time="429.14428571428573" type="appl" />
         <tli id="T529" time="429.4690476190476" type="appl" />
         <tli id="T530" time="429.79380952380956" type="appl" />
         <tli id="T531" time="430.11857142857144" type="appl" />
         <tli id="T532" time="430.4433333333333" type="appl" />
         <tli id="T533" time="430.76809523809527" type="appl" />
         <tli id="T534" time="431.09285714285716" type="appl" />
         <tli id="T535" time="431.41761904761904" type="appl" />
         <tli id="T536" time="431.742380952381" type="appl" />
         <tli id="T537" time="432.06714285714287" type="appl" />
         <tli id="T538" time="432.39190476190475" type="appl" />
         <tli id="T539" time="432.7166666666667" type="appl" />
         <tli id="T540" time="433.0414285714286" type="appl" />
         <tli id="T541" time="433.36619047619047" type="appl" />
         <tli id="T542" time="433.6909523809524" type="appl" />
         <tli id="T543" time="434.0157142857143" type="appl" />
         <tli id="T544" time="434.3404761904762" type="appl" />
         <tli id="T545" time="434.6652380952381" type="appl" />
         <tli id="T546" time="434.99" type="appl" />
         <tli id="T547" time="435.7572" type="appl" />
         <tli id="T548" time="436.5244" type="appl" />
         <tli id="T549" time="437.2916" type="appl" />
         <tli id="T550" time="438.0588" type="appl" />
         <tli id="T551" time="438.826" type="appl" />
         <tli id="T552" time="439.59319999999997" type="appl" />
         <tli id="T553" time="440.36039999999997" type="appl" />
         <tli id="T554" time="441.1276" type="appl" />
         <tli id="T555" time="441.8948" type="appl" />
         <tli id="T556" time="443.08865494030414" />
         <tli id="T557" time="443.2360714285714" type="appl" />
         <tli id="T558" time="443.81014285714286" type="appl" />
         <tli id="T559" time="444.3842142857143" type="appl" />
         <tli id="T560" time="444.9582857142857" type="appl" />
         <tli id="T561" time="445.5323571428571" type="appl" />
         <tli id="T562" time="446.1064285714286" type="appl" />
         <tli id="T563" time="446.6805" type="appl" />
         <tli id="T564" time="447.2545714285714" type="appl" />
         <tli id="T565" time="447.8286428571429" type="appl" />
         <tli id="T566" time="448.4027142857143" type="appl" />
         <tli id="T567" time="448.9767857142857" type="appl" />
         <tli id="T568" time="449.5508571428571" type="appl" />
         <tli id="T569" time="450.1249285714286" type="appl" />
         <tli id="T570" time="450.699" type="appl" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-UkOA"
                      id="tx-UkOA"
                      speaker="UkOA"
                      type="t">
         <timeline-fork end="T112" start="T111">
            <tli id="T111.tx-UkOA.1" />
            <tli id="T111.tx-UkOA.2" />
         </timeline-fork>
         <timeline-fork end="T170" start="T169">
            <tli id="T169.tx-UkOA.1" />
         </timeline-fork>
         <timeline-fork end="T382" start="T381">
            <tli id="T381.tx-UkOA.1" />
         </timeline-fork>
         <timeline-fork end="T409" start="T408">
            <tli id="T408.tx-UkOA.1" />
            <tli id="T408.tx-UkOA.2" />
            <tli id="T408.tx-UkOA.3" />
         </timeline-fork>
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-UkOA">
            <ts e="T4" id="Seg_0" n="sc" s="T1">
               <ts e="T4" id="Seg_2" n="HIAT:u" s="T1">
                  <nts id="Seg_3" n="HIAT:ip">–</nts>
                  <nts id="Seg_4" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_6" n="HIAT:w" s="T1">Hakalɨː</ts>
                  <nts id="Seg_7" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T3" id="Seg_9" n="HIAT:w" s="T2">kepsi͡em</ts>
                  <nts id="Seg_10" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T4" id="Seg_12" n="HIAT:w" s="T3">du͡o</ts>
                  <nts id="Seg_13" n="HIAT:ip">?</nts>
                  <nts id="Seg_14" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T283" id="Seg_15" n="sc" s="T8">
               <ts e="T24" id="Seg_17" n="HIAT:u" s="T8">
                  <nts id="Seg_18" n="HIAT:ip">–</nts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T10" id="Seg_21" n="HIAT:w" s="T8">Hürbe</ts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_24" n="HIAT:w" s="T10">ikkileːk</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_27" n="HIAT:w" s="T11">ijunʼŋa</ts>
                  <nts id="Seg_28" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T13" id="Seg_30" n="HIAT:w" s="T12">ikki</ts>
                  <nts id="Seg_31" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_33" n="HIAT:w" s="T13">hüːs</ts>
                  <nts id="Seg_34" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T15" id="Seg_36" n="HIAT:w" s="T14">u͡onnaːk</ts>
                  <nts id="Seg_37" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_39" n="HIAT:w" s="T15">dʼɨlga</ts>
                  <nts id="Seg_40" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_42" n="HIAT:w" s="T16">Xantɨ-Mansijskaj</ts>
                  <nts id="Seg_43" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_45" n="HIAT:w" s="T17">gorakka</ts>
                  <nts id="Seg_46" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_48" n="HIAT:w" s="T18">bu͡olbuta</ts>
                  <nts id="Seg_49" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_51" n="HIAT:w" s="T19">fʼestʼival</ts>
                  <nts id="Seg_52" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_53" n="HIAT:ip">"</nts>
                  <ts e="T21" id="Seg_55" n="HIAT:w" s="T20">Sʼevʼernaje</ts>
                  <nts id="Seg_56" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_58" n="HIAT:w" s="T21">sʼijanʼije</ts>
                  <nts id="Seg_59" n="HIAT:ip">"</nts>
                  <nts id="Seg_60" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_62" n="HIAT:w" s="T22">di͡en</ts>
                  <nts id="Seg_63" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T24" id="Seg_65" n="HIAT:w" s="T23">aːttaːk</ts>
                  <nts id="Seg_66" n="HIAT:ip">.</nts>
                  <nts id="Seg_67" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T30" id="Seg_69" n="HIAT:u" s="T24">
                  <ts e="T25" id="Seg_71" n="HIAT:w" s="T24">Onno</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_74" n="HIAT:w" s="T25">Tajmɨːrtan</ts>
                  <nts id="Seg_75" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T27" id="Seg_77" n="HIAT:w" s="T26">bihigi</ts>
                  <nts id="Seg_78" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_80" n="HIAT:w" s="T27">barbɨppɨt</ts>
                  <nts id="Seg_81" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_83" n="HIAT:w" s="T28">ikki</ts>
                  <nts id="Seg_84" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_86" n="HIAT:w" s="T29">kihi</ts>
                  <nts id="Seg_87" n="HIAT:ip">.</nts>
                  <nts id="Seg_88" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T47" id="Seg_90" n="HIAT:u" s="T30">
                  <ts e="T31" id="Seg_92" n="HIAT:w" s="T30">Birges</ts>
                  <nts id="Seg_93" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_95" n="HIAT:w" s="T31">bu͡olla</ts>
                  <nts id="Seg_96" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_98" n="HIAT:w" s="T32">Vladʼimʼir</ts>
                  <nts id="Seg_99" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_101" n="HIAT:w" s="T33">Eːnovʼičʼ</ts>
                  <nts id="Seg_102" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_104" n="HIAT:w" s="T34">Sʼigunʼej</ts>
                  <nts id="Seg_105" n="HIAT:ip">,</nts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_107" n="HIAT:ip">"</nts>
                  <ts e="T36" id="Seg_109" n="HIAT:w" s="T35">Naroːdnaj</ts>
                  <nts id="Seg_110" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_112" n="HIAT:w" s="T36">Artʼist</ts>
                  <nts id="Seg_113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_115" n="HIAT:w" s="T37">Rassʼii</ts>
                  <nts id="Seg_116" n="HIAT:ip">"</nts>
                  <nts id="Seg_117" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_119" n="HIAT:w" s="T38">di͡en</ts>
                  <nts id="Seg_120" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_122" n="HIAT:w" s="T39">kihi</ts>
                  <nts id="Seg_123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_125" n="HIAT:w" s="T40">aːttaːk</ts>
                  <nts id="Seg_126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_127" n="HIAT:ip">"</nts>
                  <ts e="T42" id="Seg_129" n="HIAT:w" s="T41">Zaslužennɨj</ts>
                  <nts id="Seg_130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_132" n="HIAT:w" s="T42">artʼist</ts>
                  <nts id="Seg_133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_135" n="HIAT:w" s="T43">Rassʼii</ts>
                  <nts id="Seg_136" n="HIAT:ip">"</nts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_139" n="HIAT:w" s="T44">i</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_142" n="HIAT:w" s="T45">min</ts>
                  <nts id="Seg_143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_145" n="HIAT:w" s="T46">Nasku͡ottan</ts>
                  <nts id="Seg_146" n="HIAT:ip">.</nts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T54" id="Seg_149" n="HIAT:u" s="T47">
                  <ts e="T48" id="Seg_151" n="HIAT:w" s="T47">Onno</ts>
                  <nts id="Seg_152" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T49" id="Seg_154" n="HIAT:w" s="T48">bert</ts>
                  <nts id="Seg_155" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_157" n="HIAT:w" s="T49">üčügejdik</ts>
                  <nts id="Seg_158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_160" n="HIAT:w" s="T50">bihigitin</ts>
                  <nts id="Seg_161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_163" n="HIAT:w" s="T51">tohujdular</ts>
                  <nts id="Seg_164" n="HIAT:ip">,</nts>
                  <nts id="Seg_165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_167" n="HIAT:w" s="T52">tuːstaːk</ts>
                  <nts id="Seg_168" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_170" n="HIAT:w" s="T53">kili͡ebinen</ts>
                  <nts id="Seg_171" n="HIAT:ip">.</nts>
                  <nts id="Seg_172" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T64" id="Seg_174" n="HIAT:u" s="T54">
                  <ts e="T55" id="Seg_176" n="HIAT:w" s="T54">Ulakan</ts>
                  <nts id="Seg_177" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_179" n="HIAT:w" s="T55">bagaj</ts>
                  <nts id="Seg_180" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_182" n="HIAT:w" s="T56">gastʼinʼisaga</ts>
                  <nts id="Seg_183" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T58" id="Seg_185" n="HIAT:w" s="T57">dʼi͡eleːtiler</ts>
                  <nts id="Seg_186" n="HIAT:ip">,</nts>
                  <nts id="Seg_187" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_189" n="HIAT:w" s="T58">aːta</ts>
                  <nts id="Seg_190" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_192" n="HIAT:w" s="T59">bu͡olla</ts>
                  <nts id="Seg_193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_194" n="HIAT:ip">"</nts>
                  <ts e="T61" id="Seg_196" n="HIAT:w" s="T60">Hette</ts>
                  <nts id="Seg_197" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_199" n="HIAT:w" s="T61">girbej</ts>
                  <nts id="Seg_200" n="HIAT:ip">"</nts>
                  <nts id="Seg_201" n="HIAT:ip">,</nts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_203" n="HIAT:ip">"</nts>
                  <ts e="T63" id="Seg_205" n="HIAT:w" s="T62">Semʼ</ts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_208" n="HIAT:w" s="T63">xalmov</ts>
                  <nts id="Seg_209" n="HIAT:ip">"</nts>
                  <nts id="Seg_210" n="HIAT:ip">.</nts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T74" id="Seg_213" n="HIAT:u" s="T64">
                  <ts e="T65" id="Seg_215" n="HIAT:w" s="T64">Bert</ts>
                  <nts id="Seg_216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_218" n="HIAT:w" s="T65">üčügejdik</ts>
                  <nts id="Seg_219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_221" n="HIAT:w" s="T66">bihigini</ts>
                  <nts id="Seg_222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T68" id="Seg_224" n="HIAT:w" s="T67">ahatallar</ts>
                  <nts id="Seg_225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T69" id="Seg_227" n="HIAT:w" s="T68">onno</ts>
                  <nts id="Seg_228" n="HIAT:ip">,</nts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_231" n="HIAT:w" s="T69">ahappɨttara</ts>
                  <nts id="Seg_232" n="HIAT:ip">,</nts>
                  <nts id="Seg_233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_235" n="HIAT:w" s="T70">utupputtara</ts>
                  <nts id="Seg_236" n="HIAT:ip">,</nts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_239" n="HIAT:w" s="T71">onton</ts>
                  <nts id="Seg_240" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_242" n="HIAT:w" s="T72">harsɨŋŋɨtɨn</ts>
                  <nts id="Seg_243" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_245" n="HIAT:w" s="T73">rʼepʼetʼicɨjalaːppɨt</ts>
                  <nts id="Seg_246" n="HIAT:ip">.</nts>
                  <nts id="Seg_247" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T80" id="Seg_249" n="HIAT:u" s="T74">
                  <ts e="T75" id="Seg_251" n="HIAT:w" s="T74">Hol</ts>
                  <nts id="Seg_252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_254" n="HIAT:w" s="T75">ki͡ehetin</ts>
                  <nts id="Seg_255" n="HIAT:ip">,</nts>
                  <nts id="Seg_256" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_258" n="HIAT:w" s="T76">hol</ts>
                  <nts id="Seg_259" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T78" id="Seg_261" n="HIAT:w" s="T77">ki͡ehetin</ts>
                  <nts id="Seg_262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_264" n="HIAT:w" s="T78">že</ts>
                  <nts id="Seg_265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_267" n="HIAT:w" s="T79">vɨstupajdaːppɨt</ts>
                  <nts id="Seg_268" n="HIAT:ip">.</nts>
                  <nts id="Seg_269" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T87" id="Seg_271" n="HIAT:u" s="T80">
                  <ts e="T81" id="Seg_273" n="HIAT:w" s="T80">Ol</ts>
                  <nts id="Seg_274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_276" n="HIAT:w" s="T81">kennitten</ts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_279" n="HIAT:w" s="T82">tögürük</ts>
                  <nts id="Seg_280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T84" id="Seg_282" n="HIAT:w" s="T83">ostol</ts>
                  <nts id="Seg_283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_285" n="HIAT:w" s="T84">bu͡olta</ts>
                  <nts id="Seg_286" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_288" n="HIAT:w" s="T85">ikkis</ts>
                  <nts id="Seg_289" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_291" n="HIAT:w" s="T86">künüger</ts>
                  <nts id="Seg_292" n="HIAT:ip">.</nts>
                  <nts id="Seg_293" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_295" n="HIAT:u" s="T87">
                  <ts e="T88" id="Seg_297" n="HIAT:w" s="T87">Ol</ts>
                  <nts id="Seg_298" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_300" n="HIAT:w" s="T88">tögürük</ts>
                  <nts id="Seg_301" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_303" n="HIAT:w" s="T89">ostolgo</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_306" n="HIAT:w" s="T90">bu͡olla</ts>
                  <nts id="Seg_307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_309" n="HIAT:w" s="T91">eŋin</ts>
                  <nts id="Seg_310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_312" n="HIAT:w" s="T92">eː</ts>
                  <nts id="Seg_313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_315" n="HIAT:w" s="T93">elbek</ts>
                  <nts id="Seg_316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_318" n="HIAT:w" s="T94">kihi</ts>
                  <nts id="Seg_319" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_321" n="HIAT:w" s="T95">komullubuta</ts>
                  <nts id="Seg_322" n="HIAT:ip">.</nts>
                  <nts id="Seg_323" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T104" id="Seg_325" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_327" n="HIAT:w" s="T96">Ü͡örüːnü</ts>
                  <nts id="Seg_328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_330" n="HIAT:w" s="T97">kuhaganɨ</ts>
                  <nts id="Seg_331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_333" n="HIAT:w" s="T98">eː</ts>
                  <nts id="Seg_334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_336" n="HIAT:w" s="T99">olok</ts>
                  <nts id="Seg_337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_339" n="HIAT:w" s="T100">tuhunan</ts>
                  <nts id="Seg_340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_342" n="HIAT:w" s="T101">kepsetiː</ts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_345" n="HIAT:w" s="T102">baːr</ts>
                  <nts id="Seg_346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_348" n="HIAT:w" s="T103">ete</ts>
                  <nts id="Seg_349" n="HIAT:ip">.</nts>
                  <nts id="Seg_350" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T127" id="Seg_352" n="HIAT:u" s="T104">
                  <ts e="T105" id="Seg_354" n="HIAT:w" s="T104">Ogolor</ts>
                  <nts id="Seg_355" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_357" n="HIAT:w" s="T105">ü͡örenellerin</ts>
                  <nts id="Seg_358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_360" n="HIAT:w" s="T106">tustarɨnan</ts>
                  <nts id="Seg_361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_363" n="HIAT:w" s="T107">studʼenttar</ts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_366" n="HIAT:w" s="T108">eː</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_369" n="HIAT:w" s="T109">iti</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_372" n="HIAT:w" s="T110">ulakan</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111.tx-UkOA.1" id="Seg_375" n="HIAT:w" s="T111">vɨššɨj</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111.tx-UkOA.2" id="Seg_378" n="HIAT:w" s="T111.tx-UkOA.1">učʼebnɨj</ts>
                  <nts id="Seg_379" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_381" n="HIAT:w" s="T111.tx-UkOA.2">zavʼedʼenʼijalar</ts>
                  <nts id="Seg_382" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_384" n="HIAT:w" s="T112">tustarɨgar</ts>
                  <nts id="Seg_385" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_387" n="HIAT:w" s="T113">togo</ts>
                  <nts id="Seg_388" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T115" id="Seg_390" n="HIAT:w" s="T114">bɨragallarɨn</ts>
                  <nts id="Seg_391" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_393" n="HIAT:w" s="T115">ili</ts>
                  <nts id="Seg_394" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T117" id="Seg_396" n="HIAT:w" s="T116">togo</ts>
                  <nts id="Seg_397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T118" id="Seg_399" n="HIAT:w" s="T117">ü͡öremmetterin</ts>
                  <nts id="Seg_400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_402" n="HIAT:w" s="T118">ol</ts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_405" n="HIAT:w" s="T119">ol</ts>
                  <nts id="Seg_406" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_408" n="HIAT:w" s="T120">tuhugar</ts>
                  <nts id="Seg_409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T122" id="Seg_411" n="HIAT:w" s="T121">eː</ts>
                  <nts id="Seg_412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_414" n="HIAT:w" s="T122">ulakan</ts>
                  <nts id="Seg_415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_417" n="HIAT:w" s="T123">onton</ts>
                  <nts id="Seg_418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_420" n="HIAT:w" s="T124">vapros</ts>
                  <nts id="Seg_421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_423" n="HIAT:w" s="T125">turar</ts>
                  <nts id="Seg_424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_426" n="HIAT:w" s="T126">ete</ts>
                  <nts id="Seg_427" n="HIAT:ip">.</nts>
                  <nts id="Seg_428" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T138" id="Seg_430" n="HIAT:u" s="T127">
                  <ts e="T128" id="Seg_432" n="HIAT:w" s="T127">Onton</ts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_435" n="HIAT:w" s="T128">bu͡olla</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_437" n="HIAT:ip">(</nts>
                  <ts e="T130" id="Seg_439" n="HIAT:w" s="T129">eme-</ts>
                  <nts id="Seg_440" n="HIAT:ip">)</nts>
                  <nts id="Seg_441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T131" id="Seg_443" n="HIAT:w" s="T130">eː</ts>
                  <nts id="Seg_444" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_446" n="HIAT:w" s="T131">emeːksin</ts>
                  <nts id="Seg_447" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_449" n="HIAT:w" s="T132">vɨstupajdaːbɨt</ts>
                  <nts id="Seg_450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_452" n="HIAT:w" s="T133">onno</ts>
                  <nts id="Seg_453" n="HIAT:ip">,</nts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_456" n="HIAT:w" s="T134">Maldaːnova</ts>
                  <nts id="Seg_457" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_459" n="HIAT:w" s="T135">di͡en</ts>
                  <nts id="Seg_460" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T137" id="Seg_462" n="HIAT:w" s="T136">Marʼija</ts>
                  <nts id="Seg_463" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_465" n="HIAT:w" s="T137">Kuzʼmʼinʼičʼna</ts>
                  <nts id="Seg_466" n="HIAT:ip">.</nts>
                  <nts id="Seg_467" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T142" id="Seg_469" n="HIAT:u" s="T138">
                  <ts e="T139" id="Seg_471" n="HIAT:w" s="T138">Öjdöːk</ts>
                  <nts id="Seg_472" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_474" n="HIAT:w" s="T139">dʼaktar</ts>
                  <nts id="Seg_475" n="HIAT:ip">,</nts>
                  <nts id="Seg_476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_478" n="HIAT:w" s="T140">ol</ts>
                  <nts id="Seg_479" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_481" n="HIAT:w" s="T141">haŋarbɨta</ts>
                  <nts id="Seg_482" n="HIAT:ip">:</nts>
                  <nts id="Seg_483" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T156" id="Seg_485" n="HIAT:u" s="T142">
                  <ts e="T143" id="Seg_487" n="HIAT:w" s="T142">Eː</ts>
                  <nts id="Seg_488" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_490" n="HIAT:w" s="T143">ogo</ts>
                  <nts id="Seg_491" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_493" n="HIAT:w" s="T144">kihiler</ts>
                  <nts id="Seg_494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_496" n="HIAT:w" s="T145">eː</ts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_499" n="HIAT:w" s="T146">ogo</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_502" n="HIAT:w" s="T147">kihiler</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_505" n="HIAT:w" s="T148">bejelerin</ts>
                  <nts id="Seg_506" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_508" n="HIAT:w" s="T149">kergenneriger</ts>
                  <nts id="Seg_509" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_511" n="HIAT:w" s="T150">bejelerin</ts>
                  <nts id="Seg_512" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_514" n="HIAT:w" s="T151">kergennerin</ts>
                  <nts id="Seg_515" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T153" id="Seg_517" n="HIAT:w" s="T152">isteriger</ts>
                  <nts id="Seg_518" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T154" id="Seg_520" n="HIAT:w" s="T153">isteritten</ts>
                  <nts id="Seg_521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_523" n="HIAT:w" s="T154">vaspʼitanʼija</ts>
                  <nts id="Seg_524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_526" n="HIAT:w" s="T155">bu͡olardaːk</ts>
                  <nts id="Seg_527" n="HIAT:ip">.</nts>
                  <nts id="Seg_528" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T163" id="Seg_530" n="HIAT:u" s="T156">
                  <ts e="T157" id="Seg_532" n="HIAT:w" s="T156">Töröppüt</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_535" n="HIAT:w" s="T157">kihi</ts>
                  <nts id="Seg_536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_538" n="HIAT:w" s="T158">bi͡ererdeːk</ts>
                  <nts id="Seg_539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_541" n="HIAT:w" s="T159">ü͡öregi</ts>
                  <nts id="Seg_542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_544" n="HIAT:w" s="T160">ütü͡önü</ts>
                  <nts id="Seg_545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_547" n="HIAT:w" s="T161">kuhaganɨ</ts>
                  <nts id="Seg_548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_550" n="HIAT:w" s="T162">ogogo</ts>
                  <nts id="Seg_551" n="HIAT:ip">.</nts>
                  <nts id="Seg_552" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T181" id="Seg_554" n="HIAT:u" s="T163">
                  <ts e="T164" id="Seg_556" n="HIAT:w" s="T163">Aː</ts>
                  <nts id="Seg_557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_559" n="HIAT:w" s="T164">usku͡ola</ts>
                  <nts id="Seg_560" n="HIAT:ip">,</nts>
                  <nts id="Seg_561" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_563" n="HIAT:w" s="T165">sadʼik</ts>
                  <nts id="Seg_564" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_566" n="HIAT:w" s="T166">onuga</ts>
                  <nts id="Seg_567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_569" n="HIAT:w" s="T167">bu͡olla</ts>
                  <nts id="Seg_570" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_572" n="HIAT:w" s="T168">eː</ts>
                  <nts id="Seg_573" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169.tx-UkOA.1" id="Seg_575" n="HIAT:w" s="T169">učʼebnɨj</ts>
                  <nts id="Seg_576" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T170" id="Seg_578" n="HIAT:w" s="T169.tx-UkOA.1">zavʼedʼenʼijalar</ts>
                  <nts id="Seg_579" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_581" n="HIAT:w" s="T170">onuga</ts>
                  <nts id="Seg_582" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_584" n="HIAT:w" s="T171">kömö</ts>
                  <nts id="Seg_585" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_587" n="HIAT:w" s="T172">ere</ts>
                  <nts id="Seg_588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_590" n="HIAT:w" s="T173">bu͡olu͡oktarɨn</ts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_593" n="HIAT:w" s="T174">naːda</ts>
                  <nts id="Seg_594" n="HIAT:ip">,</nts>
                  <nts id="Seg_595" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_597" n="HIAT:w" s="T175">a</ts>
                  <nts id="Seg_598" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_600" n="HIAT:w" s="T176">anʼi</ts>
                  <nts id="Seg_601" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_603" n="HIAT:w" s="T177">asnavnoj</ts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T179" id="Seg_606" n="HIAT:w" s="T178">bazɨ</ts>
                  <nts id="Seg_607" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_609" n="HIAT:w" s="T179">kördük</ts>
                  <nts id="Seg_610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_612" n="HIAT:w" s="T180">bu͡olu͡oktarɨn</ts>
                  <nts id="Seg_613" n="HIAT:ip">.</nts>
                  <nts id="Seg_614" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T198" id="Seg_616" n="HIAT:u" s="T181">
                  <ts e="T182" id="Seg_618" n="HIAT:w" s="T181">Ol</ts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_621" n="HIAT:w" s="T182">ihin</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_624" n="HIAT:w" s="T183">bu͡olla</ts>
                  <nts id="Seg_625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_627" n="HIAT:w" s="T184">anɨ</ts>
                  <nts id="Seg_628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_630" n="HIAT:w" s="T185">inni</ts>
                  <nts id="Seg_631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T187" id="Seg_633" n="HIAT:w" s="T186">di͡ek</ts>
                  <nts id="Seg_634" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_636" n="HIAT:w" s="T187">eː</ts>
                  <nts id="Seg_637" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T189" id="Seg_639" n="HIAT:w" s="T188">ulaːtɨnnarɨ͡akka</ts>
                  <nts id="Seg_640" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_642" n="HIAT:w" s="T189">naːda</ts>
                  <nts id="Seg_643" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_645" n="HIAT:w" s="T190">diːr</ts>
                  <nts id="Seg_646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T192" id="Seg_648" n="HIAT:w" s="T191">eː</ts>
                  <nts id="Seg_649" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_651" n="HIAT:w" s="T192">ol</ts>
                  <nts id="Seg_652" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_654" n="HIAT:w" s="T193">dʼaktar</ts>
                  <nts id="Seg_655" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T195" id="Seg_657" n="HIAT:w" s="T194">eppi͡eti</ts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T196" id="Seg_660" n="HIAT:w" s="T195">törötör</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T197" id="Seg_663" n="HIAT:w" s="T196">kihilerten</ts>
                  <nts id="Seg_664" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T198" id="Seg_666" n="HIAT:w" s="T197">radʼitelʼlartan</ts>
                  <nts id="Seg_667" n="HIAT:ip">.</nts>
                  <nts id="Seg_668" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T213" id="Seg_670" n="HIAT:u" s="T198">
                  <ts e="T199" id="Seg_672" n="HIAT:w" s="T198">Da</ts>
                  <nts id="Seg_673" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T200" id="Seg_675" n="HIAT:w" s="T199">i</ts>
                  <nts id="Seg_676" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T201" id="Seg_678" n="HIAT:w" s="T200">ütü͡önü</ts>
                  <nts id="Seg_679" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T202" id="Seg_681" n="HIAT:w" s="T201">elbeːtinneri͡ekke</ts>
                  <nts id="Seg_682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T203" id="Seg_684" n="HIAT:w" s="T202">di͡en</ts>
                  <nts id="Seg_685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T204" id="Seg_687" n="HIAT:w" s="T203">haŋarbɨta</ts>
                  <nts id="Seg_688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T205" id="Seg_690" n="HIAT:w" s="T204">ol</ts>
                  <nts id="Seg_691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T206" id="Seg_693" n="HIAT:w" s="T205">dʼaktar</ts>
                  <nts id="Seg_694" n="HIAT:ip">,</nts>
                  <nts id="Seg_695" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T207" id="Seg_697" n="HIAT:w" s="T206">ütü͡önü</ts>
                  <nts id="Seg_698" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T208" id="Seg_700" n="HIAT:w" s="T207">elbeːtinneri͡ekke</ts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T209" id="Seg_703" n="HIAT:w" s="T208">a</ts>
                  <nts id="Seg_704" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T210" id="Seg_706" n="HIAT:w" s="T209">kuhaganɨ</ts>
                  <nts id="Seg_707" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_709" n="HIAT:w" s="T210">agɨjaːtɨnnarɨ͡akka</ts>
                  <nts id="Seg_710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_712" n="HIAT:w" s="T211">bu</ts>
                  <nts id="Seg_713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_715" n="HIAT:w" s="T212">hirge</ts>
                  <nts id="Seg_716" n="HIAT:ip">.</nts>
                  <nts id="Seg_717" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T234" id="Seg_719" n="HIAT:u" s="T213">
                  <ts e="T214" id="Seg_721" n="HIAT:w" s="T213">Vot</ts>
                  <nts id="Seg_722" n="HIAT:ip">,</nts>
                  <nts id="Seg_723" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_725" n="HIAT:w" s="T214">nu</ts>
                  <nts id="Seg_726" n="HIAT:ip">,</nts>
                  <nts id="Seg_727" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_729" n="HIAT:w" s="T215">koloːn</ts>
                  <nts id="Seg_730" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_732" n="HIAT:w" s="T216">kördökkö</ts>
                  <nts id="Seg_733" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_734" n="HIAT:ip">(</nts>
                  <ts e="T218" id="Seg_736" n="HIAT:w" s="T217">jeslʼi</ts>
                  <nts id="Seg_737" n="HIAT:ip">)</nts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_740" n="HIAT:w" s="T218">enigin</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_743" n="HIAT:w" s="T219">kuhagannɨk</ts>
                  <nts id="Seg_744" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T221" id="Seg_746" n="HIAT:w" s="T220">haŋardɨlar</ts>
                  <nts id="Seg_747" n="HIAT:ip">,</nts>
                  <nts id="Seg_748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_750" n="HIAT:w" s="T221">kuhaganɨ</ts>
                  <nts id="Seg_751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_753" n="HIAT:w" s="T222">gɨnnɨlar</ts>
                  <nts id="Seg_754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_756" n="HIAT:w" s="T223">du͡o</ts>
                  <nts id="Seg_757" n="HIAT:ip">,</nts>
                  <nts id="Seg_758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_760" n="HIAT:w" s="T224">en</ts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_763" n="HIAT:w" s="T225">bu͡o</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_766" n="HIAT:w" s="T226">kepseːmi͡ekkin</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_769" n="HIAT:w" s="T227">naːda</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_772" n="HIAT:w" s="T228">ol</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_775" n="HIAT:w" s="T229">kihi</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_778" n="HIAT:w" s="T230">tuhunan</ts>
                  <nts id="Seg_779" n="HIAT:ip">,</nts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_782" n="HIAT:w" s="T231">diːr</ts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T233" id="Seg_785" n="HIAT:w" s="T232">ol</ts>
                  <nts id="Seg_786" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_788" n="HIAT:w" s="T233">dʼaktar</ts>
                  <nts id="Seg_789" n="HIAT:ip">.</nts>
                  <nts id="Seg_790" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T251" id="Seg_792" n="HIAT:u" s="T234">
                  <ts e="T235" id="Seg_794" n="HIAT:w" s="T234">Ol</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_797" n="HIAT:w" s="T235">kihi</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_799" n="HIAT:ip">(</nts>
                  <ts e="T237" id="Seg_801" n="HIAT:w" s="T236">tuhu-</ts>
                  <nts id="Seg_802" n="HIAT:ip">)</nts>
                  <nts id="Seg_803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_805" n="HIAT:w" s="T237">ol</ts>
                  <nts id="Seg_806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_808" n="HIAT:w" s="T238">kihi</ts>
                  <nts id="Seg_809" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T240" id="Seg_811" n="HIAT:w" s="T239">tuhunan</ts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_814" n="HIAT:w" s="T240">bejetiger</ts>
                  <nts id="Seg_815" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_817" n="HIAT:w" s="T241">haŋarɨmɨ͡akkɨn</ts>
                  <nts id="Seg_818" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_820" n="HIAT:w" s="T242">naːda</ts>
                  <nts id="Seg_821" n="HIAT:ip">,</nts>
                  <nts id="Seg_822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_824" n="HIAT:w" s="T243">no</ts>
                  <nts id="Seg_825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_827" n="HIAT:w" s="T244">kihilerge</ts>
                  <nts id="Seg_828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_830" n="HIAT:w" s="T245">emi͡e</ts>
                  <nts id="Seg_831" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T247" id="Seg_833" n="HIAT:w" s="T246">kepseːmi͡ekke</ts>
                  <nts id="Seg_834" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_836" n="HIAT:w" s="T247">naːda</ts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_839" n="HIAT:w" s="T248">štob</ts>
                  <nts id="Seg_840" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_842" n="HIAT:w" s="T249">kuhagan</ts>
                  <nts id="Seg_843" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T251" id="Seg_845" n="HIAT:w" s="T250">ulaːtɨmi͡egin</ts>
                  <nts id="Seg_846" n="HIAT:ip">.</nts>
                  <nts id="Seg_847" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T264" id="Seg_849" n="HIAT:u" s="T251">
                  <ts e="T252" id="Seg_851" n="HIAT:w" s="T251">A</ts>
                  <nts id="Seg_852" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_854" n="HIAT:w" s="T252">üčügejdik</ts>
                  <nts id="Seg_855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_857" n="HIAT:w" s="T253">gɨmmɨt</ts>
                  <nts id="Seg_858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_860" n="HIAT:w" s="T254">kihi͡eke</ts>
                  <nts id="Seg_861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_863" n="HIAT:w" s="T255">bu͡olla</ts>
                  <nts id="Seg_864" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_866" n="HIAT:w" s="T256">ol</ts>
                  <nts id="Seg_867" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_869" n="HIAT:w" s="T257">kihi͡eke</ts>
                  <nts id="Seg_870" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_872" n="HIAT:w" s="T258">možešʼ</ts>
                  <nts id="Seg_873" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_875" n="HIAT:w" s="T259">emi͡e</ts>
                  <nts id="Seg_876" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_878" n="HIAT:w" s="T260">bejetiger</ts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_881" n="HIAT:w" s="T261">haŋarɨmɨ͡akkɨn</ts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_884" n="HIAT:w" s="T262">štob</ts>
                  <nts id="Seg_885" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T264" id="Seg_887" n="HIAT:w" s="T263">kihirgeːmi͡egin</ts>
                  <nts id="Seg_888" n="HIAT:ip">.</nts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T283" id="Seg_891" n="HIAT:u" s="T264">
                  <ts e="T265" id="Seg_893" n="HIAT:w" s="T264">A</ts>
                  <nts id="Seg_894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_896" n="HIAT:w" s="T265">atɨn</ts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_899" n="HIAT:w" s="T266">kihilerge</ts>
                  <nts id="Seg_900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_902" n="HIAT:w" s="T267">kepsi͡ekkin</ts>
                  <nts id="Seg_903" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_905" n="HIAT:w" s="T268">naːda</ts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T270" id="Seg_908" n="HIAT:w" s="T269">što</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_911" n="HIAT:w" s="T270">ol</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_914" n="HIAT:w" s="T271">kihi</ts>
                  <nts id="Seg_915" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_917" n="HIAT:w" s="T272">üčügeji</ts>
                  <nts id="Seg_918" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T274" id="Seg_920" n="HIAT:w" s="T273">gɨmmɨta</ts>
                  <nts id="Seg_921" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_923" n="HIAT:w" s="T274">di͡en</ts>
                  <nts id="Seg_924" n="HIAT:ip">,</nts>
                  <nts id="Seg_925" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_927" n="HIAT:w" s="T275">ol</ts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_930" n="HIAT:w" s="T276">kihi</ts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_933" n="HIAT:w" s="T277">onnuk</ts>
                  <nts id="Seg_934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_936" n="HIAT:w" s="T278">di͡en</ts>
                  <nts id="Seg_937" n="HIAT:ip">,</nts>
                  <nts id="Seg_938" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_940" n="HIAT:w" s="T279">ol</ts>
                  <nts id="Seg_941" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_943" n="HIAT:w" s="T280">kihi</ts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T282" id="Seg_946" n="HIAT:w" s="T281">itinnik</ts>
                  <nts id="Seg_947" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T283" id="Seg_949" n="HIAT:w" s="T282">di͡en</ts>
                  <nts id="Seg_950" n="HIAT:ip">.</nts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T570" id="Seg_952" n="sc" s="T284">
               <ts e="T288" id="Seg_954" n="HIAT:u" s="T284">
                  <ts e="T285" id="Seg_956" n="HIAT:w" s="T284">Oččogo</ts>
                  <nts id="Seg_957" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_959" n="HIAT:w" s="T285">bu͡olla</ts>
                  <nts id="Seg_960" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_962" n="HIAT:w" s="T286">ütü͡ö</ts>
                  <nts id="Seg_963" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_965" n="HIAT:w" s="T287">ulaːtar</ts>
                  <nts id="Seg_966" n="HIAT:ip">.</nts>
                  <nts id="Seg_967" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T301" id="Seg_969" n="HIAT:u" s="T288">
                  <ts e="T289" id="Seg_971" n="HIAT:w" s="T288">Ütü͡ö</ts>
                  <nts id="Seg_972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_974" n="HIAT:w" s="T289">ulaːtar</ts>
                  <nts id="Seg_975" n="HIAT:ip">,</nts>
                  <nts id="Seg_976" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_978" n="HIAT:w" s="T290">onnuk</ts>
                  <nts id="Seg_979" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_981" n="HIAT:w" s="T291">formulannan</ts>
                  <nts id="Seg_982" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T293" id="Seg_984" n="HIAT:w" s="T292">di͡en</ts>
                  <nts id="Seg_985" n="HIAT:ip">,</nts>
                  <nts id="Seg_986" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T294" id="Seg_988" n="HIAT:w" s="T293">no</ts>
                  <nts id="Seg_989" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_991" n="HIAT:w" s="T294">emi͡e</ts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_994" n="HIAT:w" s="T295">tak</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_997" n="HIAT:w" s="T296">i</ts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1000" n="HIAT:w" s="T297">ol</ts>
                  <nts id="Seg_1001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1003" n="HIAT:w" s="T298">formulaga</ts>
                  <nts id="Seg_1004" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1006" n="HIAT:w" s="T299">min</ts>
                  <nts id="Seg_1007" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T301" id="Seg_1009" n="HIAT:w" s="T300">haglastammappɨn</ts>
                  <nts id="Seg_1010" n="HIAT:ip">.</nts>
                  <nts id="Seg_1011" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T303" id="Seg_1013" n="HIAT:u" s="T301">
                  <ts e="T302" id="Seg_1015" n="HIAT:w" s="T301">Togo</ts>
                  <nts id="Seg_1016" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1018" n="HIAT:w" s="T302">di͡eŋŋit</ts>
                  <nts id="Seg_1019" n="HIAT:ip">?</nts>
                  <nts id="Seg_1020" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T312" id="Seg_1022" n="HIAT:u" s="T303">
                  <ts e="T304" id="Seg_1024" n="HIAT:w" s="T303">Kuhagan</ts>
                  <nts id="Seg_1025" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1027" n="HIAT:w" s="T304">ikkis</ts>
                  <nts id="Seg_1028" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1030" n="HIAT:w" s="T305">formula</ts>
                  <nts id="Seg_1031" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T307" id="Seg_1033" n="HIAT:w" s="T306">baːr</ts>
                  <nts id="Seg_1034" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1036" n="HIAT:w" s="T307">ol</ts>
                  <nts id="Seg_1037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1039" n="HIAT:w" s="T308">ürdütüger</ts>
                  <nts id="Seg_1040" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1042" n="HIAT:w" s="T309">oččogo</ts>
                  <nts id="Seg_1043" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1045" n="HIAT:w" s="T310">kuhagan</ts>
                  <nts id="Seg_1046" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1048" n="HIAT:w" s="T311">ulaːtɨmɨ͡agɨn</ts>
                  <nts id="Seg_1049" n="HIAT:ip">.</nts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T335" id="Seg_1052" n="HIAT:u" s="T312">
                  <ts e="T313" id="Seg_1054" n="HIAT:w" s="T312">Kuhaganɨ</ts>
                  <nts id="Seg_1055" n="HIAT:ip">,</nts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T314" id="Seg_1058" n="HIAT:w" s="T313">inni</ts>
                  <nts id="Seg_1059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1061" n="HIAT:w" s="T314">di͡ek</ts>
                  <nts id="Seg_1062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1064" n="HIAT:w" s="T315">töttörü</ts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1067" n="HIAT:w" s="T316">ulakanɨ</ts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1070" n="HIAT:w" s="T317">eppi͡et</ts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1073" n="HIAT:w" s="T318">bi͡eri͡ekke</ts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T320" id="Seg_1076" n="HIAT:w" s="T319">naːda</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1079" n="HIAT:w" s="T320">gini͡eke</ts>
                  <nts id="Seg_1080" n="HIAT:ip">,</nts>
                  <nts id="Seg_1081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T322" id="Seg_1083" n="HIAT:w" s="T321">štob</ts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1086" n="HIAT:w" s="T322">kuhaganɨ</ts>
                  <nts id="Seg_1087" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1089" n="HIAT:w" s="T323">gɨmmɨt</ts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1092" n="HIAT:w" s="T324">kihi</ts>
                  <nts id="Seg_1093" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1095" n="HIAT:w" s="T325">bili͡egin</ts>
                  <nts id="Seg_1096" n="HIAT:ip">,</nts>
                  <nts id="Seg_1097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1099" n="HIAT:w" s="T326">što</ts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T328" id="Seg_1102" n="HIAT:w" s="T327">meːne</ts>
                  <nts id="Seg_1103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1105" n="HIAT:w" s="T328">kihini</ts>
                  <nts id="Seg_1106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1108" n="HIAT:w" s="T329">gini͡ene</ts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1111" n="HIAT:w" s="T330">kuhagana</ts>
                  <nts id="Seg_1112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1114" n="HIAT:w" s="T331">meːne</ts>
                  <nts id="Seg_1115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1117" n="HIAT:w" s="T332">ahɨ͡a</ts>
                  <nts id="Seg_1118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1120" n="HIAT:w" s="T333">hu͡oga</ts>
                  <nts id="Seg_1121" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1123" n="HIAT:w" s="T334">gini͡eke</ts>
                  <nts id="Seg_1124" n="HIAT:ip">.</nts>
                  <nts id="Seg_1125" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T352" id="Seg_1127" n="HIAT:u" s="T335">
                  <ts e="T336" id="Seg_1129" n="HIAT:w" s="T335">Gini</ts>
                  <nts id="Seg_1130" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T337" id="Seg_1132" n="HIAT:w" s="T336">bu͡olla</ts>
                  <nts id="Seg_1133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1135" n="HIAT:w" s="T337">kihi</ts>
                  <nts id="Seg_1136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1138" n="HIAT:w" s="T338">innitiger</ts>
                  <nts id="Seg_1139" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1141" n="HIAT:w" s="T339">eppi͡ettiːrdeːk</ts>
                  <nts id="Seg_1142" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1144" n="HIAT:w" s="T340">štob</ts>
                  <nts id="Seg_1145" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1147" n="HIAT:w" s="T341">inni</ts>
                  <nts id="Seg_1148" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1150" n="HIAT:w" s="T342">di͡ek</ts>
                  <nts id="Seg_1151" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1153" n="HIAT:w" s="T343">kuhaganɨ</ts>
                  <nts id="Seg_1154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1156" n="HIAT:w" s="T344">gɨnɨmɨ͡agɨn</ts>
                  <nts id="Seg_1157" n="HIAT:ip">,</nts>
                  <nts id="Seg_1158" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T346" id="Seg_1160" n="HIAT:w" s="T345">oččogo</ts>
                  <nts id="Seg_1161" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1163" n="HIAT:w" s="T346">bu͡olla</ts>
                  <nts id="Seg_1164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1166" n="HIAT:w" s="T347">ütü͡ö</ts>
                  <nts id="Seg_1167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1169" n="HIAT:w" s="T348">ikkite</ts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1172" n="HIAT:w" s="T349">raz</ts>
                  <nts id="Seg_1173" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1175" n="HIAT:w" s="T350">ulaːtan</ts>
                  <nts id="Seg_1176" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1178" n="HIAT:w" s="T351">taksɨ͡aga</ts>
                  <nts id="Seg_1179" n="HIAT:ip">.</nts>
                  <nts id="Seg_1180" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T369" id="Seg_1182" n="HIAT:u" s="T352">
                  <ts e="T353" id="Seg_1184" n="HIAT:w" s="T352">I</ts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1187" n="HIAT:w" s="T353">oloroːčču</ts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T355" id="Seg_1190" n="HIAT:w" s="T354">kihiler</ts>
                  <nts id="Seg_1191" n="HIAT:ip">,</nts>
                  <nts id="Seg_1192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T356" id="Seg_1194" n="HIAT:w" s="T355">onno</ts>
                  <nts id="Seg_1195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T357" id="Seg_1197" n="HIAT:w" s="T356">oloroːčču</ts>
                  <nts id="Seg_1198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1200" n="HIAT:w" s="T357">kihiler</ts>
                  <nts id="Seg_1201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1203" n="HIAT:w" s="T358">bejelerin</ts>
                  <nts id="Seg_1204" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1206" n="HIAT:w" s="T359">inni</ts>
                  <nts id="Seg_1207" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1209" n="HIAT:w" s="T360">di͡ek</ts>
                  <nts id="Seg_1210" n="HIAT:ip">,</nts>
                  <nts id="Seg_1211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1213" n="HIAT:w" s="T361">bejelerin</ts>
                  <nts id="Seg_1214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T363" id="Seg_1216" n="HIAT:w" s="T362">inni</ts>
                  <nts id="Seg_1217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1219" n="HIAT:w" s="T363">di͡ek</ts>
                  <nts id="Seg_1220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1222" n="HIAT:w" s="T364">atagastatɨ͡aktara</ts>
                  <nts id="Seg_1223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T366" id="Seg_1225" n="HIAT:w" s="T365">hu͡oktara</ts>
                  <nts id="Seg_1226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1228" n="HIAT:w" s="T366">da</ts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1231" n="HIAT:w" s="T367">atagastatɨ͡aktara</ts>
                  <nts id="Seg_1232" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1234" n="HIAT:w" s="T368">hu͡oktara</ts>
                  <nts id="Seg_1235" n="HIAT:ip">.</nts>
                  <nts id="Seg_1236" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T392" id="Seg_1238" n="HIAT:u" s="T369">
                  <ts e="T370" id="Seg_1240" n="HIAT:w" s="T369">Aː</ts>
                  <nts id="Seg_1241" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T371" id="Seg_1243" n="HIAT:w" s="T370">no</ts>
                  <nts id="Seg_1244" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1246" n="HIAT:w" s="T371">onton</ts>
                  <nts id="Seg_1247" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1249" n="HIAT:w" s="T372">ol</ts>
                  <nts id="Seg_1250" n="HIAT:ip">,</nts>
                  <nts id="Seg_1251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1253" n="HIAT:w" s="T373">ol</ts>
                  <nts id="Seg_1254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1256" n="HIAT:w" s="T374">kruglɨj</ts>
                  <nts id="Seg_1257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1259" n="HIAT:w" s="T375">tögürük</ts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T377" id="Seg_1262" n="HIAT:w" s="T376">ostol</ts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1265" n="HIAT:w" s="T377">büppütün</ts>
                  <nts id="Seg_1266" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1268" n="HIAT:w" s="T378">kenne</ts>
                  <nts id="Seg_1269" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1271" n="HIAT:w" s="T379">emi͡e</ts>
                  <nts id="Seg_1272" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1274" n="HIAT:w" s="T380">kancert</ts>
                  <nts id="Seg_1275" n="HIAT:ip">,</nts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1277" n="HIAT:ip">"</nts>
                  <ts e="T381.tx-UkOA.1" id="Seg_1279" n="HIAT:w" s="T381">gala</ts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1282" n="HIAT:w" s="T381.tx-UkOA.1">-</ts>
                  <nts id="Seg_1283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1285" n="HIAT:w" s="T382">kancert</ts>
                  <nts id="Seg_1286" n="HIAT:ip">"</nts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1289" n="HIAT:w" s="T383">di͡en</ts>
                  <nts id="Seg_1290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1292" n="HIAT:w" s="T384">bu͡olta</ts>
                  <nts id="Seg_1293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1295" n="HIAT:w" s="T385">onno</ts>
                  <nts id="Seg_1296" n="HIAT:ip">,</nts>
                  <nts id="Seg_1297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1299" n="HIAT:w" s="T386">bert</ts>
                  <nts id="Seg_1300" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1302" n="HIAT:w" s="T387">üčügejdik</ts>
                  <nts id="Seg_1303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1305" n="HIAT:w" s="T388">vɨstupajdaːbɨttara</ts>
                  <nts id="Seg_1306" n="HIAT:ip">,</nts>
                  <nts id="Seg_1307" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1309" n="HIAT:w" s="T389">elbek</ts>
                  <nts id="Seg_1310" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T391" id="Seg_1312" n="HIAT:w" s="T390">kallʼektiv</ts>
                  <nts id="Seg_1313" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1315" n="HIAT:w" s="T391">kelbite</ts>
                  <nts id="Seg_1316" n="HIAT:ip">.</nts>
                  <nts id="Seg_1317" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T403" id="Seg_1319" n="HIAT:u" s="T392">
                  <ts e="T393" id="Seg_1321" n="HIAT:w" s="T392">Eː</ts>
                  <nts id="Seg_1322" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1324" n="HIAT:w" s="T393">Xantɨ-Mansijskajtan</ts>
                  <nts id="Seg_1325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1327" n="HIAT:w" s="T394">bejetitten</ts>
                  <nts id="Seg_1328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1330" n="HIAT:w" s="T395">tʼeatralʼnaj</ts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1333" n="HIAT:w" s="T396">gruppa</ts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1336" n="HIAT:w" s="T397">baːr</ts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1339" n="HIAT:w" s="T398">ete</ts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1342" n="HIAT:w" s="T399">bihi͡ene</ts>
                  <nts id="Seg_1343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1345" n="HIAT:w" s="T400">ansamblʼ</ts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1348" n="HIAT:w" s="T401">Xejrobut</ts>
                  <nts id="Seg_1349" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1351" n="HIAT:w" s="T402">kördük</ts>
                  <nts id="Seg_1352" n="HIAT:ip">.</nts>
                  <nts id="Seg_1353" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T416" id="Seg_1355" n="HIAT:u" s="T403">
                  <ts e="T404" id="Seg_1357" n="HIAT:w" s="T403">Onton</ts>
                  <nts id="Seg_1358" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1360" n="HIAT:w" s="T404">bu͡olla</ts>
                  <nts id="Seg_1361" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1363" n="HIAT:w" s="T405">Magadantan</ts>
                  <nts id="Seg_1364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1366" n="HIAT:w" s="T406">kelbittere</ts>
                  <nts id="Seg_1367" n="HIAT:ip">,</nts>
                  <nts id="Seg_1368" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1370" n="HIAT:w" s="T407">eː</ts>
                  <nts id="Seg_1371" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408.tx-UkOA.1" id="Seg_1373" n="HIAT:w" s="T408">Jamala</ts>
                  <nts id="Seg_1374" n="HIAT:ip">_</nts>
                  <ts e="T408.tx-UkOA.2" id="Seg_1376" n="HIAT:w" s="T408.tx-UkOA.1">Nʼenʼeckij</ts>
                  <nts id="Seg_1377" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408.tx-UkOA.3" id="Seg_1379" n="HIAT:w" s="T408.tx-UkOA.2">avtonomnɨj</ts>
                  <nts id="Seg_1380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T409" id="Seg_1382" n="HIAT:w" s="T408.tx-UkOA.3">oːkrugtan</ts>
                  <nts id="Seg_1383" n="HIAT:ip">,</nts>
                  <nts id="Seg_1384" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1386" n="HIAT:w" s="T409">Čʼukotkattan</ts>
                  <nts id="Seg_1387" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1389" n="HIAT:w" s="T410">ansamblʼ</ts>
                  <nts id="Seg_1390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1392" n="HIAT:w" s="T411">baːr</ts>
                  <nts id="Seg_1393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T413" id="Seg_1395" n="HIAT:w" s="T412">ete</ts>
                  <nts id="Seg_1396" n="HIAT:ip">,</nts>
                  <nts id="Seg_1397" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1399" n="HIAT:w" s="T413">ɨraːk</ts>
                  <nts id="Seg_1400" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1402" n="HIAT:w" s="T414">hirten</ts>
                  <nts id="Seg_1403" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1405" n="HIAT:w" s="T415">keliteleːbitter</ts>
                  <nts id="Seg_1406" n="HIAT:ip">.</nts>
                  <nts id="Seg_1407" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T421" id="Seg_1409" n="HIAT:u" s="T416">
                  <ts e="T417" id="Seg_1411" n="HIAT:w" s="T416">Onton</ts>
                  <nts id="Seg_1412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1414" n="HIAT:w" s="T417">elbek-elbek</ts>
                  <nts id="Seg_1415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1417" n="HIAT:w" s="T418">hirten</ts>
                  <nts id="Seg_1418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1420" n="HIAT:w" s="T419">keliteleːbitter</ts>
                  <nts id="Seg_1421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T421" id="Seg_1423" n="HIAT:w" s="T420">onno</ts>
                  <nts id="Seg_1424" n="HIAT:ip">.</nts>
                  <nts id="Seg_1425" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T427" id="Seg_1427" n="HIAT:u" s="T421">
                  <ts e="T422" id="Seg_1429" n="HIAT:w" s="T421">Hürbe</ts>
                  <nts id="Seg_1430" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1432" n="HIAT:w" s="T422">ikki</ts>
                  <nts id="Seg_1433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1435" n="HIAT:w" s="T423">kallʼektʼiv</ts>
                  <nts id="Seg_1436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T425" id="Seg_1438" n="HIAT:w" s="T424">baːr</ts>
                  <nts id="Seg_1439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1441" n="HIAT:w" s="T425">ete</ts>
                  <nts id="Seg_1442" n="HIAT:ip">,</nts>
                  <nts id="Seg_1443" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1445" n="HIAT:w" s="T426">eː</ts>
                  <nts id="Seg_1446" n="HIAT:ip">.</nts>
                  <nts id="Seg_1447" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T457" id="Seg_1449" n="HIAT:u" s="T427">
                  <ts e="T428" id="Seg_1451" n="HIAT:w" s="T427">Onton</ts>
                  <nts id="Seg_1452" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1454" n="HIAT:w" s="T428">kuratardarbɨt</ts>
                  <nts id="Seg_1455" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1457" n="HIAT:w" s="T429">üčügej</ts>
                  <nts id="Seg_1458" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1460" n="HIAT:w" s="T430">bagajɨ</ts>
                  <nts id="Seg_1461" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T432" id="Seg_1463" n="HIAT:w" s="T431">dʼaktattar</ts>
                  <nts id="Seg_1464" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1466" n="HIAT:w" s="T432">etilere</ts>
                  <nts id="Seg_1467" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1469" n="HIAT:w" s="T433">bihigitin</ts>
                  <nts id="Seg_1470" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1472" n="HIAT:w" s="T434">barɨtɨn</ts>
                  <nts id="Seg_1473" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1475" n="HIAT:w" s="T435">eː</ts>
                  <nts id="Seg_1476" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1478" n="HIAT:w" s="T436">illenner</ts>
                  <nts id="Seg_1479" n="HIAT:ip">,</nts>
                  <nts id="Seg_1480" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1482" n="HIAT:w" s="T437">egelenner</ts>
                  <nts id="Seg_1483" n="HIAT:ip">,</nts>
                  <nts id="Seg_1484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T439" id="Seg_1486" n="HIAT:w" s="T438">küččügüj</ts>
                  <nts id="Seg_1487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1489" n="HIAT:w" s="T439">ogoluː</ts>
                  <nts id="Seg_1490" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1492" n="HIAT:w" s="T440">ututannar</ts>
                  <nts id="Seg_1493" n="HIAT:ip">,</nts>
                  <nts id="Seg_1494" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1496" n="HIAT:w" s="T441">onton</ts>
                  <nts id="Seg_1497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1499" n="HIAT:w" s="T442">ahatannar</ts>
                  <nts id="Seg_1500" n="HIAT:ip">,</nts>
                  <nts id="Seg_1501" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1503" n="HIAT:w" s="T443">onton</ts>
                  <nts id="Seg_1504" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1506" n="HIAT:w" s="T444">muzʼejdar</ts>
                  <nts id="Seg_1507" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1509" n="HIAT:w" s="T445">üstün</ts>
                  <nts id="Seg_1510" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1512" n="HIAT:w" s="T446">tʼeplaxokka</ts>
                  <nts id="Seg_1513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1515" n="HIAT:w" s="T447">hɨldʼɨbɨppɨt</ts>
                  <nts id="Seg_1516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1517" n="HIAT:ip">"</nts>
                  <ts e="T449" id="Seg_1519" n="HIAT:w" s="T448">Irtɨš</ts>
                  <nts id="Seg_1520" n="HIAT:ip">"</nts>
                  <nts id="Seg_1521" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T450" id="Seg_1523" n="HIAT:w" s="T449">di͡en</ts>
                  <nts id="Seg_1524" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1526" n="HIAT:w" s="T450">eː</ts>
                  <nts id="Seg_1527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1529" n="HIAT:w" s="T451">ürek</ts>
                  <nts id="Seg_1530" n="HIAT:ip">,</nts>
                  <nts id="Seg_1531" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1532" n="HIAT:ip">"</nts>
                  <ts e="T453" id="Seg_1534" n="HIAT:w" s="T452">Irtɨš</ts>
                  <nts id="Seg_1535" n="HIAT:ip">"</nts>
                  <nts id="Seg_1536" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1538" n="HIAT:w" s="T453">di͡en</ts>
                  <nts id="Seg_1539" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1541" n="HIAT:w" s="T454">ürek</ts>
                  <nts id="Seg_1542" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1544" n="HIAT:w" s="T455">üstün</ts>
                  <nts id="Seg_1545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1547" n="HIAT:w" s="T456">barbɨppɨt</ts>
                  <nts id="Seg_1548" n="HIAT:ip">.</nts>
                  <nts id="Seg_1549" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T469" id="Seg_1551" n="HIAT:u" s="T457">
                  <ts e="T458" id="Seg_1553" n="HIAT:w" s="T457">Ol</ts>
                  <nts id="Seg_1554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1556" n="HIAT:w" s="T458">Irtɨš</ts>
                  <nts id="Seg_1557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1559" n="HIAT:w" s="T459">bu͡olla</ts>
                  <nts id="Seg_1560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1562" n="HIAT:w" s="T460">ulakan</ts>
                  <nts id="Seg_1563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1565" n="HIAT:w" s="T461">ürekke</ts>
                  <nts id="Seg_1566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1568" n="HIAT:w" s="T462">kɨttar</ts>
                  <nts id="Seg_1569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1570" n="HIAT:ip">"</nts>
                  <ts e="T464" id="Seg_1572" n="HIAT:w" s="T463">Obʼ</ts>
                  <nts id="Seg_1573" n="HIAT:ip">"</nts>
                  <nts id="Seg_1574" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1576" n="HIAT:w" s="T464">di͡en</ts>
                  <nts id="Seg_1577" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1579" n="HIAT:w" s="T465">ürekke</ts>
                  <nts id="Seg_1580" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1582" n="HIAT:w" s="T466">ol</ts>
                  <nts id="Seg_1583" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T468" id="Seg_1585" n="HIAT:w" s="T467">kɨttɨbɨttarga</ts>
                  <nts id="Seg_1586" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1588" n="HIAT:w" s="T468">belektemmippit</ts>
                  <nts id="Seg_1589" n="HIAT:ip">.</nts>
                  <nts id="Seg_1590" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T477" id="Seg_1592" n="HIAT:u" s="T469">
                  <nts id="Seg_1593" n="HIAT:ip">(</nts>
                  <ts e="T470" id="Seg_1595" n="HIAT:w" s="T469">Ɨlarɨ-</ts>
                  <nts id="Seg_1596" n="HIAT:ip">)</nts>
                  <nts id="Seg_1597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1599" n="HIAT:w" s="T470">ɨrɨ͡alarɨ</ts>
                  <nts id="Seg_1600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1602" n="HIAT:w" s="T471">ɨllaːbɨppɨt</ts>
                  <nts id="Seg_1603" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T473" id="Seg_1605" n="HIAT:w" s="T472">keleːribit</ts>
                  <nts id="Seg_1606" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_1608" n="HIAT:w" s="T473">baraːrɨbɨt</ts>
                  <nts id="Seg_1609" n="HIAT:ip">,</nts>
                  <nts id="Seg_1610" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_1612" n="HIAT:w" s="T474">beseleː</ts>
                  <nts id="Seg_1613" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_1615" n="HIAT:w" s="T475">bagaji</ts>
                  <nts id="Seg_1616" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_1618" n="HIAT:w" s="T476">ete</ts>
                  <nts id="Seg_1619" n="HIAT:ip">.</nts>
                  <nts id="Seg_1620" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T487" id="Seg_1622" n="HIAT:u" s="T477">
                  <ts e="T478" id="Seg_1624" n="HIAT:w" s="T477">Kʼinoːga</ts>
                  <nts id="Seg_1625" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T479" id="Seg_1627" n="HIAT:w" s="T478">uhulbuttara</ts>
                  <nts id="Seg_1628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1630" n="HIAT:w" s="T479">bihigitin</ts>
                  <nts id="Seg_1631" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T481" id="Seg_1633" n="HIAT:w" s="T480">onton</ts>
                  <nts id="Seg_1634" n="HIAT:ip">,</nts>
                  <nts id="Seg_1635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1637" n="HIAT:w" s="T481">onton</ts>
                  <nts id="Seg_1638" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T483" id="Seg_1640" n="HIAT:w" s="T482">töttörü</ts>
                  <nts id="Seg_1641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T484" id="Seg_1643" n="HIAT:w" s="T483">kelemmit</ts>
                  <nts id="Seg_1644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T485" id="Seg_1646" n="HIAT:w" s="T484">eː</ts>
                  <nts id="Seg_1647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1649" n="HIAT:w" s="T485">ol</ts>
                  <nts id="Seg_1650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1652" n="HIAT:w" s="T486">harsɨŋŋɨtɨn</ts>
                  <nts id="Seg_1653" n="HIAT:ip">.</nts>
                  <nts id="Seg_1654" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T505" id="Seg_1656" n="HIAT:u" s="T487">
                  <ts e="T488" id="Seg_1658" n="HIAT:w" s="T487">Ol</ts>
                  <nts id="Seg_1659" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1661" n="HIAT:w" s="T488">üs</ts>
                  <nts id="Seg_1662" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1664" n="HIAT:w" s="T489">künü</ts>
                  <nts id="Seg_1665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1667" n="HIAT:w" s="T490">bu͡olbupput</ts>
                  <nts id="Seg_1668" n="HIAT:ip">,</nts>
                  <nts id="Seg_1669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1671" n="HIAT:w" s="T491">eː</ts>
                  <nts id="Seg_1672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1674" n="HIAT:w" s="T492">üs</ts>
                  <nts id="Seg_1675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1677" n="HIAT:w" s="T493">künü</ts>
                  <nts id="Seg_1678" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1680" n="HIAT:w" s="T494">bu͡olaːččɨ</ts>
                  <nts id="Seg_1681" n="HIAT:ip">,</nts>
                  <nts id="Seg_1682" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1684" n="HIAT:w" s="T495">eː</ts>
                  <nts id="Seg_1685" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T497" id="Seg_1687" n="HIAT:w" s="T496">üs</ts>
                  <nts id="Seg_1688" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1690" n="HIAT:w" s="T497">künü</ts>
                  <nts id="Seg_1691" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1693" n="HIAT:w" s="T498">bu͡olbupputugar</ts>
                  <nts id="Seg_1694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1696" n="HIAT:w" s="T499">Xantɨ-Mansijskajga</ts>
                  <nts id="Seg_1697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1699" n="HIAT:w" s="T500">üčügej</ts>
                  <nts id="Seg_1700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1702" n="HIAT:w" s="T501">bagaji</ts>
                  <nts id="Seg_1703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1705" n="HIAT:w" s="T502">künner</ts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1708" n="HIAT:w" s="T503">etilere</ts>
                  <nts id="Seg_1709" n="HIAT:ip">,</nts>
                  <nts id="Seg_1710" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1712" n="HIAT:w" s="T504">kallaːn</ts>
                  <nts id="Seg_1713" n="HIAT:ip">.</nts>
                  <nts id="Seg_1714" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T525" id="Seg_1716" n="HIAT:u" s="T505">
                  <ts e="T506" id="Seg_1718" n="HIAT:w" s="T505">Ol</ts>
                  <nts id="Seg_1719" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1721" n="HIAT:w" s="T506">innitiger</ts>
                  <nts id="Seg_1722" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1724" n="HIAT:w" s="T507">hamɨːrdɨːr</ts>
                  <nts id="Seg_1725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T509" id="Seg_1727" n="HIAT:w" s="T508">ete</ts>
                  <nts id="Seg_1728" n="HIAT:ip">,</nts>
                  <nts id="Seg_1729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T510" id="Seg_1731" n="HIAT:w" s="T509">diːr</ts>
                  <nts id="Seg_1732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T511" id="Seg_1734" n="HIAT:w" s="T510">etilere</ts>
                  <nts id="Seg_1735" n="HIAT:ip">,</nts>
                  <nts id="Seg_1736" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T512" id="Seg_1738" n="HIAT:w" s="T511">onton</ts>
                  <nts id="Seg_1739" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T513" id="Seg_1741" n="HIAT:w" s="T512">hürbetten</ts>
                  <nts id="Seg_1742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1744" n="HIAT:w" s="T513">taksa</ts>
                  <nts id="Seg_1745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1747" n="HIAT:w" s="T514">gradus</ts>
                  <nts id="Seg_1748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1750" n="HIAT:w" s="T515">ete</ts>
                  <nts id="Seg_1751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T517" id="Seg_1753" n="HIAT:w" s="T516">hürbe</ts>
                  <nts id="Seg_1754" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1756" n="HIAT:w" s="T517">tü͡ört</ts>
                  <nts id="Seg_1757" n="HIAT:ip">,</nts>
                  <nts id="Seg_1758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1760" n="HIAT:w" s="T518">hürbe</ts>
                  <nts id="Seg_1761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T520" id="Seg_1763" n="HIAT:w" s="T519">bi͡es</ts>
                  <nts id="Seg_1764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T521" id="Seg_1766" n="HIAT:w" s="T520">gradus</ts>
                  <nts id="Seg_1767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1769" n="HIAT:w" s="T521">bu͡olan</ts>
                  <nts id="Seg_1770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1772" n="HIAT:w" s="T522">iste</ts>
                  <nts id="Seg_1773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1775" n="HIAT:w" s="T523">iti</ts>
                  <nts id="Seg_1776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1778" n="HIAT:w" s="T524">kütterge</ts>
                  <nts id="Seg_1779" n="HIAT:ip">.</nts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T546" id="Seg_1782" n="HIAT:u" s="T525">
                  <ts e="T526" id="Seg_1784" n="HIAT:w" s="T525">Ol</ts>
                  <nts id="Seg_1785" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1787" n="HIAT:w" s="T526">ihin</ts>
                  <nts id="Seg_1788" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T528" id="Seg_1790" n="HIAT:w" s="T527">bu͡olla</ts>
                  <nts id="Seg_1791" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1793" n="HIAT:w" s="T528">bihi͡eke</ts>
                  <nts id="Seg_1794" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_1796" n="HIAT:w" s="T529">olus</ts>
                  <nts id="Seg_1797" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_1799" n="HIAT:w" s="T530">da</ts>
                  <nts id="Seg_1800" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_1802" n="HIAT:w" s="T531">itiːte</ts>
                  <nts id="Seg_1803" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_1805" n="HIAT:w" s="T532">hu͡ok</ts>
                  <nts id="Seg_1806" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1808" n="HIAT:w" s="T533">ete</ts>
                  <nts id="Seg_1809" n="HIAT:ip">,</nts>
                  <nts id="Seg_1810" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_1812" n="HIAT:w" s="T534">no</ts>
                  <nts id="Seg_1813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1815" n="HIAT:w" s="T535">olus</ts>
                  <nts id="Seg_1816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T537" id="Seg_1818" n="HIAT:w" s="T536">da</ts>
                  <nts id="Seg_1819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_1821" n="HIAT:w" s="T537">i</ts>
                  <nts id="Seg_1822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1824" n="HIAT:w" s="T538">tɨmnɨːta</ts>
                  <nts id="Seg_1825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1827" n="HIAT:w" s="T539">hu͡ok</ts>
                  <nts id="Seg_1828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_1830" n="HIAT:w" s="T540">ete</ts>
                  <nts id="Seg_1831" n="HIAT:ip">,</nts>
                  <nts id="Seg_1832" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_1834" n="HIAT:w" s="T541">ol</ts>
                  <nts id="Seg_1835" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_1837" n="HIAT:w" s="T542">ihin</ts>
                  <nts id="Seg_1838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_1840" n="HIAT:w" s="T543">bert</ts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_1843" n="HIAT:w" s="T544">üčügej</ts>
                  <nts id="Seg_1844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_1846" n="HIAT:w" s="T545">bihi͡eke</ts>
                  <nts id="Seg_1847" n="HIAT:ip">.</nts>
                  <nts id="Seg_1848" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T556" id="Seg_1850" n="HIAT:u" s="T546">
                  <ts e="T547" id="Seg_1852" n="HIAT:w" s="T546">Min</ts>
                  <nts id="Seg_1853" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T548" id="Seg_1855" n="HIAT:w" s="T547">hanaːbar</ts>
                  <nts id="Seg_1856" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_1858" n="HIAT:w" s="T548">itinnik</ts>
                  <nts id="Seg_1859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_1861" n="HIAT:w" s="T549">klimat</ts>
                  <nts id="Seg_1862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_1864" n="HIAT:w" s="T550">ginilerge</ts>
                  <nts id="Seg_1865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_1867" n="HIAT:w" s="T551">itinnik</ts>
                  <nts id="Seg_1868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_1870" n="HIAT:w" s="T552">kallaːn</ts>
                  <nts id="Seg_1871" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_1873" n="HIAT:w" s="T553">bert</ts>
                  <nts id="Seg_1874" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_1876" n="HIAT:w" s="T554">üčügejdik</ts>
                  <nts id="Seg_1877" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_1879" n="HIAT:w" s="T555">tuksar</ts>
                  <nts id="Seg_1880" n="HIAT:ip">.</nts>
                  <nts id="Seg_1881" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T570" id="Seg_1883" n="HIAT:u" s="T556">
                  <ts e="T557" id="Seg_1885" n="HIAT:w" s="T556">Ol</ts>
                  <nts id="Seg_1886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_1888" n="HIAT:w" s="T557">ihin</ts>
                  <nts id="Seg_1889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T559" id="Seg_1891" n="HIAT:w" s="T558">össü͡ö</ts>
                  <nts id="Seg_1892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T560" id="Seg_1894" n="HIAT:w" s="T559">da</ts>
                  <nts id="Seg_1895" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_1897" n="HIAT:w" s="T560">ɨgɨrdaktarɨna</ts>
                  <nts id="Seg_1898" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_1900" n="HIAT:w" s="T561">min</ts>
                  <nts id="Seg_1901" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_1903" n="HIAT:w" s="T562">hanaːbar</ts>
                  <nts id="Seg_1904" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_1906" n="HIAT:w" s="T563">min</ts>
                  <nts id="Seg_1907" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_1909" n="HIAT:w" s="T564">bu͡o</ts>
                  <nts id="Seg_1910" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_1912" n="HIAT:w" s="T565">barar</ts>
                  <nts id="Seg_1913" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_1915" n="HIAT:w" s="T566">kördükpün</ts>
                  <nts id="Seg_1916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_1918" n="HIAT:w" s="T567">össü͡ö</ts>
                  <nts id="Seg_1919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_1921" n="HIAT:w" s="T568">biːrde</ts>
                  <nts id="Seg_1922" n="HIAT:ip">,</nts>
                  <nts id="Seg_1923" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_1925" n="HIAT:w" s="T569">ɨ͡allana</ts>
                  <nts id="Seg_1926" n="HIAT:ip">.</nts>
                  <nts id="Seg_1927" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-UkOA">
            <ts e="T4" id="Seg_1928" n="sc" s="T1">
               <ts e="T2" id="Seg_1930" n="e" s="T1">– Hakalɨː </ts>
               <ts e="T3" id="Seg_1932" n="e" s="T2">kepsi͡em </ts>
               <ts e="T4" id="Seg_1934" n="e" s="T3">du͡o? </ts>
            </ts>
            <ts e="T283" id="Seg_1935" n="sc" s="T8">
               <ts e="T10" id="Seg_1937" n="e" s="T8">– Hürbe </ts>
               <ts e="T11" id="Seg_1939" n="e" s="T10">ikkileːk </ts>
               <ts e="T12" id="Seg_1941" n="e" s="T11">ijunʼŋa </ts>
               <ts e="T13" id="Seg_1943" n="e" s="T12">ikki </ts>
               <ts e="T14" id="Seg_1945" n="e" s="T13">hüːs </ts>
               <ts e="T15" id="Seg_1947" n="e" s="T14">u͡onnaːk </ts>
               <ts e="T16" id="Seg_1949" n="e" s="T15">dʼɨlga </ts>
               <ts e="T17" id="Seg_1951" n="e" s="T16">Xantɨ-Mansijskaj </ts>
               <ts e="T18" id="Seg_1953" n="e" s="T17">gorakka </ts>
               <ts e="T19" id="Seg_1955" n="e" s="T18">bu͡olbuta </ts>
               <ts e="T20" id="Seg_1957" n="e" s="T19">fʼestʼival </ts>
               <ts e="T21" id="Seg_1959" n="e" s="T20">"Sʼevʼernaje </ts>
               <ts e="T22" id="Seg_1961" n="e" s="T21">sʼijanʼije" </ts>
               <ts e="T23" id="Seg_1963" n="e" s="T22">di͡en </ts>
               <ts e="T24" id="Seg_1965" n="e" s="T23">aːttaːk. </ts>
               <ts e="T25" id="Seg_1967" n="e" s="T24">Onno </ts>
               <ts e="T26" id="Seg_1969" n="e" s="T25">Tajmɨːrtan </ts>
               <ts e="T27" id="Seg_1971" n="e" s="T26">bihigi </ts>
               <ts e="T28" id="Seg_1973" n="e" s="T27">barbɨppɨt </ts>
               <ts e="T29" id="Seg_1975" n="e" s="T28">ikki </ts>
               <ts e="T30" id="Seg_1977" n="e" s="T29">kihi. </ts>
               <ts e="T31" id="Seg_1979" n="e" s="T30">Birges </ts>
               <ts e="T32" id="Seg_1981" n="e" s="T31">bu͡olla </ts>
               <ts e="T33" id="Seg_1983" n="e" s="T32">Vladʼimʼir </ts>
               <ts e="T34" id="Seg_1985" n="e" s="T33">Eːnovʼičʼ </ts>
               <ts e="T35" id="Seg_1987" n="e" s="T34">Sʼigunʼej, </ts>
               <ts e="T36" id="Seg_1989" n="e" s="T35">"Naroːdnaj </ts>
               <ts e="T37" id="Seg_1991" n="e" s="T36">Artʼist </ts>
               <ts e="T38" id="Seg_1993" n="e" s="T37">Rassʼii" </ts>
               <ts e="T39" id="Seg_1995" n="e" s="T38">di͡en </ts>
               <ts e="T40" id="Seg_1997" n="e" s="T39">kihi </ts>
               <ts e="T41" id="Seg_1999" n="e" s="T40">aːttaːk </ts>
               <ts e="T42" id="Seg_2001" n="e" s="T41">"Zaslužennɨj </ts>
               <ts e="T43" id="Seg_2003" n="e" s="T42">artʼist </ts>
               <ts e="T44" id="Seg_2005" n="e" s="T43">Rassʼii" </ts>
               <ts e="T45" id="Seg_2007" n="e" s="T44">i </ts>
               <ts e="T46" id="Seg_2009" n="e" s="T45">min </ts>
               <ts e="T47" id="Seg_2011" n="e" s="T46">Nasku͡ottan. </ts>
               <ts e="T48" id="Seg_2013" n="e" s="T47">Onno </ts>
               <ts e="T49" id="Seg_2015" n="e" s="T48">bert </ts>
               <ts e="T50" id="Seg_2017" n="e" s="T49">üčügejdik </ts>
               <ts e="T51" id="Seg_2019" n="e" s="T50">bihigitin </ts>
               <ts e="T52" id="Seg_2021" n="e" s="T51">tohujdular, </ts>
               <ts e="T53" id="Seg_2023" n="e" s="T52">tuːstaːk </ts>
               <ts e="T54" id="Seg_2025" n="e" s="T53">kili͡ebinen. </ts>
               <ts e="T55" id="Seg_2027" n="e" s="T54">Ulakan </ts>
               <ts e="T56" id="Seg_2029" n="e" s="T55">bagaj </ts>
               <ts e="T57" id="Seg_2031" n="e" s="T56">gastʼinʼisaga </ts>
               <ts e="T58" id="Seg_2033" n="e" s="T57">dʼi͡eleːtiler, </ts>
               <ts e="T59" id="Seg_2035" n="e" s="T58">aːta </ts>
               <ts e="T60" id="Seg_2037" n="e" s="T59">bu͡olla </ts>
               <ts e="T61" id="Seg_2039" n="e" s="T60">"Hette </ts>
               <ts e="T62" id="Seg_2041" n="e" s="T61">girbej", </ts>
               <ts e="T63" id="Seg_2043" n="e" s="T62">"Semʼ </ts>
               <ts e="T64" id="Seg_2045" n="e" s="T63">xalmov". </ts>
               <ts e="T65" id="Seg_2047" n="e" s="T64">Bert </ts>
               <ts e="T66" id="Seg_2049" n="e" s="T65">üčügejdik </ts>
               <ts e="T67" id="Seg_2051" n="e" s="T66">bihigini </ts>
               <ts e="T68" id="Seg_2053" n="e" s="T67">ahatallar </ts>
               <ts e="T69" id="Seg_2055" n="e" s="T68">onno, </ts>
               <ts e="T70" id="Seg_2057" n="e" s="T69">ahappɨttara, </ts>
               <ts e="T71" id="Seg_2059" n="e" s="T70">utupputtara, </ts>
               <ts e="T72" id="Seg_2061" n="e" s="T71">onton </ts>
               <ts e="T73" id="Seg_2063" n="e" s="T72">harsɨŋŋɨtɨn </ts>
               <ts e="T74" id="Seg_2065" n="e" s="T73">rʼepʼetʼicɨjalaːppɨt. </ts>
               <ts e="T75" id="Seg_2067" n="e" s="T74">Hol </ts>
               <ts e="T76" id="Seg_2069" n="e" s="T75">ki͡ehetin, </ts>
               <ts e="T77" id="Seg_2071" n="e" s="T76">hol </ts>
               <ts e="T78" id="Seg_2073" n="e" s="T77">ki͡ehetin </ts>
               <ts e="T79" id="Seg_2075" n="e" s="T78">že </ts>
               <ts e="T80" id="Seg_2077" n="e" s="T79">vɨstupajdaːppɨt. </ts>
               <ts e="T81" id="Seg_2079" n="e" s="T80">Ol </ts>
               <ts e="T82" id="Seg_2081" n="e" s="T81">kennitten </ts>
               <ts e="T83" id="Seg_2083" n="e" s="T82">tögürük </ts>
               <ts e="T84" id="Seg_2085" n="e" s="T83">ostol </ts>
               <ts e="T85" id="Seg_2087" n="e" s="T84">bu͡olta </ts>
               <ts e="T86" id="Seg_2089" n="e" s="T85">ikkis </ts>
               <ts e="T87" id="Seg_2091" n="e" s="T86">künüger. </ts>
               <ts e="T88" id="Seg_2093" n="e" s="T87">Ol </ts>
               <ts e="T89" id="Seg_2095" n="e" s="T88">tögürük </ts>
               <ts e="T90" id="Seg_2097" n="e" s="T89">ostolgo </ts>
               <ts e="T91" id="Seg_2099" n="e" s="T90">bu͡olla </ts>
               <ts e="T92" id="Seg_2101" n="e" s="T91">eŋin </ts>
               <ts e="T93" id="Seg_2103" n="e" s="T92">eː </ts>
               <ts e="T94" id="Seg_2105" n="e" s="T93">elbek </ts>
               <ts e="T95" id="Seg_2107" n="e" s="T94">kihi </ts>
               <ts e="T96" id="Seg_2109" n="e" s="T95">komullubuta. </ts>
               <ts e="T97" id="Seg_2111" n="e" s="T96">Ü͡örüːnü </ts>
               <ts e="T98" id="Seg_2113" n="e" s="T97">kuhaganɨ </ts>
               <ts e="T99" id="Seg_2115" n="e" s="T98">eː </ts>
               <ts e="T100" id="Seg_2117" n="e" s="T99">olok </ts>
               <ts e="T101" id="Seg_2119" n="e" s="T100">tuhunan </ts>
               <ts e="T102" id="Seg_2121" n="e" s="T101">kepsetiː </ts>
               <ts e="T103" id="Seg_2123" n="e" s="T102">baːr </ts>
               <ts e="T104" id="Seg_2125" n="e" s="T103">ete. </ts>
               <ts e="T105" id="Seg_2127" n="e" s="T104">Ogolor </ts>
               <ts e="T106" id="Seg_2129" n="e" s="T105">ü͡örenellerin </ts>
               <ts e="T107" id="Seg_2131" n="e" s="T106">tustarɨnan </ts>
               <ts e="T108" id="Seg_2133" n="e" s="T107">studʼenttar </ts>
               <ts e="T109" id="Seg_2135" n="e" s="T108">eː </ts>
               <ts e="T110" id="Seg_2137" n="e" s="T109">iti </ts>
               <ts e="T111" id="Seg_2139" n="e" s="T110">ulakan </ts>
               <ts e="T112" id="Seg_2141" n="e" s="T111">vɨššɨj učʼebnɨj zavʼedʼenʼijalar </ts>
               <ts e="T113" id="Seg_2143" n="e" s="T112">tustarɨgar </ts>
               <ts e="T114" id="Seg_2145" n="e" s="T113">togo </ts>
               <ts e="T115" id="Seg_2147" n="e" s="T114">bɨragallarɨn </ts>
               <ts e="T116" id="Seg_2149" n="e" s="T115">ili </ts>
               <ts e="T117" id="Seg_2151" n="e" s="T116">togo </ts>
               <ts e="T118" id="Seg_2153" n="e" s="T117">ü͡öremmetterin </ts>
               <ts e="T119" id="Seg_2155" n="e" s="T118">ol </ts>
               <ts e="T120" id="Seg_2157" n="e" s="T119">ol </ts>
               <ts e="T121" id="Seg_2159" n="e" s="T120">tuhugar </ts>
               <ts e="T122" id="Seg_2161" n="e" s="T121">eː </ts>
               <ts e="T123" id="Seg_2163" n="e" s="T122">ulakan </ts>
               <ts e="T124" id="Seg_2165" n="e" s="T123">onton </ts>
               <ts e="T125" id="Seg_2167" n="e" s="T124">vapros </ts>
               <ts e="T126" id="Seg_2169" n="e" s="T125">turar </ts>
               <ts e="T127" id="Seg_2171" n="e" s="T126">ete. </ts>
               <ts e="T128" id="Seg_2173" n="e" s="T127">Onton </ts>
               <ts e="T129" id="Seg_2175" n="e" s="T128">bu͡olla </ts>
               <ts e="T130" id="Seg_2177" n="e" s="T129">(eme-) </ts>
               <ts e="T131" id="Seg_2179" n="e" s="T130">eː </ts>
               <ts e="T132" id="Seg_2181" n="e" s="T131">emeːksin </ts>
               <ts e="T133" id="Seg_2183" n="e" s="T132">vɨstupajdaːbɨt </ts>
               <ts e="T134" id="Seg_2185" n="e" s="T133">onno, </ts>
               <ts e="T135" id="Seg_2187" n="e" s="T134">Maldaːnova </ts>
               <ts e="T136" id="Seg_2189" n="e" s="T135">di͡en </ts>
               <ts e="T137" id="Seg_2191" n="e" s="T136">Marʼija </ts>
               <ts e="T138" id="Seg_2193" n="e" s="T137">Kuzʼmʼinʼičʼna. </ts>
               <ts e="T139" id="Seg_2195" n="e" s="T138">Öjdöːk </ts>
               <ts e="T140" id="Seg_2197" n="e" s="T139">dʼaktar, </ts>
               <ts e="T141" id="Seg_2199" n="e" s="T140">ol </ts>
               <ts e="T142" id="Seg_2201" n="e" s="T141">haŋarbɨta: </ts>
               <ts e="T143" id="Seg_2203" n="e" s="T142">Eː </ts>
               <ts e="T144" id="Seg_2205" n="e" s="T143">ogo </ts>
               <ts e="T145" id="Seg_2207" n="e" s="T144">kihiler </ts>
               <ts e="T146" id="Seg_2209" n="e" s="T145">eː </ts>
               <ts e="T147" id="Seg_2211" n="e" s="T146">ogo </ts>
               <ts e="T148" id="Seg_2213" n="e" s="T147">kihiler </ts>
               <ts e="T149" id="Seg_2215" n="e" s="T148">bejelerin </ts>
               <ts e="T150" id="Seg_2217" n="e" s="T149">kergenneriger </ts>
               <ts e="T151" id="Seg_2219" n="e" s="T150">bejelerin </ts>
               <ts e="T152" id="Seg_2221" n="e" s="T151">kergennerin </ts>
               <ts e="T153" id="Seg_2223" n="e" s="T152">isteriger </ts>
               <ts e="T154" id="Seg_2225" n="e" s="T153">isteritten </ts>
               <ts e="T155" id="Seg_2227" n="e" s="T154">vaspʼitanʼija </ts>
               <ts e="T156" id="Seg_2229" n="e" s="T155">bu͡olardaːk. </ts>
               <ts e="T157" id="Seg_2231" n="e" s="T156">Töröppüt </ts>
               <ts e="T158" id="Seg_2233" n="e" s="T157">kihi </ts>
               <ts e="T159" id="Seg_2235" n="e" s="T158">bi͡ererdeːk </ts>
               <ts e="T160" id="Seg_2237" n="e" s="T159">ü͡öregi </ts>
               <ts e="T161" id="Seg_2239" n="e" s="T160">ütü͡önü </ts>
               <ts e="T162" id="Seg_2241" n="e" s="T161">kuhaganɨ </ts>
               <ts e="T163" id="Seg_2243" n="e" s="T162">ogogo. </ts>
               <ts e="T164" id="Seg_2245" n="e" s="T163">Aː </ts>
               <ts e="T165" id="Seg_2247" n="e" s="T164">usku͡ola, </ts>
               <ts e="T166" id="Seg_2249" n="e" s="T165">sadʼik </ts>
               <ts e="T167" id="Seg_2251" n="e" s="T166">onuga </ts>
               <ts e="T168" id="Seg_2253" n="e" s="T167">bu͡olla </ts>
               <ts e="T169" id="Seg_2255" n="e" s="T168">eː </ts>
               <ts e="T170" id="Seg_2257" n="e" s="T169">učʼebnɨj zavʼedʼenʼijalar </ts>
               <ts e="T171" id="Seg_2259" n="e" s="T170">onuga </ts>
               <ts e="T172" id="Seg_2261" n="e" s="T171">kömö </ts>
               <ts e="T173" id="Seg_2263" n="e" s="T172">ere </ts>
               <ts e="T174" id="Seg_2265" n="e" s="T173">bu͡olu͡oktarɨn </ts>
               <ts e="T175" id="Seg_2267" n="e" s="T174">naːda, </ts>
               <ts e="T176" id="Seg_2269" n="e" s="T175">a </ts>
               <ts e="T177" id="Seg_2271" n="e" s="T176">anʼi </ts>
               <ts e="T178" id="Seg_2273" n="e" s="T177">asnavnoj </ts>
               <ts e="T179" id="Seg_2275" n="e" s="T178">bazɨ </ts>
               <ts e="T180" id="Seg_2277" n="e" s="T179">kördük </ts>
               <ts e="T181" id="Seg_2279" n="e" s="T180">bu͡olu͡oktarɨn. </ts>
               <ts e="T182" id="Seg_2281" n="e" s="T181">Ol </ts>
               <ts e="T183" id="Seg_2283" n="e" s="T182">ihin </ts>
               <ts e="T184" id="Seg_2285" n="e" s="T183">bu͡olla </ts>
               <ts e="T185" id="Seg_2287" n="e" s="T184">anɨ </ts>
               <ts e="T186" id="Seg_2289" n="e" s="T185">inni </ts>
               <ts e="T187" id="Seg_2291" n="e" s="T186">di͡ek </ts>
               <ts e="T188" id="Seg_2293" n="e" s="T187">eː </ts>
               <ts e="T189" id="Seg_2295" n="e" s="T188">ulaːtɨnnarɨ͡akka </ts>
               <ts e="T190" id="Seg_2297" n="e" s="T189">naːda </ts>
               <ts e="T191" id="Seg_2299" n="e" s="T190">diːr </ts>
               <ts e="T192" id="Seg_2301" n="e" s="T191">eː </ts>
               <ts e="T193" id="Seg_2303" n="e" s="T192">ol </ts>
               <ts e="T194" id="Seg_2305" n="e" s="T193">dʼaktar </ts>
               <ts e="T195" id="Seg_2307" n="e" s="T194">eppi͡eti </ts>
               <ts e="T196" id="Seg_2309" n="e" s="T195">törötör </ts>
               <ts e="T197" id="Seg_2311" n="e" s="T196">kihilerten </ts>
               <ts e="T198" id="Seg_2313" n="e" s="T197">radʼitelʼlartan. </ts>
               <ts e="T199" id="Seg_2315" n="e" s="T198">Da </ts>
               <ts e="T200" id="Seg_2317" n="e" s="T199">i </ts>
               <ts e="T201" id="Seg_2319" n="e" s="T200">ütü͡önü </ts>
               <ts e="T202" id="Seg_2321" n="e" s="T201">elbeːtinneri͡ekke </ts>
               <ts e="T203" id="Seg_2323" n="e" s="T202">di͡en </ts>
               <ts e="T204" id="Seg_2325" n="e" s="T203">haŋarbɨta </ts>
               <ts e="T205" id="Seg_2327" n="e" s="T204">ol </ts>
               <ts e="T206" id="Seg_2329" n="e" s="T205">dʼaktar, </ts>
               <ts e="T207" id="Seg_2331" n="e" s="T206">ütü͡önü </ts>
               <ts e="T208" id="Seg_2333" n="e" s="T207">elbeːtinneri͡ekke </ts>
               <ts e="T209" id="Seg_2335" n="e" s="T208">a </ts>
               <ts e="T210" id="Seg_2337" n="e" s="T209">kuhaganɨ </ts>
               <ts e="T211" id="Seg_2339" n="e" s="T210">agɨjaːtɨnnarɨ͡akka </ts>
               <ts e="T212" id="Seg_2341" n="e" s="T211">bu </ts>
               <ts e="T213" id="Seg_2343" n="e" s="T212">hirge. </ts>
               <ts e="T214" id="Seg_2345" n="e" s="T213">Vot, </ts>
               <ts e="T215" id="Seg_2347" n="e" s="T214">nu, </ts>
               <ts e="T216" id="Seg_2349" n="e" s="T215">koloːn </ts>
               <ts e="T217" id="Seg_2351" n="e" s="T216">kördökkö </ts>
               <ts e="T218" id="Seg_2353" n="e" s="T217">(jeslʼi) </ts>
               <ts e="T219" id="Seg_2355" n="e" s="T218">enigin </ts>
               <ts e="T220" id="Seg_2357" n="e" s="T219">kuhagannɨk </ts>
               <ts e="T221" id="Seg_2359" n="e" s="T220">haŋardɨlar, </ts>
               <ts e="T222" id="Seg_2361" n="e" s="T221">kuhaganɨ </ts>
               <ts e="T223" id="Seg_2363" n="e" s="T222">gɨnnɨlar </ts>
               <ts e="T224" id="Seg_2365" n="e" s="T223">du͡o, </ts>
               <ts e="T225" id="Seg_2367" n="e" s="T224">en </ts>
               <ts e="T226" id="Seg_2369" n="e" s="T225">bu͡o </ts>
               <ts e="T227" id="Seg_2371" n="e" s="T226">kepseːmi͡ekkin </ts>
               <ts e="T228" id="Seg_2373" n="e" s="T227">naːda </ts>
               <ts e="T229" id="Seg_2375" n="e" s="T228">ol </ts>
               <ts e="T230" id="Seg_2377" n="e" s="T229">kihi </ts>
               <ts e="T231" id="Seg_2379" n="e" s="T230">tuhunan, </ts>
               <ts e="T232" id="Seg_2381" n="e" s="T231">diːr </ts>
               <ts e="T233" id="Seg_2383" n="e" s="T232">ol </ts>
               <ts e="T234" id="Seg_2385" n="e" s="T233">dʼaktar. </ts>
               <ts e="T235" id="Seg_2387" n="e" s="T234">Ol </ts>
               <ts e="T236" id="Seg_2389" n="e" s="T235">kihi </ts>
               <ts e="T237" id="Seg_2391" n="e" s="T236">(tuhu-) </ts>
               <ts e="T238" id="Seg_2393" n="e" s="T237">ol </ts>
               <ts e="T239" id="Seg_2395" n="e" s="T238">kihi </ts>
               <ts e="T240" id="Seg_2397" n="e" s="T239">tuhunan </ts>
               <ts e="T241" id="Seg_2399" n="e" s="T240">bejetiger </ts>
               <ts e="T242" id="Seg_2401" n="e" s="T241">haŋarɨmɨ͡akkɨn </ts>
               <ts e="T243" id="Seg_2403" n="e" s="T242">naːda, </ts>
               <ts e="T244" id="Seg_2405" n="e" s="T243">no </ts>
               <ts e="T245" id="Seg_2407" n="e" s="T244">kihilerge </ts>
               <ts e="T246" id="Seg_2409" n="e" s="T245">emi͡e </ts>
               <ts e="T247" id="Seg_2411" n="e" s="T246">kepseːmi͡ekke </ts>
               <ts e="T248" id="Seg_2413" n="e" s="T247">naːda </ts>
               <ts e="T249" id="Seg_2415" n="e" s="T248">štob </ts>
               <ts e="T250" id="Seg_2417" n="e" s="T249">kuhagan </ts>
               <ts e="T251" id="Seg_2419" n="e" s="T250">ulaːtɨmi͡egin. </ts>
               <ts e="T252" id="Seg_2421" n="e" s="T251">A </ts>
               <ts e="T253" id="Seg_2423" n="e" s="T252">üčügejdik </ts>
               <ts e="T254" id="Seg_2425" n="e" s="T253">gɨmmɨt </ts>
               <ts e="T255" id="Seg_2427" n="e" s="T254">kihi͡eke </ts>
               <ts e="T256" id="Seg_2429" n="e" s="T255">bu͡olla </ts>
               <ts e="T257" id="Seg_2431" n="e" s="T256">ol </ts>
               <ts e="T258" id="Seg_2433" n="e" s="T257">kihi͡eke </ts>
               <ts e="T259" id="Seg_2435" n="e" s="T258">možešʼ </ts>
               <ts e="T260" id="Seg_2437" n="e" s="T259">emi͡e </ts>
               <ts e="T261" id="Seg_2439" n="e" s="T260">bejetiger </ts>
               <ts e="T262" id="Seg_2441" n="e" s="T261">haŋarɨmɨ͡akkɨn </ts>
               <ts e="T263" id="Seg_2443" n="e" s="T262">štob </ts>
               <ts e="T264" id="Seg_2445" n="e" s="T263">kihirgeːmi͡egin. </ts>
               <ts e="T265" id="Seg_2447" n="e" s="T264">A </ts>
               <ts e="T266" id="Seg_2449" n="e" s="T265">atɨn </ts>
               <ts e="T267" id="Seg_2451" n="e" s="T266">kihilerge </ts>
               <ts e="T268" id="Seg_2453" n="e" s="T267">kepsi͡ekkin </ts>
               <ts e="T269" id="Seg_2455" n="e" s="T268">naːda </ts>
               <ts e="T270" id="Seg_2457" n="e" s="T269">što </ts>
               <ts e="T271" id="Seg_2459" n="e" s="T270">ol </ts>
               <ts e="T272" id="Seg_2461" n="e" s="T271">kihi </ts>
               <ts e="T273" id="Seg_2463" n="e" s="T272">üčügeji </ts>
               <ts e="T274" id="Seg_2465" n="e" s="T273">gɨmmɨta </ts>
               <ts e="T275" id="Seg_2467" n="e" s="T274">di͡en, </ts>
               <ts e="T276" id="Seg_2469" n="e" s="T275">ol </ts>
               <ts e="T277" id="Seg_2471" n="e" s="T276">kihi </ts>
               <ts e="T278" id="Seg_2473" n="e" s="T277">onnuk </ts>
               <ts e="T279" id="Seg_2475" n="e" s="T278">di͡en, </ts>
               <ts e="T280" id="Seg_2477" n="e" s="T279">ol </ts>
               <ts e="T281" id="Seg_2479" n="e" s="T280">kihi </ts>
               <ts e="T282" id="Seg_2481" n="e" s="T281">itinnik </ts>
               <ts e="T283" id="Seg_2483" n="e" s="T282">di͡en. </ts>
            </ts>
            <ts e="T570" id="Seg_2484" n="sc" s="T284">
               <ts e="T285" id="Seg_2486" n="e" s="T284">Oččogo </ts>
               <ts e="T286" id="Seg_2488" n="e" s="T285">bu͡olla </ts>
               <ts e="T287" id="Seg_2490" n="e" s="T286">ütü͡ö </ts>
               <ts e="T288" id="Seg_2492" n="e" s="T287">ulaːtar. </ts>
               <ts e="T289" id="Seg_2494" n="e" s="T288">Ütü͡ö </ts>
               <ts e="T290" id="Seg_2496" n="e" s="T289">ulaːtar, </ts>
               <ts e="T291" id="Seg_2498" n="e" s="T290">onnuk </ts>
               <ts e="T292" id="Seg_2500" n="e" s="T291">formulannan </ts>
               <ts e="T293" id="Seg_2502" n="e" s="T292">di͡en, </ts>
               <ts e="T294" id="Seg_2504" n="e" s="T293">no </ts>
               <ts e="T295" id="Seg_2506" n="e" s="T294">emi͡e </ts>
               <ts e="T296" id="Seg_2508" n="e" s="T295">tak </ts>
               <ts e="T297" id="Seg_2510" n="e" s="T296">i </ts>
               <ts e="T298" id="Seg_2512" n="e" s="T297">ol </ts>
               <ts e="T299" id="Seg_2514" n="e" s="T298">formulaga </ts>
               <ts e="T300" id="Seg_2516" n="e" s="T299">min </ts>
               <ts e="T301" id="Seg_2518" n="e" s="T300">haglastammappɨn. </ts>
               <ts e="T302" id="Seg_2520" n="e" s="T301">Togo </ts>
               <ts e="T303" id="Seg_2522" n="e" s="T302">di͡eŋŋit? </ts>
               <ts e="T304" id="Seg_2524" n="e" s="T303">Kuhagan </ts>
               <ts e="T305" id="Seg_2526" n="e" s="T304">ikkis </ts>
               <ts e="T306" id="Seg_2528" n="e" s="T305">formula </ts>
               <ts e="T307" id="Seg_2530" n="e" s="T306">baːr </ts>
               <ts e="T308" id="Seg_2532" n="e" s="T307">ol </ts>
               <ts e="T309" id="Seg_2534" n="e" s="T308">ürdütüger </ts>
               <ts e="T310" id="Seg_2536" n="e" s="T309">oččogo </ts>
               <ts e="T311" id="Seg_2538" n="e" s="T310">kuhagan </ts>
               <ts e="T312" id="Seg_2540" n="e" s="T311">ulaːtɨmɨ͡agɨn. </ts>
               <ts e="T313" id="Seg_2542" n="e" s="T312">Kuhaganɨ, </ts>
               <ts e="T314" id="Seg_2544" n="e" s="T313">inni </ts>
               <ts e="T315" id="Seg_2546" n="e" s="T314">di͡ek </ts>
               <ts e="T316" id="Seg_2548" n="e" s="T315">töttörü </ts>
               <ts e="T317" id="Seg_2550" n="e" s="T316">ulakanɨ </ts>
               <ts e="T318" id="Seg_2552" n="e" s="T317">eppi͡et </ts>
               <ts e="T319" id="Seg_2554" n="e" s="T318">bi͡eri͡ekke </ts>
               <ts e="T320" id="Seg_2556" n="e" s="T319">naːda </ts>
               <ts e="T321" id="Seg_2558" n="e" s="T320">gini͡eke, </ts>
               <ts e="T322" id="Seg_2560" n="e" s="T321">štob </ts>
               <ts e="T323" id="Seg_2562" n="e" s="T322">kuhaganɨ </ts>
               <ts e="T324" id="Seg_2564" n="e" s="T323">gɨmmɨt </ts>
               <ts e="T325" id="Seg_2566" n="e" s="T324">kihi </ts>
               <ts e="T326" id="Seg_2568" n="e" s="T325">bili͡egin, </ts>
               <ts e="T327" id="Seg_2570" n="e" s="T326">što </ts>
               <ts e="T328" id="Seg_2572" n="e" s="T327">meːne </ts>
               <ts e="T329" id="Seg_2574" n="e" s="T328">kihini </ts>
               <ts e="T330" id="Seg_2576" n="e" s="T329">gini͡ene </ts>
               <ts e="T331" id="Seg_2578" n="e" s="T330">kuhagana </ts>
               <ts e="T332" id="Seg_2580" n="e" s="T331">meːne </ts>
               <ts e="T333" id="Seg_2582" n="e" s="T332">ahɨ͡a </ts>
               <ts e="T334" id="Seg_2584" n="e" s="T333">hu͡oga </ts>
               <ts e="T335" id="Seg_2586" n="e" s="T334">gini͡eke. </ts>
               <ts e="T336" id="Seg_2588" n="e" s="T335">Gini </ts>
               <ts e="T337" id="Seg_2590" n="e" s="T336">bu͡olla </ts>
               <ts e="T338" id="Seg_2592" n="e" s="T337">kihi </ts>
               <ts e="T339" id="Seg_2594" n="e" s="T338">innitiger </ts>
               <ts e="T340" id="Seg_2596" n="e" s="T339">eppi͡ettiːrdeːk </ts>
               <ts e="T341" id="Seg_2598" n="e" s="T340">štob </ts>
               <ts e="T342" id="Seg_2600" n="e" s="T341">inni </ts>
               <ts e="T343" id="Seg_2602" n="e" s="T342">di͡ek </ts>
               <ts e="T344" id="Seg_2604" n="e" s="T343">kuhaganɨ </ts>
               <ts e="T345" id="Seg_2606" n="e" s="T344">gɨnɨmɨ͡agɨn, </ts>
               <ts e="T346" id="Seg_2608" n="e" s="T345">oččogo </ts>
               <ts e="T347" id="Seg_2610" n="e" s="T346">bu͡olla </ts>
               <ts e="T348" id="Seg_2612" n="e" s="T347">ütü͡ö </ts>
               <ts e="T349" id="Seg_2614" n="e" s="T348">ikkite </ts>
               <ts e="T350" id="Seg_2616" n="e" s="T349">raz </ts>
               <ts e="T351" id="Seg_2618" n="e" s="T350">ulaːtan </ts>
               <ts e="T352" id="Seg_2620" n="e" s="T351">taksɨ͡aga. </ts>
               <ts e="T353" id="Seg_2622" n="e" s="T352">I </ts>
               <ts e="T354" id="Seg_2624" n="e" s="T353">oloroːčču </ts>
               <ts e="T355" id="Seg_2626" n="e" s="T354">kihiler, </ts>
               <ts e="T356" id="Seg_2628" n="e" s="T355">onno </ts>
               <ts e="T357" id="Seg_2630" n="e" s="T356">oloroːčču </ts>
               <ts e="T358" id="Seg_2632" n="e" s="T357">kihiler </ts>
               <ts e="T359" id="Seg_2634" n="e" s="T358">bejelerin </ts>
               <ts e="T360" id="Seg_2636" n="e" s="T359">inni </ts>
               <ts e="T361" id="Seg_2638" n="e" s="T360">di͡ek, </ts>
               <ts e="T362" id="Seg_2640" n="e" s="T361">bejelerin </ts>
               <ts e="T363" id="Seg_2642" n="e" s="T362">inni </ts>
               <ts e="T364" id="Seg_2644" n="e" s="T363">di͡ek </ts>
               <ts e="T365" id="Seg_2646" n="e" s="T364">atagastatɨ͡aktara </ts>
               <ts e="T366" id="Seg_2648" n="e" s="T365">hu͡oktara </ts>
               <ts e="T367" id="Seg_2650" n="e" s="T366">da </ts>
               <ts e="T368" id="Seg_2652" n="e" s="T367">atagastatɨ͡aktara </ts>
               <ts e="T369" id="Seg_2654" n="e" s="T368">hu͡oktara. </ts>
               <ts e="T370" id="Seg_2656" n="e" s="T369">Aː </ts>
               <ts e="T371" id="Seg_2658" n="e" s="T370">no </ts>
               <ts e="T372" id="Seg_2660" n="e" s="T371">onton </ts>
               <ts e="T373" id="Seg_2662" n="e" s="T372">ol, </ts>
               <ts e="T374" id="Seg_2664" n="e" s="T373">ol </ts>
               <ts e="T375" id="Seg_2666" n="e" s="T374">kruglɨj </ts>
               <ts e="T376" id="Seg_2668" n="e" s="T375">tögürük </ts>
               <ts e="T377" id="Seg_2670" n="e" s="T376">ostol </ts>
               <ts e="T378" id="Seg_2672" n="e" s="T377">büppütün </ts>
               <ts e="T379" id="Seg_2674" n="e" s="T378">kenne </ts>
               <ts e="T380" id="Seg_2676" n="e" s="T379">emi͡e </ts>
               <ts e="T381" id="Seg_2678" n="e" s="T380">kancert, </ts>
               <ts e="T382" id="Seg_2680" n="e" s="T381">"gala - </ts>
               <ts e="T383" id="Seg_2682" n="e" s="T382">kancert" </ts>
               <ts e="T384" id="Seg_2684" n="e" s="T383">di͡en </ts>
               <ts e="T385" id="Seg_2686" n="e" s="T384">bu͡olta </ts>
               <ts e="T386" id="Seg_2688" n="e" s="T385">onno, </ts>
               <ts e="T387" id="Seg_2690" n="e" s="T386">bert </ts>
               <ts e="T388" id="Seg_2692" n="e" s="T387">üčügejdik </ts>
               <ts e="T389" id="Seg_2694" n="e" s="T388">vɨstupajdaːbɨttara, </ts>
               <ts e="T390" id="Seg_2696" n="e" s="T389">elbek </ts>
               <ts e="T391" id="Seg_2698" n="e" s="T390">kallʼektiv </ts>
               <ts e="T392" id="Seg_2700" n="e" s="T391">kelbite. </ts>
               <ts e="T393" id="Seg_2702" n="e" s="T392">Eː </ts>
               <ts e="T394" id="Seg_2704" n="e" s="T393">Xantɨ-Mansijskajtan </ts>
               <ts e="T395" id="Seg_2706" n="e" s="T394">bejetitten </ts>
               <ts e="T396" id="Seg_2708" n="e" s="T395">tʼeatralʼnaj </ts>
               <ts e="T397" id="Seg_2710" n="e" s="T396">gruppa </ts>
               <ts e="T398" id="Seg_2712" n="e" s="T397">baːr </ts>
               <ts e="T399" id="Seg_2714" n="e" s="T398">ete </ts>
               <ts e="T400" id="Seg_2716" n="e" s="T399">bihi͡ene </ts>
               <ts e="T401" id="Seg_2718" n="e" s="T400">ansamblʼ </ts>
               <ts e="T402" id="Seg_2720" n="e" s="T401">Xejrobut </ts>
               <ts e="T403" id="Seg_2722" n="e" s="T402">kördük. </ts>
               <ts e="T404" id="Seg_2724" n="e" s="T403">Onton </ts>
               <ts e="T405" id="Seg_2726" n="e" s="T404">bu͡olla </ts>
               <ts e="T406" id="Seg_2728" n="e" s="T405">Magadantan </ts>
               <ts e="T407" id="Seg_2730" n="e" s="T406">kelbittere, </ts>
               <ts e="T408" id="Seg_2732" n="e" s="T407">eː </ts>
               <ts e="T409" id="Seg_2734" n="e" s="T408">Jamala_Nʼenʼeckij avtonomnɨj oːkrugtan, </ts>
               <ts e="T410" id="Seg_2736" n="e" s="T409">Čʼukotkattan </ts>
               <ts e="T411" id="Seg_2738" n="e" s="T410">ansamblʼ </ts>
               <ts e="T412" id="Seg_2740" n="e" s="T411">baːr </ts>
               <ts e="T413" id="Seg_2742" n="e" s="T412">ete, </ts>
               <ts e="T414" id="Seg_2744" n="e" s="T413">ɨraːk </ts>
               <ts e="T415" id="Seg_2746" n="e" s="T414">hirten </ts>
               <ts e="T416" id="Seg_2748" n="e" s="T415">keliteleːbitter. </ts>
               <ts e="T417" id="Seg_2750" n="e" s="T416">Onton </ts>
               <ts e="T418" id="Seg_2752" n="e" s="T417">elbek-elbek </ts>
               <ts e="T419" id="Seg_2754" n="e" s="T418">hirten </ts>
               <ts e="T420" id="Seg_2756" n="e" s="T419">keliteleːbitter </ts>
               <ts e="T421" id="Seg_2758" n="e" s="T420">onno. </ts>
               <ts e="T422" id="Seg_2760" n="e" s="T421">Hürbe </ts>
               <ts e="T423" id="Seg_2762" n="e" s="T422">ikki </ts>
               <ts e="T424" id="Seg_2764" n="e" s="T423">kallʼektʼiv </ts>
               <ts e="T425" id="Seg_2766" n="e" s="T424">baːr </ts>
               <ts e="T426" id="Seg_2768" n="e" s="T425">ete, </ts>
               <ts e="T427" id="Seg_2770" n="e" s="T426">eː. </ts>
               <ts e="T428" id="Seg_2772" n="e" s="T427">Onton </ts>
               <ts e="T429" id="Seg_2774" n="e" s="T428">kuratardarbɨt </ts>
               <ts e="T430" id="Seg_2776" n="e" s="T429">üčügej </ts>
               <ts e="T431" id="Seg_2778" n="e" s="T430">bagajɨ </ts>
               <ts e="T432" id="Seg_2780" n="e" s="T431">dʼaktattar </ts>
               <ts e="T433" id="Seg_2782" n="e" s="T432">etilere </ts>
               <ts e="T434" id="Seg_2784" n="e" s="T433">bihigitin </ts>
               <ts e="T435" id="Seg_2786" n="e" s="T434">barɨtɨn </ts>
               <ts e="T436" id="Seg_2788" n="e" s="T435">eː </ts>
               <ts e="T437" id="Seg_2790" n="e" s="T436">illenner, </ts>
               <ts e="T438" id="Seg_2792" n="e" s="T437">egelenner, </ts>
               <ts e="T439" id="Seg_2794" n="e" s="T438">küččügüj </ts>
               <ts e="T440" id="Seg_2796" n="e" s="T439">ogoluː </ts>
               <ts e="T441" id="Seg_2798" n="e" s="T440">ututannar, </ts>
               <ts e="T442" id="Seg_2800" n="e" s="T441">onton </ts>
               <ts e="T443" id="Seg_2802" n="e" s="T442">ahatannar, </ts>
               <ts e="T444" id="Seg_2804" n="e" s="T443">onton </ts>
               <ts e="T445" id="Seg_2806" n="e" s="T444">muzʼejdar </ts>
               <ts e="T446" id="Seg_2808" n="e" s="T445">üstün </ts>
               <ts e="T447" id="Seg_2810" n="e" s="T446">tʼeplaxokka </ts>
               <ts e="T448" id="Seg_2812" n="e" s="T447">hɨldʼɨbɨppɨt </ts>
               <ts e="T449" id="Seg_2814" n="e" s="T448">"Irtɨš" </ts>
               <ts e="T450" id="Seg_2816" n="e" s="T449">di͡en </ts>
               <ts e="T451" id="Seg_2818" n="e" s="T450">eː </ts>
               <ts e="T452" id="Seg_2820" n="e" s="T451">ürek, </ts>
               <ts e="T453" id="Seg_2822" n="e" s="T452">"Irtɨš" </ts>
               <ts e="T454" id="Seg_2824" n="e" s="T453">di͡en </ts>
               <ts e="T455" id="Seg_2826" n="e" s="T454">ürek </ts>
               <ts e="T456" id="Seg_2828" n="e" s="T455">üstün </ts>
               <ts e="T457" id="Seg_2830" n="e" s="T456">barbɨppɨt. </ts>
               <ts e="T458" id="Seg_2832" n="e" s="T457">Ol </ts>
               <ts e="T459" id="Seg_2834" n="e" s="T458">Irtɨš </ts>
               <ts e="T460" id="Seg_2836" n="e" s="T459">bu͡olla </ts>
               <ts e="T461" id="Seg_2838" n="e" s="T460">ulakan </ts>
               <ts e="T462" id="Seg_2840" n="e" s="T461">ürekke </ts>
               <ts e="T463" id="Seg_2842" n="e" s="T462">kɨttar </ts>
               <ts e="T464" id="Seg_2844" n="e" s="T463">"Obʼ" </ts>
               <ts e="T465" id="Seg_2846" n="e" s="T464">di͡en </ts>
               <ts e="T466" id="Seg_2848" n="e" s="T465">ürekke </ts>
               <ts e="T467" id="Seg_2850" n="e" s="T466">ol </ts>
               <ts e="T468" id="Seg_2852" n="e" s="T467">kɨttɨbɨttarga </ts>
               <ts e="T469" id="Seg_2854" n="e" s="T468">belektemmippit. </ts>
               <ts e="T470" id="Seg_2856" n="e" s="T469">(Ɨlarɨ-) </ts>
               <ts e="T471" id="Seg_2858" n="e" s="T470">ɨrɨ͡alarɨ </ts>
               <ts e="T472" id="Seg_2860" n="e" s="T471">ɨllaːbɨppɨt </ts>
               <ts e="T473" id="Seg_2862" n="e" s="T472">keleːribit </ts>
               <ts e="T474" id="Seg_2864" n="e" s="T473">baraːrɨbɨt, </ts>
               <ts e="T475" id="Seg_2866" n="e" s="T474">beseleː </ts>
               <ts e="T476" id="Seg_2868" n="e" s="T475">bagaji </ts>
               <ts e="T477" id="Seg_2870" n="e" s="T476">ete. </ts>
               <ts e="T478" id="Seg_2872" n="e" s="T477">Kʼinoːga </ts>
               <ts e="T479" id="Seg_2874" n="e" s="T478">uhulbuttara </ts>
               <ts e="T480" id="Seg_2876" n="e" s="T479">bihigitin </ts>
               <ts e="T481" id="Seg_2878" n="e" s="T480">onton, </ts>
               <ts e="T482" id="Seg_2880" n="e" s="T481">onton </ts>
               <ts e="T483" id="Seg_2882" n="e" s="T482">töttörü </ts>
               <ts e="T484" id="Seg_2884" n="e" s="T483">kelemmit </ts>
               <ts e="T485" id="Seg_2886" n="e" s="T484">eː </ts>
               <ts e="T486" id="Seg_2888" n="e" s="T485">ol </ts>
               <ts e="T487" id="Seg_2890" n="e" s="T486">harsɨŋŋɨtɨn. </ts>
               <ts e="T488" id="Seg_2892" n="e" s="T487">Ol </ts>
               <ts e="T489" id="Seg_2894" n="e" s="T488">üs </ts>
               <ts e="T490" id="Seg_2896" n="e" s="T489">künü </ts>
               <ts e="T491" id="Seg_2898" n="e" s="T490">bu͡olbupput, </ts>
               <ts e="T492" id="Seg_2900" n="e" s="T491">eː </ts>
               <ts e="T493" id="Seg_2902" n="e" s="T492">üs </ts>
               <ts e="T494" id="Seg_2904" n="e" s="T493">künü </ts>
               <ts e="T495" id="Seg_2906" n="e" s="T494">bu͡olaːččɨ, </ts>
               <ts e="T496" id="Seg_2908" n="e" s="T495">eː </ts>
               <ts e="T497" id="Seg_2910" n="e" s="T496">üs </ts>
               <ts e="T498" id="Seg_2912" n="e" s="T497">künü </ts>
               <ts e="T499" id="Seg_2914" n="e" s="T498">bu͡olbupputugar </ts>
               <ts e="T500" id="Seg_2916" n="e" s="T499">Xantɨ-Mansijskajga </ts>
               <ts e="T501" id="Seg_2918" n="e" s="T500">üčügej </ts>
               <ts e="T502" id="Seg_2920" n="e" s="T501">bagaji </ts>
               <ts e="T503" id="Seg_2922" n="e" s="T502">künner </ts>
               <ts e="T504" id="Seg_2924" n="e" s="T503">etilere, </ts>
               <ts e="T505" id="Seg_2926" n="e" s="T504">kallaːn. </ts>
               <ts e="T506" id="Seg_2928" n="e" s="T505">Ol </ts>
               <ts e="T507" id="Seg_2930" n="e" s="T506">innitiger </ts>
               <ts e="T508" id="Seg_2932" n="e" s="T507">hamɨːrdɨːr </ts>
               <ts e="T509" id="Seg_2934" n="e" s="T508">ete, </ts>
               <ts e="T510" id="Seg_2936" n="e" s="T509">diːr </ts>
               <ts e="T511" id="Seg_2938" n="e" s="T510">etilere, </ts>
               <ts e="T512" id="Seg_2940" n="e" s="T511">onton </ts>
               <ts e="T513" id="Seg_2942" n="e" s="T512">hürbetten </ts>
               <ts e="T514" id="Seg_2944" n="e" s="T513">taksa </ts>
               <ts e="T515" id="Seg_2946" n="e" s="T514">gradus </ts>
               <ts e="T516" id="Seg_2948" n="e" s="T515">ete </ts>
               <ts e="T517" id="Seg_2950" n="e" s="T516">hürbe </ts>
               <ts e="T518" id="Seg_2952" n="e" s="T517">tü͡ört, </ts>
               <ts e="T519" id="Seg_2954" n="e" s="T518">hürbe </ts>
               <ts e="T520" id="Seg_2956" n="e" s="T519">bi͡es </ts>
               <ts e="T521" id="Seg_2958" n="e" s="T520">gradus </ts>
               <ts e="T522" id="Seg_2960" n="e" s="T521">bu͡olan </ts>
               <ts e="T523" id="Seg_2962" n="e" s="T522">iste </ts>
               <ts e="T524" id="Seg_2964" n="e" s="T523">iti </ts>
               <ts e="T525" id="Seg_2966" n="e" s="T524">kütterge. </ts>
               <ts e="T526" id="Seg_2968" n="e" s="T525">Ol </ts>
               <ts e="T527" id="Seg_2970" n="e" s="T526">ihin </ts>
               <ts e="T528" id="Seg_2972" n="e" s="T527">bu͡olla </ts>
               <ts e="T529" id="Seg_2974" n="e" s="T528">bihi͡eke </ts>
               <ts e="T530" id="Seg_2976" n="e" s="T529">olus </ts>
               <ts e="T531" id="Seg_2978" n="e" s="T530">da </ts>
               <ts e="T532" id="Seg_2980" n="e" s="T531">itiːte </ts>
               <ts e="T533" id="Seg_2982" n="e" s="T532">hu͡ok </ts>
               <ts e="T534" id="Seg_2984" n="e" s="T533">ete, </ts>
               <ts e="T535" id="Seg_2986" n="e" s="T534">no </ts>
               <ts e="T536" id="Seg_2988" n="e" s="T535">olus </ts>
               <ts e="T537" id="Seg_2990" n="e" s="T536">da </ts>
               <ts e="T538" id="Seg_2992" n="e" s="T537">i </ts>
               <ts e="T539" id="Seg_2994" n="e" s="T538">tɨmnɨːta </ts>
               <ts e="T540" id="Seg_2996" n="e" s="T539">hu͡ok </ts>
               <ts e="T541" id="Seg_2998" n="e" s="T540">ete, </ts>
               <ts e="T542" id="Seg_3000" n="e" s="T541">ol </ts>
               <ts e="T543" id="Seg_3002" n="e" s="T542">ihin </ts>
               <ts e="T544" id="Seg_3004" n="e" s="T543">bert </ts>
               <ts e="T545" id="Seg_3006" n="e" s="T544">üčügej </ts>
               <ts e="T546" id="Seg_3008" n="e" s="T545">bihi͡eke. </ts>
               <ts e="T547" id="Seg_3010" n="e" s="T546">Min </ts>
               <ts e="T548" id="Seg_3012" n="e" s="T547">hanaːbar </ts>
               <ts e="T549" id="Seg_3014" n="e" s="T548">itinnik </ts>
               <ts e="T550" id="Seg_3016" n="e" s="T549">klimat </ts>
               <ts e="T551" id="Seg_3018" n="e" s="T550">ginilerge </ts>
               <ts e="T552" id="Seg_3020" n="e" s="T551">itinnik </ts>
               <ts e="T553" id="Seg_3022" n="e" s="T552">kallaːn </ts>
               <ts e="T554" id="Seg_3024" n="e" s="T553">bert </ts>
               <ts e="T555" id="Seg_3026" n="e" s="T554">üčügejdik </ts>
               <ts e="T556" id="Seg_3028" n="e" s="T555">tuksar. </ts>
               <ts e="T557" id="Seg_3030" n="e" s="T556">Ol </ts>
               <ts e="T558" id="Seg_3032" n="e" s="T557">ihin </ts>
               <ts e="T559" id="Seg_3034" n="e" s="T558">össü͡ö </ts>
               <ts e="T560" id="Seg_3036" n="e" s="T559">da </ts>
               <ts e="T561" id="Seg_3038" n="e" s="T560">ɨgɨrdaktarɨna </ts>
               <ts e="T562" id="Seg_3040" n="e" s="T561">min </ts>
               <ts e="T563" id="Seg_3042" n="e" s="T562">hanaːbar </ts>
               <ts e="T564" id="Seg_3044" n="e" s="T563">min </ts>
               <ts e="T565" id="Seg_3046" n="e" s="T564">bu͡o </ts>
               <ts e="T566" id="Seg_3048" n="e" s="T565">barar </ts>
               <ts e="T567" id="Seg_3050" n="e" s="T566">kördükpün </ts>
               <ts e="T568" id="Seg_3052" n="e" s="T567">össü͡ö </ts>
               <ts e="T569" id="Seg_3054" n="e" s="T568">biːrde, </ts>
               <ts e="T570" id="Seg_3056" n="e" s="T569">ɨ͡allana. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-UkOA">
            <ta e="T4" id="Seg_3057" s="T1">UkOA_2010_Festival_nar.UkOA.001 (001.001)</ta>
            <ta e="T24" id="Seg_3058" s="T8">UkOA_2010_Festival_nar.UkOA.002 (001.003)</ta>
            <ta e="T30" id="Seg_3059" s="T24">UkOA_2010_Festival_nar.UkOA.003 (001.004)</ta>
            <ta e="T47" id="Seg_3060" s="T30">UkOA_2010_Festival_nar.UkOA.004 (001.005)</ta>
            <ta e="T54" id="Seg_3061" s="T47">UkOA_2010_Festival_nar.UkOA.005 (001.006)</ta>
            <ta e="T64" id="Seg_3062" s="T54">UkOA_2010_Festival_nar.UkOA.006 (001.007)</ta>
            <ta e="T74" id="Seg_3063" s="T64">UkOA_2010_Festival_nar.UkOA.007 (001.008)</ta>
            <ta e="T80" id="Seg_3064" s="T74">UkOA_2010_Festival_nar.UkOA.008 (001.009)</ta>
            <ta e="T87" id="Seg_3065" s="T80">UkOA_2010_Festival_nar.UkOA.009 (001.010)</ta>
            <ta e="T96" id="Seg_3066" s="T87">UkOA_2010_Festival_nar.UkOA.010 (001.011)</ta>
            <ta e="T104" id="Seg_3067" s="T96">UkOA_2010_Festival_nar.UkOA.011 (001.012)</ta>
            <ta e="T127" id="Seg_3068" s="T104">UkOA_2010_Festival_nar.UkOA.012 (001.013)</ta>
            <ta e="T138" id="Seg_3069" s="T127">UkOA_2010_Festival_nar.UkOA.013 (001.014)</ta>
            <ta e="T142" id="Seg_3070" s="T138">UkOA_2010_Festival_nar.UkOA.014 (001.015)</ta>
            <ta e="T156" id="Seg_3071" s="T142">UkOA_2010_Festival_nar.UkOA.015 (001.016)</ta>
            <ta e="T163" id="Seg_3072" s="T156">UkOA_2010_Festival_nar.UkOA.016 (001.017)</ta>
            <ta e="T181" id="Seg_3073" s="T163">UkOA_2010_Festival_nar.UkOA.017 (001.018)</ta>
            <ta e="T198" id="Seg_3074" s="T181">UkOA_2010_Festival_nar.UkOA.018 (001.019)</ta>
            <ta e="T213" id="Seg_3075" s="T198">UkOA_2010_Festival_nar.UkOA.019 (001.020)</ta>
            <ta e="T234" id="Seg_3076" s="T213">UkOA_2010_Festival_nar.UkOA.020 (001.021)</ta>
            <ta e="T251" id="Seg_3077" s="T234">UkOA_2010_Festival_nar.UkOA.021 (001.022)</ta>
            <ta e="T264" id="Seg_3078" s="T251">UkOA_2010_Festival_nar.UkOA.022 (001.023)</ta>
            <ta e="T283" id="Seg_3079" s="T264">UkOA_2010_Festival_nar.UkOA.023 (001.024)</ta>
            <ta e="T288" id="Seg_3080" s="T284">UkOA_2010_Festival_nar.UkOA.024 (001.025)</ta>
            <ta e="T301" id="Seg_3081" s="T288">UkOA_2010_Festival_nar.UkOA.025 (001.026)</ta>
            <ta e="T303" id="Seg_3082" s="T301">UkOA_2010_Festival_nar.UkOA.026 (001.027)</ta>
            <ta e="T312" id="Seg_3083" s="T303">UkOA_2010_Festival_nar.UkOA.027 (001.028)</ta>
            <ta e="T335" id="Seg_3084" s="T312">UkOA_2010_Festival_nar.UkOA.028 (001.029)</ta>
            <ta e="T352" id="Seg_3085" s="T335">UkOA_2010_Festival_nar.UkOA.029 (001.030)</ta>
            <ta e="T369" id="Seg_3086" s="T352">UkOA_2010_Festival_nar.UkOA.030 (001.031)</ta>
            <ta e="T392" id="Seg_3087" s="T369">UkOA_2010_Festival_nar.UkOA.031 (001.032)</ta>
            <ta e="T403" id="Seg_3088" s="T392">UkOA_2010_Festival_nar.UkOA.032 (001.033)</ta>
            <ta e="T416" id="Seg_3089" s="T403">UkOA_2010_Festival_nar.UkOA.033 (001.034)</ta>
            <ta e="T421" id="Seg_3090" s="T416">UkOA_2010_Festival_nar.UkOA.034 (001.035)</ta>
            <ta e="T427" id="Seg_3091" s="T421">UkOA_2010_Festival_nar.UkOA.035 (001.036)</ta>
            <ta e="T457" id="Seg_3092" s="T427">UkOA_2010_Festival_nar.UkOA.036 (001.037)</ta>
            <ta e="T469" id="Seg_3093" s="T457">UkOA_2010_Festival_nar.UkOA.037 (001.038)</ta>
            <ta e="T477" id="Seg_3094" s="T469">UkOA_2010_Festival_nar.UkOA.038 (001.039)</ta>
            <ta e="T487" id="Seg_3095" s="T477">UkOA_2010_Festival_nar.UkOA.039 (001.040)</ta>
            <ta e="T505" id="Seg_3096" s="T487">UkOA_2010_Festival_nar.UkOA.040 (001.041)</ta>
            <ta e="T525" id="Seg_3097" s="T505">UkOA_2010_Festival_nar.UkOA.041 (001.042)</ta>
            <ta e="T546" id="Seg_3098" s="T525">UkOA_2010_Festival_nar.UkOA.042 (001.043)</ta>
            <ta e="T556" id="Seg_3099" s="T546">UkOA_2010_Festival_nar.UkOA.043 (001.044)</ta>
            <ta e="T570" id="Seg_3100" s="T556">UkOA_2010_Festival_nar.UkOA.044 (001.045)</ta>
         </annotation>
         <annotation name="st" tierref="st-UkOA">
            <ta e="T4" id="Seg_3101" s="T1">Олег: Һакалыы кэпсиэм дуо?</ta>
            <ta e="T24" id="Seg_3102" s="T8">Һүрбэ иккилээк июньӈа икки һүс уоннаак дьылга Ханты-Мансийскай городка буолбута фестиваль "Северное сияние" диэн ааттаак.</ta>
            <ta e="T30" id="Seg_3103" s="T24">Онно Таймыртан биһиги барбыппыт икки киһи.</ta>
            <ta e="T47" id="Seg_3104" s="T30">Биргэс буоллага Владимир Энович Сигуней Народнай Артист России диэн киһи ааттаак заслуженный артист России и мин Наскуоттан.</ta>
            <ta e="T54" id="Seg_3105" s="T47">Онно бэрт үчүгэйдик биһигитин тоһуйдулар, туустаак килибинэн.</ta>
            <ta e="T64" id="Seg_3106" s="T54">Улакан багайы гостинацага диэлэтилэр аата буолла "Һэттэ гирбэй" - семь холмов.</ta>
            <ta e="T74" id="Seg_3107" s="T64">Бэрт үчүгэдик биһигини аһаталлар онно, аһаппыттара, утуппуттара, онтон һарсыӈӈытын репетициялаабыппыт.</ta>
            <ta e="T80" id="Seg_3108" s="T74">Һол киэһэтин, һол киэһэтин же выступайдаабыппыт.</ta>
            <ta e="T87" id="Seg_3109" s="T80">Ол кэнниттэн төгүрүк остол буолта иккис күнүгэр, </ta>
            <ta e="T96" id="Seg_3110" s="T87">ол төгүрүк остолго буолла эӈин ээ элбэк киһи комуллубута, </ta>
            <ta e="T104" id="Seg_3111" s="T96">үөрүнү куоһаганы ээ олок туһунан кэпсэтии баар этэ, </ta>
            <ta e="T127" id="Seg_3112" s="T104">оголор үөрэнэллэрин тустарынан студенттар ээ ити улакан высший учебный заведениялар тустарыгар того бырагалларын или того үөрэммэттэрин ол туһугар улакан отто вопрос турар этэ.</ta>
            <ta e="T138" id="Seg_3113" s="T127">Онтон буолла (эмэ- ) эмээксин выступайдаабыта онно Малданова диэн Мария Кузьминична.</ta>
            <ta e="T142" id="Seg_3114" s="T138">Өйдөөк дьактар, ол һаӈарбыта: </ta>
            <ta e="T156" id="Seg_3115" s="T142">ээ ого киһилэр ээ ого киһилэр бэйэлэрин кэргэннэригэр бэйэлэрин кэргэннэрин истэригэр истэриттэн воспитания буолардак.</ta>
            <ta e="T163" id="Seg_3116" s="T156">Төрөппүт киһи биэрэрдээк үөрэги үтүөнү куһуоганы огого.</ta>
            <ta e="T181" id="Seg_3117" s="T163">Аа оскуола, садик онуга буолла учебный заведениялар онуга көмө эрэ буолуоктарын наада они основной базы көрдүк буолуоктарын.</ta>
            <ta e="T198" id="Seg_3118" s="T181">Ол иһин буолла аны инни диэк ээ улатыннарыакка наада диир ээ ол дьактар эппиэти төрөтөр киһилэртэр родителляртан.</ta>
            <ta e="T213" id="Seg_3119" s="T198">Да и үтүөнү элбэттиннэриэккэ диэн һаӈарбыта ол дьактар, үтүөнү элбэтиннэриэккэ аа куһуоганы агыйатыннарыакка бу һиргэ.</ta>
            <ta e="T234" id="Seg_3120" s="T213">Вот колоон көрдөккө (если) энигин куһуоганнык һаӈардылар, куһуоганы гыннылар дуо, эн буо кэпсээмиэккин наада ол киһи туһунан, диир ол дьактар.</ta>
            <ta e="T251" id="Seg_3121" s="T234">Ол киһи туһу.. ол киһи туһунан бэйэтигэр һаӈарымыаккын наада но киһилэргэ эмиэ кэпсэмиэккэ наада чтоб куһуоган улатымыэгын.</ta>
            <ta e="T264" id="Seg_3122" s="T251">А үчүгэйдик гыммыт киһиэккэ буолла ол киһиэкэ можешь эмиэ бэйэтигэр һаӈарымыаккын чтоб киһиргэмиэӈин.</ta>
            <ta e="T283" id="Seg_3123" s="T264">А атын киһилэргэ кэпсиэккин наада что ол киһи үчүгэйи гыммыта диэн, ол киһи оннук диэн, ол киһи итинник диэн.</ta>
            <ta e="T288" id="Seg_3124" s="T284">Оччого буолла үтүө улаатар.</ta>
            <ta e="T301" id="Seg_3125" s="T288">Үтүө улаатар, оннук формуланнан диэн, но эмиэ таки ол формулага мин һагластаммаппын.</ta>
            <ta e="T303" id="Seg_3126" s="T301">Того диэӈӈит? </ta>
            <ta e="T312" id="Seg_3127" s="T303">куһуоган иккис формула баар ол үрдүтүгэр оччого куһаган улатымыагын.</ta>
            <ta e="T335" id="Seg_3128" s="T312">Куһаганы, инни диэк төттөрү улакан эппиэт биэриэккэ наада гиниэкэ, чтоб куһаганы гыммыт киһи билиэгин, что мээнэ киһини гиниэнэ куһугана мээнэ аһыа һуога гиниэкэ </ta>
            <ta e="T352" id="Seg_3129" s="T335">гини буолла киһи иннитигэр эппиэттирдээк чтоб инни диэк куһаганы гыныммыагын, оччого буолла үтүө икките раз улаатан таксыага.</ta>
            <ta e="T369" id="Seg_3130" s="T352">И олорооччу киһилэр онного олороччу киһилэр бэйэлэрин инни диэк, бэйэлэрин инни диэк атагастатыактара һуоктара да атагастатыактара һуоктара.</ta>
            <ta e="T392" id="Seg_3131" s="T369">Аа но онтон ол, ол круглый төгүрүк остол бүппүтүн кэннэ эмиэ концерт гала концерт диэн буоллта онно бэрт үчүгэйдик выступайдаабыттара элбэк коллектив кэлбитэ.</ta>
            <ta e="T403" id="Seg_3132" s="T392">Ээ Ханты-Мансийскайтан бэйэтиттэн театральнай группа баар этэ биһиэнэ ансамбль хэйробут көрдүк </ta>
            <ta e="T416" id="Seg_3133" s="T403">онтон буолла Магадантан кэлбиттэрэ ээ Ямало-Ненецкий автономнуй округтан, Чукоткаттан анасамбль баар этэ, ыраак һиртэн кэлитэлээбиттэр.</ta>
            <ta e="T421" id="Seg_3134" s="T416">Онтон элбэк-элбэк һиртэн кэлитэлэбиттэр онно</ta>
            <ta e="T427" id="Seg_3135" s="T421">һүрбэ иккки коллектив баар этэ, ээ.</ta>
            <ta e="T457" id="Seg_3136" s="T427">Онтон куратордарбыт үчүгэй багайи дьактаттар этилэрэ биһигитин барытын ээ иллэннэр, эгэлэннэр, куччугуй оголуу утутаннар, онтон аһатаннар, онтон музейдар үстүн теплоходка һылдьыбыппыт "Иртыш" диэн ээ оо үрэк, "Иртыш" диэн үрэк үстүн барбыппыт.</ta>
            <ta e="T469" id="Seg_3137" s="T457">Ол Иртыш буолла улакан үрэккэ кыттар "Обь" диэн үрэккэ ол кыттыбыттарга бэлэктэммиппит.</ta>
            <ta e="T477" id="Seg_3138" s="T469">Ылары ырыалары ыллаабыппыт кэлээрибит бараарыбыт бэсэслээ багайи этэ.</ta>
            <ta e="T487" id="Seg_3139" s="T477">Кинога уһулбуттара биһигитин онтон, онтон төттөрү кэлэммит ээ ол һарсыӈӈытын.</ta>
            <ta e="T505" id="Seg_3140" s="T487">Ол үс күнү буолбуппут, ээ үс күнү буолааччы, ээ үс күнү буолбуппутугар Ханты-Мансийскайга үчүгэй багайи күннэр этилэрэ, каллаан.</ta>
            <ta e="T525" id="Seg_3141" s="T505">Ол иннитигэр һамыырдыр этэ диир этилэр онтон һүрбэттэн такса градус этэ һүрбэ түөрт-һүрбэ биэс градус буолан истэ ити күттэргэ </ta>
            <ta e="T546" id="Seg_3142" s="T525">ол иһин буолла биһиэкэ олуста ититэ һуок этэ но олуста и тымныыта һуок этэ, ол иһин бэрт үчүгэй биһиэкэ.</ta>
            <ta e="T556" id="Seg_3143" s="T546">Мин һанаабар итинник климат гинилэргэ итинник каллаан бэрт үчүгэйдик туксар.</ta>
            <ta e="T570" id="Seg_3144" s="T556">Ол иһин өссүө да ыгырдыктарына мин һанаабар мин буо барар көрдүкпүн өссүө бирдэ, ыаллана.</ta>
         </annotation>
         <annotation name="ts" tierref="ts-UkOA">
            <ta e="T4" id="Seg_3145" s="T1">– Hakalɨː kepsi͡em du͡o? </ta>
            <ta e="T24" id="Seg_3146" s="T8">– Hürbe ikkileːk ijunʼŋa ikki hüːs u͡onnaːk dʼɨlga Xantɨ-Mansijskaj gorakka bu͡olbuta fʼestʼival "Sʼevʼernaje sʼijanʼije" di͡en aːttaːk. </ta>
            <ta e="T30" id="Seg_3147" s="T24">Onno Tajmɨːrtan bihigi barbɨppɨt ikki kihi. </ta>
            <ta e="T47" id="Seg_3148" s="T30">Birges bu͡olla Vladʼimʼir Eːnovʼičʼ Sʼigunʼej, "Naroːdnaj Artʼist Rassʼii" di͡en kihi aːttaːk "Zaslužennɨj artʼist Rassʼii" i min Nasku͡ottan. </ta>
            <ta e="T54" id="Seg_3149" s="T47">Onno bert üčügejdik bihigitin tohujdular, tuːstaːk kili͡ebinen. </ta>
            <ta e="T64" id="Seg_3150" s="T54">Ulakan bagaj gastʼinʼisaga dʼi͡eleːtiler, aːta bu͡olla "Hette girbej", "Semʼ xalmov". </ta>
            <ta e="T74" id="Seg_3151" s="T64">Bert üčügejdik bihigini ahatallar onno, ahappɨttara, utupputtara, onton harsɨŋŋɨtɨn rʼepʼetʼicɨjalaːppɨt. </ta>
            <ta e="T80" id="Seg_3152" s="T74">Hol ki͡ehetin, hol ki͡ehetin že vɨstupajdaːppɨt. </ta>
            <ta e="T87" id="Seg_3153" s="T80">Ol kennitten tögürük ostol bu͡olta ikkis künüger. </ta>
            <ta e="T96" id="Seg_3154" s="T87">Ol tögürük ostolgo bu͡olla eŋin eː elbek kihi komullubuta. </ta>
            <ta e="T104" id="Seg_3155" s="T96">Ü͡örüːnü kuhaganɨ eː olok tuhunan kepsetiː baːr ete. </ta>
            <ta e="T127" id="Seg_3156" s="T104">Ogolor ü͡örenellerin tustarɨnan studʼenttar eː iti ulakan vɨššɨj učʼebnɨj zavʼedʼenʼijalar tustarɨgar togo bɨragallarɨn ili togo ü͡öremmetterin ol ol tuhugar eː ulakan onton vapros turar ete. </ta>
            <ta e="T138" id="Seg_3157" s="T127">Onton bu͡olla (eme-) eː emeːksin vɨstupajdaːbɨt onno, Maldaːnova di͡en Marʼija Kuzʼmʼinʼičʼna. </ta>
            <ta e="T142" id="Seg_3158" s="T138">Öjdöːk dʼaktar, ol haŋarbɨta: </ta>
            <ta e="T156" id="Seg_3159" s="T142">Eː ogo kihiler eː ogo kihiler bejelerin kergenneriger bejelerin kergennerin isteriger isteritten vaspʼitanʼija bu͡olardaːk. </ta>
            <ta e="T163" id="Seg_3160" s="T156">Töröppüt kihi bi͡ererdeːk ü͡öregi ütü͡önü kuhaganɨ ogogo. </ta>
            <ta e="T181" id="Seg_3161" s="T163">Aː usku͡ola, sadʼik onuga bu͡olla eː učʼebnɨj zavʼedʼenʼijalar onuga kömö ere bu͡olu͡oktarɨn naːda, a anʼi asnavnoj bazɨ kördük bu͡olu͡oktarɨn. </ta>
            <ta e="T198" id="Seg_3162" s="T181">Ol ihin bu͡olla anɨ inni di͡ek eː ulaːtɨnnarɨ͡akka naːda diːr eː ol dʼaktar eppi͡eti törötör kihilerten radʼitelʼlartan. </ta>
            <ta e="T213" id="Seg_3163" s="T198">Da i ütü͡önü elbeːtinneri͡ekke di͡en haŋarbɨta ol dʼaktar, ütü͡önü elbeːtinneri͡ekke a kuhaganɨ agɨjaːtɨnnarɨ͡akka bu hirge. </ta>
            <ta e="T234" id="Seg_3164" s="T213">Vot, nu, koloːn kördökkö (jeslʼi) enigin kuhagannɨk haŋardɨlar, kuhaganɨ gɨnnɨlar du͡o, en bu͡o kepseːmi͡ekkin naːda ol kihi tuhunan, diːr ol dʼaktar. </ta>
            <ta e="T251" id="Seg_3165" s="T234">Ol kihi (tuhu-) ol kihi tuhunan bejetiger haŋarɨmɨ͡akkɨn naːda, no kihilerge emi͡e kepseːmi͡ekke naːda štob kuhagan ulaːtɨmi͡egin. </ta>
            <ta e="T264" id="Seg_3166" s="T251">A üčügejdik gɨmmɨt kihi͡eke bu͡olla ol kihi͡eke možešʼ emi͡e bejetiger haŋarɨmɨ͡akkɨn štob kihirgeːmi͡egin. </ta>
            <ta e="T283" id="Seg_3167" s="T264">A atɨn kihilerge kepsi͡ekkin naːda što ol kihi üčügeji gɨmmɨta di͡en, ol kihi onnuk di͡en, ol kihi itinnik di͡en. </ta>
            <ta e="T288" id="Seg_3168" s="T284">Oččogo bu͡olla ütü͡ö ulaːtar. </ta>
            <ta e="T301" id="Seg_3169" s="T288">Ütü͡ö ulaːtar, onnuk formulannan di͡en, no emi͡e tak i ol formulaga min haglastammappɨn. </ta>
            <ta e="T303" id="Seg_3170" s="T301">Togo di͡eŋŋit? </ta>
            <ta e="T312" id="Seg_3171" s="T303">Kuhagan ikkis formula baːr ol ürdütüger oččogo kuhagan ulaːtɨmɨ͡agɨn. </ta>
            <ta e="T335" id="Seg_3172" s="T312">Kuhaganɨ, inni di͡ek töttörü ulakanɨ eppi͡et bi͡eri͡ekke naːda gini͡eke, štob kuhaganɨ gɨmmɨt kihi bili͡egin, što meːne kihini gini͡ene kuhagana meːne ahɨ͡a hu͡oga gini͡eke. </ta>
            <ta e="T352" id="Seg_3173" s="T335">Gini bu͡olla kihi innitiger eppi͡ettiːrdeːk štob inni di͡ek kuhaganɨ gɨnɨmɨ͡agɨn, oččogo bu͡olla ütü͡ö ikkite raz ulaːtan taksɨ͡aga. </ta>
            <ta e="T369" id="Seg_3174" s="T352">I oloroːčču kihiler, onno oloroːčču kihiler bejelerin inni di͡ek, bejelerin inni di͡ek atagastatɨ͡aktara hu͡oktara da atagastatɨ͡aktara hu͡oktara. </ta>
            <ta e="T392" id="Seg_3175" s="T369">Aː no onton ol, ol kruglɨj tögürük ostol büppütün kenne emi͡e kancert, "gala - kancert" di͡en bu͡olta onno, bert üčügejdik vɨstupajdaːbɨttara, elbek kallʼektiv kelbite. </ta>
            <ta e="T403" id="Seg_3176" s="T392">Eː Xantɨ-Mansijskajtan bejetitten tʼeatralʼnaj gruppa baːr ete bihi͡ene ansamblʼ Xejrobut kördük. </ta>
            <ta e="T416" id="Seg_3177" s="T403">Onton bu͡olla Magadantan kelbittere, eː Jamala_Nʼenʼeckij avtonomnɨj oːkrugtan, Čʼukotkattan ansamblʼ baːr ete, ɨraːk hirten keliteleːbitter. </ta>
            <ta e="T421" id="Seg_3178" s="T416">Onton elbek-elbek hirten keliteleːbitter onno. </ta>
            <ta e="T427" id="Seg_3179" s="T421">Hürbe ikki kallʼektʼiv baːr ete, eː. </ta>
            <ta e="T457" id="Seg_3180" s="T427">Onton kuratardarbɨt üčügej bagajɨ dʼaktattar etilere bihigitin barɨtɨn eː illenner, egelenner, küččügüj ogoluː ututannar, onton ahatannar, onton muzʼejdar üstün tʼeplaxokka hɨldʼɨbɨppɨt "Irtɨš" di͡en eː ürek, "Irtɨš" di͡en ürek üstün barbɨppɨt. </ta>
            <ta e="T469" id="Seg_3181" s="T457">Ol Irtɨš bu͡olla ulakan ürekke kɨttar "Obʼ" di͡en ürekke ol kɨttɨbɨttarga belektemmippit. </ta>
            <ta e="T477" id="Seg_3182" s="T469">(Ɨlarɨ-) ɨrɨ͡alarɨ ɨllaːbɨppɨt keleːribit baraːrɨbɨt, beseleː bagaji ete. </ta>
            <ta e="T487" id="Seg_3183" s="T477">Kʼinoːga uhulbuttara bihigitin onton, onton töttörü kelemmit eː ol harsɨŋŋɨtɨn. </ta>
            <ta e="T505" id="Seg_3184" s="T487">Ol üs künü bu͡olbupput, eː üs künü bu͡olaːččɨ, eː üs künü bu͡olbupputugar Xantɨ-Mansijskajga üčügej bagaji künner etilere, kallaːn. </ta>
            <ta e="T525" id="Seg_3185" s="T505">Ol innitiger hamɨːrdɨːr ete, diːr etilere, onton hürbetten taksa gradus ete hürbe tü͡ört, hürbe bi͡es gradus bu͡olan iste iti kütterge. </ta>
            <ta e="T546" id="Seg_3186" s="T525">Ol ihin bu͡olla bihi͡eke olus da itiːte hu͡ok ete, no olus da i tɨmnɨːta hu͡ok ete, ol ihin bert üčügej bihi͡eke. </ta>
            <ta e="T556" id="Seg_3187" s="T546">Min hanaːbar itinnik klimat ginilerge itinnik kallaːn bert üčügejdik tuksar. </ta>
            <ta e="T570" id="Seg_3188" s="T556">Ol ihin össü͡ö da ɨgɨrdaktarɨna min hanaːbar min bu͡o barar kördükpün össü͡ö biːrde, ɨ͡allana. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-UkOA">
            <ta e="T2" id="Seg_3189" s="T1">haka-lɨː</ta>
            <ta e="T3" id="Seg_3190" s="T2">keps-i͡e-m</ta>
            <ta e="T4" id="Seg_3191" s="T3">du͡o</ta>
            <ta e="T10" id="Seg_3192" s="T8">hürbe</ta>
            <ta e="T11" id="Seg_3193" s="T10">ikki-leːk</ta>
            <ta e="T12" id="Seg_3194" s="T11">ijunʼ-ŋa</ta>
            <ta e="T13" id="Seg_3195" s="T12">ikki</ta>
            <ta e="T14" id="Seg_3196" s="T13">hüːs</ta>
            <ta e="T15" id="Seg_3197" s="T14">u͡on-naːk</ta>
            <ta e="T16" id="Seg_3198" s="T15">dʼɨl-ga</ta>
            <ta e="T17" id="Seg_3199" s="T16">Xantɨ_Mansijskaj</ta>
            <ta e="T18" id="Seg_3200" s="T17">gorak-ka</ta>
            <ta e="T19" id="Seg_3201" s="T18">bu͡ol-but-a</ta>
            <ta e="T20" id="Seg_3202" s="T19">fʼestʼival</ta>
            <ta e="T23" id="Seg_3203" s="T22">di͡e-n</ta>
            <ta e="T24" id="Seg_3204" s="T23">aːt-taːk</ta>
            <ta e="T25" id="Seg_3205" s="T24">onno</ta>
            <ta e="T26" id="Seg_3206" s="T25">Tajmɨːr-tan</ta>
            <ta e="T27" id="Seg_3207" s="T26">bihigi</ta>
            <ta e="T28" id="Seg_3208" s="T27">bar-bɨp-pɨt</ta>
            <ta e="T29" id="Seg_3209" s="T28">ikki</ta>
            <ta e="T30" id="Seg_3210" s="T29">kihi</ta>
            <ta e="T31" id="Seg_3211" s="T30">birges</ta>
            <ta e="T32" id="Seg_3212" s="T31">bu͡ol-l-a</ta>
            <ta e="T33" id="Seg_3213" s="T32">Vladʼimʼir</ta>
            <ta e="T34" id="Seg_3214" s="T33">Eːnovʼičʼ</ta>
            <ta e="T35" id="Seg_3215" s="T34">Sʼigunʼej</ta>
            <ta e="T39" id="Seg_3216" s="T38">di͡e-n</ta>
            <ta e="T40" id="Seg_3217" s="T39">kihi</ta>
            <ta e="T41" id="Seg_3218" s="T40">aːt-taːk</ta>
            <ta e="T45" id="Seg_3219" s="T44">i</ta>
            <ta e="T46" id="Seg_3220" s="T45">min</ta>
            <ta e="T47" id="Seg_3221" s="T46">Nasku͡o-ttan</ta>
            <ta e="T48" id="Seg_3222" s="T47">onno</ta>
            <ta e="T49" id="Seg_3223" s="T48">bert</ta>
            <ta e="T50" id="Seg_3224" s="T49">üčügej-dik</ta>
            <ta e="T51" id="Seg_3225" s="T50">bihigiti-n</ta>
            <ta e="T52" id="Seg_3226" s="T51">tohuj-du-lar</ta>
            <ta e="T53" id="Seg_3227" s="T52">tuːs-taːk</ta>
            <ta e="T54" id="Seg_3228" s="T53">kili͡eb-i-nen</ta>
            <ta e="T55" id="Seg_3229" s="T54">ulakan</ta>
            <ta e="T56" id="Seg_3230" s="T55">bagaj</ta>
            <ta e="T57" id="Seg_3231" s="T56">gastʼinʼisa-ga</ta>
            <ta e="T58" id="Seg_3232" s="T57">dʼi͡e-leː-ti-ler</ta>
            <ta e="T59" id="Seg_3233" s="T58">aːt-a</ta>
            <ta e="T60" id="Seg_3234" s="T59">bu͡ol-l-a</ta>
            <ta e="T61" id="Seg_3235" s="T60">hette</ta>
            <ta e="T62" id="Seg_3236" s="T61">girbej</ta>
            <ta e="T65" id="Seg_3237" s="T64">bert</ta>
            <ta e="T66" id="Seg_3238" s="T65">üčügej-dik</ta>
            <ta e="T67" id="Seg_3239" s="T66">bihigi-ni</ta>
            <ta e="T68" id="Seg_3240" s="T67">ah-a-t-al-lar</ta>
            <ta e="T69" id="Seg_3241" s="T68">onno</ta>
            <ta e="T70" id="Seg_3242" s="T69">ah-a-p-pɨt-tara</ta>
            <ta e="T71" id="Seg_3243" s="T70">utu-p-put-tara</ta>
            <ta e="T72" id="Seg_3244" s="T71">onton</ta>
            <ta e="T73" id="Seg_3245" s="T72">harsɨŋŋɨ-tɨ-n</ta>
            <ta e="T74" id="Seg_3246" s="T73">rʼepʼetʼicɨja-laː-p-pɨt</ta>
            <ta e="T75" id="Seg_3247" s="T74">hol</ta>
            <ta e="T76" id="Seg_3248" s="T75">ki͡ehe-ti-n</ta>
            <ta e="T77" id="Seg_3249" s="T76">hol</ta>
            <ta e="T78" id="Seg_3250" s="T77">ki͡ehe-ti-n</ta>
            <ta e="T79" id="Seg_3251" s="T78">že</ta>
            <ta e="T80" id="Seg_3252" s="T79">vɨstupaj-daː-p-pɨt</ta>
            <ta e="T81" id="Seg_3253" s="T80">ol</ta>
            <ta e="T82" id="Seg_3254" s="T81">kenn-i-tten</ta>
            <ta e="T83" id="Seg_3255" s="T82">tögürük</ta>
            <ta e="T84" id="Seg_3256" s="T83">ostol</ta>
            <ta e="T85" id="Seg_3257" s="T84">bu͡ol-t-a</ta>
            <ta e="T86" id="Seg_3258" s="T85">ikki-s</ta>
            <ta e="T87" id="Seg_3259" s="T86">kün-ü-ger</ta>
            <ta e="T88" id="Seg_3260" s="T87">ol</ta>
            <ta e="T89" id="Seg_3261" s="T88">tögürük</ta>
            <ta e="T90" id="Seg_3262" s="T89">ostol-go</ta>
            <ta e="T91" id="Seg_3263" s="T90">bu͡olla</ta>
            <ta e="T92" id="Seg_3264" s="T91">eŋin</ta>
            <ta e="T93" id="Seg_3265" s="T92">eː</ta>
            <ta e="T94" id="Seg_3266" s="T93">elbek</ta>
            <ta e="T95" id="Seg_3267" s="T94">kihi</ta>
            <ta e="T96" id="Seg_3268" s="T95">komu-ll-u-but-a</ta>
            <ta e="T97" id="Seg_3269" s="T96">ü͡ör-üː-nü</ta>
            <ta e="T98" id="Seg_3270" s="T97">kuhagan-ɨ</ta>
            <ta e="T99" id="Seg_3271" s="T98">eː</ta>
            <ta e="T100" id="Seg_3272" s="T99">olok</ta>
            <ta e="T101" id="Seg_3273" s="T100">tuh-u-nan</ta>
            <ta e="T102" id="Seg_3274" s="T101">kepset-iː</ta>
            <ta e="T103" id="Seg_3275" s="T102">baːr</ta>
            <ta e="T104" id="Seg_3276" s="T103">e-t-e</ta>
            <ta e="T105" id="Seg_3277" s="T104">ogo-lor</ta>
            <ta e="T106" id="Seg_3278" s="T105">ü͡ören-el-leri-n</ta>
            <ta e="T107" id="Seg_3279" s="T106">tus-tarɨ-nan</ta>
            <ta e="T108" id="Seg_3280" s="T107">studʼent-tar</ta>
            <ta e="T109" id="Seg_3281" s="T108">eː</ta>
            <ta e="T110" id="Seg_3282" s="T109">iti</ta>
            <ta e="T111" id="Seg_3283" s="T110">ulakan</ta>
            <ta e="T112" id="Seg_3284" s="T111">vɨššɨj učʼebnɨj zavʼedʼenʼija-lar</ta>
            <ta e="T113" id="Seg_3285" s="T112">tus-tarɨ-gar</ta>
            <ta e="T114" id="Seg_3286" s="T113">togo</ta>
            <ta e="T115" id="Seg_3287" s="T114">bɨrag-al-larɨ-n</ta>
            <ta e="T116" id="Seg_3288" s="T115">ili</ta>
            <ta e="T117" id="Seg_3289" s="T116">togo</ta>
            <ta e="T118" id="Seg_3290" s="T117">ü͡örem-met-teri-n</ta>
            <ta e="T119" id="Seg_3291" s="T118">ol</ta>
            <ta e="T120" id="Seg_3292" s="T119">ol</ta>
            <ta e="T121" id="Seg_3293" s="T120">tuh-u-gar</ta>
            <ta e="T122" id="Seg_3294" s="T121">eː</ta>
            <ta e="T123" id="Seg_3295" s="T122">ulakan</ta>
            <ta e="T124" id="Seg_3296" s="T123">onton</ta>
            <ta e="T125" id="Seg_3297" s="T124">vapros</ta>
            <ta e="T126" id="Seg_3298" s="T125">tur-ar</ta>
            <ta e="T127" id="Seg_3299" s="T126">e-t-e</ta>
            <ta e="T128" id="Seg_3300" s="T127">onton</ta>
            <ta e="T129" id="Seg_3301" s="T128">bu͡olla</ta>
            <ta e="T131" id="Seg_3302" s="T130">eː</ta>
            <ta e="T132" id="Seg_3303" s="T131">emeːksin</ta>
            <ta e="T133" id="Seg_3304" s="T132">vɨstupaj-daː-bɨt</ta>
            <ta e="T134" id="Seg_3305" s="T133">onno</ta>
            <ta e="T135" id="Seg_3306" s="T134">Maldaːnova</ta>
            <ta e="T136" id="Seg_3307" s="T135">di͡e-n</ta>
            <ta e="T137" id="Seg_3308" s="T136">Marʼija</ta>
            <ta e="T138" id="Seg_3309" s="T137">Kuzʼmʼinʼičʼna</ta>
            <ta e="T139" id="Seg_3310" s="T138">öj-döːk</ta>
            <ta e="T140" id="Seg_3311" s="T139">dʼaktar</ta>
            <ta e="T141" id="Seg_3312" s="T140">ol</ta>
            <ta e="T142" id="Seg_3313" s="T141">haŋar-bɨt-a</ta>
            <ta e="T143" id="Seg_3314" s="T142">eː</ta>
            <ta e="T144" id="Seg_3315" s="T143">ogo</ta>
            <ta e="T145" id="Seg_3316" s="T144">kihi-ler</ta>
            <ta e="T146" id="Seg_3317" s="T145">eː</ta>
            <ta e="T147" id="Seg_3318" s="T146">ogo</ta>
            <ta e="T148" id="Seg_3319" s="T147">kihi-ler</ta>
            <ta e="T149" id="Seg_3320" s="T148">beje-leri-n</ta>
            <ta e="T150" id="Seg_3321" s="T149">kergen-neri-ger</ta>
            <ta e="T151" id="Seg_3322" s="T150">beje-leri-n</ta>
            <ta e="T152" id="Seg_3323" s="T151">kergen-neri-n</ta>
            <ta e="T153" id="Seg_3324" s="T152">is-teri-ger</ta>
            <ta e="T154" id="Seg_3325" s="T153">is-teri-tten</ta>
            <ta e="T155" id="Seg_3326" s="T154">vaspʼitanʼija</ta>
            <ta e="T156" id="Seg_3327" s="T155">bu͡ol-ar-daːk</ta>
            <ta e="T157" id="Seg_3328" s="T156">tör-ö-p-püt</ta>
            <ta e="T158" id="Seg_3329" s="T157">kihi</ta>
            <ta e="T159" id="Seg_3330" s="T158">bi͡er-er-deːk</ta>
            <ta e="T160" id="Seg_3331" s="T159">ü͡öreg-i</ta>
            <ta e="T161" id="Seg_3332" s="T160">ütü͡ö-nü</ta>
            <ta e="T162" id="Seg_3333" s="T161">kuhagan-ɨ</ta>
            <ta e="T163" id="Seg_3334" s="T162">ogo-go</ta>
            <ta e="T164" id="Seg_3335" s="T163">aː</ta>
            <ta e="T165" id="Seg_3336" s="T164">usku͡ola</ta>
            <ta e="T166" id="Seg_3337" s="T165">sadʼik</ta>
            <ta e="T167" id="Seg_3338" s="T166">onu-ga</ta>
            <ta e="T168" id="Seg_3339" s="T167">bu͡olla</ta>
            <ta e="T169" id="Seg_3340" s="T168">eː</ta>
            <ta e="T170" id="Seg_3341" s="T169">učʼebnɨj zavʼedʼenʼija-lar</ta>
            <ta e="T171" id="Seg_3342" s="T170">onu-ga</ta>
            <ta e="T172" id="Seg_3343" s="T171">kömö</ta>
            <ta e="T173" id="Seg_3344" s="T172">ere</ta>
            <ta e="T174" id="Seg_3345" s="T173">bu͡ol-u͡ok-tarɨ-n</ta>
            <ta e="T175" id="Seg_3346" s="T174">naːda</ta>
            <ta e="T176" id="Seg_3347" s="T175">a</ta>
            <ta e="T180" id="Seg_3348" s="T179">kördük</ta>
            <ta e="T181" id="Seg_3349" s="T180">bu͡ol-u͡ok-tarɨ-n</ta>
            <ta e="T182" id="Seg_3350" s="T181">ol</ta>
            <ta e="T183" id="Seg_3351" s="T182">ihin</ta>
            <ta e="T184" id="Seg_3352" s="T183">bu͡olla</ta>
            <ta e="T185" id="Seg_3353" s="T184">anɨ</ta>
            <ta e="T186" id="Seg_3354" s="T185">inni</ta>
            <ta e="T187" id="Seg_3355" s="T186">di͡ek</ta>
            <ta e="T188" id="Seg_3356" s="T187">eː</ta>
            <ta e="T189" id="Seg_3357" s="T188">ulaːt-ɨnnar-ɨ͡ak-ka</ta>
            <ta e="T190" id="Seg_3358" s="T189">naːda</ta>
            <ta e="T191" id="Seg_3359" s="T190">diː-r</ta>
            <ta e="T192" id="Seg_3360" s="T191">eː</ta>
            <ta e="T193" id="Seg_3361" s="T192">ol</ta>
            <ta e="T194" id="Seg_3362" s="T193">dʼaktar</ta>
            <ta e="T195" id="Seg_3363" s="T194">eppi͡et-i</ta>
            <ta e="T196" id="Seg_3364" s="T195">tör-ö-t-ör</ta>
            <ta e="T197" id="Seg_3365" s="T196">kihi-ler-ten</ta>
            <ta e="T198" id="Seg_3366" s="T197">radʼitelʼ-lar-tan</ta>
            <ta e="T199" id="Seg_3367" s="T198">da</ta>
            <ta e="T200" id="Seg_3368" s="T199">i</ta>
            <ta e="T201" id="Seg_3369" s="T200">ütü͡ö-nü</ta>
            <ta e="T202" id="Seg_3370" s="T201">elbeː-t-inner-i͡ek-ke</ta>
            <ta e="T203" id="Seg_3371" s="T202">di͡e-n</ta>
            <ta e="T204" id="Seg_3372" s="T203">haŋar-bɨt-a</ta>
            <ta e="T205" id="Seg_3373" s="T204">ol</ta>
            <ta e="T206" id="Seg_3374" s="T205">dʼaktar</ta>
            <ta e="T207" id="Seg_3375" s="T206">ütü͡ö-nü</ta>
            <ta e="T208" id="Seg_3376" s="T207">elbeː-t-inner-i͡ek-ke</ta>
            <ta e="T209" id="Seg_3377" s="T208">a</ta>
            <ta e="T210" id="Seg_3378" s="T209">kuhagan-ɨ</ta>
            <ta e="T211" id="Seg_3379" s="T210">agɨjaː-t-ɨnnar-ɨ͡ak-ka</ta>
            <ta e="T212" id="Seg_3380" s="T211">bu</ta>
            <ta e="T213" id="Seg_3381" s="T212">hir-ge</ta>
            <ta e="T214" id="Seg_3382" s="T213">vot</ta>
            <ta e="T215" id="Seg_3383" s="T214">nu</ta>
            <ta e="T216" id="Seg_3384" s="T215">koloː-n</ta>
            <ta e="T217" id="Seg_3385" s="T216">kör-dök-kö</ta>
            <ta e="T218" id="Seg_3386" s="T217">jeslʼi</ta>
            <ta e="T219" id="Seg_3387" s="T218">enigi-n</ta>
            <ta e="T220" id="Seg_3388" s="T219">kuhagan-nɨk</ta>
            <ta e="T221" id="Seg_3389" s="T220">haŋar-dɨ-lar</ta>
            <ta e="T222" id="Seg_3390" s="T221">kuhagan-ɨ</ta>
            <ta e="T223" id="Seg_3391" s="T222">gɨn-nɨ-lar</ta>
            <ta e="T224" id="Seg_3392" s="T223">du͡o</ta>
            <ta e="T225" id="Seg_3393" s="T224">en</ta>
            <ta e="T226" id="Seg_3394" s="T225">bu͡o</ta>
            <ta e="T227" id="Seg_3395" s="T226">kepseː-m-i͡ek-ki-n</ta>
            <ta e="T228" id="Seg_3396" s="T227">naːda</ta>
            <ta e="T229" id="Seg_3397" s="T228">ol</ta>
            <ta e="T230" id="Seg_3398" s="T229">kihi</ta>
            <ta e="T231" id="Seg_3399" s="T230">tuh-u-nan</ta>
            <ta e="T232" id="Seg_3400" s="T231">diː-r</ta>
            <ta e="T233" id="Seg_3401" s="T232">ol</ta>
            <ta e="T234" id="Seg_3402" s="T233">dʼaktar</ta>
            <ta e="T235" id="Seg_3403" s="T234">ol</ta>
            <ta e="T236" id="Seg_3404" s="T235">kihi</ta>
            <ta e="T237" id="Seg_3405" s="T236">tuh-u</ta>
            <ta e="T238" id="Seg_3406" s="T237">ol</ta>
            <ta e="T239" id="Seg_3407" s="T238">kihi</ta>
            <ta e="T240" id="Seg_3408" s="T239">tuh-u-nan</ta>
            <ta e="T241" id="Seg_3409" s="T240">beje-ti-ger</ta>
            <ta e="T242" id="Seg_3410" s="T241">haŋar-ɨ-m-ɨ͡ak-kɨ-n</ta>
            <ta e="T243" id="Seg_3411" s="T242">naːda</ta>
            <ta e="T244" id="Seg_3412" s="T243">no</ta>
            <ta e="T245" id="Seg_3413" s="T244">kihi-ler-ge</ta>
            <ta e="T246" id="Seg_3414" s="T245">emi͡e</ta>
            <ta e="T247" id="Seg_3415" s="T246">kepseː-m-i͡ek-ke</ta>
            <ta e="T248" id="Seg_3416" s="T247">naːda</ta>
            <ta e="T249" id="Seg_3417" s="T248">štob</ta>
            <ta e="T250" id="Seg_3418" s="T249">kuhagan</ta>
            <ta e="T251" id="Seg_3419" s="T250">ulaːt-ɨ-m-i͡eg-i-n</ta>
            <ta e="T252" id="Seg_3420" s="T251">a</ta>
            <ta e="T253" id="Seg_3421" s="T252">üčügej-dik</ta>
            <ta e="T254" id="Seg_3422" s="T253">gɨm-mɨt</ta>
            <ta e="T255" id="Seg_3423" s="T254">kihi͡e-ke</ta>
            <ta e="T256" id="Seg_3424" s="T255">bu͡olla</ta>
            <ta e="T257" id="Seg_3425" s="T256">ol</ta>
            <ta e="T258" id="Seg_3426" s="T257">kihi͡e-ke</ta>
            <ta e="T260" id="Seg_3427" s="T259">emi͡e</ta>
            <ta e="T261" id="Seg_3428" s="T260">beje-ti-ger</ta>
            <ta e="T262" id="Seg_3429" s="T261">haŋar-ɨ-m-ɨ͡ak-kɨ-n</ta>
            <ta e="T263" id="Seg_3430" s="T262">štob</ta>
            <ta e="T264" id="Seg_3431" s="T263">kihirgeː-m-i͡eg-i-n</ta>
            <ta e="T265" id="Seg_3432" s="T264">a</ta>
            <ta e="T266" id="Seg_3433" s="T265">atɨn</ta>
            <ta e="T267" id="Seg_3434" s="T266">kihi-ler-ge</ta>
            <ta e="T268" id="Seg_3435" s="T267">keps-i͡ek-ki-n</ta>
            <ta e="T269" id="Seg_3436" s="T268">naːda</ta>
            <ta e="T270" id="Seg_3437" s="T269">što</ta>
            <ta e="T271" id="Seg_3438" s="T270">ol</ta>
            <ta e="T272" id="Seg_3439" s="T271">kihi</ta>
            <ta e="T273" id="Seg_3440" s="T272">üčügej-i</ta>
            <ta e="T274" id="Seg_3441" s="T273">gɨm-mɨt-a</ta>
            <ta e="T275" id="Seg_3442" s="T274">di͡e-n</ta>
            <ta e="T276" id="Seg_3443" s="T275">ol</ta>
            <ta e="T277" id="Seg_3444" s="T276">kihi</ta>
            <ta e="T278" id="Seg_3445" s="T277">onnuk</ta>
            <ta e="T279" id="Seg_3446" s="T278">di͡e-n</ta>
            <ta e="T280" id="Seg_3447" s="T279">ol</ta>
            <ta e="T281" id="Seg_3448" s="T280">kihi</ta>
            <ta e="T282" id="Seg_3449" s="T281">itinnik</ta>
            <ta e="T283" id="Seg_3450" s="T282">di͡e-n</ta>
            <ta e="T285" id="Seg_3451" s="T284">oččogo</ta>
            <ta e="T286" id="Seg_3452" s="T285">bu͡olla</ta>
            <ta e="T287" id="Seg_3453" s="T286">ütü͡ö</ta>
            <ta e="T288" id="Seg_3454" s="T287">ulaːt-ar</ta>
            <ta e="T289" id="Seg_3455" s="T288">ütü͡ö</ta>
            <ta e="T290" id="Seg_3456" s="T289">ulaːt-ar</ta>
            <ta e="T291" id="Seg_3457" s="T290">onnuk</ta>
            <ta e="T292" id="Seg_3458" s="T291">formula-nnan</ta>
            <ta e="T293" id="Seg_3459" s="T292">di͡e-n</ta>
            <ta e="T294" id="Seg_3460" s="T293">no</ta>
            <ta e="T295" id="Seg_3461" s="T294">emi͡e</ta>
            <ta e="T296" id="Seg_3462" s="T295">tak</ta>
            <ta e="T297" id="Seg_3463" s="T296">i</ta>
            <ta e="T298" id="Seg_3464" s="T297">ol</ta>
            <ta e="T299" id="Seg_3465" s="T298">formula-ga</ta>
            <ta e="T300" id="Seg_3466" s="T299">min</ta>
            <ta e="T301" id="Seg_3467" s="T300">haglas-tam-map-pɨn</ta>
            <ta e="T302" id="Seg_3468" s="T301">togo</ta>
            <ta e="T303" id="Seg_3469" s="T302">di͡e-ŋ-ŋit</ta>
            <ta e="T304" id="Seg_3470" s="T303">kuhagan</ta>
            <ta e="T305" id="Seg_3471" s="T304">ikki-s</ta>
            <ta e="T306" id="Seg_3472" s="T305">formula</ta>
            <ta e="T307" id="Seg_3473" s="T306">baːr</ta>
            <ta e="T308" id="Seg_3474" s="T307">ol</ta>
            <ta e="T309" id="Seg_3475" s="T308">ürdü-tü-ger</ta>
            <ta e="T310" id="Seg_3476" s="T309">oččogo</ta>
            <ta e="T311" id="Seg_3477" s="T310">kuhagan</ta>
            <ta e="T312" id="Seg_3478" s="T311">ulaːt-ɨ-m-ɨ͡ag-ɨ-n</ta>
            <ta e="T313" id="Seg_3479" s="T312">kuhagan-ɨ</ta>
            <ta e="T314" id="Seg_3480" s="T313">inni</ta>
            <ta e="T315" id="Seg_3481" s="T314">di͡ek</ta>
            <ta e="T316" id="Seg_3482" s="T315">töttörü</ta>
            <ta e="T317" id="Seg_3483" s="T316">ulakan-ɨ</ta>
            <ta e="T318" id="Seg_3484" s="T317">eppi͡et</ta>
            <ta e="T319" id="Seg_3485" s="T318">bi͡er-i͡ek-ke</ta>
            <ta e="T320" id="Seg_3486" s="T319">naːda</ta>
            <ta e="T321" id="Seg_3487" s="T320">gini͡e-ke</ta>
            <ta e="T322" id="Seg_3488" s="T321">štob</ta>
            <ta e="T323" id="Seg_3489" s="T322">kuhagan-ɨ</ta>
            <ta e="T324" id="Seg_3490" s="T323">gɨm-mɨt</ta>
            <ta e="T325" id="Seg_3491" s="T324">kihi</ta>
            <ta e="T326" id="Seg_3492" s="T325">bil-i͡eg-i-n</ta>
            <ta e="T327" id="Seg_3493" s="T326">što</ta>
            <ta e="T328" id="Seg_3494" s="T327">meːne</ta>
            <ta e="T329" id="Seg_3495" s="T328">kihi-ni</ta>
            <ta e="T330" id="Seg_3496" s="T329">gini͡ene</ta>
            <ta e="T331" id="Seg_3497" s="T330">kuhagan-a</ta>
            <ta e="T332" id="Seg_3498" s="T331">meːne</ta>
            <ta e="T333" id="Seg_3499" s="T332">ah-ɨ͡a</ta>
            <ta e="T334" id="Seg_3500" s="T333">hu͡og-a</ta>
            <ta e="T335" id="Seg_3501" s="T334">gini͡e-ke</ta>
            <ta e="T336" id="Seg_3502" s="T335">gini</ta>
            <ta e="T337" id="Seg_3503" s="T336">bu͡olla</ta>
            <ta e="T338" id="Seg_3504" s="T337">kihi</ta>
            <ta e="T339" id="Seg_3505" s="T338">inni-ti-ger</ta>
            <ta e="T340" id="Seg_3506" s="T339">eppi͡et-tiː-r-deːk</ta>
            <ta e="T341" id="Seg_3507" s="T340">štob</ta>
            <ta e="T342" id="Seg_3508" s="T341">inni</ta>
            <ta e="T343" id="Seg_3509" s="T342">di͡ek</ta>
            <ta e="T344" id="Seg_3510" s="T343">kuhagan-ɨ</ta>
            <ta e="T345" id="Seg_3511" s="T344">gɨn-ɨ-m-ɨ͡ag-ɨ-n</ta>
            <ta e="T346" id="Seg_3512" s="T345">oččogo</ta>
            <ta e="T347" id="Seg_3513" s="T346">bu͡olla</ta>
            <ta e="T348" id="Seg_3514" s="T347">ütü͡ö</ta>
            <ta e="T349" id="Seg_3515" s="T348">ikki-te</ta>
            <ta e="T350" id="Seg_3516" s="T349">raz</ta>
            <ta e="T351" id="Seg_3517" s="T350">ulaːt-an</ta>
            <ta e="T352" id="Seg_3518" s="T351">taks-ɨ͡ag-a</ta>
            <ta e="T353" id="Seg_3519" s="T352">i</ta>
            <ta e="T354" id="Seg_3520" s="T353">olor-oːčču</ta>
            <ta e="T355" id="Seg_3521" s="T354">kihi-ler</ta>
            <ta e="T356" id="Seg_3522" s="T355">onno</ta>
            <ta e="T357" id="Seg_3523" s="T356">olor-oːčču</ta>
            <ta e="T358" id="Seg_3524" s="T357">kihi-ler</ta>
            <ta e="T359" id="Seg_3525" s="T358">beje-leri-n</ta>
            <ta e="T360" id="Seg_3526" s="T359">inni</ta>
            <ta e="T361" id="Seg_3527" s="T360">di͡ek</ta>
            <ta e="T362" id="Seg_3528" s="T361">beje-leri-n</ta>
            <ta e="T363" id="Seg_3529" s="T362">inni</ta>
            <ta e="T364" id="Seg_3530" s="T363">di͡ek</ta>
            <ta e="T365" id="Seg_3531" s="T364">atagast-a-t-ɨ͡ak-tara</ta>
            <ta e="T366" id="Seg_3532" s="T365">hu͡ok-tara</ta>
            <ta e="T367" id="Seg_3533" s="T366">da</ta>
            <ta e="T368" id="Seg_3534" s="T367">atagast-a-t-ɨ͡ak-tara</ta>
            <ta e="T369" id="Seg_3535" s="T368">hu͡ok-tara</ta>
            <ta e="T370" id="Seg_3536" s="T369">aː</ta>
            <ta e="T371" id="Seg_3537" s="T370">no</ta>
            <ta e="T372" id="Seg_3538" s="T371">onton</ta>
            <ta e="T373" id="Seg_3539" s="T372">ol</ta>
            <ta e="T374" id="Seg_3540" s="T373">ol</ta>
            <ta e="T375" id="Seg_3541" s="T374">kruglɨj</ta>
            <ta e="T376" id="Seg_3542" s="T375">tögürük</ta>
            <ta e="T377" id="Seg_3543" s="T376">ostol</ta>
            <ta e="T378" id="Seg_3544" s="T377">büp-püt-ü-n</ta>
            <ta e="T379" id="Seg_3545" s="T378">kenne</ta>
            <ta e="T380" id="Seg_3546" s="T379">emi͡e</ta>
            <ta e="T381" id="Seg_3547" s="T380">kancert</ta>
            <ta e="T384" id="Seg_3548" s="T383">di͡e-n</ta>
            <ta e="T385" id="Seg_3549" s="T384">bu͡ol-t-a</ta>
            <ta e="T386" id="Seg_3550" s="T385">onno</ta>
            <ta e="T387" id="Seg_3551" s="T386">bert</ta>
            <ta e="T388" id="Seg_3552" s="T387">üčügej-dik</ta>
            <ta e="T389" id="Seg_3553" s="T388">vɨstupaj-daː-bɨt-tara</ta>
            <ta e="T390" id="Seg_3554" s="T389">elbek</ta>
            <ta e="T391" id="Seg_3555" s="T390">kallʼektiv</ta>
            <ta e="T392" id="Seg_3556" s="T391">kel-bit-e</ta>
            <ta e="T393" id="Seg_3557" s="T392">eː</ta>
            <ta e="T394" id="Seg_3558" s="T393">Xantɨ_Mansijskaj-tan</ta>
            <ta e="T395" id="Seg_3559" s="T394">beje-ti-tten</ta>
            <ta e="T396" id="Seg_3560" s="T395">tʼeatralʼnaj</ta>
            <ta e="T397" id="Seg_3561" s="T396">gruppa</ta>
            <ta e="T398" id="Seg_3562" s="T397">baːr</ta>
            <ta e="T399" id="Seg_3563" s="T398">e-t-e</ta>
            <ta e="T400" id="Seg_3564" s="T399">bihi͡ene</ta>
            <ta e="T401" id="Seg_3565" s="T400">ansamblʼ</ta>
            <ta e="T402" id="Seg_3566" s="T401">Xejro-but</ta>
            <ta e="T403" id="Seg_3567" s="T402">kördük</ta>
            <ta e="T404" id="Seg_3568" s="T403">onton</ta>
            <ta e="T405" id="Seg_3569" s="T404">bu͡olla</ta>
            <ta e="T406" id="Seg_3570" s="T405">Magadan-tan</ta>
            <ta e="T407" id="Seg_3571" s="T406">kel-bit-tere</ta>
            <ta e="T408" id="Seg_3572" s="T407">eː</ta>
            <ta e="T409" id="Seg_3573" s="T408">Jamala_Nʼenʼeckij avtonomnɨj oːkrug-tan</ta>
            <ta e="T410" id="Seg_3574" s="T409">Čʼukotka-ttan</ta>
            <ta e="T411" id="Seg_3575" s="T410">ansamblʼ</ta>
            <ta e="T412" id="Seg_3576" s="T411">baːr</ta>
            <ta e="T413" id="Seg_3577" s="T412">e-t-e</ta>
            <ta e="T414" id="Seg_3578" s="T413">ɨraːk</ta>
            <ta e="T415" id="Seg_3579" s="T414">hir-ten</ta>
            <ta e="T416" id="Seg_3580" s="T415">kel-iteleː-bit-ter</ta>
            <ta e="T417" id="Seg_3581" s="T416">onton</ta>
            <ta e="T418" id="Seg_3582" s="T417">elbek-elbek</ta>
            <ta e="T419" id="Seg_3583" s="T418">hir-ten</ta>
            <ta e="T420" id="Seg_3584" s="T419">kel-iteleː-bit-ter</ta>
            <ta e="T421" id="Seg_3585" s="T420">onno</ta>
            <ta e="T422" id="Seg_3586" s="T421">hürbe</ta>
            <ta e="T423" id="Seg_3587" s="T422">ikki</ta>
            <ta e="T424" id="Seg_3588" s="T423">kallʼektʼiv</ta>
            <ta e="T425" id="Seg_3589" s="T424">baːr</ta>
            <ta e="T426" id="Seg_3590" s="T425">e-t-e</ta>
            <ta e="T427" id="Seg_3591" s="T426">eː</ta>
            <ta e="T428" id="Seg_3592" s="T427">onton</ta>
            <ta e="T429" id="Seg_3593" s="T428">kuratar-dar-bɨt</ta>
            <ta e="T430" id="Seg_3594" s="T429">üčügej</ta>
            <ta e="T431" id="Seg_3595" s="T430">bagajɨ</ta>
            <ta e="T432" id="Seg_3596" s="T431">dʼaktat-tar</ta>
            <ta e="T433" id="Seg_3597" s="T432">e-ti-lere</ta>
            <ta e="T434" id="Seg_3598" s="T433">bihigiti-n</ta>
            <ta e="T435" id="Seg_3599" s="T434">barɨ-tɨ-n</ta>
            <ta e="T436" id="Seg_3600" s="T435">eː</ta>
            <ta e="T437" id="Seg_3601" s="T436">ill-en-ner</ta>
            <ta e="T438" id="Seg_3602" s="T437">egel-en-ner</ta>
            <ta e="T439" id="Seg_3603" s="T438">küččügüj</ta>
            <ta e="T440" id="Seg_3604" s="T439">ogo-luː</ta>
            <ta e="T441" id="Seg_3605" s="T440">utu-t-an-nar</ta>
            <ta e="T442" id="Seg_3606" s="T441">onton</ta>
            <ta e="T443" id="Seg_3607" s="T442">ah-a-t-an-nar</ta>
            <ta e="T444" id="Seg_3608" s="T443">onton</ta>
            <ta e="T445" id="Seg_3609" s="T444">muzʼej-dar</ta>
            <ta e="T446" id="Seg_3610" s="T445">üstün</ta>
            <ta e="T447" id="Seg_3611" s="T446">tʼeplaxok-ka</ta>
            <ta e="T448" id="Seg_3612" s="T447">hɨldʼ-ɨ-bɨp-pɨt</ta>
            <ta e="T449" id="Seg_3613" s="T448">Irtɨš</ta>
            <ta e="T450" id="Seg_3614" s="T449">di͡e-n</ta>
            <ta e="T451" id="Seg_3615" s="T450">eː</ta>
            <ta e="T452" id="Seg_3616" s="T451">ürek</ta>
            <ta e="T453" id="Seg_3617" s="T452">Irtɨš</ta>
            <ta e="T454" id="Seg_3618" s="T453">di͡e-n</ta>
            <ta e="T455" id="Seg_3619" s="T454">ürek</ta>
            <ta e="T456" id="Seg_3620" s="T455">üstün</ta>
            <ta e="T457" id="Seg_3621" s="T456">bar-bɨp-pɨt</ta>
            <ta e="T458" id="Seg_3622" s="T457">ol</ta>
            <ta e="T459" id="Seg_3623" s="T458">Irtɨš</ta>
            <ta e="T460" id="Seg_3624" s="T459">bu͡olla</ta>
            <ta e="T461" id="Seg_3625" s="T460">ulakan</ta>
            <ta e="T462" id="Seg_3626" s="T461">ürek-ke</ta>
            <ta e="T463" id="Seg_3627" s="T462">kɨtt-ar</ta>
            <ta e="T464" id="Seg_3628" s="T463">Obʼ</ta>
            <ta e="T465" id="Seg_3629" s="T464">di͡e-n</ta>
            <ta e="T466" id="Seg_3630" s="T465">ürek-ke</ta>
            <ta e="T467" id="Seg_3631" s="T466">ol</ta>
            <ta e="T468" id="Seg_3632" s="T467">kɨtt-ɨ-bɨt-tar-ga</ta>
            <ta e="T469" id="Seg_3633" s="T468">belek-tem-mip-pit</ta>
            <ta e="T471" id="Seg_3634" s="T470">ɨrɨ͡a-lar-ɨ</ta>
            <ta e="T472" id="Seg_3635" s="T471">ɨllaː-bɨp-pɨt</ta>
            <ta e="T473" id="Seg_3636" s="T472">kel-eːri-bit</ta>
            <ta e="T474" id="Seg_3637" s="T473">bar-aːrɨ-bɨt</ta>
            <ta e="T475" id="Seg_3638" s="T474">beseleː</ta>
            <ta e="T476" id="Seg_3639" s="T475">bagaji</ta>
            <ta e="T477" id="Seg_3640" s="T476">e-t-e</ta>
            <ta e="T478" id="Seg_3641" s="T477">kʼinoː-ga</ta>
            <ta e="T479" id="Seg_3642" s="T478">uhul-but-tara</ta>
            <ta e="T480" id="Seg_3643" s="T479">bihigiti-n</ta>
            <ta e="T481" id="Seg_3644" s="T480">onton</ta>
            <ta e="T482" id="Seg_3645" s="T481">onton</ta>
            <ta e="T483" id="Seg_3646" s="T482">töttörü</ta>
            <ta e="T484" id="Seg_3647" s="T483">kel-em-mit</ta>
            <ta e="T485" id="Seg_3648" s="T484">eː</ta>
            <ta e="T486" id="Seg_3649" s="T485">ol</ta>
            <ta e="T487" id="Seg_3650" s="T486">harsɨŋŋɨ-tɨ-n</ta>
            <ta e="T488" id="Seg_3651" s="T487">ol</ta>
            <ta e="T489" id="Seg_3652" s="T488">üs</ta>
            <ta e="T490" id="Seg_3653" s="T489">kün-ü</ta>
            <ta e="T491" id="Seg_3654" s="T490">bu͡ol-bup-put</ta>
            <ta e="T492" id="Seg_3655" s="T491">eː</ta>
            <ta e="T493" id="Seg_3656" s="T492">üs</ta>
            <ta e="T494" id="Seg_3657" s="T493">kün-ü</ta>
            <ta e="T495" id="Seg_3658" s="T494">bu͡ol-aːččɨ</ta>
            <ta e="T496" id="Seg_3659" s="T495">eː</ta>
            <ta e="T497" id="Seg_3660" s="T496">üs</ta>
            <ta e="T498" id="Seg_3661" s="T497">kün-ü</ta>
            <ta e="T499" id="Seg_3662" s="T498">bu͡ol-bup-putu-gar</ta>
            <ta e="T500" id="Seg_3663" s="T499">Xantɨ_Mansijskaj-ga</ta>
            <ta e="T501" id="Seg_3664" s="T500">üčügej</ta>
            <ta e="T502" id="Seg_3665" s="T501">bagaji</ta>
            <ta e="T503" id="Seg_3666" s="T502">kün-ner</ta>
            <ta e="T504" id="Seg_3667" s="T503">e-ti-lere</ta>
            <ta e="T505" id="Seg_3668" s="T504">kallaːn</ta>
            <ta e="T506" id="Seg_3669" s="T505">ol</ta>
            <ta e="T507" id="Seg_3670" s="T506">inni-ti-ger</ta>
            <ta e="T508" id="Seg_3671" s="T507">hamɨːr-dɨː-r</ta>
            <ta e="T509" id="Seg_3672" s="T508">e-t-e</ta>
            <ta e="T510" id="Seg_3673" s="T509">diː-r</ta>
            <ta e="T511" id="Seg_3674" s="T510">e-ti-lere</ta>
            <ta e="T512" id="Seg_3675" s="T511">onton</ta>
            <ta e="T513" id="Seg_3676" s="T512">hürbe-tten</ta>
            <ta e="T514" id="Seg_3677" s="T513">taksa</ta>
            <ta e="T515" id="Seg_3678" s="T514">gradus</ta>
            <ta e="T516" id="Seg_3679" s="T515">e-t-e</ta>
            <ta e="T517" id="Seg_3680" s="T516">hürbe</ta>
            <ta e="T518" id="Seg_3681" s="T517">tü͡ört</ta>
            <ta e="T519" id="Seg_3682" s="T518">hürbe</ta>
            <ta e="T520" id="Seg_3683" s="T519">bi͡es</ta>
            <ta e="T521" id="Seg_3684" s="T520">gradus</ta>
            <ta e="T522" id="Seg_3685" s="T521">bu͡ol-an</ta>
            <ta e="T523" id="Seg_3686" s="T522">is-t-e</ta>
            <ta e="T524" id="Seg_3687" s="T523">iti</ta>
            <ta e="T525" id="Seg_3688" s="T524">küt-ter-ge</ta>
            <ta e="T526" id="Seg_3689" s="T525">ol</ta>
            <ta e="T527" id="Seg_3690" s="T526">ihin</ta>
            <ta e="T528" id="Seg_3691" s="T527">bu͡olla</ta>
            <ta e="T529" id="Seg_3692" s="T528">bihi͡e-ke</ta>
            <ta e="T530" id="Seg_3693" s="T529">olus</ta>
            <ta e="T531" id="Seg_3694" s="T530">da</ta>
            <ta e="T532" id="Seg_3695" s="T531">itiː-te</ta>
            <ta e="T533" id="Seg_3696" s="T532">hu͡ok</ta>
            <ta e="T534" id="Seg_3697" s="T533">e-t-e</ta>
            <ta e="T535" id="Seg_3698" s="T534">no</ta>
            <ta e="T536" id="Seg_3699" s="T535">olus</ta>
            <ta e="T537" id="Seg_3700" s="T536">da</ta>
            <ta e="T538" id="Seg_3701" s="T537">i</ta>
            <ta e="T539" id="Seg_3702" s="T538">tɨmnɨː-ta</ta>
            <ta e="T540" id="Seg_3703" s="T539">hu͡ok</ta>
            <ta e="T541" id="Seg_3704" s="T540">e-t-e</ta>
            <ta e="T542" id="Seg_3705" s="T541">ol</ta>
            <ta e="T543" id="Seg_3706" s="T542">ihin</ta>
            <ta e="T544" id="Seg_3707" s="T543">bert</ta>
            <ta e="T545" id="Seg_3708" s="T544">üčügej</ta>
            <ta e="T546" id="Seg_3709" s="T545">bihi͡e-ke</ta>
            <ta e="T547" id="Seg_3710" s="T546">min</ta>
            <ta e="T548" id="Seg_3711" s="T547">hanaː-ba-r</ta>
            <ta e="T549" id="Seg_3712" s="T548">itinnik</ta>
            <ta e="T550" id="Seg_3713" s="T549">klimat</ta>
            <ta e="T551" id="Seg_3714" s="T550">giniler-ge</ta>
            <ta e="T552" id="Seg_3715" s="T551">itinnik</ta>
            <ta e="T553" id="Seg_3716" s="T552">kallaːn</ta>
            <ta e="T554" id="Seg_3717" s="T553">bert</ta>
            <ta e="T555" id="Seg_3718" s="T554">üčügej-dik</ta>
            <ta e="T556" id="Seg_3719" s="T555">tuks-ar</ta>
            <ta e="T557" id="Seg_3720" s="T556">ol</ta>
            <ta e="T558" id="Seg_3721" s="T557">ihin</ta>
            <ta e="T559" id="Seg_3722" s="T558">össü͡ö</ta>
            <ta e="T560" id="Seg_3723" s="T559">da</ta>
            <ta e="T561" id="Seg_3724" s="T560">ɨgɨr-dak-tarɨna</ta>
            <ta e="T562" id="Seg_3725" s="T561">min</ta>
            <ta e="T563" id="Seg_3726" s="T562">hanaː-ba-r</ta>
            <ta e="T564" id="Seg_3727" s="T563">min</ta>
            <ta e="T565" id="Seg_3728" s="T564">bu͡o</ta>
            <ta e="T566" id="Seg_3729" s="T565">bar-ar</ta>
            <ta e="T567" id="Seg_3730" s="T566">kördük-pün</ta>
            <ta e="T568" id="Seg_3731" s="T567">össü͡ö</ta>
            <ta e="T569" id="Seg_3732" s="T568">biːrde</ta>
            <ta e="T570" id="Seg_3733" s="T569">ɨ͡allan-a</ta>
         </annotation>
         <annotation name="mp" tierref="mp-UkOA">
            <ta e="T2" id="Seg_3734" s="T1">haka-LIː</ta>
            <ta e="T3" id="Seg_3735" s="T2">kepseː-IAK-m</ta>
            <ta e="T4" id="Seg_3736" s="T3">du͡o</ta>
            <ta e="T10" id="Seg_3737" s="T8">hüːrbe</ta>
            <ta e="T11" id="Seg_3738" s="T10">ikki-LAːK</ta>
            <ta e="T12" id="Seg_3739" s="T11">ijunʼ-GA</ta>
            <ta e="T13" id="Seg_3740" s="T12">ikki</ta>
            <ta e="T14" id="Seg_3741" s="T13">hüːs</ta>
            <ta e="T15" id="Seg_3742" s="T14">u͡on-LAːK</ta>
            <ta e="T16" id="Seg_3743" s="T15">dʼɨl-GA</ta>
            <ta e="T17" id="Seg_3744" s="T16">Xantɨ_Mansijskaj</ta>
            <ta e="T18" id="Seg_3745" s="T17">gu͡orat-GA</ta>
            <ta e="T19" id="Seg_3746" s="T18">bu͡ol-BIT-tA</ta>
            <ta e="T20" id="Seg_3747" s="T19">fʼestʼival</ta>
            <ta e="T23" id="Seg_3748" s="T22">di͡e-An</ta>
            <ta e="T24" id="Seg_3749" s="T23">aːt-LAːK</ta>
            <ta e="T25" id="Seg_3750" s="T24">onno</ta>
            <ta e="T26" id="Seg_3751" s="T25">Tajmɨr-ttAn</ta>
            <ta e="T27" id="Seg_3752" s="T26">bihigi</ta>
            <ta e="T28" id="Seg_3753" s="T27">bar-BIT-BIt</ta>
            <ta e="T29" id="Seg_3754" s="T28">ikki</ta>
            <ta e="T30" id="Seg_3755" s="T29">kihi</ta>
            <ta e="T31" id="Seg_3756" s="T30">biːrges</ta>
            <ta e="T32" id="Seg_3757" s="T31">bu͡ol-TI-tA</ta>
            <ta e="T33" id="Seg_3758" s="T32">Vladʼimʼir</ta>
            <ta e="T34" id="Seg_3759" s="T33">Eːnovʼičʼ</ta>
            <ta e="T35" id="Seg_3760" s="T34">Sʼigunʼej</ta>
            <ta e="T39" id="Seg_3761" s="T38">di͡e-An</ta>
            <ta e="T40" id="Seg_3762" s="T39">kihi</ta>
            <ta e="T41" id="Seg_3763" s="T40">aːt-LAːK</ta>
            <ta e="T45" id="Seg_3764" s="T44">i</ta>
            <ta e="T46" id="Seg_3765" s="T45">min</ta>
            <ta e="T47" id="Seg_3766" s="T46">Nosku͡o-ttAn</ta>
            <ta e="T48" id="Seg_3767" s="T47">onno</ta>
            <ta e="T49" id="Seg_3768" s="T48">bert</ta>
            <ta e="T50" id="Seg_3769" s="T49">üčügej-LIk</ta>
            <ta e="T51" id="Seg_3770" s="T50">bihigi-n</ta>
            <ta e="T52" id="Seg_3771" s="T51">tohuj-TI-LAr</ta>
            <ta e="T53" id="Seg_3772" s="T52">tuːs-LAːK</ta>
            <ta e="T54" id="Seg_3773" s="T53">keli͡ep-I-nAn</ta>
            <ta e="T55" id="Seg_3774" s="T54">ulakan</ta>
            <ta e="T56" id="Seg_3775" s="T55">bagajɨ</ta>
            <ta e="T57" id="Seg_3776" s="T56">gostinaca-GA</ta>
            <ta e="T58" id="Seg_3777" s="T57">dʼi͡e-LAː-TI-LAr</ta>
            <ta e="T59" id="Seg_3778" s="T58">aːt-tA</ta>
            <ta e="T60" id="Seg_3779" s="T59">bu͡ol-TI-tA</ta>
            <ta e="T61" id="Seg_3780" s="T60">hette</ta>
            <ta e="T62" id="Seg_3781" s="T61">girbej</ta>
            <ta e="T65" id="Seg_3782" s="T64">bert</ta>
            <ta e="T66" id="Seg_3783" s="T65">üčügej-LIk</ta>
            <ta e="T67" id="Seg_3784" s="T66">bihigi-nI</ta>
            <ta e="T68" id="Seg_3785" s="T67">ahaː-A-t-Ar-LAr</ta>
            <ta e="T69" id="Seg_3786" s="T68">onno</ta>
            <ta e="T70" id="Seg_3787" s="T69">ahaː-A-t-BIT-LArA</ta>
            <ta e="T71" id="Seg_3788" s="T70">utuj-t-BIT-LArA</ta>
            <ta e="T72" id="Seg_3789" s="T71">onton</ta>
            <ta e="T73" id="Seg_3790" s="T72">harsɨŋŋɨ-tI-n</ta>
            <ta e="T74" id="Seg_3791" s="T73">repeticija-LAː-BIT-BIt</ta>
            <ta e="T75" id="Seg_3792" s="T74">hol</ta>
            <ta e="T76" id="Seg_3793" s="T75">ki͡ehe-tI-n</ta>
            <ta e="T77" id="Seg_3794" s="T76">hol</ta>
            <ta e="T78" id="Seg_3795" s="T77">ki͡ehe-tI-n</ta>
            <ta e="T79" id="Seg_3796" s="T78">že</ta>
            <ta e="T80" id="Seg_3797" s="T79">vɨstupaj-LAː-BIT-BIt</ta>
            <ta e="T81" id="Seg_3798" s="T80">ol</ta>
            <ta e="T82" id="Seg_3799" s="T81">kelin-tI-ttAn</ta>
            <ta e="T83" id="Seg_3800" s="T82">tögürük</ta>
            <ta e="T84" id="Seg_3801" s="T83">ostu͡ol</ta>
            <ta e="T85" id="Seg_3802" s="T84">bu͡ol-BIT-tA</ta>
            <ta e="T86" id="Seg_3803" s="T85">ikki-Is</ta>
            <ta e="T87" id="Seg_3804" s="T86">kün-tI-GAr</ta>
            <ta e="T88" id="Seg_3805" s="T87">ol</ta>
            <ta e="T89" id="Seg_3806" s="T88">tögürük</ta>
            <ta e="T90" id="Seg_3807" s="T89">ostu͡ol-GA</ta>
            <ta e="T91" id="Seg_3808" s="T90">bu͡olla</ta>
            <ta e="T92" id="Seg_3809" s="T91">eŋin</ta>
            <ta e="T93" id="Seg_3810" s="T92">eː</ta>
            <ta e="T94" id="Seg_3811" s="T93">elbek</ta>
            <ta e="T95" id="Seg_3812" s="T94">kihi</ta>
            <ta e="T96" id="Seg_3813" s="T95">komuj-LIN-I-BIT-tA</ta>
            <ta e="T97" id="Seg_3814" s="T96">ü͡ör-Iː-nI</ta>
            <ta e="T98" id="Seg_3815" s="T97">kuhagan-nI</ta>
            <ta e="T99" id="Seg_3816" s="T98">eː</ta>
            <ta e="T100" id="Seg_3817" s="T99">olok</ta>
            <ta e="T101" id="Seg_3818" s="T100">tus-tI-nAn</ta>
            <ta e="T102" id="Seg_3819" s="T101">kepset-Iː</ta>
            <ta e="T103" id="Seg_3820" s="T102">baːr</ta>
            <ta e="T104" id="Seg_3821" s="T103">e-TI-tA</ta>
            <ta e="T105" id="Seg_3822" s="T104">ogo-LAr</ta>
            <ta e="T106" id="Seg_3823" s="T105">ü͡ören-Ar-LArI-n</ta>
            <ta e="T107" id="Seg_3824" s="T106">tus-LArI-nAn</ta>
            <ta e="T108" id="Seg_3825" s="T107">student-LAr</ta>
            <ta e="T109" id="Seg_3826" s="T108">eː</ta>
            <ta e="T110" id="Seg_3827" s="T109">iti</ta>
            <ta e="T111" id="Seg_3828" s="T110">ulakan</ta>
            <ta e="T112" id="Seg_3829" s="T111">vɨššɨj učʼebnɨj zavʼedʼenʼija-LAr</ta>
            <ta e="T113" id="Seg_3830" s="T112">tus-LArI-GAr</ta>
            <ta e="T114" id="Seg_3831" s="T113">togo</ta>
            <ta e="T115" id="Seg_3832" s="T114">bɨrak-Ar-LArI-n</ta>
            <ta e="T116" id="Seg_3833" s="T115">ili</ta>
            <ta e="T117" id="Seg_3834" s="T116">togo</ta>
            <ta e="T118" id="Seg_3835" s="T117">ü͡ören-BAT-LArI-n</ta>
            <ta e="T119" id="Seg_3836" s="T118">ol</ta>
            <ta e="T120" id="Seg_3837" s="T119">ol</ta>
            <ta e="T121" id="Seg_3838" s="T120">tus-tI-GAr</ta>
            <ta e="T122" id="Seg_3839" s="T121">eː</ta>
            <ta e="T123" id="Seg_3840" s="T122">ulakan</ta>
            <ta e="T124" id="Seg_3841" s="T123">onton</ta>
            <ta e="T125" id="Seg_3842" s="T124">vaproːs</ta>
            <ta e="T126" id="Seg_3843" s="T125">tur-Ar</ta>
            <ta e="T127" id="Seg_3844" s="T126">e-TI-tA</ta>
            <ta e="T128" id="Seg_3845" s="T127">onton</ta>
            <ta e="T129" id="Seg_3846" s="T128">bu͡olla</ta>
            <ta e="T131" id="Seg_3847" s="T130">eː</ta>
            <ta e="T132" id="Seg_3848" s="T131">emeːksin</ta>
            <ta e="T133" id="Seg_3849" s="T132">vɨstupaj-LAː-BIT</ta>
            <ta e="T134" id="Seg_3850" s="T133">onno</ta>
            <ta e="T135" id="Seg_3851" s="T134">Maldaːnova</ta>
            <ta e="T136" id="Seg_3852" s="T135">di͡e-An</ta>
            <ta e="T137" id="Seg_3853" s="T136">Marʼiːja</ta>
            <ta e="T138" id="Seg_3854" s="T137">Kuzʼmʼinʼičʼna</ta>
            <ta e="T139" id="Seg_3855" s="T138">öj-LAːK</ta>
            <ta e="T140" id="Seg_3856" s="T139">dʼaktar</ta>
            <ta e="T141" id="Seg_3857" s="T140">ol</ta>
            <ta e="T142" id="Seg_3858" s="T141">haŋar-BIT-tA</ta>
            <ta e="T143" id="Seg_3859" s="T142">eː</ta>
            <ta e="T144" id="Seg_3860" s="T143">ogo</ta>
            <ta e="T145" id="Seg_3861" s="T144">kihi-LAr</ta>
            <ta e="T146" id="Seg_3862" s="T145">eː</ta>
            <ta e="T147" id="Seg_3863" s="T146">ogo</ta>
            <ta e="T148" id="Seg_3864" s="T147">kihi-LAr</ta>
            <ta e="T149" id="Seg_3865" s="T148">beje-LArI-n</ta>
            <ta e="T150" id="Seg_3866" s="T149">kergen-LArI-GAr</ta>
            <ta e="T151" id="Seg_3867" s="T150">beje-LArI-n</ta>
            <ta e="T152" id="Seg_3868" s="T151">kergen-LArI-n</ta>
            <ta e="T153" id="Seg_3869" s="T152">is-LArI-GAr</ta>
            <ta e="T154" id="Seg_3870" s="T153">is-LArI-ttAn</ta>
            <ta e="T155" id="Seg_3871" s="T154">vaspʼitanʼija</ta>
            <ta e="T156" id="Seg_3872" s="T155">bu͡ol-Ar-LAːK</ta>
            <ta e="T157" id="Seg_3873" s="T156">töröː-A-t-BIT</ta>
            <ta e="T158" id="Seg_3874" s="T157">kihi</ta>
            <ta e="T159" id="Seg_3875" s="T158">bi͡er-Ar-LAːK</ta>
            <ta e="T160" id="Seg_3876" s="T159">ü͡örek-nI</ta>
            <ta e="T161" id="Seg_3877" s="T160">ötü͡ö-nI</ta>
            <ta e="T162" id="Seg_3878" s="T161">kuhagan-nI</ta>
            <ta e="T163" id="Seg_3879" s="T162">ogo-GA</ta>
            <ta e="T164" id="Seg_3880" s="T163">aː</ta>
            <ta e="T165" id="Seg_3881" s="T164">usku͡ola</ta>
            <ta e="T166" id="Seg_3882" s="T165">saːdik</ta>
            <ta e="T167" id="Seg_3883" s="T166">ol-GA</ta>
            <ta e="T168" id="Seg_3884" s="T167">bu͡olla</ta>
            <ta e="T169" id="Seg_3885" s="T168">eː</ta>
            <ta e="T170" id="Seg_3886" s="T169">učʼebnɨj zavʼedʼenʼija-LAr</ta>
            <ta e="T171" id="Seg_3887" s="T170">ol-GA</ta>
            <ta e="T172" id="Seg_3888" s="T171">kömö</ta>
            <ta e="T173" id="Seg_3889" s="T172">ere</ta>
            <ta e="T174" id="Seg_3890" s="T173">bu͡ol-IAK-LArI-n</ta>
            <ta e="T175" id="Seg_3891" s="T174">naːda</ta>
            <ta e="T176" id="Seg_3892" s="T175">a</ta>
            <ta e="T180" id="Seg_3893" s="T179">kördük</ta>
            <ta e="T181" id="Seg_3894" s="T180">bu͡ol-IAK-LArI-n</ta>
            <ta e="T182" id="Seg_3895" s="T181">ol</ta>
            <ta e="T183" id="Seg_3896" s="T182">ihin</ta>
            <ta e="T184" id="Seg_3897" s="T183">bu͡olla</ta>
            <ta e="T185" id="Seg_3898" s="T184">anɨ</ta>
            <ta e="T186" id="Seg_3899" s="T185">ilin</ta>
            <ta e="T187" id="Seg_3900" s="T186">dek</ta>
            <ta e="T188" id="Seg_3901" s="T187">eː</ta>
            <ta e="T189" id="Seg_3902" s="T188">ulaːt-InnAr-IAK-GA</ta>
            <ta e="T190" id="Seg_3903" s="T189">naːda</ta>
            <ta e="T191" id="Seg_3904" s="T190">di͡e-Ar</ta>
            <ta e="T192" id="Seg_3905" s="T191">eː</ta>
            <ta e="T193" id="Seg_3906" s="T192">ol</ta>
            <ta e="T194" id="Seg_3907" s="T193">dʼaktar</ta>
            <ta e="T195" id="Seg_3908" s="T194">eppi͡et-nI</ta>
            <ta e="T196" id="Seg_3909" s="T195">töröː-A-t-Ar</ta>
            <ta e="T197" id="Seg_3910" s="T196">kihi-LAr-ttAn</ta>
            <ta e="T198" id="Seg_3911" s="T197">roditel-LAr-ttAn</ta>
            <ta e="T199" id="Seg_3912" s="T198">da</ta>
            <ta e="T200" id="Seg_3913" s="T199">i</ta>
            <ta e="T201" id="Seg_3914" s="T200">ötü͡ö-nI</ta>
            <ta e="T202" id="Seg_3915" s="T201">elbeː-t-InnAr-IAK-GA</ta>
            <ta e="T203" id="Seg_3916" s="T202">di͡e-An</ta>
            <ta e="T204" id="Seg_3917" s="T203">haŋar-BIT-tA</ta>
            <ta e="T205" id="Seg_3918" s="T204">ol</ta>
            <ta e="T206" id="Seg_3919" s="T205">dʼaktar</ta>
            <ta e="T207" id="Seg_3920" s="T206">ötü͡ö-nI</ta>
            <ta e="T208" id="Seg_3921" s="T207">elbeː-t-InnAr-IAK-GA</ta>
            <ta e="T209" id="Seg_3922" s="T208">a</ta>
            <ta e="T210" id="Seg_3923" s="T209">kuhagan-nI</ta>
            <ta e="T211" id="Seg_3924" s="T210">agɨjaː-t-InnAr-IAK-GA</ta>
            <ta e="T212" id="Seg_3925" s="T211">bu</ta>
            <ta e="T213" id="Seg_3926" s="T212">hir-GA</ta>
            <ta e="T214" id="Seg_3927" s="T213">vot</ta>
            <ta e="T215" id="Seg_3928" s="T214">nu</ta>
            <ta e="T216" id="Seg_3929" s="T215">koloː-An</ta>
            <ta e="T217" id="Seg_3930" s="T216">kör-TAK-GA</ta>
            <ta e="T218" id="Seg_3931" s="T217">jeslʼi</ta>
            <ta e="T219" id="Seg_3932" s="T218">en-n</ta>
            <ta e="T220" id="Seg_3933" s="T219">kuhagan-LIk</ta>
            <ta e="T221" id="Seg_3934" s="T220">haŋar-TI-LAr</ta>
            <ta e="T222" id="Seg_3935" s="T221">kuhagan-nI</ta>
            <ta e="T223" id="Seg_3936" s="T222">gɨn-TI-LAr</ta>
            <ta e="T224" id="Seg_3937" s="T223">du͡o</ta>
            <ta e="T225" id="Seg_3938" s="T224">en</ta>
            <ta e="T226" id="Seg_3939" s="T225">bu͡o</ta>
            <ta e="T227" id="Seg_3940" s="T226">kepseː-m-IAK-GI-n</ta>
            <ta e="T228" id="Seg_3941" s="T227">naːda</ta>
            <ta e="T229" id="Seg_3942" s="T228">ol</ta>
            <ta e="T230" id="Seg_3943" s="T229">kihi</ta>
            <ta e="T231" id="Seg_3944" s="T230">tus-tI-nAn</ta>
            <ta e="T232" id="Seg_3945" s="T231">di͡e-Ar</ta>
            <ta e="T233" id="Seg_3946" s="T232">ol</ta>
            <ta e="T234" id="Seg_3947" s="T233">dʼaktar</ta>
            <ta e="T235" id="Seg_3948" s="T234">ol</ta>
            <ta e="T236" id="Seg_3949" s="T235">kihi</ta>
            <ta e="T237" id="Seg_3950" s="T236">tus-tI</ta>
            <ta e="T238" id="Seg_3951" s="T237">ol</ta>
            <ta e="T239" id="Seg_3952" s="T238">kihi</ta>
            <ta e="T240" id="Seg_3953" s="T239">tus-tI-nAn</ta>
            <ta e="T241" id="Seg_3954" s="T240">beje-tI-GAr</ta>
            <ta e="T242" id="Seg_3955" s="T241">haŋar-I-m-IAK-GI-n</ta>
            <ta e="T243" id="Seg_3956" s="T242">naːda</ta>
            <ta e="T244" id="Seg_3957" s="T243">no</ta>
            <ta e="T245" id="Seg_3958" s="T244">kihi-LAr-GA</ta>
            <ta e="T246" id="Seg_3959" s="T245">emi͡e</ta>
            <ta e="T247" id="Seg_3960" s="T246">kepseː-m-IAK-GA</ta>
            <ta e="T248" id="Seg_3961" s="T247">naːda</ta>
            <ta e="T249" id="Seg_3962" s="T248">štobɨ</ta>
            <ta e="T250" id="Seg_3963" s="T249">kuhagan</ta>
            <ta e="T251" id="Seg_3964" s="T250">ulaːt-I-m-IAK-tI-n</ta>
            <ta e="T252" id="Seg_3965" s="T251">a</ta>
            <ta e="T253" id="Seg_3966" s="T252">üčügej-LIk</ta>
            <ta e="T254" id="Seg_3967" s="T253">gɨn-BIT</ta>
            <ta e="T255" id="Seg_3968" s="T254">kihi-GA</ta>
            <ta e="T256" id="Seg_3969" s="T255">bu͡olla</ta>
            <ta e="T257" id="Seg_3970" s="T256">ol</ta>
            <ta e="T258" id="Seg_3971" s="T257">kihi-GA</ta>
            <ta e="T260" id="Seg_3972" s="T259">emi͡e</ta>
            <ta e="T261" id="Seg_3973" s="T260">beje-tI-GAr</ta>
            <ta e="T262" id="Seg_3974" s="T261">haŋar-I-m-IAK-GI-n</ta>
            <ta e="T263" id="Seg_3975" s="T262">štobɨ</ta>
            <ta e="T264" id="Seg_3976" s="T263">kihirgeː-m-IAK-tI-n</ta>
            <ta e="T265" id="Seg_3977" s="T264">a</ta>
            <ta e="T266" id="Seg_3978" s="T265">atɨn</ta>
            <ta e="T267" id="Seg_3979" s="T266">kihi-LAr-GA</ta>
            <ta e="T268" id="Seg_3980" s="T267">kepseː-IAK-GI-n</ta>
            <ta e="T269" id="Seg_3981" s="T268">naːda</ta>
            <ta e="T270" id="Seg_3982" s="T269">što</ta>
            <ta e="T271" id="Seg_3983" s="T270">ol</ta>
            <ta e="T272" id="Seg_3984" s="T271">kihi</ta>
            <ta e="T273" id="Seg_3985" s="T272">üčügej-nI</ta>
            <ta e="T274" id="Seg_3986" s="T273">gɨn-BIT-tA</ta>
            <ta e="T275" id="Seg_3987" s="T274">di͡e-An</ta>
            <ta e="T276" id="Seg_3988" s="T275">ol</ta>
            <ta e="T277" id="Seg_3989" s="T276">kihi</ta>
            <ta e="T278" id="Seg_3990" s="T277">onnuk</ta>
            <ta e="T279" id="Seg_3991" s="T278">di͡e-An</ta>
            <ta e="T280" id="Seg_3992" s="T279">ol</ta>
            <ta e="T281" id="Seg_3993" s="T280">kihi</ta>
            <ta e="T282" id="Seg_3994" s="T281">itinnik</ta>
            <ta e="T283" id="Seg_3995" s="T282">di͡e-An</ta>
            <ta e="T285" id="Seg_3996" s="T284">oččogo</ta>
            <ta e="T286" id="Seg_3997" s="T285">bu͡olla</ta>
            <ta e="T287" id="Seg_3998" s="T286">ötü͡ö</ta>
            <ta e="T288" id="Seg_3999" s="T287">ulaːt-Ar</ta>
            <ta e="T289" id="Seg_4000" s="T288">ötü͡ö</ta>
            <ta e="T290" id="Seg_4001" s="T289">ulaːt-Ar</ta>
            <ta e="T291" id="Seg_4002" s="T290">onnuk</ta>
            <ta e="T292" id="Seg_4003" s="T291">formula-nAn</ta>
            <ta e="T293" id="Seg_4004" s="T292">di͡e-An</ta>
            <ta e="T294" id="Seg_4005" s="T293">no</ta>
            <ta e="T295" id="Seg_4006" s="T294">emi͡e</ta>
            <ta e="T296" id="Seg_4007" s="T295">taːk</ta>
            <ta e="T297" id="Seg_4008" s="T296">i</ta>
            <ta e="T298" id="Seg_4009" s="T297">ol</ta>
            <ta e="T299" id="Seg_4010" s="T298">formula-GA</ta>
            <ta e="T300" id="Seg_4011" s="T299">min</ta>
            <ta e="T301" id="Seg_4012" s="T300">haglas-LAN-BAT-BIn</ta>
            <ta e="T302" id="Seg_4013" s="T301">togo</ta>
            <ta e="T303" id="Seg_4014" s="T302">di͡e-An-GIt</ta>
            <ta e="T304" id="Seg_4015" s="T303">kuhagan</ta>
            <ta e="T305" id="Seg_4016" s="T304">ikki-Is</ta>
            <ta e="T306" id="Seg_4017" s="T305">formula</ta>
            <ta e="T307" id="Seg_4018" s="T306">baːr</ta>
            <ta e="T308" id="Seg_4019" s="T307">ol</ta>
            <ta e="T309" id="Seg_4020" s="T308">ürüt-tI-GAr</ta>
            <ta e="T310" id="Seg_4021" s="T309">oččogo</ta>
            <ta e="T311" id="Seg_4022" s="T310">kuhagan</ta>
            <ta e="T312" id="Seg_4023" s="T311">ulaːt-I-m-IAK-tI-n</ta>
            <ta e="T313" id="Seg_4024" s="T312">kuhagan-nI</ta>
            <ta e="T314" id="Seg_4025" s="T313">ilin</ta>
            <ta e="T315" id="Seg_4026" s="T314">dek</ta>
            <ta e="T316" id="Seg_4027" s="T315">töttörü</ta>
            <ta e="T317" id="Seg_4028" s="T316">ulakan-nI</ta>
            <ta e="T318" id="Seg_4029" s="T317">eppi͡et</ta>
            <ta e="T319" id="Seg_4030" s="T318">bi͡er-IAK-GA</ta>
            <ta e="T320" id="Seg_4031" s="T319">naːda</ta>
            <ta e="T321" id="Seg_4032" s="T320">gini-GA</ta>
            <ta e="T322" id="Seg_4033" s="T321">štobɨ</ta>
            <ta e="T323" id="Seg_4034" s="T322">kuhagan-nI</ta>
            <ta e="T324" id="Seg_4035" s="T323">gɨn-BIT</ta>
            <ta e="T325" id="Seg_4036" s="T324">kihi</ta>
            <ta e="T326" id="Seg_4037" s="T325">bil-IAK-tI-n</ta>
            <ta e="T327" id="Seg_4038" s="T326">što</ta>
            <ta e="T328" id="Seg_4039" s="T327">meːne</ta>
            <ta e="T329" id="Seg_4040" s="T328">kihi-nI</ta>
            <ta e="T330" id="Seg_4041" s="T329">gini͡ene</ta>
            <ta e="T331" id="Seg_4042" s="T330">kuhagan-tA</ta>
            <ta e="T332" id="Seg_4043" s="T331">meːne</ta>
            <ta e="T333" id="Seg_4044" s="T332">aːs-IAK.[tA]</ta>
            <ta e="T334" id="Seg_4045" s="T333">hu͡ok-tA</ta>
            <ta e="T335" id="Seg_4046" s="T334">gini-GA</ta>
            <ta e="T336" id="Seg_4047" s="T335">gini</ta>
            <ta e="T337" id="Seg_4048" s="T336">bu͡olla</ta>
            <ta e="T338" id="Seg_4049" s="T337">kihi</ta>
            <ta e="T339" id="Seg_4050" s="T338">ilin-tI-GAr</ta>
            <ta e="T340" id="Seg_4051" s="T339">eppi͡et-LAː-Ar-LAːK</ta>
            <ta e="T341" id="Seg_4052" s="T340">štobɨ</ta>
            <ta e="T342" id="Seg_4053" s="T341">ilin</ta>
            <ta e="T343" id="Seg_4054" s="T342">dek</ta>
            <ta e="T344" id="Seg_4055" s="T343">kuhagan-nI</ta>
            <ta e="T345" id="Seg_4056" s="T344">gɨn-I-m-IAK-tI-n</ta>
            <ta e="T346" id="Seg_4057" s="T345">oččogo</ta>
            <ta e="T347" id="Seg_4058" s="T346">bu͡olla</ta>
            <ta e="T348" id="Seg_4059" s="T347">ötü͡ö</ta>
            <ta e="T349" id="Seg_4060" s="T348">ikki-TA</ta>
            <ta e="T350" id="Seg_4061" s="T349">raz</ta>
            <ta e="T351" id="Seg_4062" s="T350">ulaːt-An</ta>
            <ta e="T352" id="Seg_4063" s="T351">tagɨs-IAK-tA</ta>
            <ta e="T353" id="Seg_4064" s="T352">i</ta>
            <ta e="T354" id="Seg_4065" s="T353">olor-AːččI</ta>
            <ta e="T355" id="Seg_4066" s="T354">kihi-LAr</ta>
            <ta e="T356" id="Seg_4067" s="T355">onno</ta>
            <ta e="T357" id="Seg_4068" s="T356">olor-AːččI</ta>
            <ta e="T358" id="Seg_4069" s="T357">kihi-LAr</ta>
            <ta e="T359" id="Seg_4070" s="T358">beje-LArI-n</ta>
            <ta e="T360" id="Seg_4071" s="T359">ilin</ta>
            <ta e="T361" id="Seg_4072" s="T360">dek</ta>
            <ta e="T362" id="Seg_4073" s="T361">beje-LArI-n</ta>
            <ta e="T363" id="Seg_4074" s="T362">ilin</ta>
            <ta e="T364" id="Seg_4075" s="T363">dek</ta>
            <ta e="T365" id="Seg_4076" s="T364">atagastaː-A-t-IAK-LArA</ta>
            <ta e="T366" id="Seg_4077" s="T365">hu͡ok-LArA</ta>
            <ta e="T367" id="Seg_4078" s="T366">da</ta>
            <ta e="T368" id="Seg_4079" s="T367">atagastaː-A-t-IAK-LArA</ta>
            <ta e="T369" id="Seg_4080" s="T368">hu͡ok-LArA</ta>
            <ta e="T370" id="Seg_4081" s="T369">aː</ta>
            <ta e="T371" id="Seg_4082" s="T370">no</ta>
            <ta e="T372" id="Seg_4083" s="T371">onton</ta>
            <ta e="T373" id="Seg_4084" s="T372">ol</ta>
            <ta e="T374" id="Seg_4085" s="T373">ol</ta>
            <ta e="T375" id="Seg_4086" s="T374">kruglɨj</ta>
            <ta e="T376" id="Seg_4087" s="T375">tögürük</ta>
            <ta e="T377" id="Seg_4088" s="T376">ostu͡ol</ta>
            <ta e="T378" id="Seg_4089" s="T377">büt-BIT-tI-n</ta>
            <ta e="T379" id="Seg_4090" s="T378">genne</ta>
            <ta e="T380" id="Seg_4091" s="T379">emi͡e</ta>
            <ta e="T381" id="Seg_4092" s="T380">kancert</ta>
            <ta e="T384" id="Seg_4093" s="T383">di͡e-An</ta>
            <ta e="T385" id="Seg_4094" s="T384">bu͡ol-BIT-tA</ta>
            <ta e="T386" id="Seg_4095" s="T385">onno</ta>
            <ta e="T387" id="Seg_4096" s="T386">bert</ta>
            <ta e="T388" id="Seg_4097" s="T387">üčügej-LIk</ta>
            <ta e="T389" id="Seg_4098" s="T388">vɨstupaj-LAː-BIT-LArA</ta>
            <ta e="T390" id="Seg_4099" s="T389">elbek</ta>
            <ta e="T391" id="Seg_4100" s="T390">kallʼektʼiv</ta>
            <ta e="T392" id="Seg_4101" s="T391">kel-BIT-tA</ta>
            <ta e="T393" id="Seg_4102" s="T392">eː</ta>
            <ta e="T394" id="Seg_4103" s="T393">Xantɨ_Mansijskaj-ttAn</ta>
            <ta e="T395" id="Seg_4104" s="T394">beje-tI-ttAn</ta>
            <ta e="T396" id="Seg_4105" s="T395">teatralʼnaj</ta>
            <ta e="T397" id="Seg_4106" s="T396">gruppa</ta>
            <ta e="T398" id="Seg_4107" s="T397">baːr</ta>
            <ta e="T399" id="Seg_4108" s="T398">e-TI-tA</ta>
            <ta e="T400" id="Seg_4109" s="T399">bihi͡ene</ta>
            <ta e="T401" id="Seg_4110" s="T400">ansamblʼ</ta>
            <ta e="T402" id="Seg_4111" s="T401">xejro-BIt</ta>
            <ta e="T403" id="Seg_4112" s="T402">kördük</ta>
            <ta e="T404" id="Seg_4113" s="T403">onton</ta>
            <ta e="T405" id="Seg_4114" s="T404">bu͡olla</ta>
            <ta e="T406" id="Seg_4115" s="T405">Magadan-ttAn</ta>
            <ta e="T407" id="Seg_4116" s="T406">kel-BIT-LArA</ta>
            <ta e="T408" id="Seg_4117" s="T407">eː</ta>
            <ta e="T409" id="Seg_4118" s="T408">Jamala_Nʼenʼeckij avtonomnɨj oːkrug-ttAn</ta>
            <ta e="T410" id="Seg_4119" s="T409">Čukotka-ttAn</ta>
            <ta e="T411" id="Seg_4120" s="T410">ansamblʼ</ta>
            <ta e="T412" id="Seg_4121" s="T411">baːr</ta>
            <ta e="T413" id="Seg_4122" s="T412">e-TI-tA</ta>
            <ta e="T414" id="Seg_4123" s="T413">ɨraːk</ta>
            <ta e="T415" id="Seg_4124" s="T414">hir-ttAn</ta>
            <ta e="T416" id="Seg_4125" s="T415">kel-ItAlAː-BIT-LAr</ta>
            <ta e="T417" id="Seg_4126" s="T416">onton</ta>
            <ta e="T418" id="Seg_4127" s="T417">elbek-elbek</ta>
            <ta e="T419" id="Seg_4128" s="T418">hir-ttAn</ta>
            <ta e="T420" id="Seg_4129" s="T419">kel-ItAlAː-BIT-LAr</ta>
            <ta e="T421" id="Seg_4130" s="T420">onno</ta>
            <ta e="T422" id="Seg_4131" s="T421">hüːrbe</ta>
            <ta e="T423" id="Seg_4132" s="T422">ikki</ta>
            <ta e="T424" id="Seg_4133" s="T423">kallʼektʼiv</ta>
            <ta e="T425" id="Seg_4134" s="T424">baːr</ta>
            <ta e="T426" id="Seg_4135" s="T425">e-TI-tA</ta>
            <ta e="T427" id="Seg_4136" s="T426">eː</ta>
            <ta e="T428" id="Seg_4137" s="T427">onton</ta>
            <ta e="T429" id="Seg_4138" s="T428">kurator-LAr-BIt</ta>
            <ta e="T430" id="Seg_4139" s="T429">üčügej</ta>
            <ta e="T431" id="Seg_4140" s="T430">bagajɨ</ta>
            <ta e="T432" id="Seg_4141" s="T431">dʼaktar-LAr</ta>
            <ta e="T433" id="Seg_4142" s="T432">e-TI-LArA</ta>
            <ta e="T434" id="Seg_4143" s="T433">bihigi-n</ta>
            <ta e="T435" id="Seg_4144" s="T434">barɨ-tI-n</ta>
            <ta e="T436" id="Seg_4145" s="T435">eː</ta>
            <ta e="T437" id="Seg_4146" s="T436">ilin-An-LAr</ta>
            <ta e="T438" id="Seg_4147" s="T437">egel-An-LAr</ta>
            <ta e="T439" id="Seg_4148" s="T438">küččügüj</ta>
            <ta e="T440" id="Seg_4149" s="T439">ogo-LIː</ta>
            <ta e="T441" id="Seg_4150" s="T440">utuj-t-An-LAr</ta>
            <ta e="T442" id="Seg_4151" s="T441">onton</ta>
            <ta e="T443" id="Seg_4152" s="T442">ahaː-A-t-An-LAr</ta>
            <ta e="T444" id="Seg_4153" s="T443">onton</ta>
            <ta e="T445" id="Seg_4154" s="T444">muzej-LAr</ta>
            <ta e="T446" id="Seg_4155" s="T445">üstün</ta>
            <ta e="T447" id="Seg_4156" s="T446">teploxod-GA</ta>
            <ta e="T448" id="Seg_4157" s="T447">hɨrɨt-I-BIT-BIt</ta>
            <ta e="T449" id="Seg_4158" s="T448">Irtɨš</ta>
            <ta e="T450" id="Seg_4159" s="T449">di͡e-An</ta>
            <ta e="T451" id="Seg_4160" s="T450">eː</ta>
            <ta e="T452" id="Seg_4161" s="T451">ürek</ta>
            <ta e="T453" id="Seg_4162" s="T452">Irtɨš</ta>
            <ta e="T454" id="Seg_4163" s="T453">di͡e-An</ta>
            <ta e="T455" id="Seg_4164" s="T454">ürek</ta>
            <ta e="T456" id="Seg_4165" s="T455">üstün</ta>
            <ta e="T457" id="Seg_4166" s="T456">bar-BIT-BIt</ta>
            <ta e="T458" id="Seg_4167" s="T457">ol</ta>
            <ta e="T459" id="Seg_4168" s="T458">Irtɨš</ta>
            <ta e="T460" id="Seg_4169" s="T459">bu͡olla</ta>
            <ta e="T461" id="Seg_4170" s="T460">ulakan</ta>
            <ta e="T462" id="Seg_4171" s="T461">ürek-GA</ta>
            <ta e="T463" id="Seg_4172" s="T462">kɨtɨn-Ar</ta>
            <ta e="T464" id="Seg_4173" s="T463">Obʼ</ta>
            <ta e="T465" id="Seg_4174" s="T464">di͡e-An</ta>
            <ta e="T466" id="Seg_4175" s="T465">ürek-GA</ta>
            <ta e="T467" id="Seg_4176" s="T466">ol</ta>
            <ta e="T468" id="Seg_4177" s="T467">kɨtɨn-I-BIT-LAr-GA</ta>
            <ta e="T469" id="Seg_4178" s="T468">belek-LAN-BIT-BIt</ta>
            <ta e="T471" id="Seg_4179" s="T470">ɨrɨ͡a-LAr-nI</ta>
            <ta e="T472" id="Seg_4180" s="T471">ɨllaː-BIT-BIt</ta>
            <ta e="T473" id="Seg_4181" s="T472">kel-AːrI-BIt</ta>
            <ta e="T474" id="Seg_4182" s="T473">bar-AːrI-BIt</ta>
            <ta e="T475" id="Seg_4183" s="T474">beseleː</ta>
            <ta e="T476" id="Seg_4184" s="T475">bagajɨ</ta>
            <ta e="T477" id="Seg_4185" s="T476">e-TI-tA</ta>
            <ta e="T478" id="Seg_4186" s="T477">kinoː-GA</ta>
            <ta e="T479" id="Seg_4187" s="T478">ugul-BIT-LArA</ta>
            <ta e="T480" id="Seg_4188" s="T479">bihigi-n</ta>
            <ta e="T481" id="Seg_4189" s="T480">onton</ta>
            <ta e="T482" id="Seg_4190" s="T481">onton</ta>
            <ta e="T483" id="Seg_4191" s="T482">töttörü</ta>
            <ta e="T484" id="Seg_4192" s="T483">kel-An-BIt</ta>
            <ta e="T485" id="Seg_4193" s="T484">eː</ta>
            <ta e="T486" id="Seg_4194" s="T485">ol</ta>
            <ta e="T487" id="Seg_4195" s="T486">harsɨŋŋɨ-tI-n</ta>
            <ta e="T488" id="Seg_4196" s="T487">ol</ta>
            <ta e="T489" id="Seg_4197" s="T488">üs</ta>
            <ta e="T490" id="Seg_4198" s="T489">kün-nI</ta>
            <ta e="T491" id="Seg_4199" s="T490">bu͡ol-BIT-BIt</ta>
            <ta e="T492" id="Seg_4200" s="T491">eː</ta>
            <ta e="T493" id="Seg_4201" s="T492">üs</ta>
            <ta e="T494" id="Seg_4202" s="T493">kün-nI</ta>
            <ta e="T495" id="Seg_4203" s="T494">bu͡ol-AːččI</ta>
            <ta e="T496" id="Seg_4204" s="T495">eː</ta>
            <ta e="T497" id="Seg_4205" s="T496">üs</ta>
            <ta e="T498" id="Seg_4206" s="T497">kün-nI</ta>
            <ta e="T499" id="Seg_4207" s="T498">bu͡ol-BIT-BItI-GAr</ta>
            <ta e="T500" id="Seg_4208" s="T499">Xantɨ_Mansijskaj-GA</ta>
            <ta e="T501" id="Seg_4209" s="T500">üčügej</ta>
            <ta e="T502" id="Seg_4210" s="T501">bagajɨ</ta>
            <ta e="T503" id="Seg_4211" s="T502">kün-LAr</ta>
            <ta e="T504" id="Seg_4212" s="T503">e-TI-LArA</ta>
            <ta e="T505" id="Seg_4213" s="T504">kallaːn</ta>
            <ta e="T506" id="Seg_4214" s="T505">ol</ta>
            <ta e="T507" id="Seg_4215" s="T506">ilin-tI-GAr</ta>
            <ta e="T508" id="Seg_4216" s="T507">hamɨːr-LAː-Ar</ta>
            <ta e="T509" id="Seg_4217" s="T508">e-TI-tA</ta>
            <ta e="T510" id="Seg_4218" s="T509">di͡e-Ar</ta>
            <ta e="T511" id="Seg_4219" s="T510">e-TI-LArA</ta>
            <ta e="T512" id="Seg_4220" s="T511">onton</ta>
            <ta e="T513" id="Seg_4221" s="T512">hüːrbe-ttAn</ta>
            <ta e="T514" id="Seg_4222" s="T513">taksa</ta>
            <ta e="T515" id="Seg_4223" s="T514">gradus</ta>
            <ta e="T516" id="Seg_4224" s="T515">e-TI-tA</ta>
            <ta e="T517" id="Seg_4225" s="T516">hüːrbe</ta>
            <ta e="T518" id="Seg_4226" s="T517">tü͡ört</ta>
            <ta e="T519" id="Seg_4227" s="T518">hüːrbe</ta>
            <ta e="T520" id="Seg_4228" s="T519">bi͡es</ta>
            <ta e="T521" id="Seg_4229" s="T520">gradus</ta>
            <ta e="T522" id="Seg_4230" s="T521">bu͡ol-An</ta>
            <ta e="T523" id="Seg_4231" s="T522">is-TI-tA</ta>
            <ta e="T524" id="Seg_4232" s="T523">iti</ta>
            <ta e="T525" id="Seg_4233" s="T524">kün-LAr-GA</ta>
            <ta e="T526" id="Seg_4234" s="T525">ol</ta>
            <ta e="T527" id="Seg_4235" s="T526">ihin</ta>
            <ta e="T528" id="Seg_4236" s="T527">bu͡olla</ta>
            <ta e="T529" id="Seg_4237" s="T528">bihigi-GA</ta>
            <ta e="T530" id="Seg_4238" s="T529">olus</ta>
            <ta e="T531" id="Seg_4239" s="T530">da</ta>
            <ta e="T532" id="Seg_4240" s="T531">itiː-tA</ta>
            <ta e="T533" id="Seg_4241" s="T532">hu͡ok</ta>
            <ta e="T534" id="Seg_4242" s="T533">e-TI-tA</ta>
            <ta e="T535" id="Seg_4243" s="T534">no</ta>
            <ta e="T536" id="Seg_4244" s="T535">olus</ta>
            <ta e="T537" id="Seg_4245" s="T536">da</ta>
            <ta e="T538" id="Seg_4246" s="T537">i</ta>
            <ta e="T539" id="Seg_4247" s="T538">tɨmnɨː-tA</ta>
            <ta e="T540" id="Seg_4248" s="T539">hu͡ok</ta>
            <ta e="T541" id="Seg_4249" s="T540">e-TI-tA</ta>
            <ta e="T542" id="Seg_4250" s="T541">ol</ta>
            <ta e="T543" id="Seg_4251" s="T542">ihin</ta>
            <ta e="T544" id="Seg_4252" s="T543">bert</ta>
            <ta e="T545" id="Seg_4253" s="T544">üčügej</ta>
            <ta e="T546" id="Seg_4254" s="T545">bihigi-GA</ta>
            <ta e="T547" id="Seg_4255" s="T546">min</ta>
            <ta e="T548" id="Seg_4256" s="T547">hanaː-BA-r</ta>
            <ta e="T549" id="Seg_4257" s="T548">itinnik</ta>
            <ta e="T550" id="Seg_4258" s="T549">klimat</ta>
            <ta e="T551" id="Seg_4259" s="T550">giniler-GA</ta>
            <ta e="T552" id="Seg_4260" s="T551">itinnik</ta>
            <ta e="T553" id="Seg_4261" s="T552">kallaːn</ta>
            <ta e="T554" id="Seg_4262" s="T553">bert</ta>
            <ta e="T555" id="Seg_4263" s="T554">üčügej-LIk</ta>
            <ta e="T556" id="Seg_4264" s="T555">tugus-Ar</ta>
            <ta e="T557" id="Seg_4265" s="T556">ol</ta>
            <ta e="T558" id="Seg_4266" s="T557">ihin</ta>
            <ta e="T559" id="Seg_4267" s="T558">össü͡ö</ta>
            <ta e="T560" id="Seg_4268" s="T559">da</ta>
            <ta e="T561" id="Seg_4269" s="T560">ɨgɨr-TAK-TArInA</ta>
            <ta e="T562" id="Seg_4270" s="T561">min</ta>
            <ta e="T563" id="Seg_4271" s="T562">hanaː-BA-r</ta>
            <ta e="T564" id="Seg_4272" s="T563">min</ta>
            <ta e="T565" id="Seg_4273" s="T564">bu͡o</ta>
            <ta e="T566" id="Seg_4274" s="T565">bar-Ar</ta>
            <ta e="T567" id="Seg_4275" s="T566">kördük-BIn</ta>
            <ta e="T568" id="Seg_4276" s="T567">össü͡ö</ta>
            <ta e="T569" id="Seg_4277" s="T568">biːrde</ta>
            <ta e="T570" id="Seg_4278" s="T569">ɨ͡allan-A</ta>
         </annotation>
         <annotation name="ge" tierref="ge-UkOA">
            <ta e="T2" id="Seg_4279" s="T1">Dolgan-SIM</ta>
            <ta e="T3" id="Seg_4280" s="T2">tell-FUT-1SG</ta>
            <ta e="T4" id="Seg_4281" s="T3">Q</ta>
            <ta e="T10" id="Seg_4282" s="T8">twenty</ta>
            <ta e="T11" id="Seg_4283" s="T10">two-PROPR</ta>
            <ta e="T12" id="Seg_4284" s="T11">june-DAT/LOC</ta>
            <ta e="T13" id="Seg_4285" s="T12">two</ta>
            <ta e="T14" id="Seg_4286" s="T13">hundred</ta>
            <ta e="T15" id="Seg_4287" s="T14">ten-PROPR</ta>
            <ta e="T16" id="Seg_4288" s="T15">year-DAT/LOC</ta>
            <ta e="T17" id="Seg_4289" s="T16">Khanty_Mansiysk</ta>
            <ta e="T18" id="Seg_4290" s="T17">city-DAT/LOC</ta>
            <ta e="T19" id="Seg_4291" s="T18">become-PST2-3SG</ta>
            <ta e="T20" id="Seg_4292" s="T19">festival.[NOM]</ta>
            <ta e="T23" id="Seg_4293" s="T22">say-CVB.SEQ</ta>
            <ta e="T24" id="Seg_4294" s="T23">name-PROPR</ta>
            <ta e="T25" id="Seg_4295" s="T24">thither</ta>
            <ta e="T26" id="Seg_4296" s="T25">Taymyr-ABL</ta>
            <ta e="T27" id="Seg_4297" s="T26">1PL.[NOM]</ta>
            <ta e="T28" id="Seg_4298" s="T27">go-PST2-1PL</ta>
            <ta e="T29" id="Seg_4299" s="T28">two</ta>
            <ta e="T30" id="Seg_4300" s="T29">human.being.[NOM]</ta>
            <ta e="T31" id="Seg_4301" s="T30">other.[NOM]</ta>
            <ta e="T32" id="Seg_4302" s="T31">be-PST1-3SG</ta>
            <ta e="T33" id="Seg_4303" s="T32">Vladimir</ta>
            <ta e="T34" id="Seg_4304" s="T33">Enovich</ta>
            <ta e="T35" id="Seg_4305" s="T34">Siguney.[NOM]</ta>
            <ta e="T39" id="Seg_4306" s="T38">say-CVB.SEQ</ta>
            <ta e="T40" id="Seg_4307" s="T39">human.being.[NOM]</ta>
            <ta e="T41" id="Seg_4308" s="T40">name-PROPR</ta>
            <ta e="T45" id="Seg_4309" s="T44">and</ta>
            <ta e="T46" id="Seg_4310" s="T45">1SG.[NOM]</ta>
            <ta e="T47" id="Seg_4311" s="T46">Khatanga-ABL</ta>
            <ta e="T48" id="Seg_4312" s="T47">there</ta>
            <ta e="T49" id="Seg_4313" s="T48">very</ta>
            <ta e="T50" id="Seg_4314" s="T49">good-ADVZ</ta>
            <ta e="T51" id="Seg_4315" s="T50">1PL-ACC</ta>
            <ta e="T52" id="Seg_4316" s="T51">receive-PST1-3PL</ta>
            <ta e="T53" id="Seg_4317" s="T52">salt-PROPR</ta>
            <ta e="T54" id="Seg_4318" s="T53">bread-EP-INSTR</ta>
            <ta e="T55" id="Seg_4319" s="T54">big</ta>
            <ta e="T56" id="Seg_4320" s="T55">very</ta>
            <ta e="T57" id="Seg_4321" s="T56">hotel-DAT/LOC</ta>
            <ta e="T58" id="Seg_4322" s="T57">house-VBZ-PST1-3PL</ta>
            <ta e="T59" id="Seg_4323" s="T58">name-3SG.[NOM]</ta>
            <ta e="T60" id="Seg_4324" s="T59">be-PST1-3SG</ta>
            <ta e="T61" id="Seg_4325" s="T60">seven</ta>
            <ta e="T62" id="Seg_4326" s="T61">hill.[NOM]</ta>
            <ta e="T65" id="Seg_4327" s="T64">very</ta>
            <ta e="T66" id="Seg_4328" s="T65">good-ADVZ</ta>
            <ta e="T67" id="Seg_4329" s="T66">1PL-ACC</ta>
            <ta e="T68" id="Seg_4330" s="T67">eat-EP-CAUS-PRS-3PL</ta>
            <ta e="T69" id="Seg_4331" s="T68">there</ta>
            <ta e="T70" id="Seg_4332" s="T69">eat-EP-CAUS-PST2-3PL</ta>
            <ta e="T71" id="Seg_4333" s="T70">sleep-CAUS-PST2-3PL</ta>
            <ta e="T72" id="Seg_4334" s="T71">then</ta>
            <ta e="T73" id="Seg_4335" s="T72">next.morning-3SG-ACC</ta>
            <ta e="T74" id="Seg_4336" s="T73">rehearsal-VBZ-PST2-1PL</ta>
            <ta e="T75" id="Seg_4337" s="T74">that.EMPH.[NOM]</ta>
            <ta e="T76" id="Seg_4338" s="T75">evening-3SG-ACC</ta>
            <ta e="T77" id="Seg_4339" s="T76">that.EMPH.[NOM]</ta>
            <ta e="T78" id="Seg_4340" s="T77">evening-3SG-ACC</ta>
            <ta e="T79" id="Seg_4341" s="T78">EMPH</ta>
            <ta e="T80" id="Seg_4342" s="T79">perform-VBZ-PST2-1PL</ta>
            <ta e="T81" id="Seg_4343" s="T80">that.[NOM]</ta>
            <ta e="T82" id="Seg_4344" s="T81">back-3SG-ABL</ta>
            <ta e="T83" id="Seg_4345" s="T82">round</ta>
            <ta e="T84" id="Seg_4346" s="T83">table.[NOM]</ta>
            <ta e="T85" id="Seg_4347" s="T84">be-PST2-3SG</ta>
            <ta e="T86" id="Seg_4348" s="T85">two-ORD</ta>
            <ta e="T87" id="Seg_4349" s="T86">day-3SG-DAT/LOC</ta>
            <ta e="T88" id="Seg_4350" s="T87">that</ta>
            <ta e="T89" id="Seg_4351" s="T88">round</ta>
            <ta e="T90" id="Seg_4352" s="T89">table-DAT/LOC</ta>
            <ta e="T91" id="Seg_4353" s="T90">MOD</ta>
            <ta e="T92" id="Seg_4354" s="T91">different</ta>
            <ta e="T93" id="Seg_4355" s="T92">eh</ta>
            <ta e="T94" id="Seg_4356" s="T93">many</ta>
            <ta e="T95" id="Seg_4357" s="T94">human.being.[NOM]</ta>
            <ta e="T96" id="Seg_4358" s="T95">gather-PASS/REFL-EP-PST2-3SG</ta>
            <ta e="T97" id="Seg_4359" s="T96">be.happy-NMNZ-ACC</ta>
            <ta e="T98" id="Seg_4360" s="T97">bad-ACC</ta>
            <ta e="T99" id="Seg_4361" s="T98">eh</ta>
            <ta e="T100" id="Seg_4362" s="T99">life.[NOM]</ta>
            <ta e="T101" id="Seg_4363" s="T100">side-3SG-INSTR</ta>
            <ta e="T102" id="Seg_4364" s="T101">chat-NMNZ.[NOM]</ta>
            <ta e="T103" id="Seg_4365" s="T102">there.is</ta>
            <ta e="T104" id="Seg_4366" s="T103">be-PST1-3SG</ta>
            <ta e="T105" id="Seg_4367" s="T104">child-PL.[NOM]</ta>
            <ta e="T106" id="Seg_4368" s="T105">learn-PTCP.PRS-3PL-GEN</ta>
            <ta e="T107" id="Seg_4369" s="T106">side-3PL-INSTR</ta>
            <ta e="T108" id="Seg_4370" s="T107">student-PL.[NOM]</ta>
            <ta e="T109" id="Seg_4371" s="T108">eh</ta>
            <ta e="T110" id="Seg_4372" s="T109">that</ta>
            <ta e="T111" id="Seg_4373" s="T110">big</ta>
            <ta e="T112" id="Seg_4374" s="T111">college-PL.[NOM]</ta>
            <ta e="T113" id="Seg_4375" s="T112">side-3PL-DAT/LOC</ta>
            <ta e="T114" id="Seg_4376" s="T113">why</ta>
            <ta e="T115" id="Seg_4377" s="T114">throw-PTCP.PRS-3PL-ACC</ta>
            <ta e="T116" id="Seg_4378" s="T115">or</ta>
            <ta e="T117" id="Seg_4379" s="T116">why</ta>
            <ta e="T118" id="Seg_4380" s="T117">learn-NEG.PTCP-3PL-ACC</ta>
            <ta e="T119" id="Seg_4381" s="T118">that</ta>
            <ta e="T120" id="Seg_4382" s="T119">that</ta>
            <ta e="T121" id="Seg_4383" s="T120">side-3SG-DAT/LOC</ta>
            <ta e="T122" id="Seg_4384" s="T121">eh</ta>
            <ta e="T123" id="Seg_4385" s="T122">big</ta>
            <ta e="T124" id="Seg_4386" s="T123">then</ta>
            <ta e="T125" id="Seg_4387" s="T124">question.[NOM]</ta>
            <ta e="T126" id="Seg_4388" s="T125">stand-PTCP.PRS</ta>
            <ta e="T127" id="Seg_4389" s="T126">be-PST1-3SG</ta>
            <ta e="T128" id="Seg_4390" s="T127">then</ta>
            <ta e="T129" id="Seg_4391" s="T128">MOD</ta>
            <ta e="T131" id="Seg_4392" s="T130">eh</ta>
            <ta e="T132" id="Seg_4393" s="T131">old.woman.[NOM]</ta>
            <ta e="T133" id="Seg_4394" s="T132">perform-VBZ-PST2.[3SG]</ta>
            <ta e="T134" id="Seg_4395" s="T133">there</ta>
            <ta e="T135" id="Seg_4396" s="T134">Moldanova</ta>
            <ta e="T136" id="Seg_4397" s="T135">say-CVB.SEQ</ta>
            <ta e="T137" id="Seg_4398" s="T136">Mariya</ta>
            <ta e="T138" id="Seg_4399" s="T137">Kuzminichna</ta>
            <ta e="T139" id="Seg_4400" s="T138">mind-PROPR</ta>
            <ta e="T140" id="Seg_4401" s="T139">woman.[NOM]</ta>
            <ta e="T141" id="Seg_4402" s="T140">that.[NOM]</ta>
            <ta e="T142" id="Seg_4403" s="T141">speak-PST2-3SG</ta>
            <ta e="T143" id="Seg_4404" s="T142">eh</ta>
            <ta e="T144" id="Seg_4405" s="T143">young</ta>
            <ta e="T145" id="Seg_4406" s="T144">human.being-PL.[NOM]</ta>
            <ta e="T146" id="Seg_4407" s="T145">eh</ta>
            <ta e="T147" id="Seg_4408" s="T146">young</ta>
            <ta e="T148" id="Seg_4409" s="T147">human.being-PL.[NOM]</ta>
            <ta e="T149" id="Seg_4410" s="T148">self-3PL-GEN</ta>
            <ta e="T150" id="Seg_4411" s="T149">family-3PL-DAT/LOC</ta>
            <ta e="T151" id="Seg_4412" s="T150">self-3PL-GEN</ta>
            <ta e="T152" id="Seg_4413" s="T151">family-3PL-GEN</ta>
            <ta e="T153" id="Seg_4414" s="T152">inside-3PL-DAT/LOC</ta>
            <ta e="T154" id="Seg_4415" s="T153">inside-3PL-ABL</ta>
            <ta e="T155" id="Seg_4416" s="T154">education.[NOM]</ta>
            <ta e="T156" id="Seg_4417" s="T155">be-PRS-NEC.[3SG]</ta>
            <ta e="T157" id="Seg_4418" s="T156">give.birth-EP-CAUS-PTCP.PST</ta>
            <ta e="T158" id="Seg_4419" s="T157">human.being.[NOM]</ta>
            <ta e="T159" id="Seg_4420" s="T158">give-PRS-NEC.[3SG]</ta>
            <ta e="T160" id="Seg_4421" s="T159">education-ACC</ta>
            <ta e="T161" id="Seg_4422" s="T160">good-ACC</ta>
            <ta e="T162" id="Seg_4423" s="T161">bad-ACC</ta>
            <ta e="T163" id="Seg_4424" s="T162">child-DAT/LOC</ta>
            <ta e="T164" id="Seg_4425" s="T163">ah</ta>
            <ta e="T165" id="Seg_4426" s="T164">school.[NOM]</ta>
            <ta e="T166" id="Seg_4427" s="T165">kindergarten.[NOM]</ta>
            <ta e="T167" id="Seg_4428" s="T166">that-DAT/LOC</ta>
            <ta e="T168" id="Seg_4429" s="T167">MOD</ta>
            <ta e="T169" id="Seg_4430" s="T168">eh</ta>
            <ta e="T170" id="Seg_4431" s="T169">educational.institution-PL.[NOM]</ta>
            <ta e="T171" id="Seg_4432" s="T170">that-DAT/LOC</ta>
            <ta e="T172" id="Seg_4433" s="T171">help.[NOM]</ta>
            <ta e="T173" id="Seg_4434" s="T172">just</ta>
            <ta e="T174" id="Seg_4435" s="T173">be-PTCP.FUT-3PL-ACC</ta>
            <ta e="T175" id="Seg_4436" s="T174">need.to</ta>
            <ta e="T176" id="Seg_4437" s="T175">and</ta>
            <ta e="T180" id="Seg_4438" s="T179">similar</ta>
            <ta e="T181" id="Seg_4439" s="T180">be-PTCP.FUT-3PL-ACC</ta>
            <ta e="T182" id="Seg_4440" s="T181">that.[NOM]</ta>
            <ta e="T183" id="Seg_4441" s="T182">because.of</ta>
            <ta e="T184" id="Seg_4442" s="T183">MOD</ta>
            <ta e="T185" id="Seg_4443" s="T184">now</ta>
            <ta e="T186" id="Seg_4444" s="T185">front.[NOM]</ta>
            <ta e="T187" id="Seg_4445" s="T186">to</ta>
            <ta e="T188" id="Seg_4446" s="T187">eh</ta>
            <ta e="T189" id="Seg_4447" s="T188">grow-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T190" id="Seg_4448" s="T189">need.to</ta>
            <ta e="T191" id="Seg_4449" s="T190">say-PRS.[3SG]</ta>
            <ta e="T192" id="Seg_4450" s="T191">eh</ta>
            <ta e="T193" id="Seg_4451" s="T192">that</ta>
            <ta e="T194" id="Seg_4452" s="T193">woman.[NOM]</ta>
            <ta e="T195" id="Seg_4453" s="T194">responsibility-ACC</ta>
            <ta e="T196" id="Seg_4454" s="T195">give.birth-EP-CAUS-PTCP.PRS</ta>
            <ta e="T197" id="Seg_4455" s="T196">human.being-PL-ABL</ta>
            <ta e="T198" id="Seg_4456" s="T197">parent-PL-ABL</ta>
            <ta e="T199" id="Seg_4457" s="T198">and</ta>
            <ta e="T200" id="Seg_4458" s="T199">and</ta>
            <ta e="T201" id="Seg_4459" s="T200">good-ACC</ta>
            <ta e="T202" id="Seg_4460" s="T201">become.more-CAUS-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T203" id="Seg_4461" s="T202">say-CVB.SEQ</ta>
            <ta e="T204" id="Seg_4462" s="T203">speak-PST2-3SG</ta>
            <ta e="T205" id="Seg_4463" s="T204">that</ta>
            <ta e="T206" id="Seg_4464" s="T205">woman.[NOM]</ta>
            <ta e="T207" id="Seg_4465" s="T206">good-ACC</ta>
            <ta e="T208" id="Seg_4466" s="T207">become.more-CAUS-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T209" id="Seg_4467" s="T208">and</ta>
            <ta e="T210" id="Seg_4468" s="T209">bad-ACC</ta>
            <ta e="T211" id="Seg_4469" s="T210">decrease-CAUS-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T212" id="Seg_4470" s="T211">this</ta>
            <ta e="T213" id="Seg_4471" s="T212">earth-DAT/LOC</ta>
            <ta e="T214" id="Seg_4472" s="T213">well</ta>
            <ta e="T215" id="Seg_4473" s="T214">so</ta>
            <ta e="T216" id="Seg_4474" s="T215">ascertain-CVB.SEQ</ta>
            <ta e="T217" id="Seg_4475" s="T216">see-PTCP.COND-DAT/LOC</ta>
            <ta e="T218" id="Seg_4476" s="T217">if</ta>
            <ta e="T219" id="Seg_4477" s="T218">2SG-ACC</ta>
            <ta e="T220" id="Seg_4478" s="T219">bad-ADVZ</ta>
            <ta e="T221" id="Seg_4479" s="T220">speak-PST1-3PL</ta>
            <ta e="T222" id="Seg_4480" s="T221">bad-ACC</ta>
            <ta e="T223" id="Seg_4481" s="T222">make-PST1-3PL</ta>
            <ta e="T224" id="Seg_4482" s="T223">Q</ta>
            <ta e="T225" id="Seg_4483" s="T224">2SG.[NOM]</ta>
            <ta e="T226" id="Seg_4484" s="T225">EMPH</ta>
            <ta e="T227" id="Seg_4485" s="T226">tell-NEG-PTCP.FUT-2SG-ACC</ta>
            <ta e="T228" id="Seg_4486" s="T227">need.to</ta>
            <ta e="T229" id="Seg_4487" s="T228">that</ta>
            <ta e="T230" id="Seg_4488" s="T229">human.being.[NOM]</ta>
            <ta e="T231" id="Seg_4489" s="T230">side-3SG-INSTR</ta>
            <ta e="T232" id="Seg_4490" s="T231">say-PRS.[3SG]</ta>
            <ta e="T233" id="Seg_4491" s="T232">that</ta>
            <ta e="T234" id="Seg_4492" s="T233">woman.[NOM]</ta>
            <ta e="T235" id="Seg_4493" s="T234">that</ta>
            <ta e="T236" id="Seg_4494" s="T235">human.being.[NOM]</ta>
            <ta e="T237" id="Seg_4495" s="T236">side-3SG</ta>
            <ta e="T238" id="Seg_4496" s="T237">that</ta>
            <ta e="T239" id="Seg_4497" s="T238">human.being.[NOM]</ta>
            <ta e="T240" id="Seg_4498" s="T239">side-3SG-INSTR</ta>
            <ta e="T241" id="Seg_4499" s="T240">self-3SG-DAT/LOC</ta>
            <ta e="T242" id="Seg_4500" s="T241">speak-EP-NEG-PTCP.FUT-2SG-ACC</ta>
            <ta e="T243" id="Seg_4501" s="T242">need.to</ta>
            <ta e="T244" id="Seg_4502" s="T243">but</ta>
            <ta e="T245" id="Seg_4503" s="T244">human.being-PL-DAT/LOC</ta>
            <ta e="T246" id="Seg_4504" s="T245">also</ta>
            <ta e="T247" id="Seg_4505" s="T246">tell-NEG-PTCP.FUT-DAT/LOC</ta>
            <ta e="T248" id="Seg_4506" s="T247">need.to</ta>
            <ta e="T249" id="Seg_4507" s="T248">in.order</ta>
            <ta e="T250" id="Seg_4508" s="T249">bad.[NOM]</ta>
            <ta e="T251" id="Seg_4509" s="T250">grow-EP-NEG-PTCP.FUT-3SG-ACC</ta>
            <ta e="T252" id="Seg_4510" s="T251">and</ta>
            <ta e="T253" id="Seg_4511" s="T252">good-ADVZ</ta>
            <ta e="T254" id="Seg_4512" s="T253">make-PTCP.PST</ta>
            <ta e="T255" id="Seg_4513" s="T254">human.being-DAT/LOC</ta>
            <ta e="T256" id="Seg_4514" s="T255">MOD</ta>
            <ta e="T257" id="Seg_4515" s="T256">that</ta>
            <ta e="T258" id="Seg_4516" s="T257">human.being-DAT/LOC</ta>
            <ta e="T260" id="Seg_4517" s="T259">also</ta>
            <ta e="T261" id="Seg_4518" s="T260">self-3SG-DAT/LOC</ta>
            <ta e="T262" id="Seg_4519" s="T261">speak-EP-NEG-PTCP.FUT-2SG-ACC</ta>
            <ta e="T263" id="Seg_4520" s="T262">in.order</ta>
            <ta e="T264" id="Seg_4521" s="T263">brag-NEG-PTCP.FUT-3SG-ACC</ta>
            <ta e="T265" id="Seg_4522" s="T264">and</ta>
            <ta e="T266" id="Seg_4523" s="T265">different</ta>
            <ta e="T267" id="Seg_4524" s="T266">human.being-PL-DAT/LOC</ta>
            <ta e="T268" id="Seg_4525" s="T267">tell-PTCP.FUT-2SG-ACC</ta>
            <ta e="T269" id="Seg_4526" s="T268">need.to</ta>
            <ta e="T270" id="Seg_4527" s="T269">that</ta>
            <ta e="T271" id="Seg_4528" s="T270">that</ta>
            <ta e="T272" id="Seg_4529" s="T271">human.being.[NOM]</ta>
            <ta e="T273" id="Seg_4530" s="T272">good-ACC</ta>
            <ta e="T274" id="Seg_4531" s="T273">make-PST2-3SG</ta>
            <ta e="T275" id="Seg_4532" s="T274">say-CVB.SEQ</ta>
            <ta e="T276" id="Seg_4533" s="T275">that</ta>
            <ta e="T277" id="Seg_4534" s="T276">human.being.[NOM]</ta>
            <ta e="T278" id="Seg_4535" s="T277">such.[NOM]</ta>
            <ta e="T279" id="Seg_4536" s="T278">say-CVB.SEQ</ta>
            <ta e="T280" id="Seg_4537" s="T279">that</ta>
            <ta e="T281" id="Seg_4538" s="T280">human.being.[NOM]</ta>
            <ta e="T282" id="Seg_4539" s="T281">such.[NOM]</ta>
            <ta e="T283" id="Seg_4540" s="T282">say-CVB.SEQ</ta>
            <ta e="T285" id="Seg_4541" s="T284">then</ta>
            <ta e="T286" id="Seg_4542" s="T285">MOD</ta>
            <ta e="T287" id="Seg_4543" s="T286">good.[NOM]</ta>
            <ta e="T288" id="Seg_4544" s="T287">grow-PRS.[3SG]</ta>
            <ta e="T289" id="Seg_4545" s="T288">good.[NOM]</ta>
            <ta e="T290" id="Seg_4546" s="T289">grow-PRS.[3SG]</ta>
            <ta e="T291" id="Seg_4547" s="T290">such</ta>
            <ta e="T292" id="Seg_4548" s="T291">formula-INSTR</ta>
            <ta e="T293" id="Seg_4549" s="T292">say-CVB.SEQ</ta>
            <ta e="T294" id="Seg_4550" s="T293">but</ta>
            <ta e="T295" id="Seg_4551" s="T294">again</ta>
            <ta e="T296" id="Seg_4552" s="T295">so</ta>
            <ta e="T297" id="Seg_4553" s="T296">and</ta>
            <ta e="T298" id="Seg_4554" s="T297">that</ta>
            <ta e="T299" id="Seg_4555" s="T298">formula-DAT/LOC</ta>
            <ta e="T300" id="Seg_4556" s="T299">1SG.[NOM]</ta>
            <ta e="T301" id="Seg_4557" s="T300">agreement-VBZ-NEG-1SG</ta>
            <ta e="T302" id="Seg_4558" s="T301">why</ta>
            <ta e="T303" id="Seg_4559" s="T302">say-CVB.SEQ-2PL</ta>
            <ta e="T304" id="Seg_4560" s="T303">bad.[NOM]</ta>
            <ta e="T305" id="Seg_4561" s="T304">two-ORD</ta>
            <ta e="T306" id="Seg_4562" s="T305">formula.[NOM]</ta>
            <ta e="T307" id="Seg_4563" s="T306">there.is</ta>
            <ta e="T308" id="Seg_4564" s="T307">that</ta>
            <ta e="T309" id="Seg_4565" s="T308">upper.part-3SG-DAT/LOC</ta>
            <ta e="T310" id="Seg_4566" s="T309">then</ta>
            <ta e="T311" id="Seg_4567" s="T310">bad.[NOM]</ta>
            <ta e="T312" id="Seg_4568" s="T311">grow-EP-NEG-PTCP.FUT-3SG-ACC</ta>
            <ta e="T313" id="Seg_4569" s="T312">bad-ACC</ta>
            <ta e="T314" id="Seg_4570" s="T313">front.[NOM]</ta>
            <ta e="T315" id="Seg_4571" s="T314">to</ta>
            <ta e="T316" id="Seg_4572" s="T315">back</ta>
            <ta e="T317" id="Seg_4573" s="T316">big-ACC</ta>
            <ta e="T318" id="Seg_4574" s="T317">answer.[NOM]</ta>
            <ta e="T319" id="Seg_4575" s="T318">give-PTCP.FUT-DAT/LOC</ta>
            <ta e="T320" id="Seg_4576" s="T319">need.to</ta>
            <ta e="T321" id="Seg_4577" s="T320">3SG-DAT/LOC</ta>
            <ta e="T322" id="Seg_4578" s="T321">in.order</ta>
            <ta e="T323" id="Seg_4579" s="T322">bad-ACC</ta>
            <ta e="T324" id="Seg_4580" s="T323">make-PTCP.PST</ta>
            <ta e="T325" id="Seg_4581" s="T324">human.being.[NOM]</ta>
            <ta e="T326" id="Seg_4582" s="T325">know-PTCP.FUT-3SG-ACC</ta>
            <ta e="T327" id="Seg_4583" s="T326">that</ta>
            <ta e="T328" id="Seg_4584" s="T327">simply</ta>
            <ta e="T329" id="Seg_4585" s="T328">human.being-ACC</ta>
            <ta e="T330" id="Seg_4586" s="T329">his</ta>
            <ta e="T331" id="Seg_4587" s="T330">bad-3SG.[NOM]</ta>
            <ta e="T332" id="Seg_4588" s="T331">simply</ta>
            <ta e="T333" id="Seg_4589" s="T332">pass.by-FUT.[3SG]</ta>
            <ta e="T334" id="Seg_4590" s="T333">NEG-3SG</ta>
            <ta e="T335" id="Seg_4591" s="T334">3SG-DAT/LOC</ta>
            <ta e="T336" id="Seg_4592" s="T335">3SG.[NOM]</ta>
            <ta e="T337" id="Seg_4593" s="T336">MOD</ta>
            <ta e="T338" id="Seg_4594" s="T337">human.being.[NOM]</ta>
            <ta e="T339" id="Seg_4595" s="T338">front-3SG-DAT/LOC</ta>
            <ta e="T340" id="Seg_4596" s="T339">responsibility-VBZ-PRS-NEC.[3SG]</ta>
            <ta e="T341" id="Seg_4597" s="T340">in.order</ta>
            <ta e="T342" id="Seg_4598" s="T341">front.[NOM]</ta>
            <ta e="T343" id="Seg_4599" s="T342">to</ta>
            <ta e="T344" id="Seg_4600" s="T343">bad-ACC</ta>
            <ta e="T345" id="Seg_4601" s="T344">make-EP-NEG-PTCP.FUT-3SG-ACC</ta>
            <ta e="T346" id="Seg_4602" s="T345">then</ta>
            <ta e="T347" id="Seg_4603" s="T346">MOD</ta>
            <ta e="T348" id="Seg_4604" s="T347">good.[NOM]</ta>
            <ta e="T349" id="Seg_4605" s="T348">two-MLTP</ta>
            <ta e="T350" id="Seg_4606" s="T349">time.[NOM]</ta>
            <ta e="T351" id="Seg_4607" s="T350">grow-CVB.SEQ</ta>
            <ta e="T352" id="Seg_4608" s="T351">go.out-FUT-3SG</ta>
            <ta e="T353" id="Seg_4609" s="T352">and</ta>
            <ta e="T354" id="Seg_4610" s="T353">live-PTCP.HAB</ta>
            <ta e="T355" id="Seg_4611" s="T354">human.being-PL.[NOM]</ta>
            <ta e="T356" id="Seg_4612" s="T355">there</ta>
            <ta e="T357" id="Seg_4613" s="T356">live-PTCP.HAB</ta>
            <ta e="T358" id="Seg_4614" s="T357">human.being-PL.[NOM]</ta>
            <ta e="T359" id="Seg_4615" s="T358">self-3PL-GEN</ta>
            <ta e="T360" id="Seg_4616" s="T359">front.[NOM]</ta>
            <ta e="T361" id="Seg_4617" s="T360">to</ta>
            <ta e="T362" id="Seg_4618" s="T361">self-3PL-GEN</ta>
            <ta e="T363" id="Seg_4619" s="T362">front.[NOM]</ta>
            <ta e="T364" id="Seg_4620" s="T363">to</ta>
            <ta e="T365" id="Seg_4621" s="T364">offend-EP-CAUS-FUT-3PL</ta>
            <ta e="T366" id="Seg_4622" s="T365">NEG-3PL</ta>
            <ta e="T367" id="Seg_4623" s="T366">NEG</ta>
            <ta e="T368" id="Seg_4624" s="T367">offend-EP-CAUS-FUT-3PL</ta>
            <ta e="T369" id="Seg_4625" s="T368">NEG-3PL</ta>
            <ta e="T370" id="Seg_4626" s="T369">ah</ta>
            <ta e="T371" id="Seg_4627" s="T370">but</ta>
            <ta e="T372" id="Seg_4628" s="T371">then</ta>
            <ta e="T373" id="Seg_4629" s="T372">that</ta>
            <ta e="T374" id="Seg_4630" s="T373">that</ta>
            <ta e="T375" id="Seg_4631" s="T374">round</ta>
            <ta e="T376" id="Seg_4632" s="T375">round</ta>
            <ta e="T377" id="Seg_4633" s="T376">table.[NOM]</ta>
            <ta e="T378" id="Seg_4634" s="T377">stop-PTCP.PST-3SG-ACC</ta>
            <ta e="T379" id="Seg_4635" s="T378">after</ta>
            <ta e="T380" id="Seg_4636" s="T379">also</ta>
            <ta e="T381" id="Seg_4637" s="T380">concert.[NOM]</ta>
            <ta e="T384" id="Seg_4638" s="T383">say-CVB.SEQ</ta>
            <ta e="T385" id="Seg_4639" s="T384">be-PST2-3SG</ta>
            <ta e="T386" id="Seg_4640" s="T385">there</ta>
            <ta e="T387" id="Seg_4641" s="T386">very</ta>
            <ta e="T388" id="Seg_4642" s="T387">good-ADVZ</ta>
            <ta e="T389" id="Seg_4643" s="T388">perform-VBZ-PST2-3PL</ta>
            <ta e="T390" id="Seg_4644" s="T389">many</ta>
            <ta e="T391" id="Seg_4645" s="T390">collective.[NOM]</ta>
            <ta e="T392" id="Seg_4646" s="T391">come-PST2-3SG</ta>
            <ta e="T393" id="Seg_4647" s="T392">eh</ta>
            <ta e="T394" id="Seg_4648" s="T393">Khanty_Mansiysk-ABL</ta>
            <ta e="T395" id="Seg_4649" s="T394">self-3SG-ABL</ta>
            <ta e="T396" id="Seg_4650" s="T395">theater</ta>
            <ta e="T397" id="Seg_4651" s="T396">group.[NOM]</ta>
            <ta e="T398" id="Seg_4652" s="T397">there.is</ta>
            <ta e="T399" id="Seg_4653" s="T398">be-PST1-3SG</ta>
            <ta e="T400" id="Seg_4654" s="T399">our</ta>
            <ta e="T401" id="Seg_4655" s="T400">ensemble.[NOM]</ta>
            <ta e="T402" id="Seg_4656" s="T401">dolgan.dance-1PL.[NOM]</ta>
            <ta e="T403" id="Seg_4657" s="T402">similar</ta>
            <ta e="T404" id="Seg_4658" s="T403">then</ta>
            <ta e="T405" id="Seg_4659" s="T404">MOD</ta>
            <ta e="T406" id="Seg_4660" s="T405">Magadan-ABL</ta>
            <ta e="T407" id="Seg_4661" s="T406">come-PST2-3PL</ta>
            <ta e="T408" id="Seg_4662" s="T407">eh</ta>
            <ta e="T409" id="Seg_4663" s="T408">Yamalo_Nenets.Autonomous.Okrug-ABL</ta>
            <ta e="T410" id="Seg_4664" s="T409">Chukotka-ABL</ta>
            <ta e="T411" id="Seg_4665" s="T410">ensemble.[NOM]</ta>
            <ta e="T412" id="Seg_4666" s="T411">there.is</ta>
            <ta e="T413" id="Seg_4667" s="T412">be-PST1-3SG</ta>
            <ta e="T414" id="Seg_4668" s="T413">distant</ta>
            <ta e="T415" id="Seg_4669" s="T414">place-ABL</ta>
            <ta e="T416" id="Seg_4670" s="T415">come-FREQ-PST2-3PL</ta>
            <ta e="T417" id="Seg_4671" s="T416">then</ta>
            <ta e="T418" id="Seg_4672" s="T417">many-many</ta>
            <ta e="T419" id="Seg_4673" s="T418">place-ABL</ta>
            <ta e="T420" id="Seg_4674" s="T419">come-FREQ-PST2-3PL</ta>
            <ta e="T421" id="Seg_4675" s="T420">thither</ta>
            <ta e="T422" id="Seg_4676" s="T421">twenty</ta>
            <ta e="T423" id="Seg_4677" s="T422">two</ta>
            <ta e="T424" id="Seg_4678" s="T423">collective.[NOM]</ta>
            <ta e="T425" id="Seg_4679" s="T424">there.is</ta>
            <ta e="T426" id="Seg_4680" s="T425">be-PST1-3SG</ta>
            <ta e="T427" id="Seg_4681" s="T426">eh</ta>
            <ta e="T428" id="Seg_4682" s="T427">then</ta>
            <ta e="T429" id="Seg_4683" s="T428">curator-PL-1PL.[NOM]</ta>
            <ta e="T430" id="Seg_4684" s="T429">good</ta>
            <ta e="T431" id="Seg_4685" s="T430">very</ta>
            <ta e="T432" id="Seg_4686" s="T431">woman-PL.[NOM]</ta>
            <ta e="T433" id="Seg_4687" s="T432">be-PST1-3PL</ta>
            <ta e="T434" id="Seg_4688" s="T433">1PL-ACC</ta>
            <ta e="T435" id="Seg_4689" s="T434">whole-3SG-ACC</ta>
            <ta e="T436" id="Seg_4690" s="T435">eh</ta>
            <ta e="T437" id="Seg_4691" s="T436">take.away-CVB.SEQ-3PL</ta>
            <ta e="T438" id="Seg_4692" s="T437">bring-CVB.SEQ-3PL</ta>
            <ta e="T439" id="Seg_4693" s="T438">small</ta>
            <ta e="T440" id="Seg_4694" s="T439">child-SIM</ta>
            <ta e="T441" id="Seg_4695" s="T440">sleep-CAUS-CVB.SEQ-3PL</ta>
            <ta e="T442" id="Seg_4696" s="T441">then</ta>
            <ta e="T443" id="Seg_4697" s="T442">eat-EP-CAUS-CVB.SEQ-3PL</ta>
            <ta e="T444" id="Seg_4698" s="T443">then</ta>
            <ta e="T445" id="Seg_4699" s="T444">museum-PL.[NOM]</ta>
            <ta e="T446" id="Seg_4700" s="T445">through</ta>
            <ta e="T447" id="Seg_4701" s="T446">motor.ship-DAT/LOC</ta>
            <ta e="T448" id="Seg_4702" s="T447">go-EP-PST2-1PL</ta>
            <ta e="T449" id="Seg_4703" s="T448">Irtysh</ta>
            <ta e="T450" id="Seg_4704" s="T449">say-CVB.SEQ</ta>
            <ta e="T451" id="Seg_4705" s="T450">eh</ta>
            <ta e="T452" id="Seg_4706" s="T451">river.[NOM]</ta>
            <ta e="T453" id="Seg_4707" s="T452">Irtysh</ta>
            <ta e="T454" id="Seg_4708" s="T453">say-CVB.SEQ</ta>
            <ta e="T455" id="Seg_4709" s="T454">river.[NOM]</ta>
            <ta e="T456" id="Seg_4710" s="T455">along</ta>
            <ta e="T457" id="Seg_4711" s="T456">go-PST2-1PL</ta>
            <ta e="T458" id="Seg_4712" s="T457">that</ta>
            <ta e="T459" id="Seg_4713" s="T458">Irtysh</ta>
            <ta e="T460" id="Seg_4714" s="T459">MOD</ta>
            <ta e="T461" id="Seg_4715" s="T460">big</ta>
            <ta e="T462" id="Seg_4716" s="T461">river-DAT/LOC</ta>
            <ta e="T463" id="Seg_4717" s="T462">unite-PRS.[3SG]</ta>
            <ta e="T464" id="Seg_4718" s="T463">Ob</ta>
            <ta e="T465" id="Seg_4719" s="T464">say-CVB.SEQ</ta>
            <ta e="T466" id="Seg_4720" s="T465">river-DAT/LOC</ta>
            <ta e="T467" id="Seg_4721" s="T466">that</ta>
            <ta e="T468" id="Seg_4722" s="T467">unite-EP-PTCP.PST-PL-DAT/LOC</ta>
            <ta e="T469" id="Seg_4723" s="T468">present-VBZ-PST2-1PL</ta>
            <ta e="T471" id="Seg_4724" s="T470">song-PL-ACC</ta>
            <ta e="T472" id="Seg_4725" s="T471">sing-PST2-1PL</ta>
            <ta e="T473" id="Seg_4726" s="T472">come-CVB.PURP-1PL</ta>
            <ta e="T474" id="Seg_4727" s="T473">go-CVB.PURP-1PL</ta>
            <ta e="T475" id="Seg_4728" s="T474">happy.[NOM]</ta>
            <ta e="T476" id="Seg_4729" s="T475">very</ta>
            <ta e="T477" id="Seg_4730" s="T476">be-PST1-3SG</ta>
            <ta e="T478" id="Seg_4731" s="T477">cinema-DAT/LOC</ta>
            <ta e="T479" id="Seg_4732" s="T478">take.away-PST2-3PL</ta>
            <ta e="T480" id="Seg_4733" s="T479">1PL-ACC</ta>
            <ta e="T481" id="Seg_4734" s="T480">then</ta>
            <ta e="T482" id="Seg_4735" s="T481">then</ta>
            <ta e="T483" id="Seg_4736" s="T482">back</ta>
            <ta e="T484" id="Seg_4737" s="T483">come-CVB.SEQ-1PL</ta>
            <ta e="T485" id="Seg_4738" s="T484">eh</ta>
            <ta e="T486" id="Seg_4739" s="T485">that</ta>
            <ta e="T487" id="Seg_4740" s="T486">next.morning-3SG-ACC</ta>
            <ta e="T488" id="Seg_4741" s="T487">that</ta>
            <ta e="T489" id="Seg_4742" s="T488">three</ta>
            <ta e="T490" id="Seg_4743" s="T489">day-ACC</ta>
            <ta e="T491" id="Seg_4744" s="T490">become-PST2-1PL</ta>
            <ta e="T492" id="Seg_4745" s="T491">eh</ta>
            <ta e="T493" id="Seg_4746" s="T492">three</ta>
            <ta e="T494" id="Seg_4747" s="T493">day-ACC</ta>
            <ta e="T495" id="Seg_4748" s="T494">be-HAB.[3SG]</ta>
            <ta e="T496" id="Seg_4749" s="T495">eh</ta>
            <ta e="T497" id="Seg_4750" s="T496">three</ta>
            <ta e="T498" id="Seg_4751" s="T497">day-ACC</ta>
            <ta e="T499" id="Seg_4752" s="T498">be-PST2-1PL-DAT/LOC</ta>
            <ta e="T500" id="Seg_4753" s="T499">Khanty_Mansiysk-DAT/LOC</ta>
            <ta e="T501" id="Seg_4754" s="T500">good.[NOM]</ta>
            <ta e="T502" id="Seg_4755" s="T501">very</ta>
            <ta e="T503" id="Seg_4756" s="T502">day-PL.[NOM]</ta>
            <ta e="T504" id="Seg_4757" s="T503">be-PST1-3PL</ta>
            <ta e="T505" id="Seg_4758" s="T504">weather.[NOM]</ta>
            <ta e="T506" id="Seg_4759" s="T505">that.[NOM]</ta>
            <ta e="T507" id="Seg_4760" s="T506">front-3SG-DAT/LOC</ta>
            <ta e="T508" id="Seg_4761" s="T507">rain-VBZ-PTCP.PRS</ta>
            <ta e="T509" id="Seg_4762" s="T508">be-PST1-3SG</ta>
            <ta e="T510" id="Seg_4763" s="T509">say-PTCP.PRS</ta>
            <ta e="T511" id="Seg_4764" s="T510">be-PST1-3PL</ta>
            <ta e="T512" id="Seg_4765" s="T511">then</ta>
            <ta e="T513" id="Seg_4766" s="T512">twenty-ABL</ta>
            <ta e="T514" id="Seg_4767" s="T513">over</ta>
            <ta e="T515" id="Seg_4768" s="T514">degree.[NOM]</ta>
            <ta e="T516" id="Seg_4769" s="T515">be-PST1-3SG</ta>
            <ta e="T517" id="Seg_4770" s="T516">twenty</ta>
            <ta e="T518" id="Seg_4771" s="T517">four</ta>
            <ta e="T519" id="Seg_4772" s="T518">twenty</ta>
            <ta e="T520" id="Seg_4773" s="T519">five</ta>
            <ta e="T521" id="Seg_4774" s="T520">degree.[NOM]</ta>
            <ta e="T522" id="Seg_4775" s="T521">be-CVB.SEQ</ta>
            <ta e="T523" id="Seg_4776" s="T522">go-PST1-3SG</ta>
            <ta e="T524" id="Seg_4777" s="T523">that</ta>
            <ta e="T525" id="Seg_4778" s="T524">day-PL-DAT/LOC</ta>
            <ta e="T526" id="Seg_4779" s="T525">that.[NOM]</ta>
            <ta e="T527" id="Seg_4780" s="T526">because.of</ta>
            <ta e="T528" id="Seg_4781" s="T527">MOD</ta>
            <ta e="T529" id="Seg_4782" s="T528">1PL-DAT/LOC</ta>
            <ta e="T530" id="Seg_4783" s="T529">very</ta>
            <ta e="T531" id="Seg_4784" s="T530">NEG</ta>
            <ta e="T532" id="Seg_4785" s="T531">warm-POSS</ta>
            <ta e="T533" id="Seg_4786" s="T532">NEG</ta>
            <ta e="T534" id="Seg_4787" s="T533">be-PST1-3SG</ta>
            <ta e="T535" id="Seg_4788" s="T534">but</ta>
            <ta e="T536" id="Seg_4789" s="T535">very</ta>
            <ta e="T537" id="Seg_4790" s="T536">NEG</ta>
            <ta e="T538" id="Seg_4791" s="T537">and</ta>
            <ta e="T539" id="Seg_4792" s="T538">cold-3SG</ta>
            <ta e="T540" id="Seg_4793" s="T539">NEG</ta>
            <ta e="T541" id="Seg_4794" s="T540">be-PST1-3SG</ta>
            <ta e="T542" id="Seg_4795" s="T541">that.[NOM]</ta>
            <ta e="T543" id="Seg_4796" s="T542">because.of</ta>
            <ta e="T544" id="Seg_4797" s="T543">very</ta>
            <ta e="T545" id="Seg_4798" s="T544">good.[NOM]</ta>
            <ta e="T546" id="Seg_4799" s="T545">1PL-DAT/LOC</ta>
            <ta e="T547" id="Seg_4800" s="T546">1SG.[NOM]</ta>
            <ta e="T548" id="Seg_4801" s="T547">thought-1SG-DAT/LOC</ta>
            <ta e="T549" id="Seg_4802" s="T548">such</ta>
            <ta e="T550" id="Seg_4803" s="T549">climate.[NOM]</ta>
            <ta e="T551" id="Seg_4804" s="T550">3PL-DAT/LOC</ta>
            <ta e="T552" id="Seg_4805" s="T551">such</ta>
            <ta e="T553" id="Seg_4806" s="T552">weather.[NOM]</ta>
            <ta e="T554" id="Seg_4807" s="T553">very</ta>
            <ta e="T555" id="Seg_4808" s="T554">good-ADVZ</ta>
            <ta e="T556" id="Seg_4809" s="T555">work-PRS.[3SG]</ta>
            <ta e="T557" id="Seg_4810" s="T556">that.[NOM]</ta>
            <ta e="T558" id="Seg_4811" s="T557">because.of</ta>
            <ta e="T559" id="Seg_4812" s="T558">still</ta>
            <ta e="T560" id="Seg_4813" s="T559">and</ta>
            <ta e="T561" id="Seg_4814" s="T560">invite-TEMP-3PL</ta>
            <ta e="T562" id="Seg_4815" s="T561">1SG.[NOM]</ta>
            <ta e="T563" id="Seg_4816" s="T562">thought-1SG-DAT/LOC</ta>
            <ta e="T564" id="Seg_4817" s="T563">1SG.[NOM]</ta>
            <ta e="T565" id="Seg_4818" s="T564">EMPH</ta>
            <ta e="T566" id="Seg_4819" s="T565">go-PTCP.PRS</ta>
            <ta e="T567" id="Seg_4820" s="T566">like-1SG</ta>
            <ta e="T568" id="Seg_4821" s="T567">still</ta>
            <ta e="T569" id="Seg_4822" s="T568">once</ta>
            <ta e="T570" id="Seg_4823" s="T569">visit-CVB.SIM</ta>
         </annotation>
         <annotation name="gg" tierref="gg-UkOA">
            <ta e="T2" id="Seg_4824" s="T1">dolganisch-SIM</ta>
            <ta e="T3" id="Seg_4825" s="T2">erzählen-FUT-1SG</ta>
            <ta e="T4" id="Seg_4826" s="T3">Q</ta>
            <ta e="T10" id="Seg_4827" s="T8">zwanzig</ta>
            <ta e="T11" id="Seg_4828" s="T10">zwei-PROPR</ta>
            <ta e="T12" id="Seg_4829" s="T11">Juni-DAT/LOC</ta>
            <ta e="T13" id="Seg_4830" s="T12">zwei</ta>
            <ta e="T14" id="Seg_4831" s="T13">hundert</ta>
            <ta e="T15" id="Seg_4832" s="T14">zehn-PROPR</ta>
            <ta e="T16" id="Seg_4833" s="T15">Jahr-DAT/LOC</ta>
            <ta e="T17" id="Seg_4834" s="T16">Chanty_Mansijsk</ta>
            <ta e="T18" id="Seg_4835" s="T17">Stadt-DAT/LOC</ta>
            <ta e="T19" id="Seg_4836" s="T18">werden-PST2-3SG</ta>
            <ta e="T20" id="Seg_4837" s="T19">Festival.[NOM]</ta>
            <ta e="T23" id="Seg_4838" s="T22">sagen-CVB.SEQ</ta>
            <ta e="T24" id="Seg_4839" s="T23">Name-PROPR</ta>
            <ta e="T25" id="Seg_4840" s="T24">dorthin</ta>
            <ta e="T26" id="Seg_4841" s="T25">Taimyr-ABL</ta>
            <ta e="T27" id="Seg_4842" s="T26">1PL.[NOM]</ta>
            <ta e="T28" id="Seg_4843" s="T27">gehen-PST2-1PL</ta>
            <ta e="T29" id="Seg_4844" s="T28">zwei</ta>
            <ta e="T30" id="Seg_4845" s="T29">Mensch.[NOM]</ta>
            <ta e="T31" id="Seg_4846" s="T30">anderer.[NOM]</ta>
            <ta e="T32" id="Seg_4847" s="T31">sein-PST1-3SG</ta>
            <ta e="T33" id="Seg_4848" s="T32">Vladimir</ta>
            <ta e="T34" id="Seg_4849" s="T33">Enowitsch</ta>
            <ta e="T35" id="Seg_4850" s="T34">Sigunej.[NOM]</ta>
            <ta e="T39" id="Seg_4851" s="T38">sagen-CVB.SEQ</ta>
            <ta e="T40" id="Seg_4852" s="T39">Mensch.[NOM]</ta>
            <ta e="T41" id="Seg_4853" s="T40">Name-PROPR</ta>
            <ta e="T45" id="Seg_4854" s="T44">und</ta>
            <ta e="T46" id="Seg_4855" s="T45">1SG.[NOM]</ta>
            <ta e="T47" id="Seg_4856" s="T46">Chatanga-ABL</ta>
            <ta e="T48" id="Seg_4857" s="T47">dort</ta>
            <ta e="T49" id="Seg_4858" s="T48">sehr</ta>
            <ta e="T50" id="Seg_4859" s="T49">gut-ADVZ</ta>
            <ta e="T51" id="Seg_4860" s="T50">1PL-ACC</ta>
            <ta e="T52" id="Seg_4861" s="T51">empfangen-PST1-3PL</ta>
            <ta e="T53" id="Seg_4862" s="T52">Salz-PROPR</ta>
            <ta e="T54" id="Seg_4863" s="T53">Brot-EP-INSTR</ta>
            <ta e="T55" id="Seg_4864" s="T54">groß</ta>
            <ta e="T56" id="Seg_4865" s="T55">sehr</ta>
            <ta e="T57" id="Seg_4866" s="T56">Hotel-DAT/LOC</ta>
            <ta e="T58" id="Seg_4867" s="T57">Haus-VBZ-PST1-3PL</ta>
            <ta e="T59" id="Seg_4868" s="T58">Name-3SG.[NOM]</ta>
            <ta e="T60" id="Seg_4869" s="T59">sein-PST1-3SG</ta>
            <ta e="T61" id="Seg_4870" s="T60">sieben</ta>
            <ta e="T62" id="Seg_4871" s="T61">Hügel.[NOM]</ta>
            <ta e="T65" id="Seg_4872" s="T64">sehr</ta>
            <ta e="T66" id="Seg_4873" s="T65">gut-ADVZ</ta>
            <ta e="T67" id="Seg_4874" s="T66">1PL-ACC</ta>
            <ta e="T68" id="Seg_4875" s="T67">essen-EP-CAUS-PRS-3PL</ta>
            <ta e="T69" id="Seg_4876" s="T68">dort</ta>
            <ta e="T70" id="Seg_4877" s="T69">essen-EP-CAUS-PST2-3PL</ta>
            <ta e="T71" id="Seg_4878" s="T70">schlafen-CAUS-PST2-3PL</ta>
            <ta e="T72" id="Seg_4879" s="T71">dann</ta>
            <ta e="T73" id="Seg_4880" s="T72">nächster.Morgen-3SG-ACC</ta>
            <ta e="T74" id="Seg_4881" s="T73">Probe-VBZ-PST2-1PL</ta>
            <ta e="T75" id="Seg_4882" s="T74">jenes.EMPH.[NOM]</ta>
            <ta e="T76" id="Seg_4883" s="T75">Abend-3SG-ACC</ta>
            <ta e="T77" id="Seg_4884" s="T76">jenes.EMPH.[NOM]</ta>
            <ta e="T78" id="Seg_4885" s="T77">Abend-3SG-ACC</ta>
            <ta e="T79" id="Seg_4886" s="T78">EMPH</ta>
            <ta e="T80" id="Seg_4887" s="T79">auftreten-VBZ-PST2-1PL</ta>
            <ta e="T81" id="Seg_4888" s="T80">jenes.[NOM]</ta>
            <ta e="T82" id="Seg_4889" s="T81">Hinterteil-3SG-ABL</ta>
            <ta e="T83" id="Seg_4890" s="T82">rund</ta>
            <ta e="T84" id="Seg_4891" s="T83">Tisch.[NOM]</ta>
            <ta e="T85" id="Seg_4892" s="T84">sein-PST2-3SG</ta>
            <ta e="T86" id="Seg_4893" s="T85">zwei-ORD</ta>
            <ta e="T87" id="Seg_4894" s="T86">Tag-3SG-DAT/LOC</ta>
            <ta e="T88" id="Seg_4895" s="T87">jenes</ta>
            <ta e="T89" id="Seg_4896" s="T88">rund</ta>
            <ta e="T90" id="Seg_4897" s="T89">Tisch-DAT/LOC</ta>
            <ta e="T91" id="Seg_4898" s="T90">MOD</ta>
            <ta e="T92" id="Seg_4899" s="T91">unterschiedlich</ta>
            <ta e="T93" id="Seg_4900" s="T92">äh</ta>
            <ta e="T94" id="Seg_4901" s="T93">viel</ta>
            <ta e="T95" id="Seg_4902" s="T94">Mensch.[NOM]</ta>
            <ta e="T96" id="Seg_4903" s="T95">sammeln-PASS/REFL-EP-PST2-3SG</ta>
            <ta e="T97" id="Seg_4904" s="T96">sich.freuen-NMNZ-ACC</ta>
            <ta e="T98" id="Seg_4905" s="T97">schlecht-ACC</ta>
            <ta e="T99" id="Seg_4906" s="T98">äh</ta>
            <ta e="T100" id="Seg_4907" s="T99">Leben.[NOM]</ta>
            <ta e="T101" id="Seg_4908" s="T100">Seite-3SG-INSTR</ta>
            <ta e="T102" id="Seg_4909" s="T101">sich.unterhalten-NMNZ.[NOM]</ta>
            <ta e="T103" id="Seg_4910" s="T102">es.gibt</ta>
            <ta e="T104" id="Seg_4911" s="T103">sein-PST1-3SG</ta>
            <ta e="T105" id="Seg_4912" s="T104">Kind-PL.[NOM]</ta>
            <ta e="T106" id="Seg_4913" s="T105">lernen-PTCP.PRS-3PL-GEN</ta>
            <ta e="T107" id="Seg_4914" s="T106">Seite-3PL-INSTR</ta>
            <ta e="T108" id="Seg_4915" s="T107">Student-PL.[NOM]</ta>
            <ta e="T109" id="Seg_4916" s="T108">äh</ta>
            <ta e="T110" id="Seg_4917" s="T109">dieses</ta>
            <ta e="T111" id="Seg_4918" s="T110">groß</ta>
            <ta e="T112" id="Seg_4919" s="T111">Hochschule-PL.[NOM]</ta>
            <ta e="T113" id="Seg_4920" s="T112">Seite-3PL-DAT/LOC</ta>
            <ta e="T114" id="Seg_4921" s="T113">warum</ta>
            <ta e="T115" id="Seg_4922" s="T114">werfen-PTCP.PRS-3PL-ACC</ta>
            <ta e="T116" id="Seg_4923" s="T115">oder</ta>
            <ta e="T117" id="Seg_4924" s="T116">warum</ta>
            <ta e="T118" id="Seg_4925" s="T117">lernen-NEG.PTCP-3PL-ACC</ta>
            <ta e="T119" id="Seg_4926" s="T118">jenes</ta>
            <ta e="T120" id="Seg_4927" s="T119">jenes</ta>
            <ta e="T121" id="Seg_4928" s="T120">Seite-3SG-DAT/LOC</ta>
            <ta e="T122" id="Seg_4929" s="T121">äh</ta>
            <ta e="T123" id="Seg_4930" s="T122">groß</ta>
            <ta e="T124" id="Seg_4931" s="T123">dann</ta>
            <ta e="T125" id="Seg_4932" s="T124">Frage.[NOM]</ta>
            <ta e="T126" id="Seg_4933" s="T125">stehen-PTCP.PRS</ta>
            <ta e="T127" id="Seg_4934" s="T126">sein-PST1-3SG</ta>
            <ta e="T128" id="Seg_4935" s="T127">dann</ta>
            <ta e="T129" id="Seg_4936" s="T128">MOD</ta>
            <ta e="T131" id="Seg_4937" s="T130">äh</ta>
            <ta e="T132" id="Seg_4938" s="T131">Alte.[NOM]</ta>
            <ta e="T133" id="Seg_4939" s="T132">auftreten-VBZ-PST2.[3SG]</ta>
            <ta e="T134" id="Seg_4940" s="T133">dort</ta>
            <ta e="T135" id="Seg_4941" s="T134">Moldanova</ta>
            <ta e="T136" id="Seg_4942" s="T135">sagen-CVB.SEQ</ta>
            <ta e="T137" id="Seg_4943" s="T136">Marija</ta>
            <ta e="T138" id="Seg_4944" s="T137">Kuzminitschna</ta>
            <ta e="T139" id="Seg_4945" s="T138">Verstand-PROPR</ta>
            <ta e="T140" id="Seg_4946" s="T139">Frau.[NOM]</ta>
            <ta e="T141" id="Seg_4947" s="T140">jenes.[NOM]</ta>
            <ta e="T142" id="Seg_4948" s="T141">sprechen-PST2-3SG</ta>
            <ta e="T143" id="Seg_4949" s="T142">äh</ta>
            <ta e="T144" id="Seg_4950" s="T143">jung</ta>
            <ta e="T145" id="Seg_4951" s="T144">Mensch-PL.[NOM]</ta>
            <ta e="T146" id="Seg_4952" s="T145">äh</ta>
            <ta e="T147" id="Seg_4953" s="T146">jung</ta>
            <ta e="T148" id="Seg_4954" s="T147">Mensch-PL.[NOM]</ta>
            <ta e="T149" id="Seg_4955" s="T148">selbst-3PL-GEN</ta>
            <ta e="T150" id="Seg_4956" s="T149">Familie-3PL-DAT/LOC</ta>
            <ta e="T151" id="Seg_4957" s="T150">selbst-3PL-GEN</ta>
            <ta e="T152" id="Seg_4958" s="T151">Familie-3PL-GEN</ta>
            <ta e="T153" id="Seg_4959" s="T152">Inneres-3PL-DAT/LOC</ta>
            <ta e="T154" id="Seg_4960" s="T153">Inneres-3PL-ABL</ta>
            <ta e="T155" id="Seg_4961" s="T154">Erziehung.[NOM]</ta>
            <ta e="T156" id="Seg_4962" s="T155">sein-PRS-NEC.[3SG]</ta>
            <ta e="T157" id="Seg_4963" s="T156">gebären-EP-CAUS-PTCP.PST</ta>
            <ta e="T158" id="Seg_4964" s="T157">Mensch.[NOM]</ta>
            <ta e="T159" id="Seg_4965" s="T158">geben-PRS-NEC.[3SG]</ta>
            <ta e="T160" id="Seg_4966" s="T159">Ausbildung-ACC</ta>
            <ta e="T161" id="Seg_4967" s="T160">gut-ACC</ta>
            <ta e="T162" id="Seg_4968" s="T161">schlecht-ACC</ta>
            <ta e="T163" id="Seg_4969" s="T162">Kind-DAT/LOC</ta>
            <ta e="T164" id="Seg_4970" s="T163">ah</ta>
            <ta e="T165" id="Seg_4971" s="T164">Schule.[NOM]</ta>
            <ta e="T166" id="Seg_4972" s="T165">Kindergarten.[NOM]</ta>
            <ta e="T167" id="Seg_4973" s="T166">jenes-DAT/LOC</ta>
            <ta e="T168" id="Seg_4974" s="T167">MOD</ta>
            <ta e="T169" id="Seg_4975" s="T168">äh</ta>
            <ta e="T170" id="Seg_4976" s="T169">Lehranstalt-PL.[NOM]</ta>
            <ta e="T171" id="Seg_4977" s="T170">jenes-DAT/LOC</ta>
            <ta e="T172" id="Seg_4978" s="T171">Hilfe.[NOM]</ta>
            <ta e="T173" id="Seg_4979" s="T172">nur</ta>
            <ta e="T174" id="Seg_4980" s="T173">sein-PTCP.FUT-3PL-ACC</ta>
            <ta e="T175" id="Seg_4981" s="T174">man.muss</ta>
            <ta e="T176" id="Seg_4982" s="T175">und</ta>
            <ta e="T180" id="Seg_4983" s="T179">ähnlich</ta>
            <ta e="T181" id="Seg_4984" s="T180">sein-PTCP.FUT-3PL-ACC</ta>
            <ta e="T182" id="Seg_4985" s="T181">jenes.[NOM]</ta>
            <ta e="T183" id="Seg_4986" s="T182">wegen</ta>
            <ta e="T184" id="Seg_4987" s="T183">MOD</ta>
            <ta e="T185" id="Seg_4988" s="T184">jetzt</ta>
            <ta e="T186" id="Seg_4989" s="T185">Vorderteil.[NOM]</ta>
            <ta e="T187" id="Seg_4990" s="T186">zu</ta>
            <ta e="T188" id="Seg_4991" s="T187">äh</ta>
            <ta e="T189" id="Seg_4992" s="T188">wachsen-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T190" id="Seg_4993" s="T189">man.muss</ta>
            <ta e="T191" id="Seg_4994" s="T190">sagen-PRS.[3SG]</ta>
            <ta e="T192" id="Seg_4995" s="T191">äh</ta>
            <ta e="T193" id="Seg_4996" s="T192">jenes</ta>
            <ta e="T194" id="Seg_4997" s="T193">Frau.[NOM]</ta>
            <ta e="T195" id="Seg_4998" s="T194">Verantwortung-ACC</ta>
            <ta e="T196" id="Seg_4999" s="T195">gebären-EP-CAUS-PTCP.PRS</ta>
            <ta e="T197" id="Seg_5000" s="T196">Mensch-PL-ABL</ta>
            <ta e="T198" id="Seg_5001" s="T197">Elter-PL-ABL</ta>
            <ta e="T199" id="Seg_5002" s="T198">und</ta>
            <ta e="T200" id="Seg_5003" s="T199">und</ta>
            <ta e="T201" id="Seg_5004" s="T200">gut-ACC</ta>
            <ta e="T202" id="Seg_5005" s="T201">mehr.werden-CAUS-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T203" id="Seg_5006" s="T202">sagen-CVB.SEQ</ta>
            <ta e="T204" id="Seg_5007" s="T203">sprechen-PST2-3SG</ta>
            <ta e="T205" id="Seg_5008" s="T204">jenes</ta>
            <ta e="T206" id="Seg_5009" s="T205">Frau.[NOM]</ta>
            <ta e="T207" id="Seg_5010" s="T206">gut-ACC</ta>
            <ta e="T208" id="Seg_5011" s="T207">mehr.werden-CAUS-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T209" id="Seg_5012" s="T208">und</ta>
            <ta e="T210" id="Seg_5013" s="T209">schlecht-ACC</ta>
            <ta e="T211" id="Seg_5014" s="T210">abnehmen-CAUS-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T212" id="Seg_5015" s="T211">dieses</ta>
            <ta e="T213" id="Seg_5016" s="T212">Erde-DAT/LOC</ta>
            <ta e="T214" id="Seg_5017" s="T213">so</ta>
            <ta e="T215" id="Seg_5018" s="T214">also</ta>
            <ta e="T216" id="Seg_5019" s="T215">feststellen-CVB.SEQ</ta>
            <ta e="T217" id="Seg_5020" s="T216">sehen-PTCP.COND-DAT/LOC</ta>
            <ta e="T218" id="Seg_5021" s="T217">wenn</ta>
            <ta e="T219" id="Seg_5022" s="T218">2SG-ACC</ta>
            <ta e="T220" id="Seg_5023" s="T219">schlecht-ADVZ</ta>
            <ta e="T221" id="Seg_5024" s="T220">sprechen-PST1-3PL</ta>
            <ta e="T222" id="Seg_5025" s="T221">schlecht-ACC</ta>
            <ta e="T223" id="Seg_5026" s="T222">machen-PST1-3PL</ta>
            <ta e="T224" id="Seg_5027" s="T223">Q</ta>
            <ta e="T225" id="Seg_5028" s="T224">2SG.[NOM]</ta>
            <ta e="T226" id="Seg_5029" s="T225">EMPH</ta>
            <ta e="T227" id="Seg_5030" s="T226">erzählen-NEG-PTCP.FUT-2SG-ACC</ta>
            <ta e="T228" id="Seg_5031" s="T227">man.muss</ta>
            <ta e="T229" id="Seg_5032" s="T228">jenes</ta>
            <ta e="T230" id="Seg_5033" s="T229">Mensch.[NOM]</ta>
            <ta e="T231" id="Seg_5034" s="T230">Seite-3SG-INSTR</ta>
            <ta e="T232" id="Seg_5035" s="T231">sagen-PRS.[3SG]</ta>
            <ta e="T233" id="Seg_5036" s="T232">jenes</ta>
            <ta e="T234" id="Seg_5037" s="T233">Frau.[NOM]</ta>
            <ta e="T235" id="Seg_5038" s="T234">jenes</ta>
            <ta e="T236" id="Seg_5039" s="T235">Mensch.[NOM]</ta>
            <ta e="T237" id="Seg_5040" s="T236">Seite-3SG</ta>
            <ta e="T238" id="Seg_5041" s="T237">jenes</ta>
            <ta e="T239" id="Seg_5042" s="T238">Mensch.[NOM]</ta>
            <ta e="T240" id="Seg_5043" s="T239">Seite-3SG-INSTR</ta>
            <ta e="T241" id="Seg_5044" s="T240">selbst-3SG-DAT/LOC</ta>
            <ta e="T242" id="Seg_5045" s="T241">sprechen-EP-NEG-PTCP.FUT-2SG-ACC</ta>
            <ta e="T243" id="Seg_5046" s="T242">man.muss</ta>
            <ta e="T244" id="Seg_5047" s="T243">aber</ta>
            <ta e="T245" id="Seg_5048" s="T244">Mensch-PL-DAT/LOC</ta>
            <ta e="T246" id="Seg_5049" s="T245">auch</ta>
            <ta e="T247" id="Seg_5050" s="T246">erzählen-NEG-PTCP.FUT-DAT/LOC</ta>
            <ta e="T248" id="Seg_5051" s="T247">man.muss</ta>
            <ta e="T249" id="Seg_5052" s="T248">damit</ta>
            <ta e="T250" id="Seg_5053" s="T249">schlecht.[NOM]</ta>
            <ta e="T251" id="Seg_5054" s="T250">wachsen-EP-NEG-PTCP.FUT-3SG-ACC</ta>
            <ta e="T252" id="Seg_5055" s="T251">und</ta>
            <ta e="T253" id="Seg_5056" s="T252">gut-ADVZ</ta>
            <ta e="T254" id="Seg_5057" s="T253">machen-PTCP.PST</ta>
            <ta e="T255" id="Seg_5058" s="T254">Mensch-DAT/LOC</ta>
            <ta e="T256" id="Seg_5059" s="T255">MOD</ta>
            <ta e="T257" id="Seg_5060" s="T256">jenes</ta>
            <ta e="T258" id="Seg_5061" s="T257">Mensch-DAT/LOC</ta>
            <ta e="T260" id="Seg_5062" s="T259">auch</ta>
            <ta e="T261" id="Seg_5063" s="T260">selbst-3SG-DAT/LOC</ta>
            <ta e="T262" id="Seg_5064" s="T261">sprechen-EP-NEG-PTCP.FUT-2SG-ACC</ta>
            <ta e="T263" id="Seg_5065" s="T262">damit</ta>
            <ta e="T264" id="Seg_5066" s="T263">prahlen-NEG-PTCP.FUT-3SG-ACC</ta>
            <ta e="T265" id="Seg_5067" s="T264">und</ta>
            <ta e="T266" id="Seg_5068" s="T265">anders</ta>
            <ta e="T267" id="Seg_5069" s="T266">Mensch-PL-DAT/LOC</ta>
            <ta e="T268" id="Seg_5070" s="T267">erzählen-PTCP.FUT-2SG-ACC</ta>
            <ta e="T269" id="Seg_5071" s="T268">man.muss</ta>
            <ta e="T270" id="Seg_5072" s="T269">dass</ta>
            <ta e="T271" id="Seg_5073" s="T270">jenes</ta>
            <ta e="T272" id="Seg_5074" s="T271">Mensch.[NOM]</ta>
            <ta e="T273" id="Seg_5075" s="T272">gut-ACC</ta>
            <ta e="T274" id="Seg_5076" s="T273">machen-PST2-3SG</ta>
            <ta e="T275" id="Seg_5077" s="T274">sagen-CVB.SEQ</ta>
            <ta e="T276" id="Seg_5078" s="T275">jenes</ta>
            <ta e="T277" id="Seg_5079" s="T276">Mensch.[NOM]</ta>
            <ta e="T278" id="Seg_5080" s="T277">solch.[NOM]</ta>
            <ta e="T279" id="Seg_5081" s="T278">sagen-CVB.SEQ</ta>
            <ta e="T280" id="Seg_5082" s="T279">jenes</ta>
            <ta e="T281" id="Seg_5083" s="T280">Mensch.[NOM]</ta>
            <ta e="T282" id="Seg_5084" s="T281">solch.[NOM]</ta>
            <ta e="T283" id="Seg_5085" s="T282">sagen-CVB.SEQ</ta>
            <ta e="T285" id="Seg_5086" s="T284">dann</ta>
            <ta e="T286" id="Seg_5087" s="T285">MOD</ta>
            <ta e="T287" id="Seg_5088" s="T286">gut.[NOM]</ta>
            <ta e="T288" id="Seg_5089" s="T287">wachsen-PRS.[3SG]</ta>
            <ta e="T289" id="Seg_5090" s="T288">gut.[NOM]</ta>
            <ta e="T290" id="Seg_5091" s="T289">wachsen-PRS.[3SG]</ta>
            <ta e="T291" id="Seg_5092" s="T290">solch</ta>
            <ta e="T292" id="Seg_5093" s="T291">Formel-INSTR</ta>
            <ta e="T293" id="Seg_5094" s="T292">sagen-CVB.SEQ</ta>
            <ta e="T294" id="Seg_5095" s="T293">aber</ta>
            <ta e="T295" id="Seg_5096" s="T294">wieder</ta>
            <ta e="T296" id="Seg_5097" s="T295">so</ta>
            <ta e="T297" id="Seg_5098" s="T296">und</ta>
            <ta e="T298" id="Seg_5099" s="T297">jenes</ta>
            <ta e="T299" id="Seg_5100" s="T298">Formel-DAT/LOC</ta>
            <ta e="T300" id="Seg_5101" s="T299">1SG.[NOM]</ta>
            <ta e="T301" id="Seg_5102" s="T300">Einverständnis-VBZ-NEG-1SG</ta>
            <ta e="T302" id="Seg_5103" s="T301">warum</ta>
            <ta e="T303" id="Seg_5104" s="T302">sagen-CVB.SEQ-2PL</ta>
            <ta e="T304" id="Seg_5105" s="T303">schlecht.[NOM]</ta>
            <ta e="T305" id="Seg_5106" s="T304">zwei-ORD</ta>
            <ta e="T306" id="Seg_5107" s="T305">Formel.[NOM]</ta>
            <ta e="T307" id="Seg_5108" s="T306">es.gibt</ta>
            <ta e="T308" id="Seg_5109" s="T307">jenes</ta>
            <ta e="T309" id="Seg_5110" s="T308">oberer.Teil-3SG-DAT/LOC</ta>
            <ta e="T310" id="Seg_5111" s="T309">dann</ta>
            <ta e="T311" id="Seg_5112" s="T310">schlecht.[NOM]</ta>
            <ta e="T312" id="Seg_5113" s="T311">wachsen-EP-NEG-PTCP.FUT-3SG-ACC</ta>
            <ta e="T313" id="Seg_5114" s="T312">schlecht-ACC</ta>
            <ta e="T314" id="Seg_5115" s="T313">Vorderteil.[NOM]</ta>
            <ta e="T315" id="Seg_5116" s="T314">zu</ta>
            <ta e="T316" id="Seg_5117" s="T315">zurück</ta>
            <ta e="T317" id="Seg_5118" s="T316">groß-ACC</ta>
            <ta e="T318" id="Seg_5119" s="T317">Antwort.[NOM]</ta>
            <ta e="T319" id="Seg_5120" s="T318">geben-PTCP.FUT-DAT/LOC</ta>
            <ta e="T320" id="Seg_5121" s="T319">man.muss</ta>
            <ta e="T321" id="Seg_5122" s="T320">3SG-DAT/LOC</ta>
            <ta e="T322" id="Seg_5123" s="T321">damit</ta>
            <ta e="T323" id="Seg_5124" s="T322">schlecht-ACC</ta>
            <ta e="T324" id="Seg_5125" s="T323">machen-PTCP.PST</ta>
            <ta e="T325" id="Seg_5126" s="T324">Mensch.[NOM]</ta>
            <ta e="T326" id="Seg_5127" s="T325">wissen-PTCP.FUT-3SG-ACC</ta>
            <ta e="T327" id="Seg_5128" s="T326">dass</ta>
            <ta e="T328" id="Seg_5129" s="T327">einfach</ta>
            <ta e="T329" id="Seg_5130" s="T328">Mensch-ACC</ta>
            <ta e="T330" id="Seg_5131" s="T329">sein</ta>
            <ta e="T331" id="Seg_5132" s="T330">schlecht-3SG.[NOM]</ta>
            <ta e="T332" id="Seg_5133" s="T331">einfach</ta>
            <ta e="T333" id="Seg_5134" s="T332">vorbeigehen-FUT.[3SG]</ta>
            <ta e="T334" id="Seg_5135" s="T333">NEG-3SG</ta>
            <ta e="T335" id="Seg_5136" s="T334">3SG-DAT/LOC</ta>
            <ta e="T336" id="Seg_5137" s="T335">3SG.[NOM]</ta>
            <ta e="T337" id="Seg_5138" s="T336">MOD</ta>
            <ta e="T338" id="Seg_5139" s="T337">Mensch.[NOM]</ta>
            <ta e="T339" id="Seg_5140" s="T338">Vorderteil-3SG-DAT/LOC</ta>
            <ta e="T340" id="Seg_5141" s="T339">Verantwortung-VBZ-PRS-NEC.[3SG]</ta>
            <ta e="T341" id="Seg_5142" s="T340">damit</ta>
            <ta e="T342" id="Seg_5143" s="T341">Vorderteil.[NOM]</ta>
            <ta e="T343" id="Seg_5144" s="T342">zu</ta>
            <ta e="T344" id="Seg_5145" s="T343">schlecht-ACC</ta>
            <ta e="T345" id="Seg_5146" s="T344">machen-EP-NEG-PTCP.FUT-3SG-ACC</ta>
            <ta e="T346" id="Seg_5147" s="T345">dann</ta>
            <ta e="T347" id="Seg_5148" s="T346">MOD</ta>
            <ta e="T348" id="Seg_5149" s="T347">gut.[NOM]</ta>
            <ta e="T349" id="Seg_5150" s="T348">zwei-MLTP</ta>
            <ta e="T350" id="Seg_5151" s="T349">Mal.[NOM]</ta>
            <ta e="T351" id="Seg_5152" s="T350">wachsen-CVB.SEQ</ta>
            <ta e="T352" id="Seg_5153" s="T351">hinausgehen-FUT-3SG</ta>
            <ta e="T353" id="Seg_5154" s="T352">und</ta>
            <ta e="T354" id="Seg_5155" s="T353">leben-PTCP.HAB</ta>
            <ta e="T355" id="Seg_5156" s="T354">Mensch-PL.[NOM]</ta>
            <ta e="T356" id="Seg_5157" s="T355">dort</ta>
            <ta e="T357" id="Seg_5158" s="T356">leben-PTCP.HAB</ta>
            <ta e="T358" id="Seg_5159" s="T357">Mensch-PL.[NOM]</ta>
            <ta e="T359" id="Seg_5160" s="T358">selbst-3PL-GEN</ta>
            <ta e="T360" id="Seg_5161" s="T359">Vorderteil.[NOM]</ta>
            <ta e="T361" id="Seg_5162" s="T360">zu</ta>
            <ta e="T362" id="Seg_5163" s="T361">selbst-3PL-GEN</ta>
            <ta e="T363" id="Seg_5164" s="T362">Vorderteil.[NOM]</ta>
            <ta e="T364" id="Seg_5165" s="T363">zu</ta>
            <ta e="T365" id="Seg_5166" s="T364">kränken-EP-CAUS-FUT-3PL</ta>
            <ta e="T366" id="Seg_5167" s="T365">NEG-3PL</ta>
            <ta e="T367" id="Seg_5168" s="T366">NEG</ta>
            <ta e="T368" id="Seg_5169" s="T367">kränken-EP-CAUS-FUT-3PL</ta>
            <ta e="T369" id="Seg_5170" s="T368">NEG-3PL</ta>
            <ta e="T370" id="Seg_5171" s="T369">ah</ta>
            <ta e="T371" id="Seg_5172" s="T370">aber</ta>
            <ta e="T372" id="Seg_5173" s="T371">dann</ta>
            <ta e="T373" id="Seg_5174" s="T372">jenes</ta>
            <ta e="T374" id="Seg_5175" s="T373">jenes</ta>
            <ta e="T375" id="Seg_5176" s="T374">rund</ta>
            <ta e="T376" id="Seg_5177" s="T375">rund</ta>
            <ta e="T377" id="Seg_5178" s="T376">Tisch.[NOM]</ta>
            <ta e="T378" id="Seg_5179" s="T377">aufhören-PTCP.PST-3SG-ACC</ta>
            <ta e="T379" id="Seg_5180" s="T378">nachdem</ta>
            <ta e="T380" id="Seg_5181" s="T379">auch</ta>
            <ta e="T381" id="Seg_5182" s="T380">Konzert.[NOM]</ta>
            <ta e="T384" id="Seg_5183" s="T383">sagen-CVB.SEQ</ta>
            <ta e="T385" id="Seg_5184" s="T384">sein-PST2-3SG</ta>
            <ta e="T386" id="Seg_5185" s="T385">dort</ta>
            <ta e="T387" id="Seg_5186" s="T386">sehr</ta>
            <ta e="T388" id="Seg_5187" s="T387">gut-ADVZ</ta>
            <ta e="T389" id="Seg_5188" s="T388">auftreten-VBZ-PST2-3PL</ta>
            <ta e="T390" id="Seg_5189" s="T389">viel</ta>
            <ta e="T391" id="Seg_5190" s="T390">Kollektiv.[NOM]</ta>
            <ta e="T392" id="Seg_5191" s="T391">kommen-PST2-3SG</ta>
            <ta e="T393" id="Seg_5192" s="T392">äh</ta>
            <ta e="T394" id="Seg_5193" s="T393">Chanty_Mansijsk-ABL</ta>
            <ta e="T395" id="Seg_5194" s="T394">selbst-3SG-ABL</ta>
            <ta e="T396" id="Seg_5195" s="T395">Theater</ta>
            <ta e="T397" id="Seg_5196" s="T396">Gruppe.[NOM]</ta>
            <ta e="T398" id="Seg_5197" s="T397">es.gibt</ta>
            <ta e="T399" id="Seg_5198" s="T398">sein-PST1-3SG</ta>
            <ta e="T400" id="Seg_5199" s="T399">unser</ta>
            <ta e="T401" id="Seg_5200" s="T400">Ensemble.[NOM]</ta>
            <ta e="T402" id="Seg_5201" s="T401">dolganischer.Tanz-1PL.[NOM]</ta>
            <ta e="T403" id="Seg_5202" s="T402">ähnlich</ta>
            <ta e="T404" id="Seg_5203" s="T403">dann</ta>
            <ta e="T405" id="Seg_5204" s="T404">MOD</ta>
            <ta e="T406" id="Seg_5205" s="T405">Magadan-ABL</ta>
            <ta e="T407" id="Seg_5206" s="T406">kommen-PST2-3PL</ta>
            <ta e="T408" id="Seg_5207" s="T407">äh</ta>
            <ta e="T409" id="Seg_5208" s="T408">Jamalo_Nenzischer.Autonomer.Kreis-ABL</ta>
            <ta e="T410" id="Seg_5209" s="T409">Tschukotka-ABL</ta>
            <ta e="T411" id="Seg_5210" s="T410">Ensemble.[NOM]</ta>
            <ta e="T412" id="Seg_5211" s="T411">es.gibt</ta>
            <ta e="T413" id="Seg_5212" s="T412">sein-PST1-3SG</ta>
            <ta e="T414" id="Seg_5213" s="T413">fern</ta>
            <ta e="T415" id="Seg_5214" s="T414">Ort-ABL</ta>
            <ta e="T416" id="Seg_5215" s="T415">kommen-FREQ-PST2-3PL</ta>
            <ta e="T417" id="Seg_5216" s="T416">dann</ta>
            <ta e="T418" id="Seg_5217" s="T417">viel-viel</ta>
            <ta e="T419" id="Seg_5218" s="T418">Ort-ABL</ta>
            <ta e="T420" id="Seg_5219" s="T419">kommen-FREQ-PST2-3PL</ta>
            <ta e="T421" id="Seg_5220" s="T420">dorthin</ta>
            <ta e="T422" id="Seg_5221" s="T421">zwanzig</ta>
            <ta e="T423" id="Seg_5222" s="T422">zwei</ta>
            <ta e="T424" id="Seg_5223" s="T423">Kollektiv.[NOM]</ta>
            <ta e="T425" id="Seg_5224" s="T424">es.gibt</ta>
            <ta e="T426" id="Seg_5225" s="T425">sein-PST1-3SG</ta>
            <ta e="T427" id="Seg_5226" s="T426">äh</ta>
            <ta e="T428" id="Seg_5227" s="T427">dann</ta>
            <ta e="T429" id="Seg_5228" s="T428">Kurator-PL-1PL.[NOM]</ta>
            <ta e="T430" id="Seg_5229" s="T429">gut</ta>
            <ta e="T431" id="Seg_5230" s="T430">sehr</ta>
            <ta e="T432" id="Seg_5231" s="T431">Frau-PL.[NOM]</ta>
            <ta e="T433" id="Seg_5232" s="T432">sein-PST1-3PL</ta>
            <ta e="T434" id="Seg_5233" s="T433">1PL-ACC</ta>
            <ta e="T435" id="Seg_5234" s="T434">ganz-3SG-ACC</ta>
            <ta e="T436" id="Seg_5235" s="T435">äh</ta>
            <ta e="T437" id="Seg_5236" s="T436">mitnehmen-CVB.SEQ-3PL</ta>
            <ta e="T438" id="Seg_5237" s="T437">bringen-CVB.SEQ-3PL</ta>
            <ta e="T439" id="Seg_5238" s="T438">klein</ta>
            <ta e="T440" id="Seg_5239" s="T439">Kind-SIM</ta>
            <ta e="T441" id="Seg_5240" s="T440">schlafen-CAUS-CVB.SEQ-3PL</ta>
            <ta e="T442" id="Seg_5241" s="T441">dann</ta>
            <ta e="T443" id="Seg_5242" s="T442">essen-EP-CAUS-CVB.SEQ-3PL</ta>
            <ta e="T444" id="Seg_5243" s="T443">dann</ta>
            <ta e="T445" id="Seg_5244" s="T444">Museum-PL.[NOM]</ta>
            <ta e="T446" id="Seg_5245" s="T445">durch</ta>
            <ta e="T447" id="Seg_5246" s="T446">Motorschiff-DAT/LOC</ta>
            <ta e="T448" id="Seg_5247" s="T447">gehen-EP-PST2-1PL</ta>
            <ta e="T449" id="Seg_5248" s="T448">Irtysch</ta>
            <ta e="T450" id="Seg_5249" s="T449">sagen-CVB.SEQ</ta>
            <ta e="T451" id="Seg_5250" s="T450">äh</ta>
            <ta e="T452" id="Seg_5251" s="T451">Fluss.[NOM]</ta>
            <ta e="T453" id="Seg_5252" s="T452">Irtysch</ta>
            <ta e="T454" id="Seg_5253" s="T453">sagen-CVB.SEQ</ta>
            <ta e="T455" id="Seg_5254" s="T454">Fluss.[NOM]</ta>
            <ta e="T456" id="Seg_5255" s="T455">entlang</ta>
            <ta e="T457" id="Seg_5256" s="T456">gehen-PST2-1PL</ta>
            <ta e="T458" id="Seg_5257" s="T457">jenes</ta>
            <ta e="T459" id="Seg_5258" s="T458">Irtysch</ta>
            <ta e="T460" id="Seg_5259" s="T459">MOD</ta>
            <ta e="T461" id="Seg_5260" s="T460">groß</ta>
            <ta e="T462" id="Seg_5261" s="T461">Fluss-DAT/LOC</ta>
            <ta e="T463" id="Seg_5262" s="T462">sich.verbinden-PRS.[3SG]</ta>
            <ta e="T464" id="Seg_5263" s="T463">Ob</ta>
            <ta e="T465" id="Seg_5264" s="T464">sagen-CVB.SEQ</ta>
            <ta e="T466" id="Seg_5265" s="T465">Fluss-DAT/LOC</ta>
            <ta e="T467" id="Seg_5266" s="T466">jenes</ta>
            <ta e="T468" id="Seg_5267" s="T467">sich.verbinden-EP-PTCP.PST-PL-DAT/LOC</ta>
            <ta e="T469" id="Seg_5268" s="T468">Geschenk-VBZ-PST2-1PL</ta>
            <ta e="T471" id="Seg_5269" s="T470">Lied-PL-ACC</ta>
            <ta e="T472" id="Seg_5270" s="T471">singen-PST2-1PL</ta>
            <ta e="T473" id="Seg_5271" s="T472">kommen-CVB.PURP-1PL</ta>
            <ta e="T474" id="Seg_5272" s="T473">gehen-CVB.PURP-1PL</ta>
            <ta e="T475" id="Seg_5273" s="T474">fröhlich.[NOM]</ta>
            <ta e="T476" id="Seg_5274" s="T475">sehr</ta>
            <ta e="T477" id="Seg_5275" s="T476">sein-PST1-3SG</ta>
            <ta e="T478" id="Seg_5276" s="T477">Kino-DAT/LOC</ta>
            <ta e="T479" id="Seg_5277" s="T478">wegnehmen-PST2-3PL</ta>
            <ta e="T480" id="Seg_5278" s="T479">1PL-ACC</ta>
            <ta e="T481" id="Seg_5279" s="T480">dann</ta>
            <ta e="T482" id="Seg_5280" s="T481">dann</ta>
            <ta e="T483" id="Seg_5281" s="T482">zurück</ta>
            <ta e="T484" id="Seg_5282" s="T483">kommen-CVB.SEQ-1PL</ta>
            <ta e="T485" id="Seg_5283" s="T484">äh</ta>
            <ta e="T486" id="Seg_5284" s="T485">jenes</ta>
            <ta e="T487" id="Seg_5285" s="T486">nächster.Morgen-3SG-ACC</ta>
            <ta e="T488" id="Seg_5286" s="T487">jenes</ta>
            <ta e="T489" id="Seg_5287" s="T488">drei</ta>
            <ta e="T490" id="Seg_5288" s="T489">Tag-ACC</ta>
            <ta e="T491" id="Seg_5289" s="T490">werden-PST2-1PL</ta>
            <ta e="T492" id="Seg_5290" s="T491">äh</ta>
            <ta e="T493" id="Seg_5291" s="T492">drei</ta>
            <ta e="T494" id="Seg_5292" s="T493">Tag-ACC</ta>
            <ta e="T495" id="Seg_5293" s="T494">sein-HAB.[3SG]</ta>
            <ta e="T496" id="Seg_5294" s="T495">äh</ta>
            <ta e="T497" id="Seg_5295" s="T496">drei</ta>
            <ta e="T498" id="Seg_5296" s="T497">Tag-ACC</ta>
            <ta e="T499" id="Seg_5297" s="T498">sein-PST2-1PL-DAT/LOC</ta>
            <ta e="T500" id="Seg_5298" s="T499">Chanty_Mansijsk-DAT/LOC</ta>
            <ta e="T501" id="Seg_5299" s="T500">gut.[NOM]</ta>
            <ta e="T502" id="Seg_5300" s="T501">sehr</ta>
            <ta e="T503" id="Seg_5301" s="T502">Tag-PL.[NOM]</ta>
            <ta e="T504" id="Seg_5302" s="T503">sein-PST1-3PL</ta>
            <ta e="T505" id="Seg_5303" s="T504">Wetter.[NOM]</ta>
            <ta e="T506" id="Seg_5304" s="T505">jenes.[NOM]</ta>
            <ta e="T507" id="Seg_5305" s="T506">Vorderteil-3SG-DAT/LOC</ta>
            <ta e="T508" id="Seg_5306" s="T507">Regen-VBZ-PTCP.PRS</ta>
            <ta e="T509" id="Seg_5307" s="T508">sein-PST1-3SG</ta>
            <ta e="T510" id="Seg_5308" s="T509">sagen-PTCP.PRS</ta>
            <ta e="T511" id="Seg_5309" s="T510">sein-PST1-3PL</ta>
            <ta e="T512" id="Seg_5310" s="T511">dann</ta>
            <ta e="T513" id="Seg_5311" s="T512">zwanzig-ABL</ta>
            <ta e="T514" id="Seg_5312" s="T513">über</ta>
            <ta e="T515" id="Seg_5313" s="T514">Grad.[NOM]</ta>
            <ta e="T516" id="Seg_5314" s="T515">sein-PST1-3SG</ta>
            <ta e="T517" id="Seg_5315" s="T516">zwanzig</ta>
            <ta e="T518" id="Seg_5316" s="T517">vier</ta>
            <ta e="T519" id="Seg_5317" s="T518">zwanzig</ta>
            <ta e="T520" id="Seg_5318" s="T519">fünf</ta>
            <ta e="T521" id="Seg_5319" s="T520">Grad.[NOM]</ta>
            <ta e="T522" id="Seg_5320" s="T521">sein-CVB.SEQ</ta>
            <ta e="T523" id="Seg_5321" s="T522">gehen-PST1-3SG</ta>
            <ta e="T524" id="Seg_5322" s="T523">dieses</ta>
            <ta e="T525" id="Seg_5323" s="T524">Tag-PL-DAT/LOC</ta>
            <ta e="T526" id="Seg_5324" s="T525">jenes.[NOM]</ta>
            <ta e="T527" id="Seg_5325" s="T526">wegen</ta>
            <ta e="T528" id="Seg_5326" s="T527">MOD</ta>
            <ta e="T529" id="Seg_5327" s="T528">1PL-DAT/LOC</ta>
            <ta e="T530" id="Seg_5328" s="T529">sehr</ta>
            <ta e="T531" id="Seg_5329" s="T530">NEG</ta>
            <ta e="T532" id="Seg_5330" s="T531">warm-POSS</ta>
            <ta e="T533" id="Seg_5331" s="T532">NEG</ta>
            <ta e="T534" id="Seg_5332" s="T533">sein-PST1-3SG</ta>
            <ta e="T535" id="Seg_5333" s="T534">aber</ta>
            <ta e="T536" id="Seg_5334" s="T535">sehr</ta>
            <ta e="T537" id="Seg_5335" s="T536">NEG</ta>
            <ta e="T538" id="Seg_5336" s="T537">und</ta>
            <ta e="T539" id="Seg_5337" s="T538">kalt-3SG</ta>
            <ta e="T540" id="Seg_5338" s="T539">NEG</ta>
            <ta e="T541" id="Seg_5339" s="T540">sein-PST1-3SG</ta>
            <ta e="T542" id="Seg_5340" s="T541">jenes.[NOM]</ta>
            <ta e="T543" id="Seg_5341" s="T542">wegen</ta>
            <ta e="T544" id="Seg_5342" s="T543">sehr</ta>
            <ta e="T545" id="Seg_5343" s="T544">gut.[NOM]</ta>
            <ta e="T546" id="Seg_5344" s="T545">1PL-DAT/LOC</ta>
            <ta e="T547" id="Seg_5345" s="T546">1SG.[NOM]</ta>
            <ta e="T548" id="Seg_5346" s="T547">Gedanke-1SG-DAT/LOC</ta>
            <ta e="T549" id="Seg_5347" s="T548">solch</ta>
            <ta e="T550" id="Seg_5348" s="T549">Klima.[NOM]</ta>
            <ta e="T551" id="Seg_5349" s="T550">3PL-DAT/LOC</ta>
            <ta e="T552" id="Seg_5350" s="T551">solch</ta>
            <ta e="T553" id="Seg_5351" s="T552">Wetter.[NOM]</ta>
            <ta e="T554" id="Seg_5352" s="T553">sehr</ta>
            <ta e="T555" id="Seg_5353" s="T554">gut-ADVZ</ta>
            <ta e="T556" id="Seg_5354" s="T555">klappen-PRS.[3SG]</ta>
            <ta e="T557" id="Seg_5355" s="T556">jenes.[NOM]</ta>
            <ta e="T558" id="Seg_5356" s="T557">wegen</ta>
            <ta e="T559" id="Seg_5357" s="T558">noch</ta>
            <ta e="T560" id="Seg_5358" s="T559">und</ta>
            <ta e="T561" id="Seg_5359" s="T560">einladen-TEMP-3PL</ta>
            <ta e="T562" id="Seg_5360" s="T561">1SG.[NOM]</ta>
            <ta e="T563" id="Seg_5361" s="T562">Gedanke-1SG-DAT/LOC</ta>
            <ta e="T564" id="Seg_5362" s="T563">1SG.[NOM]</ta>
            <ta e="T565" id="Seg_5363" s="T564">EMPH</ta>
            <ta e="T566" id="Seg_5364" s="T565">gehen-PTCP.PRS</ta>
            <ta e="T567" id="Seg_5365" s="T566">wie-1SG</ta>
            <ta e="T568" id="Seg_5366" s="T567">noch</ta>
            <ta e="T569" id="Seg_5367" s="T568">einmal</ta>
            <ta e="T570" id="Seg_5368" s="T569">besuchen-CVB.SIM</ta>
         </annotation>
         <annotation name="gr" tierref="gr-UkOA">
            <ta e="T2" id="Seg_5369" s="T1">долганский-SIM</ta>
            <ta e="T3" id="Seg_5370" s="T2">рассказывать-FUT-1SG</ta>
            <ta e="T4" id="Seg_5371" s="T3">Q</ta>
            <ta e="T10" id="Seg_5372" s="T8">двадцать</ta>
            <ta e="T11" id="Seg_5373" s="T10">два-PROPR</ta>
            <ta e="T12" id="Seg_5374" s="T11">июнь-DAT/LOC</ta>
            <ta e="T13" id="Seg_5375" s="T12">два</ta>
            <ta e="T14" id="Seg_5376" s="T13">сто</ta>
            <ta e="T15" id="Seg_5377" s="T14">десять-PROPR</ta>
            <ta e="T16" id="Seg_5378" s="T15">год-DAT/LOC</ta>
            <ta e="T17" id="Seg_5379" s="T16">Ханты_Мансийск</ta>
            <ta e="T18" id="Seg_5380" s="T17">город-DAT/LOC</ta>
            <ta e="T19" id="Seg_5381" s="T18">становиться-PST2-3SG</ta>
            <ta e="T20" id="Seg_5382" s="T19">фестиваль.[NOM]</ta>
            <ta e="T23" id="Seg_5383" s="T22">говорить-CVB.SEQ</ta>
            <ta e="T24" id="Seg_5384" s="T23">имя-PROPR</ta>
            <ta e="T25" id="Seg_5385" s="T24">туда</ta>
            <ta e="T26" id="Seg_5386" s="T25">Таймыр-ABL</ta>
            <ta e="T27" id="Seg_5387" s="T26">1PL.[NOM]</ta>
            <ta e="T28" id="Seg_5388" s="T27">идти-PST2-1PL</ta>
            <ta e="T29" id="Seg_5389" s="T28">два</ta>
            <ta e="T30" id="Seg_5390" s="T29">человек.[NOM]</ta>
            <ta e="T31" id="Seg_5391" s="T30">другой.[NOM]</ta>
            <ta e="T32" id="Seg_5392" s="T31">быть-PST1-3SG</ta>
            <ta e="T33" id="Seg_5393" s="T32">Владимир</ta>
            <ta e="T34" id="Seg_5394" s="T33">Энович</ta>
            <ta e="T35" id="Seg_5395" s="T34">Сигуней.[NOM]</ta>
            <ta e="T39" id="Seg_5396" s="T38">говорить-CVB.SEQ</ta>
            <ta e="T40" id="Seg_5397" s="T39">человек.[NOM]</ta>
            <ta e="T41" id="Seg_5398" s="T40">имя-PROPR</ta>
            <ta e="T45" id="Seg_5399" s="T44">и</ta>
            <ta e="T46" id="Seg_5400" s="T45">1SG.[NOM]</ta>
            <ta e="T47" id="Seg_5401" s="T46">Хатанга-ABL</ta>
            <ta e="T48" id="Seg_5402" s="T47">там</ta>
            <ta e="T49" id="Seg_5403" s="T48">очень</ta>
            <ta e="T50" id="Seg_5404" s="T49">хороший-ADVZ</ta>
            <ta e="T51" id="Seg_5405" s="T50">1PL-ACC</ta>
            <ta e="T52" id="Seg_5406" s="T51">принимать-PST1-3PL</ta>
            <ta e="T53" id="Seg_5407" s="T52">соль-PROPR</ta>
            <ta e="T54" id="Seg_5408" s="T53">хлеб-EP-INSTR</ta>
            <ta e="T55" id="Seg_5409" s="T54">большой</ta>
            <ta e="T56" id="Seg_5410" s="T55">очень</ta>
            <ta e="T57" id="Seg_5411" s="T56">гостиница-DAT/LOC</ta>
            <ta e="T58" id="Seg_5412" s="T57">дом-VBZ-PST1-3PL</ta>
            <ta e="T59" id="Seg_5413" s="T58">имя-3SG.[NOM]</ta>
            <ta e="T60" id="Seg_5414" s="T59">быть-PST1-3SG</ta>
            <ta e="T61" id="Seg_5415" s="T60">семь</ta>
            <ta e="T62" id="Seg_5416" s="T61">холм.[NOM]</ta>
            <ta e="T65" id="Seg_5417" s="T64">очень</ta>
            <ta e="T66" id="Seg_5418" s="T65">хороший-ADVZ</ta>
            <ta e="T67" id="Seg_5419" s="T66">1PL-ACC</ta>
            <ta e="T68" id="Seg_5420" s="T67">есть-EP-CAUS-PRS-3PL</ta>
            <ta e="T69" id="Seg_5421" s="T68">там</ta>
            <ta e="T70" id="Seg_5422" s="T69">есть-EP-CAUS-PST2-3PL</ta>
            <ta e="T71" id="Seg_5423" s="T70">спать-CAUS-PST2-3PL</ta>
            <ta e="T72" id="Seg_5424" s="T71">потом</ta>
            <ta e="T73" id="Seg_5425" s="T72">следующее.утро-3SG-ACC</ta>
            <ta e="T74" id="Seg_5426" s="T73">репетиция-VBZ-PST2-1PL</ta>
            <ta e="T75" id="Seg_5427" s="T74">тот.EMPH.[NOM]</ta>
            <ta e="T76" id="Seg_5428" s="T75">вечер-3SG-ACC</ta>
            <ta e="T77" id="Seg_5429" s="T76">тот.EMPH.[NOM]</ta>
            <ta e="T78" id="Seg_5430" s="T77">вечер-3SG-ACC</ta>
            <ta e="T79" id="Seg_5431" s="T78">EMPH</ta>
            <ta e="T80" id="Seg_5432" s="T79">выступать-VBZ-PST2-1PL</ta>
            <ta e="T81" id="Seg_5433" s="T80">тот.[NOM]</ta>
            <ta e="T82" id="Seg_5434" s="T81">задняя.часть-3SG-ABL</ta>
            <ta e="T83" id="Seg_5435" s="T82">круглый</ta>
            <ta e="T84" id="Seg_5436" s="T83">стол.[NOM]</ta>
            <ta e="T85" id="Seg_5437" s="T84">быть-PST2-3SG</ta>
            <ta e="T86" id="Seg_5438" s="T85">два-ORD</ta>
            <ta e="T87" id="Seg_5439" s="T86">день-3SG-DAT/LOC</ta>
            <ta e="T88" id="Seg_5440" s="T87">тот</ta>
            <ta e="T89" id="Seg_5441" s="T88">круглый</ta>
            <ta e="T90" id="Seg_5442" s="T89">стол-DAT/LOC</ta>
            <ta e="T91" id="Seg_5443" s="T90">MOD</ta>
            <ta e="T92" id="Seg_5444" s="T91">разный</ta>
            <ta e="T93" id="Seg_5445" s="T92">ээ</ta>
            <ta e="T94" id="Seg_5446" s="T93">много</ta>
            <ta e="T95" id="Seg_5447" s="T94">человек.[NOM]</ta>
            <ta e="T96" id="Seg_5448" s="T95">собирать-PASS/REFL-EP-PST2-3SG</ta>
            <ta e="T97" id="Seg_5449" s="T96">радоваться-NMNZ-ACC</ta>
            <ta e="T98" id="Seg_5450" s="T97">плохой-ACC</ta>
            <ta e="T99" id="Seg_5451" s="T98">ээ</ta>
            <ta e="T100" id="Seg_5452" s="T99">жизнь.[NOM]</ta>
            <ta e="T101" id="Seg_5453" s="T100">сторона-3SG-INSTR</ta>
            <ta e="T102" id="Seg_5454" s="T101">разговаривать-NMNZ.[NOM]</ta>
            <ta e="T103" id="Seg_5455" s="T102">есть</ta>
            <ta e="T104" id="Seg_5456" s="T103">быть-PST1-3SG</ta>
            <ta e="T105" id="Seg_5457" s="T104">ребенок-PL.[NOM]</ta>
            <ta e="T106" id="Seg_5458" s="T105">учиться-PTCP.PRS-3PL-GEN</ta>
            <ta e="T107" id="Seg_5459" s="T106">сторона-3PL-INSTR</ta>
            <ta e="T108" id="Seg_5460" s="T107">студент-PL.[NOM]</ta>
            <ta e="T109" id="Seg_5461" s="T108">ээ</ta>
            <ta e="T110" id="Seg_5462" s="T109">тот</ta>
            <ta e="T111" id="Seg_5463" s="T110">большой</ta>
            <ta e="T112" id="Seg_5464" s="T111">высшее.учебное.заведение-PL.[NOM]</ta>
            <ta e="T113" id="Seg_5465" s="T112">сторона-3PL-DAT/LOC</ta>
            <ta e="T114" id="Seg_5466" s="T113">почему</ta>
            <ta e="T115" id="Seg_5467" s="T114">бросать-PTCP.PRS-3PL-ACC</ta>
            <ta e="T116" id="Seg_5468" s="T115">или</ta>
            <ta e="T117" id="Seg_5469" s="T116">почему</ta>
            <ta e="T118" id="Seg_5470" s="T117">учиться-NEG.PTCP-3PL-ACC</ta>
            <ta e="T119" id="Seg_5471" s="T118">тот</ta>
            <ta e="T120" id="Seg_5472" s="T119">тот</ta>
            <ta e="T121" id="Seg_5473" s="T120">сторона-3SG-DAT/LOC</ta>
            <ta e="T122" id="Seg_5474" s="T121">ээ</ta>
            <ta e="T123" id="Seg_5475" s="T122">большой</ta>
            <ta e="T124" id="Seg_5476" s="T123">потом</ta>
            <ta e="T125" id="Seg_5477" s="T124">вопрос.[NOM]</ta>
            <ta e="T126" id="Seg_5478" s="T125">стоять-PTCP.PRS</ta>
            <ta e="T127" id="Seg_5479" s="T126">быть-PST1-3SG</ta>
            <ta e="T128" id="Seg_5480" s="T127">потом</ta>
            <ta e="T129" id="Seg_5481" s="T128">MOD</ta>
            <ta e="T131" id="Seg_5482" s="T130">ээ</ta>
            <ta e="T132" id="Seg_5483" s="T131">старуха.[NOM]</ta>
            <ta e="T133" id="Seg_5484" s="T132">выступать-VBZ-PST2.[3SG]</ta>
            <ta e="T134" id="Seg_5485" s="T133">там</ta>
            <ta e="T135" id="Seg_5486" s="T134">Молданова</ta>
            <ta e="T136" id="Seg_5487" s="T135">говорить-CVB.SEQ</ta>
            <ta e="T137" id="Seg_5488" s="T136">Мария</ta>
            <ta e="T138" id="Seg_5489" s="T137">Кузьминична</ta>
            <ta e="T139" id="Seg_5490" s="T138">ум-PROPR</ta>
            <ta e="T140" id="Seg_5491" s="T139">жена.[NOM]</ta>
            <ta e="T141" id="Seg_5492" s="T140">тот.[NOM]</ta>
            <ta e="T142" id="Seg_5493" s="T141">говорить-PST2-3SG</ta>
            <ta e="T143" id="Seg_5494" s="T142">ээ</ta>
            <ta e="T144" id="Seg_5495" s="T143">молодой</ta>
            <ta e="T145" id="Seg_5496" s="T144">человек-PL.[NOM]</ta>
            <ta e="T146" id="Seg_5497" s="T145">ээ</ta>
            <ta e="T147" id="Seg_5498" s="T146">молодой</ta>
            <ta e="T148" id="Seg_5499" s="T147">человек-PL.[NOM]</ta>
            <ta e="T149" id="Seg_5500" s="T148">сам-3PL-GEN</ta>
            <ta e="T150" id="Seg_5501" s="T149">семья-3PL-DAT/LOC</ta>
            <ta e="T151" id="Seg_5502" s="T150">сам-3PL-GEN</ta>
            <ta e="T152" id="Seg_5503" s="T151">семья-3PL-GEN</ta>
            <ta e="T153" id="Seg_5504" s="T152">нутро-3PL-DAT/LOC</ta>
            <ta e="T154" id="Seg_5505" s="T153">нутро-3PL-ABL</ta>
            <ta e="T155" id="Seg_5506" s="T154">воспитание.[NOM]</ta>
            <ta e="T156" id="Seg_5507" s="T155">быть-PRS-NEC.[3SG]</ta>
            <ta e="T157" id="Seg_5508" s="T156">родить-EP-CAUS-PTCP.PST</ta>
            <ta e="T158" id="Seg_5509" s="T157">человек.[NOM]</ta>
            <ta e="T159" id="Seg_5510" s="T158">давать-PRS-NEC.[3SG]</ta>
            <ta e="T160" id="Seg_5511" s="T159">учеба-ACC</ta>
            <ta e="T161" id="Seg_5512" s="T160">добрый-ACC</ta>
            <ta e="T162" id="Seg_5513" s="T161">плохой-ACC</ta>
            <ta e="T163" id="Seg_5514" s="T162">ребенок-DAT/LOC</ta>
            <ta e="T164" id="Seg_5515" s="T163">ах</ta>
            <ta e="T165" id="Seg_5516" s="T164">школа.[NOM]</ta>
            <ta e="T166" id="Seg_5517" s="T165">садик.[NOM]</ta>
            <ta e="T167" id="Seg_5518" s="T166">тот-DAT/LOC</ta>
            <ta e="T168" id="Seg_5519" s="T167">MOD</ta>
            <ta e="T169" id="Seg_5520" s="T168">ээ</ta>
            <ta e="T170" id="Seg_5521" s="T169">учебное.заведение-PL.[NOM]</ta>
            <ta e="T171" id="Seg_5522" s="T170">тот-DAT/LOC</ta>
            <ta e="T172" id="Seg_5523" s="T171">помощь.[NOM]</ta>
            <ta e="T173" id="Seg_5524" s="T172">только</ta>
            <ta e="T174" id="Seg_5525" s="T173">быть-PTCP.FUT-3PL-ACC</ta>
            <ta e="T175" id="Seg_5526" s="T174">надо</ta>
            <ta e="T176" id="Seg_5527" s="T175">а</ta>
            <ta e="T180" id="Seg_5528" s="T179">подобно</ta>
            <ta e="T181" id="Seg_5529" s="T180">быть-PTCP.FUT-3PL-ACC</ta>
            <ta e="T182" id="Seg_5530" s="T181">тот.[NOM]</ta>
            <ta e="T183" id="Seg_5531" s="T182">из_за</ta>
            <ta e="T184" id="Seg_5532" s="T183">MOD</ta>
            <ta e="T185" id="Seg_5533" s="T184">теперь</ta>
            <ta e="T186" id="Seg_5534" s="T185">передняя.часть.[NOM]</ta>
            <ta e="T187" id="Seg_5535" s="T186">к</ta>
            <ta e="T188" id="Seg_5536" s="T187">ээ</ta>
            <ta e="T189" id="Seg_5537" s="T188">расти-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T190" id="Seg_5538" s="T189">надо</ta>
            <ta e="T191" id="Seg_5539" s="T190">говорить-PRS.[3SG]</ta>
            <ta e="T192" id="Seg_5540" s="T191">ээ</ta>
            <ta e="T193" id="Seg_5541" s="T192">тот</ta>
            <ta e="T194" id="Seg_5542" s="T193">жена.[NOM]</ta>
            <ta e="T195" id="Seg_5543" s="T194">ответственность-ACC</ta>
            <ta e="T196" id="Seg_5544" s="T195">родить-EP-CAUS-PTCP.PRS</ta>
            <ta e="T197" id="Seg_5545" s="T196">человек-PL-ABL</ta>
            <ta e="T198" id="Seg_5546" s="T197">родитель-PL-ABL</ta>
            <ta e="T199" id="Seg_5547" s="T198">да</ta>
            <ta e="T200" id="Seg_5548" s="T199">и</ta>
            <ta e="T201" id="Seg_5549" s="T200">добрый-ACC</ta>
            <ta e="T202" id="Seg_5550" s="T201">умножаться-CAUS-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T203" id="Seg_5551" s="T202">говорить-CVB.SEQ</ta>
            <ta e="T204" id="Seg_5552" s="T203">говорить-PST2-3SG</ta>
            <ta e="T205" id="Seg_5553" s="T204">тот</ta>
            <ta e="T206" id="Seg_5554" s="T205">жена.[NOM]</ta>
            <ta e="T207" id="Seg_5555" s="T206">добрый-ACC</ta>
            <ta e="T208" id="Seg_5556" s="T207">умножаться-CAUS-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T209" id="Seg_5557" s="T208">а</ta>
            <ta e="T210" id="Seg_5558" s="T209">плохой-ACC</ta>
            <ta e="T211" id="Seg_5559" s="T210">убывать-CAUS-CAUS-PTCP.FUT-DAT/LOC</ta>
            <ta e="T212" id="Seg_5560" s="T211">этот</ta>
            <ta e="T213" id="Seg_5561" s="T212">земля-DAT/LOC</ta>
            <ta e="T214" id="Seg_5562" s="T213">вот</ta>
            <ta e="T215" id="Seg_5563" s="T214">ну</ta>
            <ta e="T216" id="Seg_5564" s="T215">устанавливать-CVB.SEQ</ta>
            <ta e="T217" id="Seg_5565" s="T216">видеть-PTCP.COND-DAT/LOC</ta>
            <ta e="T218" id="Seg_5566" s="T217">если</ta>
            <ta e="T219" id="Seg_5567" s="T218">2SG-ACC</ta>
            <ta e="T220" id="Seg_5568" s="T219">плохой-ADVZ</ta>
            <ta e="T221" id="Seg_5569" s="T220">говорить-PST1-3PL</ta>
            <ta e="T222" id="Seg_5570" s="T221">плохой-ACC</ta>
            <ta e="T223" id="Seg_5571" s="T222">делать-PST1-3PL</ta>
            <ta e="T224" id="Seg_5572" s="T223">Q</ta>
            <ta e="T225" id="Seg_5573" s="T224">2SG.[NOM]</ta>
            <ta e="T226" id="Seg_5574" s="T225">EMPH</ta>
            <ta e="T227" id="Seg_5575" s="T226">рассказывать-NEG-PTCP.FUT-2SG-ACC</ta>
            <ta e="T228" id="Seg_5576" s="T227">надо</ta>
            <ta e="T229" id="Seg_5577" s="T228">тот</ta>
            <ta e="T230" id="Seg_5578" s="T229">человек.[NOM]</ta>
            <ta e="T231" id="Seg_5579" s="T230">сторона-3SG-INSTR</ta>
            <ta e="T232" id="Seg_5580" s="T231">говорить-PRS.[3SG]</ta>
            <ta e="T233" id="Seg_5581" s="T232">тот</ta>
            <ta e="T234" id="Seg_5582" s="T233">жена.[NOM]</ta>
            <ta e="T235" id="Seg_5583" s="T234">тот</ta>
            <ta e="T236" id="Seg_5584" s="T235">человек.[NOM]</ta>
            <ta e="T237" id="Seg_5585" s="T236">сторона-3SG</ta>
            <ta e="T238" id="Seg_5586" s="T237">тот</ta>
            <ta e="T239" id="Seg_5587" s="T238">человек.[NOM]</ta>
            <ta e="T240" id="Seg_5588" s="T239">сторона-3SG-INSTR</ta>
            <ta e="T241" id="Seg_5589" s="T240">сам-3SG-DAT/LOC</ta>
            <ta e="T242" id="Seg_5590" s="T241">говорить-EP-NEG-PTCP.FUT-2SG-ACC</ta>
            <ta e="T243" id="Seg_5591" s="T242">надо</ta>
            <ta e="T244" id="Seg_5592" s="T243">но</ta>
            <ta e="T245" id="Seg_5593" s="T244">человек-PL-DAT/LOC</ta>
            <ta e="T246" id="Seg_5594" s="T245">тоже</ta>
            <ta e="T247" id="Seg_5595" s="T246">рассказывать-NEG-PTCP.FUT-DAT/LOC</ta>
            <ta e="T248" id="Seg_5596" s="T247">надо</ta>
            <ta e="T249" id="Seg_5597" s="T248">чтобы</ta>
            <ta e="T250" id="Seg_5598" s="T249">плохой.[NOM]</ta>
            <ta e="T251" id="Seg_5599" s="T250">расти-EP-NEG-PTCP.FUT-3SG-ACC</ta>
            <ta e="T252" id="Seg_5600" s="T251">а</ta>
            <ta e="T253" id="Seg_5601" s="T252">хороший-ADVZ</ta>
            <ta e="T254" id="Seg_5602" s="T253">делать-PTCP.PST</ta>
            <ta e="T255" id="Seg_5603" s="T254">человек-DAT/LOC</ta>
            <ta e="T256" id="Seg_5604" s="T255">MOD</ta>
            <ta e="T257" id="Seg_5605" s="T256">тот</ta>
            <ta e="T258" id="Seg_5606" s="T257">человек-DAT/LOC</ta>
            <ta e="T260" id="Seg_5607" s="T259">тоже</ta>
            <ta e="T261" id="Seg_5608" s="T260">сам-3SG-DAT/LOC</ta>
            <ta e="T262" id="Seg_5609" s="T261">говорить-EP-NEG-PTCP.FUT-2SG-ACC</ta>
            <ta e="T263" id="Seg_5610" s="T262">чтобы</ta>
            <ta e="T264" id="Seg_5611" s="T263">похвастаться-NEG-PTCP.FUT-3SG-ACC</ta>
            <ta e="T265" id="Seg_5612" s="T264">а</ta>
            <ta e="T266" id="Seg_5613" s="T265">другой</ta>
            <ta e="T267" id="Seg_5614" s="T266">человек-PL-DAT/LOC</ta>
            <ta e="T268" id="Seg_5615" s="T267">рассказывать-PTCP.FUT-2SG-ACC</ta>
            <ta e="T269" id="Seg_5616" s="T268">надо</ta>
            <ta e="T270" id="Seg_5617" s="T269">что</ta>
            <ta e="T271" id="Seg_5618" s="T270">тот</ta>
            <ta e="T272" id="Seg_5619" s="T271">человек.[NOM]</ta>
            <ta e="T273" id="Seg_5620" s="T272">хороший-ACC</ta>
            <ta e="T274" id="Seg_5621" s="T273">делать-PST2-3SG</ta>
            <ta e="T275" id="Seg_5622" s="T274">говорить-CVB.SEQ</ta>
            <ta e="T276" id="Seg_5623" s="T275">тот</ta>
            <ta e="T277" id="Seg_5624" s="T276">человек.[NOM]</ta>
            <ta e="T278" id="Seg_5625" s="T277">такой.[NOM]</ta>
            <ta e="T279" id="Seg_5626" s="T278">говорить-CVB.SEQ</ta>
            <ta e="T280" id="Seg_5627" s="T279">тот</ta>
            <ta e="T281" id="Seg_5628" s="T280">человек.[NOM]</ta>
            <ta e="T282" id="Seg_5629" s="T281">такой.[NOM]</ta>
            <ta e="T283" id="Seg_5630" s="T282">говорить-CVB.SEQ</ta>
            <ta e="T285" id="Seg_5631" s="T284">тогда</ta>
            <ta e="T286" id="Seg_5632" s="T285">MOD</ta>
            <ta e="T287" id="Seg_5633" s="T286">добрый.[NOM]</ta>
            <ta e="T288" id="Seg_5634" s="T287">расти-PRS.[3SG]</ta>
            <ta e="T289" id="Seg_5635" s="T288">добрый.[NOM]</ta>
            <ta e="T290" id="Seg_5636" s="T289">расти-PRS.[3SG]</ta>
            <ta e="T291" id="Seg_5637" s="T290">такой</ta>
            <ta e="T292" id="Seg_5638" s="T291">формула-INSTR</ta>
            <ta e="T293" id="Seg_5639" s="T292">говорить-CVB.SEQ</ta>
            <ta e="T294" id="Seg_5640" s="T293">но</ta>
            <ta e="T295" id="Seg_5641" s="T294">опять</ta>
            <ta e="T296" id="Seg_5642" s="T295">так</ta>
            <ta e="T297" id="Seg_5643" s="T296">и</ta>
            <ta e="T298" id="Seg_5644" s="T297">тот</ta>
            <ta e="T299" id="Seg_5645" s="T298">формула-DAT/LOC</ta>
            <ta e="T300" id="Seg_5646" s="T299">1SG.[NOM]</ta>
            <ta e="T301" id="Seg_5647" s="T300">согласие-VBZ-NEG-1SG</ta>
            <ta e="T302" id="Seg_5648" s="T301">почему</ta>
            <ta e="T303" id="Seg_5649" s="T302">говорить-CVB.SEQ-2PL</ta>
            <ta e="T304" id="Seg_5650" s="T303">плохой.[NOM]</ta>
            <ta e="T305" id="Seg_5651" s="T304">два-ORD</ta>
            <ta e="T306" id="Seg_5652" s="T305">формула.[NOM]</ta>
            <ta e="T307" id="Seg_5653" s="T306">есть</ta>
            <ta e="T308" id="Seg_5654" s="T307">тот</ta>
            <ta e="T309" id="Seg_5655" s="T308">верхняя.часть-3SG-DAT/LOC</ta>
            <ta e="T310" id="Seg_5656" s="T309">тогда</ta>
            <ta e="T311" id="Seg_5657" s="T310">плохой.[NOM]</ta>
            <ta e="T312" id="Seg_5658" s="T311">расти-EP-NEG-PTCP.FUT-3SG-ACC</ta>
            <ta e="T313" id="Seg_5659" s="T312">плохой-ACC</ta>
            <ta e="T314" id="Seg_5660" s="T313">передняя.часть.[NOM]</ta>
            <ta e="T315" id="Seg_5661" s="T314">к</ta>
            <ta e="T316" id="Seg_5662" s="T315">назад</ta>
            <ta e="T317" id="Seg_5663" s="T316">большой-ACC</ta>
            <ta e="T318" id="Seg_5664" s="T317">ответ.[NOM]</ta>
            <ta e="T319" id="Seg_5665" s="T318">давать-PTCP.FUT-DAT/LOC</ta>
            <ta e="T320" id="Seg_5666" s="T319">надо</ta>
            <ta e="T321" id="Seg_5667" s="T320">3SG-DAT/LOC</ta>
            <ta e="T322" id="Seg_5668" s="T321">чтобы</ta>
            <ta e="T323" id="Seg_5669" s="T322">плохой-ACC</ta>
            <ta e="T324" id="Seg_5670" s="T323">делать-PTCP.PST</ta>
            <ta e="T325" id="Seg_5671" s="T324">человек.[NOM]</ta>
            <ta e="T326" id="Seg_5672" s="T325">знать-PTCP.FUT-3SG-ACC</ta>
            <ta e="T327" id="Seg_5673" s="T326">что</ta>
            <ta e="T328" id="Seg_5674" s="T327">просто</ta>
            <ta e="T329" id="Seg_5675" s="T328">человек-ACC</ta>
            <ta e="T330" id="Seg_5676" s="T329">его</ta>
            <ta e="T331" id="Seg_5677" s="T330">плохой-3SG.[NOM]</ta>
            <ta e="T332" id="Seg_5678" s="T331">просто</ta>
            <ta e="T333" id="Seg_5679" s="T332">проехать-FUT.[3SG]</ta>
            <ta e="T334" id="Seg_5680" s="T333">NEG-3SG</ta>
            <ta e="T335" id="Seg_5681" s="T334">3SG-DAT/LOC</ta>
            <ta e="T336" id="Seg_5682" s="T335">3SG.[NOM]</ta>
            <ta e="T337" id="Seg_5683" s="T336">MOD</ta>
            <ta e="T338" id="Seg_5684" s="T337">человек.[NOM]</ta>
            <ta e="T339" id="Seg_5685" s="T338">передняя.часть-3SG-DAT/LOC</ta>
            <ta e="T340" id="Seg_5686" s="T339">ответственность-VBZ-PRS-NEC.[3SG]</ta>
            <ta e="T341" id="Seg_5687" s="T340">чтобы</ta>
            <ta e="T342" id="Seg_5688" s="T341">передняя.часть.[NOM]</ta>
            <ta e="T343" id="Seg_5689" s="T342">к</ta>
            <ta e="T344" id="Seg_5690" s="T343">плохой-ACC</ta>
            <ta e="T345" id="Seg_5691" s="T344">делать-EP-NEG-PTCP.FUT-3SG-ACC</ta>
            <ta e="T346" id="Seg_5692" s="T345">тогда</ta>
            <ta e="T347" id="Seg_5693" s="T346">MOD</ta>
            <ta e="T348" id="Seg_5694" s="T347">добрый.[NOM]</ta>
            <ta e="T349" id="Seg_5695" s="T348">два-MLTP</ta>
            <ta e="T350" id="Seg_5696" s="T349">раз.[NOM]</ta>
            <ta e="T351" id="Seg_5697" s="T350">расти-CVB.SEQ</ta>
            <ta e="T352" id="Seg_5698" s="T351">выйти-FUT-3SG</ta>
            <ta e="T353" id="Seg_5699" s="T352">и</ta>
            <ta e="T354" id="Seg_5700" s="T353">жить-PTCP.HAB</ta>
            <ta e="T355" id="Seg_5701" s="T354">человек-PL.[NOM]</ta>
            <ta e="T356" id="Seg_5702" s="T355">там</ta>
            <ta e="T357" id="Seg_5703" s="T356">жить-PTCP.HAB</ta>
            <ta e="T358" id="Seg_5704" s="T357">человек-PL.[NOM]</ta>
            <ta e="T359" id="Seg_5705" s="T358">сам-3PL-GEN</ta>
            <ta e="T360" id="Seg_5706" s="T359">передняя.часть.[NOM]</ta>
            <ta e="T361" id="Seg_5707" s="T360">к</ta>
            <ta e="T362" id="Seg_5708" s="T361">сам-3PL-GEN</ta>
            <ta e="T363" id="Seg_5709" s="T362">передняя.часть.[NOM]</ta>
            <ta e="T364" id="Seg_5710" s="T363">к</ta>
            <ta e="T365" id="Seg_5711" s="T364">обижать-EP-CAUS-FUT-3PL</ta>
            <ta e="T366" id="Seg_5712" s="T365">NEG-3PL</ta>
            <ta e="T367" id="Seg_5713" s="T366">NEG</ta>
            <ta e="T368" id="Seg_5714" s="T367">обижать-EP-CAUS-FUT-3PL</ta>
            <ta e="T369" id="Seg_5715" s="T368">NEG-3PL</ta>
            <ta e="T370" id="Seg_5716" s="T369">ах</ta>
            <ta e="T371" id="Seg_5717" s="T370">но</ta>
            <ta e="T372" id="Seg_5718" s="T371">потом</ta>
            <ta e="T373" id="Seg_5719" s="T372">тот</ta>
            <ta e="T374" id="Seg_5720" s="T373">тот</ta>
            <ta e="T375" id="Seg_5721" s="T374">круглый</ta>
            <ta e="T376" id="Seg_5722" s="T375">круглый</ta>
            <ta e="T377" id="Seg_5723" s="T376">стол.[NOM]</ta>
            <ta e="T378" id="Seg_5724" s="T377">кончать-PTCP.PST-3SG-ACC</ta>
            <ta e="T379" id="Seg_5725" s="T378">после.того</ta>
            <ta e="T380" id="Seg_5726" s="T379">тоже</ta>
            <ta e="T381" id="Seg_5727" s="T380">концерт.[NOM]</ta>
            <ta e="T384" id="Seg_5728" s="T383">говорить-CVB.SEQ</ta>
            <ta e="T385" id="Seg_5729" s="T384">быть-PST2-3SG</ta>
            <ta e="T386" id="Seg_5730" s="T385">там</ta>
            <ta e="T387" id="Seg_5731" s="T386">очень</ta>
            <ta e="T388" id="Seg_5732" s="T387">хороший-ADVZ</ta>
            <ta e="T389" id="Seg_5733" s="T388">выступать-VBZ-PST2-3PL</ta>
            <ta e="T390" id="Seg_5734" s="T389">много</ta>
            <ta e="T391" id="Seg_5735" s="T390">коллектив.[NOM]</ta>
            <ta e="T392" id="Seg_5736" s="T391">приходить-PST2-3SG</ta>
            <ta e="T393" id="Seg_5737" s="T392">ээ</ta>
            <ta e="T394" id="Seg_5738" s="T393">Ханты_Мансийск-ABL</ta>
            <ta e="T395" id="Seg_5739" s="T394">сам-3SG-ABL</ta>
            <ta e="T396" id="Seg_5740" s="T395">театральный</ta>
            <ta e="T397" id="Seg_5741" s="T396">группа.[NOM]</ta>
            <ta e="T398" id="Seg_5742" s="T397">есть</ta>
            <ta e="T399" id="Seg_5743" s="T398">быть-PST1-3SG</ta>
            <ta e="T400" id="Seg_5744" s="T399">наш</ta>
            <ta e="T401" id="Seg_5745" s="T400">ансамбль.[NOM]</ta>
            <ta e="T402" id="Seg_5746" s="T401">долганский.танец-1PL.[NOM]</ta>
            <ta e="T403" id="Seg_5747" s="T402">подобно</ta>
            <ta e="T404" id="Seg_5748" s="T403">потом</ta>
            <ta e="T405" id="Seg_5749" s="T404">MOD</ta>
            <ta e="T406" id="Seg_5750" s="T405">Магадан-ABL</ta>
            <ta e="T407" id="Seg_5751" s="T406">приходить-PST2-3PL</ta>
            <ta e="T408" id="Seg_5752" s="T407">ээ</ta>
            <ta e="T409" id="Seg_5753" s="T408">Ямало_Ненецкий.Автономный.Округ-ABL</ta>
            <ta e="T410" id="Seg_5754" s="T409">Чукотка-ABL</ta>
            <ta e="T411" id="Seg_5755" s="T410">ансамбль.[NOM]</ta>
            <ta e="T412" id="Seg_5756" s="T411">есть</ta>
            <ta e="T413" id="Seg_5757" s="T412">быть-PST1-3SG</ta>
            <ta e="T414" id="Seg_5758" s="T413">далекий</ta>
            <ta e="T415" id="Seg_5759" s="T414">место-ABL</ta>
            <ta e="T416" id="Seg_5760" s="T415">приходить-FREQ-PST2-3PL</ta>
            <ta e="T417" id="Seg_5761" s="T416">потом</ta>
            <ta e="T418" id="Seg_5762" s="T417">много-много</ta>
            <ta e="T419" id="Seg_5763" s="T418">место-ABL</ta>
            <ta e="T420" id="Seg_5764" s="T419">приходить-FREQ-PST2-3PL</ta>
            <ta e="T421" id="Seg_5765" s="T420">туда</ta>
            <ta e="T422" id="Seg_5766" s="T421">двадцать</ta>
            <ta e="T423" id="Seg_5767" s="T422">два</ta>
            <ta e="T424" id="Seg_5768" s="T423">коллектив.[NOM]</ta>
            <ta e="T425" id="Seg_5769" s="T424">есть</ta>
            <ta e="T426" id="Seg_5770" s="T425">быть-PST1-3SG</ta>
            <ta e="T427" id="Seg_5771" s="T426">ээ</ta>
            <ta e="T428" id="Seg_5772" s="T427">потом</ta>
            <ta e="T429" id="Seg_5773" s="T428">куратор-PL-1PL.[NOM]</ta>
            <ta e="T430" id="Seg_5774" s="T429">хороший</ta>
            <ta e="T431" id="Seg_5775" s="T430">очень</ta>
            <ta e="T432" id="Seg_5776" s="T431">жена-PL.[NOM]</ta>
            <ta e="T433" id="Seg_5777" s="T432">быть-PST1-3PL</ta>
            <ta e="T434" id="Seg_5778" s="T433">1PL-ACC</ta>
            <ta e="T435" id="Seg_5779" s="T434">целый-3SG-ACC</ta>
            <ta e="T436" id="Seg_5780" s="T435">ээ</ta>
            <ta e="T437" id="Seg_5781" s="T436">уносить-CVB.SEQ-3PL</ta>
            <ta e="T438" id="Seg_5782" s="T437">принести-CVB.SEQ-3PL</ta>
            <ta e="T439" id="Seg_5783" s="T438">маленький</ta>
            <ta e="T440" id="Seg_5784" s="T439">ребенок-SIM</ta>
            <ta e="T441" id="Seg_5785" s="T440">спать-CAUS-CVB.SEQ-3PL</ta>
            <ta e="T442" id="Seg_5786" s="T441">потом</ta>
            <ta e="T443" id="Seg_5787" s="T442">есть-EP-CAUS-CVB.SEQ-3PL</ta>
            <ta e="T444" id="Seg_5788" s="T443">потом</ta>
            <ta e="T445" id="Seg_5789" s="T444">музей-PL.[NOM]</ta>
            <ta e="T446" id="Seg_5790" s="T445">через</ta>
            <ta e="T447" id="Seg_5791" s="T446">теплоход-DAT/LOC</ta>
            <ta e="T448" id="Seg_5792" s="T447">идти-EP-PST2-1PL</ta>
            <ta e="T449" id="Seg_5793" s="T448">Иртыш</ta>
            <ta e="T450" id="Seg_5794" s="T449">говорить-CVB.SEQ</ta>
            <ta e="T451" id="Seg_5795" s="T450">ээ</ta>
            <ta e="T452" id="Seg_5796" s="T451">река.[NOM]</ta>
            <ta e="T453" id="Seg_5797" s="T452">Иртыш</ta>
            <ta e="T454" id="Seg_5798" s="T453">говорить-CVB.SEQ</ta>
            <ta e="T455" id="Seg_5799" s="T454">река.[NOM]</ta>
            <ta e="T456" id="Seg_5800" s="T455">вдоль</ta>
            <ta e="T457" id="Seg_5801" s="T456">идти-PST2-1PL</ta>
            <ta e="T458" id="Seg_5802" s="T457">тот</ta>
            <ta e="T459" id="Seg_5803" s="T458">Иртыш</ta>
            <ta e="T460" id="Seg_5804" s="T459">MOD</ta>
            <ta e="T461" id="Seg_5805" s="T460">большой</ta>
            <ta e="T462" id="Seg_5806" s="T461">река-DAT/LOC</ta>
            <ta e="T463" id="Seg_5807" s="T462">объединяться-PRS.[3SG]</ta>
            <ta e="T464" id="Seg_5808" s="T463">Обь</ta>
            <ta e="T465" id="Seg_5809" s="T464">говорить-CVB.SEQ</ta>
            <ta e="T466" id="Seg_5810" s="T465">река-DAT/LOC</ta>
            <ta e="T467" id="Seg_5811" s="T466">тот</ta>
            <ta e="T468" id="Seg_5812" s="T467">объединяться-EP-PTCP.PST-PL-DAT/LOC</ta>
            <ta e="T469" id="Seg_5813" s="T468">подарок-VBZ-PST2-1PL</ta>
            <ta e="T471" id="Seg_5814" s="T470">песня-PL-ACC</ta>
            <ta e="T472" id="Seg_5815" s="T471">петь-PST2-1PL</ta>
            <ta e="T473" id="Seg_5816" s="T472">приходить-CVB.PURP-1PL</ta>
            <ta e="T474" id="Seg_5817" s="T473">идти-CVB.PURP-1PL</ta>
            <ta e="T475" id="Seg_5818" s="T474">веселый.[NOM]</ta>
            <ta e="T476" id="Seg_5819" s="T475">очень</ta>
            <ta e="T477" id="Seg_5820" s="T476">быть-PST1-3SG</ta>
            <ta e="T478" id="Seg_5821" s="T477">кино-DAT/LOC</ta>
            <ta e="T479" id="Seg_5822" s="T478">снимать-PST2-3PL</ta>
            <ta e="T480" id="Seg_5823" s="T479">1PL-ACC</ta>
            <ta e="T481" id="Seg_5824" s="T480">потом</ta>
            <ta e="T482" id="Seg_5825" s="T481">потом</ta>
            <ta e="T483" id="Seg_5826" s="T482">назад</ta>
            <ta e="T484" id="Seg_5827" s="T483">приходить-CVB.SEQ-1PL</ta>
            <ta e="T485" id="Seg_5828" s="T484">ээ</ta>
            <ta e="T486" id="Seg_5829" s="T485">тот</ta>
            <ta e="T487" id="Seg_5830" s="T486">следующее.утро-3SG-ACC</ta>
            <ta e="T488" id="Seg_5831" s="T487">тот</ta>
            <ta e="T489" id="Seg_5832" s="T488">три</ta>
            <ta e="T490" id="Seg_5833" s="T489">день-ACC</ta>
            <ta e="T491" id="Seg_5834" s="T490">становиться-PST2-1PL</ta>
            <ta e="T492" id="Seg_5835" s="T491">ээ</ta>
            <ta e="T493" id="Seg_5836" s="T492">три</ta>
            <ta e="T494" id="Seg_5837" s="T493">день-ACC</ta>
            <ta e="T495" id="Seg_5838" s="T494">быть-HAB.[3SG]</ta>
            <ta e="T496" id="Seg_5839" s="T495">ээ</ta>
            <ta e="T497" id="Seg_5840" s="T496">три</ta>
            <ta e="T498" id="Seg_5841" s="T497">день-ACC</ta>
            <ta e="T499" id="Seg_5842" s="T498">быть-PST2-1PL-DAT/LOC</ta>
            <ta e="T500" id="Seg_5843" s="T499">Ханты_Мансийск-DAT/LOC</ta>
            <ta e="T501" id="Seg_5844" s="T500">хороший.[NOM]</ta>
            <ta e="T502" id="Seg_5845" s="T501">очень</ta>
            <ta e="T503" id="Seg_5846" s="T502">день-PL.[NOM]</ta>
            <ta e="T504" id="Seg_5847" s="T503">быть-PST1-3PL</ta>
            <ta e="T505" id="Seg_5848" s="T504">погода.[NOM]</ta>
            <ta e="T506" id="Seg_5849" s="T505">тот.[NOM]</ta>
            <ta e="T507" id="Seg_5850" s="T506">передняя.часть-3SG-DAT/LOC</ta>
            <ta e="T508" id="Seg_5851" s="T507">дождь-VBZ-PTCP.PRS</ta>
            <ta e="T509" id="Seg_5852" s="T508">быть-PST1-3SG</ta>
            <ta e="T510" id="Seg_5853" s="T509">говорить-PTCP.PRS</ta>
            <ta e="T511" id="Seg_5854" s="T510">быть-PST1-3PL</ta>
            <ta e="T512" id="Seg_5855" s="T511">потом</ta>
            <ta e="T513" id="Seg_5856" s="T512">двадцать-ABL</ta>
            <ta e="T514" id="Seg_5857" s="T513">больше</ta>
            <ta e="T515" id="Seg_5858" s="T514">градус.[NOM]</ta>
            <ta e="T516" id="Seg_5859" s="T515">быть-PST1-3SG</ta>
            <ta e="T517" id="Seg_5860" s="T516">двадцать</ta>
            <ta e="T518" id="Seg_5861" s="T517">четыре</ta>
            <ta e="T519" id="Seg_5862" s="T518">двадцать</ta>
            <ta e="T520" id="Seg_5863" s="T519">пять</ta>
            <ta e="T521" id="Seg_5864" s="T520">градус.[NOM]</ta>
            <ta e="T522" id="Seg_5865" s="T521">быть-CVB.SEQ</ta>
            <ta e="T523" id="Seg_5866" s="T522">идти-PST1-3SG</ta>
            <ta e="T524" id="Seg_5867" s="T523">тот</ta>
            <ta e="T525" id="Seg_5868" s="T524">день-PL-DAT/LOC</ta>
            <ta e="T526" id="Seg_5869" s="T525">тот.[NOM]</ta>
            <ta e="T527" id="Seg_5870" s="T526">из_за</ta>
            <ta e="T528" id="Seg_5871" s="T527">MOD</ta>
            <ta e="T529" id="Seg_5872" s="T528">1PL-DAT/LOC</ta>
            <ta e="T530" id="Seg_5873" s="T529">очень</ta>
            <ta e="T531" id="Seg_5874" s="T530">NEG</ta>
            <ta e="T532" id="Seg_5875" s="T531">теплый-POSS</ta>
            <ta e="T533" id="Seg_5876" s="T532">NEG</ta>
            <ta e="T534" id="Seg_5877" s="T533">быть-PST1-3SG</ta>
            <ta e="T535" id="Seg_5878" s="T534">но</ta>
            <ta e="T536" id="Seg_5879" s="T535">очень</ta>
            <ta e="T537" id="Seg_5880" s="T536">NEG</ta>
            <ta e="T538" id="Seg_5881" s="T537">и</ta>
            <ta e="T539" id="Seg_5882" s="T538">холодний-3SG</ta>
            <ta e="T540" id="Seg_5883" s="T539">NEG</ta>
            <ta e="T541" id="Seg_5884" s="T540">быть-PST1-3SG</ta>
            <ta e="T542" id="Seg_5885" s="T541">тот.[NOM]</ta>
            <ta e="T543" id="Seg_5886" s="T542">из_за</ta>
            <ta e="T544" id="Seg_5887" s="T543">очень</ta>
            <ta e="T545" id="Seg_5888" s="T544">хороший.[NOM]</ta>
            <ta e="T546" id="Seg_5889" s="T545">1PL-DAT/LOC</ta>
            <ta e="T547" id="Seg_5890" s="T546">1SG.[NOM]</ta>
            <ta e="T548" id="Seg_5891" s="T547">мысль-1SG-DAT/LOC</ta>
            <ta e="T549" id="Seg_5892" s="T548">такой</ta>
            <ta e="T550" id="Seg_5893" s="T549">климат.[NOM]</ta>
            <ta e="T551" id="Seg_5894" s="T550">3PL-DAT/LOC</ta>
            <ta e="T552" id="Seg_5895" s="T551">такой</ta>
            <ta e="T553" id="Seg_5896" s="T552">погода.[NOM]</ta>
            <ta e="T554" id="Seg_5897" s="T553">очень</ta>
            <ta e="T555" id="Seg_5898" s="T554">хороший-ADVZ</ta>
            <ta e="T556" id="Seg_5899" s="T555">получаться-PRS.[3SG]</ta>
            <ta e="T557" id="Seg_5900" s="T556">тот.[NOM]</ta>
            <ta e="T558" id="Seg_5901" s="T557">из_за</ta>
            <ta e="T559" id="Seg_5902" s="T558">еще</ta>
            <ta e="T560" id="Seg_5903" s="T559">да</ta>
            <ta e="T561" id="Seg_5904" s="T560">зазывать-TEMP-3PL</ta>
            <ta e="T562" id="Seg_5905" s="T561">1SG.[NOM]</ta>
            <ta e="T563" id="Seg_5906" s="T562">мысль-1SG-DAT/LOC</ta>
            <ta e="T564" id="Seg_5907" s="T563">1SG.[NOM]</ta>
            <ta e="T565" id="Seg_5908" s="T564">EMPH</ta>
            <ta e="T566" id="Seg_5909" s="T565">идти-PTCP.PRS</ta>
            <ta e="T567" id="Seg_5910" s="T566">как-1SG</ta>
            <ta e="T568" id="Seg_5911" s="T567">еще</ta>
            <ta e="T569" id="Seg_5912" s="T568">однажды</ta>
            <ta e="T570" id="Seg_5913" s="T569">посещать-CVB.SIM</ta>
         </annotation>
         <annotation name="mc" tierref="mc-UkOA">
            <ta e="T2" id="Seg_5914" s="T1">adj-adj&gt;adv</ta>
            <ta e="T3" id="Seg_5915" s="T2">v-v:tense-v:poss.pn</ta>
            <ta e="T4" id="Seg_5916" s="T3">ptcl</ta>
            <ta e="T10" id="Seg_5917" s="T8">cardnum</ta>
            <ta e="T11" id="Seg_5918" s="T10">cardnum-cardnum&gt;adj</ta>
            <ta e="T12" id="Seg_5919" s="T11">n-n:case</ta>
            <ta e="T13" id="Seg_5920" s="T12">cardnum</ta>
            <ta e="T14" id="Seg_5921" s="T13">cardnum</ta>
            <ta e="T15" id="Seg_5922" s="T14">cardnum-cardnum&gt;adj</ta>
            <ta e="T16" id="Seg_5923" s="T15">n-n:case</ta>
            <ta e="T17" id="Seg_5924" s="T16">propr</ta>
            <ta e="T18" id="Seg_5925" s="T17">n-n:case</ta>
            <ta e="T19" id="Seg_5926" s="T18">v-v:tense-v:poss.pn</ta>
            <ta e="T20" id="Seg_5927" s="T19">n.[n:case]</ta>
            <ta e="T23" id="Seg_5928" s="T22">v-v:cvb</ta>
            <ta e="T24" id="Seg_5929" s="T23">n-n&gt;adj</ta>
            <ta e="T25" id="Seg_5930" s="T24">adv</ta>
            <ta e="T26" id="Seg_5931" s="T25">propr-n:case</ta>
            <ta e="T27" id="Seg_5932" s="T26">pers.[pro:case]</ta>
            <ta e="T28" id="Seg_5933" s="T27">v-v:tense-v:pred.pn</ta>
            <ta e="T29" id="Seg_5934" s="T28">cardnum</ta>
            <ta e="T30" id="Seg_5935" s="T29">n.[n:case]</ta>
            <ta e="T31" id="Seg_5936" s="T30">adj.[n:case]</ta>
            <ta e="T32" id="Seg_5937" s="T31">v-v:tense-v:poss.pn</ta>
            <ta e="T33" id="Seg_5938" s="T32">propr</ta>
            <ta e="T34" id="Seg_5939" s="T33">propr</ta>
            <ta e="T35" id="Seg_5940" s="T34">propr.[n:case]</ta>
            <ta e="T39" id="Seg_5941" s="T38">v-v:cvb</ta>
            <ta e="T40" id="Seg_5942" s="T39">n.[n:case]</ta>
            <ta e="T41" id="Seg_5943" s="T40">n-n&gt;adj</ta>
            <ta e="T45" id="Seg_5944" s="T44">conj</ta>
            <ta e="T46" id="Seg_5945" s="T45">pers.[pro:case]</ta>
            <ta e="T47" id="Seg_5946" s="T46">propr-n:case</ta>
            <ta e="T48" id="Seg_5947" s="T47">adv</ta>
            <ta e="T49" id="Seg_5948" s="T48">adv</ta>
            <ta e="T50" id="Seg_5949" s="T49">adj-adj&gt;adv</ta>
            <ta e="T51" id="Seg_5950" s="T50">pers-pro:case</ta>
            <ta e="T52" id="Seg_5951" s="T51">v-v:tense-v:pred.pn</ta>
            <ta e="T53" id="Seg_5952" s="T52">n-n&gt;adj</ta>
            <ta e="T54" id="Seg_5953" s="T53">n-n:(ins)-n:case</ta>
            <ta e="T55" id="Seg_5954" s="T54">adj</ta>
            <ta e="T56" id="Seg_5955" s="T55">ptcl</ta>
            <ta e="T57" id="Seg_5956" s="T56">n-n:case</ta>
            <ta e="T58" id="Seg_5957" s="T57">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T59" id="Seg_5958" s="T58">n-n:(poss).[n:case]</ta>
            <ta e="T60" id="Seg_5959" s="T59">v-v:tense-v:poss.pn</ta>
            <ta e="T61" id="Seg_5960" s="T60">cardnum</ta>
            <ta e="T62" id="Seg_5961" s="T61">n.[n:case]</ta>
            <ta e="T65" id="Seg_5962" s="T64">adv</ta>
            <ta e="T66" id="Seg_5963" s="T65">adj-adj&gt;adv</ta>
            <ta e="T67" id="Seg_5964" s="T66">pers-pro:case</ta>
            <ta e="T68" id="Seg_5965" s="T67">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T69" id="Seg_5966" s="T68">adv</ta>
            <ta e="T70" id="Seg_5967" s="T69">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T71" id="Seg_5968" s="T70">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T72" id="Seg_5969" s="T71">adv</ta>
            <ta e="T73" id="Seg_5970" s="T72">n-n:poss-n:case</ta>
            <ta e="T74" id="Seg_5971" s="T73">n-n&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T75" id="Seg_5972" s="T74">dempro.[pro:case]</ta>
            <ta e="T76" id="Seg_5973" s="T75">n-n:poss-n:case</ta>
            <ta e="T77" id="Seg_5974" s="T76">dempro.[pro:case]</ta>
            <ta e="T78" id="Seg_5975" s="T77">n-n:poss-n:case</ta>
            <ta e="T79" id="Seg_5976" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_5977" s="T79">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T81" id="Seg_5978" s="T80">dempro.[pro:case]</ta>
            <ta e="T82" id="Seg_5979" s="T81">n-n:poss-n:case</ta>
            <ta e="T83" id="Seg_5980" s="T82">adj</ta>
            <ta e="T84" id="Seg_5981" s="T83">n.[n:case]</ta>
            <ta e="T85" id="Seg_5982" s="T84">v-v:tense-v:poss.pn</ta>
            <ta e="T86" id="Seg_5983" s="T85">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T87" id="Seg_5984" s="T86">n-n:poss-n:case</ta>
            <ta e="T88" id="Seg_5985" s="T87">dempro</ta>
            <ta e="T89" id="Seg_5986" s="T88">adj</ta>
            <ta e="T90" id="Seg_5987" s="T89">n-n:case</ta>
            <ta e="T91" id="Seg_5988" s="T90">ptcl</ta>
            <ta e="T92" id="Seg_5989" s="T91">adj</ta>
            <ta e="T93" id="Seg_5990" s="T92">interj</ta>
            <ta e="T94" id="Seg_5991" s="T93">quant</ta>
            <ta e="T95" id="Seg_5992" s="T94">n.[n:case]</ta>
            <ta e="T96" id="Seg_5993" s="T95">v-v&gt;v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T97" id="Seg_5994" s="T96">v-v&gt;n-n:case</ta>
            <ta e="T98" id="Seg_5995" s="T97">adj-n:case</ta>
            <ta e="T99" id="Seg_5996" s="T98">interj</ta>
            <ta e="T100" id="Seg_5997" s="T99">n.[n:case]</ta>
            <ta e="T101" id="Seg_5998" s="T100">n-n:poss-n:case</ta>
            <ta e="T102" id="Seg_5999" s="T101">v-v&gt;n.[n:case]</ta>
            <ta e="T103" id="Seg_6000" s="T102">ptcl</ta>
            <ta e="T104" id="Seg_6001" s="T103">v-v:tense-v:poss.pn</ta>
            <ta e="T105" id="Seg_6002" s="T104">n-n:(num).[n:case]</ta>
            <ta e="T106" id="Seg_6003" s="T105">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T107" id="Seg_6004" s="T106">n-n:poss-n:case</ta>
            <ta e="T108" id="Seg_6005" s="T107">n-n:(num).[n:case]</ta>
            <ta e="T109" id="Seg_6006" s="T108">interj</ta>
            <ta e="T110" id="Seg_6007" s="T109">dempro</ta>
            <ta e="T111" id="Seg_6008" s="T110">adj</ta>
            <ta e="T112" id="Seg_6009" s="T111">n-n:(num).[n:case]</ta>
            <ta e="T113" id="Seg_6010" s="T112">n-n:poss-n:case</ta>
            <ta e="T114" id="Seg_6011" s="T113">que</ta>
            <ta e="T115" id="Seg_6012" s="T114">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T116" id="Seg_6013" s="T115">conj</ta>
            <ta e="T117" id="Seg_6014" s="T116">que</ta>
            <ta e="T118" id="Seg_6015" s="T117">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T119" id="Seg_6016" s="T118">dempro</ta>
            <ta e="T120" id="Seg_6017" s="T119">dempro</ta>
            <ta e="T121" id="Seg_6018" s="T120">n-n:poss-n:case</ta>
            <ta e="T122" id="Seg_6019" s="T121">interj</ta>
            <ta e="T123" id="Seg_6020" s="T122">adj</ta>
            <ta e="T124" id="Seg_6021" s="T123">adv</ta>
            <ta e="T125" id="Seg_6022" s="T124">n.[n:case]</ta>
            <ta e="T126" id="Seg_6023" s="T125">v-v:ptcp</ta>
            <ta e="T127" id="Seg_6024" s="T126">v-v:tense-v:poss.pn</ta>
            <ta e="T128" id="Seg_6025" s="T127">adv</ta>
            <ta e="T129" id="Seg_6026" s="T128">ptcl</ta>
            <ta e="T131" id="Seg_6027" s="T130">interj</ta>
            <ta e="T132" id="Seg_6028" s="T131">n.[n:case]</ta>
            <ta e="T133" id="Seg_6029" s="T132">v-v&gt;v-v:tense.[v:pred.pn]</ta>
            <ta e="T134" id="Seg_6030" s="T133">adv</ta>
            <ta e="T135" id="Seg_6031" s="T134">propr</ta>
            <ta e="T136" id="Seg_6032" s="T135">v-v:cvb</ta>
            <ta e="T137" id="Seg_6033" s="T136">propr</ta>
            <ta e="T138" id="Seg_6034" s="T137">propr</ta>
            <ta e="T139" id="Seg_6035" s="T138">n-n&gt;adj</ta>
            <ta e="T140" id="Seg_6036" s="T139">n.[n:case]</ta>
            <ta e="T141" id="Seg_6037" s="T140">dempro.[pro:case]</ta>
            <ta e="T142" id="Seg_6038" s="T141">v-v:tense-v:poss.pn</ta>
            <ta e="T143" id="Seg_6039" s="T142">interj</ta>
            <ta e="T144" id="Seg_6040" s="T143">adj</ta>
            <ta e="T145" id="Seg_6041" s="T144">n-n:(num).[n:case]</ta>
            <ta e="T146" id="Seg_6042" s="T145">interj</ta>
            <ta e="T147" id="Seg_6043" s="T146">adj</ta>
            <ta e="T148" id="Seg_6044" s="T147">n-n:(num).[n:case]</ta>
            <ta e="T149" id="Seg_6045" s="T148">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T150" id="Seg_6046" s="T149">n-n:poss-n:case</ta>
            <ta e="T151" id="Seg_6047" s="T150">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T152" id="Seg_6048" s="T151">n-n:poss-n:case</ta>
            <ta e="T153" id="Seg_6049" s="T152">n-n:poss-n:case</ta>
            <ta e="T154" id="Seg_6050" s="T153">n-n:poss-n:case</ta>
            <ta e="T155" id="Seg_6051" s="T154">n.[n:case]</ta>
            <ta e="T156" id="Seg_6052" s="T155">v-v:tense-v:mood.[v:pred.pn]</ta>
            <ta e="T157" id="Seg_6053" s="T156">v-v:(ins)-v&gt;v-v:ptcp</ta>
            <ta e="T158" id="Seg_6054" s="T157">n.[n:case]</ta>
            <ta e="T159" id="Seg_6055" s="T158">v-v:tense-v:mood.[v:pred.pn]</ta>
            <ta e="T160" id="Seg_6056" s="T159">n-n:case</ta>
            <ta e="T161" id="Seg_6057" s="T160">adj-n:case</ta>
            <ta e="T162" id="Seg_6058" s="T161">adj-n:case</ta>
            <ta e="T163" id="Seg_6059" s="T162">n-n:case</ta>
            <ta e="T164" id="Seg_6060" s="T163">interj</ta>
            <ta e="T165" id="Seg_6061" s="T164">n.[n:case]</ta>
            <ta e="T166" id="Seg_6062" s="T165">n.[n:case]</ta>
            <ta e="T167" id="Seg_6063" s="T166">dempro-pro:case</ta>
            <ta e="T168" id="Seg_6064" s="T167">ptcl</ta>
            <ta e="T169" id="Seg_6065" s="T168">interj</ta>
            <ta e="T170" id="Seg_6066" s="T169">n-n:(num).[n:case]</ta>
            <ta e="T171" id="Seg_6067" s="T170">dempro-pro:case</ta>
            <ta e="T172" id="Seg_6068" s="T171">n.[n:case]</ta>
            <ta e="T173" id="Seg_6069" s="T172">ptcl</ta>
            <ta e="T174" id="Seg_6070" s="T173">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T175" id="Seg_6071" s="T174">ptcl</ta>
            <ta e="T176" id="Seg_6072" s="T175">conj</ta>
            <ta e="T180" id="Seg_6073" s="T179">post</ta>
            <ta e="T181" id="Seg_6074" s="T180">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T182" id="Seg_6075" s="T181">dempro.[pro:case]</ta>
            <ta e="T183" id="Seg_6076" s="T182">post</ta>
            <ta e="T184" id="Seg_6077" s="T183">ptcl</ta>
            <ta e="T185" id="Seg_6078" s="T184">adv</ta>
            <ta e="T186" id="Seg_6079" s="T185">n.[n:case]</ta>
            <ta e="T187" id="Seg_6080" s="T186">post</ta>
            <ta e="T188" id="Seg_6081" s="T187">interj</ta>
            <ta e="T189" id="Seg_6082" s="T188">v-v&gt;v-v:ptcp-v:(case)</ta>
            <ta e="T190" id="Seg_6083" s="T189">ptcl</ta>
            <ta e="T191" id="Seg_6084" s="T190">v-v:tense.[v:pred.pn]</ta>
            <ta e="T192" id="Seg_6085" s="T191">interj</ta>
            <ta e="T193" id="Seg_6086" s="T192">dempro</ta>
            <ta e="T194" id="Seg_6087" s="T193">n.[n:case]</ta>
            <ta e="T195" id="Seg_6088" s="T194">n-n:case</ta>
            <ta e="T196" id="Seg_6089" s="T195">v-v:(ins)-v&gt;v-v:ptcp</ta>
            <ta e="T197" id="Seg_6090" s="T196">n-n:(num)-n:case</ta>
            <ta e="T198" id="Seg_6091" s="T197">n-n:(num)-n:case</ta>
            <ta e="T199" id="Seg_6092" s="T198">conj</ta>
            <ta e="T200" id="Seg_6093" s="T199">conj</ta>
            <ta e="T201" id="Seg_6094" s="T200">adj-n:case</ta>
            <ta e="T202" id="Seg_6095" s="T201">v-v&gt;v-v&gt;v-v:ptcp-v:(case)</ta>
            <ta e="T203" id="Seg_6096" s="T202">v-v:cvb</ta>
            <ta e="T204" id="Seg_6097" s="T203">v-v:tense-v:poss.pn</ta>
            <ta e="T205" id="Seg_6098" s="T204">dempro</ta>
            <ta e="T206" id="Seg_6099" s="T205">n.[n:case]</ta>
            <ta e="T207" id="Seg_6100" s="T206">adj-n:case</ta>
            <ta e="T208" id="Seg_6101" s="T207">v-v&gt;v-v&gt;v-v:ptcp-v:(case)</ta>
            <ta e="T209" id="Seg_6102" s="T208">conj</ta>
            <ta e="T210" id="Seg_6103" s="T209">adj-n:case</ta>
            <ta e="T211" id="Seg_6104" s="T210">v-v&gt;v-v&gt;v-v:ptcp-v:(case)</ta>
            <ta e="T212" id="Seg_6105" s="T211">dempro</ta>
            <ta e="T213" id="Seg_6106" s="T212">n-n:case</ta>
            <ta e="T214" id="Seg_6107" s="T213">ptcl</ta>
            <ta e="T215" id="Seg_6108" s="T214">ptcl</ta>
            <ta e="T216" id="Seg_6109" s="T215">v-v:cvb</ta>
            <ta e="T217" id="Seg_6110" s="T216">v-v:ptcp-v:(case)</ta>
            <ta e="T218" id="Seg_6111" s="T217">conj</ta>
            <ta e="T219" id="Seg_6112" s="T218">pers-pro:case</ta>
            <ta e="T220" id="Seg_6113" s="T219">adj-adj&gt;adv</ta>
            <ta e="T221" id="Seg_6114" s="T220">v-v:tense-v:pred.pn</ta>
            <ta e="T222" id="Seg_6115" s="T221">adj-n:case</ta>
            <ta e="T223" id="Seg_6116" s="T222">v-v:tense-v:pred.pn</ta>
            <ta e="T224" id="Seg_6117" s="T223">ptcl</ta>
            <ta e="T225" id="Seg_6118" s="T224">pers.[pro:case]</ta>
            <ta e="T226" id="Seg_6119" s="T225">ptcl</ta>
            <ta e="T227" id="Seg_6120" s="T226">v-v:(neg)-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T228" id="Seg_6121" s="T227">ptcl</ta>
            <ta e="T229" id="Seg_6122" s="T228">dempro</ta>
            <ta e="T230" id="Seg_6123" s="T229">n.[n:case]</ta>
            <ta e="T231" id="Seg_6124" s="T230">n-n:poss-n:case</ta>
            <ta e="T232" id="Seg_6125" s="T231">v-v:tense.[v:pred.pn]</ta>
            <ta e="T233" id="Seg_6126" s="T232">dempro</ta>
            <ta e="T234" id="Seg_6127" s="T233">n.[n:case]</ta>
            <ta e="T235" id="Seg_6128" s="T234">dempro</ta>
            <ta e="T236" id="Seg_6129" s="T235">n.[n:case]</ta>
            <ta e="T237" id="Seg_6130" s="T236">n-n:poss</ta>
            <ta e="T238" id="Seg_6131" s="T237">dempro</ta>
            <ta e="T239" id="Seg_6132" s="T238">n.[n:case]</ta>
            <ta e="T240" id="Seg_6133" s="T239">n-n:poss-n:case</ta>
            <ta e="T241" id="Seg_6134" s="T240">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T242" id="Seg_6135" s="T241">v-v:(ins)-v:(neg)-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T243" id="Seg_6136" s="T242">ptcl</ta>
            <ta e="T244" id="Seg_6137" s="T243">conj</ta>
            <ta e="T245" id="Seg_6138" s="T244">n-n:(num)-n:case</ta>
            <ta e="T246" id="Seg_6139" s="T245">ptcl</ta>
            <ta e="T247" id="Seg_6140" s="T246">v-v:(neg)-v:ptcp-v:(case)</ta>
            <ta e="T248" id="Seg_6141" s="T247">ptcl</ta>
            <ta e="T249" id="Seg_6142" s="T248">conj</ta>
            <ta e="T250" id="Seg_6143" s="T249">adj.[n:case]</ta>
            <ta e="T251" id="Seg_6144" s="T250">v-v:(ins)-v:(neg)-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T252" id="Seg_6145" s="T251">conj</ta>
            <ta e="T253" id="Seg_6146" s="T252">adj-adj&gt;adv</ta>
            <ta e="T254" id="Seg_6147" s="T253">v-v:ptcp</ta>
            <ta e="T255" id="Seg_6148" s="T254">n-n:case</ta>
            <ta e="T256" id="Seg_6149" s="T255">ptcl</ta>
            <ta e="T257" id="Seg_6150" s="T256">dempro</ta>
            <ta e="T258" id="Seg_6151" s="T257">n-n:case</ta>
            <ta e="T260" id="Seg_6152" s="T259">ptcl</ta>
            <ta e="T261" id="Seg_6153" s="T260">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T262" id="Seg_6154" s="T261">v-v:(ins)-v:(neg)-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T263" id="Seg_6155" s="T262">conj</ta>
            <ta e="T264" id="Seg_6156" s="T263">v-v:(neg)-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T265" id="Seg_6157" s="T264">conj</ta>
            <ta e="T266" id="Seg_6158" s="T265">adj</ta>
            <ta e="T267" id="Seg_6159" s="T266">n-n:(num)-n:case</ta>
            <ta e="T268" id="Seg_6160" s="T267">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T269" id="Seg_6161" s="T268">ptcl</ta>
            <ta e="T270" id="Seg_6162" s="T269">conj</ta>
            <ta e="T271" id="Seg_6163" s="T270">dempro</ta>
            <ta e="T272" id="Seg_6164" s="T271">n.[n:case]</ta>
            <ta e="T273" id="Seg_6165" s="T272">adj-n:case</ta>
            <ta e="T274" id="Seg_6166" s="T273">v-v:tense-v:poss.pn</ta>
            <ta e="T275" id="Seg_6167" s="T274">v-v:cvb</ta>
            <ta e="T276" id="Seg_6168" s="T275">dempro</ta>
            <ta e="T277" id="Seg_6169" s="T276">n.[n:case]</ta>
            <ta e="T278" id="Seg_6170" s="T277">dempro.[pro:case]</ta>
            <ta e="T279" id="Seg_6171" s="T278">v-v:cvb</ta>
            <ta e="T280" id="Seg_6172" s="T279">dempro</ta>
            <ta e="T281" id="Seg_6173" s="T280">n.[n:case]</ta>
            <ta e="T282" id="Seg_6174" s="T281">dempro.[pro:case]</ta>
            <ta e="T283" id="Seg_6175" s="T282">v-v:cvb</ta>
            <ta e="T285" id="Seg_6176" s="T284">adv</ta>
            <ta e="T286" id="Seg_6177" s="T285">ptcl</ta>
            <ta e="T287" id="Seg_6178" s="T286">adj.[n:case]</ta>
            <ta e="T288" id="Seg_6179" s="T287">v-v:tense.[v:pred.pn]</ta>
            <ta e="T289" id="Seg_6180" s="T288">adj.[n:case]</ta>
            <ta e="T290" id="Seg_6181" s="T289">v-v:tense.[v:pred.pn]</ta>
            <ta e="T291" id="Seg_6182" s="T290">dempro</ta>
            <ta e="T292" id="Seg_6183" s="T291">n-n:case</ta>
            <ta e="T293" id="Seg_6184" s="T292">v-v:cvb</ta>
            <ta e="T294" id="Seg_6185" s="T293">conj</ta>
            <ta e="T295" id="Seg_6186" s="T294">ptcl</ta>
            <ta e="T296" id="Seg_6187" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_6188" s="T296">conj</ta>
            <ta e="T298" id="Seg_6189" s="T297">dempro</ta>
            <ta e="T299" id="Seg_6190" s="T298">n-n:case</ta>
            <ta e="T300" id="Seg_6191" s="T299">pers.[pro:case]</ta>
            <ta e="T301" id="Seg_6192" s="T300">n-n&gt;v-v:(neg)-v:pred.pn</ta>
            <ta e="T302" id="Seg_6193" s="T301">que</ta>
            <ta e="T303" id="Seg_6194" s="T302">v-v:cvb-n:(poss)</ta>
            <ta e="T304" id="Seg_6195" s="T303">adj.[n:case]</ta>
            <ta e="T305" id="Seg_6196" s="T304">cardnum-cardnum&gt;ordnum</ta>
            <ta e="T306" id="Seg_6197" s="T305">n.[n:case]</ta>
            <ta e="T307" id="Seg_6198" s="T306">ptcl</ta>
            <ta e="T308" id="Seg_6199" s="T307">dempro</ta>
            <ta e="T309" id="Seg_6200" s="T308">n-n:poss-n:case</ta>
            <ta e="T310" id="Seg_6201" s="T309">adv</ta>
            <ta e="T311" id="Seg_6202" s="T310">adj.[n:case]</ta>
            <ta e="T312" id="Seg_6203" s="T311">v-v:(ins)-v:(neg)-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T313" id="Seg_6204" s="T312">adj-n:case</ta>
            <ta e="T314" id="Seg_6205" s="T313">n.[n:case]</ta>
            <ta e="T315" id="Seg_6206" s="T314">post</ta>
            <ta e="T316" id="Seg_6207" s="T315">adv</ta>
            <ta e="T317" id="Seg_6208" s="T316">adj-n:case</ta>
            <ta e="T318" id="Seg_6209" s="T317">n.[n:case]</ta>
            <ta e="T319" id="Seg_6210" s="T318">v-v:ptcp-v:(case)</ta>
            <ta e="T320" id="Seg_6211" s="T319">ptcl</ta>
            <ta e="T321" id="Seg_6212" s="T320">pers-pro:case</ta>
            <ta e="T322" id="Seg_6213" s="T321">conj</ta>
            <ta e="T323" id="Seg_6214" s="T322">adj-n:case</ta>
            <ta e="T324" id="Seg_6215" s="T323">v-v:ptcp</ta>
            <ta e="T325" id="Seg_6216" s="T324">n.[n:case]</ta>
            <ta e="T326" id="Seg_6217" s="T325">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T327" id="Seg_6218" s="T326">conj</ta>
            <ta e="T328" id="Seg_6219" s="T327">adv</ta>
            <ta e="T329" id="Seg_6220" s="T328">n-n:case</ta>
            <ta e="T330" id="Seg_6221" s="T329">posspr</ta>
            <ta e="T331" id="Seg_6222" s="T330">adj-n:(poss).[n:case]</ta>
            <ta e="T332" id="Seg_6223" s="T331">adv</ta>
            <ta e="T333" id="Seg_6224" s="T332">v-v:tense.[v:poss.pn]</ta>
            <ta e="T334" id="Seg_6225" s="T333">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T335" id="Seg_6226" s="T334">pers-pro:case</ta>
            <ta e="T336" id="Seg_6227" s="T335">pers.[pro:case]</ta>
            <ta e="T337" id="Seg_6228" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_6229" s="T337">n.[n:case]</ta>
            <ta e="T339" id="Seg_6230" s="T338">n-n:poss-n:case</ta>
            <ta e="T340" id="Seg_6231" s="T339">n-n&gt;v-v:tense-v:mood.[v:pred.pn]</ta>
            <ta e="T341" id="Seg_6232" s="T340">conj</ta>
            <ta e="T342" id="Seg_6233" s="T341">n.[n:case]</ta>
            <ta e="T343" id="Seg_6234" s="T342">post</ta>
            <ta e="T344" id="Seg_6235" s="T343">adj-n:case</ta>
            <ta e="T345" id="Seg_6236" s="T344">v-v:(ins)-v:(neg)-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T346" id="Seg_6237" s="T345">adv</ta>
            <ta e="T347" id="Seg_6238" s="T346">ptcl</ta>
            <ta e="T348" id="Seg_6239" s="T347">adj.[n:case]</ta>
            <ta e="T349" id="Seg_6240" s="T348">cardnum-cardnum&gt;adv</ta>
            <ta e="T350" id="Seg_6241" s="T349">n.[n:case]</ta>
            <ta e="T351" id="Seg_6242" s="T350">v-v:cvb</ta>
            <ta e="T352" id="Seg_6243" s="T351">v-v:tense-v:poss.pn</ta>
            <ta e="T353" id="Seg_6244" s="T352">conj</ta>
            <ta e="T354" id="Seg_6245" s="T353">v-v:ptcp</ta>
            <ta e="T355" id="Seg_6246" s="T354">n-n:(num).[n:case]</ta>
            <ta e="T356" id="Seg_6247" s="T355">adv</ta>
            <ta e="T357" id="Seg_6248" s="T356">v-v:ptcp</ta>
            <ta e="T358" id="Seg_6249" s="T357">n-n:(num).[n:case]</ta>
            <ta e="T359" id="Seg_6250" s="T358">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T360" id="Seg_6251" s="T359">n.[n:case]</ta>
            <ta e="T361" id="Seg_6252" s="T360">post</ta>
            <ta e="T362" id="Seg_6253" s="T361">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T363" id="Seg_6254" s="T362">n.[n:case]</ta>
            <ta e="T364" id="Seg_6255" s="T363">post</ta>
            <ta e="T365" id="Seg_6256" s="T364">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T366" id="Seg_6257" s="T365">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T367" id="Seg_6258" s="T366">ptcl</ta>
            <ta e="T368" id="Seg_6259" s="T367">v-v:(ins)-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T369" id="Seg_6260" s="T368">ptcl-ptcl:(poss.pn)</ta>
            <ta e="T370" id="Seg_6261" s="T369">interj</ta>
            <ta e="T371" id="Seg_6262" s="T370">conj</ta>
            <ta e="T372" id="Seg_6263" s="T371">adv</ta>
            <ta e="T373" id="Seg_6264" s="T372">dempro</ta>
            <ta e="T374" id="Seg_6265" s="T373">dempro</ta>
            <ta e="T375" id="Seg_6266" s="T374">adj</ta>
            <ta e="T376" id="Seg_6267" s="T375">adj</ta>
            <ta e="T377" id="Seg_6268" s="T376">n.[n:case]</ta>
            <ta e="T378" id="Seg_6269" s="T377">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T379" id="Seg_6270" s="T378">post</ta>
            <ta e="T380" id="Seg_6271" s="T379">ptcl</ta>
            <ta e="T381" id="Seg_6272" s="T380">n.[n:case]</ta>
            <ta e="T384" id="Seg_6273" s="T383">v-v:cvb</ta>
            <ta e="T385" id="Seg_6274" s="T384">v-v:tense-v:poss.pn</ta>
            <ta e="T386" id="Seg_6275" s="T385">adv</ta>
            <ta e="T387" id="Seg_6276" s="T386">adv</ta>
            <ta e="T388" id="Seg_6277" s="T387">adj-adj&gt;adv</ta>
            <ta e="T389" id="Seg_6278" s="T388">v-v&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T390" id="Seg_6279" s="T389">quant</ta>
            <ta e="T391" id="Seg_6280" s="T390">n.[n:case]</ta>
            <ta e="T392" id="Seg_6281" s="T391">v-v:tense-v:poss.pn</ta>
            <ta e="T393" id="Seg_6282" s="T392">interj</ta>
            <ta e="T394" id="Seg_6283" s="T393">propr-n:case</ta>
            <ta e="T395" id="Seg_6284" s="T394">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T396" id="Seg_6285" s="T395">adj</ta>
            <ta e="T397" id="Seg_6286" s="T396">n.[n:case]</ta>
            <ta e="T398" id="Seg_6287" s="T397">ptcl</ta>
            <ta e="T399" id="Seg_6288" s="T398">v-v:tense-v:poss.pn</ta>
            <ta e="T400" id="Seg_6289" s="T399">posspr</ta>
            <ta e="T401" id="Seg_6290" s="T400">n.[n:case]</ta>
            <ta e="T402" id="Seg_6291" s="T401">n-n:(poss).[n:case]</ta>
            <ta e="T403" id="Seg_6292" s="T402">post</ta>
            <ta e="T404" id="Seg_6293" s="T403">adv</ta>
            <ta e="T405" id="Seg_6294" s="T404">ptcl</ta>
            <ta e="T406" id="Seg_6295" s="T405">propr-n:case</ta>
            <ta e="T407" id="Seg_6296" s="T406">v-v:tense-v:poss.pn</ta>
            <ta e="T408" id="Seg_6297" s="T407">interj</ta>
            <ta e="T409" id="Seg_6298" s="T408">propr-n:case</ta>
            <ta e="T410" id="Seg_6299" s="T409">propr-n:case</ta>
            <ta e="T411" id="Seg_6300" s="T410">n.[n:case]</ta>
            <ta e="T412" id="Seg_6301" s="T411">ptcl</ta>
            <ta e="T413" id="Seg_6302" s="T412">v-v:tense-v:poss.pn</ta>
            <ta e="T414" id="Seg_6303" s="T413">adj</ta>
            <ta e="T415" id="Seg_6304" s="T414">n-n:case</ta>
            <ta e="T416" id="Seg_6305" s="T415">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T417" id="Seg_6306" s="T416">adv</ta>
            <ta e="T418" id="Seg_6307" s="T417">quant-quant</ta>
            <ta e="T419" id="Seg_6308" s="T418">n-n:case</ta>
            <ta e="T420" id="Seg_6309" s="T419">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T421" id="Seg_6310" s="T420">adv</ta>
            <ta e="T422" id="Seg_6311" s="T421">cardnum</ta>
            <ta e="T423" id="Seg_6312" s="T422">cardnum</ta>
            <ta e="T424" id="Seg_6313" s="T423">n.[n:case]</ta>
            <ta e="T425" id="Seg_6314" s="T424">ptcl</ta>
            <ta e="T426" id="Seg_6315" s="T425">v-v:tense-v:poss.pn</ta>
            <ta e="T427" id="Seg_6316" s="T426">interj</ta>
            <ta e="T428" id="Seg_6317" s="T427">adv</ta>
            <ta e="T429" id="Seg_6318" s="T428">n-n:(num)-n:(poss).[n:case]</ta>
            <ta e="T430" id="Seg_6319" s="T429">adj</ta>
            <ta e="T431" id="Seg_6320" s="T430">ptcl</ta>
            <ta e="T432" id="Seg_6321" s="T431">n-n:(num).[n:case]</ta>
            <ta e="T433" id="Seg_6322" s="T432">v-v:tense-v:poss.pn</ta>
            <ta e="T434" id="Seg_6323" s="T433">pers-pro:case</ta>
            <ta e="T435" id="Seg_6324" s="T434">adj-n:poss-n:case</ta>
            <ta e="T436" id="Seg_6325" s="T435">interj</ta>
            <ta e="T437" id="Seg_6326" s="T436">v-v:cvb-v:pred.pn</ta>
            <ta e="T438" id="Seg_6327" s="T437">v-v:cvb-v:pred.pn</ta>
            <ta e="T439" id="Seg_6328" s="T438">adj</ta>
            <ta e="T440" id="Seg_6329" s="T439">n-n&gt;adv</ta>
            <ta e="T441" id="Seg_6330" s="T440">v-v&gt;v-v:cvb-v:pred.pn</ta>
            <ta e="T442" id="Seg_6331" s="T441">adv</ta>
            <ta e="T443" id="Seg_6332" s="T442">v-v:(ins)-v&gt;v-v:cvb-v:pred.pn</ta>
            <ta e="T444" id="Seg_6333" s="T443">adv</ta>
            <ta e="T445" id="Seg_6334" s="T444">n-n:(num).[n:case]</ta>
            <ta e="T446" id="Seg_6335" s="T445">post</ta>
            <ta e="T447" id="Seg_6336" s="T446">n-n:case</ta>
            <ta e="T448" id="Seg_6337" s="T447">v-v:(ins)-v:tense-v:poss.pn</ta>
            <ta e="T449" id="Seg_6338" s="T448">propr</ta>
            <ta e="T450" id="Seg_6339" s="T449">v-v:cvb</ta>
            <ta e="T451" id="Seg_6340" s="T450">interj</ta>
            <ta e="T452" id="Seg_6341" s="T451">n.[n:case]</ta>
            <ta e="T453" id="Seg_6342" s="T452">propr</ta>
            <ta e="T454" id="Seg_6343" s="T453">v-v:cvb</ta>
            <ta e="T455" id="Seg_6344" s="T454">n.[n:case]</ta>
            <ta e="T456" id="Seg_6345" s="T455">post</ta>
            <ta e="T457" id="Seg_6346" s="T456">v-v:tense-v:pred.pn</ta>
            <ta e="T458" id="Seg_6347" s="T457">dempro</ta>
            <ta e="T459" id="Seg_6348" s="T458">propr</ta>
            <ta e="T460" id="Seg_6349" s="T459">ptcl</ta>
            <ta e="T461" id="Seg_6350" s="T460">adj</ta>
            <ta e="T462" id="Seg_6351" s="T461">n-n:case</ta>
            <ta e="T463" id="Seg_6352" s="T462">v-v:tense.[v:pred.pn]</ta>
            <ta e="T464" id="Seg_6353" s="T463">propr</ta>
            <ta e="T465" id="Seg_6354" s="T464">v-v:cvb</ta>
            <ta e="T466" id="Seg_6355" s="T465">n-n:case</ta>
            <ta e="T467" id="Seg_6356" s="T466">dempro</ta>
            <ta e="T468" id="Seg_6357" s="T467">v-v:(ins)-v:ptcp-v:(num)-v:(case)</ta>
            <ta e="T469" id="Seg_6358" s="T468">n-n&gt;v-v:tense-v:poss.pn</ta>
            <ta e="T471" id="Seg_6359" s="T470">n-n:(num)-n:case</ta>
            <ta e="T472" id="Seg_6360" s="T471">v-v:tense-v:pred.pn</ta>
            <ta e="T473" id="Seg_6361" s="T472">v-v:cvb-v:pred.pn</ta>
            <ta e="T474" id="Seg_6362" s="T473">v-v:cvb-v:pred.pn</ta>
            <ta e="T475" id="Seg_6363" s="T474">adj.[n:case]</ta>
            <ta e="T476" id="Seg_6364" s="T475">ptcl</ta>
            <ta e="T477" id="Seg_6365" s="T476">v-v:tense-v:poss.pn</ta>
            <ta e="T478" id="Seg_6366" s="T477">n-n:case</ta>
            <ta e="T479" id="Seg_6367" s="T478">v-v:tense-v:poss.pn</ta>
            <ta e="T480" id="Seg_6368" s="T479">pers-pro:case</ta>
            <ta e="T481" id="Seg_6369" s="T480">adv</ta>
            <ta e="T482" id="Seg_6370" s="T481">adv</ta>
            <ta e="T483" id="Seg_6371" s="T482">adv</ta>
            <ta e="T484" id="Seg_6372" s="T483">v-v:cvb-v:pred.pn</ta>
            <ta e="T485" id="Seg_6373" s="T484">interj</ta>
            <ta e="T486" id="Seg_6374" s="T485">dempro</ta>
            <ta e="T487" id="Seg_6375" s="T486">n-n:poss-n:case</ta>
            <ta e="T488" id="Seg_6376" s="T487">dempro</ta>
            <ta e="T489" id="Seg_6377" s="T488">cardnum</ta>
            <ta e="T490" id="Seg_6378" s="T489">n-n:case</ta>
            <ta e="T491" id="Seg_6379" s="T490">v-v:tense-v:pred.pn</ta>
            <ta e="T492" id="Seg_6380" s="T491">interj</ta>
            <ta e="T493" id="Seg_6381" s="T492">cardnum</ta>
            <ta e="T494" id="Seg_6382" s="T493">n-n:case</ta>
            <ta e="T495" id="Seg_6383" s="T494">v-v:mood.[v:pred.pn]</ta>
            <ta e="T496" id="Seg_6384" s="T495">interj</ta>
            <ta e="T497" id="Seg_6385" s="T496">cardnum</ta>
            <ta e="T498" id="Seg_6386" s="T497">n-n:case</ta>
            <ta e="T499" id="Seg_6387" s="T498">v-v:tense-v:(poss)-v:(case)</ta>
            <ta e="T500" id="Seg_6388" s="T499">propr-n:case</ta>
            <ta e="T501" id="Seg_6389" s="T500">adj.[n:case]</ta>
            <ta e="T502" id="Seg_6390" s="T501">ptcl</ta>
            <ta e="T503" id="Seg_6391" s="T502">n-n:(num).[n:case]</ta>
            <ta e="T504" id="Seg_6392" s="T503">v-v:tense-v:poss.pn</ta>
            <ta e="T505" id="Seg_6393" s="T504">n.[n:case]</ta>
            <ta e="T506" id="Seg_6394" s="T505">dempro.[pro:case]</ta>
            <ta e="T507" id="Seg_6395" s="T506">n-n:poss-n:case</ta>
            <ta e="T508" id="Seg_6396" s="T507">n-n&gt;v-v:ptcp</ta>
            <ta e="T509" id="Seg_6397" s="T508">v-v:tense-v:poss.pn</ta>
            <ta e="T510" id="Seg_6398" s="T509">v-v:ptcp</ta>
            <ta e="T511" id="Seg_6399" s="T510">v-v:tense-v:poss.pn</ta>
            <ta e="T512" id="Seg_6400" s="T511">adv</ta>
            <ta e="T513" id="Seg_6401" s="T512">cardnum-n:case</ta>
            <ta e="T514" id="Seg_6402" s="T513">post</ta>
            <ta e="T515" id="Seg_6403" s="T514">n.[n:case]</ta>
            <ta e="T516" id="Seg_6404" s="T515">v-v:tense-v:poss.pn</ta>
            <ta e="T517" id="Seg_6405" s="T516">cardnum</ta>
            <ta e="T518" id="Seg_6406" s="T517">cardnum</ta>
            <ta e="T519" id="Seg_6407" s="T518">cardnum</ta>
            <ta e="T520" id="Seg_6408" s="T519">cardnum</ta>
            <ta e="T521" id="Seg_6409" s="T520">n.[n:case]</ta>
            <ta e="T522" id="Seg_6410" s="T521">v-v:cvb</ta>
            <ta e="T523" id="Seg_6411" s="T522">v-v:tense-v:poss.pn</ta>
            <ta e="T524" id="Seg_6412" s="T523">dempro</ta>
            <ta e="T525" id="Seg_6413" s="T524">n-n:(num)-n:case</ta>
            <ta e="T526" id="Seg_6414" s="T525">dempro.[pro:case]</ta>
            <ta e="T527" id="Seg_6415" s="T526">post</ta>
            <ta e="T528" id="Seg_6416" s="T527">ptcl</ta>
            <ta e="T529" id="Seg_6417" s="T528">pers-pro:case</ta>
            <ta e="T530" id="Seg_6418" s="T529">adv</ta>
            <ta e="T531" id="Seg_6419" s="T530">ptcl</ta>
            <ta e="T532" id="Seg_6420" s="T531">adj-n:(poss)</ta>
            <ta e="T533" id="Seg_6421" s="T532">ptcl</ta>
            <ta e="T534" id="Seg_6422" s="T533">v-v:tense-v:poss.pn</ta>
            <ta e="T535" id="Seg_6423" s="T534">conj</ta>
            <ta e="T536" id="Seg_6424" s="T535">adv</ta>
            <ta e="T537" id="Seg_6425" s="T536">ptcl</ta>
            <ta e="T538" id="Seg_6426" s="T537">conj</ta>
            <ta e="T539" id="Seg_6427" s="T538">adj-n:(poss)</ta>
            <ta e="T540" id="Seg_6428" s="T539">ptcl</ta>
            <ta e="T541" id="Seg_6429" s="T540">v-v:tense-v:poss.pn</ta>
            <ta e="T542" id="Seg_6430" s="T541">dempro.[pro:case]</ta>
            <ta e="T543" id="Seg_6431" s="T542">post</ta>
            <ta e="T544" id="Seg_6432" s="T543">adv</ta>
            <ta e="T545" id="Seg_6433" s="T544">adj.[n:case]</ta>
            <ta e="T546" id="Seg_6434" s="T545">pers-pro:case</ta>
            <ta e="T547" id="Seg_6435" s="T546">pers.[pro:case]</ta>
            <ta e="T548" id="Seg_6436" s="T547">n-n:poss-n:case</ta>
            <ta e="T549" id="Seg_6437" s="T548">dempro</ta>
            <ta e="T550" id="Seg_6438" s="T549">n.[n:case]</ta>
            <ta e="T551" id="Seg_6439" s="T550">pers-pro:case</ta>
            <ta e="T552" id="Seg_6440" s="T551">dempro</ta>
            <ta e="T553" id="Seg_6441" s="T552">n.[n:case]</ta>
            <ta e="T554" id="Seg_6442" s="T553">adv</ta>
            <ta e="T555" id="Seg_6443" s="T554">adj-adj&gt;adv</ta>
            <ta e="T556" id="Seg_6444" s="T555">v-v:tense.[v:pred.pn]</ta>
            <ta e="T557" id="Seg_6445" s="T556">dempro.[pro:case]</ta>
            <ta e="T558" id="Seg_6446" s="T557">post</ta>
            <ta e="T559" id="Seg_6447" s="T558">adv</ta>
            <ta e="T560" id="Seg_6448" s="T559">conj</ta>
            <ta e="T561" id="Seg_6449" s="T560">v-v:mood-v:temp.pn</ta>
            <ta e="T562" id="Seg_6450" s="T561">pers.[pro:case]</ta>
            <ta e="T563" id="Seg_6451" s="T562">n-n:poss-n:case</ta>
            <ta e="T564" id="Seg_6452" s="T563">pers.[pro:case]</ta>
            <ta e="T565" id="Seg_6453" s="T564">ptcl</ta>
            <ta e="T566" id="Seg_6454" s="T565">v-v:ptcp</ta>
            <ta e="T567" id="Seg_6455" s="T566">post-n:(pred.pn)</ta>
            <ta e="T568" id="Seg_6456" s="T567">adv</ta>
            <ta e="T569" id="Seg_6457" s="T568">adv</ta>
            <ta e="T570" id="Seg_6458" s="T569">v-v:cvb</ta>
         </annotation>
         <annotation name="ps" tierref="ps-UkOA">
            <ta e="T2" id="Seg_6459" s="T1">adv</ta>
            <ta e="T3" id="Seg_6460" s="T2">v</ta>
            <ta e="T4" id="Seg_6461" s="T3">ptcl</ta>
            <ta e="T10" id="Seg_6462" s="T8">cardnum</ta>
            <ta e="T11" id="Seg_6463" s="T10">adj</ta>
            <ta e="T12" id="Seg_6464" s="T11">n</ta>
            <ta e="T13" id="Seg_6465" s="T12">cardnum</ta>
            <ta e="T14" id="Seg_6466" s="T13">cardnum</ta>
            <ta e="T15" id="Seg_6467" s="T14">adj</ta>
            <ta e="T16" id="Seg_6468" s="T15">n</ta>
            <ta e="T17" id="Seg_6469" s="T16">propr</ta>
            <ta e="T18" id="Seg_6470" s="T17">n</ta>
            <ta e="T19" id="Seg_6471" s="T18">v</ta>
            <ta e="T20" id="Seg_6472" s="T19">n</ta>
            <ta e="T23" id="Seg_6473" s="T22">v</ta>
            <ta e="T24" id="Seg_6474" s="T23">adj</ta>
            <ta e="T25" id="Seg_6475" s="T24">adv</ta>
            <ta e="T26" id="Seg_6476" s="T25">propr</ta>
            <ta e="T27" id="Seg_6477" s="T26">pers</ta>
            <ta e="T28" id="Seg_6478" s="T27">v</ta>
            <ta e="T29" id="Seg_6479" s="T28">cardnum</ta>
            <ta e="T30" id="Seg_6480" s="T29">n</ta>
            <ta e="T31" id="Seg_6481" s="T30">adj</ta>
            <ta e="T32" id="Seg_6482" s="T31">cop</ta>
            <ta e="T33" id="Seg_6483" s="T32">propr</ta>
            <ta e="T34" id="Seg_6484" s="T33">propr</ta>
            <ta e="T35" id="Seg_6485" s="T34">propr</ta>
            <ta e="T39" id="Seg_6486" s="T38">v</ta>
            <ta e="T40" id="Seg_6487" s="T39">n</ta>
            <ta e="T41" id="Seg_6488" s="T40">adj</ta>
            <ta e="T45" id="Seg_6489" s="T44">conj</ta>
            <ta e="T46" id="Seg_6490" s="T45">pers</ta>
            <ta e="T47" id="Seg_6491" s="T46">propr</ta>
            <ta e="T48" id="Seg_6492" s="T47">adv</ta>
            <ta e="T49" id="Seg_6493" s="T48">adv</ta>
            <ta e="T50" id="Seg_6494" s="T49">adv</ta>
            <ta e="T51" id="Seg_6495" s="T50">pers</ta>
            <ta e="T52" id="Seg_6496" s="T51">v</ta>
            <ta e="T53" id="Seg_6497" s="T52">adj</ta>
            <ta e="T54" id="Seg_6498" s="T53">n</ta>
            <ta e="T55" id="Seg_6499" s="T54">adj</ta>
            <ta e="T56" id="Seg_6500" s="T55">ptcl</ta>
            <ta e="T57" id="Seg_6501" s="T56">n</ta>
            <ta e="T58" id="Seg_6502" s="T57">v</ta>
            <ta e="T59" id="Seg_6503" s="T58">n</ta>
            <ta e="T60" id="Seg_6504" s="T59">cop</ta>
            <ta e="T61" id="Seg_6505" s="T60">cardnum</ta>
            <ta e="T62" id="Seg_6506" s="T61">n</ta>
            <ta e="T65" id="Seg_6507" s="T64">adv</ta>
            <ta e="T66" id="Seg_6508" s="T65">adv</ta>
            <ta e="T67" id="Seg_6509" s="T66">pers</ta>
            <ta e="T68" id="Seg_6510" s="T67">v</ta>
            <ta e="T69" id="Seg_6511" s="T68">adv</ta>
            <ta e="T70" id="Seg_6512" s="T69">v</ta>
            <ta e="T71" id="Seg_6513" s="T70">v</ta>
            <ta e="T72" id="Seg_6514" s="T71">adv</ta>
            <ta e="T73" id="Seg_6515" s="T72">n</ta>
            <ta e="T74" id="Seg_6516" s="T73">v</ta>
            <ta e="T75" id="Seg_6517" s="T74">dempro</ta>
            <ta e="T76" id="Seg_6518" s="T75">n</ta>
            <ta e="T77" id="Seg_6519" s="T76">dempro</ta>
            <ta e="T78" id="Seg_6520" s="T77">n</ta>
            <ta e="T79" id="Seg_6521" s="T78">ptcl</ta>
            <ta e="T80" id="Seg_6522" s="T79">v</ta>
            <ta e="T81" id="Seg_6523" s="T80">dempro</ta>
            <ta e="T82" id="Seg_6524" s="T81">n</ta>
            <ta e="T83" id="Seg_6525" s="T82">adj</ta>
            <ta e="T84" id="Seg_6526" s="T83">n</ta>
            <ta e="T85" id="Seg_6527" s="T84">v</ta>
            <ta e="T86" id="Seg_6528" s="T85">ordnum</ta>
            <ta e="T87" id="Seg_6529" s="T86">n</ta>
            <ta e="T88" id="Seg_6530" s="T87">dempro</ta>
            <ta e="T89" id="Seg_6531" s="T88">adj</ta>
            <ta e="T90" id="Seg_6532" s="T89">n</ta>
            <ta e="T91" id="Seg_6533" s="T90">ptcl</ta>
            <ta e="T92" id="Seg_6534" s="T91">adj</ta>
            <ta e="T93" id="Seg_6535" s="T92">interj</ta>
            <ta e="T94" id="Seg_6536" s="T93">quant</ta>
            <ta e="T95" id="Seg_6537" s="T94">n</ta>
            <ta e="T96" id="Seg_6538" s="T95">v</ta>
            <ta e="T97" id="Seg_6539" s="T96">n</ta>
            <ta e="T98" id="Seg_6540" s="T97">n</ta>
            <ta e="T99" id="Seg_6541" s="T98">interj</ta>
            <ta e="T100" id="Seg_6542" s="T99">n</ta>
            <ta e="T101" id="Seg_6543" s="T100">n</ta>
            <ta e="T102" id="Seg_6544" s="T101">n</ta>
            <ta e="T103" id="Seg_6545" s="T102">ptcl</ta>
            <ta e="T104" id="Seg_6546" s="T103">cop</ta>
            <ta e="T105" id="Seg_6547" s="T104">n</ta>
            <ta e="T106" id="Seg_6548" s="T105">v</ta>
            <ta e="T107" id="Seg_6549" s="T106">n</ta>
            <ta e="T108" id="Seg_6550" s="T107">n</ta>
            <ta e="T109" id="Seg_6551" s="T108">interj</ta>
            <ta e="T110" id="Seg_6552" s="T109">dempro</ta>
            <ta e="T111" id="Seg_6553" s="T110">adj</ta>
            <ta e="T112" id="Seg_6554" s="T111">n</ta>
            <ta e="T113" id="Seg_6555" s="T112">n</ta>
            <ta e="T114" id="Seg_6556" s="T113">que</ta>
            <ta e="T115" id="Seg_6557" s="T114">v</ta>
            <ta e="T116" id="Seg_6558" s="T115">conj</ta>
            <ta e="T117" id="Seg_6559" s="T116">que</ta>
            <ta e="T118" id="Seg_6560" s="T117">v</ta>
            <ta e="T119" id="Seg_6561" s="T118">dempro</ta>
            <ta e="T120" id="Seg_6562" s="T119">dempro</ta>
            <ta e="T121" id="Seg_6563" s="T120">n</ta>
            <ta e="T122" id="Seg_6564" s="T121">interj</ta>
            <ta e="T123" id="Seg_6565" s="T122">adj</ta>
            <ta e="T124" id="Seg_6566" s="T123">adv</ta>
            <ta e="T125" id="Seg_6567" s="T124">n</ta>
            <ta e="T126" id="Seg_6568" s="T125">v</ta>
            <ta e="T127" id="Seg_6569" s="T126">aux</ta>
            <ta e="T128" id="Seg_6570" s="T127">adv</ta>
            <ta e="T129" id="Seg_6571" s="T128">ptcl</ta>
            <ta e="T131" id="Seg_6572" s="T130">interj</ta>
            <ta e="T132" id="Seg_6573" s="T131">n</ta>
            <ta e="T133" id="Seg_6574" s="T132">v</ta>
            <ta e="T134" id="Seg_6575" s="T133">adv</ta>
            <ta e="T135" id="Seg_6576" s="T134">propr</ta>
            <ta e="T136" id="Seg_6577" s="T135">v</ta>
            <ta e="T137" id="Seg_6578" s="T136">propr</ta>
            <ta e="T138" id="Seg_6579" s="T137">propr</ta>
            <ta e="T139" id="Seg_6580" s="T138">adj</ta>
            <ta e="T140" id="Seg_6581" s="T139">n</ta>
            <ta e="T141" id="Seg_6582" s="T140">dempro</ta>
            <ta e="T142" id="Seg_6583" s="T141">v</ta>
            <ta e="T143" id="Seg_6584" s="T142">interj</ta>
            <ta e="T144" id="Seg_6585" s="T143">adj</ta>
            <ta e="T145" id="Seg_6586" s="T144">n</ta>
            <ta e="T146" id="Seg_6587" s="T145">interj</ta>
            <ta e="T147" id="Seg_6588" s="T146">adj</ta>
            <ta e="T148" id="Seg_6589" s="T147">n</ta>
            <ta e="T149" id="Seg_6590" s="T148">emphpro</ta>
            <ta e="T150" id="Seg_6591" s="T149">n</ta>
            <ta e="T151" id="Seg_6592" s="T150">emphpro</ta>
            <ta e="T152" id="Seg_6593" s="T151">n</ta>
            <ta e="T153" id="Seg_6594" s="T152">n</ta>
            <ta e="T154" id="Seg_6595" s="T153">n</ta>
            <ta e="T155" id="Seg_6596" s="T154">n</ta>
            <ta e="T156" id="Seg_6597" s="T155">cop</ta>
            <ta e="T157" id="Seg_6598" s="T156">v</ta>
            <ta e="T158" id="Seg_6599" s="T157">n</ta>
            <ta e="T159" id="Seg_6600" s="T158">v</ta>
            <ta e="T160" id="Seg_6601" s="T159">n</ta>
            <ta e="T161" id="Seg_6602" s="T160">n</ta>
            <ta e="T162" id="Seg_6603" s="T161">n</ta>
            <ta e="T163" id="Seg_6604" s="T162">n</ta>
            <ta e="T164" id="Seg_6605" s="T163">interj</ta>
            <ta e="T165" id="Seg_6606" s="T164">n</ta>
            <ta e="T166" id="Seg_6607" s="T165">n</ta>
            <ta e="T167" id="Seg_6608" s="T166">dempro</ta>
            <ta e="T168" id="Seg_6609" s="T167">ptcl</ta>
            <ta e="T169" id="Seg_6610" s="T168">interj</ta>
            <ta e="T170" id="Seg_6611" s="T169">n</ta>
            <ta e="T171" id="Seg_6612" s="T170">dempro</ta>
            <ta e="T172" id="Seg_6613" s="T171">n</ta>
            <ta e="T173" id="Seg_6614" s="T172">ptcl</ta>
            <ta e="T174" id="Seg_6615" s="T173">cop</ta>
            <ta e="T175" id="Seg_6616" s="T174">ptcl</ta>
            <ta e="T176" id="Seg_6617" s="T175">conj</ta>
            <ta e="T180" id="Seg_6618" s="T179">post</ta>
            <ta e="T181" id="Seg_6619" s="T180">cop</ta>
            <ta e="T182" id="Seg_6620" s="T181">dempro</ta>
            <ta e="T183" id="Seg_6621" s="T182">post</ta>
            <ta e="T184" id="Seg_6622" s="T183">ptcl</ta>
            <ta e="T185" id="Seg_6623" s="T184">adv</ta>
            <ta e="T186" id="Seg_6624" s="T185">n</ta>
            <ta e="T187" id="Seg_6625" s="T186">post</ta>
            <ta e="T188" id="Seg_6626" s="T187">interj</ta>
            <ta e="T189" id="Seg_6627" s="T188">v</ta>
            <ta e="T190" id="Seg_6628" s="T189">ptcl</ta>
            <ta e="T191" id="Seg_6629" s="T190">v</ta>
            <ta e="T192" id="Seg_6630" s="T191">interj</ta>
            <ta e="T193" id="Seg_6631" s="T192">dempro</ta>
            <ta e="T194" id="Seg_6632" s="T193">n</ta>
            <ta e="T195" id="Seg_6633" s="T194">n</ta>
            <ta e="T196" id="Seg_6634" s="T195">v</ta>
            <ta e="T197" id="Seg_6635" s="T196">n</ta>
            <ta e="T198" id="Seg_6636" s="T197">n</ta>
            <ta e="T199" id="Seg_6637" s="T198">conj</ta>
            <ta e="T200" id="Seg_6638" s="T199">conj</ta>
            <ta e="T201" id="Seg_6639" s="T200">n</ta>
            <ta e="T202" id="Seg_6640" s="T201">v</ta>
            <ta e="T203" id="Seg_6641" s="T202">v</ta>
            <ta e="T204" id="Seg_6642" s="T203">v</ta>
            <ta e="T205" id="Seg_6643" s="T204">dempro</ta>
            <ta e="T206" id="Seg_6644" s="T205">n</ta>
            <ta e="T207" id="Seg_6645" s="T206">n</ta>
            <ta e="T208" id="Seg_6646" s="T207">v</ta>
            <ta e="T209" id="Seg_6647" s="T208">conj</ta>
            <ta e="T210" id="Seg_6648" s="T209">n</ta>
            <ta e="T211" id="Seg_6649" s="T210">v</ta>
            <ta e="T212" id="Seg_6650" s="T211">dempro</ta>
            <ta e="T213" id="Seg_6651" s="T212">n</ta>
            <ta e="T214" id="Seg_6652" s="T213">ptcl</ta>
            <ta e="T215" id="Seg_6653" s="T214">ptcl</ta>
            <ta e="T216" id="Seg_6654" s="T215">v</ta>
            <ta e="T217" id="Seg_6655" s="T216">aux</ta>
            <ta e="T218" id="Seg_6656" s="T217">conj</ta>
            <ta e="T219" id="Seg_6657" s="T218">pers</ta>
            <ta e="T220" id="Seg_6658" s="T219">adv</ta>
            <ta e="T221" id="Seg_6659" s="T220">v</ta>
            <ta e="T222" id="Seg_6660" s="T221">n</ta>
            <ta e="T223" id="Seg_6661" s="T222">v</ta>
            <ta e="T224" id="Seg_6662" s="T223">ptcl</ta>
            <ta e="T225" id="Seg_6663" s="T224">pers</ta>
            <ta e="T226" id="Seg_6664" s="T225">ptcl</ta>
            <ta e="T227" id="Seg_6665" s="T226">v</ta>
            <ta e="T228" id="Seg_6666" s="T227">ptcl</ta>
            <ta e="T229" id="Seg_6667" s="T228">dempro</ta>
            <ta e="T230" id="Seg_6668" s="T229">n</ta>
            <ta e="T231" id="Seg_6669" s="T230">n</ta>
            <ta e="T232" id="Seg_6670" s="T231">v</ta>
            <ta e="T233" id="Seg_6671" s="T232">dempro</ta>
            <ta e="T234" id="Seg_6672" s="T233">n</ta>
            <ta e="T235" id="Seg_6673" s="T234">dempro</ta>
            <ta e="T236" id="Seg_6674" s="T235">n</ta>
            <ta e="T237" id="Seg_6675" s="T236">n</ta>
            <ta e="T238" id="Seg_6676" s="T237">dempro</ta>
            <ta e="T239" id="Seg_6677" s="T238">n</ta>
            <ta e="T240" id="Seg_6678" s="T239">n</ta>
            <ta e="T241" id="Seg_6679" s="T240">emphpro</ta>
            <ta e="T242" id="Seg_6680" s="T241">v</ta>
            <ta e="T243" id="Seg_6681" s="T242">ptcl</ta>
            <ta e="T244" id="Seg_6682" s="T243">conj</ta>
            <ta e="T245" id="Seg_6683" s="T244">n</ta>
            <ta e="T246" id="Seg_6684" s="T245">ptcl</ta>
            <ta e="T247" id="Seg_6685" s="T246">v</ta>
            <ta e="T248" id="Seg_6686" s="T247">ptcl</ta>
            <ta e="T249" id="Seg_6687" s="T248">conj</ta>
            <ta e="T250" id="Seg_6688" s="T249">n</ta>
            <ta e="T251" id="Seg_6689" s="T250">v</ta>
            <ta e="T252" id="Seg_6690" s="T251">conj</ta>
            <ta e="T253" id="Seg_6691" s="T252">adv</ta>
            <ta e="T254" id="Seg_6692" s="T253">v</ta>
            <ta e="T255" id="Seg_6693" s="T254">n</ta>
            <ta e="T256" id="Seg_6694" s="T255">ptcl</ta>
            <ta e="T257" id="Seg_6695" s="T256">dempro</ta>
            <ta e="T258" id="Seg_6696" s="T257">n</ta>
            <ta e="T260" id="Seg_6697" s="T259">ptcl</ta>
            <ta e="T261" id="Seg_6698" s="T260">emphpro</ta>
            <ta e="T262" id="Seg_6699" s="T261">v</ta>
            <ta e="T263" id="Seg_6700" s="T262">conj</ta>
            <ta e="T264" id="Seg_6701" s="T263">v</ta>
            <ta e="T265" id="Seg_6702" s="T264">conj</ta>
            <ta e="T266" id="Seg_6703" s="T265">adj</ta>
            <ta e="T267" id="Seg_6704" s="T266">n</ta>
            <ta e="T268" id="Seg_6705" s="T267">v</ta>
            <ta e="T269" id="Seg_6706" s="T268">ptcl</ta>
            <ta e="T270" id="Seg_6707" s="T269">conj</ta>
            <ta e="T271" id="Seg_6708" s="T270">dempro</ta>
            <ta e="T272" id="Seg_6709" s="T271">n</ta>
            <ta e="T273" id="Seg_6710" s="T272">n</ta>
            <ta e="T274" id="Seg_6711" s="T273">v</ta>
            <ta e="T275" id="Seg_6712" s="T274">v</ta>
            <ta e="T276" id="Seg_6713" s="T275">dempro</ta>
            <ta e="T277" id="Seg_6714" s="T276">n</ta>
            <ta e="T278" id="Seg_6715" s="T277">dempro</ta>
            <ta e="T279" id="Seg_6716" s="T278">v</ta>
            <ta e="T280" id="Seg_6717" s="T279">dempro</ta>
            <ta e="T281" id="Seg_6718" s="T280">n</ta>
            <ta e="T282" id="Seg_6719" s="T281">dempro</ta>
            <ta e="T283" id="Seg_6720" s="T282">v</ta>
            <ta e="T285" id="Seg_6721" s="T284">adv</ta>
            <ta e="T286" id="Seg_6722" s="T285">ptcl</ta>
            <ta e="T287" id="Seg_6723" s="T286">n</ta>
            <ta e="T288" id="Seg_6724" s="T287">v</ta>
            <ta e="T289" id="Seg_6725" s="T288">n</ta>
            <ta e="T290" id="Seg_6726" s="T289">v</ta>
            <ta e="T291" id="Seg_6727" s="T290">dempro</ta>
            <ta e="T292" id="Seg_6728" s="T291">n</ta>
            <ta e="T293" id="Seg_6729" s="T292">v</ta>
            <ta e="T294" id="Seg_6730" s="T293">conj</ta>
            <ta e="T295" id="Seg_6731" s="T294">ptcl</ta>
            <ta e="T296" id="Seg_6732" s="T295">ptcl</ta>
            <ta e="T297" id="Seg_6733" s="T296">conj</ta>
            <ta e="T298" id="Seg_6734" s="T297">dempro</ta>
            <ta e="T299" id="Seg_6735" s="T298">n</ta>
            <ta e="T300" id="Seg_6736" s="T299">pers</ta>
            <ta e="T301" id="Seg_6737" s="T300">v</ta>
            <ta e="T302" id="Seg_6738" s="T301">que</ta>
            <ta e="T303" id="Seg_6739" s="T302">v</ta>
            <ta e="T304" id="Seg_6740" s="T303">n</ta>
            <ta e="T305" id="Seg_6741" s="T304">ordnum</ta>
            <ta e="T306" id="Seg_6742" s="T305">n</ta>
            <ta e="T307" id="Seg_6743" s="T306">ptcl</ta>
            <ta e="T308" id="Seg_6744" s="T307">dempro</ta>
            <ta e="T309" id="Seg_6745" s="T308">n</ta>
            <ta e="T310" id="Seg_6746" s="T309">adv</ta>
            <ta e="T311" id="Seg_6747" s="T310">n</ta>
            <ta e="T312" id="Seg_6748" s="T311">v</ta>
            <ta e="T313" id="Seg_6749" s="T312">n</ta>
            <ta e="T314" id="Seg_6750" s="T313">n</ta>
            <ta e="T315" id="Seg_6751" s="T314">post</ta>
            <ta e="T316" id="Seg_6752" s="T315">adv</ta>
            <ta e="T317" id="Seg_6753" s="T316">adj</ta>
            <ta e="T318" id="Seg_6754" s="T317">n</ta>
            <ta e="T319" id="Seg_6755" s="T318">v</ta>
            <ta e="T320" id="Seg_6756" s="T319">ptcl</ta>
            <ta e="T321" id="Seg_6757" s="T320">pers</ta>
            <ta e="T322" id="Seg_6758" s="T321">conj</ta>
            <ta e="T323" id="Seg_6759" s="T322">n</ta>
            <ta e="T324" id="Seg_6760" s="T323">v</ta>
            <ta e="T325" id="Seg_6761" s="T324">n</ta>
            <ta e="T326" id="Seg_6762" s="T325">v</ta>
            <ta e="T327" id="Seg_6763" s="T326">conj</ta>
            <ta e="T328" id="Seg_6764" s="T327">adv</ta>
            <ta e="T329" id="Seg_6765" s="T328">n</ta>
            <ta e="T330" id="Seg_6766" s="T329">posspr</ta>
            <ta e="T331" id="Seg_6767" s="T330">n</ta>
            <ta e="T332" id="Seg_6768" s="T331">adv</ta>
            <ta e="T333" id="Seg_6769" s="T332">v</ta>
            <ta e="T334" id="Seg_6770" s="T333">ptcl</ta>
            <ta e="T335" id="Seg_6771" s="T334">pers</ta>
            <ta e="T336" id="Seg_6772" s="T335">pers</ta>
            <ta e="T337" id="Seg_6773" s="T336">ptcl</ta>
            <ta e="T338" id="Seg_6774" s="T337">n</ta>
            <ta e="T339" id="Seg_6775" s="T338">n</ta>
            <ta e="T340" id="Seg_6776" s="T339">v</ta>
            <ta e="T341" id="Seg_6777" s="T340">conj</ta>
            <ta e="T342" id="Seg_6778" s="T341">n</ta>
            <ta e="T343" id="Seg_6779" s="T342">post</ta>
            <ta e="T344" id="Seg_6780" s="T343">n</ta>
            <ta e="T345" id="Seg_6781" s="T344">v</ta>
            <ta e="T346" id="Seg_6782" s="T345">adv</ta>
            <ta e="T347" id="Seg_6783" s="T346">ptcl</ta>
            <ta e="T348" id="Seg_6784" s="T347">n</ta>
            <ta e="T349" id="Seg_6785" s="T348">adv</ta>
            <ta e="T350" id="Seg_6786" s="T349">n</ta>
            <ta e="T351" id="Seg_6787" s="T350">v</ta>
            <ta e="T352" id="Seg_6788" s="T351">aux</ta>
            <ta e="T353" id="Seg_6789" s="T352">conj</ta>
            <ta e="T354" id="Seg_6790" s="T353">v</ta>
            <ta e="T355" id="Seg_6791" s="T354">n</ta>
            <ta e="T356" id="Seg_6792" s="T355">adv</ta>
            <ta e="T357" id="Seg_6793" s="T356">v</ta>
            <ta e="T358" id="Seg_6794" s="T357">n</ta>
            <ta e="T359" id="Seg_6795" s="T358">emphpro</ta>
            <ta e="T360" id="Seg_6796" s="T359">n</ta>
            <ta e="T361" id="Seg_6797" s="T360">post</ta>
            <ta e="T362" id="Seg_6798" s="T361">emphpro</ta>
            <ta e="T363" id="Seg_6799" s="T362">n</ta>
            <ta e="T364" id="Seg_6800" s="T363">post</ta>
            <ta e="T365" id="Seg_6801" s="T364">v</ta>
            <ta e="T366" id="Seg_6802" s="T365">ptcl</ta>
            <ta e="T367" id="Seg_6803" s="T366">ptcl</ta>
            <ta e="T368" id="Seg_6804" s="T367">v</ta>
            <ta e="T369" id="Seg_6805" s="T368">ptcl</ta>
            <ta e="T370" id="Seg_6806" s="T369">interj</ta>
            <ta e="T371" id="Seg_6807" s="T370">conj</ta>
            <ta e="T372" id="Seg_6808" s="T371">adv</ta>
            <ta e="T373" id="Seg_6809" s="T372">dempro</ta>
            <ta e="T374" id="Seg_6810" s="T373">dempro</ta>
            <ta e="T375" id="Seg_6811" s="T374">adj</ta>
            <ta e="T376" id="Seg_6812" s="T375">adj</ta>
            <ta e="T377" id="Seg_6813" s="T376">n</ta>
            <ta e="T378" id="Seg_6814" s="T377">v</ta>
            <ta e="T379" id="Seg_6815" s="T378">post</ta>
            <ta e="T380" id="Seg_6816" s="T379">ptcl</ta>
            <ta e="T381" id="Seg_6817" s="T380">n</ta>
            <ta e="T384" id="Seg_6818" s="T383">v</ta>
            <ta e="T385" id="Seg_6819" s="T384">cop</ta>
            <ta e="T386" id="Seg_6820" s="T385">adv</ta>
            <ta e="T387" id="Seg_6821" s="T386">adv</ta>
            <ta e="T388" id="Seg_6822" s="T387">adv</ta>
            <ta e="T389" id="Seg_6823" s="T388">v</ta>
            <ta e="T390" id="Seg_6824" s="T389">quant</ta>
            <ta e="T391" id="Seg_6825" s="T390">n</ta>
            <ta e="T392" id="Seg_6826" s="T391">v</ta>
            <ta e="T393" id="Seg_6827" s="T392">interj</ta>
            <ta e="T394" id="Seg_6828" s="T393">propr</ta>
            <ta e="T395" id="Seg_6829" s="T394">emphpro</ta>
            <ta e="T396" id="Seg_6830" s="T395">adj</ta>
            <ta e="T397" id="Seg_6831" s="T396">n</ta>
            <ta e="T398" id="Seg_6832" s="T397">ptcl</ta>
            <ta e="T399" id="Seg_6833" s="T398">cop</ta>
            <ta e="T400" id="Seg_6834" s="T399">posspr</ta>
            <ta e="T401" id="Seg_6835" s="T400">n</ta>
            <ta e="T402" id="Seg_6836" s="T401">n</ta>
            <ta e="T403" id="Seg_6837" s="T402">post</ta>
            <ta e="T404" id="Seg_6838" s="T403">adv</ta>
            <ta e="T405" id="Seg_6839" s="T404">ptcl</ta>
            <ta e="T406" id="Seg_6840" s="T405">propr</ta>
            <ta e="T407" id="Seg_6841" s="T406">v</ta>
            <ta e="T408" id="Seg_6842" s="T407">interj</ta>
            <ta e="T409" id="Seg_6843" s="T408">propr</ta>
            <ta e="T410" id="Seg_6844" s="T409">propr</ta>
            <ta e="T411" id="Seg_6845" s="T410">n</ta>
            <ta e="T412" id="Seg_6846" s="T411">ptcl</ta>
            <ta e="T413" id="Seg_6847" s="T412">cop</ta>
            <ta e="T414" id="Seg_6848" s="T413">adj</ta>
            <ta e="T415" id="Seg_6849" s="T414">n</ta>
            <ta e="T416" id="Seg_6850" s="T415">v</ta>
            <ta e="T417" id="Seg_6851" s="T416">adv</ta>
            <ta e="T418" id="Seg_6852" s="T417">quant</ta>
            <ta e="T419" id="Seg_6853" s="T418">n</ta>
            <ta e="T420" id="Seg_6854" s="T419">v</ta>
            <ta e="T421" id="Seg_6855" s="T420">adv</ta>
            <ta e="T422" id="Seg_6856" s="T421">cardnum</ta>
            <ta e="T423" id="Seg_6857" s="T422">cardnum</ta>
            <ta e="T424" id="Seg_6858" s="T423">n</ta>
            <ta e="T425" id="Seg_6859" s="T424">ptcl</ta>
            <ta e="T426" id="Seg_6860" s="T425">cop</ta>
            <ta e="T427" id="Seg_6861" s="T426">interj</ta>
            <ta e="T428" id="Seg_6862" s="T427">adv</ta>
            <ta e="T429" id="Seg_6863" s="T428">n</ta>
            <ta e="T430" id="Seg_6864" s="T429">adj</ta>
            <ta e="T431" id="Seg_6865" s="T430">ptcl</ta>
            <ta e="T432" id="Seg_6866" s="T431">n</ta>
            <ta e="T433" id="Seg_6867" s="T432">cop</ta>
            <ta e="T434" id="Seg_6868" s="T433">pers</ta>
            <ta e="T435" id="Seg_6869" s="T434">adj</ta>
            <ta e="T436" id="Seg_6870" s="T435">interj</ta>
            <ta e="T437" id="Seg_6871" s="T436">v</ta>
            <ta e="T438" id="Seg_6872" s="T437">v</ta>
            <ta e="T439" id="Seg_6873" s="T438">adj</ta>
            <ta e="T440" id="Seg_6874" s="T439">adv</ta>
            <ta e="T441" id="Seg_6875" s="T440">v</ta>
            <ta e="T442" id="Seg_6876" s="T441">adv</ta>
            <ta e="T443" id="Seg_6877" s="T442">v</ta>
            <ta e="T444" id="Seg_6878" s="T443">adv</ta>
            <ta e="T445" id="Seg_6879" s="T444">n</ta>
            <ta e="T446" id="Seg_6880" s="T445">post</ta>
            <ta e="T447" id="Seg_6881" s="T446">n</ta>
            <ta e="T448" id="Seg_6882" s="T447">v</ta>
            <ta e="T449" id="Seg_6883" s="T448">propr</ta>
            <ta e="T450" id="Seg_6884" s="T449">v</ta>
            <ta e="T451" id="Seg_6885" s="T450">interj</ta>
            <ta e="T452" id="Seg_6886" s="T451">n</ta>
            <ta e="T453" id="Seg_6887" s="T452">propr</ta>
            <ta e="T454" id="Seg_6888" s="T453">v</ta>
            <ta e="T455" id="Seg_6889" s="T454">n</ta>
            <ta e="T456" id="Seg_6890" s="T455">post</ta>
            <ta e="T457" id="Seg_6891" s="T456">v</ta>
            <ta e="T458" id="Seg_6892" s="T457">dempro</ta>
            <ta e="T459" id="Seg_6893" s="T458">propr</ta>
            <ta e="T460" id="Seg_6894" s="T459">ptcl</ta>
            <ta e="T461" id="Seg_6895" s="T460">adj</ta>
            <ta e="T462" id="Seg_6896" s="T461">n</ta>
            <ta e="T463" id="Seg_6897" s="T462">v</ta>
            <ta e="T464" id="Seg_6898" s="T463">propr</ta>
            <ta e="T465" id="Seg_6899" s="T464">v</ta>
            <ta e="T466" id="Seg_6900" s="T465">n</ta>
            <ta e="T467" id="Seg_6901" s="T466">dempro</ta>
            <ta e="T468" id="Seg_6902" s="T467">v</ta>
            <ta e="T469" id="Seg_6903" s="T468">v</ta>
            <ta e="T471" id="Seg_6904" s="T470">n</ta>
            <ta e="T472" id="Seg_6905" s="T471">v</ta>
            <ta e="T473" id="Seg_6906" s="T472">v</ta>
            <ta e="T474" id="Seg_6907" s="T473">v</ta>
            <ta e="T475" id="Seg_6908" s="T474">adj</ta>
            <ta e="T476" id="Seg_6909" s="T475">ptcl</ta>
            <ta e="T477" id="Seg_6910" s="T476">cop</ta>
            <ta e="T478" id="Seg_6911" s="T477">n</ta>
            <ta e="T479" id="Seg_6912" s="T478">v</ta>
            <ta e="T480" id="Seg_6913" s="T479">pers</ta>
            <ta e="T481" id="Seg_6914" s="T480">adv</ta>
            <ta e="T482" id="Seg_6915" s="T481">adv</ta>
            <ta e="T483" id="Seg_6916" s="T482">adv</ta>
            <ta e="T484" id="Seg_6917" s="T483">v</ta>
            <ta e="T485" id="Seg_6918" s="T484">interj</ta>
            <ta e="T486" id="Seg_6919" s="T485">dempro</ta>
            <ta e="T487" id="Seg_6920" s="T486">n</ta>
            <ta e="T488" id="Seg_6921" s="T487">dempro</ta>
            <ta e="T489" id="Seg_6922" s="T488">cardnum</ta>
            <ta e="T490" id="Seg_6923" s="T489">n</ta>
            <ta e="T491" id="Seg_6924" s="T490">cop</ta>
            <ta e="T492" id="Seg_6925" s="T491">interj</ta>
            <ta e="T493" id="Seg_6926" s="T492">cardnum</ta>
            <ta e="T494" id="Seg_6927" s="T493">n</ta>
            <ta e="T495" id="Seg_6928" s="T494">cop</ta>
            <ta e="T496" id="Seg_6929" s="T495">interj</ta>
            <ta e="T497" id="Seg_6930" s="T496">cardnum</ta>
            <ta e="T498" id="Seg_6931" s="T497">n</ta>
            <ta e="T499" id="Seg_6932" s="T498">v</ta>
            <ta e="T500" id="Seg_6933" s="T499">propr</ta>
            <ta e="T501" id="Seg_6934" s="T500">adj</ta>
            <ta e="T502" id="Seg_6935" s="T501">ptcl</ta>
            <ta e="T503" id="Seg_6936" s="T502">n</ta>
            <ta e="T504" id="Seg_6937" s="T503">cop</ta>
            <ta e="T505" id="Seg_6938" s="T504">n</ta>
            <ta e="T506" id="Seg_6939" s="T505">dempro</ta>
            <ta e="T507" id="Seg_6940" s="T506">n</ta>
            <ta e="T508" id="Seg_6941" s="T507">v</ta>
            <ta e="T509" id="Seg_6942" s="T508">aux</ta>
            <ta e="T510" id="Seg_6943" s="T509">v</ta>
            <ta e="T511" id="Seg_6944" s="T510">aux</ta>
            <ta e="T512" id="Seg_6945" s="T511">adv</ta>
            <ta e="T513" id="Seg_6946" s="T512">cardnum</ta>
            <ta e="T514" id="Seg_6947" s="T513">post</ta>
            <ta e="T515" id="Seg_6948" s="T514">n</ta>
            <ta e="T516" id="Seg_6949" s="T515">cop</ta>
            <ta e="T517" id="Seg_6950" s="T516">cardnum</ta>
            <ta e="T518" id="Seg_6951" s="T517">cardnum</ta>
            <ta e="T519" id="Seg_6952" s="T518">cardnum</ta>
            <ta e="T520" id="Seg_6953" s="T519">cardnum</ta>
            <ta e="T521" id="Seg_6954" s="T520">n</ta>
            <ta e="T522" id="Seg_6955" s="T521">cop</ta>
            <ta e="T523" id="Seg_6956" s="T522">aux</ta>
            <ta e="T524" id="Seg_6957" s="T523">dempro</ta>
            <ta e="T525" id="Seg_6958" s="T524">n</ta>
            <ta e="T526" id="Seg_6959" s="T525">dempro</ta>
            <ta e="T527" id="Seg_6960" s="T526">post</ta>
            <ta e="T528" id="Seg_6961" s="T527">ptcl</ta>
            <ta e="T529" id="Seg_6962" s="T528">pers</ta>
            <ta e="T530" id="Seg_6963" s="T529">adv</ta>
            <ta e="T531" id="Seg_6964" s="T530">ptcl</ta>
            <ta e="T532" id="Seg_6965" s="T531">adj</ta>
            <ta e="T533" id="Seg_6966" s="T532">ptcl</ta>
            <ta e="T534" id="Seg_6967" s="T533">cop</ta>
            <ta e="T535" id="Seg_6968" s="T534">conj</ta>
            <ta e="T536" id="Seg_6969" s="T535">adv</ta>
            <ta e="T537" id="Seg_6970" s="T536">ptcl</ta>
            <ta e="T538" id="Seg_6971" s="T537">conj</ta>
            <ta e="T539" id="Seg_6972" s="T538">adj</ta>
            <ta e="T540" id="Seg_6973" s="T539">ptcl</ta>
            <ta e="T541" id="Seg_6974" s="T540">cop</ta>
            <ta e="T542" id="Seg_6975" s="T541">dempro</ta>
            <ta e="T543" id="Seg_6976" s="T542">post</ta>
            <ta e="T544" id="Seg_6977" s="T543">adv</ta>
            <ta e="T545" id="Seg_6978" s="T544">adj</ta>
            <ta e="T546" id="Seg_6979" s="T545">pers</ta>
            <ta e="T547" id="Seg_6980" s="T546">pers</ta>
            <ta e="T548" id="Seg_6981" s="T547">n</ta>
            <ta e="T549" id="Seg_6982" s="T548">dempro</ta>
            <ta e="T550" id="Seg_6983" s="T549">n</ta>
            <ta e="T551" id="Seg_6984" s="T550">pers</ta>
            <ta e="T552" id="Seg_6985" s="T551">dempro</ta>
            <ta e="T553" id="Seg_6986" s="T552">n</ta>
            <ta e="T554" id="Seg_6987" s="T553">adv</ta>
            <ta e="T555" id="Seg_6988" s="T554">adv</ta>
            <ta e="T556" id="Seg_6989" s="T555">v</ta>
            <ta e="T557" id="Seg_6990" s="T556">dempro</ta>
            <ta e="T558" id="Seg_6991" s="T557">post</ta>
            <ta e="T559" id="Seg_6992" s="T558">adv</ta>
            <ta e="T560" id="Seg_6993" s="T559">conj</ta>
            <ta e="T561" id="Seg_6994" s="T560">v</ta>
            <ta e="T562" id="Seg_6995" s="T561">pers</ta>
            <ta e="T563" id="Seg_6996" s="T562">n</ta>
            <ta e="T564" id="Seg_6997" s="T563">pers</ta>
            <ta e="T565" id="Seg_6998" s="T564">ptcl</ta>
            <ta e="T566" id="Seg_6999" s="T565">v</ta>
            <ta e="T567" id="Seg_7000" s="T566">post</ta>
            <ta e="T568" id="Seg_7001" s="T567">adv</ta>
            <ta e="T569" id="Seg_7002" s="T568">adv</ta>
            <ta e="T570" id="Seg_7003" s="T569">v</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-UkOA" />
         <annotation name="SyF" tierref="SyF-UkOA" />
         <annotation name="IST" tierref="IST-UkOA" />
         <annotation name="Top" tierref="Top-UkOA" />
         <annotation name="Foc" tierref="Foc-UkOA" />
         <annotation name="BOR" tierref="BOR-UkOA">
            <ta e="T12" id="Seg_7004" s="T11">RUS:cult</ta>
            <ta e="T17" id="Seg_7005" s="T16">RUS:cult</ta>
            <ta e="T18" id="Seg_7006" s="T17">RUS:cult</ta>
            <ta e="T20" id="Seg_7007" s="T19">RUS:cult</ta>
            <ta e="T33" id="Seg_7008" s="T32">RUS:cult</ta>
            <ta e="T34" id="Seg_7009" s="T33">RUS:cult</ta>
            <ta e="T35" id="Seg_7010" s="T34">RUS:cult</ta>
            <ta e="T45" id="Seg_7011" s="T44">RUS:gram</ta>
            <ta e="T54" id="Seg_7012" s="T53">RUS:cult</ta>
            <ta e="T57" id="Seg_7013" s="T56">RUS:cult</ta>
            <ta e="T74" id="Seg_7014" s="T73">RUS:cult</ta>
            <ta e="T79" id="Seg_7015" s="T78">RUS:mod</ta>
            <ta e="T80" id="Seg_7016" s="T79">RUS:cult</ta>
            <ta e="T84" id="Seg_7017" s="T83">RUS:cult</ta>
            <ta e="T90" id="Seg_7018" s="T89">RUS:cult</ta>
            <ta e="T108" id="Seg_7019" s="T107">RUS:cult</ta>
            <ta e="T112" id="Seg_7020" s="T111">RUS:cult</ta>
            <ta e="T116" id="Seg_7021" s="T115">RUS:gram</ta>
            <ta e="T125" id="Seg_7022" s="T124">RUS:cult</ta>
            <ta e="T133" id="Seg_7023" s="T132">RUS:cult</ta>
            <ta e="T135" id="Seg_7024" s="T134">RUS:cult</ta>
            <ta e="T137" id="Seg_7025" s="T136">RUS:cult</ta>
            <ta e="T138" id="Seg_7026" s="T137">RUS:cult</ta>
            <ta e="T155" id="Seg_7027" s="T154">RUS:cult</ta>
            <ta e="T165" id="Seg_7028" s="T164">RUS:cult</ta>
            <ta e="T166" id="Seg_7029" s="T165">RUS:cult</ta>
            <ta e="T170" id="Seg_7030" s="T169">RUS:cult</ta>
            <ta e="T175" id="Seg_7031" s="T174">RUS:mod</ta>
            <ta e="T176" id="Seg_7032" s="T175">RUS:gram</ta>
            <ta e="T190" id="Seg_7033" s="T189">RUS:mod</ta>
            <ta e="T195" id="Seg_7034" s="T194">RUS:cult</ta>
            <ta e="T198" id="Seg_7035" s="T197">RUS:cult</ta>
            <ta e="T199" id="Seg_7036" s="T198">RUS:gram</ta>
            <ta e="T200" id="Seg_7037" s="T199">RUS:gram</ta>
            <ta e="T209" id="Seg_7038" s="T208">RUS:gram</ta>
            <ta e="T214" id="Seg_7039" s="T213">RUS:disc</ta>
            <ta e="T215" id="Seg_7040" s="T214">RUS:disc</ta>
            <ta e="T218" id="Seg_7041" s="T217">RUS:gram</ta>
            <ta e="T228" id="Seg_7042" s="T227">RUS:mod</ta>
            <ta e="T243" id="Seg_7043" s="T242">RUS:mod</ta>
            <ta e="T244" id="Seg_7044" s="T243">RUS:gram</ta>
            <ta e="T248" id="Seg_7045" s="T247">RUS:mod</ta>
            <ta e="T249" id="Seg_7046" s="T248">RUS:gram</ta>
            <ta e="T252" id="Seg_7047" s="T251">RUS:gram</ta>
            <ta e="T263" id="Seg_7048" s="T262">RUS:gram</ta>
            <ta e="T265" id="Seg_7049" s="T264">RUS:gram</ta>
            <ta e="T269" id="Seg_7050" s="T268">RUS:mod</ta>
            <ta e="T270" id="Seg_7051" s="T269">RUS:gram</ta>
            <ta e="T292" id="Seg_7052" s="T291">RUS:cult</ta>
            <ta e="T294" id="Seg_7053" s="T293">RUS:gram</ta>
            <ta e="T296" id="Seg_7054" s="T295">RUS:disc</ta>
            <ta e="T297" id="Seg_7055" s="T296">RUS:gram</ta>
            <ta e="T299" id="Seg_7056" s="T298">RUS:cult</ta>
            <ta e="T301" id="Seg_7057" s="T300">RUS:cult</ta>
            <ta e="T306" id="Seg_7058" s="T305">RUS:cult</ta>
            <ta e="T318" id="Seg_7059" s="T317">RUS:cult</ta>
            <ta e="T320" id="Seg_7060" s="T319">RUS:mod</ta>
            <ta e="T322" id="Seg_7061" s="T321">RUS:gram</ta>
            <ta e="T327" id="Seg_7062" s="T326">RUS:gram</ta>
            <ta e="T340" id="Seg_7063" s="T339">RUS:cult</ta>
            <ta e="T341" id="Seg_7064" s="T340">RUS:gram</ta>
            <ta e="T350" id="Seg_7065" s="T349">RUS:core</ta>
            <ta e="T353" id="Seg_7066" s="T352">RUS:gram</ta>
            <ta e="T371" id="Seg_7067" s="T370">RUS:gram</ta>
            <ta e="T375" id="Seg_7068" s="T374">RUS:core</ta>
            <ta e="T377" id="Seg_7069" s="T376">RUS:cult</ta>
            <ta e="T381" id="Seg_7070" s="T380">RUS:cult</ta>
            <ta e="T389" id="Seg_7071" s="T388">RUS:cult</ta>
            <ta e="T391" id="Seg_7072" s="T390">RUS:cult</ta>
            <ta e="T394" id="Seg_7073" s="T393">RUS:cult</ta>
            <ta e="T396" id="Seg_7074" s="T395">RUS:cult</ta>
            <ta e="T397" id="Seg_7075" s="T396">RUS:cult</ta>
            <ta e="T401" id="Seg_7076" s="T400">RUS:cult</ta>
            <ta e="T406" id="Seg_7077" s="T405">RUS:cult</ta>
            <ta e="T409" id="Seg_7078" s="T408">RUS:cult</ta>
            <ta e="T410" id="Seg_7079" s="T409">RUS:cult</ta>
            <ta e="T411" id="Seg_7080" s="T410">RUS:cult</ta>
            <ta e="T424" id="Seg_7081" s="T423">RUS:cult</ta>
            <ta e="T429" id="Seg_7082" s="T428">RUS:cult</ta>
            <ta e="T445" id="Seg_7083" s="T444">RUS:cult</ta>
            <ta e="T447" id="Seg_7084" s="T446">RUS:cult</ta>
            <ta e="T449" id="Seg_7085" s="T448">RUS:cult</ta>
            <ta e="T453" id="Seg_7086" s="T452">RUS:cult</ta>
            <ta e="T459" id="Seg_7087" s="T458">RUS:cult</ta>
            <ta e="T464" id="Seg_7088" s="T463">RUS:cult</ta>
            <ta e="T475" id="Seg_7089" s="T474">RUS:core</ta>
            <ta e="T478" id="Seg_7090" s="T477">RUS:cult</ta>
            <ta e="T500" id="Seg_7091" s="T499">RUS:cult</ta>
            <ta e="T515" id="Seg_7092" s="T514">RUS:cult</ta>
            <ta e="T521" id="Seg_7093" s="T520">RUS:cult</ta>
            <ta e="T535" id="Seg_7094" s="T534">RUS:gram</ta>
            <ta e="T538" id="Seg_7095" s="T537">RUS:gram</ta>
            <ta e="T550" id="Seg_7096" s="T549">RUS:cult</ta>
            <ta e="T559" id="Seg_7097" s="T558">RUS:mod</ta>
            <ta e="T560" id="Seg_7098" s="T559">RUS:gram</ta>
            <ta e="T568" id="Seg_7099" s="T567">RUS:mod</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-UkOA" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-UkOA" />
         <annotation name="CS" tierref="CS-UkOA" />
         <annotation name="fe" tierref="fe-UkOA">
            <ta e="T4" id="Seg_7100" s="T1">– I'll tell in Dolgan, right?</ta>
            <ta e="T24" id="Seg_7101" s="T8">– On the 22nd July 2010 there was a festival in Khanty-Mansiysk named "Northern Lights".</ta>
            <ta e="T30" id="Seg_7102" s="T24">From Taymyr we went there with two people.</ta>
            <ta e="T47" id="Seg_7103" s="T30">One of us was Vladimir Enovich Sigunej, a man titled "National Artist of Russia" and "Merited Artist of Russia", and I from Khatanga.</ta>
            <ta e="T54" id="Seg_7104" s="T47">We were received there very well, with salt and bread.</ta>
            <ta e="T64" id="Seg_7105" s="T54">We were accomodated in a very big hotel, its name was "Seven hills".</ta>
            <ta e="T74" id="Seg_7106" s="T64">We were fed very well there, were fed and accomodated, then in the next morning we rehearsed.</ta>
            <ta e="T80" id="Seg_7107" s="T74">In that evening, in that evening we performed.</ta>
            <ta e="T87" id="Seg_7108" s="T80">After that a round table took place on the second day.</ta>
            <ta e="T96" id="Seg_7109" s="T87">At the round table many different people gathered.</ta>
            <ta e="T104" id="Seg_7110" s="T96">There were discussions about the pleasant and bad sides of life.</ta>
            <ta e="T127" id="Seg_7111" s="T104">About the children's learning, eh, about the colleges, about why they drop it or why they don't learn, the big question came up.</ta>
            <ta e="T138" id="Seg_7112" s="T127">And then an old woman appeared, Mariya Kuzminichna Moldanova.</ta>
            <ta e="T142" id="Seg_7113" s="T138">A clever woman, she said:</ta>
            <ta e="T156" id="Seg_7114" s="T142">Eh, the young people, eh, the young people should be risen up in their own families, inside their own families.</ta>
            <ta e="T163" id="Seg_7115" s="T156">A person, who got a child, has to teach the child good and bad.</ta>
            <ta e="T181" id="Seg_7116" s="T163">The school, the kindergarten, eh, the educational institutions can only help, they [the people] have to be the base.</ta>
            <ta e="T198" id="Seg_7117" s="T181">Therefore, this woman says, from now on one has to increase the responsibilty of the people who get a child, of the parents. </ta>
            <ta e="T213" id="Seg_7118" s="T198">And one has to multiply the good, this woman says, one has to multiply the good and to reduce the bad on this earth. </ta>
            <ta e="T234" id="Seg_7119" s="T213">So, well, when you notice that people spoke badly about you, that you were done injustice, then you shouldn't speak about that person, the woman says.</ta>
            <ta e="T251" id="Seg_7120" s="T234">One shouldn't say anything about the person to him-/herself, but you shouldn't say anything to the people either, for that the bad doesn't increase.</ta>
            <ta e="T264" id="Seg_7121" s="T251">But a person who did something good, one shouldn't say anything to this person, too, for that s/he doesn't brag. </ta>
            <ta e="T283" id="Seg_7122" s="T264">But one should tell other people that this person has done something good, that this person is like this and that. </ta>
            <ta e="T288" id="Seg_7123" s="T284">Then the good increases.</ta>
            <ta e="T301" id="Seg_7124" s="T288">The good increases with this formula, but I don't agree with this formula.</ta>
            <ta e="T303" id="Seg_7125" s="T301">Why do you say?</ta>
            <ta e="T312" id="Seg_7126" s="T303">The bad, there is, moreover, a second formula, for that the bad doesn't increase.</ta>
            <ta e="T335" id="Seg_7127" s="T312">The bad, one has to give a big answer to it in the future, for that a person, who did something bad, knows that the bad doesn't simply pass by him. </ta>
            <ta e="T352" id="Seg_7128" s="T335">It has to be the person's responsibility, for that he won't do anything bad in the future, then the good increases twice as much. </ta>
            <ta e="T369" id="Seg_7129" s="T352">And the living people, the people living there won't accept any offendings in the future, won't accept offendings.</ta>
            <ta e="T392" id="Seg_7130" s="T369">Ah, and then, when the round table was over, there was also a concert, a "gala concert", they did perform very well, many collectives had come.</ta>
            <ta e="T403" id="Seg_7131" s="T392">There was a theater group from Khanty-Mansiysk, like our ensemble "Xejro".</ta>
            <ta e="T416" id="Seg_7132" s="T403">Then they came from Magadan, eh, from the Yamalo-Nenets Autonomous District, there were ensembles from Chukotka, they came from places far away.</ta>
            <ta e="T421" id="Seg_7133" s="T416">They came there from many, many places.</ta>
            <ta e="T427" id="Seg_7134" s="T421">There were twenty-two collectives.</ta>
            <ta e="T457" id="Seg_7135" s="T427">Then our curators were very good women, they took and brought us, they accomodated us like small children and fed us, then through museums, then we went by boat on the river Irtysh.</ta>
            <ta e="T469" id="Seg_7136" s="T457">The Irtysh enters into a big river, named "Ob", at the mouth we (gave presents?).</ta>
            <ta e="T477" id="Seg_7137" s="T469">We sang songs, in order to come and go, it was very joyful.</ta>
            <ta e="T487" id="Seg_7138" s="T477">Then we were filmed, we came back in the next morning.</ta>
            <ta e="T505" id="Seg_7139" s="T487">These three days we were, eh, it was three days, eh, when we were in Khanty-Mansiysk for three days, it were very good days, the weather.</ta>
            <ta e="T525" id="Seg_7140" s="T505">Before it had rained, they said, then it was more then 20 degrees, it was 24, 25 degrees on these days.</ta>
            <ta e="T546" id="Seg_7141" s="T525">Therefore it was neither too warm nor too cold for us, therefore it was very good for us.</ta>
            <ta e="T556" id="Seg_7142" s="T546">From my point of view such a climate for them, they have it good with the weather.</ta>
            <ta e="T570" id="Seg_7143" s="T556">Therefore, if they call once more, I think, I would go once more for a visit.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-UkOA">
            <ta e="T4" id="Seg_7144" s="T1">– Auf Dolganisch erzähle ich?</ta>
            <ta e="T24" id="Seg_7145" s="T8">– Am 22. Juni 2010 fand in Chanty-Mansijsk ein Festival mit dem Namen "Polarlicht" statt.</ta>
            <ta e="T30" id="Seg_7146" s="T24">Dorthin sind von der Tajmyr wir zwei Leute gefahren.</ta>
            <ta e="T47" id="Seg_7147" s="T30">Der eine war Vladimir Enowitsch Sigunej, ein Mensch mit den Titeln "Nationaler Künstler Russlands" und "Verdienter Künstler Russlands", und ich aus Chatanga.</ta>
            <ta e="T54" id="Seg_7148" s="T47">Dort wurden wir sehr gut empfangen, mit Salz und Brot.</ta>
            <ta e="T64" id="Seg_7149" s="T54">In einem sehr großen Hotel wurden wir unterbracht, sein Name war "Sieben Hügel".</ta>
            <ta e="T74" id="Seg_7150" s="T64">Wir werden sehr gut verköstigt dort, wurden verköstigt und untergebracht, dann am nächsten Morgen probten wir.</ta>
            <ta e="T80" id="Seg_7151" s="T74">An jenem Abend, an jenem Abend sind wir aufgetreten.</ta>
            <ta e="T87" id="Seg_7152" s="T80">Danach fand am zweiten Tag ein runder Tisch statt.</ta>
            <ta e="T96" id="Seg_7153" s="T87">Am runden Tisch versammelten sich viele unterschiedliche Leute. </ta>
            <ta e="T104" id="Seg_7154" s="T96">Es gab Gespräche über das Erfreuliche und das Schlechte im Leben.</ta>
            <ta e="T127" id="Seg_7155" s="T104">Über das Lernen der Kinder, äh, über die Hochschulen, darüber, warum sie es hinwerfen oder warum sie nicht lernen, kam die große Frage auf.</ta>
            <ta e="T138" id="Seg_7156" s="T127">Dann aber trat eine alte Frau auf, Marija Kuzminitschna Moldanova.</ta>
            <ta e="T142" id="Seg_7157" s="T138">Eine kluge Frau, sie sagte:</ta>
            <ta e="T156" id="Seg_7158" s="T142">Äh, die jungen Leute, äh, die jungen Leute müssen in ihren eigenen Familien, in ihren eigenen Familien müssen sie erzogen werden.</ta>
            <ta e="T163" id="Seg_7159" s="T156">Ein Mensch, der ein Kind bekommen hat, muss das Kind erziehen, Gutes und Schlechtes.</ta>
            <ta e="T181" id="Seg_7160" s="T163">Die Schule, der Kindergarten, äh, die Lehranstalten können dabei nur helfen, sie [die Menschen] müssen die Basis sein.</ta>
            <ta e="T198" id="Seg_7161" s="T181">Deshalb muss man von jetzt an, sagt diese Frau, die Verantwortung bei den Menschen, die ein Kind bekommen, bei den Eltern erhöhen.</ta>
            <ta e="T213" id="Seg_7162" s="T198">Und das Gute muss man vermehren, sagte diese Frau, das Gute muss man vermehren und das Schlechte muss man vermindern auf dieser Erde.</ta>
            <ta e="T234" id="Seg_7163" s="T213">Nun, also, wenn man feststellt, dass über dich schlecht gesprochen wurde, dass dir Unrecht getan wurde, dann sollst du über diesen Menschen nicht sprechen, sagt diese Frau.</ta>
            <ta e="T251" id="Seg_7164" s="T234">Über den Menschen soll man ihm selbst nichts sagen, aber den Leuten soll man auch nichts sagen, damit das Schlechte nicht wächst.</ta>
            <ta e="T264" id="Seg_7165" s="T251">Aber einem Menschen, der Gutes gemacht hat, diesem Menschen selbst soll man auch nichts sagen, damit er nicht prahlt.</ta>
            <ta e="T283" id="Seg_7166" s="T264">Aber anderen Leuten soll man erzählen, dass dieser Mensch etwas Gutes gemacht hat, dass dieser Mensch so und so ist.</ta>
            <ta e="T288" id="Seg_7167" s="T284">Dann wächst das Gute.</ta>
            <ta e="T301" id="Seg_7168" s="T288">Das Gute wächst mit dieser Formel, aber auch mit dieser Formel bin ich nicht einverstanden.</ta>
            <ta e="T303" id="Seg_7169" s="T301">Warum sagt ihr?</ta>
            <ta e="T312" id="Seg_7170" s="T303">Das Schlechte, es gibt eine zweite Formel darüber hinaus, damit das Schlechte nicht wächst.</ta>
            <ta e="T335" id="Seg_7171" s="T312">Dem Schlechten, man muss ihm in Zukunft eine große Antwort geben, damit ein Mensch, der etwas Schlechtes gemacht hat, weiß, dass das Schlechte nicht einfach an ihm vorüber geht.</ta>
            <ta e="T352" id="Seg_7172" s="T335">Es muss in der Verantwortung des Menschen liegen, damit er in Zukunft nichts Schlechtes macht, dann wächst das Gute gleich doppelt.</ta>
            <ta e="T369" id="Seg_7173" s="T352">Und die lebenden Menschen, die dort lebenden Menschen werden in Zukunft keine Kränkungen mehr zulassen und keine Kränkungen mehr zulassen.</ta>
            <ta e="T392" id="Seg_7174" s="T369">Ah und dann, als der runde Tisch aufgehört hatte, gab es noch ein Konzert, ein "Galakonzert", sie traten sehr gut auf, es waren viele Kollektive gekommen.</ta>
            <ta e="T403" id="Seg_7175" s="T392">Aus Chanty-Mansijsk selbst war eine Theatergruppe da, wie unser Xejro-Ensemble.</ta>
            <ta e="T416" id="Seg_7176" s="T403">Dann kamen sie aus Magadan, äh, aus dem Jamalo-Nenzischen Autonomen Distrikt, aus Tschukotka waren Ensembles da, von fernen Orten kamen sie.</ta>
            <ta e="T421" id="Seg_7177" s="T416">Von vielen, vielen Orten kamen sie dorthin.</ta>
            <ta e="T427" id="Seg_7178" s="T421">22 Kollektive gab es.</ta>
            <ta e="T457" id="Seg_7179" s="T427">Dann unsere Kuratoren waren sehr gute Frauen, sie nahmen und brachten uns, wie kleine Kinder brachten sie uns unter und verköstigten uns, dann durch Museen, dann fuhren wir mit dem Schiff den Fluss Irtysch entlang.</ta>
            <ta e="T469" id="Seg_7180" s="T457">Der Irtysch mündet in einen großen Fluss, der Ob heißt, an der Mündung (beschenkten wir [sie]?).</ta>
            <ta e="T477" id="Seg_7181" s="T469">Wir sangen Lieder, um zu kommen und zu gehen, es war sehr fröhlich.</ta>
            <ta e="T487" id="Seg_7182" s="T477">Gefilmt wurden wir dann, dann kamen wir am nächsten Morgen zurück.</ta>
            <ta e="T505" id="Seg_7183" s="T487">Diese drei Tage waren wir, äh, es waren drei Tage, äh, als wir drei Tage in Chanty-Mansijsk waren, waren es sehr gute Tage, das Wetter.</ta>
            <ta e="T525" id="Seg_7184" s="T505">Vorher hatte es geregnet, wurde gesagt, dann waren es über 20 Grad, es waren 24, 25 Grad an diesen Tagen.</ta>
            <ta e="T546" id="Seg_7185" s="T525">Deshalb war es für uns weder zu warm noch zu kalt, deshalb für uns sehr gut.</ta>
            <ta e="T556" id="Seg_7186" s="T546">Meiner Meinung nach ist so ein Klima für sie, haben sie es gut mit dem Wetter.</ta>
            <ta e="T570" id="Seg_7187" s="T556">Deshalb, wenn sie nochmal einladen, denke ich, würde ich noch einmal auf Besuch fahren.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-UkOA">
            <ta e="T4" id="Seg_7188" s="T1">На долганском говорить?</ta>
            <ta e="T24" id="Seg_7189" s="T8">Двадцать второго июня 2010 года в Ханты-Мансийске прошел фестиваль под названием "Северное сияние".</ta>
            <ta e="T30" id="Seg_7190" s="T24">Туда с Таймыра мы вдвоём поехали.</ta>
            <ta e="T47" id="Seg_7191" s="T30">Другой был Владимир Энович Сигуней, человек со званием "Народный артист России", "Заслуженный артист России", и я из Хатанги.</ta>
            <ta e="T54" id="Seg_7192" s="T47">Там очень хорошо нас встретили, хлебом-солью.</ta>
            <ta e="T64" id="Seg_7193" s="T54">В очень большой гостинице разместили, под названием "Семь холмов".</ta>
            <ta e="T74" id="Seg_7194" s="T64">Очень хорошо нас там накормили, спать уложили, потом с завтрашнего репетиция была.</ta>
            <ta e="T80" id="Seg_7195" s="T74">В тот же вечер выступили.</ta>
            <ta e="T87" id="Seg_7196" s="T80">После этого круглый стол был во второй день.</ta>
            <ta e="T96" id="Seg_7197" s="T87">На этом круглом столе много разных людей собралось, </ta>
            <ta e="T104" id="Seg_7198" s="T96">о хорошем, о плохом, о жизни разговаривали.</ta>
            <ta e="T127" id="Seg_7199" s="T104">Много обсуждали вопрос об учащихся, о студентах, почему высшие учебные заведения бросают или почему не учатся.</ta>
            <ta e="T138" id="Seg_7200" s="T127">Потом выступала бабушка, зовут Молданова Мария Кузьминична.</ta>
            <ta e="T142" id="Seg_7201" s="T138">Умная женщина, так сказала:</ta>
            <ta e="T156" id="Seg_7202" s="T142">Молодые люди, э-э, молодые люди должны воспитываться в своём семейном… в своём семейном кругу.</ta>
            <ta e="T163" id="Seg_7203" s="T156">Образование ребенку дают родители — хорошее [или] плохое.</ta>
            <ta e="T181" id="Seg_7204" s="T163">Школа, садик, учебные заведения должны им только помогать.</ta>
            <ta e="T198" id="Seg_7205" s="T181">Поэтому сейчас сперва надо повысить, говорит эта женщина, ответственность родителям.</ta>
            <ta e="T213" id="Seg_7206" s="T198">Да, и прекрасное преумножать, сказала эта женщина, прекрасное приумножать, а плохое убавлять на этой земле.</ta>
            <ta e="T234" id="Seg_7207" s="T213">Вот, ну, к примеру, если про тебя плохое говорят, плохое [тебе] сделали, не надо рассказывать про этого человека, говорит она.</ta>
            <ta e="T251" id="Seg_7208" s="T234">Про этого человека ему самому говорить не надо, но и людям тоже говорить не надо, чтоб плохого не становилось больше.</ta>
            <ta e="T264" id="Seg_7209" s="T251">А человеку, сделавшему хорошее, этому человеку можешь тоже не говорить, чтобы он не хвастался.</ta>
            <ta e="T283" id="Seg_7210" s="T264">А другим людям надо рассаказывать, что этот человек хорошее сделал, этот человек вот такой, этот человек такой.</ta>
            <ta e="T288" id="Seg_7211" s="T284">Прекрасное растёт.</ta>
            <ta e="T301" id="Seg_7212" s="T288">Прекрасное растёт, по такой формуле то есть, но опять-таки с этой формулой я не согласен.</ta>
            <ta e="T303" id="Seg_7213" s="T301">Почему, скажете? </ta>
            <ta e="T312" id="Seg_7214" s="T303">Плохое — вторая формула есть выше тогда — плохое чтобы не увеличивалось.</ta>
            <ta e="T335" id="Seg_7215" s="T312">Плохому, сперва большой отпор надо ему дать, чтоб человек, сделавший плохое, знал, что его зло не сойдет ему с рук.</ta>
            <ta e="T352" id="Seg_7216" s="T335">Он будет перед человеком отвечать, чтоб впредь плохого не делал, тогда добро вдвойне увеличится.</ta>
            <ta e="T369" id="Seg_7217" s="T352">И живущие люди, живущие там люди себя больше в обиду не дадут.</ta>
            <ta e="T392" id="Seg_7218" s="T369">Ну, потом этот круглый стол когда закончился, снова был концерт, гала-концерт, там очень хорошо выступали, много коллективов приехали.</ta>
            <ta e="T403" id="Seg_7219" s="T392">Из самого Ханты-Мансийска была театральная группа, как наш ансамбль "Хэйро". </ta>
            <ta e="T416" id="Seg_7220" s="T403">Потом из Магадана приехали, из Ямало-Ненецкого автономного округа, с Чукотки ансамбль был, из отдалённых мест приезжали.</ta>
            <ta e="T421" id="Seg_7221" s="T416">Потом из многих-многих мест приехали туда.</ta>
            <ta e="T427" id="Seg_7222" s="T421">Двадцать два коллектива было.</ta>
            <ta e="T457" id="Seg_7223" s="T427">Ещё кураторы были очень хорошие женщины, нас всех сопровождали, приводили, как маленьких детей спать укладывали, потом кормили, потом в музеи, на теплоходе гуляли по реке Иртыш, по реке Иртыш ездили.</ta>
            <ta e="T469" id="Seg_7224" s="T457">Этот Иртыш с большой рекой соединяется по имени Обь, там, где они сливаются, (мы одарили?).</ta>
            <ta e="T477" id="Seg_7225" s="T469">Песни пели, когда приезжали, уезжали, очень весело было.</ta>
            <ta e="T487" id="Seg_7226" s="T477">В кино снимали нас потом, потом мы обратно приехали, на другое утро.</ta>
            <ta e="T505" id="Seg_7227" s="T487">Эти три дня были, три дня в Ханты-Мансийске очень хорошие дни были, погода.</ta>
            <ta e="T525" id="Seg_7228" s="T505">Перед этим дожди были, говорят, потом двадцать с лишним градусов было, двадцать четыре-двадцать пять градусов становилось в эти дни.</ta>
            <ta e="T546" id="Seg_7229" s="T525">Поэтому для нас не очень жарко и не очень холодно было, поэтому для нас очень хорошо.</ta>
            <ta e="T556" id="Seg_7230" s="T546">Я думаю, такой климат для них, такая погода очень хорошо подходит.</ta>
            <ta e="T570" id="Seg_7231" s="T556">Поэтому если ещё пригласят, я думаю, поеду, наверное, ещё один раз, в гости.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr-UkOA">
            <ta e="T4" id="Seg_7232" s="T1">Олег: на долганском говорить?</ta>
            <ta e="T24" id="Seg_7233" s="T8">Двадцать второе июня 2010 года В Ханты-Мансийском городе прошел фестиваль "Северное сияние" назыается.</ta>
            <ta e="T30" id="Seg_7234" s="T24">Туда с Таймыра мы поехали два челвека.</ta>
            <ta e="T47" id="Seg_7235" s="T30">Другой был Владимир Энович Сигуней НАР имеет человек звание ЗАР и я сХатанги.</ta>
            <ta e="T54" id="Seg_7236" s="T47">Там очень хорошо нас встретили, солью с хлебом.</ta>
            <ta e="T64" id="Seg_7237" s="T54">В большой очень гостинице поместили навание будет "семь холмов".</ta>
            <ta e="T74" id="Seg_7238" s="T64">Очень хорошо нас накрмили там кормили, спали, потом с завтрашнего репетиция была.</ta>
            <ta e="T80" id="Seg_7239" s="T74">В тот же вечер выступили.</ta>
            <ta e="T87" id="Seg_7240" s="T80">После этого круглый стол был во второй день</ta>
            <ta e="T96" id="Seg_7241" s="T87">на этом круглом столе разных много людей собралось, </ta>
            <ta e="T104" id="Seg_7242" s="T96">радостное плохое о жизни разговоры были.</ta>
            <ta e="T127" id="Seg_7243" s="T104">дети учаться о них студентах эти большие высшие учебные заведения почему бросают или почему не учаться про это большой это вопрос стоял (поднимали).</ta>
            <ta e="T138" id="Seg_7244" s="T127">Потом было бабушка выступала там Малданова зовут Мария Кузьминична.</ta>
            <ta e="T142" id="Seg_7245" s="T138">Умная женщина, это сказала:</ta>
            <ta e="T156" id="Seg_7246" s="T142">молодые люди (молодежь) ээ молодежь в своём семейном в своём семейном кругу в кругу воспитание должно быть.</ta>
            <ta e="T163" id="Seg_7247" s="T156">Родивший человек дает образование, прекрасное, плохое ребенку.</ta>
            <ta e="T181" id="Seg_7248" s="T163">Школа, садик, к этому ещё учебные заведенияэтому помощью только быть должны.</ta>
            <ta e="T198" id="Seg_7249" s="T181">Поэтому сейчас впереди повысить надо говорит это женщина ответственность родящему человеку родителям.</ta>
            <ta e="T213" id="Seg_7250" s="T198">Да и прекрасного (доброго) прибавитть сказала это женщина, прекрасное (доброе) прибавить аа плохое уменьшить на этой земле.</ta>
            <ta e="T234" id="Seg_7251" s="T213">Вот сравнить посмотреть если про тебя плохое говорят плохое сделали ты говорить не надо про человека этого говорит это женщина.</ta>
            <ta e="T251" id="Seg_7252" s="T234">Про человека этого ему говорить не надо но людям тоже говорить не надо чтоб плохим не вырос.</ta>
            <ta e="T264" id="Seg_7253" s="T251">А хорошее сделавшему человеку этому человеку можешь тоже ему не говорить чтоб не хвастался.</ta>
            <ta e="T283" id="Seg_7254" s="T264">А другим людям рассаказывать надо что этот человек хорошее сделал, этот человек вот такой, этот человек такой.</ta>
            <ta e="T288" id="Seg_7255" s="T284">Прекрасное (доброе) растет.</ta>
            <ta e="T301" id="Seg_7256" s="T288">Прекрасное (доброе) растет, такой формулой то еть, но опять таки этой формуле я не согласен.</ta>
            <ta e="T303" id="Seg_7257" s="T301">Почему сказать? </ta>
            <ta e="T312" id="Seg_7258" s="T303">плохая вторая формула есть на верху тогда плохое не увеличивалось.</ta>
            <ta e="T335" id="Seg_7259" s="T312">Плохое, впереди обратный большой ответ отдать надо ему, чтоб плохое сделавший человек знал, что просто человека его плохое просто не пройдет ему </ta>
            <ta e="T352" id="Seg_7260" s="T335">он будет человеку перед отвечать чтоб в дальнейшем плохое не делал, тогда добро в двойне увеличиться.</ta>
            <ta e="T369" id="Seg_7261" s="T352">И сидящие люди там сидящие люди самих вперед, оскорбить не дадут.</ta>
            <ta e="T392" id="Seg_7262" s="T369">Но потом этот круглый круглый стол закончился после снова концерт гала концерт был там очень хорошо выступили много коллективов приехали.</ta>
            <ta e="T403" id="Seg_7263" s="T392">С Ханты-Мансийска с самого театральная группа была наш ансамбль хэйро как </ta>
            <ta e="T416" id="Seg_7264" s="T403">потом с МАгадана приехали с Ямало-Ненецкого автономного округа, с Чукотки ансамбль был, с отдаленных территорий приезжали.</ta>
            <ta e="T421" id="Seg_7265" s="T416">Потом с многих-с многих территорий приезжали там </ta>
            <ta e="T427" id="Seg_7266" s="T421">двадцать два коллектива было.</ta>
            <ta e="T457" id="Seg_7267" s="T427">Ещё кураторы хорошие очень женщины были нас всем ээ сопровождали, приводили, (как) маленьких детей укладывали, потом кормили, потом в музеи водили на теплоходе гуляли "Иртыш" называется река, "Иртыш" называется река по нему ездили.</ta>
            <ta e="T469" id="Seg_7268" s="T457">Этот Иртыш с большой рекой соединяется "Обь" называется река там … одарили.</ta>
            <ta e="T477" id="Seg_7269" s="T469">….. песни пели по приезду по отъезду весело очень было.</ta>
            <ta e="T487" id="Seg_7270" s="T477">В кино снимали нас потом, потом обратно приехали ээ на завтра.</ta>
            <ta e="T505" id="Seg_7271" s="T487">Это три дня были, ээ три дня бывает, ээ три дня были в Ханты-Мансийском хорошие очень дни были, небо.</ta>
            <ta e="T525" id="Seg_7272" s="T505">Перед этим дожди были говорили потом двадцать с лишним градусов было двадцать четыре-двадцать пять градусов становилось в эти дни </ta>
            <ta e="T546" id="Seg_7273" s="T525">поэтому для нас не очень жарко и не очень холодно было из-за этого очень хорошо для нас.</ta>
            <ta e="T556" id="Seg_7274" s="T546">Я думаю такой климат для них такое небо очень хорошо подходит.</ta>
            <ta e="T570" id="Seg_7275" s="T556">Поэтому ещё пригласят я думаю я поеду наверное ещё один раз, в гости.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-UkOA" />
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-SE"
                      id="tx-SE"
                      speaker="SE"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-SE">
            <ts e="T8" id="Seg_7276" n="sc" s="T4">
               <ts e="T8" id="Seg_7278" n="HIAT:u" s="T4">
                  <nts id="Seg_7279" n="HIAT:ip">–</nts>
                  <nts id="Seg_7280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_7282" n="HIAT:w" s="T4">Mhm</ts>
                  <nts id="Seg_7283" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_7285" n="HIAT:w" s="T5">hakalɨː</ts>
                  <nts id="Seg_7286" n="HIAT:ip">,</nts>
                  <nts id="Seg_7287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_7289" n="HIAT:w" s="T6">hakalɨː</ts>
                  <nts id="Seg_7290" n="HIAT:ip">.</nts>
                  <nts id="Seg_7291" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-SE">
            <ts e="T8" id="Seg_7292" n="sc" s="T4">
               <ts e="T5" id="Seg_7294" n="e" s="T4">– Mhm </ts>
               <ts e="T6" id="Seg_7296" n="e" s="T5">hakalɨː, </ts>
               <ts e="T8" id="Seg_7298" n="e" s="T6">hakalɨː. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-SE">
            <ta e="T8" id="Seg_7299" s="T4">UkOA_2010_Festival_nar.ES.001 (001.002)</ta>
         </annotation>
         <annotation name="st" tierref="st-SE">
            <ta e="T8" id="Seg_7300" s="T4">Мһмм һакалыы, һакалыы.</ta>
         </annotation>
         <annotation name="ts" tierref="ts-SE">
            <ta e="T8" id="Seg_7301" s="T4">– Mhm hakalɨː, hakalɨː. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-SE">
            <ta e="T5" id="Seg_7302" s="T4">mhm</ta>
            <ta e="T6" id="Seg_7303" s="T5">haka-lɨː</ta>
            <ta e="T8" id="Seg_7304" s="T6">haka-lɨː</ta>
         </annotation>
         <annotation name="mp" tierref="mp-SE">
            <ta e="T5" id="Seg_7305" s="T4">mm</ta>
            <ta e="T6" id="Seg_7306" s="T5">haka-LIː</ta>
            <ta e="T8" id="Seg_7307" s="T6">haka-LIː</ta>
         </annotation>
         <annotation name="ge" tierref="ge-SE">
            <ta e="T5" id="Seg_7308" s="T4">mm</ta>
            <ta e="T6" id="Seg_7309" s="T5">Dolgan-SIM</ta>
            <ta e="T8" id="Seg_7310" s="T6">Dolgan-SIM</ta>
         </annotation>
         <annotation name="gg" tierref="gg-SE">
            <ta e="T5" id="Seg_7311" s="T4">mm</ta>
            <ta e="T6" id="Seg_7312" s="T5">dolganisch-SIM</ta>
            <ta e="T8" id="Seg_7313" s="T6">dolganisch-SIM</ta>
         </annotation>
         <annotation name="gr" tierref="gr-SE">
            <ta e="T5" id="Seg_7314" s="T4">мм</ta>
            <ta e="T6" id="Seg_7315" s="T5">долганский-SIM</ta>
            <ta e="T8" id="Seg_7316" s="T6">долганский-SIM</ta>
         </annotation>
         <annotation name="mc" tierref="mc-SE">
            <ta e="T5" id="Seg_7317" s="T4">interj</ta>
            <ta e="T6" id="Seg_7318" s="T5">adj-adj&gt;adv</ta>
            <ta e="T8" id="Seg_7319" s="T6">adj-adj&gt;adv</ta>
         </annotation>
         <annotation name="ps" tierref="ps-SE">
            <ta e="T5" id="Seg_7320" s="T4">interj</ta>
            <ta e="T6" id="Seg_7321" s="T5">adv</ta>
            <ta e="T8" id="Seg_7322" s="T6">adv</ta>
         </annotation>
         <annotation name="CS" tierref="CS-SE" />
         <annotation name="fe" tierref="fe-SE">
            <ta e="T8" id="Seg_7323" s="T4">– Mhm, in Dolgan, in Dolgan.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-SE">
            <ta e="T8" id="Seg_7324" s="T4">– Mhm, auf Dolganisch, auf Dolganisch. </ta>
         </annotation>
         <annotation name="fr" tierref="fr-SE">
            <ta e="T8" id="Seg_7325" s="T4">Да, на долганском, на долганском.</ta>
         </annotation>
         <annotation name="ltr" tierref="ltr-SE">
            <ta e="T8" id="Seg_7326" s="T4">Да на долганском на долганском.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-SE" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T3" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T117" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T195" />
            <conversion-tli id="T196" />
            <conversion-tli id="T197" />
            <conversion-tli id="T198" />
            <conversion-tli id="T199" />
            <conversion-tli id="T200" />
            <conversion-tli id="T201" />
            <conversion-tli id="T202" />
            <conversion-tli id="T203" />
            <conversion-tli id="T204" />
            <conversion-tli id="T205" />
            <conversion-tli id="T206" />
            <conversion-tli id="T207" />
            <conversion-tli id="T208" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T283" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T355" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T483" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T509" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T559" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-UkOA"
                          name="ref"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-UkOA"
                          name="st"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-UkOA"
                          name="ts"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-UkOA"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-UkOA"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-UkOA"
                          name="mb"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-UkOA"
                          name="mp"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-UkOA"
                          name="ge"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-UkOA"
                          name="gg"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-UkOA"
                          name="gr"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-UkOA"
                          name="mc"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-UkOA"
                          name="ps"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-UkOA"
                          name="SeR"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-UkOA"
                          name="SyF"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-UkOA"
                          name="IST"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-UkOA"
                          name="Top"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-UkOA"
                          name="Foc"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-UkOA"
                          name="BOR"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-UkOA"
                          name="BOR-Phon"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-UkOA"
                          name="BOR-Morph"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-UkOA"
                          name="CS"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-UkOA"
                          name="fe"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-UkOA"
                          name="fg"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-UkOA"
                          name="fr"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-UkOA"
                          name="ltr"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-UkOA"
                          name="nt"
                          segmented-tier-id="tx-UkOA"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-SE"
                          name="ref"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-SE"
                          name="st"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-SE"
                          name="ts"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-SE"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-SE"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-SE"
                          name="mb"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-SE"
                          name="mp"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-SE"
                          name="ge"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-SE"
                          name="gg"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-SE"
                          name="gr"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-SE"
                          name="mc"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-SE"
                          name="ps"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-SE"
                          name="CS"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-SE"
                          name="fe"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-SE"
                          name="fg"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-SE"
                          name="fr"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-SE"
                          name="ltr"
                          segmented-tier-id="tx-SE"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-SE"
                          name="nt"
                          segmented-tier-id="tx-SE"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
