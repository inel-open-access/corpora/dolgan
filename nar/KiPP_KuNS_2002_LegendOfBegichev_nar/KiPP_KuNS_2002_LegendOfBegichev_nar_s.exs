<?xml version="1.0" encoding="UTF-8"?>
<!-- (c) http://www.rrz.uni-hamburg.de/exmaralda -->
<segmented-transcription Id="CIDID43563CEB-569B-7350-F07B-6C7ABA95424E">
   <head>
      <meta-information>
         <project-name>Dolgan</project-name>
         <transcription-name>KiPP_KuNS_2002_LegendOfBegichev_nar</transcription-name>
         <referenced-file url="KiPP_KuNS_2002_LegendOfBegichev_nar.wav" />
         <referenced-file url="KiPP_KuNS_2002_LegendOfBegichev_nar.mp3" />
         <ud-meta-information>
            <ud-information attribute-name="# EXB-SOURCE">F:\INEL\corpora\DolganCorpus\nar\KiPP_KuNS_2002_LegendOfBegichev_nar\KiPP_KuNS_2002_LegendOfBegichev_nar.exb</ud-information>
            <ud-information attribute-name="# HIAT:ip">927</ud-information>
            <ud-information attribute-name="# HIAT:w">650</ud-information>
            <ud-information attribute-name="# e">652</ud-information>
            <ud-information attribute-name="# HIAT:non-pho">2</ud-information>
            <ud-information attribute-name="# HIAT:u">81</ud-information>
            <ud-information attribute-name="# sc">20</ud-information>
         </ud-meta-information>
         <comment />
         <transcription-convention />
      </meta-information>
      <speakertable>
         <speaker id="KiPP">
            <abbreviation>KiPP</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
         <speaker id="KuNS">
            <abbreviation>KuNS</abbreviation>
            <sex value="u" />
            <languages-used></languages-used>
            <l1 />
            <l2 />
            <ud-speaker-information></ud-speaker-information>
            <comment />
         </speaker>
      </speakertable>
   </head>
   <segmented-body>
      <common-timeline>
         <tli id="T0" time="0.0" type="appl" />
         <tli id="T1" time="1.377" type="appl" />
         <tli id="T2" time="2.7999804911117807" />
         <tli id="T4" time="3.798" type="appl" />
         <tli id="T5" time="4.32" type="appl" />
         <tli id="T6" time="4.841" type="appl" />
         <tli id="T7" time="5.363" type="appl" />
         <tli id="T8" time="5.885" type="appl" />
         <tli id="T9" time="6.619953875414282" />
         <tli id="T10" time="7.098" type="appl" />
         <tli id="T11" time="7.789" type="appl" />
         <tli id="T12" time="8.393334260256573" />
         <tli id="T13" time="9.086" type="appl" />
         <tli id="T14" time="9.692" type="appl" />
         <tli id="T15" time="10.298" type="appl" />
         <tli id="T16" time="10.904" type="appl" />
         <tli id="T17" time="11.51" type="appl" />
         <tli id="T18" time="12.116" type="appl" />
         <tli id="T19" time="12.722" type="appl" />
         <tli id="T20" time="13.328" type="appl" />
         <tli id="T21" time="13.934" type="appl" />
         <tli id="T22" time="14.54" type="appl" />
         <tli id="T23" time="15.319331738986492" />
         <tli id="T24" time="15.894" type="appl" />
         <tli id="T25" time="16.642" type="appl" />
         <tli id="T26" time="17.336666316002926" />
         <tli id="T27" time="18.017" type="appl" />
         <tli id="T28" time="18.643" type="appl" />
         <tli id="T29" time="19.27" type="appl" />
         <tli id="T30" time="19.897" type="appl" />
         <tli id="T31" time="20.523" type="appl" />
         <tli id="T32" time="21.15" type="appl" />
         <tli id="T33" time="21.777" type="appl" />
         <tli id="T34" time="22.403" type="appl" />
         <tli id="T35" time="23.04333391151808" />
         <tli id="T36" time="23.551" type="appl" />
         <tli id="T37" time="24.072" type="appl" />
         <tli id="T38" time="24.593" type="appl" />
         <tli id="T39" time="25.114" type="appl" />
         <tli id="T40" time="25.635" type="appl" />
         <tli id="T41" time="26.156" type="appl" />
         <tli id="T42" time="26.676" type="appl" />
         <tli id="T43" time="27.197" type="appl" />
         <tli id="T44" time="27.718" type="appl" />
         <tli id="T45" time="28.239" type="appl" />
         <tli id="T46" time="28.76" type="appl" />
         <tli id="T47" time="29.281" type="appl" />
         <tli id="T48" time="29.91533192925012" />
         <tli id="T49" time="30.398" type="appl" />
         <tli id="T50" time="30.995" type="appl" />
         <tli id="T51" time="31.591" type="appl" />
         <tli id="T52" time="32.188" type="appl" />
         <tli id="T53" time="32.784" type="appl" />
         <tli id="T54" time="33.381" type="appl" />
         <tli id="T55" time="33.977" type="appl" />
         <tli id="T56" time="34.574" type="appl" />
         <tli id="T57" time="35.316664738125525" />
         <tli id="T58" time="35.628" type="appl" />
         <tli id="T59" time="36.086" type="appl" />
         <tli id="T60" time="36.543" type="appl" />
         <tli id="T61" time="37.001" type="appl" />
         <tli id="T62" time="37.459" type="appl" />
         <tli id="T63" time="37.917" type="appl" />
         <tli id="T64" time="38.375" type="appl" />
         <tli id="T65" time="38.832" type="appl" />
         <tli id="T66" time="39.29" type="appl" />
         <tli id="T67" time="39.78800077155315" />
         <tli id="T68" time="40.198" type="appl" />
         <tli id="T69" time="40.648" type="appl" />
         <tli id="T70" time="41.099" type="appl" />
         <tli id="T71" time="41.549" type="appl" />
         <tli id="T72" time="41.999" type="appl" />
         <tli id="T73" time="42.449" type="appl" />
         <tli id="T74" time="42.899" type="appl" />
         <tli id="T75" time="43.35" type="appl" />
         <tli id="T76" time="43.8" type="appl" />
         <tli id="T77" time="44.29000065383015" />
         <tli id="T78" time="44.668" type="appl" />
         <tli id="T79" time="45.087" type="appl" />
         <tli id="T80" time="45.505" type="appl" />
         <tli id="T81" time="45.923" type="appl" />
         <tli id="T82" time="46.342" type="appl" />
         <tli id="T83" time="46.90666536490453" />
         <tli id="T84" time="47.272" type="appl" />
         <tli id="T85" time="47.785" type="appl" />
         <tli id="T86" time="48.297" type="appl" />
         <tli id="T87" time="48.809" type="appl" />
         <tli id="T88" time="49.322" type="appl" />
         <tli id="T89" time="49.834" type="appl" />
         <tli id="T90" time="50.346" type="appl" />
         <tli id="T91" time="50.858" type="appl" />
         <tli id="T92" time="51.371" type="appl" />
         <tli id="T93" time="51.883" type="appl" />
         <tli id="T94" time="52.395" type="appl" />
         <tli id="T95" time="52.908" type="appl" />
         <tli id="T96" time="53.55999791400262" />
         <tli id="T97" time="53.739" type="appl" />
         <tli id="T98" time="54.058" type="appl" />
         <tli id="T99" time="54.377" type="appl" />
         <tli id="T100" time="54.696" type="appl" />
         <tli id="T101" time="55.015" type="appl" />
         <tli id="T102" time="55.334" type="appl" />
         <tli id="T103" time="55.653" type="appl" />
         <tli id="T104" time="55.972" type="appl" />
         <tli id="T105" time="56.29" type="appl" />
         <tli id="T106" time="56.609" type="appl" />
         <tli id="T107" time="56.928" type="appl" />
         <tli id="T108" time="57.247" type="appl" />
         <tli id="T109" time="57.566" type="appl" />
         <tli id="T110" time="57.885" type="appl" />
         <tli id="T111" time="58.204" type="appl" />
         <tli id="T112" time="58.523" type="appl" />
         <tli id="T113" time="58.842" type="appl" />
         <tli id="T114" time="59.24099999823827" />
         <tli id="T115" time="59.469" type="appl" />
         <tli id="T116" time="59.778" type="appl" />
         <tli id="T118" time="60.395" type="appl" />
         <tli id="T119" time="60.703" type="appl" />
         <tli id="T120" time="61.012" type="appl" />
         <tli id="T121" time="61.29333595898643" />
         <tli id="T122" time="61.788" type="appl" />
         <tli id="T123" time="62.256" type="appl" />
         <tli id="T124" time="62.723" type="appl" />
         <tli id="T125" time="63.191" type="appl" />
         <tli id="T126" time="63.659" type="appl" />
         <tli id="T127" time="64.127" type="appl" />
         <tli id="T128" time="64.594" type="appl" />
         <tli id="T129" time="65.062" type="appl" />
         <tli id="T130" time="66.13953917219041" />
         <tli id="T131" time="66.29126958609521" type="intp" />
         <tli id="T132" time="66.443" type="appl" />
         <tli id="T133" time="66.9" type="appl" />
         <tli id="T134" time="67.356" type="appl" />
         <tli id="T135" time="67.812" type="appl" />
         <tli id="T136" time="68.269" type="appl" />
         <tli id="T137" time="68.726" type="appl" />
         <tli id="T138" time="69.182" type="appl" />
         <tli id="T139" time="69.638" type="appl" />
         <tli id="T140" time="70.095" type="appl" />
         <tli id="T141" time="70.551" type="appl" />
         <tli id="T142" time="71.42133179833809" />
         <tli id="T143" time="71.507" type="appl" />
         <tli id="T144" time="72.006" type="appl" />
         <tli id="T145" time="72.504" type="appl" />
         <tli id="T146" time="73.003" type="appl" />
         <tli id="T147" time="73.502" type="appl" />
         <tli id="T148" time="74.001" type="appl" />
         <tli id="T149" time="74.5" type="appl" />
         <tli id="T150" time="74.998" type="appl" />
         <tli id="T151" time="75.497" type="appl" />
         <tli id="T152" time="76.00266706534508" />
         <tli id="T153" time="76.3893310379272" />
         <tli id="T154" time="76.915" type="appl" />
         <tli id="T155" time="77.393" type="appl" />
         <tli id="T156" time="77.872" type="appl" />
         <tli id="T157" time="78.35" type="appl" />
         <tli id="T158" time="78.829" type="appl" />
         <tli id="T159" time="79.307" type="appl" />
         <tli id="T160" time="79.786" type="appl" />
         <tli id="T161" time="80.264" type="appl" />
         <tli id="T162" time="80.743" type="appl" />
         <tli id="T163" time="81.222" type="appl" />
         <tli id="T164" time="81.7" type="appl" />
         <tli id="T165" time="82.179" type="appl" />
         <tli id="T166" time="82.657" type="appl" />
         <tli id="T167" time="83.136" type="appl" />
         <tli id="T168" time="83.614" type="appl" />
         <tli id="T169" time="84.55941083157578" />
         <tli id="T170" time="84.627" type="appl" />
         <tli id="T171" time="85.16" type="appl" />
         <tli id="T172" time="85.694" type="appl" />
         <tli id="T173" time="86.227" type="appl" />
         <tli id="T174" time="86.761" type="appl" />
         <tli id="T175" time="87.294" type="appl" />
         <tli id="T176" time="87.828" type="appl" />
         <tli id="T177" time="88.361" type="appl" />
         <tli id="T178" time="88.93499883421198" />
         <tli id="T179" time="89.305" type="appl" />
         <tli id="T180" time="89.716" type="appl" />
         <tli id="T181" time="90.126" type="appl" />
         <tli id="T182" time="90.536" type="appl" />
         <tli id="T183" time="90.946" type="appl" />
         <tli id="T184" time="91.357" type="appl" />
         <tli id="T185" time="91.767" type="appl" />
         <tli id="T186" time="92.177" type="appl" />
         <tli id="T187" time="92.588" type="appl" />
         <tli id="T188" time="93.01799772963194" />
         <tli id="T189" time="93.427" type="appl" />
         <tli id="T190" time="93.855" type="appl" />
         <tli id="T191" time="94.59066255332623" />
         <tli id="T192" time="94.753" type="appl" />
         <tli id="T193" time="95.222" type="appl" />
         <tli id="T194" time="96.23766279658783" />
         <tli id="T209" time="103.77261029677612" />
         <tli id="T210" time="104.251" type="appl" />
         <tli id="T211" time="104.694" type="appl" />
         <tli id="T212" time="105.138" type="appl" />
         <tli id="T213" time="105.581" type="appl" />
         <tli id="T214" time="106.185" type="appl" />
         <tli id="T215" time="106.788" type="appl" />
         <tli id="T216" time="107.392" type="appl" />
         <tli id="T217" time="107.995" type="appl" />
         <tli id="T218" time="108.599" type="appl" />
         <tli id="T219" time="109.202" type="appl" />
         <tli id="T220" time="109.85267210055974" />
         <tli id="T221" time="110.372" type="appl" />
         <tli id="T222" time="110.939" type="appl" />
         <tli id="T223" time="111.505" type="appl" />
         <tli id="T224" time="112.071" type="appl" />
         <tli id="T225" time="112.638" type="appl" />
         <tli id="T226" time="113.204" type="appl" />
         <tli id="T227" time="113.77" type="appl" />
         <tli id="T228" time="114.337" type="appl" />
         <tli id="T229" time="114.903" type="appl" />
         <tli id="T230" time="115.469" type="appl" />
         <tli id="T231" time="116.036" type="appl" />
         <tli id="T232" time="117.10199659082885" />
         <tli id="T233" time="117.137" type="appl" />
         <tli id="T234" time="117.672" type="appl" />
         <tli id="T235" time="118.207" type="appl" />
         <tli id="T236" time="118.743" type="appl" />
         <tli id="T237" time="119.278" type="appl" />
         <tli id="T238" time="119.813" type="appl" />
         <tli id="T239" time="120.348" type="appl" />
         <tli id="T240" time="120.869" type="appl" />
         <tli id="T241" time="121.39" type="appl" />
         <tli id="T242" time="121.911" type="appl" />
         <tli id="T243" time="122.431" type="appl" />
         <tli id="T244" time="122.952" type="appl" />
         <tli id="T245" time="123.473" type="appl" />
         <tli id="T246" time="123.96067276127681" />
         <tli id="T247" time="124.576" type="appl" />
         <tli id="T248" time="125.159" type="appl" />
         <tli id="T249" time="125.742" type="appl" />
         <tli id="T250" time="126.29066954783742" />
         <tli id="T251" time="126.773" type="appl" />
         <tli id="T252" time="127.221" type="appl" />
         <tli id="T253" time="127.67" type="appl" />
         <tli id="T254" time="128.118" type="appl" />
         <tli id="T255" time="128.567" type="appl" />
         <tli id="T256" time="129.016" type="appl" />
         <tli id="T257" time="129.464" type="appl" />
         <tli id="T258" time="129.913" type="appl" />
         <tli id="T259" time="130.362" type="appl" />
         <tli id="T260" time="130.81" type="appl" />
         <tli id="T261" time="131.259" type="appl" />
         <tli id="T262" time="131.707" type="appl" />
         <tli id="T263" time="131.98934077979646" />
         <tli id="T264" time="132.593" type="appl" />
         <tli id="T265" time="133.031" type="appl" />
         <tli id="T266" time="133.468" type="appl" />
         <tli id="T267" time="133.905" type="appl" />
         <tli id="T268" time="134.343" type="appl" />
         <tli id="T269" time="134.78" type="appl" />
         <tli id="T270" time="135.594" type="appl" />
         <tli id="T271" time="136.409" type="appl" />
         <tli id="T272" time="137.224" type="appl" />
         <tli id="T273" time="138.21132867804212" />
         <tli id="T274" time="138.642" type="appl" />
         <tli id="T275" time="139.246" type="appl" />
         <tli id="T276" time="139.85" type="appl" />
         <tli id="T277" time="140.454" type="appl" />
         <tli id="T278" time="141.058" type="appl" />
         <tli id="T279" time="141.662" type="appl" />
         <tli id="T280" time="142.266" type="appl" />
         <tli id="T281" time="142.90332723650172" />
         <tli id="T282" time="143.34" type="appl" />
         <tli id="T284" time="144.279" type="appl" />
         <tli id="T285" time="144.749" type="appl" />
         <tli id="T286" time="145.219" type="appl" />
         <tli id="T287" time="145.688" type="appl" />
         <tli id="T288" time="146.158" type="appl" />
         <tli id="T289" time="146.628" type="appl" />
         <tli id="T290" time="147.098" type="appl" />
         <tli id="T291" time="147.567" type="appl" />
         <tli id="T292" time="148.30366721375304" />
         <tli id="T293" time="148.665" type="appl" />
         <tli id="T294" time="149.293" type="appl" />
         <tli id="T295" time="149.921" type="appl" />
         <tli id="T296" time="150.548" type="appl" />
         <tli id="T297" time="151.176" type="appl" />
         <tli id="T298" time="151.804" type="appl" />
         <tli id="T299" time="152.432" type="appl" />
         <tli id="T300" time="153.1800004253593" />
         <tli id="T301" time="153.429" type="appl" />
         <tli id="T302" time="153.798" type="appl" />
         <tli id="T303" time="154.166" type="appl" />
         <tli id="T304" time="154.535" type="appl" />
         <tli id="T305" time="154.904" type="appl" />
         <tli id="T306" time="155.7329904498793" />
         <tli id="T307" time="155.751" type="appl" />
         <tli id="T308" time="156.229" type="appl" />
         <tli id="T309" time="156.707" type="appl" />
         <tli id="T310" time="157.186" type="appl" />
         <tli id="T311" time="157.664" type="appl" />
         <tli id="T312" time="158.142" type="appl" />
         <tli id="T313" time="158.9199994943663" />
         <tli id="T314" time="159.146" type="appl" />
         <tli id="T315" time="159.673" type="appl" />
         <tli id="T316" time="160.199" type="appl" />
         <tli id="T317" time="160.726" type="appl" />
         <tli id="T318" time="161.252" type="appl" />
         <tli id="T319" time="161.778" type="appl" />
         <tli id="T320" time="162.305" type="appl" />
         <tli id="T321" time="162.997666395687" />
         <tli id="T322" time="163.206" type="appl" />
         <tli id="T323" time="163.581" type="appl" />
         <tli id="T324" time="163.956" type="appl" />
         <tli id="T325" time="164.331" type="appl" />
         <tli id="T326" time="164.706" type="appl" />
         <tli id="T327" time="165.11433133530576" />
         <tli id="T328" time="165.556" type="appl" />
         <tli id="T329" time="166.032" type="appl" />
         <tli id="T330" time="166.507" type="appl" />
         <tli id="T331" time="166.983" type="appl" />
         <tli id="T332" time="167.458" type="appl" />
         <tli id="T333" time="167.934" type="appl" />
         <tli id="T334" time="168.409" type="appl" />
         <tli id="T335" time="168.885" type="appl" />
         <tli id="T336" time="169.38000473918956" />
         <tli id="T337" time="169.897" type="appl" />
         <tli id="T338" time="170.433" type="appl" />
         <tli id="T339" time="170.97" type="appl" />
         <tli id="T340" time="171.506" type="appl" />
         <tli id="T341" time="172.043" type="appl" />
         <tli id="T342" time="172.579" type="appl" />
         <tli id="T343" time="173.116" type="appl" />
         <tli id="T344" time="173.652" type="appl" />
         <tli id="T345" time="174.14899494870912" />
         <tli id="T346" time="174.619" type="appl" />
         <tli id="T347" time="175.048" type="appl" />
         <tli id="T348" time="175.478" type="appl" />
         <tli id="T349" time="175.908" type="appl" />
         <tli id="T350" time="176.338" type="appl" />
         <tli id="T351" time="176.767" type="appl" />
         <tli id="T352" time="176.99032931835404" />
         <tli id="T353" time="177.517" type="appl" />
         <tli id="T354" time="177.836" type="appl" />
         <tli id="T356" time="178.60932845463032" />
         <tli id="T357" time="178.949" type="appl" />
         <tli id="T358" time="179.421" type="appl" />
         <tli id="T359" time="179.894" type="appl" />
         <tli id="T360" time="180.367" type="appl" />
         <tli id="T361" time="180.84" type="appl" />
         <tli id="T362" time="181.312" type="appl" />
         <tli id="T363" time="181.785" type="appl" />
         <tli id="T364" time="182.258" type="appl" />
         <tli id="T365" time="182.73" type="appl" />
         <tli id="T366" time="183.203" type="appl" />
         <tli id="T367" time="183.676" type="appl" />
         <tli id="T368" time="184.149" type="appl" />
         <tli id="T369" time="184.621" type="appl" />
         <tli id="T370" time="185.14066315583275" />
         <tli id="T371" time="185.658" type="appl" />
         <tli id="T372" time="186.223" type="appl" />
         <tli id="T373" time="186.787" type="appl" />
         <tli id="T374" time="187.351" type="appl" />
         <tli id="T375" time="187.916" type="appl" />
         <tli id="T376" time="188.33333622335567" />
         <tli id="T377" time="188.921" type="appl" />
         <tli id="T378" time="189.363" type="appl" />
         <tli id="T379" time="189.804" type="appl" />
         <tli id="T380" time="190.245" type="appl" />
         <tli id="T381" time="190.686" type="appl" />
         <tli id="T382" time="191.128" type="appl" />
         <tli id="T383" time="191.569" type="appl" />
         <tli id="T384" time="192.01" type="appl" />
         <tli id="T385" time="192.452" type="appl" />
         <tli id="T386" time="192.893" type="appl" />
         <tli id="T387" time="193.334" type="appl" />
         <tli id="T388" time="193.775" type="appl" />
         <tli id="T389" time="194.217" type="appl" />
         <tli id="T390" time="194.658" type="appl" />
         <tli id="T391" time="195.091" type="appl" />
         <tli id="T392" time="195.524" type="appl" />
         <tli id="T393" time="195.957" type="appl" />
         <tli id="T394" time="196.39" type="appl" />
         <tli id="T395" time="196.824" type="appl" />
         <tli id="T396" time="197.257" type="appl" />
         <tli id="T397" time="197.69" type="appl" />
         <tli id="T398" time="198.123" type="appl" />
         <tli id="T399" time="198.556" type="appl" />
         <tli id="T400" time="198.989" type="appl" />
         <tli id="T401" time="199.422" type="appl" />
         <tli id="T402" time="199.855" type="appl" />
         <tli id="T403" time="200.288" type="appl" />
         <tli id="T404" time="200.722" type="appl" />
         <tli id="T405" time="201.155" type="appl" />
         <tli id="T406" time="201.588" type="appl" />
         <tli id="T407" time="202.021" type="appl" />
         <tli id="T408" time="202.46733930706728" />
         <tli id="T409" time="202.95" type="appl" />
         <tli id="T410" time="203.445" type="appl" />
         <tli id="T411" time="203.941" type="appl" />
         <tli id="T412" time="204.3426647823875" />
         <tli id="T413" time="204.864" type="appl" />
         <tli id="T414" time="205.292" type="appl" />
         <tli id="T415" time="205.72" type="appl" />
         <tli id="T416" time="206.148" type="appl" />
         <tli id="T417" time="206.576" type="appl" />
         <tli id="T418" time="207.004" type="appl" />
         <tli id="T419" time="207.432" type="appl" />
         <tli id="T420" time="207.86" type="appl" />
         <tli id="T421" time="208.235" type="appl" />
         <tli id="T422" time="208.609" type="appl" />
         <tli id="T423" time="208.984" type="appl" />
         <tli id="T424" time="209.358" type="appl" />
         <tli id="T425" time="209.733" type="appl" />
         <tli id="T426" time="210.107" type="appl" />
         <tli id="T427" time="210.482" type="appl" />
         <tli id="T428" time="210.856" type="appl" />
         <tli id="T429" time="211.231" type="appl" />
         <tli id="T430" time="211.605" type="appl" />
         <tli id="T431" time="211.98" type="appl" />
         <tli id="T432" time="212.561" type="appl" />
         <tli id="T433" time="213.142" type="appl" />
         <tli id="T434" time="213.723" type="appl" />
         <tli id="T435" time="214.303" type="appl" />
         <tli id="T436" time="214.884" type="appl" />
         <tli id="T437" time="215.465" type="appl" />
         <tli id="T438" time="216.21266020309204" />
         <tli id="T439" time="216.605" type="appl" />
         <tli id="T440" time="217.163" type="appl" />
         <tli id="T441" time="217.722" type="appl" />
         <tli id="T442" time="218.281" type="appl" />
         <tli id="T443" time="218.839" type="appl" />
         <tli id="T444" time="219.398" type="appl" />
         <tli id="T445" time="219.956" type="appl" />
         <tli id="T446" time="220.515" type="appl" />
         <tli id="T447" time="221.074" type="appl" />
         <tli id="T448" time="221.632" type="appl" />
         <tli id="T449" time="222.191" type="appl" />
         <tli id="T450" time="222.731" type="appl" />
         <tli id="T451" time="223.271" type="appl" />
         <tli id="T452" time="223.811" type="appl" />
         <tli id="T453" time="224.35" type="appl" />
         <tli id="T454" time="224.89" type="appl" />
         <tli id="T455" time="225.43" type="appl" />
         <tli id="T456" time="225.97" type="appl" />
         <tli id="T457" time="226.55667667273477" />
         <tli id="T458" time="227.15" type="appl" />
         <tli id="T459" time="227.789" type="appl" />
         <tli id="T460" time="228.429" type="appl" />
         <tli id="T461" time="229.069" type="appl" />
         <tli id="T462" time="229.709" type="appl" />
         <tli id="T463" time="230.348" type="appl" />
         <tli id="T464" time="230.988" type="appl" />
         <tli id="T465" time="231.628" type="appl" />
         <tli id="T466" time="232.267" type="appl" />
         <tli id="T467" time="232.907" type="appl" />
         <tli id="T468" time="233.368" type="appl" />
         <tli id="T469" time="233.83123368208854" />
         <tli id="T470" time="234.289" type="appl" />
         <tli id="T471" time="234.749" type="appl" />
         <tli id="T472" time="235.2969996790287" />
         <tli id="T473" time="235.498" type="appl" />
         <tli id="T474" time="235.786" type="appl" />
         <tli id="T475" time="236.074" type="appl" />
         <tli id="T476" time="236.362" type="appl" />
         <tli id="T477" time="236.87834954805663" />
         <tli id="T478" time="237.189" type="appl" />
         <tli id="T479" time="237.583" type="appl" />
         <tli id="T480" time="237.976" type="appl" />
         <tli id="T481" time="238.22500683187707" />
         <tli id="T482" time="238.58" type="appl" />
         <tli id="T484" time="239.224999864417" />
         <tli id="T485" time="239.377" type="appl" />
         <tli id="T486" time="239.733" type="appl" />
         <tli id="T487" time="240.09" type="appl" />
         <tli id="T488" time="240.447" type="appl" />
         <tli id="T489" time="240.803" type="appl" />
         <tli id="T490" time="241.16" type="appl" />
         <tli id="T491" time="241.517" type="appl" />
         <tli id="T492" time="241.873" type="appl" />
         <tli id="T493" time="242.23" type="appl" />
         <tli id="T494" time="242.587" type="appl" />
         <tli id="T495" time="242.943" type="appl" />
         <tli id="T496" time="243.3" type="appl" />
         <tli id="T497" time="243.741" type="appl" />
         <tli id="T498" time="244.183" type="appl" />
         <tli id="T499" time="244.624" type="appl" />
         <tli id="T500" time="245.066" type="appl" />
         <tli id="T501" time="245.507" type="appl" />
         <tli id="T502" time="245.949" type="appl" />
         <tli id="T503" time="246.39" type="appl" />
         <tli id="T504" time="246.831" type="appl" />
         <tli id="T505" time="247.273" type="appl" />
         <tli id="T506" time="247.714" type="appl" />
         <tli id="T507" time="248.156" type="appl" />
         <tli id="T508" time="249.144930747213" />
         <tli id="T510" time="249.48" type="appl" />
         <tli id="T511" time="250.0649243371497" />
         <tli id="T512" time="251.294" type="appl" />
         <tli id="T513" time="251.938" type="appl" />
         <tli id="T514" time="252.582" type="appl" />
         <tli id="T515" time="253.226" type="appl" />
         <tli id="T516" time="254.09666707928076" />
         <tli id="T517" time="254.343" type="appl" />
         <tli id="T518" time="254.817" type="appl" />
         <tli id="T519" time="255.15665969377307" />
         <tli id="T520" time="255.894" type="appl" />
         <tli id="T521" time="256.497" type="appl" />
         <tli id="T522" time="257.1" type="appl" />
         <tli id="T523" time="257.704" type="appl" />
         <tli id="T524" time="258.308" type="appl" />
         <tli id="T525" time="258.911" type="appl" />
         <tli id="T526" time="259.514" type="appl" />
         <tli id="T527" time="260.118" type="appl" />
         <tli id="T528" time="260.509" type="appl" />
         <tli id="T529" time="260.9" type="appl" />
         <tli id="T530" time="261.291" type="appl" />
         <tli id="T531" time="261.682" type="appl" />
         <tli id="T532" time="262.072" type="appl" />
         <tli id="T533" time="262.463" type="appl" />
         <tli id="T534" time="262.854" type="appl" />
         <tli id="T535" time="263.245" type="appl" />
         <tli id="T536" time="263.8026671609748" />
         <tli id="T537" time="264.088" type="appl" />
         <tli id="T538" time="264.54" type="appl" />
         <tli id="T539" time="264.993" type="appl" />
         <tli id="T540" time="265.445" type="appl" />
         <tli id="T541" time="265.897" type="appl" />
         <tli id="T542" time="266.349" type="appl" />
         <tli id="T543" time="266.801" type="appl" />
         <tli id="T544" time="267.253" type="appl" />
         <tli id="T545" time="267.706" type="appl" />
         <tli id="T546" time="268.158" type="appl" />
         <tli id="T547" time="268.9100013672463" />
         <tli id="T548" time="268.976" type="appl" />
         <tli id="T549" time="269.342" type="appl" />
         <tli id="T550" time="269.708" type="appl" />
         <tli id="T551" time="270.074" type="appl" />
         <tli id="T552" time="270.441" type="appl" />
         <tli id="T553" time="270.807" type="appl" />
         <tli id="T554" time="271.173" type="appl" />
         <tli id="T555" time="271.539" type="appl" />
         <tli id="T556" time="271.905" type="appl" />
         <tli id="T557" time="272.318" type="appl" />
         <tli id="T558" time="272.731" type="appl" />
         <tli id="T560" time="273.556" type="appl" />
         <tli id="T561" time="273.969" type="appl" />
         <tli id="T562" time="274.382" type="appl" />
         <tli id="T563" time="274.795" type="appl" />
         <tli id="T564" time="275.208" type="appl" />
         <tli id="T565" time="275.621" type="appl" />
         <tli id="T566" time="276.034" type="appl" />
         <tli id="T567" time="276.446" type="appl" />
         <tli id="T568" time="276.859" type="appl" />
         <tli id="T569" time="277.272" type="appl" />
         <tli id="T570" time="277.685" type="appl" />
         <tli id="T571" time="278.098" type="appl" />
         <tli id="T572" time="278.511" type="appl" />
         <tli id="T573" time="278.923" type="appl" />
         <tli id="T574" time="279.336" type="appl" />
         <tli id="T575" time="279.749" type="appl" />
         <tli id="T576" time="280.1820061627891" />
         <tli id="T577" time="280.657" type="appl" />
         <tli id="T578" time="281.152" type="appl" />
         <tli id="T579" time="281.646" type="appl" />
         <tli id="T580" time="282.141" type="appl" />
         <tli id="T581" time="282.636" type="appl" />
         <tli id="T582" time="283.131" type="appl" />
         <tli id="T583" time="283.626" type="appl" />
         <tli id="T584" time="284.12" type="appl" />
         <tli id="T585" time="284.615" type="appl" />
         <tli id="T586" time="285.11" type="appl" />
         <tli id="T587" time="285.605" type="appl" />
         <tli id="T588" time="286.099" type="appl" />
         <tli id="T589" time="286.594" type="appl" />
         <tli id="T590" time="287.55565270350985" />
         <tli id="T591" time="287.665" type="appl" />
         <tli id="T592" time="288.241" type="appl" />
         <tli id="T593" time="288.818" type="appl" />
         <tli id="T594" time="289.394" type="appl" />
         <tli id="T595" time="289.97" type="appl" />
         <tli id="T596" time="290.546" type="appl" />
         <tli id="T597" time="291.123" type="appl" />
         <tli id="T598" time="291.699" type="appl" />
         <tli id="T599" time="292.275" type="appl" />
         <tli id="T600" time="292.851" type="appl" />
         <tli id="T601" time="293.428" type="appl" />
         <tli id="T602" time="294.004" type="appl" />
         <tli id="T603" time="294.6200045242443" />
         <tli id="T604" time="295.056" type="appl" />
         <tli id="T605" time="295.532" type="appl" />
         <tli id="T606" time="296.009" type="appl" />
         <tli id="T607" time="296.485" type="appl" />
         <tli id="T608" time="296.961" type="appl" />
         <tli id="T609" time="297.438" type="appl" />
         <tli id="T610" time="297.914" type="appl" />
         <tli id="T611" time="298.56333642404525" />
         <tli id="T612" time="298.882" type="appl" />
         <tli id="T613" time="299.375" type="appl" />
         <tli id="T614" time="299.867" type="appl" />
         <tli id="T615" time="300.36" type="appl" />
         <tli id="T616" time="300.852" type="appl" />
         <tli id="T617" time="301.344" type="appl" />
         <tli id="T618" time="301.837" type="appl" />
         <tli id="T619" time="302.329" type="appl" />
         <tli id="T620" time="302.822" type="appl" />
         <tli id="T621" time="303.314" type="appl" />
         <tli id="T622" time="303.807" type="appl" />
         <tli id="T623" time="304.6978770149141" />
         <tli id="T624" time="304.763" type="appl" />
         <tli id="T625" time="305.227" type="appl" />
         <tli id="T626" time="305.69" type="appl" />
         <tli id="T627" time="306.154" type="appl" />
         <tli id="T628" time="306.618" type="appl" />
         <tli id="T630" time="307.546" type="appl" />
         <tli id="T631" time="308.01" type="appl" />
         <tli id="T632" time="308.473" type="appl" />
         <tli id="T633" time="308.937" type="appl" />
         <tli id="T634" time="309.401" type="appl" />
         <tli id="T635" time="309.865" type="appl" />
         <tli id="T636" time="310.329" type="appl" />
         <tli id="T637" time="310.792" type="appl" />
         <tli id="T638" time="311.256" type="appl" />
         <tli id="T639" time="311.72" type="appl" />
         <tli id="T640" time="312.317" type="appl" />
         <tli id="T641" time="312.914" type="appl" />
         <tli id="T642" time="313.51" type="appl" />
         <tli id="T643" time="314.107" type="appl" />
         <tli id="T644" time="314.512" type="appl" />
         <tli id="T645" time="314.916" type="appl" />
         <tli id="T646" time="315.321" type="appl" />
         <tli id="T647" time="315.725" type="appl" />
         <tli id="T648" time="315.99779828261524" />
         <tli id="T649" time="316.513" type="appl" />
         <tli id="T650" time="316.897" type="appl" />
         <tli id="T651" time="317.10667076487" />
         <tli id="T652" time="317.858" type="appl" />
         <tli id="T653" time="318.457" type="appl" />
         <tli id="T654" time="319.055" type="appl" />
         <tli id="T655" time="319.654" type="appl" />
         <tli id="T656" time="320.252" type="appl" />
         <tli id="T657" time="320.851" type="appl" />
         <tli id="T658" time="321.449" type="appl" />
         <tli id="T659" time="322.048" type="appl" />
         <tli id="T660" time="322.646" type="appl" />
         <tli id="T661" time="323.245" type="appl" />
         <tli id="T662" time="323.843" type="appl" />
         <tli id="T663" time="324.442" type="appl" />
         <tli id="T664" time="325.04" type="appl" />
         <tli id="T665" time="325.479" type="appl" />
         <tli id="T666" time="325.918" type="appl" />
         <tli id="T667" time="326.357" type="appl" />
         <tli id="T668" time="326.796" type="appl" />
         <tli id="T669" time="327.234" type="appl" />
         <tli id="T670" time="327.673" type="appl" />
         <tli id="T671" time="328.112" type="appl" />
         <tli id="T672" time="328.78432377572193" />
         <tli id="T3" time="329.1610138594639" type="intp" />
         <tli id="T673" time="329.53770394320577" />
         <tli id="T675" time="331.75102185522746" />
         <tli id="T680" time="331.897" />
      </common-timeline>
      <segmented-tier category="tx"
                      display-name="tx-KiPP"
                      id="tx-KiPP"
                      speaker="KiPP"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KiPP">
            <ts e="T194" id="Seg_0" n="sc" s="T0">
               <ts e="T2" id="Seg_2" n="HIAT:u" s="T0">
                  <ts e="T1" id="Seg_4" n="HIAT:w" s="T0">Bi͡egičebi</ts>
                  <nts id="Seg_5" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T2" id="Seg_7" n="HIAT:w" s="T1">illeller</ts>
                  <nts id="Seg_8" n="HIAT:ip">.</nts>
                  <nts id="Seg_9" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T9" id="Seg_11" n="HIAT:u" s="T2">
                  <nts id="Seg_12" n="HIAT:ip">(</nts>
                  <ts e="T4" id="Seg_14" n="HIAT:w" s="T2">Hit-</ts>
                  <nts id="Seg_15" n="HIAT:ip">)</nts>
                  <nts id="Seg_16" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T5" id="Seg_18" n="HIAT:w" s="T4">hirinen</ts>
                  <nts id="Seg_19" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T6" id="Seg_21" n="HIAT:w" s="T5">hɨldʼar</ts>
                  <nts id="Seg_22" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T7" id="Seg_24" n="HIAT:w" s="T6">ete</ts>
                  <nts id="Seg_25" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T8" id="Seg_27" n="HIAT:w" s="T7">urut</ts>
                  <nts id="Seg_28" n="HIAT:ip">,</nts>
                  <nts id="Seg_29" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T9" id="Seg_31" n="HIAT:w" s="T8">espʼidʼisʼijalar</ts>
                  <nts id="Seg_32" n="HIAT:ip">.</nts>
                  <nts id="Seg_33" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T12" id="Seg_35" n="HIAT:u" s="T9">
                  <ts e="T10" id="Seg_37" n="HIAT:w" s="T9">Onu</ts>
                  <nts id="Seg_38" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T11" id="Seg_40" n="HIAT:w" s="T10">ehem</ts>
                  <nts id="Seg_41" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T12" id="Seg_43" n="HIAT:w" s="T11">kepseːčči</ts>
                  <nts id="Seg_44" n="HIAT:ip">.</nts>
                  <nts id="Seg_45" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T23" id="Seg_47" n="HIAT:u" s="T12">
                  <ts e="T13" id="Seg_49" n="HIAT:w" s="T12">Mini͡ene</ts>
                  <nts id="Seg_50" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T14" id="Seg_52" n="HIAT:w" s="T13">inʼem</ts>
                  <nts id="Seg_53" n="HIAT:ip">,</nts>
                  <nts id="Seg_54" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_55" n="HIAT:ip">(</nts>
                  <ts e="T15" id="Seg_57" n="HIAT:w" s="T14">ehe</ts>
                  <nts id="Seg_58" n="HIAT:ip">)</nts>
                  <nts id="Seg_59" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T16" id="Seg_61" n="HIAT:w" s="T15">iti</ts>
                  <nts id="Seg_62" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T17" id="Seg_64" n="HIAT:w" s="T16">inʼem</ts>
                  <nts id="Seg_65" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T18" id="Seg_67" n="HIAT:w" s="T17">uruːlara</ts>
                  <nts id="Seg_68" n="HIAT:ip">,</nts>
                  <nts id="Seg_69" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T19" id="Seg_71" n="HIAT:w" s="T18">Markʼel</ts>
                  <nts id="Seg_72" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T20" id="Seg_74" n="HIAT:w" s="T19">ogonnʼor</ts>
                  <nts id="Seg_75" n="HIAT:ip">,</nts>
                  <nts id="Seg_76" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T21" id="Seg_78" n="HIAT:w" s="T20">Ölöːnö</ts>
                  <nts id="Seg_79" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T22" id="Seg_81" n="HIAT:w" s="T21">agata</ts>
                  <nts id="Seg_82" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T23" id="Seg_84" n="HIAT:w" s="T22">ogonnʼor</ts>
                  <nts id="Seg_85" n="HIAT:ip">.</nts>
                  <nts id="Seg_86" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T26" id="Seg_88" n="HIAT:u" s="T23">
                  <ts e="T24" id="Seg_90" n="HIAT:w" s="T23">Olor</ts>
                  <nts id="Seg_91" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T25" id="Seg_93" n="HIAT:w" s="T24">kepsetellerin</ts>
                  <nts id="Seg_94" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T26" id="Seg_96" n="HIAT:w" s="T25">isteːččibin</ts>
                  <nts id="Seg_97" n="HIAT:ip">.</nts>
                  <nts id="Seg_98" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T35" id="Seg_100" n="HIAT:u" s="T26">
                  <ts e="T27" id="Seg_102" n="HIAT:w" s="T26">Bu</ts>
                  <nts id="Seg_103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T28" id="Seg_105" n="HIAT:w" s="T27">Bi͡egičep</ts>
                  <nts id="Seg_106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T29" id="Seg_108" n="HIAT:w" s="T28">bu͡ollagɨna</ts>
                  <nts id="Seg_109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T30" id="Seg_111" n="HIAT:w" s="T29">tabannan</ts>
                  <nts id="Seg_112" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T31" id="Seg_114" n="HIAT:w" s="T30">ajannɨːr</ts>
                  <nts id="Seg_115" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T32" id="Seg_117" n="HIAT:w" s="T31">Bi͡egičep</ts>
                  <nts id="Seg_118" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T33" id="Seg_120" n="HIAT:w" s="T32">arɨːtɨgar</ts>
                  <nts id="Seg_121" n="HIAT:ip">,</nts>
                  <nts id="Seg_122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T34" id="Seg_124" n="HIAT:w" s="T33">hette</ts>
                  <nts id="Seg_125" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T35" id="Seg_127" n="HIAT:w" s="T34">kihileːk</ts>
                  <nts id="Seg_128" n="HIAT:ip">.</nts>
                  <nts id="Seg_129" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T48" id="Seg_131" n="HIAT:u" s="T35">
                  <ts e="T36" id="Seg_133" n="HIAT:w" s="T35">Bu</ts>
                  <nts id="Seg_134" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T37" id="Seg_136" n="HIAT:w" s="T36">kihi</ts>
                  <nts id="Seg_137" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T38" id="Seg_139" n="HIAT:w" s="T37">ajannaːn</ts>
                  <nts id="Seg_140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T39" id="Seg_142" n="HIAT:w" s="T38">ihen</ts>
                  <nts id="Seg_143" n="HIAT:ip">,</nts>
                  <nts id="Seg_144" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T40" id="Seg_146" n="HIAT:w" s="T39">tabannan</ts>
                  <nts id="Seg_147" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T41" id="Seg_149" n="HIAT:w" s="T40">illeller</ts>
                  <nts id="Seg_150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T42" id="Seg_152" n="HIAT:w" s="T41">bu͡o</ts>
                  <nts id="Seg_153" n="HIAT:ip">,</nts>
                  <nts id="Seg_154" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T43" id="Seg_156" n="HIAT:w" s="T42">atɨntan-atɨn</ts>
                  <nts id="Seg_157" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T44" id="Seg_159" n="HIAT:w" s="T43">tabannan</ts>
                  <nts id="Seg_160" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T45" id="Seg_162" n="HIAT:w" s="T44">illeller</ts>
                  <nts id="Seg_163" n="HIAT:ip">,</nts>
                  <nts id="Seg_164" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T46" id="Seg_166" n="HIAT:w" s="T45">haŋattan-haŋa</ts>
                  <nts id="Seg_167" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T47" id="Seg_169" n="HIAT:w" s="T46">tabannan</ts>
                  <nts id="Seg_170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T48" id="Seg_172" n="HIAT:w" s="T47">illeller</ts>
                  <nts id="Seg_173" n="HIAT:ip">.</nts>
                  <nts id="Seg_174" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T57" id="Seg_176" n="HIAT:u" s="T48">
                  <ts e="T49" id="Seg_178" n="HIAT:w" s="T48">Onno</ts>
                  <nts id="Seg_179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T50" id="Seg_181" n="HIAT:w" s="T49">Čagɨdaj</ts>
                  <nts id="Seg_182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T51" id="Seg_184" n="HIAT:w" s="T50">ogonnʼoru</ts>
                  <nts id="Seg_185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T52" id="Seg_187" n="HIAT:w" s="T51">iltereller</ts>
                  <nts id="Seg_188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T53" id="Seg_190" n="HIAT:w" s="T52">bu͡olla</ts>
                  <nts id="Seg_191" n="HIAT:ip">,</nts>
                  <nts id="Seg_192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T54" id="Seg_194" n="HIAT:w" s="T53">min</ts>
                  <nts id="Seg_195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T55" id="Seg_197" n="HIAT:w" s="T54">ogonnʼorum</ts>
                  <nts id="Seg_198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T56" id="Seg_200" n="HIAT:w" s="T55">ehetineːk</ts>
                  <nts id="Seg_201" n="HIAT:ip">,</nts>
                  <nts id="Seg_202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T57" id="Seg_204" n="HIAT:w" s="T56">ehelere</ts>
                  <nts id="Seg_205" n="HIAT:ip">.</nts>
                  <nts id="Seg_206" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T67" id="Seg_208" n="HIAT:u" s="T57">
                  <ts e="T58" id="Seg_210" n="HIAT:w" s="T57">Ol</ts>
                  <nts id="Seg_211" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T59" id="Seg_213" n="HIAT:w" s="T58">ogonnʼor</ts>
                  <nts id="Seg_214" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T60" id="Seg_216" n="HIAT:w" s="T59">tu͡ok</ts>
                  <nts id="Seg_217" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T61" id="Seg_219" n="HIAT:w" s="T60">daː</ts>
                  <nts id="Seg_220" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T62" id="Seg_222" n="HIAT:w" s="T61">hippetek</ts>
                  <nts id="Seg_223" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T63" id="Seg_225" n="HIAT:w" s="T62">kihite</ts>
                  <nts id="Seg_226" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T64" id="Seg_228" n="HIAT:w" s="T63">ete</ts>
                  <nts id="Seg_229" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_230" n="HIAT:ip">–</nts>
                  <nts id="Seg_231" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T65" id="Seg_233" n="HIAT:w" s="T64">hürdeːk</ts>
                  <nts id="Seg_234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T66" id="Seg_236" n="HIAT:w" s="T65">kamnastaːk</ts>
                  <nts id="Seg_237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T67" id="Seg_239" n="HIAT:w" s="T66">kihi</ts>
                  <nts id="Seg_240" n="HIAT:ip">.</nts>
                  <nts id="Seg_241" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T77" id="Seg_243" n="HIAT:u" s="T67">
                  <ts e="T68" id="Seg_245" n="HIAT:w" s="T67">Ol</ts>
                  <nts id="Seg_246" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_247" n="HIAT:ip">(</nts>
                  <ts e="T69" id="Seg_249" n="HIAT:w" s="T68">ogonnʼor-</ts>
                  <nts id="Seg_250" n="HIAT:ip">)</nts>
                  <nts id="Seg_251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T70" id="Seg_253" n="HIAT:w" s="T69">hataːbat</ts>
                  <nts id="Seg_254" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T71" id="Seg_256" n="HIAT:w" s="T70">tabalarɨ</ts>
                  <nts id="Seg_257" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T72" id="Seg_259" n="HIAT:w" s="T71">kölütteren</ts>
                  <nts id="Seg_260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T73" id="Seg_262" n="HIAT:w" s="T72">baraːn</ts>
                  <nts id="Seg_263" n="HIAT:ip">,</nts>
                  <nts id="Seg_264" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T74" id="Seg_266" n="HIAT:w" s="T73">Bi͡egičep</ts>
                  <nts id="Seg_267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T75" id="Seg_269" n="HIAT:w" s="T74">arɨːtɨgar</ts>
                  <nts id="Seg_270" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T76" id="Seg_272" n="HIAT:w" s="T75">iltereller</ts>
                  <nts id="Seg_273" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T77" id="Seg_275" n="HIAT:w" s="T76">bu͡o</ts>
                  <nts id="Seg_276" n="HIAT:ip">.</nts>
                  <nts id="Seg_277" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T83" id="Seg_279" n="HIAT:u" s="T77">
                  <ts e="T78" id="Seg_281" n="HIAT:w" s="T77">Onno</ts>
                  <nts id="Seg_282" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T79" id="Seg_284" n="HIAT:w" s="T78">tabalar</ts>
                  <nts id="Seg_285" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T80" id="Seg_287" n="HIAT:w" s="T79">möŋöllör</ts>
                  <nts id="Seg_288" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T81" id="Seg_290" n="HIAT:w" s="T80">ühü</ts>
                  <nts id="Seg_291" n="HIAT:ip">,</nts>
                  <nts id="Seg_292" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T82" id="Seg_294" n="HIAT:w" s="T81">i͡e</ts>
                  <nts id="Seg_295" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T83" id="Seg_297" n="HIAT:w" s="T82">diː</ts>
                  <nts id="Seg_298" n="HIAT:ip">.</nts>
                  <nts id="Seg_299" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T96" id="Seg_301" n="HIAT:u" s="T83">
                  <ts e="T84" id="Seg_303" n="HIAT:w" s="T83">Tabalar</ts>
                  <nts id="Seg_304" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T85" id="Seg_306" n="HIAT:w" s="T84">möŋöllör</ts>
                  <nts id="Seg_307" n="HIAT:ip">,</nts>
                  <nts id="Seg_308" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T86" id="Seg_310" n="HIAT:w" s="T85">bu</ts>
                  <nts id="Seg_311" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T87" id="Seg_313" n="HIAT:w" s="T86">kihi</ts>
                  <nts id="Seg_314" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T88" id="Seg_316" n="HIAT:w" s="T87">bergehetin</ts>
                  <nts id="Seg_317" n="HIAT:ip">,</nts>
                  <nts id="Seg_318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T89" id="Seg_320" n="HIAT:w" s="T88">ol</ts>
                  <nts id="Seg_321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T90" id="Seg_323" n="HIAT:w" s="T89">Bi͡egičep</ts>
                  <nts id="Seg_324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T91" id="Seg_326" n="HIAT:w" s="T90">gi͡enin</ts>
                  <nts id="Seg_327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T92" id="Seg_329" n="HIAT:w" s="T91">bergehetin</ts>
                  <nts id="Seg_330" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T93" id="Seg_332" n="HIAT:w" s="T92">taba</ts>
                  <nts id="Seg_333" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T94" id="Seg_335" n="HIAT:w" s="T93">iːlen</ts>
                  <nts id="Seg_336" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T95" id="Seg_338" n="HIAT:w" s="T94">bɨragar</ts>
                  <nts id="Seg_339" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T96" id="Seg_341" n="HIAT:w" s="T95">ühü</ts>
                  <nts id="Seg_342" n="HIAT:ip">.</nts>
                  <nts id="Seg_343" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T114" id="Seg_345" n="HIAT:u" s="T96">
                  <ts e="T97" id="Seg_347" n="HIAT:w" s="T96">Bu</ts>
                  <nts id="Seg_348" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T98" id="Seg_350" n="HIAT:w" s="T97">hɨrganɨ</ts>
                  <nts id="Seg_351" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T99" id="Seg_353" n="HIAT:w" s="T98">kurdarɨ</ts>
                  <nts id="Seg_354" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T100" id="Seg_356" n="HIAT:w" s="T99">bergehe</ts>
                  <nts id="Seg_357" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T101" id="Seg_359" n="HIAT:w" s="T100">tühen</ts>
                  <nts id="Seg_360" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T102" id="Seg_362" n="HIAT:w" s="T101">erdegine</ts>
                  <nts id="Seg_363" n="HIAT:ip">,</nts>
                  <nts id="Seg_364" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T103" id="Seg_366" n="HIAT:w" s="T102">onu</ts>
                  <nts id="Seg_367" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T104" id="Seg_369" n="HIAT:w" s="T103">ogonnʼor</ts>
                  <nts id="Seg_370" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T105" id="Seg_372" n="HIAT:w" s="T104">hɨrganɨ</ts>
                  <nts id="Seg_373" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T106" id="Seg_375" n="HIAT:w" s="T105">kurdarɨ</ts>
                  <nts id="Seg_376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T107" id="Seg_378" n="HIAT:w" s="T106">ojon</ts>
                  <nts id="Seg_379" n="HIAT:ip">,</nts>
                  <nts id="Seg_380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T108" id="Seg_382" n="HIAT:w" s="T107">kaban</ts>
                  <nts id="Seg_383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T109" id="Seg_385" n="HIAT:w" s="T108">ɨlbɨt</ts>
                  <nts id="Seg_386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T110" id="Seg_388" n="HIAT:w" s="T109">bu͡o</ts>
                  <nts id="Seg_389" n="HIAT:ip">,</nts>
                  <nts id="Seg_390" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T111" id="Seg_392" n="HIAT:w" s="T110">hirge</ts>
                  <nts id="Seg_393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T112" id="Seg_395" n="HIAT:w" s="T111">tühü͡ögün</ts>
                  <nts id="Seg_396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T113" id="Seg_398" n="HIAT:w" s="T112">betereː</ts>
                  <nts id="Seg_399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T114" id="Seg_401" n="HIAT:w" s="T113">öttüger</ts>
                  <nts id="Seg_402" n="HIAT:ip">.</nts>
                  <nts id="Seg_403" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T121" id="Seg_405" n="HIAT:u" s="T114">
                  <ts e="T115" id="Seg_407" n="HIAT:w" s="T114">Onu</ts>
                  <nts id="Seg_408" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T116" id="Seg_410" n="HIAT:w" s="T115">ol</ts>
                  <nts id="Seg_411" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_412" n="HIAT:ip">(</nts>
                  <ts e="T118" id="Seg_414" n="HIAT:w" s="T116">ogonnʼ-</ts>
                  <nts id="Seg_415" n="HIAT:ip">)</nts>
                  <nts id="Seg_416" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T119" id="Seg_418" n="HIAT:w" s="T118">Bi͡egičebe</ts>
                  <nts id="Seg_419" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T120" id="Seg_421" n="HIAT:w" s="T119">diːr</ts>
                  <nts id="Seg_422" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T121" id="Seg_424" n="HIAT:w" s="T120">ühü</ts>
                  <nts id="Seg_425" n="HIAT:ip">:</nts>
                  <nts id="Seg_426" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T130" id="Seg_428" n="HIAT:u" s="T121">
                  <nts id="Seg_429" n="HIAT:ip">"</nts>
                  <ts e="T122" id="Seg_431" n="HIAT:w" s="T121">Oː</ts>
                  <nts id="Seg_432" n="HIAT:ip">,</nts>
                  <nts id="Seg_433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T123" id="Seg_435" n="HIAT:w" s="T122">haka</ts>
                  <nts id="Seg_436" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T124" id="Seg_438" n="HIAT:w" s="T123">hiriger</ts>
                  <nts id="Seg_439" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T125" id="Seg_441" n="HIAT:w" s="T124">da</ts>
                  <nts id="Seg_442" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T126" id="Seg_444" n="HIAT:w" s="T125">kamnastaːk</ts>
                  <nts id="Seg_445" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T127" id="Seg_447" n="HIAT:w" s="T126">kihi</ts>
                  <nts id="Seg_448" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T128" id="Seg_450" n="HIAT:w" s="T127">baːr</ts>
                  <nts id="Seg_451" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T129" id="Seg_453" n="HIAT:w" s="T128">bu͡olar</ts>
                  <nts id="Seg_454" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_455" n="HIAT:ip">(</nts>
                  <nts id="Seg_456" n="HIAT:ip">(</nts>
                  <ats e="T130" id="Seg_457" n="HIAT:non-pho" s="T129">…</ats>
                  <nts id="Seg_458" n="HIAT:ip">)</nts>
                  <nts id="Seg_459" n="HIAT:ip">)</nts>
                  <nts id="Seg_460" n="HIAT:ip">.</nts>
                  <nts id="Seg_461" n="HIAT:ip">"</nts>
                  <nts id="Seg_462" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T142" id="Seg_464" n="HIAT:u" s="T130">
                  <ts e="T131" id="Seg_466" n="HIAT:w" s="T130">Dʼe</ts>
                  <nts id="Seg_467" n="HIAT:ip">,</nts>
                  <nts id="Seg_468" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T132" id="Seg_470" n="HIAT:w" s="T131">bu</ts>
                  <nts id="Seg_471" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T133" id="Seg_473" n="HIAT:w" s="T132">ogonnʼor</ts>
                  <nts id="Seg_474" n="HIAT:ip">,</nts>
                  <nts id="Seg_475" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T134" id="Seg_477" n="HIAT:w" s="T133">Bi͡egičep</ts>
                  <nts id="Seg_478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T135" id="Seg_480" n="HIAT:w" s="T134">bu͡ollagɨna</ts>
                  <nts id="Seg_481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T136" id="Seg_483" n="HIAT:w" s="T135">ol</ts>
                  <nts id="Seg_484" n="HIAT:ip">,</nts>
                  <nts id="Seg_485" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_486" n="HIAT:ip">"</nts>
                  <ts e="T137" id="Seg_488" n="HIAT:w" s="T136">ol</ts>
                  <nts id="Seg_489" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T138" id="Seg_491" n="HIAT:w" s="T137">arɨːga</ts>
                  <nts id="Seg_492" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T139" id="Seg_494" n="HIAT:w" s="T138">tiːjbitim</ts>
                  <nts id="Seg_495" n="HIAT:ip">"</nts>
                  <nts id="Seg_496" n="HIAT:ip">,</nts>
                  <nts id="Seg_497" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T140" id="Seg_499" n="HIAT:w" s="T139">di͡en</ts>
                  <nts id="Seg_500" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T141" id="Seg_502" n="HIAT:w" s="T140">kepsiːr</ts>
                  <nts id="Seg_503" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T142" id="Seg_505" n="HIAT:w" s="T141">ühü</ts>
                  <nts id="Seg_506" n="HIAT:ip">.</nts>
                  <nts id="Seg_507" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T152" id="Seg_509" n="HIAT:u" s="T142">
                  <nts id="Seg_510" n="HIAT:ip">"</nts>
                  <ts e="T143" id="Seg_512" n="HIAT:w" s="T142">Bu</ts>
                  <nts id="Seg_513" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T144" id="Seg_515" n="HIAT:w" s="T143">tu͡ok</ts>
                  <nts id="Seg_516" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T145" id="Seg_518" n="HIAT:w" s="T144">oduːnu</ts>
                  <nts id="Seg_519" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T146" id="Seg_521" n="HIAT:w" s="T145">kördüŋ</ts>
                  <nts id="Seg_522" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T147" id="Seg_524" n="HIAT:w" s="T146">onno</ts>
                  <nts id="Seg_525" n="HIAT:ip">"</nts>
                  <nts id="Seg_526" n="HIAT:ip">,</nts>
                  <nts id="Seg_527" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T148" id="Seg_529" n="HIAT:w" s="T147">di͡en</ts>
                  <nts id="Seg_530" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T149" id="Seg_532" n="HIAT:w" s="T148">ɨjɨtallar</ts>
                  <nts id="Seg_533" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T150" id="Seg_535" n="HIAT:w" s="T149">hakalar</ts>
                  <nts id="Seg_536" n="HIAT:ip">,</nts>
                  <nts id="Seg_537" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T151" id="Seg_539" n="HIAT:w" s="T150">i͡e</ts>
                  <nts id="Seg_540" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T152" id="Seg_542" n="HIAT:w" s="T151">di</ts>
                  <nts id="Seg_543" n="HIAT:ip">.</nts>
                  <nts id="Seg_544" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T153" id="Seg_546" n="HIAT:u" s="T152">
                  <ts e="T153" id="Seg_548" n="HIAT:w" s="T152">Onuga</ts>
                  <nts id="Seg_549" n="HIAT:ip">:</nts>
                  <nts id="Seg_550" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T169" id="Seg_552" n="HIAT:u" s="T153">
                  <nts id="Seg_553" n="HIAT:ip">"</nts>
                  <ts e="T154" id="Seg_555" n="HIAT:w" s="T153">Min</ts>
                  <nts id="Seg_556" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T155" id="Seg_558" n="HIAT:w" s="T154">oduːnu</ts>
                  <nts id="Seg_559" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T156" id="Seg_561" n="HIAT:w" s="T155">tugu</ts>
                  <nts id="Seg_562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T157" id="Seg_564" n="HIAT:w" s="T156">da</ts>
                  <nts id="Seg_565" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T158" id="Seg_567" n="HIAT:w" s="T157">olus</ts>
                  <nts id="Seg_568" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T159" id="Seg_570" n="HIAT:w" s="T158">bulbatɨm</ts>
                  <nts id="Seg_571" n="HIAT:ip">,</nts>
                  <nts id="Seg_572" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T160" id="Seg_574" n="HIAT:w" s="T159">marsalara</ts>
                  <nts id="Seg_575" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T161" id="Seg_577" n="HIAT:w" s="T160">bu͡ollagɨna</ts>
                  <nts id="Seg_578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T162" id="Seg_580" n="HIAT:w" s="T161">tiːsteritten</ts>
                  <nts id="Seg_581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T163" id="Seg_583" n="HIAT:w" s="T162">baːjan</ts>
                  <nts id="Seg_584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T164" id="Seg_586" n="HIAT:w" s="T163">baran</ts>
                  <nts id="Seg_587" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T165" id="Seg_589" n="HIAT:w" s="T164">utujallar</ts>
                  <nts id="Seg_590" n="HIAT:ip">,</nts>
                  <nts id="Seg_591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T166" id="Seg_593" n="HIAT:w" s="T165">utuja</ts>
                  <nts id="Seg_594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T167" id="Seg_596" n="HIAT:w" s="T166">hɨtallarɨn</ts>
                  <nts id="Seg_597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T168" id="Seg_599" n="HIAT:w" s="T167">baːjan</ts>
                  <nts id="Seg_600" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T169" id="Seg_602" n="HIAT:w" s="T168">bremnoːttan</ts>
                  <nts id="Seg_603" n="HIAT:ip">.</nts>
                  <nts id="Seg_604" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T178" id="Seg_606" n="HIAT:u" s="T169">
                  <ts e="T170" id="Seg_608" n="HIAT:w" s="T169">Onu</ts>
                  <nts id="Seg_609" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T171" id="Seg_611" n="HIAT:w" s="T170">hep</ts>
                  <nts id="Seg_612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T172" id="Seg_614" n="HIAT:w" s="T171">daː</ts>
                  <nts id="Seg_615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T173" id="Seg_617" n="HIAT:w" s="T172">di͡ebetter</ts>
                  <nts id="Seg_618" n="HIAT:ip">,</nts>
                  <nts id="Seg_619" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T174" id="Seg_621" n="HIAT:w" s="T173">hagastaːn</ts>
                  <nts id="Seg_622" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T175" id="Seg_624" n="HIAT:w" s="T174">kiːllereller</ts>
                  <nts id="Seg_625" n="HIAT:ip">,</nts>
                  <nts id="Seg_626" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T176" id="Seg_628" n="HIAT:w" s="T175">uːga</ts>
                  <nts id="Seg_629" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T177" id="Seg_631" n="HIAT:w" s="T176">kiːlleren</ts>
                  <nts id="Seg_632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T178" id="Seg_634" n="HIAT:w" s="T177">keːheller</ts>
                  <nts id="Seg_635" n="HIAT:ip">.</nts>
                  <nts id="Seg_636" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T188" id="Seg_638" n="HIAT:u" s="T178">
                  <ts e="T179" id="Seg_640" n="HIAT:w" s="T178">Onton</ts>
                  <nts id="Seg_641" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T180" id="Seg_643" n="HIAT:w" s="T179">araj</ts>
                  <nts id="Seg_644" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T181" id="Seg_646" n="HIAT:w" s="T180">arɨːga</ts>
                  <nts id="Seg_647" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T182" id="Seg_649" n="HIAT:w" s="T181">taːs</ts>
                  <nts id="Seg_650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T183" id="Seg_652" n="HIAT:w" s="T182">kajaga</ts>
                  <nts id="Seg_653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T184" id="Seg_655" n="HIAT:w" s="T183">bullum</ts>
                  <nts id="Seg_656" n="HIAT:ip">"</nts>
                  <nts id="Seg_657" n="HIAT:ip">,</nts>
                  <nts id="Seg_658" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T185" id="Seg_660" n="HIAT:w" s="T184">diːr</ts>
                  <nts id="Seg_661" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T186" id="Seg_663" n="HIAT:w" s="T185">ühü</ts>
                  <nts id="Seg_664" n="HIAT:ip">,</nts>
                  <nts id="Seg_665" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_666" n="HIAT:ip">"</nts>
                  <ts e="T187" id="Seg_668" n="HIAT:w" s="T186">biːr</ts>
                  <nts id="Seg_669" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T188" id="Seg_671" n="HIAT:w" s="T187">oduːnu</ts>
                  <nts id="Seg_672" n="HIAT:ip">.</nts>
                  <nts id="Seg_673" n="HIAT:ip">"</nts>
                  <nts id="Seg_674" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T191" id="Seg_676" n="HIAT:u" s="T188">
                  <nts id="Seg_677" n="HIAT:ip">"</nts>
                  <ts e="T189" id="Seg_679" n="HIAT:w" s="T188">Dʼe</ts>
                  <nts id="Seg_680" n="HIAT:ip">,</nts>
                  <nts id="Seg_681" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T190" id="Seg_683" n="HIAT:w" s="T189">tugu</ts>
                  <nts id="Seg_684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T191" id="Seg_686" n="HIAT:w" s="T190">bulluŋ</ts>
                  <nts id="Seg_687" n="HIAT:ip">?</nts>
                  <nts id="Seg_688" n="HIAT:ip">"</nts>
                  <nts id="Seg_689" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T194" id="Seg_691" n="HIAT:u" s="T191">
                  <ts e="T192" id="Seg_693" n="HIAT:w" s="T191">Onu</ts>
                  <nts id="Seg_694" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T193" id="Seg_696" n="HIAT:w" s="T192">diːr</ts>
                  <nts id="Seg_697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T194" id="Seg_699" n="HIAT:w" s="T193">bu͡o</ts>
                  <nts id="Seg_700" n="HIAT:ip">:</nts>
                  <nts id="Seg_701" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T472" id="Seg_702" n="sc" s="T209">
               <ts e="T213" id="Seg_704" n="HIAT:u" s="T209">
                  <nts id="Seg_705" n="HIAT:ip">"</nts>
                  <ts e="T210" id="Seg_707" n="HIAT:w" s="T209">Onuga</ts>
                  <nts id="Seg_708" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T211" id="Seg_710" n="HIAT:w" s="T210">kiːren</ts>
                  <nts id="Seg_711" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T212" id="Seg_713" n="HIAT:w" s="T211">bardɨm</ts>
                  <nts id="Seg_714" n="HIAT:ip">"</nts>
                  <nts id="Seg_715" n="HIAT:ip">,</nts>
                  <nts id="Seg_716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T213" id="Seg_718" n="HIAT:w" s="T212">diːr</ts>
                  <nts id="Seg_719" n="HIAT:ip">.</nts>
                  <nts id="Seg_720" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T220" id="Seg_722" n="HIAT:u" s="T213">
                  <ts e="T214" id="Seg_724" n="HIAT:w" s="T213">Üs</ts>
                  <nts id="Seg_725" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T215" id="Seg_727" n="HIAT:w" s="T214">kihileːk</ts>
                  <nts id="Seg_728" n="HIAT:ip">,</nts>
                  <nts id="Seg_729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T216" id="Seg_731" n="HIAT:w" s="T215">bu</ts>
                  <nts id="Seg_732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T217" id="Seg_734" n="HIAT:w" s="T216">kihiler</ts>
                  <nts id="Seg_735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T218" id="Seg_737" n="HIAT:w" s="T217">aha</ts>
                  <nts id="Seg_738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T219" id="Seg_740" n="HIAT:w" s="T218">hu͡ok</ts>
                  <nts id="Seg_741" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T220" id="Seg_743" n="HIAT:w" s="T219">bu͡olbupput</ts>
                  <nts id="Seg_744" n="HIAT:ip">.</nts>
                  <nts id="Seg_745" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T232" id="Seg_747" n="HIAT:u" s="T220">
                  <ts e="T221" id="Seg_749" n="HIAT:w" s="T220">Oččogo</ts>
                  <nts id="Seg_750" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T222" id="Seg_752" n="HIAT:w" s="T221">bu͡ollagɨna</ts>
                  <nts id="Seg_753" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T223" id="Seg_755" n="HIAT:w" s="T222">ebetten</ts>
                  <nts id="Seg_756" n="HIAT:ip">,</nts>
                  <nts id="Seg_757" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T224" id="Seg_759" n="HIAT:w" s="T223">o</ts>
                  <nts id="Seg_760" n="HIAT:ip">,</nts>
                  <nts id="Seg_761" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T225" id="Seg_763" n="HIAT:w" s="T224">kɨra</ts>
                  <nts id="Seg_764" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T226" id="Seg_766" n="HIAT:w" s="T225">balɨkkaːnɨ</ts>
                  <nts id="Seg_767" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T227" id="Seg_769" n="HIAT:w" s="T226">kabɨnannar</ts>
                  <nts id="Seg_770" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T228" id="Seg_772" n="HIAT:w" s="T227">ol</ts>
                  <nts id="Seg_773" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T229" id="Seg_775" n="HIAT:w" s="T228">aːŋŋa</ts>
                  <nts id="Seg_776" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T230" id="Seg_778" n="HIAT:w" s="T229">kiːrenner</ts>
                  <nts id="Seg_779" n="HIAT:ip">,</nts>
                  <nts id="Seg_780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T231" id="Seg_782" n="HIAT:w" s="T230">ahaːrɨlar</ts>
                  <nts id="Seg_783" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T232" id="Seg_785" n="HIAT:w" s="T231">kiːrbitter</ts>
                  <nts id="Seg_786" n="HIAT:ip">.</nts>
                  <nts id="Seg_787" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T239" id="Seg_789" n="HIAT:u" s="T232">
                  <ts e="T233" id="Seg_791" n="HIAT:w" s="T232">Bu</ts>
                  <nts id="Seg_792" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T234" id="Seg_794" n="HIAT:w" s="T233">kihi</ts>
                  <nts id="Seg_795" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T235" id="Seg_797" n="HIAT:w" s="T234">araj</ts>
                  <nts id="Seg_798" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T236" id="Seg_800" n="HIAT:w" s="T235">kiːrbite</ts>
                  <nts id="Seg_801" n="HIAT:ip">,</nts>
                  <nts id="Seg_802" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T237" id="Seg_804" n="HIAT:w" s="T236">bu</ts>
                  <nts id="Seg_805" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T238" id="Seg_807" n="HIAT:w" s="T237">kihi</ts>
                  <nts id="Seg_808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T239" id="Seg_810" n="HIAT:w" s="T238">oloror</ts>
                  <nts id="Seg_811" n="HIAT:ip">.</nts>
                  <nts id="Seg_812" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T246" id="Seg_814" n="HIAT:u" s="T239">
                  <ts e="T240" id="Seg_816" n="HIAT:w" s="T239">Olordoguna</ts>
                  <nts id="Seg_817" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T241" id="Seg_819" n="HIAT:w" s="T240">tu͡ok</ts>
                  <nts id="Seg_820" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T242" id="Seg_822" n="HIAT:w" s="T241">ere</ts>
                  <nts id="Seg_823" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T243" id="Seg_825" n="HIAT:w" s="T242">kihite</ts>
                  <nts id="Seg_826" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T244" id="Seg_828" n="HIAT:w" s="T243">kelbit</ts>
                  <nts id="Seg_829" n="HIAT:ip">,</nts>
                  <nts id="Seg_830" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T245" id="Seg_832" n="HIAT:w" s="T244">i͡e</ts>
                  <nts id="Seg_833" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T246" id="Seg_835" n="HIAT:w" s="T245">diː</ts>
                  <nts id="Seg_836" n="HIAT:ip">.</nts>
                  <nts id="Seg_837" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T250" id="Seg_839" n="HIAT:u" s="T246">
                  <ts e="T247" id="Seg_841" n="HIAT:w" s="T246">Kihi</ts>
                  <nts id="Seg_842" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T248" id="Seg_844" n="HIAT:w" s="T247">duː</ts>
                  <nts id="Seg_845" n="HIAT:ip">,</nts>
                  <nts id="Seg_846" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T249" id="Seg_848" n="HIAT:w" s="T248">tu͡ok</ts>
                  <nts id="Seg_849" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T250" id="Seg_851" n="HIAT:w" s="T249">duː</ts>
                  <nts id="Seg_852" n="HIAT:ip">?</nts>
                  <nts id="Seg_853" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T263" id="Seg_855" n="HIAT:u" s="T250">
                  <nts id="Seg_856" n="HIAT:ip">"</nts>
                  <ts e="T251" id="Seg_858" n="HIAT:w" s="T250">Hannɨttan</ts>
                  <nts id="Seg_859" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T252" id="Seg_861" n="HIAT:w" s="T251">menʼiːte</ts>
                  <nts id="Seg_862" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T253" id="Seg_864" n="HIAT:w" s="T252">hu͡ok</ts>
                  <nts id="Seg_865" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T254" id="Seg_867" n="HIAT:w" s="T253">kihi</ts>
                  <nts id="Seg_868" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T255" id="Seg_870" n="HIAT:w" s="T254">kiːrbit</ts>
                  <nts id="Seg_871" n="HIAT:ip">,</nts>
                  <nts id="Seg_872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T256" id="Seg_874" n="HIAT:w" s="T255">kihi</ts>
                  <nts id="Seg_875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T257" id="Seg_877" n="HIAT:w" s="T256">duː</ts>
                  <nts id="Seg_878" n="HIAT:ip">,</nts>
                  <nts id="Seg_879" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T258" id="Seg_881" n="HIAT:w" s="T257">tu͡ok</ts>
                  <nts id="Seg_882" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T259" id="Seg_884" n="HIAT:w" s="T258">duː</ts>
                  <nts id="Seg_885" n="HIAT:ip">,</nts>
                  <nts id="Seg_886" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T260" id="Seg_888" n="HIAT:w" s="T259">tugu</ts>
                  <nts id="Seg_889" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T261" id="Seg_891" n="HIAT:w" s="T260">daː</ts>
                  <nts id="Seg_892" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T262" id="Seg_894" n="HIAT:w" s="T261">tu͡olkulaːbappɨn</ts>
                  <nts id="Seg_895" n="HIAT:ip">"</nts>
                  <nts id="Seg_896" n="HIAT:ip">,</nts>
                  <nts id="Seg_897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T263" id="Seg_899" n="HIAT:w" s="T262">di͡ebit</ts>
                  <nts id="Seg_900" n="HIAT:ip">.</nts>
                  <nts id="Seg_901" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T269" id="Seg_903" n="HIAT:u" s="T263">
                  <ts e="T264" id="Seg_905" n="HIAT:w" s="T263">Ol</ts>
                  <nts id="Seg_906" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T265" id="Seg_908" n="HIAT:w" s="T264">kiːren</ts>
                  <nts id="Seg_909" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T266" id="Seg_911" n="HIAT:w" s="T265">aːŋŋa</ts>
                  <nts id="Seg_912" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T267" id="Seg_914" n="HIAT:w" s="T266">olorbut</ts>
                  <nts id="Seg_915" n="HIAT:ip">,</nts>
                  <nts id="Seg_916" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T268" id="Seg_918" n="HIAT:w" s="T267">i͡e</ts>
                  <nts id="Seg_919" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T269" id="Seg_921" n="HIAT:w" s="T268">diː</ts>
                  <nts id="Seg_922" n="HIAT:ip">.</nts>
                  <nts id="Seg_923" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T273" id="Seg_925" n="HIAT:u" s="T269">
                  <ts e="T270" id="Seg_927" n="HIAT:w" s="T269">Ol</ts>
                  <nts id="Seg_928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T271" id="Seg_930" n="HIAT:w" s="T270">oloron</ts>
                  <nts id="Seg_931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T272" id="Seg_933" n="HIAT:w" s="T271">bu͡ollagɨna</ts>
                  <nts id="Seg_934" n="HIAT:ip">,</nts>
                  <nts id="Seg_935" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T273" id="Seg_937" n="HIAT:w" s="T272">kajaː</ts>
                  <nts id="Seg_938" n="HIAT:ip">.</nts>
                  <nts id="Seg_939" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T281" id="Seg_941" n="HIAT:u" s="T273">
                  <ts e="T274" id="Seg_943" n="HIAT:w" s="T273">Külen</ts>
                  <nts id="Seg_944" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T275" id="Seg_946" n="HIAT:w" s="T274">ɨrdʼaŋnaːbɨt</ts>
                  <nts id="Seg_947" n="HIAT:ip">,</nts>
                  <nts id="Seg_948" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T276" id="Seg_950" n="HIAT:w" s="T275">kanna</ts>
                  <nts id="Seg_951" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T277" id="Seg_953" n="HIAT:w" s="T276">ere</ts>
                  <nts id="Seg_954" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T278" id="Seg_956" n="HIAT:w" s="T277">karaktaːk</ts>
                  <nts id="Seg_957" n="HIAT:ip">,</nts>
                  <nts id="Seg_958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T279" id="Seg_960" n="HIAT:w" s="T278">töbötüger</ts>
                  <nts id="Seg_961" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T280" id="Seg_963" n="HIAT:w" s="T279">hogotok</ts>
                  <nts id="Seg_964" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T281" id="Seg_966" n="HIAT:w" s="T280">karaktaːk</ts>
                  <nts id="Seg_967" n="HIAT:ip">.</nts>
                  <nts id="Seg_968" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T292" id="Seg_970" n="HIAT:u" s="T281">
                  <nts id="Seg_971" n="HIAT:ip">"</nts>
                  <ts e="T282" id="Seg_973" n="HIAT:w" s="T281">Kaja</ts>
                  <nts id="Seg_974" n="HIAT:ip">,</nts>
                  <nts id="Seg_975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_976" n="HIAT:ip">(</nts>
                  <ts e="T284" id="Seg_978" n="HIAT:w" s="T282">tu͡o-</ts>
                  <nts id="Seg_979" n="HIAT:ip">)</nts>
                  <nts id="Seg_980" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T285" id="Seg_982" n="HIAT:w" s="T284">dogottor</ts>
                  <nts id="Seg_983" n="HIAT:ip">,</nts>
                  <nts id="Seg_984" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T286" id="Seg_986" n="HIAT:w" s="T285">bigetik</ts>
                  <nts id="Seg_987" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T287" id="Seg_989" n="HIAT:w" s="T286">tuttuŋ</ts>
                  <nts id="Seg_990" n="HIAT:ip">"</nts>
                  <nts id="Seg_991" n="HIAT:ip">,</nts>
                  <nts id="Seg_992" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T288" id="Seg_994" n="HIAT:w" s="T287">diːr</ts>
                  <nts id="Seg_995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T289" id="Seg_997" n="HIAT:w" s="T288">bu͡o</ts>
                  <nts id="Seg_998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T290" id="Seg_1000" n="HIAT:w" s="T289">dogorun</ts>
                  <nts id="Seg_1001" n="HIAT:ip">,</nts>
                  <nts id="Seg_1002" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T291" id="Seg_1004" n="HIAT:w" s="T290">u͡olattarɨn</ts>
                  <nts id="Seg_1005" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T292" id="Seg_1007" n="HIAT:w" s="T291">Bi͡egičep</ts>
                  <nts id="Seg_1008" n="HIAT:ip">.</nts>
                  <nts id="Seg_1009" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T300" id="Seg_1011" n="HIAT:u" s="T292">
                  <ts e="T293" id="Seg_1013" n="HIAT:w" s="T292">Ontuta</ts>
                  <nts id="Seg_1014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1015" n="HIAT:ip">"</nts>
                  <ts e="T294" id="Seg_1017" n="HIAT:w" s="T293">huːp</ts>
                  <nts id="Seg_1018" n="HIAT:ip">"</nts>
                  <nts id="Seg_1019" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T295" id="Seg_1021" n="HIAT:w" s="T294">di͡en</ts>
                  <nts id="Seg_1022" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T296" id="Seg_1024" n="HIAT:w" s="T295">oborbut</ts>
                  <nts id="Seg_1025" n="HIAT:ip">,</nts>
                  <nts id="Seg_1026" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T297" id="Seg_1028" n="HIAT:w" s="T296">ikki</ts>
                  <nts id="Seg_1029" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T298" id="Seg_1031" n="HIAT:w" s="T297">kihitin</ts>
                  <nts id="Seg_1032" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T299" id="Seg_1034" n="HIAT:w" s="T298">oboron</ts>
                  <nts id="Seg_1035" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T300" id="Seg_1037" n="HIAT:w" s="T299">ɨlbɨt</ts>
                  <nts id="Seg_1038" n="HIAT:ip">.</nts>
                  <nts id="Seg_1039" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T306" id="Seg_1041" n="HIAT:u" s="T300">
                  <ts e="T301" id="Seg_1043" n="HIAT:w" s="T300">Oččo</ts>
                  <nts id="Seg_1044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T302" id="Seg_1046" n="HIAT:w" s="T301">bu</ts>
                  <nts id="Seg_1047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T303" id="Seg_1049" n="HIAT:w" s="T302">kihi</ts>
                  <nts id="Seg_1050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T304" id="Seg_1052" n="HIAT:w" s="T303">hogotok</ts>
                  <nts id="Seg_1053" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T305" id="Seg_1055" n="HIAT:w" s="T304">kihileːk</ts>
                  <nts id="Seg_1056" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T306" id="Seg_1058" n="HIAT:w" s="T305">kaːlbɨt</ts>
                  <nts id="Seg_1059" n="HIAT:ip">.</nts>
                  <nts id="Seg_1060" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T313" id="Seg_1062" n="HIAT:u" s="T306">
                  <ts e="T307" id="Seg_1064" n="HIAT:w" s="T306">Onno</ts>
                  <nts id="Seg_1065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T308" id="Seg_1067" n="HIAT:w" s="T307">bu</ts>
                  <nts id="Seg_1068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T309" id="Seg_1070" n="HIAT:w" s="T308">kihi</ts>
                  <nts id="Seg_1071" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T310" id="Seg_1073" n="HIAT:w" s="T309">bu͡olla</ts>
                  <nts id="Seg_1074" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T311" id="Seg_1076" n="HIAT:w" s="T310">pʼistalʼettaːk</ts>
                  <nts id="Seg_1077" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T312" id="Seg_1079" n="HIAT:w" s="T311">ebit</ts>
                  <nts id="Seg_1080" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T313" id="Seg_1082" n="HIAT:w" s="T312">ol</ts>
                  <nts id="Seg_1083" n="HIAT:ip">.</nts>
                  <nts id="Seg_1084" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T321" id="Seg_1086" n="HIAT:u" s="T313">
                  <nts id="Seg_1087" n="HIAT:ip">"</nts>
                  <ts e="T314" id="Seg_1089" n="HIAT:w" s="T313">Onno</ts>
                  <nts id="Seg_1090" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T315" id="Seg_1092" n="HIAT:w" s="T314">diːbin</ts>
                  <nts id="Seg_1093" n="HIAT:ip">,</nts>
                  <nts id="Seg_1094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T316" id="Seg_1096" n="HIAT:w" s="T315">haŋarar</ts>
                  <nts id="Seg_1097" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T317" id="Seg_1099" n="HIAT:w" s="T316">tɨlɨnan</ts>
                  <nts id="Seg_1100" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T318" id="Seg_1102" n="HIAT:w" s="T317">haŋarabɨn</ts>
                  <nts id="Seg_1103" n="HIAT:ip">"</nts>
                  <nts id="Seg_1104" n="HIAT:ip">,</nts>
                  <nts id="Seg_1105" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T319" id="Seg_1107" n="HIAT:w" s="T318">di͡ebit</ts>
                  <nts id="Seg_1108" n="HIAT:ip">,</nts>
                  <nts id="Seg_1109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1110" n="HIAT:ip">"</nts>
                  <ts e="T320" id="Seg_1112" n="HIAT:w" s="T319">pʼistalʼetɨm</ts>
                  <nts id="Seg_1113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T321" id="Seg_1115" n="HIAT:w" s="T320">anʼagɨgar</ts>
                  <nts id="Seg_1116" n="HIAT:ip">.</nts>
                  <nts id="Seg_1117" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T327" id="Seg_1119" n="HIAT:u" s="T321">
                  <ts e="T322" id="Seg_1121" n="HIAT:w" s="T321">Ka</ts>
                  <nts id="Seg_1122" n="HIAT:ip">,</nts>
                  <nts id="Seg_1123" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T323" id="Seg_1125" n="HIAT:w" s="T322">bu</ts>
                  <nts id="Seg_1126" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T324" id="Seg_1128" n="HIAT:w" s="T323">dojduga</ts>
                  <nts id="Seg_1129" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T325" id="Seg_1131" n="HIAT:w" s="T324">kaːlabɨn</ts>
                  <nts id="Seg_1132" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T326" id="Seg_1134" n="HIAT:w" s="T325">du͡o</ts>
                  <nts id="Seg_1135" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T327" id="Seg_1137" n="HIAT:w" s="T326">min</ts>
                  <nts id="Seg_1138" n="HIAT:ip">?</nts>
                  <nts id="Seg_1139" n="HIAT:ip">"</nts>
                  <nts id="Seg_1140" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T336" id="Seg_1142" n="HIAT:u" s="T327">
                  <nts id="Seg_1143" n="HIAT:ip">"</nts>
                  <ts e="T328" id="Seg_1145" n="HIAT:w" s="T327">Minigin</ts>
                  <nts id="Seg_1146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T329" id="Seg_1148" n="HIAT:w" s="T328">bɨːhaː</ts>
                  <nts id="Seg_1149" n="HIAT:ip">,</nts>
                  <nts id="Seg_1150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T330" id="Seg_1152" n="HIAT:w" s="T329">bunu</ts>
                  <nts id="Seg_1153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T331" id="Seg_1155" n="HIAT:w" s="T330">kanna</ts>
                  <nts id="Seg_1156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T332" id="Seg_1158" n="HIAT:w" s="T331">eme</ts>
                  <nts id="Seg_1159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T333" id="Seg_1161" n="HIAT:w" s="T332">uŋarar</ts>
                  <nts id="Seg_1162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T334" id="Seg_1164" n="HIAT:w" s="T333">hirger</ts>
                  <nts id="Seg_1165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T335" id="Seg_1167" n="HIAT:w" s="T334">tiri͡er</ts>
                  <nts id="Seg_1168" n="HIAT:ip">"</nts>
                  <nts id="Seg_1169" n="HIAT:ip">,</nts>
                  <nts id="Seg_1170" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T336" id="Seg_1172" n="HIAT:w" s="T335">diːr</ts>
                  <nts id="Seg_1173" n="HIAT:ip">.</nts>
                  <nts id="Seg_1174" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T345" id="Seg_1176" n="HIAT:u" s="T336">
                  <ts e="T337" id="Seg_1178" n="HIAT:w" s="T336">Hüːhüger</ts>
                  <nts id="Seg_1179" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T338" id="Seg_1181" n="HIAT:w" s="T337">ɨppɨt</ts>
                  <nts id="Seg_1182" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T339" id="Seg_1184" n="HIAT:w" s="T338">bu͡olla</ts>
                  <nts id="Seg_1185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T340" id="Seg_1187" n="HIAT:w" s="T339">bu</ts>
                  <nts id="Seg_1188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T341" id="Seg_1190" n="HIAT:w" s="T340">pʼistalʼetɨnan</ts>
                  <nts id="Seg_1191" n="HIAT:ip">,</nts>
                  <nts id="Seg_1192" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T342" id="Seg_1194" n="HIAT:w" s="T341">ontuta</ts>
                  <nts id="Seg_1195" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T343" id="Seg_1197" n="HIAT:w" s="T342">dʼe</ts>
                  <nts id="Seg_1198" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T344" id="Seg_1200" n="HIAT:w" s="T343">uŋan</ts>
                  <nts id="Seg_1201" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T345" id="Seg_1203" n="HIAT:w" s="T344">tüspüt</ts>
                  <nts id="Seg_1204" n="HIAT:ip">.</nts>
                  <nts id="Seg_1205" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T352" id="Seg_1207" n="HIAT:u" s="T345">
                  <ts e="T346" id="Seg_1209" n="HIAT:w" s="T345">Ol</ts>
                  <nts id="Seg_1210" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T347" id="Seg_1212" n="HIAT:w" s="T346">uŋan</ts>
                  <nts id="Seg_1213" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T348" id="Seg_1215" n="HIAT:w" s="T347">tüspütüger</ts>
                  <nts id="Seg_1216" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T349" id="Seg_1218" n="HIAT:w" s="T348">ürdünnen</ts>
                  <nts id="Seg_1219" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T350" id="Seg_1221" n="HIAT:w" s="T349">hüːren</ts>
                  <nts id="Seg_1222" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T351" id="Seg_1224" n="HIAT:w" s="T350">taksɨbɨttar</ts>
                  <nts id="Seg_1225" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T352" id="Seg_1227" n="HIAT:w" s="T351">bu͡ollar</ts>
                  <nts id="Seg_1228" n="HIAT:ip">.</nts>
                  <nts id="Seg_1229" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T356" id="Seg_1231" n="HIAT:u" s="T352">
                  <ts e="T353" id="Seg_1233" n="HIAT:w" s="T352">Bi͡egičebiŋ</ts>
                  <nts id="Seg_1234" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T354" id="Seg_1236" n="HIAT:w" s="T353">kepsiːr</ts>
                  <nts id="Seg_1237" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1238" n="HIAT:ip">(</nts>
                  <ts e="T356" id="Seg_1240" n="HIAT:w" s="T354">kepsi-</ts>
                  <nts id="Seg_1241" n="HIAT:ip">)</nts>
                  <nts id="Seg_1242" n="HIAT:ip">:</nts>
                  <nts id="Seg_1243" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T370" id="Seg_1245" n="HIAT:u" s="T356">
                  <nts id="Seg_1246" n="HIAT:ip">"</nts>
                  <ts e="T357" id="Seg_1248" n="HIAT:w" s="T356">Ol</ts>
                  <nts id="Seg_1249" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T358" id="Seg_1251" n="HIAT:w" s="T357">tijemmit</ts>
                  <nts id="Seg_1252" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T359" id="Seg_1254" n="HIAT:w" s="T358">kimi͡eke</ts>
                  <nts id="Seg_1255" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T360" id="Seg_1257" n="HIAT:w" s="T359">bardɨbɨt</ts>
                  <nts id="Seg_1258" n="HIAT:ip">"</nts>
                  <nts id="Seg_1259" n="HIAT:ip">,</nts>
                  <nts id="Seg_1260" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T361" id="Seg_1262" n="HIAT:w" s="T360">diːr</ts>
                  <nts id="Seg_1263" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T362" id="Seg_1265" n="HIAT:w" s="T361">bu͡o</ts>
                  <nts id="Seg_1266" n="HIAT:ip">,</nts>
                  <nts id="Seg_1267" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1268" n="HIAT:ip">"</nts>
                  <ts e="T363" id="Seg_1270" n="HIAT:w" s="T362">kaːtʼerbar</ts>
                  <nts id="Seg_1271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T364" id="Seg_1273" n="HIAT:w" s="T363">bardɨm</ts>
                  <nts id="Seg_1274" n="HIAT:ip">"</nts>
                  <nts id="Seg_1275" n="HIAT:ip">,</nts>
                  <nts id="Seg_1276" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T365" id="Seg_1278" n="HIAT:w" s="T364">diːr</ts>
                  <nts id="Seg_1279" n="HIAT:ip">,</nts>
                  <nts id="Seg_1280" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1281" n="HIAT:ip">"</nts>
                  <ts e="T366" id="Seg_1283" n="HIAT:w" s="T365">kaːtʼerbar</ts>
                  <nts id="Seg_1284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T367" id="Seg_1286" n="HIAT:w" s="T366">olorommun</ts>
                  <nts id="Seg_1287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T368" id="Seg_1289" n="HIAT:w" s="T367">ebe</ts>
                  <nts id="Seg_1290" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T369" id="Seg_1292" n="HIAT:w" s="T368">ü͡öhüger</ts>
                  <nts id="Seg_1293" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T370" id="Seg_1295" n="HIAT:w" s="T369">kiːrdim</ts>
                  <nts id="Seg_1296" n="HIAT:ip">.</nts>
                  <nts id="Seg_1297" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T376" id="Seg_1299" n="HIAT:u" s="T370">
                  <ts e="T371" id="Seg_1301" n="HIAT:w" s="T370">Körbütüm</ts>
                  <nts id="Seg_1302" n="HIAT:ip">,</nts>
                  <nts id="Seg_1303" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T372" id="Seg_1305" n="HIAT:w" s="T371">kennibitten</ts>
                  <nts id="Seg_1306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T373" id="Seg_1308" n="HIAT:w" s="T372">baːjdʼɨgɨm</ts>
                  <nts id="Seg_1309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T374" id="Seg_1311" n="HIAT:w" s="T373">turan</ts>
                  <nts id="Seg_1312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T375" id="Seg_1314" n="HIAT:w" s="T374">kelle</ts>
                  <nts id="Seg_1315" n="HIAT:ip">,</nts>
                  <nts id="Seg_1316" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T376" id="Seg_1318" n="HIAT:w" s="T375">di͡en</ts>
                  <nts id="Seg_1319" n="HIAT:ip">.</nts>
                  <nts id="Seg_1320" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T390" id="Seg_1322" n="HIAT:u" s="T376">
                  <ts e="T377" id="Seg_1324" n="HIAT:w" s="T376">Turan</ts>
                  <nts id="Seg_1325" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T378" id="Seg_1327" n="HIAT:w" s="T377">kelen</ts>
                  <nts id="Seg_1328" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T379" id="Seg_1330" n="HIAT:w" s="T378">bu͡ollagɨna</ts>
                  <nts id="Seg_1331" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T380" id="Seg_1333" n="HIAT:w" s="T379">kaːtʼerdaːgar</ts>
                  <nts id="Seg_1334" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T381" id="Seg_1336" n="HIAT:w" s="T380">daː</ts>
                  <nts id="Seg_1337" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T382" id="Seg_1339" n="HIAT:w" s="T381">ulakan</ts>
                  <nts id="Seg_1340" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T383" id="Seg_1342" n="HIAT:w" s="T382">boldogu</ts>
                  <nts id="Seg_1343" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T384" id="Seg_1345" n="HIAT:w" s="T383">ɨlan</ts>
                  <nts id="Seg_1346" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T385" id="Seg_1348" n="HIAT:w" s="T384">baraːn</ts>
                  <nts id="Seg_1349" n="HIAT:ip">,</nts>
                  <nts id="Seg_1350" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T386" id="Seg_1352" n="HIAT:w" s="T385">dʼe</ts>
                  <nts id="Seg_1353" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T387" id="Seg_1355" n="HIAT:w" s="T386">bɨrakpɨt</ts>
                  <nts id="Seg_1356" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T388" id="Seg_1358" n="HIAT:w" s="T387">kim</ts>
                  <nts id="Seg_1359" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T389" id="Seg_1361" n="HIAT:w" s="T388">dʼe</ts>
                  <nts id="Seg_1362" n="HIAT:ip">,</nts>
                  <nts id="Seg_1363" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T390" id="Seg_1365" n="HIAT:w" s="T389">ɨttaraːččɨta</ts>
                  <nts id="Seg_1366" n="HIAT:ip">.</nts>
                  <nts id="Seg_1367" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T408" id="Seg_1369" n="HIAT:u" s="T390">
                  <ts e="T391" id="Seg_1371" n="HIAT:w" s="T390">Ol</ts>
                  <nts id="Seg_1372" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T392" id="Seg_1374" n="HIAT:w" s="T391">bɨrakpɨtɨgar</ts>
                  <nts id="Seg_1375" n="HIAT:ip">,</nts>
                  <nts id="Seg_1376" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T393" id="Seg_1378" n="HIAT:w" s="T392">he</ts>
                  <nts id="Seg_1379" n="HIAT:ip">,</nts>
                  <nts id="Seg_1380" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T394" id="Seg_1382" n="HIAT:w" s="T393">arɨččɨ</ts>
                  <nts id="Seg_1383" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T395" id="Seg_1385" n="HIAT:w" s="T394">ebe</ts>
                  <nts id="Seg_1386" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T396" id="Seg_1388" n="HIAT:w" s="T395">ü͡öhüger</ts>
                  <nts id="Seg_1389" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T397" id="Seg_1391" n="HIAT:w" s="T396">tiːjbitim</ts>
                  <nts id="Seg_1392" n="HIAT:ip">,</nts>
                  <nts id="Seg_1393" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T398" id="Seg_1395" n="HIAT:w" s="T397">tiːjerin</ts>
                  <nts id="Seg_1396" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T399" id="Seg_1398" n="HIAT:w" s="T398">kɨtta</ts>
                  <nts id="Seg_1399" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T400" id="Seg_1401" n="HIAT:w" s="T399">bu͡olla</ts>
                  <nts id="Seg_1402" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T401" id="Seg_1404" n="HIAT:w" s="T400">ol</ts>
                  <nts id="Seg_1405" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T402" id="Seg_1407" n="HIAT:w" s="T401">dolgunugar</ts>
                  <nts id="Seg_1408" n="HIAT:ip">,</nts>
                  <nts id="Seg_1409" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T403" id="Seg_1411" n="HIAT:w" s="T402">bɨrakpɨt</ts>
                  <nts id="Seg_1412" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T404" id="Seg_1414" n="HIAT:w" s="T403">dolgunugar</ts>
                  <nts id="Seg_1415" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T405" id="Seg_1417" n="HIAT:w" s="T404">čut</ts>
                  <nts id="Seg_1418" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T406" id="Seg_1420" n="HIAT:w" s="T405">tü͡öre</ts>
                  <nts id="Seg_1421" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T407" id="Seg_1423" n="HIAT:w" s="T406">bara</ts>
                  <nts id="Seg_1424" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T408" id="Seg_1426" n="HIAT:w" s="T407">hɨspɨt</ts>
                  <nts id="Seg_1427" n="HIAT:ip">.</nts>
                  <nts id="Seg_1428" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T412" id="Seg_1430" n="HIAT:u" s="T408">
                  <ts e="T409" id="Seg_1432" n="HIAT:w" s="T408">Ol</ts>
                  <nts id="Seg_1433" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T410" id="Seg_1435" n="HIAT:w" s="T409">küreːtibit</ts>
                  <nts id="Seg_1436" n="HIAT:ip">"</nts>
                  <nts id="Seg_1437" n="HIAT:ip">,</nts>
                  <nts id="Seg_1438" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T411" id="Seg_1440" n="HIAT:w" s="T410">di͡ebit</ts>
                  <nts id="Seg_1441" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T412" id="Seg_1443" n="HIAT:w" s="T411">bu͡olla</ts>
                  <nts id="Seg_1444" n="HIAT:ip">.</nts>
                  <nts id="Seg_1445" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T420" id="Seg_1447" n="HIAT:u" s="T412">
                  <ts e="T413" id="Seg_1449" n="HIAT:w" s="T412">Oččo</ts>
                  <nts id="Seg_1450" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T414" id="Seg_1452" n="HIAT:w" s="T413">hette</ts>
                  <nts id="Seg_1453" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T415" id="Seg_1455" n="HIAT:w" s="T414">kihitten</ts>
                  <nts id="Seg_1456" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T416" id="Seg_1458" n="HIAT:w" s="T415">hogotok</ts>
                  <nts id="Seg_1459" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T417" id="Seg_1461" n="HIAT:w" s="T416">kihileːk</ts>
                  <nts id="Seg_1462" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T418" id="Seg_1464" n="HIAT:w" s="T417">kelbit</ts>
                  <nts id="Seg_1465" n="HIAT:ip">,</nts>
                  <nts id="Seg_1466" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T419" id="Seg_1468" n="HIAT:w" s="T418">oktonnor</ts>
                  <nts id="Seg_1469" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T420" id="Seg_1471" n="HIAT:w" s="T419">bejelere</ts>
                  <nts id="Seg_1472" n="HIAT:ip">.</nts>
                  <nts id="Seg_1473" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T431" id="Seg_1475" n="HIAT:u" s="T420">
                  <ts e="T421" id="Seg_1477" n="HIAT:w" s="T420">Tugu</ts>
                  <nts id="Seg_1478" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T422" id="Seg_1480" n="HIAT:w" s="T421">da</ts>
                  <nts id="Seg_1481" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T423" id="Seg_1483" n="HIAT:w" s="T422">bulbakka</ts>
                  <nts id="Seg_1484" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T424" id="Seg_1486" n="HIAT:w" s="T423">ergijen</ts>
                  <nts id="Seg_1487" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1488" n="HIAT:ip">(</nts>
                  <ts e="T425" id="Seg_1490" n="HIAT:w" s="T424">kel-</ts>
                  <nts id="Seg_1491" n="HIAT:ip">)</nts>
                  <nts id="Seg_1492" n="HIAT:ip">,</nts>
                  <nts id="Seg_1493" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T426" id="Seg_1495" n="HIAT:w" s="T425">ol</ts>
                  <nts id="Seg_1496" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T427" id="Seg_1498" n="HIAT:w" s="T426">kelen</ts>
                  <nts id="Seg_1499" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T428" id="Seg_1501" n="HIAT:w" s="T427">ol</ts>
                  <nts id="Seg_1502" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T429" id="Seg_1504" n="HIAT:w" s="T428">üːhe</ts>
                  <nts id="Seg_1505" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T430" id="Seg_1507" n="HIAT:w" s="T429">di͡ek</ts>
                  <nts id="Seg_1508" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T431" id="Seg_1510" n="HIAT:w" s="T430">barbɨt</ts>
                  <nts id="Seg_1511" n="HIAT:ip">.</nts>
                  <nts id="Seg_1512" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T438" id="Seg_1514" n="HIAT:u" s="T431">
                  <ts e="T432" id="Seg_1516" n="HIAT:w" s="T431">Onton</ts>
                  <nts id="Seg_1517" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T433" id="Seg_1519" n="HIAT:w" s="T432">Čagɨdajɨ</ts>
                  <nts id="Seg_1520" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T434" id="Seg_1522" n="HIAT:w" s="T433">bu͡ollagɨn</ts>
                  <nts id="Seg_1523" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T435" id="Seg_1525" n="HIAT:w" s="T434">hajɨn</ts>
                  <nts id="Seg_1526" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T436" id="Seg_1528" n="HIAT:w" s="T435">biːrge</ts>
                  <nts id="Seg_1529" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T437" id="Seg_1531" n="HIAT:w" s="T436">olorollor</ts>
                  <nts id="Seg_1532" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T438" id="Seg_1534" n="HIAT:w" s="T437">ühü</ts>
                  <nts id="Seg_1535" n="HIAT:ip">.</nts>
                  <nts id="Seg_1536" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T449" id="Seg_1538" n="HIAT:u" s="T438">
                  <ts e="T439" id="Seg_1540" n="HIAT:w" s="T438">Ahaːn</ts>
                  <nts id="Seg_1541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T440" id="Seg_1543" n="HIAT:w" s="T439">oloronnor</ts>
                  <nts id="Seg_1544" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T441" id="Seg_1546" n="HIAT:w" s="T440">ka</ts>
                  <nts id="Seg_1547" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T442" id="Seg_1549" n="HIAT:w" s="T441">huruk</ts>
                  <nts id="Seg_1550" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T443" id="Seg_1552" n="HIAT:w" s="T442">kelbit</ts>
                  <nts id="Seg_1553" n="HIAT:ip">,</nts>
                  <nts id="Seg_1554" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T444" id="Seg_1556" n="HIAT:w" s="T443">bu</ts>
                  <nts id="Seg_1557" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T445" id="Seg_1559" n="HIAT:w" s="T444">hurugu</ts>
                  <nts id="Seg_1560" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T446" id="Seg_1562" n="HIAT:w" s="T445">bu͡ollagɨna</ts>
                  <nts id="Seg_1563" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T447" id="Seg_1565" n="HIAT:w" s="T446">Nosku͡oga</ts>
                  <nts id="Seg_1566" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T448" id="Seg_1568" n="HIAT:w" s="T447">tiri͡eri͡ekterin</ts>
                  <nts id="Seg_1569" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T449" id="Seg_1571" n="HIAT:w" s="T448">naːda</ts>
                  <nts id="Seg_1572" n="HIAT:ip">.</nts>
                  <nts id="Seg_1573" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T457" id="Seg_1575" n="HIAT:u" s="T449">
                  <ts e="T450" id="Seg_1577" n="HIAT:w" s="T449">Kantan</ts>
                  <nts id="Seg_1578" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T451" id="Seg_1580" n="HIAT:w" s="T450">ere</ts>
                  <nts id="Seg_1581" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T452" id="Seg_1583" n="HIAT:w" s="T451">kelbit</ts>
                  <nts id="Seg_1584" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T453" id="Seg_1586" n="HIAT:w" s="T452">tɨːnnan</ts>
                  <nts id="Seg_1587" n="HIAT:ip">,</nts>
                  <nts id="Seg_1588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T454" id="Seg_1590" n="HIAT:w" s="T453">oččo</ts>
                  <nts id="Seg_1591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T455" id="Seg_1593" n="HIAT:w" s="T454">tu͡ok</ts>
                  <nts id="Seg_1594" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T456" id="Seg_1596" n="HIAT:w" s="T455">lu͡okkata</ts>
                  <nts id="Seg_1597" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T457" id="Seg_1599" n="HIAT:w" s="T456">keli͡ej</ts>
                  <nts id="Seg_1600" n="HIAT:ip">.</nts>
                  <nts id="Seg_1601" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T467" id="Seg_1603" n="HIAT:u" s="T457">
                  <nts id="Seg_1604" n="HIAT:ip">"</nts>
                  <ts e="T458" id="Seg_1606" n="HIAT:w" s="T457">Ka</ts>
                  <nts id="Seg_1607" n="HIAT:ip">,</nts>
                  <nts id="Seg_1608" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T459" id="Seg_1610" n="HIAT:w" s="T458">Čagɨdaj</ts>
                  <nts id="Seg_1611" n="HIAT:ip">,</nts>
                  <nts id="Seg_1612" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T460" id="Seg_1614" n="HIAT:w" s="T459">bu</ts>
                  <nts id="Seg_1615" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T461" id="Seg_1617" n="HIAT:w" s="T460">hurugu</ts>
                  <nts id="Seg_1618" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T462" id="Seg_1620" n="HIAT:w" s="T461">bu͡olla</ts>
                  <nts id="Seg_1621" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T463" id="Seg_1623" n="HIAT:w" s="T462">kimi͡eke</ts>
                  <nts id="Seg_1624" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T464" id="Seg_1626" n="HIAT:w" s="T463">tiri͡er</ts>
                  <nts id="Seg_1627" n="HIAT:ip">,</nts>
                  <nts id="Seg_1628" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T465" id="Seg_1630" n="HIAT:w" s="T464">Kiri͡eske</ts>
                  <nts id="Seg_1631" n="HIAT:ip">,</nts>
                  <nts id="Seg_1632" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T466" id="Seg_1634" n="HIAT:w" s="T465">Rɨmnaj</ts>
                  <nts id="Seg_1635" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T467" id="Seg_1637" n="HIAT:w" s="T466">Kiri͡ehiger</ts>
                  <nts id="Seg_1638" n="HIAT:ip">.</nts>
                  <nts id="Seg_1639" n="HIAT:ip">"</nts>
                  <nts id="Seg_1640" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T472" id="Seg_1642" n="HIAT:u" s="T467">
                  <ts e="T468" id="Seg_1644" n="HIAT:w" s="T467">Kör</ts>
                  <nts id="Seg_1645" n="HIAT:ip">,</nts>
                  <nts id="Seg_1646" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T469" id="Seg_1648" n="HIAT:w" s="T468">ɨraːgɨn</ts>
                  <nts id="Seg_1649" n="HIAT:ip">,</nts>
                  <nts id="Seg_1650" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T470" id="Seg_1652" n="HIAT:w" s="T469">Rɨmnaj</ts>
                  <nts id="Seg_1653" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T471" id="Seg_1655" n="HIAT:w" s="T470">ɨnaraː</ts>
                  <nts id="Seg_1656" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T472" id="Seg_1658" n="HIAT:w" s="T471">öttüger</ts>
                  <nts id="Seg_1659" n="HIAT:ip">.</nts>
                  <nts id="Seg_1660" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T510" id="Seg_1661" n="sc" s="T476">
               <ts e="T478" id="Seg_1663" n="HIAT:u" s="T476">
                  <ts e="T478" id="Seg_1665" n="HIAT:w" s="T476">Hu͡ok</ts>
                  <nts id="Seg_1666" n="HIAT:ip">.</nts>
                  <nts id="Seg_1667" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T482" id="Seg_1669" n="HIAT:u" s="T478">
                  <ts e="T479" id="Seg_1671" n="HIAT:w" s="T478">Ol</ts>
                  <nts id="Seg_1672" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T480" id="Seg_1674" n="HIAT:w" s="T479">bejelerin</ts>
                  <nts id="Seg_1675" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T482" id="Seg_1677" n="HIAT:w" s="T480">gi͡ettere</ts>
                  <nts id="Seg_1678" n="HIAT:ip">.</nts>
                  <nts id="Seg_1679" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T496" id="Seg_1681" n="HIAT:u" s="T482">
                  <ts e="T485" id="Seg_1683" n="HIAT:w" s="T482">Heː</ts>
                  <nts id="Seg_1684" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T486" id="Seg_1686" n="HIAT:w" s="T485">Bi͡egičeptere</ts>
                  <nts id="Seg_1687" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T487" id="Seg_1689" n="HIAT:w" s="T486">honon</ts>
                  <nts id="Seg_1690" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T488" id="Seg_1692" n="HIAT:w" s="T487">ajannaːbɨt</ts>
                  <nts id="Seg_1693" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T489" id="Seg_1695" n="HIAT:w" s="T488">bu͡olla</ts>
                  <nts id="Seg_1696" n="HIAT:ip">,</nts>
                  <nts id="Seg_1697" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T490" id="Seg_1699" n="HIAT:w" s="T489">ol</ts>
                  <nts id="Seg_1700" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T491" id="Seg_1702" n="HIAT:w" s="T490">barɨːtɨnan</ts>
                  <nts id="Seg_1703" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T492" id="Seg_1705" n="HIAT:w" s="T491">barbɨt</ts>
                  <nts id="Seg_1706" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T493" id="Seg_1708" n="HIAT:w" s="T492">bu</ts>
                  <nts id="Seg_1709" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T494" id="Seg_1711" n="HIAT:w" s="T493">di͡ek</ts>
                  <nts id="Seg_1712" n="HIAT:ip">,</nts>
                  <nts id="Seg_1713" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T495" id="Seg_1715" n="HIAT:w" s="T494">üːhe</ts>
                  <nts id="Seg_1716" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T496" id="Seg_1718" n="HIAT:w" s="T495">di͡ek</ts>
                  <nts id="Seg_1719" n="HIAT:ip">.</nts>
                  <nts id="Seg_1720" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T510" id="Seg_1722" n="HIAT:u" s="T496">
                  <ts e="T497" id="Seg_1724" n="HIAT:w" s="T496">Če</ts>
                  <nts id="Seg_1725" n="HIAT:ip">,</nts>
                  <nts id="Seg_1726" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T498" id="Seg_1728" n="HIAT:w" s="T497">ol</ts>
                  <nts id="Seg_1729" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T499" id="Seg_1731" n="HIAT:w" s="T498">bu͡o</ts>
                  <nts id="Seg_1732" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T500" id="Seg_1734" n="HIAT:w" s="T499">ol</ts>
                  <nts id="Seg_1735" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T501" id="Seg_1737" n="HIAT:w" s="T500">hurugu</ts>
                  <nts id="Seg_1738" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T502" id="Seg_1740" n="HIAT:w" s="T501">iltereller</ts>
                  <nts id="Seg_1741" n="HIAT:ip">,</nts>
                  <nts id="Seg_1742" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T503" id="Seg_1744" n="HIAT:w" s="T502">oččo</ts>
                  <nts id="Seg_1745" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T504" id="Seg_1747" n="HIAT:w" s="T503">huruktaːktar</ts>
                  <nts id="Seg_1748" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T505" id="Seg_1750" n="HIAT:w" s="T504">ebite</ts>
                  <nts id="Seg_1751" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T506" id="Seg_1753" n="HIAT:w" s="T505">bu͡olla</ts>
                  <nts id="Seg_1754" n="HIAT:ip">,</nts>
                  <nts id="Seg_1755" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T507" id="Seg_1757" n="HIAT:w" s="T506">tojonnoːktor</ts>
                  <nts id="Seg_1758" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T508" id="Seg_1760" n="HIAT:w" s="T507">bu͡o</ts>
                  <nts id="Seg_1761" n="HIAT:ip">,</nts>
                  <nts id="Seg_1762" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1763" n="HIAT:ip">(</nts>
                  <ts e="T510" id="Seg_1765" n="HIAT:w" s="T508">ogonnʼ-</ts>
                  <nts id="Seg_1766" n="HIAT:ip">)</nts>
                  <nts id="Seg_1767" n="HIAT:ip">…</nts>
                  <nts id="Seg_1768" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T648" id="Seg_1769" n="sc" s="T511">
               <ts e="T512" id="Seg_1771" n="HIAT:u" s="T511">
                  <ts e="T512" id="Seg_1773" n="HIAT:w" s="T511">Hakalar</ts>
                  <nts id="Seg_1774" n="HIAT:ip">.</nts>
                  <nts id="Seg_1775" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T516" id="Seg_1777" n="HIAT:u" s="T512">
                  <ts e="T513" id="Seg_1779" n="HIAT:w" s="T512">Ol</ts>
                  <nts id="Seg_1780" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T514" id="Seg_1782" n="HIAT:w" s="T513">ülehittere</ts>
                  <nts id="Seg_1783" n="HIAT:ip">,</nts>
                  <nts id="Seg_1784" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T515" id="Seg_1786" n="HIAT:w" s="T514">habi͡ettara</ts>
                  <nts id="Seg_1787" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T516" id="Seg_1789" n="HIAT:w" s="T515">tu͡oktara</ts>
                  <nts id="Seg_1790" n="HIAT:ip">.</nts>
                  <nts id="Seg_1791" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T519" id="Seg_1793" n="HIAT:u" s="T516">
                  <ts e="T517" id="Seg_1795" n="HIAT:w" s="T516">Huruk</ts>
                  <nts id="Seg_1796" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T518" id="Seg_1798" n="HIAT:w" s="T517">ɨːtallar</ts>
                  <nts id="Seg_1799" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T519" id="Seg_1801" n="HIAT:w" s="T518">bu͡o</ts>
                  <nts id="Seg_1802" n="HIAT:ip">.</nts>
                  <nts id="Seg_1803" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T527" id="Seg_1805" n="HIAT:u" s="T519">
                  <ts e="T520" id="Seg_1807" n="HIAT:w" s="T519">Innʼe</ts>
                  <nts id="Seg_1808" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1809" n="HIAT:ip">(</nts>
                  <ts e="T521" id="Seg_1811" n="HIAT:w" s="T520">gɨn-</ts>
                  <nts id="Seg_1812" n="HIAT:ip">)</nts>
                  <nts id="Seg_1813" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T522" id="Seg_1815" n="HIAT:w" s="T521">bu</ts>
                  <nts id="Seg_1816" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T523" id="Seg_1818" n="HIAT:w" s="T522">ogonnʼor</ts>
                  <nts id="Seg_1819" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T524" id="Seg_1821" n="HIAT:w" s="T523">bu͡olla</ts>
                  <nts id="Seg_1822" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T525" id="Seg_1824" n="HIAT:w" s="T524">dʼe</ts>
                  <nts id="Seg_1825" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T526" id="Seg_1827" n="HIAT:w" s="T525">ki͡ehe</ts>
                  <nts id="Seg_1828" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T527" id="Seg_1830" n="HIAT:w" s="T526">kamnɨːr</ts>
                  <nts id="Seg_1831" n="HIAT:ip">.</nts>
                  <nts id="Seg_1832" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T536" id="Seg_1834" n="HIAT:u" s="T527">
                  <nts id="Seg_1835" n="HIAT:ip">"</nts>
                  <ts e="T528" id="Seg_1837" n="HIAT:w" s="T527">Harsi͡erda</ts>
                  <nts id="Seg_1838" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T529" id="Seg_1840" n="HIAT:w" s="T528">erdehit</ts>
                  <nts id="Seg_1841" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T530" id="Seg_1843" n="HIAT:w" s="T529">dʼaktar</ts>
                  <nts id="Seg_1844" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T531" id="Seg_1846" n="HIAT:w" s="T530">turar</ts>
                  <nts id="Seg_1847" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T532" id="Seg_1849" n="HIAT:w" s="T531">kemiger</ts>
                  <nts id="Seg_1850" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T533" id="Seg_1852" n="HIAT:w" s="T532">keli͡em</ts>
                  <nts id="Seg_1853" n="HIAT:ip">"</nts>
                  <nts id="Seg_1854" n="HIAT:ip">,</nts>
                  <nts id="Seg_1855" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T534" id="Seg_1857" n="HIAT:w" s="T533">di͡ebit</ts>
                  <nts id="Seg_1858" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T535" id="Seg_1860" n="HIAT:w" s="T534">bu</ts>
                  <nts id="Seg_1861" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T536" id="Seg_1863" n="HIAT:w" s="T535">ogonnʼor</ts>
                  <nts id="Seg_1864" n="HIAT:ip">.</nts>
                  <nts id="Seg_1865" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T547" id="Seg_1867" n="HIAT:u" s="T536">
                  <nts id="Seg_1868" n="HIAT:ip">"</nts>
                  <ts e="T537" id="Seg_1870" n="HIAT:w" s="T536">O</ts>
                  <nts id="Seg_1871" n="HIAT:ip">,</nts>
                  <nts id="Seg_1872" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T538" id="Seg_1874" n="HIAT:w" s="T537">kajdak</ts>
                  <nts id="Seg_1875" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T539" id="Seg_1877" n="HIAT:w" s="T538">gɨnan</ts>
                  <nts id="Seg_1878" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T540" id="Seg_1880" n="HIAT:w" s="T539">keleːri</ts>
                  <nts id="Seg_1881" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T541" id="Seg_1883" n="HIAT:w" s="T540">gɨnagɨn</ts>
                  <nts id="Seg_1884" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T542" id="Seg_1886" n="HIAT:w" s="T541">Rɨmnaj</ts>
                  <nts id="Seg_1887" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T543" id="Seg_1889" n="HIAT:w" s="T542">ɨnaːra</ts>
                  <nts id="Seg_1890" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T544" id="Seg_1892" n="HIAT:w" s="T543">öttütten</ts>
                  <nts id="Seg_1893" n="HIAT:ip">,</nts>
                  <nts id="Seg_1894" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T545" id="Seg_1896" n="HIAT:w" s="T544">hürdeːk</ts>
                  <nts id="Seg_1897" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T546" id="Seg_1899" n="HIAT:w" s="T545">ɨraːk</ts>
                  <nts id="Seg_1900" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T547" id="Seg_1902" n="HIAT:w" s="T546">Pobügejten</ts>
                  <nts id="Seg_1903" n="HIAT:ip">.</nts>
                  <nts id="Seg_1904" n="HIAT:ip">"</nts>
                  <nts id="Seg_1905" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T556" id="Seg_1907" n="HIAT:u" s="T547">
                  <ts e="T548" id="Seg_1909" n="HIAT:w" s="T547">Dʼe</ts>
                  <nts id="Seg_1910" n="HIAT:ip">,</nts>
                  <nts id="Seg_1911" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T549" id="Seg_1913" n="HIAT:w" s="T548">bu</ts>
                  <nts id="Seg_1914" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T550" id="Seg_1916" n="HIAT:w" s="T549">ogonnʼor</ts>
                  <nts id="Seg_1917" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T551" id="Seg_1919" n="HIAT:w" s="T550">kaːman</ts>
                  <nts id="Seg_1920" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T552" id="Seg_1922" n="HIAT:w" s="T551">kaːlbɨt</ts>
                  <nts id="Seg_1923" n="HIAT:ip">,</nts>
                  <nts id="Seg_1924" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T553" id="Seg_1926" n="HIAT:w" s="T552">e</ts>
                  <nts id="Seg_1927" n="HIAT:ip">,</nts>
                  <nts id="Seg_1928" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T554" id="Seg_1930" n="HIAT:w" s="T553">baran</ts>
                  <nts id="Seg_1931" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T555" id="Seg_1933" n="HIAT:w" s="T554">kaːlbɨt</ts>
                  <nts id="Seg_1934" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T556" id="Seg_1936" n="HIAT:w" s="T555">tɨːnnan</ts>
                  <nts id="Seg_1937" n="HIAT:ip">.</nts>
                  <nts id="Seg_1938" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T576" id="Seg_1940" n="HIAT:u" s="T556">
                  <ts e="T557" id="Seg_1942" n="HIAT:w" s="T556">Harsi͡erda</ts>
                  <nts id="Seg_1943" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T558" id="Seg_1945" n="HIAT:w" s="T557">emeːksitter</ts>
                  <nts id="Seg_1946" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_1947" n="HIAT:ip">(</nts>
                  <ts e="T560" id="Seg_1949" n="HIAT:w" s="T558">turbu-</ts>
                  <nts id="Seg_1950" n="HIAT:ip">)</nts>
                  <nts id="Seg_1951" n="HIAT:ip">,</nts>
                  <nts id="Seg_1952" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T561" id="Seg_1954" n="HIAT:w" s="T560">erdehit</ts>
                  <nts id="Seg_1955" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T562" id="Seg_1957" n="HIAT:w" s="T561">dʼaktattar</ts>
                  <nts id="Seg_1958" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T563" id="Seg_1960" n="HIAT:w" s="T562">turallar</ts>
                  <nts id="Seg_1961" n="HIAT:ip">,</nts>
                  <nts id="Seg_1962" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T564" id="Seg_1964" n="HIAT:w" s="T563">naprimer</ts>
                  <nts id="Seg_1965" n="HIAT:ip">,</nts>
                  <nts id="Seg_1966" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T565" id="Seg_1968" n="HIAT:w" s="T564">oččo</ts>
                  <nts id="Seg_1969" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T566" id="Seg_1971" n="HIAT:w" s="T565">hette</ts>
                  <nts id="Seg_1972" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T567" id="Seg_1974" n="HIAT:w" s="T566">čaːs</ts>
                  <nts id="Seg_1975" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T568" id="Seg_1977" n="HIAT:w" s="T567">itte</ts>
                  <nts id="Seg_1978" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T569" id="Seg_1980" n="HIAT:w" s="T568">ebit</ts>
                  <nts id="Seg_1981" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T570" id="Seg_1983" n="HIAT:w" s="T569">eni</ts>
                  <nts id="Seg_1984" n="HIAT:ip">,</nts>
                  <nts id="Seg_1985" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T571" id="Seg_1987" n="HIAT:w" s="T570">tu͡ok</ts>
                  <nts id="Seg_1988" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T572" id="Seg_1990" n="HIAT:w" s="T571">čaːha</ts>
                  <nts id="Seg_1991" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T573" id="Seg_1993" n="HIAT:w" s="T572">keli͡ej</ts>
                  <nts id="Seg_1994" n="HIAT:ip">,</nts>
                  <nts id="Seg_1995" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T574" id="Seg_1997" n="HIAT:w" s="T573">künnerinen</ts>
                  <nts id="Seg_1998" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T575" id="Seg_2000" n="HIAT:w" s="T574">köröllör</ts>
                  <nts id="Seg_2001" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T576" id="Seg_2003" n="HIAT:w" s="T575">bu͡o</ts>
                  <nts id="Seg_2004" n="HIAT:ip">.</nts>
                  <nts id="Seg_2005" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T590" id="Seg_2007" n="HIAT:u" s="T576">
                  <ts e="T577" id="Seg_2009" n="HIAT:w" s="T576">Araː</ts>
                  <nts id="Seg_2010" n="HIAT:ip">,</nts>
                  <nts id="Seg_2011" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T578" id="Seg_2013" n="HIAT:w" s="T577">hette</ts>
                  <nts id="Seg_2014" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T579" id="Seg_2016" n="HIAT:w" s="T578">čaːska</ts>
                  <nts id="Seg_2017" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T580" id="Seg_2019" n="HIAT:w" s="T579">Čagɨdaj</ts>
                  <nts id="Seg_2020" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T581" id="Seg_2022" n="HIAT:w" s="T580">kelbit</ts>
                  <nts id="Seg_2023" n="HIAT:ip">,</nts>
                  <nts id="Seg_2024" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T582" id="Seg_2026" n="HIAT:w" s="T581">tɨːta</ts>
                  <nts id="Seg_2027" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T583" id="Seg_2029" n="HIAT:w" s="T582">umsana</ts>
                  <nts id="Seg_2030" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T584" id="Seg_2032" n="HIAT:w" s="T583">hɨtar</ts>
                  <nts id="Seg_2033" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T585" id="Seg_2035" n="HIAT:w" s="T584">diːn</ts>
                  <nts id="Seg_2036" n="HIAT:ip">,</nts>
                  <nts id="Seg_2037" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T586" id="Seg_2039" n="HIAT:w" s="T585">kör</ts>
                  <nts id="Seg_2040" n="HIAT:ip">,</nts>
                  <nts id="Seg_2041" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T587" id="Seg_2043" n="HIAT:w" s="T586">onnuk</ts>
                  <nts id="Seg_2044" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T588" id="Seg_2046" n="HIAT:w" s="T587">korsun</ts>
                  <nts id="Seg_2047" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T589" id="Seg_2049" n="HIAT:w" s="T588">kihiler</ts>
                  <nts id="Seg_2050" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T590" id="Seg_2052" n="HIAT:w" s="T589">baːllar</ts>
                  <nts id="Seg_2053" n="HIAT:ip">.</nts>
                  <nts id="Seg_2054" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T603" id="Seg_2056" n="HIAT:u" s="T590">
                  <ts e="T591" id="Seg_2058" n="HIAT:w" s="T590">Onton</ts>
                  <nts id="Seg_2059" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T592" id="Seg_2061" n="HIAT:w" s="T591">dʼaktar</ts>
                  <nts id="Seg_2062" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T593" id="Seg_2064" n="HIAT:w" s="T592">törüːr</ts>
                  <nts id="Seg_2065" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T594" id="Seg_2067" n="HIAT:w" s="T593">hol</ts>
                  <nts id="Seg_2068" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T595" id="Seg_2070" n="HIAT:w" s="T594">huːrtarɨgar</ts>
                  <nts id="Seg_2071" n="HIAT:ip">,</nts>
                  <nts id="Seg_2072" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T596" id="Seg_2074" n="HIAT:w" s="T595">bu</ts>
                  <nts id="Seg_2075" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T597" id="Seg_2077" n="HIAT:w" s="T596">dʼaktar</ts>
                  <nts id="Seg_2078" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T598" id="Seg_2080" n="HIAT:w" s="T597">ölöːrü</ts>
                  <nts id="Seg_2081" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T599" id="Seg_2083" n="HIAT:w" s="T598">gɨmmɨt</ts>
                  <nts id="Seg_2084" n="HIAT:ip">,</nts>
                  <nts id="Seg_2085" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T600" id="Seg_2087" n="HIAT:w" s="T599">kajdak</ts>
                  <nts id="Seg_2088" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T601" id="Seg_2090" n="HIAT:w" s="T600">da</ts>
                  <nts id="Seg_2091" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T602" id="Seg_2093" n="HIAT:w" s="T601">gɨnɨ͡aktarɨn</ts>
                  <nts id="Seg_2094" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T603" id="Seg_2096" n="HIAT:w" s="T602">bert</ts>
                  <nts id="Seg_2097" n="HIAT:ip">.</nts>
                  <nts id="Seg_2098" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T611" id="Seg_2100" n="HIAT:u" s="T603">
                  <ts e="T604" id="Seg_2102" n="HIAT:w" s="T603">Du͡oktura</ts>
                  <nts id="Seg_2103" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T605" id="Seg_2105" n="HIAT:w" s="T604">hu͡ok</ts>
                  <nts id="Seg_2106" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T606" id="Seg_2108" n="HIAT:w" s="T605">üjege</ts>
                  <nts id="Seg_2109" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T607" id="Seg_2111" n="HIAT:w" s="T606">ke</ts>
                  <nts id="Seg_2112" n="HIAT:ip">,</nts>
                  <nts id="Seg_2113" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T608" id="Seg_2115" n="HIAT:w" s="T607">baːbɨskalara</ts>
                  <nts id="Seg_2116" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T609" id="Seg_2118" n="HIAT:w" s="T608">muŋnaːk</ts>
                  <nts id="Seg_2119" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T610" id="Seg_2121" n="HIAT:w" s="T609">hɨlajan</ts>
                  <nts id="Seg_2122" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T611" id="Seg_2124" n="HIAT:w" s="T610">büppüt</ts>
                  <nts id="Seg_2125" n="HIAT:ip">.</nts>
                  <nts id="Seg_2126" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T623" id="Seg_2128" n="HIAT:u" s="T611">
                  <nts id="Seg_2129" n="HIAT:ip">"</nts>
                  <ts e="T612" id="Seg_2131" n="HIAT:w" s="T611">Kaja</ts>
                  <nts id="Seg_2132" n="HIAT:ip">,</nts>
                  <nts id="Seg_2133" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T613" id="Seg_2135" n="HIAT:w" s="T612">Čagɨdaj</ts>
                  <nts id="Seg_2136" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T614" id="Seg_2138" n="HIAT:w" s="T613">kellin</ts>
                  <nts id="Seg_2139" n="HIAT:ip">,</nts>
                  <nts id="Seg_2140" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T615" id="Seg_2142" n="HIAT:w" s="T614">dʼaktar</ts>
                  <nts id="Seg_2143" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T616" id="Seg_2145" n="HIAT:w" s="T615">ölöːrü</ts>
                  <nts id="Seg_2146" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T617" id="Seg_2148" n="HIAT:w" s="T616">gɨnna</ts>
                  <nts id="Seg_2149" n="HIAT:ip">,</nts>
                  <nts id="Seg_2150" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T618" id="Seg_2152" n="HIAT:w" s="T617">kajdak</ts>
                  <nts id="Seg_2153" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T619" id="Seg_2155" n="HIAT:w" s="T618">eme</ts>
                  <nts id="Seg_2156" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T620" id="Seg_2158" n="HIAT:w" s="T619">gɨnnɨn</ts>
                  <nts id="Seg_2159" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T621" id="Seg_2161" n="HIAT:w" s="T620">oŋu͡ok</ts>
                  <nts id="Seg_2162" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T622" id="Seg_2164" n="HIAT:w" s="T621">araːrdɨn</ts>
                  <nts id="Seg_2165" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T623" id="Seg_2167" n="HIAT:w" s="T622">munna</ts>
                  <nts id="Seg_2168" n="HIAT:ip">.</nts>
                  <nts id="Seg_2169" n="HIAT:ip">"</nts>
                  <nts id="Seg_2170" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T639" id="Seg_2172" n="HIAT:u" s="T623">
                  <ts e="T624" id="Seg_2174" n="HIAT:w" s="T623">Hinnʼe</ts>
                  <nts id="Seg_2175" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T625" id="Seg_2177" n="HIAT:w" s="T624">bu</ts>
                  <nts id="Seg_2178" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T626" id="Seg_2180" n="HIAT:w" s="T625">Čagɨdaj</ts>
                  <nts id="Seg_2181" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T627" id="Seg_2183" n="HIAT:w" s="T626">keler</ts>
                  <nts id="Seg_2184" n="HIAT:ip">,</nts>
                  <nts id="Seg_2185" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T628" id="Seg_2187" n="HIAT:w" s="T627">kajdak</ts>
                  <nts id="Seg_2188" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_2189" n="HIAT:ip">(</nts>
                  <ts e="T630" id="Seg_2191" n="HIAT:w" s="T628">gɨ-</ts>
                  <nts id="Seg_2192" n="HIAT:ip">)</nts>
                  <nts id="Seg_2193" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T631" id="Seg_2195" n="HIAT:w" s="T630">bu</ts>
                  <nts id="Seg_2196" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T632" id="Seg_2198" n="HIAT:w" s="T631">dʼaktarɨ</ts>
                  <nts id="Seg_2199" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T633" id="Seg_2201" n="HIAT:w" s="T632">iliːleːn</ts>
                  <nts id="Seg_2202" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T634" id="Seg_2204" n="HIAT:w" s="T633">ɨlar</ts>
                  <nts id="Seg_2205" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T635" id="Seg_2207" n="HIAT:w" s="T634">ogotun</ts>
                  <nts id="Seg_2208" n="HIAT:ip">,</nts>
                  <nts id="Seg_2209" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T636" id="Seg_2211" n="HIAT:w" s="T635">honon</ts>
                  <nts id="Seg_2212" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T637" id="Seg_2214" n="HIAT:w" s="T636">tɨːnnak</ts>
                  <nts id="Seg_2215" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T638" id="Seg_2217" n="HIAT:w" s="T637">keːher</ts>
                  <nts id="Seg_2218" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T639" id="Seg_2220" n="HIAT:w" s="T638">kör</ts>
                  <nts id="Seg_2221" n="HIAT:ip">.</nts>
                  <nts id="Seg_2222" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T643" id="Seg_2224" n="HIAT:u" s="T639">
                  <ts e="T640" id="Seg_2226" n="HIAT:w" s="T639">Onnuktar</ts>
                  <nts id="Seg_2227" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T641" id="Seg_2229" n="HIAT:w" s="T640">baːr</ts>
                  <nts id="Seg_2230" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T642" id="Seg_2232" n="HIAT:w" s="T641">etiler</ts>
                  <nts id="Seg_2233" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T643" id="Seg_2235" n="HIAT:w" s="T642">kɨrdʼagastar</ts>
                  <nts id="Seg_2236" n="HIAT:ip">.</nts>
                  <nts id="Seg_2237" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T648" id="Seg_2239" n="HIAT:u" s="T643">
                  <ts e="T644" id="Seg_2241" n="HIAT:w" s="T643">I</ts>
                  <nts id="Seg_2242" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T645" id="Seg_2244" n="HIAT:w" s="T644">Čakɨːla</ts>
                  <nts id="Seg_2245" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T646" id="Seg_2247" n="HIAT:w" s="T645">ogonnʼor</ts>
                  <nts id="Seg_2248" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T647" id="Seg_2250" n="HIAT:w" s="T646">ehete</ts>
                  <nts id="Seg_2251" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T648" id="Seg_2253" n="HIAT:w" s="T647">ontuŋ</ts>
                  <nts id="Seg_2254" n="HIAT:ip">.</nts>
                  <nts id="Seg_2255" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T673" id="Seg_2256" n="sc" s="T651">
               <ts e="T664" id="Seg_2258" n="HIAT:u" s="T651">
                  <ts e="T652" id="Seg_2260" n="HIAT:w" s="T651">Heː</ts>
                  <nts id="Seg_2261" n="HIAT:ip">,</nts>
                  <nts id="Seg_2262" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T653" id="Seg_2264" n="HIAT:w" s="T652">oččogo</ts>
                  <nts id="Seg_2265" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T654" id="Seg_2267" n="HIAT:w" s="T653">ol</ts>
                  <nts id="Seg_2268" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T655" id="Seg_2270" n="HIAT:w" s="T654">Čagɨdajɨŋ</ts>
                  <nts id="Seg_2271" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T656" id="Seg_2273" n="HIAT:w" s="T655">dʼaktara</ts>
                  <nts id="Seg_2274" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T657" id="Seg_2276" n="HIAT:w" s="T656">bu͡ollagɨna</ts>
                  <nts id="Seg_2277" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T658" id="Seg_2279" n="HIAT:w" s="T657">emeːksin</ts>
                  <nts id="Seg_2280" n="HIAT:ip">,</nts>
                  <nts id="Seg_2281" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T659" id="Seg_2283" n="HIAT:w" s="T658">Anna</ts>
                  <nts id="Seg_2284" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T660" id="Seg_2286" n="HIAT:w" s="T659">inʼete</ts>
                  <nts id="Seg_2287" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T661" id="Seg_2289" n="HIAT:w" s="T660">emeːksin</ts>
                  <nts id="Seg_2290" n="HIAT:ip">,</nts>
                  <nts id="Seg_2291" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T662" id="Seg_2293" n="HIAT:w" s="T661">baː</ts>
                  <nts id="Seg_2294" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T663" id="Seg_2296" n="HIAT:w" s="T662">Čeːke</ts>
                  <nts id="Seg_2297" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T664" id="Seg_2299" n="HIAT:w" s="T663">emeːksin</ts>
                  <nts id="Seg_2300" n="HIAT:ip">.</nts>
                  <nts id="Seg_2301" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
               <ts e="T673" id="Seg_2303" n="HIAT:u" s="T664">
                  <ts e="T665" id="Seg_2305" n="HIAT:w" s="T664">Ontuŋ</ts>
                  <nts id="Seg_2306" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T666" id="Seg_2308" n="HIAT:w" s="T665">teːtete</ts>
                  <nts id="Seg_2309" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T667" id="Seg_2311" n="HIAT:w" s="T666">bihigi</ts>
                  <nts id="Seg_2312" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T668" id="Seg_2314" n="HIAT:w" s="T667">eheːbitin</ts>
                  <nts id="Seg_2315" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T669" id="Seg_2317" n="HIAT:w" s="T668">kɨtta</ts>
                  <nts id="Seg_2318" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T670" id="Seg_2320" n="HIAT:w" s="T669">biːrge</ts>
                  <nts id="Seg_2321" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T671" id="Seg_2323" n="HIAT:w" s="T670">töröːbütter</ts>
                  <nts id="Seg_2324" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T672" id="Seg_2326" n="HIAT:w" s="T671">Kudrʼakop</ts>
                  <nts id="Seg_2327" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T673" id="Seg_2329" n="HIAT:w" s="T672">ogonnʼor</ts>
                  <nts id="Seg_2330" n="HIAT:ip">.</nts>
                  <nts id="Seg_2331" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KiPP">
            <ts e="T194" id="Seg_2332" n="sc" s="T0">
               <ts e="T1" id="Seg_2334" n="e" s="T0">Bi͡egičebi </ts>
               <ts e="T2" id="Seg_2336" n="e" s="T1">illeller. </ts>
               <ts e="T4" id="Seg_2338" n="e" s="T2">(Hit-) </ts>
               <ts e="T5" id="Seg_2340" n="e" s="T4">hirinen </ts>
               <ts e="T6" id="Seg_2342" n="e" s="T5">hɨldʼar </ts>
               <ts e="T7" id="Seg_2344" n="e" s="T6">ete </ts>
               <ts e="T8" id="Seg_2346" n="e" s="T7">urut, </ts>
               <ts e="T9" id="Seg_2348" n="e" s="T8">espʼidʼisʼijalar. </ts>
               <ts e="T10" id="Seg_2350" n="e" s="T9">Onu </ts>
               <ts e="T11" id="Seg_2352" n="e" s="T10">ehem </ts>
               <ts e="T12" id="Seg_2354" n="e" s="T11">kepseːčči. </ts>
               <ts e="T13" id="Seg_2356" n="e" s="T12">Mini͡ene </ts>
               <ts e="T14" id="Seg_2358" n="e" s="T13">inʼem, </ts>
               <ts e="T15" id="Seg_2360" n="e" s="T14">(ehe) </ts>
               <ts e="T16" id="Seg_2362" n="e" s="T15">iti </ts>
               <ts e="T17" id="Seg_2364" n="e" s="T16">inʼem </ts>
               <ts e="T18" id="Seg_2366" n="e" s="T17">uruːlara, </ts>
               <ts e="T19" id="Seg_2368" n="e" s="T18">Markʼel </ts>
               <ts e="T20" id="Seg_2370" n="e" s="T19">ogonnʼor, </ts>
               <ts e="T21" id="Seg_2372" n="e" s="T20">Ölöːnö </ts>
               <ts e="T22" id="Seg_2374" n="e" s="T21">agata </ts>
               <ts e="T23" id="Seg_2376" n="e" s="T22">ogonnʼor. </ts>
               <ts e="T24" id="Seg_2378" n="e" s="T23">Olor </ts>
               <ts e="T25" id="Seg_2380" n="e" s="T24">kepsetellerin </ts>
               <ts e="T26" id="Seg_2382" n="e" s="T25">isteːččibin. </ts>
               <ts e="T27" id="Seg_2384" n="e" s="T26">Bu </ts>
               <ts e="T28" id="Seg_2386" n="e" s="T27">Bi͡egičep </ts>
               <ts e="T29" id="Seg_2388" n="e" s="T28">bu͡ollagɨna </ts>
               <ts e="T30" id="Seg_2390" n="e" s="T29">tabannan </ts>
               <ts e="T31" id="Seg_2392" n="e" s="T30">ajannɨːr </ts>
               <ts e="T32" id="Seg_2394" n="e" s="T31">Bi͡egičep </ts>
               <ts e="T33" id="Seg_2396" n="e" s="T32">arɨːtɨgar, </ts>
               <ts e="T34" id="Seg_2398" n="e" s="T33">hette </ts>
               <ts e="T35" id="Seg_2400" n="e" s="T34">kihileːk. </ts>
               <ts e="T36" id="Seg_2402" n="e" s="T35">Bu </ts>
               <ts e="T37" id="Seg_2404" n="e" s="T36">kihi </ts>
               <ts e="T38" id="Seg_2406" n="e" s="T37">ajannaːn </ts>
               <ts e="T39" id="Seg_2408" n="e" s="T38">ihen, </ts>
               <ts e="T40" id="Seg_2410" n="e" s="T39">tabannan </ts>
               <ts e="T41" id="Seg_2412" n="e" s="T40">illeller </ts>
               <ts e="T42" id="Seg_2414" n="e" s="T41">bu͡o, </ts>
               <ts e="T43" id="Seg_2416" n="e" s="T42">atɨntan-atɨn </ts>
               <ts e="T44" id="Seg_2418" n="e" s="T43">tabannan </ts>
               <ts e="T45" id="Seg_2420" n="e" s="T44">illeller, </ts>
               <ts e="T46" id="Seg_2422" n="e" s="T45">haŋattan-haŋa </ts>
               <ts e="T47" id="Seg_2424" n="e" s="T46">tabannan </ts>
               <ts e="T48" id="Seg_2426" n="e" s="T47">illeller. </ts>
               <ts e="T49" id="Seg_2428" n="e" s="T48">Onno </ts>
               <ts e="T50" id="Seg_2430" n="e" s="T49">Čagɨdaj </ts>
               <ts e="T51" id="Seg_2432" n="e" s="T50">ogonnʼoru </ts>
               <ts e="T52" id="Seg_2434" n="e" s="T51">iltereller </ts>
               <ts e="T53" id="Seg_2436" n="e" s="T52">bu͡olla, </ts>
               <ts e="T54" id="Seg_2438" n="e" s="T53">min </ts>
               <ts e="T55" id="Seg_2440" n="e" s="T54">ogonnʼorum </ts>
               <ts e="T56" id="Seg_2442" n="e" s="T55">ehetineːk, </ts>
               <ts e="T57" id="Seg_2444" n="e" s="T56">ehelere. </ts>
               <ts e="T58" id="Seg_2446" n="e" s="T57">Ol </ts>
               <ts e="T59" id="Seg_2448" n="e" s="T58">ogonnʼor </ts>
               <ts e="T60" id="Seg_2450" n="e" s="T59">tu͡ok </ts>
               <ts e="T61" id="Seg_2452" n="e" s="T60">daː </ts>
               <ts e="T62" id="Seg_2454" n="e" s="T61">hippetek </ts>
               <ts e="T63" id="Seg_2456" n="e" s="T62">kihite </ts>
               <ts e="T64" id="Seg_2458" n="e" s="T63">ete – </ts>
               <ts e="T65" id="Seg_2460" n="e" s="T64">hürdeːk </ts>
               <ts e="T66" id="Seg_2462" n="e" s="T65">kamnastaːk </ts>
               <ts e="T67" id="Seg_2464" n="e" s="T66">kihi. </ts>
               <ts e="T68" id="Seg_2466" n="e" s="T67">Ol </ts>
               <ts e="T69" id="Seg_2468" n="e" s="T68">(ogonnʼor-) </ts>
               <ts e="T70" id="Seg_2470" n="e" s="T69">hataːbat </ts>
               <ts e="T71" id="Seg_2472" n="e" s="T70">tabalarɨ </ts>
               <ts e="T72" id="Seg_2474" n="e" s="T71">kölütteren </ts>
               <ts e="T73" id="Seg_2476" n="e" s="T72">baraːn, </ts>
               <ts e="T74" id="Seg_2478" n="e" s="T73">Bi͡egičep </ts>
               <ts e="T75" id="Seg_2480" n="e" s="T74">arɨːtɨgar </ts>
               <ts e="T76" id="Seg_2482" n="e" s="T75">iltereller </ts>
               <ts e="T77" id="Seg_2484" n="e" s="T76">bu͡o. </ts>
               <ts e="T78" id="Seg_2486" n="e" s="T77">Onno </ts>
               <ts e="T79" id="Seg_2488" n="e" s="T78">tabalar </ts>
               <ts e="T80" id="Seg_2490" n="e" s="T79">möŋöllör </ts>
               <ts e="T81" id="Seg_2492" n="e" s="T80">ühü, </ts>
               <ts e="T82" id="Seg_2494" n="e" s="T81">i͡e </ts>
               <ts e="T83" id="Seg_2496" n="e" s="T82">diː. </ts>
               <ts e="T84" id="Seg_2498" n="e" s="T83">Tabalar </ts>
               <ts e="T85" id="Seg_2500" n="e" s="T84">möŋöllör, </ts>
               <ts e="T86" id="Seg_2502" n="e" s="T85">bu </ts>
               <ts e="T87" id="Seg_2504" n="e" s="T86">kihi </ts>
               <ts e="T88" id="Seg_2506" n="e" s="T87">bergehetin, </ts>
               <ts e="T89" id="Seg_2508" n="e" s="T88">ol </ts>
               <ts e="T90" id="Seg_2510" n="e" s="T89">Bi͡egičep </ts>
               <ts e="T91" id="Seg_2512" n="e" s="T90">gi͡enin </ts>
               <ts e="T92" id="Seg_2514" n="e" s="T91">bergehetin </ts>
               <ts e="T93" id="Seg_2516" n="e" s="T92">taba </ts>
               <ts e="T94" id="Seg_2518" n="e" s="T93">iːlen </ts>
               <ts e="T95" id="Seg_2520" n="e" s="T94">bɨragar </ts>
               <ts e="T96" id="Seg_2522" n="e" s="T95">ühü. </ts>
               <ts e="T97" id="Seg_2524" n="e" s="T96">Bu </ts>
               <ts e="T98" id="Seg_2526" n="e" s="T97">hɨrganɨ </ts>
               <ts e="T99" id="Seg_2528" n="e" s="T98">kurdarɨ </ts>
               <ts e="T100" id="Seg_2530" n="e" s="T99">bergehe </ts>
               <ts e="T101" id="Seg_2532" n="e" s="T100">tühen </ts>
               <ts e="T102" id="Seg_2534" n="e" s="T101">erdegine, </ts>
               <ts e="T103" id="Seg_2536" n="e" s="T102">onu </ts>
               <ts e="T104" id="Seg_2538" n="e" s="T103">ogonnʼor </ts>
               <ts e="T105" id="Seg_2540" n="e" s="T104">hɨrganɨ </ts>
               <ts e="T106" id="Seg_2542" n="e" s="T105">kurdarɨ </ts>
               <ts e="T107" id="Seg_2544" n="e" s="T106">ojon, </ts>
               <ts e="T108" id="Seg_2546" n="e" s="T107">kaban </ts>
               <ts e="T109" id="Seg_2548" n="e" s="T108">ɨlbɨt </ts>
               <ts e="T110" id="Seg_2550" n="e" s="T109">bu͡o, </ts>
               <ts e="T111" id="Seg_2552" n="e" s="T110">hirge </ts>
               <ts e="T112" id="Seg_2554" n="e" s="T111">tühü͡ögün </ts>
               <ts e="T113" id="Seg_2556" n="e" s="T112">betereː </ts>
               <ts e="T114" id="Seg_2558" n="e" s="T113">öttüger. </ts>
               <ts e="T115" id="Seg_2560" n="e" s="T114">Onu </ts>
               <ts e="T116" id="Seg_2562" n="e" s="T115">ol </ts>
               <ts e="T118" id="Seg_2564" n="e" s="T116">(ogonnʼ-) </ts>
               <ts e="T119" id="Seg_2566" n="e" s="T118">Bi͡egičebe </ts>
               <ts e="T120" id="Seg_2568" n="e" s="T119">diːr </ts>
               <ts e="T121" id="Seg_2570" n="e" s="T120">ühü: </ts>
               <ts e="T122" id="Seg_2572" n="e" s="T121">"Oː, </ts>
               <ts e="T123" id="Seg_2574" n="e" s="T122">haka </ts>
               <ts e="T124" id="Seg_2576" n="e" s="T123">hiriger </ts>
               <ts e="T125" id="Seg_2578" n="e" s="T124">da </ts>
               <ts e="T126" id="Seg_2580" n="e" s="T125">kamnastaːk </ts>
               <ts e="T127" id="Seg_2582" n="e" s="T126">kihi </ts>
               <ts e="T128" id="Seg_2584" n="e" s="T127">baːr </ts>
               <ts e="T129" id="Seg_2586" n="e" s="T128">bu͡olar </ts>
               <ts e="T130" id="Seg_2588" n="e" s="T129">((…))." </ts>
               <ts e="T131" id="Seg_2590" n="e" s="T130">Dʼe, </ts>
               <ts e="T132" id="Seg_2592" n="e" s="T131">bu </ts>
               <ts e="T133" id="Seg_2594" n="e" s="T132">ogonnʼor, </ts>
               <ts e="T134" id="Seg_2596" n="e" s="T133">Bi͡egičep </ts>
               <ts e="T135" id="Seg_2598" n="e" s="T134">bu͡ollagɨna </ts>
               <ts e="T136" id="Seg_2600" n="e" s="T135">ol, </ts>
               <ts e="T137" id="Seg_2602" n="e" s="T136">"ol </ts>
               <ts e="T138" id="Seg_2604" n="e" s="T137">arɨːga </ts>
               <ts e="T139" id="Seg_2606" n="e" s="T138">tiːjbitim", </ts>
               <ts e="T140" id="Seg_2608" n="e" s="T139">di͡en </ts>
               <ts e="T141" id="Seg_2610" n="e" s="T140">kepsiːr </ts>
               <ts e="T142" id="Seg_2612" n="e" s="T141">ühü. </ts>
               <ts e="T143" id="Seg_2614" n="e" s="T142">"Bu </ts>
               <ts e="T144" id="Seg_2616" n="e" s="T143">tu͡ok </ts>
               <ts e="T145" id="Seg_2618" n="e" s="T144">oduːnu </ts>
               <ts e="T146" id="Seg_2620" n="e" s="T145">kördüŋ </ts>
               <ts e="T147" id="Seg_2622" n="e" s="T146">onno", </ts>
               <ts e="T148" id="Seg_2624" n="e" s="T147">di͡en </ts>
               <ts e="T149" id="Seg_2626" n="e" s="T148">ɨjɨtallar </ts>
               <ts e="T150" id="Seg_2628" n="e" s="T149">hakalar, </ts>
               <ts e="T151" id="Seg_2630" n="e" s="T150">i͡e </ts>
               <ts e="T152" id="Seg_2632" n="e" s="T151">di. </ts>
               <ts e="T153" id="Seg_2634" n="e" s="T152">Onuga: </ts>
               <ts e="T154" id="Seg_2636" n="e" s="T153">"Min </ts>
               <ts e="T155" id="Seg_2638" n="e" s="T154">oduːnu </ts>
               <ts e="T156" id="Seg_2640" n="e" s="T155">tugu </ts>
               <ts e="T157" id="Seg_2642" n="e" s="T156">da </ts>
               <ts e="T158" id="Seg_2644" n="e" s="T157">olus </ts>
               <ts e="T159" id="Seg_2646" n="e" s="T158">bulbatɨm, </ts>
               <ts e="T160" id="Seg_2648" n="e" s="T159">marsalara </ts>
               <ts e="T161" id="Seg_2650" n="e" s="T160">bu͡ollagɨna </ts>
               <ts e="T162" id="Seg_2652" n="e" s="T161">tiːsteritten </ts>
               <ts e="T163" id="Seg_2654" n="e" s="T162">baːjan </ts>
               <ts e="T164" id="Seg_2656" n="e" s="T163">baran </ts>
               <ts e="T165" id="Seg_2658" n="e" s="T164">utujallar, </ts>
               <ts e="T166" id="Seg_2660" n="e" s="T165">utuja </ts>
               <ts e="T167" id="Seg_2662" n="e" s="T166">hɨtallarɨn </ts>
               <ts e="T168" id="Seg_2664" n="e" s="T167">baːjan </ts>
               <ts e="T169" id="Seg_2666" n="e" s="T168">bremnoːttan. </ts>
               <ts e="T170" id="Seg_2668" n="e" s="T169">Onu </ts>
               <ts e="T171" id="Seg_2670" n="e" s="T170">hep </ts>
               <ts e="T172" id="Seg_2672" n="e" s="T171">daː </ts>
               <ts e="T173" id="Seg_2674" n="e" s="T172">di͡ebetter, </ts>
               <ts e="T174" id="Seg_2676" n="e" s="T173">hagastaːn </ts>
               <ts e="T175" id="Seg_2678" n="e" s="T174">kiːllereller, </ts>
               <ts e="T176" id="Seg_2680" n="e" s="T175">uːga </ts>
               <ts e="T177" id="Seg_2682" n="e" s="T176">kiːlleren </ts>
               <ts e="T178" id="Seg_2684" n="e" s="T177">keːheller. </ts>
               <ts e="T179" id="Seg_2686" n="e" s="T178">Onton </ts>
               <ts e="T180" id="Seg_2688" n="e" s="T179">araj </ts>
               <ts e="T181" id="Seg_2690" n="e" s="T180">arɨːga </ts>
               <ts e="T182" id="Seg_2692" n="e" s="T181">taːs </ts>
               <ts e="T183" id="Seg_2694" n="e" s="T182">kajaga </ts>
               <ts e="T184" id="Seg_2696" n="e" s="T183">bullum", </ts>
               <ts e="T185" id="Seg_2698" n="e" s="T184">diːr </ts>
               <ts e="T186" id="Seg_2700" n="e" s="T185">ühü, </ts>
               <ts e="T187" id="Seg_2702" n="e" s="T186">"biːr </ts>
               <ts e="T188" id="Seg_2704" n="e" s="T187">oduːnu." </ts>
               <ts e="T189" id="Seg_2706" n="e" s="T188">"Dʼe, </ts>
               <ts e="T190" id="Seg_2708" n="e" s="T189">tugu </ts>
               <ts e="T191" id="Seg_2710" n="e" s="T190">bulluŋ?" </ts>
               <ts e="T192" id="Seg_2712" n="e" s="T191">Onu </ts>
               <ts e="T193" id="Seg_2714" n="e" s="T192">diːr </ts>
               <ts e="T194" id="Seg_2716" n="e" s="T193">bu͡o: </ts>
            </ts>
            <ts e="T472" id="Seg_2717" n="sc" s="T209">
               <ts e="T210" id="Seg_2719" n="e" s="T209">"Onuga </ts>
               <ts e="T211" id="Seg_2721" n="e" s="T210">kiːren </ts>
               <ts e="T212" id="Seg_2723" n="e" s="T211">bardɨm", </ts>
               <ts e="T213" id="Seg_2725" n="e" s="T212">diːr. </ts>
               <ts e="T214" id="Seg_2727" n="e" s="T213">Üs </ts>
               <ts e="T215" id="Seg_2729" n="e" s="T214">kihileːk, </ts>
               <ts e="T216" id="Seg_2731" n="e" s="T215">bu </ts>
               <ts e="T217" id="Seg_2733" n="e" s="T216">kihiler </ts>
               <ts e="T218" id="Seg_2735" n="e" s="T217">aha </ts>
               <ts e="T219" id="Seg_2737" n="e" s="T218">hu͡ok </ts>
               <ts e="T220" id="Seg_2739" n="e" s="T219">bu͡olbupput. </ts>
               <ts e="T221" id="Seg_2741" n="e" s="T220">Oččogo </ts>
               <ts e="T222" id="Seg_2743" n="e" s="T221">bu͡ollagɨna </ts>
               <ts e="T223" id="Seg_2745" n="e" s="T222">ebetten, </ts>
               <ts e="T224" id="Seg_2747" n="e" s="T223">o, </ts>
               <ts e="T225" id="Seg_2749" n="e" s="T224">kɨra </ts>
               <ts e="T226" id="Seg_2751" n="e" s="T225">balɨkkaːnɨ </ts>
               <ts e="T227" id="Seg_2753" n="e" s="T226">kabɨnannar </ts>
               <ts e="T228" id="Seg_2755" n="e" s="T227">ol </ts>
               <ts e="T229" id="Seg_2757" n="e" s="T228">aːŋŋa </ts>
               <ts e="T230" id="Seg_2759" n="e" s="T229">kiːrenner, </ts>
               <ts e="T231" id="Seg_2761" n="e" s="T230">ahaːrɨlar </ts>
               <ts e="T232" id="Seg_2763" n="e" s="T231">kiːrbitter. </ts>
               <ts e="T233" id="Seg_2765" n="e" s="T232">Bu </ts>
               <ts e="T234" id="Seg_2767" n="e" s="T233">kihi </ts>
               <ts e="T235" id="Seg_2769" n="e" s="T234">araj </ts>
               <ts e="T236" id="Seg_2771" n="e" s="T235">kiːrbite, </ts>
               <ts e="T237" id="Seg_2773" n="e" s="T236">bu </ts>
               <ts e="T238" id="Seg_2775" n="e" s="T237">kihi </ts>
               <ts e="T239" id="Seg_2777" n="e" s="T238">oloror. </ts>
               <ts e="T240" id="Seg_2779" n="e" s="T239">Olordoguna </ts>
               <ts e="T241" id="Seg_2781" n="e" s="T240">tu͡ok </ts>
               <ts e="T242" id="Seg_2783" n="e" s="T241">ere </ts>
               <ts e="T243" id="Seg_2785" n="e" s="T242">kihite </ts>
               <ts e="T244" id="Seg_2787" n="e" s="T243">kelbit, </ts>
               <ts e="T245" id="Seg_2789" n="e" s="T244">i͡e </ts>
               <ts e="T246" id="Seg_2791" n="e" s="T245">diː. </ts>
               <ts e="T247" id="Seg_2793" n="e" s="T246">Kihi </ts>
               <ts e="T248" id="Seg_2795" n="e" s="T247">duː, </ts>
               <ts e="T249" id="Seg_2797" n="e" s="T248">tu͡ok </ts>
               <ts e="T250" id="Seg_2799" n="e" s="T249">duː? </ts>
               <ts e="T251" id="Seg_2801" n="e" s="T250">"Hannɨttan </ts>
               <ts e="T252" id="Seg_2803" n="e" s="T251">menʼiːte </ts>
               <ts e="T253" id="Seg_2805" n="e" s="T252">hu͡ok </ts>
               <ts e="T254" id="Seg_2807" n="e" s="T253">kihi </ts>
               <ts e="T255" id="Seg_2809" n="e" s="T254">kiːrbit, </ts>
               <ts e="T256" id="Seg_2811" n="e" s="T255">kihi </ts>
               <ts e="T257" id="Seg_2813" n="e" s="T256">duː, </ts>
               <ts e="T258" id="Seg_2815" n="e" s="T257">tu͡ok </ts>
               <ts e="T259" id="Seg_2817" n="e" s="T258">duː, </ts>
               <ts e="T260" id="Seg_2819" n="e" s="T259">tugu </ts>
               <ts e="T261" id="Seg_2821" n="e" s="T260">daː </ts>
               <ts e="T262" id="Seg_2823" n="e" s="T261">tu͡olkulaːbappɨn", </ts>
               <ts e="T263" id="Seg_2825" n="e" s="T262">di͡ebit. </ts>
               <ts e="T264" id="Seg_2827" n="e" s="T263">Ol </ts>
               <ts e="T265" id="Seg_2829" n="e" s="T264">kiːren </ts>
               <ts e="T266" id="Seg_2831" n="e" s="T265">aːŋŋa </ts>
               <ts e="T267" id="Seg_2833" n="e" s="T266">olorbut, </ts>
               <ts e="T268" id="Seg_2835" n="e" s="T267">i͡e </ts>
               <ts e="T269" id="Seg_2837" n="e" s="T268">diː. </ts>
               <ts e="T270" id="Seg_2839" n="e" s="T269">Ol </ts>
               <ts e="T271" id="Seg_2841" n="e" s="T270">oloron </ts>
               <ts e="T272" id="Seg_2843" n="e" s="T271">bu͡ollagɨna, </ts>
               <ts e="T273" id="Seg_2845" n="e" s="T272">kajaː. </ts>
               <ts e="T274" id="Seg_2847" n="e" s="T273">Külen </ts>
               <ts e="T275" id="Seg_2849" n="e" s="T274">ɨrdʼaŋnaːbɨt, </ts>
               <ts e="T276" id="Seg_2851" n="e" s="T275">kanna </ts>
               <ts e="T277" id="Seg_2853" n="e" s="T276">ere </ts>
               <ts e="T278" id="Seg_2855" n="e" s="T277">karaktaːk, </ts>
               <ts e="T279" id="Seg_2857" n="e" s="T278">töbötüger </ts>
               <ts e="T280" id="Seg_2859" n="e" s="T279">hogotok </ts>
               <ts e="T281" id="Seg_2861" n="e" s="T280">karaktaːk. </ts>
               <ts e="T282" id="Seg_2863" n="e" s="T281">"Kaja, </ts>
               <ts e="T284" id="Seg_2865" n="e" s="T282">(tu͡o-) </ts>
               <ts e="T285" id="Seg_2867" n="e" s="T284">dogottor, </ts>
               <ts e="T286" id="Seg_2869" n="e" s="T285">bigetik </ts>
               <ts e="T287" id="Seg_2871" n="e" s="T286">tuttuŋ", </ts>
               <ts e="T288" id="Seg_2873" n="e" s="T287">diːr </ts>
               <ts e="T289" id="Seg_2875" n="e" s="T288">bu͡o </ts>
               <ts e="T290" id="Seg_2877" n="e" s="T289">dogorun, </ts>
               <ts e="T291" id="Seg_2879" n="e" s="T290">u͡olattarɨn </ts>
               <ts e="T292" id="Seg_2881" n="e" s="T291">Bi͡egičep. </ts>
               <ts e="T293" id="Seg_2883" n="e" s="T292">Ontuta </ts>
               <ts e="T294" id="Seg_2885" n="e" s="T293">"huːp" </ts>
               <ts e="T295" id="Seg_2887" n="e" s="T294">di͡en </ts>
               <ts e="T296" id="Seg_2889" n="e" s="T295">oborbut, </ts>
               <ts e="T297" id="Seg_2891" n="e" s="T296">ikki </ts>
               <ts e="T298" id="Seg_2893" n="e" s="T297">kihitin </ts>
               <ts e="T299" id="Seg_2895" n="e" s="T298">oboron </ts>
               <ts e="T300" id="Seg_2897" n="e" s="T299">ɨlbɨt. </ts>
               <ts e="T301" id="Seg_2899" n="e" s="T300">Oččo </ts>
               <ts e="T302" id="Seg_2901" n="e" s="T301">bu </ts>
               <ts e="T303" id="Seg_2903" n="e" s="T302">kihi </ts>
               <ts e="T304" id="Seg_2905" n="e" s="T303">hogotok </ts>
               <ts e="T305" id="Seg_2907" n="e" s="T304">kihileːk </ts>
               <ts e="T306" id="Seg_2909" n="e" s="T305">kaːlbɨt. </ts>
               <ts e="T307" id="Seg_2911" n="e" s="T306">Onno </ts>
               <ts e="T308" id="Seg_2913" n="e" s="T307">bu </ts>
               <ts e="T309" id="Seg_2915" n="e" s="T308">kihi </ts>
               <ts e="T310" id="Seg_2917" n="e" s="T309">bu͡olla </ts>
               <ts e="T311" id="Seg_2919" n="e" s="T310">pʼistalʼettaːk </ts>
               <ts e="T312" id="Seg_2921" n="e" s="T311">ebit </ts>
               <ts e="T313" id="Seg_2923" n="e" s="T312">ol. </ts>
               <ts e="T314" id="Seg_2925" n="e" s="T313">"Onno </ts>
               <ts e="T315" id="Seg_2927" n="e" s="T314">diːbin, </ts>
               <ts e="T316" id="Seg_2929" n="e" s="T315">haŋarar </ts>
               <ts e="T317" id="Seg_2931" n="e" s="T316">tɨlɨnan </ts>
               <ts e="T318" id="Seg_2933" n="e" s="T317">haŋarabɨn", </ts>
               <ts e="T319" id="Seg_2935" n="e" s="T318">di͡ebit, </ts>
               <ts e="T320" id="Seg_2937" n="e" s="T319">"pʼistalʼetɨm </ts>
               <ts e="T321" id="Seg_2939" n="e" s="T320">anʼagɨgar. </ts>
               <ts e="T322" id="Seg_2941" n="e" s="T321">Ka, </ts>
               <ts e="T323" id="Seg_2943" n="e" s="T322">bu </ts>
               <ts e="T324" id="Seg_2945" n="e" s="T323">dojduga </ts>
               <ts e="T325" id="Seg_2947" n="e" s="T324">kaːlabɨn </ts>
               <ts e="T326" id="Seg_2949" n="e" s="T325">du͡o </ts>
               <ts e="T327" id="Seg_2951" n="e" s="T326">min?" </ts>
               <ts e="T328" id="Seg_2953" n="e" s="T327">"Minigin </ts>
               <ts e="T329" id="Seg_2955" n="e" s="T328">bɨːhaː, </ts>
               <ts e="T330" id="Seg_2957" n="e" s="T329">bunu </ts>
               <ts e="T331" id="Seg_2959" n="e" s="T330">kanna </ts>
               <ts e="T332" id="Seg_2961" n="e" s="T331">eme </ts>
               <ts e="T333" id="Seg_2963" n="e" s="T332">uŋarar </ts>
               <ts e="T334" id="Seg_2965" n="e" s="T333">hirger </ts>
               <ts e="T335" id="Seg_2967" n="e" s="T334">tiri͡er", </ts>
               <ts e="T336" id="Seg_2969" n="e" s="T335">diːr. </ts>
               <ts e="T337" id="Seg_2971" n="e" s="T336">Hüːhüger </ts>
               <ts e="T338" id="Seg_2973" n="e" s="T337">ɨppɨt </ts>
               <ts e="T339" id="Seg_2975" n="e" s="T338">bu͡olla </ts>
               <ts e="T340" id="Seg_2977" n="e" s="T339">bu </ts>
               <ts e="T341" id="Seg_2979" n="e" s="T340">pʼistalʼetɨnan, </ts>
               <ts e="T342" id="Seg_2981" n="e" s="T341">ontuta </ts>
               <ts e="T343" id="Seg_2983" n="e" s="T342">dʼe </ts>
               <ts e="T344" id="Seg_2985" n="e" s="T343">uŋan </ts>
               <ts e="T345" id="Seg_2987" n="e" s="T344">tüspüt. </ts>
               <ts e="T346" id="Seg_2989" n="e" s="T345">Ol </ts>
               <ts e="T347" id="Seg_2991" n="e" s="T346">uŋan </ts>
               <ts e="T348" id="Seg_2993" n="e" s="T347">tüspütüger </ts>
               <ts e="T349" id="Seg_2995" n="e" s="T348">ürdünnen </ts>
               <ts e="T350" id="Seg_2997" n="e" s="T349">hüːren </ts>
               <ts e="T351" id="Seg_2999" n="e" s="T350">taksɨbɨttar </ts>
               <ts e="T352" id="Seg_3001" n="e" s="T351">bu͡ollar. </ts>
               <ts e="T353" id="Seg_3003" n="e" s="T352">Bi͡egičebiŋ </ts>
               <ts e="T354" id="Seg_3005" n="e" s="T353">kepsiːr </ts>
               <ts e="T356" id="Seg_3007" n="e" s="T354">(kepsi-): </ts>
               <ts e="T357" id="Seg_3009" n="e" s="T356">"Ol </ts>
               <ts e="T358" id="Seg_3011" n="e" s="T357">tijemmit </ts>
               <ts e="T359" id="Seg_3013" n="e" s="T358">kimi͡eke </ts>
               <ts e="T360" id="Seg_3015" n="e" s="T359">bardɨbɨt", </ts>
               <ts e="T361" id="Seg_3017" n="e" s="T360">diːr </ts>
               <ts e="T362" id="Seg_3019" n="e" s="T361">bu͡o, </ts>
               <ts e="T363" id="Seg_3021" n="e" s="T362">"kaːtʼerbar </ts>
               <ts e="T364" id="Seg_3023" n="e" s="T363">bardɨm", </ts>
               <ts e="T365" id="Seg_3025" n="e" s="T364">diːr, </ts>
               <ts e="T366" id="Seg_3027" n="e" s="T365">"kaːtʼerbar </ts>
               <ts e="T367" id="Seg_3029" n="e" s="T366">olorommun </ts>
               <ts e="T368" id="Seg_3031" n="e" s="T367">ebe </ts>
               <ts e="T369" id="Seg_3033" n="e" s="T368">ü͡öhüger </ts>
               <ts e="T370" id="Seg_3035" n="e" s="T369">kiːrdim. </ts>
               <ts e="T371" id="Seg_3037" n="e" s="T370">Körbütüm, </ts>
               <ts e="T372" id="Seg_3039" n="e" s="T371">kennibitten </ts>
               <ts e="T373" id="Seg_3041" n="e" s="T372">baːjdʼɨgɨm </ts>
               <ts e="T374" id="Seg_3043" n="e" s="T373">turan </ts>
               <ts e="T375" id="Seg_3045" n="e" s="T374">kelle, </ts>
               <ts e="T376" id="Seg_3047" n="e" s="T375">di͡en. </ts>
               <ts e="T377" id="Seg_3049" n="e" s="T376">Turan </ts>
               <ts e="T378" id="Seg_3051" n="e" s="T377">kelen </ts>
               <ts e="T379" id="Seg_3053" n="e" s="T378">bu͡ollagɨna </ts>
               <ts e="T380" id="Seg_3055" n="e" s="T379">kaːtʼerdaːgar </ts>
               <ts e="T381" id="Seg_3057" n="e" s="T380">daː </ts>
               <ts e="T382" id="Seg_3059" n="e" s="T381">ulakan </ts>
               <ts e="T383" id="Seg_3061" n="e" s="T382">boldogu </ts>
               <ts e="T384" id="Seg_3063" n="e" s="T383">ɨlan </ts>
               <ts e="T385" id="Seg_3065" n="e" s="T384">baraːn, </ts>
               <ts e="T386" id="Seg_3067" n="e" s="T385">dʼe </ts>
               <ts e="T387" id="Seg_3069" n="e" s="T386">bɨrakpɨt </ts>
               <ts e="T388" id="Seg_3071" n="e" s="T387">kim </ts>
               <ts e="T389" id="Seg_3073" n="e" s="T388">dʼe, </ts>
               <ts e="T390" id="Seg_3075" n="e" s="T389">ɨttaraːččɨta. </ts>
               <ts e="T391" id="Seg_3077" n="e" s="T390">Ol </ts>
               <ts e="T392" id="Seg_3079" n="e" s="T391">bɨrakpɨtɨgar, </ts>
               <ts e="T393" id="Seg_3081" n="e" s="T392">he, </ts>
               <ts e="T394" id="Seg_3083" n="e" s="T393">arɨččɨ </ts>
               <ts e="T395" id="Seg_3085" n="e" s="T394">ebe </ts>
               <ts e="T396" id="Seg_3087" n="e" s="T395">ü͡öhüger </ts>
               <ts e="T397" id="Seg_3089" n="e" s="T396">tiːjbitim, </ts>
               <ts e="T398" id="Seg_3091" n="e" s="T397">tiːjerin </ts>
               <ts e="T399" id="Seg_3093" n="e" s="T398">kɨtta </ts>
               <ts e="T400" id="Seg_3095" n="e" s="T399">bu͡olla </ts>
               <ts e="T401" id="Seg_3097" n="e" s="T400">ol </ts>
               <ts e="T402" id="Seg_3099" n="e" s="T401">dolgunugar, </ts>
               <ts e="T403" id="Seg_3101" n="e" s="T402">bɨrakpɨt </ts>
               <ts e="T404" id="Seg_3103" n="e" s="T403">dolgunugar </ts>
               <ts e="T405" id="Seg_3105" n="e" s="T404">čut </ts>
               <ts e="T406" id="Seg_3107" n="e" s="T405">tü͡öre </ts>
               <ts e="T407" id="Seg_3109" n="e" s="T406">bara </ts>
               <ts e="T408" id="Seg_3111" n="e" s="T407">hɨspɨt. </ts>
               <ts e="T409" id="Seg_3113" n="e" s="T408">Ol </ts>
               <ts e="T410" id="Seg_3115" n="e" s="T409">küreːtibit", </ts>
               <ts e="T411" id="Seg_3117" n="e" s="T410">di͡ebit </ts>
               <ts e="T412" id="Seg_3119" n="e" s="T411">bu͡olla. </ts>
               <ts e="T413" id="Seg_3121" n="e" s="T412">Oččo </ts>
               <ts e="T414" id="Seg_3123" n="e" s="T413">hette </ts>
               <ts e="T415" id="Seg_3125" n="e" s="T414">kihitten </ts>
               <ts e="T416" id="Seg_3127" n="e" s="T415">hogotok </ts>
               <ts e="T417" id="Seg_3129" n="e" s="T416">kihileːk </ts>
               <ts e="T418" id="Seg_3131" n="e" s="T417">kelbit, </ts>
               <ts e="T419" id="Seg_3133" n="e" s="T418">oktonnor </ts>
               <ts e="T420" id="Seg_3135" n="e" s="T419">bejelere. </ts>
               <ts e="T421" id="Seg_3137" n="e" s="T420">Tugu </ts>
               <ts e="T422" id="Seg_3139" n="e" s="T421">da </ts>
               <ts e="T423" id="Seg_3141" n="e" s="T422">bulbakka </ts>
               <ts e="T424" id="Seg_3143" n="e" s="T423">ergijen </ts>
               <ts e="T425" id="Seg_3145" n="e" s="T424">(kel-), </ts>
               <ts e="T426" id="Seg_3147" n="e" s="T425">ol </ts>
               <ts e="T427" id="Seg_3149" n="e" s="T426">kelen </ts>
               <ts e="T428" id="Seg_3151" n="e" s="T427">ol </ts>
               <ts e="T429" id="Seg_3153" n="e" s="T428">üːhe </ts>
               <ts e="T430" id="Seg_3155" n="e" s="T429">di͡ek </ts>
               <ts e="T431" id="Seg_3157" n="e" s="T430">barbɨt. </ts>
               <ts e="T432" id="Seg_3159" n="e" s="T431">Onton </ts>
               <ts e="T433" id="Seg_3161" n="e" s="T432">Čagɨdajɨ </ts>
               <ts e="T434" id="Seg_3163" n="e" s="T433">bu͡ollagɨn </ts>
               <ts e="T435" id="Seg_3165" n="e" s="T434">hajɨn </ts>
               <ts e="T436" id="Seg_3167" n="e" s="T435">biːrge </ts>
               <ts e="T437" id="Seg_3169" n="e" s="T436">olorollor </ts>
               <ts e="T438" id="Seg_3171" n="e" s="T437">ühü. </ts>
               <ts e="T439" id="Seg_3173" n="e" s="T438">Ahaːn </ts>
               <ts e="T440" id="Seg_3175" n="e" s="T439">oloronnor </ts>
               <ts e="T441" id="Seg_3177" n="e" s="T440">ka </ts>
               <ts e="T442" id="Seg_3179" n="e" s="T441">huruk </ts>
               <ts e="T443" id="Seg_3181" n="e" s="T442">kelbit, </ts>
               <ts e="T444" id="Seg_3183" n="e" s="T443">bu </ts>
               <ts e="T445" id="Seg_3185" n="e" s="T444">hurugu </ts>
               <ts e="T446" id="Seg_3187" n="e" s="T445">bu͡ollagɨna </ts>
               <ts e="T447" id="Seg_3189" n="e" s="T446">Nosku͡oga </ts>
               <ts e="T448" id="Seg_3191" n="e" s="T447">tiri͡eri͡ekterin </ts>
               <ts e="T449" id="Seg_3193" n="e" s="T448">naːda. </ts>
               <ts e="T450" id="Seg_3195" n="e" s="T449">Kantan </ts>
               <ts e="T451" id="Seg_3197" n="e" s="T450">ere </ts>
               <ts e="T452" id="Seg_3199" n="e" s="T451">kelbit </ts>
               <ts e="T453" id="Seg_3201" n="e" s="T452">tɨːnnan, </ts>
               <ts e="T454" id="Seg_3203" n="e" s="T453">oččo </ts>
               <ts e="T455" id="Seg_3205" n="e" s="T454">tu͡ok </ts>
               <ts e="T456" id="Seg_3207" n="e" s="T455">lu͡okkata </ts>
               <ts e="T457" id="Seg_3209" n="e" s="T456">keli͡ej. </ts>
               <ts e="T458" id="Seg_3211" n="e" s="T457">"Ka, </ts>
               <ts e="T459" id="Seg_3213" n="e" s="T458">Čagɨdaj, </ts>
               <ts e="T460" id="Seg_3215" n="e" s="T459">bu </ts>
               <ts e="T461" id="Seg_3217" n="e" s="T460">hurugu </ts>
               <ts e="T462" id="Seg_3219" n="e" s="T461">bu͡olla </ts>
               <ts e="T463" id="Seg_3221" n="e" s="T462">kimi͡eke </ts>
               <ts e="T464" id="Seg_3223" n="e" s="T463">tiri͡er, </ts>
               <ts e="T465" id="Seg_3225" n="e" s="T464">Kiri͡eske, </ts>
               <ts e="T466" id="Seg_3227" n="e" s="T465">Rɨmnaj </ts>
               <ts e="T467" id="Seg_3229" n="e" s="T466">Kiri͡ehiger." </ts>
               <ts e="T468" id="Seg_3231" n="e" s="T467">Kör, </ts>
               <ts e="T469" id="Seg_3233" n="e" s="T468">ɨraːgɨn, </ts>
               <ts e="T470" id="Seg_3235" n="e" s="T469">Rɨmnaj </ts>
               <ts e="T471" id="Seg_3237" n="e" s="T470">ɨnaraː </ts>
               <ts e="T472" id="Seg_3239" n="e" s="T471">öttüger. </ts>
            </ts>
            <ts e="T510" id="Seg_3240" n="sc" s="T476">
               <ts e="T478" id="Seg_3242" n="e" s="T476">Hu͡ok. </ts>
               <ts e="T479" id="Seg_3244" n="e" s="T478">Ol </ts>
               <ts e="T480" id="Seg_3246" n="e" s="T479">bejelerin </ts>
               <ts e="T482" id="Seg_3248" n="e" s="T480">gi͡ettere. </ts>
               <ts e="T485" id="Seg_3250" n="e" s="T482">Heː </ts>
               <ts e="T486" id="Seg_3252" n="e" s="T485">Bi͡egičeptere </ts>
               <ts e="T487" id="Seg_3254" n="e" s="T486">honon </ts>
               <ts e="T488" id="Seg_3256" n="e" s="T487">ajannaːbɨt </ts>
               <ts e="T489" id="Seg_3258" n="e" s="T488">bu͡olla, </ts>
               <ts e="T490" id="Seg_3260" n="e" s="T489">ol </ts>
               <ts e="T491" id="Seg_3262" n="e" s="T490">barɨːtɨnan </ts>
               <ts e="T492" id="Seg_3264" n="e" s="T491">barbɨt </ts>
               <ts e="T493" id="Seg_3266" n="e" s="T492">bu </ts>
               <ts e="T494" id="Seg_3268" n="e" s="T493">di͡ek, </ts>
               <ts e="T495" id="Seg_3270" n="e" s="T494">üːhe </ts>
               <ts e="T496" id="Seg_3272" n="e" s="T495">di͡ek. </ts>
               <ts e="T497" id="Seg_3274" n="e" s="T496">Če, </ts>
               <ts e="T498" id="Seg_3276" n="e" s="T497">ol </ts>
               <ts e="T499" id="Seg_3278" n="e" s="T498">bu͡o </ts>
               <ts e="T500" id="Seg_3280" n="e" s="T499">ol </ts>
               <ts e="T501" id="Seg_3282" n="e" s="T500">hurugu </ts>
               <ts e="T502" id="Seg_3284" n="e" s="T501">iltereller, </ts>
               <ts e="T503" id="Seg_3286" n="e" s="T502">oččo </ts>
               <ts e="T504" id="Seg_3288" n="e" s="T503">huruktaːktar </ts>
               <ts e="T505" id="Seg_3290" n="e" s="T504">ebite </ts>
               <ts e="T506" id="Seg_3292" n="e" s="T505">bu͡olla, </ts>
               <ts e="T507" id="Seg_3294" n="e" s="T506">tojonnoːktor </ts>
               <ts e="T508" id="Seg_3296" n="e" s="T507">bu͡o, </ts>
               <ts e="T510" id="Seg_3298" n="e" s="T508">(ogonnʼ-)… </ts>
            </ts>
            <ts e="T648" id="Seg_3299" n="sc" s="T511">
               <ts e="T512" id="Seg_3301" n="e" s="T511">Hakalar. </ts>
               <ts e="T513" id="Seg_3303" n="e" s="T512">Ol </ts>
               <ts e="T514" id="Seg_3305" n="e" s="T513">ülehittere, </ts>
               <ts e="T515" id="Seg_3307" n="e" s="T514">habi͡ettara </ts>
               <ts e="T516" id="Seg_3309" n="e" s="T515">tu͡oktara. </ts>
               <ts e="T517" id="Seg_3311" n="e" s="T516">Huruk </ts>
               <ts e="T518" id="Seg_3313" n="e" s="T517">ɨːtallar </ts>
               <ts e="T519" id="Seg_3315" n="e" s="T518">bu͡o. </ts>
               <ts e="T520" id="Seg_3317" n="e" s="T519">Innʼe </ts>
               <ts e="T521" id="Seg_3319" n="e" s="T520">(gɨn-) </ts>
               <ts e="T522" id="Seg_3321" n="e" s="T521">bu </ts>
               <ts e="T523" id="Seg_3323" n="e" s="T522">ogonnʼor </ts>
               <ts e="T524" id="Seg_3325" n="e" s="T523">bu͡olla </ts>
               <ts e="T525" id="Seg_3327" n="e" s="T524">dʼe </ts>
               <ts e="T526" id="Seg_3329" n="e" s="T525">ki͡ehe </ts>
               <ts e="T527" id="Seg_3331" n="e" s="T526">kamnɨːr. </ts>
               <ts e="T528" id="Seg_3333" n="e" s="T527">"Harsi͡erda </ts>
               <ts e="T529" id="Seg_3335" n="e" s="T528">erdehit </ts>
               <ts e="T530" id="Seg_3337" n="e" s="T529">dʼaktar </ts>
               <ts e="T531" id="Seg_3339" n="e" s="T530">turar </ts>
               <ts e="T532" id="Seg_3341" n="e" s="T531">kemiger </ts>
               <ts e="T533" id="Seg_3343" n="e" s="T532">keli͡em", </ts>
               <ts e="T534" id="Seg_3345" n="e" s="T533">di͡ebit </ts>
               <ts e="T535" id="Seg_3347" n="e" s="T534">bu </ts>
               <ts e="T536" id="Seg_3349" n="e" s="T535">ogonnʼor. </ts>
               <ts e="T537" id="Seg_3351" n="e" s="T536">"O, </ts>
               <ts e="T538" id="Seg_3353" n="e" s="T537">kajdak </ts>
               <ts e="T539" id="Seg_3355" n="e" s="T538">gɨnan </ts>
               <ts e="T540" id="Seg_3357" n="e" s="T539">keleːri </ts>
               <ts e="T541" id="Seg_3359" n="e" s="T540">gɨnagɨn </ts>
               <ts e="T542" id="Seg_3361" n="e" s="T541">Rɨmnaj </ts>
               <ts e="T543" id="Seg_3363" n="e" s="T542">ɨnaːra </ts>
               <ts e="T544" id="Seg_3365" n="e" s="T543">öttütten, </ts>
               <ts e="T545" id="Seg_3367" n="e" s="T544">hürdeːk </ts>
               <ts e="T546" id="Seg_3369" n="e" s="T545">ɨraːk </ts>
               <ts e="T547" id="Seg_3371" n="e" s="T546">Pobügejten." </ts>
               <ts e="T548" id="Seg_3373" n="e" s="T547">Dʼe, </ts>
               <ts e="T549" id="Seg_3375" n="e" s="T548">bu </ts>
               <ts e="T550" id="Seg_3377" n="e" s="T549">ogonnʼor </ts>
               <ts e="T551" id="Seg_3379" n="e" s="T550">kaːman </ts>
               <ts e="T552" id="Seg_3381" n="e" s="T551">kaːlbɨt, </ts>
               <ts e="T553" id="Seg_3383" n="e" s="T552">e, </ts>
               <ts e="T554" id="Seg_3385" n="e" s="T553">baran </ts>
               <ts e="T555" id="Seg_3387" n="e" s="T554">kaːlbɨt </ts>
               <ts e="T556" id="Seg_3389" n="e" s="T555">tɨːnnan. </ts>
               <ts e="T557" id="Seg_3391" n="e" s="T556">Harsi͡erda </ts>
               <ts e="T558" id="Seg_3393" n="e" s="T557">emeːksitter </ts>
               <ts e="T560" id="Seg_3395" n="e" s="T558">(turbu-), </ts>
               <ts e="T561" id="Seg_3397" n="e" s="T560">erdehit </ts>
               <ts e="T562" id="Seg_3399" n="e" s="T561">dʼaktattar </ts>
               <ts e="T563" id="Seg_3401" n="e" s="T562">turallar, </ts>
               <ts e="T564" id="Seg_3403" n="e" s="T563">naprimer, </ts>
               <ts e="T565" id="Seg_3405" n="e" s="T564">oččo </ts>
               <ts e="T566" id="Seg_3407" n="e" s="T565">hette </ts>
               <ts e="T567" id="Seg_3409" n="e" s="T566">čaːs </ts>
               <ts e="T568" id="Seg_3411" n="e" s="T567">itte </ts>
               <ts e="T569" id="Seg_3413" n="e" s="T568">ebit </ts>
               <ts e="T570" id="Seg_3415" n="e" s="T569">eni, </ts>
               <ts e="T571" id="Seg_3417" n="e" s="T570">tu͡ok </ts>
               <ts e="T572" id="Seg_3419" n="e" s="T571">čaːha </ts>
               <ts e="T573" id="Seg_3421" n="e" s="T572">keli͡ej, </ts>
               <ts e="T574" id="Seg_3423" n="e" s="T573">künnerinen </ts>
               <ts e="T575" id="Seg_3425" n="e" s="T574">köröllör </ts>
               <ts e="T576" id="Seg_3427" n="e" s="T575">bu͡o. </ts>
               <ts e="T577" id="Seg_3429" n="e" s="T576">Araː, </ts>
               <ts e="T578" id="Seg_3431" n="e" s="T577">hette </ts>
               <ts e="T579" id="Seg_3433" n="e" s="T578">čaːska </ts>
               <ts e="T580" id="Seg_3435" n="e" s="T579">Čagɨdaj </ts>
               <ts e="T581" id="Seg_3437" n="e" s="T580">kelbit, </ts>
               <ts e="T582" id="Seg_3439" n="e" s="T581">tɨːta </ts>
               <ts e="T583" id="Seg_3441" n="e" s="T582">umsana </ts>
               <ts e="T584" id="Seg_3443" n="e" s="T583">hɨtar </ts>
               <ts e="T585" id="Seg_3445" n="e" s="T584">diːn, </ts>
               <ts e="T586" id="Seg_3447" n="e" s="T585">kör, </ts>
               <ts e="T587" id="Seg_3449" n="e" s="T586">onnuk </ts>
               <ts e="T588" id="Seg_3451" n="e" s="T587">korsun </ts>
               <ts e="T589" id="Seg_3453" n="e" s="T588">kihiler </ts>
               <ts e="T590" id="Seg_3455" n="e" s="T589">baːllar. </ts>
               <ts e="T591" id="Seg_3457" n="e" s="T590">Onton </ts>
               <ts e="T592" id="Seg_3459" n="e" s="T591">dʼaktar </ts>
               <ts e="T593" id="Seg_3461" n="e" s="T592">törüːr </ts>
               <ts e="T594" id="Seg_3463" n="e" s="T593">hol </ts>
               <ts e="T595" id="Seg_3465" n="e" s="T594">huːrtarɨgar, </ts>
               <ts e="T596" id="Seg_3467" n="e" s="T595">bu </ts>
               <ts e="T597" id="Seg_3469" n="e" s="T596">dʼaktar </ts>
               <ts e="T598" id="Seg_3471" n="e" s="T597">ölöːrü </ts>
               <ts e="T599" id="Seg_3473" n="e" s="T598">gɨmmɨt, </ts>
               <ts e="T600" id="Seg_3475" n="e" s="T599">kajdak </ts>
               <ts e="T601" id="Seg_3477" n="e" s="T600">da </ts>
               <ts e="T602" id="Seg_3479" n="e" s="T601">gɨnɨ͡aktarɨn </ts>
               <ts e="T603" id="Seg_3481" n="e" s="T602">bert. </ts>
               <ts e="T604" id="Seg_3483" n="e" s="T603">Du͡oktura </ts>
               <ts e="T605" id="Seg_3485" n="e" s="T604">hu͡ok </ts>
               <ts e="T606" id="Seg_3487" n="e" s="T605">üjege </ts>
               <ts e="T607" id="Seg_3489" n="e" s="T606">ke, </ts>
               <ts e="T608" id="Seg_3491" n="e" s="T607">baːbɨskalara </ts>
               <ts e="T609" id="Seg_3493" n="e" s="T608">muŋnaːk </ts>
               <ts e="T610" id="Seg_3495" n="e" s="T609">hɨlajan </ts>
               <ts e="T611" id="Seg_3497" n="e" s="T610">büppüt. </ts>
               <ts e="T612" id="Seg_3499" n="e" s="T611">"Kaja, </ts>
               <ts e="T613" id="Seg_3501" n="e" s="T612">Čagɨdaj </ts>
               <ts e="T614" id="Seg_3503" n="e" s="T613">kellin, </ts>
               <ts e="T615" id="Seg_3505" n="e" s="T614">dʼaktar </ts>
               <ts e="T616" id="Seg_3507" n="e" s="T615">ölöːrü </ts>
               <ts e="T617" id="Seg_3509" n="e" s="T616">gɨnna, </ts>
               <ts e="T618" id="Seg_3511" n="e" s="T617">kajdak </ts>
               <ts e="T619" id="Seg_3513" n="e" s="T618">eme </ts>
               <ts e="T620" id="Seg_3515" n="e" s="T619">gɨnnɨn </ts>
               <ts e="T621" id="Seg_3517" n="e" s="T620">oŋu͡ok </ts>
               <ts e="T622" id="Seg_3519" n="e" s="T621">araːrdɨn </ts>
               <ts e="T623" id="Seg_3521" n="e" s="T622">munna." </ts>
               <ts e="T624" id="Seg_3523" n="e" s="T623">Hinnʼe </ts>
               <ts e="T625" id="Seg_3525" n="e" s="T624">bu </ts>
               <ts e="T626" id="Seg_3527" n="e" s="T625">Čagɨdaj </ts>
               <ts e="T627" id="Seg_3529" n="e" s="T626">keler, </ts>
               <ts e="T628" id="Seg_3531" n="e" s="T627">kajdak </ts>
               <ts e="T630" id="Seg_3533" n="e" s="T628">(gɨ-) </ts>
               <ts e="T631" id="Seg_3535" n="e" s="T630">bu </ts>
               <ts e="T632" id="Seg_3537" n="e" s="T631">dʼaktarɨ </ts>
               <ts e="T633" id="Seg_3539" n="e" s="T632">iliːleːn </ts>
               <ts e="T634" id="Seg_3541" n="e" s="T633">ɨlar </ts>
               <ts e="T635" id="Seg_3543" n="e" s="T634">ogotun, </ts>
               <ts e="T636" id="Seg_3545" n="e" s="T635">honon </ts>
               <ts e="T637" id="Seg_3547" n="e" s="T636">tɨːnnak </ts>
               <ts e="T638" id="Seg_3549" n="e" s="T637">keːher </ts>
               <ts e="T639" id="Seg_3551" n="e" s="T638">kör. </ts>
               <ts e="T640" id="Seg_3553" n="e" s="T639">Onnuktar </ts>
               <ts e="T641" id="Seg_3555" n="e" s="T640">baːr </ts>
               <ts e="T642" id="Seg_3557" n="e" s="T641">etiler </ts>
               <ts e="T643" id="Seg_3559" n="e" s="T642">kɨrdʼagastar. </ts>
               <ts e="T644" id="Seg_3561" n="e" s="T643">I </ts>
               <ts e="T645" id="Seg_3563" n="e" s="T644">Čakɨːla </ts>
               <ts e="T646" id="Seg_3565" n="e" s="T645">ogonnʼor </ts>
               <ts e="T647" id="Seg_3567" n="e" s="T646">ehete </ts>
               <ts e="T648" id="Seg_3569" n="e" s="T647">ontuŋ. </ts>
            </ts>
            <ts e="T673" id="Seg_3570" n="sc" s="T651">
               <ts e="T652" id="Seg_3572" n="e" s="T651">Heː, </ts>
               <ts e="T653" id="Seg_3574" n="e" s="T652">oččogo </ts>
               <ts e="T654" id="Seg_3576" n="e" s="T653">ol </ts>
               <ts e="T655" id="Seg_3578" n="e" s="T654">Čagɨdajɨŋ </ts>
               <ts e="T656" id="Seg_3580" n="e" s="T655">dʼaktara </ts>
               <ts e="T657" id="Seg_3582" n="e" s="T656">bu͡ollagɨna </ts>
               <ts e="T658" id="Seg_3584" n="e" s="T657">emeːksin, </ts>
               <ts e="T659" id="Seg_3586" n="e" s="T658">Anna </ts>
               <ts e="T660" id="Seg_3588" n="e" s="T659">inʼete </ts>
               <ts e="T661" id="Seg_3590" n="e" s="T660">emeːksin, </ts>
               <ts e="T662" id="Seg_3592" n="e" s="T661">baː </ts>
               <ts e="T663" id="Seg_3594" n="e" s="T662">Čeːke </ts>
               <ts e="T664" id="Seg_3596" n="e" s="T663">emeːksin. </ts>
               <ts e="T665" id="Seg_3598" n="e" s="T664">Ontuŋ </ts>
               <ts e="T666" id="Seg_3600" n="e" s="T665">teːtete </ts>
               <ts e="T667" id="Seg_3602" n="e" s="T666">bihigi </ts>
               <ts e="T668" id="Seg_3604" n="e" s="T667">eheːbitin </ts>
               <ts e="T669" id="Seg_3606" n="e" s="T668">kɨtta </ts>
               <ts e="T670" id="Seg_3608" n="e" s="T669">biːrge </ts>
               <ts e="T671" id="Seg_3610" n="e" s="T670">töröːbütter </ts>
               <ts e="T672" id="Seg_3612" n="e" s="T671">Kudrʼakop </ts>
               <ts e="T673" id="Seg_3614" n="e" s="T672">ogonnʼor. </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KiPP">
            <ta e="T2" id="Seg_3615" s="T0">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.001 (001.001)</ta>
            <ta e="T9" id="Seg_3616" s="T2">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.002 (001.002)</ta>
            <ta e="T12" id="Seg_3617" s="T9">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.003 (001.003)</ta>
            <ta e="T23" id="Seg_3618" s="T12">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.004 (001.004)</ta>
            <ta e="T26" id="Seg_3619" s="T23">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.005 (001.005)</ta>
            <ta e="T35" id="Seg_3620" s="T26">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.006 (001.006)</ta>
            <ta e="T48" id="Seg_3621" s="T35">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.007 (001.007)</ta>
            <ta e="T57" id="Seg_3622" s="T48">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.008 (001.008)</ta>
            <ta e="T67" id="Seg_3623" s="T57">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.009 (001.009)</ta>
            <ta e="T77" id="Seg_3624" s="T67">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.010 (001.010)</ta>
            <ta e="T83" id="Seg_3625" s="T77">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.011 (001.011)</ta>
            <ta e="T96" id="Seg_3626" s="T83">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.012 (001.012)</ta>
            <ta e="T114" id="Seg_3627" s="T96">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.013 (001.013)</ta>
            <ta e="T121" id="Seg_3628" s="T114">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.014 (001.014)</ta>
            <ta e="T130" id="Seg_3629" s="T121">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.015 (001.015)</ta>
            <ta e="T142" id="Seg_3630" s="T130">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.016 (001.016)</ta>
            <ta e="T152" id="Seg_3631" s="T142">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.017 (001.017)</ta>
            <ta e="T153" id="Seg_3632" s="T152">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.018 (001.018)</ta>
            <ta e="T169" id="Seg_3633" s="T153">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.019 (001.019)</ta>
            <ta e="T178" id="Seg_3634" s="T169">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.020 (001.020)</ta>
            <ta e="T188" id="Seg_3635" s="T178">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.021 (001.021)</ta>
            <ta e="T191" id="Seg_3636" s="T188">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.022 (001.022)</ta>
            <ta e="T194" id="Seg_3637" s="T191">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.023 (001.023)</ta>
            <ta e="T213" id="Seg_3638" s="T209">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.024 (001.025)</ta>
            <ta e="T220" id="Seg_3639" s="T213">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.025 (001.026)</ta>
            <ta e="T232" id="Seg_3640" s="T220">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.026 (001.027)</ta>
            <ta e="T239" id="Seg_3641" s="T232">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.027 (001.028)</ta>
            <ta e="T246" id="Seg_3642" s="T239">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.028 (001.029)</ta>
            <ta e="T250" id="Seg_3643" s="T246">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.029 (001.030)</ta>
            <ta e="T263" id="Seg_3644" s="T250">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.030 (001.031)</ta>
            <ta e="T269" id="Seg_3645" s="T263">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.031 (001.032)</ta>
            <ta e="T273" id="Seg_3646" s="T269">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.032 (001.033)</ta>
            <ta e="T281" id="Seg_3647" s="T273">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.033 (001.034)</ta>
            <ta e="T292" id="Seg_3648" s="T281">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.034 (001.035)</ta>
            <ta e="T300" id="Seg_3649" s="T292">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.035 (001.036)</ta>
            <ta e="T306" id="Seg_3650" s="T300">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.036 (001.037)</ta>
            <ta e="T313" id="Seg_3651" s="T306">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.037 (001.038)</ta>
            <ta e="T321" id="Seg_3652" s="T313">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.038 (001.039)</ta>
            <ta e="T327" id="Seg_3653" s="T321">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.039 (001.040)</ta>
            <ta e="T336" id="Seg_3654" s="T327">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.040 (001.041)</ta>
            <ta e="T345" id="Seg_3655" s="T336">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.041 (001.042)</ta>
            <ta e="T352" id="Seg_3656" s="T345">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.042 (001.043)</ta>
            <ta e="T356" id="Seg_3657" s="T352">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.043 (001.044)</ta>
            <ta e="T370" id="Seg_3658" s="T356">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.044 (001.045)</ta>
            <ta e="T376" id="Seg_3659" s="T370">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.045 (001.046)</ta>
            <ta e="T390" id="Seg_3660" s="T376">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.046 (001.047)</ta>
            <ta e="T408" id="Seg_3661" s="T390">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.047 (001.048)</ta>
            <ta e="T412" id="Seg_3662" s="T408">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.048 (001.049)</ta>
            <ta e="T420" id="Seg_3663" s="T412">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.049 (001.050)</ta>
            <ta e="T431" id="Seg_3664" s="T420">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.050 (001.051)</ta>
            <ta e="T438" id="Seg_3665" s="T431">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.051 (001.052)</ta>
            <ta e="T449" id="Seg_3666" s="T438">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.052 (001.053)</ta>
            <ta e="T457" id="Seg_3667" s="T449">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.053 (001.054)</ta>
            <ta e="T467" id="Seg_3668" s="T457">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.054 (001.055)</ta>
            <ta e="T472" id="Seg_3669" s="T467">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.055 (001.056)</ta>
            <ta e="T478" id="Seg_3670" s="T476">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.056 (001.058)</ta>
            <ta e="T482" id="Seg_3671" s="T478">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.057 (001.059)</ta>
            <ta e="T496" id="Seg_3672" s="T482">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.058 (001.061)</ta>
            <ta e="T510" id="Seg_3673" s="T496">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.059 (001.062)</ta>
            <ta e="T512" id="Seg_3674" s="T511">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.060 (001.064)</ta>
            <ta e="T516" id="Seg_3675" s="T512">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.061 (001.065)</ta>
            <ta e="T519" id="Seg_3676" s="T516">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.062 (001.066)</ta>
            <ta e="T527" id="Seg_3677" s="T519">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.063 (001.067)</ta>
            <ta e="T536" id="Seg_3678" s="T527">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.064 (001.068)</ta>
            <ta e="T547" id="Seg_3679" s="T536">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.065 (001.069)</ta>
            <ta e="T556" id="Seg_3680" s="T547">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.066 (001.070)</ta>
            <ta e="T576" id="Seg_3681" s="T556">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.067 (001.071)</ta>
            <ta e="T590" id="Seg_3682" s="T576">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.068 (001.072)</ta>
            <ta e="T603" id="Seg_3683" s="T590">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.069 (001.073)</ta>
            <ta e="T611" id="Seg_3684" s="T603">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.070 (001.074)</ta>
            <ta e="T623" id="Seg_3685" s="T611">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.071 (001.075)</ta>
            <ta e="T639" id="Seg_3686" s="T623">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.072 (001.076)</ta>
            <ta e="T643" id="Seg_3687" s="T639">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.073 (001.077)</ta>
            <ta e="T648" id="Seg_3688" s="T643">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.074 (001.078)</ta>
            <ta e="T664" id="Seg_3689" s="T651">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.075 (001.080)</ta>
            <ta e="T673" id="Seg_3690" s="T664">KiPP_KuNS_2002_LegendOfBegichev_nar.KiPP.076 (001.081)</ta>
         </annotation>
         <annotation name="st" tierref="st-KiPP">
            <ta e="T2" id="Seg_3691" s="T0">Киргизова Правсковья Петровна: Биэгичэби иллэллэр.</ta>
            <ta e="T9" id="Seg_3692" s="T2">Һит… һиринэн һылдьар этэ үһү урут, экспедициялар.</ta>
            <ta e="T12" id="Seg_3693" s="T9">Ону эһэм кэпсээччи.</ta>
            <ta e="T23" id="Seg_3694" s="T12">Миниэнэ иньэм, эһэ… ити иньэм уруулара: Маркел огонньор, Өлөөнө агата огонньор.</ta>
            <ta e="T26" id="Seg_3695" s="T23">Олор кэпсэтэллэрин истээччибин.</ta>
            <ta e="T35" id="Seg_3696" s="T26">Бу Биэгичэп буоллагына табаннан айанныыр Биэгичэп арыытыгар, һэттэ киһилээк.</ta>
            <ta e="T48" id="Seg_3697" s="T35">Бу киһи айаннаан иһэн: табаннан иллэллэр буо, атынтан-атын табаннан иллэллэр, һаӈаттан-һаӈа табаннан иллэллэр.</ta>
            <ta e="T57" id="Seg_3698" s="T48">Онно Чагыдай огонньору илтэрэллэр булар, мин огоньорум эһэтинээк, эһэлэрэ. </ta>
            <ta e="T67" id="Seg_3699" s="T57">Ол огонньор туок даа һиппэтэк киһитэ этэ - һүрдээк камнастаак киһи.</ta>
            <ta e="T77" id="Seg_3700" s="T67">Ол огонньор.. һатаабат табалары көлүттэрэн бараан, Биэгичэп арыытыгар илтэрэллэр буо.</ta>
            <ta e="T83" id="Seg_3701" s="T77">Онно табалар мөӈөллөр үһү,иэ дии.</ta>
            <ta e="T96" id="Seg_3702" s="T83">Табалар мөӈөллөр, бу киһи бэргэһэтин, ол Биэгичэп гиэнин бэргэһэтин таба иилэн бырагар үһү.</ta>
            <ta e="T114" id="Seg_3703" s="T96">Бу һырганы курдары бэргэһэ түһэн эрдэгинэ, ону огонньор һырганы курдары ойон, кабан ылбыт буо, һиргэ түһүөгүн бэтэтэрээ өттүгэр.</ta>
            <ta e="T121" id="Seg_3704" s="T114">Ону ол огонньор Биэгичэбэ диир үһү: </ta>
            <ta e="T130" id="Seg_3705" s="T121">"О-о, бу һака һиригэр да камнастаак киһи баар буолар…"</ta>
            <ta e="T142" id="Seg_3706" s="T130">Дьэ, бу огоннь…Биэгичэп буоллагына ол, ол арыыга тийбитим, диэн кэпсиир үһү.</ta>
            <ta e="T152" id="Seg_3707" s="T142">"Бу туок одууну көрдүӈ онно?" - диэн ыйыталлар һакалар, иэ ди.</ta>
            <ta e="T153" id="Seg_3708" s="T152">Онуга:</ta>
            <ta e="T169" id="Seg_3709" s="T153">"Мин одууну тугу да олус булбатым, маржалара буоллаӈына(буоллагына) тиистэриттэн баайан баран утуйаллар, утуйа һыталларын байан бремноттан".</ta>
            <ta e="T178" id="Seg_3710" s="T169">Ону һэп даа диэбэттэр, һагастаан кииллэрэллэр, ууга кииллэрэн кээһэллэр, эни.</ta>
            <ta e="T188" id="Seg_3711" s="T178">Онтон арай арыыга таас кайага буллум, диир, үһү биир одуну.</ta>
            <ta e="T191" id="Seg_3712" s="T188">"Дьэ, тугу буллуӈ?"</ta>
            <ta e="T194" id="Seg_3713" s="T191">Ону диир буо:</ta>
            <ta e="T213" id="Seg_3714" s="T209">Онуга киирэн бардым", – диир.</ta>
            <ta e="T220" id="Seg_3715" s="T213">Үс киһилэк, бу киһилэр аһа һуок буолбуппут.</ta>
            <ta e="T232" id="Seg_3716" s="T220">Оччого буоллагына эбэттэн, о, кыра балыккааны кабынаннар ол аӈӈа киирэннэр, аһаарылар киирбиттэр.</ta>
            <ta e="T239" id="Seg_3717" s="T232">Бу киһи арай киирбитэ, бу киһи олорор.</ta>
            <ta e="T246" id="Seg_3718" s="T239">Олордогуна туок эрэ киһитэ кэлбит, иэ дии.</ta>
            <ta e="T250" id="Seg_3719" s="T246">Киһи дуу, туок дуу?</ta>
            <ta e="T263" id="Seg_3720" s="T250">"Һанныттан мэньиитэ һуок киһи киирбит, киһи дуу, туок дуу, тугу даа туолкулаабаппын", – диэбит.</ta>
            <ta e="T269" id="Seg_3721" s="T263">Ол киирэн ааӈӈа олорбут, иэ ди.</ta>
            <ta e="T273" id="Seg_3722" s="T269">Ол олорон буоллагына, кайаа.</ta>
            <ta e="T281" id="Seg_3723" s="T273">Күлэн ырдьаӈнаабыт, канна эрэ карактаак, төбөтүгэр һоготок карактаак.</ta>
            <ta e="T292" id="Seg_3724" s="T281">"Кайа, туо…, доготтор, бигэтик туттуӈ", – диир буо догорун, уолаттарын Биэгичэп.</ta>
            <ta e="T300" id="Seg_3725" s="T292">Онтута "һу-ууп" диэн оборбут, икки киһитин оборон ылбыт.</ta>
            <ta e="T306" id="Seg_3726" s="T300">Оччо (Оччого) бу киһи һоготок киһилээк каалбыт.</ta>
            <ta e="T313" id="Seg_3727" s="T306">Онно бу киһи буолла пистолеттаак эбит ол.</ta>
            <ta e="T321" id="Seg_3728" s="T313">"Онно диибин, һаӈарар тылынан һаӈарабын, - диэбит, - пистолетым аньагыгар.</ta>
            <ta e="T327" id="Seg_3729" s="T321">Ка, бу дойдуга каалабын дуо мин?"</ta>
            <ta e="T336" id="Seg_3730" s="T327">"Минигин быыһа, буну канна эмэ уӈарар һиргэр тириэр" - диир.</ta>
            <ta e="T345" id="Seg_3731" s="T336">Һүүһүгэр ыппыт буолла бу пистолетынан, онтута дьэ уӈан түспүт.</ta>
            <ta e="T352" id="Seg_3732" s="T345">Ол уӈан түспүтүгэр үрдүннэн һүүрэн таксыбыттар булар.</ta>
            <ta e="T356" id="Seg_3733" s="T352">Биэгичэбиӈ кэпсиир..си:</ta>
            <ta e="T370" id="Seg_3734" s="T356">"Ол тийэммит кимиэкэ бардыбыт, - диир буо, - каатербар бардым диир, - каатербар олороммун эбэ үөһүгэр киирдим.</ta>
            <ta e="T376" id="Seg_3735" s="T370">Көрбүтүм, кэннибиттэн баайдьыгым туран кэллэ, диэн.</ta>
            <ta e="T390" id="Seg_3736" s="T376">Туран кэлэн буоллагына каатердаагар даа улакан болдогу ылан бараан, дьэ бырак бу (кимнэрэ ?), ыттарааччыта.</ta>
            <ta e="T408" id="Seg_3737" s="T390">Ол быракпытыгар, һэ, арыччы эбэ үөһүгэр тийбитим, тийэрин кытта буолла ол долгунугар, быракпыт долгунугар чут түөрэ бара һыспыт.</ta>
            <ta e="T412" id="Seg_3738" s="T408">Ол күрээтибит, диэбит буолла.</ta>
            <ta e="T420" id="Seg_3739" s="T412">Оччо һэттэ киһиттэн һоготок киһилээк кэлбит, октоннор бэйэлэрэ (киһилэрэ).</ta>
            <ta e="T431" id="Seg_3740" s="T420">Тугу да булбакка эргийэн кэл.., ол кэлэн ол үүһэ диэк барбыт.</ta>
            <ta e="T438" id="Seg_3741" s="T431">Онтон Чагыдайы буоллагын һайын бииргэ олороллор үһү.</ta>
            <ta e="T449" id="Seg_3742" s="T438">Аһаан олороннор ка һурук кэлбит, бу һуругу буоллагына Носкуога тириэриэктэрин.</ta>
            <ta e="T457" id="Seg_3743" s="T449">Кантан эрэ кэлбит тыыннан, оччо (оччого) туок луокката кэлиэй.</ta>
            <ta e="T467" id="Seg_3744" s="T457">"Ка, Чагыдай, бу һуругу буолла кимиэкэ тириэр, Кириэскэ, Рымнай Кириэһигэр."Көр, ыраагын, Рымнай Ынараа өттүгэр.</ta>
            <ta e="T472" id="Seg_3745" s="T467">Көр, ыраагын, Рымнай Ынараа өттүгэр.</ta>
            <ta e="T478" id="Seg_3746" s="T476">П.П: Һуок.</ta>
            <ta e="T482" id="Seg_3747" s="T478">Oл бэйэлэрин гиэттэрэ.</ta>
            <ta e="T496" id="Seg_3748" s="T482">П.П: һээ Биэгичэптэрэ һонон айаннаабыт буолла… ол барыытынан барбыт бу диэк, үүһэ диэк.</ta>
            <ta e="T510" id="Seg_3749" s="T496">Чэ, ол буо ол… ол һуругу илтэрэллэр, оччо (оччого) һуруктаактар эбитэ буолла, тойонноктор буо, oгоннь..</ta>
            <ta e="T512" id="Seg_3750" s="T511">П.П: Һакалар.</ta>
            <ta e="T516" id="Seg_3751" s="T512">Ол үлэһиттэрэ, һабиэттара туоктара.</ta>
            <ta e="T519" id="Seg_3752" s="T516">Һорук ыыталлар буо.</ta>
            <ta e="T527" id="Seg_3753" s="T519">Инньэ гын.. бу огонньор буолла дьэ киэһэ камныыр.</ta>
            <ta e="T536" id="Seg_3754" s="T527">"Һарсиэрда эрдэһит дьактар турар кэмигэр кэлиэм", – диэбит бу огонньор.</ta>
            <ta e="T547" id="Seg_3755" s="T536">"О, кайдак гынан кэлээри гынагын Рымнай ынаара өттүттэн, һүрдээк ыраак Побүгэйтэн".</ta>
            <ta e="T556" id="Seg_3756" s="T547">Дьэ, бу огонньор кааман каалбыт, э, баран каалбыт тыыннан.</ta>
            <ta e="T576" id="Seg_3757" s="T556">Һарсиэрда эмээкситтэр турбу.., эрдэһит дьактаттар тураллар, например, оччо һэттэ чаас иттэ эбит эни, туок чааһа кэлиэй, күннэринэн көрөллөр буо.</ta>
            <ta e="T590" id="Seg_3758" s="T576">Араа, һэттэ чааска Чагыдай кэлбит, тыыта умсанна һытар диин, көр, оннук корсун киһилэр бааллар.</ta>
            <ta e="T603" id="Seg_3759" s="T590">Онтон дьактар төрүүр һол һуртарыгар, бу дьактар өлөөрү гыммыт, кайдак да гыныактарын бэрт.</ta>
            <ta e="T611" id="Seg_3760" s="T603">Дуоктура һуок үйэгэ кэ, баабускалара муӈнаак һылайан бүппүт.</ta>
            <ta e="T623" id="Seg_3761" s="T611">"Кайа, Чагыдай кэллин, дьактар өлөөрү гынна, кайдак эмэ гыннын оӈуок араардын мунна".</ta>
            <ta e="T639" id="Seg_3762" s="T623">Һинньэ (Инньэ) гынан бу Чагыдай кэлэр, кайдак гы.. (гынан) бу дьактары илилээн ылар оготун, һонон тыыннак кэһэр көр.</ta>
            <ta e="T643" id="Seg_3763" s="T639">Оннуктар баар этилэр кырдьагастар. </ta>
            <ta e="T648" id="Seg_3764" s="T643">И… (Ити) Чакыыла огонньор эһэтэ онтуӈ.</ta>
            <ta e="T664" id="Seg_3765" s="T651">П.П: Һээ, оччого ол Чагыдайыӈ дьактара буоллагына эмээксин, Анна иньэтэ эмээксин, баа Чээкэ эмээксин.</ta>
            <ta e="T673" id="Seg_3766" s="T664">Онтуӈ тээтэтэ биһиги эһээбитин кытта бииргэ төрөөбүттэр Кудрякоп огонньор.</ta>
         </annotation>
         <annotation name="ts" tierref="ts-KiPP">
            <ta e="T2" id="Seg_3767" s="T0">– Bi͡egičebi illeller. </ta>
            <ta e="T9" id="Seg_3768" s="T2">(Hit-) hirinen hɨldʼar ete urut, espʼidʼisʼijalar. </ta>
            <ta e="T12" id="Seg_3769" s="T9">Onu ehem kepseːčči. </ta>
            <ta e="T23" id="Seg_3770" s="T12">Mini͡ene inʼem, (ehe) iti inʼem uruːlara, Markʼel ogonnʼor, Ölöːnö agata ogonnʼor. </ta>
            <ta e="T26" id="Seg_3771" s="T23">Olor kepsetellerin isteːččibin. </ta>
            <ta e="T35" id="Seg_3772" s="T26">Bu Bi͡egičep bu͡ollagɨna tabannan ajannɨːr Bi͡egičep arɨːtɨgar, hette kihileːk. </ta>
            <ta e="T48" id="Seg_3773" s="T35">Bu kihi ajannaːn ihen, tabannan illeller bu͡o, atɨntan-atɨn tabannan illeller, haŋattan-haŋa tabannan illeller. </ta>
            <ta e="T57" id="Seg_3774" s="T48">Onno Čagɨdaj ogonnʼoru iltereller bu͡olla, min ogonnʼorum ehetineːk, ehelere. </ta>
            <ta e="T67" id="Seg_3775" s="T57">Ol ogonnʼor tu͡ok daː hippetek kihite ete – hürdeːk kamnastaːk kihi. </ta>
            <ta e="T77" id="Seg_3776" s="T67">Ol (ogonnʼor-) hataːbat tabalarɨ kölütteren baraːn, Bi͡egičep arɨːtɨgar iltereller bu͡o. </ta>
            <ta e="T83" id="Seg_3777" s="T77">Onno tabalar möŋöllör ühü, i͡e diː. </ta>
            <ta e="T96" id="Seg_3778" s="T83">Tabalar möŋöllör, bu kihi bergehetin, ol Bi͡egičep gi͡enin bergehetin taba iːlen bɨragar ühü. </ta>
            <ta e="T114" id="Seg_3779" s="T96">Bu hɨrganɨ kurdarɨ bergehe tühen erdegine, onu ogonnʼor hɨrganɨ kurdarɨ ojon, kaban ɨlbɨt bu͡o, hirge tühü͡ögün betereː öttüger. </ta>
            <ta e="T121" id="Seg_3780" s="T114">Onu ol (ogonnʼ-) Bi͡egičebe diːr ühü: </ta>
            <ta e="T130" id="Seg_3781" s="T121">"Oː, haka hiriger da kamnastaːk kihi baːr bu͡olar ((…))." </ta>
            <ta e="T142" id="Seg_3782" s="T130">Dʼe, bu ogonnʼor, Bi͡egičep bu͡ollagɨna ol, "ol arɨːga tiːjbitim", di͡en kepsiːr ühü. </ta>
            <ta e="T152" id="Seg_3783" s="T142">"Bu tu͡ok oduːnu kördüŋ onno", di͡en ɨjɨtallar hakalar, i͡e di. </ta>
            <ta e="T153" id="Seg_3784" s="T152">Onuga: </ta>
            <ta e="T169" id="Seg_3785" s="T153">"Min oduːnu tugu da olus bulbatɨm, marsalara bu͡ollagɨna tiːsteritten baːjan baran utujallar, utuja hɨtallarɨn baːjan bremnoːttan. </ta>
            <ta e="T178" id="Seg_3786" s="T169">Onu hep daː di͡ebetter, hagastaːn kiːllereller, uːga kiːlleren keːheller. </ta>
            <ta e="T188" id="Seg_3787" s="T178">Onton araj arɨːga taːs kajaga bullum", diːr ühü, "biːr oduːnu." </ta>
            <ta e="T191" id="Seg_3788" s="T188">"Dʼe, tugu bulluŋ?" </ta>
            <ta e="T194" id="Seg_3789" s="T191">Onu diːr bu͡o: </ta>
            <ta e="T213" id="Seg_3790" s="T209">"Onuga kiːren bardɨm", diːr. </ta>
            <ta e="T220" id="Seg_3791" s="T213">Üs kihileːk, bu kihiler aha hu͡ok bu͡olbupput. </ta>
            <ta e="T232" id="Seg_3792" s="T220">Oččogo bu͡ollagɨna ebetten, o, kɨra balɨkkaːnɨ kabɨnannar ol aːŋŋa kiːrenner, ahaːrɨlar kiːrbitter. </ta>
            <ta e="T239" id="Seg_3793" s="T232">Bu kihi araj kiːrbite, bu kihi oloror. </ta>
            <ta e="T246" id="Seg_3794" s="T239">Olordoguna tu͡ok ere kihite kelbit, i͡e diː. </ta>
            <ta e="T250" id="Seg_3795" s="T246">((NOISE)) Kihi duː, tu͡ok duː? </ta>
            <ta e="T263" id="Seg_3796" s="T250">"Hannɨttan menʼiːte hu͡ok kihi kiːrbit, kihi duː, tu͡ok duː, tugu daː tu͡olkulaːbappɨn", di͡ebit. </ta>
            <ta e="T269" id="Seg_3797" s="T263">Ol kiːren aːŋŋa olorbut, i͡e diː. </ta>
            <ta e="T273" id="Seg_3798" s="T269">Ol oloron bu͡ollagɨna, kajaː. </ta>
            <ta e="T281" id="Seg_3799" s="T273">Külen ɨrdʼaŋnaːbɨt, kanna ere karaktaːk, töbötüger hogotok karaktaːk. </ta>
            <ta e="T292" id="Seg_3800" s="T281">"Kaja, (tu͡o-) dogottor, bigetik tuttuŋ", diːr bu͡o dogorun, u͡olattarɨn Bi͡egičep. </ta>
            <ta e="T300" id="Seg_3801" s="T292">Ontuta "huːp" di͡en oborbut, ikki kihitin oboron ɨlbɨt. </ta>
            <ta e="T306" id="Seg_3802" s="T300">Oččo bu kihi hogotok kihileːk kaːlbɨt. </ta>
            <ta e="T313" id="Seg_3803" s="T306">Onno bu kihi bu͡olla pʼistalʼettaːk ebit ol. </ta>
            <ta e="T321" id="Seg_3804" s="T313">"Onno diːbin, haŋarar tɨlɨnan haŋarabɨn", di͡ebit, "pʼistalʼetɨm anʼagɨgar. </ta>
            <ta e="T327" id="Seg_3805" s="T321">Ka, bu dojduga kaːlabɨn du͡o min?" </ta>
            <ta e="T336" id="Seg_3806" s="T327">"Minigin bɨːhaː, bunu kanna eme uŋarar hirger tiri͡er", diːr. </ta>
            <ta e="T345" id="Seg_3807" s="T336">Hüːhüger ɨppɨt bu͡olla bu pʼistalʼetɨnan, ontuta dʼe uŋan tüspüt. </ta>
            <ta e="T352" id="Seg_3808" s="T345">Ol uŋan tüspütüger ürdünnen hüːren taksɨbɨttar bu͡ollar. </ta>
            <ta e="T356" id="Seg_3809" s="T352">Bi͡egičebiŋ kepsiːr (kepsi-): </ta>
            <ta e="T370" id="Seg_3810" s="T356">"Ol tijemmit kimi͡eke bardɨbɨt", diːr bu͡o, "kaːtʼerbar bardɨm", diːr, "kaːtʼerbar olorommun ebe ü͡öhüger kiːrdim. </ta>
            <ta e="T376" id="Seg_3811" s="T370">Körbütüm, kennibitten baːjdʼɨgɨm turan kelle, di͡en. </ta>
            <ta e="T390" id="Seg_3812" s="T376">Turan kelen bu͡ollagɨna kaːtʼerdaːgar daː ulakan boldogu ɨlan baraːn, dʼe bɨrakpɨt kim dʼe, ɨttaraːččɨta. </ta>
            <ta e="T408" id="Seg_3813" s="T390">Ol bɨrakpɨtɨgar, he, arɨččɨ ebe ü͡öhüger tiːjbitim, tiːjerin kɨtta bu͡olla ol dolgunugar, bɨrakpɨt dolgunugar čut tü͡öre bara hɨspɨt. </ta>
            <ta e="T412" id="Seg_3814" s="T408">Ol küreːtibit", di͡ebit bu͡olla. </ta>
            <ta e="T420" id="Seg_3815" s="T412">Oččo hette kihitten hogotok kihileːk kelbit, oktonnor bejelere. </ta>
            <ta e="T431" id="Seg_3816" s="T420">Tugu da bulbakka ergijen (kel-), ol kelen ol üːhe di͡ek barbɨt. </ta>
            <ta e="T438" id="Seg_3817" s="T431">Onton Čagɨdajɨ bu͡ollagɨn hajɨn biːrge olorollor ühü. </ta>
            <ta e="T449" id="Seg_3818" s="T438">Ahaːn oloronnor ka huruk kelbit, bu hurugu bu͡ollagɨna Nosku͡oga tiri͡eri͡ekterin naːda. </ta>
            <ta e="T457" id="Seg_3819" s="T449">Kantan ere kelbit tɨːnnan, oččo tu͡ok lu͡okkata keli͡ej. </ta>
            <ta e="T467" id="Seg_3820" s="T457">"Ka, Čagɨdaj, bu hurugu bu͡olla kimi͡eke tiri͡er, Kiri͡eske, Rɨmnaj Kiri͡ehiger." </ta>
            <ta e="T472" id="Seg_3821" s="T467">Kör, ɨraːgɨn, Rɨmnaj ɨnaraː öttüger. </ta>
            <ta e="T478" id="Seg_3822" s="T476">– Hu͡ok. </ta>
            <ta e="T482" id="Seg_3823" s="T478">Ol bejelerin gi͡ettere. </ta>
            <ta e="T496" id="Seg_3824" s="T482">– Heː Bi͡egičeptere honon ajannaːbɨt bu͡olla, ol barɨːtɨnan barbɨt bu di͡ek, üːhe di͡ek. </ta>
            <ta e="T510" id="Seg_3825" s="T496">Če, ol bu͡o ol hurugu iltereller, oččo huruktaːktar ebite bu͡olla, tojonnoːktor bu͡o, (ogonnʼ-)… </ta>
            <ta e="T512" id="Seg_3826" s="T511">– Hakalar. </ta>
            <ta e="T516" id="Seg_3827" s="T512">Ol ülehittere, habi͡ettara tu͡oktara. </ta>
            <ta e="T519" id="Seg_3828" s="T516">Huruk ɨːtallar bu͡o. </ta>
            <ta e="T527" id="Seg_3829" s="T519">Innʼe (gɨn-) bu ogonnʼor bu͡olla dʼe ki͡ehe kamnɨːr. </ta>
            <ta e="T536" id="Seg_3830" s="T527">"Harsi͡erda erdehit dʼaktar turar kemiger keli͡em", di͡ebit bu ogonnʼor. </ta>
            <ta e="T547" id="Seg_3831" s="T536">"O, kajdak gɨnan keleːri gɨnagɨn Rɨmnaj ɨnaːra öttütten, hürdeːk ɨraːk Pobügejten." </ta>
            <ta e="T556" id="Seg_3832" s="T547">Dʼe, bu ogonnʼor kaːman kaːlbɨt, e, baran kaːlbɨt tɨːnnan. </ta>
            <ta e="T576" id="Seg_3833" s="T556">Harsi͡erda emeːksitter (turbu-), erdehit dʼaktattar turallar, naprimer, oččo hette čaːs itte ebit eni, tu͡ok čaːha keli͡ej, künnerinen köröllör bu͡o. </ta>
            <ta e="T590" id="Seg_3834" s="T576">Araː, hette čaːska Čagɨdaj kelbit, tɨːta umsana hɨtar diːn, kör, onnuk korsun kihiler baːllar. </ta>
            <ta e="T603" id="Seg_3835" s="T590">Onton dʼaktar törüːr hol huːrtarɨgar, bu dʼaktar ölöːrü gɨmmɨt, kajdak da gɨnɨ͡aktarɨn bert. </ta>
            <ta e="T611" id="Seg_3836" s="T603">Du͡oktura hu͡ok üjege ke, baːbɨskalara muŋnaːk hɨlajan büppüt. </ta>
            <ta e="T623" id="Seg_3837" s="T611">"Kaja, Čagɨdaj kellin, dʼaktar ölöːrü gɨnna, kajdak eme gɨnnɨn oŋu͡ok araːrdɨn munna." </ta>
            <ta e="T639" id="Seg_3838" s="T623">Hinnʼe bu Čagɨdaj keler, kajdak (gɨ-) bu dʼaktarɨ iliːleːn ɨlar ogotun, honon tɨːnnak keːher kör. </ta>
            <ta e="T643" id="Seg_3839" s="T639">Onnuktar baːr etiler kɨrdʼagastar. </ta>
            <ta e="T648" id="Seg_3840" s="T643">I Čakɨːla ogonnʼor ehete ontuŋ. </ta>
            <ta e="T664" id="Seg_3841" s="T651">– Heː, oččogo ol Čagɨdajɨŋ dʼaktara bu͡ollagɨna emeːksin, Anna inʼete emeːksin, baː Čeːke emeːksin. </ta>
            <ta e="T673" id="Seg_3842" s="T664">Ontuŋ teːtete bihigi eheːbitin kɨtta biːrge töröːbütter Kudrʼakop ogonnʼor. </ta>
         </annotation>
         <annotation name="mb" tierref="mb-KiPP">
            <ta e="T1" id="Seg_3843" s="T0">Bi͡egičeb-i</ta>
            <ta e="T2" id="Seg_3844" s="T1">ill-el-ler</ta>
            <ta e="T5" id="Seg_3845" s="T4">hir-i-nen</ta>
            <ta e="T6" id="Seg_3846" s="T5">hɨldʼ-ar</ta>
            <ta e="T7" id="Seg_3847" s="T6">e-t-e</ta>
            <ta e="T8" id="Seg_3848" s="T7">urut</ta>
            <ta e="T9" id="Seg_3849" s="T8">espʼidʼisʼija-lar</ta>
            <ta e="T10" id="Seg_3850" s="T9">o-nu</ta>
            <ta e="T11" id="Seg_3851" s="T10">ehe-m</ta>
            <ta e="T12" id="Seg_3852" s="T11">kepseː-čči</ta>
            <ta e="T13" id="Seg_3853" s="T12">mini͡ene</ta>
            <ta e="T14" id="Seg_3854" s="T13">inʼe-m</ta>
            <ta e="T15" id="Seg_3855" s="T14">ehe</ta>
            <ta e="T16" id="Seg_3856" s="T15">iti</ta>
            <ta e="T17" id="Seg_3857" s="T16">inʼe-m</ta>
            <ta e="T18" id="Seg_3858" s="T17">uruː-lar-a</ta>
            <ta e="T19" id="Seg_3859" s="T18">Markʼel</ta>
            <ta e="T20" id="Seg_3860" s="T19">ogonnʼor</ta>
            <ta e="T21" id="Seg_3861" s="T20">Ölöːnö</ta>
            <ta e="T22" id="Seg_3862" s="T21">aga-ta</ta>
            <ta e="T23" id="Seg_3863" s="T22">ogonnʼor</ta>
            <ta e="T24" id="Seg_3864" s="T23">o-lor</ta>
            <ta e="T25" id="Seg_3865" s="T24">kepset-el-leri-n</ta>
            <ta e="T26" id="Seg_3866" s="T25">ist-eːčči-bin</ta>
            <ta e="T27" id="Seg_3867" s="T26">bu</ta>
            <ta e="T28" id="Seg_3868" s="T27">Bi͡egičep</ta>
            <ta e="T29" id="Seg_3869" s="T28">bu͡ollagɨna</ta>
            <ta e="T30" id="Seg_3870" s="T29">taba-nnan</ta>
            <ta e="T31" id="Seg_3871" s="T30">ajannɨː-r</ta>
            <ta e="T32" id="Seg_3872" s="T31">Bi͡egičep</ta>
            <ta e="T33" id="Seg_3873" s="T32">arɨː-tɨ-gar</ta>
            <ta e="T34" id="Seg_3874" s="T33">hette</ta>
            <ta e="T35" id="Seg_3875" s="T34">kihi-leːk</ta>
            <ta e="T36" id="Seg_3876" s="T35">bu</ta>
            <ta e="T37" id="Seg_3877" s="T36">kihi</ta>
            <ta e="T38" id="Seg_3878" s="T37">ajannaː-n</ta>
            <ta e="T39" id="Seg_3879" s="T38">ih-en</ta>
            <ta e="T40" id="Seg_3880" s="T39">taba-nnan</ta>
            <ta e="T41" id="Seg_3881" s="T40">ill-el-ler</ta>
            <ta e="T42" id="Seg_3882" s="T41">bu͡o</ta>
            <ta e="T43" id="Seg_3883" s="T42">atɨn-tan-atɨn</ta>
            <ta e="T44" id="Seg_3884" s="T43">taba-nnan</ta>
            <ta e="T45" id="Seg_3885" s="T44">ill-el-ler</ta>
            <ta e="T46" id="Seg_3886" s="T45">haŋa-ttan-haŋa</ta>
            <ta e="T47" id="Seg_3887" s="T46">taba-nnan</ta>
            <ta e="T48" id="Seg_3888" s="T47">ill-el-ler</ta>
            <ta e="T49" id="Seg_3889" s="T48">onno</ta>
            <ta e="T50" id="Seg_3890" s="T49">Čagɨdaj</ta>
            <ta e="T51" id="Seg_3891" s="T50">ogonnʼor-u</ta>
            <ta e="T52" id="Seg_3892" s="T51">ilt-e-r-el-ler</ta>
            <ta e="T53" id="Seg_3893" s="T52">bu͡olla</ta>
            <ta e="T54" id="Seg_3894" s="T53">min</ta>
            <ta e="T55" id="Seg_3895" s="T54">ogonnʼor-u-m</ta>
            <ta e="T56" id="Seg_3896" s="T55">ehe-ti-n-eːk</ta>
            <ta e="T57" id="Seg_3897" s="T56">ehe-lere</ta>
            <ta e="T58" id="Seg_3898" s="T57">ol</ta>
            <ta e="T59" id="Seg_3899" s="T58">ogonnʼor</ta>
            <ta e="T60" id="Seg_3900" s="T59">tu͡ok</ta>
            <ta e="T61" id="Seg_3901" s="T60">daː</ta>
            <ta e="T62" id="Seg_3902" s="T61">hip-petek</ta>
            <ta e="T63" id="Seg_3903" s="T62">kihi-te</ta>
            <ta e="T64" id="Seg_3904" s="T63">e-t-e</ta>
            <ta e="T65" id="Seg_3905" s="T64">hürdeːk</ta>
            <ta e="T66" id="Seg_3906" s="T65">kamnastaːk</ta>
            <ta e="T67" id="Seg_3907" s="T66">kihi</ta>
            <ta e="T68" id="Seg_3908" s="T67">ol</ta>
            <ta e="T69" id="Seg_3909" s="T68">ogonnʼor</ta>
            <ta e="T70" id="Seg_3910" s="T69">hataː-bat</ta>
            <ta e="T71" id="Seg_3911" s="T70">taba-lar-ɨ</ta>
            <ta e="T72" id="Seg_3912" s="T71">kölüt-ter-en</ta>
            <ta e="T73" id="Seg_3913" s="T72">baraːn</ta>
            <ta e="T74" id="Seg_3914" s="T73">Bi͡egičep</ta>
            <ta e="T75" id="Seg_3915" s="T74">arɨː-tɨ-gar</ta>
            <ta e="T76" id="Seg_3916" s="T75">ilt-e-r-el-ler</ta>
            <ta e="T77" id="Seg_3917" s="T76">bu͡o</ta>
            <ta e="T78" id="Seg_3918" s="T77">onno</ta>
            <ta e="T79" id="Seg_3919" s="T78">taba-lar</ta>
            <ta e="T80" id="Seg_3920" s="T79">möŋ-öl-lör</ta>
            <ta e="T81" id="Seg_3921" s="T80">ühü</ta>
            <ta e="T82" id="Seg_3922" s="T81">i͡e</ta>
            <ta e="T83" id="Seg_3923" s="T82">diː</ta>
            <ta e="T84" id="Seg_3924" s="T83">taba-lar</ta>
            <ta e="T85" id="Seg_3925" s="T84">möŋ-öl-lör</ta>
            <ta e="T86" id="Seg_3926" s="T85">bu</ta>
            <ta e="T87" id="Seg_3927" s="T86">kihi</ta>
            <ta e="T88" id="Seg_3928" s="T87">bergehe-ti-n</ta>
            <ta e="T89" id="Seg_3929" s="T88">ol</ta>
            <ta e="T90" id="Seg_3930" s="T89">Bi͡egičep</ta>
            <ta e="T91" id="Seg_3931" s="T90">gi͡en-i-n</ta>
            <ta e="T92" id="Seg_3932" s="T91">bergehe-ti-n</ta>
            <ta e="T93" id="Seg_3933" s="T92">taba</ta>
            <ta e="T94" id="Seg_3934" s="T93">iːl-en</ta>
            <ta e="T95" id="Seg_3935" s="T94">bɨrag-ar</ta>
            <ta e="T96" id="Seg_3936" s="T95">ühü</ta>
            <ta e="T97" id="Seg_3937" s="T96">bu</ta>
            <ta e="T98" id="Seg_3938" s="T97">hɨrga-nɨ</ta>
            <ta e="T99" id="Seg_3939" s="T98">kurdarɨ</ta>
            <ta e="T100" id="Seg_3940" s="T99">bergehe</ta>
            <ta e="T101" id="Seg_3941" s="T100">tüh-en</ta>
            <ta e="T102" id="Seg_3942" s="T101">er-deg-ine</ta>
            <ta e="T103" id="Seg_3943" s="T102">o-nu</ta>
            <ta e="T104" id="Seg_3944" s="T103">ogonnʼor</ta>
            <ta e="T105" id="Seg_3945" s="T104">hɨrga-nɨ</ta>
            <ta e="T106" id="Seg_3946" s="T105">kurdarɨ</ta>
            <ta e="T107" id="Seg_3947" s="T106">oj-on</ta>
            <ta e="T108" id="Seg_3948" s="T107">kab-an</ta>
            <ta e="T109" id="Seg_3949" s="T108">ɨl-bɨt</ta>
            <ta e="T110" id="Seg_3950" s="T109">bu͡o</ta>
            <ta e="T111" id="Seg_3951" s="T110">hir-ge</ta>
            <ta e="T112" id="Seg_3952" s="T111">tüh-ü͡ög-ü-n</ta>
            <ta e="T113" id="Seg_3953" s="T112">betereː</ta>
            <ta e="T114" id="Seg_3954" s="T113">ött-ü-ger</ta>
            <ta e="T115" id="Seg_3955" s="T114">o-nu</ta>
            <ta e="T116" id="Seg_3956" s="T115">ol</ta>
            <ta e="T119" id="Seg_3957" s="T118">Bi͡egičeb-e</ta>
            <ta e="T120" id="Seg_3958" s="T119">diː-r</ta>
            <ta e="T121" id="Seg_3959" s="T120">ühü</ta>
            <ta e="T122" id="Seg_3960" s="T121">oː</ta>
            <ta e="T123" id="Seg_3961" s="T122">haka</ta>
            <ta e="T124" id="Seg_3962" s="T123">hir-i-ger</ta>
            <ta e="T125" id="Seg_3963" s="T124">da</ta>
            <ta e="T126" id="Seg_3964" s="T125">kamnastaːk</ta>
            <ta e="T127" id="Seg_3965" s="T126">kihi</ta>
            <ta e="T128" id="Seg_3966" s="T127">baːr</ta>
            <ta e="T129" id="Seg_3967" s="T128">bu͡ol-ar</ta>
            <ta e="T131" id="Seg_3968" s="T130">dʼe</ta>
            <ta e="T132" id="Seg_3969" s="T131">bu</ta>
            <ta e="T133" id="Seg_3970" s="T132">ogonnʼor</ta>
            <ta e="T134" id="Seg_3971" s="T133">Bi͡egičep</ta>
            <ta e="T135" id="Seg_3972" s="T134">bu͡ollagɨna</ta>
            <ta e="T136" id="Seg_3973" s="T135">ol</ta>
            <ta e="T137" id="Seg_3974" s="T136">ol</ta>
            <ta e="T138" id="Seg_3975" s="T137">arɨː-ga</ta>
            <ta e="T139" id="Seg_3976" s="T138">tiːj-bit-i-m</ta>
            <ta e="T140" id="Seg_3977" s="T139">di͡e-n</ta>
            <ta e="T141" id="Seg_3978" s="T140">kepsiː-r</ta>
            <ta e="T142" id="Seg_3979" s="T141">ühü</ta>
            <ta e="T143" id="Seg_3980" s="T142">bu</ta>
            <ta e="T144" id="Seg_3981" s="T143">tu͡ok</ta>
            <ta e="T145" id="Seg_3982" s="T144">oduː-nu</ta>
            <ta e="T146" id="Seg_3983" s="T145">kör-dü-ŋ</ta>
            <ta e="T147" id="Seg_3984" s="T146">onno</ta>
            <ta e="T148" id="Seg_3985" s="T147">di͡e-n</ta>
            <ta e="T149" id="Seg_3986" s="T148">ɨjɨt-al-lar</ta>
            <ta e="T150" id="Seg_3987" s="T149">haka-lar</ta>
            <ta e="T151" id="Seg_3988" s="T150">i͡e</ta>
            <ta e="T152" id="Seg_3989" s="T151">di</ta>
            <ta e="T153" id="Seg_3990" s="T152">onu-ga</ta>
            <ta e="T154" id="Seg_3991" s="T153">min</ta>
            <ta e="T155" id="Seg_3992" s="T154">oduː-nu</ta>
            <ta e="T156" id="Seg_3993" s="T155">tug-u</ta>
            <ta e="T157" id="Seg_3994" s="T156">da</ta>
            <ta e="T158" id="Seg_3995" s="T157">olus</ta>
            <ta e="T159" id="Seg_3996" s="T158">bul-ba-tɨ-m</ta>
            <ta e="T160" id="Seg_3997" s="T159">marsa-lara</ta>
            <ta e="T161" id="Seg_3998" s="T160">bu͡ollagɨna</ta>
            <ta e="T162" id="Seg_3999" s="T161">tiːs-teri-tten</ta>
            <ta e="T163" id="Seg_4000" s="T162">baːj-an</ta>
            <ta e="T164" id="Seg_4001" s="T163">baran</ta>
            <ta e="T165" id="Seg_4002" s="T164">utuj-al-lar</ta>
            <ta e="T166" id="Seg_4003" s="T165">utuj-a</ta>
            <ta e="T167" id="Seg_4004" s="T166">hɨt-al-larɨ-n</ta>
            <ta e="T168" id="Seg_4005" s="T167">baːj-an</ta>
            <ta e="T169" id="Seg_4006" s="T168">brevnoː-ttan</ta>
            <ta e="T170" id="Seg_4007" s="T169">o-nu</ta>
            <ta e="T171" id="Seg_4008" s="T170">hep</ta>
            <ta e="T172" id="Seg_4009" s="T171">daː</ta>
            <ta e="T173" id="Seg_4010" s="T172">di͡e-bet-ter</ta>
            <ta e="T174" id="Seg_4011" s="T173">hagastaː-n</ta>
            <ta e="T175" id="Seg_4012" s="T174">kiːl-ler-el-ler</ta>
            <ta e="T176" id="Seg_4013" s="T175">uː-ga</ta>
            <ta e="T177" id="Seg_4014" s="T176">kiːl-ler-en</ta>
            <ta e="T178" id="Seg_4015" s="T177">keːh-el-ler</ta>
            <ta e="T179" id="Seg_4016" s="T178">onton</ta>
            <ta e="T180" id="Seg_4017" s="T179">araj</ta>
            <ta e="T181" id="Seg_4018" s="T180">arɨː-ga</ta>
            <ta e="T182" id="Seg_4019" s="T181">taːs</ta>
            <ta e="T183" id="Seg_4020" s="T182">kaja-ga</ta>
            <ta e="T184" id="Seg_4021" s="T183">bul-lu-m</ta>
            <ta e="T185" id="Seg_4022" s="T184">diː-r</ta>
            <ta e="T186" id="Seg_4023" s="T185">ühü</ta>
            <ta e="T187" id="Seg_4024" s="T186">biːr</ta>
            <ta e="T188" id="Seg_4025" s="T187">oduː-nu</ta>
            <ta e="T189" id="Seg_4026" s="T188">dʼe</ta>
            <ta e="T190" id="Seg_4027" s="T189">tug-u</ta>
            <ta e="T191" id="Seg_4028" s="T190">bul-lu-ŋ</ta>
            <ta e="T192" id="Seg_4029" s="T191">o-nu</ta>
            <ta e="T193" id="Seg_4030" s="T192">diː-r</ta>
            <ta e="T194" id="Seg_4031" s="T193">bu͡o</ta>
            <ta e="T210" id="Seg_4032" s="T209">onu-ga</ta>
            <ta e="T211" id="Seg_4033" s="T210">kiːr-en</ta>
            <ta e="T212" id="Seg_4034" s="T211">bar-dɨ-m</ta>
            <ta e="T213" id="Seg_4035" s="T212">diː-r</ta>
            <ta e="T214" id="Seg_4036" s="T213">üs</ta>
            <ta e="T215" id="Seg_4037" s="T214">kihi-leːk</ta>
            <ta e="T216" id="Seg_4038" s="T215">bu</ta>
            <ta e="T217" id="Seg_4039" s="T216">kihi-ler</ta>
            <ta e="T218" id="Seg_4040" s="T217">ah-a</ta>
            <ta e="T219" id="Seg_4041" s="T218">hu͡ok</ta>
            <ta e="T220" id="Seg_4042" s="T219">bu͡ol-bup-put</ta>
            <ta e="T221" id="Seg_4043" s="T220">oččogo</ta>
            <ta e="T222" id="Seg_4044" s="T221">bu͡ollagɨna</ta>
            <ta e="T223" id="Seg_4045" s="T222">ebe-tten</ta>
            <ta e="T224" id="Seg_4046" s="T223">o</ta>
            <ta e="T225" id="Seg_4047" s="T224">kɨra</ta>
            <ta e="T226" id="Seg_4048" s="T225">balɨk-kaːn-ɨ</ta>
            <ta e="T227" id="Seg_4049" s="T226">kab-ɨ-n-an-nar</ta>
            <ta e="T228" id="Seg_4050" s="T227">ol</ta>
            <ta e="T229" id="Seg_4051" s="T228">aːŋ-ŋa</ta>
            <ta e="T230" id="Seg_4052" s="T229">kiːr-en-ner</ta>
            <ta e="T231" id="Seg_4053" s="T230">ah-aːrɨ-lar</ta>
            <ta e="T232" id="Seg_4054" s="T231">kiːr-bit-ter</ta>
            <ta e="T233" id="Seg_4055" s="T232">bu</ta>
            <ta e="T234" id="Seg_4056" s="T233">kihi</ta>
            <ta e="T235" id="Seg_4057" s="T234">araj</ta>
            <ta e="T236" id="Seg_4058" s="T235">kiːr-bit-e</ta>
            <ta e="T237" id="Seg_4059" s="T236">bu</ta>
            <ta e="T238" id="Seg_4060" s="T237">kihi</ta>
            <ta e="T239" id="Seg_4061" s="T238">olor-or</ta>
            <ta e="T240" id="Seg_4062" s="T239">olor-dog-una</ta>
            <ta e="T241" id="Seg_4063" s="T240">tu͡ok</ta>
            <ta e="T242" id="Seg_4064" s="T241">ere</ta>
            <ta e="T243" id="Seg_4065" s="T242">kihi-te</ta>
            <ta e="T244" id="Seg_4066" s="T243">kel-bit</ta>
            <ta e="T245" id="Seg_4067" s="T244">i͡e</ta>
            <ta e="T246" id="Seg_4068" s="T245">diː</ta>
            <ta e="T247" id="Seg_4069" s="T246">kihi</ta>
            <ta e="T248" id="Seg_4070" s="T247">duː</ta>
            <ta e="T249" id="Seg_4071" s="T248">tu͡ok</ta>
            <ta e="T250" id="Seg_4072" s="T249">duː</ta>
            <ta e="T251" id="Seg_4073" s="T250">hannɨ-ttan</ta>
            <ta e="T252" id="Seg_4074" s="T251">menʼiː-te</ta>
            <ta e="T253" id="Seg_4075" s="T252">hu͡ok</ta>
            <ta e="T254" id="Seg_4076" s="T253">kihi</ta>
            <ta e="T255" id="Seg_4077" s="T254">kiːr-bit</ta>
            <ta e="T256" id="Seg_4078" s="T255">kihi</ta>
            <ta e="T257" id="Seg_4079" s="T256">duː</ta>
            <ta e="T258" id="Seg_4080" s="T257">tu͡ok</ta>
            <ta e="T259" id="Seg_4081" s="T258">duː</ta>
            <ta e="T260" id="Seg_4082" s="T259">tug-u</ta>
            <ta e="T261" id="Seg_4083" s="T260">daː</ta>
            <ta e="T262" id="Seg_4084" s="T261">tu͡olkulaː-bap-pɨn</ta>
            <ta e="T263" id="Seg_4085" s="T262">di͡e-bit</ta>
            <ta e="T264" id="Seg_4086" s="T263">ol</ta>
            <ta e="T265" id="Seg_4087" s="T264">kiːr-en</ta>
            <ta e="T266" id="Seg_4088" s="T265">aːŋ-ŋa</ta>
            <ta e="T267" id="Seg_4089" s="T266">olor-but</ta>
            <ta e="T268" id="Seg_4090" s="T267">i͡e</ta>
            <ta e="T269" id="Seg_4091" s="T268">diː</ta>
            <ta e="T270" id="Seg_4092" s="T269">ol</ta>
            <ta e="T271" id="Seg_4093" s="T270">olor-on</ta>
            <ta e="T272" id="Seg_4094" s="T271">bu͡ollagɨna</ta>
            <ta e="T273" id="Seg_4095" s="T272">kajaː</ta>
            <ta e="T274" id="Seg_4096" s="T273">kül-en</ta>
            <ta e="T275" id="Seg_4097" s="T274">ɨrdʼaŋnaː-bɨt</ta>
            <ta e="T276" id="Seg_4098" s="T275">kanna</ta>
            <ta e="T277" id="Seg_4099" s="T276">ere</ta>
            <ta e="T278" id="Seg_4100" s="T277">karak-taːk</ta>
            <ta e="T279" id="Seg_4101" s="T278">töbö-tü-ger</ta>
            <ta e="T280" id="Seg_4102" s="T279">hogotok</ta>
            <ta e="T281" id="Seg_4103" s="T280">karak-taːk</ta>
            <ta e="T282" id="Seg_4104" s="T281">kaja</ta>
            <ta e="T285" id="Seg_4105" s="T284">dogot-tor</ta>
            <ta e="T286" id="Seg_4106" s="T285">bige-tik</ta>
            <ta e="T287" id="Seg_4107" s="T286">tut-t-u-ŋ</ta>
            <ta e="T288" id="Seg_4108" s="T287">diː-r</ta>
            <ta e="T289" id="Seg_4109" s="T288">bu͡o</ta>
            <ta e="T290" id="Seg_4110" s="T289">dogor-u-n</ta>
            <ta e="T291" id="Seg_4111" s="T290">u͡olat-tar-ɨ-n</ta>
            <ta e="T292" id="Seg_4112" s="T291">Bi͡egičep</ta>
            <ta e="T293" id="Seg_4113" s="T292">on-tu-ta</ta>
            <ta e="T294" id="Seg_4114" s="T293">huːp</ta>
            <ta e="T295" id="Seg_4115" s="T294">di͡e-n</ta>
            <ta e="T296" id="Seg_4116" s="T295">obor-but</ta>
            <ta e="T297" id="Seg_4117" s="T296">ikki</ta>
            <ta e="T298" id="Seg_4118" s="T297">kihi-ti-n</ta>
            <ta e="T299" id="Seg_4119" s="T298">obor-on</ta>
            <ta e="T300" id="Seg_4120" s="T299">ɨl-bɨt</ta>
            <ta e="T301" id="Seg_4121" s="T300">oččo</ta>
            <ta e="T302" id="Seg_4122" s="T301">bu</ta>
            <ta e="T303" id="Seg_4123" s="T302">kihi</ta>
            <ta e="T304" id="Seg_4124" s="T303">hogotok</ta>
            <ta e="T305" id="Seg_4125" s="T304">kihi-leːk</ta>
            <ta e="T306" id="Seg_4126" s="T305">kaːl-bɨt</ta>
            <ta e="T307" id="Seg_4127" s="T306">onno</ta>
            <ta e="T308" id="Seg_4128" s="T307">bu</ta>
            <ta e="T309" id="Seg_4129" s="T308">kihi</ta>
            <ta e="T310" id="Seg_4130" s="T309">bu͡olla</ta>
            <ta e="T311" id="Seg_4131" s="T310">pʼistalʼet-taːk</ta>
            <ta e="T312" id="Seg_4132" s="T311">e-bit</ta>
            <ta e="T313" id="Seg_4133" s="T312">ol</ta>
            <ta e="T314" id="Seg_4134" s="T313">onno</ta>
            <ta e="T315" id="Seg_4135" s="T314">d-iː-bin</ta>
            <ta e="T316" id="Seg_4136" s="T315">haŋar-ar</ta>
            <ta e="T317" id="Seg_4137" s="T316">tɨl-ɨ-nan</ta>
            <ta e="T318" id="Seg_4138" s="T317">haŋar-a-bɨn</ta>
            <ta e="T319" id="Seg_4139" s="T318">di͡e-bit</ta>
            <ta e="T320" id="Seg_4140" s="T319">pʼistalʼet-ɨ-m</ta>
            <ta e="T321" id="Seg_4141" s="T320">anʼag-ɨ-gar</ta>
            <ta e="T322" id="Seg_4142" s="T321">ka</ta>
            <ta e="T323" id="Seg_4143" s="T322">bu</ta>
            <ta e="T324" id="Seg_4144" s="T323">dojdu-ga</ta>
            <ta e="T325" id="Seg_4145" s="T324">kaːl-a-bɨn</ta>
            <ta e="T326" id="Seg_4146" s="T325">du͡o</ta>
            <ta e="T327" id="Seg_4147" s="T326">min</ta>
            <ta e="T328" id="Seg_4148" s="T327">minigi-n</ta>
            <ta e="T329" id="Seg_4149" s="T328">bɨːhaː</ta>
            <ta e="T330" id="Seg_4150" s="T329">bu-nu</ta>
            <ta e="T331" id="Seg_4151" s="T330">kanna</ta>
            <ta e="T332" id="Seg_4152" s="T331">eme</ta>
            <ta e="T333" id="Seg_4153" s="T332">uŋ-a-r-ar</ta>
            <ta e="T334" id="Seg_4154" s="T333">hir-ge-r</ta>
            <ta e="T335" id="Seg_4155" s="T334">tiri͡er</ta>
            <ta e="T336" id="Seg_4156" s="T335">diː-r</ta>
            <ta e="T337" id="Seg_4157" s="T336">hüːh-ü-ger</ta>
            <ta e="T338" id="Seg_4158" s="T337">ɨp-pɨt</ta>
            <ta e="T339" id="Seg_4159" s="T338">bu͡olla</ta>
            <ta e="T340" id="Seg_4160" s="T339">bu</ta>
            <ta e="T341" id="Seg_4161" s="T340">pʼistalʼet-ɨ-nan</ta>
            <ta e="T342" id="Seg_4162" s="T341">on-tu-ta</ta>
            <ta e="T343" id="Seg_4163" s="T342">dʼe</ta>
            <ta e="T344" id="Seg_4164" s="T343">uŋ-an</ta>
            <ta e="T345" id="Seg_4165" s="T344">tüs-püt</ta>
            <ta e="T346" id="Seg_4166" s="T345">ol</ta>
            <ta e="T347" id="Seg_4167" s="T346">uŋ-an</ta>
            <ta e="T348" id="Seg_4168" s="T347">tüs-püt-ü-ger</ta>
            <ta e="T349" id="Seg_4169" s="T348">ürd-ü-nnen</ta>
            <ta e="T350" id="Seg_4170" s="T349">hüːr-en</ta>
            <ta e="T351" id="Seg_4171" s="T350">taks-ɨ-bɨt-tar</ta>
            <ta e="T352" id="Seg_4172" s="T351">bu͡ol-lar</ta>
            <ta e="T353" id="Seg_4173" s="T352">Bi͡egičeb-i-ŋ</ta>
            <ta e="T354" id="Seg_4174" s="T353">kepsiː-r</ta>
            <ta e="T357" id="Seg_4175" s="T356">ol</ta>
            <ta e="T358" id="Seg_4176" s="T357">tij-em-mit</ta>
            <ta e="T359" id="Seg_4177" s="T358">kimi͡e-ke</ta>
            <ta e="T360" id="Seg_4178" s="T359">bar-dɨ-bɨt</ta>
            <ta e="T361" id="Seg_4179" s="T360">diː-r</ta>
            <ta e="T362" id="Seg_4180" s="T361">bu͡o</ta>
            <ta e="T363" id="Seg_4181" s="T362">kaːtʼer-ba-r</ta>
            <ta e="T364" id="Seg_4182" s="T363">bar-dɨ-m</ta>
            <ta e="T365" id="Seg_4183" s="T364">diː-r</ta>
            <ta e="T366" id="Seg_4184" s="T365">kaːtʼer-ba-r</ta>
            <ta e="T367" id="Seg_4185" s="T366">olor-om-mun</ta>
            <ta e="T368" id="Seg_4186" s="T367">ebe</ta>
            <ta e="T369" id="Seg_4187" s="T368">ü͡öh-ü-ger</ta>
            <ta e="T370" id="Seg_4188" s="T369">kiːr-di-m</ta>
            <ta e="T371" id="Seg_4189" s="T370">kör-büt-ü-m</ta>
            <ta e="T372" id="Seg_4190" s="T371">kenni-bi-tten</ta>
            <ta e="T373" id="Seg_4191" s="T372">baːjdʼɨg-ɨ-m</ta>
            <ta e="T374" id="Seg_4192" s="T373">tur-an</ta>
            <ta e="T375" id="Seg_4193" s="T374">kel-l-e</ta>
            <ta e="T376" id="Seg_4194" s="T375">di͡e-n</ta>
            <ta e="T377" id="Seg_4195" s="T376">tur-an</ta>
            <ta e="T378" id="Seg_4196" s="T377">kel-en</ta>
            <ta e="T379" id="Seg_4197" s="T378">bu͡ollagɨna</ta>
            <ta e="T380" id="Seg_4198" s="T379">kaːtʼer-daːgar</ta>
            <ta e="T381" id="Seg_4199" s="T380">daː</ta>
            <ta e="T382" id="Seg_4200" s="T381">ulakan</ta>
            <ta e="T383" id="Seg_4201" s="T382">boldog-u</ta>
            <ta e="T384" id="Seg_4202" s="T383">ɨl-an</ta>
            <ta e="T385" id="Seg_4203" s="T384">baraːn</ta>
            <ta e="T386" id="Seg_4204" s="T385">dʼe</ta>
            <ta e="T387" id="Seg_4205" s="T386">bɨrak-pɨt</ta>
            <ta e="T388" id="Seg_4206" s="T387">kim</ta>
            <ta e="T389" id="Seg_4207" s="T388">dʼe</ta>
            <ta e="T390" id="Seg_4208" s="T389">ɨt-tar-aːččɨ-ta</ta>
            <ta e="T391" id="Seg_4209" s="T390">ol</ta>
            <ta e="T392" id="Seg_4210" s="T391">bɨrak-pɨt-ɨ-gar</ta>
            <ta e="T393" id="Seg_4211" s="T392">he</ta>
            <ta e="T394" id="Seg_4212" s="T393">arɨččɨ</ta>
            <ta e="T395" id="Seg_4213" s="T394">ebe</ta>
            <ta e="T396" id="Seg_4214" s="T395">ü͡öh-ü-ger</ta>
            <ta e="T397" id="Seg_4215" s="T396">tiːj-bit-i-m</ta>
            <ta e="T398" id="Seg_4216" s="T397">tiːj-er-i-n</ta>
            <ta e="T399" id="Seg_4217" s="T398">kɨtta</ta>
            <ta e="T400" id="Seg_4218" s="T399">bu͡olla</ta>
            <ta e="T401" id="Seg_4219" s="T400">ol</ta>
            <ta e="T402" id="Seg_4220" s="T401">dolgun-u-gar</ta>
            <ta e="T403" id="Seg_4221" s="T402">bɨrak-pɨt</ta>
            <ta e="T404" id="Seg_4222" s="T403">dolgun-u-gar</ta>
            <ta e="T405" id="Seg_4223" s="T404">čut</ta>
            <ta e="T406" id="Seg_4224" s="T405">tü͡ör-e</ta>
            <ta e="T407" id="Seg_4225" s="T406">bar-a</ta>
            <ta e="T408" id="Seg_4226" s="T407">hɨs-pɨt</ta>
            <ta e="T409" id="Seg_4227" s="T408">ol</ta>
            <ta e="T410" id="Seg_4228" s="T409">küreː-ti-bit</ta>
            <ta e="T411" id="Seg_4229" s="T410">di͡e-bit</ta>
            <ta e="T412" id="Seg_4230" s="T411">bu͡olla</ta>
            <ta e="T413" id="Seg_4231" s="T412">oččo</ta>
            <ta e="T414" id="Seg_4232" s="T413">hette</ta>
            <ta e="T415" id="Seg_4233" s="T414">kihi-tten</ta>
            <ta e="T416" id="Seg_4234" s="T415">hogotok</ta>
            <ta e="T417" id="Seg_4235" s="T416">kihi-leːk</ta>
            <ta e="T418" id="Seg_4236" s="T417">kel-bit</ta>
            <ta e="T419" id="Seg_4237" s="T418">okt-on-nor</ta>
            <ta e="T420" id="Seg_4238" s="T419">beje-lere</ta>
            <ta e="T421" id="Seg_4239" s="T420">tug-u</ta>
            <ta e="T422" id="Seg_4240" s="T421">da</ta>
            <ta e="T423" id="Seg_4241" s="T422">bul-bakka</ta>
            <ta e="T424" id="Seg_4242" s="T423">ergij-en</ta>
            <ta e="T425" id="Seg_4243" s="T424">kel</ta>
            <ta e="T426" id="Seg_4244" s="T425">ol</ta>
            <ta e="T427" id="Seg_4245" s="T426">kel-en</ta>
            <ta e="T428" id="Seg_4246" s="T427">ol</ta>
            <ta e="T429" id="Seg_4247" s="T428">üːhe</ta>
            <ta e="T430" id="Seg_4248" s="T429">di͡ek</ta>
            <ta e="T431" id="Seg_4249" s="T430">bar-bɨt</ta>
            <ta e="T432" id="Seg_4250" s="T431">onton</ta>
            <ta e="T433" id="Seg_4251" s="T432">Čagɨdaj-ɨ</ta>
            <ta e="T434" id="Seg_4252" s="T433">bu͡ollagɨn</ta>
            <ta e="T435" id="Seg_4253" s="T434">hajɨn</ta>
            <ta e="T436" id="Seg_4254" s="T435">biːrge</ta>
            <ta e="T437" id="Seg_4255" s="T436">olor-ol-lor</ta>
            <ta e="T438" id="Seg_4256" s="T437">ühü</ta>
            <ta e="T439" id="Seg_4257" s="T438">ahaː-n</ta>
            <ta e="T440" id="Seg_4258" s="T439">olor-on-nor</ta>
            <ta e="T441" id="Seg_4259" s="T440">ka</ta>
            <ta e="T442" id="Seg_4260" s="T441">huruk</ta>
            <ta e="T443" id="Seg_4261" s="T442">kel-bit</ta>
            <ta e="T444" id="Seg_4262" s="T443">bu</ta>
            <ta e="T445" id="Seg_4263" s="T444">hurug-u</ta>
            <ta e="T446" id="Seg_4264" s="T445">bu͡ollagɨna</ta>
            <ta e="T447" id="Seg_4265" s="T446">Nosku͡o-ga</ta>
            <ta e="T448" id="Seg_4266" s="T447">tiri͡er-i͡ek-teri-n</ta>
            <ta e="T449" id="Seg_4267" s="T448">naːda</ta>
            <ta e="T450" id="Seg_4268" s="T449">kantan</ta>
            <ta e="T451" id="Seg_4269" s="T450">ere</ta>
            <ta e="T452" id="Seg_4270" s="T451">kel-bit</ta>
            <ta e="T453" id="Seg_4271" s="T452">tɨː-nnan</ta>
            <ta e="T454" id="Seg_4272" s="T453">oččo</ta>
            <ta e="T455" id="Seg_4273" s="T454">tu͡ok</ta>
            <ta e="T456" id="Seg_4274" s="T455">lu͡okka-ta</ta>
            <ta e="T457" id="Seg_4275" s="T456">kel-i͡e=j</ta>
            <ta e="T458" id="Seg_4276" s="T457">ka</ta>
            <ta e="T459" id="Seg_4277" s="T458">Čagɨdaj</ta>
            <ta e="T460" id="Seg_4278" s="T459">bu</ta>
            <ta e="T461" id="Seg_4279" s="T460">hurug-u</ta>
            <ta e="T462" id="Seg_4280" s="T461">bu͡olla</ta>
            <ta e="T463" id="Seg_4281" s="T462">kimi͡e-ke</ta>
            <ta e="T464" id="Seg_4282" s="T463">tiri͡er</ta>
            <ta e="T465" id="Seg_4283" s="T464">Kiri͡es-ke</ta>
            <ta e="T466" id="Seg_4284" s="T465">Rɨmnaj</ta>
            <ta e="T467" id="Seg_4285" s="T466">Kiri͡eh-i-ger</ta>
            <ta e="T468" id="Seg_4286" s="T467">kör</ta>
            <ta e="T469" id="Seg_4287" s="T468">ɨraːg-ɨn</ta>
            <ta e="T470" id="Seg_4288" s="T469">Rɨmnaj</ta>
            <ta e="T471" id="Seg_4289" s="T470">ɨnaraː</ta>
            <ta e="T472" id="Seg_4290" s="T471">ött-ü-ger</ta>
            <ta e="T478" id="Seg_4291" s="T476">hu͡ok</ta>
            <ta e="T479" id="Seg_4292" s="T478">ol</ta>
            <ta e="T480" id="Seg_4293" s="T479">beje-ler-i-n</ta>
            <ta e="T482" id="Seg_4294" s="T480">gi͡et-tere</ta>
            <ta e="T485" id="Seg_4295" s="T482">heː</ta>
            <ta e="T486" id="Seg_4296" s="T485">Bi͡egičep-tere</ta>
            <ta e="T487" id="Seg_4297" s="T486">h-onon</ta>
            <ta e="T488" id="Seg_4298" s="T487">ajannaː-bɨt</ta>
            <ta e="T489" id="Seg_4299" s="T488">bu͡ol-l-a</ta>
            <ta e="T490" id="Seg_4300" s="T489">ol</ta>
            <ta e="T491" id="Seg_4301" s="T490">bar-ɨːt-ɨ-nan</ta>
            <ta e="T492" id="Seg_4302" s="T491">bar-bɨt</ta>
            <ta e="T493" id="Seg_4303" s="T492">bu</ta>
            <ta e="T494" id="Seg_4304" s="T493">di͡ek</ta>
            <ta e="T495" id="Seg_4305" s="T494">üːhe</ta>
            <ta e="T496" id="Seg_4306" s="T495">di͡ek</ta>
            <ta e="T497" id="Seg_4307" s="T496">če</ta>
            <ta e="T498" id="Seg_4308" s="T497">ol</ta>
            <ta e="T499" id="Seg_4309" s="T498">bu͡o</ta>
            <ta e="T500" id="Seg_4310" s="T499">ol</ta>
            <ta e="T501" id="Seg_4311" s="T500">hurug-u</ta>
            <ta e="T502" id="Seg_4312" s="T501">ilt-e-r-el-ler</ta>
            <ta e="T503" id="Seg_4313" s="T502">oččo</ta>
            <ta e="T504" id="Seg_4314" s="T503">huruk-taːk-tar</ta>
            <ta e="T505" id="Seg_4315" s="T504">e-bit-e</ta>
            <ta e="T506" id="Seg_4316" s="T505">bu͡olla</ta>
            <ta e="T507" id="Seg_4317" s="T506">tojon-noːk-tor</ta>
            <ta e="T508" id="Seg_4318" s="T507">bu͡o</ta>
            <ta e="T512" id="Seg_4319" s="T511">haka-lar</ta>
            <ta e="T513" id="Seg_4320" s="T512">ol</ta>
            <ta e="T514" id="Seg_4321" s="T513">ülehit-tere</ta>
            <ta e="T515" id="Seg_4322" s="T514">habi͡et-tara</ta>
            <ta e="T516" id="Seg_4323" s="T515">tu͡ok-tara</ta>
            <ta e="T517" id="Seg_4324" s="T516">huruk</ta>
            <ta e="T518" id="Seg_4325" s="T517">ɨːt-al-lar</ta>
            <ta e="T519" id="Seg_4326" s="T518">bu͡o</ta>
            <ta e="T520" id="Seg_4327" s="T519">innʼe</ta>
            <ta e="T521" id="Seg_4328" s="T520">gɨn</ta>
            <ta e="T522" id="Seg_4329" s="T521">bu</ta>
            <ta e="T523" id="Seg_4330" s="T522">ogonnʼor</ta>
            <ta e="T524" id="Seg_4331" s="T523">bu͡olla</ta>
            <ta e="T525" id="Seg_4332" s="T524">dʼe</ta>
            <ta e="T526" id="Seg_4333" s="T525">ki͡ehe</ta>
            <ta e="T527" id="Seg_4334" s="T526">kamnɨː-r</ta>
            <ta e="T528" id="Seg_4335" s="T527">harsi͡erda</ta>
            <ta e="T529" id="Seg_4336" s="T528">erde-hit</ta>
            <ta e="T530" id="Seg_4337" s="T529">dʼaktar</ta>
            <ta e="T531" id="Seg_4338" s="T530">tur-ar</ta>
            <ta e="T532" id="Seg_4339" s="T531">kem-i-ger</ta>
            <ta e="T533" id="Seg_4340" s="T532">kel-i͡e-m</ta>
            <ta e="T534" id="Seg_4341" s="T533">di͡e-bit</ta>
            <ta e="T535" id="Seg_4342" s="T534">bu</ta>
            <ta e="T536" id="Seg_4343" s="T535">ogonnʼor</ta>
            <ta e="T537" id="Seg_4344" s="T536">o</ta>
            <ta e="T538" id="Seg_4345" s="T537">kajdak</ta>
            <ta e="T539" id="Seg_4346" s="T538">gɨn-an</ta>
            <ta e="T540" id="Seg_4347" s="T539">kel-eːri</ta>
            <ta e="T541" id="Seg_4348" s="T540">gɨn-a-gɨn</ta>
            <ta e="T542" id="Seg_4349" s="T541">Rɨmnaj</ta>
            <ta e="T543" id="Seg_4350" s="T542">ɨnaːra</ta>
            <ta e="T544" id="Seg_4351" s="T543">ött-ü-tten</ta>
            <ta e="T545" id="Seg_4352" s="T544">hürdeːk</ta>
            <ta e="T546" id="Seg_4353" s="T545">ɨraːk</ta>
            <ta e="T547" id="Seg_4354" s="T546">Pobügej-ten</ta>
            <ta e="T548" id="Seg_4355" s="T547">dʼe</ta>
            <ta e="T549" id="Seg_4356" s="T548">bu</ta>
            <ta e="T550" id="Seg_4357" s="T549">ogonnʼor</ta>
            <ta e="T551" id="Seg_4358" s="T550">kaːm-an</ta>
            <ta e="T552" id="Seg_4359" s="T551">kaːl-bɨt</ta>
            <ta e="T553" id="Seg_4360" s="T552">e</ta>
            <ta e="T554" id="Seg_4361" s="T553">bar-an</ta>
            <ta e="T555" id="Seg_4362" s="T554">kaːl-bɨt</ta>
            <ta e="T556" id="Seg_4363" s="T555">tɨː-nnan</ta>
            <ta e="T557" id="Seg_4364" s="T556">harsi͡erda</ta>
            <ta e="T558" id="Seg_4365" s="T557">emeːksit-ter</ta>
            <ta e="T561" id="Seg_4366" s="T560">erde-hit</ta>
            <ta e="T562" id="Seg_4367" s="T561">dʼaktat-tar</ta>
            <ta e="T563" id="Seg_4368" s="T562">tur-al-lar</ta>
            <ta e="T564" id="Seg_4369" s="T563">naprimer</ta>
            <ta e="T565" id="Seg_4370" s="T564">oččo</ta>
            <ta e="T566" id="Seg_4371" s="T565">hette</ta>
            <ta e="T567" id="Seg_4372" s="T566">čaːs</ta>
            <ta e="T568" id="Seg_4373" s="T567">itte</ta>
            <ta e="T569" id="Seg_4374" s="T568">e-bit</ta>
            <ta e="T570" id="Seg_4375" s="T569">eni</ta>
            <ta e="T571" id="Seg_4376" s="T570">tu͡ok</ta>
            <ta e="T572" id="Seg_4377" s="T571">čaːh-a</ta>
            <ta e="T573" id="Seg_4378" s="T572">kel-i͡e=j</ta>
            <ta e="T574" id="Seg_4379" s="T573">kün-neri-nen</ta>
            <ta e="T575" id="Seg_4380" s="T574">kör-öl-lör</ta>
            <ta e="T576" id="Seg_4381" s="T575">bu͡o</ta>
            <ta e="T577" id="Seg_4382" s="T576">araː</ta>
            <ta e="T578" id="Seg_4383" s="T577">hette</ta>
            <ta e="T579" id="Seg_4384" s="T578">čaːs-ka</ta>
            <ta e="T580" id="Seg_4385" s="T579">Čagɨdaj</ta>
            <ta e="T581" id="Seg_4386" s="T580">kel-bit</ta>
            <ta e="T582" id="Seg_4387" s="T581">tɨː-ta</ta>
            <ta e="T583" id="Seg_4388" s="T582">ums-a-n-a</ta>
            <ta e="T584" id="Seg_4389" s="T583">hɨt-ar</ta>
            <ta e="T585" id="Seg_4390" s="T584">diːn</ta>
            <ta e="T586" id="Seg_4391" s="T585">kör</ta>
            <ta e="T587" id="Seg_4392" s="T586">onnuk</ta>
            <ta e="T588" id="Seg_4393" s="T587">korsun</ta>
            <ta e="T589" id="Seg_4394" s="T588">kihi-ler</ta>
            <ta e="T590" id="Seg_4395" s="T589">baːl-lar</ta>
            <ta e="T591" id="Seg_4396" s="T590">onton</ta>
            <ta e="T592" id="Seg_4397" s="T591">dʼaktar</ta>
            <ta e="T593" id="Seg_4398" s="T592">törüː-r</ta>
            <ta e="T594" id="Seg_4399" s="T593">hol</ta>
            <ta e="T595" id="Seg_4400" s="T594">huːr-tarɨ-gar</ta>
            <ta e="T596" id="Seg_4401" s="T595">bu</ta>
            <ta e="T597" id="Seg_4402" s="T596">dʼaktar</ta>
            <ta e="T598" id="Seg_4403" s="T597">öl-öːrü</ta>
            <ta e="T599" id="Seg_4404" s="T598">gɨm-mɨt</ta>
            <ta e="T600" id="Seg_4405" s="T599">kajdak</ta>
            <ta e="T601" id="Seg_4406" s="T600">da</ta>
            <ta e="T602" id="Seg_4407" s="T601">gɨn-ɨ͡ak-tarɨ-n</ta>
            <ta e="T603" id="Seg_4408" s="T602">bert</ta>
            <ta e="T604" id="Seg_4409" s="T603">du͡oktur-a</ta>
            <ta e="T605" id="Seg_4410" s="T604">hu͡ok</ta>
            <ta e="T606" id="Seg_4411" s="T605">üje-ge</ta>
            <ta e="T607" id="Seg_4412" s="T606">ke</ta>
            <ta e="T608" id="Seg_4413" s="T607">baːbɨska-lara</ta>
            <ta e="T609" id="Seg_4414" s="T608">muŋ-naːk</ta>
            <ta e="T610" id="Seg_4415" s="T609">hɨlaj-an</ta>
            <ta e="T611" id="Seg_4416" s="T610">büp-püt</ta>
            <ta e="T612" id="Seg_4417" s="T611">kaja</ta>
            <ta e="T613" id="Seg_4418" s="T612">Čagɨdaj</ta>
            <ta e="T614" id="Seg_4419" s="T613">kel-lin</ta>
            <ta e="T615" id="Seg_4420" s="T614">dʼaktar</ta>
            <ta e="T616" id="Seg_4421" s="T615">öl-öːrü</ta>
            <ta e="T617" id="Seg_4422" s="T616">gɨn-n-a</ta>
            <ta e="T618" id="Seg_4423" s="T617">kajdak</ta>
            <ta e="T619" id="Seg_4424" s="T618">eme</ta>
            <ta e="T620" id="Seg_4425" s="T619">gɨn-nɨn</ta>
            <ta e="T621" id="Seg_4426" s="T620">oŋu͡ok</ta>
            <ta e="T622" id="Seg_4427" s="T621">araːr-dɨn</ta>
            <ta e="T623" id="Seg_4428" s="T622">munna</ta>
            <ta e="T624" id="Seg_4429" s="T623">h-innʼe</ta>
            <ta e="T625" id="Seg_4430" s="T624">bu</ta>
            <ta e="T626" id="Seg_4431" s="T625">Čagɨdaj</ta>
            <ta e="T627" id="Seg_4432" s="T626">kel-er</ta>
            <ta e="T628" id="Seg_4433" s="T627">kajdak</ta>
            <ta e="T631" id="Seg_4434" s="T630">bu</ta>
            <ta e="T632" id="Seg_4435" s="T631">dʼaktar-ɨ</ta>
            <ta e="T633" id="Seg_4436" s="T632">iliː-leː-n</ta>
            <ta e="T634" id="Seg_4437" s="T633">ɨl-ar</ta>
            <ta e="T635" id="Seg_4438" s="T634">ogo-tu-n</ta>
            <ta e="T636" id="Seg_4439" s="T635">h-onon</ta>
            <ta e="T637" id="Seg_4440" s="T636">tɨːnnaːk</ta>
            <ta e="T638" id="Seg_4441" s="T637">keːh-er</ta>
            <ta e="T639" id="Seg_4442" s="T638">kör</ta>
            <ta e="T640" id="Seg_4443" s="T639">onnuk-tar</ta>
            <ta e="T641" id="Seg_4444" s="T640">baːr</ta>
            <ta e="T642" id="Seg_4445" s="T641">e-ti-ler</ta>
            <ta e="T643" id="Seg_4446" s="T642">kɨrdʼagas-tar</ta>
            <ta e="T644" id="Seg_4447" s="T643">i</ta>
            <ta e="T645" id="Seg_4448" s="T644">Čakɨːla</ta>
            <ta e="T646" id="Seg_4449" s="T645">ogonnʼor</ta>
            <ta e="T647" id="Seg_4450" s="T646">ehe-te</ta>
            <ta e="T648" id="Seg_4451" s="T647">on-tu-ŋ</ta>
            <ta e="T652" id="Seg_4452" s="T651">heː</ta>
            <ta e="T653" id="Seg_4453" s="T652">oččogo</ta>
            <ta e="T654" id="Seg_4454" s="T653">ol</ta>
            <ta e="T655" id="Seg_4455" s="T654">Čagɨdaj-ɨ-ŋ</ta>
            <ta e="T656" id="Seg_4456" s="T655">dʼaktar-a</ta>
            <ta e="T657" id="Seg_4457" s="T656">bu͡ollagɨna</ta>
            <ta e="T658" id="Seg_4458" s="T657">emeːksin</ta>
            <ta e="T659" id="Seg_4459" s="T658">Anna</ta>
            <ta e="T660" id="Seg_4460" s="T659">inʼe-te</ta>
            <ta e="T661" id="Seg_4461" s="T660">emeːksin</ta>
            <ta e="T662" id="Seg_4462" s="T661">baː</ta>
            <ta e="T663" id="Seg_4463" s="T662">Čeːke</ta>
            <ta e="T664" id="Seg_4464" s="T663">emeːksin</ta>
            <ta e="T665" id="Seg_4465" s="T664">on-tu-ŋ</ta>
            <ta e="T666" id="Seg_4466" s="T665">teːte-te</ta>
            <ta e="T667" id="Seg_4467" s="T666">bihigi</ta>
            <ta e="T668" id="Seg_4468" s="T667">eheː-biti-n</ta>
            <ta e="T669" id="Seg_4469" s="T668">kɨtta</ta>
            <ta e="T670" id="Seg_4470" s="T669">biːrge</ta>
            <ta e="T671" id="Seg_4471" s="T670">töröː-büt-ter</ta>
            <ta e="T672" id="Seg_4472" s="T671">Kudrʼakop</ta>
            <ta e="T673" id="Seg_4473" s="T672">ogonnʼor</ta>
         </annotation>
         <annotation name="mp" tierref="mp-KiPP">
            <ta e="T1" id="Seg_4474" s="T0">Bi͡egičep-nI</ta>
            <ta e="T2" id="Seg_4475" s="T1">ilin-Ar-LAr</ta>
            <ta e="T5" id="Seg_4476" s="T4">hir-I-nAn</ta>
            <ta e="T6" id="Seg_4477" s="T5">hɨrɨt-Ar</ta>
            <ta e="T7" id="Seg_4478" s="T6">e-TI-tA</ta>
            <ta e="T8" id="Seg_4479" s="T7">urut</ta>
            <ta e="T9" id="Seg_4480" s="T8">ekspʼedʼicɨja-LAr</ta>
            <ta e="T10" id="Seg_4481" s="T9">ol-nI</ta>
            <ta e="T11" id="Seg_4482" s="T10">ehe-m</ta>
            <ta e="T12" id="Seg_4483" s="T11">kepseː-AːččI</ta>
            <ta e="T13" id="Seg_4484" s="T12">mini͡ene</ta>
            <ta e="T14" id="Seg_4485" s="T13">inʼe-m</ta>
            <ta e="T15" id="Seg_4486" s="T14">ehe</ta>
            <ta e="T16" id="Seg_4487" s="T15">iti</ta>
            <ta e="T17" id="Seg_4488" s="T16">inʼe-m</ta>
            <ta e="T18" id="Seg_4489" s="T17">uruː-LAr-tA</ta>
            <ta e="T19" id="Seg_4490" s="T18">Markʼel</ta>
            <ta e="T20" id="Seg_4491" s="T19">ogonnʼor</ta>
            <ta e="T21" id="Seg_4492" s="T20">Ölöːnö</ta>
            <ta e="T22" id="Seg_4493" s="T21">aga-tA</ta>
            <ta e="T23" id="Seg_4494" s="T22">ogonnʼor</ta>
            <ta e="T24" id="Seg_4495" s="T23">ol-LAr</ta>
            <ta e="T25" id="Seg_4496" s="T24">kepset-Ar-LArI-n</ta>
            <ta e="T26" id="Seg_4497" s="T25">ihit-AːččI-BIn</ta>
            <ta e="T27" id="Seg_4498" s="T26">bu</ta>
            <ta e="T28" id="Seg_4499" s="T27">Bi͡egičep</ta>
            <ta e="T29" id="Seg_4500" s="T28">bu͡ollagɨna</ta>
            <ta e="T30" id="Seg_4501" s="T29">taba-nAn</ta>
            <ta e="T31" id="Seg_4502" s="T30">ajannaː-Ar</ta>
            <ta e="T32" id="Seg_4503" s="T31">Bi͡egičep</ta>
            <ta e="T33" id="Seg_4504" s="T32">arɨː-tI-GAr</ta>
            <ta e="T34" id="Seg_4505" s="T33">hette</ta>
            <ta e="T35" id="Seg_4506" s="T34">kihi-LAːK</ta>
            <ta e="T36" id="Seg_4507" s="T35">bu</ta>
            <ta e="T37" id="Seg_4508" s="T36">kihi</ta>
            <ta e="T38" id="Seg_4509" s="T37">ajannaː-An</ta>
            <ta e="T39" id="Seg_4510" s="T38">is-An</ta>
            <ta e="T40" id="Seg_4511" s="T39">taba-nAn</ta>
            <ta e="T41" id="Seg_4512" s="T40">ilin-Ar-LAr</ta>
            <ta e="T42" id="Seg_4513" s="T41">bu͡o</ta>
            <ta e="T43" id="Seg_4514" s="T42">atɨn-ttAn-atɨn</ta>
            <ta e="T44" id="Seg_4515" s="T43">taba-nAn</ta>
            <ta e="T45" id="Seg_4516" s="T44">ilin-Ar-LAr</ta>
            <ta e="T46" id="Seg_4517" s="T45">haŋa-ttAn-haŋa</ta>
            <ta e="T47" id="Seg_4518" s="T46">taba-nAn</ta>
            <ta e="T48" id="Seg_4519" s="T47">ilin-Ar-LAr</ta>
            <ta e="T49" id="Seg_4520" s="T48">onno</ta>
            <ta e="T50" id="Seg_4521" s="T49">Čagɨdaj</ta>
            <ta e="T51" id="Seg_4522" s="T50">ogonnʼor-nI</ta>
            <ta e="T52" id="Seg_4523" s="T51">ilt-A-r-Ar-LAr</ta>
            <ta e="T53" id="Seg_4524" s="T52">bu͡olla</ta>
            <ta e="T54" id="Seg_4525" s="T53">min</ta>
            <ta e="T55" id="Seg_4526" s="T54">ogonnʼor-I-m</ta>
            <ta e="T56" id="Seg_4527" s="T55">ehe-tI-n-LAːK</ta>
            <ta e="T57" id="Seg_4528" s="T56">ehe-LArA</ta>
            <ta e="T58" id="Seg_4529" s="T57">ol</ta>
            <ta e="T59" id="Seg_4530" s="T58">ogonnʼor</ta>
            <ta e="T60" id="Seg_4531" s="T59">tu͡ok</ta>
            <ta e="T61" id="Seg_4532" s="T60">da</ta>
            <ta e="T62" id="Seg_4533" s="T61">hit-BAtAK</ta>
            <ta e="T63" id="Seg_4534" s="T62">kihi-tA</ta>
            <ta e="T64" id="Seg_4535" s="T63">e-TI-tA</ta>
            <ta e="T65" id="Seg_4536" s="T64">hürdeːk</ta>
            <ta e="T66" id="Seg_4537" s="T65">kamnastaːk</ta>
            <ta e="T67" id="Seg_4538" s="T66">kihi</ta>
            <ta e="T68" id="Seg_4539" s="T67">ol</ta>
            <ta e="T69" id="Seg_4540" s="T68">ogonnʼor</ta>
            <ta e="T70" id="Seg_4541" s="T69">hataː-BAT</ta>
            <ta e="T71" id="Seg_4542" s="T70">taba-LAr-nI</ta>
            <ta e="T72" id="Seg_4543" s="T71">kölün-TAr-An</ta>
            <ta e="T73" id="Seg_4544" s="T72">baran</ta>
            <ta e="T74" id="Seg_4545" s="T73">Bi͡egičep</ta>
            <ta e="T75" id="Seg_4546" s="T74">arɨː-tI-GAr</ta>
            <ta e="T76" id="Seg_4547" s="T75">ilt-A-r-Ar-LAr</ta>
            <ta e="T77" id="Seg_4548" s="T76">bu͡o</ta>
            <ta e="T78" id="Seg_4549" s="T77">onno</ta>
            <ta e="T79" id="Seg_4550" s="T78">taba-LAr</ta>
            <ta e="T80" id="Seg_4551" s="T79">möŋ-Ar-LAr</ta>
            <ta e="T81" id="Seg_4552" s="T80">ühü</ta>
            <ta e="T82" id="Seg_4553" s="T81">eː</ta>
            <ta e="T83" id="Seg_4554" s="T82">diː</ta>
            <ta e="T84" id="Seg_4555" s="T83">taba-LAr</ta>
            <ta e="T85" id="Seg_4556" s="T84">möŋ-Ar-LAr</ta>
            <ta e="T86" id="Seg_4557" s="T85">bu</ta>
            <ta e="T87" id="Seg_4558" s="T86">kihi</ta>
            <ta e="T88" id="Seg_4559" s="T87">bergehe-tI-n</ta>
            <ta e="T89" id="Seg_4560" s="T88">ol</ta>
            <ta e="T90" id="Seg_4561" s="T89">Bi͡egičep</ta>
            <ta e="T91" id="Seg_4562" s="T90">gi͡en-tI-n</ta>
            <ta e="T92" id="Seg_4563" s="T91">bergehe-tI-n</ta>
            <ta e="T93" id="Seg_4564" s="T92">taba</ta>
            <ta e="T94" id="Seg_4565" s="T93">iːl-An</ta>
            <ta e="T95" id="Seg_4566" s="T94">bɨrak-Ar</ta>
            <ta e="T96" id="Seg_4567" s="T95">ühü</ta>
            <ta e="T97" id="Seg_4568" s="T96">bu</ta>
            <ta e="T98" id="Seg_4569" s="T97">hɨrga-nI</ta>
            <ta e="T99" id="Seg_4570" s="T98">kurdarɨ</ta>
            <ta e="T100" id="Seg_4571" s="T99">bergehe</ta>
            <ta e="T101" id="Seg_4572" s="T100">tüs-An</ta>
            <ta e="T102" id="Seg_4573" s="T101">er-TAK-InA</ta>
            <ta e="T103" id="Seg_4574" s="T102">ol-nI</ta>
            <ta e="T104" id="Seg_4575" s="T103">ogonnʼor</ta>
            <ta e="T105" id="Seg_4576" s="T104">hɨrga-nI</ta>
            <ta e="T106" id="Seg_4577" s="T105">kurdarɨ</ta>
            <ta e="T107" id="Seg_4578" s="T106">oj-An</ta>
            <ta e="T108" id="Seg_4579" s="T107">kap-An</ta>
            <ta e="T109" id="Seg_4580" s="T108">ɨl-BIT</ta>
            <ta e="T110" id="Seg_4581" s="T109">bu͡o</ta>
            <ta e="T111" id="Seg_4582" s="T110">hir-GA</ta>
            <ta e="T112" id="Seg_4583" s="T111">tüs-IAK-tI-n</ta>
            <ta e="T113" id="Seg_4584" s="T112">betereː</ta>
            <ta e="T114" id="Seg_4585" s="T113">örüt-tI-GAr</ta>
            <ta e="T115" id="Seg_4586" s="T114">ol-nI</ta>
            <ta e="T116" id="Seg_4587" s="T115">ol</ta>
            <ta e="T119" id="Seg_4588" s="T118">Bi͡egičep-tA</ta>
            <ta e="T120" id="Seg_4589" s="T119">di͡e-Ar</ta>
            <ta e="T121" id="Seg_4590" s="T120">ühü</ta>
            <ta e="T122" id="Seg_4591" s="T121">oː</ta>
            <ta e="T123" id="Seg_4592" s="T122">haka</ta>
            <ta e="T124" id="Seg_4593" s="T123">hir-tI-GAr</ta>
            <ta e="T125" id="Seg_4594" s="T124">da</ta>
            <ta e="T126" id="Seg_4595" s="T125">kamnastaːk</ta>
            <ta e="T127" id="Seg_4596" s="T126">kihi</ta>
            <ta e="T128" id="Seg_4597" s="T127">baːr</ta>
            <ta e="T129" id="Seg_4598" s="T128">bu͡ol-Ar</ta>
            <ta e="T131" id="Seg_4599" s="T130">dʼe</ta>
            <ta e="T132" id="Seg_4600" s="T131">bu</ta>
            <ta e="T133" id="Seg_4601" s="T132">ogonnʼor</ta>
            <ta e="T134" id="Seg_4602" s="T133">Bi͡egičep</ta>
            <ta e="T135" id="Seg_4603" s="T134">bu͡ollagɨna</ta>
            <ta e="T136" id="Seg_4604" s="T135">ol</ta>
            <ta e="T137" id="Seg_4605" s="T136">ol</ta>
            <ta e="T138" id="Seg_4606" s="T137">arɨː-GA</ta>
            <ta e="T139" id="Seg_4607" s="T138">tij-BIT-I-m</ta>
            <ta e="T140" id="Seg_4608" s="T139">di͡e-An</ta>
            <ta e="T141" id="Seg_4609" s="T140">kepseː-Ar</ta>
            <ta e="T142" id="Seg_4610" s="T141">ühü</ta>
            <ta e="T143" id="Seg_4611" s="T142">bu</ta>
            <ta e="T144" id="Seg_4612" s="T143">tu͡ok</ta>
            <ta e="T145" id="Seg_4613" s="T144">oduː-nI</ta>
            <ta e="T146" id="Seg_4614" s="T145">kör-TI-ŋ</ta>
            <ta e="T147" id="Seg_4615" s="T146">onno</ta>
            <ta e="T148" id="Seg_4616" s="T147">di͡e-An</ta>
            <ta e="T149" id="Seg_4617" s="T148">ɨjɨt-Ar-LAr</ta>
            <ta e="T150" id="Seg_4618" s="T149">haka-LAr</ta>
            <ta e="T151" id="Seg_4619" s="T150">eː</ta>
            <ta e="T152" id="Seg_4620" s="T151">diː</ta>
            <ta e="T153" id="Seg_4621" s="T152">ol-GA</ta>
            <ta e="T154" id="Seg_4622" s="T153">min</ta>
            <ta e="T155" id="Seg_4623" s="T154">oduː-nI</ta>
            <ta e="T156" id="Seg_4624" s="T155">tu͡ok-nI</ta>
            <ta e="T157" id="Seg_4625" s="T156">da</ta>
            <ta e="T158" id="Seg_4626" s="T157">olus</ta>
            <ta e="T159" id="Seg_4627" s="T158">bul-BA-TI-m</ta>
            <ta e="T160" id="Seg_4628" s="T159">marsa-LArA</ta>
            <ta e="T161" id="Seg_4629" s="T160">bu͡ollagɨna</ta>
            <ta e="T162" id="Seg_4630" s="T161">tiːs-LArI-ttAn</ta>
            <ta e="T163" id="Seg_4631" s="T162">baːj-An</ta>
            <ta e="T164" id="Seg_4632" s="T163">baran</ta>
            <ta e="T165" id="Seg_4633" s="T164">utuj-Ar-LAr</ta>
            <ta e="T166" id="Seg_4634" s="T165">utuj-A</ta>
            <ta e="T167" id="Seg_4635" s="T166">hɨt-Ar-LArI-n</ta>
            <ta e="T168" id="Seg_4636" s="T167">baːj-An</ta>
            <ta e="T169" id="Seg_4637" s="T168">brevnoː-ttAn</ta>
            <ta e="T170" id="Seg_4638" s="T169">ol-nI</ta>
            <ta e="T171" id="Seg_4639" s="T170">hep</ta>
            <ta e="T172" id="Seg_4640" s="T171">da</ta>
            <ta e="T173" id="Seg_4641" s="T172">di͡e-BAT-LAr</ta>
            <ta e="T174" id="Seg_4642" s="T173">hagastaː-An</ta>
            <ta e="T175" id="Seg_4643" s="T174">kiːr-TAr-Ar-LAr</ta>
            <ta e="T176" id="Seg_4644" s="T175">uː-GA</ta>
            <ta e="T177" id="Seg_4645" s="T176">kiːr-TAr-An</ta>
            <ta e="T178" id="Seg_4646" s="T177">keːs-Ar-LAr</ta>
            <ta e="T179" id="Seg_4647" s="T178">onton</ta>
            <ta e="T180" id="Seg_4648" s="T179">agaj</ta>
            <ta e="T181" id="Seg_4649" s="T180">arɨː-GA</ta>
            <ta e="T182" id="Seg_4650" s="T181">taːs</ta>
            <ta e="T183" id="Seg_4651" s="T182">kaja-GA</ta>
            <ta e="T184" id="Seg_4652" s="T183">bul-TI-m</ta>
            <ta e="T185" id="Seg_4653" s="T184">di͡e-Ar</ta>
            <ta e="T186" id="Seg_4654" s="T185">ühü</ta>
            <ta e="T187" id="Seg_4655" s="T186">biːr</ta>
            <ta e="T188" id="Seg_4656" s="T187">oduː-nI</ta>
            <ta e="T189" id="Seg_4657" s="T188">dʼe</ta>
            <ta e="T190" id="Seg_4658" s="T189">tu͡ok-nI</ta>
            <ta e="T191" id="Seg_4659" s="T190">bul-TI-ŋ</ta>
            <ta e="T192" id="Seg_4660" s="T191">ol-nI</ta>
            <ta e="T193" id="Seg_4661" s="T192">di͡e-Ar</ta>
            <ta e="T194" id="Seg_4662" s="T193">bu͡o</ta>
            <ta e="T210" id="Seg_4663" s="T209">ol-GA</ta>
            <ta e="T211" id="Seg_4664" s="T210">kiːr-An</ta>
            <ta e="T212" id="Seg_4665" s="T211">bar-TI-m</ta>
            <ta e="T213" id="Seg_4666" s="T212">di͡e-Ar</ta>
            <ta e="T214" id="Seg_4667" s="T213">üs</ta>
            <ta e="T215" id="Seg_4668" s="T214">kihi-LAːK</ta>
            <ta e="T216" id="Seg_4669" s="T215">bu</ta>
            <ta e="T217" id="Seg_4670" s="T216">kihi-LAr</ta>
            <ta e="T218" id="Seg_4671" s="T217">as-tA</ta>
            <ta e="T219" id="Seg_4672" s="T218">hu͡ok</ta>
            <ta e="T220" id="Seg_4673" s="T219">bu͡ol-BIT-BIt</ta>
            <ta e="T221" id="Seg_4674" s="T220">oččogo</ta>
            <ta e="T222" id="Seg_4675" s="T221">bu͡ollagɨna</ta>
            <ta e="T223" id="Seg_4676" s="T222">ebe-ttAn</ta>
            <ta e="T224" id="Seg_4677" s="T223">o</ta>
            <ta e="T225" id="Seg_4678" s="T224">kɨra</ta>
            <ta e="T226" id="Seg_4679" s="T225">balɨk-kAːN-nI</ta>
            <ta e="T227" id="Seg_4680" s="T226">kap-I-n-An-LAr</ta>
            <ta e="T228" id="Seg_4681" s="T227">ol</ta>
            <ta e="T229" id="Seg_4682" s="T228">aːn-GA</ta>
            <ta e="T230" id="Seg_4683" s="T229">kiːr-An-LAr</ta>
            <ta e="T231" id="Seg_4684" s="T230">ahaː-AːrI-LAr</ta>
            <ta e="T232" id="Seg_4685" s="T231">kiːr-BIT-LAr</ta>
            <ta e="T233" id="Seg_4686" s="T232">bu</ta>
            <ta e="T234" id="Seg_4687" s="T233">kihi</ta>
            <ta e="T235" id="Seg_4688" s="T234">agaj</ta>
            <ta e="T236" id="Seg_4689" s="T235">kiːr-BIT-tA</ta>
            <ta e="T237" id="Seg_4690" s="T236">bu</ta>
            <ta e="T238" id="Seg_4691" s="T237">kihi</ta>
            <ta e="T239" id="Seg_4692" s="T238">olor-Ar</ta>
            <ta e="T240" id="Seg_4693" s="T239">olor-TAK-InA</ta>
            <ta e="T241" id="Seg_4694" s="T240">tu͡ok</ta>
            <ta e="T242" id="Seg_4695" s="T241">ere</ta>
            <ta e="T243" id="Seg_4696" s="T242">kihi-tA</ta>
            <ta e="T244" id="Seg_4697" s="T243">kel-BIT</ta>
            <ta e="T245" id="Seg_4698" s="T244">eː</ta>
            <ta e="T246" id="Seg_4699" s="T245">diː</ta>
            <ta e="T247" id="Seg_4700" s="T246">kihi</ta>
            <ta e="T248" id="Seg_4701" s="T247">du͡o</ta>
            <ta e="T249" id="Seg_4702" s="T248">tu͡ok</ta>
            <ta e="T250" id="Seg_4703" s="T249">du͡o</ta>
            <ta e="T251" id="Seg_4704" s="T250">hannɨ-ttAn</ta>
            <ta e="T252" id="Seg_4705" s="T251">menʼiː-tA</ta>
            <ta e="T253" id="Seg_4706" s="T252">hu͡ok</ta>
            <ta e="T254" id="Seg_4707" s="T253">kihi</ta>
            <ta e="T255" id="Seg_4708" s="T254">kiːr-BIT</ta>
            <ta e="T256" id="Seg_4709" s="T255">kihi</ta>
            <ta e="T257" id="Seg_4710" s="T256">du͡o</ta>
            <ta e="T258" id="Seg_4711" s="T257">tu͡ok</ta>
            <ta e="T259" id="Seg_4712" s="T258">du͡o</ta>
            <ta e="T260" id="Seg_4713" s="T259">tu͡ok-nI</ta>
            <ta e="T261" id="Seg_4714" s="T260">da</ta>
            <ta e="T262" id="Seg_4715" s="T261">tu͡olkulaː-BAT-BIn</ta>
            <ta e="T263" id="Seg_4716" s="T262">di͡e-BIT</ta>
            <ta e="T264" id="Seg_4717" s="T263">ol</ta>
            <ta e="T265" id="Seg_4718" s="T264">kiːr-An</ta>
            <ta e="T266" id="Seg_4719" s="T265">aːn-GA</ta>
            <ta e="T267" id="Seg_4720" s="T266">olor-BIT</ta>
            <ta e="T268" id="Seg_4721" s="T267">eː</ta>
            <ta e="T269" id="Seg_4722" s="T268">diː</ta>
            <ta e="T270" id="Seg_4723" s="T269">ol</ta>
            <ta e="T271" id="Seg_4724" s="T270">olor-An</ta>
            <ta e="T272" id="Seg_4725" s="T271">bu͡ollagɨna</ta>
            <ta e="T273" id="Seg_4726" s="T272">kajaː</ta>
            <ta e="T274" id="Seg_4727" s="T273">kül-An</ta>
            <ta e="T275" id="Seg_4728" s="T274">ɨrdʼaŋnaː-BIT</ta>
            <ta e="T276" id="Seg_4729" s="T275">kanna</ta>
            <ta e="T277" id="Seg_4730" s="T276">ere</ta>
            <ta e="T278" id="Seg_4731" s="T277">karak-LAːK</ta>
            <ta e="T279" id="Seg_4732" s="T278">töbö-tI-GAr</ta>
            <ta e="T280" id="Seg_4733" s="T279">čogotok</ta>
            <ta e="T281" id="Seg_4734" s="T280">karak-LAːK</ta>
            <ta e="T282" id="Seg_4735" s="T281">kaja</ta>
            <ta e="T285" id="Seg_4736" s="T284">dogor-LAr</ta>
            <ta e="T286" id="Seg_4737" s="T285">bige-LIk</ta>
            <ta e="T287" id="Seg_4738" s="T286">tut-n-I-ŋ</ta>
            <ta e="T288" id="Seg_4739" s="T287">di͡e-Ar</ta>
            <ta e="T289" id="Seg_4740" s="T288">bu͡o</ta>
            <ta e="T290" id="Seg_4741" s="T289">dogor-tI-n</ta>
            <ta e="T291" id="Seg_4742" s="T290">u͡ol-LAr-tI-n</ta>
            <ta e="T292" id="Seg_4743" s="T291">Bi͡egičep</ta>
            <ta e="T293" id="Seg_4744" s="T292">ol-tI-tA</ta>
            <ta e="T294" id="Seg_4745" s="T293">huːp</ta>
            <ta e="T295" id="Seg_4746" s="T294">di͡e-An</ta>
            <ta e="T296" id="Seg_4747" s="T295">obor-BIT</ta>
            <ta e="T297" id="Seg_4748" s="T296">ikki</ta>
            <ta e="T298" id="Seg_4749" s="T297">kihi-tI-n</ta>
            <ta e="T299" id="Seg_4750" s="T298">obor-An</ta>
            <ta e="T300" id="Seg_4751" s="T299">ɨl-BIT</ta>
            <ta e="T301" id="Seg_4752" s="T300">oččogo</ta>
            <ta e="T302" id="Seg_4753" s="T301">bu</ta>
            <ta e="T303" id="Seg_4754" s="T302">kihi</ta>
            <ta e="T304" id="Seg_4755" s="T303">čogotok</ta>
            <ta e="T305" id="Seg_4756" s="T304">kihi-LAːK</ta>
            <ta e="T306" id="Seg_4757" s="T305">kaːl-BIT</ta>
            <ta e="T307" id="Seg_4758" s="T306">onno</ta>
            <ta e="T308" id="Seg_4759" s="T307">bu</ta>
            <ta e="T309" id="Seg_4760" s="T308">kihi</ta>
            <ta e="T310" id="Seg_4761" s="T309">bu͡olla</ta>
            <ta e="T311" id="Seg_4762" s="T310">pʼistalʼet-LAːK</ta>
            <ta e="T312" id="Seg_4763" s="T311">e-BIT</ta>
            <ta e="T313" id="Seg_4764" s="T312">ol</ta>
            <ta e="T314" id="Seg_4765" s="T313">onno</ta>
            <ta e="T315" id="Seg_4766" s="T314">di͡e-A-BIn</ta>
            <ta e="T316" id="Seg_4767" s="T315">haŋar-Ar</ta>
            <ta e="T317" id="Seg_4768" s="T316">tɨl-I-nAn</ta>
            <ta e="T318" id="Seg_4769" s="T317">haŋar-A-BIn</ta>
            <ta e="T319" id="Seg_4770" s="T318">di͡e-BIT</ta>
            <ta e="T320" id="Seg_4771" s="T319">pʼistalʼet-I-m</ta>
            <ta e="T321" id="Seg_4772" s="T320">anʼak-tI-GAr</ta>
            <ta e="T322" id="Seg_4773" s="T321">ka</ta>
            <ta e="T323" id="Seg_4774" s="T322">bu</ta>
            <ta e="T324" id="Seg_4775" s="T323">dojdu-GA</ta>
            <ta e="T325" id="Seg_4776" s="T324">kaːl-A-BIn</ta>
            <ta e="T326" id="Seg_4777" s="T325">du͡o</ta>
            <ta e="T327" id="Seg_4778" s="T326">min</ta>
            <ta e="T328" id="Seg_4779" s="T327">min-n</ta>
            <ta e="T329" id="Seg_4780" s="T328">bɨːhaː</ta>
            <ta e="T330" id="Seg_4781" s="T329">bu-nI</ta>
            <ta e="T331" id="Seg_4782" s="T330">kanna</ta>
            <ta e="T332" id="Seg_4783" s="T331">eme</ta>
            <ta e="T333" id="Seg_4784" s="T332">uŋ-A-r-Ar</ta>
            <ta e="T334" id="Seg_4785" s="T333">hir-GA-r</ta>
            <ta e="T335" id="Seg_4786" s="T334">tiri͡er</ta>
            <ta e="T336" id="Seg_4787" s="T335">di͡e-Ar</ta>
            <ta e="T337" id="Seg_4788" s="T336">hüːs-tI-GAr</ta>
            <ta e="T338" id="Seg_4789" s="T337">ɨt-BIT</ta>
            <ta e="T339" id="Seg_4790" s="T338">bu͡olla</ta>
            <ta e="T340" id="Seg_4791" s="T339">bu</ta>
            <ta e="T341" id="Seg_4792" s="T340">pʼistalʼet-tI-nAn</ta>
            <ta e="T342" id="Seg_4793" s="T341">ol-tI-tA</ta>
            <ta e="T343" id="Seg_4794" s="T342">dʼe</ta>
            <ta e="T344" id="Seg_4795" s="T343">uŋ-An</ta>
            <ta e="T345" id="Seg_4796" s="T344">tüs-BIT</ta>
            <ta e="T346" id="Seg_4797" s="T345">ol</ta>
            <ta e="T347" id="Seg_4798" s="T346">uŋ-An</ta>
            <ta e="T348" id="Seg_4799" s="T347">tüs-BIT-tI-GAr</ta>
            <ta e="T349" id="Seg_4800" s="T348">ürüt-I-nAn</ta>
            <ta e="T350" id="Seg_4801" s="T349">hüːr-An</ta>
            <ta e="T351" id="Seg_4802" s="T350">tagɨs-I-BIT-LAr</ta>
            <ta e="T352" id="Seg_4803" s="T351">bu͡ol-TAR</ta>
            <ta e="T353" id="Seg_4804" s="T352">Bi͡egičep-I-ŋ</ta>
            <ta e="T354" id="Seg_4805" s="T353">kepseː-Ar</ta>
            <ta e="T357" id="Seg_4806" s="T356">ol</ta>
            <ta e="T358" id="Seg_4807" s="T357">tij-An-BIt</ta>
            <ta e="T359" id="Seg_4808" s="T358">kim-GA</ta>
            <ta e="T360" id="Seg_4809" s="T359">bar-TI-BIt</ta>
            <ta e="T361" id="Seg_4810" s="T360">di͡e-Ar</ta>
            <ta e="T362" id="Seg_4811" s="T361">bu͡o</ta>
            <ta e="T363" id="Seg_4812" s="T362">kaːtʼer-BA-r</ta>
            <ta e="T364" id="Seg_4813" s="T363">bar-TI-m</ta>
            <ta e="T365" id="Seg_4814" s="T364">di͡e-Ar</ta>
            <ta e="T366" id="Seg_4815" s="T365">kaːtʼer-BA-r</ta>
            <ta e="T367" id="Seg_4816" s="T366">olor-An-BIn</ta>
            <ta e="T368" id="Seg_4817" s="T367">ebe</ta>
            <ta e="T369" id="Seg_4818" s="T368">ü͡ös-tI-GAr</ta>
            <ta e="T370" id="Seg_4819" s="T369">kiːr-TI-m</ta>
            <ta e="T371" id="Seg_4820" s="T370">kör-BIT-I-m</ta>
            <ta e="T372" id="Seg_4821" s="T371">kelin-BI-ttAn</ta>
            <ta e="T373" id="Seg_4822" s="T372">maːjdiːn-I-m</ta>
            <ta e="T374" id="Seg_4823" s="T373">tur-An</ta>
            <ta e="T375" id="Seg_4824" s="T374">kel-TI-tA</ta>
            <ta e="T376" id="Seg_4825" s="T375">di͡e-An</ta>
            <ta e="T377" id="Seg_4826" s="T376">tur-An</ta>
            <ta e="T378" id="Seg_4827" s="T377">kel-An</ta>
            <ta e="T379" id="Seg_4828" s="T378">bu͡ollagɨna</ta>
            <ta e="T380" id="Seg_4829" s="T379">kaːtʼer-TAːgAr</ta>
            <ta e="T381" id="Seg_4830" s="T380">da</ta>
            <ta e="T382" id="Seg_4831" s="T381">ulakan</ta>
            <ta e="T383" id="Seg_4832" s="T382">boldok-nI</ta>
            <ta e="T384" id="Seg_4833" s="T383">ɨl-An</ta>
            <ta e="T385" id="Seg_4834" s="T384">baran</ta>
            <ta e="T386" id="Seg_4835" s="T385">dʼe</ta>
            <ta e="T387" id="Seg_4836" s="T386">bɨrak-BIT</ta>
            <ta e="T388" id="Seg_4837" s="T387">kim</ta>
            <ta e="T389" id="Seg_4838" s="T388">dʼe</ta>
            <ta e="T390" id="Seg_4839" s="T389">ɨt-TAr-AːččI-tA</ta>
            <ta e="T391" id="Seg_4840" s="T390">ol</ta>
            <ta e="T392" id="Seg_4841" s="T391">bɨrak-BIT-tI-GAr</ta>
            <ta e="T393" id="Seg_4842" s="T392">eː</ta>
            <ta e="T394" id="Seg_4843" s="T393">arɨːččɨ</ta>
            <ta e="T395" id="Seg_4844" s="T394">ebe</ta>
            <ta e="T396" id="Seg_4845" s="T395">ü͡ös-tI-GAr</ta>
            <ta e="T397" id="Seg_4846" s="T396">tij-BIT-I-m</ta>
            <ta e="T398" id="Seg_4847" s="T397">tij-Ar-tI-n</ta>
            <ta e="T399" id="Seg_4848" s="T398">kɨtta</ta>
            <ta e="T400" id="Seg_4849" s="T399">bu͡olla</ta>
            <ta e="T401" id="Seg_4850" s="T400">ol</ta>
            <ta e="T402" id="Seg_4851" s="T401">dolgun-tI-GAr</ta>
            <ta e="T403" id="Seg_4852" s="T402">bɨrak-BIT</ta>
            <ta e="T404" id="Seg_4853" s="T403">dolgun-tI-GAr</ta>
            <ta e="T405" id="Seg_4854" s="T404">čut</ta>
            <ta e="T406" id="Seg_4855" s="T405">tü͡ör-A</ta>
            <ta e="T407" id="Seg_4856" s="T406">bar-A</ta>
            <ta e="T408" id="Seg_4857" s="T407">hɨs-BIT</ta>
            <ta e="T409" id="Seg_4858" s="T408">ol</ta>
            <ta e="T410" id="Seg_4859" s="T409">küreː-TI-BIt</ta>
            <ta e="T411" id="Seg_4860" s="T410">di͡e-BIT</ta>
            <ta e="T412" id="Seg_4861" s="T411">bu͡olla</ta>
            <ta e="T413" id="Seg_4862" s="T412">oččogo</ta>
            <ta e="T414" id="Seg_4863" s="T413">hette</ta>
            <ta e="T415" id="Seg_4864" s="T414">kihi-ttAn</ta>
            <ta e="T416" id="Seg_4865" s="T415">čogotok</ta>
            <ta e="T417" id="Seg_4866" s="T416">kihi-LAːK</ta>
            <ta e="T418" id="Seg_4867" s="T417">kel-BIT</ta>
            <ta e="T419" id="Seg_4868" s="T418">ogut-An-LAr</ta>
            <ta e="T420" id="Seg_4869" s="T419">beje-LArA</ta>
            <ta e="T421" id="Seg_4870" s="T420">tu͡ok-nI</ta>
            <ta e="T422" id="Seg_4871" s="T421">da</ta>
            <ta e="T423" id="Seg_4872" s="T422">bul-BAkkA</ta>
            <ta e="T424" id="Seg_4873" s="T423">ergij-An</ta>
            <ta e="T425" id="Seg_4874" s="T424">kel</ta>
            <ta e="T426" id="Seg_4875" s="T425">ol</ta>
            <ta e="T427" id="Seg_4876" s="T426">kel-An</ta>
            <ta e="T428" id="Seg_4877" s="T427">ol</ta>
            <ta e="T429" id="Seg_4878" s="T428">üːhe</ta>
            <ta e="T430" id="Seg_4879" s="T429">dek</ta>
            <ta e="T431" id="Seg_4880" s="T430">bar-BIT</ta>
            <ta e="T432" id="Seg_4881" s="T431">onton</ta>
            <ta e="T433" id="Seg_4882" s="T432">Čagɨdaj-nI</ta>
            <ta e="T434" id="Seg_4883" s="T433">bu͡ollagɨna</ta>
            <ta e="T435" id="Seg_4884" s="T434">hajɨn</ta>
            <ta e="T436" id="Seg_4885" s="T435">biːrge</ta>
            <ta e="T437" id="Seg_4886" s="T436">olor-Ar-LAr</ta>
            <ta e="T438" id="Seg_4887" s="T437">ühü</ta>
            <ta e="T439" id="Seg_4888" s="T438">ahaː-An</ta>
            <ta e="T440" id="Seg_4889" s="T439">olor-An-LAr</ta>
            <ta e="T441" id="Seg_4890" s="T440">ka</ta>
            <ta e="T442" id="Seg_4891" s="T441">huruk</ta>
            <ta e="T443" id="Seg_4892" s="T442">kel-BIT</ta>
            <ta e="T444" id="Seg_4893" s="T443">bu</ta>
            <ta e="T445" id="Seg_4894" s="T444">huruk-nI</ta>
            <ta e="T446" id="Seg_4895" s="T445">bu͡ollagɨna</ta>
            <ta e="T447" id="Seg_4896" s="T446">Nosku͡o-GA</ta>
            <ta e="T448" id="Seg_4897" s="T447">tiri͡er-IAK-LArI-n</ta>
            <ta e="T449" id="Seg_4898" s="T448">naːda</ta>
            <ta e="T450" id="Seg_4899" s="T449">kantan</ta>
            <ta e="T451" id="Seg_4900" s="T450">ere</ta>
            <ta e="T452" id="Seg_4901" s="T451">kel-BIT</ta>
            <ta e="T453" id="Seg_4902" s="T452">tɨː-nAn</ta>
            <ta e="T454" id="Seg_4903" s="T453">oččogo</ta>
            <ta e="T455" id="Seg_4904" s="T454">tu͡ok</ta>
            <ta e="T456" id="Seg_4905" s="T455">lu͡otka-tA</ta>
            <ta e="T457" id="Seg_4906" s="T456">kel-IAK.[tA]=Ij</ta>
            <ta e="T458" id="Seg_4907" s="T457">ka</ta>
            <ta e="T459" id="Seg_4908" s="T458">Čagɨdaj</ta>
            <ta e="T460" id="Seg_4909" s="T459">bu</ta>
            <ta e="T461" id="Seg_4910" s="T460">huruk-nI</ta>
            <ta e="T462" id="Seg_4911" s="T461">bu͡olla</ta>
            <ta e="T463" id="Seg_4912" s="T462">kim-GA</ta>
            <ta e="T464" id="Seg_4913" s="T463">tiri͡er</ta>
            <ta e="T465" id="Seg_4914" s="T464">Kires-GA</ta>
            <ta e="T466" id="Seg_4915" s="T465">Rɨmnaj</ta>
            <ta e="T467" id="Seg_4916" s="T466">Kires-tI-GAr</ta>
            <ta e="T468" id="Seg_4917" s="T467">kör</ta>
            <ta e="T469" id="Seg_4918" s="T468">ɨraːk-In</ta>
            <ta e="T470" id="Seg_4919" s="T469">Rɨmnaj</ta>
            <ta e="T471" id="Seg_4920" s="T470">ɨnaraː</ta>
            <ta e="T472" id="Seg_4921" s="T471">örüt-tI-GAr</ta>
            <ta e="T478" id="Seg_4922" s="T476">hu͡ok</ta>
            <ta e="T479" id="Seg_4923" s="T478">ol</ta>
            <ta e="T480" id="Seg_4924" s="T479">beje-LAr-tI-n</ta>
            <ta e="T482" id="Seg_4925" s="T480">gi͡en-LArA</ta>
            <ta e="T485" id="Seg_4926" s="T482">eː</ta>
            <ta e="T486" id="Seg_4927" s="T485">Bi͡egičep-LArA</ta>
            <ta e="T487" id="Seg_4928" s="T486">h-onon</ta>
            <ta e="T488" id="Seg_4929" s="T487">ajannaː-BIT</ta>
            <ta e="T489" id="Seg_4930" s="T488">bu͡ol-TI-tA</ta>
            <ta e="T490" id="Seg_4931" s="T489">ol</ta>
            <ta e="T491" id="Seg_4932" s="T490">bar-BIT-tI-nAn</ta>
            <ta e="T492" id="Seg_4933" s="T491">bar-BIT</ta>
            <ta e="T493" id="Seg_4934" s="T492">bu</ta>
            <ta e="T494" id="Seg_4935" s="T493">dek</ta>
            <ta e="T495" id="Seg_4936" s="T494">üːhe</ta>
            <ta e="T496" id="Seg_4937" s="T495">dek</ta>
            <ta e="T497" id="Seg_4938" s="T496">dʼe</ta>
            <ta e="T498" id="Seg_4939" s="T497">ol</ta>
            <ta e="T499" id="Seg_4940" s="T498">bu͡o</ta>
            <ta e="T500" id="Seg_4941" s="T499">ol</ta>
            <ta e="T501" id="Seg_4942" s="T500">huruk-nI</ta>
            <ta e="T502" id="Seg_4943" s="T501">ilt-A-r-Ar-LAr</ta>
            <ta e="T503" id="Seg_4944" s="T502">oččogo</ta>
            <ta e="T504" id="Seg_4945" s="T503">huruk-LAːK-LAr</ta>
            <ta e="T505" id="Seg_4946" s="T504">e-BIT-tA</ta>
            <ta e="T506" id="Seg_4947" s="T505">bu͡olla</ta>
            <ta e="T507" id="Seg_4948" s="T506">tojon-LAːK-LAr</ta>
            <ta e="T508" id="Seg_4949" s="T507">bu͡o</ta>
            <ta e="T512" id="Seg_4950" s="T511">haka-LAr</ta>
            <ta e="T513" id="Seg_4951" s="T512">ol</ta>
            <ta e="T514" id="Seg_4952" s="T513">ülehit-LArA</ta>
            <ta e="T515" id="Seg_4953" s="T514">savʼet-LArA</ta>
            <ta e="T516" id="Seg_4954" s="T515">tu͡ok-LArA</ta>
            <ta e="T517" id="Seg_4955" s="T516">huruk</ta>
            <ta e="T518" id="Seg_4956" s="T517">ɨːt-Ar-LAr</ta>
            <ta e="T519" id="Seg_4957" s="T518">bu͡o</ta>
            <ta e="T520" id="Seg_4958" s="T519">innʼe</ta>
            <ta e="T521" id="Seg_4959" s="T520">gɨn</ta>
            <ta e="T522" id="Seg_4960" s="T521">bu</ta>
            <ta e="T523" id="Seg_4961" s="T522">ogonnʼor</ta>
            <ta e="T524" id="Seg_4962" s="T523">bu͡olla</ta>
            <ta e="T525" id="Seg_4963" s="T524">dʼe</ta>
            <ta e="T526" id="Seg_4964" s="T525">ki͡ehe</ta>
            <ta e="T527" id="Seg_4965" s="T526">kamnaː-Ar</ta>
            <ta e="T528" id="Seg_4966" s="T527">harsi͡erda</ta>
            <ta e="T529" id="Seg_4967" s="T528">erde-ČIt</ta>
            <ta e="T530" id="Seg_4968" s="T529">dʼaktar</ta>
            <ta e="T531" id="Seg_4969" s="T530">tur-Ar</ta>
            <ta e="T532" id="Seg_4970" s="T531">kem-tI-GAr</ta>
            <ta e="T533" id="Seg_4971" s="T532">kel-IAK-m</ta>
            <ta e="T534" id="Seg_4972" s="T533">di͡e-BIT</ta>
            <ta e="T535" id="Seg_4973" s="T534">bu</ta>
            <ta e="T536" id="Seg_4974" s="T535">ogonnʼor</ta>
            <ta e="T537" id="Seg_4975" s="T536">o</ta>
            <ta e="T538" id="Seg_4976" s="T537">kajdak</ta>
            <ta e="T539" id="Seg_4977" s="T538">gɨn-An</ta>
            <ta e="T540" id="Seg_4978" s="T539">kel-AːrI</ta>
            <ta e="T541" id="Seg_4979" s="T540">gɨn-A-GIn</ta>
            <ta e="T542" id="Seg_4980" s="T541">Rɨmnaj</ta>
            <ta e="T543" id="Seg_4981" s="T542">ɨnaraː</ta>
            <ta e="T544" id="Seg_4982" s="T543">örüt-tI-ttAn</ta>
            <ta e="T545" id="Seg_4983" s="T544">hürdeːk</ta>
            <ta e="T546" id="Seg_4984" s="T545">ɨraːk</ta>
            <ta e="T547" id="Seg_4985" s="T546">Popigaj-ttAn</ta>
            <ta e="T548" id="Seg_4986" s="T547">dʼe</ta>
            <ta e="T549" id="Seg_4987" s="T548">bu</ta>
            <ta e="T550" id="Seg_4988" s="T549">ogonnʼor</ta>
            <ta e="T551" id="Seg_4989" s="T550">kaːm-An</ta>
            <ta e="T552" id="Seg_4990" s="T551">kaːl-BIT</ta>
            <ta e="T553" id="Seg_4991" s="T552">e</ta>
            <ta e="T554" id="Seg_4992" s="T553">bar-An</ta>
            <ta e="T555" id="Seg_4993" s="T554">kaːl-BIT</ta>
            <ta e="T556" id="Seg_4994" s="T555">tɨː-nAn</ta>
            <ta e="T557" id="Seg_4995" s="T556">harsi͡erda</ta>
            <ta e="T558" id="Seg_4996" s="T557">emeːksin-LAr</ta>
            <ta e="T561" id="Seg_4997" s="T560">erde-ČIt</ta>
            <ta e="T562" id="Seg_4998" s="T561">dʼaktar-LAr</ta>
            <ta e="T563" id="Seg_4999" s="T562">tur-Ar-LAr</ta>
            <ta e="T564" id="Seg_5000" s="T563">naprimer</ta>
            <ta e="T565" id="Seg_5001" s="T564">oččogo</ta>
            <ta e="T566" id="Seg_5002" s="T565">hette</ta>
            <ta e="T567" id="Seg_5003" s="T566">čaːs</ta>
            <ta e="T568" id="Seg_5004" s="T567">itte</ta>
            <ta e="T569" id="Seg_5005" s="T568">e-BIT</ta>
            <ta e="T570" id="Seg_5006" s="T569">eni</ta>
            <ta e="T571" id="Seg_5007" s="T570">tu͡ok</ta>
            <ta e="T572" id="Seg_5008" s="T571">čaːs-tA</ta>
            <ta e="T573" id="Seg_5009" s="T572">kel-IAK.[tA]=Ij</ta>
            <ta e="T574" id="Seg_5010" s="T573">kün-LArI-nAn</ta>
            <ta e="T575" id="Seg_5011" s="T574">kör-Ar-LAr</ta>
            <ta e="T576" id="Seg_5012" s="T575">bu͡o</ta>
            <ta e="T577" id="Seg_5013" s="T576">araː</ta>
            <ta e="T578" id="Seg_5014" s="T577">hette</ta>
            <ta e="T579" id="Seg_5015" s="T578">čaːs-GA</ta>
            <ta e="T580" id="Seg_5016" s="T579">Čagɨdaj</ta>
            <ta e="T581" id="Seg_5017" s="T580">kel-BIT</ta>
            <ta e="T582" id="Seg_5018" s="T581">tɨː-tA</ta>
            <ta e="T583" id="Seg_5019" s="T582">umus-A-n-A</ta>
            <ta e="T584" id="Seg_5020" s="T583">hɨt-Ar</ta>
            <ta e="T585" id="Seg_5021" s="T584">diː</ta>
            <ta e="T586" id="Seg_5022" s="T585">kör</ta>
            <ta e="T587" id="Seg_5023" s="T586">onnuk</ta>
            <ta e="T588" id="Seg_5024" s="T587">korsun</ta>
            <ta e="T589" id="Seg_5025" s="T588">kihi-LAr</ta>
            <ta e="T590" id="Seg_5026" s="T589">baːr-LAr</ta>
            <ta e="T591" id="Seg_5027" s="T590">onton</ta>
            <ta e="T592" id="Seg_5028" s="T591">dʼaktar</ta>
            <ta e="T593" id="Seg_5029" s="T592">töröː-Ar</ta>
            <ta e="T594" id="Seg_5030" s="T593">hol</ta>
            <ta e="T595" id="Seg_5031" s="T594">huːrt-LArI-GAr</ta>
            <ta e="T596" id="Seg_5032" s="T595">bu</ta>
            <ta e="T597" id="Seg_5033" s="T596">dʼaktar</ta>
            <ta e="T598" id="Seg_5034" s="T597">öl-AːrI</ta>
            <ta e="T599" id="Seg_5035" s="T598">gɨn-BIT</ta>
            <ta e="T600" id="Seg_5036" s="T599">kajdak</ta>
            <ta e="T601" id="Seg_5037" s="T600">da</ta>
            <ta e="T602" id="Seg_5038" s="T601">gɨn-IAK-LArI-n</ta>
            <ta e="T603" id="Seg_5039" s="T602">bert</ta>
            <ta e="T604" id="Seg_5040" s="T603">du͡oktuːr-tA</ta>
            <ta e="T605" id="Seg_5041" s="T604">hu͡ok</ta>
            <ta e="T606" id="Seg_5042" s="T605">üje-GA</ta>
            <ta e="T607" id="Seg_5043" s="T606">ka</ta>
            <ta e="T608" id="Seg_5044" s="T607">baːbɨska-LArA</ta>
            <ta e="T609" id="Seg_5045" s="T608">muŋ-LAːK</ta>
            <ta e="T610" id="Seg_5046" s="T609">hɨlaj-An</ta>
            <ta e="T611" id="Seg_5047" s="T610">büt-BIT</ta>
            <ta e="T612" id="Seg_5048" s="T611">kaja</ta>
            <ta e="T613" id="Seg_5049" s="T612">Čagɨdaj</ta>
            <ta e="T614" id="Seg_5050" s="T613">kel-TIn</ta>
            <ta e="T615" id="Seg_5051" s="T614">dʼaktar</ta>
            <ta e="T616" id="Seg_5052" s="T615">öl-AːrI</ta>
            <ta e="T617" id="Seg_5053" s="T616">gɨn-TI-tA</ta>
            <ta e="T618" id="Seg_5054" s="T617">kajdak</ta>
            <ta e="T619" id="Seg_5055" s="T618">eme</ta>
            <ta e="T620" id="Seg_5056" s="T619">gɨn-TIn</ta>
            <ta e="T621" id="Seg_5057" s="T620">oŋu͡ok</ta>
            <ta e="T622" id="Seg_5058" s="T621">araːr-TIn</ta>
            <ta e="T623" id="Seg_5059" s="T622">manna</ta>
            <ta e="T624" id="Seg_5060" s="T623">h-innʼe</ta>
            <ta e="T625" id="Seg_5061" s="T624">bu</ta>
            <ta e="T626" id="Seg_5062" s="T625">Čagɨdaj</ta>
            <ta e="T627" id="Seg_5063" s="T626">kel-Ar</ta>
            <ta e="T628" id="Seg_5064" s="T627">kajdak</ta>
            <ta e="T631" id="Seg_5065" s="T630">bu</ta>
            <ta e="T632" id="Seg_5066" s="T631">dʼaktar-nI</ta>
            <ta e="T633" id="Seg_5067" s="T632">iliː-LAː-An</ta>
            <ta e="T634" id="Seg_5068" s="T633">ɨl-Ar</ta>
            <ta e="T635" id="Seg_5069" s="T634">ogo-tI-n</ta>
            <ta e="T636" id="Seg_5070" s="T635">h-onon</ta>
            <ta e="T637" id="Seg_5071" s="T636">tɨːnnaːk</ta>
            <ta e="T638" id="Seg_5072" s="T637">keːs-Ar</ta>
            <ta e="T639" id="Seg_5073" s="T638">kör</ta>
            <ta e="T640" id="Seg_5074" s="T639">onnuk-LAr</ta>
            <ta e="T641" id="Seg_5075" s="T640">baːr</ta>
            <ta e="T642" id="Seg_5076" s="T641">e-TI-LAr</ta>
            <ta e="T643" id="Seg_5077" s="T642">kɨrdʼagas-LAr</ta>
            <ta e="T644" id="Seg_5078" s="T643">i</ta>
            <ta e="T645" id="Seg_5079" s="T644">Čakɨːla</ta>
            <ta e="T646" id="Seg_5080" s="T645">ogonnʼor</ta>
            <ta e="T647" id="Seg_5081" s="T646">ehe-tA</ta>
            <ta e="T648" id="Seg_5082" s="T647">ol-tI-ŋ</ta>
            <ta e="T652" id="Seg_5083" s="T651">eː</ta>
            <ta e="T653" id="Seg_5084" s="T652">oččogo</ta>
            <ta e="T654" id="Seg_5085" s="T653">ol</ta>
            <ta e="T655" id="Seg_5086" s="T654">Čagɨdaj-I-ŋ</ta>
            <ta e="T656" id="Seg_5087" s="T655">dʼaktar-tA</ta>
            <ta e="T657" id="Seg_5088" s="T656">bu͡ollagɨna</ta>
            <ta e="T658" id="Seg_5089" s="T657">emeːksin</ta>
            <ta e="T659" id="Seg_5090" s="T658">Anna</ta>
            <ta e="T660" id="Seg_5091" s="T659">inʼe-tA</ta>
            <ta e="T661" id="Seg_5092" s="T660">emeːksin</ta>
            <ta e="T662" id="Seg_5093" s="T661">bu</ta>
            <ta e="T663" id="Seg_5094" s="T662">Čeːke</ta>
            <ta e="T664" id="Seg_5095" s="T663">emeːksin</ta>
            <ta e="T665" id="Seg_5096" s="T664">ol-tI-ŋ</ta>
            <ta e="T666" id="Seg_5097" s="T665">teːte-tA</ta>
            <ta e="T667" id="Seg_5098" s="T666">bihigi</ta>
            <ta e="T668" id="Seg_5099" s="T667">ehe-BItI-n</ta>
            <ta e="T669" id="Seg_5100" s="T668">kɨtta</ta>
            <ta e="T670" id="Seg_5101" s="T669">biːrge</ta>
            <ta e="T671" id="Seg_5102" s="T670">töröː-BIT-LAr</ta>
            <ta e="T672" id="Seg_5103" s="T671">Kudrʼakov</ta>
            <ta e="T673" id="Seg_5104" s="T672">ogonnʼor</ta>
         </annotation>
         <annotation name="ge" tierref="ge-KiPP">
            <ta e="T1" id="Seg_5105" s="T0">Begichev-ACC</ta>
            <ta e="T2" id="Seg_5106" s="T1">take.away-PRS-3PL</ta>
            <ta e="T5" id="Seg_5107" s="T4">earth-EP-INSTR</ta>
            <ta e="T6" id="Seg_5108" s="T5">go-PTCP.PRS</ta>
            <ta e="T7" id="Seg_5109" s="T6">be-PST1-3SG</ta>
            <ta e="T8" id="Seg_5110" s="T7">before</ta>
            <ta e="T9" id="Seg_5111" s="T8">expedition-PL.[NOM]</ta>
            <ta e="T10" id="Seg_5112" s="T9">that-ACC</ta>
            <ta e="T11" id="Seg_5113" s="T10">grandfather-1SG.[NOM]</ta>
            <ta e="T12" id="Seg_5114" s="T11">tell-HAB.[3SG]</ta>
            <ta e="T13" id="Seg_5115" s="T12">my</ta>
            <ta e="T14" id="Seg_5116" s="T13">mother-1SG.[NOM]</ta>
            <ta e="T15" id="Seg_5117" s="T14">grandfather.[NOM]</ta>
            <ta e="T16" id="Seg_5118" s="T15">that</ta>
            <ta e="T17" id="Seg_5119" s="T16">mother-1SG.[NOM]</ta>
            <ta e="T18" id="Seg_5120" s="T17">sibling-PL-3SG.[NOM]</ta>
            <ta e="T19" id="Seg_5121" s="T18">Markel</ta>
            <ta e="T20" id="Seg_5122" s="T19">old.man.[NOM]</ta>
            <ta e="T21" id="Seg_5123" s="T20">Elena.[NOM]</ta>
            <ta e="T22" id="Seg_5124" s="T21">father-3SG.[NOM]</ta>
            <ta e="T23" id="Seg_5125" s="T22">old.man.[NOM]</ta>
            <ta e="T24" id="Seg_5126" s="T23">that-PL.[NOM]</ta>
            <ta e="T25" id="Seg_5127" s="T24">chat-PTCP.PRS-3PL-ACC</ta>
            <ta e="T26" id="Seg_5128" s="T25">hear-HAB-1SG</ta>
            <ta e="T27" id="Seg_5129" s="T26">this</ta>
            <ta e="T28" id="Seg_5130" s="T27">Begichev.[NOM]</ta>
            <ta e="T29" id="Seg_5131" s="T28">though</ta>
            <ta e="T30" id="Seg_5132" s="T29">reindeer-INSTR</ta>
            <ta e="T31" id="Seg_5133" s="T30">travel-PRS.[3SG]</ta>
            <ta e="T32" id="Seg_5134" s="T31">Begichev.[NOM]</ta>
            <ta e="T33" id="Seg_5135" s="T32">island-3SG-DAT/LOC</ta>
            <ta e="T34" id="Seg_5136" s="T33">seven</ta>
            <ta e="T35" id="Seg_5137" s="T34">human.being-PROPR</ta>
            <ta e="T36" id="Seg_5138" s="T35">this</ta>
            <ta e="T37" id="Seg_5139" s="T36">human.being.[NOM]</ta>
            <ta e="T38" id="Seg_5140" s="T37">travel-CVB.SEQ</ta>
            <ta e="T39" id="Seg_5141" s="T38">go-CVB.SEQ</ta>
            <ta e="T40" id="Seg_5142" s="T39">reindeer-INSTR</ta>
            <ta e="T41" id="Seg_5143" s="T40">take.away-PRS-3PL</ta>
            <ta e="T42" id="Seg_5144" s="T41">EMPH</ta>
            <ta e="T43" id="Seg_5145" s="T42">different-ABL-different</ta>
            <ta e="T44" id="Seg_5146" s="T43">reindeer-INSTR</ta>
            <ta e="T45" id="Seg_5147" s="T44">take.away-PRS-3PL</ta>
            <ta e="T46" id="Seg_5148" s="T45">new-ABL-new</ta>
            <ta e="T47" id="Seg_5149" s="T46">reindeer-INSTR</ta>
            <ta e="T48" id="Seg_5150" s="T47">take.away-PRS-3PL</ta>
            <ta e="T49" id="Seg_5151" s="T48">there</ta>
            <ta e="T50" id="Seg_5152" s="T49">Chagydaj</ta>
            <ta e="T51" id="Seg_5153" s="T50">old.man-ACC</ta>
            <ta e="T52" id="Seg_5154" s="T51">bring-EP-CAUS-PRS-3PL</ta>
            <ta e="T53" id="Seg_5155" s="T52">MOD</ta>
            <ta e="T54" id="Seg_5156" s="T53">1SG.[NOM]</ta>
            <ta e="T55" id="Seg_5157" s="T54">old.man-EP-1SG.[NOM]</ta>
            <ta e="T56" id="Seg_5158" s="T55">grandfather-3SG-ACC-PROPR</ta>
            <ta e="T57" id="Seg_5159" s="T56">grandfather-3PL.[NOM]</ta>
            <ta e="T58" id="Seg_5160" s="T57">that</ta>
            <ta e="T59" id="Seg_5161" s="T58">old.man.[NOM]</ta>
            <ta e="T60" id="Seg_5162" s="T59">what.[NOM]</ta>
            <ta e="T61" id="Seg_5163" s="T60">NEG</ta>
            <ta e="T62" id="Seg_5164" s="T61">chase-NEG.PTCP.PST</ta>
            <ta e="T63" id="Seg_5165" s="T62">human.being-3SG.[NOM]</ta>
            <ta e="T64" id="Seg_5166" s="T63">be-PST1-3SG</ta>
            <ta e="T65" id="Seg_5167" s="T64">very</ta>
            <ta e="T66" id="Seg_5168" s="T65">energetic</ta>
            <ta e="T67" id="Seg_5169" s="T66">human.being.[NOM]</ta>
            <ta e="T68" id="Seg_5170" s="T67">that</ta>
            <ta e="T69" id="Seg_5171" s="T68">old.man</ta>
            <ta e="T70" id="Seg_5172" s="T69">can-NEG.PTCP</ta>
            <ta e="T71" id="Seg_5173" s="T70">reindeer-PL-ACC</ta>
            <ta e="T72" id="Seg_5174" s="T71">harness-CAUS-CVB.SEQ</ta>
            <ta e="T73" id="Seg_5175" s="T72">after</ta>
            <ta e="T74" id="Seg_5176" s="T73">Begichev.[NOM]</ta>
            <ta e="T75" id="Seg_5177" s="T74">island-3SG-DAT/LOC</ta>
            <ta e="T76" id="Seg_5178" s="T75">bring-EP-CAUS-PRS-3PL</ta>
            <ta e="T77" id="Seg_5179" s="T76">EMPH</ta>
            <ta e="T78" id="Seg_5180" s="T77">there</ta>
            <ta e="T79" id="Seg_5181" s="T78">reindeer-PL.[NOM]</ta>
            <ta e="T80" id="Seg_5182" s="T79">move-PRS-3PL</ta>
            <ta e="T81" id="Seg_5183" s="T80">it.is.said</ta>
            <ta e="T82" id="Seg_5184" s="T81">EMPH</ta>
            <ta e="T83" id="Seg_5185" s="T82">EMPH</ta>
            <ta e="T84" id="Seg_5186" s="T83">reindeer-PL.[NOM]</ta>
            <ta e="T85" id="Seg_5187" s="T84">move-PRS-3PL</ta>
            <ta e="T86" id="Seg_5188" s="T85">this</ta>
            <ta e="T87" id="Seg_5189" s="T86">human.being.[NOM]</ta>
            <ta e="T88" id="Seg_5190" s="T87">cap-3SG-ACC</ta>
            <ta e="T89" id="Seg_5191" s="T88">that</ta>
            <ta e="T90" id="Seg_5192" s="T89">Begichev.[NOM]</ta>
            <ta e="T91" id="Seg_5193" s="T90">own-3SG-GEN</ta>
            <ta e="T92" id="Seg_5194" s="T91">cap-3SG-ACC</ta>
            <ta e="T93" id="Seg_5195" s="T92">reindeer.[NOM]</ta>
            <ta e="T94" id="Seg_5196" s="T93">put.on-CVB.SEQ</ta>
            <ta e="T95" id="Seg_5197" s="T94">throw-PRS.[3SG]</ta>
            <ta e="T96" id="Seg_5198" s="T95">it.is.said</ta>
            <ta e="T97" id="Seg_5199" s="T96">this</ta>
            <ta e="T98" id="Seg_5200" s="T97">sledge-ACC</ta>
            <ta e="T99" id="Seg_5201" s="T98">through</ta>
            <ta e="T100" id="Seg_5202" s="T99">cap.[NOM]</ta>
            <ta e="T101" id="Seg_5203" s="T100">fall-CVB.SEQ</ta>
            <ta e="T102" id="Seg_5204" s="T101">be-TEMP-3SG</ta>
            <ta e="T103" id="Seg_5205" s="T102">that-ACC</ta>
            <ta e="T104" id="Seg_5206" s="T103">old.man.[NOM]</ta>
            <ta e="T105" id="Seg_5207" s="T104">sledge-ACC</ta>
            <ta e="T106" id="Seg_5208" s="T105">through</ta>
            <ta e="T107" id="Seg_5209" s="T106">jump-CVB.SEQ</ta>
            <ta e="T108" id="Seg_5210" s="T107">catch-CVB.SEQ</ta>
            <ta e="T109" id="Seg_5211" s="T108">take-PST2.[3SG]</ta>
            <ta e="T110" id="Seg_5212" s="T109">EMPH</ta>
            <ta e="T111" id="Seg_5213" s="T110">earth-DAT/LOC</ta>
            <ta e="T112" id="Seg_5214" s="T111">fall-PTCP.FUT-3SG-GEN</ta>
            <ta e="T113" id="Seg_5215" s="T112">that.side.[NOM]</ta>
            <ta e="T114" id="Seg_5216" s="T113">side-3SG-DAT/LOC</ta>
            <ta e="T115" id="Seg_5217" s="T114">that-ACC</ta>
            <ta e="T116" id="Seg_5218" s="T115">that</ta>
            <ta e="T119" id="Seg_5219" s="T118">Begichev-3SG.[NOM]</ta>
            <ta e="T120" id="Seg_5220" s="T119">say-PRS.[3SG]</ta>
            <ta e="T121" id="Seg_5221" s="T120">it.is.said</ta>
            <ta e="T122" id="Seg_5222" s="T121">oh</ta>
            <ta e="T123" id="Seg_5223" s="T122">Dolgan</ta>
            <ta e="T124" id="Seg_5224" s="T123">earth-3SG-DAT/LOC</ta>
            <ta e="T125" id="Seg_5225" s="T124">and</ta>
            <ta e="T126" id="Seg_5226" s="T125">energetic</ta>
            <ta e="T127" id="Seg_5227" s="T126">human.being.[NOM]</ta>
            <ta e="T128" id="Seg_5228" s="T127">there.is</ta>
            <ta e="T129" id="Seg_5229" s="T128">be-PRS.[3SG]</ta>
            <ta e="T131" id="Seg_5230" s="T130">well</ta>
            <ta e="T132" id="Seg_5231" s="T131">this</ta>
            <ta e="T133" id="Seg_5232" s="T132">old.man.[NOM]</ta>
            <ta e="T134" id="Seg_5233" s="T133">Begichev.[NOM]</ta>
            <ta e="T135" id="Seg_5234" s="T134">though</ta>
            <ta e="T136" id="Seg_5235" s="T135">that</ta>
            <ta e="T137" id="Seg_5236" s="T136">that</ta>
            <ta e="T138" id="Seg_5237" s="T137">island-DAT/LOC</ta>
            <ta e="T139" id="Seg_5238" s="T138">reach-PST2-EP-1SG</ta>
            <ta e="T140" id="Seg_5239" s="T139">say-CVB.SEQ</ta>
            <ta e="T141" id="Seg_5240" s="T140">tell-PRS.[3SG]</ta>
            <ta e="T142" id="Seg_5241" s="T141">it.is.said</ta>
            <ta e="T143" id="Seg_5242" s="T142">this</ta>
            <ta e="T144" id="Seg_5243" s="T143">what</ta>
            <ta e="T145" id="Seg_5244" s="T144">miracle-ACC</ta>
            <ta e="T146" id="Seg_5245" s="T145">see-PST1-2SG</ta>
            <ta e="T147" id="Seg_5246" s="T146">there</ta>
            <ta e="T148" id="Seg_5247" s="T147">say-CVB.SEQ</ta>
            <ta e="T149" id="Seg_5248" s="T148">ask-PRS-3PL</ta>
            <ta e="T150" id="Seg_5249" s="T149">Dolgan-PL.[NOM]</ta>
            <ta e="T151" id="Seg_5250" s="T150">EMPH</ta>
            <ta e="T152" id="Seg_5251" s="T151">EMPH</ta>
            <ta e="T153" id="Seg_5252" s="T152">that-DAT/LOC</ta>
            <ta e="T154" id="Seg_5253" s="T153">1SG.[NOM]</ta>
            <ta e="T155" id="Seg_5254" s="T154">miracle-ACC</ta>
            <ta e="T156" id="Seg_5255" s="T155">what-ACC</ta>
            <ta e="T157" id="Seg_5256" s="T156">NEG</ta>
            <ta e="T158" id="Seg_5257" s="T157">very</ta>
            <ta e="T159" id="Seg_5258" s="T158">find-NEG-PST1-1SG</ta>
            <ta e="T160" id="Seg_5259" s="T159">walrus-3PL.[NOM]</ta>
            <ta e="T161" id="Seg_5260" s="T160">though</ta>
            <ta e="T162" id="Seg_5261" s="T161">tooth-3PL-ABL</ta>
            <ta e="T163" id="Seg_5262" s="T162">tie-CVB.SEQ</ta>
            <ta e="T164" id="Seg_5263" s="T163">after</ta>
            <ta e="T165" id="Seg_5264" s="T164">sleep-PRS-3PL</ta>
            <ta e="T166" id="Seg_5265" s="T165">sleep-CVB.SIM</ta>
            <ta e="T167" id="Seg_5266" s="T166">lie-PTCP.PRS-3PL-ACC</ta>
            <ta e="T168" id="Seg_5267" s="T167">tie-CVB.SEQ</ta>
            <ta e="T169" id="Seg_5268" s="T168">log-ABL</ta>
            <ta e="T170" id="Seg_5269" s="T169">that-ACC</ta>
            <ta e="T171" id="Seg_5270" s="T170">thing.[NOM]</ta>
            <ta e="T172" id="Seg_5271" s="T171">NEG</ta>
            <ta e="T173" id="Seg_5272" s="T172">think-NEG-3PL</ta>
            <ta e="T174" id="Seg_5273" s="T173">pull-CVB.SEQ</ta>
            <ta e="T175" id="Seg_5274" s="T174">go.in-CAUS-PRS-3PL</ta>
            <ta e="T176" id="Seg_5275" s="T175">water-DAT/LOC</ta>
            <ta e="T177" id="Seg_5276" s="T176">go.in-CAUS-CVB.SEQ</ta>
            <ta e="T178" id="Seg_5277" s="T177">throw-PRS-3PL</ta>
            <ta e="T179" id="Seg_5278" s="T178">then</ta>
            <ta e="T180" id="Seg_5279" s="T179">only</ta>
            <ta e="T181" id="Seg_5280" s="T180">island-DAT/LOC</ta>
            <ta e="T182" id="Seg_5281" s="T181">stone.[NOM]</ta>
            <ta e="T183" id="Seg_5282" s="T182">mountain-DAT/LOC</ta>
            <ta e="T184" id="Seg_5283" s="T183">find-PST1-1SG</ta>
            <ta e="T185" id="Seg_5284" s="T184">say-PRS.[3SG]</ta>
            <ta e="T186" id="Seg_5285" s="T185">it.is.said</ta>
            <ta e="T187" id="Seg_5286" s="T186">one</ta>
            <ta e="T188" id="Seg_5287" s="T187">miracle-ACC</ta>
            <ta e="T189" id="Seg_5288" s="T188">well</ta>
            <ta e="T190" id="Seg_5289" s="T189">what-ACC</ta>
            <ta e="T191" id="Seg_5290" s="T190">find-PST1-2SG</ta>
            <ta e="T192" id="Seg_5291" s="T191">that-ACC</ta>
            <ta e="T193" id="Seg_5292" s="T192">say-PRS.[3SG]</ta>
            <ta e="T194" id="Seg_5293" s="T193">EMPH</ta>
            <ta e="T210" id="Seg_5294" s="T209">that-DAT/LOC</ta>
            <ta e="T211" id="Seg_5295" s="T210">go.in-CVB.SEQ</ta>
            <ta e="T212" id="Seg_5296" s="T211">go-PST1-1SG</ta>
            <ta e="T213" id="Seg_5297" s="T212">say-PRS.[3SG]</ta>
            <ta e="T214" id="Seg_5298" s="T213">three</ta>
            <ta e="T215" id="Seg_5299" s="T214">human.being-PROPR.[NOM]</ta>
            <ta e="T216" id="Seg_5300" s="T215">this</ta>
            <ta e="T217" id="Seg_5301" s="T216">human.being-PL.[NOM]</ta>
            <ta e="T218" id="Seg_5302" s="T217">food-POSS</ta>
            <ta e="T219" id="Seg_5303" s="T218">NEG</ta>
            <ta e="T220" id="Seg_5304" s="T219">become-PST2-1PL</ta>
            <ta e="T221" id="Seg_5305" s="T220">then</ta>
            <ta e="T222" id="Seg_5306" s="T221">though</ta>
            <ta e="T223" id="Seg_5307" s="T222">bay-ABL</ta>
            <ta e="T224" id="Seg_5308" s="T223">oh</ta>
            <ta e="T225" id="Seg_5309" s="T224">small</ta>
            <ta e="T226" id="Seg_5310" s="T225">fish-DIM-ACC</ta>
            <ta e="T227" id="Seg_5311" s="T226">catch-EP-MED-CVB.SEQ-3PL</ta>
            <ta e="T228" id="Seg_5312" s="T227">that</ta>
            <ta e="T229" id="Seg_5313" s="T228">door-DAT/LOC</ta>
            <ta e="T230" id="Seg_5314" s="T229">go.in-CVB.SEQ-3PL</ta>
            <ta e="T231" id="Seg_5315" s="T230">eat-CVB.PURP-3PL</ta>
            <ta e="T232" id="Seg_5316" s="T231">go.in-PST2-3PL</ta>
            <ta e="T233" id="Seg_5317" s="T232">this</ta>
            <ta e="T234" id="Seg_5318" s="T233">human.being.[NOM]</ta>
            <ta e="T235" id="Seg_5319" s="T234">only</ta>
            <ta e="T236" id="Seg_5320" s="T235">go.in-PST2-3SG</ta>
            <ta e="T237" id="Seg_5321" s="T236">this</ta>
            <ta e="T238" id="Seg_5322" s="T237">human.being.[NOM]</ta>
            <ta e="T239" id="Seg_5323" s="T238">sit-PRS.[3SG]</ta>
            <ta e="T240" id="Seg_5324" s="T239">sit-TEMP-3SG</ta>
            <ta e="T241" id="Seg_5325" s="T240">what.[NOM]</ta>
            <ta e="T242" id="Seg_5326" s="T241">INDEF</ta>
            <ta e="T243" id="Seg_5327" s="T242">human.being-3SG.[NOM]</ta>
            <ta e="T244" id="Seg_5328" s="T243">come-PST2.[3SG]</ta>
            <ta e="T245" id="Seg_5329" s="T244">EMPH</ta>
            <ta e="T246" id="Seg_5330" s="T245">EMPH</ta>
            <ta e="T247" id="Seg_5331" s="T246">human.being.[NOM]</ta>
            <ta e="T248" id="Seg_5332" s="T247">Q</ta>
            <ta e="T249" id="Seg_5333" s="T248">what.[NOM]</ta>
            <ta e="T250" id="Seg_5334" s="T249">Q</ta>
            <ta e="T251" id="Seg_5335" s="T250">shoulder-ABL</ta>
            <ta e="T252" id="Seg_5336" s="T251">head-POSS</ta>
            <ta e="T253" id="Seg_5337" s="T252">NEG</ta>
            <ta e="T254" id="Seg_5338" s="T253">human.being.[NOM]</ta>
            <ta e="T255" id="Seg_5339" s="T254">go.in-PST2.[3SG]</ta>
            <ta e="T256" id="Seg_5340" s="T255">human.being.[NOM]</ta>
            <ta e="T257" id="Seg_5341" s="T256">Q</ta>
            <ta e="T258" id="Seg_5342" s="T257">what.[NOM]</ta>
            <ta e="T259" id="Seg_5343" s="T258">Q</ta>
            <ta e="T260" id="Seg_5344" s="T259">what-ACC</ta>
            <ta e="T261" id="Seg_5345" s="T260">NEG</ta>
            <ta e="T262" id="Seg_5346" s="T261">understand-NEG-1SG</ta>
            <ta e="T263" id="Seg_5347" s="T262">say-PST2.[3SG]</ta>
            <ta e="T264" id="Seg_5348" s="T263">that</ta>
            <ta e="T265" id="Seg_5349" s="T264">go.in-CVB.SEQ</ta>
            <ta e="T266" id="Seg_5350" s="T265">door-DAT/LOC</ta>
            <ta e="T267" id="Seg_5351" s="T266">sit.down-PST2.[3SG]</ta>
            <ta e="T268" id="Seg_5352" s="T267">EMPH</ta>
            <ta e="T269" id="Seg_5353" s="T268">EMPH</ta>
            <ta e="T270" id="Seg_5354" s="T269">that</ta>
            <ta e="T271" id="Seg_5355" s="T270">sit-CVB.SEQ</ta>
            <ta e="T272" id="Seg_5356" s="T271">though</ta>
            <ta e="T273" id="Seg_5357" s="T272">hey</ta>
            <ta e="T274" id="Seg_5358" s="T273">laugh-CVB.SEQ</ta>
            <ta e="T275" id="Seg_5359" s="T274">bare.ones.teeth-PST2.[3SG]</ta>
            <ta e="T276" id="Seg_5360" s="T275">where</ta>
            <ta e="T277" id="Seg_5361" s="T276">INDEF</ta>
            <ta e="T278" id="Seg_5362" s="T277">eye-PROPR.[NOM]</ta>
            <ta e="T279" id="Seg_5363" s="T278">head-3SG-DAT/LOC</ta>
            <ta e="T280" id="Seg_5364" s="T279">lonely</ta>
            <ta e="T281" id="Seg_5365" s="T280">eye-PROPR.[NOM]</ta>
            <ta e="T282" id="Seg_5366" s="T281">well</ta>
            <ta e="T285" id="Seg_5367" s="T284">friend-PL.[NOM]</ta>
            <ta e="T286" id="Seg_5368" s="T285">strong-ADVZ</ta>
            <ta e="T287" id="Seg_5369" s="T286">hold-REFL-EP-IMP.2PL</ta>
            <ta e="T288" id="Seg_5370" s="T287">say-PRS.[3SG]</ta>
            <ta e="T289" id="Seg_5371" s="T288">EMPH</ta>
            <ta e="T290" id="Seg_5372" s="T289">friend-3SG-ACC</ta>
            <ta e="T291" id="Seg_5373" s="T290">boy-PL-3SG-ACC</ta>
            <ta e="T292" id="Seg_5374" s="T291">Begichev.[NOM]</ta>
            <ta e="T293" id="Seg_5375" s="T292">that-3SG-3SG.[NOM]</ta>
            <ta e="T294" id="Seg_5376" s="T293">hup</ta>
            <ta e="T295" id="Seg_5377" s="T294">say-CVB.SEQ</ta>
            <ta e="T296" id="Seg_5378" s="T295">suck-PST2.[3SG]</ta>
            <ta e="T297" id="Seg_5379" s="T296">two</ta>
            <ta e="T298" id="Seg_5380" s="T297">human.being-3SG-ACC</ta>
            <ta e="T299" id="Seg_5381" s="T298">suck-CVB.SEQ</ta>
            <ta e="T300" id="Seg_5382" s="T299">take-PST2.[3SG]</ta>
            <ta e="T301" id="Seg_5383" s="T300">then</ta>
            <ta e="T302" id="Seg_5384" s="T301">this</ta>
            <ta e="T303" id="Seg_5385" s="T302">human.being.[NOM]</ta>
            <ta e="T304" id="Seg_5386" s="T303">lonely</ta>
            <ta e="T305" id="Seg_5387" s="T304">human.being-PROPR.[NOM]</ta>
            <ta e="T306" id="Seg_5388" s="T305">stay-PST2.[3SG]</ta>
            <ta e="T307" id="Seg_5389" s="T306">there</ta>
            <ta e="T308" id="Seg_5390" s="T307">this</ta>
            <ta e="T309" id="Seg_5391" s="T308">human.being.[NOM]</ta>
            <ta e="T310" id="Seg_5392" s="T309">MOD</ta>
            <ta e="T311" id="Seg_5393" s="T310">pistol-PROPR.[NOM]</ta>
            <ta e="T312" id="Seg_5394" s="T311">be-PST2.[3SG]</ta>
            <ta e="T313" id="Seg_5395" s="T312">that</ta>
            <ta e="T314" id="Seg_5396" s="T313">there</ta>
            <ta e="T315" id="Seg_5397" s="T314">say-PRS-1SG</ta>
            <ta e="T316" id="Seg_5398" s="T315">speak-PTCP.PRS</ta>
            <ta e="T317" id="Seg_5399" s="T316">tongue-EP-INSTR</ta>
            <ta e="T318" id="Seg_5400" s="T317">say-PRS-1SG</ta>
            <ta e="T319" id="Seg_5401" s="T318">say-PST2.[3SG]</ta>
            <ta e="T320" id="Seg_5402" s="T319">pistol-EP-1SG.[NOM]</ta>
            <ta e="T321" id="Seg_5403" s="T320">mouth-3SG-DAT/LOC</ta>
            <ta e="T322" id="Seg_5404" s="T321">well</ta>
            <ta e="T323" id="Seg_5405" s="T322">this</ta>
            <ta e="T324" id="Seg_5406" s="T323">country-DAT/LOC</ta>
            <ta e="T325" id="Seg_5407" s="T324">stay-PRS-1SG</ta>
            <ta e="T326" id="Seg_5408" s="T325">Q</ta>
            <ta e="T327" id="Seg_5409" s="T326">1SG.[NOM]</ta>
            <ta e="T328" id="Seg_5410" s="T327">1SG-ACC</ta>
            <ta e="T329" id="Seg_5411" s="T328">save.[IMP.2SG]</ta>
            <ta e="T330" id="Seg_5412" s="T329">this-ACC</ta>
            <ta e="T331" id="Seg_5413" s="T330">whereto</ta>
            <ta e="T332" id="Seg_5414" s="T331">INDEF</ta>
            <ta e="T333" id="Seg_5415" s="T332">faint-EP-CAUS-PTCP.PRS</ta>
            <ta e="T334" id="Seg_5416" s="T333">place-2SG-DAT/LOC</ta>
            <ta e="T335" id="Seg_5417" s="T334">bring.[IMP.2SG]</ta>
            <ta e="T336" id="Seg_5418" s="T335">say-PRS.[3SG]</ta>
            <ta e="T337" id="Seg_5419" s="T336">forehead-3SG-DAT/LOC</ta>
            <ta e="T338" id="Seg_5420" s="T337">shoot-PST2.[3SG]</ta>
            <ta e="T339" id="Seg_5421" s="T338">MOD</ta>
            <ta e="T340" id="Seg_5422" s="T339">this</ta>
            <ta e="T341" id="Seg_5423" s="T340">pistol-3SG-INSTR</ta>
            <ta e="T342" id="Seg_5424" s="T341">that-3SG-3SG.[NOM]</ta>
            <ta e="T343" id="Seg_5425" s="T342">well</ta>
            <ta e="T344" id="Seg_5426" s="T343">faint-CVB.SEQ</ta>
            <ta e="T345" id="Seg_5427" s="T344">fall-PST2.[3SG]</ta>
            <ta e="T346" id="Seg_5428" s="T345">that</ta>
            <ta e="T347" id="Seg_5429" s="T346">faint-CVB.SEQ</ta>
            <ta e="T348" id="Seg_5430" s="T347">fall-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T349" id="Seg_5431" s="T348">upper.part-EP-INSTR</ta>
            <ta e="T350" id="Seg_5432" s="T349">run-CVB.SEQ</ta>
            <ta e="T351" id="Seg_5433" s="T350">go.out-EP-PST2-3PL</ta>
            <ta e="T352" id="Seg_5434" s="T351">be-COND.[3SG]</ta>
            <ta e="T353" id="Seg_5435" s="T352">Begichev-EP-2SG.[NOM]</ta>
            <ta e="T354" id="Seg_5436" s="T353">tell-PRS.[3SG]</ta>
            <ta e="T357" id="Seg_5437" s="T356">that</ta>
            <ta e="T358" id="Seg_5438" s="T357">reach-CVB.SEQ-1PL</ta>
            <ta e="T359" id="Seg_5439" s="T358">who-DAT/LOC</ta>
            <ta e="T360" id="Seg_5440" s="T359">go-PST1-1PL</ta>
            <ta e="T361" id="Seg_5441" s="T360">say-PRS.[3SG]</ta>
            <ta e="T362" id="Seg_5442" s="T361">EMPH</ta>
            <ta e="T363" id="Seg_5443" s="T362">motorboat-1SG-DAT/LOC</ta>
            <ta e="T364" id="Seg_5444" s="T363">go-PST1-1SG</ta>
            <ta e="T365" id="Seg_5445" s="T364">say-PRS.[3SG]</ta>
            <ta e="T366" id="Seg_5446" s="T365">motorboat-1SG-DAT/LOC</ta>
            <ta e="T367" id="Seg_5447" s="T366">sit.down-CVB.SEQ-1SG</ta>
            <ta e="T368" id="Seg_5448" s="T367">bay.[NOM]</ta>
            <ta e="T369" id="Seg_5449" s="T368">middle-3SG-DAT/LOC</ta>
            <ta e="T370" id="Seg_5450" s="T369">go.in-PST1-1SG</ta>
            <ta e="T371" id="Seg_5451" s="T370">see-PST2-EP-1SG</ta>
            <ta e="T372" id="Seg_5452" s="T371">back-1SG-ABL</ta>
            <ta e="T373" id="Seg_5453" s="T372">recent-EP-1SG.[NOM]</ta>
            <ta e="T374" id="Seg_5454" s="T373">stand.up-CVB.SEQ</ta>
            <ta e="T375" id="Seg_5455" s="T374">come-PST1-3SG</ta>
            <ta e="T376" id="Seg_5456" s="T375">say-CVB.SEQ</ta>
            <ta e="T377" id="Seg_5457" s="T376">stand.up-CVB.SEQ</ta>
            <ta e="T378" id="Seg_5458" s="T377">come-CVB.SEQ</ta>
            <ta e="T379" id="Seg_5459" s="T378">though</ta>
            <ta e="T380" id="Seg_5460" s="T379">motorboat-COMP</ta>
            <ta e="T381" id="Seg_5461" s="T380">EMPH</ta>
            <ta e="T382" id="Seg_5462" s="T381">big</ta>
            <ta e="T383" id="Seg_5463" s="T382">big.stone-ACC</ta>
            <ta e="T384" id="Seg_5464" s="T383">take-CVB.SEQ</ta>
            <ta e="T385" id="Seg_5465" s="T384">after</ta>
            <ta e="T386" id="Seg_5466" s="T385">well</ta>
            <ta e="T387" id="Seg_5467" s="T386">throw-PST2.[3SG]</ta>
            <ta e="T388" id="Seg_5468" s="T387">who.[NOM]</ta>
            <ta e="T389" id="Seg_5469" s="T388">well</ta>
            <ta e="T390" id="Seg_5470" s="T389">shoot-PASS-PTCP.HAB-3SG.[NOM]</ta>
            <ta e="T391" id="Seg_5471" s="T390">that</ta>
            <ta e="T392" id="Seg_5472" s="T391">throw-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T393" id="Seg_5473" s="T392">EMPH</ta>
            <ta e="T394" id="Seg_5474" s="T393">hardly</ta>
            <ta e="T395" id="Seg_5475" s="T394">bay.[NOM]</ta>
            <ta e="T396" id="Seg_5476" s="T395">middle-3SG-DAT/LOC</ta>
            <ta e="T397" id="Seg_5477" s="T396">reach-PST2-EP-1SG</ta>
            <ta e="T398" id="Seg_5478" s="T397">reach-PTCP.PRS-3SG-ACC</ta>
            <ta e="T399" id="Seg_5479" s="T398">with</ta>
            <ta e="T400" id="Seg_5480" s="T399">MOD</ta>
            <ta e="T401" id="Seg_5481" s="T400">that</ta>
            <ta e="T402" id="Seg_5482" s="T401">wave-3SG-DAT/LOC</ta>
            <ta e="T403" id="Seg_5483" s="T402">throw-PTCP.PST</ta>
            <ta e="T404" id="Seg_5484" s="T403">wave-3SG-DAT/LOC</ta>
            <ta e="T405" id="Seg_5485" s="T404">almost</ta>
            <ta e="T406" id="Seg_5486" s="T405">pull.out-CVB.SIM</ta>
            <ta e="T407" id="Seg_5487" s="T406">go-CVB.SIM</ta>
            <ta e="T408" id="Seg_5488" s="T407">almost.do-PST2.[3SG]</ta>
            <ta e="T409" id="Seg_5489" s="T408">that</ta>
            <ta e="T410" id="Seg_5490" s="T409">escape-PST1-1PL</ta>
            <ta e="T411" id="Seg_5491" s="T410">say-PST2.[3SG]</ta>
            <ta e="T412" id="Seg_5492" s="T411">MOD</ta>
            <ta e="T413" id="Seg_5493" s="T412">then</ta>
            <ta e="T414" id="Seg_5494" s="T413">seven</ta>
            <ta e="T415" id="Seg_5495" s="T414">human.being-ABL</ta>
            <ta e="T416" id="Seg_5496" s="T415">lonely</ta>
            <ta e="T417" id="Seg_5497" s="T416">human.being-PROPR</ta>
            <ta e="T418" id="Seg_5498" s="T417">come-PST2.[3SG]</ta>
            <ta e="T419" id="Seg_5499" s="T418">hunger-CVB.SEQ-3PL</ta>
            <ta e="T420" id="Seg_5500" s="T419">self-3PL.[NOM]</ta>
            <ta e="T421" id="Seg_5501" s="T420">what-ACC</ta>
            <ta e="T422" id="Seg_5502" s="T421">NEG</ta>
            <ta e="T423" id="Seg_5503" s="T422">find-NEG.CVB.SIM</ta>
            <ta e="T424" id="Seg_5504" s="T423">turn-CVB.SEQ</ta>
            <ta e="T425" id="Seg_5505" s="T424">come</ta>
            <ta e="T426" id="Seg_5506" s="T425">that</ta>
            <ta e="T427" id="Seg_5507" s="T426">come-CVB.SEQ</ta>
            <ta e="T428" id="Seg_5508" s="T427">that</ta>
            <ta e="T429" id="Seg_5509" s="T428">up</ta>
            <ta e="T430" id="Seg_5510" s="T429">to</ta>
            <ta e="T431" id="Seg_5511" s="T430">go-PST2.[3SG]</ta>
            <ta e="T432" id="Seg_5512" s="T431">then</ta>
            <ta e="T433" id="Seg_5513" s="T432">Chagydaj-ACC</ta>
            <ta e="T434" id="Seg_5514" s="T433">though</ta>
            <ta e="T435" id="Seg_5515" s="T434">in.summer</ta>
            <ta e="T436" id="Seg_5516" s="T435">together</ta>
            <ta e="T437" id="Seg_5517" s="T436">live-PRS-3PL</ta>
            <ta e="T438" id="Seg_5518" s="T437">it.is.said</ta>
            <ta e="T439" id="Seg_5519" s="T438">eat-CVB.SEQ</ta>
            <ta e="T440" id="Seg_5520" s="T439">live-CVB.SEQ-3PL</ta>
            <ta e="T441" id="Seg_5521" s="T440">well</ta>
            <ta e="T442" id="Seg_5522" s="T441">letter.[NOM]</ta>
            <ta e="T443" id="Seg_5523" s="T442">come-PST2.[3SG]</ta>
            <ta e="T444" id="Seg_5524" s="T443">this</ta>
            <ta e="T445" id="Seg_5525" s="T444">letter-ACC</ta>
            <ta e="T446" id="Seg_5526" s="T445">though</ta>
            <ta e="T447" id="Seg_5527" s="T446">Khatanga-DAT/LOC</ta>
            <ta e="T448" id="Seg_5528" s="T447">bring-PTCP.FUT-3PL-ACC</ta>
            <ta e="T449" id="Seg_5529" s="T448">need.to</ta>
            <ta e="T450" id="Seg_5530" s="T449">where.from</ta>
            <ta e="T451" id="Seg_5531" s="T450">INDEF</ta>
            <ta e="T452" id="Seg_5532" s="T451">come-PST2.[3SG]</ta>
            <ta e="T453" id="Seg_5533" s="T452">small.boat-INSTR</ta>
            <ta e="T454" id="Seg_5534" s="T453">then</ta>
            <ta e="T455" id="Seg_5535" s="T454">what.[NOM]</ta>
            <ta e="T456" id="Seg_5536" s="T455">boat-3SG.[NOM]</ta>
            <ta e="T457" id="Seg_5537" s="T456">come-FUT.[3SG]=Q</ta>
            <ta e="T458" id="Seg_5538" s="T457">well</ta>
            <ta e="T459" id="Seg_5539" s="T458">Chagydaj</ta>
            <ta e="T460" id="Seg_5540" s="T459">this</ta>
            <ta e="T461" id="Seg_5541" s="T460">letter-ACC</ta>
            <ta e="T462" id="Seg_5542" s="T461">MOD</ta>
            <ta e="T463" id="Seg_5543" s="T462">who-DAT/LOC</ta>
            <ta e="T464" id="Seg_5544" s="T463">bring.[IMP.2SG]</ta>
            <ta e="T465" id="Seg_5545" s="T464">Kresty-DAT/LOC</ta>
            <ta e="T466" id="Seg_5546" s="T465">Novorybnoe</ta>
            <ta e="T467" id="Seg_5547" s="T466">Kresty-3SG-DAT/LOC</ta>
            <ta e="T468" id="Seg_5548" s="T467">see.[IMP.2SG]</ta>
            <ta e="T469" id="Seg_5549" s="T468">distant-ADVZ</ta>
            <ta e="T470" id="Seg_5550" s="T469">Novorybnoe.[NOM]</ta>
            <ta e="T471" id="Seg_5551" s="T470">remote</ta>
            <ta e="T472" id="Seg_5552" s="T471">side-3SG-DAT/LOC</ta>
            <ta e="T478" id="Seg_5553" s="T476">no</ta>
            <ta e="T479" id="Seg_5554" s="T478">that</ta>
            <ta e="T480" id="Seg_5555" s="T479">self-PL-3SG-GEN</ta>
            <ta e="T482" id="Seg_5556" s="T480">own-3PL.[NOM]</ta>
            <ta e="T485" id="Seg_5557" s="T482">AFFIRM</ta>
            <ta e="T486" id="Seg_5558" s="T485">Begichev-3PL.[NOM]</ta>
            <ta e="T487" id="Seg_5559" s="T486">EMPH-then</ta>
            <ta e="T488" id="Seg_5560" s="T487">travel-PTCP.PST</ta>
            <ta e="T489" id="Seg_5561" s="T488">be-PST1-3SG</ta>
            <ta e="T490" id="Seg_5562" s="T489">that</ta>
            <ta e="T491" id="Seg_5563" s="T490">go-PTCP.PST-3SG-INSTR</ta>
            <ta e="T492" id="Seg_5564" s="T491">go-PST2.[3SG]</ta>
            <ta e="T493" id="Seg_5565" s="T492">this</ta>
            <ta e="T494" id="Seg_5566" s="T493">to</ta>
            <ta e="T495" id="Seg_5567" s="T494">up</ta>
            <ta e="T496" id="Seg_5568" s="T495">to</ta>
            <ta e="T497" id="Seg_5569" s="T496">well</ta>
            <ta e="T498" id="Seg_5570" s="T497">that</ta>
            <ta e="T499" id="Seg_5571" s="T498">EMPH</ta>
            <ta e="T500" id="Seg_5572" s="T499">that</ta>
            <ta e="T501" id="Seg_5573" s="T500">letter-ACC</ta>
            <ta e="T502" id="Seg_5574" s="T501">bring-EP-CAUS-PRS-3PL</ta>
            <ta e="T503" id="Seg_5575" s="T502">then</ta>
            <ta e="T504" id="Seg_5576" s="T503">letter-PROPR-3PL</ta>
            <ta e="T505" id="Seg_5577" s="T504">be-PST2-3SG</ta>
            <ta e="T506" id="Seg_5578" s="T505">MOD</ta>
            <ta e="T507" id="Seg_5579" s="T506">lord-PROPR-3PL</ta>
            <ta e="T508" id="Seg_5580" s="T507">EMPH</ta>
            <ta e="T512" id="Seg_5581" s="T511">Dolgan-PL.[NOM]</ta>
            <ta e="T513" id="Seg_5582" s="T512">that</ta>
            <ta e="T514" id="Seg_5583" s="T513">worker-3PL.[NOM]</ta>
            <ta e="T515" id="Seg_5584" s="T514">Soviet-3PL.[NOM]</ta>
            <ta e="T516" id="Seg_5585" s="T515">what-3PL.[NOM]</ta>
            <ta e="T517" id="Seg_5586" s="T516">letter.[NOM]</ta>
            <ta e="T518" id="Seg_5587" s="T517">send-PRS-3PL</ta>
            <ta e="T519" id="Seg_5588" s="T518">EMPH</ta>
            <ta e="T520" id="Seg_5589" s="T519">so</ta>
            <ta e="T521" id="Seg_5590" s="T520">make</ta>
            <ta e="T522" id="Seg_5591" s="T521">this</ta>
            <ta e="T523" id="Seg_5592" s="T522">old.man.[NOM]</ta>
            <ta e="T524" id="Seg_5593" s="T523">MOD</ta>
            <ta e="T525" id="Seg_5594" s="T524">well</ta>
            <ta e="T526" id="Seg_5595" s="T525">in.the.evening</ta>
            <ta e="T527" id="Seg_5596" s="T526">hit.the.road-PRS.[3SG]</ta>
            <ta e="T528" id="Seg_5597" s="T527">in.the.morning</ta>
            <ta e="T529" id="Seg_5598" s="T528">early-AG</ta>
            <ta e="T530" id="Seg_5599" s="T529">woman.[NOM]</ta>
            <ta e="T531" id="Seg_5600" s="T530">stand.up-PTCP.PRS</ta>
            <ta e="T532" id="Seg_5601" s="T531">time-3SG-DAT/LOC</ta>
            <ta e="T533" id="Seg_5602" s="T532">come-FUT-1SG</ta>
            <ta e="T534" id="Seg_5603" s="T533">say-PST2.[3SG]</ta>
            <ta e="T535" id="Seg_5604" s="T534">this</ta>
            <ta e="T536" id="Seg_5605" s="T535">old.man.[NOM]</ta>
            <ta e="T537" id="Seg_5606" s="T536">oh</ta>
            <ta e="T538" id="Seg_5607" s="T537">how</ta>
            <ta e="T539" id="Seg_5608" s="T538">make-CVB.SEQ</ta>
            <ta e="T540" id="Seg_5609" s="T539">come-CVB.PURP</ta>
            <ta e="T541" id="Seg_5610" s="T540">make-PRS-2SG</ta>
            <ta e="T542" id="Seg_5611" s="T541">Novorybnoe.[NOM]</ta>
            <ta e="T543" id="Seg_5612" s="T542">remote</ta>
            <ta e="T544" id="Seg_5613" s="T543">side-3SG-ABL</ta>
            <ta e="T545" id="Seg_5614" s="T544">very</ta>
            <ta e="T546" id="Seg_5615" s="T545">distant.[NOM]</ta>
            <ta e="T547" id="Seg_5616" s="T546">Popigaj-ABL</ta>
            <ta e="T548" id="Seg_5617" s="T547">well</ta>
            <ta e="T549" id="Seg_5618" s="T548">this</ta>
            <ta e="T550" id="Seg_5619" s="T549">old.man.[NOM]</ta>
            <ta e="T551" id="Seg_5620" s="T550">walk-CVB.SEQ</ta>
            <ta e="T552" id="Seg_5621" s="T551">stay-PST2.[3SG]</ta>
            <ta e="T553" id="Seg_5622" s="T552">eh</ta>
            <ta e="T554" id="Seg_5623" s="T553">go-CVB.SEQ</ta>
            <ta e="T555" id="Seg_5624" s="T554">stay-PST2.[3SG]</ta>
            <ta e="T556" id="Seg_5625" s="T555">small.boat-INSTR</ta>
            <ta e="T557" id="Seg_5626" s="T556">in.the.morning</ta>
            <ta e="T558" id="Seg_5627" s="T557">old.woman-PL.[NOM]</ta>
            <ta e="T561" id="Seg_5628" s="T560">early-AG</ta>
            <ta e="T562" id="Seg_5629" s="T561">woman-PL.[NOM]</ta>
            <ta e="T563" id="Seg_5630" s="T562">stand.up-PRS-3PL</ta>
            <ta e="T564" id="Seg_5631" s="T563">for.example</ta>
            <ta e="T565" id="Seg_5632" s="T564">then</ta>
            <ta e="T566" id="Seg_5633" s="T565">seven</ta>
            <ta e="T567" id="Seg_5634" s="T566">hour.[NOM]</ta>
            <ta e="T568" id="Seg_5635" s="T567">EMPH</ta>
            <ta e="T569" id="Seg_5636" s="T568">be-PST2.[3SG]</ta>
            <ta e="T570" id="Seg_5637" s="T569">apparently</ta>
            <ta e="T571" id="Seg_5638" s="T570">what.[NOM]</ta>
            <ta e="T572" id="Seg_5639" s="T571">hour-3SG.[NOM]</ta>
            <ta e="T573" id="Seg_5640" s="T572">come-FUT.[3SG]=Q</ta>
            <ta e="T574" id="Seg_5641" s="T573">sun-3PL-INSTR</ta>
            <ta e="T575" id="Seg_5642" s="T574">see-PRS-3PL</ta>
            <ta e="T576" id="Seg_5643" s="T575">EMPH</ta>
            <ta e="T577" id="Seg_5644" s="T576">oh.dear</ta>
            <ta e="T578" id="Seg_5645" s="T577">seven</ta>
            <ta e="T579" id="Seg_5646" s="T578">hour-DAT/LOC</ta>
            <ta e="T580" id="Seg_5647" s="T579">Chagydaj.[NOM]</ta>
            <ta e="T581" id="Seg_5648" s="T580">come-PST2.[3SG]</ta>
            <ta e="T582" id="Seg_5649" s="T581">small.boat-3SG.[NOM]</ta>
            <ta e="T583" id="Seg_5650" s="T582">dive-EP-REFL-CVB.SIM</ta>
            <ta e="T584" id="Seg_5651" s="T583">lie-PRS.[3SG]</ta>
            <ta e="T585" id="Seg_5652" s="T584">EMPH</ta>
            <ta e="T586" id="Seg_5653" s="T585">see.[IMP.2SG]</ta>
            <ta e="T587" id="Seg_5654" s="T586">such</ta>
            <ta e="T588" id="Seg_5655" s="T587">bold</ta>
            <ta e="T589" id="Seg_5656" s="T588">human.being-PL.[NOM]</ta>
            <ta e="T590" id="Seg_5657" s="T589">there.is-3PL</ta>
            <ta e="T591" id="Seg_5658" s="T590">then</ta>
            <ta e="T592" id="Seg_5659" s="T591">woman.[NOM]</ta>
            <ta e="T593" id="Seg_5660" s="T592">give.birth-PRS.[3SG]</ta>
            <ta e="T594" id="Seg_5661" s="T593">that.EMPH.[NOM]</ta>
            <ta e="T595" id="Seg_5662" s="T594">temporary.settlement-3PL-DAT/LOC</ta>
            <ta e="T596" id="Seg_5663" s="T595">this</ta>
            <ta e="T597" id="Seg_5664" s="T596">woman.[NOM]</ta>
            <ta e="T598" id="Seg_5665" s="T597">die-CVB.PURP</ta>
            <ta e="T599" id="Seg_5666" s="T598">make-PST2.[3SG]</ta>
            <ta e="T600" id="Seg_5667" s="T599">how</ta>
            <ta e="T601" id="Seg_5668" s="T600">NEG</ta>
            <ta e="T602" id="Seg_5669" s="T601">make-PTCP.FUT-3PL-ACC</ta>
            <ta e="T603" id="Seg_5670" s="T602">very</ta>
            <ta e="T604" id="Seg_5671" s="T603">doctor-POSS</ta>
            <ta e="T605" id="Seg_5672" s="T604">NEG</ta>
            <ta e="T606" id="Seg_5673" s="T605">time-DAT/LOC</ta>
            <ta e="T607" id="Seg_5674" s="T606">well</ta>
            <ta e="T608" id="Seg_5675" s="T607">midwife-3PL.[NOM]</ta>
            <ta e="T609" id="Seg_5676" s="T608">misery-PROPR</ta>
            <ta e="T610" id="Seg_5677" s="T609">get.tired-CVB.SEQ</ta>
            <ta e="T611" id="Seg_5678" s="T610">stop-PST2.[3SG]</ta>
            <ta e="T612" id="Seg_5679" s="T611">well</ta>
            <ta e="T613" id="Seg_5680" s="T612">Chagydaj.[NOM]</ta>
            <ta e="T614" id="Seg_5681" s="T613">come-IMP.3SG</ta>
            <ta e="T615" id="Seg_5682" s="T614">woman.[NOM]</ta>
            <ta e="T616" id="Seg_5683" s="T615">die-CVB.PURP</ta>
            <ta e="T617" id="Seg_5684" s="T616">make-PST1-3SG</ta>
            <ta e="T618" id="Seg_5685" s="T617">how</ta>
            <ta e="T619" id="Seg_5686" s="T618">INDEF</ta>
            <ta e="T620" id="Seg_5687" s="T619">make-IMP.3SG</ta>
            <ta e="T621" id="Seg_5688" s="T620">bone.[NOM]</ta>
            <ta e="T622" id="Seg_5689" s="T621">separate-IMP.3SG</ta>
            <ta e="T623" id="Seg_5690" s="T622">here</ta>
            <ta e="T624" id="Seg_5691" s="T623">EMPH-so</ta>
            <ta e="T625" id="Seg_5692" s="T624">this</ta>
            <ta e="T626" id="Seg_5693" s="T625">Chagydaj.[NOM]</ta>
            <ta e="T627" id="Seg_5694" s="T626">come-PRS.[3SG]</ta>
            <ta e="T628" id="Seg_5695" s="T627">how</ta>
            <ta e="T631" id="Seg_5696" s="T630">this</ta>
            <ta e="T632" id="Seg_5697" s="T631">woman-ACC</ta>
            <ta e="T633" id="Seg_5698" s="T632">hand-VBZ-CVB.SEQ</ta>
            <ta e="T634" id="Seg_5699" s="T633">take-PRS.[3SG]</ta>
            <ta e="T635" id="Seg_5700" s="T634">child-3SG-ACC</ta>
            <ta e="T636" id="Seg_5701" s="T635">EMPH-then</ta>
            <ta e="T637" id="Seg_5702" s="T636">alive</ta>
            <ta e="T638" id="Seg_5703" s="T637">let-PRS.[3SG]</ta>
            <ta e="T639" id="Seg_5704" s="T638">see.[IMP.2SG]</ta>
            <ta e="T640" id="Seg_5705" s="T639">such-PL.[NOM]</ta>
            <ta e="T641" id="Seg_5706" s="T640">there.is</ta>
            <ta e="T642" id="Seg_5707" s="T641">be-PST1-3PL</ta>
            <ta e="T643" id="Seg_5708" s="T642">old-PL.[NOM]</ta>
            <ta e="T644" id="Seg_5709" s="T643">and</ta>
            <ta e="T645" id="Seg_5710" s="T644">Chakyyla.[NOM]</ta>
            <ta e="T646" id="Seg_5711" s="T645">old.man.[NOM]</ta>
            <ta e="T647" id="Seg_5712" s="T646">grandfather-3SG.[NOM]</ta>
            <ta e="T648" id="Seg_5713" s="T647">that-3SG-2SG.[NOM]</ta>
            <ta e="T652" id="Seg_5714" s="T651">AFFIRM</ta>
            <ta e="T653" id="Seg_5715" s="T652">then</ta>
            <ta e="T654" id="Seg_5716" s="T653">that</ta>
            <ta e="T655" id="Seg_5717" s="T654">Chagydaj-EP-2SG.[NOM]</ta>
            <ta e="T656" id="Seg_5718" s="T655">woman-3SG.[NOM]</ta>
            <ta e="T657" id="Seg_5719" s="T656">though</ta>
            <ta e="T658" id="Seg_5720" s="T657">old.woman.[NOM]</ta>
            <ta e="T659" id="Seg_5721" s="T658">Anna.[NOM]</ta>
            <ta e="T660" id="Seg_5722" s="T659">mother-3SG.[NOM]</ta>
            <ta e="T661" id="Seg_5723" s="T660">old.woman.[NOM]</ta>
            <ta e="T662" id="Seg_5724" s="T661">this</ta>
            <ta e="T663" id="Seg_5725" s="T662">Cheeke</ta>
            <ta e="T664" id="Seg_5726" s="T663">old.woman.[NOM]</ta>
            <ta e="T665" id="Seg_5727" s="T664">that-3SG-2SG.[NOM]</ta>
            <ta e="T666" id="Seg_5728" s="T665">father-3SG.[NOM]</ta>
            <ta e="T667" id="Seg_5729" s="T666">1PL.[NOM]</ta>
            <ta e="T668" id="Seg_5730" s="T667">grandfather-1PL-ACC</ta>
            <ta e="T669" id="Seg_5731" s="T668">with</ta>
            <ta e="T670" id="Seg_5732" s="T669">together</ta>
            <ta e="T671" id="Seg_5733" s="T670">be.born-PST2-3PL</ta>
            <ta e="T672" id="Seg_5734" s="T671">Kudryakov</ta>
            <ta e="T673" id="Seg_5735" s="T672">old.man.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg-KiPP">
            <ta e="T1" id="Seg_5736" s="T0">Begitschew-ACC</ta>
            <ta e="T2" id="Seg_5737" s="T1">mitnehmen-PRS-3PL</ta>
            <ta e="T5" id="Seg_5738" s="T4">Erde-EP-INSTR</ta>
            <ta e="T6" id="Seg_5739" s="T5">gehen-PTCP.PRS</ta>
            <ta e="T7" id="Seg_5740" s="T6">sein-PST1-3SG</ta>
            <ta e="T8" id="Seg_5741" s="T7">früher</ta>
            <ta e="T9" id="Seg_5742" s="T8">Expedition-PL.[NOM]</ta>
            <ta e="T10" id="Seg_5743" s="T9">jenes-ACC</ta>
            <ta e="T11" id="Seg_5744" s="T10">Großvater-1SG.[NOM]</ta>
            <ta e="T12" id="Seg_5745" s="T11">erzählen-HAB.[3SG]</ta>
            <ta e="T13" id="Seg_5746" s="T12">mein</ta>
            <ta e="T14" id="Seg_5747" s="T13">Mutter-1SG.[NOM]</ta>
            <ta e="T15" id="Seg_5748" s="T14">Großvater.[NOM]</ta>
            <ta e="T16" id="Seg_5749" s="T15">dieses</ta>
            <ta e="T17" id="Seg_5750" s="T16">Mutter-1SG.[NOM]</ta>
            <ta e="T18" id="Seg_5751" s="T17">Verwandter-PL-3SG.[NOM]</ta>
            <ta e="T19" id="Seg_5752" s="T18">Markel</ta>
            <ta e="T20" id="Seg_5753" s="T19">alter.Mann.[NOM]</ta>
            <ta e="T21" id="Seg_5754" s="T20">Elena.[NOM]</ta>
            <ta e="T22" id="Seg_5755" s="T21">Vater-3SG.[NOM]</ta>
            <ta e="T23" id="Seg_5756" s="T22">alter.Mann.[NOM]</ta>
            <ta e="T24" id="Seg_5757" s="T23">jenes-PL.[NOM]</ta>
            <ta e="T25" id="Seg_5758" s="T24">sich.unterhalten-PTCP.PRS-3PL-ACC</ta>
            <ta e="T26" id="Seg_5759" s="T25">hören-HAB-1SG</ta>
            <ta e="T27" id="Seg_5760" s="T26">dieses</ta>
            <ta e="T28" id="Seg_5761" s="T27">Begitschew.[NOM]</ta>
            <ta e="T29" id="Seg_5762" s="T28">aber</ta>
            <ta e="T30" id="Seg_5763" s="T29">Rentier-INSTR</ta>
            <ta e="T31" id="Seg_5764" s="T30">reisen-PRS.[3SG]</ta>
            <ta e="T32" id="Seg_5765" s="T31">Begitschew.[NOM]</ta>
            <ta e="T33" id="Seg_5766" s="T32">Insel-3SG-DAT/LOC</ta>
            <ta e="T34" id="Seg_5767" s="T33">sieben</ta>
            <ta e="T35" id="Seg_5768" s="T34">Mensch-PROPR</ta>
            <ta e="T36" id="Seg_5769" s="T35">dieses</ta>
            <ta e="T37" id="Seg_5770" s="T36">Mensch.[NOM]</ta>
            <ta e="T38" id="Seg_5771" s="T37">reisen-CVB.SEQ</ta>
            <ta e="T39" id="Seg_5772" s="T38">gehen-CVB.SEQ</ta>
            <ta e="T40" id="Seg_5773" s="T39">Rentier-INSTR</ta>
            <ta e="T41" id="Seg_5774" s="T40">mitnehmen-PRS-3PL</ta>
            <ta e="T42" id="Seg_5775" s="T41">EMPH</ta>
            <ta e="T43" id="Seg_5776" s="T42">anders-ABL-anders</ta>
            <ta e="T44" id="Seg_5777" s="T43">Rentier-INSTR</ta>
            <ta e="T45" id="Seg_5778" s="T44">mitnehmen-PRS-3PL</ta>
            <ta e="T46" id="Seg_5779" s="T45">neu-ABL-neu</ta>
            <ta e="T47" id="Seg_5780" s="T46">Rentier-INSTR</ta>
            <ta e="T48" id="Seg_5781" s="T47">mitnehmen-PRS-3PL</ta>
            <ta e="T49" id="Seg_5782" s="T48">dort</ta>
            <ta e="T50" id="Seg_5783" s="T49">Tschagydaj</ta>
            <ta e="T51" id="Seg_5784" s="T50">alter.Mann-ACC</ta>
            <ta e="T52" id="Seg_5785" s="T51">bringen-EP-CAUS-PRS-3PL</ta>
            <ta e="T53" id="Seg_5786" s="T52">MOD</ta>
            <ta e="T54" id="Seg_5787" s="T53">1SG.[NOM]</ta>
            <ta e="T55" id="Seg_5788" s="T54">alter.Mann-EP-1SG.[NOM]</ta>
            <ta e="T56" id="Seg_5789" s="T55">Großvater-3SG-ACC-PROPR</ta>
            <ta e="T57" id="Seg_5790" s="T56">Großvater-3PL.[NOM]</ta>
            <ta e="T58" id="Seg_5791" s="T57">jenes</ta>
            <ta e="T59" id="Seg_5792" s="T58">alter.Mann.[NOM]</ta>
            <ta e="T60" id="Seg_5793" s="T59">was.[NOM]</ta>
            <ta e="T61" id="Seg_5794" s="T60">NEG</ta>
            <ta e="T62" id="Seg_5795" s="T61">verfolgen-NEG.PTCP.PST</ta>
            <ta e="T63" id="Seg_5796" s="T62">Mensch-3SG.[NOM]</ta>
            <ta e="T64" id="Seg_5797" s="T63">sein-PST1-3SG</ta>
            <ta e="T65" id="Seg_5798" s="T64">sehr</ta>
            <ta e="T66" id="Seg_5799" s="T65">energisch</ta>
            <ta e="T67" id="Seg_5800" s="T66">Mensch.[NOM]</ta>
            <ta e="T68" id="Seg_5801" s="T67">jenes</ta>
            <ta e="T69" id="Seg_5802" s="T68">alter.Mann</ta>
            <ta e="T70" id="Seg_5803" s="T69">können-NEG.PTCP</ta>
            <ta e="T71" id="Seg_5804" s="T70">Rentier-PL-ACC</ta>
            <ta e="T72" id="Seg_5805" s="T71">einspannen-CAUS-CVB.SEQ</ta>
            <ta e="T73" id="Seg_5806" s="T72">nachdem</ta>
            <ta e="T74" id="Seg_5807" s="T73">Begitschew.[NOM]</ta>
            <ta e="T75" id="Seg_5808" s="T74">Insel-3SG-DAT/LOC</ta>
            <ta e="T76" id="Seg_5809" s="T75">bringen-EP-CAUS-PRS-3PL</ta>
            <ta e="T77" id="Seg_5810" s="T76">EMPH</ta>
            <ta e="T78" id="Seg_5811" s="T77">dort</ta>
            <ta e="T79" id="Seg_5812" s="T78">Rentier-PL.[NOM]</ta>
            <ta e="T80" id="Seg_5813" s="T79">sich.bewegen-PRS-3PL</ta>
            <ta e="T81" id="Seg_5814" s="T80">man.sagt</ta>
            <ta e="T82" id="Seg_5815" s="T81">EMPH</ta>
            <ta e="T83" id="Seg_5816" s="T82">EMPH</ta>
            <ta e="T84" id="Seg_5817" s="T83">Rentier-PL.[NOM]</ta>
            <ta e="T85" id="Seg_5818" s="T84">sich.bewegen-PRS-3PL</ta>
            <ta e="T86" id="Seg_5819" s="T85">dieses</ta>
            <ta e="T87" id="Seg_5820" s="T86">Mensch.[NOM]</ta>
            <ta e="T88" id="Seg_5821" s="T87">Mütze-3SG-ACC</ta>
            <ta e="T89" id="Seg_5822" s="T88">jenes</ta>
            <ta e="T90" id="Seg_5823" s="T89">Begitschew.[NOM]</ta>
            <ta e="T91" id="Seg_5824" s="T90">eigen-3SG-GEN</ta>
            <ta e="T92" id="Seg_5825" s="T91">Mütze-3SG-ACC</ta>
            <ta e="T93" id="Seg_5826" s="T92">Rentier.[NOM]</ta>
            <ta e="T94" id="Seg_5827" s="T93">umhängen-CVB.SEQ</ta>
            <ta e="T95" id="Seg_5828" s="T94">werfen-PRS.[3SG]</ta>
            <ta e="T96" id="Seg_5829" s="T95">man.sagt</ta>
            <ta e="T97" id="Seg_5830" s="T96">dieses</ta>
            <ta e="T98" id="Seg_5831" s="T97">Schlitten-ACC</ta>
            <ta e="T99" id="Seg_5832" s="T98">durch</ta>
            <ta e="T100" id="Seg_5833" s="T99">Mütze.[NOM]</ta>
            <ta e="T101" id="Seg_5834" s="T100">fallen-CVB.SEQ</ta>
            <ta e="T102" id="Seg_5835" s="T101">sein-TEMP-3SG</ta>
            <ta e="T103" id="Seg_5836" s="T102">jenes-ACC</ta>
            <ta e="T104" id="Seg_5837" s="T103">alter.Mann.[NOM]</ta>
            <ta e="T105" id="Seg_5838" s="T104">Schlitten-ACC</ta>
            <ta e="T106" id="Seg_5839" s="T105">durch</ta>
            <ta e="T107" id="Seg_5840" s="T106">springen-CVB.SEQ</ta>
            <ta e="T108" id="Seg_5841" s="T107">fangen-CVB.SEQ</ta>
            <ta e="T109" id="Seg_5842" s="T108">nehmen-PST2.[3SG]</ta>
            <ta e="T110" id="Seg_5843" s="T109">EMPH</ta>
            <ta e="T111" id="Seg_5844" s="T110">Erde-DAT/LOC</ta>
            <ta e="T112" id="Seg_5845" s="T111">fallen-PTCP.FUT-3SG-GEN</ta>
            <ta e="T113" id="Seg_5846" s="T112">jene.Seite.[NOM]</ta>
            <ta e="T114" id="Seg_5847" s="T113">Seite-3SG-DAT/LOC</ta>
            <ta e="T115" id="Seg_5848" s="T114">jenes-ACC</ta>
            <ta e="T116" id="Seg_5849" s="T115">jenes</ta>
            <ta e="T119" id="Seg_5850" s="T118">Begitschew-3SG.[NOM]</ta>
            <ta e="T120" id="Seg_5851" s="T119">sagen-PRS.[3SG]</ta>
            <ta e="T121" id="Seg_5852" s="T120">man.sagt</ta>
            <ta e="T122" id="Seg_5853" s="T121">oh</ta>
            <ta e="T123" id="Seg_5854" s="T122">dolganisch</ta>
            <ta e="T124" id="Seg_5855" s="T123">Erde-3SG-DAT/LOC</ta>
            <ta e="T125" id="Seg_5856" s="T124">und</ta>
            <ta e="T126" id="Seg_5857" s="T125">energisch</ta>
            <ta e="T127" id="Seg_5858" s="T126">Mensch.[NOM]</ta>
            <ta e="T128" id="Seg_5859" s="T127">es.gibt</ta>
            <ta e="T129" id="Seg_5860" s="T128">sein-PRS.[3SG]</ta>
            <ta e="T131" id="Seg_5861" s="T130">doch</ta>
            <ta e="T132" id="Seg_5862" s="T131">dieses</ta>
            <ta e="T133" id="Seg_5863" s="T132">alter.Mann.[NOM]</ta>
            <ta e="T134" id="Seg_5864" s="T133">Begitschew.[NOM]</ta>
            <ta e="T135" id="Seg_5865" s="T134">aber</ta>
            <ta e="T136" id="Seg_5866" s="T135">jenes</ta>
            <ta e="T137" id="Seg_5867" s="T136">jenes</ta>
            <ta e="T138" id="Seg_5868" s="T137">Insel-DAT/LOC</ta>
            <ta e="T139" id="Seg_5869" s="T138">ankommen-PST2-EP-1SG</ta>
            <ta e="T140" id="Seg_5870" s="T139">sagen-CVB.SEQ</ta>
            <ta e="T141" id="Seg_5871" s="T140">erzählen-PRS.[3SG]</ta>
            <ta e="T142" id="Seg_5872" s="T141">man.sagt</ta>
            <ta e="T143" id="Seg_5873" s="T142">dieses</ta>
            <ta e="T144" id="Seg_5874" s="T143">was</ta>
            <ta e="T145" id="Seg_5875" s="T144">Wunder-ACC</ta>
            <ta e="T146" id="Seg_5876" s="T145">sehen-PST1-2SG</ta>
            <ta e="T147" id="Seg_5877" s="T146">dort</ta>
            <ta e="T148" id="Seg_5878" s="T147">sagen-CVB.SEQ</ta>
            <ta e="T149" id="Seg_5879" s="T148">fragen-PRS-3PL</ta>
            <ta e="T150" id="Seg_5880" s="T149">Dolgane-PL.[NOM]</ta>
            <ta e="T151" id="Seg_5881" s="T150">EMPH</ta>
            <ta e="T152" id="Seg_5882" s="T151">EMPH</ta>
            <ta e="T153" id="Seg_5883" s="T152">jenes-DAT/LOC</ta>
            <ta e="T154" id="Seg_5884" s="T153">1SG.[NOM]</ta>
            <ta e="T155" id="Seg_5885" s="T154">Wunder-ACC</ta>
            <ta e="T156" id="Seg_5886" s="T155">was-ACC</ta>
            <ta e="T157" id="Seg_5887" s="T156">NEG</ta>
            <ta e="T158" id="Seg_5888" s="T157">sehr</ta>
            <ta e="T159" id="Seg_5889" s="T158">finden-NEG-PST1-1SG</ta>
            <ta e="T160" id="Seg_5890" s="T159">Walross-3PL.[NOM]</ta>
            <ta e="T161" id="Seg_5891" s="T160">aber</ta>
            <ta e="T162" id="Seg_5892" s="T161">Zahn-3PL-ABL</ta>
            <ta e="T163" id="Seg_5893" s="T162">binden-CVB.SEQ</ta>
            <ta e="T164" id="Seg_5894" s="T163">nachdem</ta>
            <ta e="T165" id="Seg_5895" s="T164">schlafen-PRS-3PL</ta>
            <ta e="T166" id="Seg_5896" s="T165">schlafen-CVB.SIM</ta>
            <ta e="T167" id="Seg_5897" s="T166">liegen-PTCP.PRS-3PL-ACC</ta>
            <ta e="T168" id="Seg_5898" s="T167">binden-CVB.SEQ</ta>
            <ta e="T169" id="Seg_5899" s="T168">Holzklotz-ABL</ta>
            <ta e="T170" id="Seg_5900" s="T169">jenes-ACC</ta>
            <ta e="T171" id="Seg_5901" s="T170">Sache.[NOM]</ta>
            <ta e="T172" id="Seg_5902" s="T171">NEG</ta>
            <ta e="T173" id="Seg_5903" s="T172">denken-NEG-3PL</ta>
            <ta e="T174" id="Seg_5904" s="T173">ziehen-CVB.SEQ</ta>
            <ta e="T175" id="Seg_5905" s="T174">hineingehen-CAUS-PRS-3PL</ta>
            <ta e="T176" id="Seg_5906" s="T175">Wasser-DAT/LOC</ta>
            <ta e="T177" id="Seg_5907" s="T176">hineingehen-CAUS-CVB.SEQ</ta>
            <ta e="T178" id="Seg_5908" s="T177">werfen-PRS-3PL</ta>
            <ta e="T179" id="Seg_5909" s="T178">dann</ta>
            <ta e="T180" id="Seg_5910" s="T179">nur</ta>
            <ta e="T181" id="Seg_5911" s="T180">Insel-DAT/LOC</ta>
            <ta e="T182" id="Seg_5912" s="T181">Stein.[NOM]</ta>
            <ta e="T183" id="Seg_5913" s="T182">Berg-DAT/LOC</ta>
            <ta e="T184" id="Seg_5914" s="T183">finden-PST1-1SG</ta>
            <ta e="T185" id="Seg_5915" s="T184">sagen-PRS.[3SG]</ta>
            <ta e="T186" id="Seg_5916" s="T185">man.sagt</ta>
            <ta e="T187" id="Seg_5917" s="T186">eins</ta>
            <ta e="T188" id="Seg_5918" s="T187">Wunder-ACC</ta>
            <ta e="T189" id="Seg_5919" s="T188">doch</ta>
            <ta e="T190" id="Seg_5920" s="T189">was-ACC</ta>
            <ta e="T191" id="Seg_5921" s="T190">finden-PST1-2SG</ta>
            <ta e="T192" id="Seg_5922" s="T191">jenes-ACC</ta>
            <ta e="T193" id="Seg_5923" s="T192">sagen-PRS.[3SG]</ta>
            <ta e="T194" id="Seg_5924" s="T193">EMPH</ta>
            <ta e="T210" id="Seg_5925" s="T209">jenes-DAT/LOC</ta>
            <ta e="T211" id="Seg_5926" s="T210">hineingehen-CVB.SEQ</ta>
            <ta e="T212" id="Seg_5927" s="T211">gehen-PST1-1SG</ta>
            <ta e="T213" id="Seg_5928" s="T212">sagen-PRS.[3SG]</ta>
            <ta e="T214" id="Seg_5929" s="T213">drei</ta>
            <ta e="T215" id="Seg_5930" s="T214">Mensch-PROPR.[NOM]</ta>
            <ta e="T216" id="Seg_5931" s="T215">dieses</ta>
            <ta e="T217" id="Seg_5932" s="T216">Mensch-PL.[NOM]</ta>
            <ta e="T218" id="Seg_5933" s="T217">Nahrung-POSS</ta>
            <ta e="T219" id="Seg_5934" s="T218">NEG</ta>
            <ta e="T220" id="Seg_5935" s="T219">werden-PST2-1PL</ta>
            <ta e="T221" id="Seg_5936" s="T220">dann</ta>
            <ta e="T222" id="Seg_5937" s="T221">aber</ta>
            <ta e="T223" id="Seg_5938" s="T222">Bucht-ABL</ta>
            <ta e="T224" id="Seg_5939" s="T223">oh</ta>
            <ta e="T225" id="Seg_5940" s="T224">klein</ta>
            <ta e="T226" id="Seg_5941" s="T225">Fisch-DIM-ACC</ta>
            <ta e="T227" id="Seg_5942" s="T226">fangen-EP-MED-CVB.SEQ-3PL</ta>
            <ta e="T228" id="Seg_5943" s="T227">jenes</ta>
            <ta e="T229" id="Seg_5944" s="T228">Tür-DAT/LOC</ta>
            <ta e="T230" id="Seg_5945" s="T229">hineingehen-CVB.SEQ-3PL</ta>
            <ta e="T231" id="Seg_5946" s="T230">essen-CVB.PURP-3PL</ta>
            <ta e="T232" id="Seg_5947" s="T231">hineingehen-PST2-3PL</ta>
            <ta e="T233" id="Seg_5948" s="T232">dieses</ta>
            <ta e="T234" id="Seg_5949" s="T233">Mensch.[NOM]</ta>
            <ta e="T235" id="Seg_5950" s="T234">nur</ta>
            <ta e="T236" id="Seg_5951" s="T235">hineingehen-PST2-3SG</ta>
            <ta e="T237" id="Seg_5952" s="T236">dieses</ta>
            <ta e="T238" id="Seg_5953" s="T237">Mensch.[NOM]</ta>
            <ta e="T239" id="Seg_5954" s="T238">sitzen-PRS.[3SG]</ta>
            <ta e="T240" id="Seg_5955" s="T239">sitzen-TEMP-3SG</ta>
            <ta e="T241" id="Seg_5956" s="T240">was.[NOM]</ta>
            <ta e="T242" id="Seg_5957" s="T241">INDEF</ta>
            <ta e="T243" id="Seg_5958" s="T242">Mensch-3SG.[NOM]</ta>
            <ta e="T244" id="Seg_5959" s="T243">kommen-PST2.[3SG]</ta>
            <ta e="T245" id="Seg_5960" s="T244">EMPH</ta>
            <ta e="T246" id="Seg_5961" s="T245">EMPH</ta>
            <ta e="T247" id="Seg_5962" s="T246">Mensch.[NOM]</ta>
            <ta e="T248" id="Seg_5963" s="T247">Q</ta>
            <ta e="T249" id="Seg_5964" s="T248">was.[NOM]</ta>
            <ta e="T250" id="Seg_5965" s="T249">Q</ta>
            <ta e="T251" id="Seg_5966" s="T250">Schulter-ABL</ta>
            <ta e="T252" id="Seg_5967" s="T251">Kopf-POSS</ta>
            <ta e="T253" id="Seg_5968" s="T252">NEG</ta>
            <ta e="T254" id="Seg_5969" s="T253">Mensch.[NOM]</ta>
            <ta e="T255" id="Seg_5970" s="T254">hineingehen-PST2.[3SG]</ta>
            <ta e="T256" id="Seg_5971" s="T255">Mensch.[NOM]</ta>
            <ta e="T257" id="Seg_5972" s="T256">Q</ta>
            <ta e="T258" id="Seg_5973" s="T257">was.[NOM]</ta>
            <ta e="T259" id="Seg_5974" s="T258">Q</ta>
            <ta e="T260" id="Seg_5975" s="T259">was-ACC</ta>
            <ta e="T261" id="Seg_5976" s="T260">NEG</ta>
            <ta e="T262" id="Seg_5977" s="T261">verstehen-NEG-1SG</ta>
            <ta e="T263" id="Seg_5978" s="T262">sagen-PST2.[3SG]</ta>
            <ta e="T264" id="Seg_5979" s="T263">jenes</ta>
            <ta e="T265" id="Seg_5980" s="T264">hineingehen-CVB.SEQ</ta>
            <ta e="T266" id="Seg_5981" s="T265">Tür-DAT/LOC</ta>
            <ta e="T267" id="Seg_5982" s="T266">sich.setzen-PST2.[3SG]</ta>
            <ta e="T268" id="Seg_5983" s="T267">EMPH</ta>
            <ta e="T269" id="Seg_5984" s="T268">EMPH</ta>
            <ta e="T270" id="Seg_5985" s="T269">jenes</ta>
            <ta e="T271" id="Seg_5986" s="T270">sitzen-CVB.SEQ</ta>
            <ta e="T272" id="Seg_5987" s="T271">aber</ta>
            <ta e="T273" id="Seg_5988" s="T272">hey</ta>
            <ta e="T274" id="Seg_5989" s="T273">lachen-CVB.SEQ</ta>
            <ta e="T275" id="Seg_5990" s="T274">die.Zähne.zeigen-PST2.[3SG]</ta>
            <ta e="T276" id="Seg_5991" s="T275">wo</ta>
            <ta e="T277" id="Seg_5992" s="T276">INDEF</ta>
            <ta e="T278" id="Seg_5993" s="T277">Auge-PROPR.[NOM]</ta>
            <ta e="T279" id="Seg_5994" s="T278">Kopf-3SG-DAT/LOC</ta>
            <ta e="T280" id="Seg_5995" s="T279">einsam</ta>
            <ta e="T281" id="Seg_5996" s="T280">Auge-PROPR.[NOM]</ta>
            <ta e="T282" id="Seg_5997" s="T281">na</ta>
            <ta e="T285" id="Seg_5998" s="T284">Freund-PL.[NOM]</ta>
            <ta e="T286" id="Seg_5999" s="T285">kräftig-ADVZ</ta>
            <ta e="T287" id="Seg_6000" s="T286">halten-REFL-EP-IMP.2PL</ta>
            <ta e="T288" id="Seg_6001" s="T287">sagen-PRS.[3SG]</ta>
            <ta e="T289" id="Seg_6002" s="T288">EMPH</ta>
            <ta e="T290" id="Seg_6003" s="T289">Freund-3SG-ACC</ta>
            <ta e="T291" id="Seg_6004" s="T290">Junge-PL-3SG-ACC</ta>
            <ta e="T292" id="Seg_6005" s="T291">Begitschew.[NOM]</ta>
            <ta e="T293" id="Seg_6006" s="T292">jenes-3SG-3SG.[NOM]</ta>
            <ta e="T294" id="Seg_6007" s="T293">hup</ta>
            <ta e="T295" id="Seg_6008" s="T294">sagen-CVB.SEQ</ta>
            <ta e="T296" id="Seg_6009" s="T295">saugen-PST2.[3SG]</ta>
            <ta e="T297" id="Seg_6010" s="T296">zwei</ta>
            <ta e="T298" id="Seg_6011" s="T297">Mensch-3SG-ACC</ta>
            <ta e="T299" id="Seg_6012" s="T298">saugen-CVB.SEQ</ta>
            <ta e="T300" id="Seg_6013" s="T299">nehmen-PST2.[3SG]</ta>
            <ta e="T301" id="Seg_6014" s="T300">dann</ta>
            <ta e="T302" id="Seg_6015" s="T301">dieses</ta>
            <ta e="T303" id="Seg_6016" s="T302">Mensch.[NOM]</ta>
            <ta e="T304" id="Seg_6017" s="T303">einsam</ta>
            <ta e="T305" id="Seg_6018" s="T304">Mensch-PROPR.[NOM]</ta>
            <ta e="T306" id="Seg_6019" s="T305">bleiben-PST2.[3SG]</ta>
            <ta e="T307" id="Seg_6020" s="T306">dort</ta>
            <ta e="T308" id="Seg_6021" s="T307">dieses</ta>
            <ta e="T309" id="Seg_6022" s="T308">Mensch.[NOM]</ta>
            <ta e="T310" id="Seg_6023" s="T309">MOD</ta>
            <ta e="T311" id="Seg_6024" s="T310">Pistole-PROPR.[NOM]</ta>
            <ta e="T312" id="Seg_6025" s="T311">sein-PST2.[3SG]</ta>
            <ta e="T313" id="Seg_6026" s="T312">jenes</ta>
            <ta e="T314" id="Seg_6027" s="T313">dort</ta>
            <ta e="T315" id="Seg_6028" s="T314">sagen-PRS-1SG</ta>
            <ta e="T316" id="Seg_6029" s="T315">sprechen-PTCP.PRS</ta>
            <ta e="T317" id="Seg_6030" s="T316">Zunge-EP-INSTR</ta>
            <ta e="T318" id="Seg_6031" s="T317">sagen-PRS-1SG</ta>
            <ta e="T319" id="Seg_6032" s="T318">sagen-PST2.[3SG]</ta>
            <ta e="T320" id="Seg_6033" s="T319">Pistole-EP-1SG.[NOM]</ta>
            <ta e="T321" id="Seg_6034" s="T320">Mund-3SG-DAT/LOC</ta>
            <ta e="T322" id="Seg_6035" s="T321">nun</ta>
            <ta e="T323" id="Seg_6036" s="T322">dieses</ta>
            <ta e="T324" id="Seg_6037" s="T323">Land-DAT/LOC</ta>
            <ta e="T325" id="Seg_6038" s="T324">bleiben-PRS-1SG</ta>
            <ta e="T326" id="Seg_6039" s="T325">Q</ta>
            <ta e="T327" id="Seg_6040" s="T326">1SG.[NOM]</ta>
            <ta e="T328" id="Seg_6041" s="T327">1SG-ACC</ta>
            <ta e="T329" id="Seg_6042" s="T328">retten.[IMP.2SG]</ta>
            <ta e="T330" id="Seg_6043" s="T329">dieses-ACC</ta>
            <ta e="T331" id="Seg_6044" s="T330">wohin</ta>
            <ta e="T332" id="Seg_6045" s="T331">INDEF</ta>
            <ta e="T333" id="Seg_6046" s="T332">ohnmächtig.werden-EP-CAUS-PTCP.PRS</ta>
            <ta e="T334" id="Seg_6047" s="T333">Ort-2SG-DAT/LOC</ta>
            <ta e="T335" id="Seg_6048" s="T334">bringen.[IMP.2SG]</ta>
            <ta e="T336" id="Seg_6049" s="T335">sagen-PRS.[3SG]</ta>
            <ta e="T337" id="Seg_6050" s="T336">Stirn-3SG-DAT/LOC</ta>
            <ta e="T338" id="Seg_6051" s="T337">schießen-PST2.[3SG]</ta>
            <ta e="T339" id="Seg_6052" s="T338">MOD</ta>
            <ta e="T340" id="Seg_6053" s="T339">dieses</ta>
            <ta e="T341" id="Seg_6054" s="T340">Pistole-3SG-INSTR</ta>
            <ta e="T342" id="Seg_6055" s="T341">jenes-3SG-3SG.[NOM]</ta>
            <ta e="T343" id="Seg_6056" s="T342">doch</ta>
            <ta e="T344" id="Seg_6057" s="T343">ohnmächtig.werden-CVB.SEQ</ta>
            <ta e="T345" id="Seg_6058" s="T344">fallen-PST2.[3SG]</ta>
            <ta e="T346" id="Seg_6059" s="T345">jenes</ta>
            <ta e="T347" id="Seg_6060" s="T346">ohnmächtig.werden-CVB.SEQ</ta>
            <ta e="T348" id="Seg_6061" s="T347">fallen-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T349" id="Seg_6062" s="T348">oberer.Teil-EP-INSTR</ta>
            <ta e="T350" id="Seg_6063" s="T349">laufen-CVB.SEQ</ta>
            <ta e="T351" id="Seg_6064" s="T350">hinausgehen-EP-PST2-3PL</ta>
            <ta e="T352" id="Seg_6065" s="T351">sein-COND.[3SG]</ta>
            <ta e="T353" id="Seg_6066" s="T352">Begitschew-EP-2SG.[NOM]</ta>
            <ta e="T354" id="Seg_6067" s="T353">erzählen-PRS.[3SG]</ta>
            <ta e="T357" id="Seg_6068" s="T356">jenes</ta>
            <ta e="T358" id="Seg_6069" s="T357">ankommen-CVB.SEQ-1PL</ta>
            <ta e="T359" id="Seg_6070" s="T358">wer-DAT/LOC</ta>
            <ta e="T360" id="Seg_6071" s="T359">gehen-PST1-1PL</ta>
            <ta e="T361" id="Seg_6072" s="T360">sagen-PRS.[3SG]</ta>
            <ta e="T362" id="Seg_6073" s="T361">EMPH</ta>
            <ta e="T363" id="Seg_6074" s="T362">Motorboot-1SG-DAT/LOC</ta>
            <ta e="T364" id="Seg_6075" s="T363">gehen-PST1-1SG</ta>
            <ta e="T365" id="Seg_6076" s="T364">sagen-PRS.[3SG]</ta>
            <ta e="T366" id="Seg_6077" s="T365">Motorboot-1SG-DAT/LOC</ta>
            <ta e="T367" id="Seg_6078" s="T366">sich.setzen-CVB.SEQ-1SG</ta>
            <ta e="T368" id="Seg_6079" s="T367">Bucht.[NOM]</ta>
            <ta e="T369" id="Seg_6080" s="T368">Mitte-3SG-DAT/LOC</ta>
            <ta e="T370" id="Seg_6081" s="T369">hineingehen-PST1-1SG</ta>
            <ta e="T371" id="Seg_6082" s="T370">sehen-PST2-EP-1SG</ta>
            <ta e="T372" id="Seg_6083" s="T371">Hinterteil-1SG-ABL</ta>
            <ta e="T373" id="Seg_6084" s="T372">kürzlich-EP-1SG.[NOM]</ta>
            <ta e="T374" id="Seg_6085" s="T373">aufstehen-CVB.SEQ</ta>
            <ta e="T375" id="Seg_6086" s="T374">kommen-PST1-3SG</ta>
            <ta e="T376" id="Seg_6087" s="T375">sagen-CVB.SEQ</ta>
            <ta e="T377" id="Seg_6088" s="T376">aufstehen-CVB.SEQ</ta>
            <ta e="T378" id="Seg_6089" s="T377">kommen-CVB.SEQ</ta>
            <ta e="T379" id="Seg_6090" s="T378">aber</ta>
            <ta e="T380" id="Seg_6091" s="T379">Motorboot-COMP</ta>
            <ta e="T381" id="Seg_6092" s="T380">EMPH</ta>
            <ta e="T382" id="Seg_6093" s="T381">groß</ta>
            <ta e="T383" id="Seg_6094" s="T382">großer.Stein-ACC</ta>
            <ta e="T384" id="Seg_6095" s="T383">nehmen-CVB.SEQ</ta>
            <ta e="T385" id="Seg_6096" s="T384">nachdem</ta>
            <ta e="T386" id="Seg_6097" s="T385">doch</ta>
            <ta e="T387" id="Seg_6098" s="T386">werfen-PST2.[3SG]</ta>
            <ta e="T388" id="Seg_6099" s="T387">wer.[NOM]</ta>
            <ta e="T389" id="Seg_6100" s="T388">doch</ta>
            <ta e="T390" id="Seg_6101" s="T389">schießen-PASS-PTCP.HAB-3SG.[NOM]</ta>
            <ta e="T391" id="Seg_6102" s="T390">jenes</ta>
            <ta e="T392" id="Seg_6103" s="T391">werfen-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T393" id="Seg_6104" s="T392">EMPH</ta>
            <ta e="T394" id="Seg_6105" s="T393">kaum</ta>
            <ta e="T395" id="Seg_6106" s="T394">Bucht.[NOM]</ta>
            <ta e="T396" id="Seg_6107" s="T395">Mitte-3SG-DAT/LOC</ta>
            <ta e="T397" id="Seg_6108" s="T396">ankommen-PST2-EP-1SG</ta>
            <ta e="T398" id="Seg_6109" s="T397">ankommen-PTCP.PRS-3SG-ACC</ta>
            <ta e="T399" id="Seg_6110" s="T398">mit</ta>
            <ta e="T400" id="Seg_6111" s="T399">MOD</ta>
            <ta e="T401" id="Seg_6112" s="T400">jenes</ta>
            <ta e="T402" id="Seg_6113" s="T401">Welle-3SG-DAT/LOC</ta>
            <ta e="T403" id="Seg_6114" s="T402">werfen-PTCP.PST</ta>
            <ta e="T404" id="Seg_6115" s="T403">Welle-3SG-DAT/LOC</ta>
            <ta e="T405" id="Seg_6116" s="T404">fast</ta>
            <ta e="T406" id="Seg_6117" s="T405">ausreißen-CVB.SIM</ta>
            <ta e="T407" id="Seg_6118" s="T406">gehen-CVB.SIM</ta>
            <ta e="T408" id="Seg_6119" s="T407">fast.tun-PST2.[3SG]</ta>
            <ta e="T409" id="Seg_6120" s="T408">jenes</ta>
            <ta e="T410" id="Seg_6121" s="T409">entfliehen-PST1-1PL</ta>
            <ta e="T411" id="Seg_6122" s="T410">sagen-PST2.[3SG]</ta>
            <ta e="T412" id="Seg_6123" s="T411">MOD</ta>
            <ta e="T413" id="Seg_6124" s="T412">dann</ta>
            <ta e="T414" id="Seg_6125" s="T413">sieben</ta>
            <ta e="T415" id="Seg_6126" s="T414">Mensch-ABL</ta>
            <ta e="T416" id="Seg_6127" s="T415">einsam</ta>
            <ta e="T417" id="Seg_6128" s="T416">Mensch-PROPR</ta>
            <ta e="T418" id="Seg_6129" s="T417">kommen-PST2.[3SG]</ta>
            <ta e="T419" id="Seg_6130" s="T418">hungern-CVB.SEQ-3PL</ta>
            <ta e="T420" id="Seg_6131" s="T419">selbst-3PL.[NOM]</ta>
            <ta e="T421" id="Seg_6132" s="T420">was-ACC</ta>
            <ta e="T422" id="Seg_6133" s="T421">NEG</ta>
            <ta e="T423" id="Seg_6134" s="T422">finden-NEG.CVB.SIM</ta>
            <ta e="T424" id="Seg_6135" s="T423">sich.drehen-CVB.SEQ</ta>
            <ta e="T425" id="Seg_6136" s="T424">kommen</ta>
            <ta e="T426" id="Seg_6137" s="T425">jenes</ta>
            <ta e="T427" id="Seg_6138" s="T426">kommen-CVB.SEQ</ta>
            <ta e="T428" id="Seg_6139" s="T427">jenes</ta>
            <ta e="T429" id="Seg_6140" s="T428">nach.oben</ta>
            <ta e="T430" id="Seg_6141" s="T429">zu</ta>
            <ta e="T431" id="Seg_6142" s="T430">gehen-PST2.[3SG]</ta>
            <ta e="T432" id="Seg_6143" s="T431">dann</ta>
            <ta e="T433" id="Seg_6144" s="T432">Tschagydaj-ACC</ta>
            <ta e="T434" id="Seg_6145" s="T433">aber</ta>
            <ta e="T435" id="Seg_6146" s="T434">im.Sommer</ta>
            <ta e="T436" id="Seg_6147" s="T435">zusammen</ta>
            <ta e="T437" id="Seg_6148" s="T436">leben-PRS-3PL</ta>
            <ta e="T438" id="Seg_6149" s="T437">man.sagt</ta>
            <ta e="T439" id="Seg_6150" s="T438">essen-CVB.SEQ</ta>
            <ta e="T440" id="Seg_6151" s="T439">leben-CVB.SEQ-3PL</ta>
            <ta e="T441" id="Seg_6152" s="T440">nun</ta>
            <ta e="T442" id="Seg_6153" s="T441">Brief.[NOM]</ta>
            <ta e="T443" id="Seg_6154" s="T442">kommen-PST2.[3SG]</ta>
            <ta e="T444" id="Seg_6155" s="T443">dieses</ta>
            <ta e="T445" id="Seg_6156" s="T444">Brief-ACC</ta>
            <ta e="T446" id="Seg_6157" s="T445">aber</ta>
            <ta e="T447" id="Seg_6158" s="T446">Chatanga-DAT/LOC</ta>
            <ta e="T448" id="Seg_6159" s="T447">bringen-PTCP.FUT-3PL-ACC</ta>
            <ta e="T449" id="Seg_6160" s="T448">man.muss</ta>
            <ta e="T450" id="Seg_6161" s="T449">woher</ta>
            <ta e="T451" id="Seg_6162" s="T450">INDEF</ta>
            <ta e="T452" id="Seg_6163" s="T451">kommen-PST2.[3SG]</ta>
            <ta e="T453" id="Seg_6164" s="T452">kleines.Boot-INSTR</ta>
            <ta e="T454" id="Seg_6165" s="T453">dann</ta>
            <ta e="T455" id="Seg_6166" s="T454">was.[NOM]</ta>
            <ta e="T456" id="Seg_6167" s="T455">Boot-3SG.[NOM]</ta>
            <ta e="T457" id="Seg_6168" s="T456">kommen-FUT.[3SG]=Q</ta>
            <ta e="T458" id="Seg_6169" s="T457">nun</ta>
            <ta e="T459" id="Seg_6170" s="T458">Tschagydaj</ta>
            <ta e="T460" id="Seg_6171" s="T459">dieses</ta>
            <ta e="T461" id="Seg_6172" s="T460">Brief-ACC</ta>
            <ta e="T462" id="Seg_6173" s="T461">MOD</ta>
            <ta e="T463" id="Seg_6174" s="T462">wer-DAT/LOC</ta>
            <ta e="T464" id="Seg_6175" s="T463">bringen.[IMP.2SG]</ta>
            <ta e="T465" id="Seg_6176" s="T464">Kresty-DAT/LOC</ta>
            <ta e="T466" id="Seg_6177" s="T465">Novorybnoe</ta>
            <ta e="T467" id="Seg_6178" s="T466">Kresty-3SG-DAT/LOC</ta>
            <ta e="T468" id="Seg_6179" s="T467">sehen.[IMP.2SG]</ta>
            <ta e="T469" id="Seg_6180" s="T468">fern-ADVZ</ta>
            <ta e="T470" id="Seg_6181" s="T469">Novorybnoe.[NOM]</ta>
            <ta e="T471" id="Seg_6182" s="T470">entfernt</ta>
            <ta e="T472" id="Seg_6183" s="T471">Seite-3SG-DAT/LOC</ta>
            <ta e="T478" id="Seg_6184" s="T476">nein</ta>
            <ta e="T479" id="Seg_6185" s="T478">jenes</ta>
            <ta e="T480" id="Seg_6186" s="T479">selbst-PL-3SG-GEN</ta>
            <ta e="T482" id="Seg_6187" s="T480">eigen-3PL.[NOM]</ta>
            <ta e="T485" id="Seg_6188" s="T482">AFFIRM</ta>
            <ta e="T486" id="Seg_6189" s="T485">Begitschew-3PL.[NOM]</ta>
            <ta e="T487" id="Seg_6190" s="T486">EMPH-dann</ta>
            <ta e="T488" id="Seg_6191" s="T487">reisen-PTCP.PST</ta>
            <ta e="T489" id="Seg_6192" s="T488">sein-PST1-3SG</ta>
            <ta e="T490" id="Seg_6193" s="T489">jenes</ta>
            <ta e="T491" id="Seg_6194" s="T490">gehen-PTCP.PST-3SG-INSTR</ta>
            <ta e="T492" id="Seg_6195" s="T491">gehen-PST2.[3SG]</ta>
            <ta e="T493" id="Seg_6196" s="T492">dieses</ta>
            <ta e="T494" id="Seg_6197" s="T493">zu</ta>
            <ta e="T495" id="Seg_6198" s="T494">nach.oben</ta>
            <ta e="T496" id="Seg_6199" s="T495">zu</ta>
            <ta e="T497" id="Seg_6200" s="T496">doch</ta>
            <ta e="T498" id="Seg_6201" s="T497">jenes</ta>
            <ta e="T499" id="Seg_6202" s="T498">EMPH</ta>
            <ta e="T500" id="Seg_6203" s="T499">jenes</ta>
            <ta e="T501" id="Seg_6204" s="T500">Brief-ACC</ta>
            <ta e="T502" id="Seg_6205" s="T501">bringen-EP-CAUS-PRS-3PL</ta>
            <ta e="T503" id="Seg_6206" s="T502">dann</ta>
            <ta e="T504" id="Seg_6207" s="T503">Brief-PROPR-3PL</ta>
            <ta e="T505" id="Seg_6208" s="T504">sein-PST2-3SG</ta>
            <ta e="T506" id="Seg_6209" s="T505">MOD</ta>
            <ta e="T507" id="Seg_6210" s="T506">Herr-PROPR-3PL</ta>
            <ta e="T508" id="Seg_6211" s="T507">EMPH</ta>
            <ta e="T512" id="Seg_6212" s="T511">Dolgane-PL.[NOM]</ta>
            <ta e="T513" id="Seg_6213" s="T512">jenes</ta>
            <ta e="T514" id="Seg_6214" s="T513">Arbeiter-3PL.[NOM]</ta>
            <ta e="T515" id="Seg_6215" s="T514">Sowjet-3PL.[NOM]</ta>
            <ta e="T516" id="Seg_6216" s="T515">was-3PL.[NOM]</ta>
            <ta e="T517" id="Seg_6217" s="T516">Brief.[NOM]</ta>
            <ta e="T518" id="Seg_6218" s="T517">schicken-PRS-3PL</ta>
            <ta e="T519" id="Seg_6219" s="T518">EMPH</ta>
            <ta e="T520" id="Seg_6220" s="T519">so</ta>
            <ta e="T521" id="Seg_6221" s="T520">machen</ta>
            <ta e="T522" id="Seg_6222" s="T521">dieses</ta>
            <ta e="T523" id="Seg_6223" s="T522">alter.Mann.[NOM]</ta>
            <ta e="T524" id="Seg_6224" s="T523">MOD</ta>
            <ta e="T525" id="Seg_6225" s="T524">doch</ta>
            <ta e="T526" id="Seg_6226" s="T525">am.Abend</ta>
            <ta e="T527" id="Seg_6227" s="T526">sich.auf.den.Weg.machen-PRS.[3SG]</ta>
            <ta e="T528" id="Seg_6228" s="T527">am.Morgen</ta>
            <ta e="T529" id="Seg_6229" s="T528">früh-AG</ta>
            <ta e="T530" id="Seg_6230" s="T529">Frau.[NOM]</ta>
            <ta e="T531" id="Seg_6231" s="T530">aufstehen-PTCP.PRS</ta>
            <ta e="T532" id="Seg_6232" s="T531">Zeit-3SG-DAT/LOC</ta>
            <ta e="T533" id="Seg_6233" s="T532">kommen-FUT-1SG</ta>
            <ta e="T534" id="Seg_6234" s="T533">sagen-PST2.[3SG]</ta>
            <ta e="T535" id="Seg_6235" s="T534">dieses</ta>
            <ta e="T536" id="Seg_6236" s="T535">alter.Mann.[NOM]</ta>
            <ta e="T537" id="Seg_6237" s="T536">oh</ta>
            <ta e="T538" id="Seg_6238" s="T537">wie</ta>
            <ta e="T539" id="Seg_6239" s="T538">machen-CVB.SEQ</ta>
            <ta e="T540" id="Seg_6240" s="T539">kommen-CVB.PURP</ta>
            <ta e="T541" id="Seg_6241" s="T540">machen-PRS-2SG</ta>
            <ta e="T542" id="Seg_6242" s="T541">Novorybnoe.[NOM]</ta>
            <ta e="T543" id="Seg_6243" s="T542">entfernt</ta>
            <ta e="T544" id="Seg_6244" s="T543">Seite-3SG-ABL</ta>
            <ta e="T545" id="Seg_6245" s="T544">sehr</ta>
            <ta e="T546" id="Seg_6246" s="T545">fern.[NOM]</ta>
            <ta e="T547" id="Seg_6247" s="T546">Popigaj-ABL</ta>
            <ta e="T548" id="Seg_6248" s="T547">doch</ta>
            <ta e="T549" id="Seg_6249" s="T548">dieses</ta>
            <ta e="T550" id="Seg_6250" s="T549">alter.Mann.[NOM]</ta>
            <ta e="T551" id="Seg_6251" s="T550">go-CVB.SEQ</ta>
            <ta e="T552" id="Seg_6252" s="T551">bleiben-PST2.[3SG]</ta>
            <ta e="T553" id="Seg_6253" s="T552">äh</ta>
            <ta e="T554" id="Seg_6254" s="T553">gehen-CVB.SEQ</ta>
            <ta e="T555" id="Seg_6255" s="T554">bleiben-PST2.[3SG]</ta>
            <ta e="T556" id="Seg_6256" s="T555">kleines.Boot-INSTR</ta>
            <ta e="T557" id="Seg_6257" s="T556">am.Morgen</ta>
            <ta e="T558" id="Seg_6258" s="T557">Alte-PL.[NOM]</ta>
            <ta e="T561" id="Seg_6259" s="T560">früh-AG</ta>
            <ta e="T562" id="Seg_6260" s="T561">Frau-PL.[NOM]</ta>
            <ta e="T563" id="Seg_6261" s="T562">aufstehen-PRS-3PL</ta>
            <ta e="T564" id="Seg_6262" s="T563">zum.Beispiel</ta>
            <ta e="T565" id="Seg_6263" s="T564">dann</ta>
            <ta e="T566" id="Seg_6264" s="T565">sieben</ta>
            <ta e="T567" id="Seg_6265" s="T566">Stunde.[NOM]</ta>
            <ta e="T568" id="Seg_6266" s="T567">EMPH</ta>
            <ta e="T569" id="Seg_6267" s="T568">sein-PST2.[3SG]</ta>
            <ta e="T570" id="Seg_6268" s="T569">offenbar</ta>
            <ta e="T571" id="Seg_6269" s="T570">was.[NOM]</ta>
            <ta e="T572" id="Seg_6270" s="T571">Stunde-3SG.[NOM]</ta>
            <ta e="T573" id="Seg_6271" s="T572">kommen-FUT.[3SG]=Q</ta>
            <ta e="T574" id="Seg_6272" s="T573">Sonne-3PL-INSTR</ta>
            <ta e="T575" id="Seg_6273" s="T574">sehen-PRS-3PL</ta>
            <ta e="T576" id="Seg_6274" s="T575">EMPH</ta>
            <ta e="T577" id="Seg_6275" s="T576">oh.nein</ta>
            <ta e="T578" id="Seg_6276" s="T577">sieben</ta>
            <ta e="T579" id="Seg_6277" s="T578">Stunde-DAT/LOC</ta>
            <ta e="T580" id="Seg_6278" s="T579">Tschagydaj.[NOM]</ta>
            <ta e="T581" id="Seg_6279" s="T580">kommen-PST2.[3SG]</ta>
            <ta e="T582" id="Seg_6280" s="T581">kleines.Boot-3SG.[NOM]</ta>
            <ta e="T583" id="Seg_6281" s="T582">tauchen-EP-REFL-CVB.SIM</ta>
            <ta e="T584" id="Seg_6282" s="T583">liegen-PRS.[3SG]</ta>
            <ta e="T585" id="Seg_6283" s="T584">EMPH</ta>
            <ta e="T586" id="Seg_6284" s="T585">sehen.[IMP.2SG]</ta>
            <ta e="T587" id="Seg_6285" s="T586">solch</ta>
            <ta e="T588" id="Seg_6286" s="T587">kühn</ta>
            <ta e="T589" id="Seg_6287" s="T588">Mensch-PL.[NOM]</ta>
            <ta e="T590" id="Seg_6288" s="T589">es.gibt-3PL</ta>
            <ta e="T591" id="Seg_6289" s="T590">dann</ta>
            <ta e="T592" id="Seg_6290" s="T591">Frau.[NOM]</ta>
            <ta e="T593" id="Seg_6291" s="T592">gebären-PRS.[3SG]</ta>
            <ta e="T594" id="Seg_6292" s="T593">jenes.EMPH.[NOM]</ta>
            <ta e="T595" id="Seg_6293" s="T594">vorübergehende.Siedlung-3PL-DAT/LOC</ta>
            <ta e="T596" id="Seg_6294" s="T595">dieses</ta>
            <ta e="T597" id="Seg_6295" s="T596">Frau.[NOM]</ta>
            <ta e="T598" id="Seg_6296" s="T597">sterben-CVB.PURP</ta>
            <ta e="T599" id="Seg_6297" s="T598">machen-PST2.[3SG]</ta>
            <ta e="T600" id="Seg_6298" s="T599">wie</ta>
            <ta e="T601" id="Seg_6299" s="T600">NEG</ta>
            <ta e="T602" id="Seg_6300" s="T601">machen-PTCP.FUT-3PL-ACC</ta>
            <ta e="T603" id="Seg_6301" s="T602">sehr</ta>
            <ta e="T604" id="Seg_6302" s="T603">Doktor-POSS</ta>
            <ta e="T605" id="Seg_6303" s="T604">NEG</ta>
            <ta e="T606" id="Seg_6304" s="T605">Zeit-DAT/LOC</ta>
            <ta e="T607" id="Seg_6305" s="T606">nun</ta>
            <ta e="T608" id="Seg_6306" s="T607">Hebamme-3PL.[NOM]</ta>
            <ta e="T609" id="Seg_6307" s="T608">Unglück-PROPR</ta>
            <ta e="T610" id="Seg_6308" s="T609">müde.werden-CVB.SEQ</ta>
            <ta e="T611" id="Seg_6309" s="T610">aufhören-PST2.[3SG]</ta>
            <ta e="T612" id="Seg_6310" s="T611">na</ta>
            <ta e="T613" id="Seg_6311" s="T612">Tschagydaj.[NOM]</ta>
            <ta e="T614" id="Seg_6312" s="T613">kommen-IMP.3SG</ta>
            <ta e="T615" id="Seg_6313" s="T614">Frau.[NOM]</ta>
            <ta e="T616" id="Seg_6314" s="T615">sterben-CVB.PURP</ta>
            <ta e="T617" id="Seg_6315" s="T616">machen-PST1-3SG</ta>
            <ta e="T618" id="Seg_6316" s="T617">wie</ta>
            <ta e="T619" id="Seg_6317" s="T618">INDEF</ta>
            <ta e="T620" id="Seg_6318" s="T619">machen-IMP.3SG</ta>
            <ta e="T621" id="Seg_6319" s="T620">Knochen.[NOM]</ta>
            <ta e="T622" id="Seg_6320" s="T621">trennen-IMP.3SG</ta>
            <ta e="T623" id="Seg_6321" s="T622">hier</ta>
            <ta e="T624" id="Seg_6322" s="T623">EMPH-so</ta>
            <ta e="T625" id="Seg_6323" s="T624">dieses</ta>
            <ta e="T626" id="Seg_6324" s="T625">Tschagydaj.[NOM]</ta>
            <ta e="T627" id="Seg_6325" s="T626">kommen-PRS.[3SG]</ta>
            <ta e="T628" id="Seg_6326" s="T627">wie</ta>
            <ta e="T631" id="Seg_6327" s="T630">dieses</ta>
            <ta e="T632" id="Seg_6328" s="T631">Frau-ACC</ta>
            <ta e="T633" id="Seg_6329" s="T632">Hand-VBZ-CVB.SEQ</ta>
            <ta e="T634" id="Seg_6330" s="T633">nehmen-PRS.[3SG]</ta>
            <ta e="T635" id="Seg_6331" s="T634">Kind-3SG-ACC</ta>
            <ta e="T636" id="Seg_6332" s="T635">EMPH-dann</ta>
            <ta e="T637" id="Seg_6333" s="T636">lebendig</ta>
            <ta e="T638" id="Seg_6334" s="T637">lassen-PRS.[3SG]</ta>
            <ta e="T639" id="Seg_6335" s="T638">sehen.[IMP.2SG]</ta>
            <ta e="T640" id="Seg_6336" s="T639">solch-PL.[NOM]</ta>
            <ta e="T641" id="Seg_6337" s="T640">es.gibt</ta>
            <ta e="T642" id="Seg_6338" s="T641">sein-PST1-3PL</ta>
            <ta e="T643" id="Seg_6339" s="T642">alt-PL.[NOM]</ta>
            <ta e="T644" id="Seg_6340" s="T643">und</ta>
            <ta e="T645" id="Seg_6341" s="T644">Tschakyyla.[NOM]</ta>
            <ta e="T646" id="Seg_6342" s="T645">alter.Mann.[NOM]</ta>
            <ta e="T647" id="Seg_6343" s="T646">Großvater-3SG.[NOM]</ta>
            <ta e="T648" id="Seg_6344" s="T647">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T652" id="Seg_6345" s="T651">AFFIRM</ta>
            <ta e="T653" id="Seg_6346" s="T652">dann</ta>
            <ta e="T654" id="Seg_6347" s="T653">jenes</ta>
            <ta e="T655" id="Seg_6348" s="T654">Tschagydaj-EP-2SG.[NOM]</ta>
            <ta e="T656" id="Seg_6349" s="T655">Frau-3SG.[NOM]</ta>
            <ta e="T657" id="Seg_6350" s="T656">aber</ta>
            <ta e="T658" id="Seg_6351" s="T657">Alte.[NOM]</ta>
            <ta e="T659" id="Seg_6352" s="T658">Anna.[NOM]</ta>
            <ta e="T660" id="Seg_6353" s="T659">Mutter-3SG.[NOM]</ta>
            <ta e="T661" id="Seg_6354" s="T660">Alte.[NOM]</ta>
            <ta e="T662" id="Seg_6355" s="T661">dieses</ta>
            <ta e="T663" id="Seg_6356" s="T662">Tscheeke</ta>
            <ta e="T664" id="Seg_6357" s="T663">Alte.[NOM]</ta>
            <ta e="T665" id="Seg_6358" s="T664">jenes-3SG-2SG.[NOM]</ta>
            <ta e="T666" id="Seg_6359" s="T665">Vater-3SG.[NOM]</ta>
            <ta e="T667" id="Seg_6360" s="T666">1PL.[NOM]</ta>
            <ta e="T668" id="Seg_6361" s="T667">Großvater-1PL-ACC</ta>
            <ta e="T669" id="Seg_6362" s="T668">mit</ta>
            <ta e="T670" id="Seg_6363" s="T669">zusammen</ta>
            <ta e="T671" id="Seg_6364" s="T670">geboren.werden-PST2-3PL</ta>
            <ta e="T672" id="Seg_6365" s="T671">Kudrjakov</ta>
            <ta e="T673" id="Seg_6366" s="T672">alter.Mann.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr-KiPP">
            <ta e="T1" id="Seg_6367" s="T0">Бегичев-ACC</ta>
            <ta e="T2" id="Seg_6368" s="T1">уносить-PRS-3PL</ta>
            <ta e="T5" id="Seg_6369" s="T4">земля-EP-INSTR</ta>
            <ta e="T6" id="Seg_6370" s="T5">идти-PTCP.PRS</ta>
            <ta e="T7" id="Seg_6371" s="T6">быть-PST1-3SG</ta>
            <ta e="T8" id="Seg_6372" s="T7">раньше</ta>
            <ta e="T9" id="Seg_6373" s="T8">экспедиция-PL.[NOM]</ta>
            <ta e="T10" id="Seg_6374" s="T9">тот-ACC</ta>
            <ta e="T11" id="Seg_6375" s="T10">дедушка-1SG.[NOM]</ta>
            <ta e="T12" id="Seg_6376" s="T11">рассказывать-HAB.[3SG]</ta>
            <ta e="T13" id="Seg_6377" s="T12">мой</ta>
            <ta e="T14" id="Seg_6378" s="T13">мать-1SG.[NOM]</ta>
            <ta e="T15" id="Seg_6379" s="T14">дедушка.[NOM]</ta>
            <ta e="T16" id="Seg_6380" s="T15">тот</ta>
            <ta e="T17" id="Seg_6381" s="T16">мать-1SG.[NOM]</ta>
            <ta e="T18" id="Seg_6382" s="T17">родственник-PL-3SG.[NOM]</ta>
            <ta e="T19" id="Seg_6383" s="T18">Маркел</ta>
            <ta e="T20" id="Seg_6384" s="T19">старик.[NOM]</ta>
            <ta e="T21" id="Seg_6385" s="T20">Елена.[NOM]</ta>
            <ta e="T22" id="Seg_6386" s="T21">отец-3SG.[NOM]</ta>
            <ta e="T23" id="Seg_6387" s="T22">старик.[NOM]</ta>
            <ta e="T24" id="Seg_6388" s="T23">тот-PL.[NOM]</ta>
            <ta e="T25" id="Seg_6389" s="T24">разговаривать-PTCP.PRS-3PL-ACC</ta>
            <ta e="T26" id="Seg_6390" s="T25">слышать-HAB-1SG</ta>
            <ta e="T27" id="Seg_6391" s="T26">этот</ta>
            <ta e="T28" id="Seg_6392" s="T27">Бегичев.[NOM]</ta>
            <ta e="T29" id="Seg_6393" s="T28">однако</ta>
            <ta e="T30" id="Seg_6394" s="T29">олень-INSTR</ta>
            <ta e="T31" id="Seg_6395" s="T30">путешествовать-PRS.[3SG]</ta>
            <ta e="T32" id="Seg_6396" s="T31">Бегичев.[NOM]</ta>
            <ta e="T33" id="Seg_6397" s="T32">остров-3SG-DAT/LOC</ta>
            <ta e="T34" id="Seg_6398" s="T33">семь</ta>
            <ta e="T35" id="Seg_6399" s="T34">человек-PROPR</ta>
            <ta e="T36" id="Seg_6400" s="T35">этот</ta>
            <ta e="T37" id="Seg_6401" s="T36">человек.[NOM]</ta>
            <ta e="T38" id="Seg_6402" s="T37">путешествовать-CVB.SEQ</ta>
            <ta e="T39" id="Seg_6403" s="T38">идти-CVB.SEQ</ta>
            <ta e="T40" id="Seg_6404" s="T39">олень-INSTR</ta>
            <ta e="T41" id="Seg_6405" s="T40">уносить-PRS-3PL</ta>
            <ta e="T42" id="Seg_6406" s="T41">EMPH</ta>
            <ta e="T43" id="Seg_6407" s="T42">другой-ABL-другой</ta>
            <ta e="T44" id="Seg_6408" s="T43">олень-INSTR</ta>
            <ta e="T45" id="Seg_6409" s="T44">уносить-PRS-3PL</ta>
            <ta e="T46" id="Seg_6410" s="T45">новый-ABL-новый</ta>
            <ta e="T47" id="Seg_6411" s="T46">олень-INSTR</ta>
            <ta e="T48" id="Seg_6412" s="T47">уносить-PRS-3PL</ta>
            <ta e="T49" id="Seg_6413" s="T48">там</ta>
            <ta e="T50" id="Seg_6414" s="T49">Чагыдай</ta>
            <ta e="T51" id="Seg_6415" s="T50">старик-ACC</ta>
            <ta e="T52" id="Seg_6416" s="T51">приносить-EP-CAUS-PRS-3PL</ta>
            <ta e="T53" id="Seg_6417" s="T52">MOD</ta>
            <ta e="T54" id="Seg_6418" s="T53">1SG.[NOM]</ta>
            <ta e="T55" id="Seg_6419" s="T54">старик-EP-1SG.[NOM]</ta>
            <ta e="T56" id="Seg_6420" s="T55">дедушка-3SG-ACC-PROPR</ta>
            <ta e="T57" id="Seg_6421" s="T56">дедушка-3PL.[NOM]</ta>
            <ta e="T58" id="Seg_6422" s="T57">тот</ta>
            <ta e="T59" id="Seg_6423" s="T58">старик.[NOM]</ta>
            <ta e="T60" id="Seg_6424" s="T59">что.[NOM]</ta>
            <ta e="T61" id="Seg_6425" s="T60">NEG</ta>
            <ta e="T62" id="Seg_6426" s="T61">догонять-NEG.PTCP.PST</ta>
            <ta e="T63" id="Seg_6427" s="T62">человек-3SG.[NOM]</ta>
            <ta e="T64" id="Seg_6428" s="T63">быть-PST1-3SG</ta>
            <ta e="T65" id="Seg_6429" s="T64">очень</ta>
            <ta e="T66" id="Seg_6430" s="T65">энергичный</ta>
            <ta e="T67" id="Seg_6431" s="T66">человек.[NOM]</ta>
            <ta e="T68" id="Seg_6432" s="T67">тот</ta>
            <ta e="T69" id="Seg_6433" s="T68">старик</ta>
            <ta e="T70" id="Seg_6434" s="T69">мочь-NEG.PTCP</ta>
            <ta e="T71" id="Seg_6435" s="T70">олень-PL-ACC</ta>
            <ta e="T72" id="Seg_6436" s="T71">запрячь-CAUS-CVB.SEQ</ta>
            <ta e="T73" id="Seg_6437" s="T72">после</ta>
            <ta e="T74" id="Seg_6438" s="T73">Бегичев.[NOM]</ta>
            <ta e="T75" id="Seg_6439" s="T74">остров-3SG-DAT/LOC</ta>
            <ta e="T76" id="Seg_6440" s="T75">приносить-EP-CAUS-PRS-3PL</ta>
            <ta e="T77" id="Seg_6441" s="T76">EMPH</ta>
            <ta e="T78" id="Seg_6442" s="T77">там</ta>
            <ta e="T79" id="Seg_6443" s="T78">олень-PL.[NOM]</ta>
            <ta e="T80" id="Seg_6444" s="T79">двигаться-PRS-3PL</ta>
            <ta e="T81" id="Seg_6445" s="T80">говорят</ta>
            <ta e="T82" id="Seg_6446" s="T81">EMPH</ta>
            <ta e="T83" id="Seg_6447" s="T82">EMPH</ta>
            <ta e="T84" id="Seg_6448" s="T83">олень-PL.[NOM]</ta>
            <ta e="T85" id="Seg_6449" s="T84">двигаться-PRS-3PL</ta>
            <ta e="T86" id="Seg_6450" s="T85">этот</ta>
            <ta e="T87" id="Seg_6451" s="T86">человек.[NOM]</ta>
            <ta e="T88" id="Seg_6452" s="T87">шапка-3SG-ACC</ta>
            <ta e="T89" id="Seg_6453" s="T88">тот</ta>
            <ta e="T90" id="Seg_6454" s="T89">Бегичев.[NOM]</ta>
            <ta e="T91" id="Seg_6455" s="T90">собственный-3SG-GEN</ta>
            <ta e="T92" id="Seg_6456" s="T91">шапка-3SG-ACC</ta>
            <ta e="T93" id="Seg_6457" s="T92">олень.[NOM]</ta>
            <ta e="T94" id="Seg_6458" s="T93">накидывать-CVB.SEQ</ta>
            <ta e="T95" id="Seg_6459" s="T94">бросать-PRS.[3SG]</ta>
            <ta e="T96" id="Seg_6460" s="T95">говорят</ta>
            <ta e="T97" id="Seg_6461" s="T96">этот</ta>
            <ta e="T98" id="Seg_6462" s="T97">сани-ACC</ta>
            <ta e="T99" id="Seg_6463" s="T98">через</ta>
            <ta e="T100" id="Seg_6464" s="T99">шапка.[NOM]</ta>
            <ta e="T101" id="Seg_6465" s="T100">падать-CVB.SEQ</ta>
            <ta e="T102" id="Seg_6466" s="T101">быть-TEMP-3SG</ta>
            <ta e="T103" id="Seg_6467" s="T102">тот-ACC</ta>
            <ta e="T104" id="Seg_6468" s="T103">старик.[NOM]</ta>
            <ta e="T105" id="Seg_6469" s="T104">сани-ACC</ta>
            <ta e="T106" id="Seg_6470" s="T105">через</ta>
            <ta e="T107" id="Seg_6471" s="T106">прыгать-CVB.SEQ</ta>
            <ta e="T108" id="Seg_6472" s="T107">поймать-CVB.SEQ</ta>
            <ta e="T109" id="Seg_6473" s="T108">взять-PST2.[3SG]</ta>
            <ta e="T110" id="Seg_6474" s="T109">EMPH</ta>
            <ta e="T111" id="Seg_6475" s="T110">земля-DAT/LOC</ta>
            <ta e="T112" id="Seg_6476" s="T111">падать-PTCP.FUT-3SG-GEN</ta>
            <ta e="T113" id="Seg_6477" s="T112">та.сторона.[NOM]</ta>
            <ta e="T114" id="Seg_6478" s="T113">сторона-3SG-DAT/LOC</ta>
            <ta e="T115" id="Seg_6479" s="T114">тот-ACC</ta>
            <ta e="T116" id="Seg_6480" s="T115">тот</ta>
            <ta e="T119" id="Seg_6481" s="T118">Бегичев-3SG.[NOM]</ta>
            <ta e="T120" id="Seg_6482" s="T119">говорить-PRS.[3SG]</ta>
            <ta e="T121" id="Seg_6483" s="T120">говорят</ta>
            <ta e="T122" id="Seg_6484" s="T121">о</ta>
            <ta e="T123" id="Seg_6485" s="T122">долганский</ta>
            <ta e="T124" id="Seg_6486" s="T123">земля-3SG-DAT/LOC</ta>
            <ta e="T125" id="Seg_6487" s="T124">да</ta>
            <ta e="T126" id="Seg_6488" s="T125">энергичный</ta>
            <ta e="T127" id="Seg_6489" s="T126">человек.[NOM]</ta>
            <ta e="T128" id="Seg_6490" s="T127">есть</ta>
            <ta e="T129" id="Seg_6491" s="T128">быть-PRS.[3SG]</ta>
            <ta e="T131" id="Seg_6492" s="T130">вот</ta>
            <ta e="T132" id="Seg_6493" s="T131">этот</ta>
            <ta e="T133" id="Seg_6494" s="T132">старик.[NOM]</ta>
            <ta e="T134" id="Seg_6495" s="T133">Бегичев.[NOM]</ta>
            <ta e="T135" id="Seg_6496" s="T134">однако</ta>
            <ta e="T136" id="Seg_6497" s="T135">тот</ta>
            <ta e="T137" id="Seg_6498" s="T136">тот</ta>
            <ta e="T138" id="Seg_6499" s="T137">остров-DAT/LOC</ta>
            <ta e="T139" id="Seg_6500" s="T138">доезжать-PST2-EP-1SG</ta>
            <ta e="T140" id="Seg_6501" s="T139">говорить-CVB.SEQ</ta>
            <ta e="T141" id="Seg_6502" s="T140">рассказывать-PRS.[3SG]</ta>
            <ta e="T142" id="Seg_6503" s="T141">говорят</ta>
            <ta e="T143" id="Seg_6504" s="T142">этот</ta>
            <ta e="T144" id="Seg_6505" s="T143">что</ta>
            <ta e="T145" id="Seg_6506" s="T144">чудо-ACC</ta>
            <ta e="T146" id="Seg_6507" s="T145">видеть-PST1-2SG</ta>
            <ta e="T147" id="Seg_6508" s="T146">там</ta>
            <ta e="T148" id="Seg_6509" s="T147">говорить-CVB.SEQ</ta>
            <ta e="T149" id="Seg_6510" s="T148">спрашивать-PRS-3PL</ta>
            <ta e="T150" id="Seg_6511" s="T149">долганин-PL.[NOM]</ta>
            <ta e="T151" id="Seg_6512" s="T150">EMPH</ta>
            <ta e="T152" id="Seg_6513" s="T151">EMPH</ta>
            <ta e="T153" id="Seg_6514" s="T152">тот-DAT/LOC</ta>
            <ta e="T154" id="Seg_6515" s="T153">1SG.[NOM]</ta>
            <ta e="T155" id="Seg_6516" s="T154">чудо-ACC</ta>
            <ta e="T156" id="Seg_6517" s="T155">что-ACC</ta>
            <ta e="T157" id="Seg_6518" s="T156">NEG</ta>
            <ta e="T158" id="Seg_6519" s="T157">очень</ta>
            <ta e="T159" id="Seg_6520" s="T158">найти-NEG-PST1-1SG</ta>
            <ta e="T160" id="Seg_6521" s="T159">морж-3PL.[NOM]</ta>
            <ta e="T161" id="Seg_6522" s="T160">однако</ta>
            <ta e="T162" id="Seg_6523" s="T161">зуб-3PL-ABL</ta>
            <ta e="T163" id="Seg_6524" s="T162">связывать-CVB.SEQ</ta>
            <ta e="T164" id="Seg_6525" s="T163">после</ta>
            <ta e="T165" id="Seg_6526" s="T164">спать-PRS-3PL</ta>
            <ta e="T166" id="Seg_6527" s="T165">спать-CVB.SIM</ta>
            <ta e="T167" id="Seg_6528" s="T166">лежать-PTCP.PRS-3PL-ACC</ta>
            <ta e="T168" id="Seg_6529" s="T167">связывать-CVB.SEQ</ta>
            <ta e="T169" id="Seg_6530" s="T168">бревно-ABL</ta>
            <ta e="T170" id="Seg_6531" s="T169">тот-ACC</ta>
            <ta e="T171" id="Seg_6532" s="T170">вещь.[NOM]</ta>
            <ta e="T172" id="Seg_6533" s="T171">NEG</ta>
            <ta e="T173" id="Seg_6534" s="T172">думать-NEG-3PL</ta>
            <ta e="T174" id="Seg_6535" s="T173">тащить-CVB.SEQ</ta>
            <ta e="T175" id="Seg_6536" s="T174">входить-CAUS-PRS-3PL</ta>
            <ta e="T176" id="Seg_6537" s="T175">вода-DAT/LOC</ta>
            <ta e="T177" id="Seg_6538" s="T176">входить-CAUS-CVB.SEQ</ta>
            <ta e="T178" id="Seg_6539" s="T177">бросать-PRS-3PL</ta>
            <ta e="T179" id="Seg_6540" s="T178">потом</ta>
            <ta e="T180" id="Seg_6541" s="T179">только</ta>
            <ta e="T181" id="Seg_6542" s="T180">остров-DAT/LOC</ta>
            <ta e="T182" id="Seg_6543" s="T181">камень.[NOM]</ta>
            <ta e="T183" id="Seg_6544" s="T182">гора-DAT/LOC</ta>
            <ta e="T184" id="Seg_6545" s="T183">найти-PST1-1SG</ta>
            <ta e="T185" id="Seg_6546" s="T184">говорить-PRS.[3SG]</ta>
            <ta e="T186" id="Seg_6547" s="T185">говорят</ta>
            <ta e="T187" id="Seg_6548" s="T186">один</ta>
            <ta e="T188" id="Seg_6549" s="T187">чудо-ACC</ta>
            <ta e="T189" id="Seg_6550" s="T188">вот</ta>
            <ta e="T190" id="Seg_6551" s="T189">что-ACC</ta>
            <ta e="T191" id="Seg_6552" s="T190">найти-PST1-2SG</ta>
            <ta e="T192" id="Seg_6553" s="T191">тот-ACC</ta>
            <ta e="T193" id="Seg_6554" s="T192">говорить-PRS.[3SG]</ta>
            <ta e="T194" id="Seg_6555" s="T193">EMPH</ta>
            <ta e="T210" id="Seg_6556" s="T209">тот-DAT/LOC</ta>
            <ta e="T211" id="Seg_6557" s="T210">входить-CVB.SEQ</ta>
            <ta e="T212" id="Seg_6558" s="T211">идти-PST1-1SG</ta>
            <ta e="T213" id="Seg_6559" s="T212">говорить-PRS.[3SG]</ta>
            <ta e="T214" id="Seg_6560" s="T213">три</ta>
            <ta e="T215" id="Seg_6561" s="T214">человек-PROPR.[NOM]</ta>
            <ta e="T216" id="Seg_6562" s="T215">этот</ta>
            <ta e="T217" id="Seg_6563" s="T216">человек-PL.[NOM]</ta>
            <ta e="T218" id="Seg_6564" s="T217">пища-POSS</ta>
            <ta e="T219" id="Seg_6565" s="T218">NEG</ta>
            <ta e="T220" id="Seg_6566" s="T219">становиться-PST2-1PL</ta>
            <ta e="T221" id="Seg_6567" s="T220">тогда</ta>
            <ta e="T222" id="Seg_6568" s="T221">однако</ta>
            <ta e="T223" id="Seg_6569" s="T222">залив-ABL</ta>
            <ta e="T224" id="Seg_6570" s="T223">о</ta>
            <ta e="T225" id="Seg_6571" s="T224">маленький</ta>
            <ta e="T226" id="Seg_6572" s="T225">рыба-DIM-ACC</ta>
            <ta e="T227" id="Seg_6573" s="T226">поймать-EP-MED-CVB.SEQ-3PL</ta>
            <ta e="T228" id="Seg_6574" s="T227">тот</ta>
            <ta e="T229" id="Seg_6575" s="T228">дверь-DAT/LOC</ta>
            <ta e="T230" id="Seg_6576" s="T229">входить-CVB.SEQ-3PL</ta>
            <ta e="T231" id="Seg_6577" s="T230">есть-CVB.PURP-3PL</ta>
            <ta e="T232" id="Seg_6578" s="T231">входить-PST2-3PL</ta>
            <ta e="T233" id="Seg_6579" s="T232">этот</ta>
            <ta e="T234" id="Seg_6580" s="T233">человек.[NOM]</ta>
            <ta e="T235" id="Seg_6581" s="T234">только</ta>
            <ta e="T236" id="Seg_6582" s="T235">входить-PST2-3SG</ta>
            <ta e="T237" id="Seg_6583" s="T236">этот</ta>
            <ta e="T238" id="Seg_6584" s="T237">человек.[NOM]</ta>
            <ta e="T239" id="Seg_6585" s="T238">сидеть-PRS.[3SG]</ta>
            <ta e="T240" id="Seg_6586" s="T239">сидеть-TEMP-3SG</ta>
            <ta e="T241" id="Seg_6587" s="T240">что.[NOM]</ta>
            <ta e="T242" id="Seg_6588" s="T241">INDEF</ta>
            <ta e="T243" id="Seg_6589" s="T242">человек-3SG.[NOM]</ta>
            <ta e="T244" id="Seg_6590" s="T243">приходить-PST2.[3SG]</ta>
            <ta e="T245" id="Seg_6591" s="T244">EMPH</ta>
            <ta e="T246" id="Seg_6592" s="T245">EMPH</ta>
            <ta e="T247" id="Seg_6593" s="T246">человек.[NOM]</ta>
            <ta e="T248" id="Seg_6594" s="T247">Q</ta>
            <ta e="T249" id="Seg_6595" s="T248">что.[NOM]</ta>
            <ta e="T250" id="Seg_6596" s="T249">Q</ta>
            <ta e="T251" id="Seg_6597" s="T250">плечо-ABL</ta>
            <ta e="T252" id="Seg_6598" s="T251">голова-POSS</ta>
            <ta e="T253" id="Seg_6599" s="T252">NEG</ta>
            <ta e="T254" id="Seg_6600" s="T253">человек.[NOM]</ta>
            <ta e="T255" id="Seg_6601" s="T254">входить-PST2.[3SG]</ta>
            <ta e="T256" id="Seg_6602" s="T255">человек.[NOM]</ta>
            <ta e="T257" id="Seg_6603" s="T256">Q</ta>
            <ta e="T258" id="Seg_6604" s="T257">что.[NOM]</ta>
            <ta e="T259" id="Seg_6605" s="T258">Q</ta>
            <ta e="T260" id="Seg_6606" s="T259">что-ACC</ta>
            <ta e="T261" id="Seg_6607" s="T260">NEG</ta>
            <ta e="T262" id="Seg_6608" s="T261">понимать-NEG-1SG</ta>
            <ta e="T263" id="Seg_6609" s="T262">говорить-PST2.[3SG]</ta>
            <ta e="T264" id="Seg_6610" s="T263">тот</ta>
            <ta e="T265" id="Seg_6611" s="T264">входить-CVB.SEQ</ta>
            <ta e="T266" id="Seg_6612" s="T265">дверь-DAT/LOC</ta>
            <ta e="T267" id="Seg_6613" s="T266">сесть-PST2.[3SG]</ta>
            <ta e="T268" id="Seg_6614" s="T267">EMPH</ta>
            <ta e="T269" id="Seg_6615" s="T268">EMPH</ta>
            <ta e="T270" id="Seg_6616" s="T269">тот</ta>
            <ta e="T271" id="Seg_6617" s="T270">сидеть-CVB.SEQ</ta>
            <ta e="T272" id="Seg_6618" s="T271">однако</ta>
            <ta e="T273" id="Seg_6619" s="T272">ой</ta>
            <ta e="T274" id="Seg_6620" s="T273">смеяться-CVB.SEQ</ta>
            <ta e="T275" id="Seg_6621" s="T274">оскалиться-PST2.[3SG]</ta>
            <ta e="T276" id="Seg_6622" s="T275">где</ta>
            <ta e="T277" id="Seg_6623" s="T276">INDEF</ta>
            <ta e="T278" id="Seg_6624" s="T277">глаз-PROPR.[NOM]</ta>
            <ta e="T279" id="Seg_6625" s="T278">голова-3SG-DAT/LOC</ta>
            <ta e="T280" id="Seg_6626" s="T279">одинокий</ta>
            <ta e="T281" id="Seg_6627" s="T280">глаз-PROPR.[NOM]</ta>
            <ta e="T282" id="Seg_6628" s="T281">эй</ta>
            <ta e="T285" id="Seg_6629" s="T284">друг-PL.[NOM]</ta>
            <ta e="T286" id="Seg_6630" s="T285">крепкий-ADVZ</ta>
            <ta e="T287" id="Seg_6631" s="T286">держать-REFL-EP-IMP.2PL</ta>
            <ta e="T288" id="Seg_6632" s="T287">говорить-PRS.[3SG]</ta>
            <ta e="T289" id="Seg_6633" s="T288">EMPH</ta>
            <ta e="T290" id="Seg_6634" s="T289">друг-3SG-ACC</ta>
            <ta e="T291" id="Seg_6635" s="T290">мальчик-PL-3SG-ACC</ta>
            <ta e="T292" id="Seg_6636" s="T291">Бегичев.[NOM]</ta>
            <ta e="T293" id="Seg_6637" s="T292">тот-3SG-3SG.[NOM]</ta>
            <ta e="T294" id="Seg_6638" s="T293">уп</ta>
            <ta e="T295" id="Seg_6639" s="T294">говорить-CVB.SEQ</ta>
            <ta e="T296" id="Seg_6640" s="T295">сосать-PST2.[3SG]</ta>
            <ta e="T297" id="Seg_6641" s="T296">два</ta>
            <ta e="T298" id="Seg_6642" s="T297">человек-3SG-ACC</ta>
            <ta e="T299" id="Seg_6643" s="T298">сосать-CVB.SEQ</ta>
            <ta e="T300" id="Seg_6644" s="T299">взять-PST2.[3SG]</ta>
            <ta e="T301" id="Seg_6645" s="T300">тогда</ta>
            <ta e="T302" id="Seg_6646" s="T301">этот</ta>
            <ta e="T303" id="Seg_6647" s="T302">человек.[NOM]</ta>
            <ta e="T304" id="Seg_6648" s="T303">одинокий</ta>
            <ta e="T305" id="Seg_6649" s="T304">человек-PROPR.[NOM]</ta>
            <ta e="T306" id="Seg_6650" s="T305">оставаться-PST2.[3SG]</ta>
            <ta e="T307" id="Seg_6651" s="T306">там</ta>
            <ta e="T308" id="Seg_6652" s="T307">этот</ta>
            <ta e="T309" id="Seg_6653" s="T308">человек.[NOM]</ta>
            <ta e="T310" id="Seg_6654" s="T309">MOD</ta>
            <ta e="T311" id="Seg_6655" s="T310">пистолет-PROPR.[NOM]</ta>
            <ta e="T312" id="Seg_6656" s="T311">быть-PST2.[3SG]</ta>
            <ta e="T313" id="Seg_6657" s="T312">тот</ta>
            <ta e="T314" id="Seg_6658" s="T313">там</ta>
            <ta e="T315" id="Seg_6659" s="T314">говорить-PRS-1SG</ta>
            <ta e="T316" id="Seg_6660" s="T315">говорить-PTCP.PRS</ta>
            <ta e="T317" id="Seg_6661" s="T316">язык-EP-INSTR</ta>
            <ta e="T318" id="Seg_6662" s="T317">говорить-PRS-1SG</ta>
            <ta e="T319" id="Seg_6663" s="T318">говорить-PST2.[3SG]</ta>
            <ta e="T320" id="Seg_6664" s="T319">пистолет-EP-1SG.[NOM]</ta>
            <ta e="T321" id="Seg_6665" s="T320">рот-3SG-DAT/LOC</ta>
            <ta e="T322" id="Seg_6666" s="T321">вот</ta>
            <ta e="T323" id="Seg_6667" s="T322">этот</ta>
            <ta e="T324" id="Seg_6668" s="T323">страна-DAT/LOC</ta>
            <ta e="T325" id="Seg_6669" s="T324">оставаться-PRS-1SG</ta>
            <ta e="T326" id="Seg_6670" s="T325">Q</ta>
            <ta e="T327" id="Seg_6671" s="T326">1SG.[NOM]</ta>
            <ta e="T328" id="Seg_6672" s="T327">1SG-ACC</ta>
            <ta e="T329" id="Seg_6673" s="T328">спасать.[IMP.2SG]</ta>
            <ta e="T330" id="Seg_6674" s="T329">этот-ACC</ta>
            <ta e="T331" id="Seg_6675" s="T330">куда</ta>
            <ta e="T332" id="Seg_6676" s="T331">INDEF</ta>
            <ta e="T333" id="Seg_6677" s="T332">падать.в.обморок-EP-CAUS-PTCP.PRS</ta>
            <ta e="T334" id="Seg_6678" s="T333">место-2SG-DAT/LOC</ta>
            <ta e="T335" id="Seg_6679" s="T334">доводить.[IMP.2SG]</ta>
            <ta e="T336" id="Seg_6680" s="T335">говорить-PRS.[3SG]</ta>
            <ta e="T337" id="Seg_6681" s="T336">лоб-3SG-DAT/LOC</ta>
            <ta e="T338" id="Seg_6682" s="T337">стрелять-PST2.[3SG]</ta>
            <ta e="T339" id="Seg_6683" s="T338">MOD</ta>
            <ta e="T340" id="Seg_6684" s="T339">этот</ta>
            <ta e="T341" id="Seg_6685" s="T340">пистолет-3SG-INSTR</ta>
            <ta e="T342" id="Seg_6686" s="T341">тот-3SG-3SG.[NOM]</ta>
            <ta e="T343" id="Seg_6687" s="T342">вот</ta>
            <ta e="T344" id="Seg_6688" s="T343">падать.в.обморок-CVB.SEQ</ta>
            <ta e="T345" id="Seg_6689" s="T344">падать-PST2.[3SG]</ta>
            <ta e="T346" id="Seg_6690" s="T345">тот</ta>
            <ta e="T347" id="Seg_6691" s="T346">падать.в.обморок-CVB.SEQ</ta>
            <ta e="T348" id="Seg_6692" s="T347">падать-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T349" id="Seg_6693" s="T348">верхняя.часть-EP-INSTR</ta>
            <ta e="T350" id="Seg_6694" s="T349">бегать-CVB.SEQ</ta>
            <ta e="T351" id="Seg_6695" s="T350">выйти-EP-PST2-3PL</ta>
            <ta e="T352" id="Seg_6696" s="T351">быть-COND.[3SG]</ta>
            <ta e="T353" id="Seg_6697" s="T352">Бегичев-EP-2SG.[NOM]</ta>
            <ta e="T354" id="Seg_6698" s="T353">рассказывать-PRS.[3SG]</ta>
            <ta e="T357" id="Seg_6699" s="T356">тот</ta>
            <ta e="T358" id="Seg_6700" s="T357">доезжать-CVB.SEQ-1PL</ta>
            <ta e="T359" id="Seg_6701" s="T358">кто-DAT/LOC</ta>
            <ta e="T360" id="Seg_6702" s="T359">идти-PST1-1PL</ta>
            <ta e="T361" id="Seg_6703" s="T360">говорить-PRS.[3SG]</ta>
            <ta e="T362" id="Seg_6704" s="T361">EMPH</ta>
            <ta e="T363" id="Seg_6705" s="T362">катер-1SG-DAT/LOC</ta>
            <ta e="T364" id="Seg_6706" s="T363">идти-PST1-1SG</ta>
            <ta e="T365" id="Seg_6707" s="T364">говорить-PRS.[3SG]</ta>
            <ta e="T366" id="Seg_6708" s="T365">катер-1SG-DAT/LOC</ta>
            <ta e="T367" id="Seg_6709" s="T366">сесть-CVB.SEQ-1SG</ta>
            <ta e="T368" id="Seg_6710" s="T367">залив.[NOM]</ta>
            <ta e="T369" id="Seg_6711" s="T368">середина-3SG-DAT/LOC</ta>
            <ta e="T370" id="Seg_6712" s="T369">входить-PST1-1SG</ta>
            <ta e="T371" id="Seg_6713" s="T370">видеть-PST2-EP-1SG</ta>
            <ta e="T372" id="Seg_6714" s="T371">задняя.часть-1SG-ABL</ta>
            <ta e="T373" id="Seg_6715" s="T372">давешний-EP-1SG.[NOM]</ta>
            <ta e="T374" id="Seg_6716" s="T373">вставать-CVB.SEQ</ta>
            <ta e="T375" id="Seg_6717" s="T374">приходить-PST1-3SG</ta>
            <ta e="T376" id="Seg_6718" s="T375">говорить-CVB.SEQ</ta>
            <ta e="T377" id="Seg_6719" s="T376">вставать-CVB.SEQ</ta>
            <ta e="T378" id="Seg_6720" s="T377">приходить-CVB.SEQ</ta>
            <ta e="T379" id="Seg_6721" s="T378">однако</ta>
            <ta e="T380" id="Seg_6722" s="T379">катер-COMP</ta>
            <ta e="T381" id="Seg_6723" s="T380">EMPH</ta>
            <ta e="T382" id="Seg_6724" s="T381">большой</ta>
            <ta e="T383" id="Seg_6725" s="T382">валун-ACC</ta>
            <ta e="T384" id="Seg_6726" s="T383">взять-CVB.SEQ</ta>
            <ta e="T385" id="Seg_6727" s="T384">после</ta>
            <ta e="T386" id="Seg_6728" s="T385">вот</ta>
            <ta e="T387" id="Seg_6729" s="T386">бросать-PST2.[3SG]</ta>
            <ta e="T388" id="Seg_6730" s="T387">кто.[NOM]</ta>
            <ta e="T389" id="Seg_6731" s="T388">вот</ta>
            <ta e="T390" id="Seg_6732" s="T389">стрелять-PASS-PTCP.HAB-3SG.[NOM]</ta>
            <ta e="T391" id="Seg_6733" s="T390">тот</ta>
            <ta e="T392" id="Seg_6734" s="T391">бросать-PTCP.PST-3SG-DAT/LOC</ta>
            <ta e="T393" id="Seg_6735" s="T392">EMPH</ta>
            <ta e="T394" id="Seg_6736" s="T393">еле</ta>
            <ta e="T395" id="Seg_6737" s="T394">залив.[NOM]</ta>
            <ta e="T396" id="Seg_6738" s="T395">середина-3SG-DAT/LOC</ta>
            <ta e="T397" id="Seg_6739" s="T396">доезжать-PST2-EP-1SG</ta>
            <ta e="T398" id="Seg_6740" s="T397">доезжать-PTCP.PRS-3SG-ACC</ta>
            <ta e="T399" id="Seg_6741" s="T398">с</ta>
            <ta e="T400" id="Seg_6742" s="T399">MOD</ta>
            <ta e="T401" id="Seg_6743" s="T400">тот</ta>
            <ta e="T402" id="Seg_6744" s="T401">волна-3SG-DAT/LOC</ta>
            <ta e="T403" id="Seg_6745" s="T402">бросать-PTCP.PST</ta>
            <ta e="T404" id="Seg_6746" s="T403">волна-3SG-DAT/LOC</ta>
            <ta e="T405" id="Seg_6747" s="T404">чуть</ta>
            <ta e="T406" id="Seg_6748" s="T405">вырывать-CVB.SIM</ta>
            <ta e="T407" id="Seg_6749" s="T406">идти-CVB.SIM</ta>
            <ta e="T408" id="Seg_6750" s="T407">почти.делать-PST2.[3SG]</ta>
            <ta e="T409" id="Seg_6751" s="T408">тот</ta>
            <ta e="T410" id="Seg_6752" s="T409">спасаться-PST1-1PL</ta>
            <ta e="T411" id="Seg_6753" s="T410">говорить-PST2.[3SG]</ta>
            <ta e="T412" id="Seg_6754" s="T411">MOD</ta>
            <ta e="T413" id="Seg_6755" s="T412">тогда</ta>
            <ta e="T414" id="Seg_6756" s="T413">семь</ta>
            <ta e="T415" id="Seg_6757" s="T414">человек-ABL</ta>
            <ta e="T416" id="Seg_6758" s="T415">одинокий</ta>
            <ta e="T417" id="Seg_6759" s="T416">человек-PROPR</ta>
            <ta e="T418" id="Seg_6760" s="T417">приходить-PST2.[3SG]</ta>
            <ta e="T419" id="Seg_6761" s="T418">голодать-CVB.SEQ-3PL</ta>
            <ta e="T420" id="Seg_6762" s="T419">сам-3PL.[NOM]</ta>
            <ta e="T421" id="Seg_6763" s="T420">что-ACC</ta>
            <ta e="T422" id="Seg_6764" s="T421">NEG</ta>
            <ta e="T423" id="Seg_6765" s="T422">найти-NEG.CVB.SIM</ta>
            <ta e="T424" id="Seg_6766" s="T423">вертеться-CVB.SEQ</ta>
            <ta e="T425" id="Seg_6767" s="T424">приходить</ta>
            <ta e="T426" id="Seg_6768" s="T425">тот</ta>
            <ta e="T427" id="Seg_6769" s="T426">приходить-CVB.SEQ</ta>
            <ta e="T428" id="Seg_6770" s="T427">тот</ta>
            <ta e="T429" id="Seg_6771" s="T428">вверх</ta>
            <ta e="T430" id="Seg_6772" s="T429">к</ta>
            <ta e="T431" id="Seg_6773" s="T430">идти-PST2.[3SG]</ta>
            <ta e="T432" id="Seg_6774" s="T431">потом</ta>
            <ta e="T433" id="Seg_6775" s="T432">Чагыдай-ACC</ta>
            <ta e="T434" id="Seg_6776" s="T433">однако</ta>
            <ta e="T435" id="Seg_6777" s="T434">летом</ta>
            <ta e="T436" id="Seg_6778" s="T435">вместе</ta>
            <ta e="T437" id="Seg_6779" s="T436">жить-PRS-3PL</ta>
            <ta e="T438" id="Seg_6780" s="T437">говорят</ta>
            <ta e="T439" id="Seg_6781" s="T438">есть-CVB.SEQ</ta>
            <ta e="T440" id="Seg_6782" s="T439">жить-CVB.SEQ-3PL</ta>
            <ta e="T441" id="Seg_6783" s="T440">вот</ta>
            <ta e="T442" id="Seg_6784" s="T441">письмо.[NOM]</ta>
            <ta e="T443" id="Seg_6785" s="T442">приходить-PST2.[3SG]</ta>
            <ta e="T444" id="Seg_6786" s="T443">этот</ta>
            <ta e="T445" id="Seg_6787" s="T444">письмо-ACC</ta>
            <ta e="T446" id="Seg_6788" s="T445">однако</ta>
            <ta e="T447" id="Seg_6789" s="T446">Хатанга-DAT/LOC</ta>
            <ta e="T448" id="Seg_6790" s="T447">доводить-PTCP.FUT-3PL-ACC</ta>
            <ta e="T449" id="Seg_6791" s="T448">надо</ta>
            <ta e="T450" id="Seg_6792" s="T449">откуда</ta>
            <ta e="T451" id="Seg_6793" s="T450">INDEF</ta>
            <ta e="T452" id="Seg_6794" s="T451">приходить-PST2.[3SG]</ta>
            <ta e="T453" id="Seg_6795" s="T452">лодочка-INSTR</ta>
            <ta e="T454" id="Seg_6796" s="T453">тогда</ta>
            <ta e="T455" id="Seg_6797" s="T454">что.[NOM]</ta>
            <ta e="T456" id="Seg_6798" s="T455">лодка-3SG.[NOM]</ta>
            <ta e="T457" id="Seg_6799" s="T456">приходить-FUT.[3SG]=Q</ta>
            <ta e="T458" id="Seg_6800" s="T457">вот</ta>
            <ta e="T459" id="Seg_6801" s="T458">Чагыдай</ta>
            <ta e="T460" id="Seg_6802" s="T459">этот</ta>
            <ta e="T461" id="Seg_6803" s="T460">письмо-ACC</ta>
            <ta e="T462" id="Seg_6804" s="T461">MOD</ta>
            <ta e="T463" id="Seg_6805" s="T462">кто-DAT/LOC</ta>
            <ta e="T464" id="Seg_6806" s="T463">доводить.[IMP.2SG]</ta>
            <ta e="T465" id="Seg_6807" s="T464">Кресты-DAT/LOC</ta>
            <ta e="T466" id="Seg_6808" s="T465">Новорыбное</ta>
            <ta e="T467" id="Seg_6809" s="T466">Кресты-3SG-DAT/LOC</ta>
            <ta e="T468" id="Seg_6810" s="T467">видеть.[IMP.2SG]</ta>
            <ta e="T469" id="Seg_6811" s="T468">далекий-ADVZ</ta>
            <ta e="T470" id="Seg_6812" s="T469">Новорыбное.[NOM]</ta>
            <ta e="T471" id="Seg_6813" s="T470">далекий</ta>
            <ta e="T472" id="Seg_6814" s="T471">сторона-3SG-DAT/LOC</ta>
            <ta e="T478" id="Seg_6815" s="T476">нет</ta>
            <ta e="T479" id="Seg_6816" s="T478">тот</ta>
            <ta e="T480" id="Seg_6817" s="T479">сам-PL-3SG-GEN</ta>
            <ta e="T482" id="Seg_6818" s="T480">собственный-3PL.[NOM]</ta>
            <ta e="T485" id="Seg_6819" s="T482">AFFIRM</ta>
            <ta e="T486" id="Seg_6820" s="T485">Бегичев-3PL.[NOM]</ta>
            <ta e="T487" id="Seg_6821" s="T486">EMPH-вот</ta>
            <ta e="T488" id="Seg_6822" s="T487">путешествовать-PTCP.PST</ta>
            <ta e="T489" id="Seg_6823" s="T488">быть-PST1-3SG</ta>
            <ta e="T490" id="Seg_6824" s="T489">тот</ta>
            <ta e="T491" id="Seg_6825" s="T490">идти-PTCP.PST-3SG-INSTR</ta>
            <ta e="T492" id="Seg_6826" s="T491">идти-PST2.[3SG]</ta>
            <ta e="T493" id="Seg_6827" s="T492">этот</ta>
            <ta e="T494" id="Seg_6828" s="T493">к</ta>
            <ta e="T495" id="Seg_6829" s="T494">вверх</ta>
            <ta e="T496" id="Seg_6830" s="T495">к</ta>
            <ta e="T497" id="Seg_6831" s="T496">вот</ta>
            <ta e="T498" id="Seg_6832" s="T497">тот</ta>
            <ta e="T499" id="Seg_6833" s="T498">EMPH</ta>
            <ta e="T500" id="Seg_6834" s="T499">тот</ta>
            <ta e="T501" id="Seg_6835" s="T500">письмо-ACC</ta>
            <ta e="T502" id="Seg_6836" s="T501">приносить-EP-CAUS-PRS-3PL</ta>
            <ta e="T503" id="Seg_6837" s="T502">тогда</ta>
            <ta e="T504" id="Seg_6838" s="T503">письмо-PROPR-3PL</ta>
            <ta e="T505" id="Seg_6839" s="T504">быть-PST2-3SG</ta>
            <ta e="T506" id="Seg_6840" s="T505">MOD</ta>
            <ta e="T507" id="Seg_6841" s="T506">господин-PROPR-3PL</ta>
            <ta e="T508" id="Seg_6842" s="T507">EMPH</ta>
            <ta e="T512" id="Seg_6843" s="T511">долганин-PL.[NOM]</ta>
            <ta e="T513" id="Seg_6844" s="T512">тот</ta>
            <ta e="T514" id="Seg_6845" s="T513">работник-3PL.[NOM]</ta>
            <ta e="T515" id="Seg_6846" s="T514">совет-3PL.[NOM]</ta>
            <ta e="T516" id="Seg_6847" s="T515">что-3PL.[NOM]</ta>
            <ta e="T517" id="Seg_6848" s="T516">письмо.[NOM]</ta>
            <ta e="T518" id="Seg_6849" s="T517">послать-PRS-3PL</ta>
            <ta e="T519" id="Seg_6850" s="T518">EMPH</ta>
            <ta e="T520" id="Seg_6851" s="T519">так</ta>
            <ta e="T521" id="Seg_6852" s="T520">делать</ta>
            <ta e="T522" id="Seg_6853" s="T521">этот</ta>
            <ta e="T523" id="Seg_6854" s="T522">старик.[NOM]</ta>
            <ta e="T524" id="Seg_6855" s="T523">MOD</ta>
            <ta e="T525" id="Seg_6856" s="T524">вот</ta>
            <ta e="T526" id="Seg_6857" s="T525">вечером</ta>
            <ta e="T527" id="Seg_6858" s="T526">отправляться-PRS.[3SG]</ta>
            <ta e="T528" id="Seg_6859" s="T527">утром</ta>
            <ta e="T529" id="Seg_6860" s="T528">рано-AG</ta>
            <ta e="T530" id="Seg_6861" s="T529">жена.[NOM]</ta>
            <ta e="T531" id="Seg_6862" s="T530">вставать-PTCP.PRS</ta>
            <ta e="T532" id="Seg_6863" s="T531">час-3SG-DAT/LOC</ta>
            <ta e="T533" id="Seg_6864" s="T532">приходить-FUT-1SG</ta>
            <ta e="T534" id="Seg_6865" s="T533">говорить-PST2.[3SG]</ta>
            <ta e="T535" id="Seg_6866" s="T534">этот</ta>
            <ta e="T536" id="Seg_6867" s="T535">старик.[NOM]</ta>
            <ta e="T537" id="Seg_6868" s="T536">о</ta>
            <ta e="T538" id="Seg_6869" s="T537">как</ta>
            <ta e="T539" id="Seg_6870" s="T538">делать-CVB.SEQ</ta>
            <ta e="T540" id="Seg_6871" s="T539">приходить-CVB.PURP</ta>
            <ta e="T541" id="Seg_6872" s="T540">делать-PRS-2SG</ta>
            <ta e="T542" id="Seg_6873" s="T541">Новорыбное.[NOM]</ta>
            <ta e="T543" id="Seg_6874" s="T542">далекий</ta>
            <ta e="T544" id="Seg_6875" s="T543">сторона-3SG-ABL</ta>
            <ta e="T545" id="Seg_6876" s="T544">очень</ta>
            <ta e="T546" id="Seg_6877" s="T545">далекий.[NOM]</ta>
            <ta e="T547" id="Seg_6878" s="T546">Попигай-ABL</ta>
            <ta e="T548" id="Seg_6879" s="T547">вот</ta>
            <ta e="T549" id="Seg_6880" s="T548">этот</ta>
            <ta e="T550" id="Seg_6881" s="T549">старик.[NOM]</ta>
            <ta e="T551" id="Seg_6882" s="T550">идти-CVB.SEQ</ta>
            <ta e="T552" id="Seg_6883" s="T551">оставаться-PST2.[3SG]</ta>
            <ta e="T553" id="Seg_6884" s="T552">э</ta>
            <ta e="T554" id="Seg_6885" s="T553">идти-CVB.SEQ</ta>
            <ta e="T555" id="Seg_6886" s="T554">оставаться-PST2.[3SG]</ta>
            <ta e="T556" id="Seg_6887" s="T555">лодочка-INSTR</ta>
            <ta e="T557" id="Seg_6888" s="T556">утром</ta>
            <ta e="T558" id="Seg_6889" s="T557">старуха-PL.[NOM]</ta>
            <ta e="T561" id="Seg_6890" s="T560">рано-AG</ta>
            <ta e="T562" id="Seg_6891" s="T561">жена-PL.[NOM]</ta>
            <ta e="T563" id="Seg_6892" s="T562">вставать-PRS-3PL</ta>
            <ta e="T564" id="Seg_6893" s="T563">например</ta>
            <ta e="T565" id="Seg_6894" s="T564">тогда</ta>
            <ta e="T566" id="Seg_6895" s="T565">семь</ta>
            <ta e="T567" id="Seg_6896" s="T566">час.[NOM]</ta>
            <ta e="T568" id="Seg_6897" s="T567">EMPH</ta>
            <ta e="T569" id="Seg_6898" s="T568">быть-PST2.[3SG]</ta>
            <ta e="T570" id="Seg_6899" s="T569">очевидно</ta>
            <ta e="T571" id="Seg_6900" s="T570">что.[NOM]</ta>
            <ta e="T572" id="Seg_6901" s="T571">час-3SG.[NOM]</ta>
            <ta e="T573" id="Seg_6902" s="T572">приходить-FUT.[3SG]=Q</ta>
            <ta e="T574" id="Seg_6903" s="T573">солнце-3PL-INSTR</ta>
            <ta e="T575" id="Seg_6904" s="T574">видеть-PRS-3PL</ta>
            <ta e="T576" id="Seg_6905" s="T575">EMPH</ta>
            <ta e="T577" id="Seg_6906" s="T576">вот.беда</ta>
            <ta e="T578" id="Seg_6907" s="T577">семь</ta>
            <ta e="T579" id="Seg_6908" s="T578">час-DAT/LOC</ta>
            <ta e="T580" id="Seg_6909" s="T579">Чагыдай.[NOM]</ta>
            <ta e="T581" id="Seg_6910" s="T580">приходить-PST2.[3SG]</ta>
            <ta e="T582" id="Seg_6911" s="T581">лодочка-3SG.[NOM]</ta>
            <ta e="T583" id="Seg_6912" s="T582">нырять-EP-REFL-CVB.SIM</ta>
            <ta e="T584" id="Seg_6913" s="T583">лежать-PRS.[3SG]</ta>
            <ta e="T585" id="Seg_6914" s="T584">EMPH</ta>
            <ta e="T586" id="Seg_6915" s="T585">видеть.[IMP.2SG]</ta>
            <ta e="T587" id="Seg_6916" s="T586">такой</ta>
            <ta e="T588" id="Seg_6917" s="T587">отважный</ta>
            <ta e="T589" id="Seg_6918" s="T588">человек-PL.[NOM]</ta>
            <ta e="T590" id="Seg_6919" s="T589">есть-3PL</ta>
            <ta e="T591" id="Seg_6920" s="T590">потом</ta>
            <ta e="T592" id="Seg_6921" s="T591">жена.[NOM]</ta>
            <ta e="T593" id="Seg_6922" s="T592">родить-PRS.[3SG]</ta>
            <ta e="T594" id="Seg_6923" s="T593">тот.EMPH.[NOM]</ta>
            <ta e="T595" id="Seg_6924" s="T594">стоянка-3PL-DAT/LOC</ta>
            <ta e="T596" id="Seg_6925" s="T595">этот</ta>
            <ta e="T597" id="Seg_6926" s="T596">жена.[NOM]</ta>
            <ta e="T598" id="Seg_6927" s="T597">умирать-CVB.PURP</ta>
            <ta e="T599" id="Seg_6928" s="T598">делать-PST2.[3SG]</ta>
            <ta e="T600" id="Seg_6929" s="T599">как</ta>
            <ta e="T601" id="Seg_6930" s="T600">NEG</ta>
            <ta e="T602" id="Seg_6931" s="T601">делать-PTCP.FUT-3PL-ACC</ta>
            <ta e="T603" id="Seg_6932" s="T602">очень</ta>
            <ta e="T604" id="Seg_6933" s="T603">доктор-POSS</ta>
            <ta e="T605" id="Seg_6934" s="T604">NEG</ta>
            <ta e="T606" id="Seg_6935" s="T605">время-DAT/LOC</ta>
            <ta e="T607" id="Seg_6936" s="T606">вот</ta>
            <ta e="T608" id="Seg_6937" s="T607">повитуха-3PL.[NOM]</ta>
            <ta e="T609" id="Seg_6938" s="T608">беда-PROPR</ta>
            <ta e="T610" id="Seg_6939" s="T609">устать-CVB.SEQ</ta>
            <ta e="T611" id="Seg_6940" s="T610">кончать-PST2.[3SG]</ta>
            <ta e="T612" id="Seg_6941" s="T611">эй</ta>
            <ta e="T613" id="Seg_6942" s="T612">Чагыдай.[NOM]</ta>
            <ta e="T614" id="Seg_6943" s="T613">приходить-IMP.3SG</ta>
            <ta e="T615" id="Seg_6944" s="T614">жена.[NOM]</ta>
            <ta e="T616" id="Seg_6945" s="T615">умирать-CVB.PURP</ta>
            <ta e="T617" id="Seg_6946" s="T616">делать-PST1-3SG</ta>
            <ta e="T618" id="Seg_6947" s="T617">как</ta>
            <ta e="T619" id="Seg_6948" s="T618">INDEF</ta>
            <ta e="T620" id="Seg_6949" s="T619">делать-IMP.3SG</ta>
            <ta e="T621" id="Seg_6950" s="T620">кость.[NOM]</ta>
            <ta e="T622" id="Seg_6951" s="T621">разделять-IMP.3SG</ta>
            <ta e="T623" id="Seg_6952" s="T622">здесь</ta>
            <ta e="T624" id="Seg_6953" s="T623">EMPH-так</ta>
            <ta e="T625" id="Seg_6954" s="T624">этот</ta>
            <ta e="T626" id="Seg_6955" s="T625">Чагыдай.[NOM]</ta>
            <ta e="T627" id="Seg_6956" s="T626">приходить-PRS.[3SG]</ta>
            <ta e="T628" id="Seg_6957" s="T627">как</ta>
            <ta e="T631" id="Seg_6958" s="T630">этот</ta>
            <ta e="T632" id="Seg_6959" s="T631">жена-ACC</ta>
            <ta e="T633" id="Seg_6960" s="T632">рука-VBZ-CVB.SEQ</ta>
            <ta e="T634" id="Seg_6961" s="T633">взять-PRS.[3SG]</ta>
            <ta e="T635" id="Seg_6962" s="T634">ребенок-3SG-ACC</ta>
            <ta e="T636" id="Seg_6963" s="T635">EMPH-вот</ta>
            <ta e="T637" id="Seg_6964" s="T636">живой</ta>
            <ta e="T638" id="Seg_6965" s="T637">оставлять-PRS.[3SG]</ta>
            <ta e="T639" id="Seg_6966" s="T638">видеть.[IMP.2SG]</ta>
            <ta e="T640" id="Seg_6967" s="T639">такой-PL.[NOM]</ta>
            <ta e="T641" id="Seg_6968" s="T640">есть</ta>
            <ta e="T642" id="Seg_6969" s="T641">быть-PST1-3PL</ta>
            <ta e="T643" id="Seg_6970" s="T642">старый-PL.[NOM]</ta>
            <ta e="T644" id="Seg_6971" s="T643">и</ta>
            <ta e="T645" id="Seg_6972" s="T644">Чакыыла.[NOM]</ta>
            <ta e="T646" id="Seg_6973" s="T645">старик.[NOM]</ta>
            <ta e="T647" id="Seg_6974" s="T646">дедушка-3SG.[NOM]</ta>
            <ta e="T648" id="Seg_6975" s="T647">тот-3SG-2SG.[NOM]</ta>
            <ta e="T652" id="Seg_6976" s="T651">AFFIRM</ta>
            <ta e="T653" id="Seg_6977" s="T652">тогда</ta>
            <ta e="T654" id="Seg_6978" s="T653">тот</ta>
            <ta e="T655" id="Seg_6979" s="T654">Чагыдай-EP-2SG.[NOM]</ta>
            <ta e="T656" id="Seg_6980" s="T655">жена-3SG.[NOM]</ta>
            <ta e="T657" id="Seg_6981" s="T656">однако</ta>
            <ta e="T658" id="Seg_6982" s="T657">старуха.[NOM]</ta>
            <ta e="T659" id="Seg_6983" s="T658">Анна.[NOM]</ta>
            <ta e="T660" id="Seg_6984" s="T659">мать-3SG.[NOM]</ta>
            <ta e="T661" id="Seg_6985" s="T660">старуха.[NOM]</ta>
            <ta e="T662" id="Seg_6986" s="T661">этот</ta>
            <ta e="T663" id="Seg_6987" s="T662">Чээкэ</ta>
            <ta e="T664" id="Seg_6988" s="T663">старуха.[NOM]</ta>
            <ta e="T665" id="Seg_6989" s="T664">тот-3SG-2SG.[NOM]</ta>
            <ta e="T666" id="Seg_6990" s="T665">отец-3SG.[NOM]</ta>
            <ta e="T667" id="Seg_6991" s="T666">1PL.[NOM]</ta>
            <ta e="T668" id="Seg_6992" s="T667">дедушка-1PL-ACC</ta>
            <ta e="T669" id="Seg_6993" s="T668">с</ta>
            <ta e="T670" id="Seg_6994" s="T669">вместе</ta>
            <ta e="T671" id="Seg_6995" s="T670">родиться-PST2-3PL</ta>
            <ta e="T672" id="Seg_6996" s="T671">Кудряков</ta>
            <ta e="T673" id="Seg_6997" s="T672">старик.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc-KiPP">
            <ta e="T1" id="Seg_6998" s="T0">propr-n:case</ta>
            <ta e="T2" id="Seg_6999" s="T1">v-v:tense-v:pred.pn</ta>
            <ta e="T5" id="Seg_7000" s="T4">n-n:(ins)-n:case</ta>
            <ta e="T6" id="Seg_7001" s="T5">v-v:ptcp</ta>
            <ta e="T7" id="Seg_7002" s="T6">v-v:tense-v:poss.pn</ta>
            <ta e="T8" id="Seg_7003" s="T7">adv</ta>
            <ta e="T9" id="Seg_7004" s="T8">n-n:(num)-n:case</ta>
            <ta e="T10" id="Seg_7005" s="T9">dempro-pro:case</ta>
            <ta e="T11" id="Seg_7006" s="T10">n-n:(poss)-n:case</ta>
            <ta e="T12" id="Seg_7007" s="T11">v-v:mood-v:pred.pn</ta>
            <ta e="T13" id="Seg_7008" s="T12">posspr</ta>
            <ta e="T14" id="Seg_7009" s="T13">n-n:(poss)-n:case</ta>
            <ta e="T15" id="Seg_7010" s="T14">n-n:case</ta>
            <ta e="T16" id="Seg_7011" s="T15">dempro</ta>
            <ta e="T17" id="Seg_7012" s="T16">n-n:(poss)-n:case</ta>
            <ta e="T18" id="Seg_7013" s="T17">n-n:(num)-n:(poss)-n:case</ta>
            <ta e="T19" id="Seg_7014" s="T18">propr</ta>
            <ta e="T20" id="Seg_7015" s="T19">n-n:case</ta>
            <ta e="T21" id="Seg_7016" s="T20">propr-n:case</ta>
            <ta e="T22" id="Seg_7017" s="T21">n-n:(poss)-n:case</ta>
            <ta e="T23" id="Seg_7018" s="T22">n-n:case</ta>
            <ta e="T24" id="Seg_7019" s="T23">dempro-pro:(num)-pro:case</ta>
            <ta e="T25" id="Seg_7020" s="T24">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T26" id="Seg_7021" s="T25">v-v:mood-v:pred.pn</ta>
            <ta e="T27" id="Seg_7022" s="T26">dempro</ta>
            <ta e="T28" id="Seg_7023" s="T27">propr-n:case</ta>
            <ta e="T29" id="Seg_7024" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_7025" s="T29">n-n:case</ta>
            <ta e="T31" id="Seg_7026" s="T30">v-v:tense-v:pred.pn</ta>
            <ta e="T32" id="Seg_7027" s="T31">propr-n:case</ta>
            <ta e="T33" id="Seg_7028" s="T32">n-n:poss-n:case</ta>
            <ta e="T34" id="Seg_7029" s="T33">cardnum</ta>
            <ta e="T35" id="Seg_7030" s="T34">n-n&gt;adj</ta>
            <ta e="T36" id="Seg_7031" s="T35">dempro</ta>
            <ta e="T37" id="Seg_7032" s="T36">n-n:case</ta>
            <ta e="T38" id="Seg_7033" s="T37">v-v:cvb</ta>
            <ta e="T39" id="Seg_7034" s="T38">v-v:cvb</ta>
            <ta e="T40" id="Seg_7035" s="T39">n-n:case</ta>
            <ta e="T41" id="Seg_7036" s="T40">v-v:tense-v:pred.pn</ta>
            <ta e="T42" id="Seg_7037" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_7038" s="T42">adj-n:case-adj</ta>
            <ta e="T44" id="Seg_7039" s="T43">n-n:case</ta>
            <ta e="T45" id="Seg_7040" s="T44">v-v:tense-v:pred.pn</ta>
            <ta e="T46" id="Seg_7041" s="T45">adj-n:case-adj</ta>
            <ta e="T47" id="Seg_7042" s="T46">n-n:case</ta>
            <ta e="T48" id="Seg_7043" s="T47">v-v:tense-v:pred.pn</ta>
            <ta e="T49" id="Seg_7044" s="T48">adv</ta>
            <ta e="T50" id="Seg_7045" s="T49">propr</ta>
            <ta e="T51" id="Seg_7046" s="T50">n-n:case</ta>
            <ta e="T52" id="Seg_7047" s="T51">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T53" id="Seg_7048" s="T52">ptcl</ta>
            <ta e="T54" id="Seg_7049" s="T53">pers-pro:case</ta>
            <ta e="T55" id="Seg_7050" s="T54">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T56" id="Seg_7051" s="T55">n-n:poss-n:case-n&gt;adj</ta>
            <ta e="T57" id="Seg_7052" s="T56">n-n:(poss)-n:case</ta>
            <ta e="T58" id="Seg_7053" s="T57">dempro</ta>
            <ta e="T59" id="Seg_7054" s="T58">n-n:case</ta>
            <ta e="T60" id="Seg_7055" s="T59">que-pro:case</ta>
            <ta e="T61" id="Seg_7056" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_7057" s="T61">v-v:ptcp</ta>
            <ta e="T63" id="Seg_7058" s="T62">n-n:(poss)-n:case</ta>
            <ta e="T64" id="Seg_7059" s="T63">v-v:tense-v:poss.pn</ta>
            <ta e="T65" id="Seg_7060" s="T64">adv</ta>
            <ta e="T66" id="Seg_7061" s="T65">adj</ta>
            <ta e="T67" id="Seg_7062" s="T66">n-n:case</ta>
            <ta e="T68" id="Seg_7063" s="T67">dempro</ta>
            <ta e="T69" id="Seg_7064" s="T68">n</ta>
            <ta e="T70" id="Seg_7065" s="T69">v-v:ptcp</ta>
            <ta e="T71" id="Seg_7066" s="T70">n-n:(num)-n:case</ta>
            <ta e="T72" id="Seg_7067" s="T71">v-v&gt;v-v:cvb</ta>
            <ta e="T73" id="Seg_7068" s="T72">post</ta>
            <ta e="T74" id="Seg_7069" s="T73">propr-n:case</ta>
            <ta e="T75" id="Seg_7070" s="T74">n-n:poss-n:case</ta>
            <ta e="T76" id="Seg_7071" s="T75">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T77" id="Seg_7072" s="T76">ptcl</ta>
            <ta e="T78" id="Seg_7073" s="T77">adv</ta>
            <ta e="T79" id="Seg_7074" s="T78">n-n:(num)-n:case</ta>
            <ta e="T80" id="Seg_7075" s="T79">v-v:tense-v:pred.pn</ta>
            <ta e="T81" id="Seg_7076" s="T80">ptcl</ta>
            <ta e="T82" id="Seg_7077" s="T81">ptcl</ta>
            <ta e="T83" id="Seg_7078" s="T82">ptcl</ta>
            <ta e="T84" id="Seg_7079" s="T83">n-n:(num)-n:case</ta>
            <ta e="T85" id="Seg_7080" s="T84">v-v:tense-v:pred.pn</ta>
            <ta e="T86" id="Seg_7081" s="T85">dempro</ta>
            <ta e="T87" id="Seg_7082" s="T86">n-n:case</ta>
            <ta e="T88" id="Seg_7083" s="T87">n-n:poss-n:case</ta>
            <ta e="T89" id="Seg_7084" s="T88">dempro</ta>
            <ta e="T90" id="Seg_7085" s="T89">propr-n:case</ta>
            <ta e="T91" id="Seg_7086" s="T90">adj-n:poss-n:case</ta>
            <ta e="T92" id="Seg_7087" s="T91">n-n:poss-n:case</ta>
            <ta e="T93" id="Seg_7088" s="T92">n-n:case</ta>
            <ta e="T94" id="Seg_7089" s="T93">v-v:cvb</ta>
            <ta e="T95" id="Seg_7090" s="T94">v-v:tense-v:pred.pn</ta>
            <ta e="T96" id="Seg_7091" s="T95">ptcl</ta>
            <ta e="T97" id="Seg_7092" s="T96">dempro</ta>
            <ta e="T98" id="Seg_7093" s="T97">n-n:case</ta>
            <ta e="T99" id="Seg_7094" s="T98">post</ta>
            <ta e="T100" id="Seg_7095" s="T99">n-n:case</ta>
            <ta e="T101" id="Seg_7096" s="T100">v-v:cvb</ta>
            <ta e="T102" id="Seg_7097" s="T101">v-v:mood-v:temp.pn</ta>
            <ta e="T103" id="Seg_7098" s="T102">dempro-pro:case</ta>
            <ta e="T104" id="Seg_7099" s="T103">n-n:case</ta>
            <ta e="T105" id="Seg_7100" s="T104">n-n:case</ta>
            <ta e="T106" id="Seg_7101" s="T105">post</ta>
            <ta e="T107" id="Seg_7102" s="T106">v-v:cvb</ta>
            <ta e="T108" id="Seg_7103" s="T107">v-v:cvb</ta>
            <ta e="T109" id="Seg_7104" s="T108">v-v:tense-v:pred.pn</ta>
            <ta e="T110" id="Seg_7105" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_7106" s="T110">n-n:case</ta>
            <ta e="T112" id="Seg_7107" s="T111">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T113" id="Seg_7108" s="T112">n-n:case</ta>
            <ta e="T114" id="Seg_7109" s="T113">n-n:poss-n:case</ta>
            <ta e="T115" id="Seg_7110" s="T114">dempro-pro:case</ta>
            <ta e="T116" id="Seg_7111" s="T115">dempro</ta>
            <ta e="T119" id="Seg_7112" s="T118">propr-n:(poss)-n:case</ta>
            <ta e="T120" id="Seg_7113" s="T119">v-v:tense-v:pred.pn</ta>
            <ta e="T121" id="Seg_7114" s="T120">ptcl</ta>
            <ta e="T122" id="Seg_7115" s="T121">interj</ta>
            <ta e="T123" id="Seg_7116" s="T122">adj</ta>
            <ta e="T124" id="Seg_7117" s="T123">n-n:poss-n:case</ta>
            <ta e="T125" id="Seg_7118" s="T124">conj</ta>
            <ta e="T126" id="Seg_7119" s="T125">adj</ta>
            <ta e="T127" id="Seg_7120" s="T126">n-n:case</ta>
            <ta e="T128" id="Seg_7121" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_7122" s="T128">v-v:tense-v:pred.pn</ta>
            <ta e="T131" id="Seg_7123" s="T130">ptcl</ta>
            <ta e="T132" id="Seg_7124" s="T131">dempro</ta>
            <ta e="T133" id="Seg_7125" s="T132">n-n:case</ta>
            <ta e="T134" id="Seg_7126" s="T133">propr-n:case</ta>
            <ta e="T135" id="Seg_7127" s="T134">ptcl</ta>
            <ta e="T136" id="Seg_7128" s="T135">dempro</ta>
            <ta e="T137" id="Seg_7129" s="T136">dempro</ta>
            <ta e="T138" id="Seg_7130" s="T137">n-n:case</ta>
            <ta e="T139" id="Seg_7131" s="T138">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T140" id="Seg_7132" s="T139">v-v:cvb</ta>
            <ta e="T141" id="Seg_7133" s="T140">v-v:tense-v:pred.pn</ta>
            <ta e="T142" id="Seg_7134" s="T141">ptcl</ta>
            <ta e="T143" id="Seg_7135" s="T142">dempro</ta>
            <ta e="T144" id="Seg_7136" s="T143">que</ta>
            <ta e="T145" id="Seg_7137" s="T144">n-n:case</ta>
            <ta e="T146" id="Seg_7138" s="T145">v-v:tense-v:poss.pn</ta>
            <ta e="T147" id="Seg_7139" s="T146">adv</ta>
            <ta e="T148" id="Seg_7140" s="T147">v-v:cvb</ta>
            <ta e="T149" id="Seg_7141" s="T148">v-v:tense-v:pred.pn</ta>
            <ta e="T150" id="Seg_7142" s="T149">n-n:(num)-n:case</ta>
            <ta e="T151" id="Seg_7143" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_7144" s="T151">ptcl</ta>
            <ta e="T153" id="Seg_7145" s="T152">dempro-pro:case</ta>
            <ta e="T154" id="Seg_7146" s="T153">pers-pro:case</ta>
            <ta e="T155" id="Seg_7147" s="T154">n-n:case</ta>
            <ta e="T156" id="Seg_7148" s="T155">que-pro:case</ta>
            <ta e="T157" id="Seg_7149" s="T156">ptcl</ta>
            <ta e="T158" id="Seg_7150" s="T157">adv</ta>
            <ta e="T159" id="Seg_7151" s="T158">v-v:(neg)-v:tense-v:poss.pn</ta>
            <ta e="T160" id="Seg_7152" s="T159">n-n:(poss)-n:case</ta>
            <ta e="T161" id="Seg_7153" s="T160">ptcl</ta>
            <ta e="T162" id="Seg_7154" s="T161">n-n:poss-n:case</ta>
            <ta e="T163" id="Seg_7155" s="T162">v-v:cvb</ta>
            <ta e="T164" id="Seg_7156" s="T163">post</ta>
            <ta e="T165" id="Seg_7157" s="T164">v-v:tense-v:pred.pn</ta>
            <ta e="T166" id="Seg_7158" s="T165">v-v:cvb</ta>
            <ta e="T167" id="Seg_7159" s="T166">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T168" id="Seg_7160" s="T167">v-v:cvb</ta>
            <ta e="T169" id="Seg_7161" s="T168">n-n:case</ta>
            <ta e="T170" id="Seg_7162" s="T169">dempro-pro:case</ta>
            <ta e="T171" id="Seg_7163" s="T170">n-n:case</ta>
            <ta e="T172" id="Seg_7164" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_7165" s="T172">v-v:(neg)-v:pred.pn</ta>
            <ta e="T174" id="Seg_7166" s="T173">v-v:cvb</ta>
            <ta e="T175" id="Seg_7167" s="T174">v-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T176" id="Seg_7168" s="T175">n-n:case</ta>
            <ta e="T177" id="Seg_7169" s="T176">v-v&gt;v-v:cvb</ta>
            <ta e="T178" id="Seg_7170" s="T177">v-v:tense-v:pred.pn</ta>
            <ta e="T179" id="Seg_7171" s="T178">adv</ta>
            <ta e="T180" id="Seg_7172" s="T179">ptcl</ta>
            <ta e="T181" id="Seg_7173" s="T180">n-n:case</ta>
            <ta e="T182" id="Seg_7174" s="T181">n-n:case</ta>
            <ta e="T183" id="Seg_7175" s="T182">n-n:case</ta>
            <ta e="T184" id="Seg_7176" s="T183">v-v:tense-v:poss.pn</ta>
            <ta e="T185" id="Seg_7177" s="T184">v-v:tense-v:pred.pn</ta>
            <ta e="T186" id="Seg_7178" s="T185">ptcl</ta>
            <ta e="T187" id="Seg_7179" s="T186">cardnum</ta>
            <ta e="T188" id="Seg_7180" s="T187">n-n:case</ta>
            <ta e="T189" id="Seg_7181" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_7182" s="T189">que-pro:case</ta>
            <ta e="T191" id="Seg_7183" s="T190">v-v:tense-v:poss.pn</ta>
            <ta e="T192" id="Seg_7184" s="T191">dempro-pro:case</ta>
            <ta e="T193" id="Seg_7185" s="T192">v-v:tense-v:pred.pn</ta>
            <ta e="T194" id="Seg_7186" s="T193">ptcl</ta>
            <ta e="T210" id="Seg_7187" s="T209">dempro-pro:case</ta>
            <ta e="T211" id="Seg_7188" s="T210">v-v:cvb</ta>
            <ta e="T212" id="Seg_7189" s="T211">v-v:tense-v:poss.pn</ta>
            <ta e="T213" id="Seg_7190" s="T212">v-v:tense-v:pred.pn</ta>
            <ta e="T214" id="Seg_7191" s="T213">cardnum</ta>
            <ta e="T215" id="Seg_7192" s="T214">n-n&gt;adj-n:case</ta>
            <ta e="T216" id="Seg_7193" s="T215">dempro</ta>
            <ta e="T217" id="Seg_7194" s="T216">n-n:(num)-n:case</ta>
            <ta e="T218" id="Seg_7195" s="T217">n-n:(poss)</ta>
            <ta e="T219" id="Seg_7196" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_7197" s="T219">v-v:tense-v:pred.pn</ta>
            <ta e="T221" id="Seg_7198" s="T220">adv</ta>
            <ta e="T222" id="Seg_7199" s="T221">ptcl</ta>
            <ta e="T223" id="Seg_7200" s="T222">n-n:case</ta>
            <ta e="T224" id="Seg_7201" s="T223">interj</ta>
            <ta e="T225" id="Seg_7202" s="T224">adj</ta>
            <ta e="T226" id="Seg_7203" s="T225">n-n&gt;n-n:case</ta>
            <ta e="T227" id="Seg_7204" s="T226">v-v:(ins)-v&gt;v-v:cvb-v:pred.pn</ta>
            <ta e="T228" id="Seg_7205" s="T227">dempro</ta>
            <ta e="T229" id="Seg_7206" s="T228">n-n:case</ta>
            <ta e="T230" id="Seg_7207" s="T229">v-v:cvb-v:pred.pn</ta>
            <ta e="T231" id="Seg_7208" s="T230">v-v:cvb-v:pred.pn</ta>
            <ta e="T232" id="Seg_7209" s="T231">v-v:tense-v:pred.pn</ta>
            <ta e="T233" id="Seg_7210" s="T232">dempro</ta>
            <ta e="T234" id="Seg_7211" s="T233">n-n:case</ta>
            <ta e="T235" id="Seg_7212" s="T234">ptcl</ta>
            <ta e="T236" id="Seg_7213" s="T235">v-v:tense-v:poss.pn</ta>
            <ta e="T237" id="Seg_7214" s="T236">dempro</ta>
            <ta e="T238" id="Seg_7215" s="T237">n-n:case</ta>
            <ta e="T239" id="Seg_7216" s="T238">v-v:tense-v:pred.pn</ta>
            <ta e="T240" id="Seg_7217" s="T239">v-v:mood-v:temp.pn</ta>
            <ta e="T241" id="Seg_7218" s="T240">que-pro:case</ta>
            <ta e="T242" id="Seg_7219" s="T241">ptcl</ta>
            <ta e="T243" id="Seg_7220" s="T242">n-n:(poss)-n:case</ta>
            <ta e="T244" id="Seg_7221" s="T243">v-v:tense-v:pred.pn</ta>
            <ta e="T245" id="Seg_7222" s="T244">ptcl</ta>
            <ta e="T246" id="Seg_7223" s="T245">ptcl</ta>
            <ta e="T247" id="Seg_7224" s="T246">n-n:case</ta>
            <ta e="T248" id="Seg_7225" s="T247">ptcl</ta>
            <ta e="T249" id="Seg_7226" s="T248">que-pro:case</ta>
            <ta e="T250" id="Seg_7227" s="T249">ptcl</ta>
            <ta e="T251" id="Seg_7228" s="T250">n-n:case</ta>
            <ta e="T252" id="Seg_7229" s="T251">n-n:(poss)</ta>
            <ta e="T253" id="Seg_7230" s="T252">ptcl</ta>
            <ta e="T254" id="Seg_7231" s="T253">n-n:case</ta>
            <ta e="T255" id="Seg_7232" s="T254">v-v:tense-v:pred.pn</ta>
            <ta e="T256" id="Seg_7233" s="T255">n-n:case</ta>
            <ta e="T257" id="Seg_7234" s="T256">ptcl</ta>
            <ta e="T258" id="Seg_7235" s="T257">que-pro:case</ta>
            <ta e="T259" id="Seg_7236" s="T258">ptcl</ta>
            <ta e="T260" id="Seg_7237" s="T259">que-pro:case</ta>
            <ta e="T261" id="Seg_7238" s="T260">ptcl</ta>
            <ta e="T262" id="Seg_7239" s="T261">v-v:(neg)-v:pred.pn</ta>
            <ta e="T263" id="Seg_7240" s="T262">v-v:tense-v:pred.pn</ta>
            <ta e="T264" id="Seg_7241" s="T263">dempro</ta>
            <ta e="T265" id="Seg_7242" s="T264">v-v:cvb</ta>
            <ta e="T266" id="Seg_7243" s="T265">n-n:case</ta>
            <ta e="T267" id="Seg_7244" s="T266">v-v:tense-v:pred.pn</ta>
            <ta e="T268" id="Seg_7245" s="T267">ptcl</ta>
            <ta e="T269" id="Seg_7246" s="T268">ptcl</ta>
            <ta e="T270" id="Seg_7247" s="T269">dempro</ta>
            <ta e="T271" id="Seg_7248" s="T270">v-v:cvb</ta>
            <ta e="T272" id="Seg_7249" s="T271">ptcl</ta>
            <ta e="T273" id="Seg_7250" s="T272">interj</ta>
            <ta e="T274" id="Seg_7251" s="T273">v-v:cvb</ta>
            <ta e="T275" id="Seg_7252" s="T274">v-v:tense-v:pred.pn</ta>
            <ta e="T276" id="Seg_7253" s="T275">que</ta>
            <ta e="T277" id="Seg_7254" s="T276">ptcl</ta>
            <ta e="T278" id="Seg_7255" s="T277">n-n&gt;adj-n:case</ta>
            <ta e="T279" id="Seg_7256" s="T278">n-n:poss-n:case</ta>
            <ta e="T280" id="Seg_7257" s="T279">adj</ta>
            <ta e="T281" id="Seg_7258" s="T280">n-n&gt;adj-n:case</ta>
            <ta e="T282" id="Seg_7259" s="T281">interj</ta>
            <ta e="T285" id="Seg_7260" s="T284">n-n:(num)-n:case</ta>
            <ta e="T286" id="Seg_7261" s="T285">adj-adj&gt;adv</ta>
            <ta e="T287" id="Seg_7262" s="T286">v-v&gt;v-v:(ins)-v:mood.pn</ta>
            <ta e="T288" id="Seg_7263" s="T287">v-v:tense-v:pred.pn</ta>
            <ta e="T289" id="Seg_7264" s="T288">ptcl</ta>
            <ta e="T290" id="Seg_7265" s="T289">n-n:poss-n:case</ta>
            <ta e="T291" id="Seg_7266" s="T290">n-n:(num)-n:poss-n:case</ta>
            <ta e="T292" id="Seg_7267" s="T291">propr-n:case</ta>
            <ta e="T293" id="Seg_7268" s="T292">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T294" id="Seg_7269" s="T293">interj</ta>
            <ta e="T295" id="Seg_7270" s="T294">v-v:cvb</ta>
            <ta e="T296" id="Seg_7271" s="T295">v-v:tense-v:pred.pn</ta>
            <ta e="T297" id="Seg_7272" s="T296">cardnum</ta>
            <ta e="T298" id="Seg_7273" s="T297">n-n:poss-n:case</ta>
            <ta e="T299" id="Seg_7274" s="T298">v-v:cvb</ta>
            <ta e="T300" id="Seg_7275" s="T299">v-v:tense-v:pred.pn</ta>
            <ta e="T301" id="Seg_7276" s="T300">adv</ta>
            <ta e="T302" id="Seg_7277" s="T301">dempro</ta>
            <ta e="T303" id="Seg_7278" s="T302">n-n:case</ta>
            <ta e="T304" id="Seg_7279" s="T303">adj</ta>
            <ta e="T305" id="Seg_7280" s="T304">n-n&gt;adj-n:case</ta>
            <ta e="T306" id="Seg_7281" s="T305">v-v:tense-v:pred.pn</ta>
            <ta e="T307" id="Seg_7282" s="T306">adv</ta>
            <ta e="T308" id="Seg_7283" s="T307">dempro</ta>
            <ta e="T309" id="Seg_7284" s="T308">n-n:case</ta>
            <ta e="T310" id="Seg_7285" s="T309">ptcl</ta>
            <ta e="T311" id="Seg_7286" s="T310">n-n&gt;adj-n:case</ta>
            <ta e="T312" id="Seg_7287" s="T311">v-v:tense-v:pred.pn</ta>
            <ta e="T313" id="Seg_7288" s="T312">dempro</ta>
            <ta e="T314" id="Seg_7289" s="T313">adv</ta>
            <ta e="T315" id="Seg_7290" s="T314">v-v:tense-v:pred.pn</ta>
            <ta e="T316" id="Seg_7291" s="T315">v-v:ptcp</ta>
            <ta e="T317" id="Seg_7292" s="T316">n-n:(ins)-n:case</ta>
            <ta e="T318" id="Seg_7293" s="T317">v-v:tense-v:pred.pn</ta>
            <ta e="T319" id="Seg_7294" s="T318">v-v:tense-v:pred.pn</ta>
            <ta e="T320" id="Seg_7295" s="T319">n-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T321" id="Seg_7296" s="T320">n-n:poss-n:case</ta>
            <ta e="T322" id="Seg_7297" s="T321">ptcl</ta>
            <ta e="T323" id="Seg_7298" s="T322">dempro</ta>
            <ta e="T324" id="Seg_7299" s="T323">n-n:case</ta>
            <ta e="T325" id="Seg_7300" s="T324">v-v:tense-v:pred.pn</ta>
            <ta e="T326" id="Seg_7301" s="T325">ptcl</ta>
            <ta e="T327" id="Seg_7302" s="T326">pers-pro:case</ta>
            <ta e="T328" id="Seg_7303" s="T327">pers-pro:case</ta>
            <ta e="T329" id="Seg_7304" s="T328">v-v:mood.pn</ta>
            <ta e="T330" id="Seg_7305" s="T329">dempro-pro:case</ta>
            <ta e="T331" id="Seg_7306" s="T330">que</ta>
            <ta e="T332" id="Seg_7307" s="T331">ptcl</ta>
            <ta e="T333" id="Seg_7308" s="T332">v-v:(ins)-v&gt;v-v:ptcp</ta>
            <ta e="T334" id="Seg_7309" s="T333">n-n:poss-n:case</ta>
            <ta e="T335" id="Seg_7310" s="T334">v-v:mood.pn</ta>
            <ta e="T336" id="Seg_7311" s="T335">v-v:tense-v:pred.pn</ta>
            <ta e="T337" id="Seg_7312" s="T336">n-n:poss-n:case</ta>
            <ta e="T338" id="Seg_7313" s="T337">v-v:tense-v:pred.pn</ta>
            <ta e="T339" id="Seg_7314" s="T338">ptcl</ta>
            <ta e="T340" id="Seg_7315" s="T339">dempro</ta>
            <ta e="T341" id="Seg_7316" s="T340">n-n:poss-n:case</ta>
            <ta e="T342" id="Seg_7317" s="T341">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T343" id="Seg_7318" s="T342">ptcl</ta>
            <ta e="T344" id="Seg_7319" s="T343">v-v:cvb</ta>
            <ta e="T345" id="Seg_7320" s="T344">v-v:tense-v:pred.pn</ta>
            <ta e="T346" id="Seg_7321" s="T345">dempro</ta>
            <ta e="T347" id="Seg_7322" s="T346">v-v:cvb</ta>
            <ta e="T348" id="Seg_7323" s="T347">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T349" id="Seg_7324" s="T348">n-n:(ins)-n:case</ta>
            <ta e="T350" id="Seg_7325" s="T349">v-v:cvb</ta>
            <ta e="T351" id="Seg_7326" s="T350">v-v:(ins)-v:tense-v:pred.pn</ta>
            <ta e="T352" id="Seg_7327" s="T351">v-v:mood-v:pred.pn</ta>
            <ta e="T353" id="Seg_7328" s="T352">propr-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T354" id="Seg_7329" s="T353">v-v:tense-v:pred.pn</ta>
            <ta e="T357" id="Seg_7330" s="T356">dempro</ta>
            <ta e="T358" id="Seg_7331" s="T357">v-v:cvb-v:pred.pn</ta>
            <ta e="T359" id="Seg_7332" s="T358">que-pro:case</ta>
            <ta e="T360" id="Seg_7333" s="T359">v-v:tense-v:poss.pn</ta>
            <ta e="T361" id="Seg_7334" s="T360">v-v:tense-v:pred.pn</ta>
            <ta e="T362" id="Seg_7335" s="T361">ptcl</ta>
            <ta e="T363" id="Seg_7336" s="T362">n-n:poss-n:case</ta>
            <ta e="T364" id="Seg_7337" s="T363">v-v:tense-v:poss.pn</ta>
            <ta e="T365" id="Seg_7338" s="T364">v-v:tense-v:pred.pn</ta>
            <ta e="T366" id="Seg_7339" s="T365">n-n:poss-n:case</ta>
            <ta e="T367" id="Seg_7340" s="T366">v-v:cvb-v:pred.pn</ta>
            <ta e="T368" id="Seg_7341" s="T367">n-n:case</ta>
            <ta e="T369" id="Seg_7342" s="T368">n-n:poss-n:case</ta>
            <ta e="T370" id="Seg_7343" s="T369">v-v:tense-v:poss.pn</ta>
            <ta e="T371" id="Seg_7344" s="T370">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T372" id="Seg_7345" s="T371">n-n:poss-n:case</ta>
            <ta e="T373" id="Seg_7346" s="T372">adj-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T374" id="Seg_7347" s="T373">v-v:cvb</ta>
            <ta e="T375" id="Seg_7348" s="T374">v-v:tense-v:poss.pn</ta>
            <ta e="T376" id="Seg_7349" s="T375">v-v:cvb</ta>
            <ta e="T377" id="Seg_7350" s="T376">v-v:cvb</ta>
            <ta e="T378" id="Seg_7351" s="T377">v-v:cvb</ta>
            <ta e="T379" id="Seg_7352" s="T378">ptcl</ta>
            <ta e="T380" id="Seg_7353" s="T379">n-n:case</ta>
            <ta e="T381" id="Seg_7354" s="T380">ptcl</ta>
            <ta e="T382" id="Seg_7355" s="T381">adj</ta>
            <ta e="T383" id="Seg_7356" s="T382">n-n:case</ta>
            <ta e="T384" id="Seg_7357" s="T383">v-v:cvb</ta>
            <ta e="T385" id="Seg_7358" s="T384">post</ta>
            <ta e="T386" id="Seg_7359" s="T385">ptcl</ta>
            <ta e="T387" id="Seg_7360" s="T386">v-v:tense-v:pred.pn</ta>
            <ta e="T388" id="Seg_7361" s="T387">que-pro:case</ta>
            <ta e="T389" id="Seg_7362" s="T388">ptcl</ta>
            <ta e="T390" id="Seg_7363" s="T389">v-v&gt;v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T391" id="Seg_7364" s="T390">dempro</ta>
            <ta e="T392" id="Seg_7365" s="T391">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T393" id="Seg_7366" s="T392">ptcl</ta>
            <ta e="T394" id="Seg_7367" s="T393">adv</ta>
            <ta e="T395" id="Seg_7368" s="T394">n-n:case</ta>
            <ta e="T396" id="Seg_7369" s="T395">n-n:poss-n:case</ta>
            <ta e="T397" id="Seg_7370" s="T396">v-v:tense-v:(ins)-v:poss.pn</ta>
            <ta e="T398" id="Seg_7371" s="T397">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T399" id="Seg_7372" s="T398">post</ta>
            <ta e="T400" id="Seg_7373" s="T399">ptcl</ta>
            <ta e="T401" id="Seg_7374" s="T400">dempro</ta>
            <ta e="T402" id="Seg_7375" s="T401">n-n:poss-n:case</ta>
            <ta e="T403" id="Seg_7376" s="T402">v-v:ptcp</ta>
            <ta e="T404" id="Seg_7377" s="T403">n-n:poss-n:case</ta>
            <ta e="T405" id="Seg_7378" s="T404">ptcl</ta>
            <ta e="T406" id="Seg_7379" s="T405">v-v:cvb</ta>
            <ta e="T407" id="Seg_7380" s="T406">v-v:cvb</ta>
            <ta e="T408" id="Seg_7381" s="T407">v-v:tense-v:pred.pn</ta>
            <ta e="T409" id="Seg_7382" s="T408">dempro</ta>
            <ta e="T410" id="Seg_7383" s="T409">v-v:tense-v:poss.pn</ta>
            <ta e="T411" id="Seg_7384" s="T410">v-v:tense-v:pred.pn</ta>
            <ta e="T412" id="Seg_7385" s="T411">ptcl</ta>
            <ta e="T413" id="Seg_7386" s="T412">adv</ta>
            <ta e="T414" id="Seg_7387" s="T413">cardnum</ta>
            <ta e="T415" id="Seg_7388" s="T414">n-n:case</ta>
            <ta e="T416" id="Seg_7389" s="T415">adj</ta>
            <ta e="T417" id="Seg_7390" s="T416">n-n&gt;adj</ta>
            <ta e="T418" id="Seg_7391" s="T417">v-v:tense-v:pred.pn</ta>
            <ta e="T419" id="Seg_7392" s="T418">v-v:cvb-v:pred.pn</ta>
            <ta e="T420" id="Seg_7393" s="T419">emphpro-pro:(poss)-pro:case</ta>
            <ta e="T421" id="Seg_7394" s="T420">que-pro:case</ta>
            <ta e="T422" id="Seg_7395" s="T421">ptcl</ta>
            <ta e="T423" id="Seg_7396" s="T422">v-v:cvb</ta>
            <ta e="T424" id="Seg_7397" s="T423">v-v:cvb</ta>
            <ta e="T425" id="Seg_7398" s="T424">v</ta>
            <ta e="T426" id="Seg_7399" s="T425">dempro</ta>
            <ta e="T427" id="Seg_7400" s="T426">v-v:cvb</ta>
            <ta e="T428" id="Seg_7401" s="T427">dempro</ta>
            <ta e="T429" id="Seg_7402" s="T428">adv</ta>
            <ta e="T430" id="Seg_7403" s="T429">post</ta>
            <ta e="T431" id="Seg_7404" s="T430">v-v:tense-v:pred.pn</ta>
            <ta e="T432" id="Seg_7405" s="T431">adv</ta>
            <ta e="T433" id="Seg_7406" s="T432">propr-n:case</ta>
            <ta e="T434" id="Seg_7407" s="T433">ptcl</ta>
            <ta e="T435" id="Seg_7408" s="T434">adv</ta>
            <ta e="T436" id="Seg_7409" s="T435">adv</ta>
            <ta e="T437" id="Seg_7410" s="T436">v-v:tense-v:pred.pn</ta>
            <ta e="T438" id="Seg_7411" s="T437">ptcl</ta>
            <ta e="T439" id="Seg_7412" s="T438">v-v:cvb</ta>
            <ta e="T440" id="Seg_7413" s="T439">v-v:cvb-v:pred.pn</ta>
            <ta e="T441" id="Seg_7414" s="T440">ptcl</ta>
            <ta e="T442" id="Seg_7415" s="T441">n-n:case</ta>
            <ta e="T443" id="Seg_7416" s="T442">v-v:tense-v:pred.pn</ta>
            <ta e="T444" id="Seg_7417" s="T443">dempro</ta>
            <ta e="T445" id="Seg_7418" s="T444">n-n:case</ta>
            <ta e="T446" id="Seg_7419" s="T445">ptcl</ta>
            <ta e="T447" id="Seg_7420" s="T446">propr-n:case</ta>
            <ta e="T448" id="Seg_7421" s="T447">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T449" id="Seg_7422" s="T448">ptcl</ta>
            <ta e="T450" id="Seg_7423" s="T449">que</ta>
            <ta e="T451" id="Seg_7424" s="T450">ptcl</ta>
            <ta e="T452" id="Seg_7425" s="T451">v-v:tense-v:pred.pn</ta>
            <ta e="T453" id="Seg_7426" s="T452">n-n:case</ta>
            <ta e="T454" id="Seg_7427" s="T453">adv</ta>
            <ta e="T455" id="Seg_7428" s="T454">que-pro:case</ta>
            <ta e="T456" id="Seg_7429" s="T455">n-n:(poss)-n:case</ta>
            <ta e="T457" id="Seg_7430" s="T456">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T458" id="Seg_7431" s="T457">ptcl</ta>
            <ta e="T459" id="Seg_7432" s="T458">propr</ta>
            <ta e="T460" id="Seg_7433" s="T459">dempro</ta>
            <ta e="T461" id="Seg_7434" s="T460">n-n:case</ta>
            <ta e="T462" id="Seg_7435" s="T461">ptcl</ta>
            <ta e="T463" id="Seg_7436" s="T462">que-pro:case</ta>
            <ta e="T464" id="Seg_7437" s="T463">v-v:mood.pn</ta>
            <ta e="T465" id="Seg_7438" s="T464">propr-n:case</ta>
            <ta e="T466" id="Seg_7439" s="T465">propr</ta>
            <ta e="T467" id="Seg_7440" s="T466">propr-n:poss-n:case</ta>
            <ta e="T468" id="Seg_7441" s="T467">v-v:mood.pn</ta>
            <ta e="T469" id="Seg_7442" s="T468">adj-adj&gt;adv</ta>
            <ta e="T470" id="Seg_7443" s="T469">propr-n:case</ta>
            <ta e="T471" id="Seg_7444" s="T470">adj</ta>
            <ta e="T472" id="Seg_7445" s="T471">n-n:poss-n:case</ta>
            <ta e="T478" id="Seg_7446" s="T476">ptcl</ta>
            <ta e="T479" id="Seg_7447" s="T478">dempro</ta>
            <ta e="T480" id="Seg_7448" s="T479">emphpro-pro:(num)-pro:(poss)-pro:case</ta>
            <ta e="T482" id="Seg_7449" s="T480">adj-n:(poss)-n:case</ta>
            <ta e="T485" id="Seg_7450" s="T482">ptcl</ta>
            <ta e="T486" id="Seg_7451" s="T485">propr-n:(poss)-n:case</ta>
            <ta e="T487" id="Seg_7452" s="T486">adv&gt;adv-adv</ta>
            <ta e="T488" id="Seg_7453" s="T487">v-v:ptcp</ta>
            <ta e="T489" id="Seg_7454" s="T488">v-v:tense-v:poss.pn</ta>
            <ta e="T490" id="Seg_7455" s="T489">dempro</ta>
            <ta e="T491" id="Seg_7456" s="T490">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T492" id="Seg_7457" s="T491">v-v:tense-v:pred.pn</ta>
            <ta e="T493" id="Seg_7458" s="T492">dempro</ta>
            <ta e="T494" id="Seg_7459" s="T493">post</ta>
            <ta e="T495" id="Seg_7460" s="T494">adv</ta>
            <ta e="T496" id="Seg_7461" s="T495">post</ta>
            <ta e="T497" id="Seg_7462" s="T496">ptcl</ta>
            <ta e="T498" id="Seg_7463" s="T497">dempro</ta>
            <ta e="T499" id="Seg_7464" s="T498">ptcl</ta>
            <ta e="T500" id="Seg_7465" s="T499">dempro</ta>
            <ta e="T501" id="Seg_7466" s="T500">n-n:case</ta>
            <ta e="T502" id="Seg_7467" s="T501">v-v:(ins)-v&gt;v-v:tense-v:pred.pn</ta>
            <ta e="T503" id="Seg_7468" s="T502">adv</ta>
            <ta e="T504" id="Seg_7469" s="T503">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T505" id="Seg_7470" s="T504">v-v:tense-v:poss.pn</ta>
            <ta e="T506" id="Seg_7471" s="T505">ptcl</ta>
            <ta e="T507" id="Seg_7472" s="T506">n-n&gt;adj-n:(pred.pn)</ta>
            <ta e="T508" id="Seg_7473" s="T507">ptcl</ta>
            <ta e="T512" id="Seg_7474" s="T511">n-n:(num)-n:case</ta>
            <ta e="T513" id="Seg_7475" s="T512">dempro</ta>
            <ta e="T514" id="Seg_7476" s="T513">n-n:(poss)-n:case</ta>
            <ta e="T515" id="Seg_7477" s="T514">n-n:(poss)-n:case</ta>
            <ta e="T516" id="Seg_7478" s="T515">que-pro:(poss)-pro:case</ta>
            <ta e="T517" id="Seg_7479" s="T516">n-n:case</ta>
            <ta e="T518" id="Seg_7480" s="T517">v-v:tense-v:pred.pn</ta>
            <ta e="T519" id="Seg_7481" s="T518">ptcl</ta>
            <ta e="T520" id="Seg_7482" s="T519">adv</ta>
            <ta e="T521" id="Seg_7483" s="T520">v</ta>
            <ta e="T522" id="Seg_7484" s="T521">dempro</ta>
            <ta e="T523" id="Seg_7485" s="T522">n-n:case</ta>
            <ta e="T524" id="Seg_7486" s="T523">ptcl</ta>
            <ta e="T525" id="Seg_7487" s="T524">ptcl</ta>
            <ta e="T526" id="Seg_7488" s="T525">adv</ta>
            <ta e="T527" id="Seg_7489" s="T526">v-v:tense-v:pred.pn</ta>
            <ta e="T528" id="Seg_7490" s="T527">adv</ta>
            <ta e="T529" id="Seg_7491" s="T528">adv-adv&gt;n</ta>
            <ta e="T530" id="Seg_7492" s="T529">n-n:case</ta>
            <ta e="T531" id="Seg_7493" s="T530">v-v:ptcp</ta>
            <ta e="T532" id="Seg_7494" s="T531">n-n:poss-n:case</ta>
            <ta e="T533" id="Seg_7495" s="T532">v-v:tense-v:poss.pn</ta>
            <ta e="T534" id="Seg_7496" s="T533">v-v:tense-v:pred.pn</ta>
            <ta e="T535" id="Seg_7497" s="T534">dempro</ta>
            <ta e="T536" id="Seg_7498" s="T535">n-n:case</ta>
            <ta e="T537" id="Seg_7499" s="T536">interj</ta>
            <ta e="T538" id="Seg_7500" s="T537">que</ta>
            <ta e="T539" id="Seg_7501" s="T538">v-v:cvb</ta>
            <ta e="T540" id="Seg_7502" s="T539">v-v:cvb</ta>
            <ta e="T541" id="Seg_7503" s="T540">v-v:tense-v:pred.pn</ta>
            <ta e="T542" id="Seg_7504" s="T541">propr-n:case</ta>
            <ta e="T543" id="Seg_7505" s="T542">adj</ta>
            <ta e="T544" id="Seg_7506" s="T543">n-n:poss-n:case</ta>
            <ta e="T545" id="Seg_7507" s="T544">adv</ta>
            <ta e="T546" id="Seg_7508" s="T545">adj-n:case</ta>
            <ta e="T547" id="Seg_7509" s="T546">propr-n:case</ta>
            <ta e="T548" id="Seg_7510" s="T547">ptcl</ta>
            <ta e="T549" id="Seg_7511" s="T548">dempro</ta>
            <ta e="T550" id="Seg_7512" s="T549">n-n:case</ta>
            <ta e="T551" id="Seg_7513" s="T550">v-v:cvb</ta>
            <ta e="T552" id="Seg_7514" s="T551">v-v:tense-v:pred.pn</ta>
            <ta e="T553" id="Seg_7515" s="T552">interj</ta>
            <ta e="T554" id="Seg_7516" s="T553">v-v:cvb</ta>
            <ta e="T555" id="Seg_7517" s="T554">v-v:tense-v:pred.pn</ta>
            <ta e="T556" id="Seg_7518" s="T555">n-n:case</ta>
            <ta e="T557" id="Seg_7519" s="T556">adv</ta>
            <ta e="T558" id="Seg_7520" s="T557">n-n:(num)-n:case</ta>
            <ta e="T561" id="Seg_7521" s="T560">adv-adv&gt;n</ta>
            <ta e="T562" id="Seg_7522" s="T561">n-n:(num)-n:case</ta>
            <ta e="T563" id="Seg_7523" s="T562">v-v:tense-v:pred.pn</ta>
            <ta e="T564" id="Seg_7524" s="T563">ptcl</ta>
            <ta e="T565" id="Seg_7525" s="T564">adv</ta>
            <ta e="T566" id="Seg_7526" s="T565">cardnum</ta>
            <ta e="T567" id="Seg_7527" s="T566">n-n:case</ta>
            <ta e="T568" id="Seg_7528" s="T567">ptcl</ta>
            <ta e="T569" id="Seg_7529" s="T568">v-v:tense-v:pred.pn</ta>
            <ta e="T570" id="Seg_7530" s="T569">ptcl</ta>
            <ta e="T571" id="Seg_7531" s="T570">que-pro:case</ta>
            <ta e="T572" id="Seg_7532" s="T571">n-n:(poss)-n:case</ta>
            <ta e="T573" id="Seg_7533" s="T572">v-v:tense-v:poss.pn-ptcl</ta>
            <ta e="T574" id="Seg_7534" s="T573">n-n:poss-n:case</ta>
            <ta e="T575" id="Seg_7535" s="T574">v-v:tense-v:pred.pn</ta>
            <ta e="T576" id="Seg_7536" s="T575">ptcl</ta>
            <ta e="T577" id="Seg_7537" s="T576">interj</ta>
            <ta e="T578" id="Seg_7538" s="T577">cardnum</ta>
            <ta e="T579" id="Seg_7539" s="T578">n-n:case</ta>
            <ta e="T580" id="Seg_7540" s="T579">propr-n:case</ta>
            <ta e="T581" id="Seg_7541" s="T580">v-v:tense-v:pred.pn</ta>
            <ta e="T582" id="Seg_7542" s="T581">n-n:(poss)-n:case</ta>
            <ta e="T583" id="Seg_7543" s="T582">v-v:(ins)-v&gt;v-v:cvb</ta>
            <ta e="T584" id="Seg_7544" s="T583">v-v:tense-v:pred.pn</ta>
            <ta e="T585" id="Seg_7545" s="T584">ptcl</ta>
            <ta e="T586" id="Seg_7546" s="T585">v-v:mood.pn</ta>
            <ta e="T587" id="Seg_7547" s="T586">dempro</ta>
            <ta e="T588" id="Seg_7548" s="T587">adj</ta>
            <ta e="T589" id="Seg_7549" s="T588">n-n:(num)-n:case</ta>
            <ta e="T590" id="Seg_7550" s="T589">ptcl-ptcl:(pred.pn)</ta>
            <ta e="T591" id="Seg_7551" s="T590">adv</ta>
            <ta e="T592" id="Seg_7552" s="T591">n-n:case</ta>
            <ta e="T593" id="Seg_7553" s="T592">v-v:tense-v:pred.pn</ta>
            <ta e="T594" id="Seg_7554" s="T593">dempro-pro:case</ta>
            <ta e="T595" id="Seg_7555" s="T594">n-n:poss-n:case</ta>
            <ta e="T596" id="Seg_7556" s="T595">dempro</ta>
            <ta e="T597" id="Seg_7557" s="T596">n-n:case</ta>
            <ta e="T598" id="Seg_7558" s="T597">v-v:cvb</ta>
            <ta e="T599" id="Seg_7559" s="T598">v-v:tense-v:pred.pn</ta>
            <ta e="T600" id="Seg_7560" s="T599">que</ta>
            <ta e="T601" id="Seg_7561" s="T600">ptcl</ta>
            <ta e="T602" id="Seg_7562" s="T601">v-v:ptcp-v:(poss)-v:(case)</ta>
            <ta e="T603" id="Seg_7563" s="T602">adv</ta>
            <ta e="T604" id="Seg_7564" s="T603">n-n:(poss)</ta>
            <ta e="T605" id="Seg_7565" s="T604">ptcl</ta>
            <ta e="T606" id="Seg_7566" s="T605">n-n:case</ta>
            <ta e="T607" id="Seg_7567" s="T606">ptcl</ta>
            <ta e="T608" id="Seg_7568" s="T607">n-n:(poss)-n:case</ta>
            <ta e="T609" id="Seg_7569" s="T608">n-n&gt;adj</ta>
            <ta e="T610" id="Seg_7570" s="T609">v-v:cvb</ta>
            <ta e="T611" id="Seg_7571" s="T610">v-v:tense-v:pred.pn</ta>
            <ta e="T612" id="Seg_7572" s="T611">interj</ta>
            <ta e="T613" id="Seg_7573" s="T612">propr-n:case</ta>
            <ta e="T614" id="Seg_7574" s="T613">v-v:mood.pn</ta>
            <ta e="T615" id="Seg_7575" s="T614">n-n:case</ta>
            <ta e="T616" id="Seg_7576" s="T615">v-v:cvb</ta>
            <ta e="T617" id="Seg_7577" s="T616">v-v:tense-v:poss.pn</ta>
            <ta e="T618" id="Seg_7578" s="T617">que</ta>
            <ta e="T619" id="Seg_7579" s="T618">ptcl</ta>
            <ta e="T620" id="Seg_7580" s="T619">v-v:mood.pn</ta>
            <ta e="T621" id="Seg_7581" s="T620">n-n:case</ta>
            <ta e="T622" id="Seg_7582" s="T621">v-v:mood.pn</ta>
            <ta e="T623" id="Seg_7583" s="T622">adv</ta>
            <ta e="T624" id="Seg_7584" s="T623">adv&gt;adv-adv</ta>
            <ta e="T625" id="Seg_7585" s="T624">dempro</ta>
            <ta e="T626" id="Seg_7586" s="T625">propr-n:case</ta>
            <ta e="T627" id="Seg_7587" s="T626">v-v:tense-v:pred.pn</ta>
            <ta e="T628" id="Seg_7588" s="T627">que</ta>
            <ta e="T631" id="Seg_7589" s="T630">dempro</ta>
            <ta e="T632" id="Seg_7590" s="T631">n-n:case</ta>
            <ta e="T633" id="Seg_7591" s="T632">n-n&gt;v-v:cvb</ta>
            <ta e="T634" id="Seg_7592" s="T633">v-v:tense-v:pred.pn</ta>
            <ta e="T635" id="Seg_7593" s="T634">n-n:poss-n:case</ta>
            <ta e="T636" id="Seg_7594" s="T635">adv&gt;adv-adv</ta>
            <ta e="T637" id="Seg_7595" s="T636">adj</ta>
            <ta e="T638" id="Seg_7596" s="T637">v-v:tense-v:pred.pn</ta>
            <ta e="T639" id="Seg_7597" s="T638">v-v:mood.pn</ta>
            <ta e="T640" id="Seg_7598" s="T639">dempro-pro:(num)-pro:case</ta>
            <ta e="T641" id="Seg_7599" s="T640">ptcl</ta>
            <ta e="T642" id="Seg_7600" s="T641">v-v:tense-v:pred.pn</ta>
            <ta e="T643" id="Seg_7601" s="T642">adj-n:(num)-n:case</ta>
            <ta e="T644" id="Seg_7602" s="T643">conj</ta>
            <ta e="T645" id="Seg_7603" s="T644">propr-n:case</ta>
            <ta e="T646" id="Seg_7604" s="T645">n-n:case</ta>
            <ta e="T647" id="Seg_7605" s="T646">n-n:(poss)-n:case</ta>
            <ta e="T648" id="Seg_7606" s="T647">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T652" id="Seg_7607" s="T651">ptcl</ta>
            <ta e="T653" id="Seg_7608" s="T652">adv</ta>
            <ta e="T654" id="Seg_7609" s="T653">dempro</ta>
            <ta e="T655" id="Seg_7610" s="T654">propr-n:(ins)-n:(poss)-n:case</ta>
            <ta e="T656" id="Seg_7611" s="T655">n-n:(poss)-n:case</ta>
            <ta e="T657" id="Seg_7612" s="T656">ptcl</ta>
            <ta e="T658" id="Seg_7613" s="T657">n-n:case</ta>
            <ta e="T659" id="Seg_7614" s="T658">propr-n:case</ta>
            <ta e="T660" id="Seg_7615" s="T659">n-n:(poss)-n:case</ta>
            <ta e="T661" id="Seg_7616" s="T660">n-n:case</ta>
            <ta e="T662" id="Seg_7617" s="T661">dempro</ta>
            <ta e="T663" id="Seg_7618" s="T662">propr</ta>
            <ta e="T664" id="Seg_7619" s="T663">n-n:case</ta>
            <ta e="T665" id="Seg_7620" s="T664">dempro-pro:(poss)-pro:(poss)-pro:case</ta>
            <ta e="T666" id="Seg_7621" s="T665">n-n:(poss)-n:case</ta>
            <ta e="T667" id="Seg_7622" s="T666">pers-pro:case</ta>
            <ta e="T668" id="Seg_7623" s="T667">n-n:poss-n:case</ta>
            <ta e="T669" id="Seg_7624" s="T668">post</ta>
            <ta e="T670" id="Seg_7625" s="T669">adv</ta>
            <ta e="T671" id="Seg_7626" s="T670">v-v:tense-v:pred.pn</ta>
            <ta e="T672" id="Seg_7627" s="T671">propr</ta>
            <ta e="T673" id="Seg_7628" s="T672">n-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps-KiPP">
            <ta e="T1" id="Seg_7629" s="T0">propr</ta>
            <ta e="T2" id="Seg_7630" s="T1">v</ta>
            <ta e="T5" id="Seg_7631" s="T4">n</ta>
            <ta e="T6" id="Seg_7632" s="T5">v</ta>
            <ta e="T7" id="Seg_7633" s="T6">aux</ta>
            <ta e="T8" id="Seg_7634" s="T7">adv</ta>
            <ta e="T9" id="Seg_7635" s="T8">n</ta>
            <ta e="T10" id="Seg_7636" s="T9">dempro</ta>
            <ta e="T11" id="Seg_7637" s="T10">n</ta>
            <ta e="T12" id="Seg_7638" s="T11">v</ta>
            <ta e="T13" id="Seg_7639" s="T12">posspr</ta>
            <ta e="T14" id="Seg_7640" s="T13">n</ta>
            <ta e="T15" id="Seg_7641" s="T14">n</ta>
            <ta e="T16" id="Seg_7642" s="T15">dempro</ta>
            <ta e="T17" id="Seg_7643" s="T16">n</ta>
            <ta e="T18" id="Seg_7644" s="T17">n</ta>
            <ta e="T19" id="Seg_7645" s="T18">propr</ta>
            <ta e="T20" id="Seg_7646" s="T19">n</ta>
            <ta e="T21" id="Seg_7647" s="T20">propr</ta>
            <ta e="T22" id="Seg_7648" s="T21">n</ta>
            <ta e="T23" id="Seg_7649" s="T22">n</ta>
            <ta e="T24" id="Seg_7650" s="T23">dempro</ta>
            <ta e="T25" id="Seg_7651" s="T24">v</ta>
            <ta e="T26" id="Seg_7652" s="T25">v</ta>
            <ta e="T27" id="Seg_7653" s="T26">dempro</ta>
            <ta e="T28" id="Seg_7654" s="T27">propr</ta>
            <ta e="T29" id="Seg_7655" s="T28">ptcl</ta>
            <ta e="T30" id="Seg_7656" s="T29">n</ta>
            <ta e="T31" id="Seg_7657" s="T30">v</ta>
            <ta e="T32" id="Seg_7658" s="T31">propr</ta>
            <ta e="T33" id="Seg_7659" s="T32">n</ta>
            <ta e="T34" id="Seg_7660" s="T33">cardnum</ta>
            <ta e="T35" id="Seg_7661" s="T34">adj</ta>
            <ta e="T36" id="Seg_7662" s="T35">dempro</ta>
            <ta e="T37" id="Seg_7663" s="T36">n</ta>
            <ta e="T38" id="Seg_7664" s="T37">v</ta>
            <ta e="T39" id="Seg_7665" s="T38">aux</ta>
            <ta e="T40" id="Seg_7666" s="T39">n</ta>
            <ta e="T41" id="Seg_7667" s="T40">v</ta>
            <ta e="T42" id="Seg_7668" s="T41">ptcl</ta>
            <ta e="T43" id="Seg_7669" s="T42">adj</ta>
            <ta e="T44" id="Seg_7670" s="T43">n</ta>
            <ta e="T45" id="Seg_7671" s="T44">v</ta>
            <ta e="T46" id="Seg_7672" s="T45">adj</ta>
            <ta e="T47" id="Seg_7673" s="T46">n</ta>
            <ta e="T48" id="Seg_7674" s="T47">v</ta>
            <ta e="T49" id="Seg_7675" s="T48">adv</ta>
            <ta e="T50" id="Seg_7676" s="T49">propr</ta>
            <ta e="T51" id="Seg_7677" s="T50">n</ta>
            <ta e="T52" id="Seg_7678" s="T51">v</ta>
            <ta e="T53" id="Seg_7679" s="T52">ptcl</ta>
            <ta e="T54" id="Seg_7680" s="T53">pers</ta>
            <ta e="T55" id="Seg_7681" s="T54">n</ta>
            <ta e="T56" id="Seg_7682" s="T55">n</ta>
            <ta e="T57" id="Seg_7683" s="T56">n</ta>
            <ta e="T58" id="Seg_7684" s="T57">dempro</ta>
            <ta e="T59" id="Seg_7685" s="T58">n</ta>
            <ta e="T60" id="Seg_7686" s="T59">que</ta>
            <ta e="T61" id="Seg_7687" s="T60">ptcl</ta>
            <ta e="T62" id="Seg_7688" s="T61">v</ta>
            <ta e="T63" id="Seg_7689" s="T62">n</ta>
            <ta e="T64" id="Seg_7690" s="T63">cop</ta>
            <ta e="T65" id="Seg_7691" s="T64">adv</ta>
            <ta e="T66" id="Seg_7692" s="T65">adj</ta>
            <ta e="T67" id="Seg_7693" s="T66">n</ta>
            <ta e="T68" id="Seg_7694" s="T67">dempro</ta>
            <ta e="T69" id="Seg_7695" s="T68">n</ta>
            <ta e="T70" id="Seg_7696" s="T69">v</ta>
            <ta e="T71" id="Seg_7697" s="T70">n</ta>
            <ta e="T72" id="Seg_7698" s="T71">v</ta>
            <ta e="T73" id="Seg_7699" s="T72">post</ta>
            <ta e="T74" id="Seg_7700" s="T73">propr</ta>
            <ta e="T75" id="Seg_7701" s="T74">n</ta>
            <ta e="T76" id="Seg_7702" s="T75">v</ta>
            <ta e="T77" id="Seg_7703" s="T76">ptcl</ta>
            <ta e="T78" id="Seg_7704" s="T77">adv</ta>
            <ta e="T79" id="Seg_7705" s="T78">n</ta>
            <ta e="T80" id="Seg_7706" s="T79">v</ta>
            <ta e="T81" id="Seg_7707" s="T80">ptcl</ta>
            <ta e="T82" id="Seg_7708" s="T81">ptcl</ta>
            <ta e="T83" id="Seg_7709" s="T82">ptcl</ta>
            <ta e="T84" id="Seg_7710" s="T83">n</ta>
            <ta e="T85" id="Seg_7711" s="T84">v</ta>
            <ta e="T86" id="Seg_7712" s="T85">dempro</ta>
            <ta e="T87" id="Seg_7713" s="T86">n</ta>
            <ta e="T88" id="Seg_7714" s="T87">n</ta>
            <ta e="T89" id="Seg_7715" s="T88">dempro</ta>
            <ta e="T90" id="Seg_7716" s="T89">propr</ta>
            <ta e="T91" id="Seg_7717" s="T90">adj</ta>
            <ta e="T92" id="Seg_7718" s="T91">n</ta>
            <ta e="T93" id="Seg_7719" s="T92">n</ta>
            <ta e="T94" id="Seg_7720" s="T93">v</ta>
            <ta e="T95" id="Seg_7721" s="T94">v</ta>
            <ta e="T96" id="Seg_7722" s="T95">ptcl</ta>
            <ta e="T97" id="Seg_7723" s="T96">dempro</ta>
            <ta e="T98" id="Seg_7724" s="T97">n</ta>
            <ta e="T99" id="Seg_7725" s="T98">post</ta>
            <ta e="T100" id="Seg_7726" s="T99">n</ta>
            <ta e="T101" id="Seg_7727" s="T100">v</ta>
            <ta e="T102" id="Seg_7728" s="T101">aux</ta>
            <ta e="T103" id="Seg_7729" s="T102">dempro</ta>
            <ta e="T104" id="Seg_7730" s="T103">n</ta>
            <ta e="T105" id="Seg_7731" s="T104">n</ta>
            <ta e="T106" id="Seg_7732" s="T105">post</ta>
            <ta e="T107" id="Seg_7733" s="T106">v</ta>
            <ta e="T108" id="Seg_7734" s="T107">v</ta>
            <ta e="T109" id="Seg_7735" s="T108">aux</ta>
            <ta e="T110" id="Seg_7736" s="T109">ptcl</ta>
            <ta e="T111" id="Seg_7737" s="T110">n</ta>
            <ta e="T112" id="Seg_7738" s="T111">v</ta>
            <ta e="T113" id="Seg_7739" s="T112">n</ta>
            <ta e="T114" id="Seg_7740" s="T113">n</ta>
            <ta e="T115" id="Seg_7741" s="T114">dempro</ta>
            <ta e="T116" id="Seg_7742" s="T115">dempro</ta>
            <ta e="T119" id="Seg_7743" s="T118">propr</ta>
            <ta e="T120" id="Seg_7744" s="T119">v</ta>
            <ta e="T121" id="Seg_7745" s="T120">ptcl</ta>
            <ta e="T122" id="Seg_7746" s="T121">interj</ta>
            <ta e="T123" id="Seg_7747" s="T122">adj</ta>
            <ta e="T124" id="Seg_7748" s="T123">n</ta>
            <ta e="T125" id="Seg_7749" s="T124">conj</ta>
            <ta e="T126" id="Seg_7750" s="T125">adj</ta>
            <ta e="T127" id="Seg_7751" s="T126">n</ta>
            <ta e="T128" id="Seg_7752" s="T127">ptcl</ta>
            <ta e="T129" id="Seg_7753" s="T128">cop</ta>
            <ta e="T131" id="Seg_7754" s="T130">ptcl</ta>
            <ta e="T132" id="Seg_7755" s="T131">dempro</ta>
            <ta e="T133" id="Seg_7756" s="T132">n</ta>
            <ta e="T134" id="Seg_7757" s="T133">propr</ta>
            <ta e="T135" id="Seg_7758" s="T134">ptcl</ta>
            <ta e="T136" id="Seg_7759" s="T135">dempro</ta>
            <ta e="T137" id="Seg_7760" s="T136">dempro</ta>
            <ta e="T138" id="Seg_7761" s="T137">n</ta>
            <ta e="T139" id="Seg_7762" s="T138">v</ta>
            <ta e="T140" id="Seg_7763" s="T139">v</ta>
            <ta e="T141" id="Seg_7764" s="T140">v</ta>
            <ta e="T142" id="Seg_7765" s="T141">ptcl</ta>
            <ta e="T143" id="Seg_7766" s="T142">dempro</ta>
            <ta e="T144" id="Seg_7767" s="T143">que</ta>
            <ta e="T145" id="Seg_7768" s="T144">n</ta>
            <ta e="T146" id="Seg_7769" s="T145">v</ta>
            <ta e="T147" id="Seg_7770" s="T146">adv</ta>
            <ta e="T148" id="Seg_7771" s="T147">v</ta>
            <ta e="T149" id="Seg_7772" s="T148">v</ta>
            <ta e="T150" id="Seg_7773" s="T149">n</ta>
            <ta e="T151" id="Seg_7774" s="T150">ptcl</ta>
            <ta e="T152" id="Seg_7775" s="T151">ptcl</ta>
            <ta e="T153" id="Seg_7776" s="T152">dempro</ta>
            <ta e="T154" id="Seg_7777" s="T153">pers</ta>
            <ta e="T155" id="Seg_7778" s="T154">n</ta>
            <ta e="T156" id="Seg_7779" s="T155">que</ta>
            <ta e="T157" id="Seg_7780" s="T156">ptcl</ta>
            <ta e="T158" id="Seg_7781" s="T157">adv</ta>
            <ta e="T159" id="Seg_7782" s="T158">v</ta>
            <ta e="T160" id="Seg_7783" s="T159">n</ta>
            <ta e="T161" id="Seg_7784" s="T160">ptcl</ta>
            <ta e="T162" id="Seg_7785" s="T161">n</ta>
            <ta e="T163" id="Seg_7786" s="T162">v</ta>
            <ta e="T164" id="Seg_7787" s="T163">post</ta>
            <ta e="T165" id="Seg_7788" s="T164">v</ta>
            <ta e="T166" id="Seg_7789" s="T165">v</ta>
            <ta e="T167" id="Seg_7790" s="T166">aux</ta>
            <ta e="T168" id="Seg_7791" s="T167">v</ta>
            <ta e="T169" id="Seg_7792" s="T168">n</ta>
            <ta e="T170" id="Seg_7793" s="T169">dempro</ta>
            <ta e="T171" id="Seg_7794" s="T170">n</ta>
            <ta e="T172" id="Seg_7795" s="T171">ptcl</ta>
            <ta e="T173" id="Seg_7796" s="T172">v</ta>
            <ta e="T174" id="Seg_7797" s="T173">v</ta>
            <ta e="T175" id="Seg_7798" s="T174">v</ta>
            <ta e="T176" id="Seg_7799" s="T175">n</ta>
            <ta e="T177" id="Seg_7800" s="T176">v</ta>
            <ta e="T178" id="Seg_7801" s="T177">aux</ta>
            <ta e="T179" id="Seg_7802" s="T178">adv</ta>
            <ta e="T180" id="Seg_7803" s="T179">ptcl</ta>
            <ta e="T181" id="Seg_7804" s="T180">n</ta>
            <ta e="T182" id="Seg_7805" s="T181">n</ta>
            <ta e="T183" id="Seg_7806" s="T182">n</ta>
            <ta e="T184" id="Seg_7807" s="T183">v</ta>
            <ta e="T185" id="Seg_7808" s="T184">v</ta>
            <ta e="T186" id="Seg_7809" s="T185">ptcl</ta>
            <ta e="T187" id="Seg_7810" s="T186">cardnum</ta>
            <ta e="T188" id="Seg_7811" s="T187">n</ta>
            <ta e="T189" id="Seg_7812" s="T188">ptcl</ta>
            <ta e="T190" id="Seg_7813" s="T189">que</ta>
            <ta e="T191" id="Seg_7814" s="T190">v</ta>
            <ta e="T192" id="Seg_7815" s="T191">dempro</ta>
            <ta e="T193" id="Seg_7816" s="T192">v</ta>
            <ta e="T194" id="Seg_7817" s="T193">ptcl</ta>
            <ta e="T210" id="Seg_7818" s="T209">dempro</ta>
            <ta e="T211" id="Seg_7819" s="T210">v</ta>
            <ta e="T212" id="Seg_7820" s="T211">v</ta>
            <ta e="T213" id="Seg_7821" s="T212">v</ta>
            <ta e="T214" id="Seg_7822" s="T213">cardnum</ta>
            <ta e="T215" id="Seg_7823" s="T214">adj</ta>
            <ta e="T216" id="Seg_7824" s="T215">dempro</ta>
            <ta e="T217" id="Seg_7825" s="T216">n</ta>
            <ta e="T218" id="Seg_7826" s="T217">n</ta>
            <ta e="T219" id="Seg_7827" s="T218">ptcl</ta>
            <ta e="T220" id="Seg_7828" s="T219">cop</ta>
            <ta e="T221" id="Seg_7829" s="T220">adv</ta>
            <ta e="T222" id="Seg_7830" s="T221">ptcl</ta>
            <ta e="T223" id="Seg_7831" s="T222">n</ta>
            <ta e="T224" id="Seg_7832" s="T223">interj</ta>
            <ta e="T225" id="Seg_7833" s="T224">adj</ta>
            <ta e="T226" id="Seg_7834" s="T225">n</ta>
            <ta e="T227" id="Seg_7835" s="T226">v</ta>
            <ta e="T228" id="Seg_7836" s="T227">dempro</ta>
            <ta e="T229" id="Seg_7837" s="T228">n</ta>
            <ta e="T230" id="Seg_7838" s="T229">v</ta>
            <ta e="T231" id="Seg_7839" s="T230">v</ta>
            <ta e="T232" id="Seg_7840" s="T231">v</ta>
            <ta e="T233" id="Seg_7841" s="T232">dempro</ta>
            <ta e="T234" id="Seg_7842" s="T233">n</ta>
            <ta e="T235" id="Seg_7843" s="T234">ptcl</ta>
            <ta e="T236" id="Seg_7844" s="T235">v</ta>
            <ta e="T237" id="Seg_7845" s="T236">dempro</ta>
            <ta e="T238" id="Seg_7846" s="T237">n</ta>
            <ta e="T239" id="Seg_7847" s="T238">v</ta>
            <ta e="T240" id="Seg_7848" s="T239">v</ta>
            <ta e="T241" id="Seg_7849" s="T240">que</ta>
            <ta e="T242" id="Seg_7850" s="T241">ptcl</ta>
            <ta e="T243" id="Seg_7851" s="T242">n</ta>
            <ta e="T244" id="Seg_7852" s="T243">v</ta>
            <ta e="T245" id="Seg_7853" s="T244">ptcl</ta>
            <ta e="T246" id="Seg_7854" s="T245">ptcl</ta>
            <ta e="T247" id="Seg_7855" s="T246">n</ta>
            <ta e="T248" id="Seg_7856" s="T247">ptcl</ta>
            <ta e="T249" id="Seg_7857" s="T248">que</ta>
            <ta e="T250" id="Seg_7858" s="T249">ptcl</ta>
            <ta e="T251" id="Seg_7859" s="T250">n</ta>
            <ta e="T252" id="Seg_7860" s="T251">n</ta>
            <ta e="T253" id="Seg_7861" s="T252">ptcl</ta>
            <ta e="T254" id="Seg_7862" s="T253">n</ta>
            <ta e="T255" id="Seg_7863" s="T254">v</ta>
            <ta e="T256" id="Seg_7864" s="T255">n</ta>
            <ta e="T257" id="Seg_7865" s="T256">ptcl</ta>
            <ta e="T258" id="Seg_7866" s="T257">que</ta>
            <ta e="T259" id="Seg_7867" s="T258">ptcl</ta>
            <ta e="T260" id="Seg_7868" s="T259">que</ta>
            <ta e="T261" id="Seg_7869" s="T260">ptcl</ta>
            <ta e="T262" id="Seg_7870" s="T261">v</ta>
            <ta e="T263" id="Seg_7871" s="T262">v</ta>
            <ta e="T264" id="Seg_7872" s="T263">dempro</ta>
            <ta e="T265" id="Seg_7873" s="T264">v</ta>
            <ta e="T266" id="Seg_7874" s="T265">n</ta>
            <ta e="T267" id="Seg_7875" s="T266">v</ta>
            <ta e="T268" id="Seg_7876" s="T267">ptcl</ta>
            <ta e="T269" id="Seg_7877" s="T268">ptcl</ta>
            <ta e="T270" id="Seg_7878" s="T269">dempro</ta>
            <ta e="T271" id="Seg_7879" s="T270">v</ta>
            <ta e="T272" id="Seg_7880" s="T271">ptcl</ta>
            <ta e="T273" id="Seg_7881" s="T272">interj</ta>
            <ta e="T274" id="Seg_7882" s="T273">v</ta>
            <ta e="T275" id="Seg_7883" s="T274">v</ta>
            <ta e="T276" id="Seg_7884" s="T275">que</ta>
            <ta e="T277" id="Seg_7885" s="T276">ptcl</ta>
            <ta e="T278" id="Seg_7886" s="T277">adj</ta>
            <ta e="T279" id="Seg_7887" s="T278">n</ta>
            <ta e="T280" id="Seg_7888" s="T279">adj</ta>
            <ta e="T281" id="Seg_7889" s="T280">adj</ta>
            <ta e="T282" id="Seg_7890" s="T281">interj</ta>
            <ta e="T285" id="Seg_7891" s="T284">n</ta>
            <ta e="T286" id="Seg_7892" s="T285">adv</ta>
            <ta e="T287" id="Seg_7893" s="T286">v</ta>
            <ta e="T288" id="Seg_7894" s="T287">v</ta>
            <ta e="T289" id="Seg_7895" s="T288">ptcl</ta>
            <ta e="T290" id="Seg_7896" s="T289">n</ta>
            <ta e="T291" id="Seg_7897" s="T290">n</ta>
            <ta e="T292" id="Seg_7898" s="T291">propr</ta>
            <ta e="T293" id="Seg_7899" s="T292">dempro</ta>
            <ta e="T294" id="Seg_7900" s="T293">interj</ta>
            <ta e="T295" id="Seg_7901" s="T294">v</ta>
            <ta e="T296" id="Seg_7902" s="T295">v</ta>
            <ta e="T297" id="Seg_7903" s="T296">cardnum</ta>
            <ta e="T298" id="Seg_7904" s="T297">n</ta>
            <ta e="T299" id="Seg_7905" s="T298">v</ta>
            <ta e="T300" id="Seg_7906" s="T299">aux</ta>
            <ta e="T301" id="Seg_7907" s="T300">adv</ta>
            <ta e="T302" id="Seg_7908" s="T301">dempro</ta>
            <ta e="T303" id="Seg_7909" s="T302">n</ta>
            <ta e="T304" id="Seg_7910" s="T303">adj</ta>
            <ta e="T305" id="Seg_7911" s="T304">adj</ta>
            <ta e="T306" id="Seg_7912" s="T305">v</ta>
            <ta e="T307" id="Seg_7913" s="T306">adv</ta>
            <ta e="T308" id="Seg_7914" s="T307">dempro</ta>
            <ta e="T309" id="Seg_7915" s="T308">n</ta>
            <ta e="T310" id="Seg_7916" s="T309">ptcl</ta>
            <ta e="T311" id="Seg_7917" s="T310">adj</ta>
            <ta e="T312" id="Seg_7918" s="T311">cop</ta>
            <ta e="T313" id="Seg_7919" s="T312">dempro</ta>
            <ta e="T314" id="Seg_7920" s="T313">adv</ta>
            <ta e="T315" id="Seg_7921" s="T314">v</ta>
            <ta e="T316" id="Seg_7922" s="T315">v</ta>
            <ta e="T317" id="Seg_7923" s="T316">n</ta>
            <ta e="T318" id="Seg_7924" s="T317">v</ta>
            <ta e="T319" id="Seg_7925" s="T318">v</ta>
            <ta e="T320" id="Seg_7926" s="T319">n</ta>
            <ta e="T321" id="Seg_7927" s="T320">n</ta>
            <ta e="T322" id="Seg_7928" s="T321">ptcl</ta>
            <ta e="T323" id="Seg_7929" s="T322">dempro</ta>
            <ta e="T324" id="Seg_7930" s="T323">n</ta>
            <ta e="T325" id="Seg_7931" s="T324">v</ta>
            <ta e="T326" id="Seg_7932" s="T325">ptcl</ta>
            <ta e="T327" id="Seg_7933" s="T326">pers</ta>
            <ta e="T328" id="Seg_7934" s="T327">pers</ta>
            <ta e="T329" id="Seg_7935" s="T328">v</ta>
            <ta e="T330" id="Seg_7936" s="T329">dempro</ta>
            <ta e="T331" id="Seg_7937" s="T330">que</ta>
            <ta e="T332" id="Seg_7938" s="T331">ptcl</ta>
            <ta e="T333" id="Seg_7939" s="T332">v</ta>
            <ta e="T334" id="Seg_7940" s="T333">n</ta>
            <ta e="T335" id="Seg_7941" s="T334">v</ta>
            <ta e="T336" id="Seg_7942" s="T335">v</ta>
            <ta e="T337" id="Seg_7943" s="T336">n</ta>
            <ta e="T338" id="Seg_7944" s="T337">v</ta>
            <ta e="T339" id="Seg_7945" s="T338">ptcl</ta>
            <ta e="T340" id="Seg_7946" s="T339">dempro</ta>
            <ta e="T341" id="Seg_7947" s="T340">n</ta>
            <ta e="T342" id="Seg_7948" s="T341">dempro</ta>
            <ta e="T343" id="Seg_7949" s="T342">ptcl</ta>
            <ta e="T344" id="Seg_7950" s="T343">v</ta>
            <ta e="T345" id="Seg_7951" s="T344">aux</ta>
            <ta e="T346" id="Seg_7952" s="T345">dempro</ta>
            <ta e="T347" id="Seg_7953" s="T346">v</ta>
            <ta e="T348" id="Seg_7954" s="T347">aux</ta>
            <ta e="T349" id="Seg_7955" s="T348">n</ta>
            <ta e="T350" id="Seg_7956" s="T349">v</ta>
            <ta e="T351" id="Seg_7957" s="T350">v</ta>
            <ta e="T352" id="Seg_7958" s="T351">aux</ta>
            <ta e="T353" id="Seg_7959" s="T352">propr</ta>
            <ta e="T354" id="Seg_7960" s="T353">v</ta>
            <ta e="T357" id="Seg_7961" s="T356">dempro</ta>
            <ta e="T358" id="Seg_7962" s="T357">v</ta>
            <ta e="T359" id="Seg_7963" s="T358">que</ta>
            <ta e="T360" id="Seg_7964" s="T359">v</ta>
            <ta e="T361" id="Seg_7965" s="T360">v</ta>
            <ta e="T362" id="Seg_7966" s="T361">ptcl</ta>
            <ta e="T363" id="Seg_7967" s="T362">n</ta>
            <ta e="T364" id="Seg_7968" s="T363">v</ta>
            <ta e="T365" id="Seg_7969" s="T364">v</ta>
            <ta e="T366" id="Seg_7970" s="T365">n</ta>
            <ta e="T367" id="Seg_7971" s="T366">v</ta>
            <ta e="T368" id="Seg_7972" s="T367">n</ta>
            <ta e="T369" id="Seg_7973" s="T368">n</ta>
            <ta e="T370" id="Seg_7974" s="T369">v</ta>
            <ta e="T371" id="Seg_7975" s="T370">v</ta>
            <ta e="T372" id="Seg_7976" s="T371">n</ta>
            <ta e="T373" id="Seg_7977" s="T372">n</ta>
            <ta e="T374" id="Seg_7978" s="T373">v</ta>
            <ta e="T375" id="Seg_7979" s="T374">aux</ta>
            <ta e="T376" id="Seg_7980" s="T375">v</ta>
            <ta e="T377" id="Seg_7981" s="T376">v</ta>
            <ta e="T378" id="Seg_7982" s="T377">aux</ta>
            <ta e="T379" id="Seg_7983" s="T378">ptcl</ta>
            <ta e="T380" id="Seg_7984" s="T379">n</ta>
            <ta e="T381" id="Seg_7985" s="T380">ptcl</ta>
            <ta e="T382" id="Seg_7986" s="T381">adj</ta>
            <ta e="T383" id="Seg_7987" s="T382">n</ta>
            <ta e="T384" id="Seg_7988" s="T383">v</ta>
            <ta e="T385" id="Seg_7989" s="T384">post</ta>
            <ta e="T386" id="Seg_7990" s="T385">ptcl</ta>
            <ta e="T387" id="Seg_7991" s="T386">v</ta>
            <ta e="T388" id="Seg_7992" s="T387">que</ta>
            <ta e="T389" id="Seg_7993" s="T388">ptcl</ta>
            <ta e="T390" id="Seg_7994" s="T389">v</ta>
            <ta e="T391" id="Seg_7995" s="T390">dempro</ta>
            <ta e="T392" id="Seg_7996" s="T391">v</ta>
            <ta e="T393" id="Seg_7997" s="T392">ptcl</ta>
            <ta e="T394" id="Seg_7998" s="T393">adv</ta>
            <ta e="T395" id="Seg_7999" s="T394">n</ta>
            <ta e="T396" id="Seg_8000" s="T395">n</ta>
            <ta e="T397" id="Seg_8001" s="T396">v</ta>
            <ta e="T398" id="Seg_8002" s="T397">v</ta>
            <ta e="T399" id="Seg_8003" s="T398">post</ta>
            <ta e="T400" id="Seg_8004" s="T399">ptcl</ta>
            <ta e="T401" id="Seg_8005" s="T400">dempro</ta>
            <ta e="T402" id="Seg_8006" s="T401">n</ta>
            <ta e="T403" id="Seg_8007" s="T402">v</ta>
            <ta e="T404" id="Seg_8008" s="T403">n</ta>
            <ta e="T405" id="Seg_8009" s="T404">ptcl</ta>
            <ta e="T406" id="Seg_8010" s="T405">v</ta>
            <ta e="T407" id="Seg_8011" s="T406">aux</ta>
            <ta e="T408" id="Seg_8012" s="T407">v</ta>
            <ta e="T409" id="Seg_8013" s="T408">dempro</ta>
            <ta e="T410" id="Seg_8014" s="T409">v</ta>
            <ta e="T411" id="Seg_8015" s="T410">v</ta>
            <ta e="T412" id="Seg_8016" s="T411">ptcl</ta>
            <ta e="T413" id="Seg_8017" s="T412">adv</ta>
            <ta e="T414" id="Seg_8018" s="T413">cardnum</ta>
            <ta e="T415" id="Seg_8019" s="T414">n</ta>
            <ta e="T416" id="Seg_8020" s="T415">adj</ta>
            <ta e="T417" id="Seg_8021" s="T416">adj</ta>
            <ta e="T418" id="Seg_8022" s="T417">v</ta>
            <ta e="T419" id="Seg_8023" s="T418">v</ta>
            <ta e="T420" id="Seg_8024" s="T419">emphpro</ta>
            <ta e="T421" id="Seg_8025" s="T420">que</ta>
            <ta e="T422" id="Seg_8026" s="T421">ptcl</ta>
            <ta e="T423" id="Seg_8027" s="T422">v</ta>
            <ta e="T424" id="Seg_8028" s="T423">v</ta>
            <ta e="T425" id="Seg_8029" s="T424">v</ta>
            <ta e="T426" id="Seg_8030" s="T425">dempro</ta>
            <ta e="T427" id="Seg_8031" s="T426">v</ta>
            <ta e="T428" id="Seg_8032" s="T427">dempro</ta>
            <ta e="T429" id="Seg_8033" s="T428">adv</ta>
            <ta e="T430" id="Seg_8034" s="T429">post</ta>
            <ta e="T431" id="Seg_8035" s="T430">v</ta>
            <ta e="T432" id="Seg_8036" s="T431">adv</ta>
            <ta e="T433" id="Seg_8037" s="T432">propr</ta>
            <ta e="T434" id="Seg_8038" s="T433">ptcl</ta>
            <ta e="T435" id="Seg_8039" s="T434">adv</ta>
            <ta e="T436" id="Seg_8040" s="T435">adv</ta>
            <ta e="T437" id="Seg_8041" s="T436">v</ta>
            <ta e="T438" id="Seg_8042" s="T437">ptcl</ta>
            <ta e="T439" id="Seg_8043" s="T438">v</ta>
            <ta e="T440" id="Seg_8044" s="T439">v</ta>
            <ta e="T441" id="Seg_8045" s="T440">ptcl</ta>
            <ta e="T442" id="Seg_8046" s="T441">n</ta>
            <ta e="T443" id="Seg_8047" s="T442">v</ta>
            <ta e="T444" id="Seg_8048" s="T443">dempro</ta>
            <ta e="T445" id="Seg_8049" s="T444">n</ta>
            <ta e="T446" id="Seg_8050" s="T445">ptcl</ta>
            <ta e="T447" id="Seg_8051" s="T446">propr</ta>
            <ta e="T448" id="Seg_8052" s="T447">v</ta>
            <ta e="T449" id="Seg_8053" s="T448">ptcl</ta>
            <ta e="T450" id="Seg_8054" s="T449">que</ta>
            <ta e="T451" id="Seg_8055" s="T450">ptcl</ta>
            <ta e="T452" id="Seg_8056" s="T451">v</ta>
            <ta e="T453" id="Seg_8057" s="T452">n</ta>
            <ta e="T454" id="Seg_8058" s="T453">adv</ta>
            <ta e="T455" id="Seg_8059" s="T454">que</ta>
            <ta e="T456" id="Seg_8060" s="T455">n</ta>
            <ta e="T457" id="Seg_8061" s="T456">v</ta>
            <ta e="T458" id="Seg_8062" s="T457">ptcl</ta>
            <ta e="T459" id="Seg_8063" s="T458">propr</ta>
            <ta e="T460" id="Seg_8064" s="T459">dempro</ta>
            <ta e="T461" id="Seg_8065" s="T460">n</ta>
            <ta e="T462" id="Seg_8066" s="T461">ptcl</ta>
            <ta e="T463" id="Seg_8067" s="T462">que</ta>
            <ta e="T464" id="Seg_8068" s="T463">v</ta>
            <ta e="T465" id="Seg_8069" s="T464">propr</ta>
            <ta e="T466" id="Seg_8070" s="T465">propr</ta>
            <ta e="T467" id="Seg_8071" s="T466">propr</ta>
            <ta e="T468" id="Seg_8072" s="T467">v</ta>
            <ta e="T469" id="Seg_8073" s="T468">adv</ta>
            <ta e="T470" id="Seg_8074" s="T469">propr</ta>
            <ta e="T471" id="Seg_8075" s="T470">adj</ta>
            <ta e="T472" id="Seg_8076" s="T471">n</ta>
            <ta e="T478" id="Seg_8077" s="T476">ptcl</ta>
            <ta e="T479" id="Seg_8078" s="T478">dempro</ta>
            <ta e="T480" id="Seg_8079" s="T479">emphpro</ta>
            <ta e="T482" id="Seg_8080" s="T480">adj</ta>
            <ta e="T485" id="Seg_8081" s="T482">ptcl</ta>
            <ta e="T486" id="Seg_8082" s="T485">propr</ta>
            <ta e="T487" id="Seg_8083" s="T486">adv</ta>
            <ta e="T488" id="Seg_8084" s="T487">v</ta>
            <ta e="T489" id="Seg_8085" s="T488">aux</ta>
            <ta e="T490" id="Seg_8086" s="T489">dempro</ta>
            <ta e="T491" id="Seg_8087" s="T490">v</ta>
            <ta e="T492" id="Seg_8088" s="T491">v</ta>
            <ta e="T493" id="Seg_8089" s="T492">dempro</ta>
            <ta e="T494" id="Seg_8090" s="T493">post</ta>
            <ta e="T495" id="Seg_8091" s="T494">adv</ta>
            <ta e="T496" id="Seg_8092" s="T495">post</ta>
            <ta e="T497" id="Seg_8093" s="T496">ptcl</ta>
            <ta e="T498" id="Seg_8094" s="T497">dempro</ta>
            <ta e="T499" id="Seg_8095" s="T498">ptcl</ta>
            <ta e="T500" id="Seg_8096" s="T499">dempro</ta>
            <ta e="T501" id="Seg_8097" s="T500">n</ta>
            <ta e="T502" id="Seg_8098" s="T501">v</ta>
            <ta e="T503" id="Seg_8099" s="T502">adv</ta>
            <ta e="T504" id="Seg_8100" s="T503">adj</ta>
            <ta e="T505" id="Seg_8101" s="T504">cop</ta>
            <ta e="T506" id="Seg_8102" s="T505">ptcl</ta>
            <ta e="T507" id="Seg_8103" s="T506">adj</ta>
            <ta e="T508" id="Seg_8104" s="T507">ptcl</ta>
            <ta e="T512" id="Seg_8105" s="T511">n</ta>
            <ta e="T513" id="Seg_8106" s="T512">dempro</ta>
            <ta e="T514" id="Seg_8107" s="T513">n</ta>
            <ta e="T515" id="Seg_8108" s="T514">n</ta>
            <ta e="T516" id="Seg_8109" s="T515">que</ta>
            <ta e="T517" id="Seg_8110" s="T516">n</ta>
            <ta e="T518" id="Seg_8111" s="T517">v</ta>
            <ta e="T519" id="Seg_8112" s="T518">ptcl</ta>
            <ta e="T520" id="Seg_8113" s="T519">adv</ta>
            <ta e="T521" id="Seg_8114" s="T520">v</ta>
            <ta e="T522" id="Seg_8115" s="T521">dempro</ta>
            <ta e="T523" id="Seg_8116" s="T522">n</ta>
            <ta e="T524" id="Seg_8117" s="T523">ptcl</ta>
            <ta e="T525" id="Seg_8118" s="T524">ptcl</ta>
            <ta e="T526" id="Seg_8119" s="T525">adv</ta>
            <ta e="T527" id="Seg_8120" s="T526">v</ta>
            <ta e="T528" id="Seg_8121" s="T527">adv</ta>
            <ta e="T529" id="Seg_8122" s="T528">n</ta>
            <ta e="T530" id="Seg_8123" s="T529">n</ta>
            <ta e="T531" id="Seg_8124" s="T530">v</ta>
            <ta e="T532" id="Seg_8125" s="T531">n</ta>
            <ta e="T533" id="Seg_8126" s="T532">v</ta>
            <ta e="T534" id="Seg_8127" s="T533">v</ta>
            <ta e="T535" id="Seg_8128" s="T534">dempro</ta>
            <ta e="T536" id="Seg_8129" s="T535">n</ta>
            <ta e="T537" id="Seg_8130" s="T536">interj</ta>
            <ta e="T538" id="Seg_8131" s="T537">que</ta>
            <ta e="T539" id="Seg_8132" s="T538">v</ta>
            <ta e="T540" id="Seg_8133" s="T539">v</ta>
            <ta e="T541" id="Seg_8134" s="T540">v</ta>
            <ta e="T542" id="Seg_8135" s="T541">propr</ta>
            <ta e="T543" id="Seg_8136" s="T542">adj</ta>
            <ta e="T544" id="Seg_8137" s="T543">n</ta>
            <ta e="T545" id="Seg_8138" s="T544">adv</ta>
            <ta e="T546" id="Seg_8139" s="T545">adj</ta>
            <ta e="T547" id="Seg_8140" s="T546">propr</ta>
            <ta e="T548" id="Seg_8141" s="T547">ptcl</ta>
            <ta e="T549" id="Seg_8142" s="T548">dempro</ta>
            <ta e="T550" id="Seg_8143" s="T549">n</ta>
            <ta e="T551" id="Seg_8144" s="T550">v</ta>
            <ta e="T552" id="Seg_8145" s="T551">aux</ta>
            <ta e="T553" id="Seg_8146" s="T552">interj</ta>
            <ta e="T554" id="Seg_8147" s="T553">v</ta>
            <ta e="T555" id="Seg_8148" s="T554">aux</ta>
            <ta e="T556" id="Seg_8149" s="T555">n</ta>
            <ta e="T557" id="Seg_8150" s="T556">adv</ta>
            <ta e="T558" id="Seg_8151" s="T557">n</ta>
            <ta e="T561" id="Seg_8152" s="T560">n</ta>
            <ta e="T562" id="Seg_8153" s="T561">n</ta>
            <ta e="T563" id="Seg_8154" s="T562">v</ta>
            <ta e="T564" id="Seg_8155" s="T563">ptcl</ta>
            <ta e="T565" id="Seg_8156" s="T564">adv</ta>
            <ta e="T566" id="Seg_8157" s="T565">cardnum</ta>
            <ta e="T567" id="Seg_8158" s="T566">n</ta>
            <ta e="T568" id="Seg_8159" s="T567">ptcl</ta>
            <ta e="T569" id="Seg_8160" s="T568">cop</ta>
            <ta e="T570" id="Seg_8161" s="T569">ptcl</ta>
            <ta e="T571" id="Seg_8162" s="T570">que</ta>
            <ta e="T572" id="Seg_8163" s="T571">n</ta>
            <ta e="T573" id="Seg_8164" s="T572">v</ta>
            <ta e="T574" id="Seg_8165" s="T573">n</ta>
            <ta e="T575" id="Seg_8166" s="T574">v</ta>
            <ta e="T576" id="Seg_8167" s="T575">ptcl</ta>
            <ta e="T577" id="Seg_8168" s="T576">interj</ta>
            <ta e="T578" id="Seg_8169" s="T577">cardnum</ta>
            <ta e="T579" id="Seg_8170" s="T578">n</ta>
            <ta e="T580" id="Seg_8171" s="T579">propr</ta>
            <ta e="T581" id="Seg_8172" s="T580">v</ta>
            <ta e="T582" id="Seg_8173" s="T581">n</ta>
            <ta e="T583" id="Seg_8174" s="T582">v</ta>
            <ta e="T584" id="Seg_8175" s="T583">v</ta>
            <ta e="T585" id="Seg_8176" s="T584">ptcl</ta>
            <ta e="T586" id="Seg_8177" s="T585">v</ta>
            <ta e="T587" id="Seg_8178" s="T586">dempro</ta>
            <ta e="T588" id="Seg_8179" s="T587">adj</ta>
            <ta e="T589" id="Seg_8180" s="T588">n</ta>
            <ta e="T590" id="Seg_8181" s="T589">ptcl</ta>
            <ta e="T591" id="Seg_8182" s="T590">adv</ta>
            <ta e="T592" id="Seg_8183" s="T591">n</ta>
            <ta e="T593" id="Seg_8184" s="T592">v</ta>
            <ta e="T594" id="Seg_8185" s="T593">dempro</ta>
            <ta e="T595" id="Seg_8186" s="T594">n</ta>
            <ta e="T596" id="Seg_8187" s="T595">dempro</ta>
            <ta e="T597" id="Seg_8188" s="T596">n</ta>
            <ta e="T598" id="Seg_8189" s="T597">v</ta>
            <ta e="T599" id="Seg_8190" s="T598">aux</ta>
            <ta e="T600" id="Seg_8191" s="T599">que</ta>
            <ta e="T601" id="Seg_8192" s="T600">ptcl</ta>
            <ta e="T602" id="Seg_8193" s="T601">v</ta>
            <ta e="T603" id="Seg_8194" s="T602">adv</ta>
            <ta e="T604" id="Seg_8195" s="T603">n</ta>
            <ta e="T605" id="Seg_8196" s="T604">ptcl</ta>
            <ta e="T606" id="Seg_8197" s="T605">n</ta>
            <ta e="T607" id="Seg_8198" s="T606">ptcl</ta>
            <ta e="T608" id="Seg_8199" s="T607">n</ta>
            <ta e="T609" id="Seg_8200" s="T608">adj</ta>
            <ta e="T610" id="Seg_8201" s="T609">v</ta>
            <ta e="T611" id="Seg_8202" s="T610">v</ta>
            <ta e="T612" id="Seg_8203" s="T611">interj</ta>
            <ta e="T613" id="Seg_8204" s="T612">propr</ta>
            <ta e="T614" id="Seg_8205" s="T613">v</ta>
            <ta e="T615" id="Seg_8206" s="T614">n</ta>
            <ta e="T616" id="Seg_8207" s="T615">v</ta>
            <ta e="T617" id="Seg_8208" s="T616">aux</ta>
            <ta e="T618" id="Seg_8209" s="T617">que</ta>
            <ta e="T619" id="Seg_8210" s="T618">ptcl</ta>
            <ta e="T620" id="Seg_8211" s="T619">v</ta>
            <ta e="T621" id="Seg_8212" s="T620">n</ta>
            <ta e="T622" id="Seg_8213" s="T621">v</ta>
            <ta e="T623" id="Seg_8214" s="T622">adv</ta>
            <ta e="T624" id="Seg_8215" s="T623">adv</ta>
            <ta e="T625" id="Seg_8216" s="T624">dempro</ta>
            <ta e="T626" id="Seg_8217" s="T625">propr</ta>
            <ta e="T627" id="Seg_8218" s="T626">v</ta>
            <ta e="T628" id="Seg_8219" s="T627">que</ta>
            <ta e="T631" id="Seg_8220" s="T630">dempro</ta>
            <ta e="T632" id="Seg_8221" s="T631">n</ta>
            <ta e="T633" id="Seg_8222" s="T632">v</ta>
            <ta e="T634" id="Seg_8223" s="T633">v</ta>
            <ta e="T635" id="Seg_8224" s="T634">n</ta>
            <ta e="T636" id="Seg_8225" s="T635">adv</ta>
            <ta e="T637" id="Seg_8226" s="T636">adj</ta>
            <ta e="T638" id="Seg_8227" s="T637">v</ta>
            <ta e="T639" id="Seg_8228" s="T638">v</ta>
            <ta e="T640" id="Seg_8229" s="T639">dempro</ta>
            <ta e="T641" id="Seg_8230" s="T640">ptcl</ta>
            <ta e="T642" id="Seg_8231" s="T641">cop</ta>
            <ta e="T643" id="Seg_8232" s="T642">n</ta>
            <ta e="T644" id="Seg_8233" s="T643">conj</ta>
            <ta e="T645" id="Seg_8234" s="T644">propr</ta>
            <ta e="T646" id="Seg_8235" s="T645">n</ta>
            <ta e="T647" id="Seg_8236" s="T646">n</ta>
            <ta e="T648" id="Seg_8237" s="T647">dempro</ta>
            <ta e="T652" id="Seg_8238" s="T651">ptcl</ta>
            <ta e="T653" id="Seg_8239" s="T652">adv</ta>
            <ta e="T654" id="Seg_8240" s="T653">dempro</ta>
            <ta e="T655" id="Seg_8241" s="T654">propr</ta>
            <ta e="T656" id="Seg_8242" s="T655">n</ta>
            <ta e="T657" id="Seg_8243" s="T656">ptcl</ta>
            <ta e="T658" id="Seg_8244" s="T657">n</ta>
            <ta e="T659" id="Seg_8245" s="T658">propr</ta>
            <ta e="T660" id="Seg_8246" s="T659">n</ta>
            <ta e="T661" id="Seg_8247" s="T660">n</ta>
            <ta e="T662" id="Seg_8248" s="T661">dempro</ta>
            <ta e="T663" id="Seg_8249" s="T662">propr</ta>
            <ta e="T664" id="Seg_8250" s="T663">n</ta>
            <ta e="T665" id="Seg_8251" s="T664">dempro</ta>
            <ta e="T666" id="Seg_8252" s="T665">n</ta>
            <ta e="T667" id="Seg_8253" s="T666">pers</ta>
            <ta e="T668" id="Seg_8254" s="T667">n</ta>
            <ta e="T669" id="Seg_8255" s="T668">post</ta>
            <ta e="T670" id="Seg_8256" s="T669">adv</ta>
            <ta e="T671" id="Seg_8257" s="T670">v</ta>
            <ta e="T672" id="Seg_8258" s="T671">propr</ta>
            <ta e="T673" id="Seg_8259" s="T672">n</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-KiPP" />
         <annotation name="SyF" tierref="SyF-KiPP" />
         <annotation name="IST" tierref="IST-KiPP" />
         <annotation name="Top" tierref="Top-KiPP" />
         <annotation name="Foc" tierref="Foc-KiPP" />
         <annotation name="BOR" tierref="BOR-KiPP">
            <ta e="T1" id="Seg_8260" s="T0">RUS:cult</ta>
            <ta e="T9" id="Seg_8261" s="T8">RUS:cult</ta>
            <ta e="T19" id="Seg_8262" s="T18">RUS:cult</ta>
            <ta e="T21" id="Seg_8263" s="T20">RUS:cult</ta>
            <ta e="T28" id="Seg_8264" s="T27">RUS:cult</ta>
            <ta e="T32" id="Seg_8265" s="T31">RUS:cult</ta>
            <ta e="T74" id="Seg_8266" s="T73">RUS:cult</ta>
            <ta e="T90" id="Seg_8267" s="T89">RUS:cult</ta>
            <ta e="T119" id="Seg_8268" s="T118">RUS:cult</ta>
            <ta e="T125" id="Seg_8269" s="T124">RUS:gram</ta>
            <ta e="T134" id="Seg_8270" s="T133">RUS:cult</ta>
            <ta e="T160" id="Seg_8271" s="T159">RUS:cult</ta>
            <ta e="T169" id="Seg_8272" s="T168">RUS:cult</ta>
            <ta e="T226" id="Seg_8273" s="T225">EV:gram (DIM)</ta>
            <ta e="T262" id="Seg_8274" s="T261">RUS:cult</ta>
            <ta e="T285" id="Seg_8275" s="T284">EV:core</ta>
            <ta e="T290" id="Seg_8276" s="T289">EV:core</ta>
            <ta e="T292" id="Seg_8277" s="T291">RUS:cult</ta>
            <ta e="T311" id="Seg_8278" s="T310">RUS:cult</ta>
            <ta e="T320" id="Seg_8279" s="T319">RUS:cult</ta>
            <ta e="T341" id="Seg_8280" s="T340">RUS:cult</ta>
            <ta e="T353" id="Seg_8281" s="T352">RUS:cult</ta>
            <ta e="T363" id="Seg_8282" s="T362">RUS:cult</ta>
            <ta e="T366" id="Seg_8283" s="T365">RUS:cult</ta>
            <ta e="T380" id="Seg_8284" s="T379">RUS:cult</ta>
            <ta e="T405" id="Seg_8285" s="T404">RUS:mod</ta>
            <ta e="T449" id="Seg_8286" s="T448">RUS:mod</ta>
            <ta e="T456" id="Seg_8287" s="T455">RUS:cult</ta>
            <ta e="T465" id="Seg_8288" s="T464">RUS:cult</ta>
            <ta e="T466" id="Seg_8289" s="T465">RUS:cult</ta>
            <ta e="T467" id="Seg_8290" s="T466">RUS:cult</ta>
            <ta e="T470" id="Seg_8291" s="T469">RUS:cult</ta>
            <ta e="T486" id="Seg_8292" s="T485">RUS:cult</ta>
            <ta e="T515" id="Seg_8293" s="T514">RUS:cult</ta>
            <ta e="T542" id="Seg_8294" s="T541">RUS:cult</ta>
            <ta e="T547" id="Seg_8295" s="T546">NGAN:cult</ta>
            <ta e="T564" id="Seg_8296" s="T563">RUS:disc</ta>
            <ta e="T567" id="Seg_8297" s="T566">RUS:cult</ta>
            <ta e="T572" id="Seg_8298" s="T571">RUS:cult</ta>
            <ta e="T579" id="Seg_8299" s="T578">RUS:cult</ta>
            <ta e="T604" id="Seg_8300" s="T603">RUS:cult</ta>
            <ta e="T608" id="Seg_8301" s="T607">RUS:cult</ta>
            <ta e="T644" id="Seg_8302" s="T643">RUS:gram</ta>
            <ta e="T659" id="Seg_8303" s="T658">RUS:cult</ta>
            <ta e="T672" id="Seg_8304" s="T671">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-KiPP" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-KiPP" />
         <annotation name="CS" tierref="CS-KiPP" />
         <annotation name="fe" tierref="fe-KiPP">
            <ta e="T2" id="Seg_8305" s="T0">– Begichev is brought away.</ta>
            <ta e="T9" id="Seg_8306" s="T2">He was traveling the world before, expeditions. </ta>
            <ta e="T12" id="Seg_8307" s="T9">My grandfather told that.</ta>
            <ta e="T23" id="Seg_8308" s="T12">My mother, (grandfather) my mother's siblings: the old man Markel, Elena's old father.</ta>
            <ta e="T26" id="Seg_8309" s="T23">I heard them chatting.</ta>
            <ta e="T35" id="Seg_8310" s="T26">This Begichev, though, is travelling with reindeer to Begichev Island, he has seven men.</ta>
            <ta e="T48" id="Seg_8311" s="T35">This human travelling: he is taken with reindeer, with completely different reindeer he is taken, with completely new reindeer he is taken.</ta>
            <ta e="T57" id="Seg_8312" s="T48">There they let the old man Chadygaj bring them, my husband's grandfather and the siblings, the grandfather.</ta>
            <ta e="T67" id="Seg_8313" s="T57">The old man, he was such a human, not being forced by anything, a very energetic man.</ta>
            <ta e="T77" id="Seg_8314" s="T67">This old man, after they had made [him] harness untrained reindeer, they made [him] bring them to Begichev island.</ta>
            <ta e="T83" id="Seg_8315" s="T77">There the reindeer baulk and are moving, it is said.</ta>
            <ta e="T96" id="Seg_8316" s="T83">The reindeer are moving, this man's cap, Begichev's cap is taken by a reindeer and thrown away.</ta>
            <ta e="T114" id="Seg_8317" s="T96">As the cap was falling through the sledge, the old man jumped through the sledge and caught the cap before it was falling to the ground.</ta>
            <ta e="T121" id="Seg_8318" s="T114">There this Begichev says: </ta>
            <ta e="T130" id="Seg_8319" s="T121">"Oh, there are energetic people in the Dolgan land (…)."</ta>
            <ta e="T142" id="Seg_8320" s="T130">Well, this old man, Begichev though, "I reached that island", he told.</ta>
            <ta e="T152" id="Seg_8321" s="T142">"And what interesting things did you see there?", the Dolgans ask.</ta>
            <ta e="T153" id="Seg_8322" s="T152">On that:</ta>
            <ta e="T169" id="Seg_8323" s="T153">"I didn't find anything very astonishing, walrusses, though, which are sleeping having fixed their teeth, fixed to a log. </ta>
            <ta e="T178" id="Seg_8324" s="T169">They don't even think about that thing, they pull it in, they pull it into the water.</ta>
            <ta e="T188" id="Seg_8325" s="T178">Then I found on the island on a stony mountain", he says, "something astonishing."</ta>
            <ta e="T191" id="Seg_8326" s="T188">"Well, what did you find?"</ta>
            <ta e="T194" id="Seg_8327" s="T191">There he says:</ta>
            <ta e="T213" id="Seg_8328" s="T209">There I entered", he says.</ta>
            <ta e="T220" id="Seg_8329" s="T213">He has three people, those people don't have anything to eat.</ta>
            <ta e="T232" id="Seg_8330" s="T220">Then from the bay, after they had caught a small fish and entered that door, they went in in order to eat.</ta>
            <ta e="T239" id="Seg_8331" s="T232">This human just had gone in, this human is sitting.</ta>
            <ta e="T246" id="Seg_8332" s="T239">While he was sitting, some human came there, though.</ta>
            <ta e="T250" id="Seg_8333" s="T246">A human or something?</ta>
            <ta e="T263" id="Seg_8334" s="T250">"A human with no head on his shoulders entered, a human or something, I don't understand", he said.</ta>
            <ta e="T269" id="Seg_8335" s="T263">He went in and sat down at the door.</ta>
            <ta e="T273" id="Seg_8336" s="T269">Well, sitting then, oh.</ta>
            <ta e="T281" id="Seg_8337" s="T273">He laughed and bared his teeth, somewhere he has an eye, on his head he has a single eye.</ta>
            <ta e="T292" id="Seg_8338" s="T281">"Well, friends, hold yourselves firmly", Begichev says to his friends, his guys.</ta>
            <ta e="T300" id="Seg_8339" s="T292">That one made "hup" and sucked, he sucked in two people.</ta>
            <ta e="T306" id="Seg_8340" s="T300">There this human stayed with one single human.</ta>
            <ta e="T313" id="Seg_8341" s="T306">There this human had a pistol.</ta>
            <ta e="T321" id="Seg_8342" s="T313">"There I say, I say with speaking tongue", he said, "my pistol in his mouth.</ta>
            <ta e="T327" id="Seg_8343" s="T321">Well, will I stay in this world?"</ta>
            <ta e="T336" id="Seg_8344" s="T327">"Save me, bring that one somewhere, where he loses his consciousness", he says.</ta>
            <ta e="T345" id="Seg_8345" s="T336">He shot into his forehead with his pistol, that one lost consciousness.</ta>
            <ta e="T352" id="Seg_8346" s="T345">As he had lost consciousness, they went up and went out.</ta>
            <ta e="T356" id="Seg_8347" s="T352">Begichev tells:</ta>
            <ta e="T370" id="Seg_8348" s="T356">"We came there", he says, "I went to my motorboat", he says, "I sat down in my motorboat and drove out to the middle of the bay.</ta>
            <ta e="T376" id="Seg_8349" s="T370">I saw that the recent one appeared behind me.</ta>
            <ta e="T390" id="Seg_8350" s="T376">He appeared, he took a stone bigger than the motorboat, and he threw, well, that one which was shot.</ta>
            <ta e="T408" id="Seg_8351" s="T390">As he was throwing I had hardly reached the middle of the bay, as I reached the wave, the wave into which he was throwing, he pulled [it] almost over.</ta>
            <ta e="T412" id="Seg_8352" s="T408">So we escaped", he said though.</ta>
            <ta e="T420" id="Seg_8353" s="T412">There he came with one single person from the seven people, there were themselves starving.</ta>
            <ta e="T431" id="Seg_8354" s="T420">Having found nothing they turned over, having come, they drove upwards.</ta>
            <ta e="T438" id="Seg_8355" s="T431">Then he lived together with Chagyday in summer, it is said.</ta>
            <ta e="T449" id="Seg_8356" s="T438">While they were eating and living, a letter came, they had to bring this letter to Khatanga.</ta>
            <ta e="T457" id="Seg_8357" s="T449">From somewhere he came with a small boat, what kind of boat should come at that time.</ta>
            <ta e="T467" id="Seg_8358" s="T457">"Hey, Chagyday, bring this letter to whatchamacallit, to Kresty, to Kresty at Novorybnoe."</ta>
            <ta e="T472" id="Seg_8359" s="T467">Look, how far away, at the other side of Novorybnoe.</ta>
            <ta e="T478" id="Seg_8360" s="T476">– No.</ta>
            <ta e="T482" id="Seg_8361" s="T478">His people.</ta>
            <ta e="T496" id="Seg_8362" s="T482">– Yes, and Begichev departed at once, driving so he drove in this direction, upwards.</ta>
            <ta e="T510" id="Seg_8363" s="T496">Well, they made [him] bring the letter, there had apparently letters, they had chiefs…</ta>
            <ta e="T512" id="Seg_8364" s="T511">– The Dolgans.</ta>
            <ta e="T516" id="Seg_8365" s="T512">The workes, the Soviets and and son.</ta>
            <ta e="T519" id="Seg_8366" s="T516">They sent a letter, though.</ta>
            <ta e="T527" id="Seg_8367" s="T519">And so…, the old man hits the road in the evening.</ta>
            <ta e="T536" id="Seg_8368" s="T527">"I will come in the morning, at the time when the early woman rises", the old man said.</ta>
            <ta e="T547" id="Seg_8369" s="T536">"Oh, how will you make it to come from the other side of Novorybnoe, from behind the river Popigay?"</ta>
            <ta e="T556" id="Seg_8370" s="T547">Well, the old man hit the road, eh, he drove with his small boat.</ta>
            <ta e="T576" id="Seg_8371" s="T556">In the morning the old women…, the early woman stand up, for example, that was then apparently at seven o' clock, it can be seen at the sun what time it is.</ta>
            <ta e="T590" id="Seg_8372" s="T576">Oh, at seven o' clock Chagyday arrived, his small boat lies there upside down, look, there are such bold people.</ta>
            <ta e="T603" id="Seg_8373" s="T590">Then a woman is giving birth in that settlement, this woman is about to die, one cannot do anything.</ta>
            <ta e="T611" id="Seg_8374" s="T603">At a time where there was no doctor, the poor midwife had retired.</ta>
            <ta e="T623" id="Seg_8375" s="T611">"Well, Chagyday shall come, the woman is about to die, he shall do something, he shall split the bone here."</ta>
            <ta e="T639" id="Seg_8376" s="T623">And so this Chagyday comes, how… he took this woman's child with his hands, so he kept it alive, look.</ta>
            <ta e="T643" id="Seg_8377" s="T639">Such people were the old people.</ta>
            <ta e="T648" id="Seg_8378" s="T643">And that one is Chakyyla's husband's grandfather.</ta>
            <ta e="T664" id="Seg_8379" s="T651">– Yes, then this Chagyday's woman, well, Anna's old mother, this old woman Cheeke.</ta>
            <ta e="T673" id="Seg_8380" s="T664">Her father was born togehter with our grandfather, the old Kudryakov.</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KiPP">
            <ta e="T2" id="Seg_8381" s="T0">– Begitschew wird weggebracht.</ta>
            <ta e="T9" id="Seg_8382" s="T2">Er ging durch die Welt früher, Expeditionen.</ta>
            <ta e="T12" id="Seg_8383" s="T9">Das erzählte mein Großvater.</ta>
            <ta e="T23" id="Seg_8384" s="T12">Meine Mutter, (Großvater) Verwandte meiner Mutter: der alte Mann Markel, Elenas alter Vater.</ta>
            <ta e="T26" id="Seg_8385" s="T23">Die habe ich gehört, wie sie sich unterhielten.</ta>
            <ta e="T35" id="Seg_8386" s="T26">Dieser Begitschew aber reist mit Rentieren zur Begitschew-Insel, er hat sieben Männer.</ta>
            <ta e="T48" id="Seg_8387" s="T35">Dieser Mensch, während er reist: mit Rentieren nimmt man ihn mit, mit ganz unterschiedlichen Rentieren nimmt man ihn mit, mit ganz neuen Rentieren nimmt man ihn mit.</ta>
            <ta e="T57" id="Seg_8388" s="T48">Dort baten sie den alten Mann Tschagydaj, sie zu bringen, von meinem Mann der Großvater und seine Verwandten, der Großvater.</ta>
            <ta e="T67" id="Seg_8389" s="T57">Dieser alte Mann, er war so ein Mensch, der sich von nichts zwingen lässt, ein sehr energischer Mensch.</ta>
            <ta e="T77" id="Seg_8390" s="T67">Dieser alte Mann, nachdem sie untrainierte Rentiere einspannen lassen hatten, ließen sie sich zur Begitschew-Insel bringen.</ta>
            <ta e="T83" id="Seg_8391" s="T77">Da sträuben sich die Rentiere und bewegen sich.</ta>
            <ta e="T96" id="Seg_8392" s="T83">Die Rentiere bewegen sich, die Mütze dieses Menschen, von diesem Begitschew die Mütze wirft ein Rentier weg.</ta>
            <ta e="T114" id="Seg_8393" s="T96">Als die Mütze durch den Schlitten fiel, sprang der alte Mann durch den Schlitten und fing die Mütze, bevor sie auf die Erde fiel.</ta>
            <ta e="T121" id="Seg_8394" s="T114">Darauf sagt dieser Begicev:</ta>
            <ta e="T130" id="Seg_8395" s="T121">"Oh, im dolganischen Land gibt es auch energische Menschen (…)."</ta>
            <ta e="T142" id="Seg_8396" s="T130">Nun, dieser alte Mann, also Begitschew, "zu dieser Insel bin ich gekommen", erzählt er.</ta>
            <ta e="T152" id="Seg_8397" s="T142">"Und was hast du dort Interessantes gesehen?", fragen die Dolganen da.</ta>
            <ta e="T153" id="Seg_8398" s="T152">Darauf:</ta>
            <ta e="T169" id="Seg_8399" s="T153">"Ich habe nichts sehr Erstaunliches gefunden, Walrosse aber, die an ihren Zähne festgebunden schlafen, an einen Holzstamm festgebunden.</ta>
            <ta e="T178" id="Seg_8400" s="T169">Darauf denken sie nicht mal an das Ding, sie ziehen es hinein, sie ziehen es ins Wasser.</ta>
            <ta e="T188" id="Seg_8401" s="T178">Dann habe ich auf der Insel auf einem Steinberg", sagt er, "etwas Erstaunliches gefunden."</ta>
            <ta e="T191" id="Seg_8402" s="T188">"Nun, was hast du gefunden?"</ta>
            <ta e="T194" id="Seg_8403" s="T191">Darauf sagt er:</ta>
            <ta e="T213" id="Seg_8404" s="T209">Dort bin ich hineingegangen", sagt er.</ta>
            <ta e="T220" id="Seg_8405" s="T213">Er hat drei Leute, diese Leute hatten nichts mehr zu essen.</ta>
            <ta e="T232" id="Seg_8406" s="T220">Dann aber von der Bucht, nachdem sie einen kleinen Fisch gefangen hatten und zu der Tür hingegangen waren, gingen sie hinein, um zu essen.</ta>
            <ta e="T239" id="Seg_8407" s="T232">Dieser Mensch ging gerade hinein, dieser Mensch sitzt.</ta>
            <ta e="T246" id="Seg_8408" s="T239">Als er saß, kam irgendein Mensch.</ta>
            <ta e="T250" id="Seg_8409" s="T246">Ein Mensch, oder was?</ta>
            <ta e="T263" id="Seg_8410" s="T250">"Es kam ein Mensch herein, der keinen Kopf auf den Schultern hatte, ein Mensch oder sonst etwas, ich verstehe nichts", sagte er.</ta>
            <ta e="T269" id="Seg_8411" s="T263">Er kam hinein und setzte sich an die Tür.</ta>
            <ta e="T273" id="Seg_8412" s="T269">Nun, sitzend also, oh.</ta>
            <ta e="T281" id="Seg_8413" s="T273">Er lachte und zeigte seine Zähne, irgendwo hat er ein Auge, auf seinem Kopf hat er ein einziges Auge.</ta>
            <ta e="T292" id="Seg_8414" s="T281">"Na, Freunde, haltet euch gut fest", sagt Begitschew zu den Freunden, seinen Jungs.</ta>
            <ta e="T300" id="Seg_8415" s="T292">Jener machte "hup" und saugte, zwei Leute saugte er ein.</ta>
            <ta e="T306" id="Seg_8416" s="T300">Da blieb dieser Mensch mit einem einzigen Menschen zurück.</ta>
            <ta e="T313" id="Seg_8417" s="T306">Da hatte dieser Mensch eine Pistole.</ta>
            <ta e="T321" id="Seg_8418" s="T313">"Da sage ich, sage ich mit sprechender Zunge", sagte er, "meine Pistole in seinem Mund.</ta>
            <ta e="T327" id="Seg_8419" s="T321">Nun, bleibe ich wohl in dieser Welt?"</ta>
            <ta e="T336" id="Seg_8420" s="T327">"Rette mich, bring diesen irgendwohin, wo er sein Bewusstsein verliert", sagt er.</ta>
            <ta e="T345" id="Seg_8421" s="T336">Er schoss ihm mit seiner Pistole in die Stirn, jener verlor das Bewusstsein.</ta>
            <ta e="T352" id="Seg_8422" s="T345">Als jener ohnmächtig wurde, liefen sie hinauf und gingen hinaus.</ta>
            <ta e="T356" id="Seg_8423" s="T352">Begitschew erzählt:</ta>
            <ta e="T370" id="Seg_8424" s="T356">"Wir kamen dahin an", sagt er, "ich ging zu meinem Motorboot", sagt er, "ich setzte mich in mein Motorboot und fuhr in die Mitte der Bucht hinaus.</ta>
            <ta e="T376" id="Seg_8425" s="T370">Ich sah, dass hinter mir der von eben erschien.</ta>
            <ta e="T390" id="Seg_8426" s="T376">Er erschien, nahm einen Stein, der größer war als das Motorboot, und warf, also der, der erschossen wurde.</ta>
            <ta e="T408" id="Seg_8427" s="T390">Als er warf, hatte ich kaum die Mitte der Bucht erreicht, wie ich in der Welle ankam, in der Welle, in die er warf, riss er [es] fast um.</ta>
            <ta e="T412" id="Seg_8428" s="T408">So sind wir entkommen", sagte er.</ta>
            <ta e="T420" id="Seg_8429" s="T412">Da kam er mit einem einzigen Menschen von den sieben Leuten, sie hungerten selbst.</ta>
            <ta e="T431" id="Seg_8430" s="T420">Sie fanden nichts und drehten um, angekommen, fuhren sie nach oben.</ta>
            <ta e="T438" id="Seg_8431" s="T431">Dann lebt er mit Chagydaj im Sommer zusammen, sagt man.</ta>
            <ta e="T449" id="Seg_8432" s="T438">Sie lebten und aßen, es kam ein Brief, diesen Brief mussten sie nach Chatanga bringen.</ta>
            <ta e="T457" id="Seg_8433" s="T449">Irgendwoher kam er mit einem kleinen Boot, was für ein Boot käme damals.</ta>
            <ta e="T467" id="Seg_8434" s="T457">"Hey, Chagadaj, bring diesen Brief dahin, nach Kresty, nach Kresty bei Novorybnoe."</ta>
            <ta e="T472" id="Seg_8435" s="T467">Schau, wie weit, auf der anderen Seite von Novorybnoe.</ta>
            <ta e="T478" id="Seg_8436" s="T476">– Nein.</ta>
            <ta e="T482" id="Seg_8437" s="T478">Die seinen.</ta>
            <ta e="T496" id="Seg_8438" s="T482">– Ja, und Begitschew fuhr sofort los, wie er fuhr, da fuhr er hier in die Richtung, nach oben.</ta>
            <ta e="T510" id="Seg_8439" s="T496">Nun, sie ließen den Brief bringen, damals hatten sie offenbar Briefe, sie hatten Chefs…</ta>
            <ta e="T512" id="Seg_8440" s="T511">– Die Dolganen.</ta>
            <ta e="T516" id="Seg_8441" s="T512">Die Arbeiter, die Sowjets und so.</ta>
            <ta e="T519" id="Seg_8442" s="T516">Einen Brief schickte man also.</ta>
            <ta e="T527" id="Seg_8443" s="T519">Und so…, der alte Mann macht sich am Abend auf den Weg.</ta>
            <ta e="T536" id="Seg_8444" s="T527">"Am Morgen, zu der Zeit, wenn die frühe Frau aufsteht, komme ich", sagte der alte Mann.</ta>
            <ta e="T547" id="Seg_8445" s="T536">"Oh, wie machst du es, von der anderen Seite von Novorybnoe von hinter dem Popigaj zu kommen?"</ta>
            <ta e="T556" id="Seg_8446" s="T547">Nun, der alte Mann machte sich auf den Weg, äh, er fuhr mit seinem kleinen Boot.</ta>
            <ta e="T576" id="Seg_8447" s="T556">Am Morgen die alten Frauen…, die frühen Frauen stehen auf, zum Beispiel, das war damals offenbar um sieben Uhr, welche Uhrzeit ist, das sieht man an der Sonne.</ta>
            <ta e="T590" id="Seg_8448" s="T576">Oh, um sieben Uhr kam Chagyday, sein kleines Boot liegt eingetaucht/umgedreht da, schau, solche kühnen Menschen gibt es.</ta>
            <ta e="T603" id="Seg_8449" s="T590">Und dann gebärt eine Frau in dieser Siedlung, diese Frau starb fast, es ist nichts zu machen.</ta>
            <ta e="T611" id="Seg_8450" s="T603">Zu einer Zeit, wo es keinen Doktor gab, die arme Hebamme hatte aufgehört.</ta>
            <ta e="T623" id="Seg_8451" s="T611">"Na, Chagyday soll kommen, die Frau stirbt, er soll irgendetwas machen, er soll den Knochen hier zerteilen."</ta>
            <ta e="T639" id="Seg_8452" s="T623">Und so kommt dieser Chagyday, wie… er nahm das Kind von dieser Frau mit den Händen, so ließ er es am Leben, schau.</ta>
            <ta e="T643" id="Seg_8453" s="T639">So waren die alten Leute.</ta>
            <ta e="T648" id="Seg_8454" s="T643">Und das ist der Großvater des Mannes von Tschakyyla.</ta>
            <ta e="T664" id="Seg_8455" s="T651">– Ja, damals die Frau dieses Chagydaj, also die Mutter von Anna, diese alte Tscheeke.</ta>
            <ta e="T673" id="Seg_8456" s="T664">Ihr Vater ist gemeinsam mit unserem Großvater, dem alten Kudrjakov, geboren.</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KiPP" />
         <annotation name="ltr" tierref="ltr-KiPP">
            <ta e="T2" id="Seg_8457" s="T0">- Бегичева увозят.</ta>
            <ta e="T9" id="Seg_8458" s="T2">Зем… по земле ходил, вроде, раньше, экспедиции.</ta>
            <ta e="T12" id="Seg_8459" s="T9">То дедушка рассказывал.</ta>
            <ta e="T23" id="Seg_8460" s="T12">Моя мама, дед… это матери моей родственники: Маркел старик, Елены отец старик (отец Елены).</ta>
            <ta e="T26" id="Seg_8461" s="T23">Те как разговаривают слушала.</ta>
            <ta e="T35" id="Seg_8462" s="T26">Этот Бегичев, значит, на оленях отправляется в путь к Бегичева острову, у него семеро человек было.</ta>
            <ta e="T48" id="Seg_8463" s="T35">Этот человек, следуя по дороге: на оленях везут ведь, разными оленями везут, всё новыми и новыми оленями везут.</ta>
            <ta e="T57" id="Seg_8464" s="T48">Там Чагыдая старика просят повезти эти, моего старика (мужа) дедушка и его родня, дед их. </ta>
            <ta e="T67" id="Seg_8465" s="T57">Тот старик - ничто его не могло догнать, таким человеком был - очень энергичный человек. </ta>
            <ta e="T77" id="Seg_8466" s="T67">Тот старик.. необученных оленей заставив запрячь, к Бегичева острову его заставляют везти.</ta>
            <ta e="T83" id="Seg_8467" s="T77">Тогда олени засуетились, вроде, да.</ta>
            <ta e="T96" id="Seg_8468" s="T83">Олени засуетились, этот человек шапку, этого Бегичева шапку олень, зацепив, забросил, вроде.</ta>
            <ta e="T114" id="Seg_8469" s="T96">Через эти санки шапка пока падала, то старик, через санки запрыгнув, схватил вот, перед тем, как на землю упадёт.</ta>
            <ta e="T121" id="Seg_8470" s="T114">На это старик Бегичев говорит, вроде:</ta>
            <ta e="T130" id="Seg_8471" s="T121">"О-о, на этой долганской земле даже энергичные люди есть…"</ta>
            <ta e="T142" id="Seg_8472" s="T130">Вот, этот стари… Бегичев значит это, до того острова дошёл, так рассказывает, вроде.</ta>
            <ta e="T152" id="Seg_8473" s="T142">"Это что за диво (удивительное) увидел там?" - так спрашивают долганы, да.</ta>
            <ta e="T153" id="Seg_8474" s="T152">На это:</ta>
            <ta e="T169" id="Seg_8475" s="T153">"Я удивительного ничего сильно не обнаружил, моржи, значит, зубами завязав спят, спящих завязав за бревно".</ta>
            <ta e="T178" id="Seg_8476" s="T169">На тот что-то нужное даже не думают, силой затаскивают, в воду затаскивают, наверное.</ta>
            <ta e="T188" id="Seg_8477" s="T178">А потом на острове на каменной горе оказался, говорит, вроде, про одно диво.</ta>
            <ta e="T191" id="Seg_8478" s="T188">"Ну, что нашёл?"</ta>
            <ta e="T194" id="Seg_8479" s="T191">На это говорит вот:</ta>
            <ta e="T213" id="Seg_8480" s="T209">Туда зашёл", – говорит.</ta>
            <ta e="T220" id="Seg_8481" s="T213">Имеет троих человек, эти люди остались без еды.</ta>
            <ta e="T232" id="Seg_8482" s="T220">Тогда, значит, с залива, о, маленькую рыбку поймав, в те двери зайдя, чтобы поесть зашли.</ta>
            <ta e="T239" id="Seg_8483" s="T232">Этот человек только зашел, этот человек сидит. </ta>
            <ta e="T246" id="Seg_8484" s="T239">Когда он сидел, какой-то человек пришёл, да.</ta>
            <ta e="T250" id="Seg_8485" s="T246">Или человек, или что?</ta>
            <ta e="T263" id="Seg_8486" s="T250">"От плеча не имеющий головы человек зашёл, или человек, или что, ничего не понимаю", – говорит.</ta>
            <ta e="T269" id="Seg_8487" s="T263">Зайдя, у двери сел, да.</ta>
            <ta e="T273" id="Seg_8488" s="T269">Вот сидя, значит, ой.</ta>
            <ta e="T281" id="Seg_8489" s="T273">Засмеялся оскалившись, где-то глаз имеет, на макушке единственный глаз имеет.</ta>
            <ta e="T292" id="Seg_8490" s="T281">"Ой,…, друзья, крепко держитесь", – говорит на друга, на парней Бегичев.</ta>
            <ta e="T300" id="Seg_8491" s="T292">Тот со звуком "ху-ууп" засосал, двоих его людей засосал.</ta>
            <ta e="T306" id="Seg_8492" s="T300">Тогда этот человек с единственным человеком остался.</ta>
            <ta e="T313" id="Seg_8493" s="T306">На это этот человек пистолет имеет вот.</ta>
            <ta e="T321" id="Seg_8494" s="T313">Там говорю, говорящим языком говорю, - сказал, - пистолет мой в его рту.</ta>
            <ta e="T327" id="Seg_8495" s="T321">Так, на этой земле остаюсь что-ли я?"</ta>
            <ta e="T336" id="Seg_8496" s="T327">"Меня спаси, этого куда-нибудь, где можно лишить его сознания, доведи" - говорит.</ta>
            <ta e="T345" id="Seg_8497" s="T336">В лоб стрельнул вот этим пистолетом, тот вот без сознания упал.</ta>
            <ta e="T352" id="Seg_8498" s="T345">И когда тот лишился сознания, сверху выбежав, выбрались.</ta>
            <ta e="T356" id="Seg_8499" s="T352">Бегичев рассказывал:</ta>
            <ta e="T370" id="Seg_8500" s="T356">"Вот дойдя к тому пошли, - говорит, - к катеру своему пошёл, - говорит, - находясь на катере, до глубокого места залива зашёл.</ta>
            <ta e="T376" id="Seg_8501" s="T370">Посмотрел, сзади то самое оказалось, подумав.</ta>
            <ta e="T390" id="Seg_8502" s="T376">Оказавшись, значит, и чем катера здоровее булыжник взяв, как забросит (этот ?), тот, которого заставил стрелять.</ta>
            <ta e="T408" id="Seg_8503" s="T390">Когда забросил, хэ, еле-еле до середины залива дошёл, как дошёл в той волне, в которой забросил чуть не опрокинулся.</ta>
            <ta e="T412" id="Seg_8504" s="T408">Вот сбежали, сказал вот.</ta>
            <ta e="T420" id="Seg_8505" s="T412">Тогда из семерых человек с одним единственным человеком пришёл, оголодали сами(люди).</ta>
            <ta e="T431" id="Seg_8506" s="T420">Ничего не найдя, возвратившись.., вот приехав, в верховье поехал.</ta>
            <ta e="T438" id="Seg_8507" s="T431">Потом Чагыдай, значит, летом вместе живут, вроде.</ta>
            <ta e="T449" id="Seg_8508" s="T438">С пищей живут, вот письмо пришло, это письмо, значит, надо до Хатанги донести (довезти).</ta>
            <ta e="T457" id="Seg_8509" s="T449">Oткуда-то пришло на ветке, тогда откуда будут лодки.</ta>
            <ta e="T467" id="Seg_8510" s="T457">"Эй, Чагыдай, это письмо, значит, до туда донеси, в Кресты, в Новорыбинские Кресты."</ta>
            <ta e="T472" id="Seg_8511" s="T467">Смотри, как далеко, за Новорыбным.</ta>
            <ta e="T478" id="Seg_8512" s="T476">- Нет.</ta>
            <ta e="T482" id="Seg_8513" s="T478">Те, свои.</ta>
            <ta e="T496" id="Seg_8514" s="T482">- Да, а Бегичев так и отправился в путь… как отправлся тогда, так и уехал в эту сторону, в верховье.</ta>
            <ta e="T510" id="Seg_8515" s="T496">- И вот значит… вот письмо просят донести, тогда и письма были оказывается, и начальники были, старик..</ta>
            <ta e="T512" id="Seg_8516" s="T511">– Долганы.</ta>
            <ta e="T516" id="Seg_8517" s="T512">Те работники, советы и так далее.</ta>
            <ta e="T519" id="Seg_8518" s="T516">Посыльное отправляют вот..</ta>
            <ta e="T527" id="Seg_8519" s="T519">Таким образом… этот старик вот вечером тронулся в путь.</ta>
            <ta e="T536" id="Seg_8520" s="T527">"Утром, ко времени, когда ранняя женщина встаёт, приду", – сказал этот старик.</ta>
            <ta e="T547" id="Seg_8521" s="T536">"О, как ты сможешь вернуться со стороны за Новорыбной, очень далеко от Попигая".</ta>
            <ta e="T556" id="Seg_8522" s="T547">Ну, этот старик отправился в путь, э, поплыл на ветке-лодке.</ta>
            <ta e="T576" id="Seg_8523" s="T556">Рано утром, старушки став… , ранние женщины встают, например, тогда семь часов, может, было, а какие часы тогда, по дням смотрят вот.</ta>
            <ta e="T590" id="Seg_8524" s="T576">Ой, в семь часов Чагыдай пришёл, ветка-лодка его перевернувшись лежит вот, смотри, такие отважные люди есть.</ta>
            <ta e="T603" id="Seg_8525" s="T590">А затем женщина рожает на том стойбище, эта женщина чуть не умирает, ничего не могут поделать.</ta>
            <ta e="T611" id="Seg_8526" s="T603">В пору, когда не было докторов, а повитуха бедная уставши, прекратила. </ta>
            <ta e="T623" id="Seg_8527" s="T611">"Эй, Чагыдай пусть придёт, женщиа умирает, пусть что-нибудь сделает, пусть (кость?) разделит здесь".</ta>
            <ta e="T639" id="Seg_8528" s="T623">И таким образом, этот Чагыдай пришёл, как-то.. эту женщину рукой своей взял ребёнка, так вот живым оставил, смотри.</ta>
            <ta e="T643" id="Seg_8529" s="T639">Такие были старики.</ta>
            <ta e="T648" id="Seg_8530" s="T643">Э..(Это) Чакылы старика дед тот.</ta>
            <ta e="T664" id="Seg_8531" s="T651">- Да, тогда этого Чагыдая женщина, значит, мать Анны старушка, та Чээкэ старушка.</ta>
            <ta e="T673" id="Seg_8532" s="T664">Её отец с нашим дедушкой вместе родились (т.е.родные братья) Кудряков старик.</ta>
         </annotation>
         <annotation name="nt" tierref="nt-KiPP">
            <ta e="T220" id="Seg_8533" s="T213">[DCh]: Not clear, why 1PL in second clause.</ta>
            <ta e="T336" id="Seg_8534" s="T327">[DCh]: Apparently the protagonist is speaking to his pistol here (?).</ta>
            <ta e="T547" id="Seg_8535" s="T536">[DCh]: The speaker apparently talks about the river Popigay which has its mound into the river Khatanga downstream from Novorybnoe.</ta>
         </annotation>
      </segmented-tier>
      <segmented-tier category="tx"
                      display-name="tx-KuNS"
                      id="tx-KuNS"
                      speaker="KuNS"
                      type="t">
         <segmentation name="SpeakerContribution_Utterance_Word" tierref="tx-KuNS">
            <ts e="T477" id="Seg_8536" n="sc" s="T472">
               <ts e="T477" id="Seg_8538" n="HIAT:u" s="T472">
                  <ts e="T473" id="Seg_8540" n="HIAT:w" s="T472">Kim</ts>
                  <nts id="Seg_8541" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T474" id="Seg_8543" n="HIAT:w" s="T473">diːr</ts>
                  <nts id="Seg_8544" n="HIAT:ip">,</nts>
                  <nts id="Seg_8545" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T475" id="Seg_8547" n="HIAT:w" s="T474">Begičev</ts>
                  <nts id="Seg_8548" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T476" id="Seg_8550" n="HIAT:w" s="T475">diːr</ts>
                  <nts id="Seg_8551" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T477" id="Seg_8553" n="HIAT:w" s="T476">duː</ts>
                  <nts id="Seg_8554" n="HIAT:ip">?</nts>
                  <nts id="Seg_8555" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T489" id="Seg_8556" n="sc" s="T481">
               <ts e="T489" id="Seg_8558" n="HIAT:u" s="T481">
                  <ts e="T482" id="Seg_8560" n="HIAT:w" s="T481">Eː</ts>
                  <nts id="Seg_8561" n="HIAT:ip">,</nts>
                  <nts id="Seg_8562" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_8563" n="HIAT:ip">(</nts>
                  <ts e="T484" id="Seg_8565" n="HIAT:w" s="T482">bejeler-</ts>
                  <nts id="Seg_8566" n="HIAT:ip">)</nts>
                  <nts id="Seg_8567" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <nts id="Seg_8568" n="HIAT:ip">(</nts>
                  <nts id="Seg_8569" n="HIAT:ip">(</nts>
                  <ats e="T489" id="Seg_8570" n="HIAT:non-pho" s="T484">…</ats>
                  <nts id="Seg_8571" n="HIAT:ip">)</nts>
                  <nts id="Seg_8572" n="HIAT:ip">)</nts>
                  <nts id="Seg_8573" n="HIAT:ip">…</nts>
                  <nts id="Seg_8574" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T511" id="Seg_8575" n="sc" s="T508">
               <ts e="T511" id="Seg_8577" n="HIAT:u" s="T508">
                  <ts e="T511" id="Seg_8579" n="HIAT:w" s="T508">Hakalar</ts>
                  <nts id="Seg_8580" n="HIAT:ip">?</nts>
                  <nts id="Seg_8581" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T651" id="Seg_8582" n="sc" s="T648">
               <ts e="T651" id="Seg_8584" n="HIAT:u" s="T648">
                  <ts e="T649" id="Seg_8586" n="HIAT:w" s="T648">Eː</ts>
                  <nts id="Seg_8587" n="HIAT:ip">,</nts>
                  <nts id="Seg_8588" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T650" id="Seg_8590" n="HIAT:w" s="T649">ol</ts>
                  <nts id="Seg_8591" n="HIAT:ip"><![CDATA[ ]]></nts>
                  <ts e="T651" id="Seg_8593" n="HIAT:w" s="T650">Čagɨdaj</ts>
                  <nts id="Seg_8594" n="HIAT:ip">.</nts>
                  <nts id="Seg_8595" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
            <ts e="T673" id="Seg_8596" n="sc" s="T672">
               <ts e="T673" id="Seg_8598" n="HIAT:u" s="T672">
                  <nts id="Seg_8599" n="HIAT:ip">(</nts>
                  <ts e="T673" id="Seg_8601" n="HIAT:w" s="T672">Kudrʼa-</ts>
                  <nts id="Seg_8602" n="HIAT:ip">)</nts>
                  <nts id="Seg_8603" n="HIAT:ip">…</nts>
                  <nts id="Seg_8604" n="HIAT:ip"><![CDATA[ ]]></nts>
               </ts>
            </ts>
         </segmentation>
         <segmentation name="SpeakerContribution_Event" tierref="tx-KuNS">
            <ts e="T477" id="Seg_8605" n="sc" s="T472">
               <ts e="T473" id="Seg_8607" n="e" s="T472">Kim </ts>
               <ts e="T474" id="Seg_8609" n="e" s="T473">diːr, </ts>
               <ts e="T475" id="Seg_8611" n="e" s="T474">Begičev </ts>
               <ts e="T476" id="Seg_8613" n="e" s="T475">diːr </ts>
               <ts e="T477" id="Seg_8615" n="e" s="T476">duː? </ts>
            </ts>
            <ts e="T489" id="Seg_8616" n="sc" s="T481">
               <ts e="T482" id="Seg_8618" n="e" s="T481">Eː, </ts>
               <ts e="T484" id="Seg_8620" n="e" s="T482">(bejeler-) </ts>
               <ts e="T489" id="Seg_8622" n="e" s="T484">((…))… </ts>
            </ts>
            <ts e="T511" id="Seg_8623" n="sc" s="T508">
               <ts e="T511" id="Seg_8625" n="e" s="T508">Hakalar? </ts>
            </ts>
            <ts e="T651" id="Seg_8626" n="sc" s="T648">
               <ts e="T649" id="Seg_8628" n="e" s="T648">Eː, </ts>
               <ts e="T650" id="Seg_8630" n="e" s="T649">ol </ts>
               <ts e="T651" id="Seg_8632" n="e" s="T650">Čagɨdaj. </ts>
            </ts>
            <ts e="T673" id="Seg_8633" n="sc" s="T672">
               <ts e="T673" id="Seg_8635" n="e" s="T672">(Kudrʼa-)… </ts>
            </ts>
         </segmentation>
         <annotation name="ref" tierref="ref-KuNS">
            <ta e="T477" id="Seg_8636" s="T472">KiPP_KuNS_2002_LegendOfBegichev_nar.KuNS.001 (001.057)</ta>
            <ta e="T489" id="Seg_8637" s="T481">KiPP_KuNS_2002_LegendOfBegichev_nar.KuNS.002 (001.060)</ta>
            <ta e="T511" id="Seg_8638" s="T508">KiPP_KuNS_2002_LegendOfBegichev_nar.KuNS.003 (001.063)</ta>
            <ta e="T651" id="Seg_8639" s="T648">KiPP_KuNS_2002_LegendOfBegichev_nar.KuNS.004 (001.079)</ta>
            <ta e="T673" id="Seg_8640" s="T672">KiPP_KuNS_2002_LegendOfBegichev_nar.KuNS.005 (001.082)</ta>
         </annotation>
         <annotation name="st" tierref="st-KuNS">
            <ta e="T477" id="Seg_8641" s="T472">К: Ким диир, Бегичев диир дуу?</ta>
            <ta e="T489" id="Seg_8642" s="T481">К: Ээ, бэйэлир ги..</ta>
            <ta e="T511" id="Seg_8643" s="T508">К: Һакалар дуо?</ta>
            <ta e="T651" id="Seg_8644" s="T648">К: Ээ, ол Чагыдай.</ta>
            <ta e="T673" id="Seg_8645" s="T672">К: Кудря… </ta>
         </annotation>
         <annotation name="ts" tierref="ts-KuNS">
            <ta e="T477" id="Seg_8646" s="T472">– Kim diːr, Begičev diːr duː? </ta>
            <ta e="T489" id="Seg_8647" s="T481">– Eː, (bejeler-) ((…))…</ta>
            <ta e="T511" id="Seg_8648" s="T508">– Hakalar? </ta>
            <ta e="T651" id="Seg_8649" s="T648">– Eː, ol Čagɨdaj. </ta>
            <ta e="T673" id="Seg_8650" s="T672">– (Kudrʼa-)… </ta>
         </annotation>
         <annotation name="mb" tierref="mb-KuNS">
            <ta e="T473" id="Seg_8651" s="T472">kim</ta>
            <ta e="T474" id="Seg_8652" s="T473">diː-r</ta>
            <ta e="T475" id="Seg_8653" s="T474">Begičev</ta>
            <ta e="T476" id="Seg_8654" s="T475">diː-r</ta>
            <ta e="T477" id="Seg_8655" s="T476">duː</ta>
            <ta e="T482" id="Seg_8656" s="T481">eː</ta>
            <ta e="T484" id="Seg_8657" s="T482">beje-ler</ta>
            <ta e="T511" id="Seg_8658" s="T508">haka-lar</ta>
            <ta e="T649" id="Seg_8659" s="T648">eː</ta>
            <ta e="T650" id="Seg_8660" s="T649">ol</ta>
            <ta e="T651" id="Seg_8661" s="T650">Čagɨdaj</ta>
         </annotation>
         <annotation name="mp" tierref="mp-KuNS">
            <ta e="T473" id="Seg_8662" s="T472">kim</ta>
            <ta e="T474" id="Seg_8663" s="T473">di͡e-Ar</ta>
            <ta e="T475" id="Seg_8664" s="T474">Bi͡egičep</ta>
            <ta e="T476" id="Seg_8665" s="T475">di͡e-Ar</ta>
            <ta e="T477" id="Seg_8666" s="T476">du͡o</ta>
            <ta e="T482" id="Seg_8667" s="T481">eː</ta>
            <ta e="T484" id="Seg_8668" s="T482">beje-LAr</ta>
            <ta e="T511" id="Seg_8669" s="T508">haka-LAr</ta>
            <ta e="T649" id="Seg_8670" s="T648">eː</ta>
            <ta e="T650" id="Seg_8671" s="T649">ol</ta>
            <ta e="T651" id="Seg_8672" s="T650">Čagɨdaj</ta>
         </annotation>
         <annotation name="ge" tierref="ge-KuNS">
            <ta e="T473" id="Seg_8673" s="T472">who.[NOM]</ta>
            <ta e="T474" id="Seg_8674" s="T473">say-PRS.[3SG]</ta>
            <ta e="T475" id="Seg_8675" s="T474">Begichev.[NOM]</ta>
            <ta e="T476" id="Seg_8676" s="T475">say-PRS.[3SG]</ta>
            <ta e="T477" id="Seg_8677" s="T476">Q</ta>
            <ta e="T482" id="Seg_8678" s="T481">AFFIRM</ta>
            <ta e="T484" id="Seg_8679" s="T482">self-PL</ta>
            <ta e="T511" id="Seg_8680" s="T508">Dolgan-PL.[NOM]</ta>
            <ta e="T649" id="Seg_8681" s="T648">AFFIRM</ta>
            <ta e="T650" id="Seg_8682" s="T649">that</ta>
            <ta e="T651" id="Seg_8683" s="T650">Chagydaj.[NOM]</ta>
         </annotation>
         <annotation name="gg" tierref="gg-KuNS">
            <ta e="T473" id="Seg_8684" s="T472">wer.[NOM]</ta>
            <ta e="T474" id="Seg_8685" s="T473">sagen-PRS.[3SG]</ta>
            <ta e="T475" id="Seg_8686" s="T474">Begitschew.[NOM]</ta>
            <ta e="T476" id="Seg_8687" s="T475">sagen-PRS.[3SG]</ta>
            <ta e="T477" id="Seg_8688" s="T476">Q</ta>
            <ta e="T482" id="Seg_8689" s="T481">AFFIRM</ta>
            <ta e="T484" id="Seg_8690" s="T482">selbst-PL</ta>
            <ta e="T511" id="Seg_8691" s="T508">Dolgane-PL.[NOM]</ta>
            <ta e="T649" id="Seg_8692" s="T648">AFFIRM</ta>
            <ta e="T650" id="Seg_8693" s="T649">jenes</ta>
            <ta e="T651" id="Seg_8694" s="T650">Tschagydaj.[NOM]</ta>
         </annotation>
         <annotation name="gr" tierref="gr-KuNS">
            <ta e="T473" id="Seg_8695" s="T472">кто.[NOM]</ta>
            <ta e="T474" id="Seg_8696" s="T473">говорить-PRS.[3SG]</ta>
            <ta e="T475" id="Seg_8697" s="T474">Бегичев.[NOM]</ta>
            <ta e="T476" id="Seg_8698" s="T475">говорить-PRS.[3SG]</ta>
            <ta e="T477" id="Seg_8699" s="T476">Q</ta>
            <ta e="T482" id="Seg_8700" s="T481">AFFIRM</ta>
            <ta e="T484" id="Seg_8701" s="T482">сам-PL</ta>
            <ta e="T511" id="Seg_8702" s="T508">долганин-PL.[NOM]</ta>
            <ta e="T649" id="Seg_8703" s="T648">AFFIRM</ta>
            <ta e="T650" id="Seg_8704" s="T649">тот</ta>
            <ta e="T651" id="Seg_8705" s="T650">Чагыдай.[NOM]</ta>
         </annotation>
         <annotation name="mc" tierref="mc-KuNS">
            <ta e="T473" id="Seg_8706" s="T472">que-pro:case</ta>
            <ta e="T474" id="Seg_8707" s="T473">v-v:tense-v:pred.pn</ta>
            <ta e="T475" id="Seg_8708" s="T474">propr-n:case</ta>
            <ta e="T476" id="Seg_8709" s="T475">v-v:tense-v:pred.pn</ta>
            <ta e="T477" id="Seg_8710" s="T476">ptcl</ta>
            <ta e="T482" id="Seg_8711" s="T481">ptcl</ta>
            <ta e="T484" id="Seg_8712" s="T482">emphpro-pro:(num)</ta>
            <ta e="T511" id="Seg_8713" s="T508">n-n:(num)-n:case</ta>
            <ta e="T649" id="Seg_8714" s="T648">ptcl</ta>
            <ta e="T650" id="Seg_8715" s="T649">dempro</ta>
            <ta e="T651" id="Seg_8716" s="T650">propr-n:case</ta>
         </annotation>
         <annotation name="ps" tierref="ps-KuNS">
            <ta e="T473" id="Seg_8717" s="T472">que</ta>
            <ta e="T474" id="Seg_8718" s="T473">v</ta>
            <ta e="T475" id="Seg_8719" s="T474">propr</ta>
            <ta e="T476" id="Seg_8720" s="T475">v</ta>
            <ta e="T477" id="Seg_8721" s="T476">ptcl</ta>
            <ta e="T482" id="Seg_8722" s="T481">ptcl</ta>
            <ta e="T484" id="Seg_8723" s="T482">emphpro</ta>
            <ta e="T511" id="Seg_8724" s="T508">n</ta>
            <ta e="T649" id="Seg_8725" s="T648">ptcl</ta>
            <ta e="T650" id="Seg_8726" s="T649">dempro</ta>
            <ta e="T651" id="Seg_8727" s="T650">propr</ta>
         </annotation>
         <annotation name="SeR" tierref="SeR-KuNS" />
         <annotation name="SyF" tierref="SyF-KuNS" />
         <annotation name="IST" tierref="IST-KuNS" />
         <annotation name="Top" tierref="Top-KuNS" />
         <annotation name="Foc" tierref="Foc-KuNS" />
         <annotation name="BOR" tierref="BOR-KuNS">
            <ta e="T475" id="Seg_8728" s="T474">RUS:cult</ta>
         </annotation>
         <annotation name="BOR-Phon" tierref="BOR-Phon-KuNS" />
         <annotation name="BOR-Morph" tierref="BOR-Morph-KuNS" />
         <annotation name="CS" tierref="CS-KuNS" />
         <annotation name="fe" tierref="fe-KuNS">
            <ta e="T477" id="Seg_8729" s="T472">– Who says that, does Begichev say that?</ta>
            <ta e="T489" id="Seg_8730" s="T481">– Yes, his own…</ta>
            <ta e="T511" id="Seg_8731" s="T508">– The Dolgans?</ta>
            <ta e="T651" id="Seg_8732" s="T648">– Ah, that Chagyday?</ta>
            <ta e="T673" id="Seg_8733" s="T672">– Kudrya…</ta>
         </annotation>
         <annotation name="fg" tierref="fg-KuNS">
            <ta e="T477" id="Seg_8734" s="T472">– Wer sagt das, sagt das Begitschew?</ta>
            <ta e="T489" id="Seg_8735" s="T481">– Ja, seine…</ta>
            <ta e="T511" id="Seg_8736" s="T508">– Die Dolganen?</ta>
            <ta e="T651" id="Seg_8737" s="T648">– Ah, dieser Chagydaj.</ta>
            <ta e="T673" id="Seg_8738" s="T672">– Kudrja…</ta>
         </annotation>
         <annotation name="fr" tierref="fr-KuNS" />
         <annotation name="ltr" tierref="ltr-KuNS">
            <ta e="T477" id="Seg_8739" s="T472">- Кто говорит, Бегичев говорит что-ли?</ta>
            <ta e="T489" id="Seg_8740" s="T481">- А, свои..</ta>
            <ta e="T511" id="Seg_8741" s="T508">- Долганы что-ли?</ta>
            <ta e="T651" id="Seg_8742" s="T648">- Аа, тот Чагыдай.</ta>
            <ta e="T673" id="Seg_8743" s="T672">- Кудря… </ta>
         </annotation>
         <annotation name="nt" tierref="nt-KuNS" />
      </segmented-tier>
   </segmented-body>
   <conversion-info>
      <basic-transcription-conversion-info>
         <conversion-timeline>
            <conversion-tli id="T0" />
            <conversion-tli id="T1" />
            <conversion-tli id="T2" />
            <conversion-tli id="T4" />
            <conversion-tli id="T5" />
            <conversion-tli id="T6" />
            <conversion-tli id="T7" />
            <conversion-tli id="T8" />
            <conversion-tli id="T9" />
            <conversion-tli id="T10" />
            <conversion-tli id="T11" />
            <conversion-tli id="T12" />
            <conversion-tli id="T13" />
            <conversion-tli id="T14" />
            <conversion-tli id="T15" />
            <conversion-tli id="T16" />
            <conversion-tli id="T17" />
            <conversion-tli id="T18" />
            <conversion-tli id="T19" />
            <conversion-tli id="T20" />
            <conversion-tli id="T21" />
            <conversion-tli id="T22" />
            <conversion-tli id="T23" />
            <conversion-tli id="T24" />
            <conversion-tli id="T25" />
            <conversion-tli id="T26" />
            <conversion-tli id="T27" />
            <conversion-tli id="T28" />
            <conversion-tli id="T29" />
            <conversion-tli id="T30" />
            <conversion-tli id="T31" />
            <conversion-tli id="T32" />
            <conversion-tli id="T33" />
            <conversion-tli id="T34" />
            <conversion-tli id="T35" />
            <conversion-tli id="T36" />
            <conversion-tli id="T37" />
            <conversion-tli id="T38" />
            <conversion-tli id="T39" />
            <conversion-tli id="T40" />
            <conversion-tli id="T41" />
            <conversion-tli id="T42" />
            <conversion-tli id="T43" />
            <conversion-tli id="T44" />
            <conversion-tli id="T45" />
            <conversion-tli id="T46" />
            <conversion-tli id="T47" />
            <conversion-tli id="T48" />
            <conversion-tli id="T49" />
            <conversion-tli id="T50" />
            <conversion-tli id="T51" />
            <conversion-tli id="T52" />
            <conversion-tli id="T53" />
            <conversion-tli id="T54" />
            <conversion-tli id="T55" />
            <conversion-tli id="T56" />
            <conversion-tli id="T57" />
            <conversion-tli id="T58" />
            <conversion-tli id="T59" />
            <conversion-tli id="T60" />
            <conversion-tli id="T61" />
            <conversion-tli id="T62" />
            <conversion-tli id="T63" />
            <conversion-tli id="T64" />
            <conversion-tli id="T65" />
            <conversion-tli id="T66" />
            <conversion-tli id="T67" />
            <conversion-tli id="T68" />
            <conversion-tli id="T69" />
            <conversion-tli id="T70" />
            <conversion-tli id="T71" />
            <conversion-tli id="T72" />
            <conversion-tli id="T73" />
            <conversion-tli id="T74" />
            <conversion-tli id="T75" />
            <conversion-tli id="T76" />
            <conversion-tli id="T77" />
            <conversion-tli id="T78" />
            <conversion-tli id="T79" />
            <conversion-tli id="T80" />
            <conversion-tli id="T81" />
            <conversion-tli id="T82" />
            <conversion-tli id="T83" />
            <conversion-tli id="T84" />
            <conversion-tli id="T85" />
            <conversion-tli id="T86" />
            <conversion-tli id="T87" />
            <conversion-tli id="T88" />
            <conversion-tli id="T89" />
            <conversion-tli id="T90" />
            <conversion-tli id="T91" />
            <conversion-tli id="T92" />
            <conversion-tli id="T93" />
            <conversion-tli id="T94" />
            <conversion-tli id="T95" />
            <conversion-tli id="T96" />
            <conversion-tli id="T97" />
            <conversion-tli id="T98" />
            <conversion-tli id="T99" />
            <conversion-tli id="T100" />
            <conversion-tli id="T101" />
            <conversion-tli id="T102" />
            <conversion-tli id="T103" />
            <conversion-tli id="T104" />
            <conversion-tli id="T105" />
            <conversion-tli id="T106" />
            <conversion-tli id="T107" />
            <conversion-tli id="T108" />
            <conversion-tli id="T109" />
            <conversion-tli id="T110" />
            <conversion-tli id="T111" />
            <conversion-tli id="T112" />
            <conversion-tli id="T113" />
            <conversion-tli id="T114" />
            <conversion-tli id="T115" />
            <conversion-tli id="T116" />
            <conversion-tli id="T118" />
            <conversion-tli id="T119" />
            <conversion-tli id="T120" />
            <conversion-tli id="T121" />
            <conversion-tli id="T122" />
            <conversion-tli id="T123" />
            <conversion-tli id="T124" />
            <conversion-tli id="T125" />
            <conversion-tli id="T126" />
            <conversion-tli id="T127" />
            <conversion-tli id="T128" />
            <conversion-tli id="T129" />
            <conversion-tli id="T130" />
            <conversion-tli id="T131" />
            <conversion-tli id="T132" />
            <conversion-tli id="T133" />
            <conversion-tli id="T134" />
            <conversion-tli id="T135" />
            <conversion-tli id="T136" />
            <conversion-tli id="T137" />
            <conversion-tli id="T138" />
            <conversion-tli id="T139" />
            <conversion-tli id="T140" />
            <conversion-tli id="T141" />
            <conversion-tli id="T142" />
            <conversion-tli id="T143" />
            <conversion-tli id="T144" />
            <conversion-tli id="T145" />
            <conversion-tli id="T146" />
            <conversion-tli id="T147" />
            <conversion-tli id="T148" />
            <conversion-tli id="T149" />
            <conversion-tli id="T150" />
            <conversion-tli id="T151" />
            <conversion-tli id="T152" />
            <conversion-tli id="T153" />
            <conversion-tli id="T154" />
            <conversion-tli id="T155" />
            <conversion-tli id="T156" />
            <conversion-tli id="T157" />
            <conversion-tli id="T158" />
            <conversion-tli id="T159" />
            <conversion-tli id="T160" />
            <conversion-tli id="T161" />
            <conversion-tli id="T162" />
            <conversion-tli id="T163" />
            <conversion-tli id="T164" />
            <conversion-tli id="T165" />
            <conversion-tli id="T166" />
            <conversion-tli id="T167" />
            <conversion-tli id="T168" />
            <conversion-tli id="T169" />
            <conversion-tli id="T170" />
            <conversion-tli id="T171" />
            <conversion-tli id="T172" />
            <conversion-tli id="T173" />
            <conversion-tli id="T174" />
            <conversion-tli id="T175" />
            <conversion-tli id="T176" />
            <conversion-tli id="T177" />
            <conversion-tli id="T178" />
            <conversion-tli id="T179" />
            <conversion-tli id="T180" />
            <conversion-tli id="T181" />
            <conversion-tli id="T182" />
            <conversion-tli id="T183" />
            <conversion-tli id="T184" />
            <conversion-tli id="T185" />
            <conversion-tli id="T186" />
            <conversion-tli id="T187" />
            <conversion-tli id="T188" />
            <conversion-tli id="T189" />
            <conversion-tli id="T190" />
            <conversion-tli id="T191" />
            <conversion-tli id="T192" />
            <conversion-tli id="T193" />
            <conversion-tli id="T194" />
            <conversion-tli id="T209" />
            <conversion-tli id="T210" />
            <conversion-tli id="T211" />
            <conversion-tli id="T212" />
            <conversion-tli id="T213" />
            <conversion-tli id="T214" />
            <conversion-tli id="T215" />
            <conversion-tli id="T216" />
            <conversion-tli id="T217" />
            <conversion-tli id="T218" />
            <conversion-tli id="T219" />
            <conversion-tli id="T220" />
            <conversion-tli id="T221" />
            <conversion-tli id="T222" />
            <conversion-tli id="T223" />
            <conversion-tli id="T224" />
            <conversion-tli id="T225" />
            <conversion-tli id="T226" />
            <conversion-tli id="T227" />
            <conversion-tli id="T228" />
            <conversion-tli id="T229" />
            <conversion-tli id="T230" />
            <conversion-tli id="T231" />
            <conversion-tli id="T232" />
            <conversion-tli id="T233" />
            <conversion-tli id="T234" />
            <conversion-tli id="T235" />
            <conversion-tli id="T236" />
            <conversion-tli id="T237" />
            <conversion-tli id="T238" />
            <conversion-tli id="T239" />
            <conversion-tli id="T240" />
            <conversion-tli id="T241" />
            <conversion-tli id="T242" />
            <conversion-tli id="T243" />
            <conversion-tli id="T244" />
            <conversion-tli id="T245" />
            <conversion-tli id="T246" />
            <conversion-tli id="T247" />
            <conversion-tli id="T248" />
            <conversion-tli id="T249" />
            <conversion-tli id="T250" />
            <conversion-tli id="T251" />
            <conversion-tli id="T252" />
            <conversion-tli id="T253" />
            <conversion-tli id="T254" />
            <conversion-tli id="T255" />
            <conversion-tli id="T256" />
            <conversion-tli id="T257" />
            <conversion-tli id="T258" />
            <conversion-tli id="T259" />
            <conversion-tli id="T260" />
            <conversion-tli id="T261" />
            <conversion-tli id="T262" />
            <conversion-tli id="T263" />
            <conversion-tli id="T264" />
            <conversion-tli id="T265" />
            <conversion-tli id="T266" />
            <conversion-tli id="T267" />
            <conversion-tli id="T268" />
            <conversion-tli id="T269" />
            <conversion-tli id="T270" />
            <conversion-tli id="T271" />
            <conversion-tli id="T272" />
            <conversion-tli id="T273" />
            <conversion-tli id="T274" />
            <conversion-tli id="T275" />
            <conversion-tli id="T276" />
            <conversion-tli id="T277" />
            <conversion-tli id="T278" />
            <conversion-tli id="T279" />
            <conversion-tli id="T280" />
            <conversion-tli id="T281" />
            <conversion-tli id="T282" />
            <conversion-tli id="T284" />
            <conversion-tli id="T285" />
            <conversion-tli id="T286" />
            <conversion-tli id="T287" />
            <conversion-tli id="T288" />
            <conversion-tli id="T289" />
            <conversion-tli id="T290" />
            <conversion-tli id="T291" />
            <conversion-tli id="T292" />
            <conversion-tli id="T293" />
            <conversion-tli id="T294" />
            <conversion-tli id="T295" />
            <conversion-tli id="T296" />
            <conversion-tli id="T297" />
            <conversion-tli id="T298" />
            <conversion-tli id="T299" />
            <conversion-tli id="T300" />
            <conversion-tli id="T301" />
            <conversion-tli id="T302" />
            <conversion-tli id="T303" />
            <conversion-tli id="T304" />
            <conversion-tli id="T305" />
            <conversion-tli id="T306" />
            <conversion-tli id="T307" />
            <conversion-tli id="T308" />
            <conversion-tli id="T309" />
            <conversion-tli id="T310" />
            <conversion-tli id="T311" />
            <conversion-tli id="T312" />
            <conversion-tli id="T313" />
            <conversion-tli id="T314" />
            <conversion-tli id="T315" />
            <conversion-tli id="T316" />
            <conversion-tli id="T317" />
            <conversion-tli id="T318" />
            <conversion-tli id="T319" />
            <conversion-tli id="T320" />
            <conversion-tli id="T321" />
            <conversion-tli id="T322" />
            <conversion-tli id="T323" />
            <conversion-tli id="T324" />
            <conversion-tli id="T325" />
            <conversion-tli id="T326" />
            <conversion-tli id="T327" />
            <conversion-tli id="T328" />
            <conversion-tli id="T329" />
            <conversion-tli id="T330" />
            <conversion-tli id="T331" />
            <conversion-tli id="T332" />
            <conversion-tli id="T333" />
            <conversion-tli id="T334" />
            <conversion-tli id="T335" />
            <conversion-tli id="T336" />
            <conversion-tli id="T337" />
            <conversion-tli id="T338" />
            <conversion-tli id="T339" />
            <conversion-tli id="T340" />
            <conversion-tli id="T341" />
            <conversion-tli id="T342" />
            <conversion-tli id="T343" />
            <conversion-tli id="T344" />
            <conversion-tli id="T345" />
            <conversion-tli id="T346" />
            <conversion-tli id="T347" />
            <conversion-tli id="T348" />
            <conversion-tli id="T349" />
            <conversion-tli id="T350" />
            <conversion-tli id="T351" />
            <conversion-tli id="T352" />
            <conversion-tli id="T353" />
            <conversion-tli id="T354" />
            <conversion-tli id="T356" />
            <conversion-tli id="T357" />
            <conversion-tli id="T358" />
            <conversion-tli id="T359" />
            <conversion-tli id="T360" />
            <conversion-tli id="T361" />
            <conversion-tli id="T362" />
            <conversion-tli id="T363" />
            <conversion-tli id="T364" />
            <conversion-tli id="T365" />
            <conversion-tli id="T366" />
            <conversion-tli id="T367" />
            <conversion-tli id="T368" />
            <conversion-tli id="T369" />
            <conversion-tli id="T370" />
            <conversion-tli id="T371" />
            <conversion-tli id="T372" />
            <conversion-tli id="T373" />
            <conversion-tli id="T374" />
            <conversion-tli id="T375" />
            <conversion-tli id="T376" />
            <conversion-tli id="T377" />
            <conversion-tli id="T378" />
            <conversion-tli id="T379" />
            <conversion-tli id="T380" />
            <conversion-tli id="T381" />
            <conversion-tli id="T382" />
            <conversion-tli id="T383" />
            <conversion-tli id="T384" />
            <conversion-tli id="T385" />
            <conversion-tli id="T386" />
            <conversion-tli id="T387" />
            <conversion-tli id="T388" />
            <conversion-tli id="T389" />
            <conversion-tli id="T390" />
            <conversion-tli id="T391" />
            <conversion-tli id="T392" />
            <conversion-tli id="T393" />
            <conversion-tli id="T394" />
            <conversion-tli id="T395" />
            <conversion-tli id="T396" />
            <conversion-tli id="T397" />
            <conversion-tli id="T398" />
            <conversion-tli id="T399" />
            <conversion-tli id="T400" />
            <conversion-tli id="T401" />
            <conversion-tli id="T402" />
            <conversion-tli id="T403" />
            <conversion-tli id="T404" />
            <conversion-tli id="T405" />
            <conversion-tli id="T406" />
            <conversion-tli id="T407" />
            <conversion-tli id="T408" />
            <conversion-tli id="T409" />
            <conversion-tli id="T410" />
            <conversion-tli id="T411" />
            <conversion-tli id="T412" />
            <conversion-tli id="T413" />
            <conversion-tli id="T414" />
            <conversion-tli id="T415" />
            <conversion-tli id="T416" />
            <conversion-tli id="T417" />
            <conversion-tli id="T418" />
            <conversion-tli id="T419" />
            <conversion-tli id="T420" />
            <conversion-tli id="T421" />
            <conversion-tli id="T422" />
            <conversion-tli id="T423" />
            <conversion-tli id="T424" />
            <conversion-tli id="T425" />
            <conversion-tli id="T426" />
            <conversion-tli id="T427" />
            <conversion-tli id="T428" />
            <conversion-tli id="T429" />
            <conversion-tli id="T430" />
            <conversion-tli id="T431" />
            <conversion-tli id="T432" />
            <conversion-tli id="T433" />
            <conversion-tli id="T434" />
            <conversion-tli id="T435" />
            <conversion-tli id="T436" />
            <conversion-tli id="T437" />
            <conversion-tli id="T438" />
            <conversion-tli id="T439" />
            <conversion-tli id="T440" />
            <conversion-tli id="T441" />
            <conversion-tli id="T442" />
            <conversion-tli id="T443" />
            <conversion-tli id="T444" />
            <conversion-tli id="T445" />
            <conversion-tli id="T446" />
            <conversion-tli id="T447" />
            <conversion-tli id="T448" />
            <conversion-tli id="T449" />
            <conversion-tli id="T450" />
            <conversion-tli id="T451" />
            <conversion-tli id="T452" />
            <conversion-tli id="T453" />
            <conversion-tli id="T454" />
            <conversion-tli id="T455" />
            <conversion-tli id="T456" />
            <conversion-tli id="T457" />
            <conversion-tli id="T458" />
            <conversion-tli id="T459" />
            <conversion-tli id="T460" />
            <conversion-tli id="T461" />
            <conversion-tli id="T462" />
            <conversion-tli id="T463" />
            <conversion-tli id="T464" />
            <conversion-tli id="T465" />
            <conversion-tli id="T466" />
            <conversion-tli id="T467" />
            <conversion-tli id="T468" />
            <conversion-tli id="T469" />
            <conversion-tli id="T470" />
            <conversion-tli id="T471" />
            <conversion-tli id="T472" />
            <conversion-tli id="T473" />
            <conversion-tli id="T474" />
            <conversion-tli id="T475" />
            <conversion-tli id="T476" />
            <conversion-tli id="T477" />
            <conversion-tli id="T478" />
            <conversion-tli id="T479" />
            <conversion-tli id="T480" />
            <conversion-tli id="T481" />
            <conversion-tli id="T482" />
            <conversion-tli id="T484" />
            <conversion-tli id="T485" />
            <conversion-tli id="T486" />
            <conversion-tli id="T487" />
            <conversion-tli id="T488" />
            <conversion-tli id="T489" />
            <conversion-tli id="T490" />
            <conversion-tli id="T491" />
            <conversion-tli id="T492" />
            <conversion-tli id="T493" />
            <conversion-tli id="T494" />
            <conversion-tli id="T495" />
            <conversion-tli id="T496" />
            <conversion-tli id="T497" />
            <conversion-tli id="T498" />
            <conversion-tli id="T499" />
            <conversion-tli id="T500" />
            <conversion-tli id="T501" />
            <conversion-tli id="T502" />
            <conversion-tli id="T503" />
            <conversion-tli id="T504" />
            <conversion-tli id="T505" />
            <conversion-tli id="T506" />
            <conversion-tli id="T507" />
            <conversion-tli id="T508" />
            <conversion-tli id="T510" />
            <conversion-tli id="T511" />
            <conversion-tli id="T512" />
            <conversion-tli id="T513" />
            <conversion-tli id="T514" />
            <conversion-tli id="T515" />
            <conversion-tli id="T516" />
            <conversion-tli id="T517" />
            <conversion-tli id="T518" />
            <conversion-tli id="T519" />
            <conversion-tli id="T520" />
            <conversion-tli id="T521" />
            <conversion-tli id="T522" />
            <conversion-tli id="T523" />
            <conversion-tli id="T524" />
            <conversion-tli id="T525" />
            <conversion-tli id="T526" />
            <conversion-tli id="T527" />
            <conversion-tli id="T528" />
            <conversion-tli id="T529" />
            <conversion-tli id="T530" />
            <conversion-tli id="T531" />
            <conversion-tli id="T532" />
            <conversion-tli id="T533" />
            <conversion-tli id="T534" />
            <conversion-tli id="T535" />
            <conversion-tli id="T536" />
            <conversion-tli id="T537" />
            <conversion-tli id="T538" />
            <conversion-tli id="T539" />
            <conversion-tli id="T540" />
            <conversion-tli id="T541" />
            <conversion-tli id="T542" />
            <conversion-tli id="T543" />
            <conversion-tli id="T544" />
            <conversion-tli id="T545" />
            <conversion-tli id="T546" />
            <conversion-tli id="T547" />
            <conversion-tli id="T548" />
            <conversion-tli id="T549" />
            <conversion-tli id="T550" />
            <conversion-tli id="T551" />
            <conversion-tli id="T552" />
            <conversion-tli id="T553" />
            <conversion-tli id="T554" />
            <conversion-tli id="T555" />
            <conversion-tli id="T556" />
            <conversion-tli id="T557" />
            <conversion-tli id="T558" />
            <conversion-tli id="T560" />
            <conversion-tli id="T561" />
            <conversion-tli id="T562" />
            <conversion-tli id="T563" />
            <conversion-tli id="T564" />
            <conversion-tli id="T565" />
            <conversion-tli id="T566" />
            <conversion-tli id="T567" />
            <conversion-tli id="T568" />
            <conversion-tli id="T569" />
            <conversion-tli id="T570" />
            <conversion-tli id="T571" />
            <conversion-tli id="T572" />
            <conversion-tli id="T573" />
            <conversion-tli id="T574" />
            <conversion-tli id="T575" />
            <conversion-tli id="T576" />
            <conversion-tli id="T577" />
            <conversion-tli id="T578" />
            <conversion-tli id="T579" />
            <conversion-tli id="T580" />
            <conversion-tli id="T581" />
            <conversion-tli id="T582" />
            <conversion-tli id="T583" />
            <conversion-tli id="T584" />
            <conversion-tli id="T585" />
            <conversion-tli id="T586" />
            <conversion-tli id="T587" />
            <conversion-tli id="T588" />
            <conversion-tli id="T589" />
            <conversion-tli id="T590" />
            <conversion-tli id="T591" />
            <conversion-tli id="T592" />
            <conversion-tli id="T593" />
            <conversion-tli id="T594" />
            <conversion-tli id="T595" />
            <conversion-tli id="T596" />
            <conversion-tli id="T597" />
            <conversion-tli id="T598" />
            <conversion-tli id="T599" />
            <conversion-tli id="T600" />
            <conversion-tli id="T601" />
            <conversion-tli id="T602" />
            <conversion-tli id="T603" />
            <conversion-tli id="T604" />
            <conversion-tli id="T605" />
            <conversion-tli id="T606" />
            <conversion-tli id="T607" />
            <conversion-tli id="T608" />
            <conversion-tli id="T609" />
            <conversion-tli id="T610" />
            <conversion-tli id="T611" />
            <conversion-tli id="T612" />
            <conversion-tli id="T613" />
            <conversion-tli id="T614" />
            <conversion-tli id="T615" />
            <conversion-tli id="T616" />
            <conversion-tli id="T617" />
            <conversion-tli id="T618" />
            <conversion-tli id="T619" />
            <conversion-tli id="T620" />
            <conversion-tli id="T621" />
            <conversion-tli id="T622" />
            <conversion-tli id="T623" />
            <conversion-tli id="T624" />
            <conversion-tli id="T625" />
            <conversion-tli id="T626" />
            <conversion-tli id="T627" />
            <conversion-tli id="T628" />
            <conversion-tli id="T630" />
            <conversion-tli id="T631" />
            <conversion-tli id="T632" />
            <conversion-tli id="T633" />
            <conversion-tli id="T634" />
            <conversion-tli id="T635" />
            <conversion-tli id="T636" />
            <conversion-tli id="T637" />
            <conversion-tli id="T638" />
            <conversion-tli id="T639" />
            <conversion-tli id="T640" />
            <conversion-tli id="T641" />
            <conversion-tli id="T642" />
            <conversion-tli id="T643" />
            <conversion-tli id="T644" />
            <conversion-tli id="T645" />
            <conversion-tli id="T646" />
            <conversion-tli id="T647" />
            <conversion-tli id="T648" />
            <conversion-tli id="T649" />
            <conversion-tli id="T650" />
            <conversion-tli id="T651" />
            <conversion-tli id="T652" />
            <conversion-tli id="T653" />
            <conversion-tli id="T654" />
            <conversion-tli id="T655" />
            <conversion-tli id="T656" />
            <conversion-tli id="T657" />
            <conversion-tli id="T658" />
            <conversion-tli id="T659" />
            <conversion-tli id="T660" />
            <conversion-tli id="T661" />
            <conversion-tli id="T662" />
            <conversion-tli id="T663" />
            <conversion-tli id="T664" />
            <conversion-tli id="T665" />
            <conversion-tli id="T666" />
            <conversion-tli id="T667" />
            <conversion-tli id="T668" />
            <conversion-tli id="T669" />
            <conversion-tli id="T670" />
            <conversion-tli id="T671" />
            <conversion-tli id="T672" />
            <conversion-tli id="T3" />
            <conversion-tli id="T673" />
            <conversion-tli id="T675" />
            <conversion-tli id="T680" />
         </conversion-timeline>
         <conversion-tier category="ref"
                          display-name="ref-KiPP"
                          name="ref"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-KiPP"
                          name="st"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KiPP"
                          name="ts"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KiPP"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KiPP"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-KiPP"
                          name="mb"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-KiPP"
                          name="mp"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-KiPP"
                          name="ge"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-KiPP"
                          name="gg"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-KiPP"
                          name="gr"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-KiPP"
                          name="mc"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-KiPP"
                          name="ps"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-KiPP"
                          name="SeR"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-KiPP"
                          name="SyF"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-KiPP"
                          name="IST"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-KiPP"
                          name="Top"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-KiPP"
                          name="Foc"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-KiPP"
                          name="BOR"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-KiPP"
                          name="BOR-Phon"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-KiPP"
                          name="BOR-Morph"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-KiPP"
                          name="CS"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KiPP"
                          name="fe"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KiPP"
                          name="fg"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KiPP"
                          name="fr"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-KiPP"
                          name="ltr"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KiPP"
                          name="nt"
                          segmented-tier-id="tx-KiPP"
                          type="a" />
         <conversion-tier category="ref"
                          display-name="ref-KuNS"
                          name="ref"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="st"
                          display-name="st-KuNS"
                          name="st"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="ts"
                          display-name="ts-KuNS"
                          name="ts"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="tx"
                          display-name="tx-KuNS"
                          name="SpeakerContribution_Event"
                          segmented-tier-id="tx-KuNS"
                          type="t" />
         <conversion-tier category="mb"
                          display-name="mb-KuNS"
                          name="mb"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="mp"
                          display-name="mp-KuNS"
                          name="mp"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="ge"
                          display-name="ge-KuNS"
                          name="ge"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="gg"
                          display-name="gg-KuNS"
                          name="gg"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="gr"
                          display-name="gr-KuNS"
                          name="gr"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="mc"
                          display-name="mc-KuNS"
                          name="mc"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="ps"
                          display-name="ps-KuNS"
                          name="ps"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="SeR"
                          display-name="SeR-KuNS"
                          name="SeR"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="SyF"
                          display-name="SyF-KuNS"
                          name="SyF"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="IST"
                          display-name="IST-KuNS"
                          name="IST"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="Top"
                          display-name="Top-KuNS"
                          name="Top"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="Foc"
                          display-name="Foc-KuNS"
                          name="Foc"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="BOR"
                          display-name="BOR-KuNS"
                          name="BOR"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="BOR-Phon"
                          display-name="BOR-Phon-KuNS"
                          name="BOR-Phon"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="BOR-Morph"
                          display-name="BOR-Morph-KuNS"
                          name="BOR-Morph"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="CS"
                          display-name="CS-KuNS"
                          name="CS"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="fe"
                          display-name="fe-KuNS"
                          name="fe"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="fg"
                          display-name="fg-KuNS"
                          name="fg"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="fr"
                          display-name="fr-KuNS"
                          name="fr"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="ltr"
                          display-name="ltr-KuNS"
                          name="ltr"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
         <conversion-tier category="nt"
                          display-name="nt-KuNS"
                          name="nt"
                          segmented-tier-id="tx-KuNS"
                          type="a" />
      </basic-transcription-conversion-info>
   </conversion-info>
</segmented-transcription>
